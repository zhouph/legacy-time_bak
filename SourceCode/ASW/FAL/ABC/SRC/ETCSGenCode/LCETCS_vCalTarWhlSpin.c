/*
 * File: LCETCS_vCalTarWhlSpin.c
 *
 * Code generated for Simulink model 'LCETCS_vCalTarWhlSpin'.
 *
 * Model version                  : 1.181
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:11:27 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalTarWhlSpin.h"
#include "LCETCS_vCalTarWhlSpin_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalTarWhlSpin' */
void LCETCS_vCalTarWhlSpin_Init(B_LCETCS_vCalTarWhlSpin_c_T *localB,
  DW_LCETCS_vCalTarWhlSpin_f_T *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalBsTarWhlSpin' */
  LCETCS_vCalBsTarWhlSpin_Init(&(localDW->LCETCS_vCalBsTarWhlSpin_DWORK1.rtb));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalTarWhlSpinInLwToSplt' */
  LCETCS_vCalTarWhlSpinInLwToSplt_Init
    (&(localDW->LCETCS_vCalTarWhlSpinInLwToSplt_DWORK1.rtb));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalTarWhlSpnIncTire' */
  LCETCS_vCalTarWhlSpnIncTire_Init(&localB->lcetcsu1AbnrmSizeTire,
    &(localDW->LCETCS_vCalTarWhlSpnIncTire_DWORK1.rtb));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalAddTar4DepSnw' */
  LCETCS_vCalAddTar4DepSnw_Init(&(localDW->LCETCS_vCalAddTar4DepSnw_DWORK1.rtb));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalTarWhlSpnIncSpHill' */
  LCETCS_vCalTarWhlSpnIncSpHill_Init
    (&(localDW->LCETCS_vCalTarWhlSpnIncSpHill_DWORK1.rtb));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalTarSpnLmtCrnrg' */
  LCETCS_vCalTarSpnLmtCrnrg_Init(&localB->lcetcsu1TarSpnDec,
    &localB->lcetcsu1DctLmtCrng, &localB->lcetcsu1DctLmtCrngFalEdg,
    &(localDW->LCETCS_vCalTarSpnLmtCrnrg_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalTarWhlSpnIncHiRd' */
  LCETCS_vCalTarWhlSpnIncHiRd_Init
    (&(localDW->LCETCS_vCalTarWhlSpnIncHiRd_DWORK1.rtb));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalTarWhlSpnIncCtrlMd' */
  LCETCS_vCalTarWhlSpnIncCtrlMd_Init(&localB->lcetcss16AddTar4CtrlMode);

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCordTarWhlSpin' */
  LCETCS_vCordTarWhlSpin_Init(&(localDW->LCETCS_vCordTarWhlSpin_DWORK1.rtb),
    &(localDW->LCETCS_vCordTarWhlSpin_DWORK1.rtdw));
}

/* Start for referenced model: 'LCETCS_vCalTarWhlSpin' */
void LCETCS_vCalTarWhlSpin_Start(DW_LCETCS_vCalTarWhlSpin_f_T *localDW)
{
  /* Start for ModelReference: '<Root>/LCETCS_vCalBsTarWhlSpin' */
  LCETCS_vCalBsTarWhlSpin_Start(&(localDW->LCETCS_vCalBsTarWhlSpin_DWORK1.rtb));

  /* Start for ModelReference: '<Root>/LCETCS_vCalTarWhlSpnIncSpHill' */
  LCETCS_vCalTarWhlSpnIncSpHill_Start
    (&(localDW->LCETCS_vCalTarWhlSpnIncSpHill_DWORK1.rtb));

  /* Start for ModelReference: '<Root>/LCETCS_vCalTarSpnLmtCrnrg' */
  LCETCS_vCalTarSpnLmtCrnrg_Start
    (&(localDW->LCETCS_vCalTarSpnLmtCrnrg_DWORK1.rtdw));

  /* Start for ModelReference: '<Root>/LCETCS_vCordTarWhlSpin' */
  LCETCS_vCordTarWhlSpin_Start(&(localDW->LCETCS_vCordTarWhlSpin_DWORK1.rtb));
}

/* Output and update for referenced model: 'LCETCS_vCalTarWhlSpin' */
void LCETCS_vCalTarWhlSpin(const TypeETCSDepSnwStruct *rtu_ETCS_DEP_SNW, const
  TypeETCSLwToSpltStruct *rtu_ETCS_L2SPLT, int16_T rtu_lcetcss16VehSpd, const
  TypeETCSExtDctStruct *rtu_ETCS_EXT_DCT, const TypeETCSBrkSigStruct
  *rtu_ETCS_BRK_SIG, const TypeETCSAxlStruct *rtu_ETCS_FA, const
  TypeETCSAxlStruct *rtu_ETCS_RA, const TypeETCSEngStallStruct
  *rtu_ETCS_ENG_STALL_OLD, const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT_OLD,
  boolean_T rtu_lcetcsu1VehVibDctOld, const TypeETCSEscSigStruct
  *rtu_ETCS_ESC_SIG, const TypeETCSStrStatStruct *rtu_ETCS_STR_STAT, const
  TypeETCSSplitHillStruct *rtu_ETCS_SPLIT_HILL_OLD, const TypeETCSWhlStruct
  *rtu_ETCS_FL, const TypeETCSWhlStruct *rtu_ETCS_FR, const TypeETCSWhlStruct
  *rtu_ETCS_RL, const TypeETCSWhlStruct *rtu_ETCS_RR, const
  TypeETCSCtrlModeStruct *rtu_ETCS_CTRL_MODE, const TypeETCSHighDctStruct
  *rtu_ETCS_HI_DCT_OLD, const TypeETCSDrvMdlStruct *rtu_ETCS_DRV_MDL,
  TypeETCSTarSpinStruct *rty_ETCS_TAR_SPIN, B_LCETCS_vCalTarWhlSpin_c_T *localB,
  DW_LCETCS_vCalTarWhlSpin_f_T *localDW)
{
  /* local block i/o variables */
  int16_T rtb_lcetcss16BsTarWhlSpin;
  int16_T rtb_lcetcss16TarWhlSpinIncLwToSplt;
  int16_T rtb_lcetcss16CordBsTarWhlSpin;
  int16_T rtb_lcetcss16TarWhlSpnIncAbnrmTire;
  int16_T rtb_lcetcss16TarWhlSpnIncRughRd;
  int16_T rtb_lcetcss16AddTar4DepSnw;
  int16_T rtb_lcetcss16TarDecInTrn;
  int16_T rtb_lcetcss16DltYawStbCnt;
  int16_T rtb_lcetcss16TarSpnDecCnt;
  int16_T rtb_lcetcss16MaxDltYawInTarDecMd;
  int16_T rtb_lcetcss16TarDecDpndDelYaw;
  int16_T rtb_lcetcss16FnlTarSpnDec;
  int16_T rtb_lcetcss16LmtCrngDltYaw;
  int16_T rtb_lcetcss16TarWhlSpinInc;
  int16_T rtb_lcetcss16TarWhlSpdStlDetd;
  int16_T rtb_lcetcss16TarWhlSpnDecVib;
  int16_T rtb_lcetcss16TarWhlSpinDec;
  int16_T rtb_lcetcss16TarWhlSpin;
  uint8_T rtb_lcetcsu8AddTar4SpHill;
  uint8_T rtb_lcetcsu8AddTar4HighRd;

  /* ModelReference: '<Root>/LCETCS_vCalBsTarWhlSpin' */
  LCETCS_vCalBsTarWhlSpin(rtu_ETCS_BRK_SIG, rtu_ETCS_FA, rtu_ETCS_RA,
    rtu_lcetcss16VehSpd, &rtb_lcetcss16BsTarWhlSpin,
    &(localDW->LCETCS_vCalBsTarWhlSpin_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vCalTarWhlSpinInLwToSplt' */
  LCETCS_vCalTarWhlSpinInLwToSplt(rtu_ETCS_L2SPLT, rtu_ETCS_BRK_SIG,
    &rtb_lcetcss16TarWhlSpinIncLwToSplt,
    &(localDW->LCETCS_vCalTarWhlSpinInLwToSplt_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vCalCordBsTarWhlSpin' */
  LCETCS_vCalCordBsTarWhlSpin(rtb_lcetcss16BsTarWhlSpin,
    rtb_lcetcss16TarWhlSpinIncLwToSplt, &rtb_lcetcss16CordBsTarWhlSpin);

  /* ModelReference: '<Root>/LCETCS_vCalTarWhlSpnIncTire' */
  LCETCS_vCalTarWhlSpnIncTire(rtu_ETCS_FL, rtu_ETCS_FR, rtu_ETCS_RL, rtu_ETCS_RR,
    rtu_lcetcss16VehSpd, rtu_ETCS_EXT_DCT, &localB->lcetcsu1AbnrmSizeTire,
    &rtb_lcetcss16TarWhlSpnIncAbnrmTire,
    &(localDW->LCETCS_vCalTarWhlSpnIncTire_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vCalTarWhlSpnIncRughRd' */
  LCETCS_vCalTarWhlSpnIncRughRd(rtu_ETCS_FL, rtu_ETCS_FR, rtu_ETCS_RL,
    rtu_ETCS_RR, &rtb_lcetcss16TarWhlSpnIncRughRd);

  /* ModelReference: '<Root>/LCETCS_vCalAddTar4DepSnw' */
  LCETCS_vCalAddTar4DepSnw(rtu_lcetcss16VehSpd, rtu_ETCS_DEP_SNW,
    &rtb_lcetcss16AddTar4DepSnw, &(localDW->LCETCS_vCalAddTar4DepSnw_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vCalTarWhlSpnIncSpHill' */
  LCETCS_vCalTarWhlSpnIncSpHill(rtu_ETCS_SPLIT_HILL_OLD,
    &rtb_lcetcsu8AddTar4SpHill,
    &(localDW->LCETCS_vCalTarWhlSpnIncSpHill_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vCalTarSpnLmtCrnrg' */
  LCETCS_vCalTarSpnLmtCrnrg(rtu_lcetcss16VehSpd, rtu_ETCS_FA, rtu_ETCS_RA,
    rtu_ETCS_EXT_DCT, rtu_ETCS_ESC_SIG, rtu_ETCS_STR_STAT,
    &rtb_lcetcss16TarDecInTrn, &rtb_lcetcss16DltYawStbCnt,
    &localB->lcetcsu1TarSpnDec, &rtb_lcetcss16TarSpnDecCnt,
    &rtb_lcetcss16MaxDltYawInTarDecMd, &rtb_lcetcss16TarDecDpndDelYaw,
    &rtb_lcetcss16FnlTarSpnDec, &localB->lcetcsu1DctLmtCrng,
    &localB->lcetcsu1DctLmtCrngFalEdg, &rtb_lcetcss16LmtCrngDltYaw,
    &(localDW->LCETCS_vCalTarSpnLmtCrnrg_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalTarWhlSpnIncHiRd' */
  LCETCS_vCalTarWhlSpnIncHiRd(rtb_lcetcss16LmtCrngDltYaw, rtu_ETCS_HI_DCT_OLD,
    rtu_ETCS_ESC_SIG, &rtb_lcetcsu8AddTar4HighRd,
    &(localDW->LCETCS_vCalTarWhlSpnIncHiRd_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vCalTarWhlSpnIncCtrlMd' */
  LCETCS_vCalTarWhlSpnIncCtrlMd(rtu_ETCS_CTRL_MODE,
    &localB->lcetcss16AddTar4CtrlMode);

  /* ModelReference: '<Root>/LCETCS_vCalTarWhlSpinInc' */
  LCETCS_vCalTarWhlSpinInc(localB->lcetcsu1AbnrmSizeTire,
    rtb_lcetcss16TarWhlSpnIncAbnrmTire, rtu_ETCS_EXT_DCT,
    rtb_lcetcss16TarWhlSpnIncRughRd, rtb_lcetcss16AddTar4DepSnw,
    rtb_lcetcsu8AddTar4SpHill, rtb_lcetcsu8AddTar4HighRd,
    localB->lcetcss16AddTar4CtrlMode, &rtb_lcetcss16TarWhlSpinInc);

  /* ModelReference: '<Root>/LCETCS_vCalTarWhlSpdStlDetd' */
  LCETCS_vCalTarWhlSpdStlDetd(rtu_ETCS_ENG_STALL_OLD, rtu_ETCS_DRV_MDL,
    rtu_ETCS_CTL_ACT_OLD, &rtb_lcetcss16TarWhlSpdStlDetd);

  /* ModelReference: '<Root>/LCETCS_vCalTarWhlSpinDecVib' */
  LCETCS_vCalTarWhlSpinDecVib(rtu_lcetcsu1VehVibDctOld,
    &rtb_lcetcss16TarWhlSpnDecVib);

  /* ModelReference: '<Root>/LCETCS_vCalTarWhlSpinDec' */
  LCETCS_vCalTarWhlSpinDec(rtb_lcetcss16TarWhlSpnDecVib,
    rtb_lcetcss16TarWhlSpnIncAbnrmTire, rtb_lcetcss16TarDecInTrn,
    rtb_lcetcss16AddTar4DepSnw, &rtb_lcetcss16TarWhlSpinDec);

  /* ModelReference: '<Root>/LCETCS_vCordTarWhlSpin' */
  LCETCS_vCordTarWhlSpin(rtb_lcetcss16CordBsTarWhlSpin,
    rtb_lcetcss16TarWhlSpinInc, rtb_lcetcss16TarWhlSpdStlDetd,
    rtb_lcetcss16TarWhlSpinDec, &rtb_lcetcss16TarWhlSpin,
    &(localDW->LCETCS_vCordTarWhlSpin_DWORK1.rtb),
    &(localDW->LCETCS_vCordTarWhlSpin_DWORK1.rtdw));

  /* BusCreator: '<Root>/Bus Creator2' */
  rty_ETCS_TAR_SPIN->lcetcss16TarWhlSpin = rtb_lcetcss16TarWhlSpin;
  rty_ETCS_TAR_SPIN->lcetcss16BsTarWhlSpin = rtb_lcetcss16BsTarWhlSpin;
  rty_ETCS_TAR_SPIN->lcetcss16TarWhlSpinInc = rtb_lcetcss16TarWhlSpinInc;
  rty_ETCS_TAR_SPIN->lcetcss16TarWhlSpdStlDetd = rtb_lcetcss16TarWhlSpdStlDetd;
  rty_ETCS_TAR_SPIN->lcetcss16AddTar4DepSnw = rtb_lcetcss16AddTar4DepSnw;
  rty_ETCS_TAR_SPIN->lcetcss16LmtCrngDltYaw = rtb_lcetcss16LmtCrngDltYaw;
  rty_ETCS_TAR_SPIN->lcetcss16TarDecInTrn = rtb_lcetcss16TarDecInTrn;
  rty_ETCS_TAR_SPIN->lcetcss16DltYawStbCnt = rtb_lcetcss16DltYawStbCnt;
  rty_ETCS_TAR_SPIN->lcetcsu1TarSpnDec = localB->lcetcsu1TarSpnDec;
  rty_ETCS_TAR_SPIN->lcetcss16TarSpnDecCnt = rtb_lcetcss16TarSpnDecCnt;
  rty_ETCS_TAR_SPIN->lcetcss16MaxDltYawInTarDecMd =
    rtb_lcetcss16MaxDltYawInTarDecMd;
  rty_ETCS_TAR_SPIN->lcetcss16TarDecDpndDelYaw = rtb_lcetcss16TarDecDpndDelYaw;
  rty_ETCS_TAR_SPIN->lcetcss16FnlTarSpnDec = rtb_lcetcss16FnlTarSpnDec;
  rty_ETCS_TAR_SPIN->lcetcss16TarWhlSpnDecVib = rtb_lcetcss16TarWhlSpnDecVib;
  rty_ETCS_TAR_SPIN->lcetcsu1AbnrmSizeTire = localB->lcetcsu1AbnrmSizeTire;
  rty_ETCS_TAR_SPIN->lcetcss16TarWhlSpnIncAbnrmTire =
    rtb_lcetcss16TarWhlSpnIncAbnrmTire;
  rty_ETCS_TAR_SPIN->lcetcss16TarWhlSpinIncLwToSplt =
    rtb_lcetcss16TarWhlSpinIncLwToSplt;
  rty_ETCS_TAR_SPIN->lcetcss16CordBsTarWhlSpin = rtb_lcetcss16CordBsTarWhlSpin;
  rty_ETCS_TAR_SPIN->lcetcsu1DctLmtCrng = localB->lcetcsu1DctLmtCrng;
  rty_ETCS_TAR_SPIN->lcetcsu1DctLmtCrngFalEdg = localB->lcetcsu1DctLmtCrngFalEdg;
}

/* Model initialize function */
void LCETCS_vCalTarWhlSpin_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalAddTar4DepSnw' */
  LCETCS_vCalAddTar4DepSnw_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalBsTarWhlSpin' */
  LCETCS_vCalBsTarWhlSpin_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalCordBsTarWhlSpin' */
  LCETCS_vCalCordBsTarWhlSpin_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalTarSpnLmtCrnrg' */
  LCETCS_vCalTarSpnLmtCrnrg_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalTarWhlSpdStlDetd' */
  LCETCS_vCalTarWhlSpdStlDetd_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalTarWhlSpinDec' */
  LCETCS_vCalTarWhlSpinDec_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalTarWhlSpinDecVib' */
  LCETCS_vCalTarWhlSpinDecVib_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalTarWhlSpinInLwToSplt' */
  LCETCS_vCalTarWhlSpinInLwToSplt_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalTarWhlSpinInc' */
  LCETCS_vCalTarWhlSpinInc_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalTarWhlSpnIncCtrlMd' */
  LCETCS_vCalTarWhlSpnIncCtrlMd_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalTarWhlSpnIncHiRd' */
  LCETCS_vCalTarWhlSpnIncHiRd_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalTarWhlSpnIncRughRd' */
  LCETCS_vCalTarWhlSpnIncRughRd_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalTarWhlSpnIncSpHill' */
  LCETCS_vCalTarWhlSpnIncSpHill_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalTarWhlSpnIncTire' */
  LCETCS_vCalTarWhlSpnIncTire_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCordTarWhlSpin' */
  LCETCS_vCordTarWhlSpin_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
