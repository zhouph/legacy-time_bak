/*
 * File: LCETCS_vCalFnlTarSpnDec.h
 *
 * Code generated for Simulink model 'LCETCS_vCalFnlTarSpnDec'.
 *
 * Model version                  : 1.16
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:59:35 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalFnlTarSpnDec_h_
#define RTW_HEADER_LCETCS_vCalFnlTarSpnDec_h_
#ifndef LCETCS_vCalFnlTarSpnDec_COMMON_INCLUDES_
# define LCETCS_vCalFnlTarSpnDec_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalFnlTarSpnDec_COMMON_INCLUDES_ */

#include "LCETCS_vCalFnlTarSpnDec_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_s16Inter3by5.h"

/* Block signals for model 'LCETCS_vCalFnlTarSpnDec' */
typedef struct {
  int16_T x3;                          /* '<Root>/Ay level on asphalt' */
  int16_T x1;                          /* '<Root>/Ay level on ice' */
  int16_T x2;                          /* '<Root>/Ay level on snow' */
  int16_T z13;                         /* '<Root>/Final target spin decrease on asphalt 1' */
  int16_T z23;                         /* '<Root>/Final target spin decrease on asphalt 2' */
  int16_T z33;                         /* '<Root>/Final target spin decrease on asphalt 3' */
  int16_T z43;                         /* '<Root>/Final target spin decrease on asphalt 4' */
  int16_T z53;                         /* '<Root>/Final target spin decrease on asphalt 5' */
  int16_T z11;                         /* '<Root>/Final target spin decrease on ice 1' */
  int16_T z21;                         /* '<Root>/Final target spin decrease on ice 2' */
  int16_T z31;                         /* '<Root>/Final target spin decrease on ice 3' */
  int16_T z41;                         /* '<Root>/Final target spin decrease on ice 4' */
  int16_T z51;                         /* '<Root>/Final target spin decrease on ice 5' */
  int16_T z12;                         /* '<Root>/Final target spin decrease on snow 1' */
  int16_T z22;                         /* '<Root>/Final target spin decrease on snow 2' */
  int16_T z32;                         /* '<Root>/Final target spin decrease on snow 3' */
  int16_T z42;                         /* '<Root>/Final target spin decrease on snow 4' */
  int16_T z52;                         /* '<Root>/Final target spin decrease on snow 5' */
  int16_T Out;                         /* '<Root>/LCETCS_s16Inter3by5' */
} B_LCETCS_vCalFnlTarSpnDec_c_T;

typedef struct {
  B_LCETCS_vCalFnlTarSpnDec_c_T rtb;
} MdlrefDW_LCETCS_vCalFnlTarSpnDec_T;

/* Model reference registration function */
extern void LCETCS_vCalFnlTarSpnDec_initialize(void);
extern void LCETCS_vCalFnlTarSpnDec_Init(B_LCETCS_vCalFnlTarSpnDec_c_T *localB);
extern void LCETCS_vCalFnlTarSpnDec_Start(B_LCETCS_vCalFnlTarSpnDec_c_T *localB);
extern void LCETCS_vCalFnlTarSpnDec(const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG,
  int16_T rtu_lcetcss16VehSpd, int16_T *rty_lcetcss16FnlTarSpnDec,
  B_LCETCS_vCalFnlTarSpnDec_c_T *localB);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalFnlTarSpnDec'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalFnlTarSpnDec_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
