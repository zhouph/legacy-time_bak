/*
 * File: LCTMC_vDctDriveMode.c
 *
 * Code generated for Simulink model 'LCTMC_vDctDriveMode'.
 *
 * Model version                  : 1.235
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:53:13 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCTMC_vDctDriveMode.h"
#include "LCTMC_vDctDriveMode_private.h"

/* Initial conditions for referenced model: 'LCTMC_vDctDriveMode' */
void LCTMC_vDctDriveMode_Init(boolean_T *rty_lctmcu1DriveGearMode,
  B_LCTMC_vDctDriveMode_c_T *localB)
{
  /* InitializeConditions for Chart: '<Root>/Chart1' */
  *rty_lctmcu1DriveGearMode = false;
  localB->lctmcu8DriveGearModeCnt = 0U;
}

/* Output and update for referenced model: 'LCTMC_vDctDriveMode' */
void LCTMC_vDctDriveMode(const TypeETCSGearStruct *rtu_ETCS_GEAR_STRUCT,
  boolean_T *rty_lctmcu1DriveGearMode, B_LCTMC_vDctDriveMode_c_T *localB)
{
  int32_T tmp;

  /* Chart: '<Root>/Chart1' incorporates:
   *  UnitDelay: '<Root>/Unit Delay1'
   */
  /* Gateway: Chart1 */
  /* During: Chart1 */
  /* Entry Internal: Chart1 */
  /* Transition: '<S1>:10' */
  /* comment */
  if (((((rtu_ETCS_GEAR_STRUCT->lcetcss16CurntGearStep == ((int16_T)
           GEAR_POS_TM_PN)) || (rtu_ETCS_GEAR_STRUCT->lcetcss16CurntGearStep ==
          ((int16_T)GEAR_POS_TM_R))) ||
        (rtu_ETCS_GEAR_STRUCT->lcetcss16CurntGearStep > ((uint8_T)U8TMCpMaxGear)))
       || (rtu_ETCS_GEAR_STRUCT->lcetcss16CurntGearStep < ((uint8_T)
         U8TMCpMinGear))) || (rtu_ETCS_GEAR_STRUCT->lcetcsu8DrvGearSel ==
       ((int16_T)GEAR_SEL_M))) {
    /* Transition: '<S1>:1' */
    /* Transition: '<S1>:2' */
    *rty_lctmcu1DriveGearMode = false;
    localB->lctmcu8DriveGearModeCnt = 0U;

    /* Transition: '<S1>:32' */
    /* Transition: '<S1>:31' */
  } else {
    /* Transition: '<S1>:11' */
    if (localB->lctmcu8DriveGearModeCnt > 10) {
      /* Transition: '<S1>:25' */
      /* Transition: '<S1>:27' */
      *rty_lctmcu1DriveGearMode = true;

      /* Transition: '<S1>:31' */
    } else {
      /* Transition: '<S1>:30' */
      *rty_lctmcu1DriveGearMode = false;
      tmp = localB->lctmcu8DriveGearModeCnt + 1;
      if (tmp > 255) {
        tmp = 255;
      }

      localB->lctmcu8DriveGearModeCnt = (uint8_T)tmp;
    }
  }

  /* End of Chart: '<Root>/Chart1' */
  /* Transition: '<S1>:34' */
}

/* Model initialize function */
void LCTMC_vDctDriveMode_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
