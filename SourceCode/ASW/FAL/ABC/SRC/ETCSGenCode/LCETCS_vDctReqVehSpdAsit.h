/*
 * File: LCETCS_vDctReqVehSpdAsit.h
 *
 * Code generated for Simulink model 'LCETCS_vDctReqVehSpdAsit'.
 *
 * Model version                  : 1.156
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:10:27 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vDctReqVehSpdAsit_h_
#define RTW_HEADER_LCETCS_vDctReqVehSpdAsit_h_
#ifndef LCETCS_vDctReqVehSpdAsit_COMMON_INCLUDES_
# define LCETCS_vDctReqVehSpdAsit_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vDctReqVehSpdAsit_COMMON_INCLUDES_ */

#include "LCETCS_vDctReqVehSpdAsit_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vDctSetVca.h"
#include "LCETCS_vDctReqVcaAct.h"
#include "LCETCS_vDctInhibitBrkVca.h"

/* Block signals for model 'LCETCS_vDctReqVehSpdAsit' */
typedef struct {
  boolean_T lcetcsu1InhibitBrkVca;     /* '<Root>/LCETCS_vDctInhibitBrkVca' */
  boolean_T lcetcsu1ReqVcaTrqAct;      /* '<Root>/LCETCS_vDctSetVca' */
  boolean_T lcetcsu1ReqVcaEngTrqAct;   /* '<Root>/LCETCS_vDctReqVcaAct' */
  boolean_T lcetcsu1ReqVcaEngTrqRsgEdg;/* '<Root>/LCETCS_vDctReqVcaAct' */
  boolean_T lcetcsu1ReqVcaBrkTrqAct;   /* '<Root>/LCETCS_vDctReqVcaAct' */
  boolean_T lcetcsu1ReqVcaBrkTrqRsgEdg;/* '<Root>/LCETCS_vDctReqVcaAct' */
} B_LCETCS_vDctReqVehSpdAsit_c_T;

/* Block states (auto storage) for model 'LCETCS_vDctReqVehSpdAsit' */
typedef struct {
  MdlrefDW_LCETCS_vDctInhibitBrkVca_T LCETCS_vDctInhibitBrkVca_DWORK1;/* '<Root>/LCETCS_vDctInhibitBrkVca' */
  MdlrefDW_LCETCS_vDctSetVca_T LCETCS_vDctSetVca_DWORK1;/* '<Root>/LCETCS_vDctSetVca' */
  MdlrefDW_LCETCS_vDctReqVcaAct_T LCETCS_vDctReqVcaAct_DWORK1;/* '<Root>/LCETCS_vDctReqVcaAct' */
} DW_LCETCS_vDctReqVehSpdAsit_f_T;

typedef struct {
  B_LCETCS_vDctReqVehSpdAsit_c_T rtb;
  DW_LCETCS_vDctReqVehSpdAsit_f_T rtdw;
} MdlrefDW_LCETCS_vDctReqVehSpdAsit_T;

/* Model reference registration function */
extern void LCETCS_vDctReqVehSpdAsit_initialize(void);
extern void LCETCS_vDctReqVehSpdAsit_Init(B_LCETCS_vDctReqVehSpdAsit_c_T *localB,
  DW_LCETCS_vDctReqVehSpdAsit_f_T *localDW);
extern void LCETCS_vDctReqVehSpdAsit(const TypeETCSCtlActStruct
  *rtu_ETCS_CTL_ACT, const TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC,
  TypeETCSReqVCAStruct *rty_ETCS_REQ_VCA, B_LCETCS_vDctReqVehSpdAsit_c_T *localB,
  DW_LCETCS_vDctReqVehSpdAsit_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vDctReqVehSpdAsit'
 */
#endif                                 /* RTW_HEADER_LCETCS_vDctReqVehSpdAsit_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
