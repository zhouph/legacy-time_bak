/*
 * File: LCETCS_vCalCordRefRdFric.c
 *
 * Code generated for Simulink model 'LCETCS_vCalCordRefRdFric'.
 *
 * Model version                  : 1.46
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:50:11 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalCordRefRdFric.h"
#include "LCETCS_vCalCordRefRdFric_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalCordRefRdFric' */
void LCETCS_vCalCordRefRdFric_Init(int16_T *rty_lcetcss16RefRdCfFricAtIce,
  int16_T *rty_lcetcss16RefRdCfFricAtSnow, int16_T
  *rty_lcetcss16RefRdCfFricAtAsphalt)
{
  /* InitializeConditions for Chart: '<Root>/Chart' */
  *rty_lcetcss16RefRdCfFricAtIce = 0;
  *rty_lcetcss16RefRdCfFricAtSnow = 0;
  *rty_lcetcss16RefRdCfFricAtAsphalt = 0;
}

/* Output and update for referenced model: 'LCETCS_vCalCordRefRdFric' */
void LCETCS_vCalCordRefRdFric(int16_T rtu_lcetcss16ScaleDnAy, int16_T
  rtu_lcetcss16RefAxRdFricAtIce, int16_T rtu_lcetcss16RefAxRdFricAtSnow, int16_T
  rtu_lcetcss16RefAxRdFricAtAsphalt, int16_T *rty_lcetcss16RefRdCfFricAtIce,
  int16_T *rty_lcetcss16RefRdCfFricAtSnow, int16_T
  *rty_lcetcss16RefRdCfFricAtAsphalt)
{
  int16_T rtb_MathFunction2;
  int16_T rtb_Saturation;
  int16_T rtb_Saturation1;

  /* Math: '<Root>/Math Function2' */
  rtb_MathFunction2 = (int16_T)(rtu_lcetcss16ScaleDnAy * rtu_lcetcss16ScaleDnAy);

  /* Sqrt: '<Root>/Sqrt' incorporates:
   *  Math: '<Root>/Math Function1'
   *  Sum: '<Root>/Add'
   */
  rtb_Saturation = rt_sqrts16__s((int16_T)((rtu_lcetcss16RefAxRdFricAtIce *
    rtu_lcetcss16RefAxRdFricAtIce) + rtb_MathFunction2));

  /* Saturate: '<Root>/Saturation' */
  if (rtb_Saturation > 125) {
    rtb_Saturation = 125;
  } else {
    if (rtb_Saturation < 0) {
      rtb_Saturation = 0;
    }
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Sqrt: '<Root>/Sqrt1' incorporates:
   *  Math: '<Root>/Math Function3'
   *  Sum: '<Root>/Add1'
   */
  rtb_Saturation1 = rt_sqrts16__s((int16_T)((rtu_lcetcss16RefAxRdFricAtSnow *
    rtu_lcetcss16RefAxRdFricAtSnow) + rtb_MathFunction2));

  /* Saturate: '<Root>/Saturation1' */
  if (rtb_Saturation1 > 125) {
    rtb_Saturation1 = 125;
  } else {
    if (rtb_Saturation1 < 0) {
      rtb_Saturation1 = 0;
    }
  }

  /* End of Saturate: '<Root>/Saturation1' */

  /* Sqrt: '<Root>/Sqrt2' incorporates:
   *  Math: '<Root>/Math Function4'
   *  Sum: '<Root>/Add2'
   */
  rtb_MathFunction2 = rt_sqrts16__s((int16_T)((rtu_lcetcss16RefAxRdFricAtAsphalt
    * rtu_lcetcss16RefAxRdFricAtAsphalt) + rtb_MathFunction2));

  /* Saturate: '<Root>/Saturation2' */
  if (rtb_MathFunction2 > 125) {
    rtb_MathFunction2 = 125;
  } else {
    if (rtb_MathFunction2 < 0) {
      rtb_MathFunction2 = 0;
    }
  }

  /* End of Saturate: '<Root>/Saturation2' */

  /* Chart: '<Root>/Chart' */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:9' */
  /* comment */
  if (((uint8_T)U8ETCSCpAxAtIce) >= rtb_Saturation) {
    /* Transition: '<S1>:72' */
    /* Transition: '<S1>:75' */
    *rty_lcetcss16RefRdCfFricAtAsphalt = rtb_MathFunction2;
    *rty_lcetcss16RefRdCfFricAtSnow = rtb_Saturation1;
    *rty_lcetcss16RefRdCfFricAtIce = rtb_Saturation;

    /* Transition: '<S1>:76' */
    /* Transition: '<S1>:65' */
    /* Transition: '<S1>:66' */
  } else {
    /* Transition: '<S1>:73' */
    if (((uint8_T)U8ETCSCpAxAtSnow) >= rtb_Saturation1) {
      /* Transition: '<S1>:10' */
      /* Transition: '<S1>:40' */
      *rty_lcetcss16RefRdCfFricAtAsphalt = rtb_MathFunction2;
      *rty_lcetcss16RefRdCfFricAtSnow = rtb_Saturation1;
      *rty_lcetcss16RefRdCfFricAtIce = ((uint8_T)U8ETCSCpAxAtIce);

      /* Transition: '<S1>:65' */
      /* Transition: '<S1>:66' */
    } else {
      /* Transition: '<S1>:13' */
      if (((uint8_T)U8ETCSCpAxAtAsphalt) >= rtb_MathFunction2) {
        /* Transition: '<S1>:62' */
        /* Transition: '<S1>:64' */
        *rty_lcetcss16RefRdCfFricAtAsphalt = rtb_MathFunction2;
        *rty_lcetcss16RefRdCfFricAtSnow = ((uint8_T)U8ETCSCpAxAtSnow);
        *rty_lcetcss16RefRdCfFricAtIce = ((uint8_T)U8ETCSCpAxAtIce);

        /* Transition: '<S1>:66' */
      } else {
        /* Transition: '<S1>:63' */
        *rty_lcetcss16RefRdCfFricAtAsphalt = ((uint8_T)U8ETCSCpAxAtAsphalt);
        *rty_lcetcss16RefRdCfFricAtSnow = ((uint8_T)U8ETCSCpAxAtSnow);
        *rty_lcetcss16RefRdCfFricAtIce = ((uint8_T)U8ETCSCpAxAtIce);
      }
    }
  }

  /* End of Chart: '<Root>/Chart' */
  /* Transition: '<S1>:68' */
}

/* Model initialize function */
void LCETCS_vCalCordRefRdFric_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
