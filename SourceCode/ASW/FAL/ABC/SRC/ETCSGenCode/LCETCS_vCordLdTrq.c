/*
 * File: LCETCS_vCordLdTrq.c
 *
 * Code generated for Simulink model 'LCETCS_vCordLdTrq'.
 *
 * Model version                  : 1.206
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:48:21 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCordLdTrq.h"
#include "LCETCS_vCordLdTrq_private.h"

/* Output and update for referenced model: 'LCETCS_vCordLdTrq' */
void LCETCS_vCordLdTrq(int16_T rtu_lcetcss16LdTrqByGrd, int32_T
  rtu_lcetcss32LdTrqByAsymBrkCtl, int32_T *rty_lcetcss32LdTrq)
{
  /* Product: '<Root>/Divide3' */
  *rty_lcetcss32LdTrq = rtu_lcetcss16LdTrqByGrd * 10;

  /* MinMax: '<Root>/MinMax' */
  if (rtu_lcetcss32LdTrqByAsymBrkCtl >= (*rty_lcetcss32LdTrq)) {
    *rty_lcetcss32LdTrq = rtu_lcetcss32LdTrqByAsymBrkCtl;
  }

  /* End of MinMax: '<Root>/MinMax' */
}

/* Model initialize function */
void LCETCS_vCordLdTrq_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
