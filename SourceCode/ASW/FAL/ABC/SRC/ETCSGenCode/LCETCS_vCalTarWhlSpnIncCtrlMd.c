/*
 * File: LCETCS_vCalTarWhlSpnIncCtrlMd.c
 *
 * Code generated for Simulink model 'LCETCS_vCalTarWhlSpnIncCtrlMd'.
 *
 * Model version                  : 1.79
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:49:34 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalTarWhlSpnIncCtrlMd.h"
#include "LCETCS_vCalTarWhlSpnIncCtrlMd_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalTarWhlSpnIncCtrlMd' */
void LCETCS_vCalTarWhlSpnIncCtrlMd_Init(int16_T *rty_lcetcss16AddTar4CtrlMode)
{
  /* InitializeConditions for Chart: '<Root>/Chart1' */
  *rty_lcetcss16AddTar4CtrlMode = 0;
}

/* Output and update for referenced model: 'LCETCS_vCalTarWhlSpnIncCtrlMd' */
void LCETCS_vCalTarWhlSpnIncCtrlMd(const TypeETCSCtrlModeStruct
  *rtu_ETCS_CTRL_MODE, int16_T *rty_lcetcss16AddTar4CtrlMode)
{
  /* Chart: '<Root>/Chart1' */
  /* Gateway: Chart1 */
  /* During: Chart1 */
  /* Entry Internal: Chart1 */
  /* Transition: '<S1>:11' */
  /* comment */
  if (rtu_ETCS_CTRL_MODE->lcetcsu8EngCtrlMode == ((uint8_T)TCSDpCtrlModeSports1))
  {
    /* Transition: '<S1>:10' */
    /* Transition: '<S1>:9' */
    *rty_lcetcss16AddTar4CtrlMode = ((uint8_T)U8ETCSCpTarSpnAdd4Sp);

    /* Transition: '<S1>:23' */
    /* Transition: '<S1>:24' */
  } else {
    /* Transition: '<S1>:7' */
    if (rtu_ETCS_CTRL_MODE->lcetcsu8EngCtrlMode == ((uint8_T)
         TCSDpCtrlModeDrvPrtn)) {
      /* Transition: '<S1>:19' */
      /* Transition: '<S1>:21' */
      *rty_lcetcss16AddTar4CtrlMode = ((uint8_T)U8ETCSCpTarSpnAdd4DrvPrtnMd);

      /* Transition: '<S1>:24' */
    } else {
      /* Transition: '<S1>:22' */
      *rty_lcetcss16AddTar4CtrlMode = 0;
    }
  }

  /* End of Chart: '<Root>/Chart1' */
  /* Transition: '<S1>:26' */
}

/* Model initialize function */
void LCETCS_vCalTarWhlSpnIncCtrlMd_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
