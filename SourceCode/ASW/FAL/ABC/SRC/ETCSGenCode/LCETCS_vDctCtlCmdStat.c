/*
 * File: LCETCS_vDctCtlCmdStat.c
 *
 * Code generated for Simulink model 'LCETCS_vDctCtlCmdStat'.
 *
 * Model version                  : 1.197
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:51:34 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctCtlCmdStat.h"
#include "LCETCS_vDctCtlCmdStat_private.h"

/* Initial conditions for referenced model: 'LCETCS_vDctCtlCmdStat' */
void LCETCS_vDctCtlCmdStat_Init(boolean_T *rty_lcetcsu1CtlCmdHitMax, boolean_T
  *rty_lcetcsu1CtlCmdInc, DW_LCETCS_vDctCtlCmdStat_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = false;

  /* InitializeConditions for Chart: '<Root>/Detection of control command status' */
  *rty_lcetcsu1CtlCmdHitMax = false;
  *rty_lcetcsu1CtlCmdInc = false;
}

/* Output and update for referenced model: 'LCETCS_vDctCtlCmdStat' */
void LCETCS_vDctCtlCmdStat(int32_T rtu_lcetcss32CardanTrqCmd, const
  TypeETCSCrdnTrqStruct *rtu_ETCS_CRDN_TRQ, const TypeETCSCtlCmdStruct
  *rtu_ETCS_CTL_CMD_OLD, boolean_T *rty_lcetcsu1CtlCmdHitMax, boolean_T
  *rty_lcetcsu1CtlCmdInc, DW_LCETCS_vDctCtlCmdStat_f_T *localDW)
{
  /* Chart: '<Root>/Detection of control command status' incorporates:
   *  UnitDelay: '<Root>/Unit Delay1'
   */
  /* Gateway: Detection of control command status */
  /* During: Detection of control command status */
  /* Entry Internal: Detection of control command status */
  /* Transition: '<S1>:9' */
  /*  Detection of control command slope  */
  if (rtu_lcetcss32CardanTrqCmd > rtu_ETCS_CTL_CMD_OLD->lcetcss32CardanTrqCmd) {
    /* Transition: '<S1>:11' */
    /* Transition: '<S1>:12' */
    *rty_lcetcsu1CtlCmdInc = true;

    /* Transition: '<S1>:15' */
    /* Transition: '<S1>:16' */
  } else {
    /* Transition: '<S1>:10' */
    if (rtu_lcetcss32CardanTrqCmd < rtu_ETCS_CTL_CMD_OLD->lcetcss32CardanTrqCmd)
    {
      /* Transition: '<S1>:13' */
      /* Transition: '<S1>:14' */
      *rty_lcetcsu1CtlCmdInc = false;

      /* Transition: '<S1>:16' */
    } else {
      /* Transition: '<S1>:17' */
      *rty_lcetcsu1CtlCmdInc = localDW->UnitDelay1_DSTATE;
    }
  }

  /* Transition: '<S1>:18' */
  /*  Detection of control command hits max  */
  if (rtu_lcetcss32CardanTrqCmd >= rtu_ETCS_CRDN_TRQ->lcetcss32DrvReqCrdnTrq) {
    /* Transition: '<S1>:30' */
    /* Transition: '<S1>:31' */
    *rty_lcetcsu1CtlCmdHitMax = true;

    /* Transition: '<S1>:32' */
  } else {
    /* Transition: '<S1>:33' */
    *rty_lcetcsu1CtlCmdHitMax = false;
  }

  /* End of Chart: '<Root>/Detection of control command status' */

  /* Update for UnitDelay: '<Root>/Unit Delay1' */
  /* Transition: '<S1>:35' */
  localDW->UnitDelay1_DSTATE = *rty_lcetcsu1CtlCmdInc;
}

/* Model initialize function */
void LCETCS_vDctCtlCmdStat_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
