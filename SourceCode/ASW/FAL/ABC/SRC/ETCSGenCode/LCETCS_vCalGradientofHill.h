/*
 * File: LCETCS_vCalGradientofHill.h
 *
 * Code generated for Simulink model 'LCETCS_vCalGradientofHill'.
 *
 * Model version                  : 1.323
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:13:30 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalGradientofHill_h_
#define RTW_HEADER_LCETCS_vCalGradientofHill_h_
#ifndef LCETCS_vCalGradientofHill_COMMON_INCLUDES_
# define LCETCS_vCalGradientofHill_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalGradientofHill_COMMON_INCLUDES_ */

#include "LCETCS_vCalGradientofHill_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vDctInhCndGradient.h"
#include "LCETCS_vDctHoldingGradient.h"
#include "LCETCS_vCalGradientTrust.h"
#include "LCETCS_vCalGradientCnt.h"
#include "LCETCS_vCalFnlGradient.h"

/* Block states (auto storage) for model 'LCETCS_vCalGradientofHill' */
typedef struct {
  MdlrefDW_LCETCS_vDctInhCndGradient_T LCETCS_vDctInhCndGradient_DWORK1;/* '<Root>/LCETCS_vDctInhCndGradient' */
  MdlrefDW_LCETCS_vDctHoldingGradient_T LCETCS_vDctHoldingGradient_DWORK1;/* '<Root>/LCETCS_vDctHoldingGradient' */
  MdlrefDW_LCETCS_vCalGradientCnt_T LCETCS_vCalGradientCnt_DWORK1;/* '<Root>/LCETCS_vCalGradientCnt' */
  MdlrefDW_LCETCS_vCalFnlGradient_T LCETCS_vCalFnlGradient_DWORK1;/* '<Root>/LCETCS_vCalFnlGradient' */
} DW_LCETCS_vCalGradientofHill_f_T;

typedef struct {
  DW_LCETCS_vCalGradientofHill_f_T rtdw;
} MdlrefDW_LCETCS_vCalGradientofHill_T;

/* Model reference registration function */
extern void LCETCS_vCalGradientofHill_initialize(void);
extern void LCETCS_vCalGradientofHill_Init(boolean_T
  *rty_lcetcsu1InhibitGradient, boolean_T *rty_lcetcsu1DctHoldGradient,
  boolean_T *rty_lcetcsu1GradientTrstSet, DW_LCETCS_vCalGradientofHill_f_T
  *localDW);
extern void LCETCS_vCalGradientofHill_Start(DW_LCETCS_vCalGradientofHill_f_T
  *localDW);
extern void LCETCS_vCalGradientofHill(int16_T rtu_lcetcss16LongAccFilter, const
  TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC, int16_T rtu_lcetcss16VehSpd, const
  TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, int16_T *rty_lcetcss16GradientOfHill,
  int16_T *rty_lcetcss16GradientTrstCnt, boolean_T *rty_lcetcsu1InhibitGradient,
  boolean_T *rty_lcetcsu1DctHoldGradient, int16_T *rty_lcetcss16HoldGradientCnt,
  int16_T *rty_lcetcss16HoldRefGradientTime, boolean_T
  *rty_lcetcsu1GradientTrstSet, DW_LCETCS_vCalGradientofHill_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalGradientofHill'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalGradientofHill_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
