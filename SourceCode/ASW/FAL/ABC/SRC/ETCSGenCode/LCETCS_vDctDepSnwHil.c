/*
 * File: LCETCS_vDctDepSnwHil.c
 *
 * Code generated for Simulink model 'LCETCS_vDctDepSnwHil'.
 *
 * Model version                  : 1.595
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:05:33 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctDepSnwHil.h"
#include "LCETCS_vDctDepSnwHil_private.h"

/* Initial conditions for referenced model: 'LCETCS_vDctDepSnwHil' */
void LCETCS_vDctDepSnwHil_Init(B_LCETCS_vDctDepSnwHil_c_T *localB,
  DW_LCETCS_vDctDepSnwHil_f_T *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCntDepSnwHil' */
  LCETCS_vCntDepSnwHil_Init(&(localDW->LCETCS_vCntDepSnwHil_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctSetDepSnwHil' */
  LCETCS_vDctSetDepSnwHil_Init(&localB->lcetcsu1DepSnwHilAct,
    &(localDW->LCETCS_vDctSetDepSnwHil_DWORK1.rtdw));
}

/* Output and update for referenced model: 'LCETCS_vDctDepSnwHil' */
void LCETCS_vDctDepSnwHil(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const
  TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC, const TypeETCSWhlSpinStruct
  *rtu_ETCS_WHL_SPIN, const TypeETCSGearStruct *rtu_ETCS_GEAR_STRUCT, const
  TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, int16_T rtu_lcetcss16VehSpd,
  TypeETCSDepSnwStruct *rty_ETCS_DEP_SNW, B_LCETCS_vDctDepSnwHil_c_T *localB,
  DW_LCETCS_vDctDepSnwHil_f_T *localDW)
{
  /* local block i/o variables */
  int16_T rtb_lcetcss16DepSnwHilCnt;

  /* ModelReference: '<Root>/LCETCS_vCntDepSnwHil' */
  LCETCS_vCntDepSnwHil(rtu_ETCS_CTL_ACT, rtu_ETCS_VEH_ACC, rtu_ETCS_WHL_SPIN,
                       rtu_ETCS_GEAR_STRUCT, rtu_ETCS_ESC_SIG,
                       rtu_lcetcss16VehSpd, &rtb_lcetcss16DepSnwHilCnt,
                       &(localDW->LCETCS_vCntDepSnwHil_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vDctSetDepSnwHil' */
  LCETCS_vDctSetDepSnwHil(rtb_lcetcss16DepSnwHilCnt,
    &localB->lcetcsu1DepSnwHilAct,
    &(localDW->LCETCS_vDctSetDepSnwHil_DWORK1.rtdw));

  /* BusCreator: '<Root>/Bus Creator' */
  rty_ETCS_DEP_SNW->lcetcsu1DepSnwHilAct = localB->lcetcsu1DepSnwHilAct;
  rty_ETCS_DEP_SNW->lcetcss16DepSnwHilCnt = rtb_lcetcss16DepSnwHilCnt;
}

/* Model initialize function */
void LCETCS_vDctDepSnwHil_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCntDepSnwHil' */
  LCETCS_vCntDepSnwHil_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctSetDepSnwHil' */
  LCETCS_vDctSetDepSnwHil_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
