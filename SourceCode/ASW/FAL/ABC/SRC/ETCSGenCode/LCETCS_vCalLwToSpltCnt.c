/*
 * File: LCETCS_vCalLwToSpltCnt.c
 *
 * Code generated for Simulink model 'LCETCS_vCalLwToSpltCnt'.
 *
 * Model version                  : 1.215
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:56:18 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalLwToSpltCnt.h"
#include "LCETCS_vCalLwToSpltCnt_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalLwToSpltCnt' */
void LCETCS_vCalLwToSpltCnt_Init(DW_LCETCS_vCalLwToSpltCnt_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = 0;
}

/* Output and update for referenced model: 'LCETCS_vCalLwToSpltCnt' */
void LCETCS_vCalLwToSpltCnt(int16_T rtu_lcetcss16LowMuWhlSpinNF, int16_T
  rtu_lcetcss16HighMuWhlSpinNF, boolean_T rtu_lcetcsu1DctWhlOnHighmu, const
  TypeETCSAxlStruct *rtu_ETCS_FA, const TypeETCSAxlStruct *rtu_ETCS_RA, const
  TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const TypeETCSTrq4RdFricStruct
  *rtu_ETCS_TRQ_REP_RD_FRIC_OLD, const TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN,
  boolean_T rtu_lcetcsu1DctLowHm, int16_T *rty_lcetcss16LwToSplitCnt,
  DW_LCETCS_vCalLwToSpltCnt_f_T *localDW)
{
  int32_T rtb_Divide2;
  int16_T rtb_Divide1;
  int32_T tmp;

  /* If: '<Root>/If' incorporates:
   *  Constant: '<S2>/Constant'
   *  Logic: '<Root>/Logical Operator'
   */
  if (!(rtu_lcetcsu1DctWhlOnHighmu && rtu_lcetcsu1DctLowHm)) {
    /* Outputs for IfAction SubSystem: '<Root>/Non Low To Split Detect Count' incorporates:
     *  ActionPort: '<S2>/Action Port'
     */
    *rty_lcetcss16LwToSplitCnt = 0;

    /* End of Outputs for SubSystem: '<Root>/Non Low To Split Detect Count' */
  } else {
    /* Outputs for IfAction SubSystem: '<Root>/Low To Split Detect Count' incorporates:
     *  ActionPort: '<S1>/Action Port'
     */
    /* Product: '<S1>/Divide2' */
    rtb_Divide2 = rtu_ETCS_TRQ_REP_RD_FRIC_OLD->lcetcss32TrqRepRdFric / 10;

    /* Switch: '<S1>/Switch3' incorporates:
     *  Constant: '<S1>/Constant1'
     */
    if (((boolean_T)VarFwd)) {
      rtb_Divide1 = rtu_ETCS_FA->lcetcss16EstBrkTrqOnLowMuWhl;
    } else {
      rtb_Divide1 = rtu_ETCS_RA->lcetcss16EstBrkTrqOnLowMuWhl;
    }

    /* End of Switch: '<S1>/Switch3' */

    /* Product: '<S1>/Divide1' */
    rtb_Divide1 = (int16_T)(rtb_Divide1 / 10);

    /* Chart: '<S1>/Chart' incorporates:
     *  UnitDelay: '<Root>/Unit Delay1'
     */
    /* Gateway: Low To Split Detect Count/Chart */
    /* During: Low To Split Detect Count/Chart */
    /* Entry Internal: Low To Split Detect Count/Chart */
    /* Transition: '<S3>:142' */
    /* comment
       (FR_TCS.lctcsu8HghMuWhlSpnCnt < 2  */
    tmp = rtu_ETCS_TAR_SPIN->lcetcss16TarWhlSpin - ((int16_T)VREF_1_KPH);
    if (tmp > 32767) {
      tmp = 32767;
    } else {
      if (tmp < -32768) {
        tmp = -32768;
      }
    }

    if (((((rtu_lcetcss16LowMuWhlSpinNF > tmp) && (rtu_lcetcss16HighMuWhlSpinNF <
            ((uint8_T)U8TCSCpHghMuWhlSpinTh))) && (rtb_Divide2 < (((int16_T)
             S16ETCSCpCdrnTrqAtAsphalt) - ((int16_T)S16ETCSCpOffsetTrq4LwToSplt))))
         && (rtb_Divide1 < ((uint8_T)U8ETCSCpTarTransStBrkTrq2Sp))) &&
        (rtu_ETCS_ESC_SIG->lcetcss16AbsDelYawFrst <
         rtu_ETCS_TAR_SPIN->lcetcss16LmtCrngDltYaw)) {
      /* Transition: '<S3>:151' */
      /* Transition: '<S3>:153' */
      tmp = localDW->UnitDelay1_DSTATE + 1;
      if (tmp > 32767) {
        tmp = 32767;
      }

      *rty_lcetcss16LwToSplitCnt = (int16_T)tmp;

      /* Transition: '<S3>:170' */
      /* Transition: '<S3>:169' */
    } else {
      /* Transition: '<S3>:162' */
      tmp = ((uint8_T)U8TCSCpHghMuWhlSpinTh) + ((int16_T)VREF_0_5_KPH);
      if (tmp > 32767) {
        tmp = 32767;
      }

      if (((((rtu_lcetcss16LowMuWhlSpinNF <= ((int16_T)VREF_1_KPH)) ||
             (rtu_lcetcss16HighMuWhlSpinNF >= tmp)) || (rtb_Divide2 >= ((int16_T)
              S16ETCSCpCdrnTrqAtAsphalt))) || (rtb_Divide1 >= ((uint8_T)
             U8ETCSCpTarTransCplBrkTrq2Sp))) ||
          (rtu_ETCS_ESC_SIG->lcetcss16AbsDelYawFrst >=
           rtu_ETCS_TAR_SPIN->lcetcss16LmtCrngDltYaw)) {
        /* Transition: '<S3>:164' */
        /* Transition: '<S3>:166' */
        *rty_lcetcss16LwToSplitCnt = 0;

        /* Transition: '<S3>:169' */
      } else {
        /* Transition: '<S3>:168' */
        *rty_lcetcss16LwToSplitCnt = (int16_T)(localDW->UnitDelay1_DSTATE - 1);
      }
    }

    /* End of Chart: '<S1>/Chart' */
    /* End of Outputs for SubSystem: '<Root>/Low To Split Detect Count' */
    /* Transition: '<S3>:172' */
  }

  /* End of If: '<Root>/If' */

  /* Saturate: '<Root>/Saturation1' */
  if ((*rty_lcetcss16LwToSplitCnt) > ((int16_T)S16ETCSCpLwToSpltDctCplCnt)) {
    *rty_lcetcss16LwToSplitCnt = ((int16_T)S16ETCSCpLwToSpltDctCplCnt);
  } else {
    if ((*rty_lcetcss16LwToSplitCnt) < 0) {
      *rty_lcetcss16LwToSplitCnt = 0;
    }
  }

  /* End of Saturate: '<Root>/Saturation1' */

  /* Update for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = *rty_lcetcss16LwToSplitCnt;
}

/* Model initialize function */
void LCETCS_vCalLwToSpltCnt_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
