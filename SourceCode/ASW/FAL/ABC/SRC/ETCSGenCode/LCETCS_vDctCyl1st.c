/*
 * File: LCETCS_vDctCyl1st.c
 *
 * Code generated for Simulink model 'LCETCS_vDctCyl1st'.
 *
 * Model version                  : 1.176
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:05:26 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctCyl1st.h"
#include "LCETCS_vDctCyl1st_private.h"

const rtTimingBridge *LCETCS_vDctCyl1st_TimingBrdg;
const TypeETCSCyl1stStruct LCETCS_vDctCyl1st_rtZTypeETCSCyl1stStruct = {
  false,                               /* lcetcsu1Cyl1st */
  false,                               /* lcetcsu1FalEdgOfCyl1st */
  false,                               /* lcetcsu1RsgEdgOfCyl1st */
  0                                    /* lcetcss16Cyl1stTm */
} ;                                    /* TypeETCSCyl1stStruct ground */

/* Initial conditions for referenced model: 'LCETCS_vDctCyl1st' */
void LCETCS_vDctCyl1st_Init(TypeETCSCyl1stStruct *rty_ETCS_CYL_1ST,
  DW_LCETCS_vDctCyl1st_f_T *localDW, B_LCETCS_vDctCyl1st_c_T *localB)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = LCETCS_vDctCyl1st_rtZTypeETCSCyl1stStruct;

  /* InitializeConditions for Chart: '<Root>/Chart' */
  localB->lcetcsu1Cyl1st = false;

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctFallingEdge' */
  LCETCS_vDctFallingEdge_Init(&localB->lcetcsu1FalEdgOfCyl1st,
    &(localDW->LCETCS_vDctFallingEdge_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctRisingEdge' */
  LCETCS_vDctRisingEdge_Init(&localB->lcetcsu1RsgEdgOfCyl1st,
    &(localDW->LCETCS_vDctRisingEdge_DWORK1.rtdw));

  /* InitializeConditions for BusCreator: '<Root>/Bus Creator2' incorporates:
   *  InitializeConditions for Chart: '<Root>/Chart1'
   */
  if (rtmIsFirstInitCond()) {
    rty_ETCS_CYL_1ST->lcetcsu1Cyl1st = localB->lcetcsu1Cyl1st;
    rty_ETCS_CYL_1ST->lcetcsu1FalEdgOfCyl1st = localB->lcetcsu1FalEdgOfCyl1st;
    rty_ETCS_CYL_1ST->lcetcsu1RsgEdgOfCyl1st = localB->lcetcsu1RsgEdgOfCyl1st;
    rty_ETCS_CYL_1ST->lcetcss16Cyl1stTm = 0;
  }

  /* End of InitializeConditions for BusCreator: '<Root>/Bus Creator2' */
}

/* Output and update for referenced model: 'LCETCS_vDctCyl1st' */
void LCETCS_vDctCyl1st(const TypeETCSLowWhlSpnStruct *rtu_ETCS_LOW_WHL_SPN,
  const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const TypeETCSCtlErrStruct
  *rtu_ETCS_CTL_ERR, TypeETCSCyl1stStruct *rty_ETCS_CYL_1ST,
  DW_LCETCS_vDctCyl1st_f_T *localDW, B_LCETCS_vDctCyl1st_c_T *localB)
{
  int16_T lcetcss16Cyl1stTm;

  /* Chart: '<Root>/Chart' incorporates:
   *  UnitDelay: '<Root>/Unit Delay1'
   */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:96' */
  /* comment */
  if (rtu_ETCS_CTL_ACT->lcetcsu1CtlActRsgEdg == 1) {
    /* Transition: '<S1>:98' */
    /* Transition: '<S1>:103' */
    localB->lcetcsu1Cyl1st = true;

    /* Transition: '<S1>:102' */
    /* Transition: '<S1>:101' */
    /* Transition: '<S1>:108' */
  } else {
    /* Transition: '<S1>:97' */
    /*   */
    if (((rtu_ETCS_CTL_ACT->lcetcsu1CtlAct == 0) ||
         (rtu_ETCS_LOW_WHL_SPN->lcetcss16LowWhlSpnCnt >= ((uint8_T)
           U8ETCSCpLwWhlSpnTh2ClrCyl1st))) ||
        (localDW->UnitDelay1_DSTATE.lcetcss16Cyl1stTm >= ((uint8_T)
          U8ETCSCpTimeTh2ClrCyl1st))) {
      /* Transition: '<S1>:105' */
      /* Transition: '<S1>:104' */
      localB->lcetcsu1Cyl1st = false;

      /* Transition: '<S1>:101' */
      /* Transition: '<S1>:108' */
    } else {
      /* Transition: '<S1>:100' */
      if (rtu_ETCS_CTL_ERR->lcetcsu1RsgEdgOfCtlErrPos == 1) {
        /* Transition: '<S1>:106' */
        /* Transition: '<S1>:107' */
        localB->lcetcsu1Cyl1st = false;

        /* Transition: '<S1>:108' */
      } else {
        /* Transition: '<S1>:109' */
        localB->lcetcsu1Cyl1st = localDW->UnitDelay1_DSTATE.lcetcsu1Cyl1st;
      }
    }
  }

  /* End of Chart: '<Root>/Chart' */
  /* Transition: '<S1>:99' */

  /* ModelReference: '<Root>/LCETCS_vDctFallingEdge' */
  LCETCS_vDctFallingEdge(localB->lcetcsu1Cyl1st, &localB->lcetcsu1FalEdgOfCyl1st,
    &(localDW->LCETCS_vDctFallingEdge_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vDctRisingEdge' */
  LCETCS_vDctRisingEdge(localB->lcetcsu1Cyl1st, &localB->lcetcsu1RsgEdgOfCyl1st,
                        &(localDW->LCETCS_vDctRisingEdge_DWORK1.rtdw));

  /* Chart: '<Root>/Chart1' incorporates:
   *  UnitDelay: '<Root>/Unit Delay1'
   */
  /* Gateway: Chart1 */
  /* During: Chart1 */
  /* Entry Internal: Chart1 */
  /* Transition: '<S2>:8' */
  /* comment */
  if (rtu_ETCS_CTL_ACT->lcetcsu1CtlAct == 0) {
    /* Transition: '<S2>:13' */
    /* Transition: '<S2>:19' */
    lcetcss16Cyl1stTm = 0;

    /* Transition: '<S2>:10' */
    /* Transition: '<S2>:26' */
  } else {
    /* Transition: '<S2>:17' */
    if (localB->lcetcsu1Cyl1st == 1) {
      /* Transition: '<S2>:34' */
      /* Transition: '<S2>:14' */
      lcetcss16Cyl1stTm = (int16_T)(localDW->UnitDelay1_DSTATE.lcetcss16Cyl1stTm
        + 1);

      /* Transition: '<S2>:26' */
    } else {
      /* Transition: '<S2>:35' */
      lcetcss16Cyl1stTm = localDW->UnitDelay1_DSTATE.lcetcss16Cyl1stTm;
    }
  }

  /* End of Chart: '<Root>/Chart1' */

  /* BusCreator: '<Root>/Bus Creator2' */
  /* Transition: '<S2>:25' */
  rty_ETCS_CYL_1ST->lcetcsu1Cyl1st = localB->lcetcsu1Cyl1st;
  rty_ETCS_CYL_1ST->lcetcsu1FalEdgOfCyl1st = localB->lcetcsu1FalEdgOfCyl1st;
  rty_ETCS_CYL_1ST->lcetcsu1RsgEdgOfCyl1st = localB->lcetcsu1RsgEdgOfCyl1st;
  rty_ETCS_CYL_1ST->lcetcss16Cyl1stTm = lcetcss16Cyl1stTm;

  /* Update for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = *rty_ETCS_CYL_1ST;
}

/* Model initialize function */
void LCETCS_vDctCyl1st_initialize(const rtTimingBridge *timingBridge)
{
  /* Registration code */
  LCETCS_vDctCyl1st_TimingBrdg = timingBridge;

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctFallingEdge' */
  LCETCS_vDctFallingEdge_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctRisingEdge' */
  LCETCS_vDctRisingEdge_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
