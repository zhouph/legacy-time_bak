/*
 * File: LCETCS_vCntLowSpinTm.c
 *
 * Code generated for Simulink model 'LCETCS_vCntLowSpinTm'.
 *
 * Model version                  : 1.111
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:04:49 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCntLowSpinTm.h"
#include "LCETCS_vCntLowSpinTm_private.h"

const rtTimingBridge *LCETCS_vCntLowSpinTm_TimingBrdg;

/* Initial conditions for referenced model: 'LCETCS_vCntLowSpinTm' */
void LCETCS_vCntLowSpinTm_Init(TypeETCSLowWhlSpnStruct *rty_ETCS_LOW_WHL_SPN,
  B_LCETCS_vCntLowSpinTm_c_T *localB)
{
  /* InitializeConditions for Chart: '<Root>/Chart' */
  localB->lcetcsu1LowWhlSpnCntReset = false;

  /* InitializeConditions for SignalConversion: '<Root>/Signal Conversion' */
  if (rtmIsFirstInitCond()) {
    localB->lcetcsu1LowWhlSpnCntReset_j = localB->lcetcsu1LowWhlSpnCntReset;
  }

  /* End of InitializeConditions for SignalConversion: '<Root>/Signal Conversion' */

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay' */
  localB->Counter = 0;

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16CntIfSigSmlThanTh' */
  LCETCS_s16CntIfSigSmlThanTh_Init(&localB->Counter);

  /* InitializeConditions for SignalConversion: '<Root>/Signal Conversion1' */
  if (rtmIsFirstInitCond()) {
    localB->lcetcss16LowWhlSpnCnt = localB->Counter;
  }

  /* End of InitializeConditions for SignalConversion: '<Root>/Signal Conversion1' */

  /* InitializeConditions for SignalConversion: '<Root>/Signal Conversion2' incorporates:
   *  InitializeConditions for Chart: '<Root>/Chart1'
   */
  if (rtmIsFirstInitCond()) {
    localB->lcetcsu1LowWhlSpnCntInc_m = false;
  }

  /* End of InitializeConditions for SignalConversion: '<Root>/Signal Conversion2' */

  /* InitializeConditions for BusCreator: '<Root>/Bus Creator' */
  if (rtmIsFirstInitCond()) {
    rty_ETCS_LOW_WHL_SPN->lcetcsu1LowWhlSpnCntReset =
      localB->lcetcsu1LowWhlSpnCntReset_j;
    rty_ETCS_LOW_WHL_SPN->lcetcss16LowWhlSpnCnt = localB->lcetcss16LowWhlSpnCnt;
    rty_ETCS_LOW_WHL_SPN->lcetcsu1LowWhlSpnCntInc =
      localB->lcetcsu1LowWhlSpnCntInc_m;
  }

  /* End of InitializeConditions for BusCreator: '<Root>/Bus Creator' */
}

/* Start for referenced model: 'LCETCS_vCntLowSpinTm' */
void LCETCS_vCntLowSpinTm_Start(B_LCETCS_vCntLowSpinTm_c_T *localB)
{
  /* Start for Constant: '<Root>/Constant' */
  localB->CounterUpTh = ((uint8_T)U8ETCSCpLowWhlSpnCntUpTh);

  /* Start for Constant: '<Root>/Constant1' */
  localB->CounterDownTh = ((uint8_T)U8ETCSCpLowWhlSpnCntDnTh);
}

/* Output and update for referenced model: 'LCETCS_vCntLowSpinTm' */
void LCETCS_vCntLowSpinTm(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const
  TypeETCSCtlErrStruct *rtu_ETCS_CTL_ERR, const TypeETCSWhlSpinStruct
  *rtu_ETCS_WHL_SPIN, const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const
  TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN, const TypeETCSReqVCAStruct
  *rtu_ETCS_REQ_VCA, TypeETCSLowWhlSpnStruct *rty_ETCS_LOW_WHL_SPN,
  B_LCETCS_vCntLowSpinTm_c_T *localB)
{
  /* local block i/o variables */
  int16_T rtb_CounterOld;
  boolean_T lcetcsu1LowWhlSpnCntInc;

  /* Chart: '<Root>/Chart' */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:11' */
  /* comment */
  if (((((rtu_ETCS_CTL_ACT->lcetcsu1CtlAct == 0) ||
         (rtu_ETCS_CTL_ERR->lcetcsu1CtlErrNeg == 1)) ||
        (rtu_ETCS_ESC_SIG->lcetcss16AbsDelYawFrst >
         rtu_ETCS_TAR_SPIN->lcetcss16LmtCrngDltYaw)) ||
       (rtu_ETCS_REQ_VCA->lcetcsu1ReqVcaEngTrqRsgEdg == 1)) ||
      (rtu_ETCS_WHL_SPIN->lcetcss16WhlSpin >= (((uint8_T)
         U8ETCSCpLowWhlSpnCntDnTh) + ((uint8_T)U8ETCSCpLowWhlSpnCntUpTh)))) {
    /* Transition: '<S1>:10' */
    /* Transition: '<S1>:9' */
    localB->lcetcsu1LowWhlSpnCntReset = true;

    /* Transition: '<S1>:8' */
  } else {
    /* Transition: '<S1>:7' */
    localB->lcetcsu1LowWhlSpnCntReset = false;
  }

  /* End of Chart: '<Root>/Chart' */

  /* SignalConversion: '<Root>/Signal Conversion' */
  /* Transition: '<S1>:6' */
  localB->lcetcsu1LowWhlSpnCntReset_j = localB->lcetcsu1LowWhlSpnCntReset;

  /* Constant: '<Root>/Constant' */
  localB->CounterUpTh = ((uint8_T)U8ETCSCpLowWhlSpnCntUpTh);

  /* Constant: '<Root>/Constant1' */
  localB->CounterDownTh = ((uint8_T)U8ETCSCpLowWhlSpnCntDnTh);

  /* UnitDelay: '<Root>/Unit Delay' */
  rtb_CounterOld = localB->Counter;

  /* ModelReference: '<Root>/LCETCS_s16CntIfSigSmlThanTh' */
  LCETCS_s16CntIfSigSmlThanTh(localB->lcetcsu1LowWhlSpnCntReset,
    rtu_ETCS_WHL_SPIN->lcetcss16WhlSpin, localB->CounterUpTh,
    localB->CounterDownTh, rtb_CounterOld, &localB->Counter);

  /* SignalConversion: '<Root>/Signal Conversion1' */
  localB->lcetcss16LowWhlSpnCnt = localB->Counter;

  /* Chart: '<Root>/Chart1' */
  /* Gateway: Chart1 */
  /* During: Chart1 */
  /* Entry Internal: Chart1 */
  /* Transition: '<S2>:11' */
  /* comment */
  if (rtb_CounterOld < localB->Counter) {
    /* Transition: '<S2>:10' */
    /* Transition: '<S2>:9' */
    lcetcsu1LowWhlSpnCntInc = true;

    /* Transition: '<S2>:8' */
  } else {
    /* Transition: '<S2>:7' */
    lcetcsu1LowWhlSpnCntInc = false;
  }

  /* End of Chart: '<Root>/Chart1' */

  /* SignalConversion: '<Root>/Signal Conversion2' */
  /* Transition: '<S2>:6' */
  localB->lcetcsu1LowWhlSpnCntInc_m = lcetcsu1LowWhlSpnCntInc;

  /* BusCreator: '<Root>/Bus Creator' */
  rty_ETCS_LOW_WHL_SPN->lcetcsu1LowWhlSpnCntReset =
    localB->lcetcsu1LowWhlSpnCntReset_j;
  rty_ETCS_LOW_WHL_SPN->lcetcss16LowWhlSpnCnt = localB->lcetcss16LowWhlSpnCnt;
  rty_ETCS_LOW_WHL_SPN->lcetcsu1LowWhlSpnCntInc =
    localB->lcetcsu1LowWhlSpnCntInc_m;
}

/* Model initialize function */
void LCETCS_vCntLowSpinTm_initialize(const rtTimingBridge *timingBridge)
{
  /* Registration code */
  LCETCS_vCntLowSpinTm_TimingBrdg = timingBridge;

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16CntIfSigSmlThanTh' */
  LCETCS_s16CntIfSigSmlThanTh_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
