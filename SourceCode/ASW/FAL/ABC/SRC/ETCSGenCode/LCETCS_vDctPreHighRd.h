/*
 * File: LCETCS_vDctPreHighRd.h
 *
 * Code generated for Simulink model 'LCETCS_vDctPreHighRd'.
 *
 * Model version                  : 1.185
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:06:16 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vDctPreHighRd_h_
#define RTW_HEADER_LCETCS_vDctPreHighRd_h_
#ifndef LCETCS_vDctPreHighRd_COMMON_INCLUDES_
# define LCETCS_vDctPreHighRd_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vDctPreHighRd_COMMON_INCLUDES_ */

#include "LCETCS_vDctPreHighRd_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_s16Inter3Point.h"

/* Block signals for model 'LCETCS_vDctPreHighRd' */
typedef struct {
  int16_T y;                           /* '<Root>/LCETCS_s16Inter3Point' */
  int16_T lcetcss16PreHighRdDctCnt;    /* '<Root>/Chart' */
} B_LCETCS_vDctPreHighRd_c_T;

/* Block states (auto storage) for model 'LCETCS_vDctPreHighRd' */
typedef struct {
  int16_T UnitDelay1_DSTATE;           /* '<Root>/Unit Delay1' */
} DW_LCETCS_vDctPreHighRd_f_T;

typedef struct {
  B_LCETCS_vDctPreHighRd_c_T rtb;
  DW_LCETCS_vDctPreHighRd_f_T rtdw;
} MdlrefDW_LCETCS_vDctPreHighRd_T;

/* Model reference registration function */
extern void LCETCS_vDctPreHighRd_initialize(void);
extern void LCETCS_vDctPreHighRd_Init(B_LCETCS_vDctPreHighRd_c_T *localB,
  DW_LCETCS_vDctPreHighRd_f_T *localDW);
extern void LCETCS_vDctPreHighRd(const TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC,
  const TypeETCSCrdnTrqStruct *rtu_ETCS_CRDN_TRQ, int16_T rtu_lcetcss16VehSpd,
  const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const TypeETCSWhlSpinStruct
  *rtu_ETCS_WHL_SPIN, const TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN, const
  TypeETCSCtlErrStruct *rtu_ETCS_CTL_ERR, int16_T *rty_lcetcss16PreHighRdDctCnt,
  B_LCETCS_vDctPreHighRd_c_T *localB, DW_LCETCS_vDctPreHighRd_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vDctPreHighRd'
 * '<S1>'   : 'LCETCS_vDctPreHighRd/Chart'
 */
#endif                                 /* RTW_HEADER_LCETCS_vDctPreHighRd_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
