/*
 * File: LCETCS_vCalTarWhlSpnIncSpHill.c
 *
 * Code generated for Simulink model 'LCETCS_vCalTarWhlSpnIncSpHill'.
 *
 * Model version                  : 1.133
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:00:20 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalTarWhlSpnIncSpHill.h"
#include "LCETCS_vCalTarWhlSpnIncSpHill_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalTarWhlSpnIncSpHill' */
void LCETCS_vCalTarWhlSpnIncSpHill_Init(B_LCETCS_vCalTarWhlSpnIncSpHill_c_T
  *localB)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_Init(&localB->y);
}

/* Start for referenced model: 'LCETCS_vCalTarWhlSpnIncSpHill' */
void LCETCS_vCalTarWhlSpnIncSpHill_Start(B_LCETCS_vCalTarWhlSpnIncSpHill_c_T
  *localB)
{
  /* Start for Constant: '<Root>/Constant10' */
  localB->Constant10 = ((uint8_T)U8ETCSCpTarSpnAdd4Hill);
}

/* Output and update for referenced model: 'LCETCS_vCalTarWhlSpnIncSpHill' */
void LCETCS_vCalTarWhlSpnIncSpHill(const TypeETCSSplitHillStruct
  *rtu_ETCS_SPLIT_HILL_OLD, uint8_T *rty_lcetcsu8AddTar4SpHill,
  B_LCETCS_vCalTarWhlSpnIncSpHill_c_T *localB)
{
  /* local block i/o variables */
  int16_T rtb_x1;
  int16_T rtb_x2;
  int16_T rtb_y1;

  /* Constant: '<Root>/Constant10' */
  localB->Constant10 = ((uint8_T)U8ETCSCpTarSpnAdd4Hill);

  /* DataTypeConversion: '<Root>/Data Type Conversion' incorporates:
   *  Constant: '<Root>/Constant8'
   */
  rtb_x1 = ((uint8_T)U8ETCSCpHillDctTime);

  /* DataTypeConversion: '<Root>/Data Type Conversion1' incorporates:
   *  Constant: '<Root>/Constant4'
   */
  rtb_x2 = ((uint8_T)U8ETCSCpHsaHillTime);

  /* DataTypeConversion: '<Root>/Data Type Conversion2' incorporates:
   *  Constant: '<Root>/Constant5'
   */
  rtb_y1 = 0;

  /* ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point(rtu_ETCS_SPLIT_HILL_OLD->lcetcss16SplitHillDctCnt,
                        rtb_x1, rtb_x2, rtb_y1, localB->Constant10, &localB->y);

  /* DataTypeConversion: '<Root>/Data Type Conversion4' */
  *rty_lcetcsu8AddTar4SpHill = (uint8_T)localB->y;
}

/* Model initialize function */
void LCETCS_vCalTarWhlSpnIncSpHill_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
