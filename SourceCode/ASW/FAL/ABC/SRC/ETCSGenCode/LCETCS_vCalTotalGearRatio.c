/*
 * File: LCETCS_vCalTotalGearRatio.c
 *
 * Code generated for Simulink model 'LCETCS_vCalTotalGearRatio'.
 *
 * Model version                  : 1.90
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:06:57 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalTotalGearRatio.h"
#include "LCETCS_vCalTotalGearRatio_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalTotalGearRatio' */
void LCETCS_vCalTotalGearRatio_Init(B_LCETCS_vCalTotalGearRatio_c_T *localB,
  DW_LCETCS_vCalTotalGearRatio_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = 0;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localB->lcetcss16ChngRateLmtdSig = 0;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay' */
  localB->lcetcss16TranTmeCnt = 0;

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16ChngRateLmt' */
  LCETCS_s16ChngRateLmt_Init(&localB->lcetcss16ChngRateLmtdSig,
    &localB->lcetcss16TranTmeCnt, &(localDW->LCETCS_s16ChngRateLmt_DWORK1.rtb));
}

/* Output and update for referenced model: 'LCETCS_vCalTotalGearRatio' */
void LCETCS_vCalTotalGearRatio(int16_T rtu_lcetcss16TotalGearRatioRaw, uint8_T
  rtu_lcetcsu8GrTransTime, int16_T *rty_lcetcss16TotalGearRatio,
  B_LCETCS_vCalTotalGearRatio_c_T *localB, DW_LCETCS_vCalTotalGearRatio_f_T
  *localDW)
{
  /* local block i/o variables */
  int16_T rtb_lcetcss16TranTmeCntOld;
  int16_T rtb_lcetcss16TranTme;
  int16_T rtb_lcetcss16TrgtSigValOld;
  int16_T rtb_lcetcss16ChngRateLmtdSigOld;

  /* DataTypeConversion: '<Root>/Data Type Conversion' */
  rtb_lcetcss16TranTme = rtu_lcetcsu8GrTransTime;

  /* UnitDelay: '<Root>/Unit Delay2' */
  rtb_lcetcss16TrgtSigValOld = localDW->UnitDelay2_DSTATE;

  /* UnitDelay: '<Root>/Unit Delay1' */
  rtb_lcetcss16ChngRateLmtdSigOld = localB->lcetcss16ChngRateLmtdSig;

  /* UnitDelay: '<Root>/Unit Delay' */
  rtb_lcetcss16TranTmeCntOld = localB->lcetcss16TranTmeCnt;

  /* ModelReference: '<Root>/LCETCS_s16ChngRateLmt' */
  LCETCS_s16ChngRateLmt(rtu_lcetcss16TotalGearRatioRaw,
                        rtb_lcetcss16TrgtSigValOld, rtb_lcetcss16TranTme,
                        rtb_lcetcss16ChngRateLmtdSigOld,
                        rtb_lcetcss16TranTmeCntOld,
                        &localB->lcetcss16ChngRateLmtdSig,
                        &localB->lcetcss16TranTmeCnt,
                        &(localDW->LCETCS_s16ChngRateLmt_DWORK1.rtb));

  /* Saturate: '<Root>/Saturation' */
  if (localB->lcetcss16ChngRateLmtdSig > 10000) {
    *rty_lcetcss16TotalGearRatio = 10000;
  } else if (localB->lcetcss16ChngRateLmtdSig < 0) {
    *rty_lcetcss16TotalGearRatio = 0;
  } else {
    *rty_lcetcss16TotalGearRatio = localB->lcetcss16ChngRateLmtdSig;
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Update for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = rtu_lcetcss16TotalGearRatioRaw;
}

/* Model initialize function */
void LCETCS_vCalTotalGearRatio_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16ChngRateLmt' */
  LCETCS_s16ChngRateLmt_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
