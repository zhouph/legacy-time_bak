/*
 * File: LCETCS_vInterfaceInpSig.h
 *
 * Code generated for Simulink model 'LCETCS_vInterfaceInpSig'.
 *
 * Model version                  : 1.340
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:58:31 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vInterfaceInpSig_h_
#define RTW_HEADER_LCETCS_vInterfaceInpSig_h_
#ifndef LCETCS_vInterfaceInpSig_COMMON_INCLUDES_
# define LCETCS_vInterfaceInpSig_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vInterfaceInpSig_COMMON_INCLUDES_ */

#include "LCETCS_vInterfaceInpSig_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Model reference registration function */
extern void LCETCS_vInterfaceInpSig_initialize(void);
extern void LCETCS_vInterfaceInpSig_CopyAxlStruct_FA(const TCS_AXLE_STRUCT_SIM
  *rtu_BTCSAxlStruct, TypeETCSAxlStruct *rty_ETCSAxlStruct);
extern void LCETCS_vInterfaceInpSig_CopyCircuitStruct_PC(const
  TCS_CIRCUIT_STRUCT_SIM *rtu_BTCSCirStruct, TypeETCSCirStruct
  *rty_ETCSCirStruct);
extern void LCETCS_vInterfaceInpSig_CopyWhlStruct_FL(const W_STRUCT_SIM
  *rtu_ABSWhlStruct, const TCS_WL_STRUCT_SIM *rtu_BTCSWhlStruct, const
  RTA_WL_STRUCT_SIM *rtu_RTAWhlStruct, TypeETCSWhlStruct *rty_ETCSWhlStruct);
extern void LCETCS_vInterfaceInpSig(const W_STRUCT_SIM *rtu_FL, const
  W_STRUCT_SIM *rtu_FR, const W_STRUCT_SIM *rtu_RL, const W_STRUCT_SIM *rtu_RR,
  const TCS_WL_STRUCT_SIM *rtu_FL_TCS, const TCS_WL_STRUCT_SIM *rtu_FR_TCS,
  const TCS_WL_STRUCT_SIM *rtu_RL_TCS, const TCS_WL_STRUCT_SIM *rtu_RR_TCS,
  const TCS_AXLE_STRUCT_SIM *rtu_FA_TCS, const TCS_AXLE_STRUCT_SIM *rtu_RA_TCS,
  int16_T rtu_lctcss16VehSpd4TCS, int8_T rtu_gear_pos, uint8_T
  rtu_lctcsu1GearShiftFlag, int16_T rtu_gs_sel, int16_T rtu_drive_torq, int16_T
  rtu_eng_torq, uint16_T rtu_eng_rpm, int16_T rtu_mtp, const TCS_FLAG_t
  *rtu_lcTcsCtrlFlg0, const U8_BIT_STRUCT_t *rtu_VDCF7, const TCS_FLAG_t
  *rtu_lcTcsCtrlFlg1, const U8_BIT_STRUCT_t *rtu_VF15, boolean_T
  rtu_lctcsu1DctBTCHillByAx, int16_T rtu_wstr, int16_T rtu_wstr_dot, int16_T
  rtu_yaw_out, int16_T rtu_alat, int16_T rtu_ax_Filter, int16_T
  rtu_delta_yaw_first, int16_T rtu_rf, int16_T rtu_nor_alat, int16_T
  rtu_det_alatc, int16_T rtu_det_alatm, const U8_BIT_STRUCT_t *rtu_VDCF0,
  int16_T rtu_det_wstr_dot, int16_T rtu_ltcss16BaseTarWhlSpinDiff, uint8_T
  rtu_lespu1AWD_LOW_ACT, uint16_T rtu_lespu16AWD_4WD_TQC_CUR, const
  TCS_CIRCUIT_STRUCT_SIM *rtu_PC_TCS, const TCS_CIRCUIT_STRUCT_SIM *rtu_SC_TCS,
  const U8_BIT_STRUCT_t *rtu_VDCF1, const RTA_WL_STRUCT_SIM *rtu_RTA_FL, const
  RTA_WL_STRUCT_SIM *rtu_RTA_FR, const RTA_WL_STRUCT_SIM *rtu_RTA_RL, const
  RTA_WL_STRUCT_SIM *rtu_RTA_RR, int16_T rtu_rta_ratio_temp, const
  U8_BIT_STRUCT_t *rtu_HSAF0, uint8_T rtu_lcu8SccMode, const Abc_Ctrl_HdrBusType
  *rtu_Abc_CtrlBus, uint8_T rtu_lctcsu1BTCSVehAtv, TypeETCSWhlStruct
  *rty_ETCS_FL, TypeETCSWhlStruct *rty_ETCS_FR, TypeETCSWhlStruct *rty_ETCS_RL,
  TypeETCSWhlStruct *rty_ETCS_RR, int16_T *rty_lcetcss16VehSpd,
  TypeETCSGearStruct *rty_ETCS_GEAR_STRUCT, TypeETCSEngStruct
  *rty_ETCS_ENG_STRUCT, TypeETCSExtDctStruct *rty_ETCS_EXT_DCT,
  TypeETCSAxlStruct *rty_ETCS_FA, TypeETCSAxlStruct *rty_ETCS_RA,
  TypeETCSEscSigStruct *rty_ETCS_ESC_SIG, TypeETCS4WDSigStruct *rty_ETCS_4WD_SIG,
  TypeETCSCirStruct *rty_ETCS_PC, TypeETCSCirStruct *rty_ETCS_SC,
  TypeETCSBrkSigStruct *rty_ETCS_BRK_SIG);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vInterfaceInpSig'
 * '<S1>'   : 'LCETCS_vInterfaceInpSig/CopyAxlStruct_FA'
 * '<S2>'   : 'LCETCS_vInterfaceInpSig/CopyAxlStruct_RA'
 * '<S3>'   : 'LCETCS_vInterfaceInpSig/CopyCircuitStruct_PC'
 * '<S4>'   : 'LCETCS_vInterfaceInpSig/CopyCircuitStruct_SC'
 * '<S5>'   : 'LCETCS_vInterfaceInpSig/CopyWhlStruct_FL'
 * '<S6>'   : 'LCETCS_vInterfaceInpSig/CopyWhlStruct_FR'
 * '<S7>'   : 'LCETCS_vInterfaceInpSig/CopyWhlStruct_RL'
 * '<S8>'   : 'LCETCS_vInterfaceInpSig/CopyWhlStruct_RR'
 */
#endif                                 /* RTW_HEADER_LCETCS_vInterfaceInpSig_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
