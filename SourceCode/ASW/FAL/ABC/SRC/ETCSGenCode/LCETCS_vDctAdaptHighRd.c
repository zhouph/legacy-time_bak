/*
 * File: LCETCS_vDctAdaptHighRd.c
 *
 * Code generated for Simulink model 'LCETCS_vDctAdaptHighRd'.
 *
 * Model version                  : 1.139
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:55:55 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctAdaptHighRd.h"
#include "LCETCS_vDctAdaptHighRd_private.h"

/* Initial conditions for referenced model: 'LCETCS_vDctAdaptHighRd' */
void LCETCS_vDctAdaptHighRd_Init(DW_LCETCS_vDctAdaptHighRd_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay4' */
  localDW->UnitDelay4_DSTATE = 0;
}

/* Output and update for referenced model: 'LCETCS_vDctAdaptHighRd' */
void LCETCS_vDctAdaptHighRd(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const
  TypeETCSCtlErrStruct *rtu_ETCS_CTL_ERR, const TypeETCSTrq4RdFricStruct
  *rtu_ETCS_TRQ_REP_RD_FRIC_OLD, const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG,
  int16_T *rty_lcetcss16AdaptHighRdCnt, DW_LCETCS_vDctAdaptHighRd_f_T *localDW)
{
  int32_T tmp;

  /* Chart: '<Root>/Chart' incorporates:
   *  Product: '<Root>/Divide2'
   *  UnitDelay: '<Root>/Unit Delay4'
   */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:9' */
  /* comment */
  if (rtu_ETCS_CTL_ACT->lcetcsu1CtlAct == 1) {
    /* Transition: '<S1>:10' */
    /* Transition: '<S1>:40' */
    if (((rtu_ETCS_TRQ_REP_RD_FRIC_OLD->lcetcss32TrqRepRdFric / 10) >= ((int16_T)
          S16ETCSCpStrtCdrnTrqAtAsphalt)) &&
        (rtu_ETCS_CTL_ERR->lcetcsu1CtlErrPos == 1)) {
      /* Transition: '<S1>:70' */
      /* Transition: '<S1>:75' */
      if (rtu_ETCS_ESC_SIG->lcetcsu1BrkPdlPrsed == 1) {
        /* Transition: '<S1>:86' */
        /* Transition: '<S1>:90' */
        /* Transition: '<S1>:89' */
      } else {
        /* Transition: '<S1>:87' */
        tmp = localDW->UnitDelay4_DSTATE + 1;
        if (tmp > 32767) {
          tmp = 32767;
        }

        localDW->UnitDelay4_DSTATE = (int16_T)tmp;
      }

      /* Transition: '<S1>:77' */
    } else {
      /* Transition: '<S1>:78' */
      tmp = localDW->UnitDelay4_DSTATE - 1;
      if (tmp < -32768) {
        tmp = -32768;
      }

      localDW->UnitDelay4_DSTATE = (int16_T)tmp;
    }

    /* Transition: '<S1>:80' */
  } else {
    /* Transition: '<S1>:79' */
    localDW->UnitDelay4_DSTATE = 0;
  }

  /* End of Chart: '<Root>/Chart' */

  /* Saturate: '<Root>/Saturation' */
  /* Transition: '<S1>:68' */
  if (localDW->UnitDelay4_DSTATE > ((uint8_T)U8ETCSCpAdaptHighRdCplCnt)) {
    *rty_lcetcss16AdaptHighRdCnt = ((uint8_T)U8ETCSCpAdaptHighRdCplCnt);
  } else if (localDW->UnitDelay4_DSTATE < 0) {
    *rty_lcetcss16AdaptHighRdCnt = 0;
  } else {
    *rty_lcetcss16AdaptHighRdCnt = localDW->UnitDelay4_DSTATE;
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Update for UnitDelay: '<Root>/Unit Delay4' */
  localDW->UnitDelay4_DSTATE = *rty_lcetcss16AdaptHighRdCnt;
}

/* Model initialize function */
void LCETCS_vDctAdaptHighRd_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
