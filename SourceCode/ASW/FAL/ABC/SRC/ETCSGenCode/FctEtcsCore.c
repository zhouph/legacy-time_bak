/*
 * File: FctEtcsCore.c
 *
 * Code generated for Simulink model 'FctEtcsCore'.
 *
 * Model version                  : 1.483
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:20:06 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "FctEtcsCore.h"
#include "FctEtcsCore_private.h"

const TypeETCSCtlCmdStruct FctEtcsCore_rtZTypeETCSCtlCmdStruct = {
  0,                                   /* lcetcss32PCtlPor */
  0,                                   /* lcetcss32ICtlPor */
  0,                                   /* lcetcss32AdptByDelYaw */
  0,                                   /* lcetcss32CtlCmdMax */
  0,                                   /* lcetcss32CardanTrqCmd */
  false,                               /* lcetcsu1CtlCmdHitMax */
  false,                               /* lcetcsu1CtlCmdInc */
  0,                                   /* lcetcss32AdptByWhlSpn */
  0                                    /* lcetcss32AdptAmt */
} ;                                    /* TypeETCSCtlCmdStruct ground */

const TypeETCSEngCmdStruct FctEtcsCore_rtZTypeETCSEngCmdStruct = {
  0,                                   /* lcetcss16EngTrqCmd */
  0,                                   /* lcetcss16EngTrqCmdPPor */
  0                                    /* lcetcss16EngTrqCmdIPor */
} ;                                    /* TypeETCSEngCmdStruct ground */

const TypeETCSCtlActStruct FctEtcsCore_rtZTypeETCSCtlActStruct = {
  false,                               /* lcetcsu1CtlAct */
  false,                               /* lcetcsu1CtlActRsgEdg */
  0,                                   /* lcetcss16OvrWhlSpnTm */
  0,                                   /* lcetcss16OvrWhlSpnTmTh */
  0,                                   /* lcetcss16OvrEngTrqCmdTm */
  0,                                   /* lcetcss16LmtCnrgCtlEntrTm */
  false,                               /* lcetcsu1DriverAccelIntend */
  false                                /* lcetcsu1DriverAccelHysIntend */
} ;                                    /* TypeETCSCtlActStruct ground */

const TypeETCSDepSnwStruct FctEtcsCore_rtZTypeETCSDepSnwStruct = {
  false,                               /* lcetcsu1DepSnwHilAct */
  0                                    /* lcetcss16DepSnwHilCnt */
} ;                                    /* TypeETCSDepSnwStruct ground */

const TypeETCSBrkCtlCmdStruct FctEtcsCore_rtZTypeETCSBrkCtlCmdStruct = {
  0,                                   /* lcetcss16SymBrkTrqCtlCmdFA */
  0,                                   /* lcetcss16SymBrkTrqCtlCmdRA */
  0                                    /* lcetcss16MaxLmtSymBrkTrq */
} ;                                    /* TypeETCSBrkCtlCmdStruct ground */

const TypeETCSBrkCtlReqStruct FctEtcsCore_rtZTypeETCSBrkCtlReqStruct = {
  false,                               /* lcetcsu1SymBrkTrqCtlReqFA */
  false                                /* lcetcsu1SymBrkTrqCtlReqRA */
} ;                                    /* TypeETCSBrkCtlReqStruct ground */

const TypeETCSSplitHillStruct FctEtcsCore_rtZTypeETCSSplitHillStruct = {
  false,                               /* lcetcsu1SplitHillDct */
  false,                               /* lcetcsu1HillDctCompbyAx */
  0,                                   /* lcetcss16LongAccFilter */
  false,                               /* lcetcsu1HillbyHsaAtv */
  false,                               /* lcetcsu1SplitHillDctCndSet */
  false,                               /* lcetcsu1SplitHillClrCndSet */
  false,                               /* lcetcsu1SplitHillClrDecCndSet */
  0,                                   /* lcetcss16SplitHillDctCnt */
  0,                                   /* lcetcss16DistVehMovRd */
  false,                               /* lcetcsu1HillbyHsaSustain */
  0U,                                  /* lcetcsu8HillDctCntbyAx */
  0,                                   /* lcetcss16GradientTrstCnt */
  0,                                   /* lcetcss16GradientOfHill */
  false,                               /* lcetcsu1InhibitGradient */
  false,                               /* lcetcsu1DctHoldGradient */
  0,                                   /* lcetcss16HoldGradientCnt */
  0,                                   /* lcetcss16HoldRefGradientTime */
  false                                /* lcetcsu1GradientTrstSet */
} ;                                    /* TypeETCSSplitHillStruct ground */

const TypeETCSHighDctStruct FctEtcsCore_rtZTypeETCSHighDctStruct = {
  0,                                   /* lcetcss16LwToHiCnt */
  0,                                   /* lcetcss16LwToHiCntbyArea */
  0,                                   /* lcetcss16AdaptHighRdCnt */
  0                                    /* lcetcss16PreHighRdDctCnt */
} ;                                    /* TypeETCSHighDctStruct ground */

const TypeETCSEngStallStruct FctEtcsCore_rtZTypeETCSEngStallStruct = {
  false,                               /* lcetcsu1EngStall */
  false,                               /* lcetcsu1EngStallTemp */
  0,                                   /* lcetcss16EngStallThr */
  0,                                   /* lcetcss8EngStallRiskIndex */
  0                                    /* lcetcss16EngSpdSlope */
} ;                                    /* TypeETCSEngStallStruct ground */

const TypeETCSLwToSpltStruct FctEtcsCore_rtZTypeETCSLwToSpltStruct = {
  0,                                   /* lcetcss16LwToSplitCnt */
  0,                                   /* lcetcss16LowMuWhlSpinNF */
  0,                                   /* lcetcss16HighMuWhlSpinNF */
  false,                               /* lcetcsu1DctWhlOnHighmu */
  false,                               /* lcetcsu1DctLowHm */
  0                                    /* lcetcss16LowHmHldCnt */
} ;                                    /* TypeETCSLwToSpltStruct ground */

const TypeETCSTrq4RdFricStruct FctEtcsCore_rtZTypeETCSTrq4RdFricStruct = {
  0,                                   /* lcetcss32TrqRepRdFric */
  0,                                   /* lcetcss32AppTrq4RdFric */
  0,                                   /* lcetcss32LesTrq4RdFric */
  0                                    /* lcetcss32TrqAtLowSpnDcted */
} ;                                    /* TypeETCSTrq4RdFricStruct ground */

/* Initial conditions for referenced model: 'FctEtcsCore' */
void FctEtcsCore_Init(TypeETCSCtrlModeStruct *rty_ETCS_CTRL_MODE,
                      TypeETCSCyl1stStruct *rty_ETCS_CYL_1ST,
                      TypeETCSErrZroCrsStruct *rty_ETCS_ERR_ZRO_CRS,
                      TypeETCSLowWhlSpnStruct *rty_ETCS_LOW_WHL_SPN,
                      TypeETCSH2LStruct *rty_ETCS_H2L,
                      TypeETCSGainGearShftStruct *rty_ETCS_GAIN_GEAR_SHFT,
                      TypeTMCReqGearStruct *rty_TMC_REQ_GEAR_STRUCT, boolean_T
                      *rty_lcetcsu1FuncLampOn, DW_FctEtcsCore_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay7' */
  localDW->UnitDelay7_DSTATE = FctEtcsCore_rtZTypeETCSCtlActStruct;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = FctEtcsCore_rtZTypeETCSDepSnwStruct;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay3' */
  localDW->UnitDelay3_DSTATE = FctEtcsCore_rtZTypeETCSCtlCmdStruct;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay8' */
  localDW->UnitDelay8_DSTATE = false;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay9' */
  localDW->UnitDelay9_DSTATE = FctEtcsCore_rtZTypeETCSBrkCtlCmdStruct;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay10' */
  localDW->UnitDelay10_DSTATE = FctEtcsCore_rtZTypeETCSBrkCtlReqStruct;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay15' */
  localDW->UnitDelay15_DSTATE = FctEtcsCore_rtZTypeETCSSplitHillStruct;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay16' */
  localDW->UnitDelay16_DSTATE = FctEtcsCore_rtZTypeETCSHighDctStruct;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay11' */
  localDW->UnitDelay11_DSTATE = FctEtcsCore_rtZTypeETCSEngStallStruct;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay6' */
  localDW->UnitDelay6_DSTATE = FctEtcsCore_rtZTypeETCSLwToSpltStruct;

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCallCalculation' */
  LCETCS_vCallCalculation_Init(rty_ETCS_CTRL_MODE,
    &(localDW->LCETCS_vCallCalculation_DWORK1.rtdw));

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = FctEtcsCore_rtZTypeETCSBrkCtlCmdStruct;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay4' */
  localDW->UnitDelay4_DSTATE = FctEtcsCore_rtZTypeETCSEngCmdStruct;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay5' */
  localDW->UnitDelay5_DSTATE = FctEtcsCore_rtZTypeETCSCtlCmdStruct;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay13' */
  localDW->UnitDelay13_DSTATE = FctEtcsCore_rtZTypeETCSCtlActStruct;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay14' */
  localDW->UnitDelay14_DSTATE = FctEtcsCore_rtZTypeETCSSplitHillStruct;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay17' */
  localDW->UnitDelay17_DSTATE = FctEtcsCore_rtZTypeETCSTrq4RdFricStruct;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay12' */
  localDW->UnitDelay12_DSTATE = FctEtcsCore_rtZTypeETCSEngStallStruct;

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCallDetection' */
  LCETCS_vCallDetection_Init(rty_ETCS_CYL_1ST, rty_ETCS_ERR_ZRO_CRS,
    rty_ETCS_LOW_WHL_SPN, rty_ETCS_GAIN_GEAR_SHFT, rty_ETCS_H2L,
    &(localDW->LCETCS_vCallDetection_DWORK1.rtdw));

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = FctEtcsCore_rtZTypeETCSCtlCmdStruct;

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCallControl' */
  LCETCS_vCallControl_Init(rty_TMC_REQ_GEAR_STRUCT, rty_lcetcsu1FuncLampOn,
    &(localDW->LCETCS_vCallControl_DWORK1.rtdw));
}

/* Disable for referenced model: 'FctEtcsCore' */
void FctEtcsCore_Disable(DW_FctEtcsCore_f_T *localDW)
{
  /* Disable for ModelReference: '<Root>/LCETCS_vCallDetection' */
  LCETCS_vCallDetection_Disable(&(localDW->LCETCS_vCallDetection_DWORK1.rtdw));
}

/* Start for referenced model: 'FctEtcsCore' */
void FctEtcsCore_Start(DW_FctEtcsCore_f_T *localDW)
{
  /* Start for ModelReference: '<Root>/LCETCS_vCallCalculation' */
  LCETCS_vCallCalculation_Start(&(localDW->LCETCS_vCallCalculation_DWORK1.rtdw));

  /* Start for ModelReference: '<Root>/LCETCS_vCallDetection' */
  LCETCS_vCallDetection_Start(&(localDW->LCETCS_vCallDetection_DWORK1.rtdw));

  /* Start for ModelReference: '<Root>/LCETCS_vCallControl' */
  LCETCS_vCallControl_Start(&(localDW->LCETCS_vCallControl_DWORK1.rtdw));
}

/* Output and update for referenced model: 'FctEtcsCore' */
void FctEtcsCore(const TypeETCSWhlStruct *rtu_ETCS_FL, const TypeETCSWhlStruct
                 *rtu_ETCS_FR, const TypeETCSWhlStruct *rtu_ETCS_RL, const
                 TypeETCSWhlStruct *rtu_ETCS_RR, int16_T rtu_lcetcss16VehSpd,
                 const TypeETCSGearStruct *rtu_ETCS_GEAR_STRUCT, const
                 TypeETCSEngStruct *rtu_ETCS_ENG_STRUCT, const
                 TypeETCSExtDctStruct *rtu_ETCS_EXT_DCT, const TypeETCSAxlStruct
                 *rtu_ETCS_FA, const TypeETCSAxlStruct *rtu_ETCS_RA, const
                 TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const
                 TypeETCS4WDSigStruct *rtu_ETCS_4WD_SIG, const TypeETCSCirStruct
                 *rtu_ETCS_PC, const TypeETCSCirStruct *rtu_ETCS_SC, const
                 TypeETCSBrkSigStruct *rtu_ETCS_BRK_SIG, TypeETCSCtlErrStruct
                 *rty_ETCS_CTL_ERR, TypeETCSTarSpinStruct *rty_ETCS_TAR_SPIN,
                 TypeETCSDrvMdlStruct *rty_ETCS_DRV_MDL, TypeETCSCrdnTrqStruct
                 *rty_ETCS_CRDN_TRQ, TypeETCSVehAccStruct *rty_ETCS_VEH_ACC,
                 TypeETCSEngMdlStruct *rty_ETCS_ENG_MDL, TypeETCSLdTrqStruct
                 *rty_ETCS_LD_TRQ, TypeETCSAxlAccStruct *rty_ETCS_AXL_ACC,
                 int16_T *rty_lcetcss16SymBrkCtlStrtTh, TypeETCSWhlSpinStruct
                 *rty_ETCS_WHL_SPIN, TypeETCSStrStatStruct *rty_ETCS_STR_STAT,
                 TypeETCSCtrlModeStruct *rty_ETCS_CTRL_MODE,
                 TypeETCSCtlActStruct *rty_ETCS_CTL_ACT, TypeETCSCyl1stStruct
                 *rty_ETCS_CYL_1ST, TypeETCSErrZroCrsStruct
                 *rty_ETCS_ERR_ZRO_CRS, TypeETCSIValRstStruct *rty_ETCS_IVAL_RST,
                 TypeETCSRefTrqStrCtlStruct *rty_ETCS_REF_TRQ_STR_CTL,
                 TypeETCSRefTrqStr2CylStruct *rty_ETCS_REF_TRQ_STR2CYL,
                 TypeETCSRefTrq2JmpStruct *rty_ETCS_REF_TRQ2JMP,
                 TypeETCSLowWhlSpnStruct *rty_ETCS_LOW_WHL_SPN,
                 TypeETCSDepSnwStruct *rty_ETCS_DEP_SNW, TypeETCSH2LStruct
                 *rty_ETCS_H2L, TypeETCSGainGearShftStruct
                 *rty_ETCS_GAIN_GEAR_SHFT, TypeETCSBrkCtlReqStruct
                 *rty_ETCS_BRK_CTL_REQ, boolean_T *rty_lcetcsu1VehVibDct,
                 TypeETCSEngStallStruct *rty_ETCS_ENG_STALL, TypeETCSGainStruct *
                 rty_ETCS_CTL_GAINS, TypeETCSTrq4RdFricStruct
                 *rty_ETCS_TRQ_REP_RD_FRIC, TypeTMCReqGearStruct
                 *rty_TMC_REQ_GEAR_STRUCT, TypeETCSBrkCtlCmdStruct
                 *rty_ETCS_BRK_CTL_CMD, TypeETCSCtlCmdStruct *rty_ETCS_CTL_CMD,
                 TypeETCSSplitHillStruct *rty_ETCS_SPLIT_HILL, boolean_T
                 *rty_lcetcsu1FuncLampOn, TypeETCSWhlAccStruct *rty_ETCS_WHL_ACC,
                 TypeETCSHighDctStruct *rty_ETCS_HI_DCT, TypeETCSReqVCAStruct
                 *rty_ETCS_REQ_VCA, TypeETCSLwToSpltStruct *rty_ETCS_L2SPLT,
                 TypeETCSEngCmdStruct *rty_ETCS_ENG_CMD, TypeETCSAftLmtCrng
                 *rty_ETCS_AFT_TURN, DW_FctEtcsCore_f_T *localDW)
{
  /* local block i/o variables */
  TypeETCSCtlCmdStruct rtb_ETCS_CTL_CMD_OLD;
  TypeETCSCtlCmdStruct rtb_ETCS_CTL_CMD_OLD_l;
  TypeETCSCtlCmdStruct rtb_ETCS_CTL_CMD_OLD_k;
  TypeETCSSplitHillStruct rtb_ETCS_SPLIT_HILL_OLD;
  TypeETCSSplitHillStruct rtb_ETCS_SPLIT_HILL_OLD_e;
  TypeETCSCtlActStruct rtb_ETCS_CTL_ACT_OLD;
  TypeETCSCtlActStruct rtb_ETCS_CTL_ACT_OLD_f;
  TypeETCSLwToSpltStruct rtb_ETCS_L2SPLT;
  TypeETCSTrq4RdFricStruct rtb_ETCS_TRQ_REP_RD_FRIC_OLD;
  TypeETCSEngCmdStruct rtb_ETCS_ENG_CMD_OLD;
  TypeETCSDepSnwStruct rtb_ETCS_DEP_SNW;
  TypeETCSBrkCtlCmdStruct rtb_ETCS_BRK_CTL_CMD_OLD;
  TypeETCSBrkCtlCmdStruct rtb_ETCS_BRK_CTL_CMD_OLD_l;
  TypeETCSBrkCtlReqStruct rtb_ETCS_BRK_CTL_REQ_OLD;
  TypeETCSHighDctStruct rtb_ETCS_HI_DCT_OLD;
  TypeETCSEngStallStruct rtb_ETCS_ENG_STALL_OLD;
  boolean_T rtb_lcetcsu1VehVibDctOld;

  /* UnitDelay: '<Root>/Unit Delay7' */
  rtb_ETCS_CTL_ACT_OLD_f = localDW->UnitDelay7_DSTATE;

  /* UnitDelay: '<Root>/Unit Delay2' */
  rtb_ETCS_DEP_SNW = localDW->UnitDelay2_DSTATE;

  /* UnitDelay: '<Root>/Unit Delay3' */
  rtb_ETCS_CTL_CMD_OLD_k = localDW->UnitDelay3_DSTATE;

  /* UnitDelay: '<Root>/Unit Delay8' */
  rtb_lcetcsu1VehVibDctOld = localDW->UnitDelay8_DSTATE;

  /* UnitDelay: '<Root>/Unit Delay9' */
  rtb_ETCS_BRK_CTL_CMD_OLD = localDW->UnitDelay9_DSTATE;

  /* UnitDelay: '<Root>/Unit Delay10' */
  rtb_ETCS_BRK_CTL_REQ_OLD = localDW->UnitDelay10_DSTATE;

  /* UnitDelay: '<Root>/Unit Delay15' */
  rtb_ETCS_SPLIT_HILL_OLD = localDW->UnitDelay15_DSTATE;

  /* UnitDelay: '<Root>/Unit Delay16' */
  rtb_ETCS_HI_DCT_OLD = localDW->UnitDelay16_DSTATE;

  /* UnitDelay: '<Root>/Unit Delay11' */
  rtb_ETCS_ENG_STALL_OLD = localDW->UnitDelay11_DSTATE;

  /* UnitDelay: '<Root>/Unit Delay6' */
  rtb_ETCS_L2SPLT = localDW->UnitDelay6_DSTATE;

  /* ModelReference: '<Root>/LCETCS_vCallCalculation' */
  LCETCS_vCallCalculation(rtu_ETCS_FL, rtu_ETCS_FR, rtu_ETCS_RL, rtu_ETCS_RR,
    rtu_lcetcss16VehSpd, rtu_ETCS_GEAR_STRUCT, rtu_ETCS_ENG_STRUCT,
    rtu_ETCS_EXT_DCT, rtu_ETCS_FA, rtu_ETCS_RA, rtu_ETCS_ESC_SIG,
    rtu_ETCS_BRK_SIG, rtu_ETCS_4WD_SIG, &rtb_ETCS_CTL_ACT_OLD_f,
    &rtb_ETCS_DEP_SNW, &rtb_ETCS_CTL_CMD_OLD_k, rtb_lcetcsu1VehVibDctOld,
    &rtb_ETCS_BRK_CTL_CMD_OLD, &rtb_ETCS_BRK_CTL_REQ_OLD,
    &rtb_ETCS_SPLIT_HILL_OLD, &rtb_ETCS_HI_DCT_OLD, &rtb_ETCS_ENG_STALL_OLD,
    &rtb_ETCS_L2SPLT, rty_ETCS_CTL_ERR, rty_ETCS_TAR_SPIN, rty_ETCS_DRV_MDL,
    rty_ETCS_CRDN_TRQ, rty_ETCS_VEH_ACC, rty_ETCS_ENG_MDL, rty_ETCS_LD_TRQ,
    rty_ETCS_AXL_ACC, rty_lcetcss16SymBrkCtlStrtTh, rty_ETCS_WHL_SPIN,
    rty_ETCS_STR_STAT, rty_ETCS_CTRL_MODE, rty_ETCS_WHL_ACC,
    &(localDW->LCETCS_vCallCalculation_DWORK1.rtdw));

  /* UnitDelay: '<Root>/Unit Delay1' */
  rtb_ETCS_BRK_CTL_CMD_OLD_l = localDW->UnitDelay1_DSTATE;

  /* UnitDelay: '<Root>/Unit Delay4' */
  rtb_ETCS_ENG_CMD_OLD = localDW->UnitDelay4_DSTATE;

  /* UnitDelay: '<Root>/Unit Delay5' */
  rtb_ETCS_CTL_CMD_OLD = localDW->UnitDelay5_DSTATE;

  /* UnitDelay: '<Root>/Unit Delay13' */
  rtb_ETCS_CTL_ACT_OLD = localDW->UnitDelay13_DSTATE;

  /* UnitDelay: '<Root>/Unit Delay14' */
  rtb_ETCS_SPLIT_HILL_OLD_e = localDW->UnitDelay14_DSTATE;

  /* UnitDelay: '<Root>/Unit Delay17' */
  rtb_ETCS_TRQ_REP_RD_FRIC_OLD = localDW->UnitDelay17_DSTATE;

  /* UnitDelay: '<Root>/Unit Delay12' */
  rtb_ETCS_ENG_STALL_OLD = localDW->UnitDelay12_DSTATE;

  /* ModelReference: '<Root>/LCETCS_vCallDetection' */
  LCETCS_vCallDetection(rty_ETCS_CTL_ERR, rty_ETCS_WHL_SPIN, rty_ETCS_TAR_SPIN,
                        rtu_ETCS_ENG_STRUCT, rty_ETCS_CRDN_TRQ,
                        rtu_lcetcss16VehSpd, rty_ETCS_VEH_ACC,
                        rtu_ETCS_GEAR_STRUCT, rtu_ETCS_ESC_SIG, rty_ETCS_AXL_ACC,
                        rtu_ETCS_FA, rtu_ETCS_RA, (*rty_lcetcss16SymBrkCtlStrtTh),
                        &rtb_ETCS_BRK_CTL_CMD_OLD_l, &rtb_ETCS_ENG_CMD_OLD,
                        &rtb_ETCS_CTL_CMD_OLD, rtu_ETCS_EXT_DCT, rtu_ETCS_FL,
                        rtu_ETCS_FR, rtu_ETCS_RL, rtu_ETCS_RR,
                        &rtb_ETCS_CTL_ACT_OLD, &rtb_ETCS_SPLIT_HILL_OLD_e,
                        rty_ETCS_CTRL_MODE, rty_ETCS_WHL_ACC,
                        &rtb_ETCS_TRQ_REP_RD_FRIC_OLD, &rtb_ETCS_ENG_STALL_OLD,
                        rty_ETCS_CTL_ACT, rty_ETCS_CYL_1ST, rty_ETCS_ERR_ZRO_CRS,
                        rty_ETCS_IVAL_RST, rty_ETCS_REF_TRQ_STR_CTL,
                        rty_ETCS_REF_TRQ_STR2CYL, rty_ETCS_REF_TRQ2JMP,
                        rty_ETCS_LOW_WHL_SPN, rty_ETCS_DEP_SNW,
                        rty_ETCS_GAIN_GEAR_SHFT, rty_ETCS_H2L,
                        rty_ETCS_BRK_CTL_REQ, rty_lcetcsu1VehVibDct,
                        rty_ETCS_SPLIT_HILL, rty_ETCS_HI_DCT, rty_ETCS_REQ_VCA,
                        rty_ETCS_L2SPLT, rty_ETCS_AFT_TURN, rty_ETCS_ENG_STALL,
                        &(localDW->LCETCS_vCallDetection_DWORK1.rtdw));

  /* UnitDelay: '<Root>/Unit Delay' */
  rtb_ETCS_CTL_CMD_OLD_l = localDW->UnitDelay_DSTATE;

  /* ModelReference: '<Root>/LCETCS_vCallControl' */
  LCETCS_vCallControl(rty_ETCS_CTL_ACT, rty_ETCS_CTL_ERR, rty_ETCS_DRV_MDL,
                      rty_ETCS_IVAL_RST, rty_ETCS_CYL_1ST, rty_ETCS_VEH_ACC,
                      rty_ETCS_LOW_WHL_SPN, rty_ETCS_GAIN_GEAR_SHFT,
                      rty_ETCS_LD_TRQ, rtu_ETCS_EXT_DCT, rtu_ETCS_ENG_STRUCT,
                      rty_ETCS_H2L, rty_ETCS_BRK_CTL_REQ, rty_ETCS_AXL_ACC,
                      rtu_ETCS_FA, rtu_ETCS_RA, rty_ETCS_CRDN_TRQ,
                      rtu_ETCS_ESC_SIG, rty_ETCS_TAR_SPIN, rtu_lcetcss16VehSpd,
                      rtu_ETCS_GEAR_STRUCT, (*rty_lcetcsu1VehVibDct),
                      rty_ETCS_SPLIT_HILL, rty_ETCS_WHL_SPIN, rtu_ETCS_PC,
                      rtu_ETCS_SC, rty_ETCS_AFT_TURN, &rtb_ETCS_CTL_CMD_OLD_l,
                      rty_ETCS_CTL_GAINS, rty_ETCS_TRQ_REP_RD_FRIC,
                      rty_TMC_REQ_GEAR_STRUCT, rty_ETCS_BRK_CTL_CMD,
                      rty_lcetcsu1FuncLampOn, rty_ETCS_CTL_CMD,
                      &(localDW->LCETCS_vCallControl_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalEngTrqCmd' */
  LCETCS_vCalEngTrqCmd(rty_ETCS_CTL_CMD, rty_ETCS_DRV_MDL, rty_ETCS_ENG_CMD);

  /* Update for UnitDelay: '<Root>/Unit Delay7' */
  localDW->UnitDelay7_DSTATE = *rty_ETCS_CTL_ACT;

  /* Update for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = *rty_ETCS_DEP_SNW;

  /* Update for UnitDelay: '<Root>/Unit Delay3' */
  localDW->UnitDelay3_DSTATE = *rty_ETCS_CTL_CMD;

  /* Update for UnitDelay: '<Root>/Unit Delay8' */
  localDW->UnitDelay8_DSTATE = *rty_lcetcsu1VehVibDct;

  /* Update for UnitDelay: '<Root>/Unit Delay9' */
  localDW->UnitDelay9_DSTATE = *rty_ETCS_BRK_CTL_CMD;

  /* Update for UnitDelay: '<Root>/Unit Delay10' */
  localDW->UnitDelay10_DSTATE = *rty_ETCS_BRK_CTL_REQ;

  /* Update for UnitDelay: '<Root>/Unit Delay15' */
  localDW->UnitDelay15_DSTATE = *rty_ETCS_SPLIT_HILL;

  /* Update for UnitDelay: '<Root>/Unit Delay16' */
  localDW->UnitDelay16_DSTATE = *rty_ETCS_HI_DCT;

  /* Update for UnitDelay: '<Root>/Unit Delay11' */
  localDW->UnitDelay11_DSTATE = *rty_ETCS_ENG_STALL;

  /* Update for UnitDelay: '<Root>/Unit Delay6' */
  localDW->UnitDelay6_DSTATE = *rty_ETCS_L2SPLT;

  /* Update for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = *rty_ETCS_BRK_CTL_CMD;

  /* Update for UnitDelay: '<Root>/Unit Delay4' */
  localDW->UnitDelay4_DSTATE = *rty_ETCS_ENG_CMD;

  /* Update for UnitDelay: '<Root>/Unit Delay5' */
  localDW->UnitDelay5_DSTATE = *rty_ETCS_CTL_CMD;

  /* Update for UnitDelay: '<Root>/Unit Delay13' */
  localDW->UnitDelay13_DSTATE = *rty_ETCS_CTL_ACT;

  /* Update for UnitDelay: '<Root>/Unit Delay14' */
  localDW->UnitDelay14_DSTATE = *rty_ETCS_SPLIT_HILL;

  /* Update for UnitDelay: '<Root>/Unit Delay17' */
  localDW->UnitDelay17_DSTATE = *rty_ETCS_TRQ_REP_RD_FRIC;

  /* Update for UnitDelay: '<Root>/Unit Delay12' */
  localDW->UnitDelay12_DSTATE = *rty_ETCS_ENG_STALL;

  /* Update for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = *rty_ETCS_CTL_CMD;
}

/* Model initialize function */
void FctEtcsCore_initialize(const rtTimingBridge *timingBridge)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalEngTrqCmd' */
  LCETCS_vCalEngTrqCmd_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCallCalculation' */
  LCETCS_vCallCalculation_initialize(timingBridge);

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCallControl' */
  LCETCS_vCallControl_initialize(timingBridge);

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCallDetection' */
  LCETCS_vCallDetection_initialize(timingBridge);
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
