/*
 * File: LCETCS_vDctVehVib.c
 *
 * Code generated for Simulink model 'LCETCS_vDctVehVib'.
 *
 * Model version                  : 1.132
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:58:26 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctVehVib.h"
#include "LCETCS_vDctVehVib_private.h"

/* Output and update for referenced model: 'LCETCS_vDctVehVib' */
void LCETCS_vDctVehVib(const TypeETCSWhlStruct *rtu_ETCS_FL, const
  TypeETCSWhlStruct *rtu_ETCS_FR, const TypeETCSWhlStruct *rtu_ETCS_RL, const
  TypeETCSWhlStruct *rtu_ETCS_RR, boolean_T *rty_lcetcsu1VehVibDct)
{
  /* Logic: '<Root>/Logical Operator2' */
  *rty_lcetcsu1VehVibDct = ((((rtu_ETCS_FL->lcetcsu1WhlVibDct) ||
    (rtu_ETCS_FR->lcetcsu1WhlVibDct)) || (rtu_ETCS_RL->lcetcsu1WhlVibDct)) ||
    (rtu_ETCS_RR->lcetcsu1WhlVibDct));
}

/* Model initialize function */
void LCETCS_vDctVehVib_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
