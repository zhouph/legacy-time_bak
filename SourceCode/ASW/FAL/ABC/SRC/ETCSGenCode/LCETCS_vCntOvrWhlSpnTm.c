/*
 * File: LCETCS_vCntOvrWhlSpnTm.c
 *
 * Code generated for Simulink model 'LCETCS_vCntOvrWhlSpnTm'.
 *
 * Model version                  : 1.158
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:05:17 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCntOvrWhlSpnTm.h"
#include "LCETCS_vCntOvrWhlSpnTm_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCntOvrWhlSpnTm' */
void LCETCS_vCntOvrWhlSpnTm_Init(int16_T *rty_lcetcss16OvrWhlSpnTm,
  DW_LCETCS_vCntOvrWhlSpnTm_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = 0;

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16CntIfSigBigThanTh' */
  LCETCS_s16CntIfSigBigThanTh_Init(rty_lcetcss16OvrWhlSpnTm);
}

/* Output and update for referenced model: 'LCETCS_vCntOvrWhlSpnTm' */
void LCETCS_vCntOvrWhlSpnTm(const TypeETCSWhlSpinStruct *rtu_ETCS_WHL_SPIN,
  const TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN, const TypeETCSCtlActStruct
  *rtu_ETCS_CTL_ACT_OLD, const TypeETCSCtrlModeStruct *rtu_ETCS_CTRL_MODE,
  int16_T *rty_lcetcss16OvrWhlSpnTm, DW_LCETCS_vCntOvrWhlSpnTm_f_T *localDW)
{
  /* local block i/o variables */
  int16_T rtb_BaseSignal;
  int16_T rtb_CounterOld;
  boolean_T rtb_CounterReset;

  /* Logic: '<Root>/Logical Operator' incorporates:
   *  Constant: '<Root>/Constant2'
   *  RelationalOperator: '<Root>/Relational Operator1'
   */
  rtb_CounterReset = ((rtu_ETCS_CTL_ACT_OLD->lcetcsu1CtlAct) ||
                      (rtu_ETCS_CTRL_MODE->lcetcsu8EngCtrlMode == ((uint8_T)
    TCSDpCtrlModeDisable)));

  /* Sum: '<Root>/Sum' */
  rtb_BaseSignal = (int16_T)(rtu_ETCS_WHL_SPIN->lcetcss16WhlSpin -
    rtu_ETCS_TAR_SPIN->lcetcss16TarWhlSpin);

  /* UnitDelay: '<Root>/Unit Delay' */
  rtb_CounterOld = localDW->UnitDelay_DSTATE;

  /* ModelReference: '<Root>/LCETCS_s16CntIfSigBigThanTh' */
  LCETCS_s16CntIfSigBigThanTh(rtb_CounterReset, rtb_BaseSignal,
    rtCP_Constant_Value, rtCP_Constant1_Value, rtb_CounterOld,
    rty_lcetcss16OvrWhlSpnTm);

  /* Update for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = *rty_lcetcss16OvrWhlSpnTm;
}

/* Model initialize function */
void LCETCS_vCntOvrWhlSpnTm_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16CntIfSigBigThanTh' */
  LCETCS_s16CntIfSigBigThanTh_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
