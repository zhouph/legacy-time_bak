/*
 * File: LCETCS_vCalPGainFtrInTrn.h
 *
 * Code generated for Simulink model 'LCETCS_vCalPGainFtrInTrn'.
 *
 * Model version                  : 1.206
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:02:29 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalPGainFtrInTrn_h_
#define RTW_HEADER_LCETCS_vCalPGainFtrInTrn_h_
#ifndef LCETCS_vCalPGainFtrInTrn_COMMON_INCLUDES_
# define LCETCS_vCalPGainFtrInTrn_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalPGainFtrInTrn_COMMON_INCLUDES_ */

#include "LCETCS_vCalPGainFtrInTrn_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_s16Inter3by5.h"

/* Block signals for model 'LCETCS_vCalPGainFtrInTrn' */
typedef struct {
  int16_T x3;                          /* '<S1>/Ay level on asphalt' */
  int16_T x1;                          /* '<S1>/Ay level on ice' */
  int16_T x2;                          /* '<S1>/Ay level on snow' */
  int16_T z11;                         /* '<S1>/P gain factor in turn on ice at 5th gear' */
  int16_T z12;                         /* '<S1>/P gain factor in turn on snow at 5th gear' */
  int16_T z13;                         /* '<S1>/P gain factor in turn on asphalt at 5th gear' */
  int16_T z21;                         /* '<S1>/P gain factor in turn on ice at 4th gear' */
  int16_T z22;                         /* '<S1>/P gain factor in turn on snow at 4th gear' */
  int16_T z23;                         /* '<S1>/P gain factor in turn on asphalt at 4th gear' */
  int16_T z31;                         /* '<S1>/P gain factor in turn on ice at 3rd gear' */
  int16_T z32;                         /* '<S1>/P gain factor in turn on snow at 3rd gear' */
  int16_T z33;                         /* '<S1>/P gain factor in turn on asphalt at 3rd gear' */
  int16_T z41;                         /* '<S1>/P gain factor in turn on ice at 2nd gear' */
  int16_T z42;                         /* '<S1>/P gain factor in turn on snow at 2nd gear' */
  int16_T z43;                         /* '<S1>/P gain factor in turn on asphalt at 2nd gear' */
  int16_T z51;                         /* '<S1>/P gain factor in turn on ice at 1st gear' */
  int16_T z52;                         /* '<S1>/P gain factor in turn on snow at 1st gear' */
  int16_T z53;                         /* '<S1>/P gain factor in turn on asphalt at 1st gear' */
  int16_T Out;                         /* '<S1>/LCETCS_s16Inter2by3' */
} B_LCETCS_vCalPGainFtrInTrn_c_T;

typedef struct {
  B_LCETCS_vCalPGainFtrInTrn_c_T rtb;
} MdlrefDW_LCETCS_vCalPGainFtrInTrn_T;

/* Model reference registration function */
extern void LCETCS_vCalPGainFtrInTrn_initialize(void);
extern void LCETCS_vCalPGainFtrInTrn_Start(B_LCETCS_vCalPGainFtrInTrn_c_T
  *localB);
extern void LCETCS_vCalPGainFtrInTrn(const TypeETCSCtlActStruct
  *rtu_ETCS_CTL_ACT, const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const
  TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN, const TypeETCSDrvMdlStruct
  *rtu_ETCS_DRV_MDL, const TypeETCSCtlErrStruct *rtu_ETCS_CTL_ERR, int16_T
  *rty_lcetcss16PGainFacInTrn, B_LCETCS_vCalPGainFtrInTrn_c_T *localB);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalPGainFtrInTrn'
 * '<S1>'   : 'LCETCS_vCalPGainFtrInTrn/ActOfCalPGainFacInTrn'
 * '<S2>'   : 'LCETCS_vCalPGainFtrInTrn/DeActOfCalPGainFacInTrn'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalPGainFtrInTrn_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
