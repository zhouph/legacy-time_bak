/*
 * File: LCETCS_vCalEngSpdSlope.c
 *
 * Code generated for Simulink model 'LCETCS_vCalEngSpdSlope'.
 *
 * Model version                  : 1.247
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:05:39 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalEngSpdSlope.h"
#include "LCETCS_vCalEngSpdSlope_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalEngSpdSlope' */
void LCETCS_vCalEngSpdSlope_Init(DW_LCETCS_vCalEngSpdSlope_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = 0;
}

/* Output and update for referenced model: 'LCETCS_vCalEngSpdSlope' */
void LCETCS_vCalEngSpdSlope(const TypeETCSEngStruct *rtu_ETCS_ENG_STRUCT,
  int16_T *rty_lcetcss16EngSpdSlope, DW_LCETCS_vCalEngSpdSlope_f_T *localDW)
{
  /* local block i/o variables */
  int16_T rtb_Ls16SigFltd;
  int16_T rtb_Ls16InpGain4Flt;
  int16_T rtb_Ls16SigFltdOld;

  /* DataTypeConversion: '<Root>/Data Type Conversion1' incorporates:
   *  Constant: '<Root>/LowPassFilter 1Hz'
   */
  rtb_Ls16InpGain4Flt = ((uint8_T)L_U8FILTER_GAIN_20MSLOOP_1HZ);

  /* UnitDelay: '<Root>/Unit Delay' */
  rtb_Ls16SigFltdOld = localDW->UnitDelay_DSTATE;

  /* ModelReference: '<Root>/LCETCS_vCal1stOrdFlter' */
  LCETCS_vCal1stOrdFlter(rtu_ETCS_ENG_STRUCT->lcetcss16EngSpd,
    rtb_Ls16InpGain4Flt, rtb_Ls16SigFltdOld, &rtb_Ls16SigFltd);

  /* Sum: '<Root>/Add' */
  *rty_lcetcss16EngSpdSlope = (int16_T)(rtb_Ls16SigFltd - rtb_Ls16SigFltdOld);

  /* Update for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = rtb_Ls16SigFltd;
}

/* Model initialize function */
void LCETCS_vCalEngSpdSlope_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCal1stOrdFlter' */
  LCETCS_vCal1stOrdFlter_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
