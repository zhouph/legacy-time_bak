/*
 * File: LCETCS_vCalJmpUpDnFtr.h
 *
 * Code generated for Simulink model 'LCETCS_vCalJmpUpDnFtr'.
 *
 * Model version                  : 1.210
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:04:41 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalJmpUpDnFtr_h_
#define RTW_HEADER_LCETCS_vCalJmpUpDnFtr_h_
#ifndef LCETCS_vCalJmpUpDnFtr_COMMON_INCLUDES_
# define LCETCS_vCalJmpUpDnFtr_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalJmpUpDnFtr_COMMON_INCLUDES_ */

#include "LCETCS_vCalJmpUpDnFtr_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_s16Inter2Point.h"

/* Block signals for model 'LCETCS_vCalJmpUpDnFtr' */
typedef struct {
  int16_T lcetcss16Ftr4JmpDn;          /* '<Root>/LCETCS_s16Inter2Point' */
  int16_T lcetcss16Ftr4JmpUp;          /* '<Root>/LCETCS_s16Inter2Point1' */
  int16_T y2;                          /* '<Root>/Constant6' */
  int16_T y1;                          /* '<Root>/Constant1' */
  int16_T x2;                          /* '<Root>/Constant8' */
  int16_T x1;                          /* '<Root>/Constant7' */
} B_LCETCS_vCalJmpUpDnFtr_c_T;

typedef struct {
  B_LCETCS_vCalJmpUpDnFtr_c_T rtb;
} MdlrefDW_LCETCS_vCalJmpUpDnFtr_T;

/* Model reference registration function */
extern void LCETCS_vCalJmpUpDnFtr_initialize(void);
extern void LCETCS_vCalJmpUpDnFtr_Init(B_LCETCS_vCalJmpUpDnFtr_c_T *localB);
extern void LCETCS_vCalJmpUpDnFtr_Start(B_LCETCS_vCalJmpUpDnFtr_c_T *localB);
extern void LCETCS_vCalJmpUpDnFtr(int32_T rtu_lcetcss32RefTrq2Jmp, boolean_T
  rtu_lcetcsu1RefTrq4JmpSetOk, int16_T rtu_lcetcss16RefTrqSetTm,
  TypeETCSRefTrq2JmpStruct *rty_ETCS_REF_TRQ2JMP, B_LCETCS_vCalJmpUpDnFtr_c_T
  *localB);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalJmpUpDnFtr'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalJmpUpDnFtr_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
