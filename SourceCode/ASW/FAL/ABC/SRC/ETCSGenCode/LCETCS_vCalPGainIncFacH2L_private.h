/*
 * File: LCETCS_vCalPGainIncFacH2L_private.h
 *
 * Code generated for Simulink model 'LCETCS_vCalPGainIncFacH2L'.
 *
 * Model version                  : 1.177
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:02:36 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalPGainIncFacH2L_private_h_
#define RTW_HEADER_LCETCS_vCalPGainIncFacH2L_private_h_
#include "rtwtypes.h"
#include "model_reference_types.h"

/* Includes for objects with custom storage classes. */
#include "../ETCSGenCodeHeader/ETCSHeader4Import.h"

/*
 * Generate compile time checks that imported macros for parameters
 * with storage class "ImportedDefine" are defined
 */
#ifndef U8ETCSCpH2LDctHldTm
#error The variable for the parameter "U8ETCSCpH2LDctHldTm" is not defined
#endif

#ifndef U8ETCSCpPGainIncFacH2L
#error The variable for the parameter "U8ETCSCpPGainIncFacH2L" is not defined
#endif

extern const int16_T rtCP_pooled_ZjYULP70HcKf;
extern const int16_T rtCP_pooled_sqsAoF3Vfmvz;

#define rtCP_Constant1_Value           rtCP_pooled_ZjYULP70HcKf  /* Computed Parameter: Constant1_Value
                                                                  * Referenced by: '<Root>/Constant1'
                                                                  */
#define rtCP_Constant4_Value           rtCP_pooled_sqsAoF3Vfmvz  /* Computed Parameter: Constant4_Value
                                                                  * Referenced by: '<Root>/Constant4'
                                                                  */
#endif                                 /* RTW_HEADER_LCETCS_vCalPGainIncFacH2L_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
