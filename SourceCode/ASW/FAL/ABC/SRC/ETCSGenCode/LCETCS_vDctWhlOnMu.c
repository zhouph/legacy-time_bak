/*
 * File: LCETCS_vDctWhlOnMu.c
 *
 * Code generated for Simulink model 'LCETCS_vDctWhlOnMu'.
 *
 * Model version                  : 1.202
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:56:41 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctWhlOnMu.h"
#include "LCETCS_vDctWhlOnMu_private.h"

/* Initial conditions for referenced model: 'LCETCS_vDctWhlOnMu' */
void LCETCS_vDctWhlOnMu_Init(int16_T *rty_lcetcss16LowMuWhlSpinNF, int16_T
  *rty_lcetcss16HighMuWhlSpinNF, boolean_T *rty_lcetcsu1DctWhlOnHighmu)
{
  /* InitializeConditions for Chart: '<Root>/Chart' */
  *rty_lcetcss16LowMuWhlSpinNF = 0;
  *rty_lcetcss16HighMuWhlSpinNF = 0;
  *rty_lcetcsu1DctWhlOnHighmu = false;
}

/* Output and update for referenced model: 'LCETCS_vDctWhlOnMu' */
void LCETCS_vDctWhlOnMu(const TypeETCSWhlStruct *rtu_ETCS_FL, const
  TypeETCSWhlStruct *rtu_ETCS_FR, const TypeETCSWhlStruct *rtu_ETCS_RL, const
  TypeETCSWhlStruct *rtu_ETCS_RR, int16_T *rty_lcetcss16LowMuWhlSpinNF, int16_T *
  rty_lcetcss16HighMuWhlSpinNF, boolean_T *rty_lcetcsu1DctWhlOnHighmu)
{
  int32_T WhlSpinDiffNFTemp;
  int16_T rtb_Switch1;
  int16_T rtb_Switch2;

  /* Switch: '<Root>/Switch1' incorporates:
   *  Constant: '<Root>/Constant3'
   */
  if (((boolean_T)VarFwd)) {
    rtb_Switch1 = rtu_ETCS_FL->lcetcss16WhlSpinNF;
  } else {
    rtb_Switch1 = rtu_ETCS_RL->lcetcss16WhlSpinNF;
  }

  /* End of Switch: '<Root>/Switch1' */

  /* Switch: '<Root>/Switch2' incorporates:
   *  Constant: '<Root>/Constant4'
   */
  if (((boolean_T)VarFwd)) {
    rtb_Switch2 = rtu_ETCS_FR->lcetcss16WhlSpinNF;
  } else {
    rtb_Switch2 = rtu_ETCS_RR->lcetcss16WhlSpinNF;
  }

  /* End of Switch: '<Root>/Switch2' */

  /* Chart: '<Root>/Chart' */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:142' */
  /* comment */
  WhlSpinDiffNFTemp = rtb_Switch1 - rtb_Switch2;
  if (WhlSpinDiffNFTemp > ((int16_T)VREF_1_KPH)) {
    /* Transition: '<S1>:151' */
    /* Transition: '<S1>:153' */
    *rty_lcetcss16LowMuWhlSpinNF = rtb_Switch1;
    *rty_lcetcss16HighMuWhlSpinNF = rtb_Switch2;
    *rty_lcetcsu1DctWhlOnHighmu = true;

    /* Transition: '<S1>:170' */
    /* Transition: '<S1>:202' */
    /* Transition: '<S1>:201' */
  } else {
    /* Transition: '<S1>:162' */
    if (WhlSpinDiffNFTemp < (-((int16_T)VREF_1_KPH))) {
      /* Transition: '<S1>:164' */
      /* Transition: '<S1>:166' */
      *rty_lcetcss16LowMuWhlSpinNF = rtb_Switch2;
      *rty_lcetcss16HighMuWhlSpinNF = rtb_Switch1;
      *rty_lcetcsu1DctWhlOnHighmu = true;

      /* Transition: '<S1>:202' */
      /* Transition: '<S1>:201' */
    } else {
      /* Transition: '<S1>:168' */
      if (WhlSpinDiffNFTemp > 0) {
        /* Transition: '<S1>:186' */
        /* Transition: '<S1>:194' */
        *rty_lcetcss16LowMuWhlSpinNF = rtb_Switch1;
        *rty_lcetcss16HighMuWhlSpinNF = rtb_Switch2;
        *rty_lcetcsu1DctWhlOnHighmu = false;

        /* Transition: '<S1>:201' */
      } else {
        /* Transition: '<S1>:200' */
        *rty_lcetcss16LowMuWhlSpinNF = rtb_Switch2;
        *rty_lcetcss16HighMuWhlSpinNF = rtb_Switch1;
        *rty_lcetcsu1DctWhlOnHighmu = false;
      }
    }
  }

  /* End of Chart: '<Root>/Chart' */
  /* Transition: '<S1>:199' */
}

/* Model initialize function */
void LCETCS_vDctWhlOnMu_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
