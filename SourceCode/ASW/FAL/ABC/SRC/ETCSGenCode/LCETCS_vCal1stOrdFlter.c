/*
 * File: LCETCS_vCal1stOrdFlter.c
 *
 * Code generated for Simulink model 'LCETCS_vCal1stOrdFlter'.
 *
 * Model version                  : 1.81
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:47:49 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCal1stOrdFlter.h"
#include "LCETCS_vCal1stOrdFlter_private.h"

/* Output and update for referenced model: 'LCETCS_vCal1stOrdFlter' */
void LCETCS_vCal1stOrdFlter(int16_T rtu_Ls16Sig2Flt, int16_T rtu_Ls16InpGain4Flt,
  int16_T rtu_Ls16SigFltdOld, int16_T *rty_Ls16SigFltd)
{
  int32_T u0;

  /* Product: '<Root>/Divide' incorporates:
   *  Product: '<Root>/Product'
   *  Product: '<Root>/Product1'
   *  Sum: '<Root>/Add'
   *  Sum: '<Root>/Add1'
   */
  u0 = (((128 - rtu_Ls16InpGain4Flt) * rtu_Ls16SigFltdOld) + (rtu_Ls16Sig2Flt *
         rtu_Ls16InpGain4Flt)) / 128;

  /* Saturate: '<Root>/Saturation' */
  if (u0 > 32767) {
    /* DataTypeConversion: '<Root>/Data Type Conversion' */
    *rty_Ls16SigFltd = MAX_int16_T;
  } else if (u0 < -32768) {
    /* DataTypeConversion: '<Root>/Data Type Conversion' */
    *rty_Ls16SigFltd = MIN_int16_T;
  } else {
    /* DataTypeConversion: '<Root>/Data Type Conversion' */
    *rty_Ls16SigFltd = (int16_T)u0;
  }

  /* End of Saturate: '<Root>/Saturation' */
}

/* Model initialize function */
void LCETCS_vCal1stOrdFlter_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
