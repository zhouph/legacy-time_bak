/*
 * File: LCETCS_vCalLmtCrngDltYaw.c
 *
 * Code generated for Simulink model 'LCETCS_vCalLmtCrngDltYaw'.
 *
 * Model version                  : 1.66
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:59:49 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalLmtCrngDltYaw.h"
#include "LCETCS_vCalLmtCrngDltYaw_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalLmtCrngDltYaw' */
void LCETCS_vCalLmtCrngDltYaw_Init(B_LCETCS_vCalLmtCrngDltYaw_c_T *localB)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter3Point' */
  LCETCS_s16Inter3Point_Init(&localB->y);

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_Init(&localB->y_p);
}

/* Start for referenced model: 'LCETCS_vCalLmtCrngDltYaw' */
void LCETCS_vCalLmtCrngDltYaw_Start(B_LCETCS_vCalLmtCrngDltYaw_c_T *localB)
{
  /* Start for Constant: '<Root>/3Delta yaw which is not changing target spin when the speed is below x5' */
  localB->y2_d = ((uint8_T)U8ETCSCpTarNChngDltYawHiBrkTrq);

  /* Start for Constant: '<Root>/Delta yaw which is not changing target spin when the speed is below x1' */
  localB->y1 = ((uint8_T)U8ETCSCpTarNChngDltYawLwSpd);

  /* Start for Constant: '<Root>/Delta yaw which is not changing target spin when the speed is below x2' */
  localB->y2 = ((uint8_T)U8ETCSCpTarNChngDltYawHiSpd);

  /* Start for Constant: '<Root>/Delta yaw which is not changing target spin when the speed is below x3' */
  localB->y3 = ((uint8_T)U8ETCSCpTarNChngDltYawVyHSpd);

  /* Start for Constant: '<Root>/Delta yaw which is not changing target spin when the speed is below x4' */
  localB->y1_l = ((uint8_T)U8ETCSCpTarNChngDltYawLwBrkTrq);
}

/* Output and update for referenced model: 'LCETCS_vCalLmtCrngDltYaw' */
void LCETCS_vCalLmtCrngDltYaw(int16_T rtu_lcetcss16VehSpd, const
  TypeETCSAxlStruct *rtu_ETCS_FA, const TypeETCSAxlStruct *rtu_ETCS_RA, int16_T *
  rty_lcetcss16LmtCrngDltYaw, B_LCETCS_vCalLmtCrngDltYaw_c_T *localB)
{
  /* local block i/o variables */
  int16_T rtb_x1;
  int16_T rtb_x2;
  int16_T rtb_x3;
  int16_T rtb_x;
  int16_T rtb_x1_h;
  int16_T rtb_x2_m;
  int16_T tmp;

  /* Constant: '<Root>/3Delta yaw which is not changing target spin when the speed is below x5' */
  localB->y2_d = ((uint8_T)U8ETCSCpTarNChngDltYawHiBrkTrq);

  /* Constant: '<Root>/Delta yaw which is not changing target spin when the speed is below x1' */
  localB->y1 = ((uint8_T)U8ETCSCpTarNChngDltYawLwSpd);

  /* Constant: '<Root>/Delta yaw which is not changing target spin when the speed is below x2' */
  localB->y2 = ((uint8_T)U8ETCSCpTarNChngDltYawHiSpd);

  /* Constant: '<Root>/Delta yaw which is not changing target spin when the speed is below x3' */
  localB->y3 = ((uint8_T)U8ETCSCpTarNChngDltYawVyHSpd);

  /* Constant: '<Root>/Delta yaw which is not changing target spin when the speed is below x4' */
  localB->y1_l = ((uint8_T)U8ETCSCpTarNChngDltYawLwBrkTrq);

  /* Constant: '<Root>/Speed which delta yaw is not valid 1' */
  rtb_x1_h = ((uint8_T)U8ETCSCpTarTransStBrkTrq2Sp);

  /* Gain: '<Root>/Gain' */
  rtb_x1_h = (int16_T)(10 * rtb_x1_h);

  /* Constant: '<Root>/Speed which delta yaw is valid 1' */
  rtb_x2_m = ((uint8_T)U8ETCSCpTarTransCplBrkTrq2Sp);

  /* Gain: '<Root>/Gain1' */
  rtb_x2_m = (int16_T)(10 * rtb_x2_m);

  /* Gain: '<Root>/Gain4' incorporates:
   *  Constant: '<Root>/Speed which delta yaw is not valid '
   */
  rtb_x1 = (int16_T)(((uint8_T)U8ETCSCpDltYawNVldSpd) << 3);

  /* Gain: '<Root>/Gain5' incorporates:
   *  Constant: '<Root>/Speed which delta yaw is valid '
   */
  rtb_x2 = (int16_T)(((uint8_T)U8ETCSCpDltYawVldSpd) << 3);

  /* Gain: '<Root>/Gain6' incorporates:
   *  Constant: '<Root>/High Speed which delta yaw is valid'
   */
  rtb_x3 = (int16_T)(((uint8_T)U8ETCSCpDltYawVldVyHSpd) << 3);

  /* ModelReference: '<Root>/LCETCS_s16Inter3Point' */
  LCETCS_s16Inter3Point(rtu_lcetcss16VehSpd, rtb_x1, rtb_x2, rtb_x3, localB->y1,
                        localB->y2, localB->y3, &localB->y);

  /* MinMax: '<Root>/MinMax' */
  if (rtu_ETCS_FA->lcetcss16EstBrkTrqOnLowMuWhl >=
      rtu_ETCS_RA->lcetcss16EstBrkTrqOnLowMuWhl) {
    rtb_x = rtu_ETCS_FA->lcetcss16EstBrkTrqOnLowMuWhl;
  } else {
    rtb_x = rtu_ETCS_RA->lcetcss16EstBrkTrqOnLowMuWhl;
  }

  /* End of MinMax: '<Root>/MinMax' */

  /* ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point(rtb_x, rtb_x1_h, rtb_x2_m, localB->y1_l, localB->y2_d,
                        &localB->y_p);

  /* MinMax: '<Root>/MinMax1' */
  if (localB->y >= localB->y_p) {
    tmp = localB->y;
  } else {
    tmp = localB->y_p;
  }

  /* Gain: '<Root>/Gain2' incorporates:
   *  MinMax: '<Root>/MinMax1'
   */
  *rty_lcetcss16LmtCrngDltYaw = (int16_T)(10 * tmp);

  /* Saturate: '<Root>/Saturation' */
  if ((*rty_lcetcss16LmtCrngDltYaw) > 2550) {
    *rty_lcetcss16LmtCrngDltYaw = 2550;
  } else {
    if ((*rty_lcetcss16LmtCrngDltYaw) < 0) {
      *rty_lcetcss16LmtCrngDltYaw = 0;
    }
  }

  /* End of Saturate: '<Root>/Saturation' */
}

/* Model initialize function */
void LCETCS_vCalLmtCrngDltYaw_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter3Point' */
  LCETCS_s16Inter3Point_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
