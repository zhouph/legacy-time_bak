/*
 * File: LCETCS_vDctSetDepSnwHil.c
 *
 * Code generated for Simulink model 'LCETCS_vDctSetDepSnwHil'.
 *
 * Model version                  : 1.159
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:55:24 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctSetDepSnwHil.h"
#include "LCETCS_vDctSetDepSnwHil_private.h"

/* Initial conditions for referenced model: 'LCETCS_vDctSetDepSnwHil' */
void LCETCS_vDctSetDepSnwHil_Init(boolean_T *rty_lcetcsu1DepSnwHilAct,
  DW_LCETCS_vDctSetDepSnwHil_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = false;

  /* InitializeConditions for Chart: '<Root>/Chart3' */
  *rty_lcetcsu1DepSnwHilAct = false;
}

/* Output and update for referenced model: 'LCETCS_vDctSetDepSnwHil' */
void LCETCS_vDctSetDepSnwHil(int16_T rtu_lcetcss16DepSnwHilCnt, boolean_T
  *rty_lcetcsu1DepSnwHilAct, DW_LCETCS_vDctSetDepSnwHil_f_T *localDW)
{
  /* Chart: '<Root>/Chart3' incorporates:
   *  UnitDelay: '<Root>/Unit Delay2'
   */
  /* Gateway: Chart3 */
  /* During: Chart3 */
  /* Entry Internal: Chart3 */
  /* Transition: '<S1>:25' */
  /* comment */
  if (localDW->UnitDelay2_DSTATE == 0) {
    /* Transition: '<S1>:95' */
    /* Transition: '<S1>:97' */
    if (rtu_lcetcss16DepSnwHilCnt >= ((uint8_T)U8ETCSCpDetDepSnwHilCnt)) {
      /* Transition: '<S1>:144' */
      /* Transition: '<S1>:146' */
      *rty_lcetcsu1DepSnwHilAct = true;

      /* Transition: '<S1>:147' */
    } else {
      /* Transition: '<S1>:151' */
      *rty_lcetcsu1DepSnwHilAct = localDW->UnitDelay2_DSTATE;
    }

    /* Transition: '<S1>:148' */
    /* Transition: '<S1>:133' */
  } else {
    /* Transition: '<S1>:122' */
    if (rtu_lcetcss16DepSnwHilCnt >= ((uint8_T)U8ETCSCpHysDepSnwHilCnt)) {
      /* Transition: '<S1>:128' */
      /* Transition: '<S1>:130' */
      *rty_lcetcsu1DepSnwHilAct = true;

      /* Transition: '<S1>:133' */
    } else {
      /* Transition: '<S1>:132' */
      *rty_lcetcsu1DepSnwHilAct = false;
    }
  }

  /* End of Chart: '<Root>/Chart3' */

  /* Update for UnitDelay: '<Root>/Unit Delay2' */
  /* Transition: '<S1>:142' */
  localDW->UnitDelay2_DSTATE = *rty_lcetcsu1DepSnwHilAct;
}

/* Model initialize function */
void LCETCS_vDctSetDepSnwHil_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
