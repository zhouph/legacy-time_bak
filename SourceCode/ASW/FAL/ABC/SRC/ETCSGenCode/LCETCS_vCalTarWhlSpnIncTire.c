/*
 * File: LCETCS_vCalTarWhlSpnIncTire.c
 *
 * Code generated for Simulink model 'LCETCS_vCalTarWhlSpnIncTire'.
 *
 * Model version                  : 1.86
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:07:36 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalTarWhlSpnIncTire.h"
#include "LCETCS_vCalTarWhlSpnIncTire_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalTarWhlSpnIncTire' */
void LCETCS_vCalTarWhlSpnIncTire_Init(boolean_T *rty_lcetcsu1AbnrmSizeTire,
  B_LCETCS_vCalTarWhlSpnIncTire_c_T *localB)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalAbnrmSizeTire' */
  LCETCS_vCalAbnrmSizeTire_Init(&localB->lcetcsu1AbnrmSizeTireDet,
    &localB->lcetcsu1AbnrmSizeTireSus, rty_lcetcsu1AbnrmSizeTire);
}

/* Output and update for referenced model: 'LCETCS_vCalTarWhlSpnIncTire' */
void LCETCS_vCalTarWhlSpnIncTire(const TypeETCSWhlStruct *rtu_ETCS_FL, const
  TypeETCSWhlStruct *rtu_ETCS_FR, const TypeETCSWhlStruct *rtu_ETCS_RL, const
  TypeETCSWhlStruct *rtu_ETCS_RR, int16_T rtu_lcetcss16VehSpd, const
  TypeETCSExtDctStruct *rtu_ETCS_EXT_DCT, boolean_T *rty_lcetcsu1AbnrmSizeTire,
  int16_T *rty_lcetcss16TarWhlSpnIncAbnrmTire, B_LCETCS_vCalTarWhlSpnIncTire_c_T
  *localB)
{
  /* ModelReference: '<Root>/LCETCS_vCalAbnrmSizeTire' */
  LCETCS_vCalAbnrmSizeTire(rtu_ETCS_FL, rtu_ETCS_FR, rtu_ETCS_RL, rtu_ETCS_RR,
    &localB->lcetcsu1AbnrmSizeTireDet, &localB->lcetcsu1AbnrmSizeTireSus,
    rty_lcetcsu1AbnrmSizeTire);

  /* ModelReference: '<Root>/LCETCS_vCalDetTarWhlIncTire' */
  LCETCS_vCalDetTarWhlIncTire(localB->lcetcsu1AbnrmSizeTireDet,
    localB->lcetcsu1AbnrmSizeTireSus, rtu_lcetcss16VehSpd, rtu_ETCS_EXT_DCT,
    rty_lcetcss16TarWhlSpnIncAbnrmTire);
}

/* Model initialize function */
void LCETCS_vCalTarWhlSpnIncTire_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalAbnrmSizeTire' */
  LCETCS_vCalAbnrmSizeTire_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalDetTarWhlIncTire' */
  LCETCS_vCalDetTarWhlIncTire_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
