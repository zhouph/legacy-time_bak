/*
 * File: LCETCS_vDctPreHighRd.c
 *
 * Code generated for Simulink model 'LCETCS_vDctPreHighRd'.
 *
 * Model version                  : 1.185
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:06:16 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctPreHighRd.h"
#include "LCETCS_vDctPreHighRd_private.h"

/* Initial conditions for referenced model: 'LCETCS_vDctPreHighRd' */
void LCETCS_vDctPreHighRd_Init(B_LCETCS_vDctPreHighRd_c_T *localB,
  DW_LCETCS_vDctPreHighRd_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = 0;

  /* InitializeConditions for Chart: '<Root>/Chart' */
  localB->lcetcss16PreHighRdDctCnt = 0;

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter3Point' */
  LCETCS_s16Inter3Point_Init(&localB->y);
}

/* Output and update for referenced model: 'LCETCS_vDctPreHighRd' */
void LCETCS_vDctPreHighRd(const TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC, const
  TypeETCSCrdnTrqStruct *rtu_ETCS_CRDN_TRQ, int16_T rtu_lcetcss16VehSpd, const
  TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const TypeETCSWhlSpinStruct
  *rtu_ETCS_WHL_SPIN, const TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN, const
  TypeETCSCtlErrStruct *rtu_ETCS_CTL_ERR, int16_T *rty_lcetcss16PreHighRdDctCnt,
  B_LCETCS_vDctPreHighRd_c_T *localB, DW_LCETCS_vDctPreHighRd_f_T *localDW)
{
  /* local block i/o variables */
  int16_T rtb_x;
  int16_T rtb_x1;
  int16_T rtb_y1;
  int16_T rtb_y2;
  int16_T rtb_y3;
  int32_T tmp;

  /* Sum: '<Root>/Add1' */
  rtb_x = (int16_T)(rtu_ETCS_WHL_SPIN->lcetcss16WhlSpin -
                    rtu_ETCS_TAR_SPIN->lcetcss16BsTarWhlSpin);

  /* Chart: '<Root>/Chart' incorporates:
   *  Product: '<Root>/Divide2'
   *  UnitDelay: '<Root>/Unit Delay1'
   */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:9' */
  /* comment */
  if (rtu_ETCS_CTL_ERR->lcetcsu1CtlErrPos == 1) {
    /* Transition: '<S1>:141' */
    /* Transition: '<S1>:142' */
    if (rtu_ETCS_VEH_ACC->lcetcss16RsltntAcc >=
        rtu_ETCS_VEH_ACC->lcetcss16RefRdCfFricAtAsphalt) {
      /* Transition: '<S1>:10' */
      /* Transition: '<S1>:108' */
      if (rtu_ETCS_ESC_SIG->lcetcsu1BrkPdlPrsed == 1) {
        /* Transition: '<S1>:126' */
        /* Transition: '<S1>:130' */
        localB->lcetcss16PreHighRdDctCnt = localDW->UnitDelay1_DSTATE;

        /* Transition: '<S1>:129' */
        /* Transition: '<S1>:114' */
      } else {
        /* Transition: '<S1>:128' */
        if (localB->lcetcss16PreHighRdDctCnt < ((int16_T)
             S16ETCSCpPreHighRdDctStrtCnt)) {
          /* Transition: '<S1>:110' */
          /* Transition: '<S1>:112' */
          localB->lcetcss16PreHighRdDctCnt = ((int16_T)
            S16ETCSCpPreHighRdDctStrtCnt);

          /* Transition: '<S1>:114' */
        } else {
          /* Transition: '<S1>:113' */
          tmp = localDW->UnitDelay1_DSTATE + 1;
          if (tmp > 32767) {
            tmp = 32767;
          }

          localB->lcetcss16PreHighRdDctCnt = (int16_T)tmp;
        }
      }

      /* Transition: '<S1>:138' */
      /* Transition: '<S1>:137' */
      /* Transition: '<S1>:94' */
    } else {
      /* Transition: '<S1>:86' */
      if ((rtu_lcetcss16VehSpd >= ((uint8_T)VREF_10_KPH)) &&
          ((rtu_ETCS_CRDN_TRQ->lcetcss32RealCrdnTrq / 10) >= ((int16_T)
            S16ETCSCpCdrnTrqAtAsphalt))) {
        /* Transition: '<S1>:88' */
        /* Transition: '<S1>:91' */
        if (rtu_ETCS_ESC_SIG->lcetcsu1BrkPdlPrsed == 1) {
          /* Transition: '<S1>:133' */
          /* Transition: '<S1>:136' */
          localB->lcetcss16PreHighRdDctCnt = localDW->UnitDelay1_DSTATE;

          /* Transition: '<S1>:137' */
          /* Transition: '<S1>:94' */
        } else {
          /* Transition: '<S1>:134' */
          tmp = localDW->UnitDelay1_DSTATE + 1;
          if (tmp > 32767) {
            tmp = 32767;
          }

          localB->lcetcss16PreHighRdDctCnt = (int16_T)tmp;

          /* Transition: '<S1>:94' */
        }
      } else {
        /* Transition: '<S1>:92' */
        tmp = localDW->UnitDelay1_DSTATE - 1;
        if (tmp < -32768) {
          tmp = -32768;
        }

        localB->lcetcss16PreHighRdDctCnt = (int16_T)tmp;
      }
    }

    /* Transition: '<S1>:145' */
  } else {
    /* Transition: '<S1>:144' */
    tmp = localDW->UnitDelay1_DSTATE - 1;
    if (tmp < -32768) {
      tmp = -32768;
    }

    localB->lcetcss16PreHighRdDctCnt = (int16_T)tmp;
  }

  /* End of Chart: '<Root>/Chart' */

  /* DataTypeConversion: '<Root>/Data Type Conversion' incorporates:
   *  Constant: '<Root>/Spin Error 0kph'
   */
  /* Transition: '<S1>:147' */
  rtb_x1 = 0;

  /* DataTypeConversion: '<Root>/Data Type Conversion3' incorporates:
   *  Constant: '<Root>/Decrease Count 0'
   */
  rtb_y1 = 0;

  /* DataTypeConversion: '<Root>/Data Type Conversion4' incorporates:
   *  Constant: '<Root>/Decrease Count 1'
   */
  rtb_y2 = 1;

  /* DataTypeConversion: '<Root>/Data Type Conversion5' incorporates:
   *  Constant: '<Root>/Decrease Count 2'
   */
  rtb_y3 = 2;

  /* ModelReference: '<Root>/LCETCS_s16Inter3Point' */
  LCETCS_s16Inter3Point(rtb_x, rtb_x1, ((int16_T)VREF_1_KPH), ((int16_T)
    VREF_3_KPH), rtb_y1, rtb_y2, rtb_y3, &localB->y);

  /* Sum: '<Root>/Add2' */
  *rty_lcetcss16PreHighRdDctCnt = (int16_T)(localB->lcetcss16PreHighRdDctCnt -
    localB->y);

  /* Saturate: '<Root>/Saturation1' */
  if ((*rty_lcetcss16PreHighRdDctCnt) > ((int16_T)S16ETCSCpPreHighRdDctCplCnt))
  {
    *rty_lcetcss16PreHighRdDctCnt = ((int16_T)S16ETCSCpPreHighRdDctCplCnt);
  } else {
    if ((*rty_lcetcss16PreHighRdDctCnt) < 0) {
      *rty_lcetcss16PreHighRdDctCnt = 0;
    }
  }

  /* End of Saturate: '<Root>/Saturation1' */

  /* Update for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = *rty_lcetcss16PreHighRdDctCnt;
}

/* Model initialize function */
void LCETCS_vDctPreHighRd_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter3Point' */
  LCETCS_s16Inter3Point_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
