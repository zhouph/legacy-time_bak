/*
 * File: LCTMC_vCalRefHysTarGearMap.c
 *
 * Code generated for Simulink model 'LCTMC_vCalRefHysTarGearMap'.
 *
 * Model version                  : 1.299
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:53:06 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCTMC_vCalRefHysTarGearMap.h"
#include "LCTMC_vCalRefHysTarGearMap_private.h"

/* Output and update for referenced model: 'LCTMC_vCalRefHysTarGearMap' */
void LCTMC_vCalRefHysTarGearMap(boolean_T rtu_lctmcu1NonCndTarReqGear, const
  TypeTMCReqGearStruct *rtu_TMC_REQ_GEAR_STRUCT_OLD, const TypeETCSGearStruct
  *rtu_ETCS_GEAR_STRUCT, int16_T rtu_lctmcs16CordTarUpShtMaxVSbyGear, int16_T
  rtu_lctmcs16CordTarDnShtMinVSbyGear, int16_T rtu_lctmcs16CordTarUpShtVS,
  int16_T rtu_lctmcs16CordTarUpShtRPM, int16_T rtu_lctmcs16CordTarDnShtVS,
  int16_T rtu_lctmcs16CordTarDnShtRPM, TypeTMCRefLimVehSpdMap
  *rty_TMC_REF_LIM_VS_MAP, TypeTMCRefFinTarGearMap *rty_TMC_REF_FIN_TAR_GEAR_MAP)
{
  int16_T tmc_temp2;
  int16_T rtb_lctmcs16TarUpShtVS_i;
  int16_T rtb_lctmcs16TarUpShtRPM_l;
  int16_T rtb_lctmcs16TarDnShtVS_l;
  int16_T rtb_lctmcs16TarDnShtRPM_h;
  int16_T rtb_lctmcs16TarUpShtMaxVSbyGear_g;
  int16_T rtb_lctmcs16TarDnShtMinVSbyGear_n;
  int32_T tmp;

  /* Chart: '<Root>/Chart1' */
  /* Gateway: Chart1 */
  /* During: Chart1 */
  /* Entry Internal: Chart1 */
  /* Transition: '<S1>:10' */
  /* comment */
  if ((rtu_TMC_REQ_GEAR_STRUCT_OLD->lctmcu8_BSTGRReqType != 1) &&
      (rtu_lctmcu1NonCndTarReqGear == 0)) {
    /* Transition: '<S1>:77' */
    /* Gear Shift Inhibit Mode */
    /* Transition: '<S1>:68' */
    rtb_lctmcs16TarDnShtRPM_h = (int16_T)(((uint8_T)U8TMCpShftChgVehSpdHys) << 3);
    tmc_temp2 = (int16_T)(((uint8_T)U8TMCpShftChgRPMHys) * 100);
    if (rtu_TMC_REQ_GEAR_STRUCT_OLD->lctmcu8_BSTGRReqGear >
        rtu_ETCS_GEAR_STRUCT->lcetcss16CurntGearStep) {
      /* Transition: '<S1>:70' */
      /* Transition: '<S1>:74' */
      rtb_lctmcs16TarUpShtMaxVSbyGear_g = (int16_T)
        (rtu_lctmcs16CordTarUpShtMaxVSbyGear - rtb_lctmcs16TarDnShtRPM_h);
      rtb_lctmcs16TarUpShtVS_i = (int16_T)(rtu_lctmcs16CordTarUpShtVS -
        rtb_lctmcs16TarDnShtRPM_h);
      rtb_lctmcs16TarUpShtRPM_l = (int16_T)(rtu_lctmcs16CordTarUpShtRPM -
        tmc_temp2);
      rtb_lctmcs16TarDnShtMinVSbyGear_n = rtu_lctmcs16CordTarDnShtMinVSbyGear;
      rtb_lctmcs16TarDnShtVS_l = rtu_lctmcs16CordTarDnShtVS;
      rtb_lctmcs16TarDnShtRPM_h = rtu_lctmcs16CordTarDnShtRPM;

      /* Transition: '<S1>:84' */
      /* Transition: '<S1>:85' */
    } else {
      /* Transition: '<S1>:72' */
      if (rtu_TMC_REQ_GEAR_STRUCT_OLD->lctmcu8_BSTGRReqGear <
          rtu_ETCS_GEAR_STRUCT->lcetcss16CurntGearStep) {
        /* Transition: '<S1>:80' */
        /* Transition: '<S1>:86' */
        rtb_lctmcs16TarUpShtMaxVSbyGear_g = rtu_lctmcs16CordTarUpShtMaxVSbyGear;
        rtb_lctmcs16TarUpShtVS_i = rtu_lctmcs16CordTarUpShtVS;
        rtb_lctmcs16TarUpShtRPM_l = rtu_lctmcs16CordTarUpShtRPM;
        rtb_lctmcs16TarDnShtMinVSbyGear_n = (int16_T)
          (rtu_lctmcs16CordTarDnShtMinVSbyGear + rtb_lctmcs16TarDnShtRPM_h);
        rtb_lctmcs16TarDnShtVS_l = (int16_T)(rtu_lctmcs16CordTarDnShtVS +
          rtb_lctmcs16TarDnShtRPM_h);
        tmp = rtu_lctmcs16CordTarDnShtRPM + tmc_temp2;
        if (tmp > 32767) {
          tmp = 32767;
        }

        rtb_lctmcs16TarDnShtRPM_h = (int16_T)tmp;

        /* Transition: '<S1>:85' */
      } else {
        /* Transition: '<S1>:82' */
        rtb_lctmcs16TarUpShtMaxVSbyGear_g = rtu_lctmcs16CordTarUpShtMaxVSbyGear;
        rtb_lctmcs16TarUpShtVS_i = rtu_lctmcs16CordTarUpShtVS;
        rtb_lctmcs16TarUpShtRPM_l = rtu_lctmcs16CordTarUpShtRPM;
        rtb_lctmcs16TarDnShtMinVSbyGear_n = rtu_lctmcs16CordTarDnShtMinVSbyGear;
        rtb_lctmcs16TarDnShtVS_l = rtu_lctmcs16CordTarDnShtVS;
        rtb_lctmcs16TarDnShtRPM_h = rtu_lctmcs16CordTarDnShtRPM;
      }
    }

    /* Transition: '<S1>:31' */
  } else {
    /* Transition: '<S1>:78' */
    rtb_lctmcs16TarUpShtMaxVSbyGear_g = rtu_lctmcs16CordTarUpShtMaxVSbyGear;
    rtb_lctmcs16TarUpShtVS_i = rtu_lctmcs16CordTarUpShtVS;
    rtb_lctmcs16TarUpShtRPM_l = rtu_lctmcs16CordTarUpShtRPM;
    rtb_lctmcs16TarDnShtMinVSbyGear_n = rtu_lctmcs16CordTarDnShtMinVSbyGear;
    rtb_lctmcs16TarDnShtVS_l = rtu_lctmcs16CordTarDnShtVS;
    rtb_lctmcs16TarDnShtRPM_h = rtu_lctmcs16CordTarDnShtRPM;
  }

  /* End of Chart: '<Root>/Chart1' */

  /* Saturate: '<Root>/Saturation' */
  /* Transition: '<S1>:34' */
  if (rtb_lctmcs16TarUpShtVS_i < 0) {
    rty_TMC_REF_FIN_TAR_GEAR_MAP->lctmcs16TarUpShtVS = 0;
  } else {
    rty_TMC_REF_FIN_TAR_GEAR_MAP->lctmcs16TarUpShtVS = rtb_lctmcs16TarUpShtVS_i;
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Saturate: '<Root>/Saturation2' */
  if (rtb_lctmcs16TarUpShtRPM_l < 0) {
    rty_TMC_REF_FIN_TAR_GEAR_MAP->lctmcs16TarUpShtRPM = 0;
  } else {
    rty_TMC_REF_FIN_TAR_GEAR_MAP->lctmcs16TarUpShtRPM =
      rtb_lctmcs16TarUpShtRPM_l;
  }

  /* End of Saturate: '<Root>/Saturation2' */

  /* Saturate: '<Root>/Saturation1' */
  if (rtb_lctmcs16TarDnShtVS_l > 2000) {
    rty_TMC_REF_FIN_TAR_GEAR_MAP->lctmcs16TarDnShtVS = 2000;
  } else {
    rty_TMC_REF_FIN_TAR_GEAR_MAP->lctmcs16TarDnShtVS = rtb_lctmcs16TarDnShtVS_l;
  }

  /* End of Saturate: '<Root>/Saturation1' */

  /* Saturate: '<Root>/Saturation3' */
  if (rtb_lctmcs16TarDnShtRPM_h > 8000) {
    rty_TMC_REF_FIN_TAR_GEAR_MAP->lctmcs16TarDnShtRPM = 8000;
  } else {
    rty_TMC_REF_FIN_TAR_GEAR_MAP->lctmcs16TarDnShtRPM =
      rtb_lctmcs16TarDnShtRPM_h;
  }

  /* End of Saturate: '<Root>/Saturation3' */

  /* Saturate: '<Root>/Saturation4' */
  if (rtb_lctmcs16TarUpShtMaxVSbyGear_g < 0) {
    rty_TMC_REF_LIM_VS_MAP->lctmcs16TarUpShtMaxVSbyGear = 0;
  } else {
    rty_TMC_REF_LIM_VS_MAP->lctmcs16TarUpShtMaxVSbyGear =
      rtb_lctmcs16TarUpShtMaxVSbyGear_g;
  }

  /* End of Saturate: '<Root>/Saturation4' */

  /* Saturate: '<Root>/Saturation5' */
  if (rtb_lctmcs16TarDnShtMinVSbyGear_n > 2000) {
    rty_TMC_REF_LIM_VS_MAP->lctmcs16TarDnShtMinVSbyGear = 2000;
  } else {
    rty_TMC_REF_LIM_VS_MAP->lctmcs16TarDnShtMinVSbyGear =
      rtb_lctmcs16TarDnShtMinVSbyGear_n;
  }

  /* End of Saturate: '<Root>/Saturation5' */
}

/* Model initialize function */
void LCTMC_vCalRefHysTarGearMap_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
