/*
 * File: LCETCS_vCalCtlAct.h
 *
 * Code generated for Simulink model 'LCETCS_vCalCtlAct'.
 *
 * Model version                  : 1.207
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:04:57 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalCtlAct_h_
#define RTW_HEADER_LCETCS_vCalCtlAct_h_
#ifndef LCETCS_vCalCtlAct_COMMON_INCLUDES_
# define LCETCS_vCalCtlAct_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalCtlAct_COMMON_INCLUDES_ */

#include "LCETCS_vCalCtlAct_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vDctRisingEdge.h"

/* Block states (auto storage) for model 'LCETCS_vCalCtlAct' */
typedef struct {
  boolean_T UnitDelay1_DSTATE;         /* '<Root>/Unit Delay1' */
  MdlrefDW_LCETCS_vDctRisingEdge_T LCETCS_vDctRisingEdge_DWORK1;/* '<Root>/LCETCS_vDctRisingEdge' */
} DW_LCETCS_vCalCtlAct_f_T;

typedef struct {
  DW_LCETCS_vCalCtlAct_f_T rtdw;
} MdlrefDW_LCETCS_vCalCtlAct_T;

/* Model reference registration function */
extern void LCETCS_vCalCtlAct_initialize(void);
extern void LCETCS_vCalCtlAct_Init(boolean_T *rty_lcetcsu1CtlAct, boolean_T
  *rty_lcetcsu1CtlActRsgEdg, DW_LCETCS_vCalCtlAct_f_T *localDW);
extern void LCETCS_vCalCtlAct(const TypeETCSEngStallStruct *rtu_ETCS_ENG_STALL,
  boolean_T rtu_lcetcsu1DriverAccelIntend, boolean_T
  rtu_lcetcsu1DriverAccelHysIntend, int16_T rtu_lcetcss16OvrWhlSpnTm, const
  TypeETCSEngCmdStruct *rtu_ETCS_ENG_CMD_OLD, int16_T rtu_lcetcss16OvrWhlSpnTmTh,
  const TypeETCSEngStruct *rtu_ETCS_ENG_STRUCT, int16_T
  rtu_lcetcss16OvrEngTrqCmdTm, const TypeETCSCtrlModeStruct *rtu_ETCS_CTRL_MODE,
  int16_T rtu_lcetcss16LmtCnrgCtlEntrTm, boolean_T *rty_lcetcsu1CtlAct,
  boolean_T *rty_lcetcsu1CtlActRsgEdg, DW_LCETCS_vCalCtlAct_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalCtlAct'
 * '<S1>'   : 'LCETCS_vCalCtlAct/Chart'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalCtlAct_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
