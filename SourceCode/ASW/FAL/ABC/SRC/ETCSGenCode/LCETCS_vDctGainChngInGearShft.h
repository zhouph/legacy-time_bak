/*
 * File: LCETCS_vDctGainChngInGearShft.h
 *
 * Code generated for Simulink model 'LCETCS_vDctGainChngInGearShft'.
 *
 * Model version                  : 1.201
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:55:47 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vDctGainChngInGearShft_h_
#define RTW_HEADER_LCETCS_vDctGainChngInGearShft_h_
#ifndef LCETCS_vDctGainChngInGearShft_COMMON_INCLUDES_
# define LCETCS_vDctGainChngInGearShft_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vDctGainChngInGearShft_COMMON_INCLUDES_ */

#include "LCETCS_vDctGainChngInGearShft_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Block signals for model 'LCETCS_vDctGainChngInGearShft' */
typedef struct {
  uint8_T lcetcsu8GainTrnsTm;          /* '<Root>/Chart1' */
} B_LCETCS_vDctGainChngInGearShft_c_T;

/* Block states (auto storage) for model 'LCETCS_vDctGainChngInGearShft' */
typedef struct {
  TypeETCSGainGearShftStruct UnitDelay_DSTATE;/* '<Root>/Unit Delay' */
} DW_LCETCS_vDctGainChngInGearShft_f_T;

typedef struct {
  B_LCETCS_vDctGainChngInGearShft_c_T rtb;
  DW_LCETCS_vDctGainChngInGearShft_f_T rtdw;
} MdlrefDW_LCETCS_vDctGainChngInGearShft_T;

/* Model reference registration function */
extern void LCETCS_vDctGainChngInGearShft_initialize(const rtTimingBridge
  *timingBridge);
extern const TypeETCSGainGearShftStruct
  LCETCS_vDctGainChngInGearShft_rtZTypeETCSGainGearShftStruct;/* TypeETCSGainGearShftStruct ground */
extern void LCETCS_vDctGainChngInGearShft_Init(TypeETCSGainGearShftStruct
  *rty_ETCS_GAIN_GEAR_SHFT, B_LCETCS_vDctGainChngInGearShft_c_T *localB,
  DW_LCETCS_vDctGainChngInGearShft_f_T *localDW);
extern void LCETCS_vDctGainChngInGearShft(const TypeETCSH2LStruct *rtu_ETCS_H2L,
  const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const TypeETCSGearStruct
  *rtu_ETCS_GEAR_STRUCT, const TypeETCSCyl1stStruct *rtu_ETCS_CYL_1ST, const
  TypeETCSExtDctStruct *rtu_ETCS_EXT_DCT, TypeETCSGainGearShftStruct
  *rty_ETCS_GAIN_GEAR_SHFT, B_LCETCS_vDctGainChngInGearShft_c_T *localB,
  DW_LCETCS_vDctGainChngInGearShft_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vDctGainChngInGearShft'
 * '<S1>'   : 'LCETCS_vDctGainChngInGearShft/Chart'
 * '<S2>'   : 'LCETCS_vDctGainChngInGearShft/Chart1'
 */
#endif                                 /* RTW_HEADER_LCETCS_vDctGainChngInGearShft_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
