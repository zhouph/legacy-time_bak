/*
 * File: LCETCS_vCalHomoBsIGain.c
 *
 * Code generated for Simulink model 'LCETCS_vCalHomoBsIGain'.
 *
 * Model version                  : 1.153
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:01:25 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalHomoBsIGain.h"
#include "LCETCS_vCalHomoBsIGain_private.h"

/* Start for referenced model: 'LCETCS_vCalHomoBsIGain' */
void LCETCS_vCalHomoBsIGain_Start(B_LCETCS_vCalHomoBsIGain_c_T *localB)
{
  /* Start for IfAction SubSystem: '<Root>/CalPGainWhnCtlOn' */
  /* Start for IfAction SubSystem: '<S2>/CalIgainWhnCtlErrNeg' */
  /* Start for Constant: '<S3>/Igain for 1st gear ratio on asphalt' */
  localB->z53_n = ((uint8_T)U8ETCSCpNegErrIgainAsp_1);

  /* Start for Constant: '<S3>/Igain for 1st gear ratio on ice' */
  localB->z51_h = ((uint8_T)U8ETCSCpNegErrIgainIce_1);

  /* Start for Constant: '<S3>/Igain for 1st gear ratio on snow' */
  localB->z52_o = ((uint8_T)U8ETCSCpNegErrIgainSnw_1);

  /* Start for Constant: '<S3>/Igain for 2nd gear ratio on asphalt' */
  localB->z43_k = ((uint8_T)U8ETCSCpNegErrIgainAsp_2);

  /* Start for Constant: '<S3>/Igain for 2nd gear ratio on ice' */
  localB->z41_m = ((uint8_T)U8ETCSCpNegErrIgainIce_2);

  /* Start for Constant: '<S3>/Igain for 2nd gear ratio on snow' */
  localB->z42_i = ((uint8_T)U8ETCSCpNegErrIgainSnw_2);

  /* Start for Constant: '<S3>/Igain for 3rd gear ratio on asphalt' */
  localB->z33_e = ((uint8_T)U8ETCSCpNegErrIgainAsp_3);

  /* Start for Constant: '<S3>/Igain for 3rd gear ratio on ice' */
  localB->z31_k = ((uint8_T)U8ETCSCpNegErrIgainIce_3);

  /* Start for Constant: '<S3>/Igain for 3rd gear ratio on snow' */
  localB->z32_p = ((uint8_T)U8ETCSCpNegErrIgainSnw_3);

  /* Start for Constant: '<S3>/Igain for 4th gear ratio on asphalt' */
  localB->z23_g = ((uint8_T)U8ETCSCpNegErrIgainAsp_4);

  /* Start for Constant: '<S3>/Igain for 4th gear ratio on ice' */
  localB->z21_b = ((uint8_T)U8ETCSCpNegErrIgainIce_4);

  /* Start for Constant: '<S3>/Igain for 4th gear ratio on snow' */
  localB->z22_l = ((uint8_T)U8ETCSCpNegErrIgainSnw_4);

  /* Start for Constant: '<S3>/Igain for 5th gear ratio on Asphalt' */
  localB->z13_a = ((uint8_T)U8ETCSCpNegErrIgainAsp_5);

  /* Start for Constant: '<S3>/Igain for 5th gear ratio on ice' */
  localB->z11_a = ((uint8_T)U8ETCSCpNegErrIgainIce_5);

  /* Start for Constant: '<S3>/Igain for 5th gear ratio on snow' */
  localB->z12_l = ((uint8_T)U8ETCSCpNegErrIgainSnw_5);

  /* End of Start for SubSystem: '<S2>/CalIgainWhnCtlErrNeg' */

  /* InitializeConditions for IfAction SubSystem: '<S2>/CalIgainWhnCtlErrNeg' */

  /* InitializeConditions for ModelReference: '<S3>/LCETCS_s16Inter3by5' */
  LCETCS_s16Inter3by5_Init(&localB->lcetcss16HomoBsIgain_p);

  /* End of InitializeConditions for SubSystem: '<S2>/CalIgainWhnCtlErrNeg' */

  /* Start for IfAction SubSystem: '<S2>/CalPgainWhnCtlErrPos' */
  /* Start for Constant: '<S4>/Igain for 1st gear ratio on asphalt' */
  localB->z53 = ((uint8_T)U8ETCSCpPosErrIgainAsp_1);

  /* Start for Constant: '<S4>/Igain for 1st gear ratio on ice' */
  localB->z51 = ((uint8_T)U8ETCSCpPosErrIgainIce_1);

  /* Start for Constant: '<S4>/Igain for 1st gear ratio on snow' */
  localB->z52 = ((uint8_T)U8ETCSCpPosErrIgainSnw_1);

  /* Start for Constant: '<S4>/Igain for 2nd gear ratio on asphalt' */
  localB->z43 = ((uint8_T)U8ETCSCpPosErrIgainAsp_2);

  /* Start for Constant: '<S4>/Igain for 2nd gear ratio on ice' */
  localB->z41 = ((uint8_T)U8ETCSCpPosErrIgainIce_2);

  /* Start for Constant: '<S4>/Igain for 2nd gear ratio on snow' */
  localB->z42 = ((uint8_T)U8ETCSCpPosErrIgainSnw_2);

  /* Start for Constant: '<S4>/Igain for 3rd gear ratio on asphalt' */
  localB->z33 = ((uint8_T)U8ETCSCpPosErrIgainAsp_3);

  /* Start for Constant: '<S4>/Igain for 3rd gear ratio on ice' */
  localB->z31 = ((uint8_T)U8ETCSCpPosErrIgainIce_3);

  /* Start for Constant: '<S4>/Igain for 3rd gear ratio on snow' */
  localB->z32 = ((uint8_T)U8ETCSCpPosErrIgainSnw_3);

  /* Start for Constant: '<S4>/Igain for 4th gear ratio on asphalt' */
  localB->z23 = ((uint8_T)U8ETCSCpPosErrIgainAsp_4);

  /* Start for Constant: '<S4>/Igain for 4th gear ratio on ice' */
  localB->z21 = ((uint8_T)U8ETCSCpPosErrIgainIce_4);

  /* Start for Constant: '<S4>/Igain for 4th gear ratio on snow' */
  localB->z22 = ((uint8_T)U8ETCSCpPosErrIgainSnw_4);

  /* Start for Constant: '<S4>/Igain for 5th gear ratio on Asphalt' */
  localB->z13 = ((uint8_T)U8ETCSCpPosErrIgainAsp_5);

  /* Start for Constant: '<S4>/Igain for 5th gear ratio on ice' */
  localB->z11 = ((uint8_T)U8ETCSCpPosErrIgainIce_5);

  /* Start for Constant: '<S4>/Igain for 5th gear ratio on snow' */
  localB->z12 = ((uint8_T)U8ETCSCpPosErrIgainSnw_5);

  /* End of Start for SubSystem: '<S2>/CalPgainWhnCtlErrPos' */

  /* InitializeConditions for IfAction SubSystem: '<S2>/CalPgainWhnCtlErrPos' */

  /* InitializeConditions for ModelReference: '<S4>/LCETCS_s16Inter3by5_1' */
  LCETCS_s16Inter3by5_Init(&localB->lcetcss16HomoBsIgain);

  /* End of InitializeConditions for SubSystem: '<S2>/CalPgainWhnCtlErrPos' */

  /* End of Start for SubSystem: '<Root>/CalPGainWhnCtlOn' */
}

/* Output and update for referenced model: 'LCETCS_vCalHomoBsIGain' */
void LCETCS_vCalHomoBsIGain(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const
  TypeETCSDrvMdlStruct *rtu_ETCS_DRV_MDL, const TypeETCSCtlErrStruct
  *rtu_ETCS_CTL_ERR, const TypeETCSTrq4RdFricStruct *rtu_ETCS_TRQ_REP_RD_FRIC,
  int16_T *rty_lcetcss16HomoBsIgain, B_LCETCS_vCalHomoBsIGain_c_T *localB)
{
  /* local block i/o variables */
  int16_T rtb_x;
  int16_T rtb_x_m;

  /* If: '<Root>/If1' incorporates:
   *  Constant: '<S1>/Constant'
   *  If: '<S2>/If1'
   */
  if (!rtu_ETCS_CTL_ACT->lcetcsu1CtlAct) {
    /* Outputs for IfAction SubSystem: '<Root>/CalIGainWhnCtlOff' incorporates:
     *  ActionPort: '<S1>/Action Port'
     */
    *rty_lcetcss16HomoBsIgain = 0;

    /* End of Outputs for SubSystem: '<Root>/CalIGainWhnCtlOff' */
  } else {
    /* Outputs for IfAction SubSystem: '<Root>/CalPGainWhnCtlOn' incorporates:
     *  ActionPort: '<S2>/Action Port'
     */
    if (!rtu_ETCS_CTL_ERR->lcetcsu1CtlErrPos) {
      /* Outputs for IfAction SubSystem: '<S2>/CalIgainWhnCtlErrNeg' incorporates:
       *  ActionPort: '<S3>/Action Port'
       */
      rtb_x_m = (int16_T)(rtu_ETCS_TRQ_REP_RD_FRIC->lcetcss32TrqRepRdFric / 10);
      localB->z53_n = ((uint8_T)U8ETCSCpNegErrIgainAsp_1);
      localB->z51_h = ((uint8_T)U8ETCSCpNegErrIgainIce_1);
      localB->z52_o = ((uint8_T)U8ETCSCpNegErrIgainSnw_1);
      localB->z43_k = ((uint8_T)U8ETCSCpNegErrIgainAsp_2);
      localB->z41_m = ((uint8_T)U8ETCSCpNegErrIgainIce_2);
      localB->z42_i = ((uint8_T)U8ETCSCpNegErrIgainSnw_2);
      localB->z33_e = ((uint8_T)U8ETCSCpNegErrIgainAsp_3);
      localB->z31_k = ((uint8_T)U8ETCSCpNegErrIgainIce_3);
      localB->z32_p = ((uint8_T)U8ETCSCpNegErrIgainSnw_3);
      localB->z23_g = ((uint8_T)U8ETCSCpNegErrIgainAsp_4);
      localB->z21_b = ((uint8_T)U8ETCSCpNegErrIgainIce_4);
      localB->z22_l = ((uint8_T)U8ETCSCpNegErrIgainSnw_4);
      localB->z13_a = ((uint8_T)U8ETCSCpNegErrIgainAsp_5);
      localB->z11_a = ((uint8_T)U8ETCSCpNegErrIgainIce_5);
      localB->z12_l = ((uint8_T)U8ETCSCpNegErrIgainSnw_5);

      /* ModelReference: '<S3>/LCETCS_s16Inter3by5' */
      LCETCS_s16Inter3by5(rtb_x_m, ((int16_T)S16ETCSCpCdrnTrqAtIce), ((int16_T)
        S16ETCSCpCdrnTrqAtSnow), ((int16_T)S16ETCSCpCdrnTrqAtAsphalt),
                          rtu_ETCS_DRV_MDL->lcetcss16TotalGearRatio, ((int16_T)
        S16ETCSCpTotalGearRatio_5), ((int16_T)S16ETCSCpTotalGearRatio_4),
                          ((int16_T)S16ETCSCpTotalGearRatio_3), ((int16_T)
        S16ETCSCpTotalGearRatio_2), ((int16_T)S16ETCSCpTotalGearRatio_1),
                          localB->z11_a, localB->z12_l, localB->z13_a,
                          localB->z21_b, localB->z22_l, localB->z23_g,
                          localB->z31_k, localB->z32_p, localB->z33_e,
                          localB->z41_m, localB->z42_i, localB->z43_k,
                          localB->z51_h, localB->z52_o, localB->z53_n,
                          &localB->lcetcss16HomoBsIgain_p);
      *rty_lcetcss16HomoBsIgain = localB->lcetcss16HomoBsIgain_p;

      /* End of Outputs for SubSystem: '<S2>/CalIgainWhnCtlErrNeg' */
    } else {
      /* Outputs for IfAction SubSystem: '<S2>/CalPgainWhnCtlErrPos' incorporates:
       *  ActionPort: '<S4>/Action Port'
       */
      rtb_x = (int16_T)(rtu_ETCS_TRQ_REP_RD_FRIC->lcetcss32TrqRepRdFric / 10);
      localB->z53 = ((uint8_T)U8ETCSCpPosErrIgainAsp_1);
      localB->z51 = ((uint8_T)U8ETCSCpPosErrIgainIce_1);
      localB->z52 = ((uint8_T)U8ETCSCpPosErrIgainSnw_1);
      localB->z43 = ((uint8_T)U8ETCSCpPosErrIgainAsp_2);
      localB->z41 = ((uint8_T)U8ETCSCpPosErrIgainIce_2);
      localB->z42 = ((uint8_T)U8ETCSCpPosErrIgainSnw_2);
      localB->z33 = ((uint8_T)U8ETCSCpPosErrIgainAsp_3);
      localB->z31 = ((uint8_T)U8ETCSCpPosErrIgainIce_3);
      localB->z32 = ((uint8_T)U8ETCSCpPosErrIgainSnw_3);
      localB->z23 = ((uint8_T)U8ETCSCpPosErrIgainAsp_4);
      localB->z21 = ((uint8_T)U8ETCSCpPosErrIgainIce_4);
      localB->z22 = ((uint8_T)U8ETCSCpPosErrIgainSnw_4);
      localB->z13 = ((uint8_T)U8ETCSCpPosErrIgainAsp_5);
      localB->z11 = ((uint8_T)U8ETCSCpPosErrIgainIce_5);
      localB->z12 = ((uint8_T)U8ETCSCpPosErrIgainSnw_5);

      /* ModelReference: '<S4>/LCETCS_s16Inter3by5_1' */
      LCETCS_s16Inter3by5(rtb_x, ((int16_T)S16ETCSCpCdrnTrqAtIce), ((int16_T)
        S16ETCSCpCdrnTrqAtSnow), ((int16_T)S16ETCSCpCdrnTrqAtAsphalt),
                          rtu_ETCS_DRV_MDL->lcetcss16TotalGearRatio, ((int16_T)
        S16ETCSCpTotalGearRatio_5), ((int16_T)S16ETCSCpTotalGearRatio_4),
                          ((int16_T)S16ETCSCpTotalGearRatio_3), ((int16_T)
        S16ETCSCpTotalGearRatio_2), ((int16_T)S16ETCSCpTotalGearRatio_1),
                          localB->z11, localB->z12, localB->z13, localB->z21,
                          localB->z22, localB->z23, localB->z31, localB->z32,
                          localB->z33, localB->z41, localB->z42, localB->z43,
                          localB->z51, localB->z52, localB->z53,
                          &localB->lcetcss16HomoBsIgain);
      *rty_lcetcss16HomoBsIgain = localB->lcetcss16HomoBsIgain;

      /* End of Outputs for SubSystem: '<S2>/CalPgainWhnCtlErrPos' */
    }

    /* End of Outputs for SubSystem: '<Root>/CalPGainWhnCtlOn' */
  }

  /* End of If: '<Root>/If1' */
}

/* Model initialize function */
void LCETCS_vCalHomoBsIGain_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<S3>/LCETCS_s16Inter3by5' */
  LCETCS_s16Inter3by5_initialize();

  /* Model Initialize fcn for ModelReference Block: '<S4>/LCETCS_s16Inter3by5_1' */
  LCETCS_s16Inter3by5_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
