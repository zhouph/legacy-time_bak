/*
 * File: LCETCS_vCalCtlGain_private.h
 *
 * Code generated for Simulink model 'LCETCS_vCalCtlGain'.
 *
 * Model version                  : 1.230
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:12:14 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalCtlGain_private_h_
#define RTW_HEADER_LCETCS_vCalCtlGain_private_h_
#include "rtwtypes.h"
#include "model_reference_types.h"

/* Includes for objects with custom storage classes. */
#include "../ETCSGenCodeHeader/ETCSHeader4Import.h"

/*
 * Generate compile time checks that imported macros for parameters
 * with storage class "ImportedDefine" are defined
 */
#ifndef S16ETCSCpCdrnTrqAtAsphalt
#error The variable for the parameter "S16ETCSCpCdrnTrqAtAsphalt" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtIce
#error The variable for the parameter "S16ETCSCpCdrnTrqAtIce" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtSnow
#error The variable for the parameter "S16ETCSCpCdrnTrqAtSnow" is not defined
#endif

#ifndef S16ETCSCpIGainIncFacH2L
#error The variable for the parameter "S16ETCSCpIGainIncFacH2L" is not defined
#endif

#ifndef S16ETCSCpLmtCrngTarTrqAsp
#error The variable for the parameter "S16ETCSCpLmtCrngTarTrqAsp" is not defined
#endif

#ifndef S16ETCSCpLmtCrngTarTrqIce
#error The variable for the parameter "S16ETCSCpLmtCrngTarTrqIce" is not defined
#endif

#ifndef S16ETCSCpLmtCrngTarTrqSnw
#error The variable for the parameter "S16ETCSCpLmtCrngTarTrqSnw" is not defined
#endif

#ifndef S16ETCSCpSpd_1
#error The variable for the parameter "S16ETCSCpSpd_1" is not defined
#endif

#ifndef S16ETCSCpSpd_2
#error The variable for the parameter "S16ETCSCpSpd_2" is not defined
#endif

#ifndef S16ETCSCpSpd_3
#error The variable for the parameter "S16ETCSCpSpd_3" is not defined
#endif

#ifndef S16ETCSCpSpd_4
#error The variable for the parameter "S16ETCSCpSpd_4" is not defined
#endif

#ifndef S16ETCSCpSpd_5
#error The variable for the parameter "S16ETCSCpSpd_5" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_1
#error The variable for the parameter "S16ETCSCpTotalGearRatio_1" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_2
#error The variable for the parameter "S16ETCSCpTotalGearRatio_2" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_3
#error The variable for the parameter "S16ETCSCpTotalGearRatio_3" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_4
#error The variable for the parameter "S16ETCSCpTotalGearRatio_4" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_5
#error The variable for the parameter "S16ETCSCpTotalGearRatio_5" is not defined
#endif

#ifndef VREF_3_KPH
#error The variable for the parameter "VREF_3_KPH" is not defined
#endif

#ifndef S8ETCSCpPgTransTmDrvVib
#error The variable for the parameter "S8ETCSCpPgTransTmDrvVib" is not defined
#endif

#ifndef U8ETCSCpAyAsp
#error The variable for the parameter "U8ETCSCpAyAsp" is not defined
#endif

#ifndef U8ETCSCpAyIce
#error The variable for the parameter "U8ETCSCpAyIce" is not defined
#endif

#ifndef U8ETCSCpAySnw
#error The variable for the parameter "U8ETCSCpAySnw" is not defined
#endif

#ifndef U8ETCSCpBigSpnMaxIgFac
#error The variable for the parameter "U8ETCSCpBigSpnMaxIgFac" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainAsp_1
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainAsp_1" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainAsp_2
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainAsp_2" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainAsp_3
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainAsp_3" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainAsp_4
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainAsp_4" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainAsp_5
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainAsp_5" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainIce_1
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainIce_1" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainIce_2
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainIce_2" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainIce_3
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainIce_3" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainIce_4
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainIce_4" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainIce_5
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainIce_5" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainSnw_1
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainSnw_1" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainSnw_2
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainSnw_2" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainSnw_3
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainSnw_3" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainSnw_4
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainSnw_4" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainSnw_5
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainSnw_5" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainAsp_1
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainAsp_1" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainAsp_2
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainAsp_2" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainAsp_3
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainAsp_3" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainAsp_4
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainAsp_4" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainAsp_5
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainAsp_5" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainIce_1
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainIce_1" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainIce_2
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainIce_2" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainIce_3
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainIce_3" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainIce_4
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainIce_4" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainIce_5
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainIce_5" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainSnw_1
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainSnw_1" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainSnw_2
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainSnw_2" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainSnw_3
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainSnw_3" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainSnw_4
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainSnw_4" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainSnw_5
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainSnw_5" is not defined
#endif

#ifndef U8ETCSCpDelYawLowTrqGainAsp
#error The variable for the parameter "U8ETCSCpDelYawLowTrqGainAsp" is not defined
#endif

#ifndef U8ETCSCpDelYawLowTrqGainIce
#error The variable for the parameter "U8ETCSCpDelYawLowTrqGainIce" is not defined
#endif

#ifndef U8ETCSCpDelYawLowTrqGainSnw
#error The variable for the parameter "U8ETCSCpDelYawLowTrqGainSnw" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainAsp_1
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainAsp_1" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainAsp_2
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainAsp_2" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainAsp_3
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainAsp_3" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainAsp_4
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainAsp_4" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainAsp_5
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainAsp_5" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainIce_1
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainIce_1" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainIce_2
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainIce_2" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainIce_3
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainIce_3" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainIce_4
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainIce_4" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainIce_5
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainIce_5" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainSnw_1
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainSnw_1" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainSnw_2
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainSnw_2" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainSnw_3
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainSnw_3" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainSnw_4
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainSnw_4" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainSnw_5
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainSnw_5" is not defined
#endif

#ifndef U8ETCSCpGainTrnsTmAftrGearShft
#error The variable for the parameter "U8ETCSCpGainTrnsTmAftrGearShft" is not defined
#endif

#ifndef U8ETCSCpH2LDctHldTm
#error The variable for the parameter "U8ETCSCpH2LDctHldTm" is not defined
#endif

#ifndef U8ETCSCpIgFacGrChgErrNeg_1
#error The variable for the parameter "U8ETCSCpIgFacGrChgErrNeg_1" is not defined
#endif

#ifndef U8ETCSCpIgFacGrChgErrNeg_2
#error The variable for the parameter "U8ETCSCpIgFacGrChgErrNeg_2" is not defined
#endif

#ifndef U8ETCSCpIgFacGrChgErrNeg_3
#error The variable for the parameter "U8ETCSCpIgFacGrChgErrNeg_3" is not defined
#endif

#ifndef U8ETCSCpIgFacGrChgErrNeg_4
#error The variable for the parameter "U8ETCSCpIgFacGrChgErrNeg_4" is not defined
#endif

#ifndef U8ETCSCpIgFacGrChgErrNeg_5
#error The variable for the parameter "U8ETCSCpIgFacGrChgErrNeg_5" is not defined
#endif

#ifndef U8ETCSCpIgFacInTrnAsp_1
#error The variable for the parameter "U8ETCSCpIgFacInTrnAsp_1" is not defined
#endif

#ifndef U8ETCSCpIgFacInTrnAsp_2
#error The variable for the parameter "U8ETCSCpIgFacInTrnAsp_2" is not defined
#endif

#ifndef U8ETCSCpIgFacInTrnAsp_3
#error The variable for the parameter "U8ETCSCpIgFacInTrnAsp_3" is not defined
#endif

#ifndef U8ETCSCpIgFacInTrnAsp_4
#error The variable for the parameter "U8ETCSCpIgFacInTrnAsp_4" is not defined
#endif

#ifndef U8ETCSCpIgFacInTrnAsp_5
#error The variable for the parameter "U8ETCSCpIgFacInTrnAsp_5" is not defined
#endif

#ifndef U8ETCSCpIgFacInTrnIce_1
#error The variable for the parameter "U8ETCSCpIgFacInTrnIce_1" is not defined
#endif

#ifndef U8ETCSCpIgFacInTrnIce_2
#error The variable for the parameter "U8ETCSCpIgFacInTrnIce_2" is not defined
#endif

#ifndef U8ETCSCpIgFacInTrnIce_3
#error The variable for the parameter "U8ETCSCpIgFacInTrnIce_3" is not defined
#endif

#ifndef U8ETCSCpIgFacInTrnIce_4
#error The variable for the parameter "U8ETCSCpIgFacInTrnIce_4" is not defined
#endif

#ifndef U8ETCSCpIgFacInTrnIce_5
#error The variable for the parameter "U8ETCSCpIgFacInTrnIce_5" is not defined
#endif

#ifndef U8ETCSCpIgFacInTrnSnw_1
#error The variable for the parameter "U8ETCSCpIgFacInTrnSnw_1" is not defined
#endif

#ifndef U8ETCSCpIgFacInTrnSnw_2
#error The variable for the parameter "U8ETCSCpIgFacInTrnSnw_2" is not defined
#endif

#ifndef U8ETCSCpIgFacInTrnSnw_3
#error The variable for the parameter "U8ETCSCpIgFacInTrnSnw_3" is not defined
#endif

#ifndef U8ETCSCpIgFacInTrnSnw_4
#error The variable for the parameter "U8ETCSCpIgFacInTrnSnw_4" is not defined
#endif

#ifndef U8ETCSCpIgFacInTrnSnw_5
#error The variable for the parameter "U8ETCSCpIgFacInTrnSnw_5" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnAsp_1
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnAsp_1" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnAsp_2
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnAsp_2" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnAsp_3
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnAsp_3" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnAsp_4
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnAsp_4" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnAsp_5
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnAsp_5" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnIce_1
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnIce_1" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnIce_2
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnIce_2" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnIce_3
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnIce_3" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnIce_4
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnIce_4" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnIce_5
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnIce_5" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnSnow_1
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnSnow_1" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnSnow_2
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnSnow_2" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnSnow_3
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnSnow_3" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnSnow_4
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnSnow_4" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnSnow_5
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnSnow_5" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainAsp_1
#error The variable for the parameter "U8ETCSCpNegErrIgainAsp_1" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainAsp_2
#error The variable for the parameter "U8ETCSCpNegErrIgainAsp_2" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainAsp_3
#error The variable for the parameter "U8ETCSCpNegErrIgainAsp_3" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainAsp_4
#error The variable for the parameter "U8ETCSCpNegErrIgainAsp_4" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainAsp_5
#error The variable for the parameter "U8ETCSCpNegErrIgainAsp_5" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainIce_1
#error The variable for the parameter "U8ETCSCpNegErrIgainIce_1" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainIce_2
#error The variable for the parameter "U8ETCSCpNegErrIgainIce_2" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainIce_3
#error The variable for the parameter "U8ETCSCpNegErrIgainIce_3" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainIce_4
#error The variable for the parameter "U8ETCSCpNegErrIgainIce_4" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainIce_5
#error The variable for the parameter "U8ETCSCpNegErrIgainIce_5" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainSnw_1
#error The variable for the parameter "U8ETCSCpNegErrIgainSnw_1" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainSnw_2
#error The variable for the parameter "U8ETCSCpNegErrIgainSnw_2" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainSnw_3
#error The variable for the parameter "U8ETCSCpNegErrIgainSnw_3" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainSnw_4
#error The variable for the parameter "U8ETCSCpNegErrIgainSnw_4" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainSnw_5
#error The variable for the parameter "U8ETCSCpNegErrIgainSnw_5" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainSplt_1
#error The variable for the parameter "U8ETCSCpNegErrIgainSplt_1" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainSplt_2
#error The variable for the parameter "U8ETCSCpNegErrIgainSplt_2" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainSplt_3
#error The variable for the parameter "U8ETCSCpNegErrIgainSplt_3" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainSplt_4
#error The variable for the parameter "U8ETCSCpNegErrIgainSplt_4" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainSplt_5
#error The variable for the parameter "U8ETCSCpNegErrIgainSplt_5" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainAsp_1
#error The variable for the parameter "U8ETCSCpNegErrPgainAsp_1" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainAsp_2
#error The variable for the parameter "U8ETCSCpNegErrPgainAsp_2" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainAsp_3
#error The variable for the parameter "U8ETCSCpNegErrPgainAsp_3" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainAsp_4
#error The variable for the parameter "U8ETCSCpNegErrPgainAsp_4" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainAsp_5
#error The variable for the parameter "U8ETCSCpNegErrPgainAsp_5" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainIce_1
#error The variable for the parameter "U8ETCSCpNegErrPgainIce_1" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainIce_2
#error The variable for the parameter "U8ETCSCpNegErrPgainIce_2" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainIce_3
#error The variable for the parameter "U8ETCSCpNegErrPgainIce_3" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainIce_4
#error The variable for the parameter "U8ETCSCpNegErrPgainIce_4" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainIce_5
#error The variable for the parameter "U8ETCSCpNegErrPgainIce_5" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSnw_1
#error The variable for the parameter "U8ETCSCpNegErrPgainSnw_1" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSnw_2
#error The variable for the parameter "U8ETCSCpNegErrPgainSnw_2" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSnw_3
#error The variable for the parameter "U8ETCSCpNegErrPgainSnw_3" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSnw_4
#error The variable for the parameter "U8ETCSCpNegErrPgainSnw_4" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSnw_5
#error The variable for the parameter "U8ETCSCpNegErrPgainSnw_5" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSplt_1
#error The variable for the parameter "U8ETCSCpNegErrPgainSplt_1" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSplt_2
#error The variable for the parameter "U8ETCSCpNegErrPgainSplt_2" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSplt_3
#error The variable for the parameter "U8ETCSCpNegErrPgainSplt_3" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSplt_4
#error The variable for the parameter "U8ETCSCpNegErrPgainSplt_4" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSplt_5
#error The variable for the parameter "U8ETCSCpNegErrPgainSplt_5" is not defined
#endif

#ifndef U8ETCSCpPGainIncFacH2L
#error The variable for the parameter "U8ETCSCpPGainIncFacH2L" is not defined
#endif

#ifndef U8ETCSCpPgFacGrChgErrNeg_1
#error The variable for the parameter "U8ETCSCpPgFacGrChgErrNeg_1" is not defined
#endif

#ifndef U8ETCSCpPgFacGrChgErrNeg_2
#error The variable for the parameter "U8ETCSCpPgFacGrChgErrNeg_2" is not defined
#endif

#ifndef U8ETCSCpPgFacGrChgErrNeg_3
#error The variable for the parameter "U8ETCSCpPgFacGrChgErrNeg_3" is not defined
#endif

#ifndef U8ETCSCpPgFacGrChgErrNeg_4
#error The variable for the parameter "U8ETCSCpPgFacGrChgErrNeg_4" is not defined
#endif

#ifndef U8ETCSCpPgFacGrChgErrNeg_5
#error The variable for the parameter "U8ETCSCpPgFacGrChgErrNeg_5" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnAsp_1
#error The variable for the parameter "U8ETCSCpPgFacInTrnAsp_1" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnAsp_2
#error The variable for the parameter "U8ETCSCpPgFacInTrnAsp_2" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnAsp_3
#error The variable for the parameter "U8ETCSCpPgFacInTrnAsp_3" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnAsp_4
#error The variable for the parameter "U8ETCSCpPgFacInTrnAsp_4" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnAsp_5
#error The variable for the parameter "U8ETCSCpPgFacInTrnAsp_5" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnIce_1
#error The variable for the parameter "U8ETCSCpPgFacInTrnIce_1" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnIce_2
#error The variable for the parameter "U8ETCSCpPgFacInTrnIce_2" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnIce_3
#error The variable for the parameter "U8ETCSCpPgFacInTrnIce_3" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnIce_4
#error The variable for the parameter "U8ETCSCpPgFacInTrnIce_4" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnIce_5
#error The variable for the parameter "U8ETCSCpPgFacInTrnIce_5" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnSnw_1
#error The variable for the parameter "U8ETCSCpPgFacInTrnSnw_1" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnSnw_2
#error The variable for the parameter "U8ETCSCpPgFacInTrnSnw_2" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnSnw_3
#error The variable for the parameter "U8ETCSCpPgFacInTrnSnw_3" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnSnw_4
#error The variable for the parameter "U8ETCSCpPgFacInTrnSnw_4" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnSnw_5
#error The variable for the parameter "U8ETCSCpPgFacInTrnSnw_5" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainAsp_1
#error The variable for the parameter "U8ETCSCpPosErrIgainAsp_1" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainAsp_2
#error The variable for the parameter "U8ETCSCpPosErrIgainAsp_2" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainAsp_3
#error The variable for the parameter "U8ETCSCpPosErrIgainAsp_3" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainAsp_4
#error The variable for the parameter "U8ETCSCpPosErrIgainAsp_4" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainAsp_5
#error The variable for the parameter "U8ETCSCpPosErrIgainAsp_5" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainIce_1
#error The variable for the parameter "U8ETCSCpPosErrIgainIce_1" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainIce_2
#error The variable for the parameter "U8ETCSCpPosErrIgainIce_2" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainIce_3
#error The variable for the parameter "U8ETCSCpPosErrIgainIce_3" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainIce_4
#error The variable for the parameter "U8ETCSCpPosErrIgainIce_4" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainIce_5
#error The variable for the parameter "U8ETCSCpPosErrIgainIce_5" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainSnw_1
#error The variable for the parameter "U8ETCSCpPosErrIgainSnw_1" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainSnw_2
#error The variable for the parameter "U8ETCSCpPosErrIgainSnw_2" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainSnw_3
#error The variable for the parameter "U8ETCSCpPosErrIgainSnw_3" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainSnw_4
#error The variable for the parameter "U8ETCSCpPosErrIgainSnw_4" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainSnw_5
#error The variable for the parameter "U8ETCSCpPosErrIgainSnw_5" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainSplt_1
#error The variable for the parameter "U8ETCSCpPosErrIgainSplt_1" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainSplt_2
#error The variable for the parameter "U8ETCSCpPosErrIgainSplt_2" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainSplt_3
#error The variable for the parameter "U8ETCSCpPosErrIgainSplt_3" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainSplt_4
#error The variable for the parameter "U8ETCSCpPosErrIgainSplt_4" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainSplt_5
#error The variable for the parameter "U8ETCSCpPosErrIgainSplt_5" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainAsp_1
#error The variable for the parameter "U8ETCSCpPosErrPgainAsp_1" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainAsp_2
#error The variable for the parameter "U8ETCSCpPosErrPgainAsp_2" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainAsp_3
#error The variable for the parameter "U8ETCSCpPosErrPgainAsp_3" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainAsp_4
#error The variable for the parameter "U8ETCSCpPosErrPgainAsp_4" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainAsp_5
#error The variable for the parameter "U8ETCSCpPosErrPgainAsp_5" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainIce_1
#error The variable for the parameter "U8ETCSCpPosErrPgainIce_1" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainIce_2
#error The variable for the parameter "U8ETCSCpPosErrPgainIce_2" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainIce_3
#error The variable for the parameter "U8ETCSCpPosErrPgainIce_3" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainIce_4
#error The variable for the parameter "U8ETCSCpPosErrPgainIce_4" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainIce_5
#error The variable for the parameter "U8ETCSCpPosErrPgainIce_5" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSnw_1
#error The variable for the parameter "U8ETCSCpPosErrPgainSnw_1" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSnw_2
#error The variable for the parameter "U8ETCSCpPosErrPgainSnw_2" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSnw_3
#error The variable for the parameter "U8ETCSCpPosErrPgainSnw_3" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSnw_4
#error The variable for the parameter "U8ETCSCpPosErrPgainSnw_4" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSnw_5
#error The variable for the parameter "U8ETCSCpPosErrPgainSnw_5" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSplt_1
#error The variable for the parameter "U8ETCSCpPosErrPgainSplt_1" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSplt_2
#error The variable for the parameter "U8ETCSCpPosErrPgainSplt_2" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSplt_3
#error The variable for the parameter "U8ETCSCpPosErrPgainSplt_3" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSplt_4
#error The variable for the parameter "U8ETCSCpPosErrPgainSplt_4" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSplt_5
#error The variable for the parameter "U8ETCSCpPosErrPgainSplt_5" is not defined
#endif

#ifndef U8ETCSCpTarTransCplBrkTrq2Sp
#error The variable for the parameter "U8ETCSCpTarTransCplBrkTrq2Sp" is not defined
#endif

#ifndef U8ETCSCpTarTransStBrkTrq2Sp
#error The variable for the parameter "U8ETCSCpTarTransStBrkTrq2Sp" is not defined
#endif

#ifndef VREF_7_KPH
#error The variable for the parameter "VREF_7_KPH" is not defined
#endif
#endif                                 /* RTW_HEADER_LCETCS_vCalCtlGain_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
