/*
 * File: LCETCS_vCalIVal2Rst.c
 *
 * Code generated for Simulink model 'LCETCS_vCalIVal2Rst'.
 *
 * Model version                  : 1.222
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:13:00 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalIVal2Rst.h"
#include "LCETCS_vCalIVal2Rst_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalIVal2Rst' */
void LCETCS_vCalIVal2Rst_Init(B_LCETCS_vCalIVal2Rst_c_T *localB,
  DW_LCETCS_vCalIVal2Rst_f_T *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalIValAtErrZroCrs' */
  LCETCS_vCalIValAtErrZroCrs_Init(&localB->lcetcss32IValAtErrZroCrs,
    &localB->lcetcsu1RstIValAtErrZroCrs);

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctMisIValAtStrt' */
  LCETCS_vDctMisIValAtStrt_Init(&localB->lcetcsu1IValRecTorqAct,
    &(localDW->LCETCS_vDctMisIValAtStrt_DWORK1.rtb),
    &(localDW->LCETCS_vDctMisIValAtStrt_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalRefTrqAtVcaOfCtl' */
  LCETCS_vCalRefTrqAtVcaOfCtl_Init
    (&(localDW->LCETCS_vCalRefTrqAtVcaOfCtl_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vSelIVal2Rst' */
  LCETCS_vSelIVal2Rst_Init(&localB->lcetcss32IVal2Rst, &localB->lcetcsu1RstIVal);
}

/* Start for referenced model: 'LCETCS_vCalIVal2Rst' */
void LCETCS_vCalIVal2Rst_Start(DW_LCETCS_vCalIVal2Rst_f_T *localDW)
{
  /* Start for ModelReference: '<Root>/LCETCS_vCalRefTrqAtVcaOfCtl' */
  LCETCS_vCalRefTrqAtVcaOfCtl_Start
    (&(localDW->LCETCS_vCalRefTrqAtVcaOfCtl_DWORK1.rtdw));
}

/* Output and update for referenced model: 'LCETCS_vCalIVal2Rst' */
void LCETCS_vCalIVal2Rst(const TypeETCSRefTrq2JmpStruct *rtu_ETCS_REF_TRQ2JMP,
  const TypeETCSCtlCmdStruct *rtu_ETCS_CTL_CMD_OLD, const
  TypeETCSRefTrqStrCtlStruct *rtu_ETCS_REF_TRQ_STR_CTL, const
  TypeETCSErrZroCrsStruct *rtu_ETCS_ERR_ZRO_CRS, const TypeETCSCtlActStruct
  *rtu_ETCS_CTL_ACT, const TypeETCSRefTrqStr2CylStruct *rtu_ETCS_REF_TRQ_STR2CYL,
  const TypeETCSCyl1stStruct *rtu_ETCS_CYL_1ST, const TypeETCSVehAccStruct
  *rtu_ETCS_VEH_ACC, const TypeETCSHighDctStruct *rtu_ETCS_HI_DCT, const
  TypeETCSReqVCAStruct *rtu_ETCS_REQ_VCA, const TypeETCSEscSigStruct
  *rtu_ETCS_ESC_SIG, const TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN, const
  TypeETCSCrdnTrqStruct *rtu_ETCS_CRDN_TRQ, TypeETCSIValRstStruct
  *rty_ETCS_IVAL_RST, B_LCETCS_vCalIVal2Rst_c_T *localB,
  DW_LCETCS_vCalIVal2Rst_f_T *localDW)
{
  /* local block i/o variables */
  int32_T rtb_lcetcss32IValRecTorq;
  int32_T rtb_lcetcss32IValVcaTorq;
  boolean_T rtb_lcetcsu1RstIValAt2ndCylStrt;
  boolean_T rtb_lcetcsu1IValVcaTorqAct;
  boolean_T rtb_lcetcsu1RstIValAt2ndCylStrt_f;
  boolean_T rtb_lcetcsu1RstIValAtErrZroCrs;
  int32_T lcetcss32IValAtStrtOfCtl_f;
  int32_T lcetcss32IValAt2ndCylStrt_c;

  /* ModelReference: '<Root>/LCETCS_vCalIValAtStrtOfCtl' */
  LCETCS_vCalIValAtStrtOfCtl(rtu_ETCS_CTL_ACT, rtu_ETCS_REF_TRQ_STR_CTL,
    rtu_ETCS_CRDN_TRQ, &localB->lcetcss32IValAtStrtOfCtl);

  /* SignalConversion: '<Root>/Signal Conversion1' */
  lcetcss32IValAtStrtOfCtl_f = localB->lcetcss32IValAtStrtOfCtl;

  /* ModelReference: '<Root>/LCETCS_vCalIValAt2ndCylStrt' */
  LCETCS_vCalIValAt2ndCylStrt(rtu_ETCS_CTL_CMD_OLD, rtu_ETCS_REF_TRQ_STR2CYL,
    rtu_ETCS_CYL_1ST, rtu_ETCS_ERR_ZRO_CRS, &localB->lcetcss32IValAt2ndCylStrt,
    &rtb_lcetcsu1RstIValAt2ndCylStrt);

  /* SignalConversion: '<Root>/Signal Conversion2' */
  lcetcss32IValAt2ndCylStrt_c = localB->lcetcss32IValAt2ndCylStrt;

  /* SignalConversion: '<Root>/Signal Conversion3' */
  rtb_lcetcsu1RstIValAt2ndCylStrt_f = rtb_lcetcsu1RstIValAt2ndCylStrt;

  /* ModelReference: '<Root>/LCETCS_vCalIValAtErrZroCrs' */
  LCETCS_vCalIValAtErrZroCrs(rtb_lcetcsu1RstIValAt2ndCylStrt,
    rtu_ETCS_CTL_CMD_OLD, rtu_ETCS_ERR_ZRO_CRS, rtu_ETCS_REF_TRQ2JMP,
    rtu_ETCS_ESC_SIG, rtu_ETCS_TAR_SPIN, &localB->lcetcss32IValAtErrZroCrs,
    &localB->lcetcsu1RstIValAtErrZroCrs);

  /* SignalConversion: '<Root>/Signal Conversion4' */
  rty_ETCS_IVAL_RST->lcetcss32IValAtErrZroCrs = localB->lcetcss32IValAtErrZroCrs;

  /* SignalConversion: '<Root>/Signal Conversion5' */
  rtb_lcetcsu1RstIValAtErrZroCrs = localB->lcetcsu1RstIValAtErrZroCrs;

  /* ModelReference: '<Root>/LCETCS_vDctMisIValAtStrt' */
  LCETCS_vDctMisIValAtStrt(rtu_ETCS_CTL_ACT, localB->lcetcss32IValAtStrtOfCtl,
    rtu_ETCS_HI_DCT, rtu_ETCS_CTL_CMD_OLD, &localB->lcetcsu1IValRecTorqAct,
    &rtb_lcetcss32IValRecTorq, &(localDW->LCETCS_vDctMisIValAtStrt_DWORK1.rtb),
    &(localDW->LCETCS_vDctMisIValAtStrt_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalRefTrqAtVcaOfCtl' */
  LCETCS_vCalRefTrqAtVcaOfCtl(rtu_ETCS_CTL_CMD_OLD, rtu_ETCS_VEH_ACC,
    rtu_ETCS_REQ_VCA, &rtb_lcetcsu1IValVcaTorqAct, &rtb_lcetcss32IValVcaTorq,
    &(localDW->LCETCS_vCalRefTrqAtVcaOfCtl_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vSelIVal2Rst' */
  LCETCS_vSelIVal2Rst(localB->lcetcss32IValAtStrtOfCtl, rtu_ETCS_CTL_ACT,
                      localB->lcetcss32IValAt2ndCylStrt,
                      rtb_lcetcsu1RstIValAt2ndCylStrt,
                      localB->lcetcss32IValAtErrZroCrs,
                      localB->lcetcsu1RstIValAtErrZroCrs,
                      localB->lcetcsu1IValRecTorqAct, rtb_lcetcss32IValRecTorq,
                      rtb_lcetcsu1IValVcaTorqAct, rtb_lcetcss32IValVcaTorq,
                      &localB->lcetcss32IVal2Rst, &localB->lcetcsu1RstIVal);

  /* SignalConversion: '<Root>/Signal Conversion6' */
  rty_ETCS_IVAL_RST->lcetcss32IVal2Rst = localB->lcetcss32IVal2Rst;

  /* SignalConversion: '<Root>/Signal Conversion7' */
  rty_ETCS_IVAL_RST->lcetcsu1IValRecTorqAct = localB->lcetcsu1IValRecTorqAct;

  /* SignalConversion: '<Root>/Signal Conversion8' */
  rty_ETCS_IVAL_RST->lcetcss32IValRecTorq = rtb_lcetcss32IValRecTorq;

  /* SignalConversion: '<Root>/Signal Conversion9' */
  rty_ETCS_IVAL_RST->lcetcsu1IValVcaTorqAct = rtb_lcetcsu1IValVcaTorqAct;

  /* SignalConversion: '<Root>/Signal Conversion10' */
  rty_ETCS_IVAL_RST->lcetcss32IValVcaTorq = rtb_lcetcss32IValVcaTorq;

  /* BusCreator: '<Root>/Bus Creator1' */
  rty_ETCS_IVAL_RST->lcetcss32IValAtStrtOfCtl = lcetcss32IValAtStrtOfCtl_f;
  rty_ETCS_IVAL_RST->lcetcss32IValAt2ndCylStrt = lcetcss32IValAt2ndCylStrt_c;
  rty_ETCS_IVAL_RST->lcetcsu1RstIValAt2ndCylStrt =
    rtb_lcetcsu1RstIValAt2ndCylStrt_f;
  rty_ETCS_IVAL_RST->lcetcsu1RstIValAtErrZroCrs = rtb_lcetcsu1RstIValAtErrZroCrs;
  rty_ETCS_IVAL_RST->lcetcsu1RstIVal = localB->lcetcsu1RstIVal;
}

/* Model initialize function */
void LCETCS_vCalIVal2Rst_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalIValAt2ndCylStrt' */
  LCETCS_vCalIValAt2ndCylStrt_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalIValAtErrZroCrs' */
  LCETCS_vCalIValAtErrZroCrs_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalIValAtStrtOfCtl' */
  LCETCS_vCalIValAtStrtOfCtl_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalRefTrqAtVcaOfCtl' */
  LCETCS_vCalRefTrqAtVcaOfCtl_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctMisIValAtStrt' */
  LCETCS_vDctMisIValAtStrt_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vSelIVal2Rst' */
  LCETCS_vSelIVal2Rst_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
