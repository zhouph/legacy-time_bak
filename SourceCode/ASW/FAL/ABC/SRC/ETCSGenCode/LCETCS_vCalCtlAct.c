/*
 * File: LCETCS_vCalCtlAct.c
 *
 * Code generated for Simulink model 'LCETCS_vCalCtlAct'.
 *
 * Model version                  : 1.207
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:04:57 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalCtlAct.h"
#include "LCETCS_vCalCtlAct_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalCtlAct' */
void LCETCS_vCalCtlAct_Init(boolean_T *rty_lcetcsu1CtlAct, boolean_T
  *rty_lcetcsu1CtlActRsgEdg, DW_LCETCS_vCalCtlAct_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = false;

  /* InitializeConditions for Chart: '<Root>/Chart' */
  *rty_lcetcsu1CtlAct = false;

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctRisingEdge' */
  LCETCS_vDctRisingEdge_Init(rty_lcetcsu1CtlActRsgEdg,
    &(localDW->LCETCS_vDctRisingEdge_DWORK1.rtdw));
}

/* Output and update for referenced model: 'LCETCS_vCalCtlAct' */
void LCETCS_vCalCtlAct(const TypeETCSEngStallStruct *rtu_ETCS_ENG_STALL,
  boolean_T rtu_lcetcsu1DriverAccelIntend, boolean_T
  rtu_lcetcsu1DriverAccelHysIntend, int16_T rtu_lcetcss16OvrWhlSpnTm, const
  TypeETCSEngCmdStruct *rtu_ETCS_ENG_CMD_OLD, int16_T rtu_lcetcss16OvrWhlSpnTmTh,
  const TypeETCSEngStruct *rtu_ETCS_ENG_STRUCT, int16_T
  rtu_lcetcss16OvrEngTrqCmdTm, const TypeETCSCtrlModeStruct *rtu_ETCS_CTRL_MODE,
  int16_T rtu_lcetcss16LmtCnrgCtlEntrTm, boolean_T *rty_lcetcsu1CtlAct,
  boolean_T *rty_lcetcsu1CtlActRsgEdg, DW_LCETCS_vCalCtlAct_f_T *localDW)
{
  /* Chart: '<Root>/Chart' incorporates:
   *  UnitDelay: '<Root>/Unit Delay1'
   */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:70' */
  if (rtu_ETCS_CTRL_MODE->lcetcsu8EngCtrlMode == ((uint8_T)TCSDpCtrlModeDisable))
  {
    /* Transition: '<S1>:74' */
    /* Transition: '<S1>:78' */
    *rty_lcetcsu1CtlAct = false;

    /* Transition: '<S1>:79' */
    /* Transition: '<S1>:44' */
    /* Transition: '<S1>:65' */
    /* Transition: '<S1>:8' */
    /* Transition: '<S1>:32' */
    /* Transition: '<S1>:30' */
  } else {
    /* Transition: '<S1>:75' */
    /*  Check control activation conditions  */
    if (localDW->UnitDelay1_DSTATE == 0) {
      /* Transition: '<S1>:62' */
      /* Transition: '<S1>:61' */
      if ((((rtu_lcetcss16OvrWhlSpnTm >= rtu_lcetcss16OvrWhlSpnTmTh) ||
            (rtu_lcetcss16LmtCnrgCtlEntrTm >= ((uint8_T)
              U8ETCSCpCtlActTmThInLmtCnrg))) && (rtu_lcetcsu1DriverAccelIntend ==
            1)) && (rtu_ETCS_ENG_STALL->lcetcsu1EngStall == 0)) {
        /* Transition: '<S1>:37' */
        /* Transition: '<S1>:43' */
        *rty_lcetcsu1CtlAct = true;

        /* Transition: '<S1>:44' */
        /* Transition: '<S1>:65' */
        /* Transition: '<S1>:8' */
        /* Transition: '<S1>:32' */
        /* Transition: '<S1>:30' */
      } else {
        /* Transition: '<S1>:40' */
        *rty_lcetcsu1CtlAct = localDW->UnitDelay1_DSTATE;

        /* Transition: '<S1>:65' */
        /* Transition: '<S1>:8' */
        /* Transition: '<S1>:32' */
        /* Transition: '<S1>:30' */
      }
    } else {
      /* Transition: '<S1>:64' */
      /*  Check control deactivation conditions  */
      if (rtu_lcetcsu1DriverAccelHysIntend == 1) {
        /* Transition: '<S1>:10' */
        /* Transition: '<S1>:21' */
        if (rtu_lcetcss16OvrEngTrqCmdTm >= ((int8_T)S8ETCSCpCtlExitCntTh)) {
          /* Transition: '<S1>:23' */
          /* Transition: '<S1>:9' */
          *rty_lcetcsu1CtlAct = false;

          /* Transition: '<S1>:8' */
          /* Transition: '<S1>:32' */
          /* Transition: '<S1>:30' */
        } else {
          /* Transition: '<S1>:33' */
          *rty_lcetcsu1CtlAct = localDW->UnitDelay1_DSTATE;

          /* Transition: '<S1>:32' */
          /* Transition: '<S1>:30' */
        }
      } else {
        /* Transition: '<S1>:25' */
        if (rtu_ETCS_ENG_CMD_OLD->lcetcss16EngTrqCmd >=
            rtu_ETCS_ENG_STRUCT->lcetcss16DrvReqTrq) {
          /* Transition: '<S1>:27' */
          /* Transition: '<S1>:29' */
          *rty_lcetcsu1CtlAct = false;

          /* Transition: '<S1>:30' */
        } else {
          /* Transition: '<S1>:7' */
          *rty_lcetcsu1CtlAct = localDW->UnitDelay1_DSTATE;
        }
      }
    }
  }

  /* End of Chart: '<Root>/Chart' */
  /* Transition: '<S1>:6' */

  /* ModelReference: '<Root>/LCETCS_vDctRisingEdge' */
  LCETCS_vDctRisingEdge((*rty_lcetcsu1CtlAct), rty_lcetcsu1CtlActRsgEdg,
                        &(localDW->LCETCS_vDctRisingEdge_DWORK1.rtdw));

  /* Update for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = *rty_lcetcsu1CtlAct;
}

/* Model initialize function */
void LCETCS_vCalCtlAct_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctRisingEdge' */
  LCETCS_vDctRisingEdge_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
