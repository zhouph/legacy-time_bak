/*
 * File: LCETCS_vCalRefAxRepRdFric.c
 *
 * Code generated for Simulink model 'LCETCS_vCalRefAxRepRdFric'.
 *
 * Model version                  : 1.511
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:00:37 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalRefAxRepRdFric.h"
#include "LCETCS_vCalRefAxRepRdFric_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalRefAxRepRdFric' */
void LCETCS_vCalRefAxRepRdFric_Init(B_LCETCS_vCalRefAxRepRdFric_c_T *localB)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter5Point' */
  LCETCS_s16Inter5Point_Init(&localB->y);

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter5Point1' */
  LCETCS_s16Inter5Point_Init(&localB->y_k);

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter5Point2' */
  LCETCS_s16Inter5Point_Init(&localB->y_o);
}

/* Start for referenced model: 'LCETCS_vCalRefAxRepRdFric' */
void LCETCS_vCalRefAxRepRdFric_Start(B_LCETCS_vCalRefAxRepRdFric_c_T *localB)
{
  /* Start for Constant: '<Root>/Ax  for 1st gear ratio on Asphalt' */
  localB->y5 = ((uint8_T)U8ETCSCpAxAtAsphalt);

  /* Start for Constant: '<Root>/Ax  for 1st gear ratio on Ice' */
  localB->y5_l = ((uint8_T)U8ETCSCpAxAtIce);

  /* Start for Constant: '<Root>/Ax  for 1st gear ratio on Snow' */
  localB->y5_lj = ((uint8_T)U8ETCSCpAxAtSnow);

  /* Start for Constant: '<Root>/Ax  for 2nd gear ratio on Asphalt' */
  localB->y4 = ((uint8_T)U8ETCSCpAxGear2AtAsphalt);

  /* Start for Constant: '<Root>/Ax  for 2nd gear ratio on Ice' */
  localB->y4_d = ((uint8_T)U8ETCSCpAxGear2AtIce);

  /* Start for Constant: '<Root>/Ax  for 2nd gear ratio on Snow' */
  localB->y4_n = ((uint8_T)U8ETCSCpAxGear2AtSnow);

  /* Start for Constant: '<Root>/Ax  for 3rd gear ratio on Asphalt' */
  localB->y3 = ((uint8_T)U8ETCSCpAxGear3AtAsphalt);

  /* Start for Constant: '<Root>/Ax  for 3rd gear ratio on Ice' */
  localB->y3_a = ((uint8_T)U8ETCSCpAxGear3AtIce);

  /* Start for Constant: '<Root>/Ax  for 3rd gear ratio on Snow' */
  localB->y3_h = ((uint8_T)U8ETCSCpAxGear3AtSnow);

  /* Start for Constant: '<Root>/Ax  for 4th gear ratio on Asphalt' */
  localB->y2 = ((uint8_T)U8ETCSCpAxGear4AtAsphalt);

  /* Start for Constant: '<Root>/Ax  for 4th gear ratio on Ice' */
  localB->y2_i = ((uint8_T)U8ETCSCpAxGear4AtIce);

  /* Start for Constant: '<Root>/Ax  for 4th gear ratio on Snow' */
  localB->y2_l = ((uint8_T)U8ETCSCpAxGear4AtSnow);

  /* Start for Constant: '<Root>/Ax  for 5th gear ratio on Asphalt' */
  localB->y1 = ((uint8_T)U8ETCSCpAxGear5AtAsphalt);

  /* Start for Constant: '<Root>/Ax  for 5th gear ratio on Ice' */
  localB->y1_c = ((uint8_T)U8ETCSCpAxGear5AtIce);

  /* Start for Constant: '<Root>/Ax  for 5th gear ratio on Snow' */
  localB->y1_b = ((uint8_T)U8ETCSCpAxGear5AtSnow);
}

/* Output and update for referenced model: 'LCETCS_vCalRefAxRepRdFric' */
void LCETCS_vCalRefAxRepRdFric(const TypeETCSDrvMdlStruct *rtu_ETCS_DRV_MDL,
  int16_T *rty_lcetcss16RefAxRdFricAtIce, int16_T
  *rty_lcetcss16RefAxRdFricAtSnow, int16_T *rty_lcetcss16RefAxRdFricAtAsphalt,
  B_LCETCS_vCalRefAxRepRdFric_c_T *localB)
{
  /* local block i/o variables */
  int16_T rtb_x;
  int16_T rtb_x_i;
  int16_T rtb_x_h;

  /* Constant: '<Root>/Ax  for 1st gear ratio on Asphalt' */
  localB->y5 = ((uint8_T)U8ETCSCpAxAtAsphalt);

  /* Constant: '<Root>/Ax  for 1st gear ratio on Ice' */
  localB->y5_l = ((uint8_T)U8ETCSCpAxAtIce);

  /* Constant: '<Root>/Ax  for 1st gear ratio on Snow' */
  localB->y5_lj = ((uint8_T)U8ETCSCpAxAtSnow);

  /* Constant: '<Root>/Ax  for 2nd gear ratio on Asphalt' */
  localB->y4 = ((uint8_T)U8ETCSCpAxGear2AtAsphalt);

  /* Constant: '<Root>/Ax  for 2nd gear ratio on Ice' */
  localB->y4_d = ((uint8_T)U8ETCSCpAxGear2AtIce);

  /* Constant: '<Root>/Ax  for 2nd gear ratio on Snow' */
  localB->y4_n = ((uint8_T)U8ETCSCpAxGear2AtSnow);

  /* Constant: '<Root>/Ax  for 3rd gear ratio on Asphalt' */
  localB->y3 = ((uint8_T)U8ETCSCpAxGear3AtAsphalt);

  /* Constant: '<Root>/Ax  for 3rd gear ratio on Ice' */
  localB->y3_a = ((uint8_T)U8ETCSCpAxGear3AtIce);

  /* Constant: '<Root>/Ax  for 3rd gear ratio on Snow' */
  localB->y3_h = ((uint8_T)U8ETCSCpAxGear3AtSnow);

  /* Constant: '<Root>/Ax  for 4th gear ratio on Asphalt' */
  localB->y2 = ((uint8_T)U8ETCSCpAxGear4AtAsphalt);

  /* Constant: '<Root>/Ax  for 4th gear ratio on Ice' */
  localB->y2_i = ((uint8_T)U8ETCSCpAxGear4AtIce);

  /* Constant: '<Root>/Ax  for 4th gear ratio on Snow' */
  localB->y2_l = ((uint8_T)U8ETCSCpAxGear4AtSnow);

  /* Constant: '<Root>/Ax  for 5th gear ratio on Asphalt' */
  localB->y1 = ((uint8_T)U8ETCSCpAxGear5AtAsphalt);

  /* Constant: '<Root>/Ax  for 5th gear ratio on Ice' */
  localB->y1_c = ((uint8_T)U8ETCSCpAxGear5AtIce);

  /* Constant: '<Root>/Ax  for 5th gear ratio on Snow' */
  localB->y1_b = ((uint8_T)U8ETCSCpAxGear5AtSnow);

  /* Saturate: '<Root>/Saturation1' */
  if (rtu_ETCS_DRV_MDL->lcetcss16TotalGearRatio > 10000) {
    rtb_x = 10000;
  } else if (rtu_ETCS_DRV_MDL->lcetcss16TotalGearRatio < 1) {
    rtb_x = 1;
  } else {
    rtb_x = rtu_ETCS_DRV_MDL->lcetcss16TotalGearRatio;
  }

  /* End of Saturate: '<Root>/Saturation1' */

  /* ModelReference: '<Root>/LCETCS_s16Inter5Point' */
  LCETCS_s16Inter5Point(rtb_x, ((int16_T)S16ETCSCpTotalGearRatio_5), ((int16_T)
    S16ETCSCpTotalGearRatio_4), ((int16_T)S16ETCSCpTotalGearRatio_3), ((int16_T)
    S16ETCSCpTotalGearRatio_2), ((int16_T)S16ETCSCpTotalGearRatio_1),
                        localB->y1_c, localB->y2_i, localB->y3_a, localB->y4_d,
                        localB->y5_l, &localB->y);

  /* Saturate: '<Root>/Saturation2' */
  if (rtu_ETCS_DRV_MDL->lcetcss16TotalGearRatio > 10000) {
    rtb_x_i = 10000;
  } else if (rtu_ETCS_DRV_MDL->lcetcss16TotalGearRatio < 1) {
    rtb_x_i = 1;
  } else {
    rtb_x_i = rtu_ETCS_DRV_MDL->lcetcss16TotalGearRatio;
  }

  /* End of Saturate: '<Root>/Saturation2' */

  /* ModelReference: '<Root>/LCETCS_s16Inter5Point1' */
  LCETCS_s16Inter5Point(rtb_x_i, ((int16_T)S16ETCSCpTotalGearRatio_5), ((int16_T)
    S16ETCSCpTotalGearRatio_4), ((int16_T)S16ETCSCpTotalGearRatio_3), ((int16_T)
    S16ETCSCpTotalGearRatio_2), ((int16_T)S16ETCSCpTotalGearRatio_1),
                        localB->y1_b, localB->y2_l, localB->y3_h, localB->y4_n,
                        localB->y5_lj, &localB->y_k);

  /* Saturate: '<Root>/Saturation3' */
  if (rtu_ETCS_DRV_MDL->lcetcss16TotalGearRatio > 10000) {
    rtb_x_h = 10000;
  } else if (rtu_ETCS_DRV_MDL->lcetcss16TotalGearRatio < 1) {
    rtb_x_h = 1;
  } else {
    rtb_x_h = rtu_ETCS_DRV_MDL->lcetcss16TotalGearRatio;
  }

  /* End of Saturate: '<Root>/Saturation3' */

  /* ModelReference: '<Root>/LCETCS_s16Inter5Point2' */
  LCETCS_s16Inter5Point(rtb_x_h, ((int16_T)S16ETCSCpTotalGearRatio_5), ((int16_T)
    S16ETCSCpTotalGearRatio_4), ((int16_T)S16ETCSCpTotalGearRatio_3), ((int16_T)
    S16ETCSCpTotalGearRatio_2), ((int16_T)S16ETCSCpTotalGearRatio_1), localB->y1,
                        localB->y2, localB->y3, localB->y4, localB->y5,
                        &localB->y_o);

  /* Saturate: '<Root>/Saturation4' */
  if (localB->y > 100) {
    *rty_lcetcss16RefAxRdFricAtIce = 100;
  } else if (localB->y < 0) {
    *rty_lcetcss16RefAxRdFricAtIce = 0;
  } else {
    *rty_lcetcss16RefAxRdFricAtIce = localB->y;
  }

  /* End of Saturate: '<Root>/Saturation4' */

  /* Saturate: '<Root>/Saturation5' */
  if (localB->y_k > 100) {
    *rty_lcetcss16RefAxRdFricAtSnow = 100;
  } else if (localB->y_k < 0) {
    *rty_lcetcss16RefAxRdFricAtSnow = 0;
  } else {
    *rty_lcetcss16RefAxRdFricAtSnow = localB->y_k;
  }

  /* End of Saturate: '<Root>/Saturation5' */

  /* Saturate: '<Root>/Saturation6' */
  if (localB->y_o > 100) {
    *rty_lcetcss16RefAxRdFricAtAsphalt = 100;
  } else if (localB->y_o < 0) {
    *rty_lcetcss16RefAxRdFricAtAsphalt = 0;
  } else {
    *rty_lcetcss16RefAxRdFricAtAsphalt = localB->y_o;
  }

  /* End of Saturate: '<Root>/Saturation6' */
}

/* Model initialize function */
void LCETCS_vCalRefAxRepRdFric_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter5Point' */
  LCETCS_s16Inter5Point_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter5Point1' */
  LCETCS_s16Inter5Point_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter5Point2' */
  LCETCS_s16Inter5Point_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
