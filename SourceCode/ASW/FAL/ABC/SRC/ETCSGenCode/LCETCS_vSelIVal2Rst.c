/*
 * File: LCETCS_vSelIVal2Rst.c
 *
 * Code generated for Simulink model 'LCETCS_vSelIVal2Rst'.
 *
 * Model version                  : 1.167
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:54:27 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vSelIVal2Rst.h"
#include "LCETCS_vSelIVal2Rst_private.h"

/* Initial conditions for referenced model: 'LCETCS_vSelIVal2Rst' */
void LCETCS_vSelIVal2Rst_Init(int32_T *rty_lcetcss32IVal2Rst, boolean_T
  *rty_lcetcsu1RstIVal)
{
  /* InitializeConditions for Chart: '<Root>/Chart' */
  *rty_lcetcss32IVal2Rst = 0;
  *rty_lcetcsu1RstIVal = false;
}

/* Output and update for referenced model: 'LCETCS_vSelIVal2Rst' */
void LCETCS_vSelIVal2Rst(int32_T rtu_lcetcss32IValAtStrtOfCtl, const
  TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, int32_T rtu_lcetcss32IValAt2ndCylStrt,
  boolean_T rtu_lcetcsu1RstIValAt2ndCylStrt, int32_T
  rtu_lcetcss32IValAtErrZroCrs, boolean_T rtu_lcetcsu1RstIValAtErrZroCrs,
  boolean_T rtu_lcetcsu1IValRecTorqAct, int32_T rtu_lcetcss32IValRecTorq,
  boolean_T rtu_lcetcsu1IValVcaTorqAct, int32_T rtu_lcetcss32IValVcaTorq,
  int32_T *rty_lcetcss32IVal2Rst, boolean_T *rty_lcetcsu1RstIVal)
{
  boolean_T lcetcsu1RstIValTemp;

  /* Chart: '<Root>/Chart' */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:12' */
  /* comment */
  if (rtu_ETCS_CTL_ACT->lcetcsu1CtlActRsgEdg == 1) {
    /* Transition: '<S1>:14' */
    /* Transition: '<S1>:19' */
    *rty_lcetcss32IVal2Rst = rtu_lcetcss32IValAtStrtOfCtl;
    lcetcsu1RstIValTemp = true;

    /* Transition: '<S1>:18' */
    /* Transition: '<S1>:17' */
    /* Transition: '<S1>:59' */
    /* Transition: '<S1>:60' */
  } else {
    /* Transition: '<S1>:13' */
    if (rtu_lcetcsu1RstIValAt2ndCylStrt == 1) {
      /* Transition: '<S1>:21' */
      /* Transition: '<S1>:20' */
      *rty_lcetcss32IVal2Rst = rtu_lcetcss32IValAt2ndCylStrt;
      lcetcsu1RstIValTemp = true;

      /* Transition: '<S1>:17' */
      /* Transition: '<S1>:59' */
      /* Transition: '<S1>:60' */
    } else {
      /* Transition: '<S1>:16' */
      if (rtu_lcetcsu1RstIValAtErrZroCrs == 1) {
        /* Transition: '<S1>:22' */
        /* Transition: '<S1>:23' */
        *rty_lcetcss32IVal2Rst = rtu_lcetcss32IValAtErrZroCrs;
        lcetcsu1RstIValTemp = true;

        /* Transition: '<S1>:59' */
        /* Transition: '<S1>:60' */
      } else {
        /* Transition: '<S1>:51' */
        if (rtu_lcetcsu1IValVcaTorqAct == 1) {
          /* Transition: '<S1>:53' */
          /* Transition: '<S1>:58' */
          *rty_lcetcss32IVal2Rst = rtu_lcetcss32IValVcaTorq;
          lcetcsu1RstIValTemp = true;

          /* Transition: '<S1>:60' */
        } else {
          /* Transition: '<S1>:54' */
          *rty_lcetcss32IVal2Rst = 0;
          lcetcsu1RstIValTemp = false;
        }
      }
    }
  }

  /* Transition: '<S1>:15' */
  if ((lcetcsu1RstIValTemp == 1) || (rtu_lcetcsu1IValRecTorqAct == 1)) {
    /* Transition: '<S1>:39' */
    /* Transition: '<S1>:41' */
    *rty_lcetcsu1RstIVal = true;
    if ((*rty_lcetcss32IVal2Rst) >= rtu_lcetcss32IValRecTorq) {
    } else {
      *rty_lcetcss32IVal2Rst = rtu_lcetcss32IValRecTorq;
    }

    /* Transition: '<S1>:45' */
  } else {
    /* Transition: '<S1>:46' */
    *rty_lcetcsu1RstIVal = false;
    *rty_lcetcss32IVal2Rst = 0;
  }

  /* End of Chart: '<Root>/Chart' */
  /* Transition: '<S1>:49' */
}

/* Model initialize function */
void LCETCS_vSelIVal2Rst_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
