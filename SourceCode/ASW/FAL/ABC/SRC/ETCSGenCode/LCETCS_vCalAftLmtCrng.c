/*
 * File: LCETCS_vCalAftLmtCrng.c
 *
 * Code generated for Simulink model 'LCETCS_vCalAftLmtCrng'.
 *
 * Model version                  : 1.105
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:12:50 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalAftLmtCrng.h"
#include "LCETCS_vCalAftLmtCrng_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalAftLmtCrng' */
void LCETCS_vCalAftLmtCrng_Init(B_LCETCS_vCalAftLmtCrng_c_T *localB,
  DW_LCETCS_vCalAftLmtCrng_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = 0;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = 0;

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalInhAftLmtCrng' */
  LCETCS_vCalInhAftLmtCrng_Init(&localB->lcetcsu1InhAftLmtCrng,
    &(localDW->LCETCS_vCalInhAftLmtCrng_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalSetAftLmtCrng' */
  LCETCS_vCalSetAftLmtCrng_Init(&localB->lcetcsu1DctAftLmtCrng,
    &(localDW->LCETCS_vCalSetAftLmtCrng_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalCntAftLmtCrng' */
  LCETCS_vCalCntAftLmtCrng_Init(&(localDW->LCETCS_vCalCntAftLmtCrng_DWORK1.rtdw));
}

/* Start for referenced model: 'LCETCS_vCalAftLmtCrng' */
void LCETCS_vCalAftLmtCrng_Start(DW_LCETCS_vCalAftLmtCrng_f_T *localDW)
{
  /* Start for ModelReference: '<Root>/LCETCS_vCalInhAftLmtCrng' */
  LCETCS_vCalInhAftLmtCrng_Start(&(localDW->LCETCS_vCalInhAftLmtCrng_DWORK1.rtdw));
}

/* Output and update for referenced model: 'LCETCS_vCalAftLmtCrng' */
void LCETCS_vCalAftLmtCrng(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const
  TypeETCSCtlCmdStruct *rtu_ETCS_CTL_CMD, const TypeETCSEscSigStruct
  *rtu_ETCS_ESC_SIG, const TypeETCSCtlErrStruct *rtu_ETCS_CTL_ERR, const
  TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN, TypeETCSAftLmtCrng
  *rty_ETCS_AFT_TURN, B_LCETCS_vCalAftLmtCrng_c_T *localB,
  DW_LCETCS_vCalAftLmtCrng_f_T *localDW)
{
  /* local block i/o variables */
  int16_T rtb_lcetcss16AftLmtCrngCnt;
  int16_T rtb_lcetcss16InhCntAftLmtCrngSpn;
  int16_T rtb_lcetcss16ScaleDnAyMani;
  int16_T rtb_lcetcss16AftLmtCrngCnt_j;
  int16_T rtb_lcetcss16InhCntAftLmtCrngSpn_f;

  /* UnitDelay: '<Root>/Unit Delay2' */
  rtb_lcetcss16AftLmtCrngCnt = localDW->UnitDelay2_DSTATE;

  /* UnitDelay: '<Root>/Unit Delay1' */
  rtb_lcetcss16InhCntAftLmtCrngSpn = localDW->UnitDelay1_DSTATE;

  /* ModelReference: '<Root>/LCETCS_vCalInhAftLmtCrng' */
  LCETCS_vCalInhAftLmtCrng(rtu_ETCS_TAR_SPIN, rtu_ETCS_CTL_ACT, rtu_ETCS_CTL_CMD,
    rtu_ETCS_ESC_SIG, rtb_lcetcss16AftLmtCrngCnt,
    rtb_lcetcss16InhCntAftLmtCrngSpn, &localB->lcetcsu1InhAftLmtCrng,
    &rtb_lcetcss16ScaleDnAyMani, &(localDW->LCETCS_vCalInhAftLmtCrng_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalSetAftLmtCrng' */
  LCETCS_vCalSetAftLmtCrng(rtu_ETCS_TAR_SPIN, localB->lcetcsu1InhAftLmtCrng,
    &localB->lcetcsu1DctAftLmtCrng,
    &(localDW->LCETCS_vCalSetAftLmtCrng_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalCntAftLmtCrng' */
  LCETCS_vCalCntAftLmtCrng(rtu_ETCS_CTL_ERR, localB->lcetcsu1DctAftLmtCrng,
    &rtb_lcetcss16AftLmtCrngCnt_j, &rtb_lcetcss16InhCntAftLmtCrngSpn_f,
    &(localDW->LCETCS_vCalCntAftLmtCrng_DWORK1.rtdw));

  /* BusCreator: '<Root>/Bus Creator2' */
  rty_ETCS_AFT_TURN->lcetcss16AftLmtCrngCnt = rtb_lcetcss16AftLmtCrngCnt_j;
  rty_ETCS_AFT_TURN->lcetcsu1DctAftLmtCrng = localB->lcetcsu1DctAftLmtCrng;
  rty_ETCS_AFT_TURN->lcetcsu1InhAftLmtCrng = localB->lcetcsu1InhAftLmtCrng;
  rty_ETCS_AFT_TURN->lcetcss16InhCntAftLmtCrngSpn =
    rtb_lcetcss16InhCntAftLmtCrngSpn_f;
  rty_ETCS_AFT_TURN->lcetcss16ScaleDnAyMani = rtb_lcetcss16ScaleDnAyMani;

  /* Update for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = rtb_lcetcss16AftLmtCrngCnt_j;

  /* Update for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = rtb_lcetcss16InhCntAftLmtCrngSpn_f;
}

/* Model initialize function */
void LCETCS_vCalAftLmtCrng_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalCntAftLmtCrng' */
  LCETCS_vCalCntAftLmtCrng_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalInhAftLmtCrng' */
  LCETCS_vCalInhAftLmtCrng_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalSetAftLmtCrng' */
  LCETCS_vCalSetAftLmtCrng_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
