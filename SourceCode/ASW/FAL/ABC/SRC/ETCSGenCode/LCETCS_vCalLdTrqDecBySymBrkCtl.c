/*
 * File: LCETCS_vCalLdTrqDecBySymBrkCtl.c
 *
 * Code generated for Simulink model 'LCETCS_vCalLdTrqDecBySymBrkCtl'.
 *
 * Model version                  : 1.91
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:48:16 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalLdTrqDecBySymBrkCtl.h"
#include "LCETCS_vCalLdTrqDecBySymBrkCtl_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalLdTrqDecBySymBrkCtl' */
void LCETCS_vCalLdTrqDecBySymBrkCtl_Init(DW_LCETCS_vCalLdTrqDecBySymBrkCtl_f_T
  *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = 0;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = 0;
}

/* Output and update for referenced model: 'LCETCS_vCalLdTrqDecBySymBrkCtl' */
void LCETCS_vCalLdTrqDecBySymBrkCtl(const TypeETCSBrkCtlCmdStruct
  *rtu_ETCS_BRK_CTL_CMD_OLD, const TypeETCSBrkCtlReqStruct
  *rtu_ETCS_BRK_CTL_REQ_OLD, int32_T *rty_lcetcss32LdTrqDecBySymBrkCtl,
  DW_LCETCS_vCalLdTrqDecBySymBrkCtl_f_T *localDW)
{
  int16_T rtb_Switch;
  int16_T rtb_Switch1;

  /* Switch: '<Root>/Switch' incorporates:
   *  Constant: '<Root>/Constant4'
   */
  if (rtu_ETCS_BRK_CTL_REQ_OLD->lcetcsu1SymBrkTrqCtlReqFA) {
    /* Sum: '<Root>/Add1' incorporates:
     *  UnitDelay: '<Root>/Unit Delay'
     */
    rtb_Switch = (int16_T)(rtu_ETCS_BRK_CTL_CMD_OLD->lcetcss16SymBrkTrqCtlCmdFA
      - localDW->UnitDelay_DSTATE);

    /* Saturate: '<Root>/Saturation' */
    if (rtb_Switch > 0) {
      rtb_Switch = 0;
    } else {
      if (rtb_Switch < -4000) {
        rtb_Switch = -4000;
      }
    }

    /* End of Saturate: '<Root>/Saturation' */
  } else {
    rtb_Switch = 0;
  }

  /* End of Switch: '<Root>/Switch' */

  /* Switch: '<Root>/Switch1' incorporates:
   *  Constant: '<Root>/Constant5'
   */
  if (rtu_ETCS_BRK_CTL_REQ_OLD->lcetcsu1SymBrkTrqCtlReqRA) {
    /* Sum: '<Root>/Add2' incorporates:
     *  UnitDelay: '<Root>/Unit Delay1'
     */
    rtb_Switch1 = (int16_T)(rtu_ETCS_BRK_CTL_CMD_OLD->lcetcss16SymBrkTrqCtlCmdRA
      - localDW->UnitDelay1_DSTATE);

    /* Saturate: '<Root>/Saturation1' */
    if (rtb_Switch1 > 0) {
      rtb_Switch1 = 0;
    } else {
      if (rtb_Switch1 < -4000) {
        rtb_Switch1 = -4000;
      }
    }

    /* End of Saturate: '<Root>/Saturation1' */
  } else {
    rtb_Switch1 = 0;
  }

  /* End of Switch: '<Root>/Switch1' */

  /* Product: '<Root>/Divide' incorporates:
   *  Constant: '<Root>/Constant2'
   *  Product: '<Root>/Product'
   *  Product: '<Root>/Product1'
   *  Product: '<Root>/Product2'
   *  Sum: '<Root>/Add'
   */
  *rty_lcetcss32LdTrqDecBySymBrkCtl = ((((rtb_Switch + rtb_Switch1) << 1) * 10) *
                                       ((uint8_T)U8ETCSCpLdTrqChngFac)) / 100;

  /* Saturate: '<Root>/Saturation2' */
  if ((*rty_lcetcss32LdTrqDecBySymBrkCtl) < -160000) {
    *rty_lcetcss32LdTrqDecBySymBrkCtl = -160000;
  }

  /* End of Saturate: '<Root>/Saturation2' */

  /* Update for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE =
    rtu_ETCS_BRK_CTL_CMD_OLD->lcetcss16SymBrkTrqCtlCmdFA;

  /* Update for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE =
    rtu_ETCS_BRK_CTL_CMD_OLD->lcetcss16SymBrkTrqCtlCmdRA;
}

/* Model initialize function */
void LCETCS_vCalLdTrqDecBySymBrkCtl_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
