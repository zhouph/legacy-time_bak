/*
 * File: LCETCS_vCalBasDelYawCnvrgGain.c
 *
 * Code generated for Simulink model 'LCETCS_vCalBasDelYawCnvrgGain'.
 *
 * Model version                  : 1.261
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:02:50 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalBasDelYawCnvrgGain.h"
#include "LCETCS_vCalBasDelYawCnvrgGain_private.h"

/* Start for referenced model: 'LCETCS_vCalBasDelYawCnvrgGain' */
void LCETCS_vCalBasDelYawCnvrgGain_Start(B_LCETCS_vCalBasDelYawCnvrgGain_c_T
  *localB)
{
  /* Start for IfAction SubSystem: '<Root>/I gain to increase torque fast' */
  /* Start for Constant: '<S1>/Ay level on asphalt' */
  localB->x3_h = ((uint8_T)U8ETCSCpAyAsp);

  /* Start for Constant: '<S1>/Ay level on ice' */
  localB->x1_g = ((uint8_T)U8ETCSCpAyIce);

  /* Start for Constant: '<S1>/Ay level on snow' */
  localB->x2_h = ((uint8_T)U8ETCSCpAySnw);

  /* Start for Constant: '<S1>/Delta yaw gain on asphalt at speed 1' */
  localB->z13_a = ((uint8_T)U8ETCSCpDelYawFstIncGainAsp_1);

  /* Start for Constant: '<S1>/Delta yaw gain on asphalt at speed 2' */
  localB->z23_f = ((uint8_T)U8ETCSCpDelYawFstIncGainAsp_2);

  /* Start for Constant: '<S1>/Delta yaw gain on asphalt at speed 3' */
  localB->z33_n = ((uint8_T)U8ETCSCpDelYawFstIncGainAsp_3);

  /* Start for Constant: '<S1>/Delta yaw gain on asphalt at speed 4' */
  localB->z43_o = ((uint8_T)U8ETCSCpDelYawFstIncGainAsp_4);

  /* Start for Constant: '<S1>/Delta yaw gain on asphalt at speed 5' */
  localB->z53_c = ((uint8_T)U8ETCSCpDelYawFstIncGainAsp_5);

  /* Start for Constant: '<S1>/Delta yaw gain on ice at speed 1' */
  localB->z11_i = ((uint8_T)U8ETCSCpDelYawFstIncGainIce_1);

  /* Start for Constant: '<S1>/Delta yaw gain on ice at speed 2' */
  localB->z21_b = ((uint8_T)U8ETCSCpDelYawFstIncGainIce_2);

  /* Start for Constant: '<S1>/Delta yaw gain on ice at speed 3' */
  localB->z31_e = ((uint8_T)U8ETCSCpDelYawFstIncGainIce_3);

  /* Start for Constant: '<S1>/Delta yaw gain on ice at speed 4' */
  localB->z41_d = ((uint8_T)U8ETCSCpDelYawFstIncGainIce_4);

  /* Start for Constant: '<S1>/Delta yaw gain on ice at speed 5' */
  localB->z51_k = ((uint8_T)U8ETCSCpDelYawFstIncGainIce_5);

  /* Start for Constant: '<S1>/Delta yaw gain on snow at speed 1' */
  localB->z12_b = ((uint8_T)U8ETCSCpDelYawFstIncGainSnw_1);

  /* Start for Constant: '<S1>/Delta yaw gain on snow at speed 2' */
  localB->z22_l = ((uint8_T)U8ETCSCpDelYawFstIncGainSnw_2);

  /* Start for Constant: '<S1>/Delta yaw gain on snow at speed 3' */
  localB->z32_d = ((uint8_T)U8ETCSCpDelYawFstIncGainSnw_3);

  /* Start for Constant: '<S1>/Delta yaw gain on snow at speed 4' */
  localB->z42_m = ((uint8_T)U8ETCSCpDelYawFstIncGainSnw_4);

  /* Start for Constant: '<S1>/Delta yaw gain on snow at speed 5' */
  localB->z52_a = ((uint8_T)U8ETCSCpDelYawFstIncGainSnw_5);

  /* End of Start for SubSystem: '<Root>/I gain to increase torque fast' */

  /* InitializeConditions for IfAction SubSystem: '<Root>/I gain to increase torque fast' */

  /* InitializeConditions for ModelReference: '<S1>/LCETCS_s16Inter3by5' */
  LCETCS_s16Inter3by5_Init(&localB->lcetcss16BasDelYawCnvrgGain_o);

  /* End of InitializeConditions for SubSystem: '<Root>/I gain to increase torque fast' */

  /* Start for IfAction SubSystem: '<Root>/I gain to increase torque slow' */
  /* Start for Constant: '<S2>/Ay level on asphalt' */
  localB->x3 = ((uint8_T)U8ETCSCpAyAsp);

  /* Start for Constant: '<S2>/Ay level on ice' */
  localB->x1 = ((uint8_T)U8ETCSCpAyIce);

  /* Start for Constant: '<S2>/Ay level on snow' */
  localB->x2 = ((uint8_T)U8ETCSCpAySnw);

  /* Start for Constant: '<S2>/Delta yaw gain on asphalt at speed 1' */
  localB->z13 = ((uint8_T)U8ETCSCpDelYawSlwIncGainAsp_1);

  /* Start for Constant: '<S2>/Delta yaw gain on asphalt at speed 2' */
  localB->z23 = ((uint8_T)U8ETCSCpDelYawSlwIncGainAsp_2);

  /* Start for Constant: '<S2>/Delta yaw gain on asphalt at speed 3' */
  localB->z33 = ((uint8_T)U8ETCSCpDelYawSlwIncGainAsp_3);

  /* Start for Constant: '<S2>/Delta yaw gain on asphalt at speed 4' */
  localB->z43 = ((uint8_T)U8ETCSCpDelYawSlwIncGainAsp_4);

  /* Start for Constant: '<S2>/Delta yaw gain on asphalt at speed 5' */
  localB->z53 = ((uint8_T)U8ETCSCpDelYawSlwIncGainAsp_5);

  /* Start for Constant: '<S2>/Delta yaw gain on ice at speed 1' */
  localB->z11 = ((uint8_T)U8ETCSCpDelYawSlwIncGainIce_1);

  /* Start for Constant: '<S2>/Delta yaw gain on ice at speed 2' */
  localB->z21 = ((uint8_T)U8ETCSCpDelYawSlwIncGainIce_2);

  /* Start for Constant: '<S2>/Delta yaw gain on ice at speed 3' */
  localB->z31 = ((uint8_T)U8ETCSCpDelYawSlwIncGainIce_3);

  /* Start for Constant: '<S2>/Delta yaw gain on ice at speed 4' */
  localB->z41 = ((uint8_T)U8ETCSCpDelYawSlwIncGainIce_4);

  /* Start for Constant: '<S2>/Delta yaw gain on ice at speed 5' */
  localB->z51 = ((uint8_T)U8ETCSCpDelYawSlwIncGainIce_5);

  /* Start for Constant: '<S2>/Delta yaw gain on snow at speed 1' */
  localB->z12 = ((uint8_T)U8ETCSCpDelYawSlwIncGainSnw_1);

  /* Start for Constant: '<S2>/Delta yaw gain on snow at speed 2' */
  localB->z22 = ((uint8_T)U8ETCSCpDelYawSlwIncGainSnw_2);

  /* Start for Constant: '<S2>/Delta yaw gain on snow at speed 3' */
  localB->z32 = ((uint8_T)U8ETCSCpDelYawSlwIncGainSnw_3);

  /* Start for Constant: '<S2>/Delta yaw gain on snow at speed 4' */
  localB->z42 = ((uint8_T)U8ETCSCpDelYawSlwIncGainSnw_4);

  /* Start for Constant: '<S2>/Delta yaw gain on snow at speed 5' */
  localB->z52 = ((uint8_T)U8ETCSCpDelYawSlwIncGainSnw_5);

  /* End of Start for SubSystem: '<Root>/I gain to increase torque slow' */

  /* InitializeConditions for IfAction SubSystem: '<Root>/I gain to increase torque slow' */

  /* InitializeConditions for ModelReference: '<S2>/LCETCS_s16Inter3by5' */
  LCETCS_s16Inter3by5_Init(&localB->lcetcss16BasDelYawCnvrgGain);

  /* End of InitializeConditions for SubSystem: '<Root>/I gain to increase torque slow' */
}

/* Output and update for referenced model: 'LCETCS_vCalBasDelYawCnvrgGain' */
void LCETCS_vCalBasDelYawCnvrgGain(const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG,
  int16_T rtu_lcetcss16VehSpd, const TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN,
  int16_T *rty_lcetcss16BasDelYawCnvrgGain, B_LCETCS_vCalBasDelYawCnvrgGain_c_T *
  localB)
{
  /* If: '<Root>/If' incorporates:
   *  Constant: '<S1>/Ay level on asphalt'
   *  Constant: '<S1>/Ay level on ice'
   *  Constant: '<S1>/Ay level on snow'
   *  Constant: '<S1>/Delta yaw gain on asphalt at speed 1'
   *  Constant: '<S1>/Delta yaw gain on asphalt at speed 2'
   *  Constant: '<S1>/Delta yaw gain on asphalt at speed 3'
   *  Constant: '<S1>/Delta yaw gain on asphalt at speed 4'
   *  Constant: '<S1>/Delta yaw gain on asphalt at speed 5'
   *  Constant: '<S1>/Delta yaw gain on ice at speed 1'
   *  Constant: '<S1>/Delta yaw gain on ice at speed 2'
   *  Constant: '<S1>/Delta yaw gain on ice at speed 3'
   *  Constant: '<S1>/Delta yaw gain on ice at speed 4'
   *  Constant: '<S1>/Delta yaw gain on ice at speed 5'
   *  Constant: '<S1>/Delta yaw gain on snow at speed 1'
   *  Constant: '<S1>/Delta yaw gain on snow at speed 2'
   *  Constant: '<S1>/Delta yaw gain on snow at speed 3'
   *  Constant: '<S1>/Delta yaw gain on snow at speed 4'
   *  Constant: '<S1>/Delta yaw gain on snow at speed 5'
   *  Constant: '<S1>/Vehicle speed break point1'
   *  Constant: '<S1>/Vehicle speed break point2'
   *  Constant: '<S1>/Vehicle speed break point3'
   *  Constant: '<S1>/Vehicle speed break point4'
   *  Constant: '<S1>/Vehicle speed break point5'
   *  Constant: '<S2>/Ay level on asphalt'
   *  Constant: '<S2>/Ay level on ice'
   *  Constant: '<S2>/Ay level on snow'
   *  Constant: '<S2>/Delta yaw gain on asphalt at speed 1'
   *  Constant: '<S2>/Delta yaw gain on asphalt at speed 2'
   *  Constant: '<S2>/Delta yaw gain on asphalt at speed 3'
   *  Constant: '<S2>/Delta yaw gain on asphalt at speed 4'
   *  Constant: '<S2>/Delta yaw gain on asphalt at speed 5'
   *  Constant: '<S2>/Delta yaw gain on ice at speed 1'
   *  Constant: '<S2>/Delta yaw gain on ice at speed 2'
   *  Constant: '<S2>/Delta yaw gain on ice at speed 3'
   *  Constant: '<S2>/Delta yaw gain on ice at speed 4'
   *  Constant: '<S2>/Delta yaw gain on ice at speed 5'
   *  Constant: '<S2>/Delta yaw gain on snow at speed 1'
   *  Constant: '<S2>/Delta yaw gain on snow at speed 2'
   *  Constant: '<S2>/Delta yaw gain on snow at speed 3'
   *  Constant: '<S2>/Delta yaw gain on snow at speed 4'
   *  Constant: '<S2>/Delta yaw gain on snow at speed 5'
   *  Constant: '<S2>/Vehicle speed break point1'
   *  Constant: '<S2>/Vehicle speed break point2'
   *  Constant: '<S2>/Vehicle speed break point3'
   *  Constant: '<S2>/Vehicle speed break point4'
   *  Constant: '<S2>/Vehicle speed break point5'
   */
  if (!rtu_ETCS_TAR_SPIN->lcetcsu1TarSpnDec) {
    /* Outputs for IfAction SubSystem: '<Root>/I gain to increase torque fast' incorporates:
     *  ActionPort: '<S1>/Action Port'
     */
    localB->x3_h = ((uint8_T)U8ETCSCpAyAsp);
    localB->x1_g = ((uint8_T)U8ETCSCpAyIce);
    localB->x2_h = ((uint8_T)U8ETCSCpAySnw);
    localB->z13_a = ((uint8_T)U8ETCSCpDelYawFstIncGainAsp_1);
    localB->z23_f = ((uint8_T)U8ETCSCpDelYawFstIncGainAsp_2);
    localB->z33_n = ((uint8_T)U8ETCSCpDelYawFstIncGainAsp_3);
    localB->z43_o = ((uint8_T)U8ETCSCpDelYawFstIncGainAsp_4);
    localB->z53_c = ((uint8_T)U8ETCSCpDelYawFstIncGainAsp_5);
    localB->z11_i = ((uint8_T)U8ETCSCpDelYawFstIncGainIce_1);
    localB->z21_b = ((uint8_T)U8ETCSCpDelYawFstIncGainIce_2);
    localB->z31_e = ((uint8_T)U8ETCSCpDelYawFstIncGainIce_3);
    localB->z41_d = ((uint8_T)U8ETCSCpDelYawFstIncGainIce_4);
    localB->z51_k = ((uint8_T)U8ETCSCpDelYawFstIncGainIce_5);
    localB->z12_b = ((uint8_T)U8ETCSCpDelYawFstIncGainSnw_1);
    localB->z22_l = ((uint8_T)U8ETCSCpDelYawFstIncGainSnw_2);
    localB->z32_d = ((uint8_T)U8ETCSCpDelYawFstIncGainSnw_3);
    localB->z42_m = ((uint8_T)U8ETCSCpDelYawFstIncGainSnw_4);
    localB->z52_a = ((uint8_T)U8ETCSCpDelYawFstIncGainSnw_5);

    /* ModelReference: '<S1>/LCETCS_s16Inter3by5' */
    LCETCS_s16Inter3by5(rtu_ETCS_ESC_SIG->lcetcss16LatAccMsrdMani, localB->x1_g,
                        localB->x2_h, localB->x3_h, rtu_lcetcss16VehSpd,
                        ((int16_T)S16ETCSCpSpd_1), ((int16_T)S16ETCSCpSpd_2),
                        ((int16_T)S16ETCSCpSpd_3), ((int16_T)S16ETCSCpSpd_4),
                        ((int16_T)S16ETCSCpSpd_5), localB->z11_i, localB->z12_b,
                        localB->z13_a, localB->z21_b, localB->z22_l,
                        localB->z23_f, localB->z31_e, localB->z32_d,
                        localB->z33_n, localB->z41_d, localB->z42_m,
                        localB->z43_o, localB->z51_k, localB->z52_a,
                        localB->z53_c, &localB->lcetcss16BasDelYawCnvrgGain_o);

    /* SignalConversion: '<S1>/OutportBufferForlcetcss16BasDelYawCnvrgGain' incorporates:
     *  Constant: '<S1>/Ay level on asphalt'
     *  Constant: '<S1>/Ay level on ice'
     *  Constant: '<S1>/Ay level on snow'
     *  Constant: '<S1>/Delta yaw gain on asphalt at speed 1'
     *  Constant: '<S1>/Delta yaw gain on asphalt at speed 2'
     *  Constant: '<S1>/Delta yaw gain on asphalt at speed 3'
     *  Constant: '<S1>/Delta yaw gain on asphalt at speed 4'
     *  Constant: '<S1>/Delta yaw gain on asphalt at speed 5'
     *  Constant: '<S1>/Delta yaw gain on ice at speed 1'
     *  Constant: '<S1>/Delta yaw gain on ice at speed 2'
     *  Constant: '<S1>/Delta yaw gain on ice at speed 3'
     *  Constant: '<S1>/Delta yaw gain on ice at speed 4'
     *  Constant: '<S1>/Delta yaw gain on ice at speed 5'
     *  Constant: '<S1>/Delta yaw gain on snow at speed 1'
     *  Constant: '<S1>/Delta yaw gain on snow at speed 2'
     *  Constant: '<S1>/Delta yaw gain on snow at speed 3'
     *  Constant: '<S1>/Delta yaw gain on snow at speed 4'
     *  Constant: '<S1>/Delta yaw gain on snow at speed 5'
     *  Constant: '<S1>/Vehicle speed break point1'
     *  Constant: '<S1>/Vehicle speed break point2'
     *  Constant: '<S1>/Vehicle speed break point3'
     *  Constant: '<S1>/Vehicle speed break point4'
     *  Constant: '<S1>/Vehicle speed break point5'
     */
    *rty_lcetcss16BasDelYawCnvrgGain = localB->lcetcss16BasDelYawCnvrgGain_o;

    /* End of Outputs for SubSystem: '<Root>/I gain to increase torque fast' */
  } else {
    /* Outputs for IfAction SubSystem: '<Root>/I gain to increase torque slow' incorporates:
     *  ActionPort: '<S2>/Action Port'
     */
    localB->x3 = ((uint8_T)U8ETCSCpAyAsp);
    localB->x1 = ((uint8_T)U8ETCSCpAyIce);
    localB->x2 = ((uint8_T)U8ETCSCpAySnw);
    localB->z13 = ((uint8_T)U8ETCSCpDelYawSlwIncGainAsp_1);
    localB->z23 = ((uint8_T)U8ETCSCpDelYawSlwIncGainAsp_2);
    localB->z33 = ((uint8_T)U8ETCSCpDelYawSlwIncGainAsp_3);
    localB->z43 = ((uint8_T)U8ETCSCpDelYawSlwIncGainAsp_4);
    localB->z53 = ((uint8_T)U8ETCSCpDelYawSlwIncGainAsp_5);
    localB->z11 = ((uint8_T)U8ETCSCpDelYawSlwIncGainIce_1);
    localB->z21 = ((uint8_T)U8ETCSCpDelYawSlwIncGainIce_2);
    localB->z31 = ((uint8_T)U8ETCSCpDelYawSlwIncGainIce_3);
    localB->z41 = ((uint8_T)U8ETCSCpDelYawSlwIncGainIce_4);
    localB->z51 = ((uint8_T)U8ETCSCpDelYawSlwIncGainIce_5);
    localB->z12 = ((uint8_T)U8ETCSCpDelYawSlwIncGainSnw_1);
    localB->z22 = ((uint8_T)U8ETCSCpDelYawSlwIncGainSnw_2);
    localB->z32 = ((uint8_T)U8ETCSCpDelYawSlwIncGainSnw_3);
    localB->z42 = ((uint8_T)U8ETCSCpDelYawSlwIncGainSnw_4);
    localB->z52 = ((uint8_T)U8ETCSCpDelYawSlwIncGainSnw_5);

    /* ModelReference: '<S2>/LCETCS_s16Inter3by5' */
    LCETCS_s16Inter3by5(rtu_ETCS_ESC_SIG->lcetcss16LatAccMsrdMani, localB->x1,
                        localB->x2, localB->x3, rtu_lcetcss16VehSpd, ((int16_T)
      S16ETCSCpSpd_1), ((int16_T)S16ETCSCpSpd_2), ((int16_T)S16ETCSCpSpd_3),
                        ((int16_T)S16ETCSCpSpd_4), ((int16_T)S16ETCSCpSpd_5),
                        localB->z11, localB->z12, localB->z13, localB->z21,
                        localB->z22, localB->z23, localB->z31, localB->z32,
                        localB->z33, localB->z41, localB->z42, localB->z43,
                        localB->z51, localB->z52, localB->z53,
                        &localB->lcetcss16BasDelYawCnvrgGain);

    /* SignalConversion: '<S2>/OutportBufferForlcetcss16BasDelYawCnvrgGain' incorporates:
     *  Constant: '<S2>/Ay level on asphalt'
     *  Constant: '<S2>/Ay level on ice'
     *  Constant: '<S2>/Ay level on snow'
     *  Constant: '<S2>/Delta yaw gain on asphalt at speed 1'
     *  Constant: '<S2>/Delta yaw gain on asphalt at speed 2'
     *  Constant: '<S2>/Delta yaw gain on asphalt at speed 3'
     *  Constant: '<S2>/Delta yaw gain on asphalt at speed 4'
     *  Constant: '<S2>/Delta yaw gain on asphalt at speed 5'
     *  Constant: '<S2>/Delta yaw gain on ice at speed 1'
     *  Constant: '<S2>/Delta yaw gain on ice at speed 2'
     *  Constant: '<S2>/Delta yaw gain on ice at speed 3'
     *  Constant: '<S2>/Delta yaw gain on ice at speed 4'
     *  Constant: '<S2>/Delta yaw gain on ice at speed 5'
     *  Constant: '<S2>/Delta yaw gain on snow at speed 1'
     *  Constant: '<S2>/Delta yaw gain on snow at speed 2'
     *  Constant: '<S2>/Delta yaw gain on snow at speed 3'
     *  Constant: '<S2>/Delta yaw gain on snow at speed 4'
     *  Constant: '<S2>/Delta yaw gain on snow at speed 5'
     *  Constant: '<S2>/Vehicle speed break point1'
     *  Constant: '<S2>/Vehicle speed break point2'
     *  Constant: '<S2>/Vehicle speed break point3'
     *  Constant: '<S2>/Vehicle speed break point4'
     *  Constant: '<S2>/Vehicle speed break point5'
     */
    *rty_lcetcss16BasDelYawCnvrgGain = localB->lcetcss16BasDelYawCnvrgGain;

    /* End of Outputs for SubSystem: '<Root>/I gain to increase torque slow' */
  }

  /* End of If: '<Root>/If' */

  /* Saturate: '<Root>/Saturation1' */
  if ((*rty_lcetcss16BasDelYawCnvrgGain) > 250) {
    *rty_lcetcss16BasDelYawCnvrgGain = 250;
  } else {
    if ((*rty_lcetcss16BasDelYawCnvrgGain) < 0) {
      *rty_lcetcss16BasDelYawCnvrgGain = 0;
    }
  }

  /* End of Saturate: '<Root>/Saturation1' */
}

/* Model initialize function */
void LCETCS_vCalBasDelYawCnvrgGain_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<S1>/LCETCS_s16Inter3by5' */
  LCETCS_s16Inter3by5_initialize();

  /* Model Initialize fcn for ModelReference Block: '<S2>/LCETCS_s16Inter3by5' */
  LCETCS_s16Inter3by5_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
