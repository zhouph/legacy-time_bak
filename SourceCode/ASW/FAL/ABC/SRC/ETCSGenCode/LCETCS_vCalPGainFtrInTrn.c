/*
 * File: LCETCS_vCalPGainFtrInTrn.c
 *
 * Code generated for Simulink model 'LCETCS_vCalPGainFtrInTrn'.
 *
 * Model version                  : 1.206
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:02:29 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalPGainFtrInTrn.h"
#include "LCETCS_vCalPGainFtrInTrn_private.h"

/* Start for referenced model: 'LCETCS_vCalPGainFtrInTrn' */
void LCETCS_vCalPGainFtrInTrn_Start(B_LCETCS_vCalPGainFtrInTrn_c_T *localB)
{
  /* Start for IfAction SubSystem: '<Root>/ActOfCalPGainFacInTrn' */
  /* Start for Constant: '<S1>/Ay level on asphalt' */
  localB->x3 = ((uint8_T)U8ETCSCpAyAsp);

  /* Start for Constant: '<S1>/Ay level on ice' */
  localB->x1 = ((uint8_T)U8ETCSCpAyIce);

  /* Start for Constant: '<S1>/Ay level on snow' */
  localB->x2 = ((uint8_T)U8ETCSCpAySnw);

  /* Start for Constant: '<S1>/P gain factor in turn on ice at 5th gear' */
  localB->z11 = ((uint8_T)U8ETCSCpPgFacInTrnIce_5);

  /* Start for Constant: '<S1>/P gain factor in turn on snow at 5th gear' */
  localB->z12 = ((uint8_T)U8ETCSCpPgFacInTrnSnw_5);

  /* Start for Constant: '<S1>/P gain factor in turn on asphalt at 5th gear' */
  localB->z13 = ((uint8_T)U8ETCSCpPgFacInTrnAsp_5);

  /* Start for Constant: '<S1>/P gain factor in turn on ice at 4th gear' */
  localB->z21 = ((uint8_T)U8ETCSCpPgFacInTrnIce_4);

  /* Start for Constant: '<S1>/P gain factor in turn on snow at 4th gear' */
  localB->z22 = ((uint8_T)U8ETCSCpPgFacInTrnSnw_4);

  /* Start for Constant: '<S1>/P gain factor in turn on asphalt at 4th gear' */
  localB->z23 = ((uint8_T)U8ETCSCpPgFacInTrnAsp_4);

  /* Start for Constant: '<S1>/P gain factor in turn on ice at 3rd gear' */
  localB->z31 = ((uint8_T)U8ETCSCpPgFacInTrnIce_3);

  /* Start for Constant: '<S1>/P gain factor in turn on snow at 3rd gear' */
  localB->z32 = ((uint8_T)U8ETCSCpPgFacInTrnSnw_3);

  /* Start for Constant: '<S1>/P gain factor in turn on asphalt at 3rd gear' */
  localB->z33 = ((uint8_T)U8ETCSCpPgFacInTrnAsp_3);

  /* Start for Constant: '<S1>/P gain factor in turn on ice at 2nd gear' */
  localB->z41 = ((uint8_T)U8ETCSCpPgFacInTrnIce_2);

  /* Start for Constant: '<S1>/P gain factor in turn on snow at 2nd gear' */
  localB->z42 = ((uint8_T)U8ETCSCpPgFacInTrnSnw_2);

  /* Start for Constant: '<S1>/P gain factor in turn on asphalt at 2nd gear' */
  localB->z43 = ((uint8_T)U8ETCSCpPgFacInTrnAsp_2);

  /* Start for Constant: '<S1>/P gain factor in turn on ice at 1st gear' */
  localB->z51 = ((uint8_T)U8ETCSCpPgFacInTrnIce_1);

  /* Start for Constant: '<S1>/P gain factor in turn on snow at 1st gear' */
  localB->z52 = ((uint8_T)U8ETCSCpPgFacInTrnSnw_1);

  /* Start for Constant: '<S1>/P gain factor in turn on asphalt at 1st gear' */
  localB->z53 = ((uint8_T)U8ETCSCpPgFacInTrnAsp_1);

  /* End of Start for SubSystem: '<Root>/ActOfCalPGainFacInTrn' */

  /* InitializeConditions for IfAction SubSystem: '<Root>/ActOfCalPGainFacInTrn' */

  /* InitializeConditions for ModelReference: '<S1>/LCETCS_s16Inter2by3' */
  LCETCS_s16Inter3by5_Init(&localB->Out);

  /* End of InitializeConditions for SubSystem: '<Root>/ActOfCalPGainFacInTrn' */
}

/* Output and update for referenced model: 'LCETCS_vCalPGainFtrInTrn' */
void LCETCS_vCalPGainFtrInTrn(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT,
  const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const TypeETCSTarSpinStruct
  *rtu_ETCS_TAR_SPIN, const TypeETCSDrvMdlStruct *rtu_ETCS_DRV_MDL, const
  TypeETCSCtlErrStruct *rtu_ETCS_CTL_ERR, int16_T *rty_lcetcss16PGainFacInTrn,
  B_LCETCS_vCalPGainFtrInTrn_c_T *localB)
{
  /* If: '<Root>/If' incorporates:
   *  Constant: '<S1>/Ay level on asphalt'
   *  Constant: '<S1>/Ay level on ice'
   *  Constant: '<S1>/Ay level on snow'
   *  Constant: '<S1>/P gain factor in turn on asphalt at 1st gear'
   *  Constant: '<S1>/P gain factor in turn on asphalt at 2nd gear'
   *  Constant: '<S1>/P gain factor in turn on asphalt at 3rd gear'
   *  Constant: '<S1>/P gain factor in turn on asphalt at 4th gear'
   *  Constant: '<S1>/P gain factor in turn on asphalt at 5th gear'
   *  Constant: '<S1>/P gain factor in turn on ice at 1st gear'
   *  Constant: '<S1>/P gain factor in turn on ice at 2nd gear'
   *  Constant: '<S1>/P gain factor in turn on ice at 3rd gear'
   *  Constant: '<S1>/P gain factor in turn on ice at 4th gear'
   *  Constant: '<S1>/P gain factor in turn on ice at 5th gear'
   *  Constant: '<S1>/P gain factor in turn on snow at 1st gear'
   *  Constant: '<S1>/P gain factor in turn on snow at 2nd gear'
   *  Constant: '<S1>/P gain factor in turn on snow at 3rd gear'
   *  Constant: '<S1>/P gain factor in turn on snow at 4th gear'
   *  Constant: '<S1>/P gain factor in turn on snow at 5th gear'
   *  Constant: '<S1>/Total Gear Ratio at 1st Gear1'
   *  Constant: '<S1>/Total Gear Ratio at 2nd Gear1'
   *  Constant: '<S1>/Total Gear Ratio at 3rd Gear1'
   *  Constant: '<S1>/Total Gear Ratio at 4th Gear1'
   *  Constant: '<S1>/Total Gear Ratio at 5th Gear1'
   *  Constant: '<S2>/Constant'
   *  Logic: '<Root>/Logical Operator'
   *  RelationalOperator: '<Root>/Relational Operator'
   */
  if (!((rtu_ETCS_CTL_ACT->lcetcsu1CtlAct) &&
        (rtu_ETCS_TAR_SPIN->lcetcss16LmtCrngDltYaw <=
         rtu_ETCS_ESC_SIG->lcetcss16AbsDelYawFrst))) {
    /* Outputs for IfAction SubSystem: '<Root>/DeActOfCalPGainFacInTrn' incorporates:
     *  ActionPort: '<S2>/Action Port'
     */
    *rty_lcetcss16PGainFacInTrn = 100;

    /* End of Outputs for SubSystem: '<Root>/DeActOfCalPGainFacInTrn' */
  } else {
    /* Outputs for IfAction SubSystem: '<Root>/ActOfCalPGainFacInTrn' incorporates:
     *  ActionPort: '<S1>/Action Port'
     */
    localB->x3 = ((uint8_T)U8ETCSCpAyAsp);
    localB->x1 = ((uint8_T)U8ETCSCpAyIce);
    localB->x2 = ((uint8_T)U8ETCSCpAySnw);
    localB->z11 = ((uint8_T)U8ETCSCpPgFacInTrnIce_5);
    localB->z12 = ((uint8_T)U8ETCSCpPgFacInTrnSnw_5);
    localB->z13 = ((uint8_T)U8ETCSCpPgFacInTrnAsp_5);
    localB->z21 = ((uint8_T)U8ETCSCpPgFacInTrnIce_4);
    localB->z22 = ((uint8_T)U8ETCSCpPgFacInTrnSnw_4);
    localB->z23 = ((uint8_T)U8ETCSCpPgFacInTrnAsp_4);
    localB->z31 = ((uint8_T)U8ETCSCpPgFacInTrnIce_3);
    localB->z32 = ((uint8_T)U8ETCSCpPgFacInTrnSnw_3);
    localB->z33 = ((uint8_T)U8ETCSCpPgFacInTrnAsp_3);
    localB->z41 = ((uint8_T)U8ETCSCpPgFacInTrnIce_2);
    localB->z42 = ((uint8_T)U8ETCSCpPgFacInTrnSnw_2);
    localB->z43 = ((uint8_T)U8ETCSCpPgFacInTrnAsp_2);
    localB->z51 = ((uint8_T)U8ETCSCpPgFacInTrnIce_1);
    localB->z52 = ((uint8_T)U8ETCSCpPgFacInTrnSnw_1);
    localB->z53 = ((uint8_T)U8ETCSCpPgFacInTrnAsp_1);

    /* ModelReference: '<S1>/LCETCS_s16Inter2by3' */
    LCETCS_s16Inter3by5(rtu_ETCS_ESC_SIG->lcetcss16LatAccMsrdMani, localB->x1,
                        localB->x2, localB->x3,
                        rtu_ETCS_DRV_MDL->lcetcss16TotalGearRatio, ((int16_T)
      S16ETCSCpTotalGearRatio_5), ((int16_T)S16ETCSCpTotalGearRatio_4),
                        ((int16_T)S16ETCSCpTotalGearRatio_3), ((int16_T)
      S16ETCSCpTotalGearRatio_2), ((int16_T)S16ETCSCpTotalGearRatio_1),
                        localB->z11, localB->z12, localB->z13, localB->z21,
                        localB->z22, localB->z23, localB->z31, localB->z32,
                        localB->z33, localB->z41, localB->z42, localB->z43,
                        localB->z51, localB->z52, localB->z53, &localB->Out);

    /* Switch: '<S1>/Switch' incorporates:
     *  Constant: '<S1>/Ay level on asphalt'
     *  Constant: '<S1>/Ay level on ice'
     *  Constant: '<S1>/Ay level on snow'
     *  Constant: '<S1>/Constant1'
     *  Constant: '<S1>/P gain factor in turn on asphalt at 1st gear'
     *  Constant: '<S1>/P gain factor in turn on asphalt at 2nd gear'
     *  Constant: '<S1>/P gain factor in turn on asphalt at 3rd gear'
     *  Constant: '<S1>/P gain factor in turn on asphalt at 4th gear'
     *  Constant: '<S1>/P gain factor in turn on asphalt at 5th gear'
     *  Constant: '<S1>/P gain factor in turn on ice at 1st gear'
     *  Constant: '<S1>/P gain factor in turn on ice at 2nd gear'
     *  Constant: '<S1>/P gain factor in turn on ice at 3rd gear'
     *  Constant: '<S1>/P gain factor in turn on ice at 4th gear'
     *  Constant: '<S1>/P gain factor in turn on ice at 5th gear'
     *  Constant: '<S1>/P gain factor in turn on snow at 1st gear'
     *  Constant: '<S1>/P gain factor in turn on snow at 2nd gear'
     *  Constant: '<S1>/P gain factor in turn on snow at 3rd gear'
     *  Constant: '<S1>/P gain factor in turn on snow at 4th gear'
     *  Constant: '<S1>/P gain factor in turn on snow at 5th gear'
     *  Constant: '<S1>/Total Gear Ratio at 1st Gear1'
     *  Constant: '<S1>/Total Gear Ratio at 2nd Gear1'
     *  Constant: '<S1>/Total Gear Ratio at 3rd Gear1'
     *  Constant: '<S1>/Total Gear Ratio at 4th Gear1'
     *  Constant: '<S1>/Total Gear Ratio at 5th Gear1'
     *  Gain: '<S1>/Gain'
     */
    if (rtu_ETCS_CTL_ERR->lcetcsu1CtlErrPos) {
      *rty_lcetcss16PGainFacInTrn = 100;
    } else {
      *rty_lcetcss16PGainFacInTrn = (int16_T)(10 * localB->Out);
    }

    /* End of Switch: '<S1>/Switch' */
    /* End of Outputs for SubSystem: '<Root>/ActOfCalPGainFacInTrn' */
  }

  /* End of If: '<Root>/If' */

  /* Saturate: '<Root>/Saturation' */
  if ((*rty_lcetcss16PGainFacInTrn) > 2000) {
    *rty_lcetcss16PGainFacInTrn = 2000;
  } else {
    if ((*rty_lcetcss16PGainFacInTrn) < 100) {
      *rty_lcetcss16PGainFacInTrn = 100;
    }
  }

  /* End of Saturate: '<Root>/Saturation' */
}

/* Model initialize function */
void LCETCS_vCalPGainFtrInTrn_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<S1>/LCETCS_s16Inter2by3' */
  LCETCS_s16Inter3by5_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
