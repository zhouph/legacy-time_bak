/*
 * File: LCTMC_vCtrlTargetGear.c
 *
 * Code generated for Simulink model 'LCTMC_vCtrlTargetGear'.
 *
 * Model version                  : 1.638
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:53:34 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCTMC_vCtrlTargetGear.h"
#include "LCTMC_vCtrlTargetGear_private.h"

const rtTimingBridge *LCTMC_vCtrlTargetGear_TimingBrdg;
const TypeTMCReqGearStruct LCTMC_vCtrlTargetGear_rtZTypeTMCReqGearStruct = {
  0U,                                  /* lctmcu8_BSTGRReqType */
  0U                                   /* lctmcu8_BSTGRReqGear */
} ;                                    /* TypeTMCReqGearStruct ground */

/* Initial conditions for referenced model: 'LCTMC_vCtrlTargetGear' */
void LCTMC_vCtrlTargetGear_Init(TypeTMCReqGearStruct *rty_TMC_REQ_GEAR_STRUCT,
  DW_LCTMC_vCtrlTargetGear_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = LCTMC_vCtrlTargetGear_rtZTypeTMCReqGearStruct;

  /* InitializeConditions for BusCreator: '<Root>/Bus Creator2' incorporates:
   *  InitializeConditions for Chart: '<Root>/Chart1'
   */
  if (rtmIsFirstInitCond()) {
    rty_TMC_REQ_GEAR_STRUCT->lctmcu8_BSTGRReqType = 0U;
    rty_TMC_REQ_GEAR_STRUCT->lctmcu8_BSTGRReqGear = 0U;
  }

  /* End of InitializeConditions for BusCreator: '<Root>/Bus Creator2' */
}

/* Output and update for referenced model: 'LCTMC_vCtrlTargetGear' */
void LCTMC_vCtrlTargetGear(boolean_T rtu_lctmcu1GearShiftCountSet, boolean_T
  rtu_lctmcu1NonCndTarReqGear, const TypeETCSGearStruct *rtu_ETCS_GEAR_STRUCT,
  const TypeTMCRefLimVehSpdMap *rtu_TMC_REF_LIM_VS_MAP, const
  TypeTMCRefFinTarGearMap *rtu_TMC_REF_FIN_TAR_GEAR_MAP, int16_T
  rtu_lcetcss16VehSpd, const TypeETCSEngStruct *rtu_ETCS_ENG_STRUCT,
  TypeTMCReqGearStruct *rty_TMC_REQ_GEAR_STRUCT, DW_LCTMC_vCtrlTargetGear_f_T
  *localDW)
{
  uint8_T lctmcu8_BSTGRReqType;
  uint8_T lctmcu8_BSTGRReqGear;
  int32_T tmp;

  /* Chart: '<Root>/Chart1' incorporates:
   *  UnitDelay: '<Root>/Unit Delay1'
   */
  /* Gateway: Chart1 */
  /* During: Chart1 */
  /* Entry Internal: Chart1 */
  /* Transition: '<S1>:66' */
  /* comment */
  if (rtu_lctmcu1NonCndTarReqGear == 1) {
    /* Transition: '<S1>:69' */
    /* Transition: '<S1>:73' */
    lctmcu8_BSTGRReqGear = (uint8_T)rtu_ETCS_GEAR_STRUCT->lcetcss16CurntGearStep;
    lctmcu8_BSTGRReqType = 0U;

    /* Gear Shift Non Control */
    /* Transition: '<S1>:108' */
    /* Transition: '<S1>:107' */
    /* Transition: '<S1>:104' */
    /* Transition: '<S1>:92' */
  } else {
    /* Transition: '<S1>:70' */
    if ((rtu_ETCS_GEAR_STRUCT->lcetcsu1GearShifting == 1) ||
        (rtu_lctmcu1GearShiftCountSet == 1)) {
      /* Transition: '<S1>:52' */
      /* Transition: '<S1>:57' */
      lctmcu8_BSTGRReqGear = localDW->UnitDelay1_DSTATE.lctmcu8_BSTGRReqGear;
      lctmcu8_BSTGRReqType = localDW->UnitDelay1_DSTATE.lctmcu8_BSTGRReqType;

      /* Gear Shift Sustain */
      /* Transition: '<S1>:107' */
      /* Transition: '<S1>:104' */
      /* Transition: '<S1>:92' */
    } else {
      /* Transition: '<S1>:106' */
      if (((rtu_lcetcss16VehSpd >
            rtu_TMC_REF_LIM_VS_MAP->lctmcs16TarUpShtMaxVSbyGear) ||
           ((rtu_lcetcss16VehSpd >
             rtu_TMC_REF_FIN_TAR_GEAR_MAP->lctmcs16TarUpShtVS) &&
            (rtu_ETCS_ENG_STRUCT->lcetcss16EngSpd >
             rtu_TMC_REF_FIN_TAR_GEAR_MAP->lctmcs16TarUpShtRPM))) &&
          (rtu_ETCS_GEAR_STRUCT->lcetcss16CurntGearStep < ((uint8_T)
            U8TMCpMaxGear))) {
        /* Transition: '<S1>:87' */
        /* Transition: '<S1>:88' */
        lctmcu8_BSTGRReqGear = (uint8_T)
          (rtu_ETCS_GEAR_STRUCT->lcetcss16CurntGearStep + 1);
        lctmcu8_BSTGRReqType = 3U;

        /* Gear Shift Up */
        /* Transition: '<S1>:104' */
        /* Transition: '<S1>:92' */
      } else {
        /* Transition: '<S1>:86' */
        if (((rtu_lcetcss16VehSpd <
              rtu_TMC_REF_LIM_VS_MAP->lctmcs16TarDnShtMinVSbyGear) ||
             ((rtu_lcetcss16VehSpd <
               rtu_TMC_REF_FIN_TAR_GEAR_MAP->lctmcs16TarDnShtVS) &&
              (rtu_ETCS_ENG_STRUCT->lcetcss16EngSpd <
               rtu_TMC_REF_FIN_TAR_GEAR_MAP->lctmcs16TarDnShtRPM))) &&
            (rtu_ETCS_GEAR_STRUCT->lcetcss16CurntGearStep > ((uint8_T)
              U8TMCpMinGear))) {
          /* Transition: '<S1>:89' */
          /* Transition: '<S1>:91' */
          tmp = rtu_ETCS_GEAR_STRUCT->lcetcss16CurntGearStep - 1;
          if (tmp < 0) {
            tmp = 0;
          }

          lctmcu8_BSTGRReqGear = (uint8_T)tmp;
          lctmcu8_BSTGRReqType = 2U;

          /* Gear Shift Down */
          /* Transition: '<S1>:92' */
        } else {
          /* Transition: '<S1>:90' */
          lctmcu8_BSTGRReqGear = localDW->UnitDelay1_DSTATE.lctmcu8_BSTGRReqGear;
          lctmcu8_BSTGRReqType = 1U;

          /* Gear Shift Inhibit */
        }
      }
    }
  }

  /* End of Chart: '<Root>/Chart1' */

  /* BusCreator: '<Root>/Bus Creator2' */
  /* Transition: '<S1>:95' */
  rty_TMC_REQ_GEAR_STRUCT->lctmcu8_BSTGRReqType = lctmcu8_BSTGRReqType;
  rty_TMC_REQ_GEAR_STRUCT->lctmcu8_BSTGRReqGear = lctmcu8_BSTGRReqGear;

  /* Update for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = *rty_TMC_REQ_GEAR_STRUCT;
}

/* Model initialize function */
void LCTMC_vCtrlTargetGear_initialize(const rtTimingBridge *timingBridge)
{
  /* Registration code */
  LCTMC_vCtrlTargetGear_TimingBrdg = timingBridge;
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
