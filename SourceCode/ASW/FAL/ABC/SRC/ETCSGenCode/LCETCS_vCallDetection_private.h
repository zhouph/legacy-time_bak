/*
 * File: LCETCS_vCallDetection_private.h
 *
 * Code generated for Simulink model 'LCETCS_vCallDetection'.
 *
 * Model version                  : 1.397
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:17:11 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCallDetection_private_h_
#define RTW_HEADER_LCETCS_vCallDetection_private_h_
#include "rtwtypes.h"
#include "model_reference_types.h"

/* Includes for objects with custom storage classes. */
#include "../ETCSGenCodeHeader/ETCSHeader4Import.h"

/*
 * Generate compile time checks that imported macros for parameters
 * with storage class "ImportedDefine" are defined
 */
#ifndef S16ETCSCpAftTrnMaxCnt
#error The variable for the parameter "S16ETCSCpAftTrnMaxCnt" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtAspSpd_1
#error The variable for the parameter "S16ETCSCpCdrnTrqAtAspSpd_1" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtAspSpd_2
#error The variable for the parameter "S16ETCSCpCdrnTrqAtAspSpd_2" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtAspSpd_3
#error The variable for the parameter "S16ETCSCpCdrnTrqAtAspSpd_3" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtAspSpd_4
#error The variable for the parameter "S16ETCSCpCdrnTrqAtAspSpd_4" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtAspSpd_5
#error The variable for the parameter "S16ETCSCpCdrnTrqAtAspSpd_5" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtAsphalt
#error The variable for the parameter "S16ETCSCpCdrnTrqAtAsphalt" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtIce
#error The variable for the parameter "S16ETCSCpCdrnTrqAtIce" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtIceSpd_1
#error The variable for the parameter "S16ETCSCpCdrnTrqAtIceSpd_1" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtIceSpd_2
#error The variable for the parameter "S16ETCSCpCdrnTrqAtIceSpd_2" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtIceSpd_3
#error The variable for the parameter "S16ETCSCpCdrnTrqAtIceSpd_3" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtIceSpd_4
#error The variable for the parameter "S16ETCSCpCdrnTrqAtIceSpd_4" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtIceSpd_5
#error The variable for the parameter "S16ETCSCpCdrnTrqAtIceSpd_5" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtSnow
#error The variable for the parameter "S16ETCSCpCdrnTrqAtSnow" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtSnowSpd_1
#error The variable for the parameter "S16ETCSCpCdrnTrqAtSnowSpd_1" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtSnowSpd_2
#error The variable for the parameter "S16ETCSCpCdrnTrqAtSnowSpd_2" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtSnowSpd_3
#error The variable for the parameter "S16ETCSCpCdrnTrqAtSnowSpd_3" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtSnowSpd_4
#error The variable for the parameter "S16ETCSCpCdrnTrqAtSnowSpd_4" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtSnowSpd_5
#error The variable for the parameter "S16ETCSCpCdrnTrqAtSnowSpd_5" is not defined
#endif

#ifndef S16ETCSCpEngStallRpm
#error The variable for the parameter "S16ETCSCpEngStallRpm" is not defined
#endif

#ifndef S16ETCSCpHighRiskStallRpmFlat
#error The variable for the parameter "S16ETCSCpHighRiskStallRpmFlat" is not defined
#endif

#ifndef S16ETCSCpHighRiskStallRpmHill
#error The variable for the parameter "S16ETCSCpHighRiskStallRpmHill" is not defined
#endif

#ifndef S16ETCSCpLow2HighDecel
#error The variable for the parameter "S16ETCSCpLow2HighDecel" is not defined
#endif

#ifndef S16ETCSCpLowRiskStallRpmFlat
#error The variable for the parameter "S16ETCSCpLowRiskStallRpmFlat" is not defined
#endif

#ifndef S16ETCSCpLowRiskStallRpmHill
#error The variable for the parameter "S16ETCSCpLowRiskStallRpmHill" is not defined
#endif

#ifndef S16ETCSCpLwToSpltDctCplCnt
#error The variable for the parameter "S16ETCSCpLwToSpltDctCplCnt" is not defined
#endif

#ifndef S16ETCSCpMidRiskStallRpmFlat
#error The variable for the parameter "S16ETCSCpMidRiskStallRpmFlat" is not defined
#endif

#ifndef S16ETCSCpMidRiskStallRpmHill
#error The variable for the parameter "S16ETCSCpMidRiskStallRpmHill" is not defined
#endif

#ifndef S16ETCSCpOffsetTrq4LwToSplt
#error The variable for the parameter "S16ETCSCpOffsetTrq4LwToSplt" is not defined
#endif

#ifndef S16ETCSCpPreHighRdDctCplCnt
#error The variable for the parameter "S16ETCSCpPreHighRdDctCplCnt" is not defined
#endif

#ifndef S16ETCSCpPreHighRdDctStrtCnt
#error The variable for the parameter "S16ETCSCpPreHighRdDctStrtCnt" is not defined
#endif

#ifndef S16ETCSCpSpd_1
#error The variable for the parameter "S16ETCSCpSpd_1" is not defined
#endif

#ifndef S16ETCSCpSpd_2
#error The variable for the parameter "S16ETCSCpSpd_2" is not defined
#endif

#ifndef S16ETCSCpSpd_3
#error The variable for the parameter "S16ETCSCpSpd_3" is not defined
#endif

#ifndef S16ETCSCpSpd_4
#error The variable for the parameter "S16ETCSCpSpd_4" is not defined
#endif

#ifndef S16ETCSCpSpd_5
#error The variable for the parameter "S16ETCSCpSpd_5" is not defined
#endif

#ifndef S16ETCSCpSplitHillClrEngSpd
#error The variable for the parameter "S16ETCSCpSplitHillClrEngSpd" is not defined
#endif

#ifndef S16ETCSCpSplitHillDctEngSpd
#error The variable for the parameter "S16ETCSCpSplitHillDctEngSpd" is not defined
#endif

#ifndef S16ETCSCpStrtCdrnTrqAtAsphalt
#error The variable for the parameter "S16ETCSCpStrtCdrnTrqAtAsphalt" is not defined
#endif

#ifndef VREF_0_5_KPH
#error The variable for the parameter "VREF_0_5_KPH" is not defined
#endif

#ifndef VREF_1_KPH
#error The variable for the parameter "VREF_1_KPH" is not defined
#endif

#ifndef VREF_25_KPH
#error The variable for the parameter "VREF_25_KPH" is not defined
#endif

#ifndef VREF_3_KPH
#error The variable for the parameter "VREF_3_KPH" is not defined
#endif

#ifndef U16TCSCpDctLwHmMaxCnt
#error The variable for the parameter "U16TCSCpDctLwHmMaxCnt" is not defined
#endif

#ifndef S8ETCSCpCtlExitCntDnTh
#error The variable for the parameter "S8ETCSCpCtlExitCntDnTh" is not defined
#endif

#ifndef S8ETCSCpCtlExitCntTh
#error The variable for the parameter "S8ETCSCpCtlExitCntTh" is not defined
#endif

#ifndef S8ETCSCpCtlExitCntUpTh
#error The variable for the parameter "S8ETCSCpCtlExitCntUpTh" is not defined
#endif

#ifndef S8ETCSCpH2LDctDltTh
#error The variable for the parameter "S8ETCSCpH2LDctDltTh" is not defined
#endif

#ifndef S8ETCSCpH2LDctHiMuTh
#error The variable for the parameter "S8ETCSCpH2LDctHiMuTh" is not defined
#endif

#ifndef S8ETCSCpH2LDctLwMuTh
#error The variable for the parameter "S8ETCSCpH2LDctLwMuTh" is not defined
#endif

#ifndef S8ETCSCpH2LTrnsTime
#error The variable for the parameter "S8ETCSCpH2LTrnsTime" is not defined
#endif

#ifndef GRAD_10_DEG
#error The variable for the parameter "GRAD_10_DEG" is not defined
#endif

#ifndef GRAD_8_DEG
#error The variable for the parameter "GRAD_8_DEG" is not defined
#endif

#ifndef L_U8FILTER_GAIN_20MSLOOP_1HZ
#error The variable for the parameter "L_U8FILTER_GAIN_20MSLOOP_1HZ" is not defined
#endif

#ifndef L_U8FILTER_GAIN_20MSLOOP_7HZ
#error The variable for the parameter "L_U8FILTER_GAIN_20MSLOOP_7HZ" is not defined
#endif

#ifndef TCSDpCtrlModeDisable
#error The variable for the parameter "TCSDpCtrlModeDisable" is not defined
#endif

#ifndef U8ETCSCpAWDVehicle
#error The variable for the parameter "U8ETCSCpAWDVehicle" is not defined
#endif

#ifndef U8ETCSCpAdaptHighRdCplCnt
#error The variable for the parameter "U8ETCSCpAdaptHighRdCplCnt" is not defined
#endif

#ifndef U8ETCSCpAreaSetLwToHiSpin
#error The variable for the parameter "U8ETCSCpAreaSetLwToHiSpin" is not defined
#endif

#ifndef U8ETCSCpAxAtAsphalt
#error The variable for the parameter "U8ETCSCpAxAtAsphalt" is not defined
#endif

#ifndef U8ETCSCpAxAtIce
#error The variable for the parameter "U8ETCSCpAxAtIce" is not defined
#endif

#ifndef U8ETCSCpAxAtSnow
#error The variable for the parameter "U8ETCSCpAxAtSnow" is not defined
#endif

#ifndef U8ETCSCpAxSenAvail
#error The variable for the parameter "U8ETCSCpAxSenAvail" is not defined
#endif

#ifndef U8ETCSCpAyAsp
#error The variable for the parameter "U8ETCSCpAyAsp" is not defined
#endif

#ifndef U8ETCSCpAyIce
#error The variable for the parameter "U8ETCSCpAyIce" is not defined
#endif

#ifndef U8ETCSCpAySnw
#error The variable for the parameter "U8ETCSCpAySnw" is not defined
#endif

#ifndef U8ETCSCpCtlActCntThAtHiAxlAcc
#error The variable for the parameter "U8ETCSCpCtlActCntThAtHiAxlAcc" is not defined
#endif

#ifndef U8ETCSCpCtlActCntThAtLwAxlAcc
#error The variable for the parameter "U8ETCSCpCtlActCntThAtLwAxlAcc" is not defined
#endif

#ifndef U8ETCSCpCtlActTmThInLmtCnrg
#error The variable for the parameter "U8ETCSCpCtlActTmThInLmtCnrg" is not defined
#endif

#ifndef U8ETCSCpCtlEnterAccPdlTh
#error The variable for the parameter "U8ETCSCpCtlEnterAccPdlTh" is not defined
#endif

#ifndef U8ETCSCpCtlExitAccPdlTh
#error The variable for the parameter "U8ETCSCpCtlExitAccPdlTh" is not defined
#endif

#ifndef U8ETCSCpDetDepSnwHilCnt
#error The variable for the parameter "U8ETCSCpDetDepSnwHilCnt" is not defined
#endif

#ifndef U8ETCSCpEngAvail
#error The variable for the parameter "U8ETCSCpEngAvail" is not defined
#endif

#ifndef U8ETCSCpEngStallRpmOffset
#error The variable for the parameter "U8ETCSCpEngStallRpmOffset" is not defined
#endif

#ifndef U8ETCSCpFtr4JmpDnMax
#error The variable for the parameter "U8ETCSCpFtr4JmpDnMax" is not defined
#endif

#ifndef U8ETCSCpFtr4JmpDnMin
#error The variable for the parameter "U8ETCSCpFtr4JmpDnMin" is not defined
#endif

#ifndef U8ETCSCpFtr4JmpUpMax
#error The variable for the parameter "U8ETCSCpFtr4JmpUpMax" is not defined
#endif

#ifndef U8ETCSCpFtr4JmpUpMin
#error The variable for the parameter "U8ETCSCpFtr4JmpUpMin" is not defined
#endif

#ifndef U8ETCSCpFtr4RefTrqAt2ndCylStrt
#error The variable for the parameter "U8ETCSCpFtr4RefTrqAt2ndCylStrt" is not defined
#endif

#ifndef U8ETCSCpFtr4RefTrqAtVcaOfCtl
#error The variable for the parameter "U8ETCSCpFtr4RefTrqAtVcaOfCtl" is not defined
#endif

#ifndef U8ETCSCpGainTrnsTmAftrGearShft
#error The variable for the parameter "U8ETCSCpGainTrnsTmAftrGearShft" is not defined
#endif

#ifndef U8ETCSCpGradientTrstCplCnt
#error The variable for the parameter "U8ETCSCpGradientTrstCplCnt" is not defined
#endif

#ifndef U8ETCSCpGradientTrstStCnt
#error The variable for the parameter "U8ETCSCpGradientTrstStCnt" is not defined
#endif

#ifndef U8ETCSCpGrdTransTme
#error The variable for the parameter "U8ETCSCpGrdTransTme" is not defined
#endif

#ifndef U8ETCSCpH2LDctHldTm
#error The variable for the parameter "U8ETCSCpH2LDctHldTm" is not defined
#endif

#ifndef U8ETCSCpH2LDctSpnTh
#error The variable for the parameter "U8ETCSCpH2LDctSpnTh" is not defined
#endif

#ifndef U8ETCSCpHiAxlAccTh
#error The variable for the parameter "U8ETCSCpHiAxlAccTh" is not defined
#endif

#ifndef U8ETCSCpHillClrTime
#error The variable for the parameter "U8ETCSCpHillClrTime" is not defined
#endif

#ifndef U8ETCSCpHillDctAlongTh
#error The variable for the parameter "U8ETCSCpHillDctAlongTh" is not defined
#endif

#ifndef U8ETCSCpHillDctTime
#error The variable for the parameter "U8ETCSCpHillDctTime" is not defined
#endif

#ifndef U8ETCSCpHillDctTimeAx
#error The variable for the parameter "U8ETCSCpHillDctTimeAx" is not defined
#endif

#ifndef U8ETCSCpHillDiffAx
#error The variable for the parameter "U8ETCSCpHillDiffAx" is not defined
#endif

#ifndef U8ETCSCpHsaHillTime
#error The variable for the parameter "U8ETCSCpHsaHillTime" is not defined
#endif

#ifndef U8ETCSCpHysDeltAcc
#error The variable for the parameter "U8ETCSCpHysDeltAcc" is not defined
#endif

#ifndef U8ETCSCpHysDepSnwHilCnt
#error The variable for the parameter "U8ETCSCpHysDepSnwHilCnt" is not defined
#endif

#ifndef U8ETCSCpLowWhlSpnCntDnTh
#error The variable for the parameter "U8ETCSCpLowWhlSpnCntDnTh" is not defined
#endif

#ifndef U8ETCSCpLowWhlSpnCntUpTh
#error The variable for the parameter "U8ETCSCpLowWhlSpnCntUpTh" is not defined
#endif

#ifndef U8ETCSCpLwAxlAccTh
#error The variable for the parameter "U8ETCSCpLwAxlAccTh" is not defined
#endif

#ifndef U8ETCSCpLwToHiDetAreaCnt
#error The variable for the parameter "U8ETCSCpLwToHiDetAreaCnt" is not defined
#endif

#ifndef U8ETCSCpLwToHiDetCnt
#error The variable for the parameter "U8ETCSCpLwToHiDetCnt" is not defined
#endif

#ifndef U8ETCSCpLwWhlSpnTh2ClrCyl1st
#error The variable for the parameter "U8ETCSCpLwWhlSpnTh2ClrCyl1st" is not defined
#endif

#ifndef U8ETCSCpNAlwAddTar4DepSnwSpd
#error The variable for the parameter "U8ETCSCpNAlwAddTar4DepSnwSpd" is not defined
#endif

#ifndef U8ETCSCpRecTrqFacInBump
#error The variable for the parameter "U8ETCSCpRecTrqFacInBump" is not defined
#endif

#ifndef U8ETCSCpRefTrqNvldTm
#error The variable for the parameter "U8ETCSCpRefTrqNvldTm" is not defined
#endif

#ifndef U8ETCSCpRefTrqVldTm
#error The variable for the parameter "U8ETCSCpRefTrqVldTm" is not defined
#endif

#ifndef U8ETCSCpSplitHillClrAccel
#error The variable for the parameter "U8ETCSCpSplitHillClrAccel" is not defined
#endif

#ifndef U8ETCSCpSplitHillClrVehSpd
#error The variable for the parameter "U8ETCSCpSplitHillClrVehSpd" is not defined
#endif

#ifndef U8ETCSCpSplitHillDctAccel
#error The variable for the parameter "U8ETCSCpSplitHillDctAccel" is not defined
#endif

#ifndef U8ETCSCpSplitHillDctMinAx
#error The variable for the parameter "U8ETCSCpSplitHillDctMinAx" is not defined
#endif

#ifndef U8ETCSCpSplitHillDctVehSpd
#error The variable for the parameter "U8ETCSCpSplitHillDctVehSpd" is not defined
#endif

#ifndef U8ETCSCpStDeltAcc
#error The variable for the parameter "U8ETCSCpStDeltAcc" is not defined
#endif

#ifndef U8ETCSCpSymBrkCtlIhbAxTh
#error The variable for the parameter "U8ETCSCpSymBrkCtlIhbAxTh" is not defined
#endif

#ifndef U8ETCSCpTarTransCplBrkTrq2Sp
#error The variable for the parameter "U8ETCSCpTarTransCplBrkTrq2Sp" is not defined
#endif

#ifndef U8ETCSCpTarTransStBrkTrq2Sp
#error The variable for the parameter "U8ETCSCpTarTransStBrkTrq2Sp" is not defined
#endif

#ifndef U8ETCSCpTimeTh2ClrCyl1st
#error The variable for the parameter "U8ETCSCpTimeTh2ClrCyl1st" is not defined
#endif

#ifndef U8TCSCpAddRefRdCfFrci
#error The variable for the parameter "U8TCSCpAddRefRdCfFrci" is not defined
#endif

#ifndef U8TCSCpHghMuWhlSpinTh
#error The variable for the parameter "U8TCSCpHghMuWhlSpinTh" is not defined
#endif

#ifndef VREF_10_KPH
#error The variable for the parameter "VREF_10_KPH" is not defined
#endif

#ifndef VREF_12_5_KPH
#error The variable for the parameter "VREF_12_5_KPH" is not defined
#endif

#ifndef VREF_30_KPH
#error The variable for the parameter "VREF_30_KPH" is not defined
#endif

#ifndef VREF_6_KPH
#error The variable for the parameter "VREF_6_KPH" is not defined
#endif

#ifndef VREF_DECEL_0_3_G
#error The variable for the parameter "VREF_DECEL_0_3_G" is not defined
#endif

#ifndef VarFwd
#error The variable for the parameter "VarFwd" is not defined
#endif

/* Macros for accessing real-time model data structure */
#ifndef rtmGetT
# define rtmGetT()                     (*(LCETCS_vCallDetection_TimingBrdg->taskTime[0]))
#endif
#endif                                 /* RTW_HEADER_LCETCS_vCallDetection_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
