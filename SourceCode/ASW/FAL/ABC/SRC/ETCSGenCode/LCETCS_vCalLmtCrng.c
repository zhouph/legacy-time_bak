/*
 * File: LCETCS_vCalLmtCrng.c
 *
 * Code generated for Simulink model 'LCETCS_vCalLmtCrng'.
 *
 * Model version                  : 1.76
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:59:43 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalLmtCrng.h"
#include "LCETCS_vCalLmtCrng_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalLmtCrng' */
void LCETCS_vCalLmtCrng_Init(boolean_T *rty_lcetcsu1DctLmtCrng, boolean_T
  *rty_lcetcsu1DctLmtCrngFalEdg, DW_LCETCS_vCalLmtCrng_f_T *localDW)
{
  /* InitializeConditions for Chart: '<Root>/Chart' */
  *rty_lcetcsu1DctLmtCrng = false;

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctFallingEdge' */
  LCETCS_vDctFallingEdge_Init(rty_lcetcsu1DctLmtCrngFalEdg,
    &(localDW->LCETCS_vDctFallingEdge_DWORK1.rtdw));
}

/* Output and update for referenced model: 'LCETCS_vCalLmtCrng' */
void LCETCS_vCalLmtCrng(int16_T rtu_lcetcss16LmtCrngDltYaw, const
  TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, boolean_T *rty_lcetcsu1DctLmtCrng,
  boolean_T *rty_lcetcsu1DctLmtCrngFalEdg, DW_LCETCS_vCalLmtCrng_f_T *localDW)
{
  /* Chart: '<Root>/Chart' */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:34' */
  if (rtu_ETCS_ESC_SIG->lcetcss16AbsDelYawFrst >= rtu_lcetcss16LmtCrngDltYaw) {
    /* Transition: '<S1>:171' */
    /* Transition: '<S1>:173' */
    *rty_lcetcsu1DctLmtCrng = true;

    /* Transition: '<S1>:176' */
  } else {
    /* Transition: '<S1>:175' */
    *rty_lcetcsu1DctLmtCrng = false;
  }

  /* End of Chart: '<Root>/Chart' */
  /* Transition: '<S1>:178' */

  /* ModelReference: '<Root>/LCETCS_vDctFallingEdge' */
  LCETCS_vDctFallingEdge((*rty_lcetcsu1DctLmtCrng), rty_lcetcsu1DctLmtCrngFalEdg,
    &(localDW->LCETCS_vDctFallingEdge_DWORK1.rtdw));
}

/* Model initialize function */
void LCETCS_vCalLmtCrng_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctFallingEdge' */
  LCETCS_vDctFallingEdge_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
