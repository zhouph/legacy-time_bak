/*
 * File: LCETCS_vCordPCtlGain.c
 *
 * Code generated for Simulink model 'LCETCS_vCordPCtlGain'.
 *
 * Model version                  : 1.195
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:51:54 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCordPCtlGain.h"
#include "LCETCS_vCordPCtlGain_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCordPCtlGain' */
void LCETCS_vCordPCtlGain_Init(int16_T *rty_lcetcss16PgainFac)
{
  /* InitializeConditions for Chart: '<Root>/Chart' */
  *rty_lcetcss16PgainFac = 0;
}

/* Output and update for referenced model: 'LCETCS_vCordPCtlGain' */
void LCETCS_vCordPCtlGain(int16_T rtu_lcetcss16BsPgain, int16_T
  rtu_lcetcss16PgainFacInGearChng, int16_T rtu_lcetcss16PGainIncFacH2L, const
  TypeETCSH2LStruct *rtu_ETCS_H2L, int16_T rtu_lcetcss16PGainFacInTrn, const
  TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const TypeETCSTarSpinStruct
  *rtu_ETCS_TAR_SPIN, const TypeETCSGainGearShftStruct *rtu_ETCS_GAIN_GEAR_SHFT,
  int8_T rtu_lcetcss8PgainFacInDrvVibCnt, int16_T rtu_lcetcss16PGainFacInDrvVib,
  int16_T *rty_lcetcss16Pgain, int16_T *rty_lcetcss16PgainFac)
{
  int32_T rtb_Product;
  int16_T u0;

  /* Chart: '<Root>/Chart' */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:92' */
  /*  Priority #1 : High-to-low mu transition detection  */
  if (rtu_ETCS_H2L->lcetcsu1HiToLw == 1) {
    /* Transition: '<S1>:94' */
    /* Transition: '<S1>:99' */
    *rty_lcetcss16PgainFac = rtu_lcetcss16PGainIncFacH2L;

    /* Transition: '<S1>:98' */
    /* Transition: '<S1>:97' */
    /* Transition: '<S1>:113' */
    /* Transition: '<S1>:114' */
  } else {
    /* Transition: '<S1>:93' */
    /*  Priority #2 : Gear change detection  */
    if ((rtu_ETCS_GAIN_GEAR_SHFT->lcetcsu1GainChngInGearShft == 1) ||
        (rtu_ETCS_GAIN_GEAR_SHFT->lcetcsu1GainTrnsAftrGearShft == 1)) {
      /* Transition: '<S1>:101' */
      /* Transition: '<S1>:100' */
      *rty_lcetcss16PgainFac = rtu_lcetcss16PgainFacInGearChng;

      /* Transition: '<S1>:97' */
      /* Transition: '<S1>:113' */
      /* Transition: '<S1>:114' */
    } else {
      /* Transition: '<S1>:96' */
      /*  Priority #3 : Limit cornering  */
      if (rtu_ETCS_ESC_SIG->lcetcss16AbsDelYawFrst >=
          rtu_ETCS_TAR_SPIN->lcetcss16LmtCrngDltYaw) {
        /* Transition: '<S1>:102' */
        /* Transition: '<S1>:103' */
        *rty_lcetcss16PgainFac = rtu_lcetcss16PGainFacInTrn;

        /* Transition: '<S1>:113' */
        /* Transition: '<S1>:114' */
      } else {
        /* Transition: '<S1>:110' */
        /*  Priority #4 : Driveline vibration detection  */
        if (rtu_lcetcss8PgainFacInDrvVibCnt > 0) {
          /* Transition: '<S1>:116' */
          /* Transition: '<S1>:117' */
          *rty_lcetcss16PgainFac = rtu_lcetcss16PGainFacInDrvVib;

          /* Transition: '<S1>:114' */
        } else {
          /* Transition: '<S1>:115' */
          /*  100% when nothing happens  */
          *rty_lcetcss16PgainFac = 100;
        }
      }
    }
  }

  /* End of Chart: '<Root>/Chart' */

  /* Product: '<Root>/Product' */
  /* Transition: '<S1>:95' */
  rtb_Product = rtu_lcetcss16BsPgain * (*rty_lcetcss16PgainFac);

  /* Saturate: '<Root>/Saturation' incorporates:
   *  Product: '<Root>/Divide'
   */
  u0 = (int16_T)(rtb_Product / 100);
  if (u0 > 255) {
    *rty_lcetcss16Pgain = 255;
  } else {
    *rty_lcetcss16Pgain = u0;
  }

  /* End of Saturate: '<Root>/Saturation' */
}

/* Model initialize function */
void LCETCS_vCordPCtlGain_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
