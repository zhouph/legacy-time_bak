/*
 * File: LCETCS_vCalTarSpnLmtCrnrg.h
 *
 * Code generated for Simulink model 'LCETCS_vCalTarSpnLmtCrnrg'.
 *
 * Model version                  : 1.187
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:07:28 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalTarSpnLmtCrnrg_h_
#define RTW_HEADER_LCETCS_vCalTarSpnLmtCrnrg_h_
#ifndef LCETCS_vCalTarSpnLmtCrnrg_COMMON_INCLUDES_
# define LCETCS_vCalTarSpnLmtCrnrg_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalTarSpnLmtCrnrg_COMMON_INCLUDES_ */

#include "LCETCS_vCalTarSpnLmtCrnrg_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vCalTarDecMd.h"
#include "LCETCS_vCalTarDecInTrn.h"
#include "LCETCS_vCalTarDecDpndDelYaw.h"
#include "LCETCS_vCalTarDecCnt.h"
#include "LCETCS_vCalMaxDltYawInTarDecMd.h"
#include "LCETCS_vCalLmtCrngDltYaw.h"
#include "LCETCS_vCalLmtCrng.h"
#include "LCETCS_vCalFnlTarSpnDec.h"

/* Block states (auto storage) for model 'LCETCS_vCalTarSpnLmtCrnrg' */
typedef struct {
  MdlrefDW_LCETCS_vCalFnlTarSpnDec_T LCETCS_vCalFnlTarSpnDec_DWORK1;/* '<Root>/LCETCS_vCalFnlTarSpnDec' */
  MdlrefDW_LCETCS_vCalLmtCrngDltYaw_T LCETCS_vCalLmtCrngDltYaw_DWORK1;/* '<Root>/LCETCS_vCalLmtCrngDltYaw' */
  MdlrefDW_LCETCS_vCalLmtCrng_T LCETCS_vCalInLmtCrng_DWORK1;/* '<Root>/LCETCS_vCalInLmtCrng' */
  MdlrefDW_LCETCS_vCalTarDecMd_T LCETCS_vCalTarDecMd_DWORK1;/* '<Root>/LCETCS_vCalTarDecMd' */
  MdlrefDW_LCETCS_vCalMaxDltYawInTarDecMd_T
    LCETCS_vCalMaxDltYawInTarDecMd_DWORK1;/* '<Root>/LCETCS_vCalMaxDltYawInTarDecMd' */
  MdlrefDW_LCETCS_vCalTarDecCnt_T LCETCS_vCalTarDecCnt_DWORK1;/* '<Root>/LCETCS_vCalTarDecCnt' */
  MdlrefDW_LCETCS_vCalTarDecDpndDelYaw_T LCETCS_vCalTarDecDpndDelYaw_DWORK1;/* '<Root>/LCETCS_vCalTarDecDpndDelYaw' */
  MdlrefDW_LCETCS_vCalTarDecInTrn_T LCETCS_vCalTarDecInTrn_DWORK1;/* '<Root>/LCETCS_vCalTarDecInTrn' */
} DW_LCETCS_vCalTarSpnLmtCrnrg_f_T;

typedef struct {
  DW_LCETCS_vCalTarSpnLmtCrnrg_f_T rtdw;
} MdlrefDW_LCETCS_vCalTarSpnLmtCrnrg_T;

/* Model reference registration function */
extern void LCETCS_vCalTarSpnLmtCrnrg_initialize(void);
extern void LCETCS_vCalTarSpnLmtCrnrg_Init(boolean_T *rty_lcetcsu1TarSpnDec,
  boolean_T *rty_lcetcsu1DctLmtCrng, boolean_T *rty_lcetcsu1DctLmtCrngFalEdg,
  DW_LCETCS_vCalTarSpnLmtCrnrg_f_T *localDW);
extern void LCETCS_vCalTarSpnLmtCrnrg_Start(DW_LCETCS_vCalTarSpnLmtCrnrg_f_T
  *localDW);
extern void LCETCS_vCalTarSpnLmtCrnrg(int16_T rtu_lcetcss16VehSpd, const
  TypeETCSAxlStruct *rtu_ETCS_FA, const TypeETCSAxlStruct *rtu_ETCS_RA, const
  TypeETCSExtDctStruct *rtu_ETCS_EXT_DCT, const TypeETCSEscSigStruct
  *rtu_ETCS_ESC_SIG, const TypeETCSStrStatStruct *rtu_ETCS_STR_STAT, int16_T
  *rty_lcetcss16TarDecInTrn, int16_T *rty_lcetcss16DltYawStbCnt, boolean_T
  *rty_lcetcsu1TarSpnDec, int16_T *rty_lcetcss16TarSpnDecCnt, int16_T
  *rty_lcetcss16MaxDltYawInTarDecMd, int16_T *rty_lcetcss16TarDecDpndDelYaw,
  int16_T *rty_lcetcss16FnlTarSpnDec, boolean_T *rty_lcetcsu1DctLmtCrng,
  boolean_T *rty_lcetcsu1DctLmtCrngFalEdg, int16_T *rty_lcetcss16LmtCrngDltYaw,
  DW_LCETCS_vCalTarSpnLmtCrnrg_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalTarSpnLmtCrnrg'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalTarSpnLmtCrnrg_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
