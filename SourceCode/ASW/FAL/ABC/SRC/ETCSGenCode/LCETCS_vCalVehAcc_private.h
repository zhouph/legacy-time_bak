/*
 * File: LCETCS_vCalVehAcc_private.h
 *
 * Code generated for Simulink model 'LCETCS_vCalVehAcc'.
 *
 * Model version                  : 1.509
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:07:51 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalVehAcc_private_h_
#define RTW_HEADER_LCETCS_vCalVehAcc_private_h_
#include "rtwtypes.h"
#include "model_reference_types.h"

/* Includes for objects with custom storage classes. */
#include "../ETCSGenCodeHeader/ETCSHeader4Import.h"

/*
 * Generate compile time checks that imported macros for parameters
 * with storage class "ImportedDefine" are defined
 */
#ifndef S16ETCSCpTotalGearRatio_1
#error The variable for the parameter "S16ETCSCpTotalGearRatio_1" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_2
#error The variable for the parameter "S16ETCSCpTotalGearRatio_2" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_3
#error The variable for the parameter "S16ETCSCpTotalGearRatio_3" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_4
#error The variable for the parameter "S16ETCSCpTotalGearRatio_4" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_5
#error The variable for the parameter "S16ETCSCpTotalGearRatio_5" is not defined
#endif

#ifndef U8ETCSCpAxAtAsphalt
#error The variable for the parameter "U8ETCSCpAxAtAsphalt" is not defined
#endif

#ifndef U8ETCSCpAxAtIce
#error The variable for the parameter "U8ETCSCpAxAtIce" is not defined
#endif

#ifndef U8ETCSCpAxAtSnow
#error The variable for the parameter "U8ETCSCpAxAtSnow" is not defined
#endif

#ifndef U8ETCSCpAxGear2AtAsphalt
#error The variable for the parameter "U8ETCSCpAxGear2AtAsphalt" is not defined
#endif

#ifndef U8ETCSCpAxGear2AtIce
#error The variable for the parameter "U8ETCSCpAxGear2AtIce" is not defined
#endif

#ifndef U8ETCSCpAxGear2AtSnow
#error The variable for the parameter "U8ETCSCpAxGear2AtSnow" is not defined
#endif

#ifndef U8ETCSCpAxGear3AtAsphalt
#error The variable for the parameter "U8ETCSCpAxGear3AtAsphalt" is not defined
#endif

#ifndef U8ETCSCpAxGear3AtIce
#error The variable for the parameter "U8ETCSCpAxGear3AtIce" is not defined
#endif

#ifndef U8ETCSCpAxGear3AtSnow
#error The variable for the parameter "U8ETCSCpAxGear3AtSnow" is not defined
#endif

#ifndef U8ETCSCpAxGear4AtAsphalt
#error The variable for the parameter "U8ETCSCpAxGear4AtAsphalt" is not defined
#endif

#ifndef U8ETCSCpAxGear4AtIce
#error The variable for the parameter "U8ETCSCpAxGear4AtIce" is not defined
#endif

#ifndef U8ETCSCpAxGear4AtSnow
#error The variable for the parameter "U8ETCSCpAxGear4AtSnow" is not defined
#endif

#ifndef U8ETCSCpAxGear5AtAsphalt
#error The variable for the parameter "U8ETCSCpAxGear5AtAsphalt" is not defined
#endif

#ifndef U8ETCSCpAxGear5AtIce
#error The variable for the parameter "U8ETCSCpAxGear5AtIce" is not defined
#endif

#ifndef U8ETCSCpAxGear5AtSnow
#error The variable for the parameter "U8ETCSCpAxGear5AtSnow" is not defined
#endif

#ifndef U8ETCSCpAxSenAvail
#error The variable for the parameter "U8ETCSCpAxSenAvail" is not defined
#endif

#ifndef U8ETCSCpAyAsp
#error The variable for the parameter "U8ETCSCpAyAsp" is not defined
#endif

#ifndef U8ETCSCpAyIce
#error The variable for the parameter "U8ETCSCpAyIce" is not defined
#endif

#ifndef U8ETCSCpAySnw
#error The variable for the parameter "U8ETCSCpAySnw" is not defined
#endif
#endif                                 /* RTW_HEADER_LCETCS_vCalVehAcc_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
