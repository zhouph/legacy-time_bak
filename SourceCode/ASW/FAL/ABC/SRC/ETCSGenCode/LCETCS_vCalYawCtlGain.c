/*
 * File: LCETCS_vCalYawCtlGain.c
 *
 * Code generated for Simulink model 'LCETCS_vCalYawCtlGain'.
 *
 * Model version                  : 1.219
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:08:39 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalYawCtlGain.h"
#include "LCETCS_vCalYawCtlGain_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalYawCtlGain' */
void LCETCS_vCalYawCtlGain_Init(boolean_T *rty_lcetcsu1DelYawDivrg,
  DW_LCETCS_vCalYawCtlGain_f_T *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalDelYawCnvrgGain' */
  LCETCS_vCalDelYawCnvrgGain_Init
    (&(localDW->LCETCS_vCalDelYawCnvrgGain_DWORK1.rtb));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalDelYawDivrgGain' */
  LCETCS_vCalDelYawDivrgGain_Init
    (&(localDW->LCETCS_vCalDelYawDivrgGain_DWORK1.rtb));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalDelYawTrend' */
  LCETCS_vCalDelYawTrend_Init(rty_lcetcsu1DelYawDivrg,
    &(localDW->LCETCS_vCalDelYawTrend_DWORK1.rtdw));
}

/* Start for referenced model: 'LCETCS_vCalYawCtlGain' */
void LCETCS_vCalYawCtlGain_Start(DW_LCETCS_vCalYawCtlGain_f_T *localDW)
{
  /* Start for ModelReference: '<Root>/LCETCS_vCalBasDelYawCnvrgGain' */
  LCETCS_vCalBasDelYawCnvrgGain_Start
    (&(localDW->LCETCS_vCalBasDelYawCnvrgGain_DWORK1.rtb));

  /* Start for ModelReference: '<Root>/LCETCS_vCalDelYawCnvrgGain' */
  LCETCS_vCalDelYawCnvrgGain_Start
    (&(localDW->LCETCS_vCalDelYawCnvrgGain_DWORK1.rtb));

  /* Start for ModelReference: '<Root>/LCETCS_vCalDelYawDivrgGain' */
  LCETCS_vCalDelYawDivrgGain_Start
    (&(localDW->LCETCS_vCalDelYawDivrgGain_DWORK1.rtb));
}

/* Output and update for referenced model: 'LCETCS_vCalYawCtlGain' */
void LCETCS_vCalYawCtlGain(const TypeETCSCtlCmdStruct *rtu_ETCS_CTL_CMD_OLD,
  int16_T rtu_lcetcss16VehSpd, const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG,
  const TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN, int16_T
  *rty_lcetcss16DelYawGain, boolean_T *rty_lcetcsu1DelYawDivrg, int16_T
  *rty_lcetcss16DelYawDivrgCnt, int16_T *rty_lcetcss16DelYawDivrgGain, int16_T
  *rty_lcetcss16DelYawCnvrgGain, DW_LCETCS_vCalYawCtlGain_f_T *localDW)
{
  /* local block i/o variables */
  int16_T rtb_lcetcss16BasDelYawCnvrgGain;

  /* ModelReference: '<Root>/LCETCS_vCalBasDelYawCnvrgGain' */
  LCETCS_vCalBasDelYawCnvrgGain(rtu_ETCS_ESC_SIG, rtu_lcetcss16VehSpd,
    rtu_ETCS_TAR_SPIN, &rtb_lcetcss16BasDelYawCnvrgGain,
    &(localDW->LCETCS_vCalBasDelYawCnvrgGain_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vCalDelYawCnvrgGain' */
  LCETCS_vCalDelYawCnvrgGain(rtu_ETCS_CTL_CMD_OLD, rtu_ETCS_ESC_SIG,
    rtb_lcetcss16BasDelYawCnvrgGain, rty_lcetcss16DelYawCnvrgGain,
    &(localDW->LCETCS_vCalDelYawCnvrgGain_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vCalDelYawDivrgGain' */
  LCETCS_vCalDelYawDivrgGain(rtu_lcetcss16VehSpd, rtu_ETCS_ESC_SIG,
    rty_lcetcss16DelYawDivrgGain,
    &(localDW->LCETCS_vCalDelYawDivrgGain_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vCalDelYawTrend' */
  LCETCS_vCalDelYawTrend(rtu_ETCS_ESC_SIG, rtu_ETCS_TAR_SPIN,
    rty_lcetcsu1DelYawDivrg, rty_lcetcss16DelYawDivrgCnt,
    &(localDW->LCETCS_vCalDelYawTrend_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalDelYawGain' */
  LCETCS_vCalDelYawGain((*rty_lcetcsu1DelYawDivrg),
                        (*rty_lcetcss16DelYawDivrgGain),
                        (*rty_lcetcss16DelYawCnvrgGain), rtu_ETCS_ESC_SIG,
                        rtu_ETCS_TAR_SPIN, rty_lcetcss16DelYawGain);
}

/* Model initialize function */
void LCETCS_vCalYawCtlGain_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalBasDelYawCnvrgGain' */
  LCETCS_vCalBasDelYawCnvrgGain_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalDelYawCnvrgGain' */
  LCETCS_vCalDelYawCnvrgGain_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalDelYawDivrgGain' */
  LCETCS_vCalDelYawDivrgGain_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalDelYawGain' */
  LCETCS_vCalDelYawGain_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalDelYawTrend' */
  LCETCS_vCalDelYawTrend_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
