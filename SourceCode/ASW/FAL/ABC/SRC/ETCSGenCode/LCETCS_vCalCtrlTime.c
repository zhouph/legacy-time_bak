/*
 * File: LCETCS_vCalCtrlTime.c
 *
 * Code generated for Simulink model 'LCETCS_vCalCtrlTime'.
 *
 * Model version                  : 1.214
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:54:14 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalCtrlTime.h"
#include "LCETCS_vCalCtrlTime_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalCtrlTime' */
void LCETCS_vCalCtrlTime_Init(DW_LCETCS_vCalCtrlTime_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = 0;
}

/* Output and update for referenced model: 'LCETCS_vCalCtrlTime' */
void LCETCS_vCalCtrlTime(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, int16_T
  *rty_lcetcss16CtlActTime, DW_LCETCS_vCalCtrlTime_f_T *localDW)
{
  int32_T tmp;

  /* Chart: '<Root>/Chart' incorporates:
   *  UnitDelay: '<Root>/Unit Delay2'
   */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:56' */
  /* comment */
  if (rtu_ETCS_CTL_ACT->lcetcsu1CtlAct == 1) {
    /* Transition: '<S1>:14' */
    /* Transition: '<S1>:36' */
    tmp = localDW->UnitDelay2_DSTATE + 1;
    if (tmp > 32767) {
      tmp = 32767;
    }

    *rty_lcetcss16CtlActTime = (int16_T)tmp;

    /* Transition: '<S1>:81' */
  } else {
    /* Transition: '<S1>:80' */
    *rty_lcetcss16CtlActTime = 0;
  }

  /* End of Chart: '<Root>/Chart' */

  /* Saturate: '<Root>/Saturation' */
  /* Transition: '<S1>:67' */
  if ((*rty_lcetcss16CtlActTime) > 500) {
    *rty_lcetcss16CtlActTime = 500;
  } else {
    if ((*rty_lcetcss16CtlActTime) < 0) {
      *rty_lcetcss16CtlActTime = 0;
    }
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Update for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = *rty_lcetcss16CtlActTime;
}

/* Model initialize function */
void LCETCS_vCalCtrlTime_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
