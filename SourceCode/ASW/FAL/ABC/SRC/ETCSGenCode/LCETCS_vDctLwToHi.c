/*
 * File: LCETCS_vDctLwToHi.c
 *
 * Code generated for Simulink model 'LCETCS_vDctLwToHi'.
 *
 * Model version                  : 1.165
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:06:08 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctLwToHi.h"
#include "LCETCS_vDctLwToHi_private.h"

/* Initial conditions for referenced model: 'LCETCS_vDctLwToHi' */
void LCETCS_vDctLwToHi_Init(DW_LCETCS_vDctLwToHi_f_T *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctLwToHibyArea' */
  LCETCS_vDctLwToHibyArea_Init(&(localDW->LCETCS_vDctLwToHibyArea_DWORK1.rtb),
                               &(localDW->LCETCS_vDctLwToHibyArea_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctLwToHibyDecel' */
  LCETCS_vDctLwToHibyDecel_Init(&(localDW->LCETCS_vDctLwToHibyDecel_DWORK1.rtdw));
}

/* Output and update for referenced model: 'LCETCS_vDctLwToHi' */
void LCETCS_vDctLwToHi(const TypeETCSWhlAccStruct *rtu_ETCS_WHL_ACC, const
  TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const TypeETCSWhlSpinStruct
  *rtu_ETCS_WHL_SPIN, const TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN, int16_T
  *rty_lcetcss16LwToHiCnt, int16_T *rty_lcetcss16LwToHiCntbyArea,
  DW_LCETCS_vDctLwToHi_f_T *localDW)
{
  /* ModelReference: '<Root>/LCETCS_vDctLwToHibyArea' */
  LCETCS_vDctLwToHibyArea(rtu_ETCS_TAR_SPIN, rtu_ETCS_CTL_ACT, rtu_ETCS_WHL_SPIN,
    rty_lcetcss16LwToHiCntbyArea, &(localDW->LCETCS_vDctLwToHibyArea_DWORK1.rtb),
    &(localDW->LCETCS_vDctLwToHibyArea_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vDctLwToHibyDecel' */
  LCETCS_vDctLwToHibyDecel(rtu_ETCS_WHL_ACC, rtu_ETCS_CTL_ACT, rtu_ETCS_WHL_SPIN,
    rtu_ETCS_TAR_SPIN, rty_lcetcss16LwToHiCnt,
    &(localDW->LCETCS_vDctLwToHibyDecel_DWORK1.rtdw));
}

/* Model initialize function */
void LCETCS_vDctLwToHi_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctLwToHibyArea' */
  LCETCS_vDctLwToHibyArea_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctLwToHibyDecel' */
  LCETCS_vDctLwToHibyDecel_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
