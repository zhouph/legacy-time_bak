/*
 * File: LCETCS_vCalEngStallThr.c
 *
 * Code generated for Simulink model 'LCETCS_vCalEngStallThr'.
 *
 * Model version                  : 1.251
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:05:52 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalEngStallThr.h"
#include "LCETCS_vCalEngStallThr_private.h"

/* Start for referenced model: 'LCETCS_vCalEngStallThr' */
void LCETCS_vCalEngStallThr_Start(B_LCETCS_vCalEngStallThr_c_T *localB)
{
  /* InitializeConditions for IfAction SubSystem: '<Root>/Engine Stall Risk Index on Hill' */

  /* InitializeConditions for ModelReference: '<S2>/LCETCS_s16Inter3Point' */
  LCETCS_s16Inter3Point_Init(&localB->lcetcss16EngStallThrTemp_n);

  /* End of InitializeConditions for SubSystem: '<Root>/Engine Stall Risk Index on Hill' */

  /* InitializeConditions for IfAction SubSystem: '<Root>/Engine Stall Risk Index on Flat' */

  /* InitializeConditions for ModelReference: '<S1>/Model1' */
  LCETCS_s16Inter3Point_Init(&localB->lcetcss16EngStallThrTemp);

  /* End of InitializeConditions for SubSystem: '<Root>/Engine Stall Risk Index on Flat' */
}

/* Output and update for referenced model: 'LCETCS_vCalEngStallThr' */
void LCETCS_vCalEngStallThr(int8_T rtu_lcetcss8EngStallRiskIndex, const
  TypeETCSSplitHillStruct *rtu_ETCS_SPLIT_HILL_OLD, int16_T
  *rty_lcetcss16EngStallThrTemp, B_LCETCS_vCalEngStallThr_c_T *localB)
{
  /* local block i/o variables */
  int16_T rtb_x;
  int16_T rtb_x_l;

  /* If: '<Root>/If' incorporates:
   *  Constant: '<S1>/Constant1'
   *  Constant: '<S1>/Constant2'
   *  Constant: '<S1>/Constant6'
   *  Constant: '<S1>/High Risk Stall RPM on Flat'
   *  Constant: '<S1>/Low Risk Stall RPM on Flat'
   *  Constant: '<S1>/Mid Risk Stall RPM on Flat'
   *  Constant: '<S2>/Constant1'
   *  Constant: '<S2>/Constant2'
   *  Constant: '<S2>/Constant6'
   *  Constant: '<S2>/High Risk Stall RPM on Hill'
   *  Constant: '<S2>/Low Risk Stall RPM on Hill'
   *  Constant: '<S2>/Mid Risk Stall RPM on Hill'
   */
  if (rtu_ETCS_SPLIT_HILL_OLD->lcetcsu1SplitHillDct) {
    /* Outputs for IfAction SubSystem: '<Root>/Engine Stall Risk Index on Hill' incorporates:
     *  ActionPort: '<S2>/Action Port'
     */
    /* DataTypeConversion: '<S2>/Data Type Conversion1' */
    rtb_x_l = rtu_lcetcss8EngStallRiskIndex;

    /* ModelReference: '<S2>/LCETCS_s16Inter3Point' */
    LCETCS_s16Inter3Point(rtb_x_l, rtCP_Constant6_Value, rtCP_Constant1_Value,
                          rtCP_Constant2_Value, ((int16_T)
      S16ETCSCpLowRiskStallRpmHill), ((int16_T)S16ETCSCpMidRiskStallRpmHill),
                          ((int16_T)S16ETCSCpHighRiskStallRpmHill),
                          &localB->lcetcss16EngStallThrTemp_n);

    /* SignalConversion: '<S2>/OutportBufferForlcetcss16EngStallThrTemp' incorporates:
     *  Constant: '<S2>/Constant1'
     *  Constant: '<S2>/Constant2'
     *  Constant: '<S2>/Constant6'
     *  Constant: '<S2>/High Risk Stall RPM on Hill'
     *  Constant: '<S2>/Low Risk Stall RPM on Hill'
     *  Constant: '<S2>/Mid Risk Stall RPM on Hill'
     */
    *rty_lcetcss16EngStallThrTemp = localB->lcetcss16EngStallThrTemp_n;

    /* End of Outputs for SubSystem: '<Root>/Engine Stall Risk Index on Hill' */
  } else {
    /* Outputs for IfAction SubSystem: '<Root>/Engine Stall Risk Index on Flat' incorporates:
     *  ActionPort: '<S1>/Action Port'
     */
    /* DataTypeConversion: '<S1>/Data Type Conversion1' */
    rtb_x = rtu_lcetcss8EngStallRiskIndex;

    /* ModelReference: '<S1>/Model1' */
    LCETCS_s16Inter3Point(rtb_x, rtCP_Constant6_Value_c, rtCP_Constant1_Value_h,
                          rtCP_Constant2_Value_o, ((int16_T)
      S16ETCSCpLowRiskStallRpmFlat), ((int16_T)S16ETCSCpMidRiskStallRpmFlat),
                          ((int16_T)S16ETCSCpHighRiskStallRpmFlat),
                          &localB->lcetcss16EngStallThrTemp);

    /* SignalConversion: '<S1>/OutportBufferForlcetcss16EngStallThrTemp' incorporates:
     *  Constant: '<S1>/Constant1'
     *  Constant: '<S1>/Constant2'
     *  Constant: '<S1>/Constant6'
     *  Constant: '<S1>/High Risk Stall RPM on Flat'
     *  Constant: '<S1>/Low Risk Stall RPM on Flat'
     *  Constant: '<S1>/Mid Risk Stall RPM on Flat'
     */
    *rty_lcetcss16EngStallThrTemp = localB->lcetcss16EngStallThrTemp;

    /* End of Outputs for SubSystem: '<Root>/Engine Stall Risk Index on Flat' */
  }

  /* End of If: '<Root>/If' */
}

/* Model initialize function */
void LCETCS_vCalEngStallThr_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<S1>/Model1' */
  LCETCS_s16Inter3Point_initialize();

  /* Model Initialize fcn for ModelReference Block: '<S2>/LCETCS_s16Inter3Point' */
  LCETCS_s16Inter3Point_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
