/*
 * File: LCETCS_vCalCtlGain.h
 *
 * Code generated for Simulink model 'LCETCS_vCalCtlGain'.
 *
 * Model version                  : 1.230
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:12:14 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalCtlGain_h_
#define RTW_HEADER_LCETCS_vCalCtlGain_h_
#ifndef LCETCS_vCalCtlGain_COMMON_INCLUDES_
# define LCETCS_vCalCtlGain_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalCtlGain_COMMON_INCLUDES_ */

#include "LCETCS_vCalCtlGain_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vCalYawCtlGain.h"
#include "LCETCS_vCalPCtlGain.h"
#include "LCETCS_vCalICtlGain.h"

/* Block signals for model 'LCETCS_vCalCtlGain' */
typedef struct {
  int16_T lcetcss16BsPgain;            /* '<Root>/LCETCS_vCalPCtlGain' */
  int16_T lcetcss16PgainFacInGearChng; /* '<Root>/LCETCS_vCalPCtlGain' */
  int16_T lcetcss16HomoBsPgain;        /* '<Root>/LCETCS_vCalPCtlGain' */
  int16_T lcetcss16SpltBsPgain;        /* '<Root>/LCETCS_vCalPCtlGain' */
  int16_T lcetcss16PGainFacInDrvVib;   /* '<Root>/LCETCS_vCalPCtlGain' */
  int16_T lcetcss16PgainFac;           /* '<Root>/LCETCS_vCalPCtlGain' */
  int16_T lcetcss16BsIgain;            /* '<Root>/LCETCS_vCalICtlGain' */
  int16_T lcetcss16IgainFacInGearChng; /* '<Root>/LCETCS_vCalICtlGain' */
  int16_T lcetcss16HomoBsIgain;        /* '<Root>/LCETCS_vCalICtlGain' */
  int16_T lcetcss16SpltBsIgain;        /* '<Root>/LCETCS_vCalICtlGain' */
  int16_T lcetcss16IgainFac;           /* '<Root>/LCETCS_vCalICtlGain' */
  boolean_T lcetcsu1DelYawDivrg;       /* '<Root>/LCETCS_vCalYawCtlGain' */
} B_LCETCS_vCalCtlGain_c_T;

/* Block states (auto storage) for model 'LCETCS_vCalCtlGain' */
typedef struct {
  MdlrefDW_LCETCS_vCalPCtlGain_T LCETCS_vCalPCtlGain_DWORK1;/* '<Root>/LCETCS_vCalPCtlGain' */
  MdlrefDW_LCETCS_vCalICtlGain_T LCETCS_vCalICtlGain_DWORK1;/* '<Root>/LCETCS_vCalICtlGain' */
  MdlrefDW_LCETCS_vCalYawCtlGain_T LCETCS_vCalYawCtlGain_DWORK1;/* '<Root>/LCETCS_vCalYawCtlGain' */
} DW_LCETCS_vCalCtlGain_f_T;

typedef struct {
  B_LCETCS_vCalCtlGain_c_T rtb;
  DW_LCETCS_vCalCtlGain_f_T rtdw;
} MdlrefDW_LCETCS_vCalCtlGain_T;

/* Model reference registration function */
extern void LCETCS_vCalCtlGain_initialize(void);
extern void LCETCS_vCalCtlGain_Init(B_LCETCS_vCalCtlGain_c_T *localB,
  DW_LCETCS_vCalCtlGain_f_T *localDW);
extern void LCETCS_vCalCtlGain_Start(DW_LCETCS_vCalCtlGain_f_T *localDW);
extern void LCETCS_vCalCtlGain(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT,
  const TypeETCSDrvMdlStruct *rtu_ETCS_DRV_MDL, const TypeETCSCtlErrStruct
  *rtu_ETCS_CTL_ERR, const TypeETCSGainGearShftStruct *rtu_ETCS_GAIN_GEAR_SHFT,
  const TypeETCSH2LStruct *rtu_ETCS_H2L, const TypeETCSAxlStruct *rtu_ETCS_FA,
  const TypeETCSAxlStruct *rtu_ETCS_RA, const TypeETCSTarSpinStruct
  *rtu_ETCS_TAR_SPIN, const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, boolean_T
  rtu_lcetcsu1VehVibDct, const TypeETCSCtlCmdStruct *rtu_ETCS_CTL_CMD_OLD,
  int16_T rtu_lcetcss16VehSpd, const TypeETCSAftLmtCrng *rtu_ETCS_AFT_TURN,
  const TypeETCSTrq4RdFricStruct *rtu_ETCS_TRQ_REP_RD_FRIC, TypeETCSGainStruct
  *rty_ETCS_CTL_GAINS, B_LCETCS_vCalCtlGain_c_T *localB,
  DW_LCETCS_vCalCtlGain_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalCtlGain'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalCtlGain_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
