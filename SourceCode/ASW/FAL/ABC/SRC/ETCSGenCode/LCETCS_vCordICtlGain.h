/*
 * File: LCETCS_vCordICtlGain.h
 *
 * Code generated for Simulink model 'LCETCS_vCordICtlGain'.
 *
 * Model version                  : 1.231
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:51:47 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCordICtlGain_h_
#define RTW_HEADER_LCETCS_vCordICtlGain_h_
#ifndef LCETCS_vCordICtlGain_COMMON_INCLUDES_
# define LCETCS_vCordICtlGain_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCordICtlGain_COMMON_INCLUDES_ */

#include "LCETCS_vCordICtlGain_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Model reference registration function */
extern void LCETCS_vCordICtlGain_initialize(void);
extern void LCETCS_vCordICtlGain_Init(int16_T *rty_lcetcss16IgainFac);
extern void LCETCS_vCordICtlGain(int16_T rtu_lcetcss16BsIgain, int16_T
  rtu_lcetcss16IGainIncFacH2L, const TypeETCSH2LStruct *rtu_ETCS_H2L, const
  TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN, int16_T rtu_lcetcss16IGainFacInTrn,
  const TypeETCSGainGearShftStruct *rtu_ETCS_GAIN_GEAR_SHFT, int16_T
  rtu_lcetcss16IgainFacInGearChng, const TypeETCSCtlErrStruct *rtu_ETCS_CTL_ERR,
  const TypeETCSAftLmtCrng *rtu_ETCS_AFT_TURN, int16_T
  rtu_lcetcss16HmIGainFctAftrTrn, int16_T rtu_lcetcss16HmIGainFct4BigSpn,
  int16_T *rty_lcetcss16Igain, int16_T *rty_lcetcss16IgainFac);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCordICtlGain'
 * '<S1>'   : 'LCETCS_vCordICtlGain/Chart1'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCordICtlGain_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
