/*
 * File: LCETCS_vCalJmpUpDnFtr.c
 *
 * Code generated for Simulink model 'LCETCS_vCalJmpUpDnFtr'.
 *
 * Model version                  : 1.210
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:04:41 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalJmpUpDnFtr.h"
#include "LCETCS_vCalJmpUpDnFtr_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalJmpUpDnFtr' */
void LCETCS_vCalJmpUpDnFtr_Init(B_LCETCS_vCalJmpUpDnFtr_c_T *localB)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_Init(&localB->lcetcss16Ftr4JmpDn);

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter2Point1' */
  LCETCS_s16Inter2Point_Init(&localB->lcetcss16Ftr4JmpUp);
}

/* Start for referenced model: 'LCETCS_vCalJmpUpDnFtr' */
void LCETCS_vCalJmpUpDnFtr_Start(B_LCETCS_vCalJmpUpDnFtr_c_T *localB)
{
  /* Start for Constant: '<Root>/Constant7' */
  localB->x1 = ((uint8_T)U8ETCSCpRefTrqVldTm);

  /* Start for Constant: '<Root>/Constant8' */
  localB->x2 = ((uint8_T)U8ETCSCpRefTrqNvldTm);

  /* Start for Constant: '<Root>/Constant1' */
  localB->y1 = ((uint8_T)U8ETCSCpFtr4JmpUpMax);

  /* Start for Constant: '<Root>/Constant6' */
  localB->y2 = ((uint8_T)U8ETCSCpFtr4JmpUpMin);
}

/* Output and update for referenced model: 'LCETCS_vCalJmpUpDnFtr' */
void LCETCS_vCalJmpUpDnFtr(int32_T rtu_lcetcss32RefTrq2Jmp, boolean_T
  rtu_lcetcsu1RefTrq4JmpSetOk, int16_T rtu_lcetcss16RefTrqSetTm,
  TypeETCSRefTrq2JmpStruct *rty_ETCS_REF_TRQ2JMP, B_LCETCS_vCalJmpUpDnFtr_c_T
  *localB)
{
  /* Constant: '<Root>/Constant4' */
  localB->y2 = ((uint8_T)U8ETCSCpRefTrqVldTm);

  /* Constant: '<Root>/Constant5' */
  localB->y1 = ((uint8_T)U8ETCSCpRefTrqNvldTm);

  /* Constant: '<Root>/Constant2' */
  localB->x2 = ((uint8_T)U8ETCSCpFtr4JmpDnMax);

  /* Constant: '<Root>/Constant3' */
  localB->x1 = ((uint8_T)U8ETCSCpFtr4JmpDnMin);

  /* ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point(rtu_lcetcss16RefTrqSetTm, localB->y2, localB->y1,
                        localB->x2, localB->x1, &localB->lcetcss16Ftr4JmpDn);

  /* Constant: '<Root>/Constant7' */
  localB->x1 = ((uint8_T)U8ETCSCpRefTrqVldTm);

  /* Constant: '<Root>/Constant8' */
  localB->x2 = ((uint8_T)U8ETCSCpRefTrqNvldTm);

  /* Constant: '<Root>/Constant1' */
  localB->y1 = ((uint8_T)U8ETCSCpFtr4JmpUpMax);

  /* Constant: '<Root>/Constant6' */
  localB->y2 = ((uint8_T)U8ETCSCpFtr4JmpUpMin);

  /* ModelReference: '<Root>/LCETCS_s16Inter2Point1' */
  LCETCS_s16Inter2Point(rtu_lcetcss16RefTrqSetTm, localB->x1, localB->x2,
                        localB->y1, localB->y2, &localB->lcetcss16Ftr4JmpUp);

  /* BusCreator: '<Root>/Bus Creator' */
  rty_ETCS_REF_TRQ2JMP->lcetcss32RefTrq2Jmp = rtu_lcetcss32RefTrq2Jmp;
  rty_ETCS_REF_TRQ2JMP->lcetcsu1RefTrq4JmpSetOk = rtu_lcetcsu1RefTrq4JmpSetOk;
  rty_ETCS_REF_TRQ2JMP->lcetcss16RefTrqSetTm = rtu_lcetcss16RefTrqSetTm;
  rty_ETCS_REF_TRQ2JMP->lcetcss16Ftr4JmpDn = localB->lcetcss16Ftr4JmpDn;
  rty_ETCS_REF_TRQ2JMP->lcetcss16Ftr4JmpUp = localB->lcetcss16Ftr4JmpUp;
}

/* Model initialize function */
void LCETCS_vCalJmpUpDnFtr_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter2Point1' */
  LCETCS_s16Inter2Point_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
