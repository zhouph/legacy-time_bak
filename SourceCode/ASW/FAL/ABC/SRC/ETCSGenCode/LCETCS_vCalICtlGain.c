/*
 * File: LCETCS_vCalICtlGain.c
 *
 * Code generated for Simulink model 'LCETCS_vCalICtlGain'.
 *
 * Model version                  : 1.218
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:08:11 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalICtlGain.h"
#include "LCETCS_vCalICtlGain_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalICtlGain' */
void LCETCS_vCalICtlGain_Init(int16_T *rty_lcetcss16BsIgain, int16_T
  *rty_lcetcss16IgainFacInGearChng, int16_T *rty_lcetcss16IgainFac,
  DW_LCETCS_vCalICtlGain_f_T *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalBsIGain' */
  LCETCS_vCalBsIGain_Init(rty_lcetcss16BsIgain);

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalHmIGainFct4BigSpn' */
  LCETCS_vCalHmIGainFct4BigSpn_Init
    (&(localDW->LCETCS_vCalHmIGainFct4BigSpn_DWORK1.rtb));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalHmIGainFctAftrTrn' */
  LCETCS_vCalHmIGainFctAftrTrn_Init
    (&(localDW->LCETCS_vCalHmIGainFctAftrTrn_DWORK1.rtb));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalIGainFacInGearChng' */
  LCETCS_vCalIGainFacInGearChng_Init(rty_lcetcss16IgainFacInGearChng);

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalIGainIncFacH2L' */
  LCETCS_vCalIGainIncFacH2L_Init(&(localDW->LCETCS_vCalIGainIncFacH2L_DWORK1.rtb));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCordICtlGain' */
  LCETCS_vCordICtlGain_Init(rty_lcetcss16IgainFac);
}

/* Start for referenced model: 'LCETCS_vCalICtlGain' */
void LCETCS_vCalICtlGain_Start(DW_LCETCS_vCalICtlGain_f_T *localDW)
{
  /* Start for ModelReference: '<Root>/LCETCS_vCalHomoBsIGain' */
  LCETCS_vCalHomoBsIGain_Start(&(localDW->LCETCS_vCalHomoBsIGain_DWORK1.rtb));

  /* Start for ModelReference: '<Root>/LCETCS_vCalSpltBsIGain' */
  LCETCS_vCalSpltBsIGain_Start(&(localDW->LCETCS_vCalSpltBsIGain_DWORK1.rtb));

  /* Start for ModelReference: '<Root>/LCETCS_vCalHmIGainFct4BigSpn' */
  LCETCS_vCalHmIGainFct4BigSpn_Start
    (&(localDW->LCETCS_vCalHmIGainFct4BigSpn_DWORK1.rtb));

  /* Start for ModelReference: '<Root>/LCETCS_vCalHmIGainFctAftrTrn' */
  LCETCS_vCalHmIGainFctAftrTrn_Start
    (&(localDW->LCETCS_vCalHmIGainFctAftrTrn_DWORK1.rtb));

  /* Start for ModelReference: '<Root>/LCETCS_vCalIGainFacInGearChng' */
  LCETCS_vCalIGainFacInGearChng_Start
    (&(localDW->LCETCS_vCalIGainFacInGearChng_DWORK1.rtb));

  /* Start for ModelReference: '<Root>/LCETCS_vCalIGainFtrInTrn' */
  LCETCS_vCalIGainFtrInTrn_Start(&(localDW->LCETCS_vCalIGainFtrInTrn_DWORK1.rtb));

  /* Start for ModelReference: '<Root>/LCETCS_vCalIGainIncFacH2L' */
  LCETCS_vCalIGainIncFacH2L_Start
    (&(localDW->LCETCS_vCalIGainIncFacH2L_DWORK1.rtb));
}

/* Output and update for referenced model: 'LCETCS_vCalICtlGain' */
void LCETCS_vCalICtlGain(const TypeETCSAftLmtCrng *rtu_ETCS_AFT_TURN, const
  TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const TypeETCSDrvMdlStruct
  *rtu_ETCS_DRV_MDL, const TypeETCSCtlErrStruct *rtu_ETCS_CTL_ERR, const
  TypeETCSTrq4RdFricStruct *rtu_ETCS_TRQ_REP_RD_FRIC, const
  TypeETCSGainGearShftStruct *rtu_ETCS_GAIN_GEAR_SHFT, const TypeETCSAxlStruct
  *rtu_ETCS_FA, const TypeETCSAxlStruct *rtu_ETCS_RA, const
  TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN, const TypeETCSEscSigStruct
  *rtu_ETCS_ESC_SIG, const TypeETCSH2LStruct *rtu_ETCS_H2L, int16_T
  *rty_lcetcss16Igain, int16_T *rty_lcetcss16BsIgain, int16_T
  *rty_lcetcss16IgainFacInGearChng, int16_T *rty_lcetcss16IGainIncFacH2L,
  int16_T *rty_lcetcss16HomoBsIgain, int16_T *rty_lcetcss16SpltBsIgain, int16_T *
  rty_lcetcss16IGainFacInTrn, int16_T *rty_lcetcss16IgainFac, int16_T
  *rty_lcetcss16HmIGainFctAftrTrn, int16_T *rty_lcetcss16HmIGainFct4BigSpn,
  DW_LCETCS_vCalICtlGain_f_T *localDW)
{
  /* ModelReference: '<Root>/LCETCS_vCalHomoBsIGain' */
  LCETCS_vCalHomoBsIGain(rtu_ETCS_CTL_ACT, rtu_ETCS_DRV_MDL, rtu_ETCS_CTL_ERR,
    rtu_ETCS_TRQ_REP_RD_FRIC, rty_lcetcss16HomoBsIgain,
    &(localDW->LCETCS_vCalHomoBsIGain_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vCalSpltBsIGain' */
  LCETCS_vCalSpltBsIGain(rtu_ETCS_CTL_ACT, rtu_ETCS_DRV_MDL, rtu_ETCS_CTL_ERR,
    rty_lcetcss16SpltBsIgain, &(localDW->LCETCS_vCalSpltBsIGain_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vCalBsIGain' */
  LCETCS_vCalBsIGain((*rty_lcetcss16HomoBsIgain), rtu_ETCS_FA, rtu_ETCS_RA,
                     (*rty_lcetcss16SpltBsIgain), rty_lcetcss16BsIgain);

  /* ModelReference: '<Root>/LCETCS_vCalHmIGainFct4BigSpn' */
  LCETCS_vCalHmIGainFct4BigSpn(rtu_ETCS_CTL_ERR, rty_lcetcss16HmIGainFct4BigSpn,
    &(localDW->LCETCS_vCalHmIGainFct4BigSpn_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vCalHmIGainFctAftrTrn' */
  LCETCS_vCalHmIGainFctAftrTrn(rtu_ETCS_AFT_TURN, rtu_ETCS_ESC_SIG,
    rtu_ETCS_DRV_MDL, rty_lcetcss16HmIGainFctAftrTrn,
    &(localDW->LCETCS_vCalHmIGainFctAftrTrn_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vCalIGainFacInGearChng' */
  LCETCS_vCalIGainFacInGearChng(rtu_ETCS_DRV_MDL, rtu_ETCS_GAIN_GEAR_SHFT,
    rty_lcetcss16IgainFacInGearChng,
    &(localDW->LCETCS_vCalIGainFacInGearChng_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vCalIGainFtrInTrn' */
  LCETCS_vCalIGainFtrInTrn(rtu_ETCS_CTL_ACT, rtu_ETCS_ESC_SIG, rtu_ETCS_DRV_MDL,
    rty_lcetcss16IGainFacInTrn, &(localDW->LCETCS_vCalIGainFtrInTrn_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vCalIGainIncFacH2L' */
  LCETCS_vCalIGainIncFacH2L(rtu_ETCS_H2L, rty_lcetcss16IGainIncFacH2L,
    &(localDW->LCETCS_vCalIGainIncFacH2L_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vCordICtlGain' */
  LCETCS_vCordICtlGain((*rty_lcetcss16BsIgain), (*rty_lcetcss16IGainIncFacH2L),
                       rtu_ETCS_H2L, rtu_ETCS_TAR_SPIN,
                       (*rty_lcetcss16IGainFacInTrn), rtu_ETCS_GAIN_GEAR_SHFT, (*
    rty_lcetcss16IgainFacInGearChng), rtu_ETCS_CTL_ERR, rtu_ETCS_AFT_TURN,
                       (*rty_lcetcss16HmIGainFctAftrTrn),
                       (*rty_lcetcss16HmIGainFct4BigSpn), rty_lcetcss16Igain,
                       rty_lcetcss16IgainFac);
}

/* Model initialize function */
void LCETCS_vCalICtlGain_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalBsIGain' */
  LCETCS_vCalBsIGain_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalHmIGainFct4BigSpn' */
  LCETCS_vCalHmIGainFct4BigSpn_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalHmIGainFctAftrTrn' */
  LCETCS_vCalHmIGainFctAftrTrn_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalHomoBsIGain' */
  LCETCS_vCalHomoBsIGain_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalIGainFacInGearChng' */
  LCETCS_vCalIGainFacInGearChng_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalIGainFtrInTrn' */
  LCETCS_vCalIGainFtrInTrn_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalIGainIncFacH2L' */
  LCETCS_vCalIGainIncFacH2L_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalSpltBsIGain' */
  LCETCS_vCalSpltBsIGain_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCordICtlGain' */
  LCETCS_vCordICtlGain_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
