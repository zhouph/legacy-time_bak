/*
 * File: LCETCS_vCalFiltDiffAx.c
 *
 * Code generated for Simulink model 'LCETCS_vCalFiltDiffAx'.
 *
 * Model version                  : 1.95
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:57:03 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalFiltDiffAx.h"
#include "LCETCS_vCalFiltDiffAx_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalFiltDiffAx' */
void LCETCS_vCalFiltDiffAx_Init(boolean_T *rty_lcetcsu1HillDctCompbyAx,
  DW_LCETCS_vCalFiltDiffAx_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = 0U;

  /* InitializeConditions for Chart: '<Root>/Chart' */
  *rty_lcetcsu1HillDctCompbyAx = false;
}

/* Output and update for referenced model: 'LCETCS_vCalFiltDiffAx' */
void LCETCS_vCalFiltDiffAx(int16_T rtu_Ls16Sig2FltTemp, int16_T
  rtu_Ls16Sig2Fltd5TempOld, int16_T rtu_Ls16SigFltdTempOld, const
  TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC, int16_T rtu_lcetcss16VehSpd, boolean_T
  *rty_lcetcsu1HillDctCompbyAx, int16_T *rty_lcetcss16LongAccFilter, uint8_T
  *rty_lcetcsu8HillDctCntbyAx, DW_LCETCS_vCalFiltDiffAx_f_T *localDW)
{
  int32_T tmp;
  int16_T u;
  int16_T u_0;

  /* Abs: '<Root>/Abs1' */
  if (rtu_Ls16Sig2FltTemp < 0) {
    *rty_lcetcss16LongAccFilter = (int16_T)(-rtu_Ls16Sig2FltTemp);
  } else {
    *rty_lcetcss16LongAccFilter = rtu_Ls16Sig2FltTemp;
  }

  /* End of Abs: '<Root>/Abs1' */

  /* Abs: '<Root>/Abs' incorporates:
   *  Sum: '<Root>/Add'
   */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:9' */
  /* comment */
  u = (int16_T)(rtu_Ls16Sig2FltTemp - rtu_Ls16Sig2Fltd5TempOld);

  /* Abs: '<Root>/Abs2' incorporates:
   *  Sum: '<Root>/Add1'
   */
  u_0 = (int16_T)(rtu_Ls16Sig2FltTemp - rtu_Ls16SigFltdTempOld);

  /* Abs: '<Root>/Abs' */
  if (u < 0) {
    u = (int16_T)(-u);
  }

  /* Abs: '<Root>/Abs2' */
  if (u_0 < 0) {
    u_0 = (int16_T)(-u_0);
  }

  /* Chart: '<Root>/Chart' incorporates:
   *  Abs: '<Root>/Abs'
   *  Abs: '<Root>/Abs2'
   *  UnitDelay: '<Root>/Unit Delay1'
   */
  if ((((*rty_lcetcss16LongAccFilter) >= ((uint8_T)U8ETCSCpHillDctAlongTh)) &&
       (u <= ((uint8_T)U8ETCSCpHillDiffAx))) && (u_0 <= ((uint8_T)
        U8ETCSCpHillDiffAx))) {
    /* Transition: '<S1>:10' */
    /* Transition: '<S1>:40' */
    tmp = localDW->UnitDelay1_DSTATE + 1;
    if (tmp > 255) {
      tmp = 255;
    }

    *rty_lcetcsu8HillDctCntbyAx = (uint8_T)tmp;
    if (((uint8_T)tmp) >= ((uint8_T)U8ETCSCpHillDctTimeAx)) {
      /* Transition: '<S1>:54' */
      /* Transition: '<S1>:56' */
      *rty_lcetcsu1HillDctCompbyAx = true;

      /* Transition: '<S1>:59' */
    } else {
      /* Transition: '<S1>:60' */
      *rty_lcetcsu1HillDctCompbyAx = false;
    }

    /* Transition: '<S1>:75' */
  } else {
    /* Transition: '<S1>:74' */
    tmp = localDW->UnitDelay1_DSTATE - 1;
    if (tmp < 0) {
      tmp = 0;
    }

    *rty_lcetcsu8HillDctCntbyAx = (uint8_T)tmp;
    *rty_lcetcsu1HillDctCompbyAx = false;
  }

  /* Transition: '<S1>:68' */
  if ((rtu_ETCS_VEH_ACC->lcetcss16VehAccByVehSpd > ((uint8_T)
        U8ETCSCpSplitHillClrAccel)) || (rtu_lcetcss16VehSpd > ((uint8_T)
        U8ETCSCpSplitHillClrVehSpd))) {
    /* Transition: '<S1>:78' */
    /* Transition: '<S1>:80' */
    *rty_lcetcsu8HillDctCntbyAx = 0U;
    *rty_lcetcsu1HillDctCompbyAx = false;

    /* Transition: '<S1>:82' */
  } else {
    /* Transition: '<S1>:83' */
  }

  /* End of Chart: '<Root>/Chart' */

  /* Saturate: '<Root>/Saturation' */
  /* Transition: '<S1>:85' */
  if ((*rty_lcetcsu8HillDctCntbyAx) <= ((uint8_T)U8ETCSCpHillDctTimeAx)) {
  } else {
    *rty_lcetcsu8HillDctCntbyAx = ((uint8_T)U8ETCSCpHillDctTimeAx);
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Update for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = *rty_lcetcsu8HillDctCntbyAx;
}

/* Model initialize function */
void LCETCS_vCalFiltDiffAx_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
