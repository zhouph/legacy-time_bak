/*
 * File: LCETCS_vDctCyl1st.h
 *
 * Code generated for Simulink model 'LCETCS_vDctCyl1st'.
 *
 * Model version                  : 1.176
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:05:26 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vDctCyl1st_h_
#define RTW_HEADER_LCETCS_vDctCyl1st_h_
#ifndef LCETCS_vDctCyl1st_COMMON_INCLUDES_
# define LCETCS_vDctCyl1st_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vDctCyl1st_COMMON_INCLUDES_ */

#include "LCETCS_vDctCyl1st_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vDctRisingEdge.h"
#include "LCETCS_vDctFallingEdge.h"

/* Block signals for model 'LCETCS_vDctCyl1st' */
typedef struct {
  boolean_T lcetcsu1FalEdgOfCyl1st;    /* '<Root>/LCETCS_vDctFallingEdge' */
  boolean_T lcetcsu1RsgEdgOfCyl1st;    /* '<Root>/LCETCS_vDctRisingEdge' */
  boolean_T lcetcsu1Cyl1st;            /* '<Root>/Chart' */
} B_LCETCS_vDctCyl1st_c_T;

/* Block states (auto storage) for model 'LCETCS_vDctCyl1st' */
typedef struct {
  TypeETCSCyl1stStruct UnitDelay1_DSTATE;/* '<Root>/Unit Delay1' */
  MdlrefDW_LCETCS_vDctFallingEdge_T LCETCS_vDctFallingEdge_DWORK1;/* '<Root>/LCETCS_vDctFallingEdge' */
  MdlrefDW_LCETCS_vDctRisingEdge_T LCETCS_vDctRisingEdge_DWORK1;/* '<Root>/LCETCS_vDctRisingEdge' */
} DW_LCETCS_vDctCyl1st_f_T;

typedef struct {
  B_LCETCS_vDctCyl1st_c_T rtb;
  DW_LCETCS_vDctCyl1st_f_T rtdw;
} MdlrefDW_LCETCS_vDctCyl1st_T;

/* Model reference registration function */
extern void LCETCS_vDctCyl1st_initialize(const rtTimingBridge *timingBridge);
extern const TypeETCSCyl1stStruct LCETCS_vDctCyl1st_rtZTypeETCSCyl1stStruct;/* TypeETCSCyl1stStruct ground */
extern void LCETCS_vDctCyl1st_Init(TypeETCSCyl1stStruct *rty_ETCS_CYL_1ST,
  DW_LCETCS_vDctCyl1st_f_T *localDW, B_LCETCS_vDctCyl1st_c_T *localB);
extern void LCETCS_vDctCyl1st(const TypeETCSLowWhlSpnStruct
  *rtu_ETCS_LOW_WHL_SPN, const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const
  TypeETCSCtlErrStruct *rtu_ETCS_CTL_ERR, TypeETCSCyl1stStruct *rty_ETCS_CYL_1ST,
  DW_LCETCS_vDctCyl1st_f_T *localDW, B_LCETCS_vDctCyl1st_c_T *localB);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vDctCyl1st'
 * '<S1>'   : 'LCETCS_vDctCyl1st/Chart'
 * '<S2>'   : 'LCETCS_vDctCyl1st/Chart1'
 */
#endif                                 /* RTW_HEADER_LCETCS_vDctCyl1st_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
