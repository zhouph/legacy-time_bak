/*
 * File: LCETCS_vDctHoldingGradient.c
 *
 * Code generated for Simulink model 'LCETCS_vDctHoldingGradient'.
 *
 * Model version                  : 1.334
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:57:32 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctHoldingGradient.h"
#include "LCETCS_vDctHoldingGradient_private.h"

/* Initial conditions for referenced model: 'LCETCS_vDctHoldingGradient' */
void LCETCS_vDctHoldingGradient_Init(boolean_T *rty_lcetcsu1DctHoldGradient,
  DW_LCETCS_vDctHoldingGradient_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay4' */
  localDW->UnitDelay4_DSTATE = false;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay3' */
  localDW->UnitDelay3_DSTATE = false;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = 0;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = 0;

  /* InitializeConditions for Chart: '<Root>/Chart' */
  *rty_lcetcsu1DctHoldGradient = false;
}

/* Output and update for referenced model: 'LCETCS_vDctHoldingGradient' */
void LCETCS_vDctHoldingGradient(boolean_T rtu_lcetcsu1InhibitGradient, int16_T
  rtu_lcetcss16LongAccFilter, const TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC,
  const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, int16_T rtu_lcetcss16VehSpd,
  int16_T *rty_lcetcss16HoldGradientCnt, boolean_T *rty_lcetcsu1DctHoldGradient,
  int16_T *rty_lcetcss16HoldRefGradientTime, DW_LCETCS_vDctHoldingGradient_f_T
  *localDW)
{
  int16_T lcetcss16HoldGradientCnt;
  int32_T tmp;

  /* Chart: '<Root>/Chart' incorporates:
   *  UnitDelay: '<Root>/Unit Delay1'
   *  UnitDelay: '<Root>/Unit Delay2'
   *  UnitDelay: '<Root>/Unit Delay3'
   *  UnitDelay: '<Root>/Unit Delay4'
   */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:9' */
  /* comment */
  if ((localDW->UnitDelay2_DSTATE >= localDW->UnitDelay1_DSTATE) ||
      (rtu_ETCS_ESC_SIG->lcetcsu1BrkPdlPrsed == 0)) {
    /* Transition: '<S1>:171' */
    /* Transition: '<S1>:229' */
    *rty_lcetcsu1DctHoldGradient = false;

    /* Transition: '<S1>:239' */
    /* Transition: '<S1>:238' */
  } else {
    /* Transition: '<S1>:194' */
    if (rtu_ETCS_VEH_ACC->lcetcss16VehAccByVehSpd < (-((uint8_T)VREF_DECEL_0_3_G)))
    {
      /* Transition: '<S1>:233' */
      /* Transition: '<S1>:235' */
      *rty_lcetcsu1DctHoldGradient = true;

      /* Transition: '<S1>:238' */
    } else {
      /* Transition: '<S1>:237' */
      *rty_lcetcsu1DctHoldGradient = localDW->UnitDelay3_DSTATE;
    }
  }

  /* Transition: '<S1>:240' */
  if ((rtu_ETCS_ESC_SIG->lcetcsu1BrkPdlPrsed == 1) &&
      (rtu_lcetcsu1InhibitGradient == 0)) {
    /* Transition: '<S1>:200' */
    /* Transition: '<S1>:202' */
    tmp = localDW->UnitDelay2_DSTATE + 1;
    if (tmp > 32767) {
      tmp = 32767;
    }

    lcetcss16HoldGradientCnt = (int16_T)tmp;
    if (localDW->UnitDelay4_DSTATE == 0) {
      /* Transition: '<S1>:206' */
      /* Transition: '<S1>:243' */
      if (rtu_lcetcss16LongAccFilter == 0) {
        /* Transition: '<S1>:245' */
        /* Transition: '<S1>:247' */
        tmp = rtu_lcetcss16VehSpd * 17;
        if (tmp > 32767) {
          tmp = 32767;
        }

        localDW->UnitDelay1_DSTATE = (int16_T)tmp;

        /* Transition: '<S1>:250' */
      } else {
        /* Transition: '<S1>:249' */
        tmp = (rtu_lcetcss16VehSpd * 17) / rtu_lcetcss16LongAccFilter;
        if (tmp > 32767) {
          tmp = 32767;
        } else {
          if (tmp < -32768) {
            tmp = -32768;
          }
        }

        localDW->UnitDelay1_DSTATE = (int16_T)tmp;
      }

      /* Transition: '<S1>:251' */
    } else {
      /* Transition: '<S1>:210' */
    }

    /* Transition: '<S1>:211' */
  } else {
    /* Transition: '<S1>:204' */
    lcetcss16HoldGradientCnt = 0;
    localDW->UnitDelay1_DSTATE = 0;
  }

  /* End of Chart: '<Root>/Chart' */

  /* Saturate: '<Root>/Saturation' */
  /* Transition: '<S1>:214' */
  if (lcetcss16HoldGradientCnt > 30000) {
    *rty_lcetcss16HoldGradientCnt = 30000;
  } else if (lcetcss16HoldGradientCnt < 0) {
    *rty_lcetcss16HoldGradientCnt = 0;
  } else {
    *rty_lcetcss16HoldGradientCnt = lcetcss16HoldGradientCnt;
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Saturate: '<Root>/Saturation1' */
  if (localDW->UnitDelay1_DSTATE > 30000) {
    *rty_lcetcss16HoldRefGradientTime = 30000;
  } else if (localDW->UnitDelay1_DSTATE < 0) {
    *rty_lcetcss16HoldRefGradientTime = 0;
  } else {
    *rty_lcetcss16HoldRefGradientTime = localDW->UnitDelay1_DSTATE;
  }

  /* End of Saturate: '<Root>/Saturation1' */

  /* Update for UnitDelay: '<Root>/Unit Delay4' */
  localDW->UnitDelay4_DSTATE = rtu_ETCS_ESC_SIG->lcetcsu1BrkPdlPrsed;

  /* Update for UnitDelay: '<Root>/Unit Delay3' */
  localDW->UnitDelay3_DSTATE = *rty_lcetcsu1DctHoldGradient;

  /* Update for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = *rty_lcetcss16HoldGradientCnt;

  /* Update for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = *rty_lcetcss16HoldRefGradientTime;
}

/* Model initialize function */
void LCETCS_vDctHoldingGradient_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
