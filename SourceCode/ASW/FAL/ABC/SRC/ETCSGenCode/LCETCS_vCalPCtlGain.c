/*
 * File: LCETCS_vCalPCtlGain.c
 *
 * Code generated for Simulink model 'LCETCS_vCalPCtlGain'.
 *
 * Model version                  : 1.179
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:08:27 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalPCtlGain.h"
#include "LCETCS_vCalPCtlGain_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalPCtlGain' */
void LCETCS_vCalPCtlGain_Init(int16_T *rty_lcetcss16BsPgain, int16_T
  *rty_lcetcss16PgainFacInGearChng, int16_T *rty_lcetcss16PGainFacInDrvVib,
  int16_T *rty_lcetcss16PgainFac, DW_LCETCS_vCalPCtlGain_f_T *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalBsPGain' */
  LCETCS_vCalBsPGain_Init(rty_lcetcss16BsPgain);

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalPGainFacInGearChng' */
  LCETCS_vCalPGainFacInGearChng_Init(rty_lcetcss16PgainFacInGearChng);

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalPGainFtrInDrvVib' */
  LCETCS_vCalPGainFtrInDrvVib_Init(rty_lcetcss16PGainFacInDrvVib,
    &(localDW->LCETCS_vCalPGainFtrInDrvVib_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalPGainIncFacH2L' */
  LCETCS_vCalPGainIncFacH2L_Init(&(localDW->LCETCS_vCalPGainIncFacH2L_DWORK1.rtb));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCordPCtlGain' */
  LCETCS_vCordPCtlGain_Init(rty_lcetcss16PgainFac);
}

/* Start for referenced model: 'LCETCS_vCalPCtlGain' */
void LCETCS_vCalPCtlGain_Start(DW_LCETCS_vCalPCtlGain_f_T *localDW)
{
  /* Start for ModelReference: '<Root>/LCETCS_vCalHomoBsPGain' */
  LCETCS_vCalHomoBsPGain_Start(&(localDW->LCETCS_vCalHomoBsPGain_DWORK1.rtb));

  /* Start for ModelReference: '<Root>/LCETCS_vCalSpltBsPGain' */
  LCETCS_vCalSpltBsPGain_Start(&(localDW->LCETCS_vCalSpltBsPGain_DWORK1.rtb));

  /* Start for ModelReference: '<Root>/LCETCS_vCalPGainFacInGearChng' */
  LCETCS_vCalPGainFacInGearChng_Start
    (&(localDW->LCETCS_vCalPGainFacInGearChng_DWORK1.rtb));

  /* Start for ModelReference: '<Root>/LCETCS_vCalPGainFtrInDrvVib' */
  LCETCS_vCalPGainFtrInDrvVib_Start
    (&(localDW->LCETCS_vCalPGainFtrInDrvVib_DWORK1.rtb));

  /* Start for ModelReference: '<Root>/LCETCS_vCalPGainFtrInTrn' */
  LCETCS_vCalPGainFtrInTrn_Start(&(localDW->LCETCS_vCalPGainFtrInTrn_DWORK1.rtb));

  /* Start for ModelReference: '<Root>/LCETCS_vCalPGainIncFacH2L' */
  LCETCS_vCalPGainIncFacH2L_Start
    (&(localDW->LCETCS_vCalPGainIncFacH2L_DWORK1.rtb));
}

/* Output and update for referenced model: 'LCETCS_vCalPCtlGain' */
void LCETCS_vCalPCtlGain(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const
  TypeETCSDrvMdlStruct *rtu_ETCS_DRV_MDL, const TypeETCSCtlErrStruct
  *rtu_ETCS_CTL_ERR, const TypeETCSTrq4RdFricStruct *rtu_ETCS_TRQ_REP_RD_FRIC,
  const TypeETCSGainGearShftStruct *rtu_ETCS_GAIN_GEAR_SHFT, const
  TypeETCSAxlStruct *rtu_ETCS_FA, const TypeETCSAxlStruct *rtu_ETCS_RA, const
  TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN, const TypeETCSEscSigStruct
  *rtu_ETCS_ESC_SIG, const TypeETCSH2LStruct *rtu_ETCS_H2L, boolean_T
  rtu_lcetcsu1VehVibDct, int16_T *rty_lcetcss16Pgain, int16_T
  *rty_lcetcss16BsPgain, int16_T *rty_lcetcss16PgainFacInGearChng, int16_T
  *rty_lcetcss16PGainIncFacH2L, int16_T *rty_lcetcss16HomoBsPgain, int16_T
  *rty_lcetcss16SpltBsPgain, int16_T *rty_lcetcss16PGainFacInTrn, int16_T
  *rty_lcetcss16PGainFacInDrvVib, int8_T *rty_lcetcss8PgainFacInDrvVibCnt,
  int16_T *rty_lcetcss16PgainFac, DW_LCETCS_vCalPCtlGain_f_T *localDW)
{
  /* ModelReference: '<Root>/LCETCS_vCalHomoBsPGain' */
  LCETCS_vCalHomoBsPGain(rtu_ETCS_CTL_ACT, rtu_ETCS_DRV_MDL, rtu_ETCS_CTL_ERR,
    rtu_ETCS_TRQ_REP_RD_FRIC, rty_lcetcss16HomoBsPgain,
    &(localDW->LCETCS_vCalHomoBsPGain_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vCalSpltBsPGain' */
  LCETCS_vCalSpltBsPGain(rtu_ETCS_CTL_ACT, rtu_ETCS_DRV_MDL, rtu_ETCS_CTL_ERR,
    rty_lcetcss16SpltBsPgain, &(localDW->LCETCS_vCalSpltBsPGain_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vCalBsPGain' */
  LCETCS_vCalBsPGain(rtu_ETCS_FA, rtu_ETCS_RA, (*rty_lcetcss16HomoBsPgain),
                     (*rty_lcetcss16SpltBsPgain), rty_lcetcss16BsPgain);

  /* ModelReference: '<Root>/LCETCS_vCalPGainFacInGearChng' */
  LCETCS_vCalPGainFacInGearChng(rtu_ETCS_DRV_MDL, rtu_ETCS_CTL_ERR,
    rtu_ETCS_GAIN_GEAR_SHFT, rty_lcetcss16PgainFacInGearChng,
    &(localDW->LCETCS_vCalPGainFacInGearChng_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vCalPGainFtrInDrvVib' */
  LCETCS_vCalPGainFtrInDrvVib(rtu_lcetcsu1VehVibDct,
    rty_lcetcss16PGainFacInDrvVib, rty_lcetcss8PgainFacInDrvVibCnt,
    &(localDW->LCETCS_vCalPGainFtrInDrvVib_DWORK1.rtb),
    &(localDW->LCETCS_vCalPGainFtrInDrvVib_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalPGainFtrInTrn' */
  LCETCS_vCalPGainFtrInTrn(rtu_ETCS_CTL_ACT, rtu_ETCS_ESC_SIG, rtu_ETCS_TAR_SPIN,
    rtu_ETCS_DRV_MDL, rtu_ETCS_CTL_ERR, rty_lcetcss16PGainFacInTrn,
    &(localDW->LCETCS_vCalPGainFtrInTrn_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vCalPGainIncFacH2L' */
  LCETCS_vCalPGainIncFacH2L(rtu_ETCS_H2L, rty_lcetcss16PGainIncFacH2L,
    &(localDW->LCETCS_vCalPGainIncFacH2L_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vCordPCtlGain' */
  LCETCS_vCordPCtlGain((*rty_lcetcss16BsPgain),
                       (*rty_lcetcss16PgainFacInGearChng),
                       (*rty_lcetcss16PGainIncFacH2L), rtu_ETCS_H2L,
                       (*rty_lcetcss16PGainFacInTrn), rtu_ETCS_ESC_SIG,
                       rtu_ETCS_TAR_SPIN, rtu_ETCS_GAIN_GEAR_SHFT,
                       (*rty_lcetcss8PgainFacInDrvVibCnt),
                       (*rty_lcetcss16PGainFacInDrvVib), rty_lcetcss16Pgain,
                       rty_lcetcss16PgainFac);
}

/* Model initialize function */
void LCETCS_vCalPCtlGain_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalBsPGain' */
  LCETCS_vCalBsPGain_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalHomoBsPGain' */
  LCETCS_vCalHomoBsPGain_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalPGainFacInGearChng' */
  LCETCS_vCalPGainFacInGearChng_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalPGainFtrInDrvVib' */
  LCETCS_vCalPGainFtrInDrvVib_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalPGainFtrInTrn' */
  LCETCS_vCalPGainFtrInTrn_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalPGainIncFacH2L' */
  LCETCS_vCalPGainIncFacH2L_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalSpltBsPGain' */
  LCETCS_vCalSpltBsPGain_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCordPCtlGain' */
  LCETCS_vCordPCtlGain_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
