/*
 * File: LCETCS_vCalGradientCnt.c
 *
 * Code generated for Simulink model 'LCETCS_vCalGradientCnt'.
 *
 * Model version                  : 1.300
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:57:18 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalGradientCnt.h"
#include "LCETCS_vCalGradientCnt_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalGradientCnt' */
void LCETCS_vCalGradientCnt_Init(DW_LCETCS_vCalGradientCnt_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = 0;
}

/* Output and update for referenced model: 'LCETCS_vCalGradientCnt' */
void LCETCS_vCalGradientCnt(int16_T rtu_lcetcss16LongAccFilter, const
  TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC, int16_T *rty_lcetcss16GradientTrstCnt,
  int16_T *rty_lcetcss16AbsLongAccDiff, DW_LCETCS_vCalGradientCnt_f_T *localDW)
{
  int32_T tmp;
  int16_T rtu_ETCS_VEH_ACC_0;

  /* Abs: '<Root>/Abs' */
  if (rtu_ETCS_VEH_ACC->lcetcss16VehAccByVehSpd < 0) {
    rtu_ETCS_VEH_ACC_0 = (int16_T)(-rtu_ETCS_VEH_ACC->lcetcss16VehAccByVehSpd);
  } else {
    rtu_ETCS_VEH_ACC_0 = rtu_ETCS_VEH_ACC->lcetcss16VehAccByVehSpd;
  }

  /* Sum: '<Root>/Add1' incorporates:
   *  Abs: '<Root>/Abs'
   */
  *rty_lcetcss16AbsLongAccDiff = (int16_T)(rtu_lcetcss16LongAccFilter -
    rtu_ETCS_VEH_ACC_0);

  /* Chart: '<Root>/Chart' incorporates:
   *  UnitDelay: '<Root>/Unit Delay1'
   */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:9' */
  /* comment */
  if ((*rty_lcetcss16AbsLongAccDiff) > ((uint8_T)GRAD_10_DEG)) {
    /* Transition: '<S1>:171' */
    /* Transition: '<S1>:169' */
    tmp = localDW->UnitDelay1_DSTATE + 1;
    if (tmp > 32767) {
      tmp = 32767;
    }

    localDW->UnitDelay1_DSTATE = (int16_T)tmp;

    /* Transition: '<S1>:187' */
    /* Transition: '<S1>:189' */
  } else {
    /* Transition: '<S1>:173' */
    if ((*rty_lcetcss16AbsLongAccDiff) < ((uint8_T)GRAD_8_DEG)) {
      /* Transition: '<S1>:184' */
      /* Transition: '<S1>:186' */
      tmp = localDW->UnitDelay1_DSTATE - 1;
      if (tmp < -32768) {
        tmp = -32768;
      }

      localDW->UnitDelay1_DSTATE = (int16_T)tmp;

      /* Transition: '<S1>:189' */
    } else {
      /* Transition: '<S1>:188' */
    }
  }

  /* End of Chart: '<Root>/Chart' */

  /* Saturate: '<Root>/Saturation' */
  /* Transition: '<S1>:191' */
  if (localDW->UnitDelay1_DSTATE > ((uint8_T)U8ETCSCpGradientTrstCplCnt)) {
    *rty_lcetcss16GradientTrstCnt = ((uint8_T)U8ETCSCpGradientTrstCplCnt);
  } else if (localDW->UnitDelay1_DSTATE < 0) {
    *rty_lcetcss16GradientTrstCnt = 0;
  } else {
    *rty_lcetcss16GradientTrstCnt = localDW->UnitDelay1_DSTATE;
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Update for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = *rty_lcetcss16GradientTrstCnt;
}

/* Model initialize function */
void LCETCS_vCalGradientCnt_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
