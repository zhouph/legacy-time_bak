/*
 * File: LCETCS_vCalDetTarWhlIncTire.h
 *
 * Code generated for Simulink model 'LCETCS_vCalDetTarWhlIncTire'.
 *
 * Model version                  : 1.128
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:50:04 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalDetTarWhlIncTire_h_
#define RTW_HEADER_LCETCS_vCalDetTarWhlIncTire_h_
#ifndef LCETCS_vCalDetTarWhlIncTire_COMMON_INCLUDES_
# define LCETCS_vCalDetTarWhlIncTire_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalDetTarWhlIncTire_COMMON_INCLUDES_ */

#include "LCETCS_vCalDetTarWhlIncTire_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Model reference registration function */
extern void LCETCS_vCalDetTarWhlIncTire_initialize(void);
extern void LCETCS_vCalDetTarWhlIncTire(boolean_T rtu_lcetcsu1AbnrmSizeTireDet,
  boolean_T rtu_lcetcsu1AbnrmSizeTireSus, int16_T rtu_lcetcss16VehSpd, const
  TypeETCSExtDctStruct *rtu_ETCS_EXT_DCT, int16_T
  *rty_lcetcss16TarWhlSpnIncAbnrmTire);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalDetTarWhlIncTire'
 * '<S1>'   : 'LCETCS_vCalDetTarWhlIncTire/Detect Abnrm Tire'
 * '<S2>'   : 'LCETCS_vCalDetTarWhlIncTire/Non Detect Abnrm Tire'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalDetTarWhlIncTire_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
