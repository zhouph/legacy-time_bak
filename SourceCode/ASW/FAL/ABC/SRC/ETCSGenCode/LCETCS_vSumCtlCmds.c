/*
 * File: LCETCS_vSumCtlCmds.c
 *
 * Code generated for Simulink model 'LCETCS_vSumCtlCmds'.
 *
 * Model version                  : 1.193
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:51:40 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vSumCtlCmds.h"
#include "LCETCS_vSumCtlCmds_private.h"

/* Output and update for referenced model: 'LCETCS_vSumCtlCmds' */
void LCETCS_vSumCtlCmds(int32_T rtu_lcetcss32PCtlPor, int32_T
  rtu_lcetcss32ICtlPor, int32_T rtu_lcetcss32CtlCmdMax, int32_T
  *rty_lcetcss32CardanTrqCmd)
{
  /* Sum: '<Root>/Add' */
  *rty_lcetcss32CardanTrqCmd = rtu_lcetcss32PCtlPor + rtu_lcetcss32ICtlPor;

  /* MinMax: '<Root>/MinMax3' */
  if (rtu_lcetcss32CtlCmdMax <= (*rty_lcetcss32CardanTrqCmd)) {
    *rty_lcetcss32CardanTrqCmd = rtu_lcetcss32CtlCmdMax;
  }

  /* End of MinMax: '<Root>/MinMax3' */

  /* Saturate: '<Root>/Saturation' */
  if ((*rty_lcetcss32CardanTrqCmd) < ((int16_T)S16ETCSCpMinCrdnCmdTrq)) {
    *rty_lcetcss32CardanTrqCmd = ((int16_T)S16ETCSCpMinCrdnCmdTrq);
  }

  /* End of Saturate: '<Root>/Saturation' */
}

/* Model initialize function */
void LCETCS_vSumCtlCmds_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
