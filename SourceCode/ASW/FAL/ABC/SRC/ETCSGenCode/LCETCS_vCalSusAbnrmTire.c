/*
 * File: LCETCS_vCalSusAbnrmTire.c
 *
 * Code generated for Simulink model 'LCETCS_vCalSusAbnrmTire'.
 *
 * Model version                  : 1.127
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:49:59 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalSusAbnrmTire.h"
#include "LCETCS_vCalSusAbnrmTire_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalSusAbnrmTire' */
void LCETCS_vCalSusAbnrmTire_Init(boolean_T *rty_lcetcsu1AbnrmSizeTireSus)
{
  /* InitializeConditions for Chart: '<Root>/Chart1' */
  *rty_lcetcsu1AbnrmSizeTireSus = false;
}

/* Output and update for referenced model: 'LCETCS_vCalSusAbnrmTire' */
void LCETCS_vCalSusAbnrmTire(const TypeETCSWhlStruct *rtu_ETCS_FL, const
  TypeETCSWhlStruct *rtu_ETCS_FR, const TypeETCSWhlStruct *rtu_ETCS_RL, const
  TypeETCSWhlStruct *rtu_ETCS_RR, boolean_T *rty_lcetcsu1AbnrmSizeTireSus)
{
  /* Chart: '<Root>/Chart1' */
  /* Gateway: Chart1 */
  /* During: Chart1 */
  /* Entry Internal: Chart1 */
  /* Transition: '<S1>:176' */
  if ((((rtu_ETCS_FL->lcetcsu1AbnrmTireBrkSuspect == 1) ||
        (rtu_ETCS_FR->lcetcsu1AbnrmTireBrkSuspect == 1)) ||
       (rtu_ETCS_RL->lcetcsu1AbnrmTireBrkSuspect == 1)) ||
      (rtu_ETCS_RR->lcetcsu1AbnrmTireBrkSuspect == 1)) {
    /* Transition: '<S1>:179' */
    /* Transition: '<S1>:181' */
    *rty_lcetcsu1AbnrmSizeTireSus = true;

    /* Transition: '<S1>:183' */
  } else {
    /* Transition: '<S1>:184' */
    *rty_lcetcsu1AbnrmSizeTireSus = false;
  }

  /* End of Chart: '<Root>/Chart1' */
  /* Transition: '<S1>:198' */
}

/* Model initialize function */
void LCETCS_vCalSusAbnrmTire_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
