/*
 * File: LCETCS_vCalDelYawCnvrgGain.h
 *
 * Code generated for Simulink model 'LCETCS_vCalDelYawCnvrgGain'.
 *
 * Model version                  : 1.258
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:03:00 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalDelYawCnvrgGain_h_
#define RTW_HEADER_LCETCS_vCalDelYawCnvrgGain_h_
#ifndef LCETCS_vCalDelYawCnvrgGain_COMMON_INCLUDES_
# define LCETCS_vCalDelYawCnvrgGain_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalDelYawCnvrgGain_COMMON_INCLUDES_ */

#include "LCETCS_vCalDelYawCnvrgGain_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_s16Inter3Point.h"

/* Block signals for model 'LCETCS_vCalDelYawCnvrgGain' */
typedef struct {
  int16_T x3;                          /* '<Root>/Ay level on asphalt' */
  int16_T x3_c;                        /* '<Root>/Ay level on asphalt1' */
  int16_T x1;                          /* '<Root>/Ay level on ice' */
  int16_T x1_k;                        /* '<Root>/Ay level on ice1' */
  int16_T x2;                          /* '<Root>/Ay level on snow' */
  int16_T x2_a;                        /* '<Root>/Ay level on snow1' */
  int16_T y3;                          /* '<Root>/Delta yaw gain below target torque on asphalt' */
  int16_T y1;                          /* '<Root>/Delta yaw gain below target torque on ice' */
  int16_T y2;                          /* '<Root>/Delta yaw gain below target torque on snow' */
  int16_T y;                           /* '<Root>/LCETCS_s16Inter3Point' */
  int16_T y_p;                         /* '<Root>/LCETCS_s16Inter3Point1' */
} B_LCETCS_vCalDelYawCnvrgGain_c_T;

typedef struct {
  B_LCETCS_vCalDelYawCnvrgGain_c_T rtb;
} MdlrefDW_LCETCS_vCalDelYawCnvrgGain_T;

/* Model reference registration function */
extern void LCETCS_vCalDelYawCnvrgGain_initialize(void);
extern void LCETCS_vCalDelYawCnvrgGain_Init(B_LCETCS_vCalDelYawCnvrgGain_c_T
  *localB);
extern void LCETCS_vCalDelYawCnvrgGain_Start(B_LCETCS_vCalDelYawCnvrgGain_c_T
  *localB);
extern void LCETCS_vCalDelYawCnvrgGain(const TypeETCSCtlCmdStruct
  *rtu_ETCS_CTL_CMD_OLD, const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, int16_T
  rtu_lcetcss16BasDelYawCnvrgGain, int16_T *rty_lcetcss16DelYawCnvrgGain,
  B_LCETCS_vCalDelYawCnvrgGain_c_T *localB);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalDelYawCnvrgGain'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalDelYawCnvrgGain_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
