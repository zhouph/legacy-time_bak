/*
 * File: LCETCS_s16ChngRateLmt.c
 *
 * Code generated for Simulink model 'LCETCS_s16ChngRateLmt'.
 *
 * Model version                  : 1.313
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:58:56 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_s16ChngRateLmt.h"
#include "LCETCS_s16ChngRateLmt_private.h"

/* Initial conditions for referenced model: 'LCETCS_s16ChngRateLmt' */
void LCETCS_s16ChngRateLmt_Init(int16_T *rty_lcetcss16ChngRateLmtdSig, int16_T
  *rty_lcetcss16TranTmeCnt, B_LCETCS_s16ChngRateLmt_c_T *localB)
{
  /* InitializeConditions for Chart: '<Root>/Chart2' */
  *rty_lcetcss16TranTmeCnt = 0;
  localB->lcetcss16TranStrtVal = 0;

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_Init(rty_lcetcss16ChngRateLmtdSig);
}

/* Output and update for referenced model: 'LCETCS_s16ChngRateLmt' */
void LCETCS_s16ChngRateLmt(int16_T rtu_lcetcss16TrgtSigVal, int16_T
  rtu_lcetcss16TrgtSigValOld, int16_T rtu_lcetcss16TranTme, int16_T
  rtu_lcetcss16ChngRateLmtdSigOld, int16_T rtu_lcetcss16TranTmeCntOld, int16_T
  *rty_lcetcss16ChngRateLmtdSig, int16_T *rty_lcetcss16TranTmeCnt,
  B_LCETCS_s16ChngRateLmt_c_T *localB)
{
  /* local block i/o variables */
  int16_T rtb_Saturation1;
  int16_T rtb_Saturation;

  /* Saturate: '<Root>/Saturation1' */
  rtb_Saturation1 = rtu_lcetcss16TrgtSigVal;

  /* Saturate: '<Root>/Saturation' */
  rtb_Saturation = rtu_lcetcss16TranTme;

  /* Chart: '<Root>/Chart2' incorporates:
   *  RelationalOperator: '<Root>/Relational Operator'
   *  Saturate: '<Root>/Saturation2'
   *  Saturate: '<Root>/Saturation3'
   */
  /* Gateway: Chart2 */
  /* During: Chart2 */
  /* Entry Internal: Chart2 */
  /* Transition: '<S1>:171' */
  /*  Set input signals for interpolation  */
  if ((rtb_Saturation1 != rtu_lcetcss16TrgtSigValOld) == 1) {
    /* Transition: '<S1>:173' */
    /* Transition: '<S1>:197' */
    *rty_lcetcss16TranTmeCnt = 1;
    localB->lcetcss16TranStrtVal = rtu_lcetcss16ChngRateLmtdSigOld;

    /* Transition: '<S1>:214' */
    /* Transition: '<S1>:209' */
  } else {
    /* Transition: '<S1>:172' */
    if (rtb_Saturation1 != rtu_lcetcss16ChngRateLmtdSigOld) {
      /* Transition: '<S1>:175' */
      /* Transition: '<S1>:208' */
      *rty_lcetcss16TranTmeCnt = (int16_T)(rtu_lcetcss16TranTmeCntOld + 1);

      /* Transition: '<S1>:209' */
    } else {
      /* Transition: '<S1>:179' */
      *rty_lcetcss16TranTmeCnt = rtb_Saturation;
    }
  }

  /* End of Chart: '<Root>/Chart2' */

  /* ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point((*rty_lcetcss16TranTmeCnt), rtCP_Constant2_Value,
                        rtb_Saturation, localB->lcetcss16TranStrtVal,
                        rtb_Saturation1, rty_lcetcss16ChngRateLmtdSig);
}

/* Model initialize function */
void LCETCS_s16ChngRateLmt_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
