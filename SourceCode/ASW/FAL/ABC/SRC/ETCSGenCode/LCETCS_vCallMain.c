/*
 * File: LCETCS_vCallMain.c
 *
 * Code generated for Simulink model 'LCETCS_vCallMain'.
 *
 * Model version                  : 1.472
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:22:44 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCallMain.h"
#include "LCETCS_vCallMain_private.h"

rtTimingBridge LCETCS_vCallMain_TimingBrdg;

/* Exported block signals */
TypeETCSGainStruct ETCS_CTL_GAINS;     /* '<Root>/Calculation1' */
TypeETCSTarSpinStruct ETCS_TAR_SPIN;   /* '<Root>/Calculation1' */
TypeETCSIValRstStruct ETCS_IVAL_RST;   /* '<Root>/Calculation1' */
TypeETCSEscSigStruct ETCS_ESC_SIG;     /* '<Root>/Interface' */
TypeETCSCtlCmdStruct ETCS_CTL_CMD;     /* '<Root>/Calculation1' */
TypeETCSSplitHillStruct ETCS_SPLIT_HILL;/* '<Root>/Calculation1' */
TypeETCSVehAccStruct ETCS_VEH_ACC;     /* '<Root>/Calculation1' */
TypeETCSLdTrqStruct ETCS_LD_TRQ;       /* '<Root>/Calculation1' */
TypeETCSWhlStruct ETCS_FL;             /* '<Root>/Interface' */
TypeETCSWhlStruct ETCS_FR;             /* '<Root>/Interface' */
TypeETCSWhlStruct ETCS_RL;             /* '<Root>/Interface' */
TypeETCSWhlStruct ETCS_RR;             /* '<Root>/Interface' */
TypeETCSExtDctStruct ETCS_EXT_DCT;     /* '<Root>/Interface' */
TypeETCSDrvMdlStruct ETCS_DRV_MDL;     /* '<Root>/Calculation1' */
TypeETCSCrdnTrqStruct ETCS_CRDN_TRQ;   /* '<Root>/Calculation1' */
TypeETCSWhlSpinStruct ETCS_WHL_SPIN;   /* '<Root>/Calculation1' */
TypeETCSCtlActStruct ETCS_CTL_ACT;     /* '<Root>/Calculation1' */
TypeETCSRefTrqStr2CylStruct ETCS_REF_TRQ_STR2CYL;/* '<Root>/Calculation1' */
TypeETCSRefTrq2JmpStruct ETCS_REF_TRQ2JMP;/* '<Root>/Calculation1' */
TypeETCSTrq4RdFricStruct ETCS_TRQ_REP_RD_FRIC;/* '<Root>/Calculation1' */
TypeETCSLwToSpltStruct ETCS_L2SPLT;    /* '<Root>/Calculation1' */
TypeETCSGearStruct ETCS_GEAR_STRUCT;   /* '<Root>/Interface' */
TypeETCSEngStruct ETCS_ENG_STRUCT;     /* '<Root>/Interface' */
TypeETCSAxlStruct ETCS_FA;             /* '<Root>/Interface' */
TypeETCSAxlStruct ETCS_RA;             /* '<Root>/Interface' */
TypeETCS4WDSigStruct ETCS_4WD_SIG;     /* '<Root>/Interface' */
TypeETCSCirStruct ETCS_PC;             /* '<Root>/Interface' */
TypeETCSCirStruct ETCS_SC;             /* '<Root>/Interface' */
TypeETCSBrkSigStruct ETCS_BRK_SIG;     /* '<Root>/Interface' */
TypeETCSCtlErrStruct ETCS_CTL_ERR;     /* '<Root>/Calculation1' */
TypeETCSEngMdlStruct ETCS_ENG_MDL;     /* '<Root>/Calculation1' */
TypeETCSAxlAccStruct ETCS_AXL_ACC;     /* '<Root>/Calculation1' */
TypeETCSStrStatStruct ETCS_STR_STAT;   /* '<Root>/Calculation1' */
TypeETCSCtrlModeStruct ETCS_CTRL_MODE; /* '<Root>/Calculation1' */
TypeETCSCyl1stStruct ETCS_CYL_1ST;     /* '<Root>/Calculation1' */
TypeETCSErrZroCrsStruct ETCS_ERR_ZRO_CRS;/* '<Root>/Calculation1' */
TypeETCSRefTrqStrCtlStruct ETCS_REF_TRQ_STR_CTL;/* '<Root>/Calculation1' */
TypeETCSLowWhlSpnStruct ETCS_LOW_WHL_SPN;/* '<Root>/Calculation1' */
TypeETCSDepSnwStruct ETCS_DEP_SNW;     /* '<Root>/Calculation1' */
TypeETCSH2LStruct ETCS_H2L;            /* '<Root>/Calculation1' */
TypeETCSGainGearShftStruct ETCS_GAIN_GEAR_SHFT;/* '<Root>/Calculation1' */
TypeETCSBrkCtlReqStruct ETCS_BRK_CTL_REQ;/* '<Root>/Calculation1' */
TypeETCSEngStallStruct ETCS_ENG_STALL; /* '<Root>/Calculation1' */
TypeTMCReqGearStruct TMC_REQ_GEAR_STRUCT;/* '<Root>/Calculation1' */
TypeETCSBrkCtlCmdStruct ETCS_BRK_CTL_CMD;/* '<Root>/Calculation1' */
TypeETCSWhlAccStruct ETCS_WHL_ACC;     /* '<Root>/Calculation1' */
TypeETCSHighDctStruct ETCS_HI_DCT;     /* '<Root>/Calculation1' */
TypeETCSReqVCAStruct ETCS_REQ_VCA;     /* '<Root>/Calculation1' */
TypeETCSEngCmdStruct ETCS_ENG_CMD;     /* '<Root>/Calculation1' */
TypeETCSAftLmtCrng ETCS_AFT_TURN;      /* '<Root>/Calculation1' */
int16_T lcetcss16VehSpd;               /* '<Root>/Interface' */
int16_T lcetcss16SymBrkCtlStrtTh;      /* '<Root>/Calculation1' */
boolean_T lcetcsu1VehVibDct;           /* '<Root>/Calculation1' */
boolean_T lcetcsu1FuncLampOn;          /* '<Root>/Calculation1' */

/* Block states (auto storage) */
DW_LCETCS_vCallMain_T LCETCS_vCallMain_DW;

/* Real-time model */
RT_MODEL_LCETCS_vCallMain_T LCETCS_vCallMain_M_;
RT_MODEL_LCETCS_vCallMain_T *const LCETCS_vCallMain_M = &LCETCS_vCallMain_M_;

/* Model step function */
void LCETCS_vCallMain_step(void)
{
  /* ModelReference: '<Root>/Interface' */
  LCETCS_vInterfaceInpSig(&FL, &FR, &RL, &RR, &FL_TCS, &FR_TCS, &RL_TCS, &RR_TCS,
    &FA_TCS, &RA_TCS, lctcss16VehSpd4TCS, gear_pos, lctcsu1GearShiftFlag, gs_sel,
    drive_torq, eng_torq, eng_rpm, mtp, &lcTcsCtrlFlg0, &VDCF7, &lcTcsCtrlFlg1,
    &VF15, lctcsu1DctBTCHillByAx, wstr, wstr_dot, yaw_out, alat, ax_Filter,
    delta_yaw_first, rf, nor_alat, det_alatc, det_alatm, &VDCF0, det_wstr_dot,
    ltcss16BaseTarWhlSpinDiff, lespu1AWD_LOW_ACT, lespu16AWD_4WD_TQC_CUR,
    &PC_TCS, &SC_TCS, &VDCF1, &RTA_FL, &RTA_FR, &RTA_RL, &RTA_RR, rta_ratio_temp,
    &HSAF0, lcu8SccMode, &Abc_CtrlBus, lctcsu1BTCSVehAtv, &ETCS_FL,
    &ETCS_FR, &ETCS_RL, &ETCS_RR, &lcetcss16VehSpd, &ETCS_GEAR_STRUCT,
    &ETCS_ENG_STRUCT, &ETCS_EXT_DCT, &ETCS_FA, &ETCS_RA, &ETCS_ESC_SIG,
    &ETCS_4WD_SIG, &ETCS_PC, &ETCS_SC, &ETCS_BRK_SIG);

  /* ModelReference: '<Root>/Calculation1' */
  FctEtcsCore(&ETCS_FL, &ETCS_FR, &ETCS_RL, &ETCS_RR, lcetcss16VehSpd,
              &ETCS_GEAR_STRUCT, &ETCS_ENG_STRUCT, &ETCS_EXT_DCT, &ETCS_FA,
              &ETCS_RA, &ETCS_ESC_SIG, &ETCS_4WD_SIG, &ETCS_PC, &ETCS_SC,
              &ETCS_BRK_SIG, &ETCS_CTL_ERR, &ETCS_TAR_SPIN, &ETCS_DRV_MDL,
              &ETCS_CRDN_TRQ, &ETCS_VEH_ACC, &ETCS_ENG_MDL, &ETCS_LD_TRQ,
              &ETCS_AXL_ACC, &lcetcss16SymBrkCtlStrtTh, &ETCS_WHL_SPIN,
              &ETCS_STR_STAT, &ETCS_CTRL_MODE, &ETCS_CTL_ACT, &ETCS_CYL_1ST,
              &ETCS_ERR_ZRO_CRS, &ETCS_IVAL_RST, &ETCS_REF_TRQ_STR_CTL,
              &ETCS_REF_TRQ_STR2CYL, &ETCS_REF_TRQ2JMP, &ETCS_LOW_WHL_SPN,
              &ETCS_DEP_SNW, &ETCS_H2L, &ETCS_GAIN_GEAR_SHFT, &ETCS_BRK_CTL_REQ,
              &lcetcsu1VehVibDct, &ETCS_ENG_STALL, &ETCS_CTL_GAINS,
              &ETCS_TRQ_REP_RD_FRIC, &TMC_REQ_GEAR_STRUCT, &ETCS_BRK_CTL_CMD,
              &ETCS_CTL_CMD, &ETCS_SPLIT_HILL, &lcetcsu1FuncLampOn,
              &ETCS_WHL_ACC, &ETCS_HI_DCT, &ETCS_REQ_VCA, &ETCS_L2SPLT,
              &ETCS_ENG_CMD, &ETCS_AFT_TURN,
              &(LCETCS_vCallMain_DW.Calculation1_DWORK1.rtdw));
}

/* Model initialize function */
void LCETCS_vCallMain_initialize(void)
{
  /* Registration code */
  rtmSetFirstInitCond(LCETCS_vCallMain_M, 1);

  {
    LCETCS_vCallMain_TimingBrdg.nTasks = 1;
    LCETCS_vCallMain_TimingBrdg.clockTick = (NULL);
    LCETCS_vCallMain_TimingBrdg.clockTickH = (NULL);
    LCETCS_vCallMain_TimingBrdg.firstInitCond = &rtmIsFirstInitCond
      (LCETCS_vCallMain_M);
  }

  /* Model Initialize fcn for ModelReference Block: '<Root>/Calculation1' */
  FctEtcsCore_initialize(&LCETCS_vCallMain_TimingBrdg);

  /* Model Initialize fcn for ModelReference Block: '<Root>/Interface' */
  LCETCS_vInterfaceInpSig_initialize();

  /* Start for ModelReference: '<Root>/Calculation1' */
  FctEtcsCore_Start(&(LCETCS_vCallMain_DW.Calculation1_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/Calculation1' */
  FctEtcsCore_Init(&ETCS_CTRL_MODE, &ETCS_CYL_1ST, &ETCS_ERR_ZRO_CRS,
                   &ETCS_LOW_WHL_SPN, &ETCS_H2L, &ETCS_GAIN_GEAR_SHFT,
                   &TMC_REQ_GEAR_STRUCT, &lcetcsu1FuncLampOn,
                   &(LCETCS_vCallMain_DW.Calculation1_DWORK1.rtdw));

  /* set "at time zero" to false */
  if (rtmIsFirstInitCond(LCETCS_vCallMain_M)) {
    rtmSetFirstInitCond(LCETCS_vCallMain_M, 0);
  }
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
