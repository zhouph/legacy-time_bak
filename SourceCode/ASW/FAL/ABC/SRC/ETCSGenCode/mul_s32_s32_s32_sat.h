/*
 * File: mul_s32_s32_s32_sat.h
 *
 * Code generated for Simulink model 'LCETCS_vCalIValAtErrZroCrs'.
 *
 * Model version                  : 1.185
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:54:01 2015
 */

#ifndef SHARE_mul_s32_s32_s32_sat
#define SHARE_mul_s32_s32_s32_sat
#include "rtwtypes.h"

extern int32_T mul_s32_s32_s32_sat(int32_T a, int32_T b);

#endif

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
