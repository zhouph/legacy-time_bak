/*
 * File: LCETCS_vCalDiffAx.c
 *
 * Code generated for Simulink model 'LCETCS_vCalDiffAx'.
 *
 * Model version                  : 1.350
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:06:40 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalDiffAx.h"
#include "LCETCS_vCalDiffAx_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalDiffAx' */
void LCETCS_vCalDiffAx_Init(boolean_T *rty_lcetcsu1HillDctCompbyAx,
  DW_LCETCS_vCalDiffAx_f_T *localDW)
{
  int32_T i;

  /* InitializeConditions for Delay: '<Root>/Delay1' */
  for (i = 0; i < 5; i++) {
    localDW->Delay1_DSTATE[i] = 0;
  }

  localDW->CircBufIdx = 0U;

  /* End of InitializeConditions for Delay: '<Root>/Delay1' */

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = 0;

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalFiltDiffAx' */
  LCETCS_vCalFiltDiffAx_Init(rty_lcetcsu1HillDctCompbyAx,
    &(localDW->LCETCS_vCalFiltDiffAx_DWORK1.rtdw));
}

/* Output and update for referenced model: 'LCETCS_vCalDiffAx' */
void LCETCS_vCalDiffAx(const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const
  TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC, int16_T rtu_lcetcss16VehSpd, boolean_T
  *rty_lcetcsu1HillDctCompbyAx, uint8_T *rty_lcetcsu8HillDctCntbyAx, int16_T
  *rty_lcetcss16LongAccFilter, DW_LCETCS_vCalDiffAx_f_T *localDW)
{
  /* local block i/o variables */
  int16_T rtb_Ls16SigFltd;
  int16_T rtb_Ls16InpGain4Flt;
  int16_T rtb_Ls16Sig2Fltd5TempOld;
  int16_T rtb_Ls16SigFltdOld;

  /* DataTypeConversion: '<Root>/Data Type Conversion1' incorporates:
   *  Constant: '<Root>/LowPassFilter 7Hz'
   */
  rtb_Ls16InpGain4Flt = ((uint8_T)L_U8FILTER_GAIN_20MSLOOP_7HZ);

  /* Delay: '<Root>/Delay1' */
  rtb_Ls16Sig2Fltd5TempOld = localDW->Delay1_DSTATE[localDW->CircBufIdx];

  /* UnitDelay: '<Root>/Unit Delay' */
  rtb_Ls16SigFltdOld = localDW->UnitDelay_DSTATE;

  /* ModelReference: '<Root>/LCETCS_vCal1stOrdFlter' */
  LCETCS_vCal1stOrdFlter(rtu_ETCS_ESC_SIG->lcetcss16LonAcc, rtb_Ls16InpGain4Flt,
    rtb_Ls16SigFltdOld, &rtb_Ls16SigFltd);

  /* ModelReference: '<Root>/LCETCS_vCalFiltDiffAx' */
  LCETCS_vCalFiltDiffAx(rtb_Ls16SigFltd, rtb_Ls16Sig2Fltd5TempOld,
                        rtb_Ls16SigFltdOld, rtu_ETCS_VEH_ACC,
                        rtu_lcetcss16VehSpd, rty_lcetcsu1HillDctCompbyAx,
                        rty_lcetcss16LongAccFilter, rty_lcetcsu8HillDctCntbyAx,
                        &(localDW->LCETCS_vCalFiltDiffAx_DWORK1.rtdw));

  /* Update for Delay: '<Root>/Delay1' */
  localDW->Delay1_DSTATE[localDW->CircBufIdx] = rtb_Ls16SigFltd;
  if (localDW->CircBufIdx < 4U) {
    localDW->CircBufIdx++;
  } else {
    localDW->CircBufIdx = 0U;
  }

  /* End of Update for Delay: '<Root>/Delay1' */

  /* Update for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = rtb_Ls16SigFltd;
}

/* Model initialize function */
void LCETCS_vCalDiffAx_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCal1stOrdFlter' */
  LCETCS_vCal1stOrdFlter_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalFiltDiffAx' */
  LCETCS_vCalFiltDiffAx_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
