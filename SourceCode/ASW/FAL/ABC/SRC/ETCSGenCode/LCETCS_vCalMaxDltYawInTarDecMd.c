/*
 * File: LCETCS_vCalMaxDltYawInTarDecMd.c
 *
 * Code generated for Simulink model 'LCETCS_vCalMaxDltYawInTarDecMd'.
 *
 * Model version                  : 1.176
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:48:53 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalMaxDltYawInTarDecMd.h"
#include "LCETCS_vCalMaxDltYawInTarDecMd_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalMaxDltYawInTarDecMd' */
void LCETCS_vCalMaxDltYawInTarDecMd_Init(DW_LCETCS_vCalMaxDltYawInTarDecMd_f_T
  *localDW)
{
  /* InitializeConditions for Switch: '<Root>/Switch' incorporates:
   *  InitializeConditions for UnitDelay: '<Root>/Unit Delay1'
   */
  localDW->UnitDelay1_DSTATE = 0;
}

/* Output and update for referenced model: 'LCETCS_vCalMaxDltYawInTarDecMd' */
void LCETCS_vCalMaxDltYawInTarDecMd(boolean_T rtu_lcetcsu1TarSpnDec, const
  TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, int16_T
  *rty_lcetcss16MaxDltYawInTarDecMd, DW_LCETCS_vCalMaxDltYawInTarDecMd_f_T
  *localDW)
{
  /* Switch: '<Root>/Switch' incorporates:
   *  Constant: '<Root>/Constant'
   */
  if (rtu_lcetcsu1TarSpnDec) {
  } else {
    localDW->UnitDelay1_DSTATE = 0;
  }

  /* MinMax: '<Root>/MinMax' */
  if (localDW->UnitDelay1_DSTATE >= rtu_ETCS_ESC_SIG->lcetcss16AbsDelYawFrst) {
  } else {
    /* Switch: '<Root>/Switch' */
    localDW->UnitDelay1_DSTATE = rtu_ETCS_ESC_SIG->lcetcss16AbsDelYawFrst;
  }

  /* End of MinMax: '<Root>/MinMax' */

  /* Saturate: '<Root>/Saturation' */
  if (localDW->UnitDelay1_DSTATE > 30000) {
    *rty_lcetcss16MaxDltYawInTarDecMd = 30000;
  } else if (localDW->UnitDelay1_DSTATE < 0) {
    *rty_lcetcss16MaxDltYawInTarDecMd = 0;
  } else {
    *rty_lcetcss16MaxDltYawInTarDecMd = localDW->UnitDelay1_DSTATE;
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Update for Switch: '<Root>/Switch' incorporates:
   *  Update for UnitDelay: '<Root>/Unit Delay1'
   */
  localDW->UnitDelay1_DSTATE = *rty_lcetcss16MaxDltYawInTarDecMd;
}

/* Model initialize function */
void LCETCS_vCalMaxDltYawInTarDecMd_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
