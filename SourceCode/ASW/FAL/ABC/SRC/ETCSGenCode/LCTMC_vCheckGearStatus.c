/*
 * File: LCTMC_vCheckGearStatus.c
 *
 * Code generated for Simulink model 'LCTMC_vCheckGearStatus'.
 *
 * Model version                  : 1.252
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:04:20 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCTMC_vCheckGearStatus.h"
#include "LCTMC_vCheckGearStatus_private.h"

/* Initial conditions for referenced model: 'LCTMC_vCheckGearStatus' */
void LCTMC_vCheckGearStatus_Init(boolean_T *rty_lctmcu1GearShiftCountSet,
  boolean_T *rty_lctmcu1NonCndTarReqGear, B_LCTMC_vCheckGearStatus_c_T *localB,
  DW_LCTMC_vCheckGearStatus_f_T *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCTMC_vDctDriveMode' */
  LCTMC_vDctDriveMode_Init(&localB->lctmcu1DriveGearMode,
    &(localDW->LCTMC_vDctDriveMode_DWORK1.rtb));

  /* InitializeConditions for ModelReference: '<Root>/LCTMC_vDctEnterCondition' */
  LCTMC_vDctEnterCondition_Init(rty_lctmcu1NonCndTarReqGear);

  /* InitializeConditions for ModelReference: '<Root>/LCTMC_vDctGearShiftCount' */
  LCTMC_vDctGearShiftCount_Init(rty_lctmcu1GearShiftCountSet,
    &(localDW->LCTMC_vDctGearShiftCount_DWORK1.rtdw));
}

/* Output and update for referenced model: 'LCTMC_vCheckGearStatus' */
void LCTMC_vCheckGearStatus(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT,
  boolean_T rtu_lcu1TmcInhibitMode, const TypeTMCReqGearStruct
  *rtu_TMC_REQ_GEAR_STRUCT_OLD, const TypeETCSExtDctStruct *rtu_ETCS_EXT_DCT,
  const TypeETCSGearStruct *rtu_ETCS_GEAR_STRUCT, boolean_T
  *rty_lctmcu1GearShiftCountSet, boolean_T *rty_lctmcu1NonCndTarReqGear,
  B_LCTMC_vCheckGearStatus_c_T *localB, DW_LCTMC_vCheckGearStatus_f_T *localDW)
{
  /* ModelReference: '<Root>/LCTMC_vDctDriveMode' */
  LCTMC_vDctDriveMode(rtu_ETCS_GEAR_STRUCT, &localB->lctmcu1DriveGearMode,
                      &(localDW->LCTMC_vDctDriveMode_DWORK1.rtb));

  /* ModelReference: '<Root>/LCTMC_vDctEnterCondition' */
  LCTMC_vDctEnterCondition(localB->lctmcu1DriveGearMode, rtu_ETCS_EXT_DCT,
    rtu_TMC_REQ_GEAR_STRUCT_OLD->lctmcu8_BSTGRReqGear, rtu_ETCS_CTL_ACT,
    rtu_lcu1TmcInhibitMode, rty_lctmcu1NonCndTarReqGear);

  /* ModelReference: '<Root>/LCTMC_vDctGearShiftCount' */
  LCTMC_vDctGearShiftCount(rtu_TMC_REQ_GEAR_STRUCT_OLD->lctmcu8_BSTGRReqGear,
    localB->lctmcu1DriveGearMode, rtu_ETCS_CTL_ACT, rty_lctmcu1GearShiftCountSet,
    &(localDW->LCTMC_vDctGearShiftCount_DWORK1.rtdw));
}

/* Model initialize function */
void LCTMC_vCheckGearStatus_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCTMC_vDctDriveMode' */
  LCTMC_vDctDriveMode_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCTMC_vDctEnterCondition' */
  LCTMC_vDctEnterCondition_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCTMC_vDctGearShiftCount' */
  LCTMC_vDctGearShiftCount_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
