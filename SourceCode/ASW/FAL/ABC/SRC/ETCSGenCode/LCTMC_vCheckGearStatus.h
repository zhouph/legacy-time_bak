/*
 * File: LCTMC_vCheckGearStatus.h
 *
 * Code generated for Simulink model 'LCTMC_vCheckGearStatus'.
 *
 * Model version                  : 1.252
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:04:20 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCTMC_vCheckGearStatus_h_
#define RTW_HEADER_LCTMC_vCheckGearStatus_h_
#ifndef LCTMC_vCheckGearStatus_COMMON_INCLUDES_
# define LCTMC_vCheckGearStatus_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCTMC_vCheckGearStatus_COMMON_INCLUDES_ */

#include "LCTMC_vCheckGearStatus_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCTMC_vDctGearShiftCount.h"
#include "LCTMC_vDctEnterCondition.h"
#include "LCTMC_vDctDriveMode.h"

/* Block signals for model 'LCTMC_vCheckGearStatus' */
typedef struct {
  boolean_T lctmcu1DriveGearMode;      /* '<Root>/LCTMC_vDctDriveMode' */
} B_LCTMC_vCheckGearStatus_c_T;

/* Block states (auto storage) for model 'LCTMC_vCheckGearStatus' */
typedef struct {
  MdlrefDW_LCTMC_vDctDriveMode_T LCTMC_vDctDriveMode_DWORK1;/* '<Root>/LCTMC_vDctDriveMode' */
  MdlrefDW_LCTMC_vDctGearShiftCount_T LCTMC_vDctGearShiftCount_DWORK1;/* '<Root>/LCTMC_vDctGearShiftCount' */
} DW_LCTMC_vCheckGearStatus_f_T;

typedef struct {
  B_LCTMC_vCheckGearStatus_c_T rtb;
  DW_LCTMC_vCheckGearStatus_f_T rtdw;
} MdlrefDW_LCTMC_vCheckGearStatus_T;

/* Model reference registration function */
extern void LCTMC_vCheckGearStatus_initialize(void);
extern void LCTMC_vCheckGearStatus_Init(boolean_T *rty_lctmcu1GearShiftCountSet,
  boolean_T *rty_lctmcu1NonCndTarReqGear, B_LCTMC_vCheckGearStatus_c_T *localB,
  DW_LCTMC_vCheckGearStatus_f_T *localDW);
extern void LCTMC_vCheckGearStatus(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT,
  boolean_T rtu_lcu1TmcInhibitMode, const TypeTMCReqGearStruct
  *rtu_TMC_REQ_GEAR_STRUCT_OLD, const TypeETCSExtDctStruct *rtu_ETCS_EXT_DCT,
  const TypeETCSGearStruct *rtu_ETCS_GEAR_STRUCT, boolean_T
  *rty_lctmcu1GearShiftCountSet, boolean_T *rty_lctmcu1NonCndTarReqGear,
  B_LCTMC_vCheckGearStatus_c_T *localB, DW_LCTMC_vCheckGearStatus_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCTMC_vCheckGearStatus'
 */
#endif                                 /* RTW_HEADER_LCTMC_vCheckGearStatus_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
