/*
 * File: LCETCS_vDctEngStallAxSenExist.c
 *
 * Code generated for Simulink model 'LCETCS_vDctEngStallAxSenExist'.
 *
 * Model version                  : 1.237
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:55:29 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctEngStallAxSenExist.h"
#include "LCETCS_vDctEngStallAxSenExist_private.h"

/* Output and update for referenced model: 'LCETCS_vDctEngStallAxSenExist' */
void LCETCS_vDctEngStallAxSenExist(int16_T rtu_lcetcss16EngStallThrTemp, int16_T
  *rty_lcetcss16EngStallThr)
{
  /* Switch: '<Root>/Switch1' incorporates:
   *  Constant: '<Root>/Constant1'
   *  Constant: '<Root>/Engine Stall RPM'
   *  DataTypeConversion: '<Root>/Data Type Conversion'
   */
  if (((uint8_T)U8ETCSCpAxSenAvail) != 0) {
    *rty_lcetcss16EngStallThr = rtu_lcetcss16EngStallThrTemp;
  } else {
    *rty_lcetcss16EngStallThr = ((int16_T)S16ETCSCpEngStallRpm);
  }

  /* End of Switch: '<Root>/Switch1' */

  /* Saturate: '<Root>/Saturation1' */
  if ((*rty_lcetcss16EngStallThr) > 3000) {
    *rty_lcetcss16EngStallThr = 3000;
  } else {
    if ((*rty_lcetcss16EngStallThr) < 0) {
      *rty_lcetcss16EngStallThr = 0;
    }
  }

  /* End of Saturate: '<Root>/Saturation1' */
}

/* Model initialize function */
void LCETCS_vDctEngStallAxSenExist_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
