/*
 * File: LCETCS_vDctLwToSplt_private.h
 *
 * Code generated for Simulink model 'LCETCS_vDctLwToSplt'.
 *
 * Model version                  : 1.289
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:06:24 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vDctLwToSplt_private_h_
#define RTW_HEADER_LCETCS_vDctLwToSplt_private_h_
#include "rtwtypes.h"
#include "model_reference_types.h"

/* Includes for objects with custom storage classes. */
#include "../ETCSGenCodeHeader/ETCSHeader4Import.h"

/*
 * Generate compile time checks that imported macros for parameters
 * with storage class "ImportedDefine" are defined
 */
#ifndef S16ETCSCpCdrnTrqAtAsphalt
#error The variable for the parameter "S16ETCSCpCdrnTrqAtAsphalt" is not defined
#endif

#ifndef S16ETCSCpLwToSpltDctCplCnt
#error The variable for the parameter "S16ETCSCpLwToSpltDctCplCnt" is not defined
#endif

#ifndef S16ETCSCpOffsetTrq4LwToSplt
#error The variable for the parameter "S16ETCSCpOffsetTrq4LwToSplt" is not defined
#endif

#ifndef VREF_0_5_KPH
#error The variable for the parameter "VREF_0_5_KPH" is not defined
#endif

#ifndef VREF_1_KPH
#error The variable for the parameter "VREF_1_KPH" is not defined
#endif

#ifndef U16TCSCpDctLwHmMaxCnt
#error The variable for the parameter "U16TCSCpDctLwHmMaxCnt" is not defined
#endif

#ifndef U8ETCSCpTarTransCplBrkTrq2Sp
#error The variable for the parameter "U8ETCSCpTarTransCplBrkTrq2Sp" is not defined
#endif

#ifndef U8ETCSCpTarTransStBrkTrq2Sp
#error The variable for the parameter "U8ETCSCpTarTransStBrkTrq2Sp" is not defined
#endif

#ifndef U8TCSCpAddRefRdCfFrci
#error The variable for the parameter "U8TCSCpAddRefRdCfFrci" is not defined
#endif

#ifndef U8TCSCpHghMuWhlSpinTh
#error The variable for the parameter "U8TCSCpHghMuWhlSpinTh" is not defined
#endif

#ifndef VarFwd
#error The variable for the parameter "VarFwd" is not defined
#endif
#endif                                 /* RTW_HEADER_LCETCS_vDctLwToSplt_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
