/*
 * File: LCETCS_vSelIVal2Rst.h
 *
 * Code generated for Simulink model 'LCETCS_vSelIVal2Rst'.
 *
 * Model version                  : 1.167
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:54:27 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vSelIVal2Rst_h_
#define RTW_HEADER_LCETCS_vSelIVal2Rst_h_
#ifndef LCETCS_vSelIVal2Rst_COMMON_INCLUDES_
# define LCETCS_vSelIVal2Rst_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vSelIVal2Rst_COMMON_INCLUDES_ */

#include "LCETCS_vSelIVal2Rst_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Model reference registration function */
extern void LCETCS_vSelIVal2Rst_initialize(void);
extern void LCETCS_vSelIVal2Rst_Init(int32_T *rty_lcetcss32IVal2Rst, boolean_T
  *rty_lcetcsu1RstIVal);
extern void LCETCS_vSelIVal2Rst(int32_T rtu_lcetcss32IValAtStrtOfCtl, const
  TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, int32_T rtu_lcetcss32IValAt2ndCylStrt,
  boolean_T rtu_lcetcsu1RstIValAt2ndCylStrt, int32_T
  rtu_lcetcss32IValAtErrZroCrs, boolean_T rtu_lcetcsu1RstIValAtErrZroCrs,
  boolean_T rtu_lcetcsu1IValRecTorqAct, int32_T rtu_lcetcss32IValRecTorq,
  boolean_T rtu_lcetcsu1IValVcaTorqAct, int32_T rtu_lcetcss32IValVcaTorq,
  int32_T *rty_lcetcss32IVal2Rst, boolean_T *rty_lcetcsu1RstIVal);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vSelIVal2Rst'
 * '<S1>'   : 'LCETCS_vSelIVal2Rst/Chart'
 */
#endif                                 /* RTW_HEADER_LCETCS_vSelIVal2Rst_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
