/*
 * File: LCETCS_vDctFunLampFlg.c
 *
 * Code generated for Simulink model 'LCETCS_vDctFunLampFlg'.
 *
 * Model version                  : 1.200
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:52:30 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctFunLampFlg.h"
#include "LCETCS_vDctFunLampFlg_private.h"

/* Initial conditions for referenced model: 'LCETCS_vDctFunLampFlg' */
void LCETCS_vDctFunLampFlg_Init(boolean_T *rty_lcetcsu1FuncLampOn)
{
  /* InitializeConditions for Chart: '<Root>/Chart' */
  *rty_lcetcsu1FuncLampOn = false;
}

/* Output and update for referenced model: 'LCETCS_vDctFunLampFlg' */
void LCETCS_vDctFunLampFlg(uint8_T rtu_lcetcsu8FuncLampOffEngCnt, const
  TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const TypeETCSExtDctStruct
  *rtu_ETCS_EXT_DCT, uint8_T rtu_lcetcsu8FuncLampOffBrkCnt, boolean_T
  *rty_lcetcsu1FuncLampOn)
{
  /* Chart: '<Root>/Chart' */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:9' */
  /* comment */
  if (((rtu_ETCS_EXT_DCT->lcetcsu1BTCSCtlAct == 0) &&
       (rtu_ETCS_CTL_ACT->lcetcsu1CtlAct == 0)) ||
      ((rtu_lcetcsu8FuncLampOffEngCnt >= ((uint8_T)U8ETCSCpFunLampOffCnt)) &&
       (rtu_lcetcsu8FuncLampOffBrkCnt >= ((uint8_T)U8ETCSCpFunLampOffCnt)))) {
    /* Transition: '<S1>:10' */
    /* Transition: '<S1>:108' */
    *rty_lcetcsu1FuncLampOn = false;

    /* Transition: '<S1>:146' */
  } else {
    /* Transition: '<S1>:79' */
    *rty_lcetcsu1FuncLampOn = true;
  }

  /* End of Chart: '<Root>/Chart' */
  /* Transition: '<S1>:68' */
}

/* Model initialize function */
void LCETCS_vDctFunLampFlg_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
