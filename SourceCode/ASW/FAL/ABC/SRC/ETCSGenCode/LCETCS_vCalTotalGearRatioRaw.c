/*
 * File: LCETCS_vCalTotalGearRatioRaw.c
 *
 * Code generated for Simulink model 'LCETCS_vCalTotalGearRatioRaw'.
 *
 * Model version                  : 1.94
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:47:32 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalTotalGearRatioRaw.h"
#include "LCETCS_vCalTotalGearRatioRaw_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalTotalGearRatioRaw' */
void LCETCS_vCalTotalGearRatioRaw_Init(DW_LCETCS_vCalTotalGearRatioRaw_f_T
  *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = 0;
}

/* Output and update for referenced model: 'LCETCS_vCalTotalGearRatioRaw' */
void LCETCS_vCalTotalGearRatioRaw(const TypeETCS4WDSigStruct *rtu_ETCS_4WD_SIG,
  const TypeETCSGearStruct *rtu_ETCS_GEAR_STRUCT, int16_T
  *rty_lcetcss16TotalGearRatioRaw, DW_LCETCS_vCalTotalGearRatioRaw_f_T *localDW)
{
  int32_T u0;
  int16_T rtu_ETCS_4WD_SIG_0;

  /* Chart: '<Root>/Chart3' */
  /* Gateway: Chart3 */
  /* During: Chart3 */
  /* Entry Internal: Chart3 */
  /* Transition: '<S1>:123' */
  if (rtu_ETCS_GEAR_STRUCT->lcetcss16CurntGearStep == ((int16_T)GEAR_POS_TM_PN))
  {
    /* Transition: '<S1>:189' */
    /* Transition: '<S1>:188' */
    /* Transition: '<S1>:192' */
    /* Transition: '<S1>:193' */
    /* Transition: '<S1>:104' */
    /* Transition: '<S1>:110' */
    /* Transition: '<S1>:115' */
    /* Transition: '<S1>:120' */
    /* Transition: '<S1>:143' */
    /* Transition: '<S1>:144' */
    /* Transition: '<S1>:122' */
  } else {
    /* Transition: '<S1>:190' */
    if (rtu_ETCS_GEAR_STRUCT->lcetcss16CurntGearStep == ((int16_T)GEAR_POS_TM_R))
    {
      /* Transition: '<S1>:101' */
      /* Transition: '<S1>:102' */
      localDW->UnitDelay_DSTATE = ((int16_T)S16ETCSCpTotalGearRatio_R);

      /* Transition: '<S1>:103' */
      /* Transition: '<S1>:104' */
      /* Transition: '<S1>:110' */
      /* Transition: '<S1>:115' */
      /* Transition: '<S1>:120' */
      /* Transition: '<S1>:143' */
      /* Transition: '<S1>:144' */
      /* Transition: '<S1>:122' */
    } else {
      /* Transition: '<S1>:108' */
      if (rtu_ETCS_GEAR_STRUCT->lcetcss16CurntGearStep == ((int16_T)
           GEAR_POS_TM_1)) {
        /* Transition: '<S1>:105' */
        /* Transition: '<S1>:106' */
        localDW->UnitDelay_DSTATE = ((int16_T)S16ETCSCpTotalGearRatio_1);

        /* Transition: '<S1>:107' */
        /* Transition: '<S1>:110' */
        /* Transition: '<S1>:115' */
        /* Transition: '<S1>:120' */
        /* Transition: '<S1>:143' */
        /* Transition: '<S1>:144' */
        /* Transition: '<S1>:122' */
      } else {
        /* Transition: '<S1>:109' */
        if (rtu_ETCS_GEAR_STRUCT->lcetcss16CurntGearStep == ((int16_T)
             GEAR_POS_TM_2)) {
          /* Transition: '<S1>:111' */
          /* Transition: '<S1>:112' */
          localDW->UnitDelay_DSTATE = ((int16_T)S16ETCSCpTotalGearRatio_2);

          /* Transition: '<S1>:113' */
          /* Transition: '<S1>:115' */
          /* Transition: '<S1>:120' */
          /* Transition: '<S1>:143' */
          /* Transition: '<S1>:144' */
          /* Transition: '<S1>:122' */
        } else {
          /* Transition: '<S1>:114' */
          if (rtu_ETCS_GEAR_STRUCT->lcetcss16CurntGearStep == ((int16_T)
               GEAR_POS_TM_3)) {
            /* Transition: '<S1>:116' */
            /* Transition: '<S1>:117' */
            localDW->UnitDelay_DSTATE = ((int16_T)S16ETCSCpTotalGearRatio_3);

            /* Transition: '<S1>:118' */
            /* Transition: '<S1>:120' */
            /* Transition: '<S1>:143' */
            /* Transition: '<S1>:144' */
            /* Transition: '<S1>:122' */
          } else {
            /* Transition: '<S1>:119' */
            if (rtu_ETCS_GEAR_STRUCT->lcetcss16CurntGearStep == ((int16_T)
                 GEAR_POS_TM_4)) {
              /* Transition: '<S1>:128' */
              /* Transition: '<S1>:129' */
              localDW->UnitDelay_DSTATE = ((int16_T)S16ETCSCpTotalGearRatio_4);

              /* Transition: '<S1>:145' */
              /* Transition: '<S1>:143' */
              /* Transition: '<S1>:144' */
              /* Transition: '<S1>:122' */
            } else {
              /* Transition: '<S1>:137' */
              if (rtu_ETCS_GEAR_STRUCT->lcetcss16CurntGearStep == ((int16_T)
                   GEAR_POS_TM_5)) {
                /* Transition: '<S1>:134' */
                /* Transition: '<S1>:135' */
                localDW->UnitDelay_DSTATE = ((int16_T)S16ETCSCpTotalGearRatio_5);

                /* Transition: '<S1>:146' */
                /* Transition: '<S1>:144' */
                /* Transition: '<S1>:122' */
              } else {
                /* Transition: '<S1>:138' */
                /*  For the vehicle has higher gear step than 5, the total gear ratio will be saturated to fifth gear  */
                localDW->UnitDelay_DSTATE = ((int16_T)S16ETCSCpTotalGearRatio_5);
              }
            }
          }
        }
      }
    }
  }

  /* End of Chart: '<Root>/Chart3' */

  /* Switch: '<Root>/Switch' incorporates:
   *  Constant: '<Root>/Transfer case gear ratio high'
   *  Constant: '<Root>/Transfer case gear ratio low'
   */
  /* Transition: '<S1>:121' */
  if (rtu_ETCS_4WD_SIG->lcetcsu14wdLwMd) {
    rtu_ETCS_4WD_SIG_0 = ((int16_T)S16ETCSCpTrnfrCsGrRtoLwMd);
  } else {
    rtu_ETCS_4WD_SIG_0 = ((int16_T)S16ETCSCpTrnfrCsGrRtoHgMd);
  }

  /* End of Switch: '<Root>/Switch' */

  /* Product: '<Root>/Divide' incorporates:
   *  Product: '<Root>/Product'
   */
  u0 = (localDW->UnitDelay_DSTATE * rtu_ETCS_4WD_SIG_0) / 100;

  /* Saturate: '<Root>/Saturation' */
  if (u0 > 10000) {
    *rty_lcetcss16TotalGearRatioRaw = 10000;
  } else if (u0 < 0) {
    *rty_lcetcss16TotalGearRatioRaw = 0;
  } else {
    *rty_lcetcss16TotalGearRatioRaw = (int16_T)u0;
  }

  /* End of Saturate: '<Root>/Saturation' */
}

/* Model initialize function */
void LCETCS_vCalTotalGearRatioRaw_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
