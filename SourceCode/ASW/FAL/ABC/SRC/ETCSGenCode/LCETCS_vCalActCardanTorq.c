/*
 * File: LCETCS_vCalActCardanTorq.c
 *
 * Code generated for Simulink model 'LCETCS_vCalActCardanTorq'.
 *
 * Model version                  : 1.108
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:46:58 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalActCardanTorq.h"
#include "LCETCS_vCalActCardanTorq_private.h"

/* Output and update for referenced model: 'LCETCS_vCalActCardanTorq' */
void LCETCS_vCalActCardanTorq(const TypeETCSDrvMdlStruct *rtu_ETCS_DRV_MDL,
  const TypeETCSEngStruct *rtu_ETCS_ENG_STRUCT, const TypeETCSEngMdlStruct
  *rtu_ETCS_ENG_MDL, TypeETCSCrdnTrqStruct *rty_ETCS_CRDN_TRQ)
{
  int32_T u0;

  /* Product: '<Root>/Divide' incorporates:
   *  Product: '<Root>/Product'
   */
  u0 = (rtu_ETCS_DRV_MDL->lcetcss16TrqRatio *
        rtu_ETCS_ENG_MDL->lcetcss16ActEngTrqDeld) / 100;

  /* Saturate: '<Root>/Saturation' */
  if (u0 > 1500000) {
    rty_ETCS_CRDN_TRQ->lcetcss32RealCrdnTrq = 1500000;
  } else if (u0 < -1000000) {
    rty_ETCS_CRDN_TRQ->lcetcss32RealCrdnTrq = -1000000;
  } else {
    rty_ETCS_CRDN_TRQ->lcetcss32RealCrdnTrq = u0;
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Product: '<Root>/Divide1' incorporates:
   *  Product: '<Root>/Product1'
   */
  u0 = (rtu_ETCS_DRV_MDL->lcetcss16TotalGearRatio *
        rtu_ETCS_ENG_STRUCT->lcetcss16ActEngTrq) / 100;

  /* Saturate: '<Root>/Saturation1' */
  if (u0 > 1500000) {
    rty_ETCS_CRDN_TRQ->lcetcss32ActCrdnTrq = 1500000;
  } else if (u0 < -1000000) {
    rty_ETCS_CRDN_TRQ->lcetcss32ActCrdnTrq = -1000000;
  } else {
    rty_ETCS_CRDN_TRQ->lcetcss32ActCrdnTrq = u0;
  }

  /* End of Saturate: '<Root>/Saturation1' */

  /* Product: '<Root>/Divide2' incorporates:
   *  Product: '<Root>/Product2'
   */
  u0 = (rtu_ETCS_ENG_STRUCT->lcetcss16DrvReqTrq *
        rtu_ETCS_DRV_MDL->lcetcss16TotalGearRatio) / 100;

  /* Saturate: '<Root>/Saturation2' */
  if (u0 > 1500000) {
    rty_ETCS_CRDN_TRQ->lcetcss32DrvReqCrdnTrq = 1500000;
  } else if (u0 < -1000000) {
    rty_ETCS_CRDN_TRQ->lcetcss32DrvReqCrdnTrq = -1000000;
  } else {
    rty_ETCS_CRDN_TRQ->lcetcss32DrvReqCrdnTrq = u0;
  }

  /* End of Saturate: '<Root>/Saturation2' */
}

/* Model initialize function */
void LCETCS_vCalActCardanTorq_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
