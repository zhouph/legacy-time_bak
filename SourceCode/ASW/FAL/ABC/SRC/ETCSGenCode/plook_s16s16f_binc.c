/*
 * File: plook_s16s16f_binc.c
 *
 * Code generated for Simulink model 'LCETCS_vCalRefTrqAtStrtOfCtl'.
 *
 * Model version                  : 1.193
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:54:40 2015
 */

#include "rtwtypes.h"
#include "binsearch_s16s16.h"
#include "plook_s16s16f_binc.h"

int16_T plook_s16s16f_binc(int16_T u, const int16_T bp[], uint32_T maxIndex,
  real32_T *fraction)
{
  int16_T bpIndex;

  /* Prelookup - Index and Fraction
     Index Search method: 'binary'
     Extrapolation method: 'Clip'
     Use previous index: 'off'
     Use last breakpoint for index at or above upper limit: 'off'
     Remove protection against out-of-range input in generated code: 'off'
   */
  if (u <= bp[0U]) {
    bpIndex = 0;
    *fraction = 0.0F;
  } else if (u < bp[maxIndex]) {
    bpIndex = binsearch_s16s16(u, bp, maxIndex >> 1U, maxIndex);
    *fraction = ((real32_T)((uint16_T)(((uint32_T)u) - bp[(uint32_T)bpIndex]))) /
      ((real32_T)((uint16_T)(bp[bpIndex + 1U] - bp[(uint32_T)bpIndex])));
  } else {
    bpIndex = (int16_T)(maxIndex - 1U);
    *fraction = 1.0F;
  }

  return bpIndex;
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
