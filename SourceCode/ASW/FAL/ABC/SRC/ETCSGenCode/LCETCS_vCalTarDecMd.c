/*
 * File: LCETCS_vCalTarDecMd.c
 *
 * Code generated for Simulink model 'LCETCS_vCalTarDecMd'.
 *
 * Model version                  : 1.176
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:49:07 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalTarDecMd.h"
#include "LCETCS_vCalTarDecMd_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalTarDecMd' */
void LCETCS_vCalTarDecMd_Init(boolean_T *rty_lcetcsu1TarSpnDec,
  DW_LCETCS_vCalTarDecMd_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = 0;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = false;

  /* InitializeConditions for Chart: '<Root>/Chart' */
  *rty_lcetcsu1TarSpnDec = false;
}

/* Output and update for referenced model: 'LCETCS_vCalTarDecMd' */
void LCETCS_vCalTarDecMd(int16_T rtu_lcetcss16LmtCrngDltYaw, const
  TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const TypeETCSStrStatStruct
  *rtu_ETCS_STR_STAT, int16_T *rty_lcetcss16DltYawStbCnt, boolean_T
  *rty_lcetcsu1TarSpnDec, DW_LCETCS_vCalTarDecMd_f_T *localDW)
{
  int16_T rtu_ETCS_ESC_SIG_0;
  int32_T tmp;
  int32_T tmp_0;

  /* Chart: '<Root>/Chart' incorporates:
   *  UnitDelay: '<Root>/Unit Delay1'
   *  UnitDelay: '<Root>/Unit Delay2'
   */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:34' */
  /*  Understeering and oversteering  */
  if ((rtu_ETCS_ESC_SIG->lcetcss16DelYawFrst < (-rtu_lcetcss16LmtCrngDltYaw)) ||
      (rtu_ETCS_ESC_SIG->lcetcss16DelYawFrst > rtu_lcetcss16LmtCrngDltYaw)) {
    /* Transition: '<S1>:35' */
    /* Transition: '<S1>:43' */
    *rty_lcetcss16DltYawStbCnt = 0;
    if (rtu_ETCS_STR_STAT->lcetcss16StrCnvrgCnt >= ((uint8_T)U8ETCSCpCrnExDctCnt))
    {
      /* Transition: '<S1>:36' */
      /* Transition: '<S1>:41' */
      *rty_lcetcsu1TarSpnDec = false;

      /* Transition: '<S1>:40' */
    } else {
      /* Transition: '<S1>:42' */
      *rty_lcetcsu1TarSpnDec = true;
    }

    /* Transition: '<S1>:169' */
    /* Transition: '<S1>:98' */
  } else {
    /* Transition: '<S1>:38' */
    /*  Stable  */
    tmp_0 = localDW->UnitDelay2_DSTATE + 1;
    if (tmp_0 > 32767) {
      tmp_0 = 32767;
    }

    *rty_lcetcss16DltYawStbCnt = (int16_T)tmp_0;

    /*  100 means 1deg/s  */
    if (rtu_ETCS_ESC_SIG->lcetcss16YawRateCald < 0) {
      tmp = -rtu_ETCS_ESC_SIG->lcetcss16YawRateCald;
      if (tmp > 32767) {
        tmp = 32767;
      }

      rtu_ETCS_ESC_SIG_0 = (int16_T)tmp;
    } else {
      rtu_ETCS_ESC_SIG_0 = rtu_ETCS_ESC_SIG->lcetcss16YawRateCald;
    }

    if ((rtu_ETCS_STR_STAT->lcetcss16StrCnvrgCnt >= ((uint8_T)
          U8ETCSCpCrnExDctCnt)) || ((((int16_T)tmp_0) >= ((uint8_T)
           U8ETCSCpDltYawStbDctCnt)) && (rtu_ETCS_ESC_SIG_0 < 100))) {
      /* Transition: '<S1>:69' */
      /* Transition: '<S1>:78' */
      *rty_lcetcsu1TarSpnDec = false;

      /* Transition: '<S1>:98' */
    } else {
      /* Transition: '<S1>:95' */
      *rty_lcetcsu1TarSpnDec = localDW->UnitDelay1_DSTATE;
    }
  }

  /* End of Chart: '<Root>/Chart' */

  /* Saturate: '<Root>/Saturation1' */
  /* Transition: '<S1>:166' */
  if ((*rty_lcetcss16DltYawStbCnt) > ((uint8_T)U8ETCSCpDltYawStbDctCnt)) {
    *rty_lcetcss16DltYawStbCnt = ((uint8_T)U8ETCSCpDltYawStbDctCnt);
  } else {
    if ((*rty_lcetcss16DltYawStbCnt) < 0) {
      *rty_lcetcss16DltYawStbCnt = 0;
    }
  }

  /* End of Saturate: '<Root>/Saturation1' */

  /* Update for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = *rty_lcetcss16DltYawStbCnt;

  /* Update for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = *rty_lcetcsu1TarSpnDec;
}

/* Model initialize function */
void LCETCS_vCalTarDecMd_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
