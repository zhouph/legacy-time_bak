/*
 * File: LCETCS_vCalCtlErr.c
 *
 * Code generated for Simulink model 'LCETCS_vCalCtlErr'.
 *
 * Model version                  : 1.57
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:58:48 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalCtlErr.h"
#include "LCETCS_vCalCtlErr_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalCtlErr' */
void LCETCS_vCalCtlErr_Init(DW_LCETCS_vCalCtlErr_f_T *localDW,
  B_LCETCS_vCalCtlErr_c_T *localB)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctRisingEdge' */
  LCETCS_vDctRisingEdge_Init(&localB->RisingEdge,
    &(localDW->LCETCS_vDctRisingEdge_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctRisingEdge1' */
  LCETCS_vDctRisingEdge_Init(&localB->lcetcsu1FalEdgOfCyl1st,
    &(localDW->LCETCS_vDctRisingEdge1_DWORK1.rtdw));
}

/* Output and update for referenced model: 'LCETCS_vCalCtlErr' */
void LCETCS_vCalCtlErr(const TypeETCSWhlSpinStruct *rtu_ETCS_WHL_SPIN, const
  TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN, TypeETCSCtlErrStruct
  *rty_ETCS_CTL_ERR, DW_LCETCS_vCalCtlErr_f_T *localDW, B_LCETCS_vCalCtlErr_c_T *
  localB)
{
  /* local block i/o variables */
  boolean_T rtb_lcetcsu1CtlErrPos;
  boolean_T rtb_lcetcsu1CtlErrNeg;
  int16_T rtb_lcetcss16CtlErr;

  /* Sum: '<Root>/Add1' */
  rtb_lcetcss16CtlErr = (int16_T)(rtu_ETCS_TAR_SPIN->lcetcss16TarWhlSpin -
    rtu_ETCS_WHL_SPIN->lcetcss16WhlSpin);

  /* Saturate: '<Root>/Saturation' */
  if (rtb_lcetcss16CtlErr > 255) {
    rtb_lcetcss16CtlErr = 255;
  } else {
    if (rtb_lcetcss16CtlErr < -1200) {
      rtb_lcetcss16CtlErr = -1200;
    }
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Chart: '<Root>/Chart' */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:9' */
  /* comment */
  if (rtb_lcetcss16CtlErr >= 0) {
    /* Transition: '<S1>:10' */
    /* Transition: '<S1>:40' */
    rtb_lcetcsu1CtlErrPos = true;
    rtb_lcetcsu1CtlErrNeg = false;

    /* Transition: '<S1>:42' */
  } else {
    /* Transition: '<S1>:13' */
    rtb_lcetcsu1CtlErrPos = false;
    rtb_lcetcsu1CtlErrNeg = true;
  }

  /* End of Chart: '<Root>/Chart' */

  /* SignalConversion: '<Root>/Signal Conversion2' */
  /* Transition: '<S1>:12' */
  rty_ETCS_CTL_ERR->lcetcsu1CtlErrPos = rtb_lcetcsu1CtlErrPos;

  /* SignalConversion: '<Root>/Signal Conversion1' */
  rty_ETCS_CTL_ERR->lcetcsu1CtlErrNeg = rtb_lcetcsu1CtlErrNeg;

  /* ModelReference: '<Root>/LCETCS_vDctRisingEdge' */
  LCETCS_vDctRisingEdge(rtb_lcetcsu1CtlErrNeg, &localB->RisingEdge,
                        &(localDW->LCETCS_vDctRisingEdge_DWORK1.rtdw));

  /* SignalConversion: '<Root>/Signal Conversion3' */
  rty_ETCS_CTL_ERR->lcetcsu1RsgEdgOfCtlErrNeg = localB->RisingEdge;

  /* ModelReference: '<Root>/LCETCS_vDctRisingEdge1' */
  LCETCS_vDctRisingEdge(rtb_lcetcsu1CtlErrPos, &localB->lcetcsu1FalEdgOfCyl1st,
                        &(localDW->LCETCS_vDctRisingEdge1_DWORK1.rtdw));

  /* SignalConversion: '<Root>/Signal Conversion4' */
  rty_ETCS_CTL_ERR->lcetcsu1RsgEdgOfCtlErrPos = localB->lcetcsu1FalEdgOfCyl1st;

  /* BusCreator: '<Root>/Bus Creator' incorporates:
   *  SignalConversion: '<Root>/Signal Conversion6'
   */
  rty_ETCS_CTL_ERR->lcetcss16CtlErr = rtb_lcetcss16CtlErr;
}

/* Model initialize function */
void LCETCS_vCalCtlErr_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctRisingEdge' */
  LCETCS_vDctRisingEdge_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctRisingEdge1' */
  LCETCS_vDctRisingEdge_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
