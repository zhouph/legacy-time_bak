/*
 * File: LCETCS_vCalHomoBsPGain.c
 *
 * Code generated for Simulink model 'LCETCS_vCalHomoBsPGain'.
 *
 * Model version                  : 1.163
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:02:06 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalHomoBsPGain.h"
#include "LCETCS_vCalHomoBsPGain_private.h"

/* Start for referenced model: 'LCETCS_vCalHomoBsPGain' */
void LCETCS_vCalHomoBsPGain_Start(B_LCETCS_vCalHomoBsPGain_c_T *localB)
{
  /* Start for IfAction SubSystem: '<Root>/CalPGainWhnCtlOn' */
  /* Start for IfAction SubSystem: '<S2>/CalPgainWhnCtlErrNeg' */
  /* Start for Constant: '<S3>/Pgain for 5th gear ratio on ice' */
  localB->z11_j = ((uint8_T)U8ETCSCpNegErrPgainIce_5);

  /* Start for Constant: '<S3>/Pgain for 5th gear ratio on snow' */
  localB->z12_o = ((uint8_T)U8ETCSCpNegErrPgainSnw_5);

  /* Start for Constant: '<S3>/Pgain for 5th gear ratio on Asphalt' */
  localB->z13_f = ((uint8_T)U8ETCSCpNegErrPgainAsp_5);

  /* Start for Constant: '<S3>/Pgain for 4th gear ratio on ice' */
  localB->z21_p = ((uint8_T)U8ETCSCpNegErrPgainIce_4);

  /* Start for Constant: '<S3>/Pgain for 4th gear ratio on snow' */
  localB->z22_f = ((uint8_T)U8ETCSCpNegErrPgainSnw_4);

  /* Start for Constant: '<S3>/Pgain for 4th gear ratio on asphalt' */
  localB->z23_l = ((uint8_T)U8ETCSCpNegErrPgainAsp_4);

  /* Start for Constant: '<S3>/Pgain for 3rd gear ratio on ice' */
  localB->z31_b = ((uint8_T)U8ETCSCpNegErrPgainIce_3);

  /* Start for Constant: '<S3>/Pgain for 3rd gear ratio on snow' */
  localB->z32_p = ((uint8_T)U8ETCSCpNegErrPgainSnw_3);

  /* Start for Constant: '<S3>/Pgain for 3rd gear ratio on asphalt' */
  localB->z33_n = ((uint8_T)U8ETCSCpNegErrPgainAsp_3);

  /* Start for Constant: '<S3>/Pgain for 2nd gear ratio on ice' */
  localB->z41_d = ((uint8_T)U8ETCSCpNegErrPgainIce_2);

  /* Start for Constant: '<S3>/Pgain for 2nd gear ratio on snow' */
  localB->z42_h = ((uint8_T)U8ETCSCpNegErrPgainSnw_2);

  /* Start for Constant: '<S3>/Pgain for 2nd gear ratio on asphalt' */
  localB->z43_j = ((uint8_T)U8ETCSCpNegErrPgainAsp_2);

  /* Start for Constant: '<S3>/Pgain for 1st gear ratio on ice' */
  localB->z51_n = ((uint8_T)U8ETCSCpNegErrPgainIce_1);

  /* Start for Constant: '<S3>/Pgain for 1st gear ratio on snow' */
  localB->z52_l = ((uint8_T)U8ETCSCpNegErrPgainSnw_1);

  /* Start for Constant: '<S3>/Pgain for 1st gear ratio on asphalt' */
  localB->z53_k = ((uint8_T)U8ETCSCpNegErrPgainAsp_1);

  /* End of Start for SubSystem: '<S2>/CalPgainWhnCtlErrNeg' */

  /* InitializeConditions for IfAction SubSystem: '<S2>/CalPgainWhnCtlErrNeg' */

  /* InitializeConditions for ModelReference: '<S3>/LCETCS_s16Inter3by5' */
  LCETCS_s16Inter3by5_Init(&localB->lcetcss16HomoBsPgain_l);

  /* End of InitializeConditions for SubSystem: '<S2>/CalPgainWhnCtlErrNeg' */

  /* Start for IfAction SubSystem: '<S2>/CalPgainWhnCtlErrPos' */
  /* Start for Constant: '<S4>/Pgain for 5th gear ratio on ice' */
  localB->z11 = ((uint8_T)U8ETCSCpPosErrPgainIce_5);

  /* Start for Constant: '<S4>/Pgain for 5th gear ratio on snow' */
  localB->z12 = ((uint8_T)U8ETCSCpPosErrPgainSnw_5);

  /* Start for Constant: '<S4>/Pgain for 5th gear ratio on Asphalt' */
  localB->z13 = ((uint8_T)U8ETCSCpPosErrPgainAsp_5);

  /* Start for Constant: '<S4>/Pgain for 4th gear ratio on ice' */
  localB->z21 = ((uint8_T)U8ETCSCpPosErrPgainIce_4);

  /* Start for Constant: '<S4>/Pgain for 4th gear ratio on snow' */
  localB->z22 = ((uint8_T)U8ETCSCpPosErrPgainSnw_4);

  /* Start for Constant: '<S4>/Pgain for 4th gear ratio on asphalt' */
  localB->z23 = ((uint8_T)U8ETCSCpPosErrPgainAsp_4);

  /* Start for Constant: '<S4>/Pgain for 3rd gear ratio on ice' */
  localB->z31 = ((uint8_T)U8ETCSCpPosErrPgainIce_3);

  /* Start for Constant: '<S4>/Pgain for 3rd gear ratio on snow' */
  localB->z32 = ((uint8_T)U8ETCSCpPosErrPgainSnw_3);

  /* Start for Constant: '<S4>/Pgain for 3rd gear ratio on asphalt' */
  localB->z33 = ((uint8_T)U8ETCSCpPosErrPgainAsp_3);

  /* Start for Constant: '<S4>/Pgain for 2nd gear ratio on ice' */
  localB->z41 = ((uint8_T)U8ETCSCpPosErrPgainIce_2);

  /* Start for Constant: '<S4>/Pgain for 2nd gear ratio on snow' */
  localB->z42 = ((uint8_T)U8ETCSCpPosErrPgainSnw_2);

  /* Start for Constant: '<S4>/Pgain for 2nd gear ratio on asphalt' */
  localB->z43 = ((uint8_T)U8ETCSCpPosErrPgainAsp_2);

  /* Start for Constant: '<S4>/Pgain for 1st gear ratio on ice' */
  localB->z51 = ((uint8_T)U8ETCSCpPosErrPgainIce_1);

  /* Start for Constant: '<S4>/Pgain for 1st gear ratio on snow' */
  localB->z52 = ((uint8_T)U8ETCSCpPosErrPgainSnw_1);

  /* Start for Constant: '<S4>/Pgain for 1st gear ratio on asphalt' */
  localB->z53 = ((uint8_T)U8ETCSCpPosErrPgainAsp_1);

  /* End of Start for SubSystem: '<S2>/CalPgainWhnCtlErrPos' */

  /* InitializeConditions for IfAction SubSystem: '<S2>/CalPgainWhnCtlErrPos' */

  /* InitializeConditions for ModelReference: '<S4>/LCETCS_s16Inter3by5_1' */
  LCETCS_s16Inter3by5_Init(&localB->lcetcss16HomoBsPgain);

  /* End of InitializeConditions for SubSystem: '<S2>/CalPgainWhnCtlErrPos' */

  /* End of Start for SubSystem: '<Root>/CalPGainWhnCtlOn' */
}

/* Output and update for referenced model: 'LCETCS_vCalHomoBsPGain' */
void LCETCS_vCalHomoBsPGain(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const
  TypeETCSDrvMdlStruct *rtu_ETCS_DRV_MDL, const TypeETCSCtlErrStruct
  *rtu_ETCS_CTL_ERR, const TypeETCSTrq4RdFricStruct *rtu_ETCS_TRQ_REP_RD_FRIC,
  int16_T *rty_lcetcss16HomoBsPgain, B_LCETCS_vCalHomoBsPGain_c_T *localB)
{
  /* local block i/o variables */
  int16_T rtb_x;
  int16_T rtb_x_a;

  /* If: '<Root>/If' incorporates:
   *  Constant: '<S1>/Constant'
   *  If: '<S2>/If'
   */
  if (!rtu_ETCS_CTL_ACT->lcetcsu1CtlAct) {
    /* Outputs for IfAction SubSystem: '<Root>/CalPGainWhnCtlOff' incorporates:
     *  ActionPort: '<S1>/Action Port'
     */
    *rty_lcetcss16HomoBsPgain = 0;

    /* End of Outputs for SubSystem: '<Root>/CalPGainWhnCtlOff' */
  } else {
    /* Outputs for IfAction SubSystem: '<Root>/CalPGainWhnCtlOn' incorporates:
     *  ActionPort: '<S2>/Action Port'
     */
    if (!rtu_ETCS_CTL_ERR->lcetcsu1CtlErrPos) {
      /* Outputs for IfAction SubSystem: '<S2>/CalPgainWhnCtlErrNeg' incorporates:
       *  ActionPort: '<S3>/Action Port'
       */
      rtb_x_a = (int16_T)(rtu_ETCS_TRQ_REP_RD_FRIC->lcetcss32TrqRepRdFric / 10);
      localB->z11_j = ((uint8_T)U8ETCSCpNegErrPgainIce_5);
      localB->z12_o = ((uint8_T)U8ETCSCpNegErrPgainSnw_5);
      localB->z13_f = ((uint8_T)U8ETCSCpNegErrPgainAsp_5);
      localB->z21_p = ((uint8_T)U8ETCSCpNegErrPgainIce_4);
      localB->z22_f = ((uint8_T)U8ETCSCpNegErrPgainSnw_4);
      localB->z23_l = ((uint8_T)U8ETCSCpNegErrPgainAsp_4);
      localB->z31_b = ((uint8_T)U8ETCSCpNegErrPgainIce_3);
      localB->z32_p = ((uint8_T)U8ETCSCpNegErrPgainSnw_3);
      localB->z33_n = ((uint8_T)U8ETCSCpNegErrPgainAsp_3);
      localB->z41_d = ((uint8_T)U8ETCSCpNegErrPgainIce_2);
      localB->z42_h = ((uint8_T)U8ETCSCpNegErrPgainSnw_2);
      localB->z43_j = ((uint8_T)U8ETCSCpNegErrPgainAsp_2);
      localB->z51_n = ((uint8_T)U8ETCSCpNegErrPgainIce_1);
      localB->z52_l = ((uint8_T)U8ETCSCpNegErrPgainSnw_1);
      localB->z53_k = ((uint8_T)U8ETCSCpNegErrPgainAsp_1);

      /* ModelReference: '<S3>/LCETCS_s16Inter3by5' */
      LCETCS_s16Inter3by5(rtb_x_a, ((int16_T)S16ETCSCpCdrnTrqAtIce), ((int16_T)
        S16ETCSCpCdrnTrqAtSnow), ((int16_T)S16ETCSCpCdrnTrqAtAsphalt),
                          rtu_ETCS_DRV_MDL->lcetcss16TotalGearRatio, ((int16_T)
        S16ETCSCpTotalGearRatio_5), ((int16_T)S16ETCSCpTotalGearRatio_4),
                          ((int16_T)S16ETCSCpTotalGearRatio_3), ((int16_T)
        S16ETCSCpTotalGearRatio_2), ((int16_T)S16ETCSCpTotalGearRatio_1),
                          localB->z11_j, localB->z12_o, localB->z13_f,
                          localB->z21_p, localB->z22_f, localB->z23_l,
                          localB->z31_b, localB->z32_p, localB->z33_n,
                          localB->z41_d, localB->z42_h, localB->z43_j,
                          localB->z51_n, localB->z52_l, localB->z53_k,
                          &localB->lcetcss16HomoBsPgain_l);
      *rty_lcetcss16HomoBsPgain = localB->lcetcss16HomoBsPgain_l;

      /* End of Outputs for SubSystem: '<S2>/CalPgainWhnCtlErrNeg' */
    } else {
      /* Outputs for IfAction SubSystem: '<S2>/CalPgainWhnCtlErrPos' incorporates:
       *  ActionPort: '<S4>/Action Port'
       */
      rtb_x = (int16_T)(rtu_ETCS_TRQ_REP_RD_FRIC->lcetcss32TrqRepRdFric / 10);
      localB->z11 = ((uint8_T)U8ETCSCpPosErrPgainIce_5);
      localB->z12 = ((uint8_T)U8ETCSCpPosErrPgainSnw_5);
      localB->z13 = ((uint8_T)U8ETCSCpPosErrPgainAsp_5);
      localB->z21 = ((uint8_T)U8ETCSCpPosErrPgainIce_4);
      localB->z22 = ((uint8_T)U8ETCSCpPosErrPgainSnw_4);
      localB->z23 = ((uint8_T)U8ETCSCpPosErrPgainAsp_4);
      localB->z31 = ((uint8_T)U8ETCSCpPosErrPgainIce_3);
      localB->z32 = ((uint8_T)U8ETCSCpPosErrPgainSnw_3);
      localB->z33 = ((uint8_T)U8ETCSCpPosErrPgainAsp_3);
      localB->z41 = ((uint8_T)U8ETCSCpPosErrPgainIce_2);
      localB->z42 = ((uint8_T)U8ETCSCpPosErrPgainSnw_2);
      localB->z43 = ((uint8_T)U8ETCSCpPosErrPgainAsp_2);
      localB->z51 = ((uint8_T)U8ETCSCpPosErrPgainIce_1);
      localB->z52 = ((uint8_T)U8ETCSCpPosErrPgainSnw_1);
      localB->z53 = ((uint8_T)U8ETCSCpPosErrPgainAsp_1);

      /* ModelReference: '<S4>/LCETCS_s16Inter3by5_1' */
      LCETCS_s16Inter3by5(rtb_x, ((int16_T)S16ETCSCpCdrnTrqAtIce), ((int16_T)
        S16ETCSCpCdrnTrqAtSnow), ((int16_T)S16ETCSCpCdrnTrqAtAsphalt),
                          rtu_ETCS_DRV_MDL->lcetcss16TotalGearRatio, ((int16_T)
        S16ETCSCpTotalGearRatio_5), ((int16_T)S16ETCSCpTotalGearRatio_4),
                          ((int16_T)S16ETCSCpTotalGearRatio_3), ((int16_T)
        S16ETCSCpTotalGearRatio_2), ((int16_T)S16ETCSCpTotalGearRatio_1),
                          localB->z11, localB->z12, localB->z13, localB->z21,
                          localB->z22, localB->z23, localB->z31, localB->z32,
                          localB->z33, localB->z41, localB->z42, localB->z43,
                          localB->z51, localB->z52, localB->z53,
                          &localB->lcetcss16HomoBsPgain);
      *rty_lcetcss16HomoBsPgain = localB->lcetcss16HomoBsPgain;

      /* End of Outputs for SubSystem: '<S2>/CalPgainWhnCtlErrPos' */
    }

    /* End of Outputs for SubSystem: '<Root>/CalPGainWhnCtlOn' */
  }

  /* End of If: '<Root>/If' */
}

/* Model initialize function */
void LCETCS_vCalHomoBsPGain_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<S3>/LCETCS_s16Inter3by5' */
  LCETCS_s16Inter3by5_initialize();

  /* Model Initialize fcn for ModelReference Block: '<S4>/LCETCS_s16Inter3by5_1' */
  LCETCS_s16Inter3by5_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
