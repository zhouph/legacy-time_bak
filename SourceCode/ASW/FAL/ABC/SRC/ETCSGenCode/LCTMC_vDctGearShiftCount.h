/*
 * File: LCTMC_vDctGearShiftCount.h
 *
 * Code generated for Simulink model 'LCTMC_vDctGearShiftCount'.
 *
 * Model version                  : 1.254
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:53:27 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCTMC_vDctGearShiftCount_h_
#define RTW_HEADER_LCTMC_vDctGearShiftCount_h_
#ifndef LCTMC_vDctGearShiftCount_COMMON_INCLUDES_
# define LCTMC_vDctGearShiftCount_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCTMC_vDctGearShiftCount_COMMON_INCLUDES_ */

#include "LCTMC_vDctGearShiftCount_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Block states (auto storage) for model 'LCTMC_vDctGearShiftCount' */
typedef struct {
  uint16_T UnitDelay2_DSTATE;          /* '<Root>/Unit Delay2' */
  uint8_T UnitDelay1_DSTATE;           /* '<Root>/Unit Delay1' */
  boolean_T UnitDelay3_DSTATE;         /* '<Root>/Unit Delay3' */
} DW_LCTMC_vDctGearShiftCount_f_T;

typedef struct {
  DW_LCTMC_vDctGearShiftCount_f_T rtdw;
} MdlrefDW_LCTMC_vDctGearShiftCount_T;

/* Model reference registration function */
extern void LCTMC_vDctGearShiftCount_initialize(void);
extern void LCTMC_vDctGearShiftCount_Init(boolean_T
  *rty_lctmcu1GearShiftCountSet, DW_LCTMC_vDctGearShiftCount_f_T *localDW);
extern void LCTMC_vDctGearShiftCount(uint8_T rtu_lctmcu8_BSTGRReqGearOld,
  boolean_T rtu_lctmcu1DriveGearMode, const TypeETCSCtlActStruct
  *rtu_ETCS_CTL_ACT, boolean_T *rty_lctmcu1GearShiftCountSet,
  DW_LCTMC_vDctGearShiftCount_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCTMC_vDctGearShiftCount'
 * '<S1>'   : 'LCTMC_vDctGearShiftCount/Chart1'
 */
#endif                                 /* RTW_HEADER_LCTMC_vDctGearShiftCount_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
