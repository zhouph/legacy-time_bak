/*
 * File: LCTMC_vCalRefHysTarGearMap.h
 *
 * Code generated for Simulink model 'LCTMC_vCalRefHysTarGearMap'.
 *
 * Model version                  : 1.299
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:53:06 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCTMC_vCalRefHysTarGearMap_h_
#define RTW_HEADER_LCTMC_vCalRefHysTarGearMap_h_
#ifndef LCTMC_vCalRefHysTarGearMap_COMMON_INCLUDES_
# define LCTMC_vCalRefHysTarGearMap_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCTMC_vCalRefHysTarGearMap_COMMON_INCLUDES_ */

#include "LCTMC_vCalRefHysTarGearMap_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Model reference registration function */
extern void LCTMC_vCalRefHysTarGearMap_initialize(void);
extern void LCTMC_vCalRefHysTarGearMap(boolean_T rtu_lctmcu1NonCndTarReqGear,
  const TypeTMCReqGearStruct *rtu_TMC_REQ_GEAR_STRUCT_OLD, const
  TypeETCSGearStruct *rtu_ETCS_GEAR_STRUCT, int16_T
  rtu_lctmcs16CordTarUpShtMaxVSbyGear, int16_T
  rtu_lctmcs16CordTarDnShtMinVSbyGear, int16_T rtu_lctmcs16CordTarUpShtVS,
  int16_T rtu_lctmcs16CordTarUpShtRPM, int16_T rtu_lctmcs16CordTarDnShtVS,
  int16_T rtu_lctmcs16CordTarDnShtRPM, TypeTMCRefLimVehSpdMap
  *rty_TMC_REF_LIM_VS_MAP, TypeTMCRefFinTarGearMap *rty_TMC_REF_FIN_TAR_GEAR_MAP);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCTMC_vCalRefHysTarGearMap'
 * '<S1>'   : 'LCTMC_vCalRefHysTarGearMap/Chart1'
 */
#endif                                 /* RTW_HEADER_LCTMC_vCalRefHysTarGearMap_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
