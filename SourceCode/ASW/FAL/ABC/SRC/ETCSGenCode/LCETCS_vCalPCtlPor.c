/*
 * File: LCETCS_vCalPCtlPor.c
 *
 * Code generated for Simulink model 'LCETCS_vCalPCtlPor'.
 *
 * Model version                  : 1.191
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:51:28 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalPCtlPor.h"
#include "LCETCS_vCalPCtlPor_private.h"

/* Output and update for referenced model: 'LCETCS_vCalPCtlPor' */
void LCETCS_vCalPCtlPor(const TypeETCSCtlErrStruct *rtu_ETCS_CTL_ERR, const
  TypeETCSGainStruct *rtu_ETCS_CTL_GAINS, int32_T *rty_lcetcss32PCtlPor)
{
  /* Product: '<Root>/Divide' */
  *rty_lcetcss32PCtlPor = rtu_ETCS_CTL_ERR->lcetcss16CtlErr *
    rtu_ETCS_CTL_GAINS->lcetcss16Pgain;

  /* Saturate: '<Root>/Saturation1' */
  if ((*rty_lcetcss32PCtlPor) > 100000) {
    *rty_lcetcss32PCtlPor = 100000;
  } else {
    if ((*rty_lcetcss32PCtlPor) < -100000) {
      *rty_lcetcss32PCtlPor = -100000;
    }
  }

  /* End of Saturate: '<Root>/Saturation1' */
}

/* Model initialize function */
void LCETCS_vCalPCtlPor_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
