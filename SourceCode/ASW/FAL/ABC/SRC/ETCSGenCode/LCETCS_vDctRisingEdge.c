/*
 * File: LCETCS_vDctRisingEdge.c
 *
 * Code generated for Simulink model 'LCETCS_vDctRisingEdge'.
 *
 * Model version                  : 1.83
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:47:11 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctRisingEdge.h"
#include "LCETCS_vDctRisingEdge_private.h"

/* Initial conditions for referenced model: 'LCETCS_vDctRisingEdge' */
void LCETCS_vDctRisingEdge_Init(boolean_T *rty_RisingEdge,
  DW_LCETCS_vDctRisingEdge_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = false;

  /* InitializeConditions for Chart: '<Root>/Chart' */
  *rty_RisingEdge = false;
}

/* Output and update for referenced model: 'LCETCS_vDctRisingEdge' */
void LCETCS_vDctRisingEdge(boolean_T rtu_Flag, boolean_T *rty_RisingEdge,
  DW_LCETCS_vDctRisingEdge_f_T *localDW)
{
  /* Chart: '<Root>/Chart' incorporates:
   *  UnitDelay: '<Root>/Unit Delay1'
   */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:11' */
  /* comment */
  if ((rtu_Flag == 1) && (localDW->UnitDelay1_DSTATE == 0)) {
    /* Transition: '<S1>:10' */
    /* Transition: '<S1>:9' */
    *rty_RisingEdge = true;

    /* Transition: '<S1>:8' */
  } else {
    /* Transition: '<S1>:7' */
    *rty_RisingEdge = false;
  }

  /* End of Chart: '<Root>/Chart' */

  /* Update for UnitDelay: '<Root>/Unit Delay1' */
  /* Transition: '<S1>:6' */
  localDW->UnitDelay1_DSTATE = rtu_Flag;
}

/* Model initialize function */
void LCETCS_vDctRisingEdge_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
