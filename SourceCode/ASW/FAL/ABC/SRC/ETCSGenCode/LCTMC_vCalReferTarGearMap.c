/*
 * File: LCTMC_vCalReferTarGearMap.c
 *
 * Code generated for Simulink model 'LCTMC_vCalReferTarGearMap'.
 *
 * Model version                  : 1.287
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:12:40 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCTMC_vCalReferTarGearMap.h"
#include "LCTMC_vCalReferTarGearMap_private.h"

/* Initial conditions for referenced model: 'LCTMC_vCalReferTarGearMap' */
void LCTMC_vCalReferTarGearMap_Init(DW_LCTMC_vCalReferTarGearMap_f_T *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCTMC_vCalInTrnRefTarGearMap' */
  LCTMC_vCalInTrnRefTarGearMap_Init
    (&(localDW->LCTMC_vCalInTrnRefTarGearMap_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCTMC_vCalRefTarGearMapbyGear' */
  LCTMC_vCalRefTarGearMapbyGear_Init
    (&(localDW->LCTMC_vCalRefTarGearMapbyGear_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCTMC_vCalRefLimVehSpdMap' */
  LCTMC_vCalRefLimVehSpdMap_Init
    (&(localDW->LCTMC_vCalRefLimVehSpdMap_DWORK1.rtdw));
}

/* Start for referenced model: 'LCTMC_vCalReferTarGearMap' */
void LCTMC_vCalReferTarGearMap_Start(DW_LCTMC_vCalReferTarGearMap_f_T *localDW)
{
  /* Start for ModelReference: '<Root>/LCTMC_vCalInTrnRefTarGearMap' */
  LCTMC_vCalInTrnRefTarGearMap_Start
    (&(localDW->LCTMC_vCalInTrnRefTarGearMap_DWORK1.rtdw));
}

/* Output and update for referenced model: 'LCTMC_vCalReferTarGearMap' */
void LCTMC_vCalReferTarGearMap(const TypeETCSGearStruct *rtu_ETCS_GEAR_STRUCT,
  const TypeETCSGainStruct *rtu_ETCS_CTL_GAINS, const TypeETCSSplitHillStruct
  *rtu_ETCS_SPLIT_HILL, const TypeTMCReqGearStruct *rtu_TMC_REQ_GEAR_STRUCT_OLD,
  boolean_T rtu_lctmcu1NonCndTarReqGear, TypeTMCRefLimVehSpdMap
  *rty_TMC_REF_LIM_VS_MAP, TypeTMCRefFinTarGearMap *rty_TMC_REF_FIN_TAR_GEAR_MAP,
  DW_LCTMC_vCalReferTarGearMap_f_T *localDW)
{
  /* local block i/o variables */
  TypeTMCRefTarGearMap rtb_TMC_REF_TAR_GEAR_MAP;
  int16_T rtb_lctmcs16TarShtChnVSInTrn;
  int16_T rtb_lctmcs16TarShtChnRPMInTrn;
  int16_T rtb_lctmcs16CordTarUpShtVS;
  int16_T rtb_lctmcs16CordTarUpShtRPM;
  int16_T rtb_lctmcs16CordTarDnShtVS;
  int16_T rtb_lctmcs16CordTarDnShtRPM;
  int16_T rtb_lctmcs16CordTarUpShtMaxVSbyGear;
  int16_T rtb_lctmcs16CordTarDnShtMinVSbyGear;
  uint8_T rtb_lctmcu8TarShtChnVSInHill;

  /* ModelReference: '<Root>/LCTMC_vCalInHillRefTarGearMap' */
  LCTMC_vCalInHillRefTarGearMap(rtu_ETCS_SPLIT_HILL,
    &rtb_lctmcu8TarShtChnVSInHill);

  /* ModelReference: '<Root>/LCTMC_vCalInTrnRefTarGearMap' */
  LCTMC_vCalInTrnRefTarGearMap(rtu_ETCS_CTL_GAINS, &rtb_lctmcs16TarShtChnVSInTrn,
    &rtb_lctmcs16TarShtChnRPMInTrn,
    &(localDW->LCTMC_vCalInTrnRefTarGearMap_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCTMC_vCalRefTarGearMapbyGear' */
  LCTMC_vCalRefTarGearMapbyGear(rtu_ETCS_GEAR_STRUCT, &rtb_TMC_REF_TAR_GEAR_MAP,
    &(localDW->LCTMC_vCalRefTarGearMapbyGear_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCTMC_vCalRefFinalTarGearMap' */
  LCTMC_vCalRefFinalTarGearMap(&rtb_TMC_REF_TAR_GEAR_MAP,
    rtb_lctmcs16TarShtChnVSInTrn, rtb_lctmcs16TarShtChnRPMInTrn,
    rtb_lctmcu8TarShtChnVSInHill, &rtb_lctmcs16CordTarUpShtVS,
    &rtb_lctmcs16CordTarUpShtRPM, &rtb_lctmcs16CordTarDnShtVS,
    &rtb_lctmcs16CordTarDnShtRPM);

  /* ModelReference: '<Root>/LCTMC_vCalRefLimVehSpdMap' */
  LCTMC_vCalRefLimVehSpdMap(rtu_ETCS_GEAR_STRUCT,
    &rtb_lctmcs16CordTarUpShtMaxVSbyGear, &rtb_lctmcs16CordTarDnShtMinVSbyGear,
    &(localDW->LCTMC_vCalRefLimVehSpdMap_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCTMC_vCalRefHysTarGearMap' */
  LCTMC_vCalRefHysTarGearMap(rtu_lctmcu1NonCndTarReqGear,
    rtu_TMC_REQ_GEAR_STRUCT_OLD, rtu_ETCS_GEAR_STRUCT,
    rtb_lctmcs16CordTarUpShtMaxVSbyGear, rtb_lctmcs16CordTarDnShtMinVSbyGear,
    rtb_lctmcs16CordTarUpShtVS, rtb_lctmcs16CordTarUpShtRPM,
    rtb_lctmcs16CordTarDnShtVS, rtb_lctmcs16CordTarDnShtRPM,
    rty_TMC_REF_LIM_VS_MAP, rty_TMC_REF_FIN_TAR_GEAR_MAP);
}

/* Model initialize function */
void LCTMC_vCalReferTarGearMap_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCTMC_vCalInHillRefTarGearMap' */
  LCTMC_vCalInHillRefTarGearMap_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCTMC_vCalInTrnRefTarGearMap' */
  LCTMC_vCalInTrnRefTarGearMap_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCTMC_vCalRefFinalTarGearMap' */
  LCTMC_vCalRefFinalTarGearMap_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCTMC_vCalRefHysTarGearMap' */
  LCTMC_vCalRefHysTarGearMap_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCTMC_vCalRefLimVehSpdMap' */
  LCTMC_vCalRefLimVehSpdMap_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCTMC_vCalRefTarGearMapbyGear' */
  LCTMC_vCalRefTarGearMapbyGear_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
