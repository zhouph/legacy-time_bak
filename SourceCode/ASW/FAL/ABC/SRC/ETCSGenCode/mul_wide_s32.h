/*
 * File: mul_wide_s32.h
 *
 * Code generated for Simulink model 'LCETCS_vCalIValAtErrZroCrs'.
 *
 * Model version                  : 1.185
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:54:01 2015
 */

#ifndef SHARE_mul_wide_s32
#define SHARE_mul_wide_s32
#include "rtwtypes.h"

extern void mul_wide_s32(int32_T in0, int32_T in1, uint32_T *ptrOutBitsHi,
  uint32_T *ptrOutBitsLo);

#endif

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
