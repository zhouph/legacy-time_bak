/*
 * File: LCETCS_vCntCtlEntrTmInLmtCnrg.h
 *
 * Code generated for Simulink model 'LCETCS_vCntCtlEntrTmInLmtCnrg'.
 *
 * Model version                  : 1.177
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:55:02 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCntCtlEntrTmInLmtCnrg_h_
#define RTW_HEADER_LCETCS_vCntCtlEntrTmInLmtCnrg_h_
#ifndef LCETCS_vCntCtlEntrTmInLmtCnrg_COMMON_INCLUDES_
# define LCETCS_vCntCtlEntrTmInLmtCnrg_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCntCtlEntrTmInLmtCnrg_COMMON_INCLUDES_ */

#include "LCETCS_vCntCtlEntrTmInLmtCnrg_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Block states (auto storage) for model 'LCETCS_vCntCtlEntrTmInLmtCnrg' */
typedef struct {
  int16_T UnitDelay_DSTATE;            /* '<Root>/Unit Delay' */
} DW_LCETCS_vCntCtlEntrTmInLmtCnrg_f_T;

typedef struct {
  DW_LCETCS_vCntCtlEntrTmInLmtCnrg_f_T rtdw;
} MdlrefDW_LCETCS_vCntCtlEntrTmInLmtCnrg_T;

/* Model reference registration function */
extern void LCETCS_vCntCtlEntrTmInLmtCnrg_initialize(void);
extern void LCETCS_vCntCtlEntrTmInLmtCnrg_Init
  (DW_LCETCS_vCntCtlEntrTmInLmtCnrg_f_T *localDW);
extern void LCETCS_vCntCtlEntrTmInLmtCnrg(const TypeETCSWhlSpinStruct
  *rtu_ETCS_WHL_SPIN, const TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN, const
  TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC, const TypeETCSEscSigStruct
  *rtu_ETCS_ESC_SIG, const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT_OLD, int16_T
  *rty_lcetcss16LmtCnrgCtlEntrTm, DW_LCETCS_vCntCtlEntrTmInLmtCnrg_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCntCtlEntrTmInLmtCnrg'
 * '<S1>'   : 'LCETCS_vCntCtlEntrTmInLmtCnrg/Chart1'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCntCtlEntrTmInLmtCnrg_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
