/*
 * File: LCETCS_vCalFunLampCnt4Eng.h
 *
 * Code generated for Simulink model 'LCETCS_vCalFunLampCnt4Eng'.
 *
 * Model version                  : 1.229
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:52:23 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalFunLampCnt4Eng_h_
#define RTW_HEADER_LCETCS_vCalFunLampCnt4Eng_h_
#ifndef LCETCS_vCalFunLampCnt4Eng_COMMON_INCLUDES_
# define LCETCS_vCalFunLampCnt4Eng_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalFunLampCnt4Eng_COMMON_INCLUDES_ */

#include "LCETCS_vCalFunLampCnt4Eng_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Block states (auto storage) for model 'LCETCS_vCalFunLampCnt4Eng' */
typedef struct {
  uint8_T UnitDelay1_DSTATE;           /* '<Root>/Unit Delay1' */
} DW_LCETCS_vCalFunLampCnt4Eng_f_T;

typedef struct {
  DW_LCETCS_vCalFunLampCnt4Eng_f_T rtdw;
} MdlrefDW_LCETCS_vCalFunLampCnt4Eng_T;

/* Model reference registration function */
extern void LCETCS_vCalFunLampCnt4Eng_initialize(void);
extern void LCETCS_vCalFunLampCnt4Eng_Init(uint8_T
  *rty_lcetcsu8FuncLampOffEngCnt, DW_LCETCS_vCalFunLampCnt4Eng_f_T *localDW);
extern void LCETCS_vCalFunLampCnt4Eng(const TypeETCSCtlActStruct
  *rtu_ETCS_CTL_ACT, const TypeETCSTrq4RdFricStruct *rtu_ETCS_TRQ_REP_RD_FRIC,
  const TypeETCSWhlSpinStruct *rtu_ETCS_WHL_SPIN, boolean_T
  rtu_lcetcsu1FuncLampOnOld, uint8_T *rty_lcetcsu8FuncLampOffEngCnt,
  DW_LCETCS_vCalFunLampCnt4Eng_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalFunLampCnt4Eng'
 * '<S1>'   : 'LCETCS_vCalFunLampCnt4Eng/Chart'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalFunLampCnt4Eng_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
