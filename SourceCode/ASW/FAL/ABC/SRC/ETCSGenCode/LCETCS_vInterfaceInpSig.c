/*
 * File: LCETCS_vInterfaceInpSig.c
 *
 * Code generated for Simulink model 'LCETCS_vInterfaceInpSig'.
 *
 * Model version                  : 1.340
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:58:31 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vInterfaceInpSig.h"
#include "LCETCS_vInterfaceInpSig_private.h"

/*
 * Output and update for atomic system:
 *    '<Root>/CopyAxlStruct_FA'
 *    '<Root>/CopyAxlStruct_RA'
 */
void LCETCS_vInterfaceInpSig_CopyAxlStruct_FA(const TCS_AXLE_STRUCT_SIM
  *rtu_BTCSAxlStruct, TypeETCSAxlStruct *rty_ETCSAxlStruct)
{
  /* Saturate: '<S1>/Saturation6' */
  if (rtu_BTCSAxlStruct->lctcss16EstBrkTorqOnLowMuWhl > 4000) {
    rty_ETCSAxlStruct->lcetcss16EstBrkTrqOnLowMuWhl = 4000;
  } else if (rtu_BTCSAxlStruct->lctcss16EstBrkTorqOnLowMuWhl < 0) {
    rty_ETCSAxlStruct->lcetcss16EstBrkTrqOnLowMuWhl = 0;
  } else {
    rty_ETCSAxlStruct->lcetcss16EstBrkTrqOnLowMuWhl =
      rtu_BTCSAxlStruct->lctcss16EstBrkTorqOnLowMuWhl;
  }

  /* End of Saturate: '<S1>/Saturation6' */

  /* DataTypeConversion: '<S1>/Data Type Conversion8' */
  rty_ETCSAxlStruct->lcetcsu1SymSpinDctFlg =
    (rtu_BTCSAxlStruct->lctcsu1SymSpinDctFlg != 0);

  /* DataTypeConversion: '<S1>/Data Type Conversion1' */
  rty_ETCSAxlStruct->lcetcsu1HuntingDcted =
    (rtu_BTCSAxlStruct->lctcsu1HuntingDcted != 0);
}

/*
 * Output and update for atomic system:
 *    '<Root>/CopyCircuitStruct_PC'
 *    '<Root>/CopyCircuitStruct_SC'
 */
void LCETCS_vInterfaceInpSig_CopyCircuitStruct_PC(const TCS_CIRCUIT_STRUCT_SIM
  *rtu_BTCSCirStruct, TypeETCSCirStruct *rty_ETCSCirStruct)
{
  /* DataTypeConversion: '<S3>/Data Type Conversion8' */
  rty_ETCSCirStruct->lcetcsu1EstPresLowFlag =
    (rtu_BTCSCirStruct->lctcsu1EstPresLowFlag != 0);
}

/*
 * Output and update for atomic system:
 *    '<Root>/CopyWhlStruct_FL'
 *    '<Root>/CopyWhlStruct_FR'
 *    '<Root>/CopyWhlStruct_RL'
 *    '<Root>/CopyWhlStruct_RR'
 */
void LCETCS_vInterfaceInpSig_CopyWhlStruct_FL(const W_STRUCT_SIM
  *rtu_ABSWhlStruct, const TCS_WL_STRUCT_SIM *rtu_BTCSWhlStruct, const
  RTA_WL_STRUCT_SIM *rtu_RTAWhlStruct, TypeETCSWhlStruct *rty_ETCSWhlStruct)
{
  /* Saturate: '<S5>/Saturation' */
  if (rtu_ABSWhlStruct->vrad_crt > 4000) {
    rty_ETCSWhlStruct->lcetcss16WhlSpdCrt = 4000;
  } else if (rtu_ABSWhlStruct->vrad_crt < 0) {
    rty_ETCSWhlStruct->lcetcss16WhlSpdCrt = 0;
  } else {
    rty_ETCSWhlStruct->lcetcss16WhlSpdCrt = rtu_ABSWhlStruct->vrad_crt;
  }

  /* End of Saturate: '<S5>/Saturation' */

  /* Saturate: '<S5>/Saturation3' */
  if (rtu_ABSWhlStruct->vfilt_rough > 4000) {
    rty_ETCSWhlStruct->lcetcss16WhlDelSpnRughRd = 4000;
  } else if (rtu_ABSWhlStruct->vfilt_rough < 0) {
    rty_ETCSWhlStruct->lcetcss16WhlDelSpnRughRd = 0;
  } else {
    rty_ETCSWhlStruct->lcetcss16WhlDelSpnRughRd = rtu_ABSWhlStruct->vfilt_rough;
  }

  /* End of Saturate: '<S5>/Saturation3' */

  /* Saturate: '<S5>/Saturation2' */
  if (rtu_BTCSWhlStruct->ltcss16MovingAvgWhlSpdTCMF > 4000) {
    rty_ETCSWhlStruct->lcetcss16MovigAvgWhlSpd = 4000;
  } else if (rtu_BTCSWhlStruct->ltcss16MovingAvgWhlSpdTCMF < 0) {
    rty_ETCSWhlStruct->lcetcss16MovigAvgWhlSpd = 0;
  } else {
    rty_ETCSWhlStruct->lcetcss16MovigAvgWhlSpd =
      rtu_BTCSWhlStruct->ltcss16MovingAvgWhlSpdTCMF;
  }

  /* End of Saturate: '<S5>/Saturation2' */

  /* DataTypeConversion: '<S5>/Data Type Conversion2' */
  rty_ETCSWhlStruct->lcetcsu1BTCSWhlAtv = (rtu_BTCSWhlStruct->lctcsu1BTCSWhlAtv
    != 0);

  /* DataTypeConversion: '<S5>/Data Type Conversion1' */
  rty_ETCSWhlStruct->lcetcsu1WhlVibDct = (rtu_BTCSWhlStruct->lctcsu1VIBFlag_tcmf
    != 0);

  /* Saturate: '<S5>/Saturation1' */
  if (rtu_BTCSWhlStruct->lctcss16WhlSpinNF > 1200) {
    rty_ETCSWhlStruct->lcetcss16WhlSpinNF = 1200;
  } else if (rtu_BTCSWhlStruct->lctcss16WhlSpinNF < 0) {
    rty_ETCSWhlStruct->lcetcss16WhlSpinNF = 0;
  } else {
    rty_ETCSWhlStruct->lcetcss16WhlSpinNF = rtu_BTCSWhlStruct->lctcss16WhlSpinNF;
  }

  /* End of Saturate: '<S5>/Saturation1' */

  /* DataTypeConversion: '<S5>/Data Type Conversion3' */
  rty_ETCSWhlStruct->lcetcsu1AbnrmTireSuspect =
    (rtu_RTAWhlStruct->RTA_SUSPECT_WL != 0);

  /* DataTypeConversion: '<S5>/Data Type Conversion5' */
  rty_ETCSWhlStruct->lcetcsu1AbnrmTireBrkSuspect =
    (rtu_RTAWhlStruct->RTA_SUSPECT_WL_FOR_BTCS != 0);
}

/* Output and update for referenced model: 'LCETCS_vInterfaceInpSig' */
void LCETCS_vInterfaceInpSig(const W_STRUCT_SIM *rtu_FL, const W_STRUCT_SIM
  *rtu_FR, const W_STRUCT_SIM *rtu_RL, const W_STRUCT_SIM *rtu_RR, const
  TCS_WL_STRUCT_SIM *rtu_FL_TCS, const TCS_WL_STRUCT_SIM *rtu_FR_TCS, const
  TCS_WL_STRUCT_SIM *rtu_RL_TCS, const TCS_WL_STRUCT_SIM *rtu_RR_TCS, const
  TCS_AXLE_STRUCT_SIM *rtu_FA_TCS, const TCS_AXLE_STRUCT_SIM *rtu_RA_TCS,
  int16_T rtu_lctcss16VehSpd4TCS, int8_T rtu_gear_pos, uint8_T
  rtu_lctcsu1GearShiftFlag, int16_T rtu_gs_sel, int16_T rtu_drive_torq, int16_T
  rtu_eng_torq, uint16_T rtu_eng_rpm, int16_T rtu_mtp, const TCS_FLAG_t
  *rtu_lcTcsCtrlFlg0, const U8_BIT_STRUCT_t *rtu_VDCF7, const TCS_FLAG_t
  *rtu_lcTcsCtrlFlg1, const U8_BIT_STRUCT_t *rtu_VF15, boolean_T
  rtu_lctcsu1DctBTCHillByAx, int16_T rtu_wstr, int16_T rtu_wstr_dot, int16_T
  rtu_yaw_out, int16_T rtu_alat, int16_T rtu_ax_Filter, int16_T
  rtu_delta_yaw_first, int16_T rtu_rf, int16_T rtu_nor_alat, int16_T
  rtu_det_alatc, int16_T rtu_det_alatm, const U8_BIT_STRUCT_t *rtu_VDCF0,
  int16_T rtu_det_wstr_dot, int16_T rtu_ltcss16BaseTarWhlSpinDiff, uint8_T
  rtu_lespu1AWD_LOW_ACT, uint16_T rtu_lespu16AWD_4WD_TQC_CUR, const
  TCS_CIRCUIT_STRUCT_SIM *rtu_PC_TCS, const TCS_CIRCUIT_STRUCT_SIM *rtu_SC_TCS,
  const U8_BIT_STRUCT_t *rtu_VDCF1, const RTA_WL_STRUCT_SIM *rtu_RTA_FL, const
  RTA_WL_STRUCT_SIM *rtu_RTA_FR, const RTA_WL_STRUCT_SIM *rtu_RTA_RL, const
  RTA_WL_STRUCT_SIM *rtu_RTA_RR, int16_T rtu_rta_ratio_temp, const
  U8_BIT_STRUCT_t *rtu_HSAF0, uint8_T rtu_lcu8SccMode, const Abc_Ctrl_HdrBusType
  *rtu_Abc_CtrlBus, uint8_T rtu_lctcsu1BTCSVehAtv, TypeETCSWhlStruct
  *rty_ETCS_FL, TypeETCSWhlStruct *rty_ETCS_FR, TypeETCSWhlStruct *rty_ETCS_RL,
  TypeETCSWhlStruct *rty_ETCS_RR, int16_T *rty_lcetcss16VehSpd,
  TypeETCSGearStruct *rty_ETCS_GEAR_STRUCT, TypeETCSEngStruct
  *rty_ETCS_ENG_STRUCT, TypeETCSExtDctStruct *rty_ETCS_EXT_DCT,
  TypeETCSAxlStruct *rty_ETCS_FA, TypeETCSAxlStruct *rty_ETCS_RA,
  TypeETCSEscSigStruct *rty_ETCS_ESC_SIG, TypeETCS4WDSigStruct *rty_ETCS_4WD_SIG,
  TypeETCSCirStruct *rty_ETCS_PC, TypeETCSCirStruct *rty_ETCS_SC,
  TypeETCSBrkSigStruct *rty_ETCS_BRK_SIG)
{
  /* Outputs for Atomic SubSystem: '<Root>/CopyWhlStruct_FL' */
  LCETCS_vInterfaceInpSig_CopyWhlStruct_FL(rtu_FL, rtu_FL_TCS, rtu_RTA_FL,
    rty_ETCS_FL);

  /* End of Outputs for SubSystem: '<Root>/CopyWhlStruct_FL' */

  /* Outputs for Atomic SubSystem: '<Root>/CopyWhlStruct_FR' */
  LCETCS_vInterfaceInpSig_CopyWhlStruct_FL(rtu_FR, rtu_FR_TCS, rtu_RTA_FR,
    rty_ETCS_FR);

  /* End of Outputs for SubSystem: '<Root>/CopyWhlStruct_FR' */

  /* Outputs for Atomic SubSystem: '<Root>/CopyWhlStruct_RL' */
  LCETCS_vInterfaceInpSig_CopyWhlStruct_FL(rtu_RL, rtu_RL_TCS, rtu_RTA_RL,
    rty_ETCS_RL);

  /* End of Outputs for SubSystem: '<Root>/CopyWhlStruct_RL' */

  /* Outputs for Atomic SubSystem: '<Root>/CopyWhlStruct_RR' */
  LCETCS_vInterfaceInpSig_CopyWhlStruct_FL(rtu_RR, rtu_RR_TCS, rtu_RTA_RR,
    rty_ETCS_RR);

  /* End of Outputs for SubSystem: '<Root>/CopyWhlStruct_RR' */

  /* Saturate: '<Root>/Saturation' */
  *rty_lcetcss16VehSpd = rtu_lctcss16VehSpd4TCS;

  /* BusCreator: '<Root>/Bus Creator2' incorporates:
   *  DataTypeConversion: '<Root>/Data Type Conversion12'
   *  DataTypeConversion: '<Root>/Data Type Conversion2'
   *  Saturate: '<Root>/Saturation1'
   *  Saturate: '<Root>/Saturation20'
   */
  rty_ETCS_GEAR_STRUCT->lcetcss16CurntGearStep = rtu_gear_pos;
  rty_ETCS_GEAR_STRUCT->lcetcsu1GearShifting = (rtu_lctcsu1GearShiftFlag != 0);
  rty_ETCS_GEAR_STRUCT->lcetcsu8DrvGearSel = (uint8_T)rtu_gs_sel;

  /* Saturate: '<Root>/Saturation2' */
  rty_ETCS_ENG_STRUCT->lcetcss16DrvReqTrq = rtu_drive_torq;

  /* Saturate: '<Root>/Saturation3' */
  rty_ETCS_ENG_STRUCT->lcetcss16ActEngTrq = rtu_eng_torq;

  /* Saturate: '<Root>/Saturation5' */
  rty_ETCS_ENG_STRUCT->lctcss16AccPdlPos = rtu_mtp;

  /* BusCreator: '<Root>/Bus Creator1' incorporates:
   *  Saturate: '<Root>/Saturation4'
   */
  rty_ETCS_ENG_STRUCT->lcetcss16EngSpd = (int16_T)rtu_eng_rpm;

  /* DataTypeConversion: '<Root>/Data Type Conversion5' */
  rty_ETCS_EXT_DCT->lcetcsu1BankRd = (rtu_VDCF7->bit7 != 0);

  /* DataTypeConversion: '<Root>/Data Type Conversion6' */
  rty_ETCS_EXT_DCT->lcetcsu1AbnrmSizTire = (rtu_lcTcsCtrlFlg1->TCS_BIT_07 != 0);

  /* DataTypeConversion: '<Root>/Data Type Conversion8' */
  rty_ETCS_EXT_DCT->lcetcsu1AutoTrans = (rtu_VF15->bit7 != 0);

  /* DataTypeConversion: '<Root>/Data Type Conversion15' */
  rty_ETCS_EXT_DCT->lcetcsu1HsaAtv = (rtu_HSAF0->bit7 != 0);

  /* DataTypeConversion: '<Root>/Data Type Conversion16' */
  rty_ETCS_EXT_DCT->lcetcsu1EscDisabledBySW =
    (rtu_Abc_CtrlBus->Abc_CtrlEscSwtStInfo.EscDisabledBySwt != 0);

  /* BusCreator: '<Root>/Bus Creator3' incorporates:
   *  DataTypeConversion: '<Root>/Data Type Conversion13'
   *  DataTypeConversion: '<Root>/Data Type Conversion14'
   *  DataTypeConversion: '<Root>/Data Type Conversion18'
   *  DataTypeConversion: '<Root>/Data Type Conversion4'
   */
  rty_ETCS_EXT_DCT->lcetcsu1RoughRd = (rtu_lcTcsCtrlFlg0->TCS_BIT_00 != 0);
  rty_ETCS_EXT_DCT->lcetcsu1DctHilByAx = rtu_lctcsu1DctBTCHillByAx;
  rty_ETCS_EXT_DCT->lcetcsu8AbnrmTireRatio = (uint8_T)rtu_rta_ratio_temp;
  rty_ETCS_EXT_DCT->lcetcsu8SccCtlAct = rtu_lcu8SccMode;
  rty_ETCS_EXT_DCT->lcetcsu1TcsDisabledBySW =
    (rtu_Abc_CtrlBus->Abc_CtrlEscSwtStInfo.TcsDisabledBySwt != 0);
  rty_ETCS_EXT_DCT->lcetcsu1BTCSCtlAct = (rtu_lctcsu1BTCSVehAtv != 0);

  /* Outputs for Atomic SubSystem: '<Root>/CopyAxlStruct_FA' */
  LCETCS_vInterfaceInpSig_CopyAxlStruct_FA(rtu_FA_TCS, rty_ETCS_FA);

  /* End of Outputs for SubSystem: '<Root>/CopyAxlStruct_FA' */

  /* Outputs for Atomic SubSystem: '<Root>/CopyAxlStruct_RA' */
  LCETCS_vInterfaceInpSig_CopyAxlStruct_FA(rtu_RA_TCS, rty_ETCS_RA);

  /* End of Outputs for SubSystem: '<Root>/CopyAxlStruct_RA' */

  /* Product: '<Root>/Divide4' incorporates:
   *  Saturate: '<Root>/Saturation6'
   */
  rty_ETCS_ESC_SIG->lcetcss16StrWhAng = (int16_T)(rtu_wstr / 10);

  /* Product: '<Root>/Divide5' incorporates:
   *  Saturate: '<Root>/Saturation7'
   */
  rty_ETCS_ESC_SIG->lcetcss16StrWhAngGrd = (int16_T)(rtu_wstr_dot / 10);

  /* Saturate: '<Root>/Saturation8' */
  rty_ETCS_ESC_SIG->lcetcss16YawRateMsrd = rtu_yaw_out;

  /* Product: '<Root>/Divide3' incorporates:
   *  Saturate: '<Root>/Saturation9'
   */
  rty_ETCS_ESC_SIG->lcetcss16LatAccMsrd = (int16_T)(rtu_alat / 10);

  /* Saturate: '<Root>/Saturation10' */
  rty_ETCS_ESC_SIG->lcetcss16LonAcc = rtu_ax_Filter;

  /* Saturate: '<Root>/Saturation11' */
  rty_ETCS_ESC_SIG->lcetcss16DelYawFrst = rtu_delta_yaw_first;

  /* Abs: '<Root>/Abs' */
  if (rtu_delta_yaw_first < 0) {
    /* Saturate: '<Root>/Saturation21' */
    rty_ETCS_ESC_SIG->lcetcss16AbsDelYawFrst = (int16_T)(-rtu_delta_yaw_first);
  } else {
    /* Saturate: '<Root>/Saturation21' */
    rty_ETCS_ESC_SIG->lcetcss16AbsDelYawFrst = rtu_delta_yaw_first;
  }

  /* End of Abs: '<Root>/Abs' */

  /* Saturate: '<Root>/Saturation12' */
  rty_ETCS_ESC_SIG->lcetcss16YawRateCald = rtu_rf;

  /* Product: '<Root>/Divide1' incorporates:
   *  Saturate: '<Root>/Saturation14'
   */
  rty_ETCS_ESC_SIG->lcetcss16LatAccCaldMani = (int16_T)(rtu_det_alatc / 10);

  /* Saturate: '<Root>/Saturation17' */
  rty_ETCS_ESC_SIG->lcetcss16AbsStrGrdMani = rtu_det_wstr_dot;

  /* BusCreator: '<Root>/Bus Creator4' incorporates:
   *  DataTypeConversion: '<Root>/Data Type Conversion10'
   *  DataTypeConversion: '<Root>/Data Type Conversion3'
   *  Product: '<Root>/Divide'
   *  Product: '<Root>/Divide2'
   *  Saturate: '<Root>/Saturation13'
   *  Saturate: '<Root>/Saturation15'
   */
  rty_ETCS_ESC_SIG->lcetcss16LatAccCald = (int16_T)(rtu_nor_alat / 10);
  rty_ETCS_ESC_SIG->lcetcss16LatAccMsrdMani = (int16_T)(rtu_det_alatm / 10);
  rty_ETCS_ESC_SIG->lcetcsu1ESCBrkCtl = (rtu_VDCF0->bit2 != 0);
  rty_ETCS_ESC_SIG->lcetcsu1BrkPdlPrsed = (rtu_VDCF1->bit3 != 0);

  /* Product: '<Root>/Product' incorporates:
   *  Saturate: '<Root>/Saturation19'
   */
  rty_ETCS_4WD_SIG->lcetcss32SecAxlTrqMax = rtu_lespu16AWD_4WD_TQC_CUR * 10;

  /* BusCreator: '<Root>/Bus Creator5' incorporates:
   *  DataTypeConversion: '<Root>/Data Type Conversion11'
   *  Saturate: '<Root>/Saturation18'
   */
  rty_ETCS_4WD_SIG->lcetcsu14wdLwMd = (rtu_lespu1AWD_LOW_ACT != 0);

  /* Outputs for Atomic SubSystem: '<Root>/CopyCircuitStruct_PC' */
  LCETCS_vInterfaceInpSig_CopyCircuitStruct_PC(rtu_PC_TCS, rty_ETCS_PC);

  /* End of Outputs for SubSystem: '<Root>/CopyCircuitStruct_PC' */

  /* Outputs for Atomic SubSystem: '<Root>/CopyCircuitStruct_SC' */
  LCETCS_vInterfaceInpSig_CopyCircuitStruct_PC(rtu_SC_TCS, rty_ETCS_SC);

  /* End of Outputs for SubSystem: '<Root>/CopyCircuitStruct_SC' */

  /* Saturate: '<Root>/Saturation16' */
  rty_ETCS_BRK_SIG->lcetcss16BtcsTarWhlSpnDiff = rtu_ltcss16BaseTarWhlSpinDiff;
}

/* Model initialize function */
void LCETCS_vInterfaceInpSig_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
