/*
 * File: LCETCS_vCalSplitHillCnt.c
 *
 * Code generated for Simulink model 'LCETCS_vCalSplitHillCnt'.
 *
 * Model version                  : 1.486
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:57:46 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalSplitHillCnt.h"
#include "LCETCS_vCalSplitHillCnt_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalSplitHillCnt' */
void LCETCS_vCalSplitHillCnt_Init(DW_LCETCS_vCalSplitHillCnt_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = 0;
}

/* Output and update for referenced model: 'LCETCS_vCalSplitHillCnt' */
void LCETCS_vCalSplitHillCnt(boolean_T rtu_lcetcsu1HillDctCompbyAx, boolean_T
  rtu_lcetcsu1HillbyHsaSustain, boolean_T rtu_lcetcsu1SplitHillDctCndSet,
  boolean_T rtu_lcetcsu1SplitHillClrDecCndSet, boolean_T
  rtu_lcetcsu1SplitHillClrCndSet, int16_T *rty_lcetcss16SplitHillDctCnt,
  DW_LCETCS_vCalSplitHillCnt_f_T *localDW)
{
  int32_T tmp;

  /* Chart: '<Root>/Chart' incorporates:
   *  UnitDelay: '<Root>/Unit Delay'
   */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:146' */
  /* VehSpd or EngSpd or Ax by VehSpd > TP */
  if (rtu_lcetcsu1SplitHillClrCndSet == 1) {
    /* Transition: '<S1>:148' */
    /* Transition: '<S1>:150' */
    tmp = localDW->UnitDelay_DSTATE - 1;
    if (tmp < -32768) {
      tmp = -32768;
    }

    localDW->UnitDelay_DSTATE = (int16_T)tmp;

    /* Transition: '<S1>:151' */
    /* Transition: '<S1>:124' */
    /* Transition: '<S1>:125' */
    /* Transition: '<S1>:127' */
    /* Transition: '<S1>:128' */
  } else {
    /* Transition: '<S1>:153' */
    /* Status : HSA Control=1 & Filtered Ax > TP  */
    if (rtu_lcetcsu1HillbyHsaSustain == 1) {
      /* Transition: '<S1>:93' */
      /* Transition: '<S1>:95' */
      localDW->UnitDelay_DSTATE = ((uint8_T)U8ETCSCpHsaHillTime);

      /* Transition: '<S1>:124' */
      /* Transition: '<S1>:125' */
      /* Transition: '<S1>:127' */
      /* Transition: '<S1>:128' */
    } else {
      /* Transition: '<S1>:98' */
      /* VehSpd & EngSpd & Ax by VehSpd < TP
         Split Brake Control=1 & Max Brake Press & Min Filtered Ax > TP */
      if (rtu_lcetcsu1SplitHillDctCndSet == 1) {
        /* Transition: '<S1>:100' */
        /* Transition: '<S1>:102' */
        /* Filtered Ax > Tp & Steady State Filtered  Ax Value */
        if (rtu_lcetcsu1HillDctCompbyAx == 1) {
          /* Transition: '<S1>:104' */
          /* Transition: '<S1>:106' */
          tmp = localDW->UnitDelay_DSTATE + 2;
          if (tmp > 32767) {
            tmp = 32767;
          }

          localDW->UnitDelay_DSTATE = (int16_T)tmp;

          /* Transition: '<S1>:125' */
          /* Transition: '<S1>:127' */
          /* Transition: '<S1>:128' */
        } else {
          /* Transition: '<S1>:108' */
          tmp = localDW->UnitDelay_DSTATE + 1;
          if (tmp > 32767) {
            tmp = 32767;
          }

          localDW->UnitDelay_DSTATE = (int16_T)tmp;

          /* Transition: '<S1>:127' */
          /* Transition: '<S1>:128' */
        }
      } else {
        /* Transition: '<S1>:152' */
        /* Split Brake Control=0 or Max Brake Press < TP */
        if (rtu_lcetcsu1SplitHillClrDecCndSet == 1) {
          /* Transition: '<S1>:118' */
          /* Transition: '<S1>:121' */
          tmp = localDW->UnitDelay_DSTATE - 1;
          if (tmp < -32768) {
            tmp = -32768;
          }

          localDW->UnitDelay_DSTATE = (int16_T)tmp;

          /* Transition: '<S1>:128' */
        } else {
          /* Transition: '<S1>:123' */
        }
      }
    }
  }

  /* End of Chart: '<Root>/Chart' */

  /* Saturate: '<Root>/Saturation' */
  /* Transition: '<S1>:154' */
  if (localDW->UnitDelay_DSTATE > ((uint8_T)U8ETCSCpHsaHillTime)) {
    *rty_lcetcss16SplitHillDctCnt = ((uint8_T)U8ETCSCpHsaHillTime);
  } else if (localDW->UnitDelay_DSTATE < 0) {
    *rty_lcetcss16SplitHillDctCnt = 0;
  } else {
    *rty_lcetcss16SplitHillDctCnt = localDW->UnitDelay_DSTATE;
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Update for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = *rty_lcetcss16SplitHillDctCnt;
}

/* Model initialize function */
void LCETCS_vCalSplitHillCnt_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
