/*
 * File: LCETCS_vCalInhAftLmtCrng.c
 *
 * Code generated for Simulink model 'LCETCS_vCalInhAftLmtCrng'.
 *
 * Model version                  : 1.98
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:09:22 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalInhAftLmtCrng.h"
#include "LCETCS_vCalInhAftLmtCrng_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalInhAftLmtCrng' */
void LCETCS_vCalInhAftLmtCrng_Init(boolean_T *rty_lcetcsu1InhAftLmtCrng,
  DW_LCETCS_vCalInhAftLmtCrng_f_T *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalScaleDnAyMani' */
  LCETCS_vCalScaleDnAyMani_Init(&(localDW->LCETCS_vCalScaleDnAyMani_DWORK1.rtb));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vConvAx2CrdnTrq' */
  LCETCS_vConvAx2CrdnTrq_Init(&(localDW->LCETCS_vConvAx2CrdnTrq_DWORK1.rtb));

  /* InitializeConditions for Chart: '<Root>/Chart' */
  *rty_lcetcsu1InhAftLmtCrng = false;
}

/* Start for referenced model: 'LCETCS_vCalInhAftLmtCrng' */
void LCETCS_vCalInhAftLmtCrng_Start(DW_LCETCS_vCalInhAftLmtCrng_f_T *localDW)
{
  /* Start for ModelReference: '<Root>/LCETCS_vCalScaleDnAyMani' */
  LCETCS_vCalScaleDnAyMani_Start(&(localDW->LCETCS_vCalScaleDnAyMani_DWORK1.rtb));

  /* Start for ModelReference: '<Root>/LCETCS_vConvAx2CrdnTrq' */
  LCETCS_vConvAx2CrdnTrq_Start(&(localDW->LCETCS_vConvAx2CrdnTrq_DWORK1.rtb));
}

/* Output and update for referenced model: 'LCETCS_vCalInhAftLmtCrng' */
void LCETCS_vCalInhAftLmtCrng(const TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN,
  const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const TypeETCSCtlCmdStruct
  *rtu_ETCS_CTL_CMD, const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, int16_T
  rtu_lcetcss16AftLmtCrngCnt, int16_T rtu_lcetcss16InhCntAftLmtCrngSpn,
  boolean_T *rty_lcetcsu1InhAftLmtCrng, int16_T *rty_lcetcss16ScaleDnAyMani,
  DW_LCETCS_vCalInhAftLmtCrng_f_T *localDW)
{
  /* local block i/o variables */
  int32_T rtb_CrdnTrq;

  /* ModelReference: '<Root>/LCETCS_vCalScaleDnAyMani' */
  LCETCS_vCalScaleDnAyMani(rtu_ETCS_ESC_SIG, rty_lcetcss16ScaleDnAyMani,
    &(localDW->LCETCS_vCalScaleDnAyMani_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vConvAx2CrdnTrq' */
  LCETCS_vConvAx2CrdnTrq((*rty_lcetcss16ScaleDnAyMani), &rtb_CrdnTrq,
    &(localDW->LCETCS_vConvAx2CrdnTrq_DWORK1.rtb));

  /* Chart: '<Root>/Chart' */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:34' */
  if (((((rtu_ETCS_CTL_ACT->lcetcsu1CtlAct == 0) ||
         (rtu_ETCS_TAR_SPIN->lcetcsu1TarSpnDec == 1)) ||
        (rtu_ETCS_CTL_CMD->lcetcss32CardanTrqCmd >= rtb_CrdnTrq)) ||
       (rtu_lcetcss16AftLmtCrngCnt >= ((int16_T)S16ETCSCpAftTrnMaxCnt))) ||
      (rtu_lcetcss16InhCntAftLmtCrngSpn >= 5)) {
    /* Transition: '<S1>:171' */
    /* 5=100ms */
    /* Transition: '<S1>:173' */
    *rty_lcetcsu1InhAftLmtCrng = true;

    /* Transition: '<S1>:176' */
  } else {
    /* Transition: '<S1>:175' */
    *rty_lcetcsu1InhAftLmtCrng = false;
  }

  /* End of Chart: '<Root>/Chart' */
  /* Transition: '<S1>:178' */
}

/* Model initialize function */
void LCETCS_vCalInhAftLmtCrng_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalScaleDnAyMani' */
  LCETCS_vCalScaleDnAyMani_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vConvAx2CrdnTrq' */
  LCETCS_vConvAx2CrdnTrq_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
