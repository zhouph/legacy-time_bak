/*
 * File: LCETCS_vCalPCtlGain.h
 *
 * Code generated for Simulink model 'LCETCS_vCalPCtlGain'.
 *
 * Model version                  : 1.179
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:08:27 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalPCtlGain_h_
#define RTW_HEADER_LCETCS_vCalPCtlGain_h_
#ifndef LCETCS_vCalPCtlGain_COMMON_INCLUDES_
# define LCETCS_vCalPCtlGain_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalPCtlGain_COMMON_INCLUDES_ */

#include "LCETCS_vCalPCtlGain_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vCordPCtlGain.h"
#include "LCETCS_vCalSpltBsPGain.h"
#include "LCETCS_vCalPGainIncFacH2L.h"
#include "LCETCS_vCalPGainFtrInTrn.h"
#include "LCETCS_vCalPGainFtrInDrvVib.h"
#include "LCETCS_vCalPGainFacInGearChng.h"
#include "LCETCS_vCalHomoBsPGain.h"
#include "LCETCS_vCalBsPGain.h"

/* Block states (auto storage) for model 'LCETCS_vCalPCtlGain' */
typedef struct {
  MdlrefDW_LCETCS_vCalHomoBsPGain_T LCETCS_vCalHomoBsPGain_DWORK1;/* '<Root>/LCETCS_vCalHomoBsPGain' */
  MdlrefDW_LCETCS_vCalSpltBsPGain_T LCETCS_vCalSpltBsPGain_DWORK1;/* '<Root>/LCETCS_vCalSpltBsPGain' */
  MdlrefDW_LCETCS_vCalPGainFacInGearChng_T LCETCS_vCalPGainFacInGearChng_DWORK1;/* '<Root>/LCETCS_vCalPGainFacInGearChng' */
  MdlrefDW_LCETCS_vCalPGainFtrInDrvVib_T LCETCS_vCalPGainFtrInDrvVib_DWORK1;/* '<Root>/LCETCS_vCalPGainFtrInDrvVib' */
  MdlrefDW_LCETCS_vCalPGainFtrInTrn_T LCETCS_vCalPGainFtrInTrn_DWORK1;/* '<Root>/LCETCS_vCalPGainFtrInTrn' */
  MdlrefDW_LCETCS_vCalPGainIncFacH2L_T LCETCS_vCalPGainIncFacH2L_DWORK1;/* '<Root>/LCETCS_vCalPGainIncFacH2L' */
} DW_LCETCS_vCalPCtlGain_f_T;

typedef struct {
  DW_LCETCS_vCalPCtlGain_f_T rtdw;
} MdlrefDW_LCETCS_vCalPCtlGain_T;

/* Model reference registration function */
extern void LCETCS_vCalPCtlGain_initialize(void);
extern void LCETCS_vCalPCtlGain_Init(int16_T *rty_lcetcss16BsPgain, int16_T
  *rty_lcetcss16PgainFacInGearChng, int16_T *rty_lcetcss16PGainFacInDrvVib,
  int16_T *rty_lcetcss16PgainFac, DW_LCETCS_vCalPCtlGain_f_T *localDW);
extern void LCETCS_vCalPCtlGain_Start(DW_LCETCS_vCalPCtlGain_f_T *localDW);
extern void LCETCS_vCalPCtlGain(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT,
  const TypeETCSDrvMdlStruct *rtu_ETCS_DRV_MDL, const TypeETCSCtlErrStruct
  *rtu_ETCS_CTL_ERR, const TypeETCSTrq4RdFricStruct *rtu_ETCS_TRQ_REP_RD_FRIC,
  const TypeETCSGainGearShftStruct *rtu_ETCS_GAIN_GEAR_SHFT, const
  TypeETCSAxlStruct *rtu_ETCS_FA, const TypeETCSAxlStruct *rtu_ETCS_RA, const
  TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN, const TypeETCSEscSigStruct
  *rtu_ETCS_ESC_SIG, const TypeETCSH2LStruct *rtu_ETCS_H2L, boolean_T
  rtu_lcetcsu1VehVibDct, int16_T *rty_lcetcss16Pgain, int16_T
  *rty_lcetcss16BsPgain, int16_T *rty_lcetcss16PgainFacInGearChng, int16_T
  *rty_lcetcss16PGainIncFacH2L, int16_T *rty_lcetcss16HomoBsPgain, int16_T
  *rty_lcetcss16SpltBsPgain, int16_T *rty_lcetcss16PGainFacInTrn, int16_T
  *rty_lcetcss16PGainFacInDrvVib, int8_T *rty_lcetcss8PgainFacInDrvVibCnt,
  int16_T *rty_lcetcss16PgainFac, DW_LCETCS_vCalPCtlGain_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalPCtlGain'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalPCtlGain_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
