/*
 * File: FctEtcsCore.h
 *
 * Code generated for Simulink model 'FctEtcsCore'.
 *
 * Model version                  : 1.483
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:20:06 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_FctEtcsCore_h_
#define RTW_HEADER_FctEtcsCore_h_
#ifndef FctEtcsCore_COMMON_INCLUDES_
# define FctEtcsCore_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* FctEtcsCore_COMMON_INCLUDES_ */

#include "FctEtcsCore_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vCallDetection.h"
#include "LCETCS_vCallControl.h"
#include "LCETCS_vCallCalculation.h"
#include "LCETCS_vCalEngTrqCmd.h"

/* Block states (auto storage) for model 'FctEtcsCore' */
typedef struct {
  TypeETCSCtlCmdStruct UnitDelay3_DSTATE;/* '<Root>/Unit Delay3' */
  TypeETCSCtlCmdStruct UnitDelay5_DSTATE;/* '<Root>/Unit Delay5' */
  TypeETCSCtlCmdStruct UnitDelay_DSTATE;/* '<Root>/Unit Delay' */
  TypeETCSSplitHillStruct UnitDelay15_DSTATE;/* '<Root>/Unit Delay15' */
  TypeETCSSplitHillStruct UnitDelay14_DSTATE;/* '<Root>/Unit Delay14' */
  TypeETCSCtlActStruct UnitDelay7_DSTATE;/* '<Root>/Unit Delay7' */
  TypeETCSCtlActStruct UnitDelay13_DSTATE;/* '<Root>/Unit Delay13' */
  TypeETCSLwToSpltStruct UnitDelay6_DSTATE;/* '<Root>/Unit Delay6' */
  TypeETCSTrq4RdFricStruct UnitDelay17_DSTATE;/* '<Root>/Unit Delay17' */
  TypeETCSEngCmdStruct UnitDelay4_DSTATE;/* '<Root>/Unit Delay4' */
  TypeETCSDepSnwStruct UnitDelay2_DSTATE;/* '<Root>/Unit Delay2' */
  TypeETCSBrkCtlCmdStruct UnitDelay9_DSTATE;/* '<Root>/Unit Delay9' */
  TypeETCSBrkCtlCmdStruct UnitDelay1_DSTATE;/* '<Root>/Unit Delay1' */
  TypeETCSBrkCtlReqStruct UnitDelay10_DSTATE;/* '<Root>/Unit Delay10' */
  TypeETCSHighDctStruct UnitDelay16_DSTATE;/* '<Root>/Unit Delay16' */
  TypeETCSEngStallStruct UnitDelay11_DSTATE;/* '<Root>/Unit Delay11' */
  TypeETCSEngStallStruct UnitDelay12_DSTATE;/* '<Root>/Unit Delay12' */
  boolean_T UnitDelay8_DSTATE;         /* '<Root>/Unit Delay8' */
  MdlrefDW_LCETCS_vCallCalculation_T LCETCS_vCallCalculation_DWORK1;/* '<Root>/LCETCS_vCallCalculation' */
  MdlrefDW_LCETCS_vCallDetection_T LCETCS_vCallDetection_DWORK1;/* '<Root>/LCETCS_vCallDetection' */
  MdlrefDW_LCETCS_vCallControl_T LCETCS_vCallControl_DWORK1;/* '<Root>/LCETCS_vCallControl' */
} DW_FctEtcsCore_f_T;

typedef struct {
  DW_FctEtcsCore_f_T rtdw;
} MdlrefDW_FctEtcsCore_T;

/* Model reference registration function */
extern void FctEtcsCore_initialize(const rtTimingBridge *timingBridge);
extern const TypeETCSCtlCmdStruct FctEtcsCore_rtZTypeETCSCtlCmdStruct;/* TypeETCSCtlCmdStruct ground */
extern const TypeETCSEngCmdStruct FctEtcsCore_rtZTypeETCSEngCmdStruct;/* TypeETCSEngCmdStruct ground */
extern const TypeETCSCtlActStruct FctEtcsCore_rtZTypeETCSCtlActStruct;/* TypeETCSCtlActStruct ground */
extern const TypeETCSDepSnwStruct FctEtcsCore_rtZTypeETCSDepSnwStruct;/* TypeETCSDepSnwStruct ground */
extern const TypeETCSBrkCtlCmdStruct FctEtcsCore_rtZTypeETCSBrkCtlCmdStruct;/* TypeETCSBrkCtlCmdStruct ground */
extern const TypeETCSBrkCtlReqStruct FctEtcsCore_rtZTypeETCSBrkCtlReqStruct;/* TypeETCSBrkCtlReqStruct ground */
extern const TypeETCSSplitHillStruct FctEtcsCore_rtZTypeETCSSplitHillStruct;/* TypeETCSSplitHillStruct ground */
extern const TypeETCSHighDctStruct FctEtcsCore_rtZTypeETCSHighDctStruct;/* TypeETCSHighDctStruct ground */
extern const TypeETCSEngStallStruct FctEtcsCore_rtZTypeETCSEngStallStruct;/* TypeETCSEngStallStruct ground */
extern const TypeETCSLwToSpltStruct FctEtcsCore_rtZTypeETCSLwToSpltStruct;/* TypeETCSLwToSpltStruct ground */
extern const TypeETCSTrq4RdFricStruct FctEtcsCore_rtZTypeETCSTrq4RdFricStruct;/* TypeETCSTrq4RdFricStruct ground */
extern void FctEtcsCore_Init(TypeETCSCtrlModeStruct *rty_ETCS_CTRL_MODE,
  TypeETCSCyl1stStruct *rty_ETCS_CYL_1ST, TypeETCSErrZroCrsStruct
  *rty_ETCS_ERR_ZRO_CRS, TypeETCSLowWhlSpnStruct *rty_ETCS_LOW_WHL_SPN,
  TypeETCSH2LStruct *rty_ETCS_H2L, TypeETCSGainGearShftStruct
  *rty_ETCS_GAIN_GEAR_SHFT, TypeTMCReqGearStruct *rty_TMC_REQ_GEAR_STRUCT,
  boolean_T *rty_lcetcsu1FuncLampOn, DW_FctEtcsCore_f_T *localDW);
extern void FctEtcsCore_Start(DW_FctEtcsCore_f_T *localDW);
extern void FctEtcsCore_Disable(DW_FctEtcsCore_f_T *localDW);
extern void FctEtcsCore(const TypeETCSWhlStruct *rtu_ETCS_FL, const
  TypeETCSWhlStruct *rtu_ETCS_FR, const TypeETCSWhlStruct *rtu_ETCS_RL, const
  TypeETCSWhlStruct *rtu_ETCS_RR, int16_T rtu_lcetcss16VehSpd, const
  TypeETCSGearStruct *rtu_ETCS_GEAR_STRUCT, const TypeETCSEngStruct
  *rtu_ETCS_ENG_STRUCT, const TypeETCSExtDctStruct *rtu_ETCS_EXT_DCT, const
  TypeETCSAxlStruct *rtu_ETCS_FA, const TypeETCSAxlStruct *rtu_ETCS_RA, const
  TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const TypeETCS4WDSigStruct
  *rtu_ETCS_4WD_SIG, const TypeETCSCirStruct *rtu_ETCS_PC, const
  TypeETCSCirStruct *rtu_ETCS_SC, const TypeETCSBrkSigStruct *rtu_ETCS_BRK_SIG,
  TypeETCSCtlErrStruct *rty_ETCS_CTL_ERR, TypeETCSTarSpinStruct
  *rty_ETCS_TAR_SPIN, TypeETCSDrvMdlStruct *rty_ETCS_DRV_MDL,
  TypeETCSCrdnTrqStruct *rty_ETCS_CRDN_TRQ, TypeETCSVehAccStruct
  *rty_ETCS_VEH_ACC, TypeETCSEngMdlStruct *rty_ETCS_ENG_MDL, TypeETCSLdTrqStruct
  *rty_ETCS_LD_TRQ, TypeETCSAxlAccStruct *rty_ETCS_AXL_ACC, int16_T
  *rty_lcetcss16SymBrkCtlStrtTh, TypeETCSWhlSpinStruct *rty_ETCS_WHL_SPIN,
  TypeETCSStrStatStruct *rty_ETCS_STR_STAT, TypeETCSCtrlModeStruct
  *rty_ETCS_CTRL_MODE, TypeETCSCtlActStruct *rty_ETCS_CTL_ACT,
  TypeETCSCyl1stStruct *rty_ETCS_CYL_1ST, TypeETCSErrZroCrsStruct
  *rty_ETCS_ERR_ZRO_CRS, TypeETCSIValRstStruct *rty_ETCS_IVAL_RST,
  TypeETCSRefTrqStrCtlStruct *rty_ETCS_REF_TRQ_STR_CTL,
  TypeETCSRefTrqStr2CylStruct *rty_ETCS_REF_TRQ_STR2CYL,
  TypeETCSRefTrq2JmpStruct *rty_ETCS_REF_TRQ2JMP, TypeETCSLowWhlSpnStruct
  *rty_ETCS_LOW_WHL_SPN, TypeETCSDepSnwStruct *rty_ETCS_DEP_SNW,
  TypeETCSH2LStruct *rty_ETCS_H2L, TypeETCSGainGearShftStruct
  *rty_ETCS_GAIN_GEAR_SHFT, TypeETCSBrkCtlReqStruct *rty_ETCS_BRK_CTL_REQ,
  boolean_T *rty_lcetcsu1VehVibDct, TypeETCSEngStallStruct *rty_ETCS_ENG_STALL,
  TypeETCSGainStruct *rty_ETCS_CTL_GAINS, TypeETCSTrq4RdFricStruct
  *rty_ETCS_TRQ_REP_RD_FRIC, TypeTMCReqGearStruct *rty_TMC_REQ_GEAR_STRUCT,
  TypeETCSBrkCtlCmdStruct *rty_ETCS_BRK_CTL_CMD, TypeETCSCtlCmdStruct
  *rty_ETCS_CTL_CMD, TypeETCSSplitHillStruct *rty_ETCS_SPLIT_HILL, boolean_T
  *rty_lcetcsu1FuncLampOn, TypeETCSWhlAccStruct *rty_ETCS_WHL_ACC,
  TypeETCSHighDctStruct *rty_ETCS_HI_DCT, TypeETCSReqVCAStruct *rty_ETCS_REQ_VCA,
  TypeETCSLwToSpltStruct *rty_ETCS_L2SPLT, TypeETCSEngCmdStruct
  *rty_ETCS_ENG_CMD, TypeETCSAftLmtCrng *rty_ETCS_AFT_TURN, DW_FctEtcsCore_f_T
  *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'FctEtcsCore'
 */
#endif                                 /* RTW_HEADER_FctEtcsCore_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
