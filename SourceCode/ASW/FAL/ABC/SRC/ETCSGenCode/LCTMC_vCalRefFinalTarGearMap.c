/*
 * File: LCTMC_vCalRefFinalTarGearMap.c
 *
 * Code generated for Simulink model 'LCTMC_vCalRefFinalTarGearMap'.
 *
 * Model version                  : 1.249
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:52:58 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCTMC_vCalRefFinalTarGearMap.h"
#include "LCTMC_vCalRefFinalTarGearMap_private.h"

/* Output and update for referenced model: 'LCTMC_vCalRefFinalTarGearMap' */
void LCTMC_vCalRefFinalTarGearMap(const TypeTMCRefTarGearMap
  *rtu_TMC_REF_TAR_GEAR_MAP, int16_T rtu_lctmcs16TarShtChnVSInTrn, int16_T
  rtu_lctmcs16TarShtChnRPMInTrn, uint8_T rtu_lctmcu8TarShtChnVSInHill, int16_T
  *rty_lctmcs16CordTarUpShtVS, int16_T *rty_lctmcs16CordTarUpShtRPM, int16_T
  *rty_lctmcs16CordTarDnShtVS, int16_T *rty_lctmcs16CordTarDnShtRPM)
{
  /* Sum: '<Root>/Add' */
  *rty_lctmcs16CordTarUpShtVS = (int16_T)(((int16_T)
    (rtu_TMC_REF_TAR_GEAR_MAP->lctmcs16TarUpShtVSbyGear +
     rtu_lctmcs16TarShtChnVSInTrn)) + rtu_lctmcu8TarShtChnVSInHill);

  /* Sum: '<Root>/Add1' */
  *rty_lctmcs16CordTarDnShtVS = (int16_T)
    (rtu_TMC_REF_TAR_GEAR_MAP->lctmcs16TarDnShtVSbyGear -
     rtu_lctmcs16TarShtChnVSInTrn);

  /* Sum: '<Root>/Add2' */
  *rty_lctmcs16CordTarUpShtRPM = (int16_T)
    (rtu_TMC_REF_TAR_GEAR_MAP->lctmcs16TarUpShtRPMbyGear +
     rtu_lctmcs16TarShtChnRPMInTrn);

  /* Sum: '<Root>/Add3' */
  *rty_lctmcs16CordTarDnShtRPM = (int16_T)
    (rtu_TMC_REF_TAR_GEAR_MAP->lctmcs16TarDnShtRPMbyGear -
     rtu_lctmcs16TarShtChnRPMInTrn);
}

/* Model initialize function */
void LCTMC_vCalRefFinalTarGearMap_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
