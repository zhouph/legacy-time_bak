/*
 * File: LCETCS_vCalStrtThSymBrkCtl.c
 *
 * Code generated for Simulink model 'LCETCS_vCalStrtThSymBrkCtl'.
 *
 * Model version                  : 1.137
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:59:17 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalStrtThSymBrkCtl.h"
#include "LCETCS_vCalStrtThSymBrkCtl_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalStrtThSymBrkCtl' */
void LCETCS_vCalStrtThSymBrkCtl_Init(B_LCETCS_vCalStrtThSymBrkCtl_c_T *localB)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter5Point' */
  LCETCS_s16Inter5Point_Init(&localB->y);

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_Init(&localB->y_d);
}

/* Start for referenced model: 'LCETCS_vCalStrtThSymBrkCtl' */
void LCETCS_vCalStrtThSymBrkCtl_Start(B_LCETCS_vCalStrtThSymBrkCtl_c_T *localB)
{
  /* Start for Constant: '<Root>/High acceleration threshold' */
  localB->x2 = ((uint8_T)U8ETCSCpHiAxlAccTh);

  /* Start for Constant: '<Root>/Low acceleration threshold' */
  localB->x1 = ((uint8_T)U8ETCSCpLwAxlAccTh);

  /* Start for Constant: '<Root>/Symmetric brake control start threshold at vehicle speed break point 1' */
  localB->y1 = ((uint8_T)U8ETCSCpSymBrkCtlTh_1);

  /* Start for Constant: '<Root>/Symmetric brake control start threshold at vehicle speed break point 2' */
  localB->y2 = ((uint8_T)U8ETCSCpSymBrkCtlTh_2);

  /* Start for Constant: '<Root>/Symmetric brake control start threshold at vehicle speed break point 3' */
  localB->y3 = ((uint8_T)U8ETCSCpSymBrkCtlTh_3);

  /* Start for Constant: '<Root>/Symmetric brake control start threshold at vehicle speed break point 4' */
  localB->y4 = ((uint8_T)U8ETCSCpSymBrkCtlTh_4);

  /* Start for Constant: '<Root>/Symmetric brake control start threshold at vehicle speed break point 5' */
  localB->y5 = ((uint8_T)U8ETCSCpSymBrkCtlTh_5);
}

/* Output and update for referenced model: 'LCETCS_vCalStrtThSymBrkCtl' */
void LCETCS_vCalStrtThSymBrkCtl(const TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN,
  const TypeETCSAxlAccStruct *rtu_ETCS_AXL_ACC, int16_T rtu_lcetcss16VehSpd,
  int16_T *rty_lcetcss16SymBrkCtlStrtTh, B_LCETCS_vCalStrtThSymBrkCtl_c_T
  *localB)
{
  /* local block i/o variables */
  int16_T rtb_y1;

  /* Constant: '<Root>/High acceleration threshold' */
  localB->x2 = ((uint8_T)U8ETCSCpHiAxlAccTh);

  /* Constant: '<Root>/Low acceleration threshold' */
  localB->x1 = ((uint8_T)U8ETCSCpLwAxlAccTh);

  /* Constant: '<Root>/Symmetric brake control start threshold at vehicle speed break point 1' */
  localB->y1 = ((uint8_T)U8ETCSCpSymBrkCtlTh_1);

  /* Constant: '<Root>/Symmetric brake control start threshold at vehicle speed break point 2' */
  localB->y2 = ((uint8_T)U8ETCSCpSymBrkCtlTh_2);

  /* Constant: '<Root>/Symmetric brake control start threshold at vehicle speed break point 3' */
  localB->y3 = ((uint8_T)U8ETCSCpSymBrkCtlTh_3);

  /* Constant: '<Root>/Symmetric brake control start threshold at vehicle speed break point 4' */
  localB->y4 = ((uint8_T)U8ETCSCpSymBrkCtlTh_4);

  /* Constant: '<Root>/Symmetric brake control start threshold at vehicle speed break point 5' */
  localB->y5 = ((uint8_T)U8ETCSCpSymBrkCtlTh_5);

  /* ModelReference: '<Root>/LCETCS_s16Inter5Point' */
  LCETCS_s16Inter5Point(rtu_lcetcss16VehSpd, ((int16_T)S16ETCSCpSpd_1),
                        ((int16_T)S16ETCSCpSpd_2), ((int16_T)S16ETCSCpSpd_3),
                        ((int16_T)S16ETCSCpSpd_4), ((int16_T)S16ETCSCpSpd_5),
                        localB->y1, localB->y2, localB->y3, localB->y4,
                        localB->y5, &localB->y);

  /* MinMax: '<Root>/MinMax' */
  if (localB->y >= rtu_ETCS_TAR_SPIN->lcetcss16TarWhlSpin) {
    rtb_y1 = localB->y;
  } else {
    rtb_y1 = rtu_ETCS_TAR_SPIN->lcetcss16TarWhlSpin;
  }

  /* End of MinMax: '<Root>/MinMax' */

  /* ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point(rtu_ETCS_AXL_ACC->lcetcss16DrvnAxlAcc, localB->x1,
                        localB->x2, rtb_y1,
                        rtu_ETCS_TAR_SPIN->lcetcss16TarWhlSpin, &localB->y_d);

  /* Saturate: '<Root>/Saturation' */
  if (localB->y_d > 255) {
    *rty_lcetcss16SymBrkCtlStrtTh = 255;
  } else if (localB->y_d < 0) {
    *rty_lcetcss16SymBrkCtlStrtTh = 0;
  } else {
    *rty_lcetcss16SymBrkCtlStrtTh = localB->y_d;
  }

  /* End of Saturate: '<Root>/Saturation' */
}

/* Model initialize function */
void LCETCS_vCalStrtThSymBrkCtl_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter5Point' */
  LCETCS_s16Inter5Point_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
