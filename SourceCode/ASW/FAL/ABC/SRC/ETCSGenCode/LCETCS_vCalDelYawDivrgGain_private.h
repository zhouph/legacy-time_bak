/*
 * File: LCETCS_vCalDelYawDivrgGain_private.h
 *
 * Code generated for Simulink model 'LCETCS_vCalDelYawDivrgGain'.
 *
 * Model version                  : 1.240
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:03:06 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalDelYawDivrgGain_private_h_
#define RTW_HEADER_LCETCS_vCalDelYawDivrgGain_private_h_
#include "rtwtypes.h"
#include "model_reference_types.h"

/* Includes for objects with custom storage classes. */
#include "../ETCSGenCodeHeader/ETCSHeader4Import.h"

/*
 * Generate compile time checks that imported macros for parameters
 * with storage class "ImportedDefine" are defined
 */
#ifndef S16ETCSCpSpd_1
#error The variable for the parameter "S16ETCSCpSpd_1" is not defined
#endif

#ifndef S16ETCSCpSpd_2
#error The variable for the parameter "S16ETCSCpSpd_2" is not defined
#endif

#ifndef S16ETCSCpSpd_3
#error The variable for the parameter "S16ETCSCpSpd_3" is not defined
#endif

#ifndef S16ETCSCpSpd_4
#error The variable for the parameter "S16ETCSCpSpd_4" is not defined
#endif

#ifndef S16ETCSCpSpd_5
#error The variable for the parameter "S16ETCSCpSpd_5" is not defined
#endif

#ifndef U8ETCSCpAyAsp
#error The variable for the parameter "U8ETCSCpAyAsp" is not defined
#endif

#ifndef U8ETCSCpAyIce
#error The variable for the parameter "U8ETCSCpAyIce" is not defined
#endif

#ifndef U8ETCSCpAySnw
#error The variable for the parameter "U8ETCSCpAySnw" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainAsp_1
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainAsp_1" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainAsp_2
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainAsp_2" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainAsp_3
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainAsp_3" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainAsp_4
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainAsp_4" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainAsp_5
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainAsp_5" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainIce_1
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainIce_1" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainIce_2
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainIce_2" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainIce_3
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainIce_3" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainIce_4
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainIce_4" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainIce_5
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainIce_5" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainSnw_1
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainSnw_1" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainSnw_2
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainSnw_2" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainSnw_3
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainSnw_3" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainSnw_4
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainSnw_4" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainSnw_5
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainSnw_5" is not defined
#endif
#endif                                 /* RTW_HEADER_LCETCS_vCalDelYawDivrgGain_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
