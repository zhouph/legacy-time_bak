/*
 * File: LCETCS_vCalRefTrq4IValRst.c
 *
 * Code generated for Simulink model 'LCETCS_vCalRefTrq4IValRst'.
 *
 * Model version                  : 1.205
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:13:11 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalRefTrq4IValRst.h"
#include "LCETCS_vCalRefTrq4IValRst_private.h"

const TypeETCSRefTrq2JmpStruct
  LCETCS_vCalRefTrq4IValRst_rtZTypeETCSRefTrq2JmpStruct = {
  0,                                   /* lcetcss32RefTrq2Jmp */
  false,                               /* lcetcsu1RefTrq4JmpSetOk */
  0,                                   /* lcetcss16RefTrqSetTm */
  0,                                   /* lcetcss16Ftr4JmpDn */
  0                                    /* lcetcss16Ftr4JmpUp */
} ;                                    /* TypeETCSRefTrq2JmpStruct ground */

/* Initial conditions for referenced model: 'LCETCS_vCalRefTrq4IValRst' */
void LCETCS_vCalRefTrq4IValRst_Init(B_LCETCS_vCalRefTrq4IValRst_c_T *localB,
  DW_LCETCS_vCalRefTrq4IValRst_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE =
    LCETCS_vCalRefTrq4IValRst_rtZTypeETCSRefTrq2JmpStruct;

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalRefTrq2Jmp' */
  LCETCS_vCalRefTrq2Jmp_Init(&localB->ETCS_REF_TRQ2JMP,
    &localB->lcetcsu1RefTrq4JmpSetOk, &localB->lcetcss16RefTrqSetTm);

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalJmpUpDnFtr' */
  LCETCS_vCalJmpUpDnFtr_Init(&(localDW->LCETCS_vCalJmpUpDnFtr_DWORK1.rtb));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalVehAccAvgIn1stCyl' */
  LCETCS_vCalVehAccAvgIn1stCyl_Init
    (&(localDW->LCETCS_vCalVehAccAvgIn1stCyl_DWORK1.rtdw));
}

/* Start for referenced model: 'LCETCS_vCalRefTrq4IValRst' */
void LCETCS_vCalRefTrq4IValRst_Start(DW_LCETCS_vCalRefTrq4IValRst_f_T *localDW)
{
  /* Start for ModelReference: '<Root>/LCETCS_vCalJmpUpDnFtr' */
  LCETCS_vCalJmpUpDnFtr_Start(&(localDW->LCETCS_vCalJmpUpDnFtr_DWORK1.rtb));

  /* Start for ModelReference: '<Root>/LCETCS_vCalVehAccAvgIn1stCyl' */
  LCETCS_vCalVehAccAvgIn1stCyl_Start
    (&(localDW->LCETCS_vCalVehAccAvgIn1stCyl_DWORK1.rtdw));
}

/* Output and update for referenced model: 'LCETCS_vCalRefTrq4IValRst' */
void LCETCS_vCalRefTrq4IValRst(const TypeETCSH2LStruct *rtu_ETCS_H2L, const
  TypeETCSErrZroCrsStruct *rtu_ETCS_ERR_ZRO_CRS, int16_T rtu_lcetcss16VehSpd,
  const TypeETCSCyl1stStruct *rtu_ETCS_CYL_1ST, const TypeETCSCtlActStruct
  *rtu_ETCS_CTL_ACT, const TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC, const
  TypeETCSHighDctStruct *rtu_ETCS_HI_DCT, const TypeETCSCtlCmdStruct
  *rtu_ETCS_CTL_CMD_OLD, TypeETCSRefTrqStrCtlStruct *rty_ETCS_REF_TRQ_STR_CTL,
  TypeETCSRefTrqStr2CylStruct *rty_ETCS_REF_TRQ_STR2CYL,
  TypeETCSRefTrq2JmpStruct *rty_ETCS_REF_TRQ2JMP,
  B_LCETCS_vCalRefTrq4IValRst_c_T *localB, DW_LCETCS_vCalRefTrq4IValRst_f_T
  *localDW)
{
  /* local block i/o variables */
  TypeETCSRefTrq2JmpStruct rtb_ETCS_REF_TRQ2JMP_OLD;

  /* UnitDelay: '<Root>/Unit Delay' */
  rtb_ETCS_REF_TRQ2JMP_OLD = localDW->UnitDelay_DSTATE;

  /* ModelReference: '<Root>/LCETCS_vCalRefTrq2Jmp' */
  LCETCS_vCalRefTrq2Jmp(rtu_ETCS_CTL_ACT, rtu_ETCS_ERR_ZRO_CRS,
                        rtu_ETCS_CTL_CMD_OLD, rtu_ETCS_H2L,
                        &rtb_ETCS_REF_TRQ2JMP_OLD, &localB->ETCS_REF_TRQ2JMP,
                        &localB->lcetcsu1RefTrq4JmpSetOk,
                        &localB->lcetcss16RefTrqSetTm);

  /* ModelReference: '<Root>/LCETCS_vCalJmpUpDnFtr' */
  LCETCS_vCalJmpUpDnFtr(localB->ETCS_REF_TRQ2JMP,
                        localB->lcetcsu1RefTrq4JmpSetOk,
                        localB->lcetcss16RefTrqSetTm, rty_ETCS_REF_TRQ2JMP,
                        &(localDW->LCETCS_vCalJmpUpDnFtr_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vCalRefTrqAtStrtOfCtl' */
  LCETCS_vCalRefTrqAtStrtOfCtl(rtu_ETCS_VEH_ACC, rtu_ETCS_HI_DCT,
    rtu_lcetcss16VehSpd, rty_ETCS_REF_TRQ_STR_CTL);

  /* ModelReference: '<Root>/LCETCS_vCalVehAccAvgIn1stCyl' */
  LCETCS_vCalVehAccAvgIn1stCyl(rtu_lcetcss16VehSpd, rtu_ETCS_CYL_1ST,
    rtu_ETCS_CTL_ACT, rtu_ETCS_H2L, rtu_ETCS_VEH_ACC, rty_ETCS_REF_TRQ_STR2CYL,
    &(localDW->LCETCS_vCalVehAccAvgIn1stCyl_DWORK1.rtdw));

  /* Update for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = *rty_ETCS_REF_TRQ2JMP;
}

/* Model initialize function */
void LCETCS_vCalRefTrq4IValRst_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalJmpUpDnFtr' */
  LCETCS_vCalJmpUpDnFtr_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalRefTrq2Jmp' */
  LCETCS_vCalRefTrq2Jmp_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalRefTrqAtStrtOfCtl' */
  LCETCS_vCalRefTrqAtStrtOfCtl_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalVehAccAvgIn1stCyl' */
  LCETCS_vCalVehAccAvgIn1stCyl_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
