/*
 * File: LCETCS_vCalHmIGainFct4BigSpn.c
 *
 * Code generated for Simulink model 'LCETCS_vCalHmIGainFct4BigSpn'.
 *
 * Model version                  : 1.161
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:01:10 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalHmIGainFct4BigSpn.h"
#include "LCETCS_vCalHmIGainFct4BigSpn_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalHmIGainFct4BigSpn' */
void LCETCS_vCalHmIGainFct4BigSpn_Init(B_LCETCS_vCalHmIGainFct4BigSpn_c_T
  *localB)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_Init(&localB->y);
}

/* Start for referenced model: 'LCETCS_vCalHmIGainFct4BigSpn' */
void LCETCS_vCalHmIGainFct4BigSpn_Start(B_LCETCS_vCalHmIGainFct4BigSpn_c_T
  *localB)
{
  /* Start for Constant: '<Root>/Constant3' */
  localB->x2 = ((uint8_T)VREF_7_KPH);

  /* Start for Constant: '<Root>/Constant5' */
  localB->y2 = ((uint8_T)U8ETCSCpBigSpnMaxIgFac);
}

/* Output and update for referenced model: 'LCETCS_vCalHmIGainFct4BigSpn' */
void LCETCS_vCalHmIGainFct4BigSpn(const TypeETCSCtlErrStruct *rtu_ETCS_CTL_ERR,
  int16_T *rty_lcetcss16HmIGainFct4BigSpn, B_LCETCS_vCalHmIGainFct4BigSpn_c_T
  *localB)
{
  /* local block i/o variables */
  int16_T rtb_x;

  /* Abs: '<Root>/Abs' */
  if (rtu_ETCS_CTL_ERR->lcetcss16CtlErr < 0) {
    rtb_x = (int16_T)(-rtu_ETCS_CTL_ERR->lcetcss16CtlErr);
  } else {
    rtb_x = rtu_ETCS_CTL_ERR->lcetcss16CtlErr;
  }

  /* End of Abs: '<Root>/Abs' */

  /* Constant: '<Root>/Constant3' */
  localB->x2 = ((uint8_T)VREF_7_KPH);

  /* Constant: '<Root>/Constant5' */
  localB->y2 = ((uint8_T)U8ETCSCpBigSpnMaxIgFac);

  /* ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point(rtb_x, ((int16_T)VREF_3_KPH), localB->x2,
                        rtCP_Constant4_Value, localB->y2, &localB->y);

  /* Switch: '<Root>/Switch' incorporates:
   *  Constant: '<Root>/Constant1'
   */
  if (rtu_ETCS_CTL_ERR->lcetcsu1CtlErrPos) {
    *rty_lcetcss16HmIGainFct4BigSpn = 100;
  } else {
    *rty_lcetcss16HmIGainFct4BigSpn = localB->y;
  }

  /* End of Switch: '<Root>/Switch' */

  /* Saturate: '<Root>/Saturation' */
  if ((*rty_lcetcss16HmIGainFct4BigSpn) > 255) {
    *rty_lcetcss16HmIGainFct4BigSpn = 255;
  } else {
    if ((*rty_lcetcss16HmIGainFct4BigSpn) < 100) {
      *rty_lcetcss16HmIGainFct4BigSpn = 100;
    }
  }

  /* End of Saturate: '<Root>/Saturation' */
}

/* Model initialize function */
void LCETCS_vCalHmIGainFct4BigSpn_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
