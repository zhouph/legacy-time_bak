/*
 * File: LCETCS_vCalTarWhlSpdStlDetd_private.h
 *
 * Code generated for Simulink model 'LCETCS_vCalTarWhlSpdStlDetd'.
 *
 * Model version                  : 1.88
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:49:12 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalTarWhlSpdStlDetd_private_h_
#define RTW_HEADER_LCETCS_vCalTarWhlSpdStlDetd_private_h_
#include "rtwtypes.h"
#include "model_reference_types.h"

/* Includes for objects with custom storage classes. */
#include "../ETCSGenCodeHeader/ETCSHeader4Import.h"

/*
 * Generate compile time checks that imported macros for parameters
 * with storage class "ImportedDefine" are defined
 */
#ifndef S16ETCSCpCircumferenceOfTire
#error The variable for the parameter "S16ETCSCpCircumferenceOfTire" is not defined
#endif

#ifndef S16ETCSCpTrgtEngSpdStlDetd
#error The variable for the parameter "S16ETCSCpTrgtEngSpdStlDetd" is not defined
#endif
#endif                                 /* RTW_HEADER_LCETCS_vCalTarWhlSpdStlDetd_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
