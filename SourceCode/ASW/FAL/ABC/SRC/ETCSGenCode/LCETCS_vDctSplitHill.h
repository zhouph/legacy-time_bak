/*
 * File: LCETCS_vDctSplitHill.h
 *
 * Code generated for Simulink model 'LCETCS_vDctSplitHill'.
 *
 * Model version                  : 1.593
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:15:05 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vDctSplitHill_h_
#define RTW_HEADER_LCETCS_vDctSplitHill_h_
#ifndef LCETCS_vDctSplitHill_COMMON_INCLUDES_
# define LCETCS_vDctSplitHill_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vDctSplitHill_COMMON_INCLUDES_ */

#include "LCETCS_vDctSplitHill_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vDctSplitHillFlag.h"
#include "LCETCS_vDctSplitHillCndSet.h"
#include "LCETCS_vDctHillbyHsaAx.h"
#include "LCETCS_vCalSustainbyHsa.h"
#include "LCETCS_vCalSplitHillCnt.h"
#include "LCETCS_vCalGradientofHill.h"
#include "LCETCS_vCalDistanceOfVeh.h"
#include "LCETCS_vCalDiffAx.h"

/* Block signals for model 'LCETCS_vDctSplitHill' */
typedef struct {
  boolean_T lcetcsu1HillDctCompbyAx;   /* '<Root>/LCETCS_vCalDiffAx' */
  boolean_T lcetcsu1SplitHillClrCndSet;/* '<Root>/LCETCS_vDctSplitHillCndSet' */
  boolean_T lcetcsu1SplitHillClrDecCndSet;/* '<Root>/LCETCS_vDctSplitHillCndSet' */
  boolean_T lcetcsu1SplitHillDctCndSet;/* '<Root>/LCETCS_vDctSplitHillCndSet' */
  boolean_T lcetcsu1HillbyHsaSustain;  /* '<Root>/LCETCS_vCalSustainbyHsa' */
  boolean_T lcetcsu1InhibitGradient;   /* '<Root>/LCETCS_vCalGradientofHill' */
  boolean_T lcetcsu1DctHoldGradient;   /* '<Root>/LCETCS_vCalGradientofHill' */
  boolean_T lcetcsu1GradientTrstSet;   /* '<Root>/LCETCS_vCalGradientofHill' */
  boolean_T lcetcsu1SplitHillDct;      /* '<Root>/LCETCS_vDctSplitHillFlag' */
} B_LCETCS_vDctSplitHill_c_T;

/* Block states (auto storage) for model 'LCETCS_vDctSplitHill' */
typedef struct {
  MdlrefDW_LCETCS_vCalDiffAx_T LCETCS_vCalDiffAx_DWORK1;/* '<Root>/LCETCS_vCalDiffAx' */
  MdlrefDW_LCETCS_vCalDistanceOfVeh_T LCETCS_vCalDistanceOfVeh_DWORK1;/* '<Root>/LCETCS_vCalDistanceOfVeh' */
  MdlrefDW_LCETCS_vCalSustainbyHsa_T LCETCS_vCalSustainbyHsa_DWORK1;/* '<Root>/LCETCS_vCalSustainbyHsa' */
  MdlrefDW_LCETCS_vCalSplitHillCnt_T LCETCS_vCalSplitHillCnt_DWORK1;/* '<Root>/LCETCS_vCalSplitHillCnt' */
  MdlrefDW_LCETCS_vCalGradientofHill_T LCETCS_vCalGradientofHill_DWORK1;/* '<Root>/LCETCS_vCalGradientofHill' */
  MdlrefDW_LCETCS_vDctSplitHillFlag_T LCETCS_vDctSplitHillFlag_DWORK1;/* '<Root>/LCETCS_vDctSplitHillFlag' */
} DW_LCETCS_vDctSplitHill_f_T;

typedef struct {
  B_LCETCS_vDctSplitHill_c_T rtb;
  DW_LCETCS_vDctSplitHill_f_T rtdw;
} MdlrefDW_LCETCS_vDctSplitHill_T;

/* Model reference registration function */
extern void LCETCS_vDctSplitHill_initialize(void);
extern void LCETCS_vDctSplitHill_Init(B_LCETCS_vDctSplitHill_c_T *localB,
  DW_LCETCS_vDctSplitHill_f_T *localDW);
extern void LCETCS_vDctSplitHill_Start(DW_LCETCS_vDctSplitHill_f_T *localDW);
extern void LCETCS_vDctSplitHill(const TypeETCSAxlStruct *rtu_ETCS_RA, const
  TypeETCSAxlStruct *rtu_ETCS_FA, const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG,
  const TypeETCSExtDctStruct *rtu_ETCS_EXT_DCT, const TypeETCSVehAccStruct
  *rtu_ETCS_VEH_ACC, int16_T rtu_lcetcss16VehSpd, const TypeETCSEngStruct
  *rtu_ETCS_ENG_STRUCT, TypeETCSSplitHillStruct *rty_ETCS_SPLIT_HILL,
  B_LCETCS_vDctSplitHill_c_T *localB, DW_LCETCS_vDctSplitHill_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vDctSplitHill'
 */
#endif                                 /* RTW_HEADER_LCETCS_vDctSplitHill_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
