/*
 * File: LCTMC_vCalRefLimVehSpdMap.c
 *
 * Code generated for Simulink model 'LCTMC_vCalRefLimVehSpdMap'.
 *
 * Model version                  : 1.248
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:09:02 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCTMC_vCalRefLimVehSpdMap.h"
#include "LCTMC_vCalRefLimVehSpdMap_private.h"

/* Initial conditions for referenced model: 'LCTMC_vCalRefLimVehSpdMap' */
void LCTMC_vCalRefLimVehSpdMap_Init(DW_LCTMC_vCalRefLimVehSpdMap_f_T *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCTMC_vCalRefMaxVehSpdMapbyGear' */
  LCTMC_vCalRefMaxVehSpdMapbyGear_Init
    (&(localDW->LCTMC_vCalRefMaxVehSpdMapbyGear_DWORK1.rtb));

  /* InitializeConditions for ModelReference: '<Root>/LCTMC_vCalRefMinVehSpdMapbyGear' */
  LCTMC_vCalRefMinVehSpdMapbyGear_Init
    (&(localDW->LCTMC_vCalRefMinVehSpdMapbyGear_DWORK1.rtb));
}

/* Output and update for referenced model: 'LCTMC_vCalRefLimVehSpdMap' */
void LCTMC_vCalRefLimVehSpdMap(const TypeETCSGearStruct *rtu_ETCS_GEAR_STRUCT,
  int16_T *rty_lctmcs16CordTarUpShtMaxVSbyGear, int16_T
  *rty_lctmcs16CordTarDnShtMinVSbyGear, DW_LCTMC_vCalRefLimVehSpdMap_f_T
  *localDW)
{
  /* ModelReference: '<Root>/LCTMC_vCalRefMaxVehSpdMapbyGear' */
  LCTMC_vCalRefMaxVehSpdMapbyGear(rtu_ETCS_GEAR_STRUCT,
    rty_lctmcs16CordTarUpShtMaxVSbyGear,
    &(localDW->LCTMC_vCalRefMaxVehSpdMapbyGear_DWORK1.rtb));

  /* ModelReference: '<Root>/LCTMC_vCalRefMinVehSpdMapbyGear' */
  LCTMC_vCalRefMinVehSpdMapbyGear(rtu_ETCS_GEAR_STRUCT,
    rty_lctmcs16CordTarDnShtMinVSbyGear,
    &(localDW->LCTMC_vCalRefMinVehSpdMapbyGear_DWORK1.rtb));
}

/* Model initialize function */
void LCTMC_vCalRefLimVehSpdMap_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCTMC_vCalRefMaxVehSpdMapbyGear' */
  LCTMC_vCalRefMaxVehSpdMapbyGear_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCTMC_vCalRefMinVehSpdMapbyGear' */
  LCTMC_vCalRefMinVehSpdMapbyGear_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
