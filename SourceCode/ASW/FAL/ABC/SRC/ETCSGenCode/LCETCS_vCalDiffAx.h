/*
 * File: LCETCS_vCalDiffAx.h
 *
 * Code generated for Simulink model 'LCETCS_vCalDiffAx'.
 *
 * Model version                  : 1.350
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:06:40 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalDiffAx_h_
#define RTW_HEADER_LCETCS_vCalDiffAx_h_
#ifndef LCETCS_vCalDiffAx_COMMON_INCLUDES_
# define LCETCS_vCalDiffAx_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalDiffAx_COMMON_INCLUDES_ */

#include "LCETCS_vCalDiffAx_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vCalFiltDiffAx.h"
#include "LCETCS_vCal1stOrdFlter.h"

/* Block states (auto storage) for model 'LCETCS_vCalDiffAx' */
typedef struct {
  uint32_T CircBufIdx;                 /* '<Root>/Delay1' */
  int16_T Delay1_DSTATE[5];            /* '<Root>/Delay1' */
  int16_T UnitDelay_DSTATE;            /* '<Root>/Unit Delay' */
  MdlrefDW_LCETCS_vCalFiltDiffAx_T LCETCS_vCalFiltDiffAx_DWORK1;/* '<Root>/LCETCS_vCalFiltDiffAx' */
} DW_LCETCS_vCalDiffAx_f_T;

typedef struct {
  DW_LCETCS_vCalDiffAx_f_T rtdw;
} MdlrefDW_LCETCS_vCalDiffAx_T;

/* Model reference registration function */
extern void LCETCS_vCalDiffAx_initialize(void);
extern void LCETCS_vCalDiffAx_Init(boolean_T *rty_lcetcsu1HillDctCompbyAx,
  DW_LCETCS_vCalDiffAx_f_T *localDW);
extern void LCETCS_vCalDiffAx(const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG,
  const TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC, int16_T rtu_lcetcss16VehSpd,
  boolean_T *rty_lcetcsu1HillDctCompbyAx, uint8_T *rty_lcetcsu8HillDctCntbyAx,
  int16_T *rty_lcetcss16LongAccFilter, DW_LCETCS_vCalDiffAx_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalDiffAx'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalDiffAx_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
