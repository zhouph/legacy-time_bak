/*
 * File: LCETCS_vCallCalculation.h
 *
 * Code generated for Simulink model 'LCETCS_vCallCalculation'.
 *
 * Model version                  : 1.294
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:14:16 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCallCalculation_h_
#define RTW_HEADER_LCETCS_vCallCalculation_h_
#ifndef LCETCS_vCallCalculation_COMMON_INCLUDES_
# define LCETCS_vCallCalculation_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCallCalculation_COMMON_INCLUDES_ */

#include "LCETCS_vCallCalculation_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vDetCtlModeSports1.h"
#include "LCETCS_vCntStrStat.h"
#include "LCETCS_vCalWhlSpin.h"
#include "LCETCS_vCalWhlAcc.h"
#include "LCETCS_vCalVehAcc.h"
#include "LCETCS_vCalTarWhlSpin.h"
#include "LCETCS_vCalStrtThSymBrkCtl.h"
#include "LCETCS_vCalLdTrq.h"
#include "LCETCS_vCalEngMdl.h"
#include "LCETCS_vCalDrvLinMdl.h"
#include "LCETCS_vCalCtlErr.h"
#include "LCETCS_vCalAxlAcc.h"
#include "LCETCS_vCalActCardanTorq.h"

/* Block states (auto storage) for model 'LCETCS_vCallCalculation' */
typedef struct {
  MdlrefDW_LCETCS_vCalDrvLinMdl_T LCETCS_vCalDrvLinMdl_DWORK1;/* '<Root>/LCETCS_vCalDrvLinMdl' */
  MdlrefDW_LCETCS_vCalEngMdl_T LCETCS_vCalEngMdl_DWORK1;/* '<Root>/LCETCS_vCalEngMdl' */
  MdlrefDW_LCETCS_vCalAxlAcc_T LCETCS_vCalAxlAcc_DWORK1;/* '<Root>/LCETCS_vCalAxlAcc' */
  MdlrefDW_LCETCS_vCntStrStat_T LCETCS_vCntStrStat_DWORK1;/* '<Root>/LCETCS_vCntStrStat' */
  MdlrefDW_LCETCS_vCalTarWhlSpin_T LCETCS_vCalTarWhlSpin_DWORK1;/* '<Root>/LCETCS_vCalTarWhlSpin' */
  MdlrefDW_LCETCS_vCalCtlErr_T LCETCS_vCalCtlErr_DWORK1;/* '<Root>/LCETCS_vCalCtlErr' */
  MdlrefDW_LCETCS_vCalLdTrq_T LCETCS_vCalLdTrq_DWORK1;/* '<Root>/LCETCS_vCalLdTrq' */
  MdlrefDW_LCETCS_vCalStrtThSymBrkCtl_T LCETCS_vCalStrtThSymBrkCtl_DWORK1;/* '<Root>/LCETCS_vCalStrtThSymBrkCtl' */
  MdlrefDW_LCETCS_vCalVehAcc_T LCETCS_vCalVehAcc_DWORK1;/* '<Root>/LCETCS_vCalVehAcc' */
  MdlrefDW_LCETCS_vCalWhlAcc_T LCETCS_vCalWhlAcc_DWORK1;/* '<Root>/LCETCS_vCalWhlAcc' */
} DW_LCETCS_vCallCalculation_f_T;

typedef struct {
  DW_LCETCS_vCallCalculation_f_T rtdw;
} MdlrefDW_LCETCS_vCallCalculation_T;

/* Model reference registration function */
extern void LCETCS_vCallCalculation_initialize(const rtTimingBridge
  *timingBridge);
extern void LCETCS_vCallCalculation_Init(TypeETCSCtrlModeStruct
  *rty_ETCS_CTRL_MODE, DW_LCETCS_vCallCalculation_f_T *localDW);
extern void LCETCS_vCallCalculation_Start(DW_LCETCS_vCallCalculation_f_T
  *localDW);
extern void LCETCS_vCallCalculation(const TypeETCSWhlStruct *rtu_ETCS_FL, const
  TypeETCSWhlStruct *rtu_ETCS_FR, const TypeETCSWhlStruct *rtu_ETCS_RL, const
  TypeETCSWhlStruct *rtu_ETCS_RR, int16_T rtu_lcetcss16VehSpd, const
  TypeETCSGearStruct *rtu_ETCS_GEAR_STRUCT, const TypeETCSEngStruct
  *rtu_ETCS_ENG_STRUCT, const TypeETCSExtDctStruct *rtu_ETCS_EXT_DCT, const
  TypeETCSAxlStruct *rtu_ETCS_FA, const TypeETCSAxlStruct *rtu_ETCS_RA, const
  TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const TypeETCSBrkSigStruct
  *rtu_ETCS_BRK_SIG, const TypeETCS4WDSigStruct *rtu_ETCS_4WD_SIG, const
  TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT_OLD, const TypeETCSDepSnwStruct
  *rtu_ETCS_DEP_SNW, const TypeETCSCtlCmdStruct *rtu_ETCS_CTL_CMD_OLD, boolean_T
  rtu_lcetcsu1VehVibDctOld, const TypeETCSBrkCtlCmdStruct
  *rtu_ETCS_BRK_CTL_CMD_OLD, const TypeETCSBrkCtlReqStruct
  *rtu_ETCS_BRK_CTL_REQ_OLD, const TypeETCSSplitHillStruct
  *rtu_ETCS_SPLIT_HILL_OLD, const TypeETCSHighDctStruct *rtu_ETCS_HI_DCT_OLD,
  const TypeETCSEngStallStruct *rtu_ETCS_ENG_STALL_OLD, const
  TypeETCSLwToSpltStruct *rtu_ETCS_L2SPLT, TypeETCSCtlErrStruct
  *rty_ETCS_CTL_ERR, TypeETCSTarSpinStruct *rty_ETCS_TAR_SPIN,
  TypeETCSDrvMdlStruct *rty_ETCS_DRV_MDL, TypeETCSCrdnTrqStruct
  *rty_ETCS_CRDN_TRQ, TypeETCSVehAccStruct *rty_ETCS_VEH_ACC,
  TypeETCSEngMdlStruct *rty_ETCS_ENG_MDL, TypeETCSLdTrqStruct *rty_ETCS_LD_TRQ,
  TypeETCSAxlAccStruct *rty_ETCS_AXL_ACC, int16_T *rty_lcetcss16SymBrkCtlStrtTh,
  TypeETCSWhlSpinStruct *rty_ETCS_WHL_SPIN, TypeETCSStrStatStruct
  *rty_ETCS_STR_STAT, TypeETCSCtrlModeStruct *rty_ETCS_CTRL_MODE,
  TypeETCSWhlAccStruct *rty_ETCS_WHL_ACC, DW_LCETCS_vCallCalculation_f_T
  *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCallCalculation'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCallCalculation_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
