/*
 * File: LCETCS_vCalVehAccAvgIn1stCyl.c
 *
 * Code generated for Simulink model 'LCETCS_vCalVehAccAvgIn1stCyl'.
 *
 * Model version                  : 1.534
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:09:44 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalVehAccAvgIn1stCyl.h"
#include "LCETCS_vCalVehAccAvgIn1stCyl_private.h"

const TypeETCSRefTrqStr2CylStruct
  LCETCS_vCalVehAccAvgIn1stCyl_rtZTypeETCSRefTrqStr2CylStruct = {
  0,                                   /* lcetcss16VehSpdAtStrtOf1stCyl */
  0,                                   /* lcetcss16VehAccAvgIn1stCyl */
  0,                                   /* lcetcss32EstTrqByVehAccIn1StCyl */
  0                                    /* lcetcss16Ftr4RefTrqAt2ndCylStrt */
} ;                                    /* TypeETCSRefTrqStr2CylStruct ground */

/* Initial conditions for referenced model: 'LCETCS_vCalVehAccAvgIn1stCyl' */
void LCETCS_vCalVehAccAvgIn1stCyl_Init(DW_LCETCS_vCalVehAccAvgIn1stCyl_f_T
  *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE =
    LCETCS_vCalVehAccAvgIn1stCyl_rtZTypeETCSRefTrqStr2CylStruct;

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vConvAx2CrdnTrq' */
  LCETCS_vConvAx2CrdnTrq_Init(&(localDW->LCETCS_vConvAx2CrdnTrq_DWORK1.rtb));
}

/* Start for referenced model: 'LCETCS_vCalVehAccAvgIn1stCyl' */
void LCETCS_vCalVehAccAvgIn1stCyl_Start(DW_LCETCS_vCalVehAccAvgIn1stCyl_f_T
  *localDW)
{
  /* Start for ModelReference: '<Root>/LCETCS_vConvAx2CrdnTrq' */
  LCETCS_vConvAx2CrdnTrq_Start(&(localDW->LCETCS_vConvAx2CrdnTrq_DWORK1.rtb));
}

/* Output and update for referenced model: 'LCETCS_vCalVehAccAvgIn1stCyl' */
void LCETCS_vCalVehAccAvgIn1stCyl(int16_T rtu_lcetcss16VehSpd, const
  TypeETCSCyl1stStruct *rtu_ETCS_CYL_1ST, const TypeETCSCtlActStruct
  *rtu_ETCS_CTL_ACT, const TypeETCSH2LStruct *rtu_ETCS_H2L, const
  TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC, TypeETCSRefTrqStr2CylStruct
  *rty_ETCS_REF_TRQ_STR2CYL, DW_LCETCS_vCalVehAccAvgIn1stCyl_f_T *localDW)
{
  /* local block i/o variables */
  int32_T rtb_CrdnTrq;
  int16_T rtb_lcetcss16VehAccAvgIn1stCyl_o;
  int32_T lcetcsLs32temp;
  int16_T lcetcss16VehSpdAtStrtOf1stCyl;

  /* Chart: '<Root>/CalVehAccAvgIn1stCyl' incorporates:
   *  UnitDelay: '<Root>/Unit Delay1'
   */
  /* Gateway: CalVehAccAvgIn1stCyl */
  /* During: CalVehAccAvgIn1stCyl */
  /* Entry Internal: CalVehAccAvgIn1stCyl */
  /* Transition: '<S1>:100' */
  if (rtu_ETCS_CTL_ACT->lcetcsu1CtlAct == 1) {
    /* Transition: '<S1>:197' */
    /* Transition: '<S1>:199' */
    /*   In case of high-to-low detection, the initial value of vehicle speed for jump up shold be recalculated becased the road friction has been changed.  */
    if ((rtu_ETCS_CYL_1ST->lcetcsu1RsgEdgOfCyl1st == 1) ||
        (rtu_ETCS_H2L->lcetcsu1HiToLwRsgEdg == 1)) {
      /* Transition: '<S1>:105' */
      /* Transition: '<S1>:121' */
      lcetcss16VehSpdAtStrtOf1stCyl = rtu_lcetcss16VehSpd;
      rtb_lcetcss16VehAccAvgIn1stCyl_o =
        localDW->UnitDelay1_DSTATE.lcetcss16VehAccAvgIn1stCyl;

      /* Transition: '<S1>:192' */
      /* Transition: '<S1>:215' */
      /* Transition: '<S1>:66' */
    } else {
      /* Transition: '<S1>:124' */
      if (rtu_ETCS_CYL_1ST->lcetcsu1FalEdgOfCyl1st == 1) {
        /* Transition: '<S1>:125' */
        /* Transition: '<S1>:135' */
        if (rtu_ETCS_CYL_1ST->lcetcss16Cyl1stTm == 0) {
          /* Transition: '<S1>:137' */
          /* Transition: '<S1>:139' */
          /*  unreachable code just to prevent divide by zero  */
          lcetcsLs32temp = 0;

          /* Transition: '<S1>:141' */
        } else {
          /* Transition: '<S1>:142' */
          /*
             18 = (10^3) * (10^2) * (50) / (60^2) / (8) / (9.81)
             (10^3) : unit converrsion factor from km to m
             (10^2) : resolution of vehicle acceleration
             (50) : means 1sec (50 scan)
             (60^2) : unit conversion factor from hour to sec
             (8) : resolution of vehicle speed
             (9.81) : unit conversion factor from m/s^2 to g
           */
          lcetcsLs32temp = ((rtu_lcetcss16VehSpd -
                             localDW->UnitDelay1_DSTATE.lcetcss16VehSpdAtStrtOf1stCyl)
                            * 18) / rtu_ETCS_CYL_1ST->lcetcss16Cyl1stTm;
        }

        /* Transition: '<S1>:204' */
        if (lcetcsLs32temp > 200) {
          /* Transition: '<S1>:81' */
          /* Transition: '<S1>:83' */
          lcetcsLs32temp = 200;

          /* Transition: '<S1>:92' */
          /* Transition: '<S1>:94' */
        } else {
          /* Transition: '<S1>:85' */
          if (lcetcsLs32temp < -200) {
            /* Transition: '<S1>:87' */
            /* Transition: '<S1>:93' */
            lcetcsLs32temp = -200;

            /* Transition: '<S1>:94' */
          } else {
            /* Transition: '<S1>:89' */
          }
        }

        /* Transition: '<S1>:211' */
        lcetcss16VehSpdAtStrtOf1stCyl =
          localDW->UnitDelay1_DSTATE.lcetcss16VehSpdAtStrtOf1stCyl;
        if (((uint8_T)U8ETCSCpAxSenAvail) == 1) {
          /* Transition: '<S1>:213' */
          /* Transition: '<S1>:214' */
          /*  changed from lcetcss16RsltntAcc to lcetcss16VehLongAcc4Ctl
             Not to jump up at negative Ax  */
          rtb_lcetcss16VehAccAvgIn1stCyl_o =
            rtu_ETCS_VEH_ACC->lcetcss16VehLongAcc4Ctl;

          /* Transition: '<S1>:215' */
          /* Transition: '<S1>:66' */
        } else {
          /* Transition: '<S1>:216' */
          rtb_lcetcss16VehAccAvgIn1stCyl_o = (int16_T)lcetcsLs32temp;

          /* Transition: '<S1>:66' */
        }
      } else {
        /* Transition: '<S1>:127' */
        lcetcss16VehSpdAtStrtOf1stCyl =
          localDW->UnitDelay1_DSTATE.lcetcss16VehSpdAtStrtOf1stCyl;
        rtb_lcetcss16VehAccAvgIn1stCyl_o =
          localDW->UnitDelay1_DSTATE.lcetcss16VehAccAvgIn1stCyl;
      }
    }

    /* Transition: '<S1>:196' */
  } else {
    /* Transition: '<S1>:198' */
    lcetcss16VehSpdAtStrtOf1stCyl = 0;
    rtb_lcetcss16VehAccAvgIn1stCyl_o = 0;
  }

  /* End of Chart: '<Root>/CalVehAccAvgIn1stCyl' */

  /* Saturate: '<Root>/Saturation' */
  if (rtb_lcetcss16VehAccAvgIn1stCyl_o > 200) {
    rty_ETCS_REF_TRQ_STR2CYL->lcetcss16VehAccAvgIn1stCyl = 200;
  } else if (rtb_lcetcss16VehAccAvgIn1stCyl_o < -200) {
    rty_ETCS_REF_TRQ_STR2CYL->lcetcss16VehAccAvgIn1stCyl = -200;
  } else {
    rty_ETCS_REF_TRQ_STR2CYL->lcetcss16VehAccAvgIn1stCyl =
      rtb_lcetcss16VehAccAvgIn1stCyl_o;
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* ModelReference: '<Root>/LCETCS_vConvAx2CrdnTrq' */
  LCETCS_vConvAx2CrdnTrq(rtb_lcetcss16VehAccAvgIn1stCyl_o, &rtb_CrdnTrq,
    &(localDW->LCETCS_vConvAx2CrdnTrq_DWORK1.rtb));

  /* Saturate: '<Root>/Saturation1' */
  rty_ETCS_REF_TRQ_STR2CYL->lcetcss32EstTrqByVehAccIn1StCyl = rtb_CrdnTrq;

  /* Constant: '<Root>/Constant' */
  rty_ETCS_REF_TRQ_STR2CYL->lcetcss16Ftr4RefTrqAt2ndCylStrt = ((uint8_T)
    U8ETCSCpFtr4RefTrqAt2ndCylStrt);

  /* BusCreator: '<Root>/Bus Creator' */
  rty_ETCS_REF_TRQ_STR2CYL->lcetcss16VehSpdAtStrtOf1stCyl =
    lcetcss16VehSpdAtStrtOf1stCyl;

  /* Update for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = *rty_ETCS_REF_TRQ_STR2CYL;
}

/* Model initialize function */
void LCETCS_vCalVehAccAvgIn1stCyl_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vConvAx2CrdnTrq' */
  LCETCS_vConvAx2CrdnTrq_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
