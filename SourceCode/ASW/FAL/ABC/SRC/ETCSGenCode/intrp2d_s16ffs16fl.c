/*
 * File: intrp2d_s16ffs16fl.c
 *
 * Code generated for Simulink model 'LCETCS_vCalRefTrqAtStrtOfCtl'.
 *
 * Model version                  : 1.193
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:54:40 2015
 */

#include "rtwtypes.h"
#include "intrp2d_s16ffs16fl.h"

real32_T intrp2d_s16ffs16fl(const int16_T bpIndex[], const real32_T frac[],
  const int16_T table[], uint32_T stride)
{
  real32_T yL_1d;
  uint32_T offset_1d;

  /* Interpolation 2-D
     Interpolation method: 'Linear'
     Use last breakpoint for index at or above upper limit: 'off'
     Overflow mode: 'wrapping'
   */
  offset_1d = ((uint32_T)(bpIndex[1U] * ((int32_T)stride))) + bpIndex[0U];
  yL_1d = ((((real32_T)table[offset_1d + 1U]) - ((real32_T)table[offset_1d])) *
           frac[0U]) + ((real32_T)table[offset_1d]);
  offset_1d += stride;
  return (((((((real32_T)table[offset_1d + 1U]) - ((real32_T)table[offset_1d])) *
             frac[0U]) + ((real32_T)table[offset_1d])) - yL_1d) * frac[1U]) +
    yL_1d;
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
