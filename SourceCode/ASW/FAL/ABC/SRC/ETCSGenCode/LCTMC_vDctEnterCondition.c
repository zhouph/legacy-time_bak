/*
 * File: LCTMC_vDctEnterCondition.c
 *
 * Code generated for Simulink model 'LCTMC_vDctEnterCondition'.
 *
 * Model version                  : 1.224
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:53:20 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCTMC_vDctEnterCondition.h"
#include "LCTMC_vDctEnterCondition_private.h"

/* Initial conditions for referenced model: 'LCTMC_vDctEnterCondition' */
void LCTMC_vDctEnterCondition_Init(boolean_T *rty_lctmcu1NonCndTarReqGear)
{
  /* InitializeConditions for Chart: '<Root>/Chart1' */
  *rty_lctmcu1NonCndTarReqGear = false;
}

/* Output and update for referenced model: 'LCTMC_vDctEnterCondition' */
void LCTMC_vDctEnterCondition(boolean_T rtu_lctmcu1DriveGearMode, const
  TypeETCSExtDctStruct *rtu_ETCS_EXT_DCT, uint8_T rtu_lctmcu8_BSTGRReqGearOld,
  const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, boolean_T rtu_lcu1TmcInhibitMode,
  boolean_T *rty_lctmcu1NonCndTarReqGear)
{
  /* Chart: '<Root>/Chart1' */
  /* Gateway: Chart1 */
  /* During: Chart1 */
  /* Entry Internal: Chart1 */
  /* Transition: '<S1>:10' */
  /* comment */
  if (((((rtu_lctmcu1DriveGearMode == 0) || (rtu_ETCS_EXT_DCT->lcetcsu1AutoTrans
          == 0)) || (rtu_lctmcu8_BSTGRReqGearOld == 0)) ||
       (rtu_ETCS_CTL_ACT->lcetcsu1CtlAct == 0)) || (rtu_lcu1TmcInhibitMode == 1))
  {
    /* Transition: '<S1>:1' */
    /* Transition: '<S1>:47' */
    *rty_lctmcu1NonCndTarReqGear = true;

    /* Transition: '<S1>:46' */
  } else {
    /* Transition: '<S1>:45' */
    *rty_lctmcu1NonCndTarReqGear = false;
  }

  /* End of Chart: '<Root>/Chart1' */
  /* Transition: '<S1>:44' */
}

/* Model initialize function */
void LCTMC_vDctEnterCondition_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
