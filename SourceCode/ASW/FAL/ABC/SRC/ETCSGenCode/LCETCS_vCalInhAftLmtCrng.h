/*
 * File: LCETCS_vCalInhAftLmtCrng.h
 *
 * Code generated for Simulink model 'LCETCS_vCalInhAftLmtCrng'.
 *
 * Model version                  : 1.98
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:09:22 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalInhAftLmtCrng_h_
#define RTW_HEADER_LCETCS_vCalInhAftLmtCrng_h_
#ifndef LCETCS_vCalInhAftLmtCrng_COMMON_INCLUDES_
# define LCETCS_vCalInhAftLmtCrng_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalInhAftLmtCrng_COMMON_INCLUDES_ */

#include "LCETCS_vCalInhAftLmtCrng_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vConvAx2CrdnTrq.h"
#include "LCETCS_vCalScaleDnAyMani.h"

/* Block states (auto storage) for model 'LCETCS_vCalInhAftLmtCrng' */
typedef struct {
  MdlrefDW_LCETCS_vCalScaleDnAyMani_T LCETCS_vCalScaleDnAyMani_DWORK1;/* '<Root>/LCETCS_vCalScaleDnAyMani' */
  MdlrefDW_LCETCS_vConvAx2CrdnTrq_T LCETCS_vConvAx2CrdnTrq_DWORK1;/* '<Root>/LCETCS_vConvAx2CrdnTrq' */
} DW_LCETCS_vCalInhAftLmtCrng_f_T;

typedef struct {
  DW_LCETCS_vCalInhAftLmtCrng_f_T rtdw;
} MdlrefDW_LCETCS_vCalInhAftLmtCrng_T;

/* Model reference registration function */
extern void LCETCS_vCalInhAftLmtCrng_initialize(void);
extern void LCETCS_vCalInhAftLmtCrng_Init(boolean_T *rty_lcetcsu1InhAftLmtCrng,
  DW_LCETCS_vCalInhAftLmtCrng_f_T *localDW);
extern void LCETCS_vCalInhAftLmtCrng_Start(DW_LCETCS_vCalInhAftLmtCrng_f_T
  *localDW);
extern void LCETCS_vCalInhAftLmtCrng(const TypeETCSTarSpinStruct
  *rtu_ETCS_TAR_SPIN, const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const
  TypeETCSCtlCmdStruct *rtu_ETCS_CTL_CMD, const TypeETCSEscSigStruct
  *rtu_ETCS_ESC_SIG, int16_T rtu_lcetcss16AftLmtCrngCnt, int16_T
  rtu_lcetcss16InhCntAftLmtCrngSpn, boolean_T *rty_lcetcsu1InhAftLmtCrng,
  int16_T *rty_lcetcss16ScaleDnAyMani, DW_LCETCS_vCalInhAftLmtCrng_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalInhAftLmtCrng'
 * '<S1>'   : 'LCETCS_vCalInhAftLmtCrng/Chart'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalInhAftLmtCrng_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
