/*
 * File: LCETCS_vCallDetection.c
 *
 * Code generated for Simulink model 'LCETCS_vCallDetection'.
 *
 * Model version                  : 1.397
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:17:11 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCallDetection.h"
#include "LCETCS_vCallDetection_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCallDetection' */
void LCETCS_vCallDetection_Init(TypeETCSCyl1stStruct *rty_ETCS_CYL_1ST,
  TypeETCSErrZroCrsStruct *rty_ETCS_ERR_ZRO_CRS, TypeETCSLowWhlSpnStruct
  *rty_ETCS_LOW_WHL_SPN, TypeETCSGainGearShftStruct *rty_ETCS_GAIN_GEAR_SHFT,
  TypeETCSH2LStruct *rty_ETCS_H2L, DW_LCETCS_vCallDetection_f_T *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctEngStall' */
  LCETCS_vDctEngStall_Init(&(localDW->LCETCS_vDctEngStall_DWORK1.rtb),
    &(localDW->LCETCS_vDctEngStall_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctCtlEnterExitCond' */
  LCETCS_vDctCtlEnterExitCond_Init
    (&(localDW->LCETCS_vDctCtlEnterExitCond_DWORK1.rtb),
     &(localDW->LCETCS_vDctCtlEnterExitCond_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalAftLmtCrng' */
  LCETCS_vCalAftLmtCrng_Init(&(localDW->LCETCS_vCalAftLmtCrng_DWORK1.rtb),
    &(localDW->LCETCS_vCalAftLmtCrng_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctReqVehSpdAsit' */
  LCETCS_vDctReqVehSpdAsit_Init(&(localDW->LCETCS_vDctReqVehSpdAsit_DWORK1.rtb),
    &(localDW->LCETCS_vDctReqVehSpdAsit_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCntLowSpinTm' */
  LCETCS_vCntLowSpinTm_Init(rty_ETCS_LOW_WHL_SPN,
    &(localDW->LCETCS_vCntLowSpinTm_DWORK1.rtb));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctCyl1st' */
  LCETCS_vDctCyl1st_Init(rty_ETCS_CYL_1ST,
    &(localDW->LCETCS_vDctCyl1st_DWORK1.rtdw),
    &(localDW->LCETCS_vDctCyl1st_DWORK1.rtb));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctZeroCrossingOfErr' */
  LCETCS_vDctZeroCrossingOfErr_Init(rty_ETCS_ERR_ZRO_CRS,
    &(localDW->LCETCS_vDctZeroCrossingOfErr_DWORK1.rtdw),
    &(localDW->LCETCS_vDctZeroCrossingOfErr_DWORK1.rtb));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctHiToLw' */
  LCETCS_vDctHiToLw_Init(rty_ETCS_H2L, &(localDW->LCETCS_vDctHiToLw_DWORK1.rtdw),
    &(localDW->LCETCS_vDctHiToLw_DWORK1.rtb));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctHighRd' */
  LCETCS_vDctHighRd_Init(&(localDW->LCETCS_vDctHighRd_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalRefTrq4IValRst' */
  LCETCS_vCalRefTrq4IValRst_Init(&(localDW->LCETCS_vCalRefTrq4IValRst_DWORK1.rtb),
    &(localDW->LCETCS_vCalRefTrq4IValRst_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalIVal2Rst' */
  LCETCS_vCalIVal2Rst_Init(&(localDW->LCETCS_vCalIVal2Rst_DWORK1.rtb),
    &(localDW->LCETCS_vCalIVal2Rst_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctDepSnwHil' */
  LCETCS_vDctDepSnwHil_Init(&(localDW->LCETCS_vDctDepSnwHil_DWORK1.rtb),
    &(localDW->LCETCS_vDctDepSnwHil_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctGainChngInGearShft' */
  LCETCS_vDctGainChngInGearShft_Init(rty_ETCS_GAIN_GEAR_SHFT,
    &(localDW->LCETCS_vDctGainChngInGearShft_DWORK1.rtb),
    &(localDW->LCETCS_vDctGainChngInGearShft_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctLwToSplt' */
  LCETCS_vDctLwToSplt_Init(&(localDW->LCETCS_vDctLwToSplt_DWORK1.rtb),
    &(localDW->LCETCS_vDctLwToSplt_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctSplitHill' */
  LCETCS_vDctSplitHill_Init(&(localDW->LCETCS_vDctSplitHill_DWORK1.rtb),
    &(localDW->LCETCS_vDctSplitHill_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctSymBrkCtlEtrExt' */
  LCETCS_vDctSymBrkCtlEtrExt_Init
    (&(localDW->LCETCS_vDctSymBrkCtlEtrExt_DWORK1.rtdw));
}

/* Disable for referenced model: 'LCETCS_vCallDetection' */
void LCETCS_vCallDetection_Disable(DW_LCETCS_vCallDetection_f_T *localDW)
{
  /* Disable for ModelReference: '<Root>/LCETCS_vDctEngStall' */
  LCETCS_vDctEngStall_Disable(&(localDW->LCETCS_vDctEngStall_DWORK1.rtdw));
}

/* Start for referenced model: 'LCETCS_vCallDetection' */
void LCETCS_vCallDetection_Start(DW_LCETCS_vCallDetection_f_T *localDW)
{
  /* Start for ModelReference: '<Root>/LCETCS_vDctEngStall' */
  LCETCS_vDctEngStall_Start(&(localDW->LCETCS_vDctEngStall_DWORK1.rtdw));

  /* Start for ModelReference: '<Root>/LCETCS_vDctCtlEnterExitCond' */
  LCETCS_vDctCtlEnterExitCond_Start
    (&(localDW->LCETCS_vDctCtlEnterExitCond_DWORK1.rtdw));

  /* Start for ModelReference: '<Root>/LCETCS_vCalAftLmtCrng' */
  LCETCS_vCalAftLmtCrng_Start(&(localDW->LCETCS_vCalAftLmtCrng_DWORK1.rtdw));

  /* Start for ModelReference: '<Root>/LCETCS_vCntLowSpinTm' */
  LCETCS_vCntLowSpinTm_Start(&(localDW->LCETCS_vCntLowSpinTm_DWORK1.rtb));

  /* Start for ModelReference: '<Root>/LCETCS_vCalRefTrq4IValRst' */
  LCETCS_vCalRefTrq4IValRst_Start
    (&(localDW->LCETCS_vCalRefTrq4IValRst_DWORK1.rtdw));

  /* Start for ModelReference: '<Root>/LCETCS_vCalIVal2Rst' */
  LCETCS_vCalIVal2Rst_Start(&(localDW->LCETCS_vCalIVal2Rst_DWORK1.rtdw));

  /* Start for ModelReference: '<Root>/LCETCS_vDctSplitHill' */
  LCETCS_vDctSplitHill_Start(&(localDW->LCETCS_vDctSplitHill_DWORK1.rtdw));
}

/* Output and update for referenced model: 'LCETCS_vCallDetection' */
void LCETCS_vCallDetection(const TypeETCSCtlErrStruct *rtu_ETCS_CTL_ERR, const
  TypeETCSWhlSpinStruct *rtu_ETCS_WHL_SPIN, const TypeETCSTarSpinStruct
  *rtu_ETCS_TAR_SPIN, const TypeETCSEngStruct *rtu_ETCS_ENG_STRUCT, const
  TypeETCSCrdnTrqStruct *rtu_ETCS_CRDN_TRQ, int16_T rtu_lcetcss16VehSpd, const
  TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC, const TypeETCSGearStruct
  *rtu_ETCS_GEAR_STRUCT, const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const
  TypeETCSAxlAccStruct *rtu_ETCS_AXL_ACC, const TypeETCSAxlStruct *rtu_ETCS_FA,
  const TypeETCSAxlStruct *rtu_ETCS_RA, int16_T rtu_lcetcss16SymBrkCtlStrtTh,
  const TypeETCSBrkCtlCmdStruct *rtu_ETCS_BRK_CTL_CMD_OLD, const
  TypeETCSEngCmdStruct *rtu_ETCS_ENG_CMD_OLD, const TypeETCSCtlCmdStruct
  *rtu_ETCS_CTL_CMD_OLD, const TypeETCSExtDctStruct *rtu_ETCS_EXT_DCT, const
  TypeETCSWhlStruct *rtu_ETCS_FL, const TypeETCSWhlStruct *rtu_ETCS_FR, const
  TypeETCSWhlStruct *rtu_ETCS_RL, const TypeETCSWhlStruct *rtu_ETCS_RR, const
  TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT_OLD, const TypeETCSSplitHillStruct
  *rtu_ETCS_SPLIT_HILL_OLD, const TypeETCSCtrlModeStruct *rtu_ETCS_CTRL_MODE,
  const TypeETCSWhlAccStruct *rtu_ETCS_WHL_ACC, const TypeETCSTrq4RdFricStruct
  *rtu_ETCS_TRQ_REP_RD_FRIC_OLD, const TypeETCSEngStallStruct
  *rtu_ETCS_ENG_STALL_OLD, TypeETCSCtlActStruct *rty_ETCS_CTL_ACT,
  TypeETCSCyl1stStruct *rty_ETCS_CYL_1ST, TypeETCSErrZroCrsStruct
  *rty_ETCS_ERR_ZRO_CRS, TypeETCSIValRstStruct *rty_ETCS_IVAL_RST,
  TypeETCSRefTrqStrCtlStruct *rty_ETCS_REF_TRQ_STR_CTL,
  TypeETCSRefTrqStr2CylStruct *rty_ETCS_REF_TRQ_STR2CYL,
  TypeETCSRefTrq2JmpStruct *rty_ETCS_REF_TRQ2JMP, TypeETCSLowWhlSpnStruct
  *rty_ETCS_LOW_WHL_SPN, TypeETCSDepSnwStruct *rty_ETCS_DEP_SNW,
  TypeETCSGainGearShftStruct *rty_ETCS_GAIN_GEAR_SHFT, TypeETCSH2LStruct
  *rty_ETCS_H2L, TypeETCSBrkCtlReqStruct *rty_ETCS_BRK_CTL_REQ, boolean_T
  *rty_lcetcsu1VehVibDct, TypeETCSSplitHillStruct *rty_ETCS_SPLIT_HILL,
  TypeETCSHighDctStruct *rty_ETCS_HI_DCT, TypeETCSReqVCAStruct *rty_ETCS_REQ_VCA,
  TypeETCSLwToSpltStruct *rty_ETCS_L2SPLT, TypeETCSAftLmtCrng *rty_ETCS_AFT_TURN,
  TypeETCSEngStallStruct *rty_ETCS_ENG_STALL, DW_LCETCS_vCallDetection_f_T
  *localDW)
{
  /* ModelReference: '<Root>/LCETCS_vDctEngStall' */
  LCETCS_vDctEngStall(rtu_ETCS_CTL_ACT_OLD, rtu_ETCS_SPLIT_HILL_OLD,
                      rtu_lcetcss16VehSpd, rtu_ETCS_ENG_STRUCT,
                      rtu_ETCS_ENG_STALL_OLD, rty_ETCS_ENG_STALL,
                      &(localDW->LCETCS_vDctEngStall_DWORK1.rtb),
                      &(localDW->LCETCS_vDctEngStall_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vDctCtlEnterExitCond' */
  LCETCS_vDctCtlEnterExitCond(rty_ETCS_ENG_STALL, rtu_ETCS_ENG_CMD_OLD,
    rtu_ETCS_ENG_STRUCT, rtu_ETCS_WHL_SPIN, rtu_ETCS_TAR_SPIN, rtu_ETCS_AXL_ACC,
    rtu_ETCS_CTL_ACT_OLD, rtu_ETCS_VEH_ACC, rtu_ETCS_ESC_SIG, rtu_ETCS_EXT_DCT,
    rtu_ETCS_CTRL_MODE, rty_ETCS_CTL_ACT,
    &(localDW->LCETCS_vDctCtlEnterExitCond_DWORK1.rtb),
    &(localDW->LCETCS_vDctCtlEnterExitCond_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalAftLmtCrng' */
  LCETCS_vCalAftLmtCrng(rty_ETCS_CTL_ACT, rtu_ETCS_CTL_CMD_OLD, rtu_ETCS_ESC_SIG,
                        rtu_ETCS_CTL_ERR, rtu_ETCS_TAR_SPIN, rty_ETCS_AFT_TURN,
                        &(localDW->LCETCS_vCalAftLmtCrng_DWORK1.rtb),
                        &(localDW->LCETCS_vCalAftLmtCrng_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vDctReqVehSpdAsit' */
  LCETCS_vDctReqVehSpdAsit(rty_ETCS_CTL_ACT, rtu_ETCS_VEH_ACC, rty_ETCS_REQ_VCA,
    &(localDW->LCETCS_vDctReqVehSpdAsit_DWORK1.rtb),
    &(localDW->LCETCS_vDctReqVehSpdAsit_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCntLowSpinTm' */
  LCETCS_vCntLowSpinTm(rty_ETCS_CTL_ACT, rtu_ETCS_CTL_ERR, rtu_ETCS_WHL_SPIN,
                       rtu_ETCS_ESC_SIG, rtu_ETCS_TAR_SPIN, rty_ETCS_REQ_VCA,
                       rty_ETCS_LOW_WHL_SPN,
                       &(localDW->LCETCS_vCntLowSpinTm_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vDctCyl1st' */
  LCETCS_vDctCyl1st(rty_ETCS_LOW_WHL_SPN, rty_ETCS_CTL_ACT, rtu_ETCS_CTL_ERR,
                    rty_ETCS_CYL_1ST, &(localDW->LCETCS_vDctCyl1st_DWORK1.rtdw),
                    &(localDW->LCETCS_vDctCyl1st_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vDctZeroCrossingOfErr' */
  LCETCS_vDctZeroCrossingOfErr(rty_ETCS_CYL_1ST, rty_ETCS_CTL_ACT,
    rtu_ETCS_CTL_ERR, rty_ETCS_ERR_ZRO_CRS,
    &(localDW->LCETCS_vDctZeroCrossingOfErr_DWORK1.rtdw),
    &(localDW->LCETCS_vDctZeroCrossingOfErr_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vDctHiToLw' */
  LCETCS_vDctHiToLw(rty_ETCS_ERR_ZRO_CRS, rtu_ETCS_VEH_ACC, rtu_ETCS_WHL_SPIN,
                    rty_ETCS_H2L, &(localDW->LCETCS_vDctHiToLw_DWORK1.rtdw),
                    &(localDW->LCETCS_vDctHiToLw_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vDctHighRd' */
  LCETCS_vDctHighRd(rty_ETCS_CTL_ACT, rtu_ETCS_WHL_ACC, rtu_ETCS_WHL_SPIN,
                    rtu_ETCS_TRQ_REP_RD_FRIC_OLD, rtu_ETCS_VEH_ACC,
                    rtu_ETCS_CRDN_TRQ, rtu_lcetcss16VehSpd, rtu_ETCS_ESC_SIG,
                    rtu_ETCS_TAR_SPIN, rtu_ETCS_CTL_ERR, rty_ETCS_HI_DCT,
                    &(localDW->LCETCS_vDctHighRd_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalRefTrq4IValRst' */
  LCETCS_vCalRefTrq4IValRst(rty_ETCS_H2L, rty_ETCS_ERR_ZRO_CRS,
    rtu_lcetcss16VehSpd, rty_ETCS_CYL_1ST, rty_ETCS_CTL_ACT, rtu_ETCS_VEH_ACC,
    rty_ETCS_HI_DCT, rtu_ETCS_CTL_CMD_OLD, rty_ETCS_REF_TRQ_STR_CTL,
    rty_ETCS_REF_TRQ_STR2CYL, rty_ETCS_REF_TRQ2JMP,
    &(localDW->LCETCS_vCalRefTrq4IValRst_DWORK1.rtb),
    &(localDW->LCETCS_vCalRefTrq4IValRst_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalIVal2Rst' */
  LCETCS_vCalIVal2Rst(rty_ETCS_REF_TRQ2JMP, rtu_ETCS_CTL_CMD_OLD,
                      rty_ETCS_REF_TRQ_STR_CTL, rty_ETCS_ERR_ZRO_CRS,
                      rty_ETCS_CTL_ACT, rty_ETCS_REF_TRQ_STR2CYL,
                      rty_ETCS_CYL_1ST, rtu_ETCS_VEH_ACC, rty_ETCS_HI_DCT,
                      rty_ETCS_REQ_VCA, rtu_ETCS_ESC_SIG, rtu_ETCS_TAR_SPIN,
                      rtu_ETCS_CRDN_TRQ, rty_ETCS_IVAL_RST,
                      &(localDW->LCETCS_vCalIVal2Rst_DWORK1.rtb),
                      &(localDW->LCETCS_vCalIVal2Rst_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vDctDepSnwHil' */
  LCETCS_vDctDepSnwHil(rty_ETCS_CTL_ACT, rtu_ETCS_VEH_ACC, rtu_ETCS_WHL_SPIN,
                       rtu_ETCS_GEAR_STRUCT, rtu_ETCS_ESC_SIG,
                       rtu_lcetcss16VehSpd, rty_ETCS_DEP_SNW,
                       &(localDW->LCETCS_vDctDepSnwHil_DWORK1.rtb),
                       &(localDW->LCETCS_vDctDepSnwHil_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vDctGainChngInGearShft' */
  LCETCS_vDctGainChngInGearShft(rty_ETCS_H2L, rty_ETCS_CTL_ACT,
    rtu_ETCS_GEAR_STRUCT, rty_ETCS_CYL_1ST, rtu_ETCS_EXT_DCT,
    rty_ETCS_GAIN_GEAR_SHFT, &(localDW->LCETCS_vDctGainChngInGearShft_DWORK1.rtb),
    &(localDW->LCETCS_vDctGainChngInGearShft_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vDctLwToSplt' */
  LCETCS_vDctLwToSplt(rty_ETCS_CTL_ACT, rtu_ETCS_FA, rtu_ETCS_RA, rtu_ETCS_FL,
                      rtu_ETCS_FR, rtu_ETCS_RL, rtu_ETCS_RR, rtu_ETCS_ESC_SIG,
                      rtu_ETCS_TRQ_REP_RD_FRIC_OLD, rtu_ETCS_TAR_SPIN,
                      rtu_ETCS_VEH_ACC, rty_ETCS_L2SPLT,
                      &(localDW->LCETCS_vDctLwToSplt_DWORK1.rtb),
                      &(localDW->LCETCS_vDctLwToSplt_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vDctSplitHill' */
  LCETCS_vDctSplitHill(rtu_ETCS_RA, rtu_ETCS_FA, rtu_ETCS_ESC_SIG,
                       rtu_ETCS_EXT_DCT, rtu_ETCS_VEH_ACC, rtu_lcetcss16VehSpd,
                       rtu_ETCS_ENG_STRUCT, rty_ETCS_SPLIT_HILL,
                       &(localDW->LCETCS_vDctSplitHill_DWORK1.rtb),
                       &(localDW->LCETCS_vDctSplitHill_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vDctSymBrkCtlEtrExt' */
  LCETCS_vDctSymBrkCtlEtrExt(rty_ETCS_CTL_ACT, rtu_ETCS_FL, rtu_ETCS_FR,
    rtu_ETCS_RL, rtu_ETCS_RR, rtu_ETCS_WHL_SPIN, rty_ETCS_ENG_STALL,
    rtu_ETCS_AXL_ACC, rtu_lcetcss16SymBrkCtlStrtTh, rtu_ETCS_BRK_CTL_CMD_OLD,
    rtu_ETCS_VEH_ACC, rtu_ETCS_FA, rtu_ETCS_RA, rty_ETCS_BRK_CTL_REQ,
    &(localDW->LCETCS_vDctSymBrkCtlEtrExt_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vDctVehVib' */
  LCETCS_vDctVehVib(rtu_ETCS_FL, rtu_ETCS_FR, rtu_ETCS_RL, rtu_ETCS_RR,
                    rty_lcetcsu1VehVibDct);
}

/* Model initialize function */
void LCETCS_vCallDetection_initialize(const rtTimingBridge *timingBridge)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalAftLmtCrng' */
  LCETCS_vCalAftLmtCrng_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalIVal2Rst' */
  LCETCS_vCalIVal2Rst_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalRefTrq4IValRst' */
  LCETCS_vCalRefTrq4IValRst_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCntLowSpinTm' */
  LCETCS_vCntLowSpinTm_initialize(timingBridge);

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctCtlEnterExitCond' */
  LCETCS_vDctCtlEnterExitCond_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctCyl1st' */
  LCETCS_vDctCyl1st_initialize(timingBridge);

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctDepSnwHil' */
  LCETCS_vDctDepSnwHil_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctEngStall' */
  LCETCS_vDctEngStall_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctGainChngInGearShft' */
  LCETCS_vDctGainChngInGearShft_initialize(timingBridge);

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctHiToLw' */
  LCETCS_vDctHiToLw_initialize(timingBridge);

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctHighRd' */
  LCETCS_vDctHighRd_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctLwToSplt' */
  LCETCS_vDctLwToSplt_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctReqVehSpdAsit' */
  LCETCS_vDctReqVehSpdAsit_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctSplitHill' */
  LCETCS_vDctSplitHill_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctSymBrkCtlEtrExt' */
  LCETCS_vDctSymBrkCtlEtrExt_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctVehVib' */
  LCETCS_vDctVehVib_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctZeroCrossingOfErr' */
  LCETCS_vDctZeroCrossingOfErr_initialize(timingBridge);
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
