/*
 * File: LCETCS_vDctDepSnwHil.h
 *
 * Code generated for Simulink model 'LCETCS_vDctDepSnwHil'.
 *
 * Model version                  : 1.595
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:05:33 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vDctDepSnwHil_h_
#define RTW_HEADER_LCETCS_vDctDepSnwHil_h_
#ifndef LCETCS_vDctDepSnwHil_COMMON_INCLUDES_
# define LCETCS_vDctDepSnwHil_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vDctDepSnwHil_COMMON_INCLUDES_ */

#include "LCETCS_vDctDepSnwHil_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vDctSetDepSnwHil.h"
#include "LCETCS_vCntDepSnwHil.h"

/* Block signals for model 'LCETCS_vDctDepSnwHil' */
typedef struct {
  boolean_T lcetcsu1DepSnwHilAct;      /* '<Root>/LCETCS_vDctSetDepSnwHil' */
} B_LCETCS_vDctDepSnwHil_c_T;

/* Block states (auto storage) for model 'LCETCS_vDctDepSnwHil' */
typedef struct {
  MdlrefDW_LCETCS_vCntDepSnwHil_T LCETCS_vCntDepSnwHil_DWORK1;/* '<Root>/LCETCS_vCntDepSnwHil' */
  MdlrefDW_LCETCS_vDctSetDepSnwHil_T LCETCS_vDctSetDepSnwHil_DWORK1;/* '<Root>/LCETCS_vDctSetDepSnwHil' */
} DW_LCETCS_vDctDepSnwHil_f_T;

typedef struct {
  B_LCETCS_vDctDepSnwHil_c_T rtb;
  DW_LCETCS_vDctDepSnwHil_f_T rtdw;
} MdlrefDW_LCETCS_vDctDepSnwHil_T;

/* Model reference registration function */
extern void LCETCS_vDctDepSnwHil_initialize(void);
extern void LCETCS_vDctDepSnwHil_Init(B_LCETCS_vDctDepSnwHil_c_T *localB,
  DW_LCETCS_vDctDepSnwHil_f_T *localDW);
extern void LCETCS_vDctDepSnwHil(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT,
  const TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC, const TypeETCSWhlSpinStruct
  *rtu_ETCS_WHL_SPIN, const TypeETCSGearStruct *rtu_ETCS_GEAR_STRUCT, const
  TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, int16_T rtu_lcetcss16VehSpd,
  TypeETCSDepSnwStruct *rty_ETCS_DEP_SNW, B_LCETCS_vDctDepSnwHil_c_T *localB,
  DW_LCETCS_vDctDepSnwHil_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vDctDepSnwHil'
 */
#endif                                 /* RTW_HEADER_LCETCS_vDctDepSnwHil_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
