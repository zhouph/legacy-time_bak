/*
 * File: LCETCS_vCalDetTarWhlIncTire.c
 *
 * Code generated for Simulink model 'LCETCS_vCalDetTarWhlIncTire'.
 *
 * Model version                  : 1.128
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:50:04 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalDetTarWhlIncTire.h"
#include "LCETCS_vCalDetTarWhlIncTire_private.h"

/* Output and update for referenced model: 'LCETCS_vCalDetTarWhlIncTire' */
void LCETCS_vCalDetTarWhlIncTire(boolean_T rtu_lcetcsu1AbnrmSizeTireDet,
  boolean_T rtu_lcetcsu1AbnrmSizeTireSus, int16_T rtu_lcetcss16VehSpd, const
  TypeETCSExtDctStruct *rtu_ETCS_EXT_DCT, int16_T
  *rty_lcetcss16TarWhlSpnIncAbnrmTire)
{
  int16_T rtu_lcetcsu1AbnrmSizeTireDet_0;

  /* If: '<Root>/If' incorporates:
   *  Product: '<S1>/Divide2'
   */
  if (!rtu_lcetcsu1AbnrmSizeTireDet) {
    /* Outputs for IfAction SubSystem: '<Root>/Non Detect Abnrm Tire' incorporates:
     *  ActionPort: '<S2>/Action Port'
     */
    /* Switch: '<S2>/Switch' incorporates:
     *  Constant: '<S2>/Constant1'
     *  Product: '<S2>/Divide2'
     *  Product: '<S2>/Product'
     *  Sum: '<S2>/Add1'
     */
    if (rtu_lcetcsu1AbnrmSizeTireSus) {
      rtu_lcetcsu1AbnrmSizeTireDet_0 = (int16_T)(((100 -
        rtu_ETCS_EXT_DCT->lcetcsu8AbnrmTireRatio) * rtu_lcetcss16VehSpd) / 100);
    } else {
      rtu_lcetcsu1AbnrmSizeTireDet_0 = 0;
    }

    /* End of Switch: '<S2>/Switch' */
    /* End of Outputs for SubSystem: '<Root>/Non Detect Abnrm Tire' */
  } else {
    /* Outputs for IfAction SubSystem: '<Root>/Detect Abnrm Tire' incorporates:
     *  ActionPort: '<S1>/Action Port'
     */
    rtu_lcetcsu1AbnrmSizeTireDet_0 = (int16_T)(rtu_lcetcss16VehSpd / 50);

    /* End of Outputs for SubSystem: '<Root>/Detect Abnrm Tire' */
  }

  /* End of If: '<Root>/If' */

  /* Saturate: '<Root>/Saturation' incorporates:
   *  Constant: '<Root>/Constant1'
   *  Product: '<Root>/Divide2'
   *  Sum: '<Root>/Add1'
   */
  *rty_lcetcss16TarWhlSpnIncAbnrmTire = (int16_T)
    ((rtu_lcetcsu1AbnrmSizeTireDet_0 / 2) + ((uint8_T)
      U8ETCSCpTarSpnAdd4AbnmSizTire));
  if ((*rty_lcetcss16TarWhlSpnIncAbnrmTire) > 250) {
    *rty_lcetcss16TarWhlSpnIncAbnrmTire = 250;
  } else {
    if ((*rty_lcetcss16TarWhlSpnIncAbnrmTire) < -250) {
      *rty_lcetcss16TarWhlSpnIncAbnrmTire = -250;
    }
  }

  /* End of Saturate: '<Root>/Saturation' */
}

/* Model initialize function */
void LCETCS_vCalDetTarWhlIncTire_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
