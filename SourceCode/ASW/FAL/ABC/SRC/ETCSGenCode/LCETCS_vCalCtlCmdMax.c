/*
 * File: LCETCS_vCalCtlCmdMax.c
 *
 * Code generated for Simulink model 'LCETCS_vCalCtlCmdMax'.
 *
 * Model version                  : 1.193
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:51:18 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalCtlCmdMax.h"
#include "LCETCS_vCalCtlCmdMax_private.h"

/* Output and update for referenced model: 'LCETCS_vCalCtlCmdMax' */
void LCETCS_vCalCtlCmdMax(const TypeETCSCrdnTrqStruct *rtu_ETCS_CRDN_TRQ, const
  TypeETCSDrvMdlStruct *rtu_ETCS_DRV_MDL, int32_T *rty_lcetcss32CtlCmdMax)
{
  /* Sum: '<Root>/Add1' incorporates:
   *  Constant: '<Root>/50Nm'
   *  Constant: '<Root>/Constant6'
   *  Product: '<Root>/Product'
   */
  *rty_lcetcss32CtlCmdMax = ((rtu_ETCS_DRV_MDL->lcetcss16TrqRatio * ((int8_T)
    S8ETCSCpCtlExitCntUpTh)) + rtu_ETCS_CRDN_TRQ->lcetcss32DrvReqCrdnTrq) + 500;

  /* Saturate: '<Root>/Saturation' */
  if ((*rty_lcetcss32CtlCmdMax) > 1500000) {
    *rty_lcetcss32CtlCmdMax = 1500000;
  } else {
    if ((*rty_lcetcss32CtlCmdMax) < -1000000) {
      *rty_lcetcss32CtlCmdMax = -1000000;
    }
  }

  /* End of Saturate: '<Root>/Saturation' */
}

/* Model initialize function */
void LCETCS_vCalCtlCmdMax_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
