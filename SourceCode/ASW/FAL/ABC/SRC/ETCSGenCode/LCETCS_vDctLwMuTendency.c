/*
 * File: LCETCS_vDctLwMuTendency.c
 *
 * Code generated for Simulink model 'LCETCS_vDctLwMuTendency'.
 *
 * Model version                  : 1.260
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:56:33 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctLwMuTendency.h"
#include "LCETCS_vDctLwMuTendency_private.h"

/* Initial conditions for referenced model: 'LCETCS_vDctLwMuTendency' */
void LCETCS_vDctLwMuTendency_Init(DW_LCETCS_vDctLwMuTendency_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = false;
}

/* Output and update for referenced model: 'LCETCS_vDctLwMuTendency' */
void LCETCS_vDctLwMuTendency(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const
  TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC, const TypeETCSAxlStruct *rtu_ETCS_FA,
  const TypeETCSAxlStruct *rtu_ETCS_RA, int16_T rtu_lcetcss16LwToSplitCnt,
  int16_T rtu_lcetcss16LowHmHldCnt, boolean_T *rty_lcetcsu1DctLowHm, boolean_T
  *rty_lcetcsu1StrtPintLowHm, DW_LCETCS_vDctLwMuTendency_f_T *localDW)
{
  boolean_T rtb_lcetcsu1DctLowHm;
  boolean_T rtb_lcetcsu1StrtPintLowHm;
  int32_T tmp;
  boolean_T tmp_0;

  /* If: '<Root>/If' incorporates:
   *  Constant: '<S2>/Constant'
   *  Constant: '<S2>/Constant1'
   */
  if (!rtu_ETCS_CTL_ACT->lcetcsu1CtlAct) {
    /* Outputs for IfAction SubSystem: '<Root>/No DctLwMuTendency' incorporates:
     *  ActionPort: '<S2>/Action Port'
     */
    *rty_lcetcsu1DctLowHm = false;
    *rty_lcetcsu1StrtPintLowHm = false;

    /* End of Outputs for SubSystem: '<Root>/No DctLwMuTendency' */
  } else {
    /* Outputs for IfAction SubSystem: '<Root>/DctLwMuTendency' incorporates:
     *  ActionPort: '<S1>/Action Port'
     */
    /* Chart: '<S1>/Chart' incorporates:
     *  UnitDelay: '<Root>/Unit Delay1'
     */
    /* Gateway: DctLwMuTendency/Chart */
    /* During: DctLwMuTendency/Chart */
    /* Entry Internal: DctLwMuTendency/Chart */
    /* Transition: '<S3>:142' */
    /* comment */
    if ((rtu_lcetcss16LowHmHldCnt < ((uint16_T)U16TCSCpDctLwHmMaxCnt)) &&
        (rtu_lcetcss16LwToSplitCnt < ((int16_T)S16ETCSCpLwToSpltDctCplCnt))) {
      /* Transition: '<S3>:211' */
      /* Transition: '<S3>:209' */
      tmp = rtu_ETCS_VEH_ACC->lcetcss16RefRdCfFricAtIce + ((uint8_T)
        U8TCSCpAddRefRdCfFrci);
      if (tmp > 32767) {
        tmp = 32767;
      }

      if (rtu_ETCS_VEH_ACC->lcetcss16RsltntAcc < tmp) {
        /* Switch: '<S1>/Switch3' incorporates:
         *  Constant: '<S1>/Constant1'
         */
        /* Transition: '<S3>:151' */
        /* Transition: '<S3>:153' */
        if (((boolean_T)VarFwd)) {
          tmp_0 = rtu_ETCS_FA->lcetcsu1SymSpinDctFlg;
        } else {
          tmp_0 = rtu_ETCS_RA->lcetcsu1SymSpinDctFlg;
        }

        /* End of Switch: '<S1>/Switch3' */

        /* Switch: '<S1>/Switch2' incorporates:
         *  Constant: '<S1>/Constant3'
         *  UnitDelay: '<S1>/Unit Delay1'
         *  UnitDelay: '<S1>/Unit Delay2'
         */
        if (((boolean_T)VarFwd)) {
          rtb_lcetcsu1DctLowHm = localDW->UnitDelay1_DSTATE_n;
        } else {
          rtb_lcetcsu1DctLowHm = localDW->UnitDelay2_DSTATE;
        }

        /* End of Switch: '<S1>/Switch2' */

        /* Switch: '<S1>/Switch1' incorporates:
         *  Constant: '<S1>/Constant2'
         */
        if (((boolean_T)VarFwd)) {
          rtb_lcetcsu1StrtPintLowHm = rtu_ETCS_FA->lcetcsu1HuntingDcted;
        } else {
          rtb_lcetcsu1StrtPintLowHm = rtu_ETCS_RA->lcetcsu1HuntingDcted;
        }

        /* End of Switch: '<S1>/Switch1' */
        if ((tmp_0 == 1) || ((rtb_lcetcsu1DctLowHm == 0) &&
                             (rtb_lcetcsu1StrtPintLowHm == 1))) {
          /* Transition: '<S3>:206' */
          /* Transition: '<S3>:207' */
          rtb_lcetcsu1DctLowHm = true;
          rtb_lcetcsu1StrtPintLowHm = true;

          /* Transition: '<S3>:218' */
        } else {
          /* Transition: '<S3>:217' */
          rtb_lcetcsu1DctLowHm = localDW->UnitDelay1_DSTATE;
          rtb_lcetcsu1StrtPintLowHm = false;
        }

        /* Transition: '<S3>:219' */
      } else {
        /* Transition: '<S3>:215' */
        rtb_lcetcsu1DctLowHm = localDW->UnitDelay1_DSTATE;
        rtb_lcetcsu1StrtPintLowHm = false;
      }

      /* Transition: '<S3>:220' */
    } else {
      /* Transition: '<S3>:213' */
      rtb_lcetcsu1DctLowHm = false;
      rtb_lcetcsu1StrtPintLowHm = false;
    }

    /* End of Chart: '<S1>/Chart' */

    /* SignalConversion: '<S1>/OutportBufferForlcetcsu1DctLowHm' */
    /* Transition: '<S3>:222' */
    *rty_lcetcsu1DctLowHm = rtb_lcetcsu1DctLowHm;

    /* SignalConversion: '<S1>/OutportBufferForlcetcsu1StrtPintLowHm' */
    *rty_lcetcsu1StrtPintLowHm = rtb_lcetcsu1StrtPintLowHm;

    /* Update for UnitDelay: '<S1>/Unit Delay1' */
    localDW->UnitDelay1_DSTATE_n = rtu_ETCS_FA->lcetcsu1HuntingDcted;

    /* Update for UnitDelay: '<S1>/Unit Delay2' */
    localDW->UnitDelay2_DSTATE = rtu_ETCS_RA->lcetcsu1HuntingDcted;

    /* End of Outputs for SubSystem: '<Root>/DctLwMuTendency' */
  }

  /* End of If: '<Root>/If' */

  /* Update for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = *rty_lcetcsu1DctLowHm;
}

/* Model initialize function */
void LCETCS_vDctLwMuTendency_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
