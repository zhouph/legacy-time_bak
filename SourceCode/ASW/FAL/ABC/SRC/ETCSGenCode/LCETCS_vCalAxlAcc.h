/*
 * File: LCETCS_vCalAxlAcc.h
 *
 * Code generated for Simulink model 'LCETCS_vCalAxlAcc'.
 *
 * Model version                  : 1.68
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:47:03 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalAxlAcc_h_
#define RTW_HEADER_LCETCS_vCalAxlAcc_h_
#ifndef LCETCS_vCalAxlAcc_COMMON_INCLUDES_
# define LCETCS_vCalAxlAcc_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalAxlAcc_COMMON_INCLUDES_ */

#include "LCETCS_vCalAxlAcc_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Block states (auto storage) for model 'LCETCS_vCalAxlAcc' */
typedef struct {
  uint32_T CircBufIdx;                 /* '<Root>/Delay' */
  uint32_T CircBufIdx_k;               /* '<Root>/Delay1' */
  int16_T Delay_DSTATE[9];             /* '<Root>/Delay' */
  int16_T Delay1_DSTATE[9];            /* '<Root>/Delay1' */
} DW_LCETCS_vCalAxlAcc_f_T;

typedef struct {
  DW_LCETCS_vCalAxlAcc_f_T rtdw;
} MdlrefDW_LCETCS_vCalAxlAcc_T;

/* Model reference registration function */
extern void LCETCS_vCalAxlAcc_initialize(void);
extern void LCETCS_vCalAxlAcc_Init(DW_LCETCS_vCalAxlAcc_f_T *localDW);
extern void LCETCS_vCalAxlAcc(const TypeETCSWhlStruct *rtu_ETCS_FL, const
  TypeETCSWhlStruct *rtu_ETCS_FR, const TypeETCSWhlStruct *rtu_ETCS_RL, const
  TypeETCSWhlStruct *rtu_ETCS_RR, const TypeETCSExtDctStruct *rtu_ETCS_EXT_DCT,
  TypeETCSAxlAccStruct *rty_ETCS_AXL_ACC, DW_LCETCS_vCalAxlAcc_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalAxlAcc'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalAxlAcc_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
