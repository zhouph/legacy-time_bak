/*
 * File: LCETCS_vCalRefTrq2Jmp.c
 *
 * Code generated for Simulink model 'LCETCS_vCalRefTrq2Jmp'.
 *
 * Model version                  : 1.220
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:54:35 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalRefTrq2Jmp.h"
#include "LCETCS_vCalRefTrq2Jmp_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalRefTrq2Jmp' */
void LCETCS_vCalRefTrq2Jmp_Init(int32_T *rty_lcetcss32RefTrq2Jmp, boolean_T
  *rty_lcetcsu1RefTrq4JmpSetOk, int16_T *rty_lcetcss16RefTrqSetTm)
{
  /* InitializeConditions for Chart: '<Root>/CalRefTrq2Jmp' */
  *rty_lcetcss32RefTrq2Jmp = 0;
  *rty_lcetcsu1RefTrq4JmpSetOk = false;
  *rty_lcetcss16RefTrqSetTm = 0;
}

/* Output and update for referenced model: 'LCETCS_vCalRefTrq2Jmp' */
void LCETCS_vCalRefTrq2Jmp(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const
  TypeETCSErrZroCrsStruct *rtu_ETCS_ERR_ZRO_CRS, const TypeETCSCtlCmdStruct
  *rtu_ETCS_CTL_CMD_OLD, const TypeETCSH2LStruct *rtu_ETCS_H2L, const
  TypeETCSRefTrq2JmpStruct *rtu_ETCS_REF_TRQ2JMP_OLD, int32_T
  *rty_lcetcss32RefTrq2Jmp, boolean_T *rty_lcetcsu1RefTrq4JmpSetOk, int16_T
  *rty_lcetcss16RefTrqSetTm)
{
  int32_T tmp;

  /* Chart: '<Root>/CalRefTrq2Jmp' */
  /* Gateway: CalRefTrq2Jmp */
  /* During: CalRefTrq2Jmp */
  /* Entry Internal: CalRefTrq2Jmp */
  /* Transition: '<S1>:9' */
  /* comment */
  if (rtu_ETCS_CTL_ACT->lcetcsu1CtlAct == 1) {
    /* Transition: '<S1>:10' */
    /* Transition: '<S1>:18' */
    if ((rtu_ETCS_ERR_ZRO_CRS->lcetcsu1JumpUpFalEdg == 1) ||
        (rtu_ETCS_ERR_ZRO_CRS->lcetcsu1JumpDnFalEdg == 1)) {
      /* Transition: '<S1>:11' */
      /* Transition: '<S1>:16' */
      *rty_lcetcss16RefTrqSetTm = 0;
      if (rtu_ETCS_REF_TRQ2JMP_OLD->lcetcsu1RefTrq4JmpSetOk == 0) {
        /* Transition: '<S1>:58' */
        /* Transition: '<S1>:62' */
        *rty_lcetcss32RefTrq2Jmp = rtu_ETCS_CTL_CMD_OLD->lcetcss32CardanTrqCmd;
        *rty_lcetcsu1RefTrq4JmpSetOk = true;

        /* Transition: '<S1>:61' */
      } else {
        /* Transition: '<S1>:59' */
        *rty_lcetcss32RefTrq2Jmp = rtu_ETCS_CTL_CMD_OLD->lcetcss32CardanTrqCmd;
        *rty_lcetcsu1RefTrq4JmpSetOk =
          rtu_ETCS_REF_TRQ2JMP_OLD->lcetcsu1RefTrq4JmpSetOk;
      }

      /* Transition: '<S1>:52' */
      /* Transition: '<S1>:48' */
    } else {
      /* Transition: '<S1>:17' */
      *rty_lcetcsu1RefTrq4JmpSetOk =
        rtu_ETCS_REF_TRQ2JMP_OLD->lcetcsu1RefTrq4JmpSetOk;
      if (rtu_ETCS_H2L->lcetcsu1HiToLw == 1) {
        /* Transition: '<S1>:46' */
        /* Transition: '<S1>:103' */
        *rty_lcetcss16RefTrqSetTm =
          rtu_ETCS_REF_TRQ2JMP_OLD->lcetcss16RefTrqSetTm;
        *rty_lcetcss32RefTrq2Jmp = rtu_ETCS_CTL_CMD_OLD->lcetcss32CardanTrqCmd;

        /* Transition: '<S1>:48' */
      } else {
        /* Transition: '<S1>:49' */
        tmp = rtu_ETCS_REF_TRQ2JMP_OLD->lcetcss16RefTrqSetTm + 1;
        if (tmp > 32767) {
          tmp = 32767;
        }

        *rty_lcetcss16RefTrqSetTm = (int16_T)tmp;
        *rty_lcetcss32RefTrq2Jmp = rtu_ETCS_REF_TRQ2JMP_OLD->lcetcss32RefTrq2Jmp;
      }
    }

    /* Transition: '<S1>:14' */
  } else {
    /* Transition: '<S1>:13' */
    *rty_lcetcss16RefTrqSetTm = 0;
    *rty_lcetcss32RefTrq2Jmp = 0;
    *rty_lcetcsu1RefTrq4JmpSetOk = false;
  }

  /* End of Chart: '<Root>/CalRefTrq2Jmp' */
  /* Transition: '<S1>:12' */
}

/* Model initialize function */
void LCETCS_vCalRefTrq2Jmp_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
