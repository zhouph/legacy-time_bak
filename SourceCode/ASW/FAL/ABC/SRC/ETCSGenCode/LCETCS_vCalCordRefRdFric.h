/*
 * File: LCETCS_vCalCordRefRdFric.h
 *
 * Code generated for Simulink model 'LCETCS_vCalCordRefRdFric'.
 *
 * Model version                  : 1.46
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:50:11 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalCordRefRdFric_h_
#define RTW_HEADER_LCETCS_vCalCordRefRdFric_h_
#ifndef LCETCS_vCalCordRefRdFric_COMMON_INCLUDES_
# define LCETCS_vCalCordRefRdFric_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalCordRefRdFric_COMMON_INCLUDES_ */

#include "LCETCS_vCalCordRefRdFric_types.h"

/* Shared type includes */
#include "model_reference_types.h"
#include "rt_sqrts16__s.h"

/* Model reference registration function */
extern void LCETCS_vCalCordRefRdFric_initialize(void);
extern void LCETCS_vCalCordRefRdFric_Init(int16_T *rty_lcetcss16RefRdCfFricAtIce,
  int16_T *rty_lcetcss16RefRdCfFricAtSnow, int16_T
  *rty_lcetcss16RefRdCfFricAtAsphalt);
extern void LCETCS_vCalCordRefRdFric(int16_T rtu_lcetcss16ScaleDnAy, int16_T
  rtu_lcetcss16RefAxRdFricAtIce, int16_T rtu_lcetcss16RefAxRdFricAtSnow, int16_T
  rtu_lcetcss16RefAxRdFricAtAsphalt, int16_T *rty_lcetcss16RefRdCfFricAtIce,
  int16_T *rty_lcetcss16RefRdCfFricAtSnow, int16_T
  *rty_lcetcss16RefRdCfFricAtAsphalt);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalCordRefRdFric'
 * '<S1>'   : 'LCETCS_vCalCordRefRdFric/Chart'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalCordRefRdFric_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
