/*
 * File: LCETCS_vCalAddTar4DepSnw_private.h
 *
 * Code generated for Simulink model 'LCETCS_vCalAddTar4DepSnw'.
 *
 * Model version                  : 1.61
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:59:23 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalAddTar4DepSnw_private_h_
#define RTW_HEADER_LCETCS_vCalAddTar4DepSnw_private_h_
#include "rtwtypes.h"
#include "model_reference_types.h"

/* Includes for objects with custom storage classes. */
#include "../ETCSGenCodeHeader/ETCSHeader4Import.h"

/*
 * Generate compile time checks that imported macros for parameters
 * with storage class "ImportedDefine" are defined
 */
#ifndef U8ETCSCpAlwAddTar4DepSnwSpd
#error The variable for the parameter "U8ETCSCpAlwAddTar4DepSnwSpd" is not defined
#endif

#ifndef U8ETCSCpNAlwAddTar4DepSnwSpd
#error The variable for the parameter "U8ETCSCpNAlwAddTar4DepSnwSpd" is not defined
#endif

#ifndef U8ETCSCpTarSpnAdd4DepSnwHil
#error The variable for the parameter "U8ETCSCpTarSpnAdd4DepSnwHil" is not defined
#endif

extern const int16_T rtCP_pooled_ZjYULP70HcKf;

#define rtCP_Constant3_Value           rtCP_pooled_ZjYULP70HcKf  /* Computed Parameter: Constant3_Value
                                                                  * Referenced by: '<Root>/Constant3'
                                                                  */
#endif                                 /* RTW_HEADER_LCETCS_vCalAddTar4DepSnw_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
