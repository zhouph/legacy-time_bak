/*
 * File: LCETCS_vCalTrqRatio.c
 *
 * Code generated for Simulink model 'LCETCS_vCalTrqRatio'.
 *
 * Model version                  : 1.68
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:47:44 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalTrqRatio.h"
#include "LCETCS_vCalTrqRatio_private.h"

/* Output and update for referenced model: 'LCETCS_vCalTrqRatio' */
void LCETCS_vCalTrqRatio(int16_T rtu_lcetcss16TotalGearRatio, int16_T
  rtu_lcetcss16TransEfficiency, int16_T *rty_lcetcss16TrqRatio)
{
  /* Saturate: '<Root>/Saturation' incorporates:
   *  Constant: '<Root>/Constant'
   *  Product: '<Root>/Divide'
   *  Product: '<Root>/Product'
   */
  *rty_lcetcss16TrqRatio = (int16_T)((rtu_lcetcss16TotalGearRatio *
    rtu_lcetcss16TransEfficiency) / 100);
  if ((*rty_lcetcss16TrqRatio) < 10) {
    *rty_lcetcss16TrqRatio = 10;
  }

  /* End of Saturate: '<Root>/Saturation' */
}

/* Model initialize function */
void LCETCS_vCalTrqRatio_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
