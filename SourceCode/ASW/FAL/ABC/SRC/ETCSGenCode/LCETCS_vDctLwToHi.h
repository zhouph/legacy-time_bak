/*
 * File: LCETCS_vDctLwToHi.h
 *
 * Code generated for Simulink model 'LCETCS_vDctLwToHi'.
 *
 * Model version                  : 1.165
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:06:08 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vDctLwToHi_h_
#define RTW_HEADER_LCETCS_vDctLwToHi_h_
#ifndef LCETCS_vDctLwToHi_COMMON_INCLUDES_
# define LCETCS_vDctLwToHi_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vDctLwToHi_COMMON_INCLUDES_ */

#include "LCETCS_vDctLwToHi_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vDctLwToHibyDecel.h"
#include "LCETCS_vDctLwToHibyArea.h"

/* Block states (auto storage) for model 'LCETCS_vDctLwToHi' */
typedef struct {
  MdlrefDW_LCETCS_vDctLwToHibyArea_T LCETCS_vDctLwToHibyArea_DWORK1;/* '<Root>/LCETCS_vDctLwToHibyArea' */
  MdlrefDW_LCETCS_vDctLwToHibyDecel_T LCETCS_vDctLwToHibyDecel_DWORK1;/* '<Root>/LCETCS_vDctLwToHibyDecel' */
} DW_LCETCS_vDctLwToHi_f_T;

typedef struct {
  DW_LCETCS_vDctLwToHi_f_T rtdw;
} MdlrefDW_LCETCS_vDctLwToHi_T;

/* Model reference registration function */
extern void LCETCS_vDctLwToHi_initialize(void);
extern void LCETCS_vDctLwToHi_Init(DW_LCETCS_vDctLwToHi_f_T *localDW);
extern void LCETCS_vDctLwToHi(const TypeETCSWhlAccStruct *rtu_ETCS_WHL_ACC,
  const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const TypeETCSWhlSpinStruct
  *rtu_ETCS_WHL_SPIN, const TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN, int16_T
  *rty_lcetcss16LwToHiCnt, int16_T *rty_lcetcss16LwToHiCntbyArea,
  DW_LCETCS_vDctLwToHi_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vDctLwToHi'
 */
#endif                                 /* RTW_HEADER_LCETCS_vDctLwToHi_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
