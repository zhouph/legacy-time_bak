/*
 * File: LCETCS_vDctEngStlEnterExitCond.c
 *
 * Code generated for Simulink model 'LCETCS_vDctEngStlEnterExitCond'.
 *
 * Model version                  : 1.283
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:55:35 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctEngStlEnterExitCond.h"
#include "LCETCS_vDctEngStlEnterExitCond_private.h"

/* Initial conditions for referenced model: 'LCETCS_vDctEngStlEnterExitCond' */
void LCETCS_vDctEngStlEnterExitCond_Init(boolean_T *rty_lcetcsu1EngStallTemp,
  DW_LCETCS_vDctEngStlEnterExitCond_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay6' */
  localDW->UnitDelay6_DSTATE = false;

  /* InitializeConditions for Chart: '<Root>/Chart' */
  *rty_lcetcsu1EngStallTemp = false;
}

/* Output and update for referenced model: 'LCETCS_vDctEngStlEnterExitCond' */
void LCETCS_vDctEngStlEnterExitCond(int16_T rtu_lcetcss16EngStallThr, const
  TypeETCSEngStruct *rtu_ETCS_ENG_STRUCT, int16_T rtu_lcetcss16EngSpdSlope,
  boolean_T *rty_lcetcsu1EngStallTemp, DW_LCETCS_vDctEngStlEnterExitCond_f_T
  *localDW)
{
  /* Chart: '<Root>/Chart' incorporates:
   *  UnitDelay: '<Root>/Unit Delay6'
   */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:12' */
  /* comment */
  if ((rtu_ETCS_ENG_STRUCT->lcetcss16EngSpd < rtu_lcetcss16EngStallThr) &&
      (rtu_lcetcss16EngSpdSlope <= 0)) {
    /* Transition: '<S1>:14' */
    /* Transition: '<S1>:19' */
    *rty_lcetcsu1EngStallTemp = true;

    /* Transition: '<S1>:18' */
    /* Transition: '<S1>:36' */
  } else {
    /* Transition: '<S1>:13' */
    if ((rtu_ETCS_ENG_STRUCT->lcetcss16EngSpd > (rtu_lcetcss16EngStallThr +
          ((uint8_T)U8ETCSCpEngStallRpmOffset))) && (rtu_lcetcss16EngSpdSlope >=
         0)) {
      /* Transition: '<S1>:21' */
      /* Transition: '<S1>:20' */
      *rty_lcetcsu1EngStallTemp = false;

      /* Transition: '<S1>:36' */
    } else {
      /* Transition: '<S1>:35' */
      *rty_lcetcsu1EngStallTemp = localDW->UnitDelay6_DSTATE;
    }
  }

  /* End of Chart: '<Root>/Chart' */

  /* Update for UnitDelay: '<Root>/Unit Delay6' */
  /* Transition: '<S1>:15' */
  localDW->UnitDelay6_DSTATE = *rty_lcetcsu1EngStallTemp;
}

/* Model initialize function */
void LCETCS_vDctEngStlEnterExitCond_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
