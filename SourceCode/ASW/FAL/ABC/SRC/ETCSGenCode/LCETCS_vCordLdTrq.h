/*
 * File: LCETCS_vCordLdTrq.h
 *
 * Code generated for Simulink model 'LCETCS_vCordLdTrq'.
 *
 * Model version                  : 1.206
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:48:21 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCordLdTrq_h_
#define RTW_HEADER_LCETCS_vCordLdTrq_h_
#ifndef LCETCS_vCordLdTrq_COMMON_INCLUDES_
# define LCETCS_vCordLdTrq_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCordLdTrq_COMMON_INCLUDES_ */

#include "LCETCS_vCordLdTrq_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Model reference registration function */
extern void LCETCS_vCordLdTrq_initialize(void);
extern void LCETCS_vCordLdTrq(int16_T rtu_lcetcss16LdTrqByGrd, int32_T
  rtu_lcetcss32LdTrqByAsymBrkCtl, int32_T *rty_lcetcss32LdTrq);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCordLdTrq'
 * '<S1>'   : 'LCETCS_vCordLdTrq/DocBlock'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCordLdTrq_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
