/*
 * File: LCETCS_vCalTarWhlSpnIncHiRd_private.h
 *
 * Code generated for Simulink model 'LCETCS_vCalTarWhlSpnIncHiRd'.
 *
 * Model version                  : 1.135
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:00:14 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalTarWhlSpnIncHiRd_private_h_
#define RTW_HEADER_LCETCS_vCalTarWhlSpnIncHiRd_private_h_
#include "rtwtypes.h"
#include "model_reference_types.h"

/* Includes for objects with custom storage classes. */
#include "../ETCSGenCodeHeader/ETCSHeader4Import.h"

/*
 * Generate compile time checks that imported macros for parameters
 * with storage class "ImportedDefine" are defined
 */
#ifndef S16ETCSCpPreHighRdDctCplCnt
#error The variable for the parameter "S16ETCSCpPreHighRdDctCplCnt" is not defined
#endif

#ifndef S16ETCSCpPreHighRdDctStrtCnt
#error The variable for the parameter "S16ETCSCpPreHighRdDctStrtCnt" is not defined
#endif

#ifndef U8ETCSCpAdaptHighRdCplCnt
#error The variable for the parameter "U8ETCSCpAdaptHighRdCplCnt" is not defined
#endif

#ifndef U8ETCSCpAdaptHighRdStrtCnt
#error The variable for the parameter "U8ETCSCpAdaptHighRdStrtCnt" is not defined
#endif

#ifndef U8ETCSCpTarSpnAdd4AdHiRd
#error The variable for the parameter "U8ETCSCpTarSpnAdd4AdHiRd" is not defined
#endif

#ifndef U8ETCSCpTarSpnAdd4PreHiRd
#error The variable for the parameter "U8ETCSCpTarSpnAdd4PreHiRd" is not defined
#endif
#endif                                 /* RTW_HEADER_LCETCS_vCalTarWhlSpnIncHiRd_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
