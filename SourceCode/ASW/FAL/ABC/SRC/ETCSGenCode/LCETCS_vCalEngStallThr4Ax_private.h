/*
 * File: LCETCS_vCalEngStallThr4Ax_private.h
 *
 * Code generated for Simulink model 'LCETCS_vCalEngStallThr4Ax'.
 *
 * Model version                  : 1.236
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:10:09 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalEngStallThr4Ax_private_h_
#define RTW_HEADER_LCETCS_vCalEngStallThr4Ax_private_h_
#include "rtwtypes.h"
#include "model_reference_types.h"

/* Includes for objects with custom storage classes. */
#include "../ETCSGenCodeHeader/ETCSHeader4Import.h"

/*
 * Generate compile time checks that imported macros for parameters
 * with storage class "ImportedDefine" are defined
 */
#ifndef S16ETCSCpHighRiskStallRpmFlat
#error The variable for the parameter "S16ETCSCpHighRiskStallRpmFlat" is not defined
#endif

#ifndef S16ETCSCpHighRiskStallRpmHill
#error The variable for the parameter "S16ETCSCpHighRiskStallRpmHill" is not defined
#endif

#ifndef S16ETCSCpLowRiskStallRpmFlat
#error The variable for the parameter "S16ETCSCpLowRiskStallRpmFlat" is not defined
#endif

#ifndef S16ETCSCpLowRiskStallRpmHill
#error The variable for the parameter "S16ETCSCpLowRiskStallRpmHill" is not defined
#endif

#ifndef S16ETCSCpMidRiskStallRpmFlat
#error The variable for the parameter "S16ETCSCpMidRiskStallRpmFlat" is not defined
#endif

#ifndef S16ETCSCpMidRiskStallRpmHill
#error The variable for the parameter "S16ETCSCpMidRiskStallRpmHill" is not defined
#endif

#ifndef VREF_10_KPH
#error The variable for the parameter "VREF_10_KPH" is not defined
#endif

#ifndef VREF_12_5_KPH
#error The variable for the parameter "VREF_12_5_KPH" is not defined
#endif

#ifndef VREF_6_KPH
#error The variable for the parameter "VREF_6_KPH" is not defined
#endif
#endif                                 /* RTW_HEADER_LCETCS_vCalEngStallThr4Ax_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
