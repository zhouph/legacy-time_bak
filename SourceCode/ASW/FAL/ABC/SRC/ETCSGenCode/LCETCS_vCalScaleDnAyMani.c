/*
 * File: LCETCS_vCalScaleDnAyMani.c
 *
 * Code generated for Simulink model 'LCETCS_vCalScaleDnAyMani'.
 *
 * Model version                  : 1.16
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:04:26 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalScaleDnAyMani.h"
#include "LCETCS_vCalScaleDnAyMani_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalScaleDnAyMani' */
void LCETCS_vCalScaleDnAyMani_Init(B_LCETCS_vCalScaleDnAyMani_c_T *localB)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter3Point' */
  LCETCS_s16Inter3Point_Init(&localB->y);
}

/* Start for referenced model: 'LCETCS_vCalScaleDnAyMani' */
void LCETCS_vCalScaleDnAyMani_Start(B_LCETCS_vCalScaleDnAyMani_c_T *localB)
{
  /* Start for Constant: '<Root>/Ax on asphalt' */
  localB->y3 = ((uint8_T)U8ETCSCpAxAtAsphalt);

  /* Start for Constant: '<Root>/Ax on ice' */
  localB->y1 = ((uint8_T)U8ETCSCpAxAtIce);

  /* Start for Constant: '<Root>/Ax on snow' */
  localB->y2 = ((uint8_T)U8ETCSCpAxAtSnow);

  /* Start for Constant: '<Root>/Ay on asphalt' */
  localB->x3 = ((uint8_T)U8ETCSCpAyAsp);

  /* Start for Constant: '<Root>/Ay on ice' */
  localB->x1 = ((uint8_T)U8ETCSCpAyIce);

  /* Start for Constant: '<Root>/Ay on snow' */
  localB->x2 = ((uint8_T)U8ETCSCpAySnw);
}

/* Output and update for referenced model: 'LCETCS_vCalScaleDnAyMani' */
void LCETCS_vCalScaleDnAyMani(const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG,
  int16_T *rty_lcetcss16ScaleDnAyMani, B_LCETCS_vCalScaleDnAyMani_c_T *localB)
{
  /* local block i/o variables */
  int16_T rtb_x;

  /* Abs: '<Root>/Abs' */
  if (rtu_ETCS_ESC_SIG->lcetcss16LatAccMsrdMani < 0) {
    rtb_x = (int16_T)(-rtu_ETCS_ESC_SIG->lcetcss16LatAccMsrdMani);
  } else {
    rtb_x = rtu_ETCS_ESC_SIG->lcetcss16LatAccMsrdMani;
  }

  /* End of Abs: '<Root>/Abs' */

  /* Constant: '<Root>/Ax on asphalt' */
  localB->y3 = ((uint8_T)U8ETCSCpAxAtAsphalt);

  /* Constant: '<Root>/Ax on ice' */
  localB->y1 = ((uint8_T)U8ETCSCpAxAtIce);

  /* Constant: '<Root>/Ax on snow' */
  localB->y2 = ((uint8_T)U8ETCSCpAxAtSnow);

  /* Constant: '<Root>/Ay on asphalt' */
  localB->x3 = ((uint8_T)U8ETCSCpAyAsp);

  /* Constant: '<Root>/Ay on ice' */
  localB->x1 = ((uint8_T)U8ETCSCpAyIce);

  /* Constant: '<Root>/Ay on snow' */
  localB->x2 = ((uint8_T)U8ETCSCpAySnw);

  /* ModelReference: '<Root>/LCETCS_s16Inter3Point' */
  LCETCS_s16Inter3Point(rtb_x, localB->x1, localB->x2, localB->x3, localB->y1,
                        localB->y2, localB->y3, &localB->y);

  /* Saturate: '<Root>/Saturation' */
  if (localB->y > 125) {
    *rty_lcetcss16ScaleDnAyMani = 125;
  } else if (localB->y < 0) {
    *rty_lcetcss16ScaleDnAyMani = 0;
  } else {
    *rty_lcetcss16ScaleDnAyMani = localB->y;
  }

  /* End of Saturate: '<Root>/Saturation' */
}

/* Model initialize function */
void LCETCS_vCalScaleDnAyMani_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter3Point' */
  LCETCS_s16Inter3Point_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
