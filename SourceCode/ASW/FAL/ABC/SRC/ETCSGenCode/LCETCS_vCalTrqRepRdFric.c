/*
 * File: LCETCS_vCalTrqRepRdFric.c
 *
 * Code generated for Simulink model 'LCETCS_vCalTrqRepRdFric'.
 *
 * Model version                  : 1.125
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:12:27 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalTrqRepRdFric.h"
#include "LCETCS_vCalTrqRepRdFric_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalTrqRepRdFric' */
void LCETCS_vCalTrqRepRdFric_Init(B_LCETCS_vCalTrqRepRdFric_c_T *localB,
  DW_LCETCS_vCalTrqRepRdFric_f_T *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalAppTrq4RdFric' */
  LCETCS_vCalAppTrq4RdFric_Init(&localB->lcetcss32AppTrq4RdFric,
    &(localDW->LCETCS_vCalAppTrq4RdFric_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalLesTrq4RdFric' */
  LCETCS_vCalLesTrq4RdFric_Init(&localB->lcetcss32LesTrq4RdFric,
    &localB->lcetcss32TrqAtLowSpnDcted,
    &(localDW->LCETCS_vCalLesTrq4RdFric_DWORK1.rtdw));
}

/* Start for referenced model: 'LCETCS_vCalTrqRepRdFric' */
void LCETCS_vCalTrqRepRdFric_Start(DW_LCETCS_vCalTrqRepRdFric_f_T *localDW)
{
  /* Start for ModelReference: '<Root>/LCETCS_vCalAppTrq4RdFric' */
  LCETCS_vCalAppTrq4RdFric_Start(&(localDW->LCETCS_vCalAppTrq4RdFric_DWORK1.rtdw));
}

/* Output and update for referenced model: 'LCETCS_vCalTrqRepRdFric' */
void LCETCS_vCalTrqRepRdFric(const TypeETCSCtlCmdStruct *rtu_ETCS_CTL_CMD_OLD,
  const TypeETCSCyl1stStruct *rtu_ETCS_CYL_1ST, const TypeETCSVehAccStruct
  *rtu_ETCS_VEH_ACC, const TypeETCSLowWhlSpnStruct *rtu_ETCS_LOW_WHL_SPN, const
  TypeETCSH2LStruct *rtu_ETCS_H2L, TypeETCSTrq4RdFricStruct
  *rty_ETCS_TRQ_REP_RD_FRIC, B_LCETCS_vCalTrqRepRdFric_c_T *localB,
  DW_LCETCS_vCalTrqRepRdFric_f_T *localDW)
{
  /* local block i/o variables */
  int32_T rtb_lcetcss32TrqRepRdFric;

  /* ModelReference: '<Root>/LCETCS_vCalAppTrq4RdFric' */
  LCETCS_vCalAppTrq4RdFric(rtu_ETCS_CYL_1ST, rtu_ETCS_VEH_ACC,
    rtu_ETCS_CTL_CMD_OLD, rtu_ETCS_H2L, &localB->lcetcss32AppTrq4RdFric,
    &(localDW->LCETCS_vCalAppTrq4RdFric_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalLesTrq4RdFric' */
  LCETCS_vCalLesTrq4RdFric(localB->lcetcss32AppTrq4RdFric, rtu_ETCS_LOW_WHL_SPN,
    &localB->lcetcss32LesTrq4RdFric, &localB->lcetcss32TrqAtLowSpnDcted,
    &(localDW->LCETCS_vCalLesTrq4RdFric_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCordTrqRepRdFric' */
  LCETCS_vCordTrqRepRdFric(localB->lcetcss32AppTrq4RdFric,
    localB->lcetcss32LesTrq4RdFric, &rtb_lcetcss32TrqRepRdFric);

  /* SignalConversion: '<Root>/Signal Conversion' */
  rty_ETCS_TRQ_REP_RD_FRIC->lcetcss32TrqRepRdFric = rtb_lcetcss32TrqRepRdFric;

  /* SignalConversion: '<Root>/Signal Conversion1' */
  rty_ETCS_TRQ_REP_RD_FRIC->lcetcss32AppTrq4RdFric =
    localB->lcetcss32AppTrq4RdFric;

  /* SignalConversion: '<Root>/Signal Conversion2' */
  rty_ETCS_TRQ_REP_RD_FRIC->lcetcss32LesTrq4RdFric =
    localB->lcetcss32LesTrq4RdFric;

  /* SignalConversion: '<Root>/Signal Conversion3' */
  rty_ETCS_TRQ_REP_RD_FRIC->lcetcss32TrqAtLowSpnDcted =
    localB->lcetcss32TrqAtLowSpnDcted;
}

/* Model initialize function */
void LCETCS_vCalTrqRepRdFric_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalAppTrq4RdFric' */
  LCETCS_vCalAppTrq4RdFric_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalLesTrq4RdFric' */
  LCETCS_vCalLesTrq4RdFric_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCordTrqRepRdFric' */
  LCETCS_vCordTrqRepRdFric_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
