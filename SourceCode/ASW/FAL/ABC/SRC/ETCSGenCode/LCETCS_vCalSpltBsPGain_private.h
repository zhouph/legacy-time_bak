/*
 * File: LCETCS_vCalSpltBsPGain_private.h
 *
 * Code generated for Simulink model 'LCETCS_vCalSpltBsPGain'.
 *
 * Model version                  : 1.164
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:02:42 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalSpltBsPGain_private_h_
#define RTW_HEADER_LCETCS_vCalSpltBsPGain_private_h_
#include "rtwtypes.h"
#include "model_reference_types.h"

/* Includes for objects with custom storage classes. */
#include "../ETCSGenCodeHeader/ETCSHeader4Import.h"

/*
 * Generate compile time checks that imported macros for parameters
 * with storage class "ImportedDefine" are defined
 */
#ifndef S16ETCSCpTotalGearRatio_1
#error The variable for the parameter "S16ETCSCpTotalGearRatio_1" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_2
#error The variable for the parameter "S16ETCSCpTotalGearRatio_2" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_3
#error The variable for the parameter "S16ETCSCpTotalGearRatio_3" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_4
#error The variable for the parameter "S16ETCSCpTotalGearRatio_4" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_5
#error The variable for the parameter "S16ETCSCpTotalGearRatio_5" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSplt_1
#error The variable for the parameter "U8ETCSCpNegErrPgainSplt_1" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSplt_2
#error The variable for the parameter "U8ETCSCpNegErrPgainSplt_2" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSplt_3
#error The variable for the parameter "U8ETCSCpNegErrPgainSplt_3" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSplt_4
#error The variable for the parameter "U8ETCSCpNegErrPgainSplt_4" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSplt_5
#error The variable for the parameter "U8ETCSCpNegErrPgainSplt_5" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSplt_1
#error The variable for the parameter "U8ETCSCpPosErrPgainSplt_1" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSplt_2
#error The variable for the parameter "U8ETCSCpPosErrPgainSplt_2" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSplt_3
#error The variable for the parameter "U8ETCSCpPosErrPgainSplt_3" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSplt_4
#error The variable for the parameter "U8ETCSCpPosErrPgainSplt_4" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSplt_5
#error The variable for the parameter "U8ETCSCpPosErrPgainSplt_5" is not defined
#endif
#endif                                 /* RTW_HEADER_LCETCS_vCalSpltBsPGain_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
