/*
 * File: LCETCS_vCalLmtCrngDltYaw.h
 *
 * Code generated for Simulink model 'LCETCS_vCalLmtCrngDltYaw'.
 *
 * Model version                  : 1.66
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:59:49 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalLmtCrngDltYaw_h_
#define RTW_HEADER_LCETCS_vCalLmtCrngDltYaw_h_
#ifndef LCETCS_vCalLmtCrngDltYaw_COMMON_INCLUDES_
# define LCETCS_vCalLmtCrngDltYaw_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalLmtCrngDltYaw_COMMON_INCLUDES_ */

#include "LCETCS_vCalLmtCrngDltYaw_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_s16Inter3Point.h"
#include "LCETCS_s16Inter2Point.h"

/* Block signals for model 'LCETCS_vCalLmtCrngDltYaw' */
typedef struct {
  int16_T y1;                          /* '<Root>/Delta yaw which is not changing target spin when the speed is below x1' */
  int16_T y2;                          /* '<Root>/Delta yaw which is not changing target spin when the speed is below x2' */
  int16_T y3;                          /* '<Root>/Delta yaw which is not changing target spin when the speed is below x3' */
  int16_T y;                           /* '<Root>/LCETCS_s16Inter3Point' */
  int16_T y_p;                         /* '<Root>/LCETCS_s16Inter2Point' */
  int16_T y2_d;                        /* '<Root>/3Delta yaw which is not changing target spin when the speed is below x5' */
  int16_T y1_l;                        /* '<Root>/Delta yaw which is not changing target spin when the speed is below x4' */
} B_LCETCS_vCalLmtCrngDltYaw_c_T;

typedef struct {
  B_LCETCS_vCalLmtCrngDltYaw_c_T rtb;
} MdlrefDW_LCETCS_vCalLmtCrngDltYaw_T;

/* Model reference registration function */
extern void LCETCS_vCalLmtCrngDltYaw_initialize(void);
extern void LCETCS_vCalLmtCrngDltYaw_Init(B_LCETCS_vCalLmtCrngDltYaw_c_T *localB);
extern void LCETCS_vCalLmtCrngDltYaw_Start(B_LCETCS_vCalLmtCrngDltYaw_c_T
  *localB);
extern void LCETCS_vCalLmtCrngDltYaw(int16_T rtu_lcetcss16VehSpd, const
  TypeETCSAxlStruct *rtu_ETCS_FA, const TypeETCSAxlStruct *rtu_ETCS_RA, int16_T *
  rty_lcetcss16LmtCrngDltYaw, B_LCETCS_vCalLmtCrngDltYaw_c_T *localB);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalLmtCrngDltYaw'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalLmtCrngDltYaw_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
