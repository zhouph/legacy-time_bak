/*
 * File: LCETCS_vCalIValAtStrtOfCtl_private.h
 *
 * Code generated for Simulink model 'LCETCS_vCalIValAtStrtOfCtl'.
 *
 * Model version                  : 1.213
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:54:07 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalIValAtStrtOfCtl_private_h_
#define RTW_HEADER_LCETCS_vCalIValAtStrtOfCtl_private_h_
#include "rtwtypes.h"
#include "model_reference_types.h"
#endif                                 /* RTW_HEADER_LCETCS_vCalIValAtStrtOfCtl_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
