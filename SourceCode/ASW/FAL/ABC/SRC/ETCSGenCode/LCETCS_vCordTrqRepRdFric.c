/*
 * File: LCETCS_vCordTrqRepRdFric.c
 *
 * Code generated for Simulink model 'LCETCS_vCordTrqRepRdFric'.
 *
 * Model version                  : 1.116
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:52:49 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCordTrqRepRdFric.h"
#include "LCETCS_vCordTrqRepRdFric_private.h"

/* Output and update for referenced model: 'LCETCS_vCordTrqRepRdFric' */
void LCETCS_vCordTrqRepRdFric(int32_T rtu_lcetcss32AppTrq4RdFric, int32_T
  rtu_lcetcss32LesTrq4RdFric, int32_T *rty_lcetcss32TrqRepRdFric)
{
  /* MinMax: '<Root>/MinMax' */
  if (rtu_lcetcss32AppTrq4RdFric >= rtu_lcetcss32LesTrq4RdFric) {
    *rty_lcetcss32TrqRepRdFric = rtu_lcetcss32AppTrq4RdFric;
  } else {
    *rty_lcetcss32TrqRepRdFric = rtu_lcetcss32LesTrq4RdFric;
  }

  /* End of MinMax: '<Root>/MinMax' */
}

/* Model initialize function */
void LCETCS_vCordTrqRepRdFric_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
