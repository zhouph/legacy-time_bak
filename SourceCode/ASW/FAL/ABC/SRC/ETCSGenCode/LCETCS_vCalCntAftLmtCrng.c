/*
 * File: LCETCS_vCalCntAftLmtCrng.c
 *
 * Code generated for Simulink model 'LCETCS_vCalCntAftLmtCrng'.
 *
 * Model version                  : 1.84
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:53:41 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalCntAftLmtCrng.h"
#include "LCETCS_vCalCntAftLmtCrng_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalCntAftLmtCrng' */
void LCETCS_vCalCntAftLmtCrng_Init(DW_LCETCS_vCalCntAftLmtCrng_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = 0;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = 0;
}

/* Output and update for referenced model: 'LCETCS_vCalCntAftLmtCrng' */
void LCETCS_vCalCntAftLmtCrng(const TypeETCSCtlErrStruct *rtu_ETCS_CTL_ERR,
  boolean_T rtu_lcetcsu1DctAftLmtCrng, int16_T *rty_lcetcss16AftLmtCrngCnt,
  int16_T *rty_lcetcss16InhCntAftLmtCrngSpn, DW_LCETCS_vCalCntAftLmtCrng_f_T
  *localDW)
{
  int16_T rtb_lcetcss16AftLmtCrngCnt_n;
  int16_T rtb_lcetcss16InhCntAftLmtCrngSpn;
  int32_T tmp;

  /* Chart: '<Root>/Chart' incorporates:
   *  UnitDelay: '<Root>/Unit Delay1'
   *  UnitDelay: '<Root>/Unit Delay2'
   */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:34' */
  if (rtu_lcetcsu1DctAftLmtCrng == 1) {
    /* Transition: '<S1>:171' */
    /* Transition: '<S1>:173' */
    tmp = localDW->UnitDelay1_DSTATE + 1;
    if (tmp > 32767) {
      tmp = 32767;
    }

    rtb_lcetcss16AftLmtCrngCnt_n = (int16_T)tmp;
    if (rtu_ETCS_CTL_ERR->lcetcsu1CtlErrNeg == 1) {
      /* Transition: '<S1>:183' */
      /* Transition: '<S1>:185' */
      tmp = localDW->UnitDelay2_DSTATE + 1;
      if (tmp > 32767) {
        tmp = 32767;
      }

      rtb_lcetcss16InhCntAftLmtCrngSpn = (int16_T)tmp;

      /* Transition: '<S1>:188' */
    } else {
      /* Transition: '<S1>:187' */
      tmp = localDW->UnitDelay2_DSTATE - 1;
      if (tmp < -32768) {
        tmp = -32768;
      }

      rtb_lcetcss16InhCntAftLmtCrngSpn = (int16_T)tmp;
    }

    /* Transition: '<S1>:190' */
  } else {
    /* Transition: '<S1>:175' */
    rtb_lcetcss16AftLmtCrngCnt_n = 0;
    rtb_lcetcss16InhCntAftLmtCrngSpn = 0;
  }

  /* End of Chart: '<Root>/Chart' */

  /* Saturate: '<Root>/Saturation' */
  /* Transition: '<S1>:194' */
  if (rtb_lcetcss16AftLmtCrngCnt_n > 30000) {
    *rty_lcetcss16AftLmtCrngCnt = 30000;
  } else if (rtb_lcetcss16AftLmtCrngCnt_n < 0) {
    *rty_lcetcss16AftLmtCrngCnt = 0;
  } else {
    *rty_lcetcss16AftLmtCrngCnt = rtb_lcetcss16AftLmtCrngCnt_n;
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Saturate: '<Root>/Saturation1' */
  if (rtb_lcetcss16InhCntAftLmtCrngSpn > 100) {
    *rty_lcetcss16InhCntAftLmtCrngSpn = 100;
  } else if (rtb_lcetcss16InhCntAftLmtCrngSpn < 0) {
    *rty_lcetcss16InhCntAftLmtCrngSpn = 0;
  } else {
    *rty_lcetcss16InhCntAftLmtCrngSpn = rtb_lcetcss16InhCntAftLmtCrngSpn;
  }

  /* End of Saturate: '<Root>/Saturation1' */

  /* Update for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = *rty_lcetcss16AftLmtCrngCnt;

  /* Update for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = *rty_lcetcss16InhCntAftLmtCrngSpn;
}

/* Model initialize function */
void LCETCS_vCalCntAftLmtCrng_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
