/*
 * File: LCETCS_vChkDriverIntention.c
 *
 * Code generated for Simulink model 'LCETCS_vChkDriverIntention'.
 *
 * Model version                  : 1.178
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:54:55 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vChkDriverIntention.h"
#include "LCETCS_vChkDriverIntention_private.h"

/* Initial conditions for referenced model: 'LCETCS_vChkDriverIntention' */
void LCETCS_vChkDriverIntention_Init(boolean_T *rty_lcetcsu1DriverAccelIntend,
  boolean_T *rty_lcetcsu1DriverAccelHysIntend)
{
  /* InitializeConditions for Chart: '<Root>/Chart1' */
  *rty_lcetcsu1DriverAccelIntend = false;
  *rty_lcetcsu1DriverAccelHysIntend = false;
}

/* Output and update for referenced model: 'LCETCS_vChkDriverIntention' */
void LCETCS_vChkDriverIntention(const TypeETCSEngStruct *rtu_ETCS_ENG_STRUCT,
  const TypeETCSExtDctStruct *rtu_ETCS_EXT_DCT, boolean_T
  *rty_lcetcsu1DriverAccelIntend, boolean_T *rty_lcetcsu1DriverAccelHysIntend)
{
  /* Chart: '<Root>/Chart1' */
  /* Gateway: Chart1 */
  /* During: Chart1 */
  /* Entry Internal: Chart1 */
  /* Transition: '<S1>:11' */
  /* comment */
  if ((rtu_ETCS_EXT_DCT->lcetcsu8SccCtlAct == 1) ||
      (rtu_ETCS_ENG_STRUCT->lctcss16AccPdlPos >= ((uint8_T)
        U8ETCSCpCtlEnterAccPdlTh))) {
    /* Transition: '<S1>:10' */
    /* Transition: '<S1>:9' */
    *rty_lcetcsu1DriverAccelIntend = true;
    *rty_lcetcsu1DriverAccelHysIntend = true;

    /* Transition: '<S1>:23' */
    /* Transition: '<S1>:24' */
  } else {
    /* Transition: '<S1>:7' */
    if (rtu_ETCS_ENG_STRUCT->lctcss16AccPdlPos > ((uint8_T)
         U8ETCSCpCtlExitAccPdlTh)) {
      /* Transition: '<S1>:19' */
      /* Transition: '<S1>:21' */
      *rty_lcetcsu1DriverAccelIntend = false;
      *rty_lcetcsu1DriverAccelHysIntend = true;

      /* Transition: '<S1>:24' */
    } else {
      /* Transition: '<S1>:22' */
      *rty_lcetcsu1DriverAccelIntend = false;
      *rty_lcetcsu1DriverAccelHysIntend = false;
    }
  }

  /* End of Chart: '<Root>/Chart1' */
  /* Transition: '<S1>:26' */
}

/* Model initialize function */
void LCETCS_vChkDriverIntention_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
