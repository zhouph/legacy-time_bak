/*
 * File: LCETCS_vCntOvrEngTrqCmdTm.h
 *
 * Code generated for Simulink model 'LCETCS_vCntOvrEngTrqCmdTm'.
 *
 * Model version                  : 1.162
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:05:11 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCntOvrEngTrqCmdTm_h_
#define RTW_HEADER_LCETCS_vCntOvrEngTrqCmdTm_h_
#ifndef LCETCS_vCntOvrEngTrqCmdTm_COMMON_INCLUDES_
# define LCETCS_vCntOvrEngTrqCmdTm_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCntOvrEngTrqCmdTm_COMMON_INCLUDES_ */

#include "LCETCS_vCntOvrEngTrqCmdTm_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_s16CntIfSigBigThanTh.h"

/* Block signals for model 'LCETCS_vCntOvrEngTrqCmdTm' */
typedef struct {
  int16_T CounterUpTh;                 /* '<Root>/Constant' */
  int16_T CounterDownTh;               /* '<Root>/Constant1' */
  int16_T Counter;                     /* '<Root>/LCETCS_s16CntIfSigBigThanTh' */
} B_LCETCS_vCntOvrEngTrqCmdTm_c_T;

/* Block states (auto storage) for model 'LCETCS_vCntOvrEngTrqCmdTm' */
typedef struct {
  int16_T UnitDelay_DSTATE;            /* '<Root>/Unit Delay' */
} DW_LCETCS_vCntOvrEngTrqCmdTm_f_T;

typedef struct {
  B_LCETCS_vCntOvrEngTrqCmdTm_c_T rtb;
  DW_LCETCS_vCntOvrEngTrqCmdTm_f_T rtdw;
} MdlrefDW_LCETCS_vCntOvrEngTrqCmdTm_T;

/* Model reference registration function */
extern void LCETCS_vCntOvrEngTrqCmdTm_initialize(void);
extern void LCETCS_vCntOvrEngTrqCmdTm_Init(B_LCETCS_vCntOvrEngTrqCmdTm_c_T
  *localB, DW_LCETCS_vCntOvrEngTrqCmdTm_f_T *localDW);
extern void LCETCS_vCntOvrEngTrqCmdTm_Start(B_LCETCS_vCntOvrEngTrqCmdTm_c_T
  *localB);
extern void LCETCS_vCntOvrEngTrqCmdTm(const TypeETCSEngCmdStruct
  *rtu_ETCS_ENG_CMD_OLD, const TypeETCSEngStruct *rtu_ETCS_ENG_STRUCT, const
  TypeETCSWhlSpinStruct *rtu_ETCS_WHL_SPIN, const TypeETCSTarSpinStruct
  *rtu_ETCS_TAR_SPIN, int16_T *rty_lcetcss16OvrEngTrqCmdTm,
  B_LCETCS_vCntOvrEngTrqCmdTm_c_T *localB, DW_LCETCS_vCntOvrEngTrqCmdTm_f_T
  *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCntOvrEngTrqCmdTm'
 * '<S1>'   : 'LCETCS_vCntOvrEngTrqCmdTm/Chart1'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCntOvrEngTrqCmdTm_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
