/*
 * File: rt_sqrts16__s.c
 *
 * Code generated for Simulink model 'LCETCS_vCalCordRefRdFric'.
 *
 * Model version                  : 1.46
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:50:11 2015
 */

#include "rtwtypes.h"
#include "rt_sqrts16__s.h"

int16_T rt_sqrts16__s(int16_T u)
{
  int16_T y;
  int16_T tmp01_y;
  int16_T shiftMask;
  int32_T iBit;
  int32_T tmp;

  /* Fixed-Point Sqrt Computation by the bisection method. */
  y = 0;
  shiftMask = 16384;
  for (iBit = 0; iBit < 15; iBit++) {
    tmp01_y = (int16_T)(y | shiftMask);
    tmp = tmp01_y * tmp01_y;
    if (tmp > 32767) {
      tmp = 32767;
    } else {
      if (tmp < -32768) {
        tmp = -32768;
      }
    }

    if (tmp <= u) {
      y = tmp01_y;
    }

    shiftMask = (int16_T)(((uint32_T)shiftMask) >> 1U);
  }

  return y;
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
