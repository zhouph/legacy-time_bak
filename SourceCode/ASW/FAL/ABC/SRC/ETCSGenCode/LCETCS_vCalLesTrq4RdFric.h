/*
 * File: LCETCS_vCalLesTrq4RdFric.h
 *
 * Code generated for Simulink model 'LCETCS_vCalLesTrq4RdFric'.
 *
 * Model version                  : 1.123
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:52:44 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalLesTrq4RdFric_h_
#define RTW_HEADER_LCETCS_vCalLesTrq4RdFric_h_
#ifndef LCETCS_vCalLesTrq4RdFric_COMMON_INCLUDES_
# define LCETCS_vCalLesTrq4RdFric_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalLesTrq4RdFric_COMMON_INCLUDES_ */

#include "LCETCS_vCalLesTrq4RdFric_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Block states (auto storage) for model 'LCETCS_vCalLesTrq4RdFric' */
typedef struct {
  int32_T UnitDelay2_DSTATE;           /* '<Root>/Unit Delay2' */
} DW_LCETCS_vCalLesTrq4RdFric_f_T;

typedef struct {
  DW_LCETCS_vCalLesTrq4RdFric_f_T rtdw;
} MdlrefDW_LCETCS_vCalLesTrq4RdFric_T;

/* Model reference registration function */
extern void LCETCS_vCalLesTrq4RdFric_initialize(void);
extern void LCETCS_vCalLesTrq4RdFric_Init(int32_T *rty_lcetcss32LesTrq4RdFric,
  int32_T *rty_lcetcss32TrqAtLowSpnDcted, DW_LCETCS_vCalLesTrq4RdFric_f_T
  *localDW);
extern void LCETCS_vCalLesTrq4RdFric(int32_T rtu_lcetcss32AppTrq4RdFric, const
  TypeETCSLowWhlSpnStruct *rtu_ETCS_LOW_WHL_SPN, int32_T
  *rty_lcetcss32LesTrq4RdFric, int32_T *rty_lcetcss32TrqAtLowSpnDcted,
  DW_LCETCS_vCalLesTrq4RdFric_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalLesTrq4RdFric'
 * '<S1>'   : 'LCETCS_vCalLesTrq4RdFric/Chart'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalLesTrq4RdFric_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
