/*
 * File: LCETCS_vCalTarDecCnt.h
 *
 * Code generated for Simulink model 'LCETCS_vCalTarDecCnt'.
 *
 * Model version                  : 1.169
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:49:00 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalTarDecCnt_h_
#define RTW_HEADER_LCETCS_vCalTarDecCnt_h_
#ifndef LCETCS_vCalTarDecCnt_COMMON_INCLUDES_
# define LCETCS_vCalTarDecCnt_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalTarDecCnt_COMMON_INCLUDES_ */

#include "LCETCS_vCalTarDecCnt_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Block states (auto storage) for model 'LCETCS_vCalTarDecCnt' */
typedef struct {
  int16_T UnitDelay1_DSTATE;           /* '<Root>/Unit Delay1' */
} DW_LCETCS_vCalTarDecCnt_f_T;

typedef struct {
  DW_LCETCS_vCalTarDecCnt_f_T rtdw;
} MdlrefDW_LCETCS_vCalTarDecCnt_T;

/* Model reference registration function */
extern void LCETCS_vCalTarDecCnt_initialize(void);
extern void LCETCS_vCalTarDecCnt_Init(DW_LCETCS_vCalTarDecCnt_f_T *localDW);
extern void LCETCS_vCalTarDecCnt(boolean_T rtu_lcetcsu1TarSpnDec, const
  TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, int16_T *rty_lcetcss16TarSpnDecCnt,
  DW_LCETCS_vCalTarDecCnt_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalTarDecCnt'
 * '<S1>'   : 'LCETCS_vCalTarDecCnt/Chart'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalTarDecCnt_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
