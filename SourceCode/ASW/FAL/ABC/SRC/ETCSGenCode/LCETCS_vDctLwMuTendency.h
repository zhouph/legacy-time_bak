/*
 * File: LCETCS_vDctLwMuTendency.h
 *
 * Code generated for Simulink model 'LCETCS_vDctLwMuTendency'.
 *
 * Model version                  : 1.260
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:56:33 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vDctLwMuTendency_h_
#define RTW_HEADER_LCETCS_vDctLwMuTendency_h_
#ifndef LCETCS_vDctLwMuTendency_COMMON_INCLUDES_
# define LCETCS_vDctLwMuTendency_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vDctLwMuTendency_COMMON_INCLUDES_ */

#include "LCETCS_vDctLwMuTendency_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Block states (auto storage) for model 'LCETCS_vDctLwMuTendency' */
typedef struct {
  boolean_T UnitDelay1_DSTATE;         /* '<Root>/Unit Delay1' */
  boolean_T UnitDelay1_DSTATE_n;       /* '<S1>/Unit Delay1' */
  boolean_T UnitDelay2_DSTATE;         /* '<S1>/Unit Delay2' */
} DW_LCETCS_vDctLwMuTendency_f_T;

typedef struct {
  DW_LCETCS_vDctLwMuTendency_f_T rtdw;
} MdlrefDW_LCETCS_vDctLwMuTendency_T;

/* Model reference registration function */
extern void LCETCS_vDctLwMuTendency_initialize(void);
extern void LCETCS_vDctLwMuTendency_Init(DW_LCETCS_vDctLwMuTendency_f_T *localDW);
extern void LCETCS_vDctLwMuTendency(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT,
  const TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC, const TypeETCSAxlStruct
  *rtu_ETCS_FA, const TypeETCSAxlStruct *rtu_ETCS_RA, int16_T
  rtu_lcetcss16LwToSplitCnt, int16_T rtu_lcetcss16LowHmHldCnt, boolean_T
  *rty_lcetcsu1DctLowHm, boolean_T *rty_lcetcsu1StrtPintLowHm,
  DW_LCETCS_vDctLwMuTendency_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vDctLwMuTendency'
 * '<S1>'   : 'LCETCS_vDctLwMuTendency/DctLwMuTendency'
 * '<S2>'   : 'LCETCS_vDctLwMuTendency/No DctLwMuTendency'
 * '<S3>'   : 'LCETCS_vDctLwMuTendency/DctLwMuTendency/Chart'
 */
#endif                                 /* RTW_HEADER_LCETCS_vDctLwMuTendency_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
