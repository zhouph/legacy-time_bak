/*
 * File: LCETCS_vCalActEngTrqFltGain.c
 *
 * Code generated for Simulink model 'LCETCS_vCalActEngTrqFltGain'.
 *
 * Model version                  : 1.97
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:47:53 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalActEngTrqFltGain.h"
#include "LCETCS_vCalActEngTrqFltGain_private.h"

/* Output and update for referenced model: 'LCETCS_vCalActEngTrqFltGain' */
void LCETCS_vCalActEngTrqFltGain(const TypeETCSCtlCmdStruct
  *rtu_ETCS_CTL_CMD_OLD, int16_T *rty_lcetcss16ActEngTrqFltGain)
{
  /* Switch: '<Root>/Switch' incorporates:
   *  Constant: '<Root>/Filter gain when control command is decreasing'
   *  Constant: '<Root>/Filter gain when control command is increasing'
   */
  if (rtu_ETCS_CTL_CMD_OLD->lcetcsu1CtlCmdInc) {
    *rty_lcetcss16ActEngTrqFltGain = ((uint8_T)U8ETCSCpEngTrqFltGainCmdInc);
  } else {
    *rty_lcetcss16ActEngTrqFltGain = ((uint8_T)U8ETCSCpEngTrqFltGainCmdDec);
  }

  /* End of Switch: '<Root>/Switch' */

  /* Saturate: '<Root>/Saturation' */
  if ((*rty_lcetcss16ActEngTrqFltGain) > 128) {
    *rty_lcetcss16ActEngTrqFltGain = 128;
  } else {
    if ((*rty_lcetcss16ActEngTrqFltGain) < 1) {
      *rty_lcetcss16ActEngTrqFltGain = 1;
    }
  }

  /* End of Saturate: '<Root>/Saturation' */
}

/* Model initialize function */
void LCETCS_vCalActEngTrqFltGain_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
