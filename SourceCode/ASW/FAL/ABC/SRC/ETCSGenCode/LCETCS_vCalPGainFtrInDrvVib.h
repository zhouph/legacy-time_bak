/*
 * File: LCETCS_vCalPGainFtrInDrvVib.h
 *
 * Code generated for Simulink model 'LCETCS_vCalPGainFtrInDrvVib'.
 *
 * Model version                  : 1.205
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:02:23 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalPGainFtrInDrvVib_h_
#define RTW_HEADER_LCETCS_vCalPGainFtrInDrvVib_h_
#ifndef LCETCS_vCalPGainFtrInDrvVib_COMMON_INCLUDES_
# define LCETCS_vCalPGainFtrInDrvVib_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalPGainFtrInDrvVib_COMMON_INCLUDES_ */

#include "LCETCS_vCalPGainFtrInDrvVib_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_s16Inter2Point.h"

/* Block signals for model 'LCETCS_vCalPGainFtrInDrvVib' */
typedef struct {
  int16_T x2;                          /* '<Root>/Maximum counter value when driveline vibration is detected' */
} B_LCETCS_vCalPGainFtrInDrvVib_c_T;

/* Block states (auto storage) for model 'LCETCS_vCalPGainFtrInDrvVib' */
typedef struct {
  int8_T UnitDelay_DSTATE;             /* '<Root>/Unit Delay' */
} DW_LCETCS_vCalPGainFtrInDrvVib_f_T;

typedef struct {
  B_LCETCS_vCalPGainFtrInDrvVib_c_T rtb;
  DW_LCETCS_vCalPGainFtrInDrvVib_f_T rtdw;
} MdlrefDW_LCETCS_vCalPGainFtrInDrvVib_T;

/* Model reference registration function */
extern void LCETCS_vCalPGainFtrInDrvVib_initialize(void);
extern void LCETCS_vCalPGainFtrInDrvVib_Init(int16_T
  *rty_lcetcss16PGainFacInDrvVib, DW_LCETCS_vCalPGainFtrInDrvVib_f_T *localDW);
extern void LCETCS_vCalPGainFtrInDrvVib_Start(B_LCETCS_vCalPGainFtrInDrvVib_c_T *
  localB);
extern void LCETCS_vCalPGainFtrInDrvVib(boolean_T rtu_lcetcsu1VehVibDct, int16_T
  *rty_lcetcss16PGainFacInDrvVib, int8_T *rty_lcetcss8PgainFacInDrvVibCnt,
  B_LCETCS_vCalPGainFtrInDrvVib_c_T *localB, DW_LCETCS_vCalPGainFtrInDrvVib_f_T *
  localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalPGainFtrInDrvVib'
 * '<S1>'   : 'LCETCS_vCalPGainFtrInDrvVib/Chart'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalPGainFtrInDrvVib_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
