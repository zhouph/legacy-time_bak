/*
 * File: binsearch_s16s16.c
 *
 * Code generated for Simulink model 'LCETCS_vCalRefTrqAtStrtOfCtl'.
 *
 * Model version                  : 1.193
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:54:40 2015
 */

#include "rtwtypes.h"
#include "binsearch_s16s16.h"

int16_T binsearch_s16s16(int16_T u, const int16_T bp[], uint32_T startIndex,
  uint32_T maxIndex)
{
  uint32_T iRght;
  uint32_T iLeft;
  uint32_T bpIdx;

  /* Binary Search */
  bpIdx = startIndex;
  iLeft = 0U;
  iRght = maxIndex;
  while ((iRght - iLeft) > 1U) {
    if (u < bp[bpIdx]) {
      iRght = bpIdx;
    } else {
      iLeft = bpIdx;
    }

    bpIdx = ((iRght + iLeft) >> 1U);
  }

  return (int16_T)iLeft;
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
