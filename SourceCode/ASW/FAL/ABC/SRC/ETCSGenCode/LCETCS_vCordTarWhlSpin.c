/*
 * File: LCETCS_vCordTarWhlSpin.c
 *
 * Code generated for Simulink model 'LCETCS_vCordTarWhlSpin'.
 *
 * Model version                  : 1.70
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:07:43 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCordTarWhlSpin.h"
#include "LCETCS_vCordTarWhlSpin_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCordTarWhlSpin' */
void LCETCS_vCordTarWhlSpin_Init(B_LCETCS_vCordTarWhlSpin_c_T *localB,
  DW_LCETCS_vCordTarWhlSpin_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = 0;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localB->lcetcss16ChngRateLmtdSig = 0;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay' */
  localB->lcetcss16TranTmeCnt = 0;

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16ChngRateLmt' */
  LCETCS_s16ChngRateLmt_Init(&localB->lcetcss16ChngRateLmtdSig,
    &localB->lcetcss16TranTmeCnt, &(localDW->LCETCS_s16ChngRateLmt_DWORK1.rtb));
}

/* Start for referenced model: 'LCETCS_vCordTarWhlSpin' */
void LCETCS_vCordTarWhlSpin_Start(B_LCETCS_vCordTarWhlSpin_c_T *localB)
{
  /* Start for Constant: '<Root>/Constant2' */
  localB->lcetcss16TranTme = ((uint8_T)U8ETCSCpTarSpnTransTme);
}

/* Output and update for referenced model: 'LCETCS_vCordTarWhlSpin' */
void LCETCS_vCordTarWhlSpin(int16_T rtu_lcetcss16CordBsTarWhlSpin, int16_T
  rtu_lcetcss16TarWhlSpinInc, int16_T rtu_lcetcss16TarWhlSpdStlDetd, int16_T
  rtu_lcetcss16TarWhlSpinDec, int16_T *rty_lcetcss16TarWhlSpin,
  B_LCETCS_vCordTarWhlSpin_c_T *localB, DW_LCETCS_vCordTarWhlSpin_f_T *localDW)
{
  /* local block i/o variables */
  int16_T rtb_lcetcss16TrgtSigVal;
  int16_T rtb_lcetcss16TranTmeCntOld;
  int16_T rtb_lcetcss16ChngRateLmtdSigOld;
  int16_T rtb_lcetcss16TrgtSigValOld;

  /* Sum: '<Root>/Add' */
  rtb_lcetcss16TrgtSigValOld = (int16_T)((rtu_lcetcss16CordBsTarWhlSpin +
    rtu_lcetcss16TarWhlSpinInc) + rtu_lcetcss16TarWhlSpinDec);

  /* Constant: '<Root>/Constant2' */
  localB->lcetcss16TranTme = ((uint8_T)U8ETCSCpTarSpnTransTme);

  /* MinMax: '<Root>/MinMax' */
  if (rtb_lcetcss16TrgtSigValOld >= rtu_lcetcss16TarWhlSpdStlDetd) {
    rtb_lcetcss16TrgtSigVal = rtb_lcetcss16TrgtSigValOld;
  } else {
    rtb_lcetcss16TrgtSigVal = rtu_lcetcss16TarWhlSpdStlDetd;
  }

  /* End of MinMax: '<Root>/MinMax' */

  /* UnitDelay: '<Root>/Unit Delay2' */
  rtb_lcetcss16TrgtSigValOld = localDW->UnitDelay2_DSTATE;

  /* UnitDelay: '<Root>/Unit Delay1' */
  rtb_lcetcss16ChngRateLmtdSigOld = localB->lcetcss16ChngRateLmtdSig;

  /* UnitDelay: '<Root>/Unit Delay' */
  rtb_lcetcss16TranTmeCntOld = localB->lcetcss16TranTmeCnt;

  /* ModelReference: '<Root>/LCETCS_s16ChngRateLmt' */
  LCETCS_s16ChngRateLmt(rtb_lcetcss16TrgtSigVal, rtb_lcetcss16TrgtSigValOld,
                        localB->lcetcss16TranTme,
                        rtb_lcetcss16ChngRateLmtdSigOld,
                        rtb_lcetcss16TranTmeCntOld,
                        &localB->lcetcss16ChngRateLmtdSig,
                        &localB->lcetcss16TranTmeCnt,
                        &(localDW->LCETCS_s16ChngRateLmt_DWORK1.rtb));

  /* Saturate: '<Root>/Saturation' */
  if (localB->lcetcss16ChngRateLmtdSig > 255) {
    *rty_lcetcss16TarWhlSpin = 255;
  } else if (localB->lcetcss16ChngRateLmtdSig < ((uint8_T)U8ETCSCpMinTarSpn)) {
    *rty_lcetcss16TarWhlSpin = ((uint8_T)U8ETCSCpMinTarSpn);
  } else {
    *rty_lcetcss16TarWhlSpin = localB->lcetcss16ChngRateLmtdSig;
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Update for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = rtb_lcetcss16TrgtSigVal;
}

/* Model initialize function */
void LCETCS_vCordTarWhlSpin_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16ChngRateLmt' */
  LCETCS_s16ChngRateLmt_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
