/*
 * File: LCETCS_vCalBasDelYawCnvrgGain_private.h
 *
 * Code generated for Simulink model 'LCETCS_vCalBasDelYawCnvrgGain'.
 *
 * Model version                  : 1.261
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:02:50 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalBasDelYawCnvrgGain_private_h_
#define RTW_HEADER_LCETCS_vCalBasDelYawCnvrgGain_private_h_
#include "rtwtypes.h"
#include "model_reference_types.h"

/* Includes for objects with custom storage classes. */
#include "../ETCSGenCodeHeader/ETCSHeader4Import.h"

/*
 * Generate compile time checks that imported macros for parameters
 * with storage class "ImportedDefine" are defined
 */
#ifndef S16ETCSCpSpd_1
#error The variable for the parameter "S16ETCSCpSpd_1" is not defined
#endif

#ifndef S16ETCSCpSpd_2
#error The variable for the parameter "S16ETCSCpSpd_2" is not defined
#endif

#ifndef S16ETCSCpSpd_3
#error The variable for the parameter "S16ETCSCpSpd_3" is not defined
#endif

#ifndef S16ETCSCpSpd_4
#error The variable for the parameter "S16ETCSCpSpd_4" is not defined
#endif

#ifndef S16ETCSCpSpd_5
#error The variable for the parameter "S16ETCSCpSpd_5" is not defined
#endif

#ifndef U8ETCSCpAyAsp
#error The variable for the parameter "U8ETCSCpAyAsp" is not defined
#endif

#ifndef U8ETCSCpAyIce
#error The variable for the parameter "U8ETCSCpAyIce" is not defined
#endif

#ifndef U8ETCSCpAySnw
#error The variable for the parameter "U8ETCSCpAySnw" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainAsp_1
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainAsp_1" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainAsp_2
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainAsp_2" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainAsp_3
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainAsp_3" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainAsp_4
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainAsp_4" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainAsp_5
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainAsp_5" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainIce_1
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainIce_1" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainIce_2
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainIce_2" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainIce_3
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainIce_3" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainIce_4
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainIce_4" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainIce_5
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainIce_5" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainSnw_1
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainSnw_1" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainSnw_2
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainSnw_2" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainSnw_3
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainSnw_3" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainSnw_4
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainSnw_4" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainSnw_5
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainSnw_5" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainAsp_1
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainAsp_1" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainAsp_2
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainAsp_2" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainAsp_3
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainAsp_3" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainAsp_4
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainAsp_4" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainAsp_5
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainAsp_5" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainIce_1
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainIce_1" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainIce_2
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainIce_2" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainIce_3
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainIce_3" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainIce_4
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainIce_4" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainIce_5
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainIce_5" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainSnw_1
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainSnw_1" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainSnw_2
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainSnw_2" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainSnw_3
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainSnw_3" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainSnw_4
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainSnw_4" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainSnw_5
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainSnw_5" is not defined
#endif
#endif                                 /* RTW_HEADER_LCETCS_vCalBasDelYawCnvrgGain_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
