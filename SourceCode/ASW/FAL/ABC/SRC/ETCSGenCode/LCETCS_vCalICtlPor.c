/*
 * File: LCETCS_vCalICtlPor.c
 *
 * Code generated for Simulink model 'LCETCS_vCalICtlPor'.
 *
 * Model version                  : 1.197
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:51:23 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalICtlPor.h"
#include "LCETCS_vCalICtlPor_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalICtlPor' */
void LCETCS_vCalICtlPor_Init(DW_LCETCS_vCalICtlPor_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = 0;
}

/* Output and update for referenced model: 'LCETCS_vCalICtlPor' */
void LCETCS_vCalICtlPor(const TypeETCSLdTrqStruct *rtu_ETCS_LD_TRQ, const
  TypeETCSIValRstStruct *rtu_ETCS_IVAL_RST, const TypeETCSH2LStruct
  *rtu_ETCS_H2L, int32_T rtu_lcetcss32AdptAmt, int32_T rtu_lcetcss32CtlCmdMax,
  const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, int32_T *rty_lcetcss32ICtlPor,
  DW_LCETCS_vCalICtlPor_f_T *localDW)
{
  /* Switch: '<Root>/Switch1' incorporates:
   *  Constant: '<Root>/Constant1'
   */
  if (rtu_ETCS_CTL_ACT->lcetcsu1CtlAct) {
    /* Switch: '<Root>/Switch' incorporates:
     *  Sum: '<Root>/Sum3'
     *  UnitDelay: '<Root>/Unit Delay'
     */
    if (rtu_ETCS_IVAL_RST->lcetcsu1RstIVal) {
      localDW->UnitDelay_DSTATE = rtu_ETCS_IVAL_RST->lcetcss32IVal2Rst;
    } else {
      localDW->UnitDelay_DSTATE += rtu_ETCS_LD_TRQ->lcetcss32LdTrqDecBySymBrkCtl;
    }

    /* End of Switch: '<Root>/Switch' */
  } else {
    localDW->UnitDelay_DSTATE = 0;
  }

  /* End of Switch: '<Root>/Switch1' */

  /* Sum: '<Root>/Sum2' */
  localDW->UnitDelay_DSTATE += rtu_lcetcss32AdptAmt;

  /* MinMax: '<Root>/MinMax1' */
  if (rtu_lcetcss32CtlCmdMax <= localDW->UnitDelay_DSTATE) {
    localDW->UnitDelay_DSTATE = rtu_lcetcss32CtlCmdMax;
  }

  /* End of MinMax: '<Root>/MinMax1' */

  /* Saturate: '<Root>/Saturation1' */
  if (localDW->UnitDelay_DSTATE < 0) {
    localDW->UnitDelay_DSTATE = 0;
  }

  /* End of Saturate: '<Root>/Saturation1' */

  /* Switch: '<Root>/Switch3' incorporates:
   *  Constant: '<Root>/Constant5'
   */
  if (rtu_ETCS_H2L->lcetcsu1HiToLw) {
    *rty_lcetcss32ICtlPor = 0;
  } else {
    *rty_lcetcss32ICtlPor = rtu_ETCS_LD_TRQ->lcetcss32LdTrq;
  }

  /* End of Switch: '<Root>/Switch3' */

  /* MinMax: '<Root>/MinMax2' */
  if (localDW->UnitDelay_DSTATE >= (*rty_lcetcss32ICtlPor)) {
    *rty_lcetcss32ICtlPor = localDW->UnitDelay_DSTATE;
  }

  /* End of MinMax: '<Root>/MinMax2' */
}

/* Model initialize function */
void LCETCS_vCalICtlPor_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
