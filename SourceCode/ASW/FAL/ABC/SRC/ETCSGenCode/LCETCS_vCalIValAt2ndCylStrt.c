/*
 * File: LCETCS_vCalIValAt2ndCylStrt.c
 *
 * Code generated for Simulink model 'LCETCS_vCalIValAt2ndCylStrt'.
 *
 * Model version                  : 1.143
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:53:54 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalIValAt2ndCylStrt.h"
#include "LCETCS_vCalIValAt2ndCylStrt_private.h"

/* Output and update for referenced model: 'LCETCS_vCalIValAt2ndCylStrt' */
void LCETCS_vCalIValAt2ndCylStrt(const TypeETCSCtlCmdStruct
  *rtu_ETCS_CTL_CMD_OLD, const TypeETCSRefTrqStr2CylStruct
  *rtu_ETCS_REF_TRQ_STR2CYL, const TypeETCSCyl1stStruct *rtu_ETCS_CYL_1ST, const
  TypeETCSErrZroCrsStruct *rtu_ETCS_ERR_ZRO_CRS, int32_T
  *rty_lcetcss32IValAt2ndCylStrt, boolean_T *rty_lcetcsu1RstIValAt2ndCylStrt)
{
  /* Logic: '<Root>/Logical Operator' */
  *rty_lcetcsu1RstIValAt2ndCylStrt = ((rtu_ETCS_CYL_1ST->lcetcsu1FalEdgOfCyl1st)
    && (rtu_ETCS_ERR_ZRO_CRS->lcetcsu1JumpUp));

  /* If: '<Root>/If' incorporates:
   *  Constant: '<S2>/Constant'
   */
  if (*rty_lcetcsu1RstIValAt2ndCylStrt) {
    /* Outputs for IfAction SubSystem: '<Root>/If Action Subsystem' incorporates:
     *  ActionPort: '<S1>/Action Port'
     */
    /* Product: '<S1>/Divide' incorporates:
     *  Product: '<S1>/Product'
     */
    *rty_lcetcss32IValAt2ndCylStrt =
      (rtu_ETCS_REF_TRQ_STR2CYL->lcetcss32EstTrqByVehAccIn1StCyl *
       rtu_ETCS_REF_TRQ_STR2CYL->lcetcss16Ftr4RefTrqAt2ndCylStrt) / 100;

    /* MinMax: '<S1>/MinMax' */
    if ((*rty_lcetcss32IValAt2ndCylStrt) >=
        rtu_ETCS_CTL_CMD_OLD->lcetcss32CardanTrqCmd) {
    } else {
      *rty_lcetcss32IValAt2ndCylStrt =
        rtu_ETCS_CTL_CMD_OLD->lcetcss32CardanTrqCmd;
    }

    /* End of MinMax: '<S1>/MinMax' */
    /* End of Outputs for SubSystem: '<Root>/If Action Subsystem' */
  } else {
    /* Outputs for IfAction SubSystem: '<Root>/If Action Subsystem1' incorporates:
     *  ActionPort: '<S2>/Action Port'
     */
    *rty_lcetcss32IValAt2ndCylStrt = 0;

    /* End of Outputs for SubSystem: '<Root>/If Action Subsystem1' */
  }

  /* End of If: '<Root>/If' */
}

/* Model initialize function */
void LCETCS_vCalIValAt2ndCylStrt_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
