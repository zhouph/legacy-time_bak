/*
 * File: LCETCS_vCalWhlSpin.c
 *
 * Code generated for Simulink model 'LCETCS_vCalWhlSpin'.
 *
 * Model version                  : 1.57
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:00:50 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalWhlSpin.h"
#include "LCETCS_vCalWhlSpin_private.h"

/* Output and update for referenced model: 'LCETCS_vCalWhlSpin' */
void LCETCS_vCalWhlSpin(const TypeETCSWhlStruct *rtu_ETCS_FL, const
  TypeETCSWhlStruct *rtu_ETCS_FR, const TypeETCSWhlStruct *rtu_ETCS_RL, const
  TypeETCSWhlStruct *rtu_ETCS_RR, int16_T rtu_lcetcss16VehSpd,
  TypeETCSWhlSpinStruct *rty_ETCS_WHL_SPIN)
{
  /* local block i/o variables */
  int16_T rtb_lcetcss16WhlSpin;
  int16_T rtb_lcetcss16WhlSpinFrtAxl;
  int16_T rtb_lcetcss16WhlSpinRrAxl;
  int16_T rtb_lcetcss16WhlSpinNonFilt;
  int16_T rtb_lcetcss16WhlSpinFrtAxlNonFilt;
  int16_T rtb_lcetcss16WhlSpinRrAxlNonFilt;

  /* ModelReference: '<Root>/LCETCS_vCalWhlSpinFilt' */
  LCETCS_vCalWhlSpinFilt(rtu_ETCS_FL, rtu_ETCS_FR, rtu_ETCS_RL, rtu_ETCS_RR,
    rtu_lcetcss16VehSpd, &rtb_lcetcss16WhlSpin, &rtb_lcetcss16WhlSpinFrtAxl,
    &rtb_lcetcss16WhlSpinRrAxl);

  /* ModelReference: '<Root>/LCETCS_vCalWhlSpinNonFilt' */
  LCETCS_vCalWhlSpinNonFilt(rtu_ETCS_FL, rtu_ETCS_FR, rtu_ETCS_RL, rtu_ETCS_RR,
    rtu_lcetcss16VehSpd, &rtb_lcetcss16WhlSpinNonFilt,
    &rtb_lcetcss16WhlSpinFrtAxlNonFilt, &rtb_lcetcss16WhlSpinRrAxlNonFilt);

  /* BusCreator: '<Root>/Bus Creator2' */
  rty_ETCS_WHL_SPIN->lcetcss16WhlSpin = rtb_lcetcss16WhlSpin;
  rty_ETCS_WHL_SPIN->lcetcss16WhlSpinFrtAxl = rtb_lcetcss16WhlSpinFrtAxl;
  rty_ETCS_WHL_SPIN->lcetcss16WhlSpinRrAxl = rtb_lcetcss16WhlSpinRrAxl;
  rty_ETCS_WHL_SPIN->lcetcss16WhlSpinNonFilt = rtb_lcetcss16WhlSpinNonFilt;
  rty_ETCS_WHL_SPIN->lcetcss16WhlSpinFrtAxlNonFilt =
    rtb_lcetcss16WhlSpinFrtAxlNonFilt;
  rty_ETCS_WHL_SPIN->lcetcss16WhlSpinRrAxlNonFilt =
    rtb_lcetcss16WhlSpinRrAxlNonFilt;
}

/* Model initialize function */
void LCETCS_vCalWhlSpin_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalWhlSpinFilt' */
  LCETCS_vCalWhlSpinFilt_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalWhlSpinNonFilt' */
  LCETCS_vCalWhlSpinNonFilt_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
