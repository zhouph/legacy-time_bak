/*
 * File: LCETCS_vCalPCtlGain_private.h
 *
 * Code generated for Simulink model 'LCETCS_vCalPCtlGain'.
 *
 * Model version                  : 1.179
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:08:27 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalPCtlGain_private_h_
#define RTW_HEADER_LCETCS_vCalPCtlGain_private_h_
#include "rtwtypes.h"
#include "model_reference_types.h"

/* Includes for objects with custom storage classes. */
#include "../ETCSGenCodeHeader/ETCSHeader4Import.h"

/*
 * Generate compile time checks that imported macros for parameters
 * with storage class "ImportedDefine" are defined
 */
#ifndef S16ETCSCpCdrnTrqAtAsphalt
#error The variable for the parameter "S16ETCSCpCdrnTrqAtAsphalt" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtIce
#error The variable for the parameter "S16ETCSCpCdrnTrqAtIce" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtSnow
#error The variable for the parameter "S16ETCSCpCdrnTrqAtSnow" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_1
#error The variable for the parameter "S16ETCSCpTotalGearRatio_1" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_2
#error The variable for the parameter "S16ETCSCpTotalGearRatio_2" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_3
#error The variable for the parameter "S16ETCSCpTotalGearRatio_3" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_4
#error The variable for the parameter "S16ETCSCpTotalGearRatio_4" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_5
#error The variable for the parameter "S16ETCSCpTotalGearRatio_5" is not defined
#endif

#ifndef S8ETCSCpPgTransTmDrvVib
#error The variable for the parameter "S8ETCSCpPgTransTmDrvVib" is not defined
#endif

#ifndef U8ETCSCpAyAsp
#error The variable for the parameter "U8ETCSCpAyAsp" is not defined
#endif

#ifndef U8ETCSCpAyIce
#error The variable for the parameter "U8ETCSCpAyIce" is not defined
#endif

#ifndef U8ETCSCpAySnw
#error The variable for the parameter "U8ETCSCpAySnw" is not defined
#endif

#ifndef U8ETCSCpGainTrnsTmAftrGearShft
#error The variable for the parameter "U8ETCSCpGainTrnsTmAftrGearShft" is not defined
#endif

#ifndef U8ETCSCpH2LDctHldTm
#error The variable for the parameter "U8ETCSCpH2LDctHldTm" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainAsp_1
#error The variable for the parameter "U8ETCSCpNegErrPgainAsp_1" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainAsp_2
#error The variable for the parameter "U8ETCSCpNegErrPgainAsp_2" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainAsp_3
#error The variable for the parameter "U8ETCSCpNegErrPgainAsp_3" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainAsp_4
#error The variable for the parameter "U8ETCSCpNegErrPgainAsp_4" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainAsp_5
#error The variable for the parameter "U8ETCSCpNegErrPgainAsp_5" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainIce_1
#error The variable for the parameter "U8ETCSCpNegErrPgainIce_1" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainIce_2
#error The variable for the parameter "U8ETCSCpNegErrPgainIce_2" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainIce_3
#error The variable for the parameter "U8ETCSCpNegErrPgainIce_3" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainIce_4
#error The variable for the parameter "U8ETCSCpNegErrPgainIce_4" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainIce_5
#error The variable for the parameter "U8ETCSCpNegErrPgainIce_5" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSnw_1
#error The variable for the parameter "U8ETCSCpNegErrPgainSnw_1" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSnw_2
#error The variable for the parameter "U8ETCSCpNegErrPgainSnw_2" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSnw_3
#error The variable for the parameter "U8ETCSCpNegErrPgainSnw_3" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSnw_4
#error The variable for the parameter "U8ETCSCpNegErrPgainSnw_4" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSnw_5
#error The variable for the parameter "U8ETCSCpNegErrPgainSnw_5" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSplt_1
#error The variable for the parameter "U8ETCSCpNegErrPgainSplt_1" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSplt_2
#error The variable for the parameter "U8ETCSCpNegErrPgainSplt_2" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSplt_3
#error The variable for the parameter "U8ETCSCpNegErrPgainSplt_3" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSplt_4
#error The variable for the parameter "U8ETCSCpNegErrPgainSplt_4" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSplt_5
#error The variable for the parameter "U8ETCSCpNegErrPgainSplt_5" is not defined
#endif

#ifndef U8ETCSCpPGainIncFacH2L
#error The variable for the parameter "U8ETCSCpPGainIncFacH2L" is not defined
#endif

#ifndef U8ETCSCpPgFacGrChgErrNeg_1
#error The variable for the parameter "U8ETCSCpPgFacGrChgErrNeg_1" is not defined
#endif

#ifndef U8ETCSCpPgFacGrChgErrNeg_2
#error The variable for the parameter "U8ETCSCpPgFacGrChgErrNeg_2" is not defined
#endif

#ifndef U8ETCSCpPgFacGrChgErrNeg_3
#error The variable for the parameter "U8ETCSCpPgFacGrChgErrNeg_3" is not defined
#endif

#ifndef U8ETCSCpPgFacGrChgErrNeg_4
#error The variable for the parameter "U8ETCSCpPgFacGrChgErrNeg_4" is not defined
#endif

#ifndef U8ETCSCpPgFacGrChgErrNeg_5
#error The variable for the parameter "U8ETCSCpPgFacGrChgErrNeg_5" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnAsp_1
#error The variable for the parameter "U8ETCSCpPgFacInTrnAsp_1" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnAsp_2
#error The variable for the parameter "U8ETCSCpPgFacInTrnAsp_2" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnAsp_3
#error The variable for the parameter "U8ETCSCpPgFacInTrnAsp_3" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnAsp_4
#error The variable for the parameter "U8ETCSCpPgFacInTrnAsp_4" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnAsp_5
#error The variable for the parameter "U8ETCSCpPgFacInTrnAsp_5" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnIce_1
#error The variable for the parameter "U8ETCSCpPgFacInTrnIce_1" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnIce_2
#error The variable for the parameter "U8ETCSCpPgFacInTrnIce_2" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnIce_3
#error The variable for the parameter "U8ETCSCpPgFacInTrnIce_3" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnIce_4
#error The variable for the parameter "U8ETCSCpPgFacInTrnIce_4" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnIce_5
#error The variable for the parameter "U8ETCSCpPgFacInTrnIce_5" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnSnw_1
#error The variable for the parameter "U8ETCSCpPgFacInTrnSnw_1" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnSnw_2
#error The variable for the parameter "U8ETCSCpPgFacInTrnSnw_2" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnSnw_3
#error The variable for the parameter "U8ETCSCpPgFacInTrnSnw_3" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnSnw_4
#error The variable for the parameter "U8ETCSCpPgFacInTrnSnw_4" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnSnw_5
#error The variable for the parameter "U8ETCSCpPgFacInTrnSnw_5" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainAsp_1
#error The variable for the parameter "U8ETCSCpPosErrPgainAsp_1" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainAsp_2
#error The variable for the parameter "U8ETCSCpPosErrPgainAsp_2" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainAsp_3
#error The variable for the parameter "U8ETCSCpPosErrPgainAsp_3" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainAsp_4
#error The variable for the parameter "U8ETCSCpPosErrPgainAsp_4" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainAsp_5
#error The variable for the parameter "U8ETCSCpPosErrPgainAsp_5" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainIce_1
#error The variable for the parameter "U8ETCSCpPosErrPgainIce_1" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainIce_2
#error The variable for the parameter "U8ETCSCpPosErrPgainIce_2" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainIce_3
#error The variable for the parameter "U8ETCSCpPosErrPgainIce_3" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainIce_4
#error The variable for the parameter "U8ETCSCpPosErrPgainIce_4" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainIce_5
#error The variable for the parameter "U8ETCSCpPosErrPgainIce_5" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSnw_1
#error The variable for the parameter "U8ETCSCpPosErrPgainSnw_1" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSnw_2
#error The variable for the parameter "U8ETCSCpPosErrPgainSnw_2" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSnw_3
#error The variable for the parameter "U8ETCSCpPosErrPgainSnw_3" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSnw_4
#error The variable for the parameter "U8ETCSCpPosErrPgainSnw_4" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSnw_5
#error The variable for the parameter "U8ETCSCpPosErrPgainSnw_5" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSplt_1
#error The variable for the parameter "U8ETCSCpPosErrPgainSplt_1" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSplt_2
#error The variable for the parameter "U8ETCSCpPosErrPgainSplt_2" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSplt_3
#error The variable for the parameter "U8ETCSCpPosErrPgainSplt_3" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSplt_4
#error The variable for the parameter "U8ETCSCpPosErrPgainSplt_4" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSplt_5
#error The variable for the parameter "U8ETCSCpPosErrPgainSplt_5" is not defined
#endif

#ifndef U8ETCSCpTarTransCplBrkTrq2Sp
#error The variable for the parameter "U8ETCSCpTarTransCplBrkTrq2Sp" is not defined
#endif

#ifndef U8ETCSCpTarTransStBrkTrq2Sp
#error The variable for the parameter "U8ETCSCpTarTransStBrkTrq2Sp" is not defined
#endif
#endif                                 /* RTW_HEADER_LCETCS_vCalPCtlGain_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
