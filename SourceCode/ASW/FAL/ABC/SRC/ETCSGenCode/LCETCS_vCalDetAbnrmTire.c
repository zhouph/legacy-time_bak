/*
 * File: LCETCS_vCalDetAbnrmTire.c
 *
 * Code generated for Simulink model 'LCETCS_vCalDetAbnrmTire'.
 *
 * Model version                  : 1.127
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:49:45 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalDetAbnrmTire.h"
#include "LCETCS_vCalDetAbnrmTire_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalDetAbnrmTire' */
void LCETCS_vCalDetAbnrmTire_Init(boolean_T *rty_lcetcsu1AbnrmSizeTireDet)
{
  /* InitializeConditions for Chart: '<Root>/Chart' */
  *rty_lcetcsu1AbnrmSizeTireDet = false;
}

/* Output and update for referenced model: 'LCETCS_vCalDetAbnrmTire' */
void LCETCS_vCalDetAbnrmTire(const TypeETCSWhlStruct *rtu_ETCS_FL, const
  TypeETCSWhlStruct *rtu_ETCS_FR, const TypeETCSWhlStruct *rtu_ETCS_RL, const
  TypeETCSWhlStruct *rtu_ETCS_RR, boolean_T *rty_lcetcsu1AbnrmSizeTireDet)
{
  /* Chart: '<Root>/Chart' */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:34' */
  if ((((rtu_ETCS_FL->lcetcsu1AbnrmTireSuspect == 1) ||
        (rtu_ETCS_FR->lcetcsu1AbnrmTireSuspect == 1)) ||
       (rtu_ETCS_RL->lcetcsu1AbnrmTireSuspect == 1)) ||
      (rtu_ETCS_RR->lcetcsu1AbnrmTireSuspect == 1)) {
    /* Transition: '<S1>:35' */
    /* Transition: '<S1>:43' */
    *rty_lcetcsu1AbnrmSizeTireDet = true;

    /* Transition: '<S1>:174' */
  } else {
    /* Transition: '<S1>:38' */
    *rty_lcetcsu1AbnrmSizeTireDet = false;
  }

  /* End of Chart: '<Root>/Chart' */
  /* Transition: '<S1>:198' */
}

/* Model initialize function */
void LCETCS_vCalDetAbnrmTire_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
