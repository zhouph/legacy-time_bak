/*
 * File: LCETCS_vCalEngStallThr4Ax.h
 *
 * Code generated for Simulink model 'LCETCS_vCalEngStallThr4Ax'.
 *
 * Model version                  : 1.236
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:10:09 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalEngStallThr4Ax_h_
#define RTW_HEADER_LCETCS_vCalEngStallThr4Ax_h_
#ifndef LCETCS_vCalEngStallThr4Ax_COMMON_INCLUDES_
# define LCETCS_vCalEngStallThr4Ax_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalEngStallThr4Ax_COMMON_INCLUDES_ */

#include "LCETCS_vCalEngStallThr4Ax_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vCalEngStallThr.h"
#include "LCETCS_vCalEngStallRiskIndex.h"

/* Block states (auto storage) for model 'LCETCS_vCalEngStallThr4Ax' */
typedef struct {
  MdlrefDW_LCETCS_vCalEngStallRiskIndex_T LCETCS_vCalEngStallRiskIndex_DWORK1;/* '<Root>/LCETCS_vCalEngStallRiskIndex' */
  MdlrefDW_LCETCS_vCalEngStallThr_T LCETCS_vCalEngStallThr_DWORK1;/* '<Root>/LCETCS_vCalEngStallThr' */
} DW_LCETCS_vCalEngStallThr4Ax_f_T;

typedef struct {
  DW_LCETCS_vCalEngStallThr4Ax_f_T rtdw;
} MdlrefDW_LCETCS_vCalEngStallThr4Ax_T;

/* Model reference registration function */
extern void LCETCS_vCalEngStallThr4Ax_initialize(void);
extern void LCETCS_vCalEngStallThr4Ax_Init(DW_LCETCS_vCalEngStallThr4Ax_f_T
  *localDW);
extern void LCETCS_vCalEngStallThr4Ax_Start(DW_LCETCS_vCalEngStallThr4Ax_f_T
  *localDW);
extern void LCETCS_vCalEngStallThr4Ax_Disable(DW_LCETCS_vCalEngStallThr4Ax_f_T
  *localDW);
extern void LCETCS_vCalEngStallThr4Ax(const TypeETCSCtlActStruct
  *rtu_ETCS_CTL_ACT_OLD, const TypeETCSEngStallStruct *rtu_ETCS_ENG_STALL_OLD,
  const TypeETCSSplitHillStruct *rtu_ETCS_SPLIT_HILL_OLD, int16_T
  rtu_lcetcss16VehSpd, int16_T rtu_lcetcss16EngSpdSlope, int16_T
  *rty_lcetcss16EngStallThrTemp, int8_T *rty_lcetcss8EngStallRiskIndex,
  DW_LCETCS_vCalEngStallThr4Ax_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalEngStallThr4Ax'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalEngStallThr4Ax_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
