/*
 * File: LCETCS_vCalLdTrqByGrdRaw.c
 *
 * Code generated for Simulink model 'LCETCS_vCalLdTrqByGrdRaw'.
 *
 * Model version                  : 1.206
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:48:11 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalLdTrqByGrdRaw.h"
#include "LCETCS_vCalLdTrqByGrdRaw_private.h"

/* Output and update for referenced model: 'LCETCS_vCalLdTrqByGrdRaw' */
void LCETCS_vCalLdTrqByGrdRaw(int16_T rtu_lcetcss16AxAtHillDctd, const
  TypeETCSSplitHillStruct *rtu_ETCS_SPLIT_HILL_OLD, int16_T
  *rty_lcetcss16LdTrqByGrdRaw)
{
  int32_T rtb_Switch3;

  /* Switch: '<Root>/Switch3' incorporates:
   *  Constant: '<Root>/Constant3'
   *  Constant: '<Root>/Constant4'
   *  Constant: '<Root>/Constant5'
   *  Product: '<Root>/Divide3'
   *  Product: '<Root>/Divide4'
   *  Product: '<Root>/Divide5'
   *  Product: '<Root>/Divide6'
   *  Product: '<Root>/Divide8'
   */
  if (rtu_ETCS_SPLIT_HILL_OLD->lcetcsu1SplitHillDct) {
    rtb_Switch3 = (((rtu_lcetcss16AxAtHillDctd * 981) / 100) * ((((int16_T)
      S16ETCSCpTireRadi) * ((int16_T)S16ETCSCpTtlVehMass)) / 1000)) / 100;
  } else {
    rtb_Switch3 = 0;
  }

  /* End of Switch: '<Root>/Switch3' */

  /* Saturate: '<Root>/Saturation' */
  if (rtb_Switch3 > 32767) {
    *rty_lcetcss16LdTrqByGrdRaw = MAX_int16_T;
  } else if (rtb_Switch3 < 0) {
    *rty_lcetcss16LdTrqByGrdRaw = 0;
  } else {
    *rty_lcetcss16LdTrqByGrdRaw = (int16_T)rtb_Switch3;
  }

  /* End of Saturate: '<Root>/Saturation' */
}

/* Model initialize function */
void LCETCS_vCalLdTrqByGrdRaw_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
