/*
 * File: LCETCS_vConvAx2CrdnTrq.c
 *
 * Code generated for Simulink model 'LCETCS_vConvAx2CrdnTrq'.
 *
 * Model version                  : 1.491
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:03:25 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vConvAx2CrdnTrq.h"
#include "LCETCS_vConvAx2CrdnTrq_private.h"

/* Initial conditions for referenced model: 'LCETCS_vConvAx2CrdnTrq' */
void LCETCS_vConvAx2CrdnTrq_Init(B_LCETCS_vConvAx2CrdnTrq_c_T *localB)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter3Point' */
  LCETCS_s16Inter3Point_Init(&localB->y);
}

/* Start for referenced model: 'LCETCS_vConvAx2CrdnTrq' */
void LCETCS_vConvAx2CrdnTrq_Start(B_LCETCS_vConvAx2CrdnTrq_c_T *localB)
{
  /* Start for Constant: '<Root>/Constant' */
  localB->x1 = ((uint8_T)U8ETCSCpAxAtIce);

  /* Start for Constant: '<Root>/Constant2' */
  localB->x2 = ((uint8_T)U8ETCSCpAxAtSnow);

  /* Start for Constant: '<Root>/Constant3' */
  localB->x3 = ((uint8_T)U8ETCSCpAxAtAsphalt);
}

/* Output and update for referenced model: 'LCETCS_vConvAx2CrdnTrq' */
void LCETCS_vConvAx2CrdnTrq(int16_T rtu_VehAcc, int32_T *rty_CrdnTrq,
  B_LCETCS_vConvAx2CrdnTrq_c_T *localB)
{
  /* Constant: '<Root>/Constant' */
  localB->x1 = ((uint8_T)U8ETCSCpAxAtIce);

  /* Constant: '<Root>/Constant2' */
  localB->x2 = ((uint8_T)U8ETCSCpAxAtSnow);

  /* Constant: '<Root>/Constant3' */
  localB->x3 = ((uint8_T)U8ETCSCpAxAtAsphalt);

  /* ModelReference: '<Root>/LCETCS_s16Inter3Point' */
  LCETCS_s16Inter3Point(rtu_VehAcc, localB->x1, localB->x2, localB->x3,
                        ((int16_T)S16ETCSCpCdrnTrqAtIce), ((int16_T)
    S16ETCSCpCdrnTrqAtSnow), ((int16_T)S16ETCSCpCdrnTrqAtAsphalt), &localB->y);

  /* Switch: '<Root>/Switch' incorporates:
   *  Constant: '<Root>/Constant1'
   *  Constant: '<Root>/Constant4'
   *  Product: '<Root>/Product'
   *  RelationalOperator: '<Root>/Relational Operator'
   */
  if (rtu_VehAcc > 0) {
    *rty_CrdnTrq = localB->y * 10;
  } else {
    *rty_CrdnTrq = 0;
  }

  /* End of Switch: '<Root>/Switch' */

  /* Saturate: '<Root>/Saturation' */
  if ((*rty_CrdnTrq) < 0) {
    *rty_CrdnTrq = 0;
  }

  /* End of Saturate: '<Root>/Saturation' */
}

/* Model initialize function */
void LCETCS_vConvAx2CrdnTrq_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter3Point' */
  LCETCS_s16Inter3Point_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
