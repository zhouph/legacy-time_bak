/*
 * File: LCETCS_vDctSplitHillCndSet.c
 *
 * Code generated for Simulink model 'LCETCS_vDctSplitHillCndSet'.
 *
 * Model version                  : 1.482
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:58:05 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctSplitHillCndSet.h"
#include "LCETCS_vDctSplitHillCndSet_private.h"

/* Initial conditions for referenced model: 'LCETCS_vDctSplitHillCndSet' */
void LCETCS_vDctSplitHillCndSet_Init(boolean_T *rty_lcetcsu1SplitHillClrCndSet,
  boolean_T *rty_lcetcsu1SplitHillClrDecCndSet, boolean_T
  *rty_lcetcsu1SplitHillDctCndSet)
{
  /* InitializeConditions for Chart: '<Root>/Chart' */
  *rty_lcetcsu1SplitHillDctCndSet = false;
  *rty_lcetcsu1SplitHillClrCndSet = false;
  *rty_lcetcsu1SplitHillClrDecCndSet = false;
}

/* Output and update for referenced model: 'LCETCS_vDctSplitHillCndSet' */
void LCETCS_vDctSplitHillCndSet(const TypeETCSAxlStruct *rtu_ETCS_FA, const
  TypeETCSAxlStruct *rtu_ETCS_RA, int16_T rtu_lcetcss16LongAccFilter, const
  TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC, const TypeETCSEngStruct
  *rtu_ETCS_ENG_STRUCT, int16_T rtu_lcetcss16VehSpd, boolean_T
  *rty_lcetcsu1SplitHillClrCndSet, boolean_T *rty_lcetcsu1SplitHillClrDecCndSet,
  boolean_T *rty_lcetcsu1SplitHillDctCndSet)
{
  int16_T rtb_Divide1;

  /* MinMax: '<Root>/MinMax' */
  if (rtu_ETCS_FA->lcetcss16EstBrkTrqOnLowMuWhl >=
      rtu_ETCS_RA->lcetcss16EstBrkTrqOnLowMuWhl) {
    rtb_Divide1 = rtu_ETCS_FA->lcetcss16EstBrkTrqOnLowMuWhl;
  } else {
    rtb_Divide1 = rtu_ETCS_RA->lcetcss16EstBrkTrqOnLowMuWhl;
  }

  /* Product: '<Root>/Divide1' incorporates:
   *  MinMax: '<Root>/MinMax'
   */
  rtb_Divide1 = (int16_T)(rtb_Divide1 / 10);

  /* Chart: '<Root>/Chart' */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:9' */
  /* comment */
  if (((((rtb_Divide1 >= ((uint8_T)U8ETCSCpTarTransCplBrkTrq2Sp)) &&
         (rtu_lcetcss16LongAccFilter >= ((uint8_T)U8ETCSCpSplitHillDctMinAx))) &&
        (rtu_ETCS_VEH_ACC->lcetcss16VehAccByVehSpd <= ((uint8_T)
          U8ETCSCpSplitHillDctAccel))) && (rtu_lcetcss16VehSpd <= ((uint8_T)
         U8ETCSCpSplitHillDctVehSpd))) && (rtu_ETCS_ENG_STRUCT->lcetcss16EngSpd <=
       ((int16_T)S16ETCSCpSplitHillDctEngSpd))) {
    /* Transition: '<S1>:100' */
    /* Transition: '<S1>:102' */
    *rty_lcetcsu1SplitHillDctCndSet = true;

    /* Transition: '<S1>:143' */
  } else {
    /* Transition: '<S1>:110' */
    *rty_lcetcsu1SplitHillDctCndSet = false;
  }

  /* Transition: '<S1>:145' */
  if (((rtu_ETCS_VEH_ACC->lcetcss16VehAccByVehSpd > ((uint8_T)
         U8ETCSCpSplitHillClrAccel)) || (rtu_lcetcss16VehSpd > ((uint8_T)
         U8ETCSCpSplitHillClrVehSpd))) || (rtu_ETCS_ENG_STRUCT->lcetcss16EngSpd >
       ((int16_T)S16ETCSCpSplitHillClrEngSpd))) {
    /* Transition: '<S1>:147' */
    /* Transition: '<S1>:149' */
    *rty_lcetcsu1SplitHillClrCndSet = true;

    /* Transition: '<S1>:151' */
  } else {
    /* Transition: '<S1>:152' */
    *rty_lcetcsu1SplitHillClrCndSet = false;
  }

  /* Transition: '<S1>:153' */
  if (rtb_Divide1 < ((uint8_T)U8ETCSCpTarTransStBrkTrq2Sp)) {
    /* Transition: '<S1>:118' */
    /* Transition: '<S1>:155' */
    *rty_lcetcsu1SplitHillClrDecCndSet = true;

    /* Transition: '<S1>:157' */
  } else {
    /* Transition: '<S1>:158' */
    *rty_lcetcsu1SplitHillClrDecCndSet = false;
  }

  /* End of Chart: '<Root>/Chart' */
  /* Transition: '<S1>:160' */
}

/* Model initialize function */
void LCETCS_vDctSplitHillCndSet_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
