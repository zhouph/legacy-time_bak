/*
 * File: LCETCS_vCalOvrWhlSpnTmTh.c
 *
 * Code generated for Simulink model 'LCETCS_vCalOvrWhlSpnTmTh'.
 *
 * Model version                  : 1.158
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:05:04 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalOvrWhlSpnTmTh.h"
#include "LCETCS_vCalOvrWhlSpnTmTh_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalOvrWhlSpnTmTh' */
void LCETCS_vCalOvrWhlSpnTmTh_Init(int16_T *rty_lcetcss16OvrWhlSpnTmTh)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_Init(rty_lcetcss16OvrWhlSpnTmTh);
}

/* Start for referenced model: 'LCETCS_vCalOvrWhlSpnTmTh' */
void LCETCS_vCalOvrWhlSpnTmTh_Start(B_LCETCS_vCalOvrWhlSpnTmTh_c_T *localB)
{
  /* Start for Constant: '<Root>/Control act threshold at high axle acceleration' */
  localB->y2 = ((uint8_T)U8ETCSCpCtlActCntThAtHiAxlAcc);

  /* Start for Constant: '<Root>/Control act threshold at low axle acceleration' */
  localB->y1 = ((uint8_T)U8ETCSCpCtlActCntThAtLwAxlAcc);

  /* Start for Constant: '<Root>/High acceleration threshold' */
  localB->x2 = ((uint8_T)U8ETCSCpHiAxlAccTh);

  /* Start for Constant: '<Root>/Low acceleration threshold' */
  localB->x1 = ((uint8_T)U8ETCSCpLwAxlAccTh);
}

/* Output and update for referenced model: 'LCETCS_vCalOvrWhlSpnTmTh' */
void LCETCS_vCalOvrWhlSpnTmTh(const TypeETCSAxlAccStruct *rtu_ETCS_AXL_ACC,
  int16_T *rty_lcetcss16OvrWhlSpnTmTh, B_LCETCS_vCalOvrWhlSpnTmTh_c_T *localB)
{
  /* Constant: '<Root>/Control act threshold at high axle acceleration' */
  localB->y2 = ((uint8_T)U8ETCSCpCtlActCntThAtHiAxlAcc);

  /* Constant: '<Root>/Control act threshold at low axle acceleration' */
  localB->y1 = ((uint8_T)U8ETCSCpCtlActCntThAtLwAxlAcc);

  /* Constant: '<Root>/High acceleration threshold' */
  localB->x2 = ((uint8_T)U8ETCSCpHiAxlAccTh);

  /* Constant: '<Root>/Low acceleration threshold' */
  localB->x1 = ((uint8_T)U8ETCSCpLwAxlAccTh);

  /* ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point(rtu_ETCS_AXL_ACC->lcetcss16DrvnAxlAcc, localB->x1,
                        localB->x2, localB->y1, localB->y2,
                        rty_lcetcss16OvrWhlSpnTmTh);
}

/* Model initialize function */
void LCETCS_vCalOvrWhlSpnTmTh_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
