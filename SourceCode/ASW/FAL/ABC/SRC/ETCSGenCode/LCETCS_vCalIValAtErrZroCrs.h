/*
 * File: LCETCS_vCalIValAtErrZroCrs.h
 *
 * Code generated for Simulink model 'LCETCS_vCalIValAtErrZroCrs'.
 *
 * Model version                  : 1.185
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:54:01 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalIValAtErrZroCrs_h_
#define RTW_HEADER_LCETCS_vCalIValAtErrZroCrs_h_
#ifndef LCETCS_vCalIValAtErrZroCrs_COMMON_INCLUDES_
# define LCETCS_vCalIValAtErrZroCrs_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalIValAtErrZroCrs_COMMON_INCLUDES_ */

#include "LCETCS_vCalIValAtErrZroCrs_types.h"

/* Shared type includes */
#include "model_reference_types.h"
#include "mul_s32_s32_s32_sat.h"

/* Model reference registration function */
extern void LCETCS_vCalIValAtErrZroCrs_initialize(void);
extern void LCETCS_vCalIValAtErrZroCrs_Init(int32_T
  *rty_lcetcss32IValAtErrZroCrs, boolean_T *rty_lcetcsu1RstIValAtErrZroCrs);
extern void LCETCS_vCalIValAtErrZroCrs(boolean_T rtu_lcetcsu1RstIValAt2ndCylStrt,
  const TypeETCSCtlCmdStruct *rtu_ETCS_CTL_CMD_OLD, const
  TypeETCSErrZroCrsStruct *rtu_ETCS_ERR_ZRO_CRS, const TypeETCSRefTrq2JmpStruct *
  rtu_ETCS_REF_TRQ2JMP, const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const
  TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN, int32_T
  *rty_lcetcss32IValAtErrZroCrs, boolean_T *rty_lcetcsu1RstIValAtErrZroCrs);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalIValAtErrZroCrs'
 * '<S1>'   : 'LCETCS_vCalIValAtErrZroCrs/Chart'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalIValAtErrZroCrs_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
