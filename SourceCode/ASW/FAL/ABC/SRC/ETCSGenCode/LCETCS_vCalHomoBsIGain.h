/*
 * File: LCETCS_vCalHomoBsIGain.h
 *
 * Code generated for Simulink model 'LCETCS_vCalHomoBsIGain'.
 *
 * Model version                  : 1.153
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:01:25 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalHomoBsIGain_h_
#define RTW_HEADER_LCETCS_vCalHomoBsIGain_h_
#ifndef LCETCS_vCalHomoBsIGain_COMMON_INCLUDES_
# define LCETCS_vCalHomoBsIGain_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalHomoBsIGain_COMMON_INCLUDES_ */

#include "LCETCS_vCalHomoBsIGain_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_s16Inter3by5.h"

/* Block signals for model 'LCETCS_vCalHomoBsIGain' */
typedef struct {
  int16_T z53;                         /* '<S4>/Igain for 1st gear ratio on asphalt' */
  int16_T z51;                         /* '<S4>/Igain for 1st gear ratio on ice' */
  int16_T z52;                         /* '<S4>/Igain for 1st gear ratio on snow' */
  int16_T z43;                         /* '<S4>/Igain for 2nd gear ratio on asphalt' */
  int16_T z41;                         /* '<S4>/Igain for 2nd gear ratio on ice' */
  int16_T z42;                         /* '<S4>/Igain for 2nd gear ratio on snow' */
  int16_T z33;                         /* '<S4>/Igain for 3rd gear ratio on asphalt' */
  int16_T z31;                         /* '<S4>/Igain for 3rd gear ratio on ice' */
  int16_T z32;                         /* '<S4>/Igain for 3rd gear ratio on snow' */
  int16_T z23;                         /* '<S4>/Igain for 4th gear ratio on asphalt' */
  int16_T z21;                         /* '<S4>/Igain for 4th gear ratio on ice' */
  int16_T z22;                         /* '<S4>/Igain for 4th gear ratio on snow' */
  int16_T z13;                         /* '<S4>/Igain for 5th gear ratio on Asphalt' */
  int16_T z11;                         /* '<S4>/Igain for 5th gear ratio on ice' */
  int16_T z12;                         /* '<S4>/Igain for 5th gear ratio on snow' */
  int16_T lcetcss16HomoBsIgain;        /* '<S4>/LCETCS_s16Inter3by5_1' */
  int16_T z53_n;                       /* '<S3>/Igain for 1st gear ratio on asphalt' */
  int16_T z51_h;                       /* '<S3>/Igain for 1st gear ratio on ice' */
  int16_T z52_o;                       /* '<S3>/Igain for 1st gear ratio on snow' */
  int16_T z43_k;                       /* '<S3>/Igain for 2nd gear ratio on asphalt' */
  int16_T z41_m;                       /* '<S3>/Igain for 2nd gear ratio on ice' */
  int16_T z42_i;                       /* '<S3>/Igain for 2nd gear ratio on snow' */
  int16_T z33_e;                       /* '<S3>/Igain for 3rd gear ratio on asphalt' */
  int16_T z31_k;                       /* '<S3>/Igain for 3rd gear ratio on ice' */
  int16_T z32_p;                       /* '<S3>/Igain for 3rd gear ratio on snow' */
  int16_T z23_g;                       /* '<S3>/Igain for 4th gear ratio on asphalt' */
  int16_T z21_b;                       /* '<S3>/Igain for 4th gear ratio on ice' */
  int16_T z22_l;                       /* '<S3>/Igain for 4th gear ratio on snow' */
  int16_T z13_a;                       /* '<S3>/Igain for 5th gear ratio on Asphalt' */
  int16_T z11_a;                       /* '<S3>/Igain for 5th gear ratio on ice' */
  int16_T z12_l;                       /* '<S3>/Igain for 5th gear ratio on snow' */
  int16_T lcetcss16HomoBsIgain_p;      /* '<S3>/LCETCS_s16Inter3by5' */
} B_LCETCS_vCalHomoBsIGain_c_T;

typedef struct {
  B_LCETCS_vCalHomoBsIGain_c_T rtb;
} MdlrefDW_LCETCS_vCalHomoBsIGain_T;

/* Model reference registration function */
extern void LCETCS_vCalHomoBsIGain_initialize(void);
extern void LCETCS_vCalHomoBsIGain_Start(B_LCETCS_vCalHomoBsIGain_c_T *localB);
extern void LCETCS_vCalHomoBsIGain(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT,
  const TypeETCSDrvMdlStruct *rtu_ETCS_DRV_MDL, const TypeETCSCtlErrStruct
  *rtu_ETCS_CTL_ERR, const TypeETCSTrq4RdFricStruct *rtu_ETCS_TRQ_REP_RD_FRIC,
  int16_T *rty_lcetcss16HomoBsIgain, B_LCETCS_vCalHomoBsIGain_c_T *localB);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalHomoBsIGain'
 * '<S1>'   : 'LCETCS_vCalHomoBsIGain/CalIGainWhnCtlOff'
 * '<S2>'   : 'LCETCS_vCalHomoBsIGain/CalPGainWhnCtlOn'
 * '<S3>'   : 'LCETCS_vCalHomoBsIGain/CalPGainWhnCtlOn/CalIgainWhnCtlErrNeg'
 * '<S4>'   : 'LCETCS_vCalHomoBsIGain/CalPGainWhnCtlOn/CalPgainWhnCtlErrPos'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalHomoBsIGain_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
