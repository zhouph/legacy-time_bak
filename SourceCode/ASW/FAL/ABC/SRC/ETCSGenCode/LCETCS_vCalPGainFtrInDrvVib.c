/*
 * File: LCETCS_vCalPGainFtrInDrvVib.c
 *
 * Code generated for Simulink model 'LCETCS_vCalPGainFtrInDrvVib'.
 *
 * Model version                  : 1.205
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:02:23 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalPGainFtrInDrvVib.h"
#include "LCETCS_vCalPGainFtrInDrvVib_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalPGainFtrInDrvVib' */
void LCETCS_vCalPGainFtrInDrvVib_Init(int16_T *rty_lcetcss16PGainFacInDrvVib,
  DW_LCETCS_vCalPGainFtrInDrvVib_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = 0;

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_Init(rty_lcetcss16PGainFacInDrvVib);
}

/* Start for referenced model: 'LCETCS_vCalPGainFtrInDrvVib' */
void LCETCS_vCalPGainFtrInDrvVib_Start(B_LCETCS_vCalPGainFtrInDrvVib_c_T *localB)
{
  /* Start for Constant: '<Root>/Maximum counter value when driveline vibration is detected' */
  localB->x2 = ((int8_T)S8ETCSCpPgTransTmDrvVib);
}

/* Output and update for referenced model: 'LCETCS_vCalPGainFtrInDrvVib' */
void LCETCS_vCalPGainFtrInDrvVib(boolean_T rtu_lcetcsu1VehVibDct, int16_T
  *rty_lcetcss16PGainFacInDrvVib, int8_T *rty_lcetcss8PgainFacInDrvVibCnt,
  B_LCETCS_vCalPGainFtrInDrvVib_c_T *localB, DW_LCETCS_vCalPGainFtrInDrvVib_f_T *
  localDW)
{
  /* local block i/o variables */
  int16_T rtb_x;
  int32_T tmp;

  /* Chart: '<Root>/Chart' incorporates:
   *  UnitDelay: '<Root>/Unit Delay'
   */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:76' */
  if (rtu_lcetcsu1VehVibDct == 1) {
    /* Transition: '<S1>:75' */
    /* Transition: '<S1>:74' */
    tmp = localDW->UnitDelay_DSTATE + 1;
    if (tmp > 127) {
      tmp = 127;
    }

    *rty_lcetcss8PgainFacInDrvVibCnt = (int8_T)tmp;

    /* Transition: '<S1>:73' */
  } else {
    /* Transition: '<S1>:72' */
    tmp = localDW->UnitDelay_DSTATE - 1;
    if (tmp < -128) {
      tmp = -128;
    }

    *rty_lcetcss8PgainFacInDrvVibCnt = (int8_T)tmp;
  }

  /* End of Chart: '<Root>/Chart' */

  /* Saturate: '<Root>/Saturation' */
  /* Transition: '<S1>:71' */
  if ((*rty_lcetcss8PgainFacInDrvVibCnt) > ((int8_T)S8ETCSCpPgTransTmDrvVib)) {
    *rty_lcetcss8PgainFacInDrvVibCnt = ((int8_T)S8ETCSCpPgTransTmDrvVib);
  } else {
    if ((*rty_lcetcss8PgainFacInDrvVibCnt) < 0) {
      *rty_lcetcss8PgainFacInDrvVibCnt = 0;
    }
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* DataTypeConversion: '<Root>/Data Type Conversion' */
  rtb_x = *rty_lcetcss8PgainFacInDrvVibCnt;

  /* Constant: '<Root>/Maximum counter value when driveline vibration is detected' */
  localB->x2 = ((int8_T)S8ETCSCpPgTransTmDrvVib);

  /* ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point(rtb_x,
                        rtCP_Countervaluewhendrivelinevibrationisnotdetect,
                        localB->x2, rtCP_u0percent_Value, rtCP_percent_Value,
                        rty_lcetcss16PGainFacInDrvVib);

  /* Update for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = *rty_lcetcss8PgainFacInDrvVibCnt;
}

/* Model initialize function */
void LCETCS_vCalPGainFtrInDrvVib_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
