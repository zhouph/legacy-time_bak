/*
 * File: LCETCS_vCalIValAt2ndCylStrt.h
 *
 * Code generated for Simulink model 'LCETCS_vCalIValAt2ndCylStrt'.
 *
 * Model version                  : 1.143
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:53:54 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalIValAt2ndCylStrt_h_
#define RTW_HEADER_LCETCS_vCalIValAt2ndCylStrt_h_
#ifndef LCETCS_vCalIValAt2ndCylStrt_COMMON_INCLUDES_
# define LCETCS_vCalIValAt2ndCylStrt_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalIValAt2ndCylStrt_COMMON_INCLUDES_ */

#include "LCETCS_vCalIValAt2ndCylStrt_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Model reference registration function */
extern void LCETCS_vCalIValAt2ndCylStrt_initialize(void);
extern void LCETCS_vCalIValAt2ndCylStrt(const TypeETCSCtlCmdStruct
  *rtu_ETCS_CTL_CMD_OLD, const TypeETCSRefTrqStr2CylStruct
  *rtu_ETCS_REF_TRQ_STR2CYL, const TypeETCSCyl1stStruct *rtu_ETCS_CYL_1ST, const
  TypeETCSErrZroCrsStruct *rtu_ETCS_ERR_ZRO_CRS, int32_T
  *rty_lcetcss32IValAt2ndCylStrt, boolean_T *rty_lcetcsu1RstIValAt2ndCylStrt);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalIValAt2ndCylStrt'
 * '<S1>'   : 'LCETCS_vCalIValAt2ndCylStrt/If Action Subsystem'
 * '<S2>'   : 'LCETCS_vCalIValAt2ndCylStrt/If Action Subsystem1'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalIValAt2ndCylStrt_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
