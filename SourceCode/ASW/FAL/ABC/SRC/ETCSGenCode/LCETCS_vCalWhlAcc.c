/*
 * File: LCETCS_vCalWhlAcc.c
 *
 * Code generated for Simulink model 'LCETCS_vCalWhlAcc'.
 *
 * Model version                  : 1.81
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:50:31 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalWhlAcc.h"
#include "LCETCS_vCalWhlAcc_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalWhlAcc' */
void LCETCS_vCalWhlAcc_Init(DW_LCETCS_vCalWhlAcc_f_T *localDW)
{
  /* InitializeConditions for Delay: '<Root>/Delay' */
  localDW->Delay_DSTATE = 0;

  /* InitializeConditions for Delay: '<Root>/Delay3' */
  localDW->Delay3_DSTATE = 0;

  /* InitializeConditions for Delay: '<Root>/Delay1' */
  localDW->Delay1_DSTATE = 0;

  /* InitializeConditions for Delay: '<Root>/Delay2' */
  localDW->Delay2_DSTATE = 0;
}

/* Output and update for referenced model: 'LCETCS_vCalWhlAcc' */
void LCETCS_vCalWhlAcc(const TypeETCSWhlStruct *rtu_ETCS_FL, const
  TypeETCSWhlStruct *rtu_ETCS_FR, const TypeETCSWhlStruct *rtu_ETCS_RL, const
  TypeETCSWhlStruct *rtu_ETCS_RR, TypeETCSWhlAccStruct *rty_ETCS_WHL_ACC,
  DW_LCETCS_vCalWhlAcc_f_T *localDW)
{
  int16_T u0;

  /* Product: '<Root>/Product' incorporates:
   *  Delay: '<Root>/Delay'
   *  Sum: '<Root>/Add1'
   */
  u0 = (int16_T)(((int16_T)(rtu_ETCS_FL->lcetcss16WhlSpdCrt -
    localDW->Delay_DSTATE)) << 1);

  /* Saturate: '<Root>/Saturation' */
  if (u0 > 2000) {
    rty_ETCS_WHL_ACC->lcetcss16FrtLefWhlAcc = 2000;
  } else if (u0 < -2000) {
    rty_ETCS_WHL_ACC->lcetcss16FrtLefWhlAcc = -2000;
  } else {
    rty_ETCS_WHL_ACC->lcetcss16FrtLefWhlAcc = u0;
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Product: '<Root>/Product2' incorporates:
   *  Delay: '<Root>/Delay2'
   *  Sum: '<Root>/Add4'
   */
  u0 = (int16_T)(((int16_T)(rtu_ETCS_FR->lcetcss16WhlSpdCrt -
    localDW->Delay2_DSTATE)) << 1);

  /* Saturate: '<Root>/Saturation1' */
  if (u0 > 2000) {
    rty_ETCS_WHL_ACC->lcetcss16FrtRigWhlAcc = 2000;
  } else if (u0 < -2000) {
    rty_ETCS_WHL_ACC->lcetcss16FrtRigWhlAcc = -2000;
  } else {
    rty_ETCS_WHL_ACC->lcetcss16FrtRigWhlAcc = u0;
  }

  /* End of Saturate: '<Root>/Saturation1' */

  /* Product: '<Root>/Product3' incorporates:
   *  Delay: '<Root>/Delay3'
   *  Sum: '<Root>/Add2'
   */
  u0 = (int16_T)(((int16_T)(rtu_ETCS_RL->lcetcss16WhlSpdCrt -
    localDW->Delay3_DSTATE)) << 1);

  /* Saturate: '<Root>/Saturation2' */
  if (u0 > 2000) {
    rty_ETCS_WHL_ACC->lcetcss16RrLefWhlAcc = 2000;
  } else if (u0 < -2000) {
    rty_ETCS_WHL_ACC->lcetcss16RrLefWhlAcc = -2000;
  } else {
    rty_ETCS_WHL_ACC->lcetcss16RrLefWhlAcc = u0;
  }

  /* End of Saturate: '<Root>/Saturation2' */

  /* Product: '<Root>/Product1' incorporates:
   *  Delay: '<Root>/Delay1'
   *  Sum: '<Root>/Add3'
   */
  u0 = (int16_T)(((int16_T)(rtu_ETCS_RR->lcetcss16WhlSpdCrt -
    localDW->Delay1_DSTATE)) << 1);

  /* Saturate: '<Root>/Saturation3' */
  if (u0 > 2000) {
    rty_ETCS_WHL_ACC->lcetcss16RrRigWhlAcc = 2000;
  } else if (u0 < -2000) {
    rty_ETCS_WHL_ACC->lcetcss16RrRigWhlAcc = -2000;
  } else {
    rty_ETCS_WHL_ACC->lcetcss16RrRigWhlAcc = u0;
  }

  /* End of Saturate: '<Root>/Saturation3' */

  /* Update for Delay: '<Root>/Delay' */
  localDW->Delay_DSTATE = rtu_ETCS_FL->lcetcss16WhlSpdCrt;

  /* Update for Delay: '<Root>/Delay3' */
  localDW->Delay3_DSTATE = rtu_ETCS_RL->lcetcss16WhlSpdCrt;

  /* Update for Delay: '<Root>/Delay1' */
  localDW->Delay1_DSTATE = rtu_ETCS_RR->lcetcss16WhlSpdCrt;

  /* Update for Delay: '<Root>/Delay2' */
  localDW->Delay2_DSTATE = rtu_ETCS_FR->lcetcss16WhlSpdCrt;
}

/* Model initialize function */
void LCETCS_vCalWhlAcc_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
