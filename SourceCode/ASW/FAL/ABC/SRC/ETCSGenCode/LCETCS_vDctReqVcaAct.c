/*
 * File: LCETCS_vDctReqVcaAct.c
 *
 * Code generated for Simulink model 'LCETCS_vDctReqVcaAct'.
 *
 * Model version                  : 1.572
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:06:33 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctReqVcaAct.h"
#include "LCETCS_vDctReqVcaAct_private.h"

/* Initial conditions for referenced model: 'LCETCS_vDctReqVcaAct' */
void LCETCS_vDctReqVcaAct_Init(boolean_T *rty_lcetcsu1ReqVcaEngTrqAct, boolean_T
  *rty_lcetcsu1ReqVcaEngTrqRsgEdg, boolean_T *rty_lcetcsu1ReqVcaBrkTrqAct,
  boolean_T *rty_lcetcsu1ReqVcaBrkTrqRsgEdg, DW_LCETCS_vDctReqVcaAct_f_T
  *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = false;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay6' */
  localDW->UnitDelay6_DSTATE = false;

  /* InitializeConditions for Chart: '<Root>/Chart1' */
  *rty_lcetcsu1ReqVcaEngTrqAct = false;
  *rty_lcetcsu1ReqVcaBrkTrqAct = false;

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctRisingEdge' */
  LCETCS_vDctRisingEdge_Init(rty_lcetcsu1ReqVcaEngTrqRsgEdg,
    &(localDW->LCETCS_vDctRisingEdge_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctRisingEdge1' */
  LCETCS_vDctRisingEdge_Init(rty_lcetcsu1ReqVcaBrkTrqRsgEdg,
    &(localDW->LCETCS_vDctRisingEdge1_DWORK1.rtdw));
}

/* Output and update for referenced model: 'LCETCS_vDctReqVcaAct' */
void LCETCS_vDctReqVcaAct(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT,
  boolean_T rtu_lcetcsu1InhibitBrkVca, boolean_T rtu_lcetcsu1ReqVcaTrqAct,
  boolean_T *rty_lcetcsu1ReqVcaEngTrqAct, boolean_T
  *rty_lcetcsu1ReqVcaEngTrqRsgEdg, boolean_T *rty_lcetcsu1ReqVcaBrkTrqAct,
  boolean_T *rty_lcetcsu1ReqVcaBrkTrqRsgEdg, DW_LCETCS_vDctReqVcaAct_f_T
  *localDW)
{
  /* Chart: '<Root>/Chart1' incorporates:
   *  UnitDelay: '<Root>/Unit Delay1'
   *  UnitDelay: '<Root>/Unit Delay6'
   */
  /* Gateway: Chart1 */
  /* During: Chart1 */
  /* Entry Internal: Chart1 */
  /* Transition: '<S1>:25' */
  /* comment */
  if (rtu_ETCS_CTL_ACT->lcetcsu1CtlAct == 1) {
    /* Transition: '<S1>:29' */
    /* Transition: '<S1>:92' */
    if ((localDW->UnitDelay1_DSTATE == 0) && (rtu_lcetcsu1ReqVcaTrqAct == 1)) {
      /* Transition: '<S1>:94' */
      /* Transition: '<S1>:96' */
      *rty_lcetcsu1ReqVcaEngTrqAct = true;
      *rty_lcetcsu1ReqVcaBrkTrqAct = true;

      /* Transition: '<S1>:110' */
      /* Transition: '<S1>:113' */
    } else {
      /* Transition: '<S1>:98' */
      if (rtu_lcetcsu1InhibitBrkVca == 1) {
        /* Transition: '<S1>:108' */
        /* Transition: '<S1>:111' */
        *rty_lcetcsu1ReqVcaEngTrqAct = false;
        *rty_lcetcsu1ReqVcaBrkTrqAct = false;

        /* Transition: '<S1>:113' */
      } else {
        /* Transition: '<S1>:112' */
        *rty_lcetcsu1ReqVcaEngTrqAct = false;
        *rty_lcetcsu1ReqVcaBrkTrqAct = localDW->UnitDelay6_DSTATE;
      }
    }

    /* Transition: '<S1>:101' */
  } else {
    /* Transition: '<S1>:100' */
    *rty_lcetcsu1ReqVcaEngTrqAct = false;
    *rty_lcetcsu1ReqVcaBrkTrqAct = false;
  }

  /* End of Chart: '<Root>/Chart1' */
  /* Transition: '<S1>:103' */

  /* ModelReference: '<Root>/LCETCS_vDctRisingEdge' */
  LCETCS_vDctRisingEdge((*rty_lcetcsu1ReqVcaEngTrqAct),
                        rty_lcetcsu1ReqVcaEngTrqRsgEdg,
                        &(localDW->LCETCS_vDctRisingEdge_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vDctRisingEdge1' */
  LCETCS_vDctRisingEdge((*rty_lcetcsu1ReqVcaBrkTrqAct),
                        rty_lcetcsu1ReqVcaBrkTrqRsgEdg,
                        &(localDW->LCETCS_vDctRisingEdge1_DWORK1.rtdw));

  /* Update for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = rtu_lcetcsu1ReqVcaTrqAct;

  /* Update for UnitDelay: '<Root>/Unit Delay6' */
  localDW->UnitDelay6_DSTATE = *rty_lcetcsu1ReqVcaBrkTrqAct;
}

/* Model initialize function */
void LCETCS_vDctReqVcaAct_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctRisingEdge' */
  LCETCS_vDctRisingEdge_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctRisingEdge1' */
  LCETCS_vDctRisingEdge_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
