/*
 * File: LCETCS_vCordPCtlGain.h
 *
 * Code generated for Simulink model 'LCETCS_vCordPCtlGain'.
 *
 * Model version                  : 1.195
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:51:54 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCordPCtlGain_h_
#define RTW_HEADER_LCETCS_vCordPCtlGain_h_
#ifndef LCETCS_vCordPCtlGain_COMMON_INCLUDES_
# define LCETCS_vCordPCtlGain_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCordPCtlGain_COMMON_INCLUDES_ */

#include "LCETCS_vCordPCtlGain_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Model reference registration function */
extern void LCETCS_vCordPCtlGain_initialize(void);
extern void LCETCS_vCordPCtlGain_Init(int16_T *rty_lcetcss16PgainFac);
extern void LCETCS_vCordPCtlGain(int16_T rtu_lcetcss16BsPgain, int16_T
  rtu_lcetcss16PgainFacInGearChng, int16_T rtu_lcetcss16PGainIncFacH2L, const
  TypeETCSH2LStruct *rtu_ETCS_H2L, int16_T rtu_lcetcss16PGainFacInTrn, const
  TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const TypeETCSTarSpinStruct
  *rtu_ETCS_TAR_SPIN, const TypeETCSGainGearShftStruct *rtu_ETCS_GAIN_GEAR_SHFT,
  int8_T rtu_lcetcss8PgainFacInDrvVibCnt, int16_T rtu_lcetcss16PGainFacInDrvVib,
  int16_T *rty_lcetcss16Pgain, int16_T *rty_lcetcss16PgainFac);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCordPCtlGain'
 * '<S1>'   : 'LCETCS_vCordPCtlGain/Chart'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCordPCtlGain_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
