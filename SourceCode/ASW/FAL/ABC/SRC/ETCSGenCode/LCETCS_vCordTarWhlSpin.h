/*
 * File: LCETCS_vCordTarWhlSpin.h
 *
 * Code generated for Simulink model 'LCETCS_vCordTarWhlSpin'.
 *
 * Model version                  : 1.70
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:07:43 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCordTarWhlSpin_h_
#define RTW_HEADER_LCETCS_vCordTarWhlSpin_h_
#ifndef LCETCS_vCordTarWhlSpin_COMMON_INCLUDES_
# define LCETCS_vCordTarWhlSpin_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCordTarWhlSpin_COMMON_INCLUDES_ */

#include "LCETCS_vCordTarWhlSpin_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_s16ChngRateLmt.h"

/* Block signals for model 'LCETCS_vCordTarWhlSpin' */
typedef struct {
  int16_T lcetcss16ChngRateLmtdSig;    /* '<Root>/LCETCS_s16ChngRateLmt' */
  int16_T lcetcss16TranTmeCnt;         /* '<Root>/LCETCS_s16ChngRateLmt' */
  int16_T lcetcss16TranTme;            /* '<Root>/Constant2' */
} B_LCETCS_vCordTarWhlSpin_c_T;

/* Block states (auto storage) for model 'LCETCS_vCordTarWhlSpin' */
typedef struct {
  int16_T UnitDelay2_DSTATE;           /* '<Root>/Unit Delay2' */
  MdlrefDW_LCETCS_s16ChngRateLmt_T LCETCS_s16ChngRateLmt_DWORK1;/* '<Root>/LCETCS_s16ChngRateLmt' */
} DW_LCETCS_vCordTarWhlSpin_f_T;

typedef struct {
  B_LCETCS_vCordTarWhlSpin_c_T rtb;
  DW_LCETCS_vCordTarWhlSpin_f_T rtdw;
} MdlrefDW_LCETCS_vCordTarWhlSpin_T;

/* Model reference registration function */
extern void LCETCS_vCordTarWhlSpin_initialize(void);
extern void LCETCS_vCordTarWhlSpin_Init(B_LCETCS_vCordTarWhlSpin_c_T *localB,
  DW_LCETCS_vCordTarWhlSpin_f_T *localDW);
extern void LCETCS_vCordTarWhlSpin_Start(B_LCETCS_vCordTarWhlSpin_c_T *localB);
extern void LCETCS_vCordTarWhlSpin(int16_T rtu_lcetcss16CordBsTarWhlSpin,
  int16_T rtu_lcetcss16TarWhlSpinInc, int16_T rtu_lcetcss16TarWhlSpdStlDetd,
  int16_T rtu_lcetcss16TarWhlSpinDec, int16_T *rty_lcetcss16TarWhlSpin,
  B_LCETCS_vCordTarWhlSpin_c_T *localB, DW_LCETCS_vCordTarWhlSpin_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCordTarWhlSpin'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCordTarWhlSpin_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
