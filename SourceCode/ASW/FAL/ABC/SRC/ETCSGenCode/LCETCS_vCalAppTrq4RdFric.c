/*
 * File: LCETCS_vCalAppTrq4RdFric.c
 *
 * Code generated for Simulink model 'LCETCS_vCalAppTrq4RdFric'.
 *
 * Model version                  : 1.126
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:08:48 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalAppTrq4RdFric.h"
#include "LCETCS_vCalAppTrq4RdFric_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalAppTrq4RdFric' */
void LCETCS_vCalAppTrq4RdFric_Init(int32_T *rty_lcetcss32AppTrq4RdFric,
  DW_LCETCS_vCalAppTrq4RdFric_f_T *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vConvAx2CrdnTrq' */
  LCETCS_vConvAx2CrdnTrq_Init(&(localDW->LCETCS_vConvAx2CrdnTrq_DWORK1.rtb));

  /* InitializeConditions for Chart: '<Root>/Chart' */
  *rty_lcetcss32AppTrq4RdFric = 0;
}

/* Start for referenced model: 'LCETCS_vCalAppTrq4RdFric' */
void LCETCS_vCalAppTrq4RdFric_Start(DW_LCETCS_vCalAppTrq4RdFric_f_T *localDW)
{
  /* Start for ModelReference: '<Root>/LCETCS_vConvAx2CrdnTrq' */
  LCETCS_vConvAx2CrdnTrq_Start(&(localDW->LCETCS_vConvAx2CrdnTrq_DWORK1.rtb));
}

/* Output and update for referenced model: 'LCETCS_vCalAppTrq4RdFric' */
void LCETCS_vCalAppTrq4RdFric(const TypeETCSCyl1stStruct *rtu_ETCS_CYL_1ST,
  const TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC, const TypeETCSCtlCmdStruct
  *rtu_ETCS_CTL_CMD_OLD, const TypeETCSH2LStruct *rtu_ETCS_H2L, int32_T
  *rty_lcetcss32AppTrq4RdFric, DW_LCETCS_vCalAppTrq4RdFric_f_T *localDW)
{
  /* local block i/o variables */
  int32_T rtb_CrdnTrq;

  /* ModelReference: '<Root>/LCETCS_vConvAx2CrdnTrq' */
  LCETCS_vConvAx2CrdnTrq(rtu_ETCS_VEH_ACC->lcetcss16RsltntAcc, &rtb_CrdnTrq,
    &(localDW->LCETCS_vConvAx2CrdnTrq_DWORK1.rtb));

  /* Chart: '<Root>/Chart' */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:79' */
  if ((rtu_ETCS_CYL_1ST->lcetcsu1Cyl1st == 1) || (rtu_ETCS_H2L->lcetcsu1HiToLw ==
       1)) {
    /* Transition: '<S1>:78' */
    /* Transition: '<S1>:77' */
    *rty_lcetcss32AppTrq4RdFric = rtb_CrdnTrq;

    /* Transition: '<S1>:76' */
  } else {
    /* Transition: '<S1>:75' */
    if (rtb_CrdnTrq >= rtu_ETCS_CTL_CMD_OLD->lcetcss32CardanTrqCmd) {
      *rty_lcetcss32AppTrq4RdFric = rtb_CrdnTrq;
    } else {
      *rty_lcetcss32AppTrq4RdFric = rtu_ETCS_CTL_CMD_OLD->lcetcss32CardanTrqCmd;
    }
  }

  /* End of Chart: '<Root>/Chart' */
  /* Transition: '<S1>:74' */
}

/* Model initialize function */
void LCETCS_vCalAppTrq4RdFric_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vConvAx2CrdnTrq' */
  LCETCS_vConvAx2CrdnTrq_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
