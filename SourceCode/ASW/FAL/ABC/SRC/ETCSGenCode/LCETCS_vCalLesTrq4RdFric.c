/*
 * File: LCETCS_vCalLesTrq4RdFric.c
 *
 * Code generated for Simulink model 'LCETCS_vCalLesTrq4RdFric'.
 *
 * Model version                  : 1.123
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:52:44 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalLesTrq4RdFric.h"
#include "LCETCS_vCalLesTrq4RdFric_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalLesTrq4RdFric' */
void LCETCS_vCalLesTrq4RdFric_Init(int32_T *rty_lcetcss32LesTrq4RdFric, int32_T *
  rty_lcetcss32TrqAtLowSpnDcted, DW_LCETCS_vCalLesTrq4RdFric_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = 0;

  /* InitializeConditions for Chart: '<Root>/Chart' */
  *rty_lcetcss32LesTrq4RdFric = 0;
  *rty_lcetcss32TrqAtLowSpnDcted = 0;
}

/* Output and update for referenced model: 'LCETCS_vCalLesTrq4RdFric' */
void LCETCS_vCalLesTrq4RdFric(int32_T rtu_lcetcss32AppTrq4RdFric, const
  TypeETCSLowWhlSpnStruct *rtu_ETCS_LOW_WHL_SPN, int32_T
  *rty_lcetcss32LesTrq4RdFric, int32_T *rty_lcetcss32TrqAtLowSpnDcted,
  DW_LCETCS_vCalLesTrq4RdFric_f_T *localDW)
{
  /* Chart: '<Root>/Chart' incorporates:
   *  UnitDelay: '<Root>/Unit Delay2'
   */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:28' */
  /* comment */
  if (rtu_ETCS_LOW_WHL_SPN->lcetcss16LowWhlSpnCnt > 0) {
    /* Transition: '<S1>:29' */
    /* Transition: '<S1>:56' */
    if ((rtu_ETCS_LOW_WHL_SPN->lcetcss16LowWhlSpnCnt == 1) &&
        (rtu_ETCS_LOW_WHL_SPN->lcetcsu1LowWhlSpnCntInc == 1)) {
      /* Transition: '<S1>:48' */
      /* Transition: '<S1>:49' */
      *rty_lcetcss32TrqAtLowSpnDcted = rtu_lcetcss32AppTrq4RdFric;

      /* Transition: '<S1>:59' */
    } else {
      /* Transition: '<S1>:58' */
      *rty_lcetcss32TrqAtLowSpnDcted = localDW->UnitDelay2_DSTATE;
    }

    /* Transition: '<S1>:61' */
    /*  50 means 1 s  */
    *rty_lcetcss32LesTrq4RdFric = ((((((int16_T)S16ETCSCpCdrnTrqAtAsphalt) * 10)
      - (*rty_lcetcss32TrqAtLowSpnDcted)) / 50) *
      rtu_ETCS_LOW_WHL_SPN->lcetcss16LowWhlSpnCnt) +
      (*rty_lcetcss32TrqAtLowSpnDcted);

    /* Transition: '<S1>:62' */
  } else {
    /* Transition: '<S1>:32' */
    *rty_lcetcss32TrqAtLowSpnDcted = 0;
    *rty_lcetcss32LesTrq4RdFric = 0;
  }

  /* End of Chart: '<Root>/Chart' */

  /* Update for UnitDelay: '<Root>/Unit Delay2' */
  /* Transition: '<S1>:31' */
  localDW->UnitDelay2_DSTATE = *rty_lcetcss32TrqAtLowSpnDcted;
}

/* Model initialize function */
void LCETCS_vCalLesTrq4RdFric_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
