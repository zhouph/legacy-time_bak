/*
 * File: LCETCS_vDctHiToLw.c
 *
 * Code generated for Simulink model 'LCETCS_vDctHiToLw'.
 *
 * Model version                  : 1.225
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:06:01 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctHiToLw.h"
#include "LCETCS_vDctHiToLw_private.h"

const rtTimingBridge *LCETCS_vDctHiToLw_TimingBrdg;
const TypeETCSH2LStruct LCETCS_vDctHiToLw_rtZTypeETCSH2LStruct = {
  0,                                   /* lcetcss16DeltaLonAcc */
  false,                               /* lcetcsu1HiToLw */
  false,                               /* lcetcsu1HiToLwRsgEdg */
  0,                                   /* lcetcss8HiToLwHoldCnt */
  0                                    /* lcetcss16HiToLwResetTh */
} ;                                    /* TypeETCSH2LStruct ground */

/* Initial conditions for referenced model: 'LCETCS_vDctHiToLw' */
void LCETCS_vDctHiToLw_Init(TypeETCSH2LStruct *rty_ETCS_H2L,
  DW_LCETCS_vDctHiToLw_f_T *localDW, B_LCETCS_vDctHiToLw_c_T *localB)
{
  int32_T i;

  /* InitializeConditions for Delay: '<Root>/Delay1' */
  localDW->CircBufIdx = 0;
  for (i = 0; i < 25; i++) {
    /* InitializeConditions for Delay: '<Root>/Delay1' */
    localDW->Delay1_DSTATE[i] = 0;

    /* InitializeConditions for Delay: '<Root>/Delay2' */
    localDW->Delay2_DSTATE[i] = 0;
  }

  /* InitializeConditions for Delay: '<Root>/Delay2' */
  localDW->CircBufIdx_b = 0;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = LCETCS_vDctHiToLw_rtZTypeETCSH2LStruct;

  /* InitializeConditions for Chart: '<Root>/Chart' */
  localB->lcetcsu1HiToLw = false;

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctRisingEdge' */
  LCETCS_vDctRisingEdge_Init(&localB->lcetcsu1HiToLwRsgEdg,
    &(localDW->LCETCS_vDctRisingEdge_DWORK1.rtdw));

  /* InitializeConditions for BusCreator: '<Root>/Bus Creator' incorporates:
   *  InitializeConditions for Chart: '<Root>/Chart'
   */
  if (rtmIsFirstInitCond()) {
    rty_ETCS_H2L->lcetcss16DeltaLonAcc = 0;
    rty_ETCS_H2L->lcetcsu1HiToLw = localB->lcetcsu1HiToLw;
    rty_ETCS_H2L->lcetcsu1HiToLwRsgEdg = localB->lcetcsu1HiToLwRsgEdg;
    rty_ETCS_H2L->lcetcss8HiToLwHoldCnt = 0;
    rty_ETCS_H2L->lcetcss16HiToLwResetTh = 0;
  }

  /* End of InitializeConditions for BusCreator: '<Root>/Bus Creator' */
}

/* Output and update for referenced model: 'LCETCS_vDctHiToLw' */
void LCETCS_vDctHiToLw(const TypeETCSErrZroCrsStruct *rtu_ETCS_ERR_ZRO_CRS,
  const TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC, const TypeETCSWhlSpinStruct
  *rtu_ETCS_WHL_SPIN, TypeETCSH2LStruct *rty_ETCS_H2L, DW_LCETCS_vDctHiToLw_f_T *
  localDW, B_LCETCS_vDctHiToLw_c_T *localB)
{
  int8_T rtb_Saturation;
  int16_T rtb_Delay1;
  int8_T lcetcss8HiToLwHoldCnt;
  int16_T lcetcss16HiToLwResetTh;
  int8_T rtb_Saturation_0;
  int32_T tmp;
  int32_T tmp_0;

  /* Saturate: '<Root>/Saturation' incorporates:
   *  Constant: '<Root>/Constant'
   */
  if (((int8_T)S8ETCSCpH2LTrnsTime) > 25) {
    rtb_Saturation = 25;
  } else if (((int8_T)S8ETCSCpH2LTrnsTime) < 1) {
    rtb_Saturation = 1;
  } else {
    rtb_Saturation = ((int8_T)S8ETCSCpH2LTrnsTime);
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Delay: '<Root>/Delay1' */
  if (rtb_Saturation <= localDW->CircBufIdx) {
    rtb_Saturation_0 = (int8_T)(localDW->CircBufIdx - rtb_Saturation);
  } else {
    rtb_Saturation_0 = (int8_T)(((int8_T)(localDW->CircBufIdx - rtb_Saturation))
      + 25);
  }

  if (rtb_Saturation <= localDW->CircBufIdx) {
    lcetcss8HiToLwHoldCnt = (int8_T)(localDW->CircBufIdx - rtb_Saturation);
  } else {
    lcetcss8HiToLwHoldCnt = (int8_T)(((int8_T)(localDW->CircBufIdx -
      rtb_Saturation)) + 25);
  }

  rtb_Delay1 = localDW->Delay1_DSTATE[lcetcss8HiToLwHoldCnt];

  /* Chart: '<Root>/Chart' incorporates:
   *  Delay: '<Root>/Delay1'
   *  Delay: '<Root>/Delay2'
   *  UnitDelay: '<Root>/Unit Delay'
   */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:8' */
  /* comment */
  tmp_0 = rtu_ETCS_VEH_ACC->lcetcss16VehLongAcc4Ctl - localDW->
    Delay1_DSTATE[rtb_Saturation_0];
  if (tmp_0 > 32767) {
    tmp_0 = 32767;
  } else {
    if (tmp_0 < -32768) {
      tmp_0 = -32768;
    }
  }

  lcetcss16HiToLwResetTh = (int16_T)(((((int8_T)S8ETCSCpH2LDctHiMuTh) - ((int8_T)
    S8ETCSCpH2LDctLwMuTh)) / 2) + ((int8_T)S8ETCSCpH2LDctLwMuTh));
  if (localDW->UnitDelay_DSTATE.lcetcsu1HiToLw == 0) {
    /* Transition: '<S1>:13' */
    /* Transition: '<S1>:40' */
    lcetcss8HiToLwHoldCnt = 0;

    /* Delay: '<Root>/Delay2' */
    if (rtb_Saturation <= localDW->CircBufIdx_b) {
      rtb_Saturation = (int8_T)(localDW->CircBufIdx_b - rtb_Saturation);
    } else {
      rtb_Saturation = (int8_T)(((int8_T)(localDW->CircBufIdx_b - rtb_Saturation))
        + 25);
    }

    if ((((((rtu_ETCS_VEH_ACC->lcetcss16VehLongAcc4Ctl >= 0) &&
            (rtu_ETCS_VEH_ACC->lcetcss16VehLongAcc4Ctl < ((int8_T)
              S8ETCSCpH2LDctLwMuTh))) && (localDW->
            Delay1_DSTATE[rtb_Saturation_0] > ((int8_T)S8ETCSCpH2LDctHiMuTh))) &&
          (localDW->Delay2_DSTATE[rtb_Saturation] > ((int8_T)
            S8ETCSCpH2LDctHiMuTh))) && (((int16_T)tmp_0) < ((int8_T)
           S8ETCSCpH2LDctDltTh))) && (rtu_ETCS_WHL_SPIN->lcetcss16WhlSpin >
         ((uint8_T)U8ETCSCpH2LDctSpnTh))) {
      /* Transition: '<S1>:41' */
      /* Transition: '<S1>:44' */
      localB->lcetcsu1HiToLw = true;

      /* Transition: '<S1>:45' */
    } else {
      /* Transition: '<S1>:42' */
      localB->lcetcsu1HiToLw = localDW->UnitDelay_DSTATE.lcetcsu1HiToLw;
    }

    /* Transition: '<S1>:72' */
    /* Transition: '<S1>:61' */
  } else {
    /* Transition: '<S1>:35' */
    tmp = localDW->UnitDelay_DSTATE.lcetcss8HiToLwHoldCnt + 1;
    if (tmp > 127) {
      tmp = 127;
    }

    lcetcss8HiToLwHoldCnt = (int8_T)tmp;
    if ((((((int8_T)tmp) >= ((uint8_T)U8ETCSCpH2LDctHldTm)) ||
          (rtu_ETCS_ERR_ZRO_CRS->lcetcsu1JumpUp == 1)) ||
         (rtu_ETCS_VEH_ACC->lcetcss16VehLongAcc4Ctl < 0)) ||
        (rtu_ETCS_VEH_ACC->lcetcss16VehLongAcc4Ctl >= lcetcss16HiToLwResetTh)) {
      /* Transition: '<S1>:59' */
      /* Transition: '<S1>:60' */
      localB->lcetcsu1HiToLw = false;

      /* Transition: '<S1>:61' */
    } else {
      /* Transition: '<S1>:62' */
      localB->lcetcsu1HiToLw = localDW->UnitDelay_DSTATE.lcetcsu1HiToLw;
    }
  }

  /* Transition: '<S1>:25' */

  /* ModelReference: '<Root>/LCETCS_vDctRisingEdge' */
  LCETCS_vDctRisingEdge(localB->lcetcsu1HiToLw, &localB->lcetcsu1HiToLwRsgEdg,
                        &(localDW->LCETCS_vDctRisingEdge_DWORK1.rtdw));

  /* BusCreator: '<Root>/Bus Creator' incorporates:
   *  Chart: '<Root>/Chart'
   */
  rty_ETCS_H2L->lcetcss16DeltaLonAcc = (int16_T)tmp_0;
  rty_ETCS_H2L->lcetcsu1HiToLw = localB->lcetcsu1HiToLw;
  rty_ETCS_H2L->lcetcsu1HiToLwRsgEdg = localB->lcetcsu1HiToLwRsgEdg;
  rty_ETCS_H2L->lcetcss8HiToLwHoldCnt = lcetcss8HiToLwHoldCnt;
  rty_ETCS_H2L->lcetcss16HiToLwResetTh = lcetcss16HiToLwResetTh;

  /* Update for Delay: '<Root>/Delay1' */
  localDW->Delay1_DSTATE[localDW->CircBufIdx] =
    rtu_ETCS_VEH_ACC->lcetcss16VehLongAcc4Ctl;
  if (localDW->CircBufIdx < 24) {
    localDW->CircBufIdx++;
  } else {
    localDW->CircBufIdx = 0;
  }

  /* End of Update for Delay: '<Root>/Delay1' */

  /* Update for Delay: '<Root>/Delay2' */
  localDW->Delay2_DSTATE[localDW->CircBufIdx_b] = rtb_Delay1;
  if (localDW->CircBufIdx_b < 24) {
    localDW->CircBufIdx_b++;
  } else {
    localDW->CircBufIdx_b = 0;
  }

  /* End of Update for Delay: '<Root>/Delay2' */

  /* Update for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = *rty_ETCS_H2L;
}

/* Model initialize function */
void LCETCS_vDctHiToLw_initialize(const rtTimingBridge *timingBridge)
{
  /* Registration code */
  LCETCS_vDctHiToLw_TimingBrdg = timingBridge;

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctRisingEdge' */
  LCETCS_vDctRisingEdge_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
