/*
 * File: LCETCS_vCalEngStallThr.h
 *
 * Code generated for Simulink model 'LCETCS_vCalEngStallThr'.
 *
 * Model version                  : 1.251
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:05:52 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalEngStallThr_h_
#define RTW_HEADER_LCETCS_vCalEngStallThr_h_
#ifndef LCETCS_vCalEngStallThr_COMMON_INCLUDES_
# define LCETCS_vCalEngStallThr_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalEngStallThr_COMMON_INCLUDES_ */

#include "LCETCS_vCalEngStallThr_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_s16Inter3Point.h"

/* Block signals for model 'LCETCS_vCalEngStallThr' */
typedef struct {
  int16_T lcetcss16EngStallThrTemp;    /* '<S1>/Model1' */
  int16_T lcetcss16EngStallThrTemp_n;  /* '<S2>/LCETCS_s16Inter3Point' */
} B_LCETCS_vCalEngStallThr_c_T;

typedef struct {
  B_LCETCS_vCalEngStallThr_c_T rtb;
} MdlrefDW_LCETCS_vCalEngStallThr_T;

/* Model reference registration function */
extern void LCETCS_vCalEngStallThr_initialize(void);
extern void LCETCS_vCalEngStallThr_Start(B_LCETCS_vCalEngStallThr_c_T *localB);
extern void LCETCS_vCalEngStallThr(int8_T rtu_lcetcss8EngStallRiskIndex, const
  TypeETCSSplitHillStruct *rtu_ETCS_SPLIT_HILL_OLD, int16_T
  *rty_lcetcss16EngStallThrTemp, B_LCETCS_vCalEngStallThr_c_T *localB);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalEngStallThr'
 * '<S1>'   : 'LCETCS_vCalEngStallThr/Engine Stall Risk Index on Flat'
 * '<S2>'   : 'LCETCS_vCalEngStallThr/Engine Stall Risk Index on Hill'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalEngStallThr_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
