/*
 * File: LCETCS_vCalTarWhlSpnIncHiRd.c
 *
 * Code generated for Simulink model 'LCETCS_vCalTarWhlSpnIncHiRd'.
 *
 * Model version                  : 1.135
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:00:14 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalTarWhlSpnIncHiRd.h"
#include "LCETCS_vCalTarWhlSpnIncHiRd_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalTarWhlSpnIncHiRd' */
void LCETCS_vCalTarWhlSpnIncHiRd_Init(B_LCETCS_vCalTarWhlSpnIncHiRd_c_T *localB)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_Init(&localB->lcetcsu8AddTar4RdAdapt);

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter2Point1' */
  LCETCS_s16Inter2Point_Init(&localB->lcetcsu8AddTar4RdAdapt_h);
}

/* Output and update for referenced model: 'LCETCS_vCalTarWhlSpnIncHiRd' */
void LCETCS_vCalTarWhlSpnIncHiRd(int16_T rtu_lcetcss16LmtCrngDltYaw, const
  TypeETCSHighDctStruct *rtu_ETCS_HI_DCT_OLD, const TypeETCSEscSigStruct
  *rtu_ETCS_ESC_SIG, uint8_T *rty_lcetcsu8AddTar4HighRd,
  B_LCETCS_vCalTarWhlSpnIncHiRd_c_T *localB)
{
  /* local block i/o variables */
  int16_T rtb_x1;
  int16_T rtb_x2;
  int16_T rtb_y1;
  int16_T rtb_y2;
  int16_T rtb_y1_i;
  int16_T rtb_y2_j;

  /* DataTypeConversion: '<Root>/Data Type Conversion' incorporates:
   *  Constant: '<Root>/Constant8'
   */
  rtb_x1 = ((uint8_T)U8ETCSCpAdaptHighRdStrtCnt);

  /* DataTypeConversion: '<Root>/Data Type Conversion1' incorporates:
   *  Constant: '<Root>/Constant4'
   */
  rtb_x2 = ((uint8_T)U8ETCSCpAdaptHighRdCplCnt);

  /* DataTypeConversion: '<Root>/Data Type Conversion2' incorporates:
   *  Constant: '<Root>/Constant5'
   */
  rtb_y1 = 0;

  /* DataTypeConversion: '<Root>/Data Type Conversion3' incorporates:
   *  Constant: '<Root>/Constant6'
   */
  rtb_y2 = ((uint8_T)U8ETCSCpTarSpnAdd4AdHiRd);

  /* DataTypeConversion: '<Root>/Data Type Conversion6' incorporates:
   *  Constant: '<Root>/Constant2'
   */
  rtb_y1_i = 0;

  /* DataTypeConversion: '<Root>/Data Type Conversion7' incorporates:
   *  Constant: '<Root>/Constant3'
   */
  rtb_y2_j = ((uint8_T)U8ETCSCpTarSpnAdd4PreHiRd);

  /* ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point(rtu_ETCS_HI_DCT_OLD->lcetcss16AdaptHighRdCnt, rtb_x1,
                        rtb_x2, rtb_y1, rtb_y2, &localB->lcetcsu8AddTar4RdAdapt);

  /* ModelReference: '<Root>/LCETCS_s16Inter2Point1' */
  LCETCS_s16Inter2Point(rtu_ETCS_HI_DCT_OLD->lcetcss16PreHighRdDctCnt, ((int16_T)
    S16ETCSCpPreHighRdDctStrtCnt), ((int16_T)S16ETCSCpPreHighRdDctCplCnt),
                        rtb_y1_i, rtb_y2_j, &localB->lcetcsu8AddTar4RdAdapt_h);

  /* Switch: '<Root>/Switch2' incorporates:
   *  Constant: '<Root>/Constant9'
   *  RelationalOperator: '<Root>/Relational Operator'
   */
  if (rtu_ETCS_ESC_SIG->lcetcss16AbsDelYawFrst <= rtu_lcetcss16LmtCrngDltYaw) {
    /* MinMax: '<Root>/MinMax' */
    if (localB->lcetcsu8AddTar4RdAdapt >= localB->lcetcsu8AddTar4RdAdapt_h) {
      *rty_lcetcsu8AddTar4HighRd = (uint8_T)localB->lcetcsu8AddTar4RdAdapt;
    } else {
      *rty_lcetcsu8AddTar4HighRd = (uint8_T)localB->lcetcsu8AddTar4RdAdapt_h;
    }

    /* End of MinMax: '<Root>/MinMax' */
  } else {
    *rty_lcetcsu8AddTar4HighRd = 0U;
  }

  /* End of Switch: '<Root>/Switch2' */
}

/* Model initialize function */
void LCETCS_vCalTarWhlSpnIncHiRd_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter2Point1' */
  LCETCS_s16Inter2Point_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
