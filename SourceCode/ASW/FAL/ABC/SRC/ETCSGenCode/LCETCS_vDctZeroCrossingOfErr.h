/*
 * File: LCETCS_vDctZeroCrossingOfErr.h
 *
 * Code generated for Simulink model 'LCETCS_vDctZeroCrossingOfErr'.
 *
 * Model version                  : 1.126
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:06:50 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vDctZeroCrossingOfErr_h_
#define RTW_HEADER_LCETCS_vDctZeroCrossingOfErr_h_
#ifndef LCETCS_vDctZeroCrossingOfErr_COMMON_INCLUDES_
# define LCETCS_vDctZeroCrossingOfErr_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vDctZeroCrossingOfErr_COMMON_INCLUDES_ */

#include "LCETCS_vDctZeroCrossingOfErr_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vDctFallingEdge.h"

/* Block signals for model 'LCETCS_vDctZeroCrossingOfErr' */
typedef struct {
  boolean_T lcetcsu1JumpUpFalEdg;      /* '<Root>/LCETCS_vDctFallingEdge' */
  boolean_T lcetcsu1JumpDnFalEdg;      /* '<Root>/LCETCS_vDctFallingEdge1' */
  boolean_T lcetcsu1JumpUp;            /* '<Root>/Chart' */
  boolean_T lcetcsu1JumpDn;            /* '<Root>/Chart' */
} B_LCETCS_vDctZeroCrossingOfErr_c_T;

/* Block states (auto storage) for model 'LCETCS_vDctZeroCrossingOfErr' */
typedef struct {
  int16_T UnitDelay_DSTATE;            /* '<Root>/Unit Delay' */
  MdlrefDW_LCETCS_vDctFallingEdge_T LCETCS_vDctFallingEdge_DWORK1;/* '<Root>/LCETCS_vDctFallingEdge' */
  MdlrefDW_LCETCS_vDctFallingEdge_T LCETCS_vDctFallingEdge1_DWORK1;/* '<Root>/LCETCS_vDctFallingEdge1' */
} DW_LCETCS_vDctZeroCrossingOfErr_f_T;

typedef struct {
  B_LCETCS_vDctZeroCrossingOfErr_c_T rtb;
  DW_LCETCS_vDctZeroCrossingOfErr_f_T rtdw;
} MdlrefDW_LCETCS_vDctZeroCrossingOfErr_T;

/* Model reference registration function */
extern void LCETCS_vDctZeroCrossingOfErr_initialize(const rtTimingBridge
  *timingBridge);
extern void LCETCS_vDctZeroCrossingOfErr_Init(TypeETCSErrZroCrsStruct
  *rty_ETCS_ERR_ZRO_CRS, DW_LCETCS_vDctZeroCrossingOfErr_f_T *localDW,
  B_LCETCS_vDctZeroCrossingOfErr_c_T *localB);
extern void LCETCS_vDctZeroCrossingOfErr(const TypeETCSCyl1stStruct
  *rtu_ETCS_CYL_1ST, const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const
  TypeETCSCtlErrStruct *rtu_ETCS_CTL_ERR, TypeETCSErrZroCrsStruct
  *rty_ETCS_ERR_ZRO_CRS, DW_LCETCS_vDctZeroCrossingOfErr_f_T *localDW,
  B_LCETCS_vDctZeroCrossingOfErr_c_T *localB);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vDctZeroCrossingOfErr'
 * '<S1>'   : 'LCETCS_vDctZeroCrossingOfErr/Chart'
 */
#endif                                 /* RTW_HEADER_LCETCS_vDctZeroCrossingOfErr_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
