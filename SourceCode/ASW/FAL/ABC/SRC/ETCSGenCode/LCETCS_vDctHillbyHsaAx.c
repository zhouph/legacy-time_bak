/*
 * File: LCETCS_vDctHillbyHsaAx.c
 *
 * Code generated for Simulink model 'LCETCS_vDctHillbyHsaAx'.
 *
 * Model version                  : 1.295
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:57:58 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctHillbyHsaAx.h"
#include "LCETCS_vDctHillbyHsaAx_private.h"

/* Output and update for referenced model: 'LCETCS_vDctHillbyHsaAx' */
void LCETCS_vDctHillbyHsaAx(int16_T rtu_lcetcss16LongAccFilter, const
  TypeETCSExtDctStruct *rtu_ETCS_EXT_DCT, boolean_T *rty_lcetcsu1HillbyHsaAtv)
{
  /* Logic: '<Root>/Logical Operator1' incorporates:
   *  Constant: '<Root>/Ax Threshold for Hill Detection'
   *  RelationalOperator: '<Root>/Relational Operator1'
   *  RelationalOperator: '<Root>/Relational Operator2'
   */
  *rty_lcetcsu1HillbyHsaAtv = ((rtu_ETCS_EXT_DCT->lcetcsu1HsaAtv) &&
    (rtu_lcetcss16LongAccFilter >= ((uint8_T)U8ETCSCpHillDctAlongTh)));
}

/* Model initialize function */
void LCETCS_vDctHillbyHsaAx_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
