/*
 * File: LCETCS_vDctSplitHill_private.h
 *
 * Code generated for Simulink model 'LCETCS_vDctSplitHill'.
 *
 * Model version                  : 1.593
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:15:05 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vDctSplitHill_private_h_
#define RTW_HEADER_LCETCS_vDctSplitHill_private_h_
#include "rtwtypes.h"
#include "model_reference_types.h"

/* Includes for objects with custom storage classes. */
#include "../ETCSGenCodeHeader/ETCSHeader4Import.h"

/*
 * Generate compile time checks that imported macros for parameters
 * with storage class "ImportedDefine" are defined
 */
#ifndef S16ETCSCpSplitHillClrEngSpd
#error The variable for the parameter "S16ETCSCpSplitHillClrEngSpd" is not defined
#endif

#ifndef S16ETCSCpSplitHillDctEngSpd
#error The variable for the parameter "S16ETCSCpSplitHillDctEngSpd" is not defined
#endif

#ifndef VREF_0_5_KPH
#error The variable for the parameter "VREF_0_5_KPH" is not defined
#endif

#ifndef VREF_25_KPH
#error The variable for the parameter "VREF_25_KPH" is not defined
#endif

#ifndef GRAD_10_DEG
#error The variable for the parameter "GRAD_10_DEG" is not defined
#endif

#ifndef GRAD_8_DEG
#error The variable for the parameter "GRAD_8_DEG" is not defined
#endif

#ifndef L_U8FILTER_GAIN_20MSLOOP_7HZ
#error The variable for the parameter "L_U8FILTER_GAIN_20MSLOOP_7HZ" is not defined
#endif

#ifndef U8ETCSCpGradientTrstCplCnt
#error The variable for the parameter "U8ETCSCpGradientTrstCplCnt" is not defined
#endif

#ifndef U8ETCSCpGradientTrstStCnt
#error The variable for the parameter "U8ETCSCpGradientTrstStCnt" is not defined
#endif

#ifndef U8ETCSCpGrdTransTme
#error The variable for the parameter "U8ETCSCpGrdTransTme" is not defined
#endif

#ifndef U8ETCSCpHillClrTime
#error The variable for the parameter "U8ETCSCpHillClrTime" is not defined
#endif

#ifndef U8ETCSCpHillDctAlongTh
#error The variable for the parameter "U8ETCSCpHillDctAlongTh" is not defined
#endif

#ifndef U8ETCSCpHillDctTime
#error The variable for the parameter "U8ETCSCpHillDctTime" is not defined
#endif

#ifndef U8ETCSCpHillDctTimeAx
#error The variable for the parameter "U8ETCSCpHillDctTimeAx" is not defined
#endif

#ifndef U8ETCSCpHillDiffAx
#error The variable for the parameter "U8ETCSCpHillDiffAx" is not defined
#endif

#ifndef U8ETCSCpHsaHillTime
#error The variable for the parameter "U8ETCSCpHsaHillTime" is not defined
#endif

#ifndef U8ETCSCpSplitHillClrAccel
#error The variable for the parameter "U8ETCSCpSplitHillClrAccel" is not defined
#endif

#ifndef U8ETCSCpSplitHillClrVehSpd
#error The variable for the parameter "U8ETCSCpSplitHillClrVehSpd" is not defined
#endif

#ifndef U8ETCSCpSplitHillDctAccel
#error The variable for the parameter "U8ETCSCpSplitHillDctAccel" is not defined
#endif

#ifndef U8ETCSCpSplitHillDctMinAx
#error The variable for the parameter "U8ETCSCpSplitHillDctMinAx" is not defined
#endif

#ifndef U8ETCSCpSplitHillDctVehSpd
#error The variable for the parameter "U8ETCSCpSplitHillDctVehSpd" is not defined
#endif

#ifndef U8ETCSCpTarTransCplBrkTrq2Sp
#error The variable for the parameter "U8ETCSCpTarTransCplBrkTrq2Sp" is not defined
#endif

#ifndef U8ETCSCpTarTransStBrkTrq2Sp
#error The variable for the parameter "U8ETCSCpTarTransStBrkTrq2Sp" is not defined
#endif

#ifndef VREF_30_KPH
#error The variable for the parameter "VREF_30_KPH" is not defined
#endif

#ifndef VREF_DECEL_0_3_G
#error The variable for the parameter "VREF_DECEL_0_3_G" is not defined
#endif
#endif                                 /* RTW_HEADER_LCETCS_vDctSplitHill_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
