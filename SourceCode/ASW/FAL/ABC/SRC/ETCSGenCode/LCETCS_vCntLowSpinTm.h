/*
 * File: LCETCS_vCntLowSpinTm.h
 *
 * Code generated for Simulink model 'LCETCS_vCntLowSpinTm'.
 *
 * Model version                  : 1.111
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:04:49 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCntLowSpinTm_h_
#define RTW_HEADER_LCETCS_vCntLowSpinTm_h_
#ifndef LCETCS_vCntLowSpinTm_COMMON_INCLUDES_
# define LCETCS_vCntLowSpinTm_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCntLowSpinTm_COMMON_INCLUDES_ */

#include "LCETCS_vCntLowSpinTm_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_s16CntIfSigSmlThanTh.h"

/* Block signals for model 'LCETCS_vCntLowSpinTm' */
typedef struct {
  int16_T CounterUpTh;                 /* '<Root>/Constant' */
  int16_T CounterDownTh;               /* '<Root>/Constant1' */
  int16_T Counter;                     /* '<Root>/LCETCS_s16CntIfSigSmlThanTh' */
  int16_T lcetcss16LowWhlSpnCnt;       /* '<Root>/Signal Conversion1' */
  boolean_T lcetcsu1LowWhlSpnCntReset; /* '<Root>/Chart' */
  boolean_T lcetcsu1LowWhlSpnCntReset_j;/* '<Root>/Signal Conversion' */
  boolean_T lcetcsu1LowWhlSpnCntInc_m; /* '<Root>/Signal Conversion2' */
} B_LCETCS_vCntLowSpinTm_c_T;

typedef struct {
  B_LCETCS_vCntLowSpinTm_c_T rtb;
} MdlrefDW_LCETCS_vCntLowSpinTm_T;

/* Model reference registration function */
extern void LCETCS_vCntLowSpinTm_initialize(const rtTimingBridge *timingBridge);
extern void LCETCS_vCntLowSpinTm_Init(TypeETCSLowWhlSpnStruct
  *rty_ETCS_LOW_WHL_SPN, B_LCETCS_vCntLowSpinTm_c_T *localB);
extern void LCETCS_vCntLowSpinTm_Start(B_LCETCS_vCntLowSpinTm_c_T *localB);
extern void LCETCS_vCntLowSpinTm(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT,
  const TypeETCSCtlErrStruct *rtu_ETCS_CTL_ERR, const TypeETCSWhlSpinStruct
  *rtu_ETCS_WHL_SPIN, const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const
  TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN, const TypeETCSReqVCAStruct
  *rtu_ETCS_REQ_VCA, TypeETCSLowWhlSpnStruct *rty_ETCS_LOW_WHL_SPN,
  B_LCETCS_vCntLowSpinTm_c_T *localB);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCntLowSpinTm'
 * '<S1>'   : 'LCETCS_vCntLowSpinTm/Chart'
 * '<S2>'   : 'LCETCS_vCntLowSpinTm/Chart1'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCntLowSpinTm_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
