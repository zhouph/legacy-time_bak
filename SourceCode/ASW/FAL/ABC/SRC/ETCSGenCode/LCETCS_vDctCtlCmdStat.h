/*
 * File: LCETCS_vDctCtlCmdStat.h
 *
 * Code generated for Simulink model 'LCETCS_vDctCtlCmdStat'.
 *
 * Model version                  : 1.197
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:51:34 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vDctCtlCmdStat_h_
#define RTW_HEADER_LCETCS_vDctCtlCmdStat_h_
#ifndef LCETCS_vDctCtlCmdStat_COMMON_INCLUDES_
# define LCETCS_vDctCtlCmdStat_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vDctCtlCmdStat_COMMON_INCLUDES_ */

#include "LCETCS_vDctCtlCmdStat_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Block states (auto storage) for model 'LCETCS_vDctCtlCmdStat' */
typedef struct {
  boolean_T UnitDelay1_DSTATE;         /* '<Root>/Unit Delay1' */
} DW_LCETCS_vDctCtlCmdStat_f_T;

typedef struct {
  DW_LCETCS_vDctCtlCmdStat_f_T rtdw;
} MdlrefDW_LCETCS_vDctCtlCmdStat_T;

/* Model reference registration function */
extern void LCETCS_vDctCtlCmdStat_initialize(void);
extern void LCETCS_vDctCtlCmdStat_Init(boolean_T *rty_lcetcsu1CtlCmdHitMax,
  boolean_T *rty_lcetcsu1CtlCmdInc, DW_LCETCS_vDctCtlCmdStat_f_T *localDW);
extern void LCETCS_vDctCtlCmdStat(int32_T rtu_lcetcss32CardanTrqCmd, const
  TypeETCSCrdnTrqStruct *rtu_ETCS_CRDN_TRQ, const TypeETCSCtlCmdStruct
  *rtu_ETCS_CTL_CMD_OLD, boolean_T *rty_lcetcsu1CtlCmdHitMax, boolean_T
  *rty_lcetcsu1CtlCmdInc, DW_LCETCS_vDctCtlCmdStat_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vDctCtlCmdStat'
 * '<S1>'   : 'LCETCS_vDctCtlCmdStat/Detection of control command status'
 */
#endif                                 /* RTW_HEADER_LCETCS_vDctCtlCmdStat_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
