/*
 * File: LCETCS_vCalGradientTrust.c
 *
 * Code generated for Simulink model 'LCETCS_vCalGradientTrust'.
 *
 * Model version                  : 1.316
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:57:25 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalGradientTrust.h"
#include "LCETCS_vCalGradientTrust_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalGradientTrust' */
void LCETCS_vCalGradientTrust_Init(boolean_T *rty_lcetcsu1GradientTrstSet)
{
  /* InitializeConditions for Chart: '<Root>/Chart' */
  *rty_lcetcsu1GradientTrstSet = false;
}

/* Output and update for referenced model: 'LCETCS_vCalGradientTrust' */
void LCETCS_vCalGradientTrust(boolean_T rtu_lcetcsu1InhibitGradient, boolean_T
  rtu_lcetcsu1DctHoldGradient, int16_T rtu_lcetcss16GradientTrstCnt, boolean_T
  *rty_lcetcsu1GradientTrstSet)
{
  /* Chart: '<Root>/Chart' */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:9' */
  /* comment */
  /*   */
  if (((rtu_lcetcsu1InhibitGradient == 0) && (rtu_lcetcsu1DctHoldGradient == 0))
      && (rtu_lcetcss16GradientTrstCnt >= ((uint8_T)U8ETCSCpGradientTrstStCnt)))
  {
    /* Transition: '<S1>:171' */
    /* Transition: '<S1>:169' */
    *rty_lcetcsu1GradientTrstSet = true;

    /* Transition: '<S1>:195' */
  } else {
    /* Transition: '<S1>:194' */
    *rty_lcetcsu1GradientTrstSet = false;
  }

  /* End of Chart: '<Root>/Chart' */
  /* Transition: '<S1>:196' */
}

/* Model initialize function */
void LCETCS_vCalGradientTrust_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
