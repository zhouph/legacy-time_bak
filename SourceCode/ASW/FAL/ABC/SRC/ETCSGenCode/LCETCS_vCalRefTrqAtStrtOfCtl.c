/*
 * File: LCETCS_vCalRefTrqAtStrtOfCtl.c
 *
 * Code generated for Simulink model 'LCETCS_vCalRefTrqAtStrtOfCtl'.
 *
 * Model version                  : 1.193
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:54:40 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalRefTrqAtStrtOfCtl.h"
#include "LCETCS_vCalRefTrqAtStrtOfCtl_private.h"

/* Output and update for referenced model: 'LCETCS_vCalRefTrqAtStrtOfCtl' */
void LCETCS_vCalRefTrqAtStrtOfCtl(const TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC,
  const TypeETCSHighDctStruct *rtu_ETCS_HI_DCT, int16_T rtu_lcetcss16VehSpd,
  TypeETCSRefTrqStrCtlStruct *rty_ETCS_REF_TRQ_STR_CTL)
{
  int32_T rtb_Switch1;
  int16_T rtb_Prelookup1_o1;
  real32_T rtb_Prelookup1_o2;
  int16_T rtb_Prelookup_o1;
  real32_T rtb_Prelookup_o2;
  int16_T rtb_VectorConcatenate3[15];
  int16_T rtb_VectorConcatenate1[5];
  int16_T rtb_VectorConcatenate[3];
  real32_T frac[2];
  int16_T bpIndex[2];

  /* Outputs for Atomic SubSystem: '<Root>/Subsystem' */
  /* Switch: '<S1>/Switch1' incorporates:
   *  Constant: '<S1>/Asphalt Ice Road Friction Torque At VehSpd 1'
   *  Constant: '<S1>/Asphalt Ice Road Friction Torque At VehSpd 2'
   *  Constant: '<S1>/Asphalt Ice Road Friction Torque At VehSpd 3'
   *  Constant: '<S1>/Asphalt Ice Road Friction Torque At VehSpd 4'
   *  Constant: '<S1>/Asphalt Ice Road Friction Torque At VehSpd 5'
   *  Constant: '<S1>/Asphalt Road Friction Torque At VehSpd_1'
   *  Constant: '<S1>/HighRdDctCnt_2'
   *  Constant: '<S1>/Ice Road Friction Torque At VehSpd_1'
   *  Constant: '<S1>/Ice Road Friction Torque At VehSpd_2'
   *  Constant: '<S1>/Ice Road Friction Torque At VehSpd_3'
   *  Constant: '<S1>/Ice Road Friction Torque At VehSpd_4'
   *  Constant: '<S1>/Ice Road Friction Torque At VehSpd_5'
   *  Constant: '<S1>/Snow Road Friction Torque At VehSpd_1'
   *  Constant: '<S1>/Snow Road Friction Torque At VehSpd_2'
   *  Constant: '<S1>/Snow Road Friction Torque At VehSpd_3'
   *  Constant: '<S1>/Snow Road Friction Torque At VehSpd_4'
   *  Constant: '<S1>/Snow Road Friction Torque At VehSpd_5'
   *  Constant: '<S1>/Vehicle Speed Break Point 1'
   *  Constant: '<S1>/Vehicle Speed Break Point 2'
   *  Constant: '<S1>/Vehicle Speed Break Point 3'
   *  Constant: '<S1>/Vehicle Speed Break Point 4'
   *  Constant: '<S1>/Vehicle Speed Break Point 5'
   *  Interpolation_n-D: '<S1>/Interpolation Using Prelookup'
   *  RelationalOperator: '<S1>/Relational Operator'
   */
  if (rtu_ETCS_HI_DCT->lcetcss16PreHighRdDctCnt >= ((int16_T)
       S16ETCSCpPreHighRdDctCplCnt)) {
    rtb_Switch1 = ((int16_T)S16ETCSCpCdrnTrqAtAspSpd_1);
  } else {
    rtb_VectorConcatenate3[13] = ((int16_T)S16ETCSCpCdrnTrqAtSnowSpd_5);
    rtb_VectorConcatenate3[12] = ((int16_T)S16ETCSCpCdrnTrqAtIceSpd_5);
    rtb_VectorConcatenate3[14] = ((int16_T)S16ETCSCpCdrnTrqAtAspSpd_5);
    rtb_VectorConcatenate3[10] = ((int16_T)S16ETCSCpCdrnTrqAtSnowSpd_4);
    rtb_VectorConcatenate3[9] = ((int16_T)S16ETCSCpCdrnTrqAtIceSpd_4);
    rtb_VectorConcatenate3[11] = ((int16_T)S16ETCSCpCdrnTrqAtAspSpd_4);
    rtb_VectorConcatenate3[7] = ((int16_T)S16ETCSCpCdrnTrqAtSnowSpd_3);
    rtb_VectorConcatenate3[6] = ((int16_T)S16ETCSCpCdrnTrqAtIceSpd_3);
    rtb_VectorConcatenate3[8] = ((int16_T)S16ETCSCpCdrnTrqAtAspSpd_3);
    rtb_VectorConcatenate3[4] = ((int16_T)S16ETCSCpCdrnTrqAtSnowSpd_2);
    rtb_VectorConcatenate3[3] = ((int16_T)S16ETCSCpCdrnTrqAtIceSpd_2);
    rtb_VectorConcatenate3[5] = ((int16_T)S16ETCSCpCdrnTrqAtAspSpd_2);
    rtb_VectorConcatenate3[1] = ((int16_T)S16ETCSCpCdrnTrqAtSnowSpd_1);
    rtb_VectorConcatenate3[0] = ((int16_T)S16ETCSCpCdrnTrqAtIceSpd_1);
    rtb_VectorConcatenate3[2] = ((int16_T)S16ETCSCpCdrnTrqAtAspSpd_1);
    rtb_VectorConcatenate1[4] = ((int16_T)S16ETCSCpSpd_5);
    rtb_VectorConcatenate1[3] = ((int16_T)S16ETCSCpSpd_4);
    rtb_VectorConcatenate1[2] = ((int16_T)S16ETCSCpSpd_3);
    rtb_VectorConcatenate1[1] = ((int16_T)S16ETCSCpSpd_2);
    rtb_VectorConcatenate1[0] = ((int16_T)S16ETCSCpSpd_1);

    /* PreLookup: '<S1>/Prelookup1' incorporates:
     *  Constant: '<S1>/Asphalt Ice Road Friction Torque At VehSpd 1'
     *  Constant: '<S1>/Asphalt Ice Road Friction Torque At VehSpd 2'
     *  Constant: '<S1>/Asphalt Ice Road Friction Torque At VehSpd 3'
     *  Constant: '<S1>/Asphalt Ice Road Friction Torque At VehSpd 4'
     *  Constant: '<S1>/Asphalt Ice Road Friction Torque At VehSpd 5'
     *  Constant: '<S1>/Ice Road Friction Torque At VehSpd_1'
     *  Constant: '<S1>/Ice Road Friction Torque At VehSpd_2'
     *  Constant: '<S1>/Ice Road Friction Torque At VehSpd_3'
     *  Constant: '<S1>/Ice Road Friction Torque At VehSpd_4'
     *  Constant: '<S1>/Ice Road Friction Torque At VehSpd_5'
     *  Constant: '<S1>/Snow Road Friction Torque At VehSpd_1'
     *  Constant: '<S1>/Snow Road Friction Torque At VehSpd_2'
     *  Constant: '<S1>/Snow Road Friction Torque At VehSpd_3'
     *  Constant: '<S1>/Snow Road Friction Torque At VehSpd_4'
     *  Constant: '<S1>/Snow Road Friction Torque At VehSpd_5'
     *  Constant: '<S1>/Vehicle Speed Break Point 1'
     *  Constant: '<S1>/Vehicle Speed Break Point 2'
     *  Constant: '<S1>/Vehicle Speed Break Point 3'
     *  Constant: '<S1>/Vehicle Speed Break Point 4'
     *  Constant: '<S1>/Vehicle Speed Break Point 5'
     */
    rtb_Prelookup1_o1 = plook_s16s16f_linc(rtu_lcetcss16VehSpd,
      rtb_VectorConcatenate1, 4U, &rtb_Prelookup1_o2);

    /* SignalConversion: '<S1>/ConcatBufferAtVector ConcatenateIn3' */
    rtb_VectorConcatenate[2] = rtu_ETCS_VEH_ACC->lcetcss16RefRdCfFricAtAsphalt;

    /* SignalConversion: '<S1>/ConcatBufferAtVector ConcatenateIn2' */
    rtb_VectorConcatenate[1] = rtu_ETCS_VEH_ACC->lcetcss16RefRdCfFricAtSnow;

    /* SignalConversion: '<S1>/ConcatBufferAtVector ConcatenateIn1' */
    rtb_VectorConcatenate[0] = rtu_ETCS_VEH_ACC->lcetcss16RefRdCfFricAtIce;

    /* PreLookup: '<S1>/Prelookup' */
    rtb_Prelookup_o1 = plook_s16s16f_binc(rtu_ETCS_VEH_ACC->lcetcss16RsltntAcc,
      rtb_VectorConcatenate, 2U, &rtb_Prelookup_o2);

    /* Interpolation_n-D: '<S1>/Interpolation Using Prelookup' */
    frac[0] = rtb_Prelookup_o2;
    frac[1] = rtb_Prelookup1_o2;
    bpIndex[0] = rtb_Prelookup_o1;
    bpIndex[1] = rtb_Prelookup1_o1;
    rtb_Prelookup1_o2 = intrp2d_s16ffs16fl(bpIndex, frac, rtb_VectorConcatenate3,
      3U);
    rtb_Switch1 = (int32_T)((real32_T)floor(rtb_Prelookup1_o2));
  }

  /* End of Switch: '<S1>/Switch1' */

  /* Gain: '<S1>/Gain1' */
  rtb_Switch1 *= 10;

  /* End of Outputs for SubSystem: '<Root>/Subsystem' */

  /* Saturate: '<Root>/Saturation' */
  if (rtb_Switch1 > 320000) {
    rty_ETCS_REF_TRQ_STR_CTL->lcetcss32EstTrqByVehAccBfrCtl = 320000;
  } else if (rtb_Switch1 < -320000) {
    rty_ETCS_REF_TRQ_STR_CTL->lcetcss32EstTrqByVehAccBfrCtl = -320000;
  } else {
    rty_ETCS_REF_TRQ_STR_CTL->lcetcss32EstTrqByVehAccBfrCtl = rtb_Switch1;
  }

  /* End of Saturate: '<Root>/Saturation' */
}

/* Model initialize function */
void LCETCS_vCalRefTrqAtStrtOfCtl_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
