/*
 * File: LCETCS_vCalRefTrqAtVcaOfCtl.c
 *
 * Code generated for Simulink model 'LCETCS_vCalRefTrqAtVcaOfCtl'.
 *
 * Model version                  : 1.200
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:09:28 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalRefTrqAtVcaOfCtl.h"
#include "LCETCS_vCalRefTrqAtVcaOfCtl_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalRefTrqAtVcaOfCtl' */
void LCETCS_vCalRefTrqAtVcaOfCtl_Init(DW_LCETCS_vCalRefTrqAtVcaOfCtl_f_T
  *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vConvAx2CrdnTrq' */
  LCETCS_vConvAx2CrdnTrq_Init(&(localDW->LCETCS_vConvAx2CrdnTrq_DWORK1.rtb));
}

/* Start for referenced model: 'LCETCS_vCalRefTrqAtVcaOfCtl' */
void LCETCS_vCalRefTrqAtVcaOfCtl_Start(DW_LCETCS_vCalRefTrqAtVcaOfCtl_f_T
  *localDW)
{
  /* Start for ModelReference: '<Root>/LCETCS_vConvAx2CrdnTrq' */
  LCETCS_vConvAx2CrdnTrq_Start(&(localDW->LCETCS_vConvAx2CrdnTrq_DWORK1.rtb));
}

/* Output and update for referenced model: 'LCETCS_vCalRefTrqAtVcaOfCtl' */
void LCETCS_vCalRefTrqAtVcaOfCtl(const TypeETCSCtlCmdStruct
  *rtu_ETCS_CTL_CMD_OLD, const TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC, const
  TypeETCSReqVCAStruct *rtu_ETCS_REQ_VCA, boolean_T *rty_lcetcsu1IValVcaTorqAct,
  int32_T *rty_lcetcss32IValVcaTorq, DW_LCETCS_vCalRefTrqAtVcaOfCtl_f_T *localDW)
{
  /* local block i/o variables */
  int32_T rtb_CrdnTrq;

  /* ModelReference: '<Root>/LCETCS_vConvAx2CrdnTrq' */
  LCETCS_vConvAx2CrdnTrq(rtu_ETCS_VEH_ACC->lcetcss16RsltntAcc, &rtb_CrdnTrq,
    &(localDW->LCETCS_vConvAx2CrdnTrq_DWORK1.rtb));

  /* Product: '<Root>/Divide' incorporates:
   *  Constant: '<Root>/Constant2'
   *  Product: '<Root>/Product'
   */
  *rty_lcetcss32IValVcaTorq = (rtb_CrdnTrq * ((uint8_T)
    U8ETCSCpFtr4RefTrqAtVcaOfCtl)) / 100;

  /* Logic: '<Root>/Logical Operator' incorporates:
   *  RelationalOperator: '<Root>/Relational Operator'
   *  Switch: '<Root>/Switch'
   */
  *rty_lcetcsu1IValVcaTorqAct = ((rtu_ETCS_CTL_CMD_OLD->lcetcss32ICtlPor >=
    (*rty_lcetcss32IValVcaTorq)) &&
    (rtu_ETCS_REQ_VCA->lcetcsu1ReqVcaEngTrqRsgEdg));

  /* Switch: '<Root>/Switch1' incorporates:
   *  Constant: '<Root>/Constant4'
   */
  if (*rty_lcetcsu1IValVcaTorqAct) {
  } else {
    *rty_lcetcss32IValVcaTorq = 0;
  }

  /* End of Switch: '<Root>/Switch1' */
}

/* Model initialize function */
void LCETCS_vCalRefTrqAtVcaOfCtl_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vConvAx2CrdnTrq' */
  LCETCS_vConvAx2CrdnTrq_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
