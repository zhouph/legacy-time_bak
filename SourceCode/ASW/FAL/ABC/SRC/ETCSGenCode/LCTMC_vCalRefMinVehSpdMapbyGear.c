/*
 * File: LCTMC_vCalRefMinVehSpdMapbyGear.c
 *
 * Code generated for Simulink model 'LCTMC_vCalRefMinVehSpdMapbyGear'.
 *
 * Model version                  : 1.233
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:03:49 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCTMC_vCalRefMinVehSpdMapbyGear.h"
#include "LCTMC_vCalRefMinVehSpdMapbyGear_private.h"

/* Initial conditions for referenced model: 'LCTMC_vCalRefMinVehSpdMapbyGear' */
void LCTMC_vCalRefMinVehSpdMapbyGear_Init(B_LCTMC_vCalRefMinVehSpdMapbyGear_c_T *
  localB)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter5Point' */
  LCETCS_s16Inter5Point_Init(&localB->y);
}

/* Output and update for referenced model: 'LCTMC_vCalRefMinVehSpdMapbyGear' */
void LCTMC_vCalRefMinVehSpdMapbyGear(const TypeETCSGearStruct
  *rtu_ETCS_GEAR_STRUCT, int16_T *rty_lctmcs16CordTarDnShtMinVSbyGear,
  B_LCTMC_vCalRefMinVehSpdMapbyGear_c_T *localB)
{
  /* local block i/o variables */
  int16_T rtb_y1;
  int16_T rtb_y2;
  int16_T rtb_y3;
  int16_T rtb_y4;
  int16_T rtb_y5;

  /* DataTypeConversion: '<Root>/Data Type Conversion19' incorporates:
   *  Constant: '<Root>/Allowed Dn-Shift Min Vehicle Speed at 1st Gear'
   */
  rtb_y1 = ((uint8_T)U8TMCpAllowDnShftMinVehSpd_1);

  /* DataTypeConversion: '<Root>/Data Type Conversion20' incorporates:
   *  Constant: '<Root>/Allowed UDn-Shift Min Vehicle Speed at 2nd Gear'
   */
  rtb_y2 = ((uint8_T)U8TMCpAllowDnShftMinVehSpd_2);

  /* DataTypeConversion: '<Root>/Data Type Conversion21' incorporates:
   *  Constant: '<Root>/Allowed Dn-Shift Min Vehicle Speed at 3rd Gear'
   */
  rtb_y3 = ((uint8_T)U8TMCpAllowDnShftMinVehSpd_3);

  /* DataTypeConversion: '<Root>/Data Type Conversion22' incorporates:
   *  Constant: '<Root>/Allowed Dn-Shift Min Vehicle Speed at 4th Gear'
   */
  rtb_y4 = ((uint8_T)U8TMCpAllowDnShftMinVehSpd_4);

  /* DataTypeConversion: '<Root>/Data Type Conversion23' incorporates:
   *  Constant: '<Root>/Allowed Dn-Shift Min Vehicle Speed at 5th Gear'
   */
  rtb_y5 = ((uint8_T)U8TMCpAllowDnShftMinVehSpd_5);

  /* ModelReference: '<Root>/LCETCS_s16Inter5Point' */
  LCETCS_s16Inter5Point(rtu_ETCS_GEAR_STRUCT->lcetcss16CurntGearStep, ((int16_T)
    GEAR_POS_TM_1), ((int16_T)GEAR_POS_TM_2), ((int16_T)GEAR_POS_TM_3),
                        ((int16_T)GEAR_POS_TM_4), ((int16_T)GEAR_POS_TM_5),
                        rtb_y1, rtb_y2, rtb_y3, rtb_y4, rtb_y5, &localB->y);

  /* Product: '<Root>/Product' */
  *rty_lctmcs16CordTarDnShtMinVSbyGear = (int16_T)(localB->y << 3);
}

/* Model initialize function */
void LCTMC_vCalRefMinVehSpdMapbyGear_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter5Point' */
  LCETCS_s16Inter5Point_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
