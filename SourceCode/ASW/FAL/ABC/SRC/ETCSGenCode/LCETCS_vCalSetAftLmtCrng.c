/*
 * File: LCETCS_vCalSetAftLmtCrng.c
 *
 * Code generated for Simulink model 'LCETCS_vCalSetAftLmtCrng'.
 *
 * Model version                  : 1.89
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:53:48 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalSetAftLmtCrng.h"
#include "LCETCS_vCalSetAftLmtCrng_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalSetAftLmtCrng' */
void LCETCS_vCalSetAftLmtCrng_Init(boolean_T *rty_lcetcsu1DctAftLmtCrng,
  DW_LCETCS_vCalSetAftLmtCrng_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = false;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = false;

  /* InitializeConditions for Chart: '<Root>/Chart' */
  *rty_lcetcsu1DctAftLmtCrng = false;
}

/* Output and update for referenced model: 'LCETCS_vCalSetAftLmtCrng' */
void LCETCS_vCalSetAftLmtCrng(const TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN,
  boolean_T rtu_lcetcsu1InhAftLmtCrng, boolean_T *rty_lcetcsu1DctAftLmtCrng,
  DW_LCETCS_vCalSetAftLmtCrng_f_T *localDW)
{
  /* Chart: '<Root>/Chart' incorporates:
   *  UnitDelay: '<Root>/Unit Delay1'
   *  UnitDelay: '<Root>/Unit Delay2'
   */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:34' */
  if (rtu_lcetcsu1InhAftLmtCrng == 1) {
    /* Transition: '<S1>:171' */
    /* Transition: '<S1>:173' */
    *rty_lcetcsu1DctAftLmtCrng = false;

    /* Transition: '<S1>:186' */
    /* Transition: '<S1>:189' */
  } else {
    /* Transition: '<S1>:181' */
    if ((rtu_ETCS_TAR_SPIN->lcetcsu1TarSpnDec == 0) &&
        (localDW->UnitDelay2_DSTATE == 1)) {
      /* Transition: '<S1>:183' */
      /* Transition: '<S1>:185' */
      *rty_lcetcsu1DctAftLmtCrng = true;

      /* Transition: '<S1>:189' */
    } else {
      /* Transition: '<S1>:188' */
      *rty_lcetcsu1DctAftLmtCrng = localDW->UnitDelay1_DSTATE;
    }
  }

  /* End of Chart: '<Root>/Chart' */

  /* Update for UnitDelay: '<Root>/Unit Delay2' */
  /* Transition: '<S1>:191' */
  localDW->UnitDelay2_DSTATE = rtu_ETCS_TAR_SPIN->lcetcsu1TarSpnDec;

  /* Update for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = *rty_lcetcsu1DctAftLmtCrng;
}

/* Model initialize function */
void LCETCS_vCalSetAftLmtCrng_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
