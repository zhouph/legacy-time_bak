/*
 * File: LCETCS_vCalIValMemryAtStrt.c
 *
 * Code generated for Simulink model 'LCETCS_vCalIValMemryAtStrt'.
 *
 * Model version                  : 1.210
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:54:20 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalIValMemryAtStrt.h"
#include "LCETCS_vCalIValMemryAtStrt_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalIValMemryAtStrt' */
void LCETCS_vCalIValMemryAtStrt_Init(int32_T *rty_lcetcss32IValMeMryAtStrt,
  DW_LCETCS_vCalIValMemryAtStrt_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = 0;

  /* InitializeConditions for Chart: '<Root>/Chart' */
  *rty_lcetcss32IValMeMryAtStrt = 0;
}

/* Output and update for referenced model: 'LCETCS_vCalIValMemryAtStrt' */
void LCETCS_vCalIValMemryAtStrt(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT,
  int32_T rtu_lcetcss32IValAtStrtOfCtl, int32_T *rty_lcetcss32IValMeMryAtStrt,
  DW_LCETCS_vCalIValMemryAtStrt_f_T *localDW)
{
  /* Chart: '<Root>/Chart' incorporates:
   *  UnitDelay: '<Root>/Unit Delay'
   */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:56' */
  /* comment */
  if (rtu_ETCS_CTL_ACT->lcetcsu1CtlAct == 1) {
    /* Transition: '<S1>:14' */
    /* Transition: '<S1>:36' */
    if (rtu_ETCS_CTL_ACT->lcetcsu1CtlActRsgEdg == 1) {
      /* Transition: '<S1>:72' */
      /* Transition: '<S1>:74' */
      *rty_lcetcss32IValMeMryAtStrt = rtu_lcetcss32IValAtStrtOfCtl;

      /* Transition: '<S1>:77' */
    } else {
      /* Transition: '<S1>:76' */
      *rty_lcetcss32IValMeMryAtStrt = localDW->UnitDelay_DSTATE;
    }

    /* Transition: '<S1>:78' */
  } else {
    /* Transition: '<S1>:62' */
    *rty_lcetcss32IValMeMryAtStrt = 0;
  }

  /* End of Chart: '<Root>/Chart' */

  /* Update for UnitDelay: '<Root>/Unit Delay' */
  /* Transition: '<S1>:67' */
  localDW->UnitDelay_DSTATE = *rty_lcetcss32IValMeMryAtStrt;
}

/* Model initialize function */
void LCETCS_vCalIValMemryAtStrt_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
