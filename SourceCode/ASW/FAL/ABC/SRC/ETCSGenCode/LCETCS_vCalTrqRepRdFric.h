/*
 * File: LCETCS_vCalTrqRepRdFric.h
 *
 * Code generated for Simulink model 'LCETCS_vCalTrqRepRdFric'.
 *
 * Model version                  : 1.125
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:12:27 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalTrqRepRdFric_h_
#define RTW_HEADER_LCETCS_vCalTrqRepRdFric_h_
#ifndef LCETCS_vCalTrqRepRdFric_COMMON_INCLUDES_
# define LCETCS_vCalTrqRepRdFric_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalTrqRepRdFric_COMMON_INCLUDES_ */

#include "LCETCS_vCalTrqRepRdFric_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vCordTrqRepRdFric.h"
#include "LCETCS_vCalLesTrq4RdFric.h"
#include "LCETCS_vCalAppTrq4RdFric.h"

/* Block signals for model 'LCETCS_vCalTrqRepRdFric' */
typedef struct {
  int32_T lcetcss32AppTrq4RdFric;      /* '<Root>/LCETCS_vCalAppTrq4RdFric' */
  int32_T lcetcss32LesTrq4RdFric;      /* '<Root>/LCETCS_vCalLesTrq4RdFric' */
  int32_T lcetcss32TrqAtLowSpnDcted;   /* '<Root>/LCETCS_vCalLesTrq4RdFric' */
} B_LCETCS_vCalTrqRepRdFric_c_T;

/* Block states (auto storage) for model 'LCETCS_vCalTrqRepRdFric' */
typedef struct {
  MdlrefDW_LCETCS_vCalAppTrq4RdFric_T LCETCS_vCalAppTrq4RdFric_DWORK1;/* '<Root>/LCETCS_vCalAppTrq4RdFric' */
  MdlrefDW_LCETCS_vCalLesTrq4RdFric_T LCETCS_vCalLesTrq4RdFric_DWORK1;/* '<Root>/LCETCS_vCalLesTrq4RdFric' */
} DW_LCETCS_vCalTrqRepRdFric_f_T;

typedef struct {
  B_LCETCS_vCalTrqRepRdFric_c_T rtb;
  DW_LCETCS_vCalTrqRepRdFric_f_T rtdw;
} MdlrefDW_LCETCS_vCalTrqRepRdFric_T;

/* Model reference registration function */
extern void LCETCS_vCalTrqRepRdFric_initialize(void);
extern void LCETCS_vCalTrqRepRdFric_Init(B_LCETCS_vCalTrqRepRdFric_c_T *localB,
  DW_LCETCS_vCalTrqRepRdFric_f_T *localDW);
extern void LCETCS_vCalTrqRepRdFric_Start(DW_LCETCS_vCalTrqRepRdFric_f_T
  *localDW);
extern void LCETCS_vCalTrqRepRdFric(const TypeETCSCtlCmdStruct
  *rtu_ETCS_CTL_CMD_OLD, const TypeETCSCyl1stStruct *rtu_ETCS_CYL_1ST, const
  TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC, const TypeETCSLowWhlSpnStruct
  *rtu_ETCS_LOW_WHL_SPN, const TypeETCSH2LStruct *rtu_ETCS_H2L,
  TypeETCSTrq4RdFricStruct *rty_ETCS_TRQ_REP_RD_FRIC,
  B_LCETCS_vCalTrqRepRdFric_c_T *localB, DW_LCETCS_vCalTrqRepRdFric_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalTrqRepRdFric'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalTrqRepRdFric_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
