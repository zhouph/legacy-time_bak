/*
 * File: LCETCS_vCntOvrEngTrqCmdTm.c
 *
 * Code generated for Simulink model 'LCETCS_vCntOvrEngTrqCmdTm'.
 *
 * Model version                  : 1.162
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:05:11 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCntOvrEngTrqCmdTm.h"
#include "LCETCS_vCntOvrEngTrqCmdTm_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCntOvrEngTrqCmdTm' */
void LCETCS_vCntOvrEngTrqCmdTm_Init(B_LCETCS_vCntOvrEngTrqCmdTm_c_T *localB,
  DW_LCETCS_vCntOvrEngTrqCmdTm_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = 0;

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16CntIfSigBigThanTh' */
  LCETCS_s16CntIfSigBigThanTh_Init(&localB->Counter);
}

/* Start for referenced model: 'LCETCS_vCntOvrEngTrqCmdTm' */
void LCETCS_vCntOvrEngTrqCmdTm_Start(B_LCETCS_vCntOvrEngTrqCmdTm_c_T *localB)
{
  /* Start for Constant: '<Root>/Constant' */
  localB->CounterUpTh = ((int8_T)S8ETCSCpCtlExitCntUpTh);

  /* Start for Constant: '<Root>/Constant1' */
  localB->CounterDownTh = ((int8_T)S8ETCSCpCtlExitCntDnTh);
}

/* Output and update for referenced model: 'LCETCS_vCntOvrEngTrqCmdTm' */
void LCETCS_vCntOvrEngTrqCmdTm(const TypeETCSEngCmdStruct *rtu_ETCS_ENG_CMD_OLD,
  const TypeETCSEngStruct *rtu_ETCS_ENG_STRUCT, const TypeETCSWhlSpinStruct
  *rtu_ETCS_WHL_SPIN, const TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN, int16_T
  *rty_lcetcss16OvrEngTrqCmdTm, B_LCETCS_vCntOvrEngTrqCmdTm_c_T *localB,
  DW_LCETCS_vCntOvrEngTrqCmdTm_f_T *localDW)
{
  /* local block i/o variables */
  int16_T rtb_BaseSignal;
  int16_T rtb_CounterOld;
  boolean_T rtb_CounterReset;

  /* Chart: '<Root>/Chart1' */
  /* Gateway: Chart1 */
  /* During: Chart1 */
  /* Entry Internal: Chart1 */
  /* Transition: '<S1>:11' */
  /* comment */
  if (rtu_ETCS_WHL_SPIN->lcetcss16WhlSpin >=
      rtu_ETCS_TAR_SPIN->lcetcss16TarWhlSpin) {
    /* Transition: '<S1>:10' */
    /* Transition: '<S1>:9' */
    rtb_CounterReset = true;

    /* Transition: '<S1>:8' */
  } else {
    /* Transition: '<S1>:7' */
    rtb_CounterReset = false;
  }

  /* End of Chart: '<Root>/Chart1' */

  /* Constant: '<Root>/Constant' */
  /* Transition: '<S1>:6' */
  localB->CounterUpTh = ((int8_T)S8ETCSCpCtlExitCntUpTh);

  /* Constant: '<Root>/Constant1' */
  localB->CounterDownTh = ((int8_T)S8ETCSCpCtlExitCntDnTh);

  /* Sum: '<Root>/Sum' */
  rtb_BaseSignal = (int16_T)(rtu_ETCS_ENG_CMD_OLD->lcetcss16EngTrqCmd -
    rtu_ETCS_ENG_STRUCT->lcetcss16DrvReqTrq);

  /* UnitDelay: '<Root>/Unit Delay' */
  rtb_CounterOld = localDW->UnitDelay_DSTATE;

  /* ModelReference: '<Root>/LCETCS_s16CntIfSigBigThanTh' */
  LCETCS_s16CntIfSigBigThanTh(rtb_CounterReset, rtb_BaseSignal,
    localB->CounterUpTh, localB->CounterDownTh, rtb_CounterOld, &localB->Counter);

  /* Saturate: '<Root>/Saturation' */
  if (localB->Counter > 127) {
    *rty_lcetcss16OvrEngTrqCmdTm = 127;
  } else {
    *rty_lcetcss16OvrEngTrqCmdTm = localB->Counter;
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Update for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = *rty_lcetcss16OvrEngTrqCmdTm;
}

/* Model initialize function */
void LCETCS_vCntOvrEngTrqCmdTm_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16CntIfSigBigThanTh' */
  LCETCS_s16CntIfSigBigThanTh_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
