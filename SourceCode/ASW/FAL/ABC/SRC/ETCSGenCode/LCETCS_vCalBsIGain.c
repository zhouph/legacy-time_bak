/*
 * File: LCETCS_vCalBsIGain.c
 *
 * Code generated for Simulink model 'LCETCS_vCalBsIGain'.
 *
 * Model version                  : 1.160
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:01:04 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalBsIGain.h"
#include "LCETCS_vCalBsIGain_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalBsIGain' */
void LCETCS_vCalBsIGain_Init(int16_T *rty_lcetcss16BsIgain)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_Init(rty_lcetcss16BsIgain);
}

/* Output and update for referenced model: 'LCETCS_vCalBsIGain' */
void LCETCS_vCalBsIGain(int16_T rtu_lcetcss16HomoBsIgain, const
  TypeETCSAxlStruct *rtu_ETCS_FA, const TypeETCSAxlStruct *rtu_ETCS_RA, int16_T
  rtu_lcetcss16SpltBsIgain, int16_T *rty_lcetcss16BsIgain)
{
  /* local block i/o variables */
  int16_T rtb_x;
  int16_T rtb_x1;
  int16_T rtb_x2;

  /* Constant: '<Root>/Constant2' */
  rtb_x1 = ((uint8_T)U8ETCSCpTarTransStBrkTrq2Sp);

  /* Constant: '<Root>/Constant3' */
  rtb_x2 = ((uint8_T)U8ETCSCpTarTransCplBrkTrq2Sp);

  /* Gain: '<Root>/Gain' */
  rtb_x1 = (int16_T)(10 * rtb_x1);

  /* Gain: '<Root>/Gain1' */
  rtb_x2 = (int16_T)(10 * rtb_x2);

  /* MinMax: '<Root>/MinMax' */
  if (rtu_ETCS_FA->lcetcss16EstBrkTrqOnLowMuWhl >=
      rtu_ETCS_RA->lcetcss16EstBrkTrqOnLowMuWhl) {
    rtb_x = rtu_ETCS_FA->lcetcss16EstBrkTrqOnLowMuWhl;
  } else {
    rtb_x = rtu_ETCS_RA->lcetcss16EstBrkTrqOnLowMuWhl;
  }

  /* End of MinMax: '<Root>/MinMax' */

  /* ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point(rtb_x, rtb_x1, rtb_x2, rtu_lcetcss16HomoBsIgain,
                        rtu_lcetcss16SpltBsIgain, rty_lcetcss16BsIgain);
}

/* Model initialize function */
void LCETCS_vCalBsIGain_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
