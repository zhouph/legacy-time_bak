/*
 * File: LCETCS_vCalTarSpnLmtCrnrg_private.h
 *
 * Code generated for Simulink model 'LCETCS_vCalTarSpnLmtCrnrg'.
 *
 * Model version                  : 1.187
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:07:28 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalTarSpnLmtCrnrg_private_h_
#define RTW_HEADER_LCETCS_vCalTarSpnLmtCrnrg_private_h_
#include "rtwtypes.h"
#include "model_reference_types.h"

/* Includes for objects with custom storage classes. */
#include "../ETCSGenCodeHeader/ETCSHeader4Import.h"

/*
 * Generate compile time checks that imported macros for parameters
 * with storage class "ImportedDefine" are defined
 */
#ifndef S16ETCSCpSpd_1
#error The variable for the parameter "S16ETCSCpSpd_1" is not defined
#endif

#ifndef S16ETCSCpSpd_2
#error The variable for the parameter "S16ETCSCpSpd_2" is not defined
#endif

#ifndef S16ETCSCpSpd_3
#error The variable for the parameter "S16ETCSCpSpd_3" is not defined
#endif

#ifndef S16ETCSCpSpd_4
#error The variable for the parameter "S16ETCSCpSpd_4" is not defined
#endif

#ifndef S16ETCSCpSpd_5
#error The variable for the parameter "S16ETCSCpSpd_5" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecAsp_1
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecAsp_1" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecAsp_2
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecAsp_2" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecAsp_3
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecAsp_3" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecAsp_4
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecAsp_4" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecAsp_5
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecAsp_5" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecIce_1
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecIce_1" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecIce_2
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecIce_2" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecIce_3
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecIce_3" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecIce_4
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecIce_4" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecIce_5
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecIce_5" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecSnw_1
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecSnw_1" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecSnw_2
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecSnw_2" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecSnw_3
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecSnw_3" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecSnw_4
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecSnw_4" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecSnw_5
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecSnw_5" is not defined
#endif

#ifndef U8ETCSCpAyAsp
#error The variable for the parameter "U8ETCSCpAyAsp" is not defined
#endif

#ifndef U8ETCSCpAyIce
#error The variable for the parameter "U8ETCSCpAyIce" is not defined
#endif

#ifndef U8ETCSCpAySnw
#error The variable for the parameter "U8ETCSCpAySnw" is not defined
#endif

#ifndef U8ETCSCpCnt4Tar1stIncInTrn
#error The variable for the parameter "U8ETCSCpCnt4Tar1stIncInTrn" is not defined
#endif

#ifndef U8ETCSCpCnt4TarMaxDecInTrn
#error The variable for the parameter "U8ETCSCpCnt4TarMaxDecInTrn" is not defined
#endif

#ifndef U8ETCSCpCrnExDctCnt
#error The variable for the parameter "U8ETCSCpCrnExDctCnt" is not defined
#endif

#ifndef U8ETCSCpDltYawNVldSpd
#error The variable for the parameter "U8ETCSCpDltYawNVldSpd" is not defined
#endif

#ifndef U8ETCSCpDltYawOffset4MinTrgtSpn
#error The variable for the parameter "U8ETCSCpDltYawOffset4MinTrgtSpn" is not defined
#endif

#ifndef U8ETCSCpDltYawStbDctCnt
#error The variable for the parameter "U8ETCSCpDltYawStbDctCnt" is not defined
#endif

#ifndef U8ETCSCpDltYawVldSpd
#error The variable for the parameter "U8ETCSCpDltYawVldSpd" is not defined
#endif

#ifndef U8ETCSCpDltYawVldVyHSpd
#error The variable for the parameter "U8ETCSCpDltYawVldVyHSpd" is not defined
#endif

#ifndef U8ETCSCpTarNChngDltYawHiBrkTrq
#error The variable for the parameter "U8ETCSCpTarNChngDltYawHiBrkTrq" is not defined
#endif

#ifndef U8ETCSCpTarNChngDltYawHiSpd
#error The variable for the parameter "U8ETCSCpTarNChngDltYawHiSpd" is not defined
#endif

#ifndef U8ETCSCpTarNChngDltYawLwBrkTrq
#error The variable for the parameter "U8ETCSCpTarNChngDltYawLwBrkTrq" is not defined
#endif

#ifndef U8ETCSCpTarNChngDltYawLwSpd
#error The variable for the parameter "U8ETCSCpTarNChngDltYawLwSpd" is not defined
#endif

#ifndef U8ETCSCpTarNChngDltYawVyHSpd
#error The variable for the parameter "U8ETCSCpTarNChngDltYawVyHSpd" is not defined
#endif

#ifndef U8ETCSCpTarTransCplBrkTrq2Sp
#error The variable for the parameter "U8ETCSCpTarTransCplBrkTrq2Sp" is not defined
#endif

#ifndef U8ETCSCpTarTransStBrkTrq2Sp
#error The variable for the parameter "U8ETCSCpTarTransStBrkTrq2Sp" is not defined
#endif
#endif                                 /* RTW_HEADER_LCETCS_vCalTarSpnLmtCrnrg_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
