/*
 * File: LCETCS_vDctHighRd.h
 *
 * Code generated for Simulink model 'LCETCS_vDctHighRd'.
 *
 * Model version                  : 1.176
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:10:18 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vDctHighRd_h_
#define RTW_HEADER_LCETCS_vDctHighRd_h_
#ifndef LCETCS_vDctHighRd_COMMON_INCLUDES_
# define LCETCS_vDctHighRd_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vDctHighRd_COMMON_INCLUDES_ */

#include "LCETCS_vDctHighRd_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vDctPreHighRd.h"
#include "LCETCS_vDctLwToHi.h"
#include "LCETCS_vDctAdaptHighRd.h"

/* Block states (auto storage) for model 'LCETCS_vDctHighRd' */
typedef struct {
  MdlrefDW_LCETCS_vDctLwToHi_T LCETCS_vDctLwToHi_DWORK1;/* '<Root>/LCETCS_vDctLwToHi' */
  MdlrefDW_LCETCS_vDctAdaptHighRd_T LCETCS_vDctAdaptHighRd_DWORK1;/* '<Root>/LCETCS_vDctAdaptHighRd' */
  MdlrefDW_LCETCS_vDctPreHighRd_T LCETCS_vDctPreHighRd_DWORK1;/* '<Root>/LCETCS_vDctPreHighRd' */
} DW_LCETCS_vDctHighRd_f_T;

typedef struct {
  DW_LCETCS_vDctHighRd_f_T rtdw;
} MdlrefDW_LCETCS_vDctHighRd_T;

/* Model reference registration function */
extern void LCETCS_vDctHighRd_initialize(void);
extern void LCETCS_vDctHighRd_Init(DW_LCETCS_vDctHighRd_f_T *localDW);
extern void LCETCS_vDctHighRd(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT,
  const TypeETCSWhlAccStruct *rtu_ETCS_WHL_ACC, const TypeETCSWhlSpinStruct
  *rtu_ETCS_WHL_SPIN, const TypeETCSTrq4RdFricStruct
  *rtu_ETCS_TRQ_REP_RD_FRIC_OLD, const TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC,
  const TypeETCSCrdnTrqStruct *rtu_ETCS_CRDN_TRQ, int16_T rtu_lcetcss16VehSpd,
  const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const TypeETCSTarSpinStruct
  *rtu_ETCS_TAR_SPIN, const TypeETCSCtlErrStruct *rtu_ETCS_CTL_ERR,
  TypeETCSHighDctStruct *rty_ETCS_HI_DCT, DW_LCETCS_vDctHighRd_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vDctHighRd'
 */
#endif                                 /* RTW_HEADER_LCETCS_vDctHighRd_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
