/*
 * File: LCETCS_vCalTarSpnLmtCrnrg.c
 *
 * Code generated for Simulink model 'LCETCS_vCalTarSpnLmtCrnrg'.
 *
 * Model version                  : 1.187
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:07:28 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalTarSpnLmtCrnrg.h"
#include "LCETCS_vCalTarSpnLmtCrnrg_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalTarSpnLmtCrnrg' */
void LCETCS_vCalTarSpnLmtCrnrg_Init(boolean_T *rty_lcetcsu1TarSpnDec, boolean_T *
  rty_lcetcsu1DctLmtCrng, boolean_T *rty_lcetcsu1DctLmtCrngFalEdg,
  DW_LCETCS_vCalTarSpnLmtCrnrg_f_T *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalFnlTarSpnDec' */
  LCETCS_vCalFnlTarSpnDec_Init(&(localDW->LCETCS_vCalFnlTarSpnDec_DWORK1.rtb));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalLmtCrngDltYaw' */
  LCETCS_vCalLmtCrngDltYaw_Init(&(localDW->LCETCS_vCalLmtCrngDltYaw_DWORK1.rtb));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalInLmtCrng' */
  LCETCS_vCalLmtCrng_Init(rty_lcetcsu1DctLmtCrng, rty_lcetcsu1DctLmtCrngFalEdg,
    &(localDW->LCETCS_vCalInLmtCrng_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalTarDecMd' */
  LCETCS_vCalTarDecMd_Init(rty_lcetcsu1TarSpnDec,
    &(localDW->LCETCS_vCalTarDecMd_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalMaxDltYawInTarDecMd' */
  LCETCS_vCalMaxDltYawInTarDecMd_Init
    (&(localDW->LCETCS_vCalMaxDltYawInTarDecMd_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalTarDecCnt' */
  LCETCS_vCalTarDecCnt_Init(&(localDW->LCETCS_vCalTarDecCnt_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalTarDecDpndDelYaw' */
  LCETCS_vCalTarDecDpndDelYaw_Init
    (&(localDW->LCETCS_vCalTarDecDpndDelYaw_DWORK1.rtb));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalTarDecInTrn' */
  LCETCS_vCalTarDecInTrn_Init(&(localDW->LCETCS_vCalTarDecInTrn_DWORK1.rtb));
}

/* Start for referenced model: 'LCETCS_vCalTarSpnLmtCrnrg' */
void LCETCS_vCalTarSpnLmtCrnrg_Start(DW_LCETCS_vCalTarSpnLmtCrnrg_f_T *localDW)
{
  /* Start for ModelReference: '<Root>/LCETCS_vCalFnlTarSpnDec' */
  LCETCS_vCalFnlTarSpnDec_Start(&(localDW->LCETCS_vCalFnlTarSpnDec_DWORK1.rtb));

  /* Start for ModelReference: '<Root>/LCETCS_vCalLmtCrngDltYaw' */
  LCETCS_vCalLmtCrngDltYaw_Start(&(localDW->LCETCS_vCalLmtCrngDltYaw_DWORK1.rtb));

  /* Start for ModelReference: '<Root>/LCETCS_vCalTarDecInTrn' */
  LCETCS_vCalTarDecInTrn_Start(&(localDW->LCETCS_vCalTarDecInTrn_DWORK1.rtb));
}

/* Output and update for referenced model: 'LCETCS_vCalTarSpnLmtCrnrg' */
void LCETCS_vCalTarSpnLmtCrnrg(int16_T rtu_lcetcss16VehSpd, const
  TypeETCSAxlStruct *rtu_ETCS_FA, const TypeETCSAxlStruct *rtu_ETCS_RA, const
  TypeETCSExtDctStruct *rtu_ETCS_EXT_DCT, const TypeETCSEscSigStruct
  *rtu_ETCS_ESC_SIG, const TypeETCSStrStatStruct *rtu_ETCS_STR_STAT, int16_T
  *rty_lcetcss16TarDecInTrn, int16_T *rty_lcetcss16DltYawStbCnt, boolean_T
  *rty_lcetcsu1TarSpnDec, int16_T *rty_lcetcss16TarSpnDecCnt, int16_T
  *rty_lcetcss16MaxDltYawInTarDecMd, int16_T *rty_lcetcss16TarDecDpndDelYaw,
  int16_T *rty_lcetcss16FnlTarSpnDec, boolean_T *rty_lcetcsu1DctLmtCrng,
  boolean_T *rty_lcetcsu1DctLmtCrngFalEdg, int16_T *rty_lcetcss16LmtCrngDltYaw,
  DW_LCETCS_vCalTarSpnLmtCrnrg_f_T *localDW)
{
  /* ModelReference: '<Root>/LCETCS_vCalFnlTarSpnDec' */
  LCETCS_vCalFnlTarSpnDec(rtu_ETCS_ESC_SIG, rtu_lcetcss16VehSpd,
    rty_lcetcss16FnlTarSpnDec, &(localDW->LCETCS_vCalFnlTarSpnDec_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vCalLmtCrngDltYaw' */
  LCETCS_vCalLmtCrngDltYaw(rtu_lcetcss16VehSpd, rtu_ETCS_FA, rtu_ETCS_RA,
    rty_lcetcss16LmtCrngDltYaw, &(localDW->LCETCS_vCalLmtCrngDltYaw_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vCalInLmtCrng' */
  LCETCS_vCalLmtCrng((*rty_lcetcss16LmtCrngDltYaw), rtu_ETCS_ESC_SIG,
                     rty_lcetcsu1DctLmtCrng, rty_lcetcsu1DctLmtCrngFalEdg,
                     &(localDW->LCETCS_vCalInLmtCrng_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalTarDecMd' */
  LCETCS_vCalTarDecMd((*rty_lcetcss16LmtCrngDltYaw), rtu_ETCS_ESC_SIG,
                      rtu_ETCS_STR_STAT, rty_lcetcss16DltYawStbCnt,
                      rty_lcetcsu1TarSpnDec,
                      &(localDW->LCETCS_vCalTarDecMd_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalMaxDltYawInTarDecMd' */
  LCETCS_vCalMaxDltYawInTarDecMd((*rty_lcetcsu1TarSpnDec), rtu_ETCS_ESC_SIG,
    rty_lcetcss16MaxDltYawInTarDecMd,
    &(localDW->LCETCS_vCalMaxDltYawInTarDecMd_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalTarDecCnt' */
  LCETCS_vCalTarDecCnt((*rty_lcetcsu1TarSpnDec), rtu_ETCS_ESC_SIG,
                       rty_lcetcss16TarSpnDecCnt,
                       &(localDW->LCETCS_vCalTarDecCnt_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalTarDecDpndDelYaw' */
  LCETCS_vCalTarDecDpndDelYaw((*rty_lcetcss16MaxDltYawInTarDecMd),
    rtu_ETCS_EXT_DCT, (*rty_lcetcss16LmtCrngDltYaw), (*rty_lcetcss16FnlTarSpnDec),
    rty_lcetcss16TarDecDpndDelYaw,
    &(localDW->LCETCS_vCalTarDecDpndDelYaw_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vCalTarDecInTrn' */
  LCETCS_vCalTarDecInTrn((*rty_lcetcss16TarDecDpndDelYaw),
    (*rty_lcetcss16TarSpnDecCnt), rty_lcetcss16TarDecInTrn,
    &(localDW->LCETCS_vCalTarDecInTrn_DWORK1.rtb));
}

/* Model initialize function */
void LCETCS_vCalTarSpnLmtCrnrg_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalFnlTarSpnDec' */
  LCETCS_vCalFnlTarSpnDec_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalInLmtCrng' */
  LCETCS_vCalLmtCrng_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalLmtCrngDltYaw' */
  LCETCS_vCalLmtCrngDltYaw_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalMaxDltYawInTarDecMd' */
  LCETCS_vCalMaxDltYawInTarDecMd_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalTarDecCnt' */
  LCETCS_vCalTarDecCnt_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalTarDecDpndDelYaw' */
  LCETCS_vCalTarDecDpndDelYaw_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalTarDecInTrn' */
  LCETCS_vCalTarDecInTrn_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalTarDecMd' */
  LCETCS_vCalTarDecMd_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
