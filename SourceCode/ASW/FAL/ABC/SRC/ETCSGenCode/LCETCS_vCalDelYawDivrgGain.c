/*
 * File: LCETCS_vCalDelYawDivrgGain.c
 *
 * Code generated for Simulink model 'LCETCS_vCalDelYawDivrgGain'.
 *
 * Model version                  : 1.240
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:03:06 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalDelYawDivrgGain.h"
#include "LCETCS_vCalDelYawDivrgGain_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalDelYawDivrgGain' */
void LCETCS_vCalDelYawDivrgGain_Init(B_LCETCS_vCalDelYawDivrgGain_c_T *localB)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter3by5' */
  LCETCS_s16Inter3by5_Init(&localB->Out);
}

/* Start for referenced model: 'LCETCS_vCalDelYawDivrgGain' */
void LCETCS_vCalDelYawDivrgGain_Start(B_LCETCS_vCalDelYawDivrgGain_c_T *localB)
{
  /* Start for Constant: '<Root>/Ay level on asphalt' */
  localB->x3 = ((uint8_T)U8ETCSCpAyAsp);

  /* Start for Constant: '<Root>/Ay level on ice' */
  localB->x1 = ((uint8_T)U8ETCSCpAyIce);

  /* Start for Constant: '<Root>/Ay level on snow' */
  localB->x2 = ((uint8_T)U8ETCSCpAySnw);

  /* Start for Constant: '<Root>/Delta yaw gain on asphalt at speed 1' */
  localB->z13 = ((uint8_T)U8ETCSCpDelYawDivrgGainAsp_1);

  /* Start for Constant: '<Root>/Delta yaw gain on asphalt at speed 2' */
  localB->z23 = ((uint8_T)U8ETCSCpDelYawDivrgGainAsp_2);

  /* Start for Constant: '<Root>/Delta yaw gain on asphalt at speed 3' */
  localB->z33 = ((uint8_T)U8ETCSCpDelYawDivrgGainAsp_3);

  /* Start for Constant: '<Root>/Delta yaw gain on asphalt at speed 4' */
  localB->z43 = ((uint8_T)U8ETCSCpDelYawDivrgGainAsp_4);

  /* Start for Constant: '<Root>/Delta yaw gain on asphalt at speed 5' */
  localB->z53 = ((uint8_T)U8ETCSCpDelYawDivrgGainAsp_5);

  /* Start for Constant: '<Root>/Delta yaw gain on ice at speed 1' */
  localB->z11 = ((uint8_T)U8ETCSCpDelYawDivrgGainIce_1);

  /* Start for Constant: '<Root>/Delta yaw gain on ice at speed 2' */
  localB->z21 = ((uint8_T)U8ETCSCpDelYawDivrgGainIce_2);

  /* Start for Constant: '<Root>/Delta yaw gain on ice at speed 3' */
  localB->z31 = ((uint8_T)U8ETCSCpDelYawDivrgGainIce_3);

  /* Start for Constant: '<Root>/Delta yaw gain on ice at speed 4' */
  localB->z41 = ((uint8_T)U8ETCSCpDelYawDivrgGainIce_4);

  /* Start for Constant: '<Root>/Delta yaw gain on ice at speed 5' */
  localB->z51 = ((uint8_T)U8ETCSCpDelYawDivrgGainIce_5);

  /* Start for Constant: '<Root>/Delta yaw gain on snow at speed 1' */
  localB->z12 = ((uint8_T)U8ETCSCpDelYawDivrgGainSnw_1);

  /* Start for Constant: '<Root>/Delta yaw gain on snow at speed 2' */
  localB->z22 = ((uint8_T)U8ETCSCpDelYawDivrgGainSnw_2);

  /* Start for Constant: '<Root>/Delta yaw gain on snow at speed 3' */
  localB->z32 = ((uint8_T)U8ETCSCpDelYawDivrgGainSnw_3);

  /* Start for Constant: '<Root>/Delta yaw gain on snow at speed 4' */
  localB->z42 = ((uint8_T)U8ETCSCpDelYawDivrgGainSnw_4);

  /* Start for Constant: '<Root>/Delta yaw gain on snow at speed 5' */
  localB->z52 = ((uint8_T)U8ETCSCpDelYawDivrgGainSnw_5);
}

/* Output and update for referenced model: 'LCETCS_vCalDelYawDivrgGain' */
void LCETCS_vCalDelYawDivrgGain(int16_T rtu_lcetcss16VehSpd, const
  TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, int16_T *rty_lcetcss16DelYawDivrgGain,
  B_LCETCS_vCalDelYawDivrgGain_c_T *localB)
{
  /* Constant: '<Root>/Ay level on asphalt' */
  localB->x3 = ((uint8_T)U8ETCSCpAyAsp);

  /* Constant: '<Root>/Ay level on ice' */
  localB->x1 = ((uint8_T)U8ETCSCpAyIce);

  /* Constant: '<Root>/Ay level on snow' */
  localB->x2 = ((uint8_T)U8ETCSCpAySnw);

  /* Constant: '<Root>/Delta yaw gain on asphalt at speed 1' */
  localB->z13 = ((uint8_T)U8ETCSCpDelYawDivrgGainAsp_1);

  /* Constant: '<Root>/Delta yaw gain on asphalt at speed 2' */
  localB->z23 = ((uint8_T)U8ETCSCpDelYawDivrgGainAsp_2);

  /* Constant: '<Root>/Delta yaw gain on asphalt at speed 3' */
  localB->z33 = ((uint8_T)U8ETCSCpDelYawDivrgGainAsp_3);

  /* Constant: '<Root>/Delta yaw gain on asphalt at speed 4' */
  localB->z43 = ((uint8_T)U8ETCSCpDelYawDivrgGainAsp_4);

  /* Constant: '<Root>/Delta yaw gain on asphalt at speed 5' */
  localB->z53 = ((uint8_T)U8ETCSCpDelYawDivrgGainAsp_5);

  /* Constant: '<Root>/Delta yaw gain on ice at speed 1' */
  localB->z11 = ((uint8_T)U8ETCSCpDelYawDivrgGainIce_1);

  /* Constant: '<Root>/Delta yaw gain on ice at speed 2' */
  localB->z21 = ((uint8_T)U8ETCSCpDelYawDivrgGainIce_2);

  /* Constant: '<Root>/Delta yaw gain on ice at speed 3' */
  localB->z31 = ((uint8_T)U8ETCSCpDelYawDivrgGainIce_3);

  /* Constant: '<Root>/Delta yaw gain on ice at speed 4' */
  localB->z41 = ((uint8_T)U8ETCSCpDelYawDivrgGainIce_4);

  /* Constant: '<Root>/Delta yaw gain on ice at speed 5' */
  localB->z51 = ((uint8_T)U8ETCSCpDelYawDivrgGainIce_5);

  /* Constant: '<Root>/Delta yaw gain on snow at speed 1' */
  localB->z12 = ((uint8_T)U8ETCSCpDelYawDivrgGainSnw_1);

  /* Constant: '<Root>/Delta yaw gain on snow at speed 2' */
  localB->z22 = ((uint8_T)U8ETCSCpDelYawDivrgGainSnw_2);

  /* Constant: '<Root>/Delta yaw gain on snow at speed 3' */
  localB->z32 = ((uint8_T)U8ETCSCpDelYawDivrgGainSnw_3);

  /* Constant: '<Root>/Delta yaw gain on snow at speed 4' */
  localB->z42 = ((uint8_T)U8ETCSCpDelYawDivrgGainSnw_4);

  /* Constant: '<Root>/Delta yaw gain on snow at speed 5' */
  localB->z52 = ((uint8_T)U8ETCSCpDelYawDivrgGainSnw_5);

  /* ModelReference: '<Root>/LCETCS_s16Inter3by5' */
  LCETCS_s16Inter3by5(rtu_ETCS_ESC_SIG->lcetcss16LatAccMsrdMani, localB->x1,
                      localB->x2, localB->x3, rtu_lcetcss16VehSpd, ((int16_T)
    S16ETCSCpSpd_1), ((int16_T)S16ETCSCpSpd_2), ((int16_T)S16ETCSCpSpd_3),
                      ((int16_T)S16ETCSCpSpd_4), ((int16_T)S16ETCSCpSpd_5),
                      localB->z11, localB->z12, localB->z13, localB->z21,
                      localB->z22, localB->z23, localB->z31, localB->z32,
                      localB->z33, localB->z41, localB->z42, localB->z43,
                      localB->z51, localB->z52, localB->z53, &localB->Out);

  /* Saturate: '<Root>/Saturation1' */
  if (localB->Out > 250) {
    *rty_lcetcss16DelYawDivrgGain = 250;
  } else if (localB->Out < 0) {
    *rty_lcetcss16DelYawDivrgGain = 0;
  } else {
    *rty_lcetcss16DelYawDivrgGain = localB->Out;
  }

  /* End of Saturate: '<Root>/Saturation1' */
}

/* Model initialize function */
void LCETCS_vCalDelYawDivrgGain_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter3by5' */
  LCETCS_s16Inter3by5_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
