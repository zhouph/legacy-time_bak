/*
 * File: LCETCS_vCallMain_private.h
 *
 * Code generated for Simulink model 'LCETCS_vCallMain'.
 *
 * Model version                  : 1.472
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:22:44 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCallMain_private_h_
#define RTW_HEADER_LCETCS_vCallMain_private_h_
#include "rtwtypes.h"
#include "model_reference_types.h"

/* Includes for objects with custom storage classes. */
#include "../ETCSGenCodeHeader/ETCSHeader4Import.h"

/*
 * Generate compile time checks that imported macros for parameters
 * with storage class "ImportedDefine" are defined
 */
#ifndef GEAR_POS_TM_1
#error The variable for the parameter "GEAR_POS_TM_1" is not defined
#endif

#ifndef GEAR_POS_TM_2
#error The variable for the parameter "GEAR_POS_TM_2" is not defined
#endif

#ifndef GEAR_POS_TM_3
#error The variable for the parameter "GEAR_POS_TM_3" is not defined
#endif

#ifndef GEAR_POS_TM_4
#error The variable for the parameter "GEAR_POS_TM_4" is not defined
#endif

#ifndef GEAR_POS_TM_5
#error The variable for the parameter "GEAR_POS_TM_5" is not defined
#endif

#ifndef GEAR_POS_TM_PN
#error The variable for the parameter "GEAR_POS_TM_PN" is not defined
#endif

#ifndef GEAR_POS_TM_R
#error The variable for the parameter "GEAR_POS_TM_R" is not defined
#endif

#ifndef GEAR_SEL_M
#error The variable for the parameter "GEAR_SEL_M" is not defined
#endif

#ifndef S16ETCSCpAftTrnMaxCnt
#error The variable for the parameter "S16ETCSCpAftTrnMaxCnt" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtAspSpd_1
#error The variable for the parameter "S16ETCSCpCdrnTrqAtAspSpd_1" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtAspSpd_2
#error The variable for the parameter "S16ETCSCpCdrnTrqAtAspSpd_2" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtAspSpd_3
#error The variable for the parameter "S16ETCSCpCdrnTrqAtAspSpd_3" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtAspSpd_4
#error The variable for the parameter "S16ETCSCpCdrnTrqAtAspSpd_4" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtAspSpd_5
#error The variable for the parameter "S16ETCSCpCdrnTrqAtAspSpd_5" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtAsphalt
#error The variable for the parameter "S16ETCSCpCdrnTrqAtAsphalt" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtIce
#error The variable for the parameter "S16ETCSCpCdrnTrqAtIce" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtIceSpd_1
#error The variable for the parameter "S16ETCSCpCdrnTrqAtIceSpd_1" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtIceSpd_2
#error The variable for the parameter "S16ETCSCpCdrnTrqAtIceSpd_2" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtIceSpd_3
#error The variable for the parameter "S16ETCSCpCdrnTrqAtIceSpd_3" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtIceSpd_4
#error The variable for the parameter "S16ETCSCpCdrnTrqAtIceSpd_4" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtIceSpd_5
#error The variable for the parameter "S16ETCSCpCdrnTrqAtIceSpd_5" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtSnow
#error The variable for the parameter "S16ETCSCpCdrnTrqAtSnow" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtSnowSpd_1
#error The variable for the parameter "S16ETCSCpCdrnTrqAtSnowSpd_1" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtSnowSpd_2
#error The variable for the parameter "S16ETCSCpCdrnTrqAtSnowSpd_2" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtSnowSpd_3
#error The variable for the parameter "S16ETCSCpCdrnTrqAtSnowSpd_3" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtSnowSpd_4
#error The variable for the parameter "S16ETCSCpCdrnTrqAtSnowSpd_4" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtSnowSpd_5
#error The variable for the parameter "S16ETCSCpCdrnTrqAtSnowSpd_5" is not defined
#endif

#ifndef S16ETCSCpCircumferenceOfTire
#error The variable for the parameter "S16ETCSCpCircumferenceOfTire" is not defined
#endif

#ifndef S16ETCSCpEngSpd_1
#error The variable for the parameter "S16ETCSCpEngSpd_1" is not defined
#endif

#ifndef S16ETCSCpEngSpd_2
#error The variable for the parameter "S16ETCSCpEngSpd_2" is not defined
#endif

#ifndef S16ETCSCpEngSpd_3
#error The variable for the parameter "S16ETCSCpEngSpd_3" is not defined
#endif

#ifndef S16ETCSCpEngSpd_4
#error The variable for the parameter "S16ETCSCpEngSpd_4" is not defined
#endif

#ifndef S16ETCSCpEngSpd_5
#error The variable for the parameter "S16ETCSCpEngSpd_5" is not defined
#endif

#ifndef S16ETCSCpEngStallRpm
#error The variable for the parameter "S16ETCSCpEngStallRpm" is not defined
#endif

#ifndef S16ETCSCpHighRiskStallRpmFlat
#error The variable for the parameter "S16ETCSCpHighRiskStallRpmFlat" is not defined
#endif

#ifndef S16ETCSCpHighRiskStallRpmHill
#error The variable for the parameter "S16ETCSCpHighRiskStallRpmHill" is not defined
#endif

#ifndef S16ETCSCpIGainIncFacH2L
#error The variable for the parameter "S16ETCSCpIGainIncFacH2L" is not defined
#endif

#ifndef S16ETCSCpLmtCrngTarTrqAsp
#error The variable for the parameter "S16ETCSCpLmtCrngTarTrqAsp" is not defined
#endif

#ifndef S16ETCSCpLmtCrngTarTrqIce
#error The variable for the parameter "S16ETCSCpLmtCrngTarTrqIce" is not defined
#endif

#ifndef S16ETCSCpLmtCrngTarTrqSnw
#error The variable for the parameter "S16ETCSCpLmtCrngTarTrqSnw" is not defined
#endif

#ifndef S16ETCSCpLow2HighDecel
#error The variable for the parameter "S16ETCSCpLow2HighDecel" is not defined
#endif

#ifndef S16ETCSCpLowRiskStallRpmFlat
#error The variable for the parameter "S16ETCSCpLowRiskStallRpmFlat" is not defined
#endif

#ifndef S16ETCSCpLowRiskStallRpmHill
#error The variable for the parameter "S16ETCSCpLowRiskStallRpmHill" is not defined
#endif

#ifndef S16ETCSCpLwToSpltDctCplCnt
#error The variable for the parameter "S16ETCSCpLwToSpltDctCplCnt" is not defined
#endif

#ifndef S16ETCSCpLwToSpltDctStrtCnt
#error The variable for the parameter "S16ETCSCpLwToSpltDctStrtCnt" is not defined
#endif

#ifndef S16ETCSCpMidRiskStallRpmFlat
#error The variable for the parameter "S16ETCSCpMidRiskStallRpmFlat" is not defined
#endif

#ifndef S16ETCSCpMidRiskStallRpmHill
#error The variable for the parameter "S16ETCSCpMidRiskStallRpmHill" is not defined
#endif

#ifndef S16ETCSCpMinCrdnCmdTrq
#error The variable for the parameter "S16ETCSCpMinCrdnCmdTrq" is not defined
#endif

#ifndef S16ETCSCpOffsetTrq4FunLamp
#error The variable for the parameter "S16ETCSCpOffsetTrq4FunLamp" is not defined
#endif

#ifndef S16ETCSCpOffsetTrq4LwToSplt
#error The variable for the parameter "S16ETCSCpOffsetTrq4LwToSplt" is not defined
#endif

#ifndef S16ETCSCpPreHighRdDctCplCnt
#error The variable for the parameter "S16ETCSCpPreHighRdDctCplCnt" is not defined
#endif

#ifndef S16ETCSCpPreHighRdDctStrtCnt
#error The variable for the parameter "S16ETCSCpPreHighRdDctStrtCnt" is not defined
#endif

#ifndef S16ETCSCpSpd_1
#error The variable for the parameter "S16ETCSCpSpd_1" is not defined
#endif

#ifndef S16ETCSCpSpd_2
#error The variable for the parameter "S16ETCSCpSpd_2" is not defined
#endif

#ifndef S16ETCSCpSpd_3
#error The variable for the parameter "S16ETCSCpSpd_3" is not defined
#endif

#ifndef S16ETCSCpSpd_4
#error The variable for the parameter "S16ETCSCpSpd_4" is not defined
#endif

#ifndef S16ETCSCpSpd_5
#error The variable for the parameter "S16ETCSCpSpd_5" is not defined
#endif

#ifndef S16ETCSCpSplitHillClrEngSpd
#error The variable for the parameter "S16ETCSCpSplitHillClrEngSpd" is not defined
#endif

#ifndef S16ETCSCpSplitHillDctEngSpd
#error The variable for the parameter "S16ETCSCpSplitHillDctEngSpd" is not defined
#endif

#ifndef S16ETCSCpStrtCdrnTrqAtAsphalt
#error The variable for the parameter "S16ETCSCpStrtCdrnTrqAtAsphalt" is not defined
#endif

#ifndef S16ETCSCpTireRadi
#error The variable for the parameter "S16ETCSCpTireRadi" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_1
#error The variable for the parameter "S16ETCSCpTotalGearRatio_1" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_2
#error The variable for the parameter "S16ETCSCpTotalGearRatio_2" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_3
#error The variable for the parameter "S16ETCSCpTotalGearRatio_3" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_4
#error The variable for the parameter "S16ETCSCpTotalGearRatio_4" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_5
#error The variable for the parameter "S16ETCSCpTotalGearRatio_5" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_R
#error The variable for the parameter "S16ETCSCpTotalGearRatio_R" is not defined
#endif

#ifndef S16ETCSCpTrgtEngSpdStlDetd
#error The variable for the parameter "S16ETCSCpTrgtEngSpdStlDetd" is not defined
#endif

#ifndef S16ETCSCpTrnfrCsGrRtoHgMd
#error The variable for the parameter "S16ETCSCpTrnfrCsGrRtoHgMd" is not defined
#endif

#ifndef S16ETCSCpTrnfrCsGrRtoLwMd
#error The variable for the parameter "S16ETCSCpTrnfrCsGrRtoLwMd" is not defined
#endif

#ifndef S16ETCSCpTtlVehMass
#error The variable for the parameter "S16ETCSCpTtlVehMass" is not defined
#endif

#ifndef VREF_0_5_KPH
#error The variable for the parameter "VREF_0_5_KPH" is not defined
#endif

#ifndef VREF_1_KPH
#error The variable for the parameter "VREF_1_KPH" is not defined
#endif

#ifndef VREF_25_KPH
#error The variable for the parameter "VREF_25_KPH" is not defined
#endif

#ifndef VREF_3_KPH
#error The variable for the parameter "VREF_3_KPH" is not defined
#endif

#ifndef U16TCSCpDctLwHmMaxCnt
#error The variable for the parameter "U16TCSCpDctLwHmMaxCnt" is not defined
#endif

#ifndef S8ETCSCpCtlExitCntDnTh
#error The variable for the parameter "S8ETCSCpCtlExitCntDnTh" is not defined
#endif

#ifndef S8ETCSCpCtlExitCntTh
#error The variable for the parameter "S8ETCSCpCtlExitCntTh" is not defined
#endif

#ifndef S8ETCSCpCtlExitCntUpTh
#error The variable for the parameter "S8ETCSCpCtlExitCntUpTh" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecAsp_1
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecAsp_1" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecAsp_2
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecAsp_2" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecAsp_3
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecAsp_3" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecAsp_4
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecAsp_4" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecAsp_5
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecAsp_5" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecIce_1
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecIce_1" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecIce_2
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecIce_2" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecIce_3
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecIce_3" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecIce_4
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecIce_4" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecIce_5
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecIce_5" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecSnw_1
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecSnw_1" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecSnw_2
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecSnw_2" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecSnw_3
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecSnw_3" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecSnw_4
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecSnw_4" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecSnw_5
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecSnw_5" is not defined
#endif

#ifndef S8ETCSCpH2LDctDltTh
#error The variable for the parameter "S8ETCSCpH2LDctDltTh" is not defined
#endif

#ifndef S8ETCSCpH2LDctHiMuTh
#error The variable for the parameter "S8ETCSCpH2LDctHiMuTh" is not defined
#endif

#ifndef S8ETCSCpH2LDctLwMuTh
#error The variable for the parameter "S8ETCSCpH2LDctLwMuTh" is not defined
#endif

#ifndef S8ETCSCpH2LTrnsTime
#error The variable for the parameter "S8ETCSCpH2LTrnsTime" is not defined
#endif

#ifndef S8ETCSCpPgTransTmDrvVib
#error The variable for the parameter "S8ETCSCpPgTransTmDrvVib" is not defined
#endif

#ifndef S8ETCSCpTarSpnDec4Vib
#error The variable for the parameter "S8ETCSCpTarSpnDec4Vib" is not defined
#endif

#ifndef GRAD_10_DEG
#error The variable for the parameter "GRAD_10_DEG" is not defined
#endif

#ifndef GRAD_8_DEG
#error The variable for the parameter "GRAD_8_DEG" is not defined
#endif

#ifndef L_U8FILTER_GAIN_20MSLOOP_1HZ
#error The variable for the parameter "L_U8FILTER_GAIN_20MSLOOP_1HZ" is not defined
#endif

#ifndef L_U8FILTER_GAIN_20MSLOOP_7HZ
#error The variable for the parameter "L_U8FILTER_GAIN_20MSLOOP_7HZ" is not defined
#endif

#ifndef TCSDpCtrlModeDisable
#error The variable for the parameter "TCSDpCtrlModeDisable" is not defined
#endif

#ifndef TCSDpCtrlModeDrvPrtn
#error The variable for the parameter "TCSDpCtrlModeDrvPrtn" is not defined
#endif

#ifndef TCSDpCtrlModeNormal
#error The variable for the parameter "TCSDpCtrlModeNormal" is not defined
#endif

#ifndef TCSDpCtrlModeSports1
#error The variable for the parameter "TCSDpCtrlModeSports1" is not defined
#endif

#ifndef U8ETCSCpAWDVehicle
#error The variable for the parameter "U8ETCSCpAWDVehicle" is not defined
#endif

#ifndef U8ETCSCpAdaptHighRdCplCnt
#error The variable for the parameter "U8ETCSCpAdaptHighRdCplCnt" is not defined
#endif

#ifndef U8ETCSCpAdaptHighRdStrtCnt
#error The variable for the parameter "U8ETCSCpAdaptHighRdStrtCnt" is not defined
#endif

#ifndef U8ETCSCpAddTarWhlSpnLwToSp
#error The variable for the parameter "U8ETCSCpAddTarWhlSpnLwToSp" is not defined
#endif

#ifndef U8ETCSCpAddTarWhlSpnSp
#error The variable for the parameter "U8ETCSCpAddTarWhlSpnSp" is not defined
#endif

#ifndef U8ETCSCpAlwAddTar4DepSnwSpd
#error The variable for the parameter "U8ETCSCpAlwAddTar4DepSnwSpd" is not defined
#endif

#ifndef U8ETCSCpAreaSetLwToHiSpin
#error The variable for the parameter "U8ETCSCpAreaSetLwToHiSpin" is not defined
#endif

#ifndef U8ETCSCpAxAtAsphalt
#error The variable for the parameter "U8ETCSCpAxAtAsphalt" is not defined
#endif

#ifndef U8ETCSCpAxAtIce
#error The variable for the parameter "U8ETCSCpAxAtIce" is not defined
#endif

#ifndef U8ETCSCpAxAtSnow
#error The variable for the parameter "U8ETCSCpAxAtSnow" is not defined
#endif

#ifndef U8ETCSCpAxGear2AtAsphalt
#error The variable for the parameter "U8ETCSCpAxGear2AtAsphalt" is not defined
#endif

#ifndef U8ETCSCpAxGear2AtIce
#error The variable for the parameter "U8ETCSCpAxGear2AtIce" is not defined
#endif

#ifndef U8ETCSCpAxGear2AtSnow
#error The variable for the parameter "U8ETCSCpAxGear2AtSnow" is not defined
#endif

#ifndef U8ETCSCpAxGear3AtAsphalt
#error The variable for the parameter "U8ETCSCpAxGear3AtAsphalt" is not defined
#endif

#ifndef U8ETCSCpAxGear3AtIce
#error The variable for the parameter "U8ETCSCpAxGear3AtIce" is not defined
#endif

#ifndef U8ETCSCpAxGear3AtSnow
#error The variable for the parameter "U8ETCSCpAxGear3AtSnow" is not defined
#endif

#ifndef U8ETCSCpAxGear4AtAsphalt
#error The variable for the parameter "U8ETCSCpAxGear4AtAsphalt" is not defined
#endif

#ifndef U8ETCSCpAxGear4AtIce
#error The variable for the parameter "U8ETCSCpAxGear4AtIce" is not defined
#endif

#ifndef U8ETCSCpAxGear4AtSnow
#error The variable for the parameter "U8ETCSCpAxGear4AtSnow" is not defined
#endif

#ifndef U8ETCSCpAxGear5AtAsphalt
#error The variable for the parameter "U8ETCSCpAxGear5AtAsphalt" is not defined
#endif

#ifndef U8ETCSCpAxGear5AtIce
#error The variable for the parameter "U8ETCSCpAxGear5AtIce" is not defined
#endif

#ifndef U8ETCSCpAxGear5AtSnow
#error The variable for the parameter "U8ETCSCpAxGear5AtSnow" is not defined
#endif

#ifndef U8ETCSCpAxSenAvail
#error The variable for the parameter "U8ETCSCpAxSenAvail" is not defined
#endif

#ifndef U8ETCSCpAyAsp
#error The variable for the parameter "U8ETCSCpAyAsp" is not defined
#endif

#ifndef U8ETCSCpAyIce
#error The variable for the parameter "U8ETCSCpAyIce" is not defined
#endif

#ifndef U8ETCSCpAySnw
#error The variable for the parameter "U8ETCSCpAySnw" is not defined
#endif

#ifndef U8ETCSCpBigSpnMaxIgFac
#error The variable for the parameter "U8ETCSCpBigSpnMaxIgFac" is not defined
#endif

#ifndef U8ETCSCpCnt4Tar1stIncInTrn
#error The variable for the parameter "U8ETCSCpCnt4Tar1stIncInTrn" is not defined
#endif

#ifndef U8ETCSCpCnt4TarMaxDecInTrn
#error The variable for the parameter "U8ETCSCpCnt4TarMaxDecInTrn" is not defined
#endif

#ifndef U8ETCSCpCrnExDctCnt
#error The variable for the parameter "U8ETCSCpCrnExDctCnt" is not defined
#endif

#ifndef U8ETCSCpCtlActCntThAtHiAxlAcc
#error The variable for the parameter "U8ETCSCpCtlActCntThAtHiAxlAcc" is not defined
#endif

#ifndef U8ETCSCpCtlActCntThAtLwAxlAcc
#error The variable for the parameter "U8ETCSCpCtlActCntThAtLwAxlAcc" is not defined
#endif

#ifndef U8ETCSCpCtlActTmThInLmtCnrg
#error The variable for the parameter "U8ETCSCpCtlActTmThInLmtCnrg" is not defined
#endif

#ifndef U8ETCSCpCtlEnterAccPdlTh
#error The variable for the parameter "U8ETCSCpCtlEnterAccPdlTh" is not defined
#endif

#ifndef U8ETCSCpCtlExitAccPdlTh
#error The variable for the parameter "U8ETCSCpCtlExitAccPdlTh" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainAsp_1
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainAsp_1" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainAsp_2
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainAsp_2" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainAsp_3
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainAsp_3" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainAsp_4
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainAsp_4" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainAsp_5
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainAsp_5" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainIce_1
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainIce_1" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainIce_2
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainIce_2" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainIce_3
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainIce_3" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainIce_4
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainIce_4" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainIce_5
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainIce_5" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainSnw_1
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainSnw_1" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainSnw_2
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainSnw_2" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainSnw_3
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainSnw_3" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainSnw_4
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainSnw_4" is not defined
#endif

#ifndef U8ETCSCpDelYawDivrgGainSnw_5
#error The variable for the parameter "U8ETCSCpDelYawDivrgGainSnw_5" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainAsp_1
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainAsp_1" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainAsp_2
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainAsp_2" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainAsp_3
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainAsp_3" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainAsp_4
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainAsp_4" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainAsp_5
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainAsp_5" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainIce_1
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainIce_1" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainIce_2
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainIce_2" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainIce_3
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainIce_3" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainIce_4
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainIce_4" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainIce_5
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainIce_5" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainSnw_1
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainSnw_1" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainSnw_2
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainSnw_2" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainSnw_3
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainSnw_3" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainSnw_4
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainSnw_4" is not defined
#endif

#ifndef U8ETCSCpDelYawFstIncGainSnw_5
#error The variable for the parameter "U8ETCSCpDelYawFstIncGainSnw_5" is not defined
#endif

#ifndef U8ETCSCpDelYawLowTrqGainAsp
#error The variable for the parameter "U8ETCSCpDelYawLowTrqGainAsp" is not defined
#endif

#ifndef U8ETCSCpDelYawLowTrqGainIce
#error The variable for the parameter "U8ETCSCpDelYawLowTrqGainIce" is not defined
#endif

#ifndef U8ETCSCpDelYawLowTrqGainSnw
#error The variable for the parameter "U8ETCSCpDelYawLowTrqGainSnw" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainAsp_1
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainAsp_1" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainAsp_2
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainAsp_2" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainAsp_3
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainAsp_3" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainAsp_4
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainAsp_4" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainAsp_5
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainAsp_5" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainIce_1
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainIce_1" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainIce_2
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainIce_2" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainIce_3
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainIce_3" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainIce_4
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainIce_4" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainIce_5
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainIce_5" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainSnw_1
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainSnw_1" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainSnw_2
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainSnw_2" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainSnw_3
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainSnw_3" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainSnw_4
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainSnw_4" is not defined
#endif

#ifndef U8ETCSCpDelYawSlwIncGainSnw_5
#error The variable for the parameter "U8ETCSCpDelYawSlwIncGainSnw_5" is not defined
#endif

#ifndef U8ETCSCpDetDepSnwHilCnt
#error The variable for the parameter "U8ETCSCpDetDepSnwHilCnt" is not defined
#endif

#ifndef U8ETCSCpDltYawNVldSpd
#error The variable for the parameter "U8ETCSCpDltYawNVldSpd" is not defined
#endif

#ifndef U8ETCSCpDltYawOffset4MinTrgtSpn
#error The variable for the parameter "U8ETCSCpDltYawOffset4MinTrgtSpn" is not defined
#endif

#ifndef U8ETCSCpDltYawStbDctCnt
#error The variable for the parameter "U8ETCSCpDltYawStbDctCnt" is not defined
#endif

#ifndef U8ETCSCpDltYawVldSpd
#error The variable for the parameter "U8ETCSCpDltYawVldSpd" is not defined
#endif

#ifndef U8ETCSCpDltYawVldVyHSpd
#error The variable for the parameter "U8ETCSCpDltYawVldVyHSpd" is not defined
#endif

#ifndef U8ETCSCpEngAvail
#error The variable for the parameter "U8ETCSCpEngAvail" is not defined
#endif

#ifndef U8ETCSCpEngStallRpmOffset
#error The variable for the parameter "U8ETCSCpEngStallRpmOffset" is not defined
#endif

#ifndef U8ETCSCpEngTrqFltGainCmdDec
#error The variable for the parameter "U8ETCSCpEngTrqFltGainCmdDec" is not defined
#endif

#ifndef U8ETCSCpEngTrqFltGainCmdInc
#error The variable for the parameter "U8ETCSCpEngTrqFltGainCmdInc" is not defined
#endif

#ifndef U8ETCSCpFtr4JmpDnMax
#error The variable for the parameter "U8ETCSCpFtr4JmpDnMax" is not defined
#endif

#ifndef U8ETCSCpFtr4JmpDnMin
#error The variable for the parameter "U8ETCSCpFtr4JmpDnMin" is not defined
#endif

#ifndef U8ETCSCpFtr4JmpUpMax
#error The variable for the parameter "U8ETCSCpFtr4JmpUpMax" is not defined
#endif

#ifndef U8ETCSCpFtr4JmpUpMin
#error The variable for the parameter "U8ETCSCpFtr4JmpUpMin" is not defined
#endif

#ifndef U8ETCSCpFtr4RefTrqAt2ndCylStrt
#error The variable for the parameter "U8ETCSCpFtr4RefTrqAt2ndCylStrt" is not defined
#endif

#ifndef U8ETCSCpFtr4RefTrqAtVcaOfCtl
#error The variable for the parameter "U8ETCSCpFtr4RefTrqAtVcaOfCtl" is not defined
#endif

#ifndef U8ETCSCpFunLampOffCnt
#error The variable for the parameter "U8ETCSCpFunLampOffCnt" is not defined
#endif

#ifndef U8ETCSCpGainTrnsTmAftrGearShft
#error The variable for the parameter "U8ETCSCpGainTrnsTmAftrGearShft" is not defined
#endif

#ifndef U8ETCSCpGrTransTme
#error The variable for the parameter "U8ETCSCpGrTransTme" is not defined
#endif

#ifndef U8ETCSCpGradientTrstCplCnt
#error The variable for the parameter "U8ETCSCpGradientTrstCplCnt" is not defined
#endif

#ifndef U8ETCSCpGradientTrstStCnt
#error The variable for the parameter "U8ETCSCpGradientTrstStCnt" is not defined
#endif

#ifndef U8ETCSCpGrdTransTme
#error The variable for the parameter "U8ETCSCpGrdTransTme" is not defined
#endif

#ifndef U8ETCSCpH2LDctHldTm
#error The variable for the parameter "U8ETCSCpH2LDctHldTm" is not defined
#endif

#ifndef U8ETCSCpH2LDctSpnTh
#error The variable for the parameter "U8ETCSCpH2LDctSpnTh" is not defined
#endif

#ifndef U8ETCSCpHiAxlAccTh
#error The variable for the parameter "U8ETCSCpHiAxlAccTh" is not defined
#endif

#ifndef U8ETCSCpHillClrTime
#error The variable for the parameter "U8ETCSCpHillClrTime" is not defined
#endif

#ifndef U8ETCSCpHillDctAlongTh
#error The variable for the parameter "U8ETCSCpHillDctAlongTh" is not defined
#endif

#ifndef U8ETCSCpHillDctTime
#error The variable for the parameter "U8ETCSCpHillDctTime" is not defined
#endif

#ifndef U8ETCSCpHillDctTimeAx
#error The variable for the parameter "U8ETCSCpHillDctTimeAx" is not defined
#endif

#ifndef U8ETCSCpHillDiffAx
#error The variable for the parameter "U8ETCSCpHillDiffAx" is not defined
#endif

#ifndef U8ETCSCpHsaHillTime
#error The variable for the parameter "U8ETCSCpHsaHillTime" is not defined
#endif

#ifndef U8ETCSCpHysDeltAcc
#error The variable for the parameter "U8ETCSCpHysDeltAcc" is not defined
#endif

#ifndef U8ETCSCpHysDepSnwHilCnt
#error The variable for the parameter "U8ETCSCpHysDepSnwHilCnt" is not defined
#endif

#ifndef U8ETCSCpIgFacGrChgErrNeg_1
#error The variable for the parameter "U8ETCSCpIgFacGrChgErrNeg_1" is not defined
#endif

#ifndef U8ETCSCpIgFacGrChgErrNeg_2
#error The variable for the parameter "U8ETCSCpIgFacGrChgErrNeg_2" is not defined
#endif

#ifndef U8ETCSCpIgFacGrChgErrNeg_3
#error The variable for the parameter "U8ETCSCpIgFacGrChgErrNeg_3" is not defined
#endif

#ifndef U8ETCSCpIgFacGrChgErrNeg_4
#error The variable for the parameter "U8ETCSCpIgFacGrChgErrNeg_4" is not defined
#endif

#ifndef U8ETCSCpIgFacGrChgErrNeg_5
#error The variable for the parameter "U8ETCSCpIgFacGrChgErrNeg_5" is not defined
#endif

#ifndef U8ETCSCpIgFacInTrnAsp_1
#error The variable for the parameter "U8ETCSCpIgFacInTrnAsp_1" is not defined
#endif

#ifndef U8ETCSCpIgFacInTrnAsp_2
#error The variable for the parameter "U8ETCSCpIgFacInTrnAsp_2" is not defined
#endif

#ifndef U8ETCSCpIgFacInTrnAsp_3
#error The variable for the parameter "U8ETCSCpIgFacInTrnAsp_3" is not defined
#endif

#ifndef U8ETCSCpIgFacInTrnAsp_4
#error The variable for the parameter "U8ETCSCpIgFacInTrnAsp_4" is not defined
#endif

#ifndef U8ETCSCpIgFacInTrnAsp_5
#error The variable for the parameter "U8ETCSCpIgFacInTrnAsp_5" is not defined
#endif

#ifndef U8ETCSCpIgFacInTrnIce_1
#error The variable for the parameter "U8ETCSCpIgFacInTrnIce_1" is not defined
#endif

#ifndef U8ETCSCpIgFacInTrnIce_2
#error The variable for the parameter "U8ETCSCpIgFacInTrnIce_2" is not defined
#endif

#ifndef U8ETCSCpIgFacInTrnIce_3
#error The variable for the parameter "U8ETCSCpIgFacInTrnIce_3" is not defined
#endif

#ifndef U8ETCSCpIgFacInTrnIce_4
#error The variable for the parameter "U8ETCSCpIgFacInTrnIce_4" is not defined
#endif

#ifndef U8ETCSCpIgFacInTrnIce_5
#error The variable for the parameter "U8ETCSCpIgFacInTrnIce_5" is not defined
#endif

#ifndef U8ETCSCpIgFacInTrnSnw_1
#error The variable for the parameter "U8ETCSCpIgFacInTrnSnw_1" is not defined
#endif

#ifndef U8ETCSCpIgFacInTrnSnw_2
#error The variable for the parameter "U8ETCSCpIgFacInTrnSnw_2" is not defined
#endif

#ifndef U8ETCSCpIgFacInTrnSnw_3
#error The variable for the parameter "U8ETCSCpIgFacInTrnSnw_3" is not defined
#endif

#ifndef U8ETCSCpIgFacInTrnSnw_4
#error The variable for the parameter "U8ETCSCpIgFacInTrnSnw_4" is not defined
#endif

#ifndef U8ETCSCpIgFacInTrnSnw_5
#error The variable for the parameter "U8ETCSCpIgFacInTrnSnw_5" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnAsp_1
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnAsp_1" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnAsp_2
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnAsp_2" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnAsp_3
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnAsp_3" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnAsp_4
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnAsp_4" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnAsp_5
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnAsp_5" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnIce_1
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnIce_1" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnIce_2
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnIce_2" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnIce_3
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnIce_3" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnIce_4
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnIce_4" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnIce_5
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnIce_5" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnSnow_1
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnSnow_1" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnSnow_2
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnSnow_2" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnSnow_3
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnSnow_3" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnSnow_4
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnSnow_4" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnSnow_5
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnSnow_5" is not defined
#endif

#ifndef U8ETCSCpLdTrqByGrdTransTme
#error The variable for the parameter "U8ETCSCpLdTrqByGrdTransTme" is not defined
#endif

#ifndef U8ETCSCpLdTrqChngFac
#error The variable for the parameter "U8ETCSCpLdTrqChngFac" is not defined
#endif

#ifndef U8ETCSCpLowWhlSpnCntDnTh
#error The variable for the parameter "U8ETCSCpLowWhlSpnCntDnTh" is not defined
#endif

#ifndef U8ETCSCpLowWhlSpnCntUpTh
#error The variable for the parameter "U8ETCSCpLowWhlSpnCntUpTh" is not defined
#endif

#ifndef U8ETCSCpLwAxlAccTh
#error The variable for the parameter "U8ETCSCpLwAxlAccTh" is not defined
#endif

#ifndef U8ETCSCpLwToHiDetAreaCnt
#error The variable for the parameter "U8ETCSCpLwToHiDetAreaCnt" is not defined
#endif

#ifndef U8ETCSCpLwToHiDetCnt
#error The variable for the parameter "U8ETCSCpLwToHiDetCnt" is not defined
#endif

#ifndef U8ETCSCpLwWhlSpnTh2ClrCyl1st
#error The variable for the parameter "U8ETCSCpLwWhlSpnTh2ClrCyl1st" is not defined
#endif

#ifndef U8ETCSCpMinTarSpn
#error The variable for the parameter "U8ETCSCpMinTarSpn" is not defined
#endif

#ifndef U8ETCSCpNAlwAddTar4DepSnwSpd
#error The variable for the parameter "U8ETCSCpNAlwAddTar4DepSnwSpd" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainAsp_1
#error The variable for the parameter "U8ETCSCpNegErrIgainAsp_1" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainAsp_2
#error The variable for the parameter "U8ETCSCpNegErrIgainAsp_2" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainAsp_3
#error The variable for the parameter "U8ETCSCpNegErrIgainAsp_3" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainAsp_4
#error The variable for the parameter "U8ETCSCpNegErrIgainAsp_4" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainAsp_5
#error The variable for the parameter "U8ETCSCpNegErrIgainAsp_5" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainIce_1
#error The variable for the parameter "U8ETCSCpNegErrIgainIce_1" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainIce_2
#error The variable for the parameter "U8ETCSCpNegErrIgainIce_2" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainIce_3
#error The variable for the parameter "U8ETCSCpNegErrIgainIce_3" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainIce_4
#error The variable for the parameter "U8ETCSCpNegErrIgainIce_4" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainIce_5
#error The variable for the parameter "U8ETCSCpNegErrIgainIce_5" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainSnw_1
#error The variable for the parameter "U8ETCSCpNegErrIgainSnw_1" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainSnw_2
#error The variable for the parameter "U8ETCSCpNegErrIgainSnw_2" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainSnw_3
#error The variable for the parameter "U8ETCSCpNegErrIgainSnw_3" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainSnw_4
#error The variable for the parameter "U8ETCSCpNegErrIgainSnw_4" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainSnw_5
#error The variable for the parameter "U8ETCSCpNegErrIgainSnw_5" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainSplt_1
#error The variable for the parameter "U8ETCSCpNegErrIgainSplt_1" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainSplt_2
#error The variable for the parameter "U8ETCSCpNegErrIgainSplt_2" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainSplt_3
#error The variable for the parameter "U8ETCSCpNegErrIgainSplt_3" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainSplt_4
#error The variable for the parameter "U8ETCSCpNegErrIgainSplt_4" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainSplt_5
#error The variable for the parameter "U8ETCSCpNegErrIgainSplt_5" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainAsp_1
#error The variable for the parameter "U8ETCSCpNegErrPgainAsp_1" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainAsp_2
#error The variable for the parameter "U8ETCSCpNegErrPgainAsp_2" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainAsp_3
#error The variable for the parameter "U8ETCSCpNegErrPgainAsp_3" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainAsp_4
#error The variable for the parameter "U8ETCSCpNegErrPgainAsp_4" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainAsp_5
#error The variable for the parameter "U8ETCSCpNegErrPgainAsp_5" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainIce_1
#error The variable for the parameter "U8ETCSCpNegErrPgainIce_1" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainIce_2
#error The variable for the parameter "U8ETCSCpNegErrPgainIce_2" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainIce_3
#error The variable for the parameter "U8ETCSCpNegErrPgainIce_3" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainIce_4
#error The variable for the parameter "U8ETCSCpNegErrPgainIce_4" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainIce_5
#error The variable for the parameter "U8ETCSCpNegErrPgainIce_5" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSnw_1
#error The variable for the parameter "U8ETCSCpNegErrPgainSnw_1" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSnw_2
#error The variable for the parameter "U8ETCSCpNegErrPgainSnw_2" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSnw_3
#error The variable for the parameter "U8ETCSCpNegErrPgainSnw_3" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSnw_4
#error The variable for the parameter "U8ETCSCpNegErrPgainSnw_4" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSnw_5
#error The variable for the parameter "U8ETCSCpNegErrPgainSnw_5" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSplt_1
#error The variable for the parameter "U8ETCSCpNegErrPgainSplt_1" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSplt_2
#error The variable for the parameter "U8ETCSCpNegErrPgainSplt_2" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSplt_3
#error The variable for the parameter "U8ETCSCpNegErrPgainSplt_3" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSplt_4
#error The variable for the parameter "U8ETCSCpNegErrPgainSplt_4" is not defined
#endif

#ifndef U8ETCSCpNegErrPgainSplt_5
#error The variable for the parameter "U8ETCSCpNegErrPgainSplt_5" is not defined
#endif

#ifndef U8ETCSCpPGainIncFacH2L
#error The variable for the parameter "U8ETCSCpPGainIncFacH2L" is not defined
#endif

#ifndef U8ETCSCpPgFacGrChgErrNeg_1
#error The variable for the parameter "U8ETCSCpPgFacGrChgErrNeg_1" is not defined
#endif

#ifndef U8ETCSCpPgFacGrChgErrNeg_2
#error The variable for the parameter "U8ETCSCpPgFacGrChgErrNeg_2" is not defined
#endif

#ifndef U8ETCSCpPgFacGrChgErrNeg_3
#error The variable for the parameter "U8ETCSCpPgFacGrChgErrNeg_3" is not defined
#endif

#ifndef U8ETCSCpPgFacGrChgErrNeg_4
#error The variable for the parameter "U8ETCSCpPgFacGrChgErrNeg_4" is not defined
#endif

#ifndef U8ETCSCpPgFacGrChgErrNeg_5
#error The variable for the parameter "U8ETCSCpPgFacGrChgErrNeg_5" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnAsp_1
#error The variable for the parameter "U8ETCSCpPgFacInTrnAsp_1" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnAsp_2
#error The variable for the parameter "U8ETCSCpPgFacInTrnAsp_2" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnAsp_3
#error The variable for the parameter "U8ETCSCpPgFacInTrnAsp_3" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnAsp_4
#error The variable for the parameter "U8ETCSCpPgFacInTrnAsp_4" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnAsp_5
#error The variable for the parameter "U8ETCSCpPgFacInTrnAsp_5" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnIce_1
#error The variable for the parameter "U8ETCSCpPgFacInTrnIce_1" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnIce_2
#error The variable for the parameter "U8ETCSCpPgFacInTrnIce_2" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnIce_3
#error The variable for the parameter "U8ETCSCpPgFacInTrnIce_3" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnIce_4
#error The variable for the parameter "U8ETCSCpPgFacInTrnIce_4" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnIce_5
#error The variable for the parameter "U8ETCSCpPgFacInTrnIce_5" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnSnw_1
#error The variable for the parameter "U8ETCSCpPgFacInTrnSnw_1" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnSnw_2
#error The variable for the parameter "U8ETCSCpPgFacInTrnSnw_2" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnSnw_3
#error The variable for the parameter "U8ETCSCpPgFacInTrnSnw_3" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnSnw_4
#error The variable for the parameter "U8ETCSCpPgFacInTrnSnw_4" is not defined
#endif

#ifndef U8ETCSCpPgFacInTrnSnw_5
#error The variable for the parameter "U8ETCSCpPgFacInTrnSnw_5" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainAsp_1
#error The variable for the parameter "U8ETCSCpPosErrIgainAsp_1" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainAsp_2
#error The variable for the parameter "U8ETCSCpPosErrIgainAsp_2" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainAsp_3
#error The variable for the parameter "U8ETCSCpPosErrIgainAsp_3" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainAsp_4
#error The variable for the parameter "U8ETCSCpPosErrIgainAsp_4" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainAsp_5
#error The variable for the parameter "U8ETCSCpPosErrIgainAsp_5" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainIce_1
#error The variable for the parameter "U8ETCSCpPosErrIgainIce_1" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainIce_2
#error The variable for the parameter "U8ETCSCpPosErrIgainIce_2" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainIce_3
#error The variable for the parameter "U8ETCSCpPosErrIgainIce_3" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainIce_4
#error The variable for the parameter "U8ETCSCpPosErrIgainIce_4" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainIce_5
#error The variable for the parameter "U8ETCSCpPosErrIgainIce_5" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainSnw_1
#error The variable for the parameter "U8ETCSCpPosErrIgainSnw_1" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainSnw_2
#error The variable for the parameter "U8ETCSCpPosErrIgainSnw_2" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainSnw_3
#error The variable for the parameter "U8ETCSCpPosErrIgainSnw_3" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainSnw_4
#error The variable for the parameter "U8ETCSCpPosErrIgainSnw_4" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainSnw_5
#error The variable for the parameter "U8ETCSCpPosErrIgainSnw_5" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainSplt_1
#error The variable for the parameter "U8ETCSCpPosErrIgainSplt_1" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainSplt_2
#error The variable for the parameter "U8ETCSCpPosErrIgainSplt_2" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainSplt_3
#error The variable for the parameter "U8ETCSCpPosErrIgainSplt_3" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainSplt_4
#error The variable for the parameter "U8ETCSCpPosErrIgainSplt_4" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainSplt_5
#error The variable for the parameter "U8ETCSCpPosErrIgainSplt_5" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainAsp_1
#error The variable for the parameter "U8ETCSCpPosErrPgainAsp_1" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainAsp_2
#error The variable for the parameter "U8ETCSCpPosErrPgainAsp_2" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainAsp_3
#error The variable for the parameter "U8ETCSCpPosErrPgainAsp_3" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainAsp_4
#error The variable for the parameter "U8ETCSCpPosErrPgainAsp_4" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainAsp_5
#error The variable for the parameter "U8ETCSCpPosErrPgainAsp_5" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainIce_1
#error The variable for the parameter "U8ETCSCpPosErrPgainIce_1" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainIce_2
#error The variable for the parameter "U8ETCSCpPosErrPgainIce_2" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainIce_3
#error The variable for the parameter "U8ETCSCpPosErrPgainIce_3" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainIce_4
#error The variable for the parameter "U8ETCSCpPosErrPgainIce_4" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainIce_5
#error The variable for the parameter "U8ETCSCpPosErrPgainIce_5" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSnw_1
#error The variable for the parameter "U8ETCSCpPosErrPgainSnw_1" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSnw_2
#error The variable for the parameter "U8ETCSCpPosErrPgainSnw_2" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSnw_3
#error The variable for the parameter "U8ETCSCpPosErrPgainSnw_3" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSnw_4
#error The variable for the parameter "U8ETCSCpPosErrPgainSnw_4" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSnw_5
#error The variable for the parameter "U8ETCSCpPosErrPgainSnw_5" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSplt_1
#error The variable for the parameter "U8ETCSCpPosErrPgainSplt_1" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSplt_2
#error The variable for the parameter "U8ETCSCpPosErrPgainSplt_2" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSplt_3
#error The variable for the parameter "U8ETCSCpPosErrPgainSplt_3" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSplt_4
#error The variable for the parameter "U8ETCSCpPosErrPgainSplt_4" is not defined
#endif

#ifndef U8ETCSCpPosErrPgainSplt_5
#error The variable for the parameter "U8ETCSCpPosErrPgainSplt_5" is not defined
#endif

#ifndef U8ETCSCpRecTrqFacInBump
#error The variable for the parameter "U8ETCSCpRecTrqFacInBump" is not defined
#endif

#ifndef U8ETCSCpRefTrqNvldTm
#error The variable for the parameter "U8ETCSCpRefTrqNvldTm" is not defined
#endif

#ifndef U8ETCSCpRefTrqVldTm
#error The variable for the parameter "U8ETCSCpRefTrqVldTm" is not defined
#endif

#ifndef U8ETCSCpSplitHillClrAccel
#error The variable for the parameter "U8ETCSCpSplitHillClrAccel" is not defined
#endif

#ifndef U8ETCSCpSplitHillClrVehSpd
#error The variable for the parameter "U8ETCSCpSplitHillClrVehSpd" is not defined
#endif

#ifndef U8ETCSCpSplitHillDctAccel
#error The variable for the parameter "U8ETCSCpSplitHillDctAccel" is not defined
#endif

#ifndef U8ETCSCpSplitHillDctMinAx
#error The variable for the parameter "U8ETCSCpSplitHillDctMinAx" is not defined
#endif

#ifndef U8ETCSCpSplitHillDctVehSpd
#error The variable for the parameter "U8ETCSCpSplitHillDctVehSpd" is not defined
#endif

#ifndef U8ETCSCpStDeltAcc
#error The variable for the parameter "U8ETCSCpStDeltAcc" is not defined
#endif

#ifndef U8ETCSCpSymBrkCtlIhbAxTh
#error The variable for the parameter "U8ETCSCpSymBrkCtlIhbAxTh" is not defined
#endif

#ifndef U8ETCSCpSymBrkCtlTh_1
#error The variable for the parameter "U8ETCSCpSymBrkCtlTh_1" is not defined
#endif

#ifndef U8ETCSCpSymBrkCtlTh_2
#error The variable for the parameter "U8ETCSCpSymBrkCtlTh_2" is not defined
#endif

#ifndef U8ETCSCpSymBrkCtlTh_3
#error The variable for the parameter "U8ETCSCpSymBrkCtlTh_3" is not defined
#endif

#ifndef U8ETCSCpSymBrkCtlTh_4
#error The variable for the parameter "U8ETCSCpSymBrkCtlTh_4" is not defined
#endif

#ifndef U8ETCSCpSymBrkCtlTh_5
#error The variable for the parameter "U8ETCSCpSymBrkCtlTh_5" is not defined
#endif

#ifndef U8ETCSCpSymBrkTrqDecRate
#error The variable for the parameter "U8ETCSCpSymBrkTrqDecRate" is not defined
#endif

#ifndef U8ETCSCpSymBrkTrqMax_1
#error The variable for the parameter "U8ETCSCpSymBrkTrqMax_1" is not defined
#endif

#ifndef U8ETCSCpSymBrkTrqMax_2
#error The variable for the parameter "U8ETCSCpSymBrkTrqMax_2" is not defined
#endif

#ifndef U8ETCSCpSymBrkTrqMax_3
#error The variable for the parameter "U8ETCSCpSymBrkTrqMax_3" is not defined
#endif

#ifndef U8ETCSCpSymBrkTrqMax_4
#error The variable for the parameter "U8ETCSCpSymBrkTrqMax_4" is not defined
#endif

#ifndef U8ETCSCpSymBrkTrqMax_5
#error The variable for the parameter "U8ETCSCpSymBrkTrqMax_5" is not defined
#endif

#ifndef U8ETCSCpTarNChngDltYawHiBrkTrq
#error The variable for the parameter "U8ETCSCpTarNChngDltYawHiBrkTrq" is not defined
#endif

#ifndef U8ETCSCpTarNChngDltYawHiSpd
#error The variable for the parameter "U8ETCSCpTarNChngDltYawHiSpd" is not defined
#endif

#ifndef U8ETCSCpTarNChngDltYawLwBrkTrq
#error The variable for the parameter "U8ETCSCpTarNChngDltYawLwBrkTrq" is not defined
#endif

#ifndef U8ETCSCpTarNChngDltYawLwSpd
#error The variable for the parameter "U8ETCSCpTarNChngDltYawLwSpd" is not defined
#endif

#ifndef U8ETCSCpTarNChngDltYawVyHSpd
#error The variable for the parameter "U8ETCSCpTarNChngDltYawVyHSpd" is not defined
#endif

#ifndef U8ETCSCpTarSpnAdd4AbnmSizTire
#error The variable for the parameter "U8ETCSCpTarSpnAdd4AbnmSizTire" is not defined
#endif

#ifndef U8ETCSCpTarSpnAdd4AdHiRd
#error The variable for the parameter "U8ETCSCpTarSpnAdd4AdHiRd" is not defined
#endif

#ifndef U8ETCSCpTarSpnAdd4DepSnwHil
#error The variable for the parameter "U8ETCSCpTarSpnAdd4DepSnwHil" is not defined
#endif

#ifndef U8ETCSCpTarSpnAdd4DrvPrtnMd
#error The variable for the parameter "U8ETCSCpTarSpnAdd4DrvPrtnMd" is not defined
#endif

#ifndef U8ETCSCpTarSpnAdd4Hill
#error The variable for the parameter "U8ETCSCpTarSpnAdd4Hill" is not defined
#endif

#ifndef U8ETCSCpTarSpnAdd4PreHiRd
#error The variable for the parameter "U8ETCSCpTarSpnAdd4PreHiRd" is not defined
#endif

#ifndef U8ETCSCpTarSpnAdd4Sp
#error The variable for the parameter "U8ETCSCpTarSpnAdd4Sp" is not defined
#endif

#ifndef U8ETCSCpTarSpnTransTme
#error The variable for the parameter "U8ETCSCpTarSpnTransTme" is not defined
#endif

#ifndef U8ETCSCpTarTransCplBrkTrq2Sp
#error The variable for the parameter "U8ETCSCpTarTransCplBrkTrq2Sp" is not defined
#endif

#ifndef U8ETCSCpTarTransStBrkTrq2Sp
#error The variable for the parameter "U8ETCSCpTarTransStBrkTrq2Sp" is not defined
#endif

#ifndef U8ETCSCpTarWhlSpin_1
#error The variable for the parameter "U8ETCSCpTarWhlSpin_1" is not defined
#endif

#ifndef U8ETCSCpTarWhlSpin_2
#error The variable for the parameter "U8ETCSCpTarWhlSpin_2" is not defined
#endif

#ifndef U8ETCSCpTarWhlSpin_3
#error The variable for the parameter "U8ETCSCpTarWhlSpin_3" is not defined
#endif

#ifndef U8ETCSCpTarWhlSpin_4
#error The variable for the parameter "U8ETCSCpTarWhlSpin_4" is not defined
#endif

#ifndef U8ETCSCpTarWhlSpin_5
#error The variable for the parameter "U8ETCSCpTarWhlSpin_5" is not defined
#endif

#ifndef U8ETCSCpTimeTh2ClrCyl1st
#error The variable for the parameter "U8ETCSCpTimeTh2ClrCyl1st" is not defined
#endif

#ifndef U8ETCSCpTransEff_1
#error The variable for the parameter "U8ETCSCpTransEff_1" is not defined
#endif

#ifndef U8ETCSCpTransEff_2
#error The variable for the parameter "U8ETCSCpTransEff_2" is not defined
#endif

#ifndef U8ETCSCpTransEff_3
#error The variable for the parameter "U8ETCSCpTransEff_3" is not defined
#endif

#ifndef U8ETCSCpTransEff_4
#error The variable for the parameter "U8ETCSCpTransEff_4" is not defined
#endif

#ifndef U8ETCSCpTransEff_5
#error The variable for the parameter "U8ETCSCpTransEff_5" is not defined
#endif

#ifndef U8ETCSCpTransEff_R
#error The variable for the parameter "U8ETCSCpTransEff_R" is not defined
#endif

#ifndef U8ETCSCpTrqDel_1
#error The variable for the parameter "U8ETCSCpTrqDel_1" is not defined
#endif

#ifndef U8ETCSCpTrqDel_2
#error The variable for the parameter "U8ETCSCpTrqDel_2" is not defined
#endif

#ifndef U8ETCSCpTrqDel_3
#error The variable for the parameter "U8ETCSCpTrqDel_3" is not defined
#endif

#ifndef U8ETCSCpTrqDel_4
#error The variable for the parameter "U8ETCSCpTrqDel_4" is not defined
#endif

#ifndef U8ETCSCpTrqDel_5
#error The variable for the parameter "U8ETCSCpTrqDel_5" is not defined
#endif

#ifndef U8TCSCpAddRefRdCfFrci
#error The variable for the parameter "U8TCSCpAddRefRdCfFrci" is not defined
#endif

#ifndef U8TCSCpHghMuWhlSpinTh
#error The variable for the parameter "U8TCSCpHghMuWhlSpinTh" is not defined
#endif

#ifndef U8TMCCpTurnIndex_1
#error The variable for the parameter "U8TMCCpTurnIndex_1" is not defined
#endif

#ifndef U8TMCCpTurnIndex_2
#error The variable for the parameter "U8TMCCpTurnIndex_2" is not defined
#endif

#ifndef U8TMCCpTurnIndex_3
#error The variable for the parameter "U8TMCCpTurnIndex_3" is not defined
#endif

#ifndef U8TMCpAllowDnShftMinVehSpd_1
#error The variable for the parameter "U8TMCpAllowDnShftMinVehSpd_1" is not defined
#endif

#ifndef U8TMCpAllowDnShftMinVehSpd_2
#error The variable for the parameter "U8TMCpAllowDnShftMinVehSpd_2" is not defined
#endif

#ifndef U8TMCpAllowDnShftMinVehSpd_3
#error The variable for the parameter "U8TMCpAllowDnShftMinVehSpd_3" is not defined
#endif

#ifndef U8TMCpAllowDnShftMinVehSpd_4
#error The variable for the parameter "U8TMCpAllowDnShftMinVehSpd_4" is not defined
#endif

#ifndef U8TMCpAllowDnShftMinVehSpd_5
#error The variable for the parameter "U8TMCpAllowDnShftMinVehSpd_5" is not defined
#endif

#ifndef U8TMCpAllowDnShftRPM_1
#error The variable for the parameter "U8TMCpAllowDnShftRPM_1" is not defined
#endif

#ifndef U8TMCpAllowDnShftRPM_2
#error The variable for the parameter "U8TMCpAllowDnShftRPM_2" is not defined
#endif

#ifndef U8TMCpAllowDnShftRPM_3
#error The variable for the parameter "U8TMCpAllowDnShftRPM_3" is not defined
#endif

#ifndef U8TMCpAllowDnShftRPM_4
#error The variable for the parameter "U8TMCpAllowDnShftRPM_4" is not defined
#endif

#ifndef U8TMCpAllowDnShftRPM_5
#error The variable for the parameter "U8TMCpAllowDnShftRPM_5" is not defined
#endif

#ifndef U8TMCpAllowDnShftVehSpd_1
#error The variable for the parameter "U8TMCpAllowDnShftVehSpd_1" is not defined
#endif

#ifndef U8TMCpAllowDnShftVehSpd_2
#error The variable for the parameter "U8TMCpAllowDnShftVehSpd_2" is not defined
#endif

#ifndef U8TMCpAllowDnShftVehSpd_3
#error The variable for the parameter "U8TMCpAllowDnShftVehSpd_3" is not defined
#endif

#ifndef U8TMCpAllowDnShftVehSpd_4
#error The variable for the parameter "U8TMCpAllowDnShftVehSpd_4" is not defined
#endif

#ifndef U8TMCpAllowDnShftVehSpd_5
#error The variable for the parameter "U8TMCpAllowDnShftVehSpd_5" is not defined
#endif

#ifndef U8TMCpAllowUpShftMaxVehSpd_1
#error The variable for the parameter "U8TMCpAllowUpShftMaxVehSpd_1" is not defined
#endif

#ifndef U8TMCpAllowUpShftMaxVehSpd_2
#error The variable for the parameter "U8TMCpAllowUpShftMaxVehSpd_2" is not defined
#endif

#ifndef U8TMCpAllowUpShftMaxVehSpd_3
#error The variable for the parameter "U8TMCpAllowUpShftMaxVehSpd_3" is not defined
#endif

#ifndef U8TMCpAllowUpShftMaxVehSpd_4
#error The variable for the parameter "U8TMCpAllowUpShftMaxVehSpd_4" is not defined
#endif

#ifndef U8TMCpAllowUpShftMaxVehSpd_5
#error The variable for the parameter "U8TMCpAllowUpShftMaxVehSpd_5" is not defined
#endif

#ifndef U8TMCpAllowUpShftRPM_1
#error The variable for the parameter "U8TMCpAllowUpShftRPM_1" is not defined
#endif

#ifndef U8TMCpAllowUpShftRPM_2
#error The variable for the parameter "U8TMCpAllowUpShftRPM_2" is not defined
#endif

#ifndef U8TMCpAllowUpShftRPM_3
#error The variable for the parameter "U8TMCpAllowUpShftRPM_3" is not defined
#endif

#ifndef U8TMCpAllowUpShftRPM_4
#error The variable for the parameter "U8TMCpAllowUpShftRPM_4" is not defined
#endif

#ifndef U8TMCpAllowUpShftRPM_5
#error The variable for the parameter "U8TMCpAllowUpShftRPM_5" is not defined
#endif

#ifndef U8TMCpAllowUpShftVehSpd_1
#error The variable for the parameter "U8TMCpAllowUpShftVehSpd_1" is not defined
#endif

#ifndef U8TMCpAllowUpShftVehSpd_2
#error The variable for the parameter "U8TMCpAllowUpShftVehSpd_2" is not defined
#endif

#ifndef U8TMCpAllowUpShftVehSpd_3
#error The variable for the parameter "U8TMCpAllowUpShftVehSpd_3" is not defined
#endif

#ifndef U8TMCpAllowUpShftVehSpd_4
#error The variable for the parameter "U8TMCpAllowUpShftVehSpd_4" is not defined
#endif

#ifndef U8TMCpAllowUpShftVehSpd_5
#error The variable for the parameter "U8TMCpAllowUpShftVehSpd_5" is not defined
#endif

#ifndef U8TMCpGearShiftTime
#error The variable for the parameter "U8TMCpGearShiftTime" is not defined
#endif

#ifndef U8TMCpMaxGear
#error The variable for the parameter "U8TMCpMaxGear" is not defined
#endif

#ifndef U8TMCpMinGear
#error The variable for the parameter "U8TMCpMinGear" is not defined
#endif

#ifndef U8TMCpShftChgRPMHys
#error The variable for the parameter "U8TMCpShftChgRPMHys" is not defined
#endif

#ifndef U8TMCpShftChgRPMInTrn_1
#error The variable for the parameter "U8TMCpShftChgRPMInTrn_1" is not defined
#endif

#ifndef U8TMCpShftChgRPMInTrn_2
#error The variable for the parameter "U8TMCpShftChgRPMInTrn_2" is not defined
#endif

#ifndef U8TMCpShftChgRPMInTrn_3
#error The variable for the parameter "U8TMCpShftChgRPMInTrn_3" is not defined
#endif

#ifndef U8TMCpShftChgVehSpdHys
#error The variable for the parameter "U8TMCpShftChgVehSpdHys" is not defined
#endif

#ifndef U8TMCpShftChgVehSpdInHill
#error The variable for the parameter "U8TMCpShftChgVehSpdInHill" is not defined
#endif

#ifndef U8TMCpShftChgVehSpdInTrn_1
#error The variable for the parameter "U8TMCpShftChgVehSpdInTrn_1" is not defined
#endif

#ifndef U8TMCpShftChgVehSpdInTrn_2
#error The variable for the parameter "U8TMCpShftChgVehSpdInTrn_2" is not defined
#endif

#ifndef U8TMCpShftChgVehSpdInTrn_3
#error The variable for the parameter "U8TMCpShftChgVehSpdInTrn_3" is not defined
#endif

#ifndef VREF_10_KPH
#error The variable for the parameter "VREF_10_KPH" is not defined
#endif

#ifndef VREF_12_5_KPH
#error The variable for the parameter "VREF_12_5_KPH" is not defined
#endif

#ifndef VREF_30_KPH
#error The variable for the parameter "VREF_30_KPH" is not defined
#endif

#ifndef VREF_5_KPH
#error The variable for the parameter "VREF_5_KPH" is not defined
#endif

#ifndef VREF_6_KPH
#error The variable for the parameter "VREF_6_KPH" is not defined
#endif

#ifndef VREF_7_KPH
#error The variable for the parameter "VREF_7_KPH" is not defined
#endif

#ifndef VREF_DECEL_0_3_G
#error The variable for the parameter "VREF_DECEL_0_3_G" is not defined
#endif

#ifndef VarFwd
#error The variable for the parameter "VarFwd" is not defined
#endif

/* Private macros used by the generated code to access rtModel */
#ifndef rtmSetFirstInitCond
# define rtmSetFirstInitCond(rtm, val) ((rtm)->Timing.firstInitCondFlag = (val))
#endif

#ifndef rtmIsFirstInitCond
# define rtmIsFirstInitCond(rtm)       ((rtm)->Timing.firstInitCondFlag)
#endif

/* Imported (extern) block signals */
extern W_STRUCT_SIM FL;                /* '<Root>/FL' */
extern W_STRUCT_SIM FR;                /* '<Root>/FR' */
extern W_STRUCT_SIM RL;                /* '<Root>/RL' */
extern W_STRUCT_SIM RR;                /* '<Root>/RR' */
extern TCS_WL_STRUCT_SIM FL_TCS;       /* '<Root>/FL_TCS' */
extern TCS_WL_STRUCT_SIM FR_TCS;       /* '<Root>/FR_TCS' */
extern TCS_WL_STRUCT_SIM RL_TCS;       /* '<Root>/RL_TCS' */
extern TCS_WL_STRUCT_SIM RR_TCS;       /* '<Root>/RR_TCS' */
extern TCS_AXLE_STRUCT_SIM FA_TCS;     /* '<Root>/FA_TCS' */
extern TCS_AXLE_STRUCT_SIM RA_TCS;     /* '<Root>/RA_TCS' */
extern int16_T lctcss16VehSpd4TCS;     /* '<Root>/lctcss16VehSpd4TCS' */
extern int8_T gear_pos;                /* '<Root>/gear_pos' */
extern uint8_T lctcsu1GearShiftFlag;   /* '<Root>/lctcsu1GearShiftFlag' */
extern int16_T gs_sel;                 /* '<Root>/gs_sel' */
extern int16_T drive_torq;             /* '<Root>/drive_torq' */
extern int16_T eng_torq;               /* '<Root>/eng_torq' */
extern uint16_T eng_rpm;               /* '<Root>/eng_rpm' */
extern int16_T mtp;                    /* '<Root>/mtp' */
extern TCS_FLAG_t lcTcsCtrlFlg0;       /* '<Root>/lcTcsCtrlFlg0' */
extern U8_BIT_STRUCT_t VDCF7;         /* '<Root>/VDCF7' */
extern TCS_FLAG_t lcTcsCtrlFlg1;       /* '<Root>/lcTcsCtrlFlg1' */
extern U8_BIT_STRUCT_t VF15;          /* '<Root>/VF15' */
extern boolean_T lctcsu1DctBTCHillByAx;/* '<Root>/lctcsu1DctBTCHillByAx' */
extern int16_T wstr;                   /* '<Root>/wstr' */
extern int16_T wstr_dot;               /* '<Root>/wstr_dot' */
extern int16_T yaw_out;                /* '<Root>/yaw_out' */
extern int16_T alat;                   /* '<Root>/alat' */
extern int16_T ax_Filter;              /* '<Root>/ax_Filter' */
extern int16_T delta_yaw_first;        /* '<Root>/delta_yaw_first' */
extern int16_T rf;                     /* '<Root>/rf' */
extern int16_T nor_alat;               /* '<Root>/nor_alat' */
extern int16_T det_alatc;              /* '<Root>/det_alatc' */
extern int16_T det_alatm;              /* '<Root>/det_alatm' */
extern U8_BIT_STRUCT_t VDCF0;         /* '<Root>/VDCF0' */
extern int16_T det_wstr_dot;           /* '<Root>/det_wstr_dot' */
extern int16_T ltcss16BaseTarWhlSpinDiff;/* '<Root>/ltcss16BaseTarWhlSpinDiff' */
extern uint8_T lespu1AWD_LOW_ACT;      /* '<Root>/lespu1AWD_LOW_ACT' */
extern uint16_T lespu16AWD_4WD_TQC_CUR;/* '<Root>/lespu16AWD_4WD_TQC_CUR' */
extern TCS_CIRCUIT_STRUCT_SIM PC_TCS;  /* '<Root>/PC_TCS' */
extern TCS_CIRCUIT_STRUCT_SIM SC_TCS;  /* '<Root>/SC_TCS' */
extern U8_BIT_STRUCT_t VDCF1;         /* '<Root>/VDCF1' */
extern RTA_WL_STRUCT_SIM RTA_FL;       /* '<Root>/RTA_FL' */
extern RTA_WL_STRUCT_SIM RTA_FR;       /* '<Root>/RTA_FR' */
extern RTA_WL_STRUCT_SIM RTA_RL;       /* '<Root>/RTA_RL' */
extern RTA_WL_STRUCT_SIM RTA_RR;       /* '<Root>/RTA_RR' */
extern int16_T rta_ratio_temp;         /* '<Root>/rta_ratio_temp' */
extern U8_BIT_STRUCT_t HSAF0;         /* '<Root>/HSAF0' */
extern uint8_T lcu8SccMode;            /* '<Root>/lcu8SccMode' */
extern Abc_Ctrl_HdrBusType Abc_CtrlBus;/* '<Root>/fs_interface_flg' */
extern uint8_T lctcsu1BTCSVehAtv;      /* '<Root>/lctcsu1BTCSVehAtv' */
extern rtTimingBridge LCETCS_vCallMain_TimingBrdg;

#endif                                 /* RTW_HEADER_LCETCS_vCallMain_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
