/*
 * File: LCETCS_vDctEngStallEngExist.c
 *
 * Code generated for Simulink model 'LCETCS_vDctEngStallEngExist'.
 *
 * Model version                  : 1.228
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:55:40 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctEngStallEngExist.h"
#include "LCETCS_vDctEngStallEngExist_private.h"

/* Output and update for referenced model: 'LCETCS_vDctEngStallEngExist' */
void LCETCS_vDctEngStallEngExist(boolean_T rtu_lcetcsu1EngStallTemp, boolean_T
  *rty_lcetcsu1EngStall)
{
  /* Switch: '<Root>/Switch1' incorporates:
   *  Constant: '<Root>/Constant'
   *  Constant: '<Root>/Constant1'
   *  DataTypeConversion: '<Root>/Data Type Conversion'
   */
  if (((uint8_T)U8ETCSCpEngAvail) != 0) {
    *rty_lcetcsu1EngStall = rtu_lcetcsu1EngStallTemp;
  } else {
    *rty_lcetcsu1EngStall = false;
  }

  /* End of Switch: '<Root>/Switch1' */
}

/* Model initialize function */
void LCETCS_vDctEngStallEngExist_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
