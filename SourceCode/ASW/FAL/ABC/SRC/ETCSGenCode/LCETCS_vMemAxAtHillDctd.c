/*
 * File: LCETCS_vMemAxAtHillDctd.c
 *
 * Code generated for Simulink model 'LCETCS_vMemAxAtHillDctd'.
 *
 * Model version                  : 1.218
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:59:10 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vMemAxAtHillDctd.h"
#include "LCETCS_vMemAxAtHillDctd_private.h"

/* Initial conditions for referenced model: 'LCETCS_vMemAxAtHillDctd' */
void LCETCS_vMemAxAtHillDctd_Init(int16_T *rty_lcetcss16AxAtHillDctd,
  DW_LCETCS_vMemAxAtHillDctd_f_T *localDW, B_LCETCS_vMemAxAtHillDctd_c_T *localB)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctRisingEdge' */
  LCETCS_vDctRisingEdge_Init(&localB->RisingEdge,
    &(localDW->LCETCS_vDctRisingEdge_DWORK1.rtdw));

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = 0;

  /* InitializeConditions for Chart: '<Root>/Chart1' */
  *rty_lcetcss16AxAtHillDctd = 0;
}

/* Output and update for referenced model: 'LCETCS_vMemAxAtHillDctd' */
void LCETCS_vMemAxAtHillDctd(const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const
  TypeETCSSplitHillStruct *rtu_ETCS_SPLIT_HILL_OLD, int16_T
  *rty_lcetcss16AxAtHillDctd, DW_LCETCS_vMemAxAtHillDctd_f_T *localDW,
  B_LCETCS_vMemAxAtHillDctd_c_T *localB)
{
  /* ModelReference: '<Root>/LCETCS_vDctRisingEdge' */
  LCETCS_vDctRisingEdge(rtu_ETCS_SPLIT_HILL_OLD->lcetcsu1SplitHillDct,
                        &localB->RisingEdge,
                        &(localDW->LCETCS_vDctRisingEdge_DWORK1.rtdw));

  /* Chart: '<Root>/Chart1' incorporates:
   *  UnitDelay: '<Root>/Unit Delay1'
   */
  /* Gateway: Chart1 */
  /* During: Chart1 */
  /* Entry Internal: Chart1 */
  /* Transition: '<S1>:12' */
  if (rtu_ETCS_SPLIT_HILL_OLD->lcetcsu1SplitHillDct == 0) {
    /* Transition: '<S1>:14' */
    /* Transition: '<S1>:15' */
    *rty_lcetcss16AxAtHillDctd = 0;

    /* Transition: '<S1>:18' */
    /* Transition: '<S1>:19' */
  } else {
    /* Transition: '<S1>:13' */
    if (localB->RisingEdge == 1) {
      /* Transition: '<S1>:16' */
      /* Transition: '<S1>:17' */
      if (rtu_ETCS_ESC_SIG->lcetcss16LonAcc < 0) {
        *rty_lcetcss16AxAtHillDctd = (int16_T)
          (-rtu_ETCS_ESC_SIG->lcetcss16LonAcc);
      } else {
        *rty_lcetcss16AxAtHillDctd = rtu_ETCS_ESC_SIG->lcetcss16LonAcc;
      }

      /* Transition: '<S1>:19' */
    } else {
      /* Transition: '<S1>:20' */
      *rty_lcetcss16AxAtHillDctd = localDW->UnitDelay1_DSTATE;
    }
  }

  /* End of Chart: '<Root>/Chart1' */

  /* Update for UnitDelay: '<Root>/Unit Delay1' */
  /* Transition: '<S1>:21' */
  localDW->UnitDelay1_DSTATE = *rty_lcetcss16AxAtHillDctd;
}

/* Model initialize function */
void LCETCS_vMemAxAtHillDctd_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctRisingEdge' */
  LCETCS_vDctRisingEdge_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
