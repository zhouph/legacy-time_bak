/*
 * File: LCTMC_vCalRefEngSpdMapInTrn.c
 *
 * Code generated for Simulink model 'LCTMC_vCalRefEngSpdMapInTrn'.
 *
 * Model version                  : 1.230
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:03:31 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCTMC_vCalRefEngSpdMapInTrn.h"
#include "LCTMC_vCalRefEngSpdMapInTrn_private.h"

/* Initial conditions for referenced model: 'LCTMC_vCalRefEngSpdMapInTrn' */
void LCTMC_vCalRefEngSpdMapInTrn_Init(B_LCTMC_vCalRefEngSpdMapInTrn_c_T *localB)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter3Point' */
  LCETCS_s16Inter3Point_Init(&localB->y);
}

/* Start for referenced model: 'LCTMC_vCalRefEngSpdMapInTrn' */
void LCTMC_vCalRefEngSpdMapInTrn_Start(B_LCTMC_vCalRefEngSpdMapInTrn_c_T *localB)
{
  /* Start for Constant: '<Root>/Turn Index Low Level' */
  localB->TurnIndexLowLevel = ((uint8_T)U8TMCCpTurnIndex_1);

  /* Start for Constant: '<Root>/Turn Index Mid Level' */
  localB->TurnIndexMidLevel = ((uint8_T)U8TMCCpTurnIndex_2);

  /* Start for Constant: '<Root>/Turn Index High Level' */
  localB->TurnIndexHighLevel = ((uint8_T)U8TMCCpTurnIndex_3);
}

/* Output and update for referenced model: 'LCTMC_vCalRefEngSpdMapInTrn' */
void LCTMC_vCalRefEngSpdMapInTrn(const TypeETCSGainStruct *rtu_ETCS_CTL_GAINS,
  int16_T *rty_lctmcs16TarShtChnRPMInTrn, B_LCTMC_vCalRefEngSpdMapInTrn_c_T
  *localB)
{
  /* local block i/o variables */
  int16_T rtb_y1;
  int16_T rtb_y2;
  int16_T rtb_y3;

  /* DataTypeConversion: '<Root>/Data Type Conversion4' incorporates:
   *  Constant: '<Root>/Allowed Shift ChangeValue Engine Speed in Turn Index Low Level'
   */
  rtb_y1 = ((uint8_T)U8TMCpShftChgRPMInTrn_1);

  /* DataTypeConversion: '<Root>/Data Type Conversion5' incorporates:
   *  Constant: '<Root>/Allowed Shift ChangeValue Engine Speed in Turn Index Mid Level'
   */
  rtb_y2 = ((uint8_T)U8TMCpShftChgRPMInTrn_2);

  /* DataTypeConversion: '<Root>/Data Type Conversion6' incorporates:
   *  Constant: '<Root>/Allowed Shift ChangeValue Engine Speed in Turn Index High Level'
   */
  rtb_y3 = ((uint8_T)U8TMCpShftChgRPMInTrn_3);

  /* Constant: '<Root>/Turn Index Low Level' */
  localB->TurnIndexLowLevel = ((uint8_T)U8TMCCpTurnIndex_1);

  /* Constant: '<Root>/Turn Index Mid Level' */
  localB->TurnIndexMidLevel = ((uint8_T)U8TMCCpTurnIndex_2);

  /* Constant: '<Root>/Turn Index High Level' */
  localB->TurnIndexHighLevel = ((uint8_T)U8TMCCpTurnIndex_3);

  /* ModelReference: '<Root>/LCETCS_s16Inter3Point' */
  LCETCS_s16Inter3Point(rtu_ETCS_CTL_GAINS->lcetcss16PGainFacInTrn,
                        localB->TurnIndexLowLevel, localB->TurnIndexMidLevel,
                        localB->TurnIndexHighLevel, rtb_y1, rtb_y2, rtb_y3,
                        &localB->y);

  /* Product: '<Root>/Product' */
  *rty_lctmcs16TarShtChnRPMInTrn = (int16_T)(localB->y * 100);
}

/* Model initialize function */
void LCTMC_vCalRefEngSpdMapInTrn_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter3Point' */
  LCETCS_s16Inter3Point_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
