/*
 * File: rt_sqrts16__s.h
 *
 * Code generated for Simulink model 'LCETCS_vCalCordRefRdFric'.
 *
 * Model version                  : 1.46
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:50:11 2015
 */

#ifndef SHARE_rt_sqrts16__s
#define SHARE_rt_sqrts16__s
#include "rtwtypes.h"

extern int16_T rt_sqrts16__s(int16_T u);

#endif

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
