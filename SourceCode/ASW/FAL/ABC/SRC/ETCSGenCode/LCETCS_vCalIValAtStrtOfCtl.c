/*
 * File: LCETCS_vCalIValAtStrtOfCtl.c
 *
 * Code generated for Simulink model 'LCETCS_vCalIValAtStrtOfCtl'.
 *
 * Model version                  : 1.213
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:54:07 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalIValAtStrtOfCtl.h"
#include "LCETCS_vCalIValAtStrtOfCtl_private.h"

/* Output and update for referenced model: 'LCETCS_vCalIValAtStrtOfCtl' */
void LCETCS_vCalIValAtStrtOfCtl(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT,
  const TypeETCSRefTrqStrCtlStruct *rtu_ETCS_REF_TRQ_STR_CTL, const
  TypeETCSCrdnTrqStruct *rtu_ETCS_CRDN_TRQ, int32_T
  *rty_lcetcss32IValAtStrtOfCtl)
{
  /* If: '<Root>/If' */
  if (rtu_ETCS_CTL_ACT->lcetcsu1CtlActRsgEdg) {
    /* Outputs for IfAction SubSystem: '<Root>/If Action Subsystem' incorporates:
     *  ActionPort: '<S1>/Action Port'
     */
    /* MinMax: '<S1>/MinMax' */
    if (rtu_ETCS_REF_TRQ_STR_CTL->lcetcss32EstTrqByVehAccBfrCtl <=
        rtu_ETCS_CRDN_TRQ->lcetcss32RealCrdnTrq) {
      *rty_lcetcss32IValAtStrtOfCtl =
        rtu_ETCS_REF_TRQ_STR_CTL->lcetcss32EstTrqByVehAccBfrCtl;
    } else {
      *rty_lcetcss32IValAtStrtOfCtl = rtu_ETCS_CRDN_TRQ->lcetcss32RealCrdnTrq;
    }

    /* End of Outputs for SubSystem: '<Root>/If Action Subsystem' */
  } else {
    /* Outputs for IfAction SubSystem: '<Root>/If Action Subsystem' incorporates:
     *  ActionPort: '<S1>/Action Port'
     */
    /* Outputs for IfAction SubSystem: '<Root>/If Action Subsystem1' incorporates:
     *  ActionPort: '<S2>/Action Port'
     */
    /* MinMax: '<S1>/MinMax' incorporates:
     *  Constant: '<S2>/Constant'
     */
    *rty_lcetcss32IValAtStrtOfCtl = 0;

    /* End of Outputs for SubSystem: '<Root>/If Action Subsystem1' */
    /* End of Outputs for SubSystem: '<Root>/If Action Subsystem' */
  }

  /* End of If: '<Root>/If' */
}

/* Model initialize function */
void LCETCS_vCalIValAtStrtOfCtl_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
