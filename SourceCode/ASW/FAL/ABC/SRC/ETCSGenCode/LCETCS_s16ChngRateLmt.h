/*
 * File: LCETCS_s16ChngRateLmt.h
 *
 * Code generated for Simulink model 'LCETCS_s16ChngRateLmt'.
 *
 * Model version                  : 1.313
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:58:56 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_s16ChngRateLmt_h_
#define RTW_HEADER_LCETCS_s16ChngRateLmt_h_
#ifndef LCETCS_s16ChngRateLmt_COMMON_INCLUDES_
# define LCETCS_s16ChngRateLmt_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_s16ChngRateLmt_COMMON_INCLUDES_ */

#include "LCETCS_s16ChngRateLmt_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_s16Inter2Point.h"

/* Block signals for model 'LCETCS_s16ChngRateLmt' */
typedef struct {
  int16_T lcetcss16TranStrtVal;        /* '<Root>/Chart2' */
} B_LCETCS_s16ChngRateLmt_c_T;

typedef struct {
  B_LCETCS_s16ChngRateLmt_c_T rtb;
} MdlrefDW_LCETCS_s16ChngRateLmt_T;

/* Model reference registration function */
extern void LCETCS_s16ChngRateLmt_initialize(void);
extern void LCETCS_s16ChngRateLmt_Init(int16_T *rty_lcetcss16ChngRateLmtdSig,
  int16_T *rty_lcetcss16TranTmeCnt, B_LCETCS_s16ChngRateLmt_c_T *localB);
extern void LCETCS_s16ChngRateLmt(int16_T rtu_lcetcss16TrgtSigVal, int16_T
  rtu_lcetcss16TrgtSigValOld, int16_T rtu_lcetcss16TranTme, int16_T
  rtu_lcetcss16ChngRateLmtdSigOld, int16_T rtu_lcetcss16TranTmeCntOld, int16_T
  *rty_lcetcss16ChngRateLmtdSig, int16_T *rty_lcetcss16TranTmeCnt,
  B_LCETCS_s16ChngRateLmt_c_T *localB);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_s16ChngRateLmt'
 * '<S1>'   : 'LCETCS_s16ChngRateLmt/Chart2'
 */
#endif                                 /* RTW_HEADER_LCETCS_s16ChngRateLmt_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
