/*
 * File: LCETCS_vCalTotalGearRatio.h
 *
 * Code generated for Simulink model 'LCETCS_vCalTotalGearRatio'.
 *
 * Model version                  : 1.90
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:06:57 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalTotalGearRatio_h_
#define RTW_HEADER_LCETCS_vCalTotalGearRatio_h_
#ifndef LCETCS_vCalTotalGearRatio_COMMON_INCLUDES_
# define LCETCS_vCalTotalGearRatio_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalTotalGearRatio_COMMON_INCLUDES_ */

#include "LCETCS_vCalTotalGearRatio_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_s16ChngRateLmt.h"

/* Block signals for model 'LCETCS_vCalTotalGearRatio' */
typedef struct {
  int16_T lcetcss16ChngRateLmtdSig;    /* '<Root>/LCETCS_s16ChngRateLmt' */
  int16_T lcetcss16TranTmeCnt;         /* '<Root>/LCETCS_s16ChngRateLmt' */
} B_LCETCS_vCalTotalGearRatio_c_T;

/* Block states (auto storage) for model 'LCETCS_vCalTotalGearRatio' */
typedef struct {
  int16_T UnitDelay2_DSTATE;           /* '<Root>/Unit Delay2' */
  MdlrefDW_LCETCS_s16ChngRateLmt_T LCETCS_s16ChngRateLmt_DWORK1;/* '<Root>/LCETCS_s16ChngRateLmt' */
} DW_LCETCS_vCalTotalGearRatio_f_T;

typedef struct {
  B_LCETCS_vCalTotalGearRatio_c_T rtb;
  DW_LCETCS_vCalTotalGearRatio_f_T rtdw;
} MdlrefDW_LCETCS_vCalTotalGearRatio_T;

/* Model reference registration function */
extern void LCETCS_vCalTotalGearRatio_initialize(void);
extern void LCETCS_vCalTotalGearRatio_Init(B_LCETCS_vCalTotalGearRatio_c_T
  *localB, DW_LCETCS_vCalTotalGearRatio_f_T *localDW);
extern void LCETCS_vCalTotalGearRatio(int16_T rtu_lcetcss16TotalGearRatioRaw,
  uint8_T rtu_lcetcsu8GrTransTime, int16_T *rty_lcetcss16TotalGearRatio,
  B_LCETCS_vCalTotalGearRatio_c_T *localB, DW_LCETCS_vCalTotalGearRatio_f_T
  *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalTotalGearRatio'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalTotalGearRatio_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
