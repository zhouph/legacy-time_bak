/*
 * File: LCTMC_vCalInHillRefTarGearMap.c
 *
 * Code generated for Simulink model 'LCTMC_vCalInHillRefTarGearMap'.
 *
 * Model version                  : 1.238
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:52:54 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCTMC_vCalInHillRefTarGearMap.h"
#include "LCTMC_vCalInHillRefTarGearMap_private.h"

/* Output and update for referenced model: 'LCTMC_vCalInHillRefTarGearMap' */
void LCTMC_vCalInHillRefTarGearMap(const TypeETCSSplitHillStruct
  *rtu_ETCS_SPLIT_HILL, uint8_T *rty_lctmcu8TarShtChnVSInHill)
{
  /* Switch: '<Root>/Switch' incorporates:
   *  Constant: '<Root>/Add Vehicle Speed in Flat'
   *  Constant: '<Root>/Add Vehicle Speed in Hill'
   *  Product: '<Root>/Product'
   */
  if (rtu_ETCS_SPLIT_HILL->lcetcsu1SplitHillDct) {
    *rty_lctmcu8TarShtChnVSInHill = (uint8_T)(((uint8_T)
      U8TMCpShftChgVehSpdInHill) << 3);
  } else {
    *rty_lctmcu8TarShtChnVSInHill = 0U;
  }

  /* End of Switch: '<Root>/Switch' */
}

/* Model initialize function */
void LCTMC_vCalInHillRefTarGearMap_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
