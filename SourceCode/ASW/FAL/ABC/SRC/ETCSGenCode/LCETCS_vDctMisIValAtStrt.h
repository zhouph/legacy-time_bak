/*
 * File: LCETCS_vDctMisIValAtStrt.h
 *
 * Code generated for Simulink model 'LCETCS_vDctMisIValAtStrt'.
 *
 * Model version                  : 1.246
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:09:35 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vDctMisIValAtStrt_h_
#define RTW_HEADER_LCETCS_vDctMisIValAtStrt_h_
#ifndef LCETCS_vDctMisIValAtStrt_COMMON_INCLUDES_
# define LCETCS_vDctMisIValAtStrt_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vDctMisIValAtStrt_COMMON_INCLUDES_ */

#include "LCETCS_vDctMisIValAtStrt_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vDctRecTrqActInBump.h"
#include "LCETCS_vCalIValMemryAtStrt.h"
#include "LCETCS_vCalCtrlTime.h"

/* Block signals for model 'LCETCS_vDctMisIValAtStrt' */
typedef struct {
  int32_T lcetcss32IValMeMryAtStrt;    /* '<Root>/LCETCS_vCalIValMemryAtStrt' */
} B_LCETCS_vDctMisIValAtStrt_c_T;

/* Block states (auto storage) for model 'LCETCS_vDctMisIValAtStrt' */
typedef struct {
  MdlrefDW_LCETCS_vCalCtrlTime_T LCETCS_vCalCtrlTime_DWORK1;/* '<Root>/LCETCS_vCalCtrlTime' */
  MdlrefDW_LCETCS_vCalIValMemryAtStrt_T LCETCS_vCalIValMemryAtStrt_DWORK1;/* '<Root>/LCETCS_vCalIValMemryAtStrt' */
  MdlrefDW_LCETCS_vDctRecTrqActInBump_T LCETCS_vDctRecTrqActInBump_DWORK1;/* '<Root>/LCETCS_vDctRecTrqActInBump' */
} DW_LCETCS_vDctMisIValAtStrt_f_T;

typedef struct {
  B_LCETCS_vDctMisIValAtStrt_c_T rtb;
  DW_LCETCS_vDctMisIValAtStrt_f_T rtdw;
} MdlrefDW_LCETCS_vDctMisIValAtStrt_T;

/* Model reference registration function */
extern void LCETCS_vDctMisIValAtStrt_initialize(void);
extern void LCETCS_vDctMisIValAtStrt_Init(boolean_T *rty_lcetcsu1IValRecTorqAct,
  B_LCETCS_vDctMisIValAtStrt_c_T *localB, DW_LCETCS_vDctMisIValAtStrt_f_T
  *localDW);
extern void LCETCS_vDctMisIValAtStrt(const TypeETCSCtlActStruct
  *rtu_ETCS_CTL_ACT, int32_T rtu_lcetcss32IValAtStrtOfCtl, const
  TypeETCSHighDctStruct *rtu_ETCS_HI_DCT, const TypeETCSCtlCmdStruct
  *rtu_ETCS_CTL_CMD_OLD, boolean_T *rty_lcetcsu1IValRecTorqAct, int32_T
  *rty_lcetcss32IValRecTorq, B_LCETCS_vDctMisIValAtStrt_c_T *localB,
  DW_LCETCS_vDctMisIValAtStrt_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vDctMisIValAtStrt'
 */
#endif                                 /* RTW_HEADER_LCETCS_vDctMisIValAtStrt_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
