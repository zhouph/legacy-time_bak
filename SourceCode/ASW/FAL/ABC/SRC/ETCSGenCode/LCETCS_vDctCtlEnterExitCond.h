/*
 * File: LCETCS_vDctCtlEnterExitCond.h
 *
 * Code generated for Simulink model 'LCETCS_vDctCtlEnterExitCond'.
 *
 * Model version                  : 1.185
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:09:56 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vDctCtlEnterExitCond_h_
#define RTW_HEADER_LCETCS_vDctCtlEnterExitCond_h_
#ifndef LCETCS_vDctCtlEnterExitCond_COMMON_INCLUDES_
# define LCETCS_vDctCtlEnterExitCond_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vDctCtlEnterExitCond_COMMON_INCLUDES_ */

#include "LCETCS_vDctCtlEnterExitCond_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vCntOvrWhlSpnTm.h"
#include "LCETCS_vCntOvrEngTrqCmdTm.h"
#include "LCETCS_vCntCtlEntrTmInLmtCnrg.h"
#include "LCETCS_vChkDriverIntention.h"
#include "LCETCS_vCalOvrWhlSpnTmTh.h"
#include "LCETCS_vCalCtlAct.h"

/* Block signals for model 'LCETCS_vDctCtlEnterExitCond' */
typedef struct {
  int16_T lcetcss16OvrWhlSpnTm;        /* '<Root>/LCETCS_vCntOvrWhlSpnTm' */
  int16_T lcetcss16OvrWhlSpnTmTh;      /* '<Root>/LCETCS_vCalOvrWhlSpnTmTh' */
  boolean_T lcetcsu1DriverAccelIntend; /* '<Root>/LCETCS_vChkDriverIntention' */
  boolean_T lcetcsu1DriverAccelHysIntend;/* '<Root>/LCETCS_vChkDriverIntention' */
  boolean_T lcetcsu1CtlAct;            /* '<Root>/LCETCS_vCalCtlAct' */
  boolean_T lcetcsu1CtlActRsgEdg;      /* '<Root>/LCETCS_vCalCtlAct' */
} B_LCETCS_vDctCtlEnterExitCond_c_T;

/* Block states (auto storage) for model 'LCETCS_vDctCtlEnterExitCond' */
typedef struct {
  MdlrefDW_LCETCS_vCntOvrWhlSpnTm_T LCETCS_vCntOvrWhlSpnTm_DWORK1;/* '<Root>/LCETCS_vCntOvrWhlSpnTm' */
  MdlrefDW_LCETCS_vCalOvrWhlSpnTmTh_T LCETCS_vCalOvrWhlSpnTmTh_DWORK1;/* '<Root>/LCETCS_vCalOvrWhlSpnTmTh' */
  MdlrefDW_LCETCS_vCntOvrEngTrqCmdTm_T LCETCS_vCntOvrEngTrqCmdTm_DWORK1;/* '<Root>/LCETCS_vCntOvrEngTrqCmdTm' */
  MdlrefDW_LCETCS_vCntCtlEntrTmInLmtCnrg_T LCETCS_vCntCtlEntrTmInLmtCnrg_DWORK1;/* '<Root>/LCETCS_vCntCtlEntrTmInLmtCnrg' */
  MdlrefDW_LCETCS_vCalCtlAct_T LCETCS_vCalCtlAct_DWORK1;/* '<Root>/LCETCS_vCalCtlAct' */
} DW_LCETCS_vDctCtlEnterExitCond_f_T;

typedef struct {
  B_LCETCS_vDctCtlEnterExitCond_c_T rtb;
  DW_LCETCS_vDctCtlEnterExitCond_f_T rtdw;
} MdlrefDW_LCETCS_vDctCtlEnterExitCond_T;

/* Model reference registration function */
extern void LCETCS_vDctCtlEnterExitCond_initialize(void);
extern void LCETCS_vDctCtlEnterExitCond_Init(B_LCETCS_vDctCtlEnterExitCond_c_T
  *localB, DW_LCETCS_vDctCtlEnterExitCond_f_T *localDW);
extern void LCETCS_vDctCtlEnterExitCond_Start(DW_LCETCS_vDctCtlEnterExitCond_f_T
  *localDW);
extern void LCETCS_vDctCtlEnterExitCond(const TypeETCSEngStallStruct
  *rtu_ETCS_ENG_STALL, const TypeETCSEngCmdStruct *rtu_ETCS_ENG_CMD_OLD, const
  TypeETCSEngStruct *rtu_ETCS_ENG_STRUCT, const TypeETCSWhlSpinStruct
  *rtu_ETCS_WHL_SPIN, const TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN, const
  TypeETCSAxlAccStruct *rtu_ETCS_AXL_ACC, const TypeETCSCtlActStruct
  *rtu_ETCS_CTL_ACT_OLD, const TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC, const
  TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const TypeETCSExtDctStruct
  *rtu_ETCS_EXT_DCT, const TypeETCSCtrlModeStruct *rtu_ETCS_CTRL_MODE,
  TypeETCSCtlActStruct *rty_ETCS_CTL_ACT, B_LCETCS_vDctCtlEnterExitCond_c_T
  *localB, DW_LCETCS_vDctCtlEnterExitCond_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vDctCtlEnterExitCond'
 */
#endif                                 /* RTW_HEADER_LCETCS_vDctCtlEnterExitCond_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
