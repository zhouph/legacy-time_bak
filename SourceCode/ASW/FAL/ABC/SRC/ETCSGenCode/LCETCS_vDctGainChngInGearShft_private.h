/*
 * File: LCETCS_vDctGainChngInGearShft_private.h
 *
 * Code generated for Simulink model 'LCETCS_vDctGainChngInGearShft'.
 *
 * Model version                  : 1.201
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:55:47 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vDctGainChngInGearShft_private_h_
#define RTW_HEADER_LCETCS_vDctGainChngInGearShft_private_h_
#include "rtwtypes.h"
#include "model_reference_types.h"

/* Includes for objects with custom storage classes. */
#include "../ETCSGenCodeHeader/ETCSHeader4Import.h"

/*
 * Generate compile time checks that imported macros for parameters
 * with storage class "ImportedDefine" are defined
 */
#ifndef U8ETCSCpGainTrnsTmAftrGearShft
#error The variable for the parameter "U8ETCSCpGainTrnsTmAftrGearShft" is not defined
#endif

/* Private macros used by the generated code to access rtModel */
#ifndef rtmIsFirstInitCond
# define rtmIsFirstInitCond()          ( *(LCETCS_vDctGainChngInGearShft_TimingBrdg->firstInitCond) )
#endif

/* Macros for accessing real-time model data structure */
#ifndef rtmGetT
# define rtmGetT()                     (*(LCETCS_vDctGainChngInGearShft_TimingBrdg->taskTime[0]))
#endif

extern const rtTimingBridge *LCETCS_vDctGainChngInGearShft_TimingBrdg;

#endif                                 /* RTW_HEADER_LCETCS_vDctGainChngInGearShft_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
