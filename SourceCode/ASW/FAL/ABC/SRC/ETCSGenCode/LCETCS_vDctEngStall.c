/*
 * File: LCETCS_vDctEngStall.c
 *
 * Code generated for Simulink model 'LCETCS_vDctEngStall'.
 *
 * Model version                  : 1.126
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:14:53 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctEngStall.h"
#include "LCETCS_vDctEngStall_private.h"

/* Initial conditions for referenced model: 'LCETCS_vDctEngStall' */
void LCETCS_vDctEngStall_Init(B_LCETCS_vDctEngStall_c_T *localB,
  DW_LCETCS_vDctEngStall_f_T *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctEngStall4Eng' */
  LCETCS_vDctEngStall4Eng_Init(&localB->lcetcsu1EngStallTemp,
    &(localDW->LCETCS_vDctEngStall4Eng_DWORK1.rtdw));
}

/* Disable for referenced model: 'LCETCS_vDctEngStall' */
void LCETCS_vDctEngStall_Disable(DW_LCETCS_vDctEngStall_f_T *localDW)
{
  /* Disable for ModelReference: '<Root>/LCETCS_vDctEngStall4Eng' */
  LCETCS_vDctEngStall4Eng_Disable(&(localDW->LCETCS_vDctEngStall4Eng_DWORK1.rtdw));
}

/* Start for referenced model: 'LCETCS_vDctEngStall' */
void LCETCS_vDctEngStall_Start(DW_LCETCS_vDctEngStall_f_T *localDW)
{
  /* Start for ModelReference: '<Root>/LCETCS_vDctEngStall4Eng' */
  LCETCS_vDctEngStall4Eng_Start(&(localDW->LCETCS_vDctEngStall4Eng_DWORK1.rtdw));
}

/* Output and update for referenced model: 'LCETCS_vDctEngStall' */
void LCETCS_vDctEngStall(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT_OLD, const
  TypeETCSSplitHillStruct *rtu_ETCS_SPLIT_HILL_OLD, int16_T rtu_lcetcss16VehSpd,
  const TypeETCSEngStruct *rtu_ETCS_ENG_STRUCT, const TypeETCSEngStallStruct
  *rtu_ETCS_ENG_STALL_OLD, TypeETCSEngStallStruct *rty_ETCS_ENG_STALL,
  B_LCETCS_vDctEngStall_c_T *localB, DW_LCETCS_vDctEngStall_f_T *localDW)
{
  /* local block i/o variables */
  int16_T rtb_lcetcss16EngStallThr;
  int16_T rtb_lcetcss16EngSpdSlope;
  boolean_T rtb_lcetcsu1EngStall;

  /* ModelReference: '<Root>/LCETCS_vDctEngStall4Eng' */
  LCETCS_vDctEngStall4Eng(rtu_ETCS_CTL_ACT_OLD, rtu_ETCS_SPLIT_HILL_OLD,
    rtu_lcetcss16VehSpd, rtu_ETCS_ENG_STRUCT, rtu_ETCS_ENG_STALL_OLD,
    &localB->lcetcsu1EngStallTemp, &rtb_lcetcss16EngStallThr,
    &localB->lcetcss8EngStallRiskIndex, &rtb_lcetcss16EngSpdSlope,
    &(localDW->LCETCS_vDctEngStall4Eng_DWORK1.rtb),
    &(localDW->LCETCS_vDctEngStall4Eng_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vDctEngStallEngExist' */
  LCETCS_vDctEngStallEngExist(localB->lcetcsu1EngStallTemp,
    &rtb_lcetcsu1EngStall);

  /* BusCreator: '<Root>/Bus Creator1' */
  rty_ETCS_ENG_STALL->lcetcsu1EngStall = rtb_lcetcsu1EngStall;
  rty_ETCS_ENG_STALL->lcetcsu1EngStallTemp = localB->lcetcsu1EngStallTemp;
  rty_ETCS_ENG_STALL->lcetcss16EngStallThr = rtb_lcetcss16EngStallThr;
  rty_ETCS_ENG_STALL->lcetcss8EngStallRiskIndex =
    localB->lcetcss8EngStallRiskIndex;
  rty_ETCS_ENG_STALL->lcetcss16EngSpdSlope = rtb_lcetcss16EngSpdSlope;
}

/* Model initialize function */
void LCETCS_vDctEngStall_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctEngStall4Eng' */
  LCETCS_vDctEngStall4Eng_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctEngStallEngExist' */
  LCETCS_vDctEngStallEngExist_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
