/*
 * File: LCETCS_vCntDepSnwHil.c
 *
 * Code generated for Simulink model 'LCETCS_vCntDepSnwHil'.
 *
 * Model version                  : 1.184
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:55:17 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCntDepSnwHil.h"
#include "LCETCS_vCntDepSnwHil_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCntDepSnwHil' */
void LCETCS_vCntDepSnwHil_Init(DW_LCETCS_vCntDepSnwHil_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = 0;
}

/* Output and update for referenced model: 'LCETCS_vCntDepSnwHil' */
void LCETCS_vCntDepSnwHil(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const
  TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC, const TypeETCSWhlSpinStruct
  *rtu_ETCS_WHL_SPIN, const TypeETCSGearStruct *rtu_ETCS_GEAR_STRUCT, const
  TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, int16_T rtu_lcetcss16VehSpd, int16_T
  *rty_lcetcss16DepSnwHilCnt, DW_LCETCS_vCntDepSnwHil_f_T *localDW)
{
  int32_T tmp;

  /* Chart: '<Root>/Chart3' incorporates:
   *  UnitDelay: '<Root>/Unit Delay2'
   */
  /* Gateway: Chart3 */
  /* During: Chart3 */
  /* Entry Internal: Chart3 */
  /* Transition: '<S1>:25' */
  /* comment */
  if ((rtu_lcetcss16VehSpd > (((uint8_T)U8ETCSCpNAlwAddTar4DepSnwSpd) << 3)) ||
      (rtu_ETCS_ESC_SIG->lcetcss16LatAccMsrdMani > ((uint8_T)U8ETCSCpAyAsp))) {
    /* Transition: '<S1>:26' */
    /* Transition: '<S1>:136' */
    tmp = localDW->UnitDelay2_DSTATE - 1;
    if (tmp < -32768) {
      tmp = -32768;
    }

    localDW->UnitDelay2_DSTATE = (int16_T)tmp;

    /* Transition: '<S1>:139' */
    /* Transition: '<S1>:108' */
    /* Transition: '<S1>:125' */
    /* Transition: '<S1>:119' */
    /* Transition: '<S1>:134' */
    /* Transition: '<S1>:133' */
  } else {
    /* Transition: '<S1>:93' */
    if (rtu_ETCS_CTL_ACT->lcetcsu1CtlAct == 1) {
      /* Transition: '<S1>:95' */
      /* Transition: '<S1>:97' */
      if (rtu_ETCS_VEH_ACC->lcetcss16VehAccByVehSpd < 0) {
        /* Transition: '<S1>:99' */
        /* Transition: '<S1>:101' */
        if (((rtu_ETCS_WHL_SPIN->lcetcss16WhlSpin >= ((int16_T)VREF_3_KPH)) &&
             (rtu_ETCS_GEAR_STRUCT->lcetcsu1GearShifting == 0)) &&
            (rtu_ETCS_ESC_SIG->lcetcsu1BrkPdlPrsed == 0)) {
          /* Transition: '<S1>:103' */
          /* Transition: '<S1>:105' */
          tmp = localDW->UnitDelay2_DSTATE + 1;
          if (tmp > 32767) {
            tmp = 32767;
          }

          localDW->UnitDelay2_DSTATE = (int16_T)tmp;

          /* Transition: '<S1>:108' */
          /* Transition: '<S1>:125' */
          /* Transition: '<S1>:119' */
          /* Transition: '<S1>:134' */
          /* Transition: '<S1>:133' */
        } else {
          /* Transition: '<S1>:107' */
          /* Transition: '<S1>:125' */
          /* Transition: '<S1>:119' */
          /* Transition: '<S1>:134' */
          /* Transition: '<S1>:133' */
        }
      } else {
        /* Transition: '<S1>:110' */
        if (rtu_ETCS_VEH_ACC->lcetcss16VehAccByVehSpd > 0) {
          /* Transition: '<S1>:114' */
          /* Transition: '<S1>:116' */
          tmp = localDW->UnitDelay2_DSTATE - 1;
          if (tmp < -32768) {
            tmp = -32768;
          }

          localDW->UnitDelay2_DSTATE = (int16_T)tmp;

          /* Transition: '<S1>:119' */
          /* Transition: '<S1>:134' */
          /* Transition: '<S1>:133' */
        } else {
          /* Transition: '<S1>:118' */
          /* Transition: '<S1>:134' */
          /* Transition: '<S1>:133' */
        }
      }
    } else {
      /* Transition: '<S1>:122' */
      if (rtu_ETCS_VEH_ACC->lcetcss16VehAccByVehSpd > 0) {
        /* Transition: '<S1>:128' */
        /* Transition: '<S1>:130' */
        tmp = localDW->UnitDelay2_DSTATE - 1;
        if (tmp < -32768) {
          tmp = -32768;
        }

        localDW->UnitDelay2_DSTATE = (int16_T)tmp;

        /* Transition: '<S1>:133' */
      } else {
        /* Transition: '<S1>:132' */
      }
    }
  }

  /* End of Chart: '<Root>/Chart3' */

  /* Saturate: '<Root>/Saturation' */
  /* Transition: '<S1>:142' */
  if (localDW->UnitDelay2_DSTATE > ((uint8_T)U8ETCSCpDetDepSnwHilCnt)) {
    *rty_lcetcss16DepSnwHilCnt = ((uint8_T)U8ETCSCpDetDepSnwHilCnt);
  } else if (localDW->UnitDelay2_DSTATE < 0) {
    *rty_lcetcss16DepSnwHilCnt = 0;
  } else {
    *rty_lcetcss16DepSnwHilCnt = localDW->UnitDelay2_DSTATE;
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Update for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = *rty_lcetcss16DepSnwHilCnt;
}

/* Model initialize function */
void LCETCS_vCntDepSnwHil_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
