/*
 * File: LCETCS_vCntOvrWhlSpnTm_private.h
 *
 * Code generated for Simulink model 'LCETCS_vCntOvrWhlSpnTm'.
 *
 * Model version                  : 1.158
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:05:17 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCntOvrWhlSpnTm_private_h_
#define RTW_HEADER_LCETCS_vCntOvrWhlSpnTm_private_h_
#include "rtwtypes.h"
#include "model_reference_types.h"

/* Includes for objects with custom storage classes. */
#include "../ETCSGenCodeHeader/ETCSHeader4Import.h"

/*
 * Generate compile time checks that imported macros for parameters
 * with storage class "ImportedDefine" are defined
 */
#ifndef TCSDpCtrlModeDisable
#error The variable for the parameter "TCSDpCtrlModeDisable" is not defined
#endif

extern const int16_T rtCP_pooled_ZjYULP70HcKf;
extern const int16_T rtCP_pooled_xANZMqcfV9JD;

#define rtCP_Constant_Value            rtCP_pooled_ZjYULP70HcKf  /* Computed Parameter: Constant_Value
                                                                  * Referenced by: '<Root>/Constant'
                                                                  */
#define rtCP_Constant1_Value           rtCP_pooled_xANZMqcfV9JD  /* Computed Parameter: Constant1_Value
                                                                  * Referenced by: '<Root>/Constant1'
                                                                  */
#endif                                 /* RTW_HEADER_LCETCS_vCntOvrWhlSpnTm_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
