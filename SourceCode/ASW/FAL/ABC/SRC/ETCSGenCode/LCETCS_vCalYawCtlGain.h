/*
 * File: LCETCS_vCalYawCtlGain.h
 *
 * Code generated for Simulink model 'LCETCS_vCalYawCtlGain'.
 *
 * Model version                  : 1.219
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:08:39 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalYawCtlGain_h_
#define RTW_HEADER_LCETCS_vCalYawCtlGain_h_
#ifndef LCETCS_vCalYawCtlGain_COMMON_INCLUDES_
# define LCETCS_vCalYawCtlGain_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalYawCtlGain_COMMON_INCLUDES_ */

#include "LCETCS_vCalYawCtlGain_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vCalDelYawTrend.h"
#include "LCETCS_vCalDelYawGain.h"
#include "LCETCS_vCalDelYawDivrgGain.h"
#include "LCETCS_vCalDelYawCnvrgGain.h"
#include "LCETCS_vCalBasDelYawCnvrgGain.h"

/* Block states (auto storage) for model 'LCETCS_vCalYawCtlGain' */
typedef struct {
  MdlrefDW_LCETCS_vCalBasDelYawCnvrgGain_T LCETCS_vCalBasDelYawCnvrgGain_DWORK1;/* '<Root>/LCETCS_vCalBasDelYawCnvrgGain' */
  MdlrefDW_LCETCS_vCalDelYawCnvrgGain_T LCETCS_vCalDelYawCnvrgGain_DWORK1;/* '<Root>/LCETCS_vCalDelYawCnvrgGain' */
  MdlrefDW_LCETCS_vCalDelYawDivrgGain_T LCETCS_vCalDelYawDivrgGain_DWORK1;/* '<Root>/LCETCS_vCalDelYawDivrgGain' */
  MdlrefDW_LCETCS_vCalDelYawTrend_T LCETCS_vCalDelYawTrend_DWORK1;/* '<Root>/LCETCS_vCalDelYawTrend' */
} DW_LCETCS_vCalYawCtlGain_f_T;

typedef struct {
  DW_LCETCS_vCalYawCtlGain_f_T rtdw;
} MdlrefDW_LCETCS_vCalYawCtlGain_T;

/* Model reference registration function */
extern void LCETCS_vCalYawCtlGain_initialize(void);
extern void LCETCS_vCalYawCtlGain_Init(boolean_T *rty_lcetcsu1DelYawDivrg,
  DW_LCETCS_vCalYawCtlGain_f_T *localDW);
extern void LCETCS_vCalYawCtlGain_Start(DW_LCETCS_vCalYawCtlGain_f_T *localDW);
extern void LCETCS_vCalYawCtlGain(const TypeETCSCtlCmdStruct
  *rtu_ETCS_CTL_CMD_OLD, int16_T rtu_lcetcss16VehSpd, const TypeETCSEscSigStruct
  *rtu_ETCS_ESC_SIG, const TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN, int16_T
  *rty_lcetcss16DelYawGain, boolean_T *rty_lcetcsu1DelYawDivrg, int16_T
  *rty_lcetcss16DelYawDivrgCnt, int16_T *rty_lcetcss16DelYawDivrgGain, int16_T
  *rty_lcetcss16DelYawCnvrgGain, DW_LCETCS_vCalYawCtlGain_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalYawCtlGain'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalYawCtlGain_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
