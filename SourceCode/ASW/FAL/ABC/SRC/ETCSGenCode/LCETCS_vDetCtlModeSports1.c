/*
 * File: LCETCS_vDetCtlModeSports1.c
 *
 * Code generated for Simulink model 'LCETCS_vDetCtlModeSports1'.
 *
 * Model version                  : 1.201
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:50:56 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDetCtlModeSports1.h"
#include "LCETCS_vDetCtlModeSports1_private.h"

const rtTimingBridge *LCETCS_vDetCtlModeSports1_TimingBrdg;

/* Initial conditions for referenced model: 'LCETCS_vDetCtlModeSports1' */
void LCETCS_vDetCtlModeSports1_Init(TypeETCSCtrlModeStruct *rty_ETCS_CTRL_MODE)
{
  /* InitializeConditions for BusCreator: '<Root>/Bus Creator' incorporates:
   *  InitializeConditions for Chart: '<Root>/Chart1'
   */
  if (rtmIsFirstInitCond()) {
    rty_ETCS_CTRL_MODE->lcetcsu8EngCtrlMode = 0U;
    rty_ETCS_CTRL_MODE->lcetcsu8BrkCtrlMode = 0U;
  }

  /* End of InitializeConditions for BusCreator: '<Root>/Bus Creator' */
}

/* Output and update for referenced model: 'LCETCS_vDetCtlModeSports1' */
void LCETCS_vDetCtlModeSports1(const TypeETCSExtDctStruct *rtu_ETCS_EXT_DCT,
  TypeETCSCtrlModeStruct *rty_ETCS_CTRL_MODE)
{
  uint8_T lcetcsu8EngCtrlMode;
  uint8_T lcetcsu8BrkCtrlMode;

  /* Chart: '<Root>/Chart1' */
  /* Gateway: Chart1 */
  /* During: Chart1 */
  /* Entry Internal: Chart1 */
  /* Transition: '<S1>:11' */
  /* comment */
  if (rtu_ETCS_EXT_DCT->lcetcsu1EscDisabledBySW == 1) {
    /* Transition: '<S1>:10' */
    /* Transition: '<S1>:9' */
    lcetcsu8EngCtrlMode = ((uint8_T)TCSDpCtrlModeDisable);
    lcetcsu8BrkCtrlMode = ((uint8_T)TCSDpCtrlModeDisable);

    /* Transition: '<S1>:23' */
    /* Transition: '<S1>:24' */
  } else {
    /* Transition: '<S1>:7' */
    if (rtu_ETCS_EXT_DCT->lcetcsu1TcsDisabledBySW == 1) {
      /* Transition: '<S1>:19' */
      /* Transition: '<S1>:21' */
      lcetcsu8EngCtrlMode = ((uint8_T)TCSDpCtrlModeDisable);
      lcetcsu8BrkCtrlMode = ((uint8_T)TCSDpCtrlModeSports1);

      /* Transition: '<S1>:24' */
    } else {
      /* Transition: '<S1>:22' */
      lcetcsu8EngCtrlMode = ((uint8_T)TCSDpCtrlModeNormal);
      lcetcsu8BrkCtrlMode = ((uint8_T)TCSDpCtrlModeNormal);
    }
  }

  /* End of Chart: '<Root>/Chart1' */

  /* BusCreator: '<Root>/Bus Creator' */
  /* Transition: '<S1>:26' */
  rty_ETCS_CTRL_MODE->lcetcsu8EngCtrlMode = lcetcsu8EngCtrlMode;
  rty_ETCS_CTRL_MODE->lcetcsu8BrkCtrlMode = lcetcsu8BrkCtrlMode;
}

/* Model initialize function */
void LCETCS_vDetCtlModeSports1_initialize(const rtTimingBridge *timingBridge)
{
  /* Registration code */
  LCETCS_vDetCtlModeSports1_TimingBrdg = timingBridge;
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
