/*
 * File: LCETCS_vCalSpltBsIGain.c
 *
 * Code generated for Simulink model 'LCETCS_vCalSpltBsIGain'.
 *
 * Model version                  : 1.167
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:01:53 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalSpltBsIGain.h"
#include "LCETCS_vCalSpltBsIGain_private.h"

/* Start for referenced model: 'LCETCS_vCalSpltBsIGain' */
void LCETCS_vCalSpltBsIGain_Start(B_LCETCS_vCalSpltBsIGain_c_T *localB)
{
  /* Start for IfAction SubSystem: '<Root>/CalPGainWhnCtlOn' */
  /* Start for IfAction SubSystem: '<S2>/CalPgainWhnCtlErrNeg' */
  /* Start for Constant: '<S3>/Pgain for 5th gear ratio on split-mu' */
  localB->y1_j = ((uint8_T)U8ETCSCpNegErrIgainSplt_5);

  /* Start for Constant: '<S3>/Pgain for 4th gear ratio on split-mu' */
  localB->y2_c = ((uint8_T)U8ETCSCpNegErrIgainSplt_4);

  /* Start for Constant: '<S3>/Pgain for 3rd gear ratio on split-mu' */
  localB->y3_b = ((uint8_T)U8ETCSCpNegErrIgainSplt_3);

  /* Start for Constant: '<S3>/Pgain for 2nd gear ratio on split-mu' */
  localB->y4_m = ((uint8_T)U8ETCSCpNegErrIgainSplt_2);

  /* Start for Constant: '<S3>/Pgain for 1st gear ratio on split-mu' */
  localB->y5_i = ((uint8_T)U8ETCSCpNegErrIgainSplt_1);

  /* End of Start for SubSystem: '<S2>/CalPgainWhnCtlErrNeg' */

  /* InitializeConditions for IfAction SubSystem: '<S2>/CalPgainWhnCtlErrNeg' */

  /* InitializeConditions for ModelReference: '<S3>/LCETCS_s16Inter5Point' */
  LCETCS_s16Inter5Point_Init(&localB->lcetcss16SpltBsIgain_b);

  /* End of InitializeConditions for SubSystem: '<S2>/CalPgainWhnCtlErrNeg' */

  /* Start for IfAction SubSystem: '<S2>/CalPgainWhnCtlErrPos' */
  /* Start for Constant: '<S4>/Pgain for 5th gear ratio on split-mu' */
  localB->y1 = ((uint8_T)U8ETCSCpPosErrIgainSplt_5);

  /* Start for Constant: '<S4>/Pgain for 4th gear ratio on split-mu' */
  localB->y2 = ((uint8_T)U8ETCSCpPosErrIgainSplt_4);

  /* Start for Constant: '<S4>/Pgain for 3rd gear ratio on split-mu' */
  localB->y3 = ((uint8_T)U8ETCSCpPosErrIgainSplt_3);

  /* Start for Constant: '<S4>/Pgain for 2nd gear ratio on split-mu' */
  localB->y4 = ((uint8_T)U8ETCSCpPosErrIgainSplt_2);

  /* Start for Constant: '<S4>/Pgain for 1st gear ratio on split-mu' */
  localB->y5 = ((uint8_T)U8ETCSCpPosErrIgainSplt_1);

  /* End of Start for SubSystem: '<S2>/CalPgainWhnCtlErrPos' */

  /* InitializeConditions for IfAction SubSystem: '<S2>/CalPgainWhnCtlErrPos' */

  /* InitializeConditions for ModelReference: '<S4>/LCETCS_s16Inter5Point' */
  LCETCS_s16Inter5Point_Init(&localB->lcetcss16SpltBsIgain);

  /* End of InitializeConditions for SubSystem: '<S2>/CalPgainWhnCtlErrPos' */

  /* End of Start for SubSystem: '<Root>/CalPGainWhnCtlOn' */
}

/* Output and update for referenced model: 'LCETCS_vCalSpltBsIGain' */
void LCETCS_vCalSpltBsIGain(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const
  TypeETCSDrvMdlStruct *rtu_ETCS_DRV_MDL, const TypeETCSCtlErrStruct
  *rtu_ETCS_CTL_ERR, int16_T *rty_lcetcss16SpltBsIgain,
  B_LCETCS_vCalSpltBsIGain_c_T *localB)
{
  /* If: '<Root>/If' incorporates:
   *  Constant: '<S1>/Constant'
   *  If: '<S2>/If'
   */
  if (!rtu_ETCS_CTL_ACT->lcetcsu1CtlAct) {
    /* Outputs for IfAction SubSystem: '<Root>/CalPGainWhnCtlOff' incorporates:
     *  ActionPort: '<S1>/Action Port'
     */
    *rty_lcetcss16SpltBsIgain = 0;

    /* End of Outputs for SubSystem: '<Root>/CalPGainWhnCtlOff' */
  } else {
    /* Outputs for IfAction SubSystem: '<Root>/CalPGainWhnCtlOn' incorporates:
     *  ActionPort: '<S2>/Action Port'
     */
    if (!rtu_ETCS_CTL_ERR->lcetcsu1CtlErrPos) {
      /* Outputs for IfAction SubSystem: '<S2>/CalPgainWhnCtlErrNeg' incorporates:
       *  ActionPort: '<S3>/Action Port'
       */
      localB->y1_j = ((uint8_T)U8ETCSCpNegErrIgainSplt_5);
      localB->y2_c = ((uint8_T)U8ETCSCpNegErrIgainSplt_4);
      localB->y3_b = ((uint8_T)U8ETCSCpNegErrIgainSplt_3);
      localB->y4_m = ((uint8_T)U8ETCSCpNegErrIgainSplt_2);
      localB->y5_i = ((uint8_T)U8ETCSCpNegErrIgainSplt_1);

      /* ModelReference: '<S3>/LCETCS_s16Inter5Point' */
      LCETCS_s16Inter5Point(rtu_ETCS_DRV_MDL->lcetcss16TotalGearRatio, ((int16_T)
        S16ETCSCpTotalGearRatio_5), ((int16_T)S16ETCSCpTotalGearRatio_4),
                            ((int16_T)S16ETCSCpTotalGearRatio_3), ((int16_T)
        S16ETCSCpTotalGearRatio_2), ((int16_T)S16ETCSCpTotalGearRatio_1),
                            localB->y1_j, localB->y2_c, localB->y3_b,
                            localB->y4_m, localB->y5_i,
                            &localB->lcetcss16SpltBsIgain_b);
      *rty_lcetcss16SpltBsIgain = localB->lcetcss16SpltBsIgain_b;

      /* End of Outputs for SubSystem: '<S2>/CalPgainWhnCtlErrNeg' */
    } else {
      /* Outputs for IfAction SubSystem: '<S2>/CalPgainWhnCtlErrPos' incorporates:
       *  ActionPort: '<S4>/Action Port'
       */
      localB->y1 = ((uint8_T)U8ETCSCpPosErrIgainSplt_5);
      localB->y2 = ((uint8_T)U8ETCSCpPosErrIgainSplt_4);
      localB->y3 = ((uint8_T)U8ETCSCpPosErrIgainSplt_3);
      localB->y4 = ((uint8_T)U8ETCSCpPosErrIgainSplt_2);
      localB->y5 = ((uint8_T)U8ETCSCpPosErrIgainSplt_1);

      /* ModelReference: '<S4>/LCETCS_s16Inter5Point' */
      LCETCS_s16Inter5Point(rtu_ETCS_DRV_MDL->lcetcss16TotalGearRatio, ((int16_T)
        S16ETCSCpTotalGearRatio_5), ((int16_T)S16ETCSCpTotalGearRatio_4),
                            ((int16_T)S16ETCSCpTotalGearRatio_3), ((int16_T)
        S16ETCSCpTotalGearRatio_2), ((int16_T)S16ETCSCpTotalGearRatio_1),
                            localB->y1, localB->y2, localB->y3, localB->y4,
                            localB->y5, &localB->lcetcss16SpltBsIgain);
      *rty_lcetcss16SpltBsIgain = localB->lcetcss16SpltBsIgain;

      /* End of Outputs for SubSystem: '<S2>/CalPgainWhnCtlErrPos' */
    }

    /* End of Outputs for SubSystem: '<Root>/CalPGainWhnCtlOn' */
  }

  /* End of If: '<Root>/If' */
}

/* Model initialize function */
void LCETCS_vCalSpltBsIGain_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<S3>/LCETCS_s16Inter5Point' */
  LCETCS_s16Inter5Point_initialize();

  /* Model Initialize fcn for ModelReference Block: '<S4>/LCETCS_s16Inter5Point' */
  LCETCS_s16Inter5Point_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
