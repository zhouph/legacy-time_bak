/*
 * File: LCETCS_vCalFnlGradient.c
 *
 * Code generated for Simulink model 'LCETCS_vCalFnlGradient'.
 *
 * Model version                  : 1.309
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:10:35 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalFnlGradient.h"
#include "LCETCS_vCalFnlGradient_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalFnlGradient' */
void LCETCS_vCalFnlGradient_Init(B_LCETCS_vCalFnlGradient_c_T *localB,
  DW_LCETCS_vCalFnlGradient_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = 0;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localB->lcetcss16ChngRateLmtdSig = 0;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay' */
  localB->lcetcss16TranTmeCnt = 0;

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16ChngRateLmt' */
  LCETCS_s16ChngRateLmt_Init(&localB->lcetcss16ChngRateLmtdSig,
    &localB->lcetcss16TranTmeCnt, &(localDW->LCETCS_s16ChngRateLmt_DWORK1.rtb));
}

/* Start for referenced model: 'LCETCS_vCalFnlGradient' */
void LCETCS_vCalFnlGradient_Start(B_LCETCS_vCalFnlGradient_c_T *localB)
{
  /* Start for Constant: '<Root>/Constant2' */
  localB->lcetcss16TranTme = ((uint8_T)U8ETCSCpGrdTransTme);
}

/* Output and update for referenced model: 'LCETCS_vCalFnlGradient' */
void LCETCS_vCalFnlGradient(boolean_T rtu_lcetcsu1GradientTrstSet, int16_T
  rtu_lcetcss16AbsLongAccDiff, int16_T *rty_lcetcss16GradientOfHill,
  B_LCETCS_vCalFnlGradient_c_T *localB, DW_LCETCS_vCalFnlGradient_f_T *localDW)
{
  /* local block i/o variables */
  int16_T rtb_lcetcss16TranTmeCntOld;
  int16_T rtb_lcetcss16GradientOfHill_n;
  int16_T rtb_lcetcss16TrgtSigValOld;
  int16_T rtb_lcetcss16ChngRateLmtdSigOld;

  /* Chart: '<Root>/Chart' */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:9' */
  /* comment */
  if (rtu_lcetcsu1GradientTrstSet == 1) {
    /* Transition: '<S1>:171' */
    /* Transition: '<S1>:169' */
    rtb_lcetcss16GradientOfHill_n = rtu_lcetcss16AbsLongAccDiff;

    /* Transition: '<S1>:195' */
  } else {
    /* Transition: '<S1>:194' */
    rtb_lcetcss16GradientOfHill_n = 0;
  }

  /* End of Chart: '<Root>/Chart' */

  /* Constant: '<Root>/Constant2' */
  /* Transition: '<S1>:196' */
  localB->lcetcss16TranTme = ((uint8_T)U8ETCSCpGrdTransTme);

  /* UnitDelay: '<Root>/Unit Delay2' */
  rtb_lcetcss16TrgtSigValOld = localDW->UnitDelay2_DSTATE;

  /* UnitDelay: '<Root>/Unit Delay1' */
  rtb_lcetcss16ChngRateLmtdSigOld = localB->lcetcss16ChngRateLmtdSig;

  /* UnitDelay: '<Root>/Unit Delay' */
  rtb_lcetcss16TranTmeCntOld = localB->lcetcss16TranTmeCnt;

  /* ModelReference: '<Root>/LCETCS_s16ChngRateLmt' */
  LCETCS_s16ChngRateLmt(rtb_lcetcss16GradientOfHill_n,
                        rtb_lcetcss16TrgtSigValOld, localB->lcetcss16TranTme,
                        rtb_lcetcss16ChngRateLmtdSigOld,
                        rtb_lcetcss16TranTmeCntOld,
                        &localB->lcetcss16ChngRateLmtdSig,
                        &localB->lcetcss16TranTmeCnt,
                        &(localDW->LCETCS_s16ChngRateLmt_DWORK1.rtb));

  /* Saturate: '<Root>/Saturation' */
  if (localB->lcetcss16ChngRateLmtdSig > 2000) {
    *rty_lcetcss16GradientOfHill = 2000;
  } else if (localB->lcetcss16ChngRateLmtdSig < 0) {
    *rty_lcetcss16GradientOfHill = 0;
  } else {
    *rty_lcetcss16GradientOfHill = localB->lcetcss16ChngRateLmtdSig;
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Update for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = rtb_lcetcss16GradientOfHill_n;
}

/* Model initialize function */
void LCETCS_vCalFnlGradient_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16ChngRateLmt' */
  LCETCS_s16ChngRateLmt_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
