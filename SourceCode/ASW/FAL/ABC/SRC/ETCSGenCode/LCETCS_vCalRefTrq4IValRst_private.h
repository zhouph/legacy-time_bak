/*
 * File: LCETCS_vCalRefTrq4IValRst_private.h
 *
 * Code generated for Simulink model 'LCETCS_vCalRefTrq4IValRst'.
 *
 * Model version                  : 1.205
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:13:11 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalRefTrq4IValRst_private_h_
#define RTW_HEADER_LCETCS_vCalRefTrq4IValRst_private_h_
#include "rtwtypes.h"
#include "model_reference_types.h"

/* Includes for objects with custom storage classes. */
#include "../ETCSGenCodeHeader/ETCSHeader4Import.h"

/*
 * Generate compile time checks that imported macros for parameters
 * with storage class "ImportedDefine" are defined
 */
#ifndef S16ETCSCpCdrnTrqAtAspSpd_1
#error The variable for the parameter "S16ETCSCpCdrnTrqAtAspSpd_1" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtAspSpd_2
#error The variable for the parameter "S16ETCSCpCdrnTrqAtAspSpd_2" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtAspSpd_3
#error The variable for the parameter "S16ETCSCpCdrnTrqAtAspSpd_3" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtAspSpd_4
#error The variable for the parameter "S16ETCSCpCdrnTrqAtAspSpd_4" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtAspSpd_5
#error The variable for the parameter "S16ETCSCpCdrnTrqAtAspSpd_5" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtAsphalt
#error The variable for the parameter "S16ETCSCpCdrnTrqAtAsphalt" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtIce
#error The variable for the parameter "S16ETCSCpCdrnTrqAtIce" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtIceSpd_1
#error The variable for the parameter "S16ETCSCpCdrnTrqAtIceSpd_1" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtIceSpd_2
#error The variable for the parameter "S16ETCSCpCdrnTrqAtIceSpd_2" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtIceSpd_3
#error The variable for the parameter "S16ETCSCpCdrnTrqAtIceSpd_3" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtIceSpd_4
#error The variable for the parameter "S16ETCSCpCdrnTrqAtIceSpd_4" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtIceSpd_5
#error The variable for the parameter "S16ETCSCpCdrnTrqAtIceSpd_5" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtSnow
#error The variable for the parameter "S16ETCSCpCdrnTrqAtSnow" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtSnowSpd_1
#error The variable for the parameter "S16ETCSCpCdrnTrqAtSnowSpd_1" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtSnowSpd_2
#error The variable for the parameter "S16ETCSCpCdrnTrqAtSnowSpd_2" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtSnowSpd_3
#error The variable for the parameter "S16ETCSCpCdrnTrqAtSnowSpd_3" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtSnowSpd_4
#error The variable for the parameter "S16ETCSCpCdrnTrqAtSnowSpd_4" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtSnowSpd_5
#error The variable for the parameter "S16ETCSCpCdrnTrqAtSnowSpd_5" is not defined
#endif

#ifndef S16ETCSCpPreHighRdDctCplCnt
#error The variable for the parameter "S16ETCSCpPreHighRdDctCplCnt" is not defined
#endif

#ifndef S16ETCSCpSpd_1
#error The variable for the parameter "S16ETCSCpSpd_1" is not defined
#endif

#ifndef S16ETCSCpSpd_2
#error The variable for the parameter "S16ETCSCpSpd_2" is not defined
#endif

#ifndef S16ETCSCpSpd_3
#error The variable for the parameter "S16ETCSCpSpd_3" is not defined
#endif

#ifndef S16ETCSCpSpd_4
#error The variable for the parameter "S16ETCSCpSpd_4" is not defined
#endif

#ifndef S16ETCSCpSpd_5
#error The variable for the parameter "S16ETCSCpSpd_5" is not defined
#endif

#ifndef U8ETCSCpAxAtAsphalt
#error The variable for the parameter "U8ETCSCpAxAtAsphalt" is not defined
#endif

#ifndef U8ETCSCpAxAtIce
#error The variable for the parameter "U8ETCSCpAxAtIce" is not defined
#endif

#ifndef U8ETCSCpAxAtSnow
#error The variable for the parameter "U8ETCSCpAxAtSnow" is not defined
#endif

#ifndef U8ETCSCpAxSenAvail
#error The variable for the parameter "U8ETCSCpAxSenAvail" is not defined
#endif

#ifndef U8ETCSCpFtr4JmpDnMax
#error The variable for the parameter "U8ETCSCpFtr4JmpDnMax" is not defined
#endif

#ifndef U8ETCSCpFtr4JmpDnMin
#error The variable for the parameter "U8ETCSCpFtr4JmpDnMin" is not defined
#endif

#ifndef U8ETCSCpFtr4JmpUpMax
#error The variable for the parameter "U8ETCSCpFtr4JmpUpMax" is not defined
#endif

#ifndef U8ETCSCpFtr4JmpUpMin
#error The variable for the parameter "U8ETCSCpFtr4JmpUpMin" is not defined
#endif

#ifndef U8ETCSCpFtr4RefTrqAt2ndCylStrt
#error The variable for the parameter "U8ETCSCpFtr4RefTrqAt2ndCylStrt" is not defined
#endif

#ifndef U8ETCSCpRefTrqNvldTm
#error The variable for the parameter "U8ETCSCpRefTrqNvldTm" is not defined
#endif

#ifndef U8ETCSCpRefTrqVldTm
#error The variable for the parameter "U8ETCSCpRefTrqVldTm" is not defined
#endif
#endif                                 /* RTW_HEADER_LCETCS_vCalRefTrq4IValRst_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
