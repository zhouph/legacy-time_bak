/*
 * File: LCETCS_vCalAxlAcc.c
 *
 * Code generated for Simulink model 'LCETCS_vCalAxlAcc'.
 *
 * Model version                  : 1.68
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:47:03 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalAxlAcc.h"
#include "LCETCS_vCalAxlAcc_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalAxlAcc' */
void LCETCS_vCalAxlAcc_Init(DW_LCETCS_vCalAxlAcc_f_T *localDW)
{
  int32_T i;

  /* InitializeConditions for Delay: '<Root>/Delay' */
  localDW->CircBufIdx = 0U;
  for (i = 0; i < 9; i++) {
    /* InitializeConditions for Delay: '<Root>/Delay' */
    localDW->Delay_DSTATE[i] = 0;

    /* InitializeConditions for Delay: '<Root>/Delay1' */
    localDW->Delay1_DSTATE[i] = 0;
  }

  /* InitializeConditions for Delay: '<Root>/Delay1' */
  localDW->CircBufIdx_k = 0U;
}

/* Output and update for referenced model: 'LCETCS_vCalAxlAcc' */
void LCETCS_vCalAxlAcc(const TypeETCSWhlStruct *rtu_ETCS_FL, const
  TypeETCSWhlStruct *rtu_ETCS_FR, const TypeETCSWhlStruct *rtu_ETCS_RL, const
  TypeETCSWhlStruct *rtu_ETCS_RR, const TypeETCSExtDctStruct *rtu_ETCS_EXT_DCT,
  TypeETCSAxlAccStruct *rty_ETCS_AXL_ACC, DW_LCETCS_vCalAxlAcc_f_T *localDW)
{
  int16_T rtb_lcetcss16RrAxlAcc;
  int16_T rtb_Divide2;
  int16_T rtb_lcetcss16FrtAxlAcc;
  int16_T rtb_Divide1;

  /* Switch: '<Root>/Switch' */
  if (rtu_ETCS_EXT_DCT->lcetcsu1AutoTrans) {
    rtb_Divide1 = rtu_ETCS_FL->lcetcss16WhlSpdCrt;
  } else {
    rtb_Divide1 = rtu_ETCS_FL->lcetcss16MovigAvgWhlSpd;
  }

  /* End of Switch: '<Root>/Switch' */

  /* Switch: '<Root>/Switch1' */
  if (rtu_ETCS_EXT_DCT->lcetcsu1AutoTrans) {
    rtb_lcetcss16FrtAxlAcc = rtu_ETCS_FR->lcetcss16WhlSpdCrt;
  } else {
    rtb_lcetcss16FrtAxlAcc = rtu_ETCS_FR->lcetcss16MovigAvgWhlSpd;
  }

  /* End of Switch: '<Root>/Switch1' */

  /* Product: '<Root>/Divide2' incorporates:
   *  Sum: '<Root>/Add'
   */
  rtb_Divide2 = (int16_T)((rtb_Divide1 + rtb_lcetcss16FrtAxlAcc) / 2);

  /* Switch: '<Root>/Switch2' */
  if (rtu_ETCS_EXT_DCT->lcetcsu1AutoTrans) {
    rtb_Divide1 = rtu_ETCS_RL->lcetcss16WhlSpdCrt;
  } else {
    rtb_Divide1 = rtu_ETCS_RL->lcetcss16MovigAvgWhlSpd;
  }

  /* End of Switch: '<Root>/Switch2' */

  /* Switch: '<Root>/Switch3' */
  if (rtu_ETCS_EXT_DCT->lcetcsu1AutoTrans) {
    rtb_lcetcss16FrtAxlAcc = rtu_ETCS_RR->lcetcss16WhlSpdCrt;
  } else {
    rtb_lcetcss16FrtAxlAcc = rtu_ETCS_RR->lcetcss16MovigAvgWhlSpd;
  }

  /* End of Switch: '<Root>/Switch3' */

  /* Product: '<Root>/Divide1' incorporates:
   *  Sum: '<Root>/Add2'
   */
  rtb_Divide1 = (int16_T)((rtb_Divide1 + rtb_lcetcss16FrtAxlAcc) / 2);

  /* Product: '<Root>/Product' incorporates:
   *  Delay: '<Root>/Delay'
   *  Sum: '<Root>/Add1'
   */
  rtb_lcetcss16FrtAxlAcc = (int16_T)(((int16_T)(rtb_Divide2 -
    localDW->Delay_DSTATE[localDW->CircBufIdx])) << 1);

  /* Saturate: '<Root>/Saturation' */
  if (rtb_lcetcss16FrtAxlAcc > 2000) {
    rtb_lcetcss16FrtAxlAcc = 2000;
  } else {
    if (rtb_lcetcss16FrtAxlAcc < -2000) {
      rtb_lcetcss16FrtAxlAcc = -2000;
    }
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Product: '<Root>/Product1' incorporates:
   *  Delay: '<Root>/Delay1'
   *  Sum: '<Root>/Add3'
   */
  rtb_lcetcss16RrAxlAcc = (int16_T)(((int16_T)(rtb_Divide1 -
    localDW->Delay1_DSTATE[localDW->CircBufIdx_k])) << 1);

  /* Saturate: '<Root>/Saturation1' */
  if (rtb_lcetcss16RrAxlAcc > 2000) {
    rtb_lcetcss16RrAxlAcc = 2000;
  } else {
    if (rtb_lcetcss16RrAxlAcc < -2000) {
      rtb_lcetcss16RrAxlAcc = -2000;
    }
  }

  /* End of Saturate: '<Root>/Saturation1' */

  /* Switch: '<Root>/Switch4' incorporates:
   *  Constant: '<Root>/Constant4'
   */
  if (((boolean_T)VarFwd)) {
    rty_ETCS_AXL_ACC->lcetcss16DrvnAxlAcc = rtb_lcetcss16FrtAxlAcc;
  } else {
    rty_ETCS_AXL_ACC->lcetcss16DrvnAxlAcc = rtb_lcetcss16RrAxlAcc;
  }

  /* End of Switch: '<Root>/Switch4' */

  /* BusCreator: '<Root>/Bus Creator' */
  rty_ETCS_AXL_ACC->lcetcss16FrtAxlAcc = rtb_lcetcss16FrtAxlAcc;
  rty_ETCS_AXL_ACC->lcetcss16RrAxlAcc = rtb_lcetcss16RrAxlAcc;

  /* Update for Delay: '<Root>/Delay' */
  localDW->Delay_DSTATE[localDW->CircBufIdx] = rtb_Divide2;
  if (localDW->CircBufIdx < 8U) {
    localDW->CircBufIdx++;
  } else {
    localDW->CircBufIdx = 0U;
  }

  /* End of Update for Delay: '<Root>/Delay' */

  /* Update for Delay: '<Root>/Delay1' */
  localDW->Delay1_DSTATE[localDW->CircBufIdx_k] = rtb_Divide1;
  if (localDW->CircBufIdx_k < 8U) {
    localDW->CircBufIdx_k++;
  } else {
    localDW->CircBufIdx_k = 0U;
  }

  /* End of Update for Delay: '<Root>/Delay1' */
}

/* Model initialize function */
void LCETCS_vCalAxlAcc_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
