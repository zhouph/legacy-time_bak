/*
 * File: LCETCS_vCalFunLampCnt4Brk.c
 *
 * Code generated for Simulink model 'LCETCS_vCalFunLampCnt4Brk'.
 *
 * Model version                  : 1.229
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:52:16 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalFunLampCnt4Brk.h"
#include "LCETCS_vCalFunLampCnt4Brk_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalFunLampCnt4Brk' */
void LCETCS_vCalFunLampCnt4Brk_Init(uint8_T *rty_lcetcsu8FuncLampOffBrkCnt)
{
  /* InitializeConditions for Chart: '<Root>/Chart' */
  *rty_lcetcsu8FuncLampOffBrkCnt = 0U;
}

/* Output and update for referenced model: 'LCETCS_vCalFunLampCnt4Brk' */
void LCETCS_vCalFunLampCnt4Brk(const TypeETCSCirStruct *rtu_ETCS_PC, const
  TypeETCSCirStruct *rtu_ETCS_SC, const TypeETCSExtDctStruct *rtu_ETCS_EXT_DCT,
  uint8_T *rty_lcetcsu8FuncLampOffBrkCnt)
{
  /* Chart: '<Root>/Chart' */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:9' */
  /* comment */
  if (rtu_ETCS_EXT_DCT->lcetcsu1BTCSCtlAct == 1) {
    /* Transition: '<S1>:10' */
    /* Transition: '<S1>:108' */
    if ((rtu_ETCS_PC->lcetcsu1EstPresLowFlag == 1) &&
        (rtu_ETCS_SC->lcetcsu1EstPresLowFlag == 1)) {
      /* Transition: '<S1>:86' */
      /* Transition: '<S1>:88' */
      *rty_lcetcsu8FuncLampOffBrkCnt = ((uint8_T)U8ETCSCpFunLampOffCnt);

      /* Transition: '<S1>:145' */
    } else {
      /* Transition: '<S1>:98' */
      *rty_lcetcsu8FuncLampOffBrkCnt = 0U;
    }

    /* Transition: '<S1>:146' */
  } else {
    /* Transition: '<S1>:79' */
    *rty_lcetcsu8FuncLampOffBrkCnt = ((uint8_T)U8ETCSCpFunLampOffCnt);
  }

  /* End of Chart: '<Root>/Chart' */
  /* Transition: '<S1>:68' */
}

/* Model initialize function */
void LCETCS_vCalFunLampCnt4Brk_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
