/*
 * File: LCTMC_vCalInTrnRefTarGearMap_private.h
 *
 * Code generated for Simulink model 'LCTMC_vCalInTrnRefTarGearMap'.
 *
 * Model version                  : 1.234
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:08:55 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCTMC_vCalInTrnRefTarGearMap_private_h_
#define RTW_HEADER_LCTMC_vCalInTrnRefTarGearMap_private_h_
#include "rtwtypes.h"
#include "model_reference_types.h"

/* Includes for objects with custom storage classes. */
#include "../ETCSGenCodeHeader/ETCSHeader4Import.h"

/*
 * Generate compile time checks that imported macros for parameters
 * with storage class "ImportedDefine" are defined
 */
#ifndef U8TMCCpTurnIndex_1
#error The variable for the parameter "U8TMCCpTurnIndex_1" is not defined
#endif

#ifndef U8TMCCpTurnIndex_2
#error The variable for the parameter "U8TMCCpTurnIndex_2" is not defined
#endif

#ifndef U8TMCCpTurnIndex_3
#error The variable for the parameter "U8TMCCpTurnIndex_3" is not defined
#endif

#ifndef U8TMCpShftChgRPMInTrn_1
#error The variable for the parameter "U8TMCpShftChgRPMInTrn_1" is not defined
#endif

#ifndef U8TMCpShftChgRPMInTrn_2
#error The variable for the parameter "U8TMCpShftChgRPMInTrn_2" is not defined
#endif

#ifndef U8TMCpShftChgRPMInTrn_3
#error The variable for the parameter "U8TMCpShftChgRPMInTrn_3" is not defined
#endif

#ifndef U8TMCpShftChgVehSpdInTrn_1
#error The variable for the parameter "U8TMCpShftChgVehSpdInTrn_1" is not defined
#endif

#ifndef U8TMCpShftChgVehSpdInTrn_2
#error The variable for the parameter "U8TMCpShftChgVehSpdInTrn_2" is not defined
#endif

#ifndef U8TMCpShftChgVehSpdInTrn_3
#error The variable for the parameter "U8TMCpShftChgVehSpdInTrn_3" is not defined
#endif
#endif                                 /* RTW_HEADER_LCTMC_vCalInTrnRefTarGearMap_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
