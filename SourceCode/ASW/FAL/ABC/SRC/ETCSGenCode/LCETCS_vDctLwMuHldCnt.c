/*
 * File: LCETCS_vDctLwMuHldCnt.c
 *
 * Code generated for Simulink model 'LCETCS_vDctLwMuHldCnt'.
 *
 * Model version                  : 1.233
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:56:26 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctLwMuHldCnt.h"
#include "LCETCS_vDctLwMuHldCnt_private.h"

/* Initial conditions for referenced model: 'LCETCS_vDctLwMuHldCnt' */
void LCETCS_vDctLwMuHldCnt_Init(DW_LCETCS_vDctLwMuHldCnt_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = 0;
}

/* Output and update for referenced model: 'LCETCS_vDctLwMuHldCnt' */
void LCETCS_vDctLwMuHldCnt(boolean_T rtu_lcetcsu1DctLowHm, boolean_T
  rtu_lcetcsu1StrtPintLowHm, int16_T *rty_lcetcss16LowHmHldCnt,
  DW_LCETCS_vDctLwMuHldCnt_f_T *localDW)
{
  int32_T tmp;

  /* Chart: '<Root>/Chart' incorporates:
   *  UnitDelay: '<Root>/Unit Delay1'
   */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:142' */
  /* comment */
  if (rtu_lcetcsu1StrtPintLowHm == 1) {
    /* Transition: '<S1>:235' */
    /* Transition: '<S1>:238' */
    *rty_lcetcss16LowHmHldCnt = 1;

    /* Transition: '<S1>:239' */
    /* Transition: '<S1>:220' */
  } else {
    /* Transition: '<S1>:236' */
    if (rtu_lcetcsu1DctLowHm == 1) {
      /* Transition: '<S1>:211' */
      /* Transition: '<S1>:228' */
      tmp = localDW->UnitDelay1_DSTATE + 1;
      if (tmp > 32767) {
        tmp = 32767;
      }

      *rty_lcetcss16LowHmHldCnt = (int16_T)tmp;

      /* Transition: '<S1>:220' */
    } else {
      /* Transition: '<S1>:213' */
      *rty_lcetcss16LowHmHldCnt = 0;
    }
  }

  /* End of Chart: '<Root>/Chart' */

  /* Saturate: '<Root>/Saturation1' */
  /* Transition: '<S1>:222' */
  if ((*rty_lcetcss16LowHmHldCnt) > 30000) {
    *rty_lcetcss16LowHmHldCnt = 30000;
  } else {
    if ((*rty_lcetcss16LowHmHldCnt) < 0) {
      *rty_lcetcss16LowHmHldCnt = 0;
    }
  }

  /* End of Saturate: '<Root>/Saturation1' */

  /* Update for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = *rty_lcetcss16LowHmHldCnt;
}

/* Model initialize function */
void LCETCS_vDctLwMuHldCnt_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
