/*
 * File: LCETCS_vDctMisIValAtStrt.c
 *
 * Code generated for Simulink model 'LCETCS_vDctMisIValAtStrt'.
 *
 * Model version                  : 1.246
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:09:35 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctMisIValAtStrt.h"
#include "LCETCS_vDctMisIValAtStrt_private.h"

/* Initial conditions for referenced model: 'LCETCS_vDctMisIValAtStrt' */
void LCETCS_vDctMisIValAtStrt_Init(boolean_T *rty_lcetcsu1IValRecTorqAct,
  B_LCETCS_vDctMisIValAtStrt_c_T *localB, DW_LCETCS_vDctMisIValAtStrt_f_T
  *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalCtrlTime' */
  LCETCS_vCalCtrlTime_Init(&(localDW->LCETCS_vCalCtrlTime_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalIValMemryAtStrt' */
  LCETCS_vCalIValMemryAtStrt_Init(&localB->lcetcss32IValMeMryAtStrt,
    &(localDW->LCETCS_vCalIValMemryAtStrt_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctRecTrqActInBump' */
  LCETCS_vDctRecTrqActInBump_Init(rty_lcetcsu1IValRecTorqAct,
    &(localDW->LCETCS_vDctRecTrqActInBump_DWORK1.rtdw));
}

/* Output and update for referenced model: 'LCETCS_vDctMisIValAtStrt' */
void LCETCS_vDctMisIValAtStrt(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT,
  int32_T rtu_lcetcss32IValAtStrtOfCtl, const TypeETCSHighDctStruct
  *rtu_ETCS_HI_DCT, const TypeETCSCtlCmdStruct *rtu_ETCS_CTL_CMD_OLD, boolean_T *
  rty_lcetcsu1IValRecTorqAct, int32_T *rty_lcetcss32IValRecTorq,
  B_LCETCS_vDctMisIValAtStrt_c_T *localB, DW_LCETCS_vDctMisIValAtStrt_f_T
  *localDW)
{
  /* local block i/o variables */
  int16_T rtb_lcetcss16CtlActTime;

  /* ModelReference: '<Root>/LCETCS_vCalCtrlTime' */
  LCETCS_vCalCtrlTime(rtu_ETCS_CTL_ACT, &rtb_lcetcss16CtlActTime,
                      &(localDW->LCETCS_vCalCtrlTime_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalIValMemryAtStrt' */
  LCETCS_vCalIValMemryAtStrt(rtu_ETCS_CTL_ACT, rtu_lcetcss32IValAtStrtOfCtl,
    &localB->lcetcss32IValMeMryAtStrt,
    &(localDW->LCETCS_vCalIValMemryAtStrt_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vDctRecTrqActInBump' */
  LCETCS_vDctRecTrqActInBump(localB->lcetcss32IValMeMryAtStrt, rtu_ETCS_CTL_ACT,
    rtu_ETCS_HI_DCT, rtu_ETCS_CTL_CMD_OLD, rtb_lcetcss16CtlActTime,
    rty_lcetcsu1IValRecTorqAct, rty_lcetcss32IValRecTorq,
    &(localDW->LCETCS_vDctRecTrqActInBump_DWORK1.rtdw));
}

/* Model initialize function */
void LCETCS_vDctMisIValAtStrt_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalCtrlTime' */
  LCETCS_vCalCtrlTime_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalIValMemryAtStrt' */
  LCETCS_vCalIValMemryAtStrt_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctRecTrqActInBump' */
  LCETCS_vDctRecTrqActInBump_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
