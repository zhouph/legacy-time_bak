/*
 * File: LCETCS_vCalLdTrqByGrd.c
 *
 * Code generated for Simulink model 'LCETCS_vCalLdTrqByGrd'.
 *
 * Model version                  : 1.202
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:07:16 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalLdTrqByGrd.h"
#include "LCETCS_vCalLdTrqByGrd_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalLdTrqByGrd' */
void LCETCS_vCalLdTrqByGrd_Init(int16_T *rty_lcetcss16LdTrqByGrd,
  B_LCETCS_vCalLdTrqByGrd_c_T *localB, DW_LCETCS_vCalLdTrqByGrd_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = 0;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = 0;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay' */
  localB->lcetcss16TranTmeCnt = 0;

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16ChngRateLmt' */
  LCETCS_s16ChngRateLmt_Init(rty_lcetcss16LdTrqByGrd,
    &localB->lcetcss16TranTmeCnt, &(localDW->LCETCS_s16ChngRateLmt_DWORK1.rtb));
}

/* Start for referenced model: 'LCETCS_vCalLdTrqByGrd' */
void LCETCS_vCalLdTrqByGrd_Start(B_LCETCS_vCalLdTrqByGrd_c_T *localB)
{
  /* Start for Constant: '<Root>/Load torque by gradient transition time' */
  localB->lcetcss16TranTme = ((uint8_T)U8ETCSCpLdTrqByGrdTransTme);
}

/* Output and update for referenced model: 'LCETCS_vCalLdTrqByGrd' */
void LCETCS_vCalLdTrqByGrd(int16_T rtu_lcetcss16LdTrqByGrdRaw, int16_T
  *rty_lcetcss16LdTrqByGrd, B_LCETCS_vCalLdTrqByGrd_c_T *localB,
  DW_LCETCS_vCalLdTrqByGrd_f_T *localDW)
{
  /* local block i/o variables */
  int16_T rtb_lcetcss16TranTmeCntOld;
  int16_T rtb_lcetcss16TrgtSigValOld;
  int16_T rtb_lcetcss16ChngRateLmtdSigOld;

  /* UnitDelay: '<Root>/Unit Delay2' */
  rtb_lcetcss16TrgtSigValOld = localDW->UnitDelay2_DSTATE;

  /* Constant: '<Root>/Load torque by gradient transition time' */
  localB->lcetcss16TranTme = ((uint8_T)U8ETCSCpLdTrqByGrdTransTme);

  /* UnitDelay: '<Root>/Unit Delay1' */
  rtb_lcetcss16ChngRateLmtdSigOld = localDW->UnitDelay1_DSTATE;

  /* UnitDelay: '<Root>/Unit Delay' */
  rtb_lcetcss16TranTmeCntOld = localB->lcetcss16TranTmeCnt;

  /* ModelReference: '<Root>/LCETCS_s16ChngRateLmt' */
  LCETCS_s16ChngRateLmt(rtu_lcetcss16LdTrqByGrdRaw, rtb_lcetcss16TrgtSigValOld,
                        localB->lcetcss16TranTme,
                        rtb_lcetcss16ChngRateLmtdSigOld,
                        rtb_lcetcss16TranTmeCntOld, rty_lcetcss16LdTrqByGrd,
                        &localB->lcetcss16TranTmeCnt,
                        &(localDW->LCETCS_s16ChngRateLmt_DWORK1.rtb));

  /* Update for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = rtu_lcetcss16LdTrqByGrdRaw;

  /* Update for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = *rty_lcetcss16LdTrqByGrd;
}

/* Model initialize function */
void LCETCS_vCalLdTrqByGrd_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16ChngRateLmt' */
  LCETCS_s16ChngRateLmt_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
