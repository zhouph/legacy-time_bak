/*
 * File: LCTMC_vCalInTrnRefTarGearMap.c
 *
 * Code generated for Simulink model 'LCTMC_vCalInTrnRefTarGearMap'.
 *
 * Model version                  : 1.234
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:08:55 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCTMC_vCalInTrnRefTarGearMap.h"
#include "LCTMC_vCalInTrnRefTarGearMap_private.h"

/* Initial conditions for referenced model: 'LCTMC_vCalInTrnRefTarGearMap' */
void LCTMC_vCalInTrnRefTarGearMap_Init(DW_LCTMC_vCalInTrnRefTarGearMap_f_T
  *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCTMC_vCalRefEngSpdMapInTrn' */
  LCTMC_vCalRefEngSpdMapInTrn_Init
    (&(localDW->LCTMC_vCalRefEngSpdMapInTrn_DWORK1.rtb));

  /* InitializeConditions for ModelReference: '<Root>/LCTMC_vCalRefVehSpdMapInTrn' */
  LCTMC_vCalRefVehSpdMapInTrn_Init
    (&(localDW->LCTMC_vCalRefVehSpdMapInTrn_DWORK1.rtb));
}

/* Start for referenced model: 'LCTMC_vCalInTrnRefTarGearMap' */
void LCTMC_vCalInTrnRefTarGearMap_Start(DW_LCTMC_vCalInTrnRefTarGearMap_f_T
  *localDW)
{
  /* Start for ModelReference: '<Root>/LCTMC_vCalRefEngSpdMapInTrn' */
  LCTMC_vCalRefEngSpdMapInTrn_Start
    (&(localDW->LCTMC_vCalRefEngSpdMapInTrn_DWORK1.rtb));

  /* Start for ModelReference: '<Root>/LCTMC_vCalRefVehSpdMapInTrn' */
  LCTMC_vCalRefVehSpdMapInTrn_Start
    (&(localDW->LCTMC_vCalRefVehSpdMapInTrn_DWORK1.rtb));
}

/* Output and update for referenced model: 'LCTMC_vCalInTrnRefTarGearMap' */
void LCTMC_vCalInTrnRefTarGearMap(const TypeETCSGainStruct *rtu_ETCS_CTL_GAINS,
  int16_T *rty_lctmcs16TarShtChnVSInTrn, int16_T *rty_lctmcs16TarShtChnRPMInTrn,
  DW_LCTMC_vCalInTrnRefTarGearMap_f_T *localDW)
{
  /* ModelReference: '<Root>/LCTMC_vCalRefEngSpdMapInTrn' */
  LCTMC_vCalRefEngSpdMapInTrn(rtu_ETCS_CTL_GAINS, rty_lctmcs16TarShtChnRPMInTrn,
    &(localDW->LCTMC_vCalRefEngSpdMapInTrn_DWORK1.rtb));

  /* ModelReference: '<Root>/LCTMC_vCalRefVehSpdMapInTrn' */
  LCTMC_vCalRefVehSpdMapInTrn(rtu_ETCS_CTL_GAINS, rty_lctmcs16TarShtChnVSInTrn,
    &(localDW->LCTMC_vCalRefVehSpdMapInTrn_DWORK1.rtb));
}

/* Model initialize function */
void LCTMC_vCalInTrnRefTarGearMap_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCTMC_vCalRefEngSpdMapInTrn' */
  LCTMC_vCalRefEngSpdMapInTrn_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCTMC_vCalRefVehSpdMapInTrn' */
  LCTMC_vCalRefVehSpdMapInTrn_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
