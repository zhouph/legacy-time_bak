/*
 * File: LCTMC_vCalRefLimVehSpdMap.h
 *
 * Code generated for Simulink model 'LCTMC_vCalRefLimVehSpdMap'.
 *
 * Model version                  : 1.248
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:09:02 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCTMC_vCalRefLimVehSpdMap_h_
#define RTW_HEADER_LCTMC_vCalRefLimVehSpdMap_h_
#ifndef LCTMC_vCalRefLimVehSpdMap_COMMON_INCLUDES_
# define LCTMC_vCalRefLimVehSpdMap_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCTMC_vCalRefLimVehSpdMap_COMMON_INCLUDES_ */

#include "LCTMC_vCalRefLimVehSpdMap_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCTMC_vCalRefMinVehSpdMapbyGear.h"
#include "LCTMC_vCalRefMaxVehSpdMapbyGear.h"

/* Block states (auto storage) for model 'LCTMC_vCalRefLimVehSpdMap' */
typedef struct {
  MdlrefDW_LCTMC_vCalRefMaxVehSpdMapbyGear_T
    LCTMC_vCalRefMaxVehSpdMapbyGear_DWORK1;/* '<Root>/LCTMC_vCalRefMaxVehSpdMapbyGear' */
  MdlrefDW_LCTMC_vCalRefMinVehSpdMapbyGear_T
    LCTMC_vCalRefMinVehSpdMapbyGear_DWORK1;/* '<Root>/LCTMC_vCalRefMinVehSpdMapbyGear' */
} DW_LCTMC_vCalRefLimVehSpdMap_f_T;

typedef struct {
  DW_LCTMC_vCalRefLimVehSpdMap_f_T rtdw;
} MdlrefDW_LCTMC_vCalRefLimVehSpdMap_T;

/* Model reference registration function */
extern void LCTMC_vCalRefLimVehSpdMap_initialize(void);
extern void LCTMC_vCalRefLimVehSpdMap_Init(DW_LCTMC_vCalRefLimVehSpdMap_f_T
  *localDW);
extern void LCTMC_vCalRefLimVehSpdMap(const TypeETCSGearStruct
  *rtu_ETCS_GEAR_STRUCT, int16_T *rty_lctmcs16CordTarUpShtMaxVSbyGear, int16_T
  *rty_lctmcs16CordTarDnShtMinVSbyGear, DW_LCTMC_vCalRefLimVehSpdMap_f_T
  *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCTMC_vCalRefLimVehSpdMap'
 */
#endif                                 /* RTW_HEADER_LCTMC_vCalRefLimVehSpdMap_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
