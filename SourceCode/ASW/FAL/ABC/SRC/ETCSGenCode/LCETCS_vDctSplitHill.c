/*
 * File: LCETCS_vDctSplitHill.c
 *
 * Code generated for Simulink model 'LCETCS_vDctSplitHill'.
 *
 * Model version                  : 1.593
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:15:05 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctSplitHill.h"
#include "LCETCS_vDctSplitHill_private.h"

/* Initial conditions for referenced model: 'LCETCS_vDctSplitHill' */
void LCETCS_vDctSplitHill_Init(B_LCETCS_vDctSplitHill_c_T *localB,
  DW_LCETCS_vDctSplitHill_f_T *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalDiffAx' */
  LCETCS_vCalDiffAx_Init(&localB->lcetcsu1HillDctCompbyAx,
    &(localDW->LCETCS_vCalDiffAx_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalDistanceOfVeh' */
  LCETCS_vCalDistanceOfVeh_Init(&(localDW->LCETCS_vCalDistanceOfVeh_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctSplitHillCndSet' */
  LCETCS_vDctSplitHillCndSet_Init(&localB->lcetcsu1SplitHillClrCndSet,
    &localB->lcetcsu1SplitHillClrDecCndSet, &localB->lcetcsu1SplitHillDctCndSet);

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalSustainbyHsa' */
  LCETCS_vCalSustainbyHsa_Init(&localB->lcetcsu1HillbyHsaSustain,
    &(localDW->LCETCS_vCalSustainbyHsa_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalSplitHillCnt' */
  LCETCS_vCalSplitHillCnt_Init(&(localDW->LCETCS_vCalSplitHillCnt_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalGradientofHill' */
  LCETCS_vCalGradientofHill_Init(&localB->lcetcsu1InhibitGradient,
    &localB->lcetcsu1DctHoldGradient, &localB->lcetcsu1GradientTrstSet,
    &(localDW->LCETCS_vCalGradientofHill_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctSplitHillFlag' */
  LCETCS_vDctSplitHillFlag_Init(&localB->lcetcsu1SplitHillDct,
    &(localDW->LCETCS_vDctSplitHillFlag_DWORK1.rtdw));
}

/* Start for referenced model: 'LCETCS_vDctSplitHill' */
void LCETCS_vDctSplitHill_Start(DW_LCETCS_vDctSplitHill_f_T *localDW)
{
  /* Start for ModelReference: '<Root>/LCETCS_vCalGradientofHill' */
  LCETCS_vCalGradientofHill_Start
    (&(localDW->LCETCS_vCalGradientofHill_DWORK1.rtdw));
}

/* Output and update for referenced model: 'LCETCS_vDctSplitHill' */
void LCETCS_vDctSplitHill(const TypeETCSAxlStruct *rtu_ETCS_RA, const
  TypeETCSAxlStruct *rtu_ETCS_FA, const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG,
  const TypeETCSExtDctStruct *rtu_ETCS_EXT_DCT, const TypeETCSVehAccStruct
  *rtu_ETCS_VEH_ACC, int16_T rtu_lcetcss16VehSpd, const TypeETCSEngStruct
  *rtu_ETCS_ENG_STRUCT, TypeETCSSplitHillStruct *rty_ETCS_SPLIT_HILL,
  B_LCETCS_vDctSplitHill_c_T *localB, DW_LCETCS_vDctSplitHill_f_T *localDW)
{
  /* local block i/o variables */
  int16_T rtb_lcetcss16LongAccFilter;
  int16_T rtb_lcetcss16DistVehMovRd;
  int16_T rtb_lcetcss16SplitHillDctCnt;
  int16_T rtb_lcetcss16GradientOfHill;
  int16_T rtb_lcetcss16GradientTrstCnt;
  int16_T rtb_lcetcss16HoldGradientCnt;
  int16_T rtb_lcetcss16HoldRefGradientTime;
  uint8_T rtb_lcetcsu8HillDctCntbyAx;
  boolean_T rtb_lcetcsu1HillbyHsaAtv;

  /* ModelReference: '<Root>/LCETCS_vCalDiffAx' */
  LCETCS_vCalDiffAx(rtu_ETCS_ESC_SIG, rtu_ETCS_VEH_ACC, rtu_lcetcss16VehSpd,
                    &localB->lcetcsu1HillDctCompbyAx,
                    &rtb_lcetcsu8HillDctCntbyAx, &rtb_lcetcss16LongAccFilter,
                    &(localDW->LCETCS_vCalDiffAx_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vDctHillbyHsaAx' */
  LCETCS_vDctHillbyHsaAx(rtb_lcetcss16LongAccFilter, rtu_ETCS_EXT_DCT,
    &rtb_lcetcsu1HillbyHsaAtv);

  /* ModelReference: '<Root>/LCETCS_vCalDistanceOfVeh' */
  LCETCS_vCalDistanceOfVeh(rtu_lcetcss16VehSpd, &rtb_lcetcss16DistVehMovRd,
    &(localDW->LCETCS_vCalDistanceOfVeh_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vDctSplitHillCndSet' */
  LCETCS_vDctSplitHillCndSet(rtu_ETCS_FA, rtu_ETCS_RA,
    rtb_lcetcss16LongAccFilter, rtu_ETCS_VEH_ACC, rtu_ETCS_ENG_STRUCT,
    rtu_lcetcss16VehSpd, &localB->lcetcsu1SplitHillClrCndSet,
    &localB->lcetcsu1SplitHillClrDecCndSet, &localB->lcetcsu1SplitHillDctCndSet);

  /* ModelReference: '<Root>/LCETCS_vCalSustainbyHsa' */
  LCETCS_vCalSustainbyHsa(rtb_lcetcsu1HillbyHsaAtv, rtb_lcetcss16DistVehMovRd,
    localB->lcetcsu1SplitHillClrCndSet, &localB->lcetcsu1HillbyHsaSustain,
    &(localDW->LCETCS_vCalSustainbyHsa_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalSplitHillCnt' */
  LCETCS_vCalSplitHillCnt(localB->lcetcsu1HillDctCompbyAx,
    localB->lcetcsu1HillbyHsaSustain, localB->lcetcsu1SplitHillDctCndSet,
    localB->lcetcsu1SplitHillClrDecCndSet, localB->lcetcsu1SplitHillClrCndSet,
    &rtb_lcetcss16SplitHillDctCnt,
    &(localDW->LCETCS_vCalSplitHillCnt_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalGradientofHill' */
  LCETCS_vCalGradientofHill(rtb_lcetcss16LongAccFilter, rtu_ETCS_VEH_ACC,
    rtu_lcetcss16VehSpd, rtu_ETCS_ESC_SIG, &rtb_lcetcss16GradientOfHill,
    &rtb_lcetcss16GradientTrstCnt, &localB->lcetcsu1InhibitGradient,
    &localB->lcetcsu1DctHoldGradient, &rtb_lcetcss16HoldGradientCnt,
    &rtb_lcetcss16HoldRefGradientTime, &localB->lcetcsu1GradientTrstSet,
    &(localDW->LCETCS_vCalGradientofHill_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vDctSplitHillFlag' */
  LCETCS_vDctSplitHillFlag(rtb_lcetcss16SplitHillDctCnt,
    rtb_lcetcss16GradientOfHill, &localB->lcetcsu1SplitHillDct,
    &(localDW->LCETCS_vDctSplitHillFlag_DWORK1.rtdw));

  /* BusCreator: '<Root>/Bus Creator' */
  rty_ETCS_SPLIT_HILL->lcetcsu1SplitHillDct = localB->lcetcsu1SplitHillDct;
  rty_ETCS_SPLIT_HILL->lcetcsu1HillDctCompbyAx = localB->lcetcsu1HillDctCompbyAx;
  rty_ETCS_SPLIT_HILL->lcetcss16LongAccFilter = rtb_lcetcss16LongAccFilter;
  rty_ETCS_SPLIT_HILL->lcetcsu1HillbyHsaAtv = rtb_lcetcsu1HillbyHsaAtv;
  rty_ETCS_SPLIT_HILL->lcetcsu1SplitHillDctCndSet =
    localB->lcetcsu1SplitHillDctCndSet;
  rty_ETCS_SPLIT_HILL->lcetcsu1SplitHillClrCndSet =
    localB->lcetcsu1SplitHillClrCndSet;
  rty_ETCS_SPLIT_HILL->lcetcsu1SplitHillClrDecCndSet =
    localB->lcetcsu1SplitHillClrDecCndSet;
  rty_ETCS_SPLIT_HILL->lcetcss16SplitHillDctCnt = rtb_lcetcss16SplitHillDctCnt;
  rty_ETCS_SPLIT_HILL->lcetcss16DistVehMovRd = rtb_lcetcss16DistVehMovRd;
  rty_ETCS_SPLIT_HILL->lcetcsu1HillbyHsaSustain =
    localB->lcetcsu1HillbyHsaSustain;
  rty_ETCS_SPLIT_HILL->lcetcsu8HillDctCntbyAx = rtb_lcetcsu8HillDctCntbyAx;
  rty_ETCS_SPLIT_HILL->lcetcss16GradientTrstCnt = rtb_lcetcss16GradientTrstCnt;
  rty_ETCS_SPLIT_HILL->lcetcss16GradientOfHill = rtb_lcetcss16GradientOfHill;
  rty_ETCS_SPLIT_HILL->lcetcsu1InhibitGradient = localB->lcetcsu1InhibitGradient;
  rty_ETCS_SPLIT_HILL->lcetcsu1DctHoldGradient = localB->lcetcsu1DctHoldGradient;
  rty_ETCS_SPLIT_HILL->lcetcss16HoldGradientCnt = rtb_lcetcss16HoldGradientCnt;
  rty_ETCS_SPLIT_HILL->lcetcss16HoldRefGradientTime =
    rtb_lcetcss16HoldRefGradientTime;
  rty_ETCS_SPLIT_HILL->lcetcsu1GradientTrstSet = localB->lcetcsu1GradientTrstSet;
}

/* Model initialize function */
void LCETCS_vDctSplitHill_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalDiffAx' */
  LCETCS_vCalDiffAx_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalDistanceOfVeh' */
  LCETCS_vCalDistanceOfVeh_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalGradientofHill' */
  LCETCS_vCalGradientofHill_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalSplitHillCnt' */
  LCETCS_vCalSplitHillCnt_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalSustainbyHsa' */
  LCETCS_vCalSustainbyHsa_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctHillbyHsaAx' */
  LCETCS_vDctHillbyHsaAx_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctSplitHillCndSet' */
  LCETCS_vDctSplitHillCndSet_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctSplitHillFlag' */
  LCETCS_vDctSplitHillFlag_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
