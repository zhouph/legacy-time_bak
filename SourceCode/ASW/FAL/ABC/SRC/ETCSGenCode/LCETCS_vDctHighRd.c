/*
 * File: LCETCS_vDctHighRd.c
 *
 * Code generated for Simulink model 'LCETCS_vDctHighRd'.
 *
 * Model version                  : 1.176
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:10:18 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctHighRd.h"
#include "LCETCS_vDctHighRd_private.h"

/* Initial conditions for referenced model: 'LCETCS_vDctHighRd' */
void LCETCS_vDctHighRd_Init(DW_LCETCS_vDctHighRd_f_T *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctLwToHi' */
  LCETCS_vDctLwToHi_Init(&(localDW->LCETCS_vDctLwToHi_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctAdaptHighRd' */
  LCETCS_vDctAdaptHighRd_Init(&(localDW->LCETCS_vDctAdaptHighRd_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctPreHighRd' */
  LCETCS_vDctPreHighRd_Init(&(localDW->LCETCS_vDctPreHighRd_DWORK1.rtb),
    &(localDW->LCETCS_vDctPreHighRd_DWORK1.rtdw));
}

/* Output and update for referenced model: 'LCETCS_vDctHighRd' */
void LCETCS_vDctHighRd(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const
  TypeETCSWhlAccStruct *rtu_ETCS_WHL_ACC, const TypeETCSWhlSpinStruct
  *rtu_ETCS_WHL_SPIN, const TypeETCSTrq4RdFricStruct
  *rtu_ETCS_TRQ_REP_RD_FRIC_OLD, const TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC,
  const TypeETCSCrdnTrqStruct *rtu_ETCS_CRDN_TRQ, int16_T rtu_lcetcss16VehSpd,
  const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const TypeETCSTarSpinStruct
  *rtu_ETCS_TAR_SPIN, const TypeETCSCtlErrStruct *rtu_ETCS_CTL_ERR,
  TypeETCSHighDctStruct *rty_ETCS_HI_DCT, DW_LCETCS_vDctHighRd_f_T *localDW)
{
  /* local block i/o variables */
  int16_T rtb_lcetcss16LwToHiCnt;
  int16_T rtb_lcetcss16LwToHiCntbyArea;
  int16_T rtb_lcetcss16AdaptHighRdCnt;
  int16_T rtb_lcetcss16PreHighRdDctCnt;

  /* ModelReference: '<Root>/LCETCS_vDctLwToHi' */
  LCETCS_vDctLwToHi(rtu_ETCS_WHL_ACC, rtu_ETCS_CTL_ACT, rtu_ETCS_WHL_SPIN,
                    rtu_ETCS_TAR_SPIN, &rtb_lcetcss16LwToHiCnt,
                    &rtb_lcetcss16LwToHiCntbyArea,
                    &(localDW->LCETCS_vDctLwToHi_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vDctAdaptHighRd' */
  LCETCS_vDctAdaptHighRd(rtu_ETCS_CTL_ACT, rtu_ETCS_CTL_ERR,
    rtu_ETCS_TRQ_REP_RD_FRIC_OLD, rtu_ETCS_ESC_SIG, &rtb_lcetcss16AdaptHighRdCnt,
    &(localDW->LCETCS_vDctAdaptHighRd_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vDctPreHighRd' */
  LCETCS_vDctPreHighRd(rtu_ETCS_VEH_ACC, rtu_ETCS_CRDN_TRQ, rtu_lcetcss16VehSpd,
                       rtu_ETCS_ESC_SIG, rtu_ETCS_WHL_SPIN, rtu_ETCS_TAR_SPIN,
                       rtu_ETCS_CTL_ERR, &rtb_lcetcss16PreHighRdDctCnt,
                       &(localDW->LCETCS_vDctPreHighRd_DWORK1.rtb),
                       &(localDW->LCETCS_vDctPreHighRd_DWORK1.rtdw));

  /* BusCreator: '<Root>/Bus Creator' */
  rty_ETCS_HI_DCT->lcetcss16LwToHiCnt = rtb_lcetcss16LwToHiCnt;
  rty_ETCS_HI_DCT->lcetcss16LwToHiCntbyArea = rtb_lcetcss16LwToHiCntbyArea;
  rty_ETCS_HI_DCT->lcetcss16AdaptHighRdCnt = rtb_lcetcss16AdaptHighRdCnt;
  rty_ETCS_HI_DCT->lcetcss16PreHighRdDctCnt = rtb_lcetcss16PreHighRdDctCnt;
}

/* Model initialize function */
void LCETCS_vDctHighRd_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctAdaptHighRd' */
  LCETCS_vDctAdaptHighRd_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctLwToHi' */
  LCETCS_vDctLwToHi_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctPreHighRd' */
  LCETCS_vDctPreHighRd_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
