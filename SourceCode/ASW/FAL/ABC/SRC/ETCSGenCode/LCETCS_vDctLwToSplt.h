/*
 * File: LCETCS_vDctLwToSplt.h
 *
 * Code generated for Simulink model 'LCETCS_vDctLwToSplt'.
 *
 * Model version                  : 1.289
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:06:24 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vDctLwToSplt_h_
#define RTW_HEADER_LCETCS_vDctLwToSplt_h_
#ifndef LCETCS_vDctLwToSplt_COMMON_INCLUDES_
# define LCETCS_vDctLwToSplt_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vDctLwToSplt_COMMON_INCLUDES_ */

#include "LCETCS_vDctLwToSplt_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vDctWhlOnMu.h"
#include "LCETCS_vDctLwMuTendency.h"
#include "LCETCS_vDctLwMuHldCnt.h"
#include "LCETCS_vCalLwToSpltCnt.h"

/* Block signals for model 'LCETCS_vDctLwToSplt' */
typedef struct {
  int16_T lcetcss16LowMuWhlSpinNF;     /* '<Root>/LCETCS_vDctWhlOnMu' */
  int16_T lcetcss16HighMuWhlSpinNF;    /* '<Root>/LCETCS_vDctWhlOnMu' */
  boolean_T lcetcsu1DctWhlOnHighmu;    /* '<Root>/LCETCS_vDctWhlOnMu' */
  boolean_T lcetcsu1DctLowHm;          /* '<Root>/LCETCS_vDctLwMuTendency' */
  boolean_T lcetcsu1StrtPintLowHm;     /* '<Root>/LCETCS_vDctLwMuTendency' */
} B_LCETCS_vDctLwToSplt_c_T;

/* Block states (auto storage) for model 'LCETCS_vDctLwToSplt' */
typedef struct {
  int16_T UnitDelay2_DSTATE;           /* '<Root>/Unit Delay2' */
  int16_T UnitDelay1_DSTATE;           /* '<Root>/Unit Delay1' */
  MdlrefDW_LCETCS_vDctLwMuTendency_T LCETCS_vDctLwMuTendency_DWORK1;/* '<Root>/LCETCS_vDctLwMuTendency' */
  MdlrefDW_LCETCS_vCalLwToSpltCnt_T LCETCS_vCalLwToSpltCnt_DWORK1;/* '<Root>/LCETCS_vCalLwToSpltCnt' */
  MdlrefDW_LCETCS_vDctLwMuHldCnt_T LCETCS_vDctLwMuHldCnt_DWORK1;/* '<Root>/LCETCS_vDctLwMuHldCnt' */
} DW_LCETCS_vDctLwToSplt_f_T;

typedef struct {
  B_LCETCS_vDctLwToSplt_c_T rtb;
  DW_LCETCS_vDctLwToSplt_f_T rtdw;
} MdlrefDW_LCETCS_vDctLwToSplt_T;

/* Model reference registration function */
extern void LCETCS_vDctLwToSplt_initialize(void);
extern void LCETCS_vDctLwToSplt_Init(B_LCETCS_vDctLwToSplt_c_T *localB,
  DW_LCETCS_vDctLwToSplt_f_T *localDW);
extern void LCETCS_vDctLwToSplt(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT,
  const TypeETCSAxlStruct *rtu_ETCS_FA, const TypeETCSAxlStruct *rtu_ETCS_RA,
  const TypeETCSWhlStruct *rtu_ETCS_FL, const TypeETCSWhlStruct *rtu_ETCS_FR,
  const TypeETCSWhlStruct *rtu_ETCS_RL, const TypeETCSWhlStruct *rtu_ETCS_RR,
  const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const TypeETCSTrq4RdFricStruct
  *rtu_ETCS_TRQ_REP_RD_FRIC_OLD, const TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN,
  const TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC, TypeETCSLwToSpltStruct
  *rty_ETCS_L2SPLT, B_LCETCS_vDctLwToSplt_c_T *localB,
  DW_LCETCS_vDctLwToSplt_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vDctLwToSplt'
 */
#endif                                 /* RTW_HEADER_LCETCS_vDctLwToSplt_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
