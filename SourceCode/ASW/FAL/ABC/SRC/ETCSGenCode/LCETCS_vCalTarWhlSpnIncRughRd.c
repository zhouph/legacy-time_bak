/*
 * File: LCETCS_vCalTarWhlSpnIncRughRd.c
 *
 * Code generated for Simulink model 'LCETCS_vCalTarWhlSpnIncRughRd'.
 *
 * Model version                  : 1.73
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:49:39 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalTarWhlSpnIncRughRd.h"
#include "LCETCS_vCalTarWhlSpnIncRughRd_private.h"

/* Output and update for referenced model: 'LCETCS_vCalTarWhlSpnIncRughRd' */
void LCETCS_vCalTarWhlSpnIncRughRd(const TypeETCSWhlStruct *rtu_ETCS_FL, const
  TypeETCSWhlStruct *rtu_ETCS_FR, const TypeETCSWhlStruct *rtu_ETCS_RL, const
  TypeETCSWhlStruct *rtu_ETCS_RR, int16_T *rty_lcetcss16TarWhlSpnIncRughRd)
{
  int16_T u0;

  /* MinMax: '<Root>/MinMax' */
  if (rtu_ETCS_FL->lcetcss16WhlDelSpnRughRd >=
      rtu_ETCS_FR->lcetcss16WhlDelSpnRughRd) {
    u0 = rtu_ETCS_FL->lcetcss16WhlDelSpnRughRd;
  } else {
    u0 = rtu_ETCS_FR->lcetcss16WhlDelSpnRughRd;
  }

  if (u0 >= rtu_ETCS_RL->lcetcss16WhlDelSpnRughRd) {
  } else {
    u0 = rtu_ETCS_RL->lcetcss16WhlDelSpnRughRd;
  }

  if (u0 >= rtu_ETCS_RR->lcetcss16WhlDelSpnRughRd) {
  } else {
    u0 = rtu_ETCS_RR->lcetcss16WhlDelSpnRughRd;
  }

  /* Product: '<Root>/Divide' incorporates:
   *  MinMax: '<Root>/MinMax'
   */
  *rty_lcetcss16TarWhlSpnIncRughRd = (int16_T)(u0 / 2);
}

/* Model initialize function */
void LCETCS_vCalTarWhlSpnIncRughRd_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
