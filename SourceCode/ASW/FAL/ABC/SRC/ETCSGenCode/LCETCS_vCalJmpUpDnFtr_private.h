/*
 * File: LCETCS_vCalJmpUpDnFtr_private.h
 *
 * Code generated for Simulink model 'LCETCS_vCalJmpUpDnFtr'.
 *
 * Model version                  : 1.210
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:04:41 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalJmpUpDnFtr_private_h_
#define RTW_HEADER_LCETCS_vCalJmpUpDnFtr_private_h_
#include "rtwtypes.h"
#include "model_reference_types.h"

/* Includes for objects with custom storage classes. */
#include "../ETCSGenCodeHeader/ETCSHeader4Import.h"

/*
 * Generate compile time checks that imported macros for parameters
 * with storage class "ImportedDefine" are defined
 */
#ifndef U8ETCSCpFtr4JmpDnMax
#error The variable for the parameter "U8ETCSCpFtr4JmpDnMax" is not defined
#endif

#ifndef U8ETCSCpFtr4JmpDnMin
#error The variable for the parameter "U8ETCSCpFtr4JmpDnMin" is not defined
#endif

#ifndef U8ETCSCpFtr4JmpUpMax
#error The variable for the parameter "U8ETCSCpFtr4JmpUpMax" is not defined
#endif

#ifndef U8ETCSCpFtr4JmpUpMin
#error The variable for the parameter "U8ETCSCpFtr4JmpUpMin" is not defined
#endif

#ifndef U8ETCSCpRefTrqNvldTm
#error The variable for the parameter "U8ETCSCpRefTrqNvldTm" is not defined
#endif

#ifndef U8ETCSCpRefTrqVldTm
#error The variable for the parameter "U8ETCSCpRefTrqVldTm" is not defined
#endif
#endif                                 /* RTW_HEADER_LCETCS_vCalJmpUpDnFtr_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
