/*
 * File: LCETCS_vCalICtlGain.h
 *
 * Code generated for Simulink model 'LCETCS_vCalICtlGain'.
 *
 * Model version                  : 1.218
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:08:11 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalICtlGain_h_
#define RTW_HEADER_LCETCS_vCalICtlGain_h_
#ifndef LCETCS_vCalICtlGain_COMMON_INCLUDES_
# define LCETCS_vCalICtlGain_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalICtlGain_COMMON_INCLUDES_ */

#include "LCETCS_vCalICtlGain_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vCordICtlGain.h"
#include "LCETCS_vCalSpltBsIGain.h"
#include "LCETCS_vCalIGainIncFacH2L.h"
#include "LCETCS_vCalIGainFtrInTrn.h"
#include "LCETCS_vCalIGainFacInGearChng.h"
#include "LCETCS_vCalHomoBsIGain.h"
#include "LCETCS_vCalHmIGainFctAftrTrn.h"
#include "LCETCS_vCalHmIGainFct4BigSpn.h"
#include "LCETCS_vCalBsIGain.h"

/* Block states (auto storage) for model 'LCETCS_vCalICtlGain' */
typedef struct {
  MdlrefDW_LCETCS_vCalHomoBsIGain_T LCETCS_vCalHomoBsIGain_DWORK1;/* '<Root>/LCETCS_vCalHomoBsIGain' */
  MdlrefDW_LCETCS_vCalSpltBsIGain_T LCETCS_vCalSpltBsIGain_DWORK1;/* '<Root>/LCETCS_vCalSpltBsIGain' */
  MdlrefDW_LCETCS_vCalHmIGainFct4BigSpn_T LCETCS_vCalHmIGainFct4BigSpn_DWORK1;/* '<Root>/LCETCS_vCalHmIGainFct4BigSpn' */
  MdlrefDW_LCETCS_vCalHmIGainFctAftrTrn_T LCETCS_vCalHmIGainFctAftrTrn_DWORK1;/* '<Root>/LCETCS_vCalHmIGainFctAftrTrn' */
  MdlrefDW_LCETCS_vCalIGainFacInGearChng_T LCETCS_vCalIGainFacInGearChng_DWORK1;/* '<Root>/LCETCS_vCalIGainFacInGearChng' */
  MdlrefDW_LCETCS_vCalIGainFtrInTrn_T LCETCS_vCalIGainFtrInTrn_DWORK1;/* '<Root>/LCETCS_vCalIGainFtrInTrn' */
  MdlrefDW_LCETCS_vCalIGainIncFacH2L_T LCETCS_vCalIGainIncFacH2L_DWORK1;/* '<Root>/LCETCS_vCalIGainIncFacH2L' */
} DW_LCETCS_vCalICtlGain_f_T;

typedef struct {
  DW_LCETCS_vCalICtlGain_f_T rtdw;
} MdlrefDW_LCETCS_vCalICtlGain_T;

/* Model reference registration function */
extern void LCETCS_vCalICtlGain_initialize(void);
extern void LCETCS_vCalICtlGain_Init(int16_T *rty_lcetcss16BsIgain, int16_T
  *rty_lcetcss16IgainFacInGearChng, int16_T *rty_lcetcss16IgainFac,
  DW_LCETCS_vCalICtlGain_f_T *localDW);
extern void LCETCS_vCalICtlGain_Start(DW_LCETCS_vCalICtlGain_f_T *localDW);
extern void LCETCS_vCalICtlGain(const TypeETCSAftLmtCrng *rtu_ETCS_AFT_TURN,
  const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const TypeETCSDrvMdlStruct
  *rtu_ETCS_DRV_MDL, const TypeETCSCtlErrStruct *rtu_ETCS_CTL_ERR, const
  TypeETCSTrq4RdFricStruct *rtu_ETCS_TRQ_REP_RD_FRIC, const
  TypeETCSGainGearShftStruct *rtu_ETCS_GAIN_GEAR_SHFT, const TypeETCSAxlStruct
  *rtu_ETCS_FA, const TypeETCSAxlStruct *rtu_ETCS_RA, const
  TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN, const TypeETCSEscSigStruct
  *rtu_ETCS_ESC_SIG, const TypeETCSH2LStruct *rtu_ETCS_H2L, int16_T
  *rty_lcetcss16Igain, int16_T *rty_lcetcss16BsIgain, int16_T
  *rty_lcetcss16IgainFacInGearChng, int16_T *rty_lcetcss16IGainIncFacH2L,
  int16_T *rty_lcetcss16HomoBsIgain, int16_T *rty_lcetcss16SpltBsIgain, int16_T *
  rty_lcetcss16IGainFacInTrn, int16_T *rty_lcetcss16IgainFac, int16_T
  *rty_lcetcss16HmIGainFctAftrTrn, int16_T *rty_lcetcss16HmIGainFct4BigSpn,
  DW_LCETCS_vCalICtlGain_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalICtlGain'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalICtlGain_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
