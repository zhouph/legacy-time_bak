/*
 * File: LCETCS_vDctReqVehSpdAsit.c
 *
 * Code generated for Simulink model 'LCETCS_vDctReqVehSpdAsit'.
 *
 * Model version                  : 1.156
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:10:27 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctReqVehSpdAsit.h"
#include "LCETCS_vDctReqVehSpdAsit_private.h"

/* Initial conditions for referenced model: 'LCETCS_vDctReqVehSpdAsit' */
void LCETCS_vDctReqVehSpdAsit_Init(B_LCETCS_vDctReqVehSpdAsit_c_T *localB,
  DW_LCETCS_vDctReqVehSpdAsit_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localB->lcetcsu1ReqVcaBrkTrqAct = false;

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctInhibitBrkVca' */
  LCETCS_vDctInhibitBrkVca_Init(&localB->lcetcsu1InhibitBrkVca,
    &(localDW->LCETCS_vDctInhibitBrkVca_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctSetVca' */
  LCETCS_vDctSetVca_Init(&localB->lcetcsu1ReqVcaTrqAct,
    &(localDW->LCETCS_vDctSetVca_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctReqVcaAct' */
  LCETCS_vDctReqVcaAct_Init(&localB->lcetcsu1ReqVcaEngTrqAct,
    &localB->lcetcsu1ReqVcaEngTrqRsgEdg, &localB->lcetcsu1ReqVcaBrkTrqAct,
    &localB->lcetcsu1ReqVcaBrkTrqRsgEdg,
    &(localDW->LCETCS_vDctReqVcaAct_DWORK1.rtdw));
}

/* Output and update for referenced model: 'LCETCS_vDctReqVehSpdAsit' */
void LCETCS_vDctReqVehSpdAsit(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT,
  const TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC, TypeETCSReqVCAStruct
  *rty_ETCS_REQ_VCA, B_LCETCS_vDctReqVehSpdAsit_c_T *localB,
  DW_LCETCS_vDctReqVehSpdAsit_f_T *localDW)
{
  /* local block i/o variables */
  int16_T rtb_lcetcss16VehAccErrCount;
  boolean_T rtb_lcetcsu1ReqVcaBrkTrqActOld;

  /* UnitDelay: '<Root>/Unit Delay1' */
  rtb_lcetcsu1ReqVcaBrkTrqActOld = localB->lcetcsu1ReqVcaBrkTrqAct;

  /* ModelReference: '<Root>/LCETCS_vDctInhibitBrkVca' */
  LCETCS_vDctInhibitBrkVca(rtb_lcetcsu1ReqVcaBrkTrqActOld, rtu_ETCS_VEH_ACC,
    &localB->lcetcsu1InhibitBrkVca,
    &(localDW->LCETCS_vDctInhibitBrkVca_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vDctSetVca' */
  LCETCS_vDctSetVca(rtu_ETCS_VEH_ACC, rtu_ETCS_CTL_ACT,
                    &rtb_lcetcss16VehAccErrCount, &localB->lcetcsu1ReqVcaTrqAct,
                    &(localDW->LCETCS_vDctSetVca_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vDctReqVcaAct' */
  LCETCS_vDctReqVcaAct(rtu_ETCS_CTL_ACT, localB->lcetcsu1InhibitBrkVca,
                       localB->lcetcsu1ReqVcaTrqAct,
                       &localB->lcetcsu1ReqVcaEngTrqAct,
                       &localB->lcetcsu1ReqVcaEngTrqRsgEdg,
                       &localB->lcetcsu1ReqVcaBrkTrqAct,
                       &localB->lcetcsu1ReqVcaBrkTrqRsgEdg,
                       &(localDW->LCETCS_vDctReqVcaAct_DWORK1.rtdw));

  /* BusCreator: '<Root>/Bus Creator' */
  rty_ETCS_REQ_VCA->lcetcsu1ReqVcaEngTrqAct = localB->lcetcsu1ReqVcaEngTrqAct;
  rty_ETCS_REQ_VCA->lcetcsu1ReqVcaEngTrqRsgEdg =
    localB->lcetcsu1ReqVcaEngTrqRsgEdg;
  rty_ETCS_REQ_VCA->lcetcsu1ReqVcaBrkTrqAct = localB->lcetcsu1ReqVcaBrkTrqAct;
  rty_ETCS_REQ_VCA->lcetcsu1ReqVcaBrkTrqRsgEdg =
    localB->lcetcsu1ReqVcaBrkTrqRsgEdg;
  rty_ETCS_REQ_VCA->lcetcsu1ReqVcaTrqAct = localB->lcetcsu1ReqVcaTrqAct;
  rty_ETCS_REQ_VCA->lcetcss16VehAccErrCount = rtb_lcetcss16VehAccErrCount;
}

/* Model initialize function */
void LCETCS_vDctReqVehSpdAsit_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctInhibitBrkVca' */
  LCETCS_vDctInhibitBrkVca_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctReqVcaAct' */
  LCETCS_vDctReqVcaAct_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctSetVca' */
  LCETCS_vDctSetVca_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
