/*
 * File: LCETCS_vCalStrtThSymBrkCtl.h
 *
 * Code generated for Simulink model 'LCETCS_vCalStrtThSymBrkCtl'.
 *
 * Model version                  : 1.137
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:59:17 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalStrtThSymBrkCtl_h_
#define RTW_HEADER_LCETCS_vCalStrtThSymBrkCtl_h_
#ifndef LCETCS_vCalStrtThSymBrkCtl_COMMON_INCLUDES_
# define LCETCS_vCalStrtThSymBrkCtl_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalStrtThSymBrkCtl_COMMON_INCLUDES_ */

#include "LCETCS_vCalStrtThSymBrkCtl_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_s16Inter5Point.h"
#include "LCETCS_s16Inter2Point.h"

/* Block signals for model 'LCETCS_vCalStrtThSymBrkCtl' */
typedef struct {
  int16_T y1;                          /* '<Root>/Symmetric brake control start threshold at vehicle speed break point 1' */
  int16_T y2;                          /* '<Root>/Symmetric brake control start threshold at vehicle speed break point 2' */
  int16_T y3;                          /* '<Root>/Symmetric brake control start threshold at vehicle speed break point 3' */
  int16_T y4;                          /* '<Root>/Symmetric brake control start threshold at vehicle speed break point 4' */
  int16_T y5;                          /* '<Root>/Symmetric brake control start threshold at vehicle speed break point 5' */
  int16_T y;                           /* '<Root>/LCETCS_s16Inter5Point' */
  int16_T y_d;                         /* '<Root>/LCETCS_s16Inter2Point' */
  int16_T x2;                          /* '<Root>/High acceleration threshold' */
  int16_T x1;                          /* '<Root>/Low acceleration threshold' */
} B_LCETCS_vCalStrtThSymBrkCtl_c_T;

typedef struct {
  B_LCETCS_vCalStrtThSymBrkCtl_c_T rtb;
} MdlrefDW_LCETCS_vCalStrtThSymBrkCtl_T;

/* Model reference registration function */
extern void LCETCS_vCalStrtThSymBrkCtl_initialize(void);
extern void LCETCS_vCalStrtThSymBrkCtl_Init(B_LCETCS_vCalStrtThSymBrkCtl_c_T
  *localB);
extern void LCETCS_vCalStrtThSymBrkCtl_Start(B_LCETCS_vCalStrtThSymBrkCtl_c_T
  *localB);
extern void LCETCS_vCalStrtThSymBrkCtl(const TypeETCSTarSpinStruct
  *rtu_ETCS_TAR_SPIN, const TypeETCSAxlAccStruct *rtu_ETCS_AXL_ACC, int16_T
  rtu_lcetcss16VehSpd, int16_T *rty_lcetcss16SymBrkCtlStrtTh,
  B_LCETCS_vCalStrtThSymBrkCtl_c_T *localB);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalStrtThSymBrkCtl'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalStrtThSymBrkCtl_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
