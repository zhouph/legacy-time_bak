/*
 * File: LCETCS_vCalLdTrq.h
 *
 * Code generated for Simulink model 'LCETCS_vCalLdTrq'.
 *
 * Model version                  : 1.103
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:10:57 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalLdTrq_h_
#define RTW_HEADER_LCETCS_vCalLdTrq_h_
#ifndef LCETCS_vCalLdTrq_COMMON_INCLUDES_
# define LCETCS_vCalLdTrq_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalLdTrq_COMMON_INCLUDES_ */

#include "LCETCS_vCalLdTrq_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vMemAxAtHillDctd.h"
#include "LCETCS_vCordLdTrq.h"
#include "LCETCS_vCalLdTrqDecBySymBrkCtl.h"
#include "LCETCS_vCalLdTrqByGrdRaw.h"
#include "LCETCS_vCalLdTrqByGrd.h"
#include "LCETCS_vCalLdTrqByAsymBrkCtl.h"

/* Block signals for model 'LCETCS_vCalLdTrq' */
typedef struct {
  int16_T lcetcss16AxAtHillDctd;       /* '<Root>/LCETCS_vMemAxAtHillDctd' */
  int16_T lcetcss16LdTrqByGrd;         /* '<Root>/LCETCS_vCalLdTrqByGrd' */
} B_LCETCS_vCalLdTrq_c_T;

/* Block states (auto storage) for model 'LCETCS_vCalLdTrq' */
typedef struct {
  MdlrefDW_LCETCS_vCalLdTrqDecBySymBrkCtl_T
    LCETCS_vCalLdTrqDecBySymBrkCtl_DWORK1;/* '<Root>/LCETCS_vCalLdTrqDecBySymBrkCtl' */
  MdlrefDW_LCETCS_vMemAxAtHillDctd_T LCETCS_vMemAxAtHillDctd_DWORK1;/* '<Root>/LCETCS_vMemAxAtHillDctd' */
  MdlrefDW_LCETCS_vCalLdTrqByGrd_T LCETCS_vCalLdTrqByGrd_DWORK1;/* '<Root>/LCETCS_vCalLdTrqByGrd' */
} DW_LCETCS_vCalLdTrq_f_T;

typedef struct {
  B_LCETCS_vCalLdTrq_c_T rtb;
  DW_LCETCS_vCalLdTrq_f_T rtdw;
} MdlrefDW_LCETCS_vCalLdTrq_T;

/* Model reference registration function */
extern void LCETCS_vCalLdTrq_initialize(void);
extern void LCETCS_vCalLdTrq_Init(B_LCETCS_vCalLdTrq_c_T *localB,
  DW_LCETCS_vCalLdTrq_f_T *localDW);
extern void LCETCS_vCalLdTrq_Start(DW_LCETCS_vCalLdTrq_f_T *localDW);
extern void LCETCS_vCalLdTrq(const TypeETCSAxlStruct *rtu_ETCS_FA, const
  TypeETCSAxlStruct *rtu_ETCS_RA, const TypeETCSBrkCtlCmdStruct
  *rtu_ETCS_BRK_CTL_CMD_OLD, const TypeETCSBrkCtlReqStruct
  *rtu_ETCS_BRK_CTL_REQ_OLD, const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const
  TypeETCSSplitHillStruct *rtu_ETCS_SPLIT_HILL_OLD, TypeETCSLdTrqStruct
  *rty_ETCS_LD_TRQ, B_LCETCS_vCalLdTrq_c_T *localB, DW_LCETCS_vCalLdTrq_f_T
  *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalLdTrq'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalLdTrq_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
