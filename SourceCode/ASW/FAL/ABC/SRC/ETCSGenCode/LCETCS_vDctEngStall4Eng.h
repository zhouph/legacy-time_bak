/*
 * File: LCETCS_vDctEngStall4Eng.h
 *
 * Code generated for Simulink model 'LCETCS_vDctEngStall4Eng'.
 *
 * Model version                  : 1.275
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:13:21 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vDctEngStall4Eng_h_
#define RTW_HEADER_LCETCS_vDctEngStall4Eng_h_
#ifndef LCETCS_vDctEngStall4Eng_COMMON_INCLUDES_
# define LCETCS_vDctEngStall4Eng_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vDctEngStall4Eng_COMMON_INCLUDES_ */

#include "LCETCS_vDctEngStall4Eng_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vDctEngStlEnterExitCond.h"
#include "LCETCS_vDctEngStallAxSenExist.h"
#include "LCETCS_vCalEngStallThr4Ax.h"
#include "LCETCS_vCalEngSpdSlope.h"

/* Block signals for model 'LCETCS_vDctEngStall4Eng' */
typedef struct {
  int16_T lcetcss16EngStallThrTemp;    /* '<Root>/LCETCS_vCalEngStallThr4Ax' */
} B_LCETCS_vDctEngStall4Eng_c_T;

/* Block states (auto storage) for model 'LCETCS_vDctEngStall4Eng' */
typedef struct {
  MdlrefDW_LCETCS_vCalEngSpdSlope_T LCETCS_vCalEngSpdSlope_DWORK1;/* '<Root>/LCETCS_vCalEngSpdSlope' */
  MdlrefDW_LCETCS_vCalEngStallThr4Ax_T LCETCS_vCalEngStallThr4Ax_DWORK1;/* '<Root>/LCETCS_vCalEngStallThr4Ax' */
  MdlrefDW_LCETCS_vDctEngStlEnterExitCond_T
    LCETCS_vDctEngStlEnterExitCond_DWORK1;/* '<Root>/LCETCS_vDctEngStlEnterExitCond' */
} DW_LCETCS_vDctEngStall4Eng_f_T;

typedef struct {
  B_LCETCS_vDctEngStall4Eng_c_T rtb;
  DW_LCETCS_vDctEngStall4Eng_f_T rtdw;
} MdlrefDW_LCETCS_vDctEngStall4Eng_T;

/* Model reference registration function */
extern void LCETCS_vDctEngStall4Eng_initialize(void);
extern void LCETCS_vDctEngStall4Eng_Init(boolean_T *rty_lcetcsu1EngStallTemp,
  DW_LCETCS_vDctEngStall4Eng_f_T *localDW);
extern void LCETCS_vDctEngStall4Eng_Start(DW_LCETCS_vDctEngStall4Eng_f_T
  *localDW);
extern void LCETCS_vDctEngStall4Eng_Disable(DW_LCETCS_vDctEngStall4Eng_f_T
  *localDW);
extern void LCETCS_vDctEngStall4Eng(const TypeETCSCtlActStruct
  *rtu_ETCS_CTL_ACT_OLD, const TypeETCSSplitHillStruct *rtu_ETCS_SPLIT_HILL_OLD,
  int16_T rtu_lcetcss16VehSpd, const TypeETCSEngStruct *rtu_ETCS_ENG_STRUCT,
  const TypeETCSEngStallStruct *rtu_ETCS_ENG_STALL_OLD, boolean_T
  *rty_lcetcsu1EngStallTemp, int16_T *rty_lcetcss16EngStallThr, int8_T
  *rty_lcetcss8EngStallRiskIndex, int16_T *rty_lcetcss16EngSpdSlope,
  B_LCETCS_vDctEngStall4Eng_c_T *localB, DW_LCETCS_vDctEngStall4Eng_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vDctEngStall4Eng'
 */
#endif                                 /* RTW_HEADER_LCETCS_vDctEngStall4Eng_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
