/*
 * File: LCETCS_vCalDrvLinMdl.c
 *
 * Code generated for Simulink model 'LCETCS_vCalDrvLinMdl'.
 *
 * Model version                  : 1.90
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:10:47 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalDrvLinMdl.h"
#include "LCETCS_vCalDrvLinMdl_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalDrvLinMdl' */
void LCETCS_vCalDrvLinMdl_Init(DW_LCETCS_vCalDrvLinMdl_f_T *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalTotalGearRatioRaw' */
  LCETCS_vCalTotalGearRatioRaw_Init
    (&(localDW->LCETCS_vCalTotalGearRatioRaw_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalGrTransTime' */
  LCETCS_vCalGrTransTime_Init(&(localDW->LCETCS_vCalGrTransTime_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalTotalGearRatio' */
  LCETCS_vCalTotalGearRatio_Init(&(localDW->LCETCS_vCalTotalGearRatio_DWORK1.rtb),
    &(localDW->LCETCS_vCalTotalGearRatio_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalTransEfficiency' */
  LCETCS_vCalTransEfficiency_Init
    (&(localDW->LCETCS_vCalTransEfficiency_DWORK1.rtb),
     &(localDW->LCETCS_vCalTransEfficiency_DWORK1.rtdw));
}

/* Output and update for referenced model: 'LCETCS_vCalDrvLinMdl' */
void LCETCS_vCalDrvLinMdl(const TypeETCSGearStruct *rtu_ETCS_GEAR_STRUCT, const
  TypeETCS4WDSigStruct *rtu_ETCS_4WD_SIG, TypeETCSDrvMdlStruct *rty_ETCS_DRV_MDL,
  DW_LCETCS_vCalDrvLinMdl_f_T *localDW)
{
  /* local block i/o variables */
  int16_T rtb_lcetcss16TotalGearRatioRaw;
  int16_T rtb_lcetcss16TotalGearRatio;
  int16_T rtb_lcetcss16TransEfficiencyRaw;
  int16_T rtb_lcetcss16TransEfficiency;
  int16_T rtb_lcetcss16TrqRatio;
  uint8_T rtb_lcetcsu8GrTransTime;
  int16_T rtb_lcetcss16TotalGearRatio_j;

  /* ModelReference: '<Root>/LCETCS_vCalTotalGearRatioRaw' */
  LCETCS_vCalTotalGearRatioRaw(rtu_ETCS_4WD_SIG, rtu_ETCS_GEAR_STRUCT,
    &rtb_lcetcss16TotalGearRatioRaw,
    &(localDW->LCETCS_vCalTotalGearRatioRaw_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalGrTransTime' */
  LCETCS_vCalGrTransTime(rtu_ETCS_GEAR_STRUCT, &rtb_lcetcsu8GrTransTime,
    &(localDW->LCETCS_vCalGrTransTime_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalTotalGearRatio' */
  LCETCS_vCalTotalGearRatio(rtb_lcetcss16TotalGearRatioRaw,
    rtb_lcetcsu8GrTransTime, &rtb_lcetcss16TotalGearRatio,
    &(localDW->LCETCS_vCalTotalGearRatio_DWORK1.rtb),
    &(localDW->LCETCS_vCalTotalGearRatio_DWORK1.rtdw));

  /* SignalConversion: '<Root>/Signal Conversion4' */
  rtb_lcetcss16TotalGearRatio_j = rtb_lcetcss16TotalGearRatio;

  /* ModelReference: '<Root>/LCETCS_vCalTransEfficiencyRaw' */
  LCETCS_vCalTransEfficiencyRaw(rtu_ETCS_GEAR_STRUCT,
    &rtb_lcetcss16TransEfficiencyRaw);

  /* ModelReference: '<Root>/LCETCS_vCalTransEfficiency' */
  LCETCS_vCalTransEfficiency(rtb_lcetcsu8GrTransTime,
    rtb_lcetcss16TransEfficiencyRaw, &rtb_lcetcss16TransEfficiency,
    &(localDW->LCETCS_vCalTransEfficiency_DWORK1.rtb),
    &(localDW->LCETCS_vCalTransEfficiency_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalTrqRatio' */
  LCETCS_vCalTrqRatio(rtb_lcetcss16TotalGearRatio, rtb_lcetcss16TransEfficiency,
                      &rtb_lcetcss16TrqRatio);

  /* SignalConversion: '<Root>/Signal Conversion6' */
  rty_ETCS_DRV_MDL->lcetcss16TrqRatio = rtb_lcetcss16TrqRatio;

  /* BusCreator: '<Root>/Bus Creator1' incorporates:
   *  SignalConversion: '<Root>/Signal Conversion1'
   *  SignalConversion: '<Root>/Signal Conversion2'
   *  SignalConversion: '<Root>/Signal Conversion5'
   */
  rty_ETCS_DRV_MDL->lcetcss16TotalGearRatio = rtb_lcetcss16TotalGearRatio_j;
  rty_ETCS_DRV_MDL->lcetcss16TransEfficiency = rtb_lcetcss16TransEfficiency;
  rty_ETCS_DRV_MDL->lcetcss16TotalGearRatioRaw = rtb_lcetcss16TotalGearRatioRaw;
  rty_ETCS_DRV_MDL->lcetcss16TransEfficiencyRaw =
    rtb_lcetcss16TransEfficiencyRaw;
}

/* Model initialize function */
void LCETCS_vCalDrvLinMdl_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalGrTransTime' */
  LCETCS_vCalGrTransTime_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalTotalGearRatio' */
  LCETCS_vCalTotalGearRatio_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalTotalGearRatioRaw' */
  LCETCS_vCalTotalGearRatioRaw_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalTransEfficiency' */
  LCETCS_vCalTransEfficiency_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalTransEfficiencyRaw' */
  LCETCS_vCalTransEfficiencyRaw_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalTrqRatio' */
  LCETCS_vCalTrqRatio_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
