/*
 * File: LCETCS_vCalEngStallThr4Ax.c
 *
 * Code generated for Simulink model 'LCETCS_vCalEngStallThr4Ax'.
 *
 * Model version                  : 1.236
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:10:09 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalEngStallThr4Ax.h"
#include "LCETCS_vCalEngStallThr4Ax_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalEngStallThr4Ax' */
void LCETCS_vCalEngStallThr4Ax_Init(DW_LCETCS_vCalEngStallThr4Ax_f_T *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalEngStallRiskIndex' */
  LCETCS_vCalEngStallRiskIndex_Init
    (&(localDW->LCETCS_vCalEngStallRiskIndex_DWORK1.rtdw));
}

/* Disable for referenced model: 'LCETCS_vCalEngStallThr4Ax' */
void LCETCS_vCalEngStallThr4Ax_Disable(DW_LCETCS_vCalEngStallThr4Ax_f_T *localDW)
{
  /* Disable for ModelReference: '<Root>/LCETCS_vCalEngStallRiskIndex' */
  LCETCS_vCalEngStallRiskIndex_Disable
    (&(localDW->LCETCS_vCalEngStallRiskIndex_DWORK1.rtdw));
}

/* Start for referenced model: 'LCETCS_vCalEngStallThr4Ax' */
void LCETCS_vCalEngStallThr4Ax_Start(DW_LCETCS_vCalEngStallThr4Ax_f_T *localDW)
{
  /* Start for ModelReference: '<Root>/LCETCS_vCalEngStallRiskIndex' */
  LCETCS_vCalEngStallRiskIndex_Start
    (&(localDW->LCETCS_vCalEngStallRiskIndex_DWORK1.rtb),
     &(localDW->LCETCS_vCalEngStallRiskIndex_DWORK1.rtdw));

  /* Start for ModelReference: '<Root>/LCETCS_vCalEngStallThr' */
  LCETCS_vCalEngStallThr_Start(&(localDW->LCETCS_vCalEngStallThr_DWORK1.rtb));
}

/* Output and update for referenced model: 'LCETCS_vCalEngStallThr4Ax' */
void LCETCS_vCalEngStallThr4Ax(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT_OLD,
  const TypeETCSEngStallStruct *rtu_ETCS_ENG_STALL_OLD, const
  TypeETCSSplitHillStruct *rtu_ETCS_SPLIT_HILL_OLD, int16_T rtu_lcetcss16VehSpd,
  int16_T rtu_lcetcss16EngSpdSlope, int16_T *rty_lcetcss16EngStallThrTemp,
  int8_T *rty_lcetcss8EngStallRiskIndex, DW_LCETCS_vCalEngStallThr4Ax_f_T
  *localDW)
{
  /* ModelReference: '<Root>/LCETCS_vCalEngStallRiskIndex' */
  LCETCS_vCalEngStallRiskIndex(rtu_ETCS_CTL_ACT_OLD, rtu_ETCS_ENG_STALL_OLD,
    rtu_lcetcss16VehSpd, rtu_lcetcss16EngSpdSlope, rty_lcetcss8EngStallRiskIndex,
    &(localDW->LCETCS_vCalEngStallRiskIndex_DWORK1.rtb),
    &(localDW->LCETCS_vCalEngStallRiskIndex_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalEngStallThr' */
  LCETCS_vCalEngStallThr((*rty_lcetcss8EngStallRiskIndex),
    rtu_ETCS_SPLIT_HILL_OLD, rty_lcetcss16EngStallThrTemp,
    &(localDW->LCETCS_vCalEngStallThr_DWORK1.rtb));
}

/* Model initialize function */
void LCETCS_vCalEngStallThr4Ax_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalEngStallRiskIndex' */
  LCETCS_vCalEngStallRiskIndex_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalEngStallThr' */
  LCETCS_vCalEngStallThr_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
