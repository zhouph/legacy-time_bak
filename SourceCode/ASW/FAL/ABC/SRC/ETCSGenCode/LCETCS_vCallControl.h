/*
 * File: LCETCS_vCallControl.h
 *
 * Code generated for Simulink model 'LCETCS_vCallControl'.
 *
 * Model version                  : 1.260
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:16:07 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCallControl_h_
#define RTW_HEADER_LCETCS_vCallControl_h_
#ifndef LCETCS_vCallControl_COMMON_INCLUDES_
# define LCETCS_vCallControl_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCallControl_COMMON_INCLUDES_ */

#include "LCETCS_vCallControl_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCTMC_vCallControl.h"
#include "LCETCS_vCalTrqRepRdFric.h"
#include "LCETCS_vCalSymBrkCtlTrqCmd.h"
#include "LCETCS_vCalMaxLmtSymBrkTrq.h"
#include "LCETCS_vCalFunLamp.h"
#include "LCETCS_vCalCtlGain.h"
#include "LCETCS_vCalCardanTrqCmd.h"

/* Block states (auto storage) for model 'LCETCS_vCallControl' */
typedef struct {
  TypeTMCReqGearStruct UnitDelay1_DSTATE;/* '<Root>/Unit Delay1' */
  MdlrefDW_LCETCS_vCalTrqRepRdFric_T LCETCS_vCalTrqRepRdFric_DWORK1;/* '<Root>/LCETCS_vCalTrqRepRdFric' */
  MdlrefDW_LCETCS_vCalCtlGain_T LCETCS_vCalCtlGain_DWORK1;/* '<Root>/LCETCS_vCalCtlGain' */
  MdlrefDW_LCETCS_vCalCardanTrqCmd_T LCETCS_vCalCardanTrqCmd_DWORK1;/* '<Root>/LCETCS_vCalCardanTrqCmd' */
  MdlrefDW_LCETCS_vCalFunLamp_T LCETCS_vCalFunLamp_DWORK1;/* '<Root>/LCETCS_vCalFunLamp' */
  MdlrefDW_LCETCS_vCalMaxLmtSymBrkTrq_T LCETCS_vCalMaxLmtSymBrkTrq_DWORK1;/* '<Root>/LCETCS_vCalMaxLmtSymBrkTrq' */
  MdlrefDW_LCETCS_vCalSymBrkCtlTrqCmd_T LCETCS_vCalSymBrkCtlTrqCmd_DWORK1;/* '<Root>/LCETCS_vCalSymBrkCtlTrqCmd' */
  MdlrefDW_LCTMC_vCallControl_T LCTMC_vCallControl_DWORK1;/* '<Root>/LCTMC_vCallControl' */
} DW_LCETCS_vCallControl_f_T;

typedef struct {
  DW_LCETCS_vCallControl_f_T rtdw;
} MdlrefDW_LCETCS_vCallControl_T;

/* Model reference registration function */
extern void LCETCS_vCallControl_initialize(const rtTimingBridge *timingBridge);
extern const TypeTMCReqGearStruct LCETCS_vCallControl_rtZTypeTMCReqGearStruct;/* TypeTMCReqGearStruct ground */
extern void LCETCS_vCallControl_Init(TypeTMCReqGearStruct
  *rty_TMC_REQ_GEAR_STRUCT, boolean_T *rty_lcetcsu1FuncLampOn,
  DW_LCETCS_vCallControl_f_T *localDW);
extern void LCETCS_vCallControl_Start(DW_LCETCS_vCallControl_f_T *localDW);
extern void LCETCS_vCallControl(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT,
  const TypeETCSCtlErrStruct *rtu_ETCS_CTL_ERR, const TypeETCSDrvMdlStruct
  *rtu_ETCS_DRV_MDL, const TypeETCSIValRstStruct *rtu_ETCS_IVAL_RST, const
  TypeETCSCyl1stStruct *rtu_ETCS_CYL_1ST, const TypeETCSVehAccStruct
  *rtu_ETCS_VEH_ACC, const TypeETCSLowWhlSpnStruct *rtu_ETCS_LOW_WHL_SPN, const
  TypeETCSGainGearShftStruct *rtu_ETCS_GAIN_GEAR_SHFT, const TypeETCSLdTrqStruct
  *rtu_ETCS_LD_TRQ, const TypeETCSExtDctStruct *rtu_ETCS_EXT_DCT, const
  TypeETCSEngStruct *rtu_ETCS_ENG_STRUCT, const TypeETCSH2LStruct *rtu_ETCS_H2L,
  const TypeETCSBrkCtlReqStruct *rtu_ETCS_BRK_CTL_REQ, const
  TypeETCSAxlAccStruct *rtu_ETCS_AXL_ACC, const TypeETCSAxlStruct *rtu_ETCS_FA,
  const TypeETCSAxlStruct *rtu_ETCS_RA, const TypeETCSCrdnTrqStruct
  *rtu_ETCS_CRDN_TRQ, const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const
  TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN, int16_T rtu_lcetcss16VehSpd, const
  TypeETCSGearStruct *rtu_ETCS_GEAR_STRUCT, boolean_T rtu_lcetcsu1VehVibDct,
  const TypeETCSSplitHillStruct *rtu_ETCS_SPLIT_HILL, const
  TypeETCSWhlSpinStruct *rtu_ETCS_WHL_SPIN, const TypeETCSCirStruct *rtu_ETCS_PC,
  const TypeETCSCirStruct *rtu_ETCS_SC, const TypeETCSAftLmtCrng
  *rtu_ETCS_AFT_TURN, const TypeETCSCtlCmdStruct *rtu_ETCS_CTL_CMD_OLD,
  TypeETCSGainStruct *rty_ETCS_CTL_GAINS, TypeETCSTrq4RdFricStruct
  *rty_ETCS_TRQ_REP_RD_FRIC, TypeTMCReqGearStruct *rty_TMC_REQ_GEAR_STRUCT,
  TypeETCSBrkCtlCmdStruct *rty_ETCS_BRK_CTL_CMD, boolean_T
  *rty_lcetcsu1FuncLampOn, TypeETCSCtlCmdStruct *rty_ETCS_CTL_CMD,
  DW_LCETCS_vCallControl_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCallControl'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCallControl_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
