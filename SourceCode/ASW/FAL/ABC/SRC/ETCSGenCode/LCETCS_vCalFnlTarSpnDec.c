/*
 * File: LCETCS_vCalFnlTarSpnDec.c
 *
 * Code generated for Simulink model 'LCETCS_vCalFnlTarSpnDec'.
 *
 * Model version                  : 1.16
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:59:35 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalFnlTarSpnDec.h"
#include "LCETCS_vCalFnlTarSpnDec_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalFnlTarSpnDec' */
void LCETCS_vCalFnlTarSpnDec_Init(B_LCETCS_vCalFnlTarSpnDec_c_T *localB)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter3by5' */
  LCETCS_s16Inter3by5_Init(&localB->Out);
}

/* Start for referenced model: 'LCETCS_vCalFnlTarSpnDec' */
void LCETCS_vCalFnlTarSpnDec_Start(B_LCETCS_vCalFnlTarSpnDec_c_T *localB)
{
  /* Start for Constant: '<Root>/Ay level on asphalt' */
  localB->x3 = ((uint8_T)U8ETCSCpAyAsp);

  /* Start for Constant: '<Root>/Ay level on ice' */
  localB->x1 = ((uint8_T)U8ETCSCpAyIce);

  /* Start for Constant: '<Root>/Ay level on snow' */
  localB->x2 = ((uint8_T)U8ETCSCpAySnw);

  /* Start for Constant: '<Root>/Final target spin decrease on asphalt 1' */
  localB->z13 = ((int8_T)S8ETCSCpFnlTarSpnDecAsp_1);

  /* Start for Constant: '<Root>/Final target spin decrease on asphalt 2' */
  localB->z23 = ((int8_T)S8ETCSCpFnlTarSpnDecAsp_2);

  /* Start for Constant: '<Root>/Final target spin decrease on asphalt 3' */
  localB->z33 = ((int8_T)S8ETCSCpFnlTarSpnDecAsp_3);

  /* Start for Constant: '<Root>/Final target spin decrease on asphalt 4' */
  localB->z43 = ((int8_T)S8ETCSCpFnlTarSpnDecAsp_4);

  /* Start for Constant: '<Root>/Final target spin decrease on asphalt 5' */
  localB->z53 = ((int8_T)S8ETCSCpFnlTarSpnDecAsp_5);

  /* Start for Constant: '<Root>/Final target spin decrease on ice 1' */
  localB->z11 = ((int8_T)S8ETCSCpFnlTarSpnDecIce_1);

  /* Start for Constant: '<Root>/Final target spin decrease on ice 2' */
  localB->z21 = ((int8_T)S8ETCSCpFnlTarSpnDecIce_2);

  /* Start for Constant: '<Root>/Final target spin decrease on ice 3' */
  localB->z31 = ((int8_T)S8ETCSCpFnlTarSpnDecIce_3);

  /* Start for Constant: '<Root>/Final target spin decrease on ice 4' */
  localB->z41 = ((int8_T)S8ETCSCpFnlTarSpnDecIce_4);

  /* Start for Constant: '<Root>/Final target spin decrease on ice 5' */
  localB->z51 = ((int8_T)S8ETCSCpFnlTarSpnDecIce_5);

  /* Start for Constant: '<Root>/Final target spin decrease on snow 1' */
  localB->z12 = ((int8_T)S8ETCSCpFnlTarSpnDecSnw_1);

  /* Start for Constant: '<Root>/Final target spin decrease on snow 2' */
  localB->z22 = ((int8_T)S8ETCSCpFnlTarSpnDecSnw_2);

  /* Start for Constant: '<Root>/Final target spin decrease on snow 3' */
  localB->z32 = ((int8_T)S8ETCSCpFnlTarSpnDecSnw_3);

  /* Start for Constant: '<Root>/Final target spin decrease on snow 4' */
  localB->z42 = ((int8_T)S8ETCSCpFnlTarSpnDecSnw_4);

  /* Start for Constant: '<Root>/Final target spin decrease on snow 5' */
  localB->z52 = ((int8_T)S8ETCSCpFnlTarSpnDecSnw_5);
}

/* Output and update for referenced model: 'LCETCS_vCalFnlTarSpnDec' */
void LCETCS_vCalFnlTarSpnDec(const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG,
  int16_T rtu_lcetcss16VehSpd, int16_T *rty_lcetcss16FnlTarSpnDec,
  B_LCETCS_vCalFnlTarSpnDec_c_T *localB)
{
  /* Constant: '<Root>/Ay level on asphalt' */
  localB->x3 = ((uint8_T)U8ETCSCpAyAsp);

  /* Constant: '<Root>/Ay level on ice' */
  localB->x1 = ((uint8_T)U8ETCSCpAyIce);

  /* Constant: '<Root>/Ay level on snow' */
  localB->x2 = ((uint8_T)U8ETCSCpAySnw);

  /* Constant: '<Root>/Final target spin decrease on asphalt 1' */
  localB->z13 = ((int8_T)S8ETCSCpFnlTarSpnDecAsp_1);

  /* Constant: '<Root>/Final target spin decrease on asphalt 2' */
  localB->z23 = ((int8_T)S8ETCSCpFnlTarSpnDecAsp_2);

  /* Constant: '<Root>/Final target spin decrease on asphalt 3' */
  localB->z33 = ((int8_T)S8ETCSCpFnlTarSpnDecAsp_3);

  /* Constant: '<Root>/Final target spin decrease on asphalt 4' */
  localB->z43 = ((int8_T)S8ETCSCpFnlTarSpnDecAsp_4);

  /* Constant: '<Root>/Final target spin decrease on asphalt 5' */
  localB->z53 = ((int8_T)S8ETCSCpFnlTarSpnDecAsp_5);

  /* Constant: '<Root>/Final target spin decrease on ice 1' */
  localB->z11 = ((int8_T)S8ETCSCpFnlTarSpnDecIce_1);

  /* Constant: '<Root>/Final target spin decrease on ice 2' */
  localB->z21 = ((int8_T)S8ETCSCpFnlTarSpnDecIce_2);

  /* Constant: '<Root>/Final target spin decrease on ice 3' */
  localB->z31 = ((int8_T)S8ETCSCpFnlTarSpnDecIce_3);

  /* Constant: '<Root>/Final target spin decrease on ice 4' */
  localB->z41 = ((int8_T)S8ETCSCpFnlTarSpnDecIce_4);

  /* Constant: '<Root>/Final target spin decrease on ice 5' */
  localB->z51 = ((int8_T)S8ETCSCpFnlTarSpnDecIce_5);

  /* Constant: '<Root>/Final target spin decrease on snow 1' */
  localB->z12 = ((int8_T)S8ETCSCpFnlTarSpnDecSnw_1);

  /* Constant: '<Root>/Final target spin decrease on snow 2' */
  localB->z22 = ((int8_T)S8ETCSCpFnlTarSpnDecSnw_2);

  /* Constant: '<Root>/Final target spin decrease on snow 3' */
  localB->z32 = ((int8_T)S8ETCSCpFnlTarSpnDecSnw_3);

  /* Constant: '<Root>/Final target spin decrease on snow 4' */
  localB->z42 = ((int8_T)S8ETCSCpFnlTarSpnDecSnw_4);

  /* Constant: '<Root>/Final target spin decrease on snow 5' */
  localB->z52 = ((int8_T)S8ETCSCpFnlTarSpnDecSnw_5);

  /* ModelReference: '<Root>/LCETCS_s16Inter3by5' */
  LCETCS_s16Inter3by5(rtu_ETCS_ESC_SIG->lcetcss16LatAccMsrdMani, localB->x1,
                      localB->x2, localB->x3, rtu_lcetcss16VehSpd, ((int16_T)
    S16ETCSCpSpd_1), ((int16_T)S16ETCSCpSpd_2), ((int16_T)S16ETCSCpSpd_3),
                      ((int16_T)S16ETCSCpSpd_4), ((int16_T)S16ETCSCpSpd_5),
                      localB->z11, localB->z12, localB->z13, localB->z21,
                      localB->z22, localB->z23, localB->z31, localB->z32,
                      localB->z33, localB->z41, localB->z42, localB->z43,
                      localB->z51, localB->z52, localB->z53, &localB->Out);

  /* Saturate: '<Root>/Saturation1' */
  if (localB->Out > 0) {
    *rty_lcetcss16FnlTarSpnDec = 0;
  } else if (localB->Out < -120) {
    *rty_lcetcss16FnlTarSpnDec = -120;
  } else {
    *rty_lcetcss16FnlTarSpnDec = localB->Out;
  }

  /* End of Saturate: '<Root>/Saturation1' */
}

/* Model initialize function */
void LCETCS_vCalFnlTarSpnDec_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter3by5' */
  LCETCS_s16Inter3by5_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
