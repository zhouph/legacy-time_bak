/*
 * File: LCETCS_vCalTarDecInTrn.h
 *
 * Code generated for Simulink model 'LCETCS_vCalTarDecInTrn'.
 *
 * Model version                  : 1.171
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:00:01 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalTarDecInTrn_h_
#define RTW_HEADER_LCETCS_vCalTarDecInTrn_h_
#ifndef LCETCS_vCalTarDecInTrn_COMMON_INCLUDES_
# define LCETCS_vCalTarDecInTrn_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalTarDecInTrn_COMMON_INCLUDES_ */

#include "LCETCS_vCalTarDecInTrn_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_s16Inter2Point.h"

/* Block signals for model 'LCETCS_vCalTarDecInTrn' */
typedef struct {
  int16_T lcetcss16TarDecInTrn;        /* '<Root>/LCETCS_s16Inter2Point' */
  int16_T x2;                          /* '<Root>/Constant2' */
} B_LCETCS_vCalTarDecInTrn_c_T;

typedef struct {
  B_LCETCS_vCalTarDecInTrn_c_T rtb;
} MdlrefDW_LCETCS_vCalTarDecInTrn_T;

/* Model reference registration function */
extern void LCETCS_vCalTarDecInTrn_initialize(void);
extern void LCETCS_vCalTarDecInTrn_Init(B_LCETCS_vCalTarDecInTrn_c_T *localB);
extern void LCETCS_vCalTarDecInTrn_Start(B_LCETCS_vCalTarDecInTrn_c_T *localB);
extern void LCETCS_vCalTarDecInTrn(int16_T rtu_lcetcss16TarDecDpndDelYaw,
  int16_T rtu_lcetcss16TarSpnDecCnt, int16_T *rty_lcetcss16TarDecInTrn,
  B_LCETCS_vCalTarDecInTrn_c_T *localB);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalTarDecInTrn'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalTarDecInTrn_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
