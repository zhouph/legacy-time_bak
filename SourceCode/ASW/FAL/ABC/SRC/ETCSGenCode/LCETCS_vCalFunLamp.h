/*
 * File: LCETCS_vCalFunLamp.h
 *
 * Code generated for Simulink model 'LCETCS_vCalFunLamp'.
 *
 * Model version                  : 1.190
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:03:13 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalFunLamp_h_
#define RTW_HEADER_LCETCS_vCalFunLamp_h_
#ifndef LCETCS_vCalFunLamp_COMMON_INCLUDES_
# define LCETCS_vCalFunLamp_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalFunLamp_COMMON_INCLUDES_ */

#include "LCETCS_vCalFunLamp_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vDctFunLampFlg.h"
#include "LCETCS_vCalFunLampCnt4Eng.h"
#include "LCETCS_vCalFunLampCnt4Brk.h"

/* Block signals for model 'LCETCS_vCalFunLamp' */
typedef struct {
  uint8_T lcetcsu8FuncLampOffBrkCnt;   /* '<Root>/LCETCS_vCalFunLampCnt4Brk' */
  uint8_T lcetcsu8FuncLampOffEngCnt;   /* '<Root>/LCETCS_vCalFunLampCnt4Eng' */
} B_LCETCS_vCalFunLamp_c_T;

/* Block states (auto storage) for model 'LCETCS_vCalFunLamp' */
typedef struct {
  boolean_T UnitDelay2_DSTATE;         /* '<Root>/Unit Delay2' */
  MdlrefDW_LCETCS_vCalFunLampCnt4Eng_T LCETCS_vCalFunLampCnt4Eng_DWORK1;/* '<Root>/LCETCS_vCalFunLampCnt4Eng' */
} DW_LCETCS_vCalFunLamp_f_T;

typedef struct {
  B_LCETCS_vCalFunLamp_c_T rtb;
  DW_LCETCS_vCalFunLamp_f_T rtdw;
} MdlrefDW_LCETCS_vCalFunLamp_T;

/* Model reference registration function */
extern void LCETCS_vCalFunLamp_initialize(void);
extern void LCETCS_vCalFunLamp_Init(boolean_T *rty_lcetcsu1FuncLampOn,
  B_LCETCS_vCalFunLamp_c_T *localB, DW_LCETCS_vCalFunLamp_f_T *localDW);
extern void LCETCS_vCalFunLamp(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT,
  const TypeETCSTrq4RdFricStruct *rtu_ETCS_TRQ_REP_RD_FRIC, const
  TypeETCSWhlSpinStruct *rtu_ETCS_WHL_SPIN, const TypeETCSCirStruct *rtu_ETCS_PC,
  const TypeETCSCirStruct *rtu_ETCS_SC, const TypeETCSExtDctStruct
  *rtu_ETCS_EXT_DCT, boolean_T *rty_lcetcsu1FuncLampOn, B_LCETCS_vCalFunLamp_c_T
  *localB, DW_LCETCS_vCalFunLamp_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalFunLamp'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalFunLamp_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
