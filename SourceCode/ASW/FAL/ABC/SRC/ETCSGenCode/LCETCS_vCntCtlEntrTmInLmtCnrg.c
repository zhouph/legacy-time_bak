/*
 * File: LCETCS_vCntCtlEntrTmInLmtCnrg.c
 *
 * Code generated for Simulink model 'LCETCS_vCntCtlEntrTmInLmtCnrg'.
 *
 * Model version                  : 1.177
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:55:02 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCntCtlEntrTmInLmtCnrg.h"
#include "LCETCS_vCntCtlEntrTmInLmtCnrg_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCntCtlEntrTmInLmtCnrg' */
void LCETCS_vCntCtlEntrTmInLmtCnrg_Init(DW_LCETCS_vCntCtlEntrTmInLmtCnrg_f_T
  *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = 0;
}

/* Output and update for referenced model: 'LCETCS_vCntCtlEntrTmInLmtCnrg' */
void LCETCS_vCntCtlEntrTmInLmtCnrg(const TypeETCSWhlSpinStruct
  *rtu_ETCS_WHL_SPIN, const TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN, const
  TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC, const TypeETCSEscSigStruct
  *rtu_ETCS_ESC_SIG, const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT_OLD, int16_T
  *rty_lcetcss16LmtCnrgCtlEntrTm, DW_LCETCS_vCntCtlEntrTmInLmtCnrg_f_T *localDW)
{
  int32_T tmp;

  /* Chart: '<Root>/Chart1' incorporates:
   *  UnitDelay: '<Root>/Unit Delay'
   */
  /* Gateway: Chart1 */
  /* During: Chart1 */
  /* Entry Internal: Chart1 */
  /* Transition: '<S1>:36' */
  if ((rtu_ETCS_CTL_ACT_OLD->lcetcsu1CtlAct == 0) &&
      (rtu_ETCS_ESC_SIG->lcetcss16AbsDelYawFrst >=
       rtu_ETCS_TAR_SPIN->lcetcss16LmtCrngDltYaw)) {
    /* Transition: '<S1>:37' */
    /* Transition: '<S1>:54' */
    if (rtu_ETCS_WHL_SPIN->lcetcss16WhlSpin >=
        rtu_ETCS_TAR_SPIN->lcetcss16TarWhlSpin) {
      /* Transition: '<S1>:56' */
      /* Transition: '<S1>:57' */
      *rty_lcetcss16LmtCnrgCtlEntrTm = ((uint8_T)U8ETCSCpCtlActTmThInLmtCnrg);

      /* Transition: '<S1>:78' */
      /* Transition: '<S1>:79' */
    } else {
      /* Transition: '<S1>:71' */
      if (rtu_ETCS_VEH_ACC->lcetcss16VehLongAcc4Ctl > 0) {
        /* Transition: '<S1>:74' */
        /* Transition: '<S1>:75' */
        tmp = localDW->UnitDelay_DSTATE + 1;
        if (tmp > 32767) {
          tmp = 32767;
        }

        *rty_lcetcss16LmtCnrgCtlEntrTm = (int16_T)tmp;

        /* Transition: '<S1>:79' */
      } else {
        /* Transition: '<S1>:77' */
        tmp = localDW->UnitDelay_DSTATE - 1;
        if (tmp < -32768) {
          tmp = -32768;
        }

        *rty_lcetcss16LmtCnrgCtlEntrTm = (int16_T)tmp;
      }
    }

    /* Transition: '<S1>:80' */
  } else {
    /* Transition: '<S1>:40' */
    *rty_lcetcss16LmtCnrgCtlEntrTm = 0;
  }

  /* End of Chart: '<Root>/Chart1' */

  /* Saturate: '<Root>/Saturation' */
  /* Transition: '<S1>:39' */
  if ((*rty_lcetcss16LmtCnrgCtlEntrTm) > 127) {
    *rty_lcetcss16LmtCnrgCtlEntrTm = 127;
  } else {
    if ((*rty_lcetcss16LmtCnrgCtlEntrTm) < 0) {
      *rty_lcetcss16LmtCnrgCtlEntrTm = 0;
    }
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Update for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = *rty_lcetcss16LmtCnrgCtlEntrTm;
}

/* Model initialize function */
void LCETCS_vCntCtlEntrTmInLmtCnrg_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
