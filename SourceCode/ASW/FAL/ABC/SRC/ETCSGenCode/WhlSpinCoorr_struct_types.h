/*
 * File: WhlSpinCoorr_struct_types.h
 *
 * Code generated for Simulink model 'LCETCS_vCntLowSpinTm'.
 *
 * Model version                  : 1.111
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:04:49 2015
 */

#ifndef RTW_HEADER_WhlSpinCoorr_struct_types_h_
#define RTW_HEADER_WhlSpinCoorr_struct_types_h_
#include "rtwtypes.h"

typedef struct {
  boolean_T lcetcsu14wdLwMd;
  int32_T lcetcss32SecAxlTrqMax;
} TypeETCS4WDSigStruct;

typedef struct {
  int16_T lcetcss16AftLmtCrngCnt;
  boolean_T lcetcsu1DctAftLmtCrng;
  boolean_T lcetcsu1InhAftLmtCrng;
  int16_T lcetcss16InhCntAftLmtCrngSpn;
  int16_T lcetcss16ScaleDnAyMani;
} TypeETCSAftLmtCrng;

typedef struct {
  int16_T lcetcss16FrtAxlAcc;
  int16_T lcetcss16RrAxlAcc;
  int16_T lcetcss16DrvnAxlAcc;
} TypeETCSAxlAccStruct;

typedef struct {
  int16_T lcetcss16EstBrkTrqOnLowMuWhl;
  boolean_T lcetcsu1SymSpinDctFlg;
  boolean_T lcetcsu1HuntingDcted;
} TypeETCSAxlStruct;

typedef struct {
  int16_T lcetcss16SymBrkTrqCtlCmdFA;
  int16_T lcetcss16SymBrkTrqCtlCmdRA;
  int16_T lcetcss16MaxLmtSymBrkTrq;
} TypeETCSBrkCtlCmdStruct;

typedef struct {
  boolean_T lcetcsu1SymBrkTrqCtlReqFA;
  boolean_T lcetcsu1SymBrkTrqCtlReqRA;
} TypeETCSBrkCtlReqStruct;

typedef struct {
  int16_T lcetcss16BtcsTarWhlSpnDiff;
} TypeETCSBrkSigStruct;

typedef struct {
  boolean_T lcetcsu1EstPresLowFlag;
} TypeETCSCirStruct;

typedef struct {
  int32_T lcetcss32RealCrdnTrq;
  int32_T lcetcss32ActCrdnTrq;
  int32_T lcetcss32DrvReqCrdnTrq;
} TypeETCSCrdnTrqStruct;

typedef struct {
  boolean_T lcetcsu1CtlAct;
  boolean_T lcetcsu1CtlActRsgEdg;
  int16_T lcetcss16OvrWhlSpnTm;
  int16_T lcetcss16OvrWhlSpnTmTh;
  int16_T lcetcss16OvrEngTrqCmdTm;
  int16_T lcetcss16LmtCnrgCtlEntrTm;
  boolean_T lcetcsu1DriverAccelIntend;
  boolean_T lcetcsu1DriverAccelHysIntend;
} TypeETCSCtlActStruct;

typedef struct {
  int32_T lcetcss32PCtlPor;
  int32_T lcetcss32ICtlPor;
  int32_T lcetcss32AdptByDelYaw;
  int32_T lcetcss32CtlCmdMax;
  int32_T lcetcss32CardanTrqCmd;
  boolean_T lcetcsu1CtlCmdHitMax;
  boolean_T lcetcsu1CtlCmdInc;
  int32_T lcetcss32AdptByWhlSpn;
  int32_T lcetcss32AdptAmt;
} TypeETCSCtlCmdStruct;

typedef struct {
  int16_T lcetcss16CtlErr;
  boolean_T lcetcsu1CtlErrPos;
  boolean_T lcetcsu1CtlErrNeg;
  boolean_T lcetcsu1RsgEdgOfCtlErrNeg;
  boolean_T lcetcsu1RsgEdgOfCtlErrPos;
} TypeETCSCtlErrStruct;

typedef struct {
  uint8_T lcetcsu8EngCtrlMode;
  uint8_T lcetcsu8BrkCtrlMode;
} TypeETCSCtrlModeStruct;

typedef struct {
  boolean_T lcetcsu1Cyl1st;
  boolean_T lcetcsu1FalEdgOfCyl1st;
  boolean_T lcetcsu1RsgEdgOfCyl1st;
  int16_T lcetcss16Cyl1stTm;
} TypeETCSCyl1stStruct;

typedef struct {
  boolean_T lcetcsu1DepSnwHilAct;
  int16_T lcetcss16DepSnwHilCnt;
} TypeETCSDepSnwStruct;

typedef struct {
  int16_T lcetcss16TotalGearRatio;
  int16_T lcetcss16TrqRatio;
  int16_T lcetcss16TransEfficiency;
  int16_T lcetcss16TotalGearRatioRaw;
  int16_T lcetcss16TransEfficiencyRaw;
} TypeETCSDrvMdlStruct;

typedef struct {
  int16_T lcetcss16EngTrqCmd;
  int16_T lcetcss16EngTrqCmdPPor;
  int16_T lcetcss16EngTrqCmdIPor;
} TypeETCSEngCmdStruct;

typedef struct {
  int16_T lcetcss16ActEngTrqFltd;
  int16_T lcetcss16ActEngTrqDeld;
} TypeETCSEngMdlStruct;

typedef struct {
  boolean_T lcetcsu1EngStall;
  boolean_T lcetcsu1EngStallTemp;
  int16_T lcetcss16EngStallThr;
  int8_T lcetcss8EngStallRiskIndex;
  int16_T lcetcss16EngSpdSlope;
} TypeETCSEngStallStruct;

typedef struct {
  int16_T lcetcss16DrvReqTrq;
  int16_T lcetcss16ActEngTrq;
  int16_T lcetcss16EngSpd;
  int16_T lctcss16AccPdlPos;
} TypeETCSEngStruct;

typedef struct {
  boolean_T lcetcsu1ErrZroCrs;
  boolean_T lcetcsu1JumpUp;
  boolean_T lcetcsu1JumpUpFalEdg;
  boolean_T lcetcsu1JumpDn;
  boolean_T lcetcsu1JumpDnFalEdg;
} TypeETCSErrZroCrsStruct;

typedef struct {
  int16_T lcetcss16StrWhAng;
  int16_T lcetcss16StrWhAngGrd;
  int16_T lcetcss16YawRateMsrd;
  int16_T lcetcss16LatAccMsrd;
  int16_T lcetcss16LonAcc;
  int16_T lcetcss16DelYawFrst;
  int16_T lcetcss16AbsDelYawFrst;
  int16_T lcetcss16YawRateCald;
  int16_T lcetcss16LatAccCald;
  int16_T lcetcss16LatAccCaldMani;
  int16_T lcetcss16LatAccMsrdMani;
  boolean_T lcetcsu1ESCBrkCtl;
  boolean_T lcetcsu1BrkPdlPrsed;
  int16_T lcetcss16AbsStrGrdMani;
} TypeETCSEscSigStruct;

typedef struct {
  boolean_T lcetcsu1RoughRd;
  boolean_T lcetcsu1BankRd;
  boolean_T lcetcsu1AbnrmSizTire;
  boolean_T lcetcsu1AutoTrans;
  boolean_T lcetcsu1DctHilByAx;
  uint8_T lcetcsu8AbnrmTireRatio;
  boolean_T lcetcsu1HsaAtv;
  uint8_T lcetcsu8SccCtlAct;
  boolean_T lcetcsu1EscDisabledBySW;
  boolean_T lcetcsu1TcsDisabledBySW;
  boolean_T lcetcsu1BTCSCtlAct;
} TypeETCSExtDctStruct;

typedef struct {
  boolean_T lcetcsu1GainChngInGearShft;
  boolean_T lcetcsu1GainTrnsAftrGearShft;
  uint8_T lcetcsu8GainTrnsTm;
} TypeETCSGainGearShftStruct;

typedef struct {
  int16_T lcetcss16Pgain;
  int16_T lcetcss16Igain;
  int16_T lcetcss16BsPgain;
  int16_T lcetcss16BsIgain;
  int16_T lcetcss16PgainFacInGearChng;
  int16_T lcetcss16IgainFacInGearChng;
  int16_T lcetcss16PGainIncFacH2L;
  int16_T lcetcss16IGainIncFacH2L;
  int16_T lcetcss16HomoBsPgain;
  int16_T lcetcss16SpltBsPgain;
  int16_T lcetcss16HomoBsIgain;
  int16_T lcetcss16SpltBsIgain;
  int16_T lcetcss16PGainFacInTrn;
  int16_T lcetcss16IGainFacInTrn;
  int16_T lcetcss16PGainFacInDrvVib;
  int8_T lcetcss8PgainFacInDrvVibCnt;
  int16_T lcetcss16PgainFac;
  int16_T lcetcss16IgainFac;
  int16_T lcetcss16HmIGainFctAftrTrn;
  int16_T lcetcss16HmIGainFct4BigSpn;
  int16_T lcetcss16DelYawGain;
  int16_T lcetcss16DelYawDivrgCnt;
  boolean_T lcetcsu1DelYawDivrg;
  int16_T lcetcss16DelYawDivrgGain;
  int16_T lcetcss16DelYawCnvrgGain;
} TypeETCSGainStruct;

typedef struct {
  int16_T lcetcss16CurntGearStep;
  boolean_T lcetcsu1GearShifting;
  uint8_T lcetcsu8DrvGearSel;
} TypeETCSGearStruct;

typedef struct {
  int16_T lcetcss16DeltaLonAcc;
  boolean_T lcetcsu1HiToLw;
  boolean_T lcetcsu1HiToLwRsgEdg;
  int8_T lcetcss8HiToLwHoldCnt;
  int16_T lcetcss16HiToLwResetTh;
} TypeETCSH2LStruct;

typedef struct {
  int16_T lcetcss16LwToHiCnt;
  int16_T lcetcss16LwToHiCntbyArea;
  int16_T lcetcss16AdaptHighRdCnt;
  int16_T lcetcss16PreHighRdDctCnt;
} TypeETCSHighDctStruct;

typedef struct {
  int32_T lcetcss32IValAtStrtOfCtl;
  int32_T lcetcss32IValAt2ndCylStrt;
  boolean_T lcetcsu1RstIValAt2ndCylStrt;
  int32_T lcetcss32IValAtErrZroCrs;
  boolean_T lcetcsu1RstIValAtErrZroCrs;
  int32_T lcetcss32IVal2Rst;
  boolean_T lcetcsu1RstIVal;
  boolean_T lcetcsu1IValRecTorqAct;
  int32_T lcetcss32IValRecTorq;
  boolean_T lcetcsu1IValVcaTorqAct;
  int32_T lcetcss32IValVcaTorq;
} TypeETCSIValRstStruct;

typedef struct {
  int32_T lcetcss32LdTrqByAsymBrkCtl;
  int32_T lcetcss32LdTrqDecBySymBrkCtl;
  int16_T lcetcss16AxAtHillDctd;
  int16_T lcetcss16LdTrqByGrdRaw;
  int16_T lcetcss16LdTrqByGrd;
  int32_T lcetcss32LdTrq;
} TypeETCSLdTrqStruct;

typedef struct {
  boolean_T lcetcsu1LowWhlSpnCntReset;
  int16_T lcetcss16LowWhlSpnCnt;
  boolean_T lcetcsu1LowWhlSpnCntInc;
} TypeETCSLowWhlSpnStruct;

typedef struct {
  int16_T lcetcss16LwToSplitCnt;
  int16_T lcetcss16LowMuWhlSpinNF;
  int16_T lcetcss16HighMuWhlSpinNF;
  boolean_T lcetcsu1DctWhlOnHighmu;
  boolean_T lcetcsu1DctLowHm;
  int16_T lcetcss16LowHmHldCnt;
} TypeETCSLwToSpltStruct;

typedef struct {
  int32_T lcetcss32RefTrq2Jmp;
  boolean_T lcetcsu1RefTrq4JmpSetOk;
  int16_T lcetcss16RefTrqSetTm;
  int16_T lcetcss16Ftr4JmpDn;
  int16_T lcetcss16Ftr4JmpUp;
} TypeETCSRefTrq2JmpStruct;

typedef struct {
  int16_T lcetcss16VehSpdAtStrtOf1stCyl;
  int16_T lcetcss16VehAccAvgIn1stCyl;
  int32_T lcetcss32EstTrqByVehAccIn1StCyl;
  int16_T lcetcss16Ftr4RefTrqAt2ndCylStrt;
} TypeETCSRefTrqStr2CylStruct;

typedef struct {
  int32_T lcetcss32EstTrqByVehAccBfrCtl;
} TypeETCSRefTrqStrCtlStruct;

typedef struct {
  boolean_T lcetcsu1ReqVcaEngTrqAct;
  boolean_T lcetcsu1ReqVcaEngTrqRsgEdg;
  boolean_T lcetcsu1ReqVcaBrkTrqAct;
  boolean_T lcetcsu1ReqVcaBrkTrqRsgEdg;
  boolean_T lcetcsu1ReqVcaTrqAct;
  int16_T lcetcss16VehAccErrCount;
} TypeETCSReqVCAStruct;

typedef struct {
  boolean_T lcetcsu1SplitHillDct;
  boolean_T lcetcsu1HillDctCompbyAx;
  int16_T lcetcss16LongAccFilter;
  boolean_T lcetcsu1HillbyHsaAtv;
  boolean_T lcetcsu1SplitHillDctCndSet;
  boolean_T lcetcsu1SplitHillClrCndSet;
  boolean_T lcetcsu1SplitHillClrDecCndSet;
  int16_T lcetcss16SplitHillDctCnt;
  int16_T lcetcss16DistVehMovRd;
  boolean_T lcetcsu1HillbyHsaSustain;
  uint8_T lcetcsu8HillDctCntbyAx;
  int16_T lcetcss16GradientTrstCnt;
  int16_T lcetcss16GradientOfHill;
  boolean_T lcetcsu1InhibitGradient;
  boolean_T lcetcsu1DctHoldGradient;
  int16_T lcetcss16HoldGradientCnt;
  int16_T lcetcss16HoldRefGradientTime;
  boolean_T lcetcsu1GradientTrstSet;
} TypeETCSSplitHillStruct;

typedef struct {
  int16_T lcetcss16StrCnvrgCnt;
  boolean_T lcetcsu1StrPos;
  int16_T lcetcss16StrDivrgCnt;
} TypeETCSStrStatStruct;

typedef struct {
  int16_T lcetcss16TarWhlSpin;
  int16_T lcetcss16BsTarWhlSpin;
  int16_T lcetcss16TarWhlSpinInc;
  int16_T lcetcss16TarWhlSpdStlDetd;
  int16_T lcetcss16AddTar4DepSnw;
  int16_T lcetcss16LmtCrngDltYaw;
  int16_T lcetcss16TarDecInTrn;
  int16_T lcetcss16DltYawStbCnt;
  boolean_T lcetcsu1TarSpnDec;
  int16_T lcetcss16TarSpnDecCnt;
  int16_T lcetcss16MaxDltYawInTarDecMd;
  int16_T lcetcss16TarDecDpndDelYaw;
  int16_T lcetcss16FnlTarSpnDec;
  int16_T lcetcss16TarWhlSpnDecVib;
  boolean_T lcetcsu1AbnrmSizeTire;
  int16_T lcetcss16TarWhlSpnIncAbnrmTire;
  int16_T lcetcss16TarWhlSpinIncLwToSplt;
  int16_T lcetcss16CordBsTarWhlSpin;
  boolean_T lcetcsu1DctLmtCrng;
  boolean_T lcetcsu1DctLmtCrngFalEdg;
} TypeETCSTarSpinStruct;

typedef struct {
  int32_T lcetcss32TrqRepRdFric;
  int32_T lcetcss32AppTrq4RdFric;
  int32_T lcetcss32LesTrq4RdFric;
  int32_T lcetcss32TrqAtLowSpnDcted;
} TypeETCSTrq4RdFricStruct;

typedef struct {
  int16_T lcetcss16VehAccByVehSpd;
  int16_T lcetcss16VehLongAcc4Ctl;
  int16_T lcetcss16RsltntAcc;
  int16_T lcetcss16ScaleDnAy;
  int16_T lcetcss16RefRdCfFricAtIce;
  int16_T lcetcss16RefRdCfFricAtSnow;
  int16_T lcetcss16RefRdCfFricAtAsphalt;
  int16_T lcetcss16RefAxRdFricAtIce;
  int16_T lcetcss16RefAxRdFricAtSnow;
  int16_T lcetcss16RefAxRdFricAtAsphalt;
} TypeETCSVehAccStruct;

typedef struct {
  int16_T lcetcss16FrtLefWhlAcc;
  int16_T lcetcss16FrtRigWhlAcc;
  int16_T lcetcss16RrLefWhlAcc;
  int16_T lcetcss16RrRigWhlAcc;
} TypeETCSWhlAccStruct;

typedef struct {
  int16_T lcetcss16WhlSpin;
  int16_T lcetcss16WhlSpinFrtAxl;
  int16_T lcetcss16WhlSpinRrAxl;
  int16_T lcetcss16WhlSpinNonFilt;
  int16_T lcetcss16WhlSpinFrtAxlNonFilt;
  int16_T lcetcss16WhlSpinRrAxlNonFilt;
} TypeETCSWhlSpinStruct;

typedef struct {
  int16_T lcetcss16WhlSpdCrt;
  int16_T lcetcss16WhlDelSpnRughRd;
  int16_T lcetcss16MovigAvgWhlSpd;
  boolean_T lcetcsu1BTCSWhlAtv;
  boolean_T lcetcsu1WhlVibDct;
  int16_T lcetcss16WhlSpinNF;
  boolean_T lcetcsu1AbnrmTireSuspect;
  boolean_T lcetcsu1AbnrmTireBrkSuspect;
} TypeETCSWhlStruct;

typedef struct {
  int16_T lctmcs16TarUpShtVS;
  int16_T lctmcs16TarUpShtRPM;
  int16_T lctmcs16TarDnShtVS;
  int16_T lctmcs16TarDnShtRPM;
} TypeTMCRefFinTarGearMap;

typedef struct {
  int16_T lctmcs16TarUpShtMaxVSbyGear;
  int16_T lctmcs16TarDnShtMinVSbyGear;
} TypeTMCRefLimVehSpdMap;

typedef struct {
  int16_T lctmcs16TarUpShtVSbyGear;
  int16_T lctmcs16TarUpShtRPMbyGear;
  int16_T lctmcs16TarDnShtVSbyGear;
  int16_T lctmcs16TarDnShtRPMbyGear;
} TypeTMCRefTarGearMap;

typedef struct {
  uint8_T lctmcu8_BSTGRReqType;
  uint8_T lctmcu8_BSTGRReqGear;
} TypeTMCReqGearStruct;

#endif                                 /* RTW_HEADER_WhlSpinCoorr_struct_types_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
