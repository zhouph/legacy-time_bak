/*
 * File: LCETCS_vCalEngStallRiskIndex.c
 *
 * Code generated for Simulink model 'LCETCS_vCalEngStallRiskIndex'.
 *
 * Model version                  : 1.263
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:05:45 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalEngStallRiskIndex.h"
#include "LCETCS_vCalEngStallRiskIndex_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalEngStallRiskIndex' */
void LCETCS_vCalEngStallRiskIndex_Init(DW_LCETCS_vCalEngStallRiskIndex_f_T
  *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = 0;
}

/* Disable for referenced model: 'LCETCS_vCalEngStallRiskIndex' */
void LCETCS_vCalEngStallRiskIndex_Disable(DW_LCETCS_vCalEngStallRiskIndex_f_T
  *localDW)
{
  /* Disable for If: '<Root>/If' */
  localDW->If_ActiveSubsystem = -1;
}

/* Start for referenced model: 'LCETCS_vCalEngStallRiskIndex' */
void LCETCS_vCalEngStallRiskIndex_Start(B_LCETCS_vCalEngStallRiskIndex_c_T
  *localB, DW_LCETCS_vCalEngStallRiskIndex_f_T *localDW)
{
  /* Start for If: '<Root>/If' */
  localDW->If_ActiveSubsystem = -1;

  /* Start for IfAction SubSystem: '<Root>/Engine Stall Risk Index in ETCS Control' */
  /* Start for Constant: '<S1>/Constant1' */
  localB->Constant1 = ((uint8_T)VREF_6_KPH);

  /* Start for Constant: '<S1>/Constant2' */
  localB->Constant2 = ((uint8_T)VREF_10_KPH);

  /* Start for Constant: '<S1>/Constant3' */
  localB->Constant3 = ((uint8_T)VREF_12_5_KPH);

  /* End of Start for SubSystem: '<Root>/Engine Stall Risk Index in ETCS Control' */

  /* InitializeConditions for IfAction SubSystem: '<Root>/Engine Stall Risk Index in ETCS Control' */

  /* InitializeConditions for ModelReference: '<S1>/LCETCS_s16Inter3Point' */
  LCETCS_s16Inter3Point_Init(&localB->y);

  /* InitializeConditions for ModelReference: '<S1>/LCETCS_s16Inter3Point1' */
  LCETCS_s16Inter3Point_Init(&localB->y_n);

  /* End of InitializeConditions for SubSystem: '<Root>/Engine Stall Risk Index in ETCS Control' */
}

/* Output and update for referenced model: 'LCETCS_vCalEngStallRiskIndex' */
void LCETCS_vCalEngStallRiskIndex(const TypeETCSCtlActStruct
  *rtu_ETCS_CTL_ACT_OLD, const TypeETCSEngStallStruct *rtu_ETCS_ENG_STALL_OLD,
  int16_T rtu_lcetcss16VehSpd, int16_T rtu_lcetcss16EngSpdSlope, int8_T
  *rty_lcetcss8EngStallRiskIndex, B_LCETCS_vCalEngStallRiskIndex_c_T *localB,
  DW_LCETCS_vCalEngStallRiskIndex_f_T *localDW)
{
  int8_T rtPrevAction;
  int32_T u0;

  /* If: '<Root>/If' incorporates:
   *  Constant: '<Root>/Constat1'
   *  Constant: '<S1>/Changed RPM -40//20ms'
   *  Constant: '<S1>/Changed RPM-10//20ms'
   *  Constant: '<S1>/Changed RPM-20//20ms'
   *  Constant: '<S1>/Constant1'
   *  Constant: '<S1>/Constant2'
   *  Constant: '<S1>/Constant3'
   *  Constant: '<S1>/High Engine Stall Risk by Engine Speed'
   *  Constant: '<S1>/High Engine Stall Risk by Vehicle Speed'
   *  Constant: '<S1>/Low Engine Stall Risk by Engine Speed'
   *  Constant: '<S1>/Low Engine Stall Risk by Vehicle Speed'
   *  Constant: '<S1>/Mid Engine Stall Risk by Engine Speed'
   *  Constant: '<S1>/Mid Engine Stall Risk by Vehicle Speed'
   *  Inport: '<S2>/lcetcss8EngStallRiskIndexOld'
   *  Logic: '<Root>/Logical Operator'
   *  RelationalOperator: '<Root>/Relational Operator'
   *  UnitDelay: '<Root>/Unit Delay'
   */
  rtPrevAction = localDW->If_ActiveSubsystem;
  if ((rtu_ETCS_CTL_ACT_OLD->lcetcsu1CtlAct) &&
      (!rtu_ETCS_ENG_STALL_OLD->lcetcsu1EngStall)) {
    localDW->If_ActiveSubsystem = 0;
  } else {
    localDW->If_ActiveSubsystem = 1;
  }

  switch (localDW->If_ActiveSubsystem) {
   case 0:
    if (localDW->If_ActiveSubsystem != rtPrevAction) {
      /* InitializeConditions for IfAction SubSystem: '<Root>/Engine Stall Risk Index in ETCS Control' incorporates:
       *  InitializeConditions for ActionPort: '<S1>/Action Port'
       */

      /* InitializeConditions for ModelReference: '<S1>/LCETCS_s16Inter3Point' */
      LCETCS_s16Inter3Point_Init(&localB->y);

      /* InitializeConditions for ModelReference: '<S1>/LCETCS_s16Inter3Point1' */
      LCETCS_s16Inter3Point_Init(&localB->y_n);

      /* End of InitializeConditions for SubSystem: '<Root>/Engine Stall Risk Index in ETCS Control' */
    }

    /* Outputs for IfAction SubSystem: '<Root>/Engine Stall Risk Index in ETCS Control' incorporates:
     *  ActionPort: '<S1>/Action Port'
     */
    localB->Constant1 = ((uint8_T)VREF_6_KPH);
    localB->Constant2 = ((uint8_T)VREF_10_KPH);
    localB->Constant3 = ((uint8_T)VREF_12_5_KPH);

    /* ModelReference: '<S1>/LCETCS_s16Inter3Point' */
    LCETCS_s16Inter3Point(rtu_lcetcss16VehSpd, localB->Constant1,
                          localB->Constant2, localB->Constant3,
                          rtCP_HighEngineStallRiskbyVehicleSpeed_Value,
                          rtCP_MidEngineStallRiskbyVehicleSpeed_Value,
                          rtCP_LowEngineStallRiskbyVehicleSpeed_Value,
                          &localB->y);

    /* ModelReference: '<S1>/LCETCS_s16Inter3Point1' */
    LCETCS_s16Inter3Point(rtu_lcetcss16EngSpdSlope, rtCP_ChangedRPM4020ms_Value,
                          rtCP_ChangedRPM2020ms_Value,
                          rtCP_ChangedRPM1020ms_Value,
                          rtCP_HighEngineStallRiskbyEngineSpeed_Value,
                          rtCP_MidEngineStallRiskbyEngineSpeed_Value,
                          rtCP_LowEngineStallRiskbyEngineSpeed_Value,
                          &localB->y_n);

    /* Sum: '<S1>/Add' incorporates:
     *  Constant: '<S1>/Changed RPM -40//20ms'
     *  Constant: '<S1>/Changed RPM-10//20ms'
     *  Constant: '<S1>/Changed RPM-20//20ms'
     *  Constant: '<S1>/Constant1'
     *  Constant: '<S1>/Constant2'
     *  Constant: '<S1>/Constant3'
     *  Constant: '<S1>/High Engine Stall Risk by Engine Speed'
     *  Constant: '<S1>/High Engine Stall Risk by Vehicle Speed'
     *  Constant: '<S1>/Low Engine Stall Risk by Engine Speed'
     *  Constant: '<S1>/Low Engine Stall Risk by Vehicle Speed'
     *  Constant: '<S1>/Mid Engine Stall Risk by Engine Speed'
     *  Constant: '<S1>/Mid Engine Stall Risk by Vehicle Speed'
     */
    u0 = localB->y + localB->y_n;

    /* Saturate: '<S1>/Saturation' */
    if (u0 > 4) {
      /* DataTypeConversion: '<S1>/Data Type Conversion2' */
      *rty_lcetcss8EngStallRiskIndex = 4;
    } else if (u0 < -2) {
      /* DataTypeConversion: '<S1>/Data Type Conversion2' */
      *rty_lcetcss8EngStallRiskIndex = -2;
    } else {
      /* DataTypeConversion: '<S1>/Data Type Conversion2' */
      *rty_lcetcss8EngStallRiskIndex = (int8_T)u0;
    }

    /* End of Saturate: '<S1>/Saturation' */
    /* End of Outputs for SubSystem: '<Root>/Engine Stall Risk Index in ETCS Control' */
    break;

   case 1:
    /* Outputs for IfAction SubSystem: '<Root>/Engine Stall Risk Index in ETCS Non Control' incorporates:
     *  ActionPort: '<S2>/Action Port'
     */
    *rty_lcetcss8EngStallRiskIndex = localDW->UnitDelay_DSTATE;

    /* End of Outputs for SubSystem: '<Root>/Engine Stall Risk Index in ETCS Non Control' */
    break;
  }

  /* End of If: '<Root>/If' */

  /* Update for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = *rty_lcetcss8EngStallRiskIndex;
}

/* Model initialize function */
void LCETCS_vCalEngStallRiskIndex_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<S1>/LCETCS_s16Inter3Point' */
  LCETCS_s16Inter3Point_initialize();

  /* Model Initialize fcn for ModelReference Block: '<S1>/LCETCS_s16Inter3Point1' */
  LCETCS_s16Inter3Point_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
