/*
 * File: LCETCS_vCalTarWhlSpinInc.c
 *
 * Code generated for Simulink model 'LCETCS_vCalTarWhlSpinInc'.
 *
 * Model version                  : 1.88
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:49:27 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalTarWhlSpinInc.h"
#include "LCETCS_vCalTarWhlSpinInc_private.h"

/* Output and update for referenced model: 'LCETCS_vCalTarWhlSpinInc' */
void LCETCS_vCalTarWhlSpinInc(boolean_T rtu_lcetcsu1AbnrmSizeTire, int16_T
  rtu_lcetcss16TarWhlSpnIncAbnrmTire, const TypeETCSExtDctStruct
  *rtu_ETCS_EXT_DCT, int16_T rtu_lcetcss16TarWhlSpnIncRughRd, int16_T
  rtu_lcetcss16AddTar4DepSnw, uint8_T rtu_lcetcsu8AddTar4SpHill, uint8_T
  rtu_lcetcsu8AddTar4HighRd, int16_T rtu_lcetcss16AddTar4CtrlMode, int16_T
  *rty_lcetcss16TarWhlSpinInc)
{
  int16_T rtu_ETCS_EXT_DCT_0;
  int16_T rtu_lcetcsu1AbnrmSizeTire_0;

  /* Switch: '<Root>/Switch' incorporates:
   *  Constant: '<Root>/Constant1'
   */
  if (rtu_ETCS_EXT_DCT->lcetcsu1RoughRd) {
    rtu_ETCS_EXT_DCT_0 = rtu_lcetcss16TarWhlSpnIncRughRd;
  } else {
    rtu_ETCS_EXT_DCT_0 = 0;
  }

  /* End of Switch: '<Root>/Switch' */

  /* Switch: '<Root>/Switch1' incorporates:
   *  Constant: '<Root>/Constant3'
   */
  if (rtu_lcetcsu1AbnrmSizeTire) {
    rtu_lcetcsu1AbnrmSizeTire_0 = rtu_lcetcss16TarWhlSpnIncAbnrmTire;
  } else {
    rtu_lcetcsu1AbnrmSizeTire_0 = 0;
  }

  /* End of Switch: '<Root>/Switch1' */

  /* MinMax: '<Root>/MinMax' */
  if (rtu_ETCS_EXT_DCT_0 >= rtu_lcetcsu1AbnrmSizeTire_0) {
    rtu_lcetcsu1AbnrmSizeTire_0 = rtu_ETCS_EXT_DCT_0;
  }

  /* MinMax: '<Root>/MinMax1' incorporates:
   *  DataTypeConversion: '<Root>/Data Type Conversion1'
   */
  if (rtu_lcetcsu8AddTar4SpHill >= rtu_lcetcss16AddTar4DepSnw) {
    rtu_ETCS_EXT_DCT_0 = rtu_lcetcsu8AddTar4SpHill;
  } else {
    rtu_ETCS_EXT_DCT_0 = rtu_lcetcss16AddTar4DepSnw;
  }

  /* End of MinMax: '<Root>/MinMax1' */

  /* MinMax: '<Root>/MinMax' incorporates:
   *  DataTypeConversion: '<Root>/Data Type Conversion'
   */
  if (rtu_lcetcsu1AbnrmSizeTire_0 >= rtu_ETCS_EXT_DCT_0) {
    rtu_ETCS_EXT_DCT_0 = rtu_lcetcsu1AbnrmSizeTire_0;
  }

  if (rtu_ETCS_EXT_DCT_0 >= rtu_lcetcsu8AddTar4HighRd) {
  } else {
    rtu_ETCS_EXT_DCT_0 = rtu_lcetcsu8AddTar4HighRd;
  }

  /* Saturate: '<Root>/Saturation' incorporates:
   *  MinMax: '<Root>/MinMax'
   *  Sum: '<Root>/Add'
   */
  *rty_lcetcss16TarWhlSpinInc = (int16_T)(rtu_ETCS_EXT_DCT_0 +
    rtu_lcetcss16AddTar4CtrlMode);
  if ((*rty_lcetcss16TarWhlSpinInc) > 255) {
    *rty_lcetcss16TarWhlSpinInc = 255;
  }

  /* End of Saturate: '<Root>/Saturation' */
}

/* Model initialize function */
void LCETCS_vCalTarWhlSpinInc_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
