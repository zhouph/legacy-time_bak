/*
 * File: LCETCS_vCalTarWhlSpdStlDetd.c
 *
 * Code generated for Simulink model 'LCETCS_vCalTarWhlSpdStlDetd'.
 *
 * Model version                  : 1.88
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:49:12 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalTarWhlSpdStlDetd.h"
#include "LCETCS_vCalTarWhlSpdStlDetd_private.h"

/* Output and update for referenced model: 'LCETCS_vCalTarWhlSpdStlDetd' */
void LCETCS_vCalTarWhlSpdStlDetd(const TypeETCSEngStallStruct
  *rtu_ETCS_ENG_STALL_OLD, const TypeETCSDrvMdlStruct *rtu_ETCS_DRV_MDL, const
  TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT_OLD, int16_T
  *rty_lcetcss16TarWhlSpdStlDetd)
{
  int32_T u0;
  int16_T rtu_ETCS_DRV_MDL_0;

  /* Switch: '<Root>/Switch' incorporates:
   *  Constant: '<Root>/Constant4'
   *  Logic: '<Root>/Logical Operator'
   */
  if ((rtu_ETCS_ENG_STALL_OLD->lcetcsu1EngStall) &&
      (rtu_ETCS_CTL_ACT_OLD->lcetcsu1CtlAct)) {
    /* Saturate: '<Root>/Saturation' */
    if (rtu_ETCS_DRV_MDL->lcetcss16TotalGearRatio > 10000) {
      rtu_ETCS_DRV_MDL_0 = 10000;
    } else if (rtu_ETCS_DRV_MDL->lcetcss16TotalGearRatio < 1) {
      rtu_ETCS_DRV_MDL_0 = 1;
    } else {
      rtu_ETCS_DRV_MDL_0 = rtu_ETCS_DRV_MDL->lcetcss16TotalGearRatio;
    }

    /* Product: '<Root>/Divide1' incorporates:
     *  Constant: '<Root>/Constant'
     *  Constant: '<Root>/Constant1'
     *  Product: '<Root>/Divide'
     *  Product: '<Root>/Product'
     *  Product: '<Root>/Product1'
     *  Saturate: '<Root>/Saturation'
     */
    u0 = (((((int16_T)S16ETCSCpTrgtEngSpdStlDetd) * ((int16_T)
             S16ETCSCpCircumferenceOfTire)) / rtu_ETCS_DRV_MDL_0) * 480) / 10000;

    /* Saturate: '<Root>/Saturation1' */
    if (u0 > 255) {
      *rty_lcetcss16TarWhlSpdStlDetd = 255;
    } else if (u0 < 0) {
      *rty_lcetcss16TarWhlSpdStlDetd = 0;
    } else {
      *rty_lcetcss16TarWhlSpdStlDetd = (int16_T)u0;
    }

    /* End of Saturate: '<Root>/Saturation1' */
  } else {
    *rty_lcetcss16TarWhlSpdStlDetd = 0;
  }

  /* End of Switch: '<Root>/Switch' */
}

/* Model initialize function */
void LCETCS_vCalTarWhlSpdStlDetd_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
