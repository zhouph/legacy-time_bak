/*
 * File: LCETCS_vCalBsTarWhlSpin.c
 *
 * Code generated for Simulink model 'LCETCS_vCalBsTarWhlSpin'.
 *
 * Model version                  : 1.42
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:59:29 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalBsTarWhlSpin.h"
#include "LCETCS_vCalBsTarWhlSpin_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalBsTarWhlSpin' */
void LCETCS_vCalBsTarWhlSpin_Init(B_LCETCS_vCalBsTarWhlSpin_c_T *localB)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter5Point' */
  LCETCS_s16Inter5Point_Init(&localB->y1_p);

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_Init(&localB->lcetcss16BsTarWhlSpin);
}

/* Start for referenced model: 'LCETCS_vCalBsTarWhlSpin' */
void LCETCS_vCalBsTarWhlSpin_Start(B_LCETCS_vCalBsTarWhlSpin_c_T *localB)
{
  /* Start for Constant: '<Root>/Target Wheel Spin at Speed 1' */
  localB->y1 = ((uint8_T)U8ETCSCpTarWhlSpin_1);

  /* Start for Constant: '<Root>/Target Wheel Spin at Speed 2' */
  localB->y2 = ((uint8_T)U8ETCSCpTarWhlSpin_2);

  /* Start for Constant: '<Root>/Target Wheel Spin at Speed 3' */
  localB->y3 = ((uint8_T)U8ETCSCpTarWhlSpin_3);

  /* Start for Constant: '<Root>/Target Wheel Spin at Speed 4' */
  localB->y4 = ((uint8_T)U8ETCSCpTarWhlSpin_4);

  /* Start for Constant: '<Root>/Target Wheel Spin at Speed 5' */
  localB->y5 = ((uint8_T)U8ETCSCpTarWhlSpin_5);
}

/* Output and update for referenced model: 'LCETCS_vCalBsTarWhlSpin' */
void LCETCS_vCalBsTarWhlSpin(const TypeETCSBrkSigStruct *rtu_ETCS_BRK_SIG, const
  TypeETCSAxlStruct *rtu_ETCS_FA, const TypeETCSAxlStruct *rtu_ETCS_RA, int16_T
  rtu_lcetcss16VehSpd, int16_T *rty_lcetcss16BsTarWhlSpin,
  B_LCETCS_vCalBsTarWhlSpin_c_T *localB)
{
  /* local block i/o variables */
  int16_T rtb_x;
  int16_T rtb_lcetcss16TarWhlSpnSp;
  int16_T rtb_x1;
  int16_T rtb_lcetcss16BsTarWhlSpin_d;

  /* Product: '<Root>/Divide' */
  rtb_lcetcss16TarWhlSpnSp = (int16_T)
    (rtu_ETCS_BRK_SIG->lcetcss16BtcsTarWhlSpnDiff / 2);

  /* Constant: '<Root>/Constant1' */
  rtb_x1 = ((uint8_T)U8ETCSCpAddTarWhlSpnSp);

  /* Sum: '<Root>/Add' */
  rtb_lcetcss16TarWhlSpnSp += rtb_x1;

  /* Constant: '<Root>/Constant2' */
  rtb_x1 = ((uint8_T)U8ETCSCpTarTransStBrkTrq2Sp);

  /* Constant: '<Root>/Constant3' */
  rtb_lcetcss16BsTarWhlSpin_d = ((uint8_T)U8ETCSCpTarTransCplBrkTrq2Sp);

  /* Gain: '<Root>/Gain' */
  rtb_x1 = (int16_T)(10 * rtb_x1);

  /* Gain: '<Root>/Gain1' */
  rtb_lcetcss16BsTarWhlSpin_d = (int16_T)(10 * rtb_lcetcss16BsTarWhlSpin_d);

  /* MinMax: '<Root>/MinMax' */
  if (rtu_ETCS_FA->lcetcss16EstBrkTrqOnLowMuWhl >=
      rtu_ETCS_RA->lcetcss16EstBrkTrqOnLowMuWhl) {
    rtb_x = rtu_ETCS_FA->lcetcss16EstBrkTrqOnLowMuWhl;
  } else {
    rtb_x = rtu_ETCS_RA->lcetcss16EstBrkTrqOnLowMuWhl;
  }

  /* End of MinMax: '<Root>/MinMax' */

  /* Constant: '<Root>/Target Wheel Spin at Speed 1' */
  localB->y1 = ((uint8_T)U8ETCSCpTarWhlSpin_1);

  /* Constant: '<Root>/Target Wheel Spin at Speed 2' */
  localB->y2 = ((uint8_T)U8ETCSCpTarWhlSpin_2);

  /* Constant: '<Root>/Target Wheel Spin at Speed 3' */
  localB->y3 = ((uint8_T)U8ETCSCpTarWhlSpin_3);

  /* Constant: '<Root>/Target Wheel Spin at Speed 4' */
  localB->y4 = ((uint8_T)U8ETCSCpTarWhlSpin_4);

  /* Constant: '<Root>/Target Wheel Spin at Speed 5' */
  localB->y5 = ((uint8_T)U8ETCSCpTarWhlSpin_5);

  /* ModelReference: '<Root>/LCETCS_s16Inter5Point' */
  LCETCS_s16Inter5Point(rtu_lcetcss16VehSpd, ((int16_T)S16ETCSCpSpd_1),
                        ((int16_T)S16ETCSCpSpd_2), ((int16_T)S16ETCSCpSpd_3),
                        ((int16_T)S16ETCSCpSpd_4), ((int16_T)S16ETCSCpSpd_5),
                        localB->y1, localB->y2, localB->y3, localB->y4,
                        localB->y5, &localB->y1_p);

  /* ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point(rtb_x, rtb_x1, rtb_lcetcss16BsTarWhlSpin_d, localB->y1_p,
                        rtb_lcetcss16TarWhlSpnSp, &localB->lcetcss16BsTarWhlSpin);

  /* MinMax: '<Root>/MinMax1' */
  if (localB->lcetcss16BsTarWhlSpin >= localB->y1_p) {
    rtb_lcetcss16BsTarWhlSpin_d = localB->lcetcss16BsTarWhlSpin;
  } else {
    rtb_lcetcss16BsTarWhlSpin_d = localB->y1_p;
  }

  /* End of MinMax: '<Root>/MinMax1' */

  /* Saturate: '<Root>/Saturation' */
  if (rtb_lcetcss16BsTarWhlSpin_d > 255) {
    *rty_lcetcss16BsTarWhlSpin = 255;
  } else if (rtb_lcetcss16BsTarWhlSpin_d < 0) {
    *rty_lcetcss16BsTarWhlSpin = 0;
  } else {
    *rty_lcetcss16BsTarWhlSpin = rtb_lcetcss16BsTarWhlSpin_d;
  }

  /* End of Saturate: '<Root>/Saturation' */
}

/* Model initialize function */
void LCETCS_vCalBsTarWhlSpin_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter5Point' */
  LCETCS_s16Inter5Point_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
