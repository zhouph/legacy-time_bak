/*
 * File: LCETCS_vCalSustainbyHsa.c
 *
 * Code generated for Simulink model 'LCETCS_vCalSustainbyHsa'.
 *
 * Model version                  : 1.383
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:57:53 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalSustainbyHsa.h"
#include "LCETCS_vCalSustainbyHsa_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalSustainbyHsa' */
void LCETCS_vCalSustainbyHsa_Init(boolean_T *rty_lcetcsu1HillbyHsaSustain,
  DW_LCETCS_vCalSustainbyHsa_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = false;

  /* InitializeConditions for Chart: '<Root>/Chart1' */
  *rty_lcetcsu1HillbyHsaSustain = false;
}

/* Output and update for referenced model: 'LCETCS_vCalSustainbyHsa' */
void LCETCS_vCalSustainbyHsa(boolean_T rtu_lcetcsu1HillbyHsaAtv, int16_T
  rtu_lcetcss16DistVehMovRd, boolean_T rtu_lcetcsu1SplitHillClrCndSet, boolean_T
  *rty_lcetcsu1HillbyHsaSustain, DW_LCETCS_vCalSustainbyHsa_f_T *localDW)
{
  /* Chart: '<Root>/Chart1' incorporates:
   *  UnitDelay: '<Root>/Unit Delay'
   */
  /* Gateway: Chart1 */
  /* During: Chart1 */
  /* Entry Internal: Chart1 */
  /* Transition: '<S1>:9' */
  /* comment */
  if (rtu_lcetcsu1HillbyHsaAtv == 1) {
    /* Transition: '<S1>:171' */
    /* Transition: '<S1>:169' */
    *rty_lcetcsu1HillbyHsaSustain = true;

    /* Transition: '<S1>:174' */
  } else {
    /* Transition: '<S1>:173' */
    *rty_lcetcsu1HillbyHsaSustain = localDW->UnitDelay_DSTATE;
  }

  /* Transition: '<S1>:176' */
  /* 1000 = 1m */
  if ((rtu_lcetcss16DistVehMovRd >= 1000) || (rtu_lcetcsu1SplitHillClrCndSet ==
       1)) {
    /* Transition: '<S1>:181' */
    /* Transition: '<S1>:183' */
    *rty_lcetcsu1HillbyHsaSustain = false;

    /* Transition: '<S1>:201' */
  } else {
    /* Transition: '<S1>:200' */
  }

  /* End of Chart: '<Root>/Chart1' */

  /* Update for UnitDelay: '<Root>/Unit Delay' */
  /* Transition: '<S1>:202' */
  localDW->UnitDelay_DSTATE = *rty_lcetcsu1HillbyHsaSustain;
}

/* Model initialize function */
void LCETCS_vCalSustainbyHsa_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
