/*
 * File: LCETCS_vDctLwToHibyDecel.c
 *
 * Code generated for Simulink model 'LCETCS_vDctLwToHibyDecel'.
 *
 * Model version                  : 1.167
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:56:10 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctLwToHibyDecel.h"
#include "LCETCS_vDctLwToHibyDecel_private.h"

/* Initial conditions for referenced model: 'LCETCS_vDctLwToHibyDecel' */
void LCETCS_vDctLwToHibyDecel_Init(DW_LCETCS_vDctLwToHibyDecel_f_T *localDW)
{
  /* InitializeConditions for Delay: '<Root>/Delay3' */
  localDW->Delay3_DSTATE[0] = 0;
  localDW->Delay3_DSTATE[1] = 0;
  localDW->CircBufIdx = 0U;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay3' */
  localDW->UnitDelay3_DSTATE = 0;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = false;
}

/* Output and update for referenced model: 'LCETCS_vDctLwToHibyDecel' */
void LCETCS_vDctLwToHibyDecel(const TypeETCSWhlAccStruct *rtu_ETCS_WHL_ACC,
  const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const TypeETCSWhlSpinStruct
  *rtu_ETCS_WHL_SPIN, const TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN, int16_T
  *rty_lcetcss16LwToHiCnt, DW_LCETCS_vDctLwToHibyDecel_f_T *localDW)
{
  boolean_T rtb_lcetcsu1LwToHiSuspect;
  int32_T tmp;
  int16_T y;

  /* Chart: '<Root>/Chart' incorporates:
   *  Delay: '<Root>/Delay3'
   *  MinMax: '<Root>/MinMax1'
   *  Sum: '<Root>/Add1'
   *  Sum: '<Root>/Add2'
   *  UnitDelay: '<Root>/Unit Delay1'
   *  UnitDelay: '<Root>/Unit Delay3'
   */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:117' */
  /* comment */
  if (rtu_ETCS_CTL_ACT->lcetcsu1CtlAct == 1) {
    /* MinMax: '<Root>/MinMax1' */
    /* Transition: '<S1>:120' */
    /* Transition: '<S1>:176' */
    if (rtu_ETCS_WHL_ACC->lcetcss16FrtLefWhlAcc <=
        rtu_ETCS_WHL_ACC->lcetcss16FrtRigWhlAcc) {
      y = rtu_ETCS_WHL_ACC->lcetcss16FrtLefWhlAcc;
    } else {
      y = rtu_ETCS_WHL_ACC->lcetcss16FrtRigWhlAcc;
    }

    if (y <= rtu_ETCS_WHL_ACC->lcetcss16RrLefWhlAcc) {
    } else {
      y = rtu_ETCS_WHL_ACC->lcetcss16RrLefWhlAcc;
    }

    if (y <= rtu_ETCS_WHL_ACC->lcetcss16RrRigWhlAcc) {
    } else {
      y = rtu_ETCS_WHL_ACC->lcetcss16RrRigWhlAcc;
    }

    if ((y <= ((int16_T)S16ETCSCpLow2HighDecel)) || (localDW->UnitDelay1_DSTATE ==
         1)) {
      /* Transition: '<S1>:125' */
      /* Transition: '<S1>:127' */
      rtb_lcetcsu1LwToHiSuspect = true;
      if ((rtu_ETCS_TAR_SPIN->lcetcsu1TarSpnDec == 0) && ((((int16_T)
             (rtu_ETCS_TAR_SPIN->lcetcss16TarWhlSpin -
              rtu_ETCS_WHL_SPIN->lcetcss16WhlSpinNonFilt)) > 0) || (((int16_T)
             (rtu_ETCS_WHL_SPIN->lcetcss16WhlSpinNonFilt -
              localDW->Delay3_DSTATE[localDW->CircBufIdx])) < 0))) {
        /* Transition: '<S1>:140' */
        /* Transition: '<S1>:142' */
        tmp = localDW->UnitDelay3_DSTATE + 1;
        if (tmp > 32767) {
          tmp = 32767;
        }

        *rty_lcetcss16LwToHiCnt = (int16_T)tmp;

        /* Transition: '<S1>:169' */
        /* Transition: '<S1>:170' */
      } else {
        /* Transition: '<S1>:164' */
        if (localDW->UnitDelay3_DSTATE > 0) {
          /* Transition: '<S1>:166' */
          /* Transition: '<S1>:168' */
          tmp = localDW->UnitDelay3_DSTATE - 1;
          if (tmp < -32768) {
            tmp = -32768;
          }

          *rty_lcetcss16LwToHiCnt = (int16_T)tmp;

          /* Transition: '<S1>:170' */
        } else {
          /* Transition: '<S1>:171' */
          rtb_lcetcsu1LwToHiSuspect = false;
          *rty_lcetcss16LwToHiCnt = 0;
        }
      }

      /* Transition: '<S1>:178' */
    } else {
      /* Transition: '<S1>:124' */
      rtb_lcetcsu1LwToHiSuspect = false;
      *rty_lcetcss16LwToHiCnt = 0;
    }

    /* Transition: '<S1>:129' */
  } else {
    /* Transition: '<S1>:118' */
    rtb_lcetcsu1LwToHiSuspect = false;
    *rty_lcetcss16LwToHiCnt = 0;
  }

  /* End of Chart: '<Root>/Chart' */

  /* Saturate: '<Root>/Saturation' */
  /* Transition: '<S1>:126' */
  if ((*rty_lcetcss16LwToHiCnt) > ((uint8_T)U8ETCSCpLwToHiDetCnt)) {
    *rty_lcetcss16LwToHiCnt = ((uint8_T)U8ETCSCpLwToHiDetCnt);
  } else {
    if ((*rty_lcetcss16LwToHiCnt) < 0) {
      *rty_lcetcss16LwToHiCnt = 0;
    }
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Update for Delay: '<Root>/Delay3' */
  localDW->Delay3_DSTATE[localDW->CircBufIdx] =
    rtu_ETCS_WHL_SPIN->lcetcss16WhlSpinNonFilt;
  if (localDW->CircBufIdx < 1U) {
    localDW->CircBufIdx++;
  } else {
    localDW->CircBufIdx = 0U;
  }

  /* End of Update for Delay: '<Root>/Delay3' */

  /* Update for UnitDelay: '<Root>/Unit Delay3' */
  localDW->UnitDelay3_DSTATE = *rty_lcetcss16LwToHiCnt;

  /* Update for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = rtb_lcetcsu1LwToHiSuspect;
}

/* Model initialize function */
void LCETCS_vDctLwToHibyDecel_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
