/*
 * File: LCETCS_vCalDelYawCnvrgGain.c
 *
 * Code generated for Simulink model 'LCETCS_vCalDelYawCnvrgGain'.
 *
 * Model version                  : 1.258
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:03:00 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalDelYawCnvrgGain.h"
#include "LCETCS_vCalDelYawCnvrgGain_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalDelYawCnvrgGain' */
void LCETCS_vCalDelYawCnvrgGain_Init(B_LCETCS_vCalDelYawCnvrgGain_c_T *localB)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter3Point' */
  LCETCS_s16Inter3Point_Init(&localB->y);

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter3Point1' */
  LCETCS_s16Inter3Point_Init(&localB->y_p);
}

/* Start for referenced model: 'LCETCS_vCalDelYawCnvrgGain' */
void LCETCS_vCalDelYawCnvrgGain_Start(B_LCETCS_vCalDelYawCnvrgGain_c_T *localB)
{
  /* Start for Constant: '<Root>/Ay level on asphalt' */
  localB->x3 = ((uint8_T)U8ETCSCpAyAsp);

  /* Start for Constant: '<Root>/Ay level on asphalt1' */
  localB->x3_c = ((uint8_T)U8ETCSCpAyAsp);

  /* Start for Constant: '<Root>/Ay level on ice' */
  localB->x1 = ((uint8_T)U8ETCSCpAyIce);

  /* Start for Constant: '<Root>/Ay level on ice1' */
  localB->x1_k = ((uint8_T)U8ETCSCpAyIce);

  /* Start for Constant: '<Root>/Ay level on snow' */
  localB->x2 = ((uint8_T)U8ETCSCpAySnw);

  /* Start for Constant: '<Root>/Ay level on snow1' */
  localB->x2_a = ((uint8_T)U8ETCSCpAySnw);

  /* Start for Constant: '<Root>/Delta yaw gain below target torque on asphalt' */
  localB->y3 = ((uint8_T)U8ETCSCpDelYawLowTrqGainAsp);

  /* Start for Constant: '<Root>/Delta yaw gain below target torque on ice' */
  localB->y1 = ((uint8_T)U8ETCSCpDelYawLowTrqGainIce);

  /* Start for Constant: '<Root>/Delta yaw gain below target torque on snow' */
  localB->y2 = ((uint8_T)U8ETCSCpDelYawLowTrqGainSnw);
}

/* Output and update for referenced model: 'LCETCS_vCalDelYawCnvrgGain' */
void LCETCS_vCalDelYawCnvrgGain(const TypeETCSCtlCmdStruct *rtu_ETCS_CTL_CMD_OLD,
  const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, int16_T
  rtu_lcetcss16BasDelYawCnvrgGain, int16_T *rty_lcetcss16DelYawCnvrgGain,
  B_LCETCS_vCalDelYawCnvrgGain_c_T *localB)
{
  /* Constant: '<Root>/Ay level on asphalt' */
  localB->x3 = ((uint8_T)U8ETCSCpAyAsp);

  /* Constant: '<Root>/Ay level on asphalt1' */
  localB->x3_c = ((uint8_T)U8ETCSCpAyAsp);

  /* Constant: '<Root>/Ay level on ice' */
  localB->x1 = ((uint8_T)U8ETCSCpAyIce);

  /* Constant: '<Root>/Ay level on ice1' */
  localB->x1_k = ((uint8_T)U8ETCSCpAyIce);

  /* Constant: '<Root>/Ay level on snow' */
  localB->x2 = ((uint8_T)U8ETCSCpAySnw);

  /* Constant: '<Root>/Ay level on snow1' */
  localB->x2_a = ((uint8_T)U8ETCSCpAySnw);

  /* Constant: '<Root>/Delta yaw gain below target torque on asphalt' */
  localB->y3 = ((uint8_T)U8ETCSCpDelYawLowTrqGainAsp);

  /* Constant: '<Root>/Delta yaw gain below target torque on ice' */
  localB->y1 = ((uint8_T)U8ETCSCpDelYawLowTrqGainIce);

  /* Constant: '<Root>/Delta yaw gain below target torque on snow' */
  localB->y2 = ((uint8_T)U8ETCSCpDelYawLowTrqGainSnw);

  /* ModelReference: '<Root>/LCETCS_s16Inter3Point' */
  LCETCS_s16Inter3Point(rtu_ETCS_ESC_SIG->lcetcss16LatAccMsrdMani, localB->x1,
                        localB->x2, localB->x3, ((int16_T)
    S16ETCSCpLmtCrngTarTrqIce), ((int16_T)S16ETCSCpLmtCrngTarTrqSnw), ((int16_T)
    S16ETCSCpLmtCrngTarTrqAsp), &localB->y);

  /* ModelReference: '<Root>/LCETCS_s16Inter3Point1' */
  LCETCS_s16Inter3Point(rtu_ETCS_ESC_SIG->lcetcss16LatAccMsrdMani, localB->x1_k,
                        localB->x2_a, localB->x3_c, localB->y1, localB->y2,
                        localB->y3, &localB->y_p);

  /* Switch: '<Root>/Switch' incorporates:
   *  Gain: '<Root>/Gain'
   *  RelationalOperator: '<Root>/Relational Operator'
   */
  if (rtu_ETCS_CTL_CMD_OLD->lcetcss32CardanTrqCmd >= (10 * localB->y)) {
    *rty_lcetcss16DelYawCnvrgGain = rtu_lcetcss16BasDelYawCnvrgGain;
  } else {
    *rty_lcetcss16DelYawCnvrgGain = localB->y_p;
  }

  /* End of Switch: '<Root>/Switch' */
}

/* Model initialize function */
void LCETCS_vCalDelYawCnvrgGain_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter3Point' */
  LCETCS_s16Inter3Point_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter3Point1' */
  LCETCS_s16Inter3Point_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
