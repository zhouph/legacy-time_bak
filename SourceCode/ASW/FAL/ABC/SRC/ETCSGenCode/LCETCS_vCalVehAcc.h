/*
 * File: LCETCS_vCalVehAcc.h
 *
 * Code generated for Simulink model 'LCETCS_vCalVehAcc'.
 *
 * Model version                  : 1.509
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:07:51 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalVehAcc_h_
#define RTW_HEADER_LCETCS_vCalVehAcc_h_
#ifndef LCETCS_vCalVehAcc_COMMON_INCLUDES_
# define LCETCS_vCalVehAcc_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalVehAcc_COMMON_INCLUDES_ */

#include "LCETCS_vCalVehAcc_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vCalVehLongAcc4Ctl.h"
#include "LCETCS_vCalVehAccByVehSpd.h"
#include "LCETCS_vCalScaleDnAy.h"
#include "LCETCS_vCalRsltntAcc.h"
#include "LCETCS_vCalRefAxRepRdFric.h"
#include "LCETCS_vCalCordRefRdFric.h"

/* Block signals for model 'LCETCS_vCalVehAcc' */
typedef struct {
  int16_T lcetcss16RefRdCfFricAtIce;   /* '<Root>/LCETCS_vCalCordRefRdFric' */
  int16_T lcetcss16RefRdCfFricAtSnow;  /* '<Root>/LCETCS_vCalCordRefRdFric' */
  int16_T lcetcss16RefRdCfFricAtAsphalt;/* '<Root>/LCETCS_vCalCordRefRdFric' */
} B_LCETCS_vCalVehAcc_c_T;

/* Block states (auto storage) for model 'LCETCS_vCalVehAcc' */
typedef struct {
  MdlrefDW_LCETCS_vCalVehAccByVehSpd_T LCETCS_vCalVehAccByVehSpd_DWORK1;/* '<Root>/LCETCS_vCalVehAccByVehSpd' */
  MdlrefDW_LCETCS_vCalScaleDnAy_T LCETCS_vCalScaleDnAy_DWORK1;/* '<Root>/LCETCS_vCalScaleDnAy' */
  MdlrefDW_LCETCS_vCalRefAxRepRdFric_T LCETCS_vCalRefAxRepRdFric_DWORK1;/* '<Root>/LCETCS_vCalRefAxRepRdFric' */
} DW_LCETCS_vCalVehAcc_f_T;

typedef struct {
  B_LCETCS_vCalVehAcc_c_T rtb;
  DW_LCETCS_vCalVehAcc_f_T rtdw;
} MdlrefDW_LCETCS_vCalVehAcc_T;

/* Model reference registration function */
extern void LCETCS_vCalVehAcc_initialize(void);
extern void LCETCS_vCalVehAcc_Init(B_LCETCS_vCalVehAcc_c_T *localB,
  DW_LCETCS_vCalVehAcc_f_T *localDW);
extern void LCETCS_vCalVehAcc_Start(DW_LCETCS_vCalVehAcc_f_T *localDW);
extern void LCETCS_vCalVehAcc(int16_T rtu_lcetcss16VehSpd, const
  TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const TypeETCSDrvMdlStruct
  *rtu_ETCS_DRV_MDL, TypeETCSVehAccStruct *rty_ETCS_VEH_ACC,
  B_LCETCS_vCalVehAcc_c_T *localB, DW_LCETCS_vCalVehAcc_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalVehAcc'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalVehAcc_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
