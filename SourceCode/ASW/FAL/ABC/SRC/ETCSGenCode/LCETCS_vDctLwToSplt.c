/*
 * File: LCETCS_vDctLwToSplt.c
 *
 * Code generated for Simulink model 'LCETCS_vDctLwToSplt'.
 *
 * Model version                  : 1.289
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:06:24 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctLwToSplt.h"
#include "LCETCS_vDctLwToSplt_private.h"

/* Initial conditions for referenced model: 'LCETCS_vDctLwToSplt' */
void LCETCS_vDctLwToSplt_Init(B_LCETCS_vDctLwToSplt_c_T *localB,
  DW_LCETCS_vDctLwToSplt_f_T *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctWhlOnMu' */
  LCETCS_vDctWhlOnMu_Init(&localB->lcetcss16LowMuWhlSpinNF,
    &localB->lcetcss16HighMuWhlSpinNF, &localB->lcetcsu1DctWhlOnHighmu);

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = 0;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = 0;

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctLwMuTendency' */
  LCETCS_vDctLwMuTendency_Init(&(localDW->LCETCS_vDctLwMuTendency_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalLwToSpltCnt' */
  LCETCS_vCalLwToSpltCnt_Init(&(localDW->LCETCS_vCalLwToSpltCnt_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctLwMuHldCnt' */
  LCETCS_vDctLwMuHldCnt_Init(&(localDW->LCETCS_vDctLwMuHldCnt_DWORK1.rtdw));
}

/* Output and update for referenced model: 'LCETCS_vDctLwToSplt' */
void LCETCS_vDctLwToSplt(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const
  TypeETCSAxlStruct *rtu_ETCS_FA, const TypeETCSAxlStruct *rtu_ETCS_RA, const
  TypeETCSWhlStruct *rtu_ETCS_FL, const TypeETCSWhlStruct *rtu_ETCS_FR, const
  TypeETCSWhlStruct *rtu_ETCS_RL, const TypeETCSWhlStruct *rtu_ETCS_RR, const
  TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const TypeETCSTrq4RdFricStruct
  *rtu_ETCS_TRQ_REP_RD_FRIC_OLD, const TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN,
  const TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC, TypeETCSLwToSpltStruct
  *rty_ETCS_L2SPLT, B_LCETCS_vDctLwToSplt_c_T *localB,
  DW_LCETCS_vDctLwToSplt_f_T *localDW)
{
  /* local block i/o variables */
  int16_T rtb_lcetcss16LwToSplitCnt;
  int16_T rtb_lcetcss16LowHmHldCnt;
  int16_T rtb_lcetcss16LwToSplitCnt_d;
  int16_T rtb_lcetcss16LowHmHldCnt_c;

  /* ModelReference: '<Root>/LCETCS_vDctWhlOnMu' */
  LCETCS_vDctWhlOnMu(rtu_ETCS_FL, rtu_ETCS_FR, rtu_ETCS_RL, rtu_ETCS_RR,
                     &localB->lcetcss16LowMuWhlSpinNF,
                     &localB->lcetcss16HighMuWhlSpinNF,
                     &localB->lcetcsu1DctWhlOnHighmu);

  /* UnitDelay: '<Root>/Unit Delay2' */
  rtb_lcetcss16LwToSplitCnt = localDW->UnitDelay2_DSTATE;

  /* UnitDelay: '<Root>/Unit Delay1' */
  rtb_lcetcss16LowHmHldCnt = localDW->UnitDelay1_DSTATE;

  /* ModelReference: '<Root>/LCETCS_vDctLwMuTendency' */
  LCETCS_vDctLwMuTendency(rtu_ETCS_CTL_ACT, rtu_ETCS_VEH_ACC, rtu_ETCS_FA,
    rtu_ETCS_RA, rtb_lcetcss16LwToSplitCnt, rtb_lcetcss16LowHmHldCnt,
    &localB->lcetcsu1DctLowHm, &localB->lcetcsu1StrtPintLowHm,
    &(localDW->LCETCS_vDctLwMuTendency_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalLwToSpltCnt' */
  LCETCS_vCalLwToSpltCnt(localB->lcetcss16LowMuWhlSpinNF,
    localB->lcetcss16HighMuWhlSpinNF, localB->lcetcsu1DctWhlOnHighmu,
    rtu_ETCS_FA, rtu_ETCS_RA, rtu_ETCS_ESC_SIG, rtu_ETCS_TRQ_REP_RD_FRIC_OLD,
    rtu_ETCS_TAR_SPIN, localB->lcetcsu1DctLowHm, &rtb_lcetcss16LwToSplitCnt_d,
                         &(localDW->LCETCS_vCalLwToSpltCnt_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vDctLwMuHldCnt' */
  LCETCS_vDctLwMuHldCnt(localB->lcetcsu1DctLowHm, localB->lcetcsu1StrtPintLowHm,
                        &rtb_lcetcss16LowHmHldCnt_c,
                        &(localDW->LCETCS_vDctLwMuHldCnt_DWORK1.rtdw));

  /* BusCreator: '<Root>/Bus Creator' */
  rty_ETCS_L2SPLT->lcetcss16LwToSplitCnt = rtb_lcetcss16LwToSplitCnt_d;
  rty_ETCS_L2SPLT->lcetcss16LowMuWhlSpinNF = localB->lcetcss16LowMuWhlSpinNF;
  rty_ETCS_L2SPLT->lcetcss16HighMuWhlSpinNF = localB->lcetcss16HighMuWhlSpinNF;
  rty_ETCS_L2SPLT->lcetcsu1DctWhlOnHighmu = localB->lcetcsu1DctWhlOnHighmu;
  rty_ETCS_L2SPLT->lcetcsu1DctLowHm = localB->lcetcsu1DctLowHm;
  rty_ETCS_L2SPLT->lcetcss16LowHmHldCnt = rtb_lcetcss16LowHmHldCnt_c;

  /* Update for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = rtb_lcetcss16LwToSplitCnt_d;

  /* Update for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = rtb_lcetcss16LowHmHldCnt_c;
}

/* Model initialize function */
void LCETCS_vDctLwToSplt_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalLwToSpltCnt' */
  LCETCS_vCalLwToSpltCnt_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctLwMuHldCnt' */
  LCETCS_vDctLwMuHldCnt_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctLwMuTendency' */
  LCETCS_vDctLwMuTendency_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctWhlOnMu' */
  LCETCS_vDctWhlOnMu_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
