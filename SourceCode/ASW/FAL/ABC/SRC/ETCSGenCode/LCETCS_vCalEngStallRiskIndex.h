/*
 * File: LCETCS_vCalEngStallRiskIndex.h
 *
 * Code generated for Simulink model 'LCETCS_vCalEngStallRiskIndex'.
 *
 * Model version                  : 1.263
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:05:45 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalEngStallRiskIndex_h_
#define RTW_HEADER_LCETCS_vCalEngStallRiskIndex_h_
#ifndef LCETCS_vCalEngStallRiskIndex_COMMON_INCLUDES_
# define LCETCS_vCalEngStallRiskIndex_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalEngStallRiskIndex_COMMON_INCLUDES_ */

#include "LCETCS_vCalEngStallRiskIndex_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_s16Inter3Point.h"

/* Block signals for model 'LCETCS_vCalEngStallRiskIndex' */
typedef struct {
  int16_T Constant1;                   /* '<S1>/Constant1' */
  int16_T Constant2;                   /* '<S1>/Constant2' */
  int16_T Constant3;                   /* '<S1>/Constant3' */
  int16_T y;                           /* '<S1>/LCETCS_s16Inter3Point' */
  int16_T y_n;                         /* '<S1>/LCETCS_s16Inter3Point1' */
} B_LCETCS_vCalEngStallRiskIndex_c_T;

/* Block states (auto storage) for model 'LCETCS_vCalEngStallRiskIndex' */
typedef struct {
  int8_T UnitDelay_DSTATE;             /* '<Root>/Unit Delay' */
  int8_T If_ActiveSubsystem;           /* '<Root>/If' */
} DW_LCETCS_vCalEngStallRiskIndex_f_T;

typedef struct {
  B_LCETCS_vCalEngStallRiskIndex_c_T rtb;
  DW_LCETCS_vCalEngStallRiskIndex_f_T rtdw;
} MdlrefDW_LCETCS_vCalEngStallRiskIndex_T;

/* Model reference registration function */
extern void LCETCS_vCalEngStallRiskIndex_initialize(void);
extern void LCETCS_vCalEngStallRiskIndex_Init
  (DW_LCETCS_vCalEngStallRiskIndex_f_T *localDW);
extern void LCETCS_vCalEngStallRiskIndex_Start
  (B_LCETCS_vCalEngStallRiskIndex_c_T *localB,
   DW_LCETCS_vCalEngStallRiskIndex_f_T *localDW);
extern void LCETCS_vCalEngStallRiskIndex_Disable
  (DW_LCETCS_vCalEngStallRiskIndex_f_T *localDW);
extern void LCETCS_vCalEngStallRiskIndex(const TypeETCSCtlActStruct
  *rtu_ETCS_CTL_ACT_OLD, const TypeETCSEngStallStruct *rtu_ETCS_ENG_STALL_OLD,
  int16_T rtu_lcetcss16VehSpd, int16_T rtu_lcetcss16EngSpdSlope, int8_T
  *rty_lcetcss8EngStallRiskIndex, B_LCETCS_vCalEngStallRiskIndex_c_T *localB,
  DW_LCETCS_vCalEngStallRiskIndex_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalEngStallRiskIndex'
 * '<S1>'   : 'LCETCS_vCalEngStallRiskIndex/Engine Stall Risk Index in ETCS Control'
 * '<S2>'   : 'LCETCS_vCalEngStallRiskIndex/Engine Stall Risk Index in ETCS Non Control'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalEngStallRiskIndex_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
