/*
 * File: LCETCS_vCalWhlSpinNonFilt.c
 *
 * Code generated for Simulink model 'LCETCS_vCalWhlSpinNonFilt'.
 *
 * Model version                  : 1.82
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:50:42 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalWhlSpinNonFilt.h"
#include "LCETCS_vCalWhlSpinNonFilt_private.h"

/* Output and update for referenced model: 'LCETCS_vCalWhlSpinNonFilt' */
void LCETCS_vCalWhlSpinNonFilt(const TypeETCSWhlStruct *rtu_ETCS_FL, const
  TypeETCSWhlStruct *rtu_ETCS_FR, const TypeETCSWhlStruct *rtu_ETCS_RL, const
  TypeETCSWhlStruct *rtu_ETCS_RR, int16_T rtu_lcetcss16VehSpd, int16_T
  *rty_lcetcss16WhlSpinNonFilt, int16_T *rty_lcetcss16WhlSpinFrtAxlNonFilt,
  int16_T *rty_lcetcss16WhlSpinRrAxlNonFilt)
{
  int16_T u0;

  /* Saturate: '<Root>/Saturation' incorporates:
   *  Product: '<Root>/Divide2'
   *  Sum: '<Root>/Add'
   *  Sum: '<Root>/Add1'
   */
  u0 = (int16_T)(((rtu_ETCS_FL->lcetcss16WhlSpdCrt +
                   rtu_ETCS_FR->lcetcss16WhlSpdCrt) / 2) - rtu_lcetcss16VehSpd);
  if (u0 > 1200) {
    *rty_lcetcss16WhlSpinFrtAxlNonFilt = 1200;
  } else if (u0 < 0) {
    *rty_lcetcss16WhlSpinFrtAxlNonFilt = 0;
  } else {
    *rty_lcetcss16WhlSpinFrtAxlNonFilt = u0;
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Saturate: '<Root>/Saturation1' incorporates:
   *  Product: '<Root>/Divide1'
   *  Sum: '<Root>/Add2'
   *  Sum: '<Root>/Add3'
   */
  u0 = (int16_T)(((rtu_ETCS_RL->lcetcss16WhlSpdCrt +
                   rtu_ETCS_RR->lcetcss16WhlSpdCrt) / 2) - rtu_lcetcss16VehSpd);
  if (u0 > 1200) {
    *rty_lcetcss16WhlSpinRrAxlNonFilt = 1200;
  } else if (u0 < 0) {
    *rty_lcetcss16WhlSpinRrAxlNonFilt = 0;
  } else {
    *rty_lcetcss16WhlSpinRrAxlNonFilt = u0;
  }

  /* End of Saturate: '<Root>/Saturation1' */

  /* Switch: '<Root>/Switch4' incorporates:
   *  Constant: '<Root>/Constant4'
   */
  if (((boolean_T)VarFwd)) {
    u0 = *rty_lcetcss16WhlSpinFrtAxlNonFilt;
  } else {
    u0 = *rty_lcetcss16WhlSpinRrAxlNonFilt;
  }

  *rty_lcetcss16WhlSpinNonFilt = u0;

  /* End of Switch: '<Root>/Switch4' */
}

/* Model initialize function */
void LCETCS_vCalWhlSpinNonFilt_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
