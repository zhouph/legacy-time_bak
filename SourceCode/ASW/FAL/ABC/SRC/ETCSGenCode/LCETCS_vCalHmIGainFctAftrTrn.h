/*
 * File: LCETCS_vCalHmIGainFctAftrTrn.h
 *
 * Code generated for Simulink model 'LCETCS_vCalHmIGainFctAftrTrn'.
 *
 * Model version                  : 1.184
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:01:16 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalHmIGainFctAftrTrn_h_
#define RTW_HEADER_LCETCS_vCalHmIGainFctAftrTrn_h_
#ifndef LCETCS_vCalHmIGainFctAftrTrn_COMMON_INCLUDES_
# define LCETCS_vCalHmIGainFctAftrTrn_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalHmIGainFctAftrTrn_COMMON_INCLUDES_ */

#include "LCETCS_vCalHmIGainFctAftrTrn_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_s16Inter3by5.h"

/* Block signals for model 'LCETCS_vCalHmIGainFctAftrTrn' */
typedef struct {
  int16_T z11;                         /* '<Root>/Constant10' */
  int16_T z12;                         /* '<Root>/Constant11' */
  int16_T z13;                         /* '<Root>/Constant12' */
  int16_T z33;                         /* '<Root>/Constant13' */
  int16_T z43;                         /* '<Root>/Constant14' */
  int16_T z41;                         /* '<Root>/Constant15' */
  int16_T z42;                         /* '<Root>/Constant16' */
  int16_T z53;                         /* '<Root>/Constant17' */
  int16_T z51;                         /* '<Root>/Constant18' */
  int16_T z52;                         /* '<Root>/Constant19' */
  int16_T z21;                         /* '<Root>/Constant2' */
  int16_T z22;                         /* '<Root>/Constant3' */
  int16_T z23;                         /* '<Root>/Constant4' */
  int16_T z31;                         /* '<Root>/Constant5' */
  int16_T z32;                         /* '<Root>/Constant6' */
  int16_T x1;                          /* '<Root>/Constant7' */
  int16_T x2;                          /* '<Root>/Constant8' */
  int16_T x3;                          /* '<Root>/Constant9' */
  int16_T Out;                         /* '<Root>/LCETCS_s16Inter3by5' */
} B_LCETCS_vCalHmIGainFctAftrTrn_c_T;

typedef struct {
  B_LCETCS_vCalHmIGainFctAftrTrn_c_T rtb;
} MdlrefDW_LCETCS_vCalHmIGainFctAftrTrn_T;

/* Model reference registration function */
extern void LCETCS_vCalHmIGainFctAftrTrn_initialize(void);
extern void LCETCS_vCalHmIGainFctAftrTrn_Init(B_LCETCS_vCalHmIGainFctAftrTrn_c_T
  *localB);
extern void LCETCS_vCalHmIGainFctAftrTrn_Start
  (B_LCETCS_vCalHmIGainFctAftrTrn_c_T *localB);
extern void LCETCS_vCalHmIGainFctAftrTrn(const TypeETCSAftLmtCrng
  *rtu_ETCS_AFT_TURN, const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const
  TypeETCSDrvMdlStruct *rtu_ETCS_DRV_MDL, int16_T
  *rty_lcetcss16HmIGainFctAftrTrn, B_LCETCS_vCalHmIGainFctAftrTrn_c_T *localB);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalHmIGainFctAftrTrn'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalHmIGainFctAftrTrn_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
