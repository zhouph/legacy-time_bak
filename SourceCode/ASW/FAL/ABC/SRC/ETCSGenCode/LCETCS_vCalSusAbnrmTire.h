/*
 * File: LCETCS_vCalSusAbnrmTire.h
 *
 * Code generated for Simulink model 'LCETCS_vCalSusAbnrmTire'.
 *
 * Model version                  : 1.127
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:49:59 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalSusAbnrmTire_h_
#define RTW_HEADER_LCETCS_vCalSusAbnrmTire_h_
#ifndef LCETCS_vCalSusAbnrmTire_COMMON_INCLUDES_
# define LCETCS_vCalSusAbnrmTire_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalSusAbnrmTire_COMMON_INCLUDES_ */

#include "LCETCS_vCalSusAbnrmTire_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Model reference registration function */
extern void LCETCS_vCalSusAbnrmTire_initialize(void);
extern void LCETCS_vCalSusAbnrmTire_Init(boolean_T *rty_lcetcsu1AbnrmSizeTireSus);
extern void LCETCS_vCalSusAbnrmTire(const TypeETCSWhlStruct *rtu_ETCS_FL, const
  TypeETCSWhlStruct *rtu_ETCS_FR, const TypeETCSWhlStruct *rtu_ETCS_RL, const
  TypeETCSWhlStruct *rtu_ETCS_RR, boolean_T *rty_lcetcsu1AbnrmSizeTireSus);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalSusAbnrmTire'
 * '<S1>'   : 'LCETCS_vCalSusAbnrmTire/Chart1'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalSusAbnrmTire_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
