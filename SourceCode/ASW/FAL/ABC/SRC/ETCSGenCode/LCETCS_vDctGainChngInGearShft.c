/*
 * File: LCETCS_vDctGainChngInGearShft.c
 *
 * Code generated for Simulink model 'LCETCS_vDctGainChngInGearShft'.
 *
 * Model version                  : 1.201
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:55:47 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctGainChngInGearShft.h"
#include "LCETCS_vDctGainChngInGearShft_private.h"

const rtTimingBridge *LCETCS_vDctGainChngInGearShft_TimingBrdg;
const TypeETCSGainGearShftStruct
  LCETCS_vDctGainChngInGearShft_rtZTypeETCSGainGearShftStruct = {
  false,                               /* lcetcsu1GainChngInGearShft */
  false,                               /* lcetcsu1GainTrnsAftrGearShft */
  0U                                   /* lcetcsu8GainTrnsTm */
} ;                                    /* TypeETCSGainGearShftStruct ground */

/* Initial conditions for referenced model: 'LCETCS_vDctGainChngInGearShft' */
void LCETCS_vDctGainChngInGearShft_Init(TypeETCSGainGearShftStruct
  *rty_ETCS_GAIN_GEAR_SHFT, B_LCETCS_vDctGainChngInGearShft_c_T *localB,
  DW_LCETCS_vDctGainChngInGearShft_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE =
    LCETCS_vDctGainChngInGearShft_rtZTypeETCSGainGearShftStruct;

  /* InitializeConditions for Chart: '<Root>/Chart1' */
  localB->lcetcsu8GainTrnsTm = 0U;

  /* InitializeConditions for BusCreator: '<Root>/Bus Creator' incorporates:
   *  InitializeConditions for Chart: '<Root>/Chart'
   *  InitializeConditions for Chart: '<Root>/Chart1'
   */
  if (rtmIsFirstInitCond()) {
    rty_ETCS_GAIN_GEAR_SHFT->lcetcsu1GainChngInGearShft = false;
    rty_ETCS_GAIN_GEAR_SHFT->lcetcsu1GainTrnsAftrGearShft = false;
    rty_ETCS_GAIN_GEAR_SHFT->lcetcsu8GainTrnsTm = localB->lcetcsu8GainTrnsTm;
  }

  /* End of InitializeConditions for BusCreator: '<Root>/Bus Creator' */
}

/* Output and update for referenced model: 'LCETCS_vDctGainChngInGearShft' */
void LCETCS_vDctGainChngInGearShft(const TypeETCSH2LStruct *rtu_ETCS_H2L, const
  TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const TypeETCSGearStruct
  *rtu_ETCS_GEAR_STRUCT, const TypeETCSCyl1stStruct *rtu_ETCS_CYL_1ST, const
  TypeETCSExtDctStruct *rtu_ETCS_EXT_DCT, TypeETCSGainGearShftStruct
  *rty_ETCS_GAIN_GEAR_SHFT, B_LCETCS_vDctGainChngInGearShft_c_T *localB,
  DW_LCETCS_vDctGainChngInGearShft_f_T *localDW)
{
  boolean_T lcetcsu1GainChngInGearShft;
  boolean_T lcetcsu1GainTrnsAftrGearShft;
  int32_T tmp;

  /* Chart: '<Root>/Chart' */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:14' */
  /* comment */
  if ((rtu_ETCS_CTL_ACT->lcetcsu1CtlAct == 1) &&
      (rtu_ETCS_GEAR_STRUCT->lcetcsu1GearShifting == 1)) {
    /* Transition: '<S1>:15' */
    /* Transition: '<S1>:23' */
    if (rtu_ETCS_EXT_DCT->lcetcsu1AutoTrans == 1) {
      /* Transition: '<S1>:16' */
      /* Transition: '<S1>:21' */
      if ((rtu_ETCS_CYL_1ST->lcetcsu1Cyl1st == 1) ||
          (rtu_ETCS_H2L->lcetcsu1HiToLw == 1)) {
        /* Transition: '<S1>:28' */
        /* Transition: '<S1>:30' */
        lcetcsu1GainChngInGearShft = false;

        /* Transition: '<S1>:31' */
      } else {
        /* Transition: '<S1>:29' */
        lcetcsu1GainChngInGearShft = true;
      }

      /* Transition: '<S1>:20' */
    } else {
      /* Transition: '<S1>:22' */
      lcetcsu1GainChngInGearShft = true;
    }

    /* Transition: '<S1>:19' */
  } else {
    /* Transition: '<S1>:18' */
    lcetcsu1GainChngInGearShft = false;
  }

  /* End of Chart: '<Root>/Chart' */

  /* Chart: '<Root>/Chart1' incorporates:
   *  UnitDelay: '<Root>/Unit Delay'
   */
  /* Transition: '<S1>:17' */
  /* Gateway: Chart1 */
  /* During: Chart1 */
  /* Entry Internal: Chart1 */
  /* Transition: '<S2>:11' */
  /* comment */
  if ((localDW->UnitDelay_DSTATE.lcetcsu1GainChngInGearShft == 1) &&
      (lcetcsu1GainChngInGearShft == 0)) {
    /* Transition: '<S2>:10' */
    /* Transition: '<S2>:9' */
    lcetcsu1GainTrnsAftrGearShft = true;

    /* Transition: '<S2>:8' */
  } else {
    /* Transition: '<S2>:7' */
    lcetcsu1GainTrnsAftrGearShft =
      localDW->UnitDelay_DSTATE.lcetcsu1GainTrnsAftrGearShft;
  }

  /* Transition: '<S2>:6' */
  if (lcetcsu1GainTrnsAftrGearShft == 1) {
    /* Transition: '<S2>:22' */
    /* Transition: '<S2>:21' */
    /*   */
    if (((localB->lcetcsu8GainTrnsTm >= ((uint8_T)U8ETCSCpGainTrnsTmAftrGearShft))
         || (rtu_ETCS_CYL_1ST->lcetcsu1Cyl1st == 1)) ||
        (rtu_ETCS_H2L->lcetcsu1HiToLw == 1)) {
      /* Transition: '<S2>:43' */
      /* Transition: '<S2>:38' */
      lcetcsu1GainTrnsAftrGearShft = false;
      localB->lcetcsu8GainTrnsTm = 0U;

      /* Transition: '<S2>:41' */
    } else {
      /* Transition: '<S2>:42' */
      tmp = localDW->UnitDelay_DSTATE.lcetcsu8GainTrnsTm + 1;
      if (tmp > 255) {
        tmp = 255;
      }

      localB->lcetcsu8GainTrnsTm = (uint8_T)tmp;
    }

    /* Transition: '<S2>:44' */
  } else {
    /* Transition: '<S2>:19' */
    localB->lcetcsu8GainTrnsTm = 0U;
  }

  /* End of Chart: '<Root>/Chart1' */

  /* BusCreator: '<Root>/Bus Creator' */
  /* Transition: '<S2>:18' */
  rty_ETCS_GAIN_GEAR_SHFT->lcetcsu1GainChngInGearShft =
    lcetcsu1GainChngInGearShft;
  rty_ETCS_GAIN_GEAR_SHFT->lcetcsu1GainTrnsAftrGearShft =
    lcetcsu1GainTrnsAftrGearShft;
  rty_ETCS_GAIN_GEAR_SHFT->lcetcsu8GainTrnsTm = localB->lcetcsu8GainTrnsTm;

  /* Update for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = *rty_ETCS_GAIN_GEAR_SHFT;
}

/* Model initialize function */
void LCETCS_vDctGainChngInGearShft_initialize(const rtTimingBridge *timingBridge)
{
  /* Registration code */
  LCETCS_vDctGainChngInGearShft_TimingBrdg = timingBridge;
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
