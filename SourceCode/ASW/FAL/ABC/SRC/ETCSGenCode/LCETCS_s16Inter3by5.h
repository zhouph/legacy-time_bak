/*
 * File: LCETCS_s16Inter3by5.h
 *
 * Code generated for Simulink model 'LCETCS_s16Inter3by5'.
 *
 * Model version                  : 1.258
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:48:33 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_s16Inter3by5_h_
#define RTW_HEADER_LCETCS_s16Inter3by5_h_
#ifndef LCETCS_s16Inter3by5_COMMON_INCLUDES_
# define LCETCS_s16Inter3by5_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_s16Inter3by5_COMMON_INCLUDES_ */

#include "LCETCS_s16Inter3by5_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Model reference registration function */
extern void LCETCS_s16Inter3by5_initialize(void);
extern void LCETCS_s16Inter3by5_Init(int16_T *rty_Out);
extern void LCETCS_s16Inter3by5(int16_T rtu_x, int16_T rtu_x1, int16_T rtu_x2,
  int16_T rtu_x3, int16_T rtu_y, int16_T rtu_y1, int16_T rtu_y2, int16_T rtu_y3,
  int16_T rtu_y4, int16_T rtu_y5, int16_T rtu_z11, int16_T rtu_z12, int16_T
  rtu_z13, int16_T rtu_z21, int16_T rtu_z22, int16_T rtu_z23, int16_T rtu_z31,
  int16_T rtu_z32, int16_T rtu_z33, int16_T rtu_z41, int16_T rtu_z42, int16_T
  rtu_z43, int16_T rtu_z51, int16_T rtu_z52, int16_T rtu_z53, int16_T *rty_Out);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_s16Inter3by5'
 * '<S1>'   : 'LCETCS_s16Inter3by5/Chart1'
 * '<S2>'   : 'LCETCS_s16Inter3by5/Chart3'
 */
#endif                                 /* RTW_HEADER_LCETCS_s16Inter3by5_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
