/*
 * File: LCETCS_vCalAdptByWhlSpn.c
 *
 * Code generated for Simulink model 'LCETCS_vCalAdptByWhlSpn'.
 *
 * Model version                  : 1.197
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:51:13 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalAdptByWhlSpn.h"
#include "LCETCS_vCalAdptByWhlSpn_private.h"

/* Output and update for referenced model: 'LCETCS_vCalAdptByWhlSpn' */
void LCETCS_vCalAdptByWhlSpn(const TypeETCSGainStruct *rtu_ETCS_CTL_GAINS, const
  TypeETCSCtlErrStruct *rtu_ETCS_CTL_ERR, int32_T *rty_lcetcss32AdptByWhlSpn)
{
  /* Product: '<Root>/Divide2' incorporates:
   *  Product: '<Root>/Divide1'
   */
  *rty_lcetcss32AdptByWhlSpn = (rtu_ETCS_CTL_ERR->lcetcss16CtlErr *
    rtu_ETCS_CTL_GAINS->lcetcss16Igain) / 10;

  /* Saturate: '<Root>/Saturation1' */
  if ((*rty_lcetcss32AdptByWhlSpn) > 100000) {
    *rty_lcetcss32AdptByWhlSpn = 100000;
  } else {
    if ((*rty_lcetcss32AdptByWhlSpn) < -100000) {
      *rty_lcetcss32AdptByWhlSpn = -100000;
    }
  }

  /* End of Saturate: '<Root>/Saturation1' */
}

/* Model initialize function */
void LCETCS_vCalAdptByWhlSpn_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
