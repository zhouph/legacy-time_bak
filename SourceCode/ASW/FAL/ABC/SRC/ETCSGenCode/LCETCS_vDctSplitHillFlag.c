/*
 * File: LCETCS_vDctSplitHillFlag.c
 *
 * Code generated for Simulink model 'LCETCS_vDctSplitHillFlag'.
 *
 * Model version                  : 1.378
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:58:13 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctSplitHillFlag.h"
#include "LCETCS_vDctSplitHillFlag_private.h"

/* Initial conditions for referenced model: 'LCETCS_vDctSplitHillFlag' */
void LCETCS_vDctSplitHillFlag_Init(boolean_T *rty_lcetcsu1SplitHillDct,
  DW_LCETCS_vDctSplitHillFlag_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = false;

  /* InitializeConditions for Chart: '<Root>/Chart' */
  *rty_lcetcsu1SplitHillDct = false;
}

/* Output and update for referenced model: 'LCETCS_vDctSplitHillFlag' */
void LCETCS_vDctSplitHillFlag(int16_T rtu_lcetcss16SplitHillDctCnt, int16_T
  rtu_lcetcss16GradientOfHill, boolean_T *rty_lcetcsu1SplitHillDct,
  DW_LCETCS_vDctSplitHillFlag_f_T *localDW)
{
  /* Chart: '<Root>/Chart' incorporates:
   *  UnitDelay: '<Root>/Unit Delay'
   */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:9' */
  /* comment */
  if ((rtu_lcetcss16SplitHillDctCnt >= ((uint8_T)U8ETCSCpHillDctTime)) ||
      (rtu_lcetcss16GradientOfHill >= ((uint8_T)GRAD_10_DEG))) {
    /* Transition: '<S1>:10' */
    /* Transition: '<S1>:73' */
    *rty_lcetcsu1SplitHillDct = true;

    /* Transition: '<S1>:76' */
    /* Transition: '<S1>:77' */
  } else {
    /* Transition: '<S1>:13' */
    if ((rtu_lcetcss16SplitHillDctCnt <= ((uint8_T)U8ETCSCpHillClrTime)) &&
        (rtu_lcetcss16GradientOfHill <= ((uint8_T)GRAD_8_DEG))) {
      /* Transition: '<S1>:62' */
      /* Transition: '<S1>:75' */
      *rty_lcetcsu1SplitHillDct = false;

      /* Transition: '<S1>:77' */
    } else {
      /* Transition: '<S1>:71' */
      *rty_lcetcsu1SplitHillDct = localDW->UnitDelay_DSTATE;
    }
  }

  /* End of Chart: '<Root>/Chart' */

  /* Update for UnitDelay: '<Root>/Unit Delay' */
  /* Transition: '<S1>:68' */
  localDW->UnitDelay_DSTATE = *rty_lcetcsu1SplitHillDct;
}

/* Model initialize function */
void LCETCS_vDctSplitHillFlag_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
