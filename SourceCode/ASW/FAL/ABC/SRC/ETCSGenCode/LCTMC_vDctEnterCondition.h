/*
 * File: LCTMC_vDctEnterCondition.h
 *
 * Code generated for Simulink model 'LCTMC_vDctEnterCondition'.
 *
 * Model version                  : 1.224
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:53:20 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCTMC_vDctEnterCondition_h_
#define RTW_HEADER_LCTMC_vDctEnterCondition_h_
#ifndef LCTMC_vDctEnterCondition_COMMON_INCLUDES_
# define LCTMC_vDctEnterCondition_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCTMC_vDctEnterCondition_COMMON_INCLUDES_ */

#include "LCTMC_vDctEnterCondition_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Model reference registration function */
extern void LCTMC_vDctEnterCondition_initialize(void);
extern void LCTMC_vDctEnterCondition_Init(boolean_T *rty_lctmcu1NonCndTarReqGear);
extern void LCTMC_vDctEnterCondition(boolean_T rtu_lctmcu1DriveGearMode, const
  TypeETCSExtDctStruct *rtu_ETCS_EXT_DCT, uint8_T rtu_lctmcu8_BSTGRReqGearOld,
  const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, boolean_T rtu_lcu1TmcInhibitMode,
  boolean_T *rty_lctmcu1NonCndTarReqGear);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCTMC_vDctEnterCondition'
 * '<S1>'   : 'LCTMC_vDctEnterCondition/Chart1'
 */
#endif                                 /* RTW_HEADER_LCTMC_vDctEnterCondition_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
