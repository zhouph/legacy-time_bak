/*
 * File: LCETCS_vCalDeldSigDepOnEngSpd_private.h
 *
 * Code generated for Simulink model 'LCETCS_vCalDeldSigDepOnEngSpd'.
 *
 * Model version                  : 1.99
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:59:03 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalDeldSigDepOnEngSpd_private_h_
#define RTW_HEADER_LCETCS_vCalDeldSigDepOnEngSpd_private_h_
#include "rtwtypes.h"
#include "model_reference_types.h"

/* Includes for objects with custom storage classes. */
#include "../ETCSGenCodeHeader/ETCSHeader4Import.h"

/*
 * Generate compile time checks that imported macros for parameters
 * with storage class "ImportedDefine" are defined
 */
#ifndef S16ETCSCpEngSpd_1
#error The variable for the parameter "S16ETCSCpEngSpd_1" is not defined
#endif

#ifndef S16ETCSCpEngSpd_2
#error The variable for the parameter "S16ETCSCpEngSpd_2" is not defined
#endif

#ifndef S16ETCSCpEngSpd_3
#error The variable for the parameter "S16ETCSCpEngSpd_3" is not defined
#endif

#ifndef S16ETCSCpEngSpd_4
#error The variable for the parameter "S16ETCSCpEngSpd_4" is not defined
#endif

#ifndef S16ETCSCpEngSpd_5
#error The variable for the parameter "S16ETCSCpEngSpd_5" is not defined
#endif

#ifndef U8ETCSCpTrqDel_1
#error The variable for the parameter "U8ETCSCpTrqDel_1" is not defined
#endif

#ifndef U8ETCSCpTrqDel_2
#error The variable for the parameter "U8ETCSCpTrqDel_2" is not defined
#endif

#ifndef U8ETCSCpTrqDel_3
#error The variable for the parameter "U8ETCSCpTrqDel_3" is not defined
#endif

#ifndef U8ETCSCpTrqDel_4
#error The variable for the parameter "U8ETCSCpTrqDel_4" is not defined
#endif

#ifndef U8ETCSCpTrqDel_5
#error The variable for the parameter "U8ETCSCpTrqDel_5" is not defined
#endif
#endif                                 /* RTW_HEADER_LCETCS_vCalDeldSigDepOnEngSpd_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
