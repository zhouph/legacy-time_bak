/*
 * File: LCETCS_vCalGradientofHill.c
 *
 * Code generated for Simulink model 'LCETCS_vCalGradientofHill'.
 *
 * Model version                  : 1.323
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:13:30 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalGradientofHill.h"
#include "LCETCS_vCalGradientofHill_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalGradientofHill' */
void LCETCS_vCalGradientofHill_Init(boolean_T *rty_lcetcsu1InhibitGradient,
  boolean_T *rty_lcetcsu1DctHoldGradient, boolean_T *rty_lcetcsu1GradientTrstSet,
  DW_LCETCS_vCalGradientofHill_f_T *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctInhCndGradient' */
  LCETCS_vDctInhCndGradient_Init(rty_lcetcsu1InhibitGradient,
    &(localDW->LCETCS_vDctInhCndGradient_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctHoldingGradient' */
  LCETCS_vDctHoldingGradient_Init(rty_lcetcsu1DctHoldGradient,
    &(localDW->LCETCS_vDctHoldingGradient_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalGradientCnt' */
  LCETCS_vCalGradientCnt_Init(&(localDW->LCETCS_vCalGradientCnt_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctGradientTrust' */
  LCETCS_vCalGradientTrust_Init(rty_lcetcsu1GradientTrstSet);

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalFnlGradient' */
  LCETCS_vCalFnlGradient_Init(&(localDW->LCETCS_vCalFnlGradient_DWORK1.rtb),
    &(localDW->LCETCS_vCalFnlGradient_DWORK1.rtdw));
}

/* Start for referenced model: 'LCETCS_vCalGradientofHill' */
void LCETCS_vCalGradientofHill_Start(DW_LCETCS_vCalGradientofHill_f_T *localDW)
{
  /* Start for ModelReference: '<Root>/LCETCS_vCalFnlGradient' */
  LCETCS_vCalFnlGradient_Start(&(localDW->LCETCS_vCalFnlGradient_DWORK1.rtb));
}

/* Output and update for referenced model: 'LCETCS_vCalGradientofHill' */
void LCETCS_vCalGradientofHill(int16_T rtu_lcetcss16LongAccFilter, const
  TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC, int16_T rtu_lcetcss16VehSpd, const
  TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, int16_T *rty_lcetcss16GradientOfHill,
  int16_T *rty_lcetcss16GradientTrstCnt, boolean_T *rty_lcetcsu1InhibitGradient,
  boolean_T *rty_lcetcsu1DctHoldGradient, int16_T *rty_lcetcss16HoldGradientCnt,
  int16_T *rty_lcetcss16HoldRefGradientTime, boolean_T
  *rty_lcetcsu1GradientTrstSet, DW_LCETCS_vCalGradientofHill_f_T *localDW)
{
  /* local block i/o variables */
  int16_T rtb_lcetcss16AbsLongAccDiff;

  /* ModelReference: '<Root>/LCETCS_vDctInhCndGradient' */
  LCETCS_vDctInhCndGradient(rtu_lcetcss16VehSpd, rty_lcetcsu1InhibitGradient,
    &(localDW->LCETCS_vDctInhCndGradient_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vDctHoldingGradient' */
  LCETCS_vDctHoldingGradient((*rty_lcetcsu1InhibitGradient),
    rtu_lcetcss16LongAccFilter, rtu_ETCS_VEH_ACC, rtu_ETCS_ESC_SIG,
    rtu_lcetcss16VehSpd, rty_lcetcss16HoldGradientCnt,
    rty_lcetcsu1DctHoldGradient, rty_lcetcss16HoldRefGradientTime,
    &(localDW->LCETCS_vDctHoldingGradient_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalGradientCnt' */
  LCETCS_vCalGradientCnt(rtu_lcetcss16LongAccFilter, rtu_ETCS_VEH_ACC,
    rty_lcetcss16GradientTrstCnt, &rtb_lcetcss16AbsLongAccDiff,
    &(localDW->LCETCS_vCalGradientCnt_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vDctGradientTrust' */
  LCETCS_vCalGradientTrust((*rty_lcetcsu1InhibitGradient),
    (*rty_lcetcsu1DctHoldGradient), (*rty_lcetcss16GradientTrstCnt),
    rty_lcetcsu1GradientTrstSet);

  /* ModelReference: '<Root>/LCETCS_vCalFnlGradient' */
  LCETCS_vCalFnlGradient((*rty_lcetcsu1GradientTrstSet),
    rtb_lcetcss16AbsLongAccDiff, rty_lcetcss16GradientOfHill,
    &(localDW->LCETCS_vCalFnlGradient_DWORK1.rtb),
    &(localDW->LCETCS_vCalFnlGradient_DWORK1.rtdw));
}

/* Model initialize function */
void LCETCS_vCalGradientofHill_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalFnlGradient' */
  LCETCS_vCalFnlGradient_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalGradientCnt' */
  LCETCS_vCalGradientCnt_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctGradientTrust' */
  LCETCS_vCalGradientTrust_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctHoldingGradient' */
  LCETCS_vDctHoldingGradient_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctInhCndGradient' */
  LCETCS_vDctInhCndGradient_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
