/*
 * File: LCETCS_s16CntIfSigSmlThanTh.c
 *
 * Code generated for Simulink model 'LCETCS_s16CntIfSigSmlThanTh'.
 *
 * Model version                  : 1.67
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:54:48 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_s16CntIfSigSmlThanTh.h"
#include "LCETCS_s16CntIfSigSmlThanTh_private.h"

/* Initial conditions for referenced model: 'LCETCS_s16CntIfSigSmlThanTh' */
void LCETCS_s16CntIfSigSmlThanTh_Init(int16_T *rty_Counter)
{
  /* InitializeConditions for Chart: '<Root>/LCETCS_s16CntIfSigSmlThanTh' */
  *rty_Counter = 0;
}

/* Output and update for referenced model: 'LCETCS_s16CntIfSigSmlThanTh' */
void LCETCS_s16CntIfSigSmlThanTh(boolean_T rtu_CounterReset, int16_T
  rtu_BaseSignal, int16_T rtu_CounterUpTh, int16_T rtu_CounterDownTh, int16_T
  rtu_CounterOld, int16_T *rty_Counter)
{
  /* Chart: '<Root>/LCETCS_s16CntIfSigSmlThanTh' */
  /* Gateway: LCETCS_s16CntIfSigSmlThanTh */
  /* During: LCETCS_s16CntIfSigSmlThanTh */
  /* Entry Internal: LCETCS_s16CntIfSigSmlThanTh */
  /* Transition: '<S1>:14' */
  /* comment */
  if ((rtu_CounterReset == 1) || (rtu_CounterUpTh >= rtu_CounterDownTh)) {
    /* Transition: '<S1>:34' */
    /* Transition: '<S1>:36' */
    *rty_Counter = 0;

    /* Transition: '<S1>:84' */
    /* Transition: '<S1>:85' */
    /* Transition: '<S1>:82' */
  } else {
    /* Transition: '<S1>:64' */
    if ((rtu_BaseSignal < rtu_CounterUpTh) && (rtu_CounterOld < 32760)) {
      /* Transition: '<S1>:16' */
      /* Transition: '<S1>:21' */
      *rty_Counter = (int16_T)(rtu_CounterOld + 1);

      /* Transition: '<S1>:85' */
      /* Transition: '<S1>:82' */
    } else {
      /* Transition: '<S1>:15' */
      if ((rtu_BaseSignal > rtu_CounterDownTh) && (rtu_CounterOld > 0)) {
        /* Transition: '<S1>:23' */
        /* Transition: '<S1>:22' */
        *rty_Counter = (int16_T)(rtu_CounterOld - 1);

        /* Transition: '<S1>:82' */
      } else {
        /* Transition: '<S1>:29' */
        *rty_Counter = rtu_CounterOld;
      }
    }
  }

  /* End of Chart: '<Root>/LCETCS_s16CntIfSigSmlThanTh' */
  /* Transition: '<S1>:46' */
}

/* Model initialize function */
void LCETCS_s16CntIfSigSmlThanTh_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
