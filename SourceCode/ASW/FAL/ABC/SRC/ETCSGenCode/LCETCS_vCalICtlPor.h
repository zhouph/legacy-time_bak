/*
 * File: LCETCS_vCalICtlPor.h
 *
 * Code generated for Simulink model 'LCETCS_vCalICtlPor'.
 *
 * Model version                  : 1.197
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:51:23 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalICtlPor_h_
#define RTW_HEADER_LCETCS_vCalICtlPor_h_
#ifndef LCETCS_vCalICtlPor_COMMON_INCLUDES_
# define LCETCS_vCalICtlPor_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalICtlPor_COMMON_INCLUDES_ */

#include "LCETCS_vCalICtlPor_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Block states (auto storage) for model 'LCETCS_vCalICtlPor' */
typedef struct {
  int32_T UnitDelay_DSTATE;            /* '<Root>/Unit Delay' */
} DW_LCETCS_vCalICtlPor_f_T;

typedef struct {
  DW_LCETCS_vCalICtlPor_f_T rtdw;
} MdlrefDW_LCETCS_vCalICtlPor_T;

/* Model reference registration function */
extern void LCETCS_vCalICtlPor_initialize(void);
extern void LCETCS_vCalICtlPor_Init(DW_LCETCS_vCalICtlPor_f_T *localDW);
extern void LCETCS_vCalICtlPor(const TypeETCSLdTrqStruct *rtu_ETCS_LD_TRQ, const
  TypeETCSIValRstStruct *rtu_ETCS_IVAL_RST, const TypeETCSH2LStruct
  *rtu_ETCS_H2L, int32_T rtu_lcetcss32AdptAmt, int32_T rtu_lcetcss32CtlCmdMax,
  const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, int32_T *rty_lcetcss32ICtlPor,
  DW_LCETCS_vCalICtlPor_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalICtlPor'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalICtlPor_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
