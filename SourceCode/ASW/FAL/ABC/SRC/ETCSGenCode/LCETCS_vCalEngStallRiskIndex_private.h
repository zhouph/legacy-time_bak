/*
 * File: LCETCS_vCalEngStallRiskIndex_private.h
 *
 * Code generated for Simulink model 'LCETCS_vCalEngStallRiskIndex'.
 *
 * Model version                  : 1.263
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:05:45 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalEngStallRiskIndex_private_h_
#define RTW_HEADER_LCETCS_vCalEngStallRiskIndex_private_h_
#include "rtwtypes.h"
#include "model_reference_types.h"

/* Includes for objects with custom storage classes. */
#include "../ETCSGenCodeHeader/ETCSHeader4Import.h"

/*
 * Generate compile time checks that imported macros for parameters
 * with storage class "ImportedDefine" are defined
 */
#ifndef VREF_10_KPH
#error The variable for the parameter "VREF_10_KPH" is not defined
#endif

#ifndef VREF_12_5_KPH
#error The variable for the parameter "VREF_12_5_KPH" is not defined
#endif

#ifndef VREF_6_KPH
#error The variable for the parameter "VREF_6_KPH" is not defined
#endif

extern const int16_T rtCP_pooled_T1tcrUJCQflL;
extern const int16_T rtCP_pooled_Krm7oDVfoe0C;
extern const int16_T rtCP_pooled_ZjYULP70HcKf;
extern const int16_T rtCP_pooled_DVQxxQvdoG06;
extern const int16_T rtCP_pooled_9AzLBbdxaNKp;
extern const int16_T rtCP_pooled_cQQw9dgYpywR;
extern const int16_T rtCP_pooled_gP6uynpoCzWv;

#define rtCP_HighEngineStallRiskbyVehicleSpeed_Value rtCP_pooled_T1tcrUJCQflL/* Computed Parameter: HighEngineStallRiskbyVehicleSpeed_Value
                                                                      * Referenced by: '<S1>/High Engine Stall Risk by Vehicle Speed'
                                                                      */
#define rtCP_MidEngineStallRiskbyVehicleSpeed_Value rtCP_pooled_Krm7oDVfoe0C/* Computed Parameter: MidEngineStallRiskbyVehicleSpeed_Value
                                                                      * Referenced by: '<S1>/Mid Engine Stall Risk by Vehicle Speed'
                                                                      */
#define rtCP_LowEngineStallRiskbyVehicleSpeed_Value rtCP_pooled_ZjYULP70HcKf/* Computed Parameter: LowEngineStallRiskbyVehicleSpeed_Value
                                                                      * Referenced by: '<S1>/Low Engine Stall Risk by Vehicle Speed'
                                                                      */
#define rtCP_ChangedRPM4020ms_Value    rtCP_pooled_DVQxxQvdoG06  /* Computed Parameter: ChangedRPM4020ms_Value
                                                                  * Referenced by: '<S1>/Changed RPM -40//20ms'
                                                                  */
#define rtCP_ChangedRPM2020ms_Value    rtCP_pooled_9AzLBbdxaNKp  /* Computed Parameter: ChangedRPM2020ms_Value
                                                                  * Referenced by: '<S1>/Changed RPM-20//20ms'
                                                                  */
#define rtCP_ChangedRPM1020ms_Value    rtCP_pooled_cQQw9dgYpywR  /* Computed Parameter: ChangedRPM1020ms_Value
                                                                  * Referenced by: '<S1>/Changed RPM-10//20ms'
                                                                  */
#define rtCP_HighEngineStallRiskbyEngineSpeed_Value rtCP_pooled_T1tcrUJCQflL/* Computed Parameter: HighEngineStallRiskbyEngineSpeed_Value
                                                                      * Referenced by: '<S1>/High Engine Stall Risk by Engine Speed'
                                                                      */
#define rtCP_MidEngineStallRiskbyEngineSpeed_Value rtCP_pooled_ZjYULP70HcKf/* Computed Parameter: MidEngineStallRiskbyEngineSpeed_Value
                                                                      * Referenced by: '<S1>/Mid Engine Stall Risk by Engine Speed'
                                                                      */
#define rtCP_LowEngineStallRiskbyEngineSpeed_Value rtCP_pooled_gP6uynpoCzWv/* Computed Parameter: LowEngineStallRiskbyEngineSpeed_Value
                                                                      * Referenced by: '<S1>/Low Engine Stall Risk by Engine Speed'
                                                                      */
#endif                                 /* RTW_HEADER_LCETCS_vCalEngStallRiskIndex_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
