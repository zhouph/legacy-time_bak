/*
 * File: LCETCS_s16Inter2Point.c
 *
 * Code generated for Simulink model 'LCETCS_s16Inter2Point'.
 *
 * Model version                  : 1.123
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:47:25 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_s16Inter2Point.h"
#include "LCETCS_s16Inter2Point_private.h"

/* Initial conditions for atomic system: '<Root>/chart' */
void LCETCS_s16Inter2Point_chart_Init(int16_T *rty_y)
{
  *rty_y = 0;
}

/* Output and update for atomic system: '<Root>/chart' */
void LCETCS_s16Inter2Point_chart(int16_T rtu_x, const int16_T rtu_x_axis[2],
  const int16_T rtu_y_map[2], int16_T *rty_y)
{
  /* Gateway: chart */
  /* During: chart */
  /* Entry Internal: chart */
  /* Transition: '<S1>:4' */
  if (rtu_x < rtu_x_axis[0]) {
    /* Transition: '<S1>:19' */
    /* Transition: '<S1>:41' */
    *rty_y = rtu_y_map[0];

    /* Transition: '<S1>:185' */
    /* Transition: '<S1>:182' */
  } else {
    /* Transition: '<S1>:36' */
    if (rtu_x < rtu_x_axis[1]) {
      /* Transition: '<S1>:38' */
      /* Transition: '<S1>:115' */
      *rty_y = (int16_T)(((int16_T)(((((rtu_x - rtu_x_axis[0]) << 15) /
        (rtu_x_axis[1] - rtu_x_axis[0])) * (rtu_y_map[1] - rtu_y_map[0])) >> 15))
                         + rtu_y_map[0]);

      /* Transition: '<S1>:182' */
    } else {
      /* Transition: '<S1>:69' */
      *rty_y = rtu_y_map[1];
    }
  }
}

/* Initial conditions for referenced model: 'LCETCS_s16Inter2Point' */
void LCETCS_s16Inter2Point_Init(int16_T *rty_y)
{
  /* InitializeConditions for Chart: '<Root>/chart' */
  LCETCS_s16Inter2Point_chart_Init(rty_y);
}

/* Output and update for referenced model: 'LCETCS_s16Inter2Point' */
void LCETCS_s16Inter2Point(int16_T rtu_x, int16_T rtu_x1, int16_T rtu_x2,
  int16_T rtu_y1, int16_T rtu_y2, int16_T *rty_y)
{
  int16_T rtb_x_axis[2];
  int16_T rtb_y_map[2];

  /* SignalConversion: '<Root>/TmpSignal ConversionAtchartInport2' */
  rtb_x_axis[0] = rtu_x1;
  rtb_x_axis[1] = rtu_x2;

  /* SignalConversion: '<Root>/TmpSignal ConversionAtchartInport3' */
  rtb_y_map[0] = rtu_y1;
  rtb_y_map[1] = rtu_y2;

  /* Chart: '<Root>/chart' */
  LCETCS_s16Inter2Point_chart(rtu_x, rtb_x_axis, rtb_y_map, rty_y);
}

/* Model initialize function */
void LCETCS_s16Inter2Point_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
