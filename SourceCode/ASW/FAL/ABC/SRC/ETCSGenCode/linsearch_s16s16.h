/*
 * File: linsearch_s16s16.h
 *
 * Code generated for Simulink model 'LCETCS_vCalRefTrqAtStrtOfCtl'.
 *
 * Model version                  : 1.193
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:54:40 2015
 */

#ifndef SHARE_linsearch_s16s16
#define SHARE_linsearch_s16s16
#include "rtwtypes.h"

extern int16_T linsearch_s16s16(int16_T u, const int16_T bp[], uint32_T
  startIndex);

#endif

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
