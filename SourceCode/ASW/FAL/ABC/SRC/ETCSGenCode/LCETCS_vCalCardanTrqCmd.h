/*
 * File: LCETCS_vCalCardanTrqCmd.h
 *
 * Code generated for Simulink model 'LCETCS_vCalCardanTrqCmd'.
 *
 * Model version                  : 1.227
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:00:57 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalCardanTrqCmd_h_
#define RTW_HEADER_LCETCS_vCalCardanTrqCmd_h_
#ifndef LCETCS_vCalCardanTrqCmd_COMMON_INCLUDES_
# define LCETCS_vCalCardanTrqCmd_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalCardanTrqCmd_COMMON_INCLUDES_ */

#include "LCETCS_vCalCardanTrqCmd_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vSumCtlCmds.h"
#include "LCETCS_vDctCtlCmdStat.h"
#include "LCETCS_vCalPCtlPor.h"
#include "LCETCS_vCalICtlPor.h"
#include "LCETCS_vCalCtlCmdMax.h"
#include "LCETCS_vCalAdptByWhlSpn.h"
#include "LCETCS_vCalAdptByDelYaw.h"
#include "LCETCS_vCalAdptAmt.h"

/* Block signals for model 'LCETCS_vCalCardanTrqCmd' */
typedef struct {
  boolean_T lcetcsu1CtlCmdHitMax;      /* '<Root>/LCETCS_vDctCtlCmdStat' */
  boolean_T lcetcsu1CtlCmdInc;         /* '<Root>/LCETCS_vDctCtlCmdStat' */
} B_LCETCS_vCalCardanTrqCmd_c_T;

/* Block states (auto storage) for model 'LCETCS_vCalCardanTrqCmd' */
typedef struct {
  MdlrefDW_LCETCS_vCalAdptByDelYaw_T LCETCS_vCalAdptByDelYaw_DWORK1;/* '<Root>/LCETCS_vCalAdptByDelYaw' */
  MdlrefDW_LCETCS_vCalICtlPor_T LCETCS_vCalICtlPor_DWORK1;/* '<Root>/LCETCS_vCalICtlPor' */
  MdlrefDW_LCETCS_vDctCtlCmdStat_T LCETCS_vDctCtlCmdStat_DWORK1;/* '<Root>/LCETCS_vDctCtlCmdStat' */
} DW_LCETCS_vCalCardanTrqCmd_f_T;

typedef struct {
  B_LCETCS_vCalCardanTrqCmd_c_T rtb;
  DW_LCETCS_vCalCardanTrqCmd_f_T rtdw;
} MdlrefDW_LCETCS_vCalCardanTrqCmd_T;

/* Model reference registration function */
extern void LCETCS_vCalCardanTrqCmd_initialize(void);
extern void LCETCS_vCalCardanTrqCmd_Init(B_LCETCS_vCalCardanTrqCmd_c_T *localB,
  DW_LCETCS_vCalCardanTrqCmd_f_T *localDW);
extern void LCETCS_vCalCardanTrqCmd(const TypeETCSLdTrqStruct *rtu_ETCS_LD_TRQ,
  const TypeETCSCtlErrStruct *rtu_ETCS_CTL_ERR, const TypeETCSIValRstStruct
  *rtu_ETCS_IVAL_RST, const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const
  TypeETCSCtlCmdStruct *rtu_ETCS_CTL_CMD_OLD, const TypeETCSCrdnTrqStruct
  *rtu_ETCS_CRDN_TRQ, const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const
  TypeETCSDrvMdlStruct *rtu_ETCS_DRV_MDL, const TypeETCSH2LStruct *rtu_ETCS_H2L,
  const TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN, const TypeETCSGainStruct
  *rtu_ETCS_CTL_GAINS, TypeETCSCtlCmdStruct *rty_ETCS_CTL_CMD,
  B_LCETCS_vCalCardanTrqCmd_c_T *localB, DW_LCETCS_vCalCardanTrqCmd_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalCardanTrqCmd'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalCardanTrqCmd_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
