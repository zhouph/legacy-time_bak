/*
 * File: LCETCS_vCalLmtCrng.h
 *
 * Code generated for Simulink model 'LCETCS_vCalLmtCrng'.
 *
 * Model version                  : 1.76
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:59:43 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalLmtCrng_h_
#define RTW_HEADER_LCETCS_vCalLmtCrng_h_
#ifndef LCETCS_vCalLmtCrng_COMMON_INCLUDES_
# define LCETCS_vCalLmtCrng_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalLmtCrng_COMMON_INCLUDES_ */

#include "LCETCS_vCalLmtCrng_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vDctFallingEdge.h"

/* Block states (auto storage) for model 'LCETCS_vCalLmtCrng' */
typedef struct {
  MdlrefDW_LCETCS_vDctFallingEdge_T LCETCS_vDctFallingEdge_DWORK1;/* '<Root>/LCETCS_vDctFallingEdge' */
} DW_LCETCS_vCalLmtCrng_f_T;

typedef struct {
  DW_LCETCS_vCalLmtCrng_f_T rtdw;
} MdlrefDW_LCETCS_vCalLmtCrng_T;

/* Model reference registration function */
extern void LCETCS_vCalLmtCrng_initialize(void);
extern void LCETCS_vCalLmtCrng_Init(boolean_T *rty_lcetcsu1DctLmtCrng, boolean_T
  *rty_lcetcsu1DctLmtCrngFalEdg, DW_LCETCS_vCalLmtCrng_f_T *localDW);
extern void LCETCS_vCalLmtCrng(int16_T rtu_lcetcss16LmtCrngDltYaw, const
  TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, boolean_T *rty_lcetcsu1DctLmtCrng,
  boolean_T *rty_lcetcsu1DctLmtCrngFalEdg, DW_LCETCS_vCalLmtCrng_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalLmtCrng'
 * '<S1>'   : 'LCETCS_vCalLmtCrng/Chart'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalLmtCrng_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
