/*
 * File: LCTMC_vCtrlTargetGear.h
 *
 * Code generated for Simulink model 'LCTMC_vCtrlTargetGear'.
 *
 * Model version                  : 1.638
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:53:34 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCTMC_vCtrlTargetGear_h_
#define RTW_HEADER_LCTMC_vCtrlTargetGear_h_
#ifndef LCTMC_vCtrlTargetGear_COMMON_INCLUDES_
# define LCTMC_vCtrlTargetGear_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCTMC_vCtrlTargetGear_COMMON_INCLUDES_ */

#include "LCTMC_vCtrlTargetGear_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Block states (auto storage) for model 'LCTMC_vCtrlTargetGear' */
typedef struct {
  TypeTMCReqGearStruct UnitDelay1_DSTATE;/* '<Root>/Unit Delay1' */
} DW_LCTMC_vCtrlTargetGear_f_T;

typedef struct {
  DW_LCTMC_vCtrlTargetGear_f_T rtdw;
} MdlrefDW_LCTMC_vCtrlTargetGear_T;

/* Model reference registration function */
extern void LCTMC_vCtrlTargetGear_initialize(const rtTimingBridge *timingBridge);
extern const TypeTMCReqGearStruct LCTMC_vCtrlTargetGear_rtZTypeTMCReqGearStruct;/* TypeTMCReqGearStruct ground */
extern void LCTMC_vCtrlTargetGear_Init(TypeTMCReqGearStruct
  *rty_TMC_REQ_GEAR_STRUCT, DW_LCTMC_vCtrlTargetGear_f_T *localDW);
extern void LCTMC_vCtrlTargetGear(boolean_T rtu_lctmcu1GearShiftCountSet,
  boolean_T rtu_lctmcu1NonCndTarReqGear, const TypeETCSGearStruct
  *rtu_ETCS_GEAR_STRUCT, const TypeTMCRefLimVehSpdMap *rtu_TMC_REF_LIM_VS_MAP,
  const TypeTMCRefFinTarGearMap *rtu_TMC_REF_FIN_TAR_GEAR_MAP, int16_T
  rtu_lcetcss16VehSpd, const TypeETCSEngStruct *rtu_ETCS_ENG_STRUCT,
  TypeTMCReqGearStruct *rty_TMC_REQ_GEAR_STRUCT, DW_LCTMC_vCtrlTargetGear_f_T
  *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCTMC_vCtrlTargetGear'
 * '<S1>'   : 'LCTMC_vCtrlTargetGear/Chart1'
 */
#endif                                 /* RTW_HEADER_LCTMC_vCtrlTargetGear_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
