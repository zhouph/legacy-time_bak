/*
 * File: LCETCS_vCalVehAcc.c
 *
 * Code generated for Simulink model 'LCETCS_vCalVehAcc'.
 *
 * Model version                  : 1.509
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:07:51 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalVehAcc.h"
#include "LCETCS_vCalVehAcc_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalVehAcc' */
void LCETCS_vCalVehAcc_Init(B_LCETCS_vCalVehAcc_c_T *localB,
  DW_LCETCS_vCalVehAcc_f_T *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalVehAccByVehSpd' */
  LCETCS_vCalVehAccByVehSpd_Init
    (&(localDW->LCETCS_vCalVehAccByVehSpd_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalScaleDnAy' */
  LCETCS_vCalScaleDnAy_Init(&(localDW->LCETCS_vCalScaleDnAy_DWORK1.rtb));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalRefAxRepRdFric' */
  LCETCS_vCalRefAxRepRdFric_Init(&(localDW->LCETCS_vCalRefAxRepRdFric_DWORK1.rtb));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalCordRefRdFric' */
  LCETCS_vCalCordRefRdFric_Init(&localB->lcetcss16RefRdCfFricAtIce,
    &localB->lcetcss16RefRdCfFricAtSnow, &localB->lcetcss16RefRdCfFricAtAsphalt);
}

/* Start for referenced model: 'LCETCS_vCalVehAcc' */
void LCETCS_vCalVehAcc_Start(DW_LCETCS_vCalVehAcc_f_T *localDW)
{
  /* Start for ModelReference: '<Root>/LCETCS_vCalScaleDnAy' */
  LCETCS_vCalScaleDnAy_Start(&(localDW->LCETCS_vCalScaleDnAy_DWORK1.rtb));

  /* Start for ModelReference: '<Root>/LCETCS_vCalRefAxRepRdFric' */
  LCETCS_vCalRefAxRepRdFric_Start
    (&(localDW->LCETCS_vCalRefAxRepRdFric_DWORK1.rtb));
}

/* Output and update for referenced model: 'LCETCS_vCalVehAcc' */
void LCETCS_vCalVehAcc(int16_T rtu_lcetcss16VehSpd, const TypeETCSEscSigStruct
  *rtu_ETCS_ESC_SIG, const TypeETCSDrvMdlStruct *rtu_ETCS_DRV_MDL,
  TypeETCSVehAccStruct *rty_ETCS_VEH_ACC, B_LCETCS_vCalVehAcc_c_T *localB,
  DW_LCETCS_vCalVehAcc_f_T *localDW)
{
  /* local block i/o variables */
  int16_T rtb_lcetcss16VehAccByVehSpd;
  int16_T rtb_lcetcss16VehLongAcc4Ctl;
  int16_T rtb_lcetcss16ScaleDnAy;
  int16_T rtb_lcetcss16RsltntAcc;
  int16_T rtb_lcetcss16RefAxRdFricAtIce;
  int16_T rtb_lcetcss16RefAxRdFricAtSnow;
  int16_T rtb_lcetcss16RefAxRdFricAtAsphalt;

  /* ModelReference: '<Root>/LCETCS_vCalVehAccByVehSpd' */
  LCETCS_vCalVehAccByVehSpd(rtu_lcetcss16VehSpd, &rtb_lcetcss16VehAccByVehSpd,
                            &(localDW->LCETCS_vCalVehAccByVehSpd_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalVehLongAcc4Ctl' */
  LCETCS_vCalVehLongAcc4Ctl(rtb_lcetcss16VehAccByVehSpd, rtu_ETCS_ESC_SIG,
    &rtb_lcetcss16VehLongAcc4Ctl);

  /* ModelReference: '<Root>/LCETCS_vCalScaleDnAy' */
  LCETCS_vCalScaleDnAy(rtu_ETCS_ESC_SIG, &rtb_lcetcss16ScaleDnAy,
                       &(localDW->LCETCS_vCalScaleDnAy_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vCalRsltntAcc' */
  LCETCS_vCalRsltntAcc(rtb_lcetcss16VehLongAcc4Ctl, rtb_lcetcss16ScaleDnAy,
                       &rtb_lcetcss16RsltntAcc);

  /* ModelReference: '<Root>/LCETCS_vCalRefAxRepRdFric' */
  LCETCS_vCalRefAxRepRdFric(rtu_ETCS_DRV_MDL, &rtb_lcetcss16RefAxRdFricAtIce,
    &rtb_lcetcss16RefAxRdFricAtSnow, &rtb_lcetcss16RefAxRdFricAtAsphalt,
    &(localDW->LCETCS_vCalRefAxRepRdFric_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vCalCordRefRdFric' */
  LCETCS_vCalCordRefRdFric(rtb_lcetcss16ScaleDnAy, rtb_lcetcss16RefAxRdFricAtIce,
    rtb_lcetcss16RefAxRdFricAtSnow, rtb_lcetcss16RefAxRdFricAtAsphalt,
    &localB->lcetcss16RefRdCfFricAtIce, &localB->lcetcss16RefRdCfFricAtSnow,
    &localB->lcetcss16RefRdCfFricAtAsphalt);

  /* BusCreator: '<Root>/Bus Creator' */
  rty_ETCS_VEH_ACC->lcetcss16VehAccByVehSpd = rtb_lcetcss16VehAccByVehSpd;
  rty_ETCS_VEH_ACC->lcetcss16VehLongAcc4Ctl = rtb_lcetcss16VehLongAcc4Ctl;
  rty_ETCS_VEH_ACC->lcetcss16RsltntAcc = rtb_lcetcss16RsltntAcc;
  rty_ETCS_VEH_ACC->lcetcss16ScaleDnAy = rtb_lcetcss16ScaleDnAy;
  rty_ETCS_VEH_ACC->lcetcss16RefRdCfFricAtIce =
    localB->lcetcss16RefRdCfFricAtIce;
  rty_ETCS_VEH_ACC->lcetcss16RefRdCfFricAtSnow =
    localB->lcetcss16RefRdCfFricAtSnow;
  rty_ETCS_VEH_ACC->lcetcss16RefRdCfFricAtAsphalt =
    localB->lcetcss16RefRdCfFricAtAsphalt;
  rty_ETCS_VEH_ACC->lcetcss16RefAxRdFricAtIce = rtb_lcetcss16RefAxRdFricAtIce;
  rty_ETCS_VEH_ACC->lcetcss16RefAxRdFricAtSnow = rtb_lcetcss16RefAxRdFricAtSnow;
  rty_ETCS_VEH_ACC->lcetcss16RefAxRdFricAtAsphalt =
    rtb_lcetcss16RefAxRdFricAtAsphalt;
}

/* Model initialize function */
void LCETCS_vCalVehAcc_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalCordRefRdFric' */
  LCETCS_vCalCordRefRdFric_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalRefAxRepRdFric' */
  LCETCS_vCalRefAxRepRdFric_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalRsltntAcc' */
  LCETCS_vCalRsltntAcc_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalScaleDnAy' */
  LCETCS_vCalScaleDnAy_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalVehAccByVehSpd' */
  LCETCS_vCalVehAccByVehSpd_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalVehLongAcc4Ctl' */
  LCETCS_vCalVehLongAcc4Ctl_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
