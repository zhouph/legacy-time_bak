/*
 * File: LCETCS_vCalDrvLinMdl.h
 *
 * Code generated for Simulink model 'LCETCS_vCalDrvLinMdl'.
 *
 * Model version                  : 1.90
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:10:47 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalDrvLinMdl_h_
#define RTW_HEADER_LCETCS_vCalDrvLinMdl_h_
#ifndef LCETCS_vCalDrvLinMdl_COMMON_INCLUDES_
# define LCETCS_vCalDrvLinMdl_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalDrvLinMdl_COMMON_INCLUDES_ */

#include "LCETCS_vCalDrvLinMdl_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vCalTrqRatio.h"
#include "LCETCS_vCalTransEfficiencyRaw.h"
#include "LCETCS_vCalTransEfficiency.h"
#include "LCETCS_vCalTotalGearRatioRaw.h"
#include "LCETCS_vCalTotalGearRatio.h"
#include "LCETCS_vCalGrTransTime.h"

/* Block states (auto storage) for model 'LCETCS_vCalDrvLinMdl' */
typedef struct {
  MdlrefDW_LCETCS_vCalTotalGearRatioRaw_T LCETCS_vCalTotalGearRatioRaw_DWORK1;/* '<Root>/LCETCS_vCalTotalGearRatioRaw' */
  MdlrefDW_LCETCS_vCalGrTransTime_T LCETCS_vCalGrTransTime_DWORK1;/* '<Root>/LCETCS_vCalGrTransTime' */
  MdlrefDW_LCETCS_vCalTotalGearRatio_T LCETCS_vCalTotalGearRatio_DWORK1;/* '<Root>/LCETCS_vCalTotalGearRatio' */
  MdlrefDW_LCETCS_vCalTransEfficiency_T LCETCS_vCalTransEfficiency_DWORK1;/* '<Root>/LCETCS_vCalTransEfficiency' */
} DW_LCETCS_vCalDrvLinMdl_f_T;

typedef struct {
  DW_LCETCS_vCalDrvLinMdl_f_T rtdw;
} MdlrefDW_LCETCS_vCalDrvLinMdl_T;

/* Model reference registration function */
extern void LCETCS_vCalDrvLinMdl_initialize(void);
extern void LCETCS_vCalDrvLinMdl_Init(DW_LCETCS_vCalDrvLinMdl_f_T *localDW);
extern void LCETCS_vCalDrvLinMdl(const TypeETCSGearStruct *rtu_ETCS_GEAR_STRUCT,
  const TypeETCS4WDSigStruct *rtu_ETCS_4WD_SIG, TypeETCSDrvMdlStruct
  *rty_ETCS_DRV_MDL, DW_LCETCS_vCalDrvLinMdl_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalDrvLinMdl'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalDrvLinMdl_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
