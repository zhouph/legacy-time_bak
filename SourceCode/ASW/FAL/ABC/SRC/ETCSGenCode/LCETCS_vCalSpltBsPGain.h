/*
 * File: LCETCS_vCalSpltBsPGain.h
 *
 * Code generated for Simulink model 'LCETCS_vCalSpltBsPGain'.
 *
 * Model version                  : 1.164
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:02:42 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalSpltBsPGain_h_
#define RTW_HEADER_LCETCS_vCalSpltBsPGain_h_
#ifndef LCETCS_vCalSpltBsPGain_COMMON_INCLUDES_
# define LCETCS_vCalSpltBsPGain_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalSpltBsPGain_COMMON_INCLUDES_ */

#include "LCETCS_vCalSpltBsPGain_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_s16Inter5Point.h"

/* Block signals for model 'LCETCS_vCalSpltBsPGain' */
typedef struct {
  int16_T y1;                          /* '<S4>/Pgain for 5th gear ratio on split-mu' */
  int16_T y2;                          /* '<S4>/Pgain for 4th gear ratio on split-mu' */
  int16_T y3;                          /* '<S4>/Pgain for 3rd gear ratio on split-mu' */
  int16_T y4;                          /* '<S4>/Pgain for 2nd gear ratio on split-mu' */
  int16_T y5;                          /* '<S4>/Pgain for 1st gear ratio on split-mu' */
  int16_T lcetcss16SpltBsPgain;        /* '<S4>/LCETCS_s16Inter5Point' */
  int16_T y1_g;                        /* '<S3>/Pgain for 5th gear ratio on split-mu' */
  int16_T y2_g;                        /* '<S3>/Pgain for 4th gear ratio on split-mu' */
  int16_T y3_b;                        /* '<S3>/Pgain for 3rd gear ratio on split-mu' */
  int16_T y4_m;                        /* '<S3>/Pgain for 2nd gear ratio on split-mu' */
  int16_T y5_i;                        /* '<S3>/Pgain for 1st gear ratio on split-mu' */
  int16_T lcetcss16SpltBsPgain_m;      /* '<S3>/LCETCS_s16Inter5Point1' */
} B_LCETCS_vCalSpltBsPGain_c_T;

typedef struct {
  B_LCETCS_vCalSpltBsPGain_c_T rtb;
} MdlrefDW_LCETCS_vCalSpltBsPGain_T;

/* Model reference registration function */
extern void LCETCS_vCalSpltBsPGain_initialize(void);
extern void LCETCS_vCalSpltBsPGain_Start(B_LCETCS_vCalSpltBsPGain_c_T *localB);
extern void LCETCS_vCalSpltBsPGain(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT,
  const TypeETCSDrvMdlStruct *rtu_ETCS_DRV_MDL, const TypeETCSCtlErrStruct
  *rtu_ETCS_CTL_ERR, int16_T *rty_lcetcss16SpltBsPgain,
  B_LCETCS_vCalSpltBsPGain_c_T *localB);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalSpltBsPGain'
 * '<S1>'   : 'LCETCS_vCalSpltBsPGain/CalPGainWhnCtlOff'
 * '<S2>'   : 'LCETCS_vCalSpltBsPGain/CalPGainWhnCtlOn'
 * '<S3>'   : 'LCETCS_vCalSpltBsPGain/CalPGainWhnCtlOn/CalPgainWhnCtlErrNeg'
 * '<S4>'   : 'LCETCS_vCalSpltBsPGain/CalPGainWhnCtlOn/CalPgainWhnCtlErrPos'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalSpltBsPGain_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
