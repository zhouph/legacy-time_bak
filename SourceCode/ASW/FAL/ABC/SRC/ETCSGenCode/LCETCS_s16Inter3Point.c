/*
 * File: LCETCS_s16Inter3Point.c
 *
 * Code generated for Simulink model 'LCETCS_s16Inter3Point'.
 *
 * Model version                  : 1.265
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:48:48 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_s16Inter3Point.h"
#include "LCETCS_s16Inter3Point_private.h"

/* Initial conditions for referenced model: 'LCETCS_s16Inter3Point' */
void LCETCS_s16Inter3Point_Init(int16_T *rty_y)
{
  /* InitializeConditions for Chart: '<Root>/Chart3' */
  *rty_y = 0;
}

/* Output and update for referenced model: 'LCETCS_s16Inter3Point' */
void LCETCS_s16Inter3Point(int16_T rtu_x, int16_T rtu_x1, int16_T rtu_x2,
  int16_T rtu_x3, int16_T rtu_y1, int16_T rtu_y2, int16_T rtu_y3, int16_T *rty_y)
{
  int16_T rtb_TmpSignalConversionAtSFunctionInport2[3];
  int16_T rtb_x_index;
  int32_T rtb_x_frac;

  /* Chart: '<Root>/Chart1' incorporates:
   *  SignalConversion: '<S1>/TmpSignal ConversionAt SFunction Inport2'
   */
  /* Gateway: Chart1 */
  /* During: Chart1 */
  /* Entry Internal: Chart1 */
  /* Transition: '<S1>:12' */
  /* comment */
  if (rtu_x < rtu_x1) {
    /* Transition: '<S1>:106' */
    /* Transition: '<S1>:128' */
    rtb_x_index = 0;
    rtb_x_frac = 0;

    /* Transition: '<S1>:133' */
    /* Transition: '<S1>:167' */
    /* Transition: '<S1>:109' */
  } else {
    /* Transition: '<S1>:130' */
    if (rtu_x < rtu_x2) {
      /* Transition: '<S1>:132' */
      /* Transition: '<S1>:114' */
      rtb_x_index = 0;
      rtb_x_frac = ((rtu_x - rtu_x1) << 15) / (rtu_x2 - rtu_x1);

      /* Transition: '<S1>:167' */
      /* Transition: '<S1>:109' */
    } else {
      /* Transition: '<S1>:129' */
      if (rtu_x < rtu_x3) {
        /* Transition: '<S1>:146' */
        /* Transition: '<S1>:143' */
        rtb_x_index = 1;
        rtb_x_frac = ((rtu_x - rtu_x2) << 15) / (rtu_x3 - rtu_x2);

        /* Transition: '<S1>:109' */
      } else {
        /* Transition: '<S1>:138' */
        rtb_x_index = 1;
        rtb_x_frac = 32768;
      }
    }
  }

  /* End of Chart: '<Root>/Chart1' */

  /* SignalConversion: '<S2>/TmpSignal ConversionAt SFunction Inport3' incorporates:
   *  Chart: '<Root>/Chart3'
   */
  /* Transition: '<S1>:148' */
  rtb_TmpSignalConversionAtSFunctionInport2[0] = rtu_y1;
  rtb_TmpSignalConversionAtSFunctionInport2[1] = rtu_y2;
  rtb_TmpSignalConversionAtSFunctionInport2[2] = rtu_y3;

  /* Chart: '<Root>/Chart3' */
  /* Gateway: Chart3 */
  /* During: Chart3 */
  /* Entry Internal: Chart3 */
  /* Transition: '<S2>:12' */
  /* comment */
  /* Transition: '<S2>:20' */
  *rty_y = (int16_T)(((int16_T)
                      (((rtb_TmpSignalConversionAtSFunctionInport2[rtb_x_index +
    1] - rtb_TmpSignalConversionAtSFunctionInport2[rtb_x_index]) * rtb_x_frac) >>
                       15)) +
                     rtb_TmpSignalConversionAtSFunctionInport2[rtb_x_index]);
}

/* Model initialize function */
void LCETCS_s16Inter3Point_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
