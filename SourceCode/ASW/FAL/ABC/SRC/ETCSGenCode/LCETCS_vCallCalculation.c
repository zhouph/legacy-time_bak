/*
 * File: LCETCS_vCallCalculation.c
 *
 * Code generated for Simulink model 'LCETCS_vCallCalculation'.
 *
 * Model version                  : 1.294
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:14:16 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCallCalculation.h"
#include "LCETCS_vCallCalculation_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCallCalculation' */
void LCETCS_vCallCalculation_Init(TypeETCSCtrlModeStruct *rty_ETCS_CTRL_MODE,
  DW_LCETCS_vCallCalculation_f_T *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalDrvLinMdl' */
  LCETCS_vCalDrvLinMdl_Init(&(localDW->LCETCS_vCalDrvLinMdl_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalEngMdl' */
  LCETCS_vCalEngMdl_Init(&(localDW->LCETCS_vCalEngMdl_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalAxlAcc' */
  LCETCS_vCalAxlAcc_Init(&(localDW->LCETCS_vCalAxlAcc_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCntStrStat' */
  LCETCS_vCntStrStat_Init(&(localDW->LCETCS_vCntStrStat_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDetCtlModeSports1' */
  LCETCS_vDetCtlModeSports1_Init(rty_ETCS_CTRL_MODE);

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalTarWhlSpin' */
  LCETCS_vCalTarWhlSpin_Init(&(localDW->LCETCS_vCalTarWhlSpin_DWORK1.rtb),
    &(localDW->LCETCS_vCalTarWhlSpin_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalCtlErr' */
  LCETCS_vCalCtlErr_Init(&(localDW->LCETCS_vCalCtlErr_DWORK1.rtdw),
    &(localDW->LCETCS_vCalCtlErr_DWORK1.rtb));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalLdTrq' */
  LCETCS_vCalLdTrq_Init(&(localDW->LCETCS_vCalLdTrq_DWORK1.rtb),
                        &(localDW->LCETCS_vCalLdTrq_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalStrtThSymBrkCtl' */
  LCETCS_vCalStrtThSymBrkCtl_Init
    (&(localDW->LCETCS_vCalStrtThSymBrkCtl_DWORK1.rtb));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalVehAcc' */
  LCETCS_vCalVehAcc_Init(&(localDW->LCETCS_vCalVehAcc_DWORK1.rtb),
    &(localDW->LCETCS_vCalVehAcc_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalWhlAcc' */
  LCETCS_vCalWhlAcc_Init(&(localDW->LCETCS_vCalWhlAcc_DWORK1.rtdw));
}

/* Start for referenced model: 'LCETCS_vCallCalculation' */
void LCETCS_vCallCalculation_Start(DW_LCETCS_vCallCalculation_f_T *localDW)
{
  /* Start for ModelReference: '<Root>/LCETCS_vCalEngMdl' */
  LCETCS_vCalEngMdl_Start(&(localDW->LCETCS_vCalEngMdl_DWORK1.rtdw));

  /* Start for ModelReference: '<Root>/LCETCS_vCalTarWhlSpin' */
  LCETCS_vCalTarWhlSpin_Start(&(localDW->LCETCS_vCalTarWhlSpin_DWORK1.rtdw));

  /* Start for ModelReference: '<Root>/LCETCS_vCalLdTrq' */
  LCETCS_vCalLdTrq_Start(&(localDW->LCETCS_vCalLdTrq_DWORK1.rtdw));

  /* Start for ModelReference: '<Root>/LCETCS_vCalStrtThSymBrkCtl' */
  LCETCS_vCalStrtThSymBrkCtl_Start
    (&(localDW->LCETCS_vCalStrtThSymBrkCtl_DWORK1.rtb));

  /* Start for ModelReference: '<Root>/LCETCS_vCalVehAcc' */
  LCETCS_vCalVehAcc_Start(&(localDW->LCETCS_vCalVehAcc_DWORK1.rtdw));
}

/* Output and update for referenced model: 'LCETCS_vCallCalculation' */
void LCETCS_vCallCalculation(const TypeETCSWhlStruct *rtu_ETCS_FL, const
  TypeETCSWhlStruct *rtu_ETCS_FR, const TypeETCSWhlStruct *rtu_ETCS_RL, const
  TypeETCSWhlStruct *rtu_ETCS_RR, int16_T rtu_lcetcss16VehSpd, const
  TypeETCSGearStruct *rtu_ETCS_GEAR_STRUCT, const TypeETCSEngStruct
  *rtu_ETCS_ENG_STRUCT, const TypeETCSExtDctStruct *rtu_ETCS_EXT_DCT, const
  TypeETCSAxlStruct *rtu_ETCS_FA, const TypeETCSAxlStruct *rtu_ETCS_RA, const
  TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const TypeETCSBrkSigStruct
  *rtu_ETCS_BRK_SIG, const TypeETCS4WDSigStruct *rtu_ETCS_4WD_SIG, const
  TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT_OLD, const TypeETCSDepSnwStruct
  *rtu_ETCS_DEP_SNW, const TypeETCSCtlCmdStruct *rtu_ETCS_CTL_CMD_OLD, boolean_T
  rtu_lcetcsu1VehVibDctOld, const TypeETCSBrkCtlCmdStruct
  *rtu_ETCS_BRK_CTL_CMD_OLD, const TypeETCSBrkCtlReqStruct
  *rtu_ETCS_BRK_CTL_REQ_OLD, const TypeETCSSplitHillStruct
  *rtu_ETCS_SPLIT_HILL_OLD, const TypeETCSHighDctStruct *rtu_ETCS_HI_DCT_OLD,
  const TypeETCSEngStallStruct *rtu_ETCS_ENG_STALL_OLD, const
  TypeETCSLwToSpltStruct *rtu_ETCS_L2SPLT, TypeETCSCtlErrStruct
  *rty_ETCS_CTL_ERR, TypeETCSTarSpinStruct *rty_ETCS_TAR_SPIN,
  TypeETCSDrvMdlStruct *rty_ETCS_DRV_MDL, TypeETCSCrdnTrqStruct
  *rty_ETCS_CRDN_TRQ, TypeETCSVehAccStruct *rty_ETCS_VEH_ACC,
  TypeETCSEngMdlStruct *rty_ETCS_ENG_MDL, TypeETCSLdTrqStruct *rty_ETCS_LD_TRQ,
  TypeETCSAxlAccStruct *rty_ETCS_AXL_ACC, int16_T *rty_lcetcss16SymBrkCtlStrtTh,
  TypeETCSWhlSpinStruct *rty_ETCS_WHL_SPIN, TypeETCSStrStatStruct
  *rty_ETCS_STR_STAT, TypeETCSCtrlModeStruct *rty_ETCS_CTRL_MODE,
  TypeETCSWhlAccStruct *rty_ETCS_WHL_ACC, DW_LCETCS_vCallCalculation_f_T
  *localDW)
{
  /* ModelReference: '<Root>/LCETCS_vCalDrvLinMdl' */
  LCETCS_vCalDrvLinMdl(rtu_ETCS_GEAR_STRUCT, rtu_ETCS_4WD_SIG, rty_ETCS_DRV_MDL,
                       &(localDW->LCETCS_vCalDrvLinMdl_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalEngMdl' */
  LCETCS_vCalEngMdl(rtu_ETCS_ENG_STRUCT, rtu_ETCS_CTL_CMD_OLD, rty_ETCS_ENG_MDL,
                    &(localDW->LCETCS_vCalEngMdl_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalActCardanTorq' */
  LCETCS_vCalActCardanTorq(rty_ETCS_DRV_MDL, rtu_ETCS_ENG_STRUCT,
    rty_ETCS_ENG_MDL, rty_ETCS_CRDN_TRQ);

  /* ModelReference: '<Root>/LCETCS_vCalAxlAcc' */
  LCETCS_vCalAxlAcc(rtu_ETCS_FL, rtu_ETCS_FR, rtu_ETCS_RL, rtu_ETCS_RR,
                    rtu_ETCS_EXT_DCT, rty_ETCS_AXL_ACC,
                    &(localDW->LCETCS_vCalAxlAcc_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalWhlSpin' */
  LCETCS_vCalWhlSpin(rtu_ETCS_FL, rtu_ETCS_FR, rtu_ETCS_RL, rtu_ETCS_RR,
                     rtu_lcetcss16VehSpd, rty_ETCS_WHL_SPIN);

  /* ModelReference: '<Root>/LCETCS_vCntStrStat' */
  LCETCS_vCntStrStat(rtu_ETCS_ESC_SIG, rty_ETCS_STR_STAT,
                     &(localDW->LCETCS_vCntStrStat_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vDetCtlModeSports1' */
  LCETCS_vDetCtlModeSports1(rtu_ETCS_EXT_DCT, rty_ETCS_CTRL_MODE);

  /* ModelReference: '<Root>/LCETCS_vCalTarWhlSpin' */
  LCETCS_vCalTarWhlSpin(rtu_ETCS_DEP_SNW, rtu_ETCS_L2SPLT, rtu_lcetcss16VehSpd,
                        rtu_ETCS_EXT_DCT, rtu_ETCS_BRK_SIG, rtu_ETCS_FA,
                        rtu_ETCS_RA, rtu_ETCS_ENG_STALL_OLD,
                        rtu_ETCS_CTL_ACT_OLD, rtu_lcetcsu1VehVibDctOld,
                        rtu_ETCS_ESC_SIG, rty_ETCS_STR_STAT,
                        rtu_ETCS_SPLIT_HILL_OLD, rtu_ETCS_FL, rtu_ETCS_FR,
                        rtu_ETCS_RL, rtu_ETCS_RR, rty_ETCS_CTRL_MODE,
                        rtu_ETCS_HI_DCT_OLD, rty_ETCS_DRV_MDL, rty_ETCS_TAR_SPIN,
                        &(localDW->LCETCS_vCalTarWhlSpin_DWORK1.rtb),
                        &(localDW->LCETCS_vCalTarWhlSpin_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalCtlErr' */
  LCETCS_vCalCtlErr(rty_ETCS_WHL_SPIN, rty_ETCS_TAR_SPIN, rty_ETCS_CTL_ERR,
                    &(localDW->LCETCS_vCalCtlErr_DWORK1.rtdw),
                    &(localDW->LCETCS_vCalCtlErr_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vCalLdTrq' */
  LCETCS_vCalLdTrq(rtu_ETCS_FA, rtu_ETCS_RA, rtu_ETCS_BRK_CTL_CMD_OLD,
                   rtu_ETCS_BRK_CTL_REQ_OLD, rtu_ETCS_ESC_SIG,
                   rtu_ETCS_SPLIT_HILL_OLD, rty_ETCS_LD_TRQ,
                   &(localDW->LCETCS_vCalLdTrq_DWORK1.rtb),
                   &(localDW->LCETCS_vCalLdTrq_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalStrtThSymBrkCtl' */
  LCETCS_vCalStrtThSymBrkCtl(rty_ETCS_TAR_SPIN, rty_ETCS_AXL_ACC,
    rtu_lcetcss16VehSpd, rty_lcetcss16SymBrkCtlStrtTh,
    &(localDW->LCETCS_vCalStrtThSymBrkCtl_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vCalVehAcc' */
  LCETCS_vCalVehAcc(rtu_lcetcss16VehSpd, rtu_ETCS_ESC_SIG, rty_ETCS_DRV_MDL,
                    rty_ETCS_VEH_ACC, &(localDW->LCETCS_vCalVehAcc_DWORK1.rtb),
                    &(localDW->LCETCS_vCalVehAcc_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalWhlAcc' */
  LCETCS_vCalWhlAcc(rtu_ETCS_FL, rtu_ETCS_FR, rtu_ETCS_RL, rtu_ETCS_RR,
                    rty_ETCS_WHL_ACC, &(localDW->LCETCS_vCalWhlAcc_DWORK1.rtdw));
}

/* Model initialize function */
void LCETCS_vCallCalculation_initialize(const rtTimingBridge *timingBridge)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalActCardanTorq' */
  LCETCS_vCalActCardanTorq_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalAxlAcc' */
  LCETCS_vCalAxlAcc_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalCtlErr' */
  LCETCS_vCalCtlErr_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalDrvLinMdl' */
  LCETCS_vCalDrvLinMdl_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalEngMdl' */
  LCETCS_vCalEngMdl_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalLdTrq' */
  LCETCS_vCalLdTrq_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalStrtThSymBrkCtl' */
  LCETCS_vCalStrtThSymBrkCtl_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalTarWhlSpin' */
  LCETCS_vCalTarWhlSpin_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalVehAcc' */
  LCETCS_vCalVehAcc_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalWhlAcc' */
  LCETCS_vCalWhlAcc_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalWhlSpin' */
  LCETCS_vCalWhlSpin_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCntStrStat' */
  LCETCS_vCntStrStat_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDetCtlModeSports1' */
  LCETCS_vDetCtlModeSports1_initialize(timingBridge);
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
