/*
 * File: LCETCS_vCalAdptAmt.c
 *
 * Code generated for Simulink model 'LCETCS_vCalAdptAmt'.
 *
 * Model version                  : 1.228
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:51:03 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalAdptAmt.h"
#include "LCETCS_vCalAdptAmt_private.h"

/* Output and update for referenced model: 'LCETCS_vCalAdptAmt' */
void LCETCS_vCalAdptAmt(int32_T rtu_lcetcss32AdptByWhlSpn, const
  TypeETCSGainStruct *rtu_ETCS_CTL_GAINS, int32_T rtu_lcetcss32AdptByDelYaw,
  const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const TypeETCSCtlErrStruct
  *rtu_ETCS_CTL_ERR, const TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN, int32_T
  *rty_lcetcss32AdptAmt)
{
  /* Chart: '<Root>/Chart1' */
  /* Gateway: Chart1 */
  /* During: Chart1 */
  /* Entry Internal: Chart1 */
  /* Transition: '<S1>:175' */
  /*  delta yaw is unstable  */
  if (rtu_ETCS_ESC_SIG->lcetcss16AbsDelYawFrst >
      rtu_ETCS_TAR_SPIN->lcetcss16LmtCrngDltYaw) {
    /* Transition: '<S1>:14' */
    /* Transition: '<S1>:19' */
    /*  delta yaw is gettting bigger  */
    if (rtu_ETCS_CTL_GAINS->lcetcsu1DelYawDivrg == 1) {
      /* Transition: '<S1>:122' */
      /* Transition: '<S1>:124' */
      /*  working point adaptation by delta yaw rate  */
      if (rtu_lcetcss32AdptByDelYaw <= rtu_lcetcss32AdptByWhlSpn) {
        *rty_lcetcss32AdptAmt = rtu_lcetcss32AdptByDelYaw;
      } else {
        *rty_lcetcss32AdptAmt = rtu_lcetcss32AdptByWhlSpn;
      }

      /* Transition: '<S1>:133' */
      /* Transition: '<S1>:134' */
    } else {
      /* Transition: '<S1>:127' */
      /*  wheel spin > target spin  */
      if (rtu_ETCS_CTL_ERR->lcetcsu1CtlErrNeg == 1) {
        /* Transition: '<S1>:128' */
        /* Transition: '<S1>:131' */
        /*  working point adaptation by wheel spin  */
        *rty_lcetcss32AdptAmt = rtu_lcetcss32AdptByWhlSpn;

        /* Transition: '<S1>:134' */
      } else {
        /* Transition: '<S1>:132' */
        /*  working point adaptation by delta yaw rate  */
        if (rtu_lcetcss32AdptByDelYaw >= rtu_lcetcss32AdptByWhlSpn) {
          *rty_lcetcss32AdptAmt = rtu_lcetcss32AdptByDelYaw;
        } else {
          *rty_lcetcss32AdptAmt = rtu_lcetcss32AdptByWhlSpn;
        }
      }
    }

    /* Transition: '<S1>:135' */
  } else {
    /* Transition: '<S1>:48' */
    /*  working point adaptation by wheel spin  */
    *rty_lcetcss32AdptAmt = rtu_lcetcss32AdptByWhlSpn;
  }

  /* End of Chart: '<Root>/Chart1' */
  /* Transition: '<S1>:166' */
}

/* Model initialize function */
void LCETCS_vCalAdptAmt_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
