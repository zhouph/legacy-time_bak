/*
 * File: LCETCS_vCalRsltntAcc.c
 *
 * Code generated for Simulink model 'LCETCS_vCalRsltntAcc'.
 *
 * Model version                  : 1.29
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:50:17 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalRsltntAcc.h"
#include "LCETCS_vCalRsltntAcc_private.h"

/* Output and update for referenced model: 'LCETCS_vCalRsltntAcc' */
void LCETCS_vCalRsltntAcc(int16_T rtu_lcetcss16VehLongAcc4Ctl, int16_T
  rtu_lcetcss16ScaleDnAy, int16_T *rty_lcetcss16RsltntAcc)
{
  /* Sqrt: '<Root>/Sqrt' incorporates:
   *  Math: '<Root>/Math Function'
   *  Math: '<Root>/Math Function1'
   *  Sum: '<Root>/Add'
   */
  *rty_lcetcss16RsltntAcc = rt_sqrts16__s((int16_T)((rtu_lcetcss16VehLongAcc4Ctl
    * rtu_lcetcss16VehLongAcc4Ctl) + (rtu_lcetcss16ScaleDnAy *
    rtu_lcetcss16ScaleDnAy)));

  /* Saturate: '<Root>/Saturation' */
  if ((*rty_lcetcss16RsltntAcc) > 125) {
    *rty_lcetcss16RsltntAcc = 125;
  } else {
    if ((*rty_lcetcss16RsltntAcc) < 0) {
      *rty_lcetcss16RsltntAcc = 0;
    }
  }

  /* End of Saturate: '<Root>/Saturation' */
}

/* Model initialize function */
void LCETCS_vCalRsltntAcc_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
