/*
 * File: LCETCS_vCntStrStat.c
 *
 * Code generated for Simulink model 'LCETCS_vCntStrStat'.
 *
 * Model version                  : 1.130
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:50:49 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCntStrStat.h"
#include "LCETCS_vCntStrStat_private.h"

const TypeETCSStrStatStruct LCETCS_vCntStrStat_rtZTypeETCSStrStatStruct = {
  0,                                   /* lcetcss16StrCnvrgCnt */
  false,                               /* lcetcsu1StrPos */
  0                                    /* lcetcss16StrDivrgCnt */
} ;                                    /* TypeETCSStrStatStruct ground */

/* Initial conditions for referenced model: 'LCETCS_vCntStrStat' */
void LCETCS_vCntStrStat_Init(DW_LCETCS_vCntStrStat_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = LCETCS_vCntStrStat_rtZTypeETCSStrStatStruct;
}

/* Output and update for referenced model: 'LCETCS_vCntStrStat' */
void LCETCS_vCntStrStat(const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG,
  TypeETCSStrStatStruct *rty_ETCS_STR_STAT, DW_LCETCS_vCntStrStat_f_T *localDW)
{
  int16_T rtb_lcetcss16StrCnvrgCnt_b;
  int16_T rtb_lcetcss16StrDivrgCnt_j;
  int32_T tmp;

  /* Chart: '<Root>/Chart1' incorporates:
   *  UnitDelay: '<Root>/Unit Delay'
   */
  /* Gateway: Chart1 */
  /* During: Chart1 */
  /* Entry Internal: Chart1 */
  /* Transition: '<S1>:12' */
  /* comment */
  if (rtu_ETCS_ESC_SIG->lcetcss16StrWhAng > 0) {
    /* Transition: '<S1>:32' */
    /* Transition: '<S1>:33' */
    rty_ETCS_STR_STAT->lcetcsu1StrPos = true;

    /* Transition: '<S1>:34' */
  } else {
    /* Transition: '<S1>:35' */
    rty_ETCS_STR_STAT->lcetcsu1StrPos = false;
  }

  /* Transition: '<S1>:36' */
  /*  If sign of the steering angle has been chaged, reset the counters  */
  if (rty_ETCS_STR_STAT->lcetcsu1StrPos !=
      localDW->UnitDelay_DSTATE.lcetcsu1StrPos) {
    /* Transition: '<S1>:14' */
    /* Transition: '<S1>:19' */
    rtb_lcetcss16StrCnvrgCnt_b = 0;
    rtb_lcetcss16StrDivrgCnt_j = 0;

    /* Transition: '<S1>:18' */
    /* Transition: '<S1>:17' */
    /* Transition: '<S1>:24' */
  } else {
    /* Transition: '<S1>:13' */
    /*  If the steering angle converges  */
    if (((rtu_ETCS_ESC_SIG->lcetcss16StrWhAng > 0) &&
         (rtu_ETCS_ESC_SIG->lcetcss16StrWhAngGrd < 0)) ||
        ((rtu_ETCS_ESC_SIG->lcetcss16StrWhAng < 0) &&
         (rtu_ETCS_ESC_SIG->lcetcss16StrWhAngGrd > 0))) {
      /* Transition: '<S1>:21' */
      /* Transition: '<S1>:20' */
      /*  abs(lcetcss16StrWhAngGrd)/200 : count accelerator and 200 means 20deg/s  */
      if (rtu_ETCS_ESC_SIG->lcetcss16StrWhAngGrd < 0) {
        tmp = -rtu_ETCS_ESC_SIG->lcetcss16StrWhAngGrd;
        if (tmp > 32767) {
          tmp = 32767;
        }

        rtb_lcetcss16StrDivrgCnt_j = (int16_T)tmp;
      } else {
        rtb_lcetcss16StrDivrgCnt_j = rtu_ETCS_ESC_SIG->lcetcss16StrWhAngGrd;
      }

      tmp = ((rtb_lcetcss16StrDivrgCnt_j / 200) +
             localDW->UnitDelay_DSTATE.lcetcss16StrCnvrgCnt) + 1;
      if (tmp > 32767) {
        tmp = 32767;
      }

      rtb_lcetcss16StrCnvrgCnt_b = (int16_T)tmp;
      rtb_lcetcss16StrDivrgCnt_j = 0;

      /* Transition: '<S1>:17' */
      /* Transition: '<S1>:24' */
    } else {
      /* Transition: '<S1>:16' */
      /*  If the steering angle diverges  */
      if (((rtu_ETCS_ESC_SIG->lcetcss16StrWhAng < 0) &&
           (rtu_ETCS_ESC_SIG->lcetcss16StrWhAngGrd < 0)) ||
          ((rtu_ETCS_ESC_SIG->lcetcss16StrWhAng > 0) &&
           (rtu_ETCS_ESC_SIG->lcetcss16StrWhAngGrd > 0))) {
        /* Transition: '<S1>:22' */
        /* Transition: '<S1>:23' */
        /*  abs(lcetcss16StrWhAngGrd)/200 : count accelerator and 200 means 20deg/s  */
        rtb_lcetcss16StrCnvrgCnt_b = 0;
        if (rtu_ETCS_ESC_SIG->lcetcss16StrWhAngGrd < 0) {
          tmp = -rtu_ETCS_ESC_SIG->lcetcss16StrWhAngGrd;
          if (tmp > 32767) {
            tmp = 32767;
          }

          rtb_lcetcss16StrDivrgCnt_j = (int16_T)tmp;
        } else {
          rtb_lcetcss16StrDivrgCnt_j = rtu_ETCS_ESC_SIG->lcetcss16StrWhAngGrd;
        }

        tmp = ((rtb_lcetcss16StrDivrgCnt_j / 200) +
               localDW->UnitDelay_DSTATE.lcetcss16StrDivrgCnt) + 1;
        if (tmp > 32767) {
          tmp = 32767;
        }

        rtb_lcetcss16StrDivrgCnt_j = (int16_T)tmp;

        /* Transition: '<S1>:24' */
      } else {
        /* Transition: '<S1>:25' */
        tmp = localDW->UnitDelay_DSTATE.lcetcss16StrCnvrgCnt - 1;
        if (tmp < -32768) {
          tmp = -32768;
        }

        rtb_lcetcss16StrCnvrgCnt_b = (int16_T)tmp;
        tmp = localDW->UnitDelay_DSTATE.lcetcss16StrDivrgCnt - 1;
        if (tmp < -32768) {
          tmp = -32768;
        }

        rtb_lcetcss16StrDivrgCnt_j = (int16_T)tmp;
      }
    }
  }

  /* End of Chart: '<Root>/Chart1' */

  /* Saturate: '<Root>/Saturation' */
  /* Transition: '<S1>:15' */
  if (rtb_lcetcss16StrCnvrgCnt_b > 250) {
    rty_ETCS_STR_STAT->lcetcss16StrCnvrgCnt = 250;
  } else if (rtb_lcetcss16StrCnvrgCnt_b < 0) {
    rty_ETCS_STR_STAT->lcetcss16StrCnvrgCnt = 0;
  } else {
    rty_ETCS_STR_STAT->lcetcss16StrCnvrgCnt = rtb_lcetcss16StrCnvrgCnt_b;
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Saturate: '<Root>/Saturation1' */
  if (rtb_lcetcss16StrDivrgCnt_j > 250) {
    rty_ETCS_STR_STAT->lcetcss16StrDivrgCnt = 250;
  } else if (rtb_lcetcss16StrDivrgCnt_j < 0) {
    rty_ETCS_STR_STAT->lcetcss16StrDivrgCnt = 0;
  } else {
    rty_ETCS_STR_STAT->lcetcss16StrDivrgCnt = rtb_lcetcss16StrDivrgCnt_j;
  }

  /* End of Saturate: '<Root>/Saturation1' */

  /* Update for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = *rty_ETCS_STR_STAT;
}

/* Model initialize function */
void LCETCS_vCntStrStat_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
