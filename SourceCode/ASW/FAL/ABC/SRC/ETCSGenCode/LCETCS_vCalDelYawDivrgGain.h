/*
 * File: LCETCS_vCalDelYawDivrgGain.h
 *
 * Code generated for Simulink model 'LCETCS_vCalDelYawDivrgGain'.
 *
 * Model version                  : 1.240
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:03:06 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalDelYawDivrgGain_h_
#define RTW_HEADER_LCETCS_vCalDelYawDivrgGain_h_
#ifndef LCETCS_vCalDelYawDivrgGain_COMMON_INCLUDES_
# define LCETCS_vCalDelYawDivrgGain_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalDelYawDivrgGain_COMMON_INCLUDES_ */

#include "LCETCS_vCalDelYawDivrgGain_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_s16Inter3by5.h"

/* Block signals for model 'LCETCS_vCalDelYawDivrgGain' */
typedef struct {
  int16_T x3;                          /* '<Root>/Ay level on asphalt' */
  int16_T x1;                          /* '<Root>/Ay level on ice' */
  int16_T x2;                          /* '<Root>/Ay level on snow' */
  int16_T z13;                         /* '<Root>/Delta yaw gain on asphalt at speed 1' */
  int16_T z23;                         /* '<Root>/Delta yaw gain on asphalt at speed 2' */
  int16_T z33;                         /* '<Root>/Delta yaw gain on asphalt at speed 3' */
  int16_T z43;                         /* '<Root>/Delta yaw gain on asphalt at speed 4' */
  int16_T z53;                         /* '<Root>/Delta yaw gain on asphalt at speed 5' */
  int16_T z11;                         /* '<Root>/Delta yaw gain on ice at speed 1' */
  int16_T z21;                         /* '<Root>/Delta yaw gain on ice at speed 2' */
  int16_T z31;                         /* '<Root>/Delta yaw gain on ice at speed 3' */
  int16_T z41;                         /* '<Root>/Delta yaw gain on ice at speed 4' */
  int16_T z51;                         /* '<Root>/Delta yaw gain on ice at speed 5' */
  int16_T z12;                         /* '<Root>/Delta yaw gain on snow at speed 1' */
  int16_T z22;                         /* '<Root>/Delta yaw gain on snow at speed 2' */
  int16_T z32;                         /* '<Root>/Delta yaw gain on snow at speed 3' */
  int16_T z42;                         /* '<Root>/Delta yaw gain on snow at speed 4' */
  int16_T z52;                         /* '<Root>/Delta yaw gain on snow at speed 5' */
  int16_T Out;                         /* '<Root>/LCETCS_s16Inter3by5' */
} B_LCETCS_vCalDelYawDivrgGain_c_T;

typedef struct {
  B_LCETCS_vCalDelYawDivrgGain_c_T rtb;
} MdlrefDW_LCETCS_vCalDelYawDivrgGain_T;

/* Model reference registration function */
extern void LCETCS_vCalDelYawDivrgGain_initialize(void);
extern void LCETCS_vCalDelYawDivrgGain_Init(B_LCETCS_vCalDelYawDivrgGain_c_T
  *localB);
extern void LCETCS_vCalDelYawDivrgGain_Start(B_LCETCS_vCalDelYawDivrgGain_c_T
  *localB);
extern void LCETCS_vCalDelYawDivrgGain(int16_T rtu_lcetcss16VehSpd, const
  TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, int16_T *rty_lcetcss16DelYawDivrgGain,
  B_LCETCS_vCalDelYawDivrgGain_c_T *localB);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalDelYawDivrgGain'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalDelYawDivrgGain_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
