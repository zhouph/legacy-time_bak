/*
 * File: LCETCS_vDctCtlEnterExitCond.c
 *
 * Code generated for Simulink model 'LCETCS_vDctCtlEnterExitCond'.
 *
 * Model version                  : 1.185
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:09:56 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctCtlEnterExitCond.h"
#include "LCETCS_vDctCtlEnterExitCond_private.h"

/* Initial conditions for referenced model: 'LCETCS_vDctCtlEnterExitCond' */
void LCETCS_vDctCtlEnterExitCond_Init(B_LCETCS_vDctCtlEnterExitCond_c_T *localB,
  DW_LCETCS_vDctCtlEnterExitCond_f_T *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vChkDriverIntention' */
  LCETCS_vChkDriverIntention_Init(&localB->lcetcsu1DriverAccelIntend,
    &localB->lcetcsu1DriverAccelHysIntend);

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCntOvrWhlSpnTm' */
  LCETCS_vCntOvrWhlSpnTm_Init(&localB->lcetcss16OvrWhlSpnTm,
    &(localDW->LCETCS_vCntOvrWhlSpnTm_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalOvrWhlSpnTmTh' */
  LCETCS_vCalOvrWhlSpnTmTh_Init(&localB->lcetcss16OvrWhlSpnTmTh);

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCntOvrEngTrqCmdTm' */
  LCETCS_vCntOvrEngTrqCmdTm_Init(&(localDW->LCETCS_vCntOvrEngTrqCmdTm_DWORK1.rtb),
    &(localDW->LCETCS_vCntOvrEngTrqCmdTm_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCntCtlEntrTmInLmtCnrg' */
  LCETCS_vCntCtlEntrTmInLmtCnrg_Init
    (&(localDW->LCETCS_vCntCtlEntrTmInLmtCnrg_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalCtlAct' */
  LCETCS_vCalCtlAct_Init(&localB->lcetcsu1CtlAct, &localB->lcetcsu1CtlActRsgEdg,
    &(localDW->LCETCS_vCalCtlAct_DWORK1.rtdw));
}

/* Start for referenced model: 'LCETCS_vDctCtlEnterExitCond' */
void LCETCS_vDctCtlEnterExitCond_Start(DW_LCETCS_vDctCtlEnterExitCond_f_T
  *localDW)
{
  /* Start for ModelReference: '<Root>/LCETCS_vCalOvrWhlSpnTmTh' */
  LCETCS_vCalOvrWhlSpnTmTh_Start(&(localDW->LCETCS_vCalOvrWhlSpnTmTh_DWORK1.rtb));

  /* Start for ModelReference: '<Root>/LCETCS_vCntOvrEngTrqCmdTm' */
  LCETCS_vCntOvrEngTrqCmdTm_Start
    (&(localDW->LCETCS_vCntOvrEngTrqCmdTm_DWORK1.rtb));
}

/* Output and update for referenced model: 'LCETCS_vDctCtlEnterExitCond' */
void LCETCS_vDctCtlEnterExitCond(const TypeETCSEngStallStruct
  *rtu_ETCS_ENG_STALL, const TypeETCSEngCmdStruct *rtu_ETCS_ENG_CMD_OLD, const
  TypeETCSEngStruct *rtu_ETCS_ENG_STRUCT, const TypeETCSWhlSpinStruct
  *rtu_ETCS_WHL_SPIN, const TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN, const
  TypeETCSAxlAccStruct *rtu_ETCS_AXL_ACC, const TypeETCSCtlActStruct
  *rtu_ETCS_CTL_ACT_OLD, const TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC, const
  TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const TypeETCSExtDctStruct
  *rtu_ETCS_EXT_DCT, const TypeETCSCtrlModeStruct *rtu_ETCS_CTRL_MODE,
  TypeETCSCtlActStruct *rty_ETCS_CTL_ACT, B_LCETCS_vDctCtlEnterExitCond_c_T
  *localB, DW_LCETCS_vDctCtlEnterExitCond_f_T *localDW)
{
  /* local block i/o variables */
  int16_T rtb_lcetcss16OvrEngTrqCmdTm;
  int16_T rtb_lcetcss16LmtCnrgCtlEntrTm;

  /* ModelReference: '<Root>/LCETCS_vChkDriverIntention' */
  LCETCS_vChkDriverIntention(rtu_ETCS_ENG_STRUCT, rtu_ETCS_EXT_DCT,
    &localB->lcetcsu1DriverAccelIntend, &localB->lcetcsu1DriverAccelHysIntend);

  /* ModelReference: '<Root>/LCETCS_vCntOvrWhlSpnTm' */
  LCETCS_vCntOvrWhlSpnTm(rtu_ETCS_WHL_SPIN, rtu_ETCS_TAR_SPIN,
    rtu_ETCS_CTL_ACT_OLD, rtu_ETCS_CTRL_MODE, &localB->lcetcss16OvrWhlSpnTm,
    &(localDW->LCETCS_vCntOvrWhlSpnTm_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalOvrWhlSpnTmTh' */
  LCETCS_vCalOvrWhlSpnTmTh(rtu_ETCS_AXL_ACC, &localB->lcetcss16OvrWhlSpnTmTh,
    &(localDW->LCETCS_vCalOvrWhlSpnTmTh_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vCntOvrEngTrqCmdTm' */
  LCETCS_vCntOvrEngTrqCmdTm(rtu_ETCS_ENG_CMD_OLD, rtu_ETCS_ENG_STRUCT,
    rtu_ETCS_WHL_SPIN, rtu_ETCS_TAR_SPIN, &rtb_lcetcss16OvrEngTrqCmdTm,
    &(localDW->LCETCS_vCntOvrEngTrqCmdTm_DWORK1.rtb),
    &(localDW->LCETCS_vCntOvrEngTrqCmdTm_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCntCtlEntrTmInLmtCnrg' */
  LCETCS_vCntCtlEntrTmInLmtCnrg(rtu_ETCS_WHL_SPIN, rtu_ETCS_TAR_SPIN,
    rtu_ETCS_VEH_ACC, rtu_ETCS_ESC_SIG, rtu_ETCS_CTL_ACT_OLD,
    &rtb_lcetcss16LmtCnrgCtlEntrTm,
    &(localDW->LCETCS_vCntCtlEntrTmInLmtCnrg_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalCtlAct' */
  LCETCS_vCalCtlAct(rtu_ETCS_ENG_STALL, localB->lcetcsu1DriverAccelIntend,
                    localB->lcetcsu1DriverAccelHysIntend,
                    localB->lcetcss16OvrWhlSpnTm, rtu_ETCS_ENG_CMD_OLD,
                    localB->lcetcss16OvrWhlSpnTmTh, rtu_ETCS_ENG_STRUCT,
                    rtb_lcetcss16OvrEngTrqCmdTm, rtu_ETCS_CTRL_MODE,
                    rtb_lcetcss16LmtCnrgCtlEntrTm, &localB->lcetcsu1CtlAct,
                    &localB->lcetcsu1CtlActRsgEdg,
                    &(localDW->LCETCS_vCalCtlAct_DWORK1.rtdw));

  /* BusCreator: '<Root>/Bus Creator1' */
  rty_ETCS_CTL_ACT->lcetcsu1CtlAct = localB->lcetcsu1CtlAct;
  rty_ETCS_CTL_ACT->lcetcsu1CtlActRsgEdg = localB->lcetcsu1CtlActRsgEdg;
  rty_ETCS_CTL_ACT->lcetcss16OvrWhlSpnTm = localB->lcetcss16OvrWhlSpnTm;
  rty_ETCS_CTL_ACT->lcetcss16OvrWhlSpnTmTh = localB->lcetcss16OvrWhlSpnTmTh;
  rty_ETCS_CTL_ACT->lcetcss16OvrEngTrqCmdTm = rtb_lcetcss16OvrEngTrqCmdTm;
  rty_ETCS_CTL_ACT->lcetcss16LmtCnrgCtlEntrTm = rtb_lcetcss16LmtCnrgCtlEntrTm;
  rty_ETCS_CTL_ACT->lcetcsu1DriverAccelIntend =
    localB->lcetcsu1DriverAccelIntend;
  rty_ETCS_CTL_ACT->lcetcsu1DriverAccelHysIntend =
    localB->lcetcsu1DriverAccelHysIntend;
}

/* Model initialize function */
void LCETCS_vDctCtlEnterExitCond_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalCtlAct' */
  LCETCS_vCalCtlAct_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalOvrWhlSpnTmTh' */
  LCETCS_vCalOvrWhlSpnTmTh_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vChkDriverIntention' */
  LCETCS_vChkDriverIntention_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCntCtlEntrTmInLmtCnrg' */
  LCETCS_vCntCtlEntrTmInLmtCnrg_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCntOvrEngTrqCmdTm' */
  LCETCS_vCntOvrEngTrqCmdTm_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCntOvrWhlSpnTm' */
  LCETCS_vCntOvrWhlSpnTm_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
