/*
 * File: LCETCS_vCalFunLamp.c
 *
 * Code generated for Simulink model 'LCETCS_vCalFunLamp'.
 *
 * Model version                  : 1.190
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:03:13 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalFunLamp.h"
#include "LCETCS_vCalFunLamp_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalFunLamp' */
void LCETCS_vCalFunLamp_Init(boolean_T *rty_lcetcsu1FuncLampOn,
  B_LCETCS_vCalFunLamp_c_T *localB, DW_LCETCS_vCalFunLamp_f_T *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalFunLampCnt4Brk' */
  LCETCS_vCalFunLampCnt4Brk_Init(&localB->lcetcsu8FuncLampOffBrkCnt);

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = false;

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalFunLampCnt4Eng' */
  LCETCS_vCalFunLampCnt4Eng_Init(&localB->lcetcsu8FuncLampOffEngCnt,
    &(localDW->LCETCS_vCalFunLampCnt4Eng_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctFunLampFlg' */
  LCETCS_vDctFunLampFlg_Init(rty_lcetcsu1FuncLampOn);
}

/* Output and update for referenced model: 'LCETCS_vCalFunLamp' */
void LCETCS_vCalFunLamp(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const
  TypeETCSTrq4RdFricStruct *rtu_ETCS_TRQ_REP_RD_FRIC, const
  TypeETCSWhlSpinStruct *rtu_ETCS_WHL_SPIN, const TypeETCSCirStruct *rtu_ETCS_PC,
  const TypeETCSCirStruct *rtu_ETCS_SC, const TypeETCSExtDctStruct
  *rtu_ETCS_EXT_DCT, boolean_T *rty_lcetcsu1FuncLampOn, B_LCETCS_vCalFunLamp_c_T
  *localB, DW_LCETCS_vCalFunLamp_f_T *localDW)
{
  /* local block i/o variables */
  boolean_T rtb_lcetcsu1FuncLampOnOld;

  /* ModelReference: '<Root>/LCETCS_vCalFunLampCnt4Brk' */
  LCETCS_vCalFunLampCnt4Brk(rtu_ETCS_PC, rtu_ETCS_SC, rtu_ETCS_EXT_DCT,
    &localB->lcetcsu8FuncLampOffBrkCnt);

  /* UnitDelay: '<Root>/Unit Delay2' */
  rtb_lcetcsu1FuncLampOnOld = localDW->UnitDelay2_DSTATE;

  /* ModelReference: '<Root>/LCETCS_vCalFunLampCnt4Eng' */
  LCETCS_vCalFunLampCnt4Eng(rtu_ETCS_CTL_ACT, rtu_ETCS_TRQ_REP_RD_FRIC,
    rtu_ETCS_WHL_SPIN, rtb_lcetcsu1FuncLampOnOld,
    &localB->lcetcsu8FuncLampOffEngCnt,
    &(localDW->LCETCS_vCalFunLampCnt4Eng_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vDctFunLampFlg' */
  LCETCS_vDctFunLampFlg(localB->lcetcsu8FuncLampOffBrkCnt, rtu_ETCS_CTL_ACT,
                        rtu_ETCS_EXT_DCT, localB->lcetcsu8FuncLampOffEngCnt,
                        rty_lcetcsu1FuncLampOn);

  /* Update for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = *rty_lcetcsu1FuncLampOn;
}

/* Model initialize function */
void LCETCS_vCalFunLamp_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalFunLampCnt4Brk' */
  LCETCS_vCalFunLampCnt4Brk_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalFunLampCnt4Eng' */
  LCETCS_vCalFunLampCnt4Eng_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctFunLampFlg' */
  LCETCS_vDctFunLampFlg_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
