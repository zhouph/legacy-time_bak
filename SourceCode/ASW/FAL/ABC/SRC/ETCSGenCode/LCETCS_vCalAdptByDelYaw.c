/*
 * File: LCETCS_vCalAdptByDelYaw.c
 *
 * Code generated for Simulink model 'LCETCS_vCalAdptByDelYaw'.
 *
 * Model version                  : 1.203
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:51:08 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalAdptByDelYaw.h"
#include "LCETCS_vCalAdptByDelYaw_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalAdptByDelYaw' */
void LCETCS_vCalAdptByDelYaw_Init(DW_LCETCS_vCalAdptByDelYaw_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = 0;
}

/* Output and update for referenced model: 'LCETCS_vCalAdptByDelYaw' */
void LCETCS_vCalAdptByDelYaw(const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const
  TypeETCSGainStruct *rtu_ETCS_CTL_GAINS, int32_T *rty_lcetcss32AdptByDelYaw,
  DW_LCETCS_vCalAdptByDelYaw_f_T *localDW)
{
  /* Product: '<Root>/Divide2' incorporates:
   *  Product: '<Root>/Divide3'
   *  Sum: '<Root>/Add1'
   *  UnitDelay: '<Root>/Unit Delay'
   */
  *rty_lcetcss32AdptByDelYaw = (((int16_T)(localDW->UnitDelay_DSTATE -
    rtu_ETCS_ESC_SIG->lcetcss16AbsDelYawFrst)) *
    rtu_ETCS_CTL_GAINS->lcetcss16DelYawGain) / 10;

  /* Saturate: '<Root>/Saturation1' */
  if ((*rty_lcetcss32AdptByDelYaw) > 10000) {
    *rty_lcetcss32AdptByDelYaw = 10000;
  } else {
    if ((*rty_lcetcss32AdptByDelYaw) < -10000) {
      *rty_lcetcss32AdptByDelYaw = -10000;
    }
  }

  /* End of Saturate: '<Root>/Saturation1' */

  /* Update for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = rtu_ETCS_ESC_SIG->lcetcss16AbsDelYawFrst;
}

/* Model initialize function */
void LCETCS_vCalAdptByDelYaw_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
