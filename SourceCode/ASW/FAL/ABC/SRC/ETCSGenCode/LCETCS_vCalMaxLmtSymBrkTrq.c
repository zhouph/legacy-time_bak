/*
 * File: LCETCS_vCalMaxLmtSymBrkTrq.c
 *
 * Code generated for Simulink model 'LCETCS_vCalMaxLmtSymBrkTrq'.
 *
 * Model version                  : 1.129
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:03:19 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalMaxLmtSymBrkTrq.h"
#include "LCETCS_vCalMaxLmtSymBrkTrq_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalMaxLmtSymBrkTrq' */
void LCETCS_vCalMaxLmtSymBrkTrq_Init(B_LCETCS_vCalMaxLmtSymBrkTrq_c_T *localB)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter5Point' */
  LCETCS_s16Inter5Point_Init(&localB->y);
}

/* Start for referenced model: 'LCETCS_vCalMaxLmtSymBrkTrq' */
void LCETCS_vCalMaxLmtSymBrkTrq_Start(B_LCETCS_vCalMaxLmtSymBrkTrq_c_T *localB)
{
  /* Start for Constant: '<Root>/Total Gear Ratio at 5th Gear1' */
  localB->y1 = ((uint8_T)U8ETCSCpSymBrkTrqMax_5);

  /* Start for Constant: '<Root>/Total Gear Ratio at 4th Gear1' */
  localB->y2 = ((uint8_T)U8ETCSCpSymBrkTrqMax_4);

  /* Start for Constant: '<Root>/Total Gear Ratio at 3rd Gear1' */
  localB->y3 = ((uint8_T)U8ETCSCpSymBrkTrqMax_3);

  /* Start for Constant: '<Root>/Total Gear Ratio at 2nd Gear1' */
  localB->y4 = ((uint8_T)U8ETCSCpSymBrkTrqMax_2);

  /* Start for Constant: '<Root>/Total Gear Ratio at 1st Gear1' */
  localB->y5 = ((uint8_T)U8ETCSCpSymBrkTrqMax_1);
}

/* Output and update for referenced model: 'LCETCS_vCalMaxLmtSymBrkTrq' */
void LCETCS_vCalMaxLmtSymBrkTrq(const TypeETCSDrvMdlStruct *rtu_ETCS_DRV_MDL,
  int16_T *rty_lcetcss16MaxLmtSymBrkTrq, B_LCETCS_vCalMaxLmtSymBrkTrq_c_T
  *localB)
{
  /* Constant: '<Root>/Total Gear Ratio at 5th Gear1' */
  localB->y1 = ((uint8_T)U8ETCSCpSymBrkTrqMax_5);

  /* Constant: '<Root>/Total Gear Ratio at 4th Gear1' */
  localB->y2 = ((uint8_T)U8ETCSCpSymBrkTrqMax_4);

  /* Constant: '<Root>/Total Gear Ratio at 3rd Gear1' */
  localB->y3 = ((uint8_T)U8ETCSCpSymBrkTrqMax_3);

  /* Constant: '<Root>/Total Gear Ratio at 2nd Gear1' */
  localB->y4 = ((uint8_T)U8ETCSCpSymBrkTrqMax_2);

  /* Constant: '<Root>/Total Gear Ratio at 1st Gear1' */
  localB->y5 = ((uint8_T)U8ETCSCpSymBrkTrqMax_1);

  /* ModelReference: '<Root>/LCETCS_s16Inter5Point' */
  LCETCS_s16Inter5Point(rtu_ETCS_DRV_MDL->lcetcss16TotalGearRatio, ((int16_T)
    S16ETCSCpTotalGearRatio_5), ((int16_T)S16ETCSCpTotalGearRatio_4), ((int16_T)
    S16ETCSCpTotalGearRatio_3), ((int16_T)S16ETCSCpTotalGearRatio_2), ((int16_T)
    S16ETCSCpTotalGearRatio_1), localB->y1, localB->y2, localB->y3, localB->y4,
                        localB->y5, &localB->y);

  /* Saturate: '<Root>/Saturation' */
  if (localB->y > 255) {
    *rty_lcetcss16MaxLmtSymBrkTrq = 255;
  } else if (localB->y < 0) {
    *rty_lcetcss16MaxLmtSymBrkTrq = 0;
  } else {
    *rty_lcetcss16MaxLmtSymBrkTrq = localB->y;
  }

  /* End of Saturate: '<Root>/Saturation' */
}

/* Model initialize function */
void LCETCS_vCalMaxLmtSymBrkTrq_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter5Point' */
  LCETCS_s16Inter5Point_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
