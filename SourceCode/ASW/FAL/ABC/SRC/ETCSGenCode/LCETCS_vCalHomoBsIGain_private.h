/*
 * File: LCETCS_vCalHomoBsIGain_private.h
 *
 * Code generated for Simulink model 'LCETCS_vCalHomoBsIGain'.
 *
 * Model version                  : 1.153
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:01:25 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalHomoBsIGain_private_h_
#define RTW_HEADER_LCETCS_vCalHomoBsIGain_private_h_
#include "rtwtypes.h"
#include "model_reference_types.h"

/* Includes for objects with custom storage classes. */
#include "../ETCSGenCodeHeader/ETCSHeader4Import.h"

/*
 * Generate compile time checks that imported macros for parameters
 * with storage class "ImportedDefine" are defined
 */
#ifndef S16ETCSCpCdrnTrqAtAsphalt
#error The variable for the parameter "S16ETCSCpCdrnTrqAtAsphalt" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtIce
#error The variable for the parameter "S16ETCSCpCdrnTrqAtIce" is not defined
#endif

#ifndef S16ETCSCpCdrnTrqAtSnow
#error The variable for the parameter "S16ETCSCpCdrnTrqAtSnow" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_1
#error The variable for the parameter "S16ETCSCpTotalGearRatio_1" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_2
#error The variable for the parameter "S16ETCSCpTotalGearRatio_2" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_3
#error The variable for the parameter "S16ETCSCpTotalGearRatio_3" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_4
#error The variable for the parameter "S16ETCSCpTotalGearRatio_4" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_5
#error The variable for the parameter "S16ETCSCpTotalGearRatio_5" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainAsp_1
#error The variable for the parameter "U8ETCSCpNegErrIgainAsp_1" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainAsp_2
#error The variable for the parameter "U8ETCSCpNegErrIgainAsp_2" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainAsp_3
#error The variable for the parameter "U8ETCSCpNegErrIgainAsp_3" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainAsp_4
#error The variable for the parameter "U8ETCSCpNegErrIgainAsp_4" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainAsp_5
#error The variable for the parameter "U8ETCSCpNegErrIgainAsp_5" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainIce_1
#error The variable for the parameter "U8ETCSCpNegErrIgainIce_1" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainIce_2
#error The variable for the parameter "U8ETCSCpNegErrIgainIce_2" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainIce_3
#error The variable for the parameter "U8ETCSCpNegErrIgainIce_3" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainIce_4
#error The variable for the parameter "U8ETCSCpNegErrIgainIce_4" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainIce_5
#error The variable for the parameter "U8ETCSCpNegErrIgainIce_5" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainSnw_1
#error The variable for the parameter "U8ETCSCpNegErrIgainSnw_1" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainSnw_2
#error The variable for the parameter "U8ETCSCpNegErrIgainSnw_2" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainSnw_3
#error The variable for the parameter "U8ETCSCpNegErrIgainSnw_3" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainSnw_4
#error The variable for the parameter "U8ETCSCpNegErrIgainSnw_4" is not defined
#endif

#ifndef U8ETCSCpNegErrIgainSnw_5
#error The variable for the parameter "U8ETCSCpNegErrIgainSnw_5" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainAsp_1
#error The variable for the parameter "U8ETCSCpPosErrIgainAsp_1" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainAsp_2
#error The variable for the parameter "U8ETCSCpPosErrIgainAsp_2" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainAsp_3
#error The variable for the parameter "U8ETCSCpPosErrIgainAsp_3" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainAsp_4
#error The variable for the parameter "U8ETCSCpPosErrIgainAsp_4" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainAsp_5
#error The variable for the parameter "U8ETCSCpPosErrIgainAsp_5" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainIce_1
#error The variable for the parameter "U8ETCSCpPosErrIgainIce_1" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainIce_2
#error The variable for the parameter "U8ETCSCpPosErrIgainIce_2" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainIce_3
#error The variable for the parameter "U8ETCSCpPosErrIgainIce_3" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainIce_4
#error The variable for the parameter "U8ETCSCpPosErrIgainIce_4" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainIce_5
#error The variable for the parameter "U8ETCSCpPosErrIgainIce_5" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainSnw_1
#error The variable for the parameter "U8ETCSCpPosErrIgainSnw_1" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainSnw_2
#error The variable for the parameter "U8ETCSCpPosErrIgainSnw_2" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainSnw_3
#error The variable for the parameter "U8ETCSCpPosErrIgainSnw_3" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainSnw_4
#error The variable for the parameter "U8ETCSCpPosErrIgainSnw_4" is not defined
#endif

#ifndef U8ETCSCpPosErrIgainSnw_5
#error The variable for the parameter "U8ETCSCpPosErrIgainSnw_5" is not defined
#endif
#endif                                 /* RTW_HEADER_LCETCS_vCalHomoBsIGain_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
