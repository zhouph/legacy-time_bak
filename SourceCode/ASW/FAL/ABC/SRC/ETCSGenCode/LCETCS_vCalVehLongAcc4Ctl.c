/*
 * File: LCETCS_vCalVehLongAcc4Ctl.c
 *
 * Code generated for Simulink model 'LCETCS_vCalVehLongAcc4Ctl'.
 *
 * Model version                  : 1.499
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:50:26 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalVehLongAcc4Ctl.h"
#include "LCETCS_vCalVehLongAcc4Ctl_private.h"

/* Output and update for referenced model: 'LCETCS_vCalVehLongAcc4Ctl' */
void LCETCS_vCalVehLongAcc4Ctl(int16_T rtu_lcetcss16VehAccByVehSpd, const
  TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, int16_T *rty_lcetcss16VehLongAcc4Ctl)
{
  /* Switch: '<Root>/Switch' incorporates:
   *  Constant: '<Root>/Constant1'
   */
  if (((uint8_T)U8ETCSCpAxSenAvail) != 0) {
    *rty_lcetcss16VehLongAcc4Ctl = rtu_ETCS_ESC_SIG->lcetcss16LonAcc;
  } else {
    *rty_lcetcss16VehLongAcc4Ctl = rtu_lcetcss16VehAccByVehSpd;
  }

  /* End of Switch: '<Root>/Switch' */

  /* Saturate: '<Root>/Saturation' */
  if ((*rty_lcetcss16VehLongAcc4Ctl) > 125) {
    *rty_lcetcss16VehLongAcc4Ctl = 125;
  } else {
    if ((*rty_lcetcss16VehLongAcc4Ctl) < -125) {
      *rty_lcetcss16VehLongAcc4Ctl = -125;
    }
  }

  /* End of Saturate: '<Root>/Saturation' */
}

/* Model initialize function */
void LCETCS_vCalVehLongAcc4Ctl_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
