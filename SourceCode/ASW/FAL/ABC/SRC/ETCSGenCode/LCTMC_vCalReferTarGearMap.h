/*
 * File: LCTMC_vCalReferTarGearMap.h
 *
 * Code generated for Simulink model 'LCTMC_vCalReferTarGearMap'.
 *
 * Model version                  : 1.287
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:12:40 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCTMC_vCalReferTarGearMap_h_
#define RTW_HEADER_LCTMC_vCalReferTarGearMap_h_
#ifndef LCTMC_vCalReferTarGearMap_COMMON_INCLUDES_
# define LCTMC_vCalReferTarGearMap_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCTMC_vCalReferTarGearMap_COMMON_INCLUDES_ */

#include "LCTMC_vCalReferTarGearMap_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCTMC_vCalRefTarGearMapbyGear.h"
#include "LCTMC_vCalRefLimVehSpdMap.h"
#include "LCTMC_vCalRefHysTarGearMap.h"
#include "LCTMC_vCalRefFinalTarGearMap.h"
#include "LCTMC_vCalInTrnRefTarGearMap.h"
#include "LCTMC_vCalInHillRefTarGearMap.h"

/* Block states (auto storage) for model 'LCTMC_vCalReferTarGearMap' */
typedef struct {
  MdlrefDW_LCTMC_vCalInTrnRefTarGearMap_T LCTMC_vCalInTrnRefTarGearMap_DWORK1;/* '<Root>/LCTMC_vCalInTrnRefTarGearMap' */
  MdlrefDW_LCTMC_vCalRefTarGearMapbyGear_T LCTMC_vCalRefTarGearMapbyGear_DWORK1;/* '<Root>/LCTMC_vCalRefTarGearMapbyGear' */
  MdlrefDW_LCTMC_vCalRefLimVehSpdMap_T LCTMC_vCalRefLimVehSpdMap_DWORK1;/* '<Root>/LCTMC_vCalRefLimVehSpdMap' */
} DW_LCTMC_vCalReferTarGearMap_f_T;

typedef struct {
  DW_LCTMC_vCalReferTarGearMap_f_T rtdw;
} MdlrefDW_LCTMC_vCalReferTarGearMap_T;

/* Model reference registration function */
extern void LCTMC_vCalReferTarGearMap_initialize(void);
extern void LCTMC_vCalReferTarGearMap_Init(DW_LCTMC_vCalReferTarGearMap_f_T
  *localDW);
extern void LCTMC_vCalReferTarGearMap_Start(DW_LCTMC_vCalReferTarGearMap_f_T
  *localDW);
extern void LCTMC_vCalReferTarGearMap(const TypeETCSGearStruct
  *rtu_ETCS_GEAR_STRUCT, const TypeETCSGainStruct *rtu_ETCS_CTL_GAINS, const
  TypeETCSSplitHillStruct *rtu_ETCS_SPLIT_HILL, const TypeTMCReqGearStruct
  *rtu_TMC_REQ_GEAR_STRUCT_OLD, boolean_T rtu_lctmcu1NonCndTarReqGear,
  TypeTMCRefLimVehSpdMap *rty_TMC_REF_LIM_VS_MAP, TypeTMCRefFinTarGearMap
  *rty_TMC_REF_FIN_TAR_GEAR_MAP, DW_LCTMC_vCalReferTarGearMap_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCTMC_vCalReferTarGearMap'
 */
#endif                                 /* RTW_HEADER_LCTMC_vCalReferTarGearMap_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
