/*
 * File: LCETCS_vCordICtlGain.c
 *
 * Code generated for Simulink model 'LCETCS_vCordICtlGain'.
 *
 * Model version                  : 1.231
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:51:47 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCordICtlGain.h"
#include "LCETCS_vCordICtlGain_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCordICtlGain' */
void LCETCS_vCordICtlGain_Init(int16_T *rty_lcetcss16IgainFac)
{
  /* InitializeConditions for Chart: '<Root>/Chart1' */
  *rty_lcetcss16IgainFac = 0;
}

/* Output and update for referenced model: 'LCETCS_vCordICtlGain' */
void LCETCS_vCordICtlGain(int16_T rtu_lcetcss16BsIgain, int16_T
  rtu_lcetcss16IGainIncFacH2L, const TypeETCSH2LStruct *rtu_ETCS_H2L, const
  TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN, int16_T rtu_lcetcss16IGainFacInTrn,
  const TypeETCSGainGearShftStruct *rtu_ETCS_GAIN_GEAR_SHFT, int16_T
  rtu_lcetcss16IgainFacInGearChng, const TypeETCSCtlErrStruct *rtu_ETCS_CTL_ERR,
  const TypeETCSAftLmtCrng *rtu_ETCS_AFT_TURN, int16_T
  rtu_lcetcss16HmIGainFctAftrTrn, int16_T rtu_lcetcss16HmIGainFct4BigSpn,
  int16_T *rty_lcetcss16Igain, int16_T *rty_lcetcss16IgainFac)
{
  int32_T rtb_Product1;
  int16_T rtu_ETCS_CTL_ERR_0;

  /* Chart: '<Root>/Chart1' */
  /* Gateway: Chart1 */
  /* During: Chart1 */
  /* Entry Internal: Chart1 */
  /* Transition: '<S1>:175' */
  if (rtu_ETCS_CTL_ERR->lcetcsu1CtlErrNeg == 1) {
    /* Transition: '<S1>:191' */
    /* Transition: '<S1>:192' */
    if (rtu_ETCS_H2L->lcetcsu1HiToLw == 1) {
      /* Transition: '<S1>:215' */
      /* Transition: '<S1>:219' */
      *rty_lcetcss16IgainFac = rtu_lcetcss16IGainIncFacH2L;

      /* Transition: '<S1>:218' */
      /* Transition: '<S1>:200' */
      /* Transition: '<S1>:227' */
      /* Transition: '<S1>:228' */
    } else {
      /* Transition: '<S1>:216' */
      /*  delta yaw is unstable  */
      if (rtu_ETCS_TAR_SPIN->lcetcsu1DctLmtCrng == 1) {
        /* Transition: '<S1>:14' */
        /* Transition: '<S1>:177' */
        *rty_lcetcss16IgainFac = rtu_lcetcss16IGainFacInTrn;

        /* Transition: '<S1>:200' */
        /* Transition: '<S1>:227' */
        /* Transition: '<S1>:228' */
      } else {
        /* Transition: '<S1>:48' */
        if ((rtu_ETCS_GAIN_GEAR_SHFT->lcetcsu1GainChngInGearShft == 1) ||
            (rtu_ETCS_GAIN_GEAR_SHFT->lcetcsu1GainTrnsAftrGearShft == 1)) {
          /* Transition: '<S1>:196' */
          /* Transition: '<S1>:199' */
          *rty_lcetcss16IgainFac = rtu_lcetcss16IgainFacInGearChng;

          /* Transition: '<S1>:227' */
          /* Transition: '<S1>:228' */
        } else {
          /* Transition: '<S1>:201' */
          if (rtu_ETCS_CTL_ERR->lcetcss16CtlErr < 0) {
            rtb_Product1 = -rtu_ETCS_CTL_ERR->lcetcss16CtlErr;
            if (rtb_Product1 > 32767) {
              rtb_Product1 = 32767;
            }

            rtu_ETCS_CTL_ERR_0 = (int16_T)rtb_Product1;
          } else {
            rtu_ETCS_CTL_ERR_0 = rtu_ETCS_CTL_ERR->lcetcss16CtlErr;
          }

          if (rtu_ETCS_CTL_ERR_0 > ((int16_T)VREF_3_KPH)) {
            /* Transition: '<S1>:223' */
            /* Minimum Big Spin Threshold
               in LCETCS_vCalHmIGainFct4BigSpn */
            /* Transition: '<S1>:226' */
            *rty_lcetcss16IgainFac = rtu_lcetcss16HmIGainFct4BigSpn;

            /* Transition: '<S1>:228' */
          } else {
            /* Transition: '<S1>:224' */
            *rty_lcetcss16IgainFac = 100;
          }
        }
      }
    }

    /* Transition: '<S1>:236' */
    /* Transition: '<S1>:237' */
  } else {
    /* Transition: '<S1>:204' */
    if (rtu_ETCS_AFT_TURN->lcetcsu1DctAftLmtCrng == 1) {
      /* Transition: '<S1>:232' */
      /* Transition: '<S1>:235' */
      *rty_lcetcss16IgainFac = rtu_lcetcss16HmIGainFctAftrTrn;

      /* Transition: '<S1>:237' */
    } else {
      /* Transition: '<S1>:233' */
      *rty_lcetcss16IgainFac = 100;
    }
  }

  /* End of Chart: '<Root>/Chart1' */

  /* Product: '<Root>/Product1' */
  /* Transition: '<S1>:207' */
  rtb_Product1 = rtu_lcetcss16BsIgain * (*rty_lcetcss16IgainFac);

  /* Saturate: '<Root>/Saturation1' incorporates:
   *  Product: '<Root>/Divide1'
   */
  rtu_ETCS_CTL_ERR_0 = (int16_T)(rtb_Product1 / 100);
  if (rtu_ETCS_CTL_ERR_0 > 255) {
    *rty_lcetcss16Igain = 255;
  } else {
    *rty_lcetcss16Igain = rtu_ETCS_CTL_ERR_0;
  }

  /* End of Saturate: '<Root>/Saturation1' */
}

/* Model initialize function */
void LCETCS_vCordICtlGain_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
