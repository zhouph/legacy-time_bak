/*
 * File: LCTMC_vCalInTrnRefTarGearMap.h
 *
 * Code generated for Simulink model 'LCTMC_vCalInTrnRefTarGearMap'.
 *
 * Model version                  : 1.234
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:08:55 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCTMC_vCalInTrnRefTarGearMap_h_
#define RTW_HEADER_LCTMC_vCalInTrnRefTarGearMap_h_
#ifndef LCTMC_vCalInTrnRefTarGearMap_COMMON_INCLUDES_
# define LCTMC_vCalInTrnRefTarGearMap_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCTMC_vCalInTrnRefTarGearMap_COMMON_INCLUDES_ */

#include "LCTMC_vCalInTrnRefTarGearMap_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCTMC_vCalRefVehSpdMapInTrn.h"
#include "LCTMC_vCalRefEngSpdMapInTrn.h"

/* Block states (auto storage) for model 'LCTMC_vCalInTrnRefTarGearMap' */
typedef struct {
  MdlrefDW_LCTMC_vCalRefEngSpdMapInTrn_T LCTMC_vCalRefEngSpdMapInTrn_DWORK1;/* '<Root>/LCTMC_vCalRefEngSpdMapInTrn' */
  MdlrefDW_LCTMC_vCalRefVehSpdMapInTrn_T LCTMC_vCalRefVehSpdMapInTrn_DWORK1;/* '<Root>/LCTMC_vCalRefVehSpdMapInTrn' */
} DW_LCTMC_vCalInTrnRefTarGearMap_f_T;

typedef struct {
  DW_LCTMC_vCalInTrnRefTarGearMap_f_T rtdw;
} MdlrefDW_LCTMC_vCalInTrnRefTarGearMap_T;

/* Model reference registration function */
extern void LCTMC_vCalInTrnRefTarGearMap_initialize(void);
extern void LCTMC_vCalInTrnRefTarGearMap_Init
  (DW_LCTMC_vCalInTrnRefTarGearMap_f_T *localDW);
extern void LCTMC_vCalInTrnRefTarGearMap_Start
  (DW_LCTMC_vCalInTrnRefTarGearMap_f_T *localDW);
extern void LCTMC_vCalInTrnRefTarGearMap(const TypeETCSGainStruct
  *rtu_ETCS_CTL_GAINS, int16_T *rty_lctmcs16TarShtChnVSInTrn, int16_T
  *rty_lctmcs16TarShtChnRPMInTrn, DW_LCTMC_vCalInTrnRefTarGearMap_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCTMC_vCalInTrnRefTarGearMap'
 */
#endif                                 /* RTW_HEADER_LCTMC_vCalInTrnRefTarGearMap_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
