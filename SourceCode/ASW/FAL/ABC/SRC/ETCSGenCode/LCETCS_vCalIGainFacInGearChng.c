/*
 * File: LCETCS_vCalIGainFacInGearChng.c
 *
 * Code generated for Simulink model 'LCETCS_vCalIGainFacInGearChng'.
 *
 * Model version                  : 1.176
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:01:33 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalIGainFacInGearChng.h"
#include "LCETCS_vCalIGainFacInGearChng_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalIGainFacInGearChng' */
void LCETCS_vCalIGainFacInGearChng_Init(int16_T *rty_lcetcss16IgainFacInGearChng)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_Init(rty_lcetcss16IgainFacInGearChng);
}

/* Start for referenced model: 'LCETCS_vCalIGainFacInGearChng' */
void LCETCS_vCalIGainFacInGearChng_Start(B_LCETCS_vCalIGainFacInGearChng_c_T
  *localB)
{
  /* Start for Constant: '<Root>/Constant1' */
  localB->x2 = ((uint8_T)U8ETCSCpGainTrnsTmAftrGearShft);

  /* Start for IfAction SubSystem: '<Root>/SetIgainFacInGearChang' */
  /* Start for Constant: '<S2>/Igain Variation Factor In Gear Change With Negative Control Error at 1st gear' */
  localB->y5 = ((uint8_T)U8ETCSCpIgFacGrChgErrNeg_1);

  /* Start for Constant: '<S2>/Igain Variation Factor In Gear Change With Negative Control Error at 2nd gear' */
  localB->y4 = ((uint8_T)U8ETCSCpIgFacGrChgErrNeg_2);

  /* Start for Constant: '<S2>/Igain Variation Factor In Gear Change With Negative Control Error at 3rd gear' */
  localB->y3 = ((uint8_T)U8ETCSCpIgFacGrChgErrNeg_3);

  /* Start for Constant: '<S2>/Igain Variation Factor In Gear Change With Negative Control Error at 4th gear' */
  localB->y2 = ((uint8_T)U8ETCSCpIgFacGrChgErrNeg_4);

  /* Start for Constant: '<S2>/Igain Variation Factor In Gear Change With Negative Control Error at 5th gear' */
  localB->y1 = ((uint8_T)U8ETCSCpIgFacGrChgErrNeg_5);

  /* End of Start for SubSystem: '<Root>/SetIgainFacInGearChang' */

  /* InitializeConditions for IfAction SubSystem: '<Root>/SetIgainFacInGearChang' */

  /* InitializeConditions for ModelReference: '<S2>/LCETCS_s16Inter5Point' */
  LCETCS_s16Inter5Point_Init(&localB->lcetcss16IgainFacInGearChng);

  /* End of InitializeConditions for SubSystem: '<Root>/SetIgainFacInGearChang' */
}

/* Output and update for referenced model: 'LCETCS_vCalIGainFacInGearChng' */
void LCETCS_vCalIGainFacInGearChng(const TypeETCSDrvMdlStruct *rtu_ETCS_DRV_MDL,
  const TypeETCSGainGearShftStruct *rtu_ETCS_GAIN_GEAR_SHFT, int16_T
  *rty_lcetcss16IgainFacInGearChng, B_LCETCS_vCalIGainFacInGearChng_c_T *localB)
{
  /* local block i/o variables */
  int16_T rtb_x;
  int16_T rtb_lcetcss16PgainFacInGearChng;

  /* Constant: '<Root>/Constant1' */
  localB->x2 = ((uint8_T)U8ETCSCpGainTrnsTmAftrGearShft);

  /* DataTypeConversion: '<Root>/Data Type Conversion' */
  rtb_x = rtu_ETCS_GAIN_GEAR_SHFT->lcetcsu8GainTrnsTm;

  /* If: '<Root>/If' incorporates:
   *  Constant: '<S1>/Constant'
   *  Constant: '<S2>/Igain Variation Factor In Gear Change With Negative Control Error at 1st gear'
   *  Constant: '<S2>/Igain Variation Factor In Gear Change With Negative Control Error at 2nd gear'
   *  Constant: '<S2>/Igain Variation Factor In Gear Change With Negative Control Error at 3rd gear'
   *  Constant: '<S2>/Igain Variation Factor In Gear Change With Negative Control Error at 4th gear'
   *  Constant: '<S2>/Igain Variation Factor In Gear Change With Negative Control Error at 5th gear'
   *  Constant: '<S2>/Total Gear Ratio at 1st Gear'
   *  Constant: '<S2>/Total Gear Ratio at 2nd Gear'
   *  Constant: '<S2>/Total Gear Ratio at 3rd Gear'
   *  Constant: '<S2>/Total Gear Ratio at 4th Gear'
   *  Constant: '<S2>/Total Gear Ratio at 5th Gear'
   *  Logic: '<Root>/Logical Operator'
   */
  if (!((rtu_ETCS_GAIN_GEAR_SHFT->lcetcsu1GainChngInGearShft) ||
        (rtu_ETCS_GAIN_GEAR_SHFT->lcetcsu1GainTrnsAftrGearShft))) {
    /* Outputs for IfAction SubSystem: '<Root>/ResetIgainFacInGearChang' incorporates:
     *  ActionPort: '<S1>/Action Port'
     */
    rtb_lcetcss16PgainFacInGearChng = 100;

    /* End of Outputs for SubSystem: '<Root>/ResetIgainFacInGearChang' */
  } else {
    /* Outputs for IfAction SubSystem: '<Root>/SetIgainFacInGearChang' incorporates:
     *  ActionPort: '<S2>/Action Port'
     */
    localB->y5 = ((uint8_T)U8ETCSCpIgFacGrChgErrNeg_1);
    localB->y4 = ((uint8_T)U8ETCSCpIgFacGrChgErrNeg_2);
    localB->y3 = ((uint8_T)U8ETCSCpIgFacGrChgErrNeg_3);
    localB->y2 = ((uint8_T)U8ETCSCpIgFacGrChgErrNeg_4);
    localB->y1 = ((uint8_T)U8ETCSCpIgFacGrChgErrNeg_5);

    /* ModelReference: '<S2>/LCETCS_s16Inter5Point' */
    LCETCS_s16Inter5Point(rtu_ETCS_DRV_MDL->lcetcss16TotalGearRatio, ((int16_T)
      S16ETCSCpTotalGearRatio_5), ((int16_T)S16ETCSCpTotalGearRatio_4),
                          ((int16_T)S16ETCSCpTotalGearRatio_3), ((int16_T)
      S16ETCSCpTotalGearRatio_2), ((int16_T)S16ETCSCpTotalGearRatio_1),
                          localB->y1, localB->y2, localB->y3, localB->y4,
                          localB->y5, &localB->lcetcss16IgainFacInGearChng);

    /* SignalConversion: '<S2>/OutportBufferForlcetcss16IgainFacInGearChng' incorporates:
     *  Constant: '<S2>/Igain Variation Factor In Gear Change With Negative Control Error at 1st gear'
     *  Constant: '<S2>/Igain Variation Factor In Gear Change With Negative Control Error at 2nd gear'
     *  Constant: '<S2>/Igain Variation Factor In Gear Change With Negative Control Error at 3rd gear'
     *  Constant: '<S2>/Igain Variation Factor In Gear Change With Negative Control Error at 4th gear'
     *  Constant: '<S2>/Igain Variation Factor In Gear Change With Negative Control Error at 5th gear'
     *  Constant: '<S2>/Total Gear Ratio at 1st Gear'
     *  Constant: '<S2>/Total Gear Ratio at 2nd Gear'
     *  Constant: '<S2>/Total Gear Ratio at 3rd Gear'
     *  Constant: '<S2>/Total Gear Ratio at 4th Gear'
     *  Constant: '<S2>/Total Gear Ratio at 5th Gear'
     */
    rtb_lcetcss16PgainFacInGearChng = localB->lcetcss16IgainFacInGearChng;

    /* End of Outputs for SubSystem: '<Root>/SetIgainFacInGearChang' */
  }

  /* End of If: '<Root>/If' */

  /* ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point(rtb_x, rtCP_Constant2_Value, localB->x2,
                        rtb_lcetcss16PgainFacInGearChng, rtCP_Constant3_Value,
                        rty_lcetcss16IgainFacInGearChng);
}

/* Model initialize function */
void LCETCS_vCalIGainFacInGearChng_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_initialize();

  /* Model Initialize fcn for ModelReference Block: '<S2>/LCETCS_s16Inter5Point' */
  LCETCS_s16Inter5Point_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
