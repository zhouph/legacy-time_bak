/*
 * File: LCETCS_vCalDeldSigDepOnEngSpd.h
 *
 * Code generated for Simulink model 'LCETCS_vCalDeldSigDepOnEngSpd'.
 *
 * Model version                  : 1.99
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:59:03 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalDeldSigDepOnEngSpd_h_
#define RTW_HEADER_LCETCS_vCalDeldSigDepOnEngSpd_h_
#ifndef LCETCS_vCalDeldSigDepOnEngSpd_COMMON_INCLUDES_
# define LCETCS_vCalDeldSigDepOnEngSpd_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalDeldSigDepOnEngSpd_COMMON_INCLUDES_ */

#include "LCETCS_vCalDeldSigDepOnEngSpd_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_s16Inter5Point.h"

/* Block signals for model 'LCETCS_vCalDeldSigDepOnEngSpd' */
typedef struct {
  int16_T y1;                          /* '<Root>/Constant5' */
  int16_T y2;                          /* '<Root>/Constant6' */
  int16_T y3;                          /* '<Root>/Constant7' */
  int16_T y4;                          /* '<Root>/Constant8' */
  int16_T y5;                          /* '<Root>/Constant9' */
  int16_T y;                           /* '<Root>/LCETCS_s16Inter5Point' */
} B_LCETCS_vCalDeldSigDepOnEngSpd_c_T;

/* Block states (auto storage) for model 'LCETCS_vCalDeldSigDepOnEngSpd' */
typedef struct {
  int16_T Delay_DSTATE[10];            /* '<Root>/Delay' */
  int16_T CircBufIdx;                  /* '<Root>/Delay' */
} DW_LCETCS_vCalDeldSigDepOnEngSpd_f_T;

typedef struct {
  B_LCETCS_vCalDeldSigDepOnEngSpd_c_T rtb;
  DW_LCETCS_vCalDeldSigDepOnEngSpd_f_T rtdw;
} MdlrefDW_LCETCS_vCalDeldSigDepOnEngSpd_T;

/* Model reference registration function */
extern void LCETCS_vCalDeldSigDepOnEngSpd_initialize(void);
extern void LCETCS_vCalDeldSigDepOnEngSpd_Init
  (B_LCETCS_vCalDeldSigDepOnEngSpd_c_T *localB,
   DW_LCETCS_vCalDeldSigDepOnEngSpd_f_T *localDW);
extern void LCETCS_vCalDeldSigDepOnEngSpd_Start
  (B_LCETCS_vCalDeldSigDepOnEngSpd_c_T *localB);
extern void LCETCS_vCalDeldSigDepOnEngSpd(const TypeETCSEngStruct
  *rtu_ETCS_ENG_STRUCT, int16_T rtu_lcetcss16ActEngTrqFltd, TypeETCSEngMdlStruct
  *rty_ETCS_ENG_MDL, B_LCETCS_vCalDeldSigDepOnEngSpd_c_T *localB,
  DW_LCETCS_vCalDeldSigDepOnEngSpd_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalDeldSigDepOnEngSpd'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalDeldSigDepOnEngSpd_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
