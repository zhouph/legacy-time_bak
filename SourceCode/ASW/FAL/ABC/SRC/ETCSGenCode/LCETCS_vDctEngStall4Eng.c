/*
 * File: LCETCS_vDctEngStall4Eng.c
 *
 * Code generated for Simulink model 'LCETCS_vDctEngStall4Eng'.
 *
 * Model version                  : 1.275
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:13:21 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctEngStall4Eng.h"
#include "LCETCS_vDctEngStall4Eng_private.h"

/* Initial conditions for referenced model: 'LCETCS_vDctEngStall4Eng' */
void LCETCS_vDctEngStall4Eng_Init(boolean_T *rty_lcetcsu1EngStallTemp,
  DW_LCETCS_vDctEngStall4Eng_f_T *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalEngSpdSlope' */
  LCETCS_vCalEngSpdSlope_Init(&(localDW->LCETCS_vCalEngSpdSlope_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalEngStallThr4Ax' */
  LCETCS_vCalEngStallThr4Ax_Init
    (&(localDW->LCETCS_vCalEngStallThr4Ax_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctEngStlEnterExitCond' */
  LCETCS_vDctEngStlEnterExitCond_Init(rty_lcetcsu1EngStallTemp,
    &(localDW->LCETCS_vDctEngStlEnterExitCond_DWORK1.rtdw));
}

/* Disable for referenced model: 'LCETCS_vDctEngStall4Eng' */
void LCETCS_vDctEngStall4Eng_Disable(DW_LCETCS_vDctEngStall4Eng_f_T *localDW)
{
  /* Disable for ModelReference: '<Root>/LCETCS_vCalEngStallThr4Ax' */
  LCETCS_vCalEngStallThr4Ax_Disable
    (&(localDW->LCETCS_vCalEngStallThr4Ax_DWORK1.rtdw));
}

/* Start for referenced model: 'LCETCS_vDctEngStall4Eng' */
void LCETCS_vDctEngStall4Eng_Start(DW_LCETCS_vDctEngStall4Eng_f_T *localDW)
{
  /* Start for ModelReference: '<Root>/LCETCS_vCalEngStallThr4Ax' */
  LCETCS_vCalEngStallThr4Ax_Start
    (&(localDW->LCETCS_vCalEngStallThr4Ax_DWORK1.rtdw));
}

/* Output and update for referenced model: 'LCETCS_vDctEngStall4Eng' */
void LCETCS_vDctEngStall4Eng(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT_OLD,
  const TypeETCSSplitHillStruct *rtu_ETCS_SPLIT_HILL_OLD, int16_T
  rtu_lcetcss16VehSpd, const TypeETCSEngStruct *rtu_ETCS_ENG_STRUCT, const
  TypeETCSEngStallStruct *rtu_ETCS_ENG_STALL_OLD, boolean_T
  *rty_lcetcsu1EngStallTemp, int16_T *rty_lcetcss16EngStallThr, int8_T
  *rty_lcetcss8EngStallRiskIndex, int16_T *rty_lcetcss16EngSpdSlope,
  B_LCETCS_vDctEngStall4Eng_c_T *localB, DW_LCETCS_vDctEngStall4Eng_f_T *localDW)
{
  /* ModelReference: '<Root>/LCETCS_vCalEngSpdSlope' */
  LCETCS_vCalEngSpdSlope(rtu_ETCS_ENG_STRUCT, rty_lcetcss16EngSpdSlope,
    &(localDW->LCETCS_vCalEngSpdSlope_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalEngStallThr4Ax' */
  LCETCS_vCalEngStallThr4Ax(rtu_ETCS_CTL_ACT_OLD, rtu_ETCS_ENG_STALL_OLD,
    rtu_ETCS_SPLIT_HILL_OLD, rtu_lcetcss16VehSpd, (*rty_lcetcss16EngSpdSlope),
    &localB->lcetcss16EngStallThrTemp, rty_lcetcss8EngStallRiskIndex,
    &(localDW->LCETCS_vCalEngStallThr4Ax_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vDctEngStallAxSenExist' */
  LCETCS_vDctEngStallAxSenExist(localB->lcetcss16EngStallThrTemp,
    rty_lcetcss16EngStallThr);

  /* ModelReference: '<Root>/LCETCS_vDctEngStlEnterExitCond' */
  LCETCS_vDctEngStlEnterExitCond((*rty_lcetcss16EngStallThr),
    rtu_ETCS_ENG_STRUCT, (*rty_lcetcss16EngSpdSlope), rty_lcetcsu1EngStallTemp,
    &(localDW->LCETCS_vDctEngStlEnterExitCond_DWORK1.rtdw));
}

/* Model initialize function */
void LCETCS_vDctEngStall4Eng_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalEngSpdSlope' */
  LCETCS_vCalEngSpdSlope_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalEngStallThr4Ax' */
  LCETCS_vCalEngStallThr4Ax_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctEngStallAxSenExist' */
  LCETCS_vDctEngStallAxSenExist_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctEngStlEnterExitCond' */
  LCETCS_vDctEngStlEnterExitCond_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
