/*
 * File: LCETCS_vCalTarWhlSpinInLwToSplt.c
 *
 * Code generated for Simulink model 'LCETCS_vCalTarWhlSpinInLwToSplt'.
 *
 * Model version                  : 1.50
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:00:07 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalTarWhlSpinInLwToSplt.h"
#include "LCETCS_vCalTarWhlSpinInLwToSplt_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalTarWhlSpinInLwToSplt' */
void LCETCS_vCalTarWhlSpinInLwToSplt_Init(B_LCETCS_vCalTarWhlSpinInLwToSplt_c_T *
  localB)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_Init(&localB->lcetcss16TarWhlSpinIncLwToSplt);
}

/* Output and update for referenced model: 'LCETCS_vCalTarWhlSpinInLwToSplt' */
void LCETCS_vCalTarWhlSpinInLwToSplt(const TypeETCSLwToSpltStruct
  *rtu_ETCS_L2Split, const TypeETCSBrkSigStruct *rtu_ETCS_BRK_SIG, int16_T
  *rty_lcetcss16TarWhlSpinIncLwToSplt, B_LCETCS_vCalTarWhlSpinInLwToSplt_c_T
  *localB)
{
  /* local block i/o variables */
  int16_T rtb_y2;

  /* Product: '<Root>/Divide' */
  rtb_y2 = (int16_T)(rtu_ETCS_BRK_SIG->lcetcss16BtcsTarWhlSpnDiff / 2);

  /* Sum: '<Root>/Add' incorporates:
   *  Constant: '<Root>/Constant1'
   */
  rtb_y2 += ((uint8_T)U8ETCSCpAddTarWhlSpnLwToSp);

  /* ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point(rtu_ETCS_L2Split->lcetcss16LwToSplitCnt, ((int16_T)
    S16ETCSCpLwToSpltDctStrtCnt), ((int16_T)S16ETCSCpLwToSpltDctCplCnt),
                        rtCP_Constant4_Value, rtb_y2,
                        &localB->lcetcss16TarWhlSpinIncLwToSplt);

  /* Saturate: '<Root>/Saturation' */
  if (localB->lcetcss16TarWhlSpinIncLwToSplt > 255) {
    *rty_lcetcss16TarWhlSpinIncLwToSplt = 255;
  } else if (localB->lcetcss16TarWhlSpinIncLwToSplt < 0) {
    *rty_lcetcss16TarWhlSpinIncLwToSplt = 0;
  } else {
    *rty_lcetcss16TarWhlSpinIncLwToSplt = localB->lcetcss16TarWhlSpinIncLwToSplt;
  }

  /* End of Saturate: '<Root>/Saturation' */
}

/* Model initialize function */
void LCETCS_vCalTarWhlSpinInLwToSplt_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
