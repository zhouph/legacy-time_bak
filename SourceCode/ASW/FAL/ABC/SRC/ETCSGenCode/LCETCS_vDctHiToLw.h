/*
 * File: LCETCS_vDctHiToLw.h
 *
 * Code generated for Simulink model 'LCETCS_vDctHiToLw'.
 *
 * Model version                  : 1.225
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:06:01 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vDctHiToLw_h_
#define RTW_HEADER_LCETCS_vDctHiToLw_h_
#ifndef LCETCS_vDctHiToLw_COMMON_INCLUDES_
# define LCETCS_vDctHiToLw_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vDctHiToLw_COMMON_INCLUDES_ */

#include "LCETCS_vDctHiToLw_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vDctRisingEdge.h"

/* Block signals for model 'LCETCS_vDctHiToLw' */
typedef struct {
  boolean_T lcetcsu1HiToLwRsgEdg;      /* '<Root>/LCETCS_vDctRisingEdge' */
  boolean_T lcetcsu1HiToLw;            /* '<Root>/Chart' */
} B_LCETCS_vDctHiToLw_c_T;

/* Block states (auto storage) for model 'LCETCS_vDctHiToLw' */
typedef struct {
  TypeETCSH2LStruct UnitDelay_DSTATE;  /* '<Root>/Unit Delay' */
  int16_T Delay1_DSTATE[25];           /* '<Root>/Delay1' */
  int16_T Delay2_DSTATE[25];           /* '<Root>/Delay2' */
  int8_T CircBufIdx;                   /* '<Root>/Delay1' */
  int8_T CircBufIdx_b;                 /* '<Root>/Delay2' */
  MdlrefDW_LCETCS_vDctRisingEdge_T LCETCS_vDctRisingEdge_DWORK1;/* '<Root>/LCETCS_vDctRisingEdge' */
} DW_LCETCS_vDctHiToLw_f_T;

typedef struct {
  B_LCETCS_vDctHiToLw_c_T rtb;
  DW_LCETCS_vDctHiToLw_f_T rtdw;
} MdlrefDW_LCETCS_vDctHiToLw_T;

/* Model reference registration function */
extern void LCETCS_vDctHiToLw_initialize(const rtTimingBridge *timingBridge);
extern const TypeETCSH2LStruct LCETCS_vDctHiToLw_rtZTypeETCSH2LStruct;/* TypeETCSH2LStruct ground */
extern void LCETCS_vDctHiToLw_Init(TypeETCSH2LStruct *rty_ETCS_H2L,
  DW_LCETCS_vDctHiToLw_f_T *localDW, B_LCETCS_vDctHiToLw_c_T *localB);
extern void LCETCS_vDctHiToLw(const TypeETCSErrZroCrsStruct
  *rtu_ETCS_ERR_ZRO_CRS, const TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC, const
  TypeETCSWhlSpinStruct *rtu_ETCS_WHL_SPIN, TypeETCSH2LStruct *rty_ETCS_H2L,
  DW_LCETCS_vDctHiToLw_f_T *localDW, B_LCETCS_vDctHiToLw_c_T *localB);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vDctHiToLw'
 * '<S1>'   : 'LCETCS_vDctHiToLw/Chart'
 */
#endif                                 /* RTW_HEADER_LCETCS_vDctHiToLw_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
