/*
 * File: LCETCS_vCalAddTar4DepSnw.c
 *
 * Code generated for Simulink model 'LCETCS_vCalAddTar4DepSnw'.
 *
 * Model version                  : 1.61
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:59:23 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalAddTar4DepSnw.h"
#include "LCETCS_vCalAddTar4DepSnw_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalAddTar4DepSnw' */
void LCETCS_vCalAddTar4DepSnw_Init(B_LCETCS_vCalAddTar4DepSnw_c_T *localB)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_Init(&localB->y);
}

/* Output and update for referenced model: 'LCETCS_vCalAddTar4DepSnw' */
void LCETCS_vCalAddTar4DepSnw(int16_T rtu_lcetcss16VehSpd, const
  TypeETCSDepSnwStruct *rtu_ETCS_DEP_SNW, int16_T *rty_lcetcss16AddTar4DepSnw,
  B_LCETCS_vCalAddTar4DepSnw_c_T *localB)
{
  /* local block i/o variables */
  int16_T rtb_y1;
  int16_T rtb_x1;
  int16_T rtb_x2;

  /* Constant: '<Root>/Constant1' */
  rtb_x1 = ((uint8_T)U8ETCSCpAlwAddTar4DepSnwSpd);

  /* Constant: '<Root>/Constant2' */
  rtb_x2 = ((uint8_T)U8ETCSCpNAlwAddTar4DepSnwSpd);

  /* Gain: '<Root>/Gain' */
  rtb_x1 = (int16_T)(rtb_x1 << 3);

  /* Gain: '<Root>/Gain1' */
  rtb_x2 = (int16_T)(rtb_x2 << 3);

  /* Switch: '<Root>/Switch4' incorporates:
   *  Constant: '<Root>/Constant4'
   *  Constant: '<Root>/Constant5'
   */
  if (rtu_ETCS_DEP_SNW->lcetcsu1DepSnwHilAct) {
    rtb_y1 = ((uint8_T)U8ETCSCpTarSpnAdd4DepSnwHil);
  } else {
    rtb_y1 = 0;
  }

  /* End of Switch: '<Root>/Switch4' */

  /* ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point(rtu_lcetcss16VehSpd, rtb_x1, rtb_x2, rtb_y1,
                        rtCP_Constant3_Value, &localB->y);

  /* Saturate: '<Root>/Saturation' */
  if (localB->y > ((uint8_T)U8ETCSCpTarSpnAdd4DepSnwHil)) {
    *rty_lcetcss16AddTar4DepSnw = ((uint8_T)U8ETCSCpTarSpnAdd4DepSnwHil);
  } else if (localB->y < 0) {
    *rty_lcetcss16AddTar4DepSnw = 0;
  } else {
    *rty_lcetcss16AddTar4DepSnw = localB->y;
  }

  /* End of Saturate: '<Root>/Saturation' */
}

/* Model initialize function */
void LCETCS_vCalAddTar4DepSnw_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
