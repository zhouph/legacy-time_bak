/*
 * File: LCETCS_vDctZeroCrossingOfErr.c
 *
 * Code generated for Simulink model 'LCETCS_vDctZeroCrossingOfErr'.
 *
 * Model version                  : 1.126
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:06:50 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctZeroCrossingOfErr.h"
#include "LCETCS_vDctZeroCrossingOfErr_private.h"

const rtTimingBridge *LCETCS_vDctZeroCrossingOfErr_TimingBrdg;

/* Initial conditions for referenced model: 'LCETCS_vDctZeroCrossingOfErr' */
void LCETCS_vDctZeroCrossingOfErr_Init(TypeETCSErrZroCrsStruct
  *rty_ETCS_ERR_ZRO_CRS, DW_LCETCS_vDctZeroCrossingOfErr_f_T *localDW,
  B_LCETCS_vDctZeroCrossingOfErr_c_T *localB)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = 0;

  /* InitializeConditions for Chart: '<Root>/Chart' */
  localB->lcetcsu1JumpUp = false;
  localB->lcetcsu1JumpDn = false;

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctFallingEdge' */
  LCETCS_vDctFallingEdge_Init(&localB->lcetcsu1JumpUpFalEdg,
    &(localDW->LCETCS_vDctFallingEdge_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctFallingEdge1' */
  LCETCS_vDctFallingEdge_Init(&localB->lcetcsu1JumpDnFalEdg,
    &(localDW->LCETCS_vDctFallingEdge1_DWORK1.rtdw));

  /* InitializeConditions for BusCreator: '<Root>/Bus Creator' incorporates:
   *  InitializeConditions for Chart: '<Root>/Chart'
   */
  if (rtmIsFirstInitCond()) {
    rty_ETCS_ERR_ZRO_CRS->lcetcsu1ErrZroCrs = false;
    rty_ETCS_ERR_ZRO_CRS->lcetcsu1JumpUp = localB->lcetcsu1JumpUp;
    rty_ETCS_ERR_ZRO_CRS->lcetcsu1JumpUpFalEdg = localB->lcetcsu1JumpUpFalEdg;
    rty_ETCS_ERR_ZRO_CRS->lcetcsu1JumpDn = localB->lcetcsu1JumpDn;
    rty_ETCS_ERR_ZRO_CRS->lcetcsu1JumpDnFalEdg = localB->lcetcsu1JumpDnFalEdg;
  }

  /* End of InitializeConditions for BusCreator: '<Root>/Bus Creator' */
}

/* Output and update for referenced model: 'LCETCS_vDctZeroCrossingOfErr' */
void LCETCS_vDctZeroCrossingOfErr(const TypeETCSCyl1stStruct *rtu_ETCS_CYL_1ST,
  const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const TypeETCSCtlErrStruct
  *rtu_ETCS_CTL_ERR, TypeETCSErrZroCrsStruct *rty_ETCS_ERR_ZRO_CRS,
  DW_LCETCS_vDctZeroCrossingOfErr_f_T *localDW,
  B_LCETCS_vDctZeroCrossingOfErr_c_T *localB)
{
  boolean_T lcetcsu1ErrZroCrs;

  /* Chart: '<Root>/Chart' incorporates:
   *  UnitDelay: '<Root>/Unit Delay'
   */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:9' */
  /* comment */
  if ((rtu_ETCS_CTL_ACT->lcetcsu1CtlAct == 1) &&
      (rtu_ETCS_CYL_1ST->lcetcsu1Cyl1st == 0)) {
    /* Transition: '<S1>:10' */
    /* Transition: '<S1>:40' */
    if ((localDW->UnitDelay_DSTATE < 0) && (rtu_ETCS_CTL_ERR->lcetcss16CtlErr >=
         0)) {
      /* Transition: '<S1>:32' */
      /* Transition: '<S1>:33' */
      lcetcsu1ErrZroCrs = true;
      localB->lcetcsu1JumpUp = true;
      localB->lcetcsu1JumpDn = false;

      /* Transition: '<S1>:36' */
      /* Transition: '<S1>:37' */
    } else {
      /* Transition: '<S1>:31' */
      if ((localDW->UnitDelay_DSTATE > 0) && (rtu_ETCS_CTL_ERR->lcetcss16CtlErr <=
           0)) {
        /* Transition: '<S1>:34' */
        /* Transition: '<S1>:35' */
        lcetcsu1ErrZroCrs = true;
        localB->lcetcsu1JumpUp = false;
        localB->lcetcsu1JumpDn = true;

        /* Transition: '<S1>:37' */
      } else {
        /* Transition: '<S1>:38' */
        lcetcsu1ErrZroCrs = false;
        localB->lcetcsu1JumpUp = false;
        localB->lcetcsu1JumpDn = false;
      }
    }

    /* Transition: '<S1>:42' */
  } else {
    /* Transition: '<S1>:13' */
    lcetcsu1ErrZroCrs = false;
    localB->lcetcsu1JumpUp = false;
    localB->lcetcsu1JumpDn = false;
  }

  /* End of Chart: '<Root>/Chart' */
  /* Transition: '<S1>:12' */

  /* ModelReference: '<Root>/LCETCS_vDctFallingEdge' */
  LCETCS_vDctFallingEdge(localB->lcetcsu1JumpUp, &localB->lcetcsu1JumpUpFalEdg,
    &(localDW->LCETCS_vDctFallingEdge_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vDctFallingEdge1' */
  LCETCS_vDctFallingEdge(localB->lcetcsu1JumpDn, &localB->lcetcsu1JumpDnFalEdg,
    &(localDW->LCETCS_vDctFallingEdge1_DWORK1.rtdw));

  /* BusCreator: '<Root>/Bus Creator' */
  rty_ETCS_ERR_ZRO_CRS->lcetcsu1ErrZroCrs = lcetcsu1ErrZroCrs;
  rty_ETCS_ERR_ZRO_CRS->lcetcsu1JumpUp = localB->lcetcsu1JumpUp;
  rty_ETCS_ERR_ZRO_CRS->lcetcsu1JumpUpFalEdg = localB->lcetcsu1JumpUpFalEdg;
  rty_ETCS_ERR_ZRO_CRS->lcetcsu1JumpDn = localB->lcetcsu1JumpDn;
  rty_ETCS_ERR_ZRO_CRS->lcetcsu1JumpDnFalEdg = localB->lcetcsu1JumpDnFalEdg;

  /* Update for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = rtu_ETCS_CTL_ERR->lcetcss16CtlErr;
}

/* Model initialize function */
void LCETCS_vDctZeroCrossingOfErr_initialize(const rtTimingBridge *timingBridge)
{
  /* Registration code */
  LCETCS_vDctZeroCrossingOfErr_TimingBrdg = timingBridge;

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctFallingEdge' */
  LCETCS_vDctFallingEdge_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctFallingEdge1' */
  LCETCS_vDctFallingEdge_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
