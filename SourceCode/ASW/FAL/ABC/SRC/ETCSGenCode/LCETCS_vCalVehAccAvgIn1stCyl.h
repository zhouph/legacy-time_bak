/*
 * File: LCETCS_vCalVehAccAvgIn1stCyl.h
 *
 * Code generated for Simulink model 'LCETCS_vCalVehAccAvgIn1stCyl'.
 *
 * Model version                  : 1.534
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:09:44 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalVehAccAvgIn1stCyl_h_
#define RTW_HEADER_LCETCS_vCalVehAccAvgIn1stCyl_h_
#ifndef LCETCS_vCalVehAccAvgIn1stCyl_COMMON_INCLUDES_
# define LCETCS_vCalVehAccAvgIn1stCyl_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalVehAccAvgIn1stCyl_COMMON_INCLUDES_ */

#include "LCETCS_vCalVehAccAvgIn1stCyl_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vConvAx2CrdnTrq.h"

/* Block states (auto storage) for model 'LCETCS_vCalVehAccAvgIn1stCyl' */
typedef struct {
  TypeETCSRefTrqStr2CylStruct UnitDelay1_DSTATE;/* '<Root>/Unit Delay1' */
  MdlrefDW_LCETCS_vConvAx2CrdnTrq_T LCETCS_vConvAx2CrdnTrq_DWORK1;/* '<Root>/LCETCS_vConvAx2CrdnTrq' */
} DW_LCETCS_vCalVehAccAvgIn1stCyl_f_T;

typedef struct {
  DW_LCETCS_vCalVehAccAvgIn1stCyl_f_T rtdw;
} MdlrefDW_LCETCS_vCalVehAccAvgIn1stCyl_T;

/* Model reference registration function */
extern void LCETCS_vCalVehAccAvgIn1stCyl_initialize(void);
extern const TypeETCSRefTrqStr2CylStruct
  LCETCS_vCalVehAccAvgIn1stCyl_rtZTypeETCSRefTrqStr2CylStruct;/* TypeETCSRefTrqStr2CylStruct ground */
extern void LCETCS_vCalVehAccAvgIn1stCyl_Init
  (DW_LCETCS_vCalVehAccAvgIn1stCyl_f_T *localDW);
extern void LCETCS_vCalVehAccAvgIn1stCyl_Start
  (DW_LCETCS_vCalVehAccAvgIn1stCyl_f_T *localDW);
extern void LCETCS_vCalVehAccAvgIn1stCyl(int16_T rtu_lcetcss16VehSpd, const
  TypeETCSCyl1stStruct *rtu_ETCS_CYL_1ST, const TypeETCSCtlActStruct
  *rtu_ETCS_CTL_ACT, const TypeETCSH2LStruct *rtu_ETCS_H2L, const
  TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC, TypeETCSRefTrqStr2CylStruct
  *rty_ETCS_REF_TRQ_STR2CYL, DW_LCETCS_vCalVehAccAvgIn1stCyl_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalVehAccAvgIn1stCyl'
 * '<S1>'   : 'LCETCS_vCalVehAccAvgIn1stCyl/CalVehAccAvgIn1stCyl'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalVehAccAvgIn1stCyl_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
