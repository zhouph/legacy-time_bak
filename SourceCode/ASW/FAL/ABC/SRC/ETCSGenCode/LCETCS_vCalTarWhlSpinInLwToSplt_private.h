/*
 * File: LCETCS_vCalTarWhlSpinInLwToSplt_private.h
 *
 * Code generated for Simulink model 'LCETCS_vCalTarWhlSpinInLwToSplt'.
 *
 * Model version                  : 1.50
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:00:07 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalTarWhlSpinInLwToSplt_private_h_
#define RTW_HEADER_LCETCS_vCalTarWhlSpinInLwToSplt_private_h_
#include "rtwtypes.h"
#include "model_reference_types.h"

/* Includes for objects with custom storage classes. */
#include "../ETCSGenCodeHeader/ETCSHeader4Import.h"

/*
 * Generate compile time checks that imported macros for parameters
 * with storage class "ImportedDefine" are defined
 */
#ifndef S16ETCSCpLwToSpltDctCplCnt
#error The variable for the parameter "S16ETCSCpLwToSpltDctCplCnt" is not defined
#endif

#ifndef S16ETCSCpLwToSpltDctStrtCnt
#error The variable for the parameter "S16ETCSCpLwToSpltDctStrtCnt" is not defined
#endif

#ifndef U8ETCSCpAddTarWhlSpnLwToSp
#error The variable for the parameter "U8ETCSCpAddTarWhlSpnLwToSp" is not defined
#endif

extern const int16_T rtCP_pooled_ZjYULP70HcKf;

#define rtCP_Constant4_Value           rtCP_pooled_ZjYULP70HcKf  /* Computed Parameter: Constant4_Value
                                                                  * Referenced by: '<Root>/Constant4'
                                                                  */
#endif                                 /* RTW_HEADER_LCETCS_vCalTarWhlSpinInLwToSplt_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
