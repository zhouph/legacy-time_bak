/*
 * File: LCETCS_vCalTransEfficiencyRaw.c
 *
 * Code generated for Simulink model 'LCETCS_vCalTransEfficiencyRaw'.
 *
 * Model version                  : 1.74
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:47:39 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalTransEfficiencyRaw.h"
#include "LCETCS_vCalTransEfficiencyRaw_private.h"

/* Output and update for referenced model: 'LCETCS_vCalTransEfficiencyRaw' */
void LCETCS_vCalTransEfficiencyRaw(const TypeETCSGearStruct
  *rtu_ETCS_GEAR_STRUCT, int16_T *rty_lcetcss16TransEfficiencyRaw)
{
  /* Chart: '<Root>/Chart3' */
  /* Gateway: Chart3 */
  /* During: Chart3 */
  /* Entry Internal: Chart3 */
  /* Transition: '<S1>:123' */
  if (rtu_ETCS_GEAR_STRUCT->lcetcss16CurntGearStep == ((int16_T)GEAR_POS_TM_R))
  {
    /* Transition: '<S1>:101' */
    /* Transition: '<S1>:102' */
    *rty_lcetcss16TransEfficiencyRaw = ((uint8_T)U8ETCSCpTransEff_R);

    /* Transition: '<S1>:103' */
    /* Transition: '<S1>:104' */
    /* Transition: '<S1>:110' */
    /* Transition: '<S1>:115' */
    /* Transition: '<S1>:120' */
    /* Transition: '<S1>:143' */
    /* Transition: '<S1>:144' */
    /* Transition: '<S1>:122' */
  } else {
    /* Transition: '<S1>:108' */
    if (rtu_ETCS_GEAR_STRUCT->lcetcss16CurntGearStep == ((int16_T)GEAR_POS_TM_1))
    {
      /* Transition: '<S1>:105' */
      /* Transition: '<S1>:106' */
      *rty_lcetcss16TransEfficiencyRaw = ((uint8_T)U8ETCSCpTransEff_1);

      /* Transition: '<S1>:107' */
      /* Transition: '<S1>:110' */
      /* Transition: '<S1>:115' */
      /* Transition: '<S1>:120' */
      /* Transition: '<S1>:143' */
      /* Transition: '<S1>:144' */
      /* Transition: '<S1>:122' */
    } else {
      /* Transition: '<S1>:109' */
      if (rtu_ETCS_GEAR_STRUCT->lcetcss16CurntGearStep == ((int16_T)
           GEAR_POS_TM_2)) {
        /* Transition: '<S1>:111' */
        /* Transition: '<S1>:112' */
        *rty_lcetcss16TransEfficiencyRaw = ((uint8_T)U8ETCSCpTransEff_2);

        /* Transition: '<S1>:113' */
        /* Transition: '<S1>:115' */
        /* Transition: '<S1>:120' */
        /* Transition: '<S1>:143' */
        /* Transition: '<S1>:144' */
        /* Transition: '<S1>:122' */
      } else {
        /* Transition: '<S1>:114' */
        if (rtu_ETCS_GEAR_STRUCT->lcetcss16CurntGearStep == ((int16_T)
             GEAR_POS_TM_3)) {
          /* Transition: '<S1>:116' */
          /* Transition: '<S1>:117' */
          *rty_lcetcss16TransEfficiencyRaw = ((uint8_T)U8ETCSCpTransEff_3);

          /* Transition: '<S1>:118' */
          /* Transition: '<S1>:120' */
          /* Transition: '<S1>:143' */
          /* Transition: '<S1>:144' */
          /* Transition: '<S1>:122' */
        } else {
          /* Transition: '<S1>:119' */
          if (rtu_ETCS_GEAR_STRUCT->lcetcss16CurntGearStep == ((int16_T)
               GEAR_POS_TM_4)) {
            /* Transition: '<S1>:128' */
            /* Transition: '<S1>:129' */
            *rty_lcetcss16TransEfficiencyRaw = ((uint8_T)U8ETCSCpTransEff_4);

            /* Transition: '<S1>:145' */
            /* Transition: '<S1>:143' */
            /* Transition: '<S1>:144' */
            /* Transition: '<S1>:122' */
          } else {
            /* Transition: '<S1>:137' */
            if (rtu_ETCS_GEAR_STRUCT->lcetcss16CurntGearStep == ((int16_T)
                 GEAR_POS_TM_5)) {
              /* Transition: '<S1>:134' */
              /* Transition: '<S1>:135' */
              *rty_lcetcss16TransEfficiencyRaw = ((uint8_T)U8ETCSCpTransEff_5);

              /* Transition: '<S1>:146' */
              /* Transition: '<S1>:144' */
              /* Transition: '<S1>:122' */
            } else {
              /* Transition: '<S1>:138' */
              /*  For the vehicle has higher gear step than 5, the tramission efficiency will be saturated to fifth gear  */
              *rty_lcetcss16TransEfficiencyRaw = ((uint8_T)U8ETCSCpTransEff_5);
            }
          }
        }
      }
    }
  }

  /* End of Chart: '<Root>/Chart3' */

  /* Saturate: '<Root>/Saturation' */
  /* Transition: '<S1>:121' */
  if ((*rty_lcetcss16TransEfficiencyRaw) > 100) {
    *rty_lcetcss16TransEfficiencyRaw = 100;
  }

  /* End of Saturate: '<Root>/Saturation' */
}

/* Model initialize function */
void LCETCS_vCalTransEfficiencyRaw_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
