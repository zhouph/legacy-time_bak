/*
 * File: LCETCS_vCalLmtCrngDltYaw_private.h
 *
 * Code generated for Simulink model 'LCETCS_vCalLmtCrngDltYaw'.
 *
 * Model version                  : 1.66
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:59:49 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalLmtCrngDltYaw_private_h_
#define RTW_HEADER_LCETCS_vCalLmtCrngDltYaw_private_h_
#include "rtwtypes.h"
#include "model_reference_types.h"

/* Includes for objects with custom storage classes. */
#include "../ETCSGenCodeHeader/ETCSHeader4Import.h"

/*
 * Generate compile time checks that imported macros for parameters
 * with storage class "ImportedDefine" are defined
 */
#ifndef U8ETCSCpDltYawNVldSpd
#error The variable for the parameter "U8ETCSCpDltYawNVldSpd" is not defined
#endif

#ifndef U8ETCSCpDltYawVldSpd
#error The variable for the parameter "U8ETCSCpDltYawVldSpd" is not defined
#endif

#ifndef U8ETCSCpDltYawVldVyHSpd
#error The variable for the parameter "U8ETCSCpDltYawVldVyHSpd" is not defined
#endif

#ifndef U8ETCSCpTarNChngDltYawHiBrkTrq
#error The variable for the parameter "U8ETCSCpTarNChngDltYawHiBrkTrq" is not defined
#endif

#ifndef U8ETCSCpTarNChngDltYawHiSpd
#error The variable for the parameter "U8ETCSCpTarNChngDltYawHiSpd" is not defined
#endif

#ifndef U8ETCSCpTarNChngDltYawLwBrkTrq
#error The variable for the parameter "U8ETCSCpTarNChngDltYawLwBrkTrq" is not defined
#endif

#ifndef U8ETCSCpTarNChngDltYawLwSpd
#error The variable for the parameter "U8ETCSCpTarNChngDltYawLwSpd" is not defined
#endif

#ifndef U8ETCSCpTarNChngDltYawVyHSpd
#error The variable for the parameter "U8ETCSCpTarNChngDltYawVyHSpd" is not defined
#endif

#ifndef U8ETCSCpTarTransCplBrkTrq2Sp
#error The variable for the parameter "U8ETCSCpTarTransCplBrkTrq2Sp" is not defined
#endif

#ifndef U8ETCSCpTarTransStBrkTrq2Sp
#error The variable for the parameter "U8ETCSCpTarTransStBrkTrq2Sp" is not defined
#endif
#endif                                 /* RTW_HEADER_LCETCS_vCalLmtCrngDltYaw_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
