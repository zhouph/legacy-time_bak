/*
 * File: LCTMC_vDctDriveMode_private.h
 *
 * Code generated for Simulink model 'LCTMC_vDctDriveMode'.
 *
 * Model version                  : 1.235
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:53:13 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCTMC_vDctDriveMode_private_h_
#define RTW_HEADER_LCTMC_vDctDriveMode_private_h_
#include "rtwtypes.h"
#include "model_reference_types.h"

/* Includes for objects with custom storage classes. */
#include "../ETCSGenCodeHeader/ETCSHeader4Import.h"

/*
 * Generate compile time checks that imported macros for parameters
 * with storage class "ImportedDefine" are defined
 */
#ifndef GEAR_POS_TM_PN
#error The variable for the parameter "GEAR_POS_TM_PN" is not defined
#endif

#ifndef GEAR_POS_TM_R
#error The variable for the parameter "GEAR_POS_TM_R" is not defined
#endif

#ifndef GEAR_SEL_M
#error The variable for the parameter "GEAR_SEL_M" is not defined
#endif

#ifndef U8TMCpMaxGear
#error The variable for the parameter "U8TMCpMaxGear" is not defined
#endif

#ifndef U8TMCpMinGear
#error The variable for the parameter "U8TMCpMinGear" is not defined
#endif
#endif                                 /* RTW_HEADER_LCTMC_vDctDriveMode_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
