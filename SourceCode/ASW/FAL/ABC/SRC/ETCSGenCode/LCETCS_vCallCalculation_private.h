/*
 * File: LCETCS_vCallCalculation_private.h
 *
 * Code generated for Simulink model 'LCETCS_vCallCalculation'.
 *
 * Model version                  : 1.294
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:14:16 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCallCalculation_private_h_
#define RTW_HEADER_LCETCS_vCallCalculation_private_h_
#include "rtwtypes.h"
#include "model_reference_types.h"

/* Includes for objects with custom storage classes. */
#include "../ETCSGenCodeHeader/ETCSHeader4Import.h"

/*
 * Generate compile time checks that imported macros for parameters
 * with storage class "ImportedDefine" are defined
 */
#ifndef GEAR_POS_TM_1
#error The variable for the parameter "GEAR_POS_TM_1" is not defined
#endif

#ifndef GEAR_POS_TM_2
#error The variable for the parameter "GEAR_POS_TM_2" is not defined
#endif

#ifndef GEAR_POS_TM_3
#error The variable for the parameter "GEAR_POS_TM_3" is not defined
#endif

#ifndef GEAR_POS_TM_4
#error The variable for the parameter "GEAR_POS_TM_4" is not defined
#endif

#ifndef GEAR_POS_TM_5
#error The variable for the parameter "GEAR_POS_TM_5" is not defined
#endif

#ifndef GEAR_POS_TM_PN
#error The variable for the parameter "GEAR_POS_TM_PN" is not defined
#endif

#ifndef GEAR_POS_TM_R
#error The variable for the parameter "GEAR_POS_TM_R" is not defined
#endif

#ifndef S16ETCSCpCircumferenceOfTire
#error The variable for the parameter "S16ETCSCpCircumferenceOfTire" is not defined
#endif

#ifndef S16ETCSCpEngSpd_1
#error The variable for the parameter "S16ETCSCpEngSpd_1" is not defined
#endif

#ifndef S16ETCSCpEngSpd_2
#error The variable for the parameter "S16ETCSCpEngSpd_2" is not defined
#endif

#ifndef S16ETCSCpEngSpd_3
#error The variable for the parameter "S16ETCSCpEngSpd_3" is not defined
#endif

#ifndef S16ETCSCpEngSpd_4
#error The variable for the parameter "S16ETCSCpEngSpd_4" is not defined
#endif

#ifndef S16ETCSCpEngSpd_5
#error The variable for the parameter "S16ETCSCpEngSpd_5" is not defined
#endif

#ifndef S16ETCSCpLwToSpltDctCplCnt
#error The variable for the parameter "S16ETCSCpLwToSpltDctCplCnt" is not defined
#endif

#ifndef S16ETCSCpLwToSpltDctStrtCnt
#error The variable for the parameter "S16ETCSCpLwToSpltDctStrtCnt" is not defined
#endif

#ifndef S16ETCSCpPreHighRdDctCplCnt
#error The variable for the parameter "S16ETCSCpPreHighRdDctCplCnt" is not defined
#endif

#ifndef S16ETCSCpPreHighRdDctStrtCnt
#error The variable for the parameter "S16ETCSCpPreHighRdDctStrtCnt" is not defined
#endif

#ifndef S16ETCSCpSpd_1
#error The variable for the parameter "S16ETCSCpSpd_1" is not defined
#endif

#ifndef S16ETCSCpSpd_2
#error The variable for the parameter "S16ETCSCpSpd_2" is not defined
#endif

#ifndef S16ETCSCpSpd_3
#error The variable for the parameter "S16ETCSCpSpd_3" is not defined
#endif

#ifndef S16ETCSCpSpd_4
#error The variable for the parameter "S16ETCSCpSpd_4" is not defined
#endif

#ifndef S16ETCSCpSpd_5
#error The variable for the parameter "S16ETCSCpSpd_5" is not defined
#endif

#ifndef S16ETCSCpTireRadi
#error The variable for the parameter "S16ETCSCpTireRadi" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_1
#error The variable for the parameter "S16ETCSCpTotalGearRatio_1" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_2
#error The variable for the parameter "S16ETCSCpTotalGearRatio_2" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_3
#error The variable for the parameter "S16ETCSCpTotalGearRatio_3" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_4
#error The variable for the parameter "S16ETCSCpTotalGearRatio_4" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_5
#error The variable for the parameter "S16ETCSCpTotalGearRatio_5" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_R
#error The variable for the parameter "S16ETCSCpTotalGearRatio_R" is not defined
#endif

#ifndef S16ETCSCpTrgtEngSpdStlDetd
#error The variable for the parameter "S16ETCSCpTrgtEngSpdStlDetd" is not defined
#endif

#ifndef S16ETCSCpTrnfrCsGrRtoHgMd
#error The variable for the parameter "S16ETCSCpTrnfrCsGrRtoHgMd" is not defined
#endif

#ifndef S16ETCSCpTrnfrCsGrRtoLwMd
#error The variable for the parameter "S16ETCSCpTrnfrCsGrRtoLwMd" is not defined
#endif

#ifndef S16ETCSCpTtlVehMass
#error The variable for the parameter "S16ETCSCpTtlVehMass" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecAsp_1
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecAsp_1" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecAsp_2
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecAsp_2" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecAsp_3
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecAsp_3" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecAsp_4
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecAsp_4" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecAsp_5
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecAsp_5" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecIce_1
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecIce_1" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecIce_2
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecIce_2" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecIce_3
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecIce_3" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecIce_4
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecIce_4" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecIce_5
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecIce_5" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecSnw_1
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecSnw_1" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecSnw_2
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecSnw_2" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecSnw_3
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecSnw_3" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecSnw_4
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecSnw_4" is not defined
#endif

#ifndef S8ETCSCpFnlTarSpnDecSnw_5
#error The variable for the parameter "S8ETCSCpFnlTarSpnDecSnw_5" is not defined
#endif

#ifndef S8ETCSCpTarSpnDec4Vib
#error The variable for the parameter "S8ETCSCpTarSpnDec4Vib" is not defined
#endif

#ifndef TCSDpCtrlModeDisable
#error The variable for the parameter "TCSDpCtrlModeDisable" is not defined
#endif

#ifndef TCSDpCtrlModeDrvPrtn
#error The variable for the parameter "TCSDpCtrlModeDrvPrtn" is not defined
#endif

#ifndef TCSDpCtrlModeNormal
#error The variable for the parameter "TCSDpCtrlModeNormal" is not defined
#endif

#ifndef TCSDpCtrlModeSports1
#error The variable for the parameter "TCSDpCtrlModeSports1" is not defined
#endif

#ifndef U8ETCSCpAdaptHighRdCplCnt
#error The variable for the parameter "U8ETCSCpAdaptHighRdCplCnt" is not defined
#endif

#ifndef U8ETCSCpAdaptHighRdStrtCnt
#error The variable for the parameter "U8ETCSCpAdaptHighRdStrtCnt" is not defined
#endif

#ifndef U8ETCSCpAddTarWhlSpnLwToSp
#error The variable for the parameter "U8ETCSCpAddTarWhlSpnLwToSp" is not defined
#endif

#ifndef U8ETCSCpAddTarWhlSpnSp
#error The variable for the parameter "U8ETCSCpAddTarWhlSpnSp" is not defined
#endif

#ifndef U8ETCSCpAlwAddTar4DepSnwSpd
#error The variable for the parameter "U8ETCSCpAlwAddTar4DepSnwSpd" is not defined
#endif

#ifndef U8ETCSCpAxAtAsphalt
#error The variable for the parameter "U8ETCSCpAxAtAsphalt" is not defined
#endif

#ifndef U8ETCSCpAxAtIce
#error The variable for the parameter "U8ETCSCpAxAtIce" is not defined
#endif

#ifndef U8ETCSCpAxAtSnow
#error The variable for the parameter "U8ETCSCpAxAtSnow" is not defined
#endif

#ifndef U8ETCSCpAxGear2AtAsphalt
#error The variable for the parameter "U8ETCSCpAxGear2AtAsphalt" is not defined
#endif

#ifndef U8ETCSCpAxGear2AtIce
#error The variable for the parameter "U8ETCSCpAxGear2AtIce" is not defined
#endif

#ifndef U8ETCSCpAxGear2AtSnow
#error The variable for the parameter "U8ETCSCpAxGear2AtSnow" is not defined
#endif

#ifndef U8ETCSCpAxGear3AtAsphalt
#error The variable for the parameter "U8ETCSCpAxGear3AtAsphalt" is not defined
#endif

#ifndef U8ETCSCpAxGear3AtIce
#error The variable for the parameter "U8ETCSCpAxGear3AtIce" is not defined
#endif

#ifndef U8ETCSCpAxGear3AtSnow
#error The variable for the parameter "U8ETCSCpAxGear3AtSnow" is not defined
#endif

#ifndef U8ETCSCpAxGear4AtAsphalt
#error The variable for the parameter "U8ETCSCpAxGear4AtAsphalt" is not defined
#endif

#ifndef U8ETCSCpAxGear4AtIce
#error The variable for the parameter "U8ETCSCpAxGear4AtIce" is not defined
#endif

#ifndef U8ETCSCpAxGear4AtSnow
#error The variable for the parameter "U8ETCSCpAxGear4AtSnow" is not defined
#endif

#ifndef U8ETCSCpAxGear5AtAsphalt
#error The variable for the parameter "U8ETCSCpAxGear5AtAsphalt" is not defined
#endif

#ifndef U8ETCSCpAxGear5AtIce
#error The variable for the parameter "U8ETCSCpAxGear5AtIce" is not defined
#endif

#ifndef U8ETCSCpAxGear5AtSnow
#error The variable for the parameter "U8ETCSCpAxGear5AtSnow" is not defined
#endif

#ifndef U8ETCSCpAxSenAvail
#error The variable for the parameter "U8ETCSCpAxSenAvail" is not defined
#endif

#ifndef U8ETCSCpAyAsp
#error The variable for the parameter "U8ETCSCpAyAsp" is not defined
#endif

#ifndef U8ETCSCpAyIce
#error The variable for the parameter "U8ETCSCpAyIce" is not defined
#endif

#ifndef U8ETCSCpAySnw
#error The variable for the parameter "U8ETCSCpAySnw" is not defined
#endif

#ifndef U8ETCSCpCnt4Tar1stIncInTrn
#error The variable for the parameter "U8ETCSCpCnt4Tar1stIncInTrn" is not defined
#endif

#ifndef U8ETCSCpCnt4TarMaxDecInTrn
#error The variable for the parameter "U8ETCSCpCnt4TarMaxDecInTrn" is not defined
#endif

#ifndef U8ETCSCpCrnExDctCnt
#error The variable for the parameter "U8ETCSCpCrnExDctCnt" is not defined
#endif

#ifndef U8ETCSCpDltYawNVldSpd
#error The variable for the parameter "U8ETCSCpDltYawNVldSpd" is not defined
#endif

#ifndef U8ETCSCpDltYawOffset4MinTrgtSpn
#error The variable for the parameter "U8ETCSCpDltYawOffset4MinTrgtSpn" is not defined
#endif

#ifndef U8ETCSCpDltYawStbDctCnt
#error The variable for the parameter "U8ETCSCpDltYawStbDctCnt" is not defined
#endif

#ifndef U8ETCSCpDltYawVldSpd
#error The variable for the parameter "U8ETCSCpDltYawVldSpd" is not defined
#endif

#ifndef U8ETCSCpDltYawVldVyHSpd
#error The variable for the parameter "U8ETCSCpDltYawVldVyHSpd" is not defined
#endif

#ifndef U8ETCSCpEngTrqFltGainCmdDec
#error The variable for the parameter "U8ETCSCpEngTrqFltGainCmdDec" is not defined
#endif

#ifndef U8ETCSCpEngTrqFltGainCmdInc
#error The variable for the parameter "U8ETCSCpEngTrqFltGainCmdInc" is not defined
#endif

#ifndef U8ETCSCpGrTransTme
#error The variable for the parameter "U8ETCSCpGrTransTme" is not defined
#endif

#ifndef U8ETCSCpHiAxlAccTh
#error The variable for the parameter "U8ETCSCpHiAxlAccTh" is not defined
#endif

#ifndef U8ETCSCpHillDctTime
#error The variable for the parameter "U8ETCSCpHillDctTime" is not defined
#endif

#ifndef U8ETCSCpHsaHillTime
#error The variable for the parameter "U8ETCSCpHsaHillTime" is not defined
#endif

#ifndef U8ETCSCpLdTrqByGrdTransTme
#error The variable for the parameter "U8ETCSCpLdTrqByGrdTransTme" is not defined
#endif

#ifndef U8ETCSCpLdTrqChngFac
#error The variable for the parameter "U8ETCSCpLdTrqChngFac" is not defined
#endif

#ifndef U8ETCSCpLwAxlAccTh
#error The variable for the parameter "U8ETCSCpLwAxlAccTh" is not defined
#endif

#ifndef U8ETCSCpMinTarSpn
#error The variable for the parameter "U8ETCSCpMinTarSpn" is not defined
#endif

#ifndef U8ETCSCpNAlwAddTar4DepSnwSpd
#error The variable for the parameter "U8ETCSCpNAlwAddTar4DepSnwSpd" is not defined
#endif

#ifndef U8ETCSCpSymBrkCtlTh_1
#error The variable for the parameter "U8ETCSCpSymBrkCtlTh_1" is not defined
#endif

#ifndef U8ETCSCpSymBrkCtlTh_2
#error The variable for the parameter "U8ETCSCpSymBrkCtlTh_2" is not defined
#endif

#ifndef U8ETCSCpSymBrkCtlTh_3
#error The variable for the parameter "U8ETCSCpSymBrkCtlTh_3" is not defined
#endif

#ifndef U8ETCSCpSymBrkCtlTh_4
#error The variable for the parameter "U8ETCSCpSymBrkCtlTh_4" is not defined
#endif

#ifndef U8ETCSCpSymBrkCtlTh_5
#error The variable for the parameter "U8ETCSCpSymBrkCtlTh_5" is not defined
#endif

#ifndef U8ETCSCpTarNChngDltYawHiBrkTrq
#error The variable for the parameter "U8ETCSCpTarNChngDltYawHiBrkTrq" is not defined
#endif

#ifndef U8ETCSCpTarNChngDltYawHiSpd
#error The variable for the parameter "U8ETCSCpTarNChngDltYawHiSpd" is not defined
#endif

#ifndef U8ETCSCpTarNChngDltYawLwBrkTrq
#error The variable for the parameter "U8ETCSCpTarNChngDltYawLwBrkTrq" is not defined
#endif

#ifndef U8ETCSCpTarNChngDltYawLwSpd
#error The variable for the parameter "U8ETCSCpTarNChngDltYawLwSpd" is not defined
#endif

#ifndef U8ETCSCpTarNChngDltYawVyHSpd
#error The variable for the parameter "U8ETCSCpTarNChngDltYawVyHSpd" is not defined
#endif

#ifndef U8ETCSCpTarSpnAdd4AbnmSizTire
#error The variable for the parameter "U8ETCSCpTarSpnAdd4AbnmSizTire" is not defined
#endif

#ifndef U8ETCSCpTarSpnAdd4AdHiRd
#error The variable for the parameter "U8ETCSCpTarSpnAdd4AdHiRd" is not defined
#endif

#ifndef U8ETCSCpTarSpnAdd4DepSnwHil
#error The variable for the parameter "U8ETCSCpTarSpnAdd4DepSnwHil" is not defined
#endif

#ifndef U8ETCSCpTarSpnAdd4DrvPrtnMd
#error The variable for the parameter "U8ETCSCpTarSpnAdd4DrvPrtnMd" is not defined
#endif

#ifndef U8ETCSCpTarSpnAdd4Hill
#error The variable for the parameter "U8ETCSCpTarSpnAdd4Hill" is not defined
#endif

#ifndef U8ETCSCpTarSpnAdd4PreHiRd
#error The variable for the parameter "U8ETCSCpTarSpnAdd4PreHiRd" is not defined
#endif

#ifndef U8ETCSCpTarSpnAdd4Sp
#error The variable for the parameter "U8ETCSCpTarSpnAdd4Sp" is not defined
#endif

#ifndef U8ETCSCpTarSpnTransTme
#error The variable for the parameter "U8ETCSCpTarSpnTransTme" is not defined
#endif

#ifndef U8ETCSCpTarTransCplBrkTrq2Sp
#error The variable for the parameter "U8ETCSCpTarTransCplBrkTrq2Sp" is not defined
#endif

#ifndef U8ETCSCpTarTransStBrkTrq2Sp
#error The variable for the parameter "U8ETCSCpTarTransStBrkTrq2Sp" is not defined
#endif

#ifndef U8ETCSCpTarWhlSpin_1
#error The variable for the parameter "U8ETCSCpTarWhlSpin_1" is not defined
#endif

#ifndef U8ETCSCpTarWhlSpin_2
#error The variable for the parameter "U8ETCSCpTarWhlSpin_2" is not defined
#endif

#ifndef U8ETCSCpTarWhlSpin_3
#error The variable for the parameter "U8ETCSCpTarWhlSpin_3" is not defined
#endif

#ifndef U8ETCSCpTarWhlSpin_4
#error The variable for the parameter "U8ETCSCpTarWhlSpin_4" is not defined
#endif

#ifndef U8ETCSCpTarWhlSpin_5
#error The variable for the parameter "U8ETCSCpTarWhlSpin_5" is not defined
#endif

#ifndef U8ETCSCpTransEff_1
#error The variable for the parameter "U8ETCSCpTransEff_1" is not defined
#endif

#ifndef U8ETCSCpTransEff_2
#error The variable for the parameter "U8ETCSCpTransEff_2" is not defined
#endif

#ifndef U8ETCSCpTransEff_3
#error The variable for the parameter "U8ETCSCpTransEff_3" is not defined
#endif

#ifndef U8ETCSCpTransEff_4
#error The variable for the parameter "U8ETCSCpTransEff_4" is not defined
#endif

#ifndef U8ETCSCpTransEff_5
#error The variable for the parameter "U8ETCSCpTransEff_5" is not defined
#endif

#ifndef U8ETCSCpTransEff_R
#error The variable for the parameter "U8ETCSCpTransEff_R" is not defined
#endif

#ifndef U8ETCSCpTrqDel_1
#error The variable for the parameter "U8ETCSCpTrqDel_1" is not defined
#endif

#ifndef U8ETCSCpTrqDel_2
#error The variable for the parameter "U8ETCSCpTrqDel_2" is not defined
#endif

#ifndef U8ETCSCpTrqDel_3
#error The variable for the parameter "U8ETCSCpTrqDel_3" is not defined
#endif

#ifndef U8ETCSCpTrqDel_4
#error The variable for the parameter "U8ETCSCpTrqDel_4" is not defined
#endif

#ifndef U8ETCSCpTrqDel_5
#error The variable for the parameter "U8ETCSCpTrqDel_5" is not defined
#endif

#ifndef VarFwd
#error The variable for the parameter "VarFwd" is not defined
#endif

/* Macros for accessing real-time model data structure */
#ifndef rtmGetT
# define rtmGetT()                     (*(LCETCS_vCallCalculation_TimingBrdg->taskTime[0]))
#endif
#endif                                 /* RTW_HEADER_LCETCS_vCallCalculation_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
