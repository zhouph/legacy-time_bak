/*
 * File: LCETCS_vCalIGainFacInGearChng.h
 *
 * Code generated for Simulink model 'LCETCS_vCalIGainFacInGearChng'.
 *
 * Model version                  : 1.176
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:01:33 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalIGainFacInGearChng_h_
#define RTW_HEADER_LCETCS_vCalIGainFacInGearChng_h_
#ifndef LCETCS_vCalIGainFacInGearChng_COMMON_INCLUDES_
# define LCETCS_vCalIGainFacInGearChng_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalIGainFacInGearChng_COMMON_INCLUDES_ */

#include "LCETCS_vCalIGainFacInGearChng_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_s16Inter5Point.h"
#include "LCETCS_s16Inter2Point.h"

/* Block signals for model 'LCETCS_vCalIGainFacInGearChng' */
typedef struct {
  int16_T y5;                          /* '<S2>/Igain Variation Factor In Gear Change With Negative Control Error at 1st gear' */
  int16_T y4;                          /* '<S2>/Igain Variation Factor In Gear Change With Negative Control Error at 2nd gear' */
  int16_T y3;                          /* '<S2>/Igain Variation Factor In Gear Change With Negative Control Error at 3rd gear' */
  int16_T y2;                          /* '<S2>/Igain Variation Factor In Gear Change With Negative Control Error at 4th gear' */
  int16_T y1;                          /* '<S2>/Igain Variation Factor In Gear Change With Negative Control Error at 5th gear' */
  int16_T lcetcss16IgainFacInGearChng; /* '<S2>/LCETCS_s16Inter5Point' */
  int16_T x2;                          /* '<Root>/Constant1' */
} B_LCETCS_vCalIGainFacInGearChng_c_T;

typedef struct {
  B_LCETCS_vCalIGainFacInGearChng_c_T rtb;
} MdlrefDW_LCETCS_vCalIGainFacInGearChng_T;

/* Model reference registration function */
extern void LCETCS_vCalIGainFacInGearChng_initialize(void);
extern void LCETCS_vCalIGainFacInGearChng_Init(int16_T
  *rty_lcetcss16IgainFacInGearChng);
extern void LCETCS_vCalIGainFacInGearChng_Start
  (B_LCETCS_vCalIGainFacInGearChng_c_T *localB);
extern void LCETCS_vCalIGainFacInGearChng(const TypeETCSDrvMdlStruct
  *rtu_ETCS_DRV_MDL, const TypeETCSGainGearShftStruct *rtu_ETCS_GAIN_GEAR_SHFT,
  int16_T *rty_lcetcss16IgainFacInGearChng, B_LCETCS_vCalIGainFacInGearChng_c_T *
  localB);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalIGainFacInGearChng'
 * '<S1>'   : 'LCETCS_vCalIGainFacInGearChng/ResetIgainFacInGearChang'
 * '<S2>'   : 'LCETCS_vCalIGainFacInGearChng/SetIgainFacInGearChang'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalIGainFacInGearChng_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
