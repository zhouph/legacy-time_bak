/*
 * File: LCETCS_vCallDetection.h
 *
 * Code generated for Simulink model 'LCETCS_vCallDetection'.
 *
 * Model version                  : 1.397
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:17:11 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCallDetection_h_
#define RTW_HEADER_LCETCS_vCallDetection_h_
#ifndef LCETCS_vCallDetection_COMMON_INCLUDES_
# define LCETCS_vCallDetection_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCallDetection_COMMON_INCLUDES_ */

#include "LCETCS_vCallDetection_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vDctZeroCrossingOfErr.h"
#include "LCETCS_vDctVehVib.h"
#include "LCETCS_vDctSymBrkCtlEtrExt.h"
#include "LCETCS_vDctSplitHill.h"
#include "LCETCS_vDctReqVehSpdAsit.h"
#include "LCETCS_vDctLwToSplt.h"
#include "LCETCS_vDctHighRd.h"
#include "LCETCS_vDctHiToLw.h"
#include "LCETCS_vDctGainChngInGearShft.h"
#include "LCETCS_vDctEngStall.h"
#include "LCETCS_vDctDepSnwHil.h"
#include "LCETCS_vDctCyl1st.h"
#include "LCETCS_vDctCtlEnterExitCond.h"
#include "LCETCS_vCntLowSpinTm.h"
#include "LCETCS_vCalRefTrq4IValRst.h"
#include "LCETCS_vCalIVal2Rst.h"
#include "LCETCS_vCalAftLmtCrng.h"

/* Block states (auto storage) for model 'LCETCS_vCallDetection' */
typedef struct {
  MdlrefDW_LCETCS_vDctEngStall_T LCETCS_vDctEngStall_DWORK1;/* '<Root>/LCETCS_vDctEngStall' */
  MdlrefDW_LCETCS_vDctCtlEnterExitCond_T LCETCS_vDctCtlEnterExitCond_DWORK1;/* '<Root>/LCETCS_vDctCtlEnterExitCond' */
  MdlrefDW_LCETCS_vCalAftLmtCrng_T LCETCS_vCalAftLmtCrng_DWORK1;/* '<Root>/LCETCS_vCalAftLmtCrng' */
  MdlrefDW_LCETCS_vDctReqVehSpdAsit_T LCETCS_vDctReqVehSpdAsit_DWORK1;/* '<Root>/LCETCS_vDctReqVehSpdAsit' */
  MdlrefDW_LCETCS_vCntLowSpinTm_T LCETCS_vCntLowSpinTm_DWORK1;/* '<Root>/LCETCS_vCntLowSpinTm' */
  MdlrefDW_LCETCS_vDctCyl1st_T LCETCS_vDctCyl1st_DWORK1;/* '<Root>/LCETCS_vDctCyl1st' */
  MdlrefDW_LCETCS_vDctZeroCrossingOfErr_T LCETCS_vDctZeroCrossingOfErr_DWORK1;/* '<Root>/LCETCS_vDctZeroCrossingOfErr' */
  MdlrefDW_LCETCS_vDctHiToLw_T LCETCS_vDctHiToLw_DWORK1;/* '<Root>/LCETCS_vDctHiToLw' */
  MdlrefDW_LCETCS_vDctHighRd_T LCETCS_vDctHighRd_DWORK1;/* '<Root>/LCETCS_vDctHighRd' */
  MdlrefDW_LCETCS_vCalRefTrq4IValRst_T LCETCS_vCalRefTrq4IValRst_DWORK1;/* '<Root>/LCETCS_vCalRefTrq4IValRst' */
  MdlrefDW_LCETCS_vCalIVal2Rst_T LCETCS_vCalIVal2Rst_DWORK1;/* '<Root>/LCETCS_vCalIVal2Rst' */
  MdlrefDW_LCETCS_vDctDepSnwHil_T LCETCS_vDctDepSnwHil_DWORK1;/* '<Root>/LCETCS_vDctDepSnwHil' */
  MdlrefDW_LCETCS_vDctGainChngInGearShft_T LCETCS_vDctGainChngInGearShft_DWORK1;/* '<Root>/LCETCS_vDctGainChngInGearShft' */
  MdlrefDW_LCETCS_vDctLwToSplt_T LCETCS_vDctLwToSplt_DWORK1;/* '<Root>/LCETCS_vDctLwToSplt' */
  MdlrefDW_LCETCS_vDctSplitHill_T LCETCS_vDctSplitHill_DWORK1;/* '<Root>/LCETCS_vDctSplitHill' */
  MdlrefDW_LCETCS_vDctSymBrkCtlEtrExt_T LCETCS_vDctSymBrkCtlEtrExt_DWORK1;/* '<Root>/LCETCS_vDctSymBrkCtlEtrExt' */
} DW_LCETCS_vCallDetection_f_T;

typedef struct {
  DW_LCETCS_vCallDetection_f_T rtdw;
} MdlrefDW_LCETCS_vCallDetection_T;

/* Model reference registration function */
extern void LCETCS_vCallDetection_initialize(const rtTimingBridge *timingBridge);
extern void LCETCS_vCallDetection_Init(TypeETCSCyl1stStruct *rty_ETCS_CYL_1ST,
  TypeETCSErrZroCrsStruct *rty_ETCS_ERR_ZRO_CRS, TypeETCSLowWhlSpnStruct
  *rty_ETCS_LOW_WHL_SPN, TypeETCSGainGearShftStruct *rty_ETCS_GAIN_GEAR_SHFT,
  TypeETCSH2LStruct *rty_ETCS_H2L, DW_LCETCS_vCallDetection_f_T *localDW);
extern void LCETCS_vCallDetection_Start(DW_LCETCS_vCallDetection_f_T *localDW);
extern void LCETCS_vCallDetection_Disable(DW_LCETCS_vCallDetection_f_T *localDW);
extern void LCETCS_vCallDetection(const TypeETCSCtlErrStruct *rtu_ETCS_CTL_ERR,
  const TypeETCSWhlSpinStruct *rtu_ETCS_WHL_SPIN, const TypeETCSTarSpinStruct
  *rtu_ETCS_TAR_SPIN, const TypeETCSEngStruct *rtu_ETCS_ENG_STRUCT, const
  TypeETCSCrdnTrqStruct *rtu_ETCS_CRDN_TRQ, int16_T rtu_lcetcss16VehSpd, const
  TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC, const TypeETCSGearStruct
  *rtu_ETCS_GEAR_STRUCT, const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const
  TypeETCSAxlAccStruct *rtu_ETCS_AXL_ACC, const TypeETCSAxlStruct *rtu_ETCS_FA,
  const TypeETCSAxlStruct *rtu_ETCS_RA, int16_T rtu_lcetcss16SymBrkCtlStrtTh,
  const TypeETCSBrkCtlCmdStruct *rtu_ETCS_BRK_CTL_CMD_OLD, const
  TypeETCSEngCmdStruct *rtu_ETCS_ENG_CMD_OLD, const TypeETCSCtlCmdStruct
  *rtu_ETCS_CTL_CMD_OLD, const TypeETCSExtDctStruct *rtu_ETCS_EXT_DCT, const
  TypeETCSWhlStruct *rtu_ETCS_FL, const TypeETCSWhlStruct *rtu_ETCS_FR, const
  TypeETCSWhlStruct *rtu_ETCS_RL, const TypeETCSWhlStruct *rtu_ETCS_RR, const
  TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT_OLD, const TypeETCSSplitHillStruct
  *rtu_ETCS_SPLIT_HILL_OLD, const TypeETCSCtrlModeStruct *rtu_ETCS_CTRL_MODE,
  const TypeETCSWhlAccStruct *rtu_ETCS_WHL_ACC, const TypeETCSTrq4RdFricStruct
  *rtu_ETCS_TRQ_REP_RD_FRIC_OLD, const TypeETCSEngStallStruct
  *rtu_ETCS_ENG_STALL_OLD, TypeETCSCtlActStruct *rty_ETCS_CTL_ACT,
  TypeETCSCyl1stStruct *rty_ETCS_CYL_1ST, TypeETCSErrZroCrsStruct
  *rty_ETCS_ERR_ZRO_CRS, TypeETCSIValRstStruct *rty_ETCS_IVAL_RST,
  TypeETCSRefTrqStrCtlStruct *rty_ETCS_REF_TRQ_STR_CTL,
  TypeETCSRefTrqStr2CylStruct *rty_ETCS_REF_TRQ_STR2CYL,
  TypeETCSRefTrq2JmpStruct *rty_ETCS_REF_TRQ2JMP, TypeETCSLowWhlSpnStruct
  *rty_ETCS_LOW_WHL_SPN, TypeETCSDepSnwStruct *rty_ETCS_DEP_SNW,
  TypeETCSGainGearShftStruct *rty_ETCS_GAIN_GEAR_SHFT, TypeETCSH2LStruct
  *rty_ETCS_H2L, TypeETCSBrkCtlReqStruct *rty_ETCS_BRK_CTL_REQ, boolean_T
  *rty_lcetcsu1VehVibDct, TypeETCSSplitHillStruct *rty_ETCS_SPLIT_HILL,
  TypeETCSHighDctStruct *rty_ETCS_HI_DCT, TypeETCSReqVCAStruct *rty_ETCS_REQ_VCA,
  TypeETCSLwToSpltStruct *rty_ETCS_L2SPLT, TypeETCSAftLmtCrng *rty_ETCS_AFT_TURN,
  TypeETCSEngStallStruct *rty_ETCS_ENG_STALL, DW_LCETCS_vCallDetection_f_T
  *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCallDetection'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCallDetection_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
