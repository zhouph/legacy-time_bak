/*
 * File: LCETCS_vCalDelYawTrend.c
 *
 * Code generated for Simulink model 'LCETCS_vCalDelYawTrend'.
 *
 * Model version                  : 1.215
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:52:09 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalDelYawTrend.h"
#include "LCETCS_vCalDelYawTrend_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalDelYawTrend' */
void LCETCS_vCalDelYawTrend_Init(boolean_T *rty_lcetcsu1DelYawDivrg,
  DW_LCETCS_vCalDelYawTrend_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = 0;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = false;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = 0;

  /* InitializeConditions for Chart: '<Root>/Chart1' */
  *rty_lcetcsu1DelYawDivrg = false;
}

/* Output and update for referenced model: 'LCETCS_vCalDelYawTrend' */
void LCETCS_vCalDelYawTrend(const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const
  TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN, boolean_T *rty_lcetcsu1DelYawDivrg,
  int16_T *rty_lcetcss16DelYawDivrgCnt, DW_LCETCS_vCalDelYawTrend_f_T *localDW)
{
  int32_T tmp;

  /* Chart: '<Root>/Chart1' incorporates:
   *  UnitDelay: '<Root>/Unit Delay'
   *  UnitDelay: '<Root>/Unit Delay1'
   *  UnitDelay: '<Root>/Unit Delay2'
   */
  /* Gateway: Chart1 */
  /* During: Chart1 */
  /* Entry Internal: Chart1 */
  /* Transition: '<S1>:36' */
  /*  delta yaw is gettting bigger  */
  if (rtu_ETCS_ESC_SIG->lcetcss16AbsDelYawFrst > localDW->UnitDelay_DSTATE) {
    /* Transition: '<S1>:14' */
    /* Transition: '<S1>:19' */
    tmp = localDW->UnitDelay2_DSTATE + 1;
    if (tmp > 32767) {
      tmp = 32767;
    }

    *rty_lcetcss16DelYawDivrgCnt = (int16_T)tmp;

    /* Transition: '<S1>:49' */
  } else {
    /* Transition: '<S1>:48' */
    tmp = localDW->UnitDelay2_DSTATE - 1;
    if (tmp < -32768) {
      tmp = -32768;
    }

    *rty_lcetcss16DelYawDivrgCnt = (int16_T)tmp;
  }

  /* Transition: '<S1>:15' */
  if (((*rty_lcetcss16DelYawDivrgCnt) >= 3) &&
      (rtu_ETCS_ESC_SIG->lcetcss16AbsDelYawFrst >=
       rtu_ETCS_TAR_SPIN->lcetcss16LmtCrngDltYaw)) {
    /* Transition: '<S1>:50' */
    /* Transition: '<S1>:51' */
    *rty_lcetcsu1DelYawDivrg = true;

    /* Transition: '<S1>:93' */
    /* Transition: '<S1>:94' */
  } else {
    /* Transition: '<S1>:59' */
    if ((*rty_lcetcss16DelYawDivrgCnt) <= 0) {
      /* Transition: '<S1>:57' */
      /* Transition: '<S1>:60' */
      *rty_lcetcsu1DelYawDivrg = false;

      /* Transition: '<S1>:94' */
    } else {
      /* Transition: '<S1>:64' */
      *rty_lcetcsu1DelYawDivrg = localDW->UnitDelay1_DSTATE;
    }
  }

  /* End of Chart: '<Root>/Chart1' */

  /* Saturate: '<Root>/Saturation' */
  /* Transition: '<S1>:95' */
  if ((*rty_lcetcss16DelYawDivrgCnt) > 3) {
    *rty_lcetcss16DelYawDivrgCnt = 3;
  } else {
    if ((*rty_lcetcss16DelYawDivrgCnt) < 0) {
      *rty_lcetcss16DelYawDivrgCnt = 0;
    }
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Update for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = rtu_ETCS_ESC_SIG->lcetcss16AbsDelYawFrst;

  /* Update for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = *rty_lcetcsu1DelYawDivrg;

  /* Update for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = *rty_lcetcss16DelYawDivrgCnt;
}

/* Model initialize function */
void LCETCS_vCalDelYawTrend_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
