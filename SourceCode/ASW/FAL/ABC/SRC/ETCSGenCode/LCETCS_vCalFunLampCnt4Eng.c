/*
 * File: LCETCS_vCalFunLampCnt4Eng.c
 *
 * Code generated for Simulink model 'LCETCS_vCalFunLampCnt4Eng'.
 *
 * Model version                  : 1.229
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:52:23 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalFunLampCnt4Eng.h"
#include "LCETCS_vCalFunLampCnt4Eng_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalFunLampCnt4Eng' */
void LCETCS_vCalFunLampCnt4Eng_Init(uint8_T *rty_lcetcsu8FuncLampOffEngCnt,
  DW_LCETCS_vCalFunLampCnt4Eng_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = 0U;

  /* InitializeConditions for Chart: '<Root>/Chart' */
  *rty_lcetcsu8FuncLampOffEngCnt = 0U;
}

/* Output and update for referenced model: 'LCETCS_vCalFunLampCnt4Eng' */
void LCETCS_vCalFunLampCnt4Eng(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT,
  const TypeETCSTrq4RdFricStruct *rtu_ETCS_TRQ_REP_RD_FRIC, const
  TypeETCSWhlSpinStruct *rtu_ETCS_WHL_SPIN, boolean_T rtu_lcetcsu1FuncLampOnOld,
  uint8_T *rty_lcetcsu8FuncLampOffEngCnt, DW_LCETCS_vCalFunLampCnt4Eng_f_T
  *localDW)
{
  int32_T RepRdFricTrq_Temp;

  /* Chart: '<Root>/Chart' incorporates:
   *  Product: '<Root>/Divide2'
   *  UnitDelay: '<Root>/Unit Delay1'
   */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:9' */
  /* comment */
  if (rtu_lcetcsu1FuncLampOnOld == 1) {
    /* Transition: '<S1>:112' */
    /* Transition: '<S1>:114' */
    RepRdFricTrq_Temp = ((int16_T)S16ETCSCpCdrnTrqAtAsphalt) + ((int16_T)
      S16ETCSCpOffsetTrq4FunLamp);

    /* Transition: '<S1>:117' */
  } else {
    /* Transition: '<S1>:116' */
    RepRdFricTrq_Temp = ((int16_T)S16ETCSCpCdrnTrqAtAsphalt);
  }

  /* Transition: '<S1>:144' */
  if (rtu_ETCS_CTL_ACT->lcetcsu1CtlAct == 1) {
    /* Transition: '<S1>:10' */
    /* Transition: '<S1>:108' */
    if (((rtu_ETCS_TRQ_REP_RD_FRIC->lcetcss32TrqRepRdFric / 10) >=
         RepRdFricTrq_Temp) && (rtu_ETCS_WHL_SPIN->lcetcss16WhlSpin <= ((uint8_T)
          VREF_5_KPH))) {
      /* Transition: '<S1>:86' */
      /* Transition: '<S1>:88' */
      if (localDW->UnitDelay1_DSTATE < ((uint8_T)U8ETCSCpFunLampOffCnt)) {
        /* Transition: '<S1>:90' */
        /* Transition: '<S1>:93' */
        RepRdFricTrq_Temp = localDW->UnitDelay1_DSTATE + 1;
        if (RepRdFricTrq_Temp > 255) {
          RepRdFricTrq_Temp = 255;
        }

        *rty_lcetcsu8FuncLampOffEngCnt = (uint8_T)RepRdFricTrq_Temp;

        /* Transition: '<S1>:96' */
      } else {
        /* Transition: '<S1>:97' */
        *rty_lcetcsu8FuncLampOffEngCnt = localDW->UnitDelay1_DSTATE;
      }

      /* Transition: '<S1>:104' */
      /* Transition: '<S1>:107' */
    } else {
      /* Transition: '<S1>:98' */
      if (localDW->UnitDelay1_DSTATE > 0) {
        /* Transition: '<S1>:100' */
        /* Transition: '<S1>:102' */
        RepRdFricTrq_Temp = localDW->UnitDelay1_DSTATE - 1;
        if (RepRdFricTrq_Temp < 0) {
          RepRdFricTrq_Temp = 0;
        }

        *rty_lcetcsu8FuncLampOffEngCnt = (uint8_T)RepRdFricTrq_Temp;

        /* Transition: '<S1>:107' */
      } else {
        /* Transition: '<S1>:106' */
        *rty_lcetcsu8FuncLampOffEngCnt = 0U;
      }
    }

    /* Transition: '<S1>:109' */
  } else {
    /* Transition: '<S1>:79' */
    *rty_lcetcsu8FuncLampOffEngCnt = ((uint8_T)U8ETCSCpFunLampOffCnt);
  }

  /* End of Chart: '<Root>/Chart' */

  /* Update for UnitDelay: '<Root>/Unit Delay1' */
  /* Transition: '<S1>:68' */
  localDW->UnitDelay1_DSTATE = *rty_lcetcsu8FuncLampOffEngCnt;
}

/* Model initialize function */
void LCETCS_vCalFunLampCnt4Eng_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
