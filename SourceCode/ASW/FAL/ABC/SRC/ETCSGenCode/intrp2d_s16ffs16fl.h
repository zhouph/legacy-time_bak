/*
 * File: intrp2d_s16ffs16fl.h
 *
 * Code generated for Simulink model 'LCETCS_vCalRefTrqAtStrtOfCtl'.
 *
 * Model version                  : 1.193
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:54:40 2015
 */

#ifndef SHARE_intrp2d_s16ffs16fl
#define SHARE_intrp2d_s16ffs16fl
#include "rtwtypes.h"

extern real32_T intrp2d_s16ffs16fl(const int16_T bpIndex[], const real32_T frac[],
  const int16_T table[], uint32_T stride);

#endif

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
