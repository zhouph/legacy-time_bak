/*
 * File: LCETCS_vCalCtrlTime.h
 *
 * Code generated for Simulink model 'LCETCS_vCalCtrlTime'.
 *
 * Model version                  : 1.214
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:54:14 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalCtrlTime_h_
#define RTW_HEADER_LCETCS_vCalCtrlTime_h_
#ifndef LCETCS_vCalCtrlTime_COMMON_INCLUDES_
# define LCETCS_vCalCtrlTime_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalCtrlTime_COMMON_INCLUDES_ */

#include "LCETCS_vCalCtrlTime_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Block states (auto storage) for model 'LCETCS_vCalCtrlTime' */
typedef struct {
  int16_T UnitDelay2_DSTATE;           /* '<Root>/Unit Delay2' */
} DW_LCETCS_vCalCtrlTime_f_T;

typedef struct {
  DW_LCETCS_vCalCtrlTime_f_T rtdw;
} MdlrefDW_LCETCS_vCalCtrlTime_T;

/* Model reference registration function */
extern void LCETCS_vCalCtrlTime_initialize(void);
extern void LCETCS_vCalCtrlTime_Init(DW_LCETCS_vCalCtrlTime_f_T *localDW);
extern void LCETCS_vCalCtrlTime(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT,
  int16_T *rty_lcetcss16CtlActTime, DW_LCETCS_vCalCtrlTime_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalCtrlTime'
 * '<S1>'   : 'LCETCS_vCalCtrlTime/Chart'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalCtrlTime_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
