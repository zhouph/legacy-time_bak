/*
 * File: LCETCS_vDctRecTrqActInBump.c
 *
 * Code generated for Simulink model 'LCETCS_vDctRecTrqActInBump'.
 *
 * Model version                  : 1.240
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:04:33 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctRecTrqActInBump.h"
#include "LCETCS_vDctRecTrqActInBump_private.h"

/* Initial conditions for referenced model: 'LCETCS_vDctRecTrqActInBump' */
void LCETCS_vDctRecTrqActInBump_Init(boolean_T *rty_lcetcsu1IValRecTorqAct,
  DW_LCETCS_vDctRecTrqActInBump_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = 0;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = false;

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctRisingEdge' */
  LCETCS_vDctRisingEdge_Init(rty_lcetcsu1IValRecTorqAct,
    &(localDW->LCETCS_vDctRisingEdge_DWORK1.rtdw));
}

/* Output and update for referenced model: 'LCETCS_vDctRecTrqActInBump' */
void LCETCS_vDctRecTrqActInBump(int32_T rtu_lcetcss32IValMeMryAtStrt, const
  TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const TypeETCSHighDctStruct
  *rtu_ETCS_HI_DCT, const TypeETCSCtlCmdStruct *rtu_ETCS_CTL_CMD_OLD, int16_T
  rtu_lcetcss16CtlActTime, boolean_T *rty_lcetcsu1IValRecTorqAct, int32_T
  *rty_lcetcss32IValRecTorq, DW_LCETCS_vDctRecTrqActInBump_f_T *localDW)
{
  /* local block i/o variables */
  boolean_T rtb_lcetcsu1RecTrqActInBump;
  int32_T rtb_Divide2;

  /* Product: '<Root>/Divide2' */
  rtb_Divide2 = rtu_lcetcss32IValMeMryAtStrt / 10;

  /* Chart: '<Root>/Chart' incorporates:
   *  Product: '<Root>/Divide1'
   *  UnitDelay: '<Root>/Unit Delay1'
   */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:56' */
  /* comment */
  if (rtu_ETCS_CTL_ACT->lcetcsu1CtlAct == 1) {
    /* Transition: '<S1>:91' */
    /* Transition: '<S1>:92' */
    if ((rtu_lcetcss16CtlActTime < 50) && (localDW->UnitDelay1_DSTATE == 0)) {
      /* Transition: '<S1>:74' */
      /* Transition: '<S1>:75' */
      if ((rtb_Divide2 <= ((int16_T)S16ETCSCpCdrnTrqAtSnowSpd_3)) &&
          (rtb_Divide2 > 0)) {
        /* Transition: '<S1>:14' */
        /* Transition: '<S1>:36' */
        if ((rtu_ETCS_HI_DCT->lcetcss16LwToHiCnt >= ((uint8_T)
              U8ETCSCpLwToHiDetCnt)) ||
            (rtu_ETCS_HI_DCT->lcetcss16LwToHiCntbyArea >= ((uint8_T)
              U8ETCSCpLwToHiDetAreaCnt))) {
          /* Transition: '<S1>:38' */
          /* Transition: '<S1>:42' */
          rtb_Divide2 = (((int16_T)S16ETCSCpCdrnTrqAtAspSpd_1) * ((uint8_T)
            U8ETCSCpRecTrqFacInBump)) / 100;
          if ((rtu_ETCS_CTL_CMD_OLD->lcetcss32ICtlPor / 10) < rtb_Divide2) {
            /* Transition: '<S1>:44' */
            /* Transition: '<S1>:46' */
            rtb_lcetcsu1RecTrqActInBump = true;
            localDW->UnitDelay2_DSTATE = rtb_Divide2;

            /* Transition: '<S1>:51' */
          } else {
            /* Transition: '<S1>:48' */
            rtb_lcetcsu1RecTrqActInBump = localDW->UnitDelay1_DSTATE;
          }

          /* Transition: '<S1>:52' */
        } else {
          /* Transition: '<S1>:50' */
          rtb_lcetcsu1RecTrqActInBump = localDW->UnitDelay1_DSTATE;
        }

        /* Transition: '<S1>:61' */
      } else {
        /* Transition: '<S1>:62' */
        rtb_lcetcsu1RecTrqActInBump = localDW->UnitDelay1_DSTATE;
      }

      /* Transition: '<S1>:78' */
    } else {
      /* Transition: '<S1>:77' */
      rtb_lcetcsu1RecTrqActInBump = localDW->UnitDelay1_DSTATE;
    }

    /* Transition: '<S1>:96' */
  } else {
    /* Transition: '<S1>:95' */
    rtb_lcetcsu1RecTrqActInBump = false;
    localDW->UnitDelay2_DSTATE = 0;
  }

  /* End of Chart: '<Root>/Chart' */
  /* Transition: '<S1>:97' */

  /* ModelReference: '<Root>/LCETCS_vDctRisingEdge' */
  LCETCS_vDctRisingEdge(rtb_lcetcsu1RecTrqActInBump, rty_lcetcsu1IValRecTorqAct,
                        &(localDW->LCETCS_vDctRisingEdge_DWORK1.rtdw));

  /* Switch: '<Root>/Switch' incorporates:
   *  Constant: '<Root>/Constant3'
   *  Gain: '<Root>/Gain1'
   */
  if (*rty_lcetcsu1IValRecTorqAct) {
    *rty_lcetcss32IValRecTorq = 10 * localDW->UnitDelay2_DSTATE;
  } else {
    *rty_lcetcss32IValRecTorq = 0;
  }

  /* End of Switch: '<Root>/Switch' */

  /* Update for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = rtb_lcetcsu1RecTrqActInBump;
}

/* Model initialize function */
void LCETCS_vDctRecTrqActInBump_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctRisingEdge' */
  LCETCS_vDctRisingEdge_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
