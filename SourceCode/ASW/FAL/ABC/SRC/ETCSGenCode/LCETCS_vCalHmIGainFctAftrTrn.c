/*
 * File: LCETCS_vCalHmIGainFctAftrTrn.c
 *
 * Code generated for Simulink model 'LCETCS_vCalHmIGainFctAftrTrn'.
 *
 * Model version                  : 1.184
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:01:16 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalHmIGainFctAftrTrn.h"
#include "LCETCS_vCalHmIGainFctAftrTrn_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalHmIGainFctAftrTrn' */
void LCETCS_vCalHmIGainFctAftrTrn_Init(B_LCETCS_vCalHmIGainFctAftrTrn_c_T
  *localB)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter3by5' */
  LCETCS_s16Inter3by5_Init(&localB->Out);
}

/* Start for referenced model: 'LCETCS_vCalHmIGainFctAftrTrn' */
void LCETCS_vCalHmIGainFctAftrTrn_Start(B_LCETCS_vCalHmIGainFctAftrTrn_c_T
  *localB)
{
  /* Start for Constant: '<Root>/Constant10' */
  localB->z11 = ((uint8_T)U8ETCSCpIgainFacAftrTrnIce_5);

  /* Start for Constant: '<Root>/Constant11' */
  localB->z12 = ((uint8_T)U8ETCSCpIgainFacAftrTrnSnow_5);

  /* Start for Constant: '<Root>/Constant12' */
  localB->z13 = ((uint8_T)U8ETCSCpIgainFacAftrTrnAsp_5);

  /* Start for Constant: '<Root>/Constant13' */
  localB->z33 = ((uint8_T)U8ETCSCpIgainFacAftrTrnAsp_3);

  /* Start for Constant: '<Root>/Constant14' */
  localB->z43 = ((uint8_T)U8ETCSCpIgainFacAftrTrnAsp_2);

  /* Start for Constant: '<Root>/Constant15' */
  localB->z41 = ((uint8_T)U8ETCSCpIgainFacAftrTrnIce_2);

  /* Start for Constant: '<Root>/Constant16' */
  localB->z42 = ((uint8_T)U8ETCSCpIgainFacAftrTrnSnow_2);

  /* Start for Constant: '<Root>/Constant17' */
  localB->z53 = ((uint8_T)U8ETCSCpIgainFacAftrTrnAsp_1);

  /* Start for Constant: '<Root>/Constant18' */
  localB->z51 = ((uint8_T)U8ETCSCpIgainFacAftrTrnIce_1);

  /* Start for Constant: '<Root>/Constant19' */
  localB->z52 = ((uint8_T)U8ETCSCpIgainFacAftrTrnSnow_1);

  /* Start for Constant: '<Root>/Constant2' */
  localB->z21 = ((uint8_T)U8ETCSCpIgainFacAftrTrnIce_4);

  /* Start for Constant: '<Root>/Constant3' */
  localB->z22 = ((uint8_T)U8ETCSCpIgainFacAftrTrnSnow_4);

  /* Start for Constant: '<Root>/Constant4' */
  localB->z23 = ((uint8_T)U8ETCSCpIgainFacAftrTrnAsp_4);

  /* Start for Constant: '<Root>/Constant5' */
  localB->z31 = ((uint8_T)U8ETCSCpIgainFacAftrTrnIce_3);

  /* Start for Constant: '<Root>/Constant6' */
  localB->z32 = ((uint8_T)U8ETCSCpIgainFacAftrTrnSnow_3);

  /* Start for Constant: '<Root>/Constant7' */
  localB->x1 = ((uint8_T)U8ETCSCpAyIce);

  /* Start for Constant: '<Root>/Constant8' */
  localB->x2 = ((uint8_T)U8ETCSCpAySnw);

  /* Start for Constant: '<Root>/Constant9' */
  localB->x3 = ((uint8_T)U8ETCSCpAyAsp);
}

/* Output and update for referenced model: 'LCETCS_vCalHmIGainFctAftrTrn' */
void LCETCS_vCalHmIGainFctAftrTrn(const TypeETCSAftLmtCrng *rtu_ETCS_AFT_TURN,
  const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const TypeETCSDrvMdlStruct
  *rtu_ETCS_DRV_MDL, int16_T *rty_lcetcss16HmIGainFctAftrTrn,
  B_LCETCS_vCalHmIGainFctAftrTrn_c_T *localB)
{
  /* local block i/o variables */
  int16_T rtb_x;
  int16_T rtu_ETCS_ESC_SIG_0;

  /* Constant: '<Root>/Constant10' */
  localB->z11 = ((uint8_T)U8ETCSCpIgainFacAftrTrnIce_5);

  /* Constant: '<Root>/Constant11' */
  localB->z12 = ((uint8_T)U8ETCSCpIgainFacAftrTrnSnow_5);

  /* Constant: '<Root>/Constant12' */
  localB->z13 = ((uint8_T)U8ETCSCpIgainFacAftrTrnAsp_5);

  /* Constant: '<Root>/Constant13' */
  localB->z33 = ((uint8_T)U8ETCSCpIgainFacAftrTrnAsp_3);

  /* Constant: '<Root>/Constant14' */
  localB->z43 = ((uint8_T)U8ETCSCpIgainFacAftrTrnAsp_2);

  /* Constant: '<Root>/Constant15' */
  localB->z41 = ((uint8_T)U8ETCSCpIgainFacAftrTrnIce_2);

  /* Constant: '<Root>/Constant16' */
  localB->z42 = ((uint8_T)U8ETCSCpIgainFacAftrTrnSnow_2);

  /* Constant: '<Root>/Constant17' */
  localB->z53 = ((uint8_T)U8ETCSCpIgainFacAftrTrnAsp_1);

  /* Constant: '<Root>/Constant18' */
  localB->z51 = ((uint8_T)U8ETCSCpIgainFacAftrTrnIce_1);

  /* Constant: '<Root>/Constant19' */
  localB->z52 = ((uint8_T)U8ETCSCpIgainFacAftrTrnSnow_1);

  /* Constant: '<Root>/Constant2' */
  localB->z21 = ((uint8_T)U8ETCSCpIgainFacAftrTrnIce_4);

  /* Constant: '<Root>/Constant3' */
  localB->z22 = ((uint8_T)U8ETCSCpIgainFacAftrTrnSnow_4);

  /* Constant: '<Root>/Constant4' */
  localB->z23 = ((uint8_T)U8ETCSCpIgainFacAftrTrnAsp_4);

  /* Constant: '<Root>/Constant5' */
  localB->z31 = ((uint8_T)U8ETCSCpIgainFacAftrTrnIce_3);

  /* Constant: '<Root>/Constant6' */
  localB->z32 = ((uint8_T)U8ETCSCpIgainFacAftrTrnSnow_3);

  /* Constant: '<Root>/Constant7' */
  localB->x1 = ((uint8_T)U8ETCSCpAyIce);

  /* Constant: '<Root>/Constant8' */
  localB->x2 = ((uint8_T)U8ETCSCpAySnw);

  /* Constant: '<Root>/Constant9' */
  localB->x3 = ((uint8_T)U8ETCSCpAyAsp);

  /* Abs: '<Root>/Abs1' */
  if (rtu_ETCS_ESC_SIG->lcetcss16LatAccMsrd < 0) {
    rtu_ETCS_ESC_SIG_0 = (int16_T)(-rtu_ETCS_ESC_SIG->lcetcss16LatAccMsrd);
  } else {
    rtu_ETCS_ESC_SIG_0 = rtu_ETCS_ESC_SIG->lcetcss16LatAccMsrd;
  }

  /* Sum: '<Root>/Add' incorporates:
   *  Abs: '<Root>/Abs1'
   */
  rtb_x = (int16_T)(rtu_ETCS_ESC_SIG->lcetcss16LatAccMsrdMani -
                    rtu_ETCS_ESC_SIG_0);

  /* Saturate: '<Root>/Saturation1' */
  if (rtb_x > 100) {
    /* Sum: '<Root>/Add' */
    rtb_x = 100;
  } else {
    if (rtb_x < 0) {
      /* Sum: '<Root>/Add' */
      rtb_x = 0;
    }
  }

  /* End of Saturate: '<Root>/Saturation1' */

  /* ModelReference: '<Root>/LCETCS_s16Inter3by5' */
  LCETCS_s16Inter3by5(rtb_x, localB->x1, localB->x2, localB->x3,
                      rtu_ETCS_DRV_MDL->lcetcss16TotalGearRatio, ((int16_T)
    S16ETCSCpTotalGearRatio_5), ((int16_T)S16ETCSCpTotalGearRatio_4), ((int16_T)
    S16ETCSCpTotalGearRatio_3), ((int16_T)S16ETCSCpTotalGearRatio_2), ((int16_T)
    S16ETCSCpTotalGearRatio_1), localB->z11, localB->z12, localB->z13,
                      localB->z21, localB->z22, localB->z23, localB->z31,
                      localB->z32, localB->z33, localB->z41, localB->z42,
                      localB->z43, localB->z51, localB->z52, localB->z53,
                      &localB->Out);

  /* Switch: '<Root>/Switch1' incorporates:
   *  Constant: '<Root>/Constant'
   *  Gain: '<Root>/Gain16'
   */
  if (rtu_ETCS_AFT_TURN->lcetcsu1DctAftLmtCrng) {
    *rty_lcetcss16HmIGainFctAftrTrn = (int16_T)(10 * localB->Out);
  } else {
    *rty_lcetcss16HmIGainFctAftrTrn = 100;
  }

  /* End of Switch: '<Root>/Switch1' */

  /* Saturate: '<Root>/Saturation2' */
  if ((*rty_lcetcss16HmIGainFctAftrTrn) > 1000) {
    *rty_lcetcss16HmIGainFctAftrTrn = 1000;
  } else {
    if ((*rty_lcetcss16HmIGainFctAftrTrn) < 100) {
      *rty_lcetcss16HmIGainFctAftrTrn = 100;
    }
  }

  /* End of Saturate: '<Root>/Saturation2' */
}

/* Model initialize function */
void LCETCS_vCalHmIGainFctAftrTrn_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter3by5' */
  LCETCS_s16Inter3by5_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
