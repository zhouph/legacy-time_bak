/*
 * File: LCETCS_vCallControl.c
 *
 * Code generated for Simulink model 'LCETCS_vCallControl'.
 *
 * Model version                  : 1.260
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:16:07 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCallControl.h"
#include "LCETCS_vCallControl_private.h"

const TypeTMCReqGearStruct LCETCS_vCallControl_rtZTypeTMCReqGearStruct = {
  0U,                                  /* lctmcu8_BSTGRReqType */
  0U                                   /* lctmcu8_BSTGRReqGear */
} ;                                    /* TypeTMCReqGearStruct ground */

/* Initial conditions for referenced model: 'LCETCS_vCallControl' */
void LCETCS_vCallControl_Init(TypeTMCReqGearStruct *rty_TMC_REQ_GEAR_STRUCT,
  boolean_T *rty_lcetcsu1FuncLampOn, DW_LCETCS_vCallControl_f_T *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalTrqRepRdFric' */
  LCETCS_vCalTrqRepRdFric_Init(&(localDW->LCETCS_vCalTrqRepRdFric_DWORK1.rtb),
                               &(localDW->LCETCS_vCalTrqRepRdFric_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalCtlGain' */
  LCETCS_vCalCtlGain_Init(&(localDW->LCETCS_vCalCtlGain_DWORK1.rtb),
    &(localDW->LCETCS_vCalCtlGain_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalCardanTrqCmd' */
  LCETCS_vCalCardanTrqCmd_Init(&(localDW->LCETCS_vCalCardanTrqCmd_DWORK1.rtb),
                               &(localDW->LCETCS_vCalCardanTrqCmd_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalFunLamp' */
  LCETCS_vCalFunLamp_Init(rty_lcetcsu1FuncLampOn,
    &(localDW->LCETCS_vCalFunLamp_DWORK1.rtb),
    &(localDW->LCETCS_vCalFunLamp_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalMaxLmtSymBrkTrq' */
  LCETCS_vCalMaxLmtSymBrkTrq_Init
    (&(localDW->LCETCS_vCalMaxLmtSymBrkTrq_DWORK1.rtb));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalSymBrkCtlTrqCmd' */
  LCETCS_vCalSymBrkCtlTrqCmd_Init
    (&(localDW->LCETCS_vCalSymBrkCtlTrqCmd_DWORK1.rtdw));

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = LCETCS_vCallControl_rtZTypeTMCReqGearStruct;

  /* InitializeConditions for ModelReference: '<Root>/LCTMC_vCallControl' */
  LCTMC_vCallControl_Init(rty_TMC_REQ_GEAR_STRUCT,
    &(localDW->LCTMC_vCallControl_DWORK1.rtb),
    &(localDW->LCTMC_vCallControl_DWORK1.rtdw));
}

/* Start for referenced model: 'LCETCS_vCallControl' */
void LCETCS_vCallControl_Start(DW_LCETCS_vCallControl_f_T *localDW)
{
  /* Start for ModelReference: '<Root>/LCETCS_vCalTrqRepRdFric' */
  LCETCS_vCalTrqRepRdFric_Start(&(localDW->LCETCS_vCalTrqRepRdFric_DWORK1.rtdw));

  /* Start for ModelReference: '<Root>/LCETCS_vCalCtlGain' */
  LCETCS_vCalCtlGain_Start(&(localDW->LCETCS_vCalCtlGain_DWORK1.rtdw));

  /* Start for ModelReference: '<Root>/LCETCS_vCalMaxLmtSymBrkTrq' */
  LCETCS_vCalMaxLmtSymBrkTrq_Start
    (&(localDW->LCETCS_vCalMaxLmtSymBrkTrq_DWORK1.rtb));

  /* Start for ModelReference: '<Root>/LCTMC_vCallControl' */
  LCTMC_vCallControl_Start(&(localDW->LCTMC_vCallControl_DWORK1.rtdw));
}

/* Output and update for referenced model: 'LCETCS_vCallControl' */
void LCETCS_vCallControl(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const
  TypeETCSCtlErrStruct *rtu_ETCS_CTL_ERR, const TypeETCSDrvMdlStruct
  *rtu_ETCS_DRV_MDL, const TypeETCSIValRstStruct *rtu_ETCS_IVAL_RST, const
  TypeETCSCyl1stStruct *rtu_ETCS_CYL_1ST, const TypeETCSVehAccStruct
  *rtu_ETCS_VEH_ACC, const TypeETCSLowWhlSpnStruct *rtu_ETCS_LOW_WHL_SPN, const
  TypeETCSGainGearShftStruct *rtu_ETCS_GAIN_GEAR_SHFT, const TypeETCSLdTrqStruct
  *rtu_ETCS_LD_TRQ, const TypeETCSExtDctStruct *rtu_ETCS_EXT_DCT, const
  TypeETCSEngStruct *rtu_ETCS_ENG_STRUCT, const TypeETCSH2LStruct *rtu_ETCS_H2L,
  const TypeETCSBrkCtlReqStruct *rtu_ETCS_BRK_CTL_REQ, const
  TypeETCSAxlAccStruct *rtu_ETCS_AXL_ACC, const TypeETCSAxlStruct *rtu_ETCS_FA,
  const TypeETCSAxlStruct *rtu_ETCS_RA, const TypeETCSCrdnTrqStruct
  *rtu_ETCS_CRDN_TRQ, const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const
  TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN, int16_T rtu_lcetcss16VehSpd, const
  TypeETCSGearStruct *rtu_ETCS_GEAR_STRUCT, boolean_T rtu_lcetcsu1VehVibDct,
  const TypeETCSSplitHillStruct *rtu_ETCS_SPLIT_HILL, const
  TypeETCSWhlSpinStruct *rtu_ETCS_WHL_SPIN, const TypeETCSCirStruct *rtu_ETCS_PC,
  const TypeETCSCirStruct *rtu_ETCS_SC, const TypeETCSAftLmtCrng
  *rtu_ETCS_AFT_TURN, const TypeETCSCtlCmdStruct *rtu_ETCS_CTL_CMD_OLD,
  TypeETCSGainStruct *rty_ETCS_CTL_GAINS, TypeETCSTrq4RdFricStruct
  *rty_ETCS_TRQ_REP_RD_FRIC, TypeTMCReqGearStruct *rty_TMC_REQ_GEAR_STRUCT,
  TypeETCSBrkCtlCmdStruct *rty_ETCS_BRK_CTL_CMD, boolean_T
  *rty_lcetcsu1FuncLampOn, TypeETCSCtlCmdStruct *rty_ETCS_CTL_CMD,
  DW_LCETCS_vCallControl_f_T *localDW)
{
  /* local block i/o variables */
  TypeTMCReqGearStruct rtb_TMC_REQ_GEAR_STRUCT_OLD;
  int16_T rtb_lcetcss16MaxLmtSymBrkTrq;

  /* ModelReference: '<Root>/LCETCS_vCalTrqRepRdFric' */
  LCETCS_vCalTrqRepRdFric(rtu_ETCS_CTL_CMD_OLD, rtu_ETCS_CYL_1ST,
    rtu_ETCS_VEH_ACC, rtu_ETCS_LOW_WHL_SPN, rtu_ETCS_H2L,
    rty_ETCS_TRQ_REP_RD_FRIC, &(localDW->LCETCS_vCalTrqRepRdFric_DWORK1.rtb),
    &(localDW->LCETCS_vCalTrqRepRdFric_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalCtlGain' */
  LCETCS_vCalCtlGain(rtu_ETCS_CTL_ACT, rtu_ETCS_DRV_MDL, rtu_ETCS_CTL_ERR,
                     rtu_ETCS_GAIN_GEAR_SHFT, rtu_ETCS_H2L, rtu_ETCS_FA,
                     rtu_ETCS_RA, rtu_ETCS_TAR_SPIN, rtu_ETCS_ESC_SIG,
                     rtu_lcetcsu1VehVibDct, rtu_ETCS_CTL_CMD_OLD,
                     rtu_lcetcss16VehSpd, rtu_ETCS_AFT_TURN,
                     rty_ETCS_TRQ_REP_RD_FRIC, rty_ETCS_CTL_GAINS,
                     &(localDW->LCETCS_vCalCtlGain_DWORK1.rtb),
                     &(localDW->LCETCS_vCalCtlGain_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalCardanTrqCmd' */
  LCETCS_vCalCardanTrqCmd(rtu_ETCS_LD_TRQ, rtu_ETCS_CTL_ERR, rtu_ETCS_IVAL_RST,
    rtu_ETCS_CTL_ACT, rtu_ETCS_CTL_CMD_OLD, rtu_ETCS_CRDN_TRQ, rtu_ETCS_ESC_SIG,
    rtu_ETCS_DRV_MDL, rtu_ETCS_H2L, rtu_ETCS_TAR_SPIN, rty_ETCS_CTL_GAINS,
    rty_ETCS_CTL_CMD, &(localDW->LCETCS_vCalCardanTrqCmd_DWORK1.rtb),
    &(localDW->LCETCS_vCalCardanTrqCmd_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalFunLamp' */
  LCETCS_vCalFunLamp(rtu_ETCS_CTL_ACT, rty_ETCS_TRQ_REP_RD_FRIC,
                     rtu_ETCS_WHL_SPIN, rtu_ETCS_PC, rtu_ETCS_SC,
                     rtu_ETCS_EXT_DCT, rty_lcetcsu1FuncLampOn,
                     &(localDW->LCETCS_vCalFunLamp_DWORK1.rtb),
                     &(localDW->LCETCS_vCalFunLamp_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalMaxLmtSymBrkTrq' */
  LCETCS_vCalMaxLmtSymBrkTrq(rtu_ETCS_DRV_MDL, &rtb_lcetcss16MaxLmtSymBrkTrq,
    &(localDW->LCETCS_vCalMaxLmtSymBrkTrq_DWORK1.rtb));

  /* ModelReference: '<Root>/LCETCS_vCalSymBrkCtlTrqCmd' */
  LCETCS_vCalSymBrkCtlTrqCmd(rtu_ETCS_BRK_CTL_REQ, rtu_ETCS_CTL_ACT,
    rtu_ETCS_AXL_ACC, rtb_lcetcss16MaxLmtSymBrkTrq, rty_ETCS_BRK_CTL_CMD,
    &(localDW->LCETCS_vCalSymBrkCtlTrqCmd_DWORK1.rtdw));

  /* UnitDelay: '<Root>/Unit Delay1' */
  rtb_TMC_REQ_GEAR_STRUCT_OLD = localDW->UnitDelay1_DSTATE;

  /* ModelReference: '<Root>/LCTMC_vCallControl' */
  LCTMC_vCallControl(rty_ETCS_CTL_GAINS, rtu_ETCS_GEAR_STRUCT, rtu_ETCS_EXT_DCT,
                     rtu_ETCS_ENG_STRUCT, rtu_lcetcss16VehSpd, rtu_ETCS_CTL_ACT,
                     rtu_ETCS_SPLIT_HILL, rtCP_InhibitModeTemp_Value,
                     &rtb_TMC_REQ_GEAR_STRUCT_OLD, rty_TMC_REQ_GEAR_STRUCT,
                     &(localDW->LCTMC_vCallControl_DWORK1.rtb),
                     &(localDW->LCTMC_vCallControl_DWORK1.rtdw));

  /* Update for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = *rty_TMC_REQ_GEAR_STRUCT;
}

/* Model initialize function */
void LCETCS_vCallControl_initialize(const rtTimingBridge *timingBridge)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalCardanTrqCmd' */
  LCETCS_vCalCardanTrqCmd_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalCtlGain' */
  LCETCS_vCalCtlGain_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalFunLamp' */
  LCETCS_vCalFunLamp_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalMaxLmtSymBrkTrq' */
  LCETCS_vCalMaxLmtSymBrkTrq_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalSymBrkCtlTrqCmd' */
  LCETCS_vCalSymBrkCtlTrqCmd_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalTrqRepRdFric' */
  LCETCS_vCalTrqRepRdFric_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCTMC_vCallControl' */
  LCTMC_vCallControl_initialize(timingBridge);
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
