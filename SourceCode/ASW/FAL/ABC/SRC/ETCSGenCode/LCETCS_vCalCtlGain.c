/*
 * File: LCETCS_vCalCtlGain.c
 *
 * Code generated for Simulink model 'LCETCS_vCalCtlGain'.
 *
 * Model version                  : 1.230
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:12:14 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalCtlGain.h"
#include "LCETCS_vCalCtlGain_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalCtlGain' */
void LCETCS_vCalCtlGain_Init(B_LCETCS_vCalCtlGain_c_T *localB,
  DW_LCETCS_vCalCtlGain_f_T *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalPCtlGain' */
  LCETCS_vCalPCtlGain_Init(&localB->lcetcss16BsPgain,
    &localB->lcetcss16PgainFacInGearChng, &localB->lcetcss16PGainFacInDrvVib,
    &localB->lcetcss16PgainFac, &(localDW->LCETCS_vCalPCtlGain_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalICtlGain' */
  LCETCS_vCalICtlGain_Init(&localB->lcetcss16BsIgain,
    &localB->lcetcss16IgainFacInGearChng, &localB->lcetcss16IgainFac,
    &(localDW->LCETCS_vCalICtlGain_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalYawCtlGain' */
  LCETCS_vCalYawCtlGain_Init(&localB->lcetcsu1DelYawDivrg,
    &(localDW->LCETCS_vCalYawCtlGain_DWORK1.rtdw));
}

/* Start for referenced model: 'LCETCS_vCalCtlGain' */
void LCETCS_vCalCtlGain_Start(DW_LCETCS_vCalCtlGain_f_T *localDW)
{
  /* Start for ModelReference: '<Root>/LCETCS_vCalPCtlGain' */
  LCETCS_vCalPCtlGain_Start(&(localDW->LCETCS_vCalPCtlGain_DWORK1.rtdw));

  /* Start for ModelReference: '<Root>/LCETCS_vCalICtlGain' */
  LCETCS_vCalICtlGain_Start(&(localDW->LCETCS_vCalICtlGain_DWORK1.rtdw));

  /* Start for ModelReference: '<Root>/LCETCS_vCalYawCtlGain' */
  LCETCS_vCalYawCtlGain_Start(&(localDW->LCETCS_vCalYawCtlGain_DWORK1.rtdw));
}

/* Output and update for referenced model: 'LCETCS_vCalCtlGain' */
void LCETCS_vCalCtlGain(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const
  TypeETCSDrvMdlStruct *rtu_ETCS_DRV_MDL, const TypeETCSCtlErrStruct
  *rtu_ETCS_CTL_ERR, const TypeETCSGainGearShftStruct *rtu_ETCS_GAIN_GEAR_SHFT,
  const TypeETCSH2LStruct *rtu_ETCS_H2L, const TypeETCSAxlStruct *rtu_ETCS_FA,
  const TypeETCSAxlStruct *rtu_ETCS_RA, const TypeETCSTarSpinStruct
  *rtu_ETCS_TAR_SPIN, const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, boolean_T
  rtu_lcetcsu1VehVibDct, const TypeETCSCtlCmdStruct *rtu_ETCS_CTL_CMD_OLD,
  int16_T rtu_lcetcss16VehSpd, const TypeETCSAftLmtCrng *rtu_ETCS_AFT_TURN,
  const TypeETCSTrq4RdFricStruct *rtu_ETCS_TRQ_REP_RD_FRIC, TypeETCSGainStruct
  *rty_ETCS_CTL_GAINS, B_LCETCS_vCalCtlGain_c_T *localB,
  DW_LCETCS_vCalCtlGain_f_T *localDW)
{
  /* local block i/o variables */
  int16_T rtb_lcetcss16Pgain;
  int16_T rtb_lcetcss16PGainIncFacH2L;
  int16_T rtb_lcetcss16PGainFacInTrn;
  int16_T rtb_lcetcss16Igain;
  int16_T rtb_lcetcss16IGainIncFacH2L;
  int16_T rtb_lcetcss16IGainFacInTrn;
  int16_T rtb_lcetcss16HmIGainFctAftrTrn;
  int16_T rtb_lcetcss16HmIGainFct4BigSpn;
  int16_T rtb_lcetcss16DelYawGain;
  int16_T rtb_lcetcss16DelYawDivrgCnt;
  int16_T rtb_lcetcss16DelYawDivrgGain;
  int16_T rtb_lcetcss16DelYawCnvrgGain;
  int8_T rtb_lcetcss8PgainFacInDrvVibCnt;

  /* ModelReference: '<Root>/LCETCS_vCalPCtlGain' */
  LCETCS_vCalPCtlGain(rtu_ETCS_CTL_ACT, rtu_ETCS_DRV_MDL, rtu_ETCS_CTL_ERR,
                      rtu_ETCS_TRQ_REP_RD_FRIC, rtu_ETCS_GAIN_GEAR_SHFT,
                      rtu_ETCS_FA, rtu_ETCS_RA, rtu_ETCS_TAR_SPIN,
                      rtu_ETCS_ESC_SIG, rtu_ETCS_H2L, rtu_lcetcsu1VehVibDct,
                      &rtb_lcetcss16Pgain, &localB->lcetcss16BsPgain,
                      &localB->lcetcss16PgainFacInGearChng,
                      &rtb_lcetcss16PGainIncFacH2L,
                      &localB->lcetcss16HomoBsPgain,
                      &localB->lcetcss16SpltBsPgain, &rtb_lcetcss16PGainFacInTrn,
                      &localB->lcetcss16PGainFacInDrvVib,
                      &rtb_lcetcss8PgainFacInDrvVibCnt,
                      &localB->lcetcss16PgainFac,
                      &(localDW->LCETCS_vCalPCtlGain_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalICtlGain' */
  LCETCS_vCalICtlGain(rtu_ETCS_AFT_TURN, rtu_ETCS_CTL_ACT, rtu_ETCS_DRV_MDL,
                      rtu_ETCS_CTL_ERR, rtu_ETCS_TRQ_REP_RD_FRIC,
                      rtu_ETCS_GAIN_GEAR_SHFT, rtu_ETCS_FA, rtu_ETCS_RA,
                      rtu_ETCS_TAR_SPIN, rtu_ETCS_ESC_SIG, rtu_ETCS_H2L,
                      &rtb_lcetcss16Igain, &localB->lcetcss16BsIgain,
                      &localB->lcetcss16IgainFacInGearChng,
                      &rtb_lcetcss16IGainIncFacH2L,
                      &localB->lcetcss16HomoBsIgain,
                      &localB->lcetcss16SpltBsIgain, &rtb_lcetcss16IGainFacInTrn,
                      &localB->lcetcss16IgainFac,
                      &rtb_lcetcss16HmIGainFctAftrTrn,
                      &rtb_lcetcss16HmIGainFct4BigSpn,
                      &(localDW->LCETCS_vCalICtlGain_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalYawCtlGain' */
  LCETCS_vCalYawCtlGain(rtu_ETCS_CTL_CMD_OLD, rtu_lcetcss16VehSpd,
                        rtu_ETCS_ESC_SIG, rtu_ETCS_TAR_SPIN,
                        &rtb_lcetcss16DelYawGain, &localB->lcetcsu1DelYawDivrg,
                        &rtb_lcetcss16DelYawDivrgCnt,
                        &rtb_lcetcss16DelYawDivrgGain,
                        &rtb_lcetcss16DelYawCnvrgGain,
                        &(localDW->LCETCS_vCalYawCtlGain_DWORK1.rtdw));

  /* BusCreator: '<Root>/Bus Creator' */
  rty_ETCS_CTL_GAINS->lcetcss16Pgain = rtb_lcetcss16Pgain;
  rty_ETCS_CTL_GAINS->lcetcss16Igain = rtb_lcetcss16Igain;
  rty_ETCS_CTL_GAINS->lcetcss16BsPgain = localB->lcetcss16BsPgain;
  rty_ETCS_CTL_GAINS->lcetcss16BsIgain = localB->lcetcss16BsIgain;
  rty_ETCS_CTL_GAINS->lcetcss16PgainFacInGearChng =
    localB->lcetcss16PgainFacInGearChng;
  rty_ETCS_CTL_GAINS->lcetcss16IgainFacInGearChng =
    localB->lcetcss16IgainFacInGearChng;
  rty_ETCS_CTL_GAINS->lcetcss16PGainIncFacH2L = rtb_lcetcss16PGainIncFacH2L;
  rty_ETCS_CTL_GAINS->lcetcss16IGainIncFacH2L = rtb_lcetcss16IGainIncFacH2L;
  rty_ETCS_CTL_GAINS->lcetcss16HomoBsPgain = localB->lcetcss16HomoBsPgain;
  rty_ETCS_CTL_GAINS->lcetcss16SpltBsPgain = localB->lcetcss16SpltBsPgain;
  rty_ETCS_CTL_GAINS->lcetcss16HomoBsIgain = localB->lcetcss16HomoBsIgain;
  rty_ETCS_CTL_GAINS->lcetcss16SpltBsIgain = localB->lcetcss16SpltBsIgain;
  rty_ETCS_CTL_GAINS->lcetcss16PGainFacInTrn = rtb_lcetcss16PGainFacInTrn;
  rty_ETCS_CTL_GAINS->lcetcss16IGainFacInTrn = rtb_lcetcss16IGainFacInTrn;
  rty_ETCS_CTL_GAINS->lcetcss16PGainFacInDrvVib =
    localB->lcetcss16PGainFacInDrvVib;
  rty_ETCS_CTL_GAINS->lcetcss8PgainFacInDrvVibCnt =
    rtb_lcetcss8PgainFacInDrvVibCnt;
  rty_ETCS_CTL_GAINS->lcetcss16PgainFac = localB->lcetcss16PgainFac;
  rty_ETCS_CTL_GAINS->lcetcss16IgainFac = localB->lcetcss16IgainFac;
  rty_ETCS_CTL_GAINS->lcetcss16HmIGainFctAftrTrn =
    rtb_lcetcss16HmIGainFctAftrTrn;
  rty_ETCS_CTL_GAINS->lcetcss16HmIGainFct4BigSpn =
    rtb_lcetcss16HmIGainFct4BigSpn;
  rty_ETCS_CTL_GAINS->lcetcss16DelYawGain = rtb_lcetcss16DelYawGain;
  rty_ETCS_CTL_GAINS->lcetcss16DelYawDivrgCnt = rtb_lcetcss16DelYawDivrgCnt;
  rty_ETCS_CTL_GAINS->lcetcsu1DelYawDivrg = localB->lcetcsu1DelYawDivrg;
  rty_ETCS_CTL_GAINS->lcetcss16DelYawDivrgGain = rtb_lcetcss16DelYawDivrgGain;
  rty_ETCS_CTL_GAINS->lcetcss16DelYawCnvrgGain = rtb_lcetcss16DelYawCnvrgGain;
}

/* Model initialize function */
void LCETCS_vCalCtlGain_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalICtlGain' */
  LCETCS_vCalICtlGain_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalPCtlGain' */
  LCETCS_vCalPCtlGain_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalYawCtlGain' */
  LCETCS_vCalYawCtlGain_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
