/*
 * File: LCETCS_vCalFinAbnrmTire.c
 *
 * Code generated for Simulink model 'LCETCS_vCalFinAbnrmTire'.
 *
 * Model version                  : 1.131
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:49:52 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalFinAbnrmTire.h"
#include "LCETCS_vCalFinAbnrmTire_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalFinAbnrmTire' */
void LCETCS_vCalFinAbnrmTire_Init(boolean_T *rty_lcetcsu1AbnrmSizeTire)
{
  /* InitializeConditions for Chart: '<Root>/Chart' */
  *rty_lcetcsu1AbnrmSizeTire = false;
}

/* Output and update for referenced model: 'LCETCS_vCalFinAbnrmTire' */
void LCETCS_vCalFinAbnrmTire(boolean_T rtu_lcetcsu1AbnrmSizeTireDet, boolean_T
  rtu_lcetcsu1AbnrmSizeTireSus, boolean_T *rty_lcetcsu1AbnrmSizeTire)
{
  /* Chart: '<Root>/Chart' */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:186' */
  if ((rtu_lcetcsu1AbnrmSizeTireDet == 1) || (rtu_lcetcsu1AbnrmSizeTireSus == 1))
  {
    /* Transition: '<S1>:188' */
    /* Transition: '<S1>:190' */
    *rty_lcetcsu1AbnrmSizeTire = true;

    /* Transition: '<S1>:193' */
  } else {
    /* Transition: '<S1>:192' */
    *rty_lcetcsu1AbnrmSizeTire = false;
  }

  /* End of Chart: '<Root>/Chart' */
  /* Transition: '<S1>:195' */
}

/* Model initialize function */
void LCETCS_vCalFinAbnrmTire_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
