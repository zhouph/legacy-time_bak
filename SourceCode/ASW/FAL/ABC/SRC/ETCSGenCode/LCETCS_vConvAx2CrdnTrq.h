/*
 * File: LCETCS_vConvAx2CrdnTrq.h
 *
 * Code generated for Simulink model 'LCETCS_vConvAx2CrdnTrq'.
 *
 * Model version                  : 1.491
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:03:25 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vConvAx2CrdnTrq_h_
#define RTW_HEADER_LCETCS_vConvAx2CrdnTrq_h_
#ifndef LCETCS_vConvAx2CrdnTrq_COMMON_INCLUDES_
# define LCETCS_vConvAx2CrdnTrq_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vConvAx2CrdnTrq_COMMON_INCLUDES_ */

#include "LCETCS_vConvAx2CrdnTrq_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_s16Inter3Point.h"

/* Block signals for model 'LCETCS_vConvAx2CrdnTrq' */
typedef struct {
  int16_T x1;                          /* '<Root>/Constant' */
  int16_T x2;                          /* '<Root>/Constant2' */
  int16_T x3;                          /* '<Root>/Constant3' */
  int16_T y;                           /* '<Root>/LCETCS_s16Inter3Point' */
} B_LCETCS_vConvAx2CrdnTrq_c_T;

typedef struct {
  B_LCETCS_vConvAx2CrdnTrq_c_T rtb;
} MdlrefDW_LCETCS_vConvAx2CrdnTrq_T;

/* Model reference registration function */
extern void LCETCS_vConvAx2CrdnTrq_initialize(void);
extern void LCETCS_vConvAx2CrdnTrq_Init(B_LCETCS_vConvAx2CrdnTrq_c_T *localB);
extern void LCETCS_vConvAx2CrdnTrq_Start(B_LCETCS_vConvAx2CrdnTrq_c_T *localB);
extern void LCETCS_vConvAx2CrdnTrq(int16_T rtu_VehAcc, int32_T *rty_CrdnTrq,
  B_LCETCS_vConvAx2CrdnTrq_c_T *localB);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vConvAx2CrdnTrq'
 */
#endif                                 /* RTW_HEADER_LCETCS_vConvAx2CrdnTrq_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
