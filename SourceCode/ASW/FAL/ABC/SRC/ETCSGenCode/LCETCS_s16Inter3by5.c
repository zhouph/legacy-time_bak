/*
 * File: LCETCS_s16Inter3by5.c
 *
 * Code generated for Simulink model 'LCETCS_s16Inter3by5'.
 *
 * Model version                  : 1.258
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:48:33 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_s16Inter3by5.h"
#include "LCETCS_s16Inter3by5_private.h"

/* Initial conditions for referenced model: 'LCETCS_s16Inter3by5' */
void LCETCS_s16Inter3by5_Init(int16_T *rty_Out)
{
  /* InitializeConditions for Chart: '<Root>/Chart3' */
  *rty_Out = 0;
}

/* Output and update for referenced model: 'LCETCS_s16Inter3by5' */
void LCETCS_s16Inter3by5(int16_T rtu_x, int16_T rtu_x1, int16_T rtu_x2, int16_T
  rtu_x3, int16_T rtu_y, int16_T rtu_y1, int16_T rtu_y2, int16_T rtu_y3, int16_T
  rtu_y4, int16_T rtu_y5, int16_T rtu_z11, int16_T rtu_z12, int16_T rtu_z13,
  int16_T rtu_z21, int16_T rtu_z22, int16_T rtu_z23, int16_T rtu_z31, int16_T
  rtu_z32, int16_T rtu_z33, int16_T rtu_z41, int16_T rtu_z42, int16_T rtu_z43,
  int16_T rtu_z51, int16_T rtu_z52, int16_T rtu_z53, int16_T *rty_Out)
{
  int16_T z_1_index;
  int16_T z_1;
  int16_T rtb_TmpSignalConversionAtSFunctionInport5[15];
  int16_T rtb_x_index;
  int32_T rtb_x_frac;
  int16_T rtb_y_index;
  int32_T rtb_y_frac;

  /* Chart: '<Root>/Chart1' incorporates:
   *  SignalConversion: '<S1>/TmpSignal ConversionAt SFunction Inport2'
   *  SignalConversion: '<S1>/TmpSignal ConversionAt SFunction Inport4'
   */
  /* Gateway: Chart1 */
  /* During: Chart1 */
  /* Entry Internal: Chart1 */
  /* Transition: '<S1>:12' */
  /* comment */
  if (rtu_x < rtu_x1) {
    /* Transition: '<S1>:14' */
    /* Transition: '<S1>:19' */
    rtb_x_index = 0;
    rtb_x_frac = 0;

    /* Transition: '<S1>:83' */
    /* Transition: '<S1>:170' */
    /* Transition: '<S1>:89' */
  } else {
    /* Transition: '<S1>:13' */
    if (rtu_x < rtu_x2) {
      /* Transition: '<S1>:21' */
      /* Transition: '<S1>:20' */
      rtb_x_index = 0;
      rtb_x_frac = ((rtu_x - rtu_x1) << 15) / (rtu_x2 - rtu_x1);

      /* Transition: '<S1>:170' */
      /* Transition: '<S1>:89' */
    } else {
      /* Transition: '<S1>:16' */
      if (rtu_x < rtu_x3) {
        /* Transition: '<S1>:22' */
        /* Transition: '<S1>:23' */
        rtb_x_index = 1;
        rtb_x_frac = ((rtu_x - rtu_x2) << 15) / (rtu_x3 - rtu_x2);

        /* Transition: '<S1>:89' */
      } else {
        /* Transition: '<S1>:25' */
        rtb_x_index = 1;
        rtb_x_frac = 32768;
      }
    }
  }

  /* Transition: '<S1>:85' */
  if (rtu_y < rtu_y1) {
    /* Transition: '<S1>:106' */
    /* Transition: '<S1>:128' */
    rtb_y_index = 0;
    rtb_y_frac = 0;

    /* Transition: '<S1>:133' */
    /* Transition: '<S1>:140' */
    /* Transition: '<S1>:136' */
    /* Transition: '<S1>:139' */
    /* Transition: '<S1>:109' */
  } else {
    /* Transition: '<S1>:130' */
    if (rtu_y < rtu_y2) {
      /* Transition: '<S1>:132' */
      /* Transition: '<S1>:114' */
      rtb_y_index = 0;
      rtb_y_frac = ((rtu_y - rtu_y1) << 15) / (rtu_y2 - rtu_y1);

      /* Transition: '<S1>:140' */
      /* Transition: '<S1>:136' */
      /* Transition: '<S1>:139' */
      /* Transition: '<S1>:109' */
    } else {
      /* Transition: '<S1>:129' */
      if (rtu_y < rtu_y3) {
        /* Transition: '<S1>:146' */
        /* Transition: '<S1>:143' */
        rtb_y_index = 1;
        rtb_y_frac = ((rtu_y - rtu_y2) << 15) / (rtu_y3 - rtu_y2);

        /* Transition: '<S1>:136' */
        /* Transition: '<S1>:139' */
        /* Transition: '<S1>:109' */
      } else {
        /* Transition: '<S1>:144' */
        if (rtu_y < rtu_y4) {
          /* Transition: '<S1>:147' */
          /* Transition: '<S1>:142' */
          rtb_y_index = 2;
          rtb_y_frac = ((rtu_y - rtu_y3) << 15) / (rtu_y4 - rtu_y3);

          /* Transition: '<S1>:139' */
          /* Transition: '<S1>:109' */
        } else {
          /* Transition: '<S1>:145' */
          if (rtu_y < rtu_y5) {
            /* Transition: '<S1>:141' */
            /* Transition: '<S1>:135' */
            rtb_y_index = 3;
            rtb_y_frac = ((rtu_y - rtu_y4) << 15) / (rtu_y5 - rtu_y4);

            /* Transition: '<S1>:109' */
          } else {
            /* Transition: '<S1>:138' */
            rtb_y_index = 3;
            rtb_y_frac = 32768;
          }
        }
      }
    }
  }

  /* End of Chart: '<Root>/Chart1' */

  /* SignalConversion: '<S2>/TmpSignal ConversionAt SFunction Inport5' incorporates:
   *  Chart: '<Root>/Chart3'
   */
  /* Transition: '<S1>:148' */
  rtb_TmpSignalConversionAtSFunctionInport5[0] = rtu_z11;
  rtb_TmpSignalConversionAtSFunctionInport5[1] = rtu_z12;
  rtb_TmpSignalConversionAtSFunctionInport5[2] = rtu_z13;
  rtb_TmpSignalConversionAtSFunctionInport5[3] = rtu_z21;
  rtb_TmpSignalConversionAtSFunctionInport5[4] = rtu_z22;
  rtb_TmpSignalConversionAtSFunctionInport5[5] = rtu_z23;
  rtb_TmpSignalConversionAtSFunctionInport5[6] = rtu_z31;
  rtb_TmpSignalConversionAtSFunctionInport5[7] = rtu_z32;
  rtb_TmpSignalConversionAtSFunctionInport5[8] = rtu_z33;
  rtb_TmpSignalConversionAtSFunctionInport5[9] = rtu_z41;
  rtb_TmpSignalConversionAtSFunctionInport5[10] = rtu_z42;
  rtb_TmpSignalConversionAtSFunctionInport5[11] = rtu_z43;
  rtb_TmpSignalConversionAtSFunctionInport5[12] = rtu_z51;
  rtb_TmpSignalConversionAtSFunctionInport5[13] = rtu_z52;
  rtb_TmpSignalConversionAtSFunctionInport5[14] = rtu_z53;

  /* Chart: '<Root>/Chart3' */
  /* Gateway: Chart3 */
  /* During: Chart3 */
  /* Entry Internal: Chart3 */
  /* Transition: '<S2>:12' */
  /* comment */
  /* Transition: '<S2>:20' */
  z_1_index = (int16_T)((rtb_y_index * 3) + rtb_x_index);
  z_1 = (int16_T)(((int16_T)
                   (((rtb_TmpSignalConversionAtSFunctionInport5[z_1_index + 1] -
                      rtb_TmpSignalConversionAtSFunctionInport5[z_1_index]) *
                     rtb_x_frac) >> 15)) +
                  rtb_TmpSignalConversionAtSFunctionInport5[z_1_index]);

  /* Transition: '<S2>:60' */
  z_1_index = (int16_T)(((rtb_y_index + 1) * 3) + rtb_x_index);

  /* Transition: '<S2>:62' */
  *rty_Out = (int16_T)(((int16_T)(((((int16_T)(((int16_T)
    (((rtb_TmpSignalConversionAtSFunctionInport5[z_1_index + 1] -
       rtb_TmpSignalConversionAtSFunctionInport5[z_1_index]) * rtb_x_frac) >> 15))
    + rtb_TmpSignalConversionAtSFunctionInport5[z_1_index])) - z_1) * rtb_y_frac)
    >> 15)) + z_1);
}

/* Model initialize function */
void LCETCS_s16Inter3by5_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
