/*
 * File: LCETCS_vCalCardanTrqCmd.c
 *
 * Code generated for Simulink model 'LCETCS_vCalCardanTrqCmd'.
 *
 * Model version                  : 1.227
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:00:57 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalCardanTrqCmd.h"
#include "LCETCS_vCalCardanTrqCmd_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalCardanTrqCmd' */
void LCETCS_vCalCardanTrqCmd_Init(B_LCETCS_vCalCardanTrqCmd_c_T *localB,
  DW_LCETCS_vCalCardanTrqCmd_f_T *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalAdptByDelYaw' */
  LCETCS_vCalAdptByDelYaw_Init(&(localDW->LCETCS_vCalAdptByDelYaw_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalICtlPor' */
  LCETCS_vCalICtlPor_Init(&(localDW->LCETCS_vCalICtlPor_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vDctCtlCmdStat' */
  LCETCS_vDctCtlCmdStat_Init(&localB->lcetcsu1CtlCmdHitMax,
    &localB->lcetcsu1CtlCmdInc, &(localDW->LCETCS_vDctCtlCmdStat_DWORK1.rtdw));
}

/* Output and update for referenced model: 'LCETCS_vCalCardanTrqCmd' */
void LCETCS_vCalCardanTrqCmd(const TypeETCSLdTrqStruct *rtu_ETCS_LD_TRQ, const
  TypeETCSCtlErrStruct *rtu_ETCS_CTL_ERR, const TypeETCSIValRstStruct
  *rtu_ETCS_IVAL_RST, const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const
  TypeETCSCtlCmdStruct *rtu_ETCS_CTL_CMD_OLD, const TypeETCSCrdnTrqStruct
  *rtu_ETCS_CRDN_TRQ, const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const
  TypeETCSDrvMdlStruct *rtu_ETCS_DRV_MDL, const TypeETCSH2LStruct *rtu_ETCS_H2L,
  const TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN, const TypeETCSGainStruct
  *rtu_ETCS_CTL_GAINS, TypeETCSCtlCmdStruct *rty_ETCS_CTL_CMD,
  B_LCETCS_vCalCardanTrqCmd_c_T *localB, DW_LCETCS_vCalCardanTrqCmd_f_T *localDW)
{
  /* local block i/o variables */
  int32_T rtb_lcetcss32PCtlPor;
  int32_T rtb_lcetcss32AdptByWhlSpn;
  int32_T rtb_lcetcss32AdptByDelYaw;
  int32_T rtb_lcetcss32AdptAmt;
  int32_T rtb_lcetcss32CtlCmdMax;
  int32_T rtb_lcetcss32ICtlPor;
  int32_T rtb_lcetcss32CardanTrqCmd;

  /* ModelReference: '<Root>/LCETCS_vCalPCtlPor' */
  LCETCS_vCalPCtlPor(rtu_ETCS_CTL_ERR, rtu_ETCS_CTL_GAINS, &rtb_lcetcss32PCtlPor);

  /* ModelReference: '<Root>/LCETCS_vCalAdptByWhlSpn' */
  LCETCS_vCalAdptByWhlSpn(rtu_ETCS_CTL_GAINS, rtu_ETCS_CTL_ERR,
    &rtb_lcetcss32AdptByWhlSpn);

  /* ModelReference: '<Root>/LCETCS_vCalAdptByDelYaw' */
  LCETCS_vCalAdptByDelYaw(rtu_ETCS_ESC_SIG, rtu_ETCS_CTL_GAINS,
    &rtb_lcetcss32AdptByDelYaw, &(localDW->LCETCS_vCalAdptByDelYaw_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vCalAdptAmt' */
  LCETCS_vCalAdptAmt(rtb_lcetcss32AdptByWhlSpn, rtu_ETCS_CTL_GAINS,
                     rtb_lcetcss32AdptByDelYaw, rtu_ETCS_ESC_SIG,
                     rtu_ETCS_CTL_ERR, rtu_ETCS_TAR_SPIN, &rtb_lcetcss32AdptAmt);

  /* ModelReference: '<Root>/LCETCS_vCalCtlCmdMax' */
  LCETCS_vCalCtlCmdMax(rtu_ETCS_CRDN_TRQ, rtu_ETCS_DRV_MDL,
                       &rtb_lcetcss32CtlCmdMax);

  /* ModelReference: '<Root>/LCETCS_vCalICtlPor' */
  LCETCS_vCalICtlPor(rtu_ETCS_LD_TRQ, rtu_ETCS_IVAL_RST, rtu_ETCS_H2L,
                     rtb_lcetcss32AdptAmt, rtb_lcetcss32CtlCmdMax,
                     rtu_ETCS_CTL_ACT, &rtb_lcetcss32ICtlPor,
                     &(localDW->LCETCS_vCalICtlPor_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vSumCtlCmds' */
  LCETCS_vSumCtlCmds(rtb_lcetcss32PCtlPor, rtb_lcetcss32ICtlPor,
                     rtb_lcetcss32CtlCmdMax, &rtb_lcetcss32CardanTrqCmd);

  /* ModelReference: '<Root>/LCETCS_vDctCtlCmdStat' */
  LCETCS_vDctCtlCmdStat(rtb_lcetcss32CardanTrqCmd, rtu_ETCS_CRDN_TRQ,
                        rtu_ETCS_CTL_CMD_OLD, &localB->lcetcsu1CtlCmdHitMax,
                        &localB->lcetcsu1CtlCmdInc,
                        &(localDW->LCETCS_vDctCtlCmdStat_DWORK1.rtdw));

  /* BusCreator: '<Root>/Bus Creator' */
  rty_ETCS_CTL_CMD->lcetcss32PCtlPor = rtb_lcetcss32PCtlPor;
  rty_ETCS_CTL_CMD->lcetcss32ICtlPor = rtb_lcetcss32ICtlPor;
  rty_ETCS_CTL_CMD->lcetcss32AdptByDelYaw = rtb_lcetcss32AdptByDelYaw;
  rty_ETCS_CTL_CMD->lcetcss32CtlCmdMax = rtb_lcetcss32CtlCmdMax;
  rty_ETCS_CTL_CMD->lcetcss32CardanTrqCmd = rtb_lcetcss32CardanTrqCmd;
  rty_ETCS_CTL_CMD->lcetcsu1CtlCmdHitMax = localB->lcetcsu1CtlCmdHitMax;
  rty_ETCS_CTL_CMD->lcetcsu1CtlCmdInc = localB->lcetcsu1CtlCmdInc;
  rty_ETCS_CTL_CMD->lcetcss32AdptByWhlSpn = rtb_lcetcss32AdptByWhlSpn;
  rty_ETCS_CTL_CMD->lcetcss32AdptAmt = rtb_lcetcss32AdptAmt;
}

/* Model initialize function */
void LCETCS_vCalCardanTrqCmd_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalAdptAmt' */
  LCETCS_vCalAdptAmt_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalAdptByDelYaw' */
  LCETCS_vCalAdptByDelYaw_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalAdptByWhlSpn' */
  LCETCS_vCalAdptByWhlSpn_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalCtlCmdMax' */
  LCETCS_vCalCtlCmdMax_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalICtlPor' */
  LCETCS_vCalICtlPor_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalPCtlPor' */
  LCETCS_vCalPCtlPor_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vDctCtlCmdStat' */
  LCETCS_vDctCtlCmdStat_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vSumCtlCmds' */
  LCETCS_vSumCtlCmds_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
