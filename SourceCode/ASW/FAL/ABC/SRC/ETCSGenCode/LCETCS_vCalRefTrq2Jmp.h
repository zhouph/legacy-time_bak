/*
 * File: LCETCS_vCalRefTrq2Jmp.h
 *
 * Code generated for Simulink model 'LCETCS_vCalRefTrq2Jmp'.
 *
 * Model version                  : 1.220
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:54:35 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalRefTrq2Jmp_h_
#define RTW_HEADER_LCETCS_vCalRefTrq2Jmp_h_
#ifndef LCETCS_vCalRefTrq2Jmp_COMMON_INCLUDES_
# define LCETCS_vCalRefTrq2Jmp_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalRefTrq2Jmp_COMMON_INCLUDES_ */

#include "LCETCS_vCalRefTrq2Jmp_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Model reference registration function */
extern void LCETCS_vCalRefTrq2Jmp_initialize(void);
extern void LCETCS_vCalRefTrq2Jmp_Init(int32_T *rty_lcetcss32RefTrq2Jmp,
  boolean_T *rty_lcetcsu1RefTrq4JmpSetOk, int16_T *rty_lcetcss16RefTrqSetTm);
extern void LCETCS_vCalRefTrq2Jmp(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT,
  const TypeETCSErrZroCrsStruct *rtu_ETCS_ERR_ZRO_CRS, const
  TypeETCSCtlCmdStruct *rtu_ETCS_CTL_CMD_OLD, const TypeETCSH2LStruct
  *rtu_ETCS_H2L, const TypeETCSRefTrq2JmpStruct *rtu_ETCS_REF_TRQ2JMP_OLD,
  int32_T *rty_lcetcss32RefTrq2Jmp, boolean_T *rty_lcetcsu1RefTrq4JmpSetOk,
  int16_T *rty_lcetcss16RefTrqSetTm);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalRefTrq2Jmp'
 * '<S1>'   : 'LCETCS_vCalRefTrq2Jmp/CalRefTrq2Jmp'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalRefTrq2Jmp_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
