/*
 * File: LCETCS_vCalGrTransTime.c
 *
 * Code generated for Simulink model 'LCETCS_vCalGrTransTime'.
 *
 * Model version                  : 1.319
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:47:18 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalGrTransTime.h"
#include "LCETCS_vCalGrTransTime_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalGrTransTime' */
void LCETCS_vCalGrTransTime_Init(DW_LCETCS_vCalGrTransTime_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = 0;
}

/* Output and update for referenced model: 'LCETCS_vCalGrTransTime' */
void LCETCS_vCalGrTransTime(const TypeETCSGearStruct *rtu_ETCS_GEAR_STRUCT,
  uint8_T *rty_lcetcsu8GrTransTime, DW_LCETCS_vCalGrTransTime_f_T *localDW)
{
  /* Chart: '<Root>/Chart3' incorporates:
   *  UnitDelay: '<Root>/Unit Delay2'
   */
  /* Gateway: Chart3 */
  /* During: Chart3 */
  /* Entry Internal: Chart3 */
  /* Transition: '<S1>:123' */
  if (localDW->UnitDelay2_DSTATE == ((int16_T)GEAR_POS_TM_PN)) {
    /* Transition: '<S1>:101' */
    /* Transition: '<S1>:102' */
    *rty_lcetcsu8GrTransTime = 1U;

    /* Transition: '<S1>:186' */
  } else {
    /* Transition: '<S1>:108' */
    *rty_lcetcsu8GrTransTime = ((uint8_T)U8ETCSCpGrTransTme);
  }

  /* End of Chart: '<Root>/Chart3' */

  /* Saturate: '<Root>/Saturation' */
  /* Transition: '<S1>:188' */
  if ((*rty_lcetcsu8GrTransTime) > 250) {
    *rty_lcetcsu8GrTransTime = 250U;
  } else {
    if ((*rty_lcetcsu8GrTransTime) < 1) {
      *rty_lcetcsu8GrTransTime = 1U;
    }
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Update for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = rtu_ETCS_GEAR_STRUCT->lcetcss16CurntGearStep;
}

/* Model initialize function */
void LCETCS_vCalGrTransTime_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
