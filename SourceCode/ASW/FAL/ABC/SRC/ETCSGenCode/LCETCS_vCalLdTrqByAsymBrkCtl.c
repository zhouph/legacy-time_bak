/*
 * File: LCETCS_vCalLdTrqByAsymBrkCtl.c
 *
 * Code generated for Simulink model 'LCETCS_vCalLdTrqByAsymBrkCtl'.
 *
 * Model version                  : 1.85
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:48:06 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalLdTrqByAsymBrkCtl.h"
#include "LCETCS_vCalLdTrqByAsymBrkCtl_private.h"

/* Output and update for referenced model: 'LCETCS_vCalLdTrqByAsymBrkCtl' */
void LCETCS_vCalLdTrqByAsymBrkCtl(const TypeETCSAxlStruct *rtu_ETCS_FA, const
  TypeETCSAxlStruct *rtu_ETCS_RA, int32_T *rty_lcetcss32LdTrqByAsymBrkCtl)
{
  /* Product: '<Root>/Divide' incorporates:
   *  Constant: '<Root>/Constant2'
   *  Product: '<Root>/Product'
   *  Product: '<Root>/Product1'
   *  Product: '<Root>/Product2'
   *  Sum: '<Root>/Add'
   */
  *rty_lcetcss32LdTrqByAsymBrkCtl =
    ((((rtu_ETCS_FA->lcetcss16EstBrkTrqOnLowMuWhl +
        rtu_ETCS_RA->lcetcss16EstBrkTrqOnLowMuWhl) << 1) * 10) * ((uint8_T)
      U8ETCSCpLdTrqChngFac)) / 100;

  /* Saturate: '<Root>/Saturation' */
  if ((*rty_lcetcss32LdTrqByAsymBrkCtl) > 160000) {
    *rty_lcetcss32LdTrqByAsymBrkCtl = 160000;
  } else {
    if ((*rty_lcetcss32LdTrqByAsymBrkCtl) < 0) {
      *rty_lcetcss32LdTrqByAsymBrkCtl = 0;
    }
  }

  /* End of Saturate: '<Root>/Saturation' */
}

/* Model initialize function */
void LCETCS_vCalLdTrqByAsymBrkCtl_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
