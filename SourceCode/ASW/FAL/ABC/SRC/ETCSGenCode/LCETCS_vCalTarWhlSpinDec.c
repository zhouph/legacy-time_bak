/*
 * File: LCETCS_vCalTarWhlSpinDec.c
 *
 * Code generated for Simulink model 'LCETCS_vCalTarWhlSpinDec'.
 *
 * Model version                  : 1.60
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:49:17 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalTarWhlSpinDec.h"
#include "LCETCS_vCalTarWhlSpinDec_private.h"

/* Output and update for referenced model: 'LCETCS_vCalTarWhlSpinDec' */
void LCETCS_vCalTarWhlSpinDec(int16_T rtu_lcetcss16TarWhlSpnDecVib, int16_T
  rtu_lcetcss16TarWhlSpnIncAbnrmTire, int16_T rtu_lcetcss16TarDecInTrn, int16_T
  rtu_lcetcss16AddTar4DepSnw, int16_T *rty_lcetcss16TarWhlSpinDec)
{
  int16_T u0;

  /* Switch: '<Root>/Switch1' incorporates:
   *  Constant: '<Root>/Constant3'
   */
  if (rtu_lcetcss16AddTar4DepSnw > 0) {
    *rty_lcetcss16TarWhlSpinDec = 0;
  } else {
    *rty_lcetcss16TarWhlSpinDec = rtu_lcetcss16TarDecInTrn;
  }

  /* End of Switch: '<Root>/Switch1' */

  /* MinMax: '<Root>/MinMax1' */
  if (rtu_lcetcss16TarWhlSpnDecVib <= rtu_lcetcss16TarWhlSpnIncAbnrmTire) {
    u0 = rtu_lcetcss16TarWhlSpnDecVib;
  } else {
    u0 = rtu_lcetcss16TarWhlSpnIncAbnrmTire;
  }

  if (u0 <= (*rty_lcetcss16TarWhlSpinDec)) {
    *rty_lcetcss16TarWhlSpinDec = u0;
  }

  /* End of MinMax: '<Root>/MinMax1' */

  /* Saturate: '<Root>/Saturation' */
  if ((*rty_lcetcss16TarWhlSpinDec) < -120) {
    *rty_lcetcss16TarWhlSpinDec = -120;
  }

  /* End of Saturate: '<Root>/Saturation' */
}

/* Model initialize function */
void LCETCS_vCalTarWhlSpinDec_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
