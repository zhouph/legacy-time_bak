/*
 * File: LCETCS_vCalAftLmtCrng.h
 *
 * Code generated for Simulink model 'LCETCS_vCalAftLmtCrng'.
 *
 * Model version                  : 1.105
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:12:50 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalAftLmtCrng_h_
#define RTW_HEADER_LCETCS_vCalAftLmtCrng_h_
#ifndef LCETCS_vCalAftLmtCrng_COMMON_INCLUDES_
# define LCETCS_vCalAftLmtCrng_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalAftLmtCrng_COMMON_INCLUDES_ */

#include "LCETCS_vCalAftLmtCrng_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vCalSetAftLmtCrng.h"
#include "LCETCS_vCalInhAftLmtCrng.h"
#include "LCETCS_vCalCntAftLmtCrng.h"

/* Block signals for model 'LCETCS_vCalAftLmtCrng' */
typedef struct {
  boolean_T lcetcsu1InhAftLmtCrng;     /* '<Root>/LCETCS_vCalInhAftLmtCrng' */
  boolean_T lcetcsu1DctAftLmtCrng;     /* '<Root>/LCETCS_vCalSetAftLmtCrng' */
} B_LCETCS_vCalAftLmtCrng_c_T;

/* Block states (auto storage) for model 'LCETCS_vCalAftLmtCrng' */
typedef struct {
  int16_T UnitDelay2_DSTATE;           /* '<Root>/Unit Delay2' */
  int16_T UnitDelay1_DSTATE;           /* '<Root>/Unit Delay1' */
  MdlrefDW_LCETCS_vCalInhAftLmtCrng_T LCETCS_vCalInhAftLmtCrng_DWORK1;/* '<Root>/LCETCS_vCalInhAftLmtCrng' */
  MdlrefDW_LCETCS_vCalSetAftLmtCrng_T LCETCS_vCalSetAftLmtCrng_DWORK1;/* '<Root>/LCETCS_vCalSetAftLmtCrng' */
  MdlrefDW_LCETCS_vCalCntAftLmtCrng_T LCETCS_vCalCntAftLmtCrng_DWORK1;/* '<Root>/LCETCS_vCalCntAftLmtCrng' */
} DW_LCETCS_vCalAftLmtCrng_f_T;

typedef struct {
  B_LCETCS_vCalAftLmtCrng_c_T rtb;
  DW_LCETCS_vCalAftLmtCrng_f_T rtdw;
} MdlrefDW_LCETCS_vCalAftLmtCrng_T;

/* Model reference registration function */
extern void LCETCS_vCalAftLmtCrng_initialize(void);
extern void LCETCS_vCalAftLmtCrng_Init(B_LCETCS_vCalAftLmtCrng_c_T *localB,
  DW_LCETCS_vCalAftLmtCrng_f_T *localDW);
extern void LCETCS_vCalAftLmtCrng_Start(DW_LCETCS_vCalAftLmtCrng_f_T *localDW);
extern void LCETCS_vCalAftLmtCrng(const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT,
  const TypeETCSCtlCmdStruct *rtu_ETCS_CTL_CMD, const TypeETCSEscSigStruct
  *rtu_ETCS_ESC_SIG, const TypeETCSCtlErrStruct *rtu_ETCS_CTL_ERR, const
  TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN, TypeETCSAftLmtCrng
  *rty_ETCS_AFT_TURN, B_LCETCS_vCalAftLmtCrng_c_T *localB,
  DW_LCETCS_vCalAftLmtCrng_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalAftLmtCrng'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalAftLmtCrng_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
