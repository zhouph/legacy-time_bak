/*
 * File: LCETCS_vCalLdTrq.c
 *
 * Code generated for Simulink model 'LCETCS_vCalLdTrq'.
 *
 * Model version                  : 1.103
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:10:57 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalLdTrq.h"
#include "LCETCS_vCalLdTrq_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalLdTrq' */
void LCETCS_vCalLdTrq_Init(B_LCETCS_vCalLdTrq_c_T *localB,
  DW_LCETCS_vCalLdTrq_f_T *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalLdTrqDecBySymBrkCtl' */
  LCETCS_vCalLdTrqDecBySymBrkCtl_Init
    (&(localDW->LCETCS_vCalLdTrqDecBySymBrkCtl_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vMemAxAtHillDctd' */
  LCETCS_vMemAxAtHillDctd_Init(&localB->lcetcss16AxAtHillDctd,
    &(localDW->LCETCS_vMemAxAtHillDctd_DWORK1.rtdw),
    &(localDW->LCETCS_vMemAxAtHillDctd_DWORK1.rtb));

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalLdTrqByGrd' */
  LCETCS_vCalLdTrqByGrd_Init(&localB->lcetcss16LdTrqByGrd,
    &(localDW->LCETCS_vCalLdTrqByGrd_DWORK1.rtb),
    &(localDW->LCETCS_vCalLdTrqByGrd_DWORK1.rtdw));
}

/* Start for referenced model: 'LCETCS_vCalLdTrq' */
void LCETCS_vCalLdTrq_Start(DW_LCETCS_vCalLdTrq_f_T *localDW)
{
  /* Start for ModelReference: '<Root>/LCETCS_vCalLdTrqByGrd' */
  LCETCS_vCalLdTrqByGrd_Start(&(localDW->LCETCS_vCalLdTrqByGrd_DWORK1.rtb));
}

/* Output and update for referenced model: 'LCETCS_vCalLdTrq' */
void LCETCS_vCalLdTrq(const TypeETCSAxlStruct *rtu_ETCS_FA, const
                      TypeETCSAxlStruct *rtu_ETCS_RA, const
                      TypeETCSBrkCtlCmdStruct *rtu_ETCS_BRK_CTL_CMD_OLD, const
                      TypeETCSBrkCtlReqStruct *rtu_ETCS_BRK_CTL_REQ_OLD, const
                      TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const
                      TypeETCSSplitHillStruct *rtu_ETCS_SPLIT_HILL_OLD,
                      TypeETCSLdTrqStruct *rty_ETCS_LD_TRQ,
                      B_LCETCS_vCalLdTrq_c_T *localB, DW_LCETCS_vCalLdTrq_f_T
                      *localDW)
{
  /* local block i/o variables */
  int32_T rtb_lcetcss32LdTrqByAsymBrkCtl;
  int32_T rtb_lcetcss32LdTrqDecBySymBrkCtl;
  int32_T rtb_lcetcss32LdTrq;
  int16_T rtb_lcetcss16LdTrqByGrdRaw;
  int32_T rtb_lcetcss32LdTrqByAsymBrkCtl_g;
  int16_T rtb_lcetcss16LdTrqByGrdRaw_o;
  int16_T rtb_lcetcss16LdTrqByGrd;

  /* ModelReference: '<Root>/LCETCS_vCalLdTrqByAsymBrkCtl' */
  LCETCS_vCalLdTrqByAsymBrkCtl(rtu_ETCS_FA, rtu_ETCS_RA,
    &rtb_lcetcss32LdTrqByAsymBrkCtl);

  /* SignalConversion: '<Root>/Signal Conversion6' */
  rtb_lcetcss32LdTrqByAsymBrkCtl_g = rtb_lcetcss32LdTrqByAsymBrkCtl;

  /* ModelReference: '<Root>/LCETCS_vCalLdTrqDecBySymBrkCtl' */
  LCETCS_vCalLdTrqDecBySymBrkCtl(rtu_ETCS_BRK_CTL_CMD_OLD,
    rtu_ETCS_BRK_CTL_REQ_OLD, &rtb_lcetcss32LdTrqDecBySymBrkCtl,
    &(localDW->LCETCS_vCalLdTrqDecBySymBrkCtl_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCETCS_vMemAxAtHillDctd' */
  LCETCS_vMemAxAtHillDctd(rtu_ETCS_ESC_SIG, rtu_ETCS_SPLIT_HILL_OLD,
    &localB->lcetcss16AxAtHillDctd,
    &(localDW->LCETCS_vMemAxAtHillDctd_DWORK1.rtdw),
    &(localDW->LCETCS_vMemAxAtHillDctd_DWORK1.rtb));

  /* SignalConversion: '<Root>/Signal Conversion2' */
  rty_ETCS_LD_TRQ->lcetcss16AxAtHillDctd = localB->lcetcss16AxAtHillDctd;

  /* ModelReference: '<Root>/LCETCS_vCalLdTrqByGrdRaw' */
  LCETCS_vCalLdTrqByGrdRaw(localB->lcetcss16AxAtHillDctd,
    rtu_ETCS_SPLIT_HILL_OLD, &rtb_lcetcss16LdTrqByGrdRaw);

  /* SignalConversion: '<Root>/Signal Conversion3' */
  rtb_lcetcss16LdTrqByGrdRaw_o = rtb_lcetcss16LdTrqByGrdRaw;

  /* ModelReference: '<Root>/LCETCS_vCalLdTrqByGrd' */
  LCETCS_vCalLdTrqByGrd(rtb_lcetcss16LdTrqByGrdRaw, &localB->lcetcss16LdTrqByGrd,
                        &(localDW->LCETCS_vCalLdTrqByGrd_DWORK1.rtb),
                        &(localDW->LCETCS_vCalLdTrqByGrd_DWORK1.rtdw));

  /* SignalConversion: '<Root>/Signal Conversion4' */
  rtb_lcetcss16LdTrqByGrd = localB->lcetcss16LdTrqByGrd;

  /* ModelReference: '<Root>/LCETCS_vCordLdTrq' */
  LCETCS_vCordLdTrq(localB->lcetcss16LdTrqByGrd, rtb_lcetcss32LdTrqByAsymBrkCtl,
                    &rtb_lcetcss32LdTrq);

  /* BusCreator: '<Root>/Bus Creator' incorporates:
   *  SignalConversion: '<Root>/Signal Conversion1'
   *  SignalConversion: '<Root>/Signal Conversion5'
   */
  rty_ETCS_LD_TRQ->lcetcss32LdTrqByAsymBrkCtl = rtb_lcetcss32LdTrqByAsymBrkCtl_g;
  rty_ETCS_LD_TRQ->lcetcss32LdTrqDecBySymBrkCtl =
    rtb_lcetcss32LdTrqDecBySymBrkCtl;
  rty_ETCS_LD_TRQ->lcetcss16LdTrqByGrdRaw = rtb_lcetcss16LdTrqByGrdRaw_o;
  rty_ETCS_LD_TRQ->lcetcss16LdTrqByGrd = rtb_lcetcss16LdTrqByGrd;
  rty_ETCS_LD_TRQ->lcetcss32LdTrq = rtb_lcetcss32LdTrq;
}

/* Model initialize function */
void LCETCS_vCalLdTrq_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalLdTrqByAsymBrkCtl' */
  LCETCS_vCalLdTrqByAsymBrkCtl_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalLdTrqByGrd' */
  LCETCS_vCalLdTrqByGrd_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalLdTrqByGrdRaw' */
  LCETCS_vCalLdTrqByGrdRaw_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalLdTrqDecBySymBrkCtl' */
  LCETCS_vCalLdTrqDecBySymBrkCtl_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCordLdTrq' */
  LCETCS_vCordLdTrq_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vMemAxAtHillDctd' */
  LCETCS_vMemAxAtHillDctd_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
