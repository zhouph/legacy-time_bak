/*
 * File: LCETCS_vCalTarDecCnt.c
 *
 * Code generated for Simulink model 'LCETCS_vCalTarDecCnt'.
 *
 * Model version                  : 1.169
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:49:00 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalTarDecCnt.h"
#include "LCETCS_vCalTarDecCnt_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalTarDecCnt' */
void LCETCS_vCalTarDecCnt_Init(DW_LCETCS_vCalTarDecCnt_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = 0;
}

/* Output and update for referenced model: 'LCETCS_vCalTarDecCnt' */
void LCETCS_vCalTarDecCnt(boolean_T rtu_lcetcsu1TarSpnDec, const
  TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, int16_T *rty_lcetcss16TarSpnDecCnt,
  DW_LCETCS_vCalTarDecCnt_f_T *localDW)
{
  int16_T rtu_ETCS_ESC_SIG_0;
  int16_T rtu_ETCS_ESC_SIG_1;
  int32_T tmp;

  /* Chart: '<Root>/Chart' incorporates:
   *  UnitDelay: '<Root>/Unit Delay1'
   */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:37' */
  if (rtu_lcetcsu1TarSpnDec == 1) {
    /* Transition: '<S1>:110' */
    /* Transition: '<S1>:111' */
    tmp = localDW->UnitDelay1_DSTATE + 1;
    if (tmp > 32767) {
      tmp = 32767;
    }

    localDW->UnitDelay1_DSTATE = (int16_T)tmp;

    /* Transition: '<S1>:146' */
    /* Transition: '<S1>:141' */
    /* Transition: '<S1>:142' */
  } else {
    /* Transition: '<S1>:113' */
    /* 0.02g0.1g */
    if (rtu_ETCS_ESC_SIG->lcetcss16LatAccMsrd < 0) {
      tmp = -rtu_ETCS_ESC_SIG->lcetcss16LatAccMsrd;
      if (tmp > 32767) {
        tmp = 32767;
      }

      rtu_ETCS_ESC_SIG_0 = (int16_T)tmp;
    } else {
      rtu_ETCS_ESC_SIG_0 = rtu_ETCS_ESC_SIG->lcetcss16LatAccMsrd;
    }

    if (rtu_ETCS_ESC_SIG->lcetcss16LatAccMsrd < 0) {
      tmp = -rtu_ETCS_ESC_SIG->lcetcss16LatAccMsrd;
      if (tmp > 32767) {
        tmp = 32767;
      }

      rtu_ETCS_ESC_SIG_1 = (int16_T)tmp;
    } else {
      rtu_ETCS_ESC_SIG_1 = rtu_ETCS_ESC_SIG->lcetcss16LatAccMsrd;
    }

    tmp = rtu_ETCS_ESC_SIG->lcetcss16LatAccMsrdMani - rtu_ETCS_ESC_SIG_1;
    if (tmp < -32768) {
      tmp = -32768;
    }

    if ((rtu_ETCS_ESC_SIG_0 <= 2) || (tmp > 10)) {
      /* Transition: '<S1>:137' */
      /* Transition: '<S1>:138' */
      tmp = localDW->UnitDelay1_DSTATE - 1;
      if (tmp < -32768) {
        tmp = -32768;
      }

      localDW->UnitDelay1_DSTATE = (int16_T)tmp;

      /* Transition: '<S1>:141' */
      /* Transition: '<S1>:142' */
    } else {
      /* Transition: '<S1>:136' */
      if (localDW->UnitDelay1_DSTATE <= ((uint8_T)U8ETCSCpCnt4Tar1stIncInTrn)) {
        /* Transition: '<S1>:139' */
        /* Transition: '<S1>:140' */
        /* Transition: '<S1>:142' */
      } else {
        /* Transition: '<S1>:143' */
        tmp = localDW->UnitDelay1_DSTATE - 1;
        if (tmp < -32768) {
          tmp = -32768;
        }

        localDW->UnitDelay1_DSTATE = (int16_T)tmp;
      }
    }
  }

  /* End of Chart: '<Root>/Chart' */

  /* Saturate: '<Root>/Saturation' */
  /* Transition: '<S1>:144' */
  if (localDW->UnitDelay1_DSTATE > ((uint8_T)U8ETCSCpCnt4TarMaxDecInTrn)) {
    *rty_lcetcss16TarSpnDecCnt = ((uint8_T)U8ETCSCpCnt4TarMaxDecInTrn);
  } else if (localDW->UnitDelay1_DSTATE < 0) {
    *rty_lcetcss16TarSpnDecCnt = 0;
  } else {
    *rty_lcetcss16TarSpnDecCnt = localDW->UnitDelay1_DSTATE;
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Update for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = *rty_lcetcss16TarSpnDecCnt;
}

/* Model initialize function */
void LCETCS_vCalTarDecCnt_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
