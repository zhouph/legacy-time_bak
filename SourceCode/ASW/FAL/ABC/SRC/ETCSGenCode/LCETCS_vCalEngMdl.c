/*
 * File: LCETCS_vCalEngMdl.c
 *
 * Code generated for Simulink model 'LCETCS_vCalEngMdl'.
 *
 * Model version                  : 1.88
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:07:10 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalEngMdl.h"
#include "LCETCS_vCalEngMdl_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalEngMdl' */
void LCETCS_vCalEngMdl_Init(DW_LCETCS_vCalEngMdl_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = 0;

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalDeldSigDepOnEngSpd' */
  LCETCS_vCalDeldSigDepOnEngSpd_Init
    (&(localDW->LCETCS_vCalDeldSigDepOnEngSpd_DWORK1.rtb),
     &(localDW->LCETCS_vCalDeldSigDepOnEngSpd_DWORK1.rtdw));
}

/* Start for referenced model: 'LCETCS_vCalEngMdl' */
void LCETCS_vCalEngMdl_Start(DW_LCETCS_vCalEngMdl_f_T *localDW)
{
  /* Start for ModelReference: '<Root>/LCETCS_vCalDeldSigDepOnEngSpd' */
  LCETCS_vCalDeldSigDepOnEngSpd_Start
    (&(localDW->LCETCS_vCalDeldSigDepOnEngSpd_DWORK1.rtb));
}

/* Output and update for referenced model: 'LCETCS_vCalEngMdl' */
void LCETCS_vCalEngMdl(const TypeETCSEngStruct *rtu_ETCS_ENG_STRUCT, const
  TypeETCSCtlCmdStruct *rtu_ETCS_CTL_CMD_OLD, TypeETCSEngMdlStruct
  *rty_ETCS_ENG_MDL, DW_LCETCS_vCalEngMdl_f_T *localDW)
{
  /* local block i/o variables */
  int16_T rtb_Ls16InpGain4Flt;
  int16_T rtb_Ls16SigFltd;
  int16_T rtb_Ls16SigFltdOld;

  /* ModelReference: '<Root>/LCETCS_vCalActEngTrqFltGain' */
  LCETCS_vCalActEngTrqFltGain(rtu_ETCS_CTL_CMD_OLD, &rtb_Ls16InpGain4Flt);

  /* UnitDelay: '<Root>/Unit Delay' */
  rtb_Ls16SigFltdOld = localDW->UnitDelay_DSTATE;

  /* ModelReference: '<Root>/LCETCS_vCal1stOrdFlter' */
  LCETCS_vCal1stOrdFlter(rtu_ETCS_ENG_STRUCT->lcetcss16ActEngTrq,
    rtb_Ls16InpGain4Flt, rtb_Ls16SigFltdOld, &rtb_Ls16SigFltd);

  /* ModelReference: '<Root>/LCETCS_vCalDeldSigDepOnEngSpd' */
  LCETCS_vCalDeldSigDepOnEngSpd(rtu_ETCS_ENG_STRUCT, rtb_Ls16SigFltd,
    rty_ETCS_ENG_MDL, &(localDW->LCETCS_vCalDeldSigDepOnEngSpd_DWORK1.rtb),
    &(localDW->LCETCS_vCalDeldSigDepOnEngSpd_DWORK1.rtdw));

  /* Update for UnitDelay: '<Root>/Unit Delay' */
  localDW->UnitDelay_DSTATE = rtb_Ls16SigFltd;
}

/* Model initialize function */
void LCETCS_vCalEngMdl_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCal1stOrdFlter' */
  LCETCS_vCal1stOrdFlter_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalActEngTrqFltGain' */
  LCETCS_vCalActEngTrqFltGain_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalDeldSigDepOnEngSpd' */
  LCETCS_vCalDeldSigDepOnEngSpd_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
