/*
 * File: LCETCS_vCalHmIGainFctAftrTrn_private.h
 *
 * Code generated for Simulink model 'LCETCS_vCalHmIGainFctAftrTrn'.
 *
 * Model version                  : 1.184
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:01:16 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalHmIGainFctAftrTrn_private_h_
#define RTW_HEADER_LCETCS_vCalHmIGainFctAftrTrn_private_h_
#include "rtwtypes.h"
#include "model_reference_types.h"

/* Includes for objects with custom storage classes. */
#include "../ETCSGenCodeHeader/ETCSHeader4Import.h"

/*
 * Generate compile time checks that imported macros for parameters
 * with storage class "ImportedDefine" are defined
 */
#ifndef S16ETCSCpTotalGearRatio_1
#error The variable for the parameter "S16ETCSCpTotalGearRatio_1" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_2
#error The variable for the parameter "S16ETCSCpTotalGearRatio_2" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_3
#error The variable for the parameter "S16ETCSCpTotalGearRatio_3" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_4
#error The variable for the parameter "S16ETCSCpTotalGearRatio_4" is not defined
#endif

#ifndef S16ETCSCpTotalGearRatio_5
#error The variable for the parameter "S16ETCSCpTotalGearRatio_5" is not defined
#endif

#ifndef U8ETCSCpAyAsp
#error The variable for the parameter "U8ETCSCpAyAsp" is not defined
#endif

#ifndef U8ETCSCpAyIce
#error The variable for the parameter "U8ETCSCpAyIce" is not defined
#endif

#ifndef U8ETCSCpAySnw
#error The variable for the parameter "U8ETCSCpAySnw" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnAsp_1
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnAsp_1" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnAsp_2
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnAsp_2" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnAsp_3
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnAsp_3" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnAsp_4
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnAsp_4" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnAsp_5
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnAsp_5" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnIce_1
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnIce_1" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnIce_2
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnIce_2" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnIce_3
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnIce_3" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnIce_4
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnIce_4" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnIce_5
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnIce_5" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnSnow_1
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnSnow_1" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnSnow_2
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnSnow_2" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnSnow_3
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnSnow_3" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnSnow_4
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnSnow_4" is not defined
#endif

#ifndef U8ETCSCpIgainFacAftrTrnSnow_5
#error The variable for the parameter "U8ETCSCpIgainFacAftrTrnSnow_5" is not defined
#endif

#ifndef PORTABLE_WORDSIZES
#ifndef UCHAR_MAX
#include <limits.h>
#endif

#if ( UCHAR_MAX != (0xFFU) ) || ( SCHAR_MAX != (0x7F) )
#error Code was generated for compiler with different sized uchar/char. \
Consider adjusting Test hardware word size settings on the \
Hardware Implementation pane to match your compiler word sizes as \
defined in limits.h of the compiler. Alternatively, you can \
select the Test hardware is the same as production hardware option and \
select the Enable portable word sizes option on the Code Generation > \
Verification pane for ERT based targets, which will disable the \
preprocessor word size checks.
#endif

#if ( USHRT_MAX != (0xFFFFU) ) || ( SHRT_MAX != (0x7FFF) )
#error Code was generated for compiler with different sized ushort/short. \
Consider adjusting Test hardware word size settings on the \
Hardware Implementation pane to match your compiler word sizes as \
defined in limits.h of the compiler. Alternatively, you can \
select the Test hardware is the same as production hardware option and \
select the Enable portable word sizes option on the Code Generation > \
Verification pane for ERT based targets, which will disable the \
preprocessor word size checks.
#endif

#if ( UINT_MAX != (0xFFFFFFFFU) ) || ( INT_MAX != (0x7FFFFFFF) )
#error Code was generated for compiler with different sized uint/int. \
Consider adjusting Test hardware word size settings on the \
Hardware Implementation pane to match your compiler word sizes as \
defined in limits.h of the compiler. Alternatively, you can \
select the Test hardware is the same as production hardware option and \
select the Enable portable word sizes option on the Code Generation > \
Verification pane for ERT based targets, which will disable the \
preprocessor word size checks.
#endif

#if ( ULONG_MAX != (0xFFFFFFFFU) ) || ( LONG_MAX != (0x7FFFFFFF) )
#error Code was generated for compiler with different sized ulong/long. \
Consider adjusting Test hardware word size settings on the \
Hardware Implementation pane to match your compiler word sizes as \
defined in limits.h of the compiler. Alternatively, you can \
select the Test hardware is the same as production hardware option and \
select the Enable portable word sizes option on the Code Generation > \
Verification pane for ERT based targets, which will disable the \
preprocessor word size checks.
#endif
#endif                                 /* PORTABLE_WORDSIZES */
#endif                                 /* RTW_HEADER_LCETCS_vCalHmIGainFctAftrTrn_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
