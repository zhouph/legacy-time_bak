/*
 * File: LCETCS_vCalTarWhlSpin.h
 *
 * Code generated for Simulink model 'LCETCS_vCalTarWhlSpin'.
 *
 * Model version                  : 1.181
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:11:27 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalTarWhlSpin_h_
#define RTW_HEADER_LCETCS_vCalTarWhlSpin_h_
#ifndef LCETCS_vCalTarWhlSpin_COMMON_INCLUDES_
# define LCETCS_vCalTarWhlSpin_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalTarWhlSpin_COMMON_INCLUDES_ */

#include "LCETCS_vCalTarWhlSpin_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vCordTarWhlSpin.h"
#include "LCETCS_vCalTarWhlSpnIncTire.h"
#include "LCETCS_vCalTarWhlSpnIncSpHill.h"
#include "LCETCS_vCalTarWhlSpnIncRughRd.h"
#include "LCETCS_vCalTarWhlSpnIncHiRd.h"
#include "LCETCS_vCalTarWhlSpnIncCtrlMd.h"
#include "LCETCS_vCalTarWhlSpinInc.h"
#include "LCETCS_vCalTarWhlSpinInLwToSplt.h"
#include "LCETCS_vCalTarWhlSpinDecVib.h"
#include "LCETCS_vCalTarWhlSpinDec.h"
#include "LCETCS_vCalTarWhlSpdStlDetd.h"
#include "LCETCS_vCalTarSpnLmtCrnrg.h"
#include "LCETCS_vCalCordBsTarWhlSpin.h"
#include "LCETCS_vCalBsTarWhlSpin.h"
#include "LCETCS_vCalAddTar4DepSnw.h"

/* Block signals for model 'LCETCS_vCalTarWhlSpin' */
typedef struct {
  int16_T lcetcss16AddTar4CtrlMode;    /* '<Root>/LCETCS_vCalTarWhlSpnIncCtrlMd' */
  boolean_T lcetcsu1AbnrmSizeTire;     /* '<Root>/LCETCS_vCalTarWhlSpnIncTire' */
  boolean_T lcetcsu1TarSpnDec;         /* '<Root>/LCETCS_vCalTarSpnLmtCrnrg' */
  boolean_T lcetcsu1DctLmtCrng;        /* '<Root>/LCETCS_vCalTarSpnLmtCrnrg' */
  boolean_T lcetcsu1DctLmtCrngFalEdg;  /* '<Root>/LCETCS_vCalTarSpnLmtCrnrg' */
} B_LCETCS_vCalTarWhlSpin_c_T;

/* Block states (auto storage) for model 'LCETCS_vCalTarWhlSpin' */
typedef struct {
  MdlrefDW_LCETCS_vCalBsTarWhlSpin_T LCETCS_vCalBsTarWhlSpin_DWORK1;/* '<Root>/LCETCS_vCalBsTarWhlSpin' */
  MdlrefDW_LCETCS_vCalTarWhlSpinInLwToSplt_T
    LCETCS_vCalTarWhlSpinInLwToSplt_DWORK1;/* '<Root>/LCETCS_vCalTarWhlSpinInLwToSplt' */
  MdlrefDW_LCETCS_vCalTarWhlSpnIncTire_T LCETCS_vCalTarWhlSpnIncTire_DWORK1;/* '<Root>/LCETCS_vCalTarWhlSpnIncTire' */
  MdlrefDW_LCETCS_vCalAddTar4DepSnw_T LCETCS_vCalAddTar4DepSnw_DWORK1;/* '<Root>/LCETCS_vCalAddTar4DepSnw' */
  MdlrefDW_LCETCS_vCalTarWhlSpnIncSpHill_T LCETCS_vCalTarWhlSpnIncSpHill_DWORK1;/* '<Root>/LCETCS_vCalTarWhlSpnIncSpHill' */
  MdlrefDW_LCETCS_vCalTarSpnLmtCrnrg_T LCETCS_vCalTarSpnLmtCrnrg_DWORK1;/* '<Root>/LCETCS_vCalTarSpnLmtCrnrg' */
  MdlrefDW_LCETCS_vCalTarWhlSpnIncHiRd_T LCETCS_vCalTarWhlSpnIncHiRd_DWORK1;/* '<Root>/LCETCS_vCalTarWhlSpnIncHiRd' */
  MdlrefDW_LCETCS_vCordTarWhlSpin_T LCETCS_vCordTarWhlSpin_DWORK1;/* '<Root>/LCETCS_vCordTarWhlSpin' */
} DW_LCETCS_vCalTarWhlSpin_f_T;

typedef struct {
  B_LCETCS_vCalTarWhlSpin_c_T rtb;
  DW_LCETCS_vCalTarWhlSpin_f_T rtdw;
} MdlrefDW_LCETCS_vCalTarWhlSpin_T;

/* Model reference registration function */
extern void LCETCS_vCalTarWhlSpin_initialize(void);
extern void LCETCS_vCalTarWhlSpin_Init(B_LCETCS_vCalTarWhlSpin_c_T *localB,
  DW_LCETCS_vCalTarWhlSpin_f_T *localDW);
extern void LCETCS_vCalTarWhlSpin_Start(DW_LCETCS_vCalTarWhlSpin_f_T *localDW);
extern void LCETCS_vCalTarWhlSpin(const TypeETCSDepSnwStruct *rtu_ETCS_DEP_SNW,
  const TypeETCSLwToSpltStruct *rtu_ETCS_L2SPLT, int16_T rtu_lcetcss16VehSpd,
  const TypeETCSExtDctStruct *rtu_ETCS_EXT_DCT, const TypeETCSBrkSigStruct
  *rtu_ETCS_BRK_SIG, const TypeETCSAxlStruct *rtu_ETCS_FA, const
  TypeETCSAxlStruct *rtu_ETCS_RA, const TypeETCSEngStallStruct
  *rtu_ETCS_ENG_STALL_OLD, const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT_OLD,
  boolean_T rtu_lcetcsu1VehVibDctOld, const TypeETCSEscSigStruct
  *rtu_ETCS_ESC_SIG, const TypeETCSStrStatStruct *rtu_ETCS_STR_STAT, const
  TypeETCSSplitHillStruct *rtu_ETCS_SPLIT_HILL_OLD, const TypeETCSWhlStruct
  *rtu_ETCS_FL, const TypeETCSWhlStruct *rtu_ETCS_FR, const TypeETCSWhlStruct
  *rtu_ETCS_RL, const TypeETCSWhlStruct *rtu_ETCS_RR, const
  TypeETCSCtrlModeStruct *rtu_ETCS_CTRL_MODE, const TypeETCSHighDctStruct
  *rtu_ETCS_HI_DCT_OLD, const TypeETCSDrvMdlStruct *rtu_ETCS_DRV_MDL,
  TypeETCSTarSpinStruct *rty_ETCS_TAR_SPIN, B_LCETCS_vCalTarWhlSpin_c_T *localB,
  DW_LCETCS_vCalTarWhlSpin_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalTarWhlSpin'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalTarWhlSpin_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
