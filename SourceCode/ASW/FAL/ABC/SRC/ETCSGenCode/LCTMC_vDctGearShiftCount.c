/*
 * File: LCTMC_vDctGearShiftCount.c
 *
 * Code generated for Simulink model 'LCTMC_vDctGearShiftCount'.
 *
 * Model version                  : 1.254
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:53:27 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCTMC_vDctGearShiftCount.h"
#include "LCTMC_vDctGearShiftCount_private.h"

/* Initial conditions for referenced model: 'LCTMC_vDctGearShiftCount' */
void LCTMC_vDctGearShiftCount_Init(boolean_T *rty_lctmcu1GearShiftCountSet,
  DW_LCTMC_vDctGearShiftCount_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay1' */
  localDW->UnitDelay1_DSTATE = 0U;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay3' */
  localDW->UnitDelay3_DSTATE = false;

  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay2' */
  localDW->UnitDelay2_DSTATE = 0U;

  /* InitializeConditions for Chart: '<Root>/Chart1' */
  *rty_lctmcu1GearShiftCountSet = false;
}

/* Output and update for referenced model: 'LCTMC_vDctGearShiftCount' */
void LCTMC_vDctGearShiftCount(uint8_T rtu_lctmcu8_BSTGRReqGearOld, boolean_T
  rtu_lctmcu1DriveGearMode, const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT,
  boolean_T *rty_lctmcu1GearShiftCountSet, DW_LCTMC_vDctGearShiftCount_f_T
  *localDW)
{
  int32_T tmp;

  /* Chart: '<Root>/Chart1' incorporates:
   *  UnitDelay: '<Root>/Unit Delay1'
   *  UnitDelay: '<Root>/Unit Delay2'
   *  UnitDelay: '<Root>/Unit Delay3'
   */
  /* Gateway: Chart1 */
  /* During: Chart1 */
  /* Entry Internal: Chart1 */
  /* Transition: '<S1>:10' */
  /* comment */
  if (rtu_lctmcu8_BSTGRReqGearOld != localDW->UnitDelay1_DSTATE) {
    /* Transition: '<S1>:1' */
    /* Transition: '<S1>:2' */
    *rty_lctmcu1GearShiftCountSet = true;
    localDW->UnitDelay2_DSTATE = 0U;

    /* Transition: '<S1>:68' */
    /* Transition: '<S1>:62' */
    /* Transition: '<S1>:57' */
  } else {
    /* Transition: '<S1>:67' */
    if (localDW->UnitDelay3_DSTATE == 1) {
      /* Transition: '<S1>:59' */
      /* Transition: '<S1>:61' */
      if (((localDW->UnitDelay2_DSTATE > ((uint8_T)U8TMCpGearShiftTime)) ||
           (rtu_lctmcu1DriveGearMode == 0)) || (rtu_ETCS_CTL_ACT->lcetcsu1CtlAct
           == 0)) {
        /* Transition: '<S1>:65' */
        /* Transition: '<S1>:58' */
        *rty_lctmcu1GearShiftCountSet = false;
        localDW->UnitDelay2_DSTATE = 0U;

        /* Transition: '<S1>:62' */
        /* Transition: '<S1>:57' */
      } else {
        /* Transition: '<S1>:66' */
        *rty_lctmcu1GearShiftCountSet = localDW->UnitDelay3_DSTATE;
        tmp = localDW->UnitDelay2_DSTATE + 1;
        if (tmp > 65535) {
          tmp = 65535;
        }

        localDW->UnitDelay2_DSTATE = (uint16_T)tmp;

        /* Transition: '<S1>:57' */
      }
    } else {
      /* Transition: '<S1>:64' */
      *rty_lctmcu1GearShiftCountSet = false;
      localDW->UnitDelay2_DSTATE = 0U;
    }
  }

  /* End of Chart: '<Root>/Chart1' */

  /* Update for UnitDelay: '<Root>/Unit Delay1' */
  /* Transition: '<S1>:63' */
  localDW->UnitDelay1_DSTATE = rtu_lctmcu8_BSTGRReqGearOld;

  /* Update for UnitDelay: '<Root>/Unit Delay3' */
  localDW->UnitDelay3_DSTATE = *rty_lctmcu1GearShiftCountSet;
}

/* Model initialize function */
void LCTMC_vDctGearShiftCount_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
