/*
 * File: LCETCS_vCalDeldSigDepOnEngSpd.c
 *
 * Code generated for Simulink model 'LCETCS_vCalDeldSigDepOnEngSpd'.
 *
 * Model version                  : 1.99
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:59:03 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalDeldSigDepOnEngSpd.h"
#include "LCETCS_vCalDeldSigDepOnEngSpd_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalDeldSigDepOnEngSpd' */
void LCETCS_vCalDeldSigDepOnEngSpd_Init(B_LCETCS_vCalDeldSigDepOnEngSpd_c_T
  *localB, DW_LCETCS_vCalDeldSigDepOnEngSpd_f_T *localDW)
{
  int32_T i;

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter5Point' */
  LCETCS_s16Inter5Point_Init(&localB->y);

  /* InitializeConditions for Delay: '<Root>/Delay' */
  for (i = 0; i < 10; i++) {
    localDW->Delay_DSTATE[i] = 0;
  }

  localDW->CircBufIdx = 0;

  /* End of InitializeConditions for Delay: '<Root>/Delay' */
}

/* Start for referenced model: 'LCETCS_vCalDeldSigDepOnEngSpd' */
void LCETCS_vCalDeldSigDepOnEngSpd_Start(B_LCETCS_vCalDeldSigDepOnEngSpd_c_T
  *localB)
{
  /* Start for Constant: '<Root>/Constant5' */
  localB->y1 = ((uint8_T)U8ETCSCpTrqDel_1);

  /* Start for Constant: '<Root>/Constant6' */
  localB->y2 = ((uint8_T)U8ETCSCpTrqDel_2);

  /* Start for Constant: '<Root>/Constant7' */
  localB->y3 = ((uint8_T)U8ETCSCpTrqDel_3);

  /* Start for Constant: '<Root>/Constant8' */
  localB->y4 = ((uint8_T)U8ETCSCpTrqDel_4);

  /* Start for Constant: '<Root>/Constant9' */
  localB->y5 = ((uint8_T)U8ETCSCpTrqDel_5);
}

/* Output and update for referenced model: 'LCETCS_vCalDeldSigDepOnEngSpd' */
void LCETCS_vCalDeldSigDepOnEngSpd(const TypeETCSEngStruct *rtu_ETCS_ENG_STRUCT,
  int16_T rtu_lcetcss16ActEngTrqFltd, TypeETCSEngMdlStruct *rty_ETCS_ENG_MDL,
  B_LCETCS_vCalDeldSigDepOnEngSpd_c_T *localB,
  DW_LCETCS_vCalDeldSigDepOnEngSpd_f_T *localDW)
{
  int16_T rtb_Saturation;

  /* Constant: '<Root>/Constant5' */
  localB->y1 = ((uint8_T)U8ETCSCpTrqDel_1);

  /* Constant: '<Root>/Constant6' */
  localB->y2 = ((uint8_T)U8ETCSCpTrqDel_2);

  /* Constant: '<Root>/Constant7' */
  localB->y3 = ((uint8_T)U8ETCSCpTrqDel_3);

  /* Constant: '<Root>/Constant8' */
  localB->y4 = ((uint8_T)U8ETCSCpTrqDel_4);

  /* Constant: '<Root>/Constant9' */
  localB->y5 = ((uint8_T)U8ETCSCpTrqDel_5);

  /* ModelReference: '<Root>/LCETCS_s16Inter5Point' */
  LCETCS_s16Inter5Point(rtu_ETCS_ENG_STRUCT->lcetcss16EngSpd, ((int16_T)
    S16ETCSCpEngSpd_1), ((int16_T)S16ETCSCpEngSpd_2), ((int16_T)
    S16ETCSCpEngSpd_3), ((int16_T)S16ETCSCpEngSpd_4), ((int16_T)
    S16ETCSCpEngSpd_5), localB->y1, localB->y2, localB->y3, localB->y4,
                        localB->y5, &localB->y);

  /* Saturate: '<Root>/Saturation' */
  if (localB->y > 10) {
    rtb_Saturation = 10;
  } else if (localB->y < 0) {
    rtb_Saturation = 0;
  } else {
    rtb_Saturation = localB->y;
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Delay: '<Root>/Delay' */
  if (rtb_Saturation <= 0) {
    rtb_Saturation = rtu_lcetcss16ActEngTrqFltd;
  } else {
    if (rtb_Saturation <= localDW->CircBufIdx) {
      rtb_Saturation = (int16_T)(localDW->CircBufIdx - rtb_Saturation);
    } else {
      rtb_Saturation = (int16_T)(((int16_T)(localDW->CircBufIdx - rtb_Saturation))
        + 10);
    }

    rtb_Saturation = localDW->Delay_DSTATE[rtb_Saturation];
  }

  /* End of Delay: '<Root>/Delay' */

  /* Saturate: '<Root>/Saturation1' */
  if (rtb_Saturation > 15000) {
    rty_ETCS_ENG_MDL->lcetcss16ActEngTrqDeld = 15000;
  } else if (rtb_Saturation < -10000) {
    rty_ETCS_ENG_MDL->lcetcss16ActEngTrqDeld = -10000;
  } else {
    rty_ETCS_ENG_MDL->lcetcss16ActEngTrqDeld = rtb_Saturation;
  }

  /* End of Saturate: '<Root>/Saturation1' */

  /* BusCreator: '<Root>/Bus Creator' */
  rty_ETCS_ENG_MDL->lcetcss16ActEngTrqFltd = rtu_lcetcss16ActEngTrqFltd;

  /* Update for Delay: '<Root>/Delay' */
  localDW->Delay_DSTATE[localDW->CircBufIdx] = rtu_lcetcss16ActEngTrqFltd;
  if (localDW->CircBufIdx < 9) {
    localDW->CircBufIdx++;
  } else {
    localDW->CircBufIdx = 0;
  }

  /* End of Update for Delay: '<Root>/Delay' */
}

/* Model initialize function */
void LCETCS_vCalDeldSigDepOnEngSpd_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter5Point' */
  LCETCS_s16Inter5Point_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
