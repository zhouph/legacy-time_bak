/*
 * File: LCETCS_vMemAxAtHillDctd.h
 *
 * Code generated for Simulink model 'LCETCS_vMemAxAtHillDctd'.
 *
 * Model version                  : 1.218
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:59:10 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vMemAxAtHillDctd_h_
#define RTW_HEADER_LCETCS_vMemAxAtHillDctd_h_
#ifndef LCETCS_vMemAxAtHillDctd_COMMON_INCLUDES_
# define LCETCS_vMemAxAtHillDctd_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vMemAxAtHillDctd_COMMON_INCLUDES_ */

#include "LCETCS_vMemAxAtHillDctd_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vDctRisingEdge.h"

/* Block signals for model 'LCETCS_vMemAxAtHillDctd' */
typedef struct {
  boolean_T RisingEdge;                /* '<Root>/LCETCS_vDctRisingEdge' */
} B_LCETCS_vMemAxAtHillDctd_c_T;

/* Block states (auto storage) for model 'LCETCS_vMemAxAtHillDctd' */
typedef struct {
  int16_T UnitDelay1_DSTATE;           /* '<Root>/Unit Delay1' */
  MdlrefDW_LCETCS_vDctRisingEdge_T LCETCS_vDctRisingEdge_DWORK1;/* '<Root>/LCETCS_vDctRisingEdge' */
} DW_LCETCS_vMemAxAtHillDctd_f_T;

typedef struct {
  B_LCETCS_vMemAxAtHillDctd_c_T rtb;
  DW_LCETCS_vMemAxAtHillDctd_f_T rtdw;
} MdlrefDW_LCETCS_vMemAxAtHillDctd_T;

/* Model reference registration function */
extern void LCETCS_vMemAxAtHillDctd_initialize(void);
extern void LCETCS_vMemAxAtHillDctd_Init(int16_T *rty_lcetcss16AxAtHillDctd,
  DW_LCETCS_vMemAxAtHillDctd_f_T *localDW, B_LCETCS_vMemAxAtHillDctd_c_T *localB);
extern void LCETCS_vMemAxAtHillDctd(const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG,
  const TypeETCSSplitHillStruct *rtu_ETCS_SPLIT_HILL_OLD, int16_T
  *rty_lcetcss16AxAtHillDctd, DW_LCETCS_vMemAxAtHillDctd_f_T *localDW,
  B_LCETCS_vMemAxAtHillDctd_c_T *localB);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vMemAxAtHillDctd'
 * '<S1>'   : 'LCETCS_vMemAxAtHillDctd/Chart1'
 */
#endif                                 /* RTW_HEADER_LCETCS_vMemAxAtHillDctd_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
