/*
 * File: LCETCS_vCalRefAxRepRdFric.h
 *
 * Code generated for Simulink model 'LCETCS_vCalRefAxRepRdFric'.
 *
 * Model version                  : 1.511
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:00:37 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalRefAxRepRdFric_h_
#define RTW_HEADER_LCETCS_vCalRefAxRepRdFric_h_
#ifndef LCETCS_vCalRefAxRepRdFric_COMMON_INCLUDES_
# define LCETCS_vCalRefAxRepRdFric_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalRefAxRepRdFric_COMMON_INCLUDES_ */

#include "LCETCS_vCalRefAxRepRdFric_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_s16Inter5Point.h"

/* Block signals for model 'LCETCS_vCalRefAxRepRdFric' */
typedef struct {
  int16_T y5;                          /* '<Root>/Ax  for 1st gear ratio on Asphalt' */
  int16_T y5_l;                        /* '<Root>/Ax  for 1st gear ratio on Ice' */
  int16_T y5_lj;                       /* '<Root>/Ax  for 1st gear ratio on Snow' */
  int16_T y4;                          /* '<Root>/Ax  for 2nd gear ratio on Asphalt' */
  int16_T y4_d;                        /* '<Root>/Ax  for 2nd gear ratio on Ice' */
  int16_T y4_n;                        /* '<Root>/Ax  for 2nd gear ratio on Snow' */
  int16_T y3;                          /* '<Root>/Ax  for 3rd gear ratio on Asphalt' */
  int16_T y3_a;                        /* '<Root>/Ax  for 3rd gear ratio on Ice' */
  int16_T y3_h;                        /* '<Root>/Ax  for 3rd gear ratio on Snow' */
  int16_T y2;                          /* '<Root>/Ax  for 4th gear ratio on Asphalt' */
  int16_T y2_i;                        /* '<Root>/Ax  for 4th gear ratio on Ice' */
  int16_T y2_l;                        /* '<Root>/Ax  for 4th gear ratio on Snow' */
  int16_T y1;                          /* '<Root>/Ax  for 5th gear ratio on Asphalt' */
  int16_T y1_c;                        /* '<Root>/Ax  for 5th gear ratio on Ice' */
  int16_T y1_b;                        /* '<Root>/Ax  for 5th gear ratio on Snow' */
  int16_T y;                           /* '<Root>/LCETCS_s16Inter5Point' */
  int16_T y_k;                         /* '<Root>/LCETCS_s16Inter5Point1' */
  int16_T y_o;                         /* '<Root>/LCETCS_s16Inter5Point2' */
} B_LCETCS_vCalRefAxRepRdFric_c_T;

typedef struct {
  B_LCETCS_vCalRefAxRepRdFric_c_T rtb;
} MdlrefDW_LCETCS_vCalRefAxRepRdFric_T;

/* Model reference registration function */
extern void LCETCS_vCalRefAxRepRdFric_initialize(void);
extern void LCETCS_vCalRefAxRepRdFric_Init(B_LCETCS_vCalRefAxRepRdFric_c_T
  *localB);
extern void LCETCS_vCalRefAxRepRdFric_Start(B_LCETCS_vCalRefAxRepRdFric_c_T
  *localB);
extern void LCETCS_vCalRefAxRepRdFric(const TypeETCSDrvMdlStruct
  *rtu_ETCS_DRV_MDL, int16_T *rty_lcetcss16RefAxRdFricAtIce, int16_T
  *rty_lcetcss16RefAxRdFricAtSnow, int16_T *rty_lcetcss16RefAxRdFricAtAsphalt,
  B_LCETCS_vCalRefAxRepRdFric_c_T *localB);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalRefAxRepRdFric'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalRefAxRepRdFric_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
