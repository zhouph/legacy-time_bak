/*
 * File: LCETCS_vCalIValAtErrZroCrs.c
 *
 * Code generated for Simulink model 'LCETCS_vCalIValAtErrZroCrs'.
 *
 * Model version                  : 1.185
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:54:01 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalIValAtErrZroCrs.h"
#include "LCETCS_vCalIValAtErrZroCrs_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalIValAtErrZroCrs' */
void LCETCS_vCalIValAtErrZroCrs_Init(int32_T *rty_lcetcss32IValAtErrZroCrs,
  boolean_T *rty_lcetcsu1RstIValAtErrZroCrs)
{
  /* InitializeConditions for Chart: '<Root>/Chart' */
  *rty_lcetcss32IValAtErrZroCrs = 0;
  *rty_lcetcsu1RstIValAtErrZroCrs = false;
}

/* Output and update for referenced model: 'LCETCS_vCalIValAtErrZroCrs' */
void LCETCS_vCalIValAtErrZroCrs(boolean_T rtu_lcetcsu1RstIValAt2ndCylStrt, const
  TypeETCSCtlCmdStruct *rtu_ETCS_CTL_CMD_OLD, const TypeETCSErrZroCrsStruct
  *rtu_ETCS_ERR_ZRO_CRS, const TypeETCSRefTrq2JmpStruct *rtu_ETCS_REF_TRQ2JMP,
  const TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const TypeETCSTarSpinStruct
  *rtu_ETCS_TAR_SPIN, int32_T *rty_lcetcss32IValAtErrZroCrs, boolean_T
  *rty_lcetcsu1RstIValAtErrZroCrs)
{
  int32_T q1;

  /* Chart: '<Root>/Chart' */
  /* Gateway: Chart */
  /* During: Chart */
  /* Entry Internal: Chart */
  /* Transition: '<S1>:27' */
  /* comment */
  if (rtu_lcetcsu1RstIValAt2ndCylStrt == 1) {
    /* Transition: '<S1>:78' */
    /* Transition: '<S1>:82' */
    *rty_lcetcss32IValAtErrZroCrs = 0;
    *rty_lcetcsu1RstIValAtErrZroCrs = false;

    /* Transition: '<S1>:85' */
    /* Transition: '<S1>:94' */
    /* Transition: '<S1>:73' */
  } else {
    /* Transition: '<S1>:79' */
    if (((rtu_ETCS_ERR_ZRO_CRS->lcetcsu1JumpUp == 1) &&
         (rtu_ETCS_CTL_CMD_OLD->lcetcss32ICtlPor <
          rtu_ETCS_REF_TRQ2JMP->lcetcss32RefTrq2Jmp)) &&
        (rtu_ETCS_ESC_SIG->lcetcss16AbsDelYawFrst <
         rtu_ETCS_TAR_SPIN->lcetcss16LmtCrngDltYaw)) {
      /* Transition: '<S1>:29' */
      /* Transition: '<S1>:47' */
      q1 = mul_s32_s32_s32_sat((rtu_ETCS_REF_TRQ2JMP->lcetcss32RefTrq2Jmp -
        rtu_ETCS_CTL_CMD_OLD->lcetcss32ICtlPor) / 100,
        rtu_ETCS_REF_TRQ2JMP->lcetcss16Ftr4JmpUp);
      *rty_lcetcss32IValAtErrZroCrs = rtu_ETCS_CTL_CMD_OLD->lcetcss32ICtlPor +
        q1;
      if ((rtu_ETCS_CTL_CMD_OLD->lcetcss32ICtlPor < 0) && ((q1 < 0) &&
           ((*rty_lcetcss32IValAtErrZroCrs) >= 0))) {
        *rty_lcetcss32IValAtErrZroCrs = MIN_int32_T;
      } else {
        if ((rtu_ETCS_CTL_CMD_OLD->lcetcss32ICtlPor > 0) && ((q1 > 0) &&
             ((*rty_lcetcss32IValAtErrZroCrs) <= 0))) {
          *rty_lcetcss32IValAtErrZroCrs = MAX_int32_T;
        }
      }

      *rty_lcetcsu1RstIValAtErrZroCrs = true;

      /* Transition: '<S1>:94' */
      /* Transition: '<S1>:73' */
    } else {
      /* Transition: '<S1>:67' */
      if ((rtu_ETCS_ERR_ZRO_CRS->lcetcsu1JumpDn == 1) &&
          (rtu_ETCS_CTL_CMD_OLD->lcetcss32ICtlPor >
           rtu_ETCS_REF_TRQ2JMP->lcetcss32RefTrq2Jmp)) {
        /* Transition: '<S1>:74' */
        /* Transition: '<S1>:72' */
        q1 = mul_s32_s32_s32_sat((rtu_ETCS_REF_TRQ2JMP->lcetcss32RefTrq2Jmp -
          rtu_ETCS_CTL_CMD_OLD->lcetcss32ICtlPor) / 100,
          rtu_ETCS_REF_TRQ2JMP->lcetcss16Ftr4JmpDn);
        *rty_lcetcss32IValAtErrZroCrs = rtu_ETCS_CTL_CMD_OLD->lcetcss32ICtlPor +
          q1;
        if ((rtu_ETCS_CTL_CMD_OLD->lcetcss32ICtlPor < 0) && ((q1 < 0) &&
             ((*rty_lcetcss32IValAtErrZroCrs) >= 0))) {
          *rty_lcetcss32IValAtErrZroCrs = MIN_int32_T;
        } else {
          if ((rtu_ETCS_CTL_CMD_OLD->lcetcss32ICtlPor > 0) && ((q1 > 0) &&
               ((*rty_lcetcss32IValAtErrZroCrs) <= 0))) {
            *rty_lcetcss32IValAtErrZroCrs = MAX_int32_T;
          }
        }

        *rty_lcetcsu1RstIValAtErrZroCrs = true;

        /* Transition: '<S1>:73' */
      } else {
        /* Transition: '<S1>:75' */
        *rty_lcetcss32IValAtErrZroCrs = 0;
        *rty_lcetcsu1RstIValAtErrZroCrs = false;
      }
    }
  }

  /* End of Chart: '<Root>/Chart' */
}

/* Model initialize function */
void LCETCS_vCalIValAtErrZroCrs_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
