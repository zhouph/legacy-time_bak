/*
 * File: LCETCS_vCalIVal2Rst.h
 *
 * Code generated for Simulink model 'LCETCS_vCalIVal2Rst'.
 *
 * Model version                  : 1.222
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:13:00 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalIVal2Rst_h_
#define RTW_HEADER_LCETCS_vCalIVal2Rst_h_
#ifndef LCETCS_vCalIVal2Rst_COMMON_INCLUDES_
# define LCETCS_vCalIVal2Rst_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalIVal2Rst_COMMON_INCLUDES_ */

#include "LCETCS_vCalIVal2Rst_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_vSelIVal2Rst.h"
#include "LCETCS_vDctMisIValAtStrt.h"
#include "LCETCS_vCalRefTrqAtVcaOfCtl.h"
#include "LCETCS_vCalIValAtStrtOfCtl.h"
#include "LCETCS_vCalIValAtErrZroCrs.h"
#include "LCETCS_vCalIValAt2ndCylStrt.h"

/* Block signals for model 'LCETCS_vCalIVal2Rst' */
typedef struct {
  int32_T lcetcss32IValAtStrtOfCtl;    /* '<Root>/LCETCS_vCalIValAtStrtOfCtl' */
  int32_T lcetcss32IValAt2ndCylStrt;   /* '<Root>/LCETCS_vCalIValAt2ndCylStrt' */
  int32_T lcetcss32IValAtErrZroCrs;    /* '<Root>/LCETCS_vCalIValAtErrZroCrs' */
  int32_T lcetcss32IVal2Rst;           /* '<Root>/LCETCS_vSelIVal2Rst' */
  boolean_T lcetcsu1RstIValAtErrZroCrs;/* '<Root>/LCETCS_vCalIValAtErrZroCrs' */
  boolean_T lcetcsu1IValRecTorqAct;    /* '<Root>/LCETCS_vDctMisIValAtStrt' */
  boolean_T lcetcsu1RstIVal;           /* '<Root>/LCETCS_vSelIVal2Rst' */
} B_LCETCS_vCalIVal2Rst_c_T;

/* Block states (auto storage) for model 'LCETCS_vCalIVal2Rst' */
typedef struct {
  MdlrefDW_LCETCS_vDctMisIValAtStrt_T LCETCS_vDctMisIValAtStrt_DWORK1;/* '<Root>/LCETCS_vDctMisIValAtStrt' */
  MdlrefDW_LCETCS_vCalRefTrqAtVcaOfCtl_T LCETCS_vCalRefTrqAtVcaOfCtl_DWORK1;/* '<Root>/LCETCS_vCalRefTrqAtVcaOfCtl' */
} DW_LCETCS_vCalIVal2Rst_f_T;

typedef struct {
  B_LCETCS_vCalIVal2Rst_c_T rtb;
  DW_LCETCS_vCalIVal2Rst_f_T rtdw;
} MdlrefDW_LCETCS_vCalIVal2Rst_T;

/* Model reference registration function */
extern void LCETCS_vCalIVal2Rst_initialize(void);
extern void LCETCS_vCalIVal2Rst_Init(B_LCETCS_vCalIVal2Rst_c_T *localB,
  DW_LCETCS_vCalIVal2Rst_f_T *localDW);
extern void LCETCS_vCalIVal2Rst_Start(DW_LCETCS_vCalIVal2Rst_f_T *localDW);
extern void LCETCS_vCalIVal2Rst(const TypeETCSRefTrq2JmpStruct
  *rtu_ETCS_REF_TRQ2JMP, const TypeETCSCtlCmdStruct *rtu_ETCS_CTL_CMD_OLD, const
  TypeETCSRefTrqStrCtlStruct *rtu_ETCS_REF_TRQ_STR_CTL, const
  TypeETCSErrZroCrsStruct *rtu_ETCS_ERR_ZRO_CRS, const TypeETCSCtlActStruct
  *rtu_ETCS_CTL_ACT, const TypeETCSRefTrqStr2CylStruct *rtu_ETCS_REF_TRQ_STR2CYL,
  const TypeETCSCyl1stStruct *rtu_ETCS_CYL_1ST, const TypeETCSVehAccStruct
  *rtu_ETCS_VEH_ACC, const TypeETCSHighDctStruct *rtu_ETCS_HI_DCT, const
  TypeETCSReqVCAStruct *rtu_ETCS_REQ_VCA, const TypeETCSEscSigStruct
  *rtu_ETCS_ESC_SIG, const TypeETCSTarSpinStruct *rtu_ETCS_TAR_SPIN, const
  TypeETCSCrdnTrqStruct *rtu_ETCS_CRDN_TRQ, TypeETCSIValRstStruct
  *rty_ETCS_IVAL_RST, B_LCETCS_vCalIVal2Rst_c_T *localB,
  DW_LCETCS_vCalIVal2Rst_f_T *localDW);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalIVal2Rst'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalIVal2Rst_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
