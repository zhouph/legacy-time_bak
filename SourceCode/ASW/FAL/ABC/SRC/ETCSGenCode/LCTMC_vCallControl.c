/*
 * File: LCTMC_vCallControl.c
 *
 * Code generated for Simulink model 'LCTMC_vCallControl'.
 *
 * Model version                  : 1.389
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:14:41 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCTMC_vCallControl.h"
#include "LCTMC_vCallControl_private.h"

/* Initial conditions for referenced model: 'LCTMC_vCallControl' */
void LCTMC_vCallControl_Init(TypeTMCReqGearStruct *rty_TMC_REQ_GEAR_STRUCT,
  B_LCTMC_vCallControl_c_T *localB, DW_LCTMC_vCallControl_f_T *localDW)
{
  /* InitializeConditions for ModelReference: '<Root>/LCTMC_vCheckGearStatus' */
  LCTMC_vCheckGearStatus_Init(&localB->lctmcu1GearShiftCountSet,
    &localB->lctmcu1NonCndTarReqGear,
    &(localDW->LCTMC_vCheckGearStatus_DWORK1.rtb),
    &(localDW->LCTMC_vCheckGearStatus_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCTMC_vCalReferTarGearMap' */
  LCTMC_vCalReferTarGearMap_Init
    (&(localDW->LCTMC_vCalReferTarGearMap_DWORK1.rtdw));

  /* InitializeConditions for ModelReference: '<Root>/LCTMC_vCtrlTargetGear' */
  LCTMC_vCtrlTargetGear_Init(rty_TMC_REQ_GEAR_STRUCT,
    &(localDW->LCTMC_vCtrlTargetGear_DWORK1.rtdw));
}

/* Start for referenced model: 'LCTMC_vCallControl' */
void LCTMC_vCallControl_Start(DW_LCTMC_vCallControl_f_T *localDW)
{
  /* Start for ModelReference: '<Root>/LCTMC_vCalReferTarGearMap' */
  LCTMC_vCalReferTarGearMap_Start
    (&(localDW->LCTMC_vCalReferTarGearMap_DWORK1.rtdw));
}

/* Output and update for referenced model: 'LCTMC_vCallControl' */
void LCTMC_vCallControl(const TypeETCSGainStruct *rtu_ETCS_CTL_GAINS, const
  TypeETCSGearStruct *rtu_ETCS_GEAR_STRUCT, const TypeETCSExtDctStruct
  *rtu_ETCS_EXT_DCT, const TypeETCSEngStruct *rtu_ETCS_ENG_STRUCT, int16_T
  rtu_lcetcss16VehSpd, const TypeETCSCtlActStruct *rtu_ETCS_CTL_ACT, const
  TypeETCSSplitHillStruct *rtu_ETCS_SPLIT_HILL, boolean_T rtu_lcu1TmcInhibitMode,
  const TypeTMCReqGearStruct *rtu_TMC_REQ_GEAR_STRUCT_OLD, TypeTMCReqGearStruct *
  rty_TMC_REQ_GEAR_STRUCT, B_LCTMC_vCallControl_c_T *localB,
  DW_LCTMC_vCallControl_f_T *localDW)
{
  /* local block i/o variables */
  TypeTMCRefLimVehSpdMap rtb_TMC_REF_LIM_VS_MAP;
  TypeTMCRefFinTarGearMap rtb_TMC_REF_FIN_TAR_GEAR_MAP;

  /* ModelReference: '<Root>/LCTMC_vCheckGearStatus' */
  LCTMC_vCheckGearStatus(rtu_ETCS_CTL_ACT, rtu_lcu1TmcInhibitMode,
    rtu_TMC_REQ_GEAR_STRUCT_OLD, rtu_ETCS_EXT_DCT, rtu_ETCS_GEAR_STRUCT,
    &localB->lctmcu1GearShiftCountSet, &localB->lctmcu1NonCndTarReqGear,
    &(localDW->LCTMC_vCheckGearStatus_DWORK1.rtb),
    &(localDW->LCTMC_vCheckGearStatus_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCTMC_vCalReferTarGearMap' */
  LCTMC_vCalReferTarGearMap(rtu_ETCS_GEAR_STRUCT, rtu_ETCS_CTL_GAINS,
    rtu_ETCS_SPLIT_HILL, rtu_TMC_REQ_GEAR_STRUCT_OLD,
    localB->lctmcu1NonCndTarReqGear, &rtb_TMC_REF_LIM_VS_MAP,
    &rtb_TMC_REF_FIN_TAR_GEAR_MAP,
    &(localDW->LCTMC_vCalReferTarGearMap_DWORK1.rtdw));

  /* ModelReference: '<Root>/LCTMC_vCtrlTargetGear' */
  LCTMC_vCtrlTargetGear(localB->lctmcu1GearShiftCountSet,
                        localB->lctmcu1NonCndTarReqGear, rtu_ETCS_GEAR_STRUCT,
                        &rtb_TMC_REF_LIM_VS_MAP, &rtb_TMC_REF_FIN_TAR_GEAR_MAP,
                        rtu_lcetcss16VehSpd, rtu_ETCS_ENG_STRUCT,
                        rty_TMC_REQ_GEAR_STRUCT,
                        &(localDW->LCTMC_vCtrlTargetGear_DWORK1.rtdw));
}

/* Model initialize function */
void LCTMC_vCallControl_initialize(const rtTimingBridge *timingBridge)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCTMC_vCalReferTarGearMap' */
  LCTMC_vCalReferTarGearMap_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCTMC_vCheckGearStatus' */
  LCTMC_vCheckGearStatus_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCTMC_vCtrlTargetGear' */
  LCTMC_vCtrlTargetGear_initialize(timingBridge);
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
