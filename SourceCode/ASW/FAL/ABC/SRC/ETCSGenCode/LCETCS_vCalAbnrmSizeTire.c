/*
 * File: LCETCS_vCalAbnrmSizeTire.c
 *
 * Code generated for Simulink model 'LCETCS_vCalAbnrmSizeTire'.
 *
 * Model version                  : 1.92
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:00:27 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalAbnrmSizeTire.h"
#include "LCETCS_vCalAbnrmSizeTire_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalAbnrmSizeTire' */
void LCETCS_vCalAbnrmSizeTire_Init(boolean_T *rty_lcetcsu1AbnrmSizeTireDet,
  boolean_T *rty_lcetcsu1AbnrmSizeTireSus, boolean_T *rty_lcetcsu1AbnrmSizeTire)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalDetAbnrmTire' */
  LCETCS_vCalDetAbnrmTire_Init(rty_lcetcsu1AbnrmSizeTireDet);

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalSusAbnrmTire' */
  LCETCS_vCalSusAbnrmTire_Init(rty_lcetcsu1AbnrmSizeTireSus);

  /* InitializeConditions for ModelReference: '<Root>/LCETCS_vCalFinAbnrmTire' */
  LCETCS_vCalFinAbnrmTire_Init(rty_lcetcsu1AbnrmSizeTire);
}

/* Output and update for referenced model: 'LCETCS_vCalAbnrmSizeTire' */
void LCETCS_vCalAbnrmSizeTire(const TypeETCSWhlStruct *rtu_ETCS_FL, const
  TypeETCSWhlStruct *rtu_ETCS_FR, const TypeETCSWhlStruct *rtu_ETCS_RL, const
  TypeETCSWhlStruct *rtu_ETCS_RR, boolean_T *rty_lcetcsu1AbnrmSizeTireDet,
  boolean_T *rty_lcetcsu1AbnrmSizeTireSus, boolean_T *rty_lcetcsu1AbnrmSizeTire)
{
  /* ModelReference: '<Root>/LCETCS_vCalDetAbnrmTire' */
  LCETCS_vCalDetAbnrmTire(rtu_ETCS_FL, rtu_ETCS_FR, rtu_ETCS_RL, rtu_ETCS_RR,
    rty_lcetcsu1AbnrmSizeTireDet);

  /* ModelReference: '<Root>/LCETCS_vCalSusAbnrmTire' */
  LCETCS_vCalSusAbnrmTire(rtu_ETCS_FL, rtu_ETCS_FR, rtu_ETCS_RL, rtu_ETCS_RR,
    rty_lcetcsu1AbnrmSizeTireSus);

  /* ModelReference: '<Root>/LCETCS_vCalFinAbnrmTire' */
  LCETCS_vCalFinAbnrmTire((*rty_lcetcsu1AbnrmSizeTireDet),
    (*rty_lcetcsu1AbnrmSizeTireSus), rty_lcetcsu1AbnrmSizeTire);
}

/* Model initialize function */
void LCETCS_vCalAbnrmSizeTire_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalDetAbnrmTire' */
  LCETCS_vCalDetAbnrmTire_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalFinAbnrmTire' */
  LCETCS_vCalFinAbnrmTire_initialize();

  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_vCalSusAbnrmTire' */
  LCETCS_vCalSusAbnrmTire_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
