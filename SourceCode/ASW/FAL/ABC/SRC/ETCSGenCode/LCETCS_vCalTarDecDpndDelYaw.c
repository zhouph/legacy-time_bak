/*
 * File: LCETCS_vCalTarDecDpndDelYaw.c
 *
 * Code generated for Simulink model 'LCETCS_vCalTarDecDpndDelYaw'.
 *
 * Model version                  : 1.12
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:59:56 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalTarDecDpndDelYaw.h"
#include "LCETCS_vCalTarDecDpndDelYaw_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalTarDecDpndDelYaw' */
void LCETCS_vCalTarDecDpndDelYaw_Init(B_LCETCS_vCalTarDecDpndDelYaw_c_T *localB)
{
  /* InitializeConditions for ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_Init(&localB->y);
}

/* Output and update for referenced model: 'LCETCS_vCalTarDecDpndDelYaw' */
void LCETCS_vCalTarDecDpndDelYaw(int16_T rtu_lcetcss16MaxDltYawInTarDecMd, const
  TypeETCSExtDctStruct *rtu_ETCS_EXT_DCT, int16_T rtu_lcetcss16LmtCrngDltYaw,
  int16_T rtu_lcetcss16FnlTarSpnDec, int16_T *rty_lcetcss16TarDecDpndDelYaw,
  B_LCETCS_vCalTarDecDpndDelYaw_c_T *localB)
{
  /* local block i/o variables */
  int16_T rtb_Switch;

  /* Gain: '<Root>/Gain' incorporates:
   *  Constant: '<Root>/delta yaw offset for minimum target spin'
   */
  rtb_Switch = (int16_T)(10U * ((uint8_T)U8ETCSCpDltYawOffset4MinTrgtSpn));

  /* Sum: '<Root>/Add' */
  rtb_Switch += rtu_lcetcss16LmtCrngDltYaw;

  /* ModelReference: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point(rtu_lcetcss16MaxDltYawInTarDecMd,
                        rtu_lcetcss16LmtCrngDltYaw, rtb_Switch,
                        rtCP_Constant_Value, rtu_lcetcss16FnlTarSpnDec,
                        &localB->y);

  /* Switch: '<Root>/Switch' incorporates:
   *  Constant: '<Root>/Target change factor when banked road is detected'
   */
  if (rtu_ETCS_EXT_DCT->lcetcsu1BankRd) {
    rtb_Switch = 0;
  } else {
    rtb_Switch = localB->y;
  }

  /* End of Switch: '<Root>/Switch' */

  /* Saturate: '<Root>/Saturation' */
  if (rtb_Switch > 0) {
    *rty_lcetcss16TarDecDpndDelYaw = 0;
  } else if (rtb_Switch < -120) {
    *rty_lcetcss16TarDecDpndDelYaw = -120;
  } else {
    *rty_lcetcss16TarDecDpndDelYaw = rtb_Switch;
  }

  /* End of Saturate: '<Root>/Saturation' */
}

/* Model initialize function */
void LCETCS_vCalTarDecDpndDelYaw_initialize(void)
{
  /* Model Initialize fcn for ModelReference Block: '<Root>/LCETCS_s16Inter2Point' */
  LCETCS_s16Inter2Point_initialize();
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
