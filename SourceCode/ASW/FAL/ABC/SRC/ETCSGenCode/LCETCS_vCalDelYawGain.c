/*
 * File: LCETCS_vCalDelYawGain.c
 *
 * Code generated for Simulink model 'LCETCS_vCalDelYawGain'.
 *
 * Model version                  : 1.256
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:52:02 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalDelYawGain.h"
#include "LCETCS_vCalDelYawGain_private.h"

/* Output and update for referenced model: 'LCETCS_vCalDelYawGain' */
void LCETCS_vCalDelYawGain(boolean_T rtu_lcetcsu1DelYawDivrg, int16_T
  rtu_lcetcss16DelYawDivrgGain, int16_T rtu_lcetcss16DelYawCnvrgGain, const
  TypeETCSEscSigStruct *rtu_ETCS_ESC_SIG, const TypeETCSTarSpinStruct
  *rtu_ETCS_TAR_SPIN, int16_T *rty_lcetcss16DelYawGain)
{
  /* Chart: '<Root>/Chart1' */
  /* Gateway: Chart1 */
  /* During: Chart1 */
  /* Entry Internal: Chart1 */
  /* Transition: '<S1>:154' */
  /*  delta yaw is unstable  */
  if (rtu_ETCS_ESC_SIG->lcetcss16AbsDelYawFrst >
      rtu_ETCS_TAR_SPIN->lcetcss16LmtCrngDltYaw) {
    /* Transition: '<S1>:14' */
    /* Transition: '<S1>:19' */
    /*  delta yaw is gettting bigger  */
    if (rtu_lcetcsu1DelYawDivrg == 1) {
      /* Transition: '<S1>:122' */
      /* Transition: '<S1>:124' */
      *rty_lcetcss16DelYawGain = rtu_lcetcss16DelYawDivrgGain;

      /* Transition: '<S1>:166' */
    } else {
      /* Transition: '<S1>:132' */
      *rty_lcetcss16DelYawGain = rtu_lcetcss16DelYawCnvrgGain;
    }

    /* Transition: '<S1>:135' */
  } else {
    /* Transition: '<S1>:48' */
    /*  No adaptation
       Working point will be adpated by spin controller  */
    *rty_lcetcss16DelYawGain = 0;
  }

  /* End of Chart: '<Root>/Chart1' */
  /* Transition: '<S1>:159' */
}

/* Model initialize function */
void LCETCS_vCalDelYawGain_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
