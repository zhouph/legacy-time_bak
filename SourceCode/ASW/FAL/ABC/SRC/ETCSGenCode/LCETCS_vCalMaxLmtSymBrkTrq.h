/*
 * File: LCETCS_vCalMaxLmtSymBrkTrq.h
 *
 * Code generated for Simulink model 'LCETCS_vCalMaxLmtSymBrkTrq'.
 *
 * Model version                  : 1.129
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 18:03:19 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_LCETCS_vCalMaxLmtSymBrkTrq_h_
#define RTW_HEADER_LCETCS_vCalMaxLmtSymBrkTrq_h_
#ifndef LCETCS_vCalMaxLmtSymBrkTrq_COMMON_INCLUDES_
# define LCETCS_vCalMaxLmtSymBrkTrq_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* LCETCS_vCalMaxLmtSymBrkTrq_COMMON_INCLUDES_ */

#include "LCETCS_vCalMaxLmtSymBrkTrq_types.h"

/* Shared type includes */
#include "model_reference_types.h"

/* Child system includes */
#include "LCETCS_s16Inter5Point.h"

/* Block signals for model 'LCETCS_vCalMaxLmtSymBrkTrq' */
typedef struct {
  int16_T y1;                          /* '<Root>/Total Gear Ratio at 5th Gear1' */
  int16_T y2;                          /* '<Root>/Total Gear Ratio at 4th Gear1' */
  int16_T y3;                          /* '<Root>/Total Gear Ratio at 3rd Gear1' */
  int16_T y4;                          /* '<Root>/Total Gear Ratio at 2nd Gear1' */
  int16_T y5;                          /* '<Root>/Total Gear Ratio at 1st Gear1' */
  int16_T y;                           /* '<Root>/LCETCS_s16Inter5Point' */
} B_LCETCS_vCalMaxLmtSymBrkTrq_c_T;

typedef struct {
  B_LCETCS_vCalMaxLmtSymBrkTrq_c_T rtb;
} MdlrefDW_LCETCS_vCalMaxLmtSymBrkTrq_T;

/* Model reference registration function */
extern void LCETCS_vCalMaxLmtSymBrkTrq_initialize(void);
extern void LCETCS_vCalMaxLmtSymBrkTrq_Init(B_LCETCS_vCalMaxLmtSymBrkTrq_c_T
  *localB);
extern void LCETCS_vCalMaxLmtSymBrkTrq_Start(B_LCETCS_vCalMaxLmtSymBrkTrq_c_T
  *localB);
extern void LCETCS_vCalMaxLmtSymBrkTrq(const TypeETCSDrvMdlStruct
  *rtu_ETCS_DRV_MDL, int16_T *rty_lcetcss16MaxLmtSymBrkTrq,
  B_LCETCS_vCalMaxLmtSymBrkTrq_c_T *localB);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'LCETCS_vCalMaxLmtSymBrkTrq'
 */
#endif                                 /* RTW_HEADER_LCETCS_vCalMaxLmtSymBrkTrq_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
