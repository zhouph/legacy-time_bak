/*
 * File: linsearch_s16s16.c
 *
 * Code generated for Simulink model 'LCETCS_vCalRefTrqAtStrtOfCtl'.
 *
 * Model version                  : 1.193
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:54:40 2015
 */

#include "rtwtypes.h"
#include "linsearch_s16s16.h"

int16_T linsearch_s16s16(int16_T u, const int16_T bp[], uint32_T startIndex)
{
  uint32_T bpIdx;

  /* Linear Search */
  for (bpIdx = startIndex; u < bp[bpIdx]; bpIdx--) {
  }

  while (u >= bp[bpIdx + 1U]) {
    bpIdx++;
  }

  return (int16_T)bpIdx;
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
