/*
 * File: LCETCS_vCalVehAccByVehSpd.c
 *
 * Code generated for Simulink model 'LCETCS_vCalVehAccByVehSpd'.
 *
 * Model version                  : 1.487
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:50:21 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalVehAccByVehSpd.h"
#include "LCETCS_vCalVehAccByVehSpd_private.h"

/* Initial conditions for referenced model: 'LCETCS_vCalVehAccByVehSpd' */
void LCETCS_vCalVehAccByVehSpd_Init(DW_LCETCS_vCalVehAccByVehSpd_f_T *localDW)
{
  int32_T i;

  /* InitializeConditions for Delay: '<Root>/Delay' */
  for (i = 0; i < 9; i++) {
    localDW->Delay_DSTATE[i] = 0;
  }

  localDW->CircBufIdx = 0U;

  /* End of InitializeConditions for Delay: '<Root>/Delay' */
}

/* Output and update for referenced model: 'LCETCS_vCalVehAccByVehSpd' */
void LCETCS_vCalVehAccByVehSpd(int16_T rtu_lcetcss16VehSpd, int16_T
  *rty_lcetcss16VehAccByVehSpd, DW_LCETCS_vCalVehAccByVehSpd_f_T *localDW)
{
  /* Product: '<Root>/Product' incorporates:
   *  Delay: '<Root>/Delay'
   *  Sum: '<Root>/Add'
   */
  *rty_lcetcss16VehAccByVehSpd = (int16_T)(((int16_T)(rtu_lcetcss16VehSpd -
    localDW->Delay_DSTATE[localDW->CircBufIdx])) << 1);

  /* Saturate: '<Root>/Saturation' */
  if ((*rty_lcetcss16VehAccByVehSpd) > 200) {
    *rty_lcetcss16VehAccByVehSpd = 200;
  } else {
    if ((*rty_lcetcss16VehAccByVehSpd) < -200) {
      *rty_lcetcss16VehAccByVehSpd = -200;
    }
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Update for Delay: '<Root>/Delay' */
  localDW->Delay_DSTATE[localDW->CircBufIdx] = rtu_lcetcss16VehSpd;
  if (localDW->CircBufIdx < 8U) {
    localDW->CircBufIdx++;
  } else {
    localDW->CircBufIdx = 0U;
  }

  /* End of Update for Delay: '<Root>/Delay' */
}

/* Model initialize function */
void LCETCS_vCalVehAccByVehSpd_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
