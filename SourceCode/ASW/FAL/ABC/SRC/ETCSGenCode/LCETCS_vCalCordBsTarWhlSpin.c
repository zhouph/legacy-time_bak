/*
 * File: LCETCS_vCalCordBsTarWhlSpin.c
 *
 * Code generated for Simulink model 'LCETCS_vCalCordBsTarWhlSpin'.
 *
 * Model version                  : 1.70
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:48:26 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vCalCordBsTarWhlSpin.h"
#include "LCETCS_vCalCordBsTarWhlSpin_private.h"

/* Output and update for referenced model: 'LCETCS_vCalCordBsTarWhlSpin' */
void LCETCS_vCalCordBsTarWhlSpin(int16_T rtu_lcetcss16BsTarWhlSpin, int16_T
  rtu_lcetcss16TarWhlSpinIncLwToSplt, int16_T *rty_lcetcss16CordBsTarWhlSpin)
{
  /* MinMax: '<Root>/MinMax' */
  if (rtu_lcetcss16BsTarWhlSpin >= rtu_lcetcss16TarWhlSpinIncLwToSplt) {
    /* Saturate: '<Root>/Saturation1' */
    *rty_lcetcss16CordBsTarWhlSpin = rtu_lcetcss16BsTarWhlSpin;
  } else {
    /* Saturate: '<Root>/Saturation1' */
    *rty_lcetcss16CordBsTarWhlSpin = rtu_lcetcss16TarWhlSpinIncLwToSplt;
  }

  /* End of MinMax: '<Root>/MinMax' */
}

/* Model initialize function */
void LCETCS_vCalCordBsTarWhlSpin_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
