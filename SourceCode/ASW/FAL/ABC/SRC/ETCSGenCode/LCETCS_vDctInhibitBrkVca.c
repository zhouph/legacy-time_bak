/*
 * File: LCETCS_vDctInhibitBrkVca.c
 *
 * Code generated for Simulink model 'LCETCS_vDctInhibitBrkVca'.
 *
 * Model version                  : 1.597
 * Simulink Coder version         : 8.7 (R2014b) 08-Sep-2014
 * C/C++ source code generated on : Wed Jul 29 17:56:48 2015
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Generic->32-bit Embedded Processor
 * Code generation objectives:
 *    1. MISRA-C:2004 guidelines
 *    2. Execution efficiency
 *    3. ROM efficiency
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "LCETCS_vDctInhibitBrkVca.h"
#include "LCETCS_vDctInhibitBrkVca_private.h"

/* Initial conditions for referenced model: 'LCETCS_vDctInhibitBrkVca' */
void LCETCS_vDctInhibitBrkVca_Init(boolean_T *rty_lcetcsu1InhibitBrkVca,
  DW_LCETCS_vDctInhibitBrkVca_f_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<Root>/Unit Delay6' */
  localDW->UnitDelay6_DSTATE = 0;

  /* InitializeConditions for Chart: '<Root>/Chart1' */
  *rty_lcetcsu1InhibitBrkVca = false;
}

/* Output and update for referenced model: 'LCETCS_vDctInhibitBrkVca' */
void LCETCS_vDctInhibitBrkVca(boolean_T rtu_lcetcsu1ReqVcaBrkTrqActOld, const
  TypeETCSVehAccStruct *rtu_ETCS_VEH_ACC, boolean_T *rty_lcetcsu1InhibitBrkVca,
  DW_LCETCS_vDctInhibitBrkVca_f_T *localDW)
{
  int32_T tmp;

  /* Chart: '<Root>/Chart1' incorporates:
   *  UnitDelay: '<Root>/Unit Delay6'
   */
  /* Gateway: Chart1 */
  /* During: Chart1 */
  /* Entry Internal: Chart1 */
  /* Transition: '<S1>:11' */
  /* comment */
  if (rtu_lcetcsu1ReqVcaBrkTrqActOld == 1) {
    /* Transition: '<S1>:54' */
    /* Transition: '<S1>:165' */
    if (rtu_ETCS_VEH_ACC->lcetcss16RsltntAcc >=
        rtu_ETCS_VEH_ACC->lcetcss16RefRdCfFricAtAsphalt) {
      /* Transition: '<S1>:162' */
      /* Transition: '<S1>:53' */
      tmp = localDW->UnitDelay6_DSTATE + 1;
      if (tmp > 32767) {
        tmp = 32767;
      }

      localDW->UnitDelay6_DSTATE = (int16_T)tmp;

      /* Transition: '<S1>:167' */
    } else {
      /* Transition: '<S1>:168' */
      tmp = localDW->UnitDelay6_DSTATE - 1;
      if (tmp < -32768) {
        tmp = -32768;
      }

      localDW->UnitDelay6_DSTATE = (int16_T)tmp;
    }

    /* Transition: '<S1>:169' */
  } else {
    /* Transition: '<S1>:170' */
    localDW->UnitDelay6_DSTATE = 0;
  }

  /* Transition: '<S1>:159' */
  if (localDW->UnitDelay6_DSTATE >= 5) {
    /* Transition: '<S1>:172' */
    /* Transition: '<S1>:176' */
    *rty_lcetcsu1InhibitBrkVca = true;

    /* Transition: '<S1>:177' */
  } else {
    /* Transition: '<S1>:174' */
    *rty_lcetcsu1InhibitBrkVca = false;
  }

  /* End of Chart: '<Root>/Chart1' */

  /* Saturate: '<Root>/Saturation' */
  /* Transition: '<S1>:179' */
  if (localDW->UnitDelay6_DSTATE > 5) {
    /* Update for UnitDelay: '<Root>/Unit Delay6' */
    localDW->UnitDelay6_DSTATE = 5;
  } else {
    if (localDW->UnitDelay6_DSTATE < 0) {
      /* Update for UnitDelay: '<Root>/Unit Delay6' */
      localDW->UnitDelay6_DSTATE = 0;
    }
  }

  /* End of Saturate: '<Root>/Saturation' */
}

/* Model initialize function */
void LCETCS_vDctInhibitBrkVca_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
