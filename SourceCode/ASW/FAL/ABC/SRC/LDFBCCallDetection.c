/*******************************************************************************
* Project Name:     MGH40_ESP
* File Name:        LDFBCCallDetection.c
* Description:      Detection Disk Overheating and Fading of FBC
* Logic version:    HV26
********************************************************************************
*  Modification Log
*  Date         Author          Description
*  -----        -----           -----
*  5C15         eslim           Initial Release
*  6113         eslim           Detecting Fading Module Adding
*  6216         eslim           Modified based on MISRA rule
*  C520         jkLee           Delete useless code
********************************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDFBCCallDetection
	#include "Mdyn_autosar.h"
#endif
/* Includes ********************************************************************/
#include "LDFBCCallDetection.h"
#include "LCFBCCallControl.h"
#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
#include "LSESPFilterEspSensor.h"
#include "LSESPCalSensorOffset.h"
#endif

#if __FBC
//#include "../Failsafe/F_VariableLinkSet.h"

#include "LDESPEstDiscTemp.h"	

/* Logcal Definiton ************************************************************/


/* Variables Definition ********************************************************/

    uint8_t fbc_fading_decel_count;
    uint8_t FBC_ACTIVE;
    int16_t FBC_decel;         
    int16_t FBC_decelbuffer[10];     
    int16_t FBC_decel_buffer_index;  
    int16_t FBC_decel_sum;           
    uint8_t FBC_decel_buffer_Full_Flag;   

 

/* Local Function prototype ****************************************************/
void LDFBC_vCallDetection(void);
void LDFBC_vChkCondition(void);
void LDFBC_vDctVehicleFading(void);

/* Implementation **************************************************************/

/*******************************************************************************
* FUNCTION NAME:        LDFBC_vCallDetection
* CALLED BY:            LD_vCallMain()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Detection of FBC
********************************************************************************/
void LDFBC_vCallDetection(void)
{
    LDFBC_vChkCondition();
    LDFBC_vDctVehicleFading();
}


/*******************************************************************************
* FUNCTION NAME:        LDFBC_vChkCondition
* CALLED BY:            LDFBC_vCallDetection()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Check Entering Condition of FBC
********************************************************************************/
void LDFBC_vChkCondition(void)
{
  #if (__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
    esp_tempW1 =  LCESP_s16IInter3Point(lsesps16EstBrkPressByBrkPedalF, 
                                                S16_BRAKE_Low_MCPRESS , (int16_t)U8_BRAKE_Low_Decel,
        							            S16_BRAKE_Med_MCPRESS , (int16_t)U8_BRAKE_Med_Decel,  
        							            S16_BRAKE_High_MCPRESS , (int16_t)U8_BRAKE_High_Decel);
  #else  
    esp_tempW1 =  LCESP_s16IInter3Point(mpress, S16_BRAKE_Low_MCPRESS , (int16_t)U8_BRAKE_Low_Decel,
        							            S16_BRAKE_Med_MCPRESS , (int16_t)U8_BRAKE_Med_Decel,  
        							            S16_BRAKE_High_MCPRESS , (int16_t)U8_BRAKE_High_Decel);
  #endif
 
 #if __REAR_D
    esp_tempW2 = btc_tmp_rl;
    esp_tempW3 = btc_tmp_rr;
 #else
    esp_tempW2 = btc_tmp_fl;
    esp_tempW3 = btc_tmp_fr;
 
 #endif
    /* Hardware Error check */
  #if (__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
    if((lsespu1PedalBrakeOn==0)
  #else
    if((MPRESS_BRAKE_ON==0)
  #endif	
      ||(fu1ESCEcuHWErrDet==1)||(fu1MCPErrorDet==1)||(fu1MCPSusDet==1)
      ||(fu1WheelFLErrDet==1)||(fu1WheelFRErrDet==1)||(fu1WheelRLErrDet==1)||(fu1WheelRRErrDet==1))
    {
        FBC_ACTIVE = 0;
        fbc_fading_decel_count = 0;
/*      LCFBC_vInitializeVariables(); */
    }
    else
    {

	    /* Disk Thermal Load check */
	
		if(S16_BRAKE_DISK_FADE_TEMPERATURE >S16_BRAKE_DISK_MONITOR_TEMPERATURE)
		{
			esp_tempW5 = S16_BRAKE_DISK_MONITOR_TEMPERATURE+3200;
			if(esp_tempW5 > S16_BRAKE_DISK_FADE_TEMPERATURE) 
			{
				esp_tempW5 = S16_BRAKE_DISK_FADE_TEMPERATURE;
				esp_tempW6 = S16_BRAKE_DISK_FADE_TEMPERATURE;
				esp_tempW7 = S16_BRAKE_DISK_FADE_TEMPERATURE;
			}
			else 
			{
				esp_tempW5 = S16_BRAKE_DISK_MONITOR_TEMPERATURE+3200;
				esp_tempW6 = S16_BRAKE_DISK_MONITOR_TEMPERATURE+6400;
				if(esp_tempW6 >S16_BRAKE_DISK_FADE_TEMPERATURE)	esp_tempW6 = S16_BRAKE_DISK_FADE_TEMPERATURE;					
				esp_tempW7 = S16_BRAKE_DISK_FADE_TEMPERATURE;			

			}
		}
		else 
		{
			esp_tempW5 = S16_BRAKE_DISK_FADE_TEMPERATURE;
			esp_tempW6 = S16_BRAKE_DISK_FADE_TEMPERATURE;
			esp_tempW7 = S16_BRAKE_DISK_FADE_TEMPERATURE;
		}
	    
	  #if (__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
	    if((lsespu1PedalBrakeOn ==1)&&(FBC_ACTIVE==0)&&(ABS_fz==0)&&(ebd_refilt_grv >= (-esp_tempW1))
	    	&&(ebd_refilt_grv < (-10))&&(vrefk >= S16_FBC_SPEED_ENTER_THR)
	    	&&(lsesps16EstBrkPressByBrkPedalF >= S16_BRAKE_LOWMU_ABSPRESS)&&(lsespu1BrkAppSenInvalid == 0))
	  #else
	    if((MPRESS_BRAKE_ON ==1)&&(FBC_ACTIVE==0)&&(ABS_fz==0)&&(ebd_refilt_grv >= (-esp_tempW1))
	    	&&(ebd_refilt_grv < (-10))&&(vrefk >= S16_FBC_SPEED_ENTER_THR)&&(mpress >= S16_BRAKE_LOWMU_ABSPRESS))
	  #endif  
	    {
	    	if((esp_tempW2>=esp_tempW7)&&(esp_tempW3>=esp_tempW7) )
	    	{
	    		fbc_fading_decel_count = fbc_fading_decel_count + 10;
	    	}
	    	else if((esp_tempW2>=esp_tempW6)&&(esp_tempW3>=esp_tempW6) )
	    	{
	    		fbc_fading_decel_count = fbc_fading_decel_count + 3;
	    	}
	    	else if((esp_tempW2>=esp_tempW5)&&(esp_tempW3>=esp_tempW5) )
	    	{
	    		fbc_fading_decel_count = fbc_fading_decel_count + 2;
	    	}
	    	else if((esp_tempW2>=S16_BRAKE_DISK_MONITOR_TEMPERATURE)&&(esp_tempW3>=S16_BRAKE_DISK_MONITOR_TEMPERATURE) )
	    	{
	    		 if(mpress_slop_filt > (int16_t)((int16_t)U16_PBA_PRESS_RISE_RATE_THR2))
	    		{
	    			fbc_fading_decel_count= fbc_fading_decel_count + 2;
	    		}
	    		else 
	    		{
	    			fbc_fading_decel_count= fbc_fading_decel_count + 1;
	    		}
	    	}
/*	    	else if(mpress_slop_filt > 1 )
	    	{
	    		fbc_fading_decel_count= fbc_fading_decel_count + 1;
	    	} */
	    	else
	    	{
	    		;/*fbc_fading_decel_count = 0;*/	
	    	}
	    						 
	    }
	    else if((FBC_ACTIVE==1)&&(vrefk <= S16_FBC_SPEED_EXIT_THR))
	    {
	    	FBC_ACTIVE = 0;
	    	fbc_fading_decel_count = 0;
	    }
	    else
	    {
	        fbc_fading_decel_count = 0;
	    }
	    
	    if(fbc_fading_decel_count >= U8_FBC_FADING_DCT_COUNT_MAX)
	    {
	       	FBC_ACTIVE = 1;
	       	fbc_fading_decel_count = 0;
	    }		
	    else
	    {
	       	;
	    }
	}
   
    
    /* High Coefficient Surface check */
    /* to be continued...   */


}

/*******************************************************************************
* FUNCTION NAME:        LDFBC_vDctVehicleFading
* CALLED BY:            LDFBC_vCallDetection()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Detect Vehicle Decel and Fading of FBC
********************************************************************************/
void LDFBC_vDctVehicleFading(void)
{

    FBC_decel = ebd_refilt_grv;

    if(FBC_dct_cnt>=L_U8_TIME_10MSLOOP_140MS)	/* 10ms Task */
    {
        if (FBC_decel_buffer_Full_Flag==0)
        {
            FBC_decel_sum = FBC_decel_sum + (FBC_decel);
        }
        else
        {
            FBC_decel_sum = FBC_decel_sum + (FBC_decel-FBC_decelbuffer[FBC_decel_buffer_index]);

        }

        FBC_decelbuffer[FBC_decel_buffer_index]=FBC_decel;

        FBC_decel_buffer_index++;

        if (FBC_decel_buffer_index>9)
        {
            FBC_decel_buffer_index = 0;
            FBC_decel_buffer_Full_Flag =1;
        }
        else
        {
            ;
        }
    }
    else
    {
        FBC_decel_sum = 0;
        FBC_decel_buffer_index=0;
        FBC_decel_buffer_Full_Flag =0;
    }
}
#endif
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDFBCCallDetection
	#include "Mdyn_autosar.h"
#endif    
