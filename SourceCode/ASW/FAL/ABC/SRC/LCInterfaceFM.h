#ifndef __LCInterfacFM_H__
#define __LCInterfacFM_H__

#include "LVarHead.h"


extern U8_BIT_STRUCT_t         FM00;
extern U8_BIT_STRUCT_t         FM01;
extern U8_BIT_STRUCT_t         FM02;
extern U8_BIT_STRUCT_t         FM03;
                                   
#define  lcu1EbdCtrlFail           FM00.bit0
#define  lcu1AbsCtrlFail           FM00.bit1
#define  lcu1CbcCtrlFail           FM00.bit2
#define  lcu1EdcCtrlFail           FM00.bit3
#define  lcu1TcsCtrlFail           FM00.bit4
#define  lcu1EscCtrlFail           FM00.bit5
#define  lcu1HbbCtrlFail           FM00.bit6
#define  lcu1HdcCtrlFail           FM00.bit7
                                   
#define  lcu1HsaCtrlFail           FM01.bit0
#define  lcu1VsmCtrlFail           FM01.bit1
#define  lcu1SccCtrlFail           FM01.bit2
#define  lcu1CdmCtrlFail           FM01.bit3
#define  lcu1AvhCtrlFail           FM01.bit4
#define  lcu1PbaCtrlFail           FM01.bit5
#define  lcu1BdwCtrlFail           FM01.bit6
#define  lcu1EssCtrlFail           FM01.bit7
                                   
#define  lcu1TvbbCtrlFail          FM02.bit0
#define  lcu1TspCtrlFail           FM02.bit1
#define  lcu1RopCtrlFail           FM02.bit2
#define  lcu1EcdCtrlFail           FM02.bit3
#define  lcu1TodCtrlFail           FM02.bit4
#define  lcu1AebCtrlFail           FM02.bit5
#define  lcu1AdcCtrlFail           FM02.bit6
#define  lcu1TcsDiscTempErrFlg     FM02.bit7
                                   
#define  lcu1HdcDiscTempErrFlg     FM03.bit0
#define  lcu1SccDiscTempErrFlg     FM03.bit1
#define  lcu1TvbbDiscTempErrFlg    FM03.bit2
#define  lcu1SlsCtrlFail           FM03.bit3
#define  lcu1FMreserved03_04       FM03.bit4
#define  lcu1FMreserved03_05       FM03.bit5
#define  lcu1FMreserved03_06       FM03.bit6
#define  lcu1FMreserved03_07       FM02.bit7



#endif
