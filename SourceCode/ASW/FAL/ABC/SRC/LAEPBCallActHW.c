/* Includes ********************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START                       
	#define idx_FILE	idx_CL_LAEPBCallActHW
	#include "Mdyn_autosar.h"               
#endif


#include "LAEPBCallActHW.h"
  #if __EPB_INTERFACE
#include "LADECCallActHW.h"
#include "LCEPBCallControl.h"
#include "Hardware_Control.h"
#include "LCESPCalInterpolation.h"
#include "hm_logic_var.h"
#include "LCDECCallControl.h"
   #if __ADVANCED_MSC
#include "LAABSCallMotorSpeedControl.h"
   #endif
  #endif
/* Local Definition  ***********************************************/

/* Variables Definition*********************************************/
  #if __EPB_INTERFACE
 
static uint8_t	lcu8EpbValveMode;
uint8_t	lcu8EpbMotorMode;
 

EPB_VALVE_t	laEpbValveP, laEpbValveS;
  #endif

/* Local Function prototype ****************************************/
  #if __EPB_INTERFACE
   #if __TCMF_CONTROL
static void LAEPB_vOriginalValveCmd(void);
   #endif
int16_t LAMFC_s16SetMFCCurrentEPB(EPB_VALVE_t *pEpb, int16_t MFC_Current_old);   
  #endif

/* Implementation***************************************************/
  #if __EPB_INTERFACE
void LAEPB_vCallActHW(void)
{
	if(lcu1EpbActiveFlg==1)
	{
	  #if !__TCMF_CONTROL
		HV_VL_fl=lcu8DecFlNoValve;
		AV_VL_fl=lcu8DecFlNcValve;
		HV_VL_fr=lcu8DecFrNoValve;
		AV_VL_fr=lcu8DecFrNcValve;
		HV_VL_rl=lcu8DecRlNoValve;
		AV_VL_rl=lcu8DecRlNcValve;
		HV_VL_rr=lcu8DecRrNoValve;
		AV_VL_rr=lcu8DecRrNcValve;
	  #endif
		  #if __SPLIT_TYPE		/* FR Split */
		S_VALVE_PRIMARY=lcu8DecFlFrEsvValve;
		S_VALVE_SECONDARY=lcu8DecRlRrEsvValve;
		TCL_DEMAND_PRIMARY=lcu8DecFlFrTcValve;
		TCL_DEMAND_SECONDARY=lcu8DecRlRrTcValve;
		  #else				/* X Split */
		S_VALVE_RIGHT=lcu8DecFrRlEsvValve;
		S_VALVE_LEFT=lcu8DecFlRrEsvValve;
		TCL_DEMAND_fr=lcu8DecFrRlTcValve;
		TCL_DEMAND_fl=lcu8DecFlRrTcValve;
		  #endif
		lcu8EpbValveMode=lcu8DecValveMode;
		lcu8EpbMotorMode=lcu8DecMotorMode;
	}
	else
	{
		;
	}
	  #if __TCMF_CONTROL
	LAEPB_vOriginalValveCmd();
	  #endif
}


  #if __TCMF_CONTROL
void LAEPB_vOriginalValveCmd(void)
{
	if(lcu8EpbValveMode==U8_EPB_HOLD)
	{
		EPB_HV_VL_fl=1;
		EPB_AV_VL_fl=0;
		EPB_HV_VL_fr=1;
		EPB_AV_VL_fr=0;
		EPB_HV_VL_rl=1;
		EPB_AV_VL_rl=0;
		EPB_HV_VL_rr=1;
		EPB_AV_VL_rr=0;
	}
	else if(lcu8EpbValveMode==U8_EPB_APPLY)
	{
		EPB_HV_VL_fl=0;
		EPB_AV_VL_fl=0;
		EPB_HV_VL_fr=0;
		EPB_AV_VL_fr=0;
		EPB_HV_VL_rl=0;
		EPB_AV_VL_rl=0;
		EPB_HV_VL_rr=0;
		EPB_AV_VL_rr=0;
	}
	else if(lcu8EpbValveMode==U8_EPB_DUMP)
	{
		EPB_HV_VL_fl=1;
		EPB_AV_VL_fl=1;
		EPB_HV_VL_fr=1;
		EPB_AV_VL_fr=1;
		EPB_HV_VL_rl=1;
		EPB_AV_VL_rl=1;
		EPB_HV_VL_rr=1;
		EPB_AV_VL_rr=1;
	}
	else
	{
		;
	}
}
  #endif

int16_t LAMFC_s16SetMFCCurrentEPB(EPB_VALVE_t *pEpb, int16_t MFC_Current_old)
{
    int16_t     MFC_Current_new = 0;
    if(lcu1EpbActiveFlg==1)
    {
        if(MFC_Current_old==0)
        {
            MFC_Current_new=450;
        }
        else
        {
            /********************************************************/
            if((pEpb->lau1EpbWL1No==0)&&(pEpb->lau1EpbWL1Nc==0))            /* REAPPLY */
            {
                tempW0=abs(lcs16EpbTargetG*5)+550;
                if(MFC_Current_old>=tempW0)
                {
                    MFC_Current_new=tempW0;
                }
                else
                {
                    MFC_Current_new=MFC_Current_old+3;
                }
                }
                /********************************************************/
                else if((pEpb->lau1EpbWL1No==1)&&(pEpb->lau1EpbWL1Nc==0))       /* HOLD */
                {
                    MFC_Current_new=MFC_Current_old;
            }
            /********************************************************/
            else if((pEpb->lau1EpbWL1No==1)&&(pEpb->lau1EpbWL1Nc==1))       /* DUMP */
            {
                if(MFC_Current_old>200)
                {
                    if(lcs8DecState==S8_DEC_CLOSING)
                    {
                        MFC_Current_new=MFC_Current_old-(int16_t)(lcs16DecClosingCur/L_U8_TIME_10MSLOOP_200MS);
                    }
                    else
                    {
                        MFC_Current_new=MFC_Current_old-3;
                    }
                }
                else
                {
                    MFC_Current_new=200;
                }
            }
            /********************************************************/
            else                                /* Free Circulation */
            {
                MFC_Current_new = MFC_Current_old;
            }
        }
    }
    else
    {
        MFC_Current_new=LAMFC_s16SetMFCCurrentAtTCOff(0, MFC_Current_old);
    }

  return MFC_Current_new;
}

#if __ADVANCED_MSC
void LCMSC_vSetEPBTargetVoltage(void)
{
	if(lcu1EpbActiveFlg==1)
	{
		epb_msc_target_vol=lcs16DecMscTargetVol;
		EPB_MSC_MOTOR_ON=1;
	}
	else
	{
		epb_msc_target_vol=0;
		EPB_MSC_MOTOR_ON=0;
	}
}
   #endif
  #endif
  
  
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LAEPBCallActHW
	#include "Mdyn_autosar.h"               
#endif  
