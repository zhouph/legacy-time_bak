 /***************************************************************************

*  Program ID: MGH-60 DDS                                          
*  Program description:. Deflation Detection System                   
*  Input files:             
*  Output files:      
*                      
*                                           
*  Special notes: none                                          
***************************************************************************
*  Modification Log                                               
*  Date        Author           Description                      
*  ---         ------           ---------                         
*  07.07.30    Sohyun Ahn       Initial Release                   
*Includes *****************************************************************/

#include "hm_logic_var.h"
#include "LDDDSCallDetection.H"
#include "LDDDSCallDetection_AP.H"

#include "LSABSCallSensorSignal.h"
#include "LDABSCallDctForVref.H"
#include "LDABSCallVrefEst.H"
#include "LDABSCallEstVehDecel.H"
#include "LDABSCallVrefMainFilterProcess.H"

#include "LDRTACallDetection.h"

#if __VDC
#include "LDABSCallVrefCompForESP.h"
#endif




#include "DDSdef.par"

#if (__DDS_PLUS == ENABLE)
#include "LDDDSPlusCalDetection.h"
#endif

//#include "../CAN/TestCarCan/CCDrvHMCCan.h"

#if (__DDS == ENABLE)
/* Variables & Flags*********************************************************/
#if __ECU==ESP_ECU_1
//#pragma DATA_SEG __GPAGE_SEG PAGED_RAM
#endif
uint8_t	dds_temp_flg;
uint8_t DDS_flags;
int16_t DDS_max[4];
int16_t DDS_min[4];	  

uint32_t ldddsu32SumDiffForCalib[4][5];

uint8_t ldddsu8CompleteCalib[5];
int16_t ldddss16SumDiffArrayCalib[4][10][5];
int16_t ldddss16CalibRest[4][5];
int16_t ldddss16CumulativeMean[4][5];
int16_t ldddss16DetCalibValue[4][5];
int16_t WheelDiffMinMax[4][5];

int16_t ldddss16SumAxleDiffArrayDct[4][5][3];
int16_t ldddss16SumSideDiffArrayDct[4][5][3];
int16_t ldddss16SumDiagDiffArrayDct[4][5][3];
int16_t ldddss16AxleDiffRunningAvgBuf[4][5][3];
int16_t ldddss16SideDiffRunningAvgBuf[4][5][3];
int16_t ldddss16DiagDiffRunningAvgBuf[4][5][3];

int16_t ldddss16AxleDiffRunningAvg[4][3];
int16_t ldddss16SideDiffRunningAvg[4][3];
int16_t ldddss16DiagDiffRunningAvg[4][3];
int16_t ldddss16Dct1WhlDefCnt[4][5];
int8_t ldddss8PressEstByAxleDiff[4]; 
int8_t ldddss8PressEstBySideDiff[4]; 
int8_t ldddss8PressEstByDiagDiff[4]; 
uint8_t ldddsu8SingleWhlDeflation[4];
int16_t AxleDiffMinMax[4];
int16_t SideDiffMinMax[4];
int16_t DiagDiffMinMax[4];



uint8_t ldddsu8CalcDctTimer[3];
uint8_t ldddsu8CalcDctTimer2[3];
uint8_t v_inx;
uint8_t v_inx1;
uint8_t v_inx2;


#if __2WHL_Deflation_Detect
int16_t ldddss16DctFLRRDefCnt;
int16_t ldddss16DctFRRLDefCnt;
#endif

int16_t CalibrationValueFL;
int16_t CalibrationValueFR;
int16_t CalibrationValueRL;
int16_t CalibrationValueRR;

int16_t ldddss16TireWereInMileageLeft;
int16_t ldddss16TireWereInMileageRight;
int16_t ldddss16RollingCntLeft;
int16_t ldddss16RollingCntRight;
uint8_t ldddsu8SumOfEdgeLeft;
uint8_t ldddsu8SumOfEdgeRight;

		    	
uint16_t ldddsu16DDSResetSustainCnt;	

int8_t ldddss8TPMSWheelIndex;
int8_t ldddss8TPMSTireTemperature;
int8_t ldddss8TPMSTirePressure;

int8_t ldddss8TireTempFromTPMS[4];
int8_t ldddss8TirePressueFromTPMS[4];

uint8_t ldu8DDSMode;
uint8_t ldu8DDSModeOld;
uint8_t ldu8DDSSubMode;
uint8_t ldddsu8SpeedRangeIndex;
uint8_t ldddsu8CalibSamplingDistCnt[4][5];
int16_t  ldddss16DDSCalibTemp[4];
uint8_t ldddsu8CalcCalibArrayCnt[5];
uint16_t ldddsu16CntCalibTotalRev[5];
uint8_t ldddsu8DctSamplingDistCnt[4][3];
uint32_t ldddsu32SumDctInput[4][3];
uint32_t ldddsu32CorrectedWheelInput[4];


	
uint8_t ldu8DDSModeEepromValue;
uint8_t ldddsu8CalibSpeedIndex;

int16_t ldddss16AxleDiffThr[5][2];
int16_t ldddss16SideDiffThr[5][2];
int16_t ldddss16DiagDiffThr[5][2];


uint32_t	ldu32DDSCalibRef;
int32_t temp_sum[4];
uint32_t ldddsu32SumDiffForCalibTemp[4][5];
uint8_t v_inx_temp;
uint8_t ldddsu8DdsCalibWhlInx[5];

uint8_t ldddsu8DdsDctUpdateOkFlg[4];
int32_t temp_sum_Axle[4];
int32_t temp_sum_Side[4];
int32_t temp_sum_Diag[4];
uint8_t ldddsu8DdsDctWhlInx[3];	
uint8_t ldddsu1DdsDctUpdateFlag[3];	
uint8_t ldddsu8RunningAvgUpdateWhlIndex[3];
uint8_t ldddsu1DDSCalibUpdateFlag[5];

#if __3WHL_Deflation_Detect
uint8_t ldddsu8Other3whlDeflation[4];
int16_t   ldddss16Dct3WhlDefCnt[4][2];
#endif

#if __Hybrid_DDS
uint8_t ldddsu8TpmsSnrLocation=FR_WL;// TPM Sensor Location
int16_t ldddshs16RefTirePressure;
int16_t ldddshs16RefTireTemperature;
uint8_t ldddshu8WhlInxAxle;
uint8_t ldddshu8WhlInxSide;
uint8_t ldddshu8WhlInxDiag;
int16_t ldddshs16InitTpmsPressure;
int16_t ldddshs16CurrentTpmsPressure;
int8_t ldddshs8DefbyTpmsSensor;
int8_t ldddshs8DefbyAxleDiffWithTpSnr;
int8_t ldddshs8DefbySideDiffWithTpSnr;
int8_t ldddshs8DefbyDiagDiffWithTpSnr;
uint8_t ldddshu8DefDctCntByTpmsSnr;
uint8_t ldddshu8DefDctCntByAxleDiff;
uint8_t ldddshu8DefDctCntBySideDiff;
uint8_t ldddshu8DefDctCntByDiagDiff;
#endif

uint8_t ldddsu8DeflationStateFlags;

int16_t ldddss16DdsRunningResetCnt[4];

	int16_t ldddss16WheelSpdDiffRatioFrt; 
	int16_t ldddss16WheelSpdDiffRatioRear;
	int16_t ldddss16RevTimeMinMaxDiffRatio;
	int16_t	ldddss16VradMinMaxDiffRatio;
	
//	int16_t  temp_avg[4];
//    int32_t temp_var_sum[4];
//    uint16_t temp_var[4];
	uint8_t GoodC_Flag;

	uint8_t ldddsu8LoggingWheelIndex;
	uint8_t ldddsu8LoggingBufferOffset;
	uint8_t 	ldddsu8AccelPedalReleaseCnt;
#if __ECU==ESP_ECU_1	
//#pragma DATA_SEG DEFAULT
#endif

#if !defined __MATLAB_COMPILE

extern uint8_t 		com_rx_buf[8];
extern uint16_t mbu16MeasureDdsCycleTimeStart;
extern uint16_t mbu16MeasureDdsCycleTime;

extern uint8_t ldu8TPMSBuffer[8];
extern uint8_t TPMS_ISR_ISR_Flg,TPMS_ISR_COPY_Flg,TPMS_ISR_COPY_Flg2;

#else
uchar16_t    ldddsu16ElapsedTimePerRev[4];
uint8_t 	 ldddsu8ElapsedTimeUpdateFlg[4];
uint8_t lddu1WheelDataStroedStart;

#endif
//#if __ECU==ESP_ECU_1
//#pragma DATA_SEG __GPAGE_SEG PAGED_RAM
//#endif
/*
extern uchar16_t u16TirePerPulseFL[60];
extern uchar16_t u16TirePerPulseFR[60];
extern uchar16_t u16TirePerPulseRL[60];
extern uchar16_t u16TirePerPulseRR[60];
extern uchar16_t u16TirePerPulseWL[4][60];
*/
//#if __ECU==ESP_ECU_1	
//#pragma DATA_SEG DEFAULT
//#endif

U8_BIT_STRUCT_t      DDS_1, DDS_2 ,DDS_3,DDS_4;
U8_BIT_STRUCT_t      DDS_READ_OK_FLG1, DDS_READ_OK_FLG2,DDS_READ_OK_FLG3,DDS_READ_OK_FLG4,DDS_READ_OK_FLG5,DDS_READ_OK_FLG6,DDS_READ_OK_FLG7,DDS_READ_OK_FLG8;
U8_BIT_STRUCT_t      DDS_WR_OK_FLG1, DDS_WR_OK_FLG2,DDS_WR_OK_FLG3,DDS_WR_OK_FLG4,DDS_WR_OK_FLG5,DDS_WR_OK_FLG6,DDS_WR_OK_FLG7,DDS_WR_OK_FLG8;

#if __Hybrid_DDS
U8_BIT_STRUCT_t DDSh_1,DDSh_2;;
#endif

#define CNT_50		0
#define CNT_20		1
#define CNT_10_1	2	
#define CNT_10_2	3
#define CNT_10_3	4

 
#define DEF_0			0     
#define DEF_10			10     
#define DEF_25			25     
#define DEF_50			50     
#define DEF_100         100	      

#define Def_Inx_0			0   
#define Def_Inx_10			1   
#define Def_Inx_25			2   
#define Def_Inx_50			3   
#define Def_Inx_100         4	

   

/**************************************************************************/
void 	LDDDS_vCallDetection(void);
void 	LSDDS_vReadEEPROMDDSData(void);
void 	LSDDS_vWriteEEPROMDDSData(void);  
 
void	LDDDS_vClearDDSCalibParameters(void);
void	LDDDS_vClearDDSDetectParameters(void);   
void	LDDDS_vTireWearinRoutine(void);

void 	LDDDS_vDctSteadyStraightState(void);    
void 	LDDDS_vDetDDSSpeedRange(void);
void 	LDDDS_vCalcDDSCalibSpdRange(uint8_t v_inx);
void 	LDDDS_vSumDDSCalibInputSpdRange(uint8_t WHL ,uint8_t v_inx);
void 	LDDDS_vLearnDDSCalibration(void); 

void 	LDDDS_CalcDDSEachWlParameter(uint8_t WHL , uint8_t v_inx);
void 	LDDDS_vDetCalibCompletion(void);
void 	LDDDS_vCalibrateWheelSpeed(void);
void 	LDDDS_vCalcDDSDctPara(void);
void 	LDDDS_vDct1WhlDeflationCnt(void);
void 	LDDDS_vSetTyreDefEstPara(void);
void 	LDDDS_vEstTireDefPress(void);
void LDDDS_vDecDDSDetectCnt(int16_t Diff_Input,uint8_t WHL);
void 	LDDDS_vDet1WhlDeflationState(uint8_t WHL,uint8_t WHL_2 ,uint8_t WHL_3,uint8_t WHL_4);
	#if __2WHL_Deflation_Detect
void 	LDDDS_vDct2WhlDeflation(void);
	#endif

	#if __3WHL_Deflation_Detect
void 	LDDDS_vDct3WhlDeflation(void);
void 	LDDDS_vDct3WhlDeflationEachWhl(uint8_t WHL);
void 	LDDDS_vDet3WhlDeflationState(uint8_t WHL,uint8_t WHL_2 ,uint8_t WHL_3,uint8_t WHL_4);
	#endif	    
	
void 	LDDDS_vDetDeflationState(void);  
void	LSDDS_vCallDDSLogData(void);
void 	LDDDS_vDetDDSRunningReset(void);
void 	LDDDS_vInitializeDctVariable(uint8_t WHL);
void 	LDDDS_vDctRunningResetEachWhl(uint8_t WHL);
void 	LDDDS_vDetCalibCompletionBySpd(uint8_t v_inx_t, int16_t calib_dist );
int16_t 	LDDDS_vCalcWhlSpdDiffRatio(uint32_t input_1 ,uint32_t  input_2);

void 	LDDDS_vReadTPMSData(void);

void 	LDDDS_vUpdateTimeWheelRev(void);
void 	LDDDS_vDetDDSMode(void);
extern uint16_t LSESP_u16WriteEEPROM(uint16_t addr,uint16_t data);
extern uint8_t WE_u8ReadEEP_Byte(uint16_t ee_address, uint8_t *ch_ptr, uint16_t number);
int 	LCDDS_s16IInter5Point( int input_x, int x_1, int y_1, int x_2, int y_2, int x_3, int y_3, int x_4, int y_4, int x_5, int y_5 );

void 	LDDDS_vCalcDDSCalibEachWheel(uint8_t WHL ,uint8_t v_inx);
void 	LDDDS_vCalcDDSCalibCumulMean(uint8_t WHL ,uint8_t v_inx);
void 	LDDDS_vCalcDDSDctParaEachWhl(uint8_t WHL , uint8_t v_inx_t ,int16_t Axle_Diff ,int16_t Side_Diff ,int16_t Diag_Diff);
void 	LDDDS_vEstTireDefPressEachWhl(uint8_t WHL);
void 	LDDDS_vDct1WhlDeflationEachWhl(uint8_t WHL);
void	LSDDS_vCallDDSPublicRoadLogData(void);
void	LSDDS_vCallDDSPlusLogData(void);
void    LSDDS_vCallHybridDDSLogData(void);
//void	LSDDS_vCallDDSPlusLogDataWL(uint8_t WHL,uint8_t addr,uint8_t BufferOffset);
void    LDDDS_vDetTireLowPressureState(void);
#if defined __MATLAB_COMPILE
void DDS_vSetInputData(void);
void DDS_vCopyDDSDataSet(void);
void DDS_DataSet(WhlStoredData_t *DdsWhl,WhlIntStoredData_t *WhlInt,uint8_t TeethNo);
#endif

#if __Hybrid_DDS
void LDDDSh_vDetLowPressWithTpmsSnr(void);
#endif
/*--------------------------------------------------------------------------*/
void LDDDS_vCallDetection(void)
{
	LDDDS_vUpdateTimeWheelRev();
	LDDDS_vDetDDSSpeedRange();
	LDDDS_vDetDDSMode();
	LDDDS_vReadTPMSData();
	
    #if __Hybrid_DDS
    /*
    if(ldddsu8TpmsSnrLocation==FL_WL){        
        ldddshs16RefTirePressure=lespu16TirePressureFL;
        ldddshu1RefTirePressureV=lespu1TirePressureValidityFL;
        ldddshu8WhlInxAxle=FR_WL;
        ldddshu8WhlInxSide=RL_WL;
        ldddshu8WhlInxDiag=RR_WL;
    }
    else if(ldddsu8TpmsSnrLocation==FR_WL){
        ldddshs16RefTirePressure=lespu16TirePressureFR;
        ldddshu1RefTirePressureV=lespu1TirePressureValidityFR;
        ldddshu8WhlInxAxle=FL_WL;
        ldddshu8WhlInxSide=RR_WL;
        ldddshu8WhlInxDiag=RL_WL;
        
    }
    else if(ldddsu8TpmsSnrLocation==RL_WL){
        ldddshs16RefTirePressure=lespu16TirePressureRL;
        ldddshu1RefTirePressureV=lespu1TirePressureValidityRL;
        ldddshu8WhlInxAxle=RR_WL;
        ldddshu8WhlInxSide=FL_WL;
        ldddshu8WhlInxDiag=FR_WL;
        
    }
    else if(ldddsu8TpmsSnrLocation==RR_WL){
        ldddshs16RefTirePressure=lespu16TirePressureRR;
        ldddshu1RefTirePressureV=lespu1TirePressureValidityRR;
        ldddshu8WhlInxAxle=RL_WL;
        ldddshu8WhlInxSide=FR_WL;
        ldddshu8WhlInxDiag=RL_WL;
        
    }
    else{
        ;
    }
    */    
    ldddshu8WhlInxAxle=FL_WL;
    ldddshu8WhlInxSide=RR_WL;
    ldddshu8WhlInxDiag=RL_WL;
    ldddshs16RefTirePressure=ldu8TPMSBuffer[5];
    ldddshs16RefTireTemperature=ldu8TPMSBuffer[4];
    if(TPMS_ISR_COPY_Flg2==1){
        ldddshu1RefTirePressureV=1;
    }
    else{
        ldddshu1RefTirePressureV=0;
    }
    TPMS_ISR_COPY_Flg2=0;
    #endif	
    
	DDS_flags=1;
	switch(ldu8DDSMode)
    {
        case FUNCTION_IGNON:
			DDS_flags=10;
			#if !defined __MATLAB_COMPILE
			LSDDS_vReadEEPROMDDSData();
			#endif
			
		break;

		case FUNCTION_DDS_RESET:
			DDS_flags=20;
			LDDDS_vClearDDSCalibParameters();
			LDDDS_vClearDDSDetectParameters();
		break;

		case FUNCTION_DDS_TIRE_WEARIN:
			DDS_flags=30;
			LDDDS_vTireWearinRoutine();
		
		break;

		case FUNCTION_DDS_READY:
			
			switch(ldu8DDSSubMode)
			{
				case SUB_FUNCTION_CALIB_N_DCT:	
					DDS_flags=40;					
					LDDDS_vLearnDDSCalibration();
					
					DDS_flags=50;
					LDDDS_vCalcDDSDctPara();
					
					DDS_flags=60;
					LDDDS_vSetTyreDefEstPara();
					
					DDS_flags=70;
					LDDDS_vEstTireDefPress();
					
					DDS_flags=80;
					LDDDS_vDct1WhlDeflationCnt();																																										
																																															
					#if __2WHL_Deflation_Detect		
					DDS_flags=90;																																								
					LDDDS_vDct2WhlDeflation();																																										
					#endif																																										
																																															
					#if __3WHL_Deflation_Detect	
//					DDS_flags=100;																																									
					LDDDS_vDct3WhlDeflation();																																										
					#endif	    																																										
					
//					DDS_flags=110;					        																																													
					LDDDS_vDetDeflationState(); 							
					
					#if __Hybrid_DDS
					ldddshs16CurrentTpmsPressure=ldddshs16RefTirePressure;
					LDDDSh_vDetLowPressWithTpmsSnr();
					#endif
				break;

				case SUB_FUNCTION_CALIBRATION:
					DDS_flags=120;
					LDDDS_vLearnDDSCalibration();	
					
					#if __Hybrid_DDS
					ldddshs16InitTpmsPressure=ldddshs16RefTirePressure;
					#endif				
				break;
								
				case SUB_FUNCTION_DETECTION:
					DDS_flags=130;
					LDDDS_vCalcDDSDctPara();
					
					DDS_flags=140;
					LDDDS_vSetTyreDefEstPara();
					
					DDS_flags=150;
					LDDDS_vEstTireDefPress();
					
					DDS_flags=160;
					LDDDS_vDct1WhlDeflationCnt();																																										
																																															
					#if __2WHL_Deflation_Detect			
					DDS_flags=170;																																							
					LDDDS_vDct2WhlDeflation();																																										
					#endif																																										
																																															
					#if __3WHL_Deflation_Detect		
					DDS_flags=180;																																								
					LDDDS_vDct3WhlDeflation();																																										
					#endif	    																																										
					
					DDS_flags=190;					        																																													
					LDDDS_vDetDeflationState(); 
	
				    #if __Hybrid_DDS
				    ldddshs16CurrentTpmsPressure=ldddshs16RefTirePressure;
					LDDDSh_vDetLowPressWithTpmsSnr();
					#endif
				break;			
					
				case SUB_FUNCTION_STANDBY:
					 DDS_flags=200;
				break;
			}
			
		break;

		case FUNCTION_DDS_LOW_TIRE_PRESSURE:
			DDS_flags=210;
			if(ldddsu1DDSRunningReset==ENABLE)
			{
				switch(ldu8DDSSubMode)
				{				
					case SUB_FUNCTION_RUNNING_RESET:
						DDS_flags=220;
						LDDDS_vCalcDDSDctPara();
						
						DDS_flags=230;
						LDDDS_vSetTyreDefEstPara();
						
						DDS_flags=240;
						LDDDS_vEstTireDefPress();
						
						DDS_flags=250;
						LDDDS_vDetDDSRunningReset();
					break;
					
					case SUB_FUNCTION_STANDBY:
						DDS_flags=255;
					break;
				}
			}		
		break;

		case FUNCTION_INHIBIT:
			DDS_flags=255;
		break;

        default:
			DDS_flags=255;
        break;
	}
}
/*================================================================================*/
#if defined __MATLAB_COMPILE
void DDS_vSetInputData(void)
{
    if(vref_resol_change>(20*64)) 
    {
    	lddu1WheelDataStroedStart=1;
    }
    else
    {
    	lddu1WheelDataStroedStart=0;
    	
    	DDS_FL.StoredTeethCount=0;
    	DDS_FR.StoredTeethCount=0;
    	DDS_RL.StoredTeethCount=0;
    	DDS_RR.StoredTeethCount=0;
    	
    	DDS_FL.TurnedOneWheelEnd=0;
    	DDS_FR.TurnedOneWheelEnd=0;
    	DDS_RL.TurnedOneWheelEnd=0;
    	DDS_RR.TurnedOneWheelEnd=0;
    }
	DDS_vCopyDDSDataSet();
}
/************************************************************************************************/
void DDS_vCopyDDSDataSet(void)
{
//	#define MASK_FL				0x01
//	#define MASK_FR				0x02
//	#define MASK_RL				0x04
//	#define MASK_RR				0x08
	uint8_t i;
	
	DDS_DataSet(&DDS_FL,&INT_DDS_FL,U8_TEETH);
	DDS_DataSet(&DDS_FR,&INT_DDS_FR,U8_TEETH);
	if(U8_F_R_DIFF==1)
	{
		DDS_DataSet(&DDS_RL,&INT_DDS_RL,U8_TEETH_R);
		DDS_DataSet(&DDS_RR,&INT_DDS_RR,U8_TEETH_R);
	}
	else
	{
		DDS_DataSet(&DDS_RL,&INT_DDS_RL,U8_TEETH);
		DDS_DataSet(&DDS_RR,&INT_DDS_RR,U8_TEETH);	
	}
	
	
	if ((DDS_FL.TurnedOneWheelEnd==1)&&(DDS_FR.TurnedOneWheelEnd==1)&&(DDS_RL.TurnedOneWheelEnd==1)&&(DDS_RR.TurnedOneWheelEnd==1))
	{
		DDS_FL.TurnedOneWheelEnd=0;
		DDS_FR.TurnedOneWheelEnd=0;
		DDS_RL.TurnedOneWheelEnd=0;
		DDS_RR.TurnedOneWheelEnd=0;
		
		ldddsu16ElapsedTimePerRev[0]=DDS_FL.fu16TimePerRev10us;
		ldddsu16ElapsedTimePerRev[1]=DDS_FR.fu16TimePerRev10us;
		ldddsu16ElapsedTimePerRev[2]=DDS_RL.fu16TimePerRev10us;
		ldddsu16ElapsedTimePerRev[3]=DDS_RR.fu16TimePerRev10us;
		
		ldddsu8ElapsedTimeUpdateFlg[0]=1;
	    ldddsu8ElapsedTimeUpdateFlg[1]=1;
	    ldddsu8ElapsedTimeUpdateFlg[2]=1;
	    ldddsu8ElapsedTimeUpdateFlg[3]=1;
			
	}
	else
	{
	    ldddsu8ElapsedTimeUpdateFlg[0]=0;
	    ldddsu8ElapsedTimeUpdateFlg[1]=0;
	    ldddsu8ElapsedTimeUpdateFlg[2]=0;
	    ldddsu8ElapsedTimeUpdateFlg[3]=0;

	}    
	

}
/************************************************************************************************/
void DDS_DataSet(WhlStoredData_t *DdsWhl,WhlIntStoredData_t *WhlInt,uint8_t TeethNo)
{
	uint8_t TempCount,LoopCnt;
	uint8_t TeethAngle;
	

	DdsWhl->ldddsu1ElapsedTimeUpdateFlg=0;

	if(lddu1WheelDataStroedStart==0)
	{
		WhlInt->FirstStoredData=0;
		
		for(LoopCnt=0; LoopCnt<10; LoopCnt++)
		{
			DdsWhl->StoredPeriod2[LoopCnt]=0;
		}
	}
	else
	{
		if(DdsWhl->TurnedOneWheelEnd==0)
		{
            for(LoopCnt=0; LoopCnt<WhlInt->PeriodStoreCnt; LoopCnt++)
			{
				if(DdsWhl->StoredTeethCount==0)
				{
					DdsWhl->fu16TimePerRev10us=0;
				}
				
				DdsWhl->StoredPeriod[DdsWhl->StoredTeethCount]=WhlInt->IntStoredPeriod[LoopCnt];
				DdsWhl->fu16TimePerRev10us+=(DdsWhl->StoredPeriod[DdsWhl->StoredTeethCount]/10);
				DdsWhl->StoredTeethCount++;
				if(DdsWhl->StoredTeethCount>=TeethNo) 
				{
					DdsWhl->TurnedOneWheelEnd=1;
					DdsWhl->StoredTeethCount=0;
					DdsWhl->ldddsu1ElapsedTimeUpdateFlg=1;
					break;
				}
			}
		}
		DdsWhl->PeriodStoreCnt=WhlInt->PeriodStoreCnt;
		
		TeethAngle=6283/TeethNo;
		for(LoopCnt=0; LoopCnt<10; LoopCnt++)
		{

            DdsWhl->StoredPeriod2[LoopCnt]=WhlInt->IntStoredPeriod[LoopCnt];
			WhlInt->IntStoredPeriod[LoopCnt]=0;
		}
				
	}	
	
	WhlInt->PeriodStoreCnt=0;
}
#endif
/*======================================================================================*/
void LDDDS_vDetDDSMode(void)
{
	LDDDS_vDctSteadyStraightState();
	
	#if defined ( __H2DCL) 
	ldddsu1RequestDDSReset=ctu1DDS;
	#elif (__DDS_RESET_BY_ESC_SW == ENABLE)
	ldddsu1RequestDDSReset=fu1DdsSwitchDriverIntent;
	#else
	if(speed_calc_timer>T_1000_MS)
	{
		ldddsu1RequestDDSReset=0;
	}
	else
	{
		ldddsu1RequestDDSReset=1;
	}
	#endif
	

	
	if(ldu8DDSMode==FUNCTION_IGNON)
	{
		if(ldddsu1RequestDDSReset==1)
		{
			ldu8DDSMode=FUNCTION_DDS_RESET;
		}
		else if(speed_calc_timer>T_1000_MS)
		{
			if((ldu8DDSModeEepromValue==FUNCTION_IGNON)&&(ldu8DDSSubMode==0))
			{
				ldu8DDSMode=FUNCTION_DDS_RESET;
			}
			else
			{
				ldu8DDSMode=ldu8DDSModeEepromValue; 
			}
		}
		else
		{
			;
		}
	}
	else if(ldu8DDSMode==FUNCTION_DDS_RESET)
	{
		ldddsu16DDSResetSustainCnt++;
		if(ldddsu16DDSResetSustainCnt>DDS_RESET_HOLD_TIME)
		{
			ldu8DDSMode=FUNCTION_DDS_TIRE_WEARIN;
			ldu8DDSModeEepromValue=ldu8DDSMode;
			ldu8DDSModeOld=ldu8DDSMode;
			ldu8DDSSubMode=0;
			ldddsu16DDSResetSustainCnt=0;
		}
		else
		{
			;
		}
	}
	else if(ldu8DDSMode==FUNCTION_INHIBIT)
	{
		if((fu1WheelFLSusDet==0)&&(fu1WheelFRSusDet==0)&&(fu1WheelRLSusDet==0)&&(fu1WheelRRSusDet==0)&&
			(fu1WheelFLErrDet==0)&&(fu1WheelFRErrDet==0)&&(fu1WheelRLErrDet==0)&&(fu1WheelRRErrDet==0)&&
			(fu1MainCanSusDet==0)&&(fu1MainCanLineErrDet==0)&&(fu1EMSTimeOutSusDet==0) &&(fu1EMSTimeOutErrDet==0))
		{
			ldu8DDSMode=ldu8DDSModeOld;
		}
	}	
	else if(ldu8DDSMode==FUNCTION_DDS_TIRE_WEARIN)
	{
		/*if((fu1WheelFLSusDet==1)||(fu1WheelFRSusDet==1)||(fu1WheelRLSusDet==1)||(fu1WheelRRSusDet==1)||
			(fu1WheelFLErrDet==1)||(fu1WheelFRErrDet==1)||(fu1WheelRLErrDet==1)||(fu1WheelRRErrDet==1)||
			(fu1MainCanSusDet==1) ||(fu1MainCanLineErrDet==1)||(fu1EMSTimeOutSusDet==1) ||(fu1EMSTimeOutErrDet==1))
		{
			ldu8DDSModeOld=ldu8DDSMode;
			ldu8DDSMode=FUNCTION_INHIBIT;
		}		
		else */if(ldddsu1RequestDDSReset==1)
		{
			ldu8DDSMode=FUNCTION_DDS_RESET;
		}
		else if(ldddsu1TireWearInCompleted==1)
		{
			ldu8DDSMode=FUNCTION_DDS_READY;
		}
		else
		{
			;
		}
	}			
	else if(ldu8DDSMode==FUNCTION_DDS_READY)
	{
		/*Add EEPROM ERROR CONDITION*/
		/*Add SWITCH SIGNAL ERROR CONDITION*/
		/*Add Low Voltage Check CONDITION*/
		
		/*if((fu1WheelFLSusDet==1)||(fu1WheelFRSusDet==1)||(fu1WheelRLSusDet==1)||(fu1WheelRRSusDet==1)||
			(fu1WheelFLErrDet==1)||(fu1WheelFRErrDet==1)||(fu1WheelRLErrDet==1)||(fu1WheelRRErrDet==1)||
			(fu1MainCanSusDet==1) ||(fu1MainCanLineErrDet==1)||(fu1EMSTimeOutSusDet==1) ||(fu1EMSTimeOutErrDet==1))
		{
			ldu8DDSModeOld=ldu8DDSMode;
			ldu8DDSMode=FUNCTION_INHIBIT;
		}	

		else */if(ldddsu1RequestDDSReset==1)
		{
			ldu8DDSMode=FUNCTION_DDS_RESET;
		}
		else if(ldddsu1DeflationState==1)
		{
			ldu8DDSMode=FUNCTION_DDS_LOW_TIRE_PRESSURE;
		}			
		else
		{
			if((ldddsu1StdStrDrvStateForDDS==1)&&(vref_resol_change>U8_MIN_SPEED_FOR_DDS)&&(vref_resol_change<U8_MAX_SPEED_FOR_DDS))
			{
				LDDDS_vDetCalibCompletion();
/*	9818 Day Calib Value			
				ldddsu1CompleteCalib=1;
				ldddsu8CompleteCalib[0]=1;
				ldddss16DetCalibValue[FL_WL][0]=47;
				ldddss16DetCalibValue[FR_WL][0]=35;
				ldddss16DetCalibValue[RL_WL][0]=-4;
				ldddss16DetCalibValue[RR_WL][0]=2;			

				ldddsu8CompleteCalib[1]=1;
				ldddss16DetCalibValue[FL_WL][1]=58;
				ldddss16DetCalibValue[FR_WL][2]=58;
				ldddss16DetCalibValue[RL_WL][3]=-1;
				ldddss16DetCalibValue[RR_WL][4]=0;	

				ldddsu8CompleteCalib[2]=1;
				ldddss16DetCalibValue[FL_WL][2]=61; 
				ldddss16DetCalibValue[FR_WL][2]=61; 
				ldddss16DetCalibValue[RL_WL][2]=-1; 
				ldddss16DetCalibValue[RR_WL][2]=0;	

				ldddsu8CompleteCalib[3]=1;
				ldddss16DetCalibValue[FL_WL][3]=61; 
				ldddss16DetCalibValue[FR_WL][3]=61; 
				ldddss16DetCalibValue[RL_WL][3]=-1; 
				ldddss16DetCalibValue[RR_WL][3]=0;		
				
				ldddsu8CompleteCalib[4]=1;              
				ldddss16DetCalibValue[FL_WL][4]=61;     
				ldddss16DetCalibValue[FR_WL][4]=61;     
				ldddss16DetCalibValue[RL_WL][4]=-1;     
				ldddss16DetCalibValue[RR_WL][4]=0;					
*/				
/*
				ldddsu8CompleteCalib[0]=1;          
				ldddss16DetCalibValue[FL_WL][0]=55; 
				ldddss16DetCalibValue[FR_WL][0]=54; 
				ldddss16DetCalibValue[RL_WL][0]=-1; 
				ldddss16DetCalibValue[RR_WL][0]=0;	
                                                    
				ldddsu8CompleteCalib[1]=1;          
				ldddss16DetCalibValue[FL_WL][1]=58; 
				ldddss16DetCalibValue[FR_WL][1]=55; 
				ldddss16DetCalibValue[RL_WL][1]=-1; 
				ldddss16DetCalibValue[RR_WL][1]=0;	
                                                    
				ldddsu8CompleteCalib[2]=1;          
				ldddss16DetCalibValue[FL_WL][2]=63; 
				ldddss16DetCalibValue[FR_WL][2]=64; 
				ldddss16DetCalibValue[RL_WL][2]=-2; 
				ldddss16DetCalibValue[RR_WL][2]=2;	
                                                    
				ldddsu8CompleteCalib[3]=1;          
				ldddss16DetCalibValue[FL_WL][3]=63; 
				ldddss16DetCalibValue[FR_WL][3]=64; 
				ldddss16DetCalibValue[RL_WL][3]=-2; 
				ldddss16DetCalibValue[RR_WL][3]=2;	
				                                    
				ldddsu8CompleteCalib[4]=1;          
				ldddss16DetCalibValue[FL_WL][4]=63; 
				ldddss16DetCalibValue[FR_WL][4]=64; 
				ldddss16DetCalibValue[RL_WL][4]=-2; 
				ldddss16DetCalibValue[RR_WL][4]=2;	
*/				

				if(ldddsu1CompleteCalibAllSpeed==1)
				{
					ldu8DDSSubMode=SUB_FUNCTION_DETECTION;
				}
				else if(ldddsu1CompleteCalib==1)
				{
					ldu8DDSSubMode=SUB_FUNCTION_CALIB_N_DCT;
				}
				else
				{
					ldu8DDSSubMode=SUB_FUNCTION_CALIBRATION;
				}
			}
			else
			{
				ldu8DDSSubMode=SUB_FUNCTION_STANDBY;
			}
		}
	}
	else if(ldu8DDSMode==FUNCTION_DDS_LOW_TIRE_PRESSURE)
	{
		/*if((fu1WheelFLSusDet==1)||(fu1WheelFRSusDet==1)||(fu1WheelRLSusDet==1)||(fu1WheelRRSusDet==1)||
			(fu1WheelFLErrDet==1)||(fu1WheelFRErrDet==1)||(fu1WheelRLErrDet==1)||(fu1WheelRRErrDet==1)||
			(fu1MainCanSusDet==1) ||(fu1MainCanLineErrDet==1)||(fu1EMSTimeOutSusDet==1) ||(fu1EMSTimeOutErrDet==1))
		{
			ldu8DDSModeOld=ldu8DDSMode;
			ldu8DDSMode=FUNCTION_INHIBIT;
		}			
		else */if(ldddsu1RequestDDSReset==1)
		{
			ldu8DDSMode=FUNCTION_DDS_RESET;
		}	
		else if(ldddsu1DDSRunningReset==ENABLE)
		{
			
			if((ldddsu1StdStrDrvStateForDDS==1)&&(vref_resol_change>U8_MIN_SPEED_FOR_DDS)&&(vref_resol_change<U8_MAX_SPEED_FOR_DDS))
			{
				LDDDS_vDetCalibCompletion();
				
				if(ldddsu1DeflationState==0)
				{
					ldu8DDSMode=FUNCTION_DDS_RESET;
				}
				else
				{
					ldu8DDSSubMode=SUB_FUNCTION_RUNNING_RESET;
				}
			}
			else
			{
				ldu8DDSSubMode=SUB_FUNCTION_STANDBY;				
			}
		}
		else
		{
			;
		}		
	}
	else
	{
		;
	}
}
/*=================================================================================*/
void LDDDS_vDctSteadyStraightState(void)
{   
    uint8_t ldddsu1DetESPSensorUsage;
//	int16_t	ldddss16VradMinMaxDiffRatio;
//	uint8_t 	ldddsu8AccelPedalReleaseCnt;
	
	uint16_t ldddsu16CorretecTimePerRevFL;
	uint16_t ldddsu16CorretecTimePerRevFR;
	uint16_t ldddsu16CorretecTimePerRevRL;
	uint16_t ldddsu16CorretecTimePerRevRR;
	
	uint16_t ldddsu16MaxTimePerRev;
	uint16_t ldddsu16MinTimePerRev;
//	int16_t ldddss16RevTimeMinMaxDiffRatio;
	
	uint16_t ldddsu16RevTimeMaxFrt;
	uint16_t ldddsu16RevTimeMaxRear;
	uint16_t ldddsu16RevTimeMinFrt;
	uint16_t ldddsu16RevTimeMinRear;
	
//	int16_t ldddss16WheelSpdDiffRatioFrt;
//	int16_t ldddss16WheelSpdDiffRatioRear;


    #if __VDC
    ldddsu1DetESPSensorUsage=1;
  
    if((fu1AySusDet==1)||(fu1AyErrorDet==1))
    {
    	ldddsu1DetESPSensorUsage=0;
    }
    else if((fu1StrSusDet==1)||(fu1StrErrorDet==1))
    {
    	ldddsu1DetESPSensorUsage=0;
    }
    else if((fu1YawSusDet==1)||(fu1YawErrorDet==1))
    {
    	ldddsu1DetESPSensorUsage=0;
    }

	else if(SAS_CHECK_OK==0)
    {
    	ldddsu1DetESPSensorUsage=0;
    }
    else
    {
    	;
    }
    
    #endif
    
	ldddss16VradMinMaxDiffRatio=(int16_t)(((uint32_t)vrad_diff_max_resol_change*1000)/vref_resol_change);
	if(mtp>5)
	{
		ldddsu8AccelPedalReleaseCnt=T_500_MS;
	}
	else if(ldddsu8AccelPedalReleaseCnt>0)
	{
		ldddsu8AccelPedalReleaseCnt--;
	}
	else
	{
		;
	}
  	ldddsu16CorretecTimePerRevFL=(uint16_t)(((uint32_t)(CalibrationValueFL+10000)*(uint32_t)ldddsu16ElapsedTimePerRev[FL_WL])/(uint32_t)10000);
  	ldddsu16CorretecTimePerRevFR=(uint16_t)(((uint32_t)(CalibrationValueFR+10000)*(uint32_t)ldddsu16ElapsedTimePerRev[FR_WL])/(uint32_t)10000);
  	ldddsu16CorretecTimePerRevRL=(uint16_t)(((uint32_t)(CalibrationValueRL+10000)*(uint32_t)ldddsu16ElapsedTimePerRev[RL_WL])/(uint32_t)10000);
  	ldddsu16CorretecTimePerRevRR=(uint16_t)(((uint32_t)(CalibrationValueRR+10000)*(uint32_t)ldddsu16ElapsedTimePerRev[RR_WL])/(uint32_t)10000);
/*
 	ldddsu16CorretecTimePerRevFL=ldddsu16ElapsedTimePerRev[FL_WL];
  	ldddsu16CorretecTimePerRevFR=ldddsu16ElapsedTimePerRev[FR_WL];
  	ldddsu16CorretecTimePerRevRL=ldddsu16ElapsedTimePerRev[RL_WL];
  	ldddsu16CorretecTimePerRevRR=ldddsu16ElapsedTimePerRev[RR_WL];
*/
  	if(ldddsu16CorretecTimePerRevFL>ldddsu16CorretecTimePerRevFR)
  	{
  		ldddsu16RevTimeMaxFrt=ldddsu16CorretecTimePerRevFL;
  		ldddsu16RevTimeMinFrt=ldddsu16CorretecTimePerRevFR;
  	}
  	else
  	{
  		ldddsu16RevTimeMaxFrt=ldddsu16CorretecTimePerRevFR;
  		ldddsu16RevTimeMinFrt=ldddsu16CorretecTimePerRevFL;
  	}
  	if(ldddsu16CorretecTimePerRevRL>ldddsu16CorretecTimePerRevRR)
  	{
  		ldddsu16RevTimeMaxRear=ldddsu16CorretecTimePerRevRL;
  		ldddsu16RevTimeMinRear=ldddsu16CorretecTimePerRevRR;
  	}
  	else
  	{
  		ldddsu16RevTimeMaxRear=ldddsu16CorretecTimePerRevRR;
  		ldddsu16RevTimeMinRear=ldddsu16CorretecTimePerRevRL;
  	}		    
  	
  	if(ldddsu16RevTimeMaxFrt>ldddsu16RevTimeMaxRear)
  	{
  		ldddsu16MaxTimePerRev=ldddsu16RevTimeMaxFrt;
  	}
  	else
  	{
  		ldddsu16MaxTimePerRev=ldddsu16RevTimeMaxRear;
  	}
  	
  	if(ldddsu16RevTimeMinFrt>ldddsu16RevTimeMinRear)
  	{
  		ldddsu16MinTimePerRev=ldddsu16RevTimeMinRear;
  	}
  	else
  	{
  		ldddsu16MinTimePerRev=ldddsu16RevTimeMinFrt;
  	}
  	ldddss16RevTimeMinMaxDiffRatio=(int16_t)((((int32_t)ldddsu16MaxTimePerRev-(int32_t)ldddsu16MinTimePerRev)*1000)/ldddsu16MinTimePerRev);
  	tempW1=(int16_t)((int32_t)ldddsu16CorretecTimePerRevFL-(int32_t)ldddsu16CorretecTimePerRevFR);  	
	ldddss16WheelSpdDiffRatioFrt=(int16_t)(((int32_t)tempW1*(int32_t)1000)/((int32_t)ldddsu16RevTimeMinFrt));//(int16_t)((((int32_t)ldddsu16CorretecTimePerRevFL-(int32_t)ldddsu16CorretecTimePerRevFR)*1000)/ldddsu16RevTimeMinFrt);
  	tempW1=(int16_t)((int32_t)ldddsu16CorretecTimePerRevRL-(int32_t)ldddsu16CorretecTimePerRevRR); 
  	ldddss16WheelSpdDiffRatioRear=(int16_t)(((int32_t)tempW1*(int32_t)1000)/((int32_t)ldddsu16RevTimeMinRear));//(int16_t)((((int32_t)ldddsu16CorretecTimePerRevRL-(int32_t)ldddsu16CorretecTimePerRevRR)*1000)/ldddsu16RevTimeMinRear);
    
    ldddsu1StdStrDrvStateForDDS=1;
    GoodC_Flag=0;
 	if(BRAKE_SIGNAL==1)
    {
        ldddsu1StdStrDrvStateForDDS=0;
        GoodC_Flag=1;
    }
    else if(abs(afz)>AFZ_0G15)
    {
        ldddsu1StdStrDrvStateForDDS=0;
        GoodC_Flag=2;
    }
    else if(abs(aref_avg)>AFZ_0G15)
    {
        ldddsu1StdStrDrvStateForDDS=0;
        GoodC_Flag=3;
    }
    #if __EDC           
    else if(engine_state==0) 
    {
        ldddsu1StdStrDrvStateForDDS=0;
        GoodC_Flag=4;
    }
    else if(EDC_ON==1)
    {
        ldddsu1StdStrDrvStateForDDS=0;
        GoodC_Flag=5;
    }
    #endif  
    
    #if __TCS
    else if(TCS_ON==1)
    {
        ldddsu1StdStrDrvStateForDDS=0;
        GoodC_Flag=6;
    }  
    #endif
    
    
    #if __EDC || __TCS || __ETC || __VDC
    else if(eng_torq_abs>S16_MAX_ENGINE_TOQR_DDS)
    {
    	ldddsu1StdStrDrvStateForDDS=0;
    	GoodC_Flag=7;
    }
    #endif
   #if __PARKING_BRAKE_OPERATION
    else if (PARKING_BRAKE_OPERATION==1) 
    {
        ldddsu1StdStrDrvStateForDDS=0;
        GoodC_Flag=8;
    }  
  #endif 
  
  #if __MGH_40_VREF
  #if (__4WD_VARIANT_CODE==ENABLE)
    else if ((ACCEL_SPIN_FZ==1)&&((lsu8DrvMode ==DM_AUTO)||(lsu8DrvMode ==DM_4H)))
    {
        ldddsu1StdStrDrvStateForDDS=0;
        GoodC_Flag=9;
    }  
  #elif __4WD
    else if (ACCEL_SPIN_FZ==1) 
    {
        ldddsu1StdStrDrvStateForDDS=0;
        GoodC_Flag=10;
    }
   #endif
  #endif
 
    else if((RTA_FL.RTA_SUSPECT_WL==1)||(RTA_FR.RTA_SUSPECT_WL==1)||(RTA_RL.RTA_SUSPECT_WL==1)||(RTA_RR.RTA_SUSPECT_WL==1))
    {
    	ldddsu1StdStrDrvStateForDDS=0; 
    	GoodC_Flag=11;
    }
    else if((Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1))
    {
    	ldddsu1StdStrDrvStateForDDS=0; 
    	GoodC_Flag=12;
    }
    else if(FL.WhlErrFlg|FR.WhlErrFlg|RL.WhlErrFlg|RR.WhlErrFlg)
//	else if(suspect_flg_fl|suspect_flg_fr|suspect_flg_rl|suspect_flg_rr)
	{
		ldddsu1StdStrDrvStateForDDS=0; 
		GoodC_Flag=13;
	}
	// Add on 9925
	else if(ldddss16VradMinMaxDiffRatio>30)
	{
		ldddsu1StdStrDrvStateForDDS=0; 
		GoodC_Flag=14;		
	}    

	else if((mtp<5)&&(ldddsu8AccelPedalReleaseCnt>0)) 
	{
		ldddsu1StdStrDrvStateForDDS=0; 
		GoodC_Flag=15;
	} 

	else if(ebd_refilt_grv>5)
	{
		ldddsu1StdStrDrvStateForDDS=0; 
		GoodC_Flag=16;			
	}

/*Consider RTA Compensation*/	
	else if(ldddss16RevTimeMinMaxDiffRatio>30)
	{
		ldddsu1StdStrDrvStateForDDS=0; 
		GoodC_Flag=17;
	}
	else if((ldddss16RevTimeMinMaxDiffRatio>15)&&(ldddsu1CompleteCalib==1))
	{
		ldddsu1StdStrDrvStateForDDS=0; 
		GoodC_Flag=18;	
	}	
	else if((ldddss16WheelSpdDiffRatioFrt>15)&&(ldddss16WheelSpdDiffRatioRear>15))
	{
		ldddsu1StdStrDrvStateForDDS=0; 
		GoodC_Flag=19;	
	}
	else if((ldddss16WheelSpdDiffRatioFrt<-15)&&(ldddss16WheelSpdDiffRatioRear<-15))
	{
		ldddsu1StdStrDrvStateForDDS=0; 
		GoodC_Flag=20;
	}
	       
    #if __VDC
    else if(YAW_CDC_WORK==1)
    {
        ldddsu1StdStrDrvStateForDDS=0;
        GoodC_Flag=21;
    }
    else if((ldddsu1DetESPSensorUsage==1)&&(abs(delta_yaw_first) >= YAW_3DEG))
    {
		ldddsu1StdStrDrvStateForDDS=0;
        GoodC_Flag=22;
	}
    else if(((abs(yaw_out)>YAW_3DEG)||(abs(alat)>LAT_0G1G)||(abs(wstr)>WSTR_30_DEG))&&(ldddsu1DetESPSensorUsage==1))
    {
    	ldddsu1StdStrDrvStateForDDS=0;
        GoodC_Flag=23;
	}

    #endif 

    else
    {
        ;
    }       
}

/*=====================================================================*/
void LDDDS_vTireWearinRoutine(void)
{
#if !defined __MATLAB_COMPILE
	#if __REAR_D
	ldddsu8SumOfEdgeLeft+=fsu8NumberOfEdge_fl;
	ldddsu8SumOfEdgeRight+=fsu8NumberOfEdge_fr;
	#else
	ldddsu8SumOfEdgeLeft+=fsu8NumberOfEdge_rl;
	ldddsu8SumOfEdgeRight+=fsu8NumberOfEdge_rr;
	#endif
	
	if(ldddsu8SumOfEdgeLeft>=(U8_DDS_TEETH*2))
	{
		ldddss16RollingCntLeft++;
		ldddsu8SumOfEdgeLeft-=(U8_DDS_TEETH*2);
		
		if(ldddss16RollingCntLeft>=(int16_t)(((int32_t)MILE_PER_MITER*1000)/DDS_TIRE_L))
		{
			ldddss16RollingCntLeft=0;
			ldddss16TireWereInMileageLeft++;
		}
	}
	if(ldddsu8SumOfEdgeRight>=(U8_DDS_TEETH*2))
	{
		ldddss16RollingCntRight++;
		ldddsu8SumOfEdgeRight-=(U8_DDS_TEETH*2);
		if(ldddss16RollingCntRight>=(int16_t)(((int32_t)MILE_PER_MITER*1000)/DDS_TIRE_L))
		{
			ldddss16RollingCntRight=0;
			ldddss16TireWereInMileageRight++;
		}							
	}  
    
    if((ldddss16TireWereInMileageLeft>=DDS_TIRE_WEAR_IN_DISTANCE)
    	&&(ldddss16TireWereInMileageRight>=DDS_TIRE_WEAR_IN_DISTANCE))
    {
    	ldddsu1TireWearInCompleted=1;
    	ldddss16TireWereInMileageLeft=0;
    	ldddss16TireWereInMileageRight=0;
    	ldddss16RollingCntLeft=0;
    	ldddss16RollingCntRight=0;
    	ldddsu8SumOfEdgeLeft=0;
    	ldddsu8SumOfEdgeRight=0;
    }	
#else
    	ldddsu1TireWearInCompleted=1;
    	ldddss16TireWereInMileageLeft=0;
    	ldddss16TireWereInMileageRight=0;
    	ldddss16RollingCntLeft=0;
    	ldddss16RollingCntRight=0;
    	ldddsu8SumOfEdgeLeft=0;
    	ldddsu8SumOfEdgeRight=0;
#endif    
}
/*=================================================================================*/
void LDDDS_vDetDDSSpeedRange(void)
{
	if(vref_resol_change<U8_DDS_LOWER_SPEED)
	{
		ldddsu8SpeedRangeIndex=U8_LOW_SPEED_RANGE;
	}
	else if(vref_resol_change<U8_DDS_LOW_SPEED)
	{		
		ldddsu8SpeedRangeIndex=U8_LOW_MED_SPEED_RANGE;
	}
	else if(vref_resol_change<U8_DDS_LOW_MED_SPEED)
	{
		ldddsu8SpeedRangeIndex=U8_MED_LOW_SPEED_RANGE;
	}
	else if(vref_resol_change<U8_DDS_MED_SPEED)
	{
		ldddsu8SpeedRangeIndex=U8_MED_HIGH_SPEED_RANGE;
	}
	else if(vref_resol_change<U8_DDS_MED_HIGH_SPEED)
	{
		ldddsu8SpeedRangeIndex=U8_HIGH_MED_SPEED_RANGE;
	}
	else 
	{
		ldddsu8SpeedRangeIndex=U8_HIGH_SPEED_RANGE;
	}			
}
/*=================================================================================*/
void LDDDS_vLearnDDSCalibration(void)
{
	
	if((ldddsu8SpeedRangeIndex==U8_LOW_SPEED_RANGE)||(ldddsu8SpeedRangeIndex==U8_LOW_MED_SPEED_RANGE))
	{
		ldddsu8CalibSpeedIndex=0;
	}
	else if((ldddsu8SpeedRangeIndex==U8_MED_LOW_SPEED_RANGE)||(ldddsu8SpeedRangeIndex==U8_MED_HIGH_SPEED_RANGE))
	{
		ldddsu8CalibSpeedIndex=2;
	}
	else if((ldddsu8SpeedRangeIndex==U8_HIGH_MED_SPEED_RANGE)||(ldddsu8SpeedRangeIndex==U8_HIGH_SPEED_RANGE))
	{
		ldddsu8CalibSpeedIndex=4;
	}
	else
	{
		;
	}
	
	v_inx1=ldddsu8CalibSpeedIndex;
	
	DDS_flags=100;

	LDDDS_vSumDDSCalibInputSpdRange(FL_WL,ldddsu8CalibSpeedIndex);
	LDDDS_vSumDDSCalibInputSpdRange(FR_WL,ldddsu8CalibSpeedIndex);
	LDDDS_vSumDDSCalibInputSpdRange(RL_WL,ldddsu8CalibSpeedIndex);
	LDDDS_vSumDDSCalibInputSpdRange(RR_WL,ldddsu8CalibSpeedIndex);
	LDDDS_vCalcDDSCalibSpdRange(ldddsu8CalibSpeedIndex);	
	
	
	if((ldddsu8SpeedRangeIndex==U8_LOW_SPEED_RANGE)||(ldddsu8SpeedRangeIndex==U8_HIGH_SPEED_RANGE))
	{
		;
	}
	else
	{		
		if((ldddsu8SpeedRangeIndex==U8_LOW_MED_SPEED_RANGE)||(ldddsu8SpeedRangeIndex==U8_MED_LOW_SPEED_RANGE))
		{
			ldddsu8CalibSpeedIndex=1;
		}
		else if((ldddsu8SpeedRangeIndex==U8_MED_HIGH_SPEED_RANGE)||(ldddsu8SpeedRangeIndex==U8_HIGH_MED_SPEED_RANGE))
		{
			ldddsu8CalibSpeedIndex=3;
		}
		else
		{
			;
		}
		LDDDS_vSumDDSCalibInputSpdRange(FL_WL,ldddsu8CalibSpeedIndex);
		LDDDS_vSumDDSCalibInputSpdRange(FR_WL,ldddsu8CalibSpeedIndex);
		LDDDS_vSumDDSCalibInputSpdRange(RL_WL,ldddsu8CalibSpeedIndex);
		LDDDS_vSumDDSCalibInputSpdRange(RR_WL,ldddsu8CalibSpeedIndex);

		LDDDS_vCalcDDSCalibSpdRange(ldddsu8CalibSpeedIndex);					
	}
	v_inx2=ldddsu8CalibSpeedIndex;
	
		
}	
/*=================================================================================*/
void LDDDS_vSumDDSCalibInputSpdRange(uint8_t WHL ,uint8_t v_inx_t)
{
    DDS_flags=101;
	if((ldddsu8ElapsedTimeUpdateFlg[WHL]==1)&&(ldddsu8CompleteCalib[v_inx_t]==0)) /*1 scan set,reset 되는 flag 참조*/
	{
		if(ldddsu8CalibSamplingDistCnt[WHL][v_inx_t]<U8_CALIB_SAMPLING_REV)
		{
			DDS_flags=102;
			ldddsu32SumDiffForCalib[WHL][v_inx_t]=ldddsu32SumDiffForCalib[WHL][v_inx_t]+(uint32_t)(ldddsu16ElapsedTimePerRev[WHL]);
			ldddsu8CalibSamplingDistCnt[WHL][v_inx_t]++; /*counter increase at every revolution*/
		}
		else
		{
			DDS_flags=103;
		}		
	}
	else
	{
//		DDS_flags=104;
	}
}
/*=================================================================================*/
void LDDDS_vCalcDDSCalibSpdRange(uint8_t v_inx_t)
{	
//	uint32_t	ldu32DDSCalibRef;
//	int32_t temp_sum[4];             
//	uint8_t i;
//	uint16_t temp_cnt;
//	uint8_t temp_i;
		
	if((ldddsu8CalibSamplingDistCnt[FL_WL][v_inx_t]>=U8_CALIB_SAMPLING_REV)
		&&(ldddsu8CalibSamplingDistCnt[FR_WL][v_inx_t]>=U8_CALIB_SAMPLING_REV)
		&&(ldddsu8CalibSamplingDistCnt[RL_WL][v_inx_t]>=U8_CALIB_SAMPLING_REV)
		&&(ldddsu8CalibSamplingDistCnt[RR_WL][v_inx_t]>=U8_CALIB_SAMPLING_REV)
		&&(ldddsu8CompleteCalib[v_inx_t]==0))
	{
		DDS_flags=105;
		#if __REAR_D
		ldu32DDSCalibRef=(((uint32_t)ldddsu32SumDiffForCalib[FL_WL][v_inx_t]+(uint32_t)ldddsu32SumDiffForCalib[FR_WL][v_inx_t])/2);
		#else
		ldu32DDSCalibRef=(((uint32_t)ldddsu32SumDiffForCalib[RL_WL][v_inx_t]+(uint32_t)ldddsu32SumDiffForCalib[RR_WL][v_inx_t])/2);
		#endif
		
		ldddsu8CalibSamplingDistCnt[FL_WL][v_inx_t]=0;
		ldddsu8CalibSamplingDistCnt[FR_WL][v_inx_t]=0;
		ldddsu8CalibSamplingDistCnt[RL_WL][v_inx_t]=0;
		ldddsu8CalibSamplingDistCnt[RR_WL][v_inx_t]=0;
		
		ldddsu32SumDiffForCalibTemp[FL_WL][v_inx_t]=ldddsu32SumDiffForCalib[FL_WL][v_inx_t];
		ldddsu32SumDiffForCalibTemp[FR_WL][v_inx_t]=ldddsu32SumDiffForCalib[FR_WL][v_inx_t];
		ldddsu32SumDiffForCalibTemp[RL_WL][v_inx_t]=ldddsu32SumDiffForCalib[RL_WL][v_inx_t];
		ldddsu32SumDiffForCalibTemp[RR_WL][v_inx_t]=ldddsu32SumDiffForCalib[RR_WL][v_inx_t];
		                        
		ldddsu32SumDiffForCalib[FL_WL][v_inx_t]=0;
		ldddsu32SumDiffForCalib[FR_WL][v_inx_t]=0;
		ldddsu32SumDiffForCalib[RL_WL][v_inx_t]=0;
		ldddsu32SumDiffForCalib[RR_WL][v_inx_t]=0;
		
		ldddsu1DDSCalibUpdateFlag[v_inx_t]=1;
	}
	v_inx_temp=v_inx_t;
	if(ldddsu1DDSCalibUpdateFlag[v_inx_t]==1)
	{	
		if(ldddsu8DdsCalibWhlInx[v_inx_t]==FL_WL)
		{
			 LDDDS_vCalcDDSCalibEachWheel(FL_WL ,v_inx_temp);
		}
		else if(ldddsu8DdsCalibWhlInx[v_inx_t]==FR_WL)
		{
			LDDDS_vCalcDDSCalibEachWheel(FR_WL ,v_inx_temp);
    	}
    	else if(ldddsu8DdsCalibWhlInx[v_inx_t]==RL_WL)
		{
			LDDDS_vCalcDDSCalibEachWheel(RL_WL ,v_inx_temp);
    	}
    	else if(ldddsu8DdsCalibWhlInx[v_inx_t]==RR_WL)
		{
			LDDDS_vCalcDDSCalibEachWheel(RR_WL ,v_inx_temp);
    	}
    	else
    	{
    		ldddsu1DDSCalibUpdateFlag[v_inx_t]=0;
    		
	        if(ldddsu8CalcCalibArrayCnt[v_inx_t]>=9)
	        {			    
	        	if(((DDS_max[FL_WL]-DDS_min[FL_WL])<WHEEL_DIFF_MAX_THR_Front)&&((DDS_max[FR_WL]-DDS_min[FR_WL])<WHEEL_DIFF_MAX_THR_Front)
	        		&&((DDS_max[RL_WL]-DDS_min[RL_WL])<WHEEL_DIFF_MAX_THR_Rear)&&((DDS_max[RR_WL]-DDS_min[RR_WL])<WHEEL_DIFF_MAX_THR_Rear))
	        	{
	        		DDS_flags=110;
	        		ldddsu8CalcCalibArrayCnt[v_inx_t]=0;         	            
					LDDDS_vCalcDDSCalibCumulMean(FL_WL,v_inx_temp);
					LDDDS_vCalcDDSCalibCumulMean(FR_WL,v_inx_temp);
					LDDDS_vCalcDDSCalibCumulMean(RL_WL,v_inx_temp);
					LDDDS_vCalcDDSCalibCumulMean(RR_WL,v_inx_temp);
					
					ldddsu16CntCalibTotalRev[v_inx_t]=ldddsu16CntCalibTotalRev[v_inx_t]+10;
	        	}
	        	else
	        	{
	        		DDS_flags=111;
	        		if(ldddsu8CalcCalibArrayCnt[v_inx_t]==9)
	        		{
	        			ldddsu8CalcCalibArrayCnt[v_inx_t]++; 
	        			DDS_flags=112;
	        		}
	        	}
	        }
	 	    else
	        {       
		        ldddsu8CalcCalibArrayCnt[v_inx_t]++;		        
	    	}    		
    	}
		ldddsu8DdsCalibWhlInx[v_inx_t]++;
		if(ldddsu8DdsCalibWhlInx[v_inx_t]==5)
		{
			ldddsu8DdsCalibWhlInx[v_inx_t]=0;
		}	
	}
	else
	{
		DDS_flags=109;
	}
}
/*=================================================================================*/
void LDDDS_vCalcDDSCalibCumulMean(uint8_t WHL ,uint8_t v_inx_t)
{
	uint16_t temp_cnt;
	
	temp_cnt=ldddsu16CntCalibTotalRev[v_inx_t];
	tempW1=(ldddss16CumulativeMean[WHL][v_inx_t]);
    tempL0=(int32_t)(temp_cnt)*(int32_t)(ldddss16CumulativeMean[WHL][v_inx_t]);
    tempL0=tempL0+(temp_sum[WHL])+(int32_t)(ldddss16CalibRest[WHL][v_inx_t]);
    tempW0=(int16_t)(tempL0/((int16_t)temp_cnt+10));
    ldddss16CumulativeMean[WHL][v_inx_t]=tempW0;
    ldddss16CalibRest[WHL][v_inx_t]=(int16_t)(tempL0%((int16_t)temp_cnt+10));  
    
    
    ldddss16SumDiffArrayCalib[WHL][0][v_inx_t]=0; 
    ldddss16SumDiffArrayCalib[WHL][1][v_inx_t]=0; 
    ldddss16SumDiffArrayCalib[WHL][2][v_inx_t]=0; 
    ldddss16SumDiffArrayCalib[WHL][3][v_inx_t]=0; 
    ldddss16SumDiffArrayCalib[WHL][4][v_inx_t]=0; 
    ldddss16SumDiffArrayCalib[WHL][5][v_inx_t]=0;
    ldddss16SumDiffArrayCalib[WHL][6][v_inx_t]=0; 
    ldddss16SumDiffArrayCalib[WHL][7][v_inx_t]=0; 
    ldddss16SumDiffArrayCalib[WHL][8][v_inx_t]=0; 
    ldddss16SumDiffArrayCalib[WHL][9][v_inx_t]=0;    
}
/*=================================================================================*/		            
void LDDDS_vCalcDDSCalibEachWheel(uint8_t WHL ,uint8_t v_inx_t)
{
	uint8_t temp_i;
	if(ldu32DDSCalibRef<429000)
	{
		tempW3=(int16_t)((ldu32DDSCalibRef*10000)/(ldddsu32SumDiffForCalibTemp[WHL][v_inx_t]));
		ldddss16DDSCalibTemp[WHL]=tempW3-10000;
	}
	else
	{
		tempW3=(int16_t)(((int32_t)429000*10000)/(ldddsu32SumDiffForCalibTemp[WHL][v_inx_t]))+(int16_t)(((ldu32DDSCalibRef-429000)*10000)/(ldddsu32SumDiffForCalibTemp[WHL][v_inx_t]));
		ldddss16DDSCalibTemp[WHL]=tempW3-10000;		
	}
    if(ldddsu8CalcCalibArrayCnt[v_inx_t]>=9)
    {
    	DDS_flags=106;
    	if(ldddsu8CalcCalibArrayCnt[v_inx_t]==9)
    	{
    		DDS_flags=107;

        	ldddss16SumDiffArrayCalib[WHL][9][v_inx_t]=ldddss16DDSCalibTemp[WHL];		        	      		
    		DDS_max[WHL]=ldddss16SumDiffArrayCalib[WHL][9][v_inx_t];
    		DDS_min[WHL]=ldddss16SumDiffArrayCalib[WHL][9][v_inx_t];
    		temp_sum[WHL]=ldddss16SumDiffArrayCalib[WHL][9][v_inx_t];
    		
            if(DDS_max[WHL]<ldddss16SumDiffArrayCalib[WHL][0][v_inx_t])
            {
                DDS_max[WHL]=ldddss16SumDiffArrayCalib[WHL][0][v_inx_t];
            }
            else if(DDS_min[WHL]>ldddss16SumDiffArrayCalib[WHL][0][v_inx_t])
            {
                DDS_min[WHL]=ldddss16SumDiffArrayCalib[WHL][0][v_inx_t];
            }
            else
            {
                ;
            } 	
            temp_sum[WHL]+=(int32_t)(ldddss16SumDiffArrayCalib[WHL][0][v_inx_t]);       		

            if(DDS_max[WHL]<ldddss16SumDiffArrayCalib[WHL][1][v_inx_t])
            {
                DDS_max[WHL]=ldddss16SumDiffArrayCalib[WHL][1][v_inx_t];
            }
            else if(DDS_min[WHL]>ldddss16SumDiffArrayCalib[WHL][1][v_inx_t])
            {
                DDS_min[WHL]=ldddss16SumDiffArrayCalib[WHL][1][v_inx_t];
            }
            else
            {
                ;
            } 	
            temp_sum[WHL]+=(int32_t)(ldddss16SumDiffArrayCalib[WHL][1][v_inx_t]);       	        

            if(DDS_max[WHL]<ldddss16SumDiffArrayCalib[WHL][2][v_inx_t])
            {
                DDS_max[WHL]=ldddss16SumDiffArrayCalib[WHL][2][v_inx_t];
            }
            else if(DDS_min[WHL]>ldddss16SumDiffArrayCalib[WHL][2][v_inx_t])
            {
                DDS_min[WHL]=ldddss16SumDiffArrayCalib[WHL][2][v_inx_t];
            }
            else
            {
                ;
            } 	
            temp_sum[WHL]+=(int32_t)(ldddss16SumDiffArrayCalib[WHL][2][v_inx_t]);       
            
            if(DDS_max[WHL]<ldddss16SumDiffArrayCalib[WHL][3][v_inx_t])
            {
                DDS_max[WHL]=ldddss16SumDiffArrayCalib[WHL][3][v_inx_t];
            }
            else if(DDS_min[WHL]>ldddss16SumDiffArrayCalib[WHL][3][v_inx_t])
            {
                DDS_min[WHL]=ldddss16SumDiffArrayCalib[WHL][3][v_inx_t];
            }
            else
            {
                ;
            } 	
            temp_sum[WHL]+=(int32_t)(ldddss16SumDiffArrayCalib[WHL][3][v_inx_t]);       
            
            if(DDS_max[WHL]<ldddss16SumDiffArrayCalib[WHL][4][v_inx_t])
            {
                DDS_max[WHL]=ldddss16SumDiffArrayCalib[WHL][4][v_inx_t];
            }
            else if(DDS_min[WHL]>ldddss16SumDiffArrayCalib[WHL][4][v_inx_t])
            {
                DDS_min[WHL]=ldddss16SumDiffArrayCalib[WHL][4][v_inx_t];
            }
            else
            {
                ;
            } 	
            temp_sum[WHL]+=(int32_t)(ldddss16SumDiffArrayCalib[WHL][4][v_inx_t]);       
            
            if(DDS_max[WHL]<ldddss16SumDiffArrayCalib[WHL][5][v_inx_t])
            {
                DDS_max[WHL]=ldddss16SumDiffArrayCalib[WHL][5][v_inx_t];
            }
            else if(DDS_min[WHL]>ldddss16SumDiffArrayCalib[WHL][5][v_inx_t])
            {
                DDS_min[WHL]=ldddss16SumDiffArrayCalib[WHL][5][v_inx_t];
            }
            else
            {
                ;
            } 	
            temp_sum[WHL]+=(int32_t)(ldddss16SumDiffArrayCalib[WHL][5][v_inx_t]);       
            
            if(DDS_max[WHL]<ldddss16SumDiffArrayCalib[WHL][6][v_inx_t])
            {
                DDS_max[WHL]=ldddss16SumDiffArrayCalib[WHL][6][v_inx_t];
            }
            else if(DDS_min[WHL]>ldddss16SumDiffArrayCalib[WHL][6][v_inx_t])
            {
                DDS_min[WHL]=ldddss16SumDiffArrayCalib[WHL][6][v_inx_t];
            }
            else
            {
                ;
            } 	
            temp_sum[WHL]+=(int32_t)(ldddss16SumDiffArrayCalib[WHL][6][v_inx_t]);       
            
            if(DDS_max[WHL]<ldddss16SumDiffArrayCalib[WHL][7][v_inx_t])
            {
                DDS_max[WHL]=ldddss16SumDiffArrayCalib[WHL][7][v_inx_t];
            }
            else if(DDS_min[WHL]>ldddss16SumDiffArrayCalib[WHL][7][v_inx_t])
            {
                DDS_min[WHL]=ldddss16SumDiffArrayCalib[WHL][7][v_inx_t];
            }
            else
            {
                ;
            } 	
            temp_sum[WHL]+=(int32_t)(ldddss16SumDiffArrayCalib[WHL][7][v_inx_t]);       
            
            if(DDS_max[WHL]<ldddss16SumDiffArrayCalib[WHL][8][v_inx_t])
            {
                DDS_max[WHL]=ldddss16SumDiffArrayCalib[WHL][8][v_inx_t];
            }
            else if(DDS_min[WHL]>ldddss16SumDiffArrayCalib[WHL][8][v_inx_t])
            {
                DDS_min[WHL]=ldddss16SumDiffArrayCalib[WHL][8][v_inx_t];
            }
            else
            {
                ;
            } 	
            temp_sum[WHL]+=(int32_t)(ldddss16SumDiffArrayCalib[WHL][8][v_inx_t]);       
            	        
	        WheelDiffMinMax[WHL][v_inx_t]=DDS_max[WHL]-DDS_min[WHL];	        	    		        	
	    }
	    else 
	    {
	    	DDS_flags=108;
		        			    	
     		DDS_max[WHL]=ldddss16DDSCalibTemp[WHL];
    		DDS_min[WHL]=ldddss16DDSCalibTemp[WHL];
    		temp_sum[WHL]=(int32_t)(ldddss16DDSCalibTemp[WHL]);
        		
    		ldddss16SumDiffArrayCalib[WHL][0][v_inx_t]=ldddss16SumDiffArrayCalib[WHL][1][v_inx_t];
    		ldddss16SumDiffArrayCalib[WHL][1][v_inx_t]=ldddss16SumDiffArrayCalib[WHL][2][v_inx_t];
    		ldddss16SumDiffArrayCalib[WHL][2][v_inx_t]=ldddss16SumDiffArrayCalib[WHL][3][v_inx_t];
    		ldddss16SumDiffArrayCalib[WHL][3][v_inx_t]=ldddss16SumDiffArrayCalib[WHL][4][v_inx_t];
    		ldddss16SumDiffArrayCalib[WHL][4][v_inx_t]=ldddss16SumDiffArrayCalib[WHL][5][v_inx_t];
    		ldddss16SumDiffArrayCalib[WHL][5][v_inx_t]=ldddss16SumDiffArrayCalib[WHL][6][v_inx_t];
    		ldddss16SumDiffArrayCalib[WHL][6][v_inx_t]=ldddss16SumDiffArrayCalib[WHL][7][v_inx_t];
    		ldddss16SumDiffArrayCalib[WHL][7][v_inx_t]=ldddss16SumDiffArrayCalib[WHL][8][v_inx_t];
    		ldddss16SumDiffArrayCalib[WHL][8][v_inx_t]=ldddss16SumDiffArrayCalib[WHL][9][v_inx_t];
    		ldddss16SumDiffArrayCalib[WHL][9][v_inx_t]=ldddss16DDSCalibTemp[WHL];
    		
    		DDS_max[WHL]=ldddss16SumDiffArrayCalib[WHL][9][v_inx_t];
    		DDS_min[WHL]=ldddss16SumDiffArrayCalib[WHL][9][v_inx_t];
    		temp_sum[WHL]=ldddss16SumDiffArrayCalib[WHL][9][v_inx_t];
    		
            if(DDS_max[WHL]<ldddss16SumDiffArrayCalib[WHL][0][v_inx_t])
            {
                DDS_max[WHL]=ldddss16SumDiffArrayCalib[WHL][0][v_inx_t];
            }
            else if(DDS_min[WHL]>ldddss16SumDiffArrayCalib[WHL][0][v_inx_t])
            {
                DDS_min[WHL]=ldddss16SumDiffArrayCalib[WHL][0][v_inx_t];
            }
            else
            {
                ;
            } 	
            temp_sum[WHL]+=(int32_t)(ldddss16SumDiffArrayCalib[WHL][0][v_inx_t]);       		

            if(DDS_max[WHL]<ldddss16SumDiffArrayCalib[WHL][1][v_inx_t])
            {
                DDS_max[WHL]=ldddss16SumDiffArrayCalib[WHL][1][v_inx_t];
            }
            else if(DDS_min[WHL]>ldddss16SumDiffArrayCalib[WHL][1][v_inx_t])
            {
                DDS_min[WHL]=ldddss16SumDiffArrayCalib[WHL][1][v_inx_t];
            }
            else
            {
                ;
            } 	
            temp_sum[WHL]+=(int32_t)(ldddss16SumDiffArrayCalib[WHL][1][v_inx_t]);       	        

            if(DDS_max[WHL]<ldddss16SumDiffArrayCalib[WHL][2][v_inx_t])
            {
                DDS_max[WHL]=ldddss16SumDiffArrayCalib[WHL][2][v_inx_t];
            }
            else if(DDS_min[WHL]>ldddss16SumDiffArrayCalib[WHL][2][v_inx_t])
            {
                DDS_min[WHL]=ldddss16SumDiffArrayCalib[WHL][2][v_inx_t];
            }
            else
            {
                ;
            } 	
            temp_sum[WHL]+=(int32_t)(ldddss16SumDiffArrayCalib[WHL][2][v_inx_t]);       
            
            if(DDS_max[WHL]<ldddss16SumDiffArrayCalib[WHL][3][v_inx_t])
            {
                DDS_max[WHL]=ldddss16SumDiffArrayCalib[WHL][3][v_inx_t];
            }
            else if(DDS_min[WHL]>ldddss16SumDiffArrayCalib[WHL][3][v_inx_t])
            {
                DDS_min[WHL]=ldddss16SumDiffArrayCalib[WHL][3][v_inx_t];
            }
            else
            {
                ;
            } 	
            temp_sum[WHL]+=(int32_t)(ldddss16SumDiffArrayCalib[WHL][3][v_inx_t]);       
            
            if(DDS_max[WHL]<ldddss16SumDiffArrayCalib[WHL][4][v_inx_t])
            {
                DDS_max[WHL]=ldddss16SumDiffArrayCalib[WHL][4][v_inx_t];
            }
            else if(DDS_min[WHL]>ldddss16SumDiffArrayCalib[WHL][4][v_inx_t])
            {
                DDS_min[WHL]=ldddss16SumDiffArrayCalib[WHL][4][v_inx_t];
            }
            else
            {
                ;
            } 	
            temp_sum[WHL]+=(int32_t)(ldddss16SumDiffArrayCalib[WHL][4][v_inx_t]);       
            
            if(DDS_max[WHL]<ldddss16SumDiffArrayCalib[WHL][5][v_inx_t])
            {
                DDS_max[WHL]=ldddss16SumDiffArrayCalib[WHL][5][v_inx_t];
            }
            else if(DDS_min[WHL]>ldddss16SumDiffArrayCalib[WHL][5][v_inx_t])
            {
                DDS_min[WHL]=ldddss16SumDiffArrayCalib[WHL][5][v_inx_t];
            }
            else
            {
                ;
            } 	
            temp_sum[WHL]+=(int32_t)(ldddss16SumDiffArrayCalib[WHL][5][v_inx_t]);       
            
            if(DDS_max[WHL]<ldddss16SumDiffArrayCalib[WHL][6][v_inx_t])
            {
                DDS_max[WHL]=ldddss16SumDiffArrayCalib[WHL][6][v_inx_t];
            }
            else if(DDS_min[WHL]>ldddss16SumDiffArrayCalib[WHL][6][v_inx_t])
            {
                DDS_min[WHL]=ldddss16SumDiffArrayCalib[WHL][6][v_inx_t];
            }
            else
            {
                ;
            } 	
            temp_sum[WHL]+=(int32_t)(ldddss16SumDiffArrayCalib[WHL][6][v_inx_t]);       
            
            if(DDS_max[WHL]<ldddss16SumDiffArrayCalib[WHL][7][v_inx_t])
            {
                DDS_max[WHL]=ldddss16SumDiffArrayCalib[WHL][7][v_inx_t];
            }
            else if(DDS_min[WHL]>ldddss16SumDiffArrayCalib[WHL][7][v_inx_t])
            {
                DDS_min[WHL]=ldddss16SumDiffArrayCalib[WHL][7][v_inx_t];
            }
            else
            {
                ;
            } 	
            temp_sum[WHL]+=(int32_t)(ldddss16SumDiffArrayCalib[WHL][7][v_inx_t]);       
            
            if(DDS_max[WHL]<ldddss16SumDiffArrayCalib[WHL][8][v_inx_t])
            {
                DDS_max[WHL]=ldddss16SumDiffArrayCalib[WHL][8][v_inx_t];
            }
            else if(DDS_min[WHL]>ldddss16SumDiffArrayCalib[WHL][8][v_inx_t])
            {
                DDS_min[WHL]=ldddss16SumDiffArrayCalib[WHL][8][v_inx_t];
            }
            else
            {
                ;
            } 	
            temp_sum[WHL]+=(int32_t)(ldddss16SumDiffArrayCalib[WHL][8][v_inx_t]);       
            	        
	        WheelDiffMinMax[WHL][v_inx_t]=DDS_max[WHL]-DDS_min[WHL];	
	        
	        temp_sum[WHL]=temp_sum[WHL]-(int32_t)DDS_max[WHL]-(int32_t)DDS_min[WHL];
	        temp_sum[WHL]=((temp_sum[WHL]*(int32_t)10)/((int32_t)8)); 
      		       	   
	    }
    }
    else
    {
    	temp_i=ldddsu8CalcCalibArrayCnt[v_inx_t];
       	ldddss16SumDiffArrayCalib[WHL][temp_i][v_inx_t]=ldddss16DDSCalibTemp[WHL];        	        
	}    		
}
/*=================================================================================*/
void LDDDS_vDetCalibCompletionBySpd(uint8_t v_inx_t, int16_t calib_dist )
{
	int16_t CALIBRATION_COMPLETION_DISTANCE;
	
	CALIBRATION_COMPLETION_DISTANCE=(int16_t)((((int32_t)MILE_PER_MITER*1000)*calib_dist/DDS_TIRE_L)/20);
	if(ldddsu8CompleteCalib[v_inx_t]==0)
	{
		if(ldddsu16CntCalibTotalRev[v_inx_t]>CALIBRATION_COMPLETION_DISTANCE)
		{
			ldddsu8CompleteCalib[v_inx_t]=1;
			ldddss16DetCalibValue[FL_WL][v_inx_t]=ldddss16CumulativeMean[FL_WL][v_inx_t];
			ldddss16DetCalibValue[FR_WL][v_inx_t]=ldddss16CumulativeMean[FR_WL][v_inx_t];
			ldddss16DetCalibValue[RL_WL][v_inx_t]=ldddss16CumulativeMean[RL_WL][v_inx_t];
			ldddss16DetCalibValue[RR_WL][v_inx_t]=ldddss16CumulativeMean[RR_WL][v_inx_t];			
		}   
	}       
	else    
	{
		;
	}	
}
/*====================================================================================*/
void LDDDS_vDetCalibCompletion(void)
{
	LDDDS_vDetCalibCompletionBySpd(LOW_SPEED_RANGE,CALIB_DIST_LOW);
	LDDDS_vDetCalibCompletionBySpd(LOW_MED_SPEED_RANGE,CALIB_DIST_LOW_MED);
	LDDDS_vDetCalibCompletionBySpd(MED_SPEED_RANGE,CALIB_DIST_MED);
	LDDDS_vDetCalibCompletionBySpd(MED_HIGH_SPEED_RANGE,CALIB_DIST_MED_HIGH);
	LDDDS_vDetCalibCompletionBySpd(HIGH_SPEED_RANGE,CALIB_DIST_HIGH);

		
	ldddsu1CompleteCalib=0;
	if(ldddsu8SpeedRangeIndex==U8_HIGH_SPEED_RANGE)
	{
		if(ldddsu8CompleteCalib[HIGH_SPEED_RANGE]==1)
		{
			ldddsu1CompleteCalib=1;
			CalibrationValueFL=ldddss16DetCalibValue[FL_WL][HIGH_SPEED_RANGE];
			CalibrationValueFR=ldddss16DetCalibValue[FR_WL][HIGH_SPEED_RANGE];
			CalibrationValueRL=ldddss16DetCalibValue[RL_WL][HIGH_SPEED_RANGE];
			CalibrationValueRR=ldddss16DetCalibValue[RR_WL][HIGH_SPEED_RANGE];			
		}
	}	
	else if(ldddsu8SpeedRangeIndex==U8_HIGH_MED_SPEED_RANGE)
	{
		if(ldddsu8CompleteCalib[HIGH_SPEED_RANGE]==1)
		{
			if(ldddsu8CompleteCalib[MED_HIGH_SPEED_RANGE]==1)
			{
				ldddsu1CompleteCalib=1;
				CalibrationValueFL=(ldddss16DetCalibValue[FL_WL][HIGH_SPEED_RANGE]+ldddss16DetCalibValue[FL_WL][MED_HIGH_SPEED_RANGE])/2;
				CalibrationValueFR=(ldddss16DetCalibValue[FR_WL][HIGH_SPEED_RANGE]+ldddss16DetCalibValue[FR_WL][MED_HIGH_SPEED_RANGE])/2;
				CalibrationValueRL=(ldddss16DetCalibValue[RL_WL][HIGH_SPEED_RANGE]+ldddss16DetCalibValue[RL_WL][MED_HIGH_SPEED_RANGE])/2;
				CalibrationValueRR=(ldddss16DetCalibValue[RR_WL][HIGH_SPEED_RANGE]+ldddss16DetCalibValue[RR_WL][MED_HIGH_SPEED_RANGE])/2;						
			}                                                                                                                    
			else                                                                                                                 
			{                   
				ldddsu1CompleteCalib=1;                                                                                                 
				CalibrationValueFL=(ldddss16DetCalibValue[FL_WL][HIGH_SPEED_RANGE]);
				CalibrationValueFR=(ldddss16DetCalibValue[FR_WL][HIGH_SPEED_RANGE]);
				CalibrationValueRL=(ldddss16DetCalibValue[RL_WL][HIGH_SPEED_RANGE]);
				CalibrationValueRR=(ldddss16DetCalibValue[RR_WL][HIGH_SPEED_RANGE]);				
			}
		}
		else if(ldddsu8CompleteCalib[MED_HIGH_SPEED_RANGE]==1)
		{
			ldddsu1CompleteCalib=1;
			CalibrationValueFL=ldddss16DetCalibValue[FL_WL][MED_HIGH_SPEED_RANGE];
			CalibrationValueFR=ldddss16DetCalibValue[FR_WL][MED_HIGH_SPEED_RANGE];
			CalibrationValueRL=ldddss16DetCalibValue[RL_WL][MED_HIGH_SPEED_RANGE];
			CalibrationValueRR=ldddss16DetCalibValue[RR_WL][MED_HIGH_SPEED_RANGE];			
		}
		else
		{
			CalibrationValueFL=0;
			CalibrationValueFR=0;
			CalibrationValueRL=0;
			CalibrationValueRR=0;				
		}
	}
	else if(ldddsu8SpeedRangeIndex==U8_MED_HIGH_SPEED_RANGE)
	{
		if(ldddsu8CompleteCalib[MED_SPEED_RANGE]==1)
		{
			if(ldddsu8CompleteCalib[MED_HIGH_SPEED_RANGE]==1)
			{
				ldddsu1CompleteCalib=1;
				CalibrationValueFL=(ldddss16DetCalibValue[FL_WL][MED_SPEED_RANGE]+ldddss16DetCalibValue[FL_WL][MED_HIGH_SPEED_RANGE])/2;
				CalibrationValueFR=(ldddss16DetCalibValue[FR_WL][MED_SPEED_RANGE]+ldddss16DetCalibValue[FR_WL][MED_HIGH_SPEED_RANGE])/2;
				CalibrationValueRL=(ldddss16DetCalibValue[RL_WL][MED_SPEED_RANGE]+ldddss16DetCalibValue[RL_WL][MED_HIGH_SPEED_RANGE])/2;
				CalibrationValueRR=(ldddss16DetCalibValue[RR_WL][MED_SPEED_RANGE]+ldddss16DetCalibValue[RR_WL][MED_HIGH_SPEED_RANGE])/2;						
			}                                                                                                                    
			else                                                                                                                 
			{       
				ldddsu1CompleteCalib=1;                                                                                                             
				CalibrationValueFL=(ldddss16DetCalibValue[FL_WL][MED_SPEED_RANGE]);
				CalibrationValueFR=(ldddss16DetCalibValue[FR_WL][MED_SPEED_RANGE]);
				CalibrationValueRL=(ldddss16DetCalibValue[RL_WL][MED_SPEED_RANGE]);
				CalibrationValueRR=(ldddss16DetCalibValue[RR_WL][MED_SPEED_RANGE]);				
			}
		}
		else if(ldddsu8CompleteCalib[MED_HIGH_SPEED_RANGE]==1)
		{
			ldddsu1CompleteCalib=1;
			CalibrationValueFL=ldddss16DetCalibValue[FL_WL][MED_HIGH_SPEED_RANGE];
			CalibrationValueFR=ldddss16DetCalibValue[FR_WL][MED_HIGH_SPEED_RANGE];
			CalibrationValueRL=ldddss16DetCalibValue[RL_WL][MED_HIGH_SPEED_RANGE];
			CalibrationValueRR=ldddss16DetCalibValue[RR_WL][MED_HIGH_SPEED_RANGE];			
		}
		else
		{
			CalibrationValueFL=0;
			CalibrationValueFR=0;
			CalibrationValueRL=0;
			CalibrationValueRR=0;				
		}		
	}
	else if(ldddsu8SpeedRangeIndex==U8_MED_LOW_SPEED_RANGE)
	{
		if(ldddsu8CompleteCalib[MED_SPEED_RANGE]==1)
		{
			if(ldddsu8CompleteCalib[LOW_MED_SPEED_RANGE]==1)
			{
				ldddsu1CompleteCalib=1;
				CalibrationValueFL=(ldddss16DetCalibValue[FL_WL][MED_SPEED_RANGE]+ldddss16DetCalibValue[FL_WL][LOW_MED_SPEED_RANGE])/2;
				CalibrationValueFR=(ldddss16DetCalibValue[FR_WL][MED_SPEED_RANGE]+ldddss16DetCalibValue[FR_WL][LOW_MED_SPEED_RANGE])/2;
				CalibrationValueRL=(ldddss16DetCalibValue[RL_WL][MED_SPEED_RANGE]+ldddss16DetCalibValue[RL_WL][LOW_MED_SPEED_RANGE])/2;
				CalibrationValueRR=(ldddss16DetCalibValue[RR_WL][MED_SPEED_RANGE]+ldddss16DetCalibValue[RR_WL][LOW_MED_SPEED_RANGE])/2;						
			}                                                                                                                    
			else                                                                                                                 
			{        
				ldddsu1CompleteCalib=1;                                                                                                            
				CalibrationValueFL=(ldddss16DetCalibValue[FL_WL][MED_SPEED_RANGE]);
				CalibrationValueFR=(ldddss16DetCalibValue[FR_WL][MED_SPEED_RANGE]);
				CalibrationValueRL=(ldddss16DetCalibValue[RL_WL][MED_SPEED_RANGE]);
				CalibrationValueRR=(ldddss16DetCalibValue[RR_WL][MED_SPEED_RANGE]);				
			}
		}
		else if(ldddsu8CompleteCalib[LOW_MED_SPEED_RANGE]==1)
		{
			ldddsu1CompleteCalib=1;
			CalibrationValueFL=ldddss16DetCalibValue[FL_WL][LOW_MED_SPEED_RANGE];
			CalibrationValueFR=ldddss16DetCalibValue[FR_WL][LOW_MED_SPEED_RANGE];
			CalibrationValueRL=ldddss16DetCalibValue[RL_WL][LOW_MED_SPEED_RANGE];
			CalibrationValueRR=ldddss16DetCalibValue[RR_WL][LOW_MED_SPEED_RANGE];			
		}
		else
		{
			CalibrationValueFL=0;
			CalibrationValueFR=0;
			CalibrationValueRL=0;
			CalibrationValueRR=0;				
		}			
	}
	else if(ldddsu8SpeedRangeIndex==U8_LOW_MED_SPEED_RANGE)
	{
		if(ldddsu8CompleteCalib[LOW_SPEED_RANGE]==1)
		{
			if(ldddsu8CompleteCalib[LOW_MED_SPEED_RANGE]==1)
			{
				ldddsu1CompleteCalib=1;
				CalibrationValueFL=(ldddss16DetCalibValue[FL_WL][LOW_SPEED_RANGE]+ldddss16DetCalibValue[FL_WL][LOW_MED_SPEED_RANGE])/2;
				CalibrationValueFR=(ldddss16DetCalibValue[FR_WL][LOW_SPEED_RANGE]+ldddss16DetCalibValue[FR_WL][LOW_MED_SPEED_RANGE])/2;
				CalibrationValueRL=(ldddss16DetCalibValue[RL_WL][LOW_SPEED_RANGE]+ldddss16DetCalibValue[RL_WL][LOW_MED_SPEED_RANGE])/2;
				CalibrationValueRR=(ldddss16DetCalibValue[RR_WL][LOW_SPEED_RANGE]+ldddss16DetCalibValue[RR_WL][LOW_MED_SPEED_RANGE])/2;						
			}                                                                                                                    
			else                                                                                                                 
			{         
				ldddsu1CompleteCalib=1;                                                                                                           
				CalibrationValueFL=(ldddss16DetCalibValue[FL_WL][LOW_SPEED_RANGE]);
				CalibrationValueFR=(ldddss16DetCalibValue[FR_WL][LOW_SPEED_RANGE]);
				CalibrationValueRL=(ldddss16DetCalibValue[RL_WL][LOW_SPEED_RANGE]);
				CalibrationValueRR=(ldddss16DetCalibValue[RR_WL][LOW_SPEED_RANGE]);				
			}
		}
		else if(ldddsu8CompleteCalib[LOW_MED_SPEED_RANGE]==1)
		{
			ldddsu1CompleteCalib=1;
			CalibrationValueFL=ldddss16DetCalibValue[FL_WL][LOW_MED_SPEED_RANGE];
			CalibrationValueFR=ldddss16DetCalibValue[FR_WL][LOW_MED_SPEED_RANGE];
			CalibrationValueRL=ldddss16DetCalibValue[RL_WL][LOW_MED_SPEED_RANGE];
			CalibrationValueRR=ldddss16DetCalibValue[RR_WL][LOW_MED_SPEED_RANGE];			
		}
		else
		{
			CalibrationValueFL=0;
			CalibrationValueFR=0;
			CalibrationValueRL=0;
			CalibrationValueRR=0;				
		}			
	}		
	else if(ldddsu8SpeedRangeIndex==U8_LOW_SPEED_RANGE)
	{
		if(ldddsu8CompleteCalib[LOW_SPEED_RANGE]==1)
		{
			ldddsu1CompleteCalib=1;
			CalibrationValueFL=ldddss16DetCalibValue[FL_WL][LOW_SPEED_RANGE];
			CalibrationValueFR=ldddss16DetCalibValue[FR_WL][LOW_SPEED_RANGE];
			CalibrationValueRL=ldddss16DetCalibValue[RL_WL][LOW_SPEED_RANGE];
			CalibrationValueRR=ldddss16DetCalibValue[RR_WL][LOW_SPEED_RANGE];			
		}
		else
		{
			CalibrationValueFL=0;
			CalibrationValueFR=0;
			CalibrationValueRL=0;
			CalibrationValueRR=0;				
		}			
	}	
	else
	{
		;
	}
	
	if((ldddsu8CompleteCalib[0]==1)&&(ldddsu8CompleteCalib[1]==1)&&(ldddsu8CompleteCalib[2]==1)
		&&(ldddsu8CompleteCalib[3]==1)&&(ldddsu8CompleteCalib[4]==1))
	{
		ldddsu1CompleteCalibAllSpeed=1;		
	}
	else
	{
		ldddsu1CompleteCalibAllSpeed=0;
	}
} 
/*====================================================================================*/

void LDDDS_vCalibrateWheelSpeed(void)
{
	if(ldddsu32SumDctInput[FL_WL][v_inx]<400000)
	{
		ldddsu32CorrectedWheelInput[FL_WL]=(((uint32_t)(CalibrationValueFL+10000))*((uint32_t)ldddsu32SumDctInput[FL_WL][v_inx])/10000);
	}
	else
	{
		ldddsu32CorrectedWheelInput[FL_WL]=(((uint32_t)(CalibrationValueFL+10000))*((uint32_t)(400000))/10000)+(((uint32_t)(CalibrationValueFL+10000))*((uint32_t)(ldddsu32SumDctInput[FL_WL][v_inx]-400000))/10000);
		
	}
	if(ldddsu32SumDctInput[FR_WL][v_inx]<400000)
	{
		ldddsu32CorrectedWheelInput[FR_WL]=(((uint32_t)(CalibrationValueFR+10000))*((uint32_t)ldddsu32SumDctInput[FR_WL][v_inx])/10000);
	}
	else
	{
		ldddsu32CorrectedWheelInput[FR_WL]=(((uint32_t)(CalibrationValueFR+10000))*((uint32_t)(400000))/10000)+(((uint32_t)(CalibrationValueFR+10000))*((uint32_t)(ldddsu32SumDctInput[FR_WL][v_inx]-400000))/10000);
		
	}
	if(ldddsu32SumDctInput[RL_WL][v_inx]<400000)
	{
		ldddsu32CorrectedWheelInput[RL_WL]=(((uint32_t)(CalibrationValueRL+10000))*((uint32_t)ldddsu32SumDctInput[RL_WL][v_inx])/10000);
	}
	else
	{
		ldddsu32CorrectedWheelInput[RL_WL]=(((uint32_t)(CalibrationValueRL+10000))*((uint32_t)(400000))/10000)+(((uint32_t)(CalibrationValueRL+10000))*((uint32_t)(ldddsu32SumDctInput[RL_WL][v_inx]-400000))/10000);

	}
	
	if(ldddsu32SumDctInput[RR_WL][v_inx]<400000)
	{
		ldddsu32CorrectedWheelInput[RR_WL]=(((uint32_t)(CalibrationValueRR+10000))*((uint32_t)ldddsu32SumDctInput[RR_WL][v_inx])/10000);
	}
	else
	{
		ldddsu32CorrectedWheelInput[RR_WL]=(((uint32_t)(CalibrationValueRR+10000))*((uint32_t)(400000))/10000)+(((uint32_t)(CalibrationValueRR+10000))*((uint32_t)(ldddsu32SumDctInput[RR_WL][v_inx]-400000))/10000);
		
	}
}
/*====================================================================================*/
int16_t LDDDS_vCalcWhlSpdDiffRatio(uint32_t input_1 ,uint32_t input_2)
{
    LONG temp_diff;
    /*
	if(input_1<429000)
	{
		tempW3=(int16_t)(((input_1*10000)/input_2)-10000);
	}
	else
	{
		tempW3=(int16_t)((((int32_t)429000*10000)/input_2)-10000)+(int16_t)((((input_1-429000)*10000)/input_2)-10000);
	}
	*/
	if(input_1>input_2){
	    temp_diff=(int32_t)(input_1-input_2);
	    tempW3=(int16_t)((temp_diff*10000)/input_2);
	}
	else{
	    temp_diff=(int32_t)(input_2-input_1);
	    tempW3=(int16_t)((temp_diff*10000)/input_2);	
	    tempW3=-tempW3;    
	}
	if(tempW3>150)
	{
		tempW3=150;
	}
	else if(tempW3<-150)
	{
		tempW3=-150;
	}
	else
	{
		;
	}	
	
	return tempW3;
}

/*====================================================================================*/
void LDDDS_CalcDDSEachWlParameter(uint8_t WHL , uint8_t v_inx_t)
{
		uint16_t ldddsu16YawCrtWheelInput;

	if(ldddsu8ElapsedTimeUpdateFlg[WHL]==1) /*1 scan set,reset 되는 flag 참조*/
	{
		if(ldddsu8DctSamplingDistCnt[WHL][v_inx_t]<U8_DCT_SAMPLING_REV)
		{
/*			
			#if __VDC
			ldddsu16YawCrtWheelInput=(uint16_t)(((int32_t)ldddsu16ElapsedTimePerRev[WHL]*(int32_t)(abs(yaw_out)))/(int32_t)32/(int32_t)vref_resol_change);
			
			if(yaw_out>0)
			{
				if((WHL==FL_WL)||(WHL==RL_WL))
				{
					ldddsu16ElapsedTimePerRev2[WHL]=ldddsu16ElapsedTimePerRev[WHL]-ldddsu16YawCrtWheelInput;
				}
				else
				{
					ldddsu16ElapsedTimePerRev2[WHL]=ldddsu16ElapsedTimePerRev[WHL]+ldddsu16YawCrtWheelInput;
				}
			}
			else
			{
				if((WHL==FL_WL)||(WHL==RL_WL))
				{
					ldddsu16ElapsedTimePerRev2[WHL]=ldddsu16ElapsedTimePerRev[WHL]+ldddsu16YawCrtWheelInput;
				}
				else
				{
					ldddsu16ElapsedTimePerRev2[WHL]=ldddsu16ElapsedTimePerRev[WHL]-ldddsu16YawCrtWheelInput;
				}				
			}	
			#endif
*/					

			ldddsu8DctSamplingDistCnt[WHL][v_inx_t]++;
			ldddsu32SumDctInput[WHL][v_inx_t]+=(uint32_t)ldddsu16ElapsedTimePerRev[WHL];	
		}
		else
		{
			;
		}
	}
}

/*====================================================================================*/
void LDDDS_vCalcDDSDctPara(void)
{
	int16_t lddds16AxleDiffRatio[4];
	int16_t lddds16SideDiffRatio[4];
	int16_t lddds16DiagDiffRatio[4];
    int8_t jj,whl_i;
	
//	int16_t AxleDiffMax[4];
//	int16_t SideDiffMax[4];
//	int16_t DiagDiffMax[4];	
//	int16_t AxleDiffMin[4];
//	int16_t SideDiffMin[4];
//	int16_t DiagDiffMin[4];
//    int16_t AxleDiffMinMax[4];
//    int16_t SideDiffMinMax[4];
//    int16_t DiagDiffMinMax[4];                  
                    
		            
//	int32_t temp_sum_Axle[4];
//	int32_t temp_sum_Side[4];
//	int32_t temp_sum_Diag[4];
		
//
	
		
	if((ldddsu8SpeedRangeIndex==U8_LOW_SPEED_RANGE)||(ldddsu8SpeedRangeIndex==U8_LOW_MED_SPEED_RANGE))
	{
		v_inx=0;
	}
	else if((ldddsu8SpeedRangeIndex==U8_MED_LOW_SPEED_RANGE)||(ldddsu8SpeedRangeIndex==U8_MED_HIGH_SPEED_RANGE))
	{
		v_inx=1;
	}
	else if((ldddsu8SpeedRangeIndex==U8_HIGH_MED_SPEED_RANGE)||(ldddsu8SpeedRangeIndex==U8_HIGH_SPEED_RANGE))
	{
		v_inx=2;
	}
	else
	{
		;
	}

	
	LDDDS_CalcDDSEachWlParameter(FL_WL,v_inx);
	LDDDS_CalcDDSEachWlParameter(FR_WL,v_inx);
	LDDDS_CalcDDSEachWlParameter(RL_WL,v_inx);
	LDDDS_CalcDDSEachWlParameter(RR_WL,v_inx);
	 
	if((ldddsu8DctSamplingDistCnt[FL_WL][v_inx]>=U8_DCT_SAMPLING_REV)&&
		(ldddsu8DctSamplingDistCnt[FR_WL][v_inx]>=U8_DCT_SAMPLING_REV)&&
		(ldddsu8DctSamplingDistCnt[RL_WL][v_inx]>=U8_DCT_SAMPLING_REV)&&
		(ldddsu8DctSamplingDistCnt[RR_WL][v_inx]>=U8_DCT_SAMPLING_REV))
	{
		LDDDS_vCalibrateWheelSpeed();
				
				
		ldddsu8DctSamplingDistCnt[FL_WL][v_inx]=0;
		ldddsu8DctSamplingDistCnt[FR_WL][v_inx]=0;
		ldddsu8DctSamplingDistCnt[RL_WL][v_inx]=0;
		ldddsu8DctSamplingDistCnt[RR_WL][v_inx]=0;
		ldddsu32SumDctInput[FL_WL][v_inx]=0;
		ldddsu32SumDctInput[FR_WL][v_inx]=0;
		ldddsu32SumDctInput[RL_WL][v_inx]=0;
		ldddsu32SumDctInput[RR_WL][v_inx]=0;
		ldddsu1DdsDctUpdateFlag[v_inx]=1;
	}
	
	if(ldddsu1DdsDctUpdateFlag[v_inx]==1)
	{
		if(ldddsu8DdsDctWhlInx[v_inx]==FL_WL)
		{
			lddds16AxleDiffRatio[FL_WL]=LDDDS_vCalcWhlSpdDiffRatio(ldddsu32CorrectedWheelInput[FL_WL],ldddsu32CorrectedWheelInput[FR_WL]);
			lddds16SideDiffRatio[FL_WL]=LDDDS_vCalcWhlSpdDiffRatio(ldddsu32CorrectedWheelInput[FL_WL],ldddsu32CorrectedWheelInput[RL_WL]);
			lddds16DiagDiffRatio[FL_WL]=LDDDS_vCalcWhlSpdDiffRatio(ldddsu32CorrectedWheelInput[FL_WL],ldddsu32CorrectedWheelInput[RR_WL]);
			LDDDS_vCalcDDSDctParaEachWhl(FL_WL,v_inx,lddds16AxleDiffRatio[FL_WL],lddds16SideDiffRatio[FL_WL],lddds16DiagDiffRatio[FL_WL]);		                                                                                                                 			
		}
		else if(ldddsu8DdsDctWhlInx[v_inx]==FR_WL)
		{
			lddds16AxleDiffRatio[FR_WL]=LDDDS_vCalcWhlSpdDiffRatio(ldddsu32CorrectedWheelInput[FR_WL],ldddsu32CorrectedWheelInput[FL_WL]);
			lddds16SideDiffRatio[FR_WL]=LDDDS_vCalcWhlSpdDiffRatio(ldddsu32CorrectedWheelInput[FR_WL],ldddsu32CorrectedWheelInput[RR_WL]);
			lddds16DiagDiffRatio[FR_WL]=LDDDS_vCalcWhlSpdDiffRatio(ldddsu32CorrectedWheelInput[FR_WL],ldddsu32CorrectedWheelInput[RL_WL]);
			LDDDS_vCalcDDSDctParaEachWhl(FR_WL,v_inx,lddds16AxleDiffRatio[FR_WL],lddds16SideDiffRatio[FR_WL],lddds16DiagDiffRatio[FR_WL]);			                                                                                                             			
		}
		else if(ldddsu8DdsDctWhlInx[v_inx]==RL_WL)
		{
			lddds16AxleDiffRatio[RL_WL]=LDDDS_vCalcWhlSpdDiffRatio(ldddsu32CorrectedWheelInput[RL_WL],ldddsu32CorrectedWheelInput[RR_WL]);
			lddds16SideDiffRatio[RL_WL]=LDDDS_vCalcWhlSpdDiffRatio(ldddsu32CorrectedWheelInput[RL_WL],ldddsu32CorrectedWheelInput[FL_WL]);
			lddds16DiagDiffRatio[RL_WL]=LDDDS_vCalcWhlSpdDiffRatio(ldddsu32CorrectedWheelInput[RL_WL],ldddsu32CorrectedWheelInput[FR_WL]);
			LDDDS_vCalcDDSDctParaEachWhl(RL_WL,v_inx,lddds16AxleDiffRatio[RL_WL],lddds16SideDiffRatio[RL_WL],lddds16DiagDiffRatio[RL_WL]);			                                                                                                             
			
		}
		else if(ldddsu8DdsDctWhlInx[v_inx]==RR_WL)
		{
			lddds16AxleDiffRatio[RR_WL]=LDDDS_vCalcWhlSpdDiffRatio(ldddsu32CorrectedWheelInput[RR_WL],ldddsu32CorrectedWheelInput[RL_WL]);
			lddds16SideDiffRatio[RR_WL]=LDDDS_vCalcWhlSpdDiffRatio(ldddsu32CorrectedWheelInput[RR_WL],ldddsu32CorrectedWheelInput[FR_WL]);
			lddds16DiagDiffRatio[RR_WL]=LDDDS_vCalcWhlSpdDiffRatio(ldddsu32CorrectedWheelInput[RR_WL],ldddsu32CorrectedWheelInput[FL_WL]);
			LDDDS_vCalcDDSDctParaEachWhl(RR_WL,v_inx,lddds16AxleDiffRatio[RR_WL],lddds16SideDiffRatio[RR_WL],lddds16DiagDiffRatio[RR_WL]);		
		}	
		else
		{
			ldddsu1DdsDctUpdateFlag[v_inx]=0;
	        ldddsu8CalcDctTimer[v_inx]++;
	        if(ldddsu8CalcDctTimer[v_inx]>=5)
	        {
	        	ldddsu8CalcDctTimer[v_inx]=0;//	        }			
	        
//			if((ldddsu8DdsDctUpdateOkFlg[FL_WL]==1)&&(ldddsu8DdsDctUpdateOkFlg[FR_WL]==1)
//				&&(ldddsu8DdsDctUpdateOkFlg[RL_WL]==1)&&(ldddsu8DdsDctUpdateOkFlg[RR_WL]==1)){
//			
                jj=ldddsu8CalcDctTimer2[v_inx];
                for(whl_i=0;whl_i<4;whl_i++){
                    ldddss16AxleDiffRunningAvgBuf[whl_i][jj][v_inx]=(int16_t)(temp_sum_Axle[whl_i]);
                    ldddss16SideDiffRunningAvgBuf[whl_i][jj][v_inx]=(int16_t)(temp_sum_Side[whl_i]);
                    ldddss16DiagDiffRunningAvgBuf[whl_i][jj][v_inx]=(int16_t)(temp_sum_Diag[whl_i]);
                }
				ldddsu1RunningAvgUpdateFlag=1;
				
				ldddsu8CalcDctTimer2[v_inx]++;
	            if(ldddsu8CalcDctTimer2[v_inx]>=5){
	                ldddsu8CalcDctTimer2[v_inx]=0;
	            }
	            for(whl_i=0;whl_i<4;whl_i++){
    	            temp_sum_Axle[whl_i]=ldddss16AxleDiffRunningAvgBuf[whl_i][0][v_inx];
    	            temp_sum_Axle[whl_i]+=ldddss16AxleDiffRunningAvgBuf[whl_i][1][v_inx];
    	            temp_sum_Axle[whl_i]+=ldddss16AxleDiffRunningAvgBuf[whl_i][2][v_inx];
    	            temp_sum_Axle[whl_i]+=ldddss16AxleDiffRunningAvgBuf[whl_i][3][v_inx];
    	            temp_sum_Axle[whl_i]+=ldddss16AxleDiffRunningAvgBuf[whl_i][4][v_inx];    	            
    	            temp_sum_Side[whl_i]=ldddss16SideDiffRunningAvgBuf[whl_i][0][v_inx];
    	            temp_sum_Side[whl_i]+=ldddss16SideDiffRunningAvgBuf[whl_i][1][v_inx];
    	            temp_sum_Side[whl_i]+=ldddss16SideDiffRunningAvgBuf[whl_i][2][v_inx];
    	            temp_sum_Side[whl_i]+=ldddss16SideDiffRunningAvgBuf[whl_i][3][v_inx];
    	            temp_sum_Side[whl_i]+=ldddss16SideDiffRunningAvgBuf[whl_i][4][v_inx];    	            
    	            temp_sum_Diag[whl_i]=ldddss16DiagDiffRunningAvgBuf[whl_i][0][v_inx];
    	            temp_sum_Diag[whl_i]+=ldddss16DiagDiffRunningAvgBuf[whl_i][1][v_inx];
    	            temp_sum_Diag[whl_i]+=ldddss16DiagDiffRunningAvgBuf[whl_i][2][v_inx];
    	            temp_sum_Diag[whl_i]+=ldddss16DiagDiffRunningAvgBuf[whl_i][3][v_inx];
    	            temp_sum_Diag[whl_i]+=ldddss16DiagDiffRunningAvgBuf[whl_i][4][v_inx];    	            
                }
	        	ldddss16AxleDiffRunningAvg[FL_WL][v_inx]=(int16_t)(temp_sum_Axle[FL_WL]);	 
		        ldddss16SideDiffRunningAvg[FL_WL][v_inx]=(int16_t)(temp_sum_Side[FL_WL]);    
		        ldddss16DiagDiffRunningAvg[FL_WL][v_inx]=(int16_t)(temp_sum_Diag[FL_WL]);    
		                                                                           
	        	ldddss16AxleDiffRunningAvg[FR_WL][v_inx]=(int16_t)(temp_sum_Axle[FR_WL]);	 
		        ldddss16SideDiffRunningAvg[FR_WL][v_inx]=(int16_t)(temp_sum_Side[FR_WL]);    
		        ldddss16DiagDiffRunningAvg[FR_WL][v_inx]=(int16_t)(temp_sum_Diag[FR_WL]);  
		                                                                          
	        	ldddss16AxleDiffRunningAvg[RL_WL][v_inx]=(int16_t)(temp_sum_Axle[RL_WL]);	 
		        ldddss16SideDiffRunningAvg[RL_WL][v_inx]=(int16_t)(temp_sum_Side[RL_WL]);    
		        ldddss16DiagDiffRunningAvg[RL_WL][v_inx]=(int16_t)(temp_sum_Diag[RL_WL]);  
		                                                                           
	        	ldddss16AxleDiffRunningAvg[RR_WL][v_inx]=(int16_t)(temp_sum_Axle[RR_WL]);	 
		        ldddss16SideDiffRunningAvg[RR_WL][v_inx]=(int16_t)(temp_sum_Side[RR_WL]);    
		        ldddss16DiagDiffRunningAvg[RR_WL][v_inx]=(int16_t)(temp_sum_Diag[RR_WL]);  		        		        		        					    
			}  	        
		}	
		
		ldddsu8DdsDctWhlInx[v_inx]++;
		if(ldddsu8DdsDctWhlInx[v_inx]==5)
		{
			ldddsu8DdsDctWhlInx[v_inx]=0;
		}                    		     		  		
	}
	else
	{
		;
	}
		

}
/*====================================================================================*/
void LDDDS_vCalcDDSDctParaEachWhl(uint8_t WHL , uint8_t v_inx_t ,int16_t Axle_Diff ,int16_t Side_Diff ,int16_t Diag_Diff)
{
	uint8_t temp_i;
	uint8_t i;
	int16_t AxleDiffMax[4];
	int16_t SideDiffMax[4];
	int16_t DiagDiffMax[4];	
	int16_t AxleDiffMin[4];
	int16_t SideDiffMin[4];
	int16_t DiagDiffMin[4];

	
	temp_i=ldddsu8CalcDctTimer[v_inx];
	ldddss16SumAxleDiffArrayDct[WHL][temp_i][v_inx_t]=Axle_Diff;
	ldddss16SumSideDiffArrayDct[WHL][temp_i][v_inx_t]=Side_Diff;
	ldddss16SumDiagDiffArrayDct[WHL][temp_i][v_inx_t]=Diag_Diff;
	
	AxleDiffMax[WHL]=Axle_Diff;      	
    SideDiffMax[WHL]=Side_Diff;      	
   	DiagDiffMax[WHL]=Diag_Diff;      	

    AxleDiffMin[WHL]=Axle_Diff;  
    SideDiffMin[WHL]=Side_Diff;  
   	DiagDiffMin[WHL]=Diag_Diff;  
   	
   	temp_sum_Axle[WHL]=0;
	temp_sum_Side[WHL]=0;
	temp_sum_Diag[WHL]=0;	        		
    		
    			
	for(i=0;i<5;i++)
	{
    	temp_sum_Axle[WHL]+=(int32_t)ldddss16SumAxleDiffArrayDct[WHL][i][v_inx_t];
    	temp_sum_Side[WHL]+=(int32_t)ldddss16SumSideDiffArrayDct[WHL][i][v_inx_t];
    	temp_sum_Diag[WHL]+=(int32_t)ldddss16SumDiagDiffArrayDct[WHL][i][v_inx_t];	            
                                                                            
        if(AxleDiffMax[WHL]<ldddss16SumAxleDiffArrayDct[WHL][i][v_inx_t])
        {
            AxleDiffMax[WHL]=ldddss16SumAxleDiffArrayDct[WHL][i][v_inx_t];
        }
        else if(AxleDiffMin[WHL]>ldddss16SumAxleDiffArrayDct[WHL][i][v_inx_t])
        {
            AxleDiffMin[WHL]=ldddss16SumAxleDiffArrayDct[WHL][i][v_inx_t];
        }
        else
        {
            ;
        }  
        
        if(SideDiffMax[WHL]<ldddss16SumSideDiffArrayDct[WHL][i][v_inx_t])
        {
            SideDiffMax[WHL]=ldddss16SumSideDiffArrayDct[WHL][i][v_inx_t];
        }
        else if(SideDiffMin[WHL]>ldddss16SumSideDiffArrayDct[WHL][i][v_inx_t])
        {
            SideDiffMin[WHL]=ldddss16SumSideDiffArrayDct[WHL][i][v_inx_t];
        }
        else
        {
            ;
        }  
        
        if(DiagDiffMax[WHL]<ldddss16SumDiagDiffArrayDct[WHL][i][v_inx_t])
        {
            DiagDiffMax[WHL]=ldddss16SumDiagDiffArrayDct[WHL][i][v_inx_t];
        }
        else if(DiagDiffMin[WHL]>ldddss16SumDiagDiffArrayDct[WHL][i][v_inx_t])
        {
            DiagDiffMin[WHL]=ldddss16SumDiagDiffArrayDct[WHL][i][v_inx_t];
        }
        else
        {
            ;
        }  	            	            		 		
	} 
    AxleDiffMinMax[WHL]=AxleDiffMax[WHL]-AxleDiffMin[WHL];                                
    SideDiffMinMax[WHL]=SideDiffMax[WHL]-SideDiffMin[WHL];                                
    DiagDiffMinMax[WHL]=DiagDiffMax[WHL]-DiagDiffMin[WHL];
    
  	temp_sum_Axle[WHL]=(temp_sum_Axle[WHL]-AxleDiffMax[WHL]-AxleDiffMin[WHL])/3;
	temp_sum_Side[WHL]=(temp_sum_Side[WHL]-SideDiffMax[WHL]-SideDiffMin[WHL])/3;
	temp_sum_Diag[WHL]=(temp_sum_Diag[WHL]-DiagDiffMax[WHL]-DiagDiffMin[WHL])/3;	            
   
   
   /*
    if((AxleDiffMinMax[WHL]<250)&&(SideDiffMinMax[WHL]<250)&&(DiagDiffMinMax[WHL]<250))
    {
    	ldddsu8DdsDctUpdateOkFlg[WHL]=1;
    }
    else
    {
    	ldddsu8DdsDctUpdateOkFlg[WHL]=0;
    }    
    */  
}            
/*====================================================================================*/            
void LDDDS_vSetTyreDefEstPara(void)
{
	
	if(v_inx==0)
	{
		tempW0=U8_DDS_LOW_SPEED-U8_MIN_SPEED_FOR_DDS;		
		tempW2=vref_resol_change-U8_MIN_SPEED_FOR_DDS;

		ldddss16AxleDiffThr[Def_Inx_0][FRONT]=  DEF_0Pro_AXLEDIF_SPEED_MIN_FRT+(int16_t)(((int32_t)tempW2*(DEF_0Pro_AXLEDIF_SPEED_LOW_FRT-DEF_0Pro_AXLEDIF_SPEED_MIN_FRT))/tempW0);						
		ldddss16AxleDiffThr[Def_Inx_10][FRONT]= DEF_10Pro_AXLEDIF_SPEED_MIN_FRT+(int16_t)(((int32_t)tempW2*(DEF_10Pro_AXLEDIF_SPEED_LOW_FRT-DEF_10Pro_AXLEDIF_SPEED_MIN_FRT))/tempW0);	
     	ldddss16AxleDiffThr[Def_Inx_25][FRONT]= DEF_25Pro_AXLEDIF_SPEED_MIN_FRT+(int16_t)(((int32_t)tempW2*(DEF_25Pro_AXLEDIF_SPEED_LOW_FRT-DEF_25Pro_AXLEDIF_SPEED_MIN_FRT))/tempW0);	
     	ldddss16AxleDiffThr[Def_Inx_50][FRONT]= DEF_50Pro_AXLEDIF_SPEED_MIN_FRT+(int16_t)(((int32_t)tempW2*(DEF_50Pro_AXLEDIF_SPEED_LOW_FRT-DEF_50Pro_AXLEDIF_SPEED_MIN_FRT))/tempW0);	
     	ldddss16AxleDiffThr[Def_Inx_100][FRONT]=DEF_100Pro_AXLEDIF_SPEED_MIN_FRT+(int16_t)(((int32_t)tempW2*(DEF_100Pro_AXLEDIF_SPEED_LOW_FRT-DEF_100Pro_AXLEDIF_SPEED_MIN_FRT))/tempW0);	

		ldddss16SideDiffThr[Def_Inx_0][FRONT]=  DEF_0Pro_SIDEDIF_SPEED_MIN_FRT+(int16_t)(((int32_t)tempW2*(DEF_0Pro_SIDEDIF_SPEED_LOW_FRT-DEF_0Pro_SIDEDIF_SPEED_MIN_FRT))/tempW0);						
		ldddss16SideDiffThr[Def_Inx_10][FRONT]= DEF_10Pro_SIDEDIF_SPEED_MIN_FRT+(int16_t)(((int32_t)tempW2*(DEF_10Pro_SIDEDIF_SPEED_LOW_FRT-DEF_10Pro_SIDEDIF_SPEED_MIN_FRT))/tempW0);	
     	ldddss16SideDiffThr[Def_Inx_25][FRONT]= DEF_25Pro_SIDEDIF_SPEED_MIN_FRT+(int16_t)(((int32_t)tempW2*(DEF_25Pro_SIDEDIF_SPEED_LOW_FRT-DEF_25Pro_SIDEDIF_SPEED_MIN_FRT))/tempW0);	
     	ldddss16SideDiffThr[Def_Inx_50][FRONT]= DEF_50Pro_SIDEDIF_SPEED_MIN_FRT+(int16_t)(((int32_t)tempW2*(DEF_50Pro_SIDEDIF_SPEED_LOW_FRT-DEF_50Pro_SIDEDIF_SPEED_MIN_FRT))/tempW0);	
     	ldddss16SideDiffThr[Def_Inx_100][FRONT]=DEF_100Pro_SIDEDIF_SPEED_MIN_FRT+(int16_t)(((int32_t)tempW2*(DEF_100Pro_SIDEDIF_SPEED_LOW_FRT-DEF_100Pro_SIDEDIF_SPEED_MIN_FRT))/tempW0);	     	

		ldddss16DiagDiffThr[Def_Inx_0][FRONT]=  DEF_0Pro_DIAGDIF_SPEED_MIN_FRT+(int16_t)(((int32_t)tempW2*(DEF_0Pro_DIAGDIF_SPEED_LOW_FRT-DEF_0Pro_DIAGDIF_SPEED_MIN_FRT))/tempW0);						
		ldddss16DiagDiffThr[Def_Inx_10][FRONT]= DEF_10Pro_DIAGDIF_SPEED_MIN_FRT+(int16_t)(((int32_t)tempW2*(DEF_10Pro_DIAGDIF_SPEED_LOW_FRT-DEF_10Pro_DIAGDIF_SPEED_MIN_FRT))/tempW0);	
     	ldddss16DiagDiffThr[Def_Inx_25][FRONT]= DEF_25Pro_DIAGDIF_SPEED_MIN_FRT+(int16_t)(((int32_t)tempW2*(DEF_25Pro_DIAGDIF_SPEED_LOW_FRT-DEF_25Pro_DIAGDIF_SPEED_MIN_FRT))/tempW0);	
     	ldddss16DiagDiffThr[Def_Inx_50][FRONT]= DEF_50Pro_DIAGDIF_SPEED_MIN_FRT+(int16_t)(((int32_t)tempW2*(DEF_50Pro_DIAGDIF_SPEED_LOW_FRT-DEF_50Pro_DIAGDIF_SPEED_MIN_FRT))/tempW0);	
     	ldddss16DiagDiffThr[Def_Inx_100][FRONT]=DEF_100Pro_DIAGDIF_SPEED_MIN_FRT+(int16_t)(((int32_t)tempW2*(DEF_100Pro_DIAGDIF_SPEED_LOW_FRT-DEF_100Pro_DIAGDIF_SPEED_MIN_FRT))/tempW0);	
     	
	}
	else if(v_inx==1)
	{

		tempW0=U8_DDS_MED_SPEED-U8_DDS_LOW_SPEED;
		tempW2=vref_resol_change-U8_DDS_LOW_SPEED;
		
		ldddss16AxleDiffThr[Def_Inx_0][FRONT]=  DEF_0Pro_AXLEDIF_SPEED_LOW_FRT+(int16_t)(((int32_t)tempW2*(DEF_0Pro_AXLEDIF_SPEED_MED_FRT-DEF_0Pro_AXLEDIF_SPEED_LOW_FRT))/tempW0);						
		ldddss16AxleDiffThr[Def_Inx_10][FRONT]= DEF_10Pro_AXLEDIF_SPEED_LOW_FRT+(int16_t)(((int32_t)tempW2*(DEF_10Pro_AXLEDIF_SPEED_MED_FRT-DEF_10Pro_AXLEDIF_SPEED_LOW_FRT))/tempW0);	
     	ldddss16AxleDiffThr[Def_Inx_25][FRONT]= DEF_25Pro_AXLEDIF_SPEED_LOW_FRT+(int16_t)(((int32_t)tempW2*(DEF_25Pro_AXLEDIF_SPEED_MED_FRT-DEF_25Pro_AXLEDIF_SPEED_LOW_FRT))/tempW0);	
     	ldddss16AxleDiffThr[Def_Inx_50][FRONT]= DEF_50Pro_AXLEDIF_SPEED_LOW_FRT+(int16_t)(((int32_t)tempW2*(DEF_50Pro_AXLEDIF_SPEED_MED_FRT-DEF_50Pro_AXLEDIF_SPEED_LOW_FRT))/tempW0);	
     	ldddss16AxleDiffThr[Def_Inx_100][FRONT]=DEF_100Pro_AXLEDIF_SPEED_LOW_FRT+(int16_t)(((int32_t)tempW2*(DEF_100Pro_AXLEDIF_SPEED_MED_FRT-DEF_100Pro_AXLEDIF_SPEED_LOW_FRT))/tempW0);	
                                                
		ldddss16SideDiffThr[Def_Inx_0][FRONT]=  DEF_0Pro_SIDEDIF_SPEED_LOW_FRT+(int16_t)(((int32_t)tempW2*(DEF_0Pro_SIDEDIF_SPEED_MED_FRT-DEF_0Pro_SIDEDIF_SPEED_LOW_FRT))/tempW0);						
		ldddss16SideDiffThr[Def_Inx_10][FRONT]= DEF_10Pro_SIDEDIF_SPEED_LOW_FRT+(int16_t)(((int32_t)tempW2*(DEF_10Pro_SIDEDIF_SPEED_MED_FRT-DEF_10Pro_SIDEDIF_SPEED_LOW_FRT))/tempW0);	
     	ldddss16SideDiffThr[Def_Inx_25][FRONT]= DEF_25Pro_SIDEDIF_SPEED_LOW_FRT+(int16_t)(((int32_t)tempW2*(DEF_25Pro_SIDEDIF_SPEED_MED_FRT-DEF_25Pro_SIDEDIF_SPEED_LOW_FRT))/tempW0);	
     	ldddss16SideDiffThr[Def_Inx_50][FRONT]= DEF_50Pro_SIDEDIF_SPEED_LOW_FRT+(int16_t)(((int32_t)tempW2*(DEF_50Pro_SIDEDIF_SPEED_MED_FRT-DEF_50Pro_SIDEDIF_SPEED_LOW_FRT))/tempW0);	
     	ldddss16SideDiffThr[Def_Inx_100][FRONT]=DEF_100Pro_SIDEDIF_SPEED_LOW_FRT+(int16_t)(((int32_t)tempW2*(DEF_100Pro_SIDEDIF_SPEED_MED_FRT-DEF_100Pro_SIDEDIF_SPEED_LOW_FRT))/tempW0);	     	
                                                
		ldddss16DiagDiffThr[Def_Inx_0][FRONT]=  DEF_0Pro_DIAGDIF_SPEED_LOW_FRT+(int16_t)(((int32_t)tempW2*(DEF_0Pro_DIAGDIF_SPEED_MED_FRT-DEF_0Pro_DIAGDIF_SPEED_LOW_FRT))/tempW0);						
		ldddss16DiagDiffThr[Def_Inx_10][FRONT]= DEF_10Pro_DIAGDIF_SPEED_LOW_FRT+(int16_t)(((int32_t)tempW2*(DEF_10Pro_DIAGDIF_SPEED_MED_FRT-DEF_10Pro_DIAGDIF_SPEED_LOW_FRT))/tempW0);	
     	ldddss16DiagDiffThr[Def_Inx_25][FRONT]= DEF_25Pro_DIAGDIF_SPEED_LOW_FRT+(int16_t)(((int32_t)tempW2*(DEF_25Pro_DIAGDIF_SPEED_MED_FRT-DEF_25Pro_DIAGDIF_SPEED_LOW_FRT))/tempW0);	
     	ldddss16DiagDiffThr[Def_Inx_50][FRONT]= DEF_50Pro_DIAGDIF_SPEED_LOW_FRT+(int16_t)(((int32_t)tempW2*(DEF_50Pro_DIAGDIF_SPEED_MED_FRT-DEF_50Pro_DIAGDIF_SPEED_LOW_FRT))/tempW0);	
     	ldddss16DiagDiffThr[Def_Inx_100][FRONT]=DEF_100Pro_DIAGDIF_SPEED_LOW_FRT+(int16_t)(((int32_t)tempW2*(DEF_100Pro_DIAGDIF_SPEED_MED_FRT-DEF_100Pro_DIAGDIF_SPEED_LOW_FRT))/tempW0);			
		
	}
	else if(v_inx==2)
	{
		tempW0=U8_MAX_SPEED_FOR_DDS-U8_DDS_MED_SPEED;		
		tempW2=vref_resol_change-U8_DDS_MED_SPEED;
				
		ldddss16AxleDiffThr[Def_Inx_0][FRONT]=  DEF_0Pro_AXLEDIF_SPEED_MED_FRT+(int16_t)(((int32_t)tempW2*(DEF_0Pro_AXLEDIF_SPEED_HIGH_FRT-DEF_0Pro_AXLEDIF_SPEED_MED_FRT))/tempW0);						
		ldddss16AxleDiffThr[Def_Inx_10][FRONT]= DEF_10Pro_AXLEDIF_SPEED_MED_FRT+(int16_t)(((int32_t)tempW2*(DEF_10Pro_AXLEDIF_SPEED_HIGH_FRT-DEF_10Pro_AXLEDIF_SPEED_MED_FRT))/tempW0);	
     	ldddss16AxleDiffThr[Def_Inx_25][FRONT]= DEF_25Pro_AXLEDIF_SPEED_MED_FRT+(int16_t)(((int32_t)tempW2*(DEF_25Pro_AXLEDIF_SPEED_HIGH_FRT-DEF_25Pro_AXLEDIF_SPEED_MED_FRT))/tempW0);	
     	ldddss16AxleDiffThr[Def_Inx_50][FRONT]= DEF_50Pro_AXLEDIF_SPEED_MED_FRT+(int16_t)(((int32_t)tempW2*(DEF_50Pro_AXLEDIF_SPEED_HIGH_FRT-DEF_50Pro_AXLEDIF_SPEED_MED_FRT))/tempW0);	
     	ldddss16AxleDiffThr[Def_Inx_100][FRONT]=DEF_100Pro_AXLEDIF_SPEED_MED_FRT+(int16_t)(((int32_t)tempW2*(DEF_100Pro_AXLEDIF_SPEED_HIGH_FRT-DEF_100Pro_AXLEDIF_SPEED_MED_FRT))/tempW0);	
                                                
		ldddss16SideDiffThr[Def_Inx_0][FRONT]=  DEF_0Pro_SIDEDIF_SPEED_MED_FRT+(int16_t)(((int32_t)tempW2*(DEF_0Pro_SIDEDIF_SPEED_HIGH_FRT-DEF_0Pro_SIDEDIF_SPEED_MED_FRT))/tempW0);						
		ldddss16SideDiffThr[Def_Inx_10][FRONT]= DEF_10Pro_SIDEDIF_SPEED_MED_FRT+(int16_t)(((int32_t)tempW2*(DEF_10Pro_SIDEDIF_SPEED_HIGH_FRT-DEF_10Pro_SIDEDIF_SPEED_MED_FRT))/tempW0);	
     	ldddss16SideDiffThr[Def_Inx_25][FRONT]= DEF_25Pro_SIDEDIF_SPEED_MED_FRT+(int16_t)(((int32_t)tempW2*(DEF_25Pro_SIDEDIF_SPEED_HIGH_FRT-DEF_25Pro_SIDEDIF_SPEED_MED_FRT))/tempW0);	
     	ldddss16SideDiffThr[Def_Inx_50][FRONT]= DEF_50Pro_SIDEDIF_SPEED_MED_FRT+(int16_t)(((int32_t)tempW2*(DEF_50Pro_SIDEDIF_SPEED_HIGH_FRT-DEF_50Pro_SIDEDIF_SPEED_MED_FRT))/tempW0);	
     	ldddss16SideDiffThr[Def_Inx_100][FRONT]=DEF_100Pro_SIDEDIF_SPEED_MED_FRT+(int16_t)(((int32_t)tempW2*(DEF_100Pro_SIDEDIF_SPEED_HIGH_FRT-DEF_100Pro_SIDEDIF_SPEED_MED_FRT))/tempW0);	     	
                                                
		ldddss16DiagDiffThr[Def_Inx_0][FRONT]=  DEF_0Pro_DIAGDIF_SPEED_MED_FRT+(int16_t)(((int32_t)tempW2*(DEF_0Pro_DIAGDIF_SPEED_HIGH_FRT-DEF_0Pro_DIAGDIF_SPEED_MED_FRT))/tempW0);						
		ldddss16DiagDiffThr[Def_Inx_10][FRONT]= DEF_10Pro_DIAGDIF_SPEED_MED_FRT+(int16_t)(((int32_t)tempW2*(DEF_10Pro_DIAGDIF_SPEED_HIGH_FRT-DEF_10Pro_DIAGDIF_SPEED_MED_FRT))/tempW0);	
     	ldddss16DiagDiffThr[Def_Inx_25][FRONT]= DEF_25Pro_DIAGDIF_SPEED_MED_FRT+(int16_t)(((int32_t)tempW2*(DEF_25Pro_DIAGDIF_SPEED_HIGH_FRT-DEF_25Pro_DIAGDIF_SPEED_MED_FRT))/tempW0);	
     	ldddss16DiagDiffThr[Def_Inx_50][FRONT]= DEF_50Pro_DIAGDIF_SPEED_MED_FRT+(int16_t)(((int32_t)tempW2*(DEF_50Pro_DIAGDIF_SPEED_HIGH_FRT-DEF_50Pro_DIAGDIF_SPEED_MED_FRT))/tempW0);	
     	ldddss16DiagDiffThr[Def_Inx_100][FRONT]=DEF_100Pro_DIAGDIF_SPEED_MED_FRT+(int16_t)(((int32_t)tempW2*(DEF_100Pro_DIAGDIF_SPEED_HIGH_FRT-DEF_100Pro_DIAGDIF_SPEED_MED_FRT))/tempW0);			
	
	}
	else
	{
		;
	}	
	
	if(v_inx==0)
	{
		tempW0=U8_DDS_LOW_SPEED-U8_MIN_SPEED_FOR_DDS;		
		tempW2=vref_resol_change-U8_MIN_SPEED_FOR_DDS;

		ldddss16AxleDiffThr[Def_Inx_0][REAR]=  DEF_0Pro_AXLEDIF_SPEED_MIN_REAR+(int16_t)(((int32_t)tempW2*(DEF_0Pro_AXLEDIF_SPEED_LOW_REAR-DEF_0Pro_AXLEDIF_SPEED_MIN_REAR))/tempW0);						
		ldddss16AxleDiffThr[Def_Inx_10][REAR]= DEF_10Pro_AXLEDIF_SPEED_MIN_REAR+(int16_t)(((int32_t)tempW2*(DEF_10Pro_AXLEDIF_SPEED_LOW_REAR-DEF_10Pro_AXLEDIF_SPEED_MIN_REAR))/tempW0);	
     	ldddss16AxleDiffThr[Def_Inx_25][REAR]= DEF_25Pro_AXLEDIF_SPEED_MIN_REAR+(int16_t)(((int32_t)tempW2*(DEF_25Pro_AXLEDIF_SPEED_LOW_REAR-DEF_25Pro_AXLEDIF_SPEED_MIN_REAR))/tempW0);	
     	ldddss16AxleDiffThr[Def_Inx_50][REAR]= DEF_50Pro_AXLEDIF_SPEED_MIN_REAR+(int16_t)(((int32_t)tempW2*(DEF_50Pro_AXLEDIF_SPEED_LOW_REAR-DEF_50Pro_AXLEDIF_SPEED_MIN_REAR))/tempW0);	
     	ldddss16AxleDiffThr[Def_Inx_100][REAR]=DEF_100Pro_AXLEDIF_SPEED_MIN_REAR+(int16_t)(((int32_t)tempW2*(DEF_100Pro_AXLEDIF_SPEED_LOW_REAR-DEF_100Pro_AXLEDIF_SPEED_MIN_REAR))/tempW0);	

		ldddss16SideDiffThr[Def_Inx_0][REAR]=  DEF_0Pro_SIDEDIF_SPEED_MIN_REAR+(int16_t)(((int32_t)tempW2*(DEF_0Pro_SIDEDIF_SPEED_LOW_REAR-DEF_0Pro_SIDEDIF_SPEED_MIN_REAR))/tempW0);						
		ldddss16SideDiffThr[Def_Inx_10][REAR]= DEF_10Pro_SIDEDIF_SPEED_MIN_REAR+(int16_t)(((int32_t)tempW2*(DEF_10Pro_SIDEDIF_SPEED_LOW_REAR-DEF_10Pro_SIDEDIF_SPEED_MIN_REAR))/tempW0);	
     	ldddss16SideDiffThr[Def_Inx_25][REAR]= DEF_25Pro_SIDEDIF_SPEED_MIN_REAR+(int16_t)(((int32_t)tempW2*(DEF_25Pro_SIDEDIF_SPEED_LOW_REAR-DEF_25Pro_SIDEDIF_SPEED_MIN_REAR))/tempW0);	
     	ldddss16SideDiffThr[Def_Inx_50][REAR]= DEF_50Pro_SIDEDIF_SPEED_MIN_REAR+(int16_t)(((int32_t)tempW2*(DEF_50Pro_SIDEDIF_SPEED_LOW_REAR-DEF_50Pro_SIDEDIF_SPEED_MIN_REAR))/tempW0);	
     	ldddss16SideDiffThr[Def_Inx_100][REAR]=DEF_100Pro_SIDEDIF_SPEED_MIN_REAR+(int16_t)(((int32_t)tempW2*(DEF_100Pro_SIDEDIF_SPEED_LOW_REAR-DEF_100Pro_SIDEDIF_SPEED_MIN_REAR))/tempW0);	     	

		ldddss16DiagDiffThr[Def_Inx_0][REAR]=  DEF_0Pro_DIAGDIF_SPEED_MIN_REAR+(int16_t)(((int32_t)tempW2*(DEF_0Pro_DIAGDIF_SPEED_LOW_REAR-DEF_0Pro_DIAGDIF_SPEED_MIN_REAR))/tempW0);						
		ldddss16DiagDiffThr[Def_Inx_10][REAR]= DEF_10Pro_DIAGDIF_SPEED_MIN_REAR+(int16_t)(((int32_t)tempW2*(DEF_10Pro_DIAGDIF_SPEED_LOW_REAR-DEF_10Pro_DIAGDIF_SPEED_MIN_REAR))/tempW0);	
     	ldddss16DiagDiffThr[Def_Inx_25][REAR]= DEF_25Pro_DIAGDIF_SPEED_MIN_REAR+(int16_t)(((int32_t)tempW2*(DEF_25Pro_DIAGDIF_SPEED_LOW_REAR-DEF_25Pro_DIAGDIF_SPEED_MIN_REAR))/tempW0);	
     	ldddss16DiagDiffThr[Def_Inx_50][REAR]= DEF_50Pro_DIAGDIF_SPEED_MIN_REAR+(int16_t)(((int32_t)tempW2*(DEF_50Pro_DIAGDIF_SPEED_LOW_REAR-DEF_50Pro_DIAGDIF_SPEED_MIN_REAR))/tempW0);	
     	ldddss16DiagDiffThr[Def_Inx_100][REAR]=DEF_100Pro_DIAGDIF_SPEED_MIN_REAR+(int16_t)(((int32_t)tempW2*(DEF_100Pro_DIAGDIF_SPEED_LOW_REAR-DEF_100Pro_DIAGDIF_SPEED_MIN_REAR))/tempW0);	
     	
	}
	else if(v_inx==1)
	{

		tempW0=U8_DDS_MED_SPEED-U8_DDS_LOW_SPEED;
		tempW2=vref_resol_change-U8_DDS_LOW_SPEED;
		
		ldddss16AxleDiffThr[Def_Inx_0][REAR]=  DEF_0Pro_AXLEDIF_SPEED_LOW_REAR+(int16_t)(((int32_t)tempW2*(DEF_0Pro_AXLEDIF_SPEED_MED_REAR-DEF_0Pro_AXLEDIF_SPEED_LOW_REAR))/tempW0);						
		ldddss16AxleDiffThr[Def_Inx_10][REAR]= DEF_10Pro_AXLEDIF_SPEED_LOW_REAR+(int16_t)(((int32_t)tempW2*(DEF_10Pro_AXLEDIF_SPEED_MED_REAR-DEF_10Pro_AXLEDIF_SPEED_LOW_REAR))/tempW0);	
     	ldddss16AxleDiffThr[Def_Inx_25][REAR]= DEF_25Pro_AXLEDIF_SPEED_LOW_REAR+(int16_t)(((int32_t)tempW2*(DEF_25Pro_AXLEDIF_SPEED_MED_REAR-DEF_25Pro_AXLEDIF_SPEED_LOW_REAR))/tempW0);	
     	ldddss16AxleDiffThr[Def_Inx_50][REAR]= DEF_50Pro_AXLEDIF_SPEED_LOW_REAR+(int16_t)(((int32_t)tempW2*(DEF_50Pro_AXLEDIF_SPEED_MED_REAR-DEF_50Pro_AXLEDIF_SPEED_LOW_REAR))/tempW0);	
     	ldddss16AxleDiffThr[Def_Inx_100][REAR]=DEF_100Pro_AXLEDIF_SPEED_LOW_REAR+(int16_t)(((int32_t)tempW2*(DEF_100Pro_AXLEDIF_SPEED_MED_REAR-DEF_100Pro_AXLEDIF_SPEED_LOW_REAR))/tempW0);	
                                                
		ldddss16SideDiffThr[Def_Inx_0][REAR]=  DEF_0Pro_SIDEDIF_SPEED_LOW_REAR+(int16_t)(((int32_t)tempW2*(DEF_0Pro_SIDEDIF_SPEED_MED_REAR-DEF_0Pro_SIDEDIF_SPEED_LOW_REAR))/tempW0);						
		ldddss16SideDiffThr[Def_Inx_10][REAR]= DEF_10Pro_SIDEDIF_SPEED_LOW_REAR+(int16_t)(((int32_t)tempW2*(DEF_10Pro_SIDEDIF_SPEED_MED_REAR-DEF_10Pro_SIDEDIF_SPEED_LOW_REAR))/tempW0);	
     	ldddss16SideDiffThr[Def_Inx_25][REAR]= DEF_25Pro_SIDEDIF_SPEED_LOW_REAR+(int16_t)(((int32_t)tempW2*(DEF_25Pro_SIDEDIF_SPEED_MED_REAR-DEF_25Pro_SIDEDIF_SPEED_LOW_REAR))/tempW0);	
     	ldddss16SideDiffThr[Def_Inx_50][REAR]= DEF_50Pro_SIDEDIF_SPEED_LOW_REAR+(int16_t)(((int32_t)tempW2*(DEF_50Pro_SIDEDIF_SPEED_MED_REAR-DEF_50Pro_SIDEDIF_SPEED_LOW_REAR))/tempW0);	
     	ldddss16SideDiffThr[Def_Inx_100][REAR]=DEF_100Pro_SIDEDIF_SPEED_LOW_REAR+(int16_t)(((int32_t)tempW2*(DEF_100Pro_SIDEDIF_SPEED_MED_REAR-DEF_100Pro_SIDEDIF_SPEED_LOW_REAR))/tempW0);	     	
                                                
		ldddss16DiagDiffThr[Def_Inx_0][REAR]=  DEF_0Pro_DIAGDIF_SPEED_LOW_REAR+(int16_t)(((int32_t)tempW2*(DEF_0Pro_DIAGDIF_SPEED_MED_REAR-DEF_0Pro_DIAGDIF_SPEED_LOW_REAR))/tempW0);						
		ldddss16DiagDiffThr[Def_Inx_10][REAR]= DEF_10Pro_DIAGDIF_SPEED_LOW_REAR+(int16_t)(((int32_t)tempW2*(DEF_10Pro_DIAGDIF_SPEED_MED_REAR-DEF_10Pro_DIAGDIF_SPEED_LOW_REAR))/tempW0);	
     	ldddss16DiagDiffThr[Def_Inx_25][REAR]= DEF_25Pro_DIAGDIF_SPEED_LOW_REAR+(int16_t)(((int32_t)tempW2*(DEF_25Pro_DIAGDIF_SPEED_MED_REAR-DEF_25Pro_DIAGDIF_SPEED_LOW_REAR))/tempW0);	
     	ldddss16DiagDiffThr[Def_Inx_50][REAR]= DEF_50Pro_DIAGDIF_SPEED_LOW_REAR+(int16_t)(((int32_t)tempW2*(DEF_50Pro_DIAGDIF_SPEED_MED_REAR-DEF_50Pro_DIAGDIF_SPEED_LOW_REAR))/tempW0);	
     	ldddss16DiagDiffThr[Def_Inx_100][REAR]=DEF_100Pro_DIAGDIF_SPEED_LOW_REAR+(int16_t)(((int32_t)tempW2*(DEF_100Pro_DIAGDIF_SPEED_MED_REAR-DEF_100Pro_DIAGDIF_SPEED_LOW_REAR))/tempW0);			
		
	}
	else if(v_inx==2)
	{
		tempW0=U8_MAX_SPEED_FOR_DDS-U8_DDS_MED_SPEED;		
		tempW2=vref_resol_change-U8_DDS_MED_SPEED;
				
		ldddss16AxleDiffThr[Def_Inx_0][REAR]=  DEF_0Pro_AXLEDIF_SPEED_MED_REAR+(int16_t)(((int32_t)tempW2*(DEF_0Pro_AXLEDIF_SPEED_HIGH_REAR-DEF_0Pro_AXLEDIF_SPEED_MED_REAR))/tempW0);						
		ldddss16AxleDiffThr[Def_Inx_10][REAR]= DEF_10Pro_AXLEDIF_SPEED_MED_REAR+(int16_t)(((int32_t)tempW2*(DEF_10Pro_AXLEDIF_SPEED_HIGH_REAR-DEF_10Pro_AXLEDIF_SPEED_MED_REAR))/tempW0);	
     	ldddss16AxleDiffThr[Def_Inx_25][REAR]= DEF_25Pro_AXLEDIF_SPEED_MED_REAR+(int16_t)(((int32_t)tempW2*(DEF_25Pro_AXLEDIF_SPEED_HIGH_REAR-DEF_25Pro_AXLEDIF_SPEED_MED_REAR))/tempW0);	
     	ldddss16AxleDiffThr[Def_Inx_50][REAR]= DEF_50Pro_AXLEDIF_SPEED_MED_REAR+(int16_t)(((int32_t)tempW2*(DEF_50Pro_AXLEDIF_SPEED_HIGH_REAR-DEF_50Pro_AXLEDIF_SPEED_MED_REAR))/tempW0);	
     	ldddss16AxleDiffThr[Def_Inx_100][REAR]=DEF_100Pro_AXLEDIF_SPEED_MED_REAR+(int16_t)(((int32_t)tempW2*(DEF_100Pro_AXLEDIF_SPEED_HIGH_REAR-DEF_100Pro_AXLEDIF_SPEED_MED_REAR))/tempW0);	
                                                
		ldddss16SideDiffThr[Def_Inx_0][REAR]=  DEF_0Pro_SIDEDIF_SPEED_MED_REAR+(int16_t)(((int32_t)tempW2*(DEF_0Pro_SIDEDIF_SPEED_HIGH_REAR-DEF_0Pro_SIDEDIF_SPEED_MED_REAR))/tempW0);						
		ldddss16SideDiffThr[Def_Inx_10][REAR]= DEF_10Pro_SIDEDIF_SPEED_MED_REAR+(int16_t)(((int32_t)tempW2*(DEF_10Pro_SIDEDIF_SPEED_HIGH_REAR-DEF_10Pro_SIDEDIF_SPEED_MED_REAR))/tempW0);	
     	ldddss16SideDiffThr[Def_Inx_25][REAR]= DEF_25Pro_SIDEDIF_SPEED_MED_REAR+(int16_t)(((int32_t)tempW2*(DEF_25Pro_SIDEDIF_SPEED_HIGH_REAR-DEF_25Pro_SIDEDIF_SPEED_MED_REAR))/tempW0);	
     	ldddss16SideDiffThr[Def_Inx_50][REAR]= DEF_50Pro_SIDEDIF_SPEED_MED_REAR+(int16_t)(((int32_t)tempW2*(DEF_50Pro_SIDEDIF_SPEED_HIGH_REAR-DEF_50Pro_SIDEDIF_SPEED_MED_REAR))/tempW0);	
     	ldddss16SideDiffThr[Def_Inx_100][REAR]=DEF_100Pro_SIDEDIF_SPEED_MED_REAR+(int16_t)(((int32_t)tempW2*(DEF_100Pro_SIDEDIF_SPEED_HIGH_REAR-DEF_100Pro_SIDEDIF_SPEED_MED_REAR))/tempW0);	     	
                                                
		ldddss16DiagDiffThr[Def_Inx_0][REAR]=  DEF_0Pro_DIAGDIF_SPEED_MED_REAR+(int16_t)(((int32_t)tempW2*(DEF_0Pro_DIAGDIF_SPEED_HIGH_REAR-DEF_0Pro_DIAGDIF_SPEED_MED_REAR))/tempW0);						
		ldddss16DiagDiffThr[Def_Inx_10][REAR]= DEF_10Pro_DIAGDIF_SPEED_MED_REAR+(int16_t)(((int32_t)tempW2*(DEF_10Pro_DIAGDIF_SPEED_HIGH_REAR-DEF_10Pro_DIAGDIF_SPEED_MED_REAR))/tempW0);	
     	ldddss16DiagDiffThr[Def_Inx_25][REAR]= DEF_25Pro_DIAGDIF_SPEED_MED_REAR+(int16_t)(((int32_t)tempW2*(DEF_25Pro_DIAGDIF_SPEED_HIGH_REAR-DEF_25Pro_DIAGDIF_SPEED_MED_REAR))/tempW0);	
     	ldddss16DiagDiffThr[Def_Inx_50][REAR]= DEF_50Pro_DIAGDIF_SPEED_MED_REAR+(int16_t)(((int32_t)tempW2*(DEF_50Pro_DIAGDIF_SPEED_HIGH_REAR-DEF_50Pro_DIAGDIF_SPEED_MED_REAR))/tempW0);	
     	ldddss16DiagDiffThr[Def_Inx_100][REAR]=DEF_100Pro_DIAGDIF_SPEED_MED_REAR+(int16_t)(((int32_t)tempW2*(DEF_100Pro_DIAGDIF_SPEED_HIGH_REAR-DEF_100Pro_DIAGDIF_SPEED_MED_REAR))/tempW0);			
	
	}
	else
	{
		;
	}		
}
/*=====================================================================================*/ 
void LDDDS_vEstTireDefPress(void)
{
	if(ldddsu1RunningAvgUpdateFlag==1)
	{
		if(ldddsu8RunningAvgUpdateWhlIndex[v_inx]==FL_WL)
		{
			LDDDS_vEstTireDefPressEachWhl(FL_WL);
		}
		else if(ldddsu8RunningAvgUpdateWhlIndex[v_inx]==FR_WL)
		{
			LDDDS_vEstTireDefPressEachWhl(FR_WL);
		}
		else if(ldddsu8RunningAvgUpdateWhlIndex[v_inx]==RL_WL)
		{
			LDDDS_vEstTireDefPressEachWhl(RL_WL);
		}
		else if(ldddsu8RunningAvgUpdateWhlIndex[v_inx]==RR_WL)
		{
			LDDDS_vEstTireDefPressEachWhl(RR_WL);
		}
		else
		{
			;
		}					
	}	
}
/*=====================================================================================*/   
void LDDDS_vEstTireDefPressEachWhl(uint8_t WHL)                      
{
	if((WHL==FL_WL)||(WHL==FR_WL))
	{
		if(ldddss16AxleDiffRunningAvg[WHL][v_inx]>=0)
		{
			ldddss8PressEstByAxleDiff[WHL]=(int8_t)LCDDS_s16IInter5Point(ldddss16AxleDiffRunningAvg[WHL][v_inx], (-ldddss16AxleDiffThr[Def_Inx_0][FRONT]), -DEF_0, (-ldddss16AxleDiffThr[Def_Inx_10][FRONT]), (-DEF_10), (-ldddss16AxleDiffThr[Def_Inx_25][FRONT]), (-DEF_25), (-ldddss16AxleDiffThr[Def_Inx_50][FRONT]), (-DEF_50), (-ldddss16AxleDiffThr[Def_Inx_100][FRONT]), (-DEF_100));					
		}
		else
		{
			ldddss8PressEstByAxleDiff[WHL]=(int8_t)LCDDS_s16IInter5Point(ldddss16AxleDiffRunningAvg[WHL][v_inx],ldddss16AxleDiffThr[Def_Inx_100][FRONT], DEF_100, ldddss16AxleDiffThr[Def_Inx_50][FRONT], DEF_50, ldddss16AxleDiffThr[Def_Inx_25][FRONT], DEF_25,ldddss16AxleDiffThr[Def_Inx_10][FRONT], DEF_10, ldddss16AxleDiffThr[Def_Inx_0][FRONT], DEF_0);					
		} 
		if(ldddss16SideDiffRunningAvg[WHL][v_inx]>=0)
		{
			ldddss8PressEstBySideDiff[WHL]=(int8_t)LCDDS_s16IInter5Point(ldddss16SideDiffRunningAvg[WHL][v_inx], (-ldddss16SideDiffThr[Def_Inx_0][FRONT]), -DEF_0, (-ldddss16SideDiffThr[Def_Inx_10][FRONT]), (-DEF_10), (-ldddss16SideDiffThr[Def_Inx_25][FRONT]), (-DEF_25), (-ldddss16SideDiffThr[Def_Inx_50][FRONT]), (-DEF_50), (-ldddss16SideDiffThr[Def_Inx_100][FRONT]), (-DEF_100));					
		}
		else
		{
			ldddss8PressEstBySideDiff[WHL]=(int8_t)LCDDS_s16IInter5Point(ldddss16SideDiffRunningAvg[WHL][v_inx],ldddss16SideDiffThr[Def_Inx_100][FRONT], DEF_100, ldddss16SideDiffThr[Def_Inx_50][FRONT], DEF_50, ldddss16SideDiffThr[Def_Inx_25][FRONT], DEF_25,ldddss16SideDiffThr[Def_Inx_10][FRONT], DEF_10, ldddss16SideDiffThr[Def_Inx_0][FRONT], DEF_0);					
		} 
		if(ldddss16DiagDiffRunningAvg[WHL][v_inx]>=0)
		{
			ldddss8PressEstByDiagDiff[WHL]=(int8_t)LCDDS_s16IInter5Point(ldddss16DiagDiffRunningAvg[WHL][v_inx], (-ldddss16DiagDiffThr[Def_Inx_0][FRONT]), -DEF_0, (-ldddss16DiagDiffThr[Def_Inx_10][FRONT]), (-DEF_10), (-ldddss16DiagDiffThr[Def_Inx_25][FRONT]), (-DEF_25), (-ldddss16DiagDiffThr[Def_Inx_50][FRONT]), (-DEF_50), (-ldddss16DiagDiffThr[Def_Inx_100][FRONT]), (-DEF_100));					
		}
		else
		{
			ldddss8PressEstByDiagDiff[WHL]=(int8_t)LCDDS_s16IInter5Point(ldddss16DiagDiffRunningAvg[WHL][v_inx],ldddss16DiagDiffThr[Def_Inx_100][FRONT], DEF_100, ldddss16DiagDiffThr[Def_Inx_50][FRONT], DEF_50, ldddss16DiagDiffThr[Def_Inx_25][FRONT], DEF_25,ldddss16DiagDiffThr[Def_Inx_10][FRONT], DEF_10, ldddss16DiagDiffThr[Def_Inx_0][FRONT], DEF_0);					
		} 				 				                                                               
	}                                                   
	else                                                
	{      
		if(ldddss16AxleDiffRunningAvg[WHL][v_inx]>=0)
		{
			ldddss8PressEstByAxleDiff[WHL]=(int8_t)LCDDS_s16IInter5Point(ldddss16AxleDiffRunningAvg[WHL][v_inx], (-ldddss16AxleDiffThr[Def_Inx_0][REAR]), -DEF_0, (-ldddss16AxleDiffThr[Def_Inx_10][REAR]), (-DEF_10), (-ldddss16AxleDiffThr[Def_Inx_25][REAR]), (-DEF_25), (-ldddss16AxleDiffThr[Def_Inx_50][REAR]), (-DEF_50), (-ldddss16AxleDiffThr[Def_Inx_100][REAR]), (-DEF_100));					
		}
		else
		{
			ldddss8PressEstByAxleDiff[WHL]=(int8_t)LCDDS_s16IInter5Point(ldddss16AxleDiffRunningAvg[WHL][v_inx],ldddss16AxleDiffThr[Def_Inx_100][REAR], DEF_100, ldddss16AxleDiffThr[Def_Inx_50][REAR], DEF_50, ldddss16AxleDiffThr[Def_Inx_25][REAR], DEF_25,ldddss16AxleDiffThr[Def_Inx_10][REAR], DEF_10, ldddss16AxleDiffThr[Def_Inx_0][REAR], DEF_0);					
		} 
		if(ldddss16SideDiffRunningAvg[WHL][v_inx]>=0)
		{
			ldddss8PressEstBySideDiff[WHL]=(int8_t)LCDDS_s16IInter5Point(ldddss16SideDiffRunningAvg[WHL][v_inx], (-ldddss16SideDiffThr[Def_Inx_0][REAR]), -DEF_0, (-ldddss16SideDiffThr[Def_Inx_10][REAR]), (-DEF_10), (-ldddss16SideDiffThr[Def_Inx_25][REAR]), (-DEF_25), (-ldddss16SideDiffThr[Def_Inx_50][REAR]), (-DEF_50), (-ldddss16SideDiffThr[Def_Inx_100][REAR]), (-DEF_100));					
		}
		else
		{
			ldddss8PressEstBySideDiff[WHL]=(int8_t)LCDDS_s16IInter5Point(ldddss16SideDiffRunningAvg[WHL][v_inx],ldddss16SideDiffThr[Def_Inx_100][REAR], DEF_100, ldddss16SideDiffThr[Def_Inx_50][REAR], DEF_50, ldddss16SideDiffThr[Def_Inx_25][REAR], DEF_25,ldddss16SideDiffThr[Def_Inx_10][REAR], DEF_10, ldddss16SideDiffThr[Def_Inx_0][REAR], DEF_0);					
		} 
		if(ldddss16DiagDiffRunningAvg[WHL][v_inx]>=0)
		{
			ldddss8PressEstByDiagDiff[WHL]=(int8_t)LCDDS_s16IInter5Point(ldddss16DiagDiffRunningAvg[WHL][v_inx], (-ldddss16DiagDiffThr[Def_Inx_0][REAR]), -DEF_0, (-ldddss16DiagDiffThr[Def_Inx_10][REAR]), (-DEF_10), (-ldddss16DiagDiffThr[Def_Inx_25][REAR]), (-DEF_25), (-ldddss16DiagDiffThr[Def_Inx_50][REAR]), (-DEF_50), (-ldddss16DiagDiffThr[Def_Inx_100][REAR]), (-DEF_100));					
		}
		else
		{
			ldddss8PressEstByDiagDiff[WHL]=(int8_t)LCDDS_s16IInter5Point(ldddss16DiagDiffRunningAvg[WHL][v_inx],ldddss16DiagDiffThr[Def_Inx_100][REAR], DEF_100, ldddss16DiagDiffThr[Def_Inx_50][REAR], DEF_50, ldddss16DiagDiffThr[Def_Inx_25][REAR], DEF_25,ldddss16DiagDiffThr[Def_Inx_10][REAR], DEF_10, ldddss16DiagDiffThr[Def_Inx_0][REAR], DEF_0);					
		} 									
	}	
}
/*=====================================================================================*/  
void LDDDS_vDct1WhlDeflationEachWhl(uint8_t WHL)
{
	if(((ldddss8PressEstByAxleDiff[WHL]>=S8_DEFLATION_DET_THR_HIGH)&&(ldddss8PressEstByAxleDiff[WHL]<S8_DEFLATION_DET_THR_MAX))				
	&&((ldddss8PressEstBySideDiff[WHL]>=S8_DEFLATION_DET_THR_HIGH)&&(ldddss8PressEstBySideDiff[WHL]<S8_DEFLATION_DET_THR_MAX))
	&&((ldddss8PressEstByDiagDiff[WHL]>=S8_DEFLATION_DET_THR_HIGH)&&(ldddss8PressEstByDiagDiff[WHL]<S8_DEFLATION_DET_THR_MAX)))
	{
		ldddss16Dct1WhlDefCnt[WHL][CNT_50]++;
		ldddss16Dct1WhlDefCnt[WHL][CNT_20]++;
		ldddss16Dct1WhlDefCnt[WHL][CNT_10_1]++;
		ldddss16Dct1WhlDefCnt[WHL][CNT_10_2]++;
		ldddss16Dct1WhlDefCnt[WHL][CNT_10_3]++;
		
	}
	else if(((ldddss8PressEstByAxleDiff[WHL]>S8_DEFLATION_DET_THR_LOW)&&(ldddss8PressEstByAxleDiff[WHL]<S8_DEFLATION_DET_THR_MAX))				
	&&((ldddss8PressEstBySideDiff[WHL]>S8_DEFLATION_DET_THR_LOW)&&(ldddss8PressEstBySideDiff[WHL]<S8_DEFLATION_DET_THR_MAX))
	&&((ldddss8PressEstByDiagDiff[WHL]>S8_DEFLATION_DET_THR_LOW)&&(ldddss8PressEstByDiagDiff[WHL]<S8_DEFLATION_DET_THR_MAX)))
	{
		ldddss16Dct1WhlDefCnt[WHL][CNT_20]++;
		ldddss16Dct1WhlDefCnt[WHL][CNT_10_1]++;
		ldddss16Dct1WhlDefCnt[WHL][CNT_10_2]++;
		ldddss16Dct1WhlDefCnt[WHL][CNT_10_3]++;
	}
	else if(((ldddss8PressEstByAxleDiff[WHL]>S8_DEFLATION_RESET_THR)&&(ldddss8PressEstByAxleDiff[WHL]<S8_DEFLATION_DET_THR_MAX))				
	&&((ldddss8PressEstBySideDiff[WHL]>S8_DEFLATION_DET_THR_LOW)&&(ldddss8PressEstBySideDiff[WHL]<S8_DEFLATION_DET_THR_MAX))
	&&((ldddss8PressEstByDiagDiff[WHL]>S8_DEFLATION_DET_THR_LOW)&&(ldddss8PressEstByDiagDiff[WHL]<S8_DEFLATION_DET_THR_MAX)))
	{
		ldddss16Dct1WhlDefCnt[WHL][CNT_10_1]++;
	}
	else if(((ldddss8PressEstByAxleDiff[WHL]>S8_DEFLATION_DET_THR_LOW)&&(ldddss8PressEstByAxleDiff[WHL]<S8_DEFLATION_DET_THR_MAX))				
	&&((ldddss8PressEstBySideDiff[WHL]>S8_DEFLATION_RESET_THR)&&(ldddss8PressEstBySideDiff[WHL]<S8_DEFLATION_DET_THR_MAX))
	&&((ldddss8PressEstByDiagDiff[WHL]>S8_DEFLATION_DET_THR_LOW)&&(ldddss8PressEstByDiagDiff[WHL]<S8_DEFLATION_DET_THR_MAX)))
	{
		ldddss16Dct1WhlDefCnt[WHL][CNT_10_2]++;
	}			
	else if(((ldddss8PressEstByAxleDiff[WHL]>S8_DEFLATION_DET_THR_LOW)&&(ldddss8PressEstByAxleDiff[WHL]<S8_DEFLATION_DET_THR_MAX))				
	&&((ldddss8PressEstBySideDiff[WHL]>S8_DEFLATION_DET_THR_LOW)&&(ldddss8PressEstBySideDiff[WHL]<S8_DEFLATION_DET_THR_MAX))
	&&((ldddss8PressEstByDiagDiff[WHL]>S8_DEFLATION_RESET_THR)&&(ldddss8PressEstByDiagDiff[WHL]<S8_DEFLATION_DET_THR_MAX)))
	{
		ldddss16Dct1WhlDefCnt[WHL][CNT_10_3]++;
	}
	else
	{
		LDDDS_vDecDDSDetectCnt(ldddss8PressEstByAxleDiff[WHL],WHL);
		LDDDS_vDecDDSDetectCnt(ldddss8PressEstBySideDiff[WHL],WHL);
		LDDDS_vDecDDSDetectCnt(ldddss8PressEstByDiagDiff[WHL],WHL);					
	}	
	
	ldddss16Dct1WhlDefCnt[WHL][CNT_50]=LCABS_s16LimitMinMax(ldddss16Dct1WhlDefCnt[WHL][CNT_50],U16_DEFLATION_DCT_DIST_MIN,U16_DEFLATION_DCT_DIST_MAX);
	ldddss16Dct1WhlDefCnt[WHL][CNT_20]=LCABS_s16LimitMinMax(ldddss16Dct1WhlDefCnt[WHL][CNT_20],U16_DEFLATION_DCT_DIST_MIN,U16_DEFLATION_DCT_DIST_MAX);
	ldddss16Dct1WhlDefCnt[WHL][CNT_10_1]=LCABS_s16LimitMinMax(ldddss16Dct1WhlDefCnt[WHL][CNT_10_1],U16_DEFLATION_DCT_DIST_MIN,U16_DEFLATION_DCT_DIST_MAX);
	ldddss16Dct1WhlDefCnt[WHL][CNT_10_2]=LCABS_s16LimitMinMax(ldddss16Dct1WhlDefCnt[WHL][CNT_10_2],U16_DEFLATION_DCT_DIST_MIN,U16_DEFLATION_DCT_DIST_MAX);
	ldddss16Dct1WhlDefCnt[WHL][CNT_10_3]=LCABS_s16LimitMinMax(ldddss16Dct1WhlDefCnt[WHL][CNT_10_3],U16_DEFLATION_DCT_DIST_MIN,U16_DEFLATION_DCT_DIST_MAX);				
}					        	        	                                                           
/*=====================================================================================*/ 
void LDDDS_vDct1WhlDeflationCnt(void)
{            	         
	if(ldddsu1RunningAvgUpdateFlag==1)
	{
		if(ldddsu8RunningAvgUpdateWhlIndex[v_inx]==FL_WL)
		{
			LDDDS_vDct1WhlDeflationEachWhl(FL_WL);
		}
		else if(ldddsu8RunningAvgUpdateWhlIndex[v_inx]==FR_WL)
		{
			LDDDS_vDct1WhlDeflationEachWhl(FR_WL);
		}
		else if(ldddsu8RunningAvgUpdateWhlIndex[v_inx]==RL_WL)
		{
			LDDDS_vDct1WhlDeflationEachWhl(RL_WL);
		}
		else if(ldddsu8RunningAvgUpdateWhlIndex[v_inx]==RR_WL)
		{
			LDDDS_vDct1WhlDeflationEachWhl(RR_WL);
		}
		else
		{
			;
		}					
	}
}
/*=====================================================================================*/ 
void LDDDS_vDecDDSDetectCnt(int16_t Diff_Input,uint8_t WHL)            
{
	uint8_t inx;  
	for(inx=0;inx<5;inx++)
	{
		if(Diff_Input>=S8_DEFLATION_DET_THR_MAX)
		{		
			if(ldddss16Dct1WhlDefCnt[WHL][inx]>0)
			{
				ldddss16Dct1WhlDefCnt[WHL][inx]--;
			}													
		}
		else if (Diff_Input<S8_DEFLATION_RESET_THR)	
		{
			if(ldddss16Dct1WhlDefCnt[WHL][inx]>0)
			{
				ldddss16Dct1WhlDefCnt[WHL][inx]--;
			}					
	
			if	(Diff_Input<=0)
			{
				if(ldddss16Dct1WhlDefCnt[WHL][inx]>0)
				{
					ldddss16Dct1WhlDefCnt[WHL][inx]--;
				}											
			}						
		}
		else
		{
			;
		}
	}
} 
/*=====================================================================================*/ 
#if __2WHL_Deflation_Detect
void LDDDS_vDct2WhlDeflation(void)
{                    
	if((ldddsu1RunningAvgUpdateFlag==1)&&(ldddsu8RunningAvgUpdateWhlIndex[v_inx]==4))
	{
		if((ldddss8PressEstByAxleDiff[FR_WL]>S8_DEFLATION_DET_THR_LOW)&&(ldddss8PressEstBySideDiff[FR_WL]>S8_DEFLATION_DET_THR_LOW))
		{
			ldddss16DctFLRRDefCnt=0;
			if((ldddss8PressEstByAxleDiff[RL_WL]>S8_DEFLATION_DET_THR_LOW)&&(ldddss8PressEstBySideDiff[RL_WL]>S8_DEFLATION_DET_THR_LOW))
			{
				if((abs(ldddss8PressEstByDiagDiff[FL_WL])<S8_DEFLATION_RESET_THR)&&(abs(ldddss8PressEstByDiagDiff[RR_WL])<S8_DEFLATION_RESET_THR))
				{
					ldddss16DctFRRLDefCnt++;
				}
				else if((abs(ldddss8PressEstByDiagDiff[FL_WL])>S8_DEFLATION_DET_THR_LOW)||(abs(ldddss8PressEstByDiagDiff[RR_WL])>S8_DEFLATION_DET_THR_LOW))
				{
					if(ldddss16DctFRRLDefCnt>0)
					{
						ldddss16DctFRRLDefCnt--;	
					}
					else
					{
						ldddss16DctFRRLDefCnt=0;
					}					
				}
				else
				{
					;
				}
			}
			else
			{
				if(ldddss16DctFRRLDefCnt>2)
				{
					ldddss16DctFRRLDefCnt-=2;	
				}
				else
				{
					ldddss16DctFRRLDefCnt=0;
				}				
			}
		}
		else if((ldddss8PressEstByAxleDiff[FL_WL]>S8_DEFLATION_DET_THR_LOW)&&(ldddss8PressEstBySideDiff[FL_WL]>S8_DEFLATION_DET_THR_LOW))
		{
			ldddss16DctFRRLDefCnt=0;
			if((ldddss8PressEstByAxleDiff[RR_WL]>S8_DEFLATION_DET_THR_LOW)&&(ldddss8PressEstBySideDiff[RR_WL]>S8_DEFLATION_DET_THR_LOW))
			{
				if((abs(ldddss8PressEstByDiagDiff[FR_WL])<S8_DEFLATION_RESET_THR)&&(abs(ldddss8PressEstByDiagDiff[RL_WL])<S8_DEFLATION_RESET_THR))
				{
					ldddss16DctFLRRDefCnt++;
				}
				else if((abs(ldddss8PressEstByDiagDiff[FR_WL])>S8_DEFLATION_DET_THR_LOW)&&(abs(ldddss8PressEstByDiagDiff[RL_WL])>S8_DEFLATION_DET_THR_LOW))
				{
					if(ldddss16DctFLRRDefCnt>0)
					{
						ldddss16DctFLRRDefCnt--;	
					}
					else
					{
						ldddss16DctFLRRDefCnt=0;
					}					
				}
				else
				{
					;
				}
			}
			else
			{
				if(ldddss16DctFLRRDefCnt>2)
				{
					ldddss16DctFLRRDefCnt-=2;	
				}
				else
				{
					ldddss16DctFLRRDefCnt=0;
				}				
			}		
		}
		else
		{
			if(ldddss16DctFRRLDefCnt>3)
			{
				ldddss16DctFRRLDefCnt-=3;	
			}
			else
			{
				ldddss16DctFRRLDefCnt=0;
			}
			if(ldddss16DctFLRRDefCnt>3)
			{
				ldddss16DctFLRRDefCnt-=3;	
			}
			else
			{
				ldddss16DctFLRRDefCnt=0;
			}		
		}
	}
}
#endif
/*==============================================================================================*/
#if __3WHL_Deflation_Detect
void LDDDS_vDct3WhlDeflation(void)
{
	if(ldddsu1RunningAvgUpdateFlag==1)
	{
		if(ldddsu8RunningAvgUpdateWhlIndex[v_inx]==FL_WL)
		{
			LDDDS_vDct3WhlDeflationEachWhl(FL_WL);
		}
		else if(ldddsu8RunningAvgUpdateWhlIndex[v_inx]==FR_WL)
		{
			LDDDS_vDct3WhlDeflationEachWhl(FR_WL);
		}
		else if(ldddsu8RunningAvgUpdateWhlIndex[v_inx]==RL_WL)
		{
			LDDDS_vDct3WhlDeflationEachWhl(RL_WL);
		}
		else if(ldddsu8RunningAvgUpdateWhlIndex[v_inx]==RR_WL)
		{
			LDDDS_vDct3WhlDeflationEachWhl(RR_WL);
		}
		else
		{
			;
		}					
	}	
}
/*==============================================================================================*/
void LDDDS_vDct3WhlDeflationEachWhl(uint8_t WHL)
{
	
	if(((ldddss8PressEstByAxleDiff[WHL]<=S8_DEFLATION_DET_THR_HIGH_3WHL)&&(ldddss8PressEstByAxleDiff[WHL]>S8_DEFLATION_DET_THR_MIN_3WHL))				
	&&((ldddss8PressEstBySideDiff[WHL]<=S8_DEFLATION_DET_THR_HIGH_3WHL)&&(ldddss8PressEstBySideDiff[WHL]>S8_DEFLATION_DET_THR_MIN_3WHL))
	&&((ldddss8PressEstByDiagDiff[WHL]<=S8_DEFLATION_DET_THR_HIGH_3WHL)&&(ldddss8PressEstByDiagDiff[WHL]>S8_DEFLATION_DET_THR_MIN_3WHL)))
	{
		ldddss16Dct3WhlDefCnt[WHL][CNT_50]++;
		ldddss16Dct3WhlDefCnt[WHL][CNT_20]++;		
	}
	else if((ldddss8PressEstByAxleDiff[WHL]<=S8_DEFLATION_DET_THR_LOW_3WHL)				
	&&(ldddss8PressEstBySideDiff[WHL]<=S8_DEFLATION_DET_THR_LOW_3WHL)
	&&(ldddss8PressEstByDiagDiff[WHL]<=S8_DEFLATION_DET_THR_LOW_3WHL))
	{
		ldddss16Dct3WhlDefCnt[WHL][CNT_20]++;	
	}
	else if((ldddss8PressEstByAxleDiff[WHL]>S8_DEFLATION_RESET_THR_3WHL)				
	||(ldddss8PressEstBySideDiff[WHL]>S8_DEFLATION_RESET_THR_3WHL)
	||(ldddss8PressEstByDiagDiff[WHL]>S8_DEFLATION_RESET_THR_3WHL))
	{
		if(ldddss16Dct3WhlDefCnt[WHL][CNT_50]>0)
		{
			ldddss16Dct3WhlDefCnt[WHL][CNT_50]--;
		}
		if(ldddss16Dct3WhlDefCnt[WHL][CNT_20]>0)
		{
			ldddss16Dct3WhlDefCnt[WHL][CNT_20]--;
		}		
	}
	else
	{
		;		
	}		
}
/*==============================================================================================*/
void LDDDS_vDet3WhlDeflationState(uint8_t WHL,uint8_t WHL_2 ,uint8_t WHL_3,uint8_t WHL_4)
{
	if( ldddss16Dct3WhlDefCnt[WHL][CNT_50]>U16_DEFLATION_DCT_DIST_SHORT)
	{
		tempF0=1;
	}
	else if( ldddss16Dct3WhlDefCnt[WHL][CNT_20]>U16_DEFLATION_DCT_DIST_NORMAL)
	{
		tempF0=1;
	}
	else
	{
		tempF0=0;
	}	
	
	tempF1=1;
	if((ldddss16Dct3WhlDefCnt[WHL_2][CNT_50]>U16_DEFLATION_RESET_DIST)||
		(ldddss16Dct3WhlDefCnt[WHL_2][CNT_20]>U16_DEFLATION_RESET_DIST))
	{
		tempF1=0;
	}				
	else if((ldddss16Dct3WhlDefCnt[WHL_3][CNT_50]>U16_DEFLATION_RESET_DIST)||
		(ldddss16Dct3WhlDefCnt[WHL_3][CNT_20]>U16_DEFLATION_RESET_DIST))
	{
		tempF1=0;
	}				
	else if((ldddss16Dct3WhlDefCnt[WHL_4][CNT_50]>U16_DEFLATION_RESET_DIST)||
		(ldddss16Dct3WhlDefCnt[WHL_4][CNT_20]>U16_DEFLATION_RESET_DIST))
	{
		tempF1=0;
	}				
	else
	{
		;
	}
	
	if(tempF0==1)
	{
		if(tempF1==1)
		{
			ldddsu8Other3whlDeflation[WHL]=1;
		}		
	}
	else
	{
		ldddsu8Other3whlDeflation[WHL]=0;
	}
	
}
#endif	
/*=====================================================================*/
#if __Hybrid_DDS
void LDDDSh_vDetLowPressWithTpmsSnr(void)
{                     
    tempW1=(int16_t)((int32_t)(ldddshs16InitTpmsPressure-ldddshs16CurrentTpmsPressure)*100/ldddshs16InitTpmsPressure);
    
    ldddshs8DefbyTpmsSensor=(int8_t)LCABS_s16LimitMinMax(tempW1,-100,100);
    ldddshs8DefbyAxleDiffWithTpSnr=ldddss8PressEstByAxleDiff[ldddshu8WhlInxAxle]+ldddshs8DefbyTpmsSensor;
    ldddshs8DefbySideDiffWithTpSnr=ldddss8PressEstBySideDiff[ldddshu8WhlInxSide]+ldddshs8DefbyTpmsSensor;
    ldddshs8DefbyDiagDiffWithTpSnr=ldddss8PressEstByDiagDiff[ldddshu8WhlInxDiag]+ldddshs8DefbyTpmsSensor;
        
//    if(ldddshu1RefTirePressureV==1){
        if((ldddsu1RunningAvgUpdateFlag==1)&&(ldddsu8RunningAvgUpdateWhlIndex[v_inx]==ldddsu8TpmsSnrLocation)){
            if(ldddshs8DefbyTpmsSensor>S8_DEFLATION_DET_THR_LOW){
                ldddshu8DefDctCntByTpmsSnr++;
                if(ldddshu8DefDctCntByTpmsSnr>U16_DEFLATION_DCT_DIST_SHORT){
                    ldddshu1DeflationByHybridDds=1;
                    ldddshu1DefByHybridDdsFR=1;
                }
            }
            else{
                if(ldddshs8DefbyAxleDiffWithTpSnr>S8_DEFLATION_DET_THR_LOW){
                    ldddshu8DefDctCntByAxleDiff++;
                    if(ldddshu8DefDctCntByAxleDiff>U16_DEFLATION_DCT_DIST_NORMAL){
                        ldddshu1DeflationByHybridDds=1;
                        ldddshu1DefByHybridDdsFL=1;
                    }
                }
                if(ldddshs8DefbySideDiffWithTpSnr>S8_DEFLATION_DET_THR_LOW){
                    ldddshu8DefDctCntBySideDiff++;
                    if(ldddshu8DefDctCntBySideDiff>U16_DEFLATION_DCT_DIST_NORMAL){
                        ldddshu1DeflationByHybridDds=1;
                        ldddshu1DefByHybridDdsRR=1;
                    }                    
                }
                if(ldddshs8DefbyDiagDiffWithTpSnr>S8_DEFLATION_DET_THR_LOW){
                    ldddshu8DefDctCntByDiagDiff++;
                    if(ldddshu8DefDctCntByDiagDiff>U16_DEFLATION_DCT_DIST_NORMAL){
                        ldddshu1DeflationByHybridDds=1;
                        ldddshu1DefByHybridDdsRL=1;
                    } 
                }
                
                if(ldddshu1DeflationByHybridDds==1){
                        ldddshu8DefDctCntByTpmsSnr=0;
                        ldddshu8DefDctCntByAxleDiff=0;
                        ldddshu8DefDctCntBySideDiff=0;
                        ldddshu8DefDctCntByDiagDiff=0;
                }
            }
        }
        else{
            ;
        }
//    }
//    else{
//        ;
//    }
}
#endif
/*==============================================================================================*/
void LDDDS_vDetDeflationState(void)
{
	if(ldddsu1RunningAvgUpdateFlag==1)
	{
		if(ldddsu8RunningAvgUpdateWhlIndex[v_inx]==4)
		{
			LDDDS_vDet1WhlDeflationState(FL_WL,FR_WL,RL_WL,RR_WL);
			LDDDS_vDet1WhlDeflationState(FR_WL,FL_WL,RL_WL,RR_WL);
			LDDDS_vDet1WhlDeflationState(RL_WL,FR_WL,FL_WL,RR_WL);
			LDDDS_vDet1WhlDeflationState(RR_WL,FR_WL,RL_WL,FL_WL);
		
			#if __2WHL_Deflation_Detect 
			if((ldddss16DctFLRRDefCnt>U16_DEFLATION_DCT_DIST_NORMAL)&&(ldddss16DctFRRLDefCnt<U16_DEFLATION_RESET_DIST))
			{
				ldddsu1Diag2whlDeflationFLRR=1;
			}
			else if((ldddss16DctFRRLDefCnt>U16_DEFLATION_DCT_DIST_NORMAL)&&(ldddss16DctFLRRDefCnt<U16_DEFLATION_RESET_DIST))
			{
				ldddsu1Diag2whlDeflationFRRL=1;
			}
			else
			{
				;
			}
			#endif
			
			#if __3WHL_Deflation_Detect
			LDDDS_vDet3WhlDeflationState(FL_WL,FR_WL,RL_WL,RR_WL);
			LDDDS_vDet3WhlDeflationState(FR_WL,FL_WL,RL_WL,RR_WL);
			LDDDS_vDet3WhlDeflationState(RL_WL,FR_WL,FL_WL,RR_WL);
			LDDDS_vDet3WhlDeflationState(RR_WL,FR_WL,RL_WL,FL_WL);			
			#endif
/*			
			ldddsu8DeflationStateFlags=0;
			if(ldddsu8SingleWhlDeflation[FL_WL]==1)
			{
				ldddsu8DeflationStateFlags=1;
				ldddsu1DeflationState=1;
			}
			else if(ldddsu8SingleWhlDeflation[FR_WL]==1)
			{
				ldddsu8DeflationStateFlags=2;
				ldddsu1DeflationState=1;
			}				
			else if(ldddsu8SingleWhlDeflation[RL_WL]==1)
			{
				ldddsu8DeflationStateFlags=3;
				ldddsu1DeflationState=1;
			}
			else if(ldddsu8SingleWhlDeflation[RR_WL]==1)
			{
				ldddsu1DeflationState=1;	
				ldddsu8DeflationStateFlags=4;
			}
			#if __2WHL_Deflation_Detect
			else if(ldddsu1Diag2whlDeflationFLRR==1)
			{
				ldddsu1DeflationState=1;
				ldddsu8DeflationStateFlags=5;	
			} 
			else if(ldddsu1Diag2whlDeflationFRRL==1)
			{
				ldddsu1DeflationState=1;	
				ldddsu8DeflationStateFlags=6;
			} 			
			#endif
			#if __3WHL_Deflation_Detect
			else if(ldddsu8Other3whlDeflation[FL_WL]==1)
			{
				ldddsu1DeflationState=1;	
				ldddsu8DeflationStateFlags=7;
			}	
			else if(ldddsu8Other3whlDeflation[FR_WL]==1)
			{
				ldddsu1DeflationState=1;	
				ldddsu8DeflationStateFlags=8;
			}	
			else if(ldddsu8Other3whlDeflation[RL_WL]==1)
			{
				ldddsu1DeflationState=1;	
				ldddsu8DeflationStateFlags=9;
			}	
			else if(ldddsu8Other3whlDeflation[RR_WL]==1)
			{
				ldddsu1DeflationState=1;	
				ldddsu8DeflationStateFlags=10;
			}												
			#endif			
			else
			{
				;
			}
*/			
		}
		ldddsu8RunningAvgUpdateWhlIndex[v_inx]++;
		if(ldddsu8RunningAvgUpdateWhlIndex[v_inx]==5)	
		{
			ldddsu8RunningAvgUpdateWhlIndex[v_inx]=0;
			ldddsu1RunningAvgUpdateFlag=0;
		}			
	}		
}
/*=================================================================================================*/
void LDDDS_vDet1WhlDeflationState(uint8_t WHL,uint8_t WHL_2 ,uint8_t WHL_3,uint8_t WHL_4)
{
	if( ldddss16Dct1WhlDefCnt[WHL][CNT_50]>U16_DEFLATION_DCT_DIST_SHORT)
	{
		tempF0=1;
	}
	else if( ldddss16Dct1WhlDefCnt[WHL][CNT_20]>U16_DEFLATION_DCT_DIST_NORMAL)
	{
		tempF0=1;
	}
	else if( ldddss16Dct1WhlDefCnt[WHL][CNT_10_1]>U16_DEFLATION_DCT_DIST_LONG)
	{
		tempF0=1;
	}
	else if( ldddss16Dct1WhlDefCnt[WHL][CNT_10_2]>U16_DEFLATION_DCT_DIST_LONG)
	{
		tempF0=1;
	}
	else if( ldddss16Dct1WhlDefCnt[WHL][CNT_10_3]>U16_DEFLATION_DCT_DIST_LONG)
	{
		tempF0=1;
	}
	else
	{
		tempF0=0;
	}	
	
	tempF1=1;
	if((ldddss16Dct1WhlDefCnt[WHL_2][CNT_50]>U16_DEFLATION_RESET_DIST)||
		(ldddss16Dct1WhlDefCnt[WHL_2][CNT_20]>U16_DEFLATION_RESET_DIST)||
		(ldddss16Dct1WhlDefCnt[WHL_2][CNT_10_1]>U16_DEFLATION_RESET_DIST)||
		(ldddss16Dct1WhlDefCnt[WHL_2][CNT_10_2]>U16_DEFLATION_RESET_DIST)||
		(ldddss16Dct1WhlDefCnt[WHL_2][CNT_10_3]>U16_DEFLATION_RESET_DIST))
	{
		tempF1=0;
	}				
	else if((ldddss16Dct1WhlDefCnt[WHL_3][CNT_50]>U16_DEFLATION_RESET_DIST)||
		(ldddss16Dct1WhlDefCnt[WHL_3][CNT_20]>U16_DEFLATION_RESET_DIST)||
		(ldddss16Dct1WhlDefCnt[WHL_3][CNT_10_1]>U16_DEFLATION_RESET_DIST)||
		(ldddss16Dct1WhlDefCnt[WHL_3][CNT_10_2]>U16_DEFLATION_RESET_DIST)||
		(ldddss16Dct1WhlDefCnt[WHL_3][CNT_10_3]>U16_DEFLATION_RESET_DIST))
	{
		tempF1=0;
	}				
	else if((ldddss16Dct1WhlDefCnt[WHL_4][CNT_50]>U16_DEFLATION_RESET_DIST)||
		(ldddss16Dct1WhlDefCnt[WHL_4][CNT_20]>U16_DEFLATION_RESET_DIST)||
		(ldddss16Dct1WhlDefCnt[WHL_4][CNT_10_1]>U16_DEFLATION_RESET_DIST)||
		(ldddss16Dct1WhlDefCnt[WHL_4][CNT_10_2]>U16_DEFLATION_RESET_DIST)||
		(ldddss16Dct1WhlDefCnt[WHL_4][CNT_10_3]>U16_DEFLATION_RESET_DIST))
	{
		tempF1=0;
	}				
	else
	{
		;
	}
	
	if(tempF0==1)
	{
		if(tempF1==1)
		{
			ldddsu8SingleWhlDeflation[WHL]=1;
		}		
	}
	else
	{
		ldddsu8SingleWhlDeflation[WHL]=0;
	}
	
}
/*==========================================================================================================================*/
void LDDDS_vDetTireLowPressureState(void)
{
    ldddsu8DeflationStateFlags=0;
    if(ldddsu1DeflationState==0){
        ldddsu1LowTirePressStateFlgFL=0;
        ldddsu1LowTirePressStateFlgFR=0;
        ldddsu1LowTirePressStateFlgRL=0;
        ldddsu1LowTirePressStateFlgRR=0;
        
        
    	if(ldddsu8SingleWhlDeflation[FL_WL]==1)   
    	{
    		ldddsu8DeflationStateFlags=1;
    		ldddsu1DeflationState=1;
    		ldddsu1LowTirePressStateFlgFL=1;
    	}
    	else if(ldddsu8SingleWhlDeflation[FR_WL]==1)
    	{
    		ldddsu8DeflationStateFlags=2;
    		ldddsu1DeflationState=1;
    		ldddsu1LowTirePressStateFlgFR=1;
    	}				
    	else if(ldddsu8SingleWhlDeflation[RL_WL]==1)
    	{
    		ldddsu8DeflationStateFlags=3;
    		ldddsu1DeflationState=1;
    		ldddsu1LowTirePressStateFlgRL=1;
    	}
    	else if(ldddsu8SingleWhlDeflation[RR_WL]==1)
    	{
    		ldddsu1DeflationState=1;	
    		ldddsu8DeflationStateFlags=4;
    		ldddsu1LowTirePressStateFlgRR=1;
    	}
    	else{
    	    ;
    	}
    	
    	#if __2WHL_Deflation_Detect
    	if(ldddsu1Diag2whlDeflationFLRR==1)
    	{
    		ldddsu1DeflationState=1;
    		ldddsu8DeflationStateFlags=5;	
    		ldddsu1LowTirePressStateFlgFL=1;
    		ldddsu1LowTirePressStateFlgRR=1;
    	} 
    	else if(ldddsu1Diag2whlDeflationFRRL==1)
    	{
    		ldddsu1DeflationState=1;	
    		ldddsu8DeflationStateFlags=6;
    		ldddsu1LowTirePressStateFlgFR=1;
    		ldddsu1LowTirePressStateFlgRL=1;
    	} 
    	else{
    	    ;
    	}			
    	#endif
    	#if __3WHL_Deflation_Detect
    	if(ldddsu8Other3whlDeflation[FL_WL]==1)
    	{
    		ldddsu1DeflationState=1;	
    		ldddsu8DeflationStateFlags=7;
    		ldddsu1LowTirePressStateFlgFR=1;
    		ldddsu1LowTirePressStateFlgRL=1;
    		ldddsu1LowTirePressStateFlgRR=1;
    		
    	}	
    	if(ldddsu8Other3whlDeflation[FR_WL]==1)
    	{
    		ldddsu1DeflationState=1;	
    		ldddsu8DeflationStateFlags=8;
    		ldddsu1LowTirePressStateFlgFL=1;
    		ldddsu1LowTirePressStateFlgRL=1;
    		ldddsu1LowTirePressStateFlgRR=1;
    	}	
    	if(ldddsu8Other3whlDeflation[RL_WL]==1)
    	{
    		ldddsu1DeflationState=1;	
    		ldddsu8DeflationStateFlags=9;
    		ldddsu1LowTirePressStateFlgFL=1;
    		ldddsu1LowTirePressStateFlgFR=1;
    		ldddsu1LowTirePressStateFlgRR=1;
    	}	
    	if(ldddsu8Other3whlDeflation[RR_WL]==1)
    	{
    		ldddsu1DeflationState=1;	
    		ldddsu8DeflationStateFlags=10;
    		ldddsu1LowTirePressStateFlgFL=1;
    		ldddsu1LowTirePressStateFlgFR=1;
    		ldddsu1LowTirePressStateFlgRL=1;
    	}												
    	#endif
    	
    	#if (__DDS_PLUS == ENABLE)
        if(ldddspu1DeflationStateFlgFL==1){
            ldddsu1DeflationState=1;
            ldddsu8DeflationStateFlags=11;
            ldddsu1LowTirePressStateFlgFL=1;
        }
        if(ldddspu1DeflationStateFlgFR==1){
            ldddsu1DeflationState=1;
            ldddsu8DeflationStateFlags=12;
            ldddsu1LowTirePressStateFlgFR=1;            
        }
        if(ldddspu1DeflationStateFlgRL==1){
            ldddsu1DeflationState=1;
            ldddsu8DeflationStateFlags=13;
            ldddsu1LowTirePressStateFlgRL=1;            
        }
        if(ldddspu1DeflationStateFlgRR==1){
            ldddsu1DeflationState=1;
            ldddsu8DeflationStateFlags=14;
            ldddsu1LowTirePressStateFlgRR=1;            
        }
                
    	#endif	
    	
    	#if __Hybrid_DDS	
    	if(ldddshu1DeflationByHybridDds==1){
    	    ldddsu1DeflationState=1;
            ldddsu8DeflationStateFlags=15;

            if(ldddshu1DefByHybridDdsFL==1){
            	ldddsu1LowTirePressStateFlgFL=1;
            }
            if(ldddshu1DefByHybridDdsFR==1){
            	ldddsu1LowTirePressStateFlgFR=1;
            }
            if(ldddshu1DefByHybridDdsRL==1){
            	ldddsu1LowTirePressStateFlgRL=1;
            }
            if(ldddshu1DefByHybridDdsRR==1){
            	ldddsu1LowTirePressStateFlgRR=1;
            }         
        }
    	#endif	

    }
    else if(ldddsu1DDSRunningReset==ENABLE){
        ;;
        /*
        if((ldddsu8SingleWhlDeflation[FL_WL]==0)&&(ldddsu8SingleWhlDeflation[FR_WL]==0)
            &&(ldddsu8SingleWhlDeflation[RL_WL]==0)&& (ldddsu8SingleWhlDeflation[RR_WL]==0) 
              #if __2WHL_Deflation_Detect
              &&(ldddsu1Diag2whlDeflationFLRR==0)&&(ldddsu1Diag2whlDeflationFRRL==0)
              #endif
              
              #if __3WHL_Deflation_Detect
    	      &&(ldddsu8Other3whlDeflation[FL_WL]==0)&&(ldddsu8Other3whlDeflation[FR_WL]==0)
    	      &&(ldddsu8Other3whlDeflation[RL_WL]==0)&&(ldddsu8Other3whlDeflation[RR_WL]==0)
    	      #endif
    	      
    	      #if (__DDS_PLUS == ENABLE)
    	      &&(ldddspu1DeflationStateFlgFL==0)&&(ldddspu1DeflationStateFlgRL==0)
    	      &&(ldddspu1DeflationStateFlgFR==0)&&(ldddspu1DeflationStateFlgRR==0)
    	      #endif
    	    ){
    	        ldddsu1DeflationState=0;
    	        ldddsu1LowTirePressStateFlgFL=0;
    	        ldddsu1LowTirePressStateFlgFR=0;
    	        ldddsu1LowTirePressStateFlgRL=0;
    	        ldddsu1LowTirePressStateFlgRR=0;
    	        
    	    }  
    	    */
    	    
    }
    else{
        ;
    }
}

/*==========================================================================================================================*/
void LDDDS_vDetDDSRunningReset(void)
{
	if(ldddsu1RunningAvgUpdateFlag==1)
	{
		if(ldddsu8RunningAvgUpdateWhlIndex[v_inx]==FL_WL)
		{
			LDDDS_vDctRunningResetEachWhl(FL_WL);
		}
		else if(ldddsu8RunningAvgUpdateWhlIndex[v_inx]==FR_WL)
		{
			LDDDS_vDctRunningResetEachWhl(FR_WL);
		}
		else if(ldddsu8RunningAvgUpdateWhlIndex[v_inx]==RL_WL)
		{
			LDDDS_vDctRunningResetEachWhl(RL_WL);
		}
		else if(ldddsu8RunningAvgUpdateWhlIndex[v_inx]==RR_WL)
		{
			LDDDS_vDctRunningResetEachWhl(RR_WL);
		}
		else
		{
			if((ldddss16DdsRunningResetCnt[FL_WL]>U16_DEFLATION_RESET_DIST)
			&&(ldddss16DdsRunningResetCnt[FR_WL]>U16_DEFLATION_RESET_DIST)
			&&(ldddss16DdsRunningResetCnt[RL_WL]>U16_DEFLATION_RESET_DIST)
			&&(ldddss16DdsRunningResetCnt[RR_WL]>U16_DEFLATION_RESET_DIST))
			{
				ldddsu1DeflationState=0;
				
				LDDDS_vInitializeDctVariable(FL_WL);
				LDDDS_vInitializeDctVariable(FR_WL);
				LDDDS_vInitializeDctVariable(RL_WL);
				LDDDS_vInitializeDctVariable(RR_WL);
				
				#if __2WHL_Deflation_Detect				
				ldddsu1Diag2whlDeflationFLRR=0;
				ldddsu1Diag2whlDeflationFRRL=0;
				ldddss16DctFLRRDefCnt=0;
				ldddss16DctFRRLDefCnt=0;
				#endif
										
			}
		}	
		
		ldddsu8RunningAvgUpdateWhlIndex[v_inx]++;
		
		if(ldddsu8RunningAvgUpdateWhlIndex[v_inx]==5)	
		{
			ldddsu8RunningAvgUpdateWhlIndex[v_inx]=0;
			ldddsu1RunningAvgUpdateFlag=0;
		}			
	}	
}
/*============================================================================================*/
void LDDDS_vInitializeDctVariable(uint8_t WHL)
{
	ldddsu8SingleWhlDeflation[WHL]=0;
	ldddss16Dct1WhlDefCnt[WHL][CNT_50]=0;
	ldddss16Dct1WhlDefCnt[WHL][CNT_20]=0;
	ldddss16Dct1WhlDefCnt[WHL][CNT_10_1]=0;
	ldddss16Dct1WhlDefCnt[WHL][CNT_10_2]=0;
	ldddss16Dct1WhlDefCnt[WHL][CNT_10_3]=0;
	
	ldddss16DdsRunningResetCnt[WHL]=0;
	#if __3WHL_Deflation_Detect
	ldddsu8Other3whlDeflation[WHL]=0;	
	ldddss16Dct3WhlDefCnt[WHL][CNT_50]=0;
	ldddss16Dct3WhlDefCnt[WHL][CNT_20]=0;
	#endif
	
}
/*============================================================================================*/
void LDDDS_vDctRunningResetEachWhl(uint8_t WHL)
{
	if((abs(ldddss8PressEstByAxleDiff[WHL])<S8_DEFLATION_RESET_THR)&&(abs(ldddss8PressEstBySideDiff[WHL])<S8_DEFLATION_RESET_THR)&&(abs(ldddss8PressEstByDiagDiff[WHL])<S8_DEFLATION_RESET_THR))
	{
		ldddss16DdsRunningResetCnt[WHL]++;
	}
	else 
	{
		if(abs(ldddss8PressEstByAxleDiff[WHL])>S8_DEFLATION_DET_THR_LOW)
		{                              
			if(ldddss16DdsRunningResetCnt[WHL]>0)
			{
				ldddss16DdsRunningResetCnt[WHL]--;
			}
		}
		if(abs(ldddss8PressEstBySideDiff[WHL])>S8_DEFLATION_DET_THR_LOW)
		{
			if(ldddss16DdsRunningResetCnt[WHL]>0)
			{
				ldddss16DdsRunningResetCnt[WHL]--;
			}
		}	                            
		if(abs(ldddss8PressEstByDiagDiff[WHL])>S8_DEFLATION_DET_THR_LOW)
		{
			if(ldddss16DdsRunningResetCnt[WHL]>0)
			{
				ldddss16DdsRunningResetCnt[WHL]--;
			}
		}			
	}				
}
/*============================================================================================================================*/

void LDDDS_vReadTPMSData(void)
{
	int16_t whl_index;
	
	if (ldddss8TPMSWheelIndex>=1)
	{
		whl_index=ldddss8TPMSWheelIndex-1;
		
		ldddss8TireTempFromTPMS[whl_index]=ldddss8TPMSTireTemperature;
		ldddss8TirePressueFromTPMS[whl_index]=ldddss8TPMSTirePressure;
	}
	else
	{
		for(whl_index=0;whl_index<4;whl_index++)
		{
			ldddss8TireTempFromTPMS[whl_index]=0;
			ldddss8TirePressueFromTPMS[whl_index]=0;
		}
	}
}

/*============================================================================================================================*/
void LDDDS_vUpdateTimeWheelRev(void)
{
//	ldddsu8ElapsedTimeUpdateFlgFL_K=0;
//	ldddsu8ElapsedTimeUpdateFlgFR_K=0;
//	ldddsu8ElapsedTimeUpdateFlgRL_K=0;
//	ldddsu8ElapsedTimeUpdateFlgRR_K=0;
//	ldddsu1ElapsedTimeUpdateFlgOldFL=ldddsu1ElapsedTimeUpdateFlgFL;
//	ldddsu1ElapsedTimeUpdateFlgOldFR=ldddsu1ElapsedTimeUpdateFlgFR;
//	ldddsu1ElapsedTimeUpdateFlgOldRL=ldddsu1ElapsedTimeUpdateFlgRL;
//	ldddsu1ElapsedTimeUpdateFlgOldRR=ldddsu1ElapsedTimeUpdateFlgRR;
//	
//	
//	if(ldddsu16ElapsedTimePerRev[0]==1)
//	{
//		ldddsu1ElapsedTimeUpdateFlgFL=1;
//	}
//	if(ldddsu16ElapsedTimePerRev[1]==1)
//	{
//		ldddsu1ElapsedTimeUpdateFlgFR=1;
//	}
//	if(ldddsu16ElapsedTimePerRev[2]==1)
//	{
//		ldddsu1ElapsedTimeUpdateFlgRL=1;
//	}
//	if(ldddsu16ElapsedTimePerRev[3]==1)
//	{
//		ldddsu1ElapsedTimeUpdateFlgRR=1;
//	}
//				
//	if((ldddsu1ElapsedTimeUpdateFlgFL==1)&&(ldddsu1ElapsedTimeUpdateFlgOldFL==0))
//	{
//		ldddsu8ElapsedTimeUpdateFlgFL_K=1;
//	}
//	if((ldddsu1ElapsedTimeUpdateFlgFR==1)&&(ldddsu1ElapsedTimeUpdateFlgOldFR==0))
//	{
//		ldddsu8ElapsedTimeUpdateFlgFR_K=1;
//	}	
//	if((ldddsu1ElapsedTimeUpdateFlgRL==1)&&(ldddsu1ElapsedTimeUpdateFlgOldRL==0))
//	{
//		ldddsu8ElapsedTimeUpdateFlgRL_K=1;
//	}
//	if((ldddsu1ElapsedTimeUpdateFlgRR==1)&&(ldddsu1ElapsedTimeUpdateFlgOldRR==0))
//	{
//		ldddsu8ElapsedTimeUpdateFlgRR_K=1;
//	}	
//
//	if((ldddsu1ElapsedTimeUpdateFlgFL==1)&&(ldddsu1ElapsedTimeUpdateFlgFR==1)
//		&&(ldddsu1ElapsedTimeUpdateFlgRL==1)&&(ldddsu1ElapsedTimeUpdateFlgRR==1))
////	if(ldddsu16ElapsedTimePerRev[0]==1)
//	{
//		ldddsu1ElapsedTimeUpdateFlgVeh=1;
////		ldddsu1ElapsedTimeUpdateFlgFL=0;
////		ldddsu1ElapsedTimeUpdateFlgFR=0;
////		ldddsu1ElapsedTimeUpdateFlgRL=0;
////		ldddsu1ElapsedTimeUpdateFlgRR=0;
//	}
//	else
//	{
//		ldddsu1ElapsedTimeUpdateFlgVeh=0;
//	}
//	
			
}
/*============================================================================================================================*/

void LDDDS_vClearDDSCalibParameters(void)
{
	ldddsu1TireWearInCompleted=0;
	ldddss16TireWereInMileageLeft=0;
	ldddss16TireWereInMileageRight=0;	
	ldddss16RollingCntLeft=0;
	ldddss16RollingCntRight=0;
    ldddsu1CompleteCalib=0;
    ldddsu1CompleteCalibAllSpeed=0;
	ldddsu8CompleteCalib[0]=0;
	ldddsu8CompleteCalib[1]=0;
	ldddsu8CompleteCalib[2]=0;
	ldddsu8CompleteCalib[3]=0;
	ldddsu8CompleteCalib[4]=0;
	
	
	ldddss16CumulativeMean[0][0]=0;
	ldddss16CumulativeMean[0][1]=0;
	ldddss16CumulativeMean[0][2]=0;
	ldddss16CumulativeMean[0][3]=0;
	ldddss16CumulativeMean[0][4]=0;
	
	ldddss16CumulativeMean[1][0]=0;
	ldddss16CumulativeMean[1][1]=0;
	ldddss16CumulativeMean[1][2]=0;
	ldddss16CumulativeMean[1][3]=0;
	ldddss16CumulativeMean[1][4]=0;
	
	ldddss16CumulativeMean[2][0]=0;
	ldddss16CumulativeMean[2][1]=0;
	ldddss16CumulativeMean[2][2]=0;
	ldddss16CumulativeMean[2][3]=0;
	ldddss16CumulativeMean[2][4]=0;
	
	ldddss16CumulativeMean[3][0]=0;
	ldddss16CumulativeMean[3][1]=0;
	ldddss16CumulativeMean[3][2]=0;
	ldddss16CumulativeMean[3][3]=0;
	ldddss16CumulativeMean[3][4]=0;
	
	ldddss16DetCalibValue[0][0]=0;
	ldddss16DetCalibValue[0][1]=0;
	ldddss16DetCalibValue[0][2]=0;
	ldddss16DetCalibValue[0][3]=0;
	ldddss16DetCalibValue[0][4]=0;
	
	ldddss16DetCalibValue[1][0]=0;
	ldddss16DetCalibValue[1][1]=0;
	ldddss16DetCalibValue[1][2]=0;
	ldddss16DetCalibValue[1][3]=0;
	ldddss16DetCalibValue[1][4]=0;
	
	ldddss16DetCalibValue[2][0]=0;
	ldddss16DetCalibValue[2][1]=0;
	ldddss16DetCalibValue[2][2]=0;
	ldddss16DetCalibValue[2][3]=0;
	ldddss16DetCalibValue[2][4]=0;
	
	ldddss16DetCalibValue[3][0]=0;
	ldddss16DetCalibValue[3][1]=0;
	ldddss16DetCalibValue[3][2]=0;
	ldddss16DetCalibValue[3][3]=0;
	ldddss16DetCalibValue[3][4]=0;
	
	ldddsu16CntCalibTotalRev[0]=0;
	ldddsu16CntCalibTotalRev[1]=0;
	ldddsu16CntCalibTotalRev[2]=0;
	ldddsu16CntCalibTotalRev[3]=0;
	ldddsu16CntCalibTotalRev[4]=0;
	#if __Hybrid_DDS
	ldddshs16InitTpmsPressure=0;
	#endif
	
}
/*============================================================================================================================*/
void LDDDS_vClearDDSDetectParameters(void)
{
	ldddss16Dct1WhlDefCnt[0][0]=0;
	ldddss16Dct1WhlDefCnt[0][1]=0;
	ldddss16Dct1WhlDefCnt[0][2]=0;
	ldddss16Dct1WhlDefCnt[0][3]=0;
	ldddss16Dct1WhlDefCnt[0][4]=0;
	
	ldddss16Dct1WhlDefCnt[1][0]=0;
	ldddss16Dct1WhlDefCnt[1][1]=0;
	ldddss16Dct1WhlDefCnt[1][2]=0;
	ldddss16Dct1WhlDefCnt[1][3]=0;
	ldddss16Dct1WhlDefCnt[1][4]=0;
	
	ldddss16Dct1WhlDefCnt[2][0]=0;
	ldddss16Dct1WhlDefCnt[2][1]=0;
	ldddss16Dct1WhlDefCnt[2][2]=0;
	ldddss16Dct1WhlDefCnt[2][3]=0;
	ldddss16Dct1WhlDefCnt[2][4]=0;
	
	ldddss16Dct1WhlDefCnt[3][0]=0;
	ldddss16Dct1WhlDefCnt[3][1]=0;
	ldddss16Dct1WhlDefCnt[3][2]=0;
	ldddss16Dct1WhlDefCnt[3][3]=0;
	ldddss16Dct1WhlDefCnt[3][4]=0;
	
	ldddsu8SingleWhlDeflation[0]=0;
	ldddsu8SingleWhlDeflation[1]=0;
	ldddsu8SingleWhlDeflation[2]=0;
	ldddsu8SingleWhlDeflation[3]=0;
	                          
	ldddsu1DeflationState=0;
	ldddsu8DeflationStateFlags=0;
	
#if __2WHL_Deflation_Detect
	ldddss16DctFLRRDefCnt=0;
	ldddss16DctFRRLDefCnt=0;
	ldddsu1Diag2whlDeflationFLRR=0;
	ldddsu1Diag2whlDeflationFRRL=0;
#endif

#if __3WHL_Deflation_Detect
	ldddsu8Other3whlDeflation[0]=0;
	ldddsu8Other3whlDeflation[1]=0;
	ldddsu8Other3whlDeflation[2]=0;
	ldddsu8Other3whlDeflation[3]=0;

	ldddss16Dct3WhlDefCnt[0][0]=0;
	ldddss16Dct3WhlDefCnt[1][0]=0;
	ldddss16Dct3WhlDefCnt[2][0]=0;
	ldddss16Dct3WhlDefCnt[3][0]=0;
	
	ldddss16Dct3WhlDefCnt[0][1]=0;
	ldddss16Dct3WhlDefCnt[1][1]=0;
	ldddss16Dct3WhlDefCnt[2][1]=0;
	ldddss16Dct3WhlDefCnt[3][1]=0;
#endif

	#if __Hybrid_DDS
    ldddshs16CurrentTpmsPressure=0;
    ldddshs8DefbyTpmsSensor=0;
    ldddshs8DefbyAxleDiffWithTpSnr=0;
    ldddshs8DefbySideDiffWithTpSnr=0;
    ldddshs8DefbyDiagDiffWithTpSnr=0;
    ldddshu8DefDctCntByTpmsSnr=0;
    ldddshu8DefDctCntByAxleDiff=0;
    ldddshu8DefDctCntBySideDiff=0;
    ldddshu8DefDctCntByDiagDiff=0;
    
    ldddshu1DeflationByHybridDds=0;  

    ldddshu1DefByHybridDdsFL=0;
    ldddshu1DefByHybridDdsFR=0;
    ldddshu1DefByHybridDdsRL=0;
    ldddshu1DefByHybridDdsRR=0;       
	#endif

}
/*============================================================================================================================*/
#if !defined __MATLAB_COMPILE
void LSDDS_vReadEEPROMDDSData(void)
{			
     uint16_t ReadEepBuffer;
			
	if((dds_eep_read_ok_flg1==0)||(dds_eep_read_ok_flg2==0)||(dds_eep_read_ok_flg3==0)||(dds_eep_read_ok_flg4==0)||(dds_eep_read_ok_flg5==0)||
		(dds_eep_read_ok_flg6==0)||(dds_eep_read_ok_flg7==0)||(dds_eep_read_ok_flg8==0)||(dds_eep_read_ok_flg9==0))
	{
		read_dds_eeprom_end_flg=0;
	}
	else if((dds_eep_read_ok_flg10==0)||(dds_eep_read_ok_flg11==0)||(dds_eep_read_ok_flg12==0)||(dds_eep_read_ok_flg13==0)||(dds_eep_read_ok_flg14==0)||
		(dds_eep_read_ok_flg15==0)||(dds_eep_read_ok_flg16==0)||(dds_eep_read_ok_flg17==0)||(dds_eep_read_ok_flg18==0))
	{
		read_dds_eeprom_end_flg=0;
	}
	else if((dds_eep_read_ok_flg19==0)||(dds_eep_read_ok_flg20==0)||(dds_eep_read_ok_flg21==0)||(dds_eep_read_ok_flg22==0)||(dds_eep_read_ok_flg23==0)||
		(dds_eep_read_ok_flg24==0)||(dds_eep_read_ok_flg25==0)||(dds_eep_read_ok_flg26==0)||(dds_eep_read_ok_flg27==0))
	{
		read_dds_eeprom_end_flg=0;
	}
	else if((dds_eep_read_ok_flg28==0)||(dds_eep_read_ok_flg29==0)||(dds_eep_read_ok_flg30==0)||(dds_eep_read_ok_flg31==0)||(dds_eep_read_ok_flg32==0)||
		(dds_eep_read_ok_flg33==0)||(dds_eep_read_ok_flg34==0)||(dds_eep_read_ok_flg35==0)||(dds_eep_read_ok_flg36==0))
	{
		read_dds_eeprom_end_flg=0;
	}
	else if((dds_eep_read_ok_flg37==0)||(dds_eep_read_ok_flg38==0)||(dds_eep_read_ok_flg39==0)||(dds_eep_read_ok_flg40==0)||(dds_eep_read_ok_flg41==0)||
		(dds_eep_read_ok_flg42==0)||(dds_eep_read_ok_flg43==0)||(dds_eep_read_ok_flg44==0)||(dds_eep_read_ok_flg45==0))
	{
		read_dds_eeprom_end_flg=0;
	}
	else if((dds_eep_read_ok_flg46==0)||(dds_eep_read_ok_flg47==0)||(dds_eep_read_ok_flg48==0)||(dds_eep_read_ok_flg49==0)||(dds_eep_read_ok_flg50==0)||
		(dds_eep_read_ok_flg51==0)||(dds_eep_read_ok_flg52==0)||(dds_eep_read_ok_flg53==0)||(dds_eep_read_ok_flg54==0))
	{
		read_dds_eeprom_end_flg=0;
	}
	else if((dds_eep_read_ok_flg55==0)||(dds_eep_read_ok_flg56==0)||(dds_eep_read_ok_flg57==0))
	{
		read_dds_eeprom_end_flg=0;
	}
	#if __2WHL_Deflation_Detect
	else if((dds_eep_read_ok_flg58==0)||(dds_eep_read_ok_flg59==0))
	{
		read_dds_eeprom_end_flg=0;
	}	
	#endif		

	#if __3WHL_Deflation_Detect
	else if((dds_eep_read_ok_flg60==0)||(dds_eep_read_ok_flg61==0)||(dds_eep_read_ok_flg62==0)||(dds_eep_read_ok_flg63==0))
	{
		read_dds_eeprom_end_flg=0;
	}		
	#endif						
	
	#if __Hybrid_DDS
    else if((ddsh_eep_read_ok_flg1==0)||(ddsh_eep_read_ok_flg2==0)||(ddsh_eep_read_ok_flg3==0))
	{
		read_dds_eeprom_end_flg=0;
	}	
	#endif
	else
	{
		read_dds_eeprom_end_flg=1;
	}
	
	if(read_dds_eeprom_end_flg==0)
	{
		if(dds_eep_read_ok_flg1==0)
		{
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_1,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddsu16CntCalibTotalRev[0] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg1=1;
	        }
	        else 
	        {
	        	;
	        }
	    }
		if(dds_eep_read_ok_flg2==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_2,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddsu16CntCalibTotalRev[1] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg2=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }
		if(dds_eep_read_ok_flg3==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_3,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddsu16CntCalibTotalRev[2] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg3=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }	
		if(dds_eep_read_ok_flg4==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_4,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddsu16CntCalibTotalRev[3] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg4=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }
	    
		if(dds_eep_read_ok_flg5==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_5,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddsu16CntCalibTotalRev[4] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg5=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }	    			
		if(dds_eep_read_ok_flg6==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_6,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16CumulativeMean[0][0] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg6=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }
		if(dds_eep_read_ok_flg7==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_7,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16CumulativeMean[0][1] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg7=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }	    
		if(dds_eep_read_ok_flg8==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_8,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16CumulativeMean[0][2] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg8=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }

		if(dds_eep_read_ok_flg9==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_9,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16CumulativeMean[0][3] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg9=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }
		if(dds_eep_read_ok_flg10==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_10,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16CumulativeMean[0][4] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg10=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }	    
	    
		if(dds_eep_read_ok_flg11==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_11,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16CumulativeMean[1][0] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg11=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }
		if(dds_eep_read_ok_flg12==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_12,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16CumulativeMean[1][1] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg12=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }	    
		if(dds_eep_read_ok_flg13==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_13,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16CumulativeMean[1][2] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg13=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }
		if(dds_eep_read_ok_flg14==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_14,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16CumulativeMean[1][3] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg14=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }		
		if(dds_eep_read_ok_flg15==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_15,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16CumulativeMean[1][4] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg15=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }		
		if(dds_eep_read_ok_flg16==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_16,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16CumulativeMean[2][0] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg16=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }		
		if(dds_eep_read_ok_flg17==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_17,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16CumulativeMean[2][1] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg17=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }			
		if(dds_eep_read_ok_flg18==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_18,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16CumulativeMean[2][2] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg18=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }			
		if(dds_eep_read_ok_flg19==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_19,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16CumulativeMean[2][3] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg19=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }			
		if(dds_eep_read_ok_flg20==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_20,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16CumulativeMean[2][4] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg20=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }		
		if(dds_eep_read_ok_flg21==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_21,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16CumulativeMean[3][0] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg21=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }			
		if(dds_eep_read_ok_flg22==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_22,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16CumulativeMean[3][1] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg22=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }		
		if(dds_eep_read_ok_flg23==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_23,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16CumulativeMean[3][2] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg23=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }			
		if(dds_eep_read_ok_flg24==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_24,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16CumulativeMean[3][3] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg24=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }			
		if(dds_eep_read_ok_flg25==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_25,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16CumulativeMean[3][4] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg25=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }	
		if(dds_eep_read_ok_flg26==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_26,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16Dct1WhlDefCnt[0][0] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg26=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }			
		if(dds_eep_read_ok_flg27==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_27,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16Dct1WhlDefCnt[0][1] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg27=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }		
		if(dds_eep_read_ok_flg28==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_28,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16Dct1WhlDefCnt[0][2] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg28=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }			
		if(dds_eep_read_ok_flg29==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_29,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16Dct1WhlDefCnt[0][3] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg29=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }							
			
		if(dds_eep_read_ok_flg30==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_30,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16Dct1WhlDefCnt[0][4] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg30=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }
		if(dds_eep_read_ok_flg31==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_31,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16TireWereInMileageLeft = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg31=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }				
		if(dds_eep_read_ok_flg32==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_32,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16TireWereInMileageRight = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg32=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }
		if(dds_eep_read_ok_flg33==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_33,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16Dct1WhlDefCnt[1][0] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg33=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }
		if(dds_eep_read_ok_flg34==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_34,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16Dct1WhlDefCnt[1][1] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg34=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }
		if(dds_eep_read_ok_flg35==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_35,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16Dct1WhlDefCnt[1][2] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg35=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }
		if(dds_eep_read_ok_flg36==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_36,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16Dct1WhlDefCnt[1][3] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg36=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }
	    
		if(dds_eep_read_ok_flg37==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_37,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16Dct1WhlDefCnt[1][4] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg37=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }	
		
		if(dds_eep_read_ok_flg38==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_38,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16RollingCntLeft = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg38=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }				
		if(dds_eep_read_ok_flg39==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_39,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16RollingCntRight = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg39=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }	
		if(dds_eep_read_ok_flg40==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_40,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddsu1DeflationState = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg40=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }
#if __3WHL_Deflation_Detect
		if(dds_eep_read_ok_flg41==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_41,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16Dct3WhlDefCnt[0][0] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg41=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }	
		if(dds_eep_read_ok_flg42==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_42,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16Dct3WhlDefCnt[1][0] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg42=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }				
		if(dds_eep_read_ok_flg43==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_43,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16Dct3WhlDefCnt[2][0] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg43=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }	
		if(dds_eep_read_ok_flg44==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_44,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16Dct3WhlDefCnt[3][0] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg44=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }	
#else	    								
		if(dds_eep_read_ok_flg41==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_41,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddsu8SingleWhlDeflation[0] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg41=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }	
		if(dds_eep_read_ok_flg42==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_42,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddsu8SingleWhlDeflation[1] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg42=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }				
		if(dds_eep_read_ok_flg43==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_43,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddsu8SingleWhlDeflation[2] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg43=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }	
		if(dds_eep_read_ok_flg44==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_44,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddsu8SingleWhlDeflation[3] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg44=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }	
#endif	    	
		if(dds_eep_read_ok_flg45==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_45,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddsu1TireWearInCompleted = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg45=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }									

		if(dds_eep_read_ok_flg46==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_46,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16Dct1WhlDefCnt[2][0] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg46=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }	

		if(dds_eep_read_ok_flg47==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_47,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16Dct1WhlDefCnt[2][1] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg47=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }	
		if(dds_eep_read_ok_flg48==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_48,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16Dct1WhlDefCnt[2][2] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg48=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }	
		if(dds_eep_read_ok_flg49==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_49,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16Dct1WhlDefCnt[2][3] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg49=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }	
		if(dds_eep_read_ok_flg50==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_50,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16Dct1WhlDefCnt[2][4] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg50=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }	
		if(dds_eep_read_ok_flg51==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_51,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16Dct1WhlDefCnt[3][0] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg51=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }		
	    
		if(dds_eep_read_ok_flg52==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_52,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16Dct1WhlDefCnt[3][1] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg52=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }		    
		if(dds_eep_read_ok_flg53==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_53,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16Dct1WhlDefCnt[3][2] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg53=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }		    
		if(dds_eep_read_ok_flg54==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_54,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16Dct1WhlDefCnt[3][3] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg54=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }		    
		if(dds_eep_read_ok_flg55==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_55,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16Dct1WhlDefCnt[3][4] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg55=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }	
		if(dds_eep_read_ok_flg56==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_56,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldu8DDSModeEepromValue = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg56=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }	
	    	    
		if(dds_eep_read_ok_flg57==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_57,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddsu8DeflationStateFlags = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg57=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }
#if __2WHL_Deflation_Detect	
		if(dds_eep_read_ok_flg58==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_58,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16DctFLRRDefCnt = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg58=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }
		if(dds_eep_read_ok_flg59==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_59,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16DctFRRLDefCnt = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg59=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }
#endif	 
   
#if __3WHL_Deflation_Detect  
		if(dds_eep_read_ok_flg60==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_60,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16Dct3WhlDefCnt[0][1] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg60=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }	
		if(dds_eep_read_ok_flg61==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_61,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16Dct3WhlDefCnt[1][1] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg61=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }
		if(dds_eep_read_ok_flg62==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_62,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16Dct3WhlDefCnt[2][1] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg62=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }
		if(dds_eep_read_ok_flg63==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDS_EERPOM_DATA_63,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ldddss16Dct3WhlDefCnt[3][1] = *(int16_t*)&com_rx_buf[0];	
	            }
	            dds_eep_read_ok_flg63=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }	    	    	        	    		    
#endif	  	       	    	
#if __Hybrid_DDS
		if(ddsh_eep_read_ok_flg1==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDSh_EERPOM_DATA_1,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ReadEepBuffer = *(int16_t*)&com_rx_buf[0];	
	            	ldddshs16InitTpmsPressure=0xFF&(ReadEepBuffer>>8);
                    ldddshu1DeflationByHybridDds=0xFF&((ReadEepBuffer<<8)>>8);//??

	            }
	            ddsh_eep_read_ok_flg1=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }	    	    	        	    		    
		if(ddsh_eep_read_ok_flg2==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDSh_EERPOM_DATA_2,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ReadEepBuffer = *(int16_t*)&com_rx_buf[0];	
	            	ldddshu8DefDctCntByTpmsSnr=0xFF&(ReadEepBuffer>>8);
                    ldddshu8DefDctCntByAxleDiff=0xFF&((ReadEepBuffer<<8)>>8);//??

	            }
	            ddsh_eep_read_ok_flg2=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }	    	    	        	    		    
		if(ddsh_eep_read_ok_flg3==0)
		{		
			if(WE_u8ReadEEP_Byte(U16_DDSh_EERPOM_DATA_3,&com_rx_buf[0],4)==1) 
	        { 
	        	if((*(uint16_t*)&com_rx_buf[0]+*(uint16_t*)&com_rx_buf[2])==0xffff) 
	            {
	            	ReadEepBuffer = *(int16_t*)&com_rx_buf[0];	
	            	ldddshu8DefDctCntBySideDiff=0xFF&(ReadEepBuffer>>8);
                    ldddshu8DefDctCntByDiagDiff=0xFF&((ReadEepBuffer<<8)>>8);//??

	            }
	            ddsh_eep_read_ok_flg3=1;
	        }
	        else 
	        {
	        	;
	        }	        
	    }	    	    	        	    		    
#endif



	}		
}


/*=====================================================================*/
void LSDDS_vWriteEEPROMDDSData(void)
{
     uint16_t EepBuffer;
	if(ldu8DDSMode!=FUNCTION_IGNON)
	{
		ldu8DDSModeEepromValue=ldu8DDSMode;
	}
	
	
	if((dds_eep_wr_ok_flg1==0)||(dds_eep_wr_ok_flg2==0)||(dds_eep_wr_ok_flg3==0)||(dds_eep_wr_ok_flg4==0)||(dds_eep_wr_ok_flg5==0)||
		(dds_eep_wr_ok_flg6==0)||(dds_eep_wr_ok_flg7==0)||(dds_eep_wr_ok_flg8==0)||(dds_eep_wr_ok_flg9==0))
	{
		write_dds_eeprom_end_flg=0;
	}
	else if((dds_eep_wr_ok_flg10==0)||(dds_eep_wr_ok_flg11==0)||(dds_eep_wr_ok_flg12==0)||(dds_eep_wr_ok_flg13==0)||(dds_eep_wr_ok_flg14==0)||
		(dds_eep_wr_ok_flg15==0)||(dds_eep_wr_ok_flg16==0)||(dds_eep_wr_ok_flg17==0)||(dds_eep_wr_ok_flg18==0))
	{
		write_dds_eeprom_end_flg=0;
	}
	else if((dds_eep_wr_ok_flg19==0)||(dds_eep_wr_ok_flg20==0)||(dds_eep_wr_ok_flg21==0)||(dds_eep_wr_ok_flg22==0)||(dds_eep_wr_ok_flg23==0)||
		(dds_eep_wr_ok_flg24==0)||(dds_eep_wr_ok_flg25==0)||(dds_eep_wr_ok_flg26==0)||(dds_eep_wr_ok_flg27==0))
	{
		write_dds_eeprom_end_flg=0;
	}
	else if((dds_eep_wr_ok_flg28==0)||(dds_eep_wr_ok_flg29==0)||(dds_eep_wr_ok_flg30==0)||(dds_eep_wr_ok_flg31==0)||(dds_eep_wr_ok_flg32==0)||
		(dds_eep_wr_ok_flg33==0)||(dds_eep_wr_ok_flg34==0)||(dds_eep_wr_ok_flg35==0)||(dds_eep_wr_ok_flg36==0))
	{
		write_dds_eeprom_end_flg=0;
	}
	else if((dds_eep_wr_ok_flg37==0)||(dds_eep_wr_ok_flg38==0)||(dds_eep_wr_ok_flg39==0)||(dds_eep_wr_ok_flg40==0)||(dds_eep_wr_ok_flg41==0)||
		(dds_eep_wr_ok_flg42==0)||(dds_eep_wr_ok_flg43==0)||(dds_eep_wr_ok_flg44==0)||(dds_eep_wr_ok_flg45==0))
	{
		write_dds_eeprom_end_flg=0;
	}
	else if((dds_eep_wr_ok_flg46==0)||(dds_eep_wr_ok_flg47==0)||(dds_eep_wr_ok_flg48==0)||(dds_eep_wr_ok_flg49==0)||(dds_eep_wr_ok_flg50==0)||
		(dds_eep_wr_ok_flg51==0)||(dds_eep_wr_ok_flg52==0)||(dds_eep_wr_ok_flg53==0)||(dds_eep_wr_ok_flg54==0))
	{
		write_dds_eeprom_end_flg=0;
	}	
	else if((dds_eep_wr_ok_flg55==0)||(dds_eep_wr_ok_flg56==0)||(dds_eep_wr_ok_flg57==0))
	{
		write_dds_eeprom_end_flg=0;
	}
	#if __2WHL_Deflation_Detect	
	else if((dds_eep_wr_ok_flg58==0)||(dds_eep_wr_ok_flg59==0))
	{
		write_dds_eeprom_end_flg=0;
	}
	#endif
	
	#if __3WHL_Deflation_Detect	
	else if((dds_eep_wr_ok_flg60==0)||(dds_eep_wr_ok_flg61==0)||(dds_eep_wr_ok_flg62==0)||(dds_eep_wr_ok_flg63==0))
	{
		write_dds_eeprom_end_flg=0;
	}
	#endif	
	#if __Hybrid_DDS
	else if((ddsh_eep_wr_ok_flg1==0)||(ddsh_eep_wr_ok_flg2==0)||(ddsh_eep_wr_ok_flg3==0))
	{
		write_dds_eeprom_end_flg=0;
	}
	#endif
								
	else 
	{
		write_dds_eeprom_end_flg=1;
	}
	dds_temp_flg=1;
	if(write_dds_eeprom_end_flg==0)
	{	
		dds_temp_flg=2;
		if((weu1EraseSector==0)&&(weu1WriteSector==0))
		{
			dds_temp_flg=3;
			if(dds_eep_wr_ok_flg1==0)
			{
				dds_temp_flg=4;
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_1,(uint16_t)ldddsu16CntCalibTotalRev[0])==0)
				{
					dds_eep_wr_ok_flg1=1;
					dds_temp_flg=5;
				}
				else
				{
					dds_temp_flg=12;
				}
			}	
			else if(dds_eep_wr_ok_flg2==0)
			{
				dds_temp_flg=6;
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_2,(uint16_t)ldddsu16CntCalibTotalRev[1])==0)
				{
					dds_eep_wr_ok_flg2=1;
					dds_temp_flg=7;
				}
			}		
			else if(dds_eep_wr_ok_flg3==0)
			{
				dds_temp_flg=8;
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_3,(uint16_t)ldddsu16CntCalibTotalRev[2])==0)
				{
					dds_eep_wr_ok_flg3=1;
					dds_temp_flg=9;
				}
			}		
			else if(dds_eep_wr_ok_flg4==0)
			{
				dds_temp_flg=10;
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_4,(uint16_t)ldddsu16CntCalibTotalRev[3])==0)
				{
					dds_eep_wr_ok_flg4=1;
					dds_temp_flg=11;
				}
			}	
			else if(dds_eep_wr_ok_flg5==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_5,(uint16_t)ldddsu16CntCalibTotalRev[4])==0)
				{
					dds_eep_wr_ok_flg5=1;
				}
			}
			else if(dds_eep_wr_ok_flg6==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_6,(uint16_t)ldddss16CumulativeMean[0][0])==0)
				{
					dds_eep_wr_ok_flg6=1;
				}	
			}	
			else if(dds_eep_wr_ok_flg7==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_7,(uint16_t)ldddss16CumulativeMean[0][1])==0)
				{
					dds_eep_wr_ok_flg7=1;
				}	
			}	
			else if(dds_eep_wr_ok_flg8==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_8,(uint16_t)ldddss16CumulativeMean[0][2])==0)
				{
					dds_eep_wr_ok_flg8=1;
				}	
			}
			else if(dds_eep_wr_ok_flg9==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_9,(uint16_t)ldddss16CumulativeMean[0][3])==0)
				{
					dds_eep_wr_ok_flg9=1;
				}	
			}	
			else if(dds_eep_wr_ok_flg10==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_10,(uint16_t)ldddss16CumulativeMean[0][4])==0)
				{
					dds_eep_wr_ok_flg10=1;
				}	
			}	
			else if(dds_eep_wr_ok_flg11==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_11,(uint16_t)ldddss16CumulativeMean[1][0])==0)
				{
					dds_eep_wr_ok_flg11=1;
				}	
			}			
			else if(dds_eep_wr_ok_flg12==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_12,(uint16_t)ldddss16CumulativeMean[1][1])==0)
				{
					dds_eep_wr_ok_flg12=1;
				}	
			}			
			
			else if(dds_eep_wr_ok_flg13==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_13,(uint16_t)ldddss16CumulativeMean[1][2])==0)
				{
					dds_eep_wr_ok_flg13=1;
				}	
			}			
			
			else if(dds_eep_wr_ok_flg14==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_14,(uint16_t)ldddss16CumulativeMean[1][3])==0)
				{
					dds_eep_wr_ok_flg14=1;
				}	
			}			
			else if(dds_eep_wr_ok_flg15==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_15,(uint16_t)ldddss16CumulativeMean[1][4])==0)
				{
					dds_eep_wr_ok_flg15=1;
				}	
			}			
			
			else if(dds_eep_wr_ok_flg16==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_16,(uint16_t)ldddss16CumulativeMean[2][0])==0)
				{
					dds_eep_wr_ok_flg16=1;
				}	
			}			
			else if(dds_eep_wr_ok_flg17==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_17,(uint16_t)ldddss16CumulativeMean[2][1])==0)
				{
					dds_eep_wr_ok_flg17=1;
				}	
			}			
			
			else if(dds_eep_wr_ok_flg18==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_18,(uint16_t)ldddss16CumulativeMean[2][2])==0)
				{
					dds_eep_wr_ok_flg18=1;
				}	
			}			
			
			else if(dds_eep_wr_ok_flg19==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_19,(uint16_t)ldddss16CumulativeMean[2][3])==0)
				{
					dds_eep_wr_ok_flg19=1;
				}	
			}			
			else if(dds_eep_wr_ok_flg20==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_20,(uint16_t)ldddss16CumulativeMean[2][4])==0)
				{
					dds_eep_wr_ok_flg20=1;
				}	
			}				
			else if(dds_eep_wr_ok_flg21==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_21,(uint16_t)ldddss16CumulativeMean[3][0])==0)
				{
					dds_eep_wr_ok_flg21=1;
				}	
			}			
			else if(dds_eep_wr_ok_flg22==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_22,(uint16_t)ldddss16CumulativeMean[3][1])==0)
				{
					dds_eep_wr_ok_flg22=1;
				}	
			}			
			
			else if(dds_eep_wr_ok_flg23==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_23,(uint16_t)ldddss16CumulativeMean[3][2])==0)
				{
					dds_eep_wr_ok_flg23=1;
				}	
			}			
			
			else if(dds_eep_wr_ok_flg24==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_24,(uint16_t)ldddss16CumulativeMean[3][3])==0)
				{
					dds_eep_wr_ok_flg24=1;
				}	
			}			
			else if(dds_eep_wr_ok_flg25==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_25,(uint16_t)ldddss16CumulativeMean[3][4])==0)
				{
					dds_eep_wr_ok_flg25=1;
				}	
			}				
							
			else if(dds_eep_wr_ok_flg26==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_26,(uint16_t)ldddss16Dct1WhlDefCnt[0][0])==0)
				{
					dds_eep_wr_ok_flg26=1;
				}
			}	
			else if(dds_eep_wr_ok_flg27==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_27,(uint16_t)ldddss16Dct1WhlDefCnt[0][1])==0)
				{
					dds_eep_wr_ok_flg27=1;
				}
			}			
			else if(dds_eep_wr_ok_flg28==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_28,(uint16_t)ldddss16Dct1WhlDefCnt[0][2])==0)
				{
					dds_eep_wr_ok_flg28=1;
				}
			}																		
			else if(dds_eep_wr_ok_flg29==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_29,(uint16_t)ldddss16Dct1WhlDefCnt[0][3])==0)
				{
					dds_eep_wr_ok_flg29=1;
				}
			}	
			else if(dds_eep_wr_ok_flg30==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_30,(uint16_t)ldddss16Dct1WhlDefCnt[0][4])==0)
				{
					dds_eep_wr_ok_flg30=1;
				}
			}				
			else if(dds_eep_wr_ok_flg31==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_31,(uint16_t)ldddss16TireWereInMileageLeft)==0)
				{
					dds_eep_wr_ok_flg31=1;
				}
			}
			else if(dds_eep_wr_ok_flg32==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_32,(uint16_t)ldddss16TireWereInMileageRight)==0)
				{
					dds_eep_wr_ok_flg32=1;
				}
			}	

			else if(dds_eep_wr_ok_flg33==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_33,(uint16_t)ldddss16Dct1WhlDefCnt[1][0])==0)
				{
					dds_eep_wr_ok_flg33=1;
				}	
			}		
			else if(dds_eep_wr_ok_flg34==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_34,(uint16_t)ldddss16Dct1WhlDefCnt[1][1])==0)
				{
					dds_eep_wr_ok_flg34=1;
				}	
			}	
			else if(dds_eep_wr_ok_flg35==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_35,(uint16_t)ldddss16Dct1WhlDefCnt[1][2])==0)
				{
					dds_eep_wr_ok_flg35=1;
				}	
			}				
			else if(dds_eep_wr_ok_flg36==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_36,(uint16_t)ldddss16Dct1WhlDefCnt[1][3])==0)
				{
					dds_eep_wr_ok_flg36=1;
				}	
			}	
						
			else if(dds_eep_wr_ok_flg37==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_37,(uint16_t)ldddss16Dct1WhlDefCnt[1][4])==0)
				{
					dds_eep_wr_ok_flg37=1;
				}	
			}

			else if(dds_eep_wr_ok_flg38==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_38,(uint16_t)ldddss16RollingCntLeft)==0)
				{
					dds_eep_wr_ok_flg38=1;
				}	
			}
			else if(dds_eep_wr_ok_flg39==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_39,(uint16_t)ldddss16RollingCntRight)==0)
				{
					dds_eep_wr_ok_flg39=1;
				}	
			}									
			else if(dds_eep_wr_ok_flg40==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_40,(uint16_t)ldddsu1DeflationState)==0)
				{
					dds_eep_wr_ok_flg40=1;
				}			
			}
#if __3WHL_Deflation_Detect
			else if(dds_eep_wr_ok_flg41==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_41,(uint16_t)ldddss16Dct3WhlDefCnt[0][0])==0)
				{
					dds_eep_wr_ok_flg41=1;
				}			
			}	
			else if(dds_eep_wr_ok_flg42==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_42,(uint16_t)ldddss16Dct3WhlDefCnt[1][0])==0)
				{
					dds_eep_wr_ok_flg42=1;
				}			
			}	
			else if(dds_eep_wr_ok_flg43==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_43,(uint16_t)ldddss16Dct3WhlDefCnt[2][0])==0)
				{
					dds_eep_wr_ok_flg43=1;
				}			
			}	
			else if(dds_eep_wr_ok_flg44==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_44,(uint16_t)ldddss16Dct3WhlDefCnt[3][0])==0)
				{
					dds_eep_wr_ok_flg44=1;
				}			
			}	
#else			
			else if(dds_eep_wr_ok_flg41==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_41,(uint16_t)ldddsu8SingleWhlDeflation[0])==0)
				{
					dds_eep_wr_ok_flg41=1;
				}			
			}	
			else if(dds_eep_wr_ok_flg42==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_42,(uint16_t)ldddsu8SingleWhlDeflation[1])==0)
				{
					dds_eep_wr_ok_flg42=1;
				}			
			}	
			else if(dds_eep_wr_ok_flg43==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_43,(uint16_t)ldddsu8SingleWhlDeflation[2])==0)
				{
					dds_eep_wr_ok_flg43=1;
				}			
			}	
			else if(dds_eep_wr_ok_flg44==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_44,(uint16_t)ldddsu8SingleWhlDeflation[3])==0)
				{
					dds_eep_wr_ok_flg44=1;
				}			
			}	
#endif			
			
			else if(dds_eep_wr_ok_flg45==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_45,(uint16_t)ldddsu1TireWearInCompleted)==0)
				{
					dds_eep_wr_ok_flg45=1;
				}			
			}	
			else if(dds_eep_wr_ok_flg46==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_46,(uint16_t)ldddss16Dct1WhlDefCnt[2][0])==0)
				{
					dds_eep_wr_ok_flg46=1;
				}			
			}	
			else if(dds_eep_wr_ok_flg47==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_47,(uint16_t)ldddss16Dct1WhlDefCnt[2][1])==0)
				{
					dds_eep_wr_ok_flg47=1;
				}			
			}	
			else if(dds_eep_wr_ok_flg48==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_48,(uint16_t)ldddss16Dct1WhlDefCnt[2][2])==0)
				{
					dds_eep_wr_ok_flg48=1;
				}			
			}	
			else if(dds_eep_wr_ok_flg49==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_49,(uint16_t)ldddss16Dct1WhlDefCnt[2][3])==0)
				{
					dds_eep_wr_ok_flg49=1;
				}			
			}
			else if(dds_eep_wr_ok_flg50==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_50,(uint16_t)ldddss16Dct1WhlDefCnt[2][4])==0)
				{
					dds_eep_wr_ok_flg50=1;
				}			
			}
			else if(dds_eep_wr_ok_flg51==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_51,(uint16_t)ldddss16Dct1WhlDefCnt[3][0])==0)
				{
					dds_eep_wr_ok_flg51=1;
				}			
			}	
			else if(dds_eep_wr_ok_flg52==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_52,(uint16_t)ldddss16Dct1WhlDefCnt[3][1])==0)
				{
					dds_eep_wr_ok_flg52=1;
				}			
			}	
			else if(dds_eep_wr_ok_flg53==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_53,(uint16_t)ldddss16Dct1WhlDefCnt[3][2])==0)
				{
					dds_eep_wr_ok_flg53=1;
				}			
			}	
			else if(dds_eep_wr_ok_flg54==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_54,(uint16_t)ldddss16Dct1WhlDefCnt[3][3])==0)
				{
					dds_eep_wr_ok_flg54=1;
				}			
			}
			else if(dds_eep_wr_ok_flg55==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_55,(uint16_t)ldddss16Dct1WhlDefCnt[3][4])==0)
				{
					dds_eep_wr_ok_flg55=1;
				}			
			}	
			
			else if(dds_eep_wr_ok_flg56==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_56,(uint16_t)ldu8DDSModeEepromValue)==0)
				{
					dds_eep_wr_ok_flg56=1;
				}			
			}	
			else if(dds_eep_wr_ok_flg57==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_57,(uint16_t)ldddsu8DeflationStateFlags)==0)
				{
					dds_eep_wr_ok_flg57=1;
				}			
			}			
			
#if __2WHL_Deflation_Detect			
			else if(dds_eep_wr_ok_flg58==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_58,(uint16_t)ldddss16DctFLRRDefCnt)==0)
				{
					dds_eep_wr_ok_flg58=1;
				}			
			}	
			else if(dds_eep_wr_ok_flg59==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_59,(uint16_t)ldddss16DctFRRLDefCnt)==0)
				{
					dds_eep_wr_ok_flg59=1;
				}			
			}
			
#endif

#if __3WHL_Deflation_Detect	
			else if(dds_eep_wr_ok_flg60==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_60,(uint16_t)ldddss16Dct3WhlDefCnt[0][1])==0)
				{
					dds_eep_wr_ok_flg60=1;
				}			
			}					
			else if(dds_eep_wr_ok_flg61==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_61,(uint16_t)ldddss16Dct3WhlDefCnt[1][1])==0)
				{
					dds_eep_wr_ok_flg61=1;
				}			
			}	
			else if(dds_eep_wr_ok_flg62==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_62,(uint16_t)ldddss16Dct3WhlDefCnt[2][1])==0)
				{
					dds_eep_wr_ok_flg62=1;
				}			
			}
			else if(dds_eep_wr_ok_flg63==0)
			{
				if(LSESP_u16WriteEEPROM(U16_DDS_EERPOM_DATA_63,(uint16_t)ldddss16Dct3WhlDefCnt[3][1])==0)
				{
					dds_eep_wr_ok_flg63=1;
				}			
			}		
#endif										
#if __Hybrid_DDS
			else if(ddsh_eep_wr_ok_flg1==0)
			{
			    EepBuffer=((uint16_t)ldddshs16InitTpmsPressure<<8)|(ldddshu1DeflationByHybridDds);  
			    
				if(LSESP_u16WriteEEPROM(U16_DDSh_EERPOM_DATA_1,(uint16_t)EepBuffer)==0)
				{
					ddsh_eep_wr_ok_flg1=1;
				}			
			}	
			else if(ddsh_eep_wr_ok_flg2==0)
			{
			    EepBuffer=((uint16_t)ldddshu8DefDctCntByTpmsSnr<<8)|(ldddshu8DefDctCntByAxleDiff);  
			    
				if(LSESP_u16WriteEEPROM(U16_DDSh_EERPOM_DATA_2,(uint16_t)EepBuffer)==0)
				{
					ddsh_eep_wr_ok_flg2=1;
				}			
			}
			else if(ddsh_eep_wr_ok_flg3==0)
			{
			    EepBuffer=((uint16_t)ldddshu8DefDctCntBySideDiff<<8)|(ldddshu8DefDctCntByDiagDiff);  
			    
				if(LSESP_u16WriteEEPROM(U16_DDSh_EERPOM_DATA_3,(uint16_t)EepBuffer)==0)
				{
					ddsh_eep_wr_ok_flg3=1;
				}			
			}							
#endif
																				
			else
			{
				;
			}				
									
				
		}
	}
}
#endif
int LCDDS_s16IInter5Point( int input_x, int x_1, int y_1, int x_2, int y_2, int x_3, int y_3, int x_4, int y_4, int x_5, int y_5 )
{

	int16_t interpolation_tempD0;
	int16_t interpolation_tempD9;


    if( input_x <= x_1 )
        interpolation_tempD9 = y_1;
    else if ( input_x < x_2 ) {
/************************* CT 개선 전********** 2005.02.21  *************************** 
    //  tempW9 =  y_1 + (int)((((long)(y_2-y_1))*(input_x-x_1))/(x_2-x_1));
        tempW2 = (y_2-y_1); tempW1 = (input_x-x_1); tempW0 = (x_2-x_1);
        if ( tempW0==0 ) tempW0=1;
        s16muls16divs16();
        tempW9 = y_1 + tempW3;         
**************************************************************************************/ 

/**************************** CT 개선 후 : 2005.02.21(C 코드로 환원) ****************/
        interpolation_tempD0=(x_2-x_1);
        if (interpolation_tempD0==0) interpolation_tempD0=1;
        else{}
        interpolation_tempD9 =  y_1 + (int)(((long)(y_2-y_1)*(input_x-x_1))/interpolation_tempD0);
/************************************************************************************/
        
    } else if ( input_x < x_3 ) {
/************************* CT 개선 전********** 2005.02.21  *************************    
    //  tempW9 =  y_2 + (int)((((long)(y_3-y_2))*(input_x-x_2))/(x_3-x_2));
        tempW2 = (y_3-y_2); tempW1 = (input_x-x_2); tempW0 = (x_3-x_2);
        if ( tempW0==0 ) tempW0=1;
        s16muls16divs16();
        tempW9 = y_2 + tempW3;         
************************************************************************************/        
        
/**************************** CT 개선 후 : 2005.02.21(C 코드로 환원) **************/
        interpolation_tempD0=(x_3-x_2);
        if (interpolation_tempD0==0) interpolation_tempD0=1;
        else{}
        interpolation_tempD9 =  y_2 + (int)(((long)(y_3-y_2)*(input_x-x_2))/interpolation_tempD0);
/**********************************************************************************/        
    } else if ( input_x < x_4 ) {
/************************* CT 개선 전********** 2005.02.21  *************************    
    //  tempW9 =  y_2 + (int)((((long)(y_3-y_2))*(input_x-x_2))/(x_3-x_2));
        tempW2 = (y_4-y_3); tempW1 = (input_x-x_3); tempW0 = (x_4-x_3);
        if ( tempW0==0 ) tempW0=1;
        s16muls16divs16();
        tempW9 = y_3 + tempW3;
************************************************************************************/        
        
/**************************** CT 개선 후 : 2005.02.21(C 코드로 환원) **************/
        interpolation_tempD0=(x_4-x_3);
        if (interpolation_tempD0==0) interpolation_tempD0=1;
        else{}
        interpolation_tempD9 =  y_3 + (int)(((long)(y_4-y_3)*(input_x-x_3))/interpolation_tempD0);
/**********************************************************************************/        
    } else if ( input_x < x_5 ) {
/************************* CT 개선 전********** 2005.02.21  *************************    
    //  tempW9 =  y_4 + (int)((((long)(y_5-y_4))*(input_x-x_4))/(x_5-x_4));
        tempW2 = (y_5-y_4); tempW1 = (input_x-x_4); tempW0 = (x_5-x_4);
        if ( tempW0==0 ) tempW0=1;
        s16muls16divs16();
        tempW9 = y_4 + tempW3;        
************************************************************************************/        
        
/**************************** CT 개선 후 : 2005.02.21(C 코드로 환원) **************/
        interpolation_tempD0=(x_5-x_4);
        if (interpolation_tempD0==0) interpolation_tempD0=1;
        else{}
        interpolation_tempD9 =  y_4 + (int)(((long)(y_5-y_4)*(input_x-x_4))/interpolation_tempD0);
/**********************************************************************************/        
    } else
        interpolation_tempD9 = y_5;
            
    return interpolation_tempD9;      
}
/*============================================================================================================================*/
#if defined(__LOGGER) && !defined __MATLAB_COMPILE
    #if __LOGGER_DATA_TYPE==1
#include "LSCallLogData.h"

void	LSDDS_vCallDDSLogData(void)
{
	
    uint8_t i;
    CAN_LOG_DATA[0]  = (uint8_t)(system_loop_counter); 
    CAN_LOG_DATA[1]  = (uint8_t)(system_loop_counter>>8);          
    CAN_LOG_DATA[2]  = (uint8_t)(FL.vrad_resol_change);   
    CAN_LOG_DATA[3]  = (uint8_t)(FL.vrad_resol_change>>8);   
    CAN_LOG_DATA[4]  = (uint8_t)(FR.vrad_resol_change);
    CAN_LOG_DATA[5]  = (uint8_t)(FR.vrad_resol_change>>8); 
    CAN_LOG_DATA[6]  = (uint8_t)(RL.vrad_resol_change);
    CAN_LOG_DATA[7]  = (uint8_t)(RL.vrad_resol_change>>8);                         
    CAN_LOG_DATA[8]  = (uint8_t)(RR.vrad_resol_change);
    CAN_LOG_DATA[9]  = (uint8_t)(RR.vrad_resol_change>>8);                     
    CAN_LOG_DATA[10] = (uint8_t)(vref_resol_change);
    CAN_LOG_DATA[11] = (uint8_t)(vref_resol_change>>8);                     
    CAN_LOG_DATA[12] = (uint8_t)(ldu8DDSMode);	
    CAN_LOG_DATA[13] = (uint8_t)(ldu8DDSSubMode);     
    CAN_LOG_DATA[14] = (uint8_t)(((ldddss16CumulativeMean[FL_WL][v_inx1])>127)?127:(((ldddss16CumulativeMean[FL_WL][v_inx1])<-127)?-127:(int8_t)(ldddss16CumulativeMean[FL_WL][v_inx1])));
    CAN_LOG_DATA[15] = (uint8_t)(((ldddss16CumulativeMean[FR_WL][v_inx1])>127)?127:(((ldddss16CumulativeMean[FR_WL][v_inx1])<-127)?-127:(int8_t)(ldddss16CumulativeMean[FR_WL][v_inx1])));
	CAN_LOG_DATA[16] = (uint8_t)(((ldddss16CumulativeMean[RL_WL][v_inx1])>127)?127:(((ldddss16CumulativeMean[RL_WL][v_inx1])<-127)?-127:(int8_t)(ldddss16CumulativeMean[RL_WL][v_inx1])));
	CAN_LOG_DATA[17] = (uint8_t)(((ldddss16CumulativeMean[RR_WL][v_inx1])>127)?127:(((ldddss16CumulativeMean[RR_WL][v_inx1])<-127)?-127:(int8_t)(ldddss16CumulativeMean[RR_WL][v_inx1])));
	CAN_LOG_DATA[18] = (uint8_t)(((CalibrationValueFL)>127)?127:(((CalibrationValueFL)<-127)?-127:(int8_t)(CalibrationValueFL)));
	CAN_LOG_DATA[19] = (uint8_t)(((CalibrationValueFR)>127)?127:(((CalibrationValueFR)<-127)?-127:(int8_t)(CalibrationValueFR)));
	CAN_LOG_DATA[20] = (uint8_t)(((CalibrationValueRL)>127)?127:(((CalibrationValueRL)<-127)?-127:(int8_t)(CalibrationValueRL)));
	CAN_LOG_DATA[21] = (uint8_t)(((CalibrationValueRR)>127)?127:(((CalibrationValueRR)<-127)?-127:(int8_t)(CalibrationValueRR)));
	CAN_LOG_DATA[22] = (uint8_t)(ldddsu16CntCalibTotalRev[v_inx1]);   
	CAN_LOG_DATA[23] = (uint8_t)(ldddsu16CntCalibTotalRev[v_inx1]>>8);	
    CAN_LOG_DATA[24] = (uint8_t)(ldddsu16ElapsedTimePerRev[FL_WL]);                           
    CAN_LOG_DATA[25] = (uint8_t)(ldddsu16ElapsedTimePerRev[FL_WL]>>8);      					                           
    CAN_LOG_DATA[26] = (uint8_t)(ldddsu16ElapsedTimePerRev[FR_WL]);       					 
    CAN_LOG_DATA[27] = (uint8_t)(ldddsu16ElapsedTimePerRev[FR_WL]>>8); 	 	
   	CAN_LOG_DATA[28] = (uint8_t)(ldddsu16ElapsedTimePerRev[RL_WL]);                          
    CAN_LOG_DATA[29] = (uint8_t)(ldddsu16ElapsedTimePerRev[RL_WL]>>8);     					                            
    CAN_LOG_DATA[30] = (uint8_t)(ldddsu16ElapsedTimePerRev[RR_WL]);      					 
    CAN_LOG_DATA[31] = (uint8_t)(ldddsu16ElapsedTimePerRev[RR_WL]>>8);   	
  	CAN_LOG_DATA[32] = (uint8_t)((((ldddss16Dct1WhlDefCnt[FR_WL][0]))>250)?250:((((ldddss16Dct1WhlDefCnt[FR_WL][0]))<0)?0:(uint8_t)((ldddss16Dct1WhlDefCnt[FR_WL][0])))); 
	CAN_LOG_DATA[33] = (uint8_t)((((ldddss16Dct1WhlDefCnt[FR_WL][1]))>250)?250:((((ldddss16Dct1WhlDefCnt[FR_WL][1]))<0)?0:(uint8_t)((ldddss16Dct1WhlDefCnt[FR_WL][1]))));  	
    CAN_LOG_DATA[34] = (uint8_t)((((ldddss16Dct1WhlDefCnt[FR_WL][2]))>250)?250:((((ldddss16Dct1WhlDefCnt[FR_WL][2]))<0)?0:(uint8_t)((ldddss16Dct1WhlDefCnt[FR_WL][2]))));                         
    CAN_LOG_DATA[35] = (uint8_t)((((ldddss16Dct1WhlDefCnt[FR_WL][3]))>250)?250:((((ldddss16Dct1WhlDefCnt[FR_WL][3]))<0)?0:(uint8_t)((ldddss16Dct1WhlDefCnt[FR_WL][3]))));       					                           
    CAN_LOG_DATA[36] = (uint8_t)((((ldddss16Dct1WhlDefCnt[FR_WL][4]))>250)?250:((((ldddss16Dct1WhlDefCnt[FR_WL][4]))<0)?0:(uint8_t)((ldddss16Dct1WhlDefCnt[FR_WL][4]))));   					 
    CAN_LOG_DATA[37] = (uint8_t)((((ldddss16Dct1WhlDefCnt[RL_WL][0]))>250)?250:((((ldddss16Dct1WhlDefCnt[RL_WL][0]))<0)?0:(uint8_t)((ldddss16Dct1WhlDefCnt[RL_WL][0])))); 	
	CAN_LOG_DATA[38] = (uint8_t)((((ldddss16Dct1WhlDefCnt[RL_WL][1]))>250)?250:((((ldddss16Dct1WhlDefCnt[RL_WL][1]))<0)?0:(uint8_t)((ldddss16Dct1WhlDefCnt[RL_WL][1])))); 
	CAN_LOG_DATA[39] = (uint8_t)((((ldddss16Dct1WhlDefCnt[RL_WL][2]))>250)?250:((((ldddss16Dct1WhlDefCnt[RL_WL][2]))<0)?0:(uint8_t)((ldddss16Dct1WhlDefCnt[RL_WL][2])))); 
	CAN_LOG_DATA[40] = (uint8_t)((((ldddss16Dct1WhlDefCnt[RL_WL][3]))>250)?250:((((ldddss16Dct1WhlDefCnt[RL_WL][3]))<0)?0:(uint8_t)((ldddss16Dct1WhlDefCnt[RL_WL][3])))); 
	CAN_LOG_DATA[41] = (uint8_t)((((ldddss16Dct1WhlDefCnt[RL_WL][4]))>250)?250:((((ldddss16Dct1WhlDefCnt[RL_WL][4]))<0)?0:(uint8_t)((ldddss16Dct1WhlDefCnt[RL_WL][4])))); 
    CAN_LOG_DATA[42] = (uint8_t)(((ldddss16AxleDiffRunningAvg[FR_WL][0]/10)>127)?127:(((ldddss16AxleDiffRunningAvg[FR_WL][0]/10)<-127)?-127:(int8_t)(ldddss16AxleDiffRunningAvg[FR_WL][0]/10)));
    CAN_LOG_DATA[43] = (uint8_t)(((ldddss16SideDiffRunningAvg[FR_WL][0]/10)>127)?127:(((ldddss16SideDiffRunningAvg[FR_WL][0]/10)<-127)?-127:(int8_t)(ldddss16SideDiffRunningAvg[FR_WL][0]/10)));
    CAN_LOG_DATA[44] = (uint8_t)(((ldddss16DiagDiffRunningAvg[FR_WL][0]/10)>127)?127:(((ldddss16DiagDiffRunningAvg[FR_WL][0]/10)<-127)?-127:(int8_t)(ldddss16DiagDiffRunningAvg[FR_WL][0]/10)));
    CAN_LOG_DATA[45] = (uint8_t)(((ldddss16AxleDiffRunningAvg[RL_WL][0]/10)>127)?127:(((ldddss16AxleDiffRunningAvg[RL_WL][0]/10)<-127)?-127:(int8_t)(ldddss16AxleDiffRunningAvg[RL_WL][0]/10)));    
    CAN_LOG_DATA[46] = (uint8_t)(((ldddss16SideDiffRunningAvg[RL_WL][0]/10)>127)?127:(((ldddss16SideDiffRunningAvg[RL_WL][0]/10)<-127)?-127:(int8_t)(ldddss16SideDiffRunningAvg[RL_WL][0]/10)));  
    CAN_LOG_DATA[47] = (uint8_t)(((ldddss16DiagDiffRunningAvg[FL_WL][0]/10)>127)?127:(((ldddss16DiagDiffRunningAvg[FL_WL][0]/10)<-127)?-127:(int8_t)(ldddss16DiagDiffRunningAvg[FL_WL][0]/10))); 
    CAN_LOG_DATA[48] = (uint8_t)(((ldddss8PressEstByAxleDiff[FR_WL])>127)?127:(((ldddss8PressEstByAxleDiff[FR_WL])<-127)?-127:(int8_t)(ldddss8PressEstByAxleDiff[FR_WL])));                                                   
    CAN_LOG_DATA[49] = (uint8_t)(((ldddss8PressEstBySideDiff[FR_WL])>127)?127:(((ldddss8PressEstBySideDiff[FR_WL])<-127)?-127:(int8_t)(ldddss8PressEstBySideDiff[FR_WL]))); 
    CAN_LOG_DATA[50] = (uint8_t)(((ldddss8PressEstByDiagDiff[FR_WL])>127)?127:(((ldddss8PressEstByDiagDiff[FR_WL])<-127)?-127:(int8_t)(ldddss8PressEstByDiagDiff[FR_WL])));                                                   
    CAN_LOG_DATA[51] = (uint8_t)(((ldddss8PressEstByAxleDiff[RL_WL])>127)?127:(((ldddss8PressEstByAxleDiff[RL_WL])<-127)?-127:(int8_t)(ldddss8PressEstByAxleDiff[RL_WL]))); 
    CAN_LOG_DATA[52] = (uint8_t)(((ldddss8PressEstBySideDiff[RL_WL])>127)?127:(((ldddss8PressEstBySideDiff[RL_WL])<-127)?-127:(int8_t)(ldddss8PressEstBySideDiff[RL_WL]))); 
    CAN_LOG_DATA[53] = (uint8_t)(((ldddss8PressEstByDiagDiff[RL_WL])>127)?127:(((ldddss8PressEstByDiagDiff[RL_WL])<-127)?-127:(int8_t)(ldddss8PressEstByDiagDiff[RL_WL]))); 

  
    if(ldddsu8CompleteCalib[v_inx1] ==0)
    {
    CAN_LOG_DATA[54] = (uint8_t)(ldddsu8CalibSamplingDistCnt[FL_WL][v_inx1]);//(fsu8NumberOfEdge_fl);                                                                                                                             	
    CAN_LOG_DATA[55] = (uint8_t)(ldddsu8CalibSamplingDistCnt[FR_WL][v_inx1]);//(fsu8NumberOfEdge_fr);                                                                                                                          
    CAN_LOG_DATA[56] = (uint8_t)(ldddsu8CalibSamplingDistCnt[RL_WL][v_inx1]);//(fsu8NumberOfEdge_rl);                                                                                                                          
    CAN_LOG_DATA[57] = (uint8_t)(ldddsu8CalibSamplingDistCnt[RR_WL][v_inx1]);//(fsu8NumberOfEdge_rr);  
	}
	else
	{
    CAN_LOG_DATA[54] = (uint8_t)(ldddss16Dct3WhlDefCnt[FR_WL][0]);//(fsu8NumberOfEdge_fl);                                                                                                                             	
    CAN_LOG_DATA[55] = (uint8_t)(ldddss16Dct3WhlDefCnt[FR_WL][1]);//(fsu8NumberOfEdge_fr);                                                                                                                          
    CAN_LOG_DATA[56] = (uint8_t)(ldddss16Dct3WhlDefCnt[RL_WL][0]);//(fsu8NumberOfEdge_rl);                                                                                                                          
    CAN_LOG_DATA[57] = (uint8_t)(ldddss16Dct3WhlDefCnt[RL_WL][1]);//(fsu8NumberOfEdge_rr);  		
	}    
	                                                                                                                    
    CAN_LOG_DATA[58] = (uint8_t)(((eng_torq/10)>250)?250:(((eng_torq/10)<0)?0:(int8_t)(eng_torq/10)));       
                                                                                                                               
    CAN_LOG_DATA[59] = (uint8_t)(mtp);  		                                                                                                                                  
    CAN_LOG_DATA[60] = (uint8_t)(((alat/10)>127)?127:(((alat/10)<-127)?-127:(int8_t)(alat/10)));          
    CAN_LOG_DATA[61] = (uint8_t)(wstr);        
    CAN_LOG_DATA[62] = (uint8_t)(wstr>>8);
    CAN_LOG_DATA[63] = (uint8_t)(((yaw_out/10)>127)?127:(((yaw_out/10)<-127)?-127:(int8_t)(yaw_out/10))); 
    CAN_LOG_DATA[64] = (uint8_t)(((mpress/10)>127)?127:(((mpress/10)<-127)?-127:(int8_t)(mpress/10)));    
    	
    CAN_LOG_DATA[65] = (uint8_t)(ldddss16DctFLRRDefCnt);
    CAN_LOG_DATA[66] = (uint8_t)(ldddss16DctFLRRDefCnt>>8);
    CAN_LOG_DATA[67] = (uint8_t)(ldddss16DctFRRLDefCnt);
    CAN_LOG_DATA[68] = (uint8_t)(ldddss16DctFRRLDefCnt>>8);
        
    CAN_LOG_DATA[69] = (uint8_t)ldu8DDSModeEepromValue;//(DDS_flags);    
                 
    i=0;                                  
    if (ABS_fz                          ==1) i|=0x01; 
    if (BRAKE_SIGNAL                    ==1) i|=0x02; 
    if (BTCS_ON                         ==1) i|=0x04; 
    if (ETCS_ON                         ==1) i|=0x08; 
	if (ESP_TCS_ON                      ==1) i|=0x10;
	if (YAW_CDC_WORK                    ==1) i|=0x20; 
	if (VDC_REF_WORK                    ==1) i|=0x40; 
	if (ESP_ERROR_FLG                   ==1) i|=0x80; 

	CAN_LOG_DATA[70] = i;
	
	i=0; 
	
	#if __4WD||(__4WD_VARIANT_CODE==ENABLE)
		#if (__4WD_VARIANT_CODE==ENABLE)
	if(lsu8DrvMode !=DM_2WD)
	{
		#endif
    if (ESP_SENSOR_RELIABLE_FLG            	==1) i|=0x01;         
    if (USE_ALONG                       	==1) i|=0x02;
    if (SPIN_CALC_FLG                		==1) i|=0x04;
    if (BACK_DIR             				==1) i|=0x08;
    if (BIG_ACCEL_SPIN_FZ       			==1) i|=0x10; 
    if (ACCEL_SPIN_FZ                      	==1) i|=0x20; 
    if (BACKWARD_MOVE						==1) i|=0x40; 
    	#if (__4WD_VARIANT_CODE==ENABLE)
	}
		#endif       
	#endif
	#if !__4WD ||(__4WD_VARIANT_CODE==ENABLE)
		#if (__4WD_VARIANT_CODE==ENABLE)
	else
	{
		#endif
	#if defined ( __H2DCL)
	if (ctu1DDS											==1) i|=0x01; 
	#elif (__DDS_RESET_BY_ESC_SW == ENABLE)
    if (fu1DdsSwitchDriverIntent                         ==1) i|=0x01; 
    #else
    if (0                         ==1) i|=0x01;
    #endif      
    if (Rough_road_detect_vehicle         				==1) i|=0x02;
    if (ldddsu1RunningAvgUpdateFlag                		==1) i|=0x04;
    if (ESP_SENSOR_RELIABLE_FLG           				==1) i|=0x08;
    if (BLS    											==1) i|=0x10; 
    if (EBD_RA                  ==1) i|=0x20;       //
    if (BACK_DIR										==1) i|=0x40;          		
    	#if (__4WD_VARIANT_CODE==ENABLE)
	}
		#endif		
	#endif
                                             
    if (BRAKING_STATE         							==1) i|=0x80;      
    CAN_LOG_DATA[71] = i;               

    i = 0;                                   
    if (ABS_fl                          ==1) i|=0x01;
    if (ABS_fr                       	==1) i|=0x02;
    if (ABS_rl                       	==1) i|=0x04;
	if (ABS_rr                      	==1) i|=0x08; 
    if (ldddsu8Other3whlDeflation[FL_WL]                    ==1) i|=0x10;        //STABIL_fl
    if (ldddsu8Other3whlDeflation[FR_WL]     				==1) i|=0x20;		////STABIL_fl
    if (ldddsu8Other3whlDeflation[RL_WL]                    ==1) i|=0x40;      //STABIL_fl
    if (ldddsu8Other3whlDeflation[RR_WL]                    ==1) i|=0x80;       //STABIL_fl
    CAN_LOG_DATA[72] = i;      
    
    i=0;                                          
    if (ldddsu8CompleteCalib[0]                   	==1) i|=0x01;                      //HV_VL_fl                 
    if (ldddsu8CompleteCalib[1]                 	==1) i|=0x02;                      //AV_VL_fl                 
    if (ldddsu8CompleteCalib[2]                		==1) i|=0x04;                      //HV_VL_fr                 
	if (ldddsu8CompleteCalib[3]               		==1) i|=0x08;                      //AV_VL_fr                 
    if (ldddsu8CompleteCalib[4]               		==1) i|=0x10;                      //HV_VL_rl                 
	if (AV_VL_rl                                     		==1) i|=0x20;                             //
    if (HV_VL_rr                                           ==1) i|=0x40;          //
    if (AV_VL_rr                                           ==1) i|=0x80;          //
    CAN_LOG_DATA[73] = i;  
   
     i = 0;
     if (ldddsu1ElapsedTimeUpdateFlgOldFL  		==1) i|=0x01;   
     if (ldddsu1ElapsedTimeUpdateFlgOldFR		==1) i|=0x02;      
     if (ldddsu1ElapsedTimeUpdateFlgOldRL		==1) i|=0x04;      
     if (ldddsu1ElapsedTimeUpdateFlgOldRR		==1) i|=0x08;      
     if (ldddsu8ElapsedTimeUpdateFlgFL_K 		==1) i|=0x10;      
     if (ldddsu8ElapsedTimeUpdateFlgFR_K 		==1) i|=0x20;      
     if (ldddsu8ElapsedTimeUpdateFlgRL_K   		==1) i|=0x40;    
     if (ldddsu8ElapsedTimeUpdateFlgRR_K   		==1) i|=0x80;    
     
//
//    if (FL.UNRELIABLE_AT_ABS_ENTRY                           	==1) i|=0x01;                        
//    if (FR.UNRELIABLE_AT_ABS_ENTRY                        	==1) i|=0x02;                         
//    if (RL.UNRELIABLE_AT_ABS_ENTRY                        	==1) i|=0x04;  
//    if (RR.UNRELIABLE_AT_ABS_ENTRY                   		==1) i|=0x08;  
//    if (FL.UNRELIABLE_AT_BRAKE_ENTRY                  		==1) i|=0x10;           
//    if (FR.UNRELIABLE_AT_BRAKE_ENTRY     					==1) i|=0x20;  
//    if (RL.UNRELIABLE_AT_BRAKE_ENTRY                         ==1) i|=0x40;       
//    if (RR.UNRELIABLE_AT_BRAKE_ENTRY                         ==1) i|=0x80;    
//    	                 
    CAN_LOG_DATA[74] = i;  
                           
     i = 0;

    if (0                         	==1) i|=0x01;//FL.BUILT // 
    if (0                      	==1) i|=0x02;    //FR.BUILT // 
    if (0                      	==1) i|=0x04;    //RL.BUILT // 
    if (0                 		==1) i|=0x08;    //RR.BUILT //    
    if (ldddsu1ElapsedTimeUpdateFlgFL                		==1) i|=0x10;  //SPOLD_RESET         
    if (ldddsu1ElapsedTimeUpdateFlgFR     					==1) i|=0x20;  //SPOLD_RESET  
    if (ldddsu1ElapsedTimeUpdateFlgRL                        ==1) i|=0x40; //SPOLD_RESET      
    if (ldddsu1ElapsedTimeUpdateFlgRR                        ==1) i|=0x80; //SPOLD_RESET       	
    CAN_LOG_DATA[75] = i;                                                   

    i = 0;
    if (FL.RELIABLE_FOR_VREF                           	==1) i|=0x01;              
    if (FR.RELIABLE_FOR_VREF                        	==1) i|=0x02;              
    if (RL.RELIABLE_FOR_VREF                        	==1) i|=0x04;              
    if (RR.RELIABLE_FOR_VREF                   		==1) i|=0x08;                  
    if (ldddsu1CompleteCalibAllSpeed                  		==1) i|=0x10;                        
    if (ldddsu1ElapsedTimeUpdateFlgVeh     					==1) i|=0x20;   //WHL_SELECT_FLG 
#if !__RTA_2WHEEL_DCT                    
    if (RL.WHL_SELECT_FLG                         ==1) i|=0x40;                    
    if (RR.WHL_SELECT_FLG                         ==1) i|=0x80;                    
#else
	if (ldrtau1GoodCondForRta2WhlDct                         ==1) i|=0x40;
	if (ldrtau1GoodCondForRta2WhlClear                         ==1) i|=0x80;
#endif		
    CAN_LOG_DATA[76] = i;  

    i = 0;          

#if __RTA_2WHEEL_DCT
    if (RTA_FL.RTA_SUSPECT_2WHL                        	==1) i|=0x01;
    if (RTA_FR.RTA_SUSPECT_2WHL                        	==1) i|=0x02; 
    if (RTA_RL.RTA_SUSPECT_2WHL                        	==1) i|=0x04;  
    if (RTA_RR.RTA_SUSPECT_2WHL                   		==1) i|=0x08;   
#else 
    if (ESP_BRAKE_CONTROL_FL                        	==1) i|=0x01;
    if (ESP_BRAKE_CONTROL_FR                        	==1) i|=0x02; 
    if (ESP_BRAKE_CONTROL_RL                        	==1) i|=0x04;  
    if (ESP_BRAKE_CONTROL_RR                   		==1) i|=0x08;   
#endif    	
    if (ldddsu8ElapsedTimeUpdateFlg[0]                 	==1) i|=0x10;                   //ESP_BRAKE_CONTROL_FL
    if (ldddsu8ElapsedTimeUpdateFlg[1]    				==1) i|=0x20;                   //ESP_BRAKE_CONTROL_FR
    if (ldddsu8ElapsedTimeUpdateFlg[2]                  ==1) i|=0x40;                   //ESP_BRAKE_CONTROL_RL
    if (ldddsu8ElapsedTimeUpdateFlg[3]                  ==1) i|=0x80;                   //ESP_BRAKE_CONTROL_RR
    
    CAN_LOG_DATA[77] = i;                 

    i = 0;

    if (RTA_FL.RTA_SUSPECT_WL                           ==1) i|=0x01;
    if (RTA_FR.RTA_SUSPECT_WL                        	==1) i|=0x02;                    
    if (RTA_RL.RTA_SUSPECT_WL                        	==1) i|=0x04;                   
    if (RTA_RR.RTA_SUSPECT_WL                  			==1) i|=0x08;                        

    if (ldddsu1Diag2whlDeflationFLRR                  	==1) i|=0x10;
    if (ldddsu1Diag2whlDeflationFRRL     				==1) i|=0x20;  
    if (ldddsu1TireWearInCompleted                      ==1) i|=0x40;    //FL.FSF   
    if (ldddsu1PerformedDDSReset                        ==1) i|=0x80;    //FR.FSF         
                  
    CAN_LOG_DATA[78] = i;    

    i = 0;

	if (ldddsu1DeflationState        		==1) i|=0x01;       
	if (ldddsu1StdStrDrvStateForDDS  		==1) i|=0x02;       
	if (ldddsu1CompleteCalib         		==1) i|=0x04;
	if (ldddsu8SingleWhlDeflation[FL_WL]     	==1) i|=0x08;                         
	if (ldddsu8SingleWhlDeflation[FR_WL]     	==1) i|=0x10;                       
	if (ldddsu8SingleWhlDeflation[RL_WL]     	==1) i|=0x20;                     
	if (ldddsu8SingleWhlDeflation[RR_WL]     	==1) i|=0x40;                       
	if (ldddsu1RequestDDSReset       		==1) i|=0x80;                       
                               
    CAN_LOG_DATA[79] = i;    
        
  //===================================================================// 
  //                  Extended Data 80 ~ 159                           //
  //===================================================================//  
  
#if defined  __EXTEND_LOGGER
	CAN_LOG_DATA[80] = (uint8_t)(((ldddss16CumulativeMean[FL_WL][v_inx2])>127)?127:(((ldddss16CumulativeMean[FL_WL][v_inx2])<-127)?-127:(int8_t)(ldddss16CumulativeMean[FL_WL][v_inx2])));
	CAN_LOG_DATA[81] = (uint8_t)(((ldddss16CumulativeMean[FR_WL][v_inx2])>127)?127:(((ldddss16CumulativeMean[FR_WL][v_inx2])<-127)?-127:(int8_t)(ldddss16CumulativeMean[FR_WL][v_inx2])));
	CAN_LOG_DATA[82] = (uint8_t)(((ldddss16CumulativeMean[RL_WL][v_inx2])>127)?127:(((ldddss16CumulativeMean[RL_WL][v_inx2])<-127)?-127:(int8_t)(ldddss16CumulativeMean[RL_WL][v_inx2])));
	CAN_LOG_DATA[83] = (uint8_t)(((ldddss16CumulativeMean[RR_WL][v_inx2])>127)?127:(((ldddss16CumulativeMean[RR_WL][v_inx2])<-127)?-127:(int8_t)(ldddss16CumulativeMean[RR_WL][v_inx2])));
	CAN_LOG_DATA[84] = (uint8_t)(ldddsu16CntCalibTotalRev[v_inx2]);   
	CAN_LOG_DATA[85] = (uint8_t)(ldddsu16CntCalibTotalRev[v_inx2]>>8);
	CAN_LOG_DATA[86] = (uint8_t)(ldddsu32SumDctInput[FL_WL][v_inx]/20);      
	CAN_LOG_DATA[87] = (uint8_t)((ldddsu32SumDctInput[FL_WL][v_inx]/20)>>8);   
	CAN_LOG_DATA[88] = (uint8_t)(ldddsu32SumDctInput[FR_WL][v_inx]/20);      
	CAN_LOG_DATA[89] = (uint8_t)((ldddsu32SumDctInput[FR_WL][v_inx]/20)>>8);   
	CAN_LOG_DATA[90] = (uint8_t)(ldddsu32SumDctInput[RL_WL][v_inx]/20);      
	CAN_LOG_DATA[91] = (uint8_t)((ldddsu32SumDctInput[RL_WL][v_inx]/20)>>8);   
	CAN_LOG_DATA[92] = (uint8_t)(ldddsu32SumDctInput[RR_WL][v_inx]/20);      
	CAN_LOG_DATA[93] = (uint8_t)((ldddsu32SumDctInput[RR_WL][v_inx]/20)>>8);   
	CAN_LOG_DATA[94] = (uint8_t)(((ldddss16AxleDiffRunningAvg[FR_WL][1]/10)>127)?127:(((ldddss16AxleDiffRunningAvg[FR_WL][1]/10)<-127)?-127:(int8_t)(ldddss16AxleDiffRunningAvg[FR_WL][1]/10)));
	CAN_LOG_DATA[95] = (uint8_t)(((ldddss16SideDiffRunningAvg[FR_WL][1]/10)>127)?127:(((ldddss16SideDiffRunningAvg[FR_WL][1]/10)<-127)?-127:(int8_t)(ldddss16SideDiffRunningAvg[FR_WL][1]/10)));
	CAN_LOG_DATA[96] = (uint8_t)(((ldddss16DiagDiffRunningAvg[FR_WL][1]/10)>127)?127:(((ldddss16DiagDiffRunningAvg[FR_WL][1]/10)<-127)?-127:(int8_t)(ldddss16DiagDiffRunningAvg[FR_WL][1]/10)));
	CAN_LOG_DATA[97] = (uint8_t)(((ldddss16AxleDiffRunningAvg[RL_WL][1]/10)>127)?127:(((ldddss16AxleDiffRunningAvg[RL_WL][1]/10)<-127)?-127:(int8_t)(ldddss16AxleDiffRunningAvg[RL_WL][1]/10)));
	CAN_LOG_DATA[98] = (uint8_t)(((ldddss16SideDiffRunningAvg[RL_WL][1]/10)>127)?127:(((ldddss16SideDiffRunningAvg[RL_WL][1]/10)<-127)?-127:(int8_t)(ldddss16SideDiffRunningAvg[RL_WL][1]/10)));
	CAN_LOG_DATA[99] = (uint8_t)(((ldddss16DiagDiffRunningAvg[FL_WL][1]/10)>127)?127:(((ldddss16DiagDiffRunningAvg[FL_WL][1]/10)<-127)?-127:(int8_t)(ldddss16DiagDiffRunningAvg[FL_WL][1]/10)));
	CAN_LOG_DATA[100] = (uint8_t)(ldddss8TireTempFromTPMS[FL_WL]);    
	CAN_LOG_DATA[101] = (uint8_t)(ldddss8TireTempFromTPMS[FR_WL]);    
	CAN_LOG_DATA[102] = (uint8_t)(ldddss8TireTempFromTPMS[RL_WL]);    
	CAN_LOG_DATA[103] = (uint8_t)(ldddss8TireTempFromTPMS[RR_WL]);    
	CAN_LOG_DATA[104] = (uint8_t)(ldddss8TirePressueFromTPMS[FL_WL]); 
	CAN_LOG_DATA[105] = (uint8_t)(ldddss8TirePressueFromTPMS[FR_WL]); 
	CAN_LOG_DATA[106] = (uint8_t)(ldddss8TirePressueFromTPMS[RL_WL]); 
	CAN_LOG_DATA[107] = (uint8_t)(ldddss8TirePressueFromTPMS[RR_WL]); 
	CAN_LOG_DATA[108] = (uint8_t)(ldddss8TPMSWheelIndex);             
	CAN_LOG_DATA[109] = (uint8_t)(DiagDiffMinMax[FL_WL]); 
	CAN_LOG_DATA[110] = (uint8_t)(DiagDiffMinMax[FR_WL]); 
	CAN_LOG_DATA[111] = (uint8_t)(AxleDiffMinMax[FL_WL]); 
	CAN_LOG_DATA[112] = (uint8_t)(AxleDiffMinMax[RL_WL]);
	CAN_LOG_DATA[113] = (uint8_t)(SideDiffMinMax[FL_WL]);
	CAN_LOG_DATA[114] = (uint8_t)(SideDiffMinMax[FR_WL]);
	CAN_LOG_DATA[115] = (uint8_t)((ldddsu32SumDiffForCalib[FL_WL][v_inx1]/20));
	CAN_LOG_DATA[116] = (uint8_t)((ldddsu32SumDiffForCalib[FL_WL][v_inx1]/20)>>8);
	CAN_LOG_DATA[117] = (uint8_t)((ldddsu32SumDiffForCalib[FR_WL][v_inx1]/20)); 
	CAN_LOG_DATA[118] = (uint8_t)((ldddsu32SumDiffForCalib[FR_WL][v_inx1]/20)>>8); 
	CAN_LOG_DATA[119] = (uint8_t)((ldddsu32SumDiffForCalib[RL_WL][v_inx1]/20)); 
	CAN_LOG_DATA[120] = (uint8_t)((ldddsu32SumDiffForCalib[RL_WL][v_inx1]/20)>>8); 
	CAN_LOG_DATA[121] = (uint8_t)((ldddsu32SumDiffForCalib[RR_WL][v_inx1]/20)); 
	CAN_LOG_DATA[122] = (uint8_t)((ldddsu32SumDiffForCalib[RR_WL][v_inx1]/20)>>8);  
	CAN_LOG_DATA[123] = (uint8_t)(ldddss16DDSCalibTemp[FL_WL]);//((ldddsu32SumDiffForCalib[FL_WL][v_inx2])); 
	CAN_LOG_DATA[124] = (uint8_t)(ldddss16DDSCalibTemp[FL_WL]>>8);//((ldddsu32SumDiffForCalib[FL_WL][v_inx2]/20)>>8); 
	CAN_LOG_DATA[125] = (uint8_t)(ldddss16DDSCalibTemp[FR_WL]);//((ldddsu32SumDiffForCalib[FR_WL][v_inx2]));
	CAN_LOG_DATA[126] = (uint8_t)(ldddss16DDSCalibTemp[FR_WL]>>8);//((ldddsu32SumDiffForCalib[FR_WL][v_inx2]/20)>>8);
	CAN_LOG_DATA[127] = (uint8_t)(ldddss16DDSCalibTemp[RL_WL]);//((ldddsu32SumDiffForCalib[RL_WL][v_inx2]));
	CAN_LOG_DATA[128] = (uint8_t)(ldddss16DDSCalibTemp[RL_WL]>>8);//((ldddsu32SumDiffForCalib[RL_WL][v_inx2]/20)>>8); 
	CAN_LOG_DATA[129] = (uint8_t)(ldddss16DDSCalibTemp[RR_WL]);//((ldddsu32SumDiffForCalib[RR_WL][v_inx2]));
	CAN_LOG_DATA[130] = (uint8_t)(ldddss16DDSCalibTemp[RR_WL]>>8);//((ldddsu32SumDiffForCalib[RR_WL][v_inx2]/20)>>8);
	CAN_LOG_DATA[131] = (uint8_t)(ldddss16Dct1WhlDefCnt[FL_WL][0]);         
	CAN_LOG_DATA[132] = (uint8_t)(ldddss16Dct1WhlDefCnt[FL_WL][1]);         
	CAN_LOG_DATA[133] = (uint8_t)(ldddss16Dct1WhlDefCnt[FL_WL][2]);         
	CAN_LOG_DATA[134] = (uint8_t)(ldddss16Dct1WhlDefCnt[FL_WL][3]);         
	CAN_LOG_DATA[135] = (uint8_t)(ldddss16Dct1WhlDefCnt[FL_WL][4]);         
	CAN_LOG_DATA[136] = (uint8_t)(ldddss16Dct1WhlDefCnt[RR_WL][0]);         
	CAN_LOG_DATA[137] = (uint8_t)(ldddss16Dct1WhlDefCnt[RR_WL][1]);         
	CAN_LOG_DATA[138] = (uint8_t)(ldddss16Dct1WhlDefCnt[RR_WL][2]);         
	CAN_LOG_DATA[139] = (uint8_t)(ldddss16Dct1WhlDefCnt[RR_WL][3]);         
	CAN_LOG_DATA[140] = (uint8_t)(ldddss16Dct1WhlDefCnt[RR_WL][4]);         
	CAN_LOG_DATA[141] = (uint8_t)(((ldddss16AxleDiffThr[0][0]/10)>127)?127:(((ldddss16AxleDiffThr[0][0]/10)<-127)?-127:(int8_t)(ldddss16AxleDiffThr[0][0]/10)));   
	CAN_LOG_DATA[142] = (uint8_t)(((ldddss16SideDiffThr[0][0]/10)>127)?127:(((ldddss16SideDiffThr[0][0]/10)<-127)?-127:(int8_t)(ldddss16SideDiffThr[0][0]/10)));   
	CAN_LOG_DATA[143] = (uint8_t)(((ldddss16DiagDiffThr[0][0]/10)>127)?127:(((ldddss16DiagDiffThr[0][0]/10)<-127)?-127:(int8_t)(ldddss16DiagDiffThr[0][0]/10)));   
	CAN_LOG_DATA[144] = (uint8_t)(((ldddss16AxleDiffThr[0][1]/10)>127)?127:(((ldddss16AxleDiffThr[0][1]/10)<-127)?-127:(int8_t)(ldddss16AxleDiffThr[0][1]/10)));   
	CAN_LOG_DATA[145] = (uint8_t)(((ldddss16SideDiffThr[0][1]/10)>127)?127:(((ldddss16SideDiffThr[0][1]/10)<-127)?-127:(int8_t)(ldddss16SideDiffThr[0][1]/10)));   
	CAN_LOG_DATA[146] = (uint8_t)(((ldddss16DiagDiffThr[0][1]/10)>127)?127:(((ldddss16DiagDiffThr[0][1]/10)<-127)?-127:(int8_t)(ldddss16DiagDiffThr[0][1]/10)));   
	CAN_LOG_DATA[147] = (uint8_t)(((ldddss16AxleDiffThr[1][0]/10)>127)?127:(((ldddss16AxleDiffThr[1][0]/10)<-127)?-127:(int8_t)(ldddss16AxleDiffThr[1][0]/10)));
	CAN_LOG_DATA[148] = (uint8_t)(((ldddss16SideDiffThr[1][0]/10)>127)?127:(((ldddss16SideDiffThr[1][0]/10)<-127)?-127:(int8_t)(ldddss16SideDiffThr[1][0]/10)));
	CAN_LOG_DATA[149] = (uint8_t)(((ldddss16DiagDiffThr[1][0]/10)>127)?127:(((ldddss16DiagDiffThr[1][0]/10)<-127)?-127:(int8_t)(ldddss16DiagDiffThr[1][0]/10)));
	CAN_LOG_DATA[150] = (uint8_t)(((ldddss16AxleDiffThr[1][1]/10)>127)?127:(((ldddss16AxleDiffThr[1][1]/10)<-127)?-127:(int8_t)(ldddss16AxleDiffThr[1][1]/10)));
	CAN_LOG_DATA[151] = (uint8_t)(((ldddss16SideDiffThr[1][1]/10)>127)?127:(((ldddss16SideDiffThr[1][1]/10)<-127)?-127:(int8_t)(ldddss16SideDiffThr[1][1]/10)));
	CAN_LOG_DATA[152] = (uint8_t)(((ldddss16DiagDiffThr[1][1]/10)>127)?127:(((ldddss16DiagDiffThr[1][1]/10)<-127)?-127:(int8_t)(ldddss16DiagDiffThr[1][1]/10)));
	CAN_LOG_DATA[153] = (uint8_t)(((WheelDiffMinMax[FL_WL][v_inx1])>250)?250:(((WheelDiffMinMax[FL_WL][v_inx1]/10)<0)?0:(int8_t)(WheelDiffMinMax[FL_WL][v_inx1]/10)));  
	CAN_LOG_DATA[154] = (uint8_t)(((WheelDiffMinMax[FR_WL][v_inx1])>250)?250:(((WheelDiffMinMax[FR_WL][v_inx1]/10)<0)?0:(int8_t)(WheelDiffMinMax[FR_WL][v_inx1]/10)));
	CAN_LOG_DATA[155] = (uint8_t)(((WheelDiffMinMax[RL_WL][v_inx1])>250)?250:(((WheelDiffMinMax[RL_WL][v_inx1]/10)<0)?0:(int8_t)(WheelDiffMinMax[RL_WL][v_inx1]/10)));
	CAN_LOG_DATA[156] = (uint8_t)(((WheelDiffMinMax[RR_WL][v_inx1])>250)?250:(((WheelDiffMinMax[RR_WL][v_inx1]/10)<0)?0:(int8_t)(WheelDiffMinMax[RR_WL][v_inx1]/10)));
	CAN_LOG_DATA[157] = (uint8_t)(ldu8DDSModeOld);
	CAN_LOG_DATA[158] = (uint8_t)(ldu8DDSModeEepromValue);
	CAN_LOG_DATA[159] =	(uint8_t)(ldddss16TireWereInMileageLeft);   
	                            
	
#endif
	
}
//============================================================================================//
void	LSDDS_vCallDDSPublicRoadLogData(void)
{
	uint8_t i;
	
	CAN_LOG_DATA[0]  = (uint8_t)(system_loop_counter);    
	CAN_LOG_DATA[1]  = (uint8_t)(system_loop_counter>>8); 
	CAN_LOG_DATA[2]  = (uint8_t)(FL.vrad_resol_change);   
	CAN_LOG_DATA[3]  = (uint8_t)(FL.vrad_resol_change>>8);
	CAN_LOG_DATA[4]  = (uint8_t)(FR.vrad_resol_change);   
	CAN_LOG_DATA[5]  = (uint8_t)(FR.vrad_resol_change>>8);
	CAN_LOG_DATA[6]  = (uint8_t)(RL.vrad_resol_change);   
	CAN_LOG_DATA[7]  = (uint8_t)(RL.vrad_resol_change>>8);
	CAN_LOG_DATA[8]  = (uint8_t)(RR.vrad_resol_change);   
	CAN_LOG_DATA[9]  = (uint8_t)(RR.vrad_resol_change>>8);
	CAN_LOG_DATA[10] = (uint8_t)(vref_resol_change);      
	CAN_LOG_DATA[11] = (uint8_t)(vref_resol_change>>8);   
	CAN_LOG_DATA[12] = (uint8_t)(ldu8DDSMode);	        
	CAN_LOG_DATA[13] = (uint8_t)(ldu8DDSSubMode);         
	CAN_LOG_DATA[14] = (uint8_t)(ldddsu16ElapsedTimePerRev[FL_WL]);   
	CAN_LOG_DATA[15] = (uint8_t)(ldddsu16ElapsedTimePerRev[FL_WL]>>8);
	CAN_LOG_DATA[16] = (uint8_t)(ldddsu16ElapsedTimePerRev[FR_WL]);   
	CAN_LOG_DATA[17] = (uint8_t)(ldddsu16ElapsedTimePerRev[FR_WL]>>8);
	CAN_LOG_DATA[18] = (uint8_t)(ldddsu16ElapsedTimePerRev[RL_WL]);   
	CAN_LOG_DATA[19] = (uint8_t)(ldddsu16ElapsedTimePerRev[RL_WL]>>8);
	CAN_LOG_DATA[20] = (uint8_t)(ldddsu16ElapsedTimePerRev[RR_WL]);   
	CAN_LOG_DATA[21] = (uint8_t)(ldddsu16ElapsedTimePerRev[RR_WL]>>8);
	
	CAN_LOG_DATA[22] = (uint8_t)(((eng_torq_abs/10)>250)?250:(((eng_torq_abs/10)<0)?0:(int8_t)(eng_torq_abs/10)));       
	CAN_LOG_DATA[23] = (uint8_t)(((alat/10)>127)?127:(((alat/10)<-127)?-127:(int8_t)(alat/10)));                                                                 
	CAN_LOG_DATA[24] = (uint8_t)(((wstr/10)>127)?127:(((wstr/10)<-127)?-127:(int8_t)(wstr/10)));                                                              
	CAN_LOG_DATA[25] = (uint8_t)(((yaw_out/10)>127)?127:(((yaw_out/10)<-127)?-127:(int8_t)(yaw_out/10)));                
	CAN_LOG_DATA[26] = (uint8_t)(((mpress/10)>127)?127:(((mpress/10)<-127)?-127:(int8_t)(mpress/10)));    
	CAN_LOG_DATA[27] = (uint8_t)(mtp);	
	CAN_LOG_DATA[28] = (uint8_t)((CalibrationValueFL>127)?127:((CalibrationValueFL<-127)?-127:(int8_t)CalibrationValueFL));                                                                                                                                                                                                                                         
	CAN_LOG_DATA[29] = (uint8_t)((CalibrationValueFR>127)?127:((CalibrationValueFR<-127)?-127:(int8_t)CalibrationValueFR));                                                                                                                                                                                                                                         
	CAN_LOG_DATA[30] = (uint8_t)((CalibrationValueRL>127)?127:((CalibrationValueRL<-127)?-127:(int8_t)CalibrationValueRL));                                                                                                                                                                                                                                         
	CAN_LOG_DATA[31] = (uint8_t)((CalibrationValueRR>127)?127:((CalibrationValueRR<-127)?-127:(int8_t)CalibrationValueRR));                                                                                                                                                                                                                                         

			
 /* Logging Variables for Calibration */   
if(ldddsu8CompleteCalib[v_inx1] ==0)
{
	CAN_LOG_DATA[32] = (uint8_t)((ldddsu32SumDiffForCalib[FL_WL][v_inx1]/20));    
	CAN_LOG_DATA[33] = (uint8_t)((ldddsu32SumDiffForCalib[FL_WL][v_inx1]/20)>>8); 
	CAN_LOG_DATA[34] = (uint8_t)((ldddsu32SumDiffForCalib[FR_WL][v_inx1]/20));    
	CAN_LOG_DATA[35] = (uint8_t)((ldddsu32SumDiffForCalib[FR_WL][v_inx1]/20)>>8); 
	CAN_LOG_DATA[36] = (uint8_t)((ldddsu32SumDiffForCalib[RL_WL][v_inx1]/20));    
	CAN_LOG_DATA[37] = (uint8_t)((ldddsu32SumDiffForCalib[RL_WL][v_inx1]/20)>>8); 
	CAN_LOG_DATA[38] = (uint8_t)((ldddsu32SumDiffForCalib[RR_WL][v_inx1]/20));    
	CAN_LOG_DATA[39] = (uint8_t)((ldddsu32SumDiffForCalib[RR_WL][v_inx1]/20)>>8);  	
	CAN_LOG_DATA[40] = (uint8_t)(ldddsu16CntCalibTotalRev[v_inx1]);                                                                                                                                                                                                                                                                                                     
	CAN_LOG_DATA[41] = (uint8_t)(ldddsu16CntCalibTotalRev[v_inx1]>>8);	  
	CAN_LOG_DATA[42] = (uint8_t)(ldddsu16CntCalibTotalRev[v_inx2]);                                                                                                                                                                                        
	CAN_LOG_DATA[43] = (uint8_t)(ldddsu16CntCalibTotalRev[v_inx2]>>8);
	
	CAN_LOG_DATA[44] = (uint8_t)(ldu8DDSModeOld);                                                                                                                                                                                                                                                  
	CAN_LOG_DATA[45] = (uint8_t)(ldu8DDSModeEepromValue);  
	CAN_LOG_DATA[46] = (uint8_t)(((ldddss16CumulativeMean[FL_WL][v_inx1])>127)?127:(((ldddss16CumulativeMean[FL_WL][v_inx1])<-127)?-127:(int8_t)(ldddss16CumulativeMean[FL_WL][v_inx1])));                                                                                                                                                                                
	CAN_LOG_DATA[47] = (uint8_t)(((ldddss16CumulativeMean[FR_WL][v_inx1])>127)?127:(((ldddss16CumulativeMean[FR_WL][v_inx1])<-127)?-127:(int8_t)(ldddss16CumulativeMean[FR_WL][v_inx1])));                                                                                                                                                                                
	CAN_LOG_DATA[48] = (uint8_t)(((ldddss16CumulativeMean[RL_WL][v_inx1])>127)?127:(((ldddss16CumulativeMean[RL_WL][v_inx1])<-127)?-127:(int8_t)(ldddss16CumulativeMean[RL_WL][v_inx1])));                                                                                                                                                                                
	CAN_LOG_DATA[49] = (uint8_t)(((ldddss16CumulativeMean[RR_WL][v_inx1])>127)?127:(((ldddss16CumulativeMean[RR_WL][v_inx1])<-127)?-127:(int8_t)(ldddss16CumulativeMean[RR_WL][v_inx1])));                                                                                                                                                                                
 	CAN_LOG_DATA[50] = (uint8_t)((ldddss16CumulativeMean[FL_WL][v_inx2]>127)?127:((ldddss16CumulativeMean[FL_WL][v_inx2]<-127)?-127:(int8_t)ldddss16CumulativeMean[FL_WL][v_inx2]));                                                                  
	CAN_LOG_DATA[51] = (uint8_t)((ldddss16CumulativeMean[FR_WL][v_inx2]>127)?127:((ldddss16CumulativeMean[FR_WL][v_inx2]<-127)?-127:(int8_t)ldddss16CumulativeMean[FR_WL][v_inx2]));                                                                  
	CAN_LOG_DATA[52] = (uint8_t)((ldddss16CumulativeMean[RL_WL][v_inx2]>127)?127:((ldddss16CumulativeMean[RL_WL][v_inx2]<-127)?-127:(int8_t)ldddss16CumulativeMean[RL_WL][v_inx2]));                                                                   
	CAN_LOG_DATA[53] = (uint8_t)((ldddss16CumulativeMean[RR_WL][v_inx2]>127)?127:((ldddss16CumulativeMean[RR_WL][v_inx2]<-127)?-127:(int8_t)ldddss16CumulativeMean[RR_WL][v_inx2]));                                                                   

 					
	CAN_LOG_DATA[54] = (uint8_t)(ldddss16DDSCalibTemp[FL_WL]);   
	CAN_LOG_DATA[55] = (uint8_t)(ldddss16DDSCalibTemp[FL_WL]>>8); 
	CAN_LOG_DATA[56] = (uint8_t)(ldddss16DDSCalibTemp[FR_WL]);   
	CAN_LOG_DATA[57] = (uint8_t)(ldddss16DDSCalibTemp[FR_WL]>>8); 
	CAN_LOG_DATA[58] = (uint8_t)(ldddss16DDSCalibTemp[RL_WL]);   
	CAN_LOG_DATA[59] = (uint8_t)(ldddss16DDSCalibTemp[RL_WL]>>8); 
	CAN_LOG_DATA[60] = (uint8_t)(ldddss16DDSCalibTemp[RR_WL]);   
	CAN_LOG_DATA[61] = (uint8_t)(ldddss16DDSCalibTemp[RR_WL]>>8); 
                                                                                                                                                                                                                                                                                           
	CAN_LOG_DATA[62] = (uint8_t)(WheelDiffMinMax[FL_WL][v_inx1]);                                                                                                                                                                                                                
	CAN_LOG_DATA[63] = (uint8_t)(WheelDiffMinMax[FL_WL][v_inx1]>>8);                                                                                                                                                                                                                
	CAN_LOG_DATA[64] = (uint8_t)(WheelDiffMinMax[FR_WL][v_inx1]);                                                                                                                                                                                                               
	CAN_LOG_DATA[65] = (uint8_t)(WheelDiffMinMax[FR_WL][v_inx1]>>8);                                                                                                                                                                                                              
	CAN_LOG_DATA[66] = (uint8_t)(WheelDiffMinMax[RL_WL][v_inx1]);
	CAN_LOG_DATA[67] = (uint8_t)(WheelDiffMinMax[RL_WL][v_inx1]>>8);
	CAN_LOG_DATA[68] = (uint8_t)(WheelDiffMinMax[RR_WL][v_inx1]);
	CAN_LOG_DATA[69] = (uint8_t)(WheelDiffMinMax[RR_WL][v_inx1]>>8);                                         	                                                                                                                                                                                	                                                       
}
 /* Logging Variables for Detection */   
else
{		

	CAN_LOG_DATA[32] = (uint8_t)(ldddsu32SumDctInput[FL_WL][v_inx]/20);                                                                                                                        
	CAN_LOG_DATA[33] = (uint8_t)((ldddsu32SumDctInput[FL_WL][v_inx]/20)>>8);                                                                                                                   
	CAN_LOG_DATA[34] = (uint8_t)(ldddsu32SumDctInput[FR_WL][v_inx]/20);                                                                                                                        
	CAN_LOG_DATA[35] = (uint8_t)((ldddsu32SumDctInput[FR_WL][v_inx]/20)>>8);                                                                                                                   
	CAN_LOG_DATA[36] = (uint8_t)(ldddsu32SumDctInput[RL_WL][v_inx]/20);                                                                                                                        
	CAN_LOG_DATA[37] = (uint8_t)((ldddsu32SumDctInput[RL_WL][v_inx]/20)>>8);                                                                                                                   
	CAN_LOG_DATA[38] = (uint8_t)(ldddsu32SumDctInput[RR_WL][v_inx]/20);                                                                                                                        
	CAN_LOG_DATA[39] = (uint8_t)((ldddsu32SumDctInput[RR_WL][v_inx]/20)>>8);    
	CAN_LOG_DATA[40] = (uint8_t)(ldddss16DctFLRRDefCnt);    
	CAN_LOG_DATA[41] = (uint8_t)(ldddss16DctFLRRDefCnt>>8); 
	CAN_LOG_DATA[42] = (uint8_t)(ldddss16DctFRRLDefCnt);    
	CAN_LOG_DATA[43] = (uint8_t)(ldddss16DctFRRLDefCnt>>8); 	

	CAN_LOG_DATA[44] = (uint8_t)((ldddss16Dct1WhlDefCnt[FR_WL][0]>250)?250:((ldddss16Dct1WhlDefCnt[FR_WL][0]<0)?0:(uint8_t)ldddss16Dct1WhlDefCnt[FR_WL][0])); 
	CAN_LOG_DATA[45] = (uint8_t)((ldddss16Dct1WhlDefCnt[FR_WL][1]>250)?250:((ldddss16Dct1WhlDefCnt[FR_WL][1]<0)?0:(uint8_t)ldddss16Dct1WhlDefCnt[FR_WL][1]));  	
	CAN_LOG_DATA[46] = (uint8_t)((ldddss16Dct1WhlDefCnt[FR_WL][2]>250)?250:((ldddss16Dct1WhlDefCnt[FR_WL][2]<0)?0:(uint8_t)ldddss16Dct1WhlDefCnt[FR_WL][2]));                         
	CAN_LOG_DATA[47] = (uint8_t)((ldddss16Dct1WhlDefCnt[FR_WL][3]>250)?250:((ldddss16Dct1WhlDefCnt[FR_WL][3]<0)?0:(uint8_t)ldddss16Dct1WhlDefCnt[FR_WL][3]));       					                           
	CAN_LOG_DATA[48] = (uint8_t)((ldddss16Dct1WhlDefCnt[FR_WL][4]>250)?250:((ldddss16Dct1WhlDefCnt[FR_WL][4]<0)?0:(uint8_t)ldddss16Dct1WhlDefCnt[FR_WL][4]));   					 
	CAN_LOG_DATA[49] = (uint8_t)((ldddss16Dct1WhlDefCnt[RL_WL][0]>250)?250:((ldddss16Dct1WhlDefCnt[RL_WL][0]<0)?0:(uint8_t)ldddss16Dct1WhlDefCnt[RL_WL][0])); 	
	CAN_LOG_DATA[50] = (uint8_t)((ldddss16Dct1WhlDefCnt[RL_WL][1]>250)?250:((ldddss16Dct1WhlDefCnt[RL_WL][1]<0)?0:(uint8_t)ldddss16Dct1WhlDefCnt[RL_WL][1])); 
	CAN_LOG_DATA[51] = (uint8_t)((ldddss16Dct1WhlDefCnt[RL_WL][2]>250)?250:((ldddss16Dct1WhlDefCnt[RL_WL][2]<0)?0:(uint8_t)ldddss16Dct1WhlDefCnt[RL_WL][2])); 
	CAN_LOG_DATA[52] = (uint8_t)((ldddss16Dct1WhlDefCnt[RL_WL][3]>250)?250:((ldddss16Dct1WhlDefCnt[RL_WL][3]<0)?0:(uint8_t)ldddss16Dct1WhlDefCnt[RL_WL][3])); 
	CAN_LOG_DATA[53] = (uint8_t)((ldddss16Dct1WhlDefCnt[RL_WL][4]>250)?250:((ldddss16Dct1WhlDefCnt[RL_WL][4]<0)?0:(uint8_t)ldddss16Dct1WhlDefCnt[RL_WL][4])); 

	
	CAN_LOG_DATA[54] = (uint8_t)(((ldddss16AxleDiffRunningAvg[FR_WL][v_inx]/10)>127)?127:(((ldddss16AxleDiffRunningAvg[FR_WL][v_inx]/10)<-127)?-127:(int8_t)(ldddss16AxleDiffRunningAvg[FR_WL][v_inx]/10))); 
	CAN_LOG_DATA[55] = (uint8_t)(((ldddss16SideDiffRunningAvg[FR_WL][v_inx]/10)>127)?127:(((ldddss16SideDiffRunningAvg[FR_WL][v_inx]/10)<-127)?-127:(int8_t)(ldddss16SideDiffRunningAvg[FR_WL][v_inx]/10))); 
	CAN_LOG_DATA[56] = (uint8_t)(((ldddss16DiagDiffRunningAvg[FR_WL][v_inx]/10)>127)?127:(((ldddss16DiagDiffRunningAvg[FR_WL][v_inx]/10)<-127)?-127:(int8_t)(ldddss16DiagDiffRunningAvg[FR_WL][v_inx]/10))); 
	CAN_LOG_DATA[57] = (uint8_t)(((ldddss16AxleDiffRunningAvg[RL_WL][v_inx]/10)>127)?127:(((ldddss16AxleDiffRunningAvg[RL_WL][v_inx]/10)<-127)?-127:(int8_t)(ldddss16AxleDiffRunningAvg[RL_WL][v_inx]/10))); 
	CAN_LOG_DATA[58] = (uint8_t)(((ldddss16SideDiffRunningAvg[RL_WL][v_inx]/10)>127)?127:(((ldddss16SideDiffRunningAvg[RL_WL][v_inx]/10)<-127)?-127:(int8_t)(ldddss16SideDiffRunningAvg[RL_WL][v_inx]/10))); 
	CAN_LOG_DATA[59] = (uint8_t)(((ldddss16DiagDiffRunningAvg[FL_WL][v_inx]/10)>127)?127:(((ldddss16DiagDiffRunningAvg[FL_WL][v_inx]/10)<-127)?-127:(int8_t)(ldddss16DiagDiffRunningAvg[FL_WL][v_inx]/10)));    
	CAN_LOG_DATA[60] = (uint8_t)((ldddss8PressEstByAxleDiff[FR_WL]>127)?127:((ldddss8PressEstByAxleDiff[FR_WL]<-127)?-127:(int8_t)ldddss8PressEstByAxleDiff[FR_WL]));                                                   
	CAN_LOG_DATA[61] = (uint8_t)((ldddss8PressEstBySideDiff[FR_WL]>127)?127:((ldddss8PressEstBySideDiff[FR_WL]<-127)?-127:(int8_t)ldddss8PressEstBySideDiff[FR_WL])); 
	CAN_LOG_DATA[62] = (uint8_t)((ldddss8PressEstByDiagDiff[FR_WL]>127)?127:((ldddss8PressEstByDiagDiff[FR_WL]<-127)?-127:(int8_t)ldddss8PressEstByDiagDiff[FR_WL]));                                                   
	CAN_LOG_DATA[63] = (uint8_t)((ldddss8PressEstByAxleDiff[RL_WL]>127)?127:((ldddss8PressEstByAxleDiff[RL_WL]<-127)?-127:(int8_t)ldddss8PressEstByAxleDiff[RL_WL])); 
	CAN_LOG_DATA[64] = (uint8_t)((ldddss8PressEstBySideDiff[RL_WL]>127)?127:((ldddss8PressEstBySideDiff[RL_WL]<-127)?-127:(int8_t)ldddss8PressEstBySideDiff[RL_WL])); 
	CAN_LOG_DATA[65] = (uint8_t)((ldddss8PressEstByDiagDiff[RL_WL]>127)?127:((ldddss8PressEstByDiagDiff[RL_WL]<-127)?-127:(int8_t)ldddss8PressEstByDiagDiff[RL_WL])); 
	CAN_LOG_DATA[66] = (uint8_t)((AxleDiffMinMax[FL_WL]>250)?250:((AxleDiffMinMax[FL_WL]<0)?0:(uint8_t)AxleDiffMinMax[FL_WL]));
	CAN_LOG_DATA[67] = (uint8_t)((AxleDiffMinMax[RL_WL]>250)?250:((AxleDiffMinMax[RL_WL]<0)?0:(uint8_t)AxleDiffMinMax[RL_WL]));
	CAN_LOG_DATA[68] = (uint8_t)((SideDiffMinMax[FL_WL]>250)?250:((SideDiffMinMax[FL_WL]<0)?0:(uint8_t)SideDiffMinMax[FL_WL]));
	CAN_LOG_DATA[69] = (uint8_t)((SideDiffMinMax[FR_WL]>250)?250:((SideDiffMinMax[FR_WL]<0)?0:(uint8_t)SideDiffMinMax[FR_WL]));

                                                            
}
    i=0;                                  
    if (ABS_fz                          ==1) i|=0x01; 
    if (BRAKE_SIGNAL                    ==1) i|=0x02; 
    if (BTCS_ON                         ==1) i|=0x04; 
    if (ETCS_ON                         ==1) i|=0x08; 
	if (ESP_TCS_ON                      ==1) i|=0x10;
	if (YAW_CDC_WORK                    ==1) i|=0x20; 
	if (BRAKING_STATE                    ==1) i|=0x40; 
	if (ESP_ERROR_FLG                   ==1) i|=0x80; 

	CAN_LOG_DATA[70] = i;
	
	i=0; 
	#if defined ( __H2DCL)
	if (ctu1DDS											==1) i|=0x01; 
	#elif (__DDS_RESET_BY_ESC_SW == ENABLE)
    if (fu1DdsSwitchDriverIntent                         ==1) i|=0x01; 
    #else
    if (0                         ==1) i|=0x01;
    #endif
    	
    if (Rough_road_detect_vehicle                    		==1) i|=0x02; 
    if (ldddsu1RunningAvgUpdateFlag                          ==1) i|=0x04; 
    
    #if __4WD||(__4WD_VARIANT_CODE==ENABLE)
    if (USE_ALONG                        	==1) i|=0x08; 
	if (SPIN_CALC_FLG                      	==1) i|=0x10;
	if (BIG_ACCEL_SPIN_FZ                   ==1) i|=0x20; 
	if (ACCEL_SPIN_FZ                    	==1) i|=0x40; 
	if (Rough_road_suspect_vehicle                   	==1) i|=0x80; 
	#else
    if (EBD_RA                        	==1) i|=0x08; 
	if (PARKING_BRAKE_OPERATION                       	==1) i|=0x10; 
	if (SAS_CHECK_OK                    ==1) i|=0x20; 
	if (RL.RELIABLE_FOR_VREF                     	==1) i|=0x40; 
	if (Rough_road_suspect_vehicle                    	==1) i|=0x80; 	
	#endif


    CAN_LOG_DATA[71] = i;               

    i = 0;                                   
    if (ABS_fl                          ==1) i|=0x01;
    if (ABS_fr                       	==1) i|=0x02;
    if (ABS_rl                       	==1) i|=0x04;
	if (ABS_rr                      	==1) i|=0x08; 
    if (ldddsu8Other3whlDeflation[FL_WL]                    ==1) i|=0x10;      
    if (ldddsu8Other3whlDeflation[FR_WL]     				==1) i|=0x20;		
    if (ldddsu8Other3whlDeflation[RL_WL]                    ==1) i|=0x40;      
    if (ldddsu8Other3whlDeflation[RR_WL]                    ==1) i|=0x80;      
    CAN_LOG_DATA[72] = i;      
    
    i=0;                                          
    if (ldddsu8CompleteCalib[0]                   	==1) i|=0x01;                                      
    if (ldddsu8CompleteCalib[1]                 	==1) i|=0x02;                                      
    if (ldddsu8CompleteCalib[2]                		==1) i|=0x04;                                      
	if (ldddsu8CompleteCalib[3]               		==1) i|=0x08;                                      
    if (ldddsu8CompleteCalib[4]               		==1) i|=0x10;                                      
	if (ESP_SENSOR_RELIABLE_FLG                                     		==1) i|=0x20;                             //
    if (BLS                                           ==1) i|=0x40;         
    if (BACK_DIR                                           ==1) i|=0x80;    
    CAN_LOG_DATA[73] = i;  
   
     i = 0;
     if (ldddsu1ElapsedTimeUpdateFlgOldFL  		==1) i|=0x01;   
     if (ldddsu1ElapsedTimeUpdateFlgOldFR		==1) i|=0x02;      
     if (ldddsu1ElapsedTimeUpdateFlgOldRL		==1) i|=0x04;      
     if (ldddsu1ElapsedTimeUpdateFlgOldRR		==1) i|=0x08;      
     if (ldddsu8ElapsedTimeUpdateFlgFL_K 		==1) i|=0x10;      
     if (ldddsu8ElapsedTimeUpdateFlgFR_K 		==1) i|=0x20;      
     if (ldddsu8ElapsedTimeUpdateFlgRL_K   		==1) i|=0x40;    
     if (ldddsu8ElapsedTimeUpdateFlgRR_K   		==1) i|=0x80;    
        	                 
    CAN_LOG_DATA[74] = i;  
                           
     i = 0;

    if (0                         	==1) i|=0x01;
    if (0                      	==1) i|=0x02;
    if (0                      	==1) i|=0x04;
    if (0                 			==1) i|=0x08;    
    if (ldddsu1ElapsedTimeUpdateFlgFL                		==1) i|=0x10;         
    if (ldddsu1ElapsedTimeUpdateFlgFR     					==1) i|=0x20;  
    if (ldddsu1ElapsedTimeUpdateFlgRL                        ==1) i|=0x40;     
    if (ldddsu1ElapsedTimeUpdateFlgRR                        ==1) i|=0x80;      	
    CAN_LOG_DATA[75] = i;                                                   

    i = 0;
    if (FL.RELIABLE_FOR_VREF                           	==1) i|=0x01;              
    if (FR.RELIABLE_FOR_VREF                        	==1) i|=0x02;              
    if (RL.RELIABLE_FOR_VREF                        	==1) i|=0x04;              
    if (RR.RELIABLE_FOR_VREF                   		==1) i|=0x08;                  
    if (ldddsu1CompleteCalibAllSpeed                  		==1) i|=0x10;                        
    if (ldddsu1ElapsedTimeUpdateFlgVeh     					==1) i|=0x20;   //WHL_SELECT_FLG 
#if !__RTA_2WHEEL_DCT                    
    if (RL.WHL_SELECT_FLG                         ==1) i|=0x40;                    
    if (RR.WHL_SELECT_FLG                         ==1) i|=0x80;                    
#else
	if (ldrtau1GoodCondForRta2WhlDct                         ==1) i|=0x40;
	if (ldrtau1GoodCondForRta2WhlClear                         ==1) i|=0x80;
#endif		
    CAN_LOG_DATA[76] = i;  

    i = 0;          

#if __RTA_2WHEEL_DCT
    if (RTA_FL.RTA_SUSPECT_2WHL                        	==1) i|=0x01;
    if (RTA_FR.RTA_SUSPECT_2WHL                        	==1) i|=0x02; 
    if (RTA_RL.RTA_SUSPECT_2WHL                        	==1) i|=0x04;  
    if (RTA_RR.RTA_SUSPECT_2WHL                   		==1) i|=0x08;   
#else 
    if (ESP_BRAKE_CONTROL_FL                        	==1) i|=0x01;
    if (ESP_BRAKE_CONTROL_FR                        	==1) i|=0x02; 
    if (ESP_BRAKE_CONTROL_RL                        	==1) i|=0x04;  
    if (ESP_BRAKE_CONTROL_RR                   		==1) i|=0x08;   
#endif    	
    if (ldddsu8ElapsedTimeUpdateFlg[0]                 	==1) i|=0x10;                   //ESP_BRAKE_CONTROL_FL
    if (ldddsu8ElapsedTimeUpdateFlg[1]    				==1) i|=0x20;                   //ESP_BRAKE_CONTROL_FR
    if (ldddsu8ElapsedTimeUpdateFlg[2]                  ==1) i|=0x40;                   //ESP_BRAKE_CONTROL_RL
    if (ldddsu8ElapsedTimeUpdateFlg[3]                  ==1) i|=0x80;                   //ESP_BRAKE_CONTROL_RR
    
    CAN_LOG_DATA[77] = i;                 

    i = 0;

    if (RTA_FL.RTA_SUSPECT_WL                           ==1) i|=0x01;
    if (RTA_FR.RTA_SUSPECT_WL                        	==1) i|=0x02;                    
    if (RTA_RL.RTA_SUSPECT_WL                        	==1) i|=0x04;                   
    if (RTA_RR.RTA_SUSPECT_WL                  			==1) i|=0x08;                        

    if (ldddsu1Diag2whlDeflationFLRR                  	==1) i|=0x10;
    if (ldddsu1Diag2whlDeflationFRRL     				==1) i|=0x20;  
    if (ldddsu1TireWearInCompleted                      ==1) i|=0x40;    //FL.FSF   
    if (ldddsu1PerformedDDSReset                        ==1) i|=0x80;    //FR.FSF         
                  
    CAN_LOG_DATA[78] = i;    

    i = 0;

	if (ldddsu1DeflationState        		==1) i|=0x01;       
	if (ldddsu1StdStrDrvStateForDDS  		==1) i|=0x02;       
	if (ldddsu1CompleteCalib         		==1) i|=0x04;
	if (ldddsu8SingleWhlDeflation[FL_WL]     	==1) i|=0x08;                         
	if (ldddsu8SingleWhlDeflation[FR_WL]     	==1) i|=0x10;                       
	if (ldddsu8SingleWhlDeflation[RL_WL]     	==1) i|=0x20;                     
	if (ldddsu8SingleWhlDeflation[RR_WL]     	==1) i|=0x40;                       
	if (ldddsu1RequestDDSReset       		==1) i|=0x80;                       
                               
    CAN_LOG_DATA[79] = i;    
	
}
/*===========================================================*/
void	LSDDS_vCallHybridDDSLogData(void)
{
	uint8_t i;
	
	CAN_LOG_DATA[0]  = (uint8_t)(system_loop_counter);    
	CAN_LOG_DATA[1]  = (uint8_t)(system_loop_counter>>8); 
	CAN_LOG_DATA[2]  = (uint8_t)(FL.vrad_resol_change);   
	CAN_LOG_DATA[3]  = (uint8_t)(FL.vrad_resol_change>>8);
	CAN_LOG_DATA[4]  = (uint8_t)(FR.vrad_resol_change);   
	CAN_LOG_DATA[5]  = (uint8_t)(FR.vrad_resol_change>>8);
	CAN_LOG_DATA[6]  = (uint8_t)(RL.vrad_resol_change);   
	CAN_LOG_DATA[7]  = (uint8_t)(RL.vrad_resol_change>>8);
	CAN_LOG_DATA[8]  = (uint8_t)(RR.vrad_resol_change);   
	CAN_LOG_DATA[9]  = (uint8_t)(RR.vrad_resol_change>>8);
	CAN_LOG_DATA[10] = (uint8_t)(vref_resol_change);      
	CAN_LOG_DATA[11] = (uint8_t)(vref_resol_change>>8);   
	CAN_LOG_DATA[12] = (uint8_t)(ldu8DDSMode);	        
	CAN_LOG_DATA[13] = (uint8_t)(ldu8DDSSubMode);         
	CAN_LOG_DATA[14] = (uint8_t)(ldddsu16ElapsedTimePerRev[FL_WL]);   
	CAN_LOG_DATA[15] = (uint8_t)(ldddsu16ElapsedTimePerRev[FL_WL]>>8);
	CAN_LOG_DATA[16] = (uint8_t)(ldddsu16ElapsedTimePerRev[FR_WL]);   
	CAN_LOG_DATA[17] = (uint8_t)(ldddsu16ElapsedTimePerRev[FR_WL]>>8);
	CAN_LOG_DATA[18] = (uint8_t)(ldddsu16ElapsedTimePerRev[RL_WL]);   
	CAN_LOG_DATA[19] = (uint8_t)(ldddsu16ElapsedTimePerRev[RL_WL]>>8);
	CAN_LOG_DATA[20] = (uint8_t)(ldddsu16ElapsedTimePerRev[RR_WL]);   
	CAN_LOG_DATA[21] = (uint8_t)(ldddsu16ElapsedTimePerRev[RR_WL]>>8);
	
	CAN_LOG_DATA[22] = (uint8_t)(((eng_torq_abs/10)>250)?250:(((eng_torq_abs/10)<0)?0:(int8_t)(eng_torq_abs/10)));       
	CAN_LOG_DATA[23] = (uint8_t)(((alat/10)>127)?127:(((alat/10)<-127)?-127:(int8_t)(alat/10)));                                                                 
	CAN_LOG_DATA[24] = (uint8_t)(((wstr/10)>127)?127:(((wstr/10)<-127)?-127:(int8_t)(wstr/10)));                                                              
	CAN_LOG_DATA[25] = (uint8_t)(((yaw_out/10)>127)?127:(((yaw_out/10)<-127)?-127:(int8_t)(yaw_out/10)));                
	CAN_LOG_DATA[26] = (uint8_t)(((mpress/10)>127)?127:(((mpress/10)<-127)?-127:(int8_t)(mpress/10)));    
	CAN_LOG_DATA[27] = (uint8_t)(mtp);	
	CAN_LOG_DATA[28] = (uint8_t)((CalibrationValueFL>127)?127:((CalibrationValueFL<-127)?-127:(int8_t)CalibrationValueFL));                                                                                                                                                                                                                                         
	CAN_LOG_DATA[29] = (uint8_t)((CalibrationValueFR>127)?127:((CalibrationValueFR<-127)?-127:(int8_t)CalibrationValueFR));                                                                                                                                                                                                                                         
	CAN_LOG_DATA[30] = (uint8_t)((CalibrationValueRL>127)?127:((CalibrationValueRL<-127)?-127:(int8_t)CalibrationValueRL));                                                                                                                                                                                                                                         
	CAN_LOG_DATA[31] = (uint8_t)((CalibrationValueRR>127)?127:((CalibrationValueRR<-127)?-127:(int8_t)CalibrationValueRR));                                                                                                                                                                                                                                         

			
 /* Logging Variables for Calibration */   
if(ldddsu8CompleteCalib[v_inx1] ==0)
{
	CAN_LOG_DATA[32] = (uint8_t)((ldddsu32SumDiffForCalib[FL_WL][v_inx1]/20));    
	CAN_LOG_DATA[33] = (uint8_t)((ldddsu32SumDiffForCalib[FL_WL][v_inx1]/20)>>8); 
	CAN_LOG_DATA[34] = (uint8_t)((ldddsu32SumDiffForCalib[FR_WL][v_inx1]/20));    
	CAN_LOG_DATA[35] = (uint8_t)((ldddsu32SumDiffForCalib[FR_WL][v_inx1]/20)>>8); 
	CAN_LOG_DATA[36] = (uint8_t)((ldddsu32SumDiffForCalib[RL_WL][v_inx1]/20));    
	CAN_LOG_DATA[37] = (uint8_t)((ldddsu32SumDiffForCalib[RL_WL][v_inx1]/20)>>8); 
	CAN_LOG_DATA[38] = (uint8_t)((ldddsu32SumDiffForCalib[RR_WL][v_inx1]/20));    
	CAN_LOG_DATA[39] = (uint8_t)((ldddsu32SumDiffForCalib[RR_WL][v_inx1]/20)>>8);  	
	CAN_LOG_DATA[40] = (uint8_t)(ldddsu16CntCalibTotalRev[v_inx1]);                                                                                                                                                                                                                                                                                                     
	CAN_LOG_DATA[41] = (uint8_t)(ldddsu16CntCalibTotalRev[v_inx1]>>8);	  
	CAN_LOG_DATA[42] = (uint8_t)(ldddsu16CntCalibTotalRev[v_inx2]);                                                                                                                                                                                        
	CAN_LOG_DATA[43] = (uint8_t)(ldddsu16CntCalibTotalRev[v_inx2]>>8);
	
	CAN_LOG_DATA[44] = (uint8_t)(ldu8DDSModeOld);                                                                                                                                                                                                                                                  
	CAN_LOG_DATA[45] = (uint8_t)(ldu8DDSModeEepromValue);  
	CAN_LOG_DATA[46] = (uint8_t)(((ldddss16CumulativeMean[FL_WL][v_inx1])>127)?127:(((ldddss16CumulativeMean[FL_WL][v_inx1])<-127)?-127:(int8_t)(ldddss16CumulativeMean[FL_WL][v_inx1])));                                                                                                                                                                                
	CAN_LOG_DATA[47] = (uint8_t)(((ldddss16CumulativeMean[FR_WL][v_inx1])>127)?127:(((ldddss16CumulativeMean[FR_WL][v_inx1])<-127)?-127:(int8_t)(ldddss16CumulativeMean[FR_WL][v_inx1])));                                                                                                                                                                                
	CAN_LOG_DATA[48] = (uint8_t)(((ldddss16CumulativeMean[RL_WL][v_inx1])>127)?127:(((ldddss16CumulativeMean[RL_WL][v_inx1])<-127)?-127:(int8_t)(ldddss16CumulativeMean[RL_WL][v_inx1])));                                                                                                                                                                                
	CAN_LOG_DATA[49] = (uint8_t)(((ldddss16CumulativeMean[RR_WL][v_inx1])>127)?127:(((ldddss16CumulativeMean[RR_WL][v_inx1])<-127)?-127:(int8_t)(ldddss16CumulativeMean[RR_WL][v_inx1])));                                                                                                                                                                                
 	CAN_LOG_DATA[50] = (uint8_t)((ldddss16CumulativeMean[FL_WL][v_inx2]>127)?127:((ldddss16CumulativeMean[FL_WL][v_inx2]<-127)?-127:(int8_t)ldddss16CumulativeMean[FL_WL][v_inx2]));                                                                  
	CAN_LOG_DATA[51] = (uint8_t)((ldddss16CumulativeMean[FR_WL][v_inx2]>127)?127:((ldddss16CumulativeMean[FR_WL][v_inx2]<-127)?-127:(int8_t)ldddss16CumulativeMean[FR_WL][v_inx2]));                                                                  
	CAN_LOG_DATA[52] = (uint8_t)((ldddss16CumulativeMean[RL_WL][v_inx2]>127)?127:((ldddss16CumulativeMean[RL_WL][v_inx2]<-127)?-127:(int8_t)ldddss16CumulativeMean[RL_WL][v_inx2]));                                                                   
	CAN_LOG_DATA[53] = (uint8_t)((ldddss16CumulativeMean[RR_WL][v_inx2]>127)?127:((ldddss16CumulativeMean[RR_WL][v_inx2]<-127)?-127:(int8_t)ldddss16CumulativeMean[RR_WL][v_inx2]));                                                                   

 					
	CAN_LOG_DATA[54] = (uint8_t)(ldddss16DDSCalibTemp[FL_WL]);   
	CAN_LOG_DATA[55] = (uint8_t)(ldddss16DDSCalibTemp[FL_WL]>>8); 
	CAN_LOG_DATA[56] = (uint8_t)(ldddss16DDSCalibTemp[FR_WL]);   
	CAN_LOG_DATA[57] = (uint8_t)(ldddss16DDSCalibTemp[FR_WL]>>8); 
	CAN_LOG_DATA[58] = (uint8_t)(ldddss16DDSCalibTemp[RL_WL]);   
	CAN_LOG_DATA[59] = (uint8_t)(ldddss16DDSCalibTemp[RL_WL]>>8); 
	CAN_LOG_DATA[60] = (uint8_t)(ldddss16DDSCalibTemp[RR_WL]);   
	CAN_LOG_DATA[61] = (uint8_t)(ldddss16DDSCalibTemp[RR_WL]>>8); 
                                                                                                                                                                                                                                                                                           
	CAN_LOG_DATA[62] = (uint8_t)(WheelDiffMinMax[FL_WL][v_inx1]);                                                                                                                                                                                                                
	CAN_LOG_DATA[63] = (uint8_t)(WheelDiffMinMax[FL_WL][v_inx1]>>8);                                                                                                                                                                                                                
	CAN_LOG_DATA[64] = (uint8_t)(WheelDiffMinMax[FR_WL][v_inx1]);                                                                                                                                                                                                               
	CAN_LOG_DATA[65] = (uint8_t)(WheelDiffMinMax[FR_WL][v_inx1]>>8);                                                                                                                                                                                                              
	CAN_LOG_DATA[66] = (uint8_t)(WheelDiffMinMax[RL_WL][v_inx1]);
	CAN_LOG_DATA[67] = (uint8_t)(WheelDiffMinMax[RL_WL][v_inx1]>>8);
	CAN_LOG_DATA[68] = (uint8_t)(WheelDiffMinMax[RR_WL][v_inx1]);
	CAN_LOG_DATA[69] = (uint8_t)(WheelDiffMinMax[RR_WL][v_inx1]>>8);                                         	                                                                                                                                                                                	                                                       
}
 /* Logging Variables for Detection */   
else
{		
    /*
	CAN_LOG_DATA[32] = (uint8_t)(ldddsu32SumDctInput[FL_WL][v_inx]/20);                                                                                                                        
	CAN_LOG_DATA[33] = (uint8_t)((ldddsu32SumDctInput[FL_WL][v_inx]/20)>>8);                                                                                                                   
	CAN_LOG_DATA[34] = (uint8_t)(ldddsu32SumDctInput[FR_WL][v_inx]/20);                                                                                                                        
	CAN_LOG_DATA[35] = (uint8_t)((ldddsu32SumDctInput[FR_WL][v_inx]/20)>>8);                                                                                                                   
	CAN_LOG_DATA[36] = (uint8_t)(ldddsu32SumDctInput[RL_WL][v_inx]/20);                                                                                                                        
	CAN_LOG_DATA[37] = (uint8_t)((ldddsu32SumDctInput[RL_WL][v_inx]/20)>>8);                                                                                                                   
	CAN_LOG_DATA[38] = (uint8_t)(ldddsu32SumDctInput[RR_WL][v_inx]/20);                                                                                                                        
	CAN_LOG_DATA[39] = (uint8_t)((ldddsu32SumDctInput[RR_WL][v_inx]/20)>>8);  
	*/
	CAN_LOG_DATA[32] = (uint8_t)(ldddshs16InitTpmsPressure);
	CAN_LOG_DATA[33] = (uint8_t)(ldddshs16CurrentTpmsPressure);
	CAN_LOG_DATA[34] = (uint8_t)(ldddshs16RefTirePressure);
	CAN_LOG_DATA[35] = (uint8_t)(ldddshs16RefTireTemperature);
	CAN_LOG_DATA[36] = (uint8_t)(ldddshu8DefDctCntByTpmsSnr);
	CAN_LOG_DATA[37] = (uint8_t)(ldddshu8DefDctCntByAxleDiff);
	CAN_LOG_DATA[38] = (uint8_t)(ldddshu8DefDctCntBySideDiff);
	CAN_LOG_DATA[39] = (uint8_t)(ldddshu8DefDctCntByDiagDiff);
	
	  
	CAN_LOG_DATA[40] = (uint8_t)(ldddss16DctFLRRDefCnt);    
	CAN_LOG_DATA[41] = (uint8_t)(ldddss16DctFLRRDefCnt>>8); 
	CAN_LOG_DATA[42] = (uint8_t)(ldddss16DctFRRLDefCnt);    
	CAN_LOG_DATA[43] = (uint8_t)(ldddss16DctFRRLDefCnt>>8); 	

	CAN_LOG_DATA[44] = (uint8_t)((ldddss16Dct1WhlDefCnt[FR_WL][0]>250)?250:((ldddss16Dct1WhlDefCnt[FR_WL][0]<0)?0:(uint8_t)ldddss16Dct1WhlDefCnt[FR_WL][0])); 
	CAN_LOG_DATA[45] = (uint8_t)((ldddss16Dct1WhlDefCnt[FR_WL][1]>250)?250:((ldddss16Dct1WhlDefCnt[FR_WL][1]<0)?0:(uint8_t)ldddss16Dct1WhlDefCnt[FR_WL][1]));  	
	CAN_LOG_DATA[46] = (uint8_t)((ldddss16Dct1WhlDefCnt[FR_WL][2]>250)?250:((ldddss16Dct1WhlDefCnt[FR_WL][2]<0)?0:(uint8_t)ldddss16Dct1WhlDefCnt[FR_WL][2]));                         
	CAN_LOG_DATA[47] = (uint8_t)((ldddss16Dct1WhlDefCnt[FR_WL][3]>250)?250:((ldddss16Dct1WhlDefCnt[FR_WL][3]<0)?0:(uint8_t)ldddss16Dct1WhlDefCnt[FR_WL][3]));       					                           
	CAN_LOG_DATA[48] = (uint8_t)((ldddss16Dct1WhlDefCnt[FR_WL][4]>250)?250:((ldddss16Dct1WhlDefCnt[FR_WL][4]<0)?0:(uint8_t)ldddss16Dct1WhlDefCnt[FR_WL][4]));   					 
//	CAN_LOG_DATA[49] = (uint8_t)((ldddss16Dct1WhlDefCnt[RL_WL][0]>250)?250:((ldddss16Dct1WhlDefCnt[RL_WL][0]<0)?0:(uint8_t)ldddss16Dct1WhlDefCnt[RL_WL][0])); 	
//	CAN_LOG_DATA[50] = (uint8_t)((ldddss16Dct1WhlDefCnt[RL_WL][1]>250)?250:((ldddss16Dct1WhlDefCnt[RL_WL][1]<0)?0:(uint8_t)ldddss16Dct1WhlDefCnt[RL_WL][1])); 
//	CAN_LOG_DATA[51] = (uint8_t)((ldddss16Dct1WhlDefCnt[RL_WL][2]>250)?250:((ldddss16Dct1WhlDefCnt[RL_WL][2]<0)?0:(uint8_t)ldddss16Dct1WhlDefCnt[RL_WL][2])); 
//	CAN_LOG_DATA[52] = (uint8_t)((ldddss16Dct1WhlDefCnt[RL_WL][3]>250)?250:((ldddss16Dct1WhlDefCnt[RL_WL][3]<0)?0:(uint8_t)ldddss16Dct1WhlDefCnt[RL_WL][3])); 
	CAN_LOG_DATA[49] = (UCHAR)(ldddshu8DefDctCntByAxleDiff); //((ldddss16Dct1WhlDefCnt[RL_WL][0]>250)?250:((ldddss16Dct1WhlDefCnt[RL_WL][0]<0)?0:(UCHAR)ldddss16Dct1WhlDefCnt[RL_WL][0])); 	
	CAN_LOG_DATA[50] = (UCHAR)(ldddshu8DefDctCntByTpmsSnr); //((ldddss16Dct1WhlDefCnt[RL_WL][1]>250)?250:((ldddss16Dct1WhlDefCnt[RL_WL][1]<0)?0:(UCHAR)ldddss16Dct1WhlDefCnt[RL_WL][1])); 
	CAN_LOG_DATA[51] = (UCHAR)(ldddshu8DefDctCntByDiagDiff); //((ldddss16Dct1WhlDefCnt[RL_WL][2]>250)?250:((ldddss16Dct1WhlDefCnt[RL_WL][2]<0)?0:(UCHAR)ldddss16Dct1WhlDefCnt[RL_WL][2])); 
	CAN_LOG_DATA[52] = (UCHAR)(ldddshu8DefDctCntBySideDiff); //((ldddss16Dct1WhlDefCnt[RL_WL][3]>250)?250:((ldddss16Dct1WhlDefCnt[RL_WL][3]<0)?0:(UCHAR)ldddss16Dct1WhlDefCnt[RL_WL][3])); 
	
	CAN_LOG_DATA[53] = (uint8_t)((ldddss16Dct1WhlDefCnt[RL_WL][4]>250)?250:((ldddss16Dct1WhlDefCnt[RL_WL][4]<0)?0:(uint8_t)ldddss16Dct1WhlDefCnt[RL_WL][4])); 

	
	CAN_LOG_DATA[54] = (uint8_t)(((ldddss16AxleDiffRunningAvg[FR_WL][v_inx]/5)>127)?127:(((ldddss16AxleDiffRunningAvg[FR_WL][v_inx]/5)<-127)?-127:(int8_t)(ldddss16AxleDiffRunningAvg[FR_WL][v_inx]/5))); 
	CAN_LOG_DATA[55] = (uint8_t)(((ldddss16SideDiffRunningAvg[FR_WL][v_inx]/5)>127)?127:(((ldddss16SideDiffRunningAvg[FR_WL][v_inx]/5)<-127)?-127:(int8_t)(ldddss16SideDiffRunningAvg[FR_WL][v_inx]/5))); 
	CAN_LOG_DATA[56] = (uint8_t)(((ldddss16DiagDiffRunningAvg[FR_WL][v_inx]/5)>127)?127:(((ldddss16DiagDiffRunningAvg[FR_WL][v_inx]/5)<-127)?-127:(int8_t)(ldddss16DiagDiffRunningAvg[FR_WL][v_inx]/5))); 
	CAN_LOG_DATA[57] = (uint8_t)(((ldddss16AxleDiffRunningAvg[RL_WL][v_inx]/5)>127)?127:(((ldddss16AxleDiffRunningAvg[RL_WL][v_inx]/5)<-127)?-127:(int8_t)(ldddss16AxleDiffRunningAvg[RL_WL][v_inx]/5))); 
	CAN_LOG_DATA[58] = (uint8_t)(((ldddss16SideDiffRunningAvg[RL_WL][v_inx]/5)>127)?127:(((ldddss16SideDiffRunningAvg[RL_WL][v_inx]/5)<-127)?-127:(int8_t)(ldddss16SideDiffRunningAvg[RL_WL][v_inx]/5))); 
	CAN_LOG_DATA[59] = (uint8_t)(((ldddss16DiagDiffRunningAvg[FL_WL][v_inx]/5)>127)?127:(((ldddss16DiagDiffRunningAvg[FL_WL][v_inx]/5)<-127)?-127:(int8_t)(ldddss16DiagDiffRunningAvg[FL_WL][v_inx]/5)));    
	CAN_LOG_DATA[60] = (uint8_t)((ldddss8PressEstByAxleDiff[FR_WL]>127)?127:((ldddss8PressEstByAxleDiff[FR_WL]<-127)?-127:(int8_t)ldddss8PressEstByAxleDiff[FR_WL]));                                                   
	CAN_LOG_DATA[61] = (uint8_t)((ldddss8PressEstBySideDiff[FR_WL]>127)?127:((ldddss8PressEstBySideDiff[FR_WL]<-127)?-127:(int8_t)ldddss8PressEstBySideDiff[FR_WL])); 
	CAN_LOG_DATA[62] = (uint8_t)((ldddss8PressEstByDiagDiff[FR_WL]>127)?127:((ldddss8PressEstByDiagDiff[FR_WL]<-127)?-127:(int8_t)ldddss8PressEstByDiagDiff[FR_WL]));                                                   
	CAN_LOG_DATA[63] = (uint8_t)((ldddss8PressEstByAxleDiff[RL_WL]>127)?127:((ldddss8PressEstByAxleDiff[RL_WL]<-127)?-127:(int8_t)ldddss8PressEstByAxleDiff[RL_WL])); 
	CAN_LOG_DATA[64] = (uint8_t)((ldddss8PressEstBySideDiff[RL_WL]>127)?127:((ldddss8PressEstBySideDiff[RL_WL]<-127)?-127:(int8_t)ldddss8PressEstBySideDiff[RL_WL])); 
	CAN_LOG_DATA[65] = (uint8_t)((ldddss8PressEstByDiagDiff[RL_WL]>127)?127:((ldddss8PressEstByDiagDiff[RL_WL]<-127)?-127:(int8_t)ldddss8PressEstByDiagDiff[RL_WL])); 
/*
	CAN_LOG_DATA[66] = (uint8_t)((AxleDiffMinMax[FL_WL]>250)?250:((AxleDiffMinMax[FL_WL]<0)?0:(uint8_t)AxleDiffMinMax[FL_WL]));
	CAN_LOG_DATA[67] = (uint8_t)((AxleDiffMinMax[RL_WL]>250)?250:((AxleDiffMinMax[RL_WL]<0)?0:(uint8_t)AxleDiffMinMax[RL_WL]));
	CAN_LOG_DATA[68] = (uint8_t)((SideDiffMinMax[FL_WL]>250)?250:((SideDiffMinMax[FL_WL]<0)?0:(uint8_t)SideDiffMinMax[FL_WL]));
	CAN_LOG_DATA[69] = (uint8_t)((SideDiffMinMax[FR_WL]>250)?250:((SideDiffMinMax[FR_WL]<0)?0:(uint8_t)SideDiffMinMax[FR_WL]));
*/
	CAN_LOG_DATA[66] = (uint8_t)(ldddshs8DefbyTpmsSensor);
	CAN_LOG_DATA[67] = (uint8_t)(ldddshs8DefbyAxleDiffWithTpSnr);
	CAN_LOG_DATA[68] = (uint8_t)(ldddshs8DefbySideDiffWithTpSnr);
	CAN_LOG_DATA[69] = (uint8_t)(ldddshs8DefbyDiagDiffWithTpSnr);
                                                            
}
    i=0;                                  
    if (ABS_fz                          ==1) i|=0x01; 
    if (BRAKE_SIGNAL                    ==1) i|=0x02; 
    if (BTCS_ON                         ==1) i|=0x04; 
    if (ETCS_ON                         ==1) i|=0x08; 
	if (ESP_TCS_ON                      ==1) i|=0x10;
	if (YAW_CDC_WORK                    ==1) i|=0x20; 
	if (BRAKING_STATE                    ==1) i|=0x40; 
	if (ESP_ERROR_FLG                   ==1) i|=0x80; 

	CAN_LOG_DATA[70] = i;
	
	i=0; 
	#if defined ( __H2DCL)
	if (ctu1DDS											==1) i|=0x01; 
	#elif defined (__DDS_RESET_BY_ESC_SW == ENABLE)
    if (fu1DdsSwitchDriverIntent                         ==1) i|=0x01; 
    #else
    if (0                         ==1) i|=0x01;
    #endif
    	
    if (Rough_road_detect_vehicle                    		==1) i|=0x02; 
    if (ldddsu1RunningAvgUpdateFlag                          ==1) i|=0x04; 
    
    #if __4WD||(__4WD_VARIANT_CODE==ENABLE)
    if (USE_ALONG                        	==1) i|=0x08; 
	if (SPIN_CALC_FLG                      	==1) i|=0x10;
	if (BIG_ACCEL_SPIN_FZ                   ==1) i|=0x20; 
	if (ACCEL_SPIN_FZ                    	==1) i|=0x40; 
	if (Rough_road_suspect_vehicle                   	==1) i|=0x80; 
	#else
    if (EBD_RA                        	==1) i|=0x08; 
	if (PARKING_BRAKE_OPERATION                       	==1) i|=0x10; 
	if (SAS_CHECK_OK                    ==1) i|=0x20; 
	if (RL.RELIABLE_FOR_VREF                     	==1) i|=0x40; 
	if (Rough_road_suspect_vehicle                    	==1) i|=0x80; 	
	#endif


    CAN_LOG_DATA[71] = i;               

    i = 0;                                   
    if (ABS_fl                          ==1) i|=0x01;
    if (ABS_fr                       	==1) i|=0x02;
    if (ABS_rl                       	==1) i|=0x04;
	if (ABS_rr                      	==1) i|=0x08; 
    if (ldddsu8Other3whlDeflation[FL_WL]                    ==1) i|=0x10;      
    if (ldddsu8Other3whlDeflation[FR_WL]     				==1) i|=0x20;		
    if (ldddsu8Other3whlDeflation[RL_WL]                    ==1) i|=0x40;      
    if (ldddsu8Other3whlDeflation[RR_WL]                    ==1) i|=0x80;      
    CAN_LOG_DATA[72] = i;      
    
    i=0;                                          
    if (ldddsu8CompleteCalib[0]                   	==1) i|=0x01;                                      
    if (ldddsu8CompleteCalib[1]                 	==1) i|=0x02;                                      
    if (ldddsu8CompleteCalib[2]                		==1) i|=0x04;                                      
	if (ldddsu8CompleteCalib[3]               		==1) i|=0x08;                                      
    if (ldddsu8CompleteCalib[4]               		==1) i|=0x10;                                      
	if (ESP_SENSOR_RELIABLE_FLG                                     		==1) i|=0x20;                             //
    if (BLS                                           ==1) i|=0x40;         
    if (BACK_DIR                                           ==1) i|=0x80;    
    CAN_LOG_DATA[73] = i;  
   
     i = 0;
     if (ldddsu1ElapsedTimeUpdateFlgOldFL  		==1) i|=0x01;   
     if (ldddsu1ElapsedTimeUpdateFlgOldFR		==1) i|=0x02;      
     if (ldddsu1ElapsedTimeUpdateFlgOldRL		==1) i|=0x04;      
     if (ldddsu1ElapsedTimeUpdateFlgOldRR		==1) i|=0x08;      
     if (ldddsu8ElapsedTimeUpdateFlgFL_K 		==1) i|=0x10;      
     if (ldddsu8ElapsedTimeUpdateFlgFR_K 		==1) i|=0x20;      
     if (ldddsu8ElapsedTimeUpdateFlgRL_K   		==1) i|=0x40;    
     if (ldddsu8ElapsedTimeUpdateFlgRR_K   		==1) i|=0x80;    
        	                 
    CAN_LOG_DATA[74] = i;  
                           
     i = 0;

    if (ldddshu1RefTirePressureV                             	==1) i|=0x01;
    if (ldddshu1DeflationByHybridDds                      	==1) i|=0x02;
    if (0                      	==1) i|=0x04;
    if (0                 			==1) i|=0x08;    
    if (ldddsu1ElapsedTimeUpdateFlgFL                		==1) i|=0x10;         
    if (ldddsu1ElapsedTimeUpdateFlgFR     					==1) i|=0x20;  
    if (ldddsu1ElapsedTimeUpdateFlgRL                        ==1) i|=0x40;     
    if (ldddsu1ElapsedTimeUpdateFlgRR                        ==1) i|=0x80;      	
    CAN_LOG_DATA[75] = i;                                                   

    i = 0;
    if (FL.RELIABLE_FOR_VREF                           	==1) i|=0x01;              
    if (FR.RELIABLE_FOR_VREF                        	==1) i|=0x02;              
    if (RL.RELIABLE_FOR_VREF                        	==1) i|=0x04;              
    if (RR.RELIABLE_FOR_VREF                   		==1) i|=0x08;                  
    if (ldddsu1CompleteCalibAllSpeed                  		==1) i|=0x10;                        
    if (ldddsu1ElapsedTimeUpdateFlgVeh     					==1) i|=0x20;   //WHL_SELECT_FLG 
#if !__RTA_2WHEEL_DCT                    
    if (RL.WHL_SELECT_FLG                         ==1) i|=0x40;                    
    if (RR.WHL_SELECT_FLG                         ==1) i|=0x80;                    
#else
	if (ldrtau1GoodCondForRta2WhlDct                         ==1) i|=0x40;
	if (ldrtau1GoodCondForRta2WhlClear                         ==1) i|=0x80;
#endif		
    CAN_LOG_DATA[76] = i;  

    i = 0;          

#if __RTA_2WHEEL_DCT
    if (RTA_FL.RTA_SUSPECT_2WHL                        	==1) i|=0x01;
    if (RTA_FR.RTA_SUSPECT_2WHL                        	==1) i|=0x02; 
    if (RTA_RL.RTA_SUSPECT_2WHL                        	==1) i|=0x04;  
    if (RTA_RR.RTA_SUSPECT_2WHL                   		==1) i|=0x08;   
#else 
    if (ESP_BRAKE_CONTROL_FL                        	==1) i|=0x01;
    if (ESP_BRAKE_CONTROL_FR                        	==1) i|=0x02; 
    if (ESP_BRAKE_CONTROL_RL                        	==1) i|=0x04;  
    if (ESP_BRAKE_CONTROL_RR                   		==1) i|=0x08;   
#endif    	
    if (ldddsu8ElapsedTimeUpdateFlg[0]                 	==1) i|=0x10;                   //ESP_BRAKE_CONTROL_FL
    if (ldddsu8ElapsedTimeUpdateFlg[1]    				==1) i|=0x20;                   //ESP_BRAKE_CONTROL_FR
    if (ldddsu8ElapsedTimeUpdateFlg[2]                  ==1) i|=0x40;                   //ESP_BRAKE_CONTROL_RL
    if (ldddsu8ElapsedTimeUpdateFlg[3]                  ==1) i|=0x80;                   //ESP_BRAKE_CONTROL_RR
    
    CAN_LOG_DATA[77] = i;                 

    i = 0;

//    if (RTA_FL.RTA_SUSPECT_WL                           ==1) i|=0x01;
//    if (RTA_FR.RTA_SUSPECT_WL                        	==1) i|=0x02;                    
//    if (RTA_RL.RTA_SUSPECT_WL                        	==1) i|=0x04;                   
//    if (RTA_RR.RTA_SUSPECT_WL                  			==1) i|=0x08;                        
    if (ldddsu1LowTirePressStateFlgFL                         ==1) i|=0x01;//RTA_FL.RTA_SUSPECT_WL  
    if (ldddsu1LowTirePressStateFlgFR                        	==1) i|=0x02;      //RTA_FR.RTA_SUSPECT_WL                     
    if (ldddsu1LowTirePressStateFlgRL                       	==1) i|=0x04;   //RTA_RL.RTA_SUSPECT_WL                    
    if (ldddsu1LowTirePressStateFlgRR                  			==1) i|=0x08;   //RTA_RR.RTA_SUSPECT_WL                    

    if (ldddsu1Diag2whlDeflationFLRR                  	==1) i|=0x10;
    if (ldddsu1Diag2whlDeflationFRRL     				==1) i|=0x20;  
    if (ldddsu1TireWearInCompleted                      ==1) i|=0x40;    //FL.FSF   
    if (ldddsu1PerformedDDSReset                        ==1) i|=0x80;    //FR.FSF         
                  
    CAN_LOG_DATA[78] = i;    

    i = 0;

	if (ldddsu1DeflationState        		==1) i|=0x01;       
	if (ldddsu1StdStrDrvStateForDDS  		==1) i|=0x02;       
	if (ldddsu1CompleteCalib         		==1) i|=0x04;
	if (ldddsu8SingleWhlDeflation[FL_WL]     	==1) i|=0x08;                         
	if (ldddsu8SingleWhlDeflation[FR_WL]     	==1) i|=0x10;                       
	if (ldddsu8SingleWhlDeflation[RL_WL]     	==1) i|=0x20;                     
	if (ldddsu8SingleWhlDeflation[RR_WL]     	==1) i|=0x40;                       
	if (ldddsu1RequestDDSReset       		==1) i|=0x80;                       
                               
    CAN_LOG_DATA[79] = i;    
	
}
void	LSDDS_vCallDDSPlusLogData(void)
{
        uint8_t i;
        CAN_LOG_DATA[0] =(uint8_t) (system_loop_counter);   
        CAN_LOG_DATA[1] =(uint8_t) (system_loop_counter>>8);
        CAN_LOG_DATA[2] =(uint8_t) (FL.vrad_resol_change);   
        CAN_LOG_DATA[3] =(uint8_t) (FL.vrad_resol_change>>8);
        CAN_LOG_DATA[4] =(uint8_t) (FR.vrad_resol_change);   
        CAN_LOG_DATA[5] =(uint8_t) (FR.vrad_resol_change>>8);
        CAN_LOG_DATA[6] =(uint8_t) (RL.vrad_resol_change);   
        CAN_LOG_DATA[7] =(uint8_t) (RL.vrad_resol_change>>8);
        CAN_LOG_DATA[8] =(uint8_t) (RR.vrad_resol_change);   
        CAN_LOG_DATA[9] =(uint8_t) (RR.vrad_resol_change>>8);
        CAN_LOG_DATA[10] =(uint8_t) (vref_resol_change);
        CAN_LOG_DATA[11] =(uint8_t) (vref_resol_change>>8);                
        CAN_LOG_DATA[12] =(uint8_t)(DDS_FL.StoredPeriod2[0]);    
        CAN_LOG_DATA[13] =(uint8_t)(DDS_FL.StoredPeriod2[0]>>8); 
        CAN_LOG_DATA[14] =(uint8_t)(DDS_FL.StoredPeriod2[1]);    
        CAN_LOG_DATA[15] =(uint8_t)(DDS_FL.StoredPeriod2[1]>>8); 
        CAN_LOG_DATA[16] =(uint8_t)(DDS_FL.StoredPeriod2[2]);    
        CAN_LOG_DATA[17] =(uint8_t)(DDS_FL.StoredPeriod2[2]>>8); 
        CAN_LOG_DATA[18] =(uint8_t)(DDS_FL.StoredPeriod2[3]);   
        CAN_LOG_DATA[19] =(uint8_t)(DDS_FL.StoredPeriod2[3]>>8);
        CAN_LOG_DATA[20] =(uint8_t)(DDS_FL.StoredPeriod2[4]);   
        CAN_LOG_DATA[21] =(uint8_t)(DDS_FL.StoredPeriod2[4]>>8);        
        CAN_LOG_DATA[22] =(uint8_t)(DDS_FR.StoredPeriod2[0]);   
        CAN_LOG_DATA[23] =(uint8_t)(DDS_FR.StoredPeriod2[0]>>8);
        CAN_LOG_DATA[24] =(uint8_t)(DDS_FR.StoredPeriod2[1]);   
        CAN_LOG_DATA[25] =(uint8_t)(DDS_FR.StoredPeriod2[1]>>8);
        CAN_LOG_DATA[26] =(uint8_t)(DDS_FR.StoredPeriod2[2]);   
        CAN_LOG_DATA[27] =(uint8_t)(DDS_FR.StoredPeriod2[2]>>8);
        CAN_LOG_DATA[28] =(uint8_t)(DDS_FR.StoredPeriod2[3]);   
        CAN_LOG_DATA[29] =(uint8_t)(DDS_FR.StoredPeriod2[3]>>8);
        CAN_LOG_DATA[30] =(uint8_t)(DDS_FR.StoredPeriod2[4]);   
        CAN_LOG_DATA[31] =(uint8_t)(DDS_FR.StoredPeriod2[4]>>8);
        
        CAN_LOG_DATA[32] =(uint8_t)(DDS_RL.StoredPeriod2[0]);   
        CAN_LOG_DATA[33] =(uint8_t)(DDS_RL.StoredPeriod2[0]>>8);
        CAN_LOG_DATA[34] =(uint8_t)(DDS_RL.StoredPeriod2[1]);   
        CAN_LOG_DATA[35] =(uint8_t)(DDS_RL.StoredPeriod2[1]>>8);
        CAN_LOG_DATA[36] =(uint8_t)(DDS_RL.StoredPeriod2[2]);   
        CAN_LOG_DATA[37] =(uint8_t)(DDS_RL.StoredPeriod2[2]>>8);
        CAN_LOG_DATA[38] =(uint8_t)(DDS_RL.StoredPeriod2[3]);   
        CAN_LOG_DATA[39] =(uint8_t)(DDS_RL.StoredPeriod2[3]>>8);
        CAN_LOG_DATA[40] =(uint8_t)(DDS_RL.StoredPeriod2[4]);   
        CAN_LOG_DATA[41] =(uint8_t)(DDS_RL.StoredPeriod2[4]>>8);
        
        CAN_LOG_DATA[42] =(uint8_t)(DDS_RR.StoredPeriod2[0]);   
        CAN_LOG_DATA[43] =(uint8_t)(DDS_RR.StoredPeriod2[0]>>8);
        CAN_LOG_DATA[44] =(uint8_t)(DDS_RR.StoredPeriod2[1]);   
        CAN_LOG_DATA[45] =(uint8_t)(DDS_RR.StoredPeriod2[1]>>8);
        CAN_LOG_DATA[46] =(uint8_t)(DDS_RR.StoredPeriod2[2]);   
        CAN_LOG_DATA[47] =(uint8_t)(DDS_RR.StoredPeriod2[2]>>8);
        CAN_LOG_DATA[48] =(uint8_t)(DDS_RR.StoredPeriod2[3]);   
        CAN_LOG_DATA[49] =(uint8_t)(DDS_RR.StoredPeriod2[3]>>8);
        CAN_LOG_DATA[50] =(uint8_t)(DDS_RR.StoredPeriod2[4]);   
        CAN_LOG_DATA[51] =(uint8_t)(DDS_RR.StoredPeriod2[4]>>8);
        
        CAN_LOG_DATA[52] =(uint8_t) (DDS_FL.PeriodStoreCnt);
        CAN_LOG_DATA[53] =(uint8_t) (DDS_FR.PeriodStoreCnt);
        CAN_LOG_DATA[54] =(uint8_t) (DDS_RL.PeriodStoreCnt);
        CAN_LOG_DATA[55] =(uint8_t) (DDS_RR.PeriodStoreCnt);
        CAN_LOG_DATA[56] =(uint8_t)(ldu8DDSMode);	        
        CAN_LOG_DATA[57] =(uint8_t)(ldu8DDSSubMode);  
        CAN_LOG_DATA[58] =(uint8_t) (eng_torq_abs);
        CAN_LOG_DATA[59] =(uint8_t) (eng_torq_abs>>8);  
        CAN_LOG_DATA[60] =(uint8_t) (yaw_out);   
        CAN_LOG_DATA[61] =(uint8_t) (yaw_out>>8);  
        CAN_LOG_DATA[62] =(uint8_t) (wstr);
        CAN_LOG_DATA[63] =(uint8_t) (wstr>>8); 
        CAN_LOG_DATA[64] =(uint8_t) (alat);
        CAN_LOG_DATA[65] =(uint8_t) (alat>>8);                                  
        CAN_LOG_DATA[66] =(uint8_t) (ebd_refilt_grv>127)?127:((ebd_refilt_grv<-127)?-127:(int8_t)ebd_refilt_grv);  
        CAN_LOG_DATA[67] =(uint8_t) (aref_avg>127)?127:((aref_avg<-127)?-127:(int8_t)aref_avg);     
        CAN_LOG_DATA[68] =(uint8_t) (afz>127)?127:((afz<-127)?-127:(int8_t)afz);  
        CAN_LOG_DATA[69] =(uint8_t) (FL.arad);
        CAN_LOG_DATA[70] =(uint8_t) (FR.arad);
        CAN_LOG_DATA[71] =(uint8_t) (RL.arad);   
        CAN_LOG_DATA[72] =(uint8_t) (mtp);//(eng_rpm>>8);        
        CAN_LOG_DATA[73] =(uint8_t) ldddshs16RefTirePressure;
        CAN_LOG_DATA[74] =(uint8_t) ldddshs16RefTireTemperature;            
             
        i=0;
        if (ABS_fz                          ==1) i|=0x01;
        if (EBD_RA                             ==1) i|=0x02;
        if (AFZ_OK                          ==1) i|=0x04;
        if (YAW_CDC_WORK                   ==1) i|=0x08;
        if (BEND_DETECT                     ==1) i|=0x10;
        if (BLS            ==1) i|=0x20;
        if (BRAKING_STATE                   ==1) i|=0x40;
        if (BRAKE_SIGNAL                    ==1) i|=0x80;
        CAN_LOG_DATA[75] =(uint8_t) (i);                                   //57

        i=0;

	#if defined ( __H2DCL)
	if (ctu1DDS											==1) i|=0x01; 
	#elif defined (__DDS_RESET_BY_ESC_SW == ENABLE)
    if (fu1DdsSwitchDriverIntent                         ==1) i|=0x01; 
    #else
    if (0                         ==1) i|=0x01;
    #endif           

        if (PARKING_BRAKE_OPERATION                     ==1) i|=0x02;
        if (Rough_road_suspect_vehicle      ==1) i|=0x04;
        if (Rough_road_detect_vehicle              ==1) i|=0x08;
        if (MOT_ON_FLG   ==1) i|=0x10;
        if (TCS_ON       ==1) i|=0x20;
        if (SAS_CHECK_OK                      ==1) i|=0x40;
        if (ESP_SENSOR_RELIABLE_FLG                          ==1) i|=0x80;
        CAN_LOG_DATA[76] =(uint8_t) (i);                                   //58

        i = 0;
        if (ESP_BRAKE_CONTROL_FL                          ==1) i|=0x01;
        if (ESP_BRAKE_CONTROL_FR                        ==1) i|=0x02;
        if (ESP_BRAKE_CONTROL_RL                       ==1) i|=0x04;
        if (ESP_BRAKE_CONTROL_RR                      ==1) i|=0x08;
        if (FL.RELIABLE_FOR_VREF                       ==1) i|=0x10;
        if (FR.RELIABLE_FOR_VREF       ==1) i|=0x20;
        if (RL.RELIABLE_FOR_VREF                         ==1) i|=0x40;
        if (RR.RELIABLE_FOR_VREF                         ==1) i|=0x80;
        CAN_LOG_DATA[77] =(uint8_t) (i);                                   //59

        i=0;
        if (0                          ==1) i|=0x01;
        if (0                        ==1) i|=0x02;
        if (ldddshu1RefTirePressureV                       ==1) i|=0x04;
        if (ldddsu1DeflationState                     ==1) i|=0x08;
        if (ldddsu1CompleteCalib                      ==1) i|=0x10;
        if (ldddsu1StdStrDrvStateForDDS      ==1) i|=0x20;
        if (ldddsu1PerformedDDSReset                        ==1) i|=0x40;
        if (ldddsu1RequestDDSReset                        ==1) i|=0x80;
        CAN_LOG_DATA[78] =(uint8_t) (i);                                   //60

	    

        i=0;

        if(HV_VL_fl                         ==1) i|=0x01;
        if(AV_VL_fl                         ==1) i|=0x02;
        if(HV_VL_fr                         ==1) i|=0x04;
        if(AV_VL_fr                         ==1) i|=0x08;
        if(HV_VL_rl                         ==1) i|=0x10;
        if(AV_VL_rl                         ==1) i|=0x20;
        if(HV_VL_rr                         ==1) i|=0x40;
        if(AV_VL_rr                         ==1) i|=0x80;

        CAN_LOG_DATA[79] =(uint8_t) (i);                                   //63
    				
}
/*===========================================================*/
/*
void	LSDDS_vCallDDSPlusLogDataWL(uint8_t WHL,uint8_t addr,uint8_t BufferOffset)
{
	uint8_t i,j;
	uint8_t addr2;

	for (i=0;i<30;i++)
	{
		j=i+BufferOffset;
		addr2=addr+(i*2);
		CAN_LOG_DATA[addr2]=(uint8_t)(StoredPeriod[WHL][j]);
		CAN_LOG_DATA[addr2+1]=(uint8_t)(StoredPeriod[WHL][j]>>8);
	}

}
*/
/*===========================================================*/
	#endif
#endif
/*
void PeriodCOunt(void)
{
	static uint8_t TeethCount,Count;
	static uint16_t PeriodBuffer[50];
	uint8_t TempCount,i,k;
	
	TempCount=TeethCount;
	TeethCount+=TestCount2;
	Count=0;
	for(i=TempCount; i<TeethCount; i++)
	{
		PeriodBuffer[i]=PeriodTIme[Count];
		Count++;
		if(i==U8_TEETH) 
		{
			OneRevolEnd=1;
			break;
		}
	}
	TestCount2=0;
}
*/
/***********************************************************************************/
/***********************************************************************************/
//static uint16_t lddu16WheelBufferFL[50],lddu16WheelBufferFR[50],lddu16WheelBufferRL[50],lddu16WheelBufferRR[50];
//static uint8_t  TeethCountFL,TeethCountFR,TeethCountRL,TeethCountRR;

/*
	uint16_t lddssu16TimePerPulseBuffer[4][50];  //1바퀴
	uint8_t  lddu8TeethCnt[4];	
	uint16_t lddssu16TimePerPulseBuffer2[4][10];
	
void LDDDS_u8PeriodTimeCheck(uint8_t WHL ,uint8_t TEETH_NUM)
{
	uint8_t TempCount,i,k,Count;
//	TIE_ECT&=(~WheelMask);	
	if(WHL==FL_WL)
	{
		wbSped_ptr=&wbSped_fl;
	}
	else if(WHL==FR_WL)
	{
		wbSped_ptr=&wbSped_fr;
	}
	else if(WHL==RL_WL)
	{
		wbSped_ptr=&wbSped_rl;
	}		
	else if(WHL==RR_WL)
	{
		wbSped_ptr=&wbSped_rr;
	}
	else
	{
		;
	}	
	ldddsu8ElapsedTimeUpdateFlg[WHL]=0;
	ldddsu1SecondBufferCharged=0;
	TempCount=ldddsu8TeethCnt[WHL];
	ldddsu8TeethCnt[WHL]+=wbSped_ptr->u8PeriodTimeCount2;
	
	Count=0;
	k=0;
	for(i=TempCount; i<ldddsu8TeethCnt[WHL]; i++)
	{
		if(i==0)
		{
			ldddsu16ElapsedTimePerRev[WHL]=0;
		}
		if(i<TEETH_NUM)
		{
			lddssu16TimePerPulseBuffer[WHL][i]=wbSped_ptr->u16PeriodTimeBuffer[Count];
			Count++;	
			ldddsu16ElapsedTimePerRev[WHL]+=(lddssu16TimePerPulseBuffer[WHL][i]/10);	
		} 
		else if(i==TEETH_NUM) 
		{
			lddssu16TimePerPulseBuffer[WHL][i]=wbSped_ptr->u16PeriodTimeBuffer[Count];
			Count++;
			
			ldddsu8ElapsedTimeUpdateFlg[WHL]=1;
		}
		else
		{
			ldddsu8SecondBufferCharged=1;
			lddssu16TimePerPulseBuffer2[WHL][k]=wbSped_ptr->u16PeriodTimeBuffer[Count];
			Count++;
			k++;				
		}
	}	
	
	if(ldddsu1SecondBufferCharged==1)
	{
		for(i=0;i<k;i++)
		{
			wbSped_ptr->u16PeriodTimeBuffer[i]=lddssu16TimePerPulseBuffer2[WHL][i];
		}
		wbSped_ptr->u8PeriodTimeCount2=k;
	}	
	else
	{
		wbSped_ptr->u8PeriodTimeCount2=0;
	}
	
	if(ldddsu8ElapsedTimeUpdateFlg[WHL]==1)
	{
		ldddsu8TeethCnt[WHL]=0;
	}			
	
//	TIE_ECT|=(WheelMask);	
}

/***********************************************************************************/
/***********************************************************************************/



















#endif //__DDS	
