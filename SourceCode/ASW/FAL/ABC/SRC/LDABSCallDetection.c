/******************************************************************************
* Project Name: Anti-lock Braking System
* File: LDABSCallDetection.C
* Description: Detection & Estimation for ABS control
* Date: November. 21. 2005
******************************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDABSCallDetection
	#include "Mdyn_autosar.h"
#endif
/* Includes ******************************************************************/
#include "LDABSCallDetection.h"
#include "LDABSDctVehicleStatus.h"
#include "LDABSDctRoadSurface.h"
#include "LDABSDctRoadGradient.h"
#include "LDABSDctWheelStatus.h"
/* Local Definiton  **********************************************************/


/* Variables Definition*******************************************************/


/* LocalFunction prototype ***************************************************/



/* GlobalFunction prototype **************************************************/
extern void	LDABS_vCallVrefEst(void);
extern void LDABS_vCallMainSideVref(void);
extern void	LDABS_vEstWheelSlip(void);
extern void LDRTA_vCallDetection(void);

/* Implementation*************************************************************/



/******************************************************************************
* FUNCTION NAME:      LDABS_vCallDetection
* CALLED BY:          LD_vCallMainDetection()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        Detection & Estimation for ABS control
******************************************************************************/
void LDABS_vCallDetection(void)
{
	LDRTA_vCallDetection();
	LDABS_vCallVrefEst();
	LDABS_vCallMainSideVref();
	LDABS_vEstWheelSlip();
	LDABS_vDctWheelStatus();
	LDABS_vDctVehicleStatus();
	LDABS_vDctRoadSurface();
	

}

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDABSCallDetection
	#include "Mdyn_autosar.h"
#endif
