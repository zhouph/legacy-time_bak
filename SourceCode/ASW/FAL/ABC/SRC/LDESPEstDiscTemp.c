/*******************************************************************************
* Project Name:     MGH60_ESP
* File Name:        LDESPEstDiscTemp.c
* Description:      Detection Disk Temperature
* Logic version:    HV26
********************************************************************************
*  Modification Log
*  Date         Author          Description
*  120127       SeWoongKim      Initial Release
*  
********************************************************************************/

/* Includes ********************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDESPEstDiscTemp
	#include "Mdyn_autosar.h"
#endif

#include "LDESPEstDiscTemp.h"
#include "LCESPCalInterpolation.h"
#include "LDABSDctWheelStatus.h"
#include "LSENGCallSensorSignal.h"




/* Local Definition  ***********************************************/
#define TEMPT_RESOL                    32          /* 1[��] -> 32 */
#define TEMPT_AVERAGE_TIME             T_1000_MS   /* Calculation time, ex)TEMPT_AVERAGE_TIME=100 ==> Calculation time=1s */

#define S16_Limit_Inputpress_LowSpeed    VREF_50_KPH  /* X1, At BTCS, Max input press */
#define S16_Limit_Inputpress_HighSpeed   VREF_70_KPH  /* X2, At BTCS, Max input press */
#define S16_Limit_Inputpress_HighPress   MPRESS_70BAR /* Y1, At BTCS, Max input press */
#define S16_Limit_Inputpress_LowPress    MPRESS_50BAR /* Y2, At BTCS, Max input press */

/*------- RaynoldsNumber INTERPOLATION ---------------------------------------------------------
  RaynoldsNumber = SQRT(SPEED*WHEEL_RADIUS/Coefficient of kinematic viscosity)
                 = Raynolds_SQRT(SPEED/Coefficient of kinematic viscosity) * Raynolds_SQRT(WHEEL_RADIUS)
----------------------------------------------------------------------------------------------*/

/* X = SPEED*8(Resolution), Y = SQRT(SPEED/Coefficient of kinematic viscosity) */
#define  Raynolds_SQRT_X_10KPH  			  80
#define  Raynolds_SQRT_X_15KPH  	     120
#define  Raynolds_SQRT_X_20KPH  		   160
#define  Raynolds_SQRT_X_25KPH  		   200
#define  Raynolds_SQRT_X_30KPH  		   240
#define  Raynolds_SQRT_X_35KPH         280
#define  Raynolds_SQRT_X_40KPH         320
#define  Raynolds_SQRT_X_50KPH         400
#define  Raynolds_SQRT_X_60KPH   	     480
#define  Raynolds_SQRT_X_70KPH   		   560
#define  Raynolds_SQRT_X_80KPH         640
#define  Raynolds_SQRT_X_100KPH   	   800
#define  Raynolds_SQRT_X_120KPH  		   960
#define  Raynolds_SQRT_X_150KPH  		  1200
#define  Raynolds_SQRT_X_200KPH  		  1600
#define  Raynolds_SQRT_X_270KPH   	  2160

#define  Raynolds_SQRT_Y_10KPH         447
#define  Raynolds_SQRT_Y_15KPH   		   547
#define  Raynolds_SQRT_Y_20KPH   	     631
#define  Raynolds_SQRT_Y_25KPH   	     707
#define  Raynolds_SQRT_Y_30KPH   	     774
#define  Raynolds_SQRT_Y_35KPH   	     836
#define  Raynolds_SQRT_Y_40KPH         894
#define  Raynolds_SQRT_Y_50KPH   	     999
#define  Raynolds_SQRT_Y_60KPH        1095
#define  Raynolds_SQRT_Y_70KPH 		    1182
#define  Raynolds_SQRT_Y_80KPH        1264
#define  Raynolds_SQRT_Y_100KPH       1413
#define  Raynolds_SQRT_Y_120KPH  	    1548
#define  Raynolds_SQRT_Y_150KPH  	    1731
#define  Raynolds_SQRT_Y_200KPH  	    1999
#define  Raynolds_SQRT_Y_270KPH  	    2322

/* X = WHEEL_RADIUS*1000(Resolution), Y = SQRT(WHEEL_RADIUS)*1000(Resolution) */
#define  Raynolds_SQRT_X_0P25    			 250
#define  Raynolds_SQRT_X_0P29   	     290
#define  Raynolds_SQRT_X_0P31     	   310
#define  Raynolds_SQRT_X_0P33      	   330
#define  Raynolds_SQRT_X_0P35     	   350
#define  Raynolds_SQRT_X_0P39          390

#define  Raynolds_SQRT_Y_0P25          500
#define  Raynolds_SQRT_Y_0P29    	     539
#define  Raynolds_SQRT_Y_0P31   	     557
#define  Raynolds_SQRT_Y_0P33   		   575
#define  Raynolds_SQRT_Y_0P35          592
#define  Raynolds_SQRT_Y_0P39   	     633


/*******************************************************************/



/*******************************************************************/
/* Variables Definition*********************************************/


INT    ldesps16OutsideAirTempt ;
UCHAR  ldespu8OutsideAirTemptV ;
UCHAR  ldespu8OutsideAirTemptM ;

#if	(__BTEM == ENABLE)

UCHAR  ldespu8TemptCalcCnt ;

struct WL_TEMPT *TEMPT, FL_TEMPT, FR_TEMPT , RL_TEMPT, RR_TEMPT;
U8_BIT_STRUCT_t  TEMPT_0;
/*******************************************************************/

/* Local Function prototype ****************************************************/

void  LDESPEstDiscTempMain(void);
void  LDESPEstDiscTempFM(struct WL_TEMPT *TEMPT_temp ,struct W_STRUCT *WL_temp);
void  LDESPEstDiscTempPressInput(struct WL_TEMPT *TEMPT_temp ,struct W_STRUCT *WL_temp);
void  LDESPEstDiscTempWheel(struct WL_TEMPT *TEMPT_temp ,struct W_STRUCT *WL_temp , INT  );

/* Implementation **************************************************************/

/*******************************************************************************
* FUNCTION NAME:        LDESPEstDiscTemp
* CALLED BY:            LDESPCallDetection()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Estimation of Disc Temperature
********************************************************************************/

void LDESPEstDiscTempMain(void)
{
	INT FRONT_WHEEL;
	
	LDESPEstDiscTempFM(&FL_TEMPT ,&FL);
	LDESPEstDiscTempFM(&FR_TEMPT ,&FR);
	LDESPEstDiscTempFM(&RL_TEMPT ,&RL);
	LDESPEstDiscTempFM(&RR_TEMPT ,&RR);
	LDESPEstDiscTempPressInput(&FL_TEMPT ,&FL);
	LDESPEstDiscTempPressInput(&FR_TEMPT ,&FR);
	LDESPEstDiscTempPressInput(&RL_TEMPT ,&RL);
	LDESPEstDiscTempPressInput(&RR_TEMPT ,&RR);
	
	if(ldespu8TemptCalcCnt == TEMPT_AVERAGE_TIME )
	{
		ldespu8TemptCalcCnt = 0 ;
		
		/*--- Disc Temperature Calculation ---*/		
	    FRONT_WHEEL = 1;
	    LDESPEstDiscTempWheel(&FL_TEMPT ,&FL , FRONT_WHEEL);
	    LDESPEstDiscTempWheel(&FR_TEMPT ,&FR , FRONT_WHEEL);
	    
	    FRONT_WHEEL = 0;
	    LDESPEstDiscTempWheel(&RL_TEMPT ,&RL , FRONT_WHEEL);
	    LDESPEstDiscTempWheel(&RR_TEMPT ,&RR , FRONT_WHEEL);
  }
  else
  {    	    	
    	if(ldespu8TemptCalcCnt == 0)
    	{
    		FL_TEMPT.ldesps16Vrad1secOld = vrad_fl ;
    		FR_TEMPT.ldesps16Vrad1secOld = vrad_fr ;
    		RL_TEMPT.ldesps16Vrad1secOld = vrad_rl ;
    		RR_TEMPT.ldesps16Vrad1secOld = vrad_rr ;
    		
    		FL_TEMPT.ldesps16PressSumBar = 0;
    		FR_TEMPT.ldesps16PressSumBar = 0;
    		RL_TEMPT.ldesps16PressSumBar = 0;
    		RR_TEMPT.ldesps16PressSumBar = 0;
    	}
    	else
    	{
    		;
    	}
    	
    	FL_TEMPT.ldesps16PressSumBar = FL_TEMPT.ldesps16PressSumBar + (FL_TEMPT.ldesps16PressInputBar/10) ;
    	FR_TEMPT.ldesps16PressSumBar = FR_TEMPT.ldesps16PressSumBar + (FR_TEMPT.ldesps16PressInputBar/10) ;
    	RL_TEMPT.ldesps16PressSumBar = RL_TEMPT.ldesps16PressSumBar + (RL_TEMPT.ldesps16PressInputBar/10) ;
    	RR_TEMPT.ldesps16PressSumBar = RR_TEMPT.ldesps16PressSumBar + (RR_TEMPT.ldesps16PressInputBar/10) ;
    	
    	ldespu8TemptCalcCnt = ldespu8TemptCalcCnt + 1 ; 
  }
	
}

void LDESPEstDiscTempFM(struct WL_TEMPT *TEMPT_temp ,struct W_STRUCT *WL_temp)
{
	TEMPT = TEMPT_temp;
	WL = WL_temp;
	
    if ((WL->WhlErrFlg == 1) || (EST_WHEEL_PRESS_OK_FLG==0))
	{
       TEMPT->ldespu1TemptEstInhibit = 1;
	}
	else
	{
       TEMPT->ldespu1TemptEstInhibit = 0;
	}
	}

void LDESPEstDiscTempPressInput(struct WL_TEMPT *TEMPT_temp ,struct W_STRUCT *WL_temp)
{
	TEMPT_temp->ldesps16PressInputBar=WL_temp->s16_Estimated_Active_Press;

	if(BTCS_ON==1)
	{
		tempW0 = LCESP_s16IInter2Point(vref, S16_Limit_Inputpress_LowSpeed, S16_Limit_Inputpress_HighPress,
											 S16_Limit_Inputpress_HighSpeed, S16_Limit_Inputpress_LowPress);
		if (TEMPT_temp->ldesps16PressInputBar>tempW0)
		{
			TEMPT_temp->ldesps16PressInputBar=tempW0;
		}
		else
		{
			;
		}
	}
	else
	{
		;
	}
}

void  LDESPEstDiscTempWheel(struct WL_TEMPT *TEMPT_temp, struct W_STRUCT *WL_temp,  INT FRONT_WHEEL )
{
	INT s16_BRAKE_PAD_AREA_temp;
	INT s16_BRAKE_PAD_FRICTION_temp;
	INT s16_BRAKE_DISK_DIAMETER_temp;
	INT s16_BRAKE_DISK_MASS_temp;		
	INT s16_BRAKE_DISK_RATIO_temp;
	INT s16_BRAKE_THERM_EFFIC_temp;
	INT s16_BRAKE_COOLING_COEFFIC_temp; 
	INT s16_BRAKE_DISK_AREA_temp;	
	INT s16_STOP_STATE_CONV_COEFFIC_temp;

	INT s16WheelSpeed1secMean;
	INT s16FrictionForce;
	INT s16RollingDistance;
	INT s16Brake_ConvectionCoeffic;
	INT s16Brake_RaynoldsNumber;

	TEMPT = TEMPT_temp;
	WL = WL_temp;

	if(FRONT_WHEEL==1)
	{
		s16_BRAKE_PAD_AREA_temp          = S16_BRAKE_PAD_AREA_F ;
		s16_BRAKE_PAD_FRICTION_temp      = S16_BRAKE_PAD_FRICTION_F;
		s16_BRAKE_DISK_DIAMETER_temp     = S16_BRAKE_DISK_DIAMETER_F;
		s16_BRAKE_DISK_MASS_temp         = S16_BRAKE_DISK_MASS_F;
		s16_BRAKE_DISK_RATIO_temp        = (INT)(((LONG)S16_BRAKE_DISK_DIAMETER_F*100)/S16_TIRE_DIAMETER);  /* 100 = s16RollingDistance resolution (m ==> cm) */
		s16_BRAKE_THERM_EFFIC_temp       = S16_BRAKE_THERM_EFFIC_F;
		s16_BRAKE_COOLING_COEFFIC_temp   = S16_BRAKE_COOLING_COEFFIC_F;
		s16_BRAKE_DISK_AREA_temp         = S16_BRAKE_DISK_AREA_F;
		s16_STOP_STATE_CONV_COEFFIC_temp = S16_STOP_STATE_CONV_COEFFIC_F;
	}
	else
	{
		s16_BRAKE_PAD_AREA_temp          = S16_BRAKE_PAD_AREA_R ;
		s16_BRAKE_PAD_FRICTION_temp      = S16_BRAKE_PAD_FRICTION_R;
		s16_BRAKE_DISK_DIAMETER_temp     = S16_BRAKE_DISK_DIAMETER_R;
		s16_BRAKE_DISK_MASS_temp         = S16_BRAKE_DISK_MASS_R;		
    s16_BRAKE_DISK_RATIO_temp        = (INT)(((LONG)S16_BRAKE_DISK_DIAMETER_R*100)/S16_TIRE_DIAMETER) ;	/* 100 = s16RollingDistance resolution (m ==> cm) */
    s16_BRAKE_THERM_EFFIC_temp       = S16_BRAKE_THERM_EFFIC_R;	
		s16_BRAKE_COOLING_COEFFIC_temp   = S16_BRAKE_COOLING_COEFFIC_R;
		s16_BRAKE_DISK_AREA_temp         = S16_BRAKE_DISK_AREA_R;
		s16_STOP_STATE_CONV_COEFFIC_temp = S16_STOP_STATE_CONV_COEFFIC_R;
	}
	
	s16WheelSpeed1secMean      =    (INT)( ( ((LONG)TEMPT->ldesps16Vrad1secOld) + WL->vrad )/2 ) ;
	/* average speed in (TEMPT_AVERAGE_TIME/T_1000_MS) : default 1s */

	TEMPT->ldesps16PressEstBar =    (TEMPT->ldesps16PressSumBar/TEMPT_AVERAGE_TIME  );
	
	if(BTCS_ON==1)
	{
		tempW0 = LCESP_s16IInter2Point(vref, S16_Limit_Inputpress_LowSpeed, S16_Limit_Inputpress_HighPress,
											 S16_Limit_Inputpress_HighSpeed, S16_Limit_Inputpress_LowPress);
		tempW1=tempW0/10;
		if (TEMPT->ldesps16PressEstBar>tempW1)
		{
			TEMPT->ldesps16PressEstBar = tempW1;
		}
		else
		{
			;
		}
	}
	else
	{
		;
	}

	/* average estimated pressure in (TEMPT_AVERAGE_TIME/T_1000_MS) : default 1s */
	
	/* Initial temperature consider */


    /*=====================  Heating Mode Calculation  ========================

                (N/10)*(RollDist)*32           (N/10)*(RollDist)*32
        dT =  ------------------------  =  -----------------------------
                 (c/10)*m*(100)                      c*m*10
                 
                 (N/10)*(RollDist)*(32/10)           (32*N/100)*(RollDist)
           =  ------------------------------  =  -----------------------------
                         c*m                               c*m

    =========================================================================*/


	s16FrictionForce           = (INT)(((LONG)(TEMPT->ldesps16PressEstBar) * s16_BRAKE_PAD_AREA_temp * s16_BRAKE_PAD_FRICTION_temp)/10); /* N */
  /* Friction Force(N) = Wheel Pressure(bar) * Conversion Unit(N/(m^2*bar)) 100000 * Pad Area(cm^2) * Conversion Unit(m^2/cm^2) 1/10000 * Friction Coefficient(%)/100 */
  /* 100000 * 1/10000 * 1/100 = 1/10 */

	tempW0                     = T_1000_MS;
	tempW1                     = TEMPT_AVERAGE_TIME;
	tempW2                     = s16_BRAKE_DISK_RATIO_temp;
	tempW3                     = (INT)(((LONG)tempW1*tempW2)/tempW0 ) ;
  s16RollingDistance         = (INT)( ((LONG)s16WheelSpeed1secMean*tempW3*5)/144 );    /* cm */
  /* RollingDistance(cm) = average speed(kph) * resolution(8*kph ==> kph) 1/8 * Conversion Unit((m/s) / kph) 1/3.6 * calculation time(tempW1/tempW0) * BRAKE_DISK_RATIO */
  /* (1/8) * (1/3.6) = (5/144) */


	tempW1                     = (INT)(((LONG)s16FrictionForce*TEMPT_RESOL)/100 ) ;
	tempW0                     = S16_BRAKE_DISK_SPECIFIC_HEAT*s16_BRAKE_DISK_MASS_temp ;
	tempW2                     = (INT)( ((LONG)tempW1*s16RollingDistance)/tempW0 ) ;


	TEMPT->ldesps16TemptUprate = (INT)( ((LONG)tempW2*s16_BRAKE_THERM_EFFIC_temp)/100 ) ;
  /* TemptUprate(32*��) = Friction Force(N) * resolution(�� ==> 32*��) * Conversion Unit(m / cm) 1/100 * RollingDistance(cm) / (SPECIFIC_HEAT(N*m/(kg*��)) * DISK_MASS(kg)) */


    /*====================  Cooling Mode Calculation  ===============================
               (A*10000)*ConvectionCoeffic*(32*(DiskTempT-AmbientTempT))
        dT = -------------------------------------------------------------
                                      c*m*1000
                 
                               (Gamma*10000)*(CoolingCoeffic*10)*RaynoldsNumber
        ConvectionCoeffic =  ----------------------------------------------------
                                                ((r*1000)/(100))
    ===============================================================================*/
    
    
   tempW0 = LCESP_s16IInter6Point( S16_WHEEL_RADIUS, Raynolds_SQRT_X_0P25, Raynolds_SQRT_Y_0P25,
                                                     Raynolds_SQRT_X_0P29, Raynolds_SQRT_Y_0P29,
                                                     Raynolds_SQRT_X_0P31, Raynolds_SQRT_Y_0P31,
                                                     Raynolds_SQRT_X_0P33, Raynolds_SQRT_Y_0P33,
                                                     Raynolds_SQRT_X_0P35, Raynolds_SQRT_Y_0P35,
                                                     Raynolds_SQRT_X_0P39, Raynolds_SQRT_Y_0P39);
   /* tempW0 = SQRT(WHEEL_RADIUS(m)) * Resolution(m ==> mm) */

  if(WL->vrad < VREF_12_KPH)
  {
    ;
  }
	else if(WL->vrad <= VREF_35_KPH)
  {
    tempW1 = LCESP_s16IInter6Point( WL->vrad, Raynolds_SQRT_X_10KPH, Raynolds_SQRT_Y_10KPH,
                                              Raynolds_SQRT_X_15KPH, Raynolds_SQRT_Y_15KPH,
                                              Raynolds_SQRT_X_20KPH, Raynolds_SQRT_Y_20KPH,
                                              Raynolds_SQRT_X_25KPH, Raynolds_SQRT_Y_25KPH,
                                              Raynolds_SQRT_X_30KPH, Raynolds_SQRT_Y_30KPH,
                                              Raynolds_SQRT_X_35KPH, Raynolds_SQRT_Y_35KPH);
    /* tempW1 = SQRT(SPEED(m/s)/Coefficient of kinematic viscosity(m^2/s)) */
    s16Brake_RaynoldsNumber = (INT)(((LONG)tempW0 * tempW1)/1000);
  }
  else if(WL->vrad <= VREF_80_KPH)
  {
    tempW1 = LCESP_s16IInter6Point( WL->vrad, Raynolds_SQRT_X_35KPH, Raynolds_SQRT_Y_35KPH,
                                              Raynolds_SQRT_X_40KPH, Raynolds_SQRT_Y_40KPH,
                                              Raynolds_SQRT_X_50KPH, Raynolds_SQRT_Y_50KPH,
                                              Raynolds_SQRT_X_60KPH, Raynolds_SQRT_Y_60KPH,
                                              Raynolds_SQRT_X_70KPH, Raynolds_SQRT_Y_70KPH,
                                              Raynolds_SQRT_X_80KPH, Raynolds_SQRT_Y_80KPH);
    s16Brake_RaynoldsNumber = (INT)(((LONG)tempW0 * tempW1)/1000);
  }
  else
  {
    tempW1 = LCESP_s16IInter6Point( WL->vrad, Raynolds_SQRT_X_80KPH,  Raynolds_SQRT_Y_80KPH,
                                              Raynolds_SQRT_X_100KPH, Raynolds_SQRT_Y_100KPH,
                                              Raynolds_SQRT_X_120KPH, Raynolds_SQRT_Y_120KPH,
                                              Raynolds_SQRT_X_150KPH, Raynolds_SQRT_Y_150KPH,
                                              Raynolds_SQRT_X_200KPH, Raynolds_SQRT_Y_200KPH,
                                              Raynolds_SQRT_X_270KPH, Raynolds_SQRT_Y_270KPH);
    s16Brake_RaynoldsNumber = (INT)(((LONG)tempW0 * tempW1)/1000);
  }


  if(WL->vrad >= VREF_12_KPH)
  {
  tempW0                     = (INT)(((LONG)s16_BRAKE_COOLING_COEFFIC_temp*s16Brake_RaynoldsNumber)/10);
  /* 1/10 = BRAKE_COOLING_COEFFIC resolution */

  s16Brake_ConvectionCoeffic = (INT)(((LONG)tempW0*S16_AIR_CONDUCTIVITY)/(S16_WHEEL_RADIUS*10));
  /* 1/10 = 1/10000(conductivity of air resolution) * 1000(WHEEL_RADIUS resolution) */ 
  }
  else
  {
  s16Brake_ConvectionCoeffic = s16_STOP_STATE_CONV_COEFFIC_temp;
  /* stop state cooling ConvectionCoeffic = 55 */ 
  }
  
	#if __GM_VEHICLE
			if((ldespu8OutsideAirTemptV==0)&&(ldespu8OutsideAirTemptM==1))
			{
				;
			}
			else
			{
				ldesps16OutsideAirTempt = 640;
			}
	#else
			ldesps16OutsideAirTempt = 640;         /* 20[��] -> 640 */
	#endif 
  tempW0                     = S16_BRAKE_DISK_SPECIFIC_HEAT*s16_BRAKE_DISK_MASS_temp ;
  tempW1                     = TEMPT->ldesps16Tempt - ldesps16OutsideAirTempt; /* Resolution 32 */
  
  TEMPT->ldesps16TemptDownrate = (INT)((((LONG)tempW1*s16Brake_ConvectionCoeffic/100)*(s16_BRAKE_DISK_AREA_temp/10))/tempW0);

  /* 1/1000 = Downrate resolution include 32 resolution(10) / BRAKE_DISK_AREA Resolution(cm^2 -> m^2 10000) */
  /* Downrate resolution = 32*10 */
    

  /* Original cooling
  if(TEMPT->ldesps16TemptUprate < (TEMPT_RESOL*2) )
  {
    tempW0 = ( TEMPT->ldesps16Tempt/32 );
    if(tempW0 >= (INT)450)       
    {
    	tempW1 = 1;
    }
    else if(tempW0 >= (INT)350)
    {
    	tempW1 = 2;
    }
    else if(tempW0 >= (INT)250)
    {
    	tempW1 = 3;
    }
    else if(tempW0 >= (INT)170)
    {
    	tempW1 = 4;
    }
    else if(tempW0 >= (INT)110)
    {
    	tempW1 = 5;
    }
    else if(tempW0 >= (INT)70)
    {
    	tempW1 = 6;
    }
    else if(tempW0 >= (INT)40)
    {
    	tempW1 = 7;
    }
    else
    {
    	tempW1 = 8;
    }

    if(vref <= (INT)VREF_10_KPH)
    {
        tempW1 = tempW1*4;
        tempW1 += 4;
    }
    else if(vref <= (INT)VREF_30_KPH)
    {
        tempW1 = tempW1*2;
        tempW1 += 2;
    }
    else if(vref <= (INT)VREF_60_KPH)
    {
    	tempW1 += 1;
    }
    else if(vref <= (INT)VREF_90_KPH)
    {
        tempW1 = (tempW1/2);
        tempW1 += 1;
    }
    else
    {
    	tempW1 = (tempW1/2);
    }
    tempW1 += 5;
    
    TEMPT->ldesps16TemptDownrate = 7*tempW0/tempW1*10;
  }
  
  TEMPT->ldesps16Tempt = TEMPT->ldesps16Tempt  + TEMPT->ldesps16TemptUprate  - TEMPT->ldesps16TemptDownrate;
  Original cooling */

  TEMPT->ldesps32TemptTemp = (((LONG)(TEMPT->ldesps16Tempt)*10)  +  ((LONG)(TEMPT->ldesps16TemptUprate)*10))  - (LONG)(TEMPT->ldesps16TemptDownrate);	
  
	if (TEMPT -> ldespu1TemptEstInhibit == 0)
	{
		  TEMPT->ldesps16Tempt     = (INT)(TEMPT->ldesps32TemptTemp/10) ;
	}
	else
	{
		  TEMPT->ldesps16Tempt     = 640;
	}
		
		
	tempW2 = K_Brake_temperature_Overheated_Threshold * 128;	    /* resolution 1/32 */
	tempW3 = K_Brake_temperature_Overheated_Threshold_Low * 128;	/* resolution 1/32 */	
    if(TEMPT->ldesps16Tempt > tempW2)
    {
        WL->TMP_ERROR = 1;
        
        if(TEMPT->ldesps16Tempt > (int16_t)TMP_980)
        {
            TEMPT->ldesps16Tempt = (int16_t)TMP_980;
        }
        else
        {
            ;
        }
    }
    else if(TEMPT->ldesps16Tempt < tempW3)
    {
        WL->TMP_ERROR = 0;
    }
    else
    {
        ;
    }	
}

#endif /* #if	(__BTEM == ENABLE) */

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDESPEstDiscTemp
	#include "Mdyn_autosar.h"
#endif
