/*******************************************************************/
/*  Program ID: MGH-40 ABS                                         */
/*  Program description: Motor Speed Control for ABS/TCS/ESP/VAF's */
/*  Input files:                                                   */
/*                                                                 */
/*  Output files:                                                  */
/*                                                                 */
/*  Special notes:                                                 */
/*******************************************************************/
/*  Modification   Log                                             */
/*  Date           Author          Description                     */
/*  -------       ----------      ---------------------------      */
/*  02.06.20      JinKoo Lee      Initial Release                  */
/*  05.11.23      JinKoo Lee      Modified for MISRA rule          */
/*  06.01.19      yongkil Kim     Advance MSC                      */
/*  06.02.09      yongkil Kim     Modified for Advance MSC         */
/*******************************************************************/

/* Includes ********************************************************/

/* AUTOSAR --------------------------*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START                       
	#define idx_FILE	idx_CL_LAABSCallMotorSpeedControl
	#include "Mdyn_autosar.h"               
#endif
/* AUTOSAR --------------------------*/

#include "LAABSCallMotorSpeedControl.h"
#include "LCABSDecideCtrlState.h"
#include "LCEBDCallControl.h"
#include "LCABSCallControl.h"
#include "LCCBCCallControl.h"

#if __VDC
#include "LAESPCallActHW.h"
#include "LCPBACallControl.h"
#include "LAESPActuateNcNovalveF.h"
#include "LCESPInterfaceSlipController.h"
#include "LABACallActHW.h"
#endif

#if __HDC
#include "LCHDCCallControl.h"
#include "LAHDCCallActHW.h"
#endif

#if __ACC
#include "LCACCCallControl.h"
#include "LAACCCallActHW.h"
#endif

#if __DEC
#include "LCDECCallControl.h"
#include "LADECCallActHW.h"
#endif

#if __EPB_INTERFACE
#include "LCEPBCallControl.h"
#include "LAEPBCallActHW.h"
#endif



#if __BDW
#include "LCBDWCallControl.h"
#include "LABDWCallActHW.h"
#endif

#if __EBP
#include "LCEBPCallControl.h"
#endif

#if __FBC
#include "LCFBCCallControl.h"
#endif

#if __SLS
#include "LCSLSCallControl.h"
#endif

#if __SCC
#include "../SRC/SCC/LCWPCCallControl.h"
#include "../SRC/SCC/LCSCCCallControl.h"
#endif

#if __AVH
#include "LCAVHCallControl.h"
#include "LAAVHCallActHW.h"
#endif

#if __SBC
#include "LASPASCallActHW.h"
#include "LCSPASCallControl.h"
#endif

#if __HBB
#include "LCHBBCallControl.h"
#endif

#if __TVBB
#include "LCTVBBCallControl.h"
#endif

#if __HRB
#include "LCHRBCallControl.h"
#endif

#if __HSA
#include "LCHSACallControl.h"
#include "LAHSACallActHW.h"
#endif

#include "LCESPCalInterpolation.h"
#include "LCESPCalLpf.h"
#include "LCallMain.h"
#include "LAABSCallHWTest.h"
#include "LCABSCallControl.h"
#include "LABTCSCallActHW.h"
/* Local Definition  ***********************************************/
/* 1 : ON    0 : Off    */
/* #define     __MOTOR_MON                 0         1MSEC MSC Mon   */

#define     __LPA_TEST                  0       /* ABS 제어 중 3kph 이상에서 모터 구동안함. */

#define ESC_VV_TEST_MOTOR_TARGET_VOLT   MSC_14_V/*(int16_t)S16_Slip_Gain_BFAFZOK_B_10_F */
#define S16_MPRESS_OFF_TIME     (MPRESS_BRAKE_ON_off_cnt/L_CONTROL_PERIOD)

/* Variables Definition*******************************************************/

U8_BIT_STRUCT_t MSC00;
U8_BIT_STRUCT_t MSC01;
U8_BIT_STRUCT_t MSC02;
U8_BIT_STRUCT_t MSC03;
U8_BIT_STRUCT_t MSC04;
U8_BIT_STRUCT_t MSC05;
U8_BIT_STRUCT_t MSC06;
MSC_FLAG_t lamscCtrlFlg0;

int16_t MSC_ref_voltage;
int16_t MSC_indicator;

int16_t MSC_primary_indicator;    /*    required value of FL & RR line  */
int16_t MSC_secondary_indicator;  /*    required value of FR & RL line  */
  #if (__BEMF_LPF_ENABLE == ENABLE)
int16_t	laabs16MtpAdFiltered;
  #endif

static uint8_t First_Dump_counter_after_BLS;


static int16_t s16mscebdmotoff_cnt;
static uint16_t lau16MSC_tempW1,lau16MSC_tempW0;


#if __ADVANCED_MSC

static uint8_t MOT_ON_FLG_counter_old3;
static uint8_t MOT_ON_FLG_counter_old2;
static uint8_t MOT_ON_FLG_counter_old;
uint8_t MOT_ON_FLG_counter;
static int8_t MOT_ON_FLG_timer;
static int8_t MOT_ON_FLG_timer2;
static int8_t Wheel_Dump_timer;
/*static int8_t bump_low_speed_motor_stop;*/
static int16_t Sum_of_motor_msec_on_time;
//static int16_t add_Sum_of_motor_msec_on_time;
int8_t additional_abs_motor_on;
static int16_t Motor_Drv_Duty_130ms;
#if __VDC
static int16_t lcu16CalVoltBatt2Lf, lcu16CalVoltBatt2LfOld;
#else
static int16_t ref_mon_ad_lf,ref_mon_ad_lf_old;
#endif /*__VDC*/
static int16_t ABS_fz_on_cnt;
static int16_t ABS_fz_off_cnt;

/*  motor rpm estimation    */
static int16_t mot_mon_ad_lf;

static int16_t motor_speedup_rate;
static int16_t max_rpm,max_rpm_ad;

static int16_t motor_on_time_add_cnt;
static int16_t dump_cnt;

#endif

  #if (__DELAY_MOTOR_DRV_AT_LOW_MP == ENABLE)
    static uint8_t lcmscu8LPAFillCounterP;
    static uint8_t lcmscu8LPAFillCounterS;
    static uint8_t lcmscu8BrakingTimeBfABS;
    static int16_t lamscs16DelayMtrDriveInABSTime;
  #endif

static int16_t msc_tempW0,/*msc_tempW1,msc_tempW2,msc_tempW3, */msc_tempW4;
static int16_t /*msc_tempW5,msc_tempW6,*/msc_tempW7,msc_tempW8,msc_tempW9;

uint8_t lcu8_motor_on_cnt;   				/* USE in Interrupt Func */
int16_t msec_counter_max;   				/* USE in Interrupt Func */
int16_t mot_mon_ad_msec,max_motor_mon_ad;   /* USE in Interrupt Func */
/* MOTOR_TEST_MODE_SET  */
int16_t motor_test_mode;

#if __MOTOR_MON==1
/* MOTORO 1MSEC MON     */
/*  *JUHO**  Motor Monitoring variable  */
int16_t MonMotorAD_temp[10], MonMotorAD[10];
uint16_t Mon1mscnt;
#endif


/*  motor rpm estimation    */
int16_t motor_rpm;

  #if (__ESC_MOTOR_PWM_CONTROL == ENABLE)
uint8_t lau8MscDuty, lau8MscDuty_old, lau8MscSetDuty;
uint8_t	laabsu8MscOnScan, laabsu8MscScanCounter;
int8_t las8MSCAdaptDuty;
//int16_t	las16MscDuty_old;
/*static*/	int16_t	laabss16BemfForDutyAdapt;
  #endif

#if __ADVANCED_MSC

int16_t msec_counter;
int16_t motor_on_time_msec,add_motor_on_time_msec,Max_add_motor_on_time_msec;
int16_t int_motor_on_time_msec,int_add_motor_on_time_msec;

int16_t Min_motor_on_time_msec,Max_motor_on_time_msec;
int16_t target_vol,target_vol_old;
int16_t msc_target_vol;
int16_t msc_off_timer;

int16_t ebd_msc_target_vol;
int16_t abs_msc_target_vol;

  #if __VDC
//int16_t tcs_msc_target_vol;	/*HSH_COMPILE*/
int16_t vdc_msc_target_vol;

int16_t dec_msc_target_vol;
int16_t acc_msc_target_vol;
int16_t hdc_msc_target_vol;

int16_t ebp_msc_target_vol;
int16_t epc_msc_target_vol;

int16_t epb_msc_target_vol;
int16_t tsp_msc_target_vol;
int16_t rop_msc_target_vol;

int16_t aux_msc_target_vol;
  #endif

#endif


/* Local Function prototype ****************************************/

void LAABS_vSetMotorCommand(void);
void LAABS_vSetMotorCommandNEW(void);

void LAABS_vSetActiveBrakeOn(void);
void LAABS_vSetFirstDumpCounter(void);
void LAABS_vSetMSCVariable(void);
void LAABS_vSetMSCVariableWheel(struct W_STRUCT *WL);

#if __TCMF_CONTROL
void TCMF_motor_speed_control(void);
#endif

#if __VDC
void LDMSC_vEstimateMotorRPM(void);
#endif

#if __ADVANCED_MSC
void LAMSC_vCallActHW(void);
/*uint8_t LAMSC_vCallActHW_interupt(uint16_t motor_mon_ad);*/
uint8_t LAMSC_vCallActHW_interupt(uint16_t mtp_ad);

void LCMSC_vInterfaceSystem(void);
void LCMSC_vInterfaceTargetvol(void);
void LCMSC_vSetEBDTargetVoltage(void);
void LCMSC_vSetABSTargetVoltage(void);

#if __VDC
void LCMSC_vSetAUXInput(void);
void LCMSC_vSetAUXTargetVoltage(void);
void LCMSC_vActValveCommand(void);
#endif

void LCMSC_vCalTargetVoltage(void);
void LCMSC_vSetInitialontime(void);

  #if (__ESC_MOTOR_PWM_CONTROL==ENABLE)
static void LCMSC_vCompMscDuty(void);  
static void LCMSC_vMscDutyOutput(void);
  #endif
  
void LCMSC_vCountDump(void);
void LCMSC_vSetMinMaxontime(void);
void LCMSC_vSetAdvancedMSC(void);

#if (__DELAY_MOTOR_DRV_AT_LOW_MP == ENABLE)
static void LCMSC_vDecideMtrDrvCondition(void);
#endif

#endif

/*Global Extern Variable  Declaration*******************************/



/* Implementation***************************************************/

void LAABS_vSetMotorCommand(void)
{
    /*  ACTIVE_BRAKE_MOT_ON 계산 */
    LAABS_vSetActiveBrakeOn();

    LAABS_vSetFirstDumpCounter();

    /*  기존 Motor Speed Contorl Start   */
    if(ABS_fz==1)
    {
        LAABS_vSetMSCVariable();
    }
    else
    {
        FL.MSC_dump_counter=0;
        FL.MSC_no_dump_counter=0;
        FR.MSC_dump_counter=0;
        FR.MSC_no_dump_counter=0;
        RL.MSC_dump_counter=0;
        RL.MSC_no_dump_counter=0;
        RR.MSC_dump_counter=0;
        RR.MSC_no_dump_counter=0;
    }

    /*  _______________Common   */


    LAABS_vSetMotorCommandNEW();        /*  New ADVANCED MSC    */

  #if __VDC
    /*LDMSC_vEstimateMotorRPM();*/
  #endif

}

void LAABS_vSetMotorCommandNEW(void)
{

    if((ABS_fz==1)||(EBD_RA==1)
      #if __VDC
        ||(BTC_fz==1)||(VDC_MOTOR_ON==1)||(ACTIVE_BRAKE_MOT_ON==1)
      #endif
    )
    {
/*08SWD-10*/
#if	__Decel_Ctrl_Integ
      #if ( __DEC || __HDC || __TCMF_LowVacuumBoost) && __TCMF_CONTROL && __VDC
        if((ACTIVE_BRAKE_MOT_ON==1) && ((YAW_CDC_WORK==0)))
        {
            TCMF_motor_speed_control();
        }
        else {;}
      #endif
#else
      #if ( __ACC || __HDC || __TCMF_LowVacuumBoost || __EPB_INTERFACE ) && __TCMF_CONTROL && __VDC
        if((ACTIVE_BRAKE_MOT_ON==1) && ((YAW_CDC_WORK==0)))
        {
            TCMF_motor_speed_control();
        }
        else {;}
      #endif
#endif
    }
    else { }

#if __ADVANCED_MSC

    LAMSC_vCallActHW();

	if( (int16_t)fu16CalVoltIntMOTOR >= MSC_9_V)
    {
        MOT_ON_FLG=1;
    }
    else
    {
        MOT_ON_FLG=0;
    }

#endif
/* ______________________ADVANCED_MSC end_________________*/

}

void LAABS_vSetActiveBrakeOn(void){
  #if  __TCMF_CONTROL
    msc_tempW9 =1;
    if ((msc_tempW9==0)
/*08SWD-10*/
#if __Decel_Ctrl_Integ
      #if   __DEC
        ||(lcu1DecActiveFlg==1)
      #endif
      #if __HDC
        ||(lcu1HdcActiveFlg==1)
      #endif
#else
      #if   __ACC
        ||(lcu1AccActiveFlg==1)
      #endif
      #if __HDC
        ||(lcu1HdcActiveFlg==1)
      #endif
      #if __EPB_INTERFACE
        ||(lcu1EpbActiveFlg==1)
      #endif
#endif
      #if __SCC
        || (lcu8WpcActiveFlg==1)
      #endif

      #if __TVBB
        ||(lctvbbu1TVBB_ON==1)
      #endif
      #if   __TCMF_LowVacuumBoost
        ||((TCMF_LowVacuumBoost_flag==1)&&(ABS_fz==0))
      #endif
        )
    {
        ACTIVE_BRAKE_MOT_ON=1;
    }
    else
    {
        ACTIVE_BRAKE_MOT_ON=0;
    }
  #else
    ACTIVE_BRAKE_MOT_ON=0;
  #endif
}


void LAABS_vSetFirstDumpCounter(void){
    if(lcabsu1BrakeByBLS==1)
    {
        if(BLS_K==1)
        {
             First_Dump_counter_after_BLS=L_1ST_SCAN;
        }
        else
        {
            ;
        }
        if((ABS_fz==0)&&(First_Dump_counter_after_BLS<L_TIME_700MS))
        {
            First_Dump_counter_after_BLS=First_Dump_counter_after_BLS+L_1SCAN;
        }
        else
        {
            ;
        }
    }
    else
    {
        First_Dump_counter_after_BLS=L_TIME_700MS;
    }
}




/* -----------------------------------------------------------------------------*/
/*  Programed by J.K.Lee at 2002.06.20                                          */
/* -----------------------------------------------------------------------------*/
/*  A function for which MSC_ref_voltage be set by referring to MSC_indicator   */
/* -----------------------------------------------------------------------------*/

void LAABS_vSetMSCVariable(void)
{
    int16_t Dump_factor; /* weight factor related to dump and the other state   */
    int16_t MSC_indicator_max;
    int16_t MSC_BEMF_Voltage_Ref;

  #if __VDC
    if(AFZ_OK==1)                                                                                                   
    {                                                                                                               
        if(afz>=-(int16_t)U8_PULSE_UNDER_LOW_MED_MU)                                                                         
        {
            MSC_BEMF_Voltage_Ref = S16_ESP_Motor_BEMF_Voltage_Ref;
            Dump_factor = 4; /* low mu */
        }
        else if(afz>=-(int16_t)U8_PULSE_MED_HIGH_MU)
        {
        	MSC_BEMF_Voltage_Ref = S16_ESP_Motor_BEMF_Voltage_Ref; /* med mu */
        	Dump_factor = 7; /* med mu */
        }                                                                                                           
        else if(afz>-(int16_t)U8_PULSE_HIGH_MU)                                                                         
        {                                                                                                           
            MSC_BEMF_Voltage_Ref = S16_ESP_Motor_BEMF_Voltage_Ref + S16_ESP_Motor_BEMF_ADD_MED_MU; /* med-high mu */
            Dump_factor = 7; /* med mu */
      	}                                                                                                           
        else                                                                                                        
        {                                                                                                           
            MSC_BEMF_Voltage_Ref = S16_ESP_Motor_BEMF_Voltage_Ref + S16_ESP_Motor_BEMF_ADD_HIGH_MU; /* High mu   */
            Dump_factor = 10; /* High mu   */
        }                                                                                                           
    }                                                                                                               
    else                                                                                                            
    {                                                                                                               
    	MSC_BEMF_Voltage_Ref = S16_ESP_Motor_BEMF_Voltage_Ref;
        if(vdc_error_flg==0)
        {
		  #if __PRESS_EST && __MP_COMP
            if((FL.Estimated_Press_SKID<MPRESS_50BAR)&&(FR.Estimated_Press_SKID<MPRESS_50BAR))
            {
                Dump_factor = 4;
            }
            else if((FL.Estimated_Press_SKID<MPRESS_80BAR)&&(FR.Estimated_Press_SKID<MPRESS_80BAR))
            {
                Dump_factor = 7;
            }
            else
		  #endif/* __PRESS_EST && __MP_COMP */
            {
                Dump_factor = 10; /* High mu   */
            }
        }
        else
        {
            if( First_Dump_counter_after_BLS<=(uint8_t)U8_MSC_low_mu_skid_counter)
            {
                Dump_factor = 4;
            }
            else
            {
                Dump_factor = 10; /* High mu   */
            }
        }
          #if  __CAR==GM_TAHOE ||__CAR==GM_SILVERADO
        if (Dump_factor<7)
        {
            Dump_factor=7;
        }
        else {}
          #endif/*__CAR==GM_TAHOE ||__CAR==GM_SILVERADO*/    	                                                      
    }
  #else /*__VDC*/                                                                                                                 
    if(AFZ_OK==1)                                                                                                   
    {
        if(afz>=-(int16_t)U8_PULSE_UNDER_LOW_MED_MU)                                                                         
        {
            MSC_BEMF_Voltage_Ref = S16_ABS_Motor_BEMF_Voltage_Ref;
            Dump_factor = 4; /* low mu */
        }
        else if(afz>=-(int16_t)U8_PULSE_MED_HIGH_MU)
        {
        	MSC_BEMF_Voltage_Ref = S16_ABS_Motor_BEMF_Voltage_Ref; /* med mu */
        	Dump_factor = 7; /* med mu */
        }
        else if(afz>-(int16_t)U8_PULSE_HIGH_MU)                                                                         
        {
            MSC_BEMF_Voltage_Ref = S16_ABS_Motor_BEMF_Voltage_Ref + S16_ABS_Motor_BEMF_ADD_MED_MU; /* med-high mu */
            Dump_factor = 7; /* med mu */
      	}
        else                                                                                                        
        {
            MSC_BEMF_Voltage_Ref = S16_ABS_Motor_BEMF_Voltage_Ref + S16_ABS_Motor_BEMF_ADD_HIGH_MU; /* High mu   */
            Dump_factor = 10; /* High mu   */
        }
    }
    else
    {
    	MSC_BEMF_Voltage_Ref = S16_ABS_Motor_BEMF_Voltage_Ref;
        if(First_Dump_counter_after_BLS<=(uint8_t)U8_MSC_low_mu_skid_counter)
        {
            Dump_factor = 4;
        }
        else
        {
            Dump_factor = 10; /* High mu   */
        }
    }
  #endif /*__VDC*/                                                                                                                
    
    LAABS_vSetMSCVariableWheel(&FL);
    LAABS_vSetMSCVariableWheel(&FR);
    LAABS_vSetMSCVariableWheel(&RL);
    LAABS_vSetMSCVariableWheel(&RR);
    
  #if __SPLIT_TYPE                      /*FR Split  */
    lau16MSC_tempW0=(uint16_t)Dump_factor*(2*FL.MSC_dump_counter+2*FR.MSC_dump_counter);
    lau16MSC_tempW1=2*(2*FL.MSC_no_dump_counter+2*FR.MSC_no_dump_counter);

    if(lau16MSC_tempW0<=lau16MSC_tempW1)
    {
        MSC_primary_indicator=0;
        FR.MSC_dump_counter=0;
        FL.MSC_dump_counter=0;
        FR.MSC_no_dump_counter=0;
        FL.MSC_no_dump_counter=0;
    }
    else
    {
        MSC_primary_indicator = (int16_t)((lau16MSC_tempW0-lau16MSC_tempW1)/2);
    }

    lau16MSC_tempW0=(uint16_t)Dump_factor*(RL.MSC_dump_counter+RR.MSC_dump_counter);
    lau16MSC_tempW1=2*(RL.MSC_no_dump_counter+RR.MSC_no_dump_counter);

    if(lau16MSC_tempW0<=lau16MSC_tempW1)
    {
        MSC_secondary_indicator=0;
        RL.MSC_dump_counter=0;
        RR.MSC_dump_counter=0;
        RL.MSC_no_dump_counter=0;
        RR.MSC_no_dump_counter=0;
    }
    else
    {
        MSC_secondary_indicator = (int16_t)((lau16MSC_tempW0-lau16MSC_tempW1)/2);
    }
  #else                                 /*X Split  */
    lau16MSC_tempW0=(uint16_t)Dump_factor*(2*FR.MSC_dump_counter+RL.MSC_dump_counter);
    lau16MSC_tempW1=2*(2*FR.MSC_no_dump_counter+RL.MSC_no_dump_counter);

    if(lau16MSC_tempW0<=lau16MSC_tempW1)
    {
        MSC_primary_indicator=0;
        FR.MSC_dump_counter=0;
        RL.MSC_dump_counter=0;
        FR.MSC_no_dump_counter=0;
        RL.MSC_no_dump_counter=0;
    }
    else
    {
        MSC_primary_indicator = (int16_t)((lau16MSC_tempW0-lau16MSC_tempW1)/3);
    }

    lau16MSC_tempW0=(uint16_t)Dump_factor*(2*FL.MSC_dump_counter+RR.MSC_dump_counter);
    lau16MSC_tempW1=2*(2*FL.MSC_no_dump_counter+RR.MSC_no_dump_counter);

    if(lau16MSC_tempW0<=lau16MSC_tempW1)
    {
        MSC_secondary_indicator=0;
        FL.MSC_dump_counter=0;
        RR.MSC_dump_counter=0;
        FL.MSC_no_dump_counter=0;
        RR.MSC_no_dump_counter=0;
    }
    else
    {
        MSC_secondary_indicator = (int16_t)((lau16MSC_tempW0-lau16MSC_tempW1)/3);
    }

  #endif

    if(MSC_primary_indicator<MSC_secondary_indicator)
    {
        MSC_indicator = MSC_secondary_indicator;
    }
    else
    {
        MSC_indicator = MSC_primary_indicator;
    }

  #if __VDC
    MSC_indicator_max = (int16_t)((uint32_t)MSC_12_V*U8_MSC_Activate_Indicator_ABS2/MSC_BEMF_Voltage_Ref);
  #else
    MSC_indicator_max = (int16_t)((uint32_t)MSC_12_V*U8_MSC_Activate_Indicator/MSC_BEMF_Voltage_Ref);
  #endif

    if(MSC_indicator>MSC_indicator_max)
    {
        MSC_indicator = MSC_indicator_max;
    }
    else
    {
        ;
    }
                        /* Max. limit of MSC_ref_voltage is 615(=12V)   */
  #if __VDC
    if((MSC_indicator>((int16_t)U8_MSC_Activate_Indicator_ABS2))&&(ACTIVE_BRAKE_MOT_ON==0))
    {
                        /* Default Value is IGN 4V up to 7scan dump     */
        MSC_ref_voltage = (int16_t)((uint32_t)MSC_BEMF_Voltage_Ref*MSC_indicator/U8_MSC_Activate_Indicator_ABS2);
    }
    else
    {
        MSC_ref_voltage = MSC_BEMF_Voltage_Ref;
    }
  #else
    if(MSC_indicator>((int16_t)U8_MSC_Activate_Indicator))
    {
                        /* Default Value is IGN 4V up to 7scan dump     */
        MSC_ref_voltage = (int16_t)((uint32_t)MSC_BEMF_Voltage_Ref*MSC_indicator/U8_MSC_Activate_Indicator);
    }
    else
    {
        MSC_ref_voltage = MSC_BEMF_Voltage_Ref;
    }
  #endif

}


void LAABS_vSetMSCVariableWheel(struct W_STRUCT *WL)
{

  #if __VDC
    if((HV_VL_wl==1)&&(AV_VL_wl==1)&&(AV_DUMP_wl==1))
    {
    	if((WL->lcabss8DumpCase==U8_L_DUMP_CASE_10)||(WL->lcabss8DumpCase==U8_L_DUMP_CASE_01))
        {
        	WL->MSC_dump_counter = WL->MSC_dump_counter + (L_CONTROL_PERIOD/2);
        }
        else
        {
        	WL->MSC_dump_counter = WL->MSC_dump_counter + L_CONTROL_PERIOD;
        }
        /*WL->MSC_dump_counter = LCABS_u8CountDumpNum((uint8_t)WL->MSC_dump_counter);*/

        if(REAR_WHEEL_wl==0)
        {
            if(WL->MSC_dump_counter>(uint8_t)U8_MSC_Limit_Dump_Scans_F)
            {
                WL->MSC_dump_counter = (uint8_t)U8_MSC_Limit_Dump_Scans_F;
            }
            else
            {
                ;
            }
        }
        else
        {
            if(WL->MSC_dump_counter>=(uint8_t)U8_MSC_Limit_Dump_Scans_R)
            {
                WL->MSC_dump_counter = (uint8_t)U8_MSC_Limit_Dump_Scans_R;
            }
            else
            {
                ;
            }
        }
    }
    else
    {
    	if((WL->lcabss8DumpCaseOld==U8_L_DUMP_CASE_10)||(WL->lcabss8DumpCaseOld==U8_L_DUMP_CASE_01))
        {
        	WL->MSC_no_dump_counter = WL->MSC_no_dump_counter + (L_CONTROL_PERIOD/2);
        }
        else
        {
        	WL->MSC_no_dump_counter = WL->MSC_no_dump_counter + L_CONTROL_PERIOD;
        }
        /*WL->MSC_no_dump_counter = WL->MSC_no_dump_counter + L_1SCAN;*/
    }
  #else
    if((HV_VL_wl==1) && (AV_VL_wl==1))
    {
    	if((WL->lcabss8DumpCase==U8_L_DUMP_CASE_10)||(WL->lcabss8DumpCase==U8_L_DUMP_CASE_01))
        {
        	WL->MSC_dump_counter = WL->MSC_dump_counter + (L_CONTROL_PERIOD/2);
        }
        else
        {
        	WL->MSC_dump_counter = WL->MSC_dump_counter + L_CONTROL_PERIOD;
        }
        /*WL->MSC_dump_counter = LCABS_u8CountDumpNum((uint8_t)WL->MSC_dump_counter);*/
        if((REAR_WHEEL_wl==0)&&(WL->MSC_dump_counter>(uint8_t)U8_MSC_Limit_Dump_Scans_F))
        {      /* '02 NZ Winter - CSH  */
            WL->MSC_dump_counter = (uint8_t)U8_MSC_Limit_Dump_Scans_F;
        }
        else if((REAR_WHEEL_wl==1)&&(WL->MSC_dump_counter>(uint8_t)U8_MSC_Limit_Dump_Scans_R))
        {
            WL->MSC_dump_counter = (uint8_t)U8_MSC_Limit_Dump_Scans_R;
        }
        else
        {
            ;
        }
    }
    else
    {
    	if((WL->lcabss8DumpCaseOld==U8_L_DUMP_CASE_10)||(WL->lcabss8DumpCaseOld==U8_L_DUMP_CASE_01))
        {
        	WL->MSC_no_dump_counter = WL->MSC_no_dump_counter + (L_CONTROL_PERIOD/2);
        }
        else
        {
        	WL->MSC_no_dump_counter = WL->MSC_no_dump_counter + L_CONTROL_PERIOD;
        }
        /*WL->MSC_no_dump_counter = WL->MSC_no_dump_counter + L_1SCAN;*/
    }

  #endif
}


#if __TCMF_CONTROL &&  __VDC
void TCMF_motor_speed_control(void)
{

    if((VDC_MOTOR_ON==1) || (TC_MOT_ON==1) || (ABS_fz==1)
          #if __HDC
        || ((lcu8HdcMotorMode==1)&&(lcu1HdcActiveFlg==1))
          #endif

/*08SWD-10*/
#if __Decel_Ctrl_Integ
          #if __DEC
        || ((lcu8DecMotorMode==1)&&(lcu1DecActiveFlg==1))
          #endif
#else
          #if __ACC
        || ((lcu8AccMotorMode==1)&&(lcu1AccActiveFlg==1))
          #endif
          #if __EPB_INTERFACE
        || ((lcu8EpbMotorMode==1)&&(lcu1EpbActiveFlg==1))
          #endif
#endif

        )
    {
        TCMF_MOTOR_OFF_cnt=0;
        TCMF_MOTOR_OFF_flag = 0;
        TCMF_MOTOR_OFF_flag0 = 0;
    }
    else
    {
        if(TCMF_MOTOR_OFF_cnt<=L_TIME_1200MS)
        {
            TCMF_MOTOR_OFF_cnt=TCMF_MOTOR_OFF_cnt+L_1SCAN;
        }
        else { }

        if(TCMF_MOTOR_OFF_cnt>=L_TIME_70MS)
        {
            TCMF_MOTOR_OFF_flag0 = 1;

			if((int16_t)fu16CalVoltIntMOTOR<MSC_0_75_V)
            {
                TCMF_MOTOR_OFF_flag = 1;
            }
            else { }
        }
        else
        {
            TCMF_MOTOR_OFF_flag = 0;
            TCMF_MOTOR_OFF_flag0 = 0;
        }
    }
}
#endif

#if __ADVANCED_MSC

void    LAMSC_vCallActHW(void)
{
  #if (__DELAY_MOTOR_DRV_AT_LOW_MP == ENABLE)
    
    LCMSC_vDecideMtrDrvCondition();
    
  #endif

    LCMSC_vInterfaceSystem();

    LCMSC_vSetAdvancedMSC();

}

void LCMSC_vInterfaceSystem(void){

  #if __VDC
    LCMSC_vSetAUXInput();
  #endif

    LCMSC_vInterfaceTargetvol();

  #if !__LPA_TEST
    LCMSC_vCalTargetVoltage();
  #endif

    LCMSC_vSetInitialontime();
    
  #if (__ESC_MOTOR_PWM_CONTROL==ENABLE)
  	LCMSC_vCompMscDuty();
  
    LCMSC_vMscDutyOutput();
  #endif

    LCMSC_vCountDump();

    LCMSC_vSetMinMaxontime();

}

void LCMSC_vSetEBDTargetVoltage(void){

  #if !__BTC || __ESV
    if ((EBD_RA==1)
  #if __CBC
        ||(CBC_ON==1)
  #endif
  #if __SLS
        ||(SLS_ON==1)
  #endif  
  #if __DELAY_MOTOR_DRV_AT_LOW_MP
        ||(lamscu1DelayMtrDriveInABS==1)
  #endif
        )
    {
        if ((HV_VL_rl==1)&&(AV_VL_rl==1))
        {
            if(RL.ebd_dump_time < 30) {RL.ebd_dump_time++;}
        }
        else { }

        if ((HV_VL_rr==1)&&(AV_VL_rr==1))
        {
            if(RR.ebd_dump_time < 30) {RR.ebd_dump_time++;}
        }
        else { }

      #if     __SPLIT_TYPE == 1         /* 1 : F/R-split */
        if ((RL.ebd_dump_time+RR.ebd_dump_time) >= 25)
        {
            EBD_MOTOR_DRV=1;
            ebd_motor_drv_time=(uint8_t)L_TIME_500MS;
        }
        else { }

      #else                             /* 0 : X-split */
        if ((RL.ebd_dump_time >= 25)||(RR.ebd_dump_time >= 25))
        {
            EBD_MOTOR_DRV=1;
            ebd_motor_drv_time=(uint8_t)L_TIME_500MS;
        }
        else { }

      #endif
    }
    else { }
  #endif


    msc_tempW9=0;
  #if __VDC
    if(EBD_MOTOR_DRV==1)
    {
        if(target_vol> MSC_0_V)
        {								/*   24500={(14volt*1000)*(200msec(28))*2} /4/8   */
            tempW2=(int16_t)35;				/* 14*7	*/
            tempW1=MSC_1_V;
            tempW0=1;
            s16muls16divs16();
            msc_tempW8 = tempW3;

        	if(s16mscebdmotoff_cnt>msc_tempW8)   /*   24500={(14volt*1000)*(200msec(28))*2} /4/8   */
            {
                msc_tempW9=1;
            }
            else
            {
                s16mscebdmotoff_cnt = s16mscebdmotoff_cnt + (target_vol>>5);
            }
        }
        else { }
    }
    else
    {
        s16mscebdmotoff_cnt=0;
    }
  #endif

  #if __DELAY_MOTOR_DRV_AT_LOW_MP
    if(((ABS_fz==1)&&(lamscu1DelayMtrDriveInABS==0))||(msc_tempW9==1))
  #else
    if((ABS_fz==1)||(msc_tempW9==1))
  #endif
    {
        ebd_motor_drv_time=0;
        RL.ebd_dump_time=0;
        RR.ebd_dump_time=0;
        EBD_MOTOR_DRV=0;
        msc_tempW9=0;
    }
    else { }

    EBD_MOTOR_DETECT=0;

    if((ABS_fz==1)||(EBD_RA==1)
      #if __VDC
        ||(BTC_fz==1)||(VDC_MOTOR_ON==1)||(ACTIVE_BRAKE_MOT_ON==1)
      #endif
    ) { }
    else
    {
    #if !__BTC || __ESV
        if(((vref > (int16_t)VREF_40_KPH)||((vref > (int16_t)VREF_35_KPH)&&(EBD_MSC_MOTOR_ON==1)))
        &&(EBD_RA==0)&&(EBD_MOTOR_DRV ==1)
      #if __CBC
        &&(CBC_ON==0)
      #endif
      #if __SLS
        &&(SLS_ON==0)
      #endif      
     #if __VDC
      	&&( ((fu1BLSErrDet==0)&&(BLS==0))
           #if __BRK_SIG_MPS_TO_PEDAL_SEN
      		||((lcabsu1ValidBrakeSensor==0)&&(fu1BLSErrDet==1)) 
      		||((lcabsu1ValidBrakeSensor==1)&&(lcabsu1BrakeSensorOn==0))
           #else /*__BRK_SIG_MPS_TO_PEDAL_SEN*/
      		||((fu1MCPErrorDet==1)&&(fu1BLSErrDet==1)) 
      		||((fu1MCPErrorDet==0)&&(lcabsu1BrakeSensorOn==0))
           #endif /*__BRK_SIG_MPS_TO_PEDAL_SEN*/
      		)
      #else
        &&((fu1BLSErrDet==1)
        	||((fu1BLSErrDet==0)&&(BLS==0)))
      #endif
        )

        {
            if(ebd_motor_drv_time > 0)
            {
                ebd_motor_drv_time=ebd_motor_drv_time-L_1SCAN;
                EBD_MOTOR_DETECT=1;
            }
            else
            {
                ebd_dump_time_rl=0;
                ebd_dump_time_rr=0;
                EBD_MOTOR_DRV=0;
            }
        }
        else { }
    #endif
    }

    if (EBD_MOTOR_DETECT ==1)
    {
        EBD_MSC_MOTOR_ON=1;
        ebd_msc_target_vol = MSC_4_V;
    }
    else
    {
        EBD_MSC_MOTOR_ON=0;
        ebd_msc_target_vol = MSC_0_V;
    }
}


void LCMSC_vSetABSTargetVoltage(void)
{
    /*int16_t Sum_of_motor_msec_on_time=0;*/
      #if __MOTOR_OFF_AT_LOW_BRAKING_ESC==ENABLE
    uint8_t u8MsTimeRefForMotorOffLowSpd  = 30;
    uint8_t u8MsTimeRefForMotorOffHighSpd = 20;
    uint8_t u8MsTimeRefForMotorOff        = 20;
      #endif
    int16_t s16AbsMscTarVAdaptForMp;
          
    /*  For EN Bump noise, '07  ~~~ */
    msc_tempW9 = (int16_t)lcu8_motor_on_cnt;
    lcu8_motor_on_cnt = 0;

    if (ABS_fz ==1)
    {
      #if __DELAY_MOTOR_DRV_AT_LOW_MP
        if(lamscu1DelayMtrDriveInABS==0)
	  #endif
        {
	        if (ABS_fz_on_cnt<L_TIME_2MIN)
	        {
	            ABS_fz_on_cnt=ABS_fz_on_cnt+L_1SCAN;
	        }
	        else { }
		}
        ABS_fz_off_cnt=0;
    }
    else
    {
        ABS_fz_on_cnt=0;
        if (ABS_fz_off_cnt<L_TIME_2MIN)
        {
            ABS_fz_off_cnt=ABS_fz_off_cnt+L_1SCAN;
        }
        else { }
    }

    if ((ABS_fz == 1)||((ABS_fz_off_cnt>0)&&(ABS_fz_off_cnt<=L_TIME_1000MS)))
    {
        MOT_ON_FLG_counter = MOT_ON_FLG_counter + (uint8_t)msc_tempW9;
    }
    else
    {
        MOT_ON_FLG_counter_old3 =0;
        MOT_ON_FLG_counter_old2 =0;
        MOT_ON_FLG_counter_old =0;
        MOT_ON_FLG_counter=0;

        MOT_ON_FLG_timer=0;
        MOT_ON_FLG_timer2=0;
    }

    if (MOT_ON_FLG_timer<L_TIME_130MS)
    {
        MOT_ON_FLG_timer=MOT_ON_FLG_timer+L_1SCAN;
    }
    else
    {
        MOT_ON_FLG_counter_old3 = MOT_ON_FLG_counter_old2;
        MOT_ON_FLG_counter_old2 = MOT_ON_FLG_counter_old;
        MOT_ON_FLG_counter_old = MOT_ON_FLG_counter;
        MOT_ON_FLG_counter=0;
        MOT_ON_FLG_timer=0;
        if (MOT_ON_FLG_timer2<127)
        {
            MOT_ON_FLG_timer2 = MOT_ON_FLG_timer2 + L_1SCAN;
        }
        else { }
    }
    
#if __VDC
	if ((S16_MPRESS_OFF_TIME < (L_TIME_5S-L_TIME_300MS))
  #if __SPLIT_TYPE                      /*FR Split  */
    	||(S_VALVE_SECONDARY ==1) || (S_VALVE_PRIMARY ==1 )
  #else                                 /*X Split  */
    	||(S_VALVE_LEFT  ==1 ) || (S_VALVE_RIGHT ==1)
  #endif
		)
	{
		MOT_ON_FLG_timer2=0;
	}
	else { }
#endif

    if (((HV_VL_fl==1)&&(AV_VL_fl==1))||((HV_VL_fr==1)&&(AV_VL_fr==1))||
        ((HV_VL_rl==1)&&(AV_VL_rl==1))||((HV_VL_rr==1)&&(AV_VL_rr==1))
         #if __VDC
        ||(YAW_CDC_WORK==1)
         #endif
        )
    {
        Wheel_Dump_timer = S8_MSC_OFF_TIME_AFTER_DUMP;
        bump_low_speed_motor_stop =0;
    }
    else if (Wheel_Dump_timer>0)
    {
        Wheel_Dump_timer = Wheel_Dump_timer - L_1SCAN;
        bump_low_speed_motor_stop =0;
    }
    else
    {
        Sum_of_motor_msec_on_time = (int16_t)((int16_t)MOT_ON_FLG_counter_old + MOT_ON_FLG_counter_old2 + MOT_ON_FLG_counter_old3);

      #if __MOTOR_OFF_AT_LOW_BRAKING_ESC==DISABLE
        if ((vref <= VREF_25_KPH)&&(ABS_fz ==1))
        {
            if((MOT_ON_FLG_timer2>=L_3SCAN)&&(MOT_ON_FLG_timer==0))
            {
                if (Sum_of_motor_msec_on_time<=36)/*36ms*/
                {
                    bump_low_speed_motor_stop =1;
                }
                else if (Sum_of_motor_msec_on_time>36)/*36ms*/
                {
                    bump_low_speed_motor_stop =0;
                }
                else { }
            }
            else
            {
                ;
            }
        }
        else
        {
            bump_low_speed_motor_stop =0;
        }
      #else
        if ((vref <= S16_ALLOW_MOTOR_OFF_SPEED)&&(ABS_fz ==1))
        {
            if((MOT_ON_FLG_timer2>=L_3SCAN)&&(MOT_ON_FLG_timer==0))
            {
                u8MsTimeRefForMotorOffLowSpd  = (uint8_t)LCABS_s16Interpolation2P(lcabss16RefMpress, MPRESS_30BAR, MPRESS_100BAR, 45, 30);
                u8MsTimeRefForMotorOffHighSpd = (uint8_t)LCABS_s16Interpolation2P(lcabss16RefMpress, MPRESS_30BAR, MPRESS_100BAR, 20, 15);
 
                u8MsTimeRefForMotorOff = (uint8_t)LCABS_s16Interpolation2P(vref, 						   VREF_25_KPH,              S16_ALLOW_MOTOR_OFF_SPEED,
                                												 (int16_t)u8MsTimeRefForMotorOffLowSpd, (int16_t)u8MsTimeRefForMotorOffHighSpd);

                if (Sum_of_motor_msec_on_time<=u8MsTimeRefForMotorOff)
                {
                    bump_low_speed_motor_stop =1;
                }
                else
                {
                    bump_low_speed_motor_stop =0;
                }
            }
            else
            {
                ;
            }
        }
        else
        {
            bump_low_speed_motor_stop =0;
        }
      #endif
    }
    /*  ~~~ For EN Bump noise, '07  */

  #if (__DELAY_MOTOR_DRV_AT_LOW_MP==ENABLE)
    if(bump_low_speed_motor_stop==0)
    {
        if(lamscu1DelayMtrDriveInABS==1)
        {
            bump_low_speed_motor_stop = 1;
            lamscu1MtrStopByDlayMtrDrvInABS = 1;
        }
        else
        {
            lamscu1MtrStopByDlayMtrDrvInABS = 0;
        }
    }
    else
    {
        if((lamscu1DelayMtrDriveInABS==0) && (lamscu1MtrStopByDlayMtrDrvInABS==1))
        {
            bump_low_speed_motor_stop = 0;
            lamscu1MtrStopByDlayMtrDrvInABS = 0;
        }
        else
        {
            ;
        }
    }
  #endif

  #if __LPA_TEST
    bump_low_speed_motor_stop =0;
  #endif

  #if __VDC
    msc_tempW9 = (int16_t)U8_Add_ABS_Mtr_On_Duty_in_ESC;
    msc_tempW8 = (int16_t)U8_Add_ABS_Mtr_Off_Duty_in_ESC;
  #else
    msc_tempW9 = (int16_t)U8_Add_ABS_Mtr_On_Duty_in_ABS;
    msc_tempW8 = (int16_t)U8_Add_ABS_Mtr_Off_Duty_in_ABS;
  #endif

    /* 전압에 의한 해제조건 보상 로직 추가 */
    
  #if __VDC
  
	lcu16CalVoltBatt2LfOld = lcu16CalVoltBatt2Lf;

#if (__Voltage_Mon_Change == ENABLE)
	lcu16CalVoltBatt2Lf = ((int16_t)fu16CalVoltVDD/4+lcu16CalVoltBatt2LfOld/4*3);/*fu16CalVoltBATT2:disable from DV#1*/
#else
    lcu16CalVoltBatt2Lf = ((int16_t)fu16CalVoltBATT2/4+lcu16CalVoltBatt2LfOld/4*3);
#endif/*__Voltage_Mon_Change*/

    lcu16CalVoltBatt2Lf = LCABS_s16LimitMinMax(lcu16CalVoltBatt2Lf, MSC_9_V, MSC_16_V);

    msc_tempW7= LCESP_s16IInter4Point(lcu16CalVoltBatt2Lf,    MSC_11_V,3,
                                                      		  MSC_13_5_V,0,
                                                      		  MSC_15_V,-2,
                                                       		  MSC_15_V,-2);
                                                        
  #else  /*__VDC*/                                                    	
  	
    ref_mon_ad_lf_old = ref_mon_ad_lf;

    ref_mon_ad_lf = ((int16_t)fu16CalVoltFSR/4+ref_mon_ad_lf_old/4*3);

    ref_mon_ad_lf = LCABS_s16LimitMinMax(ref_mon_ad_lf, MSC_9_V, MSC_16_V);

    msc_tempW7= LCESP_s16IInter4Point(ref_mon_ad_lf,    MSC_11_V,3,
                                                        MSC_13_5_V,0,
                                                        MSC_15_V,-2,
                                                        MSC_15_V,-2);

  #endif /*__VDC*/

    msc_tempW8 = msc_tempW8 + msc_tempW7;

    /* ABS 제어 말기 모터 추가 구동로직 추가 */
    if ((ABS_fz == 1)||((ABS_fz_off_cnt>0)&&(ABS_fz_off_cnt<=L_TIME_1000MS)))
    {
        if (MOT_ON_FLG_timer2<L_2SCAN)
        {
            additional_abs_motor_on =0;
        }
        else if((MOT_ON_FLG_timer2>=L_2SCAN)&&(MOT_ON_FLG_timer==0))
        {
        	/*
            add_Sum_of_motor_msec_on_time = (int16_t)MOT_ON_FLG_counter_old;

            tempW2=(int16_t)add_Sum_of_motor_msec_on_time;
            tempW1=100;
            tempW0=(T_130_MS*7);
            s16muls16divs16();
            Motor_Drv_Duty_130ms = tempW3;
            */
		  #if (!__REORGANIZE_FOR_MGH60_CYCLETIM)
            Motor_Drv_Duty_130ms = (int16_t)(((int32_t)MOT_ON_FLG_counter_old*100)/(L_U8_TIME_10MSLOOP_130MS*10));
		  #else
            Motor_Drv_Duty_130ms = (int16_t)(((uint16_t)MOT_ON_FLG_counter_old*100)/(L_U8_TIME_10MSLOOP_130MS*10));
		  #endif 

            if (Motor_Drv_Duty_130ms>=msc_tempW9)
            {
                additional_abs_motor_on =1;
            }
            else if (Motor_Drv_Duty_130ms<msc_tempW8)
            {
                additional_abs_motor_on =0;
            }
            else { }
        }
        else { }
    }
    else
    {
    	/*
        add_Sum_of_motor_msec_on_time =0;
        */
        additional_abs_motor_on =0;
    }
    /* ~~ABS 제어 말기 모터 추가 구동로직 추가 */

    if ((additional_abs_motor_on==1)||((ABS_fz==1) 
	 #if __DELAY_MOTOR_DRV_AT_LOW_MP
	&& (lamscu1DelayMtrDriveInABS == 0)
	#endif
	))
    {
        ABS_MSC_MOTOR_ON=1;
    }
    else
    {
        ABS_MSC_MOTOR_ON=0;
    }

    if (ABS_MSC_MOTOR_ON==1)
    {
        if(bump_low_speed_motor_stop==1)
        {
            abs_msc_target_vol = MSC_0_V;
        }
        else
        {
        /* Initial Motor Driving Mode   */
            msc_tempW9 =0;
        #if __VDC && __TCMF_CONTROL
            if ((msc_tempW9==1)
          #if __HDC
            ||(lcu1HdcActiveFlg==1)
          #endif
/*08SWD-10*/
#if __Decel_Ctrl_Integ
          #if   __DEC
            ||(lcu1DecActiveFlg==1)
          #endif
#else
          #if   __ACC
            ||(lcu1AccActiveFlg==1)
          #endif

          #if __EPB_INTERFACE
            ||(lcu1EpbActiveFlg==1)
          #endif
#endif
	      #if __SCC
	        || (lcu8WpcActiveFlg==1)
	      #endif
	        
          #if   __TCMF_LowVacuumBoost
            ||(TCMF_LowVacuumBoost_flag==1)
          #endif
            )
            {
                msc_tempW9 =1;
            }
        #endif

          #if __VDC
            msc_tempW8 = (int16_t)U8_MSC_init_fully_driving_ABS2;
          #else
            msc_tempW8 = (int16_t)U8_MSC_init_fully_driving_scan;
          #endif

          #if __DELAY_MOTOR_DRV_AT_LOW_MP && (__ADD_MTR_DRV_T_AF_INIT_DELAY==ENABLE)
            if(lamscu1DelayMtrDriveInABSOld == 1)
            {
                msc_tempW8 = msc_tempW8 + (uint8_t)U8_ADD_FULL_DRV_T_AF_INIT_DELAY;
            }
          #endif

            if ((ABS_fz_on_cnt <= msc_tempW8)&&(msc_tempW9==0)&&(ABS_fz_off_cnt == 0))
            {
            	if(ABS_fz_on_cnt == L_1SCAN)
            	{
	                abs_msc_target_vol = (int16_t)U16_MSC_INITIAL_TARGET_VOL;
	               
	               #if __VDC
	                if(lcabsu1ValidBrakeSensor==1)
	                {				      
				        s16AbsMscTarVAdaptForMp = LCESP_s16IInter4Point(lcabss16RefMpress+lcabss16MpressSlopePeak, MPRESS_40BAR, MSC_6_V,/*MPRESS_30BAR+MPRESS_10BAR*/
				                																		MPRESS_50BAR, MSC_8_V,/*MPRESS_50BAR+MPRESS_15BAR*/
				                																		MPRESS_80BAR, MSC_12_V,/*MPRESS_80BAR+MPRESS_20BAR*/
				                																		MPRESS_100BAR,(int16_t)U16_MSC_INITIAL_TARGET_VOL);/*MPRESS_100BAR+MPRESS_20BAR*/
	                	if(abs_msc_target_vol > s16AbsMscTarVAdaptForMp)
	                	{
	                		abs_msc_target_vol = s16AbsMscTarVAdaptForMp;
	                	}
	                }
	                else
	                {
	                	if(abs_msc_target_vol < MSC_8_V)
	                	{
	                		abs_msc_target_vol = MSC_8_V;
	                	}	                	
	                }
	              #else
                	if(abs_msc_target_vol < MSC_8_V)
                	{
                		abs_msc_target_vol = MSC_8_V;
                	}	                	
	              #endif
	            }
            }
            else
            {
              #if __VDC                                                                                                                    
                if(AFZ_OK==1)                                                                                                              
                {                                                                                                                          
                    if(afz>=-(int16_t)U8_PULSE_MED_HIGH_MU)                                                                                    
                    {                                                                                                                      
                        abs_msc_target_vol = S16_ESP_Motor_BEMF_Voltage_Ref; /* med mu */                                                
                    }                                                                                                                      
                    else if(afz>-(int16_t)U8_PULSE_HIGH_MU)                                                                                    
                    {                                                                                                                      
                        abs_msc_target_vol = S16_ESP_Motor_BEMF_Voltage_Ref + S16_ESP_Motor_BEMF_ADD_MED_MU; /* med-high mu */           
                  	}                                                                                                                      
                    else                                                                                                                   
                    {                                                                                                                      
                        abs_msc_target_vol = S16_ESP_Motor_BEMF_Voltage_Ref + S16_ESP_Motor_BEMF_ADD_HIGH_MU; /* High mu   */            
                    }                                                                                                                      
                }                                                                                                                          
                else                                                                                                                       
                {                                                                                                                          
                	abs_msc_target_vol = S16_ESP_Motor_BEMF_Voltage_Ref;                                                                 
                }                                                                                                                          
              #else /*__VDC*/                                                                                                              
                if(AFZ_OK==1)                                                                                                              
                {                                                                                                                          
                    if(afz>=-(int16_t)U8_PULSE_MED_HIGH_MU)                                                                                    
                    {                                                                                                                      
                        abs_msc_target_vol = S16_ABS_Motor_BEMF_Voltage_Ref; /* med mu */                                                
                    }                                                                                                                      
                    else if(afz>-(int16_t)U8_PULSE_HIGH_MU)                                                                                    
                    {                                                                                                                      
                        abs_msc_target_vol = S16_ABS_Motor_BEMF_Voltage_Ref + S16_ABS_Motor_BEMF_ADD_MED_MU; /* med-high mu */           
                  	}                                                                                                                      
                    else                                                                                                                   
                    {                                                                                                                      
                        abs_msc_target_vol = S16_ABS_Motor_BEMF_Voltage_Ref + S16_ABS_Motor_BEMF_ADD_HIGH_MU; /* High mu   */            
                    }                                                                                                                      
                }                                                                                                                          
                else                                                                                                                       
                {                                                                                                                          
                	abs_msc_target_vol = S16_ABS_Motor_BEMF_Voltage_Ref;                                                                 
                }                                                                                                                          
              #endif /*__VDC*/
                if((vref < (int16_t)VREF_3_KPH)&&(ABS_fz==1)
                  #if __VDC
                &&(ACTIVE_BRAKE_FLG==0)
                  #endif
                )/*to reduce motor over run noise*/
				{
					abs_msc_target_vol = S16_ABS_MSC_TARGET_VOL_MIN;
				}
				else { }				                                                                                                               
            }
        }
    }
    else
    {
        abs_msc_target_vol = MSC_0_V;
    }

  #if __LPA_TEST
    if (vref >= VREF_3_KPH)
    {
        abs_msc_target_vol = MSC_0_V;
    }
    else
    {

    }
  #endif

}

void LCMSC_vInterfaceTargetvol(void){

    int16_t max_target_vol;
    target_vol_old = target_vol;


    LCMSC_vSetEBDTargetVoltage();

    LCMSC_vSetABSTargetVoltage();

 #if __VDC
  #if __TCS
    LCMSC_vSetTCSTargetVoltage();
  #endif

  #if __VDC
    LCMSC_vSetVDCTargetVoltage();
  #endif
/*08SWD-10*/
#if __Decel_Ctrl_Integ
  #if __DEC
    LCMSC_vSetDECTargetVoltage();
  #endif
#else
  #if __ACC
    LCMSC_vSetACCTargetVoltage();
  #endif

  #if __EPB_INTERFACE
    LCMSC_vSetEPBTargetVoltage();
  #endif
#endif

  #if __HDC
    LCMSC_vSetHDCTargetVoltage();
  #endif

/*  #if __FBC
    LCMSC_vSetFBCTargetVoltage();
  #endif
*/
  #if __BDW
    LCMSC_vSetBDWTargetVoltage();
  #endif

  #if __SCC
    LCMSC_vSetWPCTargetVoltage();
  #endif
  
  #if __EBP
    LCMSC_vSetEBPTargetVoltage();
  #endif

  #if __ENABLE_PREFILL
    LCMSC_vSetEPCTargetVoltage();
  #endif

  #if __TCMF_LowVacuumBoost
    LCMSC_vSetHBBTargetVoltage();
  #endif

  #if __TSP
    LCMSC_vSetTSPTargetVoltage();
  #endif

  #if __TVBB
     LCMSC_vSetTVBBTargetVoltage();
  #endif

   #if __VDC  
/*    LCMSC_vSetPBATargetVoltage();*/
		LCMSC_vSetBATargetVoltage();
  #endif

  #if __HOB
    LCMSC_vSetHOBTargetVoltage();
  #endif

  #if __HRB
    LCMSC_vSetHRBTargetVoltage();
  #endif

  #if __AVH
    LCMSC_vSetAVHTargetVoltage();
  #endif  

  #if __SBC
    LCMSC_vSetSPASTargetVoltage();
  #endif 

  #if __HSA
    LCMSC_vSetHSATargetVoltage();
  #endif 
/*
  #if __ROP
    LCMSC_vSetROPTargetVoltage();
  #endif
*/
  #if __VDC
    LCMSC_vSetAUXTargetVoltage();
  #endif

 #endif
  /* ~#if __VDC */


  /*_______________________ Low Voltage Limit - ABS Only _____________*/

  #if !__LPA_TEST

    if (ABS_MSC_MOTOR_ON==1)
    {
      #if __VDC
        if((lcu16CalVoltBatt2Lf<=MSC_11_V)/* && (lcabsu1EngCrankState==0) */      /* Min  */
          #if __HDC
            &&(lcu1HdcActiveFlg==0)
          #endif
      #else  
        if((ref_mon_ad_lf<=MSC_11_V)        /* Min  */
          #if __HDC
            &&(lcu1HdcActiveFlg==0)
          #endif
      #endif /*__VDC*/
        )
        {
            if (abs_msc_target_vol < MSC_14_V)
            {
                abs_msc_target_vol = MSC_14_V;
            }
            else { }
        }
        else                       /* Min   */
        {
            if (abs_msc_target_vol <= MSC_0_V)
            {
                abs_msc_target_vol = MSC_0_V;
            }
            else if (abs_msc_target_vol < S16_ABS_MSC_TARGET_VOL_MIN)
            {
                abs_msc_target_vol = S16_ABS_MSC_TARGET_VOL_MIN;
            }
            else{ }
        }
    }
    else { }
  #endif

  #if  __VDC
    if (ABS_MSC_MOTOR_ON==1)
    {
        if (VDC_MSC_MOTOR_ON==1)
        {
            if (PARTIAL_BRAKE == 1)
            {
                vdc_msc_target_vol = (int16_t)(((int32_t)abs_msc_target_vol*U8_ABS_ESC_COOP_TAR_VOL_RATIO_PB)/10);
            }
            else
            {
            	vdc_msc_target_vol = (int16_t)(((int32_t)abs_msc_target_vol*U8_ABS_ESC_COOP_TAR_VOL_RATIO)/10);
        	}
        }
        else
        {
/*08SWD-10*/
#if __Decel_Ctrl_Integ
          #if __DEC
            if (DEC_ABS_COMB_MSC_MOTOR_ON==1)
            {
                abs_msc_target_vol = dec_msc_target_vol;
            }
            else {;}
          #endif
#else

          #if __EPB_INTERFACE
            if (EPB_ABS_COMB_MSC_MOTOR_ON==1)
            {
                abs_msc_target_vol = epb_msc_target_vol;
            }
            else {;}
          #endif

          #if __ACC
            if (ACC_ABS_COMB_MSC_MOTOR_ON==1)
            {
                abs_msc_target_vol = acc_msc_target_vol;
            }
            else {;}
          #endif
#endif

          #if __HDC
            if (HDC_ABS_COMB_MSC_MOTOR_ON==1)
            {
                abs_msc_target_vol = hdc_msc_target_vol;
            }
            else { }
          #endif
          
		  #if __SCC
            if (SCC_ABS_COMB_MSC_MOTOR_ON==1)
            {
                abs_msc_target_vol = wpc_msc_target_vol;
            }
            else { }
	      #endif

          #if __TCMF_LowVacuumBoost
            if (HBB_ABS_COMB_MSC_MOTOR_ON==1)
            {
                abs_msc_target_vol = hbb_msc_target_vol;
            }
            else { }
          #endif

            if (PBA_ABS_COMB_MSC_MOTOR_ON==1)
            {
                abs_msc_target_vol = pba_msc_target_vol;
            }
            else { }
        }
    }
    else {}
  #endif


    max_target_vol = MSC_0_V;

    max_target_vol = (max_target_vol>ebd_msc_target_vol)?max_target_vol:ebd_msc_target_vol;
    max_target_vol = (max_target_vol>abs_msc_target_vol)?max_target_vol:abs_msc_target_vol;
  #if __VDC
    max_target_vol = (max_target_vol>tcs_msc_target_vol)?max_target_vol:tcs_msc_target_vol;
    max_target_vol = (max_target_vol>vdc_msc_target_vol)?max_target_vol:vdc_msc_target_vol;
/*08SWD-10*/

#if __Decel_Ctrl_Integ
    max_target_vol = (max_target_vol>dec_msc_target_vol)?max_target_vol:dec_msc_target_vol;
    max_target_vol = (max_target_vol>hdc_msc_target_vol)?max_target_vol:hdc_msc_target_vol;
#else
    max_target_vol = (max_target_vol>rop_msc_target_vol)?max_target_vol:rop_msc_target_vol;

    max_target_vol = (max_target_vol>acc_msc_target_vol)?max_target_vol:acc_msc_target_vol;
    max_target_vol = (max_target_vol>hdc_msc_target_vol)?max_target_vol:hdc_msc_target_vol;
    max_target_vol = (max_target_vol>epb_msc_target_vol)?max_target_vol:epb_msc_target_vol;
#endif
  #if __TSP
    max_target_vol = (max_target_vol>tsp_msc_target_vol)?max_target_vol:tsp_msc_target_vol;
  #endif    	
  #if __TVBB
    max_target_vol = (max_target_vol>tvbb_msc_target_vol)?max_target_vol:tvbb_msc_target_vol;
  #endif
  #if __BDW
    max_target_vol = (max_target_vol>bdw_msc_target_vol)?max_target_vol:bdw_msc_target_vol;
  #endif
    max_target_vol = (max_target_vol>ebp_msc_target_vol)?max_target_vol:ebp_msc_target_vol;
    max_target_vol = (max_target_vol>epc_msc_target_vol)?max_target_vol:epc_msc_target_vol;
#if __TCMF_LowVacuumBoost
    max_target_vol = (max_target_vol>hbb_msc_target_vol)?max_target_vol:hbb_msc_target_vol;
  #endif
  #if __FBC
    max_target_vol = (max_target_vol>fbc_msc_target_vol)?max_target_vol:fbc_msc_target_vol;
  #endif
    max_target_vol = (max_target_vol>pba_msc_target_vol)?max_target_vol:pba_msc_target_vol;
  #if __HOB
    max_target_vol = (max_target_vol>lcs16HobMscTargetV)?max_target_vol:lcs16HobMscTargetV;
  #endif  	
  #if __HRB
    max_target_vol = (max_target_vol>hrb_msc_target_vol)?max_target_vol:hrb_msc_target_vol;
  #endif
#if __AVH
    max_target_vol = (max_target_vol>avh_msc_target_vol)?max_target_vol:avh_msc_target_vol;    	
#endif 
#if __SBC
    max_target_vol = (max_target_vol>spas_msc_target_vol)?max_target_vol:spas_msc_target_vol;
#endif   	
#if __HSA
    max_target_vol = (max_target_vol>hsa_msc_target_vol)?max_target_vol:hsa_msc_target_vol;    	
#endif  
#if __SCC 
    max_target_vol = (max_target_vol>wpc_msc_target_vol)?max_target_vol:wpc_msc_target_vol;
#endif
    max_target_vol = (max_target_vol>aux_msc_target_vol)?max_target_vol:aux_msc_target_vol;

  #endif
  /* ~#if __VDC */

    target_vol = max_target_vol;

#if __HW_TEST_MODE == DISABLE
  #if __VDC
	if((lcu16CalVoltBatt2Lf<=MSC_10_V)/* && (lcabsu1EngCrankState==0) */)   /* Min  */
  #else  
    if(ref_mon_ad_lf<=MSC_10_V)   /* Min  */
  #endif /*__VDC*/
    {
        if ((target_vol > MSC_0_V)&&(target_vol < MSC_14_V))
        {
            target_vol = MSC_14_V;
        }
        else { }
    }
    else { }
#endif    
    
    

}

#if __VDC   /* LCMSC_vSetAUXInput 관련부분은 VDC에서만 작동 */
void LCMSC_vSetAUXInput(void){

 #if __VALVE_TEST_L || __VALVE_TEST_E || __VALVE_TEST_B || __BLEEDING_E || __VALVE_TEST_CPS || __VALVE_TEST_F
    if((fu1ESCDisabledBySW==1)&& (VDC_MOTOR_ON==1))
    {
        AUX_MSC_MOTOR_ON=1;
    }
    else
    {
        AUX_MSC_MOTOR_ON=0;
    }
 #elif (__HW_TEST_MODE == ENABLE)
    if ((HW_test_cnt_t>0) && (VDC_MOTOR_ON==1))
    {
        AUX_MSC_MOTOR_ON=1;
    }
    else
    {
        AUX_MSC_MOTOR_ON=0;
    }
 #else
        AUX_MSC_MOTOR_ON=0;
 #endif
}

void LCMSC_vSetAUXTargetVoltage(void)
{
  #if __VALVE_TEST_L || __VALVE_TEST_E || __VALVE_TEST_B || __BLEEDING_E

        aux_msc_target_vol = MSC_0_V;

    if((AUX_MSC_MOTOR_ON==1)&&(fu1ESCDisabledBySW==1)&&(VDC_MOTOR_ON==1))
    {
        aux_msc_target_vol= ESC_VV_TEST_MOTOR_TARGET_VOLT;
    }
    else
    {
        aux_msc_target_vol = MSC_0_V;
    }
  #else
  
  #if __VALVE_TEST_F 

        aux_msc_target_vol = MSC_0_V;

    if((AUX_MSC_MOTOR_ON==1)&&(fu1ESCDisabledBySW==1)&&(VDC_MOTOR_ON==1))
    {
        aux_msc_target_vol= MSC_4_V;
    }
    else
    {
        aux_msc_target_vol = MSC_0_V;
    }
  #endif
  
  #if __VALVE_TEST_CPS
  
  	aux_msc_target_vol = MSC_0_V;
  	
  	if((AUX_MSC_MOTOR_ON==1)&&(fu1ESCDisabledBySW==1)&&(VDC_MOTOR_ON==1))
    {
        if (PREFILL_TEST_FLAG ==1)
        {
        	aux_msc_target_vol=S16_PRE_FILL_TARGET_VOLTAGE;
        }
        else
        {
        	aux_msc_target_vol=MSC_14_V;
        }	        		
    }
    else
    {
        aux_msc_target_vol = MSC_0_V;
    }
  
  #endif
  
   #if __HW_TEST_MODE
    LAABS_vTestMotor();
   #endif  
  #endif
}

void LCMSC_vActValveCommand(void){
    HV_VL_fl = MSC_HV_VL_fl;
    AV_VL_fl = MSC_AV_VL_fl;
    HV_VL_fr = MSC_HV_VL_fr;
    AV_VL_fr = MSC_AV_VL_fr;
    HV_VL_rl = MSC_HV_VL_rl;
    AV_VL_rl = MSC_AV_VL_rl;
    HV_VL_rr = MSC_HV_VL_rr;
    AV_VL_rr = MSC_AV_VL_rr;

  #if __SPLIT_TYPE                      /*FR Split  */
    S_VALVE_SECONDARY    = MSC_S_VALVE_SECONDARY;
    S_VALVE_PRIMARY      = MSC_S_VALVE_PRIMARY;
    TCL_DEMAND_SECONDARY = MSC_TCL_DEMAND_SECONDARY;
    TCL_DEMAND_PRIMARY   = MSC_TCL_DEMAND_PRIMARY;
  #else                                 /*X Split  */
    S_VALVE_LEFT  = MSC_S_VALVE_SECONDARY;
    S_VALVE_RIGHT = MSC_S_VALVE_PRIMARY;
    TCL_DEMAND_fl = MSC_TCL_DEMAND_SECONDARY;
    TCL_DEMAND_fr = MSC_TCL_DEMAND_PRIMARY;
  #endif

}
#endif  /* ~~__VDC LCMSC_vSetAUXInput 관련부분은 VDC에서만 작동 */       



void LCMSC_vCalTargetVoltage(void)
{

  #if (!__REORGANIZE_FOR_MGH60_CYCLETIM)
    uint32_t target_vol_temp;
  #else
    uint16_t target_vol_temp;
  #endif 

  #if __VDC
    if(MSC_indicator>((int16_t)U8_MSC_Activate_Indicator_ABS2))
    {
        /* Default Value is IGN 4V up to 7scan dump */
        
	  #if (!__REORGANIZE_FOR_MGH60_CYCLETIM)
        target_vol_temp = ((uint32_t)target_vol*MSC_indicator/U8_MSC_Activate_Indicator_ABS2);
	  #else
        target_vol_temp = (uint16_t)((uint32_t)target_vol*MSC_indicator/U8_MSC_Activate_Indicator_ABS2);
	  #endif 
        
        if( target_vol_temp >= MSC_14_V )
        {
            target_vol = MSC_14_V;
        }
        else
        {
        	target_vol = (int16_t)target_vol_temp;
        }
    }
    else { }
  #else
    if(MSC_indicator>((int16_t)U8_MSC_Activate_Indicator))
    {
        /* Default Value is IGN 4V up to 7scan dump */
        
        target_vol_temp = ((uint32_t)target_vol*MSC_indicator/U8_MSC_Activate_Indicator);
        
        if( target_vol_temp >= MSC_14_V )
        {
        	target_vol = MSC_14_V;
        }
        else
        {
        	target_vol = (int16_t)target_vol_temp;
        }
    }
    else { }
  #endif
	
    if (target_vol <= MSC_0_1_V)
    {
        target_vol = MSC_0_V;
    }
    else if (target_vol > MSC_14_V)
    {
        target_vol = MSC_14_V;
    }
    else { }
}

  #if (__ESC_MOTOR_PWM_CONTROL == ENABLE)
void LCMSC_vSetInitialontime(void)
{
    if ((motor_on_time_add_cnt==0)||((target_vol_old == MSC_0_V)&&(target_vol >= MSC_0_1_V))) /* 2011.9.8 LJK */
    {
		laabsu8MscOnScan = U8_MSC_PERIOD_SCAN - U8_MSC_MOTOR_OFF_SCAN;
                                                                     
        lau8MscDuty_old = (uint8_t)LCESP_s16IInter4Point(target_vol ,	MSC_0_5_V, U8_MSC_DUTY_MIN, 
					                                                    MSC_2_V,    50, 
					                                                    MSC_8_V,   140, 
					                                                    MSC_14_V,  U8_MSC_DUTY_MAX);
    }
    else{ }
}
 /*(__ESC_MOTOR_PWM_CONTROL==ENABLE)*/
static void LCMSC_vCompMscDuty(void)
{
	int16_t	s16MscTargetVDiffAbs;
	int16_t	s16MscCompDuty, s16MscDutyNew;
	
	s16MscTargetVDiffAbs = McrAbs(target_vol - target_vol_old);
	
	if((s16MscTargetVDiffAbs >= MSC_2_V)&&(target_vol_old > 0))
	{
/*		las16MscCompTemp2 = LCABS_s16Interpolation2P(target_vol, MSC_6_V, MSC_14_V, 30, 100);*/
		if(target_vol > target_vol_old)
		{
			s16MscCompDuty = LCABS_s16Interpolation2P(s16MscTargetVDiffAbs, MSC_2_V, MSC_12_V, 20, 100);
		}
		else
		{
			s16MscCompDuty = LCABS_s16Interpolation2P(s16MscTargetVDiffAbs, MSC_2_V, MSC_12_V, -10, -80);
		}
			
		s16MscDutyNew   = (int16_t)lau8MscDuty_old + s16MscCompDuty;
		lau8MscDuty_old = (uint8_t)LCABS_s16LimitMinMax(s16MscDutyNew, (int16_t)U8_MSC_DUTY_MIN, (int16_t)U8_MSC_DUTY_MAX);/*duty range by ASIC spec:10%~90%*/		
	}
}
 /*(__ESC_MOTOR_PWM_CONTROL==ENABLE)*/
static void LCMSC_vMscDutyOutput(void)
{
	/*int8_t las8MSCAdaptDuty;   */
	int16_t las16MSCTemp;
	int16_t las16MSCTemp2;
	int16_t las16MSCMinDuty;
	
	if ((target_vol < MSC_14_V)&&(target_vol > MSC_0_V))
    {
	  	if (laabsu8MscScanCounter < laabsu8MscOnScan)
	  	{
	  		lau8MscSetDuty = lau8MscDuty_old;
	  		laabsu8MscScanCounter = laabsu8MscScanCounter + 1;
	  	}
	  	else if (laabsu8MscScanCounter < U8_MSC_PERIOD_SCAN)
	    {
	    	lau8MscSetDuty = 0;
	    	laabsu8MscScanCounter = laabsu8MscScanCounter + 1;
	    }
	    else if (laabsu8MscScanCounter >= U8_MSC_PERIOD_SCAN)
	    {	    	
	    	laabsu8MscScanCounter = 0;	    	    	
	    	
    	    las16MSCTemp = target_vol - laabss16BemfForDutyAdapt;
                        
            if((las16MSCTemp >= MSC_0_5_V)||(las16MSCTemp <= -MSC_0_5_V)) /*dead zone: no adatation between +- 0.5V*/
            {
            	las8MSCAdaptDuty = (int8_t)(las16MSCTemp/S16_COMP_VOLT_FOR_DUTY_ADAPT); /*duty adaptation: 5duty/1V*/
            }
            else
            {
        		las8MSCAdaptDuty = 0;  
            }

        	las8MSCAdaptDuty = (int8_t)LCABS_s16LimitMinMax((int16_t)las8MSCAdaptDuty, MSC_ADAPT_DUTY_MIN, MSC_ADAPT_DUTY_MAX);
            	
			las16MSCTemp2 = (int16_t)lau8MscDuty_old + (int16_t)las8MSCAdaptDuty; /* adaptation disable */
			
			las16MSCMinDuty = LCESP_s16IInter4Point(target_vol ,	MSC_0_5_V, U8_MSC_DUTY_MIN, 
					                                                MSC_2_V,    50, 
					                                                MSC_8_V,   140, 
					                                                MSC_14_V,  U8_MSC_DUTY_MAX);
					                                                
			lau8MscDuty_old = (uint8_t)LCABS_s16LimitMinMax(las16MSCTemp2, las16MSCMinDuty, (int16_t)U8_MSC_DUTY_MAX);

		    lau8MscSetDuty = lau8MscDuty_old;
		    laabsu8MscScanCounter = laabsu8MscScanCounter + 1;
	    }
	    else
	    {
			lau8MscSetDuty = 0;
			laabsu8MscScanCounter = 0;
	    }
	    
	  #if ((__MSC_DIRECT_DUTY_CONTROL == ENABLE) && (__SCC == ENABLE))
		if((ADVANCED_MSC_ON_FF==1)&&(ADVANCED_MSC_ON_FB==0))
        {
      	    lau8MscDuty_old = (uint8_t)((int32_t)wpc_msc_target_vol*200/MSC_14_V);
      	    lau8MscDuty_old = (uint8_t)LCABS_s16LimitMinMax(lau8MscDuty_old, (int16_t)U8_MSC_DUTY_MIN, (int16_t)U8_MSC_DUTY_MAX);
      	    lau8MscSetDuty = lau8MscDuty_old;
        }
        else{} 
      #endif  
	    
	  #if (__HW_TEST_MODE ==ENABLE)
	    if ((HW_test_cnt_t>0) && (VDC_MOTOR_ON==1) && (lau8HwTestMscDuty>0) && (lau8MscSetDuty>0))
	    {
	    	lau8MscSetDuty = lau8HwTestMscDuty;
	    }
	  #endif  
	    	
    }
	/*------------------------------------------------------------강제구동    */
    else if (target_vol >= MSC_14_V)
    { 
        lau8MscDuty_old = 200;/*U8_MSC_DUTY_MAX;*/
        lau8MscSetDuty = lau8MscDuty_old;
    }
    else
    { 
        lau8MscSetDuty = 0;
        laabsu8MscScanCounter = 0;
    } 
}

  #else /*(__ESC_MOTOR_PWM_CONTROL == ENABLE)*/
void LCMSC_vSetInitialontime(void){
/* ADVANCED MSC Inital On Time set [msec].*/

    if ((motor_on_time_add_cnt==0)||
		((target_vol_old == MSC_0_V)&&(target_vol >= MSC_0_5_V)&&((int16_t)fu16CalVoltIntMOTOR < MSC_0_25_V)))
    {

        motor_on_time_msec      =  (int16_t)LCESP_s16IInter4Point(target_vol ,MSC_2_V,   3,
		                                                                      MSC_6_V,  10,
		                                                                      MSC_14_V, 21,
		                                                                      MSC_14_V, 21);

        int_motor_on_time_msec  =  (int16_t)LCESP_s16IInter4Point(target_vol ,MSC_2_V,   7,
		                                                                      MSC_6_V,  21,
		                                                                      MSC_8_V,  28,
		                                                                      MSC_14_V, 35);

        int_add_motor_on_time_msec = int_motor_on_time_msec - motor_on_time_msec;

    }
    else { }
}  
  #endif /*(__ESC_MOTOR_PWM_CONTROL == ENABLE)*/
  
void LCMSC_vCountDump(void){

    if ((FL.pwm_duty_temp_dash==201)||(FR.pwm_duty_temp_dash==201))
    {
        dump_cnt = 5;
    }
    else if ((dump_cnt < 3)&&((RL.pwm_duty_temp_dash==201)||(RR.pwm_duty_temp_dash==201)))
    {
        dump_cnt = 3;
    }
    else if (dump_cnt > 0)
    {
        dump_cnt = dump_cnt - 1;
    }
    else {}

}

void LCMSC_vSetMinMaxontime(void){

#if (__ESC_MOTOR_PWM_CONTROL==ENABLE)
    Max_motor_on_time_msec = 36;    /* Duty Max 90% */
#else
	Max_motor_on_time_msec = 96;    /* Duty Max 96% */
#endif

  #if __VDC
    if ((FL.pwm_duty_temp_dash==202)||(FR.pwm_duty_temp_dash==202)
        ||(RL.pwm_duty_temp_dash==202)||(RR.pwm_duty_temp_dash==202))
    {
        msc_tempW9 = 6;
    }
    else
    {
        msc_tempW9 = 8;
    }
  #else
    msc_tempW9 = 8;
  #endif

    Min_motor_on_time_msec= LCABS_s16Interpolation2P(target_vol, MSC_2_V, MSC_10_V, 3, msc_tempW9);

    if ((dump_cnt > 0)&&(Min_motor_on_time_msec<6))
    {
        Min_motor_on_time_msec =6;
    }
    else { }

  #if __VDC
    /* Wheel Pressure Comp  */
    /*
    if ((FL.s16_Estimated_Active_Press > MPRESS_120BAR)
        ||(FR.s16_Estimated_Active_Press > MPRESS_120BAR)
        ||(RL.s16_Estimated_Active_Press > MPRESS_120BAR)
        ||(RR.s16_Estimated_Active_Press > MPRESS_120BAR))
    {
        if (Min_motor_on_time_msec < 5)
        {
            Min_motor_on_time_msec = 5;
        }
        else { }
    }
    else if ((FL.s16_Estimated_Active_Press > MPRESS_80BAR)
        ||(FR.s16_Estimated_Active_Press > MPRESS_80BAR)
        ||(RL.s16_Estimated_Active_Press > MPRESS_80BAR)
        ||(RR.s16_Estimated_Active_Press > MPRESS_80BAR))
    {
        if (Min_motor_on_time_msec < 4)
        {
            Min_motor_on_time_msec = 4;
        }
        else { }
    }
    else if ((FL.s16_Estimated_Active_Press > MPRESS_40BAR)
        ||(FR.s16_Estimated_Active_Press > MPRESS_40BAR)
        ||(RL.s16_Estimated_Active_Press > MPRESS_40BAR)
        ||(RR.s16_Estimated_Active_Press > MPRESS_40BAR))
    {
        if (Min_motor_on_time_msec < 3)
        {
            Min_motor_on_time_msec = 3;
        }
        else { }
    }
    else
    {
        Min_motor_on_time_msec = Min_motor_on_time_msec;
    }
  */
  #endif

}


void LCMSC_vSetAdvancedMSC(void){

/*    모터 구동 Flag Set    */
   if (EBD_MSC_MOTOR_ON
      ||ABS_MSC_MOTOR_ON
    #if __VDC
      ||TCS_MSC_MOTOR_ON
      ||VDC_MSC_MOTOR_ON

/*08SWD-10*/
#if __Decel_Ctrl_Integ
      ||DEC_MSC_MOTOR_ON
#else
      ||EPB_MSC_MOTOR_ON
      ||ACC_MSC_MOTOR_ON
#endif
      ||HDC_MSC_MOTOR_ON

      ||FBC_MSC_MOTOR_ON
      ||BDW_MSC_MOTOR_ON

    #if __TSP   
      ||TSP_MSC_MOTOR_ON
    #endif
    #if __TVBB
      ||TVBB_MSC_MOTOR_ON
    #endif
      ||EBP_MSC_MOTOR_ON
      ||EPC_MSC_MOTOR_ON
      ||ROP_MSC_MOTOR_ON
      ||HBB_MSC_MOTOR_ON
      ||PBA_MSC_MOTOR_ON
      ||HRB_MSC_MOTOR_ON
    #if __AVH
      ||AVH_MSC_MOTOR_ON
    #endif
    #if __SBC
      ||SPAS_MOTOR_ON
    #endif
    #if __HSA
      ||HSA_MSC_MOTOR_ON
    #endif
    #if __SCC  && (__MSC_DIRECT_DUTY_CONTROL==DISABLE)
      ||WPC_MSC_MOTOR_ON
    #endif
    #if __HOB
      ||lcu1HobMotorOnFlag
    #endif
      ||AUX_MSC_MOTOR_ON
    #endif
      )
    {
        motor_on_time_add_cnt = 2;
        ADVANCED_MSC_ON_FB = 1;
    }
    else if (motor_on_time_add_cnt>0)
    {
        motor_on_time_add_cnt--;
        ADVANCED_MSC_ON_FB = 1;
    }
    else
    { 
    	ADVANCED_MSC_ON_FB = 0;
    }

  #if (__MSC_DIRECT_DUTY_CONTROL==ENABLE)
    msc_tempW4 = 0;
    
    if((msc_tempW4==1)
    #if __SCC  
      ||WPC_MSC_MOTOR_ON
    #endif	
    )
    {
    	ADVANCED_MSC_ON_FF = 1;
    }
    else
    {
    	ADVANCED_MSC_ON_FF = 0;
    }
  #endif

    if ( (ebd_error_flg==0)&&(fu1MotErrFlg==0)&&((ADVANCED_MSC_ON_FB==1)||(ADVANCED_MSC_ON_FF==1)) )
    {
        ADVANCED_MSC_ON_FLG =1;
    }
    else
    {
        ADVANCED_MSC_ON_FLG =0;
      #if (__ESC_MOTOR_PWM_CONTROL==ENABLE)
        lau8MscDuty=0;
      #endif
        msec_counter = 0;    			 /* 초기화            */
    }

    /* Max 1cycle - ADVANCED_MSC_counter */
    if (msec_counter>1000)
    {
        msec_counter=1000;
    }
    else { }

}

  #if (__ESC_MOTOR_PWM_CONTROL==ENABLE)
uint8_t LAMSC_vCallActHW_interupt(uint16_t mtp_ad)
{
    lau8MscDuty = lau8MscSetDuty;

	  #if (__BEMF_LPF_ENABLE == ENABLE)
	laabs16MtpAdFiltered = (int16_t)((((int32_t)mtp_ad)*((int16_t)L_U8FILTER_GAIN_10MSLOOP_10HZ)+(((int32_t)laabs16MtpAdFiltered)*(128-(int16_t)L_U8FILTER_GAIN_10MSLOOP_10HZ)))/128);
	  #endif
	  
	if((target_vol > 0) && (lau8MscDuty == 0))
	{
		if(msec_counter<=((int16_t)U8_MSC_BEMF_MONITOR_MS-1))/*BEMF checking from 8ms of MSC off-period*/
		{
			msec_counter = msec_counter + 1;			
		}
		else if(msec_counter<=(int16_t)U8_MSC_BEMF_MONITOR_MS)
		{
		  #if (__BEMF_LPF_ENABLE == ENABLE)
			laabss16BemfForDutyAdapt = laabs16MtpAdFiltered;
		  #else
		  	laabss16BemfForDutyAdapt = (int16_t)mtp_ad;
		  #endif
		  
            mot_mon_ad_msec = (int16_t)mtp_ad;
            max_motor_mon_ad = (int16_t)mtp_ad;
			
			msec_counter = msec_counter + 1;
		}
		else
		{
			msec_counter = msec_counter + 1;
		}
	}
	else
	{	
		if(target_vol==0)
		{	
			laabss16BemfForDutyAdapt = 0;
            mot_mon_ad_msec = (int16_t)mtp_ad;
            max_motor_mon_ad = (int16_t)mtp_ad;
		}
		msec_counter = 0;		
	}	
		
  #if __MOTOR_MON==1
  	  #if (__BEMF_LPF_ENABLE == ENABLE)
    MonMotorAD_temp[Mon1mscnt] = laabs16MtpAdFiltered;
      #else
	MonMotorAD_temp[Mon1mscnt] = (int16_t)mtp_ad;
	  #endif
	  
    if(lau8MscDuty == 0) MonMotorAD_temp[Mon1mscnt] *= -1;
    Mon1mscnt++;
    if (Mon1mscnt >= L_MSC_ACT_PERIOD)
    { 
        Mon1mscnt = 0;
        MonMotorAD[0] = MonMotorAD_temp[0];
        MonMotorAD[1] = MonMotorAD_temp[1];
        MonMotorAD[2] = MonMotorAD_temp[2];
        MonMotorAD[3] = MonMotorAD_temp[3];
        MonMotorAD[4] = MonMotorAD_temp[4];
        MonMotorAD[5] = MonMotorAD_temp[5];
        MonMotorAD[6] = MonMotorAD_temp[6];
        MonMotorAD[7] = MonMotorAD_temp[7];
        MonMotorAD[8] = MonMotorAD_temp[8];
        MonMotorAD[9] = MonMotorAD_temp[9];        
    } 
    else{}
  #endif
 
    if (lau8MscDuty_old >= U8_MSC_DUTY_MAX )
    { 
         lcu8_motor_on_cnt++;
    } 
    else 
    {
    	;
    }
    
    return lau8MscDuty;
}
  #else/*__ESC_MOTOR_PWM_CONTROL*/
uint8_t LAMSC_vCallActHW_interupt(uint16_t mtp_ad)
{
    uint8_t ADVANCED_MSC_MOTOR_ON_FLG;

/* -------------------------------------------1msec control   */
    if ((target_vol < MSC_14_V)&&(target_vol > MSC_0_V))
    {
	  #if (__HW_TEST_MODE == ENABLE)
	      #if __VDC
	    if ((HW_test_cnt_t>0) && (las16HwTestMscOnTimeMsec>0))
	    {
	    	if (msec_counter<las16HwTestMscOnTimeMsec)
	    	{
	            ADVANCED_MSC_MOTOR_ON_FLG=1;
	            msec_counter++;
	        }
	        else 
	        {
	            ADVANCED_MSC_MOTOR_ON_FLG=0;
	            msec_counter++;
	            if(msec_counter == (las16HwTestMscOnTimeMsec+las16HwTestMscOffTimeMsec))
	            {
	            	msec_counter = 0;
	            }
	        }	
	    }
	    else
	     #endif	
	  #endif  
        if (msec_counter<motor_on_time_msec)
        {
            if (int_add_motor_on_time_msec > 0)
            {
                int_add_motor_on_time_msec --;
                msec_counter=0;
            }
            else { }

            ADVANCED_MSC_MOTOR_ON_FLG=1;
            msec_counter++;
        }
        else if (msec_counter < (motor_on_time_msec+1))
        {
            ADVANCED_MSC_MOTOR_ON_FLG=0;
            msec_counter++;
        }
        else if (msec_counter >= (motor_on_time_msec+4))
        {
            msec_counter++;

            mot_mon_ad_msec = (int16_t)mtp_ad;/*mtp_ad;*/
            max_motor_mon_ad = (int16_t)mtp_ad;/*mtp_ad;*/

            msc_off_timer = msec_counter-motor_on_time_msec;

            if ((int16_t)mtp_ad > target_vol)
            {
                ADVANCED_MSC_MOTOR_ON_FLG=0;
            }
            else
            {
                /* Most suitable Off Time > 7msec */
                if (msc_off_timer<=5)
                {
                    if ((int16_t)mtp_ad > ((int16_t)target_vol-MSC_3_V))
                    {
                        add_motor_on_time_msec=3;
                    }
                    else if ((int16_t)mtp_ad > ((int16_t)target_vol-MSC_4_V))
                    {
                        add_motor_on_time_msec=4;
                    }
                    else
                    {
                        add_motor_on_time_msec=5;
                    }

                    Max_add_motor_on_time_msec = (motor_on_time_msec+1)/2;

                    if (((int16_t)mtp_ad > ((int16_t)target_vol-MSC_2_V))&&(add_motor_on_time_msec > Max_add_motor_on_time_msec))
                    {
                        add_motor_on_time_msec = Max_add_motor_on_time_msec;
                    }
                    else { }

                    motor_on_time_msec=motor_on_time_msec+add_motor_on_time_msec;
                }
                else
                {
                    if (msc_off_timer<=7)       /* 제한 조건 7 msec 이상이면 제어 주기 증가. 최소 10%   */
                    {
                        if (motor_on_time_msec<15)
                        {
                            add_motor_on_time_msec=1;
                        }
                        else if (motor_on_time_msec<25)
                        {
                            add_motor_on_time_msec=2;
                        }
                        else
                        {
                            add_motor_on_time_msec=3;
                        }
                    }
                    else
                    {
                        add_motor_on_time_msec=0;
                    }

                    /* Motor Control Frequency < 50Hz(20ms + 3ms) */
                    /*  제어 주기에 따라서 증가만 가능한 모드   */
                    if (msec_counter<30)                        /* Max +2   */
                    {
                        if (msec_counter<16)
                        {
                            add_motor_on_time_msec=add_motor_on_time_msec+2;
                        }
                        else if (msec_counter<23)
                        {
                            add_motor_on_time_msec=add_motor_on_time_msec+1;
                        }
                        else { }
                    }
                    else if (msec_counter<50) /*Max 제어 주기 + 7msec (증감 가능함.)        Max + 2 , Min -3    */
                    {
                        if (msc_off_timer <= motor_on_time_msec)        /*Duty 50% 이상 Off Time 최적화 (Off 15msec 이상)   */
                        {
                            if (msc_off_timer<=14)
                            {
                                /*motor_on_time_msec=motor_on_time_msec+1;  */
                            }
                            else if (msc_off_timer<=21)
                            {
                                add_motor_on_time_msec=add_motor_on_time_msec-1;
                            }
                            else
                            {
                                add_motor_on_time_msec=add_motor_on_time_msec-2;
                            }
                        }
                        else    /* Duty 50% 미만 제어 주기 20msec 이상 제어주기 최적화  */
                        {
                            if (msec_counter<37)
                            {
                                add_motor_on_time_msec=add_motor_on_time_msec-1;
                            }
                            else if (msec_counter<44)
                            {
                                add_motor_on_time_msec=add_motor_on_time_msec-2;
                            }
                            else
                            {
                                add_motor_on_time_msec=add_motor_on_time_msec-3;
                            }
                        }
                    }
                    else    /* Max 제어 주기 이상. (감소만) */
                    {
                        if (msc_off_timer<=21)
                        {
                            add_motor_on_time_msec=add_motor_on_time_msec-1;
                        }
                        else if (msc_off_timer<=28)
                        {
                            add_motor_on_time_msec=add_motor_on_time_msec-2;
                        }
                        else if (msc_off_timer<=35)
                        {
                            add_motor_on_time_msec=add_motor_on_time_msec-3;
                        }
                        else
                        {
                            Min_motor_on_time_msec = 4;
                            add_motor_on_time_msec = -((motor_on_time_msec+1)/2)+2;

                            if (msc_off_timer>=70)
                            {
                                add_motor_on_time_msec = -(motor_on_time_msec-(motor_on_time_msec+2)/4)+2;
                            }
                        }
                    }

                    if ((int16_t)mtp_ad > (target_vol - MSC_0_25_V))
                    {
                       ;
                    }
                    else if ((int16_t)mtp_ad > ((int16_t)target_vol-MSC_1_V))
                    {
                        add_motor_on_time_msec=add_motor_on_time_msec+1;
                    }
                    else if ((int16_t)mtp_ad > ((int16_t)target_vol-MSC_2_V))
                    {
                        add_motor_on_time_msec=add_motor_on_time_msec+2;
                    }
                    else
                    {
                        add_motor_on_time_msec=add_motor_on_time_msec+3;
                    }

                    Max_add_motor_on_time_msec = (motor_on_time_msec+1)/2;

                    if (((int16_t)mtp_ad > ((int16_t)target_vol-MSC_2_V))&&(add_motor_on_time_msec > Max_add_motor_on_time_msec))
                    {
                        add_motor_on_time_msec=Max_add_motor_on_time_msec;
                    }
                    else { }

                    motor_on_time_msec=motor_on_time_msec+add_motor_on_time_msec;

                }
                /*  주기 확인하기   */
                if (msec_counter_max < (msec_counter-1)) msec_counter_max =(msec_counter-1);

                msec_counter=1;
                ADVANCED_MSC_MOTOR_ON_FLG=1;
            }

            if (motor_on_time_msec < Min_motor_on_time_msec)
            {
                motor_on_time_msec = Min_motor_on_time_msec;
            }
            else if (motor_on_time_msec > Max_motor_on_time_msec)
            {
                motor_on_time_msec = Max_motor_on_time_msec;
            }
            else { }
            
/*            motor_on_time_msec = LCABS_s16LimitMinMax(motor_on_time_msec, Min_motor_on_time_msec, Max_motor_on_time_msec); */

        }
        else
        {
            msec_counter++;
            ADVANCED_MSC_MOTOR_ON_FLG=0;
        }
    }
/*------------------------------------------------------------강제구동    */
    else if (target_vol >= MSC_14_V)
    {
        ADVANCED_MSC_MOTOR_ON_FLG=1;
        msec_counter=motor_on_time_msec;
    }
    else
    {
        ADVANCED_MSC_MOTOR_ON_FLG=0;
        if ( (int16_t)mtp_ad < MSC_0_25_V )
        {
            msec_counter=0;
        }
        else
        {
            msec_counter++;
        }
        mot_mon_ad_msec = (int16_t)mtp_ad;
        max_motor_mon_ad = (int16_t)mtp_ad;
    }

  #if __MOTOR_MON==1
    MonMotorAD_temp[Mon1mscnt] = (int16_t)mtp_ad;
    if(ADVANCED_MSC_MOTOR_ON_FLG == 0) MonMotorAD_temp[Mon1mscnt] *= -1;
    Mon1mscnt++;
    if (Mon1mscnt >= L_MSC_ACT_PERIOD)
    {
        Mon1mscnt = 0;
        MonMotorAD[0] = MonMotorAD_temp[0];
        MonMotorAD[1] = MonMotorAD_temp[1];
        MonMotorAD[2] = MonMotorAD_temp[2];
        MonMotorAD[3] = MonMotorAD_temp[3];
        MonMotorAD[4] = MonMotorAD_temp[4];
        MonMotorAD[5] = MonMotorAD_temp[5];
        MonMotorAD[6] = MonMotorAD_temp[6];
        MonMotorAD[7] = MonMotorAD_temp[7];
        MonMotorAD[8] = MonMotorAD_temp[8];
        MonMotorAD[9] = MonMotorAD_temp[9];
    }
    else{ }
  #endif

    if (ADVANCED_MSC_MOTOR_ON_FLG ==1 )
    {
         lcu8_motor_on_cnt++;
    }
    else {}

    return ADVANCED_MSC_MOTOR_ON_FLG;
}
  #endif/*(__ESC_MOTOR_PWM_CONTROL==ENABLE)*/
  
#endif /*__ADVANCED_MSC */

#if __VDC
void LDMSC_vEstimateMotorRPM(void)
{

    motor_speedup_rate =  LCESP_s16IInter2Point(mot_mon_ad_lf ,MSC_0_V, MSC_6_V,
                                                               MSC_9_V, MSC_0_25_V );

    if (mot_mon_ad_msec >= MSC_0_5_V)
    {
        msc_tempW0 = mot_mon_ad_msec;
        if (msc_tempW0 >= MSC_12_V)
        {
            msc_tempW0 = MSC_12_V;
        }
        else
        {
            ;
        }
        mot_mon_ad_lf = LCESP_s16Lpf1Int(msc_tempW0, mot_mon_ad_lf, L_U8FILTER_GAIN_5MSLOOP_22_5HZ);
    }
    else
    {
		if ((int16_t)fu16CalVoltIntMOTOR >=MSC_9_V)
        {
            msc_tempW0 = mot_mon_ad_lf + motor_speedup_rate;
        }
        else
        {
            msc_tempW0 = (int16_t)fu16CalVoltIntMOTOR;
        }

        if (msc_tempW0 >= MSC_12_V)
        {
            msc_tempW0 = MSC_12_V;
        }
        else
        {
            ;
        }
        /* mot_mon_ad_lf = LCESP_s16Lpf1Int(msc_tempW0, mot_mon_ad_lf, 16); */
        mot_mon_ad_lf = LCESP_s16Lpf1Int(msc_tempW0, mot_mon_ad_lf, L_U8FILTER_GAIN_5MSLOOP_22_5HZ);

    }

    if (mot_mon_ad_lf > MSC_0_25_V)
    {
        /*
          BEMF 계측에 의한 수식 적용
          motor_rpm = mot_mon_ad_lf * 448 / 51;
        */
        motor_rpm = (int16_t)((((int32_t)mot_mon_ad_lf)*448)/MSC_1_V); /*09SW*, 448 mot_mon_ad*/
    }
    else
    {
        motor_rpm = 0;
    }

/*
    #if __CAR == HMC_JM
        max_rpm = MSC_4000_RPM;
    #elif __CAR == SYC_KYRON
        max_rpm = MSC_4500_RPM;
    #elif __CAR == SYC_RAXTON
        max_rpm = MSC_4500_RPM;
    #elif __CAR == HMC_NF
        max_rpm = MSC_3500_RPM;
    #else
        max_rpm = MSC_4000_RPM;
    #endif

    tempW2=max_motor_mon_ad;
    tempW1=448;
    tempW0=51;
    s16muls16divs16();
    max_rpm_ad = tempW3;

    if (max_rpm_ad > max_rpm )
    {
        max_rpm = max_rpm_ad;
    }
    else {;}
*/
  #if __VDC
    max_rpm = (int16_t)((((int32_t)lcu16CalVoltBatt2Lf)*340)/MSC_1_V);
  #else
    max_rpm = (int16_t)((((int32_t)ref_mon_ad_lf)*340)/MSC_1_V); /*09SW*/
  #endif


    if (max_rpm > MSC_4500_RPM)
    {
        max_rpm = MSC_4500_RPM;
    }
    else if (max_rpm < MSC_3500_RPM)
    {
        max_rpm = MSC_3500_RPM;
    }
    else { }

    if (motor_rpm>max_rpm)
    {
        motor_rpm = max_rpm;
    }
    else if (motor_rpm <= 0)
    {
        motor_rpm = 0;
    }
    else{;}
    mot_mon_ad_msec = MSC_0_V;
}
#endif

#if (__DELAY_MOTOR_DRV_AT_LOW_MP == ENABLE)

static void LCMSC_vDecideMtrDrvCondition(void)
{
    int16_t s16MCPSlope;
    int16_t s16DelayMtrDrvMpress;
    int16_t s16DelayMtrDrvMPSlope;
    uint8_t u8LowBrakeInput;
    uint8_t u8LPAFillDumpMax;
    
/*
    static uint8_t lcmscu8LPAFillCounterP;
    static uint8_t lcmscu8LPAFillCounterS;
    static uint8_t lcmscu8BrakingTimeBfABS;
*/

  /* --- Check ESV actuation w/o braking --- */

    #if (__SPLIT_TYPE==0)
    if((S_VALVE_RIGHT==1) && (S_VALVE_LEFT==1) && (lau8_S_VALVE_PRIMARY_in_cnt>=L_U8_TIME_10MSLOOP_500MS))
    #else
    if((S_VALVE_PRIMARY==1) && (S_VALVE_SECONDARY==1) && (lau8_S_VALVE_PRIMARY_in_cnt>=L_U8_TIME_10MSLOOP_500MS))
    #endif
    {
        lamscu1ESVActWoBraking = 1;
    }
    else
    {
        lamscu1ESVActWoBraking = 0;
    }
  
    if((lamscu1ESVActivatedWoBrkBfABS == 0) && (lamscu1ABSAfESVActWoBraking==0))
    {
        if(lamscu1ESVActWoBraking==1)
        {
            lamscu1ESVActivatedWoBrkBfABS = 1;
        }
    }
    else
    {
        if((ABS_fz==1)&&(ACTIVE_BRAKE_FLG==0))
        {
            lamscu1ABSAfESVActWoBraking = 1;
            lamscu1ESVActivatedWoBrkBfABS = 0;
        }
        else
        {
            lamscu1ABSAfESVActWoBraking = 0;
        }
    }


  /* --- Count LPA Fill in --- */
    if(lcabsu1ValidBrakeSensor==1)
    {
    	u8LPAFillDumpMax = U8_LPA_FILL_DUMP_MAX;
    }
    else
    {
      	u8LPAFillDumpMax = U8_LPA_FILL_DUMP_MAX_MPFAIL;
    }

    if((ADVANCED_MSC_ON_FLG == 0) && (lamscu1ESVActWoBraking==0))
    {
      #if (__SPLIT_TYPE==0) /* X Split */
        /* Primary */
        if ((HV_VL_fr==1)&&(AV_VL_fr==1))
        {
            lcmscu8LPAFillCounterP = lcmscu8LPAFillCounterP + 2;
        }

        if ((HV_VL_rl==1)&&(AV_VL_rl==1))
        {
            lcmscu8LPAFillCounterP = lcmscu8LPAFillCounterP + 1;
        }
        
        /* Secondary */
        if ((HV_VL_fl==1)&&(AV_VL_fl==1))
        {
            lcmscu8LPAFillCounterS = lcmscu8LPAFillCounterS + 2;
        }

        if ((HV_VL_rr==1)&&(AV_VL_rr==1))
        {
            lcmscu8LPAFillCounterS = lcmscu8LPAFillCounterS + 1;
        }
      #else /* FR split */

        /* Primary */
        if ((HV_VL_fr==1)&&(AV_VL_fr==1))
        {
            lcmscu8LPAFillCounterP = lcmscu8LPAFillCounterP + 2;
        }

        if ((HV_VL_fl==1)&&(AV_VL_fl==1))
        {
            lcmscu8LPAFillCounterP = lcmscu8LPAFillCounterP + 2;
        }

        /* Secondary */
        if ((HV_VL_rl==1)&&(AV_VL_rl==1))
        {
            lcmscu8LPAFillCounterS = lcmscu8LPAFillCounterS + 1;
        }
        
        if ((HV_VL_rr==1)&&(AV_VL_rr==1))
        {
            lcmscu8LPAFillCounterS = lcmscu8LPAFillCounterS + 1;
        }
      #endif /*__SPLIT_TYPE*/
        
        if(lcmscu8LPAFillCounterP > u8LPAFillDumpMax)
        {
            lcmscu8LPAFillCounterP = u8LPAFillDumpMax;
        }
        
        if(lcmscu8LPAFillCounterS > u8LPAFillDumpMax)
        {
            lcmscu8LPAFillCounterS = u8LPAFillDumpMax;
        }
    }
    else
    {
        lcmscu8LPAFillCounterP = 0;
        lcmscu8LPAFillCounterS = 0;
    }

  /* --- Decide Slight braking --- */

    if(lcabsu1BrakeByBLS==1)
    {
        if(ABS_fz==0)
        {
            if(lcmscu1ABSAfterBLS == 0)
            {
                if(lcmscu8BrakingTimeBfABS < L_U8_TIME_10MSLOOP_1500MS)
                {
                    lcmscu8BrakingTimeBfABS = lcmscu8BrakingTimeBfABS + 1;
                }
            }
        }
        else
        {
            lcmscu1ABSAfterBLS = 1;
            /*lcmscu8BrakingTimeBfABS = 0;*/
        }
    }
    else
    {
        lcmscu1ABSAfterBLS = 0;
        lcmscu8BrakingTimeBfABS = 0;
    }

    s16MCPSlope = (MCpress[0]-MCpress[3]);
    
    if((vref > VREF_50_KPH) && (vref <= S16_MSC_DELAY_MTR_DRV_VREF))
    {
        tempW0 = ((vref - VREF_50_KPH)/VREF_10_KPH); /* max 3, min 0 */
        s16DelayMtrDrvMpress  = S16_MSC_DELAY_MTR_DRV_MPRESS  - (MPRESS_10BAR * tempW0); /* 10bar per 10kph */
        s16DelayMtrDrvMPSlope = S16_MSC_DELAY_MTR_DRV_MP_RATE - (MPRESS_1BAR  * tempW0); /* 1bar per 10kph */
    }
    else
    {
        s16DelayMtrDrvMpress = S16_MSC_DELAY_MTR_DRV_MPRESS;
        s16DelayMtrDrvMPSlope = S16_MSC_DELAY_MTR_DRV_MP_RATE;
    }

    if(lcabsu1ValidBrakeSensor==1)
    {      
        if((lcabss16RefMpress <= s16DelayMtrDrvMpress) && (s16MCPSlope < s16DelayMtrDrvMPSlope))
        {
        	u8LowBrakeInput = 1;
    	}
    	else
    	{
        	u8LowBrakeInput = 0;
    	}	
    }
    else
    {
    	if((lcabsu1BrakeByBLS==0) || ((ebd_filt_grv > -S16_MSC_DELAY_MTR_DRV_DECEL) && (lcmscu8BrakingTimeBfABS > S16_MSC_DELAY_MTR_DRV_BFABS_TIM)))
    	{
        	u8LowBrakeInput = 1;
    	}
    	else
    	{
        	u8LowBrakeInput = 0;
    	}
    }

  /* --- Decide Delay Motor Driving During ABS --- */
    
    if(lamscu1ABSAfESVActWoBraking == 1) /* (ABS_fz == 1) */
    {
        tempF0 = lamscu1DelayMtrDriveInABS;
        
        if(lamscs16DelayMtrDriveInABSTime < L_U16_TIME_10MSLOOP_3S) /* lamscu1DelayMtrDriveInABS == 0 */
        {
            if((lcmscu8LPAFillCounterP < u8LPAFillDumpMax) && (lcmscu8LPAFillCounterS < u8LPAFillDumpMax))
            {
                if((vref <= S16_MSC_DELAY_MTR_DRV_VREF) && (u8LowBrakeInput == 1))
                {
                    lamscu1DelayMtrDriveInABS = 1;
                }
                else
                {
                    lamscu1DelayMtrDriveInABS = 0;
                }
            }
            else
            {
                lamscu1DelayMtrDriveInABS = 0;
            }
        }
        else
        {
            lamscu1DelayMtrDriveInABS = 0;
        }
        
        if(tempF0 != lamscu1DelayMtrDriveInABS)
        {
            lamscu1DelayMtrDriveInABSOld = tempF0;
        }
        
        if(lamscu1DelayMtrDriveInABS == 1)
        {
            lamscs16DelayMtrDriveInABSTime = lamscs16DelayMtrDriveInABSTime + 1;
        }
        else
        {
            lamscs16DelayMtrDriveInABSTime = L_U16_TIME_10MSLOOP_3S;
        }
    }
    else
    {
        lamscu1DelayMtrDriveInABS = 0;
        lamscu1DelayMtrDriveInABSOld = 0;
        lamscs16DelayMtrDriveInABSTime = 0;
    }
}

#endif /*#if (__DELAY_MOTOR_DRV_AT_LOW_MP == ENABLE)*/

/*
변수 변경 :

wdu16MonMoterAD => 	fu16CalVoltIntMOTOR

mot_mon_ad    	=>  fu16CalVoltMOTOR		(참조)/fu16CalVoltIntMOTOR (MGH80)
ign_mon_ad		=>	fu16CalVoltVDD
ref_mon_ad		=>	fu16CalVoltFSR			(참조)
batt2_mon_ad	=>	fu16CalVoltBATT2

변경 AD변수.
*/
/* AUTOSAR --------------------------*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LAABSCallMotorSpeedControl
	#include "Mdyn_autosar.h"               
#endif
/* AUTOSAR --------------------------*/
