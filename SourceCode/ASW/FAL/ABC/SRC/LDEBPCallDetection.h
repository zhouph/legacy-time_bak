#ifndef __LDEBPCALLDETECTION_H__
#define __LDEBPCALLDETECTION_H__

/*includes********************************************************************/
#include "LVarHead.h"



/*Global Type Declaration ****************************************************/
extern  U8_BIT_STRUCT_t EBPD0;
#define EBP_ENABLE                  EBPD0.bit0        //EBP
#define not_used_bit_EBP01          EBPD0.bit1        //EBP
#define EBP_STATE_FLG               EBPD0.bit2        //EBP
#define EBP_STATE_FLG2              EBPD0.bit3        //EBP
#define DETECT_MTP_RELEASE          EBPD0.bit4        //EBP


/*Global Extern Variable  Declaration*****************************************/
 
extern int16_t EBP_ENABLE_cnt;
 



/*Global Extern Functions  Declaration****************************************/


#endif

