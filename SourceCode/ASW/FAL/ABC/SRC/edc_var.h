#include "Abc_Ctrl.h"
/**************************************************/
/*                                                */
/*    Define & Declare  Structure Varible         */
/*                                                */
/*                  ____ MANDO ABS TEAM.          */
/**************************************************/

// ===========================================
// --->>>>>     EDC  Variables        <<<<----
// ===========================================

#if !__TCS

    extern int16_t   cal_torq, cal_torq_old;
    extern int16_t   can_ver;
    extern uint8_t tm_gear;
    extern int8_t  gear_pos;
	extern uint8_t	lespu8TqIntvnTyp;
	extern uint16_t	lespu16TorqReqVal;      
	extern int16_t   	cal_torq_abs;
	extern int16_t   	cal_torq_rel;	
#endif    

#if __TCS || __ETC || __EDC		// ABS_PLUS_ECU 적용시 macro 삭제 예정
    extern uint16_t   in_gear_count; 
    extern uint8_t 	gear_state_temp_old,gear_state_temp,gear_state,gear_state_old;			
    extern uint8_t 	engine_state,engine_state_old,engine_state_1,engine_state_1_old,engine_state_2,engine_state_2_old;
    extern uint16_t  	EDC_tm_gear;
    extern int16_t   	diff_v_tc_rpm;		
    extern int16_t   	diff_eng_tc_rpm;		
    extern int16_t   	vel_f_mid_2, vel_f_mid_2_old;                
    extern int16_t   	vel_f_mid_rpm,vel_f_mid_rpm_old; 
	extern int16_t 	eng_rpm_filt_for_GSD,eng_rpm_filt_old_for_GSD;		/* For __EDC_GEAR_SHIFT_DOWN_DETECTION */
	extern int16_t	slope_eng_rpm_for_GSD,slope_eng_rpm_old_for_GSD;	/* For __EDC_GEAR_SHIFT_DOWN_DETECTION */
	extern int16_t	slope_v_rpm, slope_v_rpm_old;
	extern uint8_t	EDC_MTM_RATIO[6];
	extern uint8_t	Normalized_Gear_Ratio[6];
	extern int16_t	Conv_vel_f_mid_rpm[6];
	extern int16_t	Conv_slope_v_rpm[6];
	extern int16_t	diff_rpm[6];
	extern int16_t	diff_slope[6];
	extern uint8_t  In_gear_flag,In_gear_flag_temp;
	extern int16_t	flywheel_torq;
	extern int16_t    Normalized_flywheel_torq[6];	
	extern int8_t 	Engine_Drag_flag_temp1,Engine_Drag_flag_temp2;
	extern int16_t   eng_rpm_filt,eng_rpm_filt_old;
	extern int16_t   slope_eng_rpm,slope_eng_rpm_old; 
	extern uint8_t  gear_state_prev1_old,gear_state_prev1;  /* __EDC_N_ABS_UPPER_GEAR_CLEAR 관련 변수들*/
#endif	

#if __EDC
	extern uint8_t  EDC_flags;
	extern int16_t	EDC_cmd; 
	extern int16_t 	EDC_exit_counter;
	extern int16_t	EDC_mon1,EDC_mon2,EDC_mon3,EDC_mon4; 
	extern int16_t	msr_count;
    extern int16_t  	MSR_cal_torq;
	extern int16_t	lcedcs16AvgDrivenWheelSlip,lcedcs16TargetWheelSlip;
	extern int16_t	lcedcs16WheelSlipFiltOld,lcedcs16WheelSlipFilt;
	extern int16_t	lcedcs16WheelSlipError,lcedcs16WheelSlipErrDiff;
	extern int16_t	lcedcs16PGain,lcedcs16DGain;
	extern int16_t	lcedcs16PGainEffect,lcedcs16DGainEffect;
	extern uint8_t	EDC_Phase_flags,EDC_Inhibit_flags,EDC_Exit_flags;
	extern int8_t	lcedcs8EdcEntryThreshold1,lcedcs8EdcEntryThreshold2;
	extern uint8_t	lcedcs16EDCTorqDecRate;
	extern uint16_t	ledcu161SecCntAfterEDC;
	extern int8_t	ldedcs8EDCEntryTHR0,ldedcs8EDCEntryTHR1,ldedcs8EDCEntryTHR2;
	extern int16_t	ldedcs16EDCEntryTHR0Final,ldedcs16EDCEntryTHR1Final,ldedcs16EDCEntryTHR2Final;	
	extern uint8_t	ldedcu8TorqHoldLvInEDC;
    extern int16_t	ldedcs16WheelLockSuspectCnt;
	extern int16_t	ldedcs16velgapatEDCentry,ldedcs16velgapatEDCentry2,ldedcs16velgap,ldedcs16velgap2;
	extern int16_t	ldedcs16ResetTempTireDetectCnt,ldedcs16TempTireTendCntInCtl;
  #if ( (GMLAN_ENABLE==ENABLE)|| (__CAR==GM_C100) || (__CAR==GM_V275) || (__CAR==GM_CTS) || (__CAR==GM_C100_MECU) || (__CAR==GM_TAHOE) || (__CAR==GM_TAHOE_MECU) || (__CAR==GM_J300)||(__CAR == GM_LAMBDA)||(__CAR==GM_MALIBU)|| (__CAR ==GM_SILVERADO)) 	
	extern uint8_t	lespu1EngTqMaxExtRngVal;
	extern int16_t	lesps16EngTqMaxExtRng;	
  #endif	
	extern int16_t 	ledcs16VelGapBuffer[14],ledcs16VelGapBuffer2[14];
	extern int16_t	ledcs16VelGapSum,ledcs16VelGapSum2;
	extern int16_t	ledcs16VelGapIndex;
    extern int16_t	ldedcs16InitialEDCSlip;
	extern int16_t	ldedcs16InitialEDCRellam;	
	extern uint8_t	ldedcu8EDCInitialCnt;

//	[[EDC MACRO SETTING]]
#define __EDC_NEUTRAL_ABS_DETECTION		1 /* Neutral ABS 제동 감지하여, 제어 금지시키는 로직 */
#define __EDC_NEUTRAL_PARKING_DETECTION 1 /* Neutral Parking 감지하여, 제어 금지시키는 로직 */
#define __EDC_TEMP_TIRE_DETECTION		1 /* Temp Tire 감지하여, 제어 진입 둔감, 제어량 감소, 제어 종료 앞당기는 구조 */
#define __EDC_N_ABS_UPPER_GEAR_CLEAR	0 /* Neutral ABS제동중 고단기어 감지시 neutral 판단을 위한 로직 */	
#define __EDC_HIGH_MU_DETECTION			1 /* High mu 감지하여, high-mu slip발생시 제어량 감소시키는 로직 */
#define __EDC_GEAR_SHIFT_DOWN_DETECTION 1 /* 전영역 Gear estimation 적용후, GSD감지후 EDC제어 허용하는 로직 */
	
	extern int16_t    lcedcs16AvgDrivenWheelSlipOld;								/* For __EDC_GEAR_SHIFT_DOWN_DETECTION */
	extern int16_t	lcedcs16AvgDrvnWhlSlipDiff , lcedcs16AvgDrvnWhlSlipDiffOld;	/* For __EDC_GEAR_SHIFT_DOWN_DETECTION */
	extern int8_t	ldedcs8GearShiftDownCount;									/* For __EDC_GEAR_SHIFT_DOWN_DETECTION */

#endif
  
