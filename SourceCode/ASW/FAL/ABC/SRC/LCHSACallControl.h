#ifndef __LCHSACALLCONTROL_H__
#define __LCHSACALLCONTROL_H__

/*includes********************************************************************/
#include "LVarHead.h"


  #if __HSA

#define __MGH60_CYCLE_TIME_OPT_HSA              ENABLE
#define S16_HSA_CONTROLLER_ENABLE_SPEED         VREF_15_KPH
  
  
/*Global Type Declaration ****************************************************/
#define	HSA_flg						            HSAF0.bit7
#define	HSA_EST_CLUTCH_POSITION					HSAF0.bit6
#define	INHIBIT_DCT_ROLL			            HSAF0.bit5
#define	HSA_TCL_DEMAND_fl			            HSAF0.bit4
#define	HSA_TCL_DEMAND_fr			            HSAF0.bit3
#define	HSA_S_VALVE_LEFT			            HSAF0.bit2
#define	HSA_S_VALVE_RISHT			            HSAF0.bit1
#define	HSA_MSC_MOTOR_ON			            HSAF0.bit0

/*  HSA Flag1 (HSA CONTROL)  */
#define HSA_INHIBITION_flag						HSAF1.bit7
#define STOP_ON_HILL_flag						HSAF1.bit6
#define STOP_ON_UPHILL_flag						HSAF1.bit5
#define STOP_ON_DOWNHILL_flag					HSAF1.bit4
#define HSA_EXIT_CONDITION_SATISFY				HSAF1.bit3
#define HSA_ENTER_CONDITION_SATISFY				HSAF1.bit2
#define HSA_APPLY_CONDITION_SATISFY				HSAF1.bit1
#define COMPLETE_STOP_flag						HSAF1.bit0

/*  HSA Flag2 (INPUT SETTING)  */
#define AX_SENSOR_ALLOWED						HSAF2.bit7
#define GEAR_POSITION_SENSOR_ALLOWED			HSAF2.bit6
#define REVERSE_GEAR_INFORM_ALLOWED				HSAF2.bit5
#define CLUTCH_POSITION_ALLOWED					HSAF2.bit4
#define SMART_ACTIVE_WHEEL_SPEED_SENSOR_ALLOWED	HSAF2.bit3
#define SAWSS_BACKWARD_DETECT					HSAF2.bit2
#define SAWSS_FORWARD_DETECT					HSAF2.bit1
#define CLUTCH_POSITION							HSAF2.bit0

#define HSA_RAPID_REBRAKING_flag				HSAF3.bit7
#define HSA_EPB_ACTIVE_FLG						HSAF3.bit6
#define HSA_SECOND_HOLD							HSAF3.bit5
#define HSA_Ax_Increase_flag					HSAF3.bit4
#define HSA_AX_STATIONARY						HSAF3.bit3
#define HSA_RAPID_PHASE_OUT						HSAF3.bit2
#define HSA_TM_TYPE								HSAF3.bit1
#define HSA_MAINTAIN_BRK_APPLY_flag 			HSAF3.bit0

#define lcu1HsaClutchSigFail    				HSAF4.bit7
#define lcu1HsaRgearSigFail		        		HSAF4.bit6
#define ISG_REQ_ON						    	HSAF4.bit5
#define ISG_READY					            HSAF4.bit4
#define NEUTRAL_SWITCH_ALLOWED   		        HSAF4.bit3 
#define lcu1HsaMpressOn			                HSAF4.bit2
#define lcu1HsaReBrkRelease						HSAF4.bit1
#define lcu1HsaReBrk    				        HSAF4.bit0

#define ISG_INHIBITION_flag   				    HSAF5.bit7
#define ISG_GEAR_OK		        		        HSAF5.bit6
#define HSA_ISG_flg						    	HSAF5.bit5
#define HSA_ENOUGH_ENG_TORQ				        HSAF5.bit4
#define VEHICLE_MOVE_WO_EXIT_OK   		        HSAF5.bit3 
#define lcu1HsaOverloadRoll		                HSAF5.bit2
#define lcu1HsaRiseComplete					    HSAF5.bit1
#define VEHICLE_MOVE_WO_EXIT_OK_old         	HSAF5.bit0

#define lcu1HsaExitBySpinMT   				    HSAF6.bit7
#define lcu1HsaExitBySpinAT		        		HSAF6.bit6
#define lcu1HsaClutchSwOffwithBrk			    HSAF6.bit5
#define lcu1HsaDiveDetect			            HSAF6.bit4
#define HSA_Ax_Increase_flag_Old                HSAF6.bit3 
#define hsa_non_use_6_2		                    HSAF6.bit2
#define hsa_non_use_6_1					        HSAF6.bit1
#define hsa_non_use_6_0         	            HSAF6.bit0


#define HSA_READY                   1
#define HSA_HOLD                    2
#define HSA_APPLY                   3
#define HSA_RELEASE                 4
#define HSA_NORMAL                  0

#define ISG_ACTIVE_STATUS                      1  
#define ISG_ENGINE_START_STATUS                2  
#define ISG_FAULT_STATUS                       4
/*----------------------------------------*/
/* For Ford Demo                          */
/*----------------------------------------*/


#define HSA_Ax_ChangeETPerGratio			18
#define HSA_ETPerGratio_SteepSlope			1

#define S16_HSA_AXOFFSET_NONE_COMP		0

  #if (__CAR==HMC_VF)
#define __MT_NEUTRAL_SWITCH_ENABLE      DISABLE
  #else
#define __MT_NEUTRAL_SWITCH_ENABLE      DISABLE  
  #endif
  
  #if (__CAR_MAKER==HMC_KMC)
#define __ISG                       ENABLE
  #else
#define __ISG                       DISABLE
  #endif

/**************** macro setting ***********************/
#define __IMPROVE_DIVE_DETECTION	DISABLE

/* BKTU tuning parameter */
/*
#define S16_HSA_AX_DIFF_FOR_DIVE_DETECT       	   100
#define S16_HSA_AX_DIFF_LOW_REF       	   		   100
#define S16_HSA_AX_DIFF_MED_REF       	   		   600
#define S16_HSA_AX_DIFF_MAX_REF       	   		   800
#define S16_HSA_AX_AVG_VALID_TH_LOW_DIF       	   L_U8_TIME_10MSLOOP_200MS
#define S16_HSA_AX_AVG_VALID_TH_MID_DIF       	   L_U8_TIME_10MSLOOP_300MS
#define S16_HSA_AX_AVG_VALID_TH_MAX_DIF       	   L_U8_TIME_10MSLOOP_400MS

#define S16_HSA_DEFAULT_ENG_IDLE_TORQ        	   100

#define U8_HSA_SET_DECAY_RATE_EACH_SLOP        0
#define U8_HSA_SET_CLUTCH_ENGAGE_EST           0

#define S16_HSA_MIN_MP_SLOP                    10
#define S16_HSA_VV_ACT_COMP_MIN_MP_SLP         50
#define S16_HSA_MAX_MP_SLOP                    100
#define S16_HSA_VV_ACT_COMP_MAX_MP_SLP         150
#define S16_HSA_INIT_HOLD_CNTR_T			   20
#define S16_HSA_INIT_CNTR_CUR_MAX_COMP	       300
#define S16_HSA_CNTR_MIN_TC_CURRENT			   150
#define S16_HSA_STOP_ON_HILL_MIN_SPD	       VREF_0_5_KPH
#define U8_HSA_CNTR_P_SCAN                     3


#define U8_HSA_SPIN_EXIT_SET_TIME        T_500_MS
#define S16_HSA_PRESS_DEC_RATE_TC_OFF    10
#define U8_HSA_Disable            0
#define U16_HSA_holding_time_on_Accel		T_1500_MS
*/

/* for 201202 GMNA demo */
#define S16_HSA_TC_comp_HRC_transiton	    150

#define U8_HSA_FO_R_Min_Under10bar			2
#define U8_HSA_FO_R_Lmed_Under10bar			1
#define U8_HSA_FO_R_Hmed_Under10bar			1
#define U8_HSA_FO_R_Max_Under10bar			1

#define U16_HSA_holding_time_out_LM			L_U16_TIME_10MSLOOP_3S
#define U16_HSA_holding_time_out_HM         L_U16_TIME_10MSLOOP_3_5S
#define U16_HSA_holding_time_out_MAX		L_U16_TIME_10MSLOOP_4S
/* for 201202 GMNA demo */

/* AHB_GEN3_SYSTEM pedal on/off detection */
#define U8_AHB_HSA_PEDAL_ON_TH_TIME   			 L_U8_TIME_10MSLOOP_100MS
#define S16_AHB_HSA_PEDAL_ON_TH       			 MPRESS_4BAR
#define S16_AHB_HSA_PEDAL_OFF_TH       			 MPRESS_3BAR
/* AHB_GEN3_SYSTEM pedal on/off detection */


/*Global Extern Variable  Declaration*****************************************/
extern 	U8_BIT_STRUCT_t HSAF0, HSAF1, HSAF2, HSAF3, HSAF4, HSAF5, HSAF6;

 
extern int8_t			HSA_Control;
extern int16_t			HSA_pressure_release_counter, HSA_ENTER_press, HSA_retaining_press;
extern uint8_t			HSA_rebraking_counter, HSA_rebraking_start, HSA_hold_cnt_rpm, lcHsau8OverloadDctCnt;
extern int16_t			HSA_phase_out_rate, HSA_phase_out_time, HSA_apply_count;
extern uint16_t			HSA_accel_cnt, HSA_hold_counter, HSA_accel_cnt2;
extern int16_t			HSA_gs_sel, HSA_EXIT_eng_torq, S16_HSA_ETPerG_SteepSlope, MT_ENG_TORQ_INDEX;
extern int16_t			HSA_ax_sen_filt_old, HSA_ax_sum, HSA_ax_cnt, HSA_ax_avg, HSA_EXIT_ax, MT_FRICTION_CONST;
extern int16_t          lchsas16MpressSlopThres, lcs16HsaExitBySpinPress, lcs16HsaMpress, lchsas16CtrlTargetPress;
extern int16_t		    HSA_Est_eng_rpm_dot, HSA_Est_eng_rpm_dot_old, eng_torq_rel_filt_HSA, Torq_Clutch;
extern int16_t		    HSA_eng_rpm_LPF7_old, HSA_eng_rpm_dot_LPF7, HSA_eng_rpm_dot_LPF7_old;
extern int16_t	        lcs16HsaInitHoldCntrCnt, lcs16HsaValveActMp, lcs16HsaMpressSlopThres, lcs16HsaReBrkReleaseCnt;
extern uint8_t          lcu8HsaReBrkCnt;
extern uint8_t          lcu8HsaAxAvgValidTh;
extern int16_t	        lcs16HsaAxDiffMax, lcs16HsaAxSum, lcs16HsaAxAvgTime, lcs16HsaAxAvg;

/*Global Extern Functions  Declaration****************************************/
extern  void 	LCHSA_vCallControl(void);
extern  void 	HSA_reset(void);

  #endif
#endif
