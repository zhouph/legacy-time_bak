/* Includes ************************************************************/

#include "LVarHead.h"

/*Global Type Declaration **********************************************/
#if __ESP_ABS_COOPERATION_CONTROL
	#define Forced_DUMP_Scan_Front          3
	#define Forced_DUMP_Scan_Rear           3
#endif
 
/* Global Variables Declaration ****************************************/

#define U8_ABS_ESC_COOP_LEVEL0	0
#define U8_ABS_ESC_COOP_LEVEL1	1
#define U8_ABS_ESC_COOP_LEVEL2	2
#define U8_ABS_ESC_COOP_LEVEL3	3
    
/* Global Function Declaration *****************************************/

#if __VDC
extern void LCABS_vChangeABStoESP(void);
extern void LCABS_vChangeESPtoABS(void);
extern void LCABS_vRestoreLFCVariable(void);
extern void LCABS_vRecallLFCVariable(void);

  #if __ESP_ABS_COOPERATION_CONTROL
    #if	__ABS_COMB
extern void LCABS_vForcedDumpHoldWheel(void);
    #else
extern void LCABS_vForcedDumpHold(void);
    #endif
  #endif
#endif
