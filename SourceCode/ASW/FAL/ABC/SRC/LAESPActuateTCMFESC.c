
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START                       
	#define idx_FILE	idx_CL_LAESPActuateNcNovalveF
	#include "Mdyn_autosar.h"               
#endif


#include "Hardware_Control.h"
#include "LAESPActuateNcNovalveF.h"
#include "LAESPCallActHW.h"
#include "LAABSCallMotorSpeedControl.h"
#include "LCESPCalInterpolation.h"
#include "LCTCSCallBrakeControl.h"
#include "LCESPCallControl.h"
#if (__ESC_TCMF_CONTROL ==1)
#include "LCESPInterfaceTCMF.h"
#include "LAESPActuateTCMFESC.h"

uint8_t lcespu8TCMFControlWheel;
#endif

#if (__ESC_TCMF_CONTROL ==1)
int16_t LAMFC_s16SetMFCCurESP_TCMF_Pri(uint8_t CIRCUIT, int16_t MFC_Current_old);
int16_t LAMFC_s16SetMFCCurESP_TCMF_Sec(uint8_t CIRCUIT, int16_t MFC_Current_old);
int16_t LAMFC_s16CalMFCCurrentESP_TCMF(struct ESC_STRUCT *WL_ESC, int16_t MFC_Current_old);
void LAMFC_s16CalMFCIntCurrent_TCMF(struct ESC_STRUCT *WL_ESC, struct W_STRUCT *WL, int16_t MFC_Current_old);
void LAMFC_s16CalMFCStepRiseIntDuty(struct ESC_STRUCT *WL_ESC);

int16_t LAMFC_s16SetMFCCurESP_TCMF_Pri(uint8_t CIRCUIT, int16_t MFC_Current_old)
{
	  int16_t  MFC_Current_new = 0;    
      
      #if !__SPLIT_TYPE
    if((CIRCUIT == U16_PRIMARY_CIRCUIT)&&(FR_ESC.lcespu1TCMFControl ==1))
      #else
    if((CIRCUIT == U16_PRIMARY_CIRCUIT)&&(FR_ESC.lcespu1TCMFControl == 1))
      #endif
    {
    	lcespu8TCMFControlWheel = FR_Control;
    	LAMFC_s16CalMFCStepRiseIntDuty(&FR_ESC);    	 	
    	LAMFC_s16CalMFCIntCurrent_TCMF(&FR_ESC, &FR, MFC_Current_old);
    	MFC_Current_new = LAMFC_s16CalMFCCurrentESP_TCMF(&FR_ESC, MFC_Current_old);
    }
    else
    {
    	;
    }
        
    if(MFC_Current_new > TC_CURRENT_MAX)
    {
    	  MFC_Current_new = TC_CURRENT_MAX;
    }
    else if(MFC_Current_new < 0)
    {
    	  MFC_Current_new = 0;
    }
    else
    {
    	  ;
    }
    
    return MFC_Current_new;
}

int16_t LAMFC_s16SetMFCCurESP_TCMF_Sec(uint8_t CIRCUIT, int16_t MFC_Current_old)
{
	  int16_t  MFC_Current_new = 0;    
      
      #if !__SPLIT_TYPE
    if((CIRCUIT == U16_SECONDARY_CIRCUIT)&&(FL_ESC.lcespu1TCMFControl ==1))
      #else
    if((CIRCUIT == U16_PRIMARY_CIRCUIT)&&(FL_ESC.lcespu1TCMFControl ==1))
      #endif
    {
    	lcespu8TCMFControlWheel = FL_Control;
    	LAMFC_s16CalMFCStepRiseIntDuty(&FL_ESC);    	
    	LAMFC_s16CalMFCIntCurrent_TCMF(&FL_ESC, &FL, MFC_Current_old);
    	MFC_Current_new = LAMFC_s16CalMFCCurrentESP_TCMF(&FL_ESC, MFC_Current_old);
    }
    else
    {
    	;
    }
        
    if(MFC_Current_new > TC_CURRENT_MAX)
    {
    	  MFC_Current_new = TC_CURRENT_MAX;
    }
    else if(MFC_Current_new < 0)
    {
    	  MFC_Current_new = 0;
    }
    else
    {
    	  ;
    }
    
    return MFC_Current_new;
}

void LAMFC_s16CalMFCIntCurrent_TCMF(struct ESC_STRUCT *WL_ESC, struct W_STRUCT *WL, int16_t MFC_Current_old)
{
	if((WL_ESC->lcespu1TCMFInhibitOld ==1)&&(WL_ESC->lcespu1TCMFControl ==1))
	{
		esp_tempW1 = WL->s16_Estimated_Active_Press;
		WL_ESC->MFC_Current_Inter = LCESP_s16IInter5Point(esp_tempW1, S16TCSCpWhlPre_1, S16TCSCpTCcrnt_1,
																	  S16TCSCpWhlPre_2, S16TCSCpTCcrnt_2,
																	  S16TCSCpWhlPre_3, S16TCSCpTCcrnt_3,
																	  S16TCSCpWhlPre_4, S16TCSCpTCcrnt_4,
																	  S16TCSCpWhlPre_5, S16TCSCpTCcrnt_5);
	}
	else if((WL_ESC->lcespu1TCMFControl_Old ==0)&&(WL_ESC->lcespu1TCMFControl ==1)
		&&(WL_ESC->lcespu1TCMFConCBSCombFlag ==1)&&(WL_ESC->MFC_Current_Inter < S16_TCMF_RISE_DUTY_CBS_START))
	{
    	WL_ESC->MFC_Current_Inter = S16_TCMF_RISE_DUTY_CBS_START;
	}
	else if(WL_ESC->lcespu1TCMFCon_IniFull ==1)
    {    	
    	#if(__VALVE_TEST_TCMF ==1)
		WL_ESC->MFC_Current_Inter = WL_ESC->MFC_Current_Inter + (int16_t)U8_TCMF_INITIAL_DUTY_INC;
    	#else		
		WL_ESC->laesps16TCMFIniDutyInc = LCESP_s16IInter5Point(vdc_msc_target_vol, MSC_4_V, (int16_t)U8_TCMF_INITIAL_DUTY_INC_04,
																				   MSC_6_V, (int16_t)U8_TCMF_INITIAL_DUTY_INC_06,
																				   MSC_8_V, (int16_t)U8_TCMF_INITIAL_DUTY_INC_08,
																				   MSC_10_V, (int16_t)U8_TCMF_INITIAL_DUTY_INC_10,
																				   MSC_12_V, (int16_t)U8_TCMF_INITIAL_DUTY_INC_12);
		
		WL_ESC->MFC_Current_Inter = WL_ESC->MFC_Current_Inter + WL_ESC->laesps16TCMFIniDutyInc;
		#endif
    }
    else if(WL_ESC->lcespu1TCMFCon_Rise ==1)
    {
    	if(WL_ESC->lcespu1TCMFCon_Rise_MAX ==1)
    	{    		
    		#if(__VALVE_TEST_TCMF ==1)
    		WL_ESC->MFC_Current_Inter = WL_ESC->MFC_Current_Inter + (int16_t)U8_TCMF_RISE_DUTY_INC_MAX;
    		#else
    		WL_ESC->laesps16TCMFDutyMAXInc = LCESP_s16IInter5Point(vdc_msc_target_vol, MSC_4_V, (int16_t)U8_TCMF_RISE_DUTY_INC_MAX_04,
																					   MSC_6_V, (int16_t)U8_TCMF_RISE_DUTY_INC_MAX_06,
																					   MSC_8_V, (int16_t)U8_TCMF_RISE_DUTY_INC_MAX_08,
																					   MSC_10_V, (int16_t)U8_TCMF_RISE_DUTY_INC_MAX_10,
																					   MSC_12_V, (int16_t)U8_TCMF_RISE_DUTY_INC_MAX_12);
    		
    		WL_ESC->MFC_Current_Inter = WL_ESC->MFC_Current_Inter + WL_ESC->laesps16TCMFDutyMAXInc;
    		#endif
    	}
    	else if(WL_ESC->lcespu1TCMFCon_Rise_STEP ==1)
    	{
    			#if(__VALVE_TEST_TCMF ==1)
    		if(WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg3 ==1)
    		{
    			WL_ESC->MFC_Current_Inter = WL_ESC->MFC_Current_Inter + (int16_t)U8_TCMF_INC_STEP_INT_REG3;
    		}
    		else if(WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg2 ==1)
    		{
    			WL_ESC->MFC_Current_Inter = WL_ESC->MFC_Current_Inter + (int16_t)U8_TCMF_INC_STEP_INT_REG2;
    		}
    		else
    		{
    			WL_ESC->MFC_Current_Inter = WL_ESC->MFC_Current_Inter + (int16_t)U8_TCMF_INC_STEP_INT_REG1;
    		}
    			#else
    		WL_ESC->MFC_Current_Inter = WL_ESC->MFC_Current_Inter + WL_ESC->lcesps16StepIntDutybyComVol;
    			#endif
    	}
    	else
    	{
    		;
    	}
    	
    	if(WL_ESC->MFC_Current_Inter >= TC_CURRENT_MAX)
    	{
  			WL_ESC->MFC_Current_Inter = TC_CURRENT_MAX;
    	}
    	else
    	{
  			;
  		}
    }
  	else if(WL_ESC->lcespu1TCMFCon_Hold ==1)
  	{  		
  		WL_ESC->MFC_Current_Inter = WL_ESC->MFC_Current_Inter;
  	}
  	else if(WL_ESC->lcespu1TCMFCon_Dump ==1)
  	{
  		if((WL_ESC->lcespu1TCMFCon_Dump ==1)&&(WL_ESC->lcespu1TCMFConOld_Dump ==0))
  		{
  			WL_ESC->MFC_Current_Inter = WL_ESC->MFC_Current_Inter - S16_TCMF_DUMP_DUTY_DIS_DEC;
  		}
  		else
  		{
  			if(WL_ESC->lcespu1TCMFCon_Dump_SLOW ==1)
  			{
  				WL_ESC->MFC_Current_Inter = WL_ESC->MFC_Current_Inter - (int16_t)U8_TCMF_DUMP_DUTY_DEC_SLOW;  					
  			}
  			else if(WL_ESC->lcespu1TCMFCon_Dump_FAST ==1)
  			{
  				WL_ESC->MFC_Current_Inter = WL_ESC->MFC_Current_Inter - (int16_t)U8_TCMF_DUMP_DUTY_DEC_FAST;
  			}
  			else
  			{
  				;
  			}  		
  		}
  		
  		WL_ESC->lcesps16TCMFCompDutybyMP = LCESP_s16IInter3Point(mpress_slop, 0                             , 0                            ,
  		                                                                      S16_TCMF_DUTY_COMP_MP_DOT_MED , S16_TCMF_COMP_DUTY_BY_MP_MED , 
	                            					                          S16_TCMF_DUTY_COMP_MP_DOT_HIGH, S16_TCMF_COMP_DUTY_BY_MP_HIGH);
	    
	    WL_ESC->MFC_Current_Inter = WL_ESC->MFC_Current_Inter - WL_ESC->lcesps16TCMFCompDutybyMP;
  	}
  	else if(WL_ESC->lcespu1TCMFCon_FadeOut ==1)
  	{
  		if((WL_ESC->lcespu1TCMFCon_FadeOut ==1)&&(WL_ESC->lcespu1TCMFConOld_FadeOut ==0))
		{
			WL_ESC->MFC_Current_Inter = WL_ESC->MFC_Current_Inter - S16_TCMF_FADE_DUTY_DIS_DEC;
		}
		else
		{
			esp_tempW0 = McrAbs(yaw_out);
    
    		esp_tempW1 = LCESP_s16FindRoadDependatGain( esp_mu, (int16_t)U8_TCMF_FADE_SLOW_YAWM_HM, (int16_t)U8_TCMF_FADE_SLOW_YAWM_MM, (int16_t)U8_TCMF_FADE_SLOW_YAWM_LM);
  			
  			if(esp_tempW0>esp_tempW1)
			{
  				WL_ESC->MFC_Current_Inter = WL_ESC->MFC_Current_Inter - (int16_t)U8_TCMF_FADE_DUTY_DEC_SLOW;
			}
			else
			{
  				WL_ESC->MFC_Current_Inter = WL_ESC->MFC_Current_Inter - (int16_t)U8_TCMF_FADE_DUTY_DEC_FAST;
			}
		}
		
		WL_ESC->lcesps16TCMFCompDutybyMP = LCESP_s16IInter3Point(mpress_slop, 0                             , 0                            ,
  		                                                                      S16_TCMF_DUTY_COMP_MP_DOT_MED , S16_TCMF_COMP_DUTY_BY_MP_MED , 
	                            					                          S16_TCMF_DUTY_COMP_MP_DOT_HIGH, S16_TCMF_COMP_DUTY_BY_MP_HIGH);
	    
	    WL_ESC->MFC_Current_Inter = WL_ESC->MFC_Current_Inter - WL_ESC->lcesps16TCMFCompDutybyMP;
    }
  	else
    {
  		;
  	}
    
    if(WL_ESC->MFC_Current_Inter <= S16_TCMF_TC_DUTY_L_LIMIT)
    {
  		WL_ESC->MFC_Current_Inter = S16_TCMF_TC_DUTY_L_LIMIT;
    }
    else
    {
  		;
  	}
}
    	
int16_t LAMFC_s16CalMFCCurrentESP_TCMF(struct ESC_STRUCT *WL_ESC, int16_t MFC_Current_old)
{
	int16_t  MFC_Current_new = 0;
		
    if(WL_ESC->lcespu1TCMFCon_IniFull ==1)
    {
    	MFC_Current_new = TC_CURRENT_MAX;
    }
    else if(WL_ESC->lcespu1TCMFCon_Rise ==1)
  	{
    	if(WL_ESC->lcespu1TCMFCon_Rise_MAX ==1)
  		{
    		MFC_Current_new = TC_CURRENT_MAX;
  		}
    	else if(WL_ESC->lcespu1TCMFCon_Rise_STEP ==1)
  		{
  			if((WL_ESC->lcespu1TCMFControl_Old ==0)&&(WL_ESC->lcespu1TCMFControl ==1)&&
  				(WL_ESC->lcespu1TCMFConCBSCombFlag ==1)&&(MFC_Current_old < S16_TCMF_RISE_DUTY_CBS_START))
			{    			
    			MFC_Current_old = S16_TCMF_RISE_DUTY_CBS_START;
			}
			else
			{
				;
			}
  			
  			if(WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg3 ==1)
  			{  				
  				if(WL_ESC->lcespu1TCMFConSTEP_RegTrans ==1)
  				{
  					MFC_Current_new = WL_ESC->MFC_Current_Inter;
  				}
  				else
  				{
  					MFC_Current_new = MFC_Current_old + WL_ESC->lcesps16TCMF_Rise_STEP_Com;
  				}
  			}
  			else if(WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg2 ==1)
  			{  				
  				if(WL_ESC->lcespu1TCMFConSTEP_RegTrans ==1)
  				{
  					MFC_Current_new = WL_ESC->MFC_Current_Inter;
  				}
  				else
  				{
  					MFC_Current_new = MFC_Current_old + WL_ESC->lcesps16TCMF_Rise_STEP_Com;
  				}
  			}
  			else
  			{
  				if(WL_ESC->lcespu1TCMFConSTEP_RegTrans ==1)
  				{
  					MFC_Current_new = WL_ESC->MFC_Current_Inter;
  				}
  				else
  				{
  					MFC_Current_new = MFC_Current_old + WL_ESC->lcesps16TCMF_Rise_STEP_Com;
  				}
  			}
  			
  			if(MFC_Current_new < WL_ESC->MFC_Current_Inter)
  			{
  				MFC_Current_new = WL_ESC->MFC_Current_Inter;
  			}
  			else
  			{
  				;
  			}
  		}
  		else
  		{
    		;
  		}
  	}
  	else if(WL_ESC->lcespu1TCMFCon_Hold ==1)
  	{
  		MFC_Current_new = WL_ESC->MFC_Current_Inter;
  	}
  	else if(WL_ESC->lcespu1TCMFCon_Dump ==1)
  	{
  		if(WL_ESC->slip_m < -WHEEL_SLIP_60)
  		{
  			MFC_Current_new = 10;
  		}
  		else
  		{
  			MFC_Current_new = WL_ESC->MFC_Current_Inter;
  		}
  	}
  	else if(WL_ESC->lcespu1TCMFCon_FadeOut ==1)
  	{	
  		esp_tempW2 = LCESP_s16FindRoadDependatGain( esp_mu, (int16_t)U8_TCMF_FADE_OUT_FULL_HM, (int16_t)U8_TCMF_FADE_OUT_FULL_MM, (int16_t)U8_TCMF_FADE_OUT_FULL_LM);
  		
  		if((((lcespu8TCMFControlWheel ==FL_Control)&&(yaw_out >-esp_tempW2))||(WL_ESC->slip_m < -WHEEL_SLIP_60))
  		  ||(((lcespu8TCMFControlWheel ==FR_Control)&&(yaw_out < esp_tempW2))||(WL_ESC->slip_m < -WHEEL_SLIP_60)))
  		{
  			MFC_Current_new = 10;
  		}
  		else
  		{
  			MFC_Current_new = WL_ESC->MFC_Current_Inter;
  		}
  	}
  	else
  	{
  		;
  	}
  			
  	return MFC_Current_new;
}

void LAMFC_s16CalMFCStepRiseIntDuty(struct ESC_STRUCT *WL_ESC)
{
	if(WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg3 ==1)
	{
		esp_tempW3 = LCESP_s16FindRoadDependatGain(esp_mu, (int16_t)U8_TCMF_INC_STEP_COM_REG3_HM,
														   (int16_t)U8_TCMF_INC_STEP_COM_REG3_MM, 
														   (int16_t)U8_TCMF_INC_STEP_COM_REG3_LM);
														   
		WL_ESC->lcesps16TCMF_Rise_STEP_Com = esp_tempW3;
	}
	else if(WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg2 ==1)
	{
		esp_tempW2 = LCESP_s16FindRoadDependatGain(esp_mu, (int16_t)U8_TCMF_INC_STEP_COM_REG2_HM,
														   (int16_t)U8_TCMF_INC_STEP_COM_REG2_MM, 
														   (int16_t)U8_TCMF_INC_STEP_COM_REG2_LM);
		
		WL_ESC->lcesps16TCMF_Rise_STEP_Com = esp_tempW2;
	}
	else if(WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg1 ==1)
	{
		esp_tempW1 = LCESP_s16FindRoadDependatGain(esp_mu, (int16_t)U8_TCMF_INC_STEP_COM_REG1_HM,
														   (int16_t)U8_TCMF_INC_STEP_COM_REG1_MM, 
														   (int16_t)U8_TCMF_INC_STEP_COM_REG1_LM);
		
		WL_ESC->lcesps16TCMF_Rise_STEP_Com = esp_tempW1;
	}
	else
	{
		;
	}
	
	if(vdc_msc_target_vol<MSC_4_V)
	{		
		WL_ESC->lcesps16StepIntDutyPribyCom = LCESP_s16IInter5Point(WL_ESC->lcesps16TCMF_Rise_STEP_Com, 10, (int16_t)U8_TCMF_STEP_INT_DUTY_04_10,
																									 	20, (int16_t)U8_TCMF_STEP_INT_DUTY_04_20,
																									 	30, (int16_t)U8_TCMF_STEP_INT_DUTY_04_30,
																									 	40, (int16_t)U8_TCMF_STEP_INT_DUTY_04_40,
																									 	50, (int16_t)U8_TCMF_STEP_INT_DUTY_04_50);
																									 
		WL_ESC->lcesps16StepIntDutySecbyCom = LCESP_s16IInter5Point(WL_ESC->lcesps16TCMF_Rise_STEP_Com, 10, (int16_t)U8_TCMF_STEP_INT_DUTY_04_10,
																									 	20, (int16_t)U8_TCMF_STEP_INT_DUTY_04_20,
																									 	30, (int16_t)U8_TCMF_STEP_INT_DUTY_04_30,
																									 	40, (int16_t)U8_TCMF_STEP_INT_DUTY_04_40,
																									 	50, (int16_t)U8_TCMF_STEP_INT_DUTY_04_50);
		
		WL_ESC->lcesps16StepIntDutybyComVol = LCESP_s16IInter2Point(vdc_msc_target_vol, MSC_4_V, WL_ESC->lcesps16StepIntDutyPribyCom, 
																						MSC_4_V, WL_ESC->lcesps16StepIntDutySecbyCom);
	}
	else if(vdc_msc_target_vol<MSC_6_V)
	{		
		WL_ESC->lcesps16StepIntDutyPribyCom = LCESP_s16IInter5Point(WL_ESC->lcesps16TCMF_Rise_STEP_Com, 10, (int16_t)U8_TCMF_STEP_INT_DUTY_04_10,
																									 	20, (int16_t)U8_TCMF_STEP_INT_DUTY_04_20,
																									 	30, (int16_t)U8_TCMF_STEP_INT_DUTY_04_30,
																									 	40, (int16_t)U8_TCMF_STEP_INT_DUTY_04_40,
																									 	50, (int16_t)U8_TCMF_STEP_INT_DUTY_04_50);
																									 
		WL_ESC->lcesps16StepIntDutySecbyCom = LCESP_s16IInter5Point(WL_ESC->lcesps16TCMF_Rise_STEP_Com, 10, (int16_t)U8_TCMF_STEP_INT_DUTY_06_10,
																									 	20, (int16_t)U8_TCMF_STEP_INT_DUTY_06_20,
																									 	30, (int16_t)U8_TCMF_STEP_INT_DUTY_06_30,
																									 	40, (int16_t)U8_TCMF_STEP_INT_DUTY_06_40,
																									 	50, (int16_t)U8_TCMF_STEP_INT_DUTY_06_50);
		
		WL_ESC->lcesps16StepIntDutybyComVol = LCESP_s16IInter2Point(vdc_msc_target_vol, MSC_4_V, WL_ESC->lcesps16StepIntDutyPribyCom, 
																						MSC_6_V, WL_ESC->lcesps16StepIntDutySecbyCom);
		
		
	}
	else if(vdc_msc_target_vol<MSC_8_V)
	{		
		WL_ESC->lcesps16StepIntDutyPribyCom = LCESP_s16IInter5Point(WL_ESC->lcesps16TCMF_Rise_STEP_Com, 10, (int16_t)U8_TCMF_STEP_INT_DUTY_06_10,
																									 	20, (int16_t)U8_TCMF_STEP_INT_DUTY_06_20,
																									 	30, (int16_t)U8_TCMF_STEP_INT_DUTY_06_30,
																									 	40, (int16_t)U8_TCMF_STEP_INT_DUTY_06_40,
																									 	50, (int16_t)U8_TCMF_STEP_INT_DUTY_06_50);
																									 
		WL_ESC->lcesps16StepIntDutySecbyCom = LCESP_s16IInter5Point(WL_ESC->lcesps16TCMF_Rise_STEP_Com, 10, (int16_t)U8_TCMF_STEP_INT_DUTY_08_10,
																									 	20, (int16_t)U8_TCMF_STEP_INT_DUTY_08_20,
																									 	30, (int16_t)U8_TCMF_STEP_INT_DUTY_08_30,
																									 	40, (int16_t)U8_TCMF_STEP_INT_DUTY_08_40,
																									 	50, (int16_t)U8_TCMF_STEP_INT_DUTY_08_50);
		
		WL_ESC->lcesps16StepIntDutybyComVol = LCESP_s16IInter2Point(vdc_msc_target_vol, MSC_6_V, WL_ESC->lcesps16StepIntDutyPribyCom, 
																						MSC_8_V, WL_ESC->lcesps16StepIntDutySecbyCom);
	}
	else if(vdc_msc_target_vol<MSC_10_V)
	{		
		WL_ESC->lcesps16StepIntDutyPribyCom = LCESP_s16IInter5Point(WL_ESC->lcesps16TCMF_Rise_STEP_Com, 10, (int16_t)U8_TCMF_STEP_INT_DUTY_08_10,
																									 	20, (int16_t)U8_TCMF_STEP_INT_DUTY_08_20,
																									 	30, (int16_t)U8_TCMF_STEP_INT_DUTY_08_30,
																									 	40, (int16_t)U8_TCMF_STEP_INT_DUTY_08_40,
																									 	50, (int16_t)U8_TCMF_STEP_INT_DUTY_08_50);
																									 
		WL_ESC->lcesps16StepIntDutySecbyCom = LCESP_s16IInter5Point(WL_ESC->lcesps16TCMF_Rise_STEP_Com, 10, (int16_t)U8_TCMF_STEP_INT_DUTY_10_10,
																									 	20, (int16_t)U8_TCMF_STEP_INT_DUTY_10_20,
																									 	30, (int16_t)U8_TCMF_STEP_INT_DUTY_10_30,
																									 	40, (int16_t)U8_TCMF_STEP_INT_DUTY_10_40,
																									 	50, (int16_t)U8_TCMF_STEP_INT_DUTY_10_50);
		
		WL_ESC->lcesps16StepIntDutybyComVol = LCESP_s16IInter2Point(vdc_msc_target_vol, MSC_8_V, WL_ESC->lcesps16StepIntDutyPribyCom, 
																						MSC_10_V, WL_ESC->lcesps16StepIntDutySecbyCom);
	}
	else if(vdc_msc_target_vol<MSC_12_V)
	{		
		WL_ESC->lcesps16StepIntDutyPribyCom = LCESP_s16IInter5Point(WL_ESC->lcesps16TCMF_Rise_STEP_Com, 10, (int16_t)U8_TCMF_STEP_INT_DUTY_10_10,
																									 	20, (int16_t)U8_TCMF_STEP_INT_DUTY_10_20,
																									 	30, (int16_t)U8_TCMF_STEP_INT_DUTY_10_30,
																									 	40, (int16_t)U8_TCMF_STEP_INT_DUTY_10_40,
																									 	50, (int16_t)U8_TCMF_STEP_INT_DUTY_10_50);
																									 
		WL_ESC->lcesps16StepIntDutySecbyCom = LCESP_s16IInter5Point(WL_ESC->lcesps16TCMF_Rise_STEP_Com, 10, (int16_t)U8_TCMF_STEP_INT_DUTY_12_10,
																									 	20, (int16_t)U8_TCMF_STEP_INT_DUTY_12_20,
																									 	30, (int16_t)U8_TCMF_STEP_INT_DUTY_12_30,
																									 	40, (int16_t)U8_TCMF_STEP_INT_DUTY_12_40,
																									 	50, (int16_t)U8_TCMF_STEP_INT_DUTY_12_50);
		
		WL_ESC->lcesps16StepIntDutybyComVol = LCESP_s16IInter2Point(vdc_msc_target_vol, MSC_10_V, WL_ESC->lcesps16StepIntDutyPribyCom, 
																						MSC_12_V, WL_ESC->lcesps16StepIntDutySecbyCom);
	}
	else
	{		
		WL_ESC->lcesps16StepIntDutyPribyCom = LCESP_s16IInter5Point(WL_ESC->lcesps16TCMF_Rise_STEP_Com, 10, (int16_t)U8_TCMF_STEP_INT_DUTY_12_10,
																									 	20, (int16_t)U8_TCMF_STEP_INT_DUTY_12_20,
																									 	30, (int16_t)U8_TCMF_STEP_INT_DUTY_12_30,
																									 	40, (int16_t)U8_TCMF_STEP_INT_DUTY_12_40,
																									 	50, (int16_t)U8_TCMF_STEP_INT_DUTY_12_50);
																									 
		WL_ESC->lcesps16StepIntDutySecbyCom = LCESP_s16IInter5Point(WL_ESC->lcesps16TCMF_Rise_STEP_Com, 10, (int16_t)U8_TCMF_STEP_INT_DUTY_12_10,
																									 	20, (int16_t)U8_TCMF_STEP_INT_DUTY_12_20,
																									 	30, (int16_t)U8_TCMF_STEP_INT_DUTY_12_30,
																									 	40, (int16_t)U8_TCMF_STEP_INT_DUTY_12_40,
																									 	50, (int16_t)U8_TCMF_STEP_INT_DUTY_12_50);
		
		WL_ESC->lcesps16StepIntDutybyComVol = LCESP_s16IInter2Point(vdc_msc_target_vol, MSC_12_V, WL_ESC->lcesps16StepIntDutyPribyCom, 
																						MSC_12_V, WL_ESC->lcesps16StepIntDutySecbyCom);
	}
}
#endif

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LAESPActuateNcNovalveF
	#include "Mdyn_autosar.h"               
#endif
