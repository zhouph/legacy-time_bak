/*******************************************************************/

/*******************************************************************/

/* Includes ********************************************************/


#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCCDCCallControl
	#include "Mdyn_autosar.h"
#endif

#include "LCCDCCallControl.h"

/* Local Definition  ***********************************************/

/* Variables Definition*********************************************/
extern uint8_t MAST_PRES;
extern uint8_t LAT_ACCEL;
extern uint8_t WHL_DMP_FL;
extern uint8_t WHL_DMP_FR;
extern uint8_t WHL_DMP_RL;
extern uint8_t WHL_DMP_RR;
extern uint8_t ROLL_DMP;
extern uint8_t CDC_STATUS;

/* Local Function prototype ****************************************/
#if defined(__UCC_1)
	void LCCDC_vCallControl(void);
	void ESP_TO_UCC_SENSOR(void);
	void UCC_SEND_DAMPING(void);
	void DELTA_YAW_FOR_DAMPING(void);
	void UCC_YAW_CONTROL_CAL_YAW_MOMENT(void);
	void UCC_DECISION_DAMPING(void);
	void LOW_MU_ROLL_DAMPING(void);
#endif

/* Implementation***************************************************/

#if defined(__UCC_1)

void LCCDC_vCallControl(void)                   
{
    ESP_TO_UCC_SENSOR();
    UCC_SEND_DAMPING();
}

/*******************************************************************/
void ESP_TO_UCC_SENSOR(void)
{
    if(ESP_ERROR_FLG||(!mp_hw_err_flg)|(!mp_sw_err_flg)) MAST_PRES = 0xFF;
    else {
        tempW2 = raw_mpress; tempW1 = 1; tempW0 = 10;
        s16muls16divs16();
        if ( tempW3 < 0 ) tempW3 = 0;
        else if ( tempW3 > 255 ) tempW3 = 255;
        MAST_PRES = tempW3; // bar  r: 1
    }
    
    if(ESP_ERROR_FLG||VDC_UNDER_LAT_ERR) LAT_ACCEL = 0xFF;
    else {
        tempW2 = a_lat_1_1000g2; tempW1 = 98; tempW0 = 1000;
        s16muls16divs16();
        tempW4 = tempW3+128; // m/s^2  r: 0.01
        if ( tempW4 < 0 ) tempW4 = 0;
        else if ( tempW4 > 255 ) tempW4 = 255;
        LAT_ACCEL = tempW4; // m/s^2  r: 0.01
    } 
}

void UCC_SEND_DAMPING(void)
{
    DELTA_YAW_FOR_DAMPING();
    UCC_YAW_CONTROL_CAL_YAW_MOMENT();
    UCC_DECISION_DAMPING();
    
    if(ESP_ERROR_FLG) ROLL_DMP = 0xFF;
    else              ROLL_DMP = 0;
}

void DELTA_YAW_FOR_DAMPING(void)
{

    if(wstr >= 0 && yaw_out < 0) delta_yaw_damping = -delta_yaw;//-delta_yaw_damping1;
    else if(wstr < 0 && yaw_out >= 0) delta_yaw_damping = -delta_yaw;//-delta_yaw_damping1;
    else                              delta_yaw_damping = delta_yaw;//delta_yaw_damping1;
    
}

void UCC_YAW_CONTROL_CAL_YAW_MOMENT(void)
{
    
    if ( VDC_DISABLE_FLG ) {
        ucc_moment_q_front = 0;
    } else {
        if ( yaw_error > 0 ) {    // OVER Control
               
            if ( yaw_error_dot > 0 ) {      // Oversteer Inc.
            
            esp_tempW2 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_P_GAIN_F_OS_OUT_INC_H, 
                                 S16_YC_P_GAIN_F_OS_OUT_INC, S16_YC_P_GAIN_F_OS_OUT_INC_L);
            esp_tempW0 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_D_GAIN_F_OS_OUT_INC_H, 
                                 S16_YC_D_GAIN_F_OS_OUT_INC, S16_YC_D_GAIN_F_OS_OUT_INC_L);                                              
     
            } else {                        // Oversteer Dec.
            
            esp_tempW2 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_P_GAIN_F_OS_OUT_DEC_H, 
                                S16_YC_P_GAIN_F_OS_OUT_DEC, S16_YC_P_GAIN_F_OS_OUT_DEC_L);
            esp_tempW0 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_D_GAIN_F_OS_OUT_DEC_H, 
                                S16_YC_D_GAIN_F_OS_OUT_DEC, S16_YC_D_GAIN_F_OS_OUT_DEC_L);                                            
            }               

                yaw_control_UCC_P_gain_front = -esp_tempW2;
                yaw_control_UCC_D_gain_front = -esp_tempW0;         
           
        }  else {                                                // UNDER Control

            if ( yaw_error_dot > 0 ) {      
            
            esp_tempW0 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_D_GAIN_F_OS_OUT_INC_H, 
                                 S16_YC_D_GAIN_F_OS_OUT_INC, S16_YC_D_GAIN_F_OS_OUT_INC_L);                                              
     
            } else {                        
            
            esp_tempW0 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_D_GAIN_F_OS_OUT_DEC_H, 
                                S16_YC_D_GAIN_F_OS_OUT_DEC, S16_YC_D_GAIN_F_OS_OUT_DEC_L);                                            
            }               

                yaw_control_UCC_P_gain_front = 0;
                yaw_control_UCC_D_gain_front = -esp_tempW0;         

        }
    }       
    
    tempW2 = yaw_error; tempW1 = yaw_control_UCC_P_gain_front; tempW0 = YAW_SCALE;
    s16muls16divs16();   
    esp_tempW1 = tempW3;   
    tempW2 = yaw_error_dot; tempW1 = yaw_control_UCC_D_gain_front; tempW0 = YAW_DOT_SCALE;
    s16muls16divs16();
    ucc_moment_q_front =(esp_tempW1 + tempW3)/SLIP_TARGET_GAIN;     
    if ( moment_q_front < ucc_moment_q_front ) ucc_moment_q_front = moment_q_front;
    
}

void UCC_DECISION_DAMPING(void)
{                                       //det_alatm
    if(delta_yaw_damping >= 0)
    {
        cdc_q = -ucc_moment_q_front;
    }
    else
    {
        cdc_q = ucc_moment_q_front;
    }
    if(CDC_STATUS == 2)
    {
        if(esp_mu < S16_MU_MED_HIGH && ljucRoadCond == 8)
        {

            LOW_MU_ROLL_DAMPING();
        }
        else
        {
        if((vref5 < VREF_15_KPH) || (BLS == 1))
        {
            WHL_DMP_FL = 0;
            WHL_DMP_FR = 0;
            WHL_DMP_RL = 0;
            WHL_DMP_RR = 0;
        }
        else
        {
            if(cdc_q >= 9)
            {
                WHL_DMP_FL = 7;
                WHL_DMP_FR = 7;
                WHL_DMP_RL = 1;
                WHL_DMP_RR = 1;
            }
            else if(cdc_q < 9 && cdc_q >= 3)
            {
                WHL_DMP_FL = 7;
                WHL_DMP_FR = 7;
                WHL_DMP_RL = 9 - cdc_q;
                WHL_DMP_RR = 9 - cdc_q;
            }
            else if(cdc_q <= -3 && cdc_q > -9)
            {
                WHL_DMP_FL = 9 + cdc_q;
                WHL_DMP_FR = 9 + cdc_q;
                WHL_DMP_RL = 7;
                WHL_DMP_RR = 7;
            }
            else if(cdc_q <= -9)
            {
                WHL_DMP_FL = 1;
                WHL_DMP_FR = 1;
                WHL_DMP_RL = 7;
                WHL_DMP_RR = 7;
            }
            else
            {
                WHL_DMP_FL = 0;
                WHL_DMP_FR = 0;
                WHL_DMP_RL = 0;
                WHL_DMP_RR = 0;
                
                }
            }
            }

            if ( ESP_ERROR_FLG )
            {
                WHL_DMP_FL = 0;
                WHL_DMP_FR = 0;
                WHL_DMP_RL = 0;
                WHL_DMP_RR = 0;
        }
    }
}

/***** Winter test �߰����� ************/
void LOW_MU_ROLL_DAMPING(void)
{
    if(loucFlagRoll == 1)
    {
        WHL_DMP_FL = 5;//7;//4;
        WHL_DMP_FR = 5;//7;//4;
        WHL_DMP_RL = 3;//3;//1;//3;
        WHL_DMP_RR = 3;//3;//1;//3;
    }
    else
    {
        WHL_DMP_FL = 0;
        WHL_DMP_FR = 0;
        WHL_DMP_RL = 0;
        WHL_DMP_RR = 0;
    }
}

#endif		// #if __EDC ~

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCCDCCallControl
	#include "Mdyn_autosar.h"
#endif    
