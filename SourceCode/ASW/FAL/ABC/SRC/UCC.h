#if defined(__UCC_1)

 

extern int16_t cdc_wz_fl_raw , cdc_wz_fr_raw ;
extern int16_t cdc_wz_fl_raw_old , cdc_wz_fr_raw_old ;
extern int16_t cdc_wz_fl_HF , cdc_wz_fr_HF ;
extern int16_t cdc_wz_fl_HF_old , cdc_wz_fr_HF_old ;
extern int16_t cdc_wz_fl , cdc_wz_fr ;
extern int16_t cdc_wz_fl_old , cdc_wz_fr_old ;

extern int16_t cdc_bz_fl_raw , cdc_bz_fr_raw, cdc_bz_rr_raw;
extern int16_t cdc_bz_fl_raw_old , cdc_bz_fr_raw_old, cdc_bz_rr_raw_old;
extern int16_t cdc_bz_fl_HF , cdc_bz_fr_HF, cdc_bz_rr_HF;
extern int16_t cdc_bz_fl_HF_old , cdc_bz_fr_HF_old, cdc_bz_rr_HF_old;
extern int16_t cdc_bz_fl , cdc_bz_fr,  cdc_bz_rr;
extern int16_t cdc_bz_fl_old , cdc_bz_fr_old, cdc_bz_rr_old;

extern int16_t cdc_bump_counter_fl;
extern int16_t cdc_bump_counter_fr;
extern int16_t cdc_PotHole_counter_fl;
extern int16_t cdc_PotHole_counter_fr;

extern int16_t override_angle, override_angle_old;
extern int16_t override_assist, override_assist_old;
extern int16_t wstr_org;

// o_delta_yaw_thres for CDC control
extern int16_t cdc_o_delta_yaw_thres[4];
extern int16_t cdc_u_delta_yaw_thres[4];

// delta_V & LFC rate for normal force
extern int16_t vfiltn_normal_load_front;
extern int16_t vfiltn_normal_load_rear;
extern int16_t LFC_gain_normal_load_front;
extern int16_t LFC_gain_normal_load_rear;
extern int16_t cdc_pitch_index;
extern int16_t Lateral_Force_Sign;
extern int16_t Delta_Yaw_Damping_Force_Sign;
extern int16_t delta_yaw_damping;
extern int16_t delta_yaw_damping1;

extern int16_t lrswBodyvAccel[4];
extern int16_t lrswCorrectBodyvAccel[5];
extern int16_t lrswCorrectBodyvAccelU[5];
extern int16_t lrswCorrectBodyvAccel_1[5];
extern int16_t lrswCorrectBodyvAccelHF[5];
extern int16_t lrswCorrectBodyvAccelHF_1[5];
extern int16_t lrswBodyvVel[5];
extern int16_t lrswBodyvVelU[5];
extern int16_t lrswBodyvVelU_1[5];
extern int16_t lrswBodyvVelHF[5];
extern int16_t lrswBodyvVelHF_1[5];
extern int16_t lrswModalVelPitch;
extern int16_t luswUk;
extern int16_t luswYk1;
extern int32_t luslTmp[4];
extern int16_t luswYk;

extern int8_t lrscCoefCorrectAccel[3][4];

extern int16_t wstr_10deg;                                    
extern int16_t vrefk_10kph;
extern int16_t loswRolA;
extern uint16_t louwRolA_abs;
extern uint16_t alat_abs;
extern uint8_t loucFlagRoll;
extern uint8_t lducFlagDive;
extern uint8_t lsucFlagSquat;
extern uint8_t ljucRoadCond;

extern int16_t cdc_q;
extern int16_t yaw_control_UCC_P_gain_front, yaw_control_UCC_D_gain_front, ucc_moment_q_front;

extern int16_t afs_p_gain, afs_d_gain, afs_i_gain, afs_control_input;
extern int16_t delta_yaw_error, delta_yaw_integrate, delta_yaw_integrate_old, delta_yaw_diff;
extern int16_t delta_yaw_error_buf[4];

 

#endif


