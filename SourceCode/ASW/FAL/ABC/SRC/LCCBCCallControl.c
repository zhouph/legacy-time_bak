/******************************************************************************
*  Project Name: CORNER BRAKE CONTROL
*  File: LCCBCCallControl.C
*  Description:
*  Date: April. 17. 2006      
*******************************************************************************
*  Modification   Log                                             
*  Date           Author           Description                    
*  -------       ----------    --------------------------------------------      
*  06.04.17      yongkil Kim   Initial Release
*  07.07.04      JinKoo Lee    Modified for BK application                
*  07.07.23      JinKoo Lee    Ax,Ay,Mpres Estimation for ABS is added     
*  10.08.31      JinKoo Lee    HMC/KMC CBC concept is changed as ABS function from VF  
*  11.01.26      JinKoo Lee    ABS_CBC is disabled for 4WD 
*******************************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
    #define idx_START
    #define idx_FILE    idx_CL_LCCBCCallControl
    #include "Mdyn_autosar.h"
#endif
/* Includes ******************************************************************/


#include "LCCBCCallControl.h"
#include "LACBCCallActHW.h"
#include "LDABSDctWheelStatus.h"
#include "LAABSCallMotorSpeedControl.h"
#include "LCESPCalInterpolation.h"
#include "LCESPCalLpf.h"
#include "LCallMain.h"
#include "LCABSDecideCtrlState.h"
#include "LCABSCallControl.h"

#if __CBC

/* Local Definiton  *********************************************************/
/*
  #if __VDC
#define  TRACK_WIDTH_F      pEspModel->S16_WHEEL_TREAD_FRONT
#define  TRACK_WIDTH_R      pEspModel->S16_WHEEL_TREAD_REAR
  #endif
*/

/* Tuning Parameter *********************************************************/
  

/* Variables Definition*******************************************************/

struct  U8_BIT_STRUCT_t CBCC0;
struct  U8_BIT_STRUCT_t CBCC1;
struct  U8_BIT_STRUCT_t CBCC2;
struct  U8_BIT_STRUCT_t CBCC3;
struct  U8_BIT_STRUCT_t CBCC4;
struct  U8_BIT_STRUCT_t CBCC5;
struct  U8_BIT_STRUCT_t CBCC6;

CBC_WHL_CTRL_t lccbcRL, lccbcRR;
#if __FRONT_CBC_ENABLE
CBC_WHL_CTRL_t lccbcFL, lccbcFR;
#endif
 
int16_t CBC_target_p;
int16_t CBC_target_delta_p;
int8_t lccbcs8RiseHoldCnt,lccbcs8DumpHoldCnt;
int8_t lccbcs8RiseHoldMax,lccbcs8DumpHoldMax;
int16_t rise_press_dead_p, dump_press_dead_p;
int16_t lccbcs16DvRL,lccbcs16DvRR;

int16_t CBC_tempW9,CBC_tempW8,CBC_tempW7,CBC_tempW6,CBC_tempW5;
int16_t CBC_tempW4,CBC_tempW3,CBC_tempW2,CBC_tempW1,CBC_tempW0;

#if (__ABS_CBC_DISABLE==0)
int16_t lccbcs16EstAy,lccbcs16EstAyOld,lccbcs16EstAyRaw;
int16_t lccbcs16EstAx,lccbcs16EstAxOld,lccbcs16EstAxRaw;
int16_t lccbcs16EstMpress,lccbcs16EstMpressOld,lccbcs16EstMpressRaw;
int16_t lccbcs16EstWPressFL,lccbcs16EstWPressFR,lccbcs16EstWPressRL,lccbcs16EstWPressRR;
uint8_t lccbcu8PartialBrakeCounter;
int16_t lccbcs16EstVref,lccbcs16EstVrefOld;
int16_t lccbcs16EstAy2,lccbcs16EstAy2Old,lccbcs16EstAy2Raw;
int16_t lccbcs16AyCheckCounter,lccbcs16AyCheckCounter2;
int16_t lccbcs16AyOKCounter;
#endif

#if __FRONT_CBC_ENABLE
static  int16_t CBC_target_p_front,CBC_target_delta_p_front,press_dead_p_front;
int8_t lccbcs8RiseHoldCntFront,lccbcs8DumpHoldCntFront;
static  int16_t lccbcs8RiseHoldMaxFront,lccbcs8DumpHoldMaxFront;
static  int16_t lccbcs16DvFL,lccbcs16DvFR;
#endif	/* #if __FRONT_CBC_ENABLE */

/*static*/ int16_t lccbcs16CbcMpress, lccbcs16CbcAlat, lccbcs16FadeOutMaxTime;
uint8_t lccbcu8CbcMode;
static uint8_t lccbcu8ReCBCcnt;
int16_t lccbcs16ReardeltaP, lccbcs16FrontdeltaP;
int16_t cbc_alat_enter_th, cbc_alat_exit_th;

/* LocalFunction prototype ***************************************************/

#if (__ABS_CBC_DISABLE==0)
void LDCBC_vEstVehicleLateralG(void);
void LDCBC_vEstVehicleLongitudinalG(void);
void LDCBC_vEstVehicleMpress(void);
void LDCBC_vEstWheelPress(void);
#endif

void LCCBC_vCallControl(void);
static void LCCBC_vCheckInhibition(void);
static void LCCBC_vSelectCBCMode(void);
static void LCCBC_vCheckOtherCtrl(void);
static void LCCBC_vDetectLimitCorner(void);
static void LCCBC_vDetectPartialBrake(void);
static void LCCBC_vDetectInsideWheelSlip(void);
static void LCCBC_vDetectVSpeedCondition(void);
static void LCCBC_vDecideCBCControl(void);
static void LCCBC_vDecideCBCControlWL(CBC_WHL_CTRL_t *pCbcWL);
void LCCBC_vCallCalControlValue(void);
void LCCBC_vCallClearValveMotor(void);
void LCCBC_vCallCalControlPressure(void);
void LCCBC_vCallControlFadeOut(void);
static void LCCBC_vCallControlFadeOutWL(CBC_WHL_CTRL_t *pCbcWL, CBC_WHL_CTRL_t *pCbcCountWL);
int8_t LCCBC_s8CallControlFOModeWL(CBC_WHL_CTRL_t *pCbcWL);
static void LCCBC_vResetFadeOut(CBC_WHL_CTRL_t *pCbcWL);
void LCCBC_vResetControl(void);

/* Implementation*************************************************************/

void LCCBC_vCallControl(void)
{
    LCCBC_vCheckInhibition();
    LCCBC_vSelectCBCMode();
    LCCBC_vCheckOtherCtrl();
    LCCBC_vDetectLimitCorner();
    LCCBC_vDetectPartialBrake();
    LCCBC_vDetectInsideWheelSlip();
    LCCBC_vDetectVSpeedCondition();

    LCCBC_vDecideCBCControl();
    LCCBC_vCallCalControlValue();
    LCCBC_vCallClearValveMotor();
    LCCBC_vCallCalControlPressure();

    if ((CBC_FADE_OUT_rl==1)||(CBC_FADE_OUT_rr==1)||(CBC_ACTIVE_rl==1)||( CBC_ACTIVE_rr==1)
  #if __FRONT_CBC_ENABLE
    || (CBC_FADE_OUT_fl==1)||(CBC_FADE_OUT_fr==1)||(CBC_ACTIVE_fl==1)||( CBC_ACTIVE_fr==1)
  #endif /* __FRONT_CBC_ENABLE */
       )
    {
        CBC_ON=1;
    }
    else
    {
        CBC_ON=0;
    }

    LCCBC_vCallControlFadeOut();
    
    CBC_ENABLE_old = CBC_ENABLE;
}

static void LCCBC_vCheckInhibition(void)
{
  #if __GM_FailM  
    if((init_end_flg==0)||(wu8IgnStat==CE_OFF_STATE)||
       (fu1ABSEcuHWErrDet==1)||(fu1FrontWSSErrDet==1)||(fu1RearWSSErrDet==1)||
       (fu1WheelFLSusDet==1)||(fu1WheelFRSusDet==1)||(fu1WheelRLSusDet==1)||(fu1WheelRRSusDet==1)
     #if (__CAR_MAKER==GM_KOREA)
       ||(fu8EngineModeStep==0)
     #endif
       )
  #else  /* HMC  */ 
    if((fu1OnDiag==1)||(init_end_flg==0)||(wu8IgnStat==CE_OFF_STATE)||(abs_error_flg==1))
  #endif 
    {
        CBC_INHIBIT_ABS_FLAG = 1;
    }
    else
    {
        CBC_INHIBIT_ABS_FLAG = 0;
    }  
  

#if __VDC   
  #if __GM_FailM 
    if((init_end_flg==0)||(wu8IgnStat==CE_OFF_STATE)||
       (fu1ESCEcuHWErrDet==1)||(fu1ESCSensorErrDet==1)||(fu1AySusDet==1)||(fu1MCPSusDet==1)||(fu1MCPErrorDet==1)
     #if (__CAR_MAKER==GM_KOREA)
       ||(fu8EngineModeStep<=1)
     #endif
       ) 
  #else  /* HMC  */
    if((fu1OnDiag==1)||(init_end_flg==0)||(wu8IgnStat==CE_OFF_STATE)||
       (fu1YawSusDet==1)||(fu1YawErrorDet==1)||(fu1AySusDet==1)||(fu1AyErrorDet==1)||(fu1StrSusDet==1)||(fu1StrErrorDet==1)||(fu1MCPSusDet==1)||(fu1MCPErrorDet==1)
	)
  #endif   
    {
        CBC_INHIBIT_ESC_FLAG = 1;
      #if (__ABS_CBC_DISABLE==1)   
        CBC_INHIBIT_ABS_FLAG = 1;
      #endif   

    }
    else
    {
        CBC_INHIBIT_ESC_FLAG = 0;
    }   
 #endif
}

static void LCCBC_vSelectCBCMode(void)
{
    if(CBC_INHIBIT_ABS_FLAG == 0)
    {
      #if __VDC 
        if(CBC_INHIBIT_ESC_FLAG==0)
        {
            lccbcu8CbcMode    = U8_ESC_CBC;
		    lccbcs16CbcMpress = lcabss16RefMpress;
		    lccbcs16CbcAlat   = alat;
		    lccbcs16WPressRL  = RL.s16_Estimated_Active_Press;
		    lccbcs16WPressRR  = RR.s16_Estimated_Active_Press;
		    dump_press_dead_p = U8_CBC_DUMP_DEAD_PRESS;
            rise_press_dead_p = U8_CBC_RISE_DEAD_PRESS;
		  #if __FRONT_CBC_ENABLE  
		    lccbcs16WPressFL  = FL.s16_Estimated_Active_Press;
		    lccbcs16WPressFR  = FR.s16_Estimated_Active_Press;
		  #endif
        }
        else
      #endif    
        {
            lccbcu8CbcMode    = U8_ABS_CBC;
		    lccbcs16CbcMpress = lccbcs16EstMpress;
		    lccbcs16CbcAlat   = lccbcs16EstAy;
		    lccbcs16WPressRL  = lccbcs16EstWPressRL;
		    lccbcs16WPressRR  = lccbcs16EstWPressRR;
		    dump_press_dead_p = U8_CBC_DUMP_DEAD_PRESS_ABS;
	        rise_press_dead_p = U8_CBC_RISE_DEAD_PRESS_ABS;
		  #if __FRONT_CBC_ENABLE  
		    lccbcs16WPressFL  = lccbcs16EstWPressFL;
		    lccbcs16WPressFR  = lccbcs16EstWPressFR;
		  #endif  
        }       
    }
    else
    {
        lccbcu8CbcMode = U8_CBC_INHIBIT;
	    lccbcs16CbcMpress = 0;
	    lccbcs16CbcAlat   = 0;
	    lccbcs16WPressRL  = 0;
	    lccbcs16WPressRR  = 0;
		  #if __FRONT_CBC_ENABLE  
	    lccbcs16WPressFL  = 0;
	    lccbcs16WPressFR  = 0;
		  #endif  
    } 
}

static void LCCBC_vCheckOtherCtrl(void)
{
    /*________EBD Wheel Check */
    if (EBD_rl==0)
    {
        EBD_Dump_chk_rl=0;
        lccbcRL.u1CbcEbdDump = 0;
    }
    else if (RL.EBD_MODE==DUMP_START)
    {
        EBD_Dump_chk_rl=1;
        lccbcRL.u1CbcEbdDump = 1;
    }
    else
    {
    	lccbcRL.u1CbcEbdDump = 0;
    }

    if (EBD_rr==0)
    {
        EBD_Dump_chk_rr=0;
        lccbcRR.u1CbcEbdDump = 0;
    }
    else if (RR.EBD_MODE==DUMP_START)
    {
        EBD_Dump_chk_rr=1;
        lccbcRR.u1CbcEbdDump = 1;
    }
    else 
    {
    	lccbcRR.u1CbcEbdDump = 0;
    }

    /*________ABS Control Check */
    if (((lccbcu8CbcMode==U8_ABS_CBC) && (ABS_fz==1))
#if __VDC
      ||((lccbcu8CbcMode==U8_ESC_CBC) && (((ABS_fr==1)&&(lccbcs16CbcAlat > 0))||((ABS_fl==1)&&(lccbcs16CbcAlat < 0))||(ABS_rl==1)||(ABS_rr==1)))
#endif
    )
    {
        lccbcu1AbsCtrl = 1;
        lccbcu1InsideAbsCtrl = 0;
    }
#if __VDC
    else if(((ABS_fr==1)&&(lccbcs16CbcAlat < 0))||((ABS_fl==1)&&(lccbcs16CbcAlat > 0))) /* Only Inside Front ABS wheel */
    {
    	lccbcu1AbsCtrl = 0;
        lccbcu1InsideAbsCtrl = 1;
    }
#endif
    else
    {
    	lccbcu1AbsCtrl = 0;
        lccbcu1InsideAbsCtrl = 0;
    }

  #if __FRONT_CBC_ENABLE
    lccbcFL.u1CbcAbsAct = ABS_fl;
    lccbcFR.u1CbcAbsAct = ABS_fr;
  #endif
    lccbcRL.u1CbcAbsAct = ABS_rl;
    lccbcRR.u1CbcAbsAct = ABS_rr;
    
    
    /*________ESC Brake Control Check */
#if __VDC
  #if __SPLIT_TYPE==0                      /*X Split  */
    if ((ESP_BRAKE_CONTROL_FR==1)||(ESP_BRAKE_CONTROL_RL==1))   /*GV5*/
    {
        lccbcFR.u1CbcEscAct = 1;
        lccbcRL.u1CbcEscAct = 1;
    }
    else
    {
    	lccbcFR.u1CbcEscAct = 0;
        lccbcRL.u1CbcEscAct = 0;
    }
    if ((ESP_BRAKE_CONTROL_FL==1)||(ESP_BRAKE_CONTROL_RR==1))
    {
        lccbcFL.u1CbcEscAct = 1;
        lccbcRR.u1CbcEscAct = 1;
    }
    else
    {
        lccbcFL.u1CbcEscAct = 0;
        lccbcRR.u1CbcEscAct = 0;
    }
  #else                                         /*FR Split  */
    if ((ESP_BRAKE_CONTROL_FR==1)||(ESP_BRAKE_CONTROL_FL==1))
    {
        lccbcFR.u1CbcEscAct = 1;
        lccbcFL.u1CbcEscAct = 1;
    }
    else
    {
    	lccbcFR.u1CbcEscAct = 0;
        lccbcFL.u1CbcEscAct = 0;
   }
    if ((ESP_BRAKE_CONTROL_RL==1)||(ESP_BRAKE_CONTROL_RR==1))
    {
        lccbcRL.u1CbcEscAct = 1;
        lccbcRR.u1CbcEscAct = 1;
    }
    else
    {
        lccbcRL.u1CbcEscAct = 0;
        lccbcRR.u1CbcEscAct = 0;
    }
  #endif
#endif
    
}

static void LCCBC_vDetectLimitCorner(void)
{
  /*---------------------------------------------------------------------------------------------*/
      #if __VDC
    int16_t cbc_alat_enter_th_hispeed,cbc_alat_enter_th_medspeed,cbc_alat_enter_th_lowspeed;
      #endif
  /*---------------------------------------------------------------------------------------------*/  
    
      #if __VDC 
    if(lccbcu8CbcMode == U8_ESC_CBC)
    {
	    cbc_alat_exit_th  = S16_CBC_ALAT_EXIT;
	    if(cbc_alat_exit_th < S16_CBC_MIN_ALAT_EXIT_TH)
	    {
	        cbc_alat_exit_th = S16_CBC_MIN_ALAT_EXIT_TH;
	    }
	    else{}

	    cbc_alat_enter_th_hispeed = LCESP_s16IInter2Point (McrAbs(wstr_dot),   WSTR_0_DEG,     S16_CBC_ALAT_ENTER_SPD_H_SLOW_STR,
	                                                                           WSTR_360_DEG,   S16_CBC_ALAT_ENTER_SPD_H_FAST_STR);
	
	    cbc_alat_enter_th_medspeed = LCESP_s16IInter2Point (McrAbs(wstr_dot),  WSTR_0_DEG,     S16_CBC_ALAT_ENTER_SPD_M_SLOW_STR,
	                                                                           WSTR_360_DEG,   S16_CBC_ALAT_ENTER_SPD_M_FAST_STR);
	
	    cbc_alat_enter_th_lowspeed = LCESP_s16IInter2Point (McrAbs(wstr_dot),  WSTR_0_DEG,     S16_CBC_ALAT_ENTER_SPD_S_SLOW_STR,
	                                                                           WSTR_360_DEG,   S16_CBC_ALAT_ENTER_SPD_S_FAST_STR);
	
	    cbc_alat_enter_th = LCESP_s16IInter3Point(vref, S16_CBC_THRES_LOW_SPEED, cbc_alat_enter_th_lowspeed,
	                                                    S16_CBC_THRES_MED_SPEED, cbc_alat_enter_th_medspeed,
	                                                    S16_CBC_THRES_HIGH_SPEED,cbc_alat_enter_th_hispeed);
	
	    if(cbc_alat_enter_th < S16_CBC_MIN_ALAT_ENTER_TH )
	    {
	        cbc_alat_enter_th = S16_CBC_MIN_ALAT_ENTER_TH;
	    }
	    else{} 
	}
	else
      #endif    
	{
	    cbc_alat_exit_th = S16_CBC_ALAT_EXIT_ABS;
	    if(cbc_alat_exit_th < S16_CBC_MIN_ALAT_EXIT_TH_ABS)
	    {
	        cbc_alat_exit_th = S16_CBC_MIN_ALAT_EXIT_TH_ABS;
	    }
	    else{}

	    cbc_alat_enter_th = LCESP_s16IInter3Point(vref, S16_CBC_THRES_LOW_SPEED ,S16_CBC_ALAT_ENTER_SPD_S,
	                                                    S16_CBC_THRES_MED_SPEED ,S16_CBC_ALAT_ENTER_SPD_M,
	                                                    S16_CBC_THRES_HIGH_SPEED,S16_CBC_ALAT_ENTER_SPD_H);
	    
	    if(cbc_alat_enter_th < S16_CBC_MIN_ALAT_ENTER_TH_ABS )
	    {
	        cbc_alat_enter_th = S16_CBC_MIN_ALAT_ENTER_TH_ABS;
	    }
	    else{}

	    if(lccbcu1AyEstErrorSus==1)
	    {
	        cbc_alat_exit_th  = cbc_alat_exit_th  + LAT_0G3G;
	        cbc_alat_enter_th = cbc_alat_enter_th + LAT_0G3G;
	    }
	    else if(lccbcu1ReCBCFlag==1)
	    {
	        cbc_alat_enter_th = cbc_alat_enter_th + LCESP_s16IInter2Point(lccbcu8ReCBCcnt,U8_CBC_COMP_TH_MIN_CNT,LAT_0G3G,U8_CBC_COMP_TH_MAX_CNT, LAT_0G1G);
	    }
	    else{}	    
	}
	
	CBC_tempW0 = McrAbs(lccbcs16CbcAlat);
	
    if(CBC_ON == 0)
    {
    	cbc_alat_exit_th = cbc_alat_enter_th;
    }
    else{}

    if((lccbcu1AbsCtrl==1) || ((lccbcu8CbcMode==U8_ABS_CBC) && (ldcbcu1AyEstValid==0)))
    {
        DETECT_LIMIT_CORNER =0;
    } 
    else if ((CBC_tempW0 > cbc_alat_enter_th)&&(DETECT_LIMIT_CORNER==0))
    {
        DETECT_LIMIT_CORNER =1;
    }
    else if ((CBC_tempW0 > cbc_alat_exit_th)&&(DETECT_LIMIT_CORNER==1))
    {
        DETECT_LIMIT_CORNER =1;
    }
    else
    {
        DETECT_LIMIT_CORNER =0;
    }    
}

static void LCCBC_vDetectPartialBrake(void)
{
  /*---------------------------------------------------------------------------------------------*/
    int16_t cbc_mpress_enter_th,cbc_mpress_exit_th;
    int16_t cbc_mpress_enter_th_low, cbc_mpress_exit_th_low;
  /*---------------------------------------------------------------------------------------------*/  
      #if __VDC 
    if(lccbcu8CbcMode == U8_ESC_CBC)
    {
	    cbc_mpress_enter_th = S16_CBC_MPRESS_ENTER;
        cbc_mpress_exit_th = S16_CBC_MPRESS_EXIT;
        
	    if(((fu1BLSErrDet==1)||(fu1BlsSusDet==1))&&(S16_CBC_BRAKING_DET_THRES<=MPRESS_15BAR))
	    {
	        cbc_mpress_enter_th_low = MPRESS_15BAR;
	    }
	    else
	    {
	        cbc_mpress_enter_th_low = S16_CBC_BRAKING_DET_THRES;
	    }
	    /* Limit Corner reset condition */
	    if(cbc_mpress_enter_th_low<S16_CBC_BRAKING_MIN_THRES)
	    {
	    	cbc_mpress_enter_th_low = S16_CBC_BRAKING_MIN_THRES;
	    }
	    else{}
	    
	    cbc_mpress_exit_th_low = S16_CBC_BRAKING_MIN_THRES;
	    
	}
	else
      #endif    
	{
	    cbc_mpress_enter_th = S16_CBC_MPRESS_ENTER_ABS;
        cbc_mpress_exit_th  = S16_CBC_MPRESS_EXIT_ABS;
        cbc_mpress_enter_th_low = S16_CBC_BRAKING_DET_THRES_ABS;	    
	    /* Limit Corner reset condition */
	    if(cbc_mpress_enter_th_low < S16_CBC_BRAKING_MIN_THRES_ABS)
	    {
	    	cbc_mpress_enter_th_low = S16_CBC_BRAKING_MIN_THRES_ABS;
	    }
	    else{}
	    
	    cbc_mpress_exit_th_low = S16_CBC_BRAKING_MIN_THRES_ABS;
	    
	}

    if(CBC_ON == 0)
    {
        cbc_mpress_exit_th = cbc_mpress_enter_th;
        cbc_mpress_exit_th_low = cbc_mpress_enter_th_low;
    }
    else{}
    
    if ((DETECT_PARTIAL_BRAKING==0) && (lccbcs16CbcMpress < cbc_mpress_enter_th) && (lccbcs16CbcMpress >= cbc_mpress_enter_th_low)) 
    {
	      #if __VDC 
	    if(lccbcu8CbcMode == U8_ESC_CBC)
	    {
	        if((lccbcs16WPressRL >= cbc_mpress_enter_th_low) && (lccbcs16WPressRR >= cbc_mpress_enter_th_low))
	        {
	            DETECT_PARTIAL_BRAKING =1;
	        }
            else
            {
                DETECT_PARTIAL_BRAKING =0;
            }
		}
		else
	      #endif    
		{
	        if( lccbcu8PartialBrakeCounter >= L_U8_TIME_10MSLOOP_70MS )
	        {
	            DETECT_PARTIAL_BRAKING =1;
	        }
	        else
	        {
	            DETECT_PARTIAL_BRAKING =0;
	            lccbcu8PartialBrakeCounter = lccbcu8PartialBrakeCounter + 1;
	        }
		}
    }
    else if ((lccbcs16CbcMpress < cbc_mpress_exit_th) && (DETECT_PARTIAL_BRAKING==1) && (lccbcs16CbcMpress >= cbc_mpress_exit_th_low))
    {
        DETECT_PARTIAL_BRAKING =1;
    }
    else
    {
        DETECT_PARTIAL_BRAKING =0;
        lccbcu8PartialBrakeCounter=0;
    }
}

static void LCCBC_vDetectInsideWheelSlip(void)
{
  #if __FRONT_CBC_ENABLE
    int16_t lccbcs16FrontDvEnter,lccbcs16FrontDvExit;
  #endif /* __FRONT_CBC_ENABLE */		
    int16_t lccbcs16RearDvEnter,lccbcs16RearDvExit;

  #if (__CBC_WHEEL_SLIP_CONDITION==ENABLE)  
    
    if(CBC_ENABLE == 1)
    {
    	lccbcu1CbcSlipOK = 1;
    }    
    else if(lccbcu8CbcMode != U8_ESC_CBC)
    {
    	lccbcu1CbcSlipOK = 1; /* Not consider slip condition in ABS-CBC ctrl */
    }   	
    else
    {
	    if  (det_alatm>S16_CBC_DELTA_PRESS_ALAT_LEVEL_H)      
	    {
	        CBC_tempW2 = S16_CBC_DECEL_THRES_H; 
	        CBC_tempW3 = S8_CBC_SLIP_THRES_H;   
	        CBC_tempW4 = S8_CBC_ARAD_THRES_H;   
	    }
	    else if  (det_alatm>S16_CBC_DELTA_PRESS_ALAT_LEVEL_M) 
	    {
	        CBC_tempW2 = S16_CBC_DECEL_THRES_M; 
	        CBC_tempW3 = S8_CBC_SLIP_THRES_M;
	        CBC_tempW4 = S8_CBC_ARAD_THRES_M;
	    }                     
	    else
	    {
	        CBC_tempW2 = S16_CBC_DECEL_THRES_L; 
	        CBC_tempW3 = S8_CBC_SLIP_THRES_L;
	        CBC_tempW4 = S8_CBC_ARAD_THRES_L;
	    }
	
	    CBC_tempW2 = CBC_tempW2 - LCESP_s16IInter3Point(vref,   S16_CBC_THRES_LOW_SPEED, 0,
	                                                            S16_CBC_THRES_MED_SPEED, S16_CBC_DECEL_THRES_COMP_M,
	                                                            S16_CBC_THRES_HIGH_SPEED,S16_CBC_DECEL_THRES_COMP_H);
	    
	    CBC_tempW3 = CBC_tempW3 - LCESP_s16IInter3Point(vref,   S16_CBC_THRES_LOW_SPEED, 0,
	                                                            S16_CBC_THRES_MED_SPEED, S8_CBC_SLIP_THRES_COMP_M,
	                                                            S16_CBC_THRES_HIGH_SPEED,S8_CBC_SLIP_THRES_COMP_H);
	    
	    CBC_tempW4 = CBC_tempW4 - LCESP_s16IInter3Point(vref,   S16_CBC_THRES_LOW_SPEED, 0,
	                                                            S16_CBC_THRES_MED_SPEED, S8_CBC_ARAD_THRES_COMP_M,
	                                                            S16_CBC_THRES_HIGH_SPEED,S8_CBC_ARAD_THRES_COMP_H);
	    
	    if(lccbcs16CbcAlat > 0)/*(RL.REAR_WHEEL==1)*/
	    {   
	        if(((vref-RL.vrad_crt)>=CBC_tempW2)&&(RL.rel_lam<=(int8_t)(-CBC_tempW3))&&(RL.arad<=(int8_t)(-CBC_tempW4)))
	        {
	            lccbcu1CbcSlipOK = 1;
	        }
	        else
	        {
	            lccbcu1CbcSlipOK = 0;
	        }
	    }
	    else if (lccbcs16CbcAlat < 0)/*(RR.REAR_WHEEL==1)*/
	    {
	        if(((vref-RR.vrad_crt)>=CBC_tempW2)&&(RR.rel_lam<=(int8_t)(-CBC_tempW3))&&(RR.arad<=(int8_t)(-CBC_tempW4)))
	        {
	            lccbcu1CbcSlipOK = 1;
	        }
	        else
	        {
	            lccbcu1CbcSlipOK = 0;
	        }
	    }
	    else{}
	}
  #else
    	lccbcu1CbcSlipOK = 1; /* Not consider slip condition in ABS-CBC ctrl */    
  #endif/*(__CBC_WHEEL_SLIP_CONDITION==ENABLE)*/        	


  #if __FRONT_CBC_ENABLE
    if(CBC_ENABLE == 0)
    {
    	lccbcu1CbcFrontDvOK = 0;
    }    
    else if(lccbcu8CbcMode != U8_ESC_CBC)
    {
    	lccbcu1CbcFrontDvOK = 0; /* Not consider slip condition in ABS-CBC ctrl */
    }   	
    else
    {    
    	lccbcs16DvFL = vref - FL.vrad_crt;
	    lccbcs16DvFR = vref - FR.vrad_crt; 
	    
	    if(lccbcs16DvFL<0) {lccbcs16DvFL=0;}
	    else{}
	    if(lccbcs16DvFR<0) {lccbcs16DvFR=0;}
	    else{}
	    
	    lccbcs16FrontDvEnter = LCESP_s16IInter3Point(vref, S16_CBC_THRES_LOW_SPEED ,S16_CBC_FRONT_ENTER_DV_SPD_S,
	                                                       S16_CBC_THRES_MED_SPEED ,S16_CBC_FRONT_ENTER_DV_SPD_M,
	                                                       S16_CBC_THRES_HIGH_SPEED,S16_CBC_FRONT_ENTER_DV_SPD_H);
	    
	    lccbcs16FrontDvExit  = LCESP_s16IInter3Point(vref, S16_CBC_THRES_LOW_SPEED ,S16_CBC_FRONT_EXIT_DV_SPD_S,
	                                                       S16_CBC_THRES_MED_SPEED ,S16_CBC_FRONT_EXIT_DV_SPD_M,
	                                                       S16_CBC_THRES_HIGH_SPEED,S16_CBC_FRONT_EXIT_DV_SPD_H);
	
		if (lccbcs16CbcAlat > 0)
		{	
	        if(lccbcs16DvFL >= lccbcs16FrontDvEnter)
	        { 
	            lccbcu1CbcFrontDvOK = 1;
	        }
	        else if(lccbcs16DvFL <= lccbcs16FrontDvExit)
	        {
	            lccbcu1CbcFrontDvOK = 0;
	        }
	        else { }
	            
		}
		else if (lccbcs16CbcAlat < 0)
		{
	        if(lccbcs16DvFR >= lccbcs16FrontDvEnter)
	        { 
	            lccbcu1CbcFrontDvOK = 1;
	        }
	        else if(lccbcs16DvFR <= lccbcs16FrontDvExit)
	        {
	            lccbcu1CbcFrontDvOK = 0;
	        }
	        else { }            
		}
		else { }
	}
  #endif /* __FRONT_CBC_ENABLE */



  /*------------------ rear slip control --------------------------------------------------------*/
    if(CBC_ENABLE == 0)
    {
    	lccbcu1UnstableRearSlip = 0;
    } 
    else
    {   
	    lccbcs16DvRL = vref - RL.vrad_crt;
	    lccbcs16DvRR = vref - RR.vrad_crt; 
	    
	    if(lccbcs16DvRL<0) {lccbcs16DvRL=0;}
	    else{}
	    if(lccbcs16DvRR<0) {lccbcs16DvRR=0;}
	    else{}
	
	      #if __VDC 
	    if(lccbcu8CbcMode == U8_ESC_CBC)
	    {
	 	    lccbcs16RearDvEnter = LCESP_s16IInter3Point(vref, S16_CBC_THRES_LOW_SPEED , S16_CBC_REAR_ENTER_DV_SPD_S,
		                                                      S16_CBC_THRES_MED_SPEED , S16_CBC_REAR_ENTER_DV_SPD_M,
		                                                      S16_CBC_THRES_HIGH_SPEED, S16_CBC_REAR_ENTER_DV_SPD_H);
		    lccbcs16RearDvExit  = S16_CBC_REAR_EXIT_DV;
		}
		else
	      #endif    
		{
		    lccbcs16RearDvEnter = LCESP_s16IInter3Point(vref, S16_CBC_THRES_LOW_SPEED ,S16_CBC_ABS_REAR_ENTER_DV_SPD_S,
		                                                      S16_CBC_THRES_MED_SPEED ,S16_CBC_ABS_REAR_ENTER_DV_SPD_M,
		                                                      S16_CBC_THRES_HIGH_SPEED,S16_CBC_ABS_REAR_ENTER_DV_SPD_H);
		    lccbcs16RearDvExit  = S16_CBC_ABS_REAR_EXIT_DV;
		}
	
	    if((lccbcu1UnstableRearSlip==0)&&
	    (((CBC_ACTIVE_rl==1)&&(lccbcs16DvRL>=lccbcs16RearDvEnter))||((CBC_ACTIVE_rr==1)&&(lccbcs16DvRR>=lccbcs16RearDvEnter))))
	    {
	        lccbcu1UnstableRearSlip = 1;    
	    }
	    else if((lccbcu1UnstableRearSlip==1)&&
	    (((CBC_ACTIVE_rl==1)&&(lccbcs16DvRL>=lccbcs16RearDvExit))||((CBC_ACTIVE_rr==1)&&(lccbcs16DvRR>=lccbcs16RearDvExit))))
	    {
	        lccbcu1UnstableRearSlip = 1;    
	    }
	    else
	    {
	        lccbcu1UnstableRearSlip = 0;    
	    }
	}
	
}

static void LCCBC_vDetectVSpeedCondition(void)
{
	if(CBC_ON==0)
	{
		if(vref > S16_CBC_VREF_ENTER_TH)
		{
			lccbcu1CbcSpeedOK = 1;
		}
		else
		{
			lccbcu1CbcSpeedOK = 0;
		}
	}
	else
	{
		if(vref > S16_CBC_VREF_EXIT_TH)
		{
			lccbcu1CbcSpeedOK = 1;
		}
		else
		{
			lccbcu1CbcSpeedOK = 0;
		}
	}
}


	
static void LCCBC_vDecideCBCControl(void)
{
	if((CBC_INHIBIT_ABS_FLAG==0) && (lccbcu1AbsCtrl==0) && (lccbcu1CbcSlipOK==1) &&
	   (DETECT_PARTIAL_BRAKING==1) && (DETECT_LIMIT_CORNER==1) && (lccbcu1CbcSpeedOK==1) &&
       #if __VDC 
       (((lccbcu8CbcMode==U8_ESC_CBC)&&(YAW_CDC_WORK==0)&&(BACK_DIR==0)&&(CBC_INHIBIT_ESC_FLAG==0)) ||
        ((lccbcu8CbcMode==U8_ABS_CBC)&&(Rough_road_detect_vehicle==0)&&(Rough_road_suspect_vehicle==0)))
       #else
        ((lccbcu8CbcMode==U8_ABS_CBC)&&(Rough_road_detect_vehicle==0)&&(Rough_road_suspect_vehicle==0))
       #endif
    )
    {
    	CBC_ENABLE = 1;
    }
    else
    {
    	CBC_ENABLE = 0;
    }

    
  #if __4WD_VARIANT_CODE==ENABLE     
   #if __4WD_ABS_CBC_ENABLE==DISABLE
    if((lsu8DrvMode!=DM_2WD)&&(lsu8DrvMode!=DM_2H)) /* ABS CBC is disable for 4WD */
    {
        CBC_ENABLE=0;
    }
    else 
   #endif 
    if(CAN_variant_err_flg==0) /* 0:Error, 1:Normal */
    {
        CBC_ENABLE=0;
    }
    else {} 
  #endif    

    if((CBC_ENABLE==1)&&(CBC_ENABLE_old==0))
    {
        CBC_FIRST_SCAN = 1;
    }
    else
    {
        CBC_FIRST_SCAN = 0;
    }
    
    if((lcabsu1BrakeByBLS==1)&&(CBC_ENABLE==0)&&(CBC_ENABLE_old==1))
    {
        lccbcu1ReCBCFlag = 1;
        lccbcu8ReCBCcnt  = 1;
    }
    else if(lcabsu1BrakeByBLS==0)
    {
        lccbcu1ReCBCFlag = 0;
        lccbcu8ReCBCcnt  = 0;
    }
    else 
    {
        if((lccbcu8ReCBCcnt<=U8_CBC_RECBC_CNT_MAX_TIME)&&(lccbcu1ReCBCFlag==1))
        {
            lccbcu8ReCBCcnt = lccbcu8ReCBCcnt + 1;
        }
        else{}
    }
    
    LCCBC_vDecideCBCControlWL(&lccbcRL);
    LCCBC_vDecideCBCControlWL(&lccbcRR);
  #if __FRONT_CBC_ENABLE
    LCCBC_vDecideCBCControlWL(&lccbcFL);
    LCCBC_vDecideCBCControlWL(&lccbcFR);
  #endif /* __FRONT_CBC_ENABLE */
}

static void LCCBC_vDecideCBCControlWL(CBC_WHL_CTRL_t *pCbcWL)
{
	if((CBC_ENABLE==1) && (lccbcu1AbsCtrl==0) && (pCbcWL->u1CbcAbsAct==0) && (pCbcWL->u1CbcEscAct==0))
	{
		if ((lccbcs16CbcAlat > 0) && (pCbcWL->u1CbcLeftWL==1))
		{	
			if(pCbcWL->u1CbcRearWL==1)
			{
				pCbcWL->u1CbcEnable = 1;
			}
		  #if __FRONT_CBC_ENABLE	
			else
			{
				if(lccbcu1CbcFrontDvOK==1)
	            { 
	                pCbcWL->u1CbcEnable = 1;
	            }
	            else
	            {
	                pCbcWL->u1CbcEnable = 0;
	            }
	        }
	      #endif /* __FRONT_CBC_ENABLE */    
		}
		else if((lccbcs16CbcAlat < 0) && (pCbcWL->u1CbcLeftWL==0))
		{
			if(pCbcWL->u1CbcRearWL==1)
			{
				pCbcWL->u1CbcEnable = 1;
			}
		  #if __FRONT_CBC_ENABLE
			else
			{
				if(lccbcu1CbcFrontDvOK==1)
	            { 
	                pCbcWL->u1CbcEnable = 1;
	            }
	            else
	            {
	                pCbcWL->u1CbcEnable = 0;
	            }
	        }
	      #endif /* __FRONT_CBC_ENABLE */  
		}
		else
		{
			pCbcWL->u1CbcEnable = 0;
		}	
	}
	else
	{
		pCbcWL->u1CbcEnable = 0;
	}
	if((lccbcu1AbsCtrl==1) || (pCbcWL->u1CbcAbsAct==1) || (pCbcWL->u1CbcEscAct==1)) /* ABS engaged CBC WL --> FADEOUT OFF */
	{
		pCbcWL->u1CbcFadeOut = 0;

	  #if defined(__CBC_DEBUG)	
		if(lccbcu1AbsCtrl==1)
		{
			pCbcWL->u8Debugger=1;
		}
		else if(pCbcWL->u1CbcEscAct==1)
		{
			pCbcWL->u8Debugger=2;
		}
		else{}
	  #endif	
	}
}

void LCCBC_vCallCalControlValue(void)
{
      #if __VDC	
    int16_t lccbcs16DeltaPresSpeedH,lccbcs16DeltaPresSpeedM,lccbcs16DeltaPresSpeedL;
      #endif
  
    if(lccbcu1UnstableRearSlip==1)
    {
	  #if __VDC 
	    if(lccbcu8CbcMode == U8_ESC_CBC)
	    {
	        CBC_tempW1 = LCESP_s16IInter3Point(vref, S16_CBC_THRES_LOW_SPEED ,S16_CBC_REAR_SLIP_DEL_P_SPD_S,
	                                                 S16_CBC_THRES_MED_SPEED ,S16_CBC_REAR_SLIP_DEL_P_SPD_M,
	                                                 S16_CBC_THRES_HIGH_SPEED,S16_CBC_REAR_SLIP_DEL_P_SPD_H);
		}
		else
	  #endif /* __VDC */ 
		{
	        CBC_tempW1 = LCESP_s16IInter3Point(vref, S16_CBC_THRES_LOW_SPEED ,S16_CBC_ABS_REAR_SLIP_DEL_P_SPD_S,
	                                                 S16_CBC_THRES_MED_SPEED ,S16_CBC_ABS_REAR_SLIP_DEL_P_SPD_M,
	                                                 S16_CBC_THRES_HIGH_SPEED,S16_CBC_ABS_REAR_SLIP_DEL_P_SPD_H);
		}
    }
    else
    {
        CBC_tempW1 = 0;    
    }

  /*------------------------------------------------------------------------------------------------*/
    if(CBC_ENABLE==0)
    {
        CBC_target_delta_p = 0;
        CBC_target_p = lccbcs16CbcMpress;
        dump_press_dead_p = 0;
		rise_press_dead_p = 0;
    }   
    else if (lccbcs16CbcMpress < U8_CBC_MIN_TARGET_P)
    {
        CBC_target_delta_p = lccbcs16CbcMpress;
        CBC_target_p = U8_CBC_MIN_TARGET_P;
        dump_press_dead_p = 0;
		rise_press_dead_p = 0;
    }
    else
    {
	  #if __VDC 
	    if(lccbcu8CbcMode == U8_ESC_CBC)
	    {
	        if  (det_alatm>S16_CBC_DELTA_PRESS_ALAT_LEVEL_H)      
	        {
	            lccbcs16DeltaPresSpeedL = S16_CBC_DELTA_PRESS_ALAT_H_Spd_S;
	            lccbcs16DeltaPresSpeedM = S16_CBC_DELTA_PRESS_ALAT_H_Spd_M;
	            lccbcs16DeltaPresSpeedH = S16_CBC_DELTA_PRESS_ALAT_H_Spd_H;
	        }
	        else if  (det_alatm>S16_CBC_DELTA_PRESS_ALAT_LEVEL_M) 
	        {
	            lccbcs16DeltaPresSpeedL = S16_CBC_DELTA_PRESS_ALAT_M_Spd_S;
	            lccbcs16DeltaPresSpeedM = S16_CBC_DELTA_PRESS_ALAT_M_Spd_M;
	            lccbcs16DeltaPresSpeedH = S16_CBC_DELTA_PRESS_ALAT_M_Spd_H;
	        }                     
	        else                          
	        {
	            lccbcs16DeltaPresSpeedL = S16_CBC_DELTA_PRESS_ALAT_S_Spd_S;
	            lccbcs16DeltaPresSpeedM = S16_CBC_DELTA_PRESS_ALAT_S_Spd_M;
	            lccbcs16DeltaPresSpeedH = S16_CBC_DELTA_PRESS_ALAT_S_Spd_H;
	        }
	        CBC_target_delta_p = LCESP_s16IInter3Point(vref, S16_CBC_THRES_LOW_SPEED ,lccbcs16DeltaPresSpeedL,
	                                                         S16_CBC_THRES_MED_SPEED ,lccbcs16DeltaPresSpeedM,
	                                                         S16_CBC_THRES_HIGH_SPEED,lccbcs16DeltaPresSpeedH);
	      #if (__CBC_TARGET_P_COMP_BY_DEL_YAW==ENABLE)  
	        if(delta_yaw > S16_CBC_COMP_MIN_DEL_YAW)
	        {
	            CBC_tempW0 = LCABS_s16Interpolation2P(delta_yaw, S16_CBC_COMP_MIN_DEL_YAW, S16_CBC_COMP_MAX_DEL_YAW, 
	                                                             S16_CBC_COMP_MIN_PRESS,   S16_CBC_COMP_MAX_PRESS);
	        }
	        else
	        {
	            CBC_tempW0 = 0;
	        }
	        CBC_target_delta_p = CBC_target_delta_p + CBC_tempW0;        
	      #endif	/* #if (__CBC_TARGET_P_COMP_BY_DEL_YAW==ENABLE) */
		}
		else
	  #endif /* __VDC */   
		{      
	        CBC_target_delta_p = LCESP_s16IInter3Point(vref, S16_CBC_THRES_LOW_SPEED ,S16_CBC_DELTA_PRESS_ABS_SPD_S,
	                                                         S16_CBC_THRES_MED_SPEED ,S16_CBC_DELTA_PRESS_ABS_SPD_M,
	                                                         S16_CBC_THRES_HIGH_SPEED,S16_CBC_DELTA_PRESS_ABS_SPD_H);
		}
		
	    if (lccbcs16CbcAlat > 0)
	    {
	        CBC_tempW9 = lccbcs16WPressRR;
	    }
	    else
	    {
	        CBC_tempW9 = lccbcs16WPressRL;
	    }
	
	    CBC_tempW8 = LCESP_s16IInter2Point(vref, VREF_40_KPH,MPRESS_5BAR,
	                                             VREF_60_KPH,MPRESS_10BAR);
	
	    if(((CBC_ENABLE_rl==1)&&(EBD_Dump_chk_rl==1))||((CBC_ENABLE_rr==1)&&(EBD_Dump_chk_rr==1)))
	    {
	        CBC_target_p = CBC_tempW9 - CBC_target_delta_p - CBC_tempW8 - CBC_tempW1;
	    }
	    else
	    {
	        CBC_target_p = CBC_tempW9 - CBC_target_delta_p - CBC_tempW1;
	    }
	
	    if (CBC_target_p<U8_CBC_MIN_TARGET_P)   {CBC_target_p = U8_CBC_MIN_TARGET_P;}
	    else{}
	    if (CBC_target_p>CBC_tempW9)    {CBC_target_p = CBC_tempW9;}
	    else{}
		
    }
 

  /*------------------------------------------------------------------------------------------------*/
      
  #if __VDC && __FRONT_CBC_ENABLE 
    if((CBC_ENABLE==0) || (lccbcu8CbcMode != U8_ESC_CBC))
    {
        CBC_target_delta_p_front = 0;
        CBC_target_p_front = lccbcs16CbcMpress;
        press_dead_p_front = 0;
    }   
    else if (mpress < U8_CBC_MIN_TARGET_P)
    {
        CBC_target_delta_p_front = lccbcs16CbcMpress;
        CBC_target_p_front = U8_CBC_MIN_TARGET_P;
        press_dead_p_front = 0;
    }
    else
    {           
        if (lccbcs16CbcAlat > 0)
        {
            CBC_tempW7 = lccbcs16DvFL;
        }
        else
        {
            CBC_tempW7 = lccbcs16DvFR;
        }
        
        CBC_tempW6 = LCESP_s16IInter3Point(vref, S16_CBC_THRES_LOW_SPEED ,S16_CBC_FRONT_MEDIUM_DV_SPD_S,
                                                 S16_CBC_THRES_MED_SPEED ,S16_CBC_FRONT_MEDIUM_DV_SPD_M,
                                                 S16_CBC_THRES_HIGH_SPEED,S16_CBC_FRONT_MEDIUM_DV_SPD_H);
        
        if((CBC_ENABLE_fl==0)&&(CBC_ENABLE_fr==0))
        {
            CBC_target_delta_p_front = 0;
        }
        else if(CBC_tempW7 < CBC_tempW6)
        {
            CBC_target_delta_p_front = LCESP_s16IInter3Point(vref, S16_CBC_THRES_LOW_SPEED ,S16_CBC_FRONT_DEL_PRESS_S_SPD_S,
                                                                   S16_CBC_THRES_MED_SPEED ,S16_CBC_FRONT_DEL_PRESS_S_SPD_M,
                                                                   S16_CBC_THRES_HIGH_SPEED,S16_CBC_FRONT_DEL_PRESS_S_SPD_H);
        }
        else
        {
            CBC_target_delta_p_front = LCESP_s16IInter3Point(vref, S16_CBC_THRES_LOW_SPEED ,S16_CBC_FRONT_DEL_PRESS_H_SPD_S,
                                                                   S16_CBC_THRES_MED_SPEED ,S16_CBC_FRONT_DEL_PRESS_H_SPD_M,
                                                                   S16_CBC_THRES_HIGH_SPEED,S16_CBC_FRONT_DEL_PRESS_H_SPD_H);
        }    
        
	    if (lccbcs16CbcAlat > 0)
	    {
	        CBC_tempW5 = lccbcs16WPressFR;
	    }
	    else
	    {
	        CBC_tempW5 = lccbcs16WPressFL;
	    }
	    
	    CBC_target_p_front = CBC_tempW5 - CBC_target_delta_p_front;
	
	    if (CBC_target_p_front<U8_CBC_MIN_TARGET_P)   {CBC_target_p_front = U8_CBC_MIN_TARGET_P;}
	    else{}
	    if (CBC_target_p_front>CBC_tempW5)    {CBC_target_p_front = CBC_tempW5;}
	    else{}
	
	    press_dead_p_front=U8_CBC_DEAD_PRESS_FRONT;
    }


    
  #endif /* __VDC && __FRONT_CBC_ENABLE */
}

void LCCBC_vCallCalControlPressure(void)
{

    if (CBC_ENABLE_rl==1)   {CBC_ACTIVE_rl =1;}
    else                    {CBC_ACTIVE_rl =0;}

    if (CBC_ENABLE_rr==1)   {CBC_ACTIVE_rr =1;}
    else                    {CBC_ACTIVE_rr =0;}

    CBC_tempW9 = 0;
    if (CBC_ENABLE_rl==1)
    {
    	CBC_tempW9 = lccbcs16WPressRL - CBC_target_p;
    	lccbcs16ReardeltaP = lccbcs16CbcMpress - lccbcs16WPressRL; /* mcp - ctrl wl p : LFC ref press */
    }
    else{}
    if (CBC_ENABLE_rr==1)
    {
    	CBC_tempW9 = lccbcs16WPressRR - CBC_target_p;
    	lccbcs16ReardeltaP = lccbcs16CbcMpress - lccbcs16WPressRR; /* mcp - ctrl wl p : LFC ref press */
    }
    else{}

  #if __VDC 
    if(lccbcu8CbcMode == U8_ESC_CBC)
    {
	    lccbcs8RiseHoldMax = LCESP_s16IInter2Point (McrAbs(CBC_tempW9), U8_CBC_MIN_TARGET_P,(int16_t)U8_CBC_MAX_HOLD_TIMER,
	                                                            S16_CBC_DELTA_PRES_MAX_RISE,0);

        if(CBC_target_p <= U8_CBC_MIN_TARGET_P)
        {
            lccbcs8DumpHoldMax = 0;
        }
        else
        {
            lccbcs8DumpHoldMax = LCESP_s16IInter2Point (McrAbs(CBC_tempW9), U8_CBC_MIN_TARGET_P,(int16_t)U8_CBC_MAX_HOLD_TIMER,
	                                                                S16_CBC_DELTA_PRES_MAX_DUMP,0);
        }
	}
	else
  #endif /* __VDC */ 
	{
	    lccbcs8RiseHoldMax = LCESP_s16IInter2Point (McrAbs(CBC_tempW9), U8_CBC_MIN_TARGET_P,(int16_t)U8_CBC_MAX_HOLD_TIMER,
	                                                        S16_CBC_DELTA_PRES_MAX_RISE_ABS,0);
	        
	    if(CBC_target_p <= U8_CBC_MIN_TARGET_P)
        {
            lccbcs8DumpHoldMax = 0;
        }
        else
        {
    	    lccbcs8DumpHoldMax = LCESP_s16IInter2Point (McrAbs(CBC_tempW9), U8_CBC_MIN_TARGET_P,(int16_t)U8_CBC_MAX_HOLD_TIMER,
	                                                            S16_CBC_DELTA_PRES_MAX_DUMP_ABS,0);
        }
	}    

    if ((lccbcs8RiseHoldCnt>=((int8_t)lccbcs8RiseHoldMax))||(CBC_ENABLE==0))
    {
        lccbcs8RiseHoldCnt=0;
    }
    else
    {
        lccbcs8RiseHoldCnt = lccbcs8RiseHoldCnt + 1;
    }

    if ((lccbcs8DumpHoldCnt>=((int8_t)lccbcs8DumpHoldMax))||(CBC_ENABLE==0))
    {
        lccbcs8DumpHoldCnt=0;
    }
    else
    {
        lccbcs8DumpHoldCnt = lccbcs8DumpHoldCnt + 1;
    }
 
    if ((lccbcs16WPressRL > (CBC_target_p + dump_press_dead_p))&&(CBC_ACTIVE_rl==1))
    {
        lccbcu8PresCtrlModeRL = U8_CBC_PRESS_DUMP;
    }
    else if ((lccbcs16WPressRL < (CBC_target_p - rise_press_dead_p))&&(CBC_ACTIVE_rl==1))
    {
        lccbcu8PresCtrlModeRL = U8_CBC_PRESS_RISE;
    }
    else
    {
        lccbcu8PresCtrlModeRL = U8_CBC_PRESS_HOLD;
    }

    if ((lccbcs16WPressRR > (CBC_target_p + dump_press_dead_p))&&(CBC_ACTIVE_rr==1))
    {
        lccbcu8PresCtrlModeRR = U8_CBC_PRESS_DUMP;
    }
    else if ((lccbcs16WPressRR < (CBC_target_p - rise_press_dead_p))&&(CBC_ACTIVE_rr==1))
    {
        lccbcu8PresCtrlModeRR = U8_CBC_PRESS_RISE;
    }
    else
    {
        lccbcu8PresCtrlModeRR = U8_CBC_PRESS_HOLD;
    }
    
  /*-------------- Front Control ---------------------------------------------------------*/
  #if __FRONT_CBC_ENABLE
    
    if (CBC_ENABLE_fl==1)   {CBC_ACTIVE_fl =1;}
    else                    {CBC_ACTIVE_fl =0;}

    if (CBC_ENABLE_fr==1)   {CBC_ACTIVE_fr =1;}
    else                    {CBC_ACTIVE_fr =0;}

    CBC_tempW5 = 0;
    if (CBC_ENABLE_fl==1)
	{
    	CBC_tempW5 = lccbcs16WPressFL - CBC_target_p_front;
    	lccbcs16FrontdeltaP = lccbcs16CbcMpress - lccbcs16WPressFL;
    }
    else{}
    if (CBC_ENABLE_fr==1)
	{
    	CBC_tempW5 = lccbcs16WPressFR - CBC_target_p_front;
    	lccbcs16FrontdeltaP = lccbcs16CbcMpress - lccbcs16WPressFR;
    }
    else{}

    lccbcs8RiseHoldMaxFront = LCESP_s16IInter2Point (McrAbs(CBC_tempW5), U8_CBC_MIN_TARGET_P,(int16_t)U8_CBC_MAX_HOLD_TIMER,
                                                               S16_CBC_DELTA_PRES_MAX_RISE_F,0);

    if(CBC_target_p_front <= U8_CBC_MIN_TARGET_P)
    {
        lccbcs8DumpHoldMaxFront = 0;
    }
    else
    {
        lccbcs8DumpHoldMaxFront = LCESP_s16IInter2Point (McrAbs(CBC_tempW5), U8_CBC_MIN_TARGET_P,(int16_t)U8_CBC_MAX_HOLD_TIMER,
                                                                   S16_CBC_DELTA_PRES_MAX_DUMP_F,0);
    }

    if ((lccbcs8RiseHoldCntFront>=((int8_t)lccbcs8RiseHoldMaxFront))||(CBC_ENABLE==0))
    {
        lccbcs8RiseHoldCntFront=0;
    }
    else
    {
        lccbcs8RiseHoldCntFront = lccbcs8RiseHoldCntFront + 1;
    }

    if ((lccbcs8DumpHoldCntFront>=((int8_t)lccbcs8DumpHoldMaxFront))||(CBC_ENABLE==0))
    {
        lccbcs8DumpHoldCntFront=0;
    }
    else
    {
        lccbcs8DumpHoldCntFront = lccbcs8DumpHoldCntFront + 1;
    }

    if ((lccbcs16WPressFL > (CBC_target_p_front + press_dead_p_front))&&(CBC_ACTIVE_fl==1))
    {
        lccbcu8PresCtrlModeFL = U8_CBC_PRESS_DUMP;
    }
    else if ((lccbcs16WPressFL < (CBC_target_p_front - press_dead_p_front))&&(CBC_ACTIVE_fl==1))
    {
        lccbcu8PresCtrlModeFL = U8_CBC_PRESS_RISE;
    }
    else
    {
        lccbcu8PresCtrlModeFL = U8_CBC_PRESS_HOLD;
    }

    if ((lccbcs16WPressFR > (CBC_target_p_front + press_dead_p_front))&&(CBC_ACTIVE_fr==1))
    {
        lccbcu8PresCtrlModeFR = U8_CBC_PRESS_DUMP;
    }
    else if ((lccbcs16WPressFR < (CBC_target_p_front - press_dead_p_front))&&(CBC_ACTIVE_fr==1))
    {
        lccbcu8PresCtrlModeFR = U8_CBC_PRESS_RISE;
    }
    else
    {
        lccbcu8PresCtrlModeFR = U8_CBC_PRESS_HOLD;
    }
  #endif	/* __FRONT_CBC_ENABLE */  
}

void LCCBC_vCallClearValveMotor(void)
{
  #if __FRONT_CBC_ENABLE
    lccbcu8PresCtrlModeFL = U8_CBC_PRESS_RESET;
    lccbcu8PresCtrlModeFR = U8_CBC_PRESS_RESET;
  #endif /*__FRONT_CBC_ENABLE*/
    lccbcu8PresCtrlModeRL = U8_CBC_PRESS_RESET;
    lccbcu8PresCtrlModeRR = U8_CBC_PRESS_RESET;
}

void LCCBC_vResetControl(void)
{
  #if __FRONT_CBC_ENABLE
    CBC_ACTIVE_fl = 0;
    CBC_ACTIVE_fr = 0;
  #endif /*__FRONT_CBC_ENABLE*/
    CBC_ACTIVE_rl = 0;
    CBC_ACTIVE_rr = 0;
    CBC_ENABLE    = 0;
    CBC_ON        = 0;
}

void LCCBC_vCallControlFadeOut(void)
{
  #if __VDC
    if(lccbcu8CbcMode == U8_ESC_CBC)
    {	
    	lccbcs16FadeOutMaxTime = S16_CBC_FADE_OUT_TIME;
    }
    else
  #endif
	{
    	lccbcs16FadeOutMaxTime = S16_CBC_FADE_OUT_TIME_ABS;
    }
  
	LCCBC_vCallControlFadeOutWL(&lccbcRL, &lccbcRR);
	LCCBC_vCallControlFadeOutWL(&lccbcRR, &lccbcRL);

  #if __FRONT_CBC_ENABLE  
	LCCBC_vCallControlFadeOutWL(&lccbcFL, &lccbcFR);
	LCCBC_vCallControlFadeOutWL(&lccbcFR, &lccbcFL);
  #endif	/* __FRONT_CBC_ENABLE */   
}

static void LCCBC_vCallControlFadeOutWL(CBC_WHL_CTRL_t *pCbcWL, CBC_WHL_CTRL_t *pCbcCountWL)
{
	if(pCbcWL->u1CbcFadeOut == 0)
	{
		if(pCbcWL->u1CbcActive == 1) {pCbcWL->u1CbcFadeOut = 1;}
		else{}
	}
	else
	{	
	    if(pCbcWL->u1CbcActive == 1)
	    {/* It's not Fadeout, consider Re-enter in Fadeout */
			pCbcWL->s16FadeOutCnt = CBC_FADEOUT_OFF;
	    }
	    else
	    {
	        pCbcWL->s16FadeOutCnt = pCbcWL->s16FadeOutCnt + 1;
	        
		    if ((pCbcWL->s16FadeOutCnt > lccbcs16FadeOutMaxTime) || (pCbcWL->s16CbcWPress >= (pCbcCountWL->s16CbcWPress-rise_press_dead_p))
		     ||((pCbcWL->s16FadeOutCnt > 0) && (pCbcWL->u1CbcEbdDump == 1)) ||(pCbcCountWL->u1CbcEnable==1)
		    )
		    {	
		    	if(pCbcWL->u1CbcRearWL==1)
		    	{
		    		if(pCbcWL->u1CbcLeftWL==1)
			    	{	
			    		LCCBC_vResetFadeOut(&lccbcRL);	
			    	}
			    	else
			    	{	
			    		LCCBC_vResetFadeOut(&lccbcRR);
			    	}
		    	}
		      #if __FRONT_CBC_ENABLE   
		    	else
		    	{
		    		if(pCbcWL->u1CbcLeftWL==1)
			    	{
						LCCBC_vResetFadeOut(&lccbcFL);
					}
					else
					{
						LCCBC_vResetFadeOut(&lccbcFR);
					}
				}
			  #endif	/* __FRONT_CBC_ENABLE */
		    }
		    else
		    {
		    	pCbcWL->u8PresCtrlMode = U8_CBC_PRESS_FADEOUT;
		    	pCbcWL->s8FadeOutMode  = LCCBC_s8CallControlFOModeWL(pCbcWL);
		    }
		    
		  #if defined(__CBC_DEBUG)    
		    if(pCbcWL->s16FadeOutCnt > lccbcs16FadeOutMaxTime)
		    {
		    	pCbcWL->u8Debugger=3;
		    }
		    else if(pCbcWL->s16CbcWPress >= pCbcCountWL->s16CbcWPress)
		    {
		    	pCbcWL->u8Debugger=4;	
		    }
		    else if((pCbcWL->s16FadeOutCnt > 0) && (pCbcWL->u1CbcEbdDump == 1))
		    {
		    	pCbcWL->u8Debugger=5;
		    }
		    else{}
		  #endif	/* #if defined(__CBC_DEBUG) */
	    }	    
	}
}

int8_t LCCBC_s8CallControlFOModeWL(CBC_WHL_CTRL_t *pCbcWL)
{
	switch	(pCbcWL->s8FadeOutMode)
	{
		case CBC_FADEOUT_OFF:
			if((pCbcWL->u1CbcFadeOut == 0) || (pCbcWL->u1CbcActive == 1))
            {
                ;
            } 
          /*#if __VDC  
            else if((lccbcu8CbcMode == U8_ESC_CBC) && (lccbcu1AbsCtrl==1))
          #else
            else if(lccbcu1AbsCtrl==1)
          #endif	comment : (lccbcu1AbsCtrl==1)-> CBC OFF, (lccbcu1InsideAbsCtrl==1)-> pulse up*/
            else if(lccbcu1InsideAbsCtrl==1)
	        {
				pCbcWL->s8FadeOutMode=CBC_FADEOUT_PULSEUP;
	        }
          #if __VDC && __CBC_FADE_OUT_LFC_RISE
	        else if((lccbcu8CbcMode == U8_ESC_CBC) && (lccbcs16CbcMpress>=S16_CBC_BRAKING_MIN_THRES) && (CBC_INHIBIT_ESC_FLAG==0))
	        {
				pCbcWL->s8FadeOutMode=CBC_FADEOUT_LFCRISE;
	        }
          #endif
	        else
	        {
				pCbcWL->s8FadeOutMode=CBC_FADEOUT_FULLRISE;
	        }
			break;

		case CBC_FADEOUT_PULSEUP:
		  #if (__CBC_FADE_OUT_OFF_MODE==ENABLE)
			if((pCbcWL->u1CbcFadeOut == 0) || (pCbcWL->u1CbcActive == 1))
            {
                pCbcWL->s8FadeOutMode=CBC_FADEOUT_OFF;
            } 
			else if(pCbcWL->s8PressRiseCnt>U8_CBC_Limit_Cnt_Pulseup)
			{
				pCbcWL->s8FadeOutMode=CBC_FADEOUT_OFF;	
			}
		  #endif
            else if(ABS_fz==0)
			{
				pCbcWL->s8FadeOutMode=CBC_FADEOUT_FULLRISE;
			}
			else{}			
			break;

		case CBC_FADEOUT_FULLRISE:
		  #if (__CBC_FADE_OUT_OFF_MODE==ENABLE)
			if((pCbcWL->u1CbcFadeOut == 0) || (pCbcWL->u1CbcActive == 1))
            {
                pCbcWL->s8FadeOutMode=CBC_FADEOUT_OFF;
            } 
			else if((ABS_fz==1)||(pCbcWL->s16FadeOutCnt>U8_CBC_Limit_Time_Full_rise))
			{
				pCbcWL->s8FadeOutMode=CBC_FADEOUT_OFF;
			}
		  #endif
			else{}
			break;

      #if __VDC && __CBC_FADE_OUT_LFC_RISE
		case CBC_FADEOUT_LFCRISE:
		  #if (__CBC_FADE_OUT_OFF_MODE==ENABLE)
			if((lccbcu1AbsCtrl==1)||(pCbcWL->u8CbcPwmDuty<U8_CBC_Limit_LFC_Duty))
			{
				pCbcWL->s8FadeOutMode=CBC_FADEOUT_OFF;
			}
			else
		  #endif
			if((CBC_INHIBIT_ESC_FLAG==1)||(lccbcu8CbcMode == U8_ABS_CBC))
			{
				pCbcWL->s8FadeOutMode=CBC_FADEOUT_FULLRISE;
			}
			else{}
			break;
	  #endif

		default :
			pCbcWL->s8FadeOutMode=CBC_FADEOUT_OFF;				
			break;		
	}
    return pCbcWL->s8FadeOutMode;
}

static void LCCBC_vResetFadeOut(CBC_WHL_CTRL_t *pCbcWL)
{
	pCbcWL->u8PresCtrlMode   = U8_CBC_PRESS_RESET;
	pCbcWL->u1CbcFadeOut     = CBC_FADEOUT_OFF; /* Fadeout END */
	pCbcWL->s8FadeOutMode    = CBC_FADEOUT_OFF; /* Fadeout Mode reset */
	pCbcWL->s16FadeOutCnt    = CBC_FADEOUT_OFF; 
	pCbcWL->u8CbcPwmDuty     = CBC_FADEOUT_OFF;
	pCbcWL->s8PressRiseCnt   = CBC_FADEOUT_OFF;
}

#if (__ABS_CBC_DISABLE==0)
void LDCBC_vEstVehicleLateralG(void)
{
  
  /*-----------------------------------------------------------*/
  /*                vrad_out - vrad_in     1000                */
  /*    Yaw-Rate = -------------------- * ------- (rad/s)      */
  /*                   Track_Width         8*3.6               */
  /*                                                           */
  /*    if Beta-dot==0                                         */  
  /*                                1000                       */
  /*    Ay = Yaw-Rate * vref * ------------ (g*1000)           */
  /*                             8*3.6*9.81                    */
  /*-----------------------------------------------------------*/
  
    CBC_tempW1 = vref;
  #if __REAR_D
    CBC_tempW0 = TRACK_WIDTH_F;
    CBC_tempW5 = TRACK_WIDTH_R;
    CBC_tempW6 = RR.vrad_crt - RL.vrad_crt;
  #else
    CBC_tempW0 = TRACK_WIDTH_R;
    CBC_tempW5 = TRACK_WIDTH_F;
    CBC_tempW6 = FR.vrad_crt - FL.vrad_crt;
  #endif  
    
    CBC_tempW3 = LCESP_s16IInter3Point (vref, S16_CBC_AY_EST_REF_SPD_S,(int16_t)U8_CBC_AY_EST_GAIN_SPD_S,
                                              S16_CBC_AY_EST_REF_SPD_M,(int16_t)U8_CBC_AY_EST_GAIN_SPD_M,
                                              S16_CBC_AY_EST_REF_SPD_H,(int16_t)U8_CBC_AY_EST_GAIN_SPD_H);
                                                 
    CBC_tempW2 = (int16_t)((int32_t)123*CBC_tempW3*(vref_R - vref_L)/100);                                          
    
    lccbcs16EstAyRaw = (int16_t)((int32_t)CBC_tempW2*CBC_tempW1/CBC_tempW0);
    
  /*-----------------------------------------------------------*/  
    
    if(CBC_tempW6>VREF_10_KPH)
    {
        CBC_tempW6 = VREF_10_KPH;
    }
    else if(CBC_tempW6<-VREF_10_KPH)
    {
        CBC_tempW6 = -VREF_10_KPH;
    }
    else{}
    
    CBC_tempW7 = (int16_t)((int32_t)123*CBC_tempW3*CBC_tempW6/100);                                          
    
    lccbcs16EstAy2Raw = (int16_t)((int32_t)CBC_tempW7*CBC_tempW1/CBC_tempW5);
  
  /*-------------- 1st Order Filter ---------------------------*/
    
    if((Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1))
    {
        CBC_tempW3 = L_U8FILTER_GAIN_10MSLOOP_1HZ; /* 1Hz */
    }
    else if(Rough_road_detect_vehicle_EBD==1)
    {
        CBC_tempW3 = L_U8FILTER_GAIN_10MSLOOP_2HZ; /* 2Hz */
    }
    else
    {
        CBC_tempW3 = L_U8FILTER_GAIN_10MSLOOP_3HZ; /* 3Hz */
    }
    
    lccbcs16EstAy = LCESP_s16Lpf1Int(lccbcs16EstAyRaw,lccbcs16EstAyOld,(uint8_t)CBC_tempW3);
    
    if(lccbcs16EstAy > LAT_1G5G)        {lccbcs16EstAy =  LAT_1G5G;} /* +1.5g */
    else if(lccbcs16EstAy < -LAT_1G5G)  {lccbcs16EstAy = -LAT_1G5G;} /* -1.5g */
    else{}
  
    lccbcs16EstAyOld = lccbcs16EstAy;
    
  /*-------------- AyE by using driven wheel's speed -------------------------------*/  
    
    if((Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1)||(Rough_road_detect_vehicle_EBD==1))
    {
        CBC_tempW3 = L_U8FILTER_GAIN_10MSLOOP_1HZ; /* 1Hz */
    }
    else
    {
        CBC_tempW3 = L_U8FILTER_GAIN_10MSLOOP_1HZ; /* 1Hz */
    }
    
    lccbcs16EstAy2 = LCESP_s16Lpf1Int(lccbcs16EstAy2Raw,lccbcs16EstAy2Old,(uint8_t)CBC_tempW3);
    
    if(lccbcs16EstAy2 > LAT_1G5G)        {lccbcs16EstAy2 =  LAT_1G5G;} /* +1.5g */
    else if(lccbcs16EstAy2 < -LAT_1G5G)  {lccbcs16EstAy2 = -LAT_1G5G;} /* -1.5g */
    else{}
    
    lccbcs16EstAy2Old = lccbcs16EstAy2;
  
  /*-------------- Reliability Check for estimated Ay -------------------------------*/
    
    CBC_tempW8 = McrAbs(lccbcs16EstAy - lccbcs16EstAy2);
    CBC_tempW9 = McrAbs(FL.vrad_crt - RL.vrad_crt);
    CBC_tempW7 = McrAbs(FR.vrad_crt - RR.vrad_crt);
    
    if((BLS==0)&&(ABS_fz==0)&&(EBD_RA==0)&&(vref>=VREF_40_KPH)&&(vref<=VREF_200_KPH)
      #if (__CONSIDER_BLS_FAULT == ENABLE)
       &&(lcabsu1FaultBLS==0)
      #endif
       &&(Rough_road_detect_vehicle==0)&&(Rough_road_suspect_vehicle==0)
       &&(lccbcs16EstAx<S16_CBC_STEADY_MAX_ACCEL)&&(lccbcs16EstAx>-S16_CBC_STEADY_MAX_DECEL)
       &&(CBC_tempW9<=(int16_t)S8_CBC_WHEEL_SPIN_OK_LEVEL)&&(CBC_tempW7<=(int16_t)S8_CBC_WHEEL_SPIN_OK_LEVEL))
    {
        lccbcu1VehSteadyState = 1;
        
        if((vref>=VREF_80_KPH)&&(lccbcs16AyOKCounter<=L_U16_TIME_10MSLOOP_60S))
        {
            lccbcs16AyOKCounter+=2;
        }
        else if(lccbcs16AyOKCounter<=L_U16_TIME_10MSLOOP_60S)
        {
            lccbcs16AyOKCounter+=1;
        }
        else{}
    }
    else
    {
        lccbcu1VehSteadyState = 0;
    }
    
    if(lccbcs16AyOKCounter>=S16_CBC_AY_OK_CHECK_TIME)
    {
        lccbcu1AyCheckOK = 1;
    }
    else
    {
        lccbcu1AyCheckOK = 0;
    }
    
    CBC_tempW1 = S16_CBC_AY_ERROR_SUS_LEVEL;
    CBC_tempW2 = S16_CBC_AY_ERROR_DET_LEVEL;
    
    if( CBC_tempW1 < LAT_0G1G )
    {
        CBC_tempW1 = LAT_0G1G;
    }
    else{}
    
    if( CBC_tempW2 < LAT_0G2G )
    {
        CBC_tempW2 = LAT_0G2G;
    }
    else{}
    
    if( lccbcu1VehSteadyState==1 )
    {   
        if( CBC_tempW8 >= (CBC_tempW1+LAT_0G1G))
        {
            lccbcs16AyCheckCounter+=2;
        }
        else if( CBC_tempW8 >= CBC_tempW1 )
        {
            lccbcs16AyCheckCounter+=1;
        }
        else if(( CBC_tempW8 < LAT_0G05G )&&( lccbcs16AyCheckCounter > 1 ))
        {
            lccbcs16AyCheckCounter-=2;
        }
        else if(( CBC_tempW8 < LAT_0G1G )&&( lccbcs16AyCheckCounter > 0 ))
        {
            lccbcs16AyCheckCounter-=1;
        }
        else{}
        
        if( CBC_tempW8 >= (CBC_tempW2+LAT_0G1G))
        {
            lccbcs16AyCheckCounter2+=2;
        }
        else if( CBC_tempW8 >= CBC_tempW2 )
        {
            lccbcs16AyCheckCounter2+=1;
        }
        else if(( CBC_tempW8 < LAT_0G1G )&&( lccbcs16AyCheckCounter2 > 1 ))
        {
            lccbcs16AyCheckCounter2-=2;
        }
        else if(( CBC_tempW8 < LAT_0G2G )&&( lccbcs16AyCheckCounter2 > 0 ))
        {
            lccbcs16AyCheckCounter2-=1;
        }
        else{} 
    }
    else{}
    
    CBC_tempW4 = S16_CBC_AY_ERROR_CHECK_TIME;
    
    if( CBC_tempW4 < L_U16_TIME_10MSLOOP_10S)
    {
        CBC_tempW4 = L_U16_TIME_10MSLOOP_10S;
    }
    else{}
    
    if( lccbcs16AyCheckCounter >= (CBC_tempW4+L_U16_TIME_10MSLOOP_10S))
    {
        lccbcs16AyCheckCounter =  (CBC_tempW4+L_U16_TIME_10MSLOOP_10S);
    }
    else{}
    
    if((lccbcs16AyCheckCounter >= CBC_tempW4)&&(lccbcu1AyEstErrorSus==0))
    {
        lccbcu1AyEstErrorSus = 1;
    }
    else if((lccbcs16AyCheckCounter >= (CBC_tempW4-L_U16_TIME_10MSLOOP_5S))&&(lccbcu1AyEstErrorSus==1))
    {
        lccbcu1AyEstErrorSus = 1;
    }
    else
    {
        lccbcu1AyEstErrorSus = 0;
    }
    
    if( lccbcs16AyCheckCounter2 >= (CBC_tempW4+L_U16_TIME_10MSLOOP_10S))
    {
        lccbcs16AyCheckCounter2 =  (CBC_tempW4+L_U16_TIME_10MSLOOP_10S);
    }
    else{}
    
    if((lccbcs16AyCheckCounter2 >= CBC_tempW4)&&(lccbcu1AyEstErrorDet==0))
    {
        lccbcu1AyEstErrorDet = 1;
    }
    else if((lccbcs16AyCheckCounter2 >= (CBC_tempW4-L_U16_TIME_10MSLOOP_5S))&&(lccbcu1AyEstErrorDet==1))
    {
        lccbcu1AyEstErrorDet = 1;
    }
    else
    {
        lccbcu1AyEstErrorDet = 0;
    }
    
    if((lccbcu1AyEstErrorDet==0) && (lccbcu1AyCheckOK==1))
    {
        ldcbcu1AyEstValid = 1;
    }
    else
    {
        ldcbcu1AyEstValid = 0;
    }
}

void LDCBC_vEstVehicleLongitudinalG(void)
{
  
  /*------------------------------------------------------------------------------*/
  /*           vref - vref_alt                vref - vref_alt                     */
  /*    Ax =  -------------------- * 1000  = ----------------- * 1000  (g*1000)   */
  /*           8*3.6*9.81*0.007                    2                              */
  /*------------------------------------------------------------------------------*/
    
    lccbcs16EstVref = (vref_L+vref_R)/2;
    
    if((lccbcs16EstVref - lccbcs16EstVrefOld)>VREF_5_KPH)       {CBC_tempW2 =  VREF_5_KPH;} /* MGH-60:+20g,  MGH-80:+14g*/
    else if((lccbcs16EstVref - lccbcs16EstVrefOld)<-VREF_5_KPH) {CBC_tempW2 = -VREF_5_KPH;} /* MGH-60:-20g,  MGH-80:-14g*/
    else {CBC_tempW2 = (lccbcs16EstVref - lccbcs16EstVrefOld);}
  
    lccbcs16EstAxRaw = CBC_tempW2*S16_CBC_DV_AX_CONV_RATIO;
    
  /*-------------- 1st Order Filter ---------------------------*/
    
    if((Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1)||(Rough_road_detect_vehicle_EBD==1))
    {
        CBC_tempW3 = L_U8FILTER_GAIN_10MSLOOP_0_5HZ; /* 0.5Hz */
    }
    else
    {
        CBC_tempW3 = L_U8FILTER_GAIN_10MSLOOP_1HZ; /* 1Hz */
    }
    
    lccbcs16EstAx = LCESP_s16Lpf1Int(lccbcs16EstAxRaw,lccbcs16EstAxOld,(uint8_t)CBC_tempW3);
    
    if(lccbcs16EstAx > 1500)        {lccbcs16EstAx =  1500;} /* +1.5g */
    else if(lccbcs16EstAx < -1500)  {lccbcs16EstAx = -1500;} /* -1.5g */
    else{}
    
    lccbcs16EstAxOld = lccbcs16EstAx;
    lccbcs16EstVrefOld = lccbcs16EstVref;
}

void LDCBC_vEstVehicleMpress(void)
{
  
  /*-----------------------------------------------------------------------------------*/
  /*                      Ax                                                           */
  /*     Mpress = ------------------------    (bar*10)                                 */
  /*               U8_CBC_AX_MPRES_FACTOR                                              */
  /*                                                                                   */
  /*     Estimated Mpress is available in case of partial brake before ABS activation  */       
  /*-----------------------------------------------------------------------------------*/
  
    if(lccbcs16EstAx>=0)          {CBC_tempW0 = 0;}
    else if(lccbcs16EstAx<-1500)  {CBC_tempW0 = 1500;}  /* -1.5g limit  */
    else                          {CBC_tempW0 = -lccbcs16EstAx;}
  
    if(((lcabsu1BrakeByBLS==1)&&(lccbcs16EstAx<-S16_CBC_BRAKING_AX_THRES_BLS))
      ||(lccbcs16EstAx<-S16_CBC_BRAKING_AX_THRES_NO_BLS)) 
    {
        lccbcs16EstMpressRaw = CBC_tempW0*10/((int16_t)U8_CBC_AX_MPRES_FACTOR);
    }
    else 
    {
        lccbcs16EstMpressRaw = 0;
    }
  
  /*-------------- 1st Order Filter ---------------------------*/
    
    if((Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1)||(Rough_road_detect_vehicle_EBD==1))
    {
        CBC_tempW3 = L_U8FILTER_GAIN_10MSLOOP_2HZ; /* 2Hz */
    }
    else
    {
        CBC_tempW3 = L_U8FILTER_GAIN_10MSLOOP_3HZ; /* 3Hz */
    }
    
    lccbcs16EstMpress = LCESP_s16Lpf1Int(lccbcs16EstMpressRaw,lccbcs16EstMpressOld,(uint8_t)CBC_tempW3);
        
    if(lccbcs16EstMpress > MPRESS_200BAR) {lccbcs16EstMpress = MPRESS_200BAR;}
    else if(lccbcs16EstMpress < 0)        {lccbcs16EstMpress = 0;}
    else{}
        
    lccbcs16EstMpressOld = lccbcs16EstMpress;
}

void LDCBC_vEstWheelPress(void)
{
    
  /* Estimated Wheel Pressure is available in case of partial brake before ABS activation  */   
  
  #if __FRONT_CBC_ENABLE
  /*-------------- FL Pressure Estimation ---------------------------*/ 
  
    if((CBC_ACTIVE_fl==1)||(ABS_fz==1)||(CBC_FADE_OUT_fl==1)) 
    {
        if((HV_VL_fl==1)&&(AV_VL_fl==1))      /* Dump Mode */
        {
            lccbcs16EstWPressFL = lccbcs16EstWPressFL 
            - ((int16_t)U8_CBC_PRESS_EST_FRONT_DUMP_GAIN * (int16_t)(LDABS_u16FitSQRT((uint16_t)lccbcs16EstWPressFL*10)))/10;
        
            if(lccbcs16EstWPressFL<0) {lccbcs16EstWPressFL=0;}
            else{}
        }
        else if((HV_VL_fl==0)&&(AV_VL_fl==0)) /* Full Rise Mode */
        {
            CBC_tempW0 = lccbcs16EstMpress - lccbcs16EstWPressFL;
            if(CBC_tempW0<0) {CBC_tempW0=0;}
            else{} 
            
            lccbcs16EstWPressFL = lccbcs16EstWPressFL 
            + ((int16_t)U8_CBC_PRESS_EST_FRONT_RISE_GAIN * (int16_t)(LDABS_u16FitSQRT((uint16_t)CBC_tempW0*10)))/10;
        }
        else                                  /* Hold Mode */
        {
            lccbcs16EstWPressFL = lccbcs16EstWPressFL;
        }
        
        if(lccbcs16EstWPressFL>lccbcs16EstMpress)
        {
            lccbcs16EstWPressFL = lccbcs16EstMpress;
        }
        else{}
    }
    else
    {
        lccbcs16EstWPressFL = lccbcs16EstMpress;
    }   
    
  /*-------------- FR Pressure Estimation ---------------------------*/ 
  
    if((CBC_ACTIVE_fr==1)||(ABS_fz==1)||(CBC_FADE_OUT_fr==1)) 
    {
        if((HV_VL_fr==1)&&(AV_VL_fr==1))      /* Dump Mode */
        {
            lccbcs16EstWPressFR = lccbcs16EstWPressFR 
            - ((int16_t)U8_CBC_PRESS_EST_FRONT_DUMP_GAIN * (int16_t)(LDABS_u16FitSQRT((uint16_t)lccbcs16EstWPressFR*10)))/10;
        
            if(lccbcs16EstWPressFR<0) {lccbcs16EstWPressFR=0;}
            else{}
        }
        else if((HV_VL_fr==0)&&(AV_VL_fr==0)) /* Full Rise Mode */
        {
            CBC_tempW0 = lccbcs16EstMpress - lccbcs16EstWPressFR;
            if(CBC_tempW0<0) {CBC_tempW0=0;}
            else{} 
            
            lccbcs16EstWPressFR = lccbcs16EstWPressFR 
            + ((int16_t)U8_CBC_PRESS_EST_FRONT_RISE_GAIN * (int16_t)(LDABS_u16FitSQRT((uint16_t)CBC_tempW0*10)))/10;
        }
        else                                  /* Hold Mode */
        {
            lccbcs16EstWPressFR = lccbcs16EstWPressFR;
        }
        
        if(lccbcs16EstWPressFR>lccbcs16EstMpress)
        {
            lccbcs16EstWPressFR = lccbcs16EstMpress;
        }
        else{}
    }
    else
    {
        lccbcs16EstWPressFR = lccbcs16EstMpress;
    }   
  #endif /* __FRONT_CBC_ENABLE*/
    
  /*-------------- RL Pressure Estimation ---------------------------*/ 
  
    if((CBC_ACTIVE_rl==1)||(ABS_fz==1)||(EBD_rl==1)||(CBC_FADE_OUT_rl==1)) 
    {
        if((HV_VL_rl==1)&&(AV_VL_rl==1))      /* Dump Mode */
        {
            lccbcs16EstWPressRL = lccbcs16EstWPressRL 
            - ((int16_t)U8_CBC_PRESS_EST_REAR_DUMP_GAIN * (int16_t)(LDABS_u16FitSQRT((uint16_t)lccbcs16EstWPressRL*10)))/10;
        
            if(lccbcs16EstWPressRL<0) {lccbcs16EstWPressRL=0;}
            else{}
        }
        else if((HV_VL_rl==0)&&(AV_VL_rl==0)) /* Full Rise Mode */
        {
            CBC_tempW0 = lccbcs16EstMpress - lccbcs16EstWPressRL;
            if(CBC_tempW0<0) {CBC_tempW0=0;}
            else{} 
            
            lccbcs16EstWPressRL = lccbcs16EstWPressRL 
            + ((int16_t)U8_CBC_PRESS_EST_REAR_RISE_GAIN * (int16_t)(LDABS_u16FitSQRT((uint16_t)CBC_tempW0*10)))/10;
        }
        else                                  /* Hold Mode */
        {
            lccbcs16EstWPressRL = lccbcs16EstWPressRL;
        }
        
        if(lccbcs16EstWPressRL>lccbcs16EstMpress)
        {
            lccbcs16EstWPressRL = lccbcs16EstMpress;
        }
        else{}
    }
    else
    {
        lccbcs16EstWPressRL = lccbcs16EstMpress;
    }   

  /*-------------- RR Pressure Estimation ---------------------------*/ 
    
    if((CBC_ACTIVE_rr==1)||(ABS_fz==1)||(EBD_rr==1)||(CBC_FADE_OUT_rr==1)) 
    {

        if((HV_VL_rr==1)&&(AV_VL_rr==1))      /* Dump Mode */
        {
            lccbcs16EstWPressRR = lccbcs16EstWPressRR 
            - ((int16_t)U8_CBC_PRESS_EST_REAR_DUMP_GAIN * (int16_t)(LDABS_u16FitSQRT((uint16_t)lccbcs16EstWPressRR*10)))/10;
            
            if(lccbcs16EstWPressRR<0) {lccbcs16EstWPressRR=0;}
            else{}
        }
        else if((HV_VL_rr==0)&&(AV_VL_rr==0)) /* Rise Mode */
        {
            CBC_tempW0 = lccbcs16EstMpress - lccbcs16EstWPressRR;
            if(CBC_tempW0<0) {CBC_tempW0=0;}
            else{} 
            
            lccbcs16EstWPressRR = lccbcs16EstWPressRR
            + ((int16_t)U8_CBC_PRESS_EST_REAR_RISE_GAIN * (int16_t)(LDABS_u16FitSQRT((uint16_t)CBC_tempW0*10)))/10;
        }
        else                                  /* Hold Mode */
        {
            lccbcs16EstWPressRR = lccbcs16EstWPressRR;
        }
        
        if(lccbcs16EstWPressRR>lccbcs16EstMpress)
        {
            lccbcs16EstWPressRR = lccbcs16EstMpress;
        }
        else{}
    }
    else
    {
        lccbcs16EstWPressRR = lccbcs16EstMpress;
    }
}
#endif /* #if (__ABS_CBC_DISABLE==0) */

void LDCBC_vCallEstimation(void)
{
  #if (__ABS_CBC_DISABLE==0)
    LDCBC_vEstVehicleLateralG();  
    LDCBC_vEstVehicleLongitudinalG();
    LDCBC_vEstVehicleMpress();
    LDCBC_vEstWheelPress();
  #endif
}

#endif /* #if __CBC */


#if defined(__AUTOSAR_HEADER_ARCH_ENA)
    #define idx_FILE    idx_CL_LCCBCCallControl
    #include "Mdyn_autosar.h"
#endif    
