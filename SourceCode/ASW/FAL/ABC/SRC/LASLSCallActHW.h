#ifndef __LASLSCALLACTHW_H__
#define __LASLSCALLACTHW_H__

/*includes********************************************************************/

#include "LVarHead.h"


#if __SLS
/* Temporary Tuning parameter*************************************************/
#define U8_SLS_DUMPSCAN_TIME_R	10

/*Global Type Declaration ****************************************************/
typedef struct
{
    uint16_t    u1SlsHvVlAct     :1;
    uint16_t    u1SlsAvVlAct     :1;
}LASLS_VALVE_t;


/*Global Extern Variable  Declaration*****************************************/
extern LASLS_VALVE_t laslsValveRL, laslsValveRR;


/*Global Extern Functions  Declaration****************************************/
extern void    LASLS_vCallActHW(void);

#endif

#endif
