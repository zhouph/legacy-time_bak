 
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LSESPCheckEspError
	#include "Mdyn_autosar.h"
#endif


#if __TVBB
#include "LCTVBBCallControl.h"
#include "LCESPCallEngineControl.h"
#include "LCTODCallControl.h"
#include "hm_logic_var.h"

/* Variables Definition*******************************************************/
 
/* LocalFunction prototype ***************************************************/ 

void LCTVBB_vCallControl(void);
static void LCTVBB_vDetermineEnterExit(void);
static void LCTVBB_vSelectCtrlWhl(void);
static void LCTVBB_vIncRunCnt4(void);
static void LCTVBB_vCalControlMoment(void);
static void LCTVBB_vCalTVBB_Torq(void);
#if ((__TOD == 1)&&(__DRIVE_TYPE == FRONT_WHEEL_DRV)&&(__CAR_MAKER == HMC_KMC))
static void LCTVBB_vConvertTOD2EMS(void);
#endif

int8_t	    lctvbbs8ControlEnterTimer;
uint8_t	    lctvbbu8ControlExitTimer;
uint16_t	lctvbbu16TVBBCtlRunningCnt;
int16_t		tvbb_torq, tvbb_torq_up_max, tvbb_torq_up ; 
int16_t     lctvbbs16ControlMomentPGain, lctvbbs16ControlMomentDGain;
int16_t     lctvbbs16ControlMoment;
int16_t     tvbb_tempW0, tvbb_tempW1, tvbb_tempW2, tvbb_tempW3, tvbb_tempW4, tvbb_tempW5, tvbb_tempW6;
#if (__TCMF_UNDERSTEER_CONTROL == ENABLE)
uint8_t     lctvbbu8TCMFControlCnt;
#endif

/* Implementation*************************************************************/
void LCTVBB_vCallControl(void)
{
	LCTVBB_vCalControlMoment();
    LCTVBB_vDetermineEnterExit();
    LCTVBB_vSelectCtrlWhl();
	LCTVBB_vIncRunCnt4();
	LCTVBB_vCalTVBB_Torq();
	#if __TOD
	  #if ((__DRIVE_TYPE == FRONT_WHEEL_DRV)&&(__CAR_MAKER == HMC_KMC))
	LCTVBB_vConvertTOD2EMS();
      #endif
    #endif
}

static void LCTVBB_vCalControlMoment(void)
{
        if ( yaw_error_under_dot < 0 )
        {
            tvbb_tempW1 = LCESP_s16FindRoadDependatGain( det_alatm, S16_TVBB_YC_P_GAIN_INC_HM, S16_TVBB_YC_P_GAIN_INC_MM, S16_TVBB_YC_P_GAIN_INC_LM);
            tvbb_tempW2 = LCESP_s16FindRoadDependatGain( det_alatm, S16_TVBB_YC_D_GAIN_INC_HM, S16_TVBB_YC_D_GAIN_INC_MM, S16_TVBB_YC_D_GAIN_INC_LM);
        }
        else
        {
            tvbb_tempW1 = LCESP_s16FindRoadDependatGain( det_alatm, S16_TVBB_YC_P_GAIN_DEC_HM, S16_TVBB_YC_P_GAIN_DEC_MM, S16_TVBB_YC_P_GAIN_DEC_LM);
            tvbb_tempW2 = LCESP_s16FindRoadDependatGain( det_alatm, S16_TVBB_YC_D_GAIN_DEC_HM, S16_TVBB_YC_D_GAIN_DEC_MM, S16_TVBB_YC_D_GAIN_DEC_LM);
        }
               
        /* High-mu Weg  */  
        tvbb_tempW3 = LCESP_s16IInter4Point(vrefk,   S16_TVBB_HMU_LSPD  , S16_TVBB_CTRL_PD_WEG_HMU_LSPD ,
                                                     S16_TVBB_HMU_MLSPD , S16_TVBB_CTRL_PD_WEG_HMU_MLSPD,
                                                     S16_TVBB_HMU_MHSPD , S16_TVBB_CTRL_PD_WEG_HMU_MHSPD,
                                                     S16_TVBB_HMU_HSPD  , S16_TVBB_CTRL_PD_WEG_HMU_HSPD  );         
      
        /* Med-mu */ 
        tvbb_tempW4 = LCESP_s16IInter4Point(vrefk,  S16_TVBB_MMU_LSPD  , S16_TVBB_CTRL_PD_WEG_MMU_LSPD ,
                                                    S16_TVBB_MMU_MLSPD , S16_TVBB_CTRL_PD_WEG_MMU_MLSPD,
                                                    S16_TVBB_MMU_MHSPD , S16_TVBB_CTRL_PD_WEG_MMU_MHSPD,
                                                    S16_TVBB_MMU_HSPD  , S16_TVBB_CTRL_PD_WEG_MMU_HSPD  );                                                       

        /*  Low-mu */                                                  
        tvbb_tempW5 = LCESP_s16IInter4Point(vrefk,  S16_TVBB_LMU_LSPD  , S16_TVBB_CTRL_PD_WEG_LMU_LSPD ,
                                                    S16_TVBB_LMU_MLSPD , S16_TVBB_CTRL_PD_WEG_LMU_MLSPD,
                                                    S16_TVBB_LMU_MHSPD , S16_TVBB_CTRL_PD_WEG_LMU_MHSPD,
                                                    S16_TVBB_LMU_HSPD  , S16_TVBB_CTRL_PD_WEG_LMU_HSPD  );                                                      
                                                  
        tvbb_tempW6 = LCESP_s16FindRoadDependatGain( det_alatm,   tvbb_tempW3,  tvbb_tempW4,  tvbb_tempW5 ); 
   
        lctvbbs16ControlMomentPGain = (int16_t)( (((int32_t)tvbb_tempW1)*tvbb_tempW6)/100 );
     
        lctvbbs16ControlMomentDGain = (int16_t)( (((int32_t)tvbb_tempW2)*tvbb_tempW6)/100 );
        
    #if ((__CAR==GM_T300) || (__CAR == GM_M350)  || (GMLAN_ENABLE==ENABLE) || (__CAR == GM_J300)||(__CAR == GM_LAMBDA)||(__CAR==GM_MALIBU))
		if(gs_pos==1) 
		{
			tvbb_tempW6=U8_EDC_TM_1_GEAR_RATIO;
		}	
		else if(gs_pos==2) 
		{
			tvbb_tempW6=U8_EDC_TM_2_GEAR_RATIO;
		}	
		else if(gs_pos==3) 
		{
			tvbb_tempW6=U8_EDC_TM_3_GEAR_RATIO;
		}
		else if(gs_pos==4) 
		{
			tvbb_tempW6=U8_EDC_TM_4_GEAR_RATIO;
		}
		else if(gs_pos==5) 
		{
			tvbb_tempW6=U8_EDC_TM_5_GEAR_RATIO;
		}    		    
		else if(gs_pos==6) 
		{
			tvbb_tempW6=U8_EDC_TM_6_GEAR_RATIO;
		}    					
		else 
		{
			tvbb_tempW6=10;
		}      		
  #else
    tvbb_tempW6 = 10;
  #endif
    tvbb_tempW5 = (int16_t)( (((int32_t)eng_torq)*tvbb_tempW6)/10 );    
    
    /*
        tvbb_tempW0 = LCESP_s16IInter4Point(eng_torq,   S16_TVBB_ENGINE_LTQ  , S16_TVBB_CTRL_PD_WEG_ENGINE_LTQ ,  
                                                        S16_TVBB_ENGINE_MLTQ , S16_TVBB_CTRL_PD_WEG_ENGINE_MLTQ,  
                                                        S16_TVBB_ENGINE_MHTQ , S16_TVBB_CTRL_PD_WEG_ENGINE_MHTQ,  
                                                        S16_TVBB_ENGINE_HTQ  , S16_TVBB_CTRL_PD_WEG_ENGINE_HTQ   );
    */                                                
    tvbb_tempW0 = LCESP_s16IInter4Point(tvbb_tempW5, S16_TVBB_ENGINE_LTQ  , S16_TVBB_CTRL_PD_WEG_ENGINE_LTQ ,  
                                                     S16_TVBB_ENGINE_MLTQ , S16_TVBB_CTRL_PD_WEG_ENGINE_MLTQ,  
                                                     S16_TVBB_ENGINE_MHTQ , S16_TVBB_CTRL_PD_WEG_ENGINE_MHTQ,  
                                                     S16_TVBB_ENGINE_HTQ  , S16_TVBB_CTRL_PD_WEG_ENGINE_HTQ   );
                                                    
        lctvbbs16ControlMomentPGain = (int16_t)( (((int32_t)lctvbbs16ControlMomentPGain)*tvbb_tempW0)/100 );
        lctvbbs16ControlMomentDGain = (int16_t)( (((int32_t)lctvbbs16ControlMomentDGain)*tvbb_tempW0)/100 );
        
    tvbb_tempW0 = LCESP_s16IInter5Point(ldtvbbs16SlipRatioOfCtlWhl, S16_TVBB_CTRL_REF_WHL_SLIP_HMU_1, (int16_t)U8_TVBB_CTRL_WEG_FOR_SLIP_HMU_1,  
                                                                    S16_TVBB_CTRL_REF_WHL_SLIP_HMU_2, (int16_t)U8_TVBB_CTRL_WEG_FOR_SLIP_HMU_2, 
                                                                    S16_TVBB_CTRL_REF_WHL_SLIP_HMU_3, (int16_t)U8_TVBB_CTRL_WEG_FOR_SLIP_HMU_3,
                                                                    S16_TVBB_CTRL_REF_WHL_SLIP_HMU_4, (int16_t)U8_TVBB_CTRL_WEG_FOR_SLIP_HMU_4,
                                                                    S16_TVBB_CTRL_REF_WHL_SLIP_HMU_5, (int16_t)U8_TVBB_CTRL_WEG_FOR_SLIP_HMU_5);
    /*
    tvbb_tempW0 = LCESP_s16IInter5Point(ldtvbbs16SlipRatioOfCtlWhl, -WHEEL_SLIP_4, 50,  
                                                                    -WHEEL_SLIP_2, 90, 
                                                                                0, 100,
                                                                     WHEEL_SLIP_2, 100,
                                                                     WHEEL_SLIP_4, 110);*/
                                                                                
    lctvbbs16ControlMomentPGain = (int16_t)( (((int32_t)lctvbbs16ControlMomentPGain)*tvbb_tempW0)/100 );
    lctvbbs16ControlMomentDGain = (int16_t)( (((int32_t)lctvbbs16ControlMomentDGain)*tvbb_tempW0)/100 );                                                                            
        
        tvbb_tempW3 = (int16_t)( (((int32_t)delta_yaw_first)*lctvbbs16ControlMomentPGain)/100 );
    
        tvbb_tempW4 = (int16_t)( ((int32_t)yaw_error_under_dot)*lctvbbs16ControlMomentDGain/100 );
        /*
        tvbb_tempW3 = (int16_t)( (((int32_t)delta_yaw)*lctvbbs16ControlMomentPGain)/100 );  
                                                                                                  
        tvbb_tempW4 = (int16_t)( ((int32_t)yaw_error_dot)*lctvbbs16ControlMomentDGain/100 );
        */
    
    
        lctvbbs16ControlMoment = ( tvbb_tempW3 + tvbb_tempW4 ) ;   

        if( lctvbbs16ControlMoment  > 0 )
        {
            lctvbbs16ControlMoment = 0 ;
        }
        else
        {
            ;
        }
    }

static void LCTVBB_vDetermineEnterExit(void)
{
    lctvbbu1TVBB_ON_Old = lctvbbu1TVBB_ON;
    
    #if (__TCMF_UNDERSTEER_CONTROL == ENABLE)
    if (ldtvbbu1FrontWhlCtl == 0)
    {
	    if ((lcescu1TCMFUnderControlRl == 1) || (lcescu1TCMFUnderControlRr == 1))
	    {
	        if (lctvbbu8TCMFControlCnt < L_U8_TIME_10MSLOOP_30MS)
	        {
	            lctvbbu8TCMFControlCnt = lctvbbu8TCMFControlCnt + 1;
	        }
    else
    {
	            lctvbbu8TCMFControlCnt = L_U8_TIME_10MSLOOP_30MS;
	        }
	    }
	    else
	    {
	        if (lctvbbu8TCMFControlCnt >= 1)
	        {
	            lctvbbu8TCMFControlCnt = lctvbbu8TCMFControlCnt - 1;
	        }
	        else
	        {
	            lctvbbu8TCMFControlCnt = 0;
	        }
	    }
	}
	else
	{
	    if ((lcu1US2WHControlFlag_RL) || (lcu1US2WHControlFlag_RL))
	    {
	        if (lctvbbu8TCMFControlCnt < L_U8_TIME_10MSLOOP_30MS)
	        {
	            lctvbbu8TCMFControlCnt = lctvbbu8TCMFControlCnt + 1;
	        }
	        else
	        {
	            lctvbbu8TCMFControlCnt = L_U8_TIME_10MSLOOP_30MS;
	        }
	    }
	    else
	    {
	        if (lctvbbu8TCMFControlCnt >= 1)
	        {
	            lctvbbu8TCMFControlCnt = lctvbbu8TCMFControlCnt - 1;
	        }
	        else
	        {
	            lctvbbu8TCMFControlCnt = 0;
	        }
	    }
	}
	  #endif
    
	if ( (lctvbbu1TVBB_ON==0)   /* Enter Condition Detection */
		   && (ldtvbbu1InhibitFlag==0) 				/* TVBB Ready Status */
			 && (ldtvbbu1AccelPedalPushFlag==1)		/* Driver doesn't want to decrease the vehicle speed */
			 && (ldtvbbu1TurnIntentDetectFlag==1)		/* Driver want to make a turn */
			 //&& (delta_yaw <= ldtvbbs16EnterDelYawThres)	/* Understeer Condition */
			 && (delta_yaw_first <= ldtvbbs16EnterDelYawThres)	/* Understeer Condition */
			 && (vref5 < ldtvbbs16EnterSpeedLimit) 
			 && (ldtvbbs16SlipRatioOfCtlWhl>=ldtvbbs16EnterSlipThres)
			 && (lctvbbs16ControlMoment <= -30)
		    )
  {
			lctvbbs8ControlEnterTimer = lctvbbs8ControlEnterTimer + 1;
			lctvbbu8ControlExitTimer = 0;
			
	#if (__TCMF_UNDERSTEER_CONTROL == ENABLE)
        if (lctvbbu8TCMFControlCnt > 0)
        {
            lctvbbu1TVBB_ON = 1;
            lctvbbs8ControlEnterTimer = 0;
            lctvbbu16TVBBCtlRunningCnt = U16_TVBB_INIT_FULL_ON_TIME;
            
            if (wstr > 0)
			{
			    lctvbbu1Direction = 1;
			}
			else
			{
			    lctvbbu1Direction = 0;
			}
        }
        else
        {
    #endif
		if ((ldtvbbu8After2WHUScount < 21) || (lctvbbs8ControlEnterTimer >= 5 ))
		{
			lctvbbu1TVBB_ON = 1;
			lctvbbs8ControlEnterTimer = 0;
			if (ldtvbbu8After2WHUScount < 21)
			{
            lctvbbu16TVBBCtlRunningCnt = U16_TVBB_INIT_FULL_ON_TIME;
			}
			else
			{
			    lctvbbu16TVBBCtlRunningCnt = 1;
			}

			if (wstr > 0)
			{
			    lctvbbu1Direction = 1;
			}
			else
			{
			    lctvbbu1Direction = 0;
			}
		}
		else
		{
			ldtvbbu1WstrDotDirChange = 0;
			ldtvbbu1ExitByWstrDot = 0;	
			lctvbbu1Direction = 0;
		}
	#if (__TCMF_UNDERSTEER_CONTROL == ENABLE)
        }
	#endif
	} 
	else if ( (lctvbbu1TVBB_ON==1)   /* Enter Condition Detection */
		   && (ldtvbbu1InhibitFlag==0) 				/* TVBB Ready Status */
			 && (ldtvbbu1AccelPedalPushFlag==1)		/* Driver doesn't want to decrease the vehicle speed */
			 && (ldtvbbu1TurnIntentDetectFlag==1)		/* Driver want to make a turn */
			 //&& (delta_yaw <= S16_TVBB_DELTA_YAW_TH_EXTI)	/* Understeer Condition */
			 && (delta_yaw_first <= S16_TVBB_DELTA_YAW_TH_EXTI)	/* Understeer Condition */
			 && ((ldtvbbu1ExitByWstrDot == 0)||(det_alatm > lsesps16MuMed))
			 && (lctvbbs16ControlMoment <= 0)
		    )
	{
		lctvbbu1TVBB_ON = 1;
		lctvbbs8ControlEnterTimer = 0;		
		lctvbbu8ControlExitTimer = 0;     	 
	}
	else
	{
		lctvbbu8ControlExitTimer = lctvbbu8ControlExitTimer + 1;   
		
		if ((ldtvbbu1InhibitFlag == 1) && (ldtvbbu8InhibitID == 2))
		{
		    esp_tempW1 = 1;
		}
		else if (((ldtvbbu1InhibitFlag == 1) && ((ldtvbbu8InhibitID == 3) || (ldtvbbu8InhibitID == 4))) || (ldtvbbu1AccelPedalPushFlag == 0))
		{
		    esp_tempW1 = U8_TVBB_EXIT_TIME_RAPID + 1;
		}
		else
		{
		    esp_tempW1 = U8_TVBB_EXIT_TIME+1;
		}
					
		if (lctvbbu8ControlExitTimer >= esp_tempW1)
		{
			lctvbbu1TVBB_ON = 0;
			lctvbbu8ControlExitTimer = 0;  
			ldtvbbu1WstrDotDirChange = 0;
			lctvbbu1Direction = 0;
			ldtvbbu1ExitByWstrDot = 0;
		}
		else
		{
			;	
		}
		if (lctvbbs8ControlEnterTimer >= 1)
		{
		    lctvbbs8ControlEnterTimer = lctvbbs8ControlEnterTimer - 1;
		}
		else
		{
		    ;
		}
	}
}

static void LCTVBB_vSelectCtrlWhl(void)
{
    /* Right wheel or Left wheel*/
	if (lctvbbu1TVBB_ON==1)
	{
		if (nor_alat>=LAT_0G1G)
		{
			lctvbbu1LeftWhlCtl = 1;
			lctvbbu1RightWhlCtl = 0;
		}
		else if	(nor_alat<=(-LAT_0G1G))
		{
			lctvbbu1LeftWhlCtl = 0;
			lctvbbu1RightWhlCtl = 1;
		}
		else
		{
			;	/* Don't Change Control Wheel */
		}	
	}
	else
	{
		lctvbbu1LeftWhlCtl = 0;
		lctvbbu1RightWhlCtl = 0;
	}
}

static void LCTVBB_vIncRunCnt4(void)
{
	if (lctvbbu1TVBB_ON==1)
	{
	    if (lctvbbu8ControlExitTimer == 0)
	    {	        
		    lctvbbu16TVBBCtlRunningCnt = lctvbbu16TVBBCtlRunningCnt + 1;
		    if (lctvbbu16TVBBCtlRunningCnt > 250)
		    {
		        lctvbbu16TVBBCtlRunningCnt = 250;
		    }
		    else
		    {
		        ;
		    }
		}
		else
		{
		    lctvbbu16TVBBCtlRunningCnt = U16_TVBB_INIT_FULL_ON_TIME-2;
		}
	}
    else
    {
		lctvbbu16TVBBCtlRunningCnt = 0;
    }    
}

static void LCTVBB_vCalTVBB_Torq(void)
{	
#if __TVBB_ENG_TORQ_UP_CTRL

    int16_t lctvbbs16TorqIncRate, lctvbbs16TorqDecRate;
    	
	if (lctvbbu1TVBB_ON==1)
	{ 
        if (AUTO_TM == 1)
        {    
		    switch (gear_pos)
		    {
		        case 1:
		           tvbb_torq_up_max = S16_TVBB_E_TORQ_UP_1ST_G ;
		        break;
		        
		        case 2:
		           tvbb_torq_up_max = S16_TVBB_E_TORQ_UP_2ND_G ;
		        break;
		           
		        case 3:
		           tvbb_torq_up_max = S16_TVBB_E_TORQ_UP_3RD_G ;
		        break;    
		        
		        case 4:
		           tvbb_torq_up_max = S16_TVBB_E_TORQ_UP_4TH_G ;
		        break;
		        
		        case 5:
		           tvbb_torq_up_max = S16_TVBB_E_TORQ_UP_5TH_G ;
		        break;
		        
		        case 6:
		           tvbb_torq_up_max = S16_TVBB_E_TORQ_UP_6TH_G ;
		        break;
		        
                default:
		           tvbb_torq_up_max = 0;
                break;		    
		    }
	    }
		else
		{
		    switch (gear_state)
		    {
		        case 1:
		           tvbb_torq_up_max = S16_TVBB_E_TORQ_UP_1ST_G ;
		        break;
		        
		        case 2:
		           tvbb_torq_up_max = S16_TVBB_E_TORQ_UP_2ND_G ;
		        break;
		           
		        case 3:
		           tvbb_torq_up_max = S16_TVBB_E_TORQ_UP_3RD_G ;
		        break;    
		        
		        case 4:
		           tvbb_torq_up_max = S16_TVBB_E_TORQ_UP_4TH_G ;
		        break;
		        
		        case 5:
		           tvbb_torq_up_max = S16_TVBB_E_TORQ_UP_5TH_G ;
		        break;
		        
		        case 6:
		           tvbb_torq_up_max = S16_TVBB_E_TORQ_UP_6TH_G ;
		        break;
		        
                default:
		           tvbb_torq_up_max = 0;
                break;		    
		    }
		}
		
		tvbb_torq_up_max = LCESP_s16IInter2Point((-lctvbbs16ControlMoment),
		                                                                 0,(tvbb_torq_up_max/2),
		                                                               200, tvbb_torq_up_max);  
		
		lctvbbs16TorqIncRate = tvbb_torq_up_max/L_U8_TIME_10MSLOOP_300MS;
		lctvbbs16TorqDecRate = tvbb_torq_up_max/L_U8_TIME_10MSLOOP_200MS;
		
		if (lctvbbu8ControlExitTimer == 0)
		{
		    if (tvbb_torq_up < (tvbb_torq_up_max - lctvbbs16TorqIncRate))
		    {
		        tvbb_torq_up = tvbb_torq_up + lctvbbs16TorqIncRate;
		        tvbb_torq = drive_torq + tvbb_torq_up;
		    }
		    else
		    {
		        tvbb_torq = drive_torq + tvbb_torq_up_max;
		    }
		}
		else
		{
		    if (tvbb_torq_up >= lctvbbs16TorqDecRate)
		    {
		        tvbb_torq_up = tvbb_torq_up - lctvbbs16TorqDecRate;
		        tvbb_torq = drive_torq + tvbb_torq_up;
		    }
		    else
		    {
		        tvbb_torq = drive_torq;
		    }
		}				 
	}
	else
	{ 
		tvbb_torq = drive_torq ;
		tvbb_torq_up = 0;
		tvbb_torq_up_max = 0;
	}
	
#else
	
	tvbb_torq = drive_torq ;
	
#endif	
	 	
}

#if ((__TOD == 1)&&(__DRIVE_TYPE == FRONT_WHEEL_DRV)&&(__CAR_MAKER == HMC_KMC))
static void LCTVBB_vConvertTOD2EMS(void)
{
    lctvbbu1TODDisable = 0;

  #if __GM_FailM
	if((lctcsu1EngCtrlInhibit==1)||(lctcsu1InhibitByFM==1))
	{
		lctvbbu1TODDisable=1;
	}
	else
	{
		;
	}
  #else
	if((abs_error_flg==1)||(ebd_error_flg==1)||(tcs_error_flg==1)
	||(tc_error_flg==1)||(vdc_error_flg==1)
	||(fu1OnDiag == 1)||(init_end_flg == 0)||(wu8IgnStat==CE_OFF_STATE)
	||(fu1VoltageUnderErrDet==1)||(fu1VoltageLowErrDet==1)||(fu1VoltageOverErrDet==1)) 
	{
		lctvbbu1TODDisable=1;
	}
	else
	{
		;
	}
  #endif
      
    if ((lctvbbu1TODDisable == 0) && (lctvbbu1TVBB_ON == 1) && (ABS_ON == 0))
    {
       TOD4wdOpen			=2;   
       TOD4wdLimReq		    =1;       
       TOD4wdLimMode		=1;   
       TOD4wdTqcLimTorq		=U16_TVBB_TOD_LIM_TORQ;    
    }
    else
    {
        #if (__RWD_SPORTSMODE==SPORTMODE_1) /*(__SPORTSMODE == 1)*/
        if ((fu1TCSDisabledBySW == 1) && (lctvbbu1TVBB_ON_Old == 1) && (lctvbbu1TVBB_ON == 0))
        {
            TOD4wdOpen			=0;    
            TOD4wdLimReq		=0;    
            TOD4wdLimMode		=0;    
            TOD4wdTqcLimTorq	=0xFAFF;    
        }                              
        else
        {
            ;
        }
        #else
        ;
        #endif
    }
}
#endif /*__TOD*/

#endif	/* #if __TVBB */
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LSESPCheckEspError
	#include "Mdyn_autosar.h"
#endif
