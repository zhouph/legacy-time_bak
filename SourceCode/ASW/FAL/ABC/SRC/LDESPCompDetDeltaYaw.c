
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDESPCompDetDeltaYaw
	#include "Mdyn_autosar.h"
#endif
#include "LVarHead.h"
#include "LCESPCalInterpolation.h"
#include "LCESPCalLpf.h"
#if __VDC
void LDESP_vCompDetDeltaYaw(void);

void LDESP_vCompDetDeltaYaw(void)
{
    det_delta_yaw_old = det_delta_yaw;
    abs_delta_yaw = McrAbs(delta_yaw_first);      /* First delta_yaw*/

    esp_tempW5 = LCESP_s16IInter2Point( esp_mu, 400, 2, 600, 8); 

    if ( abs_delta_yaw >= det_delta_yaw ) 
    {
        det_delta_yaw = (int16_t)(LCESP_u16Lpf1Int((uint16_t)abs_delta_yaw, (uint16_t)det_delta_yaw_old, L_U8FILTER_GAIN_10MSLOOP_18HZ));               
    }
    else
    { 
        det_delta_yaw = LCESP_s16Lpf1Int1024(abs_delta_yaw, det_delta_yaw_old, (uint8_t)esp_tempW5);   
    }       
    det_delta_yaw_limit_old = det_delta_yaw_limit;
    abs_delta_yaw_limit = McrAbs(delta_yaw);       /* Second delta_yaw */

    if ( abs_delta_yaw_limit >= det_delta_yaw_limit ) 
    {
        det_delta_yaw_limit = (int16_t)(LCESP_u16Lpf1Int((uint16_t)abs_delta_yaw_limit, (uint16_t)det_delta_yaw_limit_old, L_U8FILTER_GAIN_10MSLOOP_18HZ));       
    }
    else
    { 
        det_delta_yaw_limit = LCESP_s16Lpf1Int1024(abs_delta_yaw_limit, det_delta_yaw_limit_old, (uint8_t)esp_tempW5);
	}
/*  dis MU input*/        
    if ((WSTR_360_JUMP==1)||(SAS_CHECK_OK==0)||(BACK_DIR==1)) 
    {
    	det_delta_yaw=det_delta_yaw_limit=0;
    }
    else
    {
    	;
    }
    
}
#endif
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDESPCompDetDeltaYaw
	#include "Mdyn_autosar.h"
#endif
