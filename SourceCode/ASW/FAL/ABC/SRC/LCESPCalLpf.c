
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCESPCalLpf
	#include "Mdyn_autosar.h"
#endif


#include "LCESPCalLpf.h"

int16_t LCESP_s16Lpf1Int(int16_t x_input, int16_t x_old, uint8_t input_gain);
uint16_t LCESP_u16Lpf1Int(uint16_t x_input, uint16_t x_old, uint8_t input_gain);
int16_t LCESP_s16Lpf1Int1024(int16_t x_input, int16_t x_old, uint8_t input_gain);

/*
#if !__VDC
uint16_t tempUW4;
#endif
*/

#if __ECU==ESP_ECU_1
int16_t lpf_tempW4;
uint16_t lpf_tempUW4;
#endif

int16_t LCESP_s16Lpf1Int(int16_t x_input, int16_t x_old, uint8_t input_gain)
{
	#if __ECU==ABS_ECU_1
	int16_t lpf_tempW4;
	#endif
//  tempW3 = (int16_t)( (((int32_t)tempW2)*tempW1)/tempW0);
//  esp_tempW1 = FN_MUL_DIV_S( x_input, input_gain, 128);
/*    tempW2 = x_input; tempW1 = input_gain; tempW0 = 128;
    s16muls16divs16();
    tempW4 = tempW3;
*/
/*    tempW4 = (int16_t)((((int32_t)x_input)*((int16_t)input_gain)
                   +(((int32_t)x_old)*(128-(int16_t)input_gain))) >>(int8_t)7); */
    
    lpf_tempW4 = (int16_t)((((int32_t)x_input)*((int16_t)input_gain)
                   +(((int32_t)x_old)*(128-(int16_t)input_gain)))/128);

//  esp_tempW2 = FN_MUL_DIV_S( x_old, 128-input_gain, 128);
/*    tempW2 = x_old; tempW1 = 128-input_gain; tempW0 = 128;
    s16muls16divs16(); */
    
    return (lpf_tempW4);
}

int16_t LCESP_s16Lpf1Int1024(int16_t x_input, int16_t x_old, uint8_t input_gain)
{
	#if __ECU==ABS_ECU_1
	int16_t lpf_tempW4;
	#endif
/*    tempW4 = (int16_t)((((int32_t)x_input)*((int16_t)input_gain)
                   +(((int32_t)x_old)*(1024-(int16_t)input_gain))) >>(int8_t)10); */
    lpf_tempW4 = (int16_t)((((int32_t)x_input)*((int16_t)input_gain)
                   +(((int32_t)x_old)*(1024-(int16_t)input_gain))) /1024);
                   
    return (lpf_tempW4);                   
}

uint16_t LCESP_u16Lpf1Int(uint16_t x_input, uint16_t x_old, uint8_t input_gain)
{
	#if __ECU==ABS_ECU_1
	uint16_t lpf_tempUW4;
	#endif
    
/*    tempUW4 = (uint16_t)((((uint32_t)x_input)*((uint16_t)input_gain)
                   +(((uint32_t)x_old)*(128-(uint16_t)input_gain))) >>(int8_t)7); */
    lpf_tempUW4 = (uint16_t)((((uint32_t)x_input)*((uint16_t)input_gain)
                   +(((uint32_t)x_old)*(128-(uint16_t)input_gain)))/128);

    return lpf_tempUW4;
}
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCESPCalLpf
	#include "Mdyn_autosar.h"
#endif
