#ifndef __LCHRBCALLCONTROL_H__
#define __LCHRBCALLCONTROL_H__

/*includes********************************************************************/
#include "LVarHead.h"

/*Global MACRO CONSTANT Definition********************************************/

 #if (__CAR==GM_T300) || (__CAR==GM_GSUV) || (__CAR==GM_M350)
#define __INHIBIT_HRB_FOR_IGN_OFFON 1
 #else
#define __INHIBIT_HRB_FOR_IGN_OFFON 0
 #endif

#define S8_HRB_READY                   1
#define S8_HRB_HOLD                    2
#define S8_HRB_BOOST                   3
#define S8_HRB_RELEASE                 4
#define S8_INHIBITION                  5
#define S8_HRB_PULSEUP                 6
#define S8_NORMAL                      0

// /* To be moved into Application Field */
//#define U8_HRB_MAX_PULSE_UP             4 /* Max. Num. of pulse-ups Since HRB Engagement Until Brake Boost. Default : 4 pulses */
//#define U8_HRB_INITIAL_PULSEUP_HOLD     L_U8_TIME_10MSLOOP_50MS /* Hold Scans at the first HRB pulse-up. Default 6 scans*/
//#define U8_HRB_FINAL_PULSEUP_HOLD       L_U8_TIME_10MSLOOP_20MS /* Hold Scans at the last HRB pulse-up. Default 2 scans */
//#define S8_HRB_ADD_REAPPLY_TIME         L_U8_TIME_10MSLOOP_10MS /* Compensation Value of Pulse-up Rise Time During HRB Ctrl. Default 2ms */
//#define U8_HRB_MP_INC_GAIN             MPRESS_7BAR /* Amount of adaptation per 10bar increasing, Default 7bar, Max 10bar */
//#define U8_HRB_MP_DEC_GAIN             MPRESS_7BAR /* Amount of adaptation per 10bar decreasing, Default 7bar, Max 10bar */
//#define U8_HRB_MP_DEC_GAIN_BOOST       MPRESS_5BAR /* Amount of adaptation per 10bar decreasing during brake boost, Default 5bar, Max 10bar */
//#define U8_HRB_MP_RELEASE_COMP         MPRESS_3BAR /* Additional adaptation at brake releasing state, Default 3bar, Max 5 bar*/

 /*                                    */
  #if __HRB
/*Global Type Declaration ****************************************************/
#define lchrbu1ActiveFlag              HRBF0.bit7
#define lchrbu1EnterCtrl               HRBF0.bit6
#define lchrbu1ExitCtrl                HRBF0.bit5
#define lchrbu1EndCtrl                 HRBF0.bit4
#define lchrbu1TCSecondary             HRBF0.bit3
#define lchrbu1TCPrimary               HRBF0.bit2
#define lchrbu1ESVSecondary            HRBF0.bit1
#define lchrbu1ESVPrimary              HRBF0.bit0

/*  HSA Flag1 (HSA CONTROL)  */
#define lchrbu1Inhibit                 HRBF1.bit7
#define lchrbu1SlipBwReference         HRBF1.bit6
#define lchrbu1ExitByBrkRelease        HRBF1.bit5
#define lchrbu1FirstBoostP             HRBF1.bit4
#define lchrbu1FirstBoostS             HRBF1.bit3
#define lchrbu1BoostFlag               HRBF1.bit2
#define lchrbu1MPreleasingState        HRBF1.bit1
#define lchrbu1StartBoost              HRBF1.bit0



/*Global Extern Variable  Declaration*****************************************/
extern struct   U8_BIT_STRUCT_t HRBF0, HRBF1;

 

extern int16_t lchrbs16AssistPressure;
extern int16_t lchrbs16NeedBrkFluidCntP;
extern int16_t lchrbs16NeedBrkFluidCntS;

extern int8_t	lchrbs8CtrlMode;
extern int16_t lchrbs16ApplyCntP;
extern int16_t lchrbs16ApplyCntS;
extern int8_t	lchrbs8ExitCnt;

extern int16_t lchrbs16EnterMpress;
extern int16_t lchrbs16RearSkidPress;

extern int16_t lchrbs16PriorMP;
extern int8_t  lchrbs8PriorSlip;
extern int16_t lchrbs16FrontSkidPress;
extern uint8_t lchrbu8PressReleaseCnt;
extern uint8_t lchrbu8BrakeReleaseCnt;
extern int16_t lchrbs16EnterRearPress;

extern int16_t lchrbs16MPAdaptation;
extern int16_t lchrbs16SlipAdaptation;

extern uint8_t    lchrbu8LowSlipTim;
extern uint8_t    lchrbu8PulseUpHoldScans;

 #if __ADVANCED_MSC
extern int16_t hrb_msc_target_vol;
 #endif

/*Global Extern Functions  Declaration****************************************/
extern  void    LCHRB_vCallControl(void);
extern  void    LCHRB_vResetCtrl(void);
extern  void    LCMSC_vSetHRBTargetVoltage(void);
  #endif
#endif
