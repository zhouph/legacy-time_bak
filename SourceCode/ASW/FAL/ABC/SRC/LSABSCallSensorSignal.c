/* Includes	********************************************************/
#if !SIM_MATLAB

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LSABSCallSensorSignal
	#include "Mdyn_autosar.h"
#endif
#endif

#if	__VDC
//#include	"../Failsafe/ESC/FYLgYawHWCheck.h"
#endif


#if	__VDC && (_ESCi_ENA==ENABLE)
#include    "LSESPFilterEspSensor.h"
//#include "../Failsafe/ESC/FYLgYawSensorCalc.h"
#endif

#include	"LSABSCallSensorSignal.H"
#include 	"LSABSEstLongAccSensorOffsetMon.h"
#include 	"LCHDCCallControl.H"
#include 	"LCACCCallControl.H"
#include 	"LDABSCallDctForVref.H"
#include 	"LDABSCallVrefEst.H"
#include 	"LDABSCallEstVehDecel.H"
#include 	"LDABSCallVrefMainFilterProcess.H"
#include 	"LCESPCalLpf.h"
#include 	"LCallMain.h"
#include 	"LCABSCallControl.h"
#include 	"LAABSCallMotorSpeedControl.h"
#include    "LSCANSignalInterface.h"
#if __DEC
#include 	"LCDECCallControl.h"
#endif
#if	__VDC
#include 	"LDABSCallVrefCompForESP.H"
#include 	"LSESPFilterEspSensor.h"
#include	"LSESPCalSensorOffset.h"
#endif
#if	__SCC
#include 	"SCC/LCSCCCallControl.H"
#endif
#if	(__EPB_INTERFACE==ENABLE)
#include 	"LCEPBCallControl.H"
#endif
#if __CDM
#include 	"SCC/LCWPCCallControl.h"
#endif
#if	(HSA_VARIANT_ENABLE==ENABLE) ||	(__EPB_INTERFACE==ENABLE)
/*#include 	"../System/WChkVariantCode.h"*/
#endif
#if (__MGH80_MOCi == ENABLE)
#include    "epb/L_InterfaceLinkData.h"
#endif
/*******************************************************************/

/* Local Definition	 ***********************************************/
#define	SCALE_FACTOR	8
#define	MAX_VRAD_DIFF	914 /*40g*/
/*******************************************************************/

/* Variables Definition*********************************************/

U8_BIT_STRUCT_t LSABSF0;
#if	__TCS || __ETC || __EDC
U8_BIT_STRUCT_t LSABSF1;
#endif
#if	__AX_SENSOR
int16_t	ax_Filter_temp;
int16_t	ax_Filter;
int16_t	ax_Filter_old;
int16_t	along_old;
#endif
uint8_t	bls_k_timer;
uint8_t	bls_filter_timer;															 
#if	__VDC
uint8_t	brk_st_det_cnt;
#endif
uint8_t	brk_st_reset_cnt;
uint8_t	ldabsu8BrakingStateResetTimer;
#if	__AX_SENSOR
int16_t	lss16absAx1000gtempOLD;
int16_t	lss16absAx1000gtemp;
int16_t	lss16absAx1000g;
int16_t	lss16absAx1000gDiff;
int16_t	lss16absAx1000gDiffOLD;
int16_t	lss16absAxOffsetComp1000g;
int16_t	lsabss16FilteredAxSignalOld;
int16_t	lsabss16FilteredAxSignal;
int16_t	lsabss16FilteredAxSignalTemp;
int16_t	lsabss16RawAxSignal;
int16_t	lsabss16RawAxSignalOld;
int16_t	lsabss16RawAxSignalTemp;		
	#if	__LONG_LSABS_vEstLongGsensorOffset_CALC
	int16_t	g_vref;
	int16_t	along_min;
	int16_t	along_max;
	int16_t	vref2_grv_min;
	int16_t	vref2_grv_max;
	int16_t	along_setted;
	int16_t	along_filt;
	int16_t	along_filt_diff[5];
	int16_t	vref2_grv;
	int16_t	vref2_grv_diff[5];
	int16_t	g_offset;
	int16_t	g_offset_diff[5];
	int16_t	vref2_old;
	int16_t	vref2_grv_diff1;
	int16_t	vref2_grv_diff_old;
	int16_t	acc_vref2;
	int16_t	acc_var;
	uint8_t	vref2_grv_count;
	uint8_t	along_fail,g_sensor_check_count,along_offset_count;
	#endif
#endif
int8_t	arad_timer;
int8_t	arad_avg_counter;
	
#if	_ESCi_ENA==ENABLE
	int16_t iax_RotationAngleCrt;
	int16_t	lss16absAx1000gDiffOLD_i;
	int16_t	lsabss16FilteredAxSignalOld_i;
	int16_t	lsabss16FilteredAxSignal_i;
	int16_t	lsabss16FilteredAxSignalTemp_i;	   

	int16_t	lsabss16FilteredAxSignalOld_i2;
	int16_t	lsabss16FilteredAxSignal_i2;
	int16_t	lsabss16FilteredAxSignalTemp_i2;   

	int16_t	lsesps16ESCiYawAccelCompAx;

	int16_t	lsabss16FilteredAxSignalOld_iN;
	int16_t	lsabss16FilteredAxSignal_iN;
	int16_t	lsabss16FilteredAxSignalTemp_iN;   
	int16_t	lsesps16ESCiYawAccelCompAxN;	
	
    #if __CAR==KMC_TD/*for MGH-80 TD5DR*/
		#define iyaw_offset_f        0
		#define ialat_offset_f       -15
		#define iax_offset_f2        -10
    	#define	iax_offset_f	   		 0/*0.1g*/ //TD5DR
        #define DISTANCE_ALONG_Xaxis 90
		#define DISTANCE_ALONG_Yaxis -55
        #define ESCi_Gain 1
		#define ROT_ANGLE_COS        920
		#define ROT_ANGLE_SIN        391
	#elif __CAR==PSA_C3
        #define  iyaw_offset_f      0 
        #define  ialat_offset_f     0 
        #define  iax_offset_f2      0 
		#define	iax_offset_f	   		 -10/*0.1g*/ //TD5DR
        #define DISTANCE_ALONG_Xaxis 0
		#define DISTANCE_ALONG_Yaxis 0
        #define ESCi_Gain 1
        #define ROT_ANGLE_COS 1000
        #define ROT_ANGLE_SIN 0
	#else
        #define  iyaw_offset_f      0 /*0.08 deg/s */
        #define  ialat_offset_f     0 /*0.009 g*/
		#define iax_offset_f2        0
		#define	iax_offset_f	   		 0/*0.1g*/ //TD5DR
		#define DISTANCE_ALONG_Xaxis 0 /*150*/
		#define DISTANCE_ALONG_Yaxis 0
        #define ESCi_Gain 1
        #define ROT_ANGLE_COS 1000
        #define ROT_ANGLE_SIN 0
	#endif

	#define ESCi_P_COMP_GAIN        4
	#define ESCi_P_COMP_GAIN_AX     6

#endif 
	
/*******************************************************************/
/* Local Function prototype	****************************************/
/*******************************************************************/
void	LSABS_vCallSensorSignal(void);
void	LSABS_vCallWheelSpeedSignal(void);
void	LSABS_vCalEachWheelSpeed(struct	W_STRUCT *WL_temp,struct W_STRUCT *WL_temp2,struct W_STRUCT	*WL_temp3,struct W_STRUCT *WL_temp4);
void	LSABS_vFilterWheelspeedSignal(void);
void	LSABS_vFilterWheelSpeedGradient(void);
void	LSABS_vStoreWheelSpeed(void);
void	LSABS_vCallBLS(void);
void	LSABS_vDetBLS(void);
void	LSABS_vChkBLSError(void);
void	LSABS_vDetBrakeSignal(void);
void	LSABS_vCalWheelAcceleration(void);
void	LSABS_vCalEachWheelAcceleration(struct W_STRUCT	*WL);

#if	__AX_SENSOR
void	LSABS_vFilterLongAccSignal(void);
#endif

#if	__TCS || __ETC || __EDC
void	LSABS_vCallGearInfSignal(void);
#endif
/*******************************************************************/
void LSABS_vCallSensorSignal(void)
{

	#if _ESCi_ENA==ENABLE	
		uint8_t Multiple;	
		Multiple = 10;
	#endif
	
	if(speed_calc_timer	!= (uint8_t)0xff)
	{
		speed_calc_timer++;
	}
	else
	{
		;
	}
	LSABS_vCallWheelSpeedSignal();
	LSABS_vCallBLS();
	#if	__TCS || __ETC || __EDC
	LSABS_vCallGearInfSignal();
	#endif
	#if	__AX_SENSOR
	LSABS_vFilterLongAccSignal();
	#elif _ESCi_ENA==ENABLE
		/*Old Raw Signal*/
		lsabss16FilteredAxSignalOld_i = lsabss16FilteredAxSignal_i;
		if(BACK_DIR==1)
		{
			tempW3 = (int16_t)(-s16ESCi_AxSig);
		}
		else
		{
			tempW3 = (int16_t)(s16ESCi_AxSig);
		}
		tempW3 = tempW3-iax_offset_f;
		if(ADVANCED_MSC_ON_FLG==1)
		{
			lsabss16FilteredAxSignalTemp_i = LCESP_s16Lpf1Int(Multiple*tempW3,lsabss16FilteredAxSignalTemp_i,L_U8FILTER_GAIN_10MSLOOP_1HZ);
		}
		else
		{
			lsabss16FilteredAxSignalTemp_i = LCESP_s16Lpf1Int(Multiple*tempW3,lsabss16FilteredAxSignalTemp_i,L_U8FILTER_GAIN_10MSLOOP_3HZ);
		}
		lsabss16FilteredAxSignal_i = lsabss16FilteredAxSignalTemp_i/Multiple;	  		
		/********************************************************************/
		
		/*Filter 2 Step Avg*/		
		lsabss16FilteredAxSignalOld_i2 = lsabss16FilteredAxSignal_i2;
		if(BACK_DIR==1)
		{
			tempW3 = (int16_t)(-s16ESCi_AxSigFil);
		}
		else
		{
			tempW3 = (int16_t)(s16ESCi_AxSigFil);
		}
		tempW3 = (int16_t)(tempW3-iax_offset_f);
		if(ADVANCED_MSC_ON_FLG==1)
		{
			lsabss16FilteredAxSignalTemp_i2 = LCESP_s16Lpf1Int(Multiple*tempW3,lsabss16FilteredAxSignalTemp_i2,L_U8FILTER_GAIN_10MSLOOP_1HZ);
		}
		else
		{
			lsabss16FilteredAxSignalTemp_i2 = LCESP_s16Lpf1Int(Multiple*tempW3,lsabss16FilteredAxSignalTemp_i2,L_U8FILTER_GAIN_10MSLOOP_3HZ);
		}
		lsabss16FilteredAxSignal_i2 = lsabss16FilteredAxSignalTemp_i2/Multiple;	
		/********************************************************************/
				
		/*New Filtered Signal*/
		lsabss16FilteredAxSignalOld_iN = lsabss16FilteredAxSignal_iN;
		
		tempW2 = (int16_t)(s16ESCi_AxSig2 - iax_offset_f2);
		tempW3 = (int16_t)(s16ESCi_AySig2 - ialat_offset_f);
		iax_RotationAngleCrt = (int16_t)( ( ((int32_t)tempW2*ROT_ANGLE_COS) - ((int32_t)tempW3*ROT_ANGLE_SIN) )/1000 );
		tempW3 = (int16_t)(iax_RotationAngleCrt-iax_offset_f);

		lsabss16FilteredAxSignalTemp_iN = LCESP_s16Lpf1Int(Multiple*tempW3,lsabss16FilteredAxSignalTemp_iN,L_U8FILTER_GAIN_10MSLOOP_4HZ);

		lsesps16ESCiYawAccelCompAxN = (int16_t)( ((int32_t)ESCi_P_COMP_GAIN_AX*lsesps16ESCiYawAccelN*DISTANCE_ALONG_Yaxis)/(1000) ); //[1000,g]

		lsabss16FilteredAxSignal_iN = (lsabss16FilteredAxSignalTemp_iN/Multiple) - lsesps16ESCiYawAccelCompAxN;
		/********************************************************************/
		#if  (__ESCi_CONTROL==ENABLE)
			ax_Filter_old = ax_Filter;
			along_old = along;
		if(BACK_DIR==1)
		{
					ax_Filter = (int16_t)(-lsabss16FilteredAxSignal_iN/Multiple);
					along = (int16_t)(-lsabss16FilteredAxSignal_iN/Multiple);
		}
		else
		{
					ax_Filter = (int16_t)(lsabss16FilteredAxSignal_iN/Multiple);
					along = (int16_t)(lsabss16FilteredAxSignal_iN/Multiple);
		}

			lsabss16RawAxSignal = lsabss16FilteredAxSignal_iN;
			lss16absAx1000gDiffOLD_i = lss16absAx1000gDiff;
			lss16absAx1000gDiff = LCESP_s16Lpf1Int( (lsabss16FilteredAxSignal_iN-lsabss16FilteredAxSignalOld_iN),lss16absAx1000gDiffOLD_i,L_U8FILTER_GAIN_10MSLOOP_7HZ );

			lss16absAx1000g = lsabss16FilteredAxSignal_iN;
			lss16absAxOffsetComp1000g = lsabss16FilteredAxSignal_iN;
		#else
			ax_Filter_old=ax_Filter;
			ax_Filter=lsabss16FilteredAxSignal/Multiple;
			along_old=along;
			along=lsabss16FilteredAxSignal/Multiple;
		#endif
	#endif
}

/*******************************************************************/
void	LSABS_vCallWheelSpeedSignal(void)
{

	FL.ALG_INHIBIT=fu1WheelFLErrDet;
	FR.ALG_INHIBIT=fu1WheelFRErrDet;
	RL.ALG_INHIBIT=fu1WheelRLErrDet;
	RR.ALG_INHIBIT=fu1WheelRRErrDet;

	FL.WhlErrFlg=fu1WheelFLSusDet|fu1WheelFLErrDet;
	FR.WhlErrFlg=fu1WheelFRSusDet|fu1WheelFRErrDet;
	RL.WhlErrFlg=fu1WheelRLSusDet|fu1WheelRLErrDet;
	RR.WhlErrFlg=fu1WheelRRSusDet|fu1WheelRRErrDet;

	LSABS_vCalEachWheelSpeed(&FL,&FR,&RL,&RR);
	LSABS_vCalEachWheelSpeed(&FR,&FL,&RR,&RL);
	LSABS_vCalEachWheelSpeed(&RL,&RR,&FL,&FR);
	LSABS_vCalEachWheelSpeed(&RR,&RL,&FR,&FL);

	LSABS_vCalWheelAcceleration();

}
/*******************************************************************/
void	LSABS_vCalEachWheelSpeed(struct	W_STRUCT *WL_temp,struct W_STRUCT *WL_temp2,struct W_STRUCT	*WL_temp3,struct W_STRUCT *WL_temp4)
{
	WL=WL_temp;
	WL_Axle=WL_temp2;
	WL_Side=WL_temp3;
	WL_Diag=WL_temp4;

  #if defined(__MECU_MODE)
	WL->vdiff_resol_change=WL->lsabss16WhlSpdRawSignal64-WL->vrad_resol_change;
	WL->vrad_resol_change=WL->lsabss16WhlSpdRawSignal64;
	WL->vdiff=WL->vdiff_resol_change/SCALE_FACTOR;
	WL->vrad=WL->vrad_resol_change/SCALE_FACTOR;
  #else
	/*Filter the wheel speed sensor	signal*/
	LSABS_vFilterWheelspeedSignal();
  #endif
	/* calculate the wheel acceleration	*/
	LSABS_vFilterWheelSpeedGradient();
	/* Store the calculation result	wheel acceleration */
	LSABS_vStoreWheelSpeed();
}

/*******************************************************************/

void	LSABS_vCallBLS(void)
{
/* BSL SET/RESET <-	Main.c */
	LSABS_vDetBLS();
/* BRAKE SET/RESET <- LDABS_vImplementVref() */
	LSABS_vDetBrakeSignal();
}
/*******************************************************************/
void	LSABS_vDetBLS(void)
{
/* When	Braking	State fu1BSSignal=0	*/
	if(bls_k_timer != 0)
	{
		bls_k_timer--;
	}
	else
	{
		;
	}

	#if	__VDC
	if(ESP_bls_k_timer !=0)
	{
		ESP_bls_k_timer--;
	}
	else
	{
		;
	}
	#endif
	
	if(fu1BLSSignal==1)
	{
		BLS_A=0;
		bls_filter_timer=L_U8_TIME_10MSLOOP_50MS;
		BLS_K=0;
		if(BLS==0)
		{
			BLS=1;
			BLS_K=1;
			bls_k_timer=L_U8_TIME_10MSLOOP_50MS;
			#if	__VDC
			ESP_bls_k_timer=L_U8_TIME_10MSLOOP_1000MS;
			#endif

		}
		else
		{
			;
		}
	}
	else
	{
		BLS_K=0;
		if(bls_filter_timer	!= 0)
		{
			bls_filter_timer--;
			BLS_A=0;
		}
		else
		{
			if(BLS==1)
			{
				BLS=0;
				BLS_A=1;
			}
			else
			{
			   BLS_A=0;
			}
		}
	}

	#if	__VDC
	if(ESP_bs_k_timer !=0)
	{
		ESP_bs_k_timer--;
	}
	else{}
	#if	__BRAKE_SWITCH_ENABLE
	if(fu1BSSignal==0)
	{
		BS_A=0;
		BS_K=0;
		ESP_bs_filter_timer=L_U8_TIME_10MSLOOP_1000MS;
		if(ESP_BS==0)
		{
			ESP_BS=1;
			BS_K=1;
			ESP_bs_k_timer=L_U8_TIME_10MSLOOP_1000MS;
		}
		else
		{
			;
		}
	}
	else
	{
		BS_K=0;
		BS_A=0;
		if(ESP_bs_filter_timer != 0)
		{
			ESP_bs_filter_timer--;
		}
		else
		{
			if(ESP_BS==1)
			{
				ESP_BS=0;
				BS_A=1;
			}
			else
			{
				;
			}
		}
	}

	if((BLS_K==1)||(BS_K==1))
	{		/*start	BLS	ESP_BS Siganl SET 2003.10.06 KIM YONG KIL*/
		BLS_OR_ESP_BS=1;
	}
	else
	{
		;
	}

	if((BLS_A==1)||(BS_A==1))
	{
		BLS_OR_ESP_BS=0;
	}
	else
	{
		;
	}						/*end BLS ESP_BS Signal	SET	2003.10.06 KIM YONG	KIL*/
	#else	/*__BRAKE_SWITCH_ENABLE*/				
	if(BLS_K==1)
	{						/*In case that BS signal is	of no use.			*/
		BLS_OR_ESP_BS=1;	/*BLS_OR_ESP_BS	flag is	only set by	BLS	signal	*/
	}
	else
	{
		;
	}

	if(BLS_A==1	)
	{
		BLS_OR_ESP_BS=0;
	}
	else
	{
		;
	}							
	
	#endif	/*__BRAKE_SWITCH_ENABLE*/
	#endif
}
/*******************************************************************/

void	LSABS_vDetBrakeSignal(void)
{

  	#if __VDC
	#if	__BRK_SIG_MPS_TO_PEDAL_SEN	==1
	if(lsespu1BrkAppSenInvalid==0)
	#else
	if((fu1MCPErrorDet==0)&&(fu1MCPSusDet==0))
	#endif	
	{
		if((fu1BLSErrDet==0)&&(fu1BlsSusDet==0))
		{
			/*BLS && Mpress	Signal O.K*/
			/*Set Condition*/
			if(BRAKE_SIGNAL==0)
			{
				#if	__BRK_SIG_MPS_TO_PEDAL_SEN	==1
				if(lsespu1PedalBrakeOn==1)
				#else			
				if(MPRESS_BRAKE_ON==1)
				#endif					

				{
					if(BLS==1)
					{
						BRAKE_SIGNAL=1;
					}
					else
					{
						;
					}
				}
				else
				{
					BRAKE_SIGNAL=0;
				}
			}
			/*Reset	Condition*/
			else
			{
				#if	__BRK_SIG_MPS_TO_PEDAL_SEN	==1
				if((lsespu1PedalBrakeOn==0)||(BLS==0))
				#else		
				if((MPRESS_BRAKE_ON==0)||(BLS==0))
				#endif		

				{
					BRAKE_SIGNAL=0;

				}
				else
				{
					;
				}
			}
		}

		/*BLS Error	, MCP O.K*/
		else
		{
			#if	__BRK_SIG_MPS_TO_PEDAL_SEN	==1
			if((lsesps16EstBrkPressByBrkPedalF >MPRESS_10BAR)&&(lsespu1PedalBrakeOn==1))
			{
			   BRAKE_SIGNAL=1;
			}
			else if(lsespu1PedalBrakeOn==0)
			{
				BRAKE_SIGNAL=0;
			}			
			#else
			if((mpress>MPRESS_10BAR)&&(MPRESS_BRAKE_ON==1))
			{
			   BRAKE_SIGNAL=1;
			}
			else if(MPRESS_BRAKE_ON==0)
			{
				BRAKE_SIGNAL=0;
			}
			#endif
			else
			{
				;
			}
		}
	}
	else
	{
		/*MCP Error	, BLS O.K*/
		if((fu1BLSErrDet==0)&&(fu1BlsSusDet==0))
		{
			if (BLS==1)
			{
				BRAKE_SIGNAL=1;
			}
			else
			{
				BRAKE_SIGNAL=0;
			}
		}
		/*MCP Error	, BLS Error*/
		else
		{
			BRAKE_SIGNAL=0;
		}
	}

	if(ACTIVE_BRAKE_ACT==0)
	{
		#if	__HDC
		if(lcu1HdcActiveFlg==1)
		{
			ACTIVE_BRAKE_ACT=1;
		}
		#endif

		#if	__ACC
		if(lcu1AccActiveFlg==1)
		{
			ACTIVE_BRAKE_ACT=1;
		}
		#endif
		
		#if	__SCC
		if((lcu1SccBrkCtrlReq==1)&&(lcs16SccTargetPres > MPRESS_5BAR))
		{
			ACTIVE_BRAKE_ACT=1;
		}
		#endif

		#if	(__EPB_INTERFACE==ENABLE) && (EPB_VARIANT_ENABLE==ENABLE)
		if((wu8EpbVariantCodingEndFlag==1)&&((lcu8epbLdmstate==0)&&(lcu1EpbActiveFlg==1)))
		{
			ACTIVE_BRAKE_ACT=1;
		}
		#elif (__EPB_INTERFACE==ENABLE)
		if((lcu8epbLdmstate==0)&&(lcu1EpbActiveFlg==1))
		{
			ACTIVE_BRAKE_ACT=1;
		}
		#endif
		#if __CDM
		if(lcu8WpcActiveFlg==1){
			ACTIVE_BRAKE_ACT=1;
		}
		#endif
		#if (__MGH80_MOCi == ENABLE)
		if(lcu1DYNEPBControlOn==1){
			ACTIVE_BRAKE_ACT=1;
		}
		#endif
		#if __DEC
		if(lcu1DecActiveFlg==1){
			ACTIVE_BRAKE_ACT=1;
		}
		#endif
		
	}
	else
	{
		#if	__HDC
		if(lcu1HdcActiveFlg==0)
		{
		#endif

			#if	__ACC
			if(lcu1AccActiveFlg==0)
			{
			#endif
			
			#if	__SCC
			if(lcu1SccBrkCtrlReq==0)
			{
			#endif

			#if	(__EPB_INTERFACE==ENABLE)
			if(lcu1EpbActiveFlg==0)
			{
			#endif
						#if __CDM
						if(lcu8WpcActiveFlg==0)
						{
						#endif
							#if (__MGH80_MOCi == ENABLE)
							if(lcu1DYNEPBControlOn==0)
							{
							#endif	
								#if __DEC
								if(lcu1DecActiveFlg==0)
								{
								#endif	
				ACTIVE_BRAKE_ACT=0;
								#if __DEC
								}
								#endif
							#if (__MGH80_MOCi == ENABLE)
							}
							#endif
						#if __CDM
						}
						#endif

			#if	(__EPB_INTERFACE==ENABLE)
			}
			#endif
			
			#if	__SCC
			}
			#endif 
			
			#if	__ACC
			}
			#endif

		#if	__HDC
		}
		#endif
	}
  	#else	/*__VDC*/
	if((fu1BLSErrDet==0)&&(fu1BlsSusDet==0)&&(BLS==1))
	{
		BRAKE_SIGNAL=1;
	}
	else
	{
		BRAKE_SIGNAL=0;
	}
  	#endif	/*__VDC*/

  /*---------------------------
  Braking State	Flag Set/Reset
  ----------------------------*/
	if(ldu1VehicleStopStateFlg==0){
		if((vref_resol_change<=VREF_1_KPH_RESOL_CHANGE)
			&&(FL.vrad_crt_resol_change<=VREF_1_KPH_RESOL_CHANGE)
			&&(FR.vrad_crt_resol_change<=VREF_1_KPH_RESOL_CHANGE)
			&&(RL.vrad_crt_resol_change<=VREF_1_KPH_RESOL_CHANGE)
			&&(RR.vrad_crt_resol_change<=VREF_1_KPH_RESOL_CHANGE)){
				brk_st_reset_cnt++;
				if(brk_st_reset_cnt>L_U8_TIME_10MSLOOP_1000MS){
					ldu1VehicleStopStateFlg=1;
					brk_st_reset_cnt=0;
				}
				
		}
		else if(brk_st_reset_cnt>0)
		{
			brk_st_reset_cnt--;
		}  
		else{
			;
		}
	}
	else{
		if((vref_resol_change>VREF_3_KPH_RESOL_CHANGE)
			||(FL.vrad_crt_resol_change>VREF_3_KPH_RESOL_CHANGE)
			||(FR.vrad_crt_resol_change>VREF_3_KPH_RESOL_CHANGE)
			||(RL.vrad_crt_resol_change>VREF_3_KPH_RESOL_CHANGE)
			||(RR.vrad_crt_resol_change>VREF_3_KPH_RESOL_CHANGE)){
			ldu1VehicleStopStateFlg=0;
			brk_st_reset_cnt=0;
		}
		else{}
	}  
		
	tempF0=0;
	BRAKING_STATE_OLD=BRAKING_STATE;
	if(BRAKING_STATE==0)
	{
		#if	__VDC
		if(((BRAKE_SIGNAL==1)||(ACTIVE_BRAKE_ACT==1))&&(ABS_fz==1))	/* 2010.09.30 ACTIVE_BRAKE_ACT + ABS Concept Add */
		{
			tempF0=1;
		}
		else if(BRAKE_SIGNAL==1) 
		{		
			if((FL.vrad_crt_resol_change<ABS_entry_speed)&&(FR.vrad_crt_resol_change<ABS_entry_speed)
			&&(RL.vrad_crt_resol_change<ABS_entry_speed)&&(RR.vrad_crt_resol_change<ABS_entry_speed))

			{
				brk_st_det_cnt++;
				if(brk_st_det_cnt>100){
					brk_st_det_cnt=100;
				}
			}
			else 
			{
				brk_st_det_cnt=0;
			}
			if((mtp<10)&&(eng_torq_rel<TORQ_PERCENT_40PRO))
			{
				if(brk_st_det_cnt>L_U8_TIME_10MSLOOP_70MS)
				{
					tempF0=1;
				}
				else if((EBD_rr==1)&&(EBD_rl==1))
				{
					tempF0=1;				
				}
				else if((rel_lam_fl<3)&&(rel_lam_fr<3)&&(rel_lam_rl<3)&&(rel_lam_rr<3)
					&&(arad_fl<0)&&(arad_fr<0)&&(arad_rl<0)&&(arad_rr<0))
				{
					tempF0=1;
				}
				else
				{
					;
				}			
			}
		}
		else if((ABS_fz==1)&&(BRAKE_SIGNAL==0))
		{
			if((fu1MCPErrorDet==1)||(fu1MCPSusDet==1)||(fu1BLSErrDet==1)||(fu1BlsSusDet==1))
			{
				if((rel_lam_fl<=0)&&(rel_lam_fr<=0)&&(rel_lam_rl<=0)&&(rel_lam_rr<=0))
				{
					brk_st_det_cnt++;
				}
				else{}
			}
			else
			{
				if((rel_lam_fl<=-LAM_25P)&&(rel_lam_fr<=-LAM_25P)&&(rel_lam_rl<=-LAM_25P)&&(rel_lam_rr<=-LAM_25P))
				{
					#if __AX_SENSOR
					if(ACCEL_SPIN_FADE_OUT_FLAG==0){
					#endif
                		brk_st_det_cnt++;
                	#if __AX_SENSOR
                	}
                	#endif
				}
				else{}
			}	 
				
			if(brk_st_det_cnt>L_U8_TIME_10MSLOOP_70MS)
			{
				tempF0=1;
			}
			
		}		
		else
		{
			brk_st_det_cnt=0;
		}
		if((tempF0==1)&&(ldu1VehicleStopStateFlg==0))	 
		{
			BRAKING_STATE=1;
			brk_st_det_cnt=0;
		}
		#else	/*__VDC*/
		if((BRAKE_SIGNAL==1)&&(ABS_fz==1))
		{
			BRAKING_STATE=1;
		}
		else if	(ABS_fz==1)
		{
			if((rel_lam_fl<=0)&&(rel_lam_fr<=0)&&(rel_lam_rl<=0)&&(rel_lam_rr<=0))
			{
				BRAKING_STATE=1;
			}
		}

		else
		{
			;
		}
		#endif	/*__VDC*/
		
		ldabsu8BrakingStateResetTimer=0;
	}
	else
	{		
		if(ABS_fz == 0)
		{
			if(ldu1VehicleStopStateFlg == 0)
			{
				if(BRAKE_SIGNAL==0)
				{
					BRAKING_STATE =0;	
					ldabsu8BrakingStateResetTimer =0;
				}
				else
				{
					if(ebd_refilt_grv >=AFZ_0G03)
					{
						ldabsu8BrakingStateResetTimer =	ldabsu8BrakingStateResetTimer +	1 ;
					}
					else if((ebd_refilt_grv>=-AFZ_0G01)	&&(ebd_refilt_grv<AFZ_0G03)	)
					{
						;
					}
					else
					{
						ldabsu8BrakingStateResetTimer=0;
					}
						
					if(ldabsu8BrakingStateResetTimer>=L_U8_TIME_10MSLOOP_200MS)
					{
						BRAKING_STATE=0;
						ldabsu8BrakingStateResetTimer=0;
					}
					else
					{
						;
					}							
				}				
			}
			else
			{
				BRAKING_STATE =0;
				ldabsu8BrakingStateResetTimer=0;	
			}			
		}
		else
		{
			ldabsu8BrakingStateResetTimer=0;
		}		
		#if	__VDC
		brk_st_det_cnt=0;
		#endif
	}
}
/*******************************************************************/
#if	__AX_SENSOR
void	LSABS_vFilterLongAccSignal(void)
{
	int8_t	Multiple;
	/*
	int8_t	ldu1Gear_Known;
	int8_t	ldu1gear_R;
	int8_t	ldu1gear_D;
	int8_t	ldu1gear_N_P;
	*/

	Multiple=10;

	if((speed_calc_timer < (uint8_t)L_U8_TIME_10MSLOOP_1000MS)
  		#if (HSA_VARIANT_ENABLE==ENABLE)
		||(wu8HsaVariantCodingEndFlag==0)
  		#endif
	)			
	{
	
		along=0;
		along_old=0;
		ax_Filter=0;
		ax_Filter_old=0;
		ax_Filter_temp=0;


		lss16absAx1000gtempOLD=0;
		lss16absAx1000gtemp=0;
		lss16absAx1000g=0;
		lss16absAx1000gDiffOLD=0;
		lss16absAx1000gDiff=0;
		lss16absAxOffsetComp1000g=0;

		lsabss16FilteredAxSignalOld=0;
		lsabss16FilteredAxSignal=0;
		lsabss16FilteredAxSignalTemp=0;
		
		lsabss16RawAxSignal=0;
		lsabss16RawAxSignalOld=0;
		lsabss16RawAxSignalTemp=0;
	}
	else
	{
		lsabss16RawAxSignalOld=lsabss16RawAxSignal;
		  
		#if __G_SENSOR_TYPE==CAN_TYPE
		if(cbit_process_step!=1)
		{
			lsabss16RawAxSignal=a_long_1_1000g;
		}
		else
		{
			lsabss16RawAxSignal=lsabss16RawAxSignalOld;
		}
		#else
		lsabss16RawAxSignal=a_long_1_1000g;
		#endif
		  
		  
		lss16absAx1000gtempOLD=lss16absAx1000gtemp;
		lss16absAx1000gtemp=LCESP_s16Lpf1Int(lsabss16RawAxSignal*10,lss16absAx1000gtempOLD,L_U8FILTER_GAIN_10MSLOOP_3_5HZ);
		lss16absAx1000g=lss16absAx1000gtemp/10 ;

		lss16absAx1000gDiffOLD=lss16absAx1000gDiff;
	  	lss16absAx1000gDiff=LCESP_s16Lpf1Int((lss16absAx1000gtemp-lss16absAx1000gtempOLD),lss16absAx1000gDiffOLD,L_U8FILTER_GAIN_10MSLOOP_7HZ);

		#if	__Ax_OFFSET_APPLY
		lss16absAxOffsetComp1000g=lss16absAx1000g-lsabss16AxOffsetTOTf;
		#else
		lss16absAxOffsetComp1000g=lss16absAx1000g;
		#endif

		#if	__Ax_OFFSET_APPLY
		lsabss16RawAxSignalTemp=lsabss16RawAxSignal-lsabss16AxOffsetTOTf;
		#else
		lsabss16RawAxSignalTemp=lsabss16RawAxSignal;
		#endif
		#if __SPIN_DET_IMPROVE == ENABLE
		if(gs_pos == GEAR_POS_TM_N)
		{
			if(vref_resol_change <= VREF_0G125_KPH_RESOL_CHANGE)
			{
				ldu1ngearstop = 1;
			}
			if((ldu1ngearstop == 1)&&(lds16vehgradientG > 5))
			{
				if(vref_resol_change > VREF_0_375_KPH_RESOL_CHANGE)
				{
					ldu1ngeardrv = 1;
				}
			}
		}
		else
		{
			ldu1ngearstop = 0;
			ldu1ngeardrv = 0;
		}
		#endif
		#if	__TCS || __ETC || __EDC

		if(ldu1Gear_Known==1)
		{
			if(R_GEAR_DET==0)
			{
				if(ldu1gear_R==1)
				{
					R_GEAR_DET=1;
				}
			}
			else if(ldu1gear_D==1)
			{
				R_GEAR_DET=0;
			}
			else
			{
				;
			}

			if((R_GEAR_DET == 1)
			#if __SPIN_DET_IMPROVE == ENABLE
			||(ldu1ngeardrv == 1)
			#endif
			)
			{
				lsabss16RawAxSignalTemp=(-lsabss16RawAxSignal);
			}

		}
		else
		{
		    
			ldu1Gear_Known=0;
			R_GEAR_DET=0;
			if((BACKWARD_MOVE==1)
			#if __SPIN_DET_IMPROVE == ENABLE
			||(ldu1ngeardrv == 1)
			#endif
			)
			{
				lsabss16RawAxSignalTemp=(-lsabss16RawAxSignal);
			}
			else
			{
				;
			}
		}

		#else
		/*
		ldu1Gear_Known=0;
		R_GEAR_DET=0;
		*/
		if(BACKWARD_MOVE==1)
		{
			lsabss16RawAxSignalTemp=(-lsabss16RawAxSignal);
		}
		#endif




		lsabss16FilteredAxSignalOld=lsabss16FilteredAxSignal;
		tempW3=lsabss16RawAxSignalTemp;
		lsabss16FilteredAxSignalTemp=LCESP_s16Lpf1Int(Multiple*tempW3,lsabss16FilteredAxSignalTemp,L_U8FILTER_GAIN_10MSLOOP_3_5HZ);
		lsabss16FilteredAxSignal=lsabss16FilteredAxSignalTemp/Multiple;
		
		#if	_ESCi_ENA==ENABLE
			/*Old Raw Signal*/
			lsabss16FilteredAxSignalOld_i = lsabss16FilteredAxSignal_i;
			if(BACK_DIR==1)
			{
				tempW3 = (int16_t)(-s16ESCi_AxSig);
			}
			else
			{
				tempW3 = (int16_t)(s16ESCi_AxSig);
			}
			tempW3 = tempW3-iax_offset_f;
			if(ADVANCED_MSC_ON_FLG==1)
			{
				lsabss16FilteredAxSignalTemp_i = LCESP_s16Lpf1Int(Multiple*tempW3,lsabss16FilteredAxSignalTemp_i,L_U8FILTER_GAIN_10MSLOOP_1HZ);
			}
			else
			{
				lsabss16FilteredAxSignalTemp_i = LCESP_s16Lpf1Int(Multiple*tempW3,lsabss16FilteredAxSignalTemp_i,L_U8FILTER_GAIN_10MSLOOP_3HZ);
			}
			lsabss16FilteredAxSignal_i = lsabss16FilteredAxSignalTemp_i/Multiple;
			/********************************************************************/
			
			/*Filter 2 Step Avg*/
			lsabss16FilteredAxSignalOld_i2 = lsabss16FilteredAxSignal_i2;
			if(BACK_DIR==1)
			{
				tempW3 = (int16_t)(-s16ESCi_AxSigFil);
			}
			else
			{
				tempW3 = (int16_t)(s16ESCi_AxSigFil);
			}
			tempW3 = (int16_t)(tempW3-iax_offset_f);
			if(ADVANCED_MSC_ON_FLG==1)
			{
				lsabss16FilteredAxSignalTemp_i2 = LCESP_s16Lpf1Int(Multiple*tempW3,lsabss16FilteredAxSignalTemp_i2,L_U8FILTER_GAIN_10MSLOOP_1HZ);
			}
			else
			{
				lsabss16FilteredAxSignalTemp_i2 = LCESP_s16Lpf1Int(Multiple*tempW3,lsabss16FilteredAxSignalTemp_i2,L_U8FILTER_GAIN_10MSLOOP_3HZ);
			}
			lsabss16FilteredAxSignal_i2 = lsabss16FilteredAxSignalTemp_i2/Multiple;
			/********************************************************************/

			/*New Filtered Signal*/
			lsabss16FilteredAxSignalOld_iN = lsabss16FilteredAxSignal_iN;

			tempW2 = (int16_t)(s16ESCi_AxSig2 - iax_offset_f2);
			tempW3 = (int16_t)(s16ESCi_AySig2 - ialat_offset_f);
			iax_RotationAngleCrt = (int16_t)( ( ((int32_t)tempW2*ROT_ANGLE_COS) - ((int32_t)tempW3*ROT_ANGLE_SIN) )/1000 );
			tempW3 = (int16_t)(iax_RotationAngleCrt-iax_offset_f);

			lsabss16FilteredAxSignalTemp_iN = LCESP_s16Lpf1Int(Multiple*tempW3,lsabss16FilteredAxSignalTemp_iN,L_U8FILTER_GAIN_10MSLOOP_4HZ);

			lsesps16ESCiYawAccelCompAxN = (int16_t)( ((int32_t)ESCi_P_COMP_GAIN_AX*lsesps16ESCiYawAccelN*DISTANCE_ALONG_Yaxis)/(1000) ); //[1000,g]

			lsabss16FilteredAxSignal_iN = (lsabss16FilteredAxSignalTemp_iN/Multiple) - lsesps16ESCiYawAccelCompAxN;
			/********************************************************************/
			#if	 (__ESCi_CONTROL==ENABLE)
				ax_Filter_old = ax_Filter;
				along_old = along;
				if(BACK_DIR==1)
				{
					ax_Filter = (int16_t)(-lsabss16FilteredAxSignal_iN/Multiple);
					along = (int16_t)(-lsabss16FilteredAxSignal_iN/Multiple);
				}
				else
				{
					ax_Filter = (int16_t)(lsabss16FilteredAxSignal_iN/Multiple);
					along = (int16_t)(lsabss16FilteredAxSignal_iN/Multiple);
				}

				lsabss16RawAxSignal = lsabss16FilteredAxSignal_iN;
				lss16absAx1000gDiffOLD_i = lss16absAx1000gDiff;
		  		lss16absAx1000gDiff = LCESP_s16Lpf1Int( (lsabss16FilteredAxSignal_iN-lsabss16FilteredAxSignalOld_iN),lss16absAx1000gDiffOLD_i,L_U8FILTER_GAIN_10MSLOOP_7HZ );

				lss16absAx1000g = lsabss16FilteredAxSignal_iN;
				lss16absAxOffsetComp1000g = lsabss16FilteredAxSignal_iN;
	   		#else
		ax_Filter_old=ax_Filter;
		ax_Filter=lsabss16FilteredAxSignal/Multiple;
		along_old=along;
		along=lsabss16FilteredAxSignal/Multiple;
			#endif
		#else
		ax_Filter_old=ax_Filter;
		ax_Filter=lsabss16FilteredAxSignal/Multiple;
		along_old=along;
		along=lsabss16FilteredAxSignal/Multiple;
		#endif
		
		#if	(__Ax_OFFSET_MON==1)||(__Ax_OFFSET_MON_DRV==1 )
		LSABS_vEstLongAccSensorOffsetMon();
		#endif
		

	}
}
#endif	/*__AX_SENSOR */
/*******************************************************************/
void	LSABS_vFilterWheelspeedSignal(void)
{
	if(WL->WhlErrFlg==1)
	{
		if(WL_Axle->WhlErrFlg==0)
		{
			WL->vrad_resol_change=WL_Axle->vrad_resol_change;
			WL->vdiff_resol_change=WL_Axle->vdiff_resol_change;
			WL->vrad=WL_Axle->vrad;
			WL->vdiff=WL_Axle->vdiff;
		}
		else if(WL_Side->WhlErrFlg==0)
		{
			if(WL_Diag->WhlErrFlg==0)
			{
				tempW0=(WL_Side->vrad_resol_change+WL_Diag->vrad_resol_change)/2;
				WL->vdiff_resol_change=tempW0-WL->vrad_resol_change;
				WL->vrad_resol_change=tempW0;
				WL->vdiff=WL->vdiff_resol_change/SCALE_FACTOR;
				WL->vrad=WL->vrad_resol_change/SCALE_FACTOR;
			}
			else
			{
				WL->vrad_resol_change=WL_Side->vrad_resol_change;
				WL->vdiff_resol_change=WL_Side->vdiff_resol_change;
				WL->vrad=WL_Side->vrad;
				WL->vdiff=WL_Side->vdiff;
			}
		}
		else if(WL_Diag->WhlErrFlg==0)
		{
				WL->vrad_resol_change=WL_Diag->vrad_resol_change;
				WL->vdiff_resol_change=WL_Diag->vdiff_resol_change;
				WL->vrad=WL_Diag->vrad;
				WL->vdiff=WL_Diag->vdiff;
		}
		else
		{
			tempW0=(WL_Side->vrad_resol_change+WL_Diag->vrad_resol_change)/2;
			WL->vdiff_resol_change=tempW0-WL->vrad_resol_change;
			WL->vrad_resol_change=tempW0;
			WL->vdiff=WL->vdiff_resol_change/SCALE_FACTOR;
			WL->vrad=WL->vrad_resol_change/SCALE_FACTOR;
		}
	}
	else
	{
		tempW0 = FL.SPIN_WHEEL + FR.SPIN_WHEEL + RL.SPIN_WHEEL + RR.SPIN_WHEEL;
		if((WL->SPIN_WHEEL==1) && (tempW0<=3))
		{
			WL->vdiff_resol_change=vref_resol_change-WL->vrad_resol_change;
			WL->vrad_resol_change=vref_resol_change;
			WL->vdiff=WL->vdiff_resol_change/SCALE_FACTOR;
			WL->vrad=WL->vrad_resol_change/SCALE_FACTOR;
		}
		else
		{
			tempW5=WL->lsabss16WhlSpdRawSignal64;				  /* low limit of v_ungef */
			if(tempW5 <	(int16_t)VREF_MIN_SPEED)
			{
				tempW5=VREF_MIN_SPEED;
			}
			else
			{
				;
			}
			tempW6=tempW5-WL->vrad_resol_change;

			tempF0=0;							/* detect and limit	large speed	variation */
			if(tempW6 >= (int16_t)MAX_VRAD_DIFF)
			{
				tempW6=MAX_VRAD_DIFF;
				tempF0=1;
			}
			else if(tempW6 <= (int16_t)(-MAX_VRAD_DIFF))
			{
				tempW6=-MAX_VRAD_DIFF;
				tempF0=1;
			}
			else
			{
				;
			}
 /*	set	V_STOER	- Not Used
			if(tempF0==1)
			{					 
				if(V_STOER_wl==0)
				{
					V_STOER_wl=1;
				}
				else
				{
					tempF0=0;
				}
			}
			else
			{
				V_STOER_wl=0;
			}
*/
			HEAVY_FILT_wl=0;					/* select heavy	or normal filter */
			if(tempF0==0)
			{
				if(INCREASE_VDIFF_wl==0)
				{
					if(tempW6 >	WL->vdiff_resol_change)
					{
						INCREASE_VDIFF_wl=1;
						HEAVY_FILT_wl=1;
					}
					else
					{
						;
					}

				}
				else
				{
					if(tempW6 <	WL->vdiff_resol_change)
					{
						INCREASE_VDIFF_wl=0;
						HEAVY_FILT_wl=1;
					}
					else
					{
						;
					}
				}
			}
			else
			{
				;
			}

			if(HEAVY_FILT_wl==1)
			{			   
				/* heavy	filtering */
				tempW2=(tempW6*8)+(WL->vdiff_resol_change*2)+WL->vdiff_rest; //10ms
				tempW0=13;
				tempW1=1;
			}
			else
			{	
				/* normal filtering */
				tempW2=(tempW6*11)+(WL->vdiff_resol_change*1)+WL->vdiff_rest;
				tempW0=13;
				tempW1=1;								
			}

			s16muls16divs16();
			WL->vdiff_resol_change=tempW3;
			WL->vdiff_rest=tempW2-tempW3*tempW0;
			tempW3 += WL->vrad_resol_change;
/*			
			if(tempW3 <= (int16_t)VREF_MIN_SPEED)
			{
				tempW3=VREF_MIN_SPEED;
				SPEEDMIN_wl=1;
			}
			else
			{
				if(tempW3 >	(int16_t)VREF_MAX_RESOL_CHANGE)
				{
					tempW3=VREF_MAX_RESOL_CHANGE;
				}
				else
				{
					;
				}

				SPEEDMIN_wl=0;
			}
*/
			WL->vrad_resol_change=LCABS_s16LimitMinMax(tempW3,VREF_MIN_SPEED,VREF_MAX_RESOL_CHANGE);

			tempW0=(WL->vrad_resol_change)/SCALE_FACTOR; //tempW0=(WL->vrad_resol_change)>>3;
/*			
			if ( tempW0	<= VREF_MIN_SPEED_RESOL_8 )
			{
				tempW0=VREF_MIN_SPEED_RESOL_8;
			}
			else
			{
				;
			}
*/
			tempW0=LCABS_s16LimitMinMax(tempW0,VREF_MIN_SPEED_RESOL_8,VREF_MAX);
			WL->vdiff=tempW0-WL->vrad;
			WL->vrad=tempW0;
		}
	}
}
/*******************************************************************/
void	LSABS_vFilterWheelSpeedGradient(void)
{
	#if	 __ARAD_RESOL_CHANGE
	WL->arad_alt_resol_change=WL->arad_resol_change;	  
	tempW6 =(int32_t)(WL->vdiff_resol_change);
	tempW3 =(int32_t)(WL->arad_resol_change);
	tempL0=(int32_t)tempW6*20+(int32_t)tempW3*7;
	  	  
	if(tempL0	> 32767	)
	{
		tempL0=32767;
	}
	else if(tempL0 < -32768 )
	{
		tempL0=-32768;
	}
	else
	{
		;	
	}

	WL->arad_resol_change=(int16_t)(tempL0/17);

	tempW1=LCABS_s16LimitMinMax(WL->arad_resol_change,-1016,1016);
	 
	WL->arad_alt= WL->arad;
	WL->arad=(int8_t)(tempW1/8);		 
	 
	#else
	WL->arad_alt=WL->arad;
	/*tempW1=WL->vdiff+WL->vdiff+(int16_t)WL->arad;*/
	tempW6=WL->vdiff;   /*5ms*/
	tempW3=(int16_t)(WL->arad);
	tempW1=tempW6*20+tempW3*7;
	/*
	if(tempW1	> (int16_t)255)
	{
		tempW1=255;
	}
	else if(tempW1 < (int16_t)(-256))
	{
		tempW1=-256;
	}
	else
	{
		;
	}
	*/
	tempW1=LCABS_s16LimitMinMax(tempW1,-255*6,255*6);
	/*		
	tempW0=2;
	tempW2=1;
	s16muls16divs16();
	WL->arad=tempW3;
	*/
	/*WL->arad=(int8_t)(tempW1/2);*//*7ms*/
	WL->arad=(int8_t)(tempW1/17); /*5ms*/
	#endif


}
/*******************************************************************/
void LSABS_vCalWheelAcceleration(void)
{
	if(speed_calc_timer	<= (uint8_t)L_U8_TIME_10MSLOOP_200MS)
	{
		arad_timer=0;
		arad_avg_counter=0;
	}
	else
	{
		if(arad_timer>=2)
		{
			arad_timer=0;
		}
		else if(arad_timer==1)
		{
			arad_avg_counter++;
			if(arad_avg_counter==5)
			{
				arad_avg_counter=0;
			}
			else
			{
				;
			}
		}
		else
		{
			;
		}

		arad_timer++;
	}
	LSABS_vCalEachWheelAcceleration(&FL);
	LSABS_vCalEachWheelAcceleration(&FR);
	LSABS_vCalEachWheelAcceleration(&RL);
	LSABS_vCalEachWheelAcceleration(&RR);
}
/*******************************************************************/
void LSABS_vCalEachWheelAcceleration(struct	W_STRUCT *WL)
{

	if(speed_calc_timer	<= (uint8_t)L_U8_TIME_10MSLOOP_200MS)
	{
		WL->vrad0=0;
		WL->arad_temp[0]=WL->arad_temp[1]=WL->arad_temp[2]=WL->arad_temp[3]=WL->arad_temp[4]=WL->arad_avg=0;
	}
	else
	{

		WL->vrad0+=WL->vdiff_resol_change;

		if(arad_timer==2)
		{
			;
		}
		else if(arad_timer==1)
		{
/*
			if(arad_avg_counter==1)
			{
				WL->arad10=WL->vrad0;
			}
			else if(arad_avg_counter==2)
			{
				WL->arad20=WL->vrad0;
			}
			else if(arad_avg_counter==3)
			{
				WL->arad30=WL->vrad0;
			}
			else if(arad_avg_counter==4)
			{
				WL->arad40=WL->vrad0;
			}
			else
			{
				WL->arad50=WL->vrad0;
			}
*/
			WL->arad_temp[arad_avg_counter]=WL->vrad0;
			WL->vrad0=0;

			tempW2=(WL->arad_temp[0]+WL->arad_temp[1]+WL->arad_temp[2]+WL->arad_temp[3]+WL->arad_temp[4]);
			tempW0=16;
			tempW1=7;

			s16muls16divs16();
/*
			tempW1=WL->arad_avg;
			tempW0=tempW3;
*/
/*			LFC_filter_7Hz();  */
/*			tempW2 = LCESP_s16Lpf1Int(tempW0, tempW1, 39);*/
			WL->arad_avg=LCESP_s16Lpf1Int(tempW3,WL->arad_avg,L_U8FILTER_GAIN_10MSLOOP_10HZ); /*10hz*/

		}
		else
		{
			;
		}
	}

}
/*******************************************************************/
void	LSABS_vStoreWheelSpeed(void)
{

	tempW3=WL->speed;
	WL->speed=WL->vrad;
/*
	if(WL->s_diff >	VREF_15_KPH) {WL->s_diff_old = VREF_15_KPH;}
	else if(WL->s_diff < -VREF_15_KPH) {WL->s_diff_old = -VREF_15_KPH;}
	else {WL->s_diff_old = (int8_t)WL->s_diff;}
*/
	tempW0=WL->s_diff;
	WL->s_diff_old=LCABS_s16LimitMinMax(tempW0,-VREF_21_KPH,VREF_21_KPH);
	WL->s_diff=WL->speed-tempW3;
}
#if	__TCS || __ETC || __EDC
void	LSABS_vCallGearInfSignal(void)
{
	if(tcs_error_flg==0)
	{
		if(AUTO_TM==1)
		{
			ldu1Gear_Known=1;
		}
		else
		{
			ldu1Gear_Known=0;
		}
	}
	else
	{
		ldu1Gear_Known=0;
	}
	if(gs_pos == GEAR_POS_TM_R)
	{
	    ldu1gear_R=1;
		ldu1gear_D=0;
		ldu1gear_N_P=0;
	}
	else if((gs_pos == GEAR_POS_TM_P)||(gs_pos == GEAR_POS_TM_N))
	{
	    ldu1gear_R=0;
		ldu1gear_D=0;
		ldu1gear_N_P=1;
	}
	else 
	{
	    ldu1gear_R=0;
		ldu1gear_D=1;
		ldu1gear_N_P=0;	
	}

}
#endif
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LSABSCallSensorSignal
	#include "Mdyn_autosar.h"
#endif
