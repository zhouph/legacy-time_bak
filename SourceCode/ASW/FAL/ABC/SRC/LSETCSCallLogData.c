/* AUTOSAR --------------------------*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LSETCSCallLogData
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/

/* Includes ******************************************************************/
#include "LVarHead.h"
#include "LCTCSCallControl.h"
#include "LCHSACallControl.h"
#include "LDESPEstDiscTemp.h"
#include "LAABSCallMotorSpeedControl.h"
#include "LSETCSCallLogData.h"
#include "LDESPEstBeta.h"
#include "ETCSGenCode/WhlSpinCoorr_struct_types.h"
#include "ETCSGenCode/LCETCS_vCallMain.h"

/* Local Definiton  **********************************************************/

/* Variables Definition*******************************************************/
extern int16_t	rta_ratio_temp;	/* MBD ETCS */

/* LocalFunction prototype ***************************************************/
static uint16_t LSBTCS_u16PackData12_1_1_1_1(uint16_t Variable2ReduceSize, uint16_t Flag1, uint16_t Flag2, uint16_t Flag3, uint16_t Flag4);
static uint16_t LSBTCS_u16PackData12_2_2(uint16_t Variable2ReduceSize, uint16_t Flag1, uint16_t Flag2);
static uint16_t LSBTCS_u16PackData14_1_1(uint16_t Variable2ReduceSize, uint16_t Flag1, uint16_t Flag2);
static uint8_t LSBTCS_u8PackData4_4(uint8_t data1, uint8_t data2);
static uint8_t LSBTCS_u8PackFlags(uint8_t Flag1, uint8_t Flag2, uint8_t Flag3, uint8_t Flag4, uint8_t Flag5, uint8_t Flag6, uint8_t Flag7, uint8_t Flag8);

/* Implementation*************************************************************/
#if defined(__LOGGER) && (__LOGGER_DATA_TYPE==1)
void MBDedETCS_Log(void)
{
	int16_t   LocalTemp01=0;
	int16_t   LocalTemp02=0;
	int16_t   LocalTemp03=0;
	uint8_t 	u8TempMem4Log=0;
	uint16_t 	u16TempMem4Log=0;
	int16_t 	s16TempMem4Log=0;
	uint8_t 	Reserved1Bit=0;
	uint8_t 	Reserved2Bit=0;
	uint8_t 	Reserved8Bit=0;
	uint16_t 	Reserved16Bit=0;
	
	/* Byte 0 */
	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(FL.vrad_crt, lctcsu1GearShiftFlag, ETCS_ENG_STALL.lcetcsu1EngStall/*v1.9*/, lctcsu1RoughRoad, FLAG_BANK_DETECTED);
	CAN_LOG_DATA[0] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[1] =	(uint8_t) (u16TempMem4Log>>8);	
	
	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(FR.vrad_crt, lctcsu1WhlSpdCompByRTADetect, lctcsu1InvalidWhlSpdBfRTADetect, ETCS_CTL_ACT.lcetcsu1CtlAct, ETCS_CYL_1ST.lcetcsu1Cyl1st);
	CAN_LOG_DATA[2] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[3] =	(uint8_t) (u16TempMem4Log>>8);
	
	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(RL.vrad_crt, ETCS_ERR_ZRO_CRS.lcetcsu1ErrZroCrs, ETCS_ERR_ZRO_CRS.lcetcsu1JumpUp, ETCS_ERR_ZRO_CRS.lcetcsu1JumpDn, ETCS_IVAL_RST.lcetcsu1RstIValAt2ndCylStrt);
	CAN_LOG_DATA[4] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[5] =	(uint8_t) (u16TempMem4Log>>8);
	
	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(RR.vrad_crt, ETCS_IVAL_RST.lcetcsu1RstIValAtErrZroCrs, ETCS_IVAL_RST.lcetcsu1RstIVal, AUTO_TM, ETCS_H2L.lcetcsu1HiToLw);
	CAN_LOG_DATA[6] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[7] =	(uint8_t) (u16TempMem4Log>>8);
	
	/* Byte 1 */
	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(lctcss16VehSpd4TCS, TCL_DEMAND_PRIMARY, TCL_DEMAND_SECONDARY, S_VALVE_PRIMARY, S_VALVE_SECONDARY);
	CAN_LOG_DATA[8] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[9] =	(uint8_t) (u16TempMem4Log>>8);
	
	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(FL_TCS.ltcss16MovingAvgWhlSpdTCMF, HV_VL_fl, AV_VL_fl, FL_TCS.lctcsu1BTCSWhlAtv, FL_TCS.lctcsu1VIBFlag_tcmf);
	CAN_LOG_DATA[10] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[11] =	(uint8_t) (u16TempMem4Log>>8);
	
	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(FR_TCS.ltcss16MovingAvgWhlSpdTCMF, HV_VL_fr, AV_VL_fr, FR_TCS.lctcsu1BTCSWhlAtv, FR_TCS.lctcsu1VIBFlag_tcmf);
	CAN_LOG_DATA[12] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[13] =	(uint8_t) (u16TempMem4Log>>8);
	
	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(RL_TCS.ltcss16MovingAvgWhlSpdTCMF, HV_VL_rl, AV_VL_rl, RL_TCS.lctcsu1BTCSWhlAtv, RL_TCS.lctcsu1VIBFlag_tcmf);
	CAN_LOG_DATA[14] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[15] =	(uint8_t) (u16TempMem4Log>>8);
	
	/* Byte 2 */
	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(RR_TCS.ltcss16MovingAvgWhlSpdTCMF, HV_VL_rr, AV_VL_rr, RR_TCS.lctcsu1BTCSWhlAtv, RR_TCS.lctcsu1VIBFlag_tcmf);
	CAN_LOG_DATA[16] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[17] =	(uint8_t) (u16TempMem4Log>>8);	

	u16TempMem4Log = LCTCS_s16IFindMaximum(FA_TCS.lctcss16EstBrkTorqOnLowMuWhl, RA_TCS.lctcss16EstBrkTorqOnLowMuWhl);
	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(u16TempMem4Log, FA_TCS.lctcsu1SymSpinDctFlg, FA_TCS.lctcsu1FFAdaptByHghMuWhlUnstab, FA_TCS.lctcsu1FlgOfAfterHghMuUnstb, FA_TCS.lctcsu1BrkCtlFadeOutModeFlag);	
	CAN_LOG_DATA[18] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[19] =	(uint8_t) (u16TempMem4Log>>8);

	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(rta_ratio_temp/*v1.9*/, RA_TCS.lctcsu1SymSpinDctFlg, RA_TCS.lctcsu1FFAdaptByHghMuWhlUnstab, RA_TCS.lctcsu1FlgOfAfterHghMuUnstb, RA_TCS.lctcsu1BrkCtlFadeOutModeFlag);
	CAN_LOG_DATA[20] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[21] =	(uint8_t) (u16TempMem4Log>>8);	

	u16TempMem4Log = LSBTCS_u16PackData12_2_2(gs_sel/*v1.9*/, FA_TCS.lctcsu8WhlOnHghMu, FA_TCS.lctcsu8StatWhlOnHghMu);
	CAN_LOG_DATA[22] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[23] =	(uint8_t) (u16TempMem4Log>>8);		

	/* Byte 3 */
	u16TempMem4Log = LSBTCS_u16PackData12_2_2(Beta_K_Dot/*v2.0*/, RA_TCS.lctcsu8WhlOnHghMu, FA_TCS.lctcsu8StatWhlOnHghMu);
	CAN_LOG_DATA[24] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[25] =	(uint8_t) (u16TempMem4Log>>8);		

	u16TempMem4Log = LSBTCS_u16PackData14_1_1(drive_torq/*v1.9*/, fu1ESCDisabledBySW/*v1.9*/, fu1TCSDisabledBySW/*v1.9*/);
	CAN_LOG_DATA[26] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[27] =	(uint8_t) (u16TempMem4Log>>8);

	u16TempMem4Log = LSBTCS_u16PackData14_1_1(eng_torq/*v1.9*/, lctcsu1BTCSVehAtv/*v1.9*/, Reserved1Bit);	
	CAN_LOG_DATA[28] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[29] =	(uint8_t) (u16TempMem4Log>>8);

	u16TempMem4Log = LSBTCS_u16PackData14_1_1(eng_rpm/*v1.9*/, Reserved1Bit, Reserved1Bit);	
	CAN_LOG_DATA[30] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[31] =	(uint8_t) (u16TempMem4Log>>8);		
	
	/* Byte 4 */
	CAN_LOG_DATA[32] =	(uint8_t) (wstr);
	CAN_LOG_DATA[33] =	(uint8_t) (wstr>>8);

	CAN_LOG_DATA[34] =	(uint8_t) (wstr_dot);
	CAN_LOG_DATA[35] =	(uint8_t) (wstr_dot>>8);
	
	CAN_LOG_DATA[36] =	(uint8_t) (yaw_out);
	CAN_LOG_DATA[37] =	(uint8_t) (yaw_out>>8);

	CAN_LOG_DATA[38] =	(uint8_t) (alat);
	CAN_LOG_DATA[39] =	(uint8_t) (alat>>8);
	
	/* Byte 5 */
	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(ax_Filter, ETCS_GAIN_GEAR_SHFT.lcetcsu1GainChngInGearShft, ETCS_GAIN_GEAR_SHFT.lcetcsu1GainTrnsAftrGearShft, Reserved1Bit, YAW_CDC_WORK);
	CAN_LOG_DATA[40] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[41] =	(uint8_t) (u16TempMem4Log>>8);

	CAN_LOG_DATA[42] =	(uint8_t) (ETCS_ENG_CMD.lcetcss16EngTrqCmd);
	CAN_LOG_DATA[43] =	(uint8_t) (ETCS_ENG_CMD.lcetcss16EngTrqCmd>>8);
	
	CAN_LOG_DATA[44] =	(uint8_t) (delta_yaw_first);
	CAN_LOG_DATA[45] =	(uint8_t) (delta_yaw_first>>8);

	LocalTemp01 = LCTCS_s16IFindMaximum(FL.vfilt_rough, FR.vfilt_rough);
	LocalTemp02 = LCTCS_s16IFindMaximum(RL.vfilt_rough, RR.vfilt_rough);
	LocalTemp03 = LCTCS_s16IFindMaximum(LocalTemp01, LocalTemp02);		
	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(LocalTemp03/*v1.9*/, ETCS_BRK_CTL_REQ.lcetcsu1SymBrkTrqCtlReqFA, ETCS_BRK_CTL_REQ.lcetcsu1SymBrkTrqCtlReqRA, FA_TCS.lctcsu1HuntingDcted, RA_TCS.lctcsu1HuntingDcted);
	CAN_LOG_DATA[46] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[47] =	(uint8_t) (u16TempMem4Log>>8);	
	
	/* Byte 6 */
	CAN_LOG_DATA[48] =	(uint8_t) (mtp);
	CAN_LOG_DATA[49] =	(uint8_t) (gear_pos);
	CAN_LOG_DATA[50] =	(uint8_t) (ETCS_WHL_SPIN.lcetcss16WhlSpin/2);
	CAN_LOG_DATA[51] =	(uint8_t) (ETCS_TAR_SPIN.lcetcss16TarWhlSpin);
	CAN_LOG_DATA[52] =	(uint8_t) (ltcss16BaseTarWhlSpinDiff);
	CAN_LOG_DATA[53] =	(uint8_t) (ETCS_VEH_ACC.lcetcss16VehAccByVehSpd);
	
	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(det_alatm, ETCS_TAR_SPIN.lcetcsu1TarSpnDec, HSA_flg/*v1.9*/, MPRESS_BRAKE_ON/*v1.9*/, ETCS_SPLIT_HILL.lcetcsu1SplitHillDct/*v1.9*/);
	CAN_LOG_DATA[54] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[55] =	(uint8_t) (u16TempMem4Log>>8);
	
	/* Byte 7 */
	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(Beta_MD, Reserved1Bit, Reserved1Bit, Reserved1Bit, Reserved1Bit);/*v2.0*/
	CAN_LOG_DATA[56] =	(uint8_t) (s16TempMem4Log);
	CAN_LOG_DATA[57] =	(uint8_t) (s16TempMem4Log>>8);
	u16TempMem4Log = LCTCS_s16IFindMaximum(ETCS_BRK_CTL_CMD.lcetcss16SymBrkTrqCtlCmdFA, ETCS_BRK_CTL_CMD.lcetcss16SymBrkTrqCtlCmdRA);
	u16TempMem4Log = LSBTCS_u16PackData12_1_1_1_1(u16TempMem4Log, lctcsu1DctBTCHillByAx, ETCS_4WD_SIG.lcetcsu14wdLwMd/*0701*/, PC_TCS.lctcsu1EstPresLowFlag/*v1.9*/, SC_TCS.lctcsu1EstPresLowFlag/*v1.9*/);
	CAN_LOG_DATA[58] =	(uint8_t) (u16TempMem4Log);
	CAN_LOG_DATA[59] =	(uint8_t) (u16TempMem4Log>>8);
	s16TempMem4Log = (int16_t)(ETCS_CRDN_TRQ.lcetcss32RealCrdnTrq/10);
	CAN_LOG_DATA[60] =	(uint8_t) (s16TempMem4Log);
	CAN_LOG_DATA[61] =	(uint8_t) (s16TempMem4Log>>8);
	s16TempMem4Log = (int16_t)(ETCS_LD_TRQ.lcetcss32LdTrqByAsymBrkCtl/10);
	CAN_LOG_DATA[62] =	(uint8_t) (s16TempMem4Log);
	CAN_LOG_DATA[63] =	(uint8_t) (s16TempMem4Log>>8);
	
	/* Byte 8 */
	CAN_LOG_DATA[64] =	(uint8_t) (rf);				/*v1.9*/
	CAN_LOG_DATA[65] =	(uint8_t) (rf>>8);
	s16TempMem4Log = (int16_t)(ETCS_CTL_CMD.lcetcss32PCtlPor/10);
	CAN_LOG_DATA[66] =	(uint8_t) (s16TempMem4Log);
	CAN_LOG_DATA[67] =	(uint8_t) (s16TempMem4Log>>8);
	s16TempMem4Log = (int16_t)(ETCS_CTL_CMD.lcetcss32ICtlPor/10);	
	CAN_LOG_DATA[68] =	(uint8_t) (s16TempMem4Log);
	CAN_LOG_DATA[69] =	(uint8_t) (s16TempMem4Log>>8);
	s16TempMem4Log = (int16_t)(ETCS_CTL_CMD.lcetcss32CardanTrqCmd/10);
	CAN_LOG_DATA[70] =	(uint8_t) (s16TempMem4Log);
	CAN_LOG_DATA[71] =	(uint8_t) (s16TempMem4Log>>8);
	
	/* Byte 9 */
	s16TempMem4Log = (int16_t)(ETCS_REF_TRQ2JMP.lcetcss32RefTrq2Jmp/10);
	CAN_LOG_DATA[72] =	(uint8_t) (s16TempMem4Log);
	CAN_LOG_DATA[73] =	(uint8_t) (s16TempMem4Log>>8);
	s16TempMem4Log = (int16_t)(ETCS_TRQ_REP_RD_FRIC.lcetcss32TrqRepRdFric/10);	
	CAN_LOG_DATA[74] =	(uint8_t) (s16TempMem4Log);
	CAN_LOG_DATA[75] =	(uint8_t) (s16TempMem4Log>>8);
	CAN_LOG_DATA[76] =	(uint8_t) (ETCS_LOW_WHL_SPN.lcetcss16LowWhlSpnCnt);
	CAN_LOG_DATA[77] =	(uint8_t) (ETCS_DEP_SNW.lcetcss16DepSnwHilCnt); /* v2.0 */
	CAN_LOG_DATA[78] =	(uint8_t) (ETCS_CTL_GAINS.lcetcss16Pgain);
	CAN_LOG_DATA[79] =	(uint8_t) (ETCS_CTL_GAINS.lcetcss16Igain);
}

static uint16_t LSBTCS_u16PackData12_1_1_1_1(uint16_t Variable2ReduceSize, uint16_t Flag1, uint16_t Flag2, uint16_t Flag3, uint16_t Flag4)
{
	uint16_t	OutputValue;
	uint16_t 	TempMem16Bits_1=0;
	uint16_t 	TempMem16Bits_2=0;	
	
	TempMem16Bits_1 = Variable2ReduceSize&(0x0FFF); 			/* 0FFF = 0000 1111 1111 1111 */
	TempMem16Bits_2 = ( (Flag4<<15)|(Flag3<<14)|(Flag2<<13)|(Flag1<<12) );
	OutputValue = (TempMem16Bits_1|TempMem16Bits_2);
	return OutputValue;
}

static uint16_t LSBTCS_u16PackData12_2_2(uint16_t Variable2ReduceSize, uint16_t Flag1, uint16_t Flag2)
{
	uint16_t	OutputValue;
	uint16_t 	TempMem16Bits_1=0;
	uint16_t 	TempMem16Bits_2=0;
	
	TempMem16Bits_1 = Variable2ReduceSize&(0x0FFF); 			/* 0FFF = 0000 1111 1111 1111 */
	TempMem16Bits_2 = ( (Flag2<<14)|(Flag1<<12) );
	OutputValue = (TempMem16Bits_2|TempMem16Bits_1);
	return OutputValue;
}

static uint16_t LSBTCS_u16PackData14_1_1(uint16_t Variable2ReduceSize, uint16_t Flag1, uint16_t Flag2)
{
	uint16_t	OutputValue;
	uint16_t 	TempMem16Bits_1=0;
	uint16_t 	TempMem16Bits_2=0;
	
	TempMem16Bits_1 = Variable2ReduceSize&(0x3FFF); 			/* 0FFF = 0011 1111 1111 1111 */
	TempMem16Bits_2 = ( (Flag2<<15)|(Flag1<<14) );
	OutputValue = (TempMem16Bits_2|TempMem16Bits_1);
	return OutputValue;
}

static uint8_t LSBTCS_u8PackData4_4(uint8_t data1, uint8_t data2)
{
	uint8_t	OutputValue;
	uint8_t	TempMem8Bits_1=0;
	uint8_t	TempMem8Bits_2=0;
	
	TempMem8Bits_1 = data1&(0x0F);	/* 0F = 0000 1111 */
	TempMem8Bits_2 = data2&(0x0F);
	OutputValue = ( (TempMem8Bits_2<<4)|(TempMem8Bits_1) );
	return OutputValue;
}	

static uint8_t LSBTCS_u8PackFlags(uint8_t Flag1, uint8_t Flag2, uint8_t Flag3, uint8_t Flag4, uint8_t Flag5, uint8_t Flag6, uint8_t Flag7, uint8_t Flag8)
{
	uint8_t	OutputValue;
	uint8_t	TempMem8Bits=0;
	
	TempMem8Bits = ( (Flag8<<7)|(Flag7<<6)|(Flag6<<5)|(Flag5<<4)|(Flag4<<3)|(Flag3<<2)|(Flag2<<1)|(Flag1) );
	OutputValue = TempMem8Bits;
	return OutputValue;
}	

#endif		/* #if defined(__LOGGER) && (__LOGGER_DATA_TYPE==1) */
