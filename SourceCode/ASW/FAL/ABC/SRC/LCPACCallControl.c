/******************************************************************************
*  Project Name: PRE ABS CYCLE CONTROL
*  File: LCPACCallControl.C
*  Description:
*  Date: April. 23. 2012      
*******************************************************************************
*  Modification   Log                                             
*  Date           Author           Description                    
*  -------       ----------    --------------------------------------------      
*  12.04.23      yongkil Kim   Initial Release
*  12.05.04      JinKoo Lee    Modified for GSUV application                
*  12.06.05		 Seongyon Ryu  Modified for MGH-80 application
*******************************************************************************/


#include "LCPACCallControl.h"
#include "LAPACCallActHW.h"

#include "LDABSDctWheelStatus.h"
#include "LAABSCallMotorSpeedControl.h"
#include "LCESPCalInterpolation.h"
#include "LCESPCalLpf.h"
#include "LCallMain.h"
#include "LCABSDecideCtrlState.h"
#include "LCABSDecideCtrlVariables.h"
#include "LCABSCallControl.h"

#if __PAC

/* Local Definiton  *********************************************************/


/* Tuning Parameter *********************************************************/
  

/* Variables Definition*******************************************************/

struct  U8_BIT_STRUCT_t PACC0;
struct  U8_BIT_STRUCT_t PACC1;

PAC_WHL_CTRL_t LCPACFL, LCPACFR;
 
int16_t  PAC_tempW4,PAC_tempW3,PAC_tempW2,PAC_tempW1,PAC_tempW0;
int16_t	 lcpacs16PacWheelPres, lcpacs16PacWheelPres, lcpacs16MpThresByIndex, lcpacs16MpSlopeByIndex;
uint16_t lspacu16PacFadeOutCounter,lcpacu16PacFadeOutCntRef;

uint8_t  lcpacu8PacControlState;
uint8_t	 lcpacu8MpFastRiseIndex;
uint8_t	 lcpacu8FadeOutMode, lcpacu8SteadyMpCount;


/* LocalFunction prototype ***************************************************/

void LCPAC_vCallControl(void);
static void LCPAC_vCheckInhibition(void);
static void LCPAC_vDecidePACStart(void);
static void LCPAC_vDecidePACControl(void);
static void LCPAC_vDecidePACControlWL(PAC_WHL_CTRL_t *pPacWL);
static void LCPAC_vCheckMpressCondition(void);
void LCPAC_vCallControlFadeOut(void);
static void LCPAC_vCallControlFadeOutWL(PAC_WHL_CTRL_t *pPacWL);
void LCPAC_vResetControl(void);
void LCPAC_vResetCtrlVariables(void);
void LCPAC_vCallDecideLPAEmpty(void);
void LCPAC_vDecidePACExit(void);

/* Implementation*************************************************************/

void LCPAC_vCallControl(void)
{
    LCPAC_vCheckInhibition();

	LCPAC_vCheckMpressCondition(); 

	switch (lcpacu8PacControlState)
	{
		/*-------------------------------------------------------------------------- */
		case S8_PAC_READY:
			if(lcpacu1InhibitFlg == 1)  
			{			
				lcpacu8PacControlState = S8_PAC_INHIBIT;
			}
			else 
			{
				LCPAC_vDecidePACStart();
				if(lcpacu1PACEnableFlg==1) 
				{
					lcpacu8PacControlState = S8_PAC_ACTUATION;
			        lcpacu1PacActiveFlg = 1;
				}
			}
			break;
		/*-------------------------------------------------------------------------- */
		case S8_PAC_ACTUATION:
			
			LCPAC_vDecidePACControl();
			LCPAC_vDecidePACExit();
			
			if(lcpacu1PACExitFlg == 1) 
			{
				lcpacu8PacControlState = S8_PAC_READY;				
				LCPAC_vResetCtrlVariables();
			}
			else if(lcpacu1FadeOutFlg == 1) 
			{
				lcpacu8PacControlState = S8_PAC_FADEOUT;
				LCPAC_vResetCtrlVariables();
			}
			else { }
					
			break;
		/*-------------------------------------------------------------------------- */
		case S8_PAC_FADEOUT:

			LCPAC_vDecidePACExit();			
			LCPAC_vCallControlFadeOut();

			if(lcpacu1PACExitFlg == 1) 
			{
				lcpacu8PacControlState = S8_PAC_READY;				
				LCPAC_vResetCtrlVariables();
			}
			else { }
			
			if(lcpacu1FadeOutEndFlg==1)
			{			
				if(lcpacu1InhibitFlg == 1)
				{
					lcpacu8PacControlState = S8_PAC_INHIBIT;
				}
				else
				{
					lcpacu8PacControlState = S8_PAC_READY;
					LCPAC_vResetControl();
					LCPAC_vResetCtrlVariables();
				}
				/*Empty LPA!!!!*/
			}			
			else
			{
				lcpacu1FadeOutFlg = 1;
			}
			
			break;
		/*-------------------------------------------------------------------------- */
		case S8_PAC_INHIBIT:
			if(lcpacu1InhibitFlg == 0) 
			{
				lcpacu8PacControlState = S8_PAC_READY;
				LCPAC_vResetCtrlVariables();
			}
			else 
			{
				/*LCPAC_vCallVariableReset();*/ 
				/*Control inhibition*/
			}
			break;	

		/*-------------------------------------------------------------------------- */
		default:
			if(lcpacu1InhibitFlg == 0) 
			{
				lcpacu8PacControlState = S8_PAC_READY;
				LCPAC_vResetCtrlVariables();
				/*PAC_MSC_DEBUGGER = 13;*/
			}
			else { }			
			break;	
	}
}

static void LCPAC_vCheckInhibition(void)
{
	tempW4 = McrAbs(alat);
	
	if((fu1AySusDet==1)||(fu1AyErrorDet==1)||((fu1MCPErrorDet==1)||(fu1MCPSusDet==1)||(fu1McpSenPwr1sOk==0)))
    {
        lcpacu1InhibitFlg = 1;
    }
    else if((gs_sel==7)||((fu1GearR_Switch == 1)&&(fu1ClutchSwitch == 0))/*AT: R-gear, MT: R-gear s/w+clutch*/)
    {
        lcpacu1InhibitFlg = 1;
    }
    else if((ABS_fz==1)||(YAW_CDC_WORK==1)||(BEND_DETECT2==1)||(tempW4>LAT_0G1G))
	{
        lcpacu1InhibitFlg = 1;
    }
    else if((FL.PossibleABSForBump==1)||(FR.PossibleABSForBump==1)||(PossibleABSForBumpVehicle==1)
	 	  ||(Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1))
	{
        lcpacu1InhibitFlg = 1;
    }
    else
    {
    	lcpacu1InhibitFlg = 0;
    }
}

static void LCPAC_vCheckMpressCondition(void)
{
	int16_t s16MpressSlopeFor3Scan;
	
	s16MpressSlopeFor3Scan = (MCpress[0]-MCpress[3]);/*mpress change valude per 1scan*/
	if(BRAKE_BY_MPRESS_FLG==1)
	{
		if(s16MpressSlopeFor3Scan>0)
		{
			if(s16MpressSlopeFor3Scan>MPRESS_10BAR)
			{
				lcpacu8MpFastRiseIndex = lcpacu8MpFastRiseIndex + 4;
			}
			if(s16MpressSlopeFor3Scan>MPRESS_5BAR)
			{
				lcpacu8MpFastRiseIndex = lcpacu8MpFastRiseIndex + 2;
			}
			else
			{
				lcpacu8MpFastRiseIndex = lcpacu8MpFastRiseIndex - 1;
			}
		}
		else
		{
			if(s16MpressSlopeFor3Scan<-MPRESS_30BAR)
			{
				lcpacu8MpFastRiseIndex = lcpacu8MpFastRiseIndex - 4;
			}
			else
			{
				lcpacu8MpFastRiseIndex = lcpacu8MpFastRiseIndex - 2;
			}		
		}
		lcpacu8MpFastRiseIndex = LCABS_s16LimitMinMax(lcpacu8MpFastRiseIndex,0,100);
		
		/*if((s16MpressSlopeFor3Scan<MPRESS_1BAR)&&((s16MpressSlopeFor3Scan>-MPRESS_1BAR)))
		{
			lcpacu8SteadyMpCount = lcpacu8SteadyMpCount + 1;
		}		
		else
		{
			lcpacu8SteadyMpCount = 0;
		}*/
	}
	else
	{
		lcpacu8MpFastRiseIndex = 0;
	}
	
	lcpacs16MpThresByIndex = LCESP_s16IInter4Point(vref ,S16_PAC_ENTER_SPEED_MIN,   	S16_PAC_ENTER_TH_MPRESS,
                                                         S16_PAC_ENTER_SPEED_2ND_P,  	S16_PAC_ENTER_TH_MPRESS_2ND_P,
                                                         S16_PAC_ENTER_SPEED_3RD_P,  	S16_PAC_ENTER_TH_MPRESS_3RD_P,
                                                         S16_PAC_ENTER_SPEED_4TH_P, 	S16_PAC_ENTER_TH_MPRESS_4TH_P);
                                                         
	lcpacs16MpSlopeByIndex = (int16_t)U8_PAC_ENTER_TH_MPRESS_SLOP;
	
	if((mpress>=lcpacs16MpThresByIndex)&&(s16MpressSlopeFor3Scan>=lcpacs16MpSlopeByIndex)&&((fu1McpSenPwr1sOk==1)&&(fu1MCPErrorDet==0)&&(fu1MCPSusDet==0))
	  &&(FL.s16_Estimated_Active_Press>=S16_PAC_ENTER_TH_WPRESS_MIN)&&(FR.s16_Estimated_Active_Press>=S16_PAC_ENTER_TH_WPRESS_MIN))
	{
		lcpacu1DctPacMpressThres = 1;
	}
	else
	{
		lcpacu1DctPacMpressThres = 0;
	}
}

static void LCPAC_vDecidePACStart(void)
{
	int16_t s16PacEnterMinSpeed;
	int16_t s16PacEnterMaxSpeed;
	int8_t	s8PacEnterSlip;
	int8_t	s8PacEnterDv;
	int8_t	s8PacEnterDecel;
	
	s16PacEnterMaxSpeed = S16_PAC_ENTER_SPEED_MAX;
	if(s16PacEnterMaxSpeed>VREF_250_KPH) {s16PacEnterMaxSpeed = VREF_250_KPH;}
	
	s16PacEnterMinSpeed = S16_PAC_ENTER_SPEED_MIN;
	if(s16PacEnterMinSpeed<VREF_10_KPH) {s16PacEnterMinSpeed = VREF_10_KPH;}
	
	s8PacEnterSlip = S8_PAC_ENTER_TH_SLIP;
	tempW2 = LCABS_s16LimitMinMax(tempW2, LAM_1P, LAM_6P);
		
	s8PacEnterDv = S8_PAC_ENTER_TH_DV;
	
	s8PacEnterDecel = S8_PAC_ENTER_TH_WHEEL_DECEL;

	if((lcpacu1DctPacMpressThres==1)&&(vref>=s16PacEnterMinSpeed)&&(vref<=s16PacEnterMaxSpeed)
		&&((((vref-FL.vrad_crt)>=(int16_t)s8PacEnterDv)&&(FL.rel_lam<=-s8PacEnterSlip)&&(FL.arad<=s8PacEnterDecel)) 
		||(((vref-FR.vrad_crt)>=(int16_t)s8PacEnterDv)&&(FR.rel_lam<=-s8PacEnterSlip)&&(FR.arad<=s8PacEnterDecel)))        
		&&(ebd_filt_grv<=(int16_t)S8_PAC_ENTER_TH_VEHICLE_DECEL)
    )
    {
    	lcpacu1PACEnableFlg = 1;
    }
    else
    {
		lcpacu1PACEnableFlg = 0;
    }
}

static void LCPAC_vDecidePACControl(void)
{
    LCPAC_vDecidePACControlWL(&LCPACFL);
    LCPAC_vDecidePACControlWL(&LCPACFR);
}

static void LCPAC_vDecidePACControlWL(PAC_WHL_CTRL_t *pPacWL)
{              
	int8_t  s8DesiredSlipBand;
    int16_t	s16DesiredDv;

  #if (__PAC_REAPPLY_TIME_TUNING == ENABLE)
    /*WL->LFC_Conven_Reapply_Time = U8_PMC_RISE_T;*/
	if(pPacWL->u8ReapplyCounter>U8_PAC_REAPPLY_CNT_REF_PH3)
	{
		pPacWL->u8ConvenReapplyTime = S8_PAC_REAPPLY_TIME_PH3;
	}
	else if(pPacWL->u8ReapplyCounter>U8_PAC_REAPPLY_CNT_REF_PH2)
	{
		pPacWL->u8ConvenReapplyTime = S8_PAC_REAPPLY_TIME_PH2;
	}
	else
	{
		pPacWL->u8ConvenReapplyTime = S8_PAC_REAPPLY_TIME_PH1;
	}                
  #else /*(__PAC_REAPPLY_TIME_TUNING == ENABLE)*/
    if(FL.rel_lam < FR.rel_lam)
    {
        tempB4 = FL.rel_lam;
    }
    else
    {
        tempB4 = FR.rel_lam;
    }
     /*need to change*/
    s8DesiredSlipBand = (int8_t)LCABS_s16Interpolation2P(vref, S16_PAC_ENTER_MIN_SPEED,		 S16_PAC_ENTER_MAX_SPEED, 
    													(int16_t)S8_PRE_ABS_MAX_OPTIMAL_SLIP,	(int16_t)S8_PRE_ABS_MIN_OPTIMAL_SLIP);

    pPacWL->u8ConvenReapplyTime = (uint8_t)LCABS_s16Interpolation2P(tempB4, -(int16_t)s8DesiredSlipBand   ,-(int16_t)S8_PREABS_SLIP_FOR_RISE_T_MAX, 
                                                                           (int16_t)U8_PREABS_RISE_T_AT_OPT_SLIP, (int16_t)U8_PREABS_RISE_T_MAX);
  #endif	/*(__PAC_REAPPLY_TIME_TUNING == ENABLE)*/
  		
	pPacWL->u8ConvenReapplyTime = (uint8_t)LCABS_s16LimitMinMax(pPacWL->u8ConvenReapplyTime,3,15);
	
	if(pPacWL->u8ReapplyCounter>U8_PAC_REAPPLY_CNT_REF_PH3)
	{
		pPacWL->u8HoldCounterRef = U8_PAC_PULSE_UP_HOLD_TIME_PH3;
	}
	else if(pPacWL->u8ReapplyCounter>U8_PAC_REAPPLY_CNT_REF_PH2)
	{
		pPacWL->u8HoldCounterRef = U8_PAC_PULSE_UP_HOLD_TIME_PH2;
	}
	else
	{
		pPacWL->u8HoldCounterRef = U8_PAC_PULSE_UP_HOLD_TIME_PH1;
	}
		
	if(pPacWL->u8RiseHoldCnt < pPacWL->u8HoldCounterRef)
	{
		pPacWL->u8RiseHoldCnt = pPacWL->u8RiseHoldCnt + 1;
	}
	else
	{
		pPacWL->u8RiseHoldCnt = 0;
		pPacWL->u8ReapplyCounter = pPacWL->u8ReapplyCounter + 1;
	}		
}

void LCPAC_vResetControl(void)
{
	lcpacu1PACEnableFlg = 0;
	//lcpacu1PacActiveFlg = 0;
	lcpacu1InhibitFlg = 0;
	lcpacu1FadeOutFlg = 0;
	lcpacu1FadeOutEndFlg = 0;
	lcpacu1DctPacMpressThres = 0;
}

void LCPAC_vResetCtrlVariables(void)
{
	LCPACFL.u8ConvenReapplyTime = 0;
	LCPACFL.u8HoldCounterRef = 0;
	LCPACFL.u8RiseHoldCnt = 0;
	LCPACFL.u8ReapplyCounter = 0;
	
	LCPACFR.u8ConvenReapplyTime = 0;
	LCPACFR.u8HoldCounterRef = 0;
	LCPACFR.u8RiseHoldCnt = 0;
	LCPACFR.u8ReapplyCounter = 0;
	
	lcpacu1PacActiveFlg = 0;
}

void LCPAC_vCallControlFadeOut(void)
{
	LCPAC_vCallControlFadeOutWL(&LCPACFL);
	LCPAC_vCallControlFadeOutWL(&LCPACFR);
}

static void LCPAC_vCallControlFadeOutWL(PAC_WHL_CTRL_t *pPacWL)
{
	/*if(lcpacu8FadeOutMode == PAC_FADEOUT_MAX_RISE)
	{
		pPacWL->u8HoldCounterRef = (pPacWL->u8HoldCounterRef/2);
		pPacWL->u8HoldCounterRef = (uint8_t)LCABS_s16LimitMinMax(pPacWL->u8HoldCounterRef,1,10);
	}
	else if(lcpacu8FadeOutMode == PAC_FADEOUT_MAX_ALAT)
	{
		if(alat > LAT_0G1G)
		{
			LCPACFL.u8HoldCounterRef = (LCPACFL.u8HoldCounterRef*2);
			LCPACFL.u8HoldCounterRef = (uint8_t)LCABS_s16LimitMinMax(LCPACFL.u8HoldCounterRef,5,20);
		}
		else if(alat < -LAT_0G1G)
		{
			LCPACFR.u8HoldCounterRef = (LCPACFR.u8HoldCounterRef*2);
			LCPACFR.u8HoldCounterRef = (uint8_t)LCABS_s16LimitMinMax(LCPACFR.u8HoldCounterRef,5,20);
		}
		else{}
	}
	else {}*/
	
	pPacWL->u8ConvenReapplyTime = S8_PAC_FADE_OUT_REAPPLY_TIME;
	pPacWL->u8HoldCounterRef = U8_PAC_FADEOUT_HOLD_TIME;
		
	if(lspacu16PacFadeOutCounter==0)
    {		
    	lcpacu16PacFadeOutCntRef = ((mpress-lcpacs16PacWheelPres)/MPRESS_5BAR)*U8_PAC_FADEOUT_HOLD_TIME;
    }
        
	if(lspacu16PacFadeOutCounter<lcpacu16PacFadeOutCntRef)
	{	            
        lspacu16PacFadeOutCounter = lspacu16PacFadeOutCounter + 1;
        //lcpacu1FadeOutFlg = 1;
    }
    else
    {
    	//lcpacu1FadeOutFlg = 0;
    	lcpacu1FadeOutEndFlg = 1;
    	lspacu16PacFadeOutCounter=0;
    }
    
	if(pPacWL->u8ReapplyCounter > U8_PAC_FADEOUT_END_CNT_REF)
	{
		lcpacu1FadeOutEndFlg = 1;
	}
	else
	{
		lcpacu1FadeOutEndFlg = 0;	
	}
}

void LCPAC_vDecidePACExit(void)
{
	int16_t s16PressDiff;
	
	tempW4 = McrAbs(alat);
	
	if(FL.s16_Estimated_Active_Press > FR.s16_Estimated_Active_Press)
    {
        lcpacs16PacWheelPres = FL.s16_Estimated_Active_Press;
    }
    else
    {
    	lcpacs16PacWheelPres = FR.s16_Estimated_Active_Press;
    }
    		
	s16PressDiff = mpress - lcpacs16PacWheelPres;
    
	if((ABS_fz==1)||(YAW_CDC_WORK==1)||(mpress<(lcpacs16MpThresByIndex-MPRESS_10BAR)))
	{
		lcpacu1PACExitFlg = 1;
	}
	else if(((LCPACFL.u8ReapplyCounter>=U8_PAC_END_DP_CHECK_CNT)||(LCPACFR.u8ReapplyCounter>=U8_PAC_END_DP_CHECK_CNT))&&((s16PressDiff)<MPRESS_5BAR))
	{
		lcpacu1PACExitFlg = 1;	
	}
	/*else if(((LCPACFL.u8ReapplyCounter>=U8_PAC_FADEOUT_END_CNT_REF)||(LCPACFR.u8ReapplyCounter>=U8_PAC_FADEOUT_END_CNT_REF))&&(lcpacu1FadeOutFlg==1))
	{
		lcpacu1PACExitFlg = 1;
	}*/
	else if((BEND_DETECT2==1)||(tempW4>=LAT_0G1G)||(vref<S16_PAC_ENTER_SPEED_MIN)
      ||(LCPACFL.u8ReapplyCounter>=U8_PAC_FADEOUT_START_CNT_REF)||(LCPACFR.u8ReapplyCounter>=U8_PAC_FADEOUT_START_CNT_REF) )
    {
    	lcpacu1FadeOutFlg = 1;	
    }
	else
	{
		lcpacu1PACExitFlg = 0;
		lcpacu1FadeOutFlg = 0;
	}
}

void LCPAC_vCallDecideLPAEmpty(void)
{
	
}

#endif /* #if __PAC */
