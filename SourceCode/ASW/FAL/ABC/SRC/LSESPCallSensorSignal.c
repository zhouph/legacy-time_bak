
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LSESPCallSensorSignal
	#include "Mdyn_autosar.h"
#endif

#include "LSESPCallSensorSignal.h"/*MISRA, H22*/
#include "LSESPCalSensorOffset.h"
#include "LSESPFilterEspSensor.h"
#include "LDRTACallDetection.h"


#if __CAR == GM_GSUV  
int16_t S16_HM_EMS;
int16_t S16_LM_EMS;
int16_t S16_MM_EMS;
#endif

#if __VDC

void LSESP_vCallSensorSignal(void)/*MISRA, H22*/
{
    cyctime++;
    /*11SWD cycle time*/
    if (speed_calc_timer < L_U8_TIME_10MSLOOP_200MS)
    {
        lsesps16MuHigh = S16_MU_HIGH;
        lsesps16MuMedHigh = S16_MU_MED_HIGH;      
        lsesps16MuMed = S16_MU_MED;
        lsesps16MuLow = S16_MU_LOW;
        
      #if __CAR == GM_GSUV  
        S16_HM_EMS = S16_HM_EMS_CAL;
        S16_LM_EMS = S16_LM_EMS_CAL;
        S16_MM_EMS = S16_MM_EMS_CAL;
      #endif
    }                                      
    else                                   
    {                                                
        ;                             
    }


/*    FLAG_TIME_OPT = !FLAG_TIME_OPT; */

/*
	if(FLAG_TIME_OPT_1_3)
	{
		FLAG_TIME_OPT_1_3=0;
		FLAG_TIME_OPT_2_3=1;
		FLAG_TIME_OPT_3_3=0;
	}
	else if(FLAG_TIME_OPT_2_3)
	{
		FLAG_TIME_OPT_1_3=0;
		FLAG_TIME_OPT_2_3=0;
		FLAG_TIME_OPT_3_3=1;
	}
	else if(FLAG_TIME_OPT_3_3)
	{
		FLAG_TIME_OPT_1_3=1;
		FLAG_TIME_OPT_2_3=0;
		FLAG_TIME_OPT_3_3=0;
	}
	else
	{
		;
	}

    if((!FLAG_TIME_OPT_1_3)&&(!FLAG_TIME_OPT_2_3)&&(!FLAG_TIME_OPT_3_3))
    {
    	FLAG_TIME_OPT_1_3=1;
    }
    else
    {
    	;
    }
*/
    MPA_SW=MPA_USE_SW;

#if __GM_FailM       
        
    if( (fu1ESCDisabledBySW==1)&&(RTA_FL.ldabsu1DetSpareTire==0)&&(RTA_FR.ldabsu1DetSpareTire==0)   /* '08.03.28 : for Bench_Test, 10SWD : minitire flag change, GM failM */  
         &&(RTA_RL.ldabsu1DetSpareTire==0)&&(RTA_RR.ldabsu1DetSpareTire==0)
				 &&(Flg_ESC_SW_ABS_OK==0)&&(Flg_ESC_SW_ROP_OK==0)&&(Flg_Flat_Tire_Detect_OK==0)&&(Flg_ESC_OFF_SLEEP_MODE_CBS_OK == 0)
			   #if __TCS_DRIVELINE_PROTECT
				 &&(lctcsu1DrivelineProtectionOn==0)
			   #endif	/* #if __TCS_DRIVELINE_PROTECT */
    
	   )   
#else          

	#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
		if((fu1ESCDisabledBySW==1)&&(Flg_ESC_SW_ROP_OK==0)&&(
			 ((lsespu1PedalBrakeOn==0)&&(lsespu1BrkAppSenInvalid == 0))
			 ||((BLS==0)&&(lsespu1BrkAppSenInvalid == 1))
			 ))
	#else
		if((fu1ESCDisabledBySW==1)&&(MPRESS_BRAKE_ON==0)&&(Flg_ESC_SW_ROP_OK==0))
		
	#endif
        
#endif     

	{
		vdc_on_flg =0;
	}
	else
	{
		vdc_on_flg =1;
	}

#if !SIM_MATLAB

    LSESP_vCalSensorOffset();
#endif

    LSESP_vFilterEspSensor();



}

#endif
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LSESPCallSensorSignal
	#include "Mdyn_autosar.h"
#endif
