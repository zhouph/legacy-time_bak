#ifndef	__RTA_STRUCT_H__
#define	__RTA_STRUCT_H__

#include "LVarHead.h"

#define	__RTA_ENABLE	1	
#define __SPARE_WHEEL   0

#if	__RTA_ENABLE

#define	RTA_RELIABLE_CONDITON_CHECK						RTA_1.bit7		
#define	RTA_2WHEEL_RELIABLE_CONDITON_CHECK				RTA_1.bit6
#define	RTA_2WHEEL_COMP_RATIO_CALC_OK					RTA_1.bit5
#define	WHEEL_ERROR_STATE								RTA_1.bit4		  
#define	RTA_ABSENCE_CONFIRM								RTA_1.bit3
#define	RTA_RESET_STATE									RTA_1.bit2
#define	RTA_INSTALLATION_STATE							RTA_1.bit1		  
#define	RTA_INSTALLATION_STATE_INFORM					RTA_1.bit0

#define	RTA_REF_WHEEL_DEFINED							RTA_2.bit7
#define	RTA_RELIABLE_CHECK_FOR_ESP						RTA_2.bit6
#define	RTA_RATIO_CONV_FLG								RTA_2.bit5
#define	ldrtau1GoodCondForRta2WhlDct					RTA_2.bit4
#define	ldrtau1GoodCondForRta2WhlClear					RTA_2.bit3
#define	ldrtau1GoodCondForRta2WhlSus					RTA_2.bit2
#define	ldrtau1WssInvalidFlagForRTA						RTA_2.bit1
#define	RTA_BTCS_ON_CHK									RTA_2.bit0


  #if __CAR_MAKER==GM_KOREA
#define		__GM_RFQ		1
  #else
#define		__GM_RFQ		0	
  #endif
  
	
#if	 __VDC	
	#if	__GM_RFQ 
#define		__RTA_2WHEEL_DCT	1	
	#elif __CAR==HMC_BK
#define		__RTA_2WHEEL_DCT	1	
	#else
#define		__RTA_2WHEEL_DCT	0
	#endif
#else
	#define	__RTA_2WHEEL_DCT	0
#endif	




#define	__RTA_UPDATE_VERIFY	1


struct RTA_STRUCT
{
	
	int16_t	det_rta_suspect_cnt;
	int16_t	det_rta_suspect_cnt2;
	int16_t	det_rta_suspect_cnt3;
	int16_t	det_rta_suspect_cnt4;
	int16_t	det_rta_suspect_cnt5;
	int16_t	det_rta_suspect_cnt6;
		
	uint16_t rta_confirm_cnt;
	uint8_t ldrtau8RtaDetBufInx;
	int16_t ldrtas16RtaSubTimerForBuf;
	int16_t ldrtas16RtaSusDetBuffer[5];
	int32_t ldrtas32SumOfSlipRatio;
	
	int16_t	lds16RtaDetMaxSlipRatio;
	int16_t	lds16RtaDetMinSlipRatio;
	int16_t	lds16RtaCalcdiffRatio;
	int16_t lds16RtaAvgSlipRatio;
				
	#if __RTA_2WHEEL_DCT
	int16_t	ldrtas16Rta2WhlDctCnt;
	int16_t	ldrtas16Rta2WhlSusCnt;
	int16_t	ldrtas16Rta2WhlDctRatioTemp;
	int16_t	ldrtas16Rta2WhlCompRatio;
	uint8_t	ldrtau8RtaCompFlgs;
	
	int16_t	ldrtas16Rta2WhlSusCompRatio;
	uint8_t ldrtau8Rta2WhlSusRatioCalcCnt;
	uint8_t ldrtau8Rta2WhlSusRatioRest;
	#endif	
					   
	unsigned RTA_SUSPECT_2WHL:			 		1;	/*bit7*/	
	unsigned RTA_SUSPECT_WL:		   			1;	/*bit6*/
	unsigned RTA_SUSPECT_WL_FOR_BTCS:  			1;	/*bit5*/
	unsigned RTA_RELIABLE_CONDITON_CHECK_WHEEL:	1;	/*bit4*/
	unsigned RTA_2WHEEL_SUSPECT:				1;	/*bit3*/ 
	unsigned ldabsu1DetSpareTire:				1;	/*bit2*/ 
	unsigned RTA_REF_WHEEL:						1;	/*bit1*/ 
	unsigned RTA_DET_BY_WVREF:					1;	/*bit0*/ 
							
};
extern struct RTA_STRUCT	*RTA_WL,RTA_FL,RTA_FR,RTA_RL,RTA_RR;

#endif

/*Global Extern	Variable  Declaration***********************/
extern U8_BIT_STRUCT_t	RTA_1,RTA_2; 

extern struct H_STRUCT		*HW, HFL, HFR, HRL,	HRR;

extern int16_t 	rta_ratio;
extern int16_t	rta_ratio_temp;
extern uint8_t 	ldrtau8RtaState;
extern uint16_t	ldrtau16RtaAbsnConfirmCnt;
extern uint16_t	ldrtau16RtaPresConfirmCnt;
extern int16_t	rta_ratio_calc_sum_0;
extern int16_t	rta_ratio_avg;

/***********************************************************/

/*Global Extern	Functions  Declaration**********************/
extern void	LDRTA_vCallDetection(void);
extern void	LSRTA_vCallRTALogData(void);
/***********************************************************/
#endif

		
		



