#ifndef __LDTCSCALLDETECTION_H__
#define __LDTCSCALLDETECTION_H__

/*includes**************************************************/
#include "LVarHead.h"

/*Global MACRO CONSTANT Definition******************************************************/
#define STALL_HIGH_RISK	3
#define STALL_MID_RISK	2
#define	STALL_LOW_RISK	1


        #define __TCS_MU_DETECTION				ENABLE


#define __TCS_2014_SWD_WINTER	ENABLE


#if (__TCS_MU_DETECTION == ENABLE)

extern int16_t lctcss16ALongNorm;
extern int16_t lctcs161stEstMu;
extern int16_t ldtcs16ALongFilt;
extern int16_t lctcs161stEstMuWtAlat;
extern uint8_t TCS_MU_CONT_FLAG;
extern int16_t TCS_MU_CONT_CMD;
																																			
#endif /*__TCS_MU_DETECTION*/

/*Temperary Calibration Parameter Definition  *******************************************/

#define S16_TCS_BUMP_DETECT_ARAD	-ARAD_15G0

/*Definition of Developing Function ******************************************************/

/*Global MACRO FUNCTION Definition******************************************************/

/*Global Type Declaration *******************************************************************/

/*Global Extern Variable  Declaration*******************************************************************/

/*Global Extern Functions  Declaration******************************************************/


#endif
