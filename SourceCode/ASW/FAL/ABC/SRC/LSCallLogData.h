#ifndef __LSCALLLOGDATA_H__
#define __LSCALLLOGDATA_H__
/*Includes *********************************************************************/
#include "LVarHead.h"
#include "LCHDCCallControl.h"
#include "LCACCCallControl.h"
#include "LCHSACallControl.h"
#include "LCFBCCallControl.h"
#include "LCBDWCallControl.h"
#include "LCEBPCallControl.h"
#include "LCESPInterfaceSlipController.h"


#if __SCC
#include "SCC/LCSCCCallControl.h"
#include "SCC/LCWPCCallControl.h"
#include "SCC/LDSCCCallDection.h"
#endif
/*Global MACRO CONSTANT Definition *********************************************/

/*Global MACRO FUNCTION Definition *********************************************/

/*Global Type Declaration ******************************************************/

/*Global Extern Variable Declaration *******************************************/

//extern uint16_t wdu16StoreAD0Buff[8];
//extern uint16_t wdu16StoreAD1Buff[8];
//extern uint16_t wmu16CurCycleTime;

extern U8_BIT_STRUCT_t VDCF5, VDCF6, VDCF7;
extern U8_BIT_STRUCT_t VDCF10, VDCF11, VDCF16, VDCF18;
extern U8_BIT_STRUCT_t VDCF20, VDCF21, VDCF22, VDCF23;
extern U8_BIT_STRUCT_t VDCF39;
extern struct TF5_BIT TF5;
extern struct OFFSET_STRUCT KSTEER,KYAW,KLAT;
#if defined(__CAN_SW)
extern CC_HDC_FLG_t CF10;
#endif

extern uint16_t u16CycleTime;
extern uint8_t u8CycleTime;
extern uint8_t YawTempCNT;

//extern 	int16_t abs(int16_t j);

/*Global Extern Functions Declaration ******************************************/
#if defined(__LOGGER)
    #if __LOGGER_DATA_TYPE==1
#if !__VDC
	#else 
extern	void LSESP_vCallBetaLogData(void);
//extern	void LSESP_vCallROPLogData(void);
extern	void LSABS_vCallVrefLogData(void);

#if __FBC
extern void LSFBC_vCallFBCLogData(void);
#endif

#if __HDC || __DEC
extern void LSHDC_vCallHDCLogData(void);
#endif

#if __AVH
extern void LSAVH_vCallAVHLogData(void);
#endif

extern uint8_t INHIBITION_INDICATOR(void);

#if __VSM
extern void LSUCC_vCallEPSLogData(void);
#endif

	#endif

#endif

#endif
#endif
