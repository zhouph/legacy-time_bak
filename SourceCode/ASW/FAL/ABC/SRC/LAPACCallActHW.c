/******************************************************************************
* Project Name: CORNER BRAKE CONTROL
* File: LDCBCCallActHW.C
* Date: April. 17. 2006
******************************************************************************/

/* Includes ******************************************************************/


#include "LCPACCallControl.h"
#include "LAPACCallActHw.h"
#include "LACallMain.h"
#include "LCABSDecideCtrlState.h"

#if __PAC
/* Logcal Definiton  *********************************************************/
LAPAC_VALVE_t lapacValveFL, lapacValveFR;
WHEEL_VV_OUTPUT_t lapac_FL1HP, lapac_FL2HP, lapac_FR1HP, lapac_FR2HP;

#define PAC_HV_VL_fl                        lapacValveFL.u1PacHvVlAct
#define PAC_AV_VL_fl                        lapacValveFL.u1PacAvVlAct
#define PAC_HV_VL_fr                        lapacValveFR.u1PacHvVlAct
#define PAC_AV_VL_fr                        lapacValveFR.u1PacAvVlAct

/* Variables Definition*******************************************************/
static void LAPAC_vDecideValveOutput(PAC_WHL_CTRL_t *pPacWL, LAPAC_VALVE_t *laPacValve, WHEEL_VV_OUTPUT_t *laWL1HP,  WHEEL_VV_OUTPUT_t *laWL2HP);


/* LocalFunction prototype ***************************************************/
void LAPAC_vCallActHW(void);
void LAPAC_vCallActValve(void);

/* Implementation*************************************************************/



/******************************************************************************
* FUNCTION NAME:      LAPAC_vCallActHW
* CALLED BY:          LA_vCallMainValve()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Valve/Motor actuaction by CORNER BRAKE CONTROL
******************************************************************************/
void LAPAC_vCallActHW(void)
{
    if(lcpacu1PacActiveFlg==1)
    {
		LAPAC_vDecideValveOutput(&LCPACFL, &lapacValveFL, &lapac_FL1HP, &lapac_FL2HP);
		LAPAC_vDecideValveOutput(&LCPACFR, &lapacValveFR, &lapac_FR1HP, &lapac_FR2HP); 	    
	}
	else
	{
		PAC_HV_VL_fl = 0;
		PAC_AV_VL_fl = 0;
		PAC_HV_VL_fr = 0;
		PAC_AV_VL_fr = 0;

		LA_vResetWheelValvePwmDuty(&lapac_FL1HP);
		LA_vResetWheelValvePwmDuty(&lapac_FL2HP);
		LA_vResetWheelValvePwmDuty(&lapac_FR1HP);
		LA_vResetWheelValvePwmDuty(&lapac_FR2HP);
	}
    LAPAC_vCallActValve(); 
}

void LAPAC_vCallActValve(void)
{
    if (lcpacu1PacActiveFlg==1)
    {
        HV_VL_fl = PAC_HV_VL_fl;
        AV_VL_fl = PAC_AV_VL_fl;
        HV_VL_fr = PAC_HV_VL_fr;
        AV_VL_fr = PAC_AV_VL_fr;        

        if((HV_VL_fl==0) && (AV_VL_fl==0))
        {
        	FL.LFC_Conven_OnOff_Flag = 1;
        }
        else
        {
        	FL.LFC_Conven_OnOff_Flag = 0;
        }
        
		if((HV_VL_fr==0) && (AV_VL_fr==0))
        {
        	FR.LFC_Conven_OnOff_Flag = 1;
        }
        else
        {
        	FR.LFC_Conven_OnOff_Flag = 0;
        }
        
		la_FL1HP = lapac_FL1HP; 
		la_FL2HP = lapac_FL2HP; 
		
		la_FR1HP = lapac_FR1HP; 
		la_FR2HP = lapac_FR2HP; 		        
    }
    else
    {
        ;
    }
}

static void LAPAC_vDecideValveOutput(PAC_WHL_CTRL_t *pPacWL, LAPAC_VALVE_t *laPacValve, WHEEL_VV_OUTPUT_t *laWL1HP,  WHEEL_VV_OUTPUT_t *laWL2HP)
{
	uint8_t u8PacReapplyTime;
	
    if (pPacWL->u8RiseHoldCnt==0)  /* rise phase */
    {
		laPacValve->u1PacHvVlAct = 0;
		laPacValve->u1PacAvVlAct = 0;
		u8PacReapplyTime = pPacWL->u8ConvenReapplyTime;
		LA_vSetWheelVvOnOffRiseOutput(laWL1HP, laWL2HP, u8PacReapplyTime);
	}
	else /*hold phase*/
    {
		laPacValve->u1PacHvVlAct = 1;
		laPacValve->u1PacAvVlAct = 0;
		LA_vSetWheelVvOnOffHoldOutput(laWL1HP, laWL2HP);
	}

  #if defined (__ACTUATION_DEBUG)
	laWL1HP->u8debugger = 33;
	laWL2HP->u8debugger = 33;
  #endif /* defined (__ACTUATION_DEBUG) */
}
#endif