#ifndef __LCTVBBCALLCONTROL_H__
#define __LCTVBBCALLCONTROL_H__
 
/*includes********************************************************************/
#include "LVarHead.h"



  #if __TVBB
/*Global Type Declaration ****************************************************/ 

extern struct  U8_BIT_STRUCT_t TVBB00;
extern struct  U8_BIT_STRUCT_t TVBB01;
extern struct  U8_BIT_STRUCT_t TVBB02; 

#define ldtvbbu1InhibitFlag             TVBB00.bit0
#define ldtvbbu1AccelPedalPushFlag      TVBB00.bit1
#define ldtvbbu1AccelPedalReleaseFlag   TVBB00.bit2
#define ldtvbbu1TurnIntentDetectFlag    TVBB00.bit3
#define lctvbbu1TVBB_ON                 TVBB00.bit4
#define lctvbbu1LeftWhlCtl              TVBB00.bit5
#define lctvbbu1RightWhlCtl             TVBB00.bit6
#define TVBB_MSC_MOTOR_ON               TVBB00.bit7

#define ldtvbbu1ExitByWstrDot           TVBB01.bit0
#define lctvbbu1Direction               TVBB01.bit1
#define ldtvbbu1WstrDotDirChange        TVBB01.bit2
#define ldtvbbu1FrontWhlCtl             TVBB01.bit3
#define ldtvbbu1DriveLineEnable         TVBB01.bit4
#define TVBB_FLAGS_RESERVED01_5         TVBB01.bit5
#define lctvbbu1TODDisable              TVBB01.bit6
#define lctvbbu1TVBB_ON_Old             TVBB01.bit7
         
#define TVBB_FLAGS_RESERVED02_0         TVBB02.bit0
#define TVBB_FLAGS_RESERVED02_1         TVBB02.bit1
#define TVBB_FLAGS_RESERVED02_2         TVBB02.bit2
#define TVBB_FLAGS_RESERVED02_3         TVBB02.bit3
#define TVBB_FLAGS_RESERVED02_4         TVBB02.bit4
#define TVBB_FLAGS_RESERVED02_5         TVBB02.bit5
#define TVBB_FLAGS_RESERVED02_6         TVBB02.bit6
#define TVBB_FLAGS_RESERVED02_7         TVBB02.bit7

extern uint8_t  ldtvbbu8InhibitID;
extern uint8_t  ldtvbbu8AccelPedalPushCnt;
extern uint8_t  ldtvbbu8AccelPedalReleaseCnt;
extern int8_t   ldtvbbs8TurnIntentDetectCnt;
extern int8_t   lctvbbs8ControlEnterTimer;
extern uint8_t  lctvbbu8ControlExitTimer;
extern uint8_t  ldtvbbu8After2WHUScount;
extern int16_t    lctvbbs16ControlMomentPGain, lctvbbs16ControlMomentDGain;
extern int16_t    lctvbbs16ControlMoment;
extern int16_t    ldtvbbs16SlipRatioOfCtlWhl;
extern int16_t    ldtvbbs16EnterSpeedLimit;
extern uint16_t   lctvbbu16TVBBCtlRunningCnt;
extern int16_t    tvbb_msc_target_vol, latvbbs16ExitStartCurrent; 
extern int16_t    tvbb_torq ; 
extern int16_t    tvbb_tempW0, tvbb_tempW1, tvbb_tempW2, tvbb_tempW3, tvbb_tempW4, tvbb_tempW5, tvbb_tempW6;
extern int16_t    ldtvbbs16EnterDelYawThres, ldtvbbs16EnterSlipThres;

#if ((__CAR_MAKER == GM_KOREA) || (__CAR_MAKER == TEST_CAR))
#define __TVBB_ENG_TORQ_UP_CTRL             DISABLE
#define __TVBB_USC_COMB_CONTROL             ENABLE
#else
#define __TVBB_ENG_TORQ_UP_CTRL             DISABLE
#define __TVBB_2WD_TEST                     ENABLE
#define __TVBB_USC_COMB_CONTROL             DISABLE
#endif  
#define __TVBB_OFF_SWITCH                   DISABLE

#define __TVBB_IN_EC                        ENABLE
#define __TVBB_DYNAMIC_THRES                ENABLE

/* Temporary Definition of Calibration Parameters - Start */
#define __TVBB_TC_DUTY_BY_DEL_YAW           ENABLE


/* Temporary Definition of Calibration Parameters - End */

/*Global Extern Functions  Declaration****************************************/
extern void LDTVBB_vDetection(void);
extern void LCMSC_vSetTVBBTargetVoltage(void);
extern void LCTVBB_vCtrlBrake4(void);
extern INT LAMFC_s16SetMFCCurrentTVBB(UCHAR CIRCUIT,INT MFC_Current_old);
  #endif
  
#endif
















































































