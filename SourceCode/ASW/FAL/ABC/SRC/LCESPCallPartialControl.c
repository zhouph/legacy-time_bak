/******************************************************************************
* Project Name: ESP partial performance
* File: LCESPPartialCallControl.C
* Date: Dec. 12. 2005
******************************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCESPCallPartialControl
	#include "Mdyn_autosar.h"
#endif
/* Includes ******************************************************************/

#include "LCESPCallPartialControl.h"
#include "LCESPCalInterpolation.h"
#include "LCESPCalLpf.h"
#include "LCallMain.h"

#if __HDC
#include "LCHDCCallControl.h"
extern U8_BIT_STRUCT_t HDCF1;
#endif

#if __ACC
#include "LCACCCallControl.h"
extern U8_BIT_STRUCT_t ACCF1;
#endif

#if __EBP
#include "LCEBPCallControl.h"
extern 	U8_BIT_STRUCT_t EBPC1;
#include "LDEBPCallDetection.h"
extern 	U8_BIT_STRUCT_t EBPD0;
#endif

#if __PARTIAL_PERFORMANCE
/* Logcal Definiton  *********************************************************/


/* Variables Definition*******************************************************/


/* LocalFunction prototype ***************************************************/
/*void LCESPPartial_vCallControl(void);
void LCESPPartial_vCalYawC(void);
void LCESPPartial_vCalDeltayaw(void);*/


/* Implementation*************************************************************/


/*
void LCESPPartial_vCallControl(void)
{

}

void LCESPPartial_vCalYawC(void)
{

}

void LCESPPartial_vCalDeltayaw(void)
{

}
*/

#endif
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCESPCallPartialControl
	#include "Mdyn_autosar.h"
#endif
