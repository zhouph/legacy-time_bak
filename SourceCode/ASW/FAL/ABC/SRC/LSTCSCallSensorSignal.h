#ifndef __LSTCSCALLSENSORSIGNAL_H__
#define __LSTCSCALLSENSORSIGNAL_H__

/*includes********************************************************************/
#include "LVarHead.h"



/*Global MACRO CONSTANT Definition********************************************/


#if __TCS || __BTC   

/*Global Type Declaration ****************************************************/
typedef struct
{
    uint16_t LSTCS_BIT_07     :1;
    uint16_t LSTCS_BIT_06     :1;
    uint16_t LSTCS_BIT_05     :1;
    uint16_t LSTCS_BIT_04     :1;
    uint16_t LSTCS_BIT_03     :1;
    uint16_t LSTCS_BIT_02     :1;
    uint16_t LSTCS_BIT_01     :1;
    uint16_t LSTCS_BIT_00     :1;
}LSTCS_FLAG_t;

#define lstcsu1FlagReserved7   lstcsFlag01.LSTCS_BIT_07  
#define lstcsu1FlagReserved6   lstcsFlag01.LSTCS_BIT_06  
#define lstcsu1FlagReserved5   lstcsFlag01.LSTCS_BIT_05  
#define lstcsu1FlagReserved4   lstcsFlag01.LSTCS_BIT_04  
#define lstcsu1FlagReserved3   lstcsFlag01.LSTCS_BIT_03  
#define lstcsu1FlagReserved2   lstcsFlag01.LSTCS_BIT_02  
#define lstcsu1FlagReserved1   lstcsFlag01.LSTCS_BIT_01  
#define lstcsu1FlagReserved0   lstcsFlag01.LSTCS_BIT_00  



/*Global Extern Variable  Declaration*****************************************/
extern LSTCS_FLAG_t   lstcsFlag01;


/*Global Extern Functions  Declaration****************************************/
extern void LSTCS_vCallSensorSignal(void);


#endif /* __TCS || __BTC */

#endif /*__LSTCSCALLSENSORSIGNAL_H__*/
