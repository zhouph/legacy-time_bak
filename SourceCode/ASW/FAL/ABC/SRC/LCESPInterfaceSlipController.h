#ifndef __LCESPINTERFACESLIPCONTROLLER_H__
#define __LCESPINTERFACESLIPCONTROLLER_H__

/*Includes *********************************************************************/
#include"LVarHead.h"

/*Global MACRO CONSTANT Definition *********************************************/
#if (__IDB_LOGIC == ENABLE)
extern U8_BIT_STRUCT_t IdbEscSTATE_0  ;
extern int16_t lcidbs16activebrakepress;
extern int16_t lcesps16AllowFnlStrRetime;
#define Not_used_0_7    				IdbEscSTATE_0.bit7
#define Not_used_0_6    				IdbEscSTATE_0.bit6
#define Not_used_0_5					IdbEscSTATE_0.bit5
#define Not_used_0_4					IdbEscSTATE_0.bit4
#define Not_used_0_3					IdbEscSTATE_0.bit3
#define Not_used_0_2					IdbEscSTATE_0.bit2
#define U1EscAfterStrokeR_OLD			IdbEscSTATE_0.bit1
#define U1EscAfterStrokeR			    IdbEscSTATE_0.bit0

#define U8_40Kph_HM_Frt_Rise_rate     3
#define U8_60Kph_HM_Frt_Rise_rate     3
#define U8_80Kph_HM_Frt_Rise_rate     3
#define U8_40Kph_MM_Frt_Rise_rate     3
#define U8_60Kph_MM_Frt_Rise_rate     3
#define U8_80Kph_MM_Frt_Rise_rate     3
#define U8_40Kph_LM_Frt_Rise_rate     3
#define U8_60Kph_LM_Frt_Rise_rate     3
#define U8_80Kph_LM_Frt_Rise_rate     3

#define U8_CBS_10bar_HM_Frt_Rise_rate     2
#define U8_CBS_20bar_HM_Frt_Rise_rate     2
#define U8_CBS_30bar_HM_Frt_Rise_rate     2
#define U8_CBS_10bar_MM_Frt_Rise_rate     2
#define U8_CBS_20bar_MM_Frt_Rise_rate     2
#define U8_CBS_30bar_MM_Frt_Rise_rate     2
#define U8_CBS_10bar_LM_Frt_Rise_rate     2
#define U8_CBS_20bar_LM_Frt_Rise_rate     2
#define U8_CBS_30bar_LM_Frt_Rise_rate     2

#define U8_ABS_30bar_HM_Frt_Rise_rate     2
#define U8_ABS_60bar_HM_Frt_Rise_rate     2
#define U8_ABS_90bar_HM_Frt_Rise_rate     2
#define U8_ABS_30bar_MM_Frt_Rise_rate     2
#define U8_ABS_60bar_MM_Frt_Rise_rate     2
#define U8_ABS_90bar_MM_Frt_Rise_rate     2
#define U8_ABS_30bar_LM_Frt_Rise_rate     2
#define U8_ABS_60bar_LM_Frt_Rise_rate     2
#define U8_ABS_90bar_LM_Frt_Rise_rate     2

#define U8_40Kph_HM_Rear_Rise_rate    3
#define U8_60Kph_HM_Rear_Rise_rate    3
#define U8_80Kph_HM_Rear_Rise_rate    3
#define U8_40Kph_MM_Rear_Rise_rate    4
#define U8_60Kph_MM_Rear_Rise_rate    4
#define U8_80Kph_MM_Rear_Rise_rate    3
#define U8_40Kph_LM_Rear_Rise_rate    4
#define U8_60Kph_LM_Rear_Rise_rate    3
#define U8_80Kph_LM_Rear_Rise_rate    3

#define U8_CBS_10bar_HM_Rear_Rise_rate    3
#define U8_CBS_20bar_HM_Rear_Rise_rate    3
#define U8_CBS_30bar_HM_Rear_Rise_rate    3
#define U8_CBS_10bar_MM_Rear_Rise_rate    3
#define U8_CBS_20bar_MM_Rear_Rise_rate    3
#define U8_CBS_30bar_MM_Rear_Rise_rate    3
#define U8_CBS_10bar_LM_Rear_Rise_rate    3
#define U8_CBS_20bar_LM_Rear_Rise_rate    3
#define U8_CBS_30bar_LM_Rear_Rise_rate    3

#define U8_ABS_30bar_HM_Rear_Rise_rate    3
#define U8_ABS_60bar_HM_Rear_Rise_rate    3
#define U8_ABS_90bar_HM_Rear_Rise_rate    3
#define U8_ABS_30bar_MM_Rear_Rise_rate    3
#define U8_ABS_60bar_MM_Rear_Rise_rate    3
#define U8_ABS_90bar_MM_Rear_Rise_rate    3
#define U8_ABS_30bar_LM_Rear_Rise_rate    3
#define U8_ABS_60bar_LM_Rear_Rise_rate    3
#define U8_ABS_90bar_LM_Rear_Rise_rate    3

#define U8_40Kph_HM_Frt_Rise_delP MPRESS_0BAR
#define U8_60Kph_HM_Frt_Rise_delP MPRESS_2BAR
#define U8_80Kph_HM_Frt_Rise_delP MPRESS_3BAR
#define U8_40Kph_MM_Frt_Rise_delP MPRESS_1BAR
#define U8_60Kph_MM_Frt_Rise_delP MPRESS_3BAR
#define U8_80Kph_MM_Frt_Rise_delP MPRESS_4BAR
#define U8_40Kph_LM_Frt_Rise_delP MPRESS_2BAR
#define U8_60Kph_LM_Frt_Rise_delP MPRESS_3BAR
#define U8_80Kph_LM_Frt_Rise_delP MPRESS_4BAR

#define U8_30Bar_HM_Frt_Rise_delP  MPRESS_4BAR
#define U8_60Bar_HM_Frt_Rise_delP  MPRESS_4BAR
#define U8_90Bar_HM_Frt_Rise_delP  MPRESS_3BAR
#define U8_30Bar_MM_Frt_Rise_delP  MPRESS_4BAR
#define U8_60Bar_MM_Frt_Rise_delP  MPRESS_4BAR
#define U8_90Bar_MM_Frt_Rise_delP  MPRESS_2BAR
#define U8_30Bar_LM_Frt_Rise_delP  MPRESS_5BAR
#define U8_60Bar_LM_Frt_Rise_delP  MPRESS_4BAR
#define U8_90Bar_LM_Frt_Rise_delP  MPRESS_2BAR

#define U8_CBS_10Bar_HM_Frt_Rise_delP  MPRESS_5BAR
#define U8_CBS_20Bar_HM_Frt_Rise_delP  MPRESS_5BAR
#define U8_CBS_30Bar_HM_Frt_Rise_delP  MPRESS_5BAR
#define U8_CBS_10Bar_MM_Frt_Rise_delP  MPRESS_5BAR
#define U8_CBS_20Bar_MM_Frt_Rise_delP  MPRESS_5BAR
#define U8_CBS_30Bar_MM_Frt_Rise_delP  MPRESS_5BAR
#define U8_CBS_10Bar_LM_Frt_Rise_delP  MPRESS_5BAR
#define U8_CBS_20Bar_LM_Frt_Rise_delP  MPRESS_5BAR
#define U8_CBS_30Bar_LM_Frt_Rise_delP  MPRESS_5BAR
               
#define U8_ABS_30Bar_HM_Frt_Rise_delP  MPRESS_5BAR
#define U8_ABS_60Bar_HM_Frt_Rise_delP  MPRESS_5BAR
#define U8_ABS_90Bar_HM_Frt_Rise_delP  MPRESS_5BAR
#define U8_ABS_30Bar_MM_Frt_Rise_delP  MPRESS_5BAR
#define U8_ABS_60Bar_MM_Frt_Rise_delP  MPRESS_5BAR
#define U8_ABS_90Bar_MM_Frt_Rise_delP  MPRESS_5BAR
#define U8_ABS_30Bar_LM_Frt_Rise_delP  MPRESS_5BAR
#define U8_ABS_60Bar_LM_Frt_Rise_delP  MPRESS_5BAR
#define U8_ABS_90Bar_LM_Frt_Rise_delP  MPRESS_5BAR

#define U8_30Bar_HM_Frt_Dump_delP  MPRESS_4BAR
#define U8_60Bar_HM_Frt_Dump_delP  MPRESS_6BAR
#define U8_90Bar_HM_Frt_Dump_delP  MPRESS_6BAR
#define U8_30Bar_MM_Frt_Dump_delP  MPRESS_4BAR
#define U8_60Bar_MM_Frt_Dump_delP  MPRESS_6BAR
#define U8_90Bar_MM_Frt_Dump_delP  MPRESS_6BAR
#define U8_30Bar_LM_Frt_Dump_delP  MPRESS_4BAR
#define U8_60Bar_LM_Frt_Dump_delP  MPRESS_6BAR
#define U8_90Bar_LM_Frt_Dump_delP  MPRESS_6BAR

#define U8_CBS_10Bar_HM_Frt_Dump_delP  MPRESS_8BAR
#define U8_CBS_20Bar_HM_Frt_Dump_delP  MPRESS_8BAR
#define U8_CBS_30Bar_HM_Frt_Dump_delP  MPRESS_8BAR
#define U8_CBS_10Bar_MM_Frt_Dump_delP  MPRESS_4BAR
#define U8_CBS_20Bar_MM_Frt_Dump_delP  MPRESS_6BAR
#define U8_CBS_30Bar_MM_Frt_Dump_delP  MPRESS_9BAR
#define U8_CBS_10Bar_LM_Frt_Dump_delP  MPRESS_4BAR
#define U8_CBS_20Bar_LM_Frt_Dump_delP  MPRESS_6BAR
#define U8_CBS_30Bar_LM_Frt_Dump_delP  MPRESS_9BAR
               
#define U8_ABS_30Bar_HM_Frt_Dump_delP  MPRESS_8BAR
#define U8_ABS_60Bar_HM_Frt_Dump_delP  MPRESS_8BAR
#define U8_ABS_90Bar_HM_Frt_Dump_delP  MPRESS_9BAR
#define U8_ABS_30Bar_MM_Frt_Dump_delP  MPRESS_8BAR
#define U8_ABS_60Bar_MM_Frt_Dump_delP  MPRESS_8BAR
#define U8_ABS_90Bar_MM_Frt_Dump_delP  MPRESS_9BAR
#define U8_ABS_30Bar_LM_Frt_Dump_delP  MPRESS_8BAR
#define U8_ABS_60Bar_LM_Frt_Dump_delP  MPRESS_8BAR
#define U8_ABS_90Bar_LM_Frt_Dump_delP  MPRESS_9BAR

#define U8_30Bar_HM_Rear_Rise_delP  MPRESS_6BAR
#define U8_60Bar_HM_Rear_Rise_delP  MPRESS_6BAR
#define U8_90Bar_HM_Rear_Rise_delP  MPRESS_6BAR
#define U8_30Bar_MM_Rear_Rise_delP  MPRESS_4BAR
#define U8_60Bar_MM_Rear_Rise_delP  MPRESS_4BAR
#define U8_90Bar_MM_Rear_Rise_delP  MPRESS_4BAR
#define U8_30Bar_LM_Rear_Rise_delP  MPRESS_4BAR
#define U8_60Bar_LM_Rear_Rise_delP  MPRESS_4BAR
#define U8_90Bar_LM_Rear_Rise_delP  MPRESS_4BAR

#define U8_CBS_10Bar_HM_Rear_Rise_delP  MPRESS_2BAR
#define U8_CBS_20Bar_HM_Rear_Rise_delP  MPRESS_2BAR
#define U8_CBS_30Bar_HM_Rear_Rise_delP  MPRESS_2BAR
#define U8_CBS_10Bar_MM_Rear_Rise_delP  MPRESS_2BAR
#define U8_CBS_20Bar_MM_Rear_Rise_delP  MPRESS_2BAR
#define U8_CBS_30Bar_MM_Rear_Rise_delP  MPRESS_2BAR
#define U8_CBS_10Bar_LM_Rear_Rise_delP  MPRESS_2BAR
#define U8_CBS_20Bar_LM_Rear_Rise_delP  MPRESS_2BAR
#define U8_CBS_30Bar_LM_Rear_Rise_delP  MPRESS_2BAR
               
#define U8_ABS_30Bar_HM_Rear_Rise_delP  MPRESS_2BAR
#define U8_ABS_60Bar_HM_Rear_Rise_delP  MPRESS_2BAR
#define U8_ABS_90Bar_HM_Rear_Rise_delP  MPRESS_2BAR
#define U8_ABS_30Bar_MM_Rear_Rise_delP  MPRESS_2BAR
#define U8_ABS_60Bar_MM_Rear_Rise_delP  MPRESS_2BAR
#define U8_ABS_90Bar_MM_Rear_Rise_delP  MPRESS_2BAR
#define U8_ABS_30Bar_LM_Rear_Rise_delP  MPRESS_2BAR
#define U8_ABS_60Bar_LM_Rear_Rise_delP  MPRESS_2BAR
#define U8_ABS_90Bar_LM_Rear_Rise_delP  MPRESS_2BAR

#define U8_30Bar_HM_Rear_Dump_delP  MPRESS_2BAR
#define U8_60Bar_HM_Rear_Dump_delP  MPRESS_2BAR
#define U8_90Bar_HM_Rear_Dump_delP  MPRESS_2BAR
#define U8_30Bar_MM_Rear_Dump_delP  MPRESS_2BAR
#define U8_60Bar_MM_Rear_Dump_delP  MPRESS_2BAR
#define U8_90Bar_MM_Rear_Dump_delP  MPRESS_2BAR
#define U8_30Bar_LM_Rear_Dump_delP  MPRESS_2BAR
#define U8_60Bar_LM_Rear_Dump_delP  MPRESS_2BAR
#define U8_90Bar_LM_Rear_Dump_delP  MPRESS_2BAR

#define U8_CBS_10Bar_HM_Rear_Dump_delP  MPRESS_4BAR
#define U8_CBS_20Bar_HM_Rear_Dump_delP  MPRESS_4BAR
#define U8_CBS_30Bar_HM_Rear_Dump_delP  MPRESS_4BAR
#define U8_CBS_10Bar_MM_Rear_Dump_delP  MPRESS_4BAR
#define U8_CBS_20Bar_MM_Rear_Dump_delP  MPRESS_4BAR
#define U8_CBS_30Bar_MM_Rear_Dump_delP  MPRESS_4BAR
#define U8_CBS_10Bar_LM_Rear_Dump_delP  MPRESS_4BAR
#define U8_CBS_20Bar_LM_Rear_Dump_delP  MPRESS_4BAR
#define U8_CBS_30Bar_LM_Rear_Dump_delP  MPRESS_4BAR
               
#define U8_ABS_30Bar_HM_Rear_Dump_delP MPRESS_3BAR
#define U8_ABS_60Bar_HM_Rear_Dump_delP MPRESS_3BAR
#define U8_ABS_90Bar_HM_Rear_Dump_delP MPRESS_3BAR
#define U8_ABS_30Bar_MM_Rear_Dump_delP MPRESS_3BAR
#define U8_ABS_60Bar_MM_Rear_Dump_delP MPRESS_3BAR
#define U8_ABS_90Bar_MM_Rear_Dump_delP MPRESS_3BAR
#define U8_ABS_30Bar_LM_Rear_Dump_delP MPRESS_3BAR
#define U8_ABS_60Bar_LM_Rear_Dump_delP MPRESS_3BAR
#define U8_ABS_90Bar_LM_Rear_Dump_delP MPRESS_3BAR

#define S16_TARGET_PRESS_RATE_100BAR		100
#define S16_TARGET_PRESS_RATE_200BAR		200
#define S16_TARGET_PRESS_RATE_300BAR		300
#endif
/*Global MACRO FUNCTION Definition *********************************************/

/*Global Type Declaration ******************************************************/

/*Global Extern Variable Declaration *******************************************/

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//extern int16_t    rate_counter_front,rate_counter_rear, rate_counter_fl, rate_counter_fr, rate_counter_rl, rate_counter_rr;
//extern int16_t    slip_control_rear;  
//extern int16_t    slip_control_front;    
//extern int16_t    slip_error_dot_prev_fl, slip_error_dot_prev_fr,slip_error_dot_prev_rl,slip_error_dot_prev_rr,slip_error_dot_prev_front,slip_error_dot_prev_rear;  
//extern int16_t    slip_error_prev_fl,slip_error_prev_fr,slip_error_prev_rl,slip_error_prev_rr;
//extern int16_t    rate_interval_front,rate_interval_rear , rate_interval_fl, rate_interval_fr, rate_interval_rl, rate_interval_rr;
//extern int16_t true_slip_target_fl_old, true_slip_target_fr_old;
//extern int16_t true_slip_target_fl, true_slip_target_fr;
//extern int16_t true_slip_target_rl, true_slip_target_rr;
//extern int16_t true_slip_target_rl_old, true_slip_target_rr_old;
//extern int16_t slip_control_need_counter_fl,slip_control_need_counter_fr,slip_control_need_counter_rl,slip_control_need_counter_rr;
//extern int16_t esp_pressure_count_fl, esp_pressure_count_fr,esp_pressure_count_rl,esp_pressure_count_rr;
#else
extern int16_t    rate_counter_front,rate_counter_rear, rate_counter_fl, rate_counter_fr, rate_counter_rl, rate_counter_rr;
extern int16_t    slip_control_rear;  
extern int16_t    slip_control_front;    
extern int16_t    slip_error_dot_prev_fl, slip_error_dot_prev_fr,slip_error_dot_prev_rl,slip_error_dot_prev_rr,slip_error_dot_prev_front,slip_error_dot_prev_rear;  
extern int16_t    slip_error_prev_fl,slip_error_prev_fr,slip_error_prev_rl,slip_error_prev_rr;
extern int16_t    rate_interval_front,rate_interval_rear , rate_interval_fl, rate_interval_fr, rate_interval_rl, rate_interval_rr;

extern int16_t true_slip_target_fl_old, true_slip_target_fr_old;
extern int16_t true_slip_target_fl, true_slip_target_fr;
extern int16_t true_slip_target_rl, true_slip_target_rr;
extern int16_t true_slip_target_rl_old, true_slip_target_rr_old;

extern int16_t slip_control_need_counter_fl,slip_control_need_counter_fr,slip_control_need_counter_rl,slip_control_need_counter_rr;
extern int16_t esp_pressure_count_fl, esp_pressure_count_fr,esp_pressure_count_rl,esp_pressure_count_rr;
#endif

extern int16_t esp_pulse_down_dump_rate_confirm, esp_pulse_down_dump_max_slip;
extern int16_t variable_rise_scan;
extern int16_t variable_rise_scan_rear;

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//extern int16_t slip_control_front_FL;
//extern int16_t slip_control_front_FR; 
//extern int16_t slip_r_rise_thr_change, slip_f_rise_thr_change;
#else
extern int16_t slip_control_front_FL;
extern int16_t slip_control_front_FR; 
extern int16_t slip_r_rise_thr_change, slip_f_rise_thr_change;
#endif

extern int16_t esp_pressure_count_fl_old,esp_pressure_count_fr_old,esp_pressure_count_rl_old,esp_pressure_count_rr_old;

extern int16_t pulse_dump_time_front_fl, pulse_dump_time_front_fr;
extern int16_t  pulse_dump_time_rear_rl, pulse_dump_time_rear_rr;

extern int16_t tc_close_time_fl,tc_close_time_fr,tc_close_time_rl,tc_close_time_rr;

extern int16_t esp_pulse_down_dump_rate_confirm_fl, esp_pulse_down_dump_rate_confirm_fr;
extern int16_t esp_pulse_down_dump_rate_confirm_rl, esp_pulse_down_dump_rate_confirm_rr;

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//extern int16_t slip_f_dump_thr_change, slip_r_dump_thr_change;
#else
extern int16_t slip_f_dump_thr_change, slip_r_dump_thr_change;
#endif

extern int16_t lcs16espSlipratefl,lcs16espSlipratefr, lcs16espSlipraterl, lcs16espSlipraterr; 
extern int16_t new_pulse_down_rate_fl,new_pulse_down_rate_fr,new_pulse_down_rate_rl,new_pulse_down_rate_rr;

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//extern int16_t slip_target_prev_front,slip_target_prev_rear, slip_target_prev_fl, slip_target_prev_fr,slip_target_prev_rl, slip_target_prev_rr, slip_target_prev;
//extern int16_t slip_error_prev_front, slip_error_prev_rear, slip_error_prev_fl, slip_error_prev_fr, slip_error_prev_rl, slip_error_prev_rr;
#else
extern int16_t slip_target_prev_front,slip_target_prev_rear, slip_target_prev_fl, slip_target_prev_fr,slip_target_prev_rl, slip_target_prev_rr, slip_target_prev;
extern int16_t slip_error_prev_front, slip_error_prev_rear, slip_error_prev_fl, slip_error_prev_fr, slip_error_prev_rl, slip_error_prev_rr;
#endif

extern int16_t slip_m_fl_current, slip_m_fr_current, slip_m_rl_current, slip_m_rr_current;

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//extern int16_t slip_control_rear, slip_control_P_gain_rear, slip_control_D_gain_rear;
//extern int16_t slip_control_front, slip_control_P_gain_front, slip_control_D_gain_front;
//extern int16_t slip_error_front,  slip_error_rear, slip_error_dot_front, slip_error_dot_rear;
//extern int16_t slip_target_front, slip_target_rear, slip_measurement_front, slip_measurement_rear;
#else
extern int16_t slip_control_rear, slip_control_P_gain_rear, slip_control_D_gain_rear;
extern int16_t slip_control_front, slip_control_P_gain_front, slip_control_D_gain_front;
extern int16_t slip_error_front,  slip_error_rear, slip_error_dot_front, slip_error_dot_rear;
extern int16_t slip_target_front, slip_target_rear, slip_measurement_front, slip_measurement_rear;
#endif

extern int16_t slip_error_dot_front_nflt, slip_error_dot_rear_nflt;

/*  CRAM   */
extern int16_t slip_status_position ;


#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//extern int16_t lcespCalRiserateAddDutyfl,lcespCalRiserateAddDutyfr, lcespCalRiserateAddDutyrl,lcespCalRiserateAddDutyrr;
//    extern int16_t del_slip_gain_front; 
//    extern int16_t del_wheel_sp_gain_front; 
//    extern int16_t del_slip_gain_rear; 
//    extern int16_t del_wheel_sp_gain_rear;
#else
extern int16_t lcespCalRiserateAddDutyfl,lcespCalRiserateAddDutyfr, lcespCalRiserateAddDutyrl,lcespCalRiserateAddDutyrr;
    extern int16_t del_slip_gain_front; 
    extern int16_t del_wheel_sp_gain_front; 
    extern int16_t del_slip_gain_rear; 
    extern int16_t del_wheel_sp_gain_rear; 
#endif
    
	extern int16_t gain_speed_whrate_depend;
	extern int16_t lcesps16EstPressFromDelM_FL;
	extern int16_t lcesps16EstPressFromDelM_FR;
	extern int16_t lcesps16EstPressFromDelM_RL;
	extern int16_t lcesps16EstPressFromDelM_RR;
	extern int16_t lcesps16KeepEstPress;
	extern int16_t lcesps16SlipThreshold;
	
/*Global Extern Functions Declaration ******************************************/


///* ABS/CBS Combination Control TP */
//#define	    S16_ABS_CBS_COMB_HMU_LIMIT_SLIP				2000         
//#define     S16_ABS_CBS_COMB_MMU_LIMIT_SLIP       3000  
//#define     S16_ABS_CBS_COMB_LMU_LIMIT_SLIP     	4000
//#define     S16_ABS_CBS_COMB_ADD_MIN_WHEEL_SLIP		500
//#define	    S16_ABS_CBS_COMB_HMU_GAIN         		10000
//#define     S16_ABS_CBS_COMB_MMU_GAIN         		12000
//#define     S16_ABS_CBS_COMB_LMU_GAIN     				14000
//#define     S16_ABS_CBS_COMB_PRESSURE_MARGIN			10
//#define	    S16_ABS_CBS_COMB_PRESSURE_GAIN        100 
//#define     S16_ABS_CBS_COMB_PRESSURE_OFFSET      70   
///* ABS/CBS Combination Control TP */   

#endif
