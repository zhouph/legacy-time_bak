/*******************************************************************************
* Project Name:		MGH40_ESP
* File Name:		LAFBCCallActHW.c
* Description:		Actuate Valves of FBC
* Logic version:	HV26
********************************************************************************
*  Modification Log
*  Date			Author			Description
*  -----		-----			-----
*  5C15			eslim			Initial Release
*  6120			eslim			Motor speed control request
*  6216			eslim			Modified based on MISRA rule
********************************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START                       
	#define idx_FILE	idx_CL_LABACallActHW
	#include "Mdyn_autosar.h"               
#endif
/* Includes ********************************************************************/


#include "LCHDCCallControl.h"
#include "LCACCCallControl.h"
#include "LCHSACallControl.h"
#include "LCFBCCallControl.h"
#include "LCPBACallControl.h"
#include "LACallMain.h"
#include "LDABSDctVehicleStatus.h"
#include "LCABSDecideCtrlState.h"
#include "LABACallActHW.h"

#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
#include "LSESPFilterEspSensor.h"
#include "LSESPCalSensorOffset.h"
#endif

#if __ENABLE_PREFILL
#include "LAEPCCallActHW.h"
#endif

#include "LAABSCallMotorSpeedControl.h"
#include "LCDECCallControl.h"


#if __VDC
/* Logcal Definiton ************************************************************/


/* Variables Definition ********************************************************/

int16_t	BA_hold_timer;
int16_t	BA_ABS_timer;

int16_t pba_msc_target_vol;

#if __FBC
int16_t fbc_msc_target_vol;
#endif

#if  ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
int16_t labas16TcTargetPress;
#endif

U8_BIT_STRUCT_t BAF0;

WHEEL_VV_OUTPUT_t  laBA_FL1HP,laBA_FR1HP,laBA_RL1HP,laBA_RR1HP;

/* Local Function prototype ****************************************************/

void LABA_vCallActHW(void);
void LABA_vActStandbyMode(void);
void LABA_vActApplyMode(void);
void LABA_vActHoldMode(void);
void LABA_vActDumpMode(void);
void LABA_vInitializeVariables(void);
void LABA_vInhibitCheck(void);
#if __ADVANCED_MSC
void LCMSC_vSetBATargetVoltage(void);
#endif
#if  ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
static void LABA_vTcTagetPress(void);
#endif

extern void LCPBA_vResetVaribles(void);

/* Implementation **************************************************************/

/*******************************************************************************
* FUNCTION NAME:		LAFBC_vCallActHW
* CALLED BY:			LA_vCallMain()
* Preconditions:		none
* PARAMETER:			none
* RETURN VALUE:			none
* Description:			Sensor Signal of FBC
********************************************************************************/

void LABA_vCallActHW(void)
{

    LA_vResetWheelValvePwmDuty(&laBA_FL1HP);  
    LA_vResetWheelValvePwmDuty(&laBA_FR1HP);
    LA_vResetWheelValvePwmDuty(&laBA_RL1HP);  
    LA_vResetWheelValvePwmDuty(&laBA_RR1HP);
    
    LABA_vInhibitCheck();
        
    if((labau1InhibitFlg==0)&&(fu1OnDiag==0)&&(init_end_flg==1)&&(wu8IgnStat==CE_ON_STATE)&&(BTC_fz==0)
    #if __GM_FailM 
      &&(fu1McpSenPwr1sOk==1)
    #else
      &&(fou1SenPwr1secOk==1)&&(vdc_on_flg==1)
    #endif
     ) 
    {

        #if __ENABLE_PREFILL
                if (EPC_ON==0)
                {
        #endif
                if(speed_calc_timer >= (uint8_t)L_U8_TIME_10MSLOOP_1500MS)
                {
                	if((PBA_mode == PBA_APPLY )
                		#if __FBC
                		||(FBC_mode == FBC_APPLY )
                		#endif
                	)
					{
						BA_mode = BA_APPLY;
						LABA_vActApplyMode();
				    }
				    else if((PBA_mode == PBA_HOLD )
				    	#if __FBC
                		||(FBC_mode == FBC_HOLD )
                		#endif
                		)
				    {
				    	BA_mode = BA_HOLD;
				    	LABA_vActHoldMode();
				    }
				    else if((PBA_mode == PBA_DUMP )
				    	#if __FBC
                		||(FBC_mode == FBC_DUMP )
                		#endif
                		)
				    {
				    	BA_mode = BA_DUMP;
				    	LABA_vActDumpMode();
				    }
				    else
				    {
				    	BA_mode = BA_STANDBY;
				    	LABA_vActStandbyMode();
					} 
                }
                else
                {
                      ;
                }
                
      #if __ENABLE_PREFILL
                }
                else
                {
                	#if __FBC
                    LCFBC_vInitializeVariables();
                    #endif
                    LCPBA_vInitializeVariables();
                    LABA_vInitializeVariables();
                }
      #endif
              
	  }
    else  /* if(!ESP_ERROR_FLG) */
    {
     	#if __FBC
         LCFBC_vInitializeVariables();
         #endif
         LCPBA_vInitializeVariables();
         LABA_vInitializeVariables();
    }
            
/*            if(((PBA_ON==0)&&(PBA_WORK==0))   */
   if(((PBA_ON==0)&&(PBA_WORK==0)&&(PBA_EXIT_CONTROL==0))
      #if __FBC
       &&((FBC_ON==0)&&(FBC_WORK==0))
      #endif
      )
    {
    	LABA_vInitializeVariables();
    }
 
  #if  ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    LABA_vTcTagetPress();
  #endif
}
void LABA_vInitializeVariables(void)
{
	BA_hold_timer = 0;
	BA_ABS_timer = 0;
	PBA_pulsedown = 0;
	BA_mode = BA_STANDBY;

  PBA_ON=0;
  PBA_WORK=0;
  PBA_ACT = 0; 
  PBA_ON_old = 0;
  PBA_EXIT_CONTROL = 0;
  PBA_EXIT_CONTROL_cnt = 0;  
  PBA_MSC_MOTOR_ON = 0;
  
  #if __FBC
  FBC_ON=0;
  FBC_WORK=0;
  FBC_MSC_MOTOR_ON = 0;
  #endif 
  
  LABA_vActStandbyMode();
	
}
void LABA_vActStandbyMode(void)
{
    
    if(BTCS_ON==0)
    {    
        VDC_MOTOR_ON=0;
    
      #if !__SPLIT_TYPE
        TCL_DEMAND_fl=0;
        TCL_DEMAND_fr=0;
        S_VALVE_LEFT=0;
        S_VALVE_RIGHT=0;
      #else
        TCL_DEMAND_PRIMARY=0; 
        TCL_DEMAND_SECONDARY=0;
        S_VALVE_PRIMARY=0; 
        S_VALVE_SECONDARY=0;
      #endif
    }
    else{}

    if((ABS_fz==0)&&(ESP_PULSE_DUMP_FLAG==0)&&(BTCS_ON==0))
    {
        HV_VL_fl=0; AV_VL_fl=0;
        HV_VL_fr=0; AV_VL_fr=0;
        if(EBD_RA==0) {
            HV_VL_rl=0; AV_VL_rl=0;
            HV_VL_rr=0; AV_VL_rr=0;
        }
        else
        {
        	;
        }
    }
    else
    {
    	;
    }

    if(((!PBA_ACT)&&(PBA_WORK))
   		#if __FBC
    	||((!FBC_ON)&&(FBC_WORK))
    	#endif
    	)    	
    {
        BA_hold_timer++;
#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
		if(((PBA_ACT==0)&&(PBA_WORK==1)
		  &&((lsesps16EstBrkPressByBrkPedalF<MPRESS_80BAR)&&(lsespu1BrkAppSenInvalid == 0))
		  &&(esp_mu>=U16_PBA_ESP_MU_MEDIUM))
   		#if __FBC
    	||((FBC_ON==0)&&(FBC_WORK==1)
    	  &&((lsesps16EstBrkPressByBrkPedalF<MPRESS_80BAR)&&(lsespu1BrkAppSenInvalid == 0))
		  &&(esp_mu>=S16_FBC_ESP_MU_MEDIUM))
    	#endif
#else
		if(((!PBA_ACT)&&(PBA_WORK)&&(mpress<MPRESS_80BAR)&&(esp_mu>=U16_PBA_ESP_MU_MEDIUM))
   		#if __FBC
    	||((!FBC_ON)&&(FBC_WORK)&&(mpress<MPRESS_80BAR)&&(esp_mu>=S16_FBC_ESP_MU_MEDIUM))
    	#endif
#endif
    	)
        {
			tempW0=7;
            tempW4=1;
        }
        else
        {
        	PBA_WORK=0;
        	#if __FBC
        		FBC_WORK=0;
        	#endif
        }

        if(BA_hold_timer>=7)
        {
        	PBA_WORK=0;
        	#if __FBC
        		FBC_WORK=0;
        	#endif
        }
        else
        {
        	;
        }
    } 
}

/*******************************************************************************
* FUNCTION NAME:		LAFBC_vActApplyMode
* CALLED BY:			LAFBC_vCallActHW()
* Preconditions:		none
* PARAMETER:			none
* RETURN VALUE:			none
* Description:			Actuate Valves in Apply Mode of FBC
********************************************************************************/
void LABA_vActApplyMode(void)
{
#if !__SPLIT_TYPE
    TCL_DEMAND_fl=1; TCL_DEMAND_fr=1;
#else
    TCL_DEMAND_PRIMARY=1; TCL_DEMAND_SECONDARY=1;
#endif

 	#if __TSP && __TSP_PBA_COMBINATION==1
 		if(lctspu1TspPbaComb == 1)
 		{
 			VDC_MOTOR_ON=0;
 			BA_ABS_timer=250;
	  #if !__SPLIT_TYPE
	    S_VALVE_LEFT=0; 
	    S_VALVE_RIGHT=0;
      #else
	    S_VALVE_PRIMARY=0; 
	    S_VALVE_SECONDARY=0;
        #endif
		
 		} 			
    else if(ABS_fz)
  #else
    if(ABS_fz)
  #endif
    {
        VDC_MOTOR_ON=0;
        /* for double braking */
        if(BA_ABS_timer<L_U8_TIME_10MSLOOP_1750MS)
        {
        	BA_ABS_timer++;

        	if(BA_ABS_timer<=L_U8_TIME_10MSLOOP_200MS)
        	{/*  ABS  0.007*28=0.196 sec  */
        		VDC_MOTOR_ON=1;
    			BA_hold_timer++;
				
				if(BA_hold_timer>30000)
				{
					BA_hold_timer=30000;
				}
				else
				{
					;
				}

			  #if !__SPLIT_TYPE
				S_VALVE_LEFT=1; 
				S_VALVE_RIGHT=1;
        	  #else
				S_VALVE_PRIMARY=1; 
				S_VALVE_SECONDARY=1;
        	  #endif
        	}
        	else {
        		if((HV_VL_fl==1) && (AV_VL_fl==1))
        		{
        			BA_ABS_timer=L_U8_TIME_10MSLOOP_1750MS;
        		}
        		else
        		{
        			;
        		}
        		
        		if((HV_VL_fr==1) && (AV_VL_fr==1))
        		{
        			BA_ABS_timer=L_U8_TIME_10MSLOOP_1750MS;
        		}
        		else
        		{
        			;
        		}

			  #if !__SPLIT_TYPE
        		S_VALVE_LEFT=0; 
        		S_VALVE_RIGHT=0;	    
        	  #else
				S_VALVE_PRIMARY=0; 
				S_VALVE_SECONDARY=0;
        	  #endif
        		
        	}
        }
        else
        {
        	;
        }
    }
    else
    {
        /* for double braking */
        
    	if(((PBA_ON==1)&&(vdc_vref>=(int16_t)U16_PBA_SPEED_ENTER_THR)&& (BA_ABS_timer<L_U8_TIME_10MSLOOP_1750MS ))
    		#if __FBC
        		||((FBC_ON==1)&&(vdc_vref>=S16_FBC_SPEED_ENTER_THR-320)&&( BA_ABS_timer<L_U8_TIME_10MSLOOP_1750MS))
        	#endif
        	)
    	{
			VDC_MOTOR_ON=1;
    		BA_hold_timer++;
			if(BA_hold_timer>30000)
			{
				BA_hold_timer=30000;
			}
			else
			{
				;
			}
			
			#if !__SPLIT_TYPE
			S_VALVE_LEFT=1; 
			S_VALVE_RIGHT=1;
	    	#else
			S_VALVE_PRIMARY=1; 
			S_VALVE_SECONDARY=1;
	    	#endif
        }
        else
        {
			#if !__SPLIT_TYPE
        	S_VALVE_LEFT=0; 
        	S_VALVE_RIGHT=0;
        	#else
			S_VALVE_PRIMARY=0; 
			S_VALVE_SECONDARY=0;
        	#endif
            VDC_MOTOR_ON=0;
        }

    #if	(__PBA_PRE_ABS_ENABLE == 1)
		if(lcpbau1PreABSActinPBA == 1)
		{
			if((ls16PBAPDTCount%5) <= 1)
			{
		        HV_VL_fl=0; AV_VL_fl=0;
		        HV_VL_fr=0; AV_VL_fr=0;
			}
			else
			{	
		        HV_VL_fl=1; AV_VL_fl=0;
		        HV_VL_fr=1; AV_VL_fr=0;
		        
		        LA_vSetNominalWheelValvePwmDuty(HV_VL_fl,AV_VL_fl,U8_BASE_CTRLTIME,U8_BASE_CTRLTIME,&laBA_FL1HP);
		        LA_vSetNominalWheelValvePwmDuty(HV_VL_fr,AV_VL_fr,U8_BASE_CTRLTIME,U8_BASE_CTRLTIME,&laBA_FR1HP);
		        
		        la_FL1HP = laBA_FL1HP;
                la_FL2HP = laBA_FL1HP;
                la_FR1HP = laBA_FR1HP;  
                la_FR2HP = laBA_FR1HP;  
		    }						
		}
		else
		{
	        HV_VL_fl=0; AV_VL_fl=0;
	        HV_VL_fr=0; AV_VL_fr=0;
      	}
	#else
        HV_VL_fl=0; AV_VL_fl=0;
        HV_VL_fr=0; AV_VL_fr=0;
    #endif

        if(EBD_RA==0)
        {
            HV_VL_rl=0; AV_VL_rl=0;
            HV_VL_rr=0; AV_VL_rr=0;
        }
        else
        {
        	;
        }
    }
}

/*******************************************************************************
* FUNCTION NAME:		LAFBC_vActHoldMode
* CALLED BY:			LAFBC_vCallActHW()
* Preconditions:		none
* PARAMETER:			none
* RETURN VALUE:			none
* Description:			Actuate Valves in Hold Mode of FBC
********************************************************************************/
void LABA_vActHoldMode(void)
{
    VDC_MOTOR_ON=0;

#if !__SPLIT_TYPE
    TCL_DEMAND_fl=1;
    TCL_DEMAND_fr=1;
    S_VALVE_LEFT=0;
    S_VALVE_RIGHT=0;

#else
    TCL_DEMAND_PRIMARY=1; 
    TCL_DEMAND_SECONDARY=1;
    S_VALVE_PRIMARY=0; 
    S_VALVE_SECONDARY=0;
#endif

    if(!ABS_fz)
    {
        HV_VL_fl=1; AV_VL_fl=0;
        HV_VL_fr=1; AV_VL_fr=0;

        LA_vSetNominalWheelValvePwmDuty(HV_VL_fl,AV_VL_fl,U8_BASE_CTRLTIME,U8_BASE_CTRLTIME,&laBA_FL1HP);
		LA_vSetNominalWheelValvePwmDuty(HV_VL_fr,AV_VL_fr,U8_BASE_CTRLTIME,U8_BASE_CTRLTIME,&laBA_FR1HP);
		        
        la_FL1HP = laBA_FL1HP;
        la_FL2HP = laBA_FL1HP;
        la_FR1HP = laBA_FR1HP;  
        la_FR2HP = laBA_FR1HP;  

        if(!EBD_RA)
        {
            HV_VL_rl=1; AV_VL_rl=0;
            HV_VL_rr=1; AV_VL_rr=0;
            
            LA_vSetNominalWheelValvePwmDuty(HV_VL_rl,AV_VL_rl,U8_BASE_CTRLTIME,U8_BASE_CTRLTIME,&laBA_RL1HP);
    		LA_vSetNominalWheelValvePwmDuty(HV_VL_rr,AV_VL_rr,U8_BASE_CTRLTIME,U8_BASE_CTRLTIME,&laBA_RR1HP);
    		        
            la_RL1HP = laBA_RL1HP;
            la_RL2HP = laBA_RL1HP;
            la_RR1HP = laBA_RR1HP;  
            la_RR2HP = laBA_RR1HP;
        }
        else
        {
        	;
        }
    }
    else
    {
    	;
    }
}

/*******************************************************************************
* FUNCTION NAME:		LAFBC_vActDumpMode
* CALLED BY:			LAFBC_vCallActHW()
* Preconditions:		none
* PARAMETER:			none
* RETURN VALUE:			none
* Description:			Actuate Valves in Dump Mode of FBC
********************************************************************************/
void LABA_vActDumpMode(void)
{
	esp_tempW0=PBA_STANDARD;

#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
	if(((PBA_ACT==1)&&((lsesps16EstBrkPressByBrkPedalF<S16_PBA_PRESS_EXIT_THR3)&&(lsespu1BrkAppSenInvalid==0)))
	#if __FBC
	  ||((FBC_ON==1)&&((lsesps16EstBrkPressByBrkPedalF<S16_PBA_PRESS_EXIT_THR3)&&(lsespu1BrkAppSenInvalid==0)))
	#endif
#else
	if(((PBA_ACT==1)&&(mpress < S16_PBA_PRESS_EXIT_THR3))
	#if __FBC
		||((FBC_ON==1)&&(mpress < S16_FBC_PRESS_EXIT_THR3))
	#endif
#endif
		)
	{
		if((PBA_ACT==1)&&(BA_hold_timer < PBA_DUMP_MODE_MAX_TIME)
		#if __FBC
			||(FBC_ON==1)&&(BA_hold_timer <U8_FBC_DUMP_MODE_MAX_TIME)
		#endif
		)
		{
			BA_hold_timer++;
			
			if(((PBA_ACT==1)&&(BA_hold_timer%PBA_DUMP_MODE_HOLD_TIME == 1))
		#if __FBC
			||((FBC_ON==1)&&(BA_hold_timer%U8_FBC_DUMP_MODE_HOLD_TIME == 1))
		#endif
			)
			{
				esp_tempW0=PBA_DUMP;
			}
			else
			{
				esp_tempW0=PBA_HOLD;
			}
			
			if((PBA_ACT==1)
			#if __FBC
				||(FBC_ON)
			#endif
				)
			{
				PBA_pulsedown = 1;
			}
			else
			{
				PBA_pulsedown = 0;
			}
		}
		else
		{
			PBA_WORK = 0;
			LCPBA_vResetVaribles();
			#if __FBC
			FBC_WORK = 0;
			LCFBC_vResetVariable();
			#endif
       		LABA_vInitializeVariables();
       		PBA_pulsedown = 0;
		}
	}
	else
	{
		if((PBA_ACT==1)&&(BA_hold_timer < PBA_DUMP_MODE_MAX_TIME)
		#if __FBC
			||(FBC_ON==1)&&(BA_hold_timer <U8_FBC_DUMP_MODE_MAX_TIME)
		#endif
		)
		{
			BA_hold_timer++;
			
			if(((PBA_ACT==1)&&(BA_hold_timer%PBA_DUMP_MODE_HOLD_TIME == 1))
		#if __FBC
			||((FBC_ON==1)&&(BA_hold_timer%U8_FBC_DUMP_MODE_HOLD_TIME == 1))
		#endif
			)
			{
				esp_tempW0=PBA_DUMP;
			}
			else
			{
				esp_tempW0=PBA_HOLD;
			}
			if((PBA_ACT==1)
			#if __FBC
				||(FBC_ON)
			#endif
			)
			{
				PBA_pulsedown = 1;
			}
			else
			{
				PBA_pulsedown = 0;
			}
#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
			if(((PBA_ACT == 1)
			  &&((lsesps16EstBrkPressByBrkPedalF >= U16_PBA_PRESS_ENTER_THR)&&(lsespu1BrkAppSenInvalid == 0))
			  &&(ABS_fz==0))
			#if __FBC
			  ||((FBC_ON == 1)
			  &&((lsesps16EstBrkPressByBrkPedalF >= U16_PBA_PRESS_ENTER_THR)&&(lsespu1BrkAppSenInvalid == 0))
			  &&(ABS_fz==0))
			 #endif
#else
			if(((PBA_ACT == 1)&&(mpress >= U16_PBA_PRESS_ENTER_THR)&&(ABS_fz==0))
			#if __FBC
			 	||((FBC_ON == 1)&&(mpress >= S16_BRAKE_LOWMU_ABSPRESS)&&(ABS_fz==0))
			 #endif
#endif
			 )
    		{
    			if(PBA_ACT == 1)
             	{
             		PBA_mode=PBA_HOLD;
             	}
			#if __FBC
				if(FBC_ON==1)
				{
					FBC_mode=FBC_HOLD;
				}
			#endif
             	PBA_pulsedown = 0 ;
 //            	trans_mpress=mpress;
    		}
		}
		else
		{
			PBA_WORK = 0;
			LCPBA_vResetVaribles();
			#if __FBC
			FBC_WORK = 0;
			LCFBC_vResetVariable();
			#endif
			LABA_vInitializeVariables();
		}
	}

    VDC_MOTOR_ON=0;

#if !__SPLIT_TYPE
    TCL_DEMAND_fl=1;
    TCL_DEMAND_fr=1;
    S_VALVE_LEFT=0;
    S_VALVE_RIGHT=0;

#else
    TCL_DEMAND_PRIMARY=1;
    TCL_DEMAND_SECONDARY=1;
    S_VALVE_PRIMARY=0;
    S_VALVE_SECONDARY=0;
#endif

	switch(esp_tempW0)
	{
		case PBA_STANDARD:
    	    HV_VL_fl=0; AV_VL_fl=0;
       		HV_VL_fr=0; AV_VL_fr=0;

            HV_VL_rl=0; AV_VL_rl=0;
   	    	HV_VL_rr=0; AV_VL_rr=0;
			break;

		case PBA_DUMP:
    	    HV_VL_fl=1; AV_VL_fl=1;
       		HV_VL_fr=1; AV_VL_fr=1;

            HV_VL_rl=1; AV_VL_rl=1;
   	    	HV_VL_rr=1; AV_VL_rr=1;
			break;

		case PBA_HOLD:
    	    HV_VL_fl=1;
       		HV_VL_fr=1;

		    HV_VL_rl=1;
        	HV_VL_rr=1;
    		break;
	}
	
	LA_vSetNominalWheelValvePwmDuty(HV_VL_fl,AV_VL_fl,U8_BASE_CTRLTIME,U8_BASE_CTRLTIME,&laBA_FL1HP);
    LA_vSetNominalWheelValvePwmDuty(HV_VL_fr,AV_VL_fr,U8_BASE_CTRLTIME,U8_BASE_CTRLTIME,&laBA_FR1HP);
	LA_vSetNominalWheelValvePwmDuty(HV_VL_rl,AV_VL_rl,U8_BASE_CTRLTIME,U8_BASE_CTRLTIME,&laBA_RL1HP);
    LA_vSetNominalWheelValvePwmDuty(HV_VL_rr,AV_VL_rr,U8_BASE_CTRLTIME,U8_BASE_CTRLTIME,&laBA_RR1HP);
    		        
    la_FL1HP = laBA_FL1HP;
    la_FL2HP = laBA_FL1HP;
    la_FR1HP = laBA_FR1HP;  
    la_FR2HP = laBA_FR1HP;
    la_RL1HP = laBA_RL1HP;
    la_RL2HP = laBA_RL1HP;
    la_RR1HP = laBA_RR1HP;  
    la_RR2HP = laBA_RR1HP;	
}

void LABA_vInhibitCheck(void)
{
  
  #if __GM_FailM       
    if((fu1ESCEcuHWErrDet=1)
     ||(fu1WheelFLErrDet==1)||(fu1WheelFRErrDet==1)||(fu1WheelRLErrDet==1)||(fu1WheelRRErrDet==1)
     ||(fu1MCPErrorDet==1)||(fu1MCPSusDet==1)
     ||(fu1VoltageLowErrDet==1) 	
    #if __BRAKE_FLUID_LEVEL==ENABLE
     ||(fu1DelayedBrakeFluidLevel==1)
	 ||(fu1LatchedBrakeFluidLevel==1)	
    #endif
    #if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
     ||(lsespu1BrkAppSenInvalid == 1)
    #endif
     ) /* delete fu1BLSErrDet==0 at MGH-80 */            	
  #else 
    if((vdc_error_flg==1)
    #if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
     ||(lsespu1BrkAppSenInvalid == 1)
    #endif
    )	
  #endif        
	{
	    labau1InhibitFlg = 1;
    }
    else
    {
    	labau1InhibitFlg = 0;
    }	
}

#if __ADVANCED_MSC
void LCMSC_vSetBATargetVoltage(void)
{	
	if (PBA_ON==1)
    {
        PBA_MSC_MOTOR_ON=1;

	#if __TSP
        if((ABS_fz==0) && (lctspu1TspPbaComb==0))
	#else
        if(ABS_fz==0)
    #endif
        {
            PBA_ABS_COMB_MSC_MOTOR_ON =0;
          #if __CAR == GM_TAHOE ||__CAR==GM_SILVERADO
            if (BA_hold_timer<=L_U8_TIME_10MSLOOP_200MS)       /*7Scan Full*/
            {
                pba_msc_target_vol = MSC_14_V;
            }
            else
            {
                pba_msc_target_vol = MSC_8_V;
            }
          #else
	            if (BA_hold_timer<=U16_PBA_FULL_MOTOR_ACT_TIME)        /*4Scan Full*/
	            {
	                pba_msc_target_vol = MSC_14_V;
	            }
	            else
	            {
	                pba_msc_target_vol = S16_PBA_TARGET_VOLT;
	            }
          #endif
        }
        else    /* ABS ==1*/
        {
            /*PBA_ABS_COMB_MSC_MOTOR_ON =1;     */
			pba_msc_target_vol = S16_PBA_TARGET_VOLT_ABS;
        }
    }
    else
    {
        pba_msc_target_vol = MSC_0_V;
        PBA_MSC_MOTOR_ON=0;
        PBA_ABS_COMB_MSC_MOTOR_ON =0;
    }    
 
	#if __TSP_PBA_COMBINATION==1
		if(lctspu1TspPbaComb==1)
		{
			PBA_MSC_MOTOR_ON=1;
			pba_msc_target_vol = MSC_8_V; //MSC_7_5_V;		//3400rpm
		}
	#endif					

    #if __FBC
    FBC_MSC_MOTOR_ON = FBC_ON;
    if (FBC_MSC_MOTOR_ON)
    {
        fbc_msc_target_vol = S16_FBC_TARGET_VOLT; 
    }
    else
    {
        fbc_msc_target_vol = MSC_0_V;
    }
    #endif
}
#endif

#if  ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
static void LABA_vTcTagetPress(void)
{
	if(BA_mode == BA_STANDBY)
	{
		labas16TcTargetPress = MPRESS_0BAR;
	}
	else if(BA_mode == BA_APPLY)
	{
		if(ABS_fz==1)
		{
		    labas16TcTargetPress = S16_BA_TC_TARGET_PRESS_MAX;	
		}
	  #if __FBC	
		else if(FBC_ON==1)
		{
			labas16TcTargetPress = S16_BA_TC_TARGET_PRESS_MAX;
		}	
	  #endif	
		else
		{
		    if((labas16TcTargetPress==0) && (PBA_ON==1)) /* PBA Apply 1st scan target pres. */
            {
            	labas16TcTargetPress = lsesps16EstBrkPressByBrkPedalF + MPRESS_20BAR;
            }
            else /* Apply Rate according to Target V value */
            {	
            	labas16TcTargetPress = labas16TcTargetPress + S16_PBA_TARGET_VOLT/200; /* if 4V, then 2bar/1scan apply */
            }	
		}    	
	}
	else if(PBA_EXIT_CONTROL==1) /* PBA Fade-out */
	{
		if(lsesps16EstBrkPressByBrkPedalF<(int16_t)U16_PBA_PRESS_EXIT_THR1)
		{
			labas16TcTargetPress = labas16TcTargetPress - MPRESS_10BAR;
		}
		else if(labas16TcTargetPress>MPRESS_100BAR)
		{
			labas16TcTargetPress = labas16TcTargetPress - MPRESS_10BAR;
		}
		else
		{
			labas16TcTargetPress = labas16TcTargetPress - MPRESS_4BAR;
		}	
	}
	else if(BA_mode == BA_DUMP)
	{
		labas16TcTargetPress = labas16TcTargetPress - MPRESS_10BAR;
	}
	else 
	{
		; /* Hold */
	}	

	if(labas16TcTargetPress < MPRESS_0BAR)
	{
		labas16TcTargetPress = MPRESS_0BAR;
	}
	else if(labas16TcTargetPress > S16_BA_TC_TARGET_PRESS_MAX)
	{
		labas16TcTargetPress = S16_BA_TC_TARGET_PRESS_MAX;
	}
	else
	{
		;
	}
}
#endif /* __AHB_GEN3_SYSTEM == ENABLE */


#endif /* __VDC */
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LABACallActHW
	#include "Mdyn_autosar.h"               
#endif
