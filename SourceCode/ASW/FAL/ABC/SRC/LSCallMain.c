/*******************************************************************************
* Project Name:		MGH40_ESP
* File Name:		LSCallMain.c
* Description:		Sensor Signal Processiong Main Code of Logic
* Logic version:	HV25
********************************************************************************
*  Modification Log
*  Date			Author			Description
*  -----		-----			-----
*  5C12			eslim			Initial Release
********************************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LSCallMain
	#include "Mdyn_autosar.h"
#endif
/* Includes ********************************************************************/

#include "LSCallMain.h"
#include "LSABSCallSensorSignal.h"
#include "LSENGCallSensorSignal.h"

#if ( __IVSS == ENABLE )
#include "LCESPInterfaceIVSS.h"
#endif

#if __TCS || __ETC

#include "../CAL/VariantName/LEngTunePar.h"
#include "hm_logic_var.h"
#endif

#if __TCS || __BTC
#include "LSTCSCallSensorSignal.h"
#endif
/* Logcal Definiton ************************************************************/
#if __TCS || __ETC
	#if defined __GM_CAL_SET
    	#define __SET_ENG_VAR_PER_CALSET
	#else
    	#define __ENG_VAR_SET_PER_ENG_VEH_TYPE
	#endif

	#if defined (__ENG_VAR_PER_TCS_SWITCH) || defined(__ENG_VAR_PER_COMPETIVE_SWITCH) || (defined (__ENG_VAR_PER_DRV_MODE) && ((__4WD_VARIANT_CODE==ENABLE)||(__4WD == ENABLE)))|| (__CAR == GM_XTS)
  	#define __VARIABLE_CAL_DURING_IGN_CYCLE
	#endif
#endif

/* Variables Definition ********************************************************/

#if __TCS || __ETC
LS_FLAG_t lsMainFlag1;

static int16_t lss16TMModeId;
static int16_t lss16DrvModeId;
uint8_t lsu8LoadedDrvModeOld;
uint8_t Ivss_ModeESCOld;
uint8_t lstcsu8LoadEngCalCheckCnt;
uint8_t lstcsu8LoadEngCalForChanVarCnt;
#endif

/* Local Function prototype ****************************************************/

void LS_vCallMainSensorSignal(void);

#if __TCS || __ETC
void LS_vLoadEngCalParameters(void);

  #if defined (__SET_ENG_VAR_PER_CALSET)

void LS_vDecideEngVarPerCalSet(void);

  #else /*#if defined (__SET_ENG_VAR_PER_CALSET)*/

void LS_vDecideEngVarPerEngType(void);
void LS_vDecideEngCalValue(DATA_APCALESPENG_t *pCalEngStruct,const ENG_CAL_HMC_VAR_OPTION_t *pEngVarProp);

  #endif /*#if defined (__SET_ENG_VAR_PER_CALSET)*/

void LS_vCheckLoadEngInNonDecision(void);

int16_t LS_s16CheckLoadEngCalValues(void);

#endif

/* Implementation **************************************************************/

/*******************************************************************************
* FUNCTION NAME:		LS_vCallMainSensorSignal
* CALLED BY:			L_vCallMain()
* Preconditions:		none
* PARAMETER:			none
* RETURN VALUE:			none
* Description:			Sensor Signal of each system
********************************************************************************/
void LS_vCallMainSensorSignal(void)
{
/*-------------------------------------------------------------------
called at main.c due to cycle time
should be transferred in later
---------------------------------------------------------------------*/
#if __TCS || __ETC
	LS_vLoadEngCalParameters();
#endif
	LSABS_vCallSensorSignal(); 
	
	LSCAN_vProcessCANSignal();

	LSENG_vCallSensorSignal();

#if __TCS || __BTC
#if !SIM_MATLAB
	LSTCS_vCallSensorSignal();
#endif
#endif

#if __VDC
	LSESP_vCallSensorSignal();
	LSPBA_vCallSensorSignal();
#endif

#if ( __IVSS == ENABLE )
	LSESPInterfaceIVSS();
#endif

#if defined(__CDC)
// 	LS_CalcFaultInput();
 	LSCDC_vCallSensorSignal();
 	LS_CalcSportModeInput();
#endif

#if __VSM
 	LSEPS_vCallSensorSignal();
#endif

}	


#if __TCS || __BTC
void LS_vLoadEngCalParameters(void)
{
	/*=============================================================================*/
	/* Purpose : CAN으로 송신되는 Powertrain Data를 사용하여 현재 ECU가 장착되어   */
	/*           있는 Engine 판단												   */
	/* Input Variable : None 													   */
	/* Output Variable : None 								 					   */
	/* 2006. 10. 12 By Jongtak												       */
	/*=============================================================================*/

    int16_t s16LoadEngCalValues;
    
    LS_vCheckLoadEngInNonDecision();
    
  #if defined __VARIABLE_CAL_DURING_IGN_CYCLE
    s16LoadEngCalValues=LS_s16CheckLoadEngCalValues();
  #endif
    
	if((speed_calc_timer < L_U8_TIME_10MSLOOP_1000MS) || (lstcsu8LoadEngCalCheckCnt == L_U8_TIME_10MSLOOP_2S) /* Load structure just once, except for (__DMC_MODE_CONTROL)--> Load per event */
      #if defined __VARIABLE_CAL_DURING_IGN_CYCLE
	   || (s16LoadEngCalValues==1) || (lstcsu8LoadEngCalForChanVarCnt > 0)
      #endif
	  ) /* Load structure just once or per event */
	{
      #if defined (__SET_ENG_VAR_PER_CALSET)
    
    	LS_vDecideEngVarPerCalSet();
    
      #else /*(__ENG_VAR_STRUCTURE_TYPE == ENG_VAR_PER_CALSET)*/
    
        LS_vDecideEngVarPerEngType();
    
      #endif
    }
}
#endif

  #if defined (__SET_ENG_VAR_PER_CALSET)

void LS_vDecideEngVarPerCalSet(void)
{
  #if __CAR == GM_LAMBDA
    if (SwitchModeStep==3)
    {	/* Competitive Mode */
    	ENG_VAR = 2;
		pDATA_apCalEspEng=&apCalEspEng02;    	
    }
    else
    {
    	ENG_VAR = 1;
		pDATA_apCalEspEng=&apCalEspEng01;    	   	
    }
    	
  #else
  	/* Application Team에서 요청하는 대로 Engine Variation 할당 필요 */
    ENG_VAR = 1;
    pDATA_apCalEspEng=&apCalEspEng01;
    /*pDATA_Powertrain = &apCalEscPowertrain;*/ /* 추후 BKTU 적용시 반영예정*/
  #endif    
}

  #else /* #if defined (__SET_ENG_VAR_PER_CALSET) */

#if __TCS || __BTC
void LS_vDecideEngVarPerEngType(void)
{
      #if defined (__ENG_VAR_PER_TM_TYPE)
            if(lespu1EMS_AT_TCU==1)
            {
                lss16TMModeId = S16_AUTO_TM_ID;
            }
            else if(lespu1EMS_MT_TCU==1)
            {
                lss16TMModeId = S16_MANUAL_TM_ID;
            }
            #if defined (__ENG_VAR_DCT_TM_TYPE)
            else if(lespu1EMS_DCT_TCU==1)
            {
                lss16TMModeId = S16_DCT_TM_ID;
            }
            #endif
            #if defined (__ENG_VAR_CVT_TM_TYPE)
            else if(lespu1EMS_CVT_TCU==1)
            {
                lss16TMModeId = S16_CVT_TM_ID;
            }
            #endif
            else
            {
                lss16TMModeId = 0; 
            }
      #endif /*defined (__ENG_VAR_PER_TM_TYPE)*/
        
      #if (defined __ENG_VAR_PER_DRV_MODE)
        #if (defined __SAME_ENG_VAR_FOR_DM_AUTO_N_4H)
        if((lsu8DrvMode==DM_AUTO) || (lsu8DrvMode==DM_4H))
        {
            lss16DrvModeId = DM_AUTO;
        }
        else
        #endif
        {
            lss16DrvModeId = lsu8DrvMode;
        }
      #endif        
        
        lsu1EngCalLoaded = 0;
    
    	LS_vDecideEngCalValue(&apCalEspEng01, &EngVarOption01);
    	
      #if defined (__ENG_VAR_02_DEFINED)
    	LS_vDecideEngCalValue(&apCalEspEng02, &EngVarOption02);
      #endif
    
      #if defined (__ENG_VAR_03_DEFINED)
    	LS_vDecideEngCalValue(&apCalEspEng03, &EngVarOption03);
      #endif
    
      #if defined (__ENG_VAR_04_DEFINED)
    	LS_vDecideEngCalValue(&apCalEspEng04, &EngVarOption04);
      #endif
    
      #if defined (__ENG_VAR_05_DEFINED)
    	LS_vDecideEngCalValue(&apCalEspEng05, &EngVarOption05);
      #endif
    
      #if defined (__ENG_VAR_06_DEFINED)
    	LS_vDecideEngCalValue(&apCalEspEng06, &EngVarOption06);
      #endif
    
      #if defined (__ENG_VAR_07_DEFINED)
    	LS_vDecideEngCalValue(&apCalEspEng07, &EngVarOption07);
      #endif
    
      #if defined (__ENG_VAR_08_DEFINED)
    	LS_vDecideEngCalValue(&apCalEspEng08, &EngVarOption08);
      #endif
    
      #if defined (__ENG_VAR_09_DEFINED)
    	LS_vDecideEngCalValue(&apCalEspEng09, &EngVarOption09);
      #endif
    
      #if defined (__ENG_VAR_10_DEFINED)
    	LS_vDecideEngCalValue(&apCalEspEng10, &EngVarOption10);
      #endif
    
      #if defined (__ENG_VAR_11_DEFINED)
    	LS_vDecideEngCalValue(&apCalEspEng11, &EngVarOption11);
      #endif
    
      #if defined (__ENG_VAR_12_DEFINED)
    	LS_vDecideEngCalValue(&apCalEspEng12, &EngVarOption12);
      #endif
    
      #if defined (__ENG_VAR_13_DEFINED)
    	LS_vDecideEngCalValue(&apCalEspEng13, &EngVarOption13);
      #endif
    
      #if defined (__ENG_VAR_14_DEFINED)
    	LS_vDecideEngCalValue(&apCalEspEng14, &EngVarOption14);
      #endif
    
      #if defined (__ENG_VAR_15_DEFINED)
    	LS_vDecideEngCalValue(&apCalEspEng15, &EngVarOption15);
      #endif
    
      #if defined (__ENG_VAR_16_DEFINED)
    	LS_vDecideEngCalValue(&apCalEspEng16, &EngVarOption16);
      #endif
    	
    	if(lsu1EngCalLoaded==0) /* Eng var not defined --> set default */
    	{
    	    ENG_VAR = 255;
    	    pDATA_apCalEspEng=&apCalEspEng01;
    	}
    	else
    	{
    	    ;
    	}
}
#endif

#if __TCS || __BTC
void LS_vDecideEngCalValue(DATA_APCALESPENG_t *pCalEngStruct,const ENG_CAL_HMC_VAR_OPTION_t *pEngVarProp)
{

    if(lsu1EngCalLoaded==0) /* For cycle time */
    {
        if( (1)
    
          #if defined (__ENG_VAR_PER_VOL)
            && (lespu8EMS_ENG_VOL == pEngVarProp->s16EngVolId)
          #endif
        
          #if defined (__ENG_VAR_PER_FUEL_TYPE)
            && (lespu8EMS_ENG_CHR == pEngVarProp->s16FuelTypeId)
          #endif
          
          #if defined (__ENG_VAR_PER_MAX_TQ)
            && (lesps16EMS_TQ_STND == pEngVarProp->s16MaxTqId)
          #endif
        
          #if defined (__ENG_VAR_PER_TCS_SWITCH)
            && ((int16_t)lstcsu1TCSDisabledBySW == pEngVarProp->s16TcsSwId)
          #endif
        
          #if defined (__ENG_VAR_PER_VEHICLE_CODE)
            && (lespu8MS_VEH_CODE_ENG == pEngVarProp->s16VehCodeId)
          #endif
        
          #if defined (__ENG_VAR_PER_EPS)
            && (lespu1MS_VEH_CODE_EPS == pEngVarProp->s16VehEpsId)
          #endif
        
          #if defined (__ENG_VAR_PER_DRV_MODE)
            && (lss16DrvModeId == pEngVarProp->s16DrvModeId)
          #endif
    
          #if defined (__ENG_VAR_PER_TM_TYPE)
            && (lss16TMModeId == pEngVarProp->s16TmTypeId)
          #endif
          
          #if defined (__ENG_VAR_PER_COMPETIVE_SWITCH)
            #if ( __IVSS == ENABLE )
            && (Ivss_ModeESC == pEngVarProp->s16CompetSwitchId)
	           #endif
          #endif          
    
            )
        {
            lsu1EngCalLoaded = 1;
            ENG_VAR = pEngVarProp->s16EngCalStructId;
            pDATA_apCalEspEng=pCalEngStruct;
        }
    }
    
}
#endif

#endif	/* (__ENG_VAR_STRUCTURE_TYPE == ENG_VAR_PER_VOL_FUEL_TM_DM) || (__ENG_VAR_STRUCTURE_TYPE == ENG_VAR_DEFAULT) */

#if __TCS || __BTC
void LS_vCheckLoadEngInNonDecision(void)
{
	if ((fu1MainCanLineErrDet == 0) && (fu1EMSTimeOutErrDet == 0))
	{
		if (ENG_VAR == 255)
		{
			lstcsu8LoadEngCalCheckCnt = lstcsu8LoadEngCalCheckCnt +1;
			if (lstcsu8LoadEngCalCheckCnt > L_U8_TIME_10MSLOOP_2S)
			{
				lstcsu8LoadEngCalCheckCnt = 0;
			}
			else{}
		}
		else
		{
			lstcsu8LoadEngCalCheckCnt = 0;
		}
	}
	else
	{
		lstcsu8LoadEngCalCheckCnt = 0;
	}
}
		
int16_t LS_s16CheckLoadEngCalValues(void)
{
    int16_t s16LoadEngCal = 0;
    
  #if (defined __ENG_VAR_PER_DRV_MODE) && ((__4WD_VARIANT_CODE==ENABLE)||(__4WD == ENABLE))
	if(lsu8LoadedDrvModeOld != lsu8DrvMode)
	{
	    s16LoadEngCal = 1;
	}
  #endif

  #if defined (__ENG_VAR_PER_TCS_SWITCH)
    if(lstcsu1TCSDisabledBySWOld != lstcsu1TCSDisabledBySW)
    {
        s16LoadEngCal = 1;
    }
  #endif
  
  #if defined (__ENG_VAR_PER_COMPETIVE_SWITCH)
    #if ( __IVSS == ENABLE )
    if(Ivss_ModeESCOld != Ivss_ModeESC)
    {
        s16LoadEngCal = 1;
    }
    Ivss_ModeESCOld = Ivss_ModeESC;
    #endif
  #endif  

    lsu8LoadedDrvModeOld = lsu8DrvMode;
    
    return s16LoadEngCal;
}
#endif


#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LSCallMain
	#include "Mdyn_autosar.h"
#endif
