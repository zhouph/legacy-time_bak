
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCTSPCallControl
	#include "Mdyn_autosar.h"
#endif



#include "LCTSPCallControl.h"
#include "LCDECCallControl.h"
#include "LDTSPCallDetection.h"
#include "LSESPCalSlipRatio.h"
#include "LCPBACallControl.h"

#if __TSP
void LCTSP_vCallControl(void);
void LCTSP_vCallEnter(void);
void LCTSP_vCallIntervention(void);
void LCTSP_vCallBrakeIntervention(void);
void LCTSP_vCallControlBrake(void);
void LCTSP_vCallEngineIntervention(void);
void LCTSP_vCallControlEngine(void);
void LCTSP_vCallExit(void);
void LCTSP_vCallInitialize(void);
void LCTSP_vSetDecelCtrl(uint8_t,uint8_t,uint8_t,int16_t);
#endif

#if __TSP
void LCTSP_vCallControl(void)
{
		
	LCTSP_vCallIntervention();
	
	if((TSP_ON==0)&&(U1_TSP_CONTROL_MODE_INHIBIT==0))
	{
		LCTSP_vCallEnter();
	}
	else
	{
		LCTSP_vCallExit();
	}
}

void LCTSP_vCallEnter(void)
{	
	if( (vref5>=(S16_TSP_DISABLE_SPD_DETECT_MIN))&&(vref5<=(S16_TSP_DISABLE_SPD_DETECT_MAX)) ) 
	{
		
		if(tsp_detect_oscil_count>=ldtspu8DetectOscilCycle) 
		{			
			TSP_ON = 1;
			U1_TSP_BRAKE_CTRL_ON_READY = 1;
			tsp_initial_speed = vref5;
		}
		else
		{
			;
		}
	}
	else 
	{
		;	
	}
}

void LCTSP_vCallIntervention(void)
{
	LCTSP_vCallBrakeIntervention();
	LCTSP_vCallEngineIntervention();
}

void LCTSP_vCallBrakeIntervention(void)
{

	/* DECISION BRAKE CONTROL */
	if( (TSP_ON==1)&&((TSP_BRAKE_CTRL_ON==0)||(U1_TSP_DIFF_To_DEC_ON==1))&&(U1_TSP_BRAKE_CTRL_ON_READY==1)
	&&(((TSP_PLUS_DYAW_THR==1)&&(tsp_delta_yaw_dot<U8_TSP_BRK_EN_YAW_DOT_THR))||((TSP_MINUS_DYAW_THR==1)&&(tsp_delta_yaw_dot>-U8_TSP_BRK_EN_YAW_DOT_THR))) )
	{
		if(tsp_amp_damping_ratio_1st_3rd>=U8_TSP_MAX_DAMP_RATIO_1st_3rd)
		{
			U1_TSP_1st_3rd_AMP_REDUCE = 1;
		}
		else
		{
			TSP_BRAKE_CTRL_ON = 1;
			U1_TSP_BRAKE_CTRL_ON_READY = 0;
			TSP_DIFF_ONLY_CTRL_ACT = 0;
		}
	}
	else
	{
		;
	}
	
	/* PBA CONTROL INTERVENTION */	
	#if __TSP_PBA_COMBINATION==1			
	if((TSP_BRAKE_CTRL_ON==1)&&(lctspu1TspPbaComb==1))
	{
		U1_TSP_BRAKE_CTRL_ACT = 1;
	}
	else
	{
		;
	}
	#endif
	
	/* MPRESS INTERVENTION */
#if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
	if((U1_TSP_BRAKE_CTRL_ACT==0)&&(TSP_BRAKE_CTRL_ON==1)&&(lsesps16AHBGEN3mpress<=S16_TSP_BRK_CTRL_ENTER_PRESS))
#else
	if((U1_TSP_BRAKE_CTRL_ACT==0)&&(TSP_BRAKE_CTRL_ON==1)&&(mpress<=S16_TSP_BRK_CTRL_ENTER_PRESS))
#endif
	{
		U1_TSP_BRAKE_CTRL_ACT = 1;
	}
#if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
	else if((U1_TSP_BRAKE_CTRL_ACT==1)&&(TSP_BRAKE_CTRL_ON==1)&&(lsesps16AHBGEN3mpress<=S16_TSP_BRK_CTRL_EXIT_PRESS))
#else	
	else if((U1_TSP_BRAKE_CTRL_ACT==1)&&(TSP_BRAKE_CTRL_ON==1)&&(mpress<=S16_TSP_BRK_CTRL_EXIT_PRESS))
#endif
	{
		U1_TSP_BRAKE_CTRL_ACT = 1;
	}
	else
	{
		U1_TSP_BRAKE_CTRL_ACT = 0;
	}
	
	/* BRAKE WORK COUNT */
	if(U1_TSP_BRAKE_CTRL_ACT==1)
	{
		tsp_brake_work_count++;
	}
	else
	{
		;
	}

	LCTSP_vCallControlBrake();
	
	/* DECISION FADE OUT CONTROL */
	if((TSP_ON==1)&&(TSP_DIFF_ONLY_CTRL_ACT==0))
	{
		if(U1_TSP_CONTROL_MODE_INHIBIT==0)
		{
			if((U1_TSP_FADE_CTRL_ON==0)&&(tsp_osci_mitig_count>S16_TSP_OSCI_FADE_S_TIME))
			{
				U1_TSP_FADE_CTRL_ON = 1;
				U1_TSP_FADE_CTRL_MONITOR = 1;
				tsp_fade_ctrl_cnt = tsp_fade_ctrl_cnt + 1;
				tsp_decel_ctrl_cnt = 0;
			}
		#if __TSP_PBA_COMBINATION==1			
			else if(lctspu1TspPbaComb==1) 
			{
				U1_TSP_FADE_CTRL_ON = 0;
				tsp_fade_ctrl_cnt = 0;
			}
		#endif
			else if((U1_TSP_FADE_CTRL_ON==1)&&(tsp_osci_mitig_count>S16_TSP_OSCI_FADE_S_TIME))
			{
				tsp_fade_ctrl_cnt = tsp_fade_ctrl_cnt + 1;
			}
			else if((U1_TSP_FADE_CTRL_ON==1)&&(tsp_osci_mitig_count<S16_TSP_OSCI_FADE_E_TIME))
			{
				U1_TSP_FADE_CTRL_ON = 0;
				tsp_fade_ctrl_cnt = 0; 
			}
			else
			{
				;	 
			}
		}
		else
		{
			U1_TSP_FADE_CTRL_ON = 1;
			tsp_fade_ctrl_cnt = tsp_fade_ctrl_cnt + 1;
			tsp_inhibit_work_count = tsp_inhibit_work_count + 1;
			tsp_decel_ctrl_cnt = 0;
		}
	}
	else
	{
		;
	}

	/* INDICATE DECEL DIFF BRAKE CONTROL */	
	if((U1_TSP_BRAKE_CTRL_ACT==1)&&((U1_TSP_ADD_TC_CURRENT_P==1)||(U1_TSP_ADD_TC_CURRENT_S==1))&&(TSP_DIFF_ONLY_CTRL_ACT==0))
	{
		U1_TSP_DEC_DIFF_CTRL_ACT = 1;
	}
	else if((U1_TSP_DEC_DIFF_CTRL_ACT==1)&&(U1_TSP_FADE_CTRL_ON==1))
	{
		U1_TSP_DEC_DIFF_CTRL_ACT = 0;
	}
	else
	{
		;
	}

	#if !__SPLIT_TYPE
	/* INDICATE FIRST TIME DIFF BRAKE CONTROL */	
	if((U1_TSP_ADD_TC_CURRENT_P==0)&&(U1_TSP_ADD_TC_CURRENT_S==0))
	{
		U1_TSP_FIRST_DIFF_CTRL_DETECT = 0;
	}	
	else if((U1_TSP_FIRST_DIFF_CTRL_DETECT==0)&&(U1_TSP_DIFF_To_DEC_ON==0)&&((U1_TSP_ADD_TC_CURRENT_P==1)||(U1_TSP_ADD_TC_CURRENT_S==1))&&(TSP_DIFF_ONLY_CTRL_ACT==0))
	{
		U1_TSP_FIRST_DIFF_CTRL_DETECT = 1;
	}
	else
	{
		;
	}
	#endif

	/* Frequency rate Fail */
	if((tsp_freq_rate_2nd_3rd>=S16_TSP_FREQ_MAX_RATE_2nd_3rd)||(tsp_freq_rate_3rd_4th>=S16_TSP_FREQ_MAX_RATE_3rd_4th))
	{
		U1_TSP_FRQ_RATE_FAIL = 1;
	}
	else
	{
		;
	}
	
	/* 2&4 AMPLITUDE MAX COMPARE */	
	if( (((TSP_PLUS_DYAW_THR==1)&&(tsp_delta_yaw_dot<U8_TSP_BRK_EN_YAW_DOT_THR))
		||((TSP_MINUS_DYAW_THR==1)&&(tsp_delta_yaw_dot>-U8_TSP_BRK_EN_YAW_DOT_THR)))
		&&(tsp_amp_damping_ratio_2nd_4th>=U8_TSP_MAX_DAMP_RATIO_2nd_4th) )
	{
		U1_TSP_2nd_4th_AMP_REDUCE = 1;
	}
	else
	{
		;
	}

}

void LCTSP_vCallControlBrake(void)
{	
	
	if(U1_TSP_BRAKE_CTRL_ACT==1)
	{
		/* INITIAL TARGET DECEL */	
		if(tsp_detect_oscil_count==ldtspu8DetectOscilCycle)
		{
			tsp_delta_max_amplitude_old = tsp_max_amplitude_old - tsp_yaw_threshold_max_amp_old;
		}
		else
		{
			;
		}
		
		/* DECISION DIFFERENTIAL ONLY CONTROL */
		if((U1_TSP_DIFF_To_DEC_ON==0)&&(tsp_delta_max_amplitude_old<S16_TSP_DIFF_ONLY_DYAW_P)&&(tsp_initial_speed<S16_TSP_DIFF_ONLY_SPEED))
		{
			lcs16TspTargetDecelAmp = 0;
			TSP_DIFF_ONLY_CTRL_ACT = 1;
		}
		/* DECISION DECEL CONTROL */
		else
		{
			lcs16TspTargetDecelAmp = LCESP_s16IInter2Point(tsp_delta_max_amplitude_old, S16_TSP_DIFF_ONLY_DYAW_P, S16_TSP_DYAW_AMP_MAX_TARGET_G_L, 
																																									S16_TSP_TG_DYAW_AMP_MAX_H, S16_TSP_DYAW_AMP_MAX_TARGET_G_H);
		}
		
		lcs16TspTargetDecelSpeedGain = LCESP_s16IInter3Point(tsp_initial_speed, S16_TSP_TG_GAIN_SPEED_L, S16_TSP_TARGET_G_GAIN_LS,
			 																																			S16_TSP_TG_GAIN_SPEED_M, S16_TSP_TARGET_G_GAIN_MS,
																																			 	 		S16_TSP_TG_GAIN_SPEED_H, S16_TSP_TARGET_G_GAIN_HS);
																																			 	 																																							
		lcs16TspTargetDecelInit = (int16_t)((((int32_t)(lcs16TspTargetDecelSpeedGain))*lcs16TspTargetDecelAmp)/100);
		
		
		/* DELTA MOMENT TSP */
		Delta_moment_tsp = (int16_t)(((((int32_t)(tsp_delta_yaw_first))*U8_TSP_DIFF_PGAIN)/100)+((((int32_t)(tsp_delta_yaw_dot))*U8_TSP_DIFF_DGAIN)/100));
		
		if(tsp_delta_yaw_first<0)
		{
			Delta_moment_tsp = -Delta_moment_tsp;
		}
		else
		{
			;
		}

		if(U1_TSP_FADE_CTRL_ON==0)
		{
			/* DECEL CONTROL */
			if(TSP_DIFF_ONLY_CTRL_ACT==0)
			{
				/* CONTROL */
				tsp_tempW5 = lcs16TspTargetDecelInit;
				
				lcs16TspDeltaDecelSpeed = LCESP_s16IInter2Point(tsp_initial_speed, S16_TSP_VAR_T_G_SPEED_L, S16_TSP_DELTA_SPEED_LOW,
																																					 S16_TSP_VAR_T_G_SPEED_H, S16_TSP_DELTA_SPEED_HIGH);
					
				lcs16TspDecelSpeed = tsp_initial_speed - lcs16TspDeltaDecelSpeed;
				
				if((vref5<lcs16TspDecelSpeed)&&(U1_TSP_FADE_CTRL_ON==0))
				{
					tsp_decel_ctrl_cnt = tsp_decel_ctrl_cnt + 1;
					tsp_tempW6 = tsp_tempW5 - tsp_decel_ctrl_cnt*U8_TSP_DEC_TARGET_DECEL_RATE;
						
					if(tsp_tempW6 < U8_TSP_SPEED_MIN_DECEL)   
					{
						tsp_tempW6 = U8_TSP_SPEED_MIN_DECEL;
					}
					else
					{
						;
					}
					
					lcs16tspTargetDecelMonitor = tsp_tempW6;			
				}
				else
				{
					tsp_tempW6 = tsp_tempW5;
					lcs16tspTargetDecelMonitor = tsp_tempW6;				
				}
				
				/* CURRENT MAPING*/				
//				tsp_tempW8 = LCESP_s16IInter2Point(mpress, MPRESS_5BAR, 100, MPRESS_30BAR, 0);
				tsp_tempW8 = 100;
				
				tsp_tempW9 = LCESP_s16IInter3Point(lcs16TspTargetDecelInit, S16_TSP_CURRENT_TG_DECEL_L, S16_TSP_INITIAL_CURRENT_LD,
			 																															S16_TSP_CURRENT_TG_DECEL_M, S16_TSP_INITIAL_CURRENT_MD,
																																		S16_TSP_CURRENT_TG_DECEL_H, S16_TSP_INITIAL_CURRENT_HD);				

				lcs16TspInitialDecelCurrent = (int16_t)((((int32_t)(tsp_tempW9))*tsp_tempW8)/100);
			}
			else
			{
				tsp_tempW6 = 0;
				tsp_decel_ctrl_cnt = 0;
			}
			
			/* DIFFERENTIAL CONTROL */
			if(lctspu1TspPbaComb==0)
			{
				/* CONTROL */
				if( (U1_TSP_ADD_TC_CURRENT_P==0)&&(slip_m_fr>(-1000)) 
					&&((TSP_DIFF_ONLY_CTRL_ACT==1)||(tsp_brake_work_count>U8_TSP_RISE_TIME))
					&&(SLIP_CONTROL_NEED_FL==0) 
					&&(((tsp_delta_yaw_first>=S8_TSP_DIFF_CTRL_YAW_EN_THR)&&(tsp_delta_yaw_dot>S8_TSP_DIFF_CTRL_YAW_DOT_EN_THR))||(tsp_delta_yaw_first>tsp_yaw_threshold))
					)
				{
					U1_TSP_ADD_TC_CURRENT_S = 0;
					U1_TSP_ADD_TC_CURRENT_P = 1;
				}
				else if( (U1_TSP_ADD_TC_CURRENT_S==0)&&(slip_m_fl>(-1000)) 
					     &&((TSP_DIFF_ONLY_CTRL_ACT==1)||(tsp_brake_work_count>U8_TSP_RISE_TIME))
					     &&(SLIP_CONTROL_NEED_FR==0)
					     &&(((tsp_delta_yaw_first<=-S8_TSP_DIFF_CTRL_YAW_EN_THR)&&(tsp_delta_yaw_dot<-S8_TSP_DIFF_CTRL_YAW_DOT_EN_THR))||(tsp_delta_yaw_first<-tsp_yaw_threshold))
					     )
				{
					U1_TSP_ADD_TC_CURRENT_S = 1;
					U1_TSP_ADD_TC_CURRENT_P = 0;
				}
				else
				{
					;
				}
				
				/* CURRENT */
				tsp_tempW7 = (int16_t)((((int32_t)(Delta_moment_tsp))*S16_TSP_DIFF_ADD_CURRENT_GAIN)/100);
				
				if(TSP_DIFF_ONLY_CTRL_ACT==1)
				{
					lcs16TspDiffOnlyCurrent = LCESP_s16IInter3Point(tsp_initial_speed, S16_TSP_DIFF_ONLY_CURRENT_SPEED_L, S16_TSP_DIFF_ONLY_CURRENT_LS,
			 																																			 S16_TSP_DIFF_ONLY_CURRENT_SPEED_M, S16_TSP_DIFF_ONLY_CURRENT_MS,
																																			       S16_TSP_DIFF_ONLY_CURRENT_SPEED_H, S16_TSP_DIFF_ONLY_CURRENT_HS);
					tsp_tempW7 = tsp_tempW7 + lcs16TspDiffOnlyCurrent;
				}
				else
				{ 
					;
				}
				
				if(U1_TSP_ADD_TC_CURRENT_P==1)
				{
					lcs16TspDiffCtrlAddCurP = (int16_t)((((int32_t)(tsp_tempW7))*100)/100);
					
					if(lcs16TspDiffCtrlAddCurP<0)
					{
						lcs16TspDiffCtrlAddCurP = 0;
					}
					else
					{
						;
					}
				}
				else if(U1_TSP_ADD_TC_CURRENT_S==1)
				{
					lcs16TspDiffCtrlAddCurS = (int16_t)((((int32_t)(tsp_tempW7))*100)/100);
					
					if(lcs16TspDiffCtrlAddCurS<0)
					{
						lcs16TspDiffCtrlAddCurS = 0;
					}
					else
					{
						;
					}					
				}
				else
				{
					lcs16TspDiffCtrlAddCurS = 0;
					lcs16TspDiffCtrlAddCurP = 0;
				}				
			}
			else
			{
				lcs16TspDiffCtrlAddCurS = 0;
				U1_TSP_ADD_TC_CURRENT_S = 0;
				lcs16TspDiffCtrlAddCurP = 0;
				U1_TSP_ADD_TC_CURRENT_P = 0;
			}		
		}
		/* FADEOUT CONTROL */
		else
		{
			tsp_tempW6 = lcs16tspTargetDecelMonitor - tsp_fade_ctrl_cnt*U8_TSP_FADE_DEC_DECEL_RATE;
			
			if(tsp_tempW6<U16_TSP_FADE_END_DECEL)   
			{
				tsp_tempW6 = U16_TSP_FADE_END_DECEL;
			}
			else
			{
				;
			}
			
			U1_TSP_ADD_TC_CURRENT_P = 0;
			U1_TSP_ADD_TC_CURRENT_S = 0;
			lcs16TspDiffCtrlAddCurP = 0;
			lcs16TspDiffCtrlAddCurS = 0;		
		}

		if(tsp_tempW6>0)
		{
			LCTSP_vSetDecelCtrl(1,1,0,-tsp_tempW6);		
		}
		else
		{
			LCTSP_vSetDecelCtrl(0,0,0,0);	
		}
				
		lcs16TspTargetDecelReal = -tsp_tempW6;
		lcs16TspTargetDecel = tsp_tempW6;
						
	}
	else
	{
		LCTSP_vSetDecelCtrl(0,0,0,0);
		U1_TSP_ADD_TC_CURRENT_P = 0;
		U1_TSP_ADD_TC_CURRENT_S = 0;
		lcs16TspDiffCtrlAddCurP = 0;
		lcs16TspDiffCtrlAddCurS = 0;
		U1_TSP_DEC_DIFF_CTRL_ACT = 0;
		U1_TSP_FADE_CTRL_ON = 0;
		
		tsp_fade_ctrl_cnt=0;
		tsp_decel_ctrl_cnt = 0;
		
		Delta_moment_tsp = 0;
	}
	
	/* DIFF to DEC CONTROL */
	if(TSP_DIFF_ONLY_CTRL_ACT==1)
	{
		if(tsp_max_amplitude_old>=(tsp_yaw_threshold_max_amp_old+S16_TSP_DIFF_TO_DEC_DYAW_P))
		{
			U1_TSP_BRAKE_CTRL_ON_READY = 1;
			U1_TSP_DIFF_To_DEC_ON = 1;
		}
		else
		{
			;
		}
	}
	else
	{
		;
	}	
}

void LCTSP_vCallEngineIntervention(void)
{
	if((TSP_ON==1)&&(mtp>=2)&&(TSP_BRAKE_CTRL_ON==1)&&(TSP_DIFF_ONLY_CTRL_ACT==0))
	{
		TSP_ENG_CTRL_ON = 1;
	}
	else
	{
		tsp_torq = cal_torq;
	}
	
	LCTSP_vCallControlEngine();
}

void LCTSP_vCallControlEngine(void)
{

	if(TSP_ENG_CTRL_ON==1)
	{
		lcu8TspTorqDownRate = LCESP_s16IInter2Point(tsp_initial_speed, S16_TSP_TORQ_SPEED_L, U8_TSP_TORQ_DOWN_RATE_LS, 
																															 		 S16_TSP_TORQ_SPEED_H, U8_TSP_TORQ_DOWN_RATE_HS);


		lcu8TspTorqlimit = LCESP_s16IInter2Point(lcs16TspTargetDecel, S16_TSP_TORQ_LIMIT_DECEL_L, S16_TSP_TORQ_LIMIT_LD, 
																																	S16_TSP_TORQ_LIMIT_DECEL_H, S16_TSP_TORQ_LIMIT_HD);
		
		tsp_torq = tsp_torq - lcu8TspTorqDownRate;
		
		if(tsp_torq<=lcu8TspTorqlimit)
		{
			tsp_torq = lcu8TspTorqlimit;
		}
		else
		{
			;
		}
	}
	else
	{
		;
	}

	if(tsp_torq<fric_torq)
	{
		tsp_torq = fric_torq;
	}
	else
	{
		;
	}
	
	if(tsp_torq>drive_torq)
	{
		tsp_torq = drive_torq;
	}
	else
	{
		;
	}
}

void LCTSP_vCallExit(void)
{
	if( (tsp_osci_mitig_count>S16_TSP_OSCI_MITIG_END_TIME)
	  ||(vref5<(S16_TSP_EXIT_SPD_MIN))
	  ||(tsp_inhibit_work_count>S16_TSP_INHIBIT_EXIT_TIME)
	  ||(U1_TSP_1st_3rd_AMP_REDUCE==1)
	  ||(U1_TSP_2nd_4th_AMP_REDUCE==1)
	  ||(U1_TSP_FRQ_FAIL==1)
	  ||(U1_TSP_FRQ_RATE_FAIL==1)
	  )
	{
		LCTSP_vCallInitialize();
	}
	else
	{
		;
	}
	
	if(mtp<2)
	{
		TSP_ENG_CTRL_ON=0;
	}
	else
	{
		;
	}
}

void LCTSP_vCallInitialize(void)
{
	TSP_ON = 0;		
	TSP_BRAKE_CTRL_ON = 0;
	TSP_ENG_CTRL_ON = 0;
	TSP_PLUS_DYAW_THR = 0;			
	TSP_MINUS_DYAW_THR = 0;
	U1_TSP_BRAKE_CTRL_ON_READY = 0;
	U1_TSP_BRAKE_CTRL_ACT = 0;
	TSP_DIFF_ONLY_CTRL_ACT = 0;	
	U1_TSP_ADD_TC_CURRENT_P = 0;
	U1_TSP_ADD_TC_CURRENT_S = 0;
	U1_TSP_FIRST_DIFF_CTRL_DETECT = 0;
	U1_TSP_FADE_CTRL_MONITOR = 0;
	U1_TSP_DIFF_To_DEC_ON = 0;
	U1_TSP_FRQ_FAIL = 0;
	U1_TSP_FRQ_RATE_FAIL = 0;
	TSP_PLUS_DYAW_PEAK_DETECT = 0;
	TSP_MINUS_DYAW_PEAK_DETECT = 0;
	U1_TSP_1st_3rd_AMP_REDUCE = 0;
	U1_TSP_2nd_4th_AMP_REDUCE = 0;
	U1_TSP_FADE_CTRL_ON = 0;

	tsp_freq_rate_2nd_3rd = 0;
	tsp_freq_rate_3rd_4th = 0;

	tsp_max_amplitude_1st = 0;
	tsp_max_amplitude_2nd = 0;
	tsp_max_amplitude_3rd = 0;
	tsp_max_amplitude_4th = 0;
	tsp_max_amplitude = 0;
	tsp_max_amplitude_old = 0;
	tsp_delta_max_amplitude_old = 0;
	
	tsp_amplitude_rate_1st_3rd = 0;
	tsp_amplitude_rate_2nd_4th = 0;
	
	tsp_plus_dyaw_peak_count = 0;
	tsp_minus_dyaw_peak_count = 0;
	
	tsp_amp_damping_ratio_1st_3rd = 0;
	tsp_amp_damping_ratio_2nd_4th = 0;
	
	tsp_detect_oscil_count = 0;
	tsp_osci_mitig_count = 0;	
	tsp_brake_work_count = 0;
	
	tsp_torq = cal_torq;		
	
	tsp_initial_speed=0;
	lcs16TspDecelSpeed = 0;
	lcs16TspTargetDecelAmp = 0;
	lcs16TspTargetDecelInit = 0;
	lcs16tspTargetDecelMonitor = 0;
	lcs16TspTargetDecelReal = 0;
	lcs16TspTargetDecel = 0;
	tsp_decel_ctrl_cnt = 0;
	tsp_fade_ctrl_cnt = 0;
	tsp_inhibit_work_count = 0;

	tsp_dyaw_count = 0;
	tsp_yawc_count = 0;

	lcs16TspDiffCtrlAddCurP = 0;
	lcs16TspDiffCtrlAddCurS = 0;
}

void LCTSP_vSetDecelCtrl(uint8_t lcu8TSPDecReq,uint8_t lcu8TSPDecFfReq,uint8_t lcu8TSPDecClosingReq,int16_t lcs16TSPDecTargetG)
{
    clDecTsp.lcu8DecCtrlReq=lcu8TSPDecReq;
    clDecTsp.lcu8DecFeedForwardReq=lcu8TSPDecFfReq;
    clDecTsp.lcu8DecSmoothClosingReq=lcu8TSPDecClosingReq;
    clDecTsp.lcs16DecReqTargetG=lcs16TSPDecTargetG;
}

#endif

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCTSPCallControl
	#include "Mdyn_autosar.h"
#endif    
