#ifndef __LCETSCCALLCONTROL_H__
#define __LCETSCCALLCONTROL_H__

/*includes********************************************************************/
#include "LVarHead.h"
/*Global MACRO CONSTANT Definition********************************************/
#define	ETSC_FEATURE_DISABLED		0
#define	ETSC_FEATURE_ENABLED		1
#define	ETSC_INHIBITED				1
#define	ETSC_ALLOWED				0
#define	ETSC_ACTIVE					1
#define	ETSC_NOT_ACTIVE				0

/*Global MACRO Function Definition********************************************/
#define MacroFunctionAbs(x1) (((INT)(x1) >= 0) ? (INT) (x1) : (INT) (-(x1)))
	
/*Global Type Declaration ****************************************************/



/*Global Extern Variable  Declaration*****************************************/
extern U8_BIT_STRUCT_t ETSC_FLAG01, ETSC_FLAG02;
extern int16_t letscs16_AvailableEngTorq;
extern int16_t letscs16_PcntOfAvailTorq, letscs16_PcntOfAvailTorqAtSpdP1, letscs16_PcntOfAvailTorqAtSpdP2, letscs16_PcntOfAvailTorqAtSpdP3;
extern int16_t letscs16_MaxAllowEngTorq;
extern int16_t letscs16_EngTorqStrCtlEndTimer;
extern int16_t letscs16_EngTorqStrCtlCmd;


/*Flags Definition ***********************************************************/
#define letscu1_EngTorqStrCtlInhibit		ETSC_FLAG01.bit0
#define letscu1_EngTorqStrCtlInhibitOld		ETSC_FLAG01.bit1
#define letscu1_EngTorqStrCtlStatus			ETSC_FLAG01.bit2
#define letscu1_EngTorqStrCtlStatusOld		ETSC_FLAG01.bit3
#define letscu1_EngTorqStrCtlFadeIn			ETSC_FLAG01.bit4
#define letscu1_EngTorqStrCtlFadeOut		ETSC_FLAG01.bit5
#define ETSC_RESERVED_F01_6					ETSC_FLAG01.bit6
#define ETSC_RESERVED_F01_7					ETSC_FLAG01.bit7

#define ETSC_RESERVED_F02_1					ETSC_FLAG02.bit0
#define ETSC_RESERVED_F02_2             	ETSC_FLAG02.bit1
#define ETSC_RESERVED_F02_3             	ETSC_FLAG02.bit2
#define ETSC_RESERVED_F02_4             	ETSC_FLAG02.bit3
#define ETSC_RESERVED_F02_5             	ETSC_FLAG02.bit4
#define ETSC_RESERVED_F02_6             	ETSC_FLAG02.bit5
#define ETSC_RESERVED_F02_7             	ETSC_FLAG02.bit6
#define ETSC_RESERVED_F02_8             	ETSC_FLAG02.bit7

/*Global Extern Functions  Declaration****************************************/
extern void LDETSC_vCallDetection(void);
extern void LCETSC_vCallControl(void);
extern void LCETSC_vCordEngTorqCmd(void);

/*Temp Calibration Parameters*************************************************/
#define S16_ETSC_FEATURE_ENABLE				0//S16_TCS_RESERVED_01
#define S16_ETSC_ALLOWABLE_ENG_TRQ_OFFSET	200
#define S16_ETSC_END_TIMER					L_U8_TIME_10MSLOOP_500MS
#define	S16_ETSC_MAX_TORQ_IN_SPEC			8480
#define	S16_ETSC_DISABLE_ENG_RPM			1200
#define S16_ETSC_TORQ_INC_RATE_FADE_OUT		100
#define S16_ETSC_TORQ_DEC_RATE_FADE_IN		100


	/* Data point of engine RPM */
	#define S16_ETSC_ENG_RPM_P1	1000
	#define S16_ETSC_ENG_RPM_P2	2000
	#define S16_ETSC_ENG_RPM_P3	3000
	#define S16_ETSC_ENG_RPM_P4	4000
	#define S16_ETSC_ENG_RPM_P5	5000
	#define S16_ETSC_ENG_RPM_P6	6000
	#define S16_ETSC_ENG_RPM_P7	7000
	
	/* Available engine torque */
	#define S16_ETSC_AVAIL_ENG_TORQ_P1	2000
	#define S16_ETSC_AVAIL_ENG_TORQ_P2	2500
	#define S16_ETSC_AVAIL_ENG_TORQ_P3	2700
	#define S16_ETSC_AVAIL_ENG_TORQ_P4	3000
	#define S16_ETSC_AVAIL_ENG_TORQ_P5	3200
	#define S16_ETSC_AVAIL_ENG_TORQ_P6	2800
	#define S16_ETSC_AVAIL_ENG_TORQ_P7  2500

	/* Data point of steering angle */
	#define	S16_ETSC_STR_ANG_P1	1000
	#define	S16_ETSC_STR_ANG_P2	2000
	#define	S16_ETSC_STR_ANG_P3	3000
	#define	S16_ETSC_STR_ANG_P4	4000
	#define	S16_ETSC_STR_ANG_P5	5000
	#define	S16_ETSC_STR_ANG_P6	6000
	#define	S16_ETSC_STR_ANG_P7	7000
	
	/* Data point of vehicle speed */
	#define	S16_ETSC_SPEED_P1	80
	#define	S16_ETSC_SPEED_P2	160
	#define	S16_ETSC_SPEED_P3	320
	
	/* Percentage of available torque */
	#define S16_ETSC_PCNT_AVAIL_TORQ_V1_SA1	100//  U8_TCS_TORQ_RECOVERY_F_0_0
	#define S16_ETSC_PCNT_AVAIL_TORQ_V1_SA2	80 //  U8_TCS_TORQ_RECOVERY_F_0_1
	#define S16_ETSC_PCNT_AVAIL_TORQ_V1_SA3	70 //  U8_TCS_TORQ_RECOVERY_F_0_2
	#define S16_ETSC_PCNT_AVAIL_TORQ_V1_SA4	60 //  U8_TCS_TORQ_RECOVERY_F_0_3
	#define S16_ETSC_PCNT_AVAIL_TORQ_V1_SA5	50 //  U8_TCS_TORQ_RECOVERY_F_1_0
	#define S16_ETSC_PCNT_AVAIL_TORQ_V1_SA6	40 //  U8_TCS_TORQ_RECOVERY_F_1_1
	#define S16_ETSC_PCNT_AVAIL_TORQ_V1_SA7	30 //  U8_TCS_TORQ_RECOVERY_F_1_2
	
	#define S16_ETSC_PCNT_AVAIL_TORQ_V2_SA1	100 //U8_TCS_TORQ_REDUCTION_F_0_0
	#define S16_ETSC_PCNT_AVAIL_TORQ_V2_SA2	90  //U8_TCS_TORQ_REDUCTION_F_0_1
	#define S16_ETSC_PCNT_AVAIL_TORQ_V2_SA3	80  //U8_TCS_TORQ_REDUCTION_F_0_2
	#define S16_ETSC_PCNT_AVAIL_TORQ_V2_SA4	70  //U8_TCS_TORQ_REDUCTION_F_0_3
	#define S16_ETSC_PCNT_AVAIL_TORQ_V2_SA5	60  //U8_TCS_TORQ_REDUCTION_F_1_0
	#define S16_ETSC_PCNT_AVAIL_TORQ_V2_SA6	50  //U8_TCS_TORQ_REDUCTION_F_1_1
	#define S16_ETSC_PCNT_AVAIL_TORQ_V2_SA7	40  //U8_TCS_TORQ_REDUCTION_F_1_2
	
	#define S16_ETSC_PCNT_AVAIL_TORQ_V3_SA1	100
	#define S16_ETSC_PCNT_AVAIL_TORQ_V3_SA2	100
	#define S16_ETSC_PCNT_AVAIL_TORQ_V3_SA3	90
	#define S16_ETSC_PCNT_AVAIL_TORQ_V3_SA4	80
	#define S16_ETSC_PCNT_AVAIL_TORQ_V3_SA5	70
	#define S16_ETSC_PCNT_AVAIL_TORQ_V3_SA6	60
	#define S16_ETSC_PCNT_AVAIL_TORQ_V3_SA7	50	
	
		

/*Global Extern Functions  Declaration****************************************/
extern void LCABS_vCallControl(void);


#endif
