#ifndef __LDABSVREFEST_H__
#define __LDABSVREFEST_H__
/*includes********************************************************************/
#include 	"LVarHead.h"

#define     __4WD_BLS_G_SENSOR_lIMIT   		0 
#define     __MY_09_VREF    				1 
#define		__VRSECELT_PRE_YAW_CORRECTION 	0
#if (__SPIN_DET_IMPROVE == 1)
#define     __VREF_LOGDATA_SELECTION        1
#else
#define     __VREF_LOGDATA_SELECTION        2
#endif


#if  (__FL_PRESS==ENABLE)&&(__FR_PRESS==ENABLE)
#define		__PREMIUM_VREF		1
#else
#define		__PREMIUM_VREF		0
#endif
/*Global Type Declaration ****************************************************/
extern U8_BIT_STRUCT_t VREFMF0;
	
#define	ACC_CHANGE_UP               VREFMF0.bit7
#define	vref_limit_dec_flag         VREFMF0.bit6
#define	vref_flag_unused1			VREFMF0.bit5
#define	vref_flag_unused2			VREFMF0.bit4
#define	vref_flag_unused3			VREFMF0.bit3
#define	vref_flag_unused4			VREFMF0.bit2
#define	vref_flag_unused5			VREFMF0.bit1
#define	vref_flag_unused6			VREFMF0.bit0

/*Global Extern Variable  Declaration*****************************************/


	

  	extern int16_t 	vref_resol_change; 	
 	extern int16_t 	vref_alt_resol_change;


  	extern int16_t 	vref4;
/*  extern int16_t	vref;*/
/*  extern int16_t	vref2;*/
/*Global Extern Functions  Declaration****************************************/
extern 	void	LDABS_vCallVrefEst(void);
extern 	void	LDABS_vEstWheelSlip(void);
#endif
