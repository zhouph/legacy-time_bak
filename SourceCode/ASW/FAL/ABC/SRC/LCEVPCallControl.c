/******************************************************************************
*  Project Name: ELECTRIC VACUUM PUMP
*  File: LCEVPCallControl.C
*  Description:
*  Date: Oct. 6. 2010      
*******************************************************************************
*  Modification   Log                                             
*  Date           Author           Description                    
*  -------       ----------    --------------------------------------------      
*  10.10.06      Jinhyuk Huh   Initial Release
*******************************************************************************/

/* Includes ******************************************************************/


#include "LCESPCalInterpolation.h"
#include "LCEVPCallControl.h"
#include "hm_logic_var.h"


/* Variables Definition*******************************************************/

/*struct 	bit_position EVP00;*/
	#if __EVP
int16_t		levps16VacuumSignal;
uint16_t	levpu16EVPPumpOffTimer;
uint16_t	levpu16EVPPumpOnRunTimer;
uint8_t	levpu8EVPPumpTurnOnCnt;
uint8_t	levpu8EVPAccumPumpOnSubTimer1;
uint16_t	levpu16EVPAccumPumpOnSubTimer2;
uint16_t	levpu16EVPAccumPumpOnTimer;
uint16_t	levpu16ESPEngStartTimer;
int16_t 	levps16BaroMAPPrsDiff;
uint16_t	levpu16EVPDebounceTimer;
int16_t		levps16MaxPumpOnRunTime;
int16_t		levps16K_Turn_EVP_On_Threshold;
int16_t		levps16K_Turn_EVP_Off_Threshold;
uint8_t levpu1EVPPumpOn;
uint8_t levpu1BLSActPrev;
uint8_t levpu1BLSAct;
uint8_t levpu1EVPInitPumpOn;
uint8_t levpu8EVPflags;
	#endif /* #if __EVP */
  	#if __VACUUM_PUMP_RELAY
uint8_t  	lhbbu1VacuumPumpOn;
  	#endif /* __VACUUM_PUMP_RELAY */	

/* LocalFunction prototype ***************************************************/

	#if __EVP
static void	LCEVP_vCalEVPTimer(void);
static void	LCEVP_vCallClosedloopOp(void);
static void	LCEVP_vCallOpenloopOp(void);
static void	LCEVP_vDctBrakePedalDepressed(void);
static void LCEVP_vInitializeVariables(void);
static uint16_t  LCEVP_s16ILimitRange( uint16_t InputValue, uint16_t MinValue, uint16_t MaxValue );

/* Implementation*************************************************************/

static void	LCEVP_vCalEVPTimer(void)
{
	/* Vacuum Signal 						           ( UNIT : hPa )	*/
	/*     fs16CalcVacuum     : -1000 ~ 0 hPa (resolution :   1 hPa )   */
	/* 	   levps16VacuumSignal : -100 ~ 0 kPa (resolution : 0.1 kPa ) 	*/
	levps16VacuumSignal = fs16CalcVacuum;
	/* Input Signal : System Power Mode , Engine Run Flag , CAT Lightoff , 	*/	   
	/* 				  Manifold Air Pressure , Barometric Pressure		    */
	/* Max Pump On Run Time according to altitude */
	if(lespu1BaroPressAbsV==VALID)
	{	
		levps16MaxPumpOnRunTime = LCESP_s16IInter4Point((int16_t)lespu16BaroPressAbs, (int16_t)U16_BARO_PRESS_4, (int16_t)K_Max_Pump_On_Run_Time_4,
								 	 				  							  (int16_t)U16_BARO_PRESS_3, (int16_t)K_Max_Pump_On_Run_Time_3,
								 	 				  							  (int16_t)U16_BARO_PRESS_2, (int16_t)K_Max_Pump_On_Run_Time_2,
														  						  (int16_t)U16_BARO_PRESS_1, (int16_t)K_Max_Pump_On_Run_Time_1);
		levps16K_Turn_EVP_On_Threshold = LCESP_s16IInter2Point((int16_t)lespu16BaroPressAbs, (int16_t)U16_BARO_PRESS_4, (int16_t)K_Turn_EVP_On_Threshold_High_A,
								 	 				  							         (int16_t)U16_BARO_PRESS_3, (int16_t)K_Turn_EVP_On_Threshold);
		levps16K_Turn_EVP_Off_Threshold = LCESP_s16IInter2Point((int16_t)lespu16BaroPressAbs, (int16_t)U16_BARO_PRESS_4, (int16_t)K_Turn_EVP_Off_Threshold_High_A,
								 	 				  							          (int16_t)U16_BARO_PRESS_3, (int16_t)K_Turn_EVP_Off_Threshold);								 	 				  							         
	}
	else
	{
		levps16MaxPumpOnRunTime = (int16_t)K_Max_Pump_On_Run_Time_4;
		levps16K_Turn_EVP_On_Threshold = (int16_t)K_Turn_EVP_On_Threshold;
		levps16K_Turn_EVP_Off_Threshold = (int16_t)K_Turn_EVP_Off_Threshold;
	}															  						  
	
	/* Calculus of Timer */
	if(levpu1EVPPumpOn==OFF)
	{
		levpu16EVPPumpOffTimer=levpu16EVPPumpOffTimer+1;
		levpu16EVPPumpOffTimer=LCEVP_s16ILimitRange(levpu16EVPPumpOffTimer,0,L_U16_TIME_10MSLOOP_1MIN);
		levpu16EVPPumpOnRunTimer=0;
	}
	else	/* levpu1EVPPumpOn==ON */
	{
		levpu16EVPPumpOffTimer=0;
		levpu16EVPPumpOnRunTimer=levpu16EVPPumpOnRunTimer+1;
		levpu16EVPPumpOnRunTimer=LCEVP_s16ILimitRange(levpu16EVPPumpOnRunTimer,0,L_U16_TIME_10MSLOOP_3MIN);			
		levpu8EVPAccumPumpOnSubTimer1=levpu8EVPAccumPumpOnSubTimer1+1; 
		if(levpu8EVPAccumPumpOnSubTimer1==L_U8_TIME_10MSLOOP_700MS) /* 700ms */
		{
			levpu16EVPAccumPumpOnSubTimer2=levpu16EVPAccumPumpOnSubTimer2+1;
			levpu16EVPAccumPumpOnSubTimer2=LCEVP_s16ILimitRange(levpu16EVPAccumPumpOnSubTimer2,0,L_U16_TIME_10MSLOOP_50S); /* about 83min */			
			levpu8EVPAccumPumpOnSubTimer1 = 0;
		}
		else{}		
		levpu16EVPAccumPumpOnTimer = (levpu16EVPAccumPumpOnSubTimer2*LOOP_10MS)+(levpu8EVPAccumPumpOnSubTimer1*LOOP_10MS)/100;
		/* Range : 0~6000(Resol. 0.1s) , Phy. Range : 0~600s */
		/* levpu16EVPAccumPumpOnTimer -> K_MAX_Pump_Turn_On_Time : changed to 2 timers to satisfy 0~600s range*/	 		
		levpu16EVPAccumPumpOnTimer=LCEVP_s16ILimitRange(levpu16EVPAccumPumpOnTimer,0,L_U16_TIME_10MSLOOP_5MIN); /* about 83min */		
		if(levpu16EVPPumpOnRunTimer==1)
		{
			levpu8EVPPumpTurnOnCnt=levpu8EVPPumpTurnOnCnt+1;
			levpu8EVPPumpTurnOnCnt=(uint8_t)LCEVP_s16ILimitRange(levpu8EVPPumpTurnOnCnt,0,10);
		}
		else{}
	}
	
	if(lespu1EngRunAtv==TRUE)
	{
		levpu16ESPEngStartTimer = levpu16ESPEngStartTimer+1;
		levpu16ESPEngStartTimer=LCEVP_s16ILimitRange(levpu16ESPEngStartTimer,0,L_U16_TIME_10MSLOOP_5MIN);		
	}
	else
	{
		levpu16ESPEngStartTimer = 0;
	}			
	
	if(levps16VacuumSignal>(int16_t)levps16K_Turn_EVP_On_Threshold)
	{
		levpu16EVPDebounceTimer++;
	}
	else
	{
		levpu16EVPDebounceTimer = 0;
	}		
}

static void LCEVP_vInitializeVariables(void)
{
	/* IGN off , Failsafe mode , EVP fail -> EVP variables Reset */
    levpu16EVPPumpOffTimer=0;
    levpu16EVPPumpOnRunTimer=0;
    levpu8EVPPumpTurnOnCnt=0;
    levpu8EVPAccumPumpOnSubTimer1=0;
    levpu16EVPAccumPumpOnSubTimer2=0;
    levpu16EVPAccumPumpOnTimer=0;
    levpu16ESPEngStartTimer=0;
    levps16BaroMAPPrsDiff=0;
    levpu1BLSActPrev=0;
    levpu1BLSAct=0;
    levpu1EVPInitPumpOn=0;
}	

void	LCEVP_vCallControl(void)
{
    /* Change sequence of FM & Normal Control */	
	/* EVP Fail Management */
	if( (fu1OnDiag==1)||(init_end_flg==0)||(fu1ECUHwErrDet==1)
		||(fu1VoltageUnderErrDet==1)||(fu1VoltageOverErrDet==1)||(fu1VoltageLowErrDet==1)
		||(vacuum_leak_err_flg==0)||(feu1VacuumPumpRelayShortErrFlg==0)||(feu1VacuumPumpRelayOpenErrFlg==0)
		||(fu1MainCanLineErrDet==1)||(fu1EMSTimeOutErrDet==1) || (fu8VacuumSenSusflg==1)
	  )
	{
		levpu1EVPPumpOn=0;
		LCEVP_vInitializeVariables();
		levpu8EVPflags = 5;
	}
	else
	{								
		LCEVP_vCalEVPTimer();
		LCEVP_vDctBrakePedalDepressed();
		
		/* It must run EVP only in case of System Power Mode = RUN and Engine Run Active = RUN */
		if((K_EVP_FUNCTION==ENABLE)&&(lespu8SysPwrMd==RUN)&&(lespu1EngRunAtv==TRUE))
		{	
			if(fu8VacuumModeTransition == ClosedLoopMode)		
			{
				levpu8EVPflags = 1;
				LCEVP_vCallClosedloopOp();		
			}	
			else if(fu8VacuumModeTransition == OpenLoopMode)	
			{
				levpu8EVPflags = 2;
				LCEVP_vCallOpenloopOp();
			}
			else
			{
				levpu1EVPPumpOn=0;
				LCEVP_vInitializeVariables();
				levpu8EVPflags = 3;
			}	
		}
		else
		{
			levpu1EVPPumpOn=0;
			LCEVP_vInitializeVariables();
			levpu8EVPflags = 4;
		}	
	}
}	

static void LCEVP_vCallClosedloopOp(void)
{
	if( (levpu1EVPPumpOn == OFF)&&(levpu16EVPPumpOffTimer>K_Min_Pump_Off_Time)
		&&(levps16VacuumSignal>(int16_t)levps16K_Turn_EVP_On_Threshold) 
		&&(levpu16EVPDebounceTimer > K_Debounce_Time) /* debounce time is considered */
		/* &&(levpu8EVPPumpTurnOnCnt<K_Max_Pump_Turn_On) --> deleted; EVP total active times are no limit*/
		/* &&(levpu16EVPAccumPumpOnTimer<K_MAX_Pump_Turn_On_Time) --> deleted; EVP total active time is no limit */
	  )	
		/* levps16VacuumSignal>K_Turn_EVP_On_Threshold : Changed Sign by Relative Pressure(negative value) */
	{
		levpu1EVPPumpOn = ON;	
		levpu8EVPflags = 11;
	}
	else{}

	if( (levpu1EVPPumpOn == ON)
		&&( (levps16VacuumSignal<=(int16_t)levps16K_Turn_EVP_Off_Threshold)||(levpu16EVPPumpOnRunTimer>(uint16_t)levps16MaxPumpOnRunTime) ) )
		/* levps16VacuumSignal<=K_Turn_EVP_Off_Threshold : Changed Sign by Relative Pressure(negative value) */		
		/* Inside EVP, EVP is Off when levpu16EVPAccumPumpOnTimer more than levps16MaxPumpOnRunTime */
	{
		levpu1EVPPumpOn = OFF;
		levpu8EVPflags = 12;
	}
	else{}			
}

static void	LCEVP_vCallOpenloopOp(void)
{
	if(K_CAT_LightOff_Present==TRUE)
	{
		if(levpu1EVPInitPumpOn == 0) /* Initialization Pump Turn ON */
		{	
    		if( (levpu1EVPPumpOn == OFF)
    			/* &&(levpu8EVPPumpTurnOnCnt<K_Max_Pump_Turn_On) --> deleted; EVP total active times are no limit*/
				/* &&(levpu16EVPAccumPumpOnTimer<K_MAX_Pump_Turn_On_Time) --> deleted; EVP total active time is no limit */ 
    			&&(lespu1CATLightOff==ON) )
    			/* CAT_LightOff signal is not exist DB5.1.1 (GSUV initial DB) */	
    		{
    			levpu1EVPPumpOn = ON;
    			levpu1EVPInitPumpOn = 1;	
    			levpu8EVPflags = 21;
    		}				
    		else{}
    	}
    	else						 /* Subsequent Pump Turn ON */
    	{		
    		if( (levpu1EVPPumpOn == OFF)&&(levpu16EVPPumpOffTimer>K_Min_Pump_Off_Time)
    			&&( (levpu1BLSActPrev==1)&&(levpu1BLSAct==0) ) 
    			/* &&(levpu8EVPPumpTurnOnCnt<K_Max_Pump_Turn_On) --> deleted; EVP total active times are no limit*/
				/* &&(levpu16EVPAccumPumpOnTimer<K_MAX_Pump_Turn_On_Time) --> deleted; EVP total active time is no limit */
    			&&(lespu1CATLightOff==ON) )
    			/* CAT_LightOff signal is not exist DB5.1.1 (GSUV initial DB) */    			
    		{
    			levpu1EVPPumpOn = ON;	
    			levpu8EVPflags = 22;
    		}				
    		else{}
		}
		if( (levpu1EVPPumpOn == ON)
			&&(levpu16EVPPumpOnRunTimer>K_Min_Pump_On_Run_Time)
			&&( (lespu1CATLightOff==OFF)||(levpu16EVPPumpOnRunTimer>(uint16_t)levps16MaxPumpOnRunTime) ) )
			/* CAT_LightOff signal is not exist DB5.1.1 (GSUV initial DB) */ 
		{
			levpu1EVPPumpOn = OFF;
			levpu8EVPflags = 23;
		}
		else{}										
	}
	else if (K_MAP_Present==TRUE) /* (K_CAT_LightOff_Present==FALSE) && (K_MAP_Present==TRUE)*/
	{
		levps16BaroMAPPrsDiff = (int16_t)(lespu16BaroPressAbs - lespu16EngManfldAbsPrs); /* resol : 0.1kPa */
		if(levpu1EVPInitPumpOn == 0) /* Initialization Pump Turn ON */
		{		
    		if( (lespu1EngRunAtv==TRUE)
    			/* &&(levpu16ESPEngStartTimer<=K_MAX_Start_Time) )*/
    			&&(levpu1EVPPumpOn == OFF) 
        		/* &&(levpu8EVPPumpTurnOnCnt<K_Max_Pump_Turn_On) --> deleted; EVP total active times are no limit*/
				/* &&(levpu16EVPAccumPumpOnTimer<K_MAX_Pump_Turn_On_Time) --> deleted; EVP total active time is no limit */
    			&&(   ((levps16BaroMAPPrsDiff < K_MAP_Threshold)&&((lespu1BaroPressAbsV==VALID) && (lespu1EngManfldAbsPrsV==VALID)))
    				||((lespu1BaroPressAbsV==INVALID) || (lespu1EngManfldAbsPrsV==INVALID)) )	)
    			/* SSTS : levps16BaroMAPPrsDiff > K_MAP_Threshold ; but I think that it is right 'levps16BaroMAPPrsDiff < K_MAP_Threshold' */		
    		{
    			levpu1EVPPumpOn = ON;
    			levpu1EVPInitPumpOn = 1;	
    			levpu8EVPflags = 24;
    		}
    		else{}
    	}
    	else						 /* Subsequent Pump Turn ON */
    	{    		
    		if( (lespu1EngRunAtv==TRUE)
    			/* &&(levpu16ESPEngStartTimer<=K_MAX_Start_Time) )*/
    			&&((levpu1EVPPumpOn == OFF)&&(levpu16EVPPumpOffTimer>K_Min_Pump_Off_Time))
    			&&( (levpu1BLSActPrev==1)&&(levpu1BLSAct==0) )
        		/* &&(levpu8EVPPumpTurnOnCnt<K_Max_Pump_Turn_On) --> deleted; EVP total active times are no limit*/
				/* &&(levpu16EVPAccumPumpOnTimer<K_MAX_Pump_Turn_On_Time) --> deleted; EVP total active time is no limit */
    			&&(   ((levps16BaroMAPPrsDiff < K_MAP_Threshold)&&((lespu1BaroPressAbsV==VALID) && (lespu1EngManfldAbsPrsV==VALID)))
    				||((lespu1BaroPressAbsV==INVALID) || (lespu1EngManfldAbsPrsV==INVALID)) )	)     
    			/* SSTS : levps16BaroMAPPrsDiff > K_MAP_Threshold ; but I think that it is right 'levps16BaroMAPPrsDiff < K_MAP_Threshold' */    				   				
    		{
    			levpu1EVPPumpOn = ON;
    			levpu8EVPflags = 25;
    		}
    		else{}	
		}
		if( (levpu1EVPPumpOn == ON)&&(levpu16EVPPumpOnRunTimer>K_Min_Pump_On_Run_Time) )
		{
			levpu1EVPPumpOn = OFF;
			levpu8EVPflags = 26;
		}
		else{}					
	}
	else	/* (K_CAT_LightOff_Present==FALSE) && (K_MAP_Present==FALSE)*/
	{
		if(levpu1EVPInitPumpOn == 0) /* Initialization Pump Turn ON */
		{			
			if( (lespu1EngRunAtv==TRUE)
				&&(levpu16ESPEngStartTimer>=K_MIN_Start_Time)
				/* &&(levpu16ESPEngStartTimer<=K_MAX_Start_Time)*/
				&&(levpu1EVPPumpOn == OFF)
				/*&&(levpu8EVPPumpTurnOnCnt<K_Max_Pump_Turn_On) --> deleted; EVP total active times are no limit*/
			    /*&&(levpu16EVPAccumPumpOnTimer<K_MAX_Pump_Turn_On_Time) --> deleted; EVP total active time is no limit*/
			  )		
			{
				levpu1EVPPumpOn = ON;
    			levpu1EVPInitPumpOn = 1;					
				levpu8EVPflags = 27;
			}
			else{}
    	}
    	else						 /* Subsequent Pump Turn ON */
    	{ 	
			if( (lespu1EngRunAtv==TRUE)
				&&(levpu16ESPEngStartTimer>=K_MIN_Start_Time)
				/* &&(levpu16ESPEngStartTimer<=K_MAX_Start_Time)*/
				&&(levpu1EVPPumpOn == OFF)&&(levpu16EVPPumpOffTimer>K_Min_Pump_Off_Time)
				&&( (levpu1BLSActPrev==1)&&(levpu1BLSAct==0) )
				/* &&(levpu8EVPPumpTurnOnCnt<K_Max_Pump_Turn_On) --> deleted; EVP total active times are no limit*/
				/* &&(levpu16EVPAccumPumpOnTimer<K_MAX_Pump_Turn_On_Time) --> deleted; EVP total active time is no limit */
			  )						
			{
				levpu1EVPPumpOn = ON;
				levpu8EVPflags = 28;
			}
			else{}
		}
		if( (levpu1EVPPumpOn == ON)&&(levpu16EVPPumpOnRunTimer>K_Min_Pump_On_Run_Time) )
		{
			levpu1EVPPumpOn = OFF;
			levpu8EVPflags = 29;
		}
		else{}				
	}
}
						
static void LCEVP_vDctBrakePedalDepressed(void)
{
	levpu1BLSActPrev = levpu1BLSAct;

#if !__VDC		    				
	if(fu1BLSErrDet==0) 
	{	/* BLS : Normal */
    	if(BLS==1) 
        {
        	levpu1BLSAct = 1;
        }
        else
        {
        	levpu1BLSAct = 0;
        }
    }
    else
    {	/* BLS : Fail */
    	if (ebd_filt_grv < -AFZ_0G15) /* decel rate calcurated from Ax sensor or wheel speed sensor */
    	{
    		levpu1BLSAct = 1;	
    	}
    	else if (ebd_filt_grv >= 0)
    	{
    		levpu1BLSAct = 0;
    	}
    	else{}
    }		
#else /* __VDC */
  #if __BRK_SIG_MPS_TO_PEDAL_SEN
	if( (fu1BLSErrDet==0) && (lsespu1BrkAppSenInvalid==0) )
	{	/* BLS:Normal, Master Cylinder Pressure Sensor:Normal */
    	if(   ((BLS==1) && (lsespu1PedalBrakeOn==1)) 
    	    ||((BLS==0) && (lsespu1PedalBrakeOn==1) && (lsesps16EstBrkPressByBrkPedal>=MPRESS_14BAR)) )
        {
        	levpu1BLSAct = 1;
        }
        else
        {
        	levpu1BLSAct = 0;
        }
    }
    else if ( (fu1BLSErrDet==0) && (lsespu1BrkAppSenInvalid==1) )
   	{	/* BLS:Normal, Master Cylinder Pressure Sensor:Fail */
		if (BLS==1)
	    {
        	levpu1BLSAct = 1;
    	}
    	else
    	{
        	levpu1BLSAct = 0;
    	}
    }    
    else if ( (fu1BLSErrDet==1) && (lsespu1BrkAppSenInvalid==0) )
   	{	/* BLS:Fail, Master Cylinder Pressure Sensor:Normal */
        if ( (lsespu1PedalBrakeOn==1) && (lsesps16EstBrkPressByBrkPedal>=MPRESS_14BAR) )
	    {
        	levpu1BLSAct = 1;
    	}
    	else
    	{
        	levpu1BLSAct = 0;
    	}
    }
  #else   /* #if __BRK_SIG_MPS_TO_PEDAL_SEN */
	if( (fu1BLSErrDet==0) && (fu1MCPErrorDet==0) )
	{	/* BLS:Normal, Master Cylinder Pressure Sensor:Normal */
    	if(   ((BLS==1) && (MPRESS_BRAKE_ON==1)) 
    	    ||((BLS==0) && (MPRESS_BRAKE_ON==1) && (mpress>=MPRESS_14BAR)) )
        {
        	levpu1BLSAct = 1;
        }
        else
        {
        	levpu1BLSAct = 0;
        }
    }
    else if ( (fu1BLSErrDet==0) && (fu1MCPErrorDet==1) )
   	{	/* BLS:Normal, Master Cylinder Pressure Sensor:Fail */
		if (BLS==1)
	    {
        	levpu1BLSAct = 1;
    	}
    	else
    	{
        	levpu1BLSAct = 0;
    	}
    }    
    else if ( (fu1BLSErrDet==1) && (fu1MCPErrorDet==0) )
   	{	/* BLS:Fail, Master Cylinder Pressure Sensor:Normal */
        if ( (MPRESS_BRAKE_ON==1) && (mpress>=MPRESS_14BAR) )
	    {
        	levpu1BLSAct = 1;
    	}
    	else
    	{
        	levpu1BLSAct = 0;
    	}
    }
  #endif /* #if __BRK_SIG_MPS_TO_PEDAL_SEN */    
    else
    {	/* BLS:Fail, Master Cylinder Pressure Sensor:Fail */
    	if (ebd_filt_grv < -AFZ_0G15) /* decel rate calcurated from Ax sensor or wheel speed sensor */
    	{
    		levpu1BLSAct = 1;	
    	}
    	else if (ebd_filt_grv >= 0)
    	{
    		levpu1BLSAct = 0;
    	}
    	else{}
    }		
#endif /* !__VDC */
}

static uint16_t  LCEVP_s16ILimitRange( uint16_t InputValue, uint16_t MinValue, uint16_t MaxValue )
{
	/*=============================================================================*/
	/* Purpose : Limiting the Maximum and Minimum value of InputValue			   */
	/* Input Variable : InputValue, MinValue, MaxValue	  		   				   */
	/* Output Variable : OutputValue 						   				       */
	/*=============================================================================*/
	uint16_t OutputValue;

	if 		(InputValue > MaxValue)
	{
		OutputValue = MaxValue;
	}
	else if (InputValue < MinValue)
	{
		OutputValue = MinValue;
	}
	else
	{
		OutputValue = InputValue;
	}
	return OutputValue;
}	

#endif /* #if __EVP */

  	#if __VACUUM_PUMP_RELAY
void	LAEVP_vActuateVacuumPump(void)
{
	static uint16_t	lhbbu16VacuumPumpOnTime;	
	
  /* Moved from L_vCallMain() */
    if((fu1OnDiag==1)||(init_end_flg==0)||(fu1ECUHwErrDet==1)||(fu1VoltageUnderErrDet==1)||(fu1VoltageOverErrDet==1))
    {	
    	lhbbu1VacuumPumpOn=0;
    }
    else
  /* Moved from L_vCallMain() */
    {		
		if((fcu1VacuumErrFlg==0) && (fu1VacuumStateDetect==1))		
		{
			lhbbu1VacuumPumpOn = 1;
		}
		else
		{
			;
		}
		
		if(lhbbu1VacuumPumpOn == 1)
		{	
			if (lhbbu16VacuumPumpOnTime>0)
			{
				lhbbu16VacuumPumpOnTime--;
			}
			else
			{
				lhbbu16VacuumPumpOnTime = L_U16_TIME_10MSLOOP_5S;	// 5sec	
				if(fu1VacuumStateDetect==0)
				{
					lhbbu1VacuumPumpOn = 0;
				}
				else
				{
					;
				}
			}	
		}
		else
		{
			lhbbu16VacuumPumpOnTime = L_U16_TIME_10MSLOOP_5S;  
		}	
    }					
}	
  	#endif


