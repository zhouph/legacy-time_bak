/******************************************************************************
* Project Name: Hydraulic Brake Boost
* File: LCLVBACallControl.C
* Description: LVBA control algorithm
* Date: October. 13. 2006
******************************************************************************/

/* Includes ******************************************************************/
#include "LCLVBACallControl.h"
#include "LCFBCCallControl.h"
#include "LCHSACallControl.h"
#include "LCHDCCallControl.h"
#include "LCACCCallControl.h"
#include "LCEPBCallControl.h"
#include "LCESPCalInterpolation.h"
#include "LCESPCalLpf.h"
#include "LALVBACallActHW.h"
#include "Hardware_Control.h"
#include "LSESPCalSensorOffset.h"
#include "LAABSCallActCan.h"

#if __ADVANCED_MSC
#include "LAABSCallMotorSpeedControl.h"
#endif



#if defined(__CAN_SW)
    #if __Vacuum_Sen_CBox_AD
extern uint16_t Vacuum_mon_ad;
    #endif
#endif

  #if   __LVBA
/* Local Definiton  **********************************************************/

/* Variables Definition*******************************************************/
struct	U8_BIT_STRUCT_t LVBA01, LVBA02, LVBA03, LVBA04, LVBA05;

uint8_t   LVBA_MSC_DEBUGGER, lclvbau8AbsInLvbaCnt, lclvbas8ControlState;
int16_t     lclvbas16LVBAReleaseCnt,lclvbas16LVBARlsMpresCnt, lclvbas16AssistPressBigRiseCnt;
uint8_t		lclvbau8LVBAOffCnt, lclvbau8LVBAOnCnt;
int16_t     lclvbas16VacuumLevel, lclvbas16LVBAStartSpeedThres, lclvbas16LVBAStartMpresThres;
int16_t     lclvbas16AssistPressure, lclvbas16AssistPressureOld2;
int16_t     lclvbas16AssistPressureOld, lclvbas16AssistPressureFilter, lclvbas16AssistPressureRate, lclvbas16AssistPressure_inc_cnt, lclvbas16AssistPressure_dec_cnt;
int16_t     lclvbas16DelBoostPress, lclvbas16FadeOutTcCurrent;
/*int16_t     knee_point_pressure, lclvbas16LVBATargetMpress;*/

/*int16_t     lvba_tempW0,lvba_tempW1,lvba_tempW2;*/

    #if (__VACUUM_SENSOR_ENABLE || defined(VAC_SENSOR_INSTALLED))
int16_t     lclvbas16VacuumPress;
int16_t     lclvbas16VacuumPressSlop, lclvbas16VacuumPressOld[10];
int16_t     lclvbas16VacPressSum, lclvbas16VacmPressAvg, lclvbas16VacmPressAvgOld[7], lclvbas16VacmPressAvg700;
int8_t    lclvbas16VacmPressCnt;
uint8_t   lclvbau8AftrBrkTimer;
/*int8_t    pres_sum_cnt;*/
int16_t     lclvbas16VacmPressAvgSum;

    #endif
int8_t    lclvbau8LVBADeactTimer;
int8_t	lclvbas8LVBAOtherCtrlEnable;
/*int16_t     output_at_vacuum_10percent;
int16_t     output_at_vacuum_40percent;
int16_t     output_at_vacuum_70percent;
int16_t     output_at_vacuum_100percent;*/
int16_t     lclvbas16AssistPressure_10percent;
int16_t     lclvbas16AssistPressure_40percent;
int16_t     lclvbas16AssistPressure_70percent;
int16_t     lclvbas16AssistPressure_100percent;
int16_t     lclvcas16LVBANeedBrkFluidCntP, lclvcas16LVBANeedBrkFluidCntS;
int16_t     lclvbas16RearEBDCutPress;
int16_t     lclvbas16LVBAMpressSlop, lclvbas16LVBAMpressSlopMax; /*lcs16HbbTargetMCPress, lcs16HbbTargetMCPressBuffe*/
int8_t    lclvbas8LVBAMpressFastDecCnt, lclvbas8LVBAPressMode, lclvbas8LVBAMpressSlowDecCnt;
int8_t    lclvbas8OtherCtrlEnable;
int16_t     lclvbas16LVBAMpressBuffer[14], lclvbas16LVBAMpressSlop100MS, lclvbas16LVBAMpSlopSumPos, lclvbas16LVBAMpSlopSumNeg;
uint8_t   lclvbau8LVBAMPSlopCnt, lclvbau8MPReleaseCnt, lclvbau8LVBABoostOnCnt, lclvbau8LVBABoostOffCnt;
uint8_t   lclvbau8LVBAPressDecCnt, lclvbau8LVBAPresDecAftTimer, lclvbau8LVBAStopApply;
int8_t    lclvbas8MscTargetVolDecCnt, lclvbas8MscTargetVolIncCnt;
  #if defined(PEDAL_SENSOR_INSTALLED)
int16_t     Pedal_Travel, Pedal_Travel_Buff[5], Pedal_Travel_cnt, Pedal_Travel_rate, Pedal_Travel_temp;
int16_t     Pedal_Travel_X10_LPF_1HZ, Pedal_Travel_X10_LPF_3HZ, Pedal_Travel_X10_LPF_3HZ_AVG, Pedal_Travel_X10_Buff[7], Pedal_Travel_X10_Sum;
int8_t    Pedal_Travel_AVG_cnt;
  #endif
int16_t     lclvbas16LVBATargetMpress, lclvbau8EngineStartCnt, lclvbau16EngineRPMOld, lclvbas16LvbaEnterSpeed; 
int16_t     lclvbas16LVBATargetMpressOld; /*output_rate_at_input_force;*/
/*int16_t     output_at_pedal_travel, assist_at_pedal_travel_1BAR, output_at_pedal_travel_X10, output_at_pedal_travel_X10_old,output_comp_MP;*/
int16_t     max_lvba_msc_target_vol;
int16_t     lclvbas16MPressAvg, lclvbas16MPressSum, lclvbas16MPressBuff[14], lclvbas16MPressAvgOld, lclvbas16LVBAEnterMP;
uint8_t   lclvbau1LVBAInhibitFlg, lclvbau8LVBAStartMpresSlop, lclvbau8MpressCnt;
uint8_t	LVBAdebugger;
uint8_t	lchbbu8LvbaCtrlMode;
int8_t    lclvbas8PressTemp;
 

/* LocalFunction prototype ***************************************************/
    #if (__VACUUM_SENSOR_ENABLE || defined(VAC_SENSOR_INSTALLED))
static void LCLVBA_vCallCheckVacuumPress(void);
static void LCLVBA_vCallDecideVacuumLevel(void);
    #endif
static void LCLVBA_vCheckEngineStatus(void);
static void LCLVBA_vDecideLVBAInhibition(void);
static void LCLVBA_vDecideAssistPressure(void);
static void LCLVBA_vDecideAssistPressChange(void);
static void LCLVBA_vDecideAdditionalBrkFluid(void);
static void LCLVBA_vDctMpressThres(void);
static void LCLVBA_vDctMPRelease(void);
void LCLVBA_vCallVariableReset(void);
void LCMSC_vSetLVBATargetVoltage(void);

  #if (__SPLIT_TYPE==1)
/*static void LCLVBA_DecideBrakeBoostFRSPLIT(void);*/
static void LCLVBA_vDecideBrakeBoostStart(void);
  #else  /* __SPLIT_TYPE==0 */
static void LCLVBA_vDecideBrakeBoostStart(void);
  #endif  /* __SPLIT_TYPE==0 */
static uint8_t LCLVBA_u8DecideLvbaEntrance(void);
static void LCLVBA_vDecideVlvActMode(void);
static void LCVLBA_vDecideLVBARelealse(void);
static void LCLVBA_vCalcAvgMCPressure(void);
static UCHAR LCLVBA_u8DecideLvbaExit(void);


/* GlobalFunction prototype **************************************************/


/* Implementation*************************************************************/



/******************************************************************************
* FUNCTION NAME:      LCLVBA_vCallCheckVacuumPress
* CALLED BY:          LCLVBA_vCallControl()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Calculation Booster pressure
******************************************************************************/

  #if (__VACUUM_SENSOR_ENABLE || defined(VAC_SENSOR_INSTALLED))
static void LCLVBA_vCallCheckVacuumPress(void)
{	
    static uint8_t	lclvbau8VacmPressSlopCnt = 0;
    int16_t s16VacuumPressDiff;
    /******************************************************************************
     Uout = (C1*Pe+C0)*Us 
     where, Uout = signal output voltage in V
            Us   = supply voltage in V
            Pe   = differential pressure in kPa
            C0   = 0.1
            C1   = -0.008 kPa
     
     ==> Pe = ((Uout/Us)-0.1)/(-0.008) = -(Uout*10000/Us/8 - 1000/8)
     
     vacuum_1_10kpa
      - Range : 0 ~ 100kPa
      - Resolution : 0.1kPa
    ******************************************************************************/
    /*tempW0 = fs16FilteredVacuum;//filtered vacuum_1_10kpa*/
    if(fou1Sen5VPwr1secOk==1)
    {
    	tempW0 = vacuum_1_10kpa;/*fs16CalcVacuum = -vacuum_1_10kpa*/
    /*empW0 = (INT)vacumn_mon_ad;*/
	}	

    tempW1 = lclvbas16VacuumPress;
    lclvbas16VacuumPress = LCESP_s16Lpf1Int(tempW0, tempW1, L_FILTER_GAIN_1_5HZ);  /* 1.4 Hz */

    if(speed_calc_timer > 10)
    {
		s16VacuumPressDiff = lclvbas16VacuumPress - lclvbas16VacuumPressOld[lclvbau8VacmPressSlopCnt];
	    lclvbas16VacuumPressSlop = (int16_t)(((int32_t)s16VacuumPressDiff*L_TIME_1000MS)/U8_VACUUM_PRESS_BUF_SIZE);/*kPa/sec*/
		
        if(lclvbas16VacuumPressSlop>2000)
        {
            lclvbas16VacuumPressSlop = 2000;
        }
        else if(lclvbas16VacuumPressSlop<-2000)
        {
            lclvbas16VacuumPressSlop = -2000;
        }
        else
        {
            ;
        }
    }
    else
    {
        lclvbas16VacuumPressSlop = 0;
    }
	lclvbas16VacuumPressOld[lclvbau8VacmPressSlopCnt] = lclvbas16VacuumPress;
	lclvbau8VacmPressSlopCnt = lclvbau8VacmPressSlopCnt + 1;
	if(lclvbau8VacmPressSlopCnt >= U8_VACUUM_PRESS_BUF_SIZE)
	{
		lclvbau8VacmPressSlopCnt = 0;
	}
}

static void LCLVBA_vCallDecideVacuumLevel(void)
{        
	static uint8_t lclau8VacmAvgCnt;
	
    if(speed_calc_timer > 10)
    {
        if(lclvbas16VacuumPressSlop<0)
        {
            if(lclvbau1VacPressDecFlg==0)
            {
                lclvbau1VacPressDecFlg = 1;
            }
            if(lclvbau1VacPressIncFlg==1)
            {
                lclvbau1VacPressIncFlg = 0;
            }
        }
        else if(lclvbas16VacuumPressSlop>0)
        {
            if(lclvbau1VacPressDecFlg==1)
            {
                lclvbau1VacPressDecFlg = 0;
            }
            if(lclvbau1VacPressIncFlg==0)
            {
                lclvbau1VacPressIncFlg = 1;
            }
        }
        else
        {
            ;
        }

        if(((mpress >= MPRESS_2BAR) || ((mpress>MPRESS_0BAR)&&(lclvbau1ActiveFlg==1)))&&(speed_calc_timer >= (uint8_t)T_1500_MS))
        {
            lclvbau8AftrBrkTimer = L_TIME_1000MS; /*L_TIME_100MS - NZ demo*/
            lclvbau1DctVacuumLevelAftBrk = 0;
        }
        else 
        {
            if(lclvbau8AftrBrkTimer>0)
            {
                lclvbau8AftrBrkTimer--;
            }
            else
            {
            	lclvbau1DctVacuumLevelAftBrk = 1;
            }
            	
        }

        if(lclvbau1ActiveFlg==1) {lclvbau1DctVacuumLevelAftBrk=0;LVBAdebugger = 100;}
                        
        if(lclvbas16VacmPressCnt<10)
        {
            lclvbas16VacPressSum = lclvbas16VacPressSum + lclvbas16VacuumPress;
            lclvbas16VacmPressCnt++;
        }
        else
        {
            /*lclvbas16VacPressSum = lclvbas16VacPressSum + lclvbas16VacuumPress;*/
            lclvbas16VacmPressAvg = lclvbas16VacPressSum/10;
            lclvbas16VacPressSum = 0;
            lclvbas16VacmPressCnt = 0;

            if((abs(lclvbas16VacuumPressSlop)<50) && (lclvbau1DctVacuumLevelAftBrk==1))
            {
                /*for (pres_i=9; pres_i>0; pres_i--)
                {
                    lclvbas16VacmPressAvgOld[pres_i] = lclvbas16VacmPressAvgOld[pres_i-1];
                    if(lclvbas16VacmPressAvgOld[pres_i] > 0)
                    {
                        pres_sum_cnt++;
                        lclvbas16VacmPressAvgSum = lclvbas16VacmPressAvgSum + lclvbas16VacmPressAvgOld[pres_i];
                    }
                    LVBAdebugger = 150;
                }
                lclvbas16VacmPressAvgOld[0] = lclvbas16VacmPressAvg;
                lclvbas16VacmPressAvgSum = lclvbas16VacmPressAvgSum + lclvbas16VacmPressAvgOld[0];
                lclvbas16VacmPressAvg700 = (lclvbas16VacmPressAvgSum/(pres_sum_cnt+1));*/
                lclvbas16VacmPressAvgSum = lclvbas16VacmPressAvgSum + lclvbas16VacmPressAvg - lclvbas16VacmPressAvgOld[lclau8VacmAvgCnt];
                lclvbas16VacmPressAvgOld[lclau8VacmAvgCnt] = lclvbas16VacmPressAvg;                
                lclau8VacmAvgCnt = lclau8VacmAvgCnt + 1;
                if(lclau8VacmAvgCnt>=7)
                {                	                	
                	lclau8VacmAvgCnt = 0;
                } 
                
                lclvbas16VacmPressAvg700 = (lclvbas16VacmPressAvgSum/7);
                
                LVBAdebugger=200;      
            }
        }

      #if __VACUUM_SENSOR_ENABLE
       #if (__CAR==GM_TAHOE)|| (__CAR ==GM_SILVERADO)
        if(lclvbas16VacmPressAvg700<=0)
        {           
            lclvbas16VacuumLevel = 95-(int16_t)((((int32_t)pha_mon_ad - 110)*10)/54);
        }
        else
        {
            lclvbas16VacuumLevel = 95-(int16_t)((int32_t)(lclvbas16VacmPressAvg700*10)/54);
        }           
       #else
        if(lclvbas16VacmPressAvg700<=0)
        {
			;/*lclvbas16VacuumLevel = (int16_t)((((int32_t)pha_mon_ad-LVBA_VACUUM_SEN_MIN)*LVBA_VACUUM_LEVEL_MAX)/(LVBA_VACUUM_SEN_MAX-LVBA_VACUUM_SEN_MIN));*/
        }
        else
        {
            if(lclvbas16VacmPressAvg700 > LVBA_VACUUM_SEN_MAX)
            {
                lclvbas16VacmPressAvg700 = LVBA_VACUUM_SEN_MAX;
                lclvbas16VacuumLevel = LVBA_VACUUM_LEVEL_MAX;
            }
            else if(lclvbas16VacmPressAvg700 < LVBA_VACUUM_SEN_MIN)
            {
                lclvbas16VacmPressAvg700 = LVBA_VACUUM_SEN_MIN;
                lclvbas16VacuumLevel = LVBA_VACUUM_LEVEL_MIN;
            }
            else
            {
                lclvbas16VacuumLevel = (int16_t)((((int32_t)lclvbas16VacmPressAvg700-LVBA_VACUUM_SEN_MIN)*LVBA_VACUUM_LEVEL_MAX)/(LVBA_VACUUM_SEN_MAX-LVBA_VACUUM_SEN_MIN));
                /*lclvbas16VacuumLevel = (int16_t)(((LVBA_VACUUM_SEN_MAX-(uint32_t)lclvbas16VacmPressAvg700)*LVBA_VACUUM_LEVEL_MAX)/(LVBA_VACUUM_SEN_MAX-LVBA_VACUUM_SEN_MIN));*/
            }
        }           
       #endif 
      /*#elif defined(VAC_SENSOR_INSTALLED)
       #if (__CAR==GM_TAHOE)
        lclvbas16VacuumLevel = 95-(int16_t)((int32_t)(lclvbas16VacmPressAvg700*10)/54);
       #elif (__CAR==HMC_BK)
        lclvbas16VacuumLevel = 100-(lclvbas16VacmPressAvg700/10);
       #else
        if(lclvbas16VacmPressAvg700<102) 
        {
            lclvbas16VacuumLevel = 0;
        }
        else 
        {
            lclvbas16VacuumLevel = (int16_t)((int32_t)(lclvbas16VacmPressAvg700-102)*100/820);
        }
       #endif*/ 
      #endif   /*__VACUUM_SENSOR_ENABLE*/    

		if(speed_calc_timer <= 200)/*inhibit LVBA because of low vacuum for 5s after engine run...*/
		{
	        if(lclvbau1LowVacuumDctFlg==0)
	        {
	            if(lclvbas16VacmPressAvg<=(int16_t)U8_LVBA_ACT_VACUUM_LEVEL)
	            {
	                lclvbau1LowVacuumDctFlg = 1;
	                lclvbau8LVBADeactTimer = 10;                
	            }
	            else
	            {
	            	lclvbau1LowVacuumDctFlg = 0;
	            }
	        }
	        else
	        {
	            if(lclvbas16VacmPressAvg>=U8_LVBA_DEACT_VACUUM_LEVEL)
	            {
	                if(lclvbau8LVBADeactTimer==0)
	                {
	                    lclvbau1LowVacuumDctFlg = 0;
	                }
	                else
	                {
	                    lclvbau8LVBADeactTimer--;
	                }
	            }
	        }
		}
		else
		{
	        if(lclvbau1LowVacuumDctFlg==0)
	        {
	            if(lclvbas16VacuumLevel<=U8_LVBA_ACT_VACUUM_LEVEL)
	            {
	                lclvbau1LowVacuumDctFlg = 1;
	                lclvbau8LVBADeactTimer = 10;                
	            }
	            else
	            {
	            	lclvbau1LowVacuumDctFlg = 0;
	            }
	        }
	        else
	        {
	            if(lclvbas16VacuumLevel>=U8_LVBA_DEACT_VACUUM_LEVEL)
	            {
	                if(lclvbau8LVBADeactTimer==0)
	                {
	                    lclvbau1LowVacuumDctFlg = 0;
	                }
	                else
	                {
	                    lclvbau8LVBADeactTimer--;
	                }
	            }
	        }
	    }
    }
    else
    {
        lclvbau1VacPressDecFlg = 0;
        lclvbau1VacPressIncFlg = 0;
        lclvbau1DctVacuumLevelAftBrk = 0;
        lclvbau8AftrBrkTimer = 0;
        lclvbas16VacmPressCnt = 0;
        lclvbas16VacPressSum = 0;
        lclvbas16VacmPressAvg = 0;
        lclvbas16VacmPressAvg700 = 0;
        lclvbas16VacmPressAvgOld[0] = 0;
        lclvbas16VacmPressAvgOld[1] = 0;
        lclvbas16VacmPressAvgOld[2] = 0;
        lclvbas16VacmPressAvgOld[3] = 0;
        lclvbas16VacmPressAvgOld[4] = 0;
        lclvbas16VacmPressAvgOld[5] = 0;
        lclvbas16VacmPressAvgOld[6] = 0;
        /*lclvbas16VacmPressAvgOld[7] = 0;
        lclvbas16VacmPressAvgOld[8] = 0;
        lclvbas16VacmPressAvgOld[9] = 0;*/
        lclvbas16VacuumLevel = 0;
        lclvbau8LVBADeactTimer = 0;
        lclvbau1LowVacuumDctFlg = 0;
    }
}
  #endif/*(__VACUUM_SENSOR_ENABLE || defined(VAC_SENSOR_INSTALLED))*/

static void LCLVBA_vCheckEngineStatus(void)
{	
	if(eng_rpm>200)
	{
		lclvbau1EngineRunFlg = 1; 
	}
	else
	{
		lclvbau1EngineRunFlg = 0; 
	}
	
	/* Engine Coolant Temperature: EMS2[1], TEMP_ENG =  0.75*(HEX)-48 ['C] (-48'C~142.5'C)*/
	if(eng_temp<55)/*55: -7degC*/
	{
		lclvbau1EngTempOkFlg = 1;
	}
	else
	{
		lclvbau1EngTempOkFlg = 0;
	}
	
#if (__LVBA_ENABLE_BY_ENG_VAR==ENABLE)
  #if (__CAR==KMC_KH)
  	if(lespu8EMS_ENG_VOL == 33)
  	{
  		lclvbau1EngineVarOkFlg = 1;
  	}
  #endif	
#endif/*(__LVBA_ENABLE_BY_ENG_VAR==ENABLE)*/

	lclvbau1EngineStatusOkForLVBA = 1;
}

static void LCLVBA_vDecideLVBAInhibition(void)
{	
/*fcu1VacuumErrFlg: Vacuum sensor H/W, Signal failure
  fu8VacuumSenSusflg: Vacuum sensor H/W suspect(until 1s detecting fcu1VacuumErrFlg)
  fu1Power5VErrDet: 5V sensor reference voltage failure(Analog G-Sensor, Vacuum Sensor, Pedal Sensor)*/
	if((fu1MCPErrorDet==1)||(fu1ESCEcuHWErrDet==1)||(fu1BLSErrDet==1)||/*(fu8VacuumSenSusflg==1)||*/(fcu1VacuumErrFlg==1)||(fu1Power5VErrDet==1)||(fou1Sen5VPwr1secOk==0)
	 ||(fu1WheelFLErrDet==1)||(fu1WheelFRErrDet==1)||(fu1WheelRLErrDet==1)||(fu1WheelRRErrDet==1)||(fu1FrontWSSErrDet==1)||(fu1RearWSSErrDet==1)
     ||(fu1OnDiag==1)	
  #if __BRAKE_FLUID_LEVEL==ENABLE
     ||(fu1DelayedBrakeFluidLevel==1)||(fu1LatchedBrakeFluidLevel==1)
  #endif  
  #if (__LVBA_ENABLE_BY_ENG_VAR == ENABLE)	
	 ||(lclvbau1EngineVarOkFlg!=1)
  #endif	
	)
	{
		lclvbau1InhibitFlg = 1;
	}
	else
	{
		lclvbau1InhibitFlg = 0;
	}
	
  #if __LVBA_ON_OFF_TEST
    if(fu1ESCDisabledBySW==1)
    {
        lclvbau1InhibitFlg = 1;
    }
  #endif  
  	
}

  #if defined(PEDAL_SENSOR_INSTALLED)
static void LCLVBA_vCalcBrakePedalTravel(void)
{
    lclvbas16LVBATemp02 = fs16CalcPedal1_Pecentage - OFFSET_PEDAL_TRAVEL;

    if(lclvbas16LVBATemp02 < 0) {lclvbas16LVBATemp02 = 0;}
        
    if(mot_mon_ad==0)
    {
        Pedal_Travel_temp = LCESP_s16Lpf1Int((lclvbas16LVBATemp02*10), Pedal_Travel_temp, L_FILTER_GAIN_7HZ); /* 7Hz */
        Pedal_Travel_X10_LPF_1HZ = Pedal_Travel_temp;
        Pedal_Travel_X10_LPF_3HZ = Pedal_Travel_temp;
    }
    else
    {
        Pedal_Travel_temp = LCESP_s16Lpf1Int((lclvbas16LVBATemp02*10), Pedal_Travel_temp, L_FILTER_GAIN_3HZ); /* 3Hz */
        Pedal_Travel_X10_LPF_1HZ = LCESP_s16Lpf1Int((lclvbas16LVBATemp02*10), Pedal_Travel_temp, L_FILTER_GAIN_1HZ); /* 1Hz */
        Pedal_Travel_X10_LPF_3HZ = Pedal_Travel_temp;
    }

    Pedal_Travel_X10_Sum = Pedal_Travel_X10_Sum - Pedal_Travel_X10_Buff[Pedal_Travel_AVG_cnt] + Pedal_Travel_X10_LPF_3HZ;
    Pedal_Travel_X10_LPF_3HZ_AVG = Pedal_Travel_X10_Sum/7;           
    
    Pedal_Travel_X10_Buff[Pedal_Travel_AVG_cnt] = Pedal_Travel_X10_LPF_3HZ;
    Pedal_Travel_AVG_cnt = Pedal_Travel_AVG_cnt + 1;
    if(Pedal_Travel_AVG_cnt>=7) {Pedal_Travel_AVG_cnt=0;}        

    Pedal_Travel = (Pedal_Travel_temp/10);

    Pedal_Travel_rate = LCESP_s16Lpf1Int((Pedal_Travel_X10_LPF_3HZ_AVG - Pedal_Travel_Buff[Pedal_Travel_cnt]), Pedal_Travel_rate, L_FILTER_GAIN_7HZ); /* 7Hz */ 
    Pedal_Travel_Buff[Pedal_Travel_cnt] = Pedal_Travel_X10_LPF_3HZ_AVG;
    Pedal_Travel_cnt = Pedal_Travel_cnt+1;
    if(Pedal_Travel_cnt>=5) {Pedal_Travel_cnt = 0;}

    if(Pedal_Travel_rate > 1)
    {
    	pedal_state = 1;
    }
    else if(Pedal_Travel_rate < -2)
    {
    	pedal_state = 0;
    }
    else
    {
    	;
    }
}
  #endif  /*PEDAL_SENSOR_INSTALLED*/


/*Mpress average for 100ms(14scan)*/
static void LCLVBA_vCalcAvgMCPressure(void)
{
    if(speed_calc_timer > 10)
    {                 
        lclvbas16MPressSum = lclvbas16MPressSum + mpress - lclvbas16MPressBuff[lclvbau8MpressCnt];          
        lclvbas16MPressAvg = lclvbas16MPressSum/14;           
        lclvbas16MPressAvgOld = lclvbas16MPressAvg;          	        
        lclvbas16MPressBuff[lclvbau8MpressCnt] = mpress;        
        lclvbau8MpressCnt = lclvbau8MpressCnt + 1;        
        if(lclvbau8MpressCnt>=13) {lclvbau8MpressCnt=0;}        
    }
    else
    {
        lclvbas16MPressAvg=0; lclvbas16MPressSum=0; lclvbau8MpressCnt=0; lclvbas16MPressAvgOld=0;
        lclvbas16MPressBuff[0]=0;
        lclvbas16MPressBuff[1]=0;
        lclvbas16MPressBuff[2]=0;
        lclvbas16MPressBuff[3]=0;
        lclvbas16MPressBuff[4]=0;
        lclvbas16MPressBuff[5]=0;
        lclvbas16MPressBuff[6]=0;
        lclvbas16MPressBuff[7]=0;
        lclvbas16MPressBuff[8]=0;
        lclvbas16MPressBuff[9]=0;
        lclvbas16MPressBuff[10]=0;
        lclvbas16MPressBuff[11]=0;
        lclvbas16MPressBuff[12]=0;
        lclvbas16MPressBuff[13]=0;
    }           
}

/*assist pressure determined by Vacuum Level(lclvbas16LVBATargetMpress)*/
static void LCLVBA_vDecideAssistPressure(void)
{
    int16_t input_at_vacuum_level=0;
    int16_t lclvbas16LVBAEndMpress = 0;
    int16_t lclvbas16MPforBoostAssist = 0;
    int16_t output_at_temp_V80 = 0;
    int16_t output_at_temp_V90 = 0;
    int16_t output_at_temp_V100 = 0;

    if(lclvbas16VacuumLevel<90)
    {
    	lclvbas16LVBATargetMpressOld = lclvbas16LVBATargetMpress;        
        

        if(lclvbas16VacuumLevel <= 10)      
        {
        	lclvbas16LVBATargetMpress = LCESP_s16IInter4Point( lclvbas16MPressAvg,                  
        	    												S16_LVBA_OUTPUT1_AT_10PERCENT, S16_LVBA_MPRESS1_AT_10PERCENT,
        	    												S16_LVBA_OUTPUT2_AT_10PERCENT, S16_LVBA_MPRESS2_AT_10PERCENT,
        	    												S16_LVBA_OUTPUT3_AT_10PERCENT, S16_LVBA_MPRESS3_AT_10PERCENT,
        	    												S16_LVBA_OUTPUT4_AT_10PERCENT, S16_LVBA_MPRESS4_AT_10PERCENT );
        }      
        else if(lclvbas16VacuumLevel <= 40)      
        {          
        	lclvbas16LVBAEndMpress = (int16_t)((int32_t)S16_LVBA_MPRESS2_AT_40PERCENT*(lclvbas16VacuumLevel)/40); 
          	if(lclvbas16MPressAvg>=lclvbas16LVBAEndMpress)
            {
                lclvbas16LVBATargetMpress = LCESP_s16IInter4Point( lclvbas16MPressAvg,
                    												S16_LVBA_OUTPUT1_AT_40PERCENT, S16_LVBA_MPRESS1_AT_40PERCENT,
                    												S16_LVBA_OUTPUT2_AT_40PERCENT, S16_LVBA_MPRESS2_AT_40PERCENT,
                    												S16_LVBA_OUTPUT3_AT_40PERCENT, S16_LVBA_MPRESS3_AT_40PERCENT,
                    												S16_LVBA_OUTPUT4_AT_40PERCENT, S16_LVBA_MPRESS4_AT_40PERCENT );
            }
            else
            {
                lclvbas16MPforBoostAssist = LCESP_s16IInter4Point(lclvbas16LVBAEndMpress,
                    												S16_LVBA_MPRESS1_AT_10PERCENT, S16_LVBA_OUTPUT1_AT_10PERCENT,
                    												S16_LVBA_MPRESS2_AT_10PERCENT, S16_LVBA_OUTPUT2_AT_10PERCENT,
                    												S16_LVBA_MPRESS3_AT_10PERCENT, S16_LVBA_OUTPUT3_AT_10PERCENT,
                    												S16_LVBA_MPRESS4_AT_10PERCENT, S16_LVBA_OUTPUT4_AT_10PERCENT );
                lclvbas16LVBATargetMpress = LCESP_s16IInter4Point(lclvbas16MPressAvg-lclvbas16LVBAEndMpress+lclvbas16MPforBoostAssist,
                    												S16_LVBA_OUTPUT1_AT_10PERCENT, S16_LVBA_MPRESS1_AT_10PERCENT,
                    												S16_LVBA_OUTPUT2_AT_10PERCENT, S16_LVBA_MPRESS2_AT_10PERCENT,
                    												S16_LVBA_OUTPUT3_AT_10PERCENT, S16_LVBA_MPRESS3_AT_10PERCENT,
                    												S16_LVBA_OUTPUT4_AT_10PERCENT, S16_LVBA_MPRESS4_AT_10PERCENT );
            }
        }
        else if(lclvbas16VacuumLevel <= 70)
        {
            lclvbas16LVBAEndMpress = (int16_t)(((int32_t)S16_LVBA_MPRESS2_AT_70PERCENT*(lclvbas16VacuumLevel))/70);

            if(lclvbas16MPressAvg>=lclvbas16LVBAEndMpress)
            {
                lclvbas16LVBATargetMpress = LCESP_s16IInter4Point(lclvbas16MPressAvg,
                    												S16_LVBA_OUTPUT1_AT_70PERCENT, S16_LVBA_MPRESS1_AT_70PERCENT,
                    												S16_LVBA_OUTPUT2_AT_70PERCENT, S16_LVBA_MPRESS2_AT_70PERCENT,
                    												S16_LVBA_OUTPUT3_AT_70PERCENT, S16_LVBA_MPRESS3_AT_70PERCENT,
                    												S16_LVBA_OUTPUT4_AT_70PERCENT, S16_LVBA_MPRESS4_AT_70PERCENT );
            }
            else
            {
                lclvbas16MPforBoostAssist = LCESP_s16IInter4Point(lclvbas16LVBAEndMpress,
                    												S16_LVBA_MPRESS1_AT_40PERCENT, S16_LVBA_OUTPUT1_AT_40PERCENT,
                    												S16_LVBA_MPRESS2_AT_40PERCENT, S16_LVBA_OUTPUT2_AT_40PERCENT,
                    												S16_LVBA_MPRESS3_AT_40PERCENT, S16_LVBA_OUTPUT3_AT_40PERCENT,
                    												S16_LVBA_MPRESS4_AT_40PERCENT, S16_LVBA_OUTPUT4_AT_40PERCENT );
                lclvbas16LVBATargetMpress = LCESP_s16IInter4Point(lclvbas16MPressAvg-lclvbas16LVBAEndMpress+lclvbas16MPforBoostAssist,
                    												S16_LVBA_OUTPUT1_AT_40PERCENT, S16_LVBA_MPRESS1_AT_40PERCENT,
                    												S16_LVBA_OUTPUT2_AT_40PERCENT, S16_LVBA_MPRESS2_AT_40PERCENT,
                    												S16_LVBA_OUTPUT3_AT_40PERCENT, S16_LVBA_MPRESS3_AT_40PERCENT,
                    												S16_LVBA_OUTPUT4_AT_40PERCENT, S16_LVBA_MPRESS4_AT_40PERCENT );
            }
        }
        else
        {
            lclvbas16LVBAEndMpress = (int16_t)(((int32_t)S16_LVBA_MPRESS2_AT_100PERCENT*(lclvbas16VacuumLevel))/100);
            if(lclvbas16MPressAvg>=lclvbas16LVBAEndMpress)
            {
                lclvbas16LVBATargetMpress = LCESP_s16IInter4Point(lclvbas16MPressAvg,
                    												S16_LVBA_OUTPUT1_AT_100PERCENT, S16_LVBA_MPRESS1_AT_100PERCENT,
                    												S16_LVBA_OUTPUT2_AT_100PERCENT, S16_LVBA_MPRESS2_AT_100PERCENT,
                    												S16_LVBA_OUTPUT3_AT_100PERCENT, S16_LVBA_MPRESS3_AT_100PERCENT,
                    												S16_LVBA_OUTPUT4_AT_100PERCENT, S16_LVBA_MPRESS4_AT_100PERCENT );
            }
            else
            {
                lclvbas16MPforBoostAssist = LCESP_s16IInter4Point(lclvbas16LVBAEndMpress,
                    												S16_LVBA_MPRESS1_AT_70PERCENT, S16_LVBA_OUTPUT1_AT_70PERCENT,
                    												S16_LVBA_MPRESS2_AT_70PERCENT, S16_LVBA_OUTPUT2_AT_70PERCENT,
                    												S16_LVBA_MPRESS3_AT_70PERCENT, S16_LVBA_OUTPUT3_AT_70PERCENT,
                    												S16_LVBA_MPRESS4_AT_70PERCENT, S16_LVBA_OUTPUT4_AT_70PERCENT );
                lclvbas16LVBATargetMpress = LCESP_s16IInter4Point(lclvbas16MPressAvg-lclvbas16LVBAEndMpress+lclvbas16MPforBoostAssist,
                    												S16_LVBA_OUTPUT1_AT_70PERCENT, S16_LVBA_MPRESS1_AT_70PERCENT,
                    												S16_LVBA_OUTPUT2_AT_70PERCENT, S16_LVBA_MPRESS2_AT_70PERCENT,
                    												S16_LVBA_OUTPUT3_AT_70PERCENT, S16_LVBA_MPRESS3_AT_70PERCENT,
                    												S16_LVBA_OUTPUT4_AT_70PERCENT, S16_LVBA_MPRESS4_AT_70PERCENT );
            }
        } 
             
        if((lclvbas16LVBATargetMpress/10)> lclvbas16MPressAvg) 
        {
        	lclvbas16LVBATargetMpress = (int16_t)(lclvbas16MPressAvg*10);
        }
      
        /*if(lclvbau1ActiveFlg==0) 
        {
        	lclvbas16LVBAEnterMP = lclvbas16MPressAvg;
        }
        else
        {
        	if(lclvbas16LVBAEnterMP < lclvbas16MPressAvg) 
        	{
        		lclvbas16LVBAEnterMP = lclvbas16MPressAvg;
        	}
        } 
                
        if(((lclvbas16MPressAvg >= MPRESS_0_1BAR)||(mpress>=20)) && ((lcs16HbbMpressSlop>=10)
          #if defined(PEDAL_SENSOR_INSTALLED)
        ||(Pedal_Travel_rate >= 0)
          #endif 
        ))
        {
        	if(lclvbas16LVBATargetMpressOld <= lclvbas16LVBATargetMpress)
        	{
        		tempT1 = 2;
        		
	            if((lclvbas16LVBAEnterMP > lclvbas16MPressAvg) && (lclvbas16MPressAvg>lclvbas16MPressAvgOld))
	            {
	            	tempT1 = 3;
	            	lcs16HbbTemp01 = LCESP_s16IInter3Point(lclvbas16VacuumLevel, 80, 20, 90, 30, 95, 80);
	            	output_comp_MP += (int16_t)((int32_t)(lclvbas16MPressAvg - lclvbas16MPressAvgOld)*lcs16HbbTemp01/20);		            	
	            }
	            else
	            {
	            	lcs16HbbTemp02 = LCESP_s16IInter4Point(lclvbas16VacuumLevel, 60, 30, 75, 2, 90, 2, 95, 1);
	            	lcs16HbbTemp03 = LCESP_s16IInter4Point(lclvbas16VacuumLevel, 60, 4, 75, 1, 80, 1, 90, 0);
			        if((lcs16HbbMpressSlop>=lcs16HbbTemp02) && (mpress+MPRESS_15BAR>knee_point_pressure) && (MCpress[0]-MCpress[1]>lcs16HbbTemp03))
			        {
		            	lcs16HbbTemp01 = LCESP_s16IInter3Point(lclvbas16VacuumLevel, 60, 80, 100, 30, 60, 80);
		            	lcs16HbbTemp03 = LCESP_s16IInter3Point(lcs16HbbMpressSlop, (lcs16HbbTemp02*4/5), 1, lcs16HbbTemp02, 2, (lcs16HbbTemp02*2), 4);
		            	lcs16HbbTemp04 = (int16_t)((int32_t)(MCpress[0]-MCpress[1])*lcs16HbbTemp01*lcs16HbbTemp03/50);
		            }
		            else 
		            {
		            	lcs16HbbTemp04 = 0;
		            }			            

            		lcs16HbbTemp01 = ((lclvbas16LVBATargetMpress/3)-lclvbas16MPressAvg)-((lclvbas16LVBATargetMpressOld/3)-lclvbas16MPressAvgOld);

            		if(lcs16HbbTemp04>lcs16HbbTemp01) 
            		{
            			output_comp_MP += lcs16HbbTemp04; 
            			tempT1 = 4;
            		}
            		else 
            		{
            			output_comp_MP += lcs16HbbTemp01; 
            			tempT1 = 5;
            		}
            	}
	        }
            else
            {
            	tempT1 = 6;;
            }
        }
        else
        { }*/
        lclvbas16AssistPressure = lclvbas16LVBATargetMpress - lclvbas16MPressAvg;
        
        lclvbas16AssistPressure = LCESP_s16Lpf1Int(lclvbas16AssistPressure, lclvbas16AssistPressureOld2, L_FILTER_GAIN_10HZ);
        
        lclvbas16AssistPressure = LCABS_s16LimitMinMax(lclvbas16AssistPressure, MPRESS_3BAR, MPRESS_150BAR);

        lclvbas16AssistPressureOld = lclvbas16AssistPressureFilter;
        lclvbas16AssistPressureOld2 = lclvbas16AssistPressure;
        lclvbas16AssistPressureFilter = LCESP_s16Lpf1Int((lclvbas16AssistPressure*10), lclvbas16AssistPressureOld, L_FILTER_GAIN_0_7HZ);
        lclvbas16AssistPressureRate = ((lclvbas16AssistPressureFilter - lclvbas16AssistPressureOld)*100)/7;

        if((lclvbas16AssistPressure<MPRESS_1BAR) && (lclvbas16AssistPressure>0))
        {
            if(lclvbas16VacuumLevel <= 10)
            {
                lclvbas16AssistPressure = MPRESS_1BAR;/*minimum assist pressure at vacuum fully fail*/
            }
        }
        else
        {
            ;
        }

        /*if((MPRESS_BRAKE_ON==0)&&(lclvbau8LVBAOnCnt==0))
        {
        	lclvbas16AssistPressure = 0;
        	lclvbas16AssistPressureOld = 0; 
        	lclvbas16AssistPressureFilter = 0;
        	lclvbas16AssistPressureRate = 0;
        	knee_point_pressure = 0;
        	lclvbas16LVBATargetMpress=0; 
        	lclvbas16LVBATargetMpressOld=0;
        	output_comp_MP = 0;
        }*/
    }
    else
    {
        lclvbas16AssistPressure = 0;
        lclvbas16AssistPressureOld = 0; 
        lclvbas16AssistPressureFilter = 0;
        lclvbas16AssistPressureRate = 0;
        lclvbas16LVBATargetMpress=0; 
        lclvbas16LVBATargetMpressOld=0;
    }
}

static void LCLVBA_vDecideAdditionalBrkFluid(void)
{
  /*
  #if (__SPLIT_TYPE==1)
    if(vref>=VREF_0_25_KPH)
    {
        if(ABS_fz==0)
        {
            lclvbau1AdditionalBrkFluidP = 1;
            if(EBD_RA==0)
            {
                lclvbau1AdditionalBrkFluidS = 1;
                lclvbas16RearEBDCutPress = 0;
            }
            else
            {
                lclvbau1AdditionalBrkFluidS = 0;

                if(lclvbas16RearEBDCutPress==0) {lclvbas16RearEBDCutPress = mpress;}
                else if((lclvbas16RearEBDCutPress + MPRESS_5BAR) >= mpress)
                {
                    lclvbau1AdditionalBrkFluidS = 1;
                }
                else { }
            }
            
            if((lclvbas16VacuumLevel>=80) && ((LVBA_BOOST_NEED_flg==0) || (LVBA_PRESS_DECREASE_NEED_flg==1)))
            {
                lclvbau1AdditionalBrkFluidP = 0;
                lclvbau1AdditionalBrkFluidS = 0;
            }
        }
        else
        {
            lclvbau1AdditionalBrkFluidP = 0;
            lclvbau1AdditionalBrkFluidS = 0;

            if((AV_DUMP_fl==1) || (AV_DUMP_fr==1))
            {
                lclvcas16LVBANeedBrkFluidCntP = L_TIME_1000MS;
            }
            else
            {
                if((ABS_LIMIT_SPEED_fl==1) && (ABS_LIMIT_SPEED_fr==1) && (lclvcas16LVBANeedBrkFluidCntP>L_TIME_30MS))
                {
                    lclvcas16LVBANeedBrkFluidCntP = L_TIME_30MS;
                }
                else if((FL.state==DETECTION) && (FR.state==DETECTION) && (lclvcas16LVBANeedBrkFluidCntP>L_TIME_150MS))
                {
                    lclvcas16LVBANeedBrkFluidCntP = L_TIME_150MS;
                }
                else if(((FL.state==DETECTION) || (FR.state==DETECTION)) && (lclvcas16LVBANeedBrkFluidCntP>0))
                {
                    lclvcas16LVBANeedBrkFluidCntP = lclvcas16LVBANeedBrkFluidCntP-1;
                }
                else { }

                if(lclvcas16LVBANeedBrkFluidCntP==0)
                {
                    if((FL.state==DETECTION) && ((FL.s16_Estimated_Active_Press+MPRESS_5BAR)>=mpress))
                    {
                        lclvbau1AdditionalBrkFluidP = 1;
                    }
                    if((FR.state==DETECTION) && ((FR.s16_Estimated_Active_Press+MPRESS_5BAR)>=mpress))
                    {
                        lclvbau1AdditionalBrkFluidP = 1;
                    }
                }
            }

            if((AV_DUMP_rl==1) || (AV_DUMP_rr==1))
            {
                lclvcas16LVBANeedBrkFluidCntS = L_TIME_300MS;
            }
            else
            {
                if((ABS_LIMIT_SPEED_rl==1) && (ABS_LIMIT_SPEED_rr==1) && (lclvcas16LVBANeedBrkFluidCntS>L_TIME_30MS))
                {
                    lclvcas16LVBANeedBrkFluidCntS = L_TIME_30MS;
                }
                else if((RL.state==DETECTION) && (RR.state==DETECTION) && (lclvcas16LVBANeedBrkFluidCntS>L_TIME_150MS))
                {
                    lclvcas16LVBANeedBrkFluidCntS = L_TIME_150MS;
                }
                else if(((RL.state==DETECTION) || (RR.state==DETECTION)) && (lclvcas16LVBANeedBrkFluidCntS>0))
                {
                    lclvcas16LVBANeedBrkFluidCntS = lclvcas16LVBANeedBrkFluidCntS-1;
                }
                else { }

                if(lclvcas16LVBANeedBrkFluidCntS==0)
                {
                    if((RL.state==DETECTION) && ((RL.s16_Estimated_Active_Press+MPRESS_5BAR)>=mpress))
                    {
                        lclvbau1AdditionalBrkFluidS = 1;
                    }
                    if((RR.state==DETECTION) && ((RR.s16_Estimated_Active_Press+MPRESS_5BAR)>=mpress))
                    {
                        lclvbau1AdditionalBrkFluidS = 1;
                    }

                    if((RL.s16_Estimated_Active_Press>=MPRESS_80BAR) || (RR.s16_Estimated_Active_Press>=MPRESS_80BAR)
                      || (mpress>MPRESS_80BAR))
                    {
                        lclvbau1AdditionalBrkFluidS = 0;
                    }
                }
            }
        }
    }
    else
    {
        lclvbau1AdditionalBrkFluidP = 0;
        lclvbau1AdditionalBrkFluidS = 0;
    }
  #else
  081208*/

    if((vref>=VREF_0_25_KPH)||(lclvbas16VacuumLevel<=10))
    {
        if(ABS_fz==0)
        {
            lclvbau1AdditionalBrkFluidP = 1;
            lclvbau1AdditionalBrkFluidS = 1;
            /*if((lclvbas16VacuumLevel<=40) && ((LVBA_BOOST_NEED_flg==0) || (LVBA_PRESS_DECREASE_NEED_flg==1)))
            {
                lclvbau1AdditionalBrkFluidP = 0;
                lclvbau1AdditionalBrkFluidS = 0;
            }*/ /* checking later!!! */
        }
        else/*In ABS*/
        {
            if((lclvbau8AbsInLvbaCnt<=35) && (vref>=VREF_0_5_KPH))/*ESV valve active for 250ms in ABS*/
            {
                lclvbau1AdditionalBrkFluidP = 1;
                lclvbau1AdditionalBrkFluidS = 1;
            }
            else
            {
                lclvbau1AdditionalBrkFluidP = 0;/*after 250ms, ESV v/v off because of ABS pumping for LPA clearing*/
                lclvbau1AdditionalBrkFluidS = 0;

                #if (__SPLIT_TYPE==1)
                if((AV_DUMP_fl==1) || (AV_DUMP_fr==1))
                #else
                if((AV_DUMP_fl==1) || (AV_DUMP_fr==1))
                #endif
                {
                    lclvcas16LVBANeedBrkFluidCntP = L_TIME_300MS;
                }
                else
                {
                    if((ABS_LIMIT_SPEED_rl==1) && (ABS_LIMIT_SPEED_fr==1) && (lclvcas16LVBANeedBrkFluidCntP>L_TIME_30MS))
                    {
                        lclvcas16LVBANeedBrkFluidCntP = L_TIME_30MS;
                    }
                    else 
                   #if (__SPLIT_TYPE==1)
                   		if((FL.state==DETECTION) && (FR.state==DETECTION) && (lclvcas16LVBANeedBrkFluidCntP>L_TIME_150MS))
                   #else
                    	if((RL.state==DETECTION) && (FR.state==DETECTION) && (lclvcas16LVBANeedBrkFluidCntP>L_TIME_150MS))
                   #endif
                    {
                        lclvcas16LVBANeedBrkFluidCntP = L_TIME_150MS;
                    }
                    #if (__SPLIT_TYPE==1)
                    else if(((FL.state==DETECTION) || (FR.state==DETECTION)) && (lclvcas16LVBANeedBrkFluidCntP>0))
                    #else
                    else if(((RL.state==DETECTION) || (FR.state==DETECTION)) && (lclvcas16LVBANeedBrkFluidCntP>0))
                    #endif
                    {
                        lclvcas16LVBANeedBrkFluidCntP = lclvcas16LVBANeedBrkFluidCntP-1;
                    }
                    else { }
    
                    if(lclvcas16LVBANeedBrkFluidCntP==0)
                    {
                      #if (__SPLIT_TYPE==1)
                    	if((FL.state==DETECTION) && ((FL.s16_Estimated_Active_Press+MPRESS_5BAR)>=mpress))
                      #else
                        if((RL.state==DETECTION) && ((RL.s16_Estimated_Active_Press+MPRESS_5BAR)>=mpress))
                      #endif  
                        {
                            lclvbau1AdditionalBrkFluidP = 1;
                        }
                        if((FR.state==DETECTION) && ((FR.s16_Estimated_Active_Press+MPRESS_5BAR)>=mpress))
                        {
                            lclvbau1AdditionalBrkFluidP = 1;
                        }
                    }
                }
    
                #if (__SPLIT_TYPE==1)
                if((AV_DUMP_rl==1) || (AV_DUMP_rr==1))
                #else
                if((AV_DUMP_fl==1) || (AV_DUMP_rr==1))
                #endif
                {
                    lclvcas16LVBANeedBrkFluidCntS = L_TIME_300MS;
                }
                else
                {
                    if((ABS_LIMIT_SPEED_fl==1) && (ABS_LIMIT_SPEED_rr==1) && (lclvcas16LVBANeedBrkFluidCntS>L_TIME_30MS))
                    {
                        lclvcas16LVBANeedBrkFluidCntS = L_TIME_30MS;
                    }
                    else 
                    #if (__SPLIT_TYPE==1)
                    if((RL.state==DETECTION) && (RR.state==DETECTION) && (lclvcas16LVBANeedBrkFluidCntS>L_TIME_150MS))
                    #else
                    if((FL.state==DETECTION) && (RR.state==DETECTION) && (lclvcas16LVBANeedBrkFluidCntS>L_TIME_150MS))
                    #endif
                    {
                        lclvcas16LVBANeedBrkFluidCntS = L_TIME_150MS;
                    }
                    #if (__SPLIT_TYPE==1)
                    else if(((RL.state==DETECTION) || (RR.state==DETECTION)) && (lclvcas16LVBANeedBrkFluidCntS>0))
                    #else
                    else if(((FL.state==DETECTION) || (RR.state==DETECTION)) && (lclvcas16LVBANeedBrkFluidCntS>0))
                    #endif
                    {
                        lclvcas16LVBANeedBrkFluidCntS = lclvcas16LVBANeedBrkFluidCntS-1;
                    }
                    else { }
    
                    if(lclvcas16LVBANeedBrkFluidCntS==0)
                    {
                        #if (__SPLIT_TYPE==1)
                        if((RL.state==DETECTION) && ((RL.s16_Estimated_Active_Press+MPRESS_5BAR)>=mpress))
                        #else
                        if((FL.state==DETECTION) && ((FL.s16_Estimated_Active_Press+MPRESS_5BAR)>=mpress))
                        #endif
                        {
                            lclvbau1AdditionalBrkFluidS = 1;
                        }
                        if((RR.state==DETECTION) && ((RR.s16_Estimated_Active_Press+MPRESS_5BAR)>=mpress))
                        {
                            lclvbau1AdditionalBrkFluidS = 1;
                        }
                    }
                }
            }
        }
    }
    else
    {
        lclvbau1AdditionalBrkFluidP = 0;
        lclvbau1AdditionalBrkFluidS = 0;
    }
  /*
  #endif
  081208*/

}

static void LCLVBA_vDctMPRelease(void)
{
	int16_t lclvbas16LVBATemp;
		
    if(mpress>=MPRESS_1BAR)
    {
        if((mot_mon_ad <= MSC_3_V) || (lclvbas8LVBAMpressFastDecCnt>0))
        {
            lclvbas16LVBATemp = ((MCpress[0]-MCpress[3])/2) + (lclvbas16LVBAMpressSlop/2);
        }
        else
        {
            lclvbas16LVBATemp = ((MCpress[0]-MCpress[3])/4) + ((lclvbas16LVBAMpressSlop*3)/4);
        }

        lclvbas16LVBAMpressSlop = lclvbas16LVBATemp;

        if(lclvbas16LVBAMpressSlop > lclvbas16LVBAMpressSlopMax) 
        {
        	lclvbas16LVBAMpressSlopMax = lclvbas16LVBAMpressSlop;
        }

        if(lclvbas16LVBAMpressSlop <= -MPRESS_2_1BAR)
        {
            if(lclvbas8LVBAMpressFastDecCnt<100) 
            {
            	lclvbas8LVBAMpressFastDecCnt = lclvbas8LVBAMpressFastDecCnt+1;
            }
            
            if(lclvbas8LVBAMpressFastDecCnt>5) 
            {
            	lclvbau1LVBABrkPdlFastRelease = 1;
            }

            if(lclvbas8LVBAMpressSlowDecCnt<100) 
            {
            	lclvbas8LVBAMpressSlowDecCnt = lclvbas8LVBAMpressSlowDecCnt+1;
            }
        }
        else if(lclvbas16LVBAMpressSlop >= MPRESS_0G5BAR)
        {
            lclvbas8LVBAMpressFastDecCnt = 0;
            lclvbau1LVBABrkPdlFastRelease = 0;

            lclvbas8LVBAMpressSlowDecCnt = 0;
        }
        else
        {
            if(lclvbas8LVBAMpressFastDecCnt>0) 
            {
            	lclvbas8LVBAMpressFastDecCnt = lclvbas8LVBAMpressFastDecCnt-1;
            }

            if((lclvbas8LVBAMpressFastDecCnt==0) && (lclvbas16LVBAMpressSlop >= MPRESS_0BAR))
            {
                lclvbau1LVBABrkPdlFastRelease = 0;
            }

            if(lclvbas16LVBAMpressSlop <= -MPRESS_0G5BAR)
            {
                if(lclvbas8LVBAMpressSlowDecCnt<100) 
                {
                	lclvbas8LVBAMpressSlowDecCnt = lclvbas8LVBAMpressSlowDecCnt + 1;
                }
            }
            else if(lclvbas16LVBAMpressSlop > MPRESS_0BAR)
            {
                lclvbas8LVBAMpressSlowDecCnt = 0;
            }
            else if(lclvbas16LVBAMpressSlop >= -MPRESS_0G2BAR)
            {
                if(lclvbas8LVBAMpressSlowDecCnt>=3) 
                {
                	lclvbas8LVBAMpressSlowDecCnt = lclvbas8LVBAMpressSlowDecCnt - 3;
                }
                else 
                {
                	lclvbas8LVBAMpressSlowDecCnt = 0;
                }
            }
            else { }
        }
    }
    else if(lclvbas8LVBAMpressFastDecCnt>0)
    {
        lclvbas8LVBAMpressFastDecCnt = lclvbas8LVBAMpressFastDecCnt-1;
        
        if(lclvbas8LVBAMpressSlowDecCnt>=3) 
        {
        	lclvbas8LVBAMpressSlowDecCnt = lclvbas8LVBAMpressSlowDecCnt - 3;
        }
        else 
        {
        	lclvbas8LVBAMpressSlowDecCnt = 0;
        }
        lclvbas16LVBAMpressSlop = 0;
        lclvbas16LVBAMpressSlopMax = 0;
    }
    else
    {
        lclvbas16LVBAMpressSlop = 0;
        lclvbas16LVBAMpressSlopMax = 0;
        lclvbas8LVBAMpressFastDecCnt = 0;
        lclvbau1LVBABrkPdlFastRelease = 0;
        if(lclvbas8LVBAMpressSlowDecCnt>=3) 
        {
        	lclvbas8LVBAMpressSlowDecCnt = lclvbas8LVBAMpressSlowDecCnt - 3;
        }
        else 
        {
        	lclvbas8LVBAMpressSlowDecCnt = 0;
        }
    }

    if(lclvbas1LVBAStartFlg==1)
    {
    	lclvbas16LVBARlsMpresCnt = 0;
    }
    else if(lsespu1MpressOffsetOK==0)
    {
        if(((mpress<= MPRESS_4BAR) && (lclvbas16VacuumLevel>20)) || ((mpress<= MPRESS_2BAR) && (lclvbas16VacuumLevel<=20)))
        {
            if(lclvbas16LVBARlsMpresCnt < L_TIME_1000MS) {lclvbas16LVBARlsMpresCnt++;}
        }
        else
        {
            if(lclvbas16LVBARlsMpresCnt>=5) 
            {
            	lclvbas16LVBARlsMpresCnt = lclvbas16LVBARlsMpresCnt - 5;
            }
            else 
            {
            	lclvbas16LVBARlsMpresCnt = 0;
            }
        }
    }
    else
    {
        if(((mpress<= MPRESS_1BAR) && (lclvbas16VacuumLevel>20)) || ((mpress<= MPRESS_0G5BAR) && (lclvbas16VacuumLevel<=20)))
        {
            lclvbas16LVBARlsMpresCnt++;
        }
        else
        {
            if(lclvbas16LVBARlsMpresCnt>=5) {lclvbas16LVBARlsMpresCnt = lclvbas16LVBARlsMpresCnt - 5;}
            else {lclvbas16LVBARlsMpresCnt = 0;}
        }
    }
    
    
    if(lclvbau1BrakingEndFlg==0)
    {
    	if((lclvbas16LVBARlsMpresCnt>=L_TIME_100MS) && (BLS==0))
    	{
    		lclvbau1BrakingEndFlg = 1;
    	}
    }
    else
    {
    	if(lclvbau1ActiveFlg==1)
    	{
    		lclvbau1BrakingEndFlg = 0;
    	}
    }
}

static void LCVLBA_vDecideLVBARelealse(void)
{
    if(lclvbau1ActiveFlg==1)
    {
	    if(lclvbas16VacuumLevel>20)
	    {
	        if(vref<=VREF_0_5_KPH)
	        {
	            lclvbas16LVBAReleaseCnt++;
	        }
	        else
	        {
	            lclvbas16LVBAReleaseCnt = 0;
	        }
	    }
	    else 
	    {
	        if(vref<=VREF_0_125_KPH)
	        {
	            lclvbas16LVBAReleaseCnt++;
	        }
	        else if((vref<=VREF_1_KPH) && (lclvbas16LVBAReleaseCnt>2))
	        {
	            lclvbas16LVBAReleaseCnt = lclvbas16LVBAReleaseCnt - 2;
	        }
	        else
	        {
	            lclvbas16LVBAReleaseCnt = lclvbas16LVBAReleaseCnt - 1;	            
	        }
	    }
	    lclvbas16LVBAReleaseCnt = LCABS_s16LimitMinMax(lclvbas16LVBAReleaseCnt, 0, S16_LVBA_MAX_ACTIVATION_TIME+10);
	}
	else
	{
		lclvbas16LVBAReleaseCnt = 0;
	}
}	

static void LCLVBA_vDctMpressThres(void)
{
    int16_t lclvbas16LVBATemp, lclvbas16LVBADPforVacuum;

    if(lclvbau1LVBAExceedMpresThres==0)
    {
        if(lclvbas16VacuumLevel>=20)
        {
            if(lclvbas16VacuumLevel>=50) 
            {
            	lclvbas16LVBADPforVacuum = 0;
            }
            else 
            {
            	lclvbas16LVBADPforVacuum = (50-lclvbas16VacuumLevel)*MPRESS_1BAR;
            }

            if((lclvbas16LVBAMpressSlopMax>=MPRESS_8BAR) && (mpress>=(MPRESS_65BAR-lclvbas16LVBADPforVacuum))) /* 100bar/sec */
            {
                lclvbau1LVBAExceedMpresThres=1;
            }
            else if((lclvbas16LVBAMpressSlopMax>=MPRESS_2_1BAR) && (mpress>=(MPRESS_70BAR-lclvbas16LVBADPforVacuum))) /* 100bar/sec */
            {
                lclvbau1LVBAExceedMpresThres=1;
            }
            else if((lclvbas16LVBAMpressSlopMax>=MPRESS_1_3BAR) && (mpress>=(MPRESS_80BAR-lclvbas16LVBADPforVacuum))) /* 60bar/sec */
            {
                lclvbau1LVBAExceedMpresThres=1;
            }
            else 
            {
            	;
            }
        }
        else
        {
            if((lclvbas16LVBAMpressSlopMax>=MPRESS_0_1BAR) && (mpress>=MPRESS_1_3BAR))
            {
                lclvbau1LVBAExceedMpresThres=1;
            }
            else if(BLS_K==1)
            {
                lclvbau1LVBAExceedMpresThres=1;
            }
            else { }
        }

        /* if(mpress>=MPRESS_50BAR) {lclvbau1LVBAExceedMpresThres=1;} */
    }
    else
    {
        if(lclvbas16VacuumLevel>=20)
        {
        if((mpress<=MPRESS_10BAR) && (ABS_fz==0)) {lclvbau1LVBAExceedMpresThres=0;}
    }
        else
        {
            if((mpress<=(S16_LVBA_ENTER_PRESS_TH_VL90-MPRESS_1BAR)) && (ABS_fz==0)) {lclvbau1LVBAExceedMpresThres=0;}
        }
    }
}


static uint8_t LCLVBA_u8DecideLvbaEntrance(void)
{
	uint8_t u8LvbaEnterConditionSatisfy = 0;

    int16_t lclvbas16LVBAStartPressLimit = 0;
    

    lclvbas16LVBAStartSpeedThres = LCESP_s16IInter3Point(lclvbas16VacuumLevel,  10, S16_LVBA_ENTER_SPEED_TH_ST,
    																			30, S16_LVBA_ENTER_SPEED_TH_L,
    																			60, S16_LVBA_ENTER_SPEED_TH_H);
	
    lclvbas16LVBAStartMpresThres = LCESP_s16IInter4Point(lclvbas16VacuumLevel,	10, S16_LVBA_ENTER_PRESS_TH_VL90,    																			
        												 						20, S16_LVBA_ENTER_PRESS_TH_VL80,
        												 						30, S16_LVBA_ENTER_PRESS_TH_VL70,
        												 						40, S16_LVBA_ENTER_PRESS_TH_VL60);
	
	lclvbau8LVBAStartMpresSlop =  (uint8_t)LCESP_s16IInter2Point(vref, VREF_1_KPH, U8_LVBA_ENTER_MP_SLOPE1, VREF_10_KPH, (int8_t)U8_LVBA_ENTER_MP_SLOPE2);  
    
    lclvbas16LVBAStartPressLimit = LCESP_s16IInter3Point(vref, 	VREF_5_KPH,  MPRESS_0G5BAR, 
    															VREF_10_KPH, MPRESS_0G5BAR, 
    															VREF_15_KPH, MPRESS_1BAR);

    if(lclvbas16LVBAStartMpresThres < lclvbas16LVBAStartPressLimit) 
    {
    	lclvbas16LVBAStartMpresThres = lclvbas16LVBAStartPressLimit;
    }
    
    if(BLS==1)
    {
    	lclvbas16LVBAStartMpresThres = (lclvbas16LVBAStartMpresThres>MPRESS_0G5BAR)? lclvbas16LVBAStartMpresThres : MPRESS_0G5BAR;
    }
    else
    {
    	lclvbas16LVBAStartMpresThres = (lclvbas16LVBAStartMpresThres>MPRESS_4BAR)? lclvbas16LVBAStartMpresThres : MPRESS_4BAR;
    }

      
    if( (lclvbau1LowVacuumDctFlg == 1)
	 && (lclvbau1InhibitFlg == 0)
     && (mpress > lclvbas16LVBAStartMpresThres) && (mpress < (MPRESS_180BAR-lclvbas16LVBAStartMpresThres))
     && (vref >= lclvbas16LVBAStartSpeedThres)
     && (lclvbas16LVBAMpressSlop >= lclvbau8LVBAStartMpresSlop)
     && (lclvbau1BrakingEndFlg==1)
	 && (lclvbau1EngineStatusOkForLVBA == 1)
    )
    {   
    	u8LvbaEnterConditionSatisfy = 1;     
    }
    else 
    {
       u8LvbaEnterConditionSatisfy = 0;
    }
 
    return u8LvbaEnterConditionSatisfy;

}

static uint8_t LCLVBA_u8DecideLvbaExit(void)
{
	uint8_t u8LvbaExitConditionSatisfy;
	static uint8_t lclvbau8ExitCnt;
	
    if(lclvbau1InhibitFlg==1)
    {
    	lclvbau8ExitCnt = 0;
    	u8LvbaExitConditionSatisfy = S8_LVBA_FM_EXIT;
    }
    else if(((BLS==0) && (lclvbas16LVBARlsMpresCnt>=L_TIME_30MS))
      || (mpress>=(MPRESS_180BAR-lclvbas16LVBAStartMpresThres))                                                                                                
    )
    {
    	lclvbau8ExitCnt++;
    	if(lclvbau8ExitCnt >= U8_LVBA_EXIT_WAIT_TIME)
    	{
    		lclvbau8ExitCnt = 0;
        	u8LvbaExitConditionSatisfy = S8_LVBA_NORMAL_EXIT;
        }        
      
  #if __LVBA_AVH_COORPERATION == ENABLE  
        if(lclvbas16LVBAReleaseCnt>=S16_LVBA_MAX_ACTIVATION_TIME) 
        {
        	lclvbau1AvhActiveRequestFlg = 1;
        }
  #endif  
    }
    else if(lclvbas16LVBAReleaseCnt>=S16_LVBA_MAX_ACTIVATION_TIME)
    {
    	u8LvbaExitConditionSatisfy = S8_LVBA_MAX_TIME_EXIT;
    }
  #if __LVBA_AVH_COORPERATION == ENABLE  
    else if(lclvbau1AvhActiveFlg == 1)
    {
    	u8LvbaExitConditionSatisfy = S8_LVBA_NORMAL_EXIT;
    }
  #endif  
    else
    {
    	lclvbau8ExitCnt = 0;
        u8LvbaExitConditionSatisfy = 0;
    }    
	
	return u8LvbaExitConditionSatisfy;
}

static void LCLVBA_vDecideVlvActMode(void)
{
	if((lclvbas8ControlState == S8_LVBA_INHIBIT)||(lclvbas8ControlState == S8_LVBA_READY))
    {
        LVBA_TC_Primary_ACT = 0;
        LVBA_TC_Secondary_ACT = 0;
        LVBA_ESV_Primary_ACT = 0;
        LVBA_ESV_Secondary_ACT = 0;
        LVBA_MSC_MOTOR_ON = 0;
    }
    else if(lclvbas8ControlState == S8_LVBA_ACTUATION)
    {
	    LVBA_TC_Primary_ACT = 1;
        LVBA_TC_Secondary_ACT = 1;
        LVBA_ESV_Primary_ACT = 1;
        LVBA_ESV_Secondary_ACT = 1;
        LVBA_MSC_MOTOR_ON = lclvbau1ActiveFlg;
         
        if((lclvbau1AdditionalBrkFluidP==0) && (vref>=VREF_3_KPH))
        {
            LVBA_ESV_Primary_ACT = 0;
            /*LVBA_TC_Primary_ACT = 0;*/
        }
        else
        {
        	LVBA_ESV_Primary_ACT = 1;
        	/*LVBA_TC_Primary_ACT = 1;*/
        }
        
        if((lclvbau1AdditionalBrkFluidS==0) && (vref>=VREF_3_KPH))
        {
            LVBA_ESV_Secondary_ACT = 0;
            /*LVBA_TC_Secondary_ACT = 0;*/
        }
        else{
            LVBA_ESV_Secondary_ACT = 1;
            /*LVBA_TC_Secondary_ACT = 1;*/
        }

       /* if((lclvbau8AbsInLvbaCnt<=35) && ((vref>=VREF_0_125_KPH)||(lclvbau8LVBAOnCnt<50)))
        {
            LVBA_ESV_Primary_ACT = 1;
            LVBA_ESV_Secondary_ACT = 1;
        }

        if((lclvbau1AdditionalBrkFluidP==1) && (vref>=VREF_3_KPH))
        {
            LVBA_ESV_Primary_ACT = 1;
        }
        if((lclvbau1AdditionalBrkFluidS==1) && (vref>=VREF_3_KPH))
        {
            LVBA_ESV_Secondary_ACT = 1;
        }*/
    }
    else if(lclvbas8ControlState == S8_LVBA_FADEOUT)
    {
    	if(lclvbas16LVBAReleaseCnt>=S16_LVBA_MAX_ACTIVATION_TIME)/*max time out fade out mode*/
    	{
	        LVBA_TC_Primary_ACT = 1;
	        LVBA_TC_Secondary_ACT = 1;
	        LVBA_ESV_Primary_ACT = 1;
	        LVBA_ESV_Secondary_ACT = 1;
	        LVBA_MSC_MOTOR_ON = 0;    		
	        lclvbas16FadeOutTcCurrent = (int16_t)U8_TC_CUR_FADE_OUT_RATIO_1;/*decide proper TC current ratio*/
	        if(lclvbas16LVBAReleaseCnt >= (S16_LVBA_MAX_ACTIVATION_TIME+S16_LVBA_MAX_ACTIVATION_TIME))
	        {
	        	lclvbau1FadeOutEndFlg = 1;
	        }
    	}
    	else if(lclvbau1LVBABrkPdlFastRelease == 1)/*brake pedal fast*/
    	{
	        LVBA_TC_Primary_ACT = 1;
	        LVBA_TC_Secondary_ACT = 1;
	        LVBA_ESV_Primary_ACT = 1;
	        LVBA_ESV_Secondary_ACT = 1;
	        LVBA_MSC_MOTOR_ON = 0;    	
	        lclvbas16FadeOutTcCurrent = (int16_t)U8_TC_CUR_FADE_OUT_RATIO_2;	
    	}
    	else
    	{
	        LVBA_TC_Primary_ACT = 1;
	        LVBA_TC_Secondary_ACT = 1;
	        LVBA_ESV_Primary_ACT = 0;
	        LVBA_ESV_Secondary_ACT = 0;
	        LVBA_MSC_MOTOR_ON = 0;    
	        lclvbas16FadeOutTcCurrent = (int16_t)U8_TC_CUR_FADE_OUT_RATIO_3;		
    	}
    	
        if((lclvbau1AdditionalBrkFluidP==1) && (vref>=VREF_3_KPH))
        {
            LVBA_ESV_Primary_ACT = 1;
        }
        else
        {
        	LVBA_ESV_Primary_ACT = 0;
        }
        
        if((lclvbau1AdditionalBrkFluidS==1) && (vref>=VREF_3_KPH))
        {
            LVBA_ESV_Secondary_ACT = 1;
        }
        else{
            LVBA_ESV_Secondary_ACT = 0;
        }
    }
    else 
    {
        LVBA_TC_Primary_ACT = 0;
        LVBA_TC_Secondary_ACT = 0;
        LVBA_ESV_Primary_ACT = 0;
        LVBA_ESV_Secondary_ACT = 0;
        LVBA_MSC_MOTOR_ON = 0;   	
    } 
    
    if(ABS_fz==1) 
    {
        if(lclvbau8AbsInLvbaCnt<=200) {lclvbau8AbsInLvbaCnt++;}
        else { }
        lclvbau8AbsInLvbaCnt = (uint8_t)LCABS_s16LimitMinMax((int16_t)lclvbau8AbsInLvbaCnt, 0, 200);
    }
    else if(lclvbau8AbsInLvbaCnt>0) 
    {
    	lclvbau8AbsInLvbaCnt = 100;
    }
    else 
    {
    	lclvbau8AbsInLvbaCnt = 0;
    }
    
    if(lclvbau1ActiveFlg==1)
    {
		if(lclvbau8LVBAOnCnt<200)
        {
            lclvbau8LVBAOnCnt = lclvbau8LVBAOnCnt + 1;
        }
        else
        {
            lclvbau8LVBAOnCnt = 200;
        }
        lclvbau8LVBAOffCnt = 0;
    }
    else
    {
    	if(lclvbas1LVBAEndFlg==1)
    	{
    		lclvbau8LVBAOffCnt = 1;
    	}
		else if((lclvbau8LVBAOffCnt>0) && (lclvbau8LVBAOffCnt<100))
        {
            lclvbau8LVBAOffCnt = lclvbau8LVBAOffCnt + 1;
        }
        else
        {
           lclvbau8LVBAOffCnt = 100;
        }
        lclvbau8LVBAOnCnt = 0;
    }

}

void LCLVBA_vDctOtherCtrlFuncs(void)
{
    lclvbas8OtherCtrlEnable = 0;
    if(PBA_ON==1) {lclvbas8OtherCtrlEnable = 1;}
      #if __FBC
    if(FBC_ON==1) {lclvbas8OtherCtrlEnable = 2;}
      #endif
      #if __TSP
    if(TSP_ON==1) {lclvbas8OtherCtrlEnable = 3;}
      #endif
      #if __ACC
    if(lcu1AccActiveFlg==1) {lclvbas8OtherCtrlEnable = 4;}
      #endif
      #if __HDC
    if(lcu1HdcActiveFlg==1) {lclvbas8OtherCtrlEnable = 5;}
      #endif
      #if __EPB_INTERFACE
    if(lcu1EpbActiveFlg==1) {lclvbas8OtherCtrlEnable = 6;}
      #endif
      #if __HSA
    if(HSA_flg==1) {lclvbas8OtherCtrlEnable = 7;}
      #endif
}

void LCLVBA_vCallVariableReset(void)
{
	/*LVBA_MSC_DEBUGGER=12;*/
	lclvbau1InhibitFlg = 0;
	lclvbas8ControlState = S8_LVBA_READY;
	lclvbas16LVBAReleaseCnt = 0;
	lclvbau1ActiveFlg = 0;
	lclvbau8LVBAStopApply = 0;
	lclvbau8LVBAOnCnt = 0;
	lclvbau8AbsInLvbaCnt = 0;
}

/******************************************************************************
* FUNCTION NAME:      LCLVBA_vCallControl
* CALLED BY:          LC_vCallMainControl()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Main Function of LVBA Control
******************************************************************************/

void LCLVBA_vCallControl(void)
{
	uint8_t u8LvbaEnterSatisfy, u8LvbaExitSatisfy;
	
    LCLVBA_vCheckEngineStatus();
    
	LCLVBA_vDctOtherCtrlFuncs();
	
	LCLVBA_vDecideLVBAInhibition();
	
  #if (__VACUUM_SENSOR_ENABLE || defined(VAC_SENSOR_INSTALLED))
    LCLVBA_vCallCheckVacuumPress();/*need to move to LD function files..*/
    LCLVBA_vCallDecideVacuumLevel();
  #endif

 /* #if defined(PEDAL_SENSOR_INSTALLED)
    LCLVBA_vCalcBrakePedalTravel();
  #endif  */
    
	LCLVBA_vCalcAvgMCPressure();
	LCLVBA_vDctMPRelease();/*including MPress slop*/
	LCLVBA_vDctMpressThres();
	LCLVBA_vDecideAssistPressure();
	
	lclvbas1LVBAStartFlg = 0;
	lclvbas1LVBAEndFlg = 0;

	switch (lclvbas8ControlState)
	{
		/*-------------------------------------------------------------------------- */
		case S8_LVBA_READY:						/*  */
			if(lclvbau1InhibitFlg == 1)  
			{			
				lclvbas8ControlState = S8_LVBA_INHIBIT;
			}
			else 
			{
				u8LvbaEnterSatisfy = LCLVBA_u8DecideLvbaEntrance();
				/*LVBA_MSC_DEBUGGER=15;*/
				if(u8LvbaEnterSatisfy==1) 
				{
					lclvbas8ControlState = S8_LVBA_ACTUATION;
			        lclvbau1ActiveFlg = 1; /*used to set LVBA_MSC_MOTOR_ON --> MSControl*/
			        lclvbas1LVBAStartFlg=1; 
			        lclvbas16LvbaEnterSpeed = vref;  			        
			        
			        if (lclvbas16LvbaEnterSpeed <= VREF_0_125_KPH)
			        {
			        	lclvbau8LVBAStopApply = 1;
			        }
				}
			}
			break;
		/*-------------------------------------------------------------------------- */
		case S8_LVBA_ACTUATION:
								
			LCVLBA_vDecideLVBARelealse();
			u8LvbaExitSatisfy = LCLVBA_u8DecideLvbaExit();
			
			if(u8LvbaExitSatisfy == S8_LVBA_NORMAL_EXIT) 
			{
				lclvbas8ControlState = S8_LVBA_READY;
				lclvbas1LVBAEndFlg=1;
				LCLVBA_vCallVariableReset();
			}
			else if(u8LvbaExitSatisfy == S8_LVBA_FM_EXIT)  
			{			
				lclvbas8ControlState = S8_LVBA_FADEOUT;
			}
			else if(u8LvbaExitSatisfy == S8_LVBA_MAX_TIME_EXIT)
			{
				lclvbas8ControlState = S8_LVBA_FADEOUT;
			}
			else if(lclvbau1LVBABrkPdlFastRelease == 1)
			{
				lclvbas8ControlState = S8_LVBA_FADEOUT;
			}
			else
			{
				LCLVBA_vDecideAssistPressChange();							
				LCLVBA_vDecideAdditionalBrkFluid();											
			}			
			break;
		/*-------------------------------------------------------------------------- */
		case S8_LVBA_FADEOUT:						/*  */
			u8LvbaExitSatisfy = LCLVBA_u8DecideLvbaExit();

			if(u8LvbaExitSatisfy == S8_LVBA_NORMAL_EXIT) 
			{
				lclvbas8ControlState = S8_LVBA_READY;
				lclvbas1LVBAEndFlg=1;
				LCLVBA_vCallVariableReset();
			}
			else { }
			
			if(lclvbau1FadeOutEndFlg==1)
			{			
				if(lclvbau1InhibitFlg == 1)
				{
					lclvbas8ControlState = S8_LVBA_INHIBIT;
				}
				else
				{
					lclvbas8ControlState = S8_LVBA_READY;
					lclvbas1LVBAEndFlg=1;
					LCLVBA_vCallVariableReset();
				}
			}
			
			else { }
			
			break;
		/*-------------------------------------------------------------------------- */
		case S8_LVBA_INHIBIT:
			if(lclvbau1InhibitFlg == 0) 
			{
				lclvbas8ControlState = S8_LVBA_READY;
				LCLVBA_vCallVariableReset();
			}
			else 
			{
				/*LCLVBA_vCallVariableReset();*/ 
				/*Control inhibition*/
			}
			break;	

		/*-------------------------------------------------------------------------- */
		default:
			if(lclvbau1InhibitFlg == 0) 
			{
				lclvbas8ControlState = S8_LVBA_READY;
				LCLVBA_vCallVariableReset();
				/*LVBA_MSC_DEBUGGER = 13;*/
			}
			else { }			
			break;	
	}
	
	LCLVBA_vDecideVlvActMode();
/*	if      (lclvbas8ControlState == S8_LVBA_READY)     { }
	else if (lclvbas8ControlState == S8_LVBA_ACTUATION) { }
	else if (lclvbas8ControlState == S8_LVBA_FADEOUT)   { }
	else if (lclvbas8ControlState == S8_LVBA_INHIBIT)   { }
	else                                                { }
*/
}

static void LCLVBA_vDecideAssistPressChange(void)
{
    int16_t lclvbas16EstWheelPressAvg;
    int16_t lclvbas16LVBATemp;

      #if (__CAR==GM_TAHOE)|| (__CAR ==GM_SILVERADO) && (__SPLIT_TYPE==1)
    lclvbas16EstWheelPressAvg = (FL.s16_Estimated_Active_Press+FR.s16_Estimated_Active_Press+RL.s16_Estimated_Active_Press+RR.s16_Estimated_Active_Press)/4;
      #else
    lclvbas16EstWheelPressAvg = (FL.s16_Estimated_Active_Press+FR.s16_Estimated_Active_Press)/2;
      #endif

    if(LVBA_EXCEED_MPRESS_5BAR==0)
    {
        if(mpress >= MPRESS_5BAR)
        {
            LVBA_EXCEED_MPRESS_5BAR = 1;
        }
    }
    else
    {
        if(mpress <= MPRESS_4BAR)
        {
            LVBA_EXCEED_MPRESS_5BAR = 0;
        }
    }

    if(lclvbau8LVBAMPSlopCnt==13)
    {
        lclvbau8LVBAMPSlopCnt = 0;
    }
    else
    {
        lclvbau8LVBAMPSlopCnt++;
    }

    if((mot_mon_ad <= MSC_3_V) || (lclvbas8LVBAMpressFastDecCnt>0))
    {
        lclvbas16LVBATemp = (int16_t)((int32_t)(((mpress-lclvbas16LVBAMpressBuffer[lclvbau8LVBAMPSlopCnt])*4) + lclvbas16LVBAMpressSlop100MS)/2);
    }
    else
    {
        lclvbas16LVBATemp = (int16_t)((int32_t)(((mpress-lclvbas16LVBAMpressBuffer[lclvbau8LVBAMPSlopCnt])*4) + (lclvbas16LVBAMpressSlop100MS*3))/4);
    }

    lclvbas16LVBAMpressBuffer[lclvbau8LVBAMPSlopCnt] = mpress;

    if(mpress > MPRESS_0BAR)
    {
        if((lclvbas16LVBATemp>0) && (lclvbas16LVBAMpressSlop100MS<=0))
        {
            lclvbas16LVBAMpSlopSumPos = lclvbas16LVBATemp;
        }
        else if(lclvbas16LVBATemp>0)
        {
            lclvbas16LVBAMpSlopSumPos = lclvbas16LVBAMpSlopSumPos + lclvbas16LVBATemp;
            if((lclvbas16LVBAMpSlopSumPos>30000) || (lclvbas16LVBAMpSlopSumPos<0))
            {
                lclvbas16LVBAMpSlopSumPos = 30000;
            }
        }
        else { }

        if((lclvbas16LVBATemp<0) && (lclvbas16LVBAMpSlopSumNeg==0))
        {
            lclvbas16LVBAMpSlopSumNeg = lclvbas16LVBATemp;
        }
        else if(lclvbas16LVBATemp<0)
        {
            lclvbas16LVBAMpSlopSumNeg = lclvbas16LVBAMpSlopSumNeg + lclvbas16LVBATemp;
            if((lclvbas16LVBAMpSlopSumPos<-30000) || (lclvbas16LVBAMpSlopSumPos>0))
            {
                lclvbas16LVBAMpSlopSumPos = -30000;
            }
        }
        else
        {
            if((lclvbas16LVBAMpSlopSumNeg<0) && ((lclvbas16LVBAMpSlopSumNeg+lclvbas16LVBATemp)<0))
            {
                lclvbas16LVBAMpSlopSumNeg = lclvbas16LVBAMpSlopSumNeg + lclvbas16LVBATemp;
            }
            else {lclvbas16LVBAMpSlopSumNeg = 0;}
        }
        lclvbau8MPReleaseCnt = 1;
    }
    else if((lclvbau8MPReleaseCnt>=10) || (BLS==0))
    {
        lclvbas16LVBAMpSlopSumPos = 0;
        lclvbas16LVBAMpSlopSumNeg = 0;
        lclvbau8MPReleaseCnt = 0;
    }
    else
    {
        if((lclvbau8MPReleaseCnt>0) && (lclvbau8MPReleaseCnt<142)) {lclvbau8MPReleaseCnt++;}
    }

    lclvbas16LVBAMpressSlop100MS = lclvbas16LVBATemp;


    if(lclvbau1ActiveFlg==0)
    {
        LVBA_BOOST_NEED_flg = 0;
        lclvbau8LVBABoostOnCnt = 0; lclvbau8LVBABoostOffCnt = 200;
    }
    else if((LVBA_BOOST_NEED_flg == 0) && (lclvbau8LVBABoostOffCnt>30) && ((lclvbas16LVBAMpSlopSumNeg+lclvbas16LVBAMpSlopSumPos)>-100))
    {
        if(lclvbas1LVBAStartFlg==1)
        {
            LVBA_BOOST_NEED_flg = 1;
        }
        else if(mpress <= MPRESS_1BAR)
        {
            if(lclvbas16EstWheelPressAvg < MPRESS_10BAR)
            {
                if(lclvbas16LVBAMpressSlop100MS >= 5)
                {
                    LVBA_BOOST_NEED_flg = 1;
                }
            }
            else
            {
                if(lclvbau8LVBABoostOffCnt > 100) {tempW7 = 1;}
                else if(lclvbau8LVBABoostOffCnt > 56) {tempW7 = 2;}
                else if(lclvbau8LVBABoostOffCnt > 28) {tempW7 = 3;}
                else if(lclvbau8LVBABoostOffCnt > 14) {tempW7 = 4;}
                else {tempW7 = 10;}

                if(lclvbas16LVBAMpressSlop100MS >= (tempW7*5))
                {
                    LVBA_BOOST_NEED_flg = 1;
                }
            }
        }
        else if(LVBA_EXCEED_MPRESS_5BAR==0)
        {
            if(lclvbau8LVBABoostOffCnt > 100) {tempW7 = 1;}
            else if(lclvbau8LVBABoostOffCnt > 56) {tempW7 = 2;}
            else if(lclvbau8LVBABoostOffCnt > 28) {tempW7 = 3;}
            else if(lclvbau8LVBABoostOffCnt > 14) {tempW7 = 5;}
            else {tempW7 = 10;}

            if(vref<=VREF_10_KPH)
            {
                if(lclvbas16EstWheelPressAvg < MPRESS_10BAR)
                {
                    if(lclvbas16LVBAMpressSlop100MS >= 10)
                    {
                        LVBA_BOOST_NEED_flg = 1;
                    }
                }
                else if(lclvbas16LVBAMpressSlop100MS >= (tempW7*10))
                {
                    LVBA_BOOST_NEED_flg = 1;
                }
                else { }
            }
            else
            {
                if(lclvbas16EstWheelPressAvg<(lclvbas16LVBATargetMpress/2))
                {
                    if(lclvbas16LVBAMpressSlop100MS >= (tempW7*10))
                    {
                        LVBA_BOOST_NEED_flg = 1;
                    }
                }
                else if(lclvbas16EstWheelPressAvg<((lclvbas16LVBATargetMpress*4)/5))
                {
                    if(lclvbas16LVBAMpressSlop100MS >= (tempW7*20))
                    {
                        LVBA_BOOST_NEED_flg = 1;
                    }
                }
                else
                {
                    if(lclvbas16LVBAMpressSlop100MS >= (((lclvbas16EstWheelPressAvg/MPRESS_30BAR)+1)*tempW7*15))
                    {
                        LVBA_BOOST_NEED_flg = 1;
                    }
                }
            }
        }
        else
        {
            if(lclvbau8LVBABoostOffCnt > 100) {tempW7 = 1;}
            else if(lclvbau8LVBABoostOffCnt > 56) {tempW7 = 2;}
            else if(lclvbau8LVBABoostOffCnt > 28) {tempW7 = 4;}
            else if(lclvbau8LVBABoostOffCnt > 14) {tempW7 = 6;}
            else {tempW7 = 15;}

            if(vref<=VREF_10_KPH)
            {
                if(lclvbas16LVBAMpressSlop100MS >= (tempW7*15))
                {
                    LVBA_BOOST_NEED_flg = 1;
                }
                else { }
            }
            else
            {
                if(lclvbas16EstWheelPressAvg<(lclvbas16LVBATargetMpress/2))
                {
                    if(lclvbas16LVBAMpressSlop100MS >= (tempW7*10))
                    {
                        LVBA_BOOST_NEED_flg = 1;
                    }
                }
                else if(lclvbas16EstWheelPressAvg<((lclvbas16LVBATargetMpress*4)/5))
                {
                    if(lclvbas16LVBAMpressSlop100MS >= (tempW7*15))
                    {
                        LVBA_BOOST_NEED_flg = 1;
                    }
                }
                else
                {
                    if(lclvbas16LVBAMpressSlop100MS >= (((lclvbas16EstWheelPressAvg/MPRESS_30BAR)+1)*tempW7*20))
                    {
                        LVBA_BOOST_NEED_flg = 1;
                    }
                }
            }
        }

        if(LVBA_BOOST_NEED_flg==1) {lclvbau8LVBABoostOnCnt = 1; lclvbau8LVBABoostOffCnt = 0;}
        else {lclvbau8LVBABoostOnCnt = 0; lclvbau8LVBABoostOffCnt++;}

    }
    else if(LVBA_BOOST_NEED_flg == 1)
    {
      /*
      #if (__CAR==GM_TAHOE) && (__SPLIT_TYPE==1)
        if(lclvbas16EstWheelPressAvg>=(lclvbas16LVBATargetMpress))
        {
            if(((mpress>=MPRESS_2BAR) || (lclvbas16LVBAMpressSlop100MS < 0)) && (lclvbau8LVBABoostOnCnt>=10))
            {
                LVBA_BOOST_NEED_flg = 0;
            }
        }
        else if(lclvbas16EstWheelPressAvg>=(lclvbas16LVBATargetMpress*4/5))
        {
            if((lclvbas16LVBAMpressSlop100MS <= 5) && (lclvbau8LVBABoostOnCnt>=20))
            {
                LVBA_BOOST_NEED_flg = 0;
            }
        }
        else if(lclvbas16EstWheelPressAvg>=(lclvbas16LVBATargetMpress/2))
        {
            if((lclvbas16LVBAMpressSlop100MS <= 3) && (lclvbau8LVBABoostOnCnt>=30))
            {
                LVBA_BOOST_NEED_flg = 0;
            }
        }
        else
        {
            if(lclvbau8LVBABoostOnCnt>=60)
            {
                if(lclvbas16LVBAMpressSlop100MS <= 1)
                {
                    LVBA_BOOST_NEED_flg = 0;
                }
            }
            else
            {
                if(lclvbas16LVBAMpressSlop100MS < -1)
                {
                    LVBA_BOOST_NEED_flg = 0;
                }
            }
        }
      #else
      081208*/
        if(lclvbas16EstWheelPressAvg>=(lclvbas16LVBATargetMpress))
        {
            if(lclvbas16EstWheelPressAvg>=MPRESS_10BAR)
            {
                if(((mpress>=MPRESS_5BAR) || (lclvbas16LVBAMpressSlop100MS < 0)) && (lclvbau8LVBABoostOnCnt>=10))
                {
                    LVBA_BOOST_NEED_flg = 0;
                }
            }
            else
            {
                if(((mpress>=MPRESS_5BAR) || (BLS == 0) || (lclvbas16LVBAMpressSlop100MS < -100)) && (lclvbau8LVBABoostOnCnt>=10))
                {
                    LVBA_BOOST_NEED_flg = 0;
                }
            }
        }
        else if((lclvbas16EstWheelPressAvg>=((lclvbas16LVBATargetMpress*4)/5)) && (lclvbas16EstWheelPressAvg>=MPRESS_20BAR))
        {
            if((lclvbas16LVBAMpressSlop100MS <= 5) && (lclvbau8LVBABoostOnCnt>=20))
            {
                LVBA_BOOST_NEED_flg = 0;
            }
        }
        else if((lclvbas16EstWheelPressAvg>=((lclvbas16LVBATargetMpress*2)/3)) && (lclvbas16EstWheelPressAvg>=MPRESS_30BAR))
        {
            if((lclvbas16LVBAMpressSlop100MS <= 3) && (lclvbau8LVBABoostOnCnt>=40))
            {
                LVBA_BOOST_NEED_flg = 0;
            }
        }
        else
        {
            if((lclvbas16EstWheelPressAvg>=(lclvbas16LVBATargetMpress/2)) && (lclvbas16EstWheelPressAvg>=MPRESS_40BAR))
            {
                if((lclvbas16LVBAMpressSlop100MS <= 1) && (lclvbau8LVBABoostOnCnt>=60))
                {
                    LVBA_BOOST_NEED_flg = 0;
                }
            }
            else if(BLS_PIN==0) {LVBA_BOOST_NEED_flg = 0;}
            else { }
        }
      /*
      #endif
      081208*/

        if(LVBA_BOOST_NEED_flg==1) {lclvbau8LVBABoostOnCnt++; lclvbau8LVBABoostOffCnt = 0;}
        else {lclvbau8LVBABoostOnCnt = 0; lclvbau8LVBABoostOffCnt = 1;}

    }
    else
    {
        lclvbau8LVBABoostOnCnt = 0; lclvbau8LVBABoostOffCnt++;
    }

    if(lclvbau1ActiveFlg==0)
    {
        LVBA_PRESS_DECREASE_NEED_flg = 0;
        lclvbau8LVBAPressDecCnt = 0;
        lclvbau8LVBAPresDecAftTimer = 200;
    }
    else if(LVBA_PRESS_DECREASE_NEED_flg==0)
    {
      /*
      #if (__CAR==GM_TAHOE) && (__SPLIT_TYPE==1)
        if((lclvbau8LVBAPresDecAftTimer>=170) || (lclvbau8LVBABoostOffCnt<=30))
        {
            if(LVBA_EXCEED_MPRESS_5BAR==0)
            {
                if((lclvbas16LVBAMpressSlop100MS <= -100) && (lclvbas16EstWheelPressAvg>lclvbas16LVBATargetMpress))
                {
                    LVBA_PRESS_DECREASE_NEED_flg = 1;
                }
            }
            else
            {
                if((lclvbas16LVBAMpressSlop100MS <= -120) && (lclvbas16EstWheelPressAvg>lclvbas16LVBATargetMpress))
                {
                    LVBA_PRESS_DECREASE_NEED_flg = 1;
                }
            }
        }
        else
        {
            if(LVBA_EXCEED_MPRESS_5BAR==0)
            {
                if(vref<=VREF_1_KPH)
                {
                    if((lclvbas16LVBAMpressSlop100MS <= -100) || ((lclvbas16LVBAMpSlopSumNeg<=-2000) && (lclvbas16LVBAMpressSlop100MS <= -30) && (mpress<MPRESS_3BAR)))
                    {
                        if(lclvbas16EstWheelPressAvg>(lclvbas16LVBATargetMpress*2/3))
                        {
                            LVBA_PRESS_DECREASE_NEED_flg = 1;
                        }
                    }
                }
                else
                {
                    if((lclvbas16LVBAMpressSlop100MS <= -80) || ((lclvbas16LVBAMpSlopSumNeg<=-800) && (lclvbas16LVBAMpressSlop100MS <= -20)))
                    {
                        if(lclvbas16EstWheelPressAvg>(lclvbas16LVBATargetMpress*2/3))
                        {
                            LVBA_PRESS_DECREASE_NEED_flg = 1;
                        }
                    }
                }
            }
            else
            {
                if(vref<=VREF_0_5_KPH)
                {
                    if((lclvbas16LVBAMpressSlop100MS <= -150) && (lclvbas16EstWheelPressAvg>(lclvbas16LVBATargetMpress*4/5)))
                    {
                        LVBA_PRESS_DECREASE_NEED_flg = 1;
                    }
                    else if((lclvbas16LVBAMpressSlop100MS <= -120) || ((lclvbas16LVBAMpSlopSumNeg<=-2500) && (lclvbas16LVBAMpressSlop100MS <= -100)))
                    {
                        if(lclvbas16EstWheelPressAvg>lclvbas16LVBATargetMpress)
                        {
                            LVBA_PRESS_DECREASE_NEED_flg = 1;
                        }
                    }
                    else { }
                }
                else
                {
                    if((lclvbas16LVBAMpressSlop100MS <= -60) && (lclvbas16EstWheelPressAvg>(lclvbas16LVBATargetMpress*4/5)))
                    {
                        LVBA_PRESS_DECREASE_NEED_flg = 1;
                    }
                    else if((lclvbas16LVBAMpressSlop100MS <= -30) || ((lclvbas16LVBAMpSlopSumNeg<=-800) && (lclvbas16LVBAMpressSlop100MS <= -20)))
                    {
                        if(lclvbas16EstWheelPressAvg>lclvbas16LVBATargetMpress)
                        {
                            LVBA_PRESS_DECREASE_NEED_flg = 1;
                        }
                    }
                    else { }
                }
            }
        }
      #elif (__CAR == HMC_NF)
        if(LVBA_EXCEED_MPRESS_5BAR==0)
        {
            if(vref<=VREF_1_KPH)
            {
                tempW0 = (((mpress*4)>400)?400:(((mpress*4)<200)?200:(mpress*4)));
                if((lclvbas16LVBAMpressSlop100MS <= -(tempW0*4/5)) || ((lclvbas16LVBAMpSlopSumNeg<=-(tempW0*6)) && (lclvbas16LVBAMpressSlop100MS <= -(tempW0*3/5))))
                {
                    if(lclvbas16EstWheelPressAvg>(lclvbas16LVBATargetMpress*2/3))
                    {
                        LVBA_PRESS_DECREASE_NEED_flg = 1;
                    }
                }
            }
            else
            {
                tempW0 = (((mpress*3)>200)?200:(((mpress*3)<60)?60:(mpress*3)));
                if((lclvbas16LVBAMpressSlop100MS <= -(tempW0*4/5)) || ((lclvbas16LVBAMpSlopSumNeg<=-(tempW0*6)) && (lclvbas16LVBAMpressSlop100MS <= -(tempW0*3/5))))
                {
                    if(lclvbas16EstWheelPressAvg>(lclvbas16LVBATargetMpress*2/3))
                    {
                        LVBA_PRESS_DECREASE_NEED_flg = 1;
                    }
                }
            }
        }
        else
        {
            if(vref<=VREF_0_5_KPH)
            {
                tempW0 = (((mpress*5)>500)?500:(((mpress*5)<200)?200:(mpress*5)));

                if((lclvbas16LVBAMpressSlop100MS <= -tempW0) && (lclvbas16EstWheelPressAvg>(lclvbas16LVBATargetMpress*4/5)))
                {
                    LVBA_PRESS_DECREASE_NEED_flg = 1;
                }
                else if((lclvbas16LVBAMpressSlop100MS <= -(tempW0*4/5)) || ((lclvbas16LVBAMpSlopSumNeg<=-(tempW0*6)) && (lclvbas16LVBAMpressSlop100MS <= -(tempW0*3/5))))
                {
                    if(lclvbas16EstWheelPressAvg>lclvbas16LVBATargetMpress)
                    {
                        LVBA_PRESS_DECREASE_NEED_flg = 1;
                    }
                }
                else { }
            }
            else
            {
                tempW0 = (((mpress*3)>200)?200:(((mpress*3)<60)?60:(mpress*3)));
                if((lclvbas16LVBAMpressSlop100MS <= -tempW0) && (lclvbas16EstWheelPressAvg>(lclvbas16LVBATargetMpress*4/5)))
                {
                    LVBA_PRESS_DECREASE_NEED_flg = 1;
                }
                else if((lclvbas16LVBAMpressSlop100MS <= -(tempW0*4/5)) || ((lclvbas16LVBAMpSlopSumNeg<=-(tempW0*6)) && (lclvbas16LVBAMpressSlop100MS <= -(tempW0*3/5))))
                {
                    if(lclvbas16EstWheelPressAvg>lclvbas16LVBATargetMpress)
                    {
                        LVBA_PRESS_DECREASE_NEED_flg = 1;
                    }
                }
                else { }
            }
        }
      #endif
      081208*/

        if(LVBA_PRESS_DECREASE_NEED_flg == 1)
        {
            lclvbau8LVBAPressDecCnt = 1; lclvbau8LVBAPresDecAftTimer = 200;
        }
        else
        {
            if(lclvbau8LVBAPresDecAftTimer>0) {lclvbau8LVBAPresDecAftTimer = lclvbau8LVBAPresDecAftTimer - 1;}
        }
    }
    else
    {
        if(LVBA_EXCEED_MPRESS_5BAR==0)
        {
            if(lclvbas16EstWheelPressAvg<=(lclvbas16LVBATargetMpress/2))
            {
                LVBA_PRESS_DECREASE_NEED_flg = 0;
            }
            else if(lclvbas16LVBAMpressSlop100MS >= 0)
            {
                LVBA_PRESS_DECREASE_NEED_flg = 0;
            }
            else { }
        }
        else
        {
            if(lclvbas16EstWheelPressAvg<=((lclvbas16LVBATargetMpress*2)/3))
            {
                LVBA_PRESS_DECREASE_NEED_flg = 0;
            }
            else if(lclvbas16LVBAMpressSlop100MS >= 0)
            {
                LVBA_PRESS_DECREASE_NEED_flg = 0;
            }
            else { }
        }

        if(LVBA_PRESS_DECREASE_NEED_flg == 1)
        {
            lclvbau8LVBAPressDecCnt++;
        }
        else
        {
            lclvbau8LVBAPressDecCnt = 0;
            if(lclvbau8LVBAPresDecAftTimer>0) {lclvbau8LVBAPresDecAftTimer = lclvbau8LVBAPresDecAftTimer - 1;}
        }
    }
}
  #endif
