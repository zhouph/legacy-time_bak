#include "Abc_Ctrl.h"

U8_BIT_STRUCT_t TSPF0;
U8_BIT_STRUCT_t TSPF1;
U8_BIT_STRUCT_t TSPF2;
U8_BIT_STRUCT_t TSPF3;

int16_t 	tsp_tempW0, tsp_tempW1, tsp_tempW2, tsp_tempW3, tsp_tempW4; 
int16_t 	tsp_tempW5, tsp_tempW6, tsp_tempW7, tsp_tempW8, tsp_tempW9;
         
int16_t 	tsp_yaw_threshold, tsp_yawc_thr;   
int16_t 	tsp_dyaw_add_thr, tsp_dyaw_add_LS_thr, tsp_dyaw_add_HS_thr, tsp_dyaw_add_CS_thr;
int16_t 	tsp_yawc_add_thr, tsp_yawc_add_LS_thr, tsp_yawc_add_HS_thr, tsp_yawc_add_CS_thr;
         
int16_t 	tsp_delta_yaw_first;                    
int16_t 	tsp_delta_yaw_lf_old, tsp_delta_yaw_lf; 
int16_t 	tsp_delta_yaw_dot;
         
int16_t 	tsp_yawc_first;
int16_t 	tsp_yawc_first_old, tsp_yawc_first_old2, tsp_yawc_first_old3; 
int16_t 	tsp_yawc_dot;

int16_t		tsp_initial_speed;                
uint8_t 	ldtspu8TrlrHtchSwAtv;
uint8_t 	ldtspu8DetectOscilCycle;
int16_t 	lds16TspFreqMaxTime;

int16_t 	lau16tspFirstDiffEnterTime;
         
uint8_t  	tsp_detect_oscil_count;
         
uint16_t	tsp_dyaw_count, tsp_dyaw_count_old, tsp_dyaw_count_old2;
uint16_t	tsp_plus_dyaw_peak_count, tsp_minus_dyaw_peak_count;
uint16_t	tsp_freq_rate_2nd_3rd, tsp_freq_rate_3rd_4th;

uint16_t 	tsp_max_amplitude_1st, tsp_max_amplitude_3rd;
uint16_t 	tsp_max_amplitude_2nd, tsp_max_amplitude_4th;
uint16_t 	tsp_max_amplitude, tsp_max_amplitude_old;
uint16_t 	tsp_yaw_threshold_max_amp, tsp_yaw_threshold_max_amp_old;
int16_t 	tsp_delta_max_amplitude_old;
int16_t 	tsp_amplitude_rate_1st_3rd;
int16_t 	tsp_amplitude_rate_2nd_4th;
int16_t 	tsp_amp_damping_ratio_1st_3rd;
int16_t 	tsp_amp_damping_ratio_2nd_4th;
         
uint16_t 	tsp_yawc_count;

int16_t		tsp_osci_mitig_count;
int16_t		tsp_inhibit_work_count;
uint16_t	tsp_brake_work_count;

int16_t 	lcs16TspTargetDecel;
int16_t 	lcs16TspTargetDecelAmp;
int16_t 	lcs16TspTargetDecelSpeedGain;
int16_t 	lcs16TspTargetDecelInit;
int16_t 	lcs16TspTargetDecelReal;
int16_t		lcs16tspTargetDecelMonitor;
int16_t		lcs16TspDecelSpeed;
int16_t		lcs16TspDeltaDecelSpeed;  

int16_t		tsp_fade_ctrl_cnt;
int16_t		tsp_decel_ctrl_cnt;

int16_t		lcs16TspInitialDecelCurrent;
int16_t		lcs16TspDiffOnlyCurrent;
int16_t		Delta_moment_tsp;

int16_t		lcs16TspDiffCtrlAddCurP;
int16_t		lcs16TspDiffCtrlAddCurS;
int16_t		lcs16TspMFCCurP;
int16_t		lcs16TspMFCCurS;

uint8_t 	lcu8TspTorqDownRate;
int16_t 	lcu8TspTorqlimit;
int16_t  	tsp_torq;
