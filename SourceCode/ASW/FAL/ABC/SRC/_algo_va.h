#ifndef __ALGO_VA_H_
#define __ALGO_VA_H_

#include "Abc_Ctrl.h"
#if __VDC
#include "v_struct.h"
#endif

#define     IN_SIM      0      /* set to 1 if simulation will be done */

struct W_STRUCT *WL,*WL_Axle,*WL_Side,*WL_Diag, FL, FR, RL, RR;

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
struct ESC_STRUCT *WL_ESC, FL_ESC, FR_ESC, RL_ESC, RR_ESC;
struct ESC_CIRCUIT_STRUCT *CIR_ESC, PC_ESC, SC_ESC;
struct ESC_STRUCT_COMB *WL_ESC_COMB, FL_ESC_COMB, FR_ESC_COMB, RL_ESC_COMB, RR_ESC_COMB;
#endif

#if	defined (BTCS_TCMF_CONTROL) /* BTCS_TCMF_CONTROL */
struct TCS_WL_STRUCT *WL_TCS, FL_TCS, FR_TCS, RL_TCS, RR_TCS;
struct TCS_AXLE_STRUCT *AXLE_TCS, FA_TCS, RA_TCS;
struct TCS_CIRCUIT_STRUCT *CIR_TCS, PC_TCS, SC_TCS;
#endif

struct V_STRUCT *FZ, FZ1, FZ2;

#if __SIDE_VREF
struct V_STRUCT  FZ3, FZ4;
#endif

#if (__4WD||(__4WD_VARIANT_CODE==ENABLE)) 
struct V_STRUCT		FZ_for_4WD;
#endif

#if (__VDC && __MGH40_ESP_BEND_DETECT)
struct SIDE_STRUCT  *DIRECTION, LEFT, RIGHT;
#endif

U8_BIT_STRUCT_t VF0;
U8_BIT_STRUCT_t VF1;
U8_BIT_STRUCT_t VF2;
U8_BIT_STRUCT_t VF3;
U8_BIT_STRUCT_t VF4;
U8_BIT_STRUCT_t VF5;
U8_BIT_STRUCT_t VF6;
U8_BIT_STRUCT_t VF7;
U8_BIT_STRUCT_t VF8;
U8_BIT_STRUCT_t VF9;
U8_BIT_STRUCT_t VF10;
U8_BIT_STRUCT_t VF11;     //2002.04.09 for LFC
U8_BIT_STRUCT_t VF12;     //2002.11.22 for LFC
U8_BIT_STRUCT_t VF13;     //2003.07.02 for LFC
U8_BIT_STRUCT_t VF14;     //2003.09.23 for LFC
U8_BIT_STRUCT_t VF15;     // 2004.02.25. for EDC
U8_BIT_STRUCT_t VF16;	//2007.04.30 for EDC
U8_BIT_STRUCT_t VF17;	//2007.04.30 for EDC




//#if __MGH_40_VREF
//U8_BIT_STRUCT_t VF20;
//U8_BIT_STRUCT_t VF25;
//U8_BIT_STRUCT_t VF26;
//#endif

U8_BIT_STRUCT_t VF21;

#if	__TCMF_CONTROL
U8_BIT_STRUCT_t VF19;
U8_BIT_STRUCT_t VF22;
#endif

//#if __SIDE_VREF
//U8_BIT_STRUCT_t VF23;
//#endif

U8_BIT_STRUCT_t VF24;

//#if __VREF_LIMIT_1
//U8_BIT_STRUCT_t VF27;
//#endif

//#if __Ax_OFFSET_MON
//U8_BIT_STRUCT_t VF28;
//U8_BIT_STRUCT_t VF29;
//#endif

//#if __VREF_LIMIT_2
//U8_BIT_STRUCT_t VF30;
//#endif


//  uint16_t cur_cycle_time, max_cycle_time; // For checking ABS cycle time at 03NZ winter


    uint8_t lsu8DrvMode;
    uint16_t speed_calc_timer;
    int16_t   afz_alt;
/*  Transfer to Vref Header
    int16_t   afz_alt;
    uint8_t inst_zeit;
    uint8_t bls_k_timer;
    uint8_t bls_filter_timer;
*/
//    uint8_t motor_run_on_timer;


    int16_t   vref;
    int16_t   vref2;
    int16_t	afz;
/*  Transfer to Vref Header
    #if __VREF_AUX
    int16_t   vref_aux_resol_change;
    int16_t   vref_aux_resol_change_old;
    int16_t   vref_aux;
    #endif

    #if __VREF_LIMIT_1
    int16_t   CNT_LM1;
    int16_t   WFactor;
    uint8_t CNT_L2H;
    #endif

    #if __VREF_LIMIT_2
    int16_t   CNT_LM2;
	int16_t   Estimated_Press_ABS_FL;
	int16_t   Estimated_Press_ABS_FR;
	int16_t   Estimated_Press_ABS_RL;
	int16_t   Estimated_Press_ABS_RR;
	int16_t   Estimated_Press_ABS_MP;
	int16_t   VrefPressDecelG;
    #endif

    int16_t   filt_ein;
    int16_t   voptfz_new;
    int16_t   v_afz, v_afz_old, v_afz_old2;
    int16_t   afz, afz_temp;
    uint16_t  t_afz, t_afz_old, t_afz_old2;

    int16_t   Vehicle_Decel_EST;  //0509 KSW
    int16_t   Vehicle_Decel_EST_filter; //051020 KSW
    int16_t   Vehicle_Decel_Max;//051020 KSW
    int8_t  v_afz_time;
    uint8_t afz_flags;
    uint8_t AFZ_SUS_CNT;

    int16_t   ax_Filter_temp;        //0601 China Check
    int16_t   ax_Filter;        //0601 China Check


	int8_t  arad_select_wheel;     //0606

//    uint8_t reserve_log;
    uint8_t ebd_filt_count;

    int16_t vref_input_inc_limit;
    int16_t vref_input_dec_limit;
    int16_t vref_output_inc_limit;
    int16_t vref_output_dec_limit;
    int16_t vref_limit_inc_counter;
    int16_t vref_limit_dec_counter_frt;
    int16_t vref_limit_dec_counter_rr;
    uint8_t Unstabil_counter_fl;   // 2006.01.10. for VREF_LIMITTER
    uint8_t Unstabil_counter_fr;

//  int16_t 	rta_ratio_ref_speed;
//	uint8_t 	rta_wheel_flag;

    #if (__4WD||(__4WD_VARIANT_CODE==ENABLE))
    int16_t vref_inc_limit_by_g;
    int16_t vref_dec_limit_by_g;
    int8_t g_sensor_limit_flags;
    #endif

   	#if ((__4WD||(__4WD_VARIANT_CODE==ENABLE)) && __MGH_40_VREF)
	int16_t vdiff_by_along_1;
	int16_t	vdiff_by_along_2;
	int16_t	vdiff_by_along_3;
	int16_t	vdiff_by_along_4;
	int16_t	vdiff_by_along_5;
	int16_t	vdiff_by_along_6;
	int16_t	vdiff_by_along_mod;
	int16_t	vref_along_1;
	int16_t	vref_along_2;
	int16_t	vref_along_3;
	int16_t	vref_along_4;
	int16_t	vref_along_5;
	int16_t	vref_along_6;
	int16_t vref_by_along;
	int16_t vdiff_by_along;
	int8_t accel_det_flags; // 임시
	uint8_t	accel_spin_det_cnt1;
	uint8_t	accel_spin_det_cnt2;
	uint8_t	accel_spin_off_timer;
	uint8_t	accel_spin_on_timer;
	int16_t		along_vref_sum;
	uint8_t	long_spin_cnt;
	uint8_t 	vrad_diff_max_cnt;
	#endif
*/
int16_t   abmittel_fz;
  #if __MGH40_EBD
//    int8_t  slip_thres;
//    int8_t  arad_thres;
    //int8_t front_rear_speed_percent;
//    int16_t decel_thres;
  #else
//    uint8_t s_diff_pos_timer;
  #endif
/* Transfer to Vref Header
    uint8_t avr_grv_counter;

    uint8_t grv_diff_cnt;
    int16_t vrad_diff_max;
    uint8_t vref_fail_mode_cnt;
    int16_t   ebd_refilt_grv;

//  int8_t arad_timer;
//  int8_t arad_avg_counter;
     #if __MGH_40_VREF
  int16_t vrad_crt_max;
  int16_t vrad_crt_min;
  int16_t vrad_crt_max_for_vref_est;
  int16_t	vref_limit_by_along_mod;
  #if _VRSELECT_ADVANCE
  int16_t vrselect_new1;
  int16_t vrselect_new2;
  #endif
  	#if __EDC
//  uint16_t	eng_rpm_old;
//  uint16_t tc_rpm;
//  uint16_t tc_rpm_old;
//  int16_t diff_eng_tc_rpm;
//  int8_t engine_state;
  	#endif
  #endif
   #if __WL_SPEED_RESOL_CHANGE
  int16_t vrad_diff_max_resol_change;
  int16_t vref_resol_change;
  int16_t vref_alt_resol_change;

       #if	__SIDE_VREF
//    int16_t   afz_ok_sum;                 // Side Wheel Reference 04.11.10
//    int8_t   inc_limit_f;                // Side Wheel Reference 04.11.10
//    int8_t   inc_limit_n;                // Side Wheel Reference 04.11.10
//    int8_t   dec_limit_f;                // Side Wheel Reference 04.11.10
//    int8_t   dec_limit_n;                // Side Wheel Reference 04.11.10
    int16_t    vref_L;                     // Side Wheel Reference 04.11.11
    int16_t    vref_L_old;
    int16_t    vref_L_diff;
    int16_t    vref_R;                     // Side Wheel Reference 04.11.11
    int16_t    vref_R_old;
    int16_t    vref_R_diff;
    int16_t	   voptfz_new_side;				 // Side Wheel Reference 04.12.01
    int16_t    voptfz_new3;                // Side Wheel Reference 04.11.11
    int16_t    voptfz_new4;                // Side Wheel Reference 04.11.11
//    int16_t    voptfz_out;                 // Side Wheel Reference 04.11.11
//    int16_t    voptfz_in;                  // Side Wheel Reference 04.11.11
//    int16_t    voptfz_out_1;               // Side Wheel Reference 04.11.11 (n-1) term
//    int16_t    voptfz_in_1;                // Side Wheel Reference 04.11.11 (n-1) term
//    int8_t   inside_alimit_unl;          // Side Wheel Reference 04.11.11
//    int8_t   inside_alimit_lim;          // Side Wheel Reference 04.11.11
//    int8_t   inside_alimit_inc;          // Side Wheel Reference 04.11.11
//    int8_t   inside_alimit_dec;          // Side Wheel Reference 04.11.11
    int16_t   voptfz_new_resol_change;    // Side Wheel Reference 04.11.19
    int16_t   dv_yaw_est;                 // Side Wheel Reference 05.05.31
    int16_t   dv_yaw_est_filt;            // Side Wheel Reference 05.05.31
    int16_t   dv_ay_est;				  // Side Wheel Reference 05.06.02
     #endif
  #endif

//     #if  __Ax_OFFSET_MON
//     uint16_t	AxOffCNT;
//	 int16_t	AxOffset;
//	 int16_t	AxOffsetCalc;
//	 int16_t	AxOffsetCalc1;
//	 int16_t	AxOffsetCalc2;
//	 int16_t	AxOffsetCalc3;
//	 int16_t	AxOffsetCalc1temp;
//	 int16_t	AxOffsetCalc3temp;
//	 int32_t   AxOffsetSUM;
//	 int32_t   AxOffsetSUM2;
//     #endif

    int16_t   ebd_filt_grv;
    int16_t   suma;						// 하나로 줄일 수 있음.
    int16_t   sumb;
    int16_t   sumc;
    int16_t   sumd;
    int16_t   sume;
    int16_t   ebd_filt_avr_grv;
//    int16_t   sum_grv;
//    int16_t   ebd_avr_grv;
//    int16_t   ebd_grv_hold;
//    uint8_t grv_avr_counter;
//    uint8_t ebd_inc_count;
//    uint8_t spin_counter;
//    uint8_t brake_off_time;
    uint8_t afz_cal_delay;

//    int8_t  msl_rough_slip;		//2001. 1025 cjy
//    int8_t  msl_band_slip1;
//    int8_t  msl_band_slip2;
*/
    int16_t   vref_alt;
//    int16_t   vref3, vref4;
        #if __VDC
    int16_t   vref5,vref6;
    #if __WL_SPEED_RESOL_CHANGE
 //   int16_t   vref5_resol_change;
    #endif
    	#endif

    int16_t   along;
/*
        #if ((__4WD||(__4WD_VARIANT_CODE==ENABLE)) || __AX_SENSOR)
    uint8_t   count_ice;
    int16_t   along_vref;
    int16_t   along_ice_average;
    int16_t   count_ice_up;
//    int16_t   along_ice_up;
//    int16_t   along_ice_average_up;

    int16_t   along_old;
//    int16_t   actual_g;

    int16_t	  along_standstill, di_along;
//    int16_t	  bi_along_quo, bi_along_mod;
    int16_t	  g_vref;
//    int16_t	  bi_vref, a_sen_weight;

    int16_t   along_min;
    int16_t   along_max;
    int16_t   vref2_grv_min;
    int16_t   vref2_grv_max;
    int16_t   along_setted;

    int16_t   along_filt;
    int16_t   along_filt_diff[5];	// Counter 쓰면 없앨 수 있음...
    int16_t   vref2_grv;
    int16_t   vref2_grv_diff[5];	// Counter 쓰면 없앨 수 있음...
    int16_t   g_offset;
    int16_t   g_offset_diff[5];		// Counter 쓰면 없앨 수 있음...
    int16_t   vref2_old;
    int16_t   vref2_grv_diff1;
    int16_t   vref2_grv_diff_old;
    int16_t   acc_vref2;
    int16_t   acc_var;
    uint8_t  vref_fail_count;
    uint8_t  vref_fail_count_afz;      //  050817 NZ 추가
    uint8_t  vref_fail_dump_count_fl;  //  050806 NZ 추가
    uint8_t  vref_fail_dump_count_fr;  //  050806 NZ 추가
    uint8_t  vref_fail_dump_count_rl;  //  050806 NZ 추가
    uint8_t  vref_fail_dump_count_rr;  //  050806 NZ 추가
    uint16_t  control_fail_count;
    uint8_t  standstill_count;
    uint8_t vref2_grv_count;

    int16_t   along_quo, along_mod;

//    uint8_t vref3_count;
//    uint8_t vref5_count;
//    uint8_t scan_20_count;

    uint8_t along_fail,g_sensor_check_count,along_offset_count;
    #endif

#if (__VDC && (__4WD||(__4WD_VARIANT_CODE==ENABLE)) && __MGH_40_VREF)
    int16_t   along_by_drift;
#endif
*/

  #if __HSA
  	int16_t HSA_ax_sen, HSA_ax_sen_filt;
  #endif

   
#if (__VDC || __BTC || __ETC)   //BY HJH
    uint16_t  loop_time;
    uint16_t  motor_pwm_count;
    int16_t   press_test_count;
#endif

#if __VALVE_TEST_L || __VALVE_TEST_E || __VALVE_TEST_B || __BLEEDING_E || __VALVE_TEST_CPS || __VALVE_TEST_F || __VALVE_TEST_ESC_LFC || __VALVE_TEST_ESC_ABS_LFC     
    		int16_t   esc_press_test_count;
#endif
    
/* #if __PWM */ /* 2006.06.15  K.G.Y */
//    uint8_t LFC_active_test_count;
//    uint8_t LFC_sol_repeat_period_timer;
//    uint8_t LFC_init_check_pass;
//    uint8_t LFC_init_ready_ok;
//    uint8_t check_lamp_for_LFC_fire;
//    uint16_t fl_no_current;				// MGH 25 해당 없음...
//    uint16_t fr_no_current;				// MGH 25 해당 없음...
//    uint16_t rl_no_current;				// MGH 25 해당 없음...
//    uint16_t rr_no_current;				// MGH 25 해당 없음...

//    uint16_t fl_no_current_old;      //2001.12.03
//	uint16_t fr_no_current_old;
//    uint16_t rl_no_current_old;
//    uint16_t rr_no_current_old;
//    uint8_t LFC_sol_curr_cnt;
//    uint16_t init_sol_curr_fl;
//    uint16_t init_sol_curr_fr;
//    uint16_t init_sol_curr_rl;
//    uint16_t init_sol_curr_rr;
//    uint8_t fli_21ms_on_flg;
//    uint8_t fri_21ms_on_flg;
//    uint8_t rli_21ms_on_flg;
//    uint8_t rri_21ms_on_flg;
//    uint8_t sol_volt_comp_end_flg;
//    uint8_t sol_14ms_redrive_end;
    uint8_t LFC_mot_drv_cnt;
//    uint8_t LFC_mot_pwm_ok;				// Bit 변수로 바꿀 수 있음...
//    uint8_t fli_sol_curr_ok_flg;    //2001.12.03
//    uint8_t fri_sol_curr_ok_flg;
//    uint8_t rli_sol_curr_ok_flg;
//    uint8_t rri_sol_curr_ok_flg;

//	uint8_t LFC_fli_mon_ok_flg;       // 2001.12.03
//    uint8_t LFC_fli_mon_ok_old_flg;
//    uint8_t LFC_fri_mon_ok_flg;
//    uint8_t LFC_fri_mon_ok_old_flg;
//    uint8_t LFC_rli_mon_ok_flg;
//    uint8_t LFC_rli_mon_ok_old_flg;
//    uint8_t LFC_rri_mon_ok_flg;
//    uint8_t LFC_rri_mon_ok_old_flg;
//    uint16_t volt_diff;
//    uint8_t milli_sec_cnt;			// 없앨 수 있음...
//    uint8_t msc_7ms_control;			// 없앨 수 있음...

//	int8_t ign_comp_factor;            // 2002.01.28
//    uint16_t time_after_motor_off;
//    uint8_t ign_14V_above_timer;
//    uint8_t ign_13V4_above_timer;
//    uint8_t ign_13V4_below_timer;
//    uint8_t batt_level_indicater;
//    uint8_t thre_sec_pass_aft_mot_off;
//    uint8_t LFC_ign_chk_cnt;
//    uint8_t LFC_ign_ok;
//    uint16_t LFC_ign_mon_old1;
//    uint16_t LFC_ign_mon_old2;
//    uint16_t LFC_ign_mon_old3;
//    uint16_t LFC_ign_mon_old4;
//   uint16_t LFC_ign_avr;

    int16_t   LFC_mot_corr_factor;		// int8_t 로 바꿀 수 있음...		/* LAABSCallMotorSpeedControl.C 로 이동 */


//	int8_t	Ref_dump_temp;


// ********* added by y.w.shin at 2002.winter
//    uint8_t MSL_opt_mon_rl;
//    uint8_t MSL_opt_mon_rr;
//    uint8_t gma_dump_cnt_rl_for_LFC;
//    uint8_t gma_dump_cnt_rr_for_LFC;
// ********* added by y.w.shin at 2002.winter

//------------------------------------------------------------------
//  added by CSH 020709

//------------------------------------------------------------------
/*  uint8_t Rough_road_detector_counter;
    uint8_t Rough_road_detector_counter2;
    uint8_t scan_counter;     // Moved from a_struct.h by J.K.Lee at 03NZ winter
    uint8_t num; */

/*  Transfer to Vref Header
    int16_t		aref0;
    int16_t		aref_avg;
    int16_t		aref10;
    int16_t		aref20;
    int16_t		aref30;
    int16_t		aref40;
    int16_t		aref50;
	uint8_t	aref_timer;
	uint8_t	aref_avg_counter;
	int16_t		vref0;

	uint8_t	vref_increase_cnt;
	int16_t		vref_trough;
*/

  #if	__TCMF_CONTROL
	uint8_t	TCMF_MOTOR_OFF_cnt;											/* LAABSCallMotorSpeedControl.C 로 이동 */
  #endif

/* #endif */ /* 2006.06.15  K.G.Y */
//******************** 여기까지 MGH-40, MGH-25 ESP 공통

/* #if __ESP_ECU && __PWM */ /* 2006.06.15  K.G.Y */
#if __ECU==ESP_ECU_1
//	int8_t delta_p_comp_left;
//  int8_t delta_p_comp_right;

//  uint8_t ms_int_ok;
//  uint16_t ms_otp_cnt;
//  uint16_t ms_otp_cnt1;

//  uint8_t L_timer_for_5_to_55bar;
//  int8_t master_press_comp_const;

//	uint8_t pwm_flg;
//  uint8_t pwm_cnt;

	uint8_t init_duty;
	int16_t ESP_MSC_ref_voltage;
//  uint8_t duty_ratio_inc;
//  uint8_t duty_ratio_dec;

#endif

//const	uint8_t pwm_duty_null=0;

   
#if __BTC
/* btcs */
	uint8_t btcref_count;
    uint8_t mot_count;
/* NOT USED AFTER BTCS RESTRUCTURING 120207
    int16_t   sum_btc_slip;
    int16_t   sum_btc_slip_alt;
    int16_t   avr_btc_slip;
    int16_t   avr_slip_min;
    int16_t   avr_fspeed;
    int16_t   avr_fspeed_alt;
    uint8_t avr_count1;
    uint8_t avr_count2;
    uint8_t avr_count3;
    uint8_t vcount;
    int8_t  vrefdiff;
    int8_t  vrefdiff_alt1;
    int8_t  vrefdiff_alt2;
    int8_t  avr_vref;
    int8_t  avr_vref_alt;
  NOT USED AFTER BTCS RESTRUCTURING 120207 */
    uint8_t bls_count;
    uint8_t write_count;
    uint8_t coop_count;
    uint8_t esv_count_l;
    uint8_t esv_count_r;
    uint8_t esv_count_lpa;
    /*051005 New BTCS Structure*/
	uint8_t btcs_control_period;
#endif
   

#if __WL_SPEED_RESOL_CHANGE
	int32_t	tempL0;						int32_t	tempL1;
	int32_t tempL9;
#endif

    int16_t   tempW0;                      int16_t   tempW1;
    int16_t   tempW2;                      int16_t   tempW3;
    int16_t   tempW4;                      int16_t   tempW5;
    int16_t   tempW6;                      int16_t   tempW7;
    int16_t   tempW8;                      int16_t   tempW9;

    int8_t  tempB0;                      	int8_t  tempB1;
    int8_t  tempB2;                      	int8_t  tempB3;
    int8_t  tempB4;                      	int8_t  tempB5;
    int8_t  tempB6;                      	int8_t  tempB7;
    uint8_t  tempC1;						uint8_t  tempC2;

    uint8_t  tempT1;						uint8_t  tempT2;

/////////////////////////////////////////////////////////MISRA//////////////
    uint8_t fsr_on_time;
    uint16_t tempUW0, tempUW1, tempUW2, tempUW3, tempUW4;
    uint8_t ee_btcs_data;
//    uint8_t weu16Eep_addr;

    uint8_t com_rx_buf[8];

	////////////////////
	///////////////////////////////////////////////////////////////////////////
#endif
