/******************************************************************************
* Project Name: ELECTRONIC BRAKE PREFILL
* File: LDEBPCallDetection.C
* Date: June. 4. 2005
******************************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDEBPCallDetection
	#include "Mdyn_autosar.h"
#endif
/* Includes ******************************************************************/

#include "LDEBPCallDetection.h"
#include "LAEPCCallActHW.h"
#if __HDC
#include "LCHDCCallControl.h"
#endif



  #if __BRK_SIG_MPS_TO_PEDAL_SEN
#include "LSESPFilterEspSensor.h"
  #endif

#if __EBP
/* Logcal Definiton  *********************************************************/


/* Variables Definition*******************************************************/
U8_BIT_STRUCT_t EBPD0;

/* LocalFunction prototype ***************************************************/
void LDEBP_vCallDetection(void);
void LDEBP_vDetectActCondition(void);
void LDEBP_vDetectDisable(void);

/* Implementation*************************************************************/

 
static int16_t mtp_old7,mtp_old6,mtp_old5,mtp_old4,mtp_old3,mtp_old2,mtp_old1;

int16_t EBP_ENABLE_cnt;

uint8_t lu8EbpCompMtp;
 


void LDEBP_vCallDetection(void)
{
	LDEBP_vDetectActCondition();
	LDEBP_vDetectDisable();
}

void LDEBP_vDetectActCondition(void)
{
	mtp_old7=mtp_old6;
	mtp_old6=mtp_old5;
	mtp_old5=mtp_old4;
	mtp_old4=mtp_old3;
	mtp_old3=mtp_old2;
	mtp_old2=mtp_old1;
	mtp_old1=mtp;
	
  #if (__BRK_SIG_MPS_TO_PEDAL_SEN == 1)	
	if((lsespu1PedalBrakeOn==1)&&(eng_torq < 50))
  #else
    if((MPRESS_BRAKE_ON==1)&&(eng_torq < 50))
  #endif	
	{

		if(mtp >= lu8EbpCompMtp)
		{
			lu8EbpCompMtp = mtp;
		}
		else if(mtp < 3)
		{
			lu8EbpCompMtp = 3;
		}  
		else
		{
			lu8EbpCompMtp = lu8EbpCompMtp;
		}
	}
	else if (lu8EbpCompMtp < 3)
	{
		lu8EbpCompMtp = 3;
	}
	else
	{
		;
	}
	
	if (((( mtp - mtp_old7) < -24 )||(( mtp - mtp_old3) < -15 )||(( mtp - mtp_old2) < -10 ))
		&&(mtp<lu8EbpCompMtp)
		&&(mtp_old7>=mtp_old6)
		&&(mtp_old6>=mtp_old5)
		&&(mtp_old5>=mtp_old4)
		&&(mtp_old4>=mtp_old3)
		&&(mtp_old3>=mtp_old2)
		&&(mtp_old3>=mtp))
	{
		DETECT_MTP_RELEASE=1;
	}
	else
	{
		DETECT_MTP_RELEASE=0;
	}

/*  진입 조건 SET           */
/* 작동 최저 속도 SET       */
/* 재진입 조건 설정.        */

	esp_tempW9 = VREF_60_KPH;
	esp_tempW8 = L_U8_TIME_5MSLOOP_500MS;

	if ((!EBP_ENABLE)
		&&(vref>esp_tempW9)
		&&(DETECT_MTP_RELEASE)
		&&(EBP_ENABLE_cnt<esp_tempW8)
		&&(BLS==0)
	  #if (__BRK_SIG_MPS_TO_PEDAL_SEN == 1)	
		&&(lsesps16EstBrkPressByBrkPedalF<MPRESS_4BAR))
	  #else
	    &&(mpress<MPRESS_4BAR))
	  #endif	
	{
		EBP_ENABLE=1;
	}
	else if (
	  #if (__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
	    (lsesps16EstBrkPressByBrkPedalF>MPRESS_5BAR)
	  #else	
		(mpress>MPRESS_5BAR)
	  #endif	
		||(BLS==1)
		||(EBP_ENABLE_cnt>=esp_tempW8)
		||(mtp>2))
	{
		EBP_ENABLE=0;
	}
	else
	{
		;
	}

	if (EBP_ENABLE)
	{
		EBP_ENABLE_cnt++;
	}
	else
	{
		EBP_ENABLE_cnt=0;
	}

}

void LDEBP_vDetectDisable(void){

	if ((YAW_CDC_WORK==1)||(ESP_PULSE_DUMP_FLAG==1)||(ABS_fz==1)||(BTC_fz==1)||(EBD_RA==1)
		||(EPC_ON==1)||(PBA_ON==1)||(fu1OnDiag==1)||(init_end_flg==1)

		#if __HDC
		||(lcu1HdcActiveFlg==1)
		#endif

		#if __TSP
		||(TSP_ON==1)
		#endif
        
		#if __GM_FailM
		||(fu1ESCEcuHWErrDet==1)                                                                        /* ESC HW   */
		||(fu1MCPErrorDet==1)                                                                           /* MP       */
		||(fu1WheelFLErrDet==1)||(fu1WheelFRErrDet==1)||(fu1WheelRLErrDet==1)||(fu1WheelRRErrDet==1)    /* Wheel    */
//      ||(fu1EMSTimeOutErrDet==1)                                                                      /* MTP      */

		   #if __BRAKE_FLUID_LEVEL
			||(fu1DelayedBrakeFluidLevel==1)
		   #endif
		#else
		||(ESP_ERROR_FLG==1)
		#endif

		)
	{
		EBP_ENABLE=0;
		EBP_ENABLE_cnt=0;
	}
	else{}

}
#endif

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDEBPCallDetection
	#include "Mdyn_autosar.h"
#endif    
