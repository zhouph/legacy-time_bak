#ifndef __LATCSCALLACTHW_H__
#define __LATCSCALLACTHW_H__
/*Includes *********************************************************************/
#include "LVarHead.h"

  
/*Global MACRO CONSTANT Definition *********************************************/

/*Global MACRO FUNCTION Definition *********************************************/

/*Global Type Declaration ******************************************************/

/*Global Extern Variable Declaration *******************************************/
  #if (__IDB_LOGIC == ENABLE)
extern int16_t latcss16PriTcTargetPress, latcss16SecTcTargetPress;
  #endif /* __AHB_GEN3_SYSTEM == ENABLE */

/*Global Extern Functions Declaration ******************************************/

/*Temporary Tuning Parameter Declaration ***************************************/
#define		S16TCSCpTCVOffPres		30	/* 30 : 3bar */
#define   U8_TCS_APPLY_TIME_FRT 10
#define   U8_TCS_APPLY_TIME_RR  10
#define   U16_BTCS_ESV_CONTROL  0

#if	!defined(BTCS_TCMF_CONTROL)
  #if (__ESC_MOTOR_PWM_CONTROL==ENABLE)
	#define __TCS_MSC_DIRECT_DUTY_CONTROL	  ENABLE
  #else
	#define __TCS_MSC_DIRECT_DUTY_CONTROL	  DISABLE
  #endif
#endif

#endif
