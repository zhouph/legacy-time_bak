#ifndef __LDABSDCTROADSURFACE_H__
#define __LDABSDCTROADSURFACE_H__

/* Includes ************************************************************/

#include "LVarHead.h"


/* Global Function Declaration *****************************************/

extern void LDABS_vDctRoadSurface(void);


/* Global Variables Declaration ****************************************/

extern uint8_t Rough_road_detector_counter;
extern uint8_t Rough_road_detector_counter2;
extern uint8_t scan_counter;
extern uint8_t num;  

#endif
