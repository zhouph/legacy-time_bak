/* Includes ********************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCHDCCallControl
	#include "Mdyn_autosar.h"
#endif


#include "LCHDCCallControl.h"
  #if __HDC
#include "LCDECCallControl.h"
#include "LAHDCCallActHW.h"
#include "LDESPEstBeta.h"
#include "hm_logic_var.h"
#include "Hardware_Control.h"
#include "LCESPCalLpf.h"
#include "LCESPCalInterpolation.h"
#include "LCallMain.h"
#include "LSESPCalSlipRatio.h"
#include "LSABSCallSensorSignal.h"
#include "LDABSCallEstVehDecel.H"
#include "LSABSEstLongAccSensorOffsetMon.h"
  #if defined(__dDCT_INTERFACE)
#include "LCHRCCallControl.h"
  #endif
#include "LDESPEstDiscTemp.h"


 #endif



/* Local Definition  ***********************************************/
  #if __HDC

#define U8_HDC_ENTER                    1
#define U8_HDC_READY                    2
#define U8_HDC_EXIT                     3

#define S8_HDC_SUB_H2TG                 1
#define S8_HDC_SUB_TG                   2
#define S8_HDC_SUB_L2TG                 3



#define __HDC_FORCED_SW_ON              0

#define AX_Resol                        10

#define __Dectect_Hill_by_Vehilce_Accel 0
#define __Comp_Long_G_by_Press          0
#define __STOP_ON_HILL                  1
#define __N_GEAR_HDC_Enable             0
#define __HDC_Reenter_Enable            1
#define __LongG_Offset_Test             0

#define __Yaw_Factor_th_Change          1
#define __N_Gear_Detect_Change          1
#define __SAS_OK_BACK_Change			1

/*	08SWD	*/
#define __Thetag_Dir                    1
#define __FF_Ctrl_Enable                1
#define __AX_Sensor_Buffer              0
#define __Engine_Idle_Check             1
#define __HDC_FADE_OUT_CTRL             1
#define __INITIAL_FOR_BACK_CHK          1
#define __YAW_FOR_BACK_CHK              1

#define __POWER_ENGAGE_AT_CLUTCH_ON 	1 

  #endif

/* Variables Definition*********************************************/
  #if __HDC

struct  U8_BIT_STRUCT_t HDC00;
struct  U8_BIT_STRUCT_t HDC01;
struct  U8_BIT_STRUCT_t HDC02;
struct  U8_BIT_STRUCT_t HDC03;
struct  U8_BIT_STRUCT_t HDC04;
struct  U8_BIT_STRUCT_t HDC05;
struct  U8_BIT_STRUCT_t HDC06;
struct  U8_BIT_STRUCT_t HDC07;
struct  U8_BIT_STRUCT_t HDC08;
struct  U8_BIT_STRUCT_t HDC09;
struct  U8_BIT_STRUCT_t HDC10;



int16_t     lcs16_hdc_tempW5,lcs16_hdc_tempW6,lcs16_hdc_tempW7,lcs16_hdc_tempW8,lcs16_hdc_tempW9;




int16_t 	lcs16HdcEngIdleTorque;
int16_t 	lcs16HdcEngIdleRpm;
int16_t 	lcs16HdcEngStallRpm;
int16_t 	lcs16HdcEngOverRpm;
uint8_t 	lcu8HdcEngTorq_N_cnt;
uint8_t 	lcu8HdcEngTorq_cnt;

int16_t 	lcs16HdcINGearSusCnt;
int16_t 	lcs16hdcEngEngageOkCnt;

int16_t 	lcs16HdcYaw_F_cnt;
int16_t 	lcs16HdcYaw_B_cnt;

uint8_t   lcu8hdcGearR_Switch_cnt;
uint8_t   lcu8hdcClutchSwitch_cnt;
uint8_t   lcu8hdcClutchSwitch_timer;

int16_t lcs16HdcThetaG_F_cnt,lcs16HdcThetaG_F_diff,lcs16HdcThetaG_F_old,lcs16HdcThetaG_F,lcs16HdcAXg_filter_old,lcs16HdcAXg_filter,lcs16HdcThetaG_F_diff_lpf;
int16_t lcs16HdcThetaG_B_cnt,lcs16HdcThetaG_B_diff,lcs16HdcThetaG_B_old,lcs16HdcThetaG_B,lcs16HdcWhg_filter_old,lcs16HdcWhg_filter,lcs16HdcThetaG_B_diff_lpf;
int16_t lcs16HdcThetaG_cnt;


int16_t lcs16HdcWhg_filter_diff,lcs16HdcWhg_filter_diff_old;
int16_t lcs16HdcAXg_filter_diff,lcs16HdcAXg_filter_diff_old;
int16_t lcs16HdcThetaG_N_cnt;


uint8_t	lcu8HdcEngUpdateCnt, lcu8HdcEngEngageCnt1, lcu8HdcEngEngageCnt2, lcu8HdcGearPositionOld,lcu8HdcDecelOKtimer,lcu8hdcASenSTBCnt,lcu8HdcChkGearInformCnt,lcu8HdcToqSTBtimerAfterAccel; 
int16_t	lcs16HdcEngrpmOld, lcs16HdcEngrpmOld2, lcs16HdcEngrpmDot, lcs16HdcEngrpmDotOld, lcs16HdcEngrpmDotOld2, lcs16HdcEstEngrpmDot, lcs16HdcEstEngrpmDotOld, lcs16HdcEstEngrpmDotOld2;  
int16_t lcs16HdcEngrpmDotPeakN, lcs16HdcEngTorqDiff, lcs16HdcEngTorqOld;

uint8_t   lcu8HdcWhgSusCnt,lcu8HdcWhgSusCnt2;
int16_t     lcs16HdcWhgSusmin,lcs16HdcWhgSusmax;
int16_t     lcs16HdcWhgSusmin_3,lcs16HdcWhgSusmax_3;
int16_t     lcs16HdcWhgSusmin_2,lcs16HdcWhgSusmax_2;
int16_t     lcs16HdcWhgSusmin_1,lcs16HdcWhgSusmax_1;
int16_t     lcs16HdcWhgSusmin_0,lcs16HdcWhgSusmax_0;
int16_t     lcs16HdcWhgSusTimer;

uint8_t lcu8HdcThetaGDisableCnt,lcu8HdcSpeedCnt;

int16_t lcs16HdcAx1000gbuffer_20;
int16_t lcs16HdcAx1000gbuffer_19;
int16_t lcs16HdcAx1000gbuffer_18;
int16_t lcs16HdcAx1000gbuffer_17;
int16_t lcs16HdcAx1000gbuffer_16;
int16_t lcs16HdcAx1000gbuffer_15;
int16_t lcs16HdcAx1000gbuffer_14;
int16_t lcs16HdcAx1000gbuffer_13;
int16_t lcs16HdcAx1000gbuffer_12;
int16_t lcs16HdcAx1000gbuffer_11;
int16_t lcs16HdcAx1000gbuffer_10;
int16_t lcs16HdcAx1000gbuffer_9;
int16_t lcs16HdcAx1000gbuffer_8;
int16_t lcs16HdcAx1000gbuffer_7;
int16_t lcs16HdcAx1000gbuffer_6;
int16_t lcs16HdcAx1000gbuffer_5;
int16_t lcs16HdcAx1000gbuffer_4;
int16_t lcs16HdcAx1000gbuffer_3;
int16_t lcs16HdcAx1000gbuffer_2;
int16_t lcs16HdcAx1000gbuffer_1;
int16_t lcs16HdcAx1000gbuffer_0;

uint8_t 	lcu8HdcTargetSpeedcnt;

uint8_t   lcu8HdcMFCCtrlStep;
int8_t    lcs8HdcState,lcs8HdcSubState;

uint8_t   lcu8HdcDriverAccelIntend, lcu8HdcMtpCnt,lcu8HdcMtpOffCnt;
uint8_t   lcu8HdcDriverBrakeIntend,lcS16HdcBrkCnt;

uint8_t	lcu8HdcGearPosition, lcu8HdcTgSpdSetMode;

uint8_t   lcu8HdcSmaThetaGCnt, lcu8HdcMidThetaGCnt, lcu8HdcBigThetaGCnt;
uint8_t   lcu8HdcSmaThetaGNCnt, lcu8hdcEngStallSus2OnTimer, lcu8hdcEngStallSusOntimer;
uint8_t   lcu8HdcSmaThetaGEstTime, lcu8HdcMidThetaGEstTime, lcu8HdcBigThetaGEstTime;
uint8_t   lcu8HdcFlatPreCnt, lcu8HdcFlatGCnt;
uint8_t   lcu8HdcBumpGCnt, lcu8HdcBumpSuspectCnt;

uint8_t   lcu8HdcPreBumpGCnt,lcu8HdcBumpSuspect,lcu8HdcBumpSuspect_cnt,lcu8HdcBumpDelayActTime,lcu8HdcBumpDectectTime;
uint8_t   lcu8HdcPreBumDefaultTime;

uint8_t   lcu8HdcEnteringChkResult, lcu8HdcExitChkResult, lcu8HdcBumpEstTime;
uint16_t    lcu16HdcTimeToActCnt, lcu16HdcTimeToAct;
uint16_t    lcu16HdcObserver;
int16_t     lcs16HdcCtrlPGain, lcs16HdcCtrlDGain;
int16_t     lcs16HdcCtrlRoughPGain, lcs16HdcCtrlRoughDGain;

int16_t     lcs16HdcSmaThetaGTh, lcs16HdcMidThetaGTh, lcs16HdcBigThetaGTh,lcs16HdcExThetaGTh;
int16_t     lcs16HdcThetaG,lcs16HdcThetaGDot,lcs16HdcThetaGOld;
int16_t     lcs16HdcThetaG_org,lcs16HdcThetaG_orgOld;
int16_t     lcs16HdcThetaG_max;
int16_t     lcs16HdcThetaG1,lcs16HdcThetaGOld1,lcs16HdcASen1, lcs16HdcASenOld1;
int16_t     lcs16HdcThetaG0,lcs16HdcThetaGOld0,lcs16HdcASen0, lcs16HdcASenOld0;
int16_t     lcs16HdcDecelOld1,lcs16HdcDecel1;

int16_t     lcs16HdcThetaG_N,lcs16HdcThetaGOld_N;
int16_t     lcs16HdcDecel, lcs16HdcDecelOld, lcs16HdcASen, lcs16HdcASenOld, lcs16HdcDecel_d, lcs16HdcDecel_a;
int16_t     lcs16HdcDecellf,lcs16HdcDecellfOld;
int16_t     lcs16HdcThetaGSteer, lcs16HdcThetaGPitch;
int16_t     lcs16HdcCtrlInput, lcs16HdcTargetG, lcs16HdcTargetSpeed, lcs16HdcTargetSpeedOld;
int16_t     lcs16HdcCtrlError, lcs16HdcCtrlErrorOld, lcs16HdcCtrlErrorDot;
uint16_t    lcu16HdcClosingCnt,lcu16HdcClosingCnt2, lcu16HdcClosingTime;
int16_t     lcs16HdcPreCurMax;

uint8_t   lcu8HdcExcessSlip;
int16_t     lcs16HdcExcesslat;
int16_t     lcs16HdcVehicleAccel;
int16_t     lcs16HdcAx1000g;
int16_t     lcs16HdcWheelAx1000g;

int16_t     lcs16_vehicel_speed_old,lcs16_vehicel_speed_sum,lcs16_vehicel_speed_new;

uint8_t   lcu8hdcLongOffsetCnt;

uint8_t   lcu8HdcRightGearMode,lcu8HdcRightGearModeOld,lcu8HdcRightGearCnt;
int16_t     lcs16HdcRightGearFlgCnt;

/*      Threshold Factor Setting    */
int16_t     lcs16HdcYawFactorTh, lcs16HdcYawDotFactorTh;
int16_t     lcs16HdcEngienFactorTh,lcs16HdcEngienFactorTh2,lcs16HdcBrakeFactorTh;
int16_t     lcs16HdcSpeedFactorTh,lcs16HdcReenterTh,lcs16HdcAxOfsNoneTh;
int16_t     lcs16HdcSpeedFactor;
int16_t     lcs16HdcPitchFactor;
uint8_t   lcu8HdcmtpOffCnt2,lcu8HdcmpressOffCnt,lcu8HdcmtpCnt2,lcu8HdcmpressCnt;

/*      Average ThtaG   */
uint8_t   lcu8HdcSum_ThetaG_cnt;
uint8_t   lcu8HdcSum1S_ThetaG_cnt;
int16_t     lcs16HdcSum_ThetaG;
int16_t     lcs16Hdcavr200_ThetaG_Old5;
int16_t     lcs16Hdcavr200_ThetaG_Old4;
int16_t     lcs16Hdcavr200_ThetaG_Old3;
int16_t     lcs16Hdcavr200_ThetaG_Old2;
int16_t     lcs16Hdcavr200_ThetaG;
int16_t     lcs16HdcAvr1s_ThetaG;
int16_t     lcs16HdcAvr1s_ThetaG_old;
uint8_t   lcu8HdcAvr_ThetaG_cnt;

int16_t     lcs16HdcAvr_ThetaG;
/*~     Average ThtaG   */
int16_t     lcs16hdcLongOffsetAvr300MS2,lcs16hdcLongOffsetAvr300MS1,lcs16hdcLongOffsetAvr300MS;
int16_t     lcs16hdcLongOffsetAvrage;

uint8_t   lcu8hdcAccelCnt, lcu8hdcClutchSw_off_timer;
uint8_t   lcu8hdcVehicleDirction;
uint8_t   lcu8ThetaGDotCnt,lcu8hdcStopOnHillSusCnt;
int16_t     lcs16hdcLowSpeedCnt;

uint8_t   lcu8HdcVehicleDirbySensorCnt;

uint8_t   lcu8HdcThetaGModeOld,lcu8HdcThetaGMode,lcu8HdcRightThetaGCnt;
uint8_t   lcu8HdcThetaGMode1Cnt,lcu8HdcThetaGMode2Cnt;

uint8_t   lcu8hdcVehicleForwardcnt;
uint8_t   lcu8hdcVehicleBackwardcnt;

int16_t     turbine_rpm;
int16_t     ls16HdcDiffTurbineRPMlf_old,ls16HdcDiffTurbineRPMlf;
uint8_t   lcu8HdcEngineDrg,lcu8HdcEngineDrg_cnt;

uint8_t   lcu8HdcCreepSpeedCnt;

int16_t     lcs16HdcMaxDiscTemp;

uint8_t   lcu8ecsWLamp;
uint8_t   lcu8ecsDef;
uint8_t   lcu8ecsDiag;
uint8_t   lcu8ecsLevelChangeNA;
uint8_t   lcu8ecsLifting;
uint8_t   lcu8ecsLowering;
uint8_t   lcu8ecsREQHeight;
uint8_t   lcu8ecsREQLevel;
uint8_t   lcu8ecsACTHeight;
int16_t     ls16ecsHeighRL;
int16_t     ls16ecsHeighRR;

int16_t     lcs16HdcEcsLongGCompHeight;
int16_t     ls16ecsHeighAverage,ls16ecsHeighAverage_old;
uint8_t   lcu8HdcEcsSuspect_cnt,lcu8HdcEcsSuspect_cnt2,lcu8HdcEcsCompEnable;
int16_t     lcs16HdcEcsLongGComp;

uint8_t   lcu8HdcDetmpt;

int16_t     lcs16hdcFlatPressTH;

int16_t     lcs16hdcLongOffsetAvr3;
int16_t     lcs16hdcLongOffsetAvrold2;
int16_t     lcs16hdcLongOffsetAvrold1;
int16_t     lcs16hdcLongOffsetAvr;
int16_t     lcs16hdcLongOffsetAvrlfOld,lcs16hdcLongOffsetAvrlf;
int16_t     lcs16hdcLongOffsetSum;
int16_t     lcs16HdcSuspectLongGComp;
int16_t     lcs16hdcFlatByPressOffCnt;
uint8_t   lcu8HdcFlatPressModeCnt;
uint8_t   lcu8HdcLongGSuspectCnt;

int16_t     HDC_T_DATA9,HDC_T_DATA8,HDC_T_DATA7;

int16_t     lcs16HdcStandbyFlg_on_cnt, lcs16HdcStandbyFlg_off_cnt;
uint8_t   lcu8HdcStandbyFlg_K,lcu8HdcStandbyFlg_A;

int16_t     lcs16HdcActiveFlg_on_cnt, lcs16HdcActiveFlg_off_cnt;
uint8_t   lcu8HdcActiveFlg_K,lcu8HdcActiveFlg_A;

int16_t     lcs16HdcClosingFlg_on_cnt, lcs16HdcClosingFlg_off_cnt;
uint8_t   lcu8HdcClosingFlg_K,lcu8HdcClosingFlg_A;

int16_t     lcs16HdcTerminationFlg_on_cnt, lcs16HdcTerminationFlg_off_cnt;
uint8_t   lcu8HdcTerminationFlg_K,lcu8HdcTerminationFlg_A;

int16_t     lcs16HdcSwitchFlg_on_cnt, lcs16HdcSwitchFlg_off_cnt;
uint8_t   lcu8HdcStandbyFlg_K,lcu8HdcStandbyFlg_A;

uint8_t   lcu8HdcDecCtrlReq_off_cnt;
uint8_t   lcu8HdcDecCtrlReq_on_cnt;

int16_t     lcs16_hdc_tempW0,lcs16_hdc_tempW1,lcs16_hdc_tempW2,lcs16_hdc_tempW3,lcs16_hdc_tempW4;

int16_t     lcs16_hdc_tempW5,lcs16_hdc_tempW6,lcs16_hdc_tempW7,lcs16_hdc_tempW8,lcs16_hdc_tempW9;


uint8_t   lcu8_hdc_tempW0,lcu8_hdc_tempW1,lcu8_hdc_tempW2,lcu8_hdc_tempW3,lcu8_hdc_tempW4;
uint8_t   lcu8_hdc_tempW5,lcu8_hdc_tempW6,lcu8_hdc_tempW7,lcu8_hdc_tempW8,lcu8_hdc_tempW9;



  #endif

/* Local Function prototype ****************************************/
  #if __HDC
static void LCHDC_vChkInhibition(void);
static void LCHDC_vChkDriverAccelIntend(void);
static void LCHDC_vChkDriverBrakeIntend(void);
static void	LCHDC_vCheckMpressOn(void);
static void LCHDC_vChkDriverInput(void);
static void LCHDC_vGet_HWInform(void);
static void LCHDC_vChkPowerEngage(void);
static void LCHDC_vGetGearPosition_AT(void);
static void LCHDC_vGetGearPosition_MT(void);
static void LCHDC_vGetHDCSwitch(void);
static void LCHDC_vSetTargetSpeed(void);
static void LCHDC_vAdaptTargetSpeed(char16_t lcs16HdcTargetSpeedAtGear);
static void LCHDC_vSetTargetSpeed_AT(void);
static void LCHDC_vSetTargetSpeed_MT(void);
static void LCHDC_vChkECSComp(void);
static void LCHDC_vChkDiscTempConds(void);
static void LCHDC_vChkEnginestate(void);
static void LCHDC_vChkVehicleStopOnHill(void);
static void LCHDC_vChkVehicleDirINGear(void);
static void LCHDC_vChkVehicleDirNGear(void);
static void LCHDC_vChkVehicleDirThetag(void);
static void LCHDC_vChkVehicleDirYaw(void);
static void LCHDC_vChkVehicleDirection(void);
static void LCHDC_vChkVehicleDirection_MT(void);
static void LCHDC_vCompLongGSensor(void);
static void LCHDC_vChkEnteringConds(void);
static void LCHDC_vChkExitConds(void);
static void LCHDC_vEstSlope(void);
static void LCHDC_vCalSlopeMeasures(void);
static void LCHDC_vChkEngineDrag(void);
static void LCHDC_vChkDownhillAfterStop(void);
static void LCHDC_vChkOverCreepSpeed(void);
static void LCHDC_vCalVehicleAccel(void);
static void LCHDC_vEstDownhill(void);
static void LCHDC_vCalSlopeThreshold(void);
static void LCHDC_vCalSlopeThresholdFactor(void);
static void LCHDC_vCalSlopeThresholdFactorTh(void);
static void LCHDC_vEstFlat(void);
static void LCHDC_vEstBump(void);
static void LCHDC_vEstPreBump(void);
static void LCHDC_vEstFlatPressure(void);
static void LCHDC_vSpeedCtrl(void);
static void LCHDC_vCalSpeedError(void);
static void LCHDC_vFeedBackCtrl(void);
static void LCHDC_vFeedBackCtrlGain(void);
static void LCHDC_vFeedBackCtrlInput(void);
static void LCHDC_vSetTargetDecel(void);
static void LCHDC_vSetDecelCtrl(uint8_t,uint8_t,uint8_t,int16_t);
static void LCHDC_vSmoothClosingCtrl(void);
static void LCHDC_vResetCtrlVariable(void);
static void LCHDC_vSetCtrlModeCnt(void);
static void LCHDC_vChkTransmissionType(void);
  #if __TARGET_SPD_ADAPTATION
static void LCHDC_vAdaptTargetSpdByBrkAcc(void);
static void LCHDC_vCompTargetSpdEngStall(void);
static void LCHDC_vSetTargetSpdGearEngage(void);
static void LCHDC_vEstSlopeAfterRoll(void);

  #endif
  #endif

/* Implementation***************************************************/
  #if __HDC
void LCHDC_vCallControl(void)
{

    LCHDC_vChkTransmissionType();  
    LCHDC_vChkInhibition();
    LCHDC_vCheckMpressOn();
    LCHDC_vChkDriverAccelIntend();
    LCHDC_vChkDriverBrakeIntend();
    LCHDC_vChkDriverInput();
    LCHDC_vChkECSComp();
    LCHDC_vChkDiscTempConds();
    LCHDC_vChkVehicleDirection();
    LCHDC_vChkVehicleDirection_MT();    
    LCHDC_vCompLongGSensor();    


    LCHDC_vEstSlope();
      #if __TARGET_SPD_ADAPTATION
    LCHDC_vEstSlopeAfterRoll();
      #endif
    switch(lcs8HdcState)
    {
        /********************************************************/
        case S8_HDC_STANDBY:                /* STANDBY */
            lcu1HdcActiveFlg=0;
            LCHDC_vChkEnteringConds();
            if(lcu8HdcEnteringChkResult==U8_HDC_ENTER)
            {
                lcs8HdcState=S8_HDC_ACTIVATION;
                lcu1HdcActiveFlg=1;
                LCHDC_vSpeedCtrl();
            }
            else if(lcu8HdcEnteringChkResult==U8_HDC_READY)
            {
                lcs8HdcState=S8_HDC_STANDBY;
            }
            else
            {
                lcs8HdcState=S8_HDC_TERMINATION;
                LCHDC_vResetCtrlVariable();
            }
            break;

        /********************************************************/
        case S8_HDC_ACTIVATION:             /* ACTIVATION */
            lcu1HdcActiveFlg=1;
            LCHDC_vChkExitConds();
            if(lcu8HdcExitChkResult==U8_HDC_EXIT)
            {
                lcu1HdcActiveFlg=1;
                lcs8HdcState=S8_HDC_CLOSING;
              #if __HDC_FADE_OUT_CTRL
                if (lcu8HdcDriverAccelIntend==1)
                {
                    LCHDC_vSetDecelCtrl(0,0,0,0);
                    LCHDC_vSmoothClosingCtrl();
                }
                else
                {
                    lcs16_hdc_tempW9 =LCESP_s16IInter2Point(lcs16HdcClosingFlg_on_cnt,
                                                               (int16_t)L_U8_TIME_10MSLOOP_2S,   S16_HDC_FADE_OUT_CTRL_DECEL,
                                                                        L_U16_TIME_10MSLOOP_4S,  S16_HDC_FADE_OUT_CTRL_DECEL2);

                    LCHDC_vSetDecelCtrl(1,0,0,lcs16_hdc_tempW9);
                }
              #else
                LCHDC_vSetDecelCtrl(0,0,0,0);
                LCHDC_vSmoothClosingCtrl();
              #endif
            }
            else if(lcu8HdcExitChkResult==U8_HDC_READY)
            {
                lcu1HdcActiveFlg=0;
                lcs8HdcState=S8_HDC_STANDBY;
                LCHDC_vResetCtrlVariable();
            }
            else
            {
                lcs8HdcState=S8_HDC_ACTIVATION;
                LCHDC_vSpeedCtrl();
            }
            break;

        /********************************************************/
        case S8_HDC_CLOSING:                         /* CLOSING */
            lcu1HdcActiveFlg=1;
            lcu16HdcClosingTime=L_U8_TIME_10MSLOOP_700MS;
            if(((lcu8HdcDriverAccelIntend==1)&&(mtp>=MTP_60_P))
                ||(lcu8HdcDriverBrakeIntend==1)
                  #if __HDC_FADE_OUT_CTRL
                ||(vref>(int16_t)S16_HDC_SWITCH_ON_SPD)
                  #endif
                  #if __TCMF_CONTROL
                ||(lcs16HdcPreCurMax<=U16_HDC_NO_PRE_CUR))
                  #else
                ||(lcu16HdcClosingCnt>=lcu16HdcClosingTime))
                  #endif
            {
                lcu1HdcActiveFlg=0;
                lcs8HdcState=S8_HDC_TERMINATION;
                LCHDC_vResetCtrlVariable();
            }
            /* Issue List 2007-174 : HDC 제어 중 mtp 신호에 의해 경사 판단 chattering 발생  */
              #if defined(__dDCT_INTERFACE)
            else if((lcu1HdcSwitchFlg==1)||(lcu1HrcOnReqFlg==1)
              #else
            else if((lcu1HdcSwitchFlg==1)
              #endif
              #if __HDC_FADE_OUT_CTRL
                &&(lcu1HdcInhibitFlg==0)
              #endif
                &&(lcu8HdcDriverAccelIntend==0)
              #if __STOP_ON_HILL
                &&((lcu1HdcDownhillFlg==1)||(lcu8HdcGearPosition==U8_HDC_NEU_GEAR)||(lcu1HdcDownhillAfeterStopFlg==1))
              #else
                &&((lcu1HdcDownhillFlg==1)||(lcu8HdcGearPosition==U8_HDC_NEU_GEAR))
              #endif
              )
            {
                lcs8HdcState=S8_HDC_ACTIVATION;
                lcs8HdcSubState=0;
                lcu1HdcDownhillFlg=1;
                lcu16HdcClosingCnt=0;
                LCHDC_vSpeedCtrl();
            }
            else
            {
                lcs8HdcState=S8_HDC_CLOSING;
              #if __HDC_FADE_OUT_CTRL
                if (lcu8HdcDriverAccelIntend==1)
                {
                    LCHDC_vSmoothClosingCtrl();
                }
                else
                {
                    lcs16_hdc_tempW9 =LCESP_s16IInter2Point(lcs16HdcClosingFlg_on_cnt,
                                                                 (int16_t)L_U8_TIME_10MSLOOP_2S,   S16_HDC_FADE_OUT_CTRL_DECEL,
                                                                          L_U16_TIME_10MSLOOP_4S,   S16_HDC_FADE_OUT_CTRL_DECEL2);

                    LCHDC_vSetDecelCtrl(1,0,0,lcs16_hdc_tempW9);
                }
              #else
                LCHDC_vSmoothClosingCtrl();
              #endif
                lcu16HdcClosingCnt++;
            }

            break;

        /********************************************************/
        case S8_HDC_TERMINATION:                /* TERMINATION */
            lcu1HdcActiveFlg=0;
              #if defined(__dDCT_INTERFACE)
            if(((lcu1HdcSwitchFlg==1)||(lcu1HrcOnReqFlg==1))&&(lcu1HdcInhibitFlg==0))
              #else
            if((lcu1HdcSwitchFlg==1)&&(lcu1HdcInhibitFlg==0))
              #endif
            {
                lcs8HdcState=S8_HDC_STANDBY;
                LCHDC_vResetCtrlVariable();
            }
            else
            {
                lcs8HdcState=S8_HDC_TERMINATION;
            }
            break;

        /********************************************************/
        default:                            /* DEFAULT */
            lcs8HdcState=S8_HDC_TERMINATION;
            break;
    }
    /* Issue List 2007-107 : HDC 제어 중 HDC ON/Off 반복 시 압력 상승 늦음  */
    LCHDC_vSetCtrlModeCnt();
}


void LCHDC_vChkInhibition(void)
{
	#if __GM_FailM
	if((fu1OnDiag==1)||(init_end_flg==0)
	||(fu1MainCanLineErrDet==1)                                       /* Main CAN ERR */
	|| (fu1MCPErrorDet==1)                                             /* MP error */
	|| (fu1AxErrorDet==1)                                              /* Ax sensor error */
	|| (fu1ESCEcuHWErrDet==1)                                          /* ECU ERROR */
	|| (fu1VoltageLowErrDet==1)                                        /* voltage low */
	|| (fu1VoltageUnderErrDet==1)                                      /* voltage under */
	|| (fu1VoltageOverErrDet==1)                                       /* voltage over */                        
	|| (fu1WheelFLErrDet==1)                                           /* FL WSS error */ 
	|| (fu1WheelFRErrDet==1)                                           /* FR WSS error */
	|| (fu1WheelRLErrDet==1)                                           /* RL WSS error */
	|| (fu1WheelRRErrDet==1)                                           /* RR WSS error */
	|| (fu1SubCanLineErrDet==1)                                        /* CE Line Fail Time Out Error */
	|| (hs_subnet_config_err_flg==0)                                   /* Subnet config error */
	|| (fu1TCUTimeOutErrDet==1)                                        /* TCM Fail */
	|| (fu1EMSTimeOutErrDet==1)                                        /* ECM Fail */
	|| (fu1HDCSwitchFail==1)										   /* HDC switch fail */
	|| ((HDC_TM_TYPE==HDC_MANUAL_TM)&&((fsu1GearRSwitch_error_flg==1)||(fsu1ClutchSwitch_error_flg==1)))  /* M/T GEAR_R, CLUTCH SW */
      #if __BRAKE_FLUID_LEVEL==ENABLE
    ||(fu1DelayedBrakeFluidLevel==1)
	||(fu1LatchedBrakeFluidLevel==1)  	
      #endif
    || (wu8IgnStat==CE_OFF_STATE)  
      )
  	#else
    if((fu1OnDiag==1)||(init_end_flg==0)                               /* Initial check, on diag */
    || (wu8IgnStat==CE_OFF_STATE)                                      /* IGN off */
    || (hdc_error_flg==1)                                              /* Fail safe HDC error signal */
    || (fu1VoltageLowErrDet==1)                                        /* voltage low */
	|| (fu1VoltageUnderErrDet==1)                                      /* voltage under */
    )
  	#endif
    {
        lcu1HdcInhibitFlg=1;
      #if __HDC_FADE_OUT_CTRL
        if (lcs8HdcState != S8_HDC_CLOSING)
        {
            LCHDC_vResetCtrlVariable();
        }
        else { }
      #else
        LCHDC_vResetCtrlVariable();
      #endif
    }
    else
    {
        lcu1HdcInhibitFlg=0;
    }
}


void LCHDC_vChkDriverAccelIntend(void)
{
    lcu8_hdc_tempW9 = U8_HDC_ACCEL_ENTER;
    lcu8_hdc_tempW8 = U8_HDC_ACCEL_EXIT;

    if (lcu8_hdc_tempW8 < MTP_0_P )
    {
        lcu8_hdc_tempW8 = MTP_0_P;
    }
    else {}

    if (lcu8_hdc_tempW9 < lcu8_hdc_tempW8 + MTP_1_P)
    {
        lcu8_hdc_tempW9 = lcu8_hdc_tempW8 + MTP_1_P;
    }
    else {}

    if(lcu8HdcDriverAccelIntend==0)
    {
        if(mtp>=lcu8_hdc_tempW9)
        {
            if(lcu8HdcMtpCnt<L_U8_TIME_10MSLOOP_30MS)
            {
                lcu8HdcMtpCnt++;
            }
            else
            {
                lcu8HdcDriverAccelIntend=1;
                lcu8HdcMtpCnt=0;
                lcu8HdcToqSTBtimerAfterAccel=0;
            }
        }
        else
        {
            lcu8HdcDriverAccelIntend=0;
            lcu8HdcMtpCnt=0;
            if (lcu8HdcToqSTBtimerAfterAccel>0)
            {
            	lcu8HdcToqSTBtimerAfterAccel = lcu8HdcToqSTBtimerAfterAccel-1;
            }
            else { } 
        }
    }
    else
    {
        if(mtp<=lcu8_hdc_tempW8)
        {
            if(lcu8HdcMtpCnt<L_U8_TIME_10MSLOOP_30MS)
            {
                lcu8HdcMtpCnt++;
            }
            else
            {
                lcu8HdcDriverAccelIntend=0;
                lcu8HdcMtpCnt=0;
                lcu8HdcToqSTBtimerAfterAccel=(uint8_t)LCESP_s16IInter3Point(eng_torq_rel, 80, (int16_t)L_U8_TIME_10MSLOOP_100MS,
                															             150, (int16_t)L_U8_TIME_10MSLOOP_200MS,
                															             250, (int16_t)L_U8_TIME_10MSLOOP_300MS);
            }
        }
        else
        {
            lcu8HdcDriverAccelIntend=1;
            lcu8HdcMtpCnt=0;
        }
    }

    if(lcu8HdcDriverAccelIntend==1)
    {
        lcu8HdcMtpOffCnt = (uint8_t)LCESP_s16IInter2Point(mtp,  30, (int16_t)L_U8_TIME_10MSLOOP_300MS,
                                                               100, (int16_t)L_U8_TIME_10MSLOOP_1000MS);
    }
    else
    {
        if ((int16_t)eng_rpm > (int16_t)turbine_rpm)
        {
            if (lcu8HdcMtpOffCnt < L_U8_TIME_10MSLOOP_50MS)
            {
                lcu8HdcMtpOffCnt = L_U8_TIME_10MSLOOP_50MS;
            }
            else if (lcu8HdcMtpOffCnt>0)
            {
                lcu8HdcMtpOffCnt--;
            }
            else {}
        }
        else
        {
            if (lcu8HdcMtpOffCnt>0)
            {
                lcu8HdcMtpOffCnt--;
            }
            else {}
        }
    }

  #if __N_GEAR_HDC_Enable
    if (lcu8HdcGearPosition == U8_HDC_NEU_GEAR)
    {
        lcu8HdcDriverAccelIntend = 0;
    }
    else { }
  #endif

}

static void	LCHDC_vCheckMpressOn(void)
{
  #if (__BLS_NO_WLAMP_FM==ENABLE)
	if((fu1BlsSusDet==1)||(fu1BLSErrDet==1)) 
	{
		if(lcu1HdcMpressOn==0)
		{
			if(mpress >= (MPRESS_4BAR + HDC_MAX_MP_OFFSET_BLS_FAIL))
			{
				lcu1HdcMpressOn = 1;
			}
			else { }
		}
		else /* lcu1HdcMpressOn==1 */
		{
			if(mpress < (MPRESS_1BAR + HDC_MAX_MP_OFFSET_BLS_FAIL))
			{
				lcu1HdcMpressOn = 0;
			}
			else { }
		}
	}
	else
	{
		lcu1HdcMpressOn = MPRESS_BRAKE_ON;
	}
  #else
    lcu1HdcMpressOn = MPRESS_BRAKE_ON;
  #endif
}

void LCHDC_vChkDriverBrakeIntend(void)
{
    if(lcu8HdcDriverBrakeIntend==0)
    {
        if (lcu1HdcMpressOn == 1)
        {
            if(lcS16HdcBrkCnt<(int16_t)S16_HDC_BRAKE_EXIT_TIME)
            {
                lcS16HdcBrkCnt++;
            }
            else
            {
                lcu8HdcDriverBrakeIntend=1;
                lcS16HdcBrkCnt=0;
            }
        }
        else
        {
            lcu8HdcDriverBrakeIntend=0;
            lcS16HdcBrkCnt=0;
        }
    }
    else
    {
        if (lcu1HdcMpressOn == 0)
        {
            if(lcS16HdcBrkCnt<(int16_t)S16_HDC_BRAKE_ENTER_TIME)
            {
                lcS16HdcBrkCnt++;
            }
            else
            {
                lcu8HdcDriverBrakeIntend=0;
                lcS16HdcBrkCnt=0;
            }
        }
        else
        {
            lcu8HdcDriverBrakeIntend=1;
            lcS16HdcBrkCnt=0;
        }
    }

    if (lcu1HdcMpressOn==0)
    {
        lcs16_vehicel_speed_old = 0;
        lcs16_vehicel_speed_sum = 0;
        lcs16_vehicel_speed_new = 0;
    }
    else if (lcS16HdcBrkCnt < L_U8_TIME_10MSLOOP_30MS)
    {
        lcs16_vehicel_speed_sum = lcs16_vehicel_speed_sum + vref;
    }
    else if (lcS16HdcBrkCnt == L_U8_TIME_10MSLOOP_30MS)
    {
        lcs16_vehicel_speed_sum = lcs16_vehicel_speed_sum + vref;
        lcs16_vehicel_speed_old = lcs16_vehicel_speed_sum/L_U8_TIME_10MSLOOP_30MS;


        tempW2 = (int16_t)((((int32_t)288)*S16_HDC_BRAKE_EXIT_TIME)/L_U8_TIME_10MSLOOP_1000MS);
        lcs16_hdc_tempW9 = (int16_t)((((int32_t)tempW2)*lcs16HdcTargetG)/1000);



        lcs16_vehicel_speed_new = lcs16_vehicel_speed_old + lcs16_hdc_tempW9 - VREF_1_KPH;
    }
    else if (lcs16_vehicel_speed_new > vref)
    {
        lcs16_vehicel_speed_old = 0;
        lcs16_vehicel_speed_sum = 0;
        lcs16_vehicel_speed_new = 0;
        lcu8HdcDriverBrakeIntend=1;
        lcS16HdcBrkCnt=0;
    }
    else {}

}


void LCHDC_vChkDriverInput(void)
{

    /*update engine inform cnt per 20ms*/
	lcu8HdcEngUpdateCnt++;
	if (lcu8HdcEngUpdateCnt == L_U8_TIME_10MSLOOP_20MS)	
	{
		lcu8HdcEngUpdateCnt = 0;
	}

	LCHDC_vGet_HWInform();

  #if __POWER_ENGAGE_AT_CLUTCH_ON
	if (HDC_TM_TYPE==HDC_AUTO_TM)
	{
		;
	}
   	else
   	{
   		LCHDC_vChkPowerEngage();
	}
  #endif        

    if (HDC_TM_TYPE==HDC_AUTO_TM)
    {
        LCHDC_vGetGearPosition_AT();
    }
    else
    {
        LCHDC_vGetGearPosition_MT();
    }

    LCHDC_vGetHDCSwitch();

    LCHDC_vSetTargetSpeed();
}

static void LCHDC_vGet_HWInform(void)
{
    /*  Engine setting
        Sub-Function Out Put
            1. lcs16HdcEngIdleTorque
            2. lcs16HdcEngIdleRpm
            3. lcs16HdcEngStallRpm
    */

        lcs16HdcEngIdleTorque = S16_HDC_MT_ENG_IDLE_TORQ;     
        lcs16HdcEngIdleRpm    = S16_HDC_MT_ENG_IDLE_RPM;	
        lcs16HdcEngStallRpm   = S16_HDC_MT_ENG_IDLE_STALL_RPM;
        lcs16HdcEngOverRpm    = S16_HDC_MT_ENG_IDLE_TORQ_COMP;

/*************************************************************/
    if (HDC_TM_TYPE==HDC_AUTO_TM)
    {
      #if __TURBINE_RPM_ENABLE
        turbine_rpm = (int16_t)lespu16TCU_N_TC;
      #else
        turbine_rpm = 0;
      #endif
    }
    else
    {
        turbine_rpm = 0;
    }
/*************************************************************/
    /*  TM setting
        Sub-Function Out Put
            1. lcu1HdcDrivelineLowmodeFlg
            2. lcu1hdcGearR_Switch
            3. lcu1hdcClutchSwitch
            4. lcu1HdcMTAvailableSignalFlg
            5. lcu1HdcClutchIndependEngageFlg
    */

  #if __LOW_MODE_ENABLE_VEHICLE
    if(lespu1AWD_LOW_ACT==1)
    {
        lcu1HdcDrivelineLowmodeFlg=1;
    }
    else
    {
        lcu1HdcDrivelineLowmodeFlg=0;
    }
  #else
    lcu1HdcDrivelineLowmodeFlg=0;
  #endif


    if (HDC_TM_TYPE==HDC_AUTO_TM)
    {
        lcu1HdcMTAvailableSignalFlg =0;
        lcu1HdcClutchIndependEngageFlg =0;
    }
    else                                /* Gear 정보와 Clutch 정보 있는경우에만 */
    {
      #if (__GEAR_SWITCH_TYPE!=NONE)&&(__CLUTCH_SWITCH_TYPE!=NONE)
        lcu1HdcMTAvailableSignalFlg = 1;
        lcu1HdcClutchIndependEngageFlg = 1;
      #else
        lcu1HdcMTAvailableSignalFlg = 0;
        lcu1HdcClutchIndependEngageFlg = 0;
      #endif
    }

    /*
    Sub-Function InPut
        1. fu1GearR_Switch          R Gera In 조건          1
        2. fu1ClutchSwitch          동력 전달 해제 조건     1
    */
    /* Time Delay
        Gear Chagne에 필요한 최소시간 800ms
        Singal Chattering 150~ 300ms
    */

    if (lcu1HdcMTAvailableSignalFlg==1)
    {
        lcs16_hdc_tempW9 = L_U8_TIME_10MSLOOP_100MS;                                         /* Time Delay   : 연속작동 100ms 이상*/
        lcu1hdcClutchSwitch_old = lcu1hdcClutchSwitch;

        if (lcu1hdcGearR_Switch==0)
        {
            if(fu1GearR_Switch==1)
            {
                if (lcu8hdcGearR_Switch_cnt >= lcs16_hdc_tempW9)
                {
                    lcu1hdcGearR_Switch = 1;
                }
                else
                {
                    lcu8hdcGearR_Switch_cnt++;
                }
            }
            else
            {
                lcu8hdcGearR_Switch_cnt =0;
            }
        }
        else
        {
            if(fu1GearR_Switch==0)
            {
                if (lcu8hdcGearR_Switch_cnt >= lcs16_hdc_tempW9)
                {
                    lcu1hdcGearR_Switch = 0;
                }
                else
                {
                    lcu8hdcGearR_Switch_cnt++;
                }
            }
            else
            {
                lcu8hdcGearR_Switch_cnt =0;
            }
        }

        if (lcu1hdcClutchSwitch==0)
        {
            if(fu1ClutchSwitch==1)      	  
            {
                if (lcu8hdcClutchSwitch_cnt >= lcs16_hdc_tempW9)
                {
                    lcu1hdcClutchSwitch = 1;
                }
                else
                {
                    lcu8hdcClutchSwitch_cnt++;
                }
            }
            else
            {
                lcu8hdcClutchSwitch_cnt =0;
            }
        }
        else
        {
            if(fu1ClutchSwitch==0)
            {
                if (lcu8hdcClutchSwitch_cnt >= lcs16_hdc_tempW9)
                {
                    lcu1hdcClutchSwitch = 0;
                }
                else
                {
                    lcu8hdcClutchSwitch_cnt++;
                }
            }
            else
            {
                lcu8hdcClutchSwitch_cnt =0;
            }
        }
    }
    else
    {
        lcu1hdcGearR_Switch = 0;
        lcu1hdcClutchSwitch = 0;
    }

    if (lcu1hdcClutchSwitch==1)
    {
        if (lcu8hdcClutchSwitch_timer < L_U8_TIME_10MSLOOP_1000MS)
        {
            lcu8hdcClutchSwitch_timer++;
        }
        else { }
        
        lcu8hdcClutchSw_off_timer = 0;
    }
    else
    {
        if (lcu8hdcClutchSwitch_timer > 0)
        {
            lcu8hdcClutchSwitch_timer--;
        }
        else { }
        
        if(lcu8hdcClutchSw_off_timer < L_U8_TIME_10MSLOOP_1500MS)
        {
        	 lcu8hdcClutchSw_off_timer++;
        }
        else { }
    }
}

#if __POWER_ENGAGE_AT_CLUTCH_ON
static void LCHDC_vChkPowerEngage(void)
{
/*    Sub-Function InPut
        1. eng_rpm              Engine RPM
        2. eng_torq_rel         relative real engine torque(0~100%)
        3. fric_torq_rel	    relative friction torque(100%=255)

      Sub-Function Out Put
        1. lcu1hdcPowerEngageAtClutchSWon 					
        2. lcu1hdcPowerEngage 		
        */

	if (lcu8HdcEngUpdateCnt==0)	/*update engine inform per 3scan*/
	{
		lcs16HdcEngTorqDiff = (int16_t)(eng_torq_rel-lcs16HdcEngTorqOld);
		
		lcs16_hdc_tempW9 = (int16_t)eng_rpm - lcs16HdcEngrpmOld2;
		lcs16HdcEngrpmDotOld2 = lcs16HdcEngrpmDotOld;
		lcs16HdcEngrpmDotOld = lcs16HdcEngrpmDot;
		lcs16HdcEngrpmDot = LCESP_s16Lpf1Int(lcs16_hdc_tempW9, lcs16HdcEngrpmDotOld, L_U8FILTER_GAIN_10MSLOOP_10HZ);
		lcs16HdcEngrpmOld2 = lcs16HdcEngrpmOld;
		lcs16HdcEngrpmOld = (int16_t)eng_rpm;
		
		lcs16HdcEstEngrpmDotOld2 = lcs16HdcEstEngrpmDotOld;
		lcs16HdcEstEngrpmDotOld = lcs16HdcEstEngrpmDot;
		lcs16HdcEstEngrpmDot = (int16_t)((eng_torq_rel - fric_torq_rel)/10);
		
		lcs16HdcEngTorqOld = eng_torq_rel;
		
	}
	else { }

  	if (((lcu1hdcClutchSwitch==1)&&(lcu8hdcClutchSwitch_timer > L_U8_TIME_10MSLOOP_400MS))||((lcu8hdcClutchSw_off_timer>0)&&(lcu8hdcClutchSw_off_timer<L_U8_TIME_10MSLOOP_100MS)))
	{
		lcu1hdcPowerEngage = 0;
		if ((lcs16HdcEstEngrpmDot<(lcs16HdcEngIdleTorque/10)) && (lcs16HdcEngrpmDot>0))	/*estRPM & RPM phase condition*/
		{
			if ((lcs16HdcEstEngrpmDot-lcs16HdcEstEngrpmDotOld2 <= 0)&&(lcs16HdcEngrpmDot-lcs16HdcEngrpmDotOld2 > 0))	/*estRPM decrease rate & RPM rise rate condition*/
			{
				if (lcu8HdcEngEngageCnt1 > U8_HDC_MT_P_ENGAGE_GEAR_DOWN)
				{
					lcu1hdcPowerEngageAtClutchSWon = 1;
				}
				else
				{		
					if ((lcs16HdcEngrpmDot - lcs16HdcEstEngrpmDot) > S16_HDC_MT_DGEAR_RPM_DIFF_HIGH_REF)  
					{                                                     
	    				lcu8HdcEngEngageCnt1 = lcu8HdcEngEngageCnt1 + 5;
					}                                                     
					else if ((lcs16HdcEngrpmDot - lcs16HdcEstEngrpmDot) > S16_HDC_MT_DGEAR_RPM_DIFF_MID_REF)
					{
	               		lcu8HdcEngEngageCnt1 = lcu8HdcEngEngageCnt1 + 3;
	           		}
	           		else if ((lcs16HdcEngrpmDot - lcs16HdcEstEngrpmDot) > S16_HDC_MT_DGEAR_RPM_DIFF_LOW_REF)
	           		{
	               		lcu8HdcEngEngageCnt1 = lcu8HdcEngEngageCnt1 + 1;
	           		}
	           		else { }
	           	}
			}
			else { }
		}
		else 
		{
			if (lcu8HdcEngEngageCnt1 > U8_HDC_MT_P_ENGAGE_GEAR_DOWN) 
	       	{
				if (vref < VREF_2_KPH)
				{
					lcu1hdcPowerEngageAtClutchSWon = 0;
					lcu8HdcEngEngageCnt1 = 0;
				}
				else { }
			}
			else 
			{
				lcu8HdcEngEngageCnt1 = 0;
			} 
		}

		if ((lcs16HdcEstEngrpmDot>=(lcs16HdcEngIdleTorque/10)) && (lcs16HdcEngrpmDot<0))	/*estRPM & RPM phase condition*/
		{
			if ((lcs16HdcEstEngrpmDot-lcs16HdcEstEngrpmDotOld2 >= 0)&&(lcs16HdcEngrpmDot-lcs16HdcEngrpmDotOld2 < 0))	/*estRPM rise rate & RPM decrease rate condition*/
			{
				if (lcu8HdcEngEngageCnt2 > U8_HDC_MT_P_ENGAGE_GEAR_UP)
				{
					lcu1hdcPowerEngageAtClutchSWon = 1;
					
					if(lcu1hdcVehicleBackward==1) {lcs16_hdc_tempW8 = S16_HDC_MT_REV_TG_SPD;}
		        	else                          {lcs16_hdc_tempW8 = S16_HDC_MT_1ST_TG_SPD;}
		        	
		        	if ((lcu1HdcActiveFlg==1)&&(lcs16HdcTargetSpeed<lcs16_hdc_tempW8))
	           		{
	           			lcu1hdcEngStallSuspect2 = 1;
	           		}
	           		else { }
				}
				else
				{
					if ((lcs16HdcEstEngrpmDot - lcs16HdcEngrpmDot) > S16_HDC_MT_UGEAR_RPM_DIFF_HIGH_REF)
					{
	               		lcu8HdcEngEngageCnt2 = lcu8HdcEngEngageCnt2 + 5;
	           		}
					else if ((lcs16HdcEstEngrpmDot - lcs16HdcEngrpmDot) > S16_HDC_MT_UGEAR_RPM_DIFF_MID_REF)
					{
	               		lcu8HdcEngEngageCnt2 = lcu8HdcEngEngageCnt2 + 3;
	           		}
	           		else if ((lcs16HdcEstEngrpmDot - lcs16HdcEngrpmDot) > S16_HDC_MT_UGEAR_RPM_DIFF_LOW_REF)
	           		{
	               		lcu8HdcEngEngageCnt2 = lcu8HdcEngEngageCnt2 + 1;
	           		}
	           		else { }
	           	}
	       }
		   else { }
	    }
		else 
		{
			if (lcu8HdcEngEngageCnt2 > 0) 
	       	{
				if (vref < VREF_2_KPH)
				{
					lcu1hdcPowerEngageAtClutchSWon = 0;
					lcu8HdcEngEngageCnt2 = 0;
					lcu1hdcEngStallSuspect2 = 0;
				}
				else { }
			}
			else 
			{
				lcu8HdcEngEngageCnt2 = 0;
			} 
		}	    
    }
	else 
	{
		if (lcu1hdcPowerEngageAtClutchSWon==1)
		{
			lcu1hdcPowerEngage = 1;
		}
		else { }
		
		lcu8HdcEngEngageCnt1 = 0;
		lcu8HdcEngEngageCnt2 = 0;
		lcu1hdcPowerEngageAtClutchSWon = 0;
		if(lcu8hdcClutchSw_off_timer>=U8_HDC_MT_STALL_SUS_MAINTAIN_T)
		{
			lcu1hdcEngStallSuspect2 = 0;
		}
		else { }
	}

	if ((vref < VREF_2_KPH)||((lcu8HdcDriverBrakeIntend==1)&&(lcu1hdcClutchSwitch==1)&&(lcu8hdcClutchSwitch_timer > L_U8_TIME_10MSLOOP_400MS)))
	{
		lcu1hdcPowerEngage = 0;
		lcu1hdcPowerEngageAtClutchSWon = 0;
		lcu8HdcEngEngageCnt1 = 0;
		lcu8HdcEngEngageCnt2 = 0;
		lcu1hdcEngStallSuspect2 = 0;
	}
	else { }
	
	if(lcu1hdcEngStallSuspect2==1)
	{
		if(lcu8hdcEngStallSus2OnTimer<U8_HDC_MT_STALL_SUS_MAX_T)
		{
			lcu8hdcEngStallSus2OnTimer ++;
		}
		else
		{
			lcu1hdcEngStallSuspect2 = 0;
		}
	}
	else
	{
		lcu8hdcEngStallSus2OnTimer = 0;
	}
}
#endif

static void LCHDC_vGetGearPosition_AT(void)
{
    /*
        Sub-Function Out Put
            1. lcs16HdcRightGearFlgCnt
            2. lcu1HdcRightGearFlg
            3. lcu8HdcGearPosition
    */

    /********************************************************/
    /*
    gs_pos=(int16_t)lespu8TCU_TAR_GC;       TAR_GC target gc
    0:N,P 1-6:D 7:R
    gs_ena=(int16_t)lespu1TCU_SWI_GS;
    0:no gearchange 1:gearchange is active
    gs_sel=(int16_t)lespu8TCU_G_SEL_DISP;   G_SEL_DISP
    0:P 1:L 2:2 3:3 5:D 6:N 7:R 8:sport mode/manual
    */

    lcu8HdcRightGearModeOld = lcu8HdcRightGearMode;

    if(((gs_pos>=1)&&(gs_pos<=6))
        &&((gs_sel==1)||(gs_sel==2)||(gs_sel==3)||(gs_sel==5)||(gs_sel==8)))
    {
        lcu8HdcRightGearCnt++;
        lcu8HdcGearPosition=(uint8_t)gs_pos;
        lcu8HdcRightGearMode = 1;
    }
    else if ((gs_pos==7)&&(gs_sel==7))
    {
        lcu8HdcRightGearCnt++;
        lcu8HdcGearPosition  = U8_HDC_REV_GEAR;
        lcu8HdcRightGearMode = U8_HDC_REV_GEAR;
    }
    else if((gs_pos==0)&&(gs_sel==0))
    {
        lcu8HdcRightGearCnt++;
        lcu8HdcGearPosition  = U8_HDC_PARK_GEAR;
        lcu8HdcRightGearMode = U8_HDC_PARK_GEAR;
    }
    else if ((gs_pos==0)&&(gs_sel==6))
    {
        lcu8HdcRightGearCnt++;
        lcu8HdcGearPosition  = U8_HDC_NEU_GEAR;
        lcu8HdcRightGearMode = U8_HDC_NEU_GEAR;
    }
    else
    {
        lcu8HdcRightGearCnt=0;
        lcu8HdcGearPosition  =(uint8_t)gs_pos;
        lcu8HdcRightGearMode = 11;
    }

    if (lcu8HdcRightGearModeOld != lcu8HdcRightGearMode)
    {
        lcu8HdcRightGearCnt = 0;
        lcu1HdcRightGearFlg = 0;
    }
    else if (lcu8HdcRightGearCnt == 0)
    {
        lcu1HdcRightGearFlg = 0;
    }
    else if (lcu8HdcRightGearCnt > L_U8_TIME_10MSLOOP_200MS)
    {
        lcu8HdcRightGearCnt = L_U8_TIME_10MSLOOP_200MS;
        lcu1HdcRightGearFlg = 1;
    }
    else { }

    if (lcu1HdcRightGearFlg==1)
    {
        if (lcs16HdcRightGearFlgCnt < L_U16_TIME_10MSLOOP_60S)
        {
            lcs16HdcRightGearFlgCnt++;
        }
        else { }
    }
    else
    {
        lcs16HdcRightGearFlgCnt=0;
    }

}

static void LCHDC_vGetGearPosition_MT(void)
{
    /*
        Sub-Function Out Put
            1. lcs16HdcRightGearFlgCnt
            2. lcu1HdcRightGearFlg
            3. lcu8HdcGearPosition
    */
    /********************************************************/

    if (lcu1HdcMTAvailableSignalFlg==1)
    {
        if (vref <VREF_1_KPH)                                                               /* 오판의 가능성 없음.	*/
        {
            lcu8HdcGearPosition=U8_HDC_NEU_GEAR;
            lcu1HdcRightGearFlg=1;
        }
        else if (lcu1hdcClutchSwitch==1)                                                    /* 오판의 가능성 없음. (Clutch Fail 시 무관 - 단 N 판단 느림)	*/
        {
            lcu8HdcGearPosition=U8_HDC_NEU_GEAR;
            lcu1HdcRightGearFlg=0;      /* 차량 움직임.	*/
        }
        else if ((lcu1hdcClutchSwitch==0) && (lcu1hdcMTGearSusFlag==1))                     /* N 후진 밀리는 경우.	*/
        {
            lcu8HdcGearPosition=U8_HDC_NEU_GEAR;
            lcu1HdcRightGearFlg=1;
        }
        else if ((lcu1hdcClutchSwitch==0) && (lcu1hdcGearR_Switch == 1))                    /* R Gear Fail시 오판 가능.	*/
        {
            lcu8HdcGearPosition=U8_HDC_REV_GEAR;
            lcu1HdcRightGearFlg=1;
        }
        else if ((lcu8hdcClutchSwitch_timer <= L_U8_TIME_10MSLOOP_400MS)&&(lcu1hdcPreviousGearOkFlg==1))     /*오판 가능. 이전 기어 정보에 따라 다름 오판 가능.	*/
        {
    /*      lcu8HdcGearPosition=U8_HDC_NEU_GEAR;            이전 정보 유지.	*/
            lcu1HdcRightGearFlg=1;
        }
/*        else if ((lcu1hdcClutchSwitch==0) && (gear_state >= 1 )&&(gear_state <= 6 ) && (lcu1hdcVehicleBackwardThetaG==0) )        오판 가능. 전진만 판단.  Gear N이면 판단 불가.HDC 안들어오면.??	*/
        else if ((lcu1hdcClutchSwitch==0) && (gear_state >= 1 )&&(gear_state <= 6 ))	/*&&(lcu1hdcEngEngageOkFlg==1))         오판 가능. 전진만 판단.  Gear N이면 판단 불가.HDC 안들어오면.??	*/
        {
		  #if (__STOP_ON_HILL)&&(__POWER_ENGAGE_AT_CLUTCH_ON)
			if (lcu1hdcPowerEngage==0)
        	{
        		lcu8HdcGearPosition=(uint8_t)U8_HDC_NEU_GEAR;
            	lcu1HdcRightGearFlg=0;
            }        		
        	else
        	{
	        	lcu8HdcGearPosition=(uint8_t)gear_state;      
            	lcu1HdcRightGearFlg=1;
            }     
		 #else
	        	lcu8HdcGearPosition=(uint8_t)gear_state;      
            	lcu1HdcRightGearFlg=1;
		 #endif               	
        }
        else if ((lcu1hdcClutchSwitch==0) && (gear_state == 7 ) && (lcu1hdcGearR_Switch == 0))
        {
            lcu8HdcGearPosition=U8_HDC_NEU_GEAR;                                            /* ? Gear 정보 넘어오나..? (전진인데 후진신호)	*/
            lcu1HdcRightGearFlg=0;
        }
        else                                                                                /* 믿을수 없는 조건.	*/
        {
            lcu8HdcGearPosition=(uint8_t)gear_state;
            lcu1HdcRightGearFlg=0;
        }
    }
    else
    {
      #if __CAR==FORD_C214
        lcu1HdcRightGearFlg=1;
        if(gs_pos==14)
        {
            lcu8HdcGearPosition=U8_HDC_REV_GEAR;
        }
        else
        {
            lcu8HdcGearPosition=(uint8_t)gear_state;
        }
      #else
        lcu1HdcRightGearFlg=1;
        if(BACK_DIR==1)
        {
            lcu8HdcGearPosition=U8_HDC_REV_GEAR;
        }
        else
        {
            lcu8HdcGearPosition=(uint8_t)gear_state;
        }
      #endif
    }

    /****************************************************************/

    lcs16_hdc_tempW9 = L_U8_TIME_10MSLOOP_600MS;                                                     /* 이전 Gear 정보 감지 시간     */

    if (lcu1hdcPreviousGearOkFlg ==0)
    {
        if ((lcu1hdcClutchSwitch_old ==0)&&(lcu1hdcClutchSwitch==0)
            &&(lcs16HdcRightGearFlgCnt>lcs16_hdc_tempW9))                   /* Clutch 변화 없이 일정시간 감지시 Set	*/
        {
            if (lcu1HdcClutchIndependEngageFlg == 1 )                       /*  half Clutch 조건 보완				*/
            {
                lcu1hdcPreviousGearOkFlg =1;
            }
            else
            {
                lcu1hdcPreviousGearOkFlg =0;
            }
        }
        else { }
    }
    else                                                                    /* Reset 조건 보강 필요함.				*/
    {
        if (lcu8hdcClutchSwitch_timer > L_U8_TIME_10MSLOOP_400MS)
        {
            lcu1hdcPreviousGearOkFlg =0;
        }
        else { }
    }

    /*************************************************/

    if (lcu1HdcRightGearFlg==1)
    {
        if (lcs16HdcRightGearFlgCnt < L_U16_TIME_10MSLOOP_60S)
        {
            lcs16HdcRightGearFlgCnt++;
        }
        else { }
    }
    else
    {
        lcs16HdcRightGearFlgCnt=0;
    }
    /****************************************************************/

    lcs16_hdc_tempW9 = L_U8_TIME_10MSLOOP_500MS*10;                      /* Gear 정보 감지 시간     */

    if (lcu1hdcEngEngageOkFlg ==0)
    {
        if ((lcu8hdcClutchSwitch_timer < L_U8_TIME_10MSLOOP_400MS)
            &&(gear_state >= 1 )&&(gear_state <= 6 )
            &&(vref > VREF_2_KPH))
        {
            if (eng_torq_rel >= (int16_t)lcs16HdcEngIdleTorque-50)
            {
                lcs16hdcEngEngageOkCnt++;
                if (eng_torq_rel > lcs16HdcEngIdleTorque + 100)
                {
                    lcs16hdcEngEngageOkCnt = lcs16hdcEngEngageOkCnt + 7;
                }
                else { }
            }
            else { }

            if ((int16_t)eng_rpm > lcs16HdcEngIdleRpm-100)
            {
                lcs16hdcEngEngageOkCnt++;
                if ((int16_t)eng_rpm > lcs16HdcEngIdleRpm + 300)
                {
                    lcs16hdcEngEngageOkCnt = lcs16hdcEngEngageOkCnt + 7;
                }
                else { }
            }
            else { }
        }
        else
        {
            lcs16hdcEngEngageOkCnt=0;
        }

        if (lcs16hdcEngEngageOkCnt > lcs16_hdc_tempW9)
        {
            lcu1hdcEngEngageOkFlg = 1;
            lcs16hdcEngEngageOkCnt = 0;
        }
        else { }

    }
    else                                                                    /* Reset 조건 보강 필요함.	*/
    {
        if ((lcu8hdcClutchSwitch_timer > L_U8_TIME_10MSLOOP_400MS)
            ||(gear_state < 1 )||(gear_state > 7 )
            ||(vref < VREF_2_KPH))
        {
            lcu1hdcEngEngageOkFlg = 0;
            lcs16hdcEngEngageOkCnt = 0;
        }
        else { }
    }
}

static void LCHDC_vGetHDCSwitch(void)
{
  #if ((__HDC_SW_TYPE==ANALOG_TYPE)||(__HDC_SW_TYPE==CAN_TYPE))      /* Set Switch Input */
	lcu1HdcSwitchFlg = fu1HDCEnabledBySW;  
  #elif defined(__CAN_SW)
	lcu1HdcSwitchFlg = HDC_CAN_SWITCH;   

  #else
   #if __HDC_FORCED_SW_ON
    lcu1HdcSwitchFlg=1;
   #elif !SIM_MATLAB
    lcu1HdcSwitchFlg=0;
   #endif
  #endif

   #if __HDC_FORCED_SW_ON
    lcu1HdcSwitchFlg=1;
   #endif
}

static void LCHDC_vSetTargetSpeed(void)
{
	/* Set Target Speed */
    /*
        Sub-Function Out Put
            1. lcs16HdcTargetSpeed
            2. lcs16HdcTargetSpeedOld
            3. lcu1HdcTargetSpeedUpFlg
            4. lcu1HdcTargetSpeedDownFlg
    */

    lcs16HdcTargetSpeedOld = lcs16HdcTargetSpeed;

  #if defined(__CAN_SW)
    if(HDC_TM_TYPE==HDC_AUTO_TM)
    {
        lcu8HdcTgSpdSetMode=U8_HDC_AT_TG_SPD_SET_MODE;
    }
    else
    {
        lcu8HdcTgSpdSetMode=U8_HDC_MT_TG_SPD_SET_MODE;
    }
  #else
    lcu8HdcTgSpdSetMode = 0;
  #endif

    if(lcu8HdcTgSpdSetMode==1)
    {
      #if defined(__CAN_SW)
        lcs16HdcTargetSpeed=HDC_Target_Speed_Input*VREF_1_KPH; 
        lcs16_hdc_tempW8 = (int16_t)S16_HDC_MIN_TARGET_SPD_AT;   
        lcs16_hdc_tempW9 = (int16_t)S16_HDC_MAX_TARGET_SPD_AT;   
        
        lcs16HdcTargetSpeed = (lcs16HdcTargetSpeed>lcs16_hdc_tempW9)?lcs16_hdc_tempW9:lcs16HdcTargetSpeed;
        lcs16HdcTargetSpeed = (lcs16HdcTargetSpeed>lcs16_hdc_tempW8)?lcs16HdcTargetSpeed:lcs16_hdc_tempW8;            

        lcu1HdcTargetSpeedUpFlg   = (lcs16HdcTargetSpeed > lcs16HdcTargetSpeedOld)? 1 : 0;
        lcu1HdcTargetSpeedDownFlg = (lcs16HdcTargetSpeedOld > lcs16HdcTargetSpeed)? 1 : 0;        	
      #endif
    }
    else
    {
      #if __TARGET_SPD_ADAPTATION	
        LCHDC_vAdaptTargetSpdByBrkAcc();
      #else
        if(HDC_TM_TYPE==HDC_AUTO_TM)
        {
            LCHDC_vSetTargetSpeed_AT();
        }
        else
        {
            LCHDC_vSetTargetSpeed_MT();
        }
        
        if(HDC_TM_TYPE==HDC_AUTO_TM)
        {
        	lcs16_hdc_tempW8 = (int16_t)S16_HDC_MIN_TARGET_SPD_AT;
        	lcs16_hdc_tempW9 = (int16_t)S16_HDC_MAX_TARGET_SPD_AT;
        }
        else
        {
        	lcs16_hdc_tempW8 = (int16_t)S16_HDC_MIN_TARGET_SPD_MT;
            lcs16_hdc_tempW9 = (int16_t)S16_HDC_MAX_TARGET_SPD_MT;	
        }
           
        lcs16HdcTargetSpeed = (lcs16HdcTargetSpeed>lcs16_hdc_tempW9)?lcs16_hdc_tempW9:lcs16HdcTargetSpeed;
        lcs16HdcTargetSpeed = (lcs16HdcTargetSpeed>lcs16_hdc_tempW8)?lcs16HdcTargetSpeed:lcs16_hdc_tempW8;     
      #endif
    }
}

  #if __TARGET_SPD_ADAPTATION
static void LCHDC_vAdaptTargetSpdByBrkAcc(void)
{
  #if defined(__dDCT_INTERFACE)
	if(lcu1HrcOnReqFlg==1)
	{
		lcs16HdcTargetSpeed = lcs16HrcTargetSpd;
	}
	else
	{
  #endif		
    	if(HDC_TM_TYPE==HDC_AUTO_TM)
    	{
    	    lcs16_hdc_tempW1 = S16_HDC_MIN_TARGET_SPD_AT;  /* S16_HDC_AT_FOR_TG_SPD : AT min Target Spd */
    	    lcs16_hdc_tempW2 = S16_HDC_MAX_TARGET_SPD_AT;  /* S16_HDC_AT_NEU_TG_SPD : AT max Target Spd */
    	}
    	else
    	{
    	    lcs16_hdc_tempW1 = S16_HDC_MIN_TARGET_SPD_MT;  /* S16_HDC_MT_1ST_TG_SPD : MT min Target Spd */
    	    lcs16_hdc_tempW2 = S16_HDC_MAX_TARGET_SPD_MT;  /* S16_HDC_MT_2ND_TG_SPD : MT max Target Spd */
    	}
    	
    	if((lcu1HdcActiveFlg==1)&&(lcs16HdcActiveFlg_on_cnt==1))
    	{
    	    if(vref<lcs16_hdc_tempW1)
    	    {
    	        lcs16HdcTargetSpeed = lcs16_hdc_tempW1;
    	    }
    	    else if(vref<lcs16_hdc_tempW2)
    	    {
    	        lcs16HdcTargetSpeed = vref;
    	    }
    	    else
    	    {
    	        lcs16HdcTargetSpeed = lcs16_hdc_tempW2;
    	    }
    	}
    	else if(lcu1HdcActiveFlg==1)
    	{
    	    if(lcu8HdcDriverAccelIntend==1)
    	    {
    	        lcs16_hdc_tempW8 = LCESP_s16IInter3Point(mtp, (int16_t)U8_HDC_TAR_SPD_REF_LOW_ACC,  S16_HDC_TAR_SPD_GAP_AT_LOW_ACC,
    	      	                                              (int16_t)U8_HDC_TAR_SPD_REF_MED_ACC,  S16_HDC_TAR_SPD_GAP_AT_MED_ACC,
    	                                                      (int16_t)U8_HDC_TAR_SPD_REF_HIG_ACC,  S16_HDC_TAR_SPD_GAP_AT_HIG_ACC);
    	
    	        if(lcs16_hdc_tempW8>VREF_3_KPH)
    	        {
    	        	lcs16_hdc_tempW8 = VREF_3_KPH;
    	        }
    	        else if(lcs16_hdc_tempW8<VREF_0_KPH)
    	        {
    	        	lcs16_hdc_tempW8 = VREF_0_KPH;
    	        }
    	        else { }
    	        
    	        lcs16HdcTargetSpeed = vref + lcs16_hdc_tempW8; /* for dump at accel.*/
    	        if(lcs16HdcTargetSpeed<=lcs16_hdc_tempW1)
    	        {
    	            lcs16HdcTargetSpeed = lcs16_hdc_tempW1;
    	        }
    	        else if(lcs16HdcTargetSpeed>=lcs16_hdc_tempW2)
    	        {
    	            lcs16HdcTargetSpeed = lcs16_hdc_tempW2;
    	        }
    	        else
    	        {
    	            ;
    	        }  
    	    }
    	    else if(lcu8HdcDriverBrakeIntend==1)
    	    {
    	        lcs16HdcTargetSpeed = vref;
    	        if(lcs16HdcTargetSpeed<=lcs16_hdc_tempW1)
    	        {
    	            lcs16HdcTargetSpeed = lcs16_hdc_tempW1;
    	        }
    	        else if(lcs16HdcTargetSpeed>=lcs16_hdc_tempW2)
    	        {
    	            lcs16HdcTargetSpeed = lcs16_hdc_tempW2;
    	        }
    	        else
    	        {
    	            ;
    	        }  
    	    }
    	    else
    	    {
    	    	if(HDC_TM_TYPE==HDC_AUTO_TM)
    	    	{
    	    		;
    	    	}
    	    	else /* M/T */
    	    	{
    	      	    LCHDC_vCompTargetSpdEngStall();
    	      	    LCHDC_vSetTargetSpdGearEngage();
    	      	    
    	      	    if(lcs16HdcTargetSpeed > S16_HDC_MAX_TARGET_SPD_MT)
			        {
			        	lcs16HdcTargetSpeed = S16_HDC_MAX_TARGET_SPD_MT;
			        }
			        else if((lcs16HdcTargetSpeed - vref)>=VREF_2_KPH)
			        {
			        	lcs16HdcTargetSpeed = lcs16HdcTargetSpeedOld;
			        }
			        else
			        {
			        	;
			        }
			        
			        if(lcu1hdcVehicleForward==1)
			        {
			        	lcs16_hdc_tempW3 = S16_HDC_MT_1ST_TG_SPD;
    	      		}
			        else if(lcu1hdcVehicleBackward==1)
			        {
			        	lcs16_hdc_tempW3 = S16_HDC_MT_REV_TG_SPD;
			        }
			        else
			        {
			        	lcs16_hdc_tempW3 = S16_HDC_MT_1ST_TG_SPD;
			        }
			        	
			        if((lcu1hdcEngStallSuspect2==1)&&(lcs16HdcTargetSpeedOld<lcs16_hdc_tempW3))
		            {
			            lcs16HdcTargetSpeed = lcs16_hdc_tempW3;
		            }
		            else { }
    	      	}
    	    }
    	    
			if(lcu1hdcEngStallSuspect==0)
			{
				if((lcu1hdcEngStallSuspect2==1)||((lcu8hdcClutchSw_off_timer<L_U8_TIME_10MSLOOP_500MS)&&(lcu1HdcMtTsCompByRPM==1)))
				{
					lcu1hdcEngStallSuspect = 1;
					lcu8hdcEngStallSusOntimer ++;
				}
				else { }
			}
			else
			{
				if(lcu8hdcEngStallSusOntimer<L_U8_TIME_10MSLOOP_1000MS) {lcu8hdcEngStallSusOntimer ++;}
				else {lcu8hdcEngStallSusOntimer = L_U8_TIME_10MSLOOP_1000MS;}
    	
				if((lcu1hdcEngStallSuspect2==0)&&((lcu8hdcClutchSw_off_timer>=L_U8_TIME_10MSLOOP_500MS)||(lcu8hdcEngStallSusOntimer>=L_U8_TIME_10MSLOOP_1000MS)))
				{
					lcu1hdcEngStallSuspect = 0;
					lcu8hdcEngStallSusOntimer = 0;
				}
				else { }
    	    }
    	}
    	else
    	{
    		lcs16HdcTargetSpeed = VREF_0_KPH;
    		lcu1hdcEngStallSuspect = 0;
    	}
  #if defined(__dDCT_INTERFACE)    	
    }
  #endif    
    	

    if(lcu8HdcDriverBrakeIntend==1)
    {
        lcu1HdcNoForcedHoldAtBrk = 1;
    }
    else
    {
        lcu1HdcNoForcedHoldAtBrk = 0;
    }
}

static void LCHDC_vCompTargetSpdEngStall(void)
{
	lcu1HdcMtTsCompByRPM = 0;
   	if (((lcs16HdcActiveFlg_off_cnt<L_U8_TIME_10MSLOOP_1000MS)||(lcu1HdcActiveFlg==1))&&(eng_rpm>100)&&(lcu1hdcClutchSwitch==0)&&(ABS_fz==0))      
 	{
	    if ((int16_t)eng_rpm < (int16_t)((((int32_t)lcs16HdcEngStallRpm)*U8_HDC_RAPID_STALL_RPM_RATIO)/10))
    	{
    		if (lcu8HdcEngUpdateCnt==0)
    		{                                                                                 
	    		lcs16HdcTargetSpeed = lcs16HdcTargetSpeed + 2;   
	    		lcu1HdcMtTsCompByRPM = 1;
	    	}
	    	else { }
		}                   
		else if((lcs16HdcEngrpmDot < -20) && ((int16_t)eng_rpm < (lcs16HdcEngStallRpm+S16_HDC_TS_RPMC_FAST_RPM_DW)))
		{
    		if (lcu8HdcEngUpdateCnt==0)
    		{                                                                                 
	    		lcs16HdcTargetSpeed = lcs16HdcTargetSpeed + 2;   
	    		lcu1HdcMtTsCompByRPM = 1;
	    	}
	    	else { }
		}
	    else if (((lcs16HdcEngrpmDot < -10) && ((int16_t)eng_rpm < (lcs16HdcEngStallRpm+S16_HDC_TS_RPMC_MID_RPM_DW))) || ((int16_t)eng_rpm < lcs16HdcEngStallRpm))  
    	{
    		if (lcu8HdcEngUpdateCnt==0)                                                                                 
        	{
        		lcs16HdcTargetSpeed = lcs16HdcTargetSpeed + 1;   
        		lcu1HdcMtTsCompByRPM = 1;                           
			}
			else { }
		}                                                                               
		else { }
	}
	else { }
	
}

static void LCHDC_vSetTargetSpdGearEngage(void)
{
	lcu1HdcMtTsCompByInGear = 0;
	
    if(lcu1hdcVehicleForward==1)
    {
        if(lcu8HdcGearPosition==U8_HDC_1ST_GEAR)
        {
        	if((lcu1HdcMtTsCompByRPM==0)&&(lcs16HdcTargetSpeed<S16_HDC_MT_1ST_TG_SPD))   
        	{
                lcs16HdcTargetSpeed = lcs16HdcTargetSpeed + 1;
                lcu1HdcMtTsCompByInGear = 1;
            }
            else 
            {
            	;
            }
        }
        else if(lcu8HdcGearPosition==U8_HDC_2ND_GEAR)
        {
        	if((lcu1HdcMtTsCompByRPM==0)&&(lcs16HdcTargetSpeed<S16_HDC_MT_2ND_TG_SPD))
        	{
                lcs16HdcTargetSpeed = lcs16HdcTargetSpeed + 1;
                lcu1HdcMtTsCompByInGear = 1;
            }
            else 
            {
            	;
            }
        }
        else if(lcu8HdcGearPosition==U8_HDC_3RD_GEAR)
        {
        	if((lcu1HdcMtTsCompByRPM==0)&&(lcs16HdcTargetSpeed<S16_HDC_MT_3RD_TG_SPD))
        	{
                lcs16HdcTargetSpeed = lcs16HdcTargetSpeed + 1;
                lcu1HdcMtTsCompByInGear = 1;
            }
            else 
            {
            	;
            }
        }  
        else if((lcu8HdcGearPosition>=U8_HDC_4TH_GEAR)&&(lcu8HdcGearPosition!=U8_HDC_REV_GEAR)&&(lcu8HdcGearPosition!=U8_HDC_PARK_GEAR))
        {
        	if((lcu1HdcMtTsCompByRPM==0)&&(lcs16HdcTargetSpeed<S16_HDC_MAX_TARGET_SPD_MT))
        	{
                lcs16HdcTargetSpeed = lcs16HdcTargetSpeed + 1;
                lcu1HdcMtTsCompByInGear = 1;
            }
            else 
            {
            	;
            }
        }               
        else
        {
        	;
        }
    }
    else if(lcu1hdcVehicleBackward==1)   
    {
    	if(lcu8HdcGearPosition==U8_HDC_REV_GEAR)
    	{
    	    if((lcs16HdcTargetSpeed==lcs16HdcTargetSpeedOld)&&(lcs16HdcTargetSpeed<S16_HDC_MT_REV_TG_SPD))    
    	    {
    	    	lcs16HdcTargetSpeed = lcs16HdcTargetSpeed + 1;
    	    	lcu1HdcMtTsCompByInGear = 1;
    	    }
            else 
            {
            	;
            }
        }
        else
        {
        	;
        }
    }
    else
    {
    	;
    }	
}
  #endif

static void LCHDC_vSetTargetSpeed_AT(void)
{

    if(lcu1HdcDrivelineLowmodeFlg==1)
    {
        if(lcu1hdcVehicleBackward==1)
        {
            lcs16HdcTargetSpeed=S16_HDC_AT_REV_TG_SPD_4L;
        }
        else if(lcu1hdcVehicleForward==1)
        {
            lcs16HdcTargetSpeed=S16_HDC_AT_FOR_TG_SPD_4L;
        }
        else
        {
            if (lcu1hdcVehicleBackwardSus1Flg==1)
            {
                lcs16HdcTargetSpeed=S16_HDC_AT_REV_TG_SPD_4L;
            }
            else if (lcu1hdcVehicleForwardSus1Flg==1)
            {
                lcs16HdcTargetSpeed=S16_HDC_AT_FOR_TG_SPD_4L;
            }
            else    /* default forward  */
            {
                lcs16HdcTargetSpeed=(S16_HDC_AT_FOR_TG_SPD_4L + S16_HDC_AT_REV_TG_SPD_4L)/2;
            }
        }
    }
    else
    {
        if(lcu1hdcVehicleBackward==1)
        {
            lcs16HdcTargetSpeed=S16_HDC_AT_REV_TG_SPD;
        }
        else if(lcu1hdcVehicleForward==1)
        {
            lcs16HdcTargetSpeed=S16_HDC_AT_FOR_TG_SPD;
        }
        else
        {
            if (lcu1hdcVehicleBackwardSus1Flg==1)
            {
                lcs16HdcTargetSpeed=S16_HDC_AT_REV_TG_SPD;
            }
            else if (lcu1hdcVehicleForwardSus1Flg==1)
            {
                lcs16HdcTargetSpeed=S16_HDC_AT_FOR_TG_SPD;
            }
            else    /* default forward  */
            {
                lcs16HdcTargetSpeed=(S16_HDC_AT_FOR_TG_SPD + S16_HDC_AT_REV_TG_SPD)/2;
            }
        }
    }
	lcs16HdcTargetSpeed = (VREF_20_KPH < lcs16HdcTargetSpeed)? VREF_20_KPH:lcs16HdcTargetSpeed;        	        	
}

static void LCHDC_vSetTargetSpeed_MT(void)
{
  	if (lcu8HdcGearPosition>0)
  	{
  		if (lcu8HdcChkGearInformCnt > L_U8_TIME_10MSLOOP_500MS)
  		{
			lcu8HdcGearPositionOld = lcu8HdcGearPosition;
		}
  		else
  		{
  			lcu8HdcChkGearInformCnt = lcu8HdcChkGearInformCnt + 1;
  		}
  	}
  	else 
  	{
  		lcu8HdcChkGearInformCnt =0;
  		if (vref<VREF_1_KPH)
  		{
  			lcu8HdcGearPositionOld = 0;
  		}
  		else { }
  	}

    if (lcu8HdcTargetSpeedcnt>0)
    {
    	lcu8HdcTargetSpeedcnt --;
    }
    else { }

    if (lcu1HdcDrivelineLowmodeFlg==1)
    {
        /* 전진시에만 rpm 보상적용이 필요함. */
        if (lcu1hdcVehicleBackward ==1)
        {
            if (((int16_t)eng_rpm < lcs16HdcEngStallRpm)&&(lcs16HdcTargetSpeed<S16_HDC_MT_REV_TG_SPD_4L+VREF_1_KPH)&&(lcu1HdcActiveFlg==1))
            {
                if (lcu8HdcEngUpdateCnt==0)
            	{
                	lcs16HdcTargetSpeed = lcs16HdcTargetSpeed +1;
                }
                else { }
            }
            else
            {
                lcs16HdcTargetSpeed=S16_HDC_MT_REV_TG_SPD_4L;
            }
        }
        else if (lcu1hdcVehicleForward ==1)
        {
            /* 제어 진입시에만 Target Spd 보상이 필요함.*/

            if(lcu8HdcGearPosition==U8_HDC_1ST_GEAR)
            {
                lcs16HdcTargetSpeed=S16_HDC_MT_1ST_TG_SPD_4L;
            }
            else if(lcu8HdcGearPosition==U8_HDC_2ND_GEAR)
            {
                lcs16HdcTargetSpeed=S16_HDC_MT_2ND_TG_SPD_4L;
            }
            else if(lcu8HdcGearPosition==U8_HDC_3RD_GEAR)
            {
                lcs16HdcTargetSpeed=S16_HDC_MT_3RD_TG_SPD_4L;
            }
            else if(lcu8HdcGearPosition==U8_HDC_4TH_GEAR)
            {
                lcs16HdcTargetSpeed=S16_HDC_MT_4TH_TG_SPD_4L;
            }
            else if (lcu8HdcGearPosition==U8_HDC_5TH_GEAR)
            {
                lcs16HdcTargetSpeed=S16_HDC_MT_5TH_TG_SPD_4L;
            }
            else                                                /* (lcu8HdcGearPosition==U8_HDC_NEU_GEAR) or 6th    */
            {
                lcs16HdcTargetSpeed = lcs16HdcTargetSpeed;

                if (lcu1hdcClutchSwitch==1)
                {
                    lcs16_hdc_tempW9 = LCESP_s16IInter2Point(lcu8hdcClutchSwitch_timer, L_U8_TIME_10MSLOOP_500MS,   100,
                                                                                        L_U8_TIME_10MSLOOP_1000MS,       0);
                }
                else
                {
                    lcs16_hdc_tempW9 = 0;
                }

                if ((lcs16HdcActiveFlg_off_cnt<L_U8_TIME_10MSLOOP_1000MS)||(lcu1HdcActiveFlg==1))        /* 제어 상황    */
                {
                    if (lcu8HdcGearPosition==U8_HDC_6TH_GEAR)
                    {
                        lcs16_hdc_tempW8 = VREF_3_KPH;
                    }
                    else
                    {
                        lcs16_hdc_tempW8 = VREF_0_KPH;
                    }

                    if (((int16_t)eng_rpm < lcs16HdcEngStallRpm-lcs16_hdc_tempW9)
                        &&(eng_torq_rel > lcs16HdcEngIdleTorque + 75)
                        &&(lcs16HdcTargetSpeed<S16_HDC_MT_5TH_TG_SPD_4L+lcs16_hdc_tempW8)&&(lcu8HdcEngUpdateCnt==0))
                    {
                        lcs16HdcTargetSpeed = lcs16HdcTargetSpeed +1;
                        lcu8HdcTargetSpeedcnt=L_U8_TIME_10MSLOOP_500MS;
                    }
                    else { }
                }
                else                                                                /* 비제어 상황*/
                {
                    if (lcu8HdcTargetSpeedcnt==0)
                    {
                        if (vref >= S16_HDC_MT_5TH_TG_SPD_4L)
                        {
                            if (lcs16HdcTargetSpeed > S16_HDC_MT_5TH_TG_SPD_4L)
                            {
                                lcs16HdcTargetSpeed = S16_HDC_MT_5TH_TG_SPD_4L;
                            }
                            else { }
                        }
                        else if (vref >= S16_HDC_MT_4TH_TG_SPD_4L)
                        {
                            if (lcs16HdcTargetSpeed > S16_HDC_MT_4TH_TG_SPD_4L)
                            {
                                lcs16HdcTargetSpeed = S16_HDC_MT_4TH_TG_SPD_4L;
                            }
                            else { }
                        }
                        else if (vref >= S16_HDC_MT_3RD_TG_SPD_4L)
                        {
                            if (lcs16HdcTargetSpeed > S16_HDC_MT_3RD_TG_SPD_4L)
                            {
                                lcs16HdcTargetSpeed = S16_HDC_MT_3RD_TG_SPD_4L;
                            }
                            else { }
                        }
                        else if (vref >= S16_HDC_MT_2ND_TG_SPD_4L)
                        {
                            if (lcs16HdcTargetSpeed > S16_HDC_MT_2ND_TG_SPD_4L)
                            {
                                lcs16HdcTargetSpeed = S16_HDC_MT_2ND_TG_SPD_4L;
                            }
                            else { }
                        }
                        else
                        {
                            lcs16HdcTargetSpeed = S16_HDC_MT_1ST_TG_SPD_4L;
                        }
                    }
                    else { }
                }
            }

            /* Check Min TG Speed	*/
            lcs16HdcTargetSpeed = (S16_HDC_MT_1ST_TG_SPD_4L > lcs16HdcTargetSpeed)? S16_HDC_MT_1ST_TG_SPD_4L:lcs16HdcTargetSpeed;
        }
        else
        {
            lcs16HdcTargetSpeed = (S16_HDC_MT_1ST_TG_SPD_4L + S16_HDC_MT_REV_TG_SPD_4L)/2;
        }
        /* Check Min TG Speed	*/
        lcs16_hdc_tempW9 = S16_HDC_MT_REV_TG_SPD_4L;
        lcs16_hdc_tempW9 = (lcs16_hdc_tempW9 > S16_HDC_MT_1ST_TG_SPD_4L)? S16_HDC_MT_1ST_TG_SPD_4L:lcs16_hdc_tempW9;
        lcs16_hdc_tempW9 = (lcs16_hdc_tempW9 > S16_HDC_MT_NEU_TG_SPD_4L)? S16_HDC_MT_NEU_TG_SPD_4L:lcs16_hdc_tempW9;

        lcs16HdcTargetSpeed = (lcs16_hdc_tempW9 > lcs16HdcTargetSpeed)? lcs16_hdc_tempW9:lcs16HdcTargetSpeed;
        lcs16HdcTargetSpeed = (VREF_20_KPH < lcs16HdcTargetSpeed)? VREF_20_KPH:lcs16HdcTargetSpeed;        	        	
    }
    else
    {
        /* 전진시에만 rpm 보상적용이 필요함. */

        if (lcu1hdcVehicleBackward ==1)
        {
        	lcs16HdcEngrpmDotPeakN = 0;
            if (((int16_t)eng_rpm < lcs16HdcEngStallRpm)&&(lcs16HdcTargetSpeed<S16_HDC_MT_REV_TG_SPD+VREF_1_KPH)&&(lcu1HdcActiveFlg==1))
            {
            	if (lcu8HdcEngUpdateCnt==0)
            	{
                	lcs16HdcTargetSpeed = lcs16HdcTargetSpeed +1;
                }
                else { }
            }
            else
            {
                lcs16HdcTargetSpeed=S16_HDC_MT_REV_TG_SPD;
            }
        }
        else if (lcu1hdcVehicleForward ==1)
        {
            /* 제어 진입시에만 Target Spd 보상이 필요함.*/

            if(lcu8HdcGearPosition==U8_HDC_1ST_GEAR)
            {
            	lcs16HdcEngrpmDotPeakN = 0;
                lcs16HdcTargetSpeed=S16_HDC_MT_1ST_TG_SPD;
            }
            else if(lcu8HdcGearPosition==U8_HDC_2ND_GEAR)
            {
            	lcs16HdcEngrpmDotPeakN = 0;
            	if ((lcs16HdcTargetSpeedOld >= S16_HDC_MT_2ND_TG_SPD)||(lcu1HdcEngTorq_OKFlg==1))
            	{
            		if (lcs16HdcTargetSpeed < S16_HDC_MT_2ND_TG_SPD)
            		{
            			if (lcu8HdcEngUpdateCnt==0)                                                                                 
            			{
            				lcs16HdcTargetSpeed = lcs16HdcTargetSpeed + 1;
            			}
            			else { }
            		}
            		else
            		{	
                		lcs16HdcTargetSpeed=S16_HDC_MT_2ND_TG_SPD;
                	}
                }
                else
                { 
                	LCHDC_vAdaptTargetSpeed(S16_HDC_MT_2ND_TG_SPD);
                }                 
            }
            else if(lcu8HdcGearPosition==U8_HDC_3RD_GEAR)
            {
            	lcs16HdcEngrpmDotPeakN = 0;
            	if ((lcs16HdcTargetSpeedOld >= S16_HDC_MT_3RD_TG_SPD)||(lcu1HdcEngTorq_OKFlg==1))
            	{
            		if (lcs16HdcTargetSpeed < S16_HDC_MT_3RD_TG_SPD)
            		{
            			if (lcu8HdcEngUpdateCnt==0)                                                                                 
            			{
							lcs16HdcTargetSpeed = lcs16HdcTargetSpeed + 1;
						}
						else { }
					}
					else
					{
						lcs16HdcTargetSpeed = S16_HDC_MT_3RD_TG_SPD;
					}
                }
                else
                { 
                	LCHDC_vAdaptTargetSpeed(S16_HDC_MT_3RD_TG_SPD);
                }                
            }
            else if(lcu8HdcGearPosition==U8_HDC_4TH_GEAR)
            {
            	lcs16HdcEngrpmDotPeakN = 0;
            	if ((lcs16HdcTargetSpeedOld >= S16_HDC_MT_4TH_TG_SPD)||(lcu1HdcEngTorq_OKFlg==1))
            	{
            		if (lcs16HdcTargetSpeed < S16_HDC_MT_4TH_TG_SPD)
            		{
            			if (lcu8HdcEngUpdateCnt==0)                                                                                 
            			{
							lcs16HdcTargetSpeed = lcs16HdcTargetSpeed + 1;
						}
						else { }
					}
					else
					{
						lcs16HdcTargetSpeed = S16_HDC_MT_4TH_TG_SPD;
					}
                }
                else
                { 
                	LCHDC_vAdaptTargetSpeed(S16_HDC_MT_4TH_TG_SPD);
                }                
            }
            else if (lcu8HdcGearPosition==U8_HDC_5TH_GEAR)
            {
            	lcs16HdcEngrpmDotPeakN = 0;
            	if ((lcs16HdcTargetSpeedOld >= S16_HDC_MT_5TH_TG_SPD)||(lcu1HdcEngTorq_OKFlg==1))
            	{
            		if (lcs16HdcTargetSpeed < S16_HDC_MT_5TH_TG_SPD)
            		{
            			if (lcu8HdcEngUpdateCnt==0)                                                                                 
            			{
							lcs16HdcTargetSpeed = lcs16HdcTargetSpeed + 1;
						}
						else { }
					}
					else
					{
						lcs16HdcTargetSpeed = S16_HDC_MT_5TH_TG_SPD;
					}
                }
                else
                { 
                	LCHDC_vAdaptTargetSpeed(S16_HDC_MT_5TH_TG_SPD);
                }                
            }
            else                                                /* (lcu8HdcGearPosition==U8_HDC_NEU_GEAR) or 6th    */
            {
                lcs16HdcTargetSpeed = lcs16HdcTargetSpeed;

                if (lcu1hdcClutchSwitch==1)
                {
                    lcs16_hdc_tempW9 = LCESP_s16IInter2Point((int16_t)lcu8hdcClutchSwitch_timer, L_U8_TIME_10MSLOOP_500MS,      100,
                                                                                                 L_U8_TIME_10MSLOOP_1000MS,       0);
                }
                else
                {
                    lcs16_hdc_tempW9 = 0;
                }

                if ((lcs16HdcActiveFlg_off_cnt<L_U8_TIME_10MSLOOP_1000MS)||(lcu1HdcActiveFlg==1))        /* 제어 상황    */
                {
                    if (lcu8HdcGearPosition==U8_HDC_6TH_GEAR)
                    {
                        lcs16_hdc_tempW8 = VREF_3_KPH;
                    }
                    else
                    {
                        lcs16_hdc_tempW8 = VREF_0_KPH;
                    }
                    
		            if ((lcs16HdcEngrpmDot<0)&&(lcs16HdcEngrpmDot<lcs16HdcEngrpmDotPeakN)&&(lcs16HdcEngTorqDiff>=5))
		            {
		            	lcs16HdcEngrpmDotPeakN = lcs16HdcEngrpmDot;
		            }
		            else
		            {
		            	if ((lcs16HdcEngrpmDot-lcs16HdcEngrpmDotOld)>= 1)
		            	{
		            		lcs16HdcEngrpmDotPeakN = 0;
		            	}
		            	else { }
		            }		            

		            if (lcs16HdcTargetSpeed<(S16_HDC_MT_5TH_TG_SPD+lcs16_hdc_tempW8))
		            {
		            	if ((lcs16HdcEngrpmDotPeakN<-25)&&((int16_t)eng_rpm < (lcs16HdcEngStallRpm-lcs16_hdc_tempW9)))
		            	{
		            		if((lcs16HdcTargetSpeed < (vref+VREF_1_5_KPH))&&(lcu8HdcEngUpdateCnt==0))
		            		{
			                	lcs16HdcTargetSpeed = lcs16HdcTargetSpeed + 2;
			                	lcu8HdcTargetSpeedcnt=L_U8_TIME_10MSLOOP_500MS;
			                }
			                else { }
		                }
		                else if ((lcs16HdcEngrpmDotPeakN<-15)&&((int16_t)eng_rpm < (lcs16HdcEngStallRpm-lcs16_hdc_tempW9))&&(lcu8HdcEngUpdateCnt==0))
		                {
		                	if(lcs16HdcTargetSpeed < (vref+VREF_1_5_KPH))
		                	{
			                	lcs16HdcTargetSpeed = lcs16HdcTargetSpeed + 1;
			                	lcu8HdcTargetSpeedcnt=L_U8_TIME_10MSLOOP_500MS;
			                }
			                else { }
		                }
		                else 
		                {
		                	if (((lcu8HdcGearPositionOld==1)&&(lcs16HdcTargetSpeed > (S16_HDC_MT_2ND_TG_SPD + VREF_1_KPH)))
		                	   ||((lcu8HdcGearPositionOld==2)&&(lcs16HdcTargetSpeed > (S16_HDC_MT_3RD_TG_SPD + VREF_1_KPH)))
		                	   ||((lcu8HdcGearPositionOld==3)&&(lcs16HdcTargetSpeed > (S16_HDC_MT_4TH_TG_SPD + VREF_1_KPH)))
		                	   ||((lcu8HdcGearPositionOld==4)&&(lcs16HdcTargetSpeed > (S16_HDC_MT_5TH_TG_SPD + VREF_1_KPH))))
		                	{
		                		if (lcu8HdcEngUpdateCnt==0)
		                		{
		                			lcs16HdcTargetSpeed = lcs16HdcTargetSpeed - 1;
		                		}
		                		else { }
		                	}
		                	else { }
		                }		                			                			                	
		            }
		            else { }
		        }				
                else                                                                /* 비제어 상황*/
                {
                	lcs16HdcEngrpmDotPeakN = 0;
                    if (lcu8HdcTargetSpeedcnt==0)
                    {
                        if (vref >= S16_HDC_MT_5TH_TG_SPD)
                        {
                            if (lcs16HdcTargetSpeed > S16_HDC_MT_5TH_TG_SPD)
                            {
                                lcs16HdcTargetSpeed = S16_HDC_MT_5TH_TG_SPD;
                            }
                            else { }
                        }
                        else if (vref >= S16_HDC_MT_4TH_TG_SPD)
                        {
                            if (lcs16HdcTargetSpeed > S16_HDC_MT_4TH_TG_SPD)
                            {
                                lcs16HdcTargetSpeed = S16_HDC_MT_4TH_TG_SPD;
                            }
                            else { }
                        }
                        else if (vref >= S16_HDC_MT_3RD_TG_SPD)
                        {
                            if (lcs16HdcTargetSpeed > S16_HDC_MT_3RD_TG_SPD)
                            {
                                lcs16HdcTargetSpeed = S16_HDC_MT_3RD_TG_SPD;
                            }
                            else { }
                        }
                        else if (vref >= S16_HDC_MT_2ND_TG_SPD)
                        {
                            if (lcs16HdcTargetSpeed > S16_HDC_MT_2ND_TG_SPD)
                            {
                                lcs16HdcTargetSpeed = S16_HDC_MT_2ND_TG_SPD;
                            }
                            else { }
                        }
                        else
                        {
                            lcs16HdcTargetSpeed = S16_HDC_MT_1ST_TG_SPD;
                        }
                    }
                    else { }
                }
            }

            /* Check Min TG Speed	*/
            lcs16HdcTargetSpeed = (S16_HDC_MT_1ST_TG_SPD > lcs16HdcTargetSpeed)? S16_HDC_MT_1ST_TG_SPD:lcs16HdcTargetSpeed;
        }
        else
        {
        	lcs16HdcEngrpmDotPeakN = 0;
            lcs16HdcTargetSpeed = (S16_HDC_MT_1ST_TG_SPD + S16_HDC_MT_REV_TG_SPD)/2;
        }
        /* Check Min TG Speed	*/
        lcs16_hdc_tempW9 = S16_HDC_MT_REV_TG_SPD;
        lcs16_hdc_tempW9 = (lcs16_hdc_tempW9 > S16_HDC_MT_1ST_TG_SPD)? S16_HDC_MT_1ST_TG_SPD:lcs16_hdc_tempW9;
        lcs16_hdc_tempW9 = (lcs16_hdc_tempW9 > S16_HDC_MT_NEU_TG_SPD)? S16_HDC_MT_NEU_TG_SPD:lcs16_hdc_tempW9;

        lcs16HdcTargetSpeed = (lcs16_hdc_tempW9 > lcs16HdcTargetSpeed)? lcs16_hdc_tempW9:lcs16HdcTargetSpeed;
        lcs16HdcTargetSpeed = (VREF_20_KPH < lcs16HdcTargetSpeed)? VREF_20_KPH:lcs16HdcTargetSpeed;        	
    }
}

static void LCHDC_vAdaptTargetSpeed(char16_t lcs16HdcTargetSpeedAtGear)
{
   	if ((lcs16HdcActiveFlg_off_cnt<L_U8_TIME_10MSLOOP_1000MS)||(lcu1HdcActiveFlg==1))      
 	{
 		if (lcs16HdcTargetSpeed < lcs16HdcTargetSpeedAtGear) 
        {
	      	if ((int16_t)eng_rpm < ((lcs16HdcEngStallRpm*8)/10))
    	    {
    	    	if (lcu8HdcEngUpdateCnt==0)
    	    	{                                                                                 
	    			lcs16HdcTargetSpeed = lcs16HdcTargetSpeed + 2;   
	    		}
	    		else { }
			}                   
			else if((lcs16HdcEngrpmDot < -20) && ((int16_t)eng_rpm < (lcs16HdcEngStallRpm+300)))
			{
    	    	if (lcu8HdcEngUpdateCnt==0)
    	    	{                                                                                 
	    			lcs16HdcTargetSpeed = lcs16HdcTargetSpeed + 2;   
	    		}
	    		else { }
			}
	        else if (((lcs16HdcEngrpmDot < -10) && ((int16_t)eng_rpm < (lcs16HdcEngStallRpm+200))) || ((int16_t)eng_rpm < lcs16HdcEngStallRpm))  
    	    {
    	    	if (lcu8HdcEngUpdateCnt==0)                                                                                 
            	{
            		lcs16HdcTargetSpeed = lcs16HdcTargetSpeed + 1;                              
				}
				else { }
			}                                                                               
			else { }
			
			if(lcs16HdcTargetSpeed > lcs16HdcTargetSpeedAtGear)
			{
				lcs16HdcTargetSpeed = lcs16HdcTargetSpeedAtGear;
			}
		}
		else { }
	}
	else { }
}

static void LCHDC_vChkECSComp(void)
{
    /*
        Sub-Function Out Put
            1. lcu8HdcEcsCompEnable
            2. lcs16HdcEcsLongGComp
    */

    /*
        ccu1EcsEquipFlg : ECS 장착 차량임을 의미함. (ECS1 or ECS2 메시지가 단 한번이라도 수신되면 Set됨. Clear는 없음.)
        ccu1EcsCanMsgTimeout : ECS1 or ECS2 Message가 수신이 안 될 경우 뜨는 Flag. 둘 중에 어느 하나라도 수신이 안되면 Set 됨. Clear 없음
    */
  #if __ECS_ENABLE_VEHICLE
    lcu1hdcEcsEquipFlg      = ccu1EcsEquipFlg;
    lcu1hdcEcsCanMsgTimeout = ccu1EcsCanMsgTimeout;
  #else
    lcu1hdcEcsEquipFlg      = 0;
    lcu1hdcEcsCanMsgTimeout = 0;
  #endif

  #if __ECS_ENABLE_VEHICLE
    /* Height Average   */
    if((Rough_road_detect_vehicle==0)&&(Rough_road_suspect_vehicle==0))
    {
        lcs16_hdc_tempW8 = L_U8FILTER_GAIN_10MSLOOP_1HZ;       /*  1Hz */
    }
    else
    {
        lcs16_hdc_tempW8 = L_U8FILTER_GAIN_10MSLOOP_0_5HZ;       /* 0.5Hz    */
    }

    lcs16_hdc_tempW9 = (ls16ecsHeighRL+ls16ecsHeighRR)*5;

    if (lcs16_hdc_tempW9 > 800)
    {
        lcs16_hdc_tempW9 = 800;
    }
    else if (lcs16_hdc_tempW9 < -800)
    {
        lcs16_hdc_tempW9 = -800;
    }
    else { }

    ls16ecsHeighAverage_old = ls16ecsHeighAverage;
    ls16ecsHeighAverage=LCESP_s16Lpf1Int(lcs16_hdc_tempW9,ls16ecsHeighAverage_old,(uint8_t)lcs16_hdc_tempW8);    /*   1Hz ~ 0.5Hz */

    lcs16HdcEcsLongGCompHeight = LCESP_s16IInter3Point(ls16ecsHeighAverage,
                                                          -400, S16_ECS_LOW_MODE_G_COMP,
                                                             0, S16_ECS_NORMAL_MODE_G_COMP,
                                                           400, S16_ECS_HIGH_MODE_G_COMP);

    if (lcu8ecsREQLevel==lcu8ecsACTHeight)
    {
        if ((lcu8ecsLifting==1)||(lcu8ecsLowering==1))
        {
            if (lcu8HdcEcsSuspect_cnt < L_U8_TIME_10MSLOOP_50MS)
            {
                lcu8HdcEcsSuspect_cnt++;
            }
            else
            {
                lcu1HdcEcsCompFailSigFlg = 1;
            }
        }
        else
        {
            if (lcu8HdcEcsSuspect_cnt > 0)
            {
                lcu8HdcEcsSuspect_cnt--;
            }
            else
            {
                lcu1HdcEcsCompFailSigFlg = 0;
            }
        }
    }
    else if (lcu8ecsREQLevel > lcu8ecsACTHeight)
    {
        if ((lcu8ecsLifting==0)||(lcu8ecsLowering==1))
        {
            if (lcu8HdcEcsSuspect_cnt < L_U8_TIME_10MSLOOP_50MS)
            {
                lcu8HdcEcsSuspect_cnt++;
            }
            else
            {
                lcu1HdcEcsCompFailSigFlg = 1;
            }
        }
        else
        {
            if (lcu8HdcEcsSuspect_cnt > 0)
            {
                lcu8HdcEcsSuspect_cnt--;
            }
            else
            {
                lcu1HdcEcsCompFailSigFlg = 0;
            }
        }
    }
    else /* (lcu8ecsREQLevel < lcu8ecsACTHeight)    */
    {
        if ((lcu8ecsLifting==1)||(lcu8ecsLowering==0))
        {
            if (lcu8HdcEcsSuspect_cnt < L_U8_TIME_10MSLOOP_50MS)
            {
                lcu8HdcEcsSuspect_cnt++;
            }
            else
            {
                lcu1HdcEcsCompFailSigFlg = 1;
            }
        }
        else
        {
            if (lcu8HdcEcsSuspect_cnt > 0)
            {
                lcu8HdcEcsSuspect_cnt--;
            }
            else
            {
                lcu1HdcEcsCompFailSigFlg = 0;
            }
        }
    }


    if ((lcu8ecsWLamp==1)||(lcu8ecsDef==1)||(lcu8ecsDiag==1))
    {
        if (lcu8HdcEcsSuspect_cnt2 < L_U8_TIME_10MSLOOP_50MS)
        {
            lcu8HdcEcsSuspect_cnt2++;
        }
        else
        {
            lcu1HdcEcsCompFailFlg = 1;
        }
    }
    else
    {
        if (lcu8HdcEcsSuspect_cnt2 > 0)
        {
            lcu8HdcEcsSuspect_cnt2--;
        }
        else
        {
            lcu1HdcEcsCompFailFlg = 0;
        }
    }

/*    if ((lcu1HdcEcsCompFailSigFlg==1)||(lcu1HdcEcsCompFailFlg==1))    */
    if ((lcu1HdcEcsCompFailFlg==1)
        ||(lcu1hdcEcsEquipFlg==0)||(lcu1hdcEcsCanMsgTimeout==1)
        ||(lcu8ecsACTHeight<3)||(lcu8ecsACTHeight>7))
    {
        lcu8HdcEcsCompEnable = 0;
        lcs16HdcEcsLongGComp = 0;
    }
    else if ((lcu8ecsLifting==0)&&(lcu8ecsLowering==0))
    {
        lcu8HdcEcsCompEnable = 1;

        /*  Low         3   */
        /*  HighWay     4   */
        /*  Normal      5   */
        /*  Off-road    6   */
        /*  High        7   */
        if (lcu8ecsACTHeight==3)
        {
            lcs16HdcEcsLongGComp = S16_ECS_LOW_MODE_G_COMP;
        }
        else if (lcu8ecsACTHeight==4)
        {
            lcs16HdcEcsLongGComp = S16_ECS_HIGHWAY_MODE_G_COMP;
        }
        else if (lcu8ecsACTHeight==5)
        {
            lcs16HdcEcsLongGComp = S16_ECS_NORMAL_MODE_G_COMP;
        }
        else if (lcu8ecsACTHeight==6)
        {
            lcs16HdcEcsLongGComp = S16_ECS_OFFROAD_MODE_G_COMP;
        }
        else if (lcu8ecsACTHeight==7)
        {
            lcs16HdcEcsLongGComp = S16_ECS_HIGH_MODE_G_COMP;
        }
        else  /*    (lcu8ecsACTHeight==5)   */
        {
            lcs16HdcEcsLongGComp = S16_ECS_NORMAL_MODE_G_COMP;
        }
    }
    else if ((lcu8ecsLifting==1)&&(lcu8ecsLowering==1))
    {
        lcu8HdcEcsCompEnable = 0;
        lcs16HdcEcsLongGComp = 0;
    }
    else    /*  if((lcu8ecsLifting==1) EX_or (lcu8ecsLowering==1))      */
    {
        lcu8HdcEcsCompEnable = 1;
        lcs16HdcEcsLongGComp = lcs16HdcEcsLongGCompHeight;

        if (lcs16HdcEcsLongGComp > S16_ECS_HIGH_MODE_G_COMP)
        {
            lcs16HdcEcsLongGComp = S16_ECS_HIGH_MODE_G_COMP;
        }
        else if (lcs16HdcEcsLongGComp < S16_ECS_LOW_MODE_G_COMP)
        {
            lcs16HdcEcsLongGComp = S16_ECS_LOW_MODE_G_COMP;
        }
        else { }
    }
  #else
    lcu8HdcEcsCompEnable = 0;
    lcs16HdcEcsLongGComp = 0;  
  #endif // ~#if __ECS_ENABLE_VEHICLE
  
}

void LCHDC_vChkDiscTempConds(void)
{

    lcs16HdcMaxDiscTemp = 0;
    lcs16HdcMaxDiscTemp = (lcs16HdcMaxDiscTemp>btc_tmp_fl)?lcs16HdcMaxDiscTemp:btc_tmp_fl;
    lcs16HdcMaxDiscTemp = (lcs16HdcMaxDiscTemp>btc_tmp_fr)?lcs16HdcMaxDiscTemp:btc_tmp_fr;
    lcs16HdcMaxDiscTemp = (lcs16HdcMaxDiscTemp>btc_tmp_rl)?lcs16HdcMaxDiscTemp:btc_tmp_rl;
    lcs16HdcMaxDiscTemp = (lcs16HdcMaxDiscTemp>btc_tmp_rr)?lcs16HdcMaxDiscTemp:btc_tmp_rr;

    if(lcu1HdcDiscTempErrFlg==0)
    {
        if(lcs16HdcMaxDiscTemp>S16_HDC_EXIT_DISC_TEMP)
        {
            lcu1HdcDiscTempErrFlg=1;
        }
        else
        {
            lcu1HdcDiscTempErrFlg=0;
        }
    }
    else
    {
        if(lcs16HdcMaxDiscTemp<S16_HDC_ENT_DISC_TEMP)
        {
            lcu1HdcDiscTempErrFlg=0;
        }
        else
        {
            lcu1HdcDiscTempErrFlg=1;
        }
    }
}


static void LCHDC_vChkEnginestate(void)
{
    if (HDC_TM_TYPE==HDC_AUTO_TM)
    {
        lcu1HdcEngTorq_OKFlg=0;
    }
    else    /* MT 전용  */
    {
        if ((int16_t)eng_torq_rel > (lcs16HdcEngIdleTorque + S16_HDC_MT_ENG_TORQ_OK)) 
        {
            if (lcu8HdcEngTorq_cnt<L_U8_TIME_10MSLOOP_1000MS)
            {
                lcu8HdcEngTorq_cnt++;
            }
            else { }
		}
		else
		{
            if (lcu8HdcEngTorq_cnt > 0)
            {
                lcu8HdcEngTorq_cnt--;
            }
            else { }
		}
	}

	if (lcu8HdcEngTorq_cnt > L_U8_TIME_10MSLOOP_500MS)
	{
		lcu1HdcEngTorq_OKFlg=1;
	}
	else
	{
		lcu1HdcEngTorq_OKFlg=0;
	}

/*****************************************************************/

  #if __Engine_Idle_Check
    if (HDC_TM_TYPE==HDC_AUTO_TM)
    {
        lcu1HdcEngTorq_N_OKFlg=0;
    }
    else    /* MT 전용  */
    {   
        if ((lcu1hdcVehicleBackward==0)&&(lcu1hdcVehicleForward==0)&&(lcu8HdcDriverAccelIntend==0))
        {
	        lcs16_hdc_tempW9 = LCESP_s16IInter3Point(vref,  VREF_4_KPH,40,
	                                                        VREF_8_KPH,30,
	                                                        VREF_12_KPH,20);
	                                                        
			if ((int16_t)eng_torq_rel <= lcs16_hdc_tempW9+lcs16HdcEngIdleTorque)
			{
				lcu1hdcEngNoverload = 0;
				lcs16_hdc_tempW7 = 0;
			}
			else
			{
				lcu1hdcEngNoverload = 1;
				lcs16_hdc_tempW1 = (int16_t)lcu8HdcToqSTBtimerAfterAccel;
				lcs16_hdc_tempW7 = LCESP_s16IInter2Point(lcs16_hdc_tempW1,  0,	                       lcs16HdcEngOverRpm,
                                                   						    L_U8_TIME_10MSLOOP_50MS,   0);
			}
        	lcs16_hdc_tempW9 = lcs16_hdc_tempW9 + lcs16_hdc_tempW7 + lcs16HdcEngIdleTorque;
	        lcs16_hdc_tempW8 = LCESP_s16IInter3Point(vref,  VREF_4_KPH, 20,
	                                                        VREF_8_KPH, 17,
	                                                        VREF_12_KPH,14);
	        lcs16_hdc_tempW8 = lcs16_hdc_tempW8*lcs16HdcEngIdleRpm/10;          /* rpm Msx 1500*20 = 30000  */

	      	if(lcu1hdcClutchSwitch==1)
	        {
	        	lcs16_hdc_tempW7 = L_U8_TIME_10MSLOOP_300MS;
	        }
	        else
	        {
	        	lcs16_hdc_tempW7 = L_U8_TIME_10MSLOOP_700MS;
	        }
	        
	        if((((int16_t)eng_torq_rel < lcs16_hdc_tempW9)&&((int16_t)eng_rpm < lcs16_hdc_tempW8))
	        	&&((lcu1hdcEngNoverload==0)||((lcu1hdcEngNoverload==1)&&(vref > (S16_HDC_MT_1ST_TG_SPD-VREF_2_KPH)))))
	        {
	            if (lcu8HdcEngTorq_N_cnt<lcs16_hdc_tempW7)
	            {
	                lcu8HdcEngTorq_N_cnt++;
	            }
	            else
	            {
            		lcu1HdcEngTorq_N_OKFlg=1;
	            }
	        }
	        else
	        {
	            lcu8HdcEngTorq_N_cnt=0;
	            lcu1HdcEngTorq_N_OKFlg=0;
	            lcu1hdcEngNoverload=0;
	        }
	    }
	    else
	    {
	    	lcu8HdcEngTorq_N_cnt=0;
	    	lcu1HdcEngTorq_N_OKFlg=0;
	    	lcu1hdcEngNoverload=0;
	    }
    }
  #endif  
}

#if __STOP_ON_HILL
void LCHDC_vChkVehicleStopOnHill(void)
{
    /*
        Sub-Function Out Put
            1. lcu1hdcStopOnUpHillFlg
            2. lcu1hdcStopOnDownHillFlg
    */	

    if ((lcs16HdcASen-lcs16HdcASenOld <= LONG_0G003G)&&(lcs16HdcASen-lcs16HdcASenOld >= -LONG_0G003G))
    {
    	if (lcu8hdcASenSTBCnt>U8_HDC_AX_STABLE_TIME_ONHILL)
    	{
    		lcs16_hdc_tempW8 = 1 ;
    	}
    	else
    	{
    		lcu8hdcASenSTBCnt = lcu8hdcASenSTBCnt + 1;
    	}
    }
    else
    {
    	lcu8hdcASenSTBCnt = 0;
    	lcs16_hdc_tempW8 = 0;
    }

    if ((lcu1hdcStopOnUpHillFlg==0)&&(lcu1hdcStopOnDownHillFlg==0))
    {
        if ((vref <= VREF_0_25_KPH)&&(lcu8HdcDriverBrakeIntend==1)&&(lcs16_hdc_tempW8 ==1))
        {
            if (lcs16hdcLongOffsetAvrlf > ((int16_t)S16_HDC_SMA_G_TH*(AX_Resol)-LONG_0G01G-LONG_0G005G)) /* uphill Th. 0.65g */
            {
                lcu1hdcStopOnUpHillFlg =1;
            }
            else if (lcs16hdcLongOffsetAvrlf < -((int16_t)S16_HDC_SMA_G_TH*(AX_Resol)-LONG_0G005G)) /* downhill Th. 0.75g */ 
            {
                lcu1hdcStopOnDownHillFlg =1;
            }
            else { }
        }
        else { }
    }
    else 
    { 
    	if (lcu1hdcStopOnUpHillFlg==1)
    	{
    		if (((lcu8HdcDriverAccelIntend==1)&&(gear_state!=U8_HDC_REV_GEAR))
    			||(vref >= VREF_5_KPH))
    		{
    			lcu1hdcStopOnUpHillFlg = 0;
    		}
    		else { }
    	}
    	else if (lcu1hdcStopOnDownHillFlg==1)
    	{
    		if (((lcu8HdcDriverAccelIntend==1)&&(gear_state==U8_HDC_REV_GEAR))
    			||(vref >= VREF_5_KPH))
    		{
    			lcu1hdcStopOnDownHillFlg = 0;
    		}
    		else { }
    	}
    	else { }
	}
}
#endif  

void LCHDC_vChkVehicleDirINGear(void)
{
    /*
        Sub-Function Out Put
            1. lcu1hdcVehicleForwardTM
            2. lcu1hdcVehicleBackwardTM
            3. lcu1hdcVehicleDirectionSusFlg
            4. lcu1hdcMTGearSusFlag
            5. lcu1hdcMTGearSusFlag2
    */

    /*  TM CAN Massage  */

    if ((lcu1hdcClutchSwitch==0)&&(HDC_TM_TYPE==HDC_MANUAL_TM)&&(lcu1HdcClutchIndependEngageFlg==1))
    {
        if ((lcs16HdcRightGearFlgCnt > L_U8_TIME_10MSLOOP_500MS)&&(gear_state >= 1 )&&(gear_state <= 6 ))
        {
            if (gear_state==1){lcu1hdcGear1stOkFlg=1;}
            if (gear_state==2){lcu1hdcGear2ndOkFlg=1;}
            if (gear_state==3){lcu1hdcGear3rdOkFlg=1;}
            if (gear_state==4){lcu1hdcGear4thOkFlg=1;}
            if (gear_state==5){lcu1hdcGear5thOkFlg=1;}
            if (gear_state==6){lcu1hdcGear6thOkFlg=1;}
        }
        else { }

        lcs16_hdc_tempW9 = 0;
        if (lcu1hdcGear1stOkFlg==1){lcs16_hdc_tempW9++;}
        if (lcu1hdcGear2ndOkFlg==1){lcs16_hdc_tempW9++;}
        if (lcu1hdcGear3rdOkFlg==1){lcs16_hdc_tempW9++;}
        if (lcu1hdcGear4thOkFlg==1){lcs16_hdc_tempW9++;}
        if (lcu1hdcGear5thOkFlg==1){lcs16_hdc_tempW9++;}
        if (lcu1hdcGear6thOkFlg==1){lcs16_hdc_tempW9++;}

        if (lcs16_hdc_tempW9>1)
        {
            lcu1hdcMTGearSusFlag =1;
        }
        else { }
    }
    else
    {
        lcu1hdcGear1stOkFlg=0;
        lcu1hdcGear2ndOkFlg=0;
        lcu1hdcGear3rdOkFlg=0;
        lcu1hdcGear4thOkFlg=0;
        lcu1hdcGear5thOkFlg=0;
        lcu1hdcGear6thOkFlg=0;

        lcu1hdcMTGearSusFlag =0;
    }

    /*********************************************/
    if ((lcu1hdcClutchSwitch==0)&&(HDC_TM_TYPE==HDC_MANUAL_TM)&&(lcu1HdcClutchIndependEngageFlg==1))
    {
        if ((abs(yaw_out)<YAW_20DEG)&&(abs(lcs16HdcDecel)<LONG_0G2G)&&(lcu8HdcExcessSlip==0))   /* 감지 배제 조건   */
        {
            if (gear_state==0)
            {
                if (lcs16HdcINGearSusCnt < L_U8_TIME_10MSLOOP_500MS)
                {
                    lcs16HdcINGearSusCnt++;
                }
                else
                {
                    lcu1hdcMTGearSusFlag2 =1;
                }
            }
            else
            {
                lcs16HdcINGearSusCnt=0;
            }
        }
        else { }
    }
    else
    {
        lcs16HdcINGearSusCnt =0;
        lcu1hdcMTGearSusFlag2 =0;
    }


    /*********************************************/
    lcu1hdcVehicleDirectionSusFlg = 0;

    if ((lcu1HdcRightGearFlg==1)
        &&(lcu8HdcGearPosition != U8_HDC_NEU_GEAR)
        &&(lcu8HdcGearPosition != U8_HDC_PARK_GEAR))
    {
        if ((lcu1hdcVehicleBackward==0)&&(lcu1hdcVehicleForward==0))
        {
            if (((vref <= VREF_5_KPH)&&(lcs16HdcRightGearFlgCnt > L_U8_TIME_10MSLOOP_300MS))
                ||((vref > VREF_5_KPH)&&(lcs16HdcRightGearFlgCnt > L_U8_TIME_10MSLOOP_200MS))
                ||(vref > VREF_10_KPH))
            {
                if (lcu8HdcGearPosition == U8_HDC_REV_GEAR)
                {
                    lcu1hdcVehicleForwardTM=0;
                    lcu1hdcVehicleBackwardTM=1;
                }
                else
                {
                    lcu1hdcVehicleForwardTM=1;
                    lcu1hdcVehicleBackwardTM=0;
                }
            }
            else { }
        }
        else if (((lcu1hdcVehicleBackward==1)&&(lcu8HdcGearPosition == U8_HDC_REV_GEAR))
                ||((lcu1hdcVehicleForward==1)&&(lcu8HdcGearPosition != U8_HDC_REV_GEAR)))
        {
                if (lcu8HdcGearPosition == U8_HDC_REV_GEAR)
                {
                    lcu1hdcVehicleForwardTM=0;
                    lcu1hdcVehicleBackwardTM=1;
                }
                else
                {
                    lcu1hdcVehicleForwardTM=1;
                    lcu1hdcVehicleBackwardTM=0;
                }
        }
        else if (((lcu1hdcVehicleForward==1)&&(lcu8HdcGearPosition == U8_HDC_REV_GEAR))
                ||((lcu1hdcVehicleBackward==1)&&(lcu8HdcGearPosition != U8_HDC_REV_GEAR)))
        {
            lcu1hdcVehicleDirectionSusFlg = 1;

            if (((vref <= VREF_5_KPH)&&(lcs16HdcRightGearFlgCnt > L_U16_TIME_10MSLOOP_60S))
                ||((vref > VREF_5_KPH)&&(lcs16HdcRightGearFlgCnt > L_U16_TIME_10MSLOOP_40S))
                ||((vref > VREF_20_KPH)&&(lcs16HdcRightGearFlgCnt > L_U16_TIME_10MSLOOP_30S))
                ||((vref > VREF_40_KPH)&&(lcs16HdcRightGearFlgCnt > L_U16_TIME_10MSLOOP_20S))
                ||(vref > VREF_80_KPH)
                ||(vref < VREF_1_5_KPH))
            {
                if (lcu8HdcGearPosition == U8_HDC_REV_GEAR)
                {
                    lcu1hdcVehicleForwardTM=0;
                    lcu1hdcVehicleBackwardTM=1;
                }
                else
                {
                    lcu1hdcVehicleForwardTM=1;
                    lcu1hdcVehicleBackwardTM=0;
                }
            }
            else { }
        }
        else
        {
            if ((lcu1hdcVehicleForwardTM==1)&&(lcu1hdcVehicleBackwardTM==1))
            {
                lcu1hdcVehicleForwardTM=0;
                lcu1hdcVehicleBackwardTM=0;
            }
            else { }
        }
    }
    else if ((lcu1hdcMTGearSusFlag==1) || (lcu1hdcMTGearSusFlag2==1))
    {
        lcu1hdcVehicleForwardTM=0;
        lcu1hdcVehicleBackwardTM=0;
    }
    else if(vref >= VREF_2_KPH)
    {
        lcu1hdcVehicleForwardTM =lcu1hdcVehicleForwardTM;
        lcu1hdcVehicleBackwardTM=lcu1hdcVehicleBackwardTM;
    }
    else
    {
        lcu1hdcVehicleForwardTM =0;
        lcu1hdcVehicleBackwardTM=0;
    }
}

void LCHDC_vChkVehicleDirNGear(void)
{
    /*
        Sub-Function Out Put
            1. lcu1hdcVehicleForwardSensor
            2. lcu1hdcVehicleBackwardSensor

            3. lcu1hdcVehicleForwardSus1Flg
            4. lcu1hdcVehicleBackwardSus1Flg

            5. lcu1hdcVehicleForwardSus2Flg
            6. lcu1hdcVehicleBackwardSus2Flg

            7. lcu1HdcVehicleDirbySensorSusFlg
    */

    lcu8HdcThetaGModeOld = lcu8HdcThetaGMode;

    if (((lcu8HdcGearPosition==U8_HDC_NEU_GEAR)&&(lcu1HdcRightGearFlg==1)&&(vref > VREF_2_KPH ))
        ||(lcs16HdcActiveFlg_on_cnt>L_U8_TIME_10MSLOOP_1000MS)
      #if __INITIAL_FOR_BACK_CHK
        ||(lcu1HdcEngTorq_N_OKFlg==1)
      #endif
        ||((HDC_TM_TYPE==HDC_MANUAL_TM)&&(lcs16HdcThetaG_N_OK_Flg==1)))
    {
        lcs16_hdc_tempW8=1;
    }
    else
    {
        lcs16_hdc_tempW8=0;
    }

    if (lcs16_hdc_tempW8==1)
    {
#if __N_Gear_Detect_Change
        if ((lcs16HdcThetaG0 == lcs16HdcThetaG_max)&&(abs(yaw_out)<YAW_20DEG))
        {
            lcu8HdcThetaGMode =1;
        }
        else if ((lcs16HdcThetaG1 == lcs16HdcThetaG_max)&&(abs(yaw_out)<YAW_20DEG))
        {
            lcu8HdcThetaGMode =2;
        }
        else
        {
            lcu8HdcThetaGMode =0;
        }
#else
        if (lcs16HdcThetaG0 == lcs16HdcThetaG_max)
        {
            lcu8HdcThetaGMode =1;
        }
        else if (lcs16HdcThetaG1 == lcs16HdcThetaG_max)
        {
            lcu8HdcThetaGMode =2;
        }
        else
        {
            lcu8HdcThetaGMode =0;
        }
#endif
    }
    else
    {
        lcu8HdcThetaGMode =0;
        lcu1hdcVehicleForwardSus2Flg = 0;
        lcu1hdcVehicleBackwardSus2Flg = 0;
        lcu8HdcThetaGMode1Cnt = 0;
        lcu8HdcThetaGMode2Cnt = 0;
    }

    if (lcu1hdcVehicleForwardSus2Flg==1)
    {
        if (lcu8HdcThetaGMode ==2)
        {
            if (lcu8HdcThetaGMode2Cnt<L_U8_TIME_10MSLOOP_2S)
            {
                lcu8HdcThetaGMode2Cnt++;
            }
            else
            {
                lcu1hdcVehicleForwardSus2Flg =0;
                lcu1hdcVehicleBackwardSus2Flg =1;
            }
            lcu8HdcThetaGMode1Cnt =0;
        }
        else
        {
            lcu8HdcThetaGMode2Cnt =0;
        }
    }
    else if (lcu1hdcVehicleBackwardSus2Flg==1)
    {
        if (lcu8HdcThetaGMode ==1)
        {
            if (lcu8HdcThetaGMode1Cnt<L_U8_TIME_10MSLOOP_2S)
            {
                lcu8HdcThetaGMode1Cnt++;
            }
            else
            {
                lcu1hdcVehicleForwardSus2Flg =1;
                lcu1hdcVehicleBackwardSus2Flg =0;
            }

            lcu8HdcThetaGMode2Cnt =0;
        }
        else
        {
            lcu8HdcThetaGMode1Cnt =0;
        }
    }
    else
    {
        if (lcu8HdcThetaGMode ==1)
        {
            if (lcu8HdcThetaGMode1Cnt<L_U8_TIME_10MSLOOP_1000MS)
            {
                lcu8HdcThetaGMode1Cnt++;
            }
            else
            {
                lcu1hdcVehicleForwardSus2Flg =1;
            }
        }
        else
        {
            lcu8HdcThetaGMode1Cnt=0;
        }

        if (lcu8HdcThetaGMode ==2)
        {
            if (lcu8HdcThetaGMode2Cnt<L_U8_TIME_10MSLOOP_1000MS)
            {
                lcu8HdcThetaGMode2Cnt++;
            }
            else
            {
                lcu1hdcVehicleBackwardSus2Flg =1;
            }
        }
        else
        {
            lcu8HdcThetaGMode2Cnt=0;
        }
    }

    /*******************************************************************/
    if ((lcu1hdcVehicleBackward==0)&&(lcu1hdcVehicleForward==0))
    {
        if (lcu8HdcThetaGMode1Cnt>L_U8_TIME_10MSLOOP_50MS)
        {
            lcu1hdcVehicleForwardSus1Flg=1;
            lcu1hdcVehicleBackwardSus1Flg=0;
        }
        else if (lcu8HdcThetaGMode2Cnt>L_U8_TIME_10MSLOOP_50MS)
        {
            lcu1hdcVehicleForwardSus1Flg=0;
            lcu1hdcVehicleBackwardSus1Flg=1;
        }
        else
        {
            lcu1hdcVehicleForwardSus1Flg=0;
            lcu1hdcVehicleBackwardSus1Flg=0;
        }
    }
    else
    {
        lcu1hdcVehicleForwardSus1Flg=0;
        lcu1hdcVehicleBackwardSus1Flg=0;
    }

    /*******************************************************************/
    /*  check Vehicle dir   */
    if (lcu8HdcThetaGModeOld != lcu8HdcThetaGMode)
    {
        lcu8HdcRightThetaGCnt = 0;
        lcu1HdcRightThetaGFlg = 0;
    }
    else if ((lcu8HdcThetaGMode==1)||(lcu8HdcThetaGMode==2))
    {
        if (lcu8HdcRightThetaGCnt > L_U8_TIME_10MSLOOP_200MS)
        {
            lcu1HdcRightThetaGFlg = 1;
        }
        else
        {
            lcu8HdcRightThetaGCnt++;
            lcu1HdcRightThetaGFlg=0;
        }
    }
    else
    {
        lcu8HdcRightThetaGCnt = 0;
        lcu1HdcRightThetaGFlg = 0;
    }

    if ((lcs16HdcThetaG_max > LONG_0G)&&(abs(lcs16HdcThetaG0 - lcs16HdcThetaG1) > LONG_0G12G))
    {
        lcs16_hdc_tempW9 = 1;
    }
    else
    {
        lcs16_hdc_tempW9 = 0;
    }

    /*  Set */
    if (lcu1HdcVehicleDirbySensorFlg==0)
    {
        if ((lcs16_hdc_tempW9==1)
            &&(lcu1HdcRightThetaGFlg==1)
            &&(lcs16_hdc_tempW8==1))
        {
            if (lcu8HdcVehicleDirbySensorCnt>L_U8_TIME_10MSLOOP_1000MS)
            {
                lcu1HdcVehicleDirbySensorFlg = 1;
                lcu1HdcVehicleDirbySensorSusFlg =0;
            }
            else
            {
                if ((lcu8HdcVehicleDirbySensorCnt>L_U8_TIME_10MSLOOP_300MS)
                &&
                (((lcu1hdcVehicleForward==1)&&(lcs16HdcThetaG1==lcs16HdcThetaG_max))
                ||((lcu1hdcVehicleBackward==1)&&(lcs16HdcThetaG0==lcs16HdcThetaG_max)))
                )
                {
                    lcu1HdcVehicleDirbySensorSusFlg = 1;
                }
                else { }

                if (abs(lcs16HdcThetaG0 - lcs16HdcThetaG1) > LONG_0G2G)
                {
                    lcu8HdcVehicleDirbySensorCnt = lcu8HdcVehicleDirbySensorCnt+2;
                }
                else
                {
                    lcu8HdcVehicleDirbySensorCnt = lcu8HdcVehicleDirbySensorCnt+1;
                }
            }
        }
        else
        {
            if (lcu8HdcVehicleDirbySensorCnt > L_U8_TIME_10MSLOOP_100MS)
            {
                lcu8HdcVehicleDirbySensorCnt=lcu8HdcVehicleDirbySensorCnt - L_U8_TIME_10MSLOOP_100MS;
            }
            else
            {
                lcu8HdcVehicleDirbySensorCnt=0;
            }
        }
    }
    else
    {
        /*  Clear   */
        if ((lcs16_hdc_tempW9==0)
            ||(lcu1HdcRightThetaGFlg==0)
            ||(lcs16_hdc_tempW8==0))
        {
            lcu1HdcVehicleDirbySensorFlg = 0;
            lcu1HdcVehicleDirbySensorSusFlg =0;
        }
        /* Continue */
        else
        {
            lcu8HdcVehicleDirbySensorCnt=0;
        }
    }

    /*  TM N Gear (ThetaG Max)  */
    if (vref >= VREF_2_KPH)
    {
        if ((lcu1HdcVehicleDirbySensorFlg==1)&&(abs(lcs16HdcThetaG0 - lcs16HdcThetaG1) > LONG_0G12G))
        {
            if (lcs16HdcThetaG0 == lcs16HdcThetaG_max)
            {
                lcu1hdcVehicleForwardSensor =1;
                lcu1hdcVehicleBackwardSensor=0;
            }
            else if (lcs16HdcThetaG1 == lcs16HdcThetaG_max)
            {
                lcu1hdcVehicleForwardSensor =0;
                lcu1hdcVehicleBackwardSensor=1;
            }
            else
            {
                lcu1hdcVehicleForwardSensor =0;
                lcu1hdcVehicleBackwardSensor=0;
            }
        }
        else { }
    }
    else
    {
        lcu1hdcVehicleForwardSensor =0;
        lcu1hdcVehicleBackwardSensor=0;

        lcu1hdcVehicleForwardSus1Flg  =0;
        lcu1hdcVehicleBackwardSus1Flg =0;

        lcu1hdcVehicleForwardSus2Flg  =0;
        lcu1hdcVehicleBackwardSus2Flg =0;

        lcu1HdcVehicleDirbySensorSusFlg =0;
    }
}

void LCHDC_vChkVehicleDirThetag(void)
{
    /*
        Sub-Function Out Put
            1. lcs16HdcThetaG_N_OK_Flg
            2. lcu1HdcRightGearFlg
            3. lcu8HdcGearPosition
    */
    /********************************************************/

#if __Thetag_Dir
/*
  #if __INITIAL_FOR_BACK_CHK
    if (vref < VREF_1_KPH)
    {
        lcs16_hdc_tempW7 = 57;          
    }
    else
    {
        lcs16_hdc_tempW7 = 3;         
    }
  #else
    lcs16_hdc_tempW7 = 3;           
  #endif
*/
    if (vref > VREF_0_5_KPH)
    {
        if (lcu8HdcSpeedCnt<L_U8_TIME_10MSLOOP_1000MS)
        {
            lcu8HdcSpeedCnt++;
        }
        else { }
    }
    else
    {
        if ((lcu8HdcSpeedCnt>L_U8_TIME_10MSLOOP_300MS)&&(vref < VREF_0_5_KPH))
        {
            lcu8HdcThetaGDisableCnt = L_U8_TIME_10MSLOOP_1000MS;
        }
        else
        {
            lcu8HdcSpeedCnt++;
        }
    }

    if (lcu8HdcThetaGDisableCnt>0)
    {
        lcu8HdcThetaGDisableCnt --;
    }
    else { }

  #if __AX_Sensor_Buffer
    lcs16HdcAx1000gbuffer_20 = lcs16HdcAx1000gbuffer_19;
    lcs16HdcAx1000gbuffer_19 = lcs16HdcAx1000gbuffer_18;
    lcs16HdcAx1000gbuffer_18 = lcs16HdcAx1000gbuffer_17;
    lcs16HdcAx1000gbuffer_17 = lcs16HdcAx1000gbuffer_16;
    lcs16HdcAx1000gbuffer_16 = lcs16HdcAx1000gbuffer_15;
    lcs16HdcAx1000gbuffer_15 = lcs16HdcAx1000gbuffer_14;
    lcs16HdcAx1000gbuffer_14 = lcs16HdcAx1000gbuffer_13;
    lcs16HdcAx1000gbuffer_13 = lcs16HdcAx1000gbuffer_12;
    lcs16HdcAx1000gbuffer_12 = lcs16HdcAx1000gbuffer_11;
    lcs16HdcAx1000gbuffer_11 = lcs16HdcAx1000gbuffer_10;
    lcs16HdcAx1000gbuffer_10 = lcs16HdcAx1000gbuffer_9;
    lcs16HdcAx1000gbuffer_9 = lcs16HdcAx1000gbuffer_8;
    lcs16HdcAx1000gbuffer_8 = lcs16HdcAx1000gbuffer_7;
    lcs16HdcAx1000gbuffer_7 = lcs16HdcAx1000gbuffer_6;
    lcs16HdcAx1000gbuffer_6 = lcs16HdcAx1000gbuffer_5;
    lcs16HdcAx1000gbuffer_5 = lcs16HdcAx1000gbuffer_4;
    lcs16HdcAx1000gbuffer_4 = lcs16HdcAx1000gbuffer_3;
    lcs16HdcAx1000gbuffer_3 = lcs16HdcAx1000gbuffer_2;
    lcs16HdcAx1000gbuffer_2 = lcs16HdcAx1000gbuffer_1;
    lcs16HdcAx1000gbuffer_1 = lcs16HdcAx1000gbuffer_0;
    lcs16HdcAx1000gbuffer_0 = lcs16HdcAx1000g;
  #endif

    /* Scale Gain	*/
    lcs16_hdc_tempW9 = 10;  /*	max 65535 Limit 설정 필요.	*/

  #if __AX_Sensor_Buffer
    lcs16_hdc_tempW8 = lcs16HdcAx1000gbuffer_20;
  #else
    lcs16_hdc_tempW8 = lcs16HdcAx1000g;
  #endif
    lcs16_hdc_tempW8 = (lcs16_hdc_tempW8>500)?500:lcs16_hdc_tempW8;
    lcs16_hdc_tempW8 = (lcs16_hdc_tempW8>-500)?lcs16_hdc_tempW8: -500;
    lcs16_hdc_tempW8 = lcs16_hdc_tempW8*lcs16_hdc_tempW9;


    lcs16HdcAXg_filter_old = lcs16HdcAXg_filter;
    if (vref < VREF_1_KPH)
    {
        lcs16HdcAXg_filter=LCESP_s16Lpf1Int(lcs16_hdc_tempW8,lcs16HdcAXg_filter_old,L_U8FILTER_GAIN_10MSLOOP_18HZ);
    }
    else
    {
        lcs16HdcAXg_filter=LCESP_s16Lpf1Int(lcs16_hdc_tempW8,lcs16HdcAXg_filter_old,L_U8FILTER_GAIN_10MSLOOP_0_5HZ);
    }

    lcs16_hdc_tempW9 = 10;  /*	max 65535 Limit 설정 필요.	*/
    lcs16_hdc_tempW8 = lsabss16HDCWheelAccel;
    lcs16_hdc_tempW8 = (lcs16_hdc_tempW8>500)?500:lcs16_hdc_tempW8;
    lcs16_hdc_tempW8 = (lcs16_hdc_tempW8>-500)?lcs16_hdc_tempW8: -500;
    lcs16_hdc_tempW8 = lcs16_hdc_tempW8*lcs16_hdc_tempW9;

    lcs16HdcWhg_filter_old = lcs16HdcWhg_filter;
    if (vref < VREF_1_KPH)
    {
        lcs16HdcWhg_filter=LCESP_s16Lpf1Int(lcs16_hdc_tempW8,lcs16HdcWhg_filter_old,L_U8FILTER_GAIN_10MSLOOP_18HZ);
    }
    else
    {
        lcs16HdcWhg_filter=LCESP_s16Lpf1Int(lcs16_hdc_tempW8,lcs16HdcWhg_filter_old,L_U8FILTER_GAIN_10MSLOOP_0_5HZ);
    }

    /****************************/

    lcs16HdcWhg_filter_diff_old  =  lcs16HdcWhg_filter_diff;
    lcs16HdcAXg_filter_diff_old  =  lcs16HdcAXg_filter_diff;

    lcs16_hdc_tempW0 = abs(lcs16HdcWhg_filter - lcs16HdcWhg_filter_old)*10;
    lcs16_hdc_tempW1 = abs(lcs16HdcAXg_filter - lcs16HdcAXg_filter_old)*10;
    if (vref < VREF_1_KPH)
    {
        lcs16HdcWhg_filter_diff = LCESP_s16Lpf1Int(lcs16_hdc_tempW0,lcs16HdcWhg_filter_diff,L_U8FILTER_GAIN_10MSLOOP_18HZ);
        lcs16HdcAXg_filter_diff = LCESP_s16Lpf1Int(lcs16_hdc_tempW1,lcs16HdcAXg_filter_diff,L_U8FILTER_GAIN_10MSLOOP_18HZ);
    }
    else
    {
        lcs16HdcWhg_filter_diff = LCESP_s16Lpf1Int(lcs16_hdc_tempW0,lcs16HdcWhg_filter_diff,L_U8FILTER_GAIN_10MSLOOP_0_5HZ);
        lcs16HdcAXg_filter_diff = LCESP_s16Lpf1Int(lcs16_hdc_tempW1,lcs16HdcAXg_filter_diff,L_U8FILTER_GAIN_10MSLOOP_0_5HZ);	
    }

    /*********************************/

    lcs16HdcThetaG_F_old = lcs16HdcThetaG_F ;
    lcs16HdcThetaG_B_old = lcs16HdcThetaG_B ;

    lcs16HdcThetaG_F = (lcs16HdcWhg_filter - lcs16HdcAXg_filter);
    lcs16HdcThetaG_B = (lcs16HdcWhg_filter + lcs16HdcAXg_filter);

    lcs16HdcThetaG_F_diff  =    lcs16HdcThetaG_F - lcs16HdcThetaG_F_old;
    lcs16HdcThetaG_B_diff  =    lcs16HdcThetaG_B - lcs16HdcThetaG_B_old;

    lcs16_hdc_tempW0 = abs(lcs16HdcThetaG_F_diff)*10;
    lcs16_hdc_tempW1 = abs(lcs16HdcThetaG_B_diff)*10;

    if (vref < VREF_1_KPH)
    {
        lcs16HdcThetaG_F_diff_lpf = LCESP_s16Lpf1Int(lcs16_hdc_tempW0,lcs16HdcThetaG_F_diff_lpf,L_U8FILTER_GAIN_10MSLOOP_18HZ);
        lcs16HdcThetaG_B_diff_lpf = LCESP_s16Lpf1Int(lcs16_hdc_tempW1,lcs16HdcThetaG_B_diff_lpf,L_U8FILTER_GAIN_10MSLOOP_18HZ);
    }
    else
    {
        lcs16HdcThetaG_F_diff_lpf = LCESP_s16Lpf1Int(lcs16_hdc_tempW0,lcs16HdcThetaG_F_diff_lpf,L_U8FILTER_GAIN_10MSLOOP_0_5HZ);
        lcs16HdcThetaG_B_diff_lpf = LCESP_s16Lpf1Int(lcs16_hdc_tempW1,lcs16HdcThetaG_B_diff_lpf,L_U8FILTER_GAIN_10MSLOOP_0_5HZ);	
    }
    /*********************************/
    if (lcu8HdcWhgSusCnt < L_U8_TIME_10MSLOOP_100MS)
    {
        lcu8HdcWhgSusCnt++;
        lcs16HdcWhgSusmin = (lcs16HdcDecel>lcs16HdcWhgSusmin)?lcs16HdcWhgSusmin:lcs16HdcDecel;
        lcs16HdcWhgSusmax = (lcs16HdcDecel>lcs16HdcWhgSusmax)?lcs16HdcDecel:lcs16HdcWhgSusmax;
    }
    else
    {
        lcu8HdcWhgSusCnt=0;

        if (lcu8HdcWhgSusCnt2 <L_U8_TIME_10MSLOOP_70MS)
        {
            lcu8HdcWhgSusCnt2++;
        }
        else { }

        lcs16HdcWhgSusmin_3 = lcs16HdcWhgSusmin_2;
        lcs16HdcWhgSusmin_2 = lcs16HdcWhgSusmin_1;
        lcs16HdcWhgSusmin_1 = lcs16HdcWhgSusmin_0;
        lcs16HdcWhgSusmin_0 = lcs16HdcWhgSusmin;

        lcs16HdcWhgSusmax_3 = lcs16HdcWhgSusmax_2;
        lcs16HdcWhgSusmax_2 = lcs16HdcWhgSusmax_1;
        lcs16HdcWhgSusmax_1 = lcs16HdcWhgSusmax_0;
        lcs16HdcWhgSusmax_0 = lcs16HdcWhgSusmax;

        lcs16_hdc_tempW9 = lcs16HdcWhgSusmax;
        lcs16HdcWhgSusmax = lcs16HdcWhgSusmin;
        lcs16HdcWhgSusmin = lcs16_hdc_tempW9;

        lcs16_hdc_tempW4 = lcs16HdcWhgSusmin_0;
        lcs16_hdc_tempW4 = (lcs16HdcWhgSusmin_3>lcs16_hdc_tempW4)?lcs16_hdc_tempW4:lcs16HdcWhgSusmin_3;
        lcs16_hdc_tempW4 = (lcs16HdcWhgSusmin_2>lcs16_hdc_tempW4)?lcs16_hdc_tempW4:lcs16HdcWhgSusmin_2;
        lcs16_hdc_tempW4 = (lcs16HdcWhgSusmin_1>lcs16_hdc_tempW4)?lcs16_hdc_tempW4:lcs16HdcWhgSusmin_1;

        lcs16_hdc_tempW5 = lcs16HdcWhgSusmax_0;
        lcs16_hdc_tempW5 = (lcs16HdcWhgSusmax_3>lcs16_hdc_tempW5)?lcs16HdcWhgSusmax_3:lcs16_hdc_tempW5;
        lcs16_hdc_tempW5 = (lcs16HdcWhgSusmax_2>lcs16_hdc_tempW5)?lcs16HdcWhgSusmax_2:lcs16_hdc_tempW5;
        lcs16_hdc_tempW5 = (lcs16HdcWhgSusmax_1>lcs16_hdc_tempW5)?lcs16HdcWhgSusmax_1:lcs16_hdc_tempW5;

        if (lcu8HdcWhgSusCnt2 >= L_U8_TIME_10MSLOOP_30MS)
        {
            lcs16_hdc_tempW9 = LCESP_s16IInter3Point(abs(lcs16_hdc_tempW5 - lcs16_hdc_tempW4),  LONG_0G15G,     0,
                                                                                                LONG_0G2G,      L_U8_TIME_10MSLOOP_300MS,
                                                                                                LONG_0G3G,      L_U8_TIME_10MSLOOP_400MS );
            lcs16HdcWhgSusTimer = (lcs16_hdc_tempW9>lcs16HdcWhgSusTimer)?lcs16_hdc_tempW9:lcs16HdcWhgSusTimer;
        }
        else { }

    }

    if ((lcu1hdcVehicleForwardThetaG==1)||(lcu1hdcVehicleBackwardThetaG==1))
    {
        if (lcs16HdcWhgSusTimer>0)
        {
            lcs16HdcWhgSusTimer--;
        }
        else { }
    }
    else
    {
        lcs16HdcWhgSusTimer=0;
        lcu8HdcWhgSusCnt2 =0;
    }

    /*********************************/

	lcs16_hdc_tempW7 = LCESP_s16IInter3Point(vref,                              /*	8% Hill 기준	*/
                                                        VREF_1_KPH,6,
                                                        VREF_10_KPH,58,
                                                        VREF_20_KPH,115);

    lcs16_hdc_tempW8 = LCESP_s16IInter3Point(vref,                              /*	16% Hill 기준	*/
                                                        VREF_1_KPH,12,
                                                        VREF_10_KPH,115,
                                                        VREF_20_KPH,230);

    if ((vref < VREF_1_5_KPH )||(lcu8HdcThetaGDisableCnt>0))
    {
        lcs16HdcThetaG_F_cnt=0;
        lcs16HdcThetaG_B_cnt=0;

        lcu1hdcVehicleForwardThetaG  = 0;
        lcu1hdcVehicleBackwardThetaG = 0;
    }
    else
    {
        if ((lcu1hdcVehicleForwardThetaG == 1)||(lcu1hdcVehicleBackwardThetaG ==1))
        {
            lcs16_hdc_tempW9 = YAW_15DEG;
        }
        else
        {
            lcs16_hdc_tempW9 = YAW_20DEG;
        }

        /* 변화율 제한치 최소 20% 이상 */

        if ((lcu1hdcVehicleForwardThetaG==1)||(lcu1hdcVehicleBackwardThetaG==1))
        {
            lcs16_hdc_tempW6 = 30;      /*	Nth	*/
        }
        else
        {
            lcs16_hdc_tempW6 = 10;      /*	1st	*/
        }

        lcs16_hdc_tempW1 = (lcs16HdcWhg_filter_diff>lcs16HdcAXg_filter_diff)?lcs16HdcWhg_filter_diff:lcs16HdcAXg_filter_diff;   /*	max	*/
        lcs16_hdc_tempW2 = (lcs16HdcWhg_filter_diff>lcs16HdcAXg_filter_diff)?lcs16HdcAXg_filter_diff:lcs16HdcWhg_filter_diff;   /*	min	*/

		lcs16_hdc_tempW0 = (int16_t)((((int32_t)lcs16_hdc_tempW1)*lcs16_hdc_tempW6)/100);

        lcs16_hdc_tempW3 = (lcs16_hdc_tempW2>=lcs16_hdc_tempW0)?1:0;

        if ((abs(yaw_out)<lcs16_hdc_tempW9)&&(lcs16_hdc_tempW3==1)&&(lcs16HdcWhgSusTimer==0))
        {

            lcs16_hdc_tempW2 = 50;      /*	 최소한 비율 설정.	*/
            
			lcs16_hdc_tempW0 = (int16_t)((((int32_t)lcs16HdcThetaG_F_diff_lpf)*lcs16_hdc_tempW2)/100);            
			lcs16_hdc_tempW1 = (int16_t)((((int32_t)lcs16HdcThetaG_B_diff_lpf)*lcs16_hdc_tempW2)/100);

            /* 절대 크기 지정   */

            if ((lcs16HdcThetaG_F_diff_lpf > lcs16HdcThetaG_B_diff_lpf)
                &&(lcs16HdcThetaG_F_diff_lpf > lcs16_hdc_tempW8)
                &&(lcs16_hdc_tempW0 >lcs16HdcThetaG_B_diff_lpf ))
            {
                lcs16HdcThetaG_F_cnt = lcs16HdcThetaG_F_cnt+2;
                lcs16HdcThetaG_B_cnt --;
            }
            else if ((lcs16HdcThetaG_B_diff_lpf > lcs16HdcThetaG_F_diff_lpf)
                &&(lcs16HdcThetaG_B_diff_lpf > lcs16_hdc_tempW8)
                &&(lcs16_hdc_tempW1 >lcs16HdcThetaG_F_diff_lpf ))
            {
                lcs16HdcThetaG_F_cnt--;
                lcs16HdcThetaG_B_cnt = lcs16HdcThetaG_B_cnt+2;
            }
            else if ((lcs16HdcThetaG_F_diff_lpf > lcs16HdcThetaG_B_diff_lpf)
                &&(lcs16HdcThetaG_F_diff_lpf > lcs16_hdc_tempW7)
                &&(lcs16_hdc_tempW0 >lcs16HdcThetaG_B_diff_lpf ))
            {
                lcs16HdcThetaG_F_cnt++;
                lcs16HdcThetaG_B_cnt--;
            }
            else if ((lcs16HdcThetaG_B_diff_lpf > lcs16HdcThetaG_F_diff_lpf)
                &&(lcs16HdcThetaG_B_diff_lpf > lcs16_hdc_tempW7)
                &&(lcs16_hdc_tempW1 >lcs16HdcThetaG_F_diff_lpf ))
            {
                lcs16HdcThetaG_F_cnt--;
                lcs16HdcThetaG_B_cnt++;
            }
            else
            {
                lcs16HdcThetaG_F_cnt--;
                lcs16HdcThetaG_B_cnt--;
            }

            lcs16HdcThetaG_F_cnt = (lcs16HdcThetaG_F_cnt>L_U16_TIME_10MSLOOP_5S)?L_U16_TIME_10MSLOOP_5S:lcs16HdcThetaG_F_cnt;
            lcs16HdcThetaG_B_cnt = (lcs16HdcThetaG_B_cnt>L_U16_TIME_10MSLOOP_5S)?L_U16_TIME_10MSLOOP_5S:lcs16HdcThetaG_B_cnt;

            lcs16HdcThetaG_F_cnt = (lcs16HdcThetaG_F_cnt>0)?lcs16HdcThetaG_F_cnt:0;
            lcs16HdcThetaG_B_cnt = (lcs16HdcThetaG_B_cnt>0)?lcs16HdcThetaG_B_cnt:0;

            /*	 판단 시간 설정.	*/
            if ((lcu1hdcVehicleForwardThetaG==1)||(lcu1hdcVehicleBackwardThetaG==1))
            {
                lcs16_hdc_tempW9 = ((lcs16HdcThetaG_cnt+1)*L_U8_TIME_10MSLOOP_500MS);    			/*	Nth	*/
                lcs16_hdc_tempW9 = (lcs16_hdc_tempW9>L_U16_TIME_10MSLOOP_5S)?L_U16_TIME_10MSLOOP_5S:lcs16_hdc_tempW9; 	/*	Max	*/
            }
            else
            {
                if(lcu1HdcActiveFlg==0)
                {
                    lcs16_hdc_tempW9 = L_U8_TIME_10MSLOOP_500MS;
                }
                else
                {
                    lcs16_hdc_tempW9 = L_U8_TIME_10MSLOOP_300MS;
                }
            }

            if (lcs16HdcThetaG_F_cnt >= lcs16_hdc_tempW9)
            {
                lcu1hdcVehicleForwardThetaG  =0;
                lcu1hdcVehicleBackwardThetaG =1;

                lcs16HdcThetaG_F_cnt=0;
                lcs16HdcThetaG_B_cnt=0;
                lcs16HdcThetaG_cnt = lcs16HdcThetaG_cnt+1;
            }
            else { }

            if (lcs16HdcThetaG_B_cnt >= lcs16_hdc_tempW9)
            {
                lcu1hdcVehicleForwardThetaG  = 1;
                lcu1hdcVehicleBackwardThetaG = 0;

                lcs16HdcThetaG_F_cnt=0;
                lcs16HdcThetaG_B_cnt=0;
                lcs16HdcThetaG_cnt = lcs16HdcThetaG_cnt+1;
            }
            else { }
        }
        else
        {
            lcs16HdcThetaG_F_cnt=0;
            lcs16HdcThetaG_B_cnt=0;
        }
    }
    /****************/
    if ((lcu1hdcVehicleForwardThetaG==0)&&(lcu1hdcVehicleBackwardThetaG==0))
    {
        lcs16HdcThetaG_cnt = 0;
    }
    else { }

    /****************/
    if ((HDC_TM_TYPE==HDC_MANUAL_TM)&&(lcs16HdcThetaG_N_OK_Flg==0)&&(vref < VREF_5_KPH ))
    {
        if ((lcs16HdcWhg_filter_diff > 40)&&(lcs16HdcAXg_filter_diff < 5) 		/* 판단 기준 Check.	*/
            &&(lcs16HdcWhg_filter>LONG_0G02G*10))
        {
            if (lcs16HdcThetaG_N_cnt<L_U8_TIME_10MSLOOP_300MS)
            {
                lcs16HdcThetaG_N_cnt++;
                if (lcs16HdcWhg_filter_diff >  80)
                {
                    lcs16HdcThetaG_N_cnt++;
                }
                else { }
            }
            else
            {
                lcs16HdcThetaG_N_OK_Flg=1;
                lcs16HdcThetaG_N_cnt = L_U8_TIME_10MSLOOP_600MS;
            }
        }
        else
        {
            lcs16HdcThetaG_N_cnt = (lcs16HdcThetaG_N_cnt>0)?(lcs16HdcThetaG_N_cnt--):0;
        }
    }
    else    																	/* clear 조건. 유지 조건.,Time Clear 조건	*/
    {
        if ((lcs16HdcThetaG_N_cnt>0)&&(lcs16HdcWhg_filter>LONG_0G01G*10))
        {
            lcs16HdcThetaG_N_cnt--;
            lcs16HdcThetaG_N_OK_Flg=1;
        }
        else
        {
            lcs16HdcThetaG_N_cnt=0;
            lcs16HdcThetaG_N_OK_Flg=0;
        }
    }

#endif
}

#if __YAW_FOR_BACK_CHK
void LCHDC_vChkVehicleDirYaw(void)
{

    if (SAS_CHECK_OK==1)
    {
        lcs16_hdc_tempW9 = YAW_10DEG;
    }
    else
    {
        lcs16_hdc_tempW9 = YAW_15DEG;
    }

    if (vref >= VREF_2_KPH)
    {
        if (((yaw_out > lcs16_hdc_tempW9)&&(rf > lcs16_hdc_tempW9))
            ||((yaw_out < -lcs16_hdc_tempW9)&&(rf < -lcs16_hdc_tempW9)))
        {
            lcs16HdcYaw_F_cnt++;
            lcs16HdcYaw_B_cnt--;
        }
        else if (((yaw_out > lcs16_hdc_tempW9)&&(rf < -lcs16_hdc_tempW9))
            ||((yaw_out < -lcs16_hdc_tempW9)&&(rf > lcs16_hdc_tempW9)))
        {
            lcs16HdcYaw_F_cnt--;
            lcs16HdcYaw_B_cnt++;
        }
        else
        {
            lcs16HdcYaw_F_cnt--;
            lcs16HdcYaw_B_cnt--;
        }

        lcs16HdcYaw_F_cnt = (lcs16HdcYaw_F_cnt>L_U16_TIME_10MSLOOP_5S)?L_U16_TIME_10MSLOOP_5S:lcs16HdcYaw_F_cnt;
        lcs16HdcYaw_B_cnt = (lcs16HdcYaw_B_cnt>L_U16_TIME_10MSLOOP_5S)?L_U16_TIME_10MSLOOP_5S:lcs16HdcYaw_B_cnt;

        lcs16HdcYaw_F_cnt = (lcs16HdcYaw_F_cnt>0)?lcs16HdcYaw_F_cnt:0;
        lcs16HdcYaw_B_cnt = (lcs16HdcYaw_B_cnt>0)?lcs16HdcYaw_B_cnt:0;


        if (SAS_CHECK_OK==1)
        {
            lcs16_hdc_tempW9 = L_U8_TIME_10MSLOOP_500MS;
        }
        else
        {
            lcs16_hdc_tempW9 = L_U8_TIME_10MSLOOP_1000MS;
        }

        if (lcs16HdcYaw_F_cnt >= lcs16_hdc_tempW9)
        {
            lcu1hdcVehicleForwardYaw  =1;
            lcu1hdcVehicleBackwardYaw =0;

            lcu1hdcVehicleBackwardThetaG = 0;
            lcu1hdcVehicleBackwardSensor = 0;
        }
        else { }

        if (lcs16HdcYaw_B_cnt >= lcs16_hdc_tempW9)
        {
            lcu1hdcVehicleForwardYaw  = 0;
            lcu1hdcVehicleBackwardYaw = 1;

            lcu1hdcVehicleForwardThetaG = 0;
            lcu1hdcVehicleForwardSensor = 0;
        }
        else { }

    }
    else
    {
        lcu1hdcVehicleForwardYaw  =0;
        lcu1hdcVehicleBackwardYaw =0;
        lcs16HdcYaw_F_cnt=0;
        lcs16HdcYaw_B_cnt=0;
    }
}
#endif

void LCHDC_vChkVehicleDirection(void)
{
    LCHDC_vChkEnginestate();

  #if __STOP_ON_HILL
    LCHDC_vChkVehicleStopOnHill();
  #endif

    LCHDC_vChkVehicleDirINGear();

    LCHDC_vChkVehicleDirNGear();

    LCHDC_vChkVehicleDirThetag();

  #if __YAW_FOR_BACK_CHK
    if((fu1YawErrorDet==1)||(fu1YawSusDet==1))
    {
    	lcu1hdcVehicleForwardYaw  =0;
        lcu1hdcVehicleBackwardYaw =0;
        lcs16HdcYaw_F_cnt=0;
        lcs16HdcYaw_B_cnt=0;
    }
    else
    {
        LCHDC_vChkVehicleDirYaw();
    }
  #endif
    if ((lcu1HdcRightGearFlg==1)
        &&(HDC_TM_TYPE==HDC_AUTO_TM)
        &&(lcu8HdcGearPosition != U8_HDC_NEU_GEAR)
        &&(lcu8HdcGearPosition != U8_HDC_PARK_GEAR)
        &&((lcu1hdcVehicleForwardTM==1)||(lcu1hdcVehicleBackwardTM==1))
        )
    {
        lcu1hdcVehicleForward = lcu1hdcVehicleForwardTM;
        lcu1hdcVehicleBackward= lcu1hdcVehicleBackwardTM;
    }
    else
    {
        if (vref >= VREF_80_KPH)    /* Max Backward Speed Check */
        {
            if ((lcu1hdcVehicleForward ==0)&&(lcu1hdcVehicleBackward==0))
            {
                lcu1hdcVehicleForward =1;
                lcu1hdcVehicleBackward=0;
            }
            else { }
        }
        else if (vref >= VREF_2_KPH)        /* Min Speed    */
        {
            /*  TM Gear에 의해 이전 주행 기억정보 사용. */
            if ((HDC_TM_TYPE==HDC_MANUAL_TM)&&((lcu1hdcVehicleForwardTM == 1)||(lcu1hdcVehicleBackwardTM==1)))
            {
                lcu1hdcVehicleForward = lcu1hdcVehicleForwardTM;
                lcu1hdcVehicleBackward= lcu1hdcVehicleBackwardTM;
            }
            /* 	Theta G 위상.	*/
            else if ((lcu1hdcVehicleForwardThetaG == 1)||(lcu1hdcVehicleBackwardThetaG==1))
            {
                lcu1hdcVehicleForward = lcu1hdcVehicleForwardThetaG;
                lcu1hdcVehicleBackward= lcu1hdcVehicleBackwardThetaG;
            }
            /*  N Gear 주행 정보 입력   */
            else if ((lcu1hdcVehicleForwardSensor == 1)||(lcu1hdcVehicleBackwardSensor==1))
            {
                lcu1hdcVehicleForward = lcu1hdcVehicleForwardSensor;
                lcu1hdcVehicleBackward= lcu1hdcVehicleBackwardSensor;
            }
          #if __YAW_FOR_BACK_CHK
            else if ((lcu1hdcVehicleForwardYaw == 1)||(lcu1hdcVehicleBackwardYaw==1))
            {
                lcu1hdcVehicleForward = lcu1hdcVehicleForwardYaw;
                lcu1hdcVehicleBackward= lcu1hdcVehicleBackwardYaw;
            }
          #endif
            /*  N Gear에 의한 판단 이전 우선 이상판단   */
            else if (lcu1HdcVehicleDirbySensorSusFlg==1)
            {
                lcu1hdcVehicleForward  = 0;
                lcu1hdcVehicleBackward = 0;
            }
            /*  TM Gear에 의해 이전 주행 기억정보 사용. */
            else if ((HDC_TM_TYPE==HDC_AUTO_TM)&&((lcu1hdcVehicleForwardTM == 1)||(lcu1hdcVehicleBackwardTM==1)))
            {
                lcu1hdcVehicleForward = lcu1hdcVehicleForwardTM;
                lcu1hdcVehicleBackward= lcu1hdcVehicleBackwardTM;
            }
            else              /*  이전 주행 정보 유지.    */
            {
                lcu1hdcVehicleForward  = lcu1hdcVehicleForward;
                lcu1hdcVehicleBackward = lcu1hdcVehicleBackward;
            }
        }
        else if (vref >= VREF_1_KPH)
        {
			lcu1hdcVehicleForward  = lcu1hdcVehicleForward;
			lcu1hdcVehicleBackward = lcu1hdcVehicleBackward;
        }
        else                            /* Default      */
        {
            lcu1hdcVehicleForward =0;
            lcu1hdcVehicleBackward=0;
        }
    }
/*
    lcu8hdcVehicleDirction = 0;

    if ((lcu1hdcVehicleForwardTM==1)&&(lcu1hdcVehicleBackwardTM==0))
    {
        lcu8hdcVehicleDirction = 1;
    }
    else if ((lcu1hdcVehicleForwardTM==0)&&(lcu1hdcVehicleBackwardTM==1))
    {
        lcu8hdcVehicleDirction = 2;
    }
    else if ((lcu1hdcVehicleForwardTM==0)&&(lcu1hdcVehicleBackwardTM==0))
    {
        lcu8hdcVehicleDirction = 3;
    }
    else
    {
        lcu8hdcVehicleDirction = 4;
    }

    if ((lcu1hdcVehicleForwardSensor==1)&&(lcu1hdcVehicleBackwardSensor==0))
    {
        lcu8hdcVehicleDirction = lcu8hdcVehicleDirction + 10;
    }
    else if ((lcu1hdcVehicleForwardSensor==0)&&(lcu1hdcVehicleBackwardSensor==1))
    {
        lcu8hdcVehicleDirction = lcu8hdcVehicleDirction + 20;
    }
    else if ((lcu1hdcVehicleForwardSensor==0)&&(lcu1hdcVehicleBackwardSensor==0))
    {
        lcu8hdcVehicleDirction = lcu8hdcVehicleDirction + 30;
    }
    else
    {
        lcu8hdcVehicleDirction = lcu8hdcVehicleDirction + 40;
    }
*/
}

void LCHDC_vChkVehicleDirection_MT(void)
{
    lcs16_hdc_tempW9 = (lcs16HdcThetaG1>lcs16HdcThetaG0)?lcs16HdcThetaG0:lcs16HdcThetaG1;

    if ((HDC_TM_TYPE==HDC_AUTO_TM)
        ||(lcu1hdcVehicleForward  == 1)
        ||(lcu1hdcVehicleBackward == 1)
      #if __INITIAL_FOR_BACK_CHK
        ||(lcu1HdcEngTorq_N_OKFlg==1)
      #endif
        ||(lcs16_hdc_tempW9 > lcs16HdcSmaThetaGTh)
    )
    {
        lcu1hdcVehicleDirectionOKFlg = 1;
    }
    else
    {
        lcu1hdcVehicleDirectionOKFlg = 0;
    }
}

void LCHDC_vCompLongGSensor(void)
{
    /*
    RAXTON      45
    HM 70       5
    HM 90       50
    HM 120      15
    HM 100      55
    */

    /*  Long G Offset Compensateion (- 민감, + 둔감.)    */
  #if __LongG_Offset_Test
    #if __CAR == SYC_KYRON
    lcs16HdcAx1000g = lss16absAxOffsetComp1000g + 48;
    #elif __CAR == GM_C100
    lcs16HdcAx1000g = lss16absAxOffsetComp1000g + 45;
    #else
	lcs16HdcAx1000g = lss16absAxOffsetComp1000g + (int16_t)S8_ABS_TEMP_1;
	#endif
  #else
    lcs16HdcAx1000g = lss16absAxOffsetComp1000g;
  #endif

    /* ECS Air-Sus : Long G Compensation    */
    if (lcu8HdcEcsCompEnable==1)
    {
        lcs16HdcAx1000g = lcs16HdcAx1000g + lcs16HdcEcsLongGComp;
    }
    else
    {
        lcs16HdcAx1000g = lcs16HdcAx1000g;
    }
    /*~ ECS Air-Sus : Long G Compensation   */


    if (lcu8hdcLongOffsetCnt<L_U8_TIME_10MSLOOP_100MS)
    {
       lcu8hdcLongOffsetCnt++;

        /* Max 300*T_100_MS(14) = 4200  */
        if (lcs16HdcAx1000g > LONG_0G3G)
        {
            lcs16_hdc_tempW9 = LONG_0G3G;
        }
        else if (lcs16HdcAx1000g < -LONG_0G3G)
        {
            lcs16_hdc_tempW9 = -LONG_0G3G;
        }
        else
        {
            lcs16_hdc_tempW9 = lcs16HdcAx1000g;
        }

        lcs16hdcLongOffsetSum = lcs16hdcLongOffsetSum + lcs16_hdc_tempW9;
    }
    else
    {
       lcu8hdcLongOffsetCnt=0;

        lcs16hdcLongOffsetAvrold2 = lcs16hdcLongOffsetAvrold1;
        lcs16hdcLongOffsetAvrold1 = lcs16hdcLongOffsetAvr;
        lcs16hdcLongOffsetAvr     = lcs16hdcLongOffsetSum/L_U8_TIME_10MSLOOP_100MS;
        lcs16hdcLongOffsetSum=0;
        lcs16hdcLongOffsetAvrlfOld = lcs16hdcLongOffsetAvrlf;
        lcs16hdcLongOffsetAvrlf = LCESP_s16Lpf1Int(lcs16hdcLongOffsetAvr,lcs16hdcLongOffsetAvrlfOld,L_U8FILTER_GAIN_10MSLOOP_23HZ);        /*  23hz  */
    }

    /* check signal During Dectioin Flat by Press   */
    if ((lcu8HdcFlatPreCnt>0)||(lcs16hdcFlatByPressOffCnt < L_U8_TIME_10MSLOOP_100MS))
    {
      #if ((__PARK_BRAKE_TYPE!=NONE) && (__CAR == KMC_HM))  /* Parking Brake Signal On */
        if(fu1DelayedParkingBrakeSignal==1)
        {
            lcu1hdcFlatPresSuspectFlg=1;
        }
        else
        {
            lcu1hdcFlatPresSuspectFlg=0;
        }
      #endif
    }
    else
    {
        lcu1hdcFlatPresSuspectFlg=0;
    }


    if (lcs16hdcFlatByPressOffCnt < L_U8_TIME_10MSLOOP_150MS)
    {
        ;   /* Wait */
    }
    else if (lcs16hdcFlatByPressOffCnt == L_U8_TIME_10MSLOOP_150MS) /* Call */
    {
        if ((abs(lcs16hdcLongOffsetAvrold2 - lcs16hdcLongOffsetAvrold1)<LONG_0G06G)
            &&(abs(lcs16hdcLongOffsetAvrold2 - lcs16hdcLongOffsetAvr)<LONG_0G06G)
            &&(abs(lcs16hdcLongOffsetAvr - lcs16hdcLongOffsetAvrold1)<LONG_0G06G))
        {
            lcs16_hdc_tempW7 = 1;
        }
        else
        {
            lcs16_hdc_tempW7 = 0;
        }

        if (((lcu1hdcVehicleForward==1)
            &&(lcs16hdcLongOffsetAvr<-LONG_0G03G)
            &&(lcs16hdcLongOffsetAvrold1<-LONG_0G03G)
            &&(lcs16hdcLongOffsetAvrold2<-LONG_0G03G))
            ||
            ((lcu1hdcVehicleBackward==1)
            &&(lcs16hdcLongOffsetAvr>LONG_0G03G)
            &&(lcs16hdcLongOffsetAvrold1>LONG_0G03G)
            &&(lcs16hdcLongOffsetAvrold2>LONG_0G03G)))
        {
            lcs16_hdc_tempW8 = 1;
        }
        else
        {
            lcs16_hdc_tempW8 = 0;
        }

        if ((lcs16_hdc_tempW7==1)&&(lcs16_hdc_tempW8==1)&&(lcu1hdcFlatPresSuspectFlg ==0))
        {
            lcs16hdcLongOffsetAvr300MS2 = lcs16hdcLongOffsetAvr300MS1;
            lcs16hdcLongOffsetAvr300MS1 = lcs16hdcLongOffsetAvr300MS;
            lcs16hdcLongOffsetAvr300MS = (lcs16hdcLongOffsetAvr+lcs16hdcLongOffsetAvrold1+lcs16hdcLongOffsetAvrold2)/3;

            /* 300msc 유효성 판단부분   */
            if ((abs(lcs16hdcLongOffsetAvr300MS - lcs16hdcLongOffsetAvr300MS1)<LONG_0G06G)
                &&(abs(lcs16hdcLongOffsetAvr300MS - lcs16hdcLongOffsetAvr300MS2)<LONG_0G06G)
                &&(abs(lcs16hdcLongOffsetAvr300MS1 - lcs16hdcLongOffsetAvr300MS2)<LONG_0G06G))
            {
                lcs16_hdc_tempW6 = 1;
            }
            else
            {
                lcs16_hdc_tempW6 = 0;
            }

            if (((lcu1hdcVehicleForward==1)&&(lcs16_hdc_tempW6==1)
                &&(lcs16hdcLongOffsetAvr300MS<-LONG_0G03G)
                &&(lcs16hdcLongOffsetAvr300MS1<-LONG_0G03G)
                &&(lcs16hdcLongOffsetAvr300MS2<-LONG_0G03G))
                ||
                ((lcu1hdcVehicleBackward==1)&&(lcs16_hdc_tempW6==1)
                &&(lcs16hdcLongOffsetAvr300MS>LONG_0G03G)
                &&(lcs16hdcLongOffsetAvr300MS1>LONG_0G03G)
                &&(lcs16hdcLongOffsetAvr300MS2>LONG_0G03G)))
            {
                lcs16hdcLongOffsetAvrage = (lcs16hdcLongOffsetAvr300MS+lcs16hdcLongOffsetAvr300MS1+lcs16hdcLongOffsetAvr300MS2)/3;

                if (lcu8HdcLongGSuspectCnt < L_U8_TIME_10MSLOOP_40MS)
                {
                    lcu8HdcLongGSuspectCnt++;
                }
                else { }

                if (lcu8HdcLongGSuspectCnt >= L_U8_TIME_10MSLOOP_30MS)
                {
                    lcu1HdcLongGSuspectFlg =1;      /*  Clear 조건 확인 */
                    lcs16HdcSuspectLongGComp = (-lcs16hdcLongOffsetAvrage) * lcu8HdcLongGSuspectCnt / L_U8_TIME_10MSLOOP_40MS; /* (3,4,5에서 진입) */
                }
                else { }
            }
            else
            {
                lcu8HdcLongGSuspectCnt=0;
            }
        }
        else
        {
            /* 100MS*3 DATA Suspect */
        }
    }
    else if ((lcs16hdcFlatByPressOffCnt%L_U16_TIME_10MSLOOP_5S)==0)
    {
        if (lcu8HdcLongGSuspectCnt>0)
        {
            lcu8HdcLongGSuspectCnt--;
        }
        else { }
    }
    else if (lcs16hdcFlatByPressOffCnt > L_U16_TIME_10MSLOOP_20S)
    {
        lcu8HdcLongGSuspectCnt=0;
    }
    else { }

  #if __Comp_Long_G_by_Press
    /* Long G Suspect : Long G Compensation    */
    if (lcu1HdcLongGSuspectFlg==1)
    {
        lcs16_hdc_tempW8 = lcs16HdcAx1000g + lcs16HdcSuspectLongGComp;
    }
    else
    {
        lcs16_hdc_tempW8 = lcs16HdcAx1000g;
    }
    /*~ Long G Suspect : Long G Compensation    */
  #else
    lcs16_hdc_tempW8 = lcs16HdcAx1000g;
  #endif

    /* Vehicle dirction Comp    */
    if (lcu1hdcVehicleBackward==1)
    {
        lcs16_hdc_tempW9 = -lcs16_hdc_tempW8;
    }
    else if (lcu1hdcVehicleForward==1)
    {
        lcs16_hdc_tempW9 = lcs16_hdc_tempW8;
    }
    else    /* 0, 0 or 1, 1 */
    {
        if (lcu1hdcVehicleBackwardSus1Flg==1)
        {
            lcs16_hdc_tempW9 = -lcs16_hdc_tempW8;
        }
        else if (lcu1hdcVehicleForwardSus1Flg==1)
        {
            lcs16_hdc_tempW9 = lcs16_hdc_tempW8;
        }
        else    /* default forward  */
        {
            lcs16_hdc_tempW9 = lcs16_hdc_tempW8;
        }
    }

    lcs16HdcASenOld=lcs16HdcASen;
    lcs16HdcASen=LCESP_s16Lpf1Int(lcs16_hdc_tempW9,lcs16HdcASenOld,L_U8FILTER_GAIN_10MSLOOP_43HZ);    /* 43hz */

    lcs16HdcASenOld1=lcs16HdcASen1;
    lcs16HdcASen1=LCESP_s16Lpf1Int(-lcs16_hdc_tempW8,lcs16HdcASenOld1,L_U8FILTER_GAIN_10MSLOOP_43HZ);  /* 43hz */

    lcs16HdcASenOld0=lcs16HdcASen0;
    lcs16HdcASen0=LCESP_s16Lpf1Int(lcs16_hdc_tempW8,lcs16HdcASenOld0,L_U8FILTER_GAIN_10MSLOOP_43HZ);   /* 43hz */

}


void LCHDC_vChkEnteringConds(void)
{
      #if __TARGET_SPD_ADAPTATION
    if(HDC_TM_TYPE==HDC_AUTO_TM)
    {
        lcs16_hdc_tempW2 = S16_HDC_MAX_TARGET_SPD_AT;  /* S16_HDC_AT_NEU_TG_SPD : AT max Target Spd */
    }
    else
    {
        lcs16_hdc_tempW2 = S16_HDC_MAX_TARGET_SPD_MT;  /* S16_HDC_MT_2ND_TG_SPD : MT max Target Spd */
    }	
      #endif

	  #if defined(__dDCT_INTERFACE)
    if(((lcu1HdcSwitchFlg==1)||(lcu1HrcOnReqFlg==1))&&(lcu1HdcInhibitFlg==0)&&(vref<(int16_t)S16_HDC_TERMINATE_SPD)  
      #else
    if((lcu1HdcSwitchFlg==1)&&(lcu1HdcInhibitFlg==0)&&(vref<(int16_t)S16_HDC_TERMINATE_SPD)
      #endif
      #if __TARGET_SPD_ADAPTATION
    &&(lcu1HdcMpressOn==0)&&(ABS_fz==0)&&(BTCS_ON==0)
      #endif	
    )
    {
	  #if __STOP_ON_HILL
        if(((lcu1HdcDownhillFlg==1)||(lcu1HdcDownhillAfeterStopFlg==1)||(lcu1hdcBackRollHillFlg==1))
           &&(lcu8HdcDriverBrakeIntend==0)&&(lcu8HdcDriverAccelIntend==0)
           &&(vref>=S16_HDC_ACT_SPD_MIN)&&(vref<=S16_HDC_ACT_SPD_MAX)
           &&(lcu8HdcBumpEstTime>=lcu8HdcBumpDelayActTime)&&(lcu1HdcDiscTempErrFlg==0))
      #else
        if((lcu1HdcDownhillFlg==1)&&(lcu8HdcDriverAccelIntend==0)&&(lcu8HdcDriverBrakeIntend==0)
           &&(vref<=S16_HDC_ACT_SPD_MAX)
           &&(lcu8HdcBumpEstTime>=lcu8HdcBumpDelayActTime)&&(lcu1HdcDiscTempErrFlg==0))
      #endif

        {
                lcu8HdcEnteringChkResult=U8_HDC_ENTER;
        }
        else
        {
            lcu8HdcEnteringChkResult=U8_HDC_READY;
        }
    }
    else
    {
        lcu8HdcEnteringChkResult=U8_HDC_EXIT;
    }
}


void LCHDC_vChkExitConds(void)
{
      #if __TARGET_SPD_ADAPTATION
    if(HDC_TM_TYPE==HDC_AUTO_TM)
    {
        lcs16_hdc_tempW2 = S16_HDC_MAX_TARGET_SPD_AT + VREF_1_KPH;  /* S16_HDC_AT_NEU_TG_SPD : AT max Target Spd */
    }
    else
    {
        lcs16_hdc_tempW2 = S16_HDC_MAX_TARGET_SPD_MT + VREF_1_KPH;  /* S16_HDC_MT_2ND_TG_SPD : MT max Target Spd */
    }	
    
    lcs16_hdc_tempW3 = S16_HDC_ACT_SPD_MIN - VREF_3_KPH;
    if(lcs16_hdc_tempW3<VREF_1_5_KPH) {lcs16_hdc_tempW3 = VREF_1_5_KPH;}
    else { } 
    
    if(U8_HDC_TARGET_SPD_ADAPT_MODE==HDC_ACT_AT_ACCEL_BRAKE)
    {
    	if(((vref>lcs16_hdc_tempW2)&&((lcu8HdcDriverAccelIntend==1)||(lcu8HdcDriverBrakeIntend==1)))||(BTCS_ON==1))
    	{
    		lcs16_hdc_tempW4 = 1;
    	}
    	else
    	{
    		lcs16_hdc_tempW4 = 0;
    	}
    	
    	if((lcu8HdcDriverBrakeIntend==1)&&(vref<=lcs16_hdc_tempW3))
    	{
    		lcs16_hdc_tempW5 = 1;
    	}
    	else
    	{
    		lcs16_hdc_tempW5 = 0;
    	}
    }
    else if(U8_HDC_TARGET_SPD_ADAPT_MODE==HDC_DE_ACT_AT_ACCEL_BRAKE)
    {
    	if(lcu8HdcDriverAccelIntend==1)
		{
    		lcs16_hdc_tempW4 = 1;
    	}
    	else
    	{
    		lcs16_hdc_tempW4 = 0;
    	} 
    	
    	if(lcu8HdcDriverBrakeIntend==1)
    	{
    		lcs16_hdc_tempW5 = 1;
    	}
    	else
    	{
    		lcs16_hdc_tempW5 = 0;
    	}   	
    }
    else /* default U8_HDC_TARGET_SPD_ADAPT_MODE==HDC_DE_ACT_AT_ACCEL_BRAKE */
    {
    	if(lcu8HdcDriverAccelIntend==1)
		{
    		lcs16_hdc_tempW4 = 1;
    	}
    	else
    	{
    		lcs16_hdc_tempW4 = 0;
    	} 
    	
    	if(lcu8HdcDriverBrakeIntend==1)
    	{
    		lcs16_hdc_tempW5 = 1;
    	}
    	else
    	{
    		lcs16_hdc_tempW5 = 0;
    	}
    }
      #endif

      #if defined(__dDCT_INTERFACE)
    if(((lcu1HdcSwitchFlg==0)&&(lcu1HrcOnReqFlg==0))||(lcu1HdcInhibitFlg==1)  
      #else
    if((lcu1HdcSwitchFlg==0)||(lcu1HdcInhibitFlg==1)
      #endif
      #if __TARGET_SPD_ADAPTATION
    ||(lcs16_hdc_tempW4==1)
      #else
    ||(lcu8HdcDriverAccelIntend==1)
      #endif
    ||(lcu1HdcDiscTempErrFlg==1)
    )
    {
        lcu8HdcExitChkResult=U8_HDC_EXIT;
    }
    else
    {
	  #if __STOP_ON_HILL    	
        if((lcu1HdcDownhillFlg==0)&&(lcu1HdcDownhillAfeterStopFlg==0)
        #if __TARGET_SPD_ADAPTATION
        &&(lcu1hdcBackRollHillFlg==0)
        #endif
        )
      #else
        if(lcu1HdcDownhillFlg==0)
      #endif
        {
            lcu8HdcExitChkResult=U8_HDC_READY;
        }
          #if __TARGET_SPD_ADAPTATION
        else if(lcs16_hdc_tempW5==1)
        {
            lcu8HdcExitChkResult=U8_HDC_READY;
        }          
          #else
        else if(lcu8HdcDriverBrakeIntend==1)
        {
            lcu8HdcExitChkResult=U8_HDC_READY;
        }
          #endif
        else
        {
            lcu8HdcExitChkResult=U8_HDC_ENTER;
        }
    }
}


void LCHDC_vEstSlope(void)
{

    LCHDC_vCalSlopeMeasures();

    LCHDC_vEstPreBump();

    LCHDC_vChkEngineDrag();

    LCHDC_vChkOverCreepSpeed();

    LCHDC_vCalVehicleAccel();

    LCHDC_vCalSlopeThreshold();

    lcs16HdcPreCurMax=(MFC_Current_HDC_P>=MFC_Current_HDC_S)?MFC_Current_HDC_P:MFC_Current_HDC_S;

      #if __TARGET_SPD_ADAPTATION
    if((ABS_fz==0)&&(EBD_RA==0))      
      #else
    if((FLAG_ESP_CONTROL_ACTION==0)&&(ABS_fz==0)&&(EBD_RA==0))
      #endif
    {

        LCHDC_vEstDownhill();
       #if __STOP_ON_HILL
        LCHDC_vChkDownhillAfterStop();
       #endif        

	  #if __STOP_ON_HILL    	       
        if((lcu1HdcActiveFlg==0)
            ||((lcu1HdcActiveFlg==1)&&(lcu1HdcDownhillFlg==0)))
      #else
        if((lcu1HdcActiveFlg==0)
            ||((lcu1HdcActiveFlg==1)&&(lcu1HdcDownhillFlg==0)))
      #endif      
        {

            lcu8HdcFlatPreCnt=0;

        }
        else
        {
            if((lcu8HdcBumpEstTime<lcu8HdcBumpDelayActTime+lcu8HdcBumpDectectTime)||(lcu1HdcSwitchFlg==0))
            {
                /*  Bump Check  */
            }
            else
            {
                LCHDC_vEstFlatPressure();
            }
        }
	  #if __STOP_ON_HILL    	       
        if(((lcu1HdcDownhillFlg==1)||(lcu1HdcDownhillAfeterStopFlg==1))&&(lcu1HdcSwitchFlg==1))
      #else
        if((lcu1HdcDownhillFlg==1)&&(lcu1HdcSwitchFlg==1))
      #endif
        {
            if(lcu8HdcBumpEstTime<(lcu8HdcBumpDelayActTime+lcu8HdcBumpDectectTime))
            {
                lcu8HdcBumpEstTime++;
                LCHDC_vEstBump( );
            }
            else { }

        }
        else
        {
            lcu8HdcBumpGCnt=0;
            if (lcu1HdcActiveFlg==0)
            {
                lcu8HdcBumpEstTime  =0;
            }
            else { }
        }
    }
    else { }

}

#if __TARGET_SPD_ADAPTATION
void LCHDC_vEstSlopeAfterRoll(void)
{
	if((lcu1HdcDownhillFlg==0)&&(lcu1HdcDownhillAfeterStopFlg==0)&&(vref<VREF_3_KPH))
	{
		if((lcu8HdcDriverBrakeIntend==0)&&(vref<vref_alt)&&(lcs16HdcDecel<0))
	    {
	    	lcs16HdcDecel_d = lcs16HdcDecel;
	    }
	    else
	    {
	    	if(lcu8HdcDriverBrakeIntend==1)
	    	{
	    		lcs16HdcDecel_d = 0;
	    	}
	    	else
	    	{
	    		;
	    	}
	    }
	    
	    if((lcu8HdcDriverAccelIntend==0)&&(vref>=vref_alt)&&(lcs16HdcDecel>0))
	    {
	    	lcs16HdcDecel_a = lcs16HdcDecel;
	    }
	    else
	    {
	    	if(lcu8HdcDriverAccelIntend==1)
	    	{
	    		lcs16HdcDecel_a = 0;
	    	}
	    	else
	    	{
	    	    ;
	    	}
	    }
	    
	    if((vref==VREF_2_KPH)&&(lcs16HdcDecel_a>0)&&(lcs16HdcDecel_d<0)
	    &&(lcu8hdcASenSTBCnt>L_U8_TIME_10MSLOOP_100MS)&&(abs(lcs16hdcLongOffsetAvrlf)<LONG_0G05G))
	    {
	    	  #if defined(__dDCT_INTERFACE)
	    	if((lcs16HdcDecel_a - lcs16HdcDecel_d)> LONG_0G1G)
	    	  #else
	    	if((lcs16HdcDecel_a - lcs16HdcDecel_d)> LONG_0G2G)
	    	  #endif
	    	{
	    		lcu1hdcBackRollHillFlg = 1;
	    	}
	    	else
	    	{
	    		;
	    	}
	    }
	    else
	    {
	    	;
	    }
	}
	else
	{
		lcs16HdcDecel_d = 0;
		lcs16HdcDecel_a = 0;
		if(lcu1hdcBackRollHillFlg==1)
		{
			/*if((vref>VREF_10_KPH)||(lcu1HdcDownhillFlg==1)||(lcu1HdcDownhillAfeterStopFlg==1))*/
			/*if((vref>VREF_10_KPH)||(lcu8HdcSmaThetaGCnt<L_U8_TIME_10MSLOOP_100MS)||(lcs16HdcActiveFlg_on_cnt>L_U16_TIME_10MSLOOP_3S))*/
			/*if(((lcu1HdcDownhillFlg==1)&&((lcu1hdcVehicleForward==1)||(lcu1hdcVehicleBackward==1)))||(lcs16HdcActiveFlg_on_cnt>L_U16_TIME_10MSLOOP_3S))*/
			if(((lcu1HdcDownhillFlg==1)&&((lcu1hdcVehicleForward==1)||(lcu1hdcVehicleBackward==1)))||((lcu1hdcGFlatFlag==1)&&(lcu8HdcFlatGCnt>=U8_HDC_FLAT_G_D_EST_TIME)))
			{
		        lcu1hdcBackRollHillFlg = 0;
		    }
		    else
		    {
		    	;
		    }
		}
		else
		{
			;
		}
	}
}
#endif

void LCHDC_vCalSlopeMeasures(void)
{
    uint8_t lcu8HdcUseAxSensor;

      #if (__4WD_VARIANT_CODE==ENABLE)
    if((lsu8DrvMode==DM_AUTO)||(lsu8DrvMode==DM_4H)||(lsu8DrvMode==DM_2H))
    {
        lcu8HdcUseAxSensor=1;
    }
    else
    {
          #if __AX_SENSOR
        lcu8HdcUseAxSensor=1;
          #else
        lcu8HdcUseAxSensor=0;
          #endif
    }
      #else
       #if __4WD||__AX_SENSOR
    lcu8HdcUseAxSensor=1;
       #else
    lcu8HdcUseAxSensor=0;
       #endif
      #endif


    lcs16HdcWheelAx1000g =  lsabss16HDCWheelAccel;

    lcs16HdcDecel=LCESP_s16Lpf1Int(lcs16HdcWheelAx1000g,lcs16HdcDecelOld,L_U8FILTER_GAIN_10MSLOOP_43HZ);        /*  43hz  */
    lcs16HdcDecelOld=lcs16HdcDecel;

    lcs16HdcDecellf=LCESP_s16Lpf1Int(lcs16HdcWheelAx1000g,lcs16HdcDecellfOld,L_U8FILTER_GAIN_10MSLOOP_1HZ);     /*  1hz */
    lcs16HdcDecellfOld=lcs16HdcDecellf;

  	if ((fu1AyErrorDet==1)||(fu1AySusDet==1)		/*  Ay error, sus    */
       ||(fu1YawErrorDet==1)||(fu1YawSusDet==1)		/*  yaw error, sus   */
       ||(fu1StrErrorDet==1)||(fu1StrSusDet==1)		/*  steer error, sus */
       )
  	{
  		lcs16HdcThetaGSteer=0;
  	}
  #if __SAS_OK_BACK_Change
    else if((SAS_CHECK_OK==0)
        ||(vref5<=VREF_5_KPH)
        ||((HDC_TM_TYPE==HDC_AUTO_TM)&&(BACK_DIR==1))
        ||((HDC_TM_TYPE==HDC_MANUAL_TM)&&(lcu1hdcVehicleBackward==1)))
  #else
	else if((vref5<=VREF_5_KPH)&&(BACK_DIR==1)&&(SAS_CHECK_OK==0)&&(vdc_error_flg==1))
  #endif
    {
        /*  HM : S16_HDC_THETAG_STEER   : 400  */
        /*  Kyron   : S16_HDC_THETAG_STEER   : 50   */
      #if __CAR == KMC_HM

        if (abs(yaw_out) > YAW_40DEG)
        {
			tempW3 = (int16_t)((((int32_t)(abs(yaw_out)-YAW_40DEG))*125)/20);

            lcs16HdcThetaGSteer= tempW3 + LONG_0G1G;
        }
        else
        {
            lcs16HdcThetaGSteer=(int16_t)(abs(yaw_out/(int16_t)S16_HDC_THETAG_STEER));
        }
      #else
        lcs16HdcThetaGSteer=(int16_t)(abs(yaw_out/(int16_t)S16_HDC_THETAG_STEER));
      #endif
    }
    else
    {
        /*----------------------------------------
        Vx_dot=Ax+Vy*YawRate/(3.6*9.81*57.3*100)
        ------------------------------------------*/

		tempW1 = yaw_out/10;
		lcs16HdcThetaGSteer = (int16_t)((((int32_t)Vy_MD)*tempW1)/2024);    /* 20236=3.6*9.81*57.3*10 /10(resol Ch)*/      
 
  	  #if __SAS_OK_BACK_Change
		if (lcs16HdcThetaGSteer < 0)
    	{
    		lcs16HdcThetaGSteer = 0;
    	}
    	else { }
      #endif
    }

    lcs16HdcThetaGPitch=(int16_t)(lcs16HdcDecel/S16_HDC_THETAG_PITCH);

    tempW5=lcs16HdcDecel-lcs16HdcThetaGSteer+lcs16HdcThetaGPitch-lcs16HdcASen;
    tempW6=lcs16HdcDecel-lcs16HdcThetaGSteer+lcs16HdcThetaGPitch-lcs16HdcASen1;
    tempW7=lcs16HdcDecel-lcs16HdcThetaGSteer+lcs16HdcThetaGPitch-lcs16HdcASen0;

    lcs16HdcDecel1=LCESP_s16Lpf1Int(ebd_filt_grv*10,lcs16HdcDecelOld1,L_U8FILTER_GAIN_10MSLOOP_6_5HZ);        /*  6.5hz */
    lcs16HdcDecelOld1=lcs16HdcDecel1;

/* Issue List 2007-189 : 감속 제어 中 평지 도달 시 HDC 제어 해제 늦음   */

    if(lcu8HdcUseAxSensor==1)
    {
        lcs16HdcThetaGOld    = lcs16HdcThetaG;
        lcs16HdcThetaGOld1   = lcs16HdcThetaG1;
        lcs16HdcThetaGOld0   = lcs16HdcThetaG0;
        lcs16HdcThetaGOld_N = lcs16HdcThetaG_N;
        lcs16_hdc_tempW0 = tempW5+abs(lcs16HdcASen);

        if((Rough_road_detect_vehicle==0)&&(Rough_road_suspect_vehicle==0))
        {
            lcs16HdcThetaG=LCESP_s16Lpf1Int(tempW5,lcs16HdcThetaGOld,L_U8FILTER_GAIN_10MSLOOP_17_5HZ);    /* 17.5hz */
            lcs16HdcThetaG1=LCESP_s16Lpf1Int(tempW6,lcs16HdcThetaGOld1,L_U8FILTER_GAIN_10MSLOOP_17_5HZ);
            lcs16HdcThetaG0=LCESP_s16Lpf1Int(tempW7,lcs16HdcThetaGOld0,L_U8FILTER_GAIN_10MSLOOP_17_5HZ);
            lcs16HdcThetaG_N=LCESP_s16Lpf1Int(lcs16_hdc_tempW0 ,lcs16HdcThetaGOld_N,L_U8FILTER_GAIN_10MSLOOP_17_5HZ);
        }
        else
        {
            lcs16HdcThetaG=LCESP_s16Lpf1Int(tempW5,lcs16HdcThetaGOld,L_U8FILTER_GAIN_10MSLOOP_6_5HZ);    /* 6.5hz */
            lcs16HdcThetaG1=LCESP_s16Lpf1Int(tempW6,lcs16HdcThetaGOld1,L_U8FILTER_GAIN_10MSLOOP_6_5HZ);
            lcs16HdcThetaG0=LCESP_s16Lpf1Int(tempW7,lcs16HdcThetaGOld0,L_U8FILTER_GAIN_10MSLOOP_6_5HZ);
            lcs16HdcThetaG_N=LCESP_s16Lpf1Int(lcs16_hdc_tempW0 ,lcs16HdcThetaGOld_N,L_U8FILTER_GAIN_10MSLOOP_6_5HZ);
        }

        lcs16HdcThetaG_max = (lcs16HdcThetaG1>lcs16HdcThetaG0)?lcs16HdcThetaG1:lcs16HdcThetaG0;

        lcs16HdcThetaGDot = lcs16HdcThetaG - lcs16HdcThetaGOld;

    }
    else
    {
        lcs16HdcThetaG=0;
        lcs16HdcThetaG1=0;
        lcs16HdcThetaG0=0;
        lcs16HdcThetaG_N=0;
    }

    /* Issue List 2007-351 : 급경사 노면에서 속도 추종 불가   */
    if((lcu8HdcUseAxSensor==1)
        &&((lcu1HdcDownhillFlg==1)||(lcu1HdcQuickDownhillFlg==1)))
    {
        if (lcu8HdcSum_ThetaG_cnt < L_U8_TIME_10MSLOOP_200MS)  /* Average 200msec  */
        {
            lcu8HdcSum_ThetaG_cnt++;

            if (lcs16HdcThetaG > LONG_1G )
            {
                lcs16_hdc_tempW9 = LONG_1G;
            }
            else if (lcs16HdcThetaG < -LONG_1G )
            {
                lcs16_hdc_tempW9 = -LONG_1G;
            }
            else
            {
                lcs16_hdc_tempW9 = lcs16HdcThetaG;
            }

            lcs16HdcSum_ThetaG = lcs16HdcSum_ThetaG + lcs16_hdc_tempW9;
            /* Max = 1g[1000] * 200ms[28] = 28000 */
        }
        else
        {
            lcu8HdcSum_ThetaG_cnt=0;
            if (lcu8HdcSum1S_ThetaG_cnt<5)
            {
                lcu8HdcSum1S_ThetaG_cnt++;
            }
            else { }

            lcs16Hdcavr200_ThetaG_Old5 = lcs16Hdcavr200_ThetaG_Old4;
            lcs16Hdcavr200_ThetaG_Old4 = lcs16Hdcavr200_ThetaG_Old3;
            lcs16Hdcavr200_ThetaG_Old3 = lcs16Hdcavr200_ThetaG_Old2;
            lcs16Hdcavr200_ThetaG_Old2 = lcs16Hdcavr200_ThetaG;
            lcs16Hdcavr200_ThetaG = lcs16HdcSum_ThetaG/L_U8_TIME_10MSLOOP_200MS;
            lcs16HdcSum_ThetaG =0;

            lcs16_hdc_tempW9 = lcs16Hdcavr200_ThetaG_Old5+lcs16Hdcavr200_ThetaG_Old4+lcs16Hdcavr200_ThetaG_Old3+lcs16Hdcavr200_ThetaG_Old2+lcs16Hdcavr200_ThetaG;

            lcs16HdcAvr1s_ThetaG_old = lcs16HdcAvr1s_ThetaG;

            if (lcu8HdcSum1S_ThetaG_cnt>=5)
            {
                lcs16HdcAvr1s_ThetaG = lcs16_hdc_tempW9/5;
            }
            else if (lcu8HdcSum1S_ThetaG_cnt>=1)
            {
                lcs16HdcAvr1s_ThetaG = lcs16_hdc_tempW9/(int16_t)lcu8HdcSum1S_ThetaG_cnt;
            }
            else
            {
                lcs16HdcAvr1s_ThetaG = 0;
            }
        }
    }
    else    /* Clear Hill Dection   */
    {
        lcu8HdcSum_ThetaG_cnt=0;
        lcu8HdcSum1S_ThetaG_cnt=0;

        lcs16HdcSum_ThetaG=0;

        lcs16Hdcavr200_ThetaG_Old5 = 0;
        lcs16Hdcavr200_ThetaG_Old4 = 0;
        lcs16Hdcavr200_ThetaG_Old3 = 0;
        lcs16Hdcavr200_ThetaG_Old2 = 0;
        lcs16Hdcavr200_ThetaG      = 0;

        lcs16HdcAvr1s_ThetaG = 0;
        lcs16HdcAvr1s_ThetaG_old =0;

        lcs16HdcAvr_ThetaG=0;
    }

    /* 100msec에 lcs16HdcAvr_ThetaG가 lcs16HdcAvr1s_ThetaG에 수렴 단 작아지는 조건. */
    if (lcs16HdcAvr1s_ThetaG_old > lcs16HdcAvr1s_ThetaG )
    {
        lcs16_hdc_tempW9 = L_U8_TIME_10MSLOOP_100MS/(lcs16HdcAvr1s_ThetaG_old - lcs16HdcAvr1s_ThetaG);
        lcs16_hdc_tempW7 = (((lcs16HdcAvr1s_ThetaG_old - lcs16HdcAvr1s_ThetaG)+L_U8_TIME_10MSLOOP_50MS) / L_U8_TIME_10MSLOOP_100MS);
        lcs16_hdc_tempW7 = (lcs16_hdc_tempW7>LONG_0G001G)?lcs16_hdc_tempW7:LONG_0G001G;     /*Min*/

    }
    else
    {
        lcs16_hdc_tempW9 = 0;
    }

    if (lcu8HdcAvr_ThetaG_cnt < lcs16_hdc_tempW9)
    {
        lcu8HdcAvr_ThetaG_cnt++;
        lcs16_hdc_tempW8=0;
    }
    else
    {
        lcu8HdcAvr_ThetaG_cnt=0;
        lcs16_hdc_tempW8=1;
    }


    if ((lcs16HdcAvr_ThetaG > lcs16HdcAvr1s_ThetaG )&&(lcs16_hdc_tempW8==1))
    {
        if (lcs16HdcAvr_ThetaG >= (lcs16HdcAvr1s_ThetaG+lcs16_hdc_tempW7))
        {
            lcs16HdcAvr_ThetaG = lcs16HdcAvr_ThetaG -lcs16_hdc_tempW7;
        }
        else
        {
            lcs16HdcAvr_ThetaG = lcs16HdcAvr_ThetaG;
        }
    }
    else if (lcs16HdcAvr_ThetaG > lcs16HdcAvr1s_ThetaG )
    {
        lcs16HdcAvr_ThetaG = lcs16HdcAvr_ThetaG;
    }
    else
    {
        lcs16HdcAvr_ThetaG = lcs16HdcAvr1s_ThetaG;
    }

    /*~ Issue List 2007-351 : 급경사 노면에서 속도 추종 불가   */
}

void LCHDC_vChkEngineDrag(void)
{
  #if __TURBINE_RPM_ENABLE	
	if (HDC_TM_TYPE==HDC_AUTO_TM)
	{
        lcs16_hdc_tempW9 = ((int16_t)eng_rpm - (int16_t)turbine_rpm);
        
        if (((lcu8HdcGearPosition == U8_HDC_NEU_GEAR)&&(lcu1HdcRightGearFlg==1))
            ||(vref<VREF_2_KPH))
        {
            lcs16_hdc_tempW8 = 0;
            lcs16_hdc_tempW7 = L_U8FILTER_GAIN_10MSLOOP_12_5HZ;  /*  12.5Hz     */
        }
        else
        {
            lcs16_hdc_tempW8 = lcs16_hdc_tempW9;
            lcs16_hdc_tempW7 = L_U8FILTER_GAIN_10MSLOOP_0_3HZ;   /*  0.3Hz   */
        }
        
        /* Speed Diff Limit */
        if (lcu1HdcDrivelineLowmodeFlg==0)  /* 4H   */
        {
            lcs16_hdc_tempW8 = (lcs16_hdc_tempW8>1500)?1500:lcs16_hdc_tempW8;
            lcs16_hdc_tempW8 = (lcs16_hdc_tempW8>-1500)?lcs16_hdc_tempW8:-1500;
        }
        else                                /*  4L  */
        {
            lcs16_hdc_tempW8 = (lcs16_hdc_tempW8>500)?500:lcs16_hdc_tempW8;
            lcs16_hdc_tempW8 = (lcs16_hdc_tempW8>-500)?lcs16_hdc_tempW8:-500;
        }
        
        ls16HdcDiffTurbineRPMlf_old = ls16HdcDiffTurbineRPMlf;
        ls16HdcDiffTurbineRPMlf=LCESP_s16Lpf1Int(lcs16_hdc_tempW8,ls16HdcDiffTurbineRPMlf_old,(uint8_t)lcs16_hdc_tempW7);    /*  0.5Hz -  8Hz*/
        
        lcs16_hdc_tempW9 = -lcs16_hdc_tempW9;
        
        if (lcs16_hdc_tempW9 > 300)
        {
            if (lcu8HdcEngineDrg_cnt>L_U8_TIME_10MSLOOP_500MS)
            {
                lcu8HdcEngineDrg = 1;
            }
            else
            {
                lcu8HdcEngineDrg_cnt=lcu8HdcEngineDrg_cnt+2;
            }
        }
        else if (lcs16_hdc_tempW9 > 100)
        {
            if (lcu8HdcEngineDrg_cnt>L_U8_TIME_10MSLOOP_500MS)
            {
                lcu8HdcEngineDrg = 1;
            }
            else
            {
                lcu8HdcEngineDrg_cnt=lcu8HdcEngineDrg_cnt+1;
            }
        }
        else
        {
            if (lcu8HdcEngineDrg_cnt>0)
            {
                lcu8HdcEngineDrg_cnt=lcu8HdcEngineDrg_cnt-1;
            }
            else
            {
                lcu8HdcEngineDrg = 0;
            }
        }
        
        if((lcu8HdcGearPosition==U8_HDC_NEU_GEAR)||(lcu8HdcGearPosition==U8_HDC_PARK_GEAR))
        {
            lcu8HdcEngineDrg = 0;
            lcu8HdcEngineDrg_cnt = 0;
        }
        else { }
    }
    else /* HDC_TM_TYPE==HDC_MANUAL_TM */
    {
    	lcu8HdcEngineDrg = 0;
    }
  #else
    lcu8HdcEngineDrg = 0;
  #endif
}

void LCHDC_vChkOverCreepSpeed(void)
{
    /*
        Sub-Function Out Put
            1. lcu1HdcOverCreepSpeedFlg
    */
    if (HDC_TM_TYPE==HDC_AUTO_TM)
    {
      #if __TURBINE_RPM_ENABLE
    	lcs16_hdc_tempW9 = (int16_t)(turbine_rpm - (int16_t)eng_rpm);
    	
    	if (lcu1HdcDrivelineLowmodeFlg==0)
    	{
    	    lcs16_hdc_tempW8 = S16_HDC_AT_CREEP_SPD_REF;
    	}
    	else
    	{
    	    lcs16_hdc_tempW8 = S16_HDC_AT_CREEP_SPD_REF_LOW;
    	}
    	
    	if (lcu1HdcOverCreepSpeedFlg==0)
    	{
    	    if ((lcs16_hdc_tempW9 > 0)&&(vref > lcs16_hdc_tempW8 ))
    	    {
    	        if (lcu8HdcCreepSpeedCnt>L_U8_TIME_10MSLOOP_100MS)
    	        {
    	            lcu1HdcOverCreepSpeedFlg = 1;
                    lcu8HdcCreepSpeedCnt=0;    	            
    	        }
    	        else
    	        {
    	            lcu8HdcCreepSpeedCnt++;
    	        }
    	    }
    	    else
    	    {
    	        lcu8HdcCreepSpeedCnt=0;
    	    }
    	}
    	else
    	{
    	    if ((lcs16_hdc_tempW9 < -50)||( vref < lcs16_hdc_tempW8 ))
    	    {
    	        if (lcu8HdcCreepSpeedCnt>L_U8_TIME_10MSLOOP_100MS)
    	        {
    	            lcu1HdcOverCreepSpeedFlg = 0;
                    lcu8HdcCreepSpeedCnt=0;    	            
    	        }
    	        else
    	        {
    	            lcu8HdcCreepSpeedCnt++;
    	        }
    	    }
    	    else
    	    {
    	        lcu8HdcCreepSpeedCnt=0;
    	    }
    	}
      #else
        lcu1HdcOverCreepSpeedFlg = 0;
      #endif
	}
    else  /* MT */
    {
        if (lcu1HdcOverCreepSpeedFlg==0)
        {
            if (((int16_t)eng_rpm < lcs16HdcEngIdleRpm + 200)&&(lcu1hdcClutchSwitch==1)&&(vref>S16_HDC_MT_CREEP_SPD_REF)   /* 목표속도, idle rpm 비교., Engine Torque 기준치 이하.	*/
  		  #if __POWER_ENGAGE_AT_CLUTCH_ON    		
    			&&(lcu1hdcPowerEngageAtClutchSWon==0)
    	  #endif
			   )            
            {
                if (lcu8HdcCreepSpeedCnt>L_U8_TIME_10MSLOOP_100MS)
                {
                    lcu1HdcOverCreepSpeedFlg = 1;
                    lcu8HdcCreepSpeedCnt=0;
                }
                else
                {
                    lcu8HdcCreepSpeedCnt++;
                }
            }
            else
            {
                lcu8HdcCreepSpeedCnt=0;
            }
        }
        else
        {
            if (((int16_t)eng_rpm > lcs16HdcEngIdleRpm + 200) || ((lcu1hdcClutchSwitch==1)&&(vref<(S16_HDC_MT_CREEP_SPD_REF-VREF_1_KPH))) 	/* 목표속도, idle rpm 비교.	*/
  		  #if __POWER_ENGAGE_AT_CLUTCH_ON    		
    			||(lcu1hdcPowerEngageAtClutchSWon==1)     /* power engage condition add in clutch sw on */
    	  #endif
    	  		)    		 
            {
                if (lcu8HdcCreepSpeedCnt>L_U8_TIME_10MSLOOP_100MS)
                {
                    lcu1HdcOverCreepSpeedFlg = 0;
                    lcu8HdcCreepSpeedCnt=0;
                }
                else
                {
                    lcu8HdcCreepSpeedCnt++;
                }
            }
            else
            {
                lcu8HdcCreepSpeedCnt=0;
            }
        }
    }
}


void LCHDC_vCalVehicleAccel(void)
{
  #if __Dectect_Hill_by_Vehilce_Accel
    if(HDC_TM_TYPE==HDC_AUTO_TM)
    {
        if(lcu1HdcDrivelineLowmodeFlg==0)   /* 2H, 4H   Mode*/
        {
            if (lcu8HdcGearPosition == U8_HDC_NEU_GEAR)
            {
                lcs16_hdc_tempW9 = LCESP_s16IInter2Point(vref,
                                                        VREF_8_KPH,-LONG_0G015G,
                                                        VREF_25_KPH,LONG_0G025G);
            }
            else if (lcu8HdcGearPosition == U8_HDC_REV_GEAR)
            {
                lcs16_hdc_tempW9 = LCESP_s16IInter4Point(vref,
                                                        VREF_2_KPH,LONG_0G025G,
                                                        VREF_4_KPH,LONG_0G03G,
                                                        VREF_6_KPH,LONG_0G,
                                                        VREF_10_KPH,-LONG_0G06G);
            }
            else if(lcu8HdcGearPosition==U8_HDC_1ST_GEAR)
            {
                lcs16_hdc_tempW9 = LCESP_s16IInter4Point(vref,
                                                        VREF_2_KPH,LONG_0G05G,
                                                        VREF_4_KPH,LONG_0G03G,
                                                        VREF_6_KPH,LONG_0G,
                                                        VREF_10_KPH,-LONG_0G055G);

            }
            else if(lcu8HdcGearPosition==U8_HDC_2ND_GEAR)
            {
                lcs16_hdc_tempW9 = LCESP_s16IInter5Point(vref,
                                                        VREF_2_KPH,LONG_0G02G,
                                                        VREF_4_KPH,LONG_0G015G,
                                                        VREF_6_KPH,LONG_0G01G,
                                                        VREF_10_KPH,LONG_0G,
                                                        VREF_14_KPH,-LONG_0G035G);
            }
            else
            {
                lcs16_hdc_tempW9 = LCESP_s16IInter5Point(vref,
                                                        VREF_2_KPH,LONG_0G02G,
                                                        VREF_4_KPH,LONG_0G015G,
                                                        VREF_6_KPH,LONG_0G01G,
                                                        VREF_10_KPH,LONG_0G,
                                                        VREF_14_KPH,-LONG_0G035G);
            }
        }
        else                                /* 4L Mode  */
        {/*
            if (lcu8HdcGearPosition == U8_HDC_NEU_GEAR)
            {

            }
            else if (lcu8HdcGearPosition == U8_HDC_REV_GEAR)
            {

            }
            else if(lcu8HdcGearPosition==U8_HDC_1ST_GEAR)
            {

            }
            else if(lcu8HdcGearPosition==U8_HDC_2ND_GEAR)
            {

            }
            else if(lcu8HdcGearPosition==U8_HDC_3RD_GEAR)
            {

            }
            else
            {

            }*/
        }
    }
    else
    {

    }

    lcs16HdcVehicleAccel = lcs16HdcDecel-lcs16_hdc_tempW9;
  #else
    lcs16HdcVehicleAccel = 0;
  #endif
}



void LCHDC_vSmoothClosingCtrl(void)
{
    uint16_t lcu16HdcClosingNum;

  #if __TCMF_CONTROL

    /* Issue List 2007-107 : HDC 제어 중 HDC ON/Off 반복 시 압력 상승 늦음. */


    if (lcs16HdcClosingFlg_on_cnt> L_U16_TIME_10MSLOOP_5S)
    {
        lcu16HdcClosingNum = 1;
    }
    else if (lcs16HdcClosingFlg_on_cnt> L_U16_TIME_10MSLOOP_4S)
    {
        lcu16HdcClosingNum = 2;
    }
    else if (lcs16HdcClosingFlg_on_cnt> L_U16_TIME_10MSLOOP_3S)
    {
        lcu16HdcClosingNum = 4;
    }
    else if (lcs16HdcClosingFlg_on_cnt> L_U8_TIME_10MSLOOP_1000MS)
    {
        lcu16HdcClosingNum = 6;
    }
    else
    {
        lcu16HdcClosingNum = 8;
    }

  #if  __N_GEAR_HDC_Enable
    if((lcu8HdcDriverAccelIntend==1)&&(mtp>MTP_40_P))
    {
        lcu16HdcClosingNum=1;
    }
    else if(lcu8HdcDriverAccelIntend==1)
    {
        lcu16HdcClosingNum=2;
    }
    else { }
  #else
    if ((lcu8HdcGearPosition==U8_HDC_NEU_GEAR)&&(lcu1HdcRightGearFlg==1)&&(lcu8HdcDriverAccelIntend==1))
    {
        lcu16HdcClosingNum=2;
    }
    else if((lcu8HdcDriverAccelIntend==1)&&(mtp>MTP_40_P))
    {
        lcu16HdcClosingNum=1;
    }
    else if(lcu8HdcDriverAccelIntend==1)
    {
        lcu16HdcClosingNum=2;
    }
    else { }
  #endif

    /* Closing Mode에서 차량 감속시 전류 감소량 증가 시킴. */
    if(lcs16HdcDecel <  -AFZ_0G2 )
    {
        lcu16HdcClosingNum = 1;
    }
    else if(lcs16HdcDecel <  -AFZ_0G1 )
    {
        lcu16HdcClosingNum = lcu16HdcClosingNum>>2;
    }
    else if(lcs16HdcDecel <  -AFZ_0G0 )
    {
        lcu16HdcClosingNum = lcu16HdcClosingNum>>1;
    }
    else { }

    /* Closing Mode에서 Target Speed 이하시전류 감소량 증가 시킴.   */
    if(vref < lcs16HdcTargetSpeed - VREF_4_KPH)
    {
        lcu16HdcClosingNum = 1;
    }
    else if(vref < (lcs16HdcTargetSpeed - VREF_3_KPH))
    {
        lcu16HdcClosingNum = lcu16HdcClosingNum>>2;
    }
    else if(vref < (lcs16HdcTargetSpeed - VREF_1_KPH))
    {
        lcu16HdcClosingNum = lcu16HdcClosingNum>>1;
    }
    else { }


  #else
    lcu16HdcClosingNum=2;
  #endif

    if (lcu16HdcClosingNum < 1)
    {
        lcu16HdcClosingNum = 1;
    }
    else { }

    lcu16HdcClosingCnt2++;

    if( lcu16HdcClosingCnt2 >=lcu16HdcClosingNum )
    {
        lcu8HdcValveMode=U8_HDC_DUMP;
          #if __TCMF_CONTROL
        lcu8HdcMotorMode=0;
          #else
        lcu8HdcMotorMode=1;
          #endif
        lcu16HdcClosingCnt2=0;
    }
    else
    {
        lcu8HdcValveMode=U8_HDC_HOLD;
        lcu8HdcMotorMode=0;
    }
    
    if (lcu16HdcClosingNum ==1)
    {
        lcu8HdcMFCCtrlStep = (uint8_t)LCESP_s16IInter4Point(mtp,MTP_10_P,3,
                                                       MTP_15_P,4,
                                                       MTP_30_P,5,
                                                       MTP_40_P,6);
    }
    else
    {
        lcu8HdcMFCCtrlStep = 3;
    }
}

void LCHDC_vEstDownhill(void)
{

    if((abs(slip_m_fl)<WHEEL_SLIP_50)&&(abs(slip_m_fr)<WHEEL_SLIP_50)&&
        (abs(slip_m_rl)<WHEEL_SLIP_50)&&(abs(slip_m_rr)<WHEEL_SLIP_50))
    {
        lcu8HdcExcessSlip=0;
    }
    else
    {
        lcu8HdcExcessSlip=1;
    }

	  #if defined(__dDCT_INTERFACE)
    lcs16_hdc_tempW9 = VREF_0_375_KPH;
      #else
    lcs16_hdc_tempW8 = S16_HDC_ACT_SPD_MIN - VREF_3_KPH;
    lcs16_hdc_tempW9 = (lcs16_hdc_tempW8 > VREF_2_KPH)? lcs16_hdc_tempW8 : VREF_2_KPH;
	  #endif

    if(vref<(int16_t)lcs16_hdc_tempW9)   /* Clear Under 2KPH */
    {
        if(lcu8HdcSmaThetaGCnt>0)
        {
            lcu8HdcSmaThetaGCnt--;
        }
        else { }
        if(lcu8HdcMidThetaGCnt>0)
        {
            lcu8HdcMidThetaGCnt--;
        }
        else { }
        if(lcu8HdcBigThetaGCnt>0)
        {
            lcu8HdcBigThetaGCnt--;
        }
        else { }
        if(lcu8HdcSmaThetaGNCnt>0)
        {
            lcu8HdcSmaThetaGNCnt--;
        }
        else { }

        lcu1HdcDownhillFlg = 0;
        
        if(vref<VREF_1_KPH)
        {
        	if(lcu8HdcFlatGCnt>1)
        	{
        		lcu8HdcFlatGCnt = lcu8HdcFlatGCnt - 2;
        	}
        	else 
        	{
        		lcu8HdcFlatGCnt = 0;
        		lcu1hdcGFlatFlag = 0;
        	}
        }
        else { }        
    }
    else if((lcu8HdcExcessSlip==0)
		&&(cbit_process_step != 1)
        &&(lcu1hdcVehicleDirectionOKFlg==1))
    {
        lcu1hdcSDownHillFlag = 0;
        lcu1hdcMDownHillFlag = 0;
        lcu1hdcBDownHillFlag = 0;
        lcu1hdcADownHillFlag = 0;
        lcu1hdcGFlatFlag = 0;

        /**********************************************************************/
        if(lcs16HdcThetaG>=lcs16HdcSmaThetaGTh)
        {
            if(lcu8HdcSmaThetaGCnt<lcu8HdcSmaThetaGEstTime)
            {
                lcu8HdcSmaThetaGCnt+=2;
            }
            else
            {
                lcu1hdcSDownHillFlag=1;
            }
        }
        else
        {

            if(lcu8HdcSmaThetaGCnt>1)
            {
                lcu8HdcSmaThetaGCnt = lcu8HdcSmaThetaGCnt-2;
            }
            else if(lcu8HdcSmaThetaGCnt>0)
            {
                lcu8HdcSmaThetaGCnt = lcu8HdcSmaThetaGCnt-1;
            }
            else { }
        }
        /**********************************************************************/
        if(lcs16HdcThetaG>=lcs16HdcMidThetaGTh)
        {
            if(lcu8HdcMidThetaGCnt<lcu8HdcMidThetaGEstTime)
            {
                lcu8HdcMidThetaGCnt+=2;
            }
            else
            {
                lcu1hdcMDownHillFlag=1;
            }
        }
        else
        {
            if(lcu8HdcMidThetaGCnt>1)
            {
                lcu8HdcMidThetaGCnt = lcu8HdcMidThetaGCnt-2;
            }
            else if(lcu8HdcMidThetaGCnt>0)
            {
                lcu8HdcMidThetaGCnt = lcu8HdcMidThetaGCnt-1;
            }
            else { }
        }
        /**********************************************************************/
        if(lcs16HdcThetaG>=lcs16HdcBigThetaGTh)
        {
            if(lcu8HdcBigThetaGCnt<lcu8HdcBigThetaGEstTime)
            {
                lcu8HdcBigThetaGCnt+=2;
            }
            else
            {
                lcu1hdcBDownHillFlag=1;
            }
        }
        else
        {
            if(lcu8HdcBigThetaGCnt>1)
            {
                lcu8HdcBigThetaGCnt = lcu8HdcBigThetaGCnt-2;
            }
            else if(lcu8HdcBigThetaGCnt>0)
            {
                lcu8HdcBigThetaGCnt = lcu8HdcBigThetaGCnt-1;
            }
            else { }
        }
        /**********************************************************************/
      #if __Dectect_Hill_by_Vehilce_Accel
        if(lcs16HdcVehicleAccel>=lcs16HdcSmaThetaGTh)&&(vref> VREF_2_KPH)&&(lcu8HdcMtpOffCnt==0))
      #else
        if((lcs16HdcDecel>=(lcs16HdcSmaThetaGTh - LONG_0G01G))&&(vref> VREF_2_KPH)&&(lcu8HdcMtpOffCnt==0)
            &&
            (((lcu8HdcGearPosition==U8_HDC_NEU_GEAR)&&(lcu1HdcRightGearFlg==1))
            ||(lcu1HdcOverCreepSpeedFlg==1))
            )
      #endif
        {
            if(lcu8HdcSmaThetaGNCnt<lcu8HdcSmaThetaGEstTime)
            {
                lcu8HdcSmaThetaGNCnt+=2;
            }
            else
            {
                  lcu1hdcADownHillFlag=1;
            }
        }
        else
        {
            if(lcu8HdcSmaThetaGNCnt>1)
            {
                lcu8HdcSmaThetaGNCnt = lcu8HdcSmaThetaGNCnt-2;
            }
            else if(lcu8HdcSmaThetaGNCnt>0)
            {
                lcu8HdcSmaThetaGNCnt = lcu8HdcSmaThetaGNCnt-1;
            }
            else { }
        }
        /**********************************************************************/
        if (lcs16HdcThetaG< lcs16HdcExThetaGTh )
        {
            if (lcu1hdcVehicleBackward==1)
            {
                lcs16_hdc_tempW9=U8_HDC_FLAT_G_R_EST_TIME;
            }
            else if (lcu1hdcVehicleForward==1)
            {
                lcs16_hdc_tempW9=U8_HDC_FLAT_G_D_EST_TIME;
            }
            else
            {
                if (lcu1hdcVehicleForwardSus2Flg==1)
                {
                    lcs16_hdc_tempW9=U8_HDC_FLAT_G_D_EST_TIME;
                }
                else if (lcu1hdcVehicleBackwardSus2Flg==1)
                {
                    lcs16_hdc_tempW9=U8_HDC_FLAT_G_R_EST_TIME;
                }
                else
                {
                    lcs16_hdc_tempW9=U8_HDC_FLAT_G_D_EST_TIME;
                }
            }

            if(lcu8HdcFlatGCnt<lcs16_hdc_tempW9)
            {
                lcu8HdcFlatGCnt=lcu8HdcFlatGCnt+2;
            }
            else
            {
                lcu1hdcGFlatFlag=1;
            }
        }
        else
        {
            if(lcu8HdcFlatGCnt>1)
            {
                lcu8HdcFlatGCnt = lcu8HdcFlatGCnt-2;
            }
            else if(lcu8HdcFlatGCnt>0)
            {
                lcu8HdcFlatGCnt = lcu8HdcFlatGCnt-1;
            }
            else { }
        }
        /**********************************************************************/
        if (lcu1HdcDownhillFlg==0)
        {
            if ((lcu1hdcADownHillFlag==1)
                ||(lcu1hdcSDownHillFlag==1)
                ||(lcu1hdcMDownHillFlag==1)
                ||(lcu1hdcBDownHillFlag==1))
            {
                lcu1HdcDownhillFlg = 1;
            }
            else { }
        }
        else
        {
            if (lcu1hdcGFlatFlag==1)
            {
                lcu1HdcDownhillFlg = 0;
            }
            else { }
        }
        /**********************************************************************/
    }
    else
    {
        if(lcu8HdcSmaThetaGCnt>0)
        {
            lcu8HdcSmaThetaGCnt--;
        }
        else { }
        if(lcu8HdcMidThetaGCnt>0)
        {
            lcu8HdcMidThetaGCnt--;
        }
        else { }
        if(lcu8HdcBigThetaGCnt>0)
        {
            lcu8HdcBigThetaGCnt--;
        }
        else { }
        if(lcu8HdcSmaThetaGNCnt>0)
        {
            lcu8HdcSmaThetaGNCnt--;
        }
        else { }

          #if defined(__dDCT_INTERFACE)
        if((lcu1HdcSwitchFlg==0)&&(lcu1HrcOnReqFlg==0))
          #else
        if (lcu1HdcSwitchFlg == 0)
          #endif
        {
            lcu1HdcDownhillFlg = 0;
        }
        else { }
        /* lcu1hdcVehicleDirectionOKFlg ==0 조건에서 DownHill Clear 조건 추가.*/
        if((lcu8HdcExcessSlip==0)
            &&(cbit_process_step != 1)
            &&(lcu8HdcSmaThetaGCnt==0)
            &&(lcu8HdcSmaThetaGNCnt==0)
            &&(lcu1HdcDownhillFlg==1))
        {
            lcu1HdcDownhillFlg = 0;
        }
        else { }
    }

    if((lcu8HdcSmaThetaGCnt>=lcu8HdcSmaThetaGEstTime/2)
        ||(lcu8HdcMidThetaGCnt>=lcu8HdcMidThetaGEstTime/2)
        ||(lcu8HdcBigThetaGCnt>=lcu8HdcBigThetaGEstTime/2)
        ||(lcu8HdcSmaThetaGNCnt>=lcu8HdcSmaThetaGEstTime/2))
    {
        lcu1HdcQuickDownhillFlg=1;
    }
    else
    {
        lcu1HdcQuickDownhillFlg=0;
    }
}

#if __STOP_ON_HILL    
void LCHDC_vChkDownhillAfterStop(void)
{
    /*
        Sub-Function Out Put
            1. lcu1HdcDownhillAfeterStopFlg
    */	
    if (((lcu1hdcStopOnDownHillFlg==1)||(lcu1hdcStopOnUpHillFlg==1))&&(lcu8HdcDriverAccelIntend==0))     		
    {
   		if (lcu8HdcDecelOKtimer<L_U8_TIME_10MSLOOP_1000MS)
   		{
   			if (lcs16HdcDecel>=LONG_0G25G)
   			{
   				lcu8HdcDecelOKtimer = lcu8HdcDecelOKtimer + 5;
   			}
   			else if (lcs16HdcDecel>=LONG_0G1G)
   			{
   				lcu8HdcDecelOKtimer = lcu8HdcDecelOKtimer + 3;
   			}
   			else if (lcs16HdcDecel>=LONG_0G03G)
   			{
   				lcu8HdcDecelOKtimer = lcu8HdcDecelOKtimer + 1;
   			}
 			else if ((lcu8HdcDecelOKtimer>0)&&(lcs16HdcDecel<LONG_0G02G))
   			{
   				lcu8HdcDecelOKtimer = lcu8HdcDecelOKtimer - 1;
   			}
   			else { }
   		}
   		else
   		{
			lcu8HdcDecelOKtimer = L_U8_TIME_10MSLOOP_1000MS;
   		}
   	}
   	else 
   	{
   		lcu8HdcDecelOKtimer = 0;
   	}

    if ((lcu1hdcStopOnDownHillFlg==1)&&(lcu8HdcDriverAccelIntend==0))  
   	{
   		if (((lcu8HdcGearPosition==U8_HDC_1ST_GEAR)||(lcu8HdcGearPosition==U8_HDC_2ND_GEAR))/*인기어 가속도 작은 경우*/
   			&&(lcu1HdcRightGearFlg==1)&&(lcu8HdcDecelOKtimer>L_U8_TIME_10MSLOOP_200MS))
   		{ 
   			lcu1HdcDownhillAfeterStopFlg = 1;
   		}
   		else { }
   	  #if __POWER_ENGAGE_AT_CLUTCH_ON
	    if ((((lcu8HdcGearPosition==U8_HDC_NEU_GEAR)&&(lcu1HdcRightGearFlg==1)) /*뉴트럴 정보 있는 경우*/   
   			||((HDC_TM_TYPE==HDC_MANUAL_TM)&&(lcu1hdcClutchSwitch==1)&&(lcu1hdcPowerEngageAtClutchSWon==0)))
   			&&(lcu8HdcDecelOKtimer>L_U8_TIME_10MSLOOP_300MS))
	  #else
		if ((((lcu8HdcGearPosition==U8_HDC_NEU_GEAR)&&(lcu1HdcRightGearFlg==1)) 
			||((HDC_TM_TYPE==HDC_MANUAL_TM)&&(lcu1hdcClutchSwitch==1)))
			&&(lcu8HdcDecelOKtimer>L_U8_TIME_10MSLOOP_300MS))
	  #endif
   		{
   			lcu1HdcDownhillAfeterStopFlg = 1;
   		}
   		else { }
   	}
   	else { }
   
    if ((lcu1hdcStopOnUpHillFlg==1)&&(lcu8HdcDriverAccelIntend==0))    
    {
   	  #if __POWER_ENGAGE_AT_CLUTCH_ON    	
    	if ((((HDC_TM_TYPE==HDC_AUTO_TM)&&(lcu1HdcRightGearFlg==1)&&(lcu8HdcGearPosition==U8_HDC_REV_GEAR))
    		||((HDC_TM_TYPE==HDC_MANUAL_TM)&&(lcu1hdcGearR_Switch==1))							
    		||((lcu8HdcGearPosition==U8_HDC_NEU_GEAR)&&(lcu1HdcRightGearFlg==1))    			  
    		||((HDC_TM_TYPE==HDC_MANUAL_TM)&&(lcu1hdcClutchSwitch==1)&&(lcu1hdcPowerEngageAtClutchSWon==0)))      
    		&&(lcu8HdcDecelOKtimer>L_U8_TIME_10MSLOOP_200MS))
	  #else 
    	if ((((HDC_TM_TYPE==HDC_AUTO_TM)&&(lcu1HdcRightGearFlg==1)&&(lcu8HdcGearPosition==U8_HDC_REV_GEAR))
    		||((HDC_TM_TYPE==HDC_MANUAL_TM)&&(lcu1hdcGearR_Switch==1))							
    		||((lcu8HdcGearPosition==U8_HDC_NEU_GEAR)&&(lcu1HdcRightGearFlg==1))    			  
    		||((HDC_TM_TYPE==HDC_MANUAL_TM)&&(lcu1hdcClutchSwitch==1)))      
    		&&(lcu8HdcDecelOKtimer>L_U8_TIME_10MSLOOP_200MS))	  
	  #endif	     		
    	{
    		lcu1HdcDownhillAfeterStopFlg = 1;  /*uphill 후진조건만 내리막판단*/
			lcu1hdcUpHillBackwardSusFlg = 1;
    	}
    	else { }
    }
    else { }
    
	if (vref<VREF_1_KPH)
	{
	    lcu1HdcDownhillAfeterStopFlg = 0;
   		lcu1hdcUpHillBackwardSusFlg = 0;
	}
	else if (lcu1hdcGFlatFlag==1)
	{
	    lcu1HdcDownhillAfeterStopFlg = 0;
	}
	else { }
}
#endif        

void LCHDC_vCalSlopeThreshold(void)
{

    LCHDC_vCalSlopeThresholdFactor();  
    LCHDC_vCalSlopeThresholdFactorTh();


    /* Threshold Time Set   */
    if (lcu8HdcEngUpdateCnt==0)
    {
        if (lcu1hdcVehicleBackward==1)
        {
            lcu8HdcSmaThetaGEstTime=(uint8_t)U8_HDC_SMA_G_EST_TIME-(uint8_t)lcs16HdcSpeedFactor;
            lcu8HdcMidThetaGEstTime=(uint8_t)U8_HDC_MID_G_EST_TIME-(uint8_t)lcs16HdcSpeedFactor;
            lcu8HdcBigThetaGEstTime=(uint8_t)U8_HDC_BIG_G_EST_TIME-(uint8_t)lcs16HdcSpeedFactor;
        }
        else if (lcu1hdcVehicleForward==1)
        {
            lcu8HdcSmaThetaGEstTime=(uint8_t)U8_HDC_SMA_G_EST_TIME-(uint8_t)lcs16HdcSpeedFactor+L_U8_TIME_10MSLOOP_150MS;
            lcu8HdcMidThetaGEstTime=(uint8_t)U8_HDC_MID_G_EST_TIME-(uint8_t)lcs16HdcSpeedFactor+L_U8_TIME_10MSLOOP_150MS;
            lcu8HdcBigThetaGEstTime=(uint8_t)U8_HDC_BIG_G_EST_TIME-(uint8_t)lcs16HdcSpeedFactor+L_U8_TIME_10MSLOOP_150MS;
        }
        else
        {
            if (lcu1hdcVehicleBackwardSus2Flg==1)
            {
                lcu8HdcSmaThetaGEstTime=(uint8_t)U8_HDC_SMA_G_EST_TIME-(uint8_t)lcs16HdcSpeedFactor;
                lcu8HdcMidThetaGEstTime=(uint8_t)U8_HDC_MID_G_EST_TIME-(uint8_t)lcs16HdcSpeedFactor;
                lcu8HdcBigThetaGEstTime=(uint8_t)U8_HDC_BIG_G_EST_TIME-(uint8_t)lcs16HdcSpeedFactor;
            }
            else if (lcu1hdcVehicleForwardSus2Flg==1)
            {
                lcu8HdcSmaThetaGEstTime=(uint8_t)U8_HDC_SMA_G_EST_TIME-(uint8_t)lcs16HdcSpeedFactor+L_U8_TIME_10MSLOOP_150MS;
                lcu8HdcMidThetaGEstTime=(uint8_t)U8_HDC_MID_G_EST_TIME-(uint8_t)lcs16HdcSpeedFactor+L_U8_TIME_10MSLOOP_150MS;
                lcu8HdcBigThetaGEstTime=(uint8_t)U8_HDC_BIG_G_EST_TIME-(uint8_t)lcs16HdcSpeedFactor+L_U8_TIME_10MSLOOP_150MS;
            }
            else
            {
                lcu8HdcSmaThetaGEstTime=(uint8_t)U8_HDC_SMA_G_EST_TIME-(uint8_t)lcs16HdcSpeedFactor+L_U8_TIME_10MSLOOP_100MS;
                lcu8HdcMidThetaGEstTime=(uint8_t)U8_HDC_MID_G_EST_TIME-(uint8_t)lcs16HdcSpeedFactor+L_U8_TIME_10MSLOOP_100MS;
                lcu8HdcBigThetaGEstTime=(uint8_t)U8_HDC_BIG_G_EST_TIME-(uint8_t)lcs16HdcSpeedFactor+L_U8_TIME_10MSLOOP_100MS;
            }
        }
    
        lcu8HdcSmaThetaGEstTime=lcu8HdcSmaThetaGEstTime+lcs16HdcPitchFactor;
        lcu8HdcMidThetaGEstTime=lcu8HdcMidThetaGEstTime+lcs16HdcPitchFactor;
        lcu8HdcBigThetaGEstTime=lcu8HdcBigThetaGEstTime+lcs16HdcPitchFactor;
    
    
        lcu8HdcMidThetaGEstTime=(lcu8HdcBigThetaGEstTime>lcu8HdcMidThetaGEstTime)?lcu8HdcBigThetaGEstTime:lcu8HdcMidThetaGEstTime;
        lcu8HdcSmaThetaGEstTime=(lcu8HdcMidThetaGEstTime>lcu8HdcSmaThetaGEstTime)?lcu8HdcMidThetaGEstTime:lcu8HdcSmaThetaGEstTime;
    
        /*************************************************************/
          #if defined(__dDCT_INTERFACE)
    	if(lcu1HrcOnReqFlg==1) {lcs16_hdc_tempW1 = lcs16HrcReqHsaActMinSlope;}
    	else				   {lcs16_hdc_tempW1 = S16_HDC_SMA_G_TH;}
          #else
        lcs16_hdc_tempW1 = S16_HDC_SMA_G_TH;
          #endif
    
        if (lcu1hdcVehicleBackward==1)
        {
            lcs16_hdc_tempW9=(int16_t)lcs16_hdc_tempW1*(AX_Resol)-LONG_0G01G + lcs16HdcEngienFactorTh + lcs16HdcBrakeFactorTh;
            lcs16_hdc_tempW8=(int16_t)S16_HDC_MID_G_TH*(AX_Resol)-LONG_0G01G + lcs16HdcEngienFactorTh + lcs16HdcBrakeFactorTh;
            lcs16_hdc_tempW7=(int16_t)S16_HDC_BIG_G_TH*(AX_Resol)-LONG_0G01G + lcs16HdcEngienFactorTh + lcs16HdcBrakeFactorTh;
        }
        else if (lcu1hdcVehicleForward==1)
        {
            lcs16_hdc_tempW9=(int16_t)lcs16_hdc_tempW1*(AX_Resol) + lcs16HdcEngienFactorTh + lcs16HdcBrakeFactorTh;
            lcs16_hdc_tempW8=(int16_t)S16_HDC_MID_G_TH*(AX_Resol) + lcs16HdcEngienFactorTh + lcs16HdcBrakeFactorTh;
            lcs16_hdc_tempW7=(int16_t)S16_HDC_BIG_G_TH*(AX_Resol) + lcs16HdcEngienFactorTh + lcs16HdcBrakeFactorTh;
        }
        else
        {
            if (lcu1hdcVehicleForwardSus2Flg==1)
            {
                lcs16_hdc_tempW9=(int16_t)lcs16_hdc_tempW1*(AX_Resol) + lcs16HdcEngienFactorTh + lcs16HdcBrakeFactorTh;
                lcs16_hdc_tempW8=(int16_t)S16_HDC_MID_G_TH*(AX_Resol) + lcs16HdcEngienFactorTh + lcs16HdcBrakeFactorTh;
                lcs16_hdc_tempW7=(int16_t)S16_HDC_BIG_G_TH*(AX_Resol) + lcs16HdcEngienFactorTh + lcs16HdcBrakeFactorTh;
            }
            else if (lcu1hdcVehicleBackwardSus2Flg==1)
            {
                lcs16_hdc_tempW9=(int16_t)lcs16_hdc_tempW1*(AX_Resol)-LONG_0G01G + lcs16HdcEngienFactorTh + lcs16HdcBrakeFactorTh;
                lcs16_hdc_tempW8=(int16_t)S16_HDC_MID_G_TH*(AX_Resol)-LONG_0G01G + lcs16HdcEngienFactorTh + lcs16HdcBrakeFactorTh;
                lcs16_hdc_tempW7=(int16_t)S16_HDC_BIG_G_TH*(AX_Resol)-LONG_0G01G + lcs16HdcEngienFactorTh + lcs16HdcBrakeFactorTh;
            }
            else
            {
                lcs16_hdc_tempW9=(int16_t)lcs16_hdc_tempW1*(AX_Resol)-LONG_0G005G + lcs16HdcEngienFactorTh + lcs16HdcBrakeFactorTh;
                lcs16_hdc_tempW8=(int16_t)S16_HDC_MID_G_TH*(AX_Resol)-LONG_0G005G + lcs16HdcEngienFactorTh + lcs16HdcBrakeFactorTh;
                lcs16_hdc_tempW7=(int16_t)S16_HDC_BIG_G_TH*(AX_Resol)-LONG_0G005G + lcs16HdcEngienFactorTh + lcs16HdcBrakeFactorTh;
            }
        }
    
        lcs16_hdc_tempW8=(lcs16_hdc_tempW9>lcs16_hdc_tempW8)?lcs16_hdc_tempW9:lcs16_hdc_tempW8;
        lcs16_hdc_tempW7=(lcs16_hdc_tempW8>lcs16_hdc_tempW7)?lcs16_hdc_tempW8:lcs16_hdc_tempW7;
    
        lcs16HdcSmaThetaGTh=lcs16_hdc_tempW9+lcs16HdcYawFactorTh+lcs16HdcYawDotFactorTh+lcs16HdcSpeedFactorTh;
        lcs16HdcMidThetaGTh=lcs16_hdc_tempW8+lcs16HdcYawFactorTh+lcs16HdcYawDotFactorTh+lcs16HdcSpeedFactorTh;
        lcs16HdcBigThetaGTh=lcs16_hdc_tempW7+lcs16HdcYawFactorTh+lcs16HdcYawDotFactorTh+lcs16HdcSpeedFactorTh;
    
        lcs16HdcSmaThetaGTh=(lcs16HdcSmaThetaGTh > lcs16_hdc_tempW9 )?lcs16HdcSmaThetaGTh:lcs16_hdc_tempW9;
        lcs16HdcMidThetaGTh=(lcs16HdcMidThetaGTh > lcs16_hdc_tempW8 )?lcs16HdcMidThetaGTh:lcs16_hdc_tempW8;
        lcs16HdcBigThetaGTh=(lcs16HdcBigThetaGTh > lcs16_hdc_tempW7 )?lcs16HdcBigThetaGTh:lcs16_hdc_tempW7;
    
        lcs16HdcSmaThetaGTh=lcs16HdcSmaThetaGTh+lcs16HdcAxOfsNoneTh;
        lcs16HdcMidThetaGTh=lcs16HdcMidThetaGTh+lcs16HdcAxOfsNoneTh;
        lcs16HdcBigThetaGTh=lcs16HdcBigThetaGTh+lcs16HdcAxOfsNoneTh;
    
    #if __HDC_Reenter_Enable
        lcs16HdcSmaThetaGTh=lcs16HdcSmaThetaGTh-lcs16HdcReenterTh;
        lcs16HdcMidThetaGTh=lcs16HdcMidThetaGTh-lcs16HdcReenterTh;
        lcs16HdcBigThetaGTh=lcs16HdcBigThetaGTh-lcs16HdcReenterTh;
    #endif
    
        /*************************************************************/
        lcs16_hdc_tempW6 = (vref>VREF_15_KPH)?vref:VREF_15_KPH;
    	tempW3 = (int16_t)((((int32_t)(abs(yaw_out)))*lcs16_hdc_tempW6)/1619);
        lcs16HdcExcesslat = abs(tempW3);	
    
        /*  vref[/8] * [10/36] * [*10] / [562]; */
    
        if ((SAS_CHECK_OK==1)
            &&( o_delta_yaw_thres >= delta_yaw)
            &&( u_delta_yaw_thres <= delta_yaw))
        {
            ;/* lcs16_hdc_tempW6 = 0;   */
        }
        else if (abs(yaw_out) <= YAW_5DEG)
        {
            ;/* lcs16_hdc_tempW6 = 0;   */
        }
        else if ( lcs16HdcExcesslat < LAT_0G15G)
        {
            ;/* lcs16_hdc_tempW6 = 0;   */
        }
        else
        {
            lcs16_hdc_tempW6 = LCESP_s16IInter2Point(lcs16HdcExcesslat,LAT_0G15G,LONG_0G,
                                                                        LAT_0G4G,LONG_0G1G);
    
            lcs16HdcSmaThetaGTh = lcs16HdcSmaThetaGTh + lcs16_hdc_tempW6;
            lcs16HdcMidThetaGTh = lcs16HdcMidThetaGTh + lcs16_hdc_tempW6;
            lcs16HdcBigThetaGTh = lcs16HdcBigThetaGTh + lcs16_hdc_tempW6;
        }
    
        /* Exit Thrshold    */
        lcs16_hdc_tempW9 = ((lcs16HdcSmaThetaGTh/(10)) > LONG_0G008G)? (lcs16HdcSmaThetaGTh/10):LONG_0G008G;
    
        if ( lcu1HdcActiveFlg == 0 )
        {
            lcs16HdcExThetaGTh = lcs16HdcSmaThetaGTh-lcs16_hdc_tempW9;
        }
        else
        {
              #if defined(__dDCT_INTERFACE)
    		if(lcu1HrcOnReqFlg==1) 
    		{
    			lcs16_hdc_tempW2 = lcs16HrcReqHsaActMinSlope - 3;
    			
    			if(lcs16_hdc_tempW2>3) {lcs16_hdc_tempW2 = 3;}
    			else if(lcs16_hdc_tempW2<0) {lcs16_hdc_tempW2 = 0;}
    			else { }
    		}
    		else				   
    		{
    			lcs16_hdc_tempW2 = S16_HDC_FLAT_G_TH;
    		}
              #else
    		lcs16_hdc_tempW2 = S16_HDC_FLAT_G_TH;
              #endif
        	
            lcs16HdcExThetaGTh = lcs16_hdc_tempW2*(AX_Resol)+ lcs16HdcYawFactorTh + lcs16HdcYawDotFactorTh;
            lcs16HdcExThetaGTh = lcs16HdcExThetaGTh + lcs16HdcEngienFactorTh + lcs16HdcBrakeFactorTh;
    
            lcs16HdcExThetaGTh = (lcs16HdcExThetaGTh >(lcs16_hdc_tempW2*(AX_Resol)))?lcs16HdcExThetaGTh:(lcs16_hdc_tempW2*(AX_Resol));
        }
    
        /* Air Sus CAN Massage Fail */
        if ((lcu8HdcEcsCompEnable == 0)&&(lcu1hdcEcsEquipFlg==1))
        {
            lcs16_hdc_tempW9 = abs(S16_ECS_LOW_MODE_G_COMP);
            lcs16_hdc_tempW8 = abs(S16_ECS_HIGH_MODE_G_COMP);
            lcs16_hdc_tempW7 = (lcs16_hdc_tempW9>lcs16_hdc_tempW8)?lcs16_hdc_tempW9:lcs16_hdc_tempW8;
            lcs16_hdc_tempW7 = LONG_0G;
    
            lcs16HdcSmaThetaGTh = lcs16HdcSmaThetaGTh + lcs16_hdc_tempW7;
            lcs16HdcMidThetaGTh = lcs16HdcMidThetaGTh + lcs16_hdc_tempW7;
            lcs16HdcBigThetaGTh = lcs16HdcBigThetaGTh + lcs16_hdc_tempW7;
        }
        else { }
    }
    else
    {
    	  ;
    }
}

void LCHDC_vCalSlopeThresholdFactor(void)
{
    lcs16_hdc_tempW9 = MTP_15_P;
    lcs16_hdc_tempW8 = MPRESS_15BAR;

    lcs16_hdc_tempW7 = (S16_HDC_ACT_SPD_MAX > vref)? vref : S16_HDC_ACT_SPD_MAX;
    lcs16HdcSpeedFactor=lcs16_hdc_tempW7/VREF_1_KPH;

    if ((mtp > lcs16_hdc_tempW9)&&(lcu1HdcRightGearFlg==1)
        &&(lcu8HdcGearPosition != U8_HDC_NEU_GEAR)
        &&(lcu8HdcGearPosition != U8_HDC_PARK_GEAR))
    {
        if (lcu8HdcmtpCnt2 < L_U8_TIME_10MSLOOP_500MS)
        {
            lcs16_hdc_tempW0 = mtp/lcs16_hdc_tempW9;
            lcs16_hdc_tempW0 = (lcs16_hdc_tempW0>5)?5:lcs16_hdc_tempW0;
            lcu8HdcmtpCnt2  = lcu8HdcmtpCnt2 +lcs16_hdc_tempW0;
        }
        else { }
    }
    else
    {
        if (lcu8HdcmtpCnt2 > 0)
        {
            lcu8HdcmtpCnt2 --;
        }
        else { }
    }

    if (lcu1HdcmtpSetFlg==0)
    {
        if (lcu8HdcmtpCnt2 >= L_U8_TIME_10MSLOOP_500MS)
        {
            lcu1HdcmtpSetFlg =1;
        }
        else { }
    }
    else
    {
        if (lcu8HdcmtpCnt2 < L_U8_TIME_10MSLOOP_150MS)
        {
            lcu1HdcmtpSetFlg=0;
        }
        else { }
    }

    if (lcu1HdcmtpSetFlg ==1)
    {

        lcu8HdcmtpOffCnt2 = L_U8_TIME_10MSLOOP_1500MS;
    }
    else
    {
        if (lcu8HdcmtpOffCnt2 > 0)
        {
            lcu8HdcmtpOffCnt2 --;
        }
        else { }
    }

    if ((mpress > lcs16_hdc_tempW8)&&(vref > VREF_2_KPH))
    {
        if (lcu8HdcmpressCnt < L_U8_TIME_10MSLOOP_500MS)
        {
            lcs16_hdc_tempW0 = mpress/lcs16_hdc_tempW8;
            lcs16_hdc_tempW0 = (lcs16_hdc_tempW0>5)?5:lcs16_hdc_tempW0;
            lcu8HdcmpressCnt  = lcu8HdcmpressCnt +lcs16_hdc_tempW0;
        }
        else { }
    }
    else
    {
        if (lcu8HdcmpressCnt > 0)
        {
            lcu8HdcmpressCnt --;
        }
        else { }
    }

    if (lcu1HdcmpresSetFlg==0)
    {
        if (lcu8HdcmpressCnt >= L_U8_TIME_10MSLOOP_500MS)
        {
            lcu1HdcmpresSetFlg =1;
        }
        else { }
    }
    else
    {
        if (lcu8HdcmpressCnt < L_U8_TIME_10MSLOOP_150MS)
        {
            lcu1HdcmpresSetFlg=0;
        }
        else { }
    }

    if (lcu1HdcmpresSetFlg ==1)
    {
        lcu8HdcmpressOffCnt = L_U8_TIME_10MSLOOP_1500MS;
    }
    else
    {
        if (lcu8HdcmpressOffCnt > 0)
        {
            lcu8HdcmpressOffCnt --;
        }
        else { }
    }


        lcs16_hdc_tempW4 = LCESP_s16IInter4Point(vref ,VREF_10_KPH, L_U8_TIME_10MSLOOP_100MS,
                                                       VREF_15_KPH, L_U8_TIME_10MSLOOP_300MS,
                                                       VREF_25_KPH, L_U8_TIME_10MSLOOP_300MS,
                                                       VREF_35_KPH, L_U8_TIME_10MSLOOP_100MS);
        lcs16_hdc_tempW1 = (int16_t)lcu8HdcBumpSuspect_cnt;
        lcs16_hdc_tempW5 = LCESP_s16IInter2Point(lcs16_hdc_tempW1,    0,                           0,
                                                                      L_U8_TIME_10MSLOOP_200MS,    lcs16_hdc_tempW4);
    
        lcs16_hdc_tempW6 = LCESP_s16IInter2Point((int16_t)lcu8HdcBumpSuspectCnt,    0,                           0,
                                                                                    L_U8_TIME_10MSLOOP_200MS,    L_U8_TIME_10MSLOOP_150MS);
    
        lcs16_hdc_tempW7 = (lcs16_hdc_tempW5>lcs16_hdc_tempW6)?lcs16_hdc_tempW5:lcs16_hdc_tempW6;
    
    
        if (((lcu1HdcRightGearFlg==1)&&(lcu8HdcGearPosition==U8_HDC_REV_GEAR))
            ||((lcu1HdcRightGearFlg==1)&&(lcu8HdcGearPosition==U8_HDC_1ST_GEAR))
            ||((lcu1HdcRightGearFlg==1)&&(lcu8HdcGearPosition==U8_HDC_2ND_GEAR))
            ||(lcu1HdcDrivelineLowmodeFlg==1))   /*  4L  Mode    */
        {
            lcs16_hdc_tempW9 = LCESP_s16IInter2Point((int16_t)lcu8HdcmtpOffCnt2,    0,                           0,
                                                                                    L_U8_TIME_10MSLOOP_500MS,    L_U8_TIME_10MSLOOP_300MS);
        }
        else
        {
            lcs16_hdc_tempW9 = LCESP_s16IInter2Point((int16_t)lcu8HdcmtpOffCnt2,    0,                           0,
                                                                                    L_U8_TIME_10MSLOOP_500MS,    L_U8_TIME_10MSLOOP_200MS);
        }
    
        lcs16_hdc_tempW8 = LCESP_s16IInter2Point((int16_t)lcu8HdcmpressOffCnt,    0,                           0,
                                                                                  L_U8_TIME_10MSLOOP_500MS,    L_U8_TIME_10MSLOOP_200MS);
    
    
        lcs16_hdc_tempW9    = (lcs16_hdc_tempW9>lcs16_hdc_tempW8)?lcs16_hdc_tempW9:lcs16_hdc_tempW8;
        lcs16HdcPitchFactor = (lcs16_hdc_tempW9>lcs16_hdc_tempW7)?lcs16_hdc_tempW9:lcs16_hdc_tempW7;

}

void LCHDC_vCalSlopeThresholdFactorTh(void)
{
    if ((lcu1HdcRightGearFlg==1)
        &&(lcu8HdcGearPosition != U8_HDC_NEU_GEAR)
        &&(lcu8HdcGearPosition != U8_HDC_PARK_GEAR))
    {
        if(lcu1HdcDrivelineLowmodeFlg==0)   /*  4H  Mode    */
        {
            lcs16HdcSpeedFactorTh = LCESP_s16IInter3Point(vref, VREF_2_KPH,LONG_0G02G,
                                                                VREF_3_KPH,LONG_0G015G,
                                                                VREF_5_KPH,LONG_0G);
        }
        else                                /*  4L  Mode    */
        {
            lcs16HdcSpeedFactorTh = LCESP_s16IInter3Point(vref, VREF_2_KPH,LONG_0G03G,
                                                                VREF_3_KPH,LONG_0G015G,
                                                                VREF_5_KPH,LONG_0G);
        }
    }
    else
    {
        lcs16HdcSpeedFactorTh = LONG_0G;
    }

    if ((int16_t)lcu8HdcDetmpt < (mtp*3))
    {
        lcs16_hdc_tempW9 = L_U8_TIME_10MSLOOP_1000MS;
        lcs16_hdc_tempW8 = mtp*3;
        lcs16_hdc_tempW8 = (lcs16_hdc_tempW9>lcs16_hdc_tempW8)?lcs16_hdc_tempW8:lcs16_hdc_tempW9;

        lcu8HdcDetmpt = (uint8_t)lcs16_hdc_tempW8;
    }
    else
    {
        if (lcu8HdcDetmpt > 0)
        {
            lcu8HdcDetmpt --;
        }
        else { }
    }

    if ((lcu8HdcGearPosition == U8_HDC_NEU_GEAR)&&(lcu1HdcRightGearFlg==1))
    {
        lcs16HdcEngienFactorTh = LONG_0G;
        lcs16HdcEngienFactorTh2 = LONG_0G;
    }
    else
    {
    	  #if __TARGET_SPD_ADAPTATION
    	lcs16HdcEngienFactorTh = LONG_0G;
    	  #else
        lcs16HdcEngienFactorTh = LCESP_s16IInter3Point(mtp,(int16_t)U8_HDC_ACCEL_ENTER,LONG_0G,
                                                                MTP_15_P,LONG_0G05G,
                                                                MTP_25_P,LONG_0G1G);
          #endif

        if (lcu8HdcDetmpt > 0)
        {
            if(lcu1HdcDrivelineLowmodeFlg==0)   /*  4H   Mode*/
            {
                if ((lcu8HdcGearPosition == U8_HDC_REV_GEAR)||(lcu8HdcGearPosition == U8_HDC_1ST_GEAR))
                {
                    lcs16_hdc_tempW0 =  100;
                    lcs16_hdc_tempW1 = 1200;
                    lcs16_hdc_tempW2 = LONG_0G005G;
                    lcs16_hdc_tempW3 = LONG_0G15G;
                }
                else
                {
                    lcs16_hdc_tempW0 =  100;
                    lcs16_hdc_tempW1 = 1500;
                    lcs16_hdc_tempW2 = LONG_0G005G;
                    lcs16_hdc_tempW3 = LONG_0G15G;
                }
            }
            else
            {
                if ((lcu8HdcGearPosition == U8_HDC_REV_GEAR)||(lcu8HdcGearPosition == U8_HDC_1ST_GEAR))
                {
                    lcs16_hdc_tempW0 =  25;
                    lcs16_hdc_tempW1 = 200;
                    lcs16_hdc_tempW2 = LONG_0G005G;
                    lcs16_hdc_tempW3 = LONG_0G15G;
                }
                else
                {
                    lcs16_hdc_tempW0 =  25;
                    lcs16_hdc_tempW1 = 500;
                    lcs16_hdc_tempW2 = LONG_0G005G;
                    lcs16_hdc_tempW3 = LONG_0G15G;
                }
            }

            lcs16HdcEngienFactorTh2 = LCESP_s16IInter2Point(ls16HdcDiffTurbineRPMlf,lcs16_hdc_tempW0,lcs16_hdc_tempW2,
                                                                                    lcs16_hdc_tempW1,lcs16_hdc_tempW3);
                                                                                    
            if (HDC_TM_TYPE==HDC_MANUAL_TM){lcs16HdcEngienFactorTh2=0;}
            else { }
            
        }
        else
        {
            lcs16HdcEngienFactorTh2 = LONG_0G;
        }
    }

    lcs16HdcEngienFactorTh =(lcs16HdcEngienFactorTh2>lcs16HdcEngienFactorTh)?lcs16HdcEngienFactorTh2:lcs16HdcEngienFactorTh;

    lcs16HdcBrakeFactorTh = LCESP_s16IInter3Point(mpress,MPRESS_5BAR,LONG_0G,
                                                       MPRESS_15BAR,LONG_0G02G,
                                                       MPRESS_40BAR,LONG_0G06G);

    lcs16HdcYawFactorTh=(int16_t)(abs(yaw_out/(S16_HDC_YAW_FACTOR)));

#if __Yaw_Factor_th_Change

    if (SAS_CHECK_OK==1)
    {
        lcs16_hdc_tempW3 = LCESP_s16IInter2Point(abs(yaw_out),YAW_40DEG,LONG_0G,
                                                              YAW_70DEG,LONG_0G06G);
    }
    else
    {
        lcs16_hdc_tempW3 = LCESP_s16IInter2Point(abs(yaw_out),YAW_40DEG,LONG_0G,
                                                              YAW_70DEG,LONG_0G12G);
    }

    lcs16HdcYawFactorTh = lcs16HdcYawFactorTh + lcs16_hdc_tempW3;
#endif

    if(((yaw_out>=0)&&(yawm_dot>=0))||((yaw_out<0)&&(yawm_dot<0)))
    {
        lcs16HdcYawDotFactorTh=(int16_t)(abs(yawm_dot/(S16_HDC_YAW_DOT_FACTOR)));
    }
    else
    {
        lcs16HdcYawDotFactorTh=-((int16_t)(abs(yawm_dot/(S16_HDC_YAW_DOT_FACTOR))));
    }

    lcs16HdcYawDotFactorTh = (lcs16HdcYawDotFactorTh>LONG_0G)?lcs16HdcYawDotFactorTh:LONG_0G;

  #if __HDC_Reenter_Enable
    if (lcu1hdcVehicleBackward==1)
    {
        lcs16_hdc_tempW8 = LONG_0G005G;
    }
    else if (lcu1hdcVehicleForward==1)
    {
        lcs16_hdc_tempW8 = LONG_0G01G;
    }
    else
    {
        if (lcu1hdcVehicleBackwardSus2Flg==1)
        {
            lcs16_hdc_tempW8 = LONG_0G005G;
        }
        else if (lcu1hdcVehicleForwardSus2Flg==1)
        {
            lcs16_hdc_tempW8 = LONG_0G01G;
        }
        else
        {
            lcs16_hdc_tempW8 = LONG_0G008G;
        }
    }

    if (lcs16HdcActiveFlg_on_cnt > L_U8_TIME_10MSLOOP_1000MS)
    {
        lcu1HdcReenterFlg = 1;
    }
    else if (lcs16HdcActiveFlg_off_cnt >L_U16_TIME_10MSLOOP_3S)
    {
        lcu1HdcReenterFlg = 0;
    }
    else { }

    if ((lcs16HdcActiveFlg_off_cnt >1)&&(lcs16HdcActiveFlg_off_cnt <L_U16_TIME_10MSLOOP_3S)&&(lcu1HdcReenterFlg==1))
    {
        lcs16HdcReenterTh = LCESP_s16IInter2Point(lcs16HdcActiveFlg_off_cnt,    L_U8_TIME_10MSLOOP_2S,    lcs16_hdc_tempW8,
                                                                                L_U16_TIME_10MSLOOP_3S,   LONG_0G);
    }
    else
    {
        lcs16HdcReenterTh = LONG_0G;
    }
  #endif

    /* EOL Long G Offset None   */
    lcs16HdcAxOfsNoneTh=LONG_0G;
  #if __Ax_OFFSET_APPLY
    if (lsabsu1AxOfsNone==1)
    {
        lcs16HdcAxOfsNoneTh=LONG_0G;
    }
    else { }
  #endif

}

void LCHDC_vEstBump(void)
{
    lcs16_hdc_tempW8 = LCESP_s16IInter3Point(vref , VREF_10_KPH,LONG_0G,
                                                    VREF_20_KPH,LONG_0G015G,
                                                    VREF_25_KPH,LONG_0G);

    lcs16_hdc_tempW9 = S16_HDC_BUMP_G_TH*(AX_Resol) + lcs16_hdc_tempW8;

    if((lcs16HdcThetaG<=lcs16_hdc_tempW9)&&((lcu1hdcVehicleForward==1)||(lcu1hdcVehicleBackward==1)))
    {
        if(lcu8HdcBumpGCnt<L_U8_TIME_10MSLOOP_50MS)
        {
            lcu8HdcBumpGCnt++;
        }
        else
        {
            lcu1HdcDownhillFlg=0;
            lcu8HdcBumpEstTime=0;
            lcu8HdcBumpGCnt=0;
            lcu8HdcBumpSuspectCnt=L_U8_TIME_10MSLOOP_1500MS;
        }
    }
    else
    {
        if(lcu8HdcBumpGCnt>0)
        {
            lcu8HdcBumpGCnt--;
        }
        else
        {
            lcu8HdcBumpGCnt=0;
        }
    }

}


void LCHDC_vEstPreBump(void)
{
    /*
        Sub-Function Out Put
            1. lcu8HdcBumpDelayActTime
            2. lcu8HdcBumpDectectTime
            3. lcu8HdcBumpSuspect_cnt
    */

    lcu8HdcPreBumDefaultTime = (uint8_t)LCESP_s16IInter2Point(vref,    VREF_15_KPH,    L_U8_TIME_10MSLOOP_1500MS,
                                                                       VREF_25_KPH,    L_U8_TIME_10MSLOOP_1000MS);

    if (lcs16DecRoughCnt >= L_U8_TIME_10MSLOOP_100MS)
    {
        lcs16_hdc_tempW9 = -LONG_0G01G;
    }
    else
    {
        lcs16_hdc_tempW9 = LONG_0G01G;
    }

    if((lcs16HdcThetaG<=(int16_t)(S16_HDC_BUMP_G_TH*(AX_Resol)+lcs16_hdc_tempW9))&&(vref >= VREF_1_KPH))   /* Bump Suspect */
    {
        if (lcu8HdcBumpSuspect==0)
        {
            if(lcu8HdcPreBumpGCnt<L_U8_TIME_10MSLOOP_50MS)
            {
                lcu8HdcPreBumpGCnt++;
            }
            else
            {
                lcu8HdcBumpSuspect=1;
                lcu8HdcBumpSuspect_cnt=lcu8HdcPreBumDefaultTime;
                lcu8HdcPreBumpGCnt=0;
            }
        }
        else
        {
            lcu8HdcBumpSuspect=1;

            if (lcu8HdcBumpSuspect_cnt<lcu8HdcPreBumDefaultTime)
            {
                lcu8HdcBumpSuspect_cnt=lcu8HdcPreBumDefaultTime;
            }
            else if (lcu8HdcBumpSuspect_cnt<(L_U8_TIME_10MSLOOP_1500MS+L_U8_TIME_10MSLOOP_200MS))
            {
                if(lcs16HdcThetaG>(int16_t)(S16_HDC_BUMP_G_TH*(AX_Resol)-LONG_0G1G))
                {
                    lcu8HdcBumpSuspect_cnt=lcu8HdcBumpSuspect_cnt-1;
                }
                else if(lcs16HdcThetaG>(int16_t)(S16_HDC_BUMP_G_TH*(AX_Resol)-LONG_0G2G))
                {
                    lcu8HdcBumpSuspect_cnt=lcu8HdcBumpSuspect_cnt;
                }
                else
                {
                    lcu8HdcBumpSuspect_cnt=lcu8HdcBumpSuspect_cnt+1;
                }
            }
            else
            {
                lcu8HdcBumpSuspect_cnt=lcu8HdcBumpSuspect_cnt-1;
            }
        }
    }
    else
    {
        if(lcu8HdcPreBumpGCnt>0)
        {
            lcu8HdcPreBumpGCnt--;
        }
        else
        {
            lcu8HdcPreBumpGCnt=0;
        }

        if (lcu8HdcBumpSuspect_cnt>0)
        {
            lcu8HdcBumpSuspect=1;
            lcu8HdcBumpSuspect_cnt--;
        }
        else
        {
            lcu8HdcBumpSuspect=0;
            lcu8HdcBumpSuspect_cnt=0;
        }
    }

    if (U8_HDC_BUMP_DELAY_ACT_TIME < L_U8_TIME_10MSLOOP_150MS)
    {
        lcs16_hdc_tempW9 = L_U8_TIME_10MSLOOP_150MS;
    }
    else
    {
        lcs16_hdc_tempW9 = (int16_t)U8_HDC_BUMP_DELAY_ACT_TIME;
    }

    if (lcu1HdcActiveFlg==0)
    {

        if (lcu8HdcBumpSuspect==1)
        {
            lcs16_hdc_tempW9 = lcs16_hdc_tempW9;
        }
        else
        {
            lcs16_hdc_tempW9 = lcs16_hdc_tempW9/8;
        }

        lcu8HdcBumpDelayActTime = (uint8_t)LCESP_s16IInter2Point(vref,   S16_HDC_BUMP_ACT_LOW_SPEED,    0,
                                                                         S16_HDC_BUMP_ACT_HIGH_SPEED,   lcs16_hdc_tempW9);

        lcu8HdcBumpDectectTime = (uint8_t)LCESP_s16IInter2Point(vref,    VREF_20_KPH,    L_U8_TIME_10MSLOOP_350MS,
                                                                         VREF_35_KPH,    L_U8_TIME_10MSLOOP_200MS);

    }
    else { }

    if (lcu8HdcBumpSuspectCnt > 0)
    {
        lcu8HdcBumpSuspectCnt--;
    }
    else { }

}

void LCHDC_vEstFlatPressure(void)
{
    /*  HDC 전류에 의한 해제 시간 Parameter Loading */
    if (lcu1hdcVehicleBackward == 1)
    {
        lcs16_hdc_tempW5=(int16_t)U8_HDC_FLAT_PRE_R_EST_TIME;        /*  30  */
    }
    else
    {
        lcs16_hdc_tempW5=(int16_t)U8_HDC_FLAT_PRE_D_EST_TIME;        /*  30  */
    }

    if (lcs16_hdc_tempW5 < L_U8_TIME_10MSLOOP_300MS)    /*  Min 400 MS */
    {
        lcs16_hdc_tempW5 = L_U8_TIME_10MSLOOP_300MS;
    }
    else { }

    if (HDC_TM_TYPE==HDC_AUTO_TM)
    {
        lcs16_hdc_tempW4 = 0;
    }
    else
    {
        if (lcu1HdcRightGearFlg==1)
        {
            lcs16_hdc_tempW5 = lcs16_hdc_tempW5+L_U8_TIME_10MSLOOP_300MS;
            lcs16_hdc_tempW4 = 25;
        }
        else
        {
            lcs16_hdc_tempW5 = lcs16_hdc_tempW5+L_U8_TIME_10MSLOOP_300MS;
            lcs16_hdc_tempW4 = 0;
        }
    }
    
    
    /*  HDC 전류에 의한 해제 Threshold Parameter Loading    */
    if (lcu1HdcDrivelineLowmodeFlg==1)
    {
    	if(HDC_TM_TYPE==HDC_AUTO_TM)
    	{
    		if((lcu8HdcGearPosition==U8_HDC_NEU_GEAR)
        	    ||(lcu8HdcRightGearCnt==0))
        	{
        	    lcs16_hdc_tempW9=S16_HDC_AT_FLAT_PRE_CUR_LN;
        	}
        	else if(lcu8HdcGearPosition==U8_HDC_REV_GEAR)
        	{
        	    lcs16_hdc_tempW9=S16_HDC_AT_FLAT_PRE_CUR_LR;
        	} 
        	else if((lcu8HdcGearPosition>=U8_HDC_1ST_GEAR)&&(lcu8HdcGearPosition<=U8_HDC_6TH_GEAR))
        	{
        	    lcs16_hdc_tempW9=S16_HDC_AT_FLAT_PRE_CUR_LD;
        	}
        	else 
        	{ 
        	    lcs16_hdc_tempW9=S16_HDC_AT_FLAT_PRE_CUR_LD;        		
        	}        	
    	}
    	else	/*MT*/
    	{
        	if((lcu8HdcGearPosition==U8_HDC_NEU_GEAR)
        	    ||(lcu8HdcRightGearCnt==0))
        	{
        	    lcs16_hdc_tempW9=S16_HDC_MT_FLAT_PRE_CUR_LN;
        	}
        	else if(lcu8HdcGearPosition==U8_HDC_REV_GEAR)
        	{
        	    lcs16_hdc_tempW9=S16_HDC_MT_FLAT_PRE_CUR_LR;
        	}
        	else if(lcu8HdcGearPosition==U8_HDC_1ST_GEAR)
        	{
        	    lcs16_hdc_tempW9=S16_HDC_MT_FLAT_PRE_CUR_L1;
        	}
        	else if(lcu8HdcGearPosition==U8_HDC_2ND_GEAR)
        	{
        	    lcs16_hdc_tempW9=S16_HDC_MT_FLAT_PRE_CUR_L2;
        	}
        	else if(lcu8HdcGearPosition==U8_HDC_3RD_GEAR)
        	{
        	    lcs16_hdc_tempW9=S16_HDC_MT_FLAT_PRE_CUR_L3;
        	}
        	else if(lcu8HdcGearPosition==U8_HDC_4TH_GEAR)
        	{
        	    lcs16_hdc_tempW9=S16_HDC_MT_FLAT_PRE_CUR_L4;
        	}
        	else if(lcu8HdcGearPosition==U8_HDC_5TH_GEAR)
        	{
        	    lcs16_hdc_tempW9=S16_HDC_MT_FLAT_PRE_CUR_L5;
        	}
        	else
        	{
        	    lcs16_hdc_tempW9=S16_HDC_MT_FLAT_PRE_CUR_L6;
        	}
        }
    }
    else	/* !low mode */
    {
    	if(HDC_TM_TYPE==HDC_AUTO_TM)
    	{
    		if((lcu8HdcGearPosition==U8_HDC_NEU_GEAR)
        	    ||(lcu8HdcRightGearCnt==0))
        	{
        	    lcs16_hdc_tempW9=S16_HDC_AT_FLAT_PRE_CUR_N;
        	}
        	else if(lcu8HdcGearPosition==U8_HDC_REV_GEAR)
        	{
        	    lcs16_hdc_tempW9=S16_HDC_AT_FLAT_PRE_CUR_R;
        	} 
        	else if((lcu8HdcGearPosition>=U8_HDC_1ST_GEAR)&&(lcu8HdcGearPosition<=U8_HDC_6TH_GEAR))
        	{
        	    lcs16_hdc_tempW9=S16_HDC_AT_FLAT_PRE_CUR_D;
        	}
        	else 
        	{ 
        	    lcs16_hdc_tempW9=S16_HDC_AT_FLAT_PRE_CUR_D;        		
        	}			
    	}
    	else	/*MT*/
    	{
        	if((lcu8HdcGearPosition==U8_HDC_NEU_GEAR)
        	    ||(lcu8HdcRightGearCnt==0))
        	{
        	    lcs16_hdc_tempW9=S16_HDC_MT_FLAT_PRE_CUR_N;
        	}
        	else if(lcu8HdcGearPosition==U8_HDC_REV_GEAR)
        	{
        	    lcs16_hdc_tempW9=S16_HDC_MT_FLAT_PRE_CUR_R;
        	}
        	else if(lcu8HdcGearPosition==U8_HDC_1ST_GEAR)
        	{
        	    lcs16_hdc_tempW9=S16_HDC_MT_FLAT_PRE_CUR_1;
        	}
        	else if(lcu8HdcGearPosition==U8_HDC_2ND_GEAR)
        	{
        	    lcs16_hdc_tempW9=S16_HDC_MT_FLAT_PRE_CUR_2;
        	}
        	else if(lcu8HdcGearPosition==U8_HDC_3RD_GEAR)
        	{
        	    lcs16_hdc_tempW9=S16_HDC_MT_FLAT_PRE_CUR_3;
        	}
        	else if(lcu8HdcGearPosition==U8_HDC_4TH_GEAR)
        	{
        	    lcs16_hdc_tempW9=S16_HDC_MT_FLAT_PRE_CUR_4;
        	}
        	else if(lcu8HdcGearPosition==U8_HDC_5TH_GEAR)
        	{
        	    lcs16_hdc_tempW9=S16_HDC_MT_FLAT_PRE_CUR_5;
        	}
        	else
        	{
        	    lcs16_hdc_tempW9=S16_HDC_MT_FLAT_PRE_CUR_6;
        	}
        }
    }

    if (lcu8HdcEngineDrg ==1 )
    {
        lcs16_hdc_tempW9 = lcs16_hdc_tempW9 - 100;
    }
    else if (lcu1hdcVehicleDirectionSusFlg==1 )
    {
        lcs16_hdc_tempW9 = lcs16_hdc_tempW9 - 100;
    }
    else { }

    /* Decel Ctrl Mode에서 감압시 default : 20 */
    if ((lcu1DecCtrlReqFlg ==1)||(lcu1HdcTargetSpeedDownFlg==1))
    {
        lcu8HdcFlatPressModeCnt = L_U8_TIME_10MSLOOP_1000MS;
        lcs16_hdc_tempW4 = 20;
    }
    else
    {
        if (lcu8HdcFlatPressModeCnt > 0)
        {
            lcu8HdcFlatPressModeCnt --;
        }
        else
        {
            lcu8HdcFlatPressModeCnt = 0;
        }
    }
    lcs16_hdc_tempW1 = (int16_t)lcu8HdcFlatPressModeCnt;
    lcs16_hdc_tempW8 = LCESP_s16IInter2Point(lcs16_hdc_tempW1,    0,                           0,
                                                                  L_U8_TIME_10MSLOOP_500MS,    lcs16_hdc_tempW4);


    if (lcu1DecCtrlReqFlg ==1)
    {
        if (lcs16HdcDecellf > LONG_0G02G )
        {
            lcs16_hdc_tempW7 = 30;
        }
        else if (lcs16HdcDecellf > LONG_0G01G )
        {
            lcs16_hdc_tempW7 = 20;
        }
        else if (lcs16HdcDecellf > LONG_0G )
        {
            lcs16_hdc_tempW7 = 10;
        }
        else
        {
            lcs16_hdc_tempW7 = 0;
        }
    }
    else
    {
        lcs16_hdc_tempW7 = 0;
    }

    lcs16_hdc_tempW9 = lcs16_hdc_tempW9 - lcs16_hdc_tempW8 - lcs16_hdc_tempW7;

  #if __CAR == SYC_RAXTON
    lcs16_hdc_tempW9 = lcs16_hdc_tempW9 + 25;   /*  Base : 435 (RAXTON 차량특성)    */
  #endif

    lcs16hdcFlatPressTH = lcs16_hdc_tempW9;

/* Issue List 2007-189 : 감속 제어 中 평지 도달 시 HDC 제어 해제 늦음   */

    lcu1hdcFlatByPress=0;

    if(lcs8HdcSubState>=S8_HDC_SUB_TG)
    {
      #if __TCMF_CONTROL
          #if __TARGET_SPD_ADAPTATION
        if ((lcs16HdcPreCurMax<=lcs16hdcFlatPressTH)&&(lcu8HdcSmaThetaGCnt<1))
          #else
        if (lcs16HdcPreCurMax<=lcs16hdcFlatPressTH)       
          #endif
        {
            if(lcu8HdcFlatPreCnt<lcs16_hdc_tempW5)
            {
                lcu8HdcFlatPreCnt++;
            }
            else
            {
                lcu1hdcFlatByPress=1;
            }
        }
        else if (lcs16HdcPreCurMax<=(lcs16hdcFlatPressTH+20))    /* 5%  */
        {
            if(lcu8HdcFlatPreCnt>0)
            {
                lcu8HdcFlatPreCnt--;
            }
            else { }
        }
        else
        {
            lcu8HdcFlatPreCnt=0;
        }
      #else
        if((lcu8HdcValveMode==U8_HDC_DUMP)&&(clDecHdc.lcu8DecCtrlReq==0))
        {
            if(lcu8HdcFlatPreCnt<lcs16_hdc_tempW5)
            {
                lcu8HdcFlatPreCnt+=1;
            }
            else
            {
                lcu1hdcFlatByPress=1;
            }
        }
        else if(lcu8HdcValveMode==U8_HDC_APPLY)
        {
            lcu8HdcFlatPreCnt=0;
        }
        else
        {
            if(lcu1HdcDeadzoneFlg==1)
            {
                lcu8HdcFlatPreCnt=0;
            }
            else
            {
                lcu8HdcFlatPreCnt=lcu8HdcFlatPreCnt;
            }
        }
      #endif
    }
    else
    {
        lcu8HdcFlatPreCnt=0;
    }

   #if __STOP_ON_HILL    
    if ((lcu1hdcFlatByPress==1)&&((lcu1HdcDownhillFlg==1)||(lcu1HdcDownhillAfeterStopFlg==1)))
   #else
    if ((lcu1hdcFlatByPress==1)&&(lcu1HdcDownhillFlg==1))
   #endif
    {
        lcu1HdcDownhillFlg=0;
        lcs16hdcFlatByPressOffCnt=0;
	   #if __STOP_ON_HILL            
        lcu1HdcDownhillAfeterStopFlg=0;
	   #endif        

        lcu8HdcSmaThetaGCnt=0;
        lcu8HdcMidThetaGCnt=0;
        lcu8HdcBigThetaGCnt=0;
        lcu8HdcSmaThetaGNCnt=0;
    }
    else
    {
        if (lcs16hdcFlatByPressOffCnt < L_U16_TIME_10MSLOOP_60S)
        {
            lcs16hdcFlatByPressOffCnt++;
        }
        else { }
    }
}

void LCHDC_vSpeedCtrl(void)
{
    LCHDC_vCalSpeedError();

    switch(lcs8HdcSubState)
    {
        /********************************************************/
        case S8_HDC_SUB_H2TG:
            if(lcs16HdcCtrlError>VREF_0_25_KPH)
            {
                lcs8HdcSubState=S8_HDC_SUB_L2TG;
                LCHDC_vSetTargetDecel();
                  #if (__TCMF_CONTROL) && (__FF_Ctrl_Enable == 0)
                LCHDC_vSetDecelCtrl(1,0,0,(lcs16HdcTargetG*10));
                  #else
                LCHDC_vSetDecelCtrl(1,1,0,(lcs16HdcTargetG*10));
                  #endif
            }
            else if(lcs16HdcCtrlError>-VREF_0_25_KPH)
            {
                if(abs(lcs16DecCtrlError)<=S16_HDC_ENOUGH_DECEL)
                {
                    lcs8HdcSubState=S8_HDC_SUB_TG;
                    LCHDC_vSetDecelCtrl(0,0,0,0);
                    LCHDC_vFeedBackCtrl();
                }
                else
                {
                    LCHDC_vSetTargetDecel();
                      #if (__TCMF_CONTROL) && (__FF_Ctrl_Enable == 0)
                    LCHDC_vSetDecelCtrl(1,0,0,(lcs16HdcTargetG*10));
                      #else
                    LCHDC_vSetDecelCtrl(1,1,0,(lcs16HdcTargetG*10));
                      #endif
                }
            }
            else
            {
                LCHDC_vSetTargetDecel();
                  #if (__TCMF_CONTROL) && (__FF_Ctrl_Enable == 0)
                LCHDC_vSetDecelCtrl(1,0,0,(lcs16HdcTargetG*10));
                  #else
                LCHDC_vSetDecelCtrl(1,1,0,(lcs16HdcTargetG*10));
                  #endif
            }
            break;

        /********************************************************/
        case S8_HDC_SUB_TG:
              #if __TARGET_SPD_ADAPTATION
            if((lcu8HdcDriverAccelIntend==1)||(lcu1hdcEngStallSuspect==1))
            {
                LCHDC_vSetDecelCtrl(0,0,0,0);
                LCHDC_vFeedBackCtrl();
            }
            else if((lcu8HdcDriverBrakeIntend==1)||(ABS_fz==1))
            {
                LCHDC_vSetDecelCtrl(0,0,0,0);
                LCHDC_vFeedBackCtrl();
            }
            else
              #endif        	
            if(lcs16HdcCtrlError<-VREF_3_KPH)
            {
                lcs8HdcSubState=S8_HDC_SUB_H2TG;
                LCHDC_vSetTargetDecel();
                  #if (__TCMF_CONTROL) && (__FF_Ctrl_Enable == 0)
                LCHDC_vSetDecelCtrl(1,0,0,(lcs16HdcTargetG*10));
                  #else
                LCHDC_vSetDecelCtrl(1,1,0,(lcs16HdcTargetG*10));
                  #endif
            }
            else if(lcs16HdcCtrlError<VREF_3_KPH)
            {
                LCHDC_vSetDecelCtrl(0,0,0,0);
                LCHDC_vFeedBackCtrl();
            }
            else
            {
                lcs8HdcSubState=S8_HDC_SUB_L2TG;
                LCHDC_vSetTargetDecel();
                  #if (__TCMF_CONTROL) && (__FF_Ctrl_Enable == 0)
                LCHDC_vSetDecelCtrl(1,0,0,(lcs16HdcTargetG*10));
                  #else
                LCHDC_vSetDecelCtrl(1,1,0,(lcs16HdcTargetG*10));
                  #endif
            }
            break;

        /********************************************************/
        case S8_HDC_SUB_L2TG:
              #if __TARGET_SPD_ADAPTATION
            if((lcu8HdcDriverAccelIntend==1)||(lcu1hdcEngStallSuspect==1))
            {
            	lcs8HdcSubState=S8_HDC_SUB_TG;
                LCHDC_vSetDecelCtrl(0,0,0,0);
                LCHDC_vFeedBackCtrl();
            }
            else if((lcu8HdcDriverBrakeIntend==1)||(ABS_fz==1))
            {
            	lcs8HdcSubState=S8_HDC_SUB_TG;
                LCHDC_vSetDecelCtrl(0,0,0,0);
                LCHDC_vFeedBackCtrl();
            }
            else
              #endif
            if(lcs16HdcCtrlError<-VREF_0_25_KPH)
            {
                lcs8HdcSubState=S8_HDC_SUB_H2TG;
                LCHDC_vSetTargetDecel();
                  #if (__TCMF_CONTROL) && (__FF_Ctrl_Enable == 0)
                LCHDC_vSetDecelCtrl(1,0,0,(lcs16HdcTargetG*10));
                  #else
                LCHDC_vSetDecelCtrl(1,1,0,(lcs16HdcTargetG*10));
                  #endif
            }
            else if(lcs16HdcCtrlError<VREF_0_25_KPH)
            {
                if(abs(lcs16DecCtrlError)<=S16_HDC_ENOUGH_DECEL)
                {
                    lcs8HdcSubState=S8_HDC_SUB_TG;
                    LCHDC_vSetDecelCtrl(0,0,0,0);
                    LCHDC_vFeedBackCtrl();
                }
                else
                {
                    LCHDC_vSetTargetDecel();
                      #if (__TCMF_CONTROL) && (__FF_Ctrl_Enable == 0)
                    LCHDC_vSetDecelCtrl(1,0,0,(lcs16HdcTargetG*10));
                      #else
                    LCHDC_vSetDecelCtrl(1,1,0,(lcs16HdcTargetG*10));
                      #endif
                }
            }
            else
            {
                LCHDC_vSetTargetDecel();
                  #if (__TCMF_CONTROL) && (__FF_Ctrl_Enable == 0)
                LCHDC_vSetDecelCtrl(1,0,0,(lcs16HdcTargetG*10));
                  #else
                LCHDC_vSetDecelCtrl(1,1,0,(lcs16HdcTargetG*10));
                  #endif
            }
            break;

        /********************************************************/
        default:
            if(lcs16HdcCtrlError<0)
            {
                lcs8HdcSubState=S8_HDC_SUB_H2TG;
                LCHDC_vSetTargetDecel();
                  #if (__TCMF_CONTROL) && (__FF_Ctrl_Enable == 0)
                LCHDC_vSetDecelCtrl(1,0,0,(lcs16HdcTargetG*10));
                  #else
                LCHDC_vSetDecelCtrl(1,1,0,(lcs16HdcTargetG*10));
                  #endif
            }
            else
            {
                lcs8HdcSubState=S8_HDC_SUB_L2TG;
                LCHDC_vSetTargetDecel();
                  #if (__TCMF_CONTROL) && (__FF_Ctrl_Enable == 0)
                LCHDC_vSetDecelCtrl(1,0,0,(lcs16HdcTargetG*10));
                  #else
                LCHDC_vSetDecelCtrl(1,1,0,(lcs16HdcTargetG*10));
                  #endif
            }
            break;
    }
}


void LCHDC_vCalSpeedError(void)
{
    lcs16HdcCtrlError=lcs16HdcTargetSpeed-vref;
    if(lcs16HdcCtrlError>VREF_30_KPH) {lcs16HdcCtrlError=VREF_30_KPH;}
    else if(lcs16HdcCtrlError<-VREF_30_KPH) {lcs16HdcCtrlError=-VREF_30_KPH;}
    else {lcs16HdcCtrlError=lcs16HdcCtrlError;}

    lcs16HdcCtrlErrorDot=lcs16HdcCtrlError-lcs16HdcCtrlErrorOld;
    if(lcs16HdcCtrlErrorDot>5) {lcs16HdcCtrlErrorDot=5;}
    else if(lcs16HdcCtrlErrorDot<-5) {lcs16HdcCtrlErrorDot=-5;}
    else {lcs16HdcCtrlErrorDot=lcs16HdcCtrlErrorDot;}

    lcs16HdcCtrlErrorOld=lcs16HdcCtrlError;
}


void LCHDC_vFeedBackCtrl(void)
{
    LCHDC_vFeedBackCtrlGain();
    LCHDC_vFeedBackCtrlInput();

    if(((lcs16HdcCtrlError<=S16_HDC_DEADZONE_U)&&(lcs16HdcCtrlError>=S16_HDC_DEADZONE_L)&&
        (abs(lcs16HdcCtrlErrorDot)<1))
         #if __TARGET_SPD_ADAPTATION
       ||(lcu8HdcDriverBrakeIntend==1)
         #endif
    )
    {
        lcu1HdcDeadzoneFlg=1;
        lcu8HdcValveMode=U8_HDC_HOLD;
        lcu8HdcMotorMode=0;
        lcu16HdcTimeToActCnt=1;
    }
    else
    {
        lcu1HdcDeadzoneFlg=0;
        tempUW0=lcu16HdcTimeToActCnt%lcu16HdcTimeToAct;
        if((tempUW0>0)&&(lcu16HdcTimeToAct>lcu16HdcTimeToActCnt))
        {
            lcu8HdcValveMode=U8_HDC_HOLD;
            lcu8HdcMotorMode=0;
            lcu16HdcTimeToActCnt++;
        }
        else
        {
            if(lcs16HdcCtrlInput>0)
            {
                lcu8HdcValveMode=U8_HDC_DUMP;
                  #if __TCMF_CONTROL
                lcu8HdcMotorMode=0;
                  #else
                lcu8HdcMotorMode=1;
                  #endif
            }
            else if(lcs16HdcCtrlInput<0)
            {
                lcu8HdcValveMode=U8_HDC_APPLY;
                lcu8HdcMotorMode=1;
            }
            else
            {
                lcu8HdcValveMode=U8_HDC_HOLD;
                lcu8HdcMotorMode=0;
            }
            lcu16HdcTimeToActCnt=1;
        }
    }
}


void LCHDC_vFeedBackCtrlGain(void)
{

    if ((lcu8HdcGearPosition==U8_HDC_NEU_GEAR)&&(lcu1HdcRightGearFlg==1))
    {
        lcs16_hdc_tempW9  = (int16_t)S16_HDC_PE_PED_PGAIN_N;
        lcs16_hdc_tempW8  = (int16_t)S16_HDC_PE_PED_DGAIN_N;
        lcs16_hdc_tempW7  = (int16_t)S16_HDC_PE_MED_PGAIN_N;
        lcs16_hdc_tempW6  = (int16_t)S16_HDC_PE_MED_DGAIN_N;
        lcs16_hdc_tempW5  = (int16_t)S16_HDC_ME_PED_PGAIN_N;
        lcs16_hdc_tempW4  = (int16_t)S16_HDC_ME_PED_DGAIN_N;
        lcs16_hdc_tempW3  = (int16_t)S16_HDC_ME_MED_PGAIN_N;
        lcs16_hdc_tempW2  = (int16_t)S16_HDC_ME_MED_DGAIN_N;
    }
    else if (lcu1HdcDrivelineLowmodeFlg==1)
    {
        lcs16_hdc_tempW9  = (int16_t)S16_HDC_PE_PED_PGAIN_4L;
        lcs16_hdc_tempW8  = (int16_t)S16_HDC_PE_PED_DGAIN_4L;
        lcs16_hdc_tempW7  = (int16_t)S16_HDC_PE_MED_PGAIN_4L;
        lcs16_hdc_tempW6  = (int16_t)S16_HDC_PE_MED_DGAIN_4L;
        lcs16_hdc_tempW5  = (int16_t)S16_HDC_ME_PED_PGAIN_4L;
        lcs16_hdc_tempW4  = (int16_t)S16_HDC_ME_PED_DGAIN_4L;
        lcs16_hdc_tempW3  = (int16_t)S16_HDC_ME_MED_PGAIN_4L;
        lcs16_hdc_tempW2  = (int16_t)S16_HDC_ME_MED_DGAIN_4L;
    }
    else
    {
        lcs16_hdc_tempW9  = (int16_t)S16_HDC_PE_PED_PGAIN;
        lcs16_hdc_tempW8  = (int16_t)S16_HDC_PE_PED_DGAIN;
        lcs16_hdc_tempW7  = (int16_t)S16_HDC_PE_MED_PGAIN;
        lcs16_hdc_tempW6  = (int16_t)S16_HDC_PE_MED_DGAIN;
        lcs16_hdc_tempW5  = (int16_t)S16_HDC_ME_PED_PGAIN;
        lcs16_hdc_tempW4  = (int16_t)S16_HDC_ME_PED_DGAIN;
        lcs16_hdc_tempW3  = (int16_t)S16_HDC_ME_MED_PGAIN;
        lcs16_hdc_tempW2  = (int16_t)S16_HDC_ME_MED_DGAIN;
    }

    if(lcs16HdcCtrlError>0)
    {
        if(lcs16HdcCtrlErrorDot>0)
        {
            lcs16HdcCtrlPGain=lcs16_hdc_tempW9;
            lcs16HdcCtrlDGain=lcs16_hdc_tempW8;
        }
        else if(lcs16HdcCtrlErrorDot<0)
        {
            lcs16HdcCtrlPGain=lcs16_hdc_tempW7;
            lcs16HdcCtrlDGain=lcs16_hdc_tempW6;
        }
        else
        {
            if ((lcs16HdcCtrlPGain==0)&&(lcs16HdcCtrlDGain==0))
            {
                lcs16HdcCtrlPGain=(lcs16_hdc_tempW9+lcs16_hdc_tempW7)/2;
                lcs16HdcCtrlDGain=(lcs16_hdc_tempW8+lcs16_hdc_tempW6)/2;
            }
            else
            {
                lcs16HdcCtrlPGain=lcs16HdcCtrlPGain;
                lcs16HdcCtrlDGain=lcs16HdcCtrlDGain;
            }
        }
    }
    else if(lcs16HdcCtrlError<0)
    {
        if(lcs16HdcCtrlErrorDot>0)
        {
            lcs16HdcCtrlPGain=lcs16_hdc_tempW5;
            lcs16HdcCtrlDGain=lcs16_hdc_tempW4;
        }
        else if(lcs16HdcCtrlErrorDot<0)
        {
            lcs16HdcCtrlPGain=lcs16_hdc_tempW3;
            lcs16HdcCtrlDGain=lcs16_hdc_tempW2;
        }
        else
        {
            if ((lcs16HdcCtrlPGain==0)&&(lcs16HdcCtrlDGain==0))
            {
                lcs16HdcCtrlPGain=(lcs16_hdc_tempW5+lcs16_hdc_tempW3)/2;
                lcs16HdcCtrlDGain=(lcs16_hdc_tempW4+lcs16_hdc_tempW2)/2;
            }
            else
            {
                lcs16HdcCtrlPGain=lcs16HdcCtrlPGain;
                lcs16HdcCtrlDGain=lcs16HdcCtrlDGain;
            }
        }
    }
    else
    {
        if ((lcs16HdcCtrlPGain==0)&&(lcs16HdcCtrlDGain==0))
        {
            lcs16HdcCtrlPGain=(lcs16_hdc_tempW5+lcs16_hdc_tempW3)/2;
            lcs16HdcCtrlDGain=(lcs16_hdc_tempW4+lcs16_hdc_tempW2)/2;
        }
        else
        {
            lcs16HdcCtrlPGain=lcs16HdcCtrlPGain;
            lcs16HdcCtrlDGain=lcs16HdcCtrlDGain;
        }
    }

    if (lcs16DecRoughCnt >= L_U8_TIME_10MSLOOP_100MS)
    {
        lcs16HdcCtrlRoughPGain = 11;
        lcs16HdcCtrlRoughDGain =  9;
    }
    else
    {
        lcs16HdcCtrlRoughPGain = 10;
        lcs16HdcCtrlRoughDGain = 10;
    }

}


void LCHDC_vFeedBackCtrlInput(void)
{
    lcs16_hdc_tempW1 = lcs16HdcCtrlError;
    lcs16_hdc_tempW2 = (lcs16HdcCtrlPGain*lcs16HdcCtrlRoughPGain)/10;
    lcs16_hdc_tempW3 = lcs16HdcCtrlDGain;
    lcs16_hdc_tempW4 = (lcs16HdcCtrlErrorDot*lcs16HdcCtrlRoughDGain)/10;	

  #if __TARGET_SPD_ADAPTATION
    if(lcu8HdcDriverBrakeIntend==0)
    {
        lcs16HdcCtrlInput=(int16_t)((lcs16_hdc_tempW1*lcs16_hdc_tempW2)+(lcs16_hdc_tempW3*lcs16_hdc_tempW4));	
    }
    else
    {
        lcs16HdcCtrlInput = 0;
    }
  #else
    lcs16HdcCtrlInput=(int16_t)((lcs16_hdc_tempW1*lcs16_hdc_tempW2)+(lcs16_hdc_tempW3*lcs16_hdc_tempW4));	
  #endif

    if (lcs16HdcCtrlInput==0)
    {
        lcu16HdcTimeToAct=(uint16_t)U16_HDC_CTRL_PERIOD_MAX;
    }
    else if((YAW_CDC_WORK==1)
        ||((ABS_fl==1)&&(ABS_fr==1)&&(ABS_rl==1)&&(ABS_rr==1)))
    {
        tempUW2=1;
        tempUW1=(uint16_t)U16_HDC_CTRL_PERIOD_MAX_A_ABS;
        tempUW0=(uint16_t)(abs(lcs16HdcCtrlInput));
        u16mulu16divu16();
        if(tempUW3<=(uint16_t)U16_HDC_CTRL_PERIOD_MIN_A_ABS)
        {
            lcu16HdcTimeToAct=(uint16_t)U16_HDC_CTRL_PERIOD_MIN_A_ABS;
        }
        else
        {
            lcu16HdcTimeToAct=tempUW3;
        }
    }
    else if ((ABS_fl==1)||(ABS_fr==1)||(ABS_rl==1)||(ABS_rr==1))
    {
        tempUW2=1;
        tempUW1=(uint16_t)U16_HDC_CTRL_PERIOD_MAX_P_ABS;
        tempUW0=(uint16_t)(abs(lcs16HdcCtrlInput));
        u16mulu16divu16();
        if(tempUW3<=(uint16_t)U16_HDC_CTRL_PERIOD_MIN_P_ABS)
        {
            lcu16HdcTimeToAct=(uint16_t)U16_HDC_CTRL_PERIOD_MIN_P_ABS;
        }
        else
        {
            lcu16HdcTimeToAct=tempUW3;
        }
    }
    else
    {
        tempUW2=1;
        tempUW1=U16_HDC_CTRL_PERIOD_MAX;
        tempUW0=(uint16_t)(abs(lcs16HdcCtrlInput));
        u16mulu16divu16();
        if(tempUW3<=(uint16_t)U16_HDC_CTRL_PERIOD_MIN)
        {
            lcu16HdcTimeToAct=U16_HDC_CTRL_PERIOD_MIN;
        }
        else
        {
            lcu16HdcTimeToAct=tempUW3;
        }
    }
}


void LCHDC_vSetTargetDecel(void)
{
    if(lcs16HdcCtrlError<-VREF_0_25_KPH)
    {
        lcs16HdcTargetG = LCESP_s16IInter5Point(-lcs16HdcCtrlError ,
                        (int16_t)VREF_0_KPH, (int16_t)-S16_Hdc_Ctrl_Over_Speed_Target_G_1st,
                        (int16_t)VREF_1_KPH, (int16_t)-S16_Hdc_Ctrl_Over_Speed_Target_G_2nd,
                        (int16_t)VREF_4_KPH, (int16_t)-S16_Hdc_Ctrl_Over_Speed_Target_G_3rd,
                        (int16_t)VREF_8_KPH, (int16_t)-S16_Hdc_Ctrl_Over_Speed_Target_G_4th,
                        (int16_t)VREF_16_KPH,(int16_t)-S16_Hdc_Ctrl_Over_Speed_Target_G_5th);
    
    }
    else if(lcs16HdcCtrlError>VREF_0_25_KPH)
    {
      #if defined(__dDCT_INTERFACE)
    	if(lcu1HrcOnReqFlg==1)
    	{
    		lcs16HdcTargetG = LCESP_s16IInter3Point(lcs16HdcCtrlError ,
                        (int16_t)VREF_0_KPH, (int16_t)S16_Hrc_Ctrl_Under_Speed_Target_G_1st,
                        (int16_t)VREF_1_KPH, (int16_t)S16_Hrc_Ctrl_Under_Speed_Target_G_2nd,
                        (int16_t)VREF_4_KPH, (int16_t)S16_Hrc_Ctrl_Under_Speed_Target_G_3rd);
		}
    	else
    	{
      #endif
        lcs16HdcTargetG = LCESP_s16IInter3Point(lcs16HdcCtrlError ,
                        (int16_t)VREF_0_KPH, (int16_t)S16_Hdc_Ctrl_Under_Speed_Target_G_1st,
                        (int16_t)VREF_1_KPH, (int16_t)S16_Hdc_Ctrl_Under_Speed_Target_G_2nd,
                        (int16_t)VREF_4_KPH, (int16_t)S16_Hdc_Ctrl_Under_Speed_Target_G_3rd);
      #if defined(__dDCT_INTERFACE)
		}
	  #endif
    }
    else
    {
        lcs16HdcTargetG=0;
    }
}

void LCHDC_vResetCtrlVariable(void)
{
    lcs8HdcSubState=0;
    lcu16HdcTimeToActCnt=0;
    lcs16HdcCtrlErrorOld=0;

    lcu16HdcClosingCnt=0;
    lcu16HdcClosingCnt2=0;

    lcu8HdcBumpGCnt=0;
    lcu8HdcFlatPreCnt=0;
    lcu8HdcBumpEstTime=0;
    lcu8hdcEngStallSusOntimer=0;

    lcs16HdcCtrlPGain =0;
    lcs16HdcCtrlDGain =0;
    lcs16HdcCtrlInput =0;

    lcu16HdcObserver=0;
    LCHDC_vSetDecelCtrl(0,0,0,0);
}


void LCHDC_vSetDecelCtrl(uint8_t lcu8HdcDecReq,uint8_t lcu8HdcDecFfReq,uint8_t lcu8HdcDecClosingReq,int16_t lcs16HdcDecTargetG)
{
    clDecHdc.lcu8DecCtrlReq=lcu8HdcDecReq;
    clDecHdc.lcu8DecFeedForwardReq=lcu8HdcDecFfReq;
    clDecHdc.lcu8DecSmoothClosingReq=lcu8HdcDecClosingReq;
    clDecHdc.lcs16DecReqTargetG=lcs16HdcDecTargetG;
}

void LCHDC_vSetCtrlModeCnt(void)
{
    /*if (lcs8HdcState == S8_HDC_STANDBY)
    {
        if (lcs16HdcStandbyFlg_on_cnt < L_U16_TIME_10MSLOOP_3MIN)
        {
            lcs16HdcStandbyFlg_on_cnt++;
        }
        else { }
        lcs16HdcStandbyFlg_off_cnt = 0;
    }
    else
    {
        if (lcs16HdcStandbyFlg_off_cnt < L_U16_TIME_10MSLOOP_3MIN)
        {
            lcs16HdcStandbyFlg_off_cnt++;
        }
        else { }
        lcs16HdcStandbyFlg_on_cnt = 0;
    }*/

    if (lcs8HdcState == S8_HDC_ACTIVATION)
    {
        if (lcs16HdcActiveFlg_on_cnt < L_U16_TIME_10MSLOOP_3MIN)
        {
            lcs16HdcActiveFlg_on_cnt++;
        }
        else { }
        lcs16HdcActiveFlg_off_cnt = 0;
    }
    else
    {
        if (lcs16HdcActiveFlg_off_cnt < L_U16_TIME_10MSLOOP_3MIN)
        {
            lcs16HdcActiveFlg_off_cnt++;
        }
        else { }
        lcs16HdcActiveFlg_on_cnt = 0;
    }

    if (lcs8HdcState == S8_HDC_CLOSING)
    {
        if (lcs16HdcClosingFlg_on_cnt < L_U16_TIME_10MSLOOP_3MIN)
        {
            lcs16HdcClosingFlg_on_cnt++;
        }
        else { }
        /*lcs16HdcClosingFlg_off_cnt = 0;*/
    }
    else
    {
        /*if (lcs16HdcClosingFlg_off_cnt < L_U16_TIME_10MSLOOP_3MIN)
        {
            lcs16HdcClosingFlg_off_cnt++;
        }
        else { }*/
        lcs16HdcClosingFlg_on_cnt = 0;
    }
    /*
    if (lcs8HdcState == S8_HDC_TERMINATION)
    {
        if (lcs16HdcTerminationFlg_on_cnt < L_U16_TIME_10MSLOOP_3MIN)
        {
            lcs16HdcTerminationFlg_on_cnt++;
        }
        else { }
        lcs16HdcTerminationFlg_off_cnt = 0;
    }
    else
    {
        if (lcs16HdcTerminationFlg_off_cnt < L_U16_TIME_10MSLOOP_3MIN)
        {
            lcs16HdcTerminationFlg_off_cnt++;
        }
        else { }
        lcs16HdcTerminationFlg_on_cnt = 0;
    }
    
    if (lcs16HdcStandbyFlg_on_cnt == 1)
    {
        lcu8HdcStandbyFlg_K = 1;
        lcu8HdcStandbyFlg_A = 0;
    }
    else if (lcs16HdcStandbyFlg_off_cnt == 1)
    {
        lcu8HdcStandbyFlg_K = 0;
        lcu8HdcStandbyFlg_A = 1;
    }
    else
    {
        lcu8HdcStandbyFlg_K = 0;
        lcu8HdcStandbyFlg_A = 0;
    }
    */
    if (lcs16HdcActiveFlg_on_cnt == 1)
    {
        lcu8HdcActiveFlg_K = 1;
        lcu8HdcActiveFlg_A = 0;
    }
    else if (lcs16HdcActiveFlg_off_cnt == 1)
    {
        lcu8HdcActiveFlg_K = 0;
        lcu8HdcActiveFlg_A = 1;
    }
    else
    {
        lcu8HdcActiveFlg_K = 0;
        lcu8HdcActiveFlg_A = 0;
    }
    /*
    if (lcs16HdcClosingFlg_on_cnt == 1)
    {
        lcu8HdcClosingFlg_K = 1;
        lcu8HdcClosingFlg_A = 0;
    }
    else if (lcs16HdcClosingFlg_off_cnt == 1)
    {
        lcu8HdcClosingFlg_K = 0;
        lcu8HdcClosingFlg_A = 1;
    }
    else
    {
        lcu8HdcClosingFlg_K = 0;
        lcu8HdcClosingFlg_A = 0;
    }
    
    if (lcs16HdcTerminationFlg_on_cnt == 1)
    {
        lcu8HdcTerminationFlg_K = 1;
        lcu8HdcTerminationFlg_A = 0;
    }
    else if (lcs16HdcTerminationFlg_off_cnt == 1)
    {
        lcu8HdcTerminationFlg_K = 0;
        lcu8HdcTerminationFlg_A = 1;
    }
    else
    {
        lcu8HdcTerminationFlg_K = 0;
        lcu8HdcTerminationFlg_A = 0;
    }
    
    if (lcu1HdcSwitchFlg == 1)
    {
        if (lcs16HdcSwitchFlg_on_cnt < L_U16_TIME_10MSLOOP_3MIN)
        {
            lcs16HdcSwitchFlg_on_cnt++;
        }
        else { }
        lcs16HdcSwitchFlg_off_cnt = 0;
    }
    else
    {
        if (lcs16HdcSwitchFlg_off_cnt < L_U16_TIME_10MSLOOP_3MIN)
        {
            lcs16HdcSwitchFlg_off_cnt++;
        }
        else { }
        lcs16HdcSwitchFlg_on_cnt = 0;
    }

    if (lcu1DecCtrlReqFlg == 1)
    {
        if (lcu8HdcDecCtrlReq_on_cnt < L_U8_TIME_10MSLOOP_1000MS)
        {
            lcu8HdcDecCtrlReq_on_cnt++;
        }
        else { }
        lcu8HdcDecCtrlReq_off_cnt = 0;
    }
    else
    {
        if (lcu8HdcDecCtrlReq_off_cnt < L_U8_TIME_10MSLOOP_1000MS)
        {
            lcu8HdcDecCtrlReq_off_cnt++;
        }
        else { }
        lcu8HdcDecCtrlReq_on_cnt = 0;
    }
    */
}

static void	LCHDC_vChkTransmissionType(void)
{
	if((lespu1EMS_AT_TCU==1)||(lespu1EMS_CVT_TCU==1)||(lespu1EMS_DCT_TCU==1)||(lespu1EMS_HEV_AT_TCU==1))
	{
		HDC_TM_TYPE=HDC_AUTO_TM;
	}
	else
	{
		HDC_TM_TYPE=HDC_MANUAL_TM;
	}

	/* EV car */
	#if((__HMC_CAN_VER==CAN_EV)||(__HMC_CAN_VER==CAN_FCEV))
      HDC_TM_TYPE=HDC_AUTO_TM;
	#endif
}

#endif

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCHDCCallControl
	#include "Mdyn_autosar.h"
#endif
