
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCESPCalInterpolation
	#include "Mdyn_autosar.h"
#endif
#include"LCESPCalInterpolation.h"

int16_t LCESP_s16IInter2Point( int16_t input_x, int16_t x_1, int16_t y_1, int16_t x_2, int16_t y_2 );
int16_t LCESP_s16IInter3Point( int16_t input_x, int16_t x_1, int16_t y_1, int16_t x_2, int16_t y_2, int16_t x_3, int16_t y_3 );
int16_t LCESP_s16IInter4Point( int16_t input_x, int16_t x_1, int16_t y_1, int16_t x_2, int16_t y_2, int16_t x_3, int16_t y_3, int16_t x_4, int16_t y_4 );
int16_t LCESP_s16IInter5Point( int16_t input_x, int16_t x_1, int16_t y_1, int16_t x_2, int16_t y_2, int16_t x_3, int16_t y_3, int16_t x_4, int16_t y_4, int16_t x_5, int16_t y_5 );
int16_t LCESP_s16IInter6Point( int16_t input_x, int16_t x_1, int16_t y_1, int16_t x_2, int16_t y_2, int16_t x_3, int16_t y_3, int16_t x_4, int16_t y_4, int16_t x_5, int16_t y_5, int16_t x_6, int16_t y_6 );
int16_t LCESP_s16IInter7Point( int16_t input_x, int16_t x_1, int16_t y_1, int16_t x_2, int16_t y_2, int16_t x_3, int16_t y_3, int16_t x_4, int16_t y_4, int16_t x_5, int16_t y_5, int16_t x_6, int16_t y_6, int16_t x_7, int16_t y_7 );

#if __ECU==ESP_ECU_1
int16_t interpolation_tempW0;
int16_t interpolation_tempW9;
#endif

int16_t LCESP_s16IInter2Point( int16_t input_x, int16_t x_1, int16_t y_1, int16_t x_2, int16_t y_2 )
{
	#if __ECU==ABS_ECU_1
	int16_t interpolation_tempW0;
	int16_t interpolation_tempW9;
	#endif
    
    if( input_x <= x_1 )
        interpolation_tempW9 = y_1;
    else if ( input_x < x_2 ) {
  /************************* CT 개선 전********** 2005.02.21  ****************************** 
    //  tempW9 =  y_1 + (int16_t)((((int32_t)(y_2-y_1))*(input_x-x_1))/(x_2-x_1));
    //    tempW2 = (y_2-y_1); tempW1 = (input_x-x_1); tempW0 = (x_2-x_1);
    //    if ( tempW0==0 ) tempW0=1;
    //    s16muls16divs16();
    //    tempW9 = y_1 + tempW3;
*****************************************************************************************/ 
 /************************* CT 개선 후********** 2005.02.21  ******************************/
        interpolation_tempW0=(x_2-x_1);                                                                  
        if ( interpolation_tempW0==0 ) interpolation_tempW0=1;
        else{}                                                                             
        interpolation_tempW9 =  y_1 + (int16_t)(((int32_t)(y_2-y_1)*(input_x-x_1))/interpolation_tempW0);                     
/****************************************************************************************/ 
    } else
        interpolation_tempW9 = y_2;
            
    return interpolation_tempW9;      
}     

int16_t LCESP_s16IInter3Point( int16_t input_x, int16_t x_1, int16_t y_1, int16_t x_2, int16_t y_2, int16_t x_3, int16_t y_3 )
{
	#if __ECU==ABS_ECU_1
	int16_t interpolation_tempW0;
	int16_t interpolation_tempW9;
	#endif
 
    if( input_x <= x_1 )
        interpolation_tempW9 = y_1;
    else if ( input_x < x_2 ) {
/************************* CT 개선 전********** 2005.02.21  *************************** 
    //  tempW9 =  y_1 + (int16_t)((((int32_t)(y_2-y_1))*(input_x-x_1))/(x_2-x_1));
        tempW2 = (y_2-y_1); tempW1 = (input_x-x_1); tempW0 = (x_2-x_1);
        if ( tempW0==0 ) tempW0=1;
        s16muls16divs16();
        tempW9 = y_1 + tempW3;         
**************************************************************************************/ 

/**************************** CT 개선 후 : 2005.02.21(C 코드로 환원) ****************/
        interpolation_tempW0=(x_2-x_1);
        if (interpolation_tempW0==0) interpolation_tempW0=1;
        else{}
        interpolation_tempW9 =  y_1 + (int16_t)(((int32_t)(y_2-y_1)*(input_x-x_1))/interpolation_tempW0);
/************************************************************************************/
        
    } else if ( input_x < x_3 ) {
/************************* CT 개선 전********** 2005.02.21  *************************    
    //  tempW9 =  y_2 + (int16_t)((((int32_t)(y_3-y_2))*(input_x-x_2))/(x_3-x_2));
        tempW2 = (y_3-y_2); tempW1 = (input_x-x_2); tempW0 = (x_3-x_2);
        if ( tempW0==0 ) tempW0=1;
        s16muls16divs16();
        tempW9 = y_2 + tempW3;         
************************************************************************************/        
        
/**************************** CT 개선 후 : 2005.02.21(C 코드로 환원) **************/
        interpolation_tempW0=(x_3-x_2);
        if (interpolation_tempW0==0) interpolation_tempW0=1;
        else{}
        interpolation_tempW9 =  y_2 + (int16_t)(((int32_t)(y_3-y_2)*(input_x-x_2))/interpolation_tempW0);
/**********************************************************************************/        
              
    } else
        interpolation_tempW9 = y_3;
            
    return interpolation_tempW9;      
}  

int16_t LCESP_s16IInter4Point( int16_t input_x, int16_t x_1, int16_t y_1, int16_t x_2, int16_t y_2, int16_t x_3, int16_t y_3, int16_t x_4, int16_t y_4 )
{
	#if __ECU==ABS_ECU_1
	int16_t interpolation_tempW0;
	int16_t interpolation_tempW9;
	#endif
    
    if( input_x <= x_1 )
        interpolation_tempW9 = y_1;
    else if ( input_x < x_2 ) {
/************************* CT 개선 전********** 2005.02.21  *************************** 
    //  tempW9 =  y_1 + (int16_t)((((int32_t)(y_2-y_1))*(input_x-x_1))/(x_2-x_1));
        tempW2 = (y_2-y_1); tempW1 = (input_x-x_1); tempW0 = (x_2-x_1);
        if ( tempW0==0 ) tempW0=1;
        s16muls16divs16();
        tempW9 = y_1 + tempW3;         
**************************************************************************************/ 

/**************************** CT 개선 후 : 2005.02.21(C 코드로 환원) ****************/
        interpolation_tempW0=(x_2-x_1);
        if (interpolation_tempW0==0) interpolation_tempW0=1;
        else{}
        interpolation_tempW9 =  y_1 + (int16_t)(((int32_t)(y_2-y_1)*(input_x-x_1))/interpolation_tempW0);
/************************************************************************************/
        
    } else if ( input_x < x_3 ) {
/************************* CT 개선 전********** 2005.02.21  *************************    
    //  tempW9 =  y_2 + (int16_t)((((int32_t)(y_3-y_2))*(input_x-x_2))/(x_3-x_2));
        tempW2 = (y_3-y_2); tempW1 = (input_x-x_2); tempW0 = (x_3-x_2);
        if ( tempW0==0 ) tempW0=1;
        s16muls16divs16();
        tempW9 = y_2 + tempW3;         
************************************************************************************/        
        
/**************************** CT 개선 후 : 2005.02.21(C 코드로 환원) **************/
        interpolation_tempW0=(x_3-x_2);
        if (interpolation_tempW0==0) interpolation_tempW0=1;
        else{}
        interpolation_tempW9 =  y_2 + (int16_t)(((int32_t)(y_3-y_2)*(input_x-x_2))/interpolation_tempW0);
/**********************************************************************************/        
    } else if ( input_x < x_4 ) {
/************************* CT 개선 전********** 2005.02.21  *************************    
    //  tempW9 =  y_2 + (int16_t)((((int32_t)(y_3-y_2))*(input_x-x_2))/(x_3-x_2));
        tempW2 = (y_4-y_3); tempW1 = (input_x-x_3); tempW0 = (x_4-x_3);
        if ( tempW0==0 ) tempW0=1;
        s16muls16divs16();
        tempW9 = y_3 + tempW3;
************************************************************************************/        
        
/**************************** CT 개선 후 : 2005.02.21(C 코드로 환원) **************/
        interpolation_tempW0=(x_4-x_3);
        if (interpolation_tempW0==0) interpolation_tempW0=1;
        else{}
        interpolation_tempW9 =  y_3 + (int16_t)(((int32_t)(y_4-y_3)*(input_x-x_3))/interpolation_tempW0);
/**********************************************************************************/        
    } else
        interpolation_tempW9 = y_4;
            
    return interpolation_tempW9;      
}  

int16_t LCESP_s16IInter5Point( int16_t input_x, int16_t x_1, int16_t y_1, int16_t x_2, int16_t y_2, int16_t x_3, int16_t y_3, int16_t x_4, int16_t y_4, int16_t x_5, int16_t y_5 )
{
	#if __ECU==ABS_ECU_1
	int16_t interpolation_tempW0;
	int16_t interpolation_tempW9;
	#endif

    if( input_x <= x_1 )
        interpolation_tempW9 = y_1;
    else if ( input_x < x_2 ) {
/************************* CT 개선 전********** 2005.02.21  *************************** 
    //  tempW9 =  y_1 + (int16_t)((((int32_t)(y_2-y_1))*(input_x-x_1))/(x_2-x_1));
        tempW2 = (y_2-y_1); tempW1 = (input_x-x_1); tempW0 = (x_2-x_1);
        if ( tempW0==0 ) tempW0=1;
        s16muls16divs16();
        tempW9 = y_1 + tempW3;         
**************************************************************************************/ 

/**************************** CT 개선 후 : 2005.02.21(C 코드로 환원) ****************/
        interpolation_tempW0=(x_2-x_1);
        if (interpolation_tempW0==0) interpolation_tempW0=1;
        else{}
        interpolation_tempW9 =  y_1 + (int16_t)(((int32_t)(y_2-y_1)*(input_x-x_1))/interpolation_tempW0);
/************************************************************************************/
        
    } else if ( input_x < x_3 ) {
/************************* CT 개선 전********** 2005.02.21  *************************    
    //  tempW9 =  y_2 + (int16_t)((((int32_t)(y_3-y_2))*(input_x-x_2))/(x_3-x_2));
        tempW2 = (y_3-y_2); tempW1 = (input_x-x_2); tempW0 = (x_3-x_2);
        if ( tempW0==0 ) tempW0=1;
        s16muls16divs16();
        tempW9 = y_2 + tempW3;         
************************************************************************************/        
        
/**************************** CT 개선 후 : 2005.02.21(C 코드로 환원) **************/
        interpolation_tempW0=(x_3-x_2);
        if (interpolation_tempW0==0) interpolation_tempW0=1;
        else{}
        interpolation_tempW9 =  y_2 + (int16_t)(((int32_t)(y_3-y_2)*(input_x-x_2))/interpolation_tempW0);
/**********************************************************************************/        
    } else if ( input_x < x_4 ) {
/************************* CT 개선 전********** 2005.02.21  *************************    
    //  tempW9 =  y_2 + (int16_t)((((int32_t)(y_3-y_2))*(input_x-x_2))/(x_3-x_2));
        tempW2 = (y_4-y_3); tempW1 = (input_x-x_3); tempW0 = (x_4-x_3);
        if ( tempW0==0 ) tempW0=1;
        s16muls16divs16();
        tempW9 = y_3 + tempW3;
************************************************************************************/        
        
/**************************** CT 개선 후 : 2005.02.21(C 코드로 환원) **************/
        interpolation_tempW0=(x_4-x_3);
        if (interpolation_tempW0==0) interpolation_tempW0=1;
        else{}
        interpolation_tempW9 =  y_3 + (int16_t)(((int32_t)(y_4-y_3)*(input_x-x_3))/interpolation_tempW0);
/**********************************************************************************/        
    } else if ( input_x < x_5 ) {
/************************* CT 개선 전********** 2005.02.21  *************************    
    //  tempW9 =  y_4 + (int16_t)((((int32_t)(y_5-y_4))*(input_x-x_4))/(x_5-x_4));
        tempW2 = (y_5-y_4); tempW1 = (input_x-x_4); tempW0 = (x_5-x_4);
        if ( tempW0==0 ) tempW0=1;
        s16muls16divs16();
        tempW9 = y_4 + tempW3;        
************************************************************************************/        
        
/**************************** CT 개선 후 : 2005.02.21(C 코드로 환원) **************/
        interpolation_tempW0=(x_5-x_4);
        if (interpolation_tempW0==0) interpolation_tempW0=1;
        else{}
        interpolation_tempW9 =  y_4 + (int16_t)(((int32_t)(y_5-y_4)*(input_x-x_4))/interpolation_tempW0);
/**********************************************************************************/        
    } else
        interpolation_tempW9 = y_5;
            
    return interpolation_tempW9;      
}

int16_t LCESP_s16IInter6Point( int16_t input_x, int16_t x_1, int16_t y_1, int16_t x_2, int16_t y_2, int16_t x_3, int16_t y_3, int16_t x_4, int16_t y_4, int16_t x_5, int16_t y_5, int16_t x_6, int16_t y_6 )
{
	#if __ECU==ABS_ECU_1
	int16_t interpolation_tempW0;
	int16_t interpolation_tempW9;
	#endif

    if( input_x <= x_1 )
        interpolation_tempW9 = y_1;
    else if ( input_x < x_2 ) {
/************************* CT 개선 전********** 2005.02.21  *************************** 
    //  tempW9 =  y_1 + (int16_t)((((int32_t)(y_2-y_1))*(input_x-x_1))/(x_2-x_1));
        tempW2 = (y_2-y_1); tempW1 = (input_x-x_1); tempW0 = (x_2-x_1);
        if ( tempW0==0 ) tempW0=1;
        s16muls16divs16();
        tempW9 = y_1 + tempW3;         
**************************************************************************************/ 

/**************************** CT 개선 후 : 2005.02.21(C 코드로 환원) ****************/
        interpolation_tempW0=(x_2-x_1);
        if (interpolation_tempW0==0) interpolation_tempW0=1;
        else{}
        interpolation_tempW9 =  y_1 + (int16_t)(((int32_t)(y_2-y_1)*(input_x-x_1))/interpolation_tempW0);
/************************************************************************************/
        
    } else if ( input_x < x_3 ) {
/************************* CT 개선 전********** 2005.02.21  *************************    
    //  tempW9 =  y_2 + (int16_t)((((int32_t)(y_3-y_2))*(input_x-x_2))/(x_3-x_2));
        tempW2 = (y_3-y_2); tempW1 = (input_x-x_2); tempW0 = (x_3-x_2);
        if ( tempW0==0 ) tempW0=1;
        s16muls16divs16();
        tempW9 = y_2 + tempW3;         
************************************************************************************/        
        
/**************************** CT 개선 후 : 2005.02.21(C 코드로 환원) **************/
        interpolation_tempW0=(x_3-x_2);
        if (interpolation_tempW0==0) interpolation_tempW0=1;
        else{}
        interpolation_tempW9 =  y_2 + (int16_t)(((int32_t)(y_3-y_2)*(input_x-x_2))/interpolation_tempW0);
/**********************************************************************************/        
    } else if ( input_x < x_4 ) {
/************************* CT 개선 전********** 2005.02.21  *************************    
    //  tempW9 =  y_2 + (int16_t)((((int32_t)(y_3-y_2))*(input_x-x_2))/(x_3-x_2));
        tempW2 = (y_4-y_3); tempW1 = (input_x-x_3); tempW0 = (x_4-x_3);
        if ( tempW0==0 ) tempW0=1;
        s16muls16divs16();
        tempW9 = y_3 + tempW3;
************************************************************************************/        
        
/**************************** CT 개선 후 : 2005.02.21(C 코드로 환원) **************/
        interpolation_tempW0=(x_4-x_3);
        if (interpolation_tempW0==0) interpolation_tempW0=1;
        else{}
        interpolation_tempW9 =  y_3 + (int16_t)(((int32_t)(y_4-y_3)*(input_x-x_3))/interpolation_tempW0);
/**********************************************************************************/        
    } else if ( input_x < x_5 ) {
/************************* CT 개선 전********** 2005.02.21  *************************    
    //  tempW9 =  y_4 + (int16_t)((((int32_t)(y_5-y_4))*(input_x-x_4))/(x_5-x_4));
        tempW2 = (y_5-y_4); tempW1 = (input_x-x_4); tempW0 = (x_5-x_4);
        if ( tempW0==0 ) tempW0=1;
        s16muls16divs16();
        tempW9 = y_4 + tempW3;        
************************************************************************************/        
        
/**************************** CT 개선 후 : 2005.02.21(C 코드로 환원) **************/
        interpolation_tempW0=(x_5-x_4);
        if (interpolation_tempW0==0) interpolation_tempW0=1;
        else{}
        interpolation_tempW9 =  y_4 + (int16_t)(((int32_t)(y_5-y_4)*(input_x-x_4))/interpolation_tempW0);
/**********************************************************************************/        
    } else if ( input_x < x_6 ) {
 /************************* CT 개선 전********** 2005.02.21  *************************    
       tempW2 = (y_6-y_5); tempW1 = (input_x-x_5); tempW0 = (x_6-x_5);
        if ( tempW0==0 ) tempW0=1;
        s16muls16divs16();
        tempW9 = y_5 + tempW3;        
************************************************************************************/        
        
/**************************** CT 개선 후 : 2005.02.21(C 코드로 환원) **************/
        interpolation_tempW0=(x_6-x_5);
        if (interpolation_tempW0==0) interpolation_tempW0=1;
        else{}
        interpolation_tempW9 =  y_5 + (int16_t)(((int32_t)(y_6-y_5)*(input_x-x_5))/interpolation_tempW0);
/**********************************************************************************/        
    } else
        interpolation_tempW9 = y_6;
            
    return interpolation_tempW9;      
}

int16_t LCESP_s16IInter7Point( int16_t input_x, int16_t x_1, int16_t y_1, int16_t x_2, int16_t y_2, int16_t x_3, int16_t y_3, int16_t x_4, int16_t y_4, int16_t x_5, int16_t y_5, int16_t x_6, int16_t y_6, int16_t x_7, int16_t y_7 )
{
	#if __ECU==ABS_ECU_1
	int16_t interpolation_tempW0;
	int16_t interpolation_tempW9;
	#endif

    if( input_x <= x_1 )
        interpolation_tempW9 = y_1;
    else if ( input_x < x_2 ) {
        interpolation_tempW0=(x_2-x_1);
        if (interpolation_tempW0==0) interpolation_tempW0=1;
        else{}
        interpolation_tempW9 =  y_1 + (int16_t)(((int32_t)(y_2-y_1)*(input_x-x_1))/interpolation_tempW0);
    } else if ( input_x < x_3 ) {
        interpolation_tempW0=(x_3-x_2);
        if (interpolation_tempW0==0) interpolation_tempW0=1;
        else{}
        interpolation_tempW9 =  y_2 + (int16_t)(((int32_t)(y_3-y_2)*(input_x-x_2))/interpolation_tempW0);
    } else if ( input_x < x_4 ) {
        interpolation_tempW0=(x_4-x_3);
        if (interpolation_tempW0==0) interpolation_tempW0=1;
        else{}
        interpolation_tempW9 =  y_3 + (int16_t)(((int32_t)(y_4-y_3)*(input_x-x_3))/interpolation_tempW0);
    } else if ( input_x < x_5 ) {
        interpolation_tempW0=(x_5-x_4);
        if (interpolation_tempW0==0) interpolation_tempW0=1;
        else{}
        interpolation_tempW9 =  y_4 + (int16_t)(((int32_t)(y_5-y_4)*(input_x-x_4))/interpolation_tempW0);
    } else if ( input_x < x_6 ) {
        interpolation_tempW0=(x_6-x_5);
        if (interpolation_tempW0==0) interpolation_tempW0=1;
        else{}
        interpolation_tempW9 =  y_5 + (int16_t)(((int32_t)(y_6-y_5)*(input_x-x_5))/interpolation_tempW0);
    } else if ( input_x < x_7 ) {
        interpolation_tempW0=(x_7-x_6);
        if (interpolation_tempW0==0) interpolation_tempW0=1;
        else{}
        interpolation_tempW9 =  y_6 + (int16_t)(((int32_t)(y_7-y_6)*(input_x-x_6))/interpolation_tempW0);        
    } else
        interpolation_tempW9 = y_7;
            
    return interpolation_tempW9;      
}

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCESPCalInterpolation
	#include "Mdyn_autosar.h"
#endif
