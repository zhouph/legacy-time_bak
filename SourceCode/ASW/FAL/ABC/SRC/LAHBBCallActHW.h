#ifndef __LAHBBCALLACTHW_H__
#define __LAHBBCALLACTHW_H__

/*includes********************************************************************/
#include "LVarHead.h"


/*Global Type Declaration ****************************************************/



/*Global Extern Variable  Declaration*****************************************/
  
/*Global Extern Functions  Declaration****************************************/
  #if __TCMF_LowVacuumBoost
extern void	LAHBB_vCallActHW(void);
  #endif
  
#endif
