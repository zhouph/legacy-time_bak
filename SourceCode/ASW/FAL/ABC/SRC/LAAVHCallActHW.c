/******************************************************************************
* Project Name: Auto Vehicle Hold
* File: LAAVHCallActHW.C
* Description: Valve/Motor actuaction by Auto Vehicle Hold Controller
* Date: June. 4. 2009
******************************************************************************/

/* Includes ******************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
           #define idx_START
           #define idx_FILE  idx_CL_LAAVHCallActHW
           #include "Mdyn_autosar.h"
#endif


#include "LAAVHCallActHW.h"
#include "LCAVHCallControl.h"
#include "LAABSCallMotorSpeedControl.h"
#include "Hardware_Control.h"
#include "LCESPCalInterpolation.h"
#include "hm_logic_var.h"

#if __AVH
/* Variables Definition*******************************************************/
struct	U8_BIT_STRUCT_t AVHAF0;

uint8_t lcu8AvhInitCurDownTime, AVH_debuger2;

int16_t	lcs16Avhdropcurrent, lcs16Avhdropcurrentholdtime;
int16_t lcs16AvhCurrentTemp, lcs16AvhDownDrvInitDrop;
int16_t avh_msc_target_vol;
int16_t lcs16AvhCurDropByCycle, lcs16AvhCurInitComp, lcs16AvhCurInitMaxComp;

/* LocalFunction prototype ***************************************************/
void	LAAVH_vCallActHW(void);
void	LCMSC_vSetAVHTargetVoltage(void);
int16_t	    LAMFC_s16SetMFCCurrentAVH(int16_t MFC_Current_old);

/* Implementation*************************************************************/



/******************************************************************************
* FUNCTION NAME:      LAAVH_vCallActHW
* CALLED BY:          LA_vCallMainValve()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        Valve/Motor actuaction by Hill Start Assist Controller
******************************************************************************/
void	LAAVH_vCallActHW(void)
{
	if((lcu1AvhActFlg==1)||(lcu1AvhAutonomousActFlg==1)) 
	{
	  #if (__SPLIT_TYPE==0)	
		TCL_DEMAND_fl = AVH_TCL_DEMAND_fl;
		TCL_DEMAND_fr = AVH_TCL_DEMAND_fr;
		
		if(lcs8AvhControlState==AVH_APPLY) 
		{
			S_VALVE_LEFT = AVH_S_VALVE_LEFT;
			S_VALVE_RIGHT = AVH_S_VALVE_RIGHT;
			VDC_MOTOR_ON = AVH_MSC_MOTOR_ON;
		}		
	  #else
		TCL_DEMAND_SECONDARY = AVH_TCL_DEMAND_fl;
		TCL_DEMAND_PRIMARY = AVH_TCL_DEMAND_fr;
		
		if(lcs8AvhControlState==AVH_APPLY) 
		{
			S_VALVE_SECONDARY = AVH_S_VALVE_LEFT;
			S_VALVE_PRIMARY = AVH_S_VALVE_RIGHT;
			VDC_MOTOR_ON = AVH_MSC_MOTOR_ON;
		}		
	  #endif	
		VALVE_ACT = 1;
	}
	else 
	{
		;
	}
}

int16_t 	LAMFC_s16SetMFCCurrentAVH(int16_t MFC_Current_old)
{
    static int16_t MFC_AVH_Current_new;

    if((lcu1AvhActFlg==1)||(lcu1AvhAutonomousActFlg==1))
    {
        lcs16AvhCurrentTemp = LCESP_s16IInter4Point(lcs16AvhTargetPress, lcs16AvhMinEntPres,  S16_AVH_P_TO_DUTY_MIN_CUR, 
                                                                         S16_AVH_P_TO_DUTY_LOW_REF_P,  S16_AVH_P_TO_DUTY_LOW_TH_CUR, 
        																 S16_AVH_P_TO_DUTY_MED_REF_P,  S16_AVH_P_TO_DUTY_MED_TH_CUR,
                                                                         S16_AVH_P_TO_DUTY_HIGH_REF_P, S16_AVH_P_TO_DUTY_HIGH_TH_CUR);

        lcs16AvhCurrentTemp = lcs16AvhCurrentTemp + (int16_t)(AVH_TC_CUR_COMP_CURENT*(lcs16AvhHoldCnt/AVH_TC_CUR_COMP_CYCLE));
       
		  #if __AVH_NOISE_REDUCTION == ENABLE
        lcs16AvhCurInitComp = LCESP_s16IInter4Point(lcs16AvhCurrentTemp, S16_AVH_P_TO_DUTY_MIN_CUR,      S16_AVH_MIN_CUR_INIT_COMP, 
                                                                         S16_AVH_P_TO_DUTY_LOW_TH_CUR,   S16_AVH_LOW_CUR_INIT_COMP, 
                                                                         S16_AVH_P_TO_DUTY_MED_TH_CUR,   S16_AVH_MED_CUR_INIT_COMP, 
        																 S16_AVH_P_TO_DUTY_HIGH_TH_CUR,  S16_AVH_HIGH_CUR_INIT_COMP);

		lcs16AvhCurInitMaxComp = S16_AVH_CUR_INIT_MAX_CUR_COMP;
		  #endif
		  
    	if (lcs8AvhControlState==AVH_HOLD)
    	{
    		  #if __AVH_NOISE_REDUCTION == ENABLE
    		if((lcu1AvhActFlg==1)&&(lcu8AvhInitCurCnt<=U8_AVH_CUR_INIT_UP_TIME))
            {
                MFC_AVH_Current_new = LCESP_s16IInter2Point((int16_t)lcu8AvhInitCurCnt, 1, (lcs16AvhCurrentTemp-lcs16AvhCurInitComp),
                                                                     (int16_t)U8_AVH_CUR_INIT_UP_TIME, (lcs16AvhCurrentTemp+lcs16AvhCurInitMaxComp));
                lcu8AvhInitCurDownTime = U8_AVH_CUR_INIT_DOWN_TIME;
                AVH_debuger2=1;                
            }
            else if((lcu1AvhAutonomousActFlg==1)&&(lcu8AvhInitCurCnt<=U8_AVH_CUR_INIT_UP_TIME))
            {
                MFC_AVH_Current_new = LCESP_s16IInter2Point((int16_t)lcu8AvhInitCurCnt, 1, lcs16AvhCurrentTemp,
                                                                     (int16_t)U8_AVH_CUR_INIT_UP_TIME, (lcs16AvhCurrentTemp+lcs16AvhCurInitMaxComp));
                lcu8AvhInitCurDownTime = U8_AVH_CUR_INIT_DOWN_TIME;
                AVH_debuger2=41;                
            }
            else if((lcu8AvhReBrkReleaseCnt>0)&&(lcu8AvhReBrkReleaseCnt<=U8_AVH_CUR_INIT_UP_TIME))
            {
            	MFC_AVH_Current_new = LCESP_s16IInter2Point((int16_t)lcu8AvhReBrkReleaseCnt, 1, (lcs16AvhCurrentTemp-lcs16AvhCurInitComp),
                                                                          (int16_t)U8_AVH_CUR_INIT_UP_TIME, (lcs16AvhCurrentTemp+lcs16AvhCurInitMaxComp));
                lcu8AvhInitCurDownTime = U8_AVH_CUR_INIT_DOWN_TIME;
                AVH_debuger2=40;                
            }
    		  #else
    		if((((lcu1AvhAutonomousActFlg==1)||(lcu1AvhActFlg==1))&&(lcu8AvhInitCurCnt<L_U8_TIME_10MSLOOP_40MS))||(lcu1AvhReBrkRelease==1))            
            {
                MFC_AVH_Current_new = AVH_CUR_AT_INITIAL;
                lcu8AvhInitCurDownTime = U8_AVH_CUR_INIT_DOWN_TIME;
                AVH_debuger2=1;                
            }
              #endif
            else if(lcu1AvhReBrk==1)
            {
                MFC_AVH_Current_new=AVH_TCMF_CTRL_MIN_CURRENT;
                AVH_debuger2=2; 
            }	
            else 
            {
            	if(lcu8AvhRiseEndCnt>0)
            	{
                    MFC_AVH_Current_new = LCESP_s16IInter2Point((int16_t)lcu8AvhRiseEndCnt, 1, lcs16AvhCurrentTemp,
                                                                 (int16_t)U8_AVH_CUR_DOWN_TIME_AFTER_RISE, AVH_CUR_AT_RISE);
                    AVH_debuger2=3;                                                                
                }
                else
                {
                	  #if __AVH_NOISE_REDUCTION == ENABLE
                    if(MFC_Current_old>lcs16AvhCurrentTemp)
                	{
                		MFC_AVH_Current_new = LCESP_s16IInter2Point((int16_t)lcu8AvhInitCurDownTime, 0, lcs16AvhCurrentTemp,
                                                                       (int16_t)U8_AVH_CUR_INIT_DOWN_TIME, (lcs16AvhCurrentTemp+lcs16AvhCurInitMaxComp));
                        AVH_debuger2=4;                                                                    
                	}                	  
                	  #else
                	if(MFC_Current_old>lcs16AvhCurrentTemp)
                	{
                		MFC_AVH_Current_new = LCESP_s16IInter2Point((int16_t)lcu8AvhInitCurDownTime, 0, lcs16AvhCurrentTemp,
                                                                       (int16_t)U8_AVH_CUR_INIT_DOWN_TIME, AVH_CUR_AT_INITIAL);
                        AVH_debuger2=4;                                                                    
                	}
                	  #endif
                	else
                	{
           	            MFC_AVH_Current_new = lcs16AvhCurrentTemp;
           	            AVH_debuger2=5;
           	        }
           	    }
            }
        }
    	else if (lcs8AvhControlState==AVH_APPLY)
    	{
    		MFC_AVH_Current_new = AVH_CUR_AT_RISE;
    		AVH_debuger2=6;
    	}
		else if (lcs8AvhControlState==AVH_RELEASE)
    	{
    		if (lcu1AvhDownDrive==1)
    		{
    			lcs16AvhDownDrvInitDrop = LCESP_s16IInter2Point(lcs16AvhAxGAbs, 8, S16_AVH_F_O_CUR_DROP_DW_DRV_MIN,    
    			                                                                25, S16_AVH_F_O_CUR_DROP_DW_DRV_MAX);
    			                                                                  
                lcs16AvhDownDrvInitDrop = lcs16AvhDownDrvInitDrop + (int16_t)(AVH_TC_CUR_COMP_CURENT*(lcs16AvhHoldCnt/AVH_TC_CUR_COMP_CYCLE));
                
    			if (lcs16AvhReleaseTime==1)
    			{
   					MFC_AVH_Current_new = MFC_Current_old - lcs16AvhDownDrvInitDrop;
   					AVH_debuger2=8;
    			}
    			else if (((lcs16AvhReleaseTime%(int16_t)U8_AVH_F_O_SCAN_DOWN_DRV)==0)&&(MFC_Current_old>AVH_TCMF_CTRL_MIN_CURRENT))
    			{
    				if(lcu1AvhActFlg==1) {lcs16AvhCurDropByCycle = (int16_t)U8_AVH_F_O_CUR_DOWN_DRV;}
    				else /*autonomous*/  {lcs16AvhCurDropByCycle = (int16_t)U8_AVH_F_O_CUR_DOWN_DRV_AUTO;}
    				
    			    MFC_AVH_Current_new = MFC_Current_old-(lcs16AvhCurDropByCycle*(int16_t)lcu8AvhDownDrvLevel);
    			    AVH_debuger2=9;
    			}
    			else
    			{
    				MFC_AVH_Current_new = MFC_Current_old;
    				AVH_debuger2=10;
    			}
    		}
    		else if ((lcu1AvhAccelExitOK==1)&&(lcs16AvhAxGAbs<=(int16_t)U8_AVH_F_O_FLAT_SLOPE_TH))
    		{
    			if (lcs16AvhReleaseTime==1)
    			{
   					MFC_AVH_Current_new = MFC_Current_old - S16_AVH_F_O_CUR_DROP_FLAT;
   					AVH_debuger2=12;
    			}
    			else if (((lcs16AvhReleaseTime%(int16_t)U8_AVH_F_O_SCAN_FLAT)==0)&&(MFC_Current_old>=AVH_TCMF_CTRL_MIN_CURRENT))
    			{
    				if(lcu1AvhActFlg==1) {lcs16AvhCurDropByCycle = (int16_t)U8_AVH_F_O_CUR_FLAT;}
    				else /*autonomous*/  {lcs16AvhCurDropByCycle = (int16_t)U8_AVH_F_O_CUR_FLAT_AUTO;}
    				
    				MFC_AVH_Current_new = MFC_Current_old-(lcs16AvhCurDropByCycle*(int16_t)lcu8AvhPrateLevel);
    				AVH_debuger2=13;
    			}
    			else
    			{
    				MFC_AVH_Current_new = MFC_Current_old;
    				AVH_debuger2=14;
    			}
			}
            else if (lcu1AvhAccelExitOK==1)
            {
    			if (lcs16AvhReleaseTime==1)
    			{
   					MFC_AVH_Current_new = MFC_Current_old - S16_AVH_F_O_CUR_DROP_UPHILL;
   					AVH_debuger2=24;
    			}
    			else if (((lcs16AvhReleaseTime%(int16_t)U8_AVH_F_O_SCAN_UPHILL)==0)&&(MFC_Current_old>=AVH_TCMF_CTRL_MIN_CURRENT))
    			{
    				if(lcu1AvhActFlg==1) {lcs16AvhCurDropByCycle = (int16_t)U8_AVH_F_O_CUR_UPHILL;}
    				else /*autonomous*/  {lcs16AvhCurDropByCycle = (int16_t)U8_AVH_F_O_CUR_UPHILL_AUTO;}
    				
    				MFC_AVH_Current_new = MFC_Current_old-(lcs16AvhCurDropByCycle*(int16_t)lcu8AvhPrateLevel);
    				AVH_debuger2=25;
    			}
    			else
    			{
    				MFC_AVH_Current_new = MFC_Current_old;
    				AVH_debuger2=26;
    			}            	
            }
			else if (lcu1AvhMedExitSatisfy==1)
            {
    			if (lcs16AvhReleaseTime==1)
    			{
    				MFC_AVH_Current_new = MFC_Current_old - S16_AVH_F_O_CUR_DROP_MED;
    				AVH_debuger2=28;
   				}
    			else if (((lcs16AvhReleaseTime%(int16_t)U8_AVH_F_O_SCAN_MED)==0)&&(MFC_Current_old>=AVH_TCMF_CTRL_MIN_CURRENT))
    			{
    				MFC_AVH_Current_new = MFC_Current_old-(int16_t)U8_AVH_F_O_CUR_MED;
    				AVH_debuger2=29;
				}
				else 
				{
					MFC_AVH_Current_new = MFC_Current_old;
					AVH_debuger2=30;
				}             	
            }                		
    		else if (lcu1AvhSlowExitSatisfy==1)   /* AVH to EPB, AVH switch off...  */
    		{
    			if (lcs16AvhReleaseTime==1)
    			{
    				MFC_AVH_Current_new = MFC_Current_old - S16_AVH_F_O_CUR_DROP_SLOW;
    				AVH_debuger2=32;
   				}
    			else if (((lcs16AvhReleaseTime%(int16_t)U8_AVH_F_O_SCAN_SLOW)==0)&&(MFC_Current_old>=AVH_TCMF_CTRL_MIN_CURRENT))
    			{
    				MFC_AVH_Current_new = MFC_Current_old-(int16_t)U8_AVH_F_O_CUR_SLOW; 
    				AVH_debuger2=33;  				
				}
				else 
				{
					MFC_AVH_Current_new = MFC_Current_old;
					AVH_debuger2=34;
				} 
			}
			else
			{
				MFC_AVH_Current_new = 0;
				AVH_debuger2=35;
			}
    	}
    	else
    	{
    		MFC_AVH_Current_new = 0;
    		AVH_debuger2=36;
    	}  
    	
        if (MFC_AVH_Current_new<AVH_TCMF_CTRL_MIN_CURRENT)
		{
		    MFC_AVH_Current_new = 0;
		}
		else 
		{
			;
		}    	    	  	        
    }
    else
    {
    	lcs16Avhdropcurrent =  0;       
    	lcs16Avhdropcurrentholdtime = 0; 
        MFC_AVH_Current_new = 0;
        lcs16AvhCurrentTemp=0;
        lcs16AvhCurInitComp=0;
        lcs16AvhCurInitMaxComp=0;
        AVH_debuger2=38;
    }
	
    return MFC_AVH_Current_new;
}
void	LCMSC_vSetAVHTargetVoltage(void)
{
	if(AVH_MSC_MOTOR_ON==1)
	{
		avh_msc_target_vol = S16_AVH_MOTOR_TARGET_VOLT;
	}
	else 
	{
		avh_msc_target_vol = MSC_0_V;
	}
}
#endif

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LAAVHCallActHW 
	#include "Mdyn_autosar.h"               
#endif
