/*******************************************************************************
* Project Name:     MGH40_ESP
* File Name:        LCPBACallControl.c
* Description:      Main Control of PBA
* Logic version:    HV25
********************************************************************************
*  Modification Log
*  Date         Author          Description
*  -----        -----           -----
*  5C12         eslim           Initial Release
*  6216         eslim           Modified based on MISRA rule
********************************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCPBACallControl
	#include "Mdyn_autosar.h"
#endif
/* Includes ********************************************************************/


#include "LCPBACallControl.h"
#include "LCESPInterfaceSlipController.h"
#include "LSESPCalSlipRatio.h"

#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
#include "LSESPFilterEspSensor.h"
#include "LSESPCalSensorOffset.h"
#endif

#if __CDM

#include "../SRC/SCC/LCSCCCallControl.h"
#endif

#include "LAABSCallMotorSpeedControl.h"

#include "LCFBCCallControl.h"

#if(__PBA_PRE_ABS_ENABLE == 1)
int16_t ls16PBAPDTCount;
void LCPBA_vDetPreABS(void);
#endif

int16_t   lcs16PbaSpeedEnterThr;

#if __VDC
/* Logcal Definiton ************************************************************/

int16_t   PBA_EXIT_CONTROL_cnt;
int16_t	  pba_speed_slip;
uint8_t   BA_HOLD_MPRESS_timer;


#if __TSP
int8_t      lctspu1TspPbaComb;
#endif

extern uint8_t mp_hw_err_cnt;

#if __HOB
struct  U8_BIT_STRUCT_t HOB01;

uint8_t  lcu8HobOtherCtrlCheck,lu8HobOnCounter;
int16_t  lcs16HobMscTargetV;
#endif

/* Variables Definition ********************************************************/

/* #define U16_CDM_PRESS_RISE_RATE_THR2    40 */

/* Local Function prototype ****************************************************/
void LCPBA_vCallControl(void);
void LCPBA_vCalVariable(void);
void LCPBA_vInitializeVariables(void);
void LCPBA_vCalEnterslipcondition(void);

#if PBA_DUMP_PATTERN
void LCPBA_vActDumpMode(void);
#endif

void LCPBA_vExitControl(void);
void LCPBA_vResetVaribles(void);

#if __HOB
void LCHOB_vCallControl(void);
void LCHOB_vInterfaceOtherSystems(void);
void LCHOB_vCheckInhibit(void);
void LCHOB_vDecideBrakeBoost(void);
void LCMSC_vSetHOBTargetVoltage(void);
void LAHOB_vCallActHW(void);	
#endif

/* Implementation **************************************************************/


/*******************************************************************************
* FUNCTION NAME:        LCPBA_vActDumpMode
* CALLED BY:            LCPBA_vCallControl()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Actuate Valves in Dump Mode of PBA
********************************************************************************/

#if PBA_DUMP_PATTERN
void LCPBA_vActDumpMode(void)
{
    tempW0=PBA_STANDARD;

  #if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
    if((lsesps16EstBrkPressByBrkPedalF < S16_PBA_PRESS_EXIT_THR3)&&(lsespu1BrkAppSenInvalid == 0))
  #else
    if(mpress < S16_PBA_PRESS_EXIT_THR3)
  #endif  	
    {
        if(PBA_hold_timer < PBA_DUMP_MODE_MAX_TIME)
        {
            PBA_hold_timer++;
            if(PBA_hold_timer % PBA_DUMP_MODE_HOLD_TIME == 1)
            {
                tempW0=PBA_DUMP;
            }
            else
            {
                tempW0=PBA_HOLD;
            }

            if(PBA_ACT==1)
            {
                PBA_pulsedown = 1;
            }
            else
            {
                PBA_pulsedown = 0;
            }

        }
        else
        {
            PBA_WORK = 0;
            PBA_BIG_SLOP=0;
            LCPBA_vResetVaribles();
            PBA_pulsedown = 0;
        }
    }
    else
    {
        if(PBA_hold_timer < PBA_DUMP_MODE_MAX_TIME)
        {
            PBA_hold_timer++;
            if(PBA_hold_timer%PBA_DUMP_MODE_HOLD_TIME == 1)
            {
                tempW0=PBA_DUMP;
            }
            else
            {
                tempW0=PBA_HOLD;
            }

            if(PBA_ON==1)
            {
                PBA_pulsedown = 1;
            }
            else
            {
                PBA_pulsedown = 0;
            }
          #if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)  
            if((lsesps16EstBrkPressByBrkPedalF >= U16_PBA_PRESS_ENTER_THR)&&(lsespu1BrkAppSenInvalid == 0)&&(ABS_fz==0))
          #else
            if((mpress >= U16_PBA_PRESS_ENTER_THR)&&(ABS_fz==0))
          #endif  	
    		{
             	PBA_mode=PBA_HOLD;
             	PBA_pulsedown = 0 ;
     		}
        }
        else
        {
            PBA_WORK = 0;
            PBA_BIG_SLOP=0;
            LCPBA_vResetVaribles();
            PBA_pulsedown = 0;
        }
    }
	
#if !__SPLIT_TYPE
    VDC_MOTOR_ON=0;
    TCL_DEMAND_fl=1;
    TCL_DEMAND_fr=1;
    S_VALVE_LEFT=0;
    S_VALVE_RIGHT=0;
#else
    VDC_MOTOR_ON=0;
    TCL_DEMAND_PRIMARY=1;
    TCL_DEMAND_SECONDARY=1;
    S_VALVE_PRIMARY=0;
    S_VALVE_SECONDARY=0;
#endif
    switch(tempW0)
    {
        case PBA_STANDARD:
            HV_VL_fl=0; AV_VL_fl=0;
            HV_VL_fr=0; AV_VL_fr=0;

            HV_VL_rl=0; AV_VL_rl=0;
            HV_VL_rr=0; AV_VL_rr=0;
            break;

        case PBA_DUMP:
            HV_VL_fl=1; AV_VL_fl=1;
            HV_VL_fr=1; AV_VL_fr=1;

			if(EBD_RA== 0)
			{
            	HV_VL_rl=1; AV_VL_rl=1;
            	HV_VL_rr=1; AV_VL_rr=1;
            }
            break;

        case PBA_HOLD:
            HV_VL_fl=1;
            HV_VL_fr=1;

            HV_VL_rl=1;
            HV_VL_rr=1;
            break;
        default:

        break;
    }
}
#endif

/*******************************************************************************
* FUNCTION NAME:        LCPBA_vExitControl
* CALLED BY:            LCPBA_vCallControl()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Exit Control of PBA
********************************************************************************/
void LCPBA_vExitControl(void)
{
    if((PBA_MPS_ERROR==1)||(fu1MCPErrorDet==1))
    {
        LCPBA_vInitializeVariables();
    }
    else
    {
        ;
    }
    
	if(((fu1BLSErrDet==1)||(fu1BlsSusDet==1)) && (U16_PBA_PRESS_EXIT_THR1<MPRESS_15BAR))
	{
		esp_tempW0 = MPRESS_15BAR;
	}
	else
	{
		esp_tempW0 = U16_PBA_PRESS_EXIT_THR1;
	}

#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
    if(((lsesps16EstBrkPressByBrkPedalF<esp_tempW0)&&(lsespu1BrkAppSenInvalid == 0))
	||(vdc_vref<(int16_t)U16_PBA_SPEED_EXIT_THR))
#else
    if((mpress<esp_tempW0)||(vdc_vref<(int16_t)U16_PBA_SPEED_EXIT_THR))
#endif
    {
        /* Pressure pattern control logic for BAS off condition. */
        if(PBA_ACT==1)
        {
            PBA_hold_timer=0;
        }
        else
        {
            ;
        }

        PBA_ACT=0;
        PBA_BIG_SLOP=0;
        PBA_mode=PBA_STANDARD;
        PBA_ABS_timer=0;
        PBA_LOW_MU=0;
        ABS_CHK=0;
        BA_HOLD_MPRESS_timer=0;
      #if __TSP  
        lctspu1TspPbaComb = 0;
      #endif  
    }
#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
    else if((PBA_ACT==1)
    	 &&(lsesps16EstBrkPressByBrkPedalF>((int16_t)U16_PBA_PRESS_EXIT_THR2))&&(lsespu1BrkAppSenInvalid == 0))
#else
    else if((PBA_ACT==1)&&(mpress>((int16_t)U16_PBA_PRESS_EXIT_THR2)))
#endif
    {
        LCPBA_vResetVaribles();
    }
   /*******************************************************
    *  Add BIG SLOP EXIT CONDITION By Lee Woong Ki in 2011.01.28
    *******************************************************/
#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
    else if((PBA_BIG_SLOP==1)&&(lsesps16EstBrkPressByBrkPedalF>((int16_t)U16_PBA_PRESS_EXIT_THR2-MPRESS_20BAR))
            &&(lsesps16EstBrkPressByBrkPedalF<((int16_t)U16_PBA_PRESS_EXIT_THR2))&&(lsespu1BrkAppSenInvalid == 0))
#else
    else if((PBA_BIG_SLOP==1)&&(mpress>((int16_t)U16_PBA_PRESS_EXIT_THR2-MPRESS_20BAR))
            &&(mpress<((int16_t)U16_PBA_PRESS_EXIT_THR2)))
#endif
    {
        	BA_HOLD_MPRESS_timer++;

        	if(BA_HOLD_MPRESS_timer>=((int16_t)K_Brake_Boost_MSP_Rel_Threshold))
        	{
    	       PBA_BIG_SLOP=0;
    	       BA_HOLD_MPRESS_timer = 0;
    	    }   	       
    	    else
    	    {
    	    	  ;
    	    }
    }

    /*******************************************************
    *  Add STAND-BY MODE 2 by Kim jeonghun in 2003.6.12
    *******************************************************/
    
	#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
    else if((PBA_ACT==1)&&(mpress_slop_filt>MPRESS_6BAR))
    {
        if((lsesps16EstBrkPressByBrkPedalF>=(FL.s16_Estimated_Active_Press-MPRESS_10BAR))
         &&(lsesps16EstBrkPressByBrkPedalF>=(FR.s16_Estimated_Active_Press-MPRESS_10BAR))
         &&(lsespu1BrkAppSenInvalid == 0))
        {
            LCPBA_vResetVaribles();
            PBA_WORK=0;
        }
        else
        {
            ;
        }
    }
    #else
    else if((PBA_ACT==1)&&(mpress_slop_filt>MPRESS_6BAR))
    {
        if((mpress>=(FL.s16_Estimated_Active_Press-MPRESS_10BAR))
         &&(mpress>=(FR.s16_Estimated_Active_Press-MPRESS_10BAR)))
        {
            LCPBA_vResetVaribles();
            PBA_WORK=0;
        }
        else
        {
            ;
        }
    }
    #endif
    
    /************************************************************
    *   Add PBA_LOW_MU detect by KIM Jeonghun in 2004.6.16
    *************************************************************/
    else if((PBA_ACT==1)&&(AFZ_OK==1)&&(afz>-(int16_t)U16_PBA_ESP_MU_LOW/10))
    {
        LCPBA_vResetVaribles();
        PBA_LOW_MU=1;
    }
#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
    else if((PBA_ACT==1)
    	&&((lsesps16EstBrkPressByBrkPedalF>MPRESS_40BAR)&&(lsespu1BrkAppSenInvalid == 0))
		&&(AFZ_OK==1)&&(afz>-(int16_t)U16_PBA_ESP_MU_MEDIUM/10))
#else
    else if((PBA_ACT==1)&&(mpress>MPRESS_40BAR)
            &&(AFZ_OK==1)&&(afz>-(int16_t)U16_PBA_ESP_MU_MEDIUM/10))
#endif
    {
        LCPBA_vResetVaribles();
        PBA_LOW_MU=1;
    }
#if PBA_DUMP_PATTERN
    /* GM SSTS requiments number 1 */
	#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
    else if((PBA_ACT==1)&&(PBA_WORK==1)
    	&&(lsesps16EstBrkPressByBrkPedalF<S16_PBA_PRESS_EXIT_THR3)&&(lsespu1BrkAppSenInvalid == 0))
	#else
    else if((PBA_ACT==1)&&(PBA_WORK==1)&&(mpress<S16_PBA_PRESS_EXIT_THR3))
	#endif
    {
        if(PBA_MPS_exit_timer < S16_PBA_PRESS_EXIT_THR3_TIMER)
        {
            PBA_MPS_exit_timer++;
        }
        else
        {
            if(PBA_mode!=PBA_DUMP)
            {
                PBA_hold_timer=0;
            }
            else
            {
                ;
            }
            PBA_mode=PBA_DUMP;
            PBA_ABS_timer=0;
        }
    }
    /* End of GM SSTS requiments number 1 */
#endif
    else
    {
        ;
    }

    if((esp_mu<(int16_t)U16_PBA_ESP_MU_LOW)&&(AFZ_OK==1)&&(afz>-(int16_t)U16_PBA_ESP_MU_LOW/10))
    {
        LCPBA_vResetVaribles();
        PBA_LOW_MU=1;
    }
#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
    else if(((lsesps16EstBrkPressByBrkPedalF>MPRESS_40BAR)&&(lsespu1BrkAppSenInvalid == 0))
		&&(esp_mu<(int16_t)U16_PBA_ESP_MU_MEDIUM)&&(AFZ_OK==1)&&(afz>-(int16_t)U16_PBA_ESP_MU_MEDIUM/10))
#else
    else if((mpress>MPRESS_40BAR)&&(esp_mu<(int16_t)U16_PBA_ESP_MU_MEDIUM)
            &&(AFZ_OK==1)&&(afz>-(int16_t)U16_PBA_ESP_MU_MEDIUM/10))
#endif
    {
        LCPBA_vResetVaribles();
        PBA_LOW_MU=1;
    }
    else
    {
        ;
    }

    /* Change PBA thresold by Kim Jeonghun in 2003.10.06 */
#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
    if((PBA_BIG_SLOP==1)
      &&(lsesps16EstBrkPressByBrkPedalF>((int16_t)U16_PBA_PRESS_EXIT_THR2))&&(lsespu1BrkAppSenInvalid == 0))
#else
    if((PBA_BIG_SLOP==1)&&(mpress>((int16_t)U16_PBA_PRESS_EXIT_THR2)))
  #endif
		
    {
        LCPBA_vResetVaribles();
        PBA_BIG_SLOP=0;
        PBA_WORK=0;
    }
    else
    {
        ;
    }
    
  #if __CDM
	if((PBA_IN_CDM==1) && (lcu8CdmHBACmd<=0))
	{
        LCPBA_vResetVaribles();
        PBA_BIG_SLOP=0;
        PBA_WORK=0;
    }
    else 
    {
    	;
    }
  #endif
}

/*******************************************************************************
* FUNCTION NAME:        LCPBA_vResetVaribles
* CALLED BY:            LCPBA_vCallControl()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Reset Varibles of PBA
********************************************************************************/
void LCPBA_vResetVaribles(void)
{
    PBA_ACT=0;
    PBA_mode=PBA_STANDARD;
    PBA_hold_timer=0;
    PBA_ABS_timer=0;
    PBA_MPS_exit_timer=0;
    PBA_pulsedown = 0;
    BA_HOLD_MPRESS_timer = 0;
    
  #if __TSP  
    lctspu1TspPbaComb = 0;
  #endif  
    
  #if __CDM  
    PBA_IN_CDM=0;
  #endif
}

/*******************************************************************************
* FUNCTION NAME:        LCPBA_vCallControl
* CALLED BY:            LAESP_vCallActHW()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Main Control of PBA
********************************************************************************/
void LCPBA_vCallControl(void)
{
    /****************************************************************
    *  PBA control, start condition :
    *
    *  If big slope is detected, then set the PBA_BIG_SLOP flag.
    *  When PBA_BIG_SLOP flag is setted and mpress is larger
    *  than 40 bar, the PBA mode start.
    *----------------------------------------------------------
    *  PBA control, exit condition :
    *
    *  1. mpress drop less than 5 bar [EXIT MODE]
    *  2. vref become less than 5 kph [EXIT MODE]
    *  3. mpress rise more than 100 bar [STAND BY MODE 1]
    *  4. mpress_slop rise more than 8bar/4scantime [STAND BY MODE 2]
    *  5. esp_mu below 0.4 G (wet asphalt) [STAND BY MODE 3]
    *
    *                      Coded by Kim Jeonghun in 2002.7.11
    *******************************************************************/

    LCPBA_vCalVariable();
    LCPBA_vExitControl();

	if(PBA_mode == PBA_STANDARD)
	{
    	PBA_ACT=0;

		LCPBA_vCalEnterslipcondition();
                	
		#if __CDM
        	if((lcu8CdmHBACmd==1)||(lcu8CdmHBACmd==2)||(lcu8CdmHBACmd==3))
        	{
        		esp_tempW2 = U16_CDM_PRESS_RISE_RATE_THR2;
        	}
        	else
        	{
        		esp_tempW2 = U16_PBA_PRESS_RISE_RATE_THR2;
        	}
        #else
                esp_tempW2 = U16_PBA_PRESS_RISE_RATE_THR2;
        #endif	
		
		#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
    	    if((lsesps16EstBrkPressByBrkPedalF>=(int16_t)U16_PBA_PRESS_ENTER_THR)&&(lsespu1BrkAppSenInvalid == 0)
    		&&(vdc_vref>=(int16_t)U16_PBA_SPEED_ENTER_THR))
		#else
        	if((mpress>=(int16_t)U16_PBA_PRESS_ENTER_THR)&&(vdc_vref>=(int16_t)U16_PBA_SPEED_ENTER_THR))
        #endif		
            {
            if((PBA_BIG_SLOP==1)
             #if (__BRK_SIG_MPS_TO_PEDAL_SEN == 1)  
               &&((lsesps16EstBrkPressByBrkPedalF<=((int16_t)U16_PBA_PRESS_EXIT_THR2-MPRESS_20BAR))&&(lsespu1BrkAppSenInvalid == 0))
             #else
               &&(mpress<=((int16_t)U16_PBA_PRESS_EXIT_THR2-MPRESS_20BAR))
             #endif  
                   &&(((ABS_fz==0)&&(esp_mu>=(int16_t)U16_PBA_ESP_MU_HIGH))
                   ||((AFZ_OK==1)&&(afz<=-(int16_t)U16_PBA_ESP_MU_HIGH/10))
                   ||((ABS_fz==1)&&(AFZ_OK==0)&&((slip_m_fl_current > (-pba_speed_slip))||(slip_m_fr_current > (-pba_speed_slip)))))
                   &&(mpress_slop_filt<=(int16_t)esp_tempW2)&&(PBA_LOW_MU==0))
                {
				  
				  #if __CDM 	
                	if((lcu8CdmHBACmd>0) && (lcu1CdmRunFlg==1))
                	{
                		PBA_IN_CDM=1;	
                	}
                	else
                	{
                		;
                	}
                  #endif 	
				  
				  #if __TSP && (__TSP_PBA_COMBINATION==1)
					if(TSP_ON == 0)
					{
			      #endif		
                    PBA_ACT=1;
                    PBA_hold_timer=0;
                    trans_mpress=mpress;
                    PBA_mode=PBA_APPLY;

				  #if __TSP && (__TSP_PBA_COMBINATION==1)
					} 
					else
					{
						lctspu1TspPbaComb = 1;
					}
				  #endif		
                }
                else
                {
                    ;
                }
            }
            else
            {
                ;
            }

    }
	else if(PBA_mode == PBA_APPLY)
	{

#if __CAR==GM_TAHOE || __CAR ==GM_SILVERADO
            if((!ABS_fz)&&(PBA_hold_timer>L_U16_TIME_10MSLOOP_3S))
#else
            if((!ABS_fz)&&(PBA_hold_timer>T_T_A_H))
#endif
            {
		#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
                trans_mpress=lsesps16EstBrkPressByBrkPedalF;
		#else
                trans_mpress=mpress;
		#endif
                PBA_mode=PBA_HOLD;
            }
            else
            {
                ;
            }
	}
	else if(PBA_mode == PBA_HOLD)
	{
            if(d_trans_mpress>P_T_H_A)
            {
		#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
                trans_mpress=lsesps16EstBrkPressByBrkPedalF;
		#else
                trans_mpress=mpress;
		#endif
                PBA_hold_timer=0;
                PBA_mode=PBA_APPLY;
            }
            else
            {
                ;
            }
     }
     else
     {
       	;
     }

    if(PBA_ACT==1)
    {
        PBA_ON=1;
    }
    else
    {
        PBA_ON=0;
    }

    if((PBA_ON_old==1) && (PBA_ON==0))
    {
        PBA_EXIT_CONTROL = 1;
        PBA_EXIT_CONTROL_cnt = PBA_EXIT_CONTROL_TIME;
    }
    else if(PBA_EXIT_CONTROL_cnt>0)
    {
        PBA_EXIT_CONTROL_cnt--;
    }
    else
    {
        PBA_EXIT_CONTROL = 0;
    }

    PBA_ON_old = PBA_ON;
    
    if(PBA_mode==PBA_APPLY)
    {
       		PBA_WORK=1;
    }


}
void LCPBA_vCalEnterslipcondition(void)
{	
	int16_t v_v1 = S16_PBA_SLIP_SPEED_GAIN_DEP_L_V, v_v2 = S16_PBA_SLIP_SPEED_GAIN_DEP_M1_V; 
    int16_t v_v3 = S16_PBA_SLIP_SPEED_GAIN_DEP_M2_V, v_v4 =S16_PBA_SLIP_SPEED_GAIN_DEP_H_V;
     
    if( vrefk <= v_v1 )
    {
        pba_speed_slip = S16_PBA_ENTER_SLIP_LSP;
    }
    else if ( vrefk < v_v2 )
    {
        if( v_v2==v_v1 ) 
        {
        	v_v2 = v_v2 + 1;
        }
        else
        {
        	;
        }
        tempW3 = (int16_t)( (((int32_t)(S16_PBA_ENTER_SLIP_MSP-S16_PBA_ENTER_SLIP_LSP))*(vrefk-v_v1))/(v_v2-v_v1) );
        pba_speed_slip = S16_PBA_ENTER_SLIP_LSP + tempW3;               
    }
    else if(vrefk < v_v3)
    {
        pba_speed_slip = S16_PBA_ENTER_SLIP_MSP;
    }
    else if ( vrefk < v_v4 )
    {
        if( v_v4==v_v3 ) 
        {
        	v_v4 = v_v4 + 1;
        }
        else
        {
        	;
        }
        tempW3 = (int16_t)( (((int32_t)(S16_PBA_ENTER_SLIP_HSP-S16_PBA_ENTER_SLIP_MSP))*(vrefk-v_v3))/(v_v4-v_v3) );
        pba_speed_slip = S16_PBA_ENTER_SLIP_MSP + tempW3;         
    } 
    else
    {
        pba_speed_slip = S16_PBA_ENTER_SLIP_HSP;
    }

}

/*******************************************************************************
* FUNCTION NAME:        LCPBA_vInitializeVariables
* CALLED BY:            LCPBA_vCallControl()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Initialize Variables of PBA
********************************************************************************/
void LCPBA_vInitializeVariables(void)
{
    //=========================================================================
    //  PBA variables reset by Kim Jeonghun in 2004.5.28
    //-------------------------------------------------------------------------
    PBA_ACT=PBA_BIG_SLOP=PBA_WORK=0;
    PBA_mode=PBA_STANDARD;
    PBA_hold_timer=0;
    PBA_ABS_timer=0;
    PBA_MPS_exit_timer=0;
    
    /* mpress_slop=mpress_slop_filt=0;  **060208 modified by eslim */
    PBA_LOW_MU=0;
    ABS_CHK=0;
    PBA_pulsedown = 0;
    BA_HOLD_MPRESS_timer = 0;
    
  #if __TSP  
    lctspu1TspPbaComb = 0;
  #endif  
    
  #if __CDM  
    PBA_IN_CDM=0;
  #endif  
}

/*******************************************************************************
* FUNCTION NAME:        LCPBA_vCalVariable
* CALLED BY:            LCPBA_vCallControl()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Calculate Variables of PBA
********************************************************************************/
void LCPBA_vCalVariable(void)
{
	#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
    	d_trans_mpress=lsesps16EstBrkPressByBrkPedalF-trans_mpress;
	#else
        d_trans_mpress=mpress-trans_mpress;
	#endif
	
	#if __CDM
		if(lcu8CdmHBACmd==1)
		{
			esp_tempW0 = U16_CDM_PRESS_RISE_RATE_THR1_L1;
			esp_tempW1 = U16_CDM_SPEED_ENTER_THR;
		}
		else if(lcu8CdmHBACmd==2)
		{
			esp_tempW0 = U16_CDM_PRESS_RISE_RATE_THR1_L2;
			esp_tempW1 = U16_CDM_SPEED_ENTER_THR;
		}
		else if(lcu8CdmHBACmd==3)
		{
			esp_tempW0 = U16_CDM_PRESS_RISE_RATE_THR1_L3;
			esp_tempW1 = U16_CDM_SPEED_ENTER_THR;
		}
		else
		{
			esp_tempW0 = U16_PBA_PRESS_RISE_RATE_THR1;
			esp_tempW1 = U16_PBA_SPEED_ENTER_THR;
		}
	#else
	  esp_tempW0 = U16_PBA_PRESS_RISE_RATE_THR1;		//john
	  esp_tempW1 = U16_PBA_SPEED_ENTER_THR;
	#endif	
	
	lcs16PbaSpeedEnterThr = esp_tempW1;
		
    /*==========================================================
      Check the rise rate of mpress for setting PBA_BIG_SLOP
    ----------------------------------------------------------*/
    if((PBA_BIG_SLOP==0)&&(mpress_slop_filt>((int16_t)esp_tempW0)))
    {
        PBA_BIG_SLOP=1;
        PBA_WORK=0;
        PBA_hold_timer=0;
        PBA_ABS_timer=0;
        ABS_CHK=0;
    }
    else
    {
        ;
    }

    if(PBA_LOW_MU==1)
    {
        if((AFZ_OK==1)&&(afz<=-(int16_t)U16_PBA_ESP_MU_HIGH/10))
        {
            PBA_LOW_MU=0;
        }
        else
        {
            ;
        }
    }
    else
    {
        ;
    }

    if(ABS_fz==1)
    {
        ABS_CHK=1;
    }
    else
    {
        ;
    }
#if	(__PBA_PRE_ABS_ENABLE == 1)
    LCPBA_vDetPreABS();
#endif
    
}


#if	(__PBA_PRE_ABS_ENABLE == 1)
void LCPBA_vDetPreABS(void)
{
	if((lcpbau1PreABSActinPBA == 0) && (ABS_fz==0) && (PBA_ACT==1) 
		&& ((ARAD_AT_MINUS_B_fl == 1) || (ARAD_AT_MINUS_B_fr == 1))
		&& (ebd_filt_grv < (-U8_PBA_PRE_ABS_ENTER_DECEL))
		&& ((slip_m_fl <= (-U16_PBA_PRE_ABS_ENTER_WHL_SLIP))||(slip_m_fr <= (-U16_PBA_PRE_ABS_ENTER_WHL_SLIP))) )
	{
		lcpbau1PreABSActinPBA = 1;
	}
	else if((lcpbau1PreABSActinPBA == 1) && ((ABS_fz==1) || (PBA_ACT==0))

	)
	{
		lcpbau1PreABSActinPBA = 0;
	}
	else
	{
		ls16PBAPDTCount++;
    }
}
#endif

/* Hydraulic brake boost for Over Boost */
#if __HOB 
void LCHOB_vCallControl(void)
{
    LCHOB_vCheckInhibit();
    LCHOB_vInterfaceOtherSystems();
    LCHOB_vDecideBrakeBoost();	
}


void LCHOB_vCheckInhibit(void)
{
    ;	
}


void LCHOB_vInterfaceOtherSystems(void)
{
	 lcu8HobOtherCtrlCheck = 0;
    
    if(PBA_ON==1) 
    {
        lcu8HobOtherCtrlCheck = 1;
    }
  #if __FBC
    if(FBC_ON==1) 
    {
        lcu8HobOtherCtrlCheck = 2;
    }
  #endif 
}


void LCHOB_vDecideBrakeBoost(void)
{
    
    if((lcu1HobMpInitBigSlopOK==0)
     &&(lcu1HobOnFlag==0)
     &&(mpress_slop_filt>=S16_HOB_ENTER_INIT_MP_RATE)
     &&(vref>=VREF_10_KPH) 
   #if(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)  
     &&(lsespu1PedalBrakeOn==1)
   #else
     &&(MPRESS_BRAKE_ON==1)
   #endif 
    )
    {
    	lcu1HobMpInitBigSlopOK=1;
    }
    else if((lcu1HobOnFlag==1)
   #if(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
     ||((mpress_slop_filt<0)&&(lsesps16EstBrkPressByBrkPedalF>=S16_HOB_EXIT_MPRESS_LEVEL_HIGH)&&(lsespu1BrkAppSenInvalid==0))
   #else
     ||((mpress_slop_filt<0)&&(mpress>=S16_HOB_EXIT_MPRESS_LEVEL_HIGH))	
   #endif  	
     ||(vref<VREF_5_KPH) 
   #if(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)  
     ||(lsespu1PedalBrakeOn==1)
   #else
     ||(MPRESS_BRAKE_ON==1)
   #endif
    )
    {
    	lcu1HobMpInitBigSlopOK=0;
    }
    else{}
    	
    if(lcu1HobOnFlag==0)
    {
    	lu8HobOnCounter = 0;
    	if((lcu1HobMpInitBigSlopOK==1)
      #if (__BRK_SIG_MPS_TO_PEDAL_SEN == 1)  
        &&((lsesps16EstBrkPressByBrkPedalF>=S16_HOB_ENTER_MPRESS_LEVEL)&&(lsespu1BrkAppSenInvalid==0))
      #else
        &&(mpress>=S16_HOB_ENTER_MPRESS_LEVEL)
      #endif  
        &&(vref>=S16_HOB_ENTER_SPEED_MIN) 
        &&(ABS_fz == 0)
        &&(lcu8HobOtherCtrlCheck == 0)
        &&(ESP_ERROR_FLG==0))
        {
         	lcu1HobOnFlag = 1;
        }
        else{}
    }
    else
    {
    	if(lu8HobOnCounter<=250)
    	{
    		lu8HobOnCounter++;
    	}
    	else{}
    	
    	if((vref<S16_HOB_EXIT_SPEED_MIN)
      #if (__BRK_SIG_MPS_TO_PEDAL_SEN == 1)		
    	||((lsesps16EstBrkPressByBrkPedalF<S16_HOB_EXIT_MPRESS_LEVEL_LOW)&&(lsespu1BrkAppSenInvalid==0))
      #else
        ||(mpress<S16_HOB_EXIT_MPRESS_LEVEL_LOW)
      #endif		
      #if (__BRK_SIG_MPS_TO_PEDAL_SEN == 1)	  
        ||((ABS_fz == 1)&&(lsesps16EstBrkPressByBrkPedalF>=S16_HOB_EXIT_MPRESS_LEVEL_HIGH)&&(lsespu1BrkAppSenInvalid==0))
      #else
        ||((ABS_fz == 1)&&(mpress>=S16_HOB_EXIT_MPRESS_LEVEL_HIGH))
      #endif  
        ||(lcu8HobOtherCtrlCheck > 0))
        {
    	    lcu1HobOnFlag = 0;
    	}
    	else{}
    		
    }
}


void LCMSC_vSetHOBTargetVoltage(void)
{	
	if (lcu1HobOnFlag==1)
    {
        lcu1HobMotorOnFlag=1;

	    if (lu8HobOnCounter<U8_HOB_MSC_INIT_FULL_ON_TIME)     
	    {
	        lcs16HobMscTargetV = MSC_14_V;
	    }
	    else
	    {
	        lcs16HobMscTargetV = S16_HOB_MSC_TARGET_V;
	    }
    }
    else
    {
        lcs16HobMscTargetV = MSC_0_V;
        lcu1HobMotorOnFlag=0;
    }    
}


void LAHOB_vCallActHW(void)
{
	if (lcu1HobOnFlag==1)
    {
      #if !__SPLIT_TYPE
        TCL_DEMAND_fl=1; 
        TCL_DEMAND_fr=1;
      #else
        TCL_DEMAND_PRIMARY=1; 
        TCL_DEMAND_SECONDARY=1;
      #endif

        if(ABS_fz == 1)
        { 
	      #if !__SPLIT_TYPE
	        S_VALVE_LEFT=0; 
	        S_VALVE_RIGHT=0;
          #else
	        S_VALVE_PRIMARY=0; 
	        S_VALVE_SECONDARY=0;
          #endif
        }
        else
        {
          #if !__SPLIT_TYPE
	        S_VALVE_LEFT=1; 
	        S_VALVE_RIGHT=1;
          #else
	        S_VALVE_PRIMARY=1; 
	        S_VALVE_SECONDARY=1;
          #endif
        }  
    }
    else
    {
      #if !__SPLIT_TYPE
        TCL_DEMAND_fl=0; 
        TCL_DEMAND_fr=0;
        S_VALVE_LEFT=0; 
	    S_VALVE_RIGHT=0;
      #else
        TCL_DEMAND_PRIMARY=0; 
        TCL_DEMAND_SECONDARY=0;
        S_VALVE_PRIMARY=0; 
	    S_VALVE_SECONDARY=0;
      #endif
    }    
}

#endif /* #if __HOB */

#endif /* #if __VDC */

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCPBACallControl
	#include "Mdyn_autosar.h"
#endif
