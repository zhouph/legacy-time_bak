#ifndef __LSABS_EST_LONG_G_OFFSET_H__
#define __LSABS_EST_LONG_G_OFFSET_H__


/* Includes ************************************************************/
#include "LVarHead.h"



/* Defines ************************************************************/ 

#if ( (__Ax_OFFSET_MON==1) || (__Ax_OFFSET_MON_DRV==1 ) )

#define  AxOffset_0_20G        200        /* 0.2g */
#define  AxOffset_0_15G        150
#define  AxOffset_0_10G        100    
#define  AxOffset_0_08G        80
#define  AxOffset_0_05G        50
#define  AxOffset_0_04G        40
#define  AxOffset_0_03G        30
#define  AxOffset_0_02G        20
#define  AxOffset_0_01G        10 
#define  AxOffset_MAX_LM       200

#define lsabsu1FreeRollFlag            			  G_OFFSET_0.bit7    
#define lsabsu1AxOfsEEPreadEnd     			      G_OFFSET_0.bit6
#define lsespu1WiperActFlg                	      G_OFFSET_0.bit5  /* Long_G_write_eeprom_end_flg */  /* 20110825_kjs */
#define lsabsu1AxOfsEEPreadEnd1   	    		  G_OFFSET_0.bit4
#define lsabsu1AxDrvOfsEEPwriteReq   			    G_OFFSET_0.bit3
#define lsabsu1AxMTPAfterFlg           			  G_OFFSET_0.bit2  /* SWD11_KJS */
#define lsabsu1AxOfsSportModeFlg			        G_OFFSET_0.bit1  /* 20110615_KJS */
#define lsabsu1AxOfsWiperOnFlg            		  G_OFFSET_0.bit0  /* 20110825_kjs */

#define lsabsu1AxOfsAccAftFlag        			  G_OFFSET_1.bit7
#define lsabsu1AxOfsWheelSlipFlg       			  G_OFFSET_1.bit6
#define lsabsu1AxOfsCalcInhibit  				      G_OFFSET_1.bit5
#define lsabsu1AxOfsFreeRollKineFlg    			  G_OFFSET_1.bit4
#define AxOffsetUnusedG1_3              		  G_OFFSET_1.bit3  /* 20110916_kjs */
#define lsabsu1AxBrakeAfter         			    G_OFFSET_1.bit2
#define lsabsu1AxEngineDrag         			    G_OFFSET_1.bit1
#define lsabsu1AxDyOffsetFlat       			    G_OFFSET_1.bit0

#define lsabsu1AxKineOffsetFlat      			    G_OFFSET_2.bit7
#define lsabsu1AxOffsetSuspect                G_OFFSET_2.bit6
#define lsabsu1AxOffsetEEPROMwriteReq			    G_OFFSET_2.bit5
#define lsabsu1AxOffsetEEPROMreadWell         G_OFFSET_2.bit4
#define lsabsu1AxOffsetStand5secOK  	        G_OFFSET_2.bit3
#define lsabsu1AxOffsetStand5sFirstOK        	G_OFFSET_2.bit2
#define lsabsu1AxOffsetStnd5s5cntFOK         	G_OFFSET_2.bit1
#define lsabsu1AxOffsetStandStillOK 			    G_OFFSET_2.bit0

#define lsabsu1AxOffsetStandStillSus			    G_OFFSET_3.bit7
#define lsabsu1AxOfsEepCalEndFlg              G_OFFSET_3.bit6
#define lsabsu1AxOfsEepCalOkFlg    			  	  G_OFFSET_3.bit5
#define lsabsu1AxOfsReadEepSpecInFlg          G_OFFSET_3.bit4
#define lsabsu1AxDriveModeDrag        	      G_OFFSET_3.bit3
#define lsabsu1AxOfsNone                    	G_OFFSET_3.bit2
#define lsabsu1AxOffsetEEPROMreadSpecIn      	G_OFFSET_3.bit1
#define lsabsu1AxOffsetEEPROMreadSusp			    G_OFFSET_3.bit0

#define lsabsu1AxOffsetEEPROMwriteCond1       G_OFFSET_4.bit7
#define lsabsu1AxOffsetEEPROMwriteCond2       G_OFFSET_4.bit6
#define lsabsu1AxOffsetEEPROMwriteCond3       G_OFFSET_4.bit5
#define lsabsu1AxOffsetEEPLimitOK             G_OFFSET_4.bit4
#define lsabsu1AxOffsetEEPMaxMinOK            G_OFFSET_4.bit3
#define lsabsu1AxOfsEepCalcStart            	G_OFFSET_4.bit2
#define lsabsu1AxOfsEepReCalcOK               G_OFFSET_4.bit1
#define AxOffsetUnusedG4_0            			  G_OFFSET_4.bit0


struct LongG_offset{
     	
       int16_t    lsabss16Ax1SecOfs[10];
       int16_t    lsabss16Ax10SecOfs[10];    
       int16_t    lsabss16Ax1secOffMAX;
       int16_t    lsabss16Ax1secOffMIN;
       int16_t    lsabss16Ax10secOffMean;
       int16_t    lsabss16Ax100secOffMean;
       uint8_t    lsabsu8Ax1secOffsetCnt;
       uint8_t    lsabsu8Ax10secOffsetCnt;

       int16_t    lsabss16Ax10secOffSum;
       int16_t    lsabss16Ax100secOffSum;

       uint16_t   lsabsu1Ax10secOffOK    :1;	
       uint16_t   lsabsu1Ax1secFirstOK   :1;
       uint16_t   lsabsu1Ax100secOffOK   :1;       	
       	
       uint16_t   lsabsu1Ax10secFirstOK  :1;
       uint16_t   lsabsu1Ax100secFirstOK :1;
       uint16_t   lsabsu1Ax10secOffSusp  :1;
       uint16_t   AxOffsetUnused4        :1;
       uint16_t   AxOffsetUnused3        :1;
       uint16_t   AxOffsetUnused2        :1;	
       uint16_t   AxOffsetUnused1        :1;
       uint16_t   AxOffsetUnused0        :1; 	
       	       	       
	}; 
extern struct  LongG_offset	*ALGoff, ALGoff1, ALGoff2;

/*Global Type Declaration **********************************************/

extern  struct	U8_BIT_STRUCT_t G_OFFSET_0; 
extern  struct	U8_BIT_STRUCT_t G_OFFSET_1; 
extern  struct	U8_BIT_STRUCT_t G_OFFSET_2; 
extern  struct	U8_BIT_STRUCT_t G_OFFSET_3; 
extern  struct	U8_BIT_STRUCT_t G_OFFSET_4; 

extern uint16_t		lcabsu16FuncStartTimeAx;
extern uint16_t		lcabsu16FuncCycleTimeAx;

  /* Global For Other's Use */
extern uint16_t		lsabsu16AxOffCNT;
extern int16_t		lsabss16AxOfsDyModelRaw;	 
extern int32_t   lsabss32AxOfsDyModelRawSum;
	
extern uint8_t  lsabsu8AccelAftCnt;

extern int16_t    lsabss16AxSenAirSus;
extern int16_t    lsabss16AxOffsetf;
extern int16_t    lsabss16AxAeroResist;
extern uint8_t  lsabsu8AxRollResist;
extern int16_t    lsabss16AxEngModelResist;
extern int16_t    lsabss16AxBrakeModelResist;

#if (__KINE_MODEL_AX_OFFSET ==ENABLE)  
extern int16_t      lsabss16AxOfsKModelRaw;
extern int32_t      lsabss32AxOfsKModelRawSum;
extern uint16_t	    lsabsu16AxOffKineCNT;
#endif

extern int16_t    lsabss16AxOffsetDrvf;
extern int16_t    lsabss16AxOffsetTOTf;

#if (__STANDSTATE_MODEL_AX_OFFSET ==ENABLE)
  /* Stand Still */
extern uint16_t   lsabsu16AxStandCnt;
extern int32_t   lsabss32AxStandSum;  
extern int16_t    lsabss16AxOffset5secStand;  
extern int16_t    lsabss16AxOffsetStand[5];
extern uint8_t  lsabsu8AxOffset5secStandCnt;
extern int16_t    lsabss16AxOffsetStandMax;
extern int16_t    lsabss16AxOffsetStandMin;
extern int16_t    lsabss16AxOffset5sec5cntMean;
#endif 

/* SWD11_KJS */
extern int16_t    lsabss16AxDynModel;

  /* Vehicle First EEPROM */
extern int16_t    lsabss16AxOfsFirstCal;
extern int16_t    lsabss16AxOfsFirstCalMax;
extern int16_t    lsabss16AxOfsFirstCalMin;
extern uint8_t  lsabsu8AxOfsFirstCalCnt;
extern int32_t   lsabss32AxOfsSumFirstCal;

  /*  EEPROM */
extern int16_t    lsabss16AxOffsetEEPwriteTempo;

  /* Global For Logging */
extern int16_t    lsabss16AxOfsEEPread;
extern uint8_t  lsabsu8AxOfsEEPMaxMinLMOverCnt;  
extern uint8_t  lsabsu8AxOfsDelyCnt;

extern uint8_t      u8DataCNTAxOffset;  /* For Data Logging */

extern int16_t      lsabss16AxEngTorqByGearState;

extern int16_t      lsabss16AxOfsWiperOnCNT; /* Wiper Activation */


/* Global Function Declaration *****************************************/
extern void  LSABS_vEstLongAccSensorOffsetMon(void);

#if __VDC 
extern void  LSABS_vReadEEPROMLongGSensorOffset(void);
extern void  LSABS_vWriteEEPROMLongGSensorOffset(void);
#endif

#endif
#endif
