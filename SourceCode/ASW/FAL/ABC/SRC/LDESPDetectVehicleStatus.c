
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDESPDetectVehicleStatus
	#include "Mdyn_autosar.h"
#endif

#include "LDESPDetectVehicleStatus.h"
#include "LAABSCallMotorSpeedControl.h"
#include "LDESPEstBeta.h"
#include "hm_logic_var.h"
#include "LCHDCCallControl.h"
#include "LCDECCallControl.h"
#include "LCACCCallControl.h"
#include "LCHSACallControl.h"
#include "LCESPCalInterpolation.h"
#include "LCESPCalLpf.h"
#include "LCESPCallControl.h"
#include "LCallMain.h"
#include "LSABSCallSensorSignal.h"
#include "LDABSCallDctForVref.h"
#include "LCESPInterfaceSlipController.h"
#include "LDESPCalVehicleModel.h"
#include "LSESPCalSlipRatio.h"

#include "LDROPCallDetection.h"
#include "LDABSCallEstVehDecel.h"

#if __TVBB
#include "LCTVBBCallControl.h"
#endif

#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
#include "LSESPFilterEspSensor.h"
#include "LSESPCalSensorOffset.h"
#endif
#if __CDM || __SCC
#include "SCC/LCSCCCallControl.h"
//#include "../CAN/CCBaseHead.h"
#endif
#if ((__CRAM==1)&&(__EUROPE_ADAC == 1))	 
	struct	U8_BIT_STRUCT_t ADACSTATE_0;
	struct	U8_BIT_STRUCT_t ADACSTATE_1;
int16_t	moment_pre_front_adac_preact;
int16_t	adac_ttp_cnt_fl;
int16_t	adac_ttp_cnt_fr;
int16_t	time_to_adac_preact;
#endif

#if __CAR==GM_TAHOE || __CAR ==GM_SILVERADO
/********************Active Booster************************/
 /********** The enter condition between Del_M and YAW_ERROR_DOT is "or".
    Therefor you should decrease the enter condition of YAW_ERROR_DOT also.*********/
	#define S16_ENTER_DELM_HMU_HSP_ACT_BOST 1500	 /* del_m enter condition*/
	#define S16_ENTER_DELM_HMU_MSP_ACT_BOST 2000	 /* del_m enter condition*/
	#define S16_ENTER_DELM_HMU_LSP_ACT_BOST 2500	 /* del_m enter condition*/

	#define S16_ENTER_DELM_MMU_HSP_ACT_BOST 3500	 /* del_m enter condition*/
	#define S16_ENTER_DELM_MMU_MSP_ACT_BOST 3500 	 /* del_m enter condition*/
	#define S16_ENTER_DELM_MMU_LSP_ACT_BOST 4500	 /* del_m enter condition*/

	#define S16_ENTER_DelYD_HMU_HSP_ACT_BOST 100	 /*yaw errordot enter condition*/
	#define S16_ENTER_DelYD_HMU_MSP_ACT_BOST 100	 /*yaw errordot enter condition*/
	#define S16_ENTER_DelYD_HMU_LSP_ACT_BOST 100	 /*yaw errordot enter condition*/

	#define S16_ENTER_DelYD_MMU_HSP_ACT_BOST 200	 /*yaw errordot enter condition*/
	#define S16_ENTER_DelYD_MMU_MSP_ACT_BOST 200 	 /*yaw errordot enter condition*/
	#define S16_ENTER_DelYD_MMU_LSP_ACT_BOST 200	 /*yaw errordot enter condition*/

/***********************************************************/
#elif __CAR==GM_C100
/********************Active Booster************************/

	#define S16_ENTER_DELM_HMU_HSP_ACT_BOST 1000	 /* del_m enter condition*/
	#define S16_ENTER_DELM_HMU_MSP_ACT_BOST 1500	 /* del_m enter condition*/
	#define S16_ENTER_DELM_HMU_LSP_ACT_BOST 2000	 /* del_m enter condition*/

	#define S16_ENTER_DELM_MMU_HSP_ACT_BOST 2000	 /* del_m enter condition*/
	#define S16_ENTER_DELM_MMU_MSP_ACT_BOST 3000 	 /* del_m enter condition*/
	#define S16_ENTER_DELM_MMU_LSP_ACT_BOST 3000	 /* del_m enter condition*/

	#define S16_ENTER_DelYD_HMU_HSP_ACT_BOST 100	 /*yaw errordot enter condition*/
	#define S16_ENTER_DelYD_HMU_MSP_ACT_BOST 100	 /*yaw errordot enter condition*/
	#define S16_ENTER_DelYD_HMU_LSP_ACT_BOST 100	 /*yaw errordot enter condition*/

	#define S16_ENTER_DelYD_MMU_HSP_ACT_BOST 100	 /*yaw errordot enter condition*/
	#define S16_ENTER_DelYD_MMU_MSP_ACT_BOST 120 	 /*yaw errordot enter condition*/
	#define S16_ENTER_DelYD_MMU_LSP_ACT_BOST 120	 /*yaw errordot enter condition*/


	#define S16_PRE_FILLING_OTH_ENTER      0
/***********************************************************/
#else
/********************Active Booster************************/
	#define S16_ENTER_DELM_HMU_HSP_ACT_BOST 1000	 /* del_m enter condition*/
	#define S16_ENTER_DELM_HMU_MSP_ACT_BOST 1500	 /* del_m enter condition*/
	#define S16_ENTER_DELM_HMU_LSP_ACT_BOST 2000	 /* del_m enter condition*/

	#define S16_ENTER_DELM_MMU_HSP_ACT_BOST 2000	 /* del_m enter condition*/
	#define S16_ENTER_DELM_MMU_MSP_ACT_BOST 3000 	 /* del_m enter condition*/
	#define S16_ENTER_DELM_MMU_LSP_ACT_BOST 3000	 /* del_m enter condition*/

	#define S16_ENTER_DelYD_HMU_HSP_ACT_BOST 100	 /*yaw errordot enter condition*/
	#define S16_ENTER_DelYD_HMU_MSP_ACT_BOST 100	 /*yaw errordot enter condition*/
	#define S16_ENTER_DelYD_HMU_LSP_ACT_BOST 100	 /*yaw errordot enter condition*/

	#define S16_ENTER_DelYD_MMU_HSP_ACT_BOST 100	 /*yaw errordot enter condition*/
	#define S16_ENTER_DelYD_MMU_MSP_ACT_BOST 120 	 /*yaw errordot enter condition*/
	#define S16_ENTER_DelYD_MMU_LSP_ACT_BOST 120	 /*yaw errordot enter condition*/

	#define S16_PRE_FILLING_OTH_ENTER 0
/***********************************************************/

#endif

int16_t del_target_2wheel_slip;
int16_t cal_target_2wheel;

int16_t ldesps16_gsuv_os_us_sig, ldesps16_gsuv_os_us_sig_old ;    /* GSUV OS_US Signal */

int16_t lcs16US2WH_FrtDelM;
uint8_t lcu8US2WH_count;
int16_t lcs16US2WH_FadeCount;

int16_t pre_fill_rear_del_yaw_con;
int16_t pre_fill_rear_del_yaw_con_limit;
	
int16_t pre_rear_high_speed;
int16_t pre_rear_med1_speed;
int16_t pre_rear_med2_speed;
int16_t pre_rear_low_speed;
	
int16_t ay_vx_sig, ay_vx_sig_old, ay_vx_dot_sig_minus, ay_vx_dot_sig_plus, ay_vx_dot_sig_fltr, ay_vx_dot_sig_fltr2 ; 

int16_t act_pre_esc_hmu_enter;
int16_t act_pre_esc_mmu_enter;
int16_t active_bost_enter_hmu;
int16_t active_bost_enter_mmu;
int16_t alat_rate[3];
int16_t	limt_enter_wstrd_slow;
uint8_t esc_debugger;

int16_t s16_yaw_accel_fb_gain ; 

/******************************************/
int16_t pre_act_valve_stop;

int16_t pre_act_action_fl_count;
int16_t pre_act_action_fl_count_on;
int16_t pre_act_action_fl_count_off;

int16_t pre_act_action_fr_count;
int16_t pre_act_action_fr_count_on;
int16_t pre_act_action_fr_count_off;

int16_t pre_act_action_rl_count;
int16_t pre_act_action_rl_count_on;
int16_t pre_act_action_rl_count_off;

int16_t pre_act_action_rr_count;
int16_t pre_act_action_rr_count_on;
int16_t pre_act_action_rr_count_off;

int16_t ExtUnder_Req_Decel;
int16_t ExtUnder_Req_Press;
int16_t Extr_Under_Frt_Moment_In; 
int16_t Extr_Under_Frt_Moment_Out;
int16_t Extr_Under_Frt_Moment_FL;
int16_t Extr_Under_Frt_Moment_FR;
int8_t  ext_under_on_counter ; 
int16_t Extr_Under_Frt_fr_Press_1st ;
int16_t Extr_Under_Frt_fl_Press_1st ;

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//int16_t Extr_Under_Frt_fl_Press_Max ;
//int16_t Extr_Under_Frt_fr_Press_Max ;
#else
int16_t Extr_Under_Frt_fl_Press_Max ;
int16_t Extr_Under_Frt_fr_Press_Max ;
#endif


  #if (__YAW_M_OVERSHOOT_DETECTION == 1)          
uint16_t    ldespu16DelYawStableCnt;
int16_t     ldesps16MulevelOverVxThreshold;
  #endif

/*    For CRAM   */
int16_t act_1st_cy_rl_cnt ;
int16_t act_1st_cy_rr_cnt ;
	#if (__CRAM == 1)
int16_t lds16espDelVcram;
int16_t lds16espVelcram;
int16_t lds16espVelcram_old ;  
int16_t lcs16espCramCount;

		#if (__CRAM_PRE_FRONT_W_CRTL == 1) 
	int8_t time_to_cram_preact;
	int8_t cram_ttp_cnt_fl;
	int8_t cram_ttp_cnt_fr;
	int16_t moment_pre_front_cram_preact;
		#endif
	#endif
  
/**************** Decel Brake Lamp (A430)**************/
#if (GMLAN_ENABLE==ENABLE)
int8_t	lsespu8DecelConfirmCounter;
#endif
/******************************************************/

/******************************************/

  
  #if (__YAW_M_OVERSHOOT_DETECTION == 1)
U8_BIT_STRUCT_t ESCSTATE_2;
  #endif

void LDESP_Detect2WheelUnderCtrl(void);
    
#if (__CRAM == 1)
struct	U8_BIT_STRUCT_t ESCSTATE_0;
struct	U8_BIT_STRUCT_t ESCSTATE_1;

#define ldu1espAyStartCRAM_FL				ESCSTATE_1.bit7
#define ldu1espAyStartCRAM_FR				ESCSTATE_1.bit6
#define ldu1espWstrStartCRAM_FL			ESCSTATE_1.bit5
#define ldu1espWstrStartCRAM_FR			ESCSTATE_1.bit4
#define ESC_STATUS_not_using_1_3		ESCSTATE_1.bit3
#define ESC_STATUS_not_using_1_2		ESCSTATE_1.bit2
#define ESC_STATUS_not_using_1_1		ESCSTATE_1.bit1
#define ESC_STATUS_not_using_1_0		ESCSTATE_1.bit0

void LDESP_vDetectCRAM(void);

#if (__CRAM_PRE_FRONT_W_CRTL == 1) 
void LDESP_vCheckPreCRAM(void);
#endif
#endif


int16_t lcesps16Over2wheelEnterMoment;
int16_t lcesps16Over2wheelExitMoment;

/******************************************/


#if	__VDC
void LDESP_Detect2WheelOverCtrl(void);
void LDESP_vRecognizeVehicleState(void);
void LDESP_CompBetaDiff(void);
#if MGH60_NEW_TARGET_SLIP == 0
void LDESP_DetectDriftForSlip(void);
#endif
void LDESP_CompDetBetaDotForSlip(void);
void LDESP_DetectBetaUnderDrift(void);
void LCESP_vSetRearspeedDepMUPreFill(void);

   #if __SIDE_SLIP_ANGLE_MODULE
void LDESP_CompSideSlipAngle(void);
   #endif
void DETECT_ON_CENTER(void);

void LDESP_DetectRevSteer(void);
void DETECT_SLALOM(void);
/*void DETECT_BIG_OVERSTEER(void); */
/*void DETECT_FISH_HOOK_MODE(void);*/
void DETECT_SAME_PHASE(void);

void LDESP_vCompDeltaYaw(void);
void LDESP_vCompDetDeltaYaw(void);
void LSESP_vCheckEspError(void);
/*#if __PARTIAL_PERFORMANCE
extern void LCESPPartial_vCalYawC(void);
#endif*/

void LDESP_vCalVehicleModel(void);
void LDESP_vDet1stCycle(void);

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
void LDESP_vSet1stCycleF(struct ESC_STRUCT *WL_ESC);
void LDESP_vSet1stCycleR(struct ESC_STRUCT *WL_ESC);
void LDESP_vDet1stCycleWheelF(struct ESC_STRUCT *WL_ESC);
void LDESP_vDet1stCycleWheelR(struct ESC_STRUCT *WL_ESC);
#else
void LDESP_vDet1stCycleWheel(void);
#endif

void LDESP_vActPreAction(void);
void LCESP_vCalSpeedDepPreFillRear(void);
void LDESP_vDetectCombControl(void);

#if	( __Ext_Under_Ctrl == 1 )
void LDESP_DetectExtemeUnderCtrl(void);
#endif

#if defined(__ADDON_TCMF)
void LDESP_vSetActiveBooster(void);
#endif

/**************** Decel Brake Lamp (A430)**************/
#if (GMLAN_ENABLE==ENABLE)
void LDESP_vSetDecelBrakeLamp(void);
#endif
/******************************************************/

#if (__ESC_CYCLE_DETECTION)
void LDESP_vDetESCCycle(void);

uint8_t  ldespu8ESCCycle;
uint8_t  ldespu8ESCCycleOld;
uint8_t  ldespu8ESCControlWheel;
uint8_t  ldespu8ESCControlWheelOld;
uint8_t  ldespu8ESCControlWheel2ndOld;
int16_t  lcesps16ESCCycleCount;
//uint8_t  ldespu8CycleDetQualifiedCnt;
#endif

#if (__ESC_CYCLE_DETECTION)
void LDESP_vDetESCCycle(void)
{	  
	  ldespu8ESCCycleOld = ldespu8ESCCycle;
	  ldespu8ESCControlWheel2ndOld = ldespu8ESCControlWheelOld; 
	  
	  if((SLIP_CONTROL_BRAKE_FL ==1)||(SLIP_CONTROL_BRAKE_FR ==1))
	  {
	  	  ;
	  }
	  else
	  {	  	  
	  	  lcesps16ESCCycleCount = lcesps16ESCCycleCount - 1;

        if (lcesps16ESCCycleCount < 0)
        {  
            lcesps16ESCCycleCount = 0 ;
        }
        else
        {
        	  ;
        }
	  }
/***********************************************************************/
    /*
    if((SLIP_CONTROL_BRAKE_FL ==1)||(SLIP_CONTROL_BRAKE_FR ==1))
    {
        if (ldespu8CycleDetQualifiedCnt < 10)
        {
        	  ldespu8CycleDetQualifiedCnt = ldespu8CycleDetQualifiedCnt + 1;
        	  ldespu1CycleDetQualified = 0;
        }
        else
        {
        	  ldespu8CycleDetQualifiedCnt = 10;
            ldespu1CycleDetQualified = 1;
        }
    }
    else
    {
        ldespu1CycleDetQualified = 0;
        ldespu8CycleDetQualifiedCnt = 0;
    }	  
	  */
	  
/***********************************************************************/
	  
	  if((SLIP_CONTROL_BRAKE_FL ==1)||(SLIP_CONTROL_BRAKE_FR ==1)) /*in front wheel control*/
	  {
	  	  if((SLIP_CONTROL_BRAKE_FL ==1)&&(SLIP_CONTROL_BRAKE_FR ==1))
	  	  {
	  	  	  ldespu8ESCControlWheel = 3;
	  	  }
	  	  else if((SLIP_CONTROL_BRAKE_FL ==1)&&(SLIP_CONTROL_BRAKE_FR ==0))
	      {
	      	  ldespu8ESCControlWheel = 1;
	      }
	      else if((SLIP_CONTROL_BRAKE_FL ==0)&&(SLIP_CONTROL_BRAKE_FR ==1))
	      {
	      	  ldespu8ESCControlWheel = 2;
	      }
	      else
	      {
	      	  ;
	      }
	      
	      ldespu8ESCControlWheelOld = 0;
	  }
	  else if((SLIP_CONTROL_BRAKE_FL ==0)&&(SLIP_CONTROL_BRAKE_FR ==0)&&(lcesps16ESCCycleCount >0)) /*in non-front wheel control*/
	  {                                                      
	  	  ldespu8ESCControlWheelOld = ldespu8ESCControlWheel;
	  }
	  else
	  {
	  	  ldespu8ESCControlWheel = 0;
	  	  ldespu8ESCControlWheelOld = 0;
	  }
	  

/**********************************************************************/
	  	
	  if(
	  	  ((SLIP_CONTROL_BRAKE_FL ==1)&&(SLIP_CONTROL_BRAKE_FR ==1))
	  	||((SLIP_CONTROL_BRAKE_FL_OLD ==1)&&(SLIP_CONTROL_BRAKE_FR ==1))
	  	||((SLIP_CONTROL_BRAKE_FR_OLD ==1)&&(SLIP_CONTROL_BRAKE_FL ==1))
	  	)
	  {
	  	  ldespu1ESC1stCycle = 0;
	  	  ldespu1ESC2ndCycle = 1;
	  	  lcesps16ESCCycleCount = S16_DETECT_ESC_CYCLE;
	  }
	  else
	  {
	  	  if((SLIP_CONTROL_BRAKE_FL ==1)||(SLIP_CONTROL_BRAKE_FR ==1))
	  	  {	  	  	  	  	  	  	  	  	
	  	  	  if((lcesps16ESCCycleCount>0)&&(ldespu1ESC1stCycle ==0)) /*second cycle &&(ldespu1ESC1stCycle ==0)*/
	  	  	  {	  	  	  	  
	  	  	  	  if(ldespu8ESCControlWheel==ldespu8ESCControlWheel2ndOld)
	  	  	  	  {	  	  	  	  	
	  	  	  	  	  if(ldespu8ESCCycleOld ==1)
	  	  	  	  	  {
	  	  	  	  	  	  ldespu1ESC1stCycle = 1;
	  	                  ldespu1ESC2ndCycle = 0;
	  	                  lcesps16ESCCycleCount = S16_DETECT_ESC_CYCLE;
	  	  	  	  	  }
	  	  	  	  	  else if(ldespu8ESCCycleOld ==2)
	  	  	  	  	  {
	  	  	  	  	  	  ldespu1ESC1stCycle = 0;
	  	                  ldespu1ESC2ndCycle = 1;
	  	                  lcesps16ESCCycleCount = S16_DETECT_ESC_CYCLE;
	  	  	  	  	  }
	  	  	  	  	  else
	  	  	  	  	  {
	  	  	  	  	  	  ;
	  	  	  	  	  }	
	  	  	  	  }
	  	  	  	  else
	  	  	  	  {
	  	  	  	  
	  	  	  	      ldespu1ESC1stCycle = 0;
	  	              ldespu1ESC2ndCycle = 1;
	  	              lcesps16ESCCycleCount = S16_DETECT_ESC_CYCLE;	  	  	  	  	  
	  	  	  	  }	 	  	  	  
	  	  	  }
	  	  	  else                                          
	  	  	  {
	  	  	  	  /*if(ldespu1CycleDetQualified ==1)          //first cycle
	  	  	  	  	{*/
	  	  	  	  	  ldespu1ESC1stCycle = 1;
	  	              ldespu1ESC2ndCycle = 0;
	  	              lcesps16ESCCycleCount = S16_DETECT_ESC_CYCLE;
	  	  	  	  /*//}
	  	  	  	  //else                                      //useless cycle
	  	  	  	  //{
	  	  	  	  //	  ldespu1ESC1stCycle = 0;
	  	          //    ldespu1ESC2ndCycle = 0;
	  	          //    lcesps16ESCCycleCount = 0;
	  	  	  	  //}*/
	  	  	  }
	  	  }
	  	  else
	  	  {  	  	
	  	  	  ldespu1ESC1stCycle = 0;
	  	      ldespu1ESC2ndCycle = 0;
	  	  	  /*
	  	  	  lcesps16ESCCycleCount = lcesps16ESCCycleCount - 1;

            if (lcesps16ESCCycleCount < 0)
            {  
                lcesps16ESCCycleCount = 0 ;
            }
            else
            {
            	  ;
            }
            */
            
	  	  }
	  }
	  
	  if(ldespu1ESC1stCycle ==1)
	  {
	  	  ldespu8ESCCycle = 1;
	  }
	  else if(ldespu1ESC2ndCycle ==1)
	  {
	  	  ldespu8ESCCycle = 2;
	  }
	  else
	  {
	  	  ;
	  }	  	  
}

#endif


void LDESP_Detect2WheelOverCtrl(void)
{
/* Inhibition */	
	#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
//	if((VDC_DISABLE_FLG==1)||(lsespu1AHBGEN3MpresBrkOn==1)||(EXTREME_UNDER_CONTROL==1))
	ldu1OVER2WHInhibitFlag = 1;
	#else
	if((VDC_DISABLE_FLG==1)||(MPRESS_BRAKE_ON==1)||(EXTREME_UNDER_CONTROL==1))

    {
        ldu1OVER2WHInhibitFlag = 1;
    }
    else
    {
    	ldu1OVER2WHInhibitFlag = 0;
	} 
	#endif	
	  if(SLIP_CONTROL_BRAKE_FL == 1)
    {       
    	  /*****************2wheel *********************/
#if __MOMENT_TO_TARGET_SLIP_NEW 

        if(SLIP_CONTROL_BRAKE_FR_OLD ==1)
        {
        		FLAG_TWO_WHEEL = 0;
        		FLAG_TWO_WHEEL_RR = 0;
        }
        else
        {
        		;
        }
        		
        esp_tempW7 = McrAbs(delta_moment_q);
        
        esp_tempW0 = LCESP_s16FindRoadDependatGain(esp_mu, (int16_t)U8_OS_2_WHEEL_QM_ENT_H
                                                         , (int16_t)U8_OS_2_WHEEL_QM_ENT_M
                                                         , (int16_t)U8_OS_2_WHEEL_QM_ENT_L);        
        
        esp_tempW1 = LCESP_s16FindRoadDependatGain(esp_mu, (int16_t)U8_OS_2_WHEEL_QM_EXIT_H
                                                         , (int16_t)U8_OS_2_WHEEL_QM_EXIT_M
                                                         , (int16_t)U8_OS_2_WHEEL_QM_EXIT_L);
        
        esp_tempW2 = (int16_t)U8_OS_2_WHEEL_QM_ENT_ROP_H;        
        esp_tempW3 = (int16_t)U8_OS_2_WHEEL_QM_EXIT_ROP_H;
        
        #if(__ESC_CYCLE_DETECTION)
        esp_tempW4 = LCESP_s16FindRoadDependatGain(esp_mu, (int16_t)U8_OS_2_WHEEL_QM_ENT_2ND_H
                                                         , (int16_t)U8_OS_2_WHEEL_QM_ENT_2ND_M
                                                         , (int16_t)U8_OS_2_WHEEL_QM_ENT_2ND_L);        
        
        esp_tempW5 = LCESP_s16FindRoadDependatGain(esp_mu, (int16_t)U8_OS_2_WHEEL_QM_EXIT_2ND_H
                                                         , (int16_t)U8_OS_2_WHEEL_QM_EXIT_2ND_M
                                                         , (int16_t)U8_OS_2_WHEEL_QM_EXIT_2ND_L);
        #endif
        if ( ROP_REQ_ACT==1)
        {
        	  lcesps16Over2wheelEnterMoment = esp_tempW7 - (esp_tempW2 * 100); //Enter Condition_ROP
            lcesps16Over2wheelExitMoment = esp_tempW7 - (esp_tempW3 * 100); //Exit Condition_ROP        		
        }
        else
        {
        	  #if(__ESC_CYCLE_DETECTION)
        	  if(ldespu1ESC1stCycle == 1)
            {
        	      lcesps16Over2wheelEnterMoment = esp_tempW7 - (int16_t)(((int32_t)esp_tempW0) * 100);
        	      lcesps16Over2wheelExitMoment = esp_tempW7 - (int16_t)(((int32_t)esp_tempW1) * 100);
            }
            else if(ldespu1ESC2ndCycle == 1)
            {
        	      lcesps16Over2wheelEnterMoment = esp_tempW7 - (int16_t)(((int32_t)esp_tempW4) * 100);
        	      lcesps16Over2wheelExitMoment = esp_tempW7 - (int16_t)(((int32_t)esp_tempW5) * 100);
            }
            else
            {
        	      ;
            }
        	  #else
        	  lcesps16Over2wheelEnterMoment = esp_tempW7 - (int16_t)(((int32_t)esp_tempW0) * 100); //Enter Condition_Normal
            lcesps16Over2wheelExitMoment = esp_tempW7 - (int16_t)(((int32_t)esp_tempW1) * 100); //Exit Condition_Normal     
        	  #endif     
        }
        		    
        
        /* High-speed ( H_mu, M_mu, L_mu )  */                
        esp_tempW1 = LCESP_s16FindRoadDependatGain(esp_mu, S16_TWO_REAR_TARGET_SLIP_WEG_HMU_HSP,
                                                           S16_TWO_REAR_TARGET_SLIP_WEG_MMU_HSP,  
                                                           S16_TWO_REAR_TARGET_SLIP_WEG_LMU_HSP);
                                 
        /* Med-speed ( H_mu, M_mu, L_mu )  */                
        esp_tempW2 = LCESP_s16FindRoadDependatGain(esp_mu, S16_TWO_REAR_TARGET_SLIP_WEG_HMU_MSP,
                                                           S16_TWO_REAR_TARGET_SLIP_WEG_MMU_MSP,  
                                                           S16_TWO_REAR_TARGET_SLIP_WEG_LMU_MSP);
                                                                                                                              
        /* Low-speed ( H_mu, M_mu, L_mu )  */                                                   
        esp_tempW3 = LCESP_s16FindRoadDependatGain(esp_mu, S16_TWO_REAR_TARGET_SLIP_WEG_HMU_LSP,
                                                           S16_TWO_REAR_TARGET_SLIP_WEG_MMU_LSP,  
                                                           S16_TWO_REAR_TARGET_SLIP_WEG_LMU_LSP);    
                                                        
        esp_tempW5 = LCESP_s16IInter3Point( vrefk,  S16_TWO_WHEEL_SPEED_L_V,  esp_tempW3,
                                                    S16_TWO_WHEEL_SPEED_M_V,  esp_tempW2,
                                                    S16_TWO_WHEEL_SPEED_H_V,  esp_tempW1);
        /*
        esp_tempW3 = LCESP_s16FindRoadDependatGain(esp_mu, S16_TWO_HMU_REAR_TAR_SLIP_WEG,
                                                           S16_TWO_MMU_REAR_TAR_SLIP_WEG,  
                                                           S16_TWO_LMU_REAR_TAR_SLIP_WEG);           
        */
        
        
#else 
        cal_target_2wheel = LCESP_s16IInter3Point( target_moment_f, 0, 0, m7, WHEEL_SLIP_20, m9, WHEEL_SLIP_60);

        lcesps16Over2wheelEnterMoment = cal_target_2wheel - cal_target_slip_front;

        esp_tempW0 = LCESP_s16FindRoadDependatGain(esp_mu,WHEEL_SLIP_3 , WHEEL_SLIP_5, WHEEL_SLIP_5);            

#endif  

        esp_tempW2 = LCESP_s16FindRoadDependatGain(esp_mu,S16_2WH_HMU_REAR_SLIP_MAX,
                            S16_2WH_MMU_REAR_SLIP_MAX, S16_2WH_LMU_REAR_SLIP_MAX);

#if (__CRAM == 1)
        if((ldu1espDetectCRAM==1)&&(ldu1espDetectCRAM_FL==0))    // Over-, Outside Rear
        { 
        		
        		esp_tempW4 = (int16_t)( (((int32_t)lcesps16Over2wheelEnterMoment)*esp_tempW5)/100 );
            if(esp_tempW4 <= 0)
            {
                esp_tempW4 = 0;
            }
            else if(esp_tempW4 > esp_tempW2)
            {
                esp_tempW4 = esp_tempW2;
            }
            else
            {
                ;
            }
        		if(moment_pre_rear_cram > esp_tempW4)
        		{
            	del_target_2wheel_slip =  moment_pre_rear_cram ;    
            }
            else
            {
            	del_target_2wheel_slip  = esp_tempW4;
            }
            
        }        
    #if (__Pro_Active_Roll_Ctrl == 1)        
      #if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
        else if((ROP_REQ_ACT == 1)&&(RL_ESC.lcu1ropRearCtrlReq == 1))
      #else
        else if((ROP_REQ_ACT == 1)&&(lcu1ropRearCtrlReqRL == 1))
      #endif
        {        	  
        	  
            del_target_2wheel_slip =  S16_ROP_PROACTIVE_DEL_SLIP_R ;  // Tuning parameter...

            if(del_target_2wheel_slip <= 0)
            {
                del_target_2wheel_slip = 0;
            } 
            else
            {
                ;
            }
            
        }
    #endif                          
			#if(__AHB_GEN3_SYSTEM==ENABLE) || (__IDB_LOGIC == ENABLE)
        else if(
        	     (lsespu1AHBGEN3MpresBrkOn == 0)&&(ABS_fz==0)&&(lcu1US2WHControlFlag==0)		//        else if((ABS_fz==0)
               &&( ((FLAG_TWO_WHEEL == 0)&&(lcesps16Over2wheelEnterMoment > 0))//&&(FL.u8_Estimated_Active_Press> 0))
               ||(FLAG_TWO_WHEEL == 1) )
               )
			#else
        else if(
        	     (MPRESS_BRAKE_ON == 0)&&(ABS_fz==0)&&(lcu1US2WHControlFlag==0)		//        else if((ABS_fz==0)
               &&( ((FLAG_TWO_WHEEL == 0)&&(lcesps16Over2wheelEnterMoment > 0))//&&(FL.u8_Estimated_Active_Press> 0))
               ||(FLAG_TWO_WHEEL == 1) )
               )
			#endif
        {
            del_target_2wheel_slip = (int16_t)( (((int32_t)lcesps16Over2wheelEnterMoment)*esp_tempW5)/100 );

            if(del_target_2wheel_slip <= 0)
            {
                del_target_2wheel_slip = 0;
            }
            else if(del_target_2wheel_slip > esp_tempW2)
            {
                del_target_2wheel_slip = esp_tempW2;
            }
            else
            {
                ;
            }
        }
        else
        {
            del_target_2wheel_slip = 0;
        }
#else //(__CRAM == 0)
	
    #if (__Pro_Active_Roll_Ctrl == 1)        
      #if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
      	#if(__AHB_GEN3_SYSTEM==ENABLE) || (__IDB_LOGIC == ENABLE)
      	if((ROP_REQ_ACT == 1)&&(lsespu1AHBGEN3MpresBrkOn == 0)&&((RL_ESC.lcu1ropRearCtrlReq == 1)||(lcesps16Over2wheelEnterMoment > 0)))
      	#else
      	if((ROP_REQ_ACT == 1)&&(MPRESS_BRAKE_ON == 0)&&((RL_ESC.lcu1ropRearCtrlReq == 1)||(lcesps16Over2wheelEnterMoment > 0)))
      	#endif      	
      #else
      	#if(__AHB_GEN3_SYSTEM==ENABLE) || (__IDB_LOGIC == ENABLE)
      	if((ROP_REQ_ACT == 1)&&(lsespu1AHBGEN3MpresBrkOn == 0)&&(lcu1ropRearCtrlReqRL == 1))
      	#else
      	if((ROP_REQ_ACT == 1)&&(MPRESS_BRAKE_ON == 0)&&(lcu1ropRearCtrlReqRL == 1))
      	#endif
      #endif
        {

            del_target_2wheel_slip =  S16_ROP_PROACTIVE_DEL_SLIP_R ;  // Tuning parameter...

            if(del_target_2wheel_slip <= 0)
            {
                del_target_2wheel_slip = 0;
            } 
            else
            {
                ;
            }
  
        }
        
		#else        
        if(ROP_REQ_FRT_Pre == 1)
        {
            del_target_2wheel_slip = 0;
        }
    #endif 
			#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
        else if(
        	      (lsespu1AHBGEN3MpresBrkOn == 0)&&(ABS_fz==0)&&(lcu1US2WHControlFlag==0)		//        else if((ABS_fz==0)
                &&( ((FLAG_TWO_WHEEL == 0)&&(lcesps16Over2wheelEnterMoment >0)&&(FL.s16_Estimated_Active_Press>(int16_t)U8_OVER2WHEEL_ENTER_EST_P)) //&&(FL.u8_Estimated_Active_Press> 0)
                 ||(FLAG_TWO_WHEEL == 1) )
               )
			#else
        else if(
        	      (MPRESS_BRAKE_ON == 0)&&(ABS_fz==0)&&(lcu1US2WHControlFlag==0)		//        else if((ABS_fz==0)
                &&( ((FLAG_TWO_WHEEL == 0)&&(lcesps16Over2wheelEnterMoment >0)&&(FL.s16_Estimated_Active_Press>(int16_t)U8_OVER2WHEEL_ENTER_EST_P)) //&&(FL.u8_Estimated_Active_Press> 0)
                 ||(FLAG_TWO_WHEEL == 1) )
               )
		 	#endif
        {

            del_target_2wheel_slip = (int16_t)( (((int32_t)lcesps16Over2wheelEnterMoment)*esp_tempW5)/100 );

            if(del_target_2wheel_slip <= 0)
            {
                del_target_2wheel_slip = 0;
            }
            else if(del_target_2wheel_slip > esp_tempW2)
            {
                del_target_2wheel_slip = esp_tempW2;
            }
            else
            {
                ;
            }
        }
        else
        {
            del_target_2wheel_slip = 0;
        }
#endif      


#if (__CRAM == 1)
		    if(ldu1espDetectCRAM==0)
		    {  
		        if(del_target_2wheel_slip > 0)
		        {
		            FLAG_TWO_WHEEL = 1; 
		            FLAG_TWO_WHEEL_RL = 1;            
		        }
		        else
		        {
		            FLAG_TWO_WHEEL = 0;
		            FLAG_TWO_WHEEL_RL = 0;            
		        }
		    }
		    else
		    {
		        if(del_target_2wheel_slip > 0)
		        {
		            FLAG_TWO_WHEEL = 1;
		            FLAG_TWO_WHEEL_RL = 1;  
		        }
		        else  
		        {
		            FLAG_TWO_WHEEL = 0;
		            FLAG_TWO_WHEEL_RL = 0;  
		            
		            //SLIP_CONTROL_BRAKE_RL = 0;
		        }       
		    }
		        
        if(FLAG_TWO_WHEEL_RL == 1)
        {
            del_target_slip_rl = -del_target_2wheel_slip; 
            
            if(ldu1espDetectCRAM==0)
            {
                del_target_slip_rr = 0;
            }

            //SLIP_CONTROL_BRAKE_RL = 1;
        }
        else
        {
            ;
        }
#else //(__CRAM == 0)
	
    #if (__Pro_Active_Roll_Ctrl == 1)        
        
      #if (__Pro_Active_ROP_Decel == 1)       
      
          if((del_target_2wheel_slip > 0)&&(ldu1OVER2WHInhibitFlag ==0))
  	  #else          
  		
  	      if((del_target_2wheel_slip > 0)&&(ROP_REQ_FRT_Pre_FL==0)&&(ldu1OVER2WHInhibitFlag ==0))   
      #endif  
        
	  #else         
	     if((del_target_2wheel_slip > 0)&&(ldu1OVER2WHInhibitFlag ==0))
	        
    #endif  
        {
            FLAG_TWO_WHEEL = 1;
            //FLAG_TWO_WHEEL_RL = 1;
        }
        else if((FLAG_TWO_WHEEL==1)&&(lcesps16Over2wheelExitMoment>0)&&(ldu1OVER2WHInhibitFlag ==0)&&(lcesps16OVER2WH_CompCount <5))
        {
        	  FLAG_TWO_WHEEL = 1;
        }
        else
        {
        	  FLAG_TWO_WHEEL = 0;
        }
    
    //#if (__Pro_Active_Roll_Ctrl == 1)        
    //    if((FLAG_TWO_WHEEL == 1)&&(SLIP_CONTROL_NEED_FL==1)&&(SLIP_CONTROL_NEED_FR==0)&&(ABS_fz==0)&&(ROP_REQ_FRT_Pre_FL==0)) //imsi 2012.05.18
    //#else         
        if((FLAG_TWO_WHEEL == 1)&&(SLIP_CONTROL_NEED_FL==1)&&(SLIP_CONTROL_NEED_FR==0))
    //#endif
              
        {
            FLAG_TWO_WHEEL = 1;
            FLAG_TWO_WHEEL_RL = 1;
        }
        else
        {
            FLAG_TWO_WHEEL = 0;
            FLAG_TWO_WHEEL_RL = 0;
        }

        if((FLAG_TWO_WHEEL == 1)&&(FLAG_TWO_WHEEL_RL == 1))
        {
            del_target_slip_rl = -del_target_2wheel_slip;
            del_target_slip_rr = 0;

            //SLIP_CONTROL_BRAKE_RL = 1;
        }
        else
        {
            del_target_slip_rl = 0;
            del_target_slip_rr = 0;
        }
#endif
    }
    else
    {
    	  ;
    }
    
    if(SLIP_CONTROL_BRAKE_FR == 1)
    {
#if __MOMENT_TO_TARGET_SLIP_NEW 
        
        if(SLIP_CONTROL_BRAKE_FL_OLD ==1)
        {
        		FLAG_TWO_WHEEL = 0;
        		FLAG_TWO_WHEEL_RL = 0;
        }
        else
        {
        		;
        }
        		
        esp_tempW7 = McrAbs(delta_moment_q);
        
        esp_tempW0 = LCESP_s16FindRoadDependatGain(esp_mu, (int16_t)U8_OS_2_WHEEL_QM_ENT_H
                                                         , (int16_t)U8_OS_2_WHEEL_QM_ENT_M
                                                         , (int16_t)U8_OS_2_WHEEL_QM_ENT_L);
        
        esp_tempW1 = LCESP_s16FindRoadDependatGain(esp_mu, (int16_t)U8_OS_2_WHEEL_QM_EXIT_H
                                                         , (int16_t)U8_OS_2_WHEEL_QM_EXIT_M
                                                         , (int16_t)U8_OS_2_WHEEL_QM_EXIT_L);
        
        esp_tempW2 = (int16_t)U8_OS_2_WHEEL_QM_ENT_ROP_H;        
        esp_tempW3 = (int16_t)U8_OS_2_WHEEL_QM_EXIT_ROP_H;
        
        #if(__ESC_CYCLE_DETECTION)
        esp_tempW4 = LCESP_s16FindRoadDependatGain(esp_mu, (int16_t)U8_OS_2_WHEEL_QM_ENT_2ND_H
                                                         , (int16_t)U8_OS_2_WHEEL_QM_ENT_2ND_M
                                                         , (int16_t)U8_OS_2_WHEEL_QM_ENT_2ND_L);        
        
        esp_tempW5 = LCESP_s16FindRoadDependatGain(esp_mu, (int16_t)U8_OS_2_WHEEL_QM_EXIT_2ND_H
                                                         , (int16_t)U8_OS_2_WHEEL_QM_EXIT_2ND_M
                                                         , (int16_t)U8_OS_2_WHEEL_QM_EXIT_2ND_L);
        #endif
        
        if ( ROP_REQ_ACT==1)
        {
        	  lcesps16Over2wheelEnterMoment = esp_tempW7 - (esp_tempW2 * 100); //Enter Condition_ROP
            lcesps16Over2wheelExitMoment = esp_tempW7 - (esp_tempW3 * 100); //Exit Condition_ROP        		
        }
        else
        {
        	  #if(__ESC_CYCLE_DETECTION)
        	  if(ldespu1ESC1stCycle == 1)
            {
        	      lcesps16Over2wheelEnterMoment = esp_tempW7 - (int16_t)(((int32_t)esp_tempW0) * 100);
        	      lcesps16Over2wheelExitMoment = esp_tempW7 - (int16_t)(((int32_t)esp_tempW1) * 100);
            }
            else if(ldespu1ESC2ndCycle == 1)
            {
        	      lcesps16Over2wheelEnterMoment = esp_tempW7 - (int16_t)(((int32_t)esp_tempW4) * 100);
        	      lcesps16Over2wheelExitMoment = esp_tempW7 - (int16_t)(((int32_t)esp_tempW5) * 100);
            }
            else
            {
        	      ;
            }
        	  #else
        	  lcesps16Over2wheelEnterMoment = esp_tempW7 - (int16_t)(((int32_t)esp_tempW0) * 100); //Enter Condition_Normal
            lcesps16Over2wheelExitMoment = esp_tempW7 - (int16_t)(((int32_t)esp_tempW1) * 100); //Exit Condition_Normal     
        	  #endif     
        }
        
        /* High-speed ( H_mu, M_mu, L_mu )  */                
        esp_tempW1 = LCESP_s16FindRoadDependatGain(esp_mu, S16_TWO_REAR_TARGET_SLIP_WEG_HMU_HSP,
                                                           S16_TWO_REAR_TARGET_SLIP_WEG_MMU_HSP,  
                                                           S16_TWO_REAR_TARGET_SLIP_WEG_LMU_HSP);
                                 
        /* Med-speed ( H_mu, M_mu, L_mu )  */                
        esp_tempW2 = LCESP_s16FindRoadDependatGain(esp_mu, S16_TWO_REAR_TARGET_SLIP_WEG_HMU_MSP,
                                                           S16_TWO_REAR_TARGET_SLIP_WEG_MMU_MSP,  
                                                           S16_TWO_REAR_TARGET_SLIP_WEG_LMU_MSP);
                                                                                                                              
        /* Low-speed ( H_mu, M_mu, L_mu )  */                                                   
        esp_tempW3 = LCESP_s16FindRoadDependatGain(esp_mu, S16_TWO_REAR_TARGET_SLIP_WEG_HMU_LSP,
                                                           S16_TWO_REAR_TARGET_SLIP_WEG_MMU_LSP,  
                                                           S16_TWO_REAR_TARGET_SLIP_WEG_LMU_LSP);    
                                                        
        esp_tempW5 = LCESP_s16IInter3Point( vrefk,  S16_TWO_WHEEL_SPEED_L_V,  esp_tempW3,
                                                    S16_TWO_WHEEL_SPEED_M_V,  esp_tempW2,
                                                    S16_TWO_WHEEL_SPEED_H_V,  esp_tempW1);
        
        
        /*        
        esp_tempW3 = LCESP_s16FindRoadDependatGain(esp_mu, S16_TWO_HMU_REAR_TAR_SLIP_WEG,
                                                           S16_TWO_MMU_REAR_TAR_SLIP_WEG,  
                                                           S16_TWO_LMU_REAR_TAR_SLIP_WEG);              
        */
#else  
        cal_target_2wheel = LCESP_s16IInter3Point( target_moment_f, 0, 0, m7, WHEEL_SLIP_20, m9, WHEEL_SLIP_60);

        lcesps16Over2wheelEnterMoment = cal_target_2wheel - cal_target_slip_front;

        esp_tempW0 = LCESP_s16FindRoadDependatGain(esp_mu,WHEEL_SLIP_3 , WHEEL_SLIP_5, WHEEL_SLIP_5);            

#endif 

        esp_tempW2 = LCESP_s16FindRoadDependatGain(esp_mu,S16_2WH_HMU_REAR_SLIP_MAX,
                            S16_2WH_MMU_REAR_SLIP_MAX, S16_2WH_LMU_REAR_SLIP_MAX);
                            
#if (__CRAM == 1)
        if((ldu1espDetectCRAM==1)&&(ldu1espDetectCRAM_FR==0))    // Over-, Outside Rear
        { 
        		
        		esp_tempW4 = (int16_t)( (((int32_t)lcesps16Over2wheelEnterMoment)*esp_tempW5)/100 );
            if(esp_tempW4 <= 0)
            {
                esp_tempW4 = 0;
            }
            else if(esp_tempW4 > esp_tempW2)
            {
                esp_tempW4 = esp_tempW2;
            }
            else
            {
                ;
            }
        		if(moment_pre_rear_cram > esp_tempW4)
        		{
            	del_target_2wheel_slip =  moment_pre_rear_cram ;    
            }
            else
            {
            	del_target_2wheel_slip  = esp_tempW4;
            }
            
        }        
        
    #if (__Pro_Active_Roll_Ctrl == 1)        
      #if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
        else if((ROP_REQ_ACT == 1)&&(RR_ESC.lcu1ropRearCtrlReq == 1))
      #else
        else if((ROP_REQ_ACT == 1)&&(lcu1ropRearCtrlReqRR == 1))
      #endif   
        {
        	          	  
            del_target_2wheel_slip =  S16_ROP_PROACTIVE_DEL_SLIP_R ;  // Tuning parameter...

            if(del_target_2wheel_slip <= 0)
            {
                del_target_2wheel_slip = 0;
            } 
            else
            {
                ;
            }
            
        }
    #endif 
    
			#if(__AHB_GEN3_SYSTEM==ENABLE) || (__IDB_LOGIC == ENABLE)
        else if((lsespu1AHBGEN3MpresBrkOn == 0)&&(ABS_fz==0)&&(lcu1US2WHControlFlag==0)		//        else if((ABS_fz==0)
           &&(((FLAG_TWO_WHEEL == 0)&&(lcesps16Over2wheelEnterMoment > 0)) //&&(FR.u8_Estimated_Active_Press> 0))           
           ||(FLAG_TWO_WHEEL == 1)))
			#else
        else if((MPRESS_BRAKE_ON == 0)&&(ABS_fz==0)&&(lcu1US2WHControlFlag==0)		//        else if((ABS_fz==0)
           &&(((FLAG_TWO_WHEEL == 0)&&(lcesps16Over2wheelEnterMoment > 0)) //&&(FR.u8_Estimated_Active_Press> 0))           
           ||(FLAG_TWO_WHEEL == 1)))
			#endif
        {
            del_target_2wheel_slip = (int16_t)( (((int32_t)lcesps16Over2wheelEnterMoment)*esp_tempW5)/100 );

            if(del_target_2wheel_slip <= 0)
            {
                del_target_2wheel_slip = 0;
            }
            else if(del_target_2wheel_slip > esp_tempW2)
            {
                del_target_2wheel_slip = esp_tempW2;
            }
            else
            {
                ;
            }
            
        }
        else
        {
            del_target_2wheel_slip = 0;
        }
#else //(__CRAM == 0)
    #if (__Pro_Active_Roll_Ctrl == 1)        
      #if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
      	#if(__AHB_GEN3_SYSTEM==ENABLE) || (__IDB_LOGIC == ENABLE)
      	if((ROP_REQ_ACT == 1)&&(lsespu1AHBGEN3MpresBrkOn == 0)&&((RR_ESC.lcu1ropRearCtrlReq == 1)||(lcesps16Over2wheelEnterMoment > 0)))
      	#else
      	if((ROP_REQ_ACT == 1)&&(MPRESS_BRAKE_ON == 0)&&((RR_ESC.lcu1ropRearCtrlReq == 1)||(lcesps16Over2wheelEnterMoment > 0)))
      	#endif
      #else
      	#if(__AHB_GEN3_SYSTEM==ENABLE) || (__IDB_LOGIC == ENABLE)
      	if((ROP_REQ_ACT == 1)&&(lsespu1AHBGEN3MpresBrkOn == 0)&&(lcu1ropRearCtrlReqRR == 1))
      	#else
      	if((ROP_REQ_ACT == 1)&&(MPRESS_BRAKE_ON == 0)&&(lcu1ropRearCtrlReqRR == 1))
      	#endif
      #endif        
        {

            del_target_2wheel_slip =  S16_ROP_PROACTIVE_DEL_SLIP_R ;  // Tuning parameter...

            if(del_target_2wheel_slip <= 0)
            {
                del_target_2wheel_slip = 0;
            } 
            else
            {
                ;
            }
            
        }
        
		#else        
        if(ROP_REQ_FRT_Pre == 1)
        {
            del_target_2wheel_slip = 0;
        }
    #endif 
   
			#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
        else if(
        	     (lsespu1AHBGEN3MpresBrkOn == 0)&&(ABS_fz==0)&&(lcu1US2WHControlFlag==0)		//        else if((ABS_fz==0)
               &&( ((FLAG_TWO_WHEEL == 0)&&(lcesps16Over2wheelEnterMoment > 0)&&(FR.s16_Estimated_Active_Press>(int16_t)U8_OVER2WHEEL_ENTER_EST_P))//&&(FR.u8_Estimated_Active_Press> 0))
                ||(FLAG_TWO_WHEEL == 1) )
               )
			#else
        else if(
        	     (MPRESS_BRAKE_ON == 0)&&(ABS_fz==0)&&(lcu1US2WHControlFlag==0)		//        else if((ABS_fz==0)
               &&( ((FLAG_TWO_WHEEL == 0)&&(lcesps16Over2wheelEnterMoment > 0)&&(FR.s16_Estimated_Active_Press>(int16_t)U8_OVER2WHEEL_ENTER_EST_P))//&&(FR.u8_Estimated_Active_Press> 0))
                ||(FLAG_TWO_WHEEL == 1) )
               )
			#endif
        {            

            del_target_2wheel_slip = (int16_t)( (((int32_t)lcesps16Over2wheelEnterMoment)*esp_tempW5)/100 );

            if(del_target_2wheel_slip <= 0)
            {
                del_target_2wheel_slip = 0;
            }
            else if(del_target_2wheel_slip > esp_tempW2)
            {
                del_target_2wheel_slip = esp_tempW2;
            }
            else
            {
                ;
            }
        }
        else
        {
            del_target_2wheel_slip = 0;
        }
 #endif


#if (__CRAM == 1)
		    if(ldu1espDetectCRAM==0)
		    {  
		        if(del_target_2wheel_slip > 0)
		        {
		            FLAG_TWO_WHEEL = 1;
		            FLAG_TWO_WHEEL_RR = 1;
		        }
		        else
		        {
		            FLAG_TWO_WHEEL = 0;
		            FLAG_TWO_WHEEL_RR = 0;
		        }
		    }
		    else
		    {
		        if(del_target_2wheel_slip > 0)
		        {
		            FLAG_TWO_WHEEL = 1;
		            FLAG_TWO_WHEEL_RR = 1;
		        }
		        else  
		        {
		            FLAG_TWO_WHEEL = 0; 
		            FLAG_TWO_WHEEL_RR = 0;
		            
		            //SLIP_CONTROL_BRAKE_RR = 0;
		        }       
		    } 
    
        if(FLAG_TWO_WHEEL_RR == 1)
        {
            del_target_slip_rr = -del_target_2wheel_slip;
            
            if(ldu1espDetectCRAM==0)
            {
                del_target_slip_rl = 0;
            }
        
            //SLIP_CONTROL_BRAKE_RR = 1;
        }
        else
        {
            ;
        }
#else //(__CRAM == 0)
	
    #if (__Pro_Active_Roll_Ctrl == 1)        
    
      #if (__Pro_Active_ROP_Decel == 1)       
      
          if((del_target_2wheel_slip > 0)&&(ldu1OVER2WHInhibitFlag ==0))
  		#else          
  		
  	      if((del_target_2wheel_slip > 0)&&(ROP_REQ_FRT_Pre_FR==0)&&(ldu1OVER2WHInhibitFlag ==0))   
      #endif  

    #else         
        if((del_target_2wheel_slip > 0)&&(ldu1OVER2WHInhibitFlag ==0))
            
    #endif  
        {
            FLAG_TWO_WHEEL = 1;
            //FLAG_TWO_WHEEL_RR = 1;
        }
        else if((FLAG_TWO_WHEEL==1)&&(lcesps16Over2wheelExitMoment>0)&&(ldu1OVER2WHInhibitFlag ==0)&&(lcesps16OVER2WH_CompCount <5))
        {
        	  FLAG_TWO_WHEEL = 1;
        }
        else
        {
        	  FLAG_TWO_WHEEL = 0;
        }
         
    //#if (__Pro_Active_Roll_Ctrl == 1)        
    //    if((FLAG_TWO_WHEEL == 1)&&(SLIP_CONTROL_NEED_FR==1)&&(SLIP_CONTROL_NEED_FL==0)&&(ABS_fz==0)&&(ROP_REQ_FRT_Pre_FR==0))

    //#else         
        if((FLAG_TWO_WHEEL == 1)&&(SLIP_CONTROL_NEED_FR==1)&&(SLIP_CONTROL_NEED_FL==0))
            
    //#endif         

        {
            FLAG_TWO_WHEEL = 1;
            FLAG_TWO_WHEEL_RR = 1;
        }
        else
        {
            FLAG_TWO_WHEEL = 0;
            FLAG_TWO_WHEEL_RR = 0;
        }
        
        if((FLAG_TWO_WHEEL == 1)&&(FLAG_TWO_WHEEL_RR == 1))
        {
            del_target_slip_rr = -del_target_2wheel_slip;
            del_target_slip_rl = 0;

            //SLIP_CONTROL_BRAKE_RR = 1;
        }
        else
        {
            del_target_slip_rl = 0;
            del_target_slip_rr = 0;
        }
#endif
    }
    else
    {
    	  ;
    }
    
    if((SLIP_CONTROL_BRAKE_FL ==0)&&(SLIP_CONTROL_BRAKE_FR==0))
    {
        FLAG_TWO_WHEEL = 0;
        FLAG_TWO_WHEEL_RL = 0;
        FLAG_TWO_WHEEL_RR = 0;
        
			#if (__CRAM == 1)&&( __Ext_Under_Ctrl == 0 )&&(__Pro_Active_Roll_Ctrl==0)
        FLAG_TWO_WHEEL_RL = 0;
        FLAG_TWO_WHEEL_RR = 0;
        del_target_slip_rl = 0;
        del_target_slip_rr = 0;
      #endif
    }
    else
    {
        ;
    }
    

    /************************* rear ********************************/
/*
#if __MOMENT_TO_TARGET_SLIP_NEW
 
  #if (__CRAM == 1)
    if((SLIP_CONTROL_BRAKE_RL == 1)&&(FLAG_TWO_WHEEL_RL == 0))
  #else
    if((SLIP_CONTROL_BRAKE_RL == 1)&&(FLAG_TWO_WHEEL == 0))
  #endif
    { 
        del_target_slip_rl =  -(McrAbs(delta_moment_q)); 
        
#else  
    	esp_tempW8 = DEL_TAR_SLIP_R_HMU_ALPHA_Min;
    	esp_tempW7 = DEL_TAR_SLIP_R_HMU_ALPHA_Max;
    	
    	esp_tempW5 = DEL_TAR_SLIP_R_MMU_ALPHA_Min;
    	esp_tempW4 = DEL_TAR_SLIP_R_MMU_ALPHA_Max;
    	
    	if(esp_tempW7 < esp_tempW8) esp_tempW7 =esp_tempW8+100;
     	if(esp_tempW4 < esp_tempW5) esp_tempW4 =esp_tempW5+100;
     	    	
    	if(esp_tempW5 < 100) esp_tempW5 =100;
    	if(esp_tempW8 < 100) esp_tempW8 =100;
  
     	if(esp_tempW4 > 2000) esp_tempW4 =2000;
    	if(esp_tempW7 > 2000) esp_tempW7 =2000;
    	   	   
    	m1 = LCESP_s16IInter2Point( alpha_r, esp_tempW8, 1000, esp_tempW7, 2000); //10 %,  high mu model 
    	m2 = LCESP_s16IInter2Point( alpha_r, esp_tempW8, 4000, esp_tempW7, 6000); //20%
   	
    	m4 = LCESP_s16IInter2Point( alpha_r, esp_tempW5, 1000, esp_tempW4, 2000); //10%,  low mu model 
    	m5 = LCESP_s16IInter2Point( alpha_r, esp_tempW5, 4000, esp_tempW4, 6000); //20%
    	
    	m7 = LCESP_s16IInter2Point( det_alatm, lsesps16MuMedHigh, m4, lsesps16MuHigh, m1); //10%
    	m8 = LCESP_s16IInter2Point( det_alatm, lsesps16MuMedHigh, m5, lsesps16MuHigh, m2); //20%
    	
   #if (__CRAM == 1)
    if((SLIP_CONTROL_BRAKE_RL == 1)&&(FLAG_TWO_WHEEL_RL == 0))
   #else
    if((SLIP_CONTROL_BRAKE_RL == 1)&&(FLAG_TWO_WHEEL == 0))
   #endif
    {
   	
        target_slip_r_0 = McrAbs(slip_m_rl_current);
        if(slip_m_rl_current < 0)
        {
	            target_moment_r_0 = LCESP_s16IInter3Point( target_slip_r_0, 0, 0, WHEEL_SLIP_10, m7, WHEEL_SLIP_20, m8);

        }
        else
        {
            target_moment_r_0 = 0;
        }

        target_moment_r = target_moment_r_0+McrAbs(delta_moment_q);

        cal_target_slip_rear = LCESP_s16IInter3Point( target_moment_r, 0, 0, m7, WHEEL_SLIP_10, m8, WHEEL_SLIP_20);

        del_target_slip_rl =  -(cal_target_slip_rear - target_slip_r_0);
#endif    

    }
    else
    {
     #if (__CRAM == 1)
        if( FLAG_TWO_WHEEL_RL ==0)
     #else
        if( FLAG_TWO_WHEEL ==0)
     #endif
        {
            del_target_slip_rl = 0;
        }
        else
        {
            ;
        }
    }
   
#if __MOMENT_TO_TARGET_SLIP_NEW

  #if (__CRAM == 1)
   if((SLIP_CONTROL_BRAKE_RR == 1)&&(FLAG_TWO_WHEEL_RR == 0))
  #else
   if((SLIP_CONTROL_BRAKE_RR == 1)&&(FLAG_TWO_WHEEL == 0))
  #endif
    {
        del_target_slip_rr =  -(McrAbs(delta_moment_q));
#else     
    
  #if (__CRAM == 1)
   if((SLIP_CONTROL_BRAKE_RR == 1)&&(FLAG_TWO_WHEEL_RR == 0))
  #else
   if((SLIP_CONTROL_BRAKE_RR == 1)&&(FLAG_TWO_WHEEL == 0))
  #endif
    {
        target_slip_r_0 = McrAbs(slip_m_rr_current);
        if(slip_m_rr_current < 0)
        {
            target_moment_r_0 = LCESP_s16IInter3Point( target_slip_r_0, 0, 0, WHEEL_SLIP_10, m7, WHEEL_SLIP_20, m8);

        }
        else
        {
            target_moment_r_0 = 0;
        }

        target_moment_r = target_moment_r_0+McrAbs(delta_moment_q);

        cal_target_slip_rear = LCESP_s16IInter3Point( target_moment_r, 0, 0, m7, WHEEL_SLIP_10, m8, WHEEL_SLIP_20);

        del_target_slip_rr =  -(cal_target_slip_rear - target_slip_r_0);
#endif    
    }
    else
    {
     #if (__CRAM == 1)
        if( FLAG_TWO_WHEEL_RR ==0)
     #else
        if( FLAG_TWO_WHEEL ==0)
     #endif
        {
            del_target_slip_rr = 0;
        }
        else
        {
            ;
        }
    }
*/    
    
}

void LDESP_vRecognizeVehicleState(void)
{
#if	__VDC
  	LSESP_vCheckEspError();
#endif

/***********************************************************************/
/*Gear                                                                */
/***********************************************************************/

#if	__TCS
/*	estimation_tm_gear();*/
#endif


/***********************************************************************/
/*  BEND DETECT                                                        */
/***********************************************************************/
/*    BEND_DETECTF();		*/

/* find_ay_threshold: CIRCLE_SML,CIRCLE_BIG  */


/***********************************************************************/
/* Lateral Acceleration Estimation for TCS                             */
/***********************************************************************/
#if	__VDC
/***********************************************************************/
/* Beta Estimation and Drift detection                                 */
/***********************************************************************/

    LDESP_CompBetaDiff();
    LDESP_CompDetBetaDotForSlip();    
  #if __SIDE_SLIP_ANGLE_MODULE
    LDESP_CompSideSlipAngle();
  #endif

/*MISRA, H22*/
/*  #if __MGH40_Beta_Estimation
    Beta_Estimation();
    #endif*/
    
#if MGH60_NEW_TARGET_SLIP == 0    
    LDESP_DetectDriftForSlip();
#endif    
    #if __Beta_Control
    LDESP_DetectBetaUnderDrift();
    #endif

/***********************************************************************/
/* On center feel / slalom                                            */
/***********************************************************************/

    DETECT_ON_CENTER();
    DETECT_SLALOM();
    LDESP_DetectRevSteer();

/***********************************************************************/
/* Calculation, Limitation of Vehicle Model                            */
/***********************************************************************/

/*MISRA, H22*/
/*	Vehicle_Model_Calculation();*/
	LDESP_vCalVehicleModel();

/***********************************************************************/
/* Calculation of Delta_YAW                                            */
/***********************************************************************/

    DETECT_SAME_PHASE();
    LDESP_vCompDeltaYaw();
    LDESP_vCompDetDeltaYaw();

	#if (__CRAM == 1)
   LDESP_vDetectCRAM();  
	#endif
	
	#if	( __Ext_Under_Ctrl == 1 ) 
	    LDESP_DetectExtemeUnderCtrl();
	#endif

	LDESP_Detect2WheelOverCtrl();
	LDESP_Detect2WheelUnderCtrl();	

/***********************************************************************/
/*  Deceleration Brake Lamp Signal for GM (A430)                         */
/***********************************************************************/
	#if (GMLAN_ENABLE==ENABLE)
		LDESP_vSetDecelBrakeLamp();
	#endif

#endif

}

#if	( __Ext_Under_Ctrl == 1 )
void LDESP_DetectExtemeUnderCtrl(void)
{  
	
/* Fail-management */	
	#if(__AHB_GEN3_SYSTEM==ENABLE) || (__IDB_LOGIC == ENABLE)
    if((ESP_ERROR_FLG==1)||(lsespu1AHBGEN3MpresBrkOn==1)||(fu1YawSusDet==1)||(fu1AySusDet==1)||(fu1StrSusDet==1)||(fu1MCPSusDet==1)||(VDC_DISABLE_FLG==1))
	#else
    if((ESP_ERROR_FLG==1)||(MPRESS_BRAKE_ON==1)||(fu1YawSusDet==1)||(fu1AySusDet==1)||(fu1StrSusDet==1)||(fu1MCPSusDet==1)||(VDC_DISABLE_FLG==1))
	#endif
    {
        EXTR_UNDER_INHIBIT_FLAG = 1;
    }
    else
    {
    		EXTR_UNDER_INHIBIT_FLAG = 0;
    }        	
     
    esp_tempW0 = McrAbs(delta_yaw_first);
    esp_tempW1 = (int16_t)((((int32_t)esp_tempW0)*1)/50); // 5000 --> 100'/s
         
/* Entrance of Extreme Understeer Control */	

/* 11 SWD : Low-mu Entrance condition add */

    if ( ( SLIP_CONTROL_BRAKE_RL == 1 )||( SLIP_CONTROL_BRAKE_RR == 1 ) )   
    {
    /*  High_mu  */
    esp_tempW4 = LCESP_s16IInter4Point(vrefk, S16_REAR_HMU_SPD_L ,  S16_EXT_UND_ENT_DEL_YAW_HMU_LSP,
                                              S16_REAR_HMU_SPD_M1 , S16_EXT_UND_ENT_DEL_YAW_HMU_MSP,
                                              S16_REAR_HMU_SPD_M2 , S16_EXT_UND_ENT_DEL_YAW_HMU_MSP,
                                              S16_REAR_HMU_SPD_H ,  S16_EXT_UND_ENT_DEL_YAW_HMU_HSP );  
                                              
    /* Med mu  */                                       
    esp_tempW5 = LCESP_s16IInter4Point(vrefk, S16_REAR_MMU_SPD_L ,  S16_EXT_UND_ENT_DEL_YAW_MMU_LSP,
                                              S16_REAR_MMU_SPD_M1 , S16_EXT_UND_ENT_DEL_YAW_MMU_MSP,
                                              S16_REAR_MMU_SPD_M2 , S16_EXT_UND_ENT_DEL_YAW_MMU_MSP,
                                              S16_REAR_MMU_SPD_H ,  S16_EXT_UND_ENT_DEL_YAW_MMU_HSP );                                   
                          
    /* Low mu   */                                  
    esp_tempW6 = LCESP_s16IInter4Point(vrefk, S16_REAR_LMU_SPD_L ,  S16_EXT_UND_ENT_DEL_YAW_LMU_LSP,
                                              S16_REAR_LMU_SPD_M1 , S16_EXT_UND_ENT_DEL_YAW_LMU_MSP,
                                              S16_REAR_LMU_SPD_M2 , S16_EXT_UND_ENT_DEL_YAW_LMU_MSP,
                                              S16_REAR_LMU_SPD_H ,  S16_EXT_UND_ENT_DEL_YAW_LMU_HSP );     
                                              
    esp_tempW2  = LCESP_s16FindRoadDependatGain( esp_mu,   esp_tempW4,  esp_tempW5,  esp_tempW6 ); 
    
    /*  High_mu  */
    esp_tempW7 = LCESP_s16IInter4Point(vrefk, S16_REAR_HMU_SPD_L ,  S16_EXT_UND_EXT_DEL_YAW_HMU_LSP,
                                              S16_REAR_HMU_SPD_M1 , S16_EXT_UND_EXT_DEL_YAW_HMU_MSP,
                                              S16_REAR_HMU_SPD_M2 , S16_EXT_UND_EXT_DEL_YAW_HMU_MSP,
                                              S16_REAR_HMU_SPD_H ,  S16_EXT_UND_EXT_DEL_YAW_HMU_HSP );  
                                              
    /* Med mu  */                                       
    esp_tempW8 = LCESP_s16IInter4Point(vrefk, S16_REAR_MMU_SPD_L ,  S16_EXT_UND_EXT_DEL_YAW_MMU_LSP,
                                              S16_REAR_MMU_SPD_M1 , S16_EXT_UND_EXT_DEL_YAW_MMU_MSP,
                                              S16_REAR_MMU_SPD_M2 , S16_EXT_UND_EXT_DEL_YAW_MMU_MSP,
                                              S16_REAR_MMU_SPD_H ,  S16_EXT_UND_EXT_DEL_YAW_MMU_HSP );                                   
                          
    /* Low mu   */                                  
    esp_tempW9 = LCESP_s16IInter4Point(vrefk, S16_REAR_LMU_SPD_L ,  S16_EXT_UND_EXT_DEL_YAW_LMU_LSP,
                                              S16_REAR_LMU_SPD_M1 , S16_EXT_UND_EXT_DEL_YAW_LMU_MSP,
                                              S16_REAR_LMU_SPD_M2 , S16_EXT_UND_EXT_DEL_YAW_LMU_MSP,
                                              S16_REAR_LMU_SPD_H ,  S16_EXT_UND_EXT_DEL_YAW_LMU_HSP );     
 
    esp_tempW3  = LCESP_s16FindRoadDependatGain( esp_mu,   esp_tempW7,  esp_tempW8,  esp_tempW9 );                    
                            
    }
    else
    {
      ;
    }         
                                     
    if( ( EXTR_UNDER_INHIBIT_FLAG == 0 ) 
    	  &&( EXTREME_UNDER_CONTROL == 0 )
        &&( YAW_CHANGE_LIMIT_ACT == 1 )  
        &&( vrefk < S16_MAX_UND_VEL_EXIT )  			// TP   600           
        &&( delta_yaw_first  < esp_tempW2 )     // TP   u_delta_yaw_thres
        &&( McrAbs(delta_moment_q_os) < 900  )      
        &&( delta_yaw < S16_EXT_UND_DEL_YAW_2ND_ENTER )   	// TP , 300
//        &&( McrAbs(alat) >= S16_EXT_UND_AY_ENTER ) 	// TP  850
        &&( mtp <= S16_EXT_UND_MTP_ENTER ) 	// TP  20 
        &&( vrefk > S16_EXT_UND_VEL_ENTER )  			// TP   600
//        &&( vrefk < 900 )  			// TP   600
		#if __ROP        
				&&(ROP_REQ_ACT == 0)
    #endif   
        &&( (( SLIP_CONTROL_BRAKE_RL == 1 )&&( yaw_sign >= 0 )) 
            ||(( SLIP_CONTROL_BRAKE_RR == 1 )&&( yaw_sign < 0 )) )                 
//        &&(( SLIP_CONTROL_BRAKE_RL == 1 )||( SLIP_CONTROL_BRAKE_RR == 1 ))
//        &&( (( SLIP_CONTROL_BRAKE_RL == 1 )&&( slip_m_rl < S16_EXT_UND_R_WHEEL_SLIP_ENT )) 
//            ||(( SLIP_CONTROL_BRAKE_RR == 1 )&&( slip_m_rr < S16_EXT_UND_R_WHEEL_SLIP_ENT )) )            
       )
    {  
        EXTREME_UNDER_CONTROL = 1;
        
        if( SLIP_CONTROL_BRAKE_RL == 1 )
        {
            EXTR_UNDER_FL_IN = 1 ;
            EXTR_UNDER_FR_IN = 0 ;
        }
        else if ( SLIP_CONTROL_BRAKE_RR == 1 )
        {
            EXTR_UNDER_FL_IN = 0 ;
            EXTR_UNDER_FR_IN = 1 ;
        }
        else
        {  
        	;
        }    
    }
    else if ( ( EXTR_UNDER_INHIBIT_FLAG == 0 ) 
    	  &&( EXTREME_UNDER_CONTROL == 1 )
        &&( YAW_CHANGE_LIMIT_ACT == 1 )  
        &&( delta_yaw_first  < esp_tempW3 )     // TP   u_delta_yaw_thres
        &&( McrAbs(delta_moment_q_os) < 900 )           
        &&( vrefk < S16_MAX_UND_VEL_EXIT )  			// TP   600        
        &&( delta_yaw < S16_EXT_UND_DEL_YAW_2ND_EXIT )   	// TP , 350
//        &&( McrAbs(alat) >= S16_EXT_UND_AY_EXIT ) 	// TP  850
        &&( mtp <= S16_EXT_UND_MTP_EXIT )   		// TP 
        &&( vrefk > S16_EXT_UND_VEL_EXIT )  			// TP   600
		#if __ROP        
				&&(ROP_REQ_ACT == 0)
    #endif   
        &&( (( SLIP_CONTROL_BRAKE_RL == 1 )&&( yaw_sign >= 0 )) 
            ||(( SLIP_CONTROL_BRAKE_RR == 1 )&&( yaw_sign < 0 )) )             
//        &&(( SLIP_CONTROL_BRAKE_RL == 1 )||( SLIP_CONTROL_BRAKE_RR == 1 ))
       )
    {
        EXTREME_UNDER_CONTROL = 1;
        
        if( SLIP_CONTROL_BRAKE_RL == 1 )
        {
            EXTR_UNDER_FL_IN = 1 ;
            EXTR_UNDER_FR_IN = 0 ;
        }
        else if ( SLIP_CONTROL_BRAKE_RR == 1 )
        {
            EXTR_UNDER_FL_IN = 0 ;
            EXTR_UNDER_FR_IN = 1 ;
        }
        else
        {  
        	;
        }    
    }
    else
    {
    		EXTREME_UNDER_CONTROL = 0;
            EXTR_UNDER_FL_IN = 0 ;
            EXTR_UNDER_FR_IN = 0 ;
    }  
    
/* Fade-out detection */

		if(EXTREME_UNDER_CONTROL==1)
		{
   	
			EXTREME_UNDER_CTRL_FADE_OUT = 1 ;   	
      
      ext_under_on_counter = 0 ;
        
    }
    else
    {
			if((EXTREME_UNDER_CONTROL==0)&&(EXTREME_UNDER_CTRL_FADE_OUT==1))
			{
            ext_under_on_counter = ext_under_on_counter + 1 ;
            
            if(ext_under_on_counter > 50)
            {
                EXTREME_UNDER_CTRL_FADE_OUT = 0 ;
            }
            else
            {
                ;
            }
      }
      else
      {
          ext_under_on_counter = 0 ;
      } 
    }
 

/*  Preq,frt calculation */

    if( EXTREME_UNDER_CONTROL ==1 )
    {
        if ( yaw_error_under_dot > 0 )  /* Increase */
        {
          esp_tempW1 = LCESP_s16FindRoadDependatGain(esp_mu,S16_EXT_UND_R_US_OUT_P_INC_Hmu,
                              S16_EXT_UND_R_US_OUT_P_INC_Mmu, S16_EXT_UND_R_US_OUT_P_INC_Lmu);
  
          esp_tempW2 = LCESP_s16FindRoadDependatGain(esp_mu,S16_EXT_UND_R_US_OUT_D_INC_Hmu,
                              S16_EXT_UND_R_US_OUT_D_INC_Mmu, S16_EXT_UND_R_US_OUT_D_INC_Lmu);
        }
        else                           /* Derease */
        { 
    
        esp_tempW1 = LCESP_s16FindRoadDependatGain(esp_mu,S16_EXT_UND_R_US_OUT_P_GAIN_Hmu,
                            S16_EXT_UND_R_US_OUT_P_GAIN_Mmu, S16_EXT_UND_R_US_OUT_P_GAIN_Lmu);

        esp_tempW2 = LCESP_s16FindRoadDependatGain(esp_mu,S16_EXT_UND_R_US_OUT_D_GAIN_Hmu,
                            S16_EXT_UND_R_US_OUT_D_GAIN_Mmu, S16_EXT_UND_R_US_OUT_D_GAIN_Lmu);
        }
        
        esp_tempW5 =LCESP_s16FindRoadDependatGain( esp_mu, S16_HMU_EU_F_LOW_SPD_GAIN_WEG,
                                    S16_MMU_EU_F_LOW_SPD_GAIN_WEG, S16_LMU_EU_F_LOW_SPD_GAIN_WEG);
                                    
        esp_tempW6 =LCESP_s16FindRoadDependatGain( esp_mu, S16_HMU_EU_F_MED_SPD_GAIN_WEG,
                                    S16_MMU_EU_F_MED_SPD_GAIN_WEG, S16_LMU_EU_F_MED_SPD_GAIN_WEG);
    
        esp_tempW7 =LCESP_s16FindRoadDependatGain( esp_mu, S16_HMU_EU_F_HIGH_SPD_GAIN_WEG,
                                    S16_MMU_EU_F_HIGH_SPD_GAIN_WEG, S16_LMU_EU_F_HIGH_SPD_GAIN_WEG);
 
        esp_tempW0 = LCESP_s16IInter4Point(vrefk, rear_low_speed ,  esp_tempW5,
                                                  rear_med1_speed , esp_tempW6,
                                                  rear_med2_speed , esp_tempW6,
                                                  rear_high_speed , esp_tempW7);     
                                                  
        esp_tempW1 = (int16_t)( (((int32_t)esp_tempW0)*esp_tempW1)/100 );
    
        esp_tempW2 = (int16_t)( (((int32_t)esp_tempW0)*esp_tempW2)/100 );                                                       
 
        esp_tempW3 = (int16_t)( (((int32_t)delta_yaw_first)*esp_tempW1)/100 );
    
        esp_tempW4 = (int16_t)( ((int32_t)yaw_error_under_dot)*esp_tempW2/10 );        

        ExtUnder_Req_Decel = ( esp_tempW3 + esp_tempW4 ) ;     
        
        esp_tempW5 = LCESP_s16FindRoadDependatGain(esp_mu,S16_EXT_UND_MAX_FRT_MOMENT_Hmu,
                            S16_EXT_UND_MAX_FRT_MOMENT_Mmu, S16_EXT_UND_MAX_FRT_MOMENT_Lmu); 
        
        if( ExtUnder_Req_Decel < 0 )
        {
            ExtUnder_Req_Decel = 0 ;
        }
        else if ( ExtUnder_Req_Decel > (esp_tempW5*10) )
        {
            ExtUnder_Req_Decel = esp_tempW5*10 ;
        }
        else
        {
            ;
        }                     
 
        ExtUnder_Req_Press = (int16_t)( ((int32_t)ExtUnder_Req_Decel)/10 );   // [1 bar], 30
        
        esp_tempW6 = LCESP_s16FindRoadDependatGain(esp_mu,S16_EXT_UND_MAX_FRT_IN_MOMENT_Hmu,
                            S16_EXT_UND_MAX_FRT_IN_MOMENT_Mmu, S16_EXT_UND_MAX_FRT_IN_MOMENT_Lmu);         
        
				esp_tempW1 = esp_tempW6  ; 		// TP, Max_press wrt Inside_Frt 10% [1 bar] 130
				esp_tempW2 = 100 ;    // Press --> Moment Gain
				
        if( ExtUnder_Req_Press <= esp_tempW1 )
        {
            Extr_Under_Frt_Moment_In  = (int16_t)(((int32_t)ExtUnder_Req_Press)*esp_tempW2); 
            Extr_Under_Frt_Moment_Out  = 0 ;             
        }
        else if ( ExtUnder_Req_Press > esp_tempW1 )
        {
            Extr_Under_Frt_Moment_In  = (int16_t)(((int32_t)esp_tempW1)*esp_tempW2); 
            
						esp_tempW3 = ExtUnder_Req_Press - esp_tempW1 ;        
						
						if ( esp_tempW3 < 0 )
		        {
		            esp_tempW3 = 0 ;
		        }
		        else
		        {
		            ;
		        }      
		        
            Extr_Under_Frt_Moment_Out  = (int16_t)(((int32_t)esp_tempW3)*esp_tempW2); 
        }
        else
        {
            Extr_Under_Frt_Moment_In  = 0 ; 
            Extr_Under_Frt_Moment_Out = 0 ;            
        }            				 
        
        if( EXTR_UNDER_FL_IN == 1 )
        {
            Extr_Under_Frt_Moment_FL  = Extr_Under_Frt_Moment_In ; 
            Extr_Under_Frt_Moment_FR  = Extr_Under_Frt_Moment_Out ;             
        }
        else if ( EXTR_UNDER_FR_IN == 1 )
        {
            Extr_Under_Frt_Moment_FL  = Extr_Under_Frt_Moment_Out ; 
            Extr_Under_Frt_Moment_FR  = Extr_Under_Frt_Moment_In ; 
        }
        else
        {
            Extr_Under_Frt_Moment_FL  = 0 ; 
            Extr_Under_Frt_Moment_FR  = 0 ;          
        }                
        
        /* As Frt_inside wheel_slip, Max_Pressure determine...  */
        
        if( EXTR_UNDER_FL_IN == 1 )
        {
        	
        	if((EXTR_UNDER_FRT_FL_SLIP_1ST==0)&&( slip_m_fl < S16_EXT_UND_PRESS_FRT_SLIP_1ST ))
        	{
        		EXTR_UNDER_FRT_FL_SLIP_1ST = 1 ;
        		Extr_Under_Frt_fl_Press_1st = FL.u8_Estimated_Active_Press ;
        		
        		if(Extr_Under_Frt_fl_Press_1st<S16_EXT_UND_PRESS_F_IN_HALF_MAX)
		        {
      					Extr_Under_Frt_fl_Press_1st = S16_EXT_UND_PRESS_F_IN_HALF_MAX ;
		        }
		        else
		      	{
		      			 ;
		      	}
        		
        		
        	}
        	else
      		{  
      			  if(EXTR_UNDER_FRT_FL_SLIP_1ST==0)
		        	{
      					Extr_Under_Frt_fl_Press_1st = S16_EXT_UND_PRESS_F_IN_HALF_MAX ;
		        	}
		        	else
		      		{
		      			 ;
		      		}  
      		} 
      		
    		#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
          FL_ESC.Extr_Under_Frt_Press_Max = (int16_t)( (((int32_t)Extr_Under_Frt_fl_Press_1st)*S16_EXT_UND_PRESS_F_IN_WEG)/100 );
        	FR_ESC.Extr_Under_Frt_Press_Max = (int16_t)( (((int32_t)Extr_Under_Frt_fl_Press_1st)*S16_EXT_UND_PRESS_F_OUT_WEG)/100 );
        #else
        	Extr_Under_Frt_fl_Press_Max = (int16_t)( (((int32_t)Extr_Under_Frt_fl_Press_1st)*S16_EXT_UND_PRESS_F_IN_WEG)/100 );
        	Extr_Under_Frt_fr_Press_Max = (int16_t)( (((int32_t)Extr_Under_Frt_fl_Press_1st)*S16_EXT_UND_PRESS_F_OUT_WEG)/100 );       		
        #endif
        	         
        }
        else
        {
        		EXTR_UNDER_FRT_FL_SLIP_1ST=0 ;
      			Extr_Under_Frt_fl_Press_1st = S16_EXT_UND_PRESS_F_IN_HALF_MAX ;        		
        }
        	
        if( EXTR_UNDER_FR_IN == 1 )
        {
        	
        	if((EXTR_UNDER_FRT_FR_SLIP_1ST==0)&&( slip_m_fr < S16_EXT_UND_PRESS_FRT_SLIP_1ST ))
        	{
        		EXTR_UNDER_FRT_FR_SLIP_1ST = 1 ;
        		Extr_Under_Frt_fr_Press_1st = FR.u8_Estimated_Active_Press ;        		

        		if(Extr_Under_Frt_fr_Press_1st<S16_EXT_UND_PRESS_F_IN_HALF_MAX)
		        {
      					Extr_Under_Frt_fr_Press_1st = S16_EXT_UND_PRESS_F_IN_HALF_MAX ;
		        }
		        else
		      	{
		      			 ;
		      	}        		
        		
        	}
        	else
      		{  
      			  if(EXTR_UNDER_FRT_FR_SLIP_1ST==0)
		        	{
      					Extr_Under_Frt_fr_Press_1st = S16_EXT_UND_PRESS_F_IN_HALF_MAX ;
		        	}
		        	else
		      		{
		      			 ;
		      		}  
      		}  
      		
      	#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
          FR_ESC.Extr_Under_Frt_Press_Max = (int16_t)( (((int32_t)Extr_Under_Frt_fr_Press_1st)*S16_EXT_UND_PRESS_F_IN_WEG)/100 );
        	FL_ESC.Extr_Under_Frt_Press_Max = (int16_t)( (((int32_t)Extr_Under_Frt_fr_Press_1st)*S16_EXT_UND_PRESS_F_OUT_WEG)/100 );
        #else
        	Extr_Under_Frt_fr_Press_Max = (int16_t)( (((int32_t)Extr_Under_Frt_fr_Press_1st)*S16_EXT_UND_PRESS_F_IN_WEG)/100 );
        	Extr_Under_Frt_fl_Press_Max = (int16_t)( (((int32_t)Extr_Under_Frt_fr_Press_1st)*S16_EXT_UND_PRESS_F_OUT_WEG)/100 );         		
        #endif	
        	         
        }
        else
        {
        		EXTR_UNDER_FRT_FR_SLIP_1ST=0 ;
      			Extr_Under_Frt_fr_Press_1st = S16_EXT_UND_PRESS_F_IN_HALF_MAX ;        		
        }        	 
        
    }
    else
    {
            ExtUnder_Req_Decel = 0 ;
            ExtUnder_Req_Press = 0 ; 
            Extr_Under_Frt_Moment_In  = 0 ; 
            Extr_Under_Frt_Moment_Out = 0 ;      
            EXTR_UNDER_FRT_FL_SLIP_1ST =0 ;    
        		EXTR_UNDER_FRT_FR_SLIP_1ST =0 ;
      			Extr_Under_Frt_fl_Press_1st = 0 ;                
      			Extr_Under_Frt_fr_Press_1st = 0 ;                   
                    
    }       


}
#endif 
  
void LDESP_Detect2WheelUnderCtrl(void)
{  
	
	lcu1US2WHControlOld = lcu1US2WHControlFlag ; 
	
/* Inhibition */	
	#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    if((VDC_DISABLE_FLG==1)||(lsespu1AHBGEN3MpresBrkOn==1)||(EXTREME_UNDER_CONTROL==1)||(ROP_REQ_ACT==1))
	#else    	
    if((VDC_DISABLE_FLG==1)||(MPRESS_BRAKE_ON==1)||(EXTREME_UNDER_CONTROL==1)||(ROP_REQ_ACT==1))
	#endif	
    {
        ldu1US2WHInhibitFlag = 1;
    }
    else
    {
    	ldu1US2WHInhibitFlag = 0;
    }        	


/* Entrance condition of Understeer 2wheel control */	

/* 11 SWD : Low-mu Entrance condition : Rear_Slip, delM_Limit */

    if ( ( SLIP_CONTROL_BRAKE_RL == 1 )||( SLIP_CONTROL_BRAKE_RR == 1 ) )   
    {
    /*  High_mu  */
    esp_tempW1 = LCESP_s16IInter4Point(vrefk, S16_REAR_HMU_SPD_L ,  (int16_t)S8_UND_2WH_ENT_SLIP_REAR_HM_LSP,
                                              S16_REAR_HMU_SPD_M1 , (int16_t)S8_UND_2WH_ENT_SLIP_REAR_HM_MSP,
                                              S16_REAR_HMU_SPD_M2 , (int16_t)S8_UND_2WH_ENT_SLIP_REAR_HM_MSP,
                                              S16_REAR_HMU_SPD_H ,  (int16_t)S8_UND_2WH_ENT_SLIP_REAR_HM_HSP );  
                                              
    /* Med mu  */                                       
    esp_tempW2 = LCESP_s16IInter4Point(vrefk, S16_REAR_MMU_SPD_L ,  (int16_t)S8_UND_2WH_ENT_SLIP_REAR_MM_LSP,
                                              S16_REAR_MMU_SPD_M1 , (int16_t)S8_UND_2WH_ENT_SLIP_REAR_MM_MSP,
                                              S16_REAR_MMU_SPD_M2 , (int16_t)S8_UND_2WH_ENT_SLIP_REAR_MM_MSP,
                                              S16_REAR_MMU_SPD_H ,  (int16_t)S8_UND_2WH_ENT_SLIP_REAR_MM_HSP );                                   
                          
    /* Low mu   */                                  
    esp_tempW3 = LCESP_s16IInter4Point(vrefk, S16_REAR_LMU_SPD_L ,  (int16_t)S8_UND_2WH_ENT_SLIP_REAR_LM_LSP,
                                              S16_REAR_LMU_SPD_M1 , (int16_t)S8_UND_2WH_ENT_SLIP_REAR_LM_MSP,
                                              S16_REAR_LMU_SPD_M2 , (int16_t)S8_UND_2WH_ENT_SLIP_REAR_LM_MSP,
                                              S16_REAR_LMU_SPD_H ,  (int16_t)S8_UND_2WH_ENT_SLIP_REAR_LM_HSP );     
                                              
    esp_tempW4  = LCESP_s16FindRoadDependatGain( esp_mu,   esp_tempW1,  esp_tempW2,  esp_tempW3 ); /* Slip */
    esp_tempW4 = esp_tempW4*100;
                                

    /*  High_mu  */
    esp_tempW5 = LCESP_s16IInter4Point(vrefk, S16_REAR_HMU_SPD_L ,  (int16_t)S8_UND_2WH_ENT_DELM_REAR_HM_LSP,
                                              S16_REAR_HMU_SPD_M1 , (int16_t)S8_UND_2WH_ENT_DELM_REAR_HM_MSP,
                                              S16_REAR_HMU_SPD_M2 , (int16_t)S8_UND_2WH_ENT_DELM_REAR_HM_MSP,
                                              S16_REAR_HMU_SPD_H ,  (int16_t)S8_UND_2WH_ENT_DELM_REAR_HM_HSP );  
                                                                             
    /* Med mu  */                                                              
    esp_tempW6 = LCESP_s16IInter4Point(vrefk, S16_REAR_MMU_SPD_L ,  (int16_t)S8_UND_2WH_ENT_DELM_REAR_MM_LSP,
                                              S16_REAR_MMU_SPD_M1 , (int16_t)S8_UND_2WH_ENT_DELM_REAR_MM_MSP,
                                              S16_REAR_MMU_SPD_M2 , (int16_t)S8_UND_2WH_ENT_DELM_REAR_MM_MSP,
                                              S16_REAR_MMU_SPD_H ,  (int16_t)S8_UND_2WH_ENT_DELM_REAR_MM_HSP );                                   
                                                                                
    /* Low mu   */                                                             
    esp_tempW7 = LCESP_s16IInter4Point(vrefk, S16_REAR_LMU_SPD_L ,  (int16_t)S8_UND_2WH_ENT_DELM_REAR_LM_LSP,
                                              S16_REAR_LMU_SPD_M1 , (int16_t)S8_UND_2WH_ENT_DELM_REAR_LM_MSP,
                                              S16_REAR_LMU_SPD_M2 , (int16_t)S8_UND_2WH_ENT_DELM_REAR_LM_MSP,
                                              S16_REAR_LMU_SPD_H ,  (int16_t)S8_UND_2WH_ENT_DELM_REAR_LM_HSP );     
                                              
    esp_tempW8  = LCESP_s16FindRoadDependatGain( esp_mu,   esp_tempW5,  esp_tempW6,  esp_tempW7 ); /* DELM Enter */
    esp_tempW8 = esp_tempW8*100;    
    
    /*  High_mu  */
    esp_tempW1 = LCESP_s16IInter4Point(vrefk, S16_REAR_HMU_SPD_L ,  (int16_t)S8_UND_2WH_EXIT_DELM_REAR_HM_LSP,
                                              S16_REAR_HMU_SPD_M1 , (int16_t)S8_UND_2WH_EXIT_DELM_REAR_HM_MSP,
                                              S16_REAR_HMU_SPD_M2 , (int16_t)S8_UND_2WH_EXIT_DELM_REAR_HM_MSP,
                                              S16_REAR_HMU_SPD_H ,  (int16_t)S8_UND_2WH_EXIT_DELM_REAR_HM_HSP );  
                                                                                        
    /* Med mu  */                                                                       
    esp_tempW2 = LCESP_s16IInter4Point(vrefk, S16_REAR_MMU_SPD_L ,  (int16_t)S8_UND_2WH_EXIT_DELM_REAR_MM_LSP,
                                              S16_REAR_MMU_SPD_M1 , (int16_t)S8_UND_2WH_EXIT_DELM_REAR_MM_MSP,
                                              S16_REAR_MMU_SPD_M2 , (int16_t)S8_UND_2WH_EXIT_DELM_REAR_MM_MSP,
                                              S16_REAR_MMU_SPD_H ,  (int16_t)S8_UND_2WH_EXIT_DELM_REAR_MM_HSP );                                   
                                                                                        
    /* Low mu   */                                                                    
    esp_tempW3 = LCESP_s16IInter4Point(vrefk, S16_REAR_LMU_SPD_L ,  (int16_t)S8_UND_2WH_EXIT_DELM_REAR_LM_LSP,
                                              S16_REAR_LMU_SPD_M1 , (int16_t)S8_UND_2WH_EXIT_DELM_REAR_LM_MSP,
                                              S16_REAR_LMU_SPD_M2 , (int16_t)S8_UND_2WH_EXIT_DELM_REAR_LM_MSP,
                                              S16_REAR_LMU_SPD_H ,  (int16_t)S8_UND_2WH_EXIT_DELM_REAR_LM_HSP );     
                                              
    esp_tempW9  = LCESP_s16FindRoadDependatGain( esp_mu,   esp_tempW1,  esp_tempW2,  esp_tempW3 ); /* DELM Exit */
    esp_tempW9 = esp_tempW9*100;        
                                
	    /*  High_mu  */
    esp_tempW1 = LCESP_s16IInter4Point(vrefk, S16_REAR_HMU_SPD_L ,  (int16_t)S8_UND_3WH_ENT_DELM_REAR_HM_LSP,
                                              S16_REAR_HMU_SPD_M1 , (int16_t)S8_UND_3WH_ENT_DELM_REAR_HM_MSP,
                                              S16_REAR_HMU_SPD_M2 , (int16_t)S8_UND_3WH_ENT_DELM_REAR_HM_MSP,
                                              S16_REAR_HMU_SPD_H ,  (int16_t)S8_UND_3WH_ENT_DELM_REAR_HM_HSP );  
                                              
    /* Med mu  */                                       
    esp_tempW2 = LCESP_s16IInter4Point(vrefk, S16_REAR_MMU_SPD_L ,  (int16_t)S8_UND_3WH_ENT_DELM_REAR_MM_LSP,
                                              S16_REAR_MMU_SPD_M1 , (int16_t)S8_UND_3WH_ENT_DELM_REAR_MM_MSP,
                                              S16_REAR_MMU_SPD_M2 , (int16_t)S8_UND_3WH_ENT_DELM_REAR_MM_MSP,
                                              S16_REAR_MMU_SPD_H ,  (int16_t)S8_UND_3WH_ENT_DELM_REAR_MM_HSP );                                   
                          
    /* Low mu   */                                  
    esp_tempW3 = LCESP_s16IInter4Point(vrefk, S16_REAR_LMU_SPD_L ,  (int16_t)S8_UND_3WH_ENT_DELM_REAR_LM_LSP,
                                              S16_REAR_LMU_SPD_M1 , (int16_t)S8_UND_3WH_ENT_DELM_REAR_LM_MSP,
                                              S16_REAR_LMU_SPD_M2 , (int16_t)S8_UND_3WH_ENT_DELM_REAR_LM_MSP,
                                              S16_REAR_LMU_SPD_H ,  (int16_t)S8_UND_3WH_ENT_DELM_REAR_LM_HSP );     
                                              
    esp_tempW5 = LCESP_s16FindRoadDependatGain( esp_mu,   esp_tempW1,  esp_tempW2,  esp_tempW3 ); /* 3WH ENTER DEL_M */
    esp_tempW5 = esp_tempW5*100;
    
    esp_tempW1 = LCESP_s16IInter4Point(vrefk, S16_REAR_HMU_SPD_L ,  (int16_t)S8_UND_3WH_EXIT_DELM_REAR_HM_LSP,
                                              S16_REAR_HMU_SPD_M1 , (int16_t)S8_UND_3WH_EXIT_DELM_REAR_HM_MSP,
                                              S16_REAR_HMU_SPD_M2 , (int16_t)S8_UND_3WH_EXIT_DELM_REAR_HM_MSP,
                                              S16_REAR_HMU_SPD_H ,  (int16_t)S8_UND_3WH_EXIT_DELM_REAR_HM_HSP );  
                                              
    /* Med mu  */                                       
    esp_tempW2 = LCESP_s16IInter4Point(vrefk, S16_REAR_MMU_SPD_L ,  (int16_t)S8_UND_3WH_EXIT_DELM_REAR_MM_LSP,
                                              S16_REAR_MMU_SPD_M1 , (int16_t)S8_UND_3WH_EXIT_DELM_REAR_MM_MSP,
                                              S16_REAR_MMU_SPD_M2 , (int16_t)S8_UND_3WH_EXIT_DELM_REAR_MM_MSP,
                                              S16_REAR_MMU_SPD_H ,  (int16_t)S8_UND_3WH_EXIT_DELM_REAR_MM_HSP );                                   
                          
    /* Low mu   */                                  
    esp_tempW3 = LCESP_s16IInter4Point(vrefk, S16_REAR_LMU_SPD_L ,  (int16_t)S8_UND_3WH_EXIT_DELM_REAR_LM_LSP,
                                              S16_REAR_LMU_SPD_M1 , (int16_t)S8_UND_3WH_EXIT_DELM_REAR_LM_MSP,
                                              S16_REAR_LMU_SPD_M2 , (int16_t)S8_UND_3WH_EXIT_DELM_REAR_LM_MSP,
                                              S16_REAR_LMU_SPD_H ,  (int16_t)S8_UND_3WH_EXIT_DELM_REAR_LM_HSP );     
                                              
    esp_tempW6 = LCESP_s16FindRoadDependatGain( esp_mu,   esp_tempW1,  esp_tempW2,  esp_tempW3 ); /* 3WH ENTER DEL_M */
    esp_tempW6 = esp_tempW6*100;
                            
    }
    else
    {
      ;
    }

                            
    if( ( ldu1US2WHInhibitFlag == 0 ) 
     	&&( lcu1US2WHControlFlag == 0 ) 
        &&( vrefk < S16_MAX_UND_VEL_EXIT )  			/* Extreme US TP */         
        &&( vrefk > S16_EXT_UND_VEL_ENTER )  			/* Extreme US TP */ 
        &&( McrAbs(delta_moment_q_os) < 900  )      
        
#if !__SPLIT_TYPE    	/* X-split */
        &&( (( SLIP_CONTROL_BRAKE_RL == 1 )&&( yaw_sign >= 0 )&&( slip_m_rl < -esp_tempW4 )/*&&(BTCS_rr==0)&&(BTCS_fl==0)*/) 
            ||(( SLIP_CONTROL_BRAKE_RR == 1 )&&( yaw_sign < 0 )&&( slip_m_rr < -esp_tempW4 )/*&&(BTCS_rl==0)&&(BTCS_fr==0)*/))            
#else   				/* FR-split */
        &&( (( SLIP_CONTROL_BRAKE_RL == 1 )&&( yaw_sign >= 0 )&&( slip_m_rl < -esp_tempW4 )&&(BTCS_fr==0)&&(BTCS_fl==0)) 
            ||(( SLIP_CONTROL_BRAKE_RR == 1 )&&( yaw_sign < 0 )&&( slip_m_rr < -esp_tempW4 )&&(BTCS_fr==0)&&(BTCS_fl==0)) )
#endif      
        #if (__TCMF_UNDERSTEER_CONTROL == ENABLE)
        &&((lcescu1TCMFUnderControlRr == 1)||(lcescu1TCMFUnderControlRl == 1))      
        #endif
        &&(delta_moment_q_under<-esp_tempW8) 
        )
        
    {  
        lcu1US2WHControlFlag = 1;
        
          #if __TVBB
        if ((lctvbbu1TVBB_ON == 1) && (ldtvbbu1FrontWhlCtl == 1))
        {
            lcu8US2WH_count = U8_UND_2WH_USC_INITIAL_FULL_TIME + 1;
        }
        else
        {
          #endif    
            lcu8US2WH_count = 1;
          #if __TVBB    
        }
          #endif    
        
        if( SLIP_CONTROL_BRAKE_RL == 1 )
        {
            lcu1US2WHControlFlag_RL = 1 ;
            lcu1US2WHControlFlag_RR = 0 ;
        }
        else if ( SLIP_CONTROL_BRAKE_RR == 1 )
        { 
            lcu1US2WHControlFlag_RL = 0 ;
            lcu1US2WHControlFlag_RR = 1 ;
        }
        else
        {  
        	;
        }    
    }
    else if ( ( ldu1US2WHInhibitFlag == 0 ) 
    	  &&( lcu1US2WHControlFlag == 1 ) 
        &&( vrefk < S16_MAX_UND_VEL_EXIT )  			/* Extreme US TP */    
        &&( vrefk > S16_EXT_UND_VEL_EXIT )  			/* Extreme US TP */  
        &&( McrAbs(delta_moment_q_os) < 900 )    
        
#if !__SPLIT_TYPE    	/* X-split */
        &&( (( SLIP_CONTROL_BRAKE_RL == 1 )&&( yaw_sign >= 0 )/*&&(BTCS_rr==0)&&(BTCS_fl==0)*/) 
            ||(( SLIP_CONTROL_BRAKE_RR == 1 )&&( yaw_sign < 0 )/*&&(BTCS_rl==0)&&(BTCS_fr==0)) */))          
#else   				/* FR-split */
        &&( (( SLIP_CONTROL_BRAKE_RL == 1 )&&( yaw_sign >= 0 )&&(BTCS_fr==0)&&(BTCS_fl==0)) 
            ||(( SLIP_CONTROL_BRAKE_RR == 1 )&&( yaw_sign < 0 )&&(BTCS_fr==0)&&(BTCS_fl==0)) )
#endif      
         
        &&(delta_moment_q_under<-esp_tempW9) 
        )
    {
        lcu1US2WHControlFlag = 1;
        
        if (lcu8US2WH_count < L_U8_TIME_10MSLOOP_2S)
        {
            lcu8US2WH_count = lcu8US2WH_count + 1;
        }
        else
        {
            lcu8US2WH_count = L_U8_TIME_10MSLOOP_2S;
        }
        
        if( SLIP_CONTROL_BRAKE_RL == 1 )
        {
            lcu1US2WHControlFlag_RL = 1 ;
            lcu1US2WHControlFlag_RR = 0 ;
        }
        else if ( SLIP_CONTROL_BRAKE_RR == 1 )
        { 
            lcu1US2WHControlFlag_RL = 0 ;
            lcu1US2WHControlFlag_RR = 1 ;
        }
        else
        {  
        	;
        } 
        
        if ((lcu1US3WHControlFlag == 0)
          &&(delta_moment_q_under < -esp_tempW5))
        {
//            lcu1US3WHControlFlag = 1;
             lcu1US3WHControlFlag = 0;
        }
        else if ((lcu1US3WHControlFlag == 1)
               &&(delta_moment_q_under < -esp_tempW6))
        {
 //           lcu1US3WHControlFlag = 1;
  			lcu1US3WHControlFlag = 0;
        }
        else
        {
            lcu1US3WHControlFlag = 0;
        }
    }
    else
    {
    		lcu1US2WHControlFlag = 0;
    		lcu8US2WH_count = 0;
            lcu1US2WHControlFlag_RL = 0 ;
            lcu1US2WHControlFlag_RR = 0 ;
            lcu1US3WHControlFlag = 0;
    }  

/* Fade-out */

    esp_tempW5  = LCESP_s16FindRoadDependatGain( esp_mu, S8_UND_2WH_FRT_FADE_OUT_HM,  S8_UND_2WH_FRT_FADE_OUT_MM,  S8_UND_2WH_FRT_FADE_OUT_LM ); /* Fade_out time */

    if((ldu1US2WHInhibitFlag==0) && (lcu1US2WHControlOld==1)&&(lcu1US2WHControlFlag==0)&&(McrAbs(delta_moment_q_os) < 400)
        &&(lcu1US2WHFadeout_RL==0)&&(lcu1US2WHFadeout_RR==0)
      &&(((ESP_BRAKE_CONTROL_RL==1)||(ESP_BRAKE_CONTROL_RR==1))&&(lcs16US2WH_FadeCount<=esp_tempW5)))          
    {  
        if( yaw_sign >= 0 )
        {
            lcu1US2WHFadeout_RL = 1 ;
            lcu1US2WHFadeout_RR = 0 ;
        }
        else
        { 
            lcu1US2WHFadeout_RL = 0 ;
            lcu1US2WHFadeout_RR = 1 ;
        }
        
        lcs16US2WH_FadeCount = lcs16US2WH_FadeCount + 1 ;                 
    }
    else if((ldu1US2WHInhibitFlag==0)&&(lcu1US2WHControlFlag==0)&&(McrAbs(delta_moment_q_os) < 400)
        &&((lcu1US2WHFadeout_RL==1)||(lcu1US2WHFadeout_RR==1))
      &&(((ESP_BRAKE_CONTROL_RL==1)||(ESP_BRAKE_CONTROL_RR==1))&&(lcs16US2WH_FadeCount<=esp_tempW5)))  
	{
        if( yaw_sign >= 0 )
        {
            lcu1US2WHFadeout_RL = 1 ;
            lcu1US2WHFadeout_RR = 0 ;
        }
        else
        { 
            lcu1US2WHFadeout_RL = 0 ;
            lcu1US2WHFadeout_RR = 1 ;
        }
        
        lcs16US2WH_FadeCount = lcs16US2WH_FadeCount + 1 ;
        
        if (lcs16US2WH_FadeCount>1000)
        {  
        	lcs16US2WH_FadeCount = 1000 ;
        }   
	}
    else
    {
    	    lcs16US2WH_FadeCount = 0;
            lcu1US2WHFadeout_RL = 0 ;
            lcu1US2WHFadeout_RR = 0 ;       
    }  

/*  Frt DelM calculation */

    if( lcu1US2WHControlFlag ==1 )
    {   
	    /*  High_mu  */
	    esp_tempW5 = LCESP_s16IInter4Point(vrefk, S16_REAR_HMU_SPD_L ,  (int16_t)S8_UND_2WH_FRT_DELM_LMT_HM_LSP,
	                                              S16_REAR_HMU_SPD_M1 , (int16_t)S8_UND_2WH_FRT_DELM_LMT_HM_MSP,
	                                              S16_REAR_HMU_SPD_M2 , (int16_t)S8_UND_2WH_FRT_DELM_LMT_HM_MSP,
	                                              S16_REAR_HMU_SPD_H ,  (int16_t)S8_UND_2WH_FRT_DELM_LMT_HM_HSP );  
	                                              
	    /* Med mu  */                                       
	    esp_tempW6 = LCESP_s16IInter4Point(vrefk, S16_REAR_MMU_SPD_L ,  (int16_t)S8_UND_2WH_FRT_DELM_LMT_MM_LSP,
	                                              S16_REAR_MMU_SPD_M1 , (int16_t)S8_UND_2WH_FRT_DELM_LMT_MM_MSP,
	                                              S16_REAR_MMU_SPD_M2 , (int16_t)S8_UND_2WH_FRT_DELM_LMT_MM_MSP,
	                                              S16_REAR_MMU_SPD_H ,  (int16_t)S8_UND_2WH_FRT_DELM_LMT_MM_HSP );                                   
	                          
	    /* Low mu   */                                  
	    esp_tempW7 = LCESP_s16IInter4Point(vrefk, S16_REAR_LMU_SPD_L ,  (int16_t)S8_UND_2WH_FRT_DELM_LMT_LM_LSP,
	                                              S16_REAR_LMU_SPD_M1 , (int16_t)S8_UND_2WH_FRT_DELM_LMT_LM_MSP,
	                                              S16_REAR_LMU_SPD_M2 , (int16_t)S8_UND_2WH_FRT_DELM_LMT_LM_MSP,
	                                              S16_REAR_LMU_SPD_H ,  (int16_t)S8_UND_2WH_FRT_DELM_LMT_LM_HSP );     
	 
	    esp_tempW8  = LCESP_s16FindRoadDependatGain( esp_mu,   esp_tempW5,  esp_tempW6,  esp_tempW7 );   /* DelM_Lmt */       
	    esp_tempW8 = esp_tempW8*100;   		
		
		lcs16US2WH_FrtDelM = McrAbs(delta_moment_q_under) ;            	
           	
		if ( lcs16US2WH_FrtDelM > esp_tempW8 )
        {
            lcs16US2WH_FrtDelM = esp_tempW8 ;
        }
        else
        {
            ;
        }     
    }
    else
    {
    	lcs16US2WH_FrtDelM = 0 ; 
    }        
}   
  
#if (__CRAM == 1)
	 
void LDESP_vDetectCRAM(void)
{ 
    /************** Calculation of Del_V **************/  
     
    /* ay=0.1g  */
    
    esp_tempW3 = vrefm;
    
    if( esp_tempW3 <= 83 )
    {
    	esp_tempW3 = 83;
    }
    else
    {
    	;
    }
 
    esp_tempW4 = (int16_t)( (((int32_t)LAT_0G1G)*562)/esp_tempW3 );    /* Threshold at ay = 0.1 g */
 
    if ( McrAbs(rf) < esp_tempW4 ) 
    {
        esp_tempW0 = esp_tempW4  ;
    }
    else
    {
        esp_tempW0 = McrAbs(rf) ;
    }
                       
    esp_tempW1 = McrAbs(yaw_out) ;                        
                          
    esp_tempW2 = (int16_t)( (((int32_t)esp_tempW1)*vrefk)/esp_tempW0 );    /* Target Velocity */          
   
    if ( McrAbs(rf) < esp_tempW1 ) 
    {
        esp_tempW2 = vrefk  ;
    }
/*    else if ( McrAbs(det_alatm) < S16_CRAM_DELV_CALC_ALAT ) */
    else if ( McrAbs(alat) < S16_CRAM_DELV_CALC_ALAT ) 
    {
        esp_tempW2 = vrefk  ;
    }        
    else
    {
         ;
    } 
    
    lds16espVelcram_old = lds16espVelcram ;
    
     
    lds16espVelcram = LCESP_s16Lpf1Int( esp_tempW2, lds16espVelcram_old, L_U8FILTER_GAIN_10MSLOOP_1_5HZ);     

/*if ( LOOP_TIME_20MS_TASK_1 == 1 ) 
{ */    
    esp_tempW6 = lds16espVelcram - vrefk ;

    esp_tempW5 = LCESP_s16IInter4Point(vrefk, S16_D_GAIN_V_LOW_SPD , S16_CRAM_DELV_V_LOW_WEG_R,
                                              S16_D_GAIN_LOW_SPD ,   S16_CRAM_DELV_LOW_WEG_R,
                                              S16_D_GAIN_MED_SPD,    S16_CRAM_DELV_MED_WEG_R,
                                              S16_D_GAIN_HIGH_SPD,   S16_CRAM_DELV_HIGH_WEG_R );
                                               
    lds16espDelVcram = (int16_t)( (((int32_t)esp_tempW5)*esp_tempW6)/100 );                                                        
/*}*/
    
    if ( lds16espDelVcram > 0 ) 
    {
        lds16espDelVcram = 0  ;
    }      
    else
    {
         ;
    }    
    
    /************** Limit Understeer **************/     
    
    if ( ( ldu1espDetectUnderLimit == 0 ) 
				#if(__AHB_GEN3_SYSTEM==ENABLE) || (__IDB_LOGIC == ENABLE)
         && ( lsespu1AHBGEN3MpresBrkOn == 0 )    	
				#else
         && ( MPRESS_BRAKE_ON == 0 ) 
				#endif
         && ( ABS_fz == 0 ) 
         && ( YAW_CHANGE_LIMIT_ACT == 1 ) 
         && ( det_alatm >= LAT_0G55G )       /* understeer limit  */            
         && ( delta_yaw >= S16_CRAM_LMT_ENT_DELYAW ))
/*         && ( delta_yaw_first  < u_delta_yaw_thres ) )          */
    {   
        ldu1espDetectUnderLimit = 1 ;  
    }
    else if ( ( ldu1espDetectUnderLimit == 1 )  
				#if(__AHB_GEN3_SYSTEM==ENABLE) || (__IDB_LOGIC == ENABLE)
         && ( lsespu1AHBGEN3MpresBrkOn == 0 )    	
				#else
         && ( MPRESS_BRAKE_ON == 0 ) 
				#endif
         && ( ABS_fz == 0 ) 
         && ( YAW_CHANGE_LIMIT_ACT == 1 ) 
         && ( det_alatm >= LAT_0G5G )       /* understeer limit  */   
         && ( delta_yaw >= S16_CRAM_LMT_EXT_DELYAW ))
/*         && ( delta_yaw_first  < u_delta_yaw_thres ) ) */
    {
        ldu1espDetectUnderLimit = 1 ;  
    }
    else
    {   
        ldu1espDetectUnderLimit = 0 ;     
    }   
      
    /************** Enter Condition **************/    
        
    if ( (ldu1espDetectCRAM==0) 
         && (VDC_DISABLE_FLG==0) 
/*         && ( SLIP_CONTROL_BRAKE_FL == 0 )  
         && ( SLIP_CONTROL_BRAKE_FR == 0 )  
         && (ROP_REQ_ACT==1)                 
         && (ROP_DYN_STABLE_PHASE==1)         */
         && (mtp<=S16_CRAM_ENTER_MTP)  
         && (det_alatm  >= S16_CRAM_ENTER_ALATM )  
         && (vrefk > S16_CRAM_ENTER_SPD)
         && (lds16espDelVcram <= S16_CRAM_ENTER_DELV)
         && (ldu1espDetectUnderLimit==1) ) 
    { 
    		ldu1espDetectCRAM = 1 ;  
    		lcs16espCramCount = lcs16espCramCount + 1; 
    		  
    #if (__CRAM_FRONT_2WH_ENABLE == 1) 
    					
    					if(McrAbs(alat) > LAT_0G2G)
    					{
                if (yaw_out>0)
                {  
        		    ldu1espDetectCRAM_FL = 1 ; 
        		    ldu1espDetectCRAM_FR = 0 ;
                    
                } 
                else 
                {
        		    ldu1espDetectCRAM_FL = 0 ; 
        		    ldu1espDetectCRAM_FR = 1 ; 
                                
                }      
              }
              else
              {
              	;
              }
               
    #else  
    
        		    ldu1espDetectCRAM_FL = 0 ; 
        		    ldu1espDetectCRAM_FR = 0 ;    
    #endif    		   
            
    } 
    else if ( (ldu1espDetectCRAM==1)  
         && (VDC_DISABLE_FLG==0)  
         && (det_alatm  >= S16_CRAM_EXIT_ALATM) /*&& (det_alatm  >= LAT_0G7G )            */
/*       && (ldu1espCramDynStabPhase==1)          */
         && (mtp<=(S16_CRAM_ENTER_MTP+MTP_1_P))  
         && (vrefk > S16_CRAM_EXIT_SPD)
         && (lds16espDelVcram <= S16_CRAM_EXIT_DELV)
         && (ldu1espDetectUnderLimit==1) )
    {
        	ldu1espDetectCRAM = 1 ;     
        	lcs16espCramCount = lcs16espCramCount + 1;
					
    #if (__CRAM_FRONT_2WH_ENABLE == 1)
    
    					if(McrAbs(alat) > LAT_0G15G)
    					{
                if (yaw_out>0)
                {  
        		    ldu1espDetectCRAM_FL = 1 ; 
        		    ldu1espDetectCRAM_FR = 0 ;
                    
                } 
                else 
                {
        		    ldu1espDetectCRAM_FL = 0 ; 
        		    ldu1espDetectCRAM_FR = 1 ; 
                                
                }      
              }
              else
              {
              	;
              }
    #else  
    
        		    ldu1espDetectCRAM_FL = 0 ; 
        		    ldu1espDetectCRAM_FR = 0 ;    
    #endif   		
        	 
    }    
    else 
    {
        	ldu1espDetectCRAM = 0 ;  

					lcs16espCramCount = 0;
        		    ldu1espDetectCRAM_FL = 0 ; 
        		    ldu1espDetectCRAM_FR = 0 ;    
        		    
    }    
  
    #if (__EUROPE_ADAC ==1)   
    ldu1espDetectCRAM = 0 ;  
    lcs16espCramCount = 0;
    ldu1espDetectCRAM_FL = 0 ; 
    ldu1espDetectCRAM_FR = 0 ;  
    #endif

}
 
#if (__CRAM==1) && (__CRAM_PRE_FRONT_W_CRTL == 1) 
void LDESP_vCheckPreCRAM(void)
{	
    
    int16_t prefill_wstr_engage;   
    /* Pre-act enter condition: wstr_dot */               
    esp_tempW11 = LCESP_s16IInter7Point( vrefk, VREF_K_40_KPH,  S16_EHC_WSTR_DOT_ENT_40K,
					                            VREF_K_50_KPH,  S16_EHC_WSTR_DOT_ENT_50K,    
					                            VREF_K_60_KPH,  S16_EHC_WSTR_DOT_ENT_60K,
					                            VREF_K_70_KPH,  S16_EHC_WSTR_DOT_ENT_70K,					                            
					                            VREF_K_80_KPH,  S16_EHC_WSTR_DOT_ENT_80K, 
					                            VREF_K_100_KPH, S16_EHC_WSTR_DOT_ENT_100K, 
					                            VREF_K_120_KPH, S16_EHC_WSTR_DOT_ENT_120K ) ;	 
    /* Pre-act exit condition: wstr_dot */                   
    esp_tempW12 = LCESP_s16IInter7Point( vrefk, VREF_K_40_KPH,  S16_EHC_WSTR_DOT_EXT_40K,
					                            VREF_K_50_KPH,  S16_EHC_WSTR_DOT_EXT_50K,    
					                            VREF_K_60_KPH,  S16_EHC_WSTR_DOT_EXT_60K,
					                            VREF_K_70_KPH,  S16_EHC_WSTR_DOT_EXT_70K,					                            
					                            VREF_K_80_KPH,  S16_EHC_WSTR_DOT_EXT_80K, 
					                            VREF_K_100_KPH, S16_EHC_WSTR_DOT_EXT_100K,
					                            VREF_K_120_KPH, S16_EHC_WSTR_DOT_EXT_120K ) ;	 
    /* Pre-act enter condition: yaw_dot */					                                            
    esp_tempW13 = LCESP_s16IInter7Point( vrefk, VREF_K_40_KPH,  S16_EHC_YAW_DOT_ENT_40K,
					                            VREF_K_50_KPH,  S16_EHC_YAW_DOT_ENT_50K,    
					                            VREF_K_60_KPH,  S16_EHC_YAW_DOT_ENT_60K,
					                            VREF_K_70_KPH,  S16_EHC_YAW_DOT_ENT_70K,					                            
					                            VREF_K_80_KPH,  S16_EHC_YAW_DOT_ENT_80K, 
					                            VREF_K_100_KPH, S16_EHC_YAW_DOT_ENT_100K, 
					                            VREF_K_120_KPH, S16_EHC_YAW_DOT_ENT_120K) ;	 
    /* Pre-act exit condition: yaw_dot */        
    esp_tempW14 = LCESP_s16IInter7Point( vrefk, VREF_K_40_KPH,  S16_EHC_YAW_DOT_EXT_40K,
					                            VREF_K_50_KPH,  S16_EHC_YAW_DOT_EXT_50K,    
					                            VREF_K_60_KPH,  S16_EHC_YAW_DOT_EXT_60K,
					                            VREF_K_70_KPH,  S16_EHC_YAW_DOT_EXT_70K,					                            
					                            VREF_K_80_KPH,  S16_EHC_YAW_DOT_EXT_80K,
					                            VREF_K_100_KPH, S16_EHC_YAW_DOT_EXT_100K, 
					                            VREF_K_120_KPH, S16_EHC_YAW_DOT_EXT_120K ) ;					                                     					                                     						                      
#if (__EUROPE_ADAC ==1)
    esp_tempW9 = LCESP_s16IInter5Point( vrefk,  VREF_K_40_KPH,  LAT_0G3G,
					                            VREF_K_60_KPH,  LAT_0G2G,    
					                            VREF_K_80_KPH,   LAT_0G2G,
					                            VREF_K_100_KPH,  LAT_0G2G,					                            
					                            VREF_K_120_KPH,  LAT_0G2G ) ; 
#endif				                                            
				                                                               
    esp_tempW1 = LCESP_s16IInter2Point( vrefk,  S16_ROP_TTP_VEL_SLOW,  S16_ROP_TTP_SLOW_VEL_DELAY, 	 
					                            S16_ROP_TTP_VEL_FAST,  S16_ROP_TTP_FAST_VEL_DELAY ) ;	      
					                            
    esp_tempW2 = LCESP_s16IInter2Point( McrAbs(wstr_dot),  S16_ROP_TTP_WSTR_DOT_SLOW, S16_ROP_TTP_SLOW_WSTR_DOT_DELAY, 	 
					                                    S16_ROP_TTP_WSTR_DOT_FAST, S16_ROP_TTP_FAST_WSTR_DOT_DELAY ) ;						                                         
    
    time_to_cram_preact = esp_tempW1 + esp_tempW2 ;               
                  
	#if (__ROP == 0)                   
    alat_dot2_lf_old = alat_dot2_lf ;  
    
    /* tempW2 = alat - alatold ; tempW1 = 1000; tempW0 = 7 ;     
    s16muls16divs16();
    alat_dot2 = tempW3;      */   
    alat_dot2 = (int16_t)( (((int32_t)(alat - alatold))*1000)/10 ) ; 	 /* r: 0.001 [g/sec]   */
    
    alat_dot2_lf = LCESP_s16Lpf1Int(alat_dot2, alat_dot2_lf_old, 4);    
  #endif
    
/***********  FL-Pre_wheel  *****************/
        
#if (__EUROPE_ADAC ==1)
    esp_tempW7 = LCESP_s16IInter5Point( vrefk,  S16_EHC_PEAKED_SPEED_V1,   S16_EHC_PEAKED_YAW_V1, 
    										    S16_EHC_PEAKED_SPEED_V2,   S16_EHC_PEAKED_YAW_V2,    
					                            S16_EHC_PEAKED_SPEED_V3,   S16_EHC_PEAKED_YAW_V3,
					                            S16_EHC_PEAKED_SPEED_V4,   S16_EHC_PEAKED_YAW_V4,					                            
					                            S16_EHC_PEAKED_SPEED_V5,   S16_EHC_PEAKED_YAW_V5 ) ; 
					                            
	esp_tempW8 = LCESP_s16IInter5Point( vrefk,  S16_EHC_PEAKED_SPEED_V1,   S16_EHC_PEAKED_WSTR_V1, 
    											S16_EHC_PEAKED_SPEED_V2,   S16_EHC_PEAKED_WSTR_V2,    
					                            S16_EHC_PEAKED_SPEED_V3,   S16_EHC_PEAKED_WSTR_V3,
					                            S16_EHC_PEAKED_SPEED_V4,   S16_EHC_PEAKED_WSTR_V4,					                            
					                            S16_EHC_PEAKED_SPEED_V5,   S16_EHC_PEAKED_WSTR_V5 ) ; 

/**************  FL-Pre_wheel  *****************/

esp_tempW10 = LCESP_s16IInter2Point( vrefk,  S16_EHC_2ND_ENGAGE_SPEED_V1,   500, 
    										 S16_EHC_2ND_ENGAGE_SPEED_V2,   300); /* STEER*/
    										 
esp_tempW6 = LCESP_s16IInter2Point( vrefk,  S16_EHC_2ND_ENGAGE_SPEED_V1,   S16_EHC_2ND_ENGAGE_V1_YAW, 
    										S16_EHC_2ND_ENGAGE_SPEED_V2,   S16_EHC_2ND_ENGAGE_V2_YAW); /* YAW*/
    										
prefill_wstr_engage = LCESP_s16IInter2Point( vrefk,  S16_EHC_2ND_ENGAGE_SPEED_V1,   S16_EHC_2ND_ENGAGE_V1_WSTR, 
    										         S16_EHC_2ND_ENGAGE_SPEED_V2,   S16_EHC_2ND_ENGAGE_V2_WSTR); /* STEER*/    										 
/*prefill_wstr=  LCESP_s16IInter2Point( vrefk,  600,   0, 800,   50);*/
    										
	if((ldu1espDetSteerPeak_FL==0)&&(yaw_out > esp_tempW6)&&(wstr_dot > 20)&&(wstr > esp_tempW8))
	{
		ldu1espDetSteerPeak_FL = 1;
	}
	else if((ldu1espDetSteerPeak_FL==1)&&(yaw_out > 0))
	{
		ldu1espDetSteerPeak_FL = 1;	
	}
	else
	{
		ldu1espDetSteerPeak_FL = 0;
	}
	if((ldu1espDetYawPeak_FL==0)&&(yaw_out_dot2 > 10)&&(yaw_out > esp_tempW7)&&(wstr > 100))
	{
		ldu1espDetYawPeak_FL = 1;
	}
	else if((ldu1espDetYawPeak_FL==1)&&(yaw_out > 500))
	{
		ldu1espDetYawPeak_FL = 1;	
	}
	else
	{
		ldu1espDetYawPeak_FL = 0;
	}		
        
 #endif
        
#if (__EUROPE_ADAC ==1)
    if  ((ldu1espDetSteerPeak_FL==1)&&(ldu1espWstrStartCRAM_FL== 0) && (wstr>esp_tempW10) && (wstr_dot < -esp_tempW11) )   
#else               
    if  ( (ldu1espWstrStartCRAM_FL== 0) && (wstr>500) && (wstr_dot < -esp_tempW11) )
#endif
    {
        ldu1espWstrStartCRAM_FL = 1; 
    }
    else if ( ((ldu1espWstrStartCRAM_FL==1) && (wstr_dot < -esp_tempW12)) 
            || ((ldu1espWstrStartCRAM_FL==1) && (wstr<-1800)) )
    {            
        ldu1espWstrStartCRAM_FL = 1; 
    }
    else 
    {
        ldu1espWstrStartCRAM_FL = 0; 
    }
         
#if (__EUROPE_ADAC ==1)         
    if((ldu1espDetYawPeak_FL==1)&&(ldu1espWstrStartCRAM_FL==1) && (ldu1espAyStartCRAM_FL==0) && (det_alatm > esp_tempW9)
         && ( (( alat>100 ) && (alat_dot2_lf < -S16_ROP_AY_DOT_TH_1st)) || (( yaw_out>100 ) && (yaw_out_dot2 < -esp_tempW13)) ) )    
#else
    if( (ldu1espWstrStartCRAM_FL==1) && (ldu1espAyStartCRAM_FL==0) && (det_alatm > S16_CRAM_DELV_CALC_ALAT)
         && ( (( alat>100 ) && (alat_dot2_lf < -S16_ROP_AY_DOT_TH_1st)) || (( yaw_out>100 ) && (yaw_out_dot2 < -esp_tempW13)) ) )    
#endif
    {            
        ldu1espAyStartCRAM_FL = 1; 
    }
    else if ( (ldu1espWstrStartCRAM_FL == 1) &&  (ldu1espAyStartCRAM_FL == 1)
                && ( (alat_dot2_lf < -S16_ROP_AY_DOT_TH_2nd)||(yaw_out_dot2 < -esp_tempW14) ) )
    {    
        ldu1espAyStartCRAM_FL = 1; 
    }
    else 
    {    
        ldu1espAyStartCRAM_FL = 0; 
    }  
    
 #if (__EUROPE_ADAC ==1)
    if ( (ESP_BRAKE_CONTROL_FR==1)&&(lcu1espCramPreact_FL==0) && (ldu1espAyStartCRAM_FL==1) &&( wstr<prefill_wstr_engage ) && (wstr_dot < -esp_tempW11)&& (mtp<=S16_CRAM_ENTER_MTP))
#else
    if ( (lcu1espCramPreact_FL==0) && (ldu1espAyStartCRAM_FL==1) && ( wstr<-50 ) && (wstr_dot < -esp_tempW11) && (mtp<=S16_CRAM_ENTER_MTP))
 #endif
    {
        cram_ttp_cnt_fl = cram_ttp_cnt_fl + 1;
        
        if ( cram_ttp_cnt_fl > 100 )
        {
            cram_ttp_cnt_fl = 100 ; 
        }
        else
        {
           ;
        }
        
        if ( cram_ttp_cnt_fl > time_to_cram_preact )
        {
            lcu1espCramPreact_FL = 1; 
        }
        else
        {
           ;
        }
    }
    else if ( (lcu1espCramPreact_FL==1) && (ldu1espAyStartCRAM_FL==1) && (wstr_dot < -esp_tempW12) && (mtp<=(S16_CRAM_ENTER_MTP+MTP_1_P)) )
    {
        lcu1espCramPreact_FL = 1;        
        cram_ttp_cnt_fl = 0 ;         
    }
    else
    {
        lcu1espCramPreact_FL = 0;
        cram_ttp_cnt_fl = 0 ;  
    }
    
#if (__EUROPE_ADAC ==1)       
    if((lcu1espCramPreact_FL==1)&&(nosign_delta_yaw <= sign_o_delta_yaw_thres))
    {
    	 lcu1espCramPreact_FL = 0;        
    }
    else
    {
    	;  
    }
 #endif   
/***********  FR-Pre_wheel  *****************/
 #if (__EUROPE_ADAC ==1)      
  	if((ldu1espDetSteerPeak_FR==0)&&(yaw_out < -esp_tempW6)&&(wstr_dot< -20)&&(wstr < -esp_tempW8))
	{
		ldu1espDetSteerPeak_FR = 1;
	}
	else if((ldu1espDetSteerPeak_FR==1)&&(yaw_out < 0))
	{
		ldu1espDetSteerPeak_FR = 1;	
	}
	else
	{
		ldu1espDetSteerPeak_FR = 0;
	}
	
	if((ldu1espDetYawPeak_FR == 0)&& (yaw_out_dot2 < (-10))&& (yaw_out < (-esp_tempW7)) && (wstr < (-100)))
	{
		ldu1espDetYawPeak_FR = 1;
	}
	else if((ldu1espDetYawPeak_FR==1)&&(yaw_out < (-500)))
	{
		ldu1espDetYawPeak_FR = 1;
	}
	else
	{
		ldu1espDetYawPeak_FR = 0;
	}
#endif
    
#if (__EUROPE_ADAC ==1)   		  
    if  ( (ldu1espDetSteerPeak_FR==1)&&(ldu1espWstrStartCRAM_FR== 0) && (wstr<-esp_tempW10) && (wstr_dot > esp_tempW11) )
 #else
    if  ( (ldu1espWstrStartCRAM_FR== 0) && (wstr<-500) && (wstr_dot > esp_tempW11) )
 #endif
    {
        ldu1espWstrStartCRAM_FR = 1; 
    }
    else if ( ((ldu1espWstrStartCRAM_FR==1) && (wstr_dot > esp_tempW12)) 
            || ((ldu1espWstrStartCRAM_FR==1) && (wstr>1800)) )
    {            
        ldu1espWstrStartCRAM_FR = 1; 
    }
    else 
    {
        ldu1espWstrStartCRAM_FR = 0; 
    }
         
#if (__EUROPE_ADAC ==1)         
    if((ldu1espDetYawPeak_FR==1)&& (ldu1espWstrStartCRAM_FR==1) && (ldu1espAyStartCRAM_FR==0) && (det_alatm > esp_tempW9)
         && ( (( alat<-100 ) && (alat_dot2_lf > S16_ROP_AY_DOT_TH_1st)) || (( yaw_out<-100 ) && (yaw_out_dot2 > esp_tempW13)) ) )    
#else
    if( (ldu1espWstrStartCRAM_FR==1) && (ldu1espAyStartCRAM_FR==0) && (det_alatm > S16_CRAM_DELV_CALC_ALAT)
         && ( (( alat<-100 ) && (alat_dot2_lf > S16_ROP_AY_DOT_TH_1st)) || (( yaw_out<-100 ) && (yaw_out_dot2 > esp_tempW13)) ) )    
#endif
    {   
        ldu1espAyStartCRAM_FR = 1; 
    }
    else if ( (ldu1espWstrStartCRAM_FR == 1) &&  (ldu1espAyStartCRAM_FR == 1) 
                && ( (alat_dot2_lf > S16_ROP_AY_DOT_TH_2nd)||(yaw_out_dot2 > esp_tempW14) ) )   
    {    
        ldu1espAyStartCRAM_FR = 1; 
    }
    else 
    {    
        ldu1espAyStartCRAM_FR = 0; 
    }  
    
#if (__EUROPE_ADAC ==1)     
    if ( (ESP_BRAKE_CONTROL_FL==1)&&(lcu1espCramPreact_FR==0) && (ldu1espAyStartCRAM_FR==1) && ( wstr>-prefill_wstr_engage) && (wstr_dot > esp_tempW11) && (mtp<=S16_CRAM_ENTER_MTP))
#else
    if ( (lcu1espCramPreact_FR==0) && (ldu1espAyStartCRAM_FR==1) && ( wstr>50) && (wstr_dot > esp_tempW11) && (mtp<=S16_CRAM_ENTER_MTP))
#endif
    { 
        cram_ttp_cnt_fr = cram_ttp_cnt_fr + 1;
        
        if ( cram_ttp_cnt_fr > 100 )
        {
            cram_ttp_cnt_fr = 100 ; 
        }
        else
        {
           ;
        }
        
        if ( cram_ttp_cnt_fr > time_to_cram_preact )
        {
            lcu1espCramPreact_FR = 1; 
        }
        else
        {
           ;
        }        
    }
    else if ( (lcu1espCramPreact_FR==1) && (ldu1espAyStartCRAM_FR==1) && (wstr_dot > esp_tempW12) && (mtp<=(S16_CRAM_ENTER_MTP+MTP_1_P)) )
    {
        lcu1espCramPreact_FR = 1;    
        cram_ttp_cnt_fr = 0 ;                
    }
    else
    {
        lcu1espCramPreact_FR = 0;
        cram_ttp_cnt_fr = 0 ;           
    }    
#if (__EUROPE_ADAC ==1)    
    if((lcu1espCramPreact_FR==1)&&(nosign_delta_yaw >= sign_o_delta_yaw_thres))
    {
    	 lcu1espCramPreact_FR = 0;        
    }
    else
    {
    	;  
    }
#endif        
	#if(__AHB_GEN3_SYSTEM==ENABLE) || (__IDB_LOGIC == ENABLE)
    if (  ((lcu1espCramPreact_FL==1)||(lcu1espCramPreact_FR==1)) 
           && ( lsesps16AHBGEN3mpress < S16_CRAM_PREFILL_MPRESS_TH )                      
           && (vrefk > S16_CRAM_ENTER_SPD)
           && ( det_alatm > S16_CRAM_ENTER_ALATM )  )
	#else
    if (  ((lcu1espCramPreact_FL==1)||(lcu1espCramPreact_FR==1)) 
           && ( mpress < S16_CRAM_PREFILL_MPRESS_TH )                     
           && (vrefk > S16_CRAM_ENTER_SPD)
           && ( det_alatm > S16_CRAM_ENTER_ALATM )  )
	#endif
    {
        lcu1espCramPreactFRT = lcu1espCramPreact_FL|lcu1espCramPreact_FR;
        if(( ABS_fz == 1 )&&(MPRESS_BRAKE_ON==1))
        {
            lcu1espCramPreact_FL = 0;     
            lcu1espCramPreact_FR = 0;     
            lcu1espCramPreactFRT = 0;
        }
        else
        {
            ;   
        }
    } 
    else  
    {
        lcu1espCramPreact_FL = 0;     
        lcu1espCramPreact_FR = 0;     
        lcu1espCramPreactFRT = 0;  
    }

    
    /*  When the driver handles dynamically, it is needed to stabilize more. */    
    
    if ((ldu1espCramDynStabPhase==0)&&(lcu1espCramPreactFRT==1)) 
    {
        ldu1espCramDynStabPhase = 1 ;
    } 
    else if (ldu1espCramDynStabPhase==1)
    {        
        ldu1espCramDynStabPhase = 1 ;        
    }    
    else  
    {
        ldu1espCramDynStabPhase = 0 ;     
    }

        
    if ( lcu1espCramPreactFRT==1 ) 
    {
        
        if ( wstr_dot >= 10000 )
        {
            esp_tempW3 = 10000; 
        }
        else if ( wstr_dot <= -10000 )
        {
            esp_tempW3 = -10000;         
        }
        else
        {
           esp_tempW3 = wstr_dot ;
        }
        
        if ( yaw_out_dot2 >= 4000 )
        {
            esp_tempW4 = 4000; 
        }
        else if ( yaw_out_dot2 <= -4000 )
        {
            esp_tempW4 = -4000;         
        }
        else
        {
            esp_tempW4 = yaw_out_dot2 ;
        }        
        
        /* tempW2 = esp_tempW3;  tempW1 = -1;  tempW0 = 4;
        s16muls16divs16();
        esp_tempW3 = tempW3;  */
        esp_tempW3 = (int16_t)( (((int32_t)(esp_tempW3))*(-1))/4 ) ; 
                
        moment_pre_front_cram_preact = esp_tempW3 - esp_tempW4 ;
                    
        esp_tempW5 = LCESP_s16IInter5Point( vrefk,  VREF_K_20_KPH,   S16_ROP_PREFILL_MQ_WEG_VEL_20K, 	
        						                    VREF_K_40_KPH,   S16_ROP_PREFILL_MQ_WEG_VEL_40K,
    					                            VREF_K_60_KPH,   S16_ROP_PREFILL_MQ_WEG_VEL_60K,
    					                            VREF_K_80_KPH,   S16_ROP_PREFILL_MQ_WEG_VEL_80K,					                            
    					                            VREF_K_100_KPH,  S16_ROP_PREFILL_MQ_WEG_VEL_100K ) ;	
    					                                                
        /* tempW2 = moment_pre_front_cram_preact;  tempW1 = esp_tempW5;  tempW0 = 100;
        s16muls16divs16();
        moment_pre_front_cram_preact = tempW3;      */
        moment_pre_front_cram_preact = (int16_t)( (((int32_t)moment_pre_front_cram_preact)*esp_tempW5)/100 ) ; 
        
        if ( moment_pre_front_cram_preact >= 3000 )
        {
            moment_pre_front_cram_preact = 3000; 
        }
        else if ( moment_pre_front_cram_preact <= -3000 )
        {
            moment_pre_front_cram_preact = -3000;         
        }
        else
        {
             ;
        }        
             
    } 
    else  
    {
        moment_pre_front_cram_preact = 0;       
    }    
    
    
} 
#endif    /* __CRAM_PRE_FRONT_W_CRTL */

#endif 		/* __CRAM */

void DETECT_ON_CENTER(void)
{
    esp_tempW0 =( McrAbs((int16_t)cyctime - on_center_st_counter) );

    if ( (abs_wstr > S16_MAX_ON_CENTER_WSTR) &&
            ( ( (on_center_st_wstr > 0) && ( wstr <= -S16_MAX_ON_CENTER_WSTR) )
              || ( (on_center_st_wstr <= 0) && ( wstr > S16_MAX_ON_CENTER_WSTR) ) )  )
    {

        on_center_st_counter =(int16_t)cyctime;
        on_center_st_wstr = wstr;

        if ( (esp_tempW0 < U8_ON_CENTER_DCT_CNT1) && ( esp_tempW0 > U8_ON_CENTER_DCT_CNT2) && ( on_center_counter < 6 ) )
        {
                    on_center_counter = on_center_counter + 1;
        }
        else
        {
        	;
        }
    }
    else
    {
    	;
    }

    if ( ( esp_tempW0 > U8_ON_CENTER_DCT_CNT3 ) && ( on_center_counter >= 1 ) )
    {
        on_center_st_counter =(int16_t)cyctime;
        on_center_counter = on_center_counter - 3;
        if ( on_center_counter < 0 )
        {
        	on_center_counter = 0;
        }
        else
        {
        	;
        }
    }
    else
    {
    	;
    }

    if ( on_center_counter >= U8_ON_CENTER_DCT_CYCLE )
    {
        on_center_gain = on_center_counter - 1;

		if ( on_center_gain >= ON_CENTER_GAIN_MAX )
		{
			on_center_gain = ON_CENTER_GAIN_MAX;
   		}
   		else
   		{
   			;
   		}
        FLAG_ON_CENTER = 1;
    }
    else
    {
        on_center_gain = 1;
        FLAG_ON_CENTER = 0;
    }

}

void LDESP_DetectRevSteer(void)   /* '06 LPG -> '06.07.21 */
{
    tempW2 = wstr_dot;
    tempW1 = wstr;
    tempW0 = 10000;
    s16muls16divs16();
    esp_tempW0 = tempW3;

    tempW2 = yaw_out;
    tempW1 = wstr;
    tempW0 = 10000;
    s16muls16divs16();
    esp_tempW2 = tempW3;

    esp_tempW8 = McrAbs(wstr);
    esp_tempW9 = McrAbs(wstr_dot);


    if( esp_tempW0 < 0 )  /* 2.4, 4.4 phase-plane  */
    {
        if ( esp_tempW8 > 1500 )
        {
          if (esp_tempW9 > 3000)
          {
            esp_tempW1 = 3 ;     /* Adding count(1~5) */
          }
          else if (esp_tempW9 > 1000)
          {
            esp_tempW1 = 2 ;     /* Adding count(1~5) */
          }
          else
          {
            esp_tempW1 = 1 ;     /* Adding count(1~5) */
          }
        }
        else if ( esp_tempW8 > 500 )
        {
          if (esp_tempW9 > 3000)
          {
            esp_tempW1 = 3 ;     /* Adding count(1~5) */
          }
          else if (esp_tempW9 > 1000)
          {
            esp_tempW1 = 2 ;     /* Adding count(1~5) */
          }
          else
          {
            esp_tempW1 = 1 ;     /* Adding count(1~5) */
          }
        }
        else
        {
          if (esp_tempW9 > 3000)
          {
            esp_tempW1 = 4 ;     /* Adding count(1~5) */
          }
          else if (esp_tempW9 > 1000)
          {
            esp_tempW1 = 3 ;     /* Adding count(1~5) */
          }
          else if (esp_tempW9 > 300)
          {
            esp_tempW1 = 2 ;     /* Adding count(1~5) */
          }
          else
          {
            esp_tempW1 = -1 ;     /* Adding count(1~5) */
          }
        }

    }
    else    /* 1.4, 3.4 phase-plane  */
    {
      if ( esp_tempW2 <= 0 )
      {
        if ( esp_tempW8 > 1000 )
        {
            esp_tempW1 = 0 ;     /* Adding count(1~5) */
        }
        else if ( esp_tempW8 > 500 )
        {
          if (esp_tempW9 > 3000)
          {
            esp_tempW1 = 2 ;     /* Adding count(1~5) */
          }
          else if (esp_tempW9 > 1000)
          {
            esp_tempW1 = 1 ;     /* Adding count(1~5) */
          }
          else if (esp_tempW9 > 300)
          {
            esp_tempW1 = 0 ;     /* Adding count(1~5) */
          }
          else
          {
            esp_tempW1 = -1 ;     /* Adding count(1~5) */
          }
        }
        else
        {
          if (esp_tempW9 > 3000)
          {
            esp_tempW1 = 2 ;     /* Adding count(1~5) */
          }
          else if (esp_tempW9 > 1000)
          {
            esp_tempW1 = 1 ;     /* Adding count(1~5) */
          }
          else if (esp_tempW9 > 300)
          {
            esp_tempW1 = 0 ;     /* Adding count(1~5) */
          }
          else
          {
            esp_tempW1 = -1 ;     /* Adding count(1~5) */
          }
        }
      }
      else
      {
            esp_tempW1 = -5 ;     /* Adding count(1~5) */
      }
    }

        RevSteerCnt = RevSteerCnt + esp_tempW1 ;

		if(RevSteerCnt <= 0)
		{
			RevSteerCnt = 0 ;
		}
		else if(RevSteerCnt >= 70)
		{
			RevSteerCnt = 70 ;
		}
		else
		{
		    ;
		}

    if ( (FLAG_DELTAYAW_SIGNCHANGE == 0)
         && ( ((Reverse_Steer_Flg == 0) && (RevSteerCnt >= 50))
              || ((Reverse_Steer_Flg == 1) && (RevSteerCnt >= 50))) )
    {
    	Reverse_Steer_Flg = 1;
    }
    else
    {
        Reverse_Steer_Flg = 0;
        RevSteerCnt = 0 ;
    }

/*

    if ( Reverse_Steer_Flg == 1 )
    {

        if (RevSteerCnt >= 10)
        {
            esp_tempW7 = LCESP_s16IInter2Point( McrAbs(wstr), 0, 50,
                                                          40, 70 );

            RevSteer_Wgt = LCESP_s16IInter2Point( yaw_out_dot, -200, esp_tempW7,
                                                               -60, 100 );
        }
        else if (esp_tempW0 <= 0)
        {
            RevSteer_Wgt = LCESP_s16IInter2Point( yaw_out_dot, -200, 50,
                                                               -60, 100 );
        }
        else
        {
            RevSteer_Wgt = 100 ;
        }
    }
    else
    {
        RevSteer_Wgt = 100 ;
    }
    */

    if ((yaw_sign>0)&&(rf<ab_rfm)&&(yaw_out<ab_rfm)) 
    { 

      esp_tempW1 = LCESP_s16IInter5Point( vrefk,  S16_YC_SPEED_DEP_VLOW_V,     S16_YA_F_OS_WEG_LM_VLOW,
                                                  S16_YC_SPEED_DEP_LOW_V,      S16_YA_F_OS_WEG_LM_LOW ,
                                                  S16_YC_SPEED_DEP_MED1_V,     S16_YA_F_OS_WEG_LM_MED ,
                                                  S16_YC_SPEED_DEP_HIG_V,      S16_YA_F_OS_WEG_LM_HIG ,
                                                  S16_YC_SPEED_DEP_VER_HIG_V,  S16_YA_F_OS_WEG_LM_VHIG );      
                                                        
      esp_tempW2 = LCESP_s16IInter5Point( vrefk,  S16_YC_SPEED_DEP_VLOW_V,     S16_YA_F_OS_WEG_MM_VLOW,
                                                  S16_YC_SPEED_DEP_LOW_V,      S16_YA_F_OS_WEG_MM_LOW ,
                                                  S16_YC_SPEED_DEP_MED1_V,     S16_YA_F_OS_WEG_MM_MED ,
                                                  S16_YC_SPEED_DEP_HIG_V,      S16_YA_F_OS_WEG_MM_HIG ,
                                                  S16_YC_SPEED_DEP_VER_HIG_V,  S16_YA_F_OS_WEG_MM_VHIG );      
      
      esp_tempW3 = LCESP_s16IInter5Point( vrefk,  S16_YC_SPEED_DEP_VLOW_V,     S16_YA_F_OS_WEG_HM_VLOW,
                                                  S16_YC_SPEED_DEP_LOW_V,      S16_YA_F_OS_WEG_HM_LOW ,
                                                  S16_YC_SPEED_DEP_MED1_V,     S16_YA_F_OS_WEG_HM_MED ,
                                                  S16_YC_SPEED_DEP_HIG_V,      S16_YA_F_OS_WEG_HM_HIG ,
                                                  S16_YC_SPEED_DEP_VER_HIG_V,  S16_YA_F_OS_WEG_HM_VHIG );     

      esp_tempW4 = LCESP_s16FindRoadDependatGain( esp_mu, esp_tempW3, esp_tempW2, esp_tempW1);  /* Weight */


      esp_tempW1 = LCESP_s16IInter3Point( vrefk,  S16_YC_SPEED_DEP_LOW_V,      S16_YA_F_OS_HM_TH1_LSP ,
                                                  S16_YC_SPEED_DEP_MED1_V,     S16_YA_F_OS_HM_TH1 ,
                                                  S16_YC_SPEED_DEP_HIG_V,      S16_YA_F_OS_HM_TH1_HSP );                                                                                                      
                                                                                                      
      esp_tempW2 = LCESP_s16IInter3Point( vrefk,  S16_YC_SPEED_DEP_LOW_V,      S16_YA_F_OS_MM_TH1_LSP ,
                                                  S16_YC_SPEED_DEP_MED1_V,     S16_YA_F_OS_MM_TH1 ,
                                                  S16_YC_SPEED_DEP_HIG_V,      S16_YA_F_OS_MM_TH1_HSP );    
                                                  
      esp_tempW3 = LCESP_s16IInter3Point( vrefk,  S16_YC_SPEED_DEP_LOW_V,      S16_YA_F_OS_LM_TH1_LSP ,
                                                  S16_YC_SPEED_DEP_MED1_V,     S16_YA_F_OS_LM_TH1 ,
                                                  S16_YC_SPEED_DEP_HIG_V,      S16_YA_F_OS_LM_TH1_HSP );                                                  

      esp_tempW5 = LCESP_s16FindRoadDependatGain( esp_mu, esp_tempW1, esp_tempW2, esp_tempW3);   
      
      esp_tempW1 = LCESP_s16IInter3Point( vrefk,  S16_YC_SPEED_DEP_LOW_V,      S16_YA_F_OS_HM_TH2_LSP ,
                                                  S16_YC_SPEED_DEP_MED1_V,     S16_YA_F_OS_HM_TH2 ,
                                                  S16_YC_SPEED_DEP_HIG_V,      S16_YA_F_OS_HM_TH2_HSP );                                                                                                      
                                                                                                    
      esp_tempW2 = LCESP_s16IInter3Point( vrefk,  S16_YC_SPEED_DEP_LOW_V,      S16_YA_F_OS_MM_TH2_LSP ,
                                                  S16_YC_SPEED_DEP_MED1_V,     S16_YA_F_OS_MM_TH2 ,
                                                  S16_YC_SPEED_DEP_HIG_V,      S16_YA_F_OS_MM_TH2_HSP );    
                                                                                               
      esp_tempW3 = LCESP_s16IInter3Point( vrefk,  S16_YC_SPEED_DEP_LOW_V,      S16_YA_F_OS_LM_TH2_LSP ,
                                                  S16_YC_SPEED_DEP_MED1_V,     S16_YA_F_OS_LM_TH2 ,
                                                  S16_YC_SPEED_DEP_HIG_V,      S16_YA_F_OS_LM_TH2_HSP );                                                  
 
      esp_tempW6 = LCESP_s16FindRoadDependatGain( esp_mu, esp_tempW1, esp_tempW2, esp_tempW3);      
                                                        
      
      s16_yaw_accel_fb_gain = LCESP_s16IInter2Point( yaw_out_dot2,  -esp_tempW6,  esp_tempW4,
                                                                    -esp_tempW5,  100 );        

	    if ( s16_yaw_accel_fb_gain < 20 )
	    {
	        s16_yaw_accel_fb_gain = 20;
    }
	    else
	    {
	        ;
	    }                       
    }
    else if ((yaw_sign<0)&&(rf>-ab_rfm)&&(yaw_out>-ab_rfm)) 
    {
      
      esp_tempW1 = LCESP_s16IInter5Point( vrefk,  S16_YC_SPEED_DEP_VLOW_V,     S16_YA_F_OS_WEG_LM_VLOW,
                                                  S16_YC_SPEED_DEP_LOW_V,      S16_YA_F_OS_WEG_LM_LOW ,
                                                  S16_YC_SPEED_DEP_MED1_V,     S16_YA_F_OS_WEG_LM_MED ,
                                                  S16_YC_SPEED_DEP_HIG_V,      S16_YA_F_OS_WEG_LM_HIG ,
                                                  S16_YC_SPEED_DEP_VER_HIG_V,  S16_YA_F_OS_WEG_LM_VHIG );      
                                                  
      esp_tempW2 = LCESP_s16IInter5Point( vrefk,  S16_YC_SPEED_DEP_VLOW_V,     S16_YA_F_OS_WEG_MM_VLOW,
                                                  S16_YC_SPEED_DEP_LOW_V,      S16_YA_F_OS_WEG_MM_LOW ,
                                                  S16_YC_SPEED_DEP_MED1_V,     S16_YA_F_OS_WEG_MM_MED ,
                                                  S16_YC_SPEED_DEP_HIG_V,      S16_YA_F_OS_WEG_MM_HIG ,
                                                  S16_YC_SPEED_DEP_VER_HIG_V,  S16_YA_F_OS_WEG_MM_VHIG );      
                                                  
      esp_tempW3 = LCESP_s16IInter5Point( vrefk,  S16_YC_SPEED_DEP_VLOW_V,     S16_YA_F_OS_WEG_HM_VLOW,
                                                  S16_YC_SPEED_DEP_LOW_V,      S16_YA_F_OS_WEG_HM_LOW ,
                                                  S16_YC_SPEED_DEP_MED1_V,     S16_YA_F_OS_WEG_HM_MED ,
                                                  S16_YC_SPEED_DEP_HIG_V,      S16_YA_F_OS_WEG_HM_HIG ,
                                                  S16_YC_SPEED_DEP_VER_HIG_V,  S16_YA_F_OS_WEG_HM_VHIG );     

      esp_tempW4 = LCESP_s16FindRoadDependatGain( esp_mu, esp_tempW3, esp_tempW2, esp_tempW1);  /* Weight */

      esp_tempW1 = LCESP_s16IInter3Point( vrefk,  S16_YC_SPEED_DEP_LOW_V,      S16_YA_F_OS_HM_TH1_LSP ,
                                                  S16_YC_SPEED_DEP_MED1_V,     S16_YA_F_OS_HM_TH1 ,
                                                  S16_YC_SPEED_DEP_HIG_V,      S16_YA_F_OS_HM_TH1_HSP );                                                                                                      
                                                                                                      
      esp_tempW2 = LCESP_s16IInter3Point( vrefk,  S16_YC_SPEED_DEP_LOW_V,      S16_YA_F_OS_MM_TH1_LSP ,
                                                  S16_YC_SPEED_DEP_MED1_V,     S16_YA_F_OS_MM_TH1 ,
                                                  S16_YC_SPEED_DEP_HIG_V,      S16_YA_F_OS_MM_TH1_HSP );    
                                                  
      esp_tempW3 = LCESP_s16IInter3Point( vrefk,  S16_YC_SPEED_DEP_LOW_V,      S16_YA_F_OS_LM_TH1_LSP ,
                                                  S16_YC_SPEED_DEP_MED1_V,     S16_YA_F_OS_LM_TH1 ,
                                                  S16_YC_SPEED_DEP_HIG_V,      S16_YA_F_OS_LM_TH1_HSP );                                                  

      esp_tempW5 = LCESP_s16FindRoadDependatGain( esp_mu, esp_tempW1, esp_tempW2, esp_tempW3);   
          
      esp_tempW1 = LCESP_s16IInter3Point( vrefk,  S16_YC_SPEED_DEP_LOW_V,      S16_YA_F_OS_HM_TH2_LSP ,
                                                  S16_YC_SPEED_DEP_MED1_V,     S16_YA_F_OS_HM_TH2 ,
                                                  S16_YC_SPEED_DEP_HIG_V,      S16_YA_F_OS_HM_TH2_HSP );                                                                                                      
                                                                                                    
      esp_tempW2 = LCESP_s16IInter3Point( vrefk,  S16_YC_SPEED_DEP_LOW_V,      S16_YA_F_OS_MM_TH2_LSP ,
                                                  S16_YC_SPEED_DEP_MED1_V,     S16_YA_F_OS_MM_TH2 ,
                                                  S16_YC_SPEED_DEP_HIG_V,      S16_YA_F_OS_MM_TH2_HSP );    
                                                                                               
      esp_tempW3 = LCESP_s16IInter3Point( vrefk,  S16_YC_SPEED_DEP_LOW_V,      S16_YA_F_OS_LM_TH2_LSP ,
                                                  S16_YC_SPEED_DEP_MED1_V,     S16_YA_F_OS_LM_TH2 ,
                                                  S16_YC_SPEED_DEP_HIG_V,      S16_YA_F_OS_LM_TH2_HSP );                                                  
 
      esp_tempW6 = LCESP_s16FindRoadDependatGain( esp_mu, esp_tempW1, esp_tempW2, esp_tempW3);   
      
      s16_yaw_accel_fb_gain = LCESP_s16IInter2Point( yaw_out_dot2,  esp_tempW5,  100,
                                                                    esp_tempW6,  esp_tempW4 );             
      
	    if ( s16_yaw_accel_fb_gain < 20 )
	    {
	        s16_yaw_accel_fb_gain = 20;
	    }
	    else
	    {
	        ;
	    }          
    }
    else
    {
      s16_yaw_accel_fb_gain = 100 ;   
    }

}

void DETECT_SLALOM(void)
{
    esp_tempW0 =( McrAbs((int16_t)cyctime - slalom_st_counter) );

    if ( (abs_wstr > S16_MAX_SLALOM_WSTR) &&
            ( ( (slalom_st_wstr > 0) && ( wstr <= -S16_MAX_SLALOM_WSTR) )
              || ( (slalom_st_wstr <= 0) && ( wstr > S16_MAX_SLALOM_WSTR) ) )  )
    {
        slalom_st_counter =(int16_t)cyctime;
        slalom_st_wstr = wstr;

        if ( (esp_tempW0 < U8_SLALOM_DCT_CNT1) && ( esp_tempW0 > U8_SLALOM_DCT_CNT2) && ( slalom_counter < 3 ) )
        {
                    slalom_counter = slalom_counter + 1;
        }
        else
        {
        	;
        }
    }
    else
    {
    	;
    }

    if ( (( esp_tempW0 > U8_SLALOM_DCT_CNT3 ) && ( slalom_counter >= 1 )) || (FLAG_ON_CENTER==1) )
    {
        slalom_st_counter =(int16_t)cyctime;
        slalom_counter = 0;
    }
    else
    {
    	;
    }

    if ( ((slalom_counter >= 2) && (det_alatm >= lsesps16MuHigh)) || (slalom_counter >= 3))
    {
        FLAG_SLALOM = 1;
    }
    else
    {
        FLAG_SLALOM = 0;
    }

}

/*void DETECT_BIG_OVERSTEER(void)
{

    if (FLAG_TIME_OPT==1)
    {
        esp_tempW4 = S16_DETECT_ENTER_Q_H;
        tempW2 = S16_ENTER_Q_H_MU_LOW_SP_WEG;
        tempW1 = esp_tempW4;
        tempW0 = 100;
        s16muls16divs16();
        esp_tempW3 = tempW3;

        esp_tempW7 = LCESP_s16IInter2Point( vrefk, S16_ENTER_Q_H_MU_LOW_SP, esp_tempW3,
                                   S16_ENTER_Q_H_MU_HIGH_SP, esp_tempW4);  // Enter Q - High mu      

        esp_tempW4 = S16_DETECT_ENTER_Q_M;
        tempW2 = S16_ENTER_Q_M_MU_LOW_SP_WEG;
        tempW1 = esp_tempW4;
        tempW0 = 100;
        s16muls16divs16();
        esp_tempW3 = tempW3;

        esp_tempW8 = LCESP_s16IInter2Point( vrefk, S16_ENTER_Q_M_MU_LOW_SP, esp_tempW3,
                                   S16_ENTER_Q_M_MU_HIGH_SP, esp_tempW4);  // Enter Q - Mid mu    


        esp_tempW4 = S16_DETECT_ENTER_Q_L;
        tempW2 = S16_ENTER_Q_L_MU_LOW_SP_WEG;
        tempW1 = esp_tempW4;
        tempW0 = 100;
        s16muls16divs16();
        esp_tempW3 = tempW3;

        esp_tempW9 = LCESP_s16IInter2Point( vrefk, S16_ENTER_Q_L_MU_LOW_SP, esp_tempW3,
                                   S16_ENTER_Q_L_MU_HIGH_SP, esp_tempW4);  // Enter Q - Low mu  

        big_over_enter_Q = LCESP_s16FindRoadDependatGain( esp_mu, esp_tempW7,
                            esp_tempW8, esp_tempW9);
        if ( ESP_MINI_TIRE_SUSPECT==1 )
        {
        	big_over_enter_Q = (big_over_enter_Q >> 1);
        }
        else
        {
        	;
        }

        if ( big_over_enter_Q > -15 )
        {
        	big_over_enter_Q = -15;
        }
        else
        {
        	;
        }

        big_over_exit_Q = LCESP_s16FindRoadDependatGain( esp_mu, S16_DETECT_EXIT_Q_H,
                            S16_DETECT_EXIT_Q_M, S16_DETECT_EXIT_Q_L);

        esp_tempW3 = big_over_enter_Q + 5 ;



        esp_tempW4 = big_over_enter_Q + 1 ;

    // '06 SWD     
        if ( big_over_exit_Q > esp_tempW3 )
        {
            big_over_exit_Q = esp_tempW3 ;
        }
        else if ( big_over_exit_Q < esp_tempW4 )
        {
            big_over_exit_Q = esp_tempW4 ;
        }
        else
        {
            ;
        }

    }
    else
    {
    	;
    }

    esp_tempW4 = -(ref_delta_yaw_first_dot<<2);

    // '06 SWD     
        tempW2 = esp_tempW4; tempW1 = S16_DETECT_EXIT_DEC_WEG; tempW0 = 100;
        s16muls16divs16();
        esp_tempW4 = tempW3;

    esp_tempW5 = -(ref_delta_yaw_first_dot>>2);
    esp_tempW6 =  big_over_enter_Q - 20 ;

    esp_tempW3 = LCESP_s16IInter2Point(moment_q_front, esp_tempW6, esp_tempW4, big_over_enter_Q, esp_tempW5);

    if ( ((FLAG_BIG_OVERSTEER==0) && ( moment_q_front < big_over_enter_Q )&& ( delta_yaw_dot > 0 ) )    // Enter Condition 
         ||((FLAG_BIG_OVERSTEER==1) && ( moment_q_front < big_over_exit_Q )&& ( delta_yaw_dot > esp_tempW3 )) )  // Exit Condition
    {
        FLAG_BIG_OVERSTEER = 1;

    }
    else
    {
		FLAG_BIG_OVERSTEER = 0;
    }
} */
/*
void DETECT_FISH_HOOK_MODE(void)
{
 // Fish-Hook Control  :  HMC   04/05/18 ~ 04/06/07  :

	#if __FISH_HOOK_CONTROL

		if ( abs_wstr_dot2 >= 6500 )
		{
			counter_fish_hook=counter_fish_hook+1;
		    if( counter_fish_hook > T_300MS )
		    {
		    	counter_fish_hook = T_300MS ;
		    }
		    else
		    {
		    	;
		    }
	    }
	    else
	    {
	    	if ( abs_wstr_dot2 >= 5000 )
	    	{
	    		counter_fish_hook = counter_fish_hook ;
	    	}
	    	else if ( abs_wstr_dot2 >= 3600 )
	    	{
	    		counter_fish_hook = counter_fish_hook - 1  ;
	    	}
	    	else
	    	{
	    		counter_fish_hook = counter_fish_hook - 20 ;
	    	}

	     	if( counter_fish_hook < 0 )
	     	{
	     		counter_fish_hook = 0 ;
	    	}
	    	else
	    	{
	    		;
	    	}
		}

   	if( ( (FLAG_FISH_HOOK==0) && (ABS_fz==0) && (MPRESS_BRAKE_ON==0)
   		&& ( vrefk >= 600 )     	 // 60 kph , 37.5 mph
   		&& ( vrefk <= 900 )  		 // 90 kph , 56.2 mph
   		&& ( counter_fish_hook >= T_250MS ) // 6/2/wed <--  T_250MS(36cnt)
		&& ( det_abs_wstr2 >= 1850 )  	//	6/2/wed 	<-- 190 deg , 185 deg
		&& ( abs_wstr_dot2 >= 6500 ) 	// 	6/2/wed   	<-- 630 deg , 600 deg
    	&& ( det_wstr_dot2 >= 6500 )  	//	6/2/wed
    	&& ( det_wstr_dot_lf >= 4000 )  // 	6/2/wed
    	&& ( esp_mu >= 700 ) 			// 	6/2/wed
    	&& ( det_alatc >= 1000 ) 		// 	6/2/wed
    	&& ( det_alatm >= 700 )   	 //
		&& ( moment_q_front < -20 ) )
	  || ( (FLAG_FISH_HOOK==1) && (ABS_fz==0) && (MPRESS_BRAKE_ON==0)
	  	&& ( vrefk >= 320 )     	 // 32 kph , 20 mph
		&& ( det_abs_wstr2 >= 1600 )   	// 6/2/wed
    	&& ( det_wstr_dot2 >= 1000 )   	// 6/2/wed
    	&& ( det_wstr_dot_lf >= 100 ) 	// 6/2/wed
    	&& ( esp_mu >= 700 ) 		  	// 6/2/wed
    	&& ( det_alatc >= 1000 ) 			//	6/2/wed
    	&& ( det_alatm >= 600 ) ) )
    {

		FLAG_FISH_HOOK = 1 ;

		#if ENABLE_3WHEEL_CONTROL
			FLAG_3WHEEL_CONTROL = 1;			// Logic Tune : 3Wheel enable ?
		#else
			FLAG_3WHEEL_CONTROL = 0;
		#endif

	}
	else
	{

		FLAG_FISH_HOOK = 0 ;
		FLAG_3WHEEL_CONTROL = 0;
	}
	#else
		FLAG_FISH_HOOK = 0;
        FLAG_3WHEEL_CONTROL = 0;
	#endif

} */

void DETECT_SAME_PHASE( void )
{

    tempW2 = yaw_out;
    tempW1 = rf;
    tempW0 = 10000;
    s16muls16divs16();
    esp_tempW0 = tempW3;

    esp_tempW1 = vrefm;
    if( esp_tempW1 <= 140 )
    {
    	esp_tempW1 = 140;                    /* 14.0*3.6 = 50 kph    */
    }
    else
    {
    	;
    }

/*
    if(FLAG_TIME_OPT==1)
    {
        same_phase_enter_ay = LCESP_s16FindRoadDependatGain( esp_mu, S16_ESP_SAME_PHASE_AY_H,
                                S16_ESP_SAME_PHASE_AY_M, S16_ESP_SAME_PHASE_AY_L);
    }
    else
    {
    	;
    }

    esp_tempW2 = same_phase_enter_ay;

    tempUW2 = (uint16_t)esp_tempW2;
    tempUW1 = 562;
    tempUW0 = (uint16_t)esp_tempW1;
    u16mulu16divu16();
    esp_tempW3 = (int16_t)tempUW3;

//    if(LOOP_TIME_20MS_TASK_1==1)
//    {
        same_phase_enter_yaw = LCESP_s16FindRoadDependatGain( esp_mu, S16_YAW_SAME_PHASE_LIMIT_YAW_H ,
                         S16_YAW_SAME_PHASE_LIMIT_YAW_M , S16_YAW_SAME_PHASE_LIMIT_YAW_L);
//    }
//    else
//    {
//    	;
//    }
//
     esp_tempW4 = same_phase_enter_yaw;

    if( esp_tempW3 > esp_tempW4 )
    {
        esp_tempW3 = esp_tempW4;
    }
    else
    {
    	;
    }

    if ( ((esp_tempW0>0)&&(McrAbs(yaw_out)>esp_tempW3)&& (McrAbs(rf)>esp_tempW3))
        || ( esp_mu < lsesps16MuMed ) )
    {
        YAW_SAME_PHASE=1;
    }
	else if ( (yaw_error > 1000) && (det_wstr_dot_lf <= 1000)
	           && ( yaw_out_dot >= 50 )
	           && ( McrAbs(rf) < McrAbs(yaw_out) ) && ( det_alatc < det_alatm ) )
	{
        YAW_SAME_PHASE=1;	             // '06.05.12 For LPG
	}
    else
    {
        YAW_SAME_PHASE=0;
    }
*/


  #if (__YAW_M_OVERSHOOT_DETECTION==1)
 /* GSUV 3rd shot*/
    
    if (alat > LAT_0G02G)
    {
        ldespu1AlatSign = 1;
    }
    else if (alat < -LAT_0G02G)
    {
        ldespu1AlatSign = 0;
    }
    else
    {
        ;
    }
    
    if (yaw_out > YAW_1DEG)
    {
        ldespu1YawMSign = 1;
    }
    else if (yaw_out < -YAW_1DEG)
    {
        ldespu1YawMSign = 0;
    }
    else
    {
        ;
    }
    
    if (((wstr > WSTR_0_DEG) && (wstr_dot < WSTR_0_DEG)) || ((wstr < WSTR_0_DEG) && (wstr_dot > WSTR_0_DEG)))
    {
        esp_tempW1 = LCESP_s16IInter3Point(abs_wstr, S16_3RD_SHOT_DET_REF_WSTR_L, S16_3RD_SHOT_DET_WSTR_DOT_L, 
                                                     S16_3RD_SHOT_DET_REF_WSTR_M, S16_3RD_SHOT_DET_WSTR_DOT_M, 
                                                     S16_3RD_SHOT_DET_REF_WSTR_H, S16_3RD_SHOT_DET_WSTR_DOT_H);
    }
    else
    {
        esp_tempW1 = S16_3RD_SHOT_DET_WSTR_DOT_INC;
    }
    
    if (McrAbs(nosign_delta_yaw_first) < S16_3RD_SHOT_DET_STABLE_D_YAW)
    {
        if (ldespu16DelYawStableCnt < U16_3RD_SHOT_DET_STABLE_D_YAW_T)
        {
            ldespu16DelYawStableCnt = ldespu16DelYawStableCnt + 1;
            ldespu1DelYawStable = 0;
        }
        else
        {
            ldespu1DelYawStable = 1;
            ldespu16DelYawStableCnt = U16_3RD_SHOT_DET_STABLE_D_YAW_T;
        }
    }
    else
    {
        ldespu16DelYawStableCnt = 0;
        ldespu1DelYawStable = 0;
    }
    
    if (ldespu1AlatSign == ldespu1YawMSign)
    {
        ldespu1AlatYawMSamePhase = 1;
    }
    else
    {
        ldespu1AlatYawMSamePhase = 0;
    }
    
    tempW2 = mu_level; tempW1 = 562; tempW0 = vrefm;
    s16muls16divs16();
    esp_tempW2 = tempW3;
    
    esp_tempW3 = LCESP_s16FindRoadDependatGain(esp_mu, (int16_t)U8_3RD_SHOT_DET_MU_GAIN_HMU, (int16_t)U8_3RD_SHOT_DET_MU_GAIN_MMU, (int16_t)U8_3RD_SHOT_DET_MU_GAIN_LMU); 
    
    ldesps16MulevelOverVxThreshold = (int16_t)((((int32_t)esp_tempW2)*esp_tempW3)/100 );
    
    if (ldespu1YawRateOverShoot == 0)
    {
        if (
           (ldespu1AlatYawMSamePhase == 0) 
        && (YAW_CHANGE_LIMIT_ACT == 0) 
        //&& (YAW_CDC_WORK == 0) 
        && (McrAbs(lsesps16YawSS) < ldesps16MulevelOverVxThreshold)
        && (McrAbs(yaw_out) < ldesps16MulevelOverVxThreshold)
        //&& (abs_wstr < S16_3RD_SHOT_DET_WSTR)
        && (abs_wstr_dot2 < esp_tempW1)
        //&& (((yaw_out > 0) && (nosign_delta_yaw_first < 0))||((yaw_out < 0) && (nosign_delta_yaw_first > 0)))
        && (ldespu1DelYawStable == 0)
			 #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
        && ( lsespu1AHBGEN3MpresBrkOn == 0 )    	
			 #else
        && (MPRESS_BRAKE_ON == 0)
			 #endif
        && (Bank_Susp_Flag == 0) && (Bank_Comp_Flag == 0)
           ) 
        {
            ldespu1YawRateOverShoot = 1;
            if (ldespu1YawMSign == 1)
            {
                ldespu1YawRateOverShootSign = 0;
            }
            else
            {
                ldespu1YawRateOverShootSign = 1;
            }
        }
        else
        {
            ;
        }
    }
    else
    {
        if (YAW_CHANGE_LIMIT_ACT == 1)
        {
            ldespu1YawRateOverShoot = 0;
        }
        else if(
                  (McrAbs(lsesps16YawSS) > ldesps16MulevelOverVxThreshold)
                ||(McrAbs(yaw_out) > ldesps16MulevelOverVxThreshold)
                //  (abs_wstr > S16_3RD_SHOT_DET_WSTR)
                ||(abs_wstr_dot2 > esp_tempW1)
                //|| (((yaw_out > 0) && (nosign_delta_yaw_first > 0))||((yaw_out < 0) && (nosign_delta_yaw_first < 0)))
                ||((ldespu1AlatYawMSamePhase == 1) && (ldespu1YawMSign == ldespu1YawRateOverShootSign))
                ||(ldespu1DelYawStable == 1)
              #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
                ||(lsespu1AHBGEN3MpresBrkOn == 1)
              #else  
                ||(MPRESS_BRAKE_ON == 1)
              #endif  
                ||(Bank_Susp_Flag == 1) || (Bank_Comp_Flag == 1)
               )
        {
            ldespu1YawRateOverShoot = 0;
        }
        else
        {
            ;
        }
    }
            
 /* GSUV 3rd shot*/    
 
  #endif
 /* GSUV OS_US Signal */
 
 		ldesps16_gsuv_os_us_sig_old = ldesps16_gsuv_os_us_sig ;
 		
 		lu1_gsuv_os_us_sig_valid = ESP_ERROR_FLG ; 		/* 0 : Valid , 1 : Invalid */
        
    if ( VDC_DISABLE_FLG == 1 )
    {
        ldesps16_gsuv_os_us_sig = 0 ;
    }
    else
	  {
	  	 
		    if ( lcu1espActive3rdFlg == 1 )
		    {    	
            moment_q_rear = delta_moment_q/100 ;
    		} 
	  	
        esp_tempW1 = McrAbs(moment_q_front) - McrAbs(moment_q_rear) ; /* + : OS, - : US */

    		ldesps16_gsuv_os_us_sig = LCESP_s16Lpf1Int( esp_tempW1*10, ldesps16_gsuv_os_us_sig_old, 30 );           
	  }  
	  
    if( ldesps16_gsuv_os_us_sig > 510 )
    {
        ldesps16_gsuv_os_us_sig = 510;
    }
    else if ( ldesps16_gsuv_os_us_sig < -510 )
    {
        ldesps16_gsuv_os_us_sig = -510;
    }
    else
    {
    	;
    }	  
}
void LDESP_CompBetaDiff(void)
{
    beta_dot_old = beta_dot;
    ay_vx_sig_old = ay_vx_sig;
/*    beta_dot_ems_old = beta_dot_ems;    */
/*    beta_dot_ems2_old = beta_dot_ems2;  */
/*    beta_integ_old = beta_integ ;		*/
/*    beta_pseudo_old = beta_pseudo ;*/
/*   beta_fltr_old = beta_fltr ;    */
/*    front_beta_pseudo_old = front_beta_pseudo;  */

/*    esp_tempW1 = (int16_t)((((int32_t)alat)*562)/vrefm);   // r : 0.01 , vrefm >= 2.7(*3.6=9.7kph)*/

    tempW2 = alat; tempW1 = 562; tempW0 = vrefm;
    s16muls16divs16();
    esp_tempW1 = tempW3;

    ay_vx_sig =  esp_tempW1 ; 																			/* '11.05.16 : Ay/Vx signal ,  r : 0.01 */
    
    esp_tempW2 = (esp_tempW1 - yaw_out);
    esp_tempW7 = (alat - (yaw_out/5));
/*  beta_dot_org[3] = beta_dot_org[2];
    beta_dot_org[2] = beta_dot_org[1];
    beta_dot_org[1] = beta_dot_org[0];
    beta_dot_org[0] = (esp_tempW1-yaw_out);
    tempW0 = (int16_t)( (((int32_t)beta_dot_org[0])*3)/128);     // betad = ay/vx - yaw
    tempW1 = (int16_t)( (((int32_t)beta_dot_old)*125)/128);       // 0.5 Hz -> 1 hZ
    beta_dot = tempW0+tempW1;                               // r : 0.01
*/
/*      Fc=1.1Hz(T=0.15,122,6) | Fc=2Hz(T=0.081,117,11) | Fc=3Hz(T=0.053,111,17)        */
    beta_dot = LCESP_s16Lpf1Int(esp_tempW2, beta_dot_old, L_U8FILTER_GAIN_10MSLOOP_8HZ);         /* 0.5 Hz, r=0.01 */
    /*esp_tempW3 = esp_tempW2*100;
    if ( esp_tempW3 > 30000 )
    {
    	esp_tempW3 = 30000;
    }
    else if ( esp_tempW3 < -30000 )
    {
    	esp_tempW3 = -30000;
    }
    else
    {
    	;
    }*/
/*    beta_dot_ems = LCESP_s16Lpf1Int1024(esp_tempW3, beta_dot_ems_old,4);         // 0.1 Hz, r=0.0001     */
/*    beta_dot_ems2 = LCESP_s16Lpf1Int(esp_tempW7*10, beta_dot_ems2_old,1);         // 0.1 Hz, r=0.01     */
/*   beta_dot_dot = (int16_t)(((int32_t)(beta_dot - beta_dot_old)*100)/7); // r: 0.1 deg/s^2  */

/*    if( vrefk < 50) {  */
/*        if ( beta_dot_ems > 101 )       beta_dot_ems = beta_dot_ems - 100;  */
/*        else if ( beta_dot_ems < -101 ) beta_dot_ems = beta_dot_ems + 100;  */
/*        else                beta_dot_ems = 0;  */
/*   }   */
/*    beta_dot_dot_old = beta_dot_dot;
    tempW2 = (beta_dot - beta_dot_old);
    tempW1 = 100;
    tempW0 = 5;
    s16muls16divs16();
    beta_dot_dot = LCESP_s16Lpf1Int(tempW3, beta_dot_dot_old, L_U8FILTER_GAIN_10MSLOOP_8HZ); */ /*  2 Hz, T=0.081sec  */


/*    beta_integ = (int16_t)( (int32_t)beta_dot*sam_t_beta/100 + (int32_t)beta_integ_old );   // 1/1000  */
/*    tempW2 = beta_dot; tempW1 = sam_t_beta; tempW0 = 100;*/
/*    s16muls16divs16();*/
/*    beta_integ = tempW3 + beta_integ_old;*/


/********  Pseudo Integration ********/
/* e_beta(i) = ( 1 - sam_t*time_const )*e_beta(i-1) + sam_t*e_dbeta(i) */

 /*   if  ( KSTEER.STRAIGHT_FLAG && (det_wstr_dot<100) && (det_abs_wstr<100) ) */
    /*  || ( det_alatm < 200 )                                      // r : 0.001*/
    /*  || ( det_abs_wstr < 250 )) */
   /* {*/
        /* beta_pseudo = (int16_t)((int32_t)beta_pseudo_old - */
        /*                   (int32_t)beta_pseudo_old*sam_t_beta*time_pseudo/100) ;     // r=0.001   */
        tempW2 = beta_pseudo_old; tempW1 = sam_t_beta*time_pseudo; tempW0 = 100;
        s16muls16divs16();
        beta_pseudo = beta_pseudo_old - tempW3;

   /* } else {*/
    /*time_pseudo = 10;*/
        /* beta_pseudo = (int16_t)((int32_t)beta_pseudo_old - */
        /*                 (int32_t)beta_pseudo_old*sam_t_beta*time_pseudo/10000 +  */
        /*                 (int32_t)beta_dot*sam_t_beta/100 ) ;    // r=1/1000  */
        tempW2 = beta_pseudo_old; tempW1 = sam_t_beta*time_pseudo; tempW0 = 10000;
        s16muls16divs16();
        tempW4 = tempW3;
        tempW2 = beta_dot; tempW1 = sam_t_beta; tempW0 = 10000;
        s16muls16divs16();
        beta_pseudo = (beta_pseudo_old - tempW4) + tempW3;

   /* }*/

}
void LDESP_CompDetBetaDotForSlip(void)
{

    det_beta_dot_old = det_beta_dot;

    abs_beta_dot = McrAbs(beta_dot);

    if ( abs_beta_dot >= det_beta_dot )
    {
        det_beta_dot = LCESP_s16Lpf1Int(abs_beta_dot, det_beta_dot_old, (uint8_t)L_U8FILTER_GAIN_10MSLOOP_10_5HZ);
    }
    else
    {
        det_beta_dot = LCESP_s16Lpf1Int(abs_beta_dot, det_beta_dot_old, (UCHAR)11);
    }
    /*
		esp_tempW1 = det_beta_dot2 ;
		
    if ( abs_beta_dot >= det_beta_dot2 )
    {
        det_beta_dot2 = LCESP_s16Lpf1Int(abs_beta_dot, esp_tempW1, (UCHAR)40);
    }
    else
    {
    	
    	esp_tempW2 = LCESP_s16IInter3Point( det_wstr_dot2, 	0,   	10, 
        					  					            1000,	5,						                      
        				                					2000, 1 ) ;		  
    				                          			
        det_beta_dot2 = esp_tempW1 - esp_tempW2  ;
        
		    if ( det_beta_dot2 < 0 )
		    {
		        det_beta_dot = 0 ;
		    }
		    else
		    {
		    	;
		    }        
    }
    */
		/* '11.05.16 : Ay/Vx signal  */    

		esp_tempW4 = (int16_t)((((int32_t)(ay_vx_sig - ay_vx_sig_old))*1000)/7); /* Yaw (+) */
		
		    if ( esp_tempW4 > 7000 )
		    {
		        esp_tempW4 = 7000 ;
		    }
		    else if ( esp_tempW4 < -7000 )
		    {
		    	esp_tempW4 = -7000;
		    }		    
		    else
		    {
		    	;
		    }   		 
		
		esp_tempW5 = -esp_tempW4; /* Yaw (-) */
		

		/* '11.05.24 : Ay/Vx signal filter by Ay */   

		esp_tempW7 = LCESP_s16IInter3Point( McrAbs(alat), 650,  (int16_t)L_U8FILTER_GAIN_10MSLOOP_2HZ,
    					  							   750,	 (int16_t)L_U8FILTER_GAIN_10MSLOOP_0_5HZ,						                      
    				                				  1000,  (int16_t)L_U8FILTER_GAIN_10MSLOOP_0_2HZ ) ;		  				
				
		ay_vx_dot_sig_fltr  = LCESP_s16Lpf1Int(esp_tempW4, ay_vx_dot_sig_fltr, (uint8_t)esp_tempW7);    // 11 		    
		ay_vx_dot_sig_fltr2 = LCESP_s16Lpf1Int(esp_tempW5, ay_vx_dot_sig_fltr2, (uint8_t)esp_tempW7);    		     				           
            				    
    if ( ay_vx_dot_sig_fltr >= ay_vx_dot_sig_plus )
    {
        ay_vx_dot_sig_plus = ay_vx_dot_sig_fltr ;
        esc_debugger = 1;
    }
    else
    {
    	
    	esp_tempW2 = LCESP_s16IInter3Point( det_wstr_dot2, 	0,   	15, 
    					  								 												1000,	  10,						                      
    				                					 									2000,    6 ) ;		  
    				                          			
        ay_vx_dot_sig_plus = ay_vx_dot_sig_plus - esp_tempW2  ;
        
		    if ( ay_vx_dot_sig_plus < 1 )
		    {
		        ay_vx_dot_sig_plus = 1 ;
		        esc_debugger = 2;
		    }
		    else
		    {
		    	esc_debugger = 3;
		    }        
    }
        
    if ( ay_vx_dot_sig_fltr2 >= ay_vx_dot_sig_minus )
    {
        ay_vx_dot_sig_minus = ay_vx_dot_sig_fltr2 ;
        esc_debugger = esc_debugger + 10;
    }
    else
    {
    	
    	esp_tempW2 = LCESP_s16IInter3Point( det_wstr_dot2, 	0,   	15, 
    					  								 												1000,	  10,						                      
    				                					 									2000,    6 ) ;		
    				                          			
        ay_vx_dot_sig_minus = ay_vx_dot_sig_minus - esp_tempW2  ;
        
		    if ( ay_vx_dot_sig_minus < 1 )
		    {
		        ay_vx_dot_sig_minus = 1 ;
		        esc_debugger = esc_debugger + 20;
		    }
		    else
		    {
		    	esc_debugger = esc_debugger + 30;
		    }        
    }         	           

}


#if __SIDE_SLIP_ANGLE_MODULE

void LDESP_CompSideSlipAngle(void)
{
    dot_yaw_fltr_old = dot_yaw_fltr ;       /* r=1/100*/
    xs1_beta_old = xs1_beta ;
    xs2_beta_old = xs2_beta ;
    xh1_beta_old = xh1_beta ;
    xh2_beta_old = xh2_beta ;
    yh1_beta_old = yh1_beta ;
    yh2_beta_old = yh2_beta ;
    d_xh1_beta_old = deriv_beta_est  ;
    xs1_beta_delay = beta_ref ;
    xh1_beta_delay = beta_est ;

    if (vrefm <= 40 )
    {
        vxm_beta = 40 ;     /* 15 kph  r=1/10  */
    }
    else
    {
        vxm_beta = vrefm;
    }

/*****  Calculation of Saturation force using Ay & dot_yaw  *****/
/*  dot_yaw = (int16_t)((int32_t)(yaw_out-yaw_out_old)*1000 /sam_t_beta) ;     // r=1/100  */
    tempW2 = yaw_out-yaw_out_old; tempW1 = 1000; tempW0 = sam_t_beta ;
    s16muls16divs16();
    dot_yaw = tempW3;
    dot_yaw_fltr = LCESP_s16Lpf1Int(dot_yaw, dot_yaw_fltr_old, L_U8FILTER_GAIN_10MSLOOP_1HZ);             /*  0.5 Hz, T = 0.3sec  */

/*    if(LOOP_TIME_20MS_TASK_1==1) {*/        /* Reduce Calculating Time...  */
/*   Ay Scaling ...    */
    sat_alat1 = alat  ;
    tempW2 = alat; tempW1 = 11; tempW0 = 10 ;
    s16muls16divs16();
    sat_alat2 = tempW3;
/*    }*/

/* Saturation Force : Lr(160,0.01),Lf(140,0.01),mass(1600,1),alat(1000,0.001),iiz(3600,1),dot_yaw(20000,0.01)*/
/*  Ff_sat_force = (int16_t)(( (int32_t)Lr_beta*mass_beta*sat_alat1/1000 +
                            (int32_t)iiz_beta*dot_yaw_fltr/570  ) / (Lf_beta + Lr_beta)) ; // r=10
    Fr_sat_force = (int16_t)(( (int32_t)Lf_beta*mass_beta*sat_alat2/1000 -
                            (int32_t)iiz_beta*dot_yaw_fltr/570  ) / (Lf_beta + Lr_beta)) ; // r=10  */
/*    if(LOOP_TIME_20MS_TASK_1==1) {*/    /* Reduce Calculating Time...  */
    tempW2 = mass_beta  ; tempW1 = sat_alat1  ; tempW0 = 100 ;
    s16muls16divs16();
    esp_tempW1 = tempW3;
    tempW2 = esp_tempW1  ; tempW1 = Lr_beta  ; tempW0 = 10*(Lf_beta + Lr_beta) ;
    s16muls16divs16();
    esp_tempW2 = tempW3;
    tempW2 = iiz_beta  ; tempW1 = dot_yaw_fltr  ; tempW0 = 10*(Lf_beta + Lr_beta) ;
    s16muls16divs16();
    esp_tempW3 = tempW3;
    tempW2 = esp_tempW3 ; tempW1 = 1  ; tempW0 = 57 ;
    s16muls16divs16();
    esp_tempW4 = tempW3;
    Ff_sat_force = esp_tempW2 + esp_tempW4 ;        /*  r = 10  */

    tempW2 = mass_beta  ; tempW1 = sat_alat2  ; tempW0 = 100 ;
    s16muls16divs16();
    esp_tempW1 = tempW3;
    tempW2 = esp_tempW1  ; tempW1 = Lf_beta  ; tempW0 = 10*(Lf_beta + Lr_beta) ;
    s16muls16divs16();
    esp_tempW2 = tempW3;
    Fr_sat_force = esp_tempW2 - esp_tempW4 ;        /*  r = 10  */

/*****  Overflow prevention *****/
        if (Ff_sat_force > 30000)
        {
             Ff_sat_force = 30000 ;
        }
        else if (Ff_sat_force < -30000)
        {
            Ff_sat_force = -30000 ;
        }
        else
        {
            Ff_sat_force = Ff_sat_force ;
        }

        if (Fr_sat_force > 30000)
        {
             Fr_sat_force = 30000 ;
        }
        else if (Fr_sat_force < -30000)
        {
            Fr_sat_force = -30000 ;
        }
        else
        {
            Fr_sat_force = Fr_sat_force ;
        }
/*    }*/
/*****   Reference Beta   *****/
/*     if(LOOP_TIME_20MS_TASK_1==1) {*/    /*  Reduce Calculating Time... */
/*  alpha_f_s = (int16_t)( (int32_t)wstr*(-10)/Stratio_beta +
                 (int32_t)xs1_beta_old/10 +
                 (int32_t)Lf_beta*xs2_beta_old/vxm_beta/10 ) ;     // r = 1/100  */
        tempW2 = wstr; tempW1 = -10; tempW0 = Stratio_beta ;    /* wstr(0~5400),Stratio = 17  */
        s16muls16divs16();
        esp_tempW5 = tempW3;                                    /* ~ 3176  */
        tempW2 = Lf_beta; tempW1 = xs2_beta_old; tempW0 = vxm_beta*10   ; /* Lf(120,0.01),xs2(15000,0.01),vxm(80,0.1)  */
        s16muls16divs16();
        esp_tempW6 = tempW3;                                    /* ~ 2250  */
        tempW2 = xs1_beta_old; tempW1 = 1; tempW0 = 10;         /* xs1(2500,0.001)  */
        s16muls16divs16();
        esp_tempW7 = tempW3;                                    /* ~ 250  */
        alpha_f_s = esp_tempW5 + esp_tempW6 + esp_tempW7;       /* r = 1/100  , Max = 6000   */

/*  alpha_r_s = (int16_t)( (int32_t)xs1_beta_old/10 -
                   (int32_t)Lr_beta*xs2_beta_old/vxm_beta/10 ) ;        // r = 1/100       */
        tempW2 = Lr_beta ; tempW1 = xs2_beta_old  ; tempW0 = vxm_beta*10 ;  //Lr(140,0.01),xs2(15000,0.01),vxm(80,0.1)
        s16muls16divs16();
        esp_tempW8 = tempW3;                                    /* ~2625  */
        alpha_r_s = esp_tempW7 - esp_tempW8  ;                  /* r = 1/100 , Max = 3000  */

/*  Fy_front_s = (int16_t)( (int32_t)Cf_beta_i*alpha_f_s/5730 ) ;      // r = 10   */
        tempW2 = Cf_beta_i  ; tempW1 = alpha_f_s  ; tempW0 = 5730 ; // Cf(10000,10), alpha_f(6000,1/100),tempW0(1/100)
        s16muls16divs16();
        Fy_front_s = tempW3;            /* r = 10  ,  Max = 11000    */

/*  Fy_rear_s  = (int16_t)( (int32_t)Cr_beta_i*alpha_r_s/5730 ) ;      // r = 10   */
        tempW2 = Cr_beta_i  ; tempW1 = alpha_r_s  ; tempW0 = 5730 ; /* Cr(12000,10), alpha_r(3000,1/100),tempW0(1/100)  */
        s16muls16divs16();
        Fy_rear_s = tempW3;             /* r = 10  ,  Max = 6500  */
/*****  Overflow prevention *****/
        if (Fy_front_s > 30000)
        {
             Fy_front_s = 30000 ;
        }
        else if (Fy_front_s < -30000)
        {
            Fy_front_s = -30000 ;
        }
        else
        {
            Fy_front_s = Fy_front_s ;
        }

        if (Fy_rear_s > 30000)
        {
             Fy_rear_s = 30000 ;
        }
        else if (Fy_rear_s < -30000)
        {
            Fy_rear_s = -30000 ;
        }
        else
        {
            Fy_rear_s = Fy_rear_s ;
        }
/*    }*/

/*  xs1_beta = (int16_t)( (int32_t)(Fy_front_s + Fy_rear_s)*5700*sam_t_beta/mass_beta/vxm_beta -
                 (int32_t)xs2_beta_old*sam_t_beta/100 +
                 (int32_t)xs1_beta_old );              // 1/1000       */
        tempW2 = Fy_front_s + Fy_rear_s ; tempW1 = 57*sam_t_beta ; tempW0 = mass_beta; /* Fy_total(20000,10),sam_t(7,0.001),mass(1300,1)  */
        s16muls16divs16();
        esp_tempW4 = tempW3 ;
        tempW2 = esp_tempW4 ; tempW1 = 100  ; tempW0 = vxm_beta ;
        s16muls16divs16() ;
        esp_tempW5 = tempW3 ;   /* r=0.01  */
        tempW2 = xs2_beta_old ; tempW1 = sam_t_beta  ;  tempW0 = 100 ; /* xs2(20000,0.01),sam_t(7,0.001)  */
        s16muls16divs16() ;
        esp_tempW6 = tempW3 ;   /* r=0.01  */
        xs1_beta = esp_tempW5 - esp_tempW6 + xs1_beta_old  ;            /* r=0.001  */
/*****  Overflow prevention *****/
        if (xs1_beta > 30000)                   /* 30deg  */
        {
             xs1_beta = 30000 ;
        }
        else if (xs1_beta < -30000)
        {
            xs1_beta = -30000 ;
        }
        else
        {
            xs1_beta = xs1_beta ;
        }

/*  xs2_beta = (int16_t)( ((int32_t)Lf_beta*Fy_front_s*sam_t_beta -
                     (int32_t)Lr_beta*Fy_rear_s*sam_t_beta)*57/iiz_beta/100 +
                 (int32_t)xs2_beta_old  )  ;            // 1/100   */
        tempW2 = Lf_beta*sam_t_beta ; tempW1 = Fy_front_s  ; tempW0 = iiz_beta ; /* Lf(140,0.01), sam_t(7,0.001), Fy(14000,10),iiz(2400,1)  */
        s16muls16divs16() ;
        esp_tempW4 = tempW3   ;
        tempW2 = Lr_beta*sam_t_beta ; tempW1 = Fy_rear_s  ; tempW0 = iiz_beta ;
        s16muls16divs16() ;
        esp_tempW5 = tempW3   ;
        tempW2 = ( esp_tempW4 - esp_tempW5 ); tempW1 = 57; tempW0 = 100;
        s16muls16divs16() ;
        esp_tempW6 = tempW3   ;     /* r=0.01  */
        xs2_beta = esp_tempW6 + xs2_beta_old   ;             /* 1/100  */
/*****  Overflow prevention *****/
        if (xs2_beta > 30000)
        {
             xs2_beta = 30000 ;
        }
        else if (xs2_beta < -30000)
        {
            xs2_beta = -30000 ;
        }
        else
        {
            xs2_beta = xs2_beta ;
        }

/*  ys1_beta = (int16_t)((int32_t)(Fy_front_s + Fy_rear_s)*1020/mass_beta) ;        // r = 1/1000  */
        tempW2 = Fy_front_s + Fy_rear_s;  tempW1 = 1020;  tempW0 = mass_beta ;  /* Fy_total = (25000,10), mass(1400,1)  */
        s16muls16divs16() ;
        esp_tempW5 = tempW3   ;
        ys1_beta = esp_tempW5 ;                 /* r=1/1000 */
        ys2_beta = xs2_beta ;                   /* r=1/100  */

/*****   Estimation Beta using Observer  *****/
/*    if(LOOP_TIME_20MS_TASK_1==1) {*/    /* Reduce Calculating Time...  */
/*      alpha_f_o = (int16_t)( (int32_t)wstr*(-10)/Stratio_beta +
                   (int32_t)xh1_beta_old/10 +
                   (int32_t)Lf_beta*xh2_beta_old/vxm_beta/10 ) ;  // r = 1/100     */
        tempW2 = wstr ; tempW1 = -10  ; tempW0 = Stratio_beta ;
        s16muls16divs16();
        esp_tempW5 = tempW3;
        tempW2 = Lf_beta ; tempW1 = xh2_beta_old  ; tempW0 = vxm_beta*10   ;
        s16muls16divs16();
        esp_tempW6 = tempW3;
        tempW2 = xh1_beta_old; tempW1 = 1; tempW0 = 10;
        s16muls16divs16();
        esp_tempW7 = tempW3;
        alpha_f_o = esp_tempW5 +  esp_tempW6  +  esp_tempW7;        /* r = 1/100   */

/*      alpha_r_o = (int16_t)( (int32_t)xh1_beta_old/10 -
                   (int32_t)Lr_beta*xh2_beta_old/vxm_beta/10 ) ;  // r = 1/100         */
        tempW2 = Lr_beta ; tempW1 = xh2_beta_old  ; tempW0 = vxm_beta*10   ;
        s16muls16divs16();
        esp_tempW8 = tempW3;
        alpha_r_o = esp_tempW7 - esp_tempW8;                /*  r = 1/100  */

/*      Fy_front_o = (int16_t)( (int32_t)Cf_beta_i*alpha_f_o/5730 ) ;  // r = 10           */
        tempW2 = Cf_beta_i  ; tempW1 = alpha_f_o  ; tempW0 = 5730   ;
        s16muls16divs16();
        Fy_front_o = tempW3;            /* r = 10  */

/*      Fy_rear_o = (int16_t)( (int32_t)Cr_beta_i*alpha_r_o/5730 ) ;   // r = 10           */
        tempW2 = Cr_beta_i  ; tempW1 = alpha_r_o  ; tempW0 = 5730   ;
        s16muls16divs16();
        Fy_rear_o = tempW3;         /* r = 10        */

/*****  Overflow prevention *****/
        if (Fy_front_o > 30000)
        {
             Fy_front_o = 30000 ;
        }
        else if (Fy_front_o < -30000)
        {
            Fy_front_o = -30000 ;
        }
        else
        {
            Fy_front_o = Fy_front_o;
        }

        if (Fy_rear_o > 30000)
        {
             Fy_rear_o = 30000 ;
        }
        else if (Fy_rear_o < -30000)
        {
            Fy_rear_o = -30000 ;
        }
        else
        {
            Fy_rear_o = Fy_rear_o ;
        }

        Ff_observe = Fy_front_o ;
        Fr_observe = Fy_rear_o  ;
/*    }*/

/*****  Saturation : front force  *****/
    if (vrefm >= 55 )       /* 20 kph */
    {
        if ((Fy_front_o > 0)&&(Ff_sat_force > 0))
        {
            sign_fyf = 1 ;
        }
        else if ((Fy_front_o < 0)&&(Ff_sat_force < 0))
        {
            sign_fyf = 1 ;
        }
        else
        {
            sign_fyf = -1 ;
        }

    if ( sign_fyf > 0 ) {
        if (McrAbs(Fy_front_o) > McrAbs(Ff_sat_force))
        {
            Fyf_pre = Ff_sat_force    ;
        }
        else
        {
            Fyf_pre = Fy_front_o        ;
        }
    }
    else
    {
            Fyf_pre = Fy_front_o        ;
    }

    if ((abs_wstr<100) && (det_wstr_dot_lf<100))    /* 10deg  */
    {
            Fyf_pre = Fy_front_o        ;
    }
    else
    {
        ;
    }

/*****  Saturation : rear force  *****/
    if ((Fy_rear_o > 0)&&(Fr_sat_force > 0))
    {
            sign_fyr = 1 ;
    }
    else if ((Fy_rear_o < 0)&&(Fr_sat_force < 0))
    {
            sign_fyr = 1 ;
    }
    else
    {
            sign_fyr = -1 ;
    }

    if ( sign_fyr > 0 ) {
        if (McrAbs(Fy_rear_o) > McrAbs(Fr_sat_force))
        {
            Fyr_pre = Fr_sat_force    ;
        }
        else
        {
            Fyr_pre = Fy_rear_o        ;
        }
    }
    else
    {
            Fyr_pre = Fy_rear_o        ;
    }

    if ((abs_wstr<100) && (det_wstr_dot_lf<100))    /* 10deg  */
            Fyr_pre = Fy_rear_o        ;

    Fy_front_o = Fyf_pre ;
    Fy_rear_o  = Fyr_pre ;
    }

/*  xh1_beta = (int16_t)( (int32_t)(Fy_front_o+Fy_rear_o)*5700*sam_t_beta/mass_beta/vxm_beta -      //  1/1000
                      (int32_t)xh2_beta_old*sam_t_beta/100 +
                     ((int32_t)alat*98*57/100 - (int32_t)yh1_beta_old*98*57/100)*L11_beta*sam_t_beta/100000 +
                     ((int32_t)yaw_out-(int32_t)yh2_beta_old)*L12_beta*sam_t_beta/100000 + (int32_t)xh1_beta_old ) ;     */
        tempW2 = Fy_front_o+Fy_rear_o ; tempW1 = 57*sam_t_beta  ; tempW0 = mass_beta  ;
        s16muls16divs16();
        esp_tempW4 = tempW3;
        tempW2 = esp_tempW4 ; tempW1 = 100  ; tempW0 = vxm_beta ;
        s16muls16divs16() ;
        esp_tempW5 = tempW3   ;     /* r=0.001  */
        tempW2 = xh2_beta_old ; tempW1 = sam_t_beta  ; tempW0 = 100 ;
        s16muls16divs16() ;
        esp_tempW6 = tempW3   ;     /* r=0.001   */
        tempW2 = alat - yh1_beta_old ; tempW1 = L11_beta*sam_t_beta  ; tempW0 = 1790 ; /* alat(1000),yh1(1200,0.001),L11(100,0.001),sam_t(7,0.001)  */
        s16muls16divs16() ;
        esp_tempW7 = tempW3   ;     /* r=0.001  */
        tempW2 = yaw_out - yh2_beta_old ; tempW1 = L12_beta ; tempW0 = 1000 ; /* yaw(20000,0.01),L12(3000,0.001)  */
        s16muls16divs16() ;
        esp_tempW8 = tempW3   ;
        tempW2 = esp_tempW8 ; tempW1 = sam_t_beta ; tempW0 = 100;
        s16muls16divs16() ;
        esp_tempW9 = tempW3   ;     /* r=0.001 */
        xh1_beta = esp_tempW5 - esp_tempW6 + esp_tempW7 + esp_tempW9 + xh1_beta_old   ;          /*  1/1000 */

/*****  Overflow prevention *****/
        if (xh1_beta > 30000)                   /* 30deg  */
        {
             xh1_beta = 30000 ;
        }
        else if (xh1_beta < -30000)
        {
            xh1_beta = -30000 ;
        }
        else
        {
            xh1_beta = xh1_beta ;
        }

/*  xh2_beta = (int16_t)(  ((int32_t)Lf_beta*Fy_front_o*sam_t_beta - (int32_t)Lr_beta*Fy_rear_o*sam_t_beta)*57/iiz_beta/100 +
                       ((int32_t)alat*98*57/100 - (int32_t)yh1_beta_old*98*57/100)*L21_beta*sam_t_beta/1000000 +
                       ((int32_t)yaw_out - (int32_t)yh2_beta_old)*L22_beta*sam_t_beta/1000000  +
                        (int32_t)xh2_beta_old  ) ;  // 1/100               */
        tempW2 = Lf_beta*sam_t_beta ; tempW1 = Fy_front_o  ; tempW0 = iiz_beta ;
        s16muls16divs16() ;
        esp_tempW4 = tempW3   ;
        tempW2 = Lr_beta*sam_t_beta ; tempW1 = Fy_rear_o  ; tempW0 = iiz_beta ;
        s16muls16divs16() ;
        esp_tempW5 = tempW3   ;
        tempW2 = esp_tempW4 - esp_tempW5; tempW1 = 57; tempW0 = 100 ;
        s16muls16divs16() ;
        esp_tempW6 = tempW3;            /* r=0.01  */
        tempW2 = alat - yh1_beta_old ;  tempW1 = L21_beta*sam_t_beta  ; tempW0 = 17902 ;
        s16muls16divs16() ;
        esp_tempW7 = tempW3   ;         /* r=0.01  */
        tempW2 = yaw_out - yh2_beta_old ; tempW1 = L22_beta   ; tempW0 = 10000 ;
        s16muls16divs16() ;
        esp_tempW8 = tempW3   ;
        tempW2 = esp_tempW8; tempW1 = sam_t_beta; tempW0 = 100 ;
        s16muls16divs16() ;
        esp_tempW9 = tempW3;            /* r=0.01  */
        xh2_beta = esp_tempW6 + esp_tempW7 +  esp_tempW9 + xh2_beta_old   ;      /* 1/100 */
/*****  Overflow prevention *****/
        if (xh2_beta  > 30000)
        {
             xh2_beta  = 30000 ;
        }
        else if (xh2_beta  < -30000)
        {
            xh2_beta  = -30000 ;
        }
        else
        {
            xh2_beta  = xh2_beta  ;
        }

/*  yh1_beta = (int16_t)((int32_t)(Fy_front_o + Fy_rear_o)*1020/mass_beta) ;   // r = 1/1000  */
        tempW2 = Fy_front_o + Fy_rear_o;  tempW1 = 1020; tempW0 = mass_beta ;
        s16muls16divs16() ;
        esp_tempW5 = tempW3   ;
        yh1_beta = esp_tempW5 ;                         /* r = 1/1000   */
        yh2_beta = xh2_beta;                            /* r = 1/100  */

/*****  Calculate Beta_Dot  *****/
/*   d_xh1_beta = (int16_t)( (int32_t)(Fy_front_o+Fy_rear_o)*570/mass_beta*1000/vxm_beta -     //  1/100
                         (int32_t)xh2_beta_old +
                         (int32_t)(alat - yh1_beta_old)*98*57/100*L11_beta/1000 +
                         (int32_t)(yaw_out-yh2_beta_old)*L12_beta/1000 ) ;  */
        tempW2 = Fy_front_o+Fy_rear_o ; tempW1 = 570  ; tempW0 = mass_beta ;
        s16muls16divs16();
        esp_tempW4 = tempW3;
        tempW2 = esp_tempW4 ; tempW1 = 1000  ; tempW0 = vxm_beta ;
        s16muls16divs16() ;
        esp_tempW5 = tempW3   ;         /* r=0.01   */
        tempW2 = alat - yh1_beta_old ; tempW1 = L11_beta*10  ; tempW0 = 178 ;
        s16muls16divs16() ;
        esp_tempW6 = tempW3   ;         /*  r=0.01   */
        tempW2 = yaw_out-yh2_beta_old ; tempW1 = L12_beta  ; tempW0 = 1000 ;
        s16muls16divs16() ;
        esp_tempW7 = tempW3   ;         /* r=0.01   */
        d_xh1_beta = esp_tempW5 - xh2_beta_old + esp_tempW6 + esp_tempW7 ;        /*  1/100  */

/*******  Signal Processing : Digital Filter  *******/
/*      Fc=1.1Hz(T=0.15,122,6) | Fc=2Hz(T=0.081,117,11) | Fc=3Hz(T=0.053,111,17)        */
        beta_ref        = LCESP_s16Lpf1Int(xs1_beta, xs1_beta_delay, L_U8FILTER_GAIN_10MSLOOP_1HZ);          /* Key Var, r = 1/1000 */
        deriv_beta_est  = LCESP_s16Lpf1Int(d_xh1_beta, d_xh1_beta_old, L_U8FILTER_GAIN_10MSLOOP_1HZ);        /* Key Var, r = 1/100 */
        beta_est        = LCESP_s16Lpf1Int(xh1_beta, xh1_beta_delay, L_U8FILTER_GAIN_10MSLOOP_1HZ);          /* Key Var, r = 1/1000 */
/*      front_beta_est  = (int16_t)( (int32_t)beta_est/10 + (int32_t)L_front*yh2_beta/vrefm/10 ) ;  r = 1/100  */
        tempW2 = beta_est; tempW1 = 1; tempW0 = 10 ;
        s16muls16divs16() ;
        esp_tempW1 = tempW3;
        tempW2 = L_front; tempW1 = yh2_beta; tempW0 = vrefm*10 ;
        s16muls16divs16() ;
        esp_tempW2 = tempW3;
        front_beta_est = esp_tempW1 + esp_tempW2;

/*****  Overflow prevention *****/
        if (front_beta_est > 30000)
        {
             front_beta_est = 30000 ;
        }
        else if (front_beta_est < -30000)
        {
            front_beta_est = -30000 ;
        }
        else
        {
            front_beta_est = front_beta_est ;
        }
}

#endif

#if MGH60_NEW_TARGET_SLIP ==0
void LDESP_DetectDriftForSlip(void)
{
	#define   	S16_DRIFT_BETA_DOT_SLIP_ENTER         		     600                                            	        	
	#define   	S16_DRIFT_BETA_DOT_SLIP_EXIT          		     100
	
/*    LDESP_CompDetBetaDotForSlip(); */

    esp_tempW0 = LCESP_s16IInter3Point(vrefk, 200, 1800, 500, 900, 650,S16_DRIFT_BETA_DOT_SLIP_ENTER);
    esp_tempW1 = LCESP_s16IInter3Point(vrefk, 200, 1000, 500, 400, 650,S16_DRIFT_BETA_DOT_SLIP_EXIT);

    esp_tempW2 = McrAbs(yaw_out);

    if((FLAG_BANK_DETECTED==0)&&(ABS_fz==0)&&(FLAG_COUNTER_STEER==0)
        &&(PRE_ESP_CONTROL==0)&&(((beta_dot_dot < 0 )&&(yaw_out > YAW_6DEG))||(((beta_dot_dot > 0 )&&(yaw_out < (-YAW_6DEG)))))
        &&(abs_wstr <= 3000)&&(vrefk >= 300)
        &&(det_beta_dot>=esp_tempW0)&&(abs_wstr >= 500)
        &&((det_alatm<lsesps16MuMedHigh)&&(det_alatm>=lsesps16MuMed)&&(vrefk <= 1200)))
    {
        FLAG_DETECT_DRIFT = 1;

    }
    else if((FLAG_BANK_DETECTED==0)&&(FLAG_DETECT_DRIFT==1)&&(det_beta_dot >= esp_tempW1)
        &&(vrefk >= 100)
        &&(FLAG_COUNTER_STEER==0)
        &&(det_alatm>=lsesps16MuMed)&&(esp_tempW2 >=YAW_3DEG)&&(abs_wstr >= 500))
    {
        FLAG_DETECT_DRIFT = 1;
    }
    else
    {
    	FLAG_DETECT_DRIFT = 0;
    }

}
#endif

/* ====================================================================== */
void LDESP_DetectBetaUnderDrift(void)
{

	if(ABS_fz==1) {

		esp_tempW1 = LCESP_s16IInter3Point( vrefk, 150, 2500,
                                          300, 900,
                                          500, 400 ); /* enter beta_M  r: 0.01*/

	    esp_tempW2 = LCESP_s16IInter3Point( vrefk, 150, 2000,
                                          300, 500,
                                          500, 300 ); /* exit beta_M   r: 0.01*/

	    esp_tempW3 = LCESP_s16IInter3Point( vrefk, 150, 700,
                                          300, 500,
                                          500, 400 ); /* enter beta_K_dot  r: 0.01*/

	    esp_tempW4 = LCESP_s16IInter3Point( vrefk, 150, 350,
                                          300, 350,
                                          500, 300 ); /* enter beta_K_dot  r: 0.01*/
	}
	else
	{

	    esp_tempW1 = LCESP_s16IInter3Point( vrefk, VREF_K_15_KPH, 2500,
                                          (int16_t)U16_DET_DRF_SP_M, (int16_t)U16_DET_DRF_BETA_ENT_M,
                                          (int16_t)U16_DET_DRF_SP_H, (int16_t)U16_DET_DRF_BETA_ENT_H ); /* enter beta_M  r: 0.01*/

	    esp_tempW2 = LCESP_s16IInter3Point( vrefk, VREF_K_15_KPH, 2000,
                                          (int16_t)U16_DET_DRF_SP_M, (int16_t)U16_DET_DRF_BETA_EXT_M,
                                          (int16_t)U16_DET_DRF_SP_H, (int16_t)U16_DET_DRF_BETA_EXT_H ); /* exit beta_M   r: 0.01*/

	    esp_tempW3 = LCESP_s16IInter3Point( vrefk, VREF_K_15_KPH, 700,
                                          (int16_t)U16_DET_DRF_SP_M, (int16_t)U16_DET_DRF_BETA_D_ENT_M,
                                          (int16_t)U16_DET_DRF_SP_H, (int16_t)U16_DET_DRF_BETA_D_ENT_H ); /* enter beta_K_dot  r: 0.01*/

	    esp_tempW4 = LCESP_s16IInter3Point( vrefk, VREF_K_15_KPH, 350,
                                          (int16_t)U16_DET_DRF_SP_M, 350,
                                          (int16_t)U16_DET_DRF_SP_H, 300 ); /* enter beta_K_dot  r: 0.01*/
   }

	 if ( esp_tempW1 < 300 )
	 {
	 	esp_tempW1 = 300;
	 }
	 else
	 {
	 	;
	 }


 	if ( esp_tempW1  > ( esp_tempW2 + 100 ) )
 	{
		;
 	}
 	else
 	{
 		esp_tempW2 = esp_tempW1 - 100;
 	}

 	 /* '06 LPG */
 	 esp_tempW3 = LCESP_s16IInter3Point( det_wstr_dot, 500, esp_tempW3-150,
    	                                         1000, esp_tempW3-50,
    	                                         1500, esp_tempW3);
	 if ( esp_tempW3 < 300 )
	 {
	 	esp_tempW3 = 300;
 	}
 	else
 	{
 		;
 	}


  if((FLAG_BANK_DETECTED==0)&&(FLAG_BANK_SUSPECT==0)&&(VDC_DISABLE_FLG==0)
/*    	&&(((det_alatm<lsesps16MuHigh)&&(!FLAG_BETA_UNDER_DRIFT))||((det_alatm<(lsesps16MuHigh+100))&&(FLAG_BETA_UNDER_DRIFT))) */
    	&&(((det_alatm<1200)&&(!FLAG_BETA_UNDER_DRIFT))||((det_alatm<(1200+100))&&(FLAG_BETA_UNDER_DRIFT)))
    	&&(((vrefk <= VREF_K_150_KPH)&&(!FLAG_BETA_UNDER_DRIFT))||((vrefk <= VREF_K_180_KPH)&&(FLAG_BETA_UNDER_DRIFT))) )
    {

		if ( (yaw_out >= YAW_5DEG) && (Beta_MD <= -esp_tempW1) && (Beta_K_Dot <= -esp_tempW3) )
		{
			FLAG_BETA_UNDER_DRIFT = 1;
        }
        else if ( (yaw_out < -YAW_5DEG) && (Beta_MD >= esp_tempW1) && (Beta_K_Dot >= esp_tempW3) )
        {
			FLAG_BETA_UNDER_DRIFT = 1;
        }
        else if ( (FLAG_BETA_UNDER_DRIFT==1) && (Beta_MD > -esp_tempW2) && (Beta_MD < esp_tempW2) )
        {
			FLAG_BETA_UNDER_DRIFT = 0;
        }
        else if ( vrefk < VREF_K_6_KPH )
        {
			FLAG_BETA_UNDER_DRIFT = 0;
		}
		else
		{
			;
		}

    }
    else
    {
    	FLAG_BETA_UNDER_DRIFT = 0;
	}

    FLAG_DETECT_DRIFT = FLAG_BETA_UNDER_DRIFT;
}

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
void LDESP_vDet1stCycle(void)
{
#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
	if((lsespu1AHBGEN3MpresBrkOn == 0)
		||((lsespu1AHBGEN3MpresBrkOn==1)&&(ESP_SPLIT==0))
		||((lsespu1AHBGEN3MpresBrkOn==1)&&(ESP_SPLIT==1)&&(SLIP_CONTROL_NEED_FL==1)&&(RR.MSL_BASE==1))
		||((lsespu1AHBGEN3MpresBrkOn==1)&&(ESP_SPLIT==1)&&(SLIP_CONTROL_NEED_RL==1)&&(RR.MSL_BASE==1))
		||((lsespu1AHBGEN3MpresBrkOn==1)&&(ESP_SPLIT==1)&&(SLIP_CONTROL_NEED_FR==1)&&(RL.MSL_BASE==1))
		||((lsespu1AHBGEN3MpresBrkOn==1)&&(ESP_SPLIT==1)&&(SLIP_CONTROL_NEED_RR==1)&&(RL.MSL_BASE==1))
		)
#else
	if((MPRESS_BRAKE_ON == 0)
		||((MPRESS_BRAKE_ON==1)&&(ESP_SPLIT==0))
		||((MPRESS_BRAKE_ON==1)&&(ESP_SPLIT==1)&&(SLIP_CONTROL_NEED_FL==1)&&(RR.MSL_BASE==1))
		||((MPRESS_BRAKE_ON==1)&&(ESP_SPLIT==1)&&(SLIP_CONTROL_NEED_RL==1)&&(RR.MSL_BASE==1))
		||((MPRESS_BRAKE_ON==1)&&(ESP_SPLIT==1)&&(SLIP_CONTROL_NEED_FR==1)&&(RL.MSL_BASE==1))
		||((MPRESS_BRAKE_ON==1)&&(ESP_SPLIT==1)&&(SLIP_CONTROL_NEED_RR==1)&&(RL.MSL_BASE==1))
		) /* '06 TRC */
#endif
	{
		LDESP_vSet1stCycleF(&FL_ESC);
		LDESP_vSet1stCycleF(&FR_ESC);
		
		LDESP_vSet1stCycleR(&RL_ESC);
		LDESP_vSet1stCycleR(&RR_ESC);
	}
	else
	{
		FL_ESC.FLAG_1ST_CYCLE = 0;
		FL_ESC.FLAG_1ST_CYCLE_PRE = 0;

		FR_ESC.FLAG_1ST_CYCLE = 0;
		FR_ESC.FLAG_1ST_CYCLE_PRE = 0;

		RL_ESC.FLAG_1ST_CYCLE = 0;
		RL_ESC.FLAG_1ST_CYCLE_PRE = 0;

		RR_ESC.FLAG_1ST_CYCLE = 0;
		RR_ESC.FLAG_1ST_CYCLE_PRE = 0;

/*        act_1st_cy_fl_cnt = 0; */
/*        act_1st_cy_fr_cnt = 0; */
    RL_ESC.act_1st_cy_cnt = 0;
    RR_ESC.act_1st_cy_cnt = 0;
	}
}

void LDESP_vSet1stCycleF(struct ESC_STRUCT *WL_ESC)
{
	esp_tempW1 = LCESP_s16FindRoadDependatGain(esp_mu, S16_1ST_CYCLE_EXIT_THR_H_MU_F,
  											S16_1ST_CYCLE_EXIT_THR_M_MU_F, S16_1ST_CYCLE_EXIT_THR_L_MU_F );
  esp_tempW8 = LCESP_s16FindRoadDependatGain(esp_mu, S16_1ST_CYCLE_ENTER_THR_H_MU_F,
  											S16_1ST_CYCLE_ENTER_THR_M_MU_F, S16_1ST_CYCLE_ENTER_THR_L_MU_F );
  esp_tempW0 =LCESP_s16IInter2Point( yaw_error, 700, 0, 1000, esp_tempW1 );
  
  if(WL_ESC->SLIP_CONTROL_NEED==1)
		{
			if((((yaw_error_dot  >= esp_tempW8)&&(yaw_error > (-1500)))||(OVER_UNDER_JUMP==1))
			&&(WL_ESC->FLAG_1ST_CYCLE==0)&&(WL_ESC->FLAG_1ST_CYCLE_PRE==0))
			{
				if(FLAG_ON_CENTER == 0)
				{
					WL_ESC->FLAG_1ST_CYCLE = 1;
					WL_ESC->FLAG_1ST_CYCLE_PRE =1;
				}
				else
				{
					WL_ESC->FLAG_1ST_CYCLE = 0;
					WL_ESC->FLAG_1ST_CYCLE_PRE =1;
				}
			}
			else if((WL_ESC->FLAG_1ST_CYCLE==1)&&((WL_ESC->SLIP_CONTROL_NEED==1)&&(yaw_error_dot  < (- esp_tempW0))))
			{
				WL_ESC->FLAG_1ST_CYCLE = 0;
				WL_ESC->FLAG_1ST_CYCLE_PRE =1;
			}
			else if((WL_ESC->FLAG_1ST_CYCLE==1)&&((WL_ESC->SLIP_CONTROL_NEED==1)&&(yaw_error_dot  >=(- esp_tempW0))))
			{
				WL_ESC->FLAG_1ST_CYCLE = 1;
				WL_ESC->FLAG_1ST_CYCLE_PRE =1;
			}
			else
			{
				WL_ESC->FLAG_1ST_CYCLE = 0;
			}

			if((WL_ESC->FLAG_1ST_CYCLE_WHEEL_PRE==1)&&(WL_ESC->FLAG_1ST_CYCLE_WHEEL==0))
			{
				WL_ESC->FLAG_1ST_CYCLE = 0;
			}
		}
		else
		{
			WL_ESC->FLAG_1ST_CYCLE = 0;
			WL_ESC->FLAG_1ST_CYCLE_PRE = 0;
		}
}

void LDESP_vSet1stCycleR(struct ESC_STRUCT *WL_ESC)
{
	esp_tempW2 = LCESP_s16FindRoadDependatGain(esp_mu, S16_1ST_CYCLE_EXIT_THR_H_MU_R,
  											S16_1ST_CYCLE_EXIT_THR_M_MU_R, S16_1ST_CYCLE_EXIT_THR_L_MU_R );
  esp_tempW9 = LCESP_s16FindRoadDependatGain(esp_mu, S16_1ST_CYCLE_ENTER_THR_H_MU_R,
  											S16_1ST_CYCLE_ENTER_THR_M_MU_R, S16_1ST_CYCLE_ENTER_THR_L_MU_R );
  
  /*  '08.07.01 : UnderControl -> 1st_Cycle  */	
	
    if ( lcu1espActive3rdFlg == 1 )
    {
    	esp_tempW6 = delta_yaw_first ; 
    	esp_tempW7 = yaw_error_under_dot ; 	
    }
    else
    {
    	esp_tempW6 = yaw_error ; 
    	esp_tempW7 = yaw_error_dot ; 	        
    }
    
    if(WL_ESC->SLIP_CONTROL_NEED==1)
		{
			if((esp_tempW7  <= (-esp_tempW9))&&(esp_tempW6 < 0)			
			&&(WL_ESC->FLAG_1ST_CYCLE==0)&&(WL_ESC->FLAG_1ST_CYCLE_PRE==0))
			{
				WL_ESC->FLAG_1ST_CYCLE = 1;
				WL_ESC->FLAG_1ST_CYCLE_PRE =1;
				WL_ESC->act_1st_cy_cnt ++ ;
			}
		#if __CRAM
			else if((WL_ESC->FLAG_1ST_CYCLE==1)&&(WL_ESC->SLIP_CONTROL_NEED==1)&&(esp_tempW7  > (-esp_tempW2))&&(WL_ESC->act_1st_cy_cnt > 42))		
		#else
			else if((WL_ESC->FLAG_1ST_CYCLE==1)&&(WL_ESC->SLIP_CONTROL_NEED==1)&&(esp_tempW7  > (-esp_tempW2)))		
		#endif
			{
				WL_ESC->FLAG_1ST_CYCLE = 0;
				WL_ESC->FLAG_1ST_CYCLE_PRE =1;
			}
		#if __CRAM
			else if((WL_ESC->FLAG_1ST_CYCLE==1)&&(WL_ESC->SLIP_CONTROL_NEED==1)&&(WL_ESC->act_1st_cy_cnt < 59)&&(esp_tempW7  < (-esp_tempW2)))		
		#else
			else if((WL_ESC->FLAG_1ST_CYCLE==1)&&(WL_ESC->SLIP_CONTROL_NEED==1)&&(esp_tempW7  < (-esp_tempW2)))		
		#endif
			{
				WL_ESC->FLAG_1ST_CYCLE = 1;
				WL_ESC->FLAG_1ST_CYCLE_PRE =1;
				WL_ESC->act_1st_cy_cnt ++ ;
			}
			else
			{
				WL_ESC->FLAG_1ST_CYCLE = 0;
			}

			if((WL_ESC->FLAG_1ST_CYCLE_WHEEL_PRE==1)&&(WL_ESC->FLAG_1ST_CYCLE_WHEEL==0))
			{
				WL_ESC->FLAG_1ST_CYCLE = 0;
			}
		}
		else
		{
			WL_ESC->FLAG_1ST_CYCLE = 0;
			WL_ESC->FLAG_1ST_CYCLE_PRE = 0;
	    WL_ESC->act_1st_cy_cnt = 0;
		}
}

#else
void LDESP_vDet1stCycle(void)
{

  esp_tempW1 = LCESP_s16FindRoadDependatGain(esp_mu, S16_1ST_CYCLE_EXIT_THR_H_MU_F,
  											S16_1ST_CYCLE_EXIT_THR_M_MU_F, S16_1ST_CYCLE_EXIT_THR_L_MU_F );
  esp_tempW2 = LCESP_s16FindRoadDependatGain(esp_mu, S16_1ST_CYCLE_EXIT_THR_H_MU_R,
  											S16_1ST_CYCLE_EXIT_THR_M_MU_R, S16_1ST_CYCLE_EXIT_THR_L_MU_R );
	
  esp_tempW8 = LCESP_s16FindRoadDependatGain(esp_mu, S16_1ST_CYCLE_ENTER_THR_H_MU_F,
  											S16_1ST_CYCLE_ENTER_THR_M_MU_F, S16_1ST_CYCLE_ENTER_THR_L_MU_F );
  esp_tempW9 = LCESP_s16FindRoadDependatGain(esp_mu, S16_1ST_CYCLE_ENTER_THR_H_MU_R,
  											S16_1ST_CYCLE_ENTER_THR_M_MU_R, S16_1ST_CYCLE_ENTER_THR_L_MU_R );
	
	
/*  '08.07.01 : UnderControl -> 1st_Cycle  */	
	
    if ( lcu1espActive3rdFlg == 1 )
    {
    	esp_tempW6 = delta_yaw_first ; 
    	esp_tempW7 = yaw_error_under_dot ; 	
    }
    else
    {
    	esp_tempW6 = yaw_error ; 
    	esp_tempW7 = yaw_error_dot ; 	        
    }
	
	esp_tempW0 =LCESP_s16IInter2Point( yaw_error, 700, 0, 1000, esp_tempW1 );
	esp_tempW3 =LCESP_s16IInter2Point( yaw_error, 500, 0, 1000, esp_tempW2 );
	
/*	if(((ESP_SPLIT==0)&&(MPRESS_BRAKE_ON == 1))||(MPRESS_BRAKE_ON == 0)) */
#if(__AHB_GEN3_SYSTEM==ENABLE) || (__IDB_LOGIC == ENABLE)
	if((lsespu1AHBGEN3MpresBrkOn == 0)
		||((lsespu1AHBGEN3MpresBrkOn==1)&&(ESP_SPLIT==0))
		||((lsespu1AHBGEN3MpresBrkOn==1)&&(ESP_SPLIT==1)&&(SLIP_CONTROL_NEED_FL==1)&&(RR.MSL_BASE==1))
		||((lsespu1AHBGEN3MpresBrkOn==1)&&(ESP_SPLIT==1)&&(SLIP_CONTROL_NEED_RL==1)&&(RR.MSL_BASE==1))
		||((lsespu1AHBGEN3MpresBrkOn==1)&&(ESP_SPLIT==1)&&(SLIP_CONTROL_NEED_FR==1)&&(RL.MSL_BASE==1))
		||((lsespu1AHBGEN3MpresBrkOn==1)&&(ESP_SPLIT==1)&&(SLIP_CONTROL_NEED_RR==1)&&(RL.MSL_BASE==1))
		)
#else
	if((MPRESS_BRAKE_ON == 0)
		||((MPRESS_BRAKE_ON==1)&&(ESP_SPLIT==0))
		||((MPRESS_BRAKE_ON==1)&&(ESP_SPLIT==1)&&(SLIP_CONTROL_NEED_FL==1)&&(RR.MSL_BASE==1))
		||((MPRESS_BRAKE_ON==1)&&(ESP_SPLIT==1)&&(SLIP_CONTROL_NEED_RL==1)&&(RR.MSL_BASE==1))
		||((MPRESS_BRAKE_ON==1)&&(ESP_SPLIT==1)&&(SLIP_CONTROL_NEED_FR==1)&&(RL.MSL_BASE==1))
		||((MPRESS_BRAKE_ON==1)&&(ESP_SPLIT==1)&&(SLIP_CONTROL_NEED_RR==1)&&(RL.MSL_BASE==1))
		) /* '06 TRC */
#endif
	{
		if(SLIP_CONTROL_NEED_FL==1)
		{
			if((((yaw_error_dot  >= esp_tempW8)&&(yaw_error > (-1500)))||(OVER_UNDER_JUMP==1))
			&&(FLAG_1ST_CYCLE_FL==0)&&(FLAG_1ST_CYCLE_FL_PRE==0))
			{
				if(FLAG_ON_CENTER == 0)
				{
					FLAG_1ST_CYCLE_FL = 1;
					FLAG_1ST_CYCLE_FL_PRE =1;
				}
				else
				{
					FLAG_1ST_CYCLE_FL = 0;
					FLAG_1ST_CYCLE_FL_PRE =1;
				}
			}
			else if((FLAG_1ST_CYCLE_FL==1)&&((SLIP_CONTROL_NEED_FL==1)&&(yaw_error_dot  < (- esp_tempW0))))
			{
				FLAG_1ST_CYCLE_FL = 0;
				FLAG_1ST_CYCLE_FL_PRE =1;
			}
			else if((FLAG_1ST_CYCLE_FL==1)&&((SLIP_CONTROL_NEED_FL==1)&&(yaw_error_dot  >=(- esp_tempW0))))
			{
				FLAG_1ST_CYCLE_FL = 1;
				FLAG_1ST_CYCLE_FL_PRE =1;
			}
			else
			{
				FLAG_1ST_CYCLE_FL = 0;
			}

			if((FLAG_1ST_CYCLE_WHEEL_FL_PRE==1)&&(FLAG_1ST_CYCLE_WHEEL_FL==0))
			{
				FLAG_1ST_CYCLE_FL = 0;
			}
		}
		else
		{
			FLAG_1ST_CYCLE_FL = 0;
			FLAG_1ST_CYCLE_FL_PRE = 0;
		}

		if(SLIP_CONTROL_NEED_FR==1)
		{
			if((((yaw_error_dot  >= esp_tempW8)&&(yaw_error > (-1500)))||(OVER_UNDER_JUMP==1))
			&&(FLAG_1ST_CYCLE_FR==0)&&(FLAG_1ST_CYCLE_FR_PRE==0))
			{
				if(FLAG_ON_CENTER == 0)
				{
					FLAG_1ST_CYCLE_FR = 1;
					FLAG_1ST_CYCLE_FR_PRE =1;
				}
				else
				{
					FLAG_1ST_CYCLE_FR = 0;
					FLAG_1ST_CYCLE_FR_PRE =1;
				}
			}
			else if((FLAG_1ST_CYCLE_FR==1)&&(SLIP_CONTROL_NEED_FR==1)&&(yaw_error_dot  < (- esp_tempW0)))
			{
				FLAG_1ST_CYCLE_FR = 0;
				FLAG_1ST_CYCLE_FR_PRE =1;
			}
			else if((FLAG_1ST_CYCLE_FR==1)&&(SLIP_CONTROL_NEED_FR==1)&&(yaw_error_dot  >=(- esp_tempW0)))
			{
				FLAG_1ST_CYCLE_FR = 1;
				FLAG_1ST_CYCLE_FR_PRE =1;
			}
			else
			{
				FLAG_1ST_CYCLE_FR = 0;
			}

			if((FLAG_1ST_CYCLE_WHEEL_FR_PRE==1)&&(FLAG_1ST_CYCLE_WHEEL_FR==0))
			{
				FLAG_1ST_CYCLE_FR = 0;
			}
		}
		else
		{
			FLAG_1ST_CYCLE_FR = 0;
			FLAG_1ST_CYCLE_FR_PRE = 0;
		}

		if(SLIP_CONTROL_NEED_RL==1)
		{
			if((esp_tempW7  <= (-esp_tempW9))&&(esp_tempW6 < 0)			
			&&(FLAG_1ST_CYCLE_RL==0)&&(FLAG_1ST_CYCLE_RL_PRE==0))
			{
				FLAG_1ST_CYCLE_RL = 1;
				FLAG_1ST_CYCLE_RL_PRE =1;
				act_1st_cy_rl_cnt ++ ;
			}
		#if __CRAM
			else if((FLAG_1ST_CYCLE_RL==1)&&(SLIP_CONTROL_NEED_RL==1)&&(esp_tempW7  > (-esp_tempW2))&&(act_1st_cy_rl_cnt > 42))		
		#else
			else if((FLAG_1ST_CYCLE_RL==1)&&(SLIP_CONTROL_NEED_RL==1)&&(esp_tempW7  > (-esp_tempW2)))		
		#endif
			{
				FLAG_1ST_CYCLE_RL = 0;
				FLAG_1ST_CYCLE_RL_PRE =1;
			}
		#if __CRAM
			else if((FLAG_1ST_CYCLE_RL==1)&&(SLIP_CONTROL_NEED_RL==1)&&(act_1st_cy_rl_cnt < 59)&&(esp_tempW7  < (-esp_tempW2)))		
		#else
			else if((FLAG_1ST_CYCLE_RL==1)&&(SLIP_CONTROL_NEED_RL==1)&&(esp_tempW7  < (-esp_tempW2)))		
		#endif
			{
				FLAG_1ST_CYCLE_RL = 1;
				FLAG_1ST_CYCLE_RL_PRE =1;
				act_1st_cy_rl_cnt ++ ;
			}
			else
			{
				FLAG_1ST_CYCLE_RL = 0;
			}

			if((FLAG_1ST_CYCLE_WHEEL_RL_PRE==1)&&(FLAG_1ST_CYCLE_WHEEL_RL==0))
			{
				FLAG_1ST_CYCLE_RL = 0;
			}
		}
		else
		{
			FLAG_1ST_CYCLE_RL = 0;
			FLAG_1ST_CYCLE_RL_PRE = 0;
	    act_1st_cy_rl_cnt = 0;
		}

		if(SLIP_CONTROL_NEED_RR==1)
		{
			if((esp_tempW7  <= (-esp_tempW9))&&(esp_tempW6 < 0)			
			&&(FLAG_1ST_CYCLE_RR==0)&&(FLAG_1ST_CYCLE_RR_PRE==0))
			{
				FLAG_1ST_CYCLE_RR = 1;
				FLAG_1ST_CYCLE_RR_PRE =1;
				act_1st_cy_rr_cnt ++ ;
			}
		#if __CRAM
			else if((FLAG_1ST_CYCLE_RR==1)&&(SLIP_CONTROL_NEED_RR==1)&&(esp_tempW7  > (-esp_tempW2))&&(act_1st_cy_rr_cnt > 42))		
		#else
			else if((FLAG_1ST_CYCLE_RR==1)&&(SLIP_CONTROL_NEED_RR==1)&&(esp_tempW7  > (-esp_tempW2)))		
		#endif
			{
				FLAG_1ST_CYCLE_RR = 0;
				FLAG_1ST_CYCLE_RR_PRE =1;
			}
		#if __CRAM
			else if((FLAG_1ST_CYCLE_RR==1)&&(SLIP_CONTROL_NEED_RR==1)&&(act_1st_cy_rr_cnt < 59)&&(esp_tempW7  < (-esp_tempW2)))		
		#else
			else if((FLAG_1ST_CYCLE_RR==1)&&(SLIP_CONTROL_NEED_RR==1)&&(esp_tempW7  < (-esp_tempW2)))		
		#endif
			{
				FLAG_1ST_CYCLE_RR = 1;
				FLAG_1ST_CYCLE_RR_PRE =1;
				act_1st_cy_rr_cnt ++ ;
			}
			else
			{
				FLAG_1ST_CYCLE_RR = 0;
			}

			if((FLAG_1ST_CYCLE_WHEEL_RR_PRE==1)&&(FLAG_1ST_CYCLE_WHEEL_RR==0))
			{
				FLAG_1ST_CYCLE_RR = 0;
			}
		}
		else
		{
			FLAG_1ST_CYCLE_RR = 0;
			FLAG_1ST_CYCLE_RR_PRE = 0;
	    act_1st_cy_rr_cnt = 0;
		}
	}
	else
	{
		FLAG_1ST_CYCLE_FL = 0;
		FLAG_1ST_CYCLE_FL_PRE = 0;

		FLAG_1ST_CYCLE_FR = 0;
		FLAG_1ST_CYCLE_FR_PRE = 0;

		FLAG_1ST_CYCLE_RL = 0;
		FLAG_1ST_CYCLE_RL_PRE = 0;

		FLAG_1ST_CYCLE_RR = 0;
		FLAG_1ST_CYCLE_RR_PRE = 0;

/*        act_1st_cy_fl_cnt = 0; */
/*        act_1st_cy_fr_cnt = 0; */
        act_1st_cy_rl_cnt = 0;
        act_1st_cy_rr_cnt = 0;
	}

}
#endif

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
void LDESP_vDet1stCycleWheelF(struct ESC_STRUCT *WL_ESC)
{

  esp_tempW1 = LCESP_s16FindRoadDependatGain(esp_mu, S16_1ST_CYCLE_WH_EXIT_THR_H_MU_F,
  											S16_1ST_CYCLE_WH_EXIT_THR_M_MU_F, S16_1ST_CYCLE_WH_EXIT_THR_L_MU_F );
	
	if(((WL_ESC->SLIP_CONTROL_NEED)==1)&&(FLAG_ON_CENTER==0))
	{
		if(((WL_ESC->FLAG_1ST_CYCLE_WHEEL)==0)&&((WL_ESC->FLAG_1ST_CYCLE_WHEEL_PRE)==0)&&((WL_ESC->true_slip_target) < ((WL_ESC->slip_m)-WHEEL_SLIP_3)))
		{
			(WL_ESC->FLAG_1ST_CYCLE_WHEEL)=1;
			(WL_ESC->FLAG_1ST_CYCLE_WHEEL_PRE)=1;
		}
		else if(((WL_ESC->FLAG_1ST_CYCLE_WHEEL_PRE)==1)&&((WL_ESC->FLAG_1ST_CYCLE_WHEEL)==1))
		{
			if((WL_ESC->true_slip_target) < ((WL_ESC->slip_m)-esp_tempW1))
			{
				(WL_ESC->FLAG_1ST_CYCLE_WHEEL) = 1;
			}
			else
			{
				(WL_ESC->FLAG_1ST_CYCLE_WHEEL) = 0;
			}
		}
		else
		{
			(WL_ESC->FLAG_1ST_CYCLE_WHEEL)=0;
		}
	}
	else
	{
		WL_ESC->FLAG_1ST_CYCLE_WHEEL=0;
		WL_ESC->FLAG_1ST_CYCLE_WHEEL_PRE=0;
	}
}

void LDESP_vDet1stCycleWheelR(struct ESC_STRUCT *WL_ESC)
{
  esp_tempW2 = LCESP_s16FindRoadDependatGain(esp_mu, S16_1ST_CYCLE_WH_EXIT_THR_H_MU_R,
  											S16_1ST_CYCLE_WH_EXIT_THR_M_MU_R, S16_1ST_CYCLE_WH_EXIT_THR_L_MU_R );

	if(((WL_ESC->SLIP_CONTROL_NEED)==1)&&(FLAG_ON_CENTER==0))
	{
		if(((WL_ESC->FLAG_1ST_CYCLE_WHEEL)==0)&&((WL_ESC->FLAG_1ST_CYCLE_WHEEL_PRE)==0)&&((WL_ESC->true_slip_target) < ((WL_ESC->slip_m)-WHEEL_SLIP_3)))
		{
			WL_ESC->FLAG_1ST_CYCLE_WHEEL=1;
			WL_ESC->FLAG_1ST_CYCLE_WHEEL_PRE=1;
		}
		else if(((WL_ESC->FLAG_1ST_CYCLE_WHEEL_PRE)==1)&&((WL_ESC->FLAG_1ST_CYCLE_WHEEL)==1))
		{
			if((WL_ESC->true_slip_target) < ((WL_ESC->slip_m)-esp_tempW2))
			{
				WL_ESC->FLAG_1ST_CYCLE_WHEEL = 1;
			}
			else
			{
				WL_ESC->FLAG_1ST_CYCLE_WHEEL = 0;
			}
		}
		else
		{
			WL_ESC->FLAG_1ST_CYCLE_WHEEL=0;
		}
	}
	else
	{
		WL_ESC->FLAG_1ST_CYCLE_WHEEL=0;
		WL_ESC->FLAG_1ST_CYCLE_WHEEL_PRE=0;
	}
}
#else
void LDESP_vDet1stCycleWheel(void)
{

  esp_tempW1 = LCESP_s16FindRoadDependatGain(esp_mu, S16_1ST_CYCLE_WH_EXIT_THR_H_MU_F,
  											S16_1ST_CYCLE_WH_EXIT_THR_M_MU_F, S16_1ST_CYCLE_WH_EXIT_THR_L_MU_F );
  esp_tempW2 = LCESP_s16FindRoadDependatGain(esp_mu, S16_1ST_CYCLE_WH_EXIT_THR_H_MU_R,
  											S16_1ST_CYCLE_WH_EXIT_THR_M_MU_R, S16_1ST_CYCLE_WH_EXIT_THR_L_MU_R );
	if((SLIP_CONTROL_NEED_FL==1)&&(FLAG_ON_CENTER==0))
	{
		if((FLAG_1ST_CYCLE_WHEEL_FL==0)&&(FLAG_1ST_CYCLE_WHEEL_FL_PRE==0)&&(true_slip_target_fl < (slip_m_fl-WHEEL_SLIP_3)))
		{
			FLAG_1ST_CYCLE_WHEEL_FL=1;
			FLAG_1ST_CYCLE_WHEEL_FL_PRE=1;
		}
		else if((FLAG_1ST_CYCLE_WHEEL_FL_PRE==1)&&(FLAG_1ST_CYCLE_WHEEL_FL==1))
		{
			if(true_slip_target_fl < (slip_m_fl-esp_tempW1))
			{
				FLAG_1ST_CYCLE_WHEEL_FL = 1;
			}
			else
			{
				FLAG_1ST_CYCLE_WHEEL_FL = 0;
			}
		}
		else
		{
			FLAG_1ST_CYCLE_WHEEL_FL=0;
		}
	}
	else
	{
		FLAG_1ST_CYCLE_WHEEL_FL=0;
		FLAG_1ST_CYCLE_WHEEL_FL_PRE=0;
	}

	if((SLIP_CONTROL_NEED_FR==1)&&(FLAG_ON_CENTER==0))
	{
		if((FLAG_1ST_CYCLE_WHEEL_FR==0)&&(FLAG_1ST_CYCLE_WHEEL_FR_PRE==0)&&(true_slip_target_fr < (slip_m_fr-WHEEL_SLIP_3)))
		{
			FLAG_1ST_CYCLE_WHEEL_FR=1;
			FLAG_1ST_CYCLE_WHEEL_FR_PRE=1;
		}
		else if ((FLAG_1ST_CYCLE_WHEEL_FR_PRE==1)&&(FLAG_1ST_CYCLE_WHEEL_FR==1))
		{
			if(true_slip_target_fr < (slip_m_fr-esp_tempW1))
			{
				FLAG_1ST_CYCLE_WHEEL_FR = 1;
			}
			else
			{
				FLAG_1ST_CYCLE_WHEEL_FR = 0;
			}
		}
		else
		{
			FLAG_1ST_CYCLE_WHEEL_FR=0;
		}
	}
	else
	{
		FLAG_1ST_CYCLE_WHEEL_FR=0;
		FLAG_1ST_CYCLE_WHEEL_FR_PRE=0;
	}

	if((SLIP_CONTROL_NEED_RL==1)&&(FLAG_ON_CENTER==0))
	{
		if((FLAG_1ST_CYCLE_WHEEL_RL==0)&&(FLAG_1ST_CYCLE_WHEEL_RL_PRE==0)&&(true_slip_target_rl < (slip_m_rl-WHEEL_SLIP_3)))
		{
			FLAG_1ST_CYCLE_WHEEL_RL=1;
			FLAG_1ST_CYCLE_WHEEL_RL_PRE=1;
		}
		else if ((FLAG_1ST_CYCLE_WHEEL_RL_PRE==1)&&(FLAG_1ST_CYCLE_WHEEL_RL==1))
		{
			if(true_slip_target_rl < (slip_m_fl-esp_tempW2))
			{
				FLAG_1ST_CYCLE_WHEEL_RL = 1;
			}
			else
			{
				FLAG_1ST_CYCLE_WHEEL_RL = 0;
			}
		}
		else
		{
			FLAG_1ST_CYCLE_WHEEL_RL=0;
		}
	}
	else
	{
		FLAG_1ST_CYCLE_WHEEL_RL=0;
		FLAG_1ST_CYCLE_WHEEL_RL_PRE=0;
	}

	if((SLIP_CONTROL_NEED_RR==1)&&(FLAG_ON_CENTER==0))
	{
		if((FLAG_1ST_CYCLE_WHEEL_RR==0)&&(FLAG_1ST_CYCLE_WHEEL_RR_PRE==0)&&(true_slip_target_rr < (slip_m_rr-WHEEL_SLIP_3)))
		{
			FLAG_1ST_CYCLE_WHEEL_RR=1;
			FLAG_1ST_CYCLE_WHEEL_RR_PRE=1;
		}
		else if((FLAG_1ST_CYCLE_WHEEL_RR_PRE==1)&&(FLAG_1ST_CYCLE_WHEEL_RR==1))
		{
			if(true_slip_target_rr < (slip_m_rr-esp_tempW2))
			{
				FLAG_1ST_CYCLE_WHEEL_RR = 1;
			}
			else
			{
				FLAG_1ST_CYCLE_WHEEL_RR = 0;
			}
		}
		else
		{
			FLAG_1ST_CYCLE_WHEEL_RR=0;
		}
	}
	else
	{
		FLAG_1ST_CYCLE_WHEEL_RR=0;
		FLAG_1ST_CYCLE_WHEEL_RR_PRE=0;
	}
}
#endif


void LDESP_vActPreAction(void)
{
	

/*if ( LOOP_TIME_20MS_TASK_1 == 1 ) 
{*/	
	LCESP_vCalSpeedDepPreFillRear();
/*}*/

	alat_rate[2] = alat_rate[1];
	alat_rate[1] = alat_rate[0];
	alat_rate[0] = McrAbs(alat);

	esp_tempW1 =  McrAbs(alat) - alat_rate[2];


	if (esp_tempW1 < 0)
	{
		pre_act_valve_stop++;
	}
	else
	{
		pre_act_valve_stop = 0;
	}
    esp_tempW0 = delta_det_lat;

    if(esp_tempW0 < 0)
    {
    	esp_tempW0 = 0;
    }
    else
    {
    	;
    }
/************Using Yaw Limit HIGH MU Speed Setting****************/

	esp_tempW2 = pre_filling_enter_cond + S16_PRE_ACTION_HMU_LSP_ADD_AY;
	esp_tempW3 = pre_filling_enter_cond + S16_PRE_ACTION_HMU_MSP_ADD_AY;
	esp_tempW4 = pre_filling_enter_cond + S16_PRE_ACTION_HMU_HSP_ADD_AY;

	esp_tempW6 = pre_filling_enter_cond + S16_PRE_ACTION_MMU_LSP_ADD_AY;
	esp_tempW7 = pre_filling_enter_cond + S16_PRE_ACTION_MMU_MSP_ADD_AY;
	esp_tempW8 = pre_filling_enter_cond + S16_PRE_ACTION_MMU_HSP_ADD_AY;

	limt_enter_wstrd_slow =	S16_YAW_LIMIT_ST_SLOW_ENTER;

/*if ( LOOP_TIME_20MS_TASK_1 == 1 ) 
{*/
    act_pre_esc_hmu_enter = LCESP_s16IInter3Point(vrefk, S16_YAW_LIMIT_H_SPEED_V1, esp_tempW2,
    	                                  S16_YAW_LIMIT_H_SPEED_V2, esp_tempW3,
    	                                  S16_YAW_LIMIT_H_SPEED_V3, esp_tempW4 );

    act_pre_esc_mmu_enter = LCESP_s16IInter3Point(vrefk, S16_YAW_LIMIT_M_SPEED_V1, esp_tempW6,
    	                                  S16_YAW_LIMIT_M_SPEED_V2, esp_tempW7,
    	                                  S16_YAW_LIMIT_M_SPEED_V3, esp_tempW8 );
/*}*/

    esp_tempW10 = LCESP_s16IInter2Point( det_alatm, LAT_0G5G, S16_PRE_FILL_ENTER_DELM_M, LAT_0G7G,S16_PRE_FILL_ENTER_DELM_H);
/*#if Drum_Brake */
    if(REAR_PREFILL ==1)
    {
    	esp_tempW9  = LCESP_s16IInter2Point( det_alatm, LAT_0G5G, S16_PRE_FILL_ENTER_DELM_M_R, LAT_0G7G,S16_PRE_FILL_ENTER_DELM_H_R);
	}
	else
	{
		;
	}

/*    if(act_pre_esc_hmu_enter < 50)
    {
    	act_pre_esc_hmu_enter = 50;
    }
    else
    {
    	;
    }

    if(act_pre_esc_mmu_enter < 50)
    {
    	act_pre_esc_mmu_enter = 50;
    }
    else
    {
    	;
    } */

      esp_tempW5 = LCESP_s16IInter2Point(det_alatm, lsesps16MuMedHigh,act_pre_esc_mmu_enter,
    				lsesps16MuHigh, act_pre_esc_hmu_enter ); 

/*    if(det_alatm > lsesps16MuMedHigh)
    {
    	esp_tempW5 =  act_pre_esc_hmu_enter;
    }
    else
    {
    	esp_tempW5 =  act_pre_esc_mmu_enter;
    } */

/*****************************************************************/

	if(esp_tempW5 < 50)
	{
		esp_tempW5 = 50;
	}
	else if(esp_tempW5 > 1500)
	{
		esp_tempW5 = 1500;
	}
	else
	{
		;
	}
/*********************FL Wheel**********************************/
    if(((esp_on_counter_1 < L_U8_TIME_10MSLOOP_350MS)&&(esp_on_counter_1 > 0))

        ||(VDC_DISABLE_FLG==1)
        ||(ESP_ERROR_FLG==1)||(FLAG_BANK_DETECTED==1)||(BACK_DIR==1)
        ||(SAS_CHECK_OK==0)||(PARTIAL_PERFORM==1)
        ||(cbit_process_step==1)||(KYAW.SUSPECT_FLG==1)||(KSTEER.SUSPECT_FLG==1)
		#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
        ||((fu1ESCDisabledBySW==1)&&(lsespu1AHBGEN3MpresBrkOn==0))||(ABS_PERMIT_BY_BRAKE==1)
  	  	||(lsespu1AHBGEN3MpresBrkOn==1)
		#else
        ||((fu1ESCDisabledBySW==1)&&(MPRESS_BRAKE_ON==0))||(ABS_PERMIT_BY_BRAKE==1)
  	  	||(MPRESS_BRAKE_ON==1)
		#endif
        ||(KYAW.EEPROM_SUSPECT_FLG==1)||(__ESP_DISABLE==1)
        ||(fou1SenPwr1secOk==0)
        ||(ABS_fz == 1)
        ||(BTCS_ON==1)
        ||(ESP_BRAKE_CONTROL_FL==1)
        ||(ESP_BRAKE_CONTROL_FR==1)
        ||(ESP_BRAKE_CONTROL_RL==1)
        ||(ESP_BRAKE_CONTROL_RR==1)
 /*       ||(pre_act_valve_stop >= S16_VALVE_PRE_DEF_ALAT_DEC_TIME) */
#if __ACC
        ||(lcu1AccActiveFlg==1)
#endif
#if __HDC
        ||(lcu1HdcActiveFlg==1)
#endif
#if __DEC
      	||(lcu1DecActiveFlg==1)
#endif
#if __CRAM
      	||(ldu1espDetectCRAM==1)
#endif
#if __TSP
        ||(TSP_ON==1)
#endif
#if __SCC
		||(lcu1SccBrkActiveFlg == 1)
#endif
#if __CDM
		||(lcu1CdmDecCmdAct == 1)
#endif
	)
	{
		FLAG_ACT_PRE_ACTION_FL = 0;
		FLAG_ACT_PRE_ACTION_FR = 0;
		FLAG_ACT_PRE_ACTION_RL = 0;
		FLAG_ACT_PRE_ACTION_RR = 0;
		FLAG_ACT_PRE_ACTION_FL_PRE = 0;
		FLAG_ACT_PRE_ACTION_FR_PRE = 0;
		FLAG_ACT_PRE_ACTION_RL_PRE = 0;
		FLAG_ACT_PRE_ACTION_RR_PRE = 0;
	}
	else
	{
/*#if Drum_Brake */
	if( REAR_PREFILL==1)
	{
/***********************FL Pre fill************************************/
		if((vrefk >= S16_PRE_FILL_ENTER_SPEED_FRONT)
    	&&(FLAG_ON_CENTER==0)&&(FLAG_ACT_PRE_ACTION_FL==0)
		&&(mot_mon_ad < MSC_0_5_V)
		&&(FLAG_ACT_PRE_ACTION_FL_PRE==0)&&(ESP_BRAKE_CONTROL_FR == 0)
		&&(SLIP_CONTROL_NEED_FL ==0)&&(SLIP_CONTROL_NEED_FL_old==0)&&(yaw_sign <= 0)
		&&(ESP_BRAKE_CONTROL_RL ==0)&&(ESP_BRAKE_CONTROL_RR ==0)
		&&(pre_act_valve_stop <= S16_VALVE_PRE_DEF_ALAT_DEC_TIME))
		{
			pre_act_action_fl_count_off = 0;

			if(esp_tempW0 >= esp_tempW5)
			{
				if(delta_moment_q < (-100))
				{
					pre_act_action_fl_count = 0;
				}
				else if(det_wstr_dot < 500)
				{
					pre_act_action_fl_count = 0  ;
				}
			    else if(esp_tempW5 > LAT_1G)
			    {
			        pre_act_action_fl_count = 0;
			    }
				else
				{
					pre_act_action_fl_count = 10;
				}
			}
			else if(delta_yaw > (o_delta_yaw_thres))
			{
			    if(esp_tempW5 > LAT_1G)
			    {
			        pre_act_action_fl_count = 0;
			    }
			    else if(det_alatm >= lsesps16MuHigh)
			    {
				    pre_act_action_fl_count++;
			    }
			    else
			    {
			    	;
			    }
			}
			else if( delta_moment_q > esp_tempW10)
			{
				pre_act_action_fl_count++;
			}
			else
			{
				pre_act_action_fl_count = 0;
			}

			if((SLIP_CONTROL_NEED_FL ==0)&&(pre_act_action_fl_count >= L_U8_TIME_10MSLOOP_70MS)
	#if !__SPLIT_TYPE
			&&(SLIP_CONTROL_NEED_RR == 0)&&(SLIP_CONTROL_NEED_RR_old==0)
	#else
			&&(SLIP_CONTROL_NEED_FR == 0)&&(SLIP_CONTROL_NEED_FR_old==0)
	#endif
			)
			{
				FLAG_ACT_PRE_ACTION_FL = 1;
				FLAG_ACT_PRE_ACTION_FL_PRE = 1;
				pre_act_action_fl_count_on = S16_VALVE_PRE_ACTION_TIME;
			}
			else
			{
				FLAG_ACT_PRE_ACTION_FL = 0;
				FLAG_ACT_PRE_ACTION_FL_PRE = 0;
				pre_act_action_fl_count_on = 0;
			}
		}
		else if((FLAG_ACT_PRE_ACTION_FL_PRE==1)&&(FLAG_ACT_PRE_ACTION_FL==1)&&(SLIP_CONTROL_NEED_FL==0)
			&&(SLIP_CONTROL_NEED_FL_old==0)
			&&(pre_act_action_fl_count_on > 0)
			&&(yaw_sign <= 0)
	#if !__SPLIT_TYPE
			&&(SLIP_CONTROL_NEED_RR == 0)&&(SLIP_CONTROL_NEED_RR_old==0)
	#else
			&&(SLIP_CONTROL_NEED_FR == 0)&&(SLIP_CONTROL_NEED_FR_old==0)
	#endif
			)
		{
			pre_act_action_fl_count_on--;
			pre_act_action_fl_count = 0;
			FLAG_ACT_PRE_ACTION_FL = 1;
			FLAG_ACT_PRE_ACTION_FL_PRE = 1;
		}
		else if(FLAG_ACT_PRE_ACTION_FL_PRE==1)
		{
			pre_act_action_fl_count_off++;

			FLAG_ACT_PRE_ACTION_FL = 0;
			if(pre_act_action_fl_count_off > S16_VALVE_PRE_OFF_TIME)
			{
				FLAG_ACT_PRE_ACTION_FL_PRE = 0 ;
			}
			else
			{
				;
			}
		}
		else
		{
			FLAG_ACT_PRE_ACTION_FL = 0;
			FLAG_ACT_PRE_ACTION_FL_PRE = 0;
			pre_act_action_fl_count = 0;
			pre_act_action_fl_count_on = 0;
			pre_act_action_fl_count_off = 0 ;
		}
/***********************FR Pre fill************************************/
		if((vrefk >= S16_PRE_FILL_ENTER_SPEED_FRONT)
    	&&(FLAG_ON_CENTER==0)&&(FLAG_ACT_PRE_ACTION_FR==0)
		&&(mot_mon_ad < MSC_0_5_V)
		&&(FLAG_ACT_PRE_ACTION_FR_PRE==0)&&(ESP_BRAKE_CONTROL_FL == 0)
		&&(SLIP_CONTROL_NEED_FR ==0)&&(SLIP_CONTROL_NEED_FR_old==0)&&(yaw_sign > 0)
		&&(ESP_BRAKE_CONTROL_RL == 0)&&(ESP_BRAKE_CONTROL_RR == 0)
		&&(pre_act_valve_stop <= S16_VALVE_PRE_DEF_ALAT_DEC_TIME))
		{
		    pre_act_action_fr_count_off = 0;
			if(esp_tempW0 >= esp_tempW5)
			{
				if(delta_moment_q > 100)
				{
					pre_act_action_fr_count = 0;
				}
				else if(det_wstr_dot < 500)
				{
					pre_act_action_fr_count = 0  ;
				}
				else if(esp_tempW5 > LAT_1G)
			    {
			        pre_act_action_fr_count = 0;
			    }
				else
				{
					pre_act_action_fr_count = L_U8_TIME_10MSLOOP_70MS;
				}
			}
			else if(delta_yaw > o_delta_yaw_thres)
			{
				if(esp_tempW5 > LAT_1G)
			    {
			        pre_act_action_fr_count = 0;
			    }
			    else if(det_alatm >= lsesps16MuHigh)
			    {
				    pre_act_action_fr_count++;
			    }
			    else
			    {
			    	;
			    }
			}
			else if( delta_moment_q < (-esp_tempW10))
			{
				pre_act_action_fr_count++;
			}
			else
			{
				pre_act_action_fr_count = 0;
			}

			if((SLIP_CONTROL_NEED_FR ==0)&&(pre_act_action_fr_count >= L_U8_TIME_10MSLOOP_70MS)
	#if !__SPLIT_TYPE
			&&(SLIP_CONTROL_NEED_RL == 0)&&(SLIP_CONTROL_NEED_RL_old==0)
	#else
			&&(SLIP_CONTROL_NEED_FL == 0)&&(SLIP_CONTROL_NEED_FL_old==0)
	#endif
			)
			{
				FLAG_ACT_PRE_ACTION_FR = 1;
				FLAG_ACT_PRE_ACTION_FR_PRE= 1;
				pre_act_action_fr_count_on = S16_VALVE_PRE_ACTION_TIME;
			}
			else
			{
				FLAG_ACT_PRE_ACTION_FR = 0;
				FLAG_ACT_PRE_ACTION_FR_PRE= 0;
				pre_act_action_fr_count_on = 0;
			}
		}
		else if((FLAG_ACT_PRE_ACTION_FR_PRE==1)&&(FLAG_ACT_PRE_ACTION_FR==1)&&(SLIP_CONTROL_NEED_FR==0)
			&&(SLIP_CONTROL_NEED_FR_old==0)
			&&(pre_act_action_fr_count_on > 0)
			&&(yaw_sign > 0)
	#if !__SPLIT_TYPE
			&&(SLIP_CONTROL_NEED_RL == 0)&&(SLIP_CONTROL_NEED_RL_old==0)
	#else
			&&(SLIP_CONTROL_NEED_FL == 0)&&(SLIP_CONTROL_NEED_FL_old==0)
	#endif
			)
		{
			pre_act_action_fr_count_on--;
			pre_act_action_fr_count = 0;
			FLAG_ACT_PRE_ACTION_FR = 1;
			FLAG_ACT_PRE_ACTION_FR_PRE= 1;
		}
		else if(FLAG_ACT_PRE_ACTION_FR_PRE==1)
		{
			pre_act_action_fr_count_off++;

			FLAG_ACT_PRE_ACTION_FR = 0;
			if(pre_act_action_fr_count_off > S16_VALVE_PRE_OFF_TIME)
			{
				FLAG_ACT_PRE_ACTION_FR_PRE = 0;
			}
			else
			{
				;
			}
		}
		else
		{
			FLAG_ACT_PRE_ACTION_FR = 0;
			FLAG_ACT_PRE_ACTION_FR_PRE= 0;
			pre_act_action_fr_count = 0;
			pre_act_action_fr_count_on = 0;
			pre_act_action_fr_count_off = 0;
		}
/***********************RL Pre fill************************************/
		if((vrefk >= S16_PRE_FILL_ENTER_SPEED_REAR)
    	&&(FLAG_ON_CENTER==0)&&(FLAG_ACT_PRE_ACTION_RL==0)
		&&(mot_mon_ad < MSC_0_5_V)
		&&(FLAG_ACT_PRE_ACTION_RL_PRE==0)&&(ESP_BRAKE_CONTROL_FR == 0)
		&&(SLIP_CONTROL_NEED_RL ==0)&&(SLIP_CONTROL_NEED_RL_old==0)&&(yaw_sign > 0)
		&&(ESP_BRAKE_CONTROL_FL == 0)&&(ESP_BRAKE_CONTROL_RR == 0))
		{
		    pre_act_action_rl_count_off = 0;
			if(((YAW_CHANGE_LIMIT_ACT==0)&&(delta_yaw < (u_delta_yaw_thres+pre_fill_rear_del_yaw_con)))||((YAW_CHANGE_LIMIT_ACT==1)&&(delta_yaw < (-pre_fill_rear_del_yaw_con_limit))))
			{
				
				pre_act_action_rl_count = L_U8_TIME_10MSLOOP_70MS;
			   
			}
			else if( delta_moment_q > (esp_tempW9))
			{
				pre_act_action_rl_count++;
			}
			else
			{
				pre_act_action_rl_count = 0;
			}

			if((SLIP_CONTROL_NEED_RL ==0)&&(pre_act_action_rl_count >= L_U8_TIME_10MSLOOP_70MS)
	#if !__SPLIT_TYPE
			&&(SLIP_CONTROL_NEED_FR == 0)&&(SLIP_CONTROL_NEED_FR_old==0)
	#else
			&&(SLIP_CONTROL_NEED_RR == 0)&&(SLIP_CONTROL_NEED_RR_old==0)
	#endif
			)
			{
				FLAG_ACT_PRE_ACTION_RL = 1;
				FLAG_ACT_PRE_ACTION_RL_PRE= 1;
				pre_act_action_rl_count_on = S16_VALVE_PRE_ACTION_TIME;
			}
			else
			{
				FLAG_ACT_PRE_ACTION_RL = 0;
				FLAG_ACT_PRE_ACTION_RL_PRE= 0;
				pre_act_action_rl_count_on = 0;
			}
		}
		else if((FLAG_ACT_PRE_ACTION_RL_PRE==1)&&(FLAG_ACT_PRE_ACTION_RL==1)&&(SLIP_CONTROL_NEED_RL==0)
			&&(SLIP_CONTROL_NEED_RL_old==0)
			&&(pre_act_action_rl_count_on > 0)
			&&(yaw_sign > 0)
	#if !__SPLIT_TYPE
			&&(SLIP_CONTROL_NEED_FR == 0)&&(SLIP_CONTROL_NEED_FR_old==0)
	#else
			&&(SLIP_CONTROL_NEED_RR == 0)&&(SLIP_CONTROL_NEED_RR_old==0)
	#endif
			)
		{
			pre_act_action_rl_count_on--;
			pre_act_action_rl_count = 0;
			FLAG_ACT_PRE_ACTION_RL = 1;
			FLAG_ACT_PRE_ACTION_RL_PRE= 1;
		}
		else if(FLAG_ACT_PRE_ACTION_RL_PRE==1)
		{
			pre_act_action_rl_count_off++;

			FLAG_ACT_PRE_ACTION_RL = 0;
			if(pre_act_action_rl_count_off > S16_VALVE_PRE_OFF_TIME_REAR)
			{
				FLAG_ACT_PRE_ACTION_RL_PRE = 0;
			}
			else
			{
				;
			}
		}
		else
		{
			FLAG_ACT_PRE_ACTION_RL = 0;
			FLAG_ACT_PRE_ACTION_RL_PRE= 0;
			pre_act_action_rl_count = 0;
			pre_act_action_rl_count_on = 0;
			pre_act_action_rl_count_off = 0;
		}

/***********************RR Pre fill************************************/
		if((vrefk >= S16_PRE_FILL_ENTER_SPEED_REAR)
    	&&(FLAG_ON_CENTER==0)&&(FLAG_ACT_PRE_ACTION_RR==0)
		&&(mot_mon_ad < MSC_0_5_V)
		&&(FLAG_ACT_PRE_ACTION_RR_PRE==0)&&(ESP_BRAKE_CONTROL_FL == 0)
		&&(SLIP_CONTROL_NEED_RR ==0)&&(SLIP_CONTROL_NEED_RR_old==0)&&(yaw_sign <= 0)
		&&(ESP_BRAKE_CONTROL_RL == 0)&&(ESP_BRAKE_CONTROL_FR == 0))
		{
			pre_act_action_rr_count_off = 0;

			if(((YAW_CHANGE_LIMIT_ACT==0)&&(delta_yaw < (u_delta_yaw_thres+pre_fill_rear_del_yaw_con)))||((YAW_CHANGE_LIMIT_ACT==1)&&(delta_yaw < (-pre_fill_rear_del_yaw_con_limit))))
			{
				pre_act_action_rr_count = L_U8_TIME_10MSLOOP_70MS;
			}
			else if( delta_moment_q < (-esp_tempW9))
			{
				pre_act_action_rr_count++;
			}
			else
			{
				pre_act_action_rr_count = 0;
			}

			if((SLIP_CONTROL_NEED_RR ==0)&&(pre_act_action_rr_count >= L_U8_TIME_10MSLOOP_70MS)
	#if !__SPLIT_TYPE
			&&(SLIP_CONTROL_NEED_FL == 0)&&(SLIP_CONTROL_NEED_FL_old==0)
	#else
			&&(SLIP_CONTROL_NEED_RL == 0)&&(SLIP_CONTROL_NEED_RL_old==0)
	#endif
			)
			{
				FLAG_ACT_PRE_ACTION_RR = 1;
				FLAG_ACT_PRE_ACTION_RR_PRE = 1;
				pre_act_action_rr_count_on = S16_VALVE_PRE_ACTION_TIME;
			}
			else
			{
				FLAG_ACT_PRE_ACTION_RR = 0;
				FLAG_ACT_PRE_ACTION_RR_PRE = 0;
				pre_act_action_rl_count_on = 0;
			}
		}
		else if((FLAG_ACT_PRE_ACTION_RR_PRE==1)&&(FLAG_ACT_PRE_ACTION_RR==1)&&(SLIP_CONTROL_NEED_RR==0)
			&&(SLIP_CONTROL_NEED_RR_old==0)
			&&(pre_act_action_rr_count_on > 0)
			&&(yaw_sign <= 0)
	#if !__SPLIT_TYPE
			&&(SLIP_CONTROL_NEED_FL == 0)&&(SLIP_CONTROL_NEED_FL_old==0)
	#else
			&&(SLIP_CONTROL_NEED_RL == 0)&&(SLIP_CONTROL_NEED_RL_old==0)
	#endif
			)
		{
			pre_act_action_rr_count_on--;
			pre_act_action_rr_count = 0;
			FLAG_ACT_PRE_ACTION_RR = 1;
			FLAG_ACT_PRE_ACTION_RR_PRE = 1;
		}
		else if(FLAG_ACT_PRE_ACTION_RR_PRE==1)
		{
			pre_act_action_rr_count_off++;

			FLAG_ACT_PRE_ACTION_RR = 0;
			if(pre_act_action_rr_count_off > S16_VALVE_PRE_OFF_TIME_REAR)
			{
				FLAG_ACT_PRE_ACTION_RR_PRE = 0 ;
			}
			else
			{
				;
			}
		}
		else
		{
			FLAG_ACT_PRE_ACTION_RR = 0;
			FLAG_ACT_PRE_ACTION_RR_PRE = 0;
			pre_act_action_rr_count = 0;
			pre_act_action_rr_count_on = 0;
			pre_act_action_rr_count_off = 0 ;
		}
	}
/********Drum Brake - endif **********************/
	else
	{
		if((FLAG_ON_CENTER==0)&&(FLAG_ACT_PRE_ACTION_FL==0)
		&&(FLAG_ACT_PRE_ACTION_FL_PRE==0)&&(ESP_BRAKE_CONTROL_FR == 0)
		&&(SLIP_CONTROL_NEED_FL ==0)&&(SLIP_CONTROL_NEED_FL_old==0)&&(yaw_sign <= 0))
		{
			if(esp_tempW0 >= esp_tempW5)
			{
				if(delta_moment_q < (-100))
				{
					pre_act_action_fl_count = 0;
				}
				else if(det_wstr_dot < 500)
				{
					pre_act_action_fl_count = 0  ;
				}
			    else if(esp_tempW5 > LAT_1G)
			    {
			        pre_act_action_fl_count = 0;
			    }
				else
				{
					pre_act_action_fl_count = L_U8_TIME_10MSLOOP_70MS;
				}
			}
			else if(delta_yaw > o_delta_yaw_thres)
			{
			    if(esp_tempW5 > LAT_1G)
			    {
			        pre_act_action_fl_count = 0;
			    }
			    else if(det_alatm >= lsesps16MuHigh)
			    {
				    pre_act_action_fl_count++;
			    }
			    else
			    {
			    	;
			    }
			}
			else if( delta_moment_q > esp_tempW10)
			{
				pre_act_action_fl_count++;
			}
			else
			{
				pre_act_action_fl_count = 0;
			}

			if((SLIP_CONTROL_NEED_FL ==0)&&(pre_act_action_fl_count >= L_U8_TIME_10MSLOOP_70MS)
	#if !__SPLIT_TYPE
			&&(SLIP_CONTROL_NEED_RR == 0)&&(SLIP_CONTROL_NEED_RR_old==0)
	#else
			&&(SLIP_CONTROL_NEED_FR == 0)&&(SLIP_CONTROL_NEED_FR_old==0)
	#endif
			)
			{
				FLAG_ACT_PRE_ACTION_FL = 1;
				FLAG_ACT_PRE_ACTION_FL_PRE = 1;
				pre_act_action_fl_count_on = S16_VALVE_PRE_ACTION_TIME;
			}
			else
			{
				FLAG_ACT_PRE_ACTION_FL = 0;
				FLAG_ACT_PRE_ACTION_FL_PRE = 0;
				pre_act_action_fl_count_on = 0;
			}
		}
		else if((FLAG_ACT_PRE_ACTION_FL_PRE==1)&&(FLAG_ACT_PRE_ACTION_FL==1)&&(SLIP_CONTROL_NEED_FL==0)
			&&(SLIP_CONTROL_NEED_FL_old==0)
			&&(pre_act_action_fl_count_on > 0)
			&&(yaw_sign <= 0)
	#if !__SPLIT_TYPE
			&&(SLIP_CONTROL_NEED_RR == 0)&&(SLIP_CONTROL_NEED_RR_old==0)
	#else
			&&(SLIP_CONTROL_NEED_FR == 0)&&(SLIP_CONTROL_NEED_FR_old==0)
	#endif
			)
		{
			pre_act_action_fl_count_on--;
			pre_act_action_fl_count = 0;
			FLAG_ACT_PRE_ACTION_FL = 1;
			FLAG_ACT_PRE_ACTION_FL_PRE = 1;
		}
		else if(FLAG_ACT_PRE_ACTION_FL_PRE==1)
		{
			pre_act_action_fl_count_off++;

			FLAG_ACT_PRE_ACTION_FL = 0;
			if(pre_act_action_fl_count_off > S16_VALVE_PRE_OFF_TIME)
			{
				FLAG_ACT_PRE_ACTION_FL_PRE = 0 ;
			}
			else
			{
				;
			}
		}
		else
		{
			FLAG_ACT_PRE_ACTION_FL = 0;
			FLAG_ACT_PRE_ACTION_FL_PRE = 0;
			pre_act_action_fl_count = 0;
			pre_act_action_fl_count_on = 0;
			pre_act_action_fl_count_off = 0 ;
		}
	/*********************FR Wheel**********************************/
		if((FLAG_ON_CENTER==0)&&(FLAG_ACT_PRE_ACTION_FR==0)
		&&(FLAG_ACT_PRE_ACTION_FR_PRE==0)&&(ESP_BRAKE_CONTROL_FL == 0)
		&&(SLIP_CONTROL_NEED_FR ==0)&&(SLIP_CONTROL_NEED_FR_old==0)&&(yaw_sign > 0))
		{
			if(esp_tempW0 >= esp_tempW5)
			{
				if(delta_moment_q > 100)
				{
					pre_act_action_fr_count = 0;
				}
				else if(det_wstr_dot < 500)
				{
					pre_act_action_fr_count = 0  ;
				}
				else if(esp_tempW5 > LAT_1G)
			    {
			        pre_act_action_fr_count = 0;
			    }
				else
				{
					pre_act_action_fr_count = L_U8_TIME_10MSLOOP_70MS;
				}
			}
			else if(delta_yaw > o_delta_yaw_thres)
			{
				if(esp_tempW5 > LAT_1G)
			    {
			        pre_act_action_fr_count = 0;
			    }
			    else if(det_alatm >= lsesps16MuHigh)
			    {
				    pre_act_action_fr_count++;
			    }
			    else
			    {
			    	;
			    }
			}
			else if( delta_moment_q < (-esp_tempW10))
			{
				pre_act_action_fr_count++;
			}
			else
			{
				pre_act_action_fr_count = 0;
			}

			if((SLIP_CONTROL_NEED_FR ==0)&&(pre_act_action_fr_count >= L_U8_TIME_10MSLOOP_70MS)
	#if !__SPLIT_TYPE
			&&(SLIP_CONTROL_NEED_RL == 0)&&(SLIP_CONTROL_NEED_RL_old==0)
	#else
			&&(SLIP_CONTROL_NEED_FL == 0)&&(SLIP_CONTROL_NEED_FL_old==0)
	#endif
			)
			{
				FLAG_ACT_PRE_ACTION_FR = 1;
				FLAG_ACT_PRE_ACTION_FR_PRE= 1;
				pre_act_action_fr_count_on = S16_VALVE_PRE_ACTION_TIME;
			}
			else
			{
				FLAG_ACT_PRE_ACTION_FR = 0;
				FLAG_ACT_PRE_ACTION_FR_PRE= 0;
				pre_act_action_fr_count_on = 0;
			}
		}
		else if((FLAG_ACT_PRE_ACTION_FR_PRE==1)&&(FLAG_ACT_PRE_ACTION_FR==1)&&(SLIP_CONTROL_NEED_FR==0)
			&&(SLIP_CONTROL_NEED_FR_old==0)
			&&(pre_act_action_fr_count_on > 0)
			&&(yaw_sign > 0)
	#if !__SPLIT_TYPE
			&&(SLIP_CONTROL_NEED_RL == 0)&&(SLIP_CONTROL_NEED_RL_old==0)
	#else
			&&(SLIP_CONTROL_NEED_FL == 0)&&(SLIP_CONTROL_NEED_FL_old==0)
	#endif
			)
		{
			pre_act_action_fr_count_on--;
			pre_act_action_fr_count = 0;
			FLAG_ACT_PRE_ACTION_FR = 1;
			FLAG_ACT_PRE_ACTION_FR_PRE= 1;
		}
		else if(FLAG_ACT_PRE_ACTION_FR_PRE==1)
		{
			pre_act_action_fr_count_off++;

			FLAG_ACT_PRE_ACTION_FR = 0;
			if(pre_act_action_fr_count_off > S16_VALVE_PRE_OFF_TIME)
			{
				FLAG_ACT_PRE_ACTION_FR_PRE = 0;
			}
			else
			{
				;
			}
		}
		else
		{
			FLAG_ACT_PRE_ACTION_FR = 0;
			FLAG_ACT_PRE_ACTION_FR_PRE= 0;
			pre_act_action_fr_count = 0;
			pre_act_action_fr_count_on = 0;
			pre_act_action_fr_count_off = 0;
		}
	}
	
	
	}

}

void LCESP_vCalSpeedDepPreFillRear(void)
{

    LCESP_vSetRearspeedDepMUPreFill();

    esp_tempW0 =LCESP_s16FindRoadDependatGain( det_alatm, S16_HMU_R_LSP_PreFILL_DELYAW,
                                S16_MMU_R_LSP_PreFILL_DELYAW, S16_LMU_R_LSP_PreFILL_DELYAW);

    esp_tempW1 =LCESP_s16FindRoadDependatGain( det_alatm, S16_HMU_R_MSP_PreFILL_DELYAW,
                                S16_MMU_R_MSP_PreFILL_DELYAW, S16_LMU_R_MSP_PreFILL_DELYAW);

    esp_tempW2 =LCESP_s16FindRoadDependatGain( det_alatm, S16_HMU_R_HSP_PreFILL_DELYAW,
                                S16_MMU_R_HSP_PreFILL_DELYAW, S16_LMU_R_HSP_PreFILL_DELYAW);

    pre_fill_rear_del_yaw_con = LCESP_s16IInter4Point(vrefk, pre_rear_low_speed , esp_tempW0,
                          pre_rear_med1_speed , esp_tempW1,
                          pre_rear_med2_speed , esp_tempW1,
                          pre_rear_high_speed , esp_tempW2);
                          
    esp_tempW3 =LCESP_s16FindRoadDependatGain( det_alatm, S16_HMU_R_LSP_PreFILL_LIM,
                                S16_MMU_R_LSP_PreFILL_LIM, S16_LMU_R_LSP_PreFILL_LIM);

    esp_tempW4 =LCESP_s16FindRoadDependatGain( det_alatm, S16_HMU_R_MSP_PreFILL_LIM,
                                S16_MMU_R_MSP_PreFILL_LIM, S16_LMU_R_MSP_PreFILL_LIM);

    esp_tempW5 =LCESP_s16FindRoadDependatGain( det_alatm, S16_HMU_R_HSP_PreFILL_LIM,
                                S16_MMU_R_HSP_PreFILL_LIM, S16_LMU_R_HSP_PreFILL_LIM);                      
    
    pre_fill_rear_del_yaw_con_limit = LCESP_s16IInter4Point(vrefk, pre_rear_low_speed , esp_tempW3,
                          pre_rear_med1_speed , esp_tempW4,
                          pre_rear_med2_speed , esp_tempW4,
                          pre_rear_high_speed , esp_tempW5);
                            
}

void LCESP_vSetRearspeedDepMUPreFill(void)
{
    /***********High speed****************/
    pre_rear_high_speed = LCESP_s16FindRoadDependatGain( det_alatm, S16_REAR_HMU_SPD_H,
                 S16_REAR_MMU_SPD_H, S16_REAR_LMU_SPD_H);
    /***********Med speed****************/
    pre_rear_med2_speed = LCESP_s16FindRoadDependatGain( det_alatm, S16_REAR_HMU_SPD_M2,
                 S16_REAR_MMU_SPD_M2, S16_REAR_LMU_SPD_M2);

    pre_rear_med1_speed = LCESP_s16FindRoadDependatGain( det_alatm, S16_REAR_HMU_SPD_M1,
                 S16_REAR_MMU_SPD_M1, S16_REAR_LMU_SPD_M1);
    /***********Low speed****************/
    pre_rear_low_speed = LCESP_s16FindRoadDependatGain( det_alatm, S16_REAR_HMU_SPD_L,
                 S16_REAR_MMU_SPD_L, S16_REAR_LMU_SPD_L);
}
#if defined(__ADDON_TCMF)
void LDESP_vSetActiveBooster(void)
{

/****************Active Booster**************************/
/**************************************************************************************/
    active_bost_enter_mmu = LCESP_s16IInter3Point(vrefk, 200, S16_ENTER_DELM_MMU_LSP_ACT_BOST,
    	                                          300, S16_ENTER_DELM_MMU_MSP_ACT_BOST,
    	                                          800, S16_ENTER_DELM_MMU_HSP_ACT_BOST );

    active_bost_enter_hmu = LCESP_s16IInter3Point(vrefk, 400, S16_ENTER_DELM_HMU_LSP_ACT_BOST,
    	                                          500, S16_ENTER_DELM_HMU_MSP_ACT_BOST,
    	                                          800, S16_ENTER_DELM_HMU_HSP_ACT_BOST );

	esp_tempW0 = LCESP_s16IInter3Point(vrefk, 200, S16_ENTER_DelYD_MMU_LSP_ACT_BOST,
    	                                      300, S16_ENTER_DelYD_MMU_MSP_ACT_BOST,
    	                                      800, S16_ENTER_DelYD_MMU_HSP_ACT_BOST );

    esp_tempW1 = LCESP_s16IInter3Point(vrefk, 400, S16_ENTER_DelYD_HMU_LSP_ACT_BOST,
    	                                  	  500, S16_ENTER_DelYD_HMU_MSP_ACT_BOST,
    	                                      800, S16_ENTER_DelYD_HMU_HSP_ACT_BOST );


    esp_tempW9 = LCESP_s16IInter2Point(det_alatm, lsesps16MuMedHigh,active_bost_enter_mmu,
    				lsesps16MuHigh, active_bost_enter_hmu );

    esp_tempW2 = LCESP_s16IInter2Point(det_alatm, lsesps16MuMedHigh,esp_tempW0,
    				lsesps16MuHigh, esp_tempW1 );
/**************************************************************************************/
	#if(__AHB_GEN3_SYSTEM==ENABLE) || (__IDB_LOGIC == ENABLE)
    if((lsespu1AHBGEN3MpresBrkOn==1)
	#else
    if((MPRESS_BRAKE_ON==1)
	#endif
    	||(ABS_fz ==1)
        ||((ESP_BRAKE_CONTROL_FL==1)&&(BTCS_fl==1))
        ||((ESP_BRAKE_CONTROL_FR==1)&&(BTCS_fr==1))
        ||((ESP_BRAKE_CONTROL_RL==1)&&(BTCS_rl==1))
        ||((ESP_BRAKE_CONTROL_RR==1)&&(BTCS_rr==1))
#if __ACC
        ||(lcu1AccActiveFlg==1)
#endif
#if __HDC
        ||(lcu1HdcActiveFlg==1)
#endif
#if __DEC
      	||(lcu1DecActiveFlg==1)
#endif
#if __CRAM
      	||(ldu1espDetectCRAM==1)
#endif
#if __TSP
        ||(TSP_ON==1)
#endif
	)
	{
		FLAG_ACTIVE_BOOSTER = 0;
	}
	else
	{
		if((SLIP_CONTROL_NEED_FL == 1)&&(FLAG_ACTIVE_BOOSTER == 0))
		{
/*			if(((delta_moment_q >= (esp_tempW9))||(yaw_error_dot >= esp_tempW2)||ROP_REQ_ACT) */
			if(((delta_moment_q >= (esp_tempW9))||(yaw_error_dot >= esp_tempW2))
/*			   &&((FL.s16_Estimated_Active_Press + 200)<= mpress) */
			   &&(slip_control_need_counter_fl <= L_U8_TIME_10MSLOOP_70MS))
			{
				FLAG_ACTIVE_BOOSTER = 1;
			}
			else
			{
				FLAG_ACTIVE_BOOSTER=0;
			}
		}
		else if((FLAG_ACTIVE_BOOSTER == 1)&&(SLIP_CONTROL_NEED_FL == 1))
		{
			FLAG_ACTIVE_BOOSTER = 1;
		}
		else if((SLIP_CONTROL_NEED_FR == 1)&&(FLAG_ACTIVE_BOOSTER == 0))
		{
/*			if(((delta_moment_q <= (-esp_tempW9))||(yaw_error_dot >= esp_tempW2)||ROP_REQ_ACT) */
			if(((delta_moment_q <= (-esp_tempW9))||(yaw_error_dot >= esp_tempW2))
/*			  &&((FR.s16_Estimated_Active_Press + 200)<= mpress) */
			  &&(slip_control_need_counter_fr <= L_U8_TIME_10MSLOOP_70MS))
			{
				FLAG_ACTIVE_BOOSTER = 1;
			}
			else
			{
				FLAG_ACTIVE_BOOSTER = 0;
			}

		}
		else if((FLAG_ACTIVE_BOOSTER == 1)&&(SLIP_CONTROL_NEED_FR == 1))
		{
			FLAG_ACTIVE_BOOSTER = 1;
		}
		else
		{
			FLAG_ACTIVE_BOOSTER = 0;
		}
	}

	if(FLAG_ACTIVE_BOOSTER == 1)
	{
		FLAG_ACTIVE_BOOSTER_PRE = 1;
	}
#if(__AHB_GEN3_SYSTEM==ENABLE) || (__IDB_LOGIC == ENABLE)
	else if((FLAG_ACTIVE_BOOSTER_PRE == 1)&&(lsesps16AHBGEN3mpress >= MPRESS_4BAR))	
#else
	else if((FLAG_ACTIVE_BOOSTER_PRE == 1)&&(mpress >= MPRESS_4BAR))
#endif
	{
		FLAG_ACTIVE_BOOSTER_PRE  = 1;
	}
	else
	{
		FLAG_ACTIVE_BOOSTER_PRE = 0;
	}

}
#endif

void LDESP_vDetectCombControl(void)
{

    esp_tempW0 = LCESP_s16IInter3Point(vrefk, 300, COMB_CNTR_ENTER_HMU_LSP_YAW_ERR_D,
    	                                      600, COMB_CNTR_ENTER_HMU_MSP_YAW_ERR_D,
    	                                      800, COMB_CNTR_ENTER_HMU_HSP_YAW_ERR_D);

    esp_tempW1 = LCESP_s16IInter3Point(vrefk, 300, COMB_CNTR_ENTER_MMU_LSP_YAW_ERR_D,
    	                                      600, COMB_CNTR_ENTER_MMU_MSP_YAW_ERR_D,
    	                                      800, COMB_CNTR_ENTER_MMU_HSP_YAW_ERR_D);

    esp_tempW2 = LCESP_s16IInter3Point(vrefk, 300, COMB_CNTR_ENTER_LMU_LSP_YAW_ERR_D,
    	                                      600, COMB_CNTR_ENTER_LMU_MSP_YAW_ERR_D,
    	                                      800, COMB_CNTR_ENTER_LMU_HSP_YAW_ERR_D);

 	esp_tempW6 = LCESP_s16FindRoadDependatGain( esp_mu,esp_tempW0, esp_tempW1, esp_tempW2 );


    esp_tempW3 = LCESP_s16IInter3Point(vrefk, 300, COMB_CNTR_ENTER_HMU_LSP_DelM,
    	                                      600, COMB_CNTR_ENTER_HMU_MSP_DelM,
    	                                      800, COMB_CNTR_ENTER_HMU_HSP_DelM);

    esp_tempW4 = LCESP_s16IInter3Point(vrefk, 300, COMB_CNTR_ENTER_MMU_LSP_DelM,
    	                                      600, COMB_CNTR_ENTER_MMU_MSP_DelM,
    	                                      800, COMB_CNTR_ENTER_MMU_HSP_DelM);

    esp_tempW5 = LCESP_s16IInter3Point(vrefk, 300, COMB_CNTR_ENTER_LMU_LSP_DelM,
    	                                      600, COMB_CNTR_ENTER_LMU_MSP_DelM,
    	                                      800, COMB_CNTR_ENTER_LMU_HSP_DelM);

 	esp_tempW7 = LCESP_s16FindRoadDependatGain( esp_mu,esp_tempW3, esp_tempW4, esp_tempW5 );

#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
	if((lsespu1AHBGEN3MpresBrkOn==1)&&(PARTIAL_BRAKE == 1)&&(FLAG_ACT_COMB_CNTR_FL==0)
#else
	if((MPRESS_BRAKE_ON==1)&&(PARTIAL_BRAKE == 1)&&(FLAG_ACT_COMB_CNTR_FL==0)
#endif
		&&(yaw_error_dot >= esp_tempW6)
		&&( McrAbs(delta_moment_q) >= esp_tempW7))
	{
		if(ESP_BRAKE_CONTROL_FL==1)
		{
			FLAG_ACT_COMB_CNTR_FL = 1;
		}
		else
		{
			FLAG_ACT_COMB_CNTR_FL = 0;
		}

	}
	else if(FLAG_ACT_COMB_CNTR_FL==1)
	{
		if((ESP_BRAKE_CONTROL_FL==1)&&( McrAbs(delta_moment_q) >= 1500))

		{
			FLAG_ACT_COMB_CNTR_FL =1 ;
		}
		else
		{
			FLAG_ACT_COMB_CNTR_FL =0 ;
		}
	}
	else
	{
		FLAG_ACT_COMB_CNTR_FL = 0;
	}

#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
	if((lsespu1AHBGEN3MpresBrkOn==1)&&(PARTIAL_BRAKE == 1)&&(FLAG_ACT_COMB_CNTR_FR==0)
#else
	if((MPRESS_BRAKE_ON==1)&&(PARTIAL_BRAKE == 1)&&(FLAG_ACT_COMB_CNTR_FR==0)
#endif
		&&(yaw_error_dot >= esp_tempW6)
		&&( McrAbs(delta_moment_q) >= esp_tempW7))
	{
		if(ESP_BRAKE_CONTROL_FR==1)
		{
			FLAG_ACT_COMB_CNTR_FR = 1;
		}
		else
		{
			FLAG_ACT_COMB_CNTR_FR = 0;
		}

	}
	else if(FLAG_ACT_COMB_CNTR_FR==1)
	{
		if((ESP_BRAKE_CONTROL_FR==1)&&( McrAbs(delta_moment_q) >= 1500))
		{
			FLAG_ACT_COMB_CNTR_FR =1 ;
		}
		else
		{
			FLAG_ACT_COMB_CNTR_FR =0 ;
		}
	}
	else
	{
		FLAG_ACT_COMB_CNTR_FR = 0;
	}

#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
	if((lsespu1AHBGEN3MpresBrkOn==1)&&(PARTIAL_BRAKE == 1)&&(FLAG_ACT_COMB_CNTR_RL==0)
#else
	if((MPRESS_BRAKE_ON==1)&&(PARTIAL_BRAKE == 1)&&(FLAG_ACT_COMB_CNTR_RL==0)
#endif
		&&(yaw_error_dot >= esp_tempW6)
		&&( McrAbs(delta_moment_q) >= esp_tempW7))
	{
		if(ESP_BRAKE_CONTROL_RL==1)
		{
			FLAG_ACT_COMB_CNTR_RL = 1;
		}
		else
		{
			FLAG_ACT_COMB_CNTR_RL = 0;
		}

	}
	else if(FLAG_ACT_COMB_CNTR_RL==1)
	{
		if((ESP_BRAKE_CONTROL_RL==1)&&( McrAbs(delta_moment_q) >= 1500))
		{
			FLAG_ACT_COMB_CNTR_RL =1 ;
		}
		else
		{
			FLAG_ACT_COMB_CNTR_RL =0 ;
		}
	}
	else
	{
		FLAG_ACT_COMB_CNTR_RL = 0;
	}

#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
	if((lsespu1AHBGEN3MpresBrkOn==1)&&(PARTIAL_BRAKE == 1)&&(FLAG_ACT_COMB_CNTR_RR==0)
#else
	if((MPRESS_BRAKE_ON==1)&&(PARTIAL_BRAKE == 1)&&(FLAG_ACT_COMB_CNTR_RR==0)
#endif
		&&(yaw_error_dot >= esp_tempW6)
		&&( McrAbs(delta_moment_q) >= esp_tempW7))
	{
		if(ESP_BRAKE_CONTROL_RR==1)
		{
			FLAG_ACT_COMB_CNTR_RR = 1;
		}
		else
		{
			FLAG_ACT_COMB_CNTR_RR = 0;
		}

	}
	else if(FLAG_ACT_COMB_CNTR_RR==1)
	{
		if((ESP_BRAKE_CONTROL_RR==1)&&( McrAbs(delta_moment_q) >= 1500))
		{
			FLAG_ACT_COMB_CNTR_RR =1 ;
		}
		else
		{
			FLAG_ACT_COMB_CNTR_RR =0 ;
		}
	}
	else
	{
		FLAG_ACT_COMB_CNTR_RR = 0;
	}

	if((BTCS_ON == 1)
#if __ACC
        ||(lcu1AccActiveFlg==1)
#endif
#if __HDC
        ||(lcu1HdcActiveFlg==1)
#endif
#if __DEC
      	||(lcu1DecActiveFlg==1)
#endif
#if __CRAM
      	||(ldu1espDetectCRAM==1)
#endif
#if __TSP
        ||(TSP_ON==1)
#endif
		)
	{
		if(SLIP_CONTROL_NEED_FL == 1)
		{
			FLAG_ACT_COMB_CNTR_FL = 1;
		}
		else
		{
			;
		}

		if(SLIP_CONTROL_NEED_FR == 1)
		{
			FLAG_ACT_COMB_CNTR_FR = 1;
		}
		else
		{
			;
		}

		if(SLIP_CONTROL_NEED_RL == 1)
		{
			if(BTCS_fr==1)
			{
				FLAG_ACT_COMB_CNTR_RL = 0;
			}
			else
			{
				FLAG_ACT_COMB_CNTR_RL = 1;
			}
		}
		else
		{
			;
		}

		if(SLIP_CONTROL_NEED_RR == 1)
		{
			if(BTCS_fl==1)
			{
				FLAG_ACT_COMB_CNTR_RR = 0;
			}
			else
			{
				FLAG_ACT_COMB_CNTR_RR = 1;
			}
		}
		else
		{
			;
		}

	}
}

/********************* Decel Brake Lamp (A430)**********************/
#if (GMLAN_ENABLE==ENABLE)
void LDESP_vSetDecelBrakeLamp(void)
{
	if((U8_ESC_Active_Lamp_Enable == 1)&&(YAW_CDC_WORK == 1))
	{
		if((lsabss16DecelRefiltByVref5 <= (-S16_ESC_Lamp_Act_Decel_High))&&(DECLAMP_ON == 0))	
		{
			if(lsespu8DecelConfirmCounter >= 10)
			{
				DECLAMP_ON = 1;
				lsespu8DecelConfirmCounter = 0;
			}
			else
			{
				lsespu8DecelConfirmCounter++;
			}	
		}
		else if((DECLAMP_ON == 1)&&(lsabss16DecelRefiltByVref5 < (-S16_ESC_Lamp_Act_Decel_Low)))
		{
			DECLAMP_ON = 1;
			lsespu8DecelConfirmCounter = 0;
		}
		else if((DECLAMP_ON == 1)&&(lsabss16DecelRefiltByVref5 >= (-S16_ESC_Lamp_Act_Decel_Low)))
		{	
			if(lsespu8DecelConfirmCounter >= 10)
			{
				DECLAMP_ON = 0;
				lsespu8DecelConfirmCounter = 0;
			}
			else
			{
				lsespu8DecelConfirmCounter++;
			}			
		}
		else
		{
			DECLAMP_ON = 0;
			lsespu8DecelConfirmCounter = 0;			
		}
	}
	else
	{
		DECLAMP_ON = 0;
		lsespu8DecelConfirmCounter = 0;
	}		
}
#endif
/***********************************************************************/
#endif
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDESPDetectVehicleStatus
	#include "Mdyn_autosar.h"
#endif
