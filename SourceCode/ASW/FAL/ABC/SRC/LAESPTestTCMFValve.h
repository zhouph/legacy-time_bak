#ifndef _LAESPTESTTCMFVALVE_
#define _LAESPTESTTCMFVALVE_

#if (__VALVE_TEST_TCMF ==1)
#define S16_TCMF_SCAN_INI_FULL      	S16_ESC_RESERVED_01
#define S16_TCMF_SCAN_RISE_MAX      	S16_ESC_RESERVED_02
#define S16_TCMF_SCAN_HOLD          	S16_ESC_RESERVED_03
#define S16_TCMF_SCAN_DUMP_FAST     	S16_ESC_RESERVED_04

#define S16_TCMF_SCAN_RISE_STEP_R3  	S16_ESC_RESERVED_05
#define S16_TCMF_SCAN_HOLD_R3			S16_ESC_RESERVED_06
#define S16_TCMF_SCAN_DUMP_SLOW_R3		S16_ESC_RESERVED_07

#define S16_TCMF_SCAN_RISE_STEP_R2  	S16_ESC_RESERVED_05
#define S16_TCMF_SCAN_HOLD_R2			S16_ESC_RESERVED_06
#define S16_TCMF_SCAN_DUMP_SLOW_R2		S16_ESC_RESERVED_07

#define S16_TCMF_SCAN_RISE_STEP_R1  	S16_ESC_RESERVED_05
#define S16_TCMF_SCAN_HOLD_R1			S16_ESC_RESERVED_06
#define S16_TCMF_SCAN_DUMP_SLOW_R1		S16_ESC_RESERVED_07

#define S16_TCMF_SCAN_FADE          	10
#define S16_TCMF_SCAN_RESET         	200

#define U8_TCMF_T_VOLT_INI_FULL_VALVE   (S16_ESC_RESERVED_08*1000)
#define U8_TCMF_T_VOLT_RISE_VALVE       (S16_ESC_RESERVED_08*1000)
#define U8_TCMF_T_VOLT_HOLD_VALVE       (S16_ESC_RESERVED_08*1000)
#define U8_TCMF_T_VOLT_DUMP_VALVE       (S16_ESC_RESERVED_08*1000)

extern void LAESP_vTCMFValveTest(void);
extern int16_t LCMSC_vTestTcmfValveTarVol(void);
#endif

/*Includes *********************************************************************/
#include "LVarHead.h"
/*Global MACRO CONSTANT Definition *********************************************/

/*Global MACRO FUNCTION Definition *********************************************/

/*Global Type Declaration ******************************************************/

/*Global Extern Variable Declaration *******************************************/

/*Global Extern Functions Declaration ******************************************/

#endif
