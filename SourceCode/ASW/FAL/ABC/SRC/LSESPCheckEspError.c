 
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LSESPCheckEspError
	#include "Mdyn_autosar.h"
#endif

#include "LDRTACallDetection.h"
#include "LVarHead.h"
 
#if	__VDC
void	LSESP_vCheckEspError(void);
#endif
#if	__VDC
void	LSESP_vCheckEspError(void)
{
/*    ESP_ERROR_FLG=vdc_error_flg; */
  
        #if __GM_FailM         

        if( (fu1ESCEcuHWErrDet==1)
            ||(fu1FrontWSSErrDet==1)
            ||(fu1RearWSSErrDet==1)
            ||(fu1ESCSensorErrDet==1)
            ||(fu1MainCanLineErrDet==1)
            ||(fu1EMSTimeOutErrDet==1)
            ||(fu1OnDiag == 1)
            ||(init_end_flg == 0)
            ||(wu8IgnStat==CE_OFF_STATE) ) 
        {
            /*if(PARTIAL_PERFORM) ESP_ERROR_FLG=0;
            else ESP_ERROR_FLG=1; */
                
            ESP_ERROR_FLG=1;
                
        }
        
        #if __BRAKE_FLUID_LEVEL        
        
        else if ( (fu1DelayedBrakeFluidLevel==1) || (fu1LatchedBrakeFluidLevel==1) )	/* 090901 for Bench Test */			
        {
              #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
            if ( (Low_Brk_Fluid_Enter==0)&&(ABS_fz==1)&&((vrefk/10)>K_ESC_Temporary_Enable_Speed)&&(lsesps16AHBGEN3mpress>K_ESC_Temporary_Enable_Pressure) )
              #else
            if ( (Low_Brk_Fluid_Enter==0)&&(ABS_fz==1)&&((vrefk/10)>K_ESC_Temporary_Enable_Speed)&&(mpress>K_ESC_Temporary_Enable_Pressure) )
              #endif
            {
                Low_Brk_Fluid_Enter = 1 ;
                ESP_ERROR_FLG = 0 ;
            }
            else if ( (Low_Brk_Fluid_Enter==1)&&(ABS_fz==1) )
            {
                Low_Brk_Fluid_Enter = 1 ;
                ESP_ERROR_FLG = 0 ;
            }
            else
            {
                Low_Brk_Fluid_Enter = 0 ;
                ESP_ERROR_FLG = 1 ;
            }                
                    
        }    
        #endif        
        
        else
        { 
            if(fu1ESCDisabledBySW==0)
            {
                ESP_ERROR_FLG=0;
            }
            else /* Switch_Enable */
            {              
                if((RTA_FL.ldabsu1DetSpareTire==1)||(RTA_FR.ldabsu1DetSpareTire==1)               /* '08.03.28 : for Bench_Test, 10SWD : minitire flag change, GM failM */
                  ||(RTA_RL.ldabsu1DetSpareTire==1)||(RTA_RR.ldabsu1DetSpareTire==1)
                  ||(Flg_ESC_SW_ABS_OK==1)||(Flg_ESC_SW_ROP_OK==1)||(Flg_Flat_Tire_Detect_OK==1)
			  #if __TCS_DRIVELINE_PROTECT
                  ||(lctcsu1DrivelineProtectionOn==1)
			  #endif	/* #if __TCS_DRIVELINE_PROTECT */                  	
                  )
                {            
                        ESP_ERROR_FLG=0;
                }
                else
                {
                    ESP_ERROR_FLG=1;   /* ESC_OFF */
                }               
            }  
             
        }
        
        #else       
          
            #if (__CAR_MAKER==SSANGYONG)  
              #if (__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
                if( (fu1ESCDisabledBySW==0)
                	||((fu1ESCDisabledBySW==1)&&(Flg_ESC_SW_ROP_OK==1))  /* '12, ROP_in_ESC-OFF  */  
                	||((fu1ESCDisabledBySW==1)&&(lsespu1AHBGEN3MpresBrkOn==1)&&(ABS_fz==1)))  /* 09-SWD, C-200 Req. */ 
              #else
                if( (fu1ESCDisabledBySW==0)
                	||((fu1ESCDisabledBySW==1)&&(Flg_ESC_SW_ROP_OK==1))  /* '12, ROP_in_ESC-OFF  */  
                	||((fu1ESCDisabledBySW==1)&&(MPRESS_BRAKE_ON==1)&&(ABS_fz==1)))  /* 09-SWD, C-200 Req. */   
              #endif  	
            #else 
                if( (fu1ESCDisabledBySW==0) 
                	||((fu1ESCDisabledBySW==1)&&(Flg_ESC_SW_ROP_OK==1)) )   /* '12, ROP_in_ESC-OFF  */  
                	
            #endif
            {
                if( ( vdc_error_flg == 1 )
                    ||(fu1OnDiag == 1)
                    ||(init_end_flg == 0)
                    ||(wu8IgnStat==CE_OFF_STATE)
					          ||(fu1ESCEcuHWErrDet==1)
                    ||(fu1FrontWSSErrDet==1)
                    ||(fu1RearWSSErrDet==1)
                    #if __BRAKE_FLUID_LEVEL
                    ||(fu1DelayedBrakeFluidLevel==1)
                    #endif
                    ||(fu1ESCSensorErrDet==1)
                    ||(fu1MainCanLineErrDet==1)
                    ||(fu1ESCSwitchFail==1)
                    ||(fu1BLSErrDet==1)
                    #if __AX_SENSOR
                    ||(fu1AxErrorDet==1) 
                    #endif                   
                    ||(fu1EMSTimeOutErrDet==1) ) 
                {
                    /*if(PARTIAL_PERFORM) ESP_ERROR_FLG=0;
                    else ESP_ERROR_FLG=1; */
                    ESP_ERROR_FLG=1;
                }
                else
                {
                     ESP_ERROR_FLG=0;
                }
            }
            else 
                       {
            		#if (__RWD_SPORTSMODE==SPORTMODE_2) /*__SPORTSMODE==2*/
            	if(vdc_error_flg==1)
            	{	
                ESP_ERROR_FLG=1;
              }
              else
              {	
              	ESP_ERROR_FLG=0;
              }
              	#else
                ESP_ERROR_FLG=1;
              	#endif 	
            }      
        
        #endif   

}
  #endif

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LSESPCheckEspError
	#include "Mdyn_autosar.h"
#endif
