/******************************************************************************
* Project Name: Hill Start Assist control
* File: LCHSACallControl.C
* Description: Hill Start Assist Controller
* Date: June. 4. 2005
******************************************************************************/

/* Includes ******************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCHSACallControl
	#include "Mdyn_autosar.h"
#endif


#include "LCHSACallControl.h"
#include "LAHSACallActHW.h"
#include "LDABSDctWheelStatus.h"
#include "LSABSCallSensorSignal.h"
#include "LSABSEstLongAccSensorOffsetMon.h"
#include "LCESPCalLpf.h"
#include "LCESPCalInterpolation.h"
#include "LCHDCCallControl.h"
#include "Hardware_Control.h"
#include "LCAVHCallControl.h"
#include "LSESPFilterEspSensor.h"
#include "LCEPBCallControl.h"
#include "LSESPCalSensorOffset.h"
#include "hm_logic_var.h"
#include "LSESPFilterEspSensor.h"
  #if defined(__dDCT_INTERFACE)
#include "LCHRCCallControl.h"
  #endif







#if __HSA
/* Logcal Definiton  *********************************************************/


#define __MONITOR_TORQ_CLUTCH_ENABLE       DISABLE
#define FRIC_CONST_CLUTCH                  2

#define HSA_CLUTCH_RGEAR_ERROR_CHK_TIME  L_U8_TIME_10MSLOOP_100MS 

#define TIME_100MS				L_U8_TIME_10MSLOOP_100MS 
#define TIME_200MS				L_U8_TIME_10MSLOOP_200MS 
#define TIME_250MS				L_U8_TIME_10MSLOOP_250MS 
#define TIME_400MS				L_U8_TIME_10MSLOOP_400MS 
#define TIME_500MS				L_U8_TIME_10MSLOOP_500MS	
#define TIME_1000MS				L_U8_TIME_10MSLOOP_1000MS
#define TIME_1500MS				L_U8_TIME_10MSLOOP_1500MS
#define TIME_2000MS				L_U8_TIME_10MSLOOP_2S

#define	AT_DRIVE 				5
#define	AT_NEUTRAL 				6
#define	AT_REVERSE				7

#define	MT_NEUTRAL_POS				0
#define	MT_GEAR1_POS 				1
#define	MT_GEAR2_POS 				2
#define	MT_GEAR3_POS 				3
#define	MT_REVERSE_POS 				7

#define	MT_DRIVE				1
#define	MT_NEUTRAL				0
#define	MT_REVERSE			    7

#define	DISENGAGE				0
#define	ENGAGE					1

#define	HSA_MANUAL_TM			0
#define	HSA_AUTO_TM				1

#define ENG_RPM_DOT_DIFF				8
#define	REAL_ENG_RPM_DOT_UPPER_LIMIT 	-2
#define EST_ENG_RPM_DOT_UPPER_LIMIT		3

#define HSA_AX_STATIONARY_TIME	L_U8_TIME_10MSLOOP_200MS

#if (__CAR_MAKER==GM_KOREA)
#define __INHIBIT_HSA_FOR_IGN_OFFON		1
#else
#define __INHIBIT_HSA_FOR_IGN_OFFON		0
#endif
extern int16_t wu1IgnRisingEdge;

/* Variables Definition*******************************************************/

U8_BIT_STRUCT_t HSAF0;
U8_BIT_STRUCT_t HSAF1;
U8_BIT_STRUCT_t HSAF2;
U8_BIT_STRUCT_t HSAF3;
U8_BIT_STRUCT_t HSAF4;
U8_BIT_STRUCT_t HSAF5;
U8_BIT_STRUCT_t HSAF6;

int8_t	    HSA_Control;
int8_t	    HSA_clutch_engage_cnt, HSA_flag;

uint8_t	    HSA_rebraking_counter, HSA_rebraking_start;
uint8_t	    HSA_mtp_old1, HSA_mtp_old2, HSA_mtp_old3, HSA_mtp_cnt, HSA_complete_stop_counter;
uint8_t	    HSA_ax_stationary_cnt, HSA_hold_cnt_rpm;
uint8_t     lcu8HsaReBrkCnt, lcu8HsaExitBySpinCnt;
uint8_t     lcHsau8ClutchErrorCnt, lcHsau8RgearErrorCnt, lcHsau8OverloadDctCnt;
uint8_t		lcu8HsaAxAvgValidTh, lcu8HsaAhbMpressOnCnt;

int16_t		HSA_pressure_release_counter, HSA_phase_out_time, HSA_phase_out_rate;
int16_t		HSA_EXIT_eng_torq, HSA_ENTER_press, HSA_retaining_press, HSA_gs_sel, S16_HSA_ETPerG_SteepSlope;
int16_t		HSA_apply_count, MT_ENG_TORQ_INDEX, lcs16HsaExitBySpinPress;;
int16_t		HSA_Est_eng_rpm_dot, HSA_Est_eng_rpm_dot_old;
int16_t		HSA_eng_rpm_LPF7_old, HSA_eng_rpm_dot_LPF7, HSA_eng_rpm_dot_LPF7_old, eng_torq_rel_filt_HSA;
int16_t		HSA_ax_sen_filt_old, HSA_ax_sum, HSA_ax_cnt, HSA_ax_avg, HSA_EXIT_ax, MT_FRICTION_CONST, HSA_ax_sen_filt_abs; 
int16_t	    lcs16HsaInitHoldCntrCnt, lcs16HsaMpressSlopThres, lcs16HsaValveActMp, lcs16HsaReBrkReleaseCnt;
int16_t     lchsas16MpressSlopThres, Torq_Clutch, lchsas16CtrlTargetPress; 
int16_t     lchsas16EntLimitMp, lchsas16EntInhibitMp, HSA_hold_time_max, lcs16HsaMpress;
int16_t     lcs16HsaAxSum, lcs16HsaAxAvgTime, lcs16HsaAxAvg, lcs16HsaAxDiffMax;

uint16_t	HSA_hold_counter, HSA_accel_cnt, HSA_accel_cnt2;

static int16_t HSA_pressure_release_time;

/*Global Variable Declaration*************************************************/

/* LocalFunction prototype ***************************************************/

static void	HSA_INHIBITION_CHECK(void);
static void	HSA_CLUTCH_RGEAR_FAIL_CHECK(void);
static void	STOP_ON_HILL(void);
static void	HSA_EXIT_CONDITION(void);
static void	HSA_ENTER_CONDITION(void);
static void	HSA_APPLY_CONDITION(void);
static void	COMPLETE_STOP(void);
static void	HSA_REBRAKING_DETECT(void);
static void	HSA_PRESSURE_RELEASE(void);
static void	HSA_HARDWARE_SETTING(void);
static void	HSA_TM_SETTING(void);  
static void	HSA_CLUTCH_POSITION_EST(void);  
static void	HSA_APPLYING(void);
static void	HSA_HOLDING(void);
void HSA_reset(void);
static void	HSA_PRESSURE_RELEASE_RATE_SET(void);
static void	HSA_CHK_MT_EXIT_CONDITION(void);
static void	HSA_FADE_OUT_ROLLBACK(void);
  #if __EPB_INTERFACE
static void	EPB2HSA_INTERFACE(void);
static void	HSA2EPB_INTERFACE(void);
  #endif
  #if __ISG
static void	CHECK_ISG_REQUEST(void);
  #endif
static void	LCHSA_vCheckMpressOn(void);
static void	HSA_CALC_ENTER_PRESS(void);
static void	HSA_CALC_CONTROL_PRESS(void);
static void HSA_CALC_MCP_SLOPE(void);
static void HSA_VARIABLE_INITIALIZE(void);
static void HSA_MAX_HOLD_TIME(void);
static void HSA_TCS_INTERFACE(void);

/* GlobalFunction prototype **************************************************/
/*extern 	int16_t abs(int16_t j);*/ 


/* Implementation*************************************************************/




/******************************************************************************
* FUNCTION NAME:      LCHSA_vCallControl
* CALLED BY:          StartupLogic()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        HSA controller main function
******************************************************************************/
void 	LCHSA_vCallControl(void)
{
  #if (__MGH60_CYCLE_TIME_OPT_HSA == ENABLE)
	if(vref<=S16_HSA_CONTROLLER_ENABLE_SPEED)
  #endif	
	{	
		  HSA_VARIABLE_INITIALIZE();  
		  
      #if __EPB_INTERFACE
	    EPB2HSA_INTERFACE();
      #endif

	    HSA_TM_SETTING();  
	    HSA_HARDWARE_SETTING();
        
	    HSA_INHIBITION_CHECK();
	    LCHSA_vCheckMpressOn();
	    STOP_ON_HILL();
	  #if __ISG
	    CHECK_ISG_REQUEST();
	  #endif

		HSA_CALC_ENTER_PRESS();
		HSA_CALC_MCP_SLOPE();
		HSA_MAX_HOLD_TIME();
    	HSA_CHK_MT_EXIT_CONDITION();	
    	HSA_FADE_OUT_ROLLBACK();
	    
	    switch (HSA_Control)
	    {
	    	/*-------------------------------------------------------------------------- */
	    	case HSA_READY:						/* Braking on hill */
	    		if((HSA_INHIBITION_flag==1) || ((STOP_ON_HILL_flag==0)
	    		  #if __ISG
	    		&&(ISG_READY==0)
	    		  #endif
	    		)) 
                {
	    			HSA_Control = HSA_NORMAL;
	    		}
	    		else 
	    		{
	    			if(STOP_ON_HILL_flag==1)
	    			{
	    			    HSA_ENTER_CONDITION();
	    			    if(HSA_ENTER_CONDITION_SATISFY==1) {
	    			    	HSA_Control = HSA_HOLD;
	    			    	HSA_flg = 1;
	    			    	HSA_ISG_flg = 0;
	    			    	/*HSA_retaining_press = mpress;*/
	    			    	HSA_ENTER_CONDITION_SATISFY = 0;
	    			    	HSA_HOLDING();
	    			    }
	    			    else { }
	    			}
	    			  #if __ISG
	    			else if(ISG_READY==1)
	    			{
	    			    HSA_ENTER_CONDITION();
	    			    if(HSA_ENTER_CONDITION_SATISFY==1) {
	    			    	HSA_Control = HSA_HOLD;
	    			    	HSA_flg = 1;
	    			    	HSA_ISG_flg = 1;
	    			    	/*HSA_retaining_press = mpress;*/
	    			    	HSA_ENTER_CONDITION_SATISFY = 0;
	    			    	HSA_HOLDING();
	    			    }
	    			    else { }					
	    			}
	    			  #endif
	    			else { }
	    		}
	    		break;
	    	/*-------------------------------------------------------------------------- */
	    	case HSA_HOLD:						/* Holding state */
	    		HSA_EXIT_CONDITION();
	    		if(HSA_EXIT_CONDITION_SATISFY==1) {
	    			HSA_Control = HSA_RELEASE;
	    			HSA_EXIT_CONDITION_SATISFY = 0;
	    			HSA_PRESSURE_RELEASE();
	    		}			
	    		else {
	    			HSA_APPLY_CONDITION();
	    			if(HSA_APPLY_CONDITION_SATISFY==1) {
	    				HSA_Control = HSA_APPLY;
	    				HSA_APPLY_CONDITION_SATISFY = 0;
	    				HSA_APPLYING();
	    			}
	    			else {
	    				HSA_HOLDING();
	    			}
	    		}
	    		break;
	    	/*-------------------------------------------------------------------------- */
	    	case HSA_APPLY:						/* Applying state */
	    		HSA_EXIT_CONDITION();
	    		if(HSA_EXIT_CONDITION_SATISFY==1) {
	    			HSA_Control = HSA_RELEASE;
	    			HSA_EXIT_CONDITION_SATISFY = 0;
	    			HSA_apply_count = 0;
	    			HSA_MSC_MOTOR_ON = 0;
	    			HSA_S_VALVE_LEFT=0;
        			HSA_S_VALVE_RISHT=0;
	    			HSA_PRESSURE_RELEASE();
	    		}			
	    		else {
	    			if(lcu1HsaRiseComplete==1) {
	    				HSA_Control = HSA_HOLD;
	    				COMPLETE_STOP_flag = 0;
	    				HSA_apply_count = 0;
	    				HSA_HOLDING();
	    			}
	    			else {
	    				HSA_APPLYING();
	    			}
	    		}
	    		break;						
	    	/*-------------------------------------------------------------------------- */
	    	case HSA_RELEASE:			
	    		HSA_EXIT_CONDITION();				
	    		if(HSA_EXIT_CONDITION_SATISFY==1) {
	    			HSA_EXIT_CONDITION_SATISFY = 0;
	    		}
	    		else { }
        
              #if defined(__dDCT_INTERFACE)
                if(HSA_phase_out_rate > 0) {tempW6 = (HSA_retaining_press/HSA_phase_out_rate) + L_U16_TIME_10MSLOOP_5S;}
		    	else {tempW6 = L_U16_TIME_10MSLOOP_5S;}                    
              #else
                if(HSA_phase_out_rate > 0) {tempW6 = (HSA_retaining_press/HSA_phase_out_rate) + TIME_2000MS;}
		    	else {tempW6 = TIME_2000MS;}
		      #endif
		    	if(tempW6 < HSA_phase_out_time) {tempW6 = HSA_phase_out_time;}
            
		    	if(HSA_pressure_release_time<tempW6) {HSA_pressure_release_time = tempW6;}
		    	else                                 {tempW6 = HSA_pressure_release_time;}				
		    	
			  #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
		        if(HSA_retaining_press<=MPRESS_0BAR)
		    	{
		    		if(tempW6 >= HSA_pressure_release_counter) {tempW6 = HSA_pressure_release_counter - 1;}
		    	}	
		      #elif __TCMF_CONTROL
		    	if((MFC_Current_HSA_S<=TC_I_150mA) && (MFC_Current_HSA_P<=TC_I_150mA)) 
		    	{
		    		if(tempW6 >= HSA_pressure_release_counter) {tempW6 = HSA_pressure_release_counter - 1;}
		    	}	
		      #endif

			  #if defined(__dDCT_INTERFACE)
                if((lcu1HdcActiveFlg==1)&&(lcs16HdcActiveFlg_on_cnt>L_U8_TIME_10MSLOOP_30MS))
                {
                	HSA_Control = HSA_NORMAL;
	    			HSA_reset();
	    		}
                else		
              #endif	    		
	    		if(HSA_pressure_release_counter<=tempW6) {
	    				HSA_PRESSURE_RELEASE();		
	    		}	
	    		else {
	    			HSA_Control = HSA_NORMAL;
	    			HSA_reset();
	    		}		
	    		break;	
	    	/*-------------------------------------------------------------------------- */
	    	case HSA_NORMAL:
	    		if((HSA_INHIBITION_flag==0) && ((STOP_ON_HILL_flag==1)
	    		  #if __ISG
	    		||(ISG_READY==1)
	    		  #endif
	    		)) 
	    		{
	    			HSA_Control = HSA_READY;
	    		}
	    		else { }
	    		break;	
        
	    	/*-------------------------------------------------------------------------- */
	    	default:
	    		if((HSA_INHIBITION_flag==0) && ((STOP_ON_HILL_flag==1)
	    		  #if __ISG
	    		||(ISG_READY==1)
	    		  #endif
	    		))
	    		{
	    			HSA_Control = HSA_READY;
	    		}
	    		else { }
	    		break;	
	    }	  

      #if __EPB_INTERFACE
	    HSA2EPB_INTERFACE();
      #endif

        HSA_PRESSURE_RELEASE_RATE_SET();
        HSA_CALC_CONTROL_PRESS();
        HSA_REBRAKING_DETECT();
        HSA_TCS_INTERFACE();
    }
  #if (__MGH60_CYCLE_TIME_OPT_HSA == ENABLE)
	else
	{
		HSA_reset();
	}
  #endif    
}

static void	HSA_VARIABLE_INITIALIZE(void)
{
	  if(wu1IgnRisingEdge==1)
		{
//			  HSA_reset();
     
        /* only initialize at IGN on */			
        lcu1HsaOverloadRoll = 0;
        lcHsau8OverloadDctCnt = 0;
        
		}
		else { }
}

static void	HSA_INHIBITION_CHECK(void)
{
    HSA_CLUTCH_RGEAR_FAIL_CHECK();
	
  #if (__GM_FailM==0)  /* !GM Korea */
	if((fu1OnDiag==1)||(init_end_flg==0)
	||(vdc_error_flg==1)                                              /* vdc error */
	|| (fu1AxErrorDet==1)                                              /* Ax sensor error */
	|| (fu1VoltageLowErrDet==1)                                        /* low voltage */ 	
	|| (fu1VoltageUnderErrDet==1)                                      /* under voltage */
	|| (flu1BrakeLampErrDet==1)                                        /* brake lamp relay error */
	|| ((HSA_TM_TYPE==HSA_MANUAL_TM)&&(lcu1HsaRgearSigFail==1))        /* M/T reverse gear SW error */	   
    || ((HSA_TM_TYPE==HSA_MANUAL_TM)&&(lcu1HsaClutchSigFail==1))       /* M/T clutch SW error */
    #if (HSA_VARIANT_ENABLE==ENABLE)
    ||(wu8HsaVariantCodingEndFlag==0)
    #endif     
    #if (HSA_INLINE_ENABLE == ENABLE)
    || (fu2HsaInlineState!=1)  
    #endif
	#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    || (lsespu1MpresByAHBgen3Invalid==1)
    #endif
    #if (__BRK_SIG_MPS_TO_PEDAL_SEN==ENABLE)   
    || (lsespu1BrkAppSenInvalid==1)
    #endif
    || (wu8IgnStat==CE_OFF_STATE)
    || (U8_HSA_Disable==1)       
	)
	{
		HSA_INHIBITION_flag=1;		
	}
  #else                /* GM Korea*/		 
	if((fu1OnDiag==1)||(init_end_flg==0)
	||(fu1MainCanLineErrDet==1)                                           /* Main CAN ERR */
	|| (fu1MCPErrorDet==1)                                             /* MP error */
	|| (fu1AxErrorDet==1)                                              /* Ax sensor error */
	|| (fu1ESCEcuHWErrDet==1)                                          /* ECU ERROR */
	|| (fu1VoltageLowErrDet==1)                                        /* low voltage */ 
	|| (fu1VoltageUnderErrDet==1)                                      /* under voltage */
	|| (fu1VoltageOverErrDet==1)                                       /* over voltage */   
	|| (fu1WheelFLErrDet==1)                                           /* FL WSS error */ 
	|| (fu1WheelFRErrDet==1)                                           /* FR WSS error */
	|| (fu1WheelRLErrDet==1)                                           /* RL WSS error */
	|| (fu1WheelRRErrDet==1)                                           /* RR WSS error */
	|| (fu1SubCanLineErrDet==1)                                        /* CE Line Fail Time Out Error */
	|| (hs_subnet_config_err_flg==0)                                   /* Subnet config error */
	|| (fu1TCUTimeOutErrDet==1)                                        /* TCM Fail */
	|| (fu1EMSTimeOutErrDet==1)                                        /* ECM Fail */
	|| ((HSA_TM_TYPE==HSA_MANUAL_TM)&&(lcu1HsaRgearSigFail==1))        /* M/T reverse gear SW error */	   
    || ((HSA_TM_TYPE==HSA_MANUAL_TM)&&(lcu1HsaClutchSigFail==1))       /* M/T clutch SW error */
    #if __BRAKE_FLUID_LEVEL==ENABLE
    ||(fu1DelayedBrakeFluidLevel==1)
	||(fu1LatchedBrakeFluidLevel==1)  	
		#endif
    #if __INHIBIT_HSA_FOR_IGN_OFFON	
	|| (fu8EngineModeStep<=1)                                          /* engine mode step */
	  #endif
	|| (U8_HSA_Disable==1)                                             /* HSA enable or disable By T/P */
    #if (HSA_VARIANT_ENABLE==ENABLE)
    ||(wu8HsaVariantCodingEndFlag==0)
    #endif
	#if (__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
    || (lsespu1MpresByAHBgen3Invalid==1)
    #endif
    #if (__BRK_SIG_MPS_TO_PEDAL_SEN==ENABLE)   
    || (lsespu1BrkAppSenInvalid==1)
    #endif
    || (wu8IgnStat==CE_OFF_STATE)      	
	)
	{
		HSA_INHIBITION_flag=1;
	}
  #endif		 
	else 
	{
	  #if defined(__CAN_SW)&&(__AVH_SWITCH_USING_CTRL_BOX==0)
		if(HSA_CAN_SWITCH==1) {HSA_INHIBITION_flag=1;}
		else {HSA_INHIBITION_flag=0;}
	  #else	
		HSA_INHIBITION_flag=0;
	  #endif	
	}

  #if __ISG
    if((HSA_INHIBITION_flag==1)||(lespu8EMS_EmsIsgStat==ISG_FAULT_STATUS)||(ccu1IsgEmsH2Timeout==1))
    {
    	ISG_INHIBITION_flag = 1;
    }
    else
    {
        ISG_INHIBITION_flag = 0;
    }
  #endif
}

static void	LCHSA_vCheckMpressOn(void)
{
  #if (__AHB_GEN3_SYSTEM==ENABLE)    || (__IDB_LOGIC == ENABLE)
//	if((fu1BcpPriSusDet==0)&&(fu1BcpSecSusDet==0))
//	{
//		tempW1 = (lcans16AhbCircuitBoostPressP > lcans16AhbCircuitBoostPressS)? lcans16AhbCircuitBoostPressS:lcans16AhbCircuitBoostPressP; 
//	}
//	else if((fu1BcpPriSusDet==0)&&(fu1BcpSecSusDet==1))
//	{
//		tempW1 = lcans16AhbCircuitBoostPressP;
//	}
//	else if((fu1BcpPriSusDet==1)&&(fu1BcpSecSusDet==0))
//	{
//		tempW1 = lcans16AhbCircuitBoostPressS;
//	}
//	else /* both suspect */
//	{
//		tempW1 = lsesps16MpressByAHBgen3; /* average value */
//	}

	if(fu1BcpPriSusDet==0)
	{
		tempW1 = lis16MasterPress; 
	}
	else 
	{
		tempW1 = lsesps16MpressByAHBgen3; /* average value */
	}


	if(lcu1HsaMpressOn==0)
	{
		if((lsesps16EstBrkPressByBrkPedalF >= S16_AHB_HSA_PEDAL_ON_TH)&&(liu1AHBon==1)&&(tempW1 >= S16_AHB_HSA_PEDAL_ON_TH))
		{
			if(lcu8HsaAhbMpressOnCnt >= U8_AHB_HSA_PEDAL_ON_TH_TIME)
			{
				lcu1HsaMpressOn = 1;
			}
			else
			{
				lcu8HsaAhbMpressOnCnt = lcu8HsaAhbMpressOnCnt + 1;
			}
		}
		else
		{
			if(lcu8HsaAhbMpressOnCnt>0)
			{
				lcu8HsaAhbMpressOnCnt = lcu8HsaAhbMpressOnCnt - 1;
			}
			else
			{
				lcu8HsaAhbMpressOnCnt = 0;
			}
		}
	}
	else
	{
		lcu8HsaAhbMpressOnCnt = 0;
		if(lsesps16EstBrkPressByBrkPedalF < S16_AHB_HSA_PEDAL_OFF_TH) /*pedal suspect �߰� �ʿ� */
		{
			lcu1HsaMpressOn = 0;
		}
		else
		{
			;
		}
	}
	
	if(lcu1HsaMpressOn==1)
	{
		lcs16HsaMpress = tempW1;
	}
	else
	{
		lcs16HsaMpress = MPRESS_0BAR;
	}  
  #elif (__BRK_SIG_MPS_TO_PEDAL_SEN==ENABLE)   
	lcs16HsaMpress = lsesps16EstBrkPressByBrkPedalF;
	lcu1HsaMpressOn = lsespu1PedalBrakeOn;
  #elif defined (__BLS_NO_WLAMP_FM)
	lcs16HsaMpress = mpress;
	
	if((fu1BlsSusDet==1)||(fu1BLSErrDet==1))
	{
		if(lcu1HsaMpressOn==0)
		{
			if(lcs16HsaMpress >= (MPRESS_4BAR + S16_HSA_MAX_MP_OFFSET_BLS_FAIL))
			{
				lcu1HsaMpressOn = 1;
			}
			else { }
		}
		else /* lcu1HsaMpressOn==1 */
		{
			if(lcs16HsaMpress < (MPRESS_1BAR + S16_HSA_MAX_MP_OFFSET_BLS_FAIL))
			{
				lcu1HsaMpressOn = 0;
			}
			else { }
		}
	}
	else
	{
		lcu1HsaMpressOn = MPRESS_BRAKE_ON;
	}
  #else
    lcs16HsaMpress = mpress;
    lcu1HsaMpressOn = MPRESS_BRAKE_ON;
  #endif
}

static void	HSA_CLUTCH_RGEAR_FAIL_CHECK(void)
{
/********************************* Clutch signal Check *******************************/
	  #if (__CLUTCH_SWITCH_TYPE==ANALOG_TYPE)
	    if (fsu1ClutchSwitch_error_flg==1)
	    {
	        lcu1HsaClutchSigFail=1;
	    }
	    else
	    {
	    	lcu1HsaClutchSigFail=0;
	    }
	  #elif (__CLUTCH_SWITCH_TYPE==CAN_TYPE)
	    if (lespu1_TopTrvlCltchSwActV==0) /* clutch signal unvalid */
	    {
	    	if (lcHsau8ClutchErrorCnt<HSA_CLUTCH_RGEAR_ERROR_CHK_TIME) 
	    	{
	    		lcHsau8ClutchErrorCnt = lcHsau8ClutchErrorCnt + 1;
	    	}
	    	else
	    	{
	    		lcHsau8ClutchErrorCnt = HSA_CLUTCH_RGEAR_ERROR_CHK_TIME;
	    		lcu1HsaClutchSigFail=1;
	    	}
	    }
	    else /* clutch signal valid */
	    {
	    	if (lcHsau8ClutchErrorCnt>0) 
	    	{
	    		lcHsau8ClutchErrorCnt = lcHsau8ClutchErrorCnt - 1;
	    	}
	    	else
	    	{
	    		lcHsau8ClutchErrorCnt = 0;
	    		lcu1HsaClutchSigFail=0;
	    	}
	    }
	  #else
	    lcu1HsaClutchSigFail=1;
	  #endif

/********************************* Reverse gear signal Check **************************/	  
	  #if (__GEAR_SWITCH_TYPE==ANALOG_TYPE)
	    if (fsu1GearRSwitch_error_flg==1)
	    {
	        lcu1HsaRgearSigFail=1;
	    }
	    else
	    {
	    	lcu1HsaRgearSigFail=0;
	    }
	  #elif (__GEAR_SWITCH_TYPE==CAN_TYPE)
	    if (lespu1TCU_MT_R_GEAR_ERR==1) /* Reverse gear signal unvalid */
	    {
	    	if (lcHsau8RgearErrorCnt<HSA_CLUTCH_RGEAR_ERROR_CHK_TIME) 
	    	{
	    		lcHsau8RgearErrorCnt = lcHsau8RgearErrorCnt + 1;
	    	}
	    	else
	    	{
	    		lcHsau8RgearErrorCnt = HSA_CLUTCH_RGEAR_ERROR_CHK_TIME;
	    		lcu1HsaRgearSigFail=1;
	    	}
	    }
	    else /* Reverse gear signal valid */
	    {
	    	if (lcHsau8RgearErrorCnt>0) 
	    	{
	    		lcHsau8RgearErrorCnt = lcHsau8RgearErrorCnt - 1;
	    	}
	    	else
	    	{
	    		lcHsau8RgearErrorCnt = 0;
	    		lcu1HsaRgearSigFail=0;
	    	}
	    }	  
	  #else
	    lcu1HsaRgearSigFail=1;
	  #endif
}

static void	STOP_ON_HILL(void)
{
	uint8_t HSA_BACKWARD = 0;
	
/******************************************************************************/	
		#if __AX_SENSOR
	  #if __Ax_OFFSET_APPLY	
    HSA_ax_sen_filt = (lss16absAxOffsetComp1000g)/10;
  #if __HDC  
    if (lcu8HdcEcsCompEnable==1)
    {
        HSA_ax_sen_filt = (lss16absAxOffsetComp1000g + lcs16HdcEcsLongGComp)/10;
    }
  #endif

	if(lsabsu1AxOfsNone == 1)
	{
		if(HSA_ax_sen_filt > S16_HSA_AXOFFSET_NONE_COMP)
		{
			HSA_ax_sen_filt = HSA_ax_sen_filt - S16_HSA_AXOFFSET_NONE_COMP;
		}
		else if(HSA_ax_sen_filt < -S16_HSA_AXOFFSET_NONE_COMP)
		{
			HSA_ax_sen_filt = HSA_ax_sen_filt + S16_HSA_AXOFFSET_NONE_COMP;
		}
		else 
		{
			HSA_ax_sen_filt = 0; 
		}
	}

	  #else	/*!__Ax_OFFSET_APPLY */
    HSA_ax_sen_filt = (lss16absAx1000g)/10;
  #if __HDC  
    if (lcu8HdcEcsCompEnable==1)
    {
        HSA_ax_sen_filt = (lss16absAx1000g + lcs16HdcEcsLongGComp)/10;
    }
  #endif
	  #endif /*__Ax_OFFSET_APPLY */

      #if (HSA_VARIANT_ENABLE==ENABLE)
    if(wu8HsaVariantCodingEndFlag==0)
    {
    	HSA_ax_sen_filt = 0;
    }
    else 
    {
    	;
    }
      #endif

		#else /* !__AX_SENSOR */
  #if (__EPB_INTERFACE==DISABLE)
	HSA_ax_sen_filt=0;
  #else
	HSA_ax_sen_filt=ccs16_EPB510_LongAcceleration;
  #endif
		#endif
		
	if(HSA_ax_sen_filt>=0) {HSA_ax_sen_filt_abs = HSA_ax_sen_filt;}
	else				   {HSA_ax_sen_filt_abs = -HSA_ax_sen_filt;}

/******************************************************************************/	
	if(S16_HSA_ENTER_LIMIT_MPRESS < MPRESS_3BAR) 
	{
		lchsas16EntLimitMp = MPRESS_3BAR;
	}
	else 
	{
		lchsas16EntLimitMp = S16_HSA_ENTER_LIMIT_MPRESS;
	}
	if(S16_HSA_ENTER_INBIT_MPRESS < MPRESS_1BAR)
	{
		lchsas16EntInhibitMp = MPRESS_1BAR;
	}
	else 
	{
		lchsas16EntInhibitMp = S16_HSA_ENTER_INBIT_MPRESS;
	}	
	
  #if (__BLS_NO_WLAMP_FM==ENABLE)
    if((fu1BlsSusDet==1)||(fu1BLSErrDet==1)) 
    {
    	lchsas16EntLimitMp = lchsas16EntLimitMp + S16_HSA_MAX_MP_OFFSET_BLS_FAIL;
    	lchsas16EntInhibitMp = lchsas16EntInhibitMp + S16_HSA_MAX_MP_OFFSET_BLS_FAIL;
    }
    else { }
  #endif	
	
	if(lchsas16EntLimitMp<(lchsas16EntInhibitMp+MPRESS_3BAR)) 
	{
		lchsas16EntInhibitMp = lchsas16EntLimitMp - MPRESS_3BAR;
		if(lchsas16EntInhibitMp<MPRESS_1BAR)
		{
			lchsas16EntInhibitMp = MPRESS_1BAR;
		}
		else { }
	}
	else { }
/******************************************************************************/	

	if(HSA_TM_TYPE==HSA_AUTO_TM)
	{
        /* gs_sel -> 0:P 1:L 2:2 3:3 4:DS mode 5:D 6:N 7:R 8:sport mode/manual */		
        HSA_gs_sel = gs_sel;
        if((HSA_gs_sel==1)||(HSA_gs_sel==2)||(HSA_gs_sel==3)||(HSA_gs_sel==4)||(HSA_gs_sel==8))
        {
            HSA_gs_sel=AT_DRIVE;
        }
        else { }
        
        if((HSA_gs_sel == AT_REVERSE) || ((HSA_gs_sel==AT_NEUTRAL) &&(R_GEAR_DET==1))) {HSA_BACKWARD = 1;}
	}
	else
	{
		if(GEAR_POSITION_SENSOR_ALLOWED==1)
		{
            /* gs_pos -> 0:N,P 1-4:D 7:R */			
			HSA_gs_sel = gs_pos;
            if((HSA_gs_sel==MT_GEAR1_POS)||(HSA_gs_sel==MT_GEAR2_POS)||(HSA_gs_sel==MT_GEAR3_POS)) {HSA_gs_sel = MT_DRIVE;}
            else { }
            if(HSA_gs_sel==MT_NEUTRAL_POS) {HSA_gs_sel = MT_NEUTRAL;}
            else { }
            if(HSA_gs_sel==MT_REVERSE_POS) {HSA_gs_sel = MT_REVERSE;}
            else { }
		}
	    else if(REVERSE_GEAR_INFORM_ALLOWED==1)
	    {
	      #if (__CAR == GM_C100)
		    HSA_gs_sel = lespu8TCU_TAR_GC;
		    if(lespu8TCU_G_SEL_DISP==7) {HSA_gs_sel = MT_REVERSE;}
		    else { }
		  #elif (__CAR == FORD_C214) 
		    if(cmu16_TransmissionGearPosition==14) {HSA_gs_sel = MT_REVERSE;}
	    	else                                   {HSA_gs_sel = MT_NEUTRAL;}
	      #else
	        if(NEUTRAL_SWITCH_ALLOWED==1)
	        {
	        	if(fu1GearR_Switch==1)
	        	{
	        		HSA_gs_sel = MT_REVERSE;
	        	}
	        	else
	        	{
	        		if(lespu1EPB_GEAR_N_SWITCH_NEUTRAL==1)
	        		{
	        			HSA_gs_sel = MT_NEUTRAL;
	        		}
	        		else
	        		{
	        			HSA_gs_sel = MT_DRIVE;
	        		}
	        	}
	        }
	        else
	        {
		        if(fu1GearR_Switch==1) {HSA_gs_sel = MT_REVERSE;}
		        else                   {HSA_gs_sel = MT_NEUTRAL;}		    
		    }
		  #endif
	    }
	    else
	    {
	    	HSA_gs_sel = MT_NEUTRAL;
	    }
	  	
	  	if(HSA_gs_sel == MT_REVERSE) {HSA_BACKWARD = 1;}	    
    }

	HSA_Ax_Increase_flag_Old = HSA_Ax_Increase_flag;
    if(HSA_BACKWARD==0)
    {
	    if(HSA_Ax_Increase_flag==1)
	    {
	        if(HSA_ax_sen_filt_old > HSA_ax_sen_filt)
	        {
	            HSA_Ax_Increase_flag = 0;
	        }
	    }
	    else
	    {
	        if(HSA_ax_sen_filt_old < HSA_ax_sen_filt)
	        {
	            HSA_Ax_Increase_flag = 1;
	        }
	    }
	}
	else
	{
	    if(HSA_Ax_Increase_flag==1)
	    {
	        if(HSA_ax_sen_filt_old < HSA_ax_sen_filt)
	        {
	            HSA_Ax_Increase_flag = 0;
	        }
	    }
	    else
	    {
	        if(HSA_ax_sen_filt_old > HSA_ax_sen_filt)
	        {
	            HSA_Ax_Increase_flag = 1;
	        }
	    }
	}
	
	tempW1 = 0;	
	if(HSA_Ax_Increase_flag_Old!=HSA_Ax_Increase_flag)
	{
		tempW1 = 1;
	}
	else { }
    
    if((vref <= VREF_3_KPH) 
      && (((HSA_Ax_Increase_flag==1) && (lcs16HsaMpress>=lchsas16EntInhibitMp)) || ((HSA_ax_cnt>0) && (lcs16HsaMpress>MPRESS_1BAR)) || ((vref <= VREF_1_KPH) && (lcs16HsaMpress>=lchsas16EntInhibitMp))))
    {
    	if((HSA_ax_sum>=30000) || (HSA_ax_sum<=-30000) || (HSA_ax_cnt>=10000))
    	{    		
	        HSA_ax_sum = (HSA_ax_sum + HSA_ax_sen_filt) - HSA_ax_avg;
	        if(HSA_ax_sum >= 32000) {HSA_ax_sum = 32000;}
	        else if(HSA_ax_sum <= -32000) {HSA_ax_sum = -32000;}
	        else { }
	    }
	    else
	    {
	        HSA_ax_sum = HSA_ax_sum + HSA_ax_sen_filt;
	        HSA_ax_cnt = HSA_ax_cnt + 1;
	    }	    	
        HSA_ax_avg = HSA_ax_sum/HSA_ax_cnt;
        
      #if __IMPROVE_DIVE_DETECTION==ENABLE
        tempW2 = abs(lss16absAx1000gDiff);
        if(lcu1HsaDiveDetect==0)
        {
        	if((tempW2>S16_HSA_AX_DIFF_FOR_DIVE_DETECT)&&(lcs16HsaAxAvgTime<1))
        	{
        		lcu1HsaDiveDetect = 1;
        		lcs16HsaAxDiffMax = tempW2; 
        	}
        	else 
        	{
        		lcs16HsaAxDiffMax = 0;
        	}
        }
        else
        {
        	if(lcs16HsaAxAvgTime>(int16_t)lcu8HsaAxAvgValidTh)
        	{
        		lcu1HsaDiveDetect = 0;
        		lcs16HsaAxDiffMax = 0;
        	}
        	else 
        	{
        		if(tempW2 > lcs16HsaAxDiffMax) {lcs16HsaAxDiffMax = tempW2;}
        		else { }
        	}
        }

        lcu8HsaAxAvgValidTh = (uint8_t)LCESP_s16IInter3Point(lcs16HsaAxDiffMax, S16_HSA_AX_DIFF_LOW_REF, (int16_t)U8_HSA_AX_AVG_VALID_TH_LOW_DIF, 
																 			    S16_HSA_AX_DIFF_MED_REF, (int16_t)U8_HSA_AX_AVG_VALID_TH_MID_DIF, 
																			    S16_HSA_AX_DIFF_MAX_REF, (int16_t)U8_HSA_AX_AVG_VALID_TH_MAX_DIF);
		if(lcu8HsaAxAvgValidTh<L_U8_TIME_10MSLOOP_200MS)
		{
			lcu8HsaAxAvgValidTh = L_U8_TIME_10MSLOOP_200MS;
		}
		else { }

        if((lcs16HsaAxSum>=30000) || (lcs16HsaAxSum<=-30000) || (lcs16HsaAxAvgTime>=10000))
    	{
	        lcs16HsaAxSum = lcs16HsaAxSum + HSA_ax_sen_filt - lcs16HsaAxAvg;
	        if(lcs16HsaAxSum >= 32000) {lcs16HsaAxSum = 32000;}
	        else if(lcs16HsaAxSum <= -32000) {lcs16HsaAxSum = -32000;}
	        else { }
			lcs16HsaAxAvg = lcs16HsaAxSum/lcs16HsaAxAvgTime;	 
	    }
        else if(((lcu1HsaDiveDetect==1)&&(tempW1>0))||(lcs16HsaAxAvgTime>0))
        {
        	lcs16HsaAxSum = lcs16HsaAxSum + HSA_ax_sen_filt;
        	lcs16HsaAxAvgTime = lcs16HsaAxAvgTime + 1;
        	lcs16HsaAxAvg = lcs16HsaAxSum/lcs16HsaAxAvgTime;	 
        }
        else
        {
        	lcs16HsaAxSum = 0;
        	lcs16HsaAxAvgTime = 0;
        	lcs16HsaAxAvg = 0;
        }
      #endif
    }
    else
    {
        HSA_ax_sum = 0;
        HSA_ax_cnt = 0;
        HSA_ax_avg = 0;
        lcs16HsaAxSum = 0;
        lcs16HsaAxAvgTime = 0;
        lcs16HsaAxAvg = 0;
        lcu1HsaDiveDetect = 0;
		lcs16HsaAxDiffMax = 0;
        lcu8HsaAxAvgValidTh = 0;
    }
    
	

	
	if(HSA_ax_sen_filt_old == HSA_ax_sen_filt)
	{
		if(HSA_ax_stationary_cnt<200) {HSA_ax_stationary_cnt++;}
		
		if(HSA_ax_stationary_cnt>HSA_AX_STATIONARY_TIME)
		{
			HSA_AX_STATIONARY = 1;
			HSA_EXIT_ax = S16_HSA_a_uphill_detection_along - 2;
			if(HSA_EXIT_ax < (S16_HSA_a_uphill_detection_along/2)) {HSA_EXIT_ax = (S16_HSA_a_uphill_detection_along/2);}
		}		
	}
	else
	{
		HSA_ax_stationary_cnt = 0;
		HSA_AX_STATIONARY = 0;
		HSA_EXIT_ax = (S16_HSA_a_uphill_detection_along/2);
		if(HSA_EXIT_ax > (S16_HSA_a_uphill_detection_along-2)) {HSA_EXIT_ax = (S16_HSA_a_uphill_detection_along - 2);}		
	}

	if(HSA_Control==HSA_RELEASE)
	{
		HSA_EXIT_ax = 0;
	}

	if(STOP_ON_HILL_flag==0) {
		if((vref<=S16_HSA_STOP_ON_HILL_MIN_SPD) && (lcs16HsaMpress>=lchsas16EntLimitMp)) {
			if(AX_SENSOR_ALLOWED==1) {
				if(HSA_TM_TYPE==HSA_AUTO_TM) {
					  #if defined(__dDCT_INTERFACE)
					if((HSA_ax_sen_filt>=lcs16HrcReqHsaActMinSlope) && ((HSA_gs_sel==AT_DRIVE)||((HSA_gs_sel==AT_NEUTRAL)))) {
					  #else
					if((HSA_ax_sen_filt>=S16_HSA_a_uphill_detection_along) && (HSA_gs_sel==AT_DRIVE)) {
					  #endif
						STOP_ON_UPHILL_flag = 1;
					}
					  #if defined(__dDCT_INTERFACE)
					else if((HSA_ax_sen_filt<=-lcs16HrcReqHsaActMinSlope) && ((HSA_gs_sel==AT_REVERSE)||(HSA_gs_sel==AT_NEUTRAL))) {
					  #else
					else if((HSA_ax_sen_filt<=-S16_HSA_a_uphill_detection_along) && (HSA_gs_sel==AT_REVERSE)) {
				      #endif
						STOP_ON_DOWNHILL_flag = 1;
					}
					else { }
				}
				else {
					if(GEAR_POSITION_SENSOR_ALLOWED==1) {
						if((HSA_ax_sen_filt>=S16_HSA_a_uphill_detection_along) && ((HSA_gs_sel==MT_DRIVE)||(HSA_gs_sel==MT_NEUTRAL))) {
							STOP_ON_UPHILL_flag = 1;
						}
						else if((HSA_ax_sen_filt<=-S16_HSA_a_uphill_detection_along) && (HSA_gs_sel==MT_REVERSE)) {
							STOP_ON_DOWNHILL_flag = 1;
						}
						else { }
					}	
					else if(REVERSE_GEAR_INFORM_ALLOWED==1) 
					{
					    if(NEUTRAL_SWITCH_ALLOWED==1)
					    {
					    	if((HSA_ax_sen_filt>=S16_HSA_a_uphill_detection_along) && (HSA_gs_sel==MT_DRIVE)) {
						    	STOP_ON_UPHILL_flag = 1;
						    }
						    else if((HSA_ax_sen_filt<=-S16_HSA_a_uphill_detection_along) && (HSA_gs_sel==MT_REVERSE)) {
						    	STOP_ON_DOWNHILL_flag = 1;
						    }
						    else { }
					    }
					    else
					    {
						    if((HSA_ax_sen_filt>=S16_HSA_a_uphill_detection_along) && (HSA_gs_sel!=MT_REVERSE)) {
						    	STOP_ON_UPHILL_flag = 1;
						    }
						    else if((HSA_ax_sen_filt<=-S16_HSA_a_uphill_detection_along) && (HSA_gs_sel==MT_REVERSE)) {
						    	STOP_ON_DOWNHILL_flag = 1;
						    }
						    else { }
						}
					}
					else {
						if(HSA_ax_sen_filt_abs>=S16_HSA_a_uphill_detection_along) {
							STOP_ON_HILL_flag = 1;
						}
						else { }
					}
				}

			  #if defined(__dDCT_INTERFACE)
			    if((HSA_TM_TYPE==HSA_MANUAL_TM)||((HSA_TM_TYPE==HSA_AUTO_TM)&&(lcs16HrcReqHsaActMinSlope-S16_HSA_d_Flat_by_LongG_AVG>=3)))
			    {
			    	if((STOP_ON_UPHILL_flag==1) && (HSA_ax_avg<S16_HSA_d_Flat_by_LongG_AVG))
					{
						STOP_ON_UPHILL_flag = 0;
					}
					if((STOP_ON_DOWNHILL_flag==1) && (HSA_ax_avg>-S16_HSA_d_Flat_by_LongG_AVG))
					{
						STOP_ON_DOWNHILL_flag = 0;
					}
				}
				else { }
			  #else				
			  #if __IMPROVE_DIVE_DETECTION==ENABLE
				if(lcu1HsaDiveDetect==1)
				{
					STOP_ON_UPHILL_flag = 0;
					STOP_ON_DOWNHILL_flag = 0;
				}
				else if(lcs16HsaAxAvgTime>(int16_t)lcu8HsaAxAvgValidTh)
				{
					if((STOP_ON_UPHILL_flag==1)&&(lcs16HsaAxAvg<S16_HSA_d_Flat_by_LongG_AVG))
					{
						STOP_ON_UPHILL_flag = 0;
					}
					else { }
						
					if((STOP_ON_DOWNHILL_flag==1)&&(lcs16HsaAxAvg>-S16_HSA_d_Flat_by_LongG_AVG))
					{
						STOP_ON_DOWNHILL_flag = 0;
					}
					else { }
				}
				else
				{
					;
				}
			  #else
				if((STOP_ON_UPHILL_flag==1) && (HSA_ax_avg<S16_HSA_d_Flat_by_LongG_AVG))
				{
					STOP_ON_UPHILL_flag = 0;
				}
				if((STOP_ON_DOWNHILL_flag==1) && (HSA_ax_avg>-S16_HSA_d_Flat_by_LongG_AVG))
				{
					STOP_ON_DOWNHILL_flag = 0;
				}
			  #endif
			  #endif
			}
			else {
				if(HSA_TM_TYPE==HSA_AUTO_TM) {
					if((HSA_gs_sel==AT_DRIVE) || (HSA_gs_sel==AT_REVERSE)) {
						STOP_ON_HILL_flag = 1;
					}
					else { }
				}
				else {
					if(GEAR_POSITION_SENSOR_ALLOWED==1) {
						if((HSA_gs_sel==MT_DRIVE) || (HSA_gs_sel==MT_REVERSE)) {
							STOP_ON_HILL_flag = 1;
						}
						else { }
					}
					else {
						STOP_ON_HILL_flag = 1;
					}
				}
			}
		}
		if((STOP_ON_UPHILL_flag==1) || (STOP_ON_DOWNHILL_flag==1)) {
			STOP_ON_HILL_flag = 1;
		}
		else { }
	}
	else {
		if(((vref > S16_HSA_EXIT_SPEED)&&(HSA_Control!=HSA_RELEASE)) || ((HSA_flg==0)&&(lcs16HsaMpress<=lchsas16EntInhibitMp)) || (lcu1HsaDiveDetect==1)) 
		{
			STOP_ON_HILL_flag = 0;
		}
		else {
			if(AX_SENSOR_ALLOWED==1) {
				if(STOP_ON_UPHILL_flag==1) {
				  #if defined(__dDCT_INTERFACE)
				  #else
					if(HSA_ax_sen_filt<=(HSA_EXIT_ax)) 
					{
					  #if __IMPROVE_DIVE_DETECTION==ENABLE
						if((lcs16HsaAxAvgTime>(int16_t)lcu8HsaAxAvgValidTh)&&(lcs16HsaAxAvg>=S16_HSA_a_uphill_detection_along))
						{
							;
						}
						else
						{
							STOP_ON_HILL_flag = 0;
						}
                      #else
						STOP_ON_HILL_flag = 0;
					  #endif
					}
					else { }
				  #endif
					
					if(HSA_TM_TYPE==HSA_AUTO_TM) {
						  #if defined(__dDCT_INTERFACE)
						if((HSA_gs_sel!=AT_DRIVE)&&(HSA_gs_sel!=AT_NEUTRAL)) { 
						  #else
						if(HSA_gs_sel!=AT_DRIVE) { 
						  #endif
							STOP_ON_HILL_flag = 0;
						}
						else { }
					}
					else 
					{
						if(GEAR_POSITION_SENSOR_ALLOWED==1)
						{
							if((HSA_gs_sel!=MT_DRIVE)&&(HSA_gs_sel!=MT_NEUTRAL))
							{
								STOP_ON_HILL_flag = 0;
							}
							else { }
						}
						else if(REVERSE_GEAR_INFORM_ALLOWED==1)
						{
							if(NEUTRAL_SWITCH_ALLOWED==1)
							{
								if(HSA_gs_sel!=MT_DRIVE)
								{
									STOP_ON_HILL_flag = 0;
								}
								else { }
							}
							else
							{
								if(HSA_gs_sel==MT_REVERSE)
								{
									STOP_ON_HILL_flag = 0;
								}
								else { }
							}
						}
						else { }
					}
				}
				else if(STOP_ON_DOWNHILL_flag==1) {
				  #if defined(__dDCT_INTERFACE)
				  #else
					if(HSA_ax_sen_filt>=(-HSA_EXIT_ax)) 
					{
					  #if __IMPROVE_DIVE_DETECTION==ENABLE
						if((lcs16HsaAxAvgTime>(int16_t)lcu8HsaAxAvgValidTh)&&(lcs16HsaAxAvg<=-S16_HSA_a_uphill_detection_along))
						{
							;
						}
						else
						{
							STOP_ON_HILL_flag = 0;
						}
					  #else
						STOP_ON_HILL_flag = 0;
					  #endif
					}
					else { }
				  #endif
					
					if(HSA_TM_TYPE==HSA_AUTO_TM) {
						  #if defined(__dDCT_INTERFACE)
						if((HSA_gs_sel!=AT_REVERSE)&&(HSA_gs_sel!=AT_NEUTRAL)) {
						  #else
						if(HSA_gs_sel!=AT_REVERSE) {
						  #endif
							STOP_ON_HILL_flag = 0;
						}
						else { }
					}
					else {
						if( ((GEAR_POSITION_SENSOR_ALLOWED==1) && (HSA_gs_sel!=MT_REVERSE)) 
						  || ((REVERSE_GEAR_INFORM_ALLOWED==1) && (HSA_gs_sel!=MT_REVERSE)) ) {
						  	STOP_ON_HILL_flag = 0;
						}
						else { }
					}
				}
				else {
/*					if(abs(along)<=(S16_HSA_a_uphill_detection_along/2)) { */
					if(HSA_ax_sen_filt_abs<=(HSA_EXIT_ax)) {
						STOP_ON_HILL_flag = 0;
					}
					else { }
				}
			}
			else {
				if(HSA_TM_TYPE==HSA_AUTO_TM) {
					if((HSA_gs_sel!=AT_DRIVE) && (HSA_gs_sel!=AT_REVERSE)) {
						STOP_ON_HILL_flag = 0;
					}
					else { }
				}
				else {
					if(GEAR_POSITION_SENSOR_ALLOWED==1) {
						if((HSA_gs_sel!=MT_DRIVE) && (HSA_gs_sel!=MT_REVERSE)) {
							STOP_ON_HILL_flag = 0;
						}
						else { }
					}
					else { }
/*					else {
						STOP_ON_HILL_flag = 0;
					}
*/
				}
			}
		}
		if(STOP_ON_HILL_flag==0) {
			STOP_ON_UPHILL_flag = 0;
			STOP_ON_DOWNHILL_flag = 0;
		}
		else { }
	}
	
	HSA_ax_sen_filt_old = HSA_ax_sen_filt;
}

  #if __ISG
static void	CHECK_ISG_REQUEST(void)
{
	/* Check Request */
	if((lespu8EMS_EmsIsgStat==ISG_ACTIVE_STATUS)||(lespu8EMS_EmsIsgStat==ISG_ENGINE_START_STATUS))
	{
		ISG_REQ_ON = 1;
	}
	else
	{
		ISG_REQ_ON = 0;
	}

    /* Check Gear Selection */
    /* gs_sel -> 0:P 1:L 2:2 3:3 5:D 6:N 7:R 8:sport mode/manual */		
    if((gs_sel==1)||(gs_sel==2)||(gs_sel==3)||(gs_sel==5))
    {
    	ISG_GEAR_OK = 1;
    }
    else
    {
    	ISG_GEAR_OK = 0;
    }

    if(ISG_INHIBITION_flag==0)
    {
        if(HSA_TM_TYPE==HSA_AUTO_TM)
        {
        	if((ISG_REQ_ON==1)&&(ISG_GEAR_OK==1))
        	{
          		if(HSA_ax_sen_filt<((int16_t)S8_ISG_HSA_ON_AX_TH_GEAR_D))
           		{
           			ISG_READY = 0;
           		}
           		else
           		{
           			ISG_READY = 1;
           		}
           	}
           	else
           	{
           	    ISG_READY = 0;
           	}
        }
        else /* HSA_TM_TYPE=HSA_MANUAL_TM */
        {
            ISG_READY = 0;
        }
    }
    else /* ISG_INHIBITION_flag=1 */
    {
    	ISG_READY = 0;
    }
}
  #endif


static void	HSA_CALC_ENTER_PRESS(void)
{
	if(AX_SENSOR_ALLOWED==1) 
	{
		if((STOP_ON_HILL_flag==1)||(ISG_READY==1))
		{ 
        	tempW7=HSA_ax_sen_filt_abs;
        	
			tempW1=S16_HSA_a_uphill_detection_along+S16_HSA_LongGSafetyFactor;
        	tempW2=tempW1*S16_HSA_PressToLongGRatio;
        	
			tempW3=S16_HSA_b_MidLongG+S16_HSA_LongGSafetyFactor;
        	tempW4=((tempW3*S16_HSA_PressToLongGRatio)*S16_HSA_PressRatioAtMidHill)/10;
        	
        	tempW5=S16_HSA_c_MaxLongG+S16_HSA_LongGSafetyFactor;
        	tempW6=tempW5*S16_HSA_PressToLongGRatio;
        	
        	
        	HSA_ENTER_press = LCESP_s16IInter3Point(tempW7, tempW1, tempW2, tempW3, tempW4, tempW5, tempW6);
        	
			if(HSA_ENTER_press<tempW2) {HSA_ENTER_press = tempW2;}
			else if (HSA_ENTER_press>tempW6) {HSA_ENTER_press = tempW6;}
			else { }

      	  #if __ISG
      		tempW3=(int16_t)S8_ISG_ON_MIN_SLOPE;
        	tempW4=S16_ISG_ON_MIN_SLOP_HOLD_PRESS;      
        
        	tempW8 = LCESP_s16IInter2Point(tempW7, tempW3, tempW4, tempW1, tempW2); 

        	if ((STOP_ON_HILL_flag==0)&&(ISG_READY==1))
        	{
        		HSA_ENTER_press = tempW8;
        	}
        	else
        	{
        	    ;
        	}
      	  #endif

 	  	  #if (__BLS_NO_WLAMP_FM==ENABLE)
			if((fu1BlsSusDet==1)||(fu1BLSErrDet==1)) 
			{
				HSA_ENTER_press = HSA_ENTER_press + S16_HSA_MAX_MP_OFFSET_BLS_FAIL;
			}
			else { }
	  	  #endif

			if(lcu1HsaOverloadRoll==1)
			{
				HSA_ENTER_press = HSA_ENTER_press + S16_HSA_HOLD_COMP_P_OVERLOAD;
			}
			else { }

			/* Low Limit hold press */
        	if(HSA_ENTER_press<MPRESS_5BAR) 
        	{
        		HSA_ENTER_press = MPRESS_5BAR; 
        	}
        	else 
        	{
        		;
        	}
        }
        else
		{
			HSA_ENTER_press = MPRESS_0BAR;
		}
    }    
	else 
	{
		HSA_ENTER_press = MPRESS_30BAR;
	}
}

static void	HSA_EXIT_CONDITION(void)
{	
	int16_t	HSA_EXIT_eng_torq_s, HSA_EXIT_eng_torq2, HSA_temp;
	HSA_RAPID_PHASE_OUT=0;

	if (((HSA_Control!=HSA_RELEASE)&&
	  (((STOP_ON_HILL_flag==0) 
	    #if __ISG
	  &&(HSA_ISG_flg==0)
	    #endif
	  )
	  ||(HSA_hold_counter>=HSA_hold_time_max)
	  ||(lcu1HsaClutchSwOffwithBrk==1)
	    #if __ISG
      ||((STOP_ON_HILL_flag==0)&&(HSA_ISG_flg==1)
	  &&((lespu1EMS_EngRunNorm==1)||(HSA_hold_cnt_rpm>=U8_ISG_End_RPM_OK_delay_time))
	    )	  
	    #endif
    ))
	  #if (__AVH==1)
	||(lcu8AvhEpbState==EPB_CLAMPED)
	  #endif
	  #if __EPB_INTERFACE
	||(HSA_EPB_ACTIVE_FLG==1)
	  #endif
    )
	{
		HSA_EXIT_CONDITION_SATISFY = 1;
	  #if __TCMF_CONTROL
		HSA_pressure_release_counter = 0;	/* 0 : HSA Pressure Fade-out mode enable */
	  #else	
		HSA_pressure_release_counter = 501;
	  #endif	
	    HSA_flag = 1;
	    /*HSA_PRESSURE_RELEASE_RATE_SET();*/
	}
	else 
	{
	  	S16_HSA_ETPerG_SteepSlope = S16_HSA_a_EngTorqPerLongG * HSA_ETPerGratio_SteepSlope;
	  	
		/*****************************************************************/
		if(AX_SENSOR_ALLOWED==1) {
			if(HSA_ax_sen_filt_abs<=HSA_Ax_ChangeETPerGratio)
			{
				HSA_EXIT_eng_torq = (HSA_ax_sen_filt_abs+S16_HSA_LongGSafetyFactor)*S16_HSA_a_EngTorqPerLongG;  
			}
			else
			{
				HSA_EXIT_eng_torq = ((HSA_Ax_ChangeETPerGratio+S16_HSA_LongGSafetyFactor)*S16_HSA_a_EngTorqPerLongG) 
									+ ((HSA_ax_sen_filt_abs-HSA_Ax_ChangeETPerGratio)*S16_HSA_ETPerG_SteepSlope);  
				HSA_temp		  = ((HSA_Ax_ChangeETPerGratio+S16_HSA_LongGSafetyFactor)*S16_HSA_a_EngTorqPerLongG) 
									+ ((S16_HSA_c_MaxLongG-HSA_Ax_ChangeETPerGratio)*S16_HSA_ETPerG_SteepSlope);  
				if(HSA_EXIT_eng_torq>HSA_temp) {HSA_EXIT_eng_torq = HSA_temp;}
			}
			if(HSA_EXIT_eng_torq <= S16_HSA_DEFAULT_ENG_IDLE_TORQ) 
			{
				HSA_EXIT_eng_torq = S16_HSA_DEFAULT_ENG_IDLE_TORQ;
			}
			else if(HSA_EXIT_eng_torq >= 600) 
			{
				HSA_EXIT_eng_torq = 600;
			}
			else
			{
				;
			}
			
			HSA_EXIT_eng_torq2 = ((HSA_EXIT_eng_torq-S16_HSA_b_LongGSFTorqComp)*9)/10;
			
			if(HSA_EXIT_eng_torq2 <= S16_HSA_DEFAULT_ENG_IDLE_TORQ) 
			{
				HSA_EXIT_eng_torq2 = S16_HSA_DEFAULT_ENG_IDLE_TORQ;
			}
			else if(HSA_EXIT_eng_torq2 >= 500) 
			{
				HSA_EXIT_eng_torq2 = 500;
			}
			else
			{
				;
			}
		}
		else {
			HSA_EXIT_eng_torq = 400;	/* for 10% grade hill */
			HSA_EXIT_eng_torq2 = 300;
		}
		
		tempB7 = 0;
		if(HSA_INHIBITION_flag==1) {
			tempB7 = 1;
			HSA_flag = 2;
		}
		else { }
		
		if(lcu1HsaMpressOn==0) 
		{
			HSA_EXIT_eng_torq_s = ((HSA_EXIT_eng_torq-S16_HSA_b_LongGSFTorqComp)*4)/5;
			
            if(HSA_EXIT_eng_torq_s <= S16_HSA_DEFAULT_ENG_IDLE_TORQ) 
			{
				HSA_EXIT_eng_torq_s = S16_HSA_DEFAULT_ENG_IDLE_TORQ;
			}
			else if(HSA_EXIT_eng_torq_s >= 400) 
			{
				HSA_EXIT_eng_torq_s = 400;
			}
			else
			{
				;
			}

			if(HSA_TM_TYPE==HSA_AUTO_TM) 
			{
				  #if defined(__dDCT_INTERFACE)
				if(lcs16HrcReqHsaActMinSlope<S16_HSA_a_uphill_detection_along)
				{
					tempW3 = 1;
				}
				else
				{
					tempW3 = (int16_t)U8_HSA_AT_MIN_MTP_FOR_EXIT;
				}
				  #else				  
				tempW3 = (int16_t)U8_HSA_AT_MIN_MTP_FOR_EXIT;
				  #endif

				if((mtp>=tempW3) && (eng_torq_rel>=HSA_EXIT_eng_torq_s)) {
					if(HSA_accel_cnt<=300) {HSA_accel_cnt++;}
					else { }
					
					if(HSA_accel_cnt2<=300) {HSA_accel_cnt2+=2;}
					else { }
				}
				else if(HSA_accel_cnt>5) {
					if((mtp<tempW3) || ((eng_torq_rel+40)<HSA_EXIT_eng_torq_s)) {
						HSA_accel_cnt -= 2;
					}
					else { }
					
					if((mtp<tempW3) || ((eng_torq_rel+80)<HSA_EXIT_eng_torq_s)) 
					{
						HSA_accel_cnt2 -= 4;
					}
					else if((eng_torq_rel+40)<HSA_EXIT_eng_torq_s)
					{
						HSA_accel_cnt2 -= 2;
					}
					else 
					{
						HSA_accel_cnt2 += 1;
					}

				}
				else {
					HSA_accel_cnt = 0;
					HSA_accel_cnt2 = 0;
				}
				
				if(HSA_accel_cnt >= U16_HSA_c_ReleaseTim_4_Accel_1_AT) {HSA_RAPID_PHASE_OUT = 1;} 
				else { }
				
				if(HSA_accel_cnt2 >= U16_HSA_d_ReleaseTim_4_Accel_2_AT) {HSA_RAPID_PHASE_OUT = 1;}
				else { }
				
				if((BTCS_ON==1)
				||((mtp>=(int16_t)U8_HSA_FAST_EXIT_MTP_TH)&&((FL.arad>=S16_HSA_AT_FAST_EXIT_ARAD_TH)||(FR.arad>=S16_HSA_AT_FAST_EXIT_ARAD_TH)
				||(RL.arad>=S16_HSA_AT_FAST_EXIT_ARAD_TH)||(RR.arad>=S16_HSA_AT_FAST_EXIT_ARAD_TH)))
				)
				{
					tempB7 = 1;
					lcu1HsaExitBySpinAT = 1;
					lcs16HsaExitBySpinPress = HSA_retaining_press;
				}
				else { }				
			}
			else 
			{
				if(CLUTCH_POSITION_ALLOWED==1) 
				{
					if(U8_HSA_SET_CLUTCH_ENGAGE_EST>0)
					{
					    if(CLUTCH_POSITION == ENGAGE) 
					    {
					    	if((eng_torq_rel>=HSA_EXIT_eng_torq)&&((HSA_EST_CLUTCH_POSITION==ENGAGE)||(HSA_ENOUGH_ENG_TORQ==1)))
					    	{
                                HSA_RAPID_PHASE_OUT = 1;
					    	}
					    	else { }
					    }
					    else 
					    {
					    	if((eng_torq_rel>=HSA_EXIT_eng_torq)&&((HSA_EST_CLUTCH_POSITION==ENGAGE)||(HSA_ENOUGH_ENG_TORQ==1)))
					    	{
                                HSA_RAPID_PHASE_OUT = 1;
					    	}
					    	else { }
					    }
					}
					else
					{
					    if(CLUTCH_POSITION == ENGAGE) {
                        
					    	if((mtp>=20) && (eng_torq_rel>=HSA_EXIT_eng_torq2) && (eng_rpm >= 1000)) {tempB7 = 1;}
					    	else if((mtp>=10) && (eng_torq_rel>=HSA_EXIT_eng_torq_s) && (eng_rpm >= 900)) {HSA_RAPID_PHASE_OUT = 1;}
					    	else { }
					    	HSA_accel_cnt = 0;
					    }
					    else {
					    	tempW3 = (INT)U8_HSA_AT_MIN_MTP_FOR_EXIT;
					    	if((mtp>=10) && (eng_torq_rel>=HSA_EXIT_eng_torq2)) {
					    		if(HSA_accel_cnt<=300) 
					    		{
					    			if(eng_rpm >= 1000) {HSA_accel_cnt += 3;}
					    			else {HSA_accel_cnt += 2;}
					    		}
					    		else { }
					    	}
					    	else if((mtp>=tempW3) && (eng_torq_rel>=HSA_EXIT_eng_torq_s)) {
					    		if(HSA_accel_cnt<=300) 
					    		{
					    			if(eng_rpm >= 1000) {HSA_accel_cnt += 2;}
					    			else {HSA_accel_cnt++;}
					    		}
					    		else { }
					    	}
					    	else if(HSA_accel_cnt>5) {
					    		if((mtp<tempW3) || ((eng_torq_rel+40)<HSA_EXIT_eng_torq_s)) {
					    			HSA_accel_cnt -= 2;
					    		}
					    		else { }
					    	}
					    	else {
					    		HSA_accel_cnt = 0;
					    	}
					    	
					    	if(HSA_accel_cnt >= U16_HSA_e_ReleaseTim_4_Accel_1_MT) {tempB7 = 1;}
					    	else if(HSA_accel_cnt >= U16_HSA_f_ReleaseTim_4_Accel_2_MT) {HSA_RAPID_PHASE_OUT = 1;}
					    	else if((HSA_accel_cnt >= U16_HSA_g_ReleaseTim_4_Accel_3_MT) && (HSA_EST_CLUTCH_POSITION==ENGAGE)) {HSA_RAPID_PHASE_OUT = 1;}
					    	else { }
					    }
					}
					
					if((BTCS_ON==1)
					||((mtp>=(int16_t)U8_HSA_FAST_EXIT_MTP_TH)&&((FL.arad>=S16_HSA_MT_FAST_EXIT_ARAD_TH)||(FR.arad>=S16_HSA_MT_FAST_EXIT_ARAD_TH)
					||(RL.arad>=S16_HSA_MT_FAST_EXIT_ARAD_TH)||(RR.arad>=S16_HSA_MT_FAST_EXIT_ARAD_TH)))
					)
					{
						tempB7 = 1;
					    lcu1HsaExitBySpinMT = 1;
					    lcs16HsaExitBySpinPress = HSA_retaining_press;
					}
					else { }
				}
				else 
				{
					if((mtp>=50) && (eng_torq_rel>=HSA_EXIT_eng_torq2)) {tempB7 = 1;}
					else if(mtp>=10) {
						if(eng_torq_rel>=HSA_EXIT_eng_torq) {tempB7 = 1;}
						else if((eng_torq_rel>=HSA_EXIT_eng_torq2) && (HSA_EST_CLUTCH_POSITION==ENGAGE)) {tempB7 = 1;}
						else { }
					}
					else { }
				}
			}
		}
		else {
		  #if __TCMF_CONTROL	
			if(HSA_ENTER_press >= (HSA_retaining_press + MPRESS_5BAR)) {
				tempC1 = (uint8_t)(((HSA_retaining_press/2) + 140)/10);
			}
			else {
				tempC1 = (uint8_t)(((HSA_ENTER_press/2) + 140)/10);
			}
		  #else
		  	tempC1 = 14;
		  #endif
		  
			if((lcs16HsaMpress>=MPRESS_10BAR) && (HSA_rebraking_counter>tempC1)) {
				tempB7 = 1;
				HSA_flag = 6;
			}
			else { }
			
			if(HSA_RAPID_REBRAKING_flag==1) {
				tempB7 = 1;
				HSA_flag = 7;
			}
			else { }
			
			if(HSA_Control==HSA_RELEASE)
			{
				if(lcs16HsaMpress>=HSA_ENTER_press)
				{
					tempB7 = 1;
				}
				else
				{
					HSA_rebraking_counter = 1;
				}
			}
			else { }
		}
		
		if(tempB7==1) {
			HSA_EXIT_CONDITION_SATISFY = 1;
			HSA_pressure_release_counter = L_U16_TIME_10MSLOOP_20S;
		}
		else if((HSA_RAPID_PHASE_OUT==1) || (HSA_rebraking_counter>0) || ((STOP_ON_HILL_flag==0) 
          #if __ISG
        &&(HSA_ISG_flg==0)
          #endif
        ))
		{
			HSA_EXIT_CONDITION_SATISFY = 1;
			/*HSA_PRESSURE_RELEASE_RATE_SET();*/
		}
		else {HSA_EXIT_CONDITION_SATISFY = 0;}
	}
}

static void	HSA_CHK_MT_EXIT_CONDITION(void)
{
	if(HSA_flg==1)
	{
		eng_torq_rel_filt_HSA = LCESP_s16Lpf1Int(eng_torq_rel, eng_torq_rel_filt_HSA, L_U8FILTER_GAIN_10MSLOOP_10HZ);
        if((mtp>=((int16_t)U8_HSA_EST_CLUT_MTP_TH))&&(eng_torq_rel>=S16_HSA_EST_CLUT_REL_TORQ_TH)&&(eng_rpm>=S16_HSA_EST_CLUT_RPM_TH))
        {
        	
        	if(HSA_eng_rpm_dot_LPF7!=0)
        	{
        	    MT_FRICTION_CONST = ((eng_torq_rel_filt_HSA*10)/HSA_eng_rpm_dot_LPF7);
        	}
        	else
        	{
        		;
        	}
        	
        	if(MT_FRICTION_CONST>=S16_HSA_EST_CLUT_ENG_TH_2)
        	{
        		if(MT_ENG_TORQ_INDEX<200)
        		{
        			MT_ENG_TORQ_INDEX = MT_ENG_TORQ_INDEX + 3;
        		}
        		else { }
        	}	
        	else if(MT_FRICTION_CONST>=S16_HSA_EST_CLUT_ENG_TH_1)
        	{
        		if(MT_ENG_TORQ_INDEX<200)
        		{
        			MT_ENG_TORQ_INDEX = MT_ENG_TORQ_INDEX + 1;
        		}
        		else { }
        	}
        	else if(MT_FRICTION_CONST<0)
        	{
     		    if(MT_ENG_TORQ_INDEX<200)
        		{
        			MT_ENG_TORQ_INDEX = MT_ENG_TORQ_INDEX + 5;
        		}
        		else { } 
        	}
        	else { } 
        	
        	if(MT_ENG_TORQ_INDEX>=((int16_t)U8_HSA_EST_CLUT_ENG_INDEX))
        	{
        		HSA_ENOUGH_ENG_TORQ = 1;
        	}
        	else { }
        }
        else
        {
        	MT_FRICTION_CONST = 0;
        	HSA_ENOUGH_ENG_TORQ = 0;
        	MT_ENG_TORQ_INDEX = 0;
        }
        
        #if __MONITOR_TORQ_CLUTCH_ENABLE==ENABLE
          Torq_Clutch = eng_torq_rel_filt_HSA - (FRIC_CONST_CLUTCH*HSA_eng_rpm_dot_LPF7);
        #endif
    }
    else
    {
    	eng_torq_rel_filt_HSA = 0;
    	MT_FRICTION_CONST = 0;
    	MT_ENG_TORQ_INDEX = 0;
    	HSA_ENOUGH_ENG_TORQ = 0;
    	#if __MONITOR_TORQ_CLUTCH_ENABLE==ENABLE
    	Torq_Clutch = 0;
    	#endif
    }
    
    if((HSA_TM_TYPE == HSA_MANUAL_TM)&&(HSA_flg == 1)&&(lcu1HsaMpressOn == 1)&&(CLUTCH_POSITION == ENGAGE))
    {
    	lcu1HsaClutchSwOffwithBrk = 1;
    }
    else
    {
    	lcu1HsaClutchSwOffwithBrk = 0;
    }
}

static void	HSA_ENTER_CONDITION(void)
{
	lchsas16MpressSlopThres = LCESP_s16IInter2Point(lcs16HsaMpress, MPRESS_10BAR, -MPRESS_0G5BAR, MPRESS_20BAR, -MPRESS_1BAR);

	if((vref<=S16_HSA_ENTER_LIMIT_SPEED)
    &&(lcs16HsaMpress>=lchsas16EntLimitMp)
	&&(((lcs16HsaMpress<=(HSA_ENTER_press+lcs16HsaValveActMp))&&((MCpress[0]-MCpress[2])<lcs16HsaMpressSlopThres))
	||(lcs16HsaMpress<=(HSA_ENTER_press+MPRESS_5BAR)))
	)
    {
        if((HSA_TM_TYPE==HSA_MANUAL_TM) && (CLUTCH_POSITION_ALLOWED==1)) {
			if(CLUTCH_POSITION == DISENGAGE) {HSA_ENTER_CONDITION_SATISFY=1;}
			else { }
		}
		else {HSA_ENTER_CONDITION_SATISFY=1;}
	}
	else { }

    #if __ISG 
	if((STOP_ON_HILL_flag==0)&&(ISG_READY==1)&&(lcs16HsaMpress<lchsas16EntInhibitMp)) 
	{
		  HSA_ENTER_CONDITION_SATISFY=0;
	}
  else { }
    #endif
	
	if((ABS_fz==1) || (BTC_fz==1)) {HSA_ENTER_CONDITION_SATISFY = 0;}
	else { }
	
  #if (__AVH==1)
    if(lcu1AvhSwitchOn==1)
    {
        HSA_ENTER_CONDITION_SATISFY = 0;
    }
    else 
    {
        if(lcu8AvhEpbState==EPB_CLAMPED) {HSA_ENTER_CONDITION_SATISFY = 0;}
        else { }
    }

    if(lcu1AvhAutonomousActFlg==1) {HSA_ENTER_CONDITION_SATISFY = 0;}
    else { }
  #endif  

  #if __EPB_INTERFACE
	if(HSA_EPB_ACTIVE_FLG==1) {HSA_ENTER_CONDITION_SATISFY = 0;}
	else { }
  #endif
  	
}


static void	HSA_APPLY_CONDITION(void)
{
	if(SMART_ACTIVE_WHEEL_SPEED_SENSOR_ALLOWED==1) {
		if(AX_SENSOR_ALLOWED==1) {
			if((STOP_ON_UPHILL_flag==1) && (SAWSS_BACKWARD_DETECT==1)) {
				HSA_APPLY_CONDITION_SATISFY = 1;
			}
			else if((STOP_ON_DOWNHILL_flag==1) && (SAWSS_FORWARD_DETECT==1)) {
				HSA_APPLY_CONDITION_SATISFY = 1;
			}
			else {
				HSA_APPLY_CONDITION_SATISFY = 0;
			}
		}
		else {
			HSA_APPLY_CONDITION_SATISFY = 0;
		}
	}
	else 
	{
	    if(U8_HSA_RISE_ENABLE==1)
	    {
		    if((HSA_AX_STATIONARY==1)&&(HSA_hold_counter>=L_U8_TIME_10MSLOOP_200MS) /* except movig by pitching motion */
		    &&(lcu1HsaMpressOn==0)&&(mtp<1)											/* except brk and acc. */
		    &&(lcu1HsaRiseComplete==0)												/* only 1 rise */	
		    )
		    {
		    	tempW9 = 0;
		    	if(vrad_fl>=S16_HSA_RISE_WL_SPD_TH) {tempW9++;} 
		    	if(vrad_fr>=S16_HSA_RISE_WL_SPD_TH) {tempW9++;} 
		    	if(vrad_rl>=S16_HSA_RISE_WL_SPD_TH) {tempW9++;} 
		    	if(vrad_rr>=S16_HSA_RISE_WL_SPD_TH) {tempW9++;}
		    
	        	if(tempW9>=U8_HSA_RISE_NUM_OF_WL)
	        	{
	        		HSA_APPLY_CONDITION_SATISFY = 1;
	        	}
	        	else { }
	        }
	        else
	        {
	        	HSA_APPLY_CONDITION_SATISFY = 0;
	        }
	        
	        if(HSA_APPLY_CONDITION_SATISFY==1)
	        {
	        	if(HSA_retaining_press>HSA_ENTER_press)
	        	{
	        		if(lcHsau8OverloadDctCnt<U8_HSA_DCT_OVERLOAD_RISE_NUM)
	        		{
	        			lcHsau8OverloadDctCnt++;
	        		}
	        		else
	        		{
	        			lcHsau8OverloadDctCnt = U8_HSA_DCT_OVERLOAD_RISE_NUM;
	        		}
	        		
	        		if(lcHsau8OverloadDctCnt>=U8_HSA_DCT_OVERLOAD_RISE_NUM)
	        		{
	        			lcu1HsaOverloadRoll = 1;
	        		}
	        		else { }
	        	}
	        	else { }
	        }
	        else { }
	    }
	    else
	    {
		    HSA_APPLY_CONDITION_SATISFY = 0;
		    lcu1HsaOverloadRoll = 0;
	    }
	}
}

static void	COMPLETE_STOP(void)
{
	if(vref<=VREF_0_125_KPH) {
		if(HSA_complete_stop_counter<=L_U8_TIME_10MSLOOP_30MS) {
			HSA_complete_stop_counter++;
		}
		else {
			COMPLETE_STOP_flag = 1;
			HSA_complete_stop_counter = 0;
		}
	}
	else {
		HSA_complete_stop_counter = 0;
	}
}

static void	HSA_REBRAKING_DETECT(void)
{
	if(HSA_flg==1)
	{
		if(lcs16HsaMpress>=(HSA_ENTER_press+MPRESS_7BAR))
		{
			if(lcu8HsaReBrkCnt<T_1500_MS) 
			{
				lcu8HsaReBrkCnt = lcu8HsaReBrkCnt + 1;
			}
			else 
			{
				lcu8HsaReBrkCnt = T_1500_MS;
			}
		}
		else
		{
			if(lcu8HsaReBrkCnt>1)      
			{
				lcu8HsaReBrkCnt = lcu8HsaReBrkCnt - 2;
			}
			else 
			{
				lcu8HsaReBrkCnt = 0;
			}
		}
		
		if(lcu1HsaReBrk==0)
		{
		    lcu1HsaReBrkRelease=0;
			if(lcu8HsaReBrkCnt>T_100_MS) 
			{
				lcu1HsaReBrk = 1;
				lcs16HsaReBrkReleaseCnt = 0;
			}
		    else 
		    {
		    	if(lcs16HsaReBrkReleaseCnt<T_1500_MS) 
		    	{
		    		lcs16HsaReBrkReleaseCnt++ ; 
		    	}
		    	else { }
		    }
		}
		else /*lcu1HsaReBrk==1*/
		{
			if(((lcs16HsaMpress<=(HSA_ENTER_press+lcs16HsaValveActMp))&&((MCpress[0]-MCpress[2])<lcs16HsaMpressSlopThres))
			  ||(lcs16HsaMpress<=(HSA_ENTER_press+MPRESS_5BAR))
			)
		    {
		    	lcu1HsaReBrkRelease=1;
		    	lcu1HsaReBrk=0;
		    	lcu8HsaReBrkCnt=0;
		    	lcs16HsaReBrkReleaseCnt++ ;
		    }
		    else { }
		}	
		
		if((lcs16HsaMpress>=lchsas16EntLimitMp)&&(HSA_hold_counter>0))
		{
			HSA_MAINTAIN_BRK_APPLY_flag = 1;
		}
		else
		{
			HSA_MAINTAIN_BRK_APPLY_flag = 0;
		}	
	}
	else
	{
		lcu1HsaReBrk = 0;
		lcu8HsaReBrkCnt = 0;
		lcu1HsaReBrkRelease = 0;
		lcs16HsaReBrkReleaseCnt = T_1500_MS;
		HSA_MAINTAIN_BRK_APPLY_flag = 0;
	}
}

static void	HSA_PRESSURE_RELEASE(void)
{
  #if	!__TCMF_CONTROL
	if(AX_SENSOR_ALLOWED==1) {
/*		tempW7 = abs(along); */
		tempW7 = HSA_ax_sen_filt_abs;
		if(tempW7 >= AFZ_0G3) {
			tempW6 = 8;
		}
		else if(tempW7 >= AFZ_0G2) {
			tempW6 = 6;
		}
		else if(tempW7 >= AFZ_0G1) {
			tempW6 = 5;
		}
		else {
			tempW6 = 4;
		}
	}
	else {
		tempW6 = 4;
	}
	
	if(HSA_pressure_release_counter%tempW6==0) {
		HSA_TCL_DEMAND_fl=0;
		HSA_TCL_DEMAND_fr=0;

		tempW0 = HSA_retaining_press;
		if(tempW0 <= S16_MP_Rise_Gain_Press_f_2nd) {
			tempW1 = S16_MP_Rise_Gain_f_2nd;
		}
		else if(tempW0 <= S16_MP_Rise_Gain_Press_f_3rd) {
			tempW1 = S16_MP_Rise_Gain_f_3rd;	
		}
		else {
			tempW1 = S16_MP_Rise_Gain_f_4th;
		}
		
		tempW2 = tempW1 * (int16_t)(LDABS_u16FitSQRT((uint16_t)tempW0));
		HSA_retaining_press = tempW0 - (tempW2/2);
	}
	else {
		HSA_TCL_DEMAND_fl=1;
		HSA_TCL_DEMAND_fr=1;
	}
	
	HSA_pressure_release_counter++;
	if(HSA_retaining_press <= 50) {HSA_pressure_release_counter = 502;}
	else { }
  #else
    HSA_pressure_release_counter++;
    HSA_TCL_DEMAND_fl = 1;
    HSA_TCL_DEMAND_fr = 1;
  #endif 

}


static void	HSA_APPLYING(void)
{
	HSA_TCL_DEMAND_fl=1;
	HSA_TCL_DEMAND_fr=1;
	
	if(HSA_apply_count<=S16_HSA_MOTOR_ON_TIME) 
	{
		HSA_apply_count++;
		HSA_MSC_MOTOR_ON=1;
		HSA_S_VALVE_LEFT=1;
        HSA_S_VALVE_RISHT=1;
	}
	else 
	{
		lcu1HsaRiseComplete = 1;
		HSA_apply_count=0;
		HSA_MSC_MOTOR_ON=0;
		HSA_S_VALVE_LEFT=0;
        HSA_S_VALVE_RISHT=0;
	}

	if(HSA_hold_counter>=L_U8_TIME_10MSLOOP_300MS) 
	{
		HSA_hold_counter = L_U8_TIME_10MSLOOP_300MS;	
    }
    else { }
}      

static void	HSA_HOLDING(void)
{
	HSA_TCL_DEMAND_fl=1; 
	HSA_TCL_DEMAND_fr=1;
	
	if(lcs16HsaInitHoldCntrCnt<L_U16_TIME_10MSLOOP_10S) 
	{
		lcs16HsaInitHoldCntrCnt++;
	}
	else
	{
		lcs16HsaInitHoldCntrCnt = L_U16_TIME_10MSLOOP_10S;
	}

	  #if __ISG
	if((STOP_ON_HILL_flag==0)&&(HSA_ISG_flg==1)&&(HSA_hold_counter<=HSA_hold_time_max))
	{
        if(HSA_MAINTAIN_BRK_APPLY_flag==1)
        {
            HSA_hold_counter=L_U8_TIME_10MSLOOP_150MS;
        }
        else if(lcu1HsaMpressOn==0)
        {
            HSA_hold_counter++;
        }
        else { }

		if((eng_rpm>=S16_ISG_End_RPM_Th)&&(HSA_hold_cnt_rpm<=U8_ISG_End_RPM_OK_delay_time))
	    {
	        HSA_hold_cnt_rpm++;
	    }
	    else { }
	}
	else
	  #endif
    if(HSA_hold_counter<=HSA_hold_time_max) 
    {
        if(HSA_MAINTAIN_BRK_APPLY_flag==1)
        {
            HSA_hold_counter=L_U8_TIME_10MSLOOP_150MS;
        }
        else if(lcu1HsaMpressOn==0)
        {
            HSA_hold_counter++;
        }
        else { }
    }
    else { }
} 

void	HSA_reset(void)
{
	HSA_Control = 0;
	HSA_apply_count = 0;
	HSA_hold_counter = 0;
	HSA_complete_stop_counter = 0;
	HSA_rebraking_counter = 0;
	HSA_rebraking_start = 0;
	HSA_flg = 0;
	HSA_ISG_flg = 0;
	HSA_EXIT_CONDITION_SATISFY = 0;
	HSA_ENTER_CONDITION_SATISFY = 0;
	HSA_APPLY_CONDITION_SATISFY = 0;
	HSA_RAPID_REBRAKING_flag = 0;
	COMPLETE_STOP_flag = 0;
	HSA_EXIT_eng_torq = 0;
	HSA_ENTER_press = 0;
	HSA_retaining_press = 0;
	HSA_pressure_release_counter=0;
	HSA_TCL_DEMAND_fl = 0;
	HSA_TCL_DEMAND_fr = 0;
	HSA_S_VALVE_LEFT = 0;
	HSA_S_VALVE_RISHT = 0;
	HSA_MSC_MOTOR_ON = 0;
	HSA_accel_cnt = 0;
	HSA_accel_cnt2 = 0;
	HSA_phase_out_time = 0;
	HSA_phase_out_rate = 0;
	HSA_hold_cnt_rpm = 0;
	lcu1HsaRiseComplete = 0;
  HSA_MAINTAIN_BRK_APPLY_flag = 0;
  HSA_pressure_release_time = 0;
  lcs16HsaInitHoldCntrCnt = 0;
  HSA_RAPID_PHASE_OUT=0;
}

static void	HSA_HARDWARE_SETTING(void)
{
 #if (__4WD_VARIANT_CODE==ENABLE)
 	if((lsu8DrvMode==DM_AUTO) || (lsu8DrvMode==DM_4H) || (lsu8DrvMode==DM_2H))
 	{
 		AX_SENSOR_ALLOWED = 1;
 	}
 	else if(lsu8DrvMode==DM_2WD)
 	{
	  #if (__AX_SENSOR || __EPB_INTERFACE)
		AX_SENSOR_ALLOWED = 1;
	  #else
	  	AX_SENSOR_ALLOWED = 0;
	  #endif
	}
	else
	{
		AX_SENSOR_ALLOWED = 0;
 	} 		
 #else
  #if __4WD || __AX_SENSOR || __EPB_INTERFACE
	AX_SENSOR_ALLOWED = 1;
  #else
  	AX_SENSOR_ALLOWED = 0;
  #endif
 #endif
    
  	if(HSA_TM_TYPE==HSA_AUTO_TM) {
  		CLUTCH_POSITION_ALLOWED = 0;
  		CLUTCH_POSITION = 0;
  		HSA_EST_CLUTCH_POSITION = 0;
  		REVERSE_GEAR_INFORM_ALLOWED = 0;
  		GEAR_POSITION_SENSOR_ALLOWED = 1;
  	}
  	else {
  	  #if (__CAR == HMC_JM)
  		CLUTCH_POSITION_ALLOWED = 0;
  		REVERSE_GEAR_INFORM_ALLOWED = 0;
  		GEAR_POSITION_SENSOR_ALLOWED = 0;
  	  #elif (__CAR == FORD_C214)
 		CLUTCH_POSITION_ALLOWED = 1;
  		REVERSE_GEAR_INFORM_ALLOWED = 1;
  		GEAR_POSITION_SENSOR_ALLOWED = 0;
  	  #elif (__CAR == SYC_KYRON)
  		CLUTCH_POSITION_ALLOWED = 0;
  		REVERSE_GEAR_INFORM_ALLOWED = 1;
  		GEAR_POSITION_SENSOR_ALLOWED = 0;
  	  #elif (__CAR == GM_C100)
  		CLUTCH_POSITION_ALLOWED = 1;
  		REVERSE_GEAR_INFORM_ALLOWED = 1;
  		GEAR_POSITION_SENSOR_ALLOWED = 0;
      #elif((__GEAR_SWITCH_TYPE!=NONE)&&(__CLUTCH_SWITCH_TYPE!=NONE))
           /* must be enabled clutch and R gear, must be defined R gear&clutch signal Type */
  		CLUTCH_POSITION_ALLOWED = 1;
  		REVERSE_GEAR_INFORM_ALLOWED = 1;
  		GEAR_POSITION_SENSOR_ALLOWED = 0;
  		  #if  __MT_NEUTRAL_SWITCH_ENABLE==ENABLE 
  		NEUTRAL_SWITCH_ALLOWED = 1;
  		  #else
        NEUTRAL_SWITCH_ALLOWED = 0;
  		  #endif   
  	  #else
  		CLUTCH_POSITION_ALLOWED = 0;
  		REVERSE_GEAR_INFORM_ALLOWED = 0;
  		GEAR_POSITION_SENSOR_ALLOWED = 0;
  	  #endif
  	  	HSA_CLUTCH_POSITION_EST();
  	  	
  	}
  	
  	SMART_ACTIVE_WHEEL_SPEED_SENSOR_ALLOWED = 0;  		
}

static void	HSA_CLUTCH_POSITION_EST(void)
{
	if(CLUTCH_POSITION_ALLOWED==1) {
	  #if (__CAR == FORD_C214)	
	    if(b_ClutchPedalSwitch_b==1) {
			CLUTCH_POSITION = DISENGAGE;
		}
		else {
			CLUTCH_POSITION = ENGAGE;
		}
	  #elif (__CAR == GM_C100)	
	    if(lespu1_TopTrvlCltchSwAct==1) {
			CLUTCH_POSITION = DISENGAGE;
		}
		else {
			CLUTCH_POSITION = ENGAGE;
		}
	  #else
	    if(fu1ClutchSwitch==1) 
	    {
			CLUTCH_POSITION = DISENGAGE;
		}
		else 
		{
			CLUTCH_POSITION = ENGAGE;
		}	  	
	  #endif	
	}
	else {
		CLUTCH_POSITION = DISENGAGE; 
	}
	
	
	HSA_mtp_cnt++;
	if(HSA_mtp_cnt==1) {
		
/*		HSA_eng_rpm_LPF7_old = HSA_eng_rpm_LPF7; */
/*		HSA_eng_rpm_LPF7 = LCESP_s16Lpf1Int(eng_rpm, HSA_eng_rpm_LPF7_old, 39); */
		
/*		tempW1 = HSA_eng_rpm_LPF7-HSA_eng_rpm_LPF7_old; */
		tempW1 = (int16_t)eng_rpm-HSA_eng_rpm_LPF7_old;
		HSA_eng_rpm_dot_LPF7_old = HSA_eng_rpm_dot_LPF7;
		
	  #if (__MGH60_CYCLE_TIME_OPT_HSA == ENABLE)
		if(vref>=(S16_HSA_CONTROLLER_ENABLE_SPEED-VREF_2_KPH))
		{
			HSA_eng_rpm_dot_LPF7 = tempW1;
		}
		else
	  #endif	
		{
		HSA_eng_rpm_dot_LPF7 = LCESP_s16Lpf1Int(tempW1, HSA_eng_rpm_dot_LPF7_old, L_U8FILTER_GAIN_10MSLOOP_10HZ);
		}

		HSA_eng_rpm_LPF7_old = (int16_t)eng_rpm;
		
		HSA_Est_eng_rpm_dot_old = HSA_Est_eng_rpm_dot;
		HSA_Est_eng_rpm_dot = eng_torq_rel/10;
		
		HSA_mtp_old3 = HSA_mtp_old2;
		HSA_mtp_old2 = HSA_mtp_old1;
		HSA_mtp_old1 = (uint8_t)mtp;
		HSA_mtp_cnt = 0;
	}
	else { }
	
	if((lcs16HsaMpress >= MPRESS_10BAR) && (vref<= VREF_1_KPH)) 
	{
		HSA_EST_CLUTCH_POSITION = DISENGAGE;
	}
	else { }
	
	if(vref <= VREF_1_KPH) {
		if(mtp >= 3) {
/*			if(HSA_eng_rpm_dot_LPF7 <= -3) { 
				HSA_clutch_engage_cnt += 1;
			}
			else if(HSA_clutch_engage_cnt > 3) {
				HSA_clutch_engage_cnt -= 3;
			}
			else {
				HSA_clutch_engage_cnt = 0;
			}
*/
		  if((mtp>=HSA_mtp_old2) && (HSA_mtp_old2>=HSA_mtp_old3)) {
			if(((HSA_Est_eng_rpm_dot-HSA_eng_rpm_dot_LPF7) >= ENG_RPM_DOT_DIFF) && (HSA_Est_eng_rpm_dot>=EST_ENG_RPM_DOT_UPPER_LIMIT) && (HSA_eng_rpm_dot_LPF7<=REAL_ENG_RPM_DOT_UPPER_LIMIT)&&(eng_rpm>1500))
			{
				if(HSA_Est_eng_rpm_dot>=(HSA_Est_eng_rpm_dot_old - 1))
				{
					if(HSA_eng_rpm_dot_LPF7 <= (HSA_eng_rpm_dot_LPF7_old + 1))
					{
						if(HSA_clutch_engage_cnt < 100) {
							HSA_clutch_engage_cnt++;
						}
						else { }
					}
					else if(HSA_eng_rpm_dot_LPF7 > (HSA_eng_rpm_dot_LPF7_old + 2))
					{
						if(HSA_clutch_engage_cnt>3) 
						{
							HSA_clutch_engage_cnt = HSA_clutch_engage_cnt - 3;
						}
						else 
						{
							HSA_clutch_engage_cnt = 0;
						}
					}
					else { }
				}
				else if(HSA_Est_eng_rpm_dot>=(HSA_Est_eng_rpm_dot_old - 3))
				{
					if(HSA_eng_rpm_dot_LPF7 <= (HSA_eng_rpm_dot_LPF7_old + 1))
					{
						;
					}
					else 
					{
						if(HSA_clutch_engage_cnt>3) 
						{
							HSA_clutch_engage_cnt = HSA_clutch_engage_cnt - 3;
						}
						else 
						{
							HSA_clutch_engage_cnt = 0;
						}
					}
				}
				else {
					if(HSA_clutch_engage_cnt>3) 
					{
						HSA_clutch_engage_cnt = HSA_clutch_engage_cnt - 3;
					}
					else 
					{
						HSA_clutch_engage_cnt = 0;
					}
				}
			}
			else 
			{
				if(HSA_clutch_engage_cnt>3) 
				{
					HSA_clutch_engage_cnt = HSA_clutch_engage_cnt - 3;
				}
				else 
				{
					HSA_clutch_engage_cnt = 0;
				}
			}
		  }
		}
		else 
		{
			HSA_clutch_engage_cnt = 0;
		}
		
		if(HSA_EST_CLUTCH_POSITION == DISENGAGE)
		{
			if(HSA_clutch_engage_cnt >= 8) 
			{
				HSA_EST_CLUTCH_POSITION = ENGAGE;
			}
			else { }
		}
		else if(HSA_EST_CLUTCH_POSITION == ENGAGE)
		{
			if((HSA_clutch_engage_cnt < 1)&&(mtp < 3))
			{
				HSA_EST_CLUTCH_POSITION = DISENGAGE;
			}
			else { }
		}
		else { }
	}
	else {
		/*HSA_EST_CLUTCH_POSITION = ENGAGE;*/
		if(vref >= VREF_2_KPH)
		{
			HSA_clutch_engage_cnt = 0;
		}
		else { }
	}
}

static void	HSA_PRESSURE_RELEASE_RATE_SET(void)
{
	static int16_t HSA_ax_sen_filt_at_Release;

    if(HSA_flg==1)
	{
	    if(HSA_Control!=HSA_RELEASE)
	    {
	    	HSA_ax_sen_filt_at_Release = HSA_ax_sen_filt_abs; 
	    }
	    else { }
	    
        if((STOP_ON_HILL_flag==0)
          #if __ISG
        &&(HSA_ISG_flg==0)
          #endif
        ) 
        {
        	HSA_phase_out_rate = (int16_t)U8_HSA_PHASE_OUT_RATE_NOT_HILL;
        }
        else if(HSA_rebraking_counter>0) 
        {
        	HSA_phase_out_rate = (int16_t)U8_HSA_PHASE_OUT_RATE_REBRK;
        }
        else if(HSA_RAPID_PHASE_OUT==1)
        {
	        if(AX_SENSOR_ALLOWED==1) 
	        {
	    		tempW5 = HSA_ax_sen_filt_at_Release;
	    		
	    		if(tempW5 > S16_HSA_c_MaxLongG) {tempW5 = S16_HSA_c_MaxLongG;}
	    		
	    		if(U8_HSA_SET_DECAY_RATE_EACH_SLOP>0)
	    		{
	    		    HSA_phase_out_rate = LCESP_s16IInter4Point(tempW5, S16_HSA_a_uphill_detection_along, (int16_t)U8_HSA_FO_Rate_ACC_MinG,
                                                                       (int16_t)U8_HSA_LowMedLongG,      (int16_t)U8_HSA_FO_Rate_ACC_LowMedG,
                                                                       (int16_t)U8_HSA_HighMedLongG,     (int16_t)U8_HSA_FO_Rate_ACC_HighMedG,
                                                                       S16_HSA_c_MaxLongG,               (int16_t)U8_HSA_FO_Rate_ACC_MaxG);
                }
	    		else
	    		{
	    			HSA_phase_out_rate = S16_HSA_PHASE_OUT_RATE_MAX_ACC - (int16_t)(tempW5/U8_HSA_PHASE_OUT_RATE_G_GAIN);
	    		}
	    		
	    		  #if __ISG
	    		if(HSA_ISG_flg==1) {HSA_phase_out_rate = (HSA_phase_out_rate*((int16_t)S8_RATE_GAIN_FOR_ISG_ACC))/10;}
	    		else { }
	    		  #endif
	    		if(HSA_phase_out_rate < S16_HSA_PHASE_OUT_RATE_MIN) {HSA_phase_out_rate = S16_HSA_PHASE_OUT_RATE_MIN;}
	    	}
	    	else 
	    	{
	    		HSA_phase_out_rate = MPRESS_0G8BAR;
	    	}    	
        }
        else 
        { 
	        if(AX_SENSOR_ALLOWED==1) 
	        {
	    		tempW5 = HSA_ax_sen_filt_at_Release;
	    		
	    		if(tempW5 > S16_HSA_c_MaxLongG) {tempW5 = S16_HSA_c_MaxLongG;}	
	    		
	    		if((U8_HSA_SET_CLUTCH_ENGAGE_EST>0)&&(VEHICLE_MOVE_WO_EXIT_OK==1))
	    		{
	    	        HSA_phase_out_rate = (int16_t)U8_HSA_PHASE_OUT_RATE_FOR_ROLL;
	    	        if(HSA_phase_out_rate<1) {HSA_phase_out_rate = 1;}
	    	        else { }
	    	    }
	    	    else
	    	    {
                    if(U8_HSA_SET_DECAY_RATE_EACH_SLOP>0)
                    {
	    	    	    HSA_phase_out_rate = LCESP_s16IInter4Point(tempW5, S16_HSA_a_uphill_detection_along, (int16_t)U8_HSA_FO_Rate_MinG,
                                                                           (int16_t)U8_HSA_LowMedLongG,      (int16_t)U8_HSA_FO_Rate_LowMedG,
                                                                           (int16_t)U8_HSA_HighMedLongG,     (int16_t)U8_HSA_FO_Rate_HighMedG,
                                                                           S16_HSA_c_MaxLongG,               (int16_t)U8_HSA_FO_Rate_MaxG);
                    }
                    else
                    {
	    		        HSA_phase_out_rate = S16_HSA_PHASE_OUT_RATE_MAX - (int16_t)(tempW5/U8_HSA_PHASE_OUT_RATE_G_GAIN);
	    		    }
        
        			  #if defined(__dDCT_INTERFACE)
					if((MFC_Current_HSA_S<=TC_I_300mA) && (MFC_Current_HSA_P<=TC_I_300mA))
					{
						HSA_phase_out_rate = LCESP_s16IInter4Point(tempW5, S16_HSA_a_uphill_detection_along,     (int16_t)U8_HSA_FO_R_Min_Under10bar,
                        	                                               (int16_t)U8_HSA_LowMedLongG,          (int16_t)U8_HSA_FO_R_Lmed_Under10bar,
                            	                                           (int16_t)U8_HSA_HighMedLongG,         (int16_t)U8_HSA_FO_R_Hmed_Under10bar,
                                	                                       S16_HSA_c_MaxLongG,                   (int16_t)U8_HSA_FO_R_Max_Under10bar);
					}
					else { }
				  	  #endif
        				
	    		      #if __ISG
	    		    if(HSA_ISG_flg==1) {HSA_phase_out_rate = (HSA_phase_out_rate*((int16_t)S8_RATE_GAIN_FOR_ISG))/10;}
	    		    else { }
	    		      #endif
	    		    if(HSA_phase_out_rate < S16_HSA_PHASE_OUT_RATE_MIN) {HSA_phase_out_rate = S16_HSA_PHASE_OUT_RATE_MIN;}
	    		}
	    	}
	    	else 
	    	{
	    		HSA_phase_out_rate = MPRESS_0G5BAR;
	    	}    	
	    }
	    
	    if(HSA_ENTER_press>=(HSA_retaining_press+MPRESS_5BAR))
	    {
	    	HSA_phase_out_time = HSA_retaining_press/HSA_phase_out_rate;
	    }
	    else
	    {
	    	HSA_phase_out_time = HSA_ENTER_press/HSA_phase_out_rate;
	    }
	}
	else
	{
		HSA_phase_out_rate = 0;
		HSA_phase_out_time = 0;
	}
}


  #if __EPB_INTERFACE
static void	EPB2HSA_INTERFACE(void)
{
	#if __CAR==FORD_C214
	if(b_EPB_Active_b==1)
	#else
	if(lcu8EpbiEpbState==EPBI_EPB_CLAMPED)
	/*if(0)*/ /* (ccu8_EPB510_Active==1) */
	#endif
	{
		HSA_EPB_ACTIVE_FLG = 1;
	}
	else {
		HSA_EPB_ACTIVE_FLG = 0;
	}
}

static void	HSA2EPB_INTERFACE(void)
{
	uint8_t TempMSG;
	TempMSG=0;
	if(HSA_INHIBITION_flag==1) 
	{
		TempMSG=3;/* Not available */
	}
	else if((HSA_Control==HSA_READY) || (HSA_Control==HSA_NORMAL)) 
	{
		TempMSG=0;/* None */
	}
	else if((HSA_Control==HSA_HOLD) || (HSA_Control==HSA_APPLY)) 
	{
		TempMSG=1;/* Active */
	}
	else if(HSA_Control == HSA_RELEASE)
	{
		if(HSA_pressure_release_counter > 0) 
		{
			TempMSG=5;/* Time-out Release */
		}
		else 
		{
			TempMSG=2;/* Release */
		}
	}
	else 
	{
		TempMSG=0;
	}
	
	#if __CAR==FORD_C214
	b_HLAMsgReq_b=TempMSG;
	#else
	#endif

}
  #endif /* __EPB_INTERFACE */

static void	HSA_TM_SETTING(void)
{
	if((lespu1EMS_AT_TCU==1)||(lespu1EMS_CVT_TCU==1)||(lespu1EMS_DCT_TCU==1)||(lespu1EMS_HEV_AT_TCU==1))
	{
		HSA_TM_TYPE=HSA_AUTO_TM;
	}
	else
	{
		HSA_TM_TYPE=HSA_MANUAL_TM;
	}

	/* EV car */
	#if((__HMC_CAN_VER==CAN_EV)||(__HMC_CAN_VER==CAN_FCEV))
      HSA_TM_TYPE=HSA_AUTO_TM;
	#endif
	
		/* For 10SWD Demo */
	#if ((__CAR==GM_V275)||(__CAR==GM_CTS)||(__CAR==RSM_QM5)||(__CAR==BMW740i))||(__CAR==GM_SILVERADO) || (__CAR==GM_LAMBDA)
	  HSA_TM_TYPE = HSA_AUTO_TM;
	#elif (__CAR==FORD_C214)
	  HSA_TM_TYPE=HSA_MANUAL_TM;
	#endif
}

static void	HSA_FADE_OUT_ROLLBACK(void)
{
  #if defined(__dDCT_INTERFACE)
	if(HSA_flg==0)
	{
		INHIBIT_DCT_ROLL = 0;
	}
	else if((HSA_flg==1)&&(HSA_pressure_release_counter==1))
	{
		if((vrad_fl>VREF_0G125_KPH)||(vrad_fr>VREF_0G125_KPH)||(vrad_rl>VREF_0G125_KPH)||(vrad_rr>VREF_0G125_KPH))
		{
			INHIBIT_DCT_ROLL = 1;
		}
		else { }
	}
	else { }
  #endif
	
	VEHICLE_MOVE_WO_EXIT_OK_old = VEHICLE_MOVE_WO_EXIT_OK;
	if((HSA_flg==1)&&(HSA_Control==HSA_RELEASE)&&(HSA_hold_counter>4)
	&&(((HSA_TM_TYPE==HSA_MANUAL_TM)&&(HSA_EST_CLUTCH_POSITION==0)&&(HSA_ENOUGH_ENG_TORQ==0))||((HSA_TM_TYPE==HSA_AUTO_TM)&&(mtp<3)))
	)
	{   
		tempW3 = 0;
		if(vrad_fl>VREF_0G125_KPH) {tempW3++;} 
		if(vrad_fr>VREF_0G125_KPH) {tempW3++;} 
		if(vrad_rl>VREF_0G125_KPH) {tempW3++;} 
		if(vrad_rr>VREF_0G125_KPH) {tempW3++;}
		
	  #if defined(__dDCT_INTERFACE)
	    if((tempW3>=1)&&(INHIBIT_DCT_ROLL==0))
	  #else
	    if(tempW3>=1)
	  #endif
	    {
	    	VEHICLE_MOVE_WO_EXIT_OK = 1;
	    }
	    else 
	    {
	    	VEHICLE_MOVE_WO_EXIT_OK = 0;
	    }
	}
	else
	{
		VEHICLE_MOVE_WO_EXIT_OK = 0;
	}
}

static void	HSA_CALC_MCP_SLOPE(void)
{
    lcs16HsaMpressSlopThres = LCESP_s16IInter2Point(lcs16HsaMpress, MPRESS_10BAR, -MPRESS_0G2BAR, MPRESS_20BAR, -MPRESS_0G5BAR);
    
    tempW1 = -(MCpress[0]-MCpress[2]);
    lcs16HsaValveActMp = LCESP_s16IInter2Point(tempW1, S16_HSA_MIN_MP_SLOP, S16_HSA_VV_ACT_COMP_MIN_MP_SLP, 
													   S16_HSA_MAX_MP_SLOP, S16_HSA_VV_ACT_COMP_MAX_MP_SLP);    
}

static void	HSA_CALC_CONTROL_PRESS(void)
{
	if(HSA_flg==1) 
	{
		if(HSA_Control==HSA_HOLD)
		{
			if(lcs16HsaMpress>=HSA_ENTER_press)
    		{
    			HSA_retaining_press = HSA_ENTER_press;
    		}
    		else
    		{
    			if(lcs16HsaMpress>HSA_retaining_press)
    			{
	 	    		HSA_retaining_press = lcs16HsaMpress;
	 			}
	 			else
	 			{
	 				;
	 			}
 			}
		}
		else if(HSA_Control==HSA_APPLY)
		{
			if(lcu1HsaRiseComplete==1)
			{
				HSA_retaining_press = HSA_retaining_press + S16_HSA_RISE_PRESS;
			}
			else { }
		}
		else if(HSA_Control==HSA_RELEASE)
		{
			if((HSA_pressure_release_counter%(INT)U8_HSA_CNTR_P_SCAN)==0)
			{
				HSA_retaining_press = HSA_retaining_press - HSA_phase_out_rate;
			}
			else { }

			if(HSA_retaining_press<0)
			{
				HSA_retaining_press = 0;
			}
			else { }
		}
		else
		{
			HSA_retaining_press = 0;
		}
	}
	else
	{
		HSA_retaining_press = 0;
	}
	
	lchsas16CtrlTargetPress = HSA_retaining_press;
}

static void HSA_MAX_HOLD_TIME(void)
{
  #if defined(__dDCT_INTERFACE)
	if(HSA_flg==1) 
	{
		tempW5 = HSA_ax_sen_filt_abs; 
		if(HSA_Control==HSA_HOLD)
		{
			HSA_hold_time_max = LCESP_s16IInter4Point(tempW5, S16_HSA_a_uphill_detection_along, (int16_t)U16_HSA_holding_time_out,
                                                              (int16_t)U8_HSA_LowMedLongG,      (int16_t)U16_HSA_holding_time_out_LM,
                                                              (int16_t)U8_HSA_HighMedLongG,     (int16_t)U16_HSA_holding_time_out_HM,
                                                              S16_HSA_c_MaxLongG,               (int16_t)U16_HSA_holding_time_out_MAX);
			
		}
		else { }
	}
	else
	{
		HSA_hold_time_max = 0;
	}
  #else
    HSA_hold_time_max = (int16_t)U16_HSA_holding_time_out;
  #endif
}

static void HSA_TCS_INTERFACE(void)
{
	if(HSA_flg==1)
	{
		lcu1HsaExitBySpinAT = 0;
		lcu1HsaExitBySpinMT = 0;
		lcu8HsaExitBySpinCnt = 0;
		lcs16HsaExitBySpinPress = 0;
	}
	else if((lcu1HsaExitBySpinAT==1)||(lcu1HsaExitBySpinMT==1))
	{
		if((lcs16HsaExitBySpinPress>MPRESS_0BAR)&&(lcu8HsaExitBySpinCnt<U8_HSA_SPIN_EXIT_SET_TIME))
		{
			lcu8HsaExitBySpinCnt++;
			if(lcu8HsaExitBySpinCnt>1)
			{
				lcs16HsaExitBySpinPress = lcs16HsaExitBySpinPress - S16_HSA_PRESS_DEC_RATE_TC_OFF;
			}
			else { }
			
			if(lcs16HsaExitBySpinPress<MPRESS_0BAR)
			{
				lcs16HsaExitBySpinPress = MPRESS_0BAR;
			}
			else { }
		}
		else
		{
			lcu1HsaExitBySpinAT = 0;
		    lcu1HsaExitBySpinMT = 0;
		    lcu8HsaExitBySpinCnt = 0;
		    lcs16HsaExitBySpinPress = 0;
		}
	}
	else { }
}
  
#endif /* __HSA */

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCHSACallControl
	#include "Mdyn_autosar.h"
#endif
