#ifndef __LAACCCALLACTHW_H__
#define __LAACCCALLACTHW_H__

/*includes**************************************************/
#include "LVarHead.h"
/*Global MACRO CONSTANT Definition**************************/
  #if __ACC
#define U8_ACC_HOLD         1
#define U8_ACC_APPLY        2
#define U8_ACC_DUMP         3
  #endif

/*Global MACRO FUNCTION Definition**************************/

/*Global Type Declaration **********************************/
  #if __ACC
typedef struct
{
    uint16_t lau1AccWL1No :1;
    uint16_t lau1AccWL1Nc :1;
    uint16_t lau1AccWL2No :1;
    uint16_t lau1AccWL2Nc :1;
}ACC_VALVE_t;

   #if (__SPLIT_TYPE==0)
#define ACC_HV_VL_fr            laAccValveP.lau1AccWL1No
#define ACC_AV_VL_fr            laAccValveP.lau1AccWL1Nc
#define ACC_HV_VL_rl            laAccValveP.lau1AccWL2No
#define ACC_AV_VL_rl            laAccValveP.lau1AccWL2Nc

#define ACC_HV_VL_fl            laAccValveS.lau1AccWL1No
#define ACC_AV_VL_fl            laAccValveS.lau1AccWL1Nc
#define ACC_HV_VL_rr            laAccValveS.lau1AccWL2No
#define ACC_AV_VL_rr            laAccValveS.lau1AccWL2Nc
   #else
#define ACC_HV_VL_fl            laAccValveP.lau1AccWL2No
#define ACC_AV_VL_fl            laAccValveP.lau1AccWL2Nc
#define ACC_HV_VL_fr            laAccValveP.lau1AccWL1No
#define ACC_AV_VL_fr            laAccValveP.lau1AccWL1Nc

#define ACC_HV_VL_rl            laAccValveS.lau1AccWL1No
#define ACC_AV_VL_rl            laAccValveS.lau1AccWL1Nc
#define ACC_HV_VL_rr            laAccValveS.lau1AccWL2No
#define ACC_AV_VL_rr            laAccValveS.lau1AccWL2Nc
   #endif
  #endif
/*Global Extern Variable  Declaration***********************/
/********** From */
  #if __ACC
   #if __ADVANCED_MSC
/*extern int16_t        acc_msc_target_vol, acc_initial_motor_on;*/
extern U8_BIT_STRUCT_t MSC00,MSC01;
   #endif
/********** To */

 
 

extern uint8_t  lcu8AccMotorMode;
extern ACC_VALVE_t  laAccValveP, laAccValveS;
  #endif

/*Global Extern Functions  Declaration**********************/
  #if __ACC
extern void LAACC_vCallActHW(void);
   #if __ADVANCED_MSC
extern void LCMSC_vSetACCTargetVoltage(void);
   #endif
extern int16_t LAMFC_s16SetMFCCurrentACC(ACC_VALVE_t *pAcc, int16_t MFC_Current_old);   
  #endif
#endif
