
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDTSPCallDetection
	#include "Mdyn_autosar.h"
#endif


#include "LDTSPCallDetection.h"
#include "LCTSPCallControl.h"
#include "LCESPCalInterpolation.h"
#include "LCESPCalLpf.h"
#include "LCESPCallControl.h"

#if __TSP
void LDTSP_vCallDetection(void);
void LDTSP_vDetectINHIBIT(void);
void LDTSP_vDetectVehicleStatus(void);
void LDTSP_vDetectYawParameter(void);
void LDTSP_vDetectOscillation(void);
void LDTSP_vDetectOscilCycle(void);
void LDTSP_vDetectOscilDyaw(void);
void LDTSP_vDetectOscilYawc(void);
void LDTSP_vDetectMitigation(void);
#endif


#if __TSP
void LDTSP_vCallDetection(void)
{
	LDTSP_vDetectINHIBIT();
	LDTSP_vDetectVehicleStatus();
}

void LDTSP_vDetectINHIBIT(void)
{
#if  __GM_FailM      
	if( (fu1ESCEcuHWErrDet==1)||(fu1FrontWSSErrDet==1)||(fu1RearWSSErrDet==1)||(fu1ESCSensorErrDet==1)
		||(fu1MainCanLineErrDet==1)||(fu1EMSTimeOutErrDet==1)||(fu1ESCDisabledBySW==1)||(fu1VoltageLowErrDet==1)
		||(VDC_LOW_SPEED==1)||(BACK_DIR==1)||(PARTIAL_PERFORM==1)||(SAS_CHECK_OK==0)
		||(fu1OnDiag==1)||(init_end_flg==0)||(wu8IgnStat==CE_OFF_STATE)
		#if __G_SENSOR_ENABLE
    ||(fu1AxSenPwr1sOk==0)
    #endif
    ||(fu1YawSenPwr1sOk==0)||(fu1AySenPwr1sOk==0)||(fu1SteerSenPwr1sOk==0)
		#if __BRAKE_FLUID_LEVEL==ENABLE
		||(fu1DelayedBrakeFluidLevel==1)||(fu1LatchedBrakeFluidLevel==1)
		#endif
		||(cbit_process_step==1)
		||(S8_TSP_ENABLE==0)||(FLAG_BANK_DETECTED==1)||(FLAG_BANK_SUSPECT==1) )
#else
	if( (ESP_ERROR_FLG)||(!vdc_on_flg)||(VDC_DISABLE_FLG)||(tc_error_flg)||(tcs_error_flg)||(cbit_process_step==1)
		||(S8_TSP_ENABLE==0)||(FLAG_BANK_DETECTED==1)||(FLAG_BANK_SUSPECT==1) )
#endif
	{
		U1_TSP_CONTROL_MODE_INHIBIT = 1;		
	}
	else
	{
		U1_TSP_CONTROL_MODE_INHIBIT = 0;
	}
}

void LDTSP_vDetectVehicleStatus(void)
{
	LDTSP_vDetectYawParameter();
		
	if(U1_TSP_CONTROL_MODE_INHIBIT==0)	
	{
		LDTSP_vDetectOscillation();
		LDTSP_vDetectMitigation();
	}
	else
	{
		;
	}	
}

void LDTSP_vDetectYawParameter(void)
{

	/* Determine Delta yaw Threshold*/
	tsp_tempW0 = tsp_yaw_threshold;
	
	tsp_tempW1 = LCESP_s16IInter3Point(vref5, S16_TSP_LS_THR, S16_TSP_LS_DYAW_THR,
																						S16_TSP_HS_THR, S16_TSP_HS_DYAW_THR,
																						S16_TSP_CS_THR, S16_TSP_CS_DYAW_THR);
		
	tsp_dyaw_add_LS_thr = LCESP_s16IInter3Point(det_alatc, S16_TSP_ALAT_DYAW_THR_LOW, S16_TSP_LS_ALAT_ADD_DYAW_THR_LOW,
																												 S16_TSP_ALAT_DYAW_THR_MED, S16_TSP_LS_ALAT_ADD_DYAW_THR_MED,
																												 S16_TSP_ALAT_DYAW_THR_HIGH, S16_TSP_LS_ALAT_ADD_DYAW_THR_HIGH);

	tsp_dyaw_add_HS_thr = LCESP_s16IInter3Point(det_alatc, S16_TSP_ALAT_DYAW_THR_LOW, S16_TSP_HS_ALAT_ADD_DYAW_THR_LOW,
																												 S16_TSP_ALAT_DYAW_THR_MED, S16_TSP_HS_ALAT_ADD_DYAW_THR_MED,
																												 S16_TSP_ALAT_DYAW_THR_HIGH, S16_TSP_HS_ALAT_ADD_DYAW_THR_HIGH);																										 

	tsp_dyaw_add_CS_thr = LCESP_s16IInter3Point(det_alatc, S16_TSP_ALAT_DYAW_THR_LOW, S16_TSP_CS_ALAT_ADD_DYAW_THR_LOW,
																												 S16_TSP_ALAT_DYAW_THR_MED, S16_TSP_CS_ALAT_ADD_DYAW_THR_MED,
																												 S16_TSP_ALAT_DYAW_THR_HIGH, S16_TSP_CS_ALAT_ADD_DYAW_THR_HIGH);																											 
		
	tsp_dyaw_add_thr = LCESP_s16IInter3Point(vref5, S16_TSP_LS_THR, tsp_dyaw_add_LS_thr,
																									S16_TSP_HS_THR, tsp_dyaw_add_HS_thr,
																									S16_TSP_CS_THR, tsp_dyaw_add_CS_thr);
		
	tsp_tempW1 = tsp_tempW1 + tsp_dyaw_add_thr;
	tsp_yaw_threshold = LCESP_s16Lpf1Int(tsp_tempW1, tsp_tempW0, 17);

	/*Determine yawc Threshold*/
	tsp_tempW2 = tsp_yawc_thr;
	
	tsp_tempW3 = LCESP_s16IInter3Point(vref5, S16_TSP_LS_THR, S16_TSP_LS_YAWC_THR,
										  											S16_TSP_HS_THR, S16_TSP_HS_YAWC_THR,
										  											S16_TSP_CS_THR, S16_TSP_CS_YAWC_THR);
	
	tsp_yawc_add_LS_thr = LCESP_s16IInter3Point(det_alatc, S16_TSP_ALAT_YAWC_THR_LOW, S16_TSP_LS_ALAT_ADD_YAWC_THR_LOW,
																												 S16_TSP_ALAT_YAWC_THR_MED, S16_TSP_LS_ALAT_ADD_YAWC_THR_MED,
																												 S16_TSP_ALAT_YAWC_THR_HIGH, S16_TSP_LS_ALAT_ADD_YAWC_THR_HIGH);

	tsp_yawc_add_HS_thr = LCESP_s16IInter3Point(det_alatc, S16_TSP_ALAT_YAWC_THR_LOW, S16_TSP_HS_ALAT_ADD_YAWC_THR_LOW,
																												 S16_TSP_ALAT_YAWC_THR_MED, S16_TSP_HS_ALAT_ADD_YAWC_THR_MED,
																												 S16_TSP_ALAT_YAWC_THR_HIGH, S16_TSP_HS_ALAT_ADD_YAWC_THR_HIGH);

	tsp_yawc_add_CS_thr = LCESP_s16IInter3Point(det_alatc, S16_TSP_ALAT_YAWC_THR_LOW, S16_TSP_CS_ALAT_ADD_YAWC_THR_LOW,
																												 S16_TSP_ALAT_YAWC_THR_MED, S16_TSP_CS_ALAT_ADD_YAWC_THR_MED,
																												 S16_TSP_ALAT_YAWC_THR_HIGH, S16_TSP_CS_ALAT_ADD_YAWC_THR_HIGH);

	tsp_yawc_add_thr = LCESP_s16IInter3Point(vref5, S16_TSP_LS_THR, tsp_yawc_add_LS_thr,
																									S16_TSP_HS_THR, tsp_yawc_add_HS_thr,
																									S16_TSP_CS_THR, tsp_yawc_add_CS_thr);	

	tsp_tempW3 = tsp_tempW3 + tsp_yawc_add_thr;
	tsp_yawc_thr = LCESP_s16Lpf1Int(tsp_tempW3, tsp_tempW2, 17);
	
	/*Determine TSP delta yaw*/	
	tsp_delta_yaw_first = yaw_out-rf;

	/*Determine TSP delta yaw dot*/	
	tsp_delta_yaw_lf_old = tsp_delta_yaw_lf;	
	tsp_delta_yaw_lf = LCESP_s16Lpf1Int(tsp_delta_yaw_first, tsp_delta_yaw_lf_old, 8);  
	tsp_delta_yaw_dot = (int16_t)( (((int32_t)(tsp_delta_yaw_lf - tsp_delta_yaw_lf_old))*100)/7 ) ;
    
	/*Determine TSP yawc*/    
  tsp_yawc_first_old3 = tsp_yawc_first_old2;
  tsp_yawc_first_old2 = tsp_yawc_first_old;
	tsp_yawc_first_old = tsp_yawc_first;

	tsp_yawc_first = LCESP_s16Lpf1Int(rf, tsp_yawc_first_old, 28);     		        
  
	/*Determine TSP yawc dot*/  
  tsp_tempW4 = tsp_yawc_first - tsp_yawc_first_old3;
  tsp_yawc_dot = (int16_t)((((int32_t)(tsp_tempW4))*1000)/21);
    		        
}

void LDTSP_vDetectOscillation(void)
{
	LDTSP_vDetectOscilCycle();
	LDTSP_vDetectOscilDyaw();	
	LDTSP_vDetectOscilYawc();	
}

void LDTSP_vDetectOscilCycle(void)
{
	if((ldtspu1TrlrHtchSwAtvValid==0)&&(ldtspu8TrlrHtchSwAtv==1))
	{
		U1_TSP_TRAILER_CONNECT_ON = 1;
		ldtspu8DetectOscilCycle = U8_TSP_DETECT_OSCIL_CYCLE;
	}
	else
	{
		U1_TSP_TRAILER_CONNECT_ON = 0;
		ldtspu8DetectOscilCycle = U8_TSP_DETECT_OSCIL_CYCLE_NOTR;
	}
}

void LDTSP_vDetectOscilDyaw(void)
{
	
	if(TSP_ON==0)
	{
		lds16TspFreqMaxTime = S16_TSP_FRQNC_MAX_TIME_TSP_NO;
	}
	else
	{
		lds16TspFreqMaxTime = LCESP_s16IInter2Point(vref5, S16_TSP_LS_THR, S16_TSP_FRQNC_MAX_TIME_TSP_LS, 
																											 S16_TSP_HS_THR, S16_TSP_FRQNC_MAX_TIME_TSP_HS);
	}
	
	if((TSP_PLUS_DYAW_THR==0)&&(TSP_MINUS_DYAW_THR==0)) 
	{
		if((tsp_delta_yaw_first)<-tsp_yaw_threshold) 
		{			
			TSP_PLUS_DYAW_THR = 0;			
			TSP_MINUS_DYAW_THR = 1;
		}
		else if((tsp_delta_yaw_first)>tsp_yaw_threshold) 
		{
			TSP_PLUS_DYAW_THR = 1;
			TSP_MINUS_DYAW_THR = 0;
		}
		else
		{
			;			
		}	
		
		tsp_dyaw_count = 0;
		tsp_dyaw_count_old = 0;
		tsp_dyaw_count_old2 = 0;
		tsp_max_amplitude = 0;
		tsp_max_amplitude_old = 0;
		tsp_yaw_threshold_max_amp =0;
		tsp_yaw_threshold_max_amp_old = 0;
	}
	else if((TSP_PLUS_DYAW_THR==1)&&(TSP_MINUS_DYAW_THR==0)) 
	{
		if(tsp_dyaw_count<lds16TspFreqMaxTime) 
		{
			tsp_dyaw_count ++;
			
			if(tsp_dyaw_count==1) 
			{
				tsp_max_amplitude = McrAbs(tsp_delta_yaw_first);
				tsp_yaw_threshold_max_amp = tsp_yaw_threshold;
			}
			else 
			{
				if(McrAbs(tsp_delta_yaw_first)>tsp_max_amplitude) 
				{
					tsp_max_amplitude = McrAbs(tsp_delta_yaw_first);
					tsp_yaw_threshold_max_amp = tsp_yaw_threshold;
				}
				else
				{
					if((tsp_detect_oscil_count==1)||(tsp_detect_oscil_count==2))
					{
						TSP_PLUS_DYAW_PEAK_DETECT = 1;
					}
					else if((tsp_detect_oscil_count==3)||(tsp_detect_oscil_count==4))
					{
						TSP_PLUS_DYAW_PEAK_DETECT = 0;
					}
					else
					{
						;
					}
				}
			}

			if((tsp_delta_yaw_first)<-tsp_yaw_threshold) 
			{
				TSP_PLUS_DYAW_THR = 0;			
				TSP_MINUS_DYAW_THR = 1;
				
				tsp_max_amplitude_old = tsp_max_amplitude;
				tsp_yaw_threshold_max_amp_old = tsp_yaw_threshold_max_amp;
								
				tsp_dyaw_count_old2 = tsp_dyaw_count_old;
				tsp_dyaw_count_old = tsp_dyaw_count;
				
				if (tsp_dyaw_count_old2==0)
				{
					tsp_dyaw_count_old2 = 1;
				}
				else
				{
					;
				}
				
				if((tsp_dyaw_count>=(S16_TSP_FRQNC_MIN_TIME))&&(tsp_dyaw_count<=lds16TspFreqMaxTime)) 
				{
					tsp_detect_oscil_count ++;
					U1_TSP_FRQ_FAIL = 0;
				}
				else	
				{
					tsp_detect_oscil_count = 0;
					tsp_dyaw_count_old = 0;
					tsp_dyaw_count_old2 = 0;
					U1_TSP_FRQ_FAIL = 1;
					TSP_PLUS_DYAW_PEAK_DETECT = 0;
					TSP_MINUS_DYAW_PEAK_DETECT = 0;
					tsp_plus_dyaw_peak_count = 0;
					tsp_minus_dyaw_peak_count = 0;
				}
	
				tsp_dyaw_count = 0;
			}
			else
			{
				if(tsp_delta_yaw_first < tsp_yaw_threshold)
				{
					TSP_DYAW_CHANGE = 1;
				}
				else if((TSP_DYAW_CHANGE == 1) && (tsp_delta_yaw_first > tsp_yaw_threshold))
				{
					TSP_DYAW_CHANGE = 0;
          tsp_dyaw_count = 0;
        }
        else
        {
        	;
        }
			}		
		}
		else 
		{
			tsp_detect_oscil_count = 0;		
			tsp_dyaw_count_old = 0;
			tsp_dyaw_count_old2 = 0;
			TSP_PLUS_DYAW_THR = 0;			
			TSP_MINUS_DYAW_THR = 0;
			TSP_PLUS_DYAW_PEAK_DETECT = 0;
			TSP_MINUS_DYAW_PEAK_DETECT = 0;
			tsp_plus_dyaw_peak_count = 0;
			tsp_minus_dyaw_peak_count = 0;
			TSP_DYAW_CHANGE = 0;
		}
	}
	else if((TSP_PLUS_DYAW_THR==0)&&(TSP_MINUS_DYAW_THR==1))
	{
		if(tsp_dyaw_count<lds16TspFreqMaxTime) 
		{
			tsp_dyaw_count ++;
			
			if(tsp_dyaw_count==1) 
			{
				tsp_max_amplitude = McrAbs(tsp_delta_yaw_first);
				tsp_yaw_threshold_max_amp = tsp_yaw_threshold;
			}
			else 
			{
				if(McrAbs(tsp_delta_yaw_first)>tsp_max_amplitude) 
				{
					tsp_max_amplitude = McrAbs(tsp_delta_yaw_first);
					tsp_yaw_threshold_max_amp = tsp_yaw_threshold;
				}
				else
				{
					if((tsp_detect_oscil_count==1)||(tsp_detect_oscil_count==2))
					{
						TSP_MINUS_DYAW_PEAK_DETECT = 1;
					}
					else if((tsp_detect_oscil_count==3)||(tsp_detect_oscil_count==4))
					{
						TSP_MINUS_DYAW_PEAK_DETECT = 0;
					}
					else
					{
						;
					}					
				}
			}

			if((tsp_delta_yaw_first)>tsp_yaw_threshold) 
			{
				TSP_PLUS_DYAW_THR = 1;
				TSP_MINUS_DYAW_THR = 0;			
	
				tsp_max_amplitude_old = tsp_max_amplitude;
				tsp_yaw_threshold_max_amp_old = tsp_yaw_threshold_max_amp;
				
				tsp_dyaw_count_old2 = tsp_dyaw_count_old;
				tsp_dyaw_count_old = tsp_dyaw_count;
				
				if (tsp_dyaw_count_old2==0)
				{
					tsp_dyaw_count_old2 = 1;
				}
				else
				{
					;
				}
				
				if((tsp_dyaw_count>=(S16_TSP_FRQNC_MIN_TIME))&&(tsp_dyaw_count<=lds16TspFreqMaxTime))  
				{		
					tsp_detect_oscil_count ++;					
					U1_TSP_FRQ_FAIL = 0;
				}
				else	
				{
					tsp_detect_oscil_count = 0;
					tsp_dyaw_count_old = 0;
					tsp_dyaw_count_old2 = 0;
					U1_TSP_FRQ_FAIL = 1;
					TSP_PLUS_DYAW_PEAK_DETECT = 0;
					TSP_MINUS_DYAW_PEAK_DETECT = 0;
					tsp_plus_dyaw_peak_count = 0;
					tsp_minus_dyaw_peak_count = 0;
				}
	
				tsp_dyaw_count = 0;
			}
			else
			{
				if(tsp_delta_yaw_first > -tsp_yaw_threshold)
				{
					TSP_DYAW_CHANGE = 1;
				}
				else if((TSP_DYAW_CHANGE == 1) && (tsp_delta_yaw_first < -tsp_yaw_threshold))
				{
					TSP_DYAW_CHANGE = 0;
          tsp_dyaw_count = 0;
        }
        else
        {
        	;
        }
			}
		}
		else 
		{
			tsp_detect_oscil_count = 0;			
			tsp_dyaw_count_old = 0;
			tsp_dyaw_count_old2 = 0;
			TSP_PLUS_DYAW_THR = 0;			
			TSP_MINUS_DYAW_THR = 0;
			TSP_PLUS_DYAW_PEAK_DETECT = 0;
			TSP_MINUS_DYAW_PEAK_DETECT = 0;
			tsp_plus_dyaw_peak_count = 0;
			tsp_minus_dyaw_peak_count = 0;
			TSP_DYAW_CHANGE = 0;
		}
	}
	else
	{
		;
	}
	
	/* Calculate Frequency rate */	
	if(tsp_detect_oscil_count==3)
	{
		tsp_freq_rate_2nd_3rd = McrAbs(100-((tsp_dyaw_count_old*100)/tsp_dyaw_count_old2));
	}
	else if(tsp_detect_oscil_count==4)
	{
		tsp_freq_rate_2nd_3rd = 0;
		tsp_freq_rate_3rd_4th = McrAbs(100-((tsp_dyaw_count_old*100)/tsp_dyaw_count_old2));
	}
	else
	{
		tsp_freq_rate_2nd_3rd = 0;
		tsp_freq_rate_3rd_4th = 0;
	}
	
	/* Calculate Delta yaw peak rate Time */
	if(TSP_PLUS_DYAW_PEAK_DETECT==1)
	{
		tsp_plus_dyaw_peak_count ++;
	}
	else if(tsp_detect_oscil_count>=5)
	{
		tsp_plus_dyaw_peak_count = 0;
	}
	else
	{
		;
	}
	
	if(TSP_MINUS_DYAW_PEAK_DETECT==1)
	{
		tsp_minus_dyaw_peak_count ++;
	}
	else if(tsp_detect_oscil_count>=5)
	{
		tsp_minus_dyaw_peak_count = 0;
	}
	else
	{
		;
	}

	/* Calculate Delta yaw peak rate */
	if(tsp_detect_oscil_count==1)
	{
		tsp_max_amplitude_1st = tsp_max_amplitude;
	}
	else if(tsp_detect_oscil_count==2)
	{
		tsp_max_amplitude_2nd = tsp_max_amplitude;
	}
	else if(tsp_detect_oscil_count==3)
	{
		tsp_max_amplitude_3rd = tsp_max_amplitude;
	}
	else if(tsp_detect_oscil_count==4)
	{
		tsp_max_amplitude_4th = tsp_max_amplitude;
	}
	else
	{
		;
	}
		
	if(tsp_detect_oscil_count==3)
	{
		tsp_amplitude_rate_1st_3rd = 100-(int16_t)((((int32_t)(tsp_max_amplitude_3rd))*100)/tsp_max_amplitude_1st);
	}
	else if(tsp_detect_oscil_count==4)
	{
		tsp_amplitude_rate_1st_3rd = 0;
		tsp_amplitude_rate_2nd_4th = 100-(int16_t)((((int32_t)(tsp_max_amplitude_4th))*100)/tsp_max_amplitude_2nd);
	}
	else
	{
		tsp_amplitude_rate_1st_3rd = 0;
		tsp_amplitude_rate_2nd_4th = 0;	
	}
	
	/* Calculate Delta yaw peak damping ratio */
	if(tsp_detect_oscil_count==3)
	{
		if(TSP_PLUS_DYAW_THR==1)
		{
			tsp_amp_damping_ratio_1st_3rd = (int16_t)((((int32_t)(tsp_amplitude_rate_1st_3rd))*(1000/10))/tsp_plus_dyaw_peak_count);
		}
		else if(TSP_MINUS_DYAW_THR==1)
		{
			tsp_amp_damping_ratio_1st_3rd = (int16_t)((((int32_t)(tsp_amplitude_rate_1st_3rd))*(1000/10))/tsp_minus_dyaw_peak_count);
		}
		else
		{
			;
		}
	}
	else if(tsp_detect_oscil_count==4)
	{
		tsp_amp_damping_ratio_1st_3rd = 0;
		
		if(TSP_PLUS_DYAW_THR==1)
		{
			tsp_amp_damping_ratio_2nd_4th = (int16_t)((((int32_t)(tsp_amplitude_rate_2nd_4th))*(1000/10))/tsp_plus_dyaw_peak_count);
		}
		else if(TSP_MINUS_DYAW_THR==1)
		{
			tsp_amp_damping_ratio_2nd_4th = (int16_t)((((int32_t)(tsp_amplitude_rate_2nd_4th))*(1000/10))/tsp_minus_dyaw_peak_count);
		}
		else
		{
			;
		}
	}
	else
	{
		tsp_amp_damping_ratio_1st_3rd = 0;
		tsp_amp_damping_ratio_2nd_4th = 0;
	}
		
}	
	
void LDTSP_vDetectOscilYawc(void)
{

	if((TSP_PLUS_YAWC_THR==0)&&(TSP_MINUS_YAWC_THR==0)) 
	{
		if((tsp_yawc_first)<-tsp_yawc_thr) 
		{
			TSP_PLUS_YAWC_THR = 0;			
			TSP_MINUS_YAWC_THR = 1;
		}
		else if((tsp_yawc_first)>tsp_yawc_thr) 
		{
			TSP_PLUS_YAWC_THR = 1;
			TSP_MINUS_YAWC_THR = 0;
		}	
		else
		{
			;
		}
		
		tsp_yawc_count = 0;									
	}
	else if((TSP_PLUS_YAWC_THR==1)&&(TSP_MINUS_YAWC_THR==0)) 
	{
		if(tsp_yawc_count<(S16_TSP_STEER_MAX_TIME)) 
		{
			tsp_yawc_count++;		
		}
		else 
		{		
			TSP_PLUS_YAWC_THR = 0;			
			TSP_MINUS_YAWC_THR = 0;	
		}
		
		if((TSP_ON==0)&&((tsp_yawc_first)>tsp_yawc_thr))
		{
			tsp_detect_oscil_count = 0;
			tsp_dyaw_count_old = 0;
	 		tsp_dyaw_count_old2 = 0;
	 	}
	 	else
	 	{
			if((tsp_yawc_first)<-tsp_yawc_thr) 
			{
				TSP_PLUS_YAWC_THR = 0;			
				TSP_MINUS_YAWC_THR = 1;
				
				if((TSP_ON==0)&&(tsp_yawc_count>=(S16_TSP_STEER_MIN_TIME))&&(tsp_yawc_count<=(S16_TSP_STEER_MAX_TIME)))  
				{
					tsp_detect_oscil_count = 0;
					tsp_dyaw_count_old = 0;
	 				tsp_dyaw_count_old2 = 0;
				}
				else
				{
					;
				}
				
				tsp_yawc_count = 0;			
			}		
			else
			{
				;
			}
		}
	}		
	else if((TSP_PLUS_YAWC_THR==0)&&(TSP_MINUS_YAWC_THR==1))
	{
		if(tsp_yawc_count<(S16_TSP_STEER_MAX_TIME)) 
		{
			tsp_yawc_count++;		
		}
		else 
		{
			TSP_PLUS_YAWC_THR = 0;			
			TSP_MINUS_YAWC_THR = 0;		
		}

		if((TSP_ON==0)&&((tsp_yawc_first)<-tsp_yawc_thr))
		{
			tsp_detect_oscil_count = 0;
			tsp_dyaw_count_old = 0;
	 		tsp_dyaw_count_old2 = 0;
	 	}
	 	else
	 	{
			if((tsp_yawc_first)>tsp_yawc_thr) 
			{
				TSP_PLUS_YAWC_THR = 1;
				TSP_MINUS_YAWC_THR = 0;			
    	
				if((TSP_ON==0)&&(tsp_yawc_count>=(S16_TSP_STEER_MIN_TIME))&&(tsp_yawc_count<=(S16_TSP_STEER_MAX_TIME)))  
				{				
					tsp_detect_oscil_count = 0;
					tsp_dyaw_count_old = 0;
					tsp_dyaw_count_old2 = 0;
				}
				else
				{
					;
				}
				
				tsp_yawc_count = 0;
			}
			else
			{
				;
			}
		}
  }
	else
	{
		;
	}	
}

void LDTSP_vDetectMitigation(void)
{
	if(TSP_ON==1)
	{
		if((McrAbs(tsp_delta_yaw_first)<S16_TSP_DYAW_MITIG_THR_LOW)&&(McrAbs(tsp_delta_yaw_dot)<S16_TSP_DYAW_DOT_MITIG_THR_LOW))
		{
			tsp_osci_mitig_count = tsp_osci_mitig_count + U8_MITI_LOW_THR_COUNT;
		}
		else if((McrAbs(tsp_delta_yaw_first)<S16_TSP_DYAW_MITIG_THR_MED)&&(McrAbs(tsp_delta_yaw_dot)<S16_TSP_DYAW_DOT_MITIG_THR_MED))
		{
			tsp_osci_mitig_count = tsp_osci_mitig_count + U8_MITI_MED_THR_COUNT;
		}
		else if((McrAbs(tsp_delta_yaw_first)<S16_TSP_DYAW_MITIG_THR_HIGH)&&(McrAbs(tsp_delta_yaw_dot) < S16_TSP_DYAW_DOT_MITIG_THR_HIGH) && (vref5 < VREF_90_KPH))
		{
			tsp_osci_mitig_count = tsp_osci_mitig_count + U8_MITI_HIGH_THR_COUNT;
		}
		else if((McrAbs(tsp_yawc_first)<tsp_yawc_thr)&&(tsp_osci_mitig_count > 0))
		{
			tsp_osci_mitig_count = tsp_osci_mitig_count - U8_MITI_OVER_THR_COUNT;
			
			if(tsp_osci_mitig_count<0)
			{
				tsp_osci_mitig_count = 0;
			}
			else 
			{
				;
			}
		}
		else
		{
			tsp_osci_mitig_count = 0;
		}
	}
	else
	{
		;
	}
}


#endif

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDTSPCallDetection
	#include "Mdyn_autosar.h"
#endif