#ifndef __LCPBACALLCONTROL_H__
#define __LCPBACALLCONTROL_H__

/*Includes *********************************************************************/
#include "LVarHead.h"
/*Global MACRO FUNCTION Definition *********************************************/

#define __HOB    0 /* Hydraulic Over Boost */

#define PBA_DUMP_PATTERN            0   /* 1 means PBA dump pattern used */


/*Global MACRO CONSTANT Definition *********************************************/

#define S16_PBA_SLIP_SPEED_GAIN_DEP_L_V	     300
#define S16_PBA_SLIP_SPEED_GAIN_DEP_M1_V     500
#define S16_PBA_SLIP_SPEED_GAIN_DEP_M2_V     800
#define S16_PBA_SLIP_SPEED_GAIN_DEP_H_V      1000
 
#define S16_PBA_ENTER_SLIP_LSP	2000
#define S16_PBA_ENTER_SLIP_MSP	1500
#define S16_PBA_ENTER_SLIP_HSP	1000
#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
#define __PBA_PRE_ABS_ENABLE		1
#else
#define __PBA_PRE_ABS_ENABLE		0
#endif

#if __HOB
#define lcu1HobOnFlag                   HOB01.bit7
#define lcu1HobInhibitFlag              HOB01.bit6
#define	lcu1HobMpInitBigSlopOK          HOB01.bit5
#define	lcu1HobMotorOnFlag              HOB01.bit4
#define	HOB_flag_03                     HOB01.bit3
#define	HOB_flag_02                     HOB01.bit2
#define HOB_flag_01                     HOB01.bit1
#define HOB_flag_00                     HOB01.bit0

extern  struct	U8_BIT_STRUCT_t HOB01;
#endif


/*Global Type Declaration ******************************************************/



/*Global Extern Variable Declaration *******************************************/

extern int16_t  PBA_EXIT_CONTROL_cnt;
extern int16_t  lcs16PbaSpeedEnterThr;

extern int8_t lctspu1TspPbaComb;
extern int16_t  PBA_EXIT_CONTROL_cnt;
#if	(__PBA_PRE_ABS_ENABLE == 1)
extern int16_t ls16PBAPDTCount;
#endif

#if __HOB
extern int16_t lcs16HobMscTargetV,HOBflags;
#endif


/*Global Extern Functions Declaration ******************************************/
extern void LCPBA_vInitializeVariables(void);
extern void LCPBA_vCallControl(void);
#if __HOB
extern void LCHOB_vCallControl(void);
extern void LCMSC_vSetHOBTargetVoltage(void);
extern void LAHOB_vCallActHW(void);	
#endif


#endif
