/*******************************************************************/

/*******************************************************************/

/* Includes ********************************************************/


#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCAFSCallControl
	#include "Mdyn_autosar.h"
#endif


#include "LCAFSCallControl.h"
/* Local Definition  ***********************************************/

/* Variables Definition*********************************************/
extern uint8_t AFS1_STATUS;
extern uint8_t CDC_STATUS;
extern uint8_t STR_TAG_AGL;
extern uint8_t STR_TAG_AST;
extern uint8_t FLAG_UCC;


/* Local Function prototype ****************************************/
#if defined(__UCC_1)
	void LCAFS_vCallControl(void);
	void ESP_TO_UCC_STR_ANG(void);
	void UCC_LAMP(void); 
#endif

/* Implementation***************************************************/

#if defined(__UCC_1)

void LCAFS_vCallControl(void)                   
{
	ESP_TO_UCC_STR_ANG();
    UCC_LAMP(); 
}

/*******************************************************************/
void ESP_TO_UCC_STR_ANG(void)
{
                    
//  if(ESP_ERROR_FLG||(AFS1==0xFF)) {
    if( (AFS1_STATUS ==0xFF) || ESP_ERROR_FLG || (CDC_STATUS != 2) ) {
        UCC_AFS_COMBINATION = 0;
        STR_TAG_AGL = 0xFF;
        STR_TAG_AST = 0xFF;
    } else {
        
            override_angle_old = override_angle;
            override_assist_old = override_assist;
            delta_yaw_integrate_old = delta_yaw_integrate;
            delta_yaw_error = yaw_out-rf2; 
            
            
            delta_yaw_error_buf[2]=delta_yaw_error_buf[1];
            delta_yaw_error_buf[1]=delta_yaw_error_buf[0];
            delta_yaw_error_buf[0]=delta_yaw_error;
    
            // 100 * -100 / 100 = 
            delta_yaw_diff = (delta_yaw_error_buf[0]-delta_yaw_error_buf[2]);
            
            delta_yaw_integrate = LCESP_s16Lpf1Int( delta_yaw_error*10 , delta_yaw_integrate_old , 1); //

            if( ABS_fz && ESP_SPLIT ) {
                afs_p_gain = 30;    // 127/2500
                afs_d_gain = 0;     // 
                afs_i_gain = 130;
            } else {
                afs_p_gain = 30;    
                afs_d_gain = 0;
                afs_i_gain = 0;
            }       
                    
            tempW2 = delta_yaw_error; tempW1 = afs_p_gain; tempW0 = 10;
            s16muls16divs16();      
            esp_tempW4 = tempW3;  
            
            tempW2 = delta_yaw_integrate; tempW1 = afs_i_gain; tempW0 = 100;
            s16muls16divs16();      
            esp_tempW3 = tempW3;  

            tempW3 = delta_yaw_diff*afs_d_gain; 
            
            afs_control_input =(esp_tempW4 + tempW3 + esp_tempW3);     
                                        
        if( ABS_fz && ESP_SPLIT ) {   
            
            esp_tempW3 = 200;   // dead zone
            
            if ( (!UCC_AFS_COMBINATION) && (delta_yaw_error<esp_tempW3) && (delta_yaw_error>-esp_tempW3))
                afs_control_input = 0;       
                        
                        
            // esp_tempW5 = LCESP_s16IInter3Point ( vrefk, 150, 30, 300, 80, 600, 100 );  // 
            esp_tempW5 = 100;
            tempW2 = esp_tempW5; tempW1 = afs_control_input; tempW0 = 100;
            s16muls16divs16();
            afs_control_input = tempW3;   
            
            if ( afs_control_input > 10000 ) esp_tempW5 = 10000;
            else if ( afs_control_input < -10000 ) esp_tempW5 = -10000;
            else esp_tempW5 = afs_control_input;
            
            esp_tempW3 = LCESP_s16IInter2Point ( det_abs_wstr, 900, 3000, 4500, 0 );  //  
        
            if ( afs_control_input > esp_tempW3 ) afs_control_input = esp_tempW3;
            else if ( afs_control_input < -esp_tempW3 ) afs_control_input = -esp_tempW3;
            
            if ( vrefk > 70 ) {                                              
                if ( (!UCC_AFS_COMBINATION) && (afs_control_input<200)&&(afs_control_input>-200)) UCC_AFS_COMBINATION = 0; 
                else if ( (UCC_AFS_COMBINATION) && (afs_control_input<100)&&(afs_control_input>-100)) UCC_AFS_COMBINATION = 0; 
                else UCC_AFS_COMBINATION = 1;                                   
            } else {
                afs_control_input = 0;             
                UCC_AFS_COMBINATION = 0;
            }
            
            if (UCC_AFS_COMBINATION) {
                override_angle = -afs_control_input; // 5.00 deg/s => 25.5 deg
                override_assist = -esp_tempW5;
                esp_tempW4 = 10;
            } else {
                override_angle = 0; // 5.00 deg/s => 25.5 deg
                override_assist = 0;
                esp_tempW4 = 10;
            }               
                            

        } else {

            esp_tempW3 = 200;   // dead zone
            
            if ( (!UCC_AFS_COMBINATION) && (delta_yaw_error<esp_tempW3) && (delta_yaw_error>-esp_tempW3))
                afs_control_input = 0;       
                        
                        
            esp_tempW5 = LCESP_s16IInter3Point ( vrefk, 150, 70, 300, 80, 600, 100 );  // 
            tempW2 = esp_tempW5; tempW1 = afs_control_input; tempW0 = 100;
            s16muls16divs16();
            afs_control_input = tempW3;   
            
            if ( afs_control_input > 10000 ) esp_tempW5 = 10000;
            else if ( afs_control_input < -10000 ) esp_tempW5 = -10000;
            else esp_tempW5 = afs_control_input;
            
            esp_tempW3 = LCESP_s16IInter2Point ( det_abs_wstr, 900, 3000, 4500, 0 );  //  
        
            if ( afs_control_input > esp_tempW3 ) afs_control_input = esp_tempW3;
            else if ( afs_control_input < -esp_tempW3 ) afs_control_input = -esp_tempW3;
            
            if ( vrefk > 150 ) {                                                 
                if ( (!UCC_AFS_COMBINATION) && (afs_control_input<200)&&(afs_control_input>-200)) UCC_AFS_COMBINATION = 0; 
                else if ( (UCC_AFS_COMBINATION) && (afs_control_input<100)&&(afs_control_input>-100)) UCC_AFS_COMBINATION = 0; 
                else UCC_AFS_COMBINATION = 1;                                   
            } else {
                afs_control_input = 0;             
                UCC_AFS_COMBINATION = 0;
            }
            
            if (UCC_AFS_COMBINATION) {
                override_angle = -afs_control_input; // 5.00 deg/s => 25.5 deg
                override_assist = -esp_tempW5;
                esp_tempW4 = 17;
            } else {
                override_angle = 0; // 5.00 deg/s => 25.5 deg
                override_assist = 0;
                esp_tempW4 = 17;
            }               
        }           

        override_angle = LCESP_s16Lpf1Int( override_angle , override_angle_old , esp_tempW4); //  3 Hz
        override_assist = LCESP_s16Lpf1Int( override_assist , override_assist_old , esp_tempW4); //  3 Hz
            
        if (!UCC_AFS_COMBINATION) {
            if ( (override_angle < 100) && (override_angle > -100)  )  override_angle = 0;
        }                       

        tempW2 = override_angle; tempW1 = 1; tempW0 = 100;
        s16muls16divs16();      
        STR_TAG_AST = (int8_t)tempW3;                 // +- 25.5 deg 

        tempW2 = override_assist; tempW1 = 1; tempW0 = 100;
        s16muls16divs16();      
        STR_TAG_AGL = (int8_t)tempW3;         // +- 100 Amp.
        // STR_TAG_AST = override_angle;        // +- 25.5 deg 
    }       
}
/*************************************************************************/
void UCC_LAMP(void)
{
    if ( (FL.UCC_DETECT_BUMP_flag ||
         FR.UCC_DETECT_BUMP_flag ||
         FL.UCC_DETECT_PotHole_flag ||
         FR.UCC_DETECT_PotHole_flag)&& ABS_fz )
    
        FLAG_UCC = 1;   
    else if ( (override_angle != 0) && (override_angle !=0xFF) )
        FLAG_UCC = 1;   
    else 
        FLAG_UCC = 0;
   
}   
/*************************************************************************/
#endif		// #if __EDC ~


#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCAFSCallControl
	#include "Mdyn_autosar.h"
#endif    
