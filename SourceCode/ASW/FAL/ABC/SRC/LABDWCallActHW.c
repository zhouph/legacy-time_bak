/******************************************************************************
* Project Name: BRAKE DISC WIPING
* File: LDBDWCallActHW.C
* Date: June. 4. 2005
******************************************************************************/


#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LABDWCallActHW
	#include "Mdyn_autosar.h"
#endif

/* Includes ******************************************************************/

#include "LABDWCallActHW.h"
#include "LAEPCCallActHW.h"
#include "LACallMain.h"

  #if __ADVANCED_MSC
    #include "LAABSCallMotorSpeedControl.h"
  #endif


#if __HDC
#include "LCHDCCallControl.h"
#endif


#if __EBP
#include "LDEBPCallDetection.h"
#include "LCEBPCallControl.h"
#endif



#if __BDW
/* Logcal Definiton  *********************************************************/

/* Variables Definition*******************************************************/

int16_t bdw_msc_target_vol;

WHEEL_VV_OUTPUT_t  labdw_FL1HP,labdw_FR1HP,labdw_RL1HP,labdw_RR1HP;

/* LocalFunction prototype ***************************************************/
void LABDW_vCallActHW(void);
void LABDW_vCallActValve(void);
void LCMSC_vSetBDWTargetVoltage(void);

/* Implementation*************************************************************/



/******************************************************************************
* FUNCTION NAME:      LABDW_vCallActHW
* CALLED BY:          LA_vCallMainValve()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Valve/Motor actuaction by BRAKE DISC WIPING
******************************************************************************/
void    LABDW_vCallActHW(void)
{
    LABDW_vCallActValve();

}
void LABDW_vCallActValve(void)
{

    LA_vResetWheelValvePwmDuty(&labdw_FL1HP);  
    LA_vResetWheelValvePwmDuty(&labdw_FR1HP);
    LA_vResetWheelValvePwmDuty(&labdw_RL1HP);  
    LA_vResetWheelValvePwmDuty(&labdw_RR1HP);
    
    if (BDW_ON==1)
    {
        HV_VL_fl = BDW_HV_VL_fl;
        AV_VL_fl = BDW_AV_VL_fl;
        HV_VL_fr = BDW_HV_VL_fr;
        AV_VL_fr = BDW_AV_VL_fr;
        HV_VL_rl = BDW_HV_VL_rl;
        AV_VL_rl = BDW_AV_VL_rl;
        HV_VL_rr = BDW_HV_VL_rr;
        AV_VL_rr = BDW_AV_VL_rr;
        
      #if (__MGH_80_10MS == ENABLE)
        
        if(BDW_FL_ON_FLG==1)
        {
        	 LA_vSetNominalWheelValvePwmDuty(BDW_HV_VL_fl,BDW_AV_VL_fl,U8_BASE_CTRLTIME,U8_BASE_CTRLTIME,&labdw_FL1HP);
        }
        else{}	
        	
        if(BDW_FR_ON_FLG==1)
        {
        	 LA_vSetNominalWheelValvePwmDuty(BDW_HV_VL_fr,BDW_AV_VL_fr,U8_BASE_CTRLTIME,U8_BASE_CTRLTIME,&labdw_FR1HP);
        }
        else{}
        	
        if(BDW_RL_ON_FLG==1)
        {
        	 LA_vSetNominalWheelValvePwmDuty(BDW_HV_VL_rl,BDW_AV_VL_rl,U8_BASE_CTRLTIME,U8_BASE_CTRLTIME,&labdw_RL1HP);
        }
        else{}
        	
        if(BDW_RR_ON_FLG==1)
        {
        	 LA_vSetNominalWheelValvePwmDuty(BDW_HV_VL_rr,BDW_AV_VL_rr,U8_BASE_CTRLTIME,U8_BASE_CTRLTIME,&labdw_RR1HP);
        }
        else{}			

        la_FL1HP = labdw_FL1HP;
        la_FL2HP = labdw_FL1HP;
        la_FR1HP = labdw_FR1HP;  
        la_FR2HP = labdw_FR1HP;  
        la_RL1HP = labdw_RL1HP;  
        la_RL2HP = labdw_RL1HP;  
        la_RR1HP = labdw_RR1HP;
        la_RR2HP = labdw_RR1HP;         
         
      #endif
         
      #if __SPLIT_TYPE
        S_VALVE_PRIMARY     = BDW_S_VALVE_PRIMARY;
        S_VALVE_SECONDARY   = BDW_S_VALVE_SECONDARY;
        TCL_DEMAND_PRIMARY  = BDW_TCL_DEMAND_PRIMARY;
        TCL_DEMAND_SECONDARY= BDW_TCL_DEMAND_SECONDARY;
      #else
        S_VALVE_RIGHT       = BDW_S_VALVE_PRIMARY;
        S_VALVE_LEFT        = BDW_S_VALVE_SECONDARY;
        TCL_DEMAND_fr       = BDW_TCL_DEMAND_PRIMARY;
        TCL_DEMAND_fl       = BDW_TCL_DEMAND_SECONDARY;
      #endif

    }
    else { }   
}

#if __ADVANCED_MSC
void LCMSC_vSetBDWTargetVoltage(void){
    BDW_MSC_MOTOR_ON = BDW_ON;
    if (BDW_MSC_MOTOR_ON==1)
    {
        bdw_msc_target_vol = S16_BDW_TARGET_VOLT;
    }
    else
    {
        bdw_msc_target_vol = MSC_0_V;
    }
}
#endif

#endif

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LABDWCallActHW
	#include "Mdyn_autosar.h"
#endif
