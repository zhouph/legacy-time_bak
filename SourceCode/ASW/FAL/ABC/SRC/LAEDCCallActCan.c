
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START                       
	#define idx_FILE	idx_CL_LAEDCCallActCan
	#include "Mdyn_autosar.h"               
#endif
/* Includes ********************************************************/

#include "hm_logic_var.h"
#include "LSENGCallSensorSignal.h"

#include "LCEDCCallControl.h"

/* Local Definition  ***********************************************/

/* Variables Definition*********************************************/


/* Local Function prototype ****************************************/
#if __EDC
	#if __EDC_CONV_ABS_TORQ
void LAEDC_vConvAbsToCanTorq(void);
	#endif
void LAEDC_vCallActCan(void);
	#if !SIM_MATLAB
void LAEDC_vControlEngTorq(void);
      #if __CAR_MAKER == HMC_KMC	
void LAEDC_vControlEngTorqForHMC(void);
      #elif ( (GMLAN_ENABLE==ENABLE)|| (__CAR==GM_C100) || (__CAR==GM_V275) || (__CAR==GM_CTS) || (__CAR==GM_C100_MECU) || (__CAR==GM_TAHOE) || (__CAR==GM_TAHOE_MECU)||(__CAR == GM_J300)||(__CAR == GM_LAMBDA)||(__CAR==GM_MALIBU)||(__CAR==GM_SILVERADO))	   
void LAEDC_vControlEngTorqForGM(void);
      #elif (__CAR==FIAT_PUNTO) 
void LAEDC_vControlEngTorqForDEMO(void);
      #endif
	#endif
#endif
/* Implementation***************************************************/

#if __EDC /* TAG1 */
void LAEDC_vCallActCan(void)
{

#if __EDC_ENABLE_BY_CALIBRATION == ENABLE
  if(lcedcu1CtrlEnable==1)
#endif  

  {
	#if !SIM_MATLAB
	LAEDC_vControlEngTorq();	
	#endif
  } /* if(lcedcu1CtrlEnable==1) */

}

	#if __EDC_CONV_ABS_TORQ
void LAEDC_vConvAbsToCanTorq(void)
{
  #if ( (GMLAN_ENABLE==ENABLE)|| (__CAR==GM_C100) || (__CAR==GM_V275) || (__CAR==GM_CTS) || (__CAR==GM_C100_MECU) || (__CAR==GM_TAHOE) || (__CAR==GM_TAHOE_MECU)||(__CAR == GM_J300)||(__CAR == GM_LAMBDA)||(__CAR==GM_MALIBU)||(__CAR==GM_SILVERADO))
	lesps16TCS_TQI_MSR = MSR_cal_torq;
  #elif __HMC_CAN_VER==CAN_1_2||__HMC_CAN_VER==CAN_1_3||__HMC_CAN_VER==CAN_1_4||__HMC_CAN_VER==CAN_1_6||__HMC_CAN_VER==CAN_2_0||__HMC_CAN_VER==CAN_HD_HEV || __CAR == FIAT_PUNTO
    tempW0 = (int16_t)((int32_t)MSR_cal_torq*10/max_torq);
    if(tempW0>1000)
    {
    	tempW0=1000;
    }
    else
    {
    	;
    }
	lesps16TCS_TQI_MSR = tempW0;
  #else
	lesps16TCS_TQI_MSR = MSR_cal_torq;
  #endif
}
	#endif

  #if !SIM_MATLAB	/* TAG2 */
void LAEDC_vControlEngTorq(void)	
{
    #if __CAR_MAKER == HMC_KMC	
		LAEDC_vControlEngTorqForHMC();
    #elif ( (GMLAN_ENABLE==ENABLE)|| (__CAR==GM_C100) || (__CAR==GM_V275) || (__CAR==GM_CTS) || (__CAR==GM_C100_MECU) || (__CAR==GM_TAHOE) || (__CAR==GM_TAHOE_MECU)||(__CAR == GM_J300)||(__CAR == GM_LAMBDA)||(__CAR==GM_MALIBU)||(__CAR==GM_SILVERADO))	   
		LAEDC_vControlEngTorqForGM();
	#elif (__CAR == FIAT_PUNTO)
		LAEDC_vControlEngTorqForDEMO();
    #endif	
}
    #if __CAR_MAKER == HMC_KMC
void LAEDC_vControlEngTorqForHMC(void)
{	
  #if !defined(GMDW_CAN)	/* TAG3 */
  /* Modified BY HJH for MSR fuction (040826) */
  
	if(lespu1TCS_TCS_REQ==0)		/* TCS_REQ disable */
    { 
			#if __EDC_CONV_ABS_TORQ
		LAEDC_vConvAbsToCanTorq();
			#endif    	  	
		/*-----------------------------------------------------------------------*/
		/* TCS_MFRN setting to set MSR Function (for MG 2.7 Gasoline A/T)        */
		if (lespu8EMS_ENG_CHR > 3) 		  /* ETC 적용 차량 */
		{	
			lespu1TCS_TCS_MFRN = 0;    	/* TCS_MFRN=0(FAST&SLOW) */
		}
		else 				  /* ETC 미적용 차량 */
		{	
			lespu1TCS_TCS_MFRN = 1;      /* TCS_MFRN=1(Bosch:0, Teves:1) */
		}		
        /* ----------------------------------------------------------------------*/
	
		if(EDC_ON==1)								    /* MSR Control (040826)  */
		{
	   		lespu1TCS_MSR_C_REQ = 1;    				/* set MSR_C_REQ */	
            	#if !__EDC_CONV_ABS_TORQ
            EDC_cmd = MSR_cal_torq;
            	#else
            EDC_cmd = lesps16TCS_TQI_MSR;	
    	    	#endif            	              
    	    lesps16TCS_TQI_MSR = EDC_cmd;  				/* set TQI_MSR   */
    	    tempW8 = lesps16TCS_TQI_MSR / 4;
    	    lesps16TCS_TQI_TCS = ~ (tempW8);		    /* 1's complement of TQI_MSR */
    	    lesps16TCS_TQI_TCS = lesps16TCS_TQI_TCS * 4;	
    	    lespu1TCS_TCS_REQ = 0;                      /* reset TCS_REQ */
            lespu1TCS_TCS_GSC = 0;                 		/* reset TCS_GSC */
		}
/* IVSS Engine Torque up logic */
	  #if	__DHCT
		else if(laivssu1MSRCReq==1)								// MSR Control (040826)
		{
	   		lespu1TCS_MSR_C_REQ = 1;
    		tempW0 = laivsss16MSRCalTorq;
    		if(tempW0>1000)
    		{
    			tempW0=1000;
    		}
    		else if(tempW0<lesps16EMS_DRIVE_TQI)
    		{
    			tempW0 = lesps16EMS_DRIVE_TQI;//drive_torq;
    		}
    		else 
    		{
    			  ; 
    		}

    		lesps16TCS_TQI_MSR = tempW0;
    	  
    	  tempW8 = lesps16TCS_TQI_MSR / 4;
    	  lesps16TCS_TQI_TCS = ~ (tempW8);		    /* 1's complement of TQI_MSR */
    	  lesps16TCS_TQI_TCS = lesps16TCS_TQI_TCS * 4;	
    	  lespu1TCS_TCS_REQ = 0;                      /* reset TCS_REQ */
        lespu1TCS_TCS_GSC = 0;                 		/* reset TCS_GSC */
		}
	  #endif
	  #if __TVBB
	    #if (__TVBB_ENG_TORQ_UP_CTRL == ENABLE) 
	    else if(lctvbbu1TVBB_ON==1)								    /* MSR Control (040826)  */
		{
		    tempW0 = (int16_t)(((int32_t)tvbb_torq*10)/max_torq);
        
            if(tempW0>1000)
            {
            	tempW0=1000;
            }
            else
            {
            	;
            }
	        
	        lesps16TCS_TQI_MSR = tempW0;
	    
	   		lespu1TCS_MSR_C_REQ = 1;    				/* set MSR_C_REQ */	
            //EDC_cmd = lesps16TCS_TQI_MSR;	
    	    //lesps16TCS_TQI_MSR = EDC_cmd;  				/* set TQI_MSR   */
    	    tempW8 = lesps16TCS_TQI_MSR / 4;
    	    lesps16TCS_TQI_TCS = ~ (tempW8);		    /* 1's complement of TQI_MSR */
    	    lesps16TCS_TQI_TCS = lesps16TCS_TQI_TCS * 4;	
    	    lespu1TCS_TCS_REQ = 0;                      /* reset TCS_REQ */
            lespu1TCS_TCS_GSC = 0;                 		/* reset TCS_GSC */
		}
		#endif
	  #endif
		
/* HSA Engine Torque up logic 
	  #if	__HSA && __HSA_ENG_TORQUEUP
		else if((HSA_CAN_SWITCH==1) && (EDC_HSA_flg==1) && (EDC_HSA_cnt>=71))								// MSR Control (040826)
		{
	   		lespu1TCS_MSR_C_REQ = 1;
	        if(EDC_cmd<drive_torq)EDC_cmd=drive_torq;		
    	    lesps16TCS_TQI_MSR = EDC_cmd;  				
    	    lesps16TCS_TQI_TCS = ~lesps16TCS_TQI_MSR;										                
          	lespu1TCS_TCS_REQ = 0;         
          	lespu1TCS_TCS_GSC = 0;             		
		}
	  #endif
*/
		else 
		{											    /* No EMS Control */
		    EDC_cmd=MSR_cal_torq=drive_torq;
    	    lespu1TCS_TCS_REQ = 0;                      /* reset TCS_REQ */
            lespu1TCS_TCS_GSC = 0;                 		/* reset TCS_GSC */
            lespu1TCS_MSR_C_REQ = 0;     				/* reset MSR_C_REQ        */
            lespu1TCS_TCS_CTL = 0;                      /* reset TCS_CTL		  */
            if(BTC_fz) lespu1TCS_TCS_CTL = 1;           /* set TCS_CTL		      */
            lesps16TCS_TQI_TCS = 0x3fc;                 /* clear TQI_TCS (0xff*0d4) */
            lesps16TCS_TQI_SLOW_TCS = 0x3fc;            /* clear TQI_SLOW_TCS (0xff*0d4) */            
            lesps16TCS_TQI_MSR = 0;                     /* clear TQI_MSR		  */
 		}
 	}
 	else
 	{
 		EDC_cmd=MSR_cal_torq=drive_torq;
 	}	
  #endif 	/* TAG3 */
}
    #elif ( (GMLAN_ENABLE==ENABLE)|| (__CAR==GM_C100) || (__CAR==GM_V275) || (__CAR==GM_CTS) || (__CAR==GM_C100_MECU) || (__CAR==GM_TAHOE) || (__CAR==GM_TAHOE_MECU)||(__CAR == GM_J300)||(__CAR == GM_LAMBDA)||(__CAR==GM_MALIBU)||(__CAR==GM_SILVERADO))
void LAEDC_vControlEngTorqForGM(void)
{
		#if __EDC_CONV_ABS_TORQ
	LAEDC_vConvAbsToCanTorq();
		#endif
			
  #if ((__CAR == GM_T300) || (__CAR == GM_M350) || (GMLAN_ENABLE==ENABLE)||(__CAR == GM_J300)||(__CAR == GM_LAMBDA)||(__CAR==GM_MALIBU)) 
    if((fu8EngineModeStep==0) || (fu8EngineModeStep==1))
    {
    	EDC_ON = 0;
    }
    else
    {
    	;
    }				
  #endif
			
  #if __ECU==ABS_ECU_1	
	if(EDC_ON == 1)
	{
		lespu1TCSAct = 1;
		lespu8TqIntvnTyp = 2;
	}
	else
	{
		lespu1TCSAct = 0;		
		lespu8TqIntvnTyp = 0;
	}		
  #endif		
  
	if(lespu8TqIntvnTyp != 1)
	{
		if(EDC_ON == 1)
		{
        	if((lespu1EngTqMaxExtRngVal==0) && (lesps16EngTqMaxExtRng <= -8480))
            {
            	lespu8TqIntvnTyp = 0;
            }							
            else
            {
            	;
            }			
  				#if (__CAR == GM_V275) || (__CAR == GM_TAHOE) || (__CAR == GM_C100)||(__CAR == GM_J300)||(__CAR == GM_LAMBDA)||(__CAR==GM_MALIBU)||(__CAR==GM_SILVERADO)           
  			lespu16TorqReqVal = (UINT)(((MSR_cal_torq + 2000) * 4) / 10);
  				#else	
			lespu16TorqReqVal = (UINT)(((MSR_cal_torq + 8480) * 2) / 10);
				#endif
		}
		else
		{
			;
		}
	}
	else
	{
		;
	}

    if(lespu8TqIntvnTyp==0)
    {
			#if (__CAR == GM_V275) || (__CAR == GM_TAHOE) || (__CAR == GM_C100)||(__CAR == GM_J300)||(__CAR == GM_LAMBDA)||(__CAR==GM_MALIBU)||(__CAR==GM_SILVERADO)                	
    	lespu16TorqReqVal = 800;
    		#else
    	lespu16TorqReqVal = 1696;
    		#endif	
    }
    else
    {
    	;
    }
    	#if __GM_FailM
	if((ldedcu1EDCFailMode==1)&&(lespu8TqIntvnTyp != 1))
	{    	
    	lespu1TCSAct = 0;
		lespu8TqIntvnTyp = 0; 
			#if (__CAR == GM_V275) || (__CAR == GM_TAHOE) || (__CAR == GM_C100)||(__CAR == GM_J300)||(__CAR == GM_LAMBDA)||(__CAR==GM_MALIBU)||(__CAR==GM_SILVERADO)                			
    	lespu16TorqReqVal = 800;
    		#else
    	lespu16TorqReqVal = 1696;
    		#endif	
    }
    else
    {
    	;
    }			
    	#endif	
}
	#elif (__CAR==FIAT_PUNTO)
	
void LAEDC_vControlEngTorqForDEMO(void)
{
	  #if (__CAR == FIAT_PUNTO)	/* TAG3 */
  /* Modified BY HJH for MSR fuction (040826) */
  
	if(lespu1TCS_TCS_REQ==0)		/* TCS_REQ disable */
    { 
			#if __EDC_CONV_ABS_TORQ
		LAEDC_vConvAbsToCanTorq();
			#endif    	  	

		if(EDC_ON==1)								    /* MSR Control (040826)  */
		{
	   		lespu1TCS_MSR_C_REQ = 1;    				/* set MSR_C_REQ */	

            EDC_cmd = lesps16TCS_TQI_MSR;	

		}

		else 
		{											    /* No EMS Control */
		    EDC_cmd=MSR_cal_torq=drive_torq;   	  
            lespu1TCS_MSR_C_REQ = 0;     				/* reset MSR_C_REQ        */        
            lesps16TCS_TQI_MSR = 0;                     /* clear TQI_MSR		  */
 		}
 	}
 	else
 	{
 		EDC_cmd=MSR_cal_torq=drive_torq;
 	}	
  #endif 	/* FIAT_PUNTO */
}
    #endif	
  #endif       /* TAG2 */

#endif		/* TAG1 */

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LAEDCCallActCan
	#include "Mdyn_autosar.h"               
#endif

