
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDESPCallDetection
	#include "Mdyn_autosar.h"
#endif
#include "LVarHead.h"
#include"LDESPEstDiscTemp.h"
#include"LDRTACallDetection.h"
#include"LSESPFilterEspSensor.h"
#include"LSESPCalSensorOffset.h"

void  LDESP_vIdentifyMiniWheel(void);
void 	LDRTA_vCallDetection(void);

  #if	__TCS
void 	MINI_TCS(void);                    // Mini wheel detection
  #endif

#if	__VDC
   #if 	__SPARE_WHEEL
void 	DETECT_MINI_SPARE(void);
   #endif
#endif

//  #if	__BTC
//void	BTCS_DISC_TEMP_EST(void);
//  #endif


#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
void LSESP_vCompWheelSlip(struct ESC_STRUCT *WL_ESC);
void LSESP_vCalWheelSpeedrate(struct ESC_STRUCT *WL_ESC);
void LDESP_vUpdateState(void);
#else
void LSESP_vCompWheelSlip(void);
#endif


void LDESP_vRecognizeVehicleState(void);
void LDESP_vObserveRoad(void);

#if __MGH40_Beta_Estimation
void LDESP_vEstBeta(void);
#endif

#if !SIM_MATLAB
void LDESPPartial_vDetectCircuitFail(void);
#endif // !SIM_MATLAB


void LDESP_vCallDetection(void);


#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
void LDESP_vUpdateState(void)
{	

  FL_ESC.wvref = wvref_fl;
  FR_ESC.wvref = wvref_fr;
  RL_ESC.wvref = wvref_rl;
  RR_ESC.wvref = wvref_rr;
 
  FL_ESC.vrad_crt = FL.vrad_crt;
	FR_ESC.vrad_crt = FR.vrad_crt;
	RL_ESC.vrad_crt = RL.vrad_crt;
	RR_ESC.vrad_crt = RR.vrad_crt;

  FL_ESC.vrad_crt_resol_change = FL.vrad_crt_resol_change;
  FR_ESC.vrad_crt_resol_change = FR.vrad_crt_resol_change;
  RL_ESC.vrad_crt_resol_change = RL.vrad_crt_resol_change;
  RR_ESC.vrad_crt_resol_change = RR.vrad_crt_resol_change;
  
  FL_ESC.wvref_resol_change = FL.wvref_resol_change;
	FR_ESC.wvref_resol_change = FR.wvref_resol_change;
	RL_ESC.wvref_resol_change = RL.wvref_resol_change;
	RR_ESC.wvref_resol_change = RR.wvref_resol_change;
	
	FL_ESC.arad = FL.arad;
	FR_ESC.arad = FR.arad;
	RL_ESC.arad = RL.arad;
	RR_ESC.arad = RR.arad;

	FL_ESC_COMB.FLAG_ACT_COMB_CNTR = FLAG_ACT_COMB_CNTR_FL;
    FR_ESC_COMB.FLAG_ACT_COMB_CNTR = FLAG_ACT_COMB_CNTR_FR;
    RL_ESC_COMB.FLAG_ACT_COMB_CNTR = FLAG_ACT_COMB_CNTR_RL;
    RR_ESC_COMB.FLAG_ACT_COMB_CNTR = FLAG_ACT_COMB_CNTR_RR;

}
#endif

void LDESP_vCallDetection(void)
{
	#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
	   lsesps16AHBGEN3mpressOld = lsesps16MpressByAHBgen3Old;
	
     if(lsespu1BrkAppSenInvalid ==0)
     {
     	  lsesps16AHBGEN3mpress = lsesps16EstBrkPressByBrkPedalF;
     	  lsespu1AHBGEN3MpresBrkOn = lsespu1PedalBrakeOn;
     }
     else if(lsespu1MpresByAHBgen3Invalid ==0)
     {
     	  lsesps16AHBGEN3mpress = lsesps16MpressByAHBgen3;
     	  lsespu1AHBGEN3MpresBrkOn = lsespu1MpresBrkOnByAHBgen3;
     }
     else
     {
     	  lsesps16AHBGEN3mpress = 0;
     	  lsespu1AHBGEN3MpresBrkOn = 0;
     }
  #endif

	#if	__VDC	
    #if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
      LSESP_vCompWheelSlip(&FL_ESC);
    	LSESP_vCompWheelSlip(&FR_ESC);
    	LSESP_vCompWheelSlip(&RL_ESC);
    	LSESP_vCompWheelSlip(&RR_ESC);
    	LSESP_vCalWheelSpeedrate(&FL_ESC);
 	    LSESP_vCalWheelSpeedrate(&FR_ESC);
 	    LSESP_vCalWheelSpeedrate(&RL_ESC);
 	    LSESP_vCalWheelSpeedrate(&RR_ESC);
    #else
    	LSESP_vCompWheelSlip();
 	    LSESP_vCalWheelSpeedrate();
  	#endif
  #endif

	#if __MGH40_Beta_Estimation
    	LDESP_vEstBeta();
  #endif
  
 #if	__VDC	 	
  LDESP_vIdentifyMiniWheel();
	LDESP_vRecognizeVehicleState();
	LDESP_vObserveRoad();
 #endif
	
	#if	 (__BTEM==ENABLE)
	LDESPEstDiscTempMain();
	#endif
	
		
	#if !SIM_MATLAB 
	  #if	__VDC
	LDESPPartial_vDetectCircuitFail();
	  #endif	
    #endif	  

}
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDESPCallDetection
	#include "Mdyn_autosar.h"
#endif
