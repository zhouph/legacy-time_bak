#ifndef __LCFBCCALLCONTROL_H__
#define __LCFBCCALLCONTROL_H__
/*Includes *********************************************************************/
#include "LVarHead.h"

#if	__FBC
/*Global MACRO CONSTANT Definition *********************************************/

    #define		BRAKE_DISK_WARNING_TEMPERATURE	12800
	/* deg	0.03125	0	FADE Warning temperature (브레이크 디스크 온도가 이 온도 이상이면 FADE 경고 for GM) */
	/* default 12800=400 degrees, for demo 3200=100 degrees, for test 0 degree. */

	#define		FBC_STANDBY					0
	#define     FBC_APPLY					1
	#define     FBC_HOLD					2
	#define		FBC_DUMP					3

	
/*Global MACRO FUNCTION Definition *********************************************/


/*Global Type Declaration ******************************************************/
/* FBCFLAG00*/
#define	FBC_WORK						FBCF00.bit0
#define	not_used_bit_FBC00_1			FBCF00.bit1
#define	FBC_ON							FBCF00.bit2
#define	FBC_LOW_MU						FBCF00.bit3
#define	not_used_bit_FBC00_4			FBCF00.bit4
#define	FBC_MPS_ERROR					FBCF00.bit5
#define	FBC_BIG_SLOP					FBCF00.bit6
#define	not_used_bit_FBC00_7			FBCF00.bit7



/*Global Extern Variable Declaration *******************************************/
extern U8_BIT_STRUCT_t FBCF00;
extern uint8_t	FBC_dct_cnt;
extern int16_t	FBC_decel_sum, FBC_mode;
extern uint8_t	FBC_ACTIVE;	

#if defined(__H2DCL)
extern CC_HDC_FLG_t CF10;
#endif

/*Global Extern Functions Declaration ******************************************/


#endif

#endif
