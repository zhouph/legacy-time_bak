 
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LSESPCheckEspError
	#include "Mdyn_autosar.h"
#endif
#include "LVarHead.h"
#include "LCallMain.h"

#if __TVBB
#include "LCTVBBCallControl.h"
#include "LCESPCallControl.h"
#include "LDESPDetectVehicleStatus.h"
#endif 

#include "hm_logic_var.h" 
#include "LSESPCalSlipRatio.h"  

    
#if __TVBB
void LDTVBB_vCallDetection(void);
static void LDTVBB_vDetectDriveLineConfig(void);
static void LDTVBB_vDetectDriverIntent(void);
static void LDTVBB_vDetectAccPdlStat4(void);
static void LDTVBB_vDetectTurnIntent4(void);
static void LDTVBB_vDetectInhibition(void);
static void LDTVBB_vCalEnterExitThreshold(void);
static void LDTVBB_vCalSlipRatioOfCtrlWhl(void);
#endif	/* #if __TVBB */
 
/* Flag Declaration - Start */
#if __TVBB
struct  U8_BIT_STRUCT_t TVBB00;
struct  U8_BIT_STRUCT_t TVBB01;
struct  U8_BIT_STRUCT_t TVBB02; 
#endif		/* #if __TVBB */
/* Flag Declaration - End */ 

/* Variable Declaration - Start */
#if __TVBB
 
uint8_t	ldtvbbu8InhibitID;
uint8_t	ldtvbbu8AccelPedalPushCnt;
uint8_t	ldtvbbu8AccelPedalReleaseCnt;
uint8_t	ldtvbbu8After2WHUScount;
int8_t	ldtvbbs8TurnIntentDetectCnt, ldtvbbs8TurnIntentExitCnt;
int16_t     ldtvbbs16WstrAtDirChange;
int16_t		ldtvbbs16SlipRatioOfCtlWhl;
int16_t     ldtvbbs16EnterSpeedLimit;
int16_t     ldtvbbs16InhibitCtrlWheelSlip;
int16_t     ldtvbbs16EnterDelYawThres, ldtvbbs16EnterSlipThres;

#endif		/* #if __TVBB */

#if __TVBB
void LDTVBB_vCallDetection(void)
{
	LDTVBB_vDetectDriverIntent();
	LDTVBB_vCalSlipRatioOfCtrlWhl();
	LDTVBB_vDetectInhibition();
	LDTVBB_vCalEnterExitThreshold();
}

static void LDTVBB_vDetectDriverIntent(void)
{
	LDTVBB_vDetectDriveLineConfig();
	LDTVBB_vDetectAccPdlStat4();
	LDTVBB_vDetectTurnIntent4();
}

static void LDTVBB_vDetectDriveLineConfig(void)
{
      #if ((__CAR_MAKER==GM_KOREA)||(__CAR_MAKER==TEST_CAR))
    ldtvbbu1DriveLineEnable = 1;
        #if (__4WD_VARIANT_CODE == ENABLE)
          #if (__DRIVE_TYPE == FRONT_WHEEL_DRV)
    //if ((lsu8DrvMode == DM_2WD)||(lsu8DrvMode == DM_2H))
    //{
        ldtvbbu1FrontWhlCtl = 1;
    //}
    //else
    //{
    //    ldtvbbu1FrontWhlCtl = 0;
    //}
          #else
    ldtvbbu1FrontWhlCtl = 0;  
          #endif
        #else
          #if ((__4WD == ENABLE) || (__DRIVE_TYPE == REAR_WHEEL_DRV))
    ldtvbbu1FrontWhlCtl = 0;  
          #else
    ldtvbbu1FrontWhlCtl = 1;
          #endif  
        #endif
      #else /*HMC_KMC*/
        #if (__TVBB_2WD_TEST == ENABLE)
    ldtvbbu1DriveLineEnable = 1;
          #if ((__4WD == ENABLE) || (__DRIVE_TYPE == REAR_WHEEL_DRV))
    ldtvbbu1FrontWhlCtl = 0;
          #else
    ldtvbbu1FrontWhlCtl = 1;
          #endif  
        #else
          #if (__4WD_VARIANT_CODE == ENABLE)
    if ((lsu8DrvMode == DM_2WD)||(lsu8DrvMode == DM_2H))
    {
        ldtvbbu1DriveLineEnable = 0;
        ldtvbbu1FrontWhlCtl = 0;
    }
    else
    {
        if (lespu1AWD_4WD_ERR == 1)
        {
            ldtvbbu1DriveLineEnable = 0;
            ldtvbbu1FrontWhlCtl = 0;
        }
        else
        {
	        ldtvbbu1DriveLineEnable = 1;
    	    ldtvbbu1FrontWhlCtl = 0;
    	}
    }
          #else
            #if (__4WD == ENABLE)
    if (lespu1AWD_4WD_ERR == 1)
    {
        ldtvbbu1DriveLineEnable = 0;
        ldtvbbu1FrontWhlCtl = 0;
    }
    else
    {              
        ldtvbbu1DriveLineEnable = 1;
        ldtvbbu1FrontWhlCtl = 0;  
    }
            #elif (__DRIVE_TYPE == REAR_WHEEL_DRV)
    ldtvbbu1DriveLineEnable = 1;
    ldtvbbu1FrontWhlCtl = 0;  
            #else
    ldtvbbu1DriveLineEnable = 0;
    ldtvbbu1FrontWhlCtl = 0;  
            #endif
          #endif
        #endif 
      #endif   
}

static void LDTVBB_vDetectAccPdlStat4(void)
{
    if ((AUTO_TM==1)&&(gs_pos!=0)&&(gs_pos!=7))
    {
	    if (mtp>=U8_TVBB_APS_TH)
	    {
	    	ldtvbbu8AccelPedalPushCnt = ldtvbbu8AccelPedalPushCnt + 1;
	    	ldtvbbu8AccelPedalReleaseCnt = 0;
	    }
	    else
	    {
	    	ldtvbbu8AccelPedalPushCnt = 0;
	    	ldtvbbu8AccelPedalReleaseCnt = ldtvbbu8AccelPedalReleaseCnt + 1;		
	    }
	}
	else if ((AUTO_TM == 0)&&(In_gear_flag == 1))
	{
	    if (mtp>=U8_TVBB_APS_TH)
	    {
	    	ldtvbbu8AccelPedalPushCnt = ldtvbbu8AccelPedalPushCnt + 1;
	    	ldtvbbu8AccelPedalReleaseCnt = 0;
	    }
	    else
	    {
	    	ldtvbbu8AccelPedalPushCnt = 0;
	    	ldtvbbu8AccelPedalReleaseCnt = ldtvbbu8AccelPedalReleaseCnt + 1;		
	    }
	}    
	else
	{
		ldtvbbu8AccelPedalPushCnt = 0;
		ldtvbbu8AccelPedalReleaseCnt = ldtvbbu8AccelPedalReleaseCnt + 1;		
	}
	/* Counter Limitation */
	if (ldtvbbu8AccelPedalPushCnt > 200)
	{
	    ldtvbbu8AccelPedalPushCnt = 200;
	}
	else
	{
	    ;
	}
	if (ldtvbbu8AccelPedalReleaseCnt > 200)
	{
	    ldtvbbu8AccelPedalReleaseCnt = 200;
	}
	else
	{
	    ;
	}
	
	/* Set & Reset Flag */
	if (ldtvbbu8AccelPedalPushCnt >= L_U8_TIME_10MSLOOP_50MS)
	{
		ldtvbbu1AccelPedalPushFlag = 1;
		ldtvbbu1AccelPedalReleaseFlag = 0;
	}
	else if (ldtvbbu8AccelPedalReleaseCnt >= L_U8_TIME_10MSLOOP_50MS)
	{
		ldtvbbu1AccelPedalPushFlag = 0;
		ldtvbbu1AccelPedalReleaseFlag = 1;		
	}
	else
	{
		ldtvbbu1AccelPedalPushFlag = 0;
		ldtvbbu1AccelPedalReleaseFlag = 0;
	}
}

static void LDTVBB_vDetectTurnIntent4(void)
{
	tvbb_tempW0 = McrAbs(nor_alat);
	
	if (ldtvbbu1TurnIntentDetectFlag == 0)
	{
	    if (tvbb_tempW0 >= S16_TVBB_AYC_TH)/*&&(McrAbs(rf) >= S16_TVBB_YAWC_TH))*/
	    {
	        if (ldtvbbs8TurnIntentDetectCnt < S8_TVBB_TURN_INTENT_DCT_TIME)
	        {
	            ldtvbbs8TurnIntentDetectCnt = ldtvbbs8TurnIntentDetectCnt + 1;
	        }
	        else
	        {
	            ldtvbbs8TurnIntentDetectCnt = S8_TVBB_TURN_INTENT_DCT_TIME;
	            ldtvbbu1TurnIntentDetectFlag = 1;
	        }
	    }
	    else
	    {
	        ;
	    }
	    ldtvbbs8TurnIntentExitCnt = 0;
	}
	else
	{
	    if (tvbb_tempW0 < S16_TVBB_AYC_TH) /*|| (McrAbs(rf) < S16_TVBB_YAWC_TH))*/
	    {
	        if (ldtvbbs8TurnIntentExitCnt < S8_TVBB_TURN_INTENT_DCT_TIME)
	        {
	            ldtvbbs8TurnIntentExitCnt = ldtvbbs8TurnIntentExitCnt + 1;
	        }
	        else
	        {
	            ldtvbbs8TurnIntentExitCnt = S8_TVBB_TURN_INTENT_DCT_TIME;
	            ldtvbbu1TurnIntentDetectFlag = 0;
	        }
	    }
	    else
	    {
	        ;
	    }
	    ldtvbbs8TurnIntentDetectCnt = 0;
	}
}

static void LDTVBB_vDetectInhibition(void)
{
    esp_tempW0 = LCESP_s16IInter3Point(vref5, S16_TVBB_ENTER_TH_REF_LSP, S16_TVBB_ENTER_ALATM_TH_LSP,
                                              S16_TVBB_ENTER_TH_REF_MSP, S16_TVBB_ENTER_ALATM_TH_MSP,
                                              S16_TVBB_ENTER_TH_REF_HSP, S16_TVBB_ENTER_ALATM_TH_HSP);
    if (lctvbbu1TVBB_ON == 1)
    {
        esp_tempW0 = (esp_tempW0 - LAT_0G05G);
    }
    else
    {
        ;
    }
    
    esp_tempW1 = LCESP_s16IInter3Point(vref5, S16_TVBB_ENTER_TH_REF_LSP, (int16_t)S8_TVBB_INHIBIT_WH_SLIP_LSP, 
                                              S16_TVBB_ENTER_TH_REF_MSP, (int16_t)S8_TVBB_INHIBIT_WH_SLIP_MSP,
                                              S16_TVBB_ENTER_TH_REF_HSP, (int16_t)S8_TVBB_INHIBIT_WH_SLIP_HSP); 
                                              
    ldtvbbs16InhibitCtrlWheelSlip = esp_tempW1 * 10;                                          
                                              
    if (ldtvbbs16InhibitCtrlWheelSlip >= -WHEEL_SLIP_4)
    {
        ldtvbbs16InhibitCtrlWheelSlip = -WHEEL_SLIP_4;
    }
    else
    {
        ;
    }                                             
    
    if (lctvbbu1TVBB_ON == 1)
    {
        if (((ldtvbbu1FrontWhlCtl == 0) && ((TMP_ERROR_rl == 1) || (TMP_ERROR_rr == 1)))
          ||((ldtvbbu1FrontWhlCtl == 1) && ((TMP_ERROR_fl == 1) || (TMP_ERROR_fr == 1)))) 
        {
            tc_error_flg = 1;
        }
        else
        {
            ;
        }
    }
    else
    {
        ;
    }
            
    if ((lcu1US2WHControlFlag_RL == 1) || (lcu1US2WHControlFlag_RR == 1))
    {
        ldtvbbu8After2WHUScount = 0;
    }
    else
    {
        if (ldtvbbu8After2WHUScount < 21)
        {
            ldtvbbu8After2WHUScount = ldtvbbu8After2WHUScount + 1;
        }
        else
        {
            ldtvbbu8After2WHUScount = 21;
        }
    }
            
  #if __TVBB_OFF_SWITCH
	if		(/*(lcu1HdcSwitchFlg==1)||*/(ESP_ERROR_FLG==1)||(vdc_on_flg==0)||(VDC_DISABLE_FLG==1)||(tc_error_flg==1)||(tcs_error_flg==1)||(fu1ESCDisabledBySW==1))	
  #else
	if		(/*(cu1Sw2Onflg==0)||*/(ESP_ERROR_FLG==1)||(vdc_on_flg==0)||(VDC_DISABLE_FLG==1)||(tc_error_flg==1)||(tcs_error_flg==1)||(fu1ESCDisabledBySW==1))  
  #endif				
	{	/* Error Condition */
		ldtvbbu1InhibitFlag = 1;
		ldtvbbu8InhibitID = 1;
	}
	  #if (__TVBB_USC_COMB_CONTROL == ENABLE)
    else if (
    /*
            ((ldtvbbu1FrontWhlCtl == 1)&&(((SLIP_CONTROL_NEED_FL == 1)&&(lcu1US2WHControlFlag_RL==0)&&(lcu1US2WHFadeout_RL==0)) || ((SLIP_CONTROL_NEED_FR == 1)&&(lcu1US2WHControlFlag_RR==0)&&(lcu1US2WHFadeout_RR==0)) || (BTCS_ON==1)))
    */
            ((ldtvbbu1FrontWhlCtl == 1)&&((SLIP_CONTROL_NEED_FL == 1) || (SLIP_CONTROL_NEED_FR == 1) || (BTCS_ON==1)))
            || ((ldtvbbu1FrontWhlCtl == 0) 
                #if (__TVBB_IN_EC == ENABLE)
             && ((YAW_CDC_WORK==1) || /*(ESP_BRAKE_CONTROL==1) ||*/ (BTCS_ON==1)))   
                #else
             && ((YAW_CDC_WORK==1) || (ESP_BRAKE_CONTROL==1) || (BTCS_ON==1) || (ESP_TCS_ON==1) || (ETCS_ON == 1)))
                #endif
            )
	  #else	
	    #if (__TVBB_IN_EC == ENABLE)
	else if ( (YAW_CDC_WORK==1) || (ESP_BRAKE_CONTROL==1) || (BTCS_ON == 1) )
	    #else
	else if ( (YAW_CDC_WORK==1) || (ESP_BRAKE_CONTROL==1) || (ESP_TCS_ON==1)||(ETCS_ON == 1)||(BTCS_ON==1))
	    #endif
      #endif  

	{	/* In Control */ 
		ldtvbbu1InhibitFlag = 1;
		ldtvbbu8InhibitID = 2;		
	}
	else if	(MPRESS_BRAKE_ON==1)
	{	/* Braking Condtion : MP 및 BLS Error시에는 ldtvbbu8InhibitID 1 로 제어 금지 */
		ldtvbbu1InhibitFlag = 1;
		ldtvbbu8InhibitID = 3;		
	}
	else if (ldtvbbu1AccelPedalReleaseFlag==1)
	{	/* Accel Pedal Reelase */
		ldtvbbu1InhibitFlag = 1;
		ldtvbbu8InhibitID = 4;
	}
	else if (vref5 <= S16_TVBB_SPD_MIN)
	{	/* Speed Condition */
		ldtvbbu1InhibitFlag = 1;
		ldtvbbu8InhibitID = 5;		
	}
	else if (det_alatm <= esp_tempW0)  
	{	/* Lateral Acceleration Condition */
		ldtvbbu1InhibitFlag = 1;
		ldtvbbu8InhibitID = 6;		
	}	
		
	//else if ( ldtvbbs16SlipRatioOfCtlWhl <= -S16_TVBB_INHIBIT_CTRL_WHEEL_SLIP )	    /* Wheel_Slip Control */ 
	else if ( ldtvbbs16SlipRatioOfCtlWhl <= ldtvbbs16InhibitCtrlWheelSlip )	    /* Wheel_Slip Control */ 
	{	
		ldtvbbu1InhibitFlag = 1;
		ldtvbbu8InhibitID = 7;		
	}		
	else if ((delta_yaw > S16_TVBB_DEL_YAW_INHIBIT_TH_ENTER) && (lcu1espActive3rdFlg == 0))
	{
	    if (lctvbbu1TVBB_ON == 0)
	    {
	        ldtvbbu1InhibitFlag = 1;  
	        ldtvbbu8InhibitID = 8;
	    }
	    else
	    {
	        if (delta_yaw > S16_TVBB_DEL_YAW_INHIBIT_TH_EXIT)
	        {
	            ldtvbbu1InhibitFlag = 1;  
	            ldtvbbu8InhibitID = 8;
	        }
	        else
	        {
	            ;
	        }
	    }
	}
	else if (ldtvbbu1DriveLineEnable == 0)
	{
	    ldtvbbu1InhibitFlag = 1;
	    ldtvbbu8InhibitID = 9;
	}
	else
	{
		ldtvbbu1InhibitFlag = 0; 
		ldtvbbu8InhibitID = 0;
	}
}

static void LDTVBB_vCalEnterExitThreshold(void)
{
    if (lctvbbu1TVBB_ON == 1)
    {
        if (lctvbbu1Direction == 1)
        {
            if (ldtvbbu1WstrDotDirChange == 0)
            {   
                if (wstr_dot < 0)
                {
                    ldtvbbu1WstrDotDirChange = 1;
                    ldtvbbs16WstrAtDirChange = wstr;
                }
                else
                {
                    ldtvbbu1WstrDotDirChange = 0;
                }
            }
            else
            {
                if (wstr_dot > 0)
                {
                    ldtvbbu1WstrDotDirChange = 0;
                }
                else if ((ldtvbbs16WstrAtDirChange-wstr) > S16_TVBB_WSTR_DIR_CHANGE_THRES)
                {
                    ldtvbbu1ExitByWstrDot = 1;
                }
                else
                {
                    ldtvbbu1ExitByWstrDot = 0;
                }
            }
        }
        else if (lctvbbu1Direction == 0)
        {
            if (ldtvbbu1WstrDotDirChange == 0)
            {   
                if (wstr_dot > 0)
                {
                    ldtvbbu1WstrDotDirChange = 1;
                    ldtvbbs16WstrAtDirChange = wstr;
                }
                else
                {
                    ldtvbbu1WstrDotDirChange = 0;
                }
            }
            else
            {
                if (wstr_dot < 0)
                {
                    ldtvbbu1WstrDotDirChange = 0;
                }
                else if ((wstr-ldtvbbs16WstrAtDirChange) > S16_TVBB_WSTR_DIR_CHANGE_THRES)
                {
                    ldtvbbu1ExitByWstrDot = 1;
                }
                else
                {
                    ldtvbbu1ExitByWstrDot = 0;
                }
            }
        }
        else
        {
            ldtvbbu1ExitByWstrDot = 0;
        }
    }
    else
    {
        ldtvbbu1ExitByWstrDot = 0;
    }
    
    ldtvbbs16EnterSpeedLimit = LCESP_s16FindRoadDependatGain(det_alatm, S16_TVBB_ENTER_HSPD_LIMIT_HMU, S16_TVBB_ENTER_HSPD_LIMIT_MMU, S16_TVBB_ENTER_HSPD_LIMIT_LMU);        
    
    ldtvbbs16EnterDelYawThres = LCESP_s16IInter3Point(vref5, S16_TVBB_ENTER_TH_REF_LSP, S16_TVBB_DELTA_YAW_TH_LSP,
                                                             S16_TVBB_ENTER_TH_REF_MSP, S16_TVBB_DELTA_YAW_TH_MSP,
                                                             S16_TVBB_ENTER_TH_REF_HSP, S16_TVBB_DELTA_YAW_TH_HSP);
    
    #if __TVBB_DYNAMIC_THRES
    if( yaw_out > 0 ) 
	{
	    esp_tempW0 = ay_vx_dot_sig_plus;
	}
	else
	{
	    esp_tempW0 = ay_vx_dot_sig_minus;
	}
                                                          
    esp_tempW2 = LCESP_s16IInter3Point(esp_tempW0, S16_TVBB_TH_REF_AY_VX_DOT_HM_1, S16_TVBB_TH_AY_VX_DOT_GAIN_HM_1,
                                                   S16_TVBB_TH_REF_AY_VX_DOT_HM_2, S16_TVBB_TH_AY_VX_DOT_GAIN_HM_2,
                                                   S16_TVBB_TH_REF_AY_VX_DOT_HM_3, S16_TVBB_TH_AY_VX_DOT_GAIN_HM_3);
                                                   
    esp_tempW3 = LCESP_s16IInter3Point(esp_tempW0, S16_TVBB_TH_REF_AY_VX_DOT_MM_1, S16_TVBB_TH_AY_VX_DOT_GAIN_MM_1,
                                                   S16_TVBB_TH_REF_AY_VX_DOT_MM_2, S16_TVBB_TH_AY_VX_DOT_GAIN_MM_2,
                                                   S16_TVBB_TH_REF_AY_VX_DOT_MM_3, S16_TVBB_TH_AY_VX_DOT_GAIN_MM_3);                                               
                                                   
    esp_tempW4 = LCESP_s16IInter3Point(esp_tempW0, S16_TVBB_TH_REF_AY_VX_DOT_LM_1, S16_TVBB_TH_AY_VX_DOT_GAIN_LM_1,
                                                   S16_TVBB_TH_REF_AY_VX_DOT_LM_2, S16_TVBB_TH_AY_VX_DOT_GAIN_LM_2,
                                                   S16_TVBB_TH_REF_AY_VX_DOT_LM_3, S16_TVBB_TH_AY_VX_DOT_GAIN_LM_3);
                                                   
    esp_tempW1 = LCESP_s16FindRoadDependatGain(det_alatm, esp_tempW2, esp_tempW3, esp_tempW4);                                                                                               
                                                   
    ldtvbbs16EnterDelYawThres = (int16_t)((((int32_t)ldtvbbs16EnterDelYawThres)*esp_tempW1)/100 );
    #endif
                                                          
    if (ldtvbbs16EnterDelYawThres >= -YAW_2G5DEG)
    {
        ldtvbbs16EnterDelYawThres = -YAW_2G5DEG;
    }
    else
    {
        ;
    }
    esp_tempW0 = ((int16_t)U8_TVBB_ENTER_SPIN_REF_ALATM_L)*10;
    esp_tempW1 = ((int16_t)U8_TVBB_ENTER_SPIN_REF_ALATM_M)*10;
    esp_tempW2 = ((int16_t)U8_TVBB_ENTER_SPIN_REF_ALATM_H)*10;
    
    esp_tempW3 = ((int16_t)S8_TVBB_ENTER_WHEEL_SPIN_L)*10;
    esp_tempW4 = ((int16_t)S8_TVBB_ENTER_WHEEL_SPIN_M)*10;
    esp_tempW5 = ((int16_t)S8_TVBB_ENTER_WHEEL_SPIN_H)*10;    
                                    
    if (ldtvbbu8After2WHUScount > 0)
		{
        ldtvbbs16EnterSlipThres = ldtvbbs16InhibitCtrlWheelSlip;
	}
	else
	{
        ldtvbbs16EnterSlipThres = LCESP_s16IInter3Point(det_alatm, esp_tempW0, esp_tempW3, 
                                                                   esp_tempW1, esp_tempW4,
                                                                   esp_tempW2, esp_tempW5);
	}
}

static void LDTVBB_vCalSlipRatioOfCtrlWhl(void)
{
    if (ldtvbbu1FrontWhlCtl == 1)
    {
	    if(nor_alat>=LAT_0G1G)  // lctvbbu1LeftWhlCtl
	    {
	    	ldtvbbs16SlipRatioOfCtlWhl = slip_m_fl ;
	    }
	    else if (nor_alat<=(-LAT_0G1G))   // lctvbbu1RightWhlCtl
	    {
	    	ldtvbbs16SlipRatioOfCtlWhl = slip_m_fr ;
	    }
	    else
	    {	/* Not In Control */
	    	ldtvbbs16SlipRatioOfCtlWhl = 0;
	    }	
	}
	else
	{
	    if(nor_alat>=LAT_0G1G)  // lctvbbu1LeftWhlCtl
	    {
	    	ldtvbbs16SlipRatioOfCtlWhl = slip_m_rl ;
	    }
	    else if (nor_alat<=(-LAT_0G1G))   // lctvbbu1RightWhlCtl
	    {
	    	ldtvbbs16SlipRatioOfCtlWhl = slip_m_rr ;
	    }
	    else
	    {	/* Not In Control */
	    	ldtvbbs16SlipRatioOfCtlWhl = 0;
	    }	
	}
}
#endif	/* #if __TVBB */
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LSESPCheckEspError
	#include "Mdyn_autosar.h"
#endif
    
