#ifndef __LABDWCALLACTHW_H__
#define __LABDWCALLACTHW_H__

/*includes********************************************************************/
#include "LVarHead.h"
#include "LCBDWCallControl.h"


/*Global Type Declaration ****************************************************/



/*Global Extern Variable  Declaration*****************************************/
extern U8_BIT_STRUCT_t BDWC0, BDWC1, BDWC2, BDWC3;

extern int16_t bdw_msc_target_vol;

/*Global Extern Functions  Declaration****************************************/

extern void LCMSC_vSetBDWTargetVoltage(void);

#endif
