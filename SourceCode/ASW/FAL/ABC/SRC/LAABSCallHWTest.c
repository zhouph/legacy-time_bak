/******************************************************************************
* Project Name: Anti-lock Braking System
* File: LAABSCallHWTest.C
* Description: Generation PWM duty for Wheel No Valve Characteristic Test
* Date: March. 28. 2008
******************************************************************************/

/* Includes ******************************************************************/

/* AUTOSAR --------------------------*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START                       
	#define idx_FILE	idx_CL_LAABSCallHWTest
	#include "Mdyn_autosar.h"               
#endif
/* AUTOSAR --------------------------*/

#include "LAABSCallHWTest.h"
#include "LAABSCallMotorSpeedControl.h"
#include "Hardware_Control.h"
#include "LACallMain.h"



/* Local Definiton  **********************************************************/
/****************************************************
* Edit here for ESPLUS valve test
****************************************************/
#if __HW_TEST_MODE

/*****************************************************************************************/         
/* CALIBRATION                                                                           */
/*****************************************************************************************/         
#define S8_HW_TEST_MODE_SELECTION           (int8_t)S16_Decel_Gain_BFAFZOK_100_150_F

/** 00~19 Wheel NO Test Mode ********************************************/          
#define INITIAL_DUTY_VS_RISE_RATE           1
    #define TEST_CASE_1_INIT_DUTY           (uint8_t)S16_Decel_Gain_BFAFZOK_10_20_F
    #define TEST_CASE_1_END_DUTY            (uint8_t)S16_Decel_Gain_BFAFZOK_20_40_F
    #define TEST_CASE_1_DUTY_DIFF           (uint8_t)S16_Decel_Gain_BFAFZOK_40_60_F
    #define TEST_CASE_1_HOLD_TIME           ((uint16_t)(S16_Decel_Gain_BFAFZOK_60_100_F)*T_100_MS )
    #define TEST_CASE_1_DUTY_TIME           ((uint16_t)(S16_Decel_Gain_BFAFZOK_Ab_150_F)*T_100_MS )
    #define TEST_CASE_1_INTERVAL_TIME       ((uint16_t)(S16_Decel_Gain_BFAFZOK_B_10_F)*T_100_MS   )

    #define TEST_CASE_1_SELECT_HV           (S16_Slip_Gain_BFAFZOK_100_150_F) /* FL:3 FR:4 RL:1 RR:2 All:7*/
    #define TEST_CASE_1_DUTY_ON_TIME_10MS   (S16_Slip_Gain_BFAFZOK_10_20_F )  /* time[msec] : PARAM_10MS * 10msec */
    #define TEST_CASE_1_DUTY_HOLD_TIME_10MS (S16_Slip_Gain_BFAFZOK_20_40_F ) /* time[msec] : PARAM_10MS * 10msec */

    #define TEST_CASE_1_RISE_N_HOLD_TIME    (TEST_CASE_1_DUTY_ON_TIME_10MS+TEST_CASE_1_DUTY_HOLD_TIME_10MS)
   	#define TEST_CASE_1_TIME                (TEST_CASE_1_HOLD_TIME+TEST_CASE_1_DUTY_TIME+TEST_CASE_1_INTERVAL_TIME)

#define DUTY_DEC_RATE_VS_RISE_PATTERN       2
    #define TEST_CASE_2_INIT_DUTY           (uint8_t)S16_Decel_Gain_BFAFZOK_10_20_F
    #define TEST_CASE_2_DUTY_DEC_RATE_MIN   (uint8_t)S16_Decel_Gain_BFAFZOK_20_40_F
    #define TEST_CASE_2_DUTY_DEC_RATE_MAX   (uint8_t)S16_Decel_Gain_BFAFZOK_40_60_F
    #define TEST_CASE_2_HOLD_TIME           ((uint16_t)(S16_Decel_Gain_BFAFZOK_60_100_F)*T_100_MS ) 
    #define TEST_CASE_2_DUTY_TIME           ((uint16_t)(S16_Decel_Gain_BFAFZOK_Ab_150_F)*T_100_MS ) 
    #define TEST_CASE_2_INTERVAL_TIME       ((uint16_t)(S16_Decel_Gain_BFAFZOK_B_10_F)*T_100_MS   )  

#define WHEEL_VALVE_UPDOWN_PATTERN          3
    #define TEST_CASE_3_INIT_DUTY           (uint8_t)S16_Decel_Gain_BFAFZOK_10_20_F
    #define TEST_CASE_3_END_DUTY            (uint8_t)S16_Decel_Gain_BFAFZOK_20_40_F
    #define TEST_CASE_3_DUTY_DIFF           (uint8_t)S16_Decel_Gain_BFAFZOK_40_60_F
    #define TEST_CASE_3_INIT_HOLD_TIME      ((uint16_t)(S16_Decel_Gain_BFAFZOK_60_100_F)*T_100_MS )
    #define TEST_CASE_3_DUTY_HOLD_TIME      ((uint16_t)(S16_Decel_Gain_BFAFZOK_Ab_150_F)*T_100_MS ) 
    #define TEST_CASE_3_PULSEDOWN_INTERVAL  ((uint16_t)(S16_Decel_Gain_BFAFZOK_B_10_F)*T_100_MS   )  
    #define TEST_CASE_3_PULSEDOWN_AMOUNT    (uint8_t)S16_Slip_Gain_BFAFZOK_Ab_150_F
/* s4500 TEST 01 : NO valve test */    
#define NO_DUTY_VS_RISE_RATE          		4 /* parameter_1 S16_Decel_Gain_BFAFZOK_100_150_F */
    #define TEST_CASE_4_NO_DUTY_ON_TIME		(uint16_t)(S16_Decel_Gain_BFAFZOK_40_60_F) /* parameter_4 */
    #define TEST_CASE_4_NO_DUTY_TARGET	    (uint16_t)(S16_Decel_Gain_BFAFZOK_B_10_F) /* parameter_7 */ 

/* s4500 TEST 02 : NC valve test */    
#define NC_OPEN_TIME_VS_DUMP_RATE           5 /* parameter_1 S16_Decel_Gain_BFAFZOK_100_150_F */
    #define TEST_CASE_5_NC_ON_TIME		    (uint16_t)S16_Decel_Gain_BFAFZOK_40_60_F /* parameter_4 */

#define NO_Full_ON_TIME_VS_RISE_RATE        6 /* refer INITIAL_DUTY_VS_RISE_RATE parameter */

/** 20~39 Motor Test Mode    ********************************************/          
#define TACHOMETER_RPM_TEST_MODE            21
    #define TACHOMETER_MOTOR_TAR_V_STEP     4
    #define TACHOMETER_MOTOR_TAR_V_HOLD_T   L_U16_TIME_5MSLOOP_2S
    #define MFC_20BAR_DUTY_for_TACHO_TEST   (uint8_t)S16_Slip_Gain_BFAFZOK_40_60_F
    
#define BEMF_VS_MOTOR_RPM                   22
    #define BEMF_TEST_STEP                  10
    #define BEMF_TEST_TOTAL_TIME            L_U16_TIME_5MSLOOP_5S
    #define BEMF_TEST_MOTOR_ON_TIME         T_1000_MS
    
#define MOTOR_TARGET_V_VS_STEADY_ERROR      23
    #define STEADY_ERROR_MOTOR_TAR_V_STEP   9
    #define STEADY_ERROR_MOTOR_TAR_V_HOLD_T L_U16_TIME_5MSLOOP_2S
    #define MFC_DUTY_for_MOTOR_STEADY_ERROR (uint8_t)S16_Slip_Gain_BFAFZOK_40_60_F
    
#define MOTOR_TARGET_V_VS_RESPONSE          24
    #define RESPONSE_MOTOR_TAR_V_HOLD_T     T_1000_MS
    #define MFC_DUTY_for_MOTOR_RESPONSE     (uint8_t)S16_Slip_Gain_BFAFZOK_40_60_F

#define DUTY_TARGET_V_RESPONSE              25
    #define INITIAL_DUTY_TAR_V_HOLD_T       L_U16_TIME_5MSLOOP_2S
    #define MFC_DUTY_for_MOTOR_RESPONSE     (uint8_t)S16_Slip_Gain_BFAFZOK_40_60_F

#define DUTY_ADAPTATION_TEST_MODE              26
    #define INITIAL_DUTY_TAR_V_HOLD_T       L_U16_TIME_5MSLOOP_2S
    #define MFC_DUTY_for_MOTOR_RESPONSE     (uint8_t)S16_Slip_Gain_BFAFZOK_40_60_F


/* s4500 TEST 05 : Motor/Pump test */    
#define MOTOR_TARGET_V_VS_RISE_RATE         	27 /* parameter_1 S16_Decel_Gain_BFAFZOK_100_150_F */
    #define TEST_CASE_25_MOTOR_ON_TIME			(uint16_t)S16_Decel_Gain_BFAFZOK_40_60_F /* parameter_4 */
    #define TEST_CASE_25_MOTOR_TARGET_FINAL	    (uint8_t)S16_Slip_Gain_BFAFZOK_100_150_F /* parameter_8 */

/** 40~59 TC NO Test Mode    ********************************************/          
#define PIRAMID_MODE_CURRENT1               41
    #define MFC_TEST_HOLD_TIME_P_U_B_20BAR  ((uint16_t)(S16_Decel_Gain_BFAFZOK_B_10_F)*T_100_MS  )
    #define MFC_TEST_HOLD_TIME_P_U_AB_20BAR ((uint16_t)(S16_Slip_Gain_BFAFZOK_100_150_F)*T_100_MS)
    #define MFC_TEST_HOLD_TIME_P_D_AB_20BAR ((uint16_t)(S16_Slip_Gain_BFAFZOK_10_20_F)*T_100_MS  )
    #define MFC_TEST_HOLD_TIME_P_D_B_20BAR  ((uint16_t)(S16_Slip_Gain_BFAFZOK_20_40_F)*T_100_MS  )
    #define MFC_TEST_DIFF_DUTY_B_20BAR      (uint8_t)S16_Slip_Gain_BFAFZOK_40_60_F  
    #define MFC_TEST_DIFF_DUTY_AB_20BAR     (uint8_t)S16_Slip_Gain_BFAFZOK_60_100_F 
    #define MFC_TEST_MOTOR_TARGET_VOLTAGE   (int16_t)S16_Slip_Gain_BFAFZOK_B_10_F  

#define DITHER_MODE_CURRENT                 42
    #define MFC_TEST_DIFF_DUTY_DITHER       (uint8_t)S16_Slip_Gain_BFAFZOK_40_60_F  
    #define MFC_TEST_HOLD_DITHER            ((uint16_t)(S16_Slip_Gain_BFAFZOK_Ab_150_F)*T_100_MS)
    #define MFC_TEST_MOTOR_TARGET_VOLTAGE   (int16_t)S16_Slip_Gain_BFAFZOK_B_10_F  
    
#define PIRAMID_MODE_CURRENT2               43
    #define MFC_TEST_HOLD_TIME_P_U_B_20BAR  ((uint16_t)(S16_Decel_Gain_BFAFZOK_B_10_F)*T_100_MS   )
    #define MFC_TEST_HOLD_TIME_P_U_AB_20BAR ((uint16_t)(S16_Slip_Gain_BFAFZOK_100_150_F)*T_100_MS)
    #define MFC_TEST_HOLD_TIME_P_D_AB_20BAR ((uint16_t)(S16_Slip_Gain_BFAFZOK_10_20_F)*T_100_MS )
    #define MFC_TEST_HOLD_TIME_P_D_B_20BAR  ((uint16_t)(S16_Slip_Gain_BFAFZOK_20_40_F)*T_100_MS )
    #define MFC_TEST_DIFF_DUTY_B_20BAR      (uint8_t)S16_Slip_Gain_BFAFZOK_40_60_F  
    #define MFC_TEST_DIFF_DUTY_AB_20BAR     (uint8_t)S16_Slip_Gain_BFAFZOK_60_100_F 
    #define MFC_TEST_MOTOR_TARGET_VOLTAGE   (int16_t)S16_Slip_Gain_BFAFZOK_B_10_F  

#define HSA_TEST_CURRENT                    44
    #define HSA_TEST_PATTERN                (uint16_t)S16_Slip_Gain_BFAFZOK_Ab_150_F
    #define MFC_TEST_MOTOR_TARGET_VOLTAGE   (int16_t)S16_Slip_Gain_BFAFZOK_B_10_F  

    #define MGH60_0BAR_DUTY                 (uint8_t)S16_Decel_Gain_BFAFZOK_10_20_F  // 50
    #define MGH60_20BAR_DUTY                (uint8_t)S16_Decel_Gain_BFAFZOK_20_40_F  // 70
    #define MGH60_80BAR_DUTY                (uint8_t)S16_Decel_Gain_BFAFZOK_40_60_F  // 100
    #define MGH60_160BAR_DUTY               (uint8_t)S16_Decel_Gain_BFAFZOK_60_100_F // 200
    #define MFC_DITHER_DUTY_Temp1           (uint8_t)S16_Decel_Gain_BFAFZOK_Ab_150_F
    #define MFC_DITHER_DUTY_Temp2           (uint8_t)0

#define TC_ON_OFF_REPEAT                    49                                                         /* P_1 */
    #define TC_ON_DUTY                      (uint8_t)S16_Decel_Gain_BFAFZOK_60_100_F  // 200           /* P_5 */
    #define TC_ON_TIME                      ((uint16_t)(S16_Slip_Gain_BFAFZOK_10_20_F)*L_TIME_100MS ) //1  /* P_9 */
    #define TC_ON_OFF_AMOUNT                (uint16_t)S16_Slip_Gain_BFAFZOK_20_40_F  // 100            /* P_10 */
    
    /* s4500 TEST 03 : TC valve test */    
#define TC_CURRENT_VS_PRESSURE_HOLD         50 /* parameter_1 S16_Decel_Gain_BFAFZOK_100_150_F */
    #define TEST_CASE_50_TC_CURRENT_TARGET  (uint8_t)S16_Decel_Gain_BFAFZOK_40_60_F /* parameter_4 */
    #define TEST_CASE_50_TC_CURRENT_TIME	(uint16_t)(S16_Decel_Gain_BFAFZOK_B_10_F) /* parameter_7 */ 

    /* s4500 TEST 04 : ESV valve test */    
#define ESV_VS_PRESSURE_RISE          		51 /* parameter_1 S16_Decel_Gain_BFAFZOK_100_150_F */
    #define TEST_CASE_108_ESV_ON_TIME       (uint16_t)S16_Decel_Gain_BFAFZOK_40_60_F/* parameter_4 */
    #define MFC_TEST_MOTOR_TARGET_VOLTAGE   (int16_t)S16_Slip_Gain_BFAFZOK_B_10_F /* parameter_14 */
    
/** 100~125 Extra Test Mode  ********************************************/          
#define BRAKE_TORQUE_MEASURE_MODE           101
#define PRESS_EST_MODE_FRONT                102
#define PRESS_EST_MODE_REAR                 103
    #define PRESS_EST_TEST_PULSE            6
    #define PRESS_EST_TEST_TOTAL_TIME       L_U16_TIME_5MSLOOP_20S
    #define PRESS_EST_TEST_MOTOR_ON_TIME    L_U16_TIME_5MSLOOP_5S
    #define PRESS_EST_TEST_RISE_TIME        L_U16_TIME_5MSLOOP_2S
#define PRESS_EST_MODE_FL_by_MP             104
#define PRESS_EST_MODE_FR_by_MP             105
#define PRESS_EST_MODE_RL_by_MP             106
#define PRESS_EST_MODE_RR_by_MP             107


/* MGH-80 GM s4500 HW TEST MODE */
#define TEST_MODE_SUB                		(uint16_t)S16_Slip_Gain_BFAFZOK_100_150_F  /* Parameter 8 : 1 RL, 2 RR */

/************************************************************************/          

#define MGH60_delta_10BAR_DUTY              10

#define MFC_TEST_HSA_RISE_TIME              L_U16_TIME_5MSLOOP_3S//L_U16_TIME_5MSLOOP_3s

#define MFC_TEST_MAX_CURRENT_P              200
#define MFC_CURRENT_PRESS_EST               150

#define MFC_TEST_CIRCUIT                    S16_Decel_Gain_BFAFZOK_100_150_R
#define PRIMARY_ONLY						1
#define SECONDARY_ONLY						2


/****************************************************
* Edit here for ESPLUS valve test
****************************************************/

#if (__HW_TEST_MODULE_STRUCT_CHANGE==ENABLE)
#define     HW_Test_HV_VL_fl          laHwTestValveFL.u1HwTestHvVlAct
#define     HW_Test_HV_VL_fr          laHwTestValveFR.u1HwTestHvVlAct
#define     HW_Test_HV_VL_rl          laHwTestValveRL.u1HwTestHvVlAct
#define     HW_Test_HV_VL_rr          laHwTestValveRR.u1HwTestHvVlAct
#define     HW_Test_AV_VL_fl          laHwTestValveFL.u1HwTestAvVlAct
#define     HW_Test_AV_VL_fr          laHwTestValveFR.u1HwTestAvVlAct
#define     HW_Test_AV_VL_rl          laHwTestValveRL.u1HwTestAvVlAct
#define     HW_Test_AV_VL_rr          laHwTestValveRR.u1HwTestAvVlAct

#define		lau8HwTestNoDuty_fl			laHwTestValveFL.u8HwTestPwmDuty
#define		lau8HwTestNoDuty_fr			laHwTestValveFR.u8HwTestPwmDuty
#define		lau8HwTestNoDuty_rl			laHwTestValveRL.u8HwTestPwmDuty
#define		lau8HwTestNoDuty_rr			laHwTestValveRR.u8HwTestPwmDuty

#define		lau8HwTestFullRiseTimeFL	laHwTestValveFL.u8HwTestReapplyTime
#define		lau8HwTestFullRiseTimeFR	laHwTestValveFR.u8HwTestReapplyTime
#define		lau8HwTestFullRiseTimeRL	laHwTestValveRL.u8HwTestReapplyTime
#define		lau8HwTestFullRiseTimeRR	laHwTestValveRR.u8HwTestReapplyTime

#define		lau8HwTestDumpScanTimeFL	laHwTestValveFL.u8HwTestDumpScanTime
#define		lau8HwTestDumpScanTimeFR	laHwTestValveFR.u8HwTestDumpScanTime
#define		lau8HwTestDumpScanTimeRL	laHwTestValveRL.u8HwTestDumpScanTime
#define		lau8HwTestDumpScanTimeRR	laHwTestValveRR.u8HwTestDumpScanTime

#else
#define     HW_Test_HV_VL_fl          HW_FLG00.lau1HwTestBit00
#define     HW_Test_HV_VL_fr          HW_FLG00.lau1HwTestBit01
#define     HW_Test_HV_VL_rl          HW_FLG00.lau1HwTestBit02
#define     HW_Test_HV_VL_rr          HW_FLG00.lau1HwTestBit03
#define     HW_Test_AV_VL_fl          HW_FLG00.lau1HwTestBit04
#define     HW_Test_AV_VL_fr          HW_FLG00.lau1HwTestBit05
#define     HW_Test_AV_VL_rl          HW_FLG00.lau1HwTestBit06
#define     HW_Test_AV_VL_rr          HW_FLG00.lau1HwTestBit07
#endif

#define     HW_Test_TC_NO_Prim        HW_FLG01.lau1HwTestBit00 
#define     HW_Test_TC_NO_Secn        HW_FLG01.lau1HwTestBit01 
#define     HW_Test_ESV_Prim          HW_FLG01.lau1HwTestBit02 
#define     HW_Test_ESV_Secn          HW_FLG01.lau1HwTestBit03 
#define     HW_Test_Motor_On          HW_FLG01.lau1HwTestBit04 
#define     lau8HwTestNoDutyWhl       plaHwTestWhl->u8HwTestPwmDuty
#define     lau1HWTestHvVLWhl         plaHwTestWhl->u1HwTestHvVlAct
#define     lau1HWTestAvVLWhl         plaHwTestWhl->u1HwTestAvVlAct
#define		lau8HwTestFullRiseTimeWhl	  plaHwTestWhl->u8HwTestReapplyTime

/* Variables Definition*******************************************************/

static  HW_FLG_t HW_FLG00, HW_FLG01;
int8_t    las8HwTestMode;
int8_t    las8DutyDiff;
  #if (__HW_TEST_MODULE_STRUCT_CHANGE==DISABLE)
uint8_t   lau8HwTestNoDuty_fl, lau8HwTestNoDuty_fr, lau8HwTestNoDuty_rl, lau8HwTestNoDuty_rr;
  #endif
uint16_t    HW_test_cnt_t;

uint16_t    LFC_valve_test_cnt;
int16_t     LFC_test_initial_duty;
int16_t     LFC_test_duty;
int16_t     LFC_test_duty_hold_time;
uint16_t	lau16HwTestCnt;
  #if __VDC
uint16_t    BrakeOn_cnt; 
uint16_t    TestMotorVoltage_cnt; 
uint8_t     lau8HwTestMscDuty;
int16_t     las16HwTestMscOnTimeMsec, las16HwTestMscOffTimeMsec;
  #endif

  #if (__VDC && __TCMF_ENABLE)
uint8_t   MFC_TEST_INITIAL_CURRENT;
uint16_t    valve_test_cnt, valve_test_cnt_t;
uint8_t   lau8HwTestTcNoDuty, lau8HwTestEsvDuty;
uint8_t   MFC_Duty_Dither_cnt_S, MFC_Duty_Dither_cnt_P;
uint16_t    MFC_TEST_MODE_P_UP_Time;
uint16_t    MFC_TEST_MODE_P_DOWN_Time;
uint16_t    MFC_TEST_MODE_P_B_20BAR;
uint16_t    MFC_TEST_MODE_P_AB_20BAR;
uint8_t   MFC_TEST_20BAR_CURRENT;
  #endif
  
uint8_t lau8HwTestABSLampRequest;
uint16_t lau16WhlNoTestModeActive = 0;

  #if (__HW_TEST_MODULE_STRUCT_CHANGE==ENABLE)  
LA_HWTEST_VALVE_t laHwTestValveFL, laHwTestValveFR, laHwTestValveRL, laHwTestValveRR;
LA_HWTEST_VALVE_t *plaHwTestWhl;
  #endif
WHEEL_VV_OUTPUT_t laHwTest_FL1HP, laHwTest_FL2HP, laHwTest_FR1HP, laHwTest_FR2HP;
WHEEL_VV_OUTPUT_t laHwTest_RL1HP, laHwTest_RL2HP, laHwTest_RR1HP, laHwTest_RR2HP;

/* LocalFunction prototype ***************************************************/
static void    LAABS_vResetWheelNoValveCommand(void);

  #if (__VDC && __TCMF_ENABLE)
static void    LAMFC_vTestTcNoValveDithering(void);
  #endif

#endif

#if (__HW_TEST_MODULE_STRUCT_CHANGE==ENABLE)
static void LAHW_vDecideTestValveOutput(int8_t s8HwTestHV, int8_t s8HwTestAV, LA_HWTEST_VALVE_t *laHwTestValve, WHEEL_VV_OUTPUT_t *laWL1HP,  WHEEL_VV_OUTPUT_t *laWL2HP);
#endif

/* Implementation*************************************************************/

#if __HW_TEST_MODE

void    LAABS_vTestWheelNoValve(void)
{
    uint16_t u16DutyRiseHoldSampleTime = 0; 
    

    if(TEST_CASE_1_RISE_N_HOLD_TIME>0)
    {
        u16DutyRiseHoldSampleTime = (LFC_valve_test_cnt%TEST_CASE_1_RISE_N_HOLD_TIME);
    }
    else
    {
        u16DutyRiseHoldSampleTime = 0;
    }

	if(TEST_CASE_1_SELECT_HV == 1) /* RL */
	{
        plaHwTestWhl = &laHwTestValveRL;
	}
	else if(TEST_CASE_1_SELECT_HV == 2) /* RR */
	{
        plaHwTestWhl = &laHwTestValveRR;
	}
	else if(TEST_CASE_1_SELECT_HV == 3) /* FL */
	{
        plaHwTestWhl = &laHwTestValveFL;
	}
	else if(TEST_CASE_1_SELECT_HV == 4) /* FR */
	{
        plaHwTestWhl = &laHwTestValveFR;
	}
	else if(TEST_CASE_1_SELECT_HV == 5) /* front wheels */
	{
        plaHwTestWhl = &laHwTestValveFR;
	}
	else if(TEST_CASE_1_SELECT_HV == 6) /* rear wheels */
	{
        plaHwTestWhl = &laHwTestValveRL;
	}
	else
	{
        plaHwTestWhl = &laHwTestValveFR; /* default */
	}
	
	if(lau16WhlNoTestModeActive==0)
	{
	    if(ldahbu8PressCtrlTestActive==1)
	    {
	        lau16WhlNoTestModeActive = 1;
	    }
	}
    
  #if __IDB_LOGIC==ENABLE
    /*if(lsesps16EstBrkPressByBrkPedalF > MPRESS_2BAR)*/
	if(lau16WhlNoTestModeActive==1)
  #else
   #if __VDC    
    if(fu1ESCDisabledBySW==1)
   #else    
    if(((vref<=VREF_5_KPH)&&(vref_alt>VREF_5_KPH)) || (HW_test_cnt_t>0)) // 속도 5KPH이하 이면 테스트 모드 
   #endif   
  #endif
    {
        LAABS_vResetWheelNoValveCommand();
        
        las8HwTestMode = S8_HW_TEST_MODE_SELECTION;

        HW_test_cnt_t = HW_test_cnt_t+1;
      #if !__VDC  
        if(vref>=VREF_10_KPH)
        {
            HW_test_cnt_t = 0;
        }
      #endif  
    
        if((las8HwTestMode==INITIAL_DUTY_VS_RISE_RATE) || (las8HwTestMode==NO_Full_ON_TIME_VS_RISE_RATE) )
        {

			  #if (S8_HW_TEST_MODE_SELECTION == INITIAL_DUTY_VS_RISE_RATE)
			#define TEST_CASE_1_TOTAL_TIME          (TEST_CASE_1_TIME*((TEST_CASE_1_INIT_DUTY-TEST_CASE_1_END_DUTY)/TEST_CASE_1_DUTY_DIFF+1))
			  #else
			#define TEST_CASE_1_TOTAL_TIME			((TEST_CASE_1_TIME)*(TEST_CASE_1_INIT_DUTY))
        	  #endif

            if(HW_test_cnt_t < T_1000_MS)
            {
                ;
            }
            else if(HW_test_cnt_t < TEST_CASE_1_TOTAL_TIME)
            {
                if(HW_test_cnt_t < T_1000_MS+TEST_CASE_1_TIME)
                {
                    LFC_valve_test_cnt = HW_test_cnt_t - T_1000_MS;
					if(las8HwTestMode==INITIAL_DUTY_VS_RISE_RATE)
                    {
						LFC_test_initial_duty = (int16_t)TEST_CASE_1_INIT_DUTY;
					else
                    {
						LFC_test_initial_duty = (int16_t)(0);
					}
					  
                }
                else
                {
                    LFC_valve_test_cnt = ((HW_test_cnt_t - T_1000_MS)%TEST_CASE_1_TIME);
					if(las8HwTestMode==INITIAL_DUTY_VS_RISE_RATE)
                    {
						LFC_test_initial_duty = (int16_t)(TEST_CASE_1_INIT_DUTY - ((HW_test_cnt_t - T_1000_MS)/TEST_CASE_1_TIME)*TEST_CASE_1_DUTY_DIFF);
					}
					else
                    {
						LFC_test_initial_duty = (int16_t)(0);
					}
                }

                if(LFC_valve_test_cnt<TEST_CASE_1_HOLD_TIME)
                {
                    lau8HwTestNoDutyWhl = 200; lau1HWTestHvVLWhl = 1; lau1HWTestAvVLWhl = 0;
                }
                else if(LFC_valve_test_cnt<TEST_CASE_1_HOLD_TIME+TEST_CASE_1_DUTY_TIME)
                {
                    if(u16DutyRiseHoldSampleTime < TEST_CASE_1_DUTY_ON_TIME_10MS)
                    {
                        lau8HwTestNoDutyWhl = LFC_test_initial_duty;
                       
						if(las8HwTestMode==NO_Full_ON_TIME_VS_RISE_RATE)
                   	 	{
							lau8HwTestFullRiseTimeWhl = 10;
						}
						else
                   		{
							;	/* Do Nothing */
						}

                	}
                	else
                	{
                        lau8HwTestNoDutyWhl = 200; lau1HWTestHvVLWhl = 1; lau1HWTestAvVLWhl = 0;
                    }
                }
                else
                {
                    ;
                }       
            }
            else
            {
                lau16WhlNoTestModeActive = 0;
            }
        }
        else if(las8HwTestMode==DUTY_DEC_RATE_VS_RISE_PATTERN)
        {   
            #define TEST_CASE_2_MIN_DUTY        20
            #define TEST_CASE_2_DUTY_DEC_RATE_DIFF  2
            #define TEST_CASE_2_TIME            (TEST_CASE_2_HOLD_TIME+TEST_CASE_2_DUTY_TIME+TEST_CASE_2_INTERVAL_TIME)
            #define TEST_CASE_2_CASES           (((TEST_CASE_2_DUTY_DEC_RATE_MAX-TEST_CASE_2_DUTY_DEC_RATE_MIN)/TEST_CASE_2_DUTY_DEC_RATE_DIFF)+1)
            #define TEST_CASE_2_TOTAL_TIME      (TEST_CASE_2_TIME*TEST_CASE_2_CASES)
            
            if(HW_test_cnt_t < T_1000_MS)
            {
                ;
            }
            else if(HW_test_cnt_t < TEST_CASE_2_TOTAL_TIME)
            {
                LFC_valve_test_cnt = ((HW_test_cnt_t - T_1000_MS)%TEST_CASE_2_TIME);
                LFC_test_initial_duty = TEST_CASE_2_INIT_DUTY;              
                
                if(LFC_valve_test_cnt<TEST_CASE_2_HOLD_TIME)
                {
                	if(TEST_MODE_SUB == 1) /* RL */
                	{
                		lau8HwTestNoDuty_rl=200; HW_Test_HV_VL_rl = 1; HW_Test_AV_VL_rl = 0;
                	}
                	else if(TEST_MODE_SUB == 2) /* RR */
                	{
                		lau8HwTestNoDuty_rr=200; HW_Test_HV_VL_rr = 1; HW_Test_AV_VL_rr = 0;
                	}
                	else
                	{
	                    lau8HwTestNoDuty_fl=200; HW_Test_HV_VL_fl = 1; HW_Test_AV_VL_fl = 0;
	                    lau8HwTestNoDuty_fr=200; HW_Test_HV_VL_fr = 1; HW_Test_AV_VL_fr = 0;
	                    lau8HwTestNoDuty_rl=200; HW_Test_HV_VL_rl = 1; HW_Test_AV_VL_rl = 0;
	                    lau8HwTestNoDuty_rr=200; HW_Test_HV_VL_rr = 1; HW_Test_AV_VL_rr = 0;
	                }
                }
                else if(LFC_valve_test_cnt<TEST_CASE_2_HOLD_TIME+TEST_CASE_2_DUTY_TIME)
                {
                    if(LFC_valve_test_cnt==TEST_CASE_2_HOLD_TIME)
                    {
                        LFC_test_duty_hold_time = ((HW_test_cnt_t - T_1000_MS)/TEST_CASE_2_TIME)*TEST_CASE_2_DUTY_DEC_RATE_DIFF + TEST_CASE_2_DUTY_DEC_RATE_MIN;
                    }
                    LFC_test_duty = LFC_test_initial_duty - ((LFC_valve_test_cnt - TEST_CASE_2_HOLD_TIME)/LFC_test_duty_hold_time);
                    
                    if(LFC_test_duty < TEST_CASE_2_MIN_DUTY) {LFC_test_duty = TEST_CASE_2_MIN_DUTY;}
                    
                    if(TEST_MODE_SUB == 1) /* RL */
                	{
                		lau8HwTestNoDuty_rl=(uint8_t)LFC_test_duty;
                	}
                	else if(TEST_MODE_SUB == 2) /* RR */
                	{
                		lau8HwTestNoDuty_rr=(uint8_t)LFC_test_duty;
                	}
                	else
                	{
	                    lau8HwTestNoDuty_fl=(uint8_t)LFC_test_duty;
	                    lau8HwTestNoDuty_fr=(uint8_t)LFC_test_duty;
	                    lau8HwTestNoDuty_rl=(uint8_t)LFC_test_duty;
	                    lau8HwTestNoDuty_rr=(uint8_t)LFC_test_duty;
	                }
                }
                else
                {
                    ;
                }       
            }
            else
            {
                ;
            }
        }

/*        else if(las8HwTestMode==WHEEL_VALVE_UPDOWN_PATTERN)
        {
            #define TEST_CASE_2_MIN_DUTY        20
            #define TEST_CASE_2_DUTY_DEC_RATE_DIFF  2
            #define TEST_CASE_2_TIME            (TEST_CASE_2_HOLD_TIME+TEST_CASE_2_DUTY_TIME+TEST_CASE_2_INTERVAL_TIME)
            #define TEST_CASE_2_CASES           (((TEST_CASE_2_DUTY_DEC_RATE_MAX-TEST_CASE_2_DUTY_DEC_RATE_MIN)/TEST_CASE_2_DUTY_DEC_RATE_DIFF)+1)
            #define TEST_CASE_3_TOTAL_TIME      (TEST_CASE_2_TIME*TEST_CASE_2_CASES)
    #define TEST_CASE_3_INIT_DUTY           (uint8_t)S16_Decel_Gain_BFAFZOK_10_20_F
    #define TEST_CASE_3_END_DUTY            (uint8_t)S16_Decel_Gain_BFAFZOK_20_40_F
    #define TEST_CASE_3_DUTY_DIFF           (uint8_t)S16_Decel_Gain_BFAFZOK_40_60_F
    #define TEST_CASE_3_INIT_HOLD_TIME      ((uint16_t)(S16_Decel_Gain_BFAFZOK_60_100_F)*T_100_MS )
    #define TEST_CASE_3_DUTY_HOLD_TIME      ((uint16_t)(S16_Decel_Gain_BFAFZOK_Ab_150_F)*T_100_MS ) 
    #define TEST_CASE_3_PULSEDOWN_INTERVAL  ((uint16_t)(S16_Decel_Gain_BFAFZOK_B_10_F)*T_100_MS   )  
    #define TEST_CASE_3_PULSEDOWN_AMOUNT    (uint8_t)S16_Slip_Gain_BFAFZOK_Ab_150_F
    
    #define TEST_CASE_3_WHEEL_NO_TEST_TIME  (((TEST_CASE_3_INIT_DUTY - TEST_CASE_3_END_DUTY)/TEST_CASE_3_DUTY_DIFF)+1)*TEST_CASE_3_DUTY_HOLD_TIME*2
            
            if((HW_test_cnt_t < T_1000_MS) || (HW_test_cnt_t > 60000)
            {
                ;
            }
            else if(HW_test_cnt_t < (T_1000_MS+TEST_CASE_3_INIT_HOLD_TIME))
            {
                lau8HwTestNoDuty_fl=200; HW_Test_HV_VL_fl = 1; 
                lau8HwTestNoDuty_fr=200; HW_Test_HV_VL_fr = 1; 
                lau8HwTestNoDuty_rl=200; HW_Test_HV_VL_rl = 1; 
                lau8HwTestNoDuty_rr=200; HW_Test_HV_VL_rr = 1; 
            }
            else if(HW_test_cnt_t < TEST_CASE_3_WHEEL_NO_TEST_TIME)
            {
                LFC_valve_test_cnt = ((HW_test_cnt_t - T_1000_MS)%TEST_CASE_2_TIME);
                LFC_test_initial_duty = TEST_CASE_2_INIT_DUTY;              
                
                if(LFC_valve_test_cnt<TEST_CASE_2_HOLD_TIME)
                {
                    lau8HwTestNoDuty_fl=200; HW_Test_HV_VL_fl = 1; HW_Test_AV_VL_fl = 0;
                    lau8HwTestNoDuty_fr=200; HW_Test_HV_VL_fr = 1; HW_Test_AV_VL_fr = 0;
                    lau8HwTestNoDuty_rl=200; HW_Test_HV_VL_rl = 1; HW_Test_AV_VL_rl = 0;
                    lau8HwTestNoDuty_rr=200; HW_Test_HV_VL_rr = 1; HW_Test_AV_VL_rr = 0;
                }
                else if(LFC_valve_test_cnt<TEST_CASE_2_HOLD_TIME+TEST_CASE_2_DUTY_TIME)
                {
                    if(LFC_valve_test_cnt==TEST_CASE_2_HOLD_TIME)
                    {
                        LFC_test_duty_hold_time = ((HW_test_cnt_t - T_1000_MS)/TEST_CASE_2_TIME)*TEST_CASE_2_DUTY_DEC_RATE_DIFF + TEST_CASE_2_DUTY_DEC_RATE_MIN;
                    }
                    LFC_test_duty = LFC_test_initial_duty - ((LFC_valve_test_cnt - TEST_CASE_2_HOLD_TIME)/LFC_test_duty_hold_time);
                    
                    if(LFC_test_duty < TEST_CASE_2_MIN_DUTY) {LFC_test_duty = TEST_CASE_2_MIN_DUTY;}
                    
                    lau8HwTestNoDuty_fl=(uint8_t)LFC_test_duty;
                    lau8HwTestNoDuty_fr=(uint8_t)LFC_test_duty;
                    lau8HwTestNoDuty_rl=(uint8_t)LFC_test_duty;
                    lau8HwTestNoDuty_rr=(uint8_t)LFC_test_duty;
                }
                else
                {
                    ;
                }       
            }
            else
            {
                ;
            }
        }*/
		else if(las8HwTestMode==NO_DUTY_VS_RISE_RATE)
		{
			if(HW_test_cnt_t > L_U8_TIME_10MSLOOP_500MS)
			{
				#if __VDC
				if((lau16HwTestCnt==0) && (wstr < WSTR_30_DEG))
				{
					if((TEST_MODE_SUB==1) || (TEST_MODE_SUB==3))  /* Front, all wheel */
					{
						lau8HwTestNoDuty_fl=200; HW_Test_HV_VL_fl = 1; HW_Test_AV_VL_fl = 0;
	            	    lau8HwTestNoDuty_fr=200; HW_Test_HV_VL_fr = 1; HW_Test_AV_VL_fr = 0;
	            	}
	            	
					if((TEST_MODE_SUB==2) || (TEST_MODE_SUB==3))  /* Rear, all wheel */
					{
						lau8HwTestNoDuty_rl=200; HW_Test_HV_VL_rl = 1; HW_Test_AV_VL_rl = 0;
	            	    lau8HwTestNoDuty_rr=200; HW_Test_HV_VL_rr = 1; HW_Test_AV_VL_rr = 0;
	            	}
	            	
            	    if((TEST_MODE_SUB>=1) && (TEST_MODE_SUB<=3))
            	    {
            	    	lau8HwTestABSLampRequest = 1; /* ABS W/L */
            	    }
				}
				else if(wstr >= WSTR_30_DEG)
				{
					lau16HwTestCnt=lau16HwTestCnt+1;
					if(lau16HwTestCnt<=L_U8_TIME_10MSLOOP_500MS) 
					{
						if((TEST_MODE_SUB==1) || (TEST_MODE_SUB==3))  /* Front, all wheel */
						{
							lau8HwTestNoDuty_fl=200; HW_Test_HV_VL_fl = 1; HW_Test_AV_VL_fl = 0;
		            	    lau8HwTestNoDuty_fr=200; HW_Test_HV_VL_fr = 1; HW_Test_AV_VL_fr = 0;
		            	}
	            	    if((TEST_MODE_SUB==2) || (TEST_MODE_SUB==3))  /* Rear, all wheel */
						{
	            	    	lau8HwTestNoDuty_rl=200; HW_Test_HV_VL_rl = 1; HW_Test_AV_VL_rl = 0;
	            	    	lau8HwTestNoDuty_rr=200; HW_Test_HV_VL_rr = 1; HW_Test_AV_VL_rr = 0;
	            	    }
	            	    if((TEST_MODE_SUB>=1) && (TEST_MODE_SUB<=3))
	            	    {
	            	    	lau8HwTestABSLampRequest = 1; /* ABS W/L */
	            	    }
	            	}							
					else if(lau16HwTestCnt<=(L_U8_TIME_10MSLOOP_500MS+TEST_CASE_4_NO_DUTY_ON_TIME))
					{
						if((TEST_MODE_SUB==1) || (TEST_MODE_SUB==3))  /* Front, all wheel */
						{
							lau8HwTestNoDuty_fl = TEST_CASE_4_NO_DUTY_TARGET;
	        	        	lau8HwTestNoDuty_fr = TEST_CASE_4_NO_DUTY_TARGET;
	        	        }
	        	        if((TEST_MODE_SUB==2) || (TEST_MODE_SUB==3))  /* Rear, all wheel */
						{
							lau8HwTestNoDuty_rl = TEST_CASE_4_NO_DUTY_TARGET;
	        	        	lau8HwTestNoDuty_rr = TEST_CASE_4_NO_DUTY_TARGET;
						}
					}
					else{}
				}
				else{}
				#endif
			}
			else{}
		}
		
		else if(las8HwTestMode==NC_OPEN_TIME_VS_DUMP_RATE)
		{
			if(HW_test_cnt_t <= L_U8_TIME_10MSLOOP_1000MS)
			{
				; /* ESV open */
			}
			#if __VDC 
			else if((lau16HwTestCnt==0) && (wstr < WSTR_30_DEG))
			{
        	    if((TEST_MODE_SUB>=1) && (TEST_MODE_SUB<=3))
        	    {
        	    	lau8HwTestABSLampRequest = 1; /* ABS W/L */
        	    }
        	}				
			else if((wstr >= WSTR_30_DEG) && (lau16HwTestCnt <= L_U16_TIME_10MSLOOP_4S))
			{
				lau16HwTestCnt = lau16HwTestCnt + 1;
								
				if(lau16HwTestCnt <= L_U8_TIME_10MSLOOP_1000MS)
				{
					if((TEST_MODE_SUB==1) || (TEST_MODE_SUB==3))  /* Front, all wheel */
					{
						lau8HwTestNoDuty_fl=200; HW_Test_HV_VL_fl = 1; HW_Test_AV_VL_fl = 0;
	            	    lau8HwTestNoDuty_fr=200; HW_Test_HV_VL_fr = 1; HW_Test_AV_VL_fr = 0;
	            	}
	            	
					if((TEST_MODE_SUB==2) || (TEST_MODE_SUB==3))  /* Rear, all wheel */
					{
						lau8HwTestNoDuty_rl=200; HW_Test_HV_VL_rl = 1; HW_Test_AV_VL_rl = 0;
	            	    lau8HwTestNoDuty_rr=200; HW_Test_HV_VL_rr = 1; HW_Test_AV_VL_rr = 0;
	            	}
				}
				else if(lau16HwTestCnt <= (L_U8_TIME_10MSLOOP_1000MS+TEST_CASE_5_NC_ON_TIME))
				{
					if((TEST_MODE_SUB==1) || (TEST_MODE_SUB==3))  /* Front, all wheel */
					{
                		lau8HwTestNoDuty_fl=201; HW_Test_HV_VL_fl = 1; HW_Test_AV_VL_fl = 1;
                		lau8HwTestNoDuty_fr=201; HW_Test_HV_VL_fr = 1; HW_Test_AV_VL_fr = 1;
                		lau8HwTestDumpScanTimeFL=U8_BASE_LOOPTIME;
                		lau8HwTestDumpScanTimeFR=U8_BASE_LOOPTIME;
                	}
                	if((TEST_MODE_SUB==2) || (TEST_MODE_SUB==3))  /* Rear, all wheel */
					{
                		lau8HwTestNoDuty_rl=201; HW_Test_HV_VL_rl = 1; HW_Test_AV_VL_rl = 1;
                		lau8HwTestNoDuty_rr=201; HW_Test_HV_VL_rr = 1; HW_Test_AV_VL_rr = 1;
                		lau8HwTestDumpScanTimeRL=U8_BASE_LOOPTIME;
                		lau8HwTestDumpScanTimeRR=U8_BASE_LOOPTIME;
                	}
				}
				else if(lau16HwTestCnt <= L_U16_TIME_10MSLOOP_4S)
				{
					if((TEST_MODE_SUB==1) || (TEST_MODE_SUB==3))  /* Front, all wheel */
					{
                		lau8HwTestNoDuty_fl=200; HW_Test_HV_VL_fl = 1; HW_Test_AV_VL_fl = 0;
                		lau8HwTestNoDuty_fr=200; HW_Test_HV_VL_fr = 1; HW_Test_AV_VL_fr = 0;
                	}
                	if((TEST_MODE_SUB==2) || (TEST_MODE_SUB==3))  /* Rear, all wheel */
					{
                		lau8HwTestNoDuty_rl=200; HW_Test_HV_VL_rl = 1; HW_Test_AV_VL_rl = 0;
                		lau8HwTestNoDuty_rr=200; HW_Test_HV_VL_rr = 1; HW_Test_AV_VL_rr = 0;
                	}
				}
				else{}
			}
			#endif
			else{}
        }
      #if __VDC  
        else if(las8HwTestMode==BRAKE_TORQUE_MEASURE_MODE)
        {
            if((BLS==1) && (vref>VREF_10_KPH))
            {
                BrakeOn_cnt = BrakeOn_cnt+1;
                if((BrakeOn_cnt>T_1MIN)||(mpress>MPRESS_100BAR))
                {
                    lau8HwTestNoDuty_rl=0;
                    lau8HwTestNoDuty_rr=0;
                }
                else
                {
                    HW_Test_HV_VL_rl = 1;
                    HW_Test_HV_VL_rr = 1;
                    lau8HwTestNoDuty_rl = 200;
                    lau8HwTestNoDuty_rr = 200;
                }
            }
            else
            {
                BrakeOn_cnt=0;
                lau8HwTestNoDuty_fl=0;
                lau8HwTestNoDuty_fr=0;
                lau8HwTestNoDuty_rl=0;
                lau8HwTestNoDuty_rr=0;
            }           
        }
        else if(las8HwTestMode==PRESS_EST_MODE_FRONT)
        {
            LFC_valve_test_cnt = HW_test_cnt_t%PRESS_EST_TEST_TOTAL_TIME;
            
            if(HW_test_cnt_t>=(PRESS_EST_TEST_TOTAL_TIME*PRESS_EST_TEST_PULSE))
            {
                ;
            }
            else if(LFC_valve_test_cnt<T_50_MS)
            {
              #if (__VDC && !__TCMF_ENABLE)
                HW_Test_Motor_On = 1;
              #else 
                ;
              #endif  
            }
            else if(LFC_valve_test_cnt<PRESS_EST_TEST_MOTOR_ON_TIME)
            {
              #if (__VDC && !__TCMF_ENABLE) 
                HW_Test_Motor_On = 1;
              #endif  

                if(((HW_test_cnt_t/PRESS_EST_TEST_TOTAL_TIME)%2)==0)    /* FL */
                {
                  #if __SPLIT_TYPE                  /* FR Split  */
                  
                    HW_Test_HV_VL_fr = 1;
                    lau8HwTestNoDuty_fr = 200;
                   #if (__VDC && !__TCMF_ENABLE) 
                    HW_Test_TC_NO_Prim = 1;
                    HW_Test_ESV_Prim = 1;
                   #endif 
                   
                  #else                             /* X Split  */
                  
                    HW_Test_HV_VL_rr = 1;
                    lau8HwTestNoDuty_rr = 200;
                   #if (__VDC && !__TCMF_ENABLE) 
                    HW_Test_TC_NO_Secn = 1;
                    HW_Test_ESV_Secn = 1;
                   #endif 
                   
                  #endif                  
                  
                    if(LFC_valve_test_cnt>PRESS_EST_TEST_RISE_TIME)
                    {
                        HW_Test_HV_VL_fl = 1;
                        lau8HwTestNoDuty_fl = 200;
                        if(LFC_valve_test_cnt%L_U8_TIME_5MSLOOP_100MS==0)
                        {
                            HW_Test_AV_VL_fl = 1;
                            lau8HwTestNoDuty_fl = 201;
                        }
                    }
                }
                else                                /* FR */
                {
                  #if __SPLIT_TYPE                  /* FR Split  */
                  
                    HW_Test_HV_VL_fl = 1;
                    lau8HwTestNoDuty_fl = 200;                    
                   #if (__VDC && !__TCMF_ENABLE) 
                    HW_Test_TC_NO_Prim = 1;
                    HW_Test_ESV_Prim = 1;
                   #endif 
                   
                  #else                             /* X Split  */
                  
                    HW_Test_HV_VL_rl = 1;
                    lau8HwTestNoDuty_rl = 200;
                   #if (__VDC && !__TCMF_ENABLE) 
                    HW_Test_TC_NO_Prim = 1;
                    HW_Test_ESV_Prim = 1;
                   #endif 
                   
                  #endif
                  
                    if(LFC_valve_test_cnt>PRESS_EST_TEST_RISE_TIME)
                    {
                        HW_Test_HV_VL_fr = 1;
                        lau8HwTestNoDuty_fr = 200;
                        if(LFC_valve_test_cnt%L_U8_TIME_5MSLOOP_100MS==0)
                        {
                            HW_Test_AV_VL_fr = 1;
                            lau8HwTestNoDuty_fr = 201;
                        }
                    }
                }
            }
            else
            {
                ;
            }
        }
        else if(las8HwTestMode==PRESS_EST_MODE_REAR)
        {
            LFC_valve_test_cnt = HW_test_cnt_t%PRESS_EST_TEST_TOTAL_TIME;
            
            if(HW_test_cnt_t>=(PRESS_EST_TEST_TOTAL_TIME*PRESS_EST_TEST_PULSE))
            {
                ;
            }
            else if(LFC_valve_test_cnt<T_50_MS)
            {
              #if (__VDC && !__TCMF_ENABLE)
                HW_Test_Motor_On = 1;
              #else 
                ;
              #endif  
            }
            else if(LFC_valve_test_cnt<PRESS_EST_TEST_MOTOR_ON_TIME)
            {
              #if (__VDC && !__TCMF_ENABLE) 
                HW_Test_Motor_On = 1;
              #endif  
              
                if(((HW_test_cnt_t/PRESS_EST_TEST_TOTAL_TIME)%2)==0)    /* RL */
                {
                  #if __SPLIT_TYPE                  /* FR Split  */
                  
                    HW_Test_HV_VL_rr = 1;
                    lau8HwTestNoDuty_rr = 200;
                   #if (__VDC && !__TCMF_ENABLE) 
                    HW_Test_TC_NO_Secn = 1;
                    HW_Test_ESV_Secn = 1;
                   #endif 
                   
                  #else                             /* X Split  */
                  
                    HW_Test_HV_VL_fr = 1;
                    lau8HwTestNoDuty_fr = 200;
                   #if (__VDC && !__TCMF_ENABLE) 
                    HW_Test_TC_NO_Prim = 1;
                    HW_Test_ESV_Prim = 1;
                   #endif 
                   
                  #endif                  
                  
                    if(LFC_valve_test_cnt>PRESS_EST_TEST_RISE_TIME)
                    {
                        HW_Test_HV_VL_rl = 1;
                        lau8HwTestNoDuty_rl = 200;
                        if(LFC_valve_test_cnt%L_U8_TIME_5MSLOOP_100MS==0)
                        {
                            HW_Test_AV_VL_rl = 1;
                            lau8HwTestNoDuty_rl = 201;
                        }
                    }
                }
                else
                {
                  #if __SPLIT_TYPE                      /*FR Split  */
                  
                    HW_Test_HV_VL_rl = 1;
                    lau8HwTestNoDuty_rl = 200;                    
                   #if (__VDC && !__TCMF_ENABLE) 
                    HW_Test_TC_NO_Secn = 1;
                    HW_Test_ESV_Secn = 1;
                   #endif 
                   
                  #else                            /*X Split  */
                  
                    HW_Test_HV_VL_fl = 1;
                    lau8HwTestNoDuty_fl = 200;
                   #if (__VDC && !__TCMF_ENABLE) 
                    HW_Test_TC_NO_Secn = 1;
                    HW_Test_ESV_Secn = 1;
                   #endif 
                   
                  #endif
                  
                    if(LFC_valve_test_cnt>PRESS_EST_TEST_RISE_TIME)
                    {
                        HW_Test_HV_VL_rr = 1;
                        lau8HwTestNoDuty_rr = 200;
                        if(LFC_valve_test_cnt%L_U8_TIME_5MSLOOP_100MS==0)
                        {
                            HW_Test_AV_VL_rr = 1;
                            lau8HwTestNoDuty_rr = 201;
                        }
                    }
                }
            }
            else
            {
                ;
            }
        }
        else if((las8HwTestMode==PRESS_EST_MODE_FL_by_MP) || (las8HwTestMode==PRESS_EST_MODE_FR_by_MP)
         || (las8HwTestMode==PRESS_EST_MODE_RL_by_MP) || (las8HwTestMode==PRESS_EST_MODE_RR_by_MP))
        {
            if(BLS==1)
            {
                BrakeOn_cnt = BrakeOn_cnt+1;
                if(BrakeOn_cnt>=((L_U16_TIME_5MSLOOP_5S+L_U8_TIME_5MSLOOP_1000MS)*7))
                {
                    ;
                }
                else if((BrakeOn_cnt%(L_U16_TIME_5MSLOOP_5S+L_U8_TIME_5MSLOOP_1000MS))<L_U8_TIME_5MSLOOP_1000MS)
                {
                    lau8HwTestNoDuty_fl=200; HW_Test_HV_VL_fl = 1;
                    lau8HwTestNoDuty_fr=200; HW_Test_HV_VL_fr = 1;
                    lau8HwTestNoDuty_rl=200; HW_Test_HV_VL_rl = 1;
                    lau8HwTestNoDuty_rr=200; HW_Test_HV_VL_rr = 1;
                }
                else if((BrakeOn_cnt%(L_U16_TIME_5MSLOOP_5S+L_U8_TIME_5MSLOOP_1000MS))<L_U16_TIME_5MSLOOP_5S)
                {
                    if((BrakeOn_cnt%(L_U16_TIME_5MSLOOP_5S+L_U8_TIME_5MSLOOP_1000MS))%L_U8_TIME_5MSLOOP_100MS<=L_U8_TIME_5MSLOOP_20MS)
                    {
                        ;
                    }
                    else
                    {
                        if(las8HwTestMode==PRESS_EST_MODE_FL_by_MP) {lau8HwTestNoDuty_fl=200; HW_Test_HV_VL_fl = 1;}
                        if(las8HwTestMode==PRESS_EST_MODE_FR_by_MP) {lau8HwTestNoDuty_fr=200; HW_Test_HV_VL_fr = 1;}
                        if(las8HwTestMode==PRESS_EST_MODE_RL_by_MP) {lau8HwTestNoDuty_rl=200; HW_Test_HV_VL_rl = 1;}
                        if(las8HwTestMode==PRESS_EST_MODE_RR_by_MP) {lau8HwTestNoDuty_rr=200; HW_Test_HV_VL_rr = 1;}
                    }
                }
                else
                {
                    ;
                }
                
            }
            else
            {
                BrakeOn_cnt=0;
            }
        }
      #endif  
        else
        {
            ;
        }
        
        /* Copy commands */    
        if(TEST_CASE_1_SELECT_HV == 5) /* front wheels */
    	{
    	    laHwTestValveFL = laHwTestValveFR;
    	}
    	else if(TEST_CASE_1_SELECT_HV == 6) /* rear wheels */
    	{
    	    laHwTestValveRL = laHwTestValveRR;
    	}
    	else if(TEST_CASE_1_SELECT_HV == 7) /* all wheels with same duty : copy duty from FL */
    	{
    	    laHwTestValveFL = laHwTestValveFR;
    	    laHwTestValveRL = laHwTestValveFR;
    	    laHwTestValveRR = laHwTestValveFR;
    	}
    	else
    	{
            ;
    	}

        HV_VL_fl=HW_Test_HV_VL_fl;
        HV_VL_fr=HW_Test_HV_VL_fr;
        HV_VL_rl=HW_Test_HV_VL_rl;
        HV_VL_rr=HW_Test_HV_VL_rr;
        AV_VL_fl=HW_Test_AV_VL_fl;
        AV_VL_fr=HW_Test_AV_VL_fr;
        AV_VL_rl=HW_Test_AV_VL_rl;
        AV_VL_rr=HW_Test_AV_VL_rr;
        
      #if(__HW_TEST_MODULE_STRUCT_CHANGE==DISABLE)   
        FL.pwm_duty_temp = lau8HwTestNoDuty_fl;
        FR.pwm_duty_temp = lau8HwTestNoDuty_fr;
        RL.pwm_duty_temp = lau8HwTestNoDuty_rl;
        RR.pwm_duty_temp = lau8HwTestNoDuty_rr;
      #else
		LAHW_vDecideTestValveOutput(HW_Test_HV_VL_fl, HW_Test_AV_VL_fl, &laHwTestValveFL, &laHwTest_FL1HP, &laHwTest_FL2HP);
		LAHW_vDecideTestValveOutput(HW_Test_HV_VL_fr, HW_Test_AV_VL_fr, &laHwTestValveFR, &laHwTest_FR1HP, &laHwTest_FR2HP);
		LAHW_vDecideTestValveOutput(HW_Test_HV_VL_rl, HW_Test_AV_VL_rl, &laHwTestValveRL, &laHwTest_RL1HP, &laHwTest_RL2HP);
		LAHW_vDecideTestValveOutput(HW_Test_HV_VL_rr, HW_Test_AV_VL_rr, &laHwTestValveRR, &laHwTest_RR1HP, &laHwTest_RR2HP);
		
		la_FL1HP = laHwTest_FL1HP;
		la_FL2HP = laHwTest_FL2HP;
		la_FR1HP = laHwTest_FR1HP;
		la_FR2HP = laHwTest_FR2HP;
		la_RL1HP = laHwTest_RL1HP;
		la_RL2HP = laHwTest_RL2HP;
		la_RR1HP = laHwTest_RR1HP;
		la_RR2HP = laHwTest_RR2HP;
      #endif
        
      #if (__VDC && !__TCMF_ENABLE)  
        VDC_MOTOR_ON           = HW_Test_Motor_On;
       #if __SPLIT_TYPE 
        TCL_DEMAND_PRIMARY     = HW_Test_TC_NO_Prim;
        TCL_DEMAND_SECONDARY   = HW_Test_TC_NO_Secn;
        S_VALVE_PRIMARY        = HW_Test_ESV_Prim;
        S_VALVE_SECONDARY      = HW_Test_ESV_Secn;
       #else
        TCL_DEMAND_fr          = HW_Test_TC_NO_Prim;
        TCL_DEMAND_fl          = HW_Test_TC_NO_Secn;
        S_VALVE_RIGHT          = HW_Test_ESV_Prim;
        S_VALVE_LEFT           = HW_Test_ESV_Secn;
       #endif   
      #endif    
        
        VALVE_ACT=1;
    }   
    else
    {   
        LAABS_vResetWheelNoValveCommand();

        HW_test_cnt_t = 0;
        LFC_valve_test_cnt = 0;
        LFC_test_initial_duty = 0;
        LFC_test_duty = 0;
        LFC_test_duty_hold_time = 0;
        lau16HwTestCnt=0;
      #if __VDC  
        BrakeOn_cnt = 0;
      #endif  
    }
}

static void LAHW_vDecideTestValveOutput(int8_t s8HwTestHV, int8_t s8HwTestAV, LA_HWTEST_VALVE_t *laHwTestValve, WHEEL_VV_OUTPUT_t *laWL1HP,  WHEEL_VV_OUTPUT_t *laWL2HP)
{
	if(laHwTestValve->u8HwTestReapplyTime > 0) /* pulse up */
    {
    	LA_vSetWheelVvOnOffRiseOutput(laWL1HP, laWL2HP, laHwTestValve->u8HwTestReapplyTime);	
    }
    else if(laHwTestValve->u8HwTestDumpScanTime > 0) /* dump */
    {
    	LA_vSetWheelVvOnOffDumpOutput(laWL1HP, laWL2HP, laHwTestValve->u8HwTestDumpScanTime);
    }
    else
    {
    	if(s8HwTestHV==0)	/* LFC */
    	{
			laWL1HP->u8PwmDuty = laHwTestValve->u8HwTestPwmDuty;
			laWL2HP->u8PwmDuty = laHwTestValve->u8HwTestPwmDuty;
			laWL1HP->u8OutletOnTime = 0;
			laWL2HP->u8OutletOnTime = 0;	
    	}    	
    	else	/* hold */
    	{
    		if(s8HwTestAV==1)
    		{
    			LA_vSetWheelVvOnOffDumpOutput(laWL1HP, laWL2HP, U8_BASE_LOOPTIME);
    		}
    		else
    		{
    			LA_vSetWheelVvOnOffHoldOutput(laWL1HP, laWL2HP);
    		}
    	}	
    }
	
}

static void    LAABS_vResetWheelNoValveCommand(void)
{
    HW_Test_HV_VL_fl=0;
    HW_Test_HV_VL_fr=0;
    HW_Test_HV_VL_rl=0;
    HW_Test_HV_VL_rr=0;
    HW_Test_AV_VL_fl=0;
    HW_Test_AV_VL_fr=0;
    HW_Test_AV_VL_rl=0;
    HW_Test_AV_VL_rr=0;

    lau8HwTestNoDuty_fl=0;
    lau8HwTestNoDuty_fr=0;
    lau8HwTestNoDuty_rl=0;
    lau8HwTestNoDuty_rr=0;
    
    lau8HwTestFullRiseTimeFL=0;
    lau8HwTestFullRiseTimeFR=0;
    lau8HwTestFullRiseTimeRL=0;
    lau8HwTestFullRiseTimeRR=0;
        
	lau8HwTestDumpScanTimeFL=0;
	lau8HwTestDumpScanTimeFR=0;
	lau8HwTestDumpScanTimeRL=0;
	lau8HwTestDumpScanTimeRR=0;


  #if (__VDC && !__TCMF_ENABLE)  
    HW_Test_Motor_On=0;
    HW_Test_TC_NO_Prim = 0;
    HW_Test_TC_NO_Secn = 0;
    HW_Test_ESV_Prim = 0;
    HW_Test_ESV_Secn = 0;
  #endif    
    
    lau8HwTestABSLampRequest = 0;
}


#if __VDC
void    LAABS_vTestMotor(void)
{
    if((fu1TCSDisabledBySW==1)||(wstr > WSTR_10_DEG))
    {
        aux_msc_target_vol =  MSC_0_V;
    	lau8HwTestMscDuty = 0;
		las16HwTestMscOnTimeMsec = 0;
		las16HwTestMscOffTimeMsec = 0;
    
        if((las8HwTestMode>=40) && (las8HwTestMode<=59))            /* TC NO Valve Test Mode */
        {
            if(VDC_MOTOR_ON==1)
            {
                aux_msc_target_vol = MFC_TEST_MOTOR_TARGET_VOLTAGE;
            }
            else
            {
                aux_msc_target_vol = MSC_0_V;
            }
        }
        else if((las8HwTestMode>=100) && (las8HwTestMode<=125))     /* Extra Test Mode */
        {
            if(VDC_MOTOR_ON==1)
            {
                if((las8HwTestMode==PRESS_EST_MODE_FRONT) || (las8HwTestMode==PRESS_EST_MODE_REAR))
                {
                    TestMotorVoltage_cnt = HW_test_cnt_t%PRESS_EST_TEST_TOTAL_TIME;
                    
                    if(TestMotorVoltage_cnt < PRESS_EST_TEST_RISE_TIME)        /*  Motor 구동명령 */
                    {
                        aux_msc_target_vol = MSC_6_V;      /*  Rise Mode   */
                    }
                    else if(TestMotorVoltage_cnt < PRESS_EST_TEST_MOTOR_ON_TIME)   /*  Motor 구동명령 */
                    {
                        aux_msc_target_vol = MSC_3_V;      /*  Dump Mode   */ 
                    }
                    else
                    {
                        ;
                    }
                }
                else
                {
                    aux_msc_target_vol = MSC_3_V;
                }
            }
            else
            {
                aux_msc_target_vol = MSC_0_V;
            }
        }
        else if(las8HwTestMode==TACHOMETER_RPM_TEST_MODE)
        {
        	if(TACHOMETER_MOTOR_TAR_V_HOLD_T<=L_U8_TIME_5MSLOOP_1000MS)
        	{
	            if((HW_test_cnt_t%TACHOMETER_MOTOR_TAR_V_HOLD_T) == 0)
	            {
	                TestMotorVoltage_cnt = TestMotorVoltage_cnt+1;
	            }
	            else
	            {
	                ;
	            }
	        }
	        else
	        {
	            if((HW_test_cnt_t%TACHOMETER_MOTOR_TAR_V_HOLD_T) == L_U8_TIME_5MSLOOP_1000MS)
	            {
	                TestMotorVoltage_cnt = TestMotorVoltage_cnt+1;
	            }
	            else
	            {
	                ;
	            }
	        }	        	
            
            if((TestMotorVoltage_cnt<=TACHOMETER_MOTOR_TAR_V_STEP) && (TestMotorVoltage_cnt!=0))
            {
                aux_msc_target_vol = TestMotorVoltage_cnt*MSC_2_V;
				lau8HwTestMscDuty =  TestMotorVoltage_cnt*40;
            }
            else
            {
                ;
            }
        }
        else if(las8HwTestMode==BEMF_VS_MOTOR_RPM)
        {
        	if(BEMF_TEST_TOTAL_TIME<=L_U8_TIME_5MSLOOP_1000MS)
        	{
	            if((HW_test_cnt_t%BEMF_TEST_TOTAL_TIME) == 0)
	            {
	                TestMotorVoltage_cnt = TestMotorVoltage_cnt+1;
	            }
	            else
	            {
	                ;
	            }
	        }
	        else
	        {
	            if((HW_test_cnt_t%BEMF_TEST_TOTAL_TIME) == L_U8_TIME_5MSLOOP_1000MS)
	            {
	                TestMotorVoltage_cnt = TestMotorVoltage_cnt+1;
	            }
	            else
	            {
	                ;
	            }
	        }
            
            if(TestMotorVoltage_cnt<=BEMF_TEST_STEP)
            {
                if(((HW_test_cnt_t-L_U8_TIME_5MSLOOP_1000MS)%BEMF_TEST_TOTAL_TIME) <= BEMF_TEST_MOTOR_ON_TIME)
                {
                    aux_msc_target_vol = MSC_14_V;
                }
                else
                {
                    aux_msc_target_vol = MSC_0_V;
                }
            }
            else
            {
                ;
            }   
        }
        else if(las8HwTestMode==MOTOR_TARGET_V_VS_STEADY_ERROR)
        {
        	if(STEADY_ERROR_MOTOR_TAR_V_HOLD_T<=L_U8_TIME_5MSLOOP_1000MS)
        	{
	            if((HW_test_cnt_t%STEADY_ERROR_MOTOR_TAR_V_HOLD_T) == 0)
	            {
	                TestMotorVoltage_cnt = TestMotorVoltage_cnt+1;
	            }
	            else
	            {
	                ;
	            }
	        }
	        else
	        {
	            if((HW_test_cnt_t%STEADY_ERROR_MOTOR_TAR_V_HOLD_T) == L_U8_TIME_5MSLOOP_1000MS)
	            {
	                TestMotorVoltage_cnt = TestMotorVoltage_cnt+1;
	            }
	            else
	            {
	                ;
	            }
	        }
            
            if((TestMotorVoltage_cnt<=STEADY_ERROR_MOTOR_TAR_V_STEP) && (TestMotorVoltage_cnt !=0))
            {
                aux_msc_target_vol=TestMotorVoltage_cnt*MSC_1_V;
				lau8HwTestMscDuty = (TestMotorVoltage_cnt*20);
            }
            else
            {
                ;
            }   
        }
        else if(las8HwTestMode==MOTOR_TARGET_V_VS_RESPONSE)
        {
        	if(RESPONSE_MOTOR_TAR_V_HOLD_T<=L_U8_TIME_5MSLOOP_1000MS)
        	{
	            if((HW_test_cnt_t%RESPONSE_MOTOR_TAR_V_HOLD_T) == 0)
	            {
	                TestMotorVoltage_cnt = TestMotorVoltage_cnt+1;
	            }
	            else
	            {
	                ;
	            }
	        }
	        else
	        {
	            if((HW_test_cnt_t%RESPONSE_MOTOR_TAR_V_HOLD_T) == L_U8_TIME_5MSLOOP_1000MS)
	            {
	                TestMotorVoltage_cnt = TestMotorVoltage_cnt+1;
	            }
	            else
	            {
	                ;
	            }
	        }
            
            if(TestMotorVoltage_cnt<=6)
            {
                if(TestMotorVoltage_cnt==0)
                {
                    aux_msc_target_vol=MSC_0_V;
                }
                else if(TestMotorVoltage_cnt==1)
                {
                    aux_msc_target_vol=MSC_4_V;
                }
                else if(TestMotorVoltage_cnt==2)
                {
                    aux_msc_target_vol=MSC_0_V;
                }
                else if(TestMotorVoltage_cnt==3)
                {
                    aux_msc_target_vol=MSC_8_V;
                }
                else if(TestMotorVoltage_cnt==4)
                {
                    aux_msc_target_vol=MSC_0_V;
                }
                else if(TestMotorVoltage_cnt==5)
                {
                    aux_msc_target_vol=MSC_4_V;
                }
                else if(TestMotorVoltage_cnt==6)
                {
                    aux_msc_target_vol=MSC_8_V;
                }
                else
                {
                    aux_msc_target_vol=MSC_0_V;
                }
            }
            else
            {
                aux_msc_target_vol=MSC_0_V;
            }
        }
        else if(las8HwTestMode==DUTY_TARGET_V_RESPONSE)
        {
        	if(RESPONSE_MOTOR_TAR_V_HOLD_T<=L_U8_TIME_5MSLOOP_1000MS)
        	{
	            if((HW_test_cnt_t%RESPONSE_MOTOR_TAR_V_HOLD_T) == 0)
	            {
	                TestMotorVoltage_cnt = TestMotorVoltage_cnt+1;
	            }
	            else
	            {
	                ;
	            }
	        }
	        else
	        {
	            if((HW_test_cnt_t%RESPONSE_MOTOR_TAR_V_HOLD_T) == L_U8_TIME_5MSLOOP_1000MS)
	            {
	                TestMotorVoltage_cnt = TestMotorVoltage_cnt+1;
	            }
	            else
	            {
	                ;
	            }
	        }
            
            if(TestMotorVoltage_cnt<=12)
            {
                if(TestMotorVoltage_cnt==0)
                {
					aux_msc_target_vol = 0;
                    lau8HwTestMscDuty = 0;
                }
                else if(TestMotorVoltage_cnt==1)
                {
					aux_msc_target_vol = MSC_7_V;
                    lau8HwTestMscDuty = 100;
                }
                else if(TestMotorVoltage_cnt==2)
                {
					aux_msc_target_vol = MSC_11_V;
                    lau8HwTestMscDuty = 160;
                }
                else if(TestMotorVoltage_cnt==3)
                {
					aux_msc_target_vol = MSC_7_V;
                    lau8HwTestMscDuty = 100;
                }
                else if(TestMotorVoltage_cnt==4)
                {
					aux_msc_target_vol = 0;
                    lau8HwTestMscDuty = 0;
                }
                else if(TestMotorVoltage_cnt==5)
                {
					aux_msc_target_vol = 0;
                    lau8HwTestMscDuty = 0;
                }
                else if(TestMotorVoltage_cnt==6)
                {
					aux_msc_target_vol = MSC_11_V;
                    lau8HwTestMscDuty = 160;
                }
                else if(TestMotorVoltage_cnt==7)
                {
					aux_msc_target_vol = MSC_7_V;
                    lau8HwTestMscDuty = 100;
                }
                else if(TestMotorVoltage_cnt==8)
                {
					aux_msc_target_vol = MSC_11_V;
                    lau8HwTestMscDuty = 160;
                }
                else if(TestMotorVoltage_cnt==9)
                {
					aux_msc_target_vol = 0;
                    lau8HwTestMscDuty = 0;
                }
                else if(TestMotorVoltage_cnt==10)
                {
					aux_msc_target_vol = 0;
                    lau8HwTestMscDuty = 0;
                }
                else if(TestMotorVoltage_cnt==11)
                {
					aux_msc_target_vol = MSC_14_V;
                    lau8HwTestMscDuty = 200;
                }
                else if(TestMotorVoltage_cnt==12)
                {
					aux_msc_target_vol = 0;
                    lau8HwTestMscDuty = 0;
                }
            }
            else
            {
                lau8HwTestMscDuty=0;
            }
        }
         else if(las8HwTestMode==DUTY_ADAPTATION_TEST_MODE)
        {
        	if(INITIAL_DUTY_TAR_V_HOLD_T<=L_U8_TIME_5MSLOOP_1000MS)
        	{
	            if((HW_test_cnt_t%INITIAL_DUTY_TAR_V_HOLD_T) == 0)
	            {
	                TestMotorVoltage_cnt = TestMotorVoltage_cnt+1;
	            }
	            else
	            {
	                ;
	            }
	        }
	        else
	        {
	            if((HW_test_cnt_t%INITIAL_DUTY_TAR_V_HOLD_T) == 0)
	            {
	                TestMotorVoltage_cnt = TestMotorVoltage_cnt+1;
	            }
	            else
	            {
	                ;
	            }
	        }	        	
            
            if(TestMotorVoltage_cnt>0 && TestMotorVoltage_cnt<=9)
            {
                if(TestMotorVoltage_cnt==1)
                {
                    aux_msc_target_vol = MSC_2_V;
                }
                else if(TestMotorVoltage_cnt==2)
                {
                    aux_msc_target_vol=MSC_0_V;
                }
                else if(TestMotorVoltage_cnt==3)
                {
                    aux_msc_target_vol = MSC_0_V;
                }
                else if(TestMotorVoltage_cnt==4)
                {
                    aux_msc_target_vol = MSC_4_V;
                }
                else if(TestMotorVoltage_cnt==5)
                {
                    aux_msc_target_vol = MSC_0_V;
                }
                else if(TestMotorVoltage_cnt==6)
                {
                    aux_msc_target_vol = MSC_0_V;
                }
                else if(TestMotorVoltage_cnt==7)
                {
                    aux_msc_target_vol = MSC_10_V;
                }
                else if(TestMotorVoltage_cnt==8)
                {
                    aux_msc_target_vol = MSC_0_V;
                }
                else if(TestMotorVoltage_cnt==9)
                {
                    aux_msc_target_vol= MSC_0_V;
                }
            }
            else
            {
                aux_msc_target_vol=0;
            }
        }
        else if(las8HwTestMode==MOTOR_TARGET_V_VS_RISE_RATE)
        {
        	if ((HW_test_cnt_t > L_U8_TIME_10MSLOOP_2S) && (HW_test_cnt_t <= L_U8_TIME_10MSLOOP_2S + TEST_CASE_25_MOTOR_ON_TIME))
        	{
        		aux_msc_target_vol = TEST_CASE_25_MOTOR_TARGET_FINAL*140;
	        	lau8HwTestMscDuty = TEST_CASE_25_MOTOR_TARGET_FINAL*2;
	        	
			    switch(TEST_CASE_25_MOTOR_TARGET_FINAL)
        {
			        case 0:
		        		las16HwTestMscOnTimeMsec = 0;
		        		las16HwTestMscOffTimeMsec = 0;
			            break;
			        case 10:
		        		las16HwTestMscOnTimeMsec = 4;
		        		las16HwTestMscOffTimeMsec = 36;
			            break;
			        case 20:
		        		las16HwTestMscOnTimeMsec = 4;
		        		las16HwTestMscOffTimeMsec = 16;
			            break;
			        case 30:
		        		las16HwTestMscOnTimeMsec = 6;
		        		las16HwTestMscOffTimeMsec = 14;
			            break;
			        case 40:
		        		las16HwTestMscOnTimeMsec = 8;
		        		las16HwTestMscOffTimeMsec = 12;
			            break;
			        case 50:
		        		las16HwTestMscOnTimeMsec = 10;
		        		las16HwTestMscOffTimeMsec = 10;
			            break;
			        case 60:
		        		las16HwTestMscOnTimeMsec = 12;
		        		las16HwTestMscOffTimeMsec = 8;
			            break;
			        case 70:
		        		las16HwTestMscOnTimeMsec = 14;
		        		las16HwTestMscOffTimeMsec = 6;
			            break;
			        case 80:
		        		las16HwTestMscOnTimeMsec = 16;
		        		las16HwTestMscOffTimeMsec = 4;
			            break;
			        case 90:
		        		las16HwTestMscOnTimeMsec = 36;
		        		las16HwTestMscOffTimeMsec = 4;
			            break;
			        case 100:
		        		las16HwTestMscOnTimeMsec = 100;
		        		las16HwTestMscOffTimeMsec = 0;
			            break;
			        default :
		        		las16HwTestMscOnTimeMsec = 0;
		        		las16HwTestMscOffTimeMsec = 0;
			            break;
			    }
        }
        	else{}
        }
        else{}
    }
    else
    {
        aux_msc_target_vol = MSC_0_V;
        TestMotorVoltage_cnt = 0;
    	lau8HwTestMscDuty = 0;
		las16HwTestMscOnTimeMsec = 0;
		las16HwTestMscOffTimeMsec = 0;
    }
    
}
#endif

#if (__VDC && __TCMF_ENABLE)
void    LAMFC_vTestTcNoValve(void)
{

    uint16_t HSA_RAMP_OUT_RATE=0;
    uint16_t HSA_RAMP_OUT_CURRENT=0;
    if(fu1ESCDisabledBySW==1)
    {
        HW_Test_Motor_On=0;
        HW_Test_TC_NO_Prim = 0;
        HW_Test_TC_NO_Secn = 0;
        HW_Test_ESV_Prim = 0;
        HW_Test_ESV_Secn = 0;

        if(las8HwTestMode == HSA_TEST_CURRENT)
        {
            valve_test_cnt = (HW_test_cnt_t%2000);
            HSA_RAMP_OUT_RATE = (HW_test_cnt_t/2000);
            MFC_TEST_INITIAL_CURRENT = MGH60_80BAR_DUTY;
    
            if((valve_test_cnt>L_U8_TIME_5MSLOOP_500MS) && (MFC_TEST_INITIAL_CURRENT>20) && (HSA_RAMP_OUT_RATE<HSA_TEST_PATTERN))
            {
                HW_Test_TC_NO_Prim = 1;
                HW_Test_TC_NO_Secn = 1;
    
                if(valve_test_cnt<MFC_TEST_HSA_RISE_TIME)
                {
                    lau8HwTestTcNoDuty = MFC_TEST_INITIAL_CURRENT;
                    lau8HwTestEsvDuty = ESV_DRIVE_DUTY_MIN;
                    HW_Test_Motor_On = 1;
                    HW_Test_ESV_Prim = 1;
                    HW_Test_ESV_Secn = 1;
                }
                else if(valve_test_cnt<(MFC_TEST_HSA_RISE_TIME+L_U8_TIME_5MSLOOP_35MS))
                {
                    lau8HwTestTcNoDuty = MGH60_160BAR_DUTY;
                    lau8HwTestEsvDuty = 0;
                }
                else if(valve_test_cnt<(MFC_TEST_HSA_RISE_TIME+L_U16_TIME_5MSLOOP_1500MS))
                {
                    lau8HwTestTcNoDuty = MFC_TEST_INITIAL_CURRENT+MGH60_delta_10BAR_DUTY;
                    lau8HwTestEsvDuty = 0;
                }
                else if(valve_test_cnt==(MFC_TEST_HSA_RISE_TIME+L_U16_TIME_5MSLOOP_1500MS))
                {
                    lau8HwTestTcNoDuty = MFC_TEST_INITIAL_CURRENT;
                    lau8HwTestEsvDuty = 0;
                }
                else if(valve_test_cnt<=(MFC_TEST_HSA_RISE_TIME+L_U16_TIME_5MSLOOP_1500MS+((HSA_RAMP_OUT_RATE+1)*T_1000_MS)))
                {
                    HSA_RAMP_OUT_CURRENT = ((HSA_RAMP_OUT_RATE+1)*T_1000_MS)/(MGH60_80BAR_DUTY-MGH60_0BAR_DUTY);
                    
                    if(((valve_test_cnt - MFC_TEST_HSA_RISE_TIME - L_U16_TIME_5MSLOOP_1500MS)%HSA_RAMP_OUT_CURRENT)==1)
                    {
                        lau8HwTestTcNoDuty = lau8HwTestTcNoDuty - 1;
                        lau8HwTestEsvDuty = 0;
                        if(lau8HwTestTcNoDuty < MGH60_0BAR_DUTY) 
                        {
                        	lau8HwTestTcNoDuty = MGH60_0BAR_DUTY;
                        }
                    }
                    else
                    {
                        /*lau8HwTestTcNoDuty = lau8HwTestTcNoDuty;*/
                        lau8HwTestEsvDuty = 0;
                    }
                }
                else
                {
                    lau8HwTestTcNoDuty = 0;
                    lau8HwTestEsvDuty = 0;
                }
            }
            else
            {
                lau8HwTestTcNoDuty = 0;
                lau8HwTestEsvDuty = 0;
            }
        }
        else if(las8HwTestMode == DITHER_MODE_CURRENT)
        {
            if(HW_test_cnt_t <= 10)
            {
                lau8HwTestTcNoDuty = 0;
                lau8HwTestEsvDuty = 0;
                
                MFC_TEST_INITIAL_CURRENT = MGH60_0BAR_DUTY - (MGH60_0BAR_DUTY%5);
                MFC_TEST_20BAR_CURRENT = MGH60_20BAR_DUTY - (MGH60_20BAR_DUTY%5) + 5;
                
                MFC_TEST_MODE_P_B_20BAR = (MFC_TEST_20BAR_CURRENT-MFC_TEST_INITIAL_CURRENT)/MFC_TEST_DIFF_DUTY_DITHER + 1;

                MFC_TEST_MODE_P_UP_Time = (MFC_TEST_MODE_P_B_20BAR*MFC_TEST_HOLD_DITHER);
                MFC_TEST_MODE_P_DOWN_Time = ((MFC_TEST_MODE_P_B_20BAR-1)*MFC_TEST_HOLD_DITHER);
            }
            else if(HW_test_cnt_t <= (L_U8_TIME_5MSLOOP_70MS + MFC_TEST_MODE_P_UP_Time))               /* Up Stair */
            {
                valve_test_cnt = (HW_test_cnt_t - L_U8_TIME_5MSLOOP_70MS)%MFC_TEST_HOLD_DITHER;
                
                if(valve_test_cnt==1)
                {
                    if(lau8HwTestTcNoDuty==0)
                    {
                        lau8HwTestTcNoDuty = MFC_TEST_INITIAL_CURRENT;
                    }
                    else
                    {
                        lau8HwTestTcNoDuty = lau8HwTestTcNoDuty + MFC_TEST_DIFF_DUTY_DITHER;
                    }
                }                   
                
                lau8HwTestEsvDuty = ESV_DRIVE_DUTY_MIN;

                HW_Test_TC_NO_Prim = 1;
                HW_Test_TC_NO_Secn = 1;
                HW_Test_Motor_On = 1;
                HW_Test_ESV_Prim = 1;
                HW_Test_ESV_Secn = 1;
            }
            else if(HW_test_cnt_t <= (L_U8_TIME_5MSLOOP_70MS + MFC_TEST_MODE_P_UP_Time + MFC_TEST_MODE_P_DOWN_Time))              /* Down Stair */
            {
                valve_test_cnt = (HW_test_cnt_t - (L_U8_TIME_5MSLOOP_70MS + MFC_TEST_MODE_P_UP_Time))%MFC_TEST_HOLD_DITHER;
                
                if(valve_test_cnt==1)
                {
                    lau8HwTestTcNoDuty = lau8HwTestTcNoDuty - MFC_TEST_DIFF_DUTY_DITHER;
                }   

                lau8HwTestEsvDuty = ESV_DRIVE_DUTY_MIN;

                HW_Test_TC_NO_Prim = 1;
                HW_Test_TC_NO_Secn = 1;
                HW_Test_Motor_On = 1;
                HW_Test_ESV_Prim = 1;
                HW_Test_ESV_Secn = 1;   
            }
            else
            {
                lau8HwTestTcNoDuty = 0;
                lau8HwTestEsvDuty = 0;
            }
        }
        else if((las8HwTestMode == PIRAMID_MODE_CURRENT1) || (las8HwTestMode == PIRAMID_MODE_CURRENT2))
        {
            if(HW_test_cnt_t <= L_U8_TIME_5MSLOOP_70MS)
            {
                lau8HwTestTcNoDuty = 0;
                lau8HwTestEsvDuty = 0;
                valve_test_cnt = HW_test_cnt_t;
                
                MFC_TEST_INITIAL_CURRENT = MGH60_0BAR_DUTY - (MGH60_0BAR_DUTY%5);
                MFC_TEST_20BAR_CURRENT = MGH60_20BAR_DUTY - (MGH60_20BAR_DUTY%5) + 5;

                MFC_TEST_MODE_P_B_20BAR = (MFC_TEST_20BAR_CURRENT-MFC_TEST_INITIAL_CURRENT)/MFC_TEST_DIFF_DUTY_B_20BAR + 1;
                if(MFC_TEST_MAX_CURRENT_P>MGH60_160BAR_DUTY)
                {
                    MFC_TEST_MODE_P_AB_20BAR = (MGH60_160BAR_DUTY - MFC_TEST_20BAR_CURRENT)/MFC_TEST_DIFF_DUTY_AB_20BAR + 1;
                }
                else
                {
                    MFC_TEST_MODE_P_AB_20BAR = (MFC_TEST_MAX_CURRENT_P - MFC_TEST_20BAR_CURRENT)/MFC_TEST_DIFF_DUTY_AB_20BAR + 1;
                }        
            
                MFC_TEST_MODE_P_UP_Time = (MFC_TEST_MODE_P_B_20BAR*MFC_TEST_HOLD_TIME_P_U_B_20BAR) + (MFC_TEST_MODE_P_AB_20BAR*MFC_TEST_HOLD_TIME_P_U_AB_20BAR);
                MFC_TEST_MODE_P_DOWN_Time = (MFC_TEST_MODE_P_B_20BAR*MFC_TEST_HOLD_TIME_P_D_B_20BAR) + ((MFC_TEST_MODE_P_AB_20BAR-1)*MFC_TEST_HOLD_TIME_P_D_AB_20BAR);

            }
            else if(HW_test_cnt_t <= (L_U8_TIME_5MSLOOP_70MS + MFC_TEST_MODE_P_UP_Time))               /* Up Stair */
            {
                if(HW_test_cnt_t<=(L_U8_TIME_5MSLOOP_70MS+(MFC_TEST_MODE_P_B_20BAR*MFC_TEST_HOLD_TIME_P_U_B_20BAR)))
                {
                    valve_test_cnt = (HW_test_cnt_t - L_U8_TIME_5MSLOOP_70MS)%MFC_TEST_HOLD_TIME_P_U_B_20BAR;
                    
                    if(valve_test_cnt==1)
                    {
                        if(lau8HwTestTcNoDuty==0)
                        {
                            lau8HwTestTcNoDuty = MFC_TEST_INITIAL_CURRENT;
                        }
                        else
                        {
                            lau8HwTestTcNoDuty = lau8HwTestTcNoDuty + MFC_TEST_DIFF_DUTY_B_20BAR;
                        }
                    }
                }
                else
                {
                    valve_test_cnt = (HW_test_cnt_t - L_U8_TIME_5MSLOOP_70MS - (MFC_TEST_MODE_P_B_20BAR*MFC_TEST_HOLD_TIME_P_U_B_20BAR))%MFC_TEST_HOLD_TIME_P_U_AB_20BAR;
                    
                    if(valve_test_cnt==1)
                    {
                        lau8HwTestTcNoDuty = lau8HwTestTcNoDuty + MFC_TEST_DIFF_DUTY_AB_20BAR;
                    }                   
                }
                
                lau8HwTestEsvDuty = ESV_DRIVE_DUTY_MIN;

                HW_Test_TC_NO_Prim = 1;
                HW_Test_TC_NO_Secn = 1;
                HW_Test_Motor_On = 1;
                HW_Test_ESV_Prim = 1;
                HW_Test_ESV_Secn = 1;
                
                if(las8HwTestMode == PIRAMID_MODE_CURRENT2)
                {
                	if(HW_test_cnt_t >= ((L_U8_TIME_5MSLOOP_70MS + MFC_TEST_MODE_P_UP_Time) - (MFC_TEST_HOLD_TIME_P_U_B_20BAR*2/3)))
                	{
                		HW_Test_Motor_On = 0;
                	}
                }
            }
            else if(HW_test_cnt_t <= (L_U8_TIME_5MSLOOP_70MS + MFC_TEST_MODE_P_UP_Time + MFC_TEST_MODE_P_DOWN_Time))              /* Down Stair */
            {
                if(HW_test_cnt_t<=(L_U8_TIME_5MSLOOP_70MS+MFC_TEST_MODE_P_UP_Time+((MFC_TEST_MODE_P_AB_20BAR-1)*MFC_TEST_HOLD_TIME_P_D_AB_20BAR)))
                {
                    valve_test_cnt = (HW_test_cnt_t - (L_U8_TIME_5MSLOOP_70MS+MFC_TEST_MODE_P_UP_Time))%MFC_TEST_HOLD_TIME_P_D_AB_20BAR;
                    
                    if(valve_test_cnt==1)
                    {
                        lau8HwTestTcNoDuty = lau8HwTestTcNoDuty - MFC_TEST_DIFF_DUTY_AB_20BAR;
                    }
                }
                else
                {
                    valve_test_cnt = (HW_test_cnt_t - (L_U8_TIME_5MSLOOP_70MS+MFC_TEST_MODE_P_UP_Time+((MFC_TEST_MODE_P_AB_20BAR-1)*MFC_TEST_HOLD_TIME_P_D_AB_20BAR)))%MFC_TEST_HOLD_TIME_P_D_B_20BAR;
                    
                    if(valve_test_cnt==1)
                    {
                        lau8HwTestTcNoDuty = lau8HwTestTcNoDuty - MFC_TEST_DIFF_DUTY_B_20BAR;
                    }
                }
                    
                if(las8HwTestMode == PIRAMID_MODE_CURRENT1)
                {
                    lau8HwTestEsvDuty = ESV_DRIVE_DUTY_MIN;
                    HW_Test_ESV_Prim = 1;
                    HW_Test_ESV_Secn = 1;   
                    HW_Test_Motor_On = 1;
                }
                else
                {
                    lau8HwTestEsvDuty = 0;
                    HW_Test_ESV_Prim = 0;
                    HW_Test_ESV_Secn = 0;
                    HW_Test_Motor_On = 0;   
                }

                HW_Test_TC_NO_Prim = 1;
                HW_Test_TC_NO_Secn = 1;
               
            }
            else
            {
                lau8HwTestTcNoDuty = 0;
                lau8HwTestEsvDuty = 0;
            }
        }
        else if(las8HwTestMode==PRESS_EST_MODE_FRONT)
        {
            valve_test_cnt = HW_test_cnt_t%PRESS_EST_TEST_TOTAL_TIME;
            
            if(HW_test_cnt_t>=(PRESS_EST_TEST_TOTAL_TIME*PRESS_EST_TEST_PULSE))
            {
                lau8HwTestTcNoDuty = 0;
                lau8HwTestEsvDuty = 0;
            }
            else if(valve_test_cnt<T_50_MS)
            {
                HW_Test_Motor_On = 1;
                lau8HwTestTcNoDuty = 0;
                lau8HwTestEsvDuty = 0;
            }
            else if(valve_test_cnt<PRESS_EST_TEST_MOTOR_ON_TIME)
            {
                HW_Test_Motor_On = 1;
                lau8HwTestTcNoDuty = MFC_CURRENT_PRESS_EST;
                lau8HwTestEsvDuty = ESV_DRIVE_DUTY_MIN;
                
                if(((HW_test_cnt_t/PRESS_EST_TEST_TOTAL_TIME)%2)==0)    /* FL */
                {
                  #if __SPLIT_TYPE                  /* FR Split  */
                    HW_Test_TC_NO_Prim = 1;
                    HW_Test_ESV_Prim = 1;
                  #else                             /* X Split  */
                    HW_Test_TC_NO_Secn = 1;
                    HW_Test_ESV_Secn = 1;
                  #endif 
                }
                else                                /* FR */
                {
                    HW_Test_TC_NO_Prim = 1;
                    HW_Test_ESV_Prim = 1;
                }
            }
            else
            {
                lau8HwTestTcNoDuty = 0;
                lau8HwTestEsvDuty = 0;
            }
        }
        else if(las8HwTestMode==PRESS_EST_MODE_REAR)
        {
            valve_test_cnt = HW_test_cnt_t%PRESS_EST_TEST_TOTAL_TIME;
            
            if(HW_test_cnt_t>=(PRESS_EST_TEST_TOTAL_TIME*PRESS_EST_TEST_PULSE))
            {
                lau8HwTestTcNoDuty = 0;
                lau8HwTestEsvDuty = 0;
            }
            else if(valve_test_cnt<T_50_MS)
            {
                HW_Test_Motor_On = 1;
                lau8HwTestTcNoDuty = 0;
                lau8HwTestEsvDuty = 0;
            }
            else if(valve_test_cnt<PRESS_EST_TEST_MOTOR_ON_TIME)
            {
                HW_Test_Motor_On = 1;
                lau8HwTestTcNoDuty = MFC_CURRENT_PRESS_EST;
                lau8HwTestEsvDuty = ESV_DRIVE_DUTY_MIN;
                
                if(((HW_test_cnt_t/PRESS_EST_TEST_TOTAL_TIME)%2)==0)    /* RL */
                {
                  #if __SPLIT_TYPE                  /* FR Split  */
                    HW_Test_TC_NO_Secn = 1;
                    HW_Test_ESV_Secn = 1;
                  #else                             /* X Split  */
                    HW_Test_TC_NO_Prim = 1;
                    HW_Test_ESV_Prim = 1;
                  #endif 
                }
                else                                /* RR */
                {
                    HW_Test_TC_NO_Secn = 1;
                    HW_Test_ESV_Secn = 1;
                }
            }
            else
            {
                lau8HwTestTcNoDuty = 0;
                lau8HwTestEsvDuty = 0;
            }
        }
        else if(las8HwTestMode==TACHOMETER_RPM_TEST_MODE)
        {
            if(HW_test_cnt_t < T_1000_MS)
            {
                lau8HwTestTcNoDuty = 0;
                lau8HwTestEsvDuty = 0;
            }
            else if(HW_test_cnt_t<=((TACHOMETER_MOTOR_TAR_V_STEP*TACHOMETER_MOTOR_TAR_V_HOLD_T)+T_2_S))
            {
                HW_Test_TC_NO_Prim = 1;
                HW_Test_TC_NO_Secn = 1;
                HW_Test_ESV_Prim = 1;
                HW_Test_ESV_Secn = 1;
                HW_Test_Motor_On = 1;
                lau8HwTestTcNoDuty = MFC_20BAR_DUTY_for_TACHO_TEST;
                lau8HwTestEsvDuty = ESV_DRIVE_DUTY_MIN;
            }
            else
            {
                lau8HwTestTcNoDuty = 0;
                lau8HwTestEsvDuty = 0;
            }
        }
        else if(las8HwTestMode==MOTOR_TARGET_V_VS_STEADY_ERROR)
        {
            if(HW_test_cnt_t < T_1000_MS)
            {
                lau8HwTestTcNoDuty = 0;
                lau8HwTestEsvDuty = 0;
            }
            else if(HW_test_cnt_t<=((STEADY_ERROR_MOTOR_TAR_V_STEP*STEADY_ERROR_MOTOR_TAR_V_HOLD_T)+T_2_S))
            {
                HW_Test_TC_NO_Prim = 1;
                HW_Test_TC_NO_Secn = 1;
                HW_Test_ESV_Prim = 1;
                HW_Test_ESV_Secn = 1;
                HW_Test_Motor_On = 1;
                lau8HwTestTcNoDuty = MFC_DUTY_for_MOTOR_STEADY_ERROR;
                lau8HwTestEsvDuty = ESV_DRIVE_DUTY_MIN;
            }
            else
            {
                lau8HwTestTcNoDuty = 0;
                lau8HwTestEsvDuty = 0;
            }
        }
        else if(las8HwTestMode==MOTOR_TARGET_V_VS_RESPONSE)
        {
            if(HW_test_cnt_t < T_1000_MS)
            {
                lau8HwTestTcNoDuty = 0;
                lau8HwTestEsvDuty = 0;
            }
            else if(HW_test_cnt_t<=((12*RESPONSE_MOTOR_TAR_V_HOLD_T)+T_1000_MS))
            {
                HW_Test_TC_NO_Prim = 1;
                HW_Test_TC_NO_Secn = 1;
                HW_Test_ESV_Prim = 1;
                HW_Test_ESV_Secn = 1;
                HW_Test_Motor_On = 1;
                lau8HwTestTcNoDuty = MFC_DUTY_for_MOTOR_RESPONSE;
                lau8HwTestEsvDuty = ESV_DRIVE_DUTY_MIN;
            }
            else
            {
                lau8HwTestTcNoDuty = 0;
                lau8HwTestEsvDuty = 0;
            }
        }
        else if(las8HwTestMode==DUTY_TARGET_V_RESPONSE)
        {
            if(HW_test_cnt_t < T_1000_MS)
            {
                lau8HwTestTcNoDuty = 0;
                lau8HwTestEsvDuty = 0;
            }
            else if(HW_test_cnt_t<=((12*RESPONSE_MOTOR_TAR_V_HOLD_T)+T_1000_MS))
            {
                HW_Test_TC_NO_Prim = 1;
                HW_Test_TC_NO_Secn = 1;
                HW_Test_ESV_Prim = 1;
                HW_Test_ESV_Secn = 1;
                HW_Test_Motor_On = 1;
                lau8HwTestTcNoDuty = MFC_DUTY_for_MOTOR_RESPONSE;
                lau8HwTestEsvDuty = ESV_DRIVE_DUTY_MIN;
            }
            else
            {
                lau8HwTestTcNoDuty = 0;
                lau8HwTestEsvDuty = 0;
            }
        }
        else if(las8HwTestMode==DUTY_ADAPTATION_TEST_MODE)
        {
            if(HW_test_cnt_t < T_1000_MS)
            {
                lau8HwTestTcNoDuty = 0;
                lau8HwTestEsvDuty = 0;
            }
            else if(HW_test_cnt_t<=((INITIAL_DUTY_TAR_V_HOLD_T*9)+T_1000_MS))
            {
                HW_Test_TC_NO_Prim = 1;
                HW_Test_TC_NO_Secn = 1;
                HW_Test_ESV_Prim = 1;
                HW_Test_ESV_Secn = 1;
                HW_Test_Motor_On = 1;
                lau8HwTestTcNoDuty = MFC_DUTY_for_MOTOR_STEADY_ERROR;
                lau8HwTestEsvDuty = ESV_DRIVE_DUTY_MIN;
            }
            else
            {
                lau8HwTestTcNoDuty = 0;
                lau8HwTestEsvDuty = 0;
            }
        }
        else if(las8HwTestMode==MOTOR_TARGET_V_VS_RISE_RATE)
        {
        	if(HW_test_cnt_t <= L_U8_TIME_10MSLOOP_1000MS) // step 1 none
        	{
        		lau8HwTestTcNoDuty = 0;
                lau8HwTestEsvDuty = 0;
        	}
        	else if(HW_test_cnt_t <= L_U8_TIME_10MSLOOP_2S)//step 2 TC on, esv on, Motor off
        	{
        		
        		if((TEST_MODE_SUB==1) || (TEST_MODE_SUB==3)) /* primary, all wheel */
       			{
	        		HW_Test_TC_NO_Prim = 1;
		            HW_Test_ESV_Prim = 1;
		            lau8HwTestTcNoDuty = TC_CURRENT_MAX;
		            lau8HwTestEsvDuty = U8_ESV_DRIVE_DUTY_MAX;
		        }
       			if((TEST_MODE_SUB==2) || (TEST_MODE_SUB==3)) /* secondary, all wheel */
       			{
		            HW_Test_TC_NO_Secn = 1;
		            HW_Test_ESV_Secn = 1;
		            lau8HwTestTcNoDuty = TC_CURRENT_MAX;
		            lau8HwTestEsvDuty = U8_ESV_DRIVE_DUTY_MAX;
		        }
       			if((TEST_MODE_SUB>=1) && (TEST_MODE_SUB<=3))
       			{
       				lau8HwTestABSLampRequest = 1; /* ABS W/L */
		        }
        	}
        	else if(HW_test_cnt_t <= L_U8_TIME_10MSLOOP_2S + TEST_CASE_25_MOTOR_ON_TIME)//step 3 TC on, ESV off, Motor on
        	{
        		if((TEST_MODE_SUB==1) || (TEST_MODE_SUB==3)) /* primary, all wheel */
       			{
	        		HW_Test_TC_NO_Prim = 1;
	        		HW_Test_ESV_Prim = 1;
		        	lau8HwTestTcNoDuty = TC_CURRENT_MAX;
		        	lau8HwTestEsvDuty = U8_ESV_DRIVE_DUTY_MAX;
		        }
       			if((TEST_MODE_SUB==2) || (TEST_MODE_SUB==3)) /* secondary, all wheel */
       			{
		            HW_Test_TC_NO_Secn = 1;
		            HW_Test_ESV_Secn = 1;
		            lau8HwTestTcNoDuty = TC_CURRENT_MAX;
		            lau8HwTestEsvDuty = U8_ESV_DRIVE_DUTY_MAX;
		        }
       			if((TEST_MODE_SUB>=1) && (TEST_MODE_SUB<=3))
       			{
       				lau8HwTestABSLampRequest = 1; /* ABS W/L */
       				HW_Test_Motor_On = 1;
		        }
        	}
        	else{}
        }
        else if(las8HwTestMode==TC_ON_OFF_REPEAT)
        {
            if(HW_test_cnt_t > (TC_ON_TIME*TC_ON_OFF_AMOUNT))
            {
                lau8HwTestTcNoDuty = 0;
            }
            else
            {
            	if((HW_test_cnt_t%TC_ON_TIME)==0)
            	{
                	if(lau8HwTestTcNoDuty==0) {lau8HwTestTcNoDuty = TC_ON_DUTY;}
                	else                      {lau8HwTestTcNoDuty = 0;}
                }
                
                if(lau8HwTestTcNoDuty > 0)
                {
	                HW_Test_TC_NO_Prim = 1;
	                HW_Test_TC_NO_Secn = 1;
	            }
            }
        }
        else if(las8HwTestMode==NC_OPEN_TIME_VS_DUMP_RATE)
        {
			if(HW_test_cnt_t < L_U8_TIME_10MSLOOP_1000MS)
			{	/* ESV open */
        		HW_Test_ESV_Prim = 1;
        		HW_Test_ESV_Secn = 1;
        		lau8HwTestEsvDuty = U8_ESV_DRIVE_DUTY_MAX;
			}
		}
        else if(las8HwTestMode==TC_CURRENT_VS_PRESSURE_HOLD)
        {
       		if((lau16HwTestCnt==0) && (wstr < WSTR_30_DEG))
       		{	
       			if((TEST_MODE_SUB==1) || (TEST_MODE_SUB==3)) /* primary, all wheel */
       			{
	        		HW_Test_TC_NO_Prim = 1;
		            HW_Test_ESV_Prim = 1;
		            lau8HwTestTcNoDuty = TC_CURRENT_MAX;
		            lau8HwTestEsvDuty = U8_ESV_DRIVE_DUTY_MAX;
		        }
       			if((TEST_MODE_SUB==2) || (TEST_MODE_SUB==3)) /* secondary, all wheel */
       			{
		            HW_Test_TC_NO_Secn = 1;
		            HW_Test_ESV_Secn = 1;
		            lau8HwTestTcNoDuty = TC_CURRENT_MAX;
		            lau8HwTestEsvDuty = U8_ESV_DRIVE_DUTY_MAX;
		        }
       			if((TEST_MODE_SUB>=1) && (TEST_MODE_SUB<=3))
       			{
       				lau8HwTestABSLampRequest = 1; /* ABS W/L */
		        }
	        }
	        else if(wstr >= WSTR_30_DEG)
	        {
       			lau16HwTestCnt = lau16HwTestCnt + 1;
	  			
	  			if(lau16HwTestCnt <= L_U8_TIME_10MSLOOP_1000MS)
	  			{
	  				if((TEST_MODE_SUB==1) || (TEST_MODE_SUB==3)) /* primary, all wheel */
       				{
		        		HW_Test_TC_NO_Prim = 1;
			            HW_Test_ESV_Prim = 1;
			            lau8HwTestTcNoDuty = TC_CURRENT_MAX;
			            lau8HwTestEsvDuty = U8_ESV_DRIVE_DUTY_MAX;
			        }
			        if((TEST_MODE_SUB==2) || (TEST_MODE_SUB==3)) /* secondary, all wheel */
       				{
       					HW_Test_TC_NO_Secn = 1;
			            HW_Test_ESV_Secn = 1;
			            lau8HwTestTcNoDuty = TC_CURRENT_MAX;
			            lau8HwTestEsvDuty = U8_ESV_DRIVE_DUTY_MAX;
			        }
	  			}
	  			else if(lau16HwTestCnt <= (L_U8_TIME_10MSLOOP_1000MS+TEST_CASE_50_TC_CURRENT_TIME))
       			{
       				if((TEST_MODE_SUB==1) || (TEST_MODE_SUB==3)) /* primary, all wheel */
       				{
	       				HW_Test_TC_NO_Prim = 1;
	                	HW_Test_ESV_Prim = 1;
	                	lau8HwTestTcNoDuty = TEST_CASE_50_TC_CURRENT_TARGET;
	                	lau8HwTestEsvDuty = U8_ESV_DRIVE_DUTY_MAX;
	                }
	                if((TEST_MODE_SUB==2) || (TEST_MODE_SUB==3)) /* secondary, all wheel */
       				{
       					HW_Test_TC_NO_Secn = 1;
	                	HW_Test_ESV_Secn = 1;
	                	lau8HwTestTcNoDuty = TEST_CASE_50_TC_CURRENT_TARGET;
	                	lau8HwTestEsvDuty = U8_ESV_DRIVE_DUTY_MAX;
	                }
       			}
       			else{}
			}
			else{}

        }
        else if(las8HwTestMode==ESV_VS_PRESSURE_RISE)
        {
           	HW_Test_Motor_On = 0;	        		
           	if(HW_test_cnt_t <= L_U8_TIME_10MSLOOP_1000MS)
       		{	
       			if((TEST_MODE_SUB==1) || (TEST_MODE_SUB==3)) /* primary, all wheel */
       			{
           			HW_Test_TC_NO_Prim = 1;
           			lau8HwTestTcNoDuty = TC_CURRENT_MAX;
           		}
           		if((TEST_MODE_SUB==2) || (TEST_MODE_SUB==3)) /* secondary, all wheel */
       			{
           			HW_Test_TC_NO_Secn = 1;
           			lau8HwTestTcNoDuty = TC_CURRENT_MAX;
           		}
           	}
           	else if(HW_test_cnt_t <= L_U16_TIME_10MSLOOP_4S)
       		{	
       			if((TEST_MODE_SUB==1) || (TEST_MODE_SUB==3)) /* primary, all wheel */
       			{
           			HW_Test_TC_NO_Prim = 1;
           			lau8HwTestTcNoDuty = TC_CURRENT_MAX;
           		}
           		if((TEST_MODE_SUB==2) || (TEST_MODE_SUB==3)) /* secondary, all wheel */
       			{
           			HW_Test_TC_NO_Secn = 1;
           			lau8HwTestTcNoDuty = TC_CURRENT_MAX;
           		}
           		if((TEST_MODE_SUB>=1) && (TEST_MODE_SUB<=3))
       			{
       				lau8HwTestABSLampRequest = 1; /* ABS W/L */
       				HW_Test_Motor_On = 1;
		        }
           	}
           	else if(HW_test_cnt_t <= (L_U16_TIME_10MSLOOP_4S+TEST_CASE_108_ESV_ON_TIME))
       		{
       			
           		if((TEST_MODE_SUB==1) || (TEST_MODE_SUB==3)) /* primary, all wheel */
       			{
           			HW_Test_TC_NO_Prim = 1;
           			HW_Test_ESV_Prim = 1;
           			lau8HwTestTcNoDuty = TC_CURRENT_MAX;
           			lau8HwTestEsvDuty = U8_ESV_DRIVE_DUTY_MAX;
           		}
           		if((TEST_MODE_SUB==2) || (TEST_MODE_SUB==3)) /* secondary, all wheel */
       			{
           			HW_Test_TC_NO_Secn = 1;
           			HW_Test_ESV_Secn = 1;
           			lau8HwTestTcNoDuty = TC_CURRENT_MAX;
           			lau8HwTestEsvDuty = U8_ESV_DRIVE_DUTY_MAX;
           		}
           		if((TEST_MODE_SUB>=1) && (TEST_MODE_SUB<=3))
       			{
       				lau8HwTestABSLampRequest = 1; /* ABS W/L */
       				HW_Test_Motor_On = 1;
		        }
           		
       		}
       		else if(HW_test_cnt_t <= (L_U16_TIME_10MSLOOP_7S+TEST_CASE_108_ESV_ON_TIME))
       		{	
       			if((TEST_MODE_SUB==1) || (TEST_MODE_SUB==3)) /* primary, all wheel */
       			{
           			HW_Test_TC_NO_Prim = 1;
           			lau8HwTestTcNoDuty = TC_CURRENT_MAX;
           		}
           		if((TEST_MODE_SUB==2) || (TEST_MODE_SUB==3)) /* secondary, all wheel */
       			{
           			HW_Test_TC_NO_Secn = 1;
           			lau8HwTestTcNoDuty = TC_CURRENT_MAX;
           		}
           		if((TEST_MODE_SUB>=1) && (TEST_MODE_SUB<=3))
       			{
       				lau8HwTestABSLampRequest = 1; /* ABS W/L */
       				HW_Test_Motor_On = 1;
		        }
           	}
       		else
       		{
       			HW_Test_Motor_On = 0;
       			HW_Test_TC_NO_Prim = 0;
       			HW_Test_TC_NO_Secn = 0;
           		HW_Test_ESV_Prim = 0;
           		HW_Test_ESV_Secn = 0;
           		lau8HwTestTcNoDuty = 0;
           		lau8HwTestEsvDuty = 0;
       		}
        }
        else
        {
            lau8HwTestTcNoDuty = 0;
            lau8HwTestEsvDuty = 0;
        }
        
        if(MFC_TEST_CIRCUIT==PRIMARY_ONLY)
        {
        	HW_Test_TC_NO_Secn = 0;
        	HW_Test_ESV_Secn = 0;
        }
        else if(MFC_TEST_CIRCUIT==SECONDARY_ONLY)
        {
        	HW_Test_TC_NO_Prim = 0;
        	HW_Test_ESV_Prim = 0;
        }
        else
        {
        	;
        }

        VDC_MOTOR_ON           = HW_Test_Motor_On;
        MFC_TCNO_Primary_ACT   = HW_Test_TC_NO_Prim; 
        MFC_TCNO_Secondary_ACT = HW_Test_TC_NO_Secn;
        MFC_ESV_Primary_ACT    = HW_Test_ESV_Prim; 
        MFC_ESV_Secondary_ACT  = HW_Test_ESV_Secn;
        
        if(HW_Test_TC_NO_Prim==1)
        {
            la_PtcMfc.MFC_PWM_DUTY_temp = lau8HwTestTcNoDuty;
        }
        else
        {
        	la_PtcMfc.MFC_PWM_DUTY_temp = 0;
        }
        if(HW_Test_TC_NO_Secn==1)
        {
            la_StcMfc.MFC_PWM_DUTY_temp = lau8HwTestTcNoDuty;
        }
        else
        {
        	la_StcMfc.MFC_PWM_DUTY_temp = 0;
        }
        
        MFC_PWM_DUTY_ESV = lau8HwTestEsvDuty;
        
        if((las8HwTestMode == DITHER_MODE_CURRENT) || (las8HwTestMode == PIRAMID_MODE_CURRENT2))
        {
        	LAMFC_vTestTcNoValveDithering();
        }
        else
        {
        	MFC_PWM_DUTY_P = la_PtcMfc.MFC_PWM_DUTY_temp;
        	MFC_PWM_DUTY_S = la_StcMfc.MFC_PWM_DUTY_temp;
        }
        
        MFC_PWM_DUTY_P  = (MFC_PWM_DUTY_P > TC_FULL_ON_DUTY_8BIT) ? TC_FULL_ON_DUTY_8BIT : MFC_PWM_DUTY_P;
        MFC_PWM_DUTY_S  = (MFC_PWM_DUTY_S > TC_FULL_ON_DUTY_8BIT) ? TC_FULL_ON_DUTY_8BIT : MFC_PWM_DUTY_S;
        MFC_PWM_DUTY_ESV= (MFC_PWM_DUTY_ESV>SV_FULL_ON_DUTY_8BIT) ? SV_FULL_ON_DUTY_8BIT : MFC_PWM_DUTY_ESV;

    }
    else
    {
        valve_test_cnt=0;
        lau8HwTestTcNoDuty = 0;
        lau8HwTestEsvDuty = 0;
        MFC_Duty_Dither_cnt_S = 0;
        MFC_Duty_Dither_cnt_P = 0;
        MFC_TEST_MODE_P_UP_Time = 0;
        MFC_TEST_MODE_P_DOWN_Time = 0;
        MFC_TEST_MODE_P_B_20BAR = 0;
        MFC_TEST_MODE_P_AB_20BAR = 0;
        MFC_TEST_20BAR_CURRENT = 0;

    }

}

static void    LAMFC_vTestTcNoValveDithering(void)
{
  /**********************************************************************************************/
  /* MFC Dithering pattern decision                                                             */
  /**********************************************************************************************/
    uint8_t MFC_dither_duty_temp;
    
    
    if(la_StcMfc.MFC_PWM_DUTY_temp > 0) 
    {   
        if(MFC_Duty_Dither_cnt_S >= 3)
        {
            MFC_Duty_Dither_cnt_S = 0;
        }
        else
        {
            MFC_Duty_Dither_cnt_S = MFC_Duty_Dither_cnt_S + 1;
        }
    }
    else
    {
        MFC_Duty_Dither_cnt_S = 0;
    }
    
    if((MFC_DITHER_DUTY_Temp1>0) && (MFC_DITHER_DUTY_Temp2==0))
    {
        if(la_StcMfc.MFC_PWM_DUTY_temp > MGH60_20BAR_DUTY)
        {
            MFC_PWM_DUTY_S = la_StcMfc.MFC_PWM_DUTY_temp;
        }
        else if(la_StcMfc.MFC_PWM_DUTY_temp > MGH60_0BAR_DUTY)
        {
            MFC_dither_duty_temp = (uint8_t)(MFC_DITHER_DUTY_Temp1 + (((int16_t)(la_StcMfc.MFC_PWM_DUTY_temp-MGH60_0BAR_DUTY)*(MFC_DITHER_DUTY_Temp2-MFC_DITHER_DUTY_Temp1))/(MGH60_20BAR_DUTY-MGH60_0BAR_DUTY)));
    
            if((MFC_Duty_Dither_cnt_S%4)<2) {MFC_PWM_DUTY_S = la_StcMfc.MFC_PWM_DUTY_temp + MFC_dither_duty_temp;}
            else {MFC_PWM_DUTY_S = la_StcMfc.MFC_PWM_DUTY_temp - MFC_dither_duty_temp;}
        }
        else
        {
            if((MFC_Duty_Dither_cnt_S%4)<2) {MFC_PWM_DUTY_S = la_StcMfc.MFC_PWM_DUTY_temp + MFC_DITHER_DUTY_Temp1;}
            else {MFC_PWM_DUTY_S = la_StcMfc.MFC_PWM_DUTY_temp - MFC_DITHER_DUTY_Temp1;}
    
            if(la_StcMfc.MFC_PWM_DUTY_temp==0) {MFC_PWM_DUTY_S = 0;}
        }
    }
    else
    {
        if(la_StcMfc.MFC_PWM_DUTY_temp > MGH60_20BAR_DUTY)
        {
            MFC_PWM_DUTY_S = la_StcMfc.MFC_PWM_DUTY_temp;
        }
        else if(la_StcMfc.MFC_PWM_DUTY_temp > MGH60_0BAR_DUTY)
        {
            MFC_dither_duty_temp = (uint8_t)(MFC_DITHER_DUTY1 + (((int16_t)(la_StcMfc.MFC_PWM_DUTY_temp-MGH60_0BAR_DUTY)*(MFC_DITHER_DUTY2-MFC_DITHER_DUTY1))/(MGH60_20BAR_DUTY-MGH60_0BAR_DUTY)));
    
            if((MFC_Duty_Dither_cnt_S%4)<2) {MFC_PWM_DUTY_S = la_StcMfc.MFC_PWM_DUTY_temp + MFC_dither_duty_temp;}
            else {MFC_PWM_DUTY_S = la_StcMfc.MFC_PWM_DUTY_temp - MFC_dither_duty_temp;}
        }
        else
        {
            if((MFC_Duty_Dither_cnt_S%4)<2) {MFC_PWM_DUTY_S = la_StcMfc.MFC_PWM_DUTY_temp + MFC_DITHER_DUTY1;}
            else {MFC_PWM_DUTY_S = la_StcMfc.MFC_PWM_DUTY_temp - MFC_DITHER_DUTY1;}
    
            if(la_StcMfc.MFC_PWM_DUTY_temp==0) {MFC_PWM_DUTY_S = 0;}
        }
    }
        
      
    la_StcMfc.MFC_PWM_DUTY_temp_old = la_StcMfc.MFC_PWM_DUTY_temp;

    if(la_PtcMfc.MFC_PWM_DUTY_temp > 0) 
    {   
        if(MFC_Duty_Dither_cnt_P >= 3)
        {
            MFC_Duty_Dither_cnt_P = 0;
        }
        else
        {
            MFC_Duty_Dither_cnt_P = MFC_Duty_Dither_cnt_P + 1;
        }
    }
    else
    {
        MFC_Duty_Dither_cnt_P = 0;
    }

    if((MFC_DITHER_DUTY_Temp1>0) && (MFC_DITHER_DUTY_Temp2==0))
    {
        if(la_PtcMfc.MFC_PWM_DUTY_temp > MGH60_20BAR_DUTY)
        {
            MFC_PWM_DUTY_P = la_PtcMfc.MFC_PWM_DUTY_temp;
        }
        else if(la_PtcMfc.MFC_PWM_DUTY_temp > MGH60_0BAR_DUTY)
        {
            MFC_dither_duty_temp = (uint8_t)(MFC_DITHER_DUTY_Temp1 + (((int16_t)(la_PtcMfc.MFC_PWM_DUTY_temp-MGH60_0BAR_DUTY)*(MFC_DITHER_DUTY_Temp2-MFC_DITHER_DUTY_Temp1))/(MGH60_20BAR_DUTY-MGH60_0BAR_DUTY)));
    
            if((MFC_Duty_Dither_cnt_P%4)<2) {MFC_PWM_DUTY_P = la_PtcMfc.MFC_PWM_DUTY_temp + MFC_dither_duty_temp;}
            else {MFC_PWM_DUTY_P = la_PtcMfc.MFC_PWM_DUTY_temp - MFC_dither_duty_temp;}
        }
        else
        {
            if((MFC_Duty_Dither_cnt_P%4)<2) {MFC_PWM_DUTY_P = la_PtcMfc.MFC_PWM_DUTY_temp + MFC_DITHER_DUTY_Temp1;}
            else {MFC_PWM_DUTY_P = la_PtcMfc.MFC_PWM_DUTY_temp - MFC_DITHER_DUTY_Temp1;}
    
            if(la_PtcMfc.MFC_PWM_DUTY_temp==0) {MFC_PWM_DUTY_P = 0;}
        }
    }
    else
    {
        if(la_PtcMfc.MFC_PWM_DUTY_temp > MGH60_20BAR_DUTY)
        {
            MFC_PWM_DUTY_P = la_PtcMfc.MFC_PWM_DUTY_temp;
        }
        else if(la_PtcMfc.MFC_PWM_DUTY_temp > MGH60_0BAR_DUTY)
        {
            MFC_dither_duty_temp = (uint8_t)(MFC_DITHER_DUTY1 + (((int16_t)(la_PtcMfc.MFC_PWM_DUTY_temp-MGH60_0BAR_DUTY)*(MFC_DITHER_DUTY2-MFC_DITHER_DUTY1))/(MGH60_20BAR_DUTY-MGH60_0BAR_DUTY)));
    
            if((MFC_Duty_Dither_cnt_P%4)<2) {MFC_PWM_DUTY_P = la_PtcMfc.MFC_PWM_DUTY_temp + MFC_dither_duty_temp;}
            else {MFC_PWM_DUTY_P = la_PtcMfc.MFC_PWM_DUTY_temp - MFC_dither_duty_temp;}
        }
        else
        {
            if((MFC_Duty_Dither_cnt_P%4)<2) {MFC_PWM_DUTY_P = la_PtcMfc.MFC_PWM_DUTY_temp + MFC_DITHER_DUTY1;}
            else {MFC_PWM_DUTY_P = la_PtcMfc.MFC_PWM_DUTY_temp - MFC_DITHER_DUTY1;}
    
            if(la_PtcMfc.MFC_PWM_DUTY_temp==0) {MFC_PWM_DUTY_P = 0;}
        }
    }
  
    la_PtcMfc.MFC_PWM_DUTY_temp_old = la_PtcMfc.MFC_PWM_DUTY_temp;
    
  /**********************************************************************************************/
  /* MFC Dithering pattern decision                                                             */
  /**********************************************************************************************/
  
}

#endif
#endif

/* AUTOSAR --------------------------*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LAABSCallHWTest
	#include "Mdyn_autosar.h"               
#endif
/* AUTOSAR --------------------------*/
