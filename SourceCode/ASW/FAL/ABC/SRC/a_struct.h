#ifndef __A_STRUCT_H__
#define __A_STRUCT_H__
#include "Abc_Ctrl.h"
#if !defined(__AUTOSAR_CORE_ENA)
	#include "../HDR/logic.par.h"
#else
	#include "../HDR/logic.par.h"
#endif

//#include "LDRTACallDetection.h"
extern struct W_STRUCT FL, FR, RL, RR;

struct  W_STRUCT
{
//******************************************************************************************

	int16_t	lcabss16SpecicalCompDp;
#if	(SIM_MATLAB && __VDC)
    int16_t   ESP_CONTROL_WHEEL;
    int16_t   ESP_full_rise_count;
#endif
    uint8_t check;
/*    int16_t check_dV_Ref; */

    uint8_t state;
    uint8_t zyklus_z;
//  int16_t   diff_slip;    // Local variable로 해도 됨.
//    int16_t   s0_afz;
#if	__CREEP_VIBRATION
    int16_t   v_ungef_alt;
#endif

    int16_t   lsabss16WhlSpdRawSignal64;
    int16_t   vrad_resol_change;
    int16_t   vrad_crt_resol_change;
    int16_t   vdiff_resol_change;
    int16_t	  lsabss16WhlSpdRawSigcmpRta;
    int16_t	  lsabss16WhlSpdRawSigcmpRta_old;
      #if __ARAD_RESOL_CHANGE
        int16_t   arad_alt_resol_change;
        int16_t   arad_resol_change;
      #endif
	int16_t vdiff_rest;
	
    int16_t vrad0;
/*
    int16_t arad10;
    int16_t arad20;
    int16_t arad30;
    int16_t arad40;
    int16_t arad50;
*/
	int16_t arad_temp[5];
    int16_t arad_avg;

	int8_t 	whl_reliability_chk_cnt;
	uint16_t 	whl_reliability_chk_cnt2;
	uint8_t	whl_reliability_chk_flag;
	#if __VDC
	int16_t 	esp_rise_cnt;
	#endif
	int16_t vrad_crt2;

	#if __PARKING_BRAKE_OPERATION
    uint8_t   Wheel_Big_Slip_Counter;
	#endif
    int16_t   vrad;
    int16_t   vrad_old; // variable for rough road detection
    int16_t   vrad_crt;
    int16_t   vdiff;
//    int16_t   vdiff_alt;	// '03 NZ Winter
    int16_t   speed;
//    int16_t   speed_alt;	// '03 NZ Winter
//    int16_t   speed_filtered;
//    int16_t   speed_filtered_alt;
    int16_t   s_diff, MaxPositiveSdiff;
    int8_t  s_diff_old;
/*    int16_t   s_diff_alt; */
/*    int16_t   s_diff_sum; */
//    int8_t  s_diff_kph;   // s_diff로 대체 가능함.
//    int8_t  s_diff_alt_kph;
//    int16_t   s0_afz1;
    int16_t   spold;
  #if __DUMP_THRES_COMP_for_Spold_update
    int16_t   spold_comp;
  #endif
    int16_t   vfilt_rough;
    int16_t   vfiltn;
    uint8_t   vfiltn_raw;
  #if __dV_COMP_OVERSHOOT
    int16_t   vfilt_comp_overshoot;
    uint8_t Dec_after_Overshoot;
  #endif
  #if (__4WD==1)||(__4WD_VARIANT_CODE==ENABLE)
    int16_t vfilt_Accel2ABS_comp;
  #endif

//    int16_t   avr_arad;
//    int16_t   avr_arad_alt;
      int8_t  arad;
    int8_t  arad_alt;
    int8_t  peak_acc;
    int8_t  peak_acc_rough;
    int8_t  peak_dec;
    int8_t  max_arad_instbl;
    int8_t  rel_lam;
    int8_t  peak_slip;
    int8_t  peak_slip_old;
/*    int8_t  peak_slip_reserved; */
    uint8_t reapply_tim;
    uint8_t castim;
    int8_t  thres_decel;
#if (__THRDECEL_RESOLUTION_CHANGE==1)
    int16_t  lcabss16ThresDecel32; 
#endif

#if __VREF_AUX
    int8_t  rel_lam_aux;
    int8_t  rel_lam_aux_alt;
#endif


/*
#if !__PWM
    uint8_t natrl_frq_counter;
#endif
*/ /* 2006.06.15  K.G.Y */

    uint8_t hold_timer_new;
/*    uint8_t continue_dump_timer; */
//    uint8_t timer_new;
#if __AQUA_ENABLE
    uint8_t av_t_aqua;
#endif
    uint8_t ab_z_pr;
    uint8_t ab_zaehl;
    uint8_t slew_rate_timer;  // 이용하는 곳 없음???
    uint8_t flags;
    uint8_t pulse_up_row_timer;
    uint8_t s0_reapply_counter, s0_reapply_counter_old;
/*
#if !__PWM					// '03 NZ Winter
    uint8_t ab_auf_counter;
#endif
*/ /* 2006.06.15  K.G.Y */
/*
    int8_t  time_aft_thrdec;
    uint8_t increase_p_time;
*/
    int8_t  peak_acc_alt;
    int8_t  peak_dec_alt;
    int8_t  peak_dec_max;
    uint8_t holding_cycle_timer;
//    uint8_t roughold;
    uint8_t gma_dump;
   #if __ESP_SPLIT_2
    uint8_t gma_dump2;
   #endif
    uint8_t gma_hold_counter;
    uint8_t gma_counter;
    uint8_t gma_control_counter;
    int8_t  slip_at_real_dump;
/*    uint8_t p_hold_time;
    int8_t  rel_lam_alt;
    uint8_t holding_time; */
/*
#if !__PWM					// '03 NZ Winter
    uint8_t s0_reapply_alt_count;
    uint8_t vib_counter;			//!__PWM ?
    uint8_t depress_time;			//!__PWM ?
#endif
*/ /* 2006.06.15  K.G.Y */
/*    int8_t  arad_at_dump; */
    uint8_t spold_rst_count;
/*    uint8_t vfilt_timer; */
    uint8_t frcnt;
    uint8_t rough_hold_time;
/*    uint8_t force_dump_counter; */
    uint8_t peak_acc_afz;
    int8_t  vfilt_comp;
  #if (__SPARE_WHEEL && !__RTA_ENABLE)
    uint8_t mini_whl_counter;
  #endif
/*
#if !__PWM
    uint8_t p_row_change_timer;
    uint8_t mu_jump_p_counter;
    uint8_t p_shooting_time;
#endif
*/ /* 2006.06.15  K.G.Y */
    uint8_t ebd_dump_time;
//    uint8_t on_time_inlet_alt;
    uint8_t on_time_inlet;
    uint8_t on_time_outlet;
//    uint8_t on_time_special;
    uint8_t det_count;          // Changed by J.K.Lee at 03.06.30
//    uint8_t msl_hold_time;
//    uint8_t msl_gma_counter;  // J.K.Lee at 03.11.14
    uint8_t gma_dump1;
/*    uint8_t msl_reapply_time;
    uint8_t EBD_Dump_timer; */
    uint8_t EBD_Dump_counter;
//    int8_t  ebd_peak_dec;

    int16_t   High_dump_factor;
/*    int16_t   High_dump_factor2; */
    uint8_t   Dump_hold_scan_ref;
    uint8_t   Dump_hold_scan_counter;

#if __SPECIAL_1st_PULSEDOWN
	uint8_t SPECIAL_1st_Pulsedown_need;
	uint8_t SPECIAL_1st_Pulsedown_need_old;
#endif

#if __EDC
    int16_t	  ldedcs16DeltaVelAtMinusB_0G5;    
    int16_t	  ldedcs16DeltaVelAtMinusB_1G5;
    int16_t   ldedcs16DeltaVelocity;
    int16_t	  lcedcs16WheelExitCount;
    int16_t	  lcedcs16WheelSlip;
    uint8_t ldedcu8ParkBrkSuspectCounter;
    uint16_t  ldedcu16EngRpmAtDecelCheck;
    uint8_t ldedcu8ParkDecelCheckCnt;
#endif

#if	__USE_SIDE_VREF
	uint8_t STABLE_cnt_for_SIDE_VREF;
#endif

    uint8_t STABLE_cnt_using_SIDE_VREF1;
    uint8_t STABLE_cnt_using_SIDE_VREF2;
    uint8_t STABLE_cnt_using_SIDE_VREF3;
    int8_t Slip_by_Side_Vref;

   #if __DETECT_LOCK_IN_CYCLE
    int8_t lcabsu8NoChangeInArad;
   #endif
#if __VDC
    int16_t   wvref;

    int16_t   wvref_resol_change;

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//    uint8_t esp_control_mode;                 // 03 SWD
//    uint8_t esp_control_mode_old;
#else
    uint8_t esp_control_mode;                 // 03 SWD
    uint8_t esp_control_mode_old;
#endif    
    
    uint8_t esp_final_mode;
    
#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)    
//    uint8_t esp_control_cycle;
#else
    uint8_t esp_control_cycle;
#endif
    
    uint8_t ESP_to_LFC_hold_timer;
    uint8_t ESP_end_counter;
    uint8_t ESP_end_counter_final;
    uint8_t ESP_partial_dump_counter;        // 04 SW winter

  #if __ESP_ABS_COOPERATION_CONTROL
    int8_t ESP_ABS_Rear_Forced_Dump_timer;
    int8_t ESP_ABS_Front_Forced_Dump_timer;
  #endif

  	uint8_t	lcabsu8AbsEscCoopLevel;

    int16_t   M_vrad;                          // 03 WINTER, SENSOR MODEL


    uint8_t ESP_rise_hold_timer;
    uint8_t ESP_on_timer;
    int16_t ESP_off_timer;
    int16_t ESP_off_timer2;
    uint8_t brake_fail_cnt1, brake_fail_cnt2, brake_fail_cnt3;
    uint8_t next_hold_scan;
    int16_t abs_permit_count0;
    int16_t Circuit_fail_chk_enable;
/* #if __SPLIT_STABILITY_IMPROVE_GMA */
	int16_t lcabss16YawDumpFactor, lcabss16YawDumpFactor2;
	uint8_t lcabsu8YawDumpHoldTime, lcabsu8YawRiseHoldTime;
	uint8_t lcabsu8YawRiseHoldTime2;
/* #endif */
#endif

#if (__VDC || __BTC)
//  uint8_t press_test_reapply;
//  uint8_t press_test_hold;
#endif
  #if __EST_MU_FROM_SKID
    uint8_t lcabsu8PossibleMuFromSkid;
    uint8_t lcabsu81stCycSkid;
  #endif

  #if __VDC && __PRESS_EST
    int16_t ABSCyle_Estimated_Press_Avg, ABSCyle_Estimated_Press_Sum, ABSCyle_cnt;
  #endif

  #if __PRESS_EST
    uint8_t lcabsu8EffectiveDumpScans;
  #endif
  
  #if __PRES_RISE_DEFER_FOR_UR
    int8_t lcabss8LimitURPresRiseCnt;
  #endif
  
  #if __OUT_ABS_P_RATE_COMPENSATION
    uchar8_t OutABSYMRCtrlMode;
  #endif
  
  #if __EBD_MI_ENABLE
    uchar8_t EBDMICtrlMode;
  #endif

//***************************************************************************************
/* #if __PWM       // MGH-25, MGH-40 공용  */ /* 2006.06.15  K.G.Y */
//***************************************************************************************
// PWM parameters
    uint8_t on_time_inlet_pwm;        // PWM Pattern Allocation을 위하여
                        // STD Logic의 on_time_inlet을 참조하는 변수.
 //   uint8_t on_time_inlet_pwm_old;    // PWM Pattern Allocation을 위하여
                        // STD Logic의 on_time_inlet을 참조하는 변수.
    uint8_t on_time_outlet_pwm;       // PWM Pattern Allocation을 위하여
                        // STD Logic의 on_time_outlet을 참조하는 변수.
//    uint8_t on_time_special_pwm;      // PWM Pattern Allocation을 위하여
                        // STD Logic의 on_time_special을 참조하는 변수.

    uint8_t slip_recovery_counter;
    int16_t  LFC_built_reapply_counter;
    int16_t  LFC_built_reapply_counter_old;
    int16_t  LFC_built_reapply_counter_delta;
    uint8_t LFC_s0_reapply_counter;
    uint8_t LFC_s0_reapply_counter_old;
    uint8_t LFC_s0_reapply_counter_old2;
    uint8_t LFC_STABIL_counter, LFC_STABIL_counter2, LFC_STABIL_counter_old;
//    uint8_t LFC_dump_timer;
//    uint8_t LFC_dump_end_timer;
    uint8_t LFC_dump_counter;         // Changed by J.K.Lee at 03.06.30
    uint8_t LFC_unstable_hold_counter;
    uint8_t LFC_dump_counter_old;     // Changed by J.K.Lee at 03.06.30

    int16_t   LFC_special_comp_duty;   // Special Road Situation 고려 Duty Compensation
    int16_t   LFC_pres_rise_delay_comp;
/*    int16_t   LFC_pres_rise_delay_comp_old;   */
    int16_t   LFC_initial_set_duty;
    int8_t  LFC_Conven_Reapply_Time;  // Reapply over 7ms (by CSH 2002 Winter)
    int8_t 	LFC_Conven_Reapply_Time_Old;// Reapply time old value storage

	int8_t  Forced_Hold_cnt;

    uint16_t  MSC_dump_counter;    // dump 동안의 scan counter
    uint16_t  MSC_no_dump_counter; // dump가 아닌 동안의 counter

#if __PREVENT_SUTTLE_NOISE
#if	__BTC && !__ESV
    uint8_t MSC_continue_no_dump_counter;
#endif
#endif

  /*(M_DP_BASE_BUILT_RISE == ENABLE)*/
    int16_t lcabss16TarDeltaWhlP    ;
    int16_t lcabss16TarWhlP;
    int16_t lcebds16TarWhlP;
    int16_t lcabss16EstWhlPAftDump  ;
    int16_t lcabss16RiseDeltaWhlPOld;
    int16_t lcabss16RiseDeltaWhlP;
    int16_t lcabss16DumpDeltaWhlP;
    uint8_t WheelCtrlMode;
  /*(M_DP_BASE_BUILT_RISE == ENABLE)*/

    int8_t  lcabss8WhlCtrlReqForStrkRec;
    uint8_t lcabsu8ForcedHldForStrkRecvrCnt;
    int8_t  lcabss8PresModeForStrkRecvr;

    uint8_t lcebdu8CtrlMode;


//------ Detection of Rough Road ------------------------------------

    uint8_t  Rough_road_counter;
    uint8_t  Rough_road_counter2;
    uint8_t  Rough_road_counter3;
    uint8_t  Rough_road_counter4;
    uint8_t  Rough_road_counter5;
    uint8_t  Rough_road_counter_arad;
    uint8_t  Rough_road_counter_sum[7];
    uint8_t  Rough_road_counter_sum2[7];
    uint8_t  Rough_road_counter_sum3[7];
    uint8_t  Rough_road_counter_sum4[7];
    uint8_t  Rough_road_counter_sum5[7];
    uint8_t  Rough_road_counter_arad_sum[7];
    uint8_t  Rough_road_detector;
    uint8_t  Rough_road_detector2;
    uint8_t  Rough_road_detector3;
    uint8_t  Rough_road_detector4;
    uint8_t  Rough_road_detector5;
    uint8_t  Rough_road_detector_arad;
//  uint8_t  scan_counter;
    int16_t    diff_max;
    int16_t    vrad_max_old;
    int8_t   sign_old;
	uint8_t  Rough_disable_cycle_counter;

	int16_t    diff_max2;
    uint8_t  diff_max2_counter;

//-------------------------------------------------------------------

#if __PREV_RISE_DUMP_IMPROVE		
	uint8_t  LFC_big_rise_and_dump_cnt;
#endif	

#if __INGEAR_VIB
//------ Detection of In-gear Vibration -----------------------------

//	uint8_t  Cycle_freq_counter;
	uint8_t  In_gear_vibration_counter;

	uint8_t  Acc_to_Dec_cnt;
	uint8_t  Dec_hump_counter;

//-------------------------------------------------------------------
#endif

#if __RESONANCE
//------ Detection of REZZO Resonance -------------------------------
	uint8_t	Cycle_frq_cnt;
	uint8_t	Resonance_counter;
//-------------------------------------------------------------------
#endif

  #if (L_CONTROL_PERIOD>=2)
    uint8_t lcabss8DumpCase;
    uint8_t lcabss8DumpCaseOld;
  #endif

    int16_t pwm_temp;           // Temporary PWM Variable

    int16_t init_duty;
//    int16_t pwm_temp_old;         // 1228. msl control for lsv
//    int16_t duty_ratio_dec;
//    int16_t duty_ratio_inc;
    uint8_t LFC_reapply_end_duty;
//    uint8_t LFC_reapply_end_duty_old;
    uint8_t LFC_reapply_current_duty;

//  int16_t LFC_dump_corr_flag;
//  int16_t LFC_duty_corr_front;
//  int16_t LFC_duty_corr_rear;

    uint8_t hold_timer_new_ref;
    uint8_t hold_timer_new_ref2;
    uint8_t hold_timer_new_ref_old;
//  uint8_t LFC_debug_location;   // For Logic Development!!!

    uint8_t pwm_duty_temp;            // Temporary PWM Pattern
#if	__TCMF_CONTROL2
    uint8_t pwm_duty_temp_old;            // Temporary PWM Pattern
#endif
    uint8_t pwm_duty_temp_dash;       // Temporary PWM Pattern

#if __NC_CURRENT_CONTROL
    uint8_t lau8NCPwmDuty; /* To be moved to Solenoid Drive Command Structure */
    uint8_t lau8NCOntime;  /* To be moved to Solenoid Drive Command Structure */
#endif

#if __VDC
//    uint8_t pwm_duty[7];          // Real PWM Pattern
	uint8_t pulseup_cycle_count;
	uint8_t pulseup_cycle_scan_time;
	uint8_t	pulseup_hold_cycle_count;

#endif
#if __DUTY_CHECK
    uint8_t logic_duty[7];
    uint8_t system_duty[7];
#endif

    //uint8_t Reapply_Accel_hold_timer;     // Added by csh 01.12.28

/*    uint8_t Holding_LFC_duty;       // 1228. start. msl control for lsv     //for compile */
    uint8_t timer_keeping_LFC_duty;                 //for compile
//  uint8_t timer_keeping_LFC_duty_old;

    uint8_t LFC_zyklus_z;


    uint16_t LFC_NO_Hmon;           //MGH25_ECU Current Monitor
    uint16_t LFC_NO_Lmon;

//      uint16_t  LFC_dump_to_counter;
//      int8_t duty_ratio_dec_more;              // added by y.w.shin at 2002.winter
#if	__CHANGE_MU
    int16_t   LFC_Dump_Rise_factor;
    uint8_t Mu_change_Dump_counter;
    uint8_t Mu_change_Dump_counter_old;
#endif

/*    uint8_t   LFC_Rep_duty; */

	uint16_t	pos_arad_sum;
	uint8_t	pos_arad_num;
	int16_t		arad_gauge;
	uint16_t	accel_arad_avg;

#if	__TEMP_for_CAS2
	int16_t		arad_gauge_cas2;
	int16_t		arad_gauge_cas3;
	int8_t	arad_gauge_cas2_cnt;
	int8_t	arad_gauge_cas3_cnt;
	int8_t	CAS_SUSPECT_reset_timer;
#endif

/*	int8_t	slip_at_vspold; */
//	int16_t		spold2;

#if __VDC
    int16_t   LFC_special_comp_duty_backup;         // Special Road Situation 고려 Duty Compensation
    int16_t   LFC_pres_rise_delay_comp_backup;
    int16_t    init_duty_backup;                    // not used!
    int16_t    LFC_initial_set_duty_backup;
    #if (__MP_COMP && __PRESS_EST)
    int16_t    LFC_MP_Cyclic_comp_duty_backup;
    #endif
    uint8_t  zyklus_z_backup;
    uint8_t  LFC_zyklus_z_backup;
#endif

	uint8_t	neg_arad_counter;
	uint8_t	Forced_Hold_counter;

#if	__MGH40_EBD
/*    int8_t slip_thres;
    int8_t arad_thres;
    int8_t front_rear_speed_percent;
    int8_t slip_using_side_vref; */
    uint8_t   EBD_MODE;
    uint8_t s_diff_pos_timer;
/*    int16_t EBD_Cut_In_Factor;
    int16_t EBD_Dump_factor_ref; */
    uint8_t EBD_prexit_slip_counter;
   #if __VDC
    uint8_t EBD_MP_Release_counter;
   #endif
#endif

    uint8_t Wheel_Valve_Mode;
    uint8_t     pwm_full_rise_cnt;
    uint8_t     pwm_lfc_rise_cnt;
    uint8_t     pwm_hold_cnt;
    uint8_t     pwm_dump_cnt;
    uint8_t     pwm_circulation_cnt;

    uint8_t     pwm_full_rise_cnt2;
    uint8_t     pwm_lfc_rise_cnt2;
    uint8_t     pwm_hold_cnt2;
    uint8_t     pwm_dump_cnt2;
    uint8_t     pwm_circulation_cnt2;
    
#if __PRESS_EST_ABS
    int16_t   Estimated_Press_H;
   #if __VDC
    int16_t   Estimated_Press_M;
    int16_t   Estimated_Press_L;
   #endif
    uint8_t   P_DumpCnt, P_DumpCnt_old;
	uint16_t  APratio, APratio_sum, APratio_num, APratio_avg;
	int16_t	  DeltaP, IndexMu;
#endif

#if __VDC && __PRESS_EST_ACTIVE
//	int16_t Estimated_Press_old,Estimated_Press;
    int16_t   Estimated_Force;

    int16_t   Estimated_Active_Press;
    int16_t   Estimated_Active_Press_old;
    int16_t   Estimated_Press_mon;
    int16_t   s16_Estimated_Active_Press;
    uint8_t u8_Estimated_Active_Press;
    int16_t   Circuit_Valve_Mode;
    int16_t   Circuit_Valve_Rise_Gain;
    int16_t   Circuit_Valve_Rise_mon;

    int16_t   NO_Rise_Press_Motor;
    int16_t   NO_Rise_Press_Driver;

    int16_t   NO_Rise_Press;
    int16_t   NC_Dump_Press;
    int16_t   MOTOR_Rise_Press;

    int16_t   Rise_Mode_cnt;
    int16_t   LFC_Rise_Mode_cnt;
    int16_t   Hold_Mode_cnt;
    int16_t   Dump_Mode_cnt, Dump_Mode_cnt_old;
    int16_t   Circ_Mode_cnt;

		int16_t 	ldabss16EscOnTimerWPE;
		
    /* 2012_SWD_KJS */    
    int16_t   ldabss16EstWheelPressMON_VM;
    int16_t   ldabss16EstWheelPress_VM;
    int16_t   ldabss16EstWheelVolume_VM;        
    int16_t   ldabss16EstWheelPressOld_VM;
    int16_t   ldabss16EstWheelVolumeOld_VM;
    /* 2012_SWD_KJS */

    /* 2012_On_SWD_KJS */    
    int16_t 	ldabss16BrakeOnTimer_VM;		
    int16_t 	ldabss16BrakeOnTimerOld_VM;
    /* 2012_On_SWD_KJS */
    		
#endif
	int16_t lcabss16FadeOutCnt;
	int16_t	MP_Cyclic_comp_duty;
	int16_t MP_Init_Ref;
	int16_t LFC_special_comp_duty_sync; /*Duty Sync*/

#if __VDC && __MP_COMP
    int16_t   MP_Init;
    int16_t   LFC_MP_Cyclic_comp_duty;
    uint8_t LFC_reapply_current_duty1;
    uint8_t LFC_reapply_current_duty2;

	int16_t   Estimated_Press_SKID;
  #if __UNSTABLE_REAPPLY_ENABLE
	int16_t   Estimated_Press_Lower;
	int8_t Unsable_Rise_time;
  #endif
#endif

#if	__VDC && __WP_DROP_COMP
	int16_t   Estimated_Press_init;
	int16_t   Estimated_Press_end;
	uint8_t	Drop_Press_Comp_T;
	int16_t   LFC_WP_Drop_comp_duty;
#endif

#if	__SPLIT_VERIFY
	uint8_t	LFC_UNSTABIL_counter;
	uint8_t   LFC_UNSTABIL_counter_old;
  #if __VDC
	uint8_t	dP_EST;
	int8_t	arad_ref_at_dump;
	int16_t		Sum_of_delta_arad;
	int16_t		Sum_of_dP_EST;
	int16_t		Wheel_Inertia_coeff;
#endif
#endif

 #if __UNSTABLE_REAPPLY_ENABLE
    int8_t Unst_Reapply_n_hold_timer;
    int8_t Reapply_Accel_hold_timer;
    int8_t Unstable_reapply_counter, lcabss8URScansBfUnstDump;
    int8_t Unst_Reapply_Scan_Ref, Unst_Reapply_Scan_Ref_old;  /* 로직 확인 후 local로 전환*/
/*    int16_t temp_var;  */ /* 로직 확인 후 local로 전환*/

  #if __UNSTABLE_REAPPLY_ADAPTATION
	uint8_t Unstable_Rise_Pres_Cycle_time;
	uint8_t Unstable_Rise_Pres_Cycle_cnt;
  #endif

  #if __VDC && __MP_COMP
    int8_t Unstable_Rise_MP_comp_duty;
  #endif
 #endif
    int16_t S_DIFF_OFFSET; /* 로직 확인 후 local로 전환*/
  #if __STABLE_DUMPHOLD_ENABLE
    uint8_t lcabsu8HoldAfterStabDumpCnt;
    uint8_t lcabsu8StableDumpCnt;
  #endif

  #if __HRB
    uint8_t    lchrbu8PulseUpCounter;
  #endif
  
  #if (__VDC && __ABS_CORNER_BRAKING_IMPROVE)
    uchar8_t    lcabsu8RearOutsideHoldCnt;
  #endif

#if __WHEEL_PRESSURE_SENSOR
    int8_t lcabss8WLPressRiseCnt;
    uint8_t MuFromWLPressOld;
    uint8_t MuFromWLPress;
    uint8_t DesiredDPRise;
    uint8_t WheelPressAtDump;
#endif


/* #endif */ /* 2006.06.15  K.G.Y */
//******************************** 여기 까지 D256, D60 공용 PWM *********************

//ADDED BY HJH--------------------------------//
#if !__BTC && __VDC
    int16_t   TCS_built_reapply_pattern;
    int16_t   TCS_Initial_Duty;
    int16_t   TCS_Duty_Ratio_decrease;
    uint16_t  TCS_built_reapply_time;
#endif
//--------------------------------------------//

//***********************************************************************************
#if __BTC         // MGH-25,MGH-40 공용 BTCS 변수
//**********************************************************************************
    int16_t   b_rad;
    int16_t   vrad_alt;

/* NOT USED AFTER BTCS RESTRUCTURING 120207
    int16_t   average7;
    int16_t   btc_slip;
    int16_t   slip_min;
    int16_t   btc_speed;
    int16_t   btc_speed_alt;
    int16_t   ffspeed;
    int16_t   fspeed;
    int16_t   fspeed_alt;
    int16_t   av_fspeed_alt;
    int16_t   av_fspeed;
    int16_t   av_btc_slip;
    int16_t   av_btc_slip_alt;
 NOT USED AFTER BTCS RESTRUCTURING 120207 */

    uint16_t  hold_count;
    uint16_t  hold_count_both_spin;
    uint8_t btcinc_count;
    uint8_t btcdec_count;
    uint8_t btc_count1;
    uint8_t btc_count2;
    uint8_t vib_count1;
    uint8_t vib_count2;
    uint8_t vib_count3;
    uint8_t vib_count4;
    uint8_t slip_count1;
    uint8_t slip_count2;
    uint8_t slip_count3;
    uint8_t exit_timer1;
    uint8_t exit_timer2;
    uint8_t exit_timer3;
    uint8_t exit_timer4;
    uint8_t exit_timer5;
    uint8_t hold_timer;
/* NOT USED AFTER BTCS RESTRUCTURING 120207 
    uint8_t count14;
    int8_t  refdiff;
    int8_t  refdiff_alt1;
    int8_t  refdiff_alt2;
    int8_t  av_ref;
    int8_t  av_ref_alt;
 NOT USED AFTER BTCS RESTRUCTURING 120207 */
 
    int8_t  av_ref_max;

    uint8_t btc_count3;
    uint8_t btc_count4;
    uint8_t slip_count4;
    uint8_t valve_delay_count;

/* NOT USED AFTER BTCS RESTRUCTURING 120207
    uint8_t count7;
    uint8_t count20;
    int8_t  barad_max;
    int8_t  barad_alt;
 NOT USED AFTER BTCS RESTRUCTURING 120207 */

//  LFC Variables
    int8_t   TCS_built_reapply_pattern;
    int16_t   TCS_Initial_Duty;
    uint16_t  TCS_built_reapply_time;

/* tmp */
	#if  __BTEM == DISABLE
    int16_t   btc_tmp;
    int16_t   tmprad_alt;
    int16_t   btc_tmp_alt;
    int16_t   tmprad_max;
    int16_t   cool_tmp;
    int16_t   tmp_acc;
    int16_t   acc_perc;
    int16_t   out_time;
    int16_t   tmp_count;
    int16_t   max_spin;
    int16_t   cal_tmp;
    uint8_t tmp_time;
    uint8_t tmp_down;
    uint8_t tmp_up;
    uint16_t inc_count1;
    uint16_t inc_count2;
	#endif
    uint16_t inc_count3;
    uint8_t tmp_state;
//  Flags
    uint8_t BTCS_flags;

// 051004 New BTCS Structure
	int16_t	spin, ltcss16BrkControlIndex;
	int16_t lctcss16SpinRaw;
	int16_t target_spin, rise_threshold, dump_threshold;
    uint16_t btcs_rise_scan, btcs_dump_scan, btcs_hold_scan, btcs_pressure_inc_scan_cnt, btcs_pressure_dump_scan_cnt;
    uint8_t btcs_rise_scan_disp, btcs_dump_scan_disp, btcs_hold_scan_disp;
    uint8_t ltcsu8WhlSpdDecCnt;
    uint16_t state1_running_cnt, state2_running_cnt;
    int16_t ltcss16LowSpinCnt;
    int8_t ltcss8Cnt4PlusBDetectClear;
    int8_t ltcss8BTCSWheelCtlStatus;
    int8_t ltcss8BTCSWheelCtlStatusOld;
    int8_t ltcss8BTCSCtlModeChange;
    int16_t ltcss16SumOfMovingWindow;
    int16_t ltcss16MovingAveragedWhlSpd;
    int16_t ltcss16MovingAveragedWhlSpdOld;
    int16_t ltcss16MovingAveragedWhlSpdDiff;
    int16_t ltcss16Index4MovingAvrg;
    uint8_t ltcsu8ESVCloseDelayCnt;
    uint8_t ltcsu8BTCSCtlModeFixCnt;
    uint8_t ltcsu8AllowableRiseScan;

    int8_t  lctcss8InsideWheelCnt;    
	uint8_t lctcsu8InsideWheelBrkState;
	uint8_t lctcsu8InsideWheelBrkStateod;
	uint8_t lctcsu8InsideWheelBrkExitCnt;		
	uint8_t lctcsu8InsideWheelESCnt;	
	int16_t ltcss16InsideWheelLowSpinCnt;
  #if __TCS_RISE_TH_ADJUST
    int8_t ltcss8SpinSlopeCnt;
	uint8_t btcs_pressure_hold_scan_cnt;
  #endif	/* #if __TCS_RISE_TH_ADJUST */    
#endif

#if	__TCS
	uint16_t  mini_count;
#endif

/* #if __TDWS */ /* 2006.06.27  K.G.Y */
/*
    int16_t   wref;
    int16_t   diff_sum;
    int16_t   diff_old_sum;
    int16_t   sum;
    int16_t   sum_old;

    int8_t  diff;
    int8_t  aver;
    int8_t  aver_old;
    int8_t  aver_10sec;
    int8_t  aver_10sec_old;

    int8_t  trend_count;
    int8_t  safety_count;

    int16_t   wref1;
    int16_t   diff1_sum;
    int16_t   diff1_old_sum;
    int16_t   sum1;
    int16_t   sum1_old;

    int8_t  diff1;
    int8_t  aver1;
    int8_t  aver1_old;
    int8_t  aver1_10sec;
    int8_t  aver1_10sec_old;
*/
/* #endif  */ /* 2006.06.27  K.G.Y */
                           // WFLAG0
    unsigned AV_VL:               1;   /* bit 7  */
    unsigned HV_VL:               1;   /* bit 6  */
    unsigned BUILT:               1;   /* bit 4  */
    unsigned ABS:                 1;   /* bit 4  */
    unsigned FSF:                 1;   /* bit 3  */
    unsigned STABIL:              1;   /* bit 2  */
    unsigned STAB_K:              1;   /* bit 1  */
    unsigned STAB_A:              1;   /* bit 0  */
                           // WFLAG1
    unsigned AV_HOLD:             1;   /* bit 7  */
    unsigned AV_DUMP:             1;   /* bit 6  */
    unsigned AV_REAPPLY:          1;   /* bit 5  */
    unsigned HEAVY_FILT:          1;   /* bit 4  */
    unsigned INCREASE_VDIFF:      1;   /* bit 3  */
    unsigned V_STOER:             1;   /* bit 2  */
    unsigned REAR_WHEEL:          1;   /* bit 1  */
    unsigned LEFT_WHEEL:          1;   /* bit 0  */
                           // WFLAG2
    unsigned SPEEDMIN:            1;   /* bit 7  */
    unsigned ALG_INHIBIT:         1;   /* bit 6  */
    unsigned RESET_PEAKCEL:       1;   /* bit 5  */
    unsigned CAS:                 1;   /* bit 4  */
    unsigned INIT_AFZ:             1;   /* bit 3  */
    unsigned SL_DUMP:             1;   /* bit 2  */
    unsigned MINUS_B_SET_AGAIN_OLD:  1;   /* bit 1  */    // J.K.Lee at 030926
    unsigned LFC_ABS_Forced_Enter_flag:  1;   /* bit 0  */
                           // WFLAG3
    unsigned MINI_SPARE_WHEEL:          1;   /* bit 7  */
    unsigned LFC_fictitious_cycle_flag: 1;   /* bit 6  */
    unsigned LFC_fictitious_cycle_flag2:1;   /* bit 5  */
    unsigned GMA:                 1;   /* bit 4  */
    unsigned GMA_SLIP:            1;   /* bit 3  */
    unsigned UN_GMA:              1;   /* bit 2  */
    unsigned LFC_Cyclic_Comp_flag:      1;   /* bit 1  */
    unsigned SL_Dump_Start:        1;   /* bit 0  */
                           // WFLAG4
    unsigned SLIP_2P_NEG:         1;   /* bit 7  */
    unsigned SLIP_5P_NEG:         1;   /* bit 6  */
    unsigned SLIP_10P_NEG:        1;   /* bit 5  */
    unsigned SLIP_20P_NEG:        1;   /* bit 4  */
    unsigned SLIP_30P_NEG:        1;   /* bit 3  */
    unsigned SCHNELLSTE_VREF:     1;   /* bit 2  */
    unsigned CAS_SUSPECT_flag:    1;   /* bit 1  */
    unsigned JUMP_DOWN:           1;   /* bit 0  */
                           // WFLAG5
    unsigned PEAK_ACC_PASS:       1;   /* bit 7  */
    unsigned CHECK_PEAK_ACCEL:    1;   /* bit 6  */
    unsigned DRIVEN_WHEEL:        1;   /* bit 5 FORCE_DUMP */
    unsigned ENT_ABS:  			  1;   /* bit 4  */
    unsigned SPOLD_RESET:         1;   /* bit 3  */
    unsigned EBD:      			  1;   /* bit 2  */
    unsigned FC_DUMP:             1;   /* bit 1  */
    unsigned MSL_BASE:            1;   /* bit 0  */
                           // WFLAG6
    unsigned ARAD_AT_MINUS_B:     1;   /* bit 7  */
    unsigned MINUS_B_SET_AGAIN:   1;   /* bit 6  */
    unsigned LAM_1P_MINUS:        1;   /* bit 5  */
    unsigned LAM_2P_MINUS:        1;   /* bit 4  */
    unsigned LAM_3P_MINUS:        1;   /* bit 3  */
    unsigned LAM_4P_MINUS:        1;   /* bit 2  */
    unsigned LAM_5P_MINUS:        1;   /* bit 1  */
    unsigned SET_DUMP:            1;   /* bit 0  */
                           // WFLAG7
    unsigned RES_FRQ:             1;   /* bit 7  */
    unsigned HOLD_RES:            1;   /* bit 6  */
    unsigned ARAD_3G_POS:         1;   /* bit 5  */
    unsigned ARAD_NEG:            1;   /* bit 4  */
    unsigned ARAD_POS:            1;   /* bit 3  */
    unsigned HIGH_VFILT:          1;   /* bit 2  */
    unsigned MSL_DUMP:            1;   /* bit 1  */			// Check 要!!
    unsigned THREE_SEC_FLG:             1;   /* bit 0  */
                           // WFLAG10
    unsigned BOTH_GMA_SET:        1;   /* bit 7  */
    unsigned SPLIT_JUMP:          1;   /* bit 6  */
    unsigned SPLIT_PULSE:         1;   /* bit 5  */
    unsigned PULSE_DOWN_Forbid_flag:           	1;   /* bit 4  */
    unsigned Front_SL_dump_flag_K:     1;   /* bit 3  */	// LFC variable
    unsigned SET_DUMP2:           1;   /* bit 2  */ //mi03c, bump
    unsigned Pres_Rise_Defer_flag:          1;   /* bit 1  */
    unsigned ABS_LIMIT_SPEED:            1;   /* bit 0  */
                           // WFLAG11
    unsigned Rough_road_detect_flag0:           1;   /* bit 7 */
    unsigned SPIN_WHEEL:         				1;   /* bit 6  */
    unsigned High_TO_Low_Suspect_flag: 			1;   /* bit 5  */
    unsigned WP_Drop_comp_flag:   				1;   /* bit 4  */
    unsigned LFC_Bump_Suspect_flag:            	1;   /* bit 3  */
    unsigned Holding_LFC_duty:            		1;   /* bit 2  */
    unsigned UNSTABLE_LowMu_comp_flag:          1;   /* bit 1  */
    unsigned LOW_to_HIGH_suspect2:          	1;   /* bit 0  */

    unsigned RELIABLE_FOR_VREF:   				1;   /* bit 7  */
    unsigned RELIABLE_FOR_VREF2:        				1;   /* bit 6  */
    unsigned WHL_SELECT_FLG:             			1;   /* bit 5  */

    unsigned WhlErrFlg:          		        1;   /* bit 4  */
    unsigned ESC_BRAKE_SLIP_OK:                 1;   /* bit 3  */
    unsigned ESC_CNT_SPIN_OK:           		1;   /* bit 2  */
    unsigned SPECIAL_SPIN_OK:                   1;   /* bit 1  */
    unsigned DRIVING_SPIN_OK:           		1;   /* bit 0  */

    unsigned UNRELIABLE_AT_BRAKE_ENTRY:        		1;   /* bit 7  */
    unsigned UNRELIABLE_AT_ABS_ENTRY:         	1;   /* bit 6  */

  	  	#if __PARKING_BRAKE_OPERATION
    unsigned PARKING_BRAKE_WHEEL:               1;   /* bit 5  */
    unsigned PARKING_DECEL_CHECK:      			1;   /* bit 4  */
    	#endif

  #if __UNSTABLE_REAPPLY_ENABLE
    unsigned Forced_Hold_After_Unst_Reapply:    1;   /* bit 3  */
    unsigned Reapply_Accel_cycle_old:          	1;   /* bit 2  */
    unsigned Reapply_Accel_cycle:          		1;   /* bit 1  */
    unsigned Reapply_Accel_1st_scan:          	1;   /* bit 0  */
  #endif


//  WPE for IDB

	unsigned u1ContinuedFullRiseModeFlg:		1;
	unsigned u1ContinuedLfcRiseModeFlg:			1;
    unsigned u1ContinuedHoldModeFlg:			1;
    unsigned u1ContinuedDumpModeFlg:			1;
    unsigned u1ContinuedCirculationModeFlg:		1;   

  /*(M_DP_BASE_BUILT_RISE == ENABLE)*/
    unsigned lcabsu1WhlPForcedHoldCmd:          1;
    unsigned lcabsu1UpdDeltaPRiseAftHld:        1;
    unsigned lcabsu1OutsideABSCtrlDecided:      1;
    unsigned lcabsu1InABSNthReduceHoldTimReq:   1;
    unsigned lcabsu1InABSInitialRiseDone:		1;

  /*(M_DP_BASE_BUILT_RISE == ENABLE)*/

    unsigned lcabsu1ForcedHldForStrkRecvr:      1;
    unsigned lcabsu1StrkRecvrCtrlInABS:         1;
    unsigned lcabsu1CaptureEstPressForSkid:		1;
    unsigned lcabsu1CaptureEstPressAftDump:		1;
    

#if __BTC
                           // WFLAG13
    unsigned POS_HOLD:            1;   /* bit 7  */
    unsigned COUNT20:             1;   /* bit 6  */
    unsigned POS_COUNT:           1;   /* bit 5  */
    unsigned BTCS:                1;   /* bit 4  */
    unsigned DETECT_BTC:          1;   /* bit 3  */
    unsigned TCL_DEMAND:          1;   /* bit 2  */
    unsigned OPT_NEWSET:          1;   /* bit 1  */
    unsigned FIRST_INC:           1;   /* bit 0  */
                           // WFLAG14
    unsigned ARAD_RISE:           1;   /* bit 7  */
    unsigned EXIT_BTCS_CONTROL:           1;   /* bit 6  */
    unsigned TCL_INC:             1;   /* bit 5  */
    unsigned TCL_INCF:            1;   /* bit 4  */
    unsigned LOW_TO_HIGH1:        1;   /* bit 3  */
    unsigned TCL_INCF2:           1;   /* bit 2  */
    unsigned ARAD_POSITIVE:       1;   /* bit 1  */
    unsigned VRAD_DEC:            1;   /* bit 0  */
                           // WFLAG15
    unsigned lctcsu1HigherRawSpdWhl:           1;   /* bit 7  HCONT_DEC*/
    unsigned ARAD_LOW:            1;   /* bit 6  */
    unsigned OPT_SET_HIGH:        1;   /* bit 5  */
    unsigned HALF_PULS:           1;   /* bit 4  */
    unsigned TMP_ERROR:           1;   /* bit 3  */
    unsigned HILL:                1;   /* bit 2  */
    unsigned TMP_START:           1;   /* bit 1  */
    unsigned TMP_RISE:            1;   /* bit 0  */
                           // WFLAG16
    unsigned XMT_VIB:             1;   /* bit 7  */
    unsigned BTC_LOW_SLIP:        1;   /* bit 6  */
    unsigned VIB_SET:             1;   /* bit 5  */
    unsigned DETVIB_BTC:          1;   /* bit 4  */
    unsigned lctcsu1HigherAvgSpdWhl:              1;   /* bit 3  CIRCLE*/
    unsigned VIB_COUNT:           1;   /* bit 2  */
    unsigned VCA_CTL:             1;   /* bit 1 TWO_BUILT_HI */
    unsigned TCS_VALVE_EXIT:      1;   /* bit 0  */

	// 051004 New BTCS Structure
	unsigned BTC_LOW_SLIP_EXIT:		1; 		/* bit 0 BTCS_CTL_UPDATE */
	unsigned LOW_TO_HIGH:		1; 		/* bit 1 */
	unsigned lctcsu1State2To1:		1; 		/* bit 2 */
#if __TCS_RISE_TH_ADJUST
	unsigned lctcsu1SpinSlope:		1; 		/* bit 3 */
#else	/* #if __TCS_RISE_TH_ADJUST */
	unsigned not_used_flag_NB3:		1; 		/* bit 3 */
#endif	/* #if __TCS_RISE_TH_ADJUST */	
	unsigned lctcsu1InsideWheelEngFlag:		1; 		/* bit 4 */
	unsigned lctcsu1InsideWheelBrkCheck:	1; 		/* bit 5 */
	unsigned lctcsu1InsideWheelESCheck:		1; 		/* bit 6 */
	unsigned lctcsu1CompDutyForL2Split:		1; 		/* bit 7 */
	///////////////////////////////////////////////////

#elif __HEV
    unsigned TCL_DEMAND:          1;   /* bit 2  */
   #endif

//**********************************************************************************
/* #if __ESP_ECU  // MGH-25 전용 only ESP 변수  */ /* 2006.06.15  K.G.Y */
#if __ECU==ESP_ECU_1
//  ABS+VDC                    // WFLAG17
//**********************************************************************************
    unsigned TEMP_HV_VL:          1;   /* bit 7  */
    unsigned TEMP_AV_VL:          1;   /* bit 6  */
    unsigned TEMP_TCL_DEMAND:     1;   /* bit 5  */
    unsigned VDC_ABS_WORK:        1;   /* bit 4  */    // not used!
    unsigned FIRST_VREF_SELECT:   1;   /* bit 3  */    // not used!
    unsigned F_A_R_P:             1;   /* bit 2  */    // not used!
    unsigned F_A_D_P:             1;   /* bit 1  */    // not used!
    unsigned F_A_H_P:             1;   /* bit 0  */    // not used!
                           // WFLAG18
    unsigned NOT_ZERO_VREF:       1;   /* bit 7  */    // not used!
    unsigned CAL_TVREF:           1;   /* bit 6  */    // not used!
    unsigned TEST_FLAG:           1;   /* bit 5  */    // not used!
    unsigned TEST_FLAG2:          1;   /* bit 4  */    // not used!
    unsigned ESP:                 1;   /* bit 3  */    // not used!
    unsigned flag150:             1;   /* bit 2  */
    unsigned flag151:             1;   /* bit 1  */

     			   // WFLAG19
  #if __ESP_ABS_COOPERATION_CONTROL
    unsigned ESP_ABS_Over_Front_Inside: 1;   /* bit 7  */
    unsigned ESP_ABS_Over_Rear_Inside:  1;   /* bit 6  */
    unsigned ESP_ABS_Over_Rear_Outside: 1;   /* bit 5  */
    unsigned ESP_ABS_Rear_Forced_Dump_K: 1;   /* bit 4  */
    unsigned ESP_ABS_Under_Rear_Outside: 1;   /* bit 3  */
    unsigned ESP_ABS_SLIP_CONTROL:       1;   /* bit 2  */
  #endif

  #if __UCC_COOPERATION_CONTROL
    unsigned UCC_DETECT_BUMP_flag:    1;   /* bit 1  */
    unsigned UCC_DETECT_PotHole_flag: 1;   /* bit 0  */
    unsigned ABS_PotHole_Stable_flg1:   1;   /* bit 7  */
    unsigned ABS_PotHole_Stable_flg2:	1;   /* bit 6  */
  #endif

    unsigned PF_SLIP_CONTROL:             1;   /* bit 4  */
    unsigned PF_SLIP_CONTROL_OLD:             1;   /* bit 3  */
    unsigned PF_SLIP_K:             1;   /* bit 2  */
    unsigned PF_SLIP_A:             1;   /* bit 1  */

  #if __VALVE_TEST_ESC_LFC || __VALVE_TEST_ESC_ABS_LFC
    unsigned laescu1ESCLFCTestActiveRise:             1;
  #endif          

#endif
//*********************************************************************************

//*********************************************************************************
/* #if __PWM && __ESP_ECU       // MGH-25 ESP ECU LFC PWM 전용 */ /* 2006.06.15  K.G.Y */
#if __ECU==ESP_ECU_1
//********************************************************************************
// LFC + VDC              // WFLAG20
    unsigned L_ABS_ESP_COMB:      1;   /* bit 7  */    // not used!
    unsigned L_DEL_P_CAL_OK:      1;   /* bit 6  */    // not used!
    unsigned LFC_MODE_K:          1;   /* bit 5  */    // not used!
    unsigned LFC_MODE_A:          1;   /* bit 4  */    // not used!
    unsigned LFC_WITHOUT_ESP:     1;   /* bit 3  */    // not used!
    unsigned LFC_WITH_ESP:        1;   /* bit 2  */    // not used!
    unsigned CROSS_SLIP_CONTROL:  1;   /* bit 1  */
    unsigned L_flag160:           1;   /* bit 0  */

// abs 협조제어
    unsigned WAS_LONG_REAPPLY:      1;   // bit 7      // not used!
    unsigned Forced_n_th_deep_slip_comp_flag: 1;   // bit 6
    unsigned LFC_to_ESP_flag:       1;   /* bit 5  */
    unsigned LFC_to_ESP_start_flag: 1;   /* bit 4  */
    unsigned LFC_to_ESP_end_flag:   1;   /* bit 3  */
    unsigned LFC_to_ESP_flag_old:   1;   /* bit 2  */
    unsigned ESP_control_during_ABS_flag:  1;   /* bit 1  */
  #if  (__VDC && __ABS_CORNER_BRAKING_IMPROVE) 
    unsigned lcabsu1CornerRearOutHoldPreOK:   1;   /* bit 0  */ 
    unsigned lcabsu1CornerRearOutHold:     1;   /* bit 7  */ 
    unsigned lcabsu1CornerRearOutCtrl:     1;   /* bit 6  */
  #endif  	
    unsigned ESPABS_flag_05:        1;   /* bit 5  */
    unsigned ESPABS_flag_04:        1;   /* bit 4  */
    unsigned L_SLIP_CONTROL:        1;   /* bit 3  */
    unsigned L_SLIP_CONTROL_OLD:    1;   /* bit 2  */
    unsigned l_SLIP_K:              1;   /* bit 1  */
    unsigned l_SLIP_A:              1;   /* bit 0  */
#endif
//*********************************************************************************

//*********************************************************************************
/* #if __PWM              // LFC PWM 공용 */ /* 2006.06.15  K.G.Y */
//*********************************************************************************

    unsigned OUTSIDE_ABS_hold_flag:         1;   /* bit 7  */
    unsigned Rough_road_detect_flag1:       1;   /* bit 6  */
    unsigned Rough_road_detect_flag2:       1;   /* bit 5  */
    unsigned LFC_n_th_deep_slip_comp_flag1: 1;   /* bit 4  */
    unsigned LFC_n_th_deep_slip_comp_flag2: 1;   /* bit 3  */
    unsigned LFC_three_built_check_flag:   	1;   /* bit 2  */
    unsigned LFC_n_th_deep_slip_pre_flag:   1;   /* bit 1  */
    unsigned LFC_big_rise_and_dump_flag:    1;   /* bit 0  */

    unsigned pwm_reapply_flag:              1;   /* bit 717*/
    unsigned LFC_built_reapply_flag:        1;   /* bit 6  */
    unsigned LFC_built_reapply_flag_old:    1;   /* bit 5  */
    unsigned LFC_two_built_flag:            1;   /* bit 4  */
    unsigned LFC_two_built_flag_old:        1;   /* bit 3  */
  #if __PRESS_EST_ABS
    unsigned LOW_MU_SUSPECT_by_IndexMu:     1;   /* bit 2  */
  #endif
  #if __TEMP_for_CAS
    unsigned dump_gain_mult_flag:    		1;   /* bit 1  */
  #endif
  #if __TEMP_for_CAS2
    unsigned dump_gain_mult_flag2:    		1;   /* bit 0  */
  #endif

    unsigned LFC_Conven_OnOff_Flag:             1;   /* bit 718*/
    unsigned Reapply_Accel_flag:                1;   /* bit 6  */
    unsigned LFC_initial_deep_slip_comp_flag:   1;   /* bit 5  */
    unsigned LFC_n_th_deep_slip_comp_flag:      1;   /* bit 4  */
    unsigned LFC_rough_road_disable_comp_flag:  1;   /* bit 3  */
    unsigned EBD_LFC_RISE_MODE:  				1;   /* bit 2  */
    unsigned Check_double_brake_flag:           1;   /* bit 1  */
    unsigned LFC_dump_start_flag:               1;   /* bit 0  */

    unsigned LFC_pres_rise_comp_disable_flag:   1;   /* bit 719*/
    unsigned LFC_special_comp_disable_flag:     1;   /* bit 6  */
    unsigned motor_force_off_flag:    			1;   /* bit 5  */
	unsigned Check_double_brake_flag2:		    1;	 /*	bit	4  */
	unsigned Low_Suspect_flag:			        1;	 /*	bit	3  */
	unsigned Duty_Limitation_flag:				1;	 /*	bit	2  */
    unsigned LFC_maintain_split_flag:           1;   /* bit 1  */
    unsigned MSL_long_hold_flag:				1;   /* bit 0  */

    unsigned Hold_Duty_Comp_flag:				1;   /* bit 720*/
    unsigned Hold_Duty_Comp_flag_1st:				1;   /* bit 6  */
    unsigned LFC_Big_Rise_detect:				1;   /* bit 5  */
    unsigned Shift_thresdec_flag:				1;   /* bit 4  */
    unsigned In_Gear_Vib_flag:				1;   /* bit 3  */
    unsigned In_Gear_flag:				1;   /* bit 2  */
    unsigned Jump_Mu_Cascading_sus_flag:		1;   /* bit 1  */
    unsigned Pulse_down_flag:					1;   /* bit 0  */
/* #endif */ /* 2006.06.15  K.G.Y */
//**********************************************************************************

//**********************************************************************************
					              // MGH40
//**********************************************************************************
  #if __USE_SIDE_VREF
    unsigned STABLE_for_SIDE_VREF_flag:          		    1;   /* bit 728*/
  #endif
  #if __DUMP_THRES_COMP_for_Spold_update
    unsigned SPOLD_COMP_FLG1:   		     	1;   /* bit 6  */
    unsigned SPOLD_COMP_FLG:		    	    1;   /* bit 5  */
  #endif
  #if __ESP_SPLIT_2 && __VDC
    unsigned GMA_ESP_Control:          			1;   /* bit 4  */
  #endif
  #if __UNSTABLE_REAPPLY_ENABLE
    unsigned Unstable_Rise_Pres_Cycle_flg:		1;   /* bit 3  */
    unsigned Unstable_Rise_Set_Cycle:		    1;   /* bit 2  */
    unsigned SL_Unstable_Rise_flg:		        1;   /* bit 1  */

    unsigned SL_Unstable_Rise_Cycle:		    1;   /* bit 0  */
  #endif

    unsigned In_Gear_arad_credible:		        1;   /* bit 7  */
    unsigned UNSTABLE_LowMu_comp_flag2:			1;   /* bit 6  */
  #if __DETECT_MU_FOR_UR
    unsigned lcabsu1BumpOrWAForUR:		        1;   /* bit 5  */
    unsigned lcabsu1LMuForUR:			        1;   /* bit 4  */
    unsigned lcabsu1HMuForUR:			        1;   /* bit 3  */
  #endif
//**********************************************************************************

#if	__EDC
    unsigned ldedcu1EdcDecelDetect:    				1;   /* bit 2  */
    unsigned ldedcu1EdcDecelDetect_1G5:    			1;   /* bit 2  */       	
    unsigned ldedcu1EdcNorOn:  						1;   /* bit 1  */
    unsigned lcedcu1EdcWheelExit:  					1;   /* bit 0  */
    unsigned ldedcu1ParkBrkSuspectWL:				1;   /* bit 7  */
    unsigned ldedcu1ParkDecelCheckWL:				1;   /* bit 6  */
#endif
   #if __STABLE_DUMPHOLD_ENABLE
    unsigned lcabsu1HoldAfterStabDumpFlg:           1;   /* bit 5  */
    unsigned lcabsu1StabDumpCycleScan:              1;   /* bit 4  */
    unsigned lcabsu1StabDumpHoldCycle:              1;   /* bit 3  */
    unsigned lcabsu1StabDumpAndHoldFlg:             1;   /* bit 2  */

    unsigned lcabsu1StartStabDumpAndHold:           1;   /* bit 1 */
    unsigned lcabsu1StabDump:                       1;   /* bit 0  */
   #endif

  #if __DETECT_LOCK_IN_CYCLE
    unsigned lcabsu1PossibleWLLockInCycle:				1;   /* bit 7  */
  #endif

  #if __PRES_RISE_DEFER_FOR_UR
    unsigned lcabsu1LimitURPresRise:				1;   /* bit 6  */
  #endif
  
    unsigned lcabsu1SLDumpCycle:				1;   /* bit 5  */

  #if __REDUCE_1ST_UR_PRESS_DIFF
    unsigned AdaptURPerOppositeWheel:               1;   /* bit 4  */
  #endif
  #if __REDUCE_ABS_TIME_AT_BUMP
    unsigned PossibleABSForBump:				1;   /* bit 3  */
  #endif

  #if __WHEEL_PRESSURE_SENSOR
    unsigned DumpPerSlipScan:				1;   /* bit 2  */
  #endif

  #if __IMPROVE_CASCADING
    unsigned CAS_SUSPECT_flag_old:    			1;   /* bit 1  */
  #endif

	unsigned lau1SPHighSideCompFlag:              1;  /* bit 0 */
	
  #if __ECU==ESP_ECU_1	&& __ECU_TYPE_2 == __INTEGRATED 	// MOCi 
	unsigned WheelSpeedVD:        		 	1;  /* bit 0 */ 
	unsigned WheelSpeed_VD:        			1;  /* bit 0 */ 
	unsigned WssPulse_VD:                   1;   
    unsigned WssRollBechFrtOK:              1;    
    unsigned WssRollBechRearOK:             1;
    	 
    uint8_t WssPulseCntOld;    
    uint8_t WssPulseCnt; 
    uint8_t WssPulseCntDiffCnt;
    uint8_t WssPulseCnt1secCnt;
    uint8_t WssPulseCntSumCnt;

    uint8_t WheelSpeed_VD_Cnt;              //Valid Check
	
  #endif	//~#if __ECU==ESP_ECU_1	&& __ECU_TYPE_2 == __INTEGRATED 	// MOCi 

    /* 2012_On_SWD_KJS */
    unsigned WPE_First_Control_VM:      1; // ??
    /* 2012_On_SWD_KJS */		
	unsigned CROSS_SLIP_CONTROL_old:	1;

  #if ((__AHB_GEN3_SYSTEM==ENABLE) || (__IDB_LOGIC == ENABLE))
    unsigned 	laabsu1Rise:				1;	
    unsigned	laabsu1PulseupRise:		1;	
    unsigned 	AV_DUMP_OLD:             	1; /* for temporary capture estimated Press when dump act */
    unsigned    U1EscOtherWheelCnt; 
    unsigned 	lcabsu1TarDeltaPLimitFlg:	1;
    int16_t lcesps16IdbOtherPress;
    int16_t lcesps16IdbOtherPressOld;
    int16_t lcesps16IdbOtherPressRte;  
    int16_t lcesps16IdbDelOtherPress;
  #endif	
};

  #if __ECU==ESP_ECU_1	&& __ECU_TYPE_2 == __INTEGRATED 	// MOCi 
#define lsu1WheelSpeed_FLVD         FL.WheelSpeedVD
#define lsu1WheelSpeed_FL_VD        FL.WheelSpeed_VD
#define RELIABLE_FOR_VREF_fl		FL.RELIABLE_FOR_VREF
#define lsu8FLWssPulseCntOld        FL.WssPulseCntOld	
#define lsu8FLWssPulseCnt           FL.WssPulseCnt		
#define lsu8FLWssPulseCntDiffCnt    FL.WssPulseCntDiffCnt
#define lsu8FLWssPulseCnt1secCnt    FL.WssPulseCnt1secCnt                  
#define lsu8FLWssPulse_VD		    FL.WssPulse_VD

#define lsu1WheelSpeed_FRVD         FR.WheelSpeedVD
#define lsu1WheelSpeed_FR_VD        FR.WheelSpeed_VD
#define RELIABLE_FOR_VREF_fr		FR.RELIABLE_FOR_VREF
#define lsu8FRWssPulseCntOld        FR.WssPulseCntOld	
#define lsu8FRWssPulseCnt           FR.WssPulseCnt		
#define lsu8FRWssPulseCntDiffCnt    FR.WssPulseCntDiffCnt
#define lsu8FRWssPulseCnt1secCnt    FR.WssPulseCnt1secCnt                  
#define lsu8FRWssPulse_VD		    FR.WssPulse_VD

#define lsu1WheelSpeed_RLVD         RL.WheelSpeedVD
#define lsu1WheelSpeed_RL_VD        RL.WheelSpeed_VD
#define RELIABLE_FOR_VREF_rl		RL.RELIABLE_FOR_VREF
#define lsu8RLWssPulseCntOld        RL.WssPulseCntOld	
#define lsu8RLWssPulseCnt           RL.WssPulseCnt		
#define lsu8RLWssPulseCntDiffCnt    RL.WssPulseCntDiffCnt
#define lsu8RLWssPulseCnt1secCnt    RL.WssPulseCnt1secCnt                  
#define lsu8RLWssPulse_VD		    RL.WssPulse_VD

#define lsu1WheelSpeed_RRVD         RR.WheelSpeedVD
#define lsu1WheelSpeed_RR_VD        RR.WheelSpeed_VD
#define RELIABLE_FOR_VREF_rr		RR.RELIABLE_FOR_VREF
#define lsu8RRWssPulseCntOld        RR.WssPulseCntOld	
#define lsu8RRWssPulseCnt           RR.WssPulseCnt		
#define lsu8RRWssPulseCntDiffCnt    RR.WssPulseCntDiffCnt
#define lsu8RRWssPulseCnt1secCnt    RR.WssPulseCnt1secCnt                  
#define lsu8RRWssPulse_VD		    RR.WssPulse_VD
  #endif	//~#if __ECU==ESP_ECU_1	&& __ECU_TYPE_2 == __INTEGRATED 	// MOCi 

#define MS_CONTROL_wl              WL->MS_CONTROL
#define MS_CONTROL_fl              FL.MS_CONTROL
#define MS_CONTROL_fr              FR.MS_CONTROL
#define MS_CONTROL_rl              RL.MS_CONTROL
#define MS_CONTROL_rr              RR.MS_CONTROL

//#define WAS_LONG_REAPPLY_wl        WL->WAS_LONG_REAPPLY
//#define WAS_LONG_REAPPLY_rl        RL.WAS_LONG_REAPPLY
//#define WAS_LONG_REAPPLY_rr        RR.WAS_LONG_REAPPLY

//#define NO_COMP_DUTY_wl            WL->NO_COMP_DUTY
//#define NO_COMP_DUTY_rl            RL.NO_COMP_DUTY
//#define NO_COMP_DUTY_rr            RR.NO_COMP_DUTY

#define state_fl                    FL.state
#define zyklus_z_fl                 FL.zyklus_z
#define diff_slip_fl                FL.diff_slip
#define s0_afz_fl                   FL.s0_afz
#if	__CREEP_VIBRATION
#define v_ungef_alt_fl              FL.v_ungef_alt
#endif
#define vrad_fl                     FL.vrad
#define vrad_crt_fl                 FL.vrad_crt
#define vdiff_fl                    FL.vdiff
#define speed_fl                    FL.speed
#define speed_alt_fl                FL.speed_alt
#define speed_filtered_fl           FL.speed_filtered
#define speed_filtered_alt_fl       FL.speed_filtered_alt
#define s_diff_fl                   FL.s_diff
//#define s_diff_alt_fl               FL.s_diff_alt
//#define s_diff_kph_fl               FL.s_diff_kph
//#define s_diff_alt_kph_fl           FL.s_diff_alt_kph
#define s0_afz1_fl                  FL.s0_afz1
#define spold_fl                    FL.spold
#define vfilt_rough_fl              FL.vfilt_rough
#define vfiltn_fl                   FL.vfiltn
#define avr_arad_fl                 FL.avr_arad
#define avr_arad_alt_fl             FL.avr_arad_alt
#define arad_fl                     FL.arad
#define arad_alt_fl                 FL.arad_alt
#define peak_acc_fl                 FL.peak_acc
#define peak_dec_fl                 FL.peak_dec
#define max_arad_instbl_fl          FL.max_arad_instbl
#define rel_lam_fl                  FL.rel_lam
#define peak_slip_fl                FL.peak_slip
#define reapply_tim_fl              FL.reapply_tim
#define castim_fl                   FL.castim
#define thres_decel_fl              FL.thres_decel
#define natrl_frq_counter_fl        FL.natrl_frq_counter
#define hold_timer_new_fl           FL.hold_timer_new
//#define continue_dump_timer_fl           FL.continue_dump_timer
//#define timer_new_fl                FL.timer_new
#define av_t_aqua_fl                FL.av_t_aqua
#define ab_z_pr_fl                  FL.ab_z_pr
#define ab_zaehl_fl                 FL.ab_zaehl
#define slew_rate_timer_fl          FL.slew_rate_timer
#define flags_fl                    FL.flags
#define pulse_up_row_timer_fl       FL.pulse_up_row_timer
//#define s0_reapply_counter_fl       FL.s0_reapply_counter
#define ab_auf_counter_fl           FL.ab_auf_counter
#define time_aft_thrdec_fl          FL.time_aft_thrdec
#define allow_dump_fl               FL.allow_dump
//#define increase_p_time_fl          FL.increase_p_time
#define peak_acc_alt_fl             FL.peak_acc_alt
#define peak_dec_alt_fl             FL.peak_dec_alt
#define peak_dec_max_fl             FL.peak_dec_max
#define holding_cycle_timer_fl      FL.holding_cycle_timer
//#define roughold_fl                 FL.roughold
#define gma_dump_fl                 FL.gma_dump
#define gma_counter_fl              FL.gma_counter
#define slip_at_real_dump_fl        FL.slip_at_real_dump
//#define p_hold_time_fl              FL.p_hold_time
//#define rel_lam_alt_fl              FL.rel_lam_alt
//#define holding_time_fl             FL.holding_time
#define s0_reapply_alt_count_fl     FL.s0_reapply_alt_count
#define vib_counter_fl              FL.vib_counter
#define depress_time_fl             FL.depress_time
//#define arad_at_dump_fl             FL.arad_at_dump
#define p_shooting_time_fl          FL.p_shooting_time
#define p_row_change_timer_fl       FL.p_row_change_timer
#define spold_rst_count_fl          FL.spold_rst_count
//#define vfilt_timer_fl              FL.vfilt_timer
#define frcnt_fl                    FL.frcnt
#define rough_hold_time_fl          FL.rough_hold_time
//#define force_dump_counter_fl       FL.force_dump_counter
#define peak_acc_afz_fl             FL.peak_acc_afz
#define vfilt_comp_fl               FL.vfilt_comp
//#define mini_whl_counter_fl         FL.mini_whl_counter
#define mu_jump_p_counter_fl        FL.mu_jump_p_counter
#define ebd_dump_time_fl            FL.ebd_dump_time
#define on_time_inlet_alt_fl        FL.on_time_inlet_alt
#define on_time_inlet_fl            FL.on_time_inlet
#define on_time_outlet_fl           FL.on_time_outlet
#define on_time_special_fl          FL.on_time_special
#define msl_hold_time_fl            FL.msl_hold_time
//#define msl_gma_counter_fl          FL.msl_gma_counter
#define gma_dump1_fl                FL.gma_dump1
//#define msl_reapply_time_fl         FL.msl_reapply_time
#define det_count_fl                FL.det_count
//#define EBD_Dump_timer_fl           FL.EBD_Dump_timer
#define ebd_peak_dec_fl             FL.ebd_peak_dec

				                 //020927 제동거리
#define High_dump_factor_fl         FL.High_dump_factor
//#define High_dump_factor2_fl         FL.High_dump_factor2
#define Dump_hold_scan_ref_fl       FL.Dump_hold_scan_ref
#define Dump_hold_scan_counter_fl   FL.Dump_hold_scan_counter

#if __EDC
#define   ldedcs16DeltaVelAtMinusB_1G5_fl	FL.ldedcs16DeltaVelAtMinusB_1G5
#define   ldedcs16DeltaVelAtMinusB_0G5_fl	FL.ldedcs16DeltaVelAtMinusB_0G5
#define   ldedcs16DeltaVelocity_fl		FL.ldedcs16DeltaVelocity
#define   lcedcs16WheelExitCount_fl    	FL.lcedcs16WheelExitCount
#define   lcedcs16WheelSlip_fl      	FL.lcedcs16WheelSlip
#define   ldedcu8ParkBrkSuspectCounter_fl      	FL.ldedcu8ParkBrkSuspectCounter
#define   ldedcu16EngRpmAtDecelCheck_fl      	FL.ldedcu16EngRpmAtDecelCheck
#define   ldedcu8ParkDecelCheckCnt_fl      	FL.ldedcu8ParkDecelCheckCnt
#endif

#if __VDC
#define wvref_fl                    FL.wvref

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//#define esp_control_mode_fl         FL.esp_control_mode
//#define esp_control_mode_old_fl     FL.esp_control_mode_old
#else
#define esp_control_mode_fl         FL.esp_control_mode
#define esp_control_mode_old_fl     FL.esp_control_mode_old
#endif

#define esp_final_mode_fl       	FL.esp_final_mode

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//#define esp_control_cycle_fl       	FL.esp_control_cycle
#else
#define esp_control_cycle_fl       	FL.esp_control_cycle
#endif

#endif

//  #define press_test_reapply_fl     FL.press_test_reapply
//  #define press_test_hold_fl        FL.press_test_hold

/* btcs */
#define b_rad_fl                    FL.b_rad
#define vrad_alt_fl                 FL.vrad_alt
#define average7_fl                 FL.average7
#define btc_slip_fl                 FL.btc_slip
#define slip_min_fl                 FL.slip_min
#define btc_speed_fl                FL.btc_speed
#define btc_speed_alt_fl            FL.btc_speed_alt
#define ffspeed_fl                  FL.ffspeed
#define fspeed_fl                   FL.fspeed
#define fspeed_alt_fl               FL.fspeed_alt
#define av_fspeed_alt_fl            FL.av_fspeed_alt
#define av_fspeed_fl                FL.av_fspeed
#define av_btc_slip_fl              FL.av_btc_slip
#define av_btc_slip_alt_fl          FL.av_btc_slip_alt
#define hold_count_fl               FL.hold_count
#define hold_count_both_spin_fl     FL.hold_count_both_spin
#define btcinc_count_fl             FL.btcinc_count
#define btcdec_count_fl             FL.btcdec_count
#define btc_count1_fl               FL.btc_count1
#define btc_count2_fl               FL.btc_count2
#define vib_count1_fl               FL.vib_count1
#define vib_count2_fl               FL.vib_count2
#define vib_count3_fl               FL.vib_count3
#define vib_count4_fl               FL.vib_count4
#define slip_count1_fl              FL.slip_count1
#define slip_count2_fl              FL.slip_count2
#define slip_count3_fl              FL.slip_count3
#define exit_timer1_fl              FL.exit_timer1
#define exit_timer2_fl              FL.exit_timer2
#define exit_timer3_fl              FL.exit_timer3
#define exit_timer4_fl              FL.exit_timer4
#define exit_timer5_fl              FL.exit_timer5
#define hold_timer_fl               FL.hold_timer
#define count7_fl                   FL.count7
#define count14_fl                  FL.count14
#define count20_fl                  FL.count20
#define refdiff_fl                  FL.refdiff
#define refdiff_alt1_fl             FL.refdiff_alt1
#define refdiff_alt2_fl             FL.refdiff_alt2
#define av_ref_max_fl               FL.av_ref_max
#define av_ref_fl                   FL.av_ref
#define av_ref_alt_fl               FL.av_ref_alt
#define btc_count3_fl               FL.btc_count3
#define btc_count4_fl               FL.btc_count4
#define slip_count4_fl              FL.slip_count4
#define valve_delay_count_fl              FL.valve_delay_count
#define barad_max_fl                FL.barad_max
#define barad_alt_fl                FL.barad_alt
#define TCS_built_reapply_pattern_fl  FL.TCS_built_reapply_pattern
#define TCS_Initial_Duty_fl  FL.TCS_Initial_Duty
#define TCS_Duty_Ratio_decrease_fl  FL.TCS_Duty_Ratio_decrease
#define TCS_built_reapply_time_fl   FL.TCS_built_reapply_time

/* tmp */
#if  __BTEM == DISABLE
#define btc_tmp_fl                  FL.btc_tmp
#define tmprad_alt_fl               FL.tmprad_alt
#define btc_tmp_alt_fl              FL.btc_tmp_alt
#define tmprad_max_fl               FL.tmprad_max
#define cool_tmp_fl                 FL.cool_tmp
#define tmp_acc_fl                  FL.tmp_acc
#define acc_perc_fl                 FL.acc_perc
#define out_time_fl                 FL.out_time
#define tmp_count_fl                FL.tmp_count
#define max_spin_fl                 FL.max_spin
#define tmp_time_fl                 FL.tmp_time
#define tmp_down_fl                 FL.tmp_down
#define tmp_up_fl                   FL.tmp_up
#define inc_count1_fl               FL.inc_count1
#define inc_count2_fl               FL.inc_count2
#define cal_tmp_fl                  FL.cal_tmp
#endif

#define inc_count3_fl               FL.inc_count3
#define tmp_state_fl                FL.tmp_state
#define BTCS_flags_fl				FL.BTCS_flags

#if	__TCS
#define mini_count_fl				FL.mini_count
#endif

#define state_fr                    FR.state
#define zyklus_z_fr                 FR.zyklus_z
#define diff_slip_fr                FR.diff_slip
#define s0_afz_fr                   FR.s0_afz
#if	__CREEP_VIBRATION
#define v_ungef_alt_fr              FR.v_ungef_alt
#endif
#define vrad_fr                     FR.vrad
#define vrad_crt_fr                 FR.vrad_crt
#define vdiff_fr                    FR.vdiff
#define speed_fr                    FR.speed
#define speed_alt_fr                FR.speed_alt
#define speed_filtered_fr           FR.speed_filtered
#define speed_filtered_alt_fr       FR.speed_filtered_alt
#define s_diff_fr                   FR.s_diff
//#define s_diff_alt_fr               FR.s_diff_alt
//#define s_diff_kph_fr               FR.s_diff_kph
//#define s_diff_alt_kph_fr           FR.s_diff_alt_kph
#define s0_afz1_fr                  FR.s0_afz1
#define spold_fr                    FR.spold
#define vfilt_rough_fr              FR.vfilt_rough
#define vfiltn_fr                   FR.vfiltn
#define avr_arad_fr                 FR.avr_arad
#define avr_arad_alt_fr             FR.avr_arad_alt
#define arad_fr                     FR.arad
#define arad_alt_fr                 FR.arad_alt
#define peak_acc_fr                 FR.peak_acc
#define peak_dec_fr                 FR.peak_dec
#define max_arad_instbl_fr          FR.max_arad_instbl
#define rel_lam_fr                  FR.rel_lam
#define peak_slip_fr                FR.peak_slip
#define reapply_tim_fr              FR.reapply_tim
#define castim_fr                   FR.castim
#define thres_decel_fr              FR.thres_decel
#define natrl_frq_counter_fr        FR.natrl_frq_counter
#define hold_timer_new_fr           FR.hold_timer_new
//#define continue_dump_timer_fr           FR.continue_dump_timer
//#define timer_new_fr                FR.timer_new
#define av_t_aqua_fr                FR.av_t_aqua
#define ab_z_pr_fr                  FR.ab_z_pr
#define ab_zaehl_fr                 FR.ab_zaehl
#define slew_rate_timer_fr          FR.slew_rate_timer
#define flags_fr                    FR.flags
#define pulse_up_row_timer_fr       FR.pulse_up_row_timer
//#define s0_reapply_counter_fr       FR.s0_reapply_counter
#define ab_auf_counter_fr           FR.ab_auf_counter
#define time_aft_thrdec_fr          FR.time_aft_thrdec
#define allow_dump_fr               FR.allow_dump
//#define increase_p_time_fr          FR.increase_p_time
#define peak_acc_alt_fr             FR.peak_acc_alt
#define peak_dec_alt_fr             FR.peak_dec_alt
#define peak_dec_max_fr             FR.peak_dec_max
#define holding_cycle_timer_fr      FR.holding_cycle_timer
//#define roughold_fr                 FR.roughold
#define gma_dump_fr                 FR.gma_dump
#define gma_counter_fr              FR.gma_counter
#define slip_at_real_dump_fr        FR.slip_at_real_dump
//#define p_hold_time_fr              FR.p_hold_time
//#define rel_lam_alt_fr              FR.rel_lam_alt
//#define holding_time_fr             FR.holding_time
#define s0_reapply_alt_count_fr     FR.s0_reapply_alt_count
#define vib_counter_fr              FR.vib_counter
#define depress_time_fr             FR.depress_time
//#define arad_at_dump_fr             FR.arad_at_dump
#define p_shooting_time_fr          FR.p_shooting_time
#define p_row_change_timer_fr       FR.p_row_change_timer
#define spold_rst_count_fr          FR.spold_rst_count
//#define vfilt_timer_fr              FR.vfilt_timer
#define frcnt_fr                    FR.frcnt
#define rough_hold_time_fr          FR.rough_hold_time
//#define force_dump_counter_fr       FR.force_dump_counter
#define peak_acc_afz_fr             FR.peak_acc_afz
#define vfilt_comp_fr               FR.vfilt_comp
//#define mini_whl_counter_fr         FR.mini_whl_counter
#define mu_jump_p_counter_fr        FR.mu_jump_p_counter
#define ebd_dump_time_fr            FR.ebd_dump_time
#define on_time_inlet_alt_fr        FR.on_time_inlet_alt
#define on_time_inlet_fr            FR.on_time_inlet
#define on_time_outlet_fr           FR.on_time_outlet
#define on_time_special_fr          FR.on_time_special
#define msl_hold_time_fr            FR.msl_hold_time
//#define msl_gma_counter_fr          FR.msl_gma_counter
#define gma_dump1_fr                FR.gma_dump1
//#define msl_reapply_time_fr         FR.msl_reapply_time
#define det_count_fr                FR.det_count
//#define EBD_Dump_timer_fr           FR.EBD_Dump_timer
#define ebd_peak_dec_fr             FR.ebd_peak_dec

				                 //020927 제동거리
#define High_dump_factor_fr         FR.High_dump_factor
//#define High_dump_factor2_fr         FR.High_dump_factor2
#define Dump_hold_scan_ref_fr       FR.Dump_hold_scan_ref
#define Dump_hold_scan_counter_fr   FR.Dump_hold_scan_counter

#if __EDC
#define   ldedcs16DeltaVelAtMinusB_1G5_fr	FR.ldedcs16DeltaVelAtMinusB_1G5
#define   ldedcs16DeltaVelAtMinusB_0G5_fr	FR.ldedcs16DeltaVelAtMinusB_0G5
#define   ldedcs16DeltaVelocity_fr		FR.ldedcs16DeltaVelocity
#define   lcedcs16WheelExitCount_fr    	FR.lcedcs16WheelExitCount
#define   lcedcs16WheelSlip_fr      	FR.lcedcs16WheelSlip
#define   ldedcu8ParkBrkSuspectCounter_fr      	FR.ldedcu8ParkBrkSuspectCounter
#define   ldedcu16EngRpmAtDecelCheck_fr      	FR.ldedcu16EngRpmAtDecelCheck
#define   ldedcu8ParkDecelCheckCnt_fr      	FR.ldedcu8ParkDecelCheckCnt
#endif

#if __VDC
#define wvref_fr                    FR.wvref

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//#define esp_control_mode_fr         FR.esp_control_mode
//#define esp_control_mode_old_fr     FR.esp_control_mode_old
#else
#define esp_control_mode_fr         FR.esp_control_mode
#define esp_control_mode_old_fr     FR.esp_control_mode_old
#endif

#define esp_final_mode_fr       	FR.esp_final_mode

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//#define esp_control_cycle_fr       	FR.esp_control_cycle
#else
#define esp_control_cycle_fr       	FR.esp_control_cycle
#endif

#endif

//  #define press_test_reapply_fr       FR.press_test_reapply
//  #define press_test_hold_fr          FR.press_test_hold

/* btcs */
#define b_rad_fr                    FR.b_rad
#define vrad_alt_fr                 FR.vrad_alt
#define average7_fr                 FR.average7
#define btc_slip_fr                 FR.btc_slip
#define slip_min_fr                 FR.slip_min
#define btc_speed_fr                FR.btc_speed
#define btc_speed_alt_fr            FR.btc_speed_alt
#define ffspeed_fr                  FR.ffspeed
#define fspeed_fr                   FR.fspeed
#define fspeed_alt_fr               FR.fspeed_alt
#define av_fspeed_alt_fr            FR.av_fspeed_alt
#define av_fspeed_fr                FR.av_fspeed
#define av_btc_slip_fr              FR.av_btc_slip
#define av_btc_slip_alt_fr          FR.av_btc_slip_alt
#define hold_count_fr               FR.hold_count
#define hold_count_both_spin_fr     FR.hold_count_both_spin
#define btcinc_count_fr             FR.btcinc_count
#define btcdec_count_fr             FR.btcdec_count
#define btc_count1_fr               FR.btc_count1
#define btc_count2_fr               FR.btc_count2
#define vib_count1_fr               FR.vib_count1
#define vib_count2_fr               FR.vib_count2
#define vib_count3_fr               FR.vib_count3
#define vib_count4_fr               FR.vib_count4
#define slip_count1_fr              FR.slip_count1
#define slip_count2_fr              FR.slip_count2
#define slip_count3_fr              FR.slip_count3
#define exit_timer1_fr              FR.exit_timer1
#define exit_timer2_fr              FR.exit_timer2
#define exit_timer3_fr              FR.exit_timer3
#define exit_timer4_fr              FR.exit_timer4
#define exit_timer5_fr              FR.exit_timer5
#define hold_timer_fr               FR.hold_timer
#define count7_fr                   FR.count7
#define count14_fr                  FR.count14
#define count20_fr                  FR.count20
#define refdiff_fr                  FR.refdiff
#define refdiff_alt1_fr             FR.refdiff_alt1
#define refdiff_alt2_fr             FR.refdiff_alt2
#define av_ref_max_fr               FR.av_ref_max
#define av_ref_fr                   FR.av_ref
#define av_ref_alt_fr               FR.av_ref_alt
#define btc_count3_fr               FR.btc_count3
#define btc_count4_fr               FR.btc_count4
#define slip_count4_fr              FR.slip_count4
#define valve_delay_count_fr              FR.valve_delay_count
#define barad_max_fr                FR.barad_max
#define barad_alt_fr                FR.barad_alt
#define TCS_built_reapply_pattern_fr  FR.TCS_built_reapply_pattern
#define TCS_Initial_Duty_fr         FR.TCS_Initial_Duty
#define TCS_Duty_Ratio_decrease_fr  FR.TCS_Duty_Ratio_decrease
#define TCS_built_reapply_time_fr   FR.TCS_built_reapply_time

/* tmp */
#if  __BTEM == DISABLE
#define btc_tmp_fr                  FR.btc_tmp
#define tmprad_alt_fr               FR.tmprad_alt
#define btc_tmp_alt_fr              FR.btc_tmp_alt
#define tmprad_max_fr               FR.tmprad_max
#define cool_tmp_fr                 FR.cool_tmp
#define tmp_acc_fr                  FR.tmp_acc
#define acc_perc_fr                 FR.acc_perc
#define out_time_fr                 FR.out_time
#define tmp_count_fr                FR.tmp_count
#define max_spin_fr                 FR.max_spin
#define tmp_time_fr                 FR.tmp_time
#define tmp_down_fr                 FR.tmp_down
#define tmp_up_fr                   FR.tmp_up
#define inc_count1_fr               FR.inc_count1
#define inc_count2_fr               FR.inc_count2
#define cal_tmp_fr                  FR.cal_tmp
#endif

#define inc_count3_fr               FR.inc_count3
#define tmp_state_fr                FR.tmp_state
#define BTCS_flags_fr				FR.BTCS_flags

#if	__TCS
#define mini_count_fr				FR.mini_count
#endif

#define state_rl                    RL.state
#define zyklus_z_rl                 RL.zyklus_z
#define diff_slip_rl                RL.diff_slip
#define s0_afz_rl                   RL.s0_afz
#if	__CREEP_VIBRATION
#define v_ungef_alt_rl              RL.v_ungef_alt
#endif
#define vrad_rl                     RL.vrad
#define vrad_crt_rl                 RL.vrad_crt
#define vdiff_rl                    RL.vdiff
#define speed_rl                    RL.speed
#define speed_alt_rl                RL.speed_alt
#define speed_filtered_rl           RL.speed_filtered
#define speed_filtered_alt_rl       RL.speed_filtered_alt
#define s_diff_rl                   RL.s_diff
//#define s_diff_alt_rl               RL.s_diff_alt
//#define s_diff_kph_rl               RL.s_diff_kph
//#define s_diff_alt_kph_rl           RL.s_diff_alt_kph
#define s0_afz1_rl                  RL.s0_afz1
#define spold_rl                    RL.spold
#define vfilt_rough_rl              RL.vfilt_rough
#define vfiltn_rl                   RL.vfiltn
#define avr_arad_rl                 RL.avr_arad
#define avr_arad_alt_rl             RL.avr_arad_alt
#define arad_rl                     RL.arad
#define arad_alt_rl                 RL.arad_alt
#define peak_acc_rl                 RL.peak_acc
#define peak_dec_rl                 RL.peak_dec
#define max_arad_instbl_rl          RL.max_arad_instbl
#define rel_lam_rl                  RL.rel_lam
#define peak_slip_rl                RL.peak_slip
#define reapply_tim_rl              RL.reapply_tim
#define castim_rl                   RL.castim
#define thres_decel_rl              RL.thres_decel
#define natrl_frq_counter_rl        RL.natrl_frq_counter
#define hold_timer_new_rl           RL.hold_timer_new
//#define continue_dump_timer_rl           RL.continue_dump_timer
//#define timer_new_rl                RL.timer_new
#define av_t_aqua_rl                RL.av_t_aqua
#define ab_z_pr_rl                  RL.ab_z_pr
#define ab_zaehl_rl                 RL.ab_zaehl
#define slew_rate_timer_rl          RL.slew_rate_timer
#define flags_rl                    RL.flags
#define pulse_up_row_timer_rl       RL.pulse_up_row_timer
//#define s0_reapply_counter_rl       RL.s0_reapply_counter
#define ab_auf_counter_rl           RL.ab_auf_counter
#define time_aft_thrdec_rl          RL.time_aft_thrdec
#define allow_dump_rl               RL.allow_dump
//#define increase_p_time_rl          RL.increase_p_time
#define peak_acc_alt_rl             RL.peak_acc_alt
#define peak_dec_alt_rl             RL.peak_dec_alt
#define peak_dec_max_rl             RL.peak_dec_max
#define holding_cycle_timer_rl      RL.holding_cycle_timer
//#define roughold_rl                 RL.roughold
#define gma_dump_rl                 RL.gma_dump
#define gma_counter_rl              RL.gma_counter
#define slip_at_real_dump_rl        RL.slip_at_real_dump
//#define p_hold_time_rl              RL.p_hold_time
//#define rel_lam_alt_rl              RL.rel_lam_alt
//#define holding_time_rl             RL.holding_time
#define s0_reapply_alt_count_rl     RL.s0_reapply_alt_count
#define vib_counter_rl              RL.vib_counter
#define depress_time_rl             RL.depress_time
//#define arad_at_dump_rl             RL.arad_at_dump
#define p_shooting_time_rl          RL.p_shooting_time
#define p_row_change_timer_rl       RL.p_row_change_timer
#define spold_rst_count_rl          RL.spold_rst_count
//#define vfilt_timer_rl              RL.vfilt_timer
#define frcnt_rl                    RL.frcnt
#define rough_hold_time_rl          RL.rough_hold_time
//#define force_dump_counter_rl       RL.force_dump_counter
#define peak_acc_afz_rl             RL.peak_acc_afz
#define vfilt_comp_rl               RL.vfilt_comp
//#define mini_whl_counter_rl         RL.mini_whl_counter
#define mu_jump_p_counter_rl        RL.mu_jump_p_counter
#define ebd_dump_time_rl            RL.ebd_dump_time
#define on_time_inlet_alt_rl        RL.on_time_inlet_alt
#define on_time_inlet_rl            RL.on_time_inlet
#define on_time_outlet_rl           RL.on_time_outlet
#define on_time_special_rl          RL.on_time_special
#define msl_hold_time_rl            RL.msl_hold_time
//#define msl_gma_counter_rl          RL.msl_gma_counter
#define gma_dump1_rl                RL.gma_dump1
//#define msl_reapply_time_rl         RL.msl_reapply_time
#define det_count_rl                RL.det_count
//#define EBD_Dump_timer_rl           RL.EBD_Dump_timer
#define ebd_peak_dec_rl             RL.ebd_peak_dec

				                 //020927 제동거리
#define High_dump_factor_rl         RL.High_dump_factor
//#define High_dump_factor2_rl         RL.High_dump_factor2
#define Dump_hold_scan_ref_rl       RL.Dump_hold_scan_ref
#define Dump_hold_scan_counter_rl   RL.Dump_hold_scan_counter

#if __EDC
#define   ldedcs16DeltaVelAtMinusB_1G5_rl	RL.ldedcs16DeltaVelAtMinusB_1G5
#define   ldedcs16DeltaVelAtMinusB_0G5_rl	RL.ldedcs16DeltaVelAtMinusB_0G5
#define   ldedcs16DeltaVelocity_rl		RL.ldedcs16DeltaVelocity
#define   lcedcs16WheelExitCount_rl    	RL.lcedcs16WheelExitCount
#define   lcedcs16WheelSlip_rl      	RL.lcedcs16WheelSlip
#define   ldedcu8ParkBrkSuspectCounter_rl      	RL.ldedcu8ParkBrkSuspectCounter
#define   ldedcu16EngRpmAtDecelCheck_rl      	RL.ldedcu16EngRpmAtDecelCheck
#define   ldedcu8ParkDecelCheckCnt_rl      	RL.ldedcu8ParkDecelCheckCnt
#endif

#if __VDC
#define wvref_rl                    RL.wvref

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//#define esp_control_mode_rl         RL.esp_control_mode
//#define esp_control_mode_old_rl     RL.esp_control_mode_old
#else
#define esp_control_mode_rl         RL.esp_control_mode
#define esp_control_mode_old_rl     RL.esp_control_mode_old
#endif

#define esp_final_mode_rl      	    RL.esp_final_mode

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//#define esp_control_cycle_rl        RL.esp_control_cycle
#else
#define esp_control_cycle_rl        RL.esp_control_cycle
#endif

#endif

//  #define press_test_reapply_rl     RL.press_test_reapply
//  #define press_test_hold_rl        RL.press_test_hold

/* btcs */
#define b_rad_rl                    RL.b_rad
#define vrad_alt_rl                 RL.vrad_alt
#define average7_rl                 RL.average7
#define btc_slip_rl                 RL.btc_slip
#define slip_min_rl                 RL.slip_min
#define btc_speed_rl                RL.btc_speed
#define btc_speed_alt_rl            RL.btc_speed_alt
#define ffspeed_rl                  RL.ffspeed
#define fspeed_rl                   RL.fspeed
#define fspeed_alt_rl               RL.fspeed_alt
#define av_fspeed_alt_rl            RL.av_fspeed_alt
#define av_fspeed_rl                RL.av_fspeed
#define av_btc_slip_rl              RL.av_btc_slip
#define av_btc_slip_alt_rl          RL.av_btc_slip_alt
#define hold_count_rl               RL.hold_count
#define hold_count_both_spin_rl     RL.hold_count_both_spin
#define btcinc_count_rl             RL.btcinc_count
#define btcdec_count_rl             RL.btcdec_count
#define btc_count1_rl               RL.btc_count1
#define btc_count2_rl               RL.btc_count2
#define vib_count1_rl               RL.vib_count1
#define vib_count2_rl               RL.vib_count2
#define vib_count3_rl               RL.vib_count3
#define vib_count4_rl               RL.vib_count4
#define slip_count1_rl              RL.slip_count1
#define slip_count2_rl              RL.slip_count2
#define slip_count3_rl              RL.slip_count3
#define exit_timer1_rl              RL.exit_timer1
#define exit_timer2_rl              RL.exit_timer2
#define exit_timer3_rl              RL.exit_timer3
#define exit_timer4_rl              RL.exit_timer4
#define exit_timer5_rl              RL.exit_timer5
#define hold_timer_rl               RL.hold_timer
#define count7_rl                   RL.count7
#define count14_rl                  RL.count14
#define count20_rl                  RL.count20
#define refdiff_rl                  RL.refdiff
#define refdiff_alt1_rl             RL.refdiff_alt1
#define refdiff_alt2_rl             RL.refdiff_alt2
#define av_ref_max_rl               RL.av_ref_max
#define av_ref_rl                   RL.av_ref
#define av_ref_alt_rl               RL.av_ref_alt
#define btc_count3_rl               RL.btc_count3
#define btc_count4_rl               RL.btc_count4
#define slip_count4_rl              RL.slip_count4
#define valve_delay_count_rl              RL.valve_delay_count
#define barad_max_rl                RL.barad_max
#define barad_alt_rl                RL.barad_alt
#define TCS_built_reapply_pattern_rl   RL.TCS_built_reapply_pattern
#define TCS_Initial_Duty_rl   RL.TCS_Initial_Duty
#define TCS_Duty_Ratio_decrease_rl   RL.TCS_Duty_Ratio_decrease
#define TCS_built_reapply_time_rl    RL.TCS_built_reapply_time

/* tmp */
#if  __BTEM == DISABLE
#define btc_tmp_rl                  RL.btc_tmp
#define tmprad_alt_rl               RL.tmprad_alt
#define btc_tmp_alt_rl              RL.btc_tmp_alt
#define tmprad_max_rl               RL.tmprad_max
#define cool_tmp_rl                 RL.cool_tmp
#define tmp_acc_rl                  RL.tmp_acc
#define acc_perc_rl                 RL.acc_perc
#define out_time_rl                 RL.out_time
#define tmp_count_rl                RL.tmp_count
#define max_spin_rl                 RL.max_spin
#define tmp_time_rl                 RL.tmp_time
#define tmp_down_rl                 RL.tmp_down
#define tmp_up_rl                   RL.tmp_up
#define inc_count1_rl               RL.inc_count1
#define inc_count2_rl               RL.inc_count2
#define cal_tmp_rl                  RL.cal_tmp
#endif

#define inc_count3_rl               RL.inc_count3
#define tmp_state_rl                RL.tmp_state
#define BTCS_flags_rl				RL.BTCS_flags

#if	__TCS
#define mini_count_rl				RL.mini_count
#endif

#define state_rr                    RR.state
#define zyklus_z_rr                 RR.zyklus_z
#define diff_slip_rr                RR.diff_slip
#define s0_afz_rr                   RR.s0_afz
#if	__CREEP_VIBRATION
#define v_ungef_alt_rr              RR.v_ungef_alt
#endif
#define vrad_rr                     RR.vrad
#define vrad_crt_rr                 RR.vrad_crt
#define vdiff_rr                    RR.vdiff
#define speed_rr                    RR.speed
#define speed_alt_rr                RR.speed_alt
#define speed_filtered_rr           RR.speed_filtered
#define speed_filtered_alt_rr       RR.speed_filtered_alt
#define s_diff_rr                   RR.s_diff
//#define s_diff_alt_rr               RR.s_diff_alt
//#define s_diff_kph_rr               RR.s_diff_kph
//#define s_diff_alt_kph_rr           RR.s_diff_alt_kph
#define s0_afz1_rr                  RR.s0_afz1
#define spold_rr                    RR.spold
#define vfilt_rough_rr              RR.vfilt_rough
#define vfiltn_rr                   RR.vfiltn
#define avr_arad_rr                 RR.avr_arad
#define avr_arad_alt_rr             RR.avr_arad_alt
#define arad_rr                     RR.arad
#define arad_alt_rr                 RR.arad_alt
#define peak_acc_rr                 RR.peak_acc
#define peak_dec_rr                 RR.peak_dec
#define max_arad_instbl_rr          RR.max_arad_instbl
#define rel_lam_rr                  RR.rel_lam
#define peak_slip_rr                RR.peak_slip
#define reapply_tim_rr              RR.reapply_tim
#define castim_rr                   RR.castim
#define thres_decel_rr              RR.thres_decel
#define natrl_frq_counter_rr        RR.natrl_frq_counter
#define hold_timer_new_rr           RR.hold_timer_new
//#define continue_dump_timer_rr           RR.continue_dump_timer
//#define timer_new_rr                RR.timer_new
#define av_t_aqua_rr                RR.av_t_aqua
#define ab_z_pr_rr                  RR.ab_z_pr
#define ab_zaehl_rr                 RR.ab_zaehl
#define slew_rate_timer_rr          RR.slew_rate_timer
#define flags_rr                    RR.flags
#define pulse_up_row_timer_rr       RR.pulse_up_row_timer
//#define s0_reapply_counter_rr       RR.s0_reapply_counter
#define ab_auf_counter_rr           RR.ab_auf_counter
#define time_aft_thrdec_rr          RR.time_aft_thrdec
#define allow_dump_rr               RR.allow_dump
//#define increase_p_time_rr          RR.increase_p_time
#define peak_acc_alt_rr             RR.peak_acc_alt
#define peak_dec_alt_rr             RR.peak_dec_alt
#define peak_dec_max_rr             RR.peak_dec_max
#define holding_cycle_timer_rr      RR.holding_cycle_timer
//#define roughold_rr                 RR.roughold
#define gma_dump_rr                 RR.gma_dump
#define gma_counter_rr              RR.gma_counter
#define slip_at_real_dump_rr        RR.slip_at_real_dump
//#define p_hold_time_rr              RR.p_hold_time
//#define rel_lam_alt_rr              RR.rel_lam_alt
//#define holding_time_rr             RR.holding_time
#define s0_reapply_alt_count_rr     RR.s0_reapply_alt_count
#define vib_counter_rr              RR.vib_counter
#define depress_time_rr             RR.depress_time
//#define arad_at_dump_rr             RR.arad_at_dump
#define p_shooting_time_rr          RR.p_shooting_time
#define p_row_change_timer_rr       RR.p_row_change_timer
#define spold_rst_count_rr          RR.spold_rst_count
//#define vfilt_timer_rr              RR.vfilt_timer
#define frcnt_rr                    RR.frcnt
#define rough_hold_time_rr          RR.rough_hold_time
//#define force_dump_counter_rr       RR.force_dump_counter
#define peak_acc_afz_rr             RR.peak_acc_afz
#define vfilt_comp_rr               RR.vfilt_comp
//#define mini_whl_counter_rr         RR.mini_whl_counter
#define mu_jump_p_counter_rr        RR.mu_jump_p_counter
#define ebd_dump_time_rr            RR.ebd_dump_time
#define on_time_inlet_alt_rr        RR.on_time_inlet_alt
#define on_time_inlet_rr            RR.on_time_inlet
#define on_time_outlet_rr           RR.on_time_outlet
#define on_time_special_rr          RR.on_time_special
#define msl_hold_time_rr            RR.msl_hold_time
//#define msl_gma_counter_rr          RR.msl_gma_counter
#define gma_dump1_rr                RR.gma_dump1
//#define msl_reapply_time_rr         RR.msl_reapply_time
#define det_count_rr                RR.det_count
//#define EBD_Dump_timer_rr           RR.EBD_Dump_timer
#define ebd_peak_dec_rr             RR.ebd_peak_dec

				                 //020927 제동거리
#define High_dump_factor_rr         RR.High_dump_factor
//#define High_dump_factor2_rr         RR.High_dump_factor2
#define Dump_hold_scan_ref_rr       RR.Dump_hold_scan_ref
#define Dump_hold_scan_counter_rr   RR.Dump_hold_scan_counter

#if __EDC
#define   ldedcs16DeltaVelAtMinusB_1G5_rr	RR.ldedcs16DeltaVelAtMinusB_1G5
#define   ldedcs16DeltaVelAtMinusB_0G5_rr	RR.ldedcs16DeltaVelAtMinusB_0G5
#define   ldedcs16DeltaVelocity_rr		RR.ldedcs16DeltaVelocity
#define   lcedcs16WheelExitCount_rr    	RR.lcedcs16WheelExitCount
#define   lcedcs16WheelSlip_rr         	RR.lcedcs16WheelSlip
#define   ldedcu8ParkBrkSuspectCounter_rr      	RR.ldedcu8ParkBrkSuspectCounter
#define   ldedcu16EngRpmAtDecelCheck_rr      	RR.ldedcu16EngRpmAtDecelCheck
#define   ldedcu8ParkDecelCheckCnt_rr      	RR.ldedcu8ParkDecelCheckCnt
#endif

#if __VDC
#define wvref_rr                    RR.wvref

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//#define esp_control_mode_rr         RR.esp_control_mode
//#define esp_control_mode_old_rr     RR.esp_control_mode_old
#else
#define esp_control_mode_rr         RR.esp_control_mode
#define esp_control_mode_old_rr     RR.esp_control_mode_old
#endif

#define esp_final_mode_rr       	RR.esp_final_mode

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//#define esp_control_cycle_rr       	RR.esp_control_cycle
#else
#define esp_control_cycle_rr       	RR.esp_control_cycle
#endif

#endif

//  #define press_test_reapply_rr     RR.press_test_reapply
//  #define press_test_hold_rr        RR.press_test_hold

/* btcs */
#define b_rad_rr                    RR.b_rad
#define vrad_alt_rr                 RR.vrad_alt
#define average7_rr                 RR.average7
#define btc_slip_rr                 RR.btc_slip
#define slip_min_rr                 RR.slip_min
#define btc_speed_rr                RR.btc_speed
#define btc_speed_alt_rr            RR.btc_speed_alt
#define ffspeed_rr                  RR.ffspeed
#define fspeed_rr                   RR.fspeed
#define fspeed_alt_rr               RR.fspeed_alt
#define av_fspeed_alt_rr            RR.av_fspeed_alt
#define av_fspeed_rr                RR.av_fspeed
#define av_btc_slip_rr              RR.av_btc_slip
#define av_btc_slip_alt_rr          RR.av_btc_slip_alt
#define hold_count_rr               RR.hold_count
#define hold_count_both_spin_rr     RR.hold_count_both_spin
#define btcinc_count_rr             RR.btcinc_count
#define btcdec_count_rr             RR.btcdec_count
#define btc_count1_rr               RR.btc_count1
#define btc_count2_rr               RR.btc_count2
#define vib_count1_rr               RR.vib_count1
#define vib_count2_rr               RR.vib_count2
#define vib_count3_rr               RR.vib_count3
#define vib_count4_rr               RR.vib_count4
#define slip_count1_rr              RR.slip_count1
#define slip_count2_rr              RR.slip_count2
#define slip_count3_rr              RR.slip_count3
#define exit_timer1_rr              RR.exit_timer1
#define exit_timer2_rr              RR.exit_timer2
#define exit_timer3_rr              RR.exit_timer3
#define exit_timer4_rr              RR.exit_timer4
#define exit_timer5_rr              RR.exit_timer5
#define hold_timer_rr               RR.hold_timer
#define count7_rr                   RR.count7
#define count14_rr                  RR.count14
#define count20_rr                  RR.count20
#define refdiff_rr                  RR.refdiff
#define refdiff_alt1_rr             RR.refdiff_alt1
#define refdiff_alt2_rr             RR.refdiff_alt2
#define av_ref_max_rr               RR.av_ref_max
#define av_ref_rr                   RR.av_ref
#define av_ref_alt_rr               RR.av_ref_alt
#define btc_count3_rr               RR.btc_count3
#define btc_count4_rr               RR.btc_count4
#define slip_count4_rr              RR.slip_count4
#define valve_delay_count_rr              RR.valve_delay_count
#define barad_max_rr                RR.barad_max
#define barad_alt_rr                RR.barad_alt
#define TCS_built_reapply_pattern_rr  RR.TCS_built_reapply_pattern
#define TCS_Initial_Duty_rr         RR.TCS_Initial_Duty
#define TCS_Duty_Ratio_decrease_rr  RR.TCS_Duty_Ratio_decrease
#define TCS_built_reapply_time_rr   RR.TCS_built_reapply_time

/* tmp */
#if  __BTEM == DISABLE
#define btc_tmp_rr                  RR.btc_tmp
#define tmprad_alt_rr               RR.tmprad_alt
#define btc_tmp_alt_rr              RR.btc_tmp_alt
#define tmprad_max_rr               RR.tmprad_max
#define cool_tmp_rr                 RR.cool_tmp
#define tmp_acc_rr                  RR.tmp_acc
#define acc_perc_rr                 RR.acc_perc
#define out_time_rr                 RR.out_time

#define tmp_count_rr                RR.tmp_count
#define max_spin_rr                 RR.max_spin
#define tmp_time_rr                 RR.tmp_time
#define tmp_down_rr                 RR.tmp_down
#define tmp_up_rr                   RR.tmp_up
#define inc_count1_rr               RR.inc_count1
#define inc_count2_rr               RR.inc_count2
#define cal_tmp_rr                  RR.cal_tmp
#endif

#define inc_count3_rr               RR.inc_count3
#define tmp_state_rr                RR.tmp_state
#define BTCS_flags_rr				RR.BTCS_flags
//050929 New BTCS Structure
#define spin_fl						FL.spin
#define spin_fr						FR.spin
#define spin_rl						RL.spin
#define spin_rr						RR.spin

#define ltcss16BrkControlIndex_fl						FL.ltcss16BrkControlIndex
#define ltcss16BrkControlIndex_fr						FR.ltcss16BrkControlIndex
#define ltcss16BrkControlIndex_rl						RL.ltcss16BrkControlIndex
#define ltcss16BrkControlIndex_rr						RR.ltcss16BrkControlIndex

#define btcs_rise_scan_fl				FL.btcs_rise_scan
#define btcs_rise_scan_fr				FR.btcs_rise_scan
#define btcs_rise_scan_rl				RL.btcs_rise_scan
#define btcs_rise_scan_rr				RR.btcs_rise_scan

#define btcs_dump_scan_fl				FL.btcs_dump_scan
#define btcs_dump_scan_fr				FR.btcs_dump_scan
#define btcs_dump_scan_rl				RL.btcs_dump_scan
#define btcs_dump_scan_rr				RR.btcs_dump_scan

#define btcs_hold_scan_fl				FL.btcs_hold_scan
#define btcs_hold_scan_fr				FR.btcs_hold_scan
#define btcs_hold_scan_rl				RL.btcs_hold_scan
#define btcs_hold_scan_rr				RR.btcs_hold_scan

#define btcs_rise_scan_disp_fl				FL.btcs_rise_scan_disp
#define btcs_rise_scan_disp_fr				FR.btcs_rise_scan_disp
#define btcs_rise_scan_disp_rl				RL.btcs_rise_scan_disp
#define btcs_rise_scan_disp_rr				RR.btcs_rise_scan_disp

#define btcs_dump_scan_disp_fl				FL.btcs_dump_scan_disp
#define btcs_dump_scan_disp_fr				FR.btcs_dump_scan_disp
#define btcs_dump_scan_disp_rl				RL.btcs_dump_scan_disp
#define btcs_dump_scan_disp_rr				RR.btcs_dump_scan_disp

#define btcs_hold_scan_disp_fl				FL.btcs_hold_scan_disp
#define btcs_hold_scan_disp_fr				FR.btcs_hold_scan_disp
#define btcs_hold_scan_disp_rl				RL.btcs_hold_scan_disp
#define btcs_hold_scan_disp_rr				RR.btcs_hold_scan_disp

#define target_spin_fl				FL.target_spin
#define target_spin_fr				FR.target_spin
#define target_spin_rl				RL.target_spin
#define target_spin_rr				RR.target_spin

#define rise_threshold_fl			FL.rise_threshold
#define rise_threshold_fr			FR.rise_threshold
#define rise_threshold_rl			RL.rise_threshold
#define rise_threshold_rr			RR.rise_threshold

#define dump_threshold_fl			FL.dump_threshold
#define dump_threshold_fr			FR.dump_threshold
#define dump_threshold_rl			RL.dump_threshold
#define dump_threshold_rr			RR.dump_threshold

#define btcs_pressure_inc_scan_cnt_fl	FL.btcs_pressure_inc_scan_cnt
#define btcs_pressure_inc_scan_cnt_fr	FR.btcs_pressure_inc_scan_cnt
#define btcs_pressure_inc_scan_cnt_rl	RL.btcs_pressure_inc_scan_cnt
#define btcs_pressure_inc_scan_cnt_rr	RR.btcs_pressure_inc_scan_cnt

#define btcs_pressure_dump_scan_cnt_fl	FL.btcs_pressure_dump_scan_cnt
#define btcs_pressure_dump_scan_cnt_fr	FR.btcs_pressure_dump_scan_cnt
#define btcs_pressure_dump_scan_cnt_rl	RL.btcs_pressure_dump_scan_cnt
#define btcs_pressure_dump_scan_cnt_rr	RR.btcs_pressure_dump_scan_cnt

#define	ltcsu8WhlSpdDecCnt_fl			FL.ltcsu8WhlSpdDecCnt
#define	ltcsu8WhlSpdDecCnt_fr			FR.ltcsu8WhlSpdDecCnt
#define	ltcsu8WhlSpdDecCnt_rl			RL.ltcsu8WhlSpdDecCnt
#define	ltcsu8WhlSpdDecCnt_rr			RR.ltcsu8WhlSpdDecCnt

#define state1_running_cnt_fl			FL.state1_running_cnt
#define state1_running_cnt_fr			FR.state1_running_cnt
#define state1_running_cnt_rl			RL.state1_running_cnt
#define state1_running_cnt_rr			RR.state1_running_cnt

#define state2_running_cnt_fl			FL.state2_running_cnt
#define state2_running_cnt_fr			FR.state2_running_cnt
#define state2_running_cnt_rl			RL.state2_running_cnt
#define state2_running_cnt_rr			RR.state2_running_cnt

#define ltcss16LowSpinCnt_fl	FL.ltcss16LowSpinCnt
#define ltcss16LowSpinCnt_fr	FR.ltcss16LowSpinCnt
#define ltcss16LowSpinCnt_rl	RL.ltcss16LowSpinCnt
#define ltcss16LowSpinCnt_rr	RR.ltcss16LowSpinCnt

#define ltcss8Cnt4PlusBDetectClear_fl 	FL.ltcss8Cnt4PlusBDetectClear
#define ltcss8Cnt4PlusBDetectClear_fr	FR.ltcss8Cnt4PlusBDetectClear
#define ltcss8Cnt4PlusBDetectClear_rl	RL.ltcss8Cnt4PlusBDetectClear
#define ltcss8Cnt4PlusBDetectClear_rr	RR.ltcss8Cnt4PlusBDetectClear

#define	ltcss8BTCSWheelCtlStatus_fl	FL.ltcss8BTCSWheelCtlStatus
#define	ltcss8BTCSWheelCtlStatus_fr	FR.ltcss8BTCSWheelCtlStatus
#define	ltcss8BTCSWheelCtlStatus_rl	RL.ltcss8BTCSWheelCtlStatus
#define	ltcss8BTCSWheelCtlStatus_rr	RR.ltcss8BTCSWheelCtlStatus

#define	ltcss8BTCSWheelCtlStatusOld_fl	FL.ltcss8BTCSWheelCtlStatusOld
#define	ltcss8BTCSWheelCtlStatusOld_fr	FR.ltcss8BTCSWheelCtlStatusOld
#define	ltcss8BTCSWheelCtlStatusOld_rl	RL.ltcss8BTCSWheelCtlStatusOld
#define	ltcss8BTCSWheelCtlStatusOld_rr	RR.ltcss8BTCSWheelCtlStatusOld

#define	ltcss8BTCSCtlModeChange_fl	FL.ltcss8BTCSCtlModeChange
#define	ltcss8BTCSCtlModeChange_fr	FR.ltcss8BTCSCtlModeChange
#define	ltcss8BTCSCtlModeChange_rl	RL.ltcss8BTCSCtlModeChange
#define	ltcss8BTCSCtlModeChange_rr	RR.ltcss8BTCSCtlModeChange

#define ltcss16SumOfMovingWindow_fl	FL.ltcss16SumOfMovingWindow
#define ltcss16SumOfMovingWindow_fr	FR.ltcss16SumOfMovingWindow
#define ltcss16SumOfMovingWindow_rl	RL.ltcss16SumOfMovingWindow
#define ltcss16SumOfMovingWindow_rr	RR.ltcss16SumOfMovingWindow

#define ltcss16MovingAveragedWhlSpd_fl	FL.ltcss16MovingAveragedWhlSpd
#define ltcss16MovingAveragedWhlSpd_fr	FR.ltcss16MovingAveragedWhlSpd
#define ltcss16MovingAveragedWhlSpd_rl	RL.ltcss16MovingAveragedWhlSpd
#define ltcss16MovingAveragedWhlSpd_rr	RR.ltcss16MovingAveragedWhlSpd

#define ltcss16MovingAveragedWhlSpdOld_fl	FL.ltcss16MovingAveragedWhlSpdOld
#define ltcss16MovingAveragedWhlSpdOld_fr	FR.ltcss16MovingAveragedWhlSpdOld
#define ltcss16MovingAveragedWhlSpdOld_rl	RL.ltcss16MovingAveragedWhlSpdOld
#define ltcss16MovingAveragedWhlSpdOld_rr	RR.ltcss16MovingAveragedWhlSpdOld

#define ltcss16MovingAveragedWhlSpdDiff_fl	FL.ltcss16MovingAveragedWhlSpdDiff
#define ltcss16MovingAveragedWhlSpdDiff_fr	FR.ltcss16MovingAveragedWhlSpdDiff
#define ltcss16MovingAveragedWhlSpdDiff_rl	RL.ltcss16MovingAveragedWhlSpdDiff
#define ltcss16MovingAveragedWhlSpdDiff_rr	RR.ltcss16MovingAveragedWhlSpdDiff

#define ltcss16Index4MovingAvrg_fl	FL.ltcss16Index4MovingAvrg
#define ltcss16Index4MovingAvrg_fr	FR.ltcss16Index4MovingAvrg
#define ltcss16Index4MovingAvrg_rl	RL.ltcss16Index4MovingAvrg
#define ltcss16Index4MovingAvrg_rr	RR.ltcss16Index4MovingAvrg

#define ltcsu8ESVCloseDelayCnt_fl	FL.ltcsu8ESVCloseDelayCnt
#define ltcsu8ESVCloseDelayCnt_fr	FR.ltcsu8ESVCloseDelayCnt
#define ltcsu8ESVCloseDelayCnt_rl	RL.ltcsu8ESVCloseDelayCnt
#define ltcsu8ESVCloseDelayCnt_rr	RR.ltcsu8ESVCloseDelayCnt

#define ltcsu8BTCSCtlModeFixCnt_fl	FL.ltcsu8BTCSCtlModeFixCnt
#define ltcsu8BTCSCtlModeFixCnt_fr  FR.ltcsu8BTCSCtlModeFixCnt
#define ltcsu8BTCSCtlModeFixCnt_rl  RL.ltcsu8BTCSCtlModeFixCnt
#define ltcsu8BTCSCtlModeFixCnt_rr  RR.ltcsu8BTCSCtlModeFixCnt

#define ltcsu8AllowableRiseScan_fl	FL.ltcsu8AllowableRiseScan
#define ltcsu8AllowableRiseScan_fr	FR.ltcsu8AllowableRiseScan
#define ltcsu8AllowableRiseScan_rl	RL.ltcsu8AllowableRiseScan
#define ltcsu8AllowableRiseScan_rr	RR.ltcsu8AllowableRiseScan

#define lctcss8InsideWheelCnt_fl	FL.lctcss8InsideWheelCnt
#define lctcss8InsideWheelCnt_fr	FR.lctcss8InsideWheelCnt
#define lctcss8InsideWheelCnt_rl	RL.lctcss8InsideWheelCnt
#define lctcss8InsideWheelCnt_rr	RR.lctcss8InsideWheelCnt

#define lctcsu8InsideWheelBrkState_fl	FL.lctcsu8InsideWheelBrkState
#define lctcsu8InsideWheelBrkState_fr	FR.lctcsu8InsideWheelBrkState
#define lctcsu8InsideWheelBrkState_rl	RL.lctcsu8InsideWheelBrkState
#define lctcsu8InsideWheelBrkState_rr	RR.lctcsu8InsideWheelBrkState

#define lctcsu8InsideWheelBrkStateod_fl	FL.lctcsu8InsideWheelBrkStateod
#define lctcsu8InsideWheelBrkStateod_fr	FR.lctcsu8InsideWheelBrkStateod
#define lctcsu8InsideWheelBrkStateod_rl	RL.lctcsu8InsideWheelBrkStateod
#define lctcsu8InsideWheelBrkStateod_rr	RR.lctcsu8InsideWheelBrkStateod

#define lctcsu8InsideWheelBrkExitCnt_fl	FL.lctcsu8InsideWheelBrkExitCnt
#define lctcsu8InsideWheelBrkExitCnt_fr	FR.lctcsu8InsideWheelBrkExitCnt
#define lctcsu8InsideWheelBrkExitCnt_rl	RL.lctcsu8InsideWheelBrkExitCnt
#define lctcsu8InsideWheelBrkExitCnt_rr	RR.lctcsu8InsideWheelBrkExitCnt

#define lctcsu8InsideWheelESCnt_fl FL.lctcsu8InsideWheelESCnt
#define lctcsu8InsideWheelESCnt_fr FR.lctcsu8InsideWheelESCnt
#define lctcsu8InsideWheelESCnt_rl RL.lctcsu8InsideWheelESCnt
#define lctcsu8InsideWheelESCnt_rr RR.lctcsu8InsideWheelESCnt

#define ltcss16InsideWheelLowSpinCnt_fl FL.ltcss16InsideWheelLowSpinCnt
#define ltcss16InsideWheelLowSpinCnt_fr FR.ltcss16InsideWheelLowSpinCnt
#define ltcss16InsideWheelLowSpinCnt_rl RL.ltcss16InsideWheelLowSpinCnt
#define ltcss16InsideWheelLowSpinCnt_rr RR.ltcss16InsideWheelLowSpinCnt

#if __TCS_RISE_TH_ADJUST
#define ltcss8SpinSlopeCnt_fl FL.ltcss8SpinSlopeCnt
#define ltcss8SpinSlopeCnt_fr FR.ltcss8SpinSlopeCnt
#define ltcss8SpinSlopeCnt_rl RL.ltcss8SpinSlopeCnt
#define ltcss8SpinSlopeCnt_rr RR.ltcss8SpinSlopeCnt

#define btcs_pressure_hold_scan_cnt_fl	FL.btcs_pressure_hold_scan_cnt
#define btcs_pressure_hold_scan_cnt_fr	FR.btcs_pressure_hold_scan_cnt
#define btcs_pressure_hold_scan_cnt_rl	RL.btcs_pressure_hold_scan_cnt
#define btcs_pressure_hold_scan_cnt_rr	RR.btcs_pressure_hold_scan_cnt
#endif	/* #if __TCS_RISE_TH_ADJUST */

#if ((__VDC) &&(ESC_PULSE_UP_RISE))
//////////////////////////////////////////////////////////

/*     Pulse-up cycle      */
#define pulseup_cycle_scan_time_fl	FL.pulseup_cycle_scan_time		// juho
#define pulseup_cycle_scan_time_fr	FR.pulseup_cycle_scan_time		// juho
#define pulseup_cycle_scan_time_rl	RL.pulseup_cycle_scan_time		// juho
#define pulseup_cycle_scan_time_rr	RR.pulseup_cycle_scan_time		// juho
#endif
#if	__TCS
#define mini_count_rr				RR.mini_count
#endif

#define AV_VL_wl                    WL->AV_VL
#define HV_VL_wl                    WL->HV_VL
#define BUILT_wl                    WL->BUILT
#define ABS_wl                      WL->ABS
#define FSF_wl                      WL->FSF
#define STABIL_wl                   WL->STABIL
#define STAB_K_wl                   WL->STAB_K
#define STAB_A_wl                   WL->STAB_A

#define AV_HOLD_wl                  WL->AV_HOLD
#define AV_DUMP_wl                  WL->AV_DUMP
#define AV_REAPPLY_wl               WL->AV_REAPPLY
#define HEAVY_FILT_wl               WL->HEAVY_FILT
#define INCREASE_VDIFF_wl           WL->INCREASE_VDIFF
#define V_STOER_wl                  WL->V_STOER
#define REAR_WHEEL_wl               WL->REAR_WHEEL
#define LEFT_WHEEL_wl               WL->LEFT_WHEEL

#define SPEEDMIN_wl                 WL->SPEEDMIN
#define ALG_INHIBIT_wl              WL->ALG_INHIBIT
#define RESET_PEAKCEL_wl            WL->RESET_PEAKCEL
#define CAS_wl                      WL->CAS
#define SL_HOLD_wl                  WL->SL_HOLD
#define SL_DUMP_wl                  WL->SL_DUMP
#define MINUS_B_SET_AGAIN_OLD_wl    WL->MINUS_B_SET_AGAIN_OLD   // J.K.Lee at 030926
#define SL_BASE_wl                  WL->SL_BASE
#define HIGH_MU_INDICATION_wl       WL->HIGH_MU_INDICATION

#define DUMP_END_1_wl               WL->DUMP_END_1
#define DUMP_END_2_wl               WL->DUMP_END_2
#define ADD_DUMP_wl                 WL->ADD_DUMP
#define GMA_wl                      WL->GMA
#define GMA_SLIP_wl                 WL->GMA_SLIP
#define UN_GMA_wl                   WL->UN_GMA
#define CLR_HOLD_TIMER_wl           WL->CLR_HOLD_TIMER
#define EBD_INCREASE_wl             WL->EBD_INCREASE

#define SLIP_2P_NEG_wl              WL->SLIP_2P_NEG
#define SLIP_5P_NEG_wl              WL->SLIP_5P_NEG
#define SLIP_10P_NEG_wl             WL->SLIP_10P_NEG
#define SLIP_20P_NEG_wl             WL->SLIP_20P_NEG
#define SLIP_30P_NEG_wl             WL->SLIP_30P_NEG
#define SCHNELLSTE_VREF_wl          WL->SCHNELLSTE_VREF
#define LO_HI_JUMP_wl               WL->LO_HI_JUMP
#define JUMP_DOWN_wl                WL->JUMP_DOWN

#define PEAK_ACC_PASS_wl            WL->PEAK_ACC_PASS
#define CHECK_PEAK_ACCEL_wl         WL->CHECK_PEAK_ACCEL
#define FORCE_DUMP_wl               WL->FORCE_DUMP
#define ADAPTIVE_THRESHOLD_wl       WL->ADAPTIVE_THRESHOLD
#define SPOLD_RESET_wl              WL->SPOLD_RESET
#define WAIT_FCYC_DUMP_wl           WL->WAIT_FCYC_DUMP
#define FC_DUMP_wl                  WL->FC_DUMP
#define MSL_BASE_wl                 WL->MSL_BASE

#define ARAD_AT_MINUS_B_wl          WL->ARAD_AT_MINUS_B
#define MINUS_B_SET_AGAIN_wl        WL->MINUS_B_SET_AGAIN
#define LAM_1P_MINUS_wl             WL->LAM_1P_MINUS
#define LAM_2P_MINUS_wl             WL->LAM_2P_MINUS
#define LAM_3P_MINUS_wl             WL->LAM_3P_MINUS
#define LAM_4P_MINUS_wl             WL->LAM_4P_MINUS
#define LAM_5P_MINUS_wl             WL->LAM_5P_MINUS
#define SET_DUMP_wl                 WL->SET_DUMP

#define RES_FRQ_wl                  WL->RES_FRQ
#define HOLD_RES_wl                 WL->HOLD_RES
#define ARAD_3G_POS_wl              WL->ARAD_3G_POS
#define ARAD_NEG_wl                 WL->ARAD_NEG
#define ARAD_POS_wl                 WL->ARAD_POS
#define HIGH_VFILT_wl               WL->HIGH_VFILT
#define MSL_DUMP_wl                 WL->MSL_DUMP
//#define MSL_SET_wl                  WL->MSL_SET

#define LOW_TO_HIGH_wl              WL->LOW_TO_HIGH
#define PREV_LO_HI_wl               WL->PREV_LO_HI
#define CRNT_LO_HI_wl               WL->CRNT_LO_HI
#define DECT_LO_HI_wl               WL->DECT_LO_HI
#define LOW_TO_HIGH_PULSEUP_wl      WL->LOW_TO_HIGH_PULSEUP
#define WHEEL_AT_HIGH_MU_wl         WL->WHEEL_AT_HIGH_MU
#define flag71_wl                   WL->flag71
#define flag72_wl                   WL->flag72

#define PULSE_ROW_CHANGE_wl         WL->PULSE_ROW_CHANGE
#define BEND_PULSE_wl               WL->BEND_PULSE
#define TWO_PULSE_wl                WL->TWO_PULSE
#define RLS_DEPRESS_wl              WL->RLS_DEPRESS
#define INIT_AFZ_wl                 WL->INIT_AFZ
#define MINI_SPARE_WHEEL_wl         WL->MINI_SPARE_WHEEL
#define MOT_STOP_wl                 WL->MOT_STOP
#define SPLIT_SET_wl                WL->SPLIT_SET

#define BOTH_GMA_SET_wl             WL->BOTH_GMA_SET
#define SPLIT_JUMP_wl               WL->SPLIT_JUMP
#define SPLIT_PULSE_wl              WL->SPLIT_PULSE
//#define SPLIT_FSF_wl                WL->SPLIT_FSF
#define SPLIT_CLR_wl                WL->SPLIT_CLR
#define SET_DUMP2_wl                WL->SET_DUMP2
#define SPLIT_SET2_wl               WL->SPLIT_SET2
#define ABS_LIMIT_SPEED_wl                 WL->ABS_LIMIT_SPEED

#define EBD_wl                      WL->EBD
#define ENT_ABS_wl                  WL->ENT_ABS
#define dump_flg_wl                 WL->dump_flg
#define L_SLIP_CONTROL_wl           WL->L_SLIP_CONTROL
#define L_SLIP_CONTROL_OLD_wl       WL->L_SLIP_CONTROL_OLD
#define l_SLIP_K_wl                 WL->l_SLIP_K
#define l_SLIP_A_wl                 WL->l_SLIP_A

//#define SAFETY_WARN_wl              WL->SAFETY_WARN
//#define TREND_WARN_wl               WL->TREND_WARN
//#define L_OPTIMUM_DETECT_wl         WL->L_OPTIMUM_DETECT
//#define L_DUMP_wl                   WL->L_DUMP
//#define L_DUMP_OLD_wl               WL->L_DUMP_OLD
//#define L_DUMP_K_wl                 WL->L_DUMP_K
//#define L_DUMP_A_wl                 WL->L_DUMP_A
//#define L_1ST_D_H_wl                WL->L_1ST_D_H

#define POS_HOLD_wl                 WL->POS_HOLD
#define COUNT20_wl                  WL->COUNT20
#define POS_COUNT_wl                WL->POS_COUNT
#define BTCS_wl                     WL->BTCS
#define DETECT_BTC_wl               WL->DETECT_BTC
#define TCL_DEMAND_wl               WL->TCL_DEMAND
#define OPT_NEWSET_wl               WL->OPT_NEWSET
#define FIRST_INC_wl                WL->FIRST_INC

#define ARAD_RISE_wl                WL->ARAD_RISE
#define EXIT_BTCS_CONTROL_wl                WL->EXIT_BTCS_CONTROL
#define TCL_INC_wl                  WL->TCL_INC
#define TCL_INCF_wl                 WL->TCL_INCF
#define LOW_TO_HIGH1_wl             WL->LOW_TO_HIGH1
#define TCL_INCF2_wl                WL->TCL_INCF2
#define ARAD_POSITIVE_wl            WL->ARAD_POSITIVE
#define VRAD_DEC_wl                 WL->VRAD_DEC

#define HCONT_DEC_wl                WL->HCONT_DEC
#define ARAD_LOW_wl                 WL->ARAD_LOW
#define OPT_SET_HIGH_wl             WL->OPT_SET_HIGH
#define HALF_PULS_wl                WL->HALF_PULS
#define TMP_ERROR_wl                WL->TMP_ERROR
#define HILL_wl                     WL->HILL
#define TMP_START_wl                WL->TMP_START
#define TMP_RISE_wl                 WL->TMP_RISE

#define XMT_VIB_wl                  WL->XMT_VIB
#define BTC_LOW_SLIP_wl             WL->BTC_LOW_SLIP
#define BTC_LOW_SLIP_EXIT_wl        WL->BTC_LOW_SLIP_EXIT
#define VIB_SET_wl                  WL->VIB_SET
#define DETVIB_BTC_wl               WL->DETVIB_BTC
#define CIRCLE_wl                   WL->CIRCLE
#define VIB_COUNT_wl                WL->VIB_COUNT
#define TWO_BUILT_HI_wl             WL->TWO_BUILT_HI
#define TCS_VALVE_EXIT_wl                  WL->TCS_VALVE_EXIT

#define TEMP_HV_VL_wl               WL->TEMP_HV_VL
#define TEMP_AV_VL_wl               WL->TEMP_AV_VL
#define TEMP_TCL_DEMAND_wl          WL->TEMP_TCL_DEMAND
//#define VDC_ABS_WORK_wl             WL->VDC_ABS_WORK
//#define FIRST_VREF_SELECT_wl        WL->FIRST_VREF_SELECT
//#define F_A_R_P_wl                  WL->F_A_R_P
//#define F_A_D_P_wl                  WL->F_A_D_P
//#define F_A_H_P_wl                  WL->F_A_H_P

//#define NOT_ZERO_VREF_wl            WL->NOT_ZERO_VREF
//#define CAL_TVREF_wl                WL->CAL_TVREF
//#define TEST_FLAG_wl                WL->TEST_FLAG
//#define TEST_FLAG2_wl               WL->TEST_FLAG2
//#define ESP_wl                      WL->ESP

// LFC + VDC
//#define L_ABS_ESP_COMB_wl           WL->L_ABS_ESP_COMB
//#define L_DEL_P_CAL_OK_wl           WL->L_DEL_P_CAL_OK
//#define LFC_MODE_K_wl               WL->LFC_MODE_K
//#define LFC_MODE_A_wl               WL->LFC_MODE_A
//#define LFC_WITHOUT_ESP_wl          WL->LFC_WITHOUT_ESP
//#define LFC_WITH_ESP_wl             WL->LFC_WITH_ESP
#define ESP_ABS_SLIP_CONTROL_wl     WL->ESP_ABS_SLIP_CONTROL
#define CROSS_SLIP_CONTROL_wl       WL->CROSS_SLIP_CONTROL
#define CROSS_SLIP_CONTROL_old_wl	WL->CROSS_SLIP_CONTROL_old   
#define L_flag160_wl                WL->L_flag160
// PWM, LFC

#if __EDC
#define	ldedcu1EdcDecelDetect_wl			WL->ldedcu1EdcDecelDetect
#define	ldedcu1EdcDecelDetect_1G5_wl			WL->ldedcu1EdcDecelDetect_1G5
#define ldedcu1EdcNorOn_wl				WL->ldedcu1EdcNorOn
#define lcedcu1EdcWheelExit_wl			WL->lcedcu1EdcWheelExit
#define ldedcu1ParkBrkSuspectWL_wl			WL->ldedcu1ParkBrkSuspectWL
#define ldedcu1ParkDecelCheckWL_wl			WL->ldedcu1ParkDecelCheckWL
#endif

//FL
#define AV_VL_fl                    FL.AV_VL
#define HV_VL_fl                    FL.HV_VL
#define BUILT_fl                    FL.BUILT
#define ABS_fl                      FL.ABS
#define FSF_fl                      FL.FSF
#define STABIL_fl                   FL.STABIL
#define STAB_K_fl                   FL.STAB_K
#define STAB_A_fl                   FL.STAB_A

#define AV_HOLD_fl                  FL.AV_HOLD
#define AV_DUMP_fl                  FL.AV_DUMP
#define AV_REAPPLY_fl               FL.AV_REAPPLY
#define HEAVY_FILT_fl               FL.HEAVY_FILT
#define INCREASE_VDIFF_fl           FL.INCREASE_VDIFF
#define V_STOER_fl                  FL.V_STOER
#define REAR_WHEEL_fl               FL.REAR_WHEEL
#define LEFT_WHEEL_fl               FL.LEFT_WHEEL

#define SPEEDMIN_fl                 FL.SPEEDMIN
#define ALG_INHIBIT_fl              FL.ALG_INHIBIT
#define RESET_PEAKCEL_fl            FL.RESET_PEAKCEL
#define CAS_fl                      FL.CAS
#define SL_HOLD_fl                  FL.SL_HOLD
#define SL_DUMP_fl                  FL.SL_DUMP
#define SL_BASE_fl                  FL.SL_BASE
#define HIGH_MU_INDICATION_fl       FL.HIGH_MU_INDICATION

#define DUMP_END_1_fl               FL.DUMP_END_1
#define DUMP_END_2_fl               FL.DUMP_END_2
#define ADD_DUMP_fl                 FL.ADD_DUMP
#define GMA_fl                      FL.GMA
#define GMA_SLIP_fl                 FL.GMA_SLIP
#define UN_GMA_fl                   FL.UN_GMA
#define CLR_HOLD_TIMER_fl           FL.CLR_HOLD_TIMER
#define EBD_INCREASE_fl             FL.EBD_INCREASE

#define SLIP_2P_NEG_fl              FL.SLIP_2P_NEG
#define SLIP_5P_NEG_fl              FL.SLIP_5P_NEG
#define SLIP_10P_NEG_fl             FL.SLIP_10P_NEG
#define SLIP_20P_NEG_fl             FL.SLIP_20P_NEG
#define SLIP_30P_NEG_fl             FL.SLIP_30P_NEG
#define SCHNELLSTE_VREF_fl          FL.SCHNELLSTE_VREF
#define LO_HI_JUMP_fl               FL.LO_HI_JUMP
#define JUMP_DOWN_fl                FL.JUMP_DOWN

#define PEAK_ACC_PASS_fl            FL.PEAK_ACC_PASS
#define CHECK_PEAK_ACCEL_fl         FL.CHECK_PEAK_ACCEL
#define FORCE_DUMP_fl               FL.FORCE_DUMP
#define ADAPTIVE_THRESHOLD_fl       FL.ADAPTIVE_THRESHOLD
#define SPOLD_RESET_fl              FL.SPOLD_RESET
#define WAIT_FCYC_DUMP_fl           FL.WAIT_FCYC_DUMP
#define FC_DUMP_fl                  FL.FC_DUMP
#define MSL_BASE_fl                 FL.MSL_BASE

#define ARAD_AT_MINUS_B_fl          FL.ARAD_AT_MINUS_B
#define MINUS_B_SET_AGAIN_fl        FL.MINUS_B_SET_AGAIN
#define LAM_1P_MINUS_fl             FL.LAM_1P_MINUS
#define LAM_2P_MINUS_fl             FL.LAM_2P_MINUS
#define LAM_3P_MINUS_fl             FL.LAM_3P_MINUS
#define LAM_4P_MINUS_fl             FL.LAM_4P_MINUS
#define LAM_5P_MINUS_fl             FL.LAM_5P_MINUS
#define SET_DUMP_fl                 FL.SET_DUMP

#define RES_FRQ_fl                  FL.RES_FRQ
#define HOLD_RES_fl                 FL.HOLD_RES
#define ARAD_3G_POS_fl              FL.ARAD_3G_POS
#define ARAD_NEG_fl                 FL.ARAD_NEG
#define ARAD_POS_fl                 FL.ARAD_POS
#define HIGH_VFILT_fl               FL.HIGH_VFILT
#define MSL_DUMP_fl                 FL.MSL_DUMP
//#define MSL_SET_fl                  FL.MSL_SET

#define LOW_TO_HIGH_fl              FL.LOW_TO_HIGH
#define PREV_LO_HI_fl               FL.PREV_LO_HI
#define CRNT_LO_HI_fl               FL.CRNT_LO_HI
#define DECT_LO_HI_fl               FL.DECT_LO_HI
#define LOW_TO_HIGH_PULSEUP_fl      FL.LOW_TO_HIGH_PULSEUP
#define WHEEL_AT_HIGH_MU_fl         FL.WHEEL_AT_HIGH_MU
#define flag71_fl                   FL.flag71
#define flag72_fl                   FL.flag72

#define PULSE_ROW_CHANGE_fl         FL.PULSE_ROW_CHANGE
#define BEND_PULSE_fl               FL.BEND_PULSE
#define TWO_PULSE_fl                FL.TWO_PULSE
#define RLS_DEPRESS_fl              FL.RLS_DEPRESS
#define INIT_AFZ_fl                 FL.INIT_AFZ
#define MINI_SPARE_WHEEL_fl         FL.MINI_SPARE_WHEEL
#define MOT_STOP_fl                 FL.MOT_STOP
#define SPLIT_SET_fl                FL.SPLIT_SET

#define BOTH_GMA_SET_fl             FL.BOTH_GMA_SET
#define SPLIT_JUMP_fl               FL.SPLIT_JUMP
#define SPLIT_PULSE_fl              FL.SPLIT_PULSE
//#define SPLIT_FSF_fl                FL.SPLIT_FSF
#define SPLIT_CLR_fl                FL.SPLIT_CLR
#define SET_DUMP2_fl                FL.SET_DUMP2
#define SPLIT_SET2_fl               FL.SPLIT_SET2
#define ABS_LIMIT_SPEED_fl                 FL.ABS_LIMIT_SPEED

#define EBD_fl                      FL.EBD
#define ENT_ABS_fl                  FL.ENT_ABS
#define dump_flg_fl                 FL.dump_flg
#define L_SLIP_CONTROL_fl           FL.L_SLIP_CONTROL
#define L_SLIP_CONTROL_OLD_fl       FL.L_SLIP_CONTROL_OLD
#define l_SLIP_K_fl                 FL.l_SLIP_K
#define l_SLIP_A_fl                 FL.l_SLIP_A

//#define SAFETY_WARN_fl              FL.SAFETY_WARN
//#define TREND_WARN_fl               FL.TREND_WARN
//#define L_OPTIMUM_DETECT_fl         FL.L_OPTIMUM_DETECT
//#define L_DUMP_fl                   FL.L_DUMP
//#define L_DUMP_OLD_fl               FL.L_DUMP_OLD
//#define L_DUMP_K_fl                 FL.L_DUMP_K
//#define L_DUMP_A_fl                 FL.L_DUMP_A
//#define L_1ST_D_H_fl                FL.L_1ST_D_H

#define POS_HOLD_fl                 FL.POS_HOLD
#define COUNT20_fl                  FL.COUNT20
#define POS_COUNT_fl                FL.POS_COUNT
#define BTCS_fl                     FL.BTCS
#define DETECT_BTC_fl               FL.DETECT_BTC
#define TCL_DEMAND_fl               FL.TCL_DEMAND
#define OPT_NEWSET_fl               FL.OPT_NEWSET
#define FIRST_INC_fl                FL.FIRST_INC

#define ARAD_RISE_fl                FL.ARAD_RISE
#define EXIT_BTCS_CONTROL_fl                FL.EXIT_BTCS_CONTROL
#define TCL_INC_fl                  FL.TCL_INC
#define TCL_INCF_fl                 FL.TCL_INCF
#define LOW_TO_HIGH1_fl             FL.LOW_TO_HIGH1
#define TCL_INCF2_fl                FL.TCL_INCF2
#define ARAD_POSITIVE_fl            FL.ARAD_POSITIVE
#define VRAD_DEC_fl                 FL.VRAD_DEC

#define HCONT_DEC_fl                FL.HCONT_DEC
#define ARAD_LOW_fl                 FL.ARAD_LOW
#define OPT_SET_HIGH_fl             FL.OPT_SET_HIGH
#define HALF_PULS_fl                FL.HALF_PULS
#define TMP_ERROR_fl                FL.TMP_ERROR
#define HILL_fl                     FL.HILL
#define TMP_START_fl                FL.TMP_START
#define TMP_RISE_fl                 FL.TMP_RISE

#define XMT_VIB_fl                  FL.XMT_VIB
#define BTC_LOW_SLIP_fl             FL.BTC_LOW_SLIP
#define VIB_SET_fl                  FL.VIB_SET
#define DETVIB_BTC_fl               FL.DETVIB_BTC
#define CIRCLE_fl                   FL.CIRCLE
#define VIB_COUNT_fl                FL.VIB_COUNT
#define TWO_BUILT_HI_fl             FL.TWO_BUILT_HI
#define TCS_VALVE_EXIT_fl                  FL.TCS_VALVE_EXIT

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//#define TEMP_HV_VL_fl               FL.TEMP_HV_VL
//#define TEMP_AV_VL_fl               FL.TEMP_AV_VL
#else
#define TEMP_HV_VL_fl               FL.TEMP_HV_VL
#define TEMP_AV_VL_fl               FL.TEMP_AV_VL
#endif

#define TEMP_TCL_DEMAND_fl          FL.TEMP_TCL_DEMAND
//#define VDC_ABS_WORK_fl             FL.VDC_ABS_WORK
//#define FIRST_VREF_SELECT_fl        FL.FIRST_VREF_SELECT
//#define F_A_R_P_fl                  FL.F_A_R_P
//#define F_A_D_P_fl                  FL.F_A_D_P
//#define F_A_H_P_fl                  FL.F_A_H_P

//#define NOT_ZERO_VREF_fl            FL.NOT_ZERO_VREF
//#define CAL_TVREF_fl                FL.CAL_TVREF
//#define TEST_FLAG_fl                FL.TEST_FLAG
//#define TEST_FLAG2_fl               FL.TEST_FLAG2
//#define ESP_fl                      FL.ESP
// LFC+VDC
//#define L_ABS_ESP_COMB_fl           FL.L_ABS_ESP_COMB
//#define L_DEL_P_CAL_OK_fl           FL.L_DEL_P_CAL_OK
//#define LFC_MODE_K_fl               FL.LFC_MODE_K
//#define LFC_MODE_A_fl               FL.LFC_MODE_A
//#define LFC_WITHOUT_ESP_fl          FL.LFC_WITHOUT_ESP
//#define LFC_WITH_ESP_fl             FL.LFC_WITH_ESP
#define ESP_ABS_SLIP_CONTROL_fl     FL.ESP_ABS_SLIP_CONTROL
#define CROSS_SLIP_CONTROL_fl       FL.CROSS_SLIP_CONTROL
#define CROSS_SLIP_CONTROL_old_fl   FL.CROSS_SLIP_CONTROL_old
#define L_flag160_fl                FL.L_flag160

#if __EDC
#define	ldedcu1EdcDecelDetect_fl			FL.ldedcu1EdcDecelDetect
#define	ldedcu1EdcDecelDetect_1G5_fl			FL.ldedcu1EdcDecelDetect_1G5
#define ldedcu1EdcNorOn_fl				FL.ldedcu1EdcNorOn
#define lcedcu1EdcWheelExit_fl			FL.lcedcu1EdcWheelExit
#define ldedcu1ParkBrkSuspectWL_fl			FL.ldedcu1ParkBrkSuspectWL
#define ldedcu1ParkDecelCheckWL_fl			FL.ldedcu1ParkDecelCheckWL
#endif

//FR
#define AV_VL_fr                    FR.AV_VL
#define HV_VL_fr                    FR.HV_VL
#define BUILT_fr                    FR.BUILT
#define ABS_fr                      FR.ABS
#define FSF_fr                      FR.FSF
#define STABIL_fr                   FR.STABIL
#define STAB_K_fr                   FR.STAB_K
#define STAB_A_fr                   FR.STAB_A

#define AV_HOLD_fr                  FR.AV_HOLD
#define AV_DUMP_fr                  FR.AV_DUMP
#define AV_REAPPLY_fr               FR.AV_REAPPLY
#define HEAVY_FILT_fr               FR.HEAVY_FILT
#define INCREASE_VDIFF_fr           FR.INCREASE_VDIFF
#define V_STOER_fr                  FR.V_STOER
#define REAR_WHEEL_fr               FR.REAR_WHEEL
#define LEFT_WHEEL_fr               FR.LEFT_WHEEL

#define SPEEDMIN_fr                 FR.SPEEDMIN
#define ALG_INHIBIT_fr              FR.ALG_INHIBIT
#define RESET_PEAKCEL_fr            FR.RESET_PEAKCEL
#define CAS_fr                      FR.CAS
#define SL_HOLD_fr                  FR.SL_HOLD
#define SL_DUMP_fr                  FR.SL_DUMP
#define SL_BASE_fr                  FR.SL_BASE
#define HIGH_MU_INDICATION_fr       FR.HIGH_MU_INDICATION

#define DUMP_END_1_fr               FR.DUMP_END_1
#define DUMP_END_2_fr               FR.DUMP_END_2
#define ADD_DUMP_fr                 FR.ADD_DUMP
#define GMA_fr                      FR.GMA
#define GMA_SLIP_fr                 FR.GMA_SLIP
#define UN_GMA_fr                   FR.UN_GMA
#define CLR_HOLD_TIMER_fr           FR.CLR_HOLD_TIMER
#define EBD_INCREASE_fr             FR.EBD_INCREASE

#define SLIP_2P_NEG_fr              FR.SLIP_2P_NEG
#define SLIP_5P_NEG_fr              FR.SLIP_5P_NEG
#define SLIP_10P_NEG_fr             FR.SLIP_10P_NEG
#define SLIP_20P_NEG_fr             FR.SLIP_20P_NEG
#define SLIP_30P_NEG_fr             FR.SLIP_30P_NEG
#define SCHNELLSTE_VREF_fr          FR.SCHNELLSTE_VREF
#define LO_HI_JUMP_fr               FR.LO_HI_JUMP
#define JUMP_DOWN_fr                FR.JUMP_DOWN

#define PEAK_ACC_PASS_fr            FR.PEAK_ACC_PASS
#define CHECK_PEAK_ACCEL_fr         FR.CHECK_PEAK_ACCEL
#define FORCE_DUMP_fr               FR.FORCE_DUMP
#define ADAPTIVE_THRESHOLD_fr       FR.ADAPTIVE_THRESHOLD
#define SPOLD_RESET_fr              FR.SPOLD_RESET
#define WAIT_FCYC_DUMP_fr           FR.WAIT_FCYC_DUMP
#define FC_DUMP_fr                  FR.FC_DUMP
#define MSL_BASE_fr                 FR.MSL_BASE

#define ARAD_AT_MINUS_B_fr          FR.ARAD_AT_MINUS_B
#define MINUS_B_SET_AGAIN_fr        FR.MINUS_B_SET_AGAIN
#define LAM_1P_MINUS_fr             FR.LAM_1P_MINUS
#define LAM_2P_MINUS_fr             FR.LAM_2P_MINUS
#define LAM_3P_MINUS_fr             FR.LAM_3P_MINUS
#define LAM_4P_MINUS_fr             FR.LAM_4P_MINUS
#define LAM_5P_MINUS_fr             FR.LAM_5P_MINUS
#define SET_DUMP_fr                 FR.SET_DUMP

#define RES_FRQ_fr                  FR.RES_FRQ
#define HOLD_RES_fr                 FR.HOLD_RES
#define ARAD_3G_POS_fr              FR.ARAD_3G_POS
#define ARAD_NEG_fr                 FR.ARAD_NEG
#define ARAD_POS_fr                 FR.ARAD_POS
#define HIGH_VFILT_fr               FR.HIGH_VFILT
#define MSL_DUMP_fr                 FR.MSL_DUMP
//#define MSL_SET_fr                  FR.MSL_SET

#define LOW_TO_HIGH_fr              FR.LOW_TO_HIGH
#define PREV_LO_HI_fr               FR.PREV_LO_HI
#define CRNT_LO_HI_fr               FR.CRNT_LO_HI
#define DECT_LO_HI_fr               FR.DECT_LO_HI
#define LOW_TO_HIGH_PULSEUP_fr      FR.LOW_TO_HIGH_PULSEUP
#define WHEEL_AT_HIGH_MU_fr         FR.WHEEL_AT_HIGH_MU
#define flag71_fr                   FR.flag71
#define flag72_fr                   FR.flag72

#define PULSE_ROW_CHANGE_fr         FR.PULSE_ROW_CHANGE
#define BEND_PULSE_fr               FR.BEND_PULSE
#define TWO_PULSE_fr                FR.TWO_PULSE
#define RLS_DEPRESS_fr              FR.RLS_DEPRESS
#define INIT_AFZ_fr                 FR.INIT_AFZ
#define MINI_SPARE_WHEEL_fr         FR.MINI_SPARE_WHEEL
#define MOT_STOP_fr                 FR.MOT_STOP
#define SPLIT_SET_fr                FR.SPLIT_SET

#define BOTH_GMA_SET_fr             FR.BOTH_GMA_SET
#define SPLIT_JUMP_fr               FR.SPLIT_JUMP
#define SPLIT_PULSE_fr              FR.SPLIT_PULSE
//#define SPLIT_FSF_fr                FR.SPLIT_FSF
#define SPLIT_CLR_fr                FR.SPLIT_CLR
#define SET_DUMP2_fr                FR.SET_DUMP2
#define SPLIT_SET2_fr               FR.SPLIT_SET2
#define ABS_LIMIT_SPEED_fr                 FR.ABS_LIMIT_SPEED

#define EBD_fr                      FR.EBD
#define ENT_ABS_fr                  FR.ENT_ABS
#define dump_flg_fr                 FR.dump_flg
#define L_SLIP_CONTROL_fr           FR.L_SLIP_CONTROL
#define L_SLIP_CONTROL_OLD_fr       FR.L_SLIP_CONTROL_OLD
#define l_SLIP_K_fr                 FR.l_SLIP_K
#define l_SLIP_A_fr                 FR.l_SLIP_A

//#define SAFETY_WARN_fr              FR.SAFETY_WARN
//#define TREND_WARN_fr               FR.TREND_WARN
//#define L_OPTIMUM_DETECT_fr         FR.L_OPTIMUM_DETECT
//#define L_DUMP_fr                   FR.L_DUMP
//#define L_DUMP_OLD_fr               FR.L_DUMP_OLD
//#define L_DUMP_K_fr                 FR.L_DUMP_K
//#define L_DUMP_A_fr                 FR.L_DUMP_A
//#define L_1ST_D_H_fr                FR.L_1ST_D_H

#define POS_HOLD_fr                 FR.POS_HOLD
#define COUNT20_fr                  FR.COUNT20
#define POS_COUNT_fr                FR.POS_COUNT
#define BTCS_fr                     FR.BTCS
#define DETECT_BTC_fr               FR.DETECT_BTC
#define TCL_DEMAND_fr               FR.TCL_DEMAND
#define OPT_NEWSET_fr               FR.OPT_NEWSET
#define FIRST_INC_fr                FR.FIRST_INC

#define ARAD_RISE_fr                FR.ARAD_RISE
#define EXIT_BTCS_CONTROL_fr                FR.EXIT_BTCS_CONTROL
#define TCL_INC_fr                  FR.TCL_INC
#define TCL_INCF_fr                 FR.TCL_INCF
#define LOW_TO_HIGH1_fr             FR.LOW_TO_HIGH1
#define TCL_INCF2_fr                FR.TCL_INCF2
#define ARAD_POSITIVE_fr            FR.ARAD_POSITIVE
#define VRAD_DEC_fr                 FR.VRAD_DEC

#define HCONT_DEC_fr                FR.HCONT_DEC
#define ARAD_LOW_fr                 FR.ARAD_LOW
#define OPT_SET_HIGH_fr             FR.OPT_SET_HIGH
#define HALF_PULS_fr                FR.HALF_PULS
#define TMP_ERROR_fr                FR.TMP_ERROR
#define HILL_fr                     FR.HILL
#define TMP_START_fr                FR.TMP_START
#define TMP_RISE_fr                 FR.TMP_RISE

#define XMT_VIB_fr                  FR.XMT_VIB
#define BTC_LOW_SLIP_fr             FR.BTC_LOW_SLIP
#define VIB_SET_fr                  FR.VIB_SET
#define DETVIB_BTC_fr               FR.DETVIB_BTC
#define CIRCLE_fr                   FR.CIRCLE
#define VIB_COUNT_fr                FR.VIB_COUNT
#define TWO_BUILT_HI_fr             FR.TWO_BUILT_HI
#define TCS_VALVE_EXIT_fr                  FR.TCS_VALVE_EXIT

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//#define TEMP_HV_VL_fr               FR.TEMP_HV_VL
//#define TEMP_AV_VL_fr               FR.TEMP_AV_VL
#else
#define TEMP_HV_VL_fr               FR.TEMP_HV_VL
#define TEMP_AV_VL_fr               FR.TEMP_AV_VL
#endif

#define TEMP_TCL_DEMAND_fr          FR.TEMP_TCL_DEMAND
//#define VDC_ABS_WORK_fr             FR.VDC_ABS_WORK
//#define FIRST_VREF_SELECT_fr        FR.FIRST_VREF_SELECT
//#define F_A_R_P_fr                  FR.F_A_R_P
//#define F_A_D_P_fr                  FR.F_A_D_P
//#define F_A_H_P_fr                  FR.F_A_H_P

//#define NOT_ZERO_VREF_fr            FR.NOT_ZERO_VREF
//#define CAL_TVREF_fr                FR.CAL_TVREF
//#define TEST_FLAG_fr                FR.TEST_FLAG
//#define TEST_FLAG2_fr               FR.TEST_FLAG2
//#define ESP_fr                      FR.ESP
// LFC+VDC
//#define L_ABS_ESP_COMB_fr           FR.L_ABS_ESP_COMB
//#define L_DEL_P_CAL_OK_fr           FR.L_DEL_P_CAL_OK
//#define LFC_MODE_K_fr               FR.LFC_MODE_K
//#define LFC_MODE_A_fr               FR.LFC_MODE_A
//#define LFC_WITHOUT_ESP_fr          FR.LFC_WITHOUT_ESP
//#define LFC_WITH_ESP_fr             FR.LFC_WITH_ESP
#define ESP_ABS_SLIP_CONTROL_fr     FR.ESP_ABS_SLIP_CONTROL
#define CROSS_SLIP_CONTROL_fr       FR.CROSS_SLIP_CONTROL
#define CROSS_SLIP_CONTROL_old_fr   FR.CROSS_SLIP_CONTROL_old
#define L_flag160_fr                FR.L_flag160

#if __EDC
#define	ldedcu1EdcDecelDetect_fr			FR.ldedcu1EdcDecelDetect
#define	ldedcu1EdcDecelDetect_1G5_fr			FR.ldedcu1EdcDecelDetect_1G5
#define ldedcu1EdcNorOn_fr				FR.ldedcu1EdcNorOn
#define lcedcu1EdcWheelExit_fr			FR.lcedcu1EdcWheelExit
#define ldedcu1ParkBrkSuspectWL_fr			FR.ldedcu1ParkBrkSuspectWL
#define ldedcu1ParkDecelCheckWL_fr			FR.ldedcu1ParkDecelCheckWL
#endif

//RL
#define AV_VL_rl                    RL.AV_VL
#define HV_VL_rl                    RL.HV_VL
#define BUILT_rl                    RL.BUILT
#define ABS_rl                      RL.ABS
#define FSF_rl                      RL.FSF
#define STABIL_rl                   RL.STABIL
#define STAB_K_rl                   RL.STAB_K
#define STAB_A_rl                   RL.STAB_A

#define AV_HOLD_rl                  RL.AV_HOLD
#define AV_DUMP_rl                  RL.AV_DUMP
#define AV_REAPPLY_rl               RL.AV_REAPPLY
#define HEAVY_FILT_rl               RL.HEAVY_FILT
#define INCREASE_VDIFF_rl           RL.INCREASE_VDIFF
#define V_STOER_rl                  RL.V_STOER
#define REAR_WHEEL_rl               RL.REAR_WHEEL
#define LEFT_WHEEL_rl               RL.LEFT_WHEEL

#define SPEEDMIN_rl                 RL.SPEEDMIN
#define ALG_INHIBIT_rl              RL.ALG_INHIBIT
#define RESET_PEAKCEL_rl            RL.RESET_PEAKCEL
#define CAS_rl                      RL.CAS
#define SL_HOLD_rl                  RL.SL_HOLD
#define SL_DUMP_rl                  RL.SL_DUMP
#define SL_BASE_rl                  RL.SL_BASE
#define HIGH_MU_INDICATION_rl       RL.HIGH_MU_INDICATION

#define DUMP_END_1_rl               RL.DUMP_END_1
#define DUMP_END_2_rl               RL.DUMP_END_2
#define ADD_DUMP_rl                 RL.ADD_DUMP
#define GMA_rl                      RL.GMA
#define GMA_SLIP_rl                 RL.GMA_SLIP
#define UN_GMA_rl                   RL.UN_GMA
#define CLR_HOLD_TIMER_rl           RL.CLR_HOLD_TIMER
#define EBD_INCREASE_rl             RL.EBD_INCREASE

#define SLIP_2P_NEG_rl              RL.SLIP_2P_NEG
#define SLIP_5P_NEG_rl              RL.SLIP_5P_NEG
#define SLIP_10P_NEG_rl             RL.SLIP_10P_NEG
#define SLIP_20P_NEG_rl             RL.SLIP_20P_NEG
#define SLIP_30P_NEG_rl             RL.SLIP_30P_NEG
#define SCHNELLSTE_VREF_rl          RL.SCHNELLSTE_VREF
#define LO_HI_JUMP_rl               RL.LO_HI_JUMP
#define JUMP_DOWN_rl                RL.JUMP_DOWN

#define PEAK_ACC_PASS_rl            RL.PEAK_ACC_PASS
#define CHECK_PEAK_ACCEL_rl         RL.CHECK_PEAK_ACCEL
#define FORCE_DUMP_rl               RL.FORCE_DUMP
#define ADAPTIVE_THRESHOLD_rl       RL.ADAPTIVE_THRESHOLD
#define SPOLD_RESET_rl              RL.SPOLD_RESET
#define WAIT_FCYC_DUMP_rl           RL.WAIT_FCYC_DUMP
#define FC_DUMP_rl                  RL.FC_DUMP
#define MSL_BASE_rl                 RL.MSL_BASE

#define ARAD_AT_MINUS_B_rl          RL.ARAD_AT_MINUS_B
#define MINUS_B_SET_AGAIN_rl        RL.MINUS_B_SET_AGAIN
#define LAM_1P_MINUS_rl             RL.LAM_1P_MINUS
#define LAM_2P_MINUS_rl             RL.LAM_2P_MINUS
#define LAM_3P_MINUS_rl             RL.LAM_3P_MINUS
#define LAM_4P_MINUS_rl             RL.LAM_4P_MINUS
#define LAM_5P_MINUS_rl             RL.LAM_5P_MINUS
#define SET_DUMP_rl                 RL.SET_DUMP

#define RES_FRQ_rl                  RL.RES_FRQ
#define HOLD_RES_rl                 RL.HOLD_RES
#define ARAD_3G_POS_rl              RL.ARAD_3G_POS
#define ARAD_NEG_rl                 RL.ARAD_NEG
#define ARAD_POS_rl                 RL.ARAD_POS
#define HIGH_VFILT_rl               RL.HIGH_VFILT
#define MSL_DUMP_rl                 RL.MSL_DUMP
//#define MSL_SET_rl                  RL.MSL_SET

#define LOW_TO_HIGH_rl              RL.LOW_TO_HIGH
#define PREV_LO_HI_rl               RL.PREV_LO_HI
#define CRNT_LO_HI_rl               RL.CRNT_LO_HI
#define DECT_LO_HI_rl               RL.DECT_LO_HI
#define LOW_TO_HIGH_PULSEUP_rl      RL.LOW_TO_HIGH_PULSEUP
#define WHEEL_AT_HIGH_MU_rl         RL.WHEEL_AT_HIGH_MU
#define flag71_rl                   RL.flag71
#define flag72_rl                   RL.flag72

#define PULSE_ROW_CHANGE_rl         RL.PULSE_ROW_CHANGE
#define BEND_PULSE_rl               RL.BEND_PULSE
#define TWO_PULSE_rl                RL.TWO_PULSE
#define RLS_DEPRESS_rl              RL.RLS_DEPRESS
#define INIT_AFZ_rl                 RL.INIT_AFZ
#define MINI_SPARE_WHEEL_rl         RL.MINI_SPARE_WHEEL
#define MOT_STOP_rl                 RL.MOT_STOP
#define SPLIT_SET_rl                RL.SPLIT_SET

#define BOTH_GMA_SET_rl             RL.BOTH_GMA_SET
#define SPLIT_JUMP_rl               RL.SPLIT_JUMP
#define SPLIT_PULSE_rl              RL.SPLIT_PULSE
//#define SPLIT_FSF_rl                RL.SPLIT_FSF
#define SPLIT_CLR_rl                RL.SPLIT_CLR
#define SET_DUMP2_rl                RL.SET_DUMP2
#define SPLIT_SET2_rl               RL.SPLIT_SET2
#define ABS_LIMIT_SPEED_rl                 RL.ABS_LIMIT_SPEED

#define EBD_rl                      RL.EBD
#define ENT_ABS_rl                  RL.ENT_ABS
#define dump_flg_rl                 RL.dump_flg
#define L_SLIP_CONTROL_rl           RL.L_SLIP_CONTROL
#define L_SLIP_CONTROL_OLD_rl       RL.L_SLIP_CONTROL_OLD
#define l_SLIP_K_rl                 RL.l_SLIP_K
#define l_SLIP_A_rl                 RL.l_SLIP_A

//#define SAFETY_WARN_rl              RL.SAFETY_WARN
//#define TREND_WARN_rl               RL.TREND_WARN
//#define L_OPTIMUM_DETECT_rl         RL.L_OPTIMUM_DETECT
//#define L_DUMP_rl                   RL.L_DUMP
//#define L_DUMP_OLD_rl               RL.L_DUMP_OLD
//#define L_DUMP_K_rl                 RL.L_DUMP_K
//#define L_DUMP_A_rl                 RL.L_DUMP_A
//#define L_1ST_D_H_rl                RL.L_1ST_D_H

#define POS_HOLD_rl                 RL.POS_HOLD
#define COUNT20_rl                  RL.COUNT20
#define POS_COUNT_rl                RL.POS_COUNT
#define BTCS_rl                     RL.BTCS
#define DETECT_BTC_rl               RL.DETECT_BTC
#define TCL_DEMAND_rl               RL.TCL_DEMAND
#define OPT_NEWSET_rl               RL.OPT_NEWSET
#define FIRST_INC_rl                RL.FIRST_INC

#define ARAD_RISE_rl                RL.ARAD_RISE
#define EXIT_BTCS_CONTROL_rl                RL.EXIT_BTCS_CONTROL
#define TCL_INC_rl                  RL.TCL_INC
#define TCL_INCF_rl                 RL.TCL_INCF
#define LOW_TO_HIGH1_rl             RL.LOW_TO_HIGH1
#define TCL_INCF2_rl                RL.TCL_INCF2
#define ARAD_POSITIVE_rl            RL.ARAD_POSITIVE
#define VRAD_DEC_rl                 RL.VRAD_DEC

#define HCONT_DEC_rl                RL.HCONT_DEC
#define ARAD_LOW_rl                 RL.ARAD_LOW
#define OPT_SET_HIGH_rl             RL.OPT_SET_HIGH
#define HALF_PULS_rl                RL.HALF_PULS
#define TMP_ERROR_rl                RL.TMP_ERROR
#define HILL_rl                     RL.HILL
#define TMP_START_rl                RL.TMP_START
#define TMP_RISE_rl                 RL.TMP_RISE

#define XMT_VIB_rl                  RL.XMT_VIB
#define BTC_LOW_SLIP_rl             RL.BTC_LOW_SLIP
#define VIB_SET_rl                  RL.VIB_SET
#define DETVIB_BTC_rl               RL.DETVIB_BTC
#define CIRCLE_rl                   RL.CIRCLE
#define VIB_COUNT_rl                RL.VIB_COUNT
#define TWO_BUILT_HI_rl                  RL.TWO_BUILT_HI
#define TCS_VALVE_EXIT_rl                  RL.TCS_VALVE_EXIT

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//#define TEMP_HV_VL_rl               RL.TEMP_HV_VL
//#define TEMP_AV_VL_rl               RL.TEMP_AV_VL
#else
#define TEMP_HV_VL_rl               RL.TEMP_HV_VL
#define TEMP_AV_VL_rl               RL.TEMP_AV_VL
#endif

#define TEMP_TCL_DEMAND_rl          RL.TEMP_TCL_DEMAND
//#define VDC_ABS_WORK_rl             RL.VDC_ABS_WORK
//#define FIRST_VREF_SELECT_rl        RL.FIRST_VREF_SELECT
//#define F_A_R_P_rl                  RL.F_A_R_P
//#define F_A_D_P_rl                  RL.F_A_D_P
//#define F_A_H_P_rl                  RL.F_A_H_P

//#define NOT_ZERO_VREF_rl            RL.NOT_ZERO_VREF
//#define CAL_TVREF_rl                RL.CAL_TVREF
//#define TEST_FLAG_rl                RL.TEST_FLAG
//#define TEST_FLAG2_rl               RL.TEST_FLAG2
//#define ESP_rl                      RL.ESP
// LFC+VDC
//#define L_ABS_ESP_COMB_rl           RL.L_ABS_ESP_COMB
//#define L_DEL_P_CAL_OK_rl           RL.L_DEL_P_CAL_OK
//#define LFC_MODE_K_rl               RL.LFC_MODE_K
//#define LFC_MODE_A_rl               RL.LFC_MODE_A
//#define LFC_WITHOUT_ESP_rl          RL.LFC_WITHOUT_ESP
//#define LFC_WITH_ESP_rl             RL.LFC_WITH_ESP
#define ESP_ABS_SLIP_CONTROL_rl     RL.ESP_ABS_SLIP_CONTROL
#define CROSS_SLIP_CONTROL_rl       RL.CROSS_SLIP_CONTROL
#define CROSS_SLIP_CONTROL_old_rl   RL.CROSS_SLIP_CONTROL_old
#define L_flag160_rl                RL.L_flag160

#if __EDC
#define	ldedcu1EdcDecelDetect_rl			RL.ldedcu1EdcDecelDetect
#define	ldedcu1EdcDecelDetect_1G5_rl			RL.ldedcu1EdcDecelDetect_1G5
#define ldedcu1EdcNorOn_rl				RL.ldedcu1EdcNorOn
#define lcedcu1EdcWheelExit_rl			RL.lcedcu1EdcWheelExit
#define ldedcu1ParkBrkSuspectWL_rl			RL.ldedcu1ParkBrkSuspectWL
#define ldedcu1ParkDecelCheckWL_rl			RL.ldedcu1ParkDecelCheckWL
#endif

//RR
#define AV_VL_rr                    RR.AV_VL
#define HV_VL_rr                    RR.HV_VL
#define BUILT_rr                    RR.BUILT
#define ABS_rr                      RR.ABS
#define FSF_rr                      RR.FSF
#define STABIL_rr                   RR.STABIL
#define STAB_K_rr                   RR.STAB_K
#define STAB_A_rr                   RR.STAB_A

#define AV_HOLD_rr                  RR.AV_HOLD
#define AV_DUMP_rr                  RR.AV_DUMP
#define AV_REAPPLY_rr               RR.AV_REAPPLY
#define HEAVY_FILT_rr               RR.HEAVY_FILT
#define INCREASE_VDIFF_rr           RR.INCREASE_VDIFF
#define V_STOER_rr                  RR.V_STOER
#define REAR_WHEEL_rr               RR.REAR_WHEEL
#define LEFT_WHEEL_rr               RR.LEFT_WHEEL

#define SPEEDMIN_rr                 RR.SPEEDMIN
#define ALG_INHIBIT_rr              RR.ALG_INHIBIT
#define RESET_PEAKCEL_rr            RR.RESET_PEAKCEL
#define CAS_rr                      RR.CAS
#define SL_HOLD_rr                  RR.SL_HOLD
#define SL_DUMP_rr                  RR.SL_DUMP
#define SL_BASE_rr                  RR.SL_BASE
#define HIGH_MU_INDICATION_rr       RR.HIGH_MU_INDICATION

#define DUMP_END_1_rr               RR.DUMP_END_1
#define DUMP_END_2_rr               RR.DUMP_END_2
#define ADD_DUMP_rr                 RR.ADD_DUMP
#define GMA_rr                      RR.GMA
#define GMA_SLIP_rr                 RR.GMA_SLIP
#define UN_GMA_rr                   RR.UN_GMA
#define CLR_HOLD_TIMER_rr           RR.CLR_HOLD_TIMER
#define EBD_INCREASE_rr             RR.EBD_INCREASE

#define SLIP_2P_NEG_rr              RR.SLIP_2P_NEG
#define SLIP_5P_NEG_rr              RR.SLIP_5P_NEG
#define SLIP_10P_NEG_rr             RR.SLIP_10P_NEG
#define SLIP_20P_NEG_rr             RR.SLIP_20P_NEG
#define SLIP_30P_NEG_rr             RR.SLIP_30P_NEG
#define SCHNELLSTE_VREF_rr          RR.SCHNELLSTE_VREF
#define LO_HI_JUMP_rr               RR.LO_HI_JUMP
#define JUMP_DOWN_rr                RR.JUMP_DOWN

#define PEAK_ACC_PASS_rr            RR.PEAK_ACC_PASS
#define CHECK_PEAK_ACCEL_rr         RR.CHECK_PEAK_ACCEL
#define FORCE_DUMP_rr               RR.FORCE_DUMP
#define ADAPTIVE_THRESHOLD_rr       RR.ADAPTIVE_THRESHOLD
#define SPOLD_RESET_rr              RR.SPOLD_RESET
#define WAIT_FCYC_DUMP_rr           RR.WAIT_FCYC_DUMP
#define FC_DUMP_rr                  RR.FC_DUMP
#define MSL_BASE_rr                 RR.MSL_BASE

#define ARAD_AT_MINUS_B_rr          RR.ARAD_AT_MINUS_B
#define MINUS_B_SET_AGAIN_rr        RR.MINUS_B_SET_AGAIN
#define LAM_1P_MINUS_rr             RR.LAM_1P_MINUS
#define LAM_2P_MINUS_rr             RR.LAM_2P_MINUS
#define LAM_3P_MINUS_rr             RR.LAM_3P_MINUS
#define LAM_4P_MINUS_rr             RR.LAM_4P_MINUS
#define LAM_5P_MINUS_rr             RR.LAM_5P_MINUS
#define SET_DUMP_rr                 RR.SET_DUMP

#define RES_FRQ_rr                  RR.RES_FRQ
#define HOLD_RES_rr                 RR.HOLD_RES
#define ARAD_3G_POS_rr              RR.ARAD_3G_POS
#define ARAD_NEG_rr                 RR.ARAD_NEG
#define ARAD_POS_rr                 RR.ARAD_POS
#define HIGH_VFILT_rr               RR.HIGH_VFILT
#define MSL_DUMP_rr                 RR.MSL_DUMP
//#define MSL_SET_rr                  RR.MSL_SET

#define LOW_TO_HIGH_rr              RR.LOW_TO_HIGH
#define PREV_LO_HI_rr               RR.PREV_LO_HI
#define CRNT_LO_HI_rr               RR.CRNT_LO_HI
#define DECT_LO_HI_rr               RR.DECT_LO_HI
#define LOW_TO_HIGH_PULSEUP_rr      RR.LOW_TO_HIGH_PULSEUP
#define WHEEL_AT_HIGH_MU_rr         RR.WHEEL_AT_HIGH_MU
#define flag71_rr                   RR.flag71
#define flag72_rr                   RR.flag72

#define PULSE_ROW_CHANGE_rr         RR.PULSE_ROW_CHANGE
#define BEND_PULSE_rr               RR.BEND_PULSE
#define TWO_PULSE_rr                RR.TWO_PULSE
#define RLS_DEPRESS_rr              RR.RLS_DEPRESS
#define INIT_AFZ_rr                 RR.INIT_AFZ
#define MINI_SPARE_WHEEL_rr         RR.MINI_SPARE_WHEEL
#define MOT_STOP_rr                 RR.MOT_STOP
#define SPLIT_SET_rr                RR.SPLIT_SET

#define BOTH_GMA_SET_rr             RR.BOTH_GMA_SET
#define SPLIT_JUMP_rr               RR.SPLIT_JUMP
#define SPLIT_PULSE_rr              RR.SPLIT_PULSE
//#define SPLIT_FSF_rr                RR.SPLIT_FSF
#define SPLIT_CLR_rr                RR.SPLIT_CLR
#define SET_DUMP2_rr                RR.SET_DUMP2
#define SPLIT_SET2_rr               RR.SPLIT_SET2
#define ABS_LIMIT_SPEED_rr                 RR.ABS_LIMIT_SPEED

#define EBD_rr                      RR.EBD
#define ENT_ABS_rr                  RR.ENT_ABS
#define dump_flg_rr                 RR.dump_flg
#define L_SLIP_CONTROL_rr           RR.L_SLIP_CONTROL
#define L_SLIP_CONTROL_OLD_rr       RR.L_SLIP_CONTROL_OLD
#define l_SLIP_K_rr                 RR.l_SLIP_K
#define l_SLIP_A_rr                 RR.l_SLIP_A

//#define SAFETY_WARN_rr              RR.SAFETY_WARN
//#define TREND_WARN_rr               RR.TREND_WARN
//#define L_OPTIMUM_DETECT_rr         RR.L_OPTIMUM_DETECT
//#define L_DUMP_rr                   RR.L_DUMP
//#define L_DUMP_OLD_rr               RR.L_DUMP_OLD
//#define L_DUMP_K_rr                 RR.L_DUMP_K
//#define L_DUMP_A_rr                 RR.L_DUMP_A
//#define L_1ST_D_H_rr                RR.L_1ST_D_H

#define POS_HOLD_rr                 RR.POS_HOLD
#define COUNT20_rr                  RR.COUNT20
#define POS_COUNT_rr                RR.POS_COUNT
#define BTCS_rr                     RR.BTCS
#define DETECT_BTC_rr               RR.DETECT_BTC
#define TCL_DEMAND_rr               RR.TCL_DEMAND
#define OPT_NEWSET_rr               RR.OPT_NEWSET
#define FIRST_INC_rr                RR.FIRST_INC

#define ARAD_RISE_rr                RR.ARAD_RISE
#define EXIT_BTCS_CONTROL_rr                RR.EXIT_BTCS_CONTROL
#define TCL_INC_rr                  RR.TCL_INC
#define TCL_INCF_rr                 RR.TCL_INCF
#define LOW_TO_HIGH1_rr             RR.LOW_TO_HIGH1
#define TCL_INCF2_rr                RR.TCL_INCF2
#define ARAD_POSITIVE_rr            RR.ARAD_POSITIVE
#define VRAD_DEC_rr                 RR.VRAD_DEC

#define HCONT_DEC_rr                RR.HCONT_DEC
#define ARAD_LOW_rr                 RR.ARAD_LOW
#define OPT_SET_HIGH_rr             RR.OPT_SET_HIGH
#define HALF_PULS_rr                RR.HALF_PULS
#define TMP_ERROR_rr                RR.TMP_ERROR
#define HILL_rr                     RR.HILL
#define TMP_START_rr                RR.TMP_START
#define TMP_RISE_rr                 RR.TMP_RISE

#define XMT_VIB_rr                  RR.XMT_VIB
#define BTC_LOW_SLIP_rr             RR.BTC_LOW_SLIP
#define VIB_SET_rr                  RR.VIB_SET
#define DETVIB_BTC_rr               RR.DETVIB_BTC
#define CIRCLE_rr                   RR.CIRCLE
#define VIB_COUNT_rr                RR.VIB_COUNT
#define TWO_BUILT_HI_rr                  RR.TWO_BUILT_HI
#define TCS_VALVE_EXIT_rr                  RR.TCS_VALVE_EXIT

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//#define TEMP_HV_VL_rr               RR.TEMP_HV_VL
//#define TEMP_AV_VL_rr               RR.TEMP_AV_VL
#else
#define TEMP_HV_VL_rr               RR.TEMP_HV_VL
#define TEMP_AV_VL_rr               RR.TEMP_AV_VL
#endif

#define TEMP_TCL_DEMAND_rr          RR.TEMP_TCL_DEMAND
//#define VDC_ABS_WORK_rr             RR.VDC_ABS_WORK
//#define FIRST_VREF_SELECT_rr        RR.FIRST_VREF_SELECT
//#define F_A_R_P_rr                  RR.F_A_R_P
//#define F_A_D_P_rr                  RR.F_A_D_P
//#define F_A_H_P_rr                  RR.F_A_H_P

//#define NOT_ZERO_VREF_rr            RR.NOT_ZERO_VREF
//#define CAL_TVREF_rr                RR.CAL_TVREF
//#define TEST_FLAG_rr                RR.TEST_FLAG
//#define TEST_FLAG2_rr               RR.TEST_FLAG2
//#define ESP_rr                      RR.ESP

// LFC+VDC
//#define L_ABS_ESP_COMB_rr           RR.L_ABS_ESP_COMB
//#define L_DEL_P_CAL_OK_rr           RR.L_DEL_P_CAL_OK
//#define LFC_MODE_K_rr               RR.LFC_MODE_K
//#define LFC_MODE_A_rr               RR.LFC_MODE_A
//#define LFC_WITHOUT_ESP_rr          RR.LFC_WITHOUT_ESP
//#define LFC_WITH_ESP_rr             RR.LFC_WITH_ESP
#define ESP_ABS_SLIP_CONTROL_rr     RR.ESP_ABS_SLIP_CONTROL
#define CROSS_SLIP_CONTROL_rr       RR.CROSS_SLIP_CONTROL
#define CROSS_SLIP_CONTROL_old_rr   RR.CROSS_SLIP_CONTROL_old
#define L_flag160_rr                RR.L_flag160

#if __EDC
#define	ldedcu1EdcDecelDetect_rr			RR.ldedcu1EdcDecelDetect
#define	ldedcu1EdcDecelDetect_1G5_rr			RR.ldedcu1EdcDecelDetect_1G5
#define ldedcu1EdcNorOn_rr				RR.ldedcu1EdcNorOn
#define lcedcu1EdcWheelExit_rr			RR.lcedcu1EdcWheelExit
#define ldedcu1ParkBrkSuspectWL_rr			RR.ldedcu1ParkBrkSuspectWL
#define ldedcu1ParkDecelCheckWL_rr			RR.ldedcu1ParkDecelCheckWL
#endif

// 051004 New BTCS Structure
#define BTCS_CTL_UPDATE_fl			FL.BTCS_CTL_UPDATE
#define BTCS_CTL_UPDATE_fr			FR.BTCS_CTL_UPDATE
#define BTCS_CTL_UPDATE_rl			RL.BTCS_CTL_UPDATE
#define BTCS_CTL_UPDATE_rr			RR.BTCS_CTL_UPDATE

#define lctcsu1State2To1_fl			FL.lctcsu1State2To1
#define lctcsu1State2To1_fr			FR.lctcsu1State2To1
#define lctcsu1State2To1_rl			RL.lctcsu1State2To1
#define lctcsu1State2To1_rr			RR.lctcsu1State2To1

#define lctcsu1InsideWheelEngFlag_fl	FL.lctcsu1InsideWheelEngFlag
#define lctcsu1InsideWheelEngFlag_fr	FR.lctcsu1InsideWheelEngFlag
#define lctcsu1InsideWheelEngFlag_rl	RL.lctcsu1InsideWheelEngFlag
#define lctcsu1InsideWheelEngFlag_rr	RR.lctcsu1InsideWheelEngFlag

#define lctcsu1InsideWheelBrkCheck_fl	FL.lctcsu1InsideWheelBrkCheck
#define lctcsu1InsideWheelBrkCheck_fr	FR.lctcsu1InsideWheelBrkCheck
#define lctcsu1InsideWheelBrkCheck_rl	RL.lctcsu1InsideWheelBrkCheck
#define lctcsu1InsideWheelBrkCheck_rr	RR.lctcsu1InsideWheelBrkCheck

#define lctcsu1InsideWheelESCheck_fl    FL.lctcsu1InsideWheelESCheck
#define lctcsu1InsideWheelESCheck_fr    FR.lctcsu1InsideWheelESCheck
#define lctcsu1InsideWheelESCheck_rl    RL.lctcsu1InsideWheelESCheck
#define lctcsu1InsideWheelESCheck_rr    RR.lctcsu1InsideWheelESCheck

#define lctcsu1CompDutyForL2Split_fl    FL.lctcsu1CompDutyForL2Split
#define lctcsu1CompDutyForL2Split_fr    FR.lctcsu1CompDutyForL2Split
#define lctcsu1CompDutyForL2Split_rl    RL.lctcsu1CompDutyForL2Split
#define lctcsu1CompDutyForL2Split_rr    RR.lctcsu1CompDutyForL2Split

#if __TCS_RISE_TH_ADJUST
#define lctcsu1SpinSlope_fl			FL.lctcsu1SpinSlope
#define lctcsu1SpinSlope_fr			FR.lctcsu1SpinSlope
#define lctcsu1SpinSlope_rl			RL.lctcsu1SpinSlope
#define lctcsu1SpinSlope_rr			RR.lctcsu1SpinSlope

#define lctcsu1BrkCtlCmdReset_fl	FL.lctcsu1BrkCtlCmdReset
#define lctcsu1BrkCtlCmdReset_fr	FR.lctcsu1BrkCtlCmdReset
#define lctcsu1BrkCtlCmdReset_rl	RL.lctcsu1BrkCtlCmdReset
#define lctcsu1BrkCtlCmdReset_rr	RR.lctcsu1BrkCtlCmdReset
#endif	/* #if __TCS_RISE_TH_ADJUST */

#define VCA_RR_CTL				RR.VCA_CTL
#define VCA_RL_CTL				RL.VCA_CTL


#if __VREF_AUX
#define rel_lam_aux_fl              FL.rel_lam_aux
#define rel_lam_aux_alt_fl          FL.rel_lam_aux_alt
#define rel_lam_aux_fr              FR.rel_lam_aux
#define rel_lam_aux_alt_fr          FR.rel_lam_aux_alt
#define rel_lam_aux_rl              RL.rel_lam_aux
#define rel_lam_aux_alt_rl          RL.rel_lam_aux_alt
#define rel_lam_aux_rr              RR.rel_lam_aux
#define rel_lam_aux_alt_rr          RR.rel_lam_aux_alt
#endif

#define AV_DUMP_OLD_wl              WL->AV_DUMP_OLD

struct  V_STRUCT
{
    int16_t vrselect;
    int16_t voptfz_mittel;
    int16_t voptfz_rest;
    int16_t filter_out;
    int16_t voptfz;
    int16_t voptfz_diff;

    int16_t vrselect_resol_change;
    int16_t voptfz_mittel_resol_change;
    int16_t filter_out_resol_change;
    int16_t voptfz_resol_change;
  	int16_t filt_inp;
  	int8_t vref_dec_limit_mod;
  	int8_t vref_inc_limit_mod;
  	int8_t vref_inc_limit_flg;
  	int8_t vref_dec_limit_flg;
  	
  	int16_t lss16VrefVehAccel;
};

#define vrselect1                   FZ1.vrselect
#define voptfz_mittel1              FZ1.voptfz_mittel
#define voptfz_rest1                FZ1.voptfz_rest
#define filter_out1                 FZ1.filter_out
#define voptfz1                     FZ1.voptfz
#define voptfz_diff1                FZ1.voptfz_diff

#define vrselect2                   FZ2.vrselect
#define voptfz_mittel2              FZ2.voptfz_mittel
#define voptfz_rest2                FZ2.voptfz_rest
#define filter_out2                 FZ2.filter_out
#define voptfz2                     FZ2.voptfz
#define voptfz_diff2                FZ2.voptfz_diff

#define voptfz_raw1                 FZ1.voptfz_raw
#define voptfz_raw2                 FZ2.voptfz_raw


#define vrselect1_resol_change                   FZ1.vrselect_resol_change
#define vrselect2_resol_change                   FZ2.vrselect_resol_change
		#if __SIDE_VREF
#define	vrselect3_resol_change                   FZ3.vrselect_resol_change
#define vrselect4_resol_change                   FZ4.vrselect_resol_change
#define	vrselect3                   				FZ3.vrselect
#define vrselect4                   				FZ4.vrselect
#define	voptfz3_resol_change                   	FZ3.voptfz_resol_change
#define voptfz4_resol_change                   	FZ4.voptfz_resol_change
		#endif

#if (__4WD||(__4WD_VARIANT_CODE==ENABLE))
#define	vrselect_for_4WD			FZ_for_4WD.vrselect
#define	voptfz_by_along				FZ_for_4WD.voptfz

#define	vrselect_for_4WD_resol_change			FZ_for_4WD.vrselect_resol_change
#define	voptfz_by_along_resol_change			FZ_for_4WD.voptfz_resol_change
#endif

#define VFLAG0  *(uint8_t*)&VF0
#define VFLAG1  *(uint8_t*)&VF1
#define VFLAG2  *(uint8_t*)&VF2
#define VFLAG3  *(uint8_t*)&VF3
#define VFLAG4  *(uint8_t*)&VF4
#define VFLAG5  *(uint8_t*)&VF5
#define VFLAG6  *(uint8_t*)&VF6
#define VFLAG7  *(uint8_t*)&VF7
#define VFLAG8  *(uint8_t*)&VF8
#define VFLAG9  *(uint8_t*)&VF9
#define VFLAG10  *(uint8_t*)&VF10
#define VFLAG11  *(uint8_t*)&VF11      // 2002.04.09. for LFC
#define VFLAG12  *(uint8_t*)&VF12      // 2002.11.22. for LFC
#define VFLAG13  *(uint8_t*)&VF13      // 2003.07.02. for LFC
#define VFLAG14  *(uint8_t*)&VF14      // 2003.09.23. for LFC
#define VFLAG15  *(uint8_t*)&VF15      // 2004.02.25. for EDC
#define VFLAG16  *(uint8_t*)&VF16      // 2004.02.25. for EDC
#define VFLAG17  *(uint8_t*)&VF17      // 2004.02.25. for EDC


#define VFLAG21  *(uint8_t*)&VF21     // 2005.05.23. for MGH40 ABS


/* VFLAG0 */
#define ABS_fz                      VF0.bit7
#define BLS                         VF0.bit6
#define BLS_K                       VF0.bit5
#define BLS_A                       VF0.bit4
#define BLS_PIN                     VF0.bit3
#define AFZ_OK                      VF0.bit2
#define AFZ_OK_K                    VF0.bit1
#define EBD_RA                      VF0.bit0
/*       VFLAG1 */
#define INIT_NEU                    VF1.bit7
#define EMERGENCY                   VF1.bit6
#define V_INST_80                   VF1.bit5
#define AQUA_END                    VF1.bit4
#define AFZ_AQUA                    VF1.bit3
#define EBD_DETN                    VF1.bit2
#define EBD_OFF                     VF1.bit1
#define EBD_MOTOR_DRV               VF1.bit0
/*       VFLAG2 */
#define tempF0                      VF2.bit7
#define tempF1                      VF2.bit6
#define UNDERVOLT_DETECT_ABS        VF2.bit5
#define BEND_DETECT                 VF2.bit4
#define BRAKE_RELEASE               VF2.bit3
#define INSIDE_ABS_fz               VF2.bit2
#define VALVE_ACT                   VF2.bit1
#define GMA_PULSE                   VF2.bit0
/*       VFLAG3 */
#define CIRCUIT_FAIL                VF3.bit7
#define VF3_6                      	VF3.bit6
#define VF3_5               		VF3.bit5
#define VF3_4                  		VF3.bit4
#define VF3_3                		VF3.bit3
#define VF3_2                		VF3.bit2
#define VF3_1                		VF3.bit1
#define VF3_0                		VF3.bit0

/*       VFLAG4 */
#define AWD_flg                     VF4.bit7
#define LFC_mot_pwm_ok                VF4.bit6
#define temp_0_flg                VF4.bit5
#define CALC_FIRST_SPIN             VF4.bit4
#define lcIgnOffModeFlg             VF4.bit3
#define VREF_4WD                    VF4.bit2
#define lcedcu1CtrlTimeOver         VF4.bit1
#define ALL_DUMP                    VF4.bit0
/*       VFLAG5 */
#define ICE_SET                     VF5.bit7
//#define CHECK_ICE                   VF5.bit6
#define ACTIVE_BRAKE_MOT_ON         VF5.bit6
#define VF5_5                   VF5.bit5
#define VF5_4                  VF5.bit4
#define CYCLE_START_FLAG            VF5.bit3
#define Rough_road_detect_vehicle_EBD         VF5.bit2
#define VF5_1                VF5.bit1
#define VF5_0                  VF5.bit0

/*       VFLAG6 */
#define BTC_FRONT                   VF6.bit7
#define BTC_REAR                    VF6.bit6
#define BTCS_VIB                    VF6.bit5
/*#define A_FIRST_INC                 VF6.bit4*/
/*#define A_OPT_NEWSET                VF6.bit3*/
#define A_OPT_SET_HIGH              VF6.bit2
/*#define SLIP_SET                    VF6.bit1*/
//#define GMA_SET                     VF6.bit0  //not used!!!
#define RE_ABS_flag                 VF6.bit0
/*       VFLAG7 */
#define TC_MOT_ON                   VF7.bit7
#define BTCS_TWO_OFF                VF7.bit6
#define MOT_ON_FLG                  VF7.bit5
#define BTC_fz                      VF7.bit4
#define tempF2                      VF7.bit3
#define tc_error_flg                VF7.bit2
#define tc_on_flg                   VF7.bit1
#define vcc_off_flg                 VF7.bit0
/*       VFLAG8 BTCS */
#define BLS_START                   VF8.bit7
#define BLS_SAFE                    VF8.bit6
#define BLS_ERROR                   VF8.bit5
#define BLS_RISE                    VF8.bit4
#define BLS_ERROR_ALT               VF8.bit3
#define BLS_WRITE                   VF8.bit2
#define BLS_EEPROM                  VF8.bit1
#define BLS_EPR                     VF8.bit0
/*       VFLAG9 BTCS */
#define RE_ON                       VF9.bit7
#define CHANGE_SLIP                 VF9.bit6
#define YAW_SET                     VF9.bit5
#define HIGH_SLIP                   VF9.bit4
#define BTC_LEFT                    VF9.bit3
#define BTC_RIGHT                   VF9.bit2
#define BTC_SPIN                    VF9.bit1
//#define ICE_SET_UP                  VF9.bit0
#define AVR_DRV_WHEEL_SPEED_Update 	VF9.bit0
/*       VFLAG10 */
#define USE_VREF                    VF10.bit7
#define LFC_Split_flag_start        VF10.bit6
#define MSL_SPLIT_CLR               VF10.bit5
#define Rough_road_suspect_vehicle  VF10.bit4        // ROUGH_SUSPECT  // mi
#define VF10_3             VF10.bit3
#define VF10_2               VF10.bit2
#define Rough_road_arad_OK          VF10.bit1        // GOTO_CHECK_EBD // mi
#define VF10_0                VF10.bit0
/*       VFLAG11(LFC) */
#define Partial_Brk_YCW_flag        VF11.bit7
#define Partial_Brk_YCW_flag_old    VF11.bit6
#define L_MEDIUM_BRAKE              VF11.bit5    // not used !
#define L_QUICK_BRAKE               VF11.bit4    // not used !
#define L_ASSUME_LOW_MU             VF11.bit3    // not used !
#define VFLAG11_2                   VF11.bit2    // not used !
#define L_ASSUME_HI_MU              VF11.bit1    // not used !
#define Rough_road_detect_vehicle   VF11.bit0
/*       VFLAG12(LFC) */
#define Double_brake_YMR            VF12.bit7
#define LFC_Split_suspect_flag      VF12.bit6
#define LFC_end_duty_OK_flag		VF12.bit5
#define LFC_Split_flag_end          VF12.bit4
#define MOTOR_FORCE_OFF_VEHICLE     VF12.bit3
#define	LFC_Split_flag				VF12.bit2
#define LFC_H_to_Split_flag         VF12.bit1
#define LFC_L_to_Split_flag         VF12.bit0
/*       VFLAG13(LFC) */
#define Double_suspect            	VF13.bit7
#define PREFILL_TEST_FLAG      	    VF13.bit6
#define not_use_13_5			VF13.bit5
#define not_use_13_4            VF13.bit4
#define not_use_13_3    		VF13.bit3
#define	LOW_to_HIGH_suspect			VF13.bit2
#define LOW_to_HIGH_suspect_K       VF13.bit1
#define H_to_Split_Front_Dump  		VF13.bit0
/*       VFLAG14(LFC) */
#define BEND_DETECT_EBD            	VF14.bit7
#define LFC_GMA_Split_flag  		VF14.bit6
//#define MOTOR_FORCE_DOWN_VEHICLE    VF14.bit5
#define BTCS_Entry_Inhibition    	VF14.bit5
#define CERTAIN_SPLIT_flag     		VF14.bit4
#if	__CASCADING_WEAKEN
#define AFZ_LOW_to_MEDIUM_flag		VF14.bit3
#define	AFZ_LOW_to_HIGH_flag		VF14.bit2
#define AFZ_MEDIUM_to_HIGH_flag		VF14.bit1
#else
#define VFLAG14_3    				VF14.bit3
#define	VFLAG14_2					VF14.bit2
#define VFLAG14_1       			VF14.bit1
#endif
#if __UCC_COOPERATION_CONTROL
#define UCC_DETECT_BUMP_Vehicle_flag VF14.bit0
#else
#define VFLAG14_0  					VF14.bit0
#endif

/*       VFLAG15(EDC) */
#define AUTO_TM       				VF15.bit7
#define Engine_Drag_flag   			VF15.bit6
#define ABS_ON_flag					VF15.bit5
#define Engine_braking_flag         VF15.bit4
#define EDC_NOR_ON_FLAG				VF15.bit3
#define	EDC_ON						VF15.bit2
#define EDC_ABS_ON			       	VF15.bit1
#define EDC_INHIBIT_flag			VF15.bit0

/*       VFLAG16(EDC) */
#define EDC_SOLO_ON					VF16.bit7
#define EDC_SUSTAIN_1SEC			VF16.bit6
#define EDC_EXIT					VF16.bit5
#define EDC_NOR_EXIT	  			VF16.bit4
#define EDC_ESP_ON	    			VF16.bit3
#define	ldedcu1ParkBrkSuspectFlag	VF16.bit2
#define ldedcu1EDCFailMode 			VF16.bit1
#define ldedcu1HighMuSuspectFlag	VF16.bit0

/*       VFLAG17(EDC) */
#define ldedcu1TempTireSuspect				VF17.bit7
#define ldedcu1TempTireSuspectInCtl			VF17.bit6
#define ldedcu1WheelLockDetectFlag			VF17.bit5
#define ldedcu1EDCPermitSpeed	  			VF17.bit4
#define ldedcu1GearShiftDownSuspectFlag		VF17.bit3
#define	ldedcu1DetectTempTire_1WL			VF17.bit2
#define ldedcu1DetectTempTire_2WL			VF17.bit1
#define lcedcu1CtrlEnable   				VF17.bit0


/*              VFLAG21         */
#define lcedcu1ExitByExcessiveTqUp          VF21.bit7
#define lcedcu1TqDownByExcessiveTqUp    	VF21.bit6
  #if __BEND_DETECT_using_SIDEVREF
#define BEND_DETECT_flag                          VF21.bit5
#define BEND_DETECT_before_ABS_flag               VF21.bit4
#define BEND_DETECT_LEFT                          VF21.bit3
#define BEND_DETECT_RIGHT                         VF21.bit2
#define BEND_DETECT_ESP                   VF21.bit1
#define BEND_DETECT2                    VF21.bit0
  #else
#define reserved_for_MGH40ABS5                    VF21.bit5
#define reserved_for_MGH40ABS4                    VF21.bit4
#define reserved_for_MGH40ABS3                    VF21.bit3
#define reserved_for_MGH40ABS2                    VF21.bit2
#define reserved_for_MGH40ABS1                    VF21.bit1
#define reserved_for_MGH40ABS0                    VF21.bit0
   #endif


#if	__TCMF_CONTROL
#define BOOSTER_PRESSURE_INCREASE_flag				VF19.bit7
#define BOOSTER_PRESSURE_DECREASE_flag             	VF19.bit6
#define PERMIT_DCT_VACUUM_LEVEL_AFTER_BRAKE			VF19.bit5
#define HBB_ON		                     			VF19.bit4
#define not_use_19_3                     			VF19.bit3
#define not_use_19_2                     			VF19.bit2
#define not_use_19_1                     			VF19.bit1
#define not_use_19_0                     			VF19.bit0

#define Low_Vacuum_Boost_Assist_flag				VF22.bit7      //Out side-Left ==1/ Inside-Left==0
#define TCMF_LowVacuumBoost_flag                 	VF22.bit6
#define TCMF_MOTOR_OFF_flag0         				VF22.bit5
#define TCMF_MOTOR_OFF_flag                     	VF22.bit4
#define not_use_22_3                     			VF22.bit3
#define TCMF_MOT_ON                     			VF22.bit2
#define TCMF_LONG_HOLD_flag_p                     	VF22.bit1
#define TCMF_LONG_HOLD_flag_s                     	VF22.bit0
#endif


#define Debug_flag_24_07                     VF24.bit7
#define Debug_flag_24_06                     VF24.bit6
#define Debug_flag_24_05                     VF24.bit5
#define Debug_flag_24_04                     VF24.bit4
#define Debug_flag_24_03                     VF24.bit3
#define Debug_flag_24_02                     VF24.bit2
#define Debug_flag_24_01               		 VF24.bit1
#define Flg_SPORTSMODE_1_ESP_OK              		 VF24.bit0



  #if (__VDC && __MGH40_ESP_BEND_DETECT)
struct SIDE_STRUCT
{
    uint8_t Bend_detect_cnt_ESP;
    int8_t sign;

    uint16_t side_temp7:          	      1;   /* bit 7  */
    uint16_t ESP_BEND_flag:          	   1;   /* bit 6  */
    uint16_t side_temp5:          			1;   /* bit 5  */ /* BEND_DETECT */
    uint16_t side_temp4:          			1;   /* bit 4  */
    uint16_t side_temp3:          			1;   /* bit 3  */
    uint16_t side_temp2:          			1;   /* bit 2  */
    uint16_t side_temp1:          			1;   /* bit 1  */
    uint16_t side_temp0:          			1;   /* bit 0  */
};
#endif

#endif
