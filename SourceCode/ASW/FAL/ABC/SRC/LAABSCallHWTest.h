#ifndef __LAABSCALLHWTEST_H__
#define __LAABSCALLHWTEST_H__

/*includes********************************************************************/
#include "LVarHead.h"


/*Global Type Declaration ****************************************************/
#define __HW_TEST_MODE                  0

#if __HW_TEST_MODE

#define __HW_TEST_MODULE_STRUCT_CHANGE	ENABLE

typedef struct
{
    uint16_t    lau1HwTestBit00     :1;
    uint16_t    lau1HwTestBit01     :1;
    uint16_t    lau1HwTestBit02     :1;
    uint16_t    lau1HwTestBit03     :1;
    uint16_t    lau1HwTestBit04     :1;
    uint16_t    lau1HwTestBit05     :1;
    uint16_t    lau1HwTestBit06     :1;
    uint16_t    lau1HwTestBit07     :1;
}HW_FLG_t;

typedef struct
{
	uint8_t  u8HwTestPwmDuty;
	uint8_t  u8HwTestReapplyTime;
	uint8_t  u8HwTestDumpScanTime;
	
    uint16_t    u1HwTestHvVlAct     :1;
    uint16_t    u1HwTestAvVlAct     :1;
}LA_HWTEST_VALVE_t;

#define U16_TEST_MOTOR_ON_TIME_MAX	L_U16_TIME_10MSLOOP_20S

/*Global Extern Variable  Declaration*****************************************/
extern uint16_t lau16WhlNoTestModeActive;
extern int8_t    las8HwTestMode;
extern uint16_t    HW_test_cnt_t;
extern uint16_t    lau16HwTestCnt;

  #if (__VDC && __TCMF_ENABLE)
extern uint16_t    valve_test_cnt;
extern uint8_t   lau8HwTestTcNoDuty, lau8HwTestEsvDuty;
  #endif
 #if __VDC
extern uint8_t lau8HwTestABSLampRequest;
extern uint8_t lau8HwTestMscDuty;
extern int16_t las16HwTestMscOnTimeMsec, las16HwTestMscOffTimeMsec;
  #endif

extern LA_HWTEST_VALVE_t laHwTestValveFL, laHwTestValveFR, laHwTestValveRL, laHwTestValveRR;

/*Global Extern Functions  Declaration****************************************/
extern void LAABS_vTestWheelNoValve(void);
  #if __VDC
extern void LAABS_vTestMotor(void);
  #endif
  #if (__VDC && __TCMF_ENABLE)
extern void LAMFC_vTestTcNoValve(void);
  #endif
  
#endif

#endif
