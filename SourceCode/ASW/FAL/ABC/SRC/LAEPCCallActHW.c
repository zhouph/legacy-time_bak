/******************************************************************************
* Project Name: ESP Precirculation
* File: LAEPCCallActHW.C
* Date: Dec. 12. 2005
******************************************************************************/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START                       
	#define idx_FILE	idx_CL_LAEPCCallActHW 
	#include "Mdyn_autosar.h"               
#endif
/* Includes ******************************************************************/

#include "LAEPCCallActHW.h"
#include "LACallMain.h"
#if	(__BRK_SIG_MPS_TO_PEDAL_SEN==1)
#include "LSESPFilterEspSensor.h"
#include "LSESPCalSensorOffset.h"
#endif

#if __CDM
#include "SCC/LCSCCCallControl.h"
#endif

  #if __ADVANCED_MSC
    #include "LAABSCallMotorSpeedControl.h"
    void LCMSC_vSetEPCTargetVoltage(void);
  #endif



/* Logcal Definiton  *********************************************************/
U8_BIT_STRUCT_t EPCA0;
U8_BIT_STRUCT_t EPCA1;
U8_BIT_STRUCT_t EPCA2;

#if __ENABLE_PREFILL
/* Variables Definition*******************************************************/

#define S16_PRE_FILL_TARGET_VOLTAGE_REAR 3000
/* LocalFunction prototype ***************************************************/
void LAEPC_vCallActHW(void);
void LAEPC_vCallActValve(void);
void LCEPC_vCallClearValveMotor(void);

/* Implementation*************************************************************/


void LAEPC_vCallActHW(void)
{

    LCEPC_vCallClearValveMotor();

#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
  #if __GM_FailM
	if ((ESP_ERROR_FLG==1)||(BLS==1)||(
			((lsespu1PedalBrakeOn==1)&&(lsespu1BrkAppSenInvalid == 0))
			||((BLS==1)&&(lsespu1BrkAppSenInvalid == 1))
			)||(ABS_fz==1)||(EBD_RA==1)||(FTCS_ON==1)||(BTC_fz==1)||(ETCS_ON==1)||(ESP_TCS_ON==1))
  #else
	if ((ESP_ERROR_FLG==1)||(BLS==1)||(
			((lsespu1PedalBrakeOn==1)&&(lsespu1BrkAppSenInvalid == 0))
			||((BLS==1)&&(lsespu1BrkAppSenInvalid == 1))
			)||(ABS_fz==1)||(EBD_RA==1)||(FTCS_ON==1)||(BTC_fz==1)||(ETCS_ON==1)||(ESP_TCS_ON==1))
  #endif
#else
  #if __GM_FailM
	if ((ESP_ERROR_FLG==1)||(BLS==1)||(MPRESS_BRAKE_ON==1)||(ABS_fz==1)||(EBD_RA==1)||(FTCS_ON==1)||(BTC_fz==1)||(ETCS_ON==1)||(ESP_TCS_ON==1))
  #else
	if ((ESP_ERROR_FLG==1)||(BLS==1)||(MPRESS_BRAKE_ON==1)||(ABS_fz==1)||(EBD_RA==1)||(FTCS_ON==1)||(BTC_fz==1)||(ETCS_ON==1)||(ESP_TCS_ON==1))
  #endif
#endif
    {
	    FL_PRE_CIRCULATION = 0;
    	FR_PRE_CIRCULATION = 0;
		RL_PRE_CIRCULATION = 0;
		RR_PRE_CIRCULATION = 0;
    }
    else            /* Active */
    {
	    FL_PRE_CIRCULATION = FLAG_ACT_PRE_ACTION_FL;
    	FR_PRE_CIRCULATION = FLAG_ACT_PRE_ACTION_FR;
		
		RL_PRE_CIRCULATION = FLAG_ACT_PRE_ACTION_RL;
		RR_PRE_CIRCULATION = FLAG_ACT_PRE_ACTION_RR;
		
	}

    if ((FL_PRE_CIRCULATION==1)||(FR_PRE_CIRCULATION==1)||(RL_PRE_CIRCULATION==1)||(RR_PRE_CIRCULATION==1))
    {
        if (FL_PRE_CIRCULATION==1)
        {
          #if __SPLIT_TYPE      /* FR  split */
            EPC_S_VALVE_PRIMARY=1;
            EPC_TCL_DEMAND_PRIMARY=1;
            EPC_HV_VL_fl=0;
            EPC_AV_VL_fl=1;
            EPC_HV_VL_fr=1;
            EPC_AV_VL_fr=1;
        	  LA_vSetNominalWheelValvePwmDuty(EPC_HV_VL_fl,EPC_AV_VL_fl,U8_BASE_CTRLTIME,U8_BASE_CTRLTIME,&laepc_FL1HP);
        	  LA_vSetNominalWheelValvePwmDuty(EPC_HV_VL_fr,EPC_AV_VL_fr,U8_BASE_CTRLTIME,U8_BASE_CTRLTIME,&laepc_FR1HP);       	  
          #else                 /* X  split */
            EPC_S_VALVE_SECONDARY=1;
            EPC_TCL_DEMAND_SECONDARY=1;
            EPC_HV_VL_fl=0;
            EPC_AV_VL_fl=1;
            EPC_HV_VL_rr=1;
            EPC_AV_VL_rr=1;
            LA_vSetNominalWheelValvePwmDuty(EPC_HV_VL_fl,EPC_AV_VL_fl,U8_BASE_CTRLTIME,U8_BASE_CTRLTIME,&laepc_FL1HP);
						LA_vSetNominalWheelValvePwmDuty(EPC_HV_VL_rr,EPC_AV_VL_rr,U8_BASE_CTRLTIME,U8_BASE_CTRLTIME,&laepc_RR1HP);          
          #endif
        }
        else {}

        if (FR_PRE_CIRCULATION==1)
        {
          #if __SPLIT_TYPE      /* FR  split */
            EPC_S_VALVE_PRIMARY=1;
            EPC_TCL_DEMAND_PRIMARY=1;
            EPC_HV_VL_fr=0;
            EPC_AV_VL_fr=1;
            EPC_HV_VL_fl=1;
            EPC_AV_VL_fl=1;
            LA_vSetNominalWheelValvePwmDuty(EPC_HV_VL_fl,EPC_AV_VL_fl,U8_BASE_CTRLTIME,U8_BASE_CTRLTIME,&laepc_FL1HP);
        	  LA_vSetNominalWheelValvePwmDuty(EPC_HV_VL_fr,EPC_AV_VL_fr,U8_BASE_CTRLTIME,U8_BASE_CTRLTIME,&laepc_FR1HP);          
          #else                 /* X  split */
            EPC_S_VALVE_PRIMARY=1;
            EPC_TCL_DEMAND_PRIMARY=1;
            EPC_HV_VL_fr=0;
            EPC_AV_VL_fr=1;
            EPC_HV_VL_rl=1;
            EPC_AV_VL_rl=1;
        	  LA_vSetNominalWheelValvePwmDuty(EPC_HV_VL_fr,EPC_AV_VL_fr,U8_BASE_CTRLTIME,U8_BASE_CTRLTIME,&laepc_FR1HP);
        	  LA_vSetNominalWheelValvePwmDuty(EPC_HV_VL_rl,EPC_AV_VL_rl,U8_BASE_CTRLTIME,U8_BASE_CTRLTIME,&laepc_RL1HP);
          #endif
        }
        else {}
         if  ((FR_PRE_CIRCULATION==0)&&(RL_PRE_CIRCULATION==1))
        {
          #if __SPLIT_TYPE      /* FR  split */
            EPC_S_VALVE_PRIMARY=1;
            EPC_TCL_DEMAND_PRIMARY=1;
            EPC_HV_VL_rl=0;
            EPC_AV_VL_rl=1;
            EPC_HV_VL_rr=1;
            EPC_AV_VL_rr=1;
            LA_vSetNominalWheelValvePwmDuty(EPC_HV_VL_rl,EPC_AV_VL_rl,U8_BASE_CTRLTIME,U8_BASE_CTRLTIME,&laepc_RL1HP);
        	  LA_vSetNominalWheelValvePwmDuty(EPC_HV_VL_rr,EPC_AV_VL_rr,U8_BASE_CTRLTIME,U8_BASE_CTRLTIME,&laepc_RR1HP);
          #else                 /* X  split */
			EPC_S_VALVE_PRIMARY=1;
            EPC_TCL_DEMAND_PRIMARY=1;
            EPC_HV_VL_fr=1;
            EPC_AV_VL_fr=1;
            EPC_HV_VL_rl=0;
            EPC_AV_VL_rl=1;
            LA_vSetNominalWheelValvePwmDuty(EPC_HV_VL_fr,EPC_AV_VL_fr,U8_BASE_CTRLTIME,U8_BASE_CTRLTIME,&laepc_FR1HP);
        	  LA_vSetNominalWheelValvePwmDuty(EPC_HV_VL_rl,EPC_AV_VL_rl,U8_BASE_CTRLTIME,U8_BASE_CTRLTIME,&laepc_RL1HP);         
          #endif
        }
        else {}

        if ((FL_PRE_CIRCULATION==0)&&(RR_PRE_CIRCULATION==1))
        {
          #if __SPLIT_TYPE      /* FR  split */
            EPC_S_VALVE_SECONDARY=1;
            EPC_TCL_DEMAND_SECONDARY=1;
            EPC_HV_VL_rr=0;
            EPC_AV_VL_rr=1;
            EPC_HV_VL_rl=1;
            EPC_AV_VL_rl=1;
            LA_vSetNominalWheelValvePwmDuty(EPC_HV_VL_rl,EPC_AV_VL_rl,U8_BASE_CTRLTIME,U8_BASE_CTRLTIME,&laepc_RL1HP);
        	  LA_vSetNominalWheelValvePwmDuty(EPC_HV_VL_rr,EPC_AV_VL_rr,U8_BASE_CTRLTIME,U8_BASE_CTRLTIME,&laepc_RR1HP);         
          #else                 /* X  split */
            EPC_S_VALVE_SECONDARY=1;
            EPC_TCL_DEMAND_SECONDARY=1;
            EPC_HV_VL_rr=0;
            EPC_AV_VL_rr=1;
            EPC_HV_VL_fl=1;
            EPC_AV_VL_fl=1;
            LA_vSetNominalWheelValvePwmDuty(EPC_HV_VL_fl,EPC_AV_VL_fl,U8_BASE_CTRLTIME,U8_BASE_CTRLTIME,&laepc_FL1HP);
        	  LA_vSetNominalWheelValvePwmDuty(EPC_HV_VL_rr,EPC_AV_VL_rr,U8_BASE_CTRLTIME,U8_BASE_CTRLTIME,&laepc_RR1HP);          
          #endif
        }
        else {}

        EPC_ON = 1;
        
        la_FL1HP = laepc_FL1HP; 
				la_FL2HP = laepc_FL1HP;
				la_FR1HP = laepc_FR1HP; 
				la_FR2HP = laepc_FR1HP;
				la_RL1HP = laepc_RL1HP; 
				la_RL2HP = laepc_RL1HP;
				la_RR1HP = laepc_RR1HP; 
				la_RR2HP = laepc_RR1HP;
    }
    else
    {
        EPC_ON = 0;

        FL_PRE_CIRCULATION=0;
        FR_PRE_CIRCULATION=0;
        RL_PRE_CIRCULATION=0;
        RR_PRE_CIRCULATION=0;

    }

    LAEPC_vCallActValve();

}

void LCEPC_vCallClearValveMotor(void)
{

    EPC_HV_VL_fl=0;
    EPC_AV_VL_fl=0;
    EPC_HV_VL_fr=0;
    EPC_AV_VL_fr=0;
    EPC_HV_VL_rl=0;
    EPC_AV_VL_rl=0;
    EPC_HV_VL_rr=0;
    EPC_AV_VL_rr=0;

    EPC_S_VALVE_PRIMARY     =0;
    EPC_S_VALVE_SECONDARY   =0;
    EPC_TCL_DEMAND_PRIMARY  =0;
    EPC_TCL_DEMAND_SECONDARY=0;

}


void LAEPC_vCallActValve(void)
{
#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
    if ((ESP_ERROR_FLG==1)||(BLS==1)||(
				((lsespu1PedalBrakeOn==1)&&(lsespu1BrkAppSenInvalid == 0))
				||((BLS==1)&&(lsespu1BrkAppSenInvalid == 1))
				)||(ABS_fz==1)||(EBD_RA==1))
#else
    if ((ESP_ERROR_FLG==1)||(BLS==1)||(MPRESS_BRAKE_ON==1)||(ABS_fz==1)||(EBD_RA==1))
#endif
    {
        ;
    }
    else            /* Active */
    {

	    if (EPC_ON)
	    {
	        HV_VL_fl = EPC_HV_VL_fl;
	        AV_VL_fl = EPC_AV_VL_fl;
	        HV_VL_fr = EPC_HV_VL_fr;
	        AV_VL_fr = EPC_AV_VL_fr;
	        HV_VL_rl = EPC_HV_VL_rl;
	        AV_VL_rl = EPC_AV_VL_rl;
	        HV_VL_rr = EPC_HV_VL_rr;
	        AV_VL_rr = EPC_AV_VL_rr;
	
	      #if __SPLIT_TYPE      /* FR  split */
	        S_VALVE_PRIMARY     = EPC_S_VALVE_PRIMARY;
	        S_VALVE_SECONDARY   = EPC_S_VALVE_SECONDARY;
	        TCL_DEMAND_PRIMARY  = EPC_TCL_DEMAND_PRIMARY;
	        TCL_DEMAND_SECONDARY= EPC_TCL_DEMAND_SECONDARY;
	      #else                 /* X  split */
	        S_VALVE_RIGHT       = EPC_S_VALVE_PRIMARY;
	        S_VALVE_LEFT        = EPC_S_VALVE_SECONDARY;
	        TCL_DEMAND_fr       = EPC_TCL_DEMAND_PRIMARY;
	        TCL_DEMAND_fl       = EPC_TCL_DEMAND_SECONDARY;
	      #endif
	    }
	    else {}
	}
}


#if __ADVANCED_MSC
void LCMSC_vSetEPCTargetVoltage(void){

    EPC_MSC_MOTOR_ON = EPC_ON;

    if (EPC_MSC_MOTOR_ON)
    {
		if((FL_PRE_CIRCULATION==1)||(FR_PRE_CIRCULATION==1))
		{
			epc_msc_target_vol = (int16_t)S16_PRE_FILL_TARGET_VOLTAGE;
		  #if __CDM
		    if(FLAG_ACT_PRE_ACTION_CDM==1)
		    {
		    	epc_msc_target_vol = (int16_t)S16_CDM_PRE_FILL_TARGET_VOLTAGE;
		    }
		    else{}	
		  #endif
		  
		}
		else
		{
			epc_msc_target_vol = (int16_t)S16_PRE_FILL_TARGET_VOLTAGE_REAR;
		}


		if (epc_msc_target_vol > MSC_8_V)
		{
			epc_msc_target_vol = MSC_8_V;
		}
		else if (epc_msc_target_vol < MSC_1_V)
		{
			epc_msc_target_vol = MSC_1_V;
		}
		else {}
    }
    else
    {
        epc_msc_target_vol = MSC_0_V;
    }
}
#endif

#endif

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LAEPCCallActHW 
	#include "Mdyn_autosar.h"               
#endif
