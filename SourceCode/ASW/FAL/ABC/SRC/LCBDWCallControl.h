#ifndef __LCBDWCALLCONTROL_H__
#define __LCBDWCALLCONTROL_H__

/*includes********************************************************************/
#include "LVarHead.h"
//#include "LDBDWCallDetection.h"


  #if __BDW
/*Global Type Declaration ****************************************************/
#define BDW_flag_00_0                       BDWC0.bit0
#define BDW_flag_00_1                       BDWC0.bit1
#define BDW_flag_00_2                       BDWC0.bit2
#define BDW_flag_00_3                       BDWC0.bit3
#define BDW_FL_ON_FLG                       BDWC0.bit4
#define BDW_FR_ON_FLG                       BDWC0.bit5
#define BDW_RL_ON_FLG                       BDWC0.bit6
#define BDW_RR_ON_FLG                       BDWC0.bit7

#define BDW_flag_01_0                       BDWC1.bit0
#define BDW_flag_01_1                       BDWC1.bit1
#define BDW_ON                              BDWC1.bit2
#define BDW_flag_01_3                       BDWC1.bit3
#define BDW_flag_01_4                       BDWC1.bit4
#define BDW_flag_01_5                       BDWC1.bit5
#define BDW_flag_01_6                       BDWC1.bit6
#define BDW_flag_01_7                       BDWC1.bit7

#define BDW_HV_VL_fl                        BDWC2.bit0
#define BDW_AV_VL_fl                        BDWC2.bit1
#define BDW_HV_VL_fr                        BDWC2.bit2
#define BDW_AV_VL_fr                        BDWC2.bit3
#define BDW_HV_VL_rl                        BDWC2.bit4
#define BDW_AV_VL_rl                        BDWC2.bit5
#define BDW_HV_VL_rr                        BDWC2.bit6
#define BDW_AV_VL_rr                        BDWC2.bit7

#define BDW_S_VALVE_PRIMARY                 BDWC3.bit0
#define BDW_S_VALVE_SECONDARY               BDWC3.bit1
#define BDW_TCL_DEMAND_PRIMARY              BDWC3.bit2
#define BDW_TCL_DEMAND_SECONDARY            BDWC3.bit3
#define BDW_MOTOR_ON                        BDWC3.bit4
#define BDW_flag_03_5                       BDWC3.bit5
#define BDW_flag_03_6                       BDWC3.bit6
#define BDW_flag_03_7                       BDWC3.bit7




/*Global Extern Variable  Declaration*****************************************/
extern U8_BIT_STRUCT_t BDWC0,BDWC1;

 
extern int16_t bdw_active_counter;
 

/*Global Extern Functions  Declaration****************************************/

  #endif
#endif





