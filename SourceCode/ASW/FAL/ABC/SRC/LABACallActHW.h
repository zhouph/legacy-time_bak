#ifndef __LABACALLACTHW_H__
#define __LABACALLACTHW_H__

/*includes********************************************************************/

#include "LVarHead.h"


#define S16_BA_TC_TARGET_PRESS_MAX		MPRESS_150BAR

/*Global Type Declaration ****************************************************/

#define labau1InhibitFlg                 BAF0.bit0
#define BA_flag0_1                       BAF0.bit1
#define BA_flag0_2                       BAF0.bit2
#define BA_flag0_3                       BAF0.bit3
#define BA_flag0_4                       BAF0.bit4
#define BA_flag0_5                       BAF0.bit5
#define BA_flag0_6                       BAF0.bit6
#define BA_flag0_7                       BAF0.bit7

/*Global Extern Variable  Declaration*****************************************/

extern U8_BIT_STRUCT_t BAF0;

extern int16_t pba_msc_target_vol;

#if __FBC
extern int16_t fbc_msc_target_vol;
#endif

#if  ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
extern int16_t labas16TcTargetPress;
#endif


/*Global Extern Functions  Declaration****************************************/

extern void LCMSC_vSetBATargetVoltage(void);

#endif
