#ifndef __LSCANSIGNALINTERFACE_H__
#define __LSCANSIGNALINTERFACE_H__

/*Includes *********************************************************************/
#include "LVarHead.h"

/*Global MACRO CONSTANT Definition *********************************************/

/*Global MACRO FUNCTION Definition *********************************************/

/*Global Type Declaration ******************************************************/

/*Global Extern Variable Declaration *******************************************/

    extern int16_t   mtp;
    extern int16_t   mtp_resol1000;        
    extern int16_t   drive_torq;

    extern int16_t   max_torq;
    extern int16_t   drive_torq_old;
    extern int16_t   gs_ena;
    extern int16_t   gs_pos;
    extern int16_t   gs_sel;
    extern int16_t   eng_torq, eng_torq_old;
    extern uint16_t  eng_rpm;
    extern int16_t   fric_torq;
	extern int16_t   drive_torq_abs, eng_torq_abs, fric_torq_abs;
	extern int16_t   drive_torq_rel, eng_torq_rel, fric_torq_rel;
	extern uint16_t  tc_rpm, tc_rpm_old;	

	extern int16_t 	 eng_torq_filt_old;
	extern int16_t 	 eng_torq_filt;
	extern int16_t 	 slope_eng_torq_old;
	extern int16_t 	 slope_eng_torq;
	               	 
	extern int16_t 	 cal_torq_filt_old;
	extern int16_t 	 cal_torq_filt;
	extern int16_t 	 slope_cal_torq_old;
	extern int16_t 	 slope_cal_torq;	
	               	 
	extern int16_t 	 acc_r_filt_old;
	extern int16_t 	 acc_r_filt;
	extern int16_t 	 slope_acc_r_old;
	extern int16_t 	 slope_acc_r;		
	               	 
	extern uint8_t 	 lctcsu8FLampCntForOff;
	extern uint8_t 	 lctcsu1FLampOffByDeltaTorq;
	extern uint8_t 	 lctcsu1FLampOffByTorqRate;
	extern uint8_t 	 lctcsu1FLampOffByAx;
	extern uint8_t   lctcsu1FLampOffByEstimatedBrk;
	extern uint8_t 	 lctcsu1FLampOffByAccelForce;

	#if(NEW_FM_ENABLE==ENABLE)  

	extern uchar8_t lu1DrvModeInv;
	extern uchar8_t lu1eng_torqInv;
	extern uchar8_t lu1TMModeIdInv;
	extern uchar8_t lu1EMS_ENG_CHRInv;
	extern uchar8_t lu1EMS_ENG_VOLInv;
	extern uchar8_t lu1max_torqInv;
	extern uchar8_t lu1TCU_F_TCUInv;
	extern uchar8_t lu1gs_enaInv;
	extern uchar8_t lu1drive_torqInv;
	extern uchar8_t lu1gs_posInv;
	extern uchar8_t lu1DriveLineEnableInv;
	extern uchar8_t lu1fric_torqInv;
	
	#endif/*NEW_FM_ENABLE*/

#define     GEAR_POS_TM_P                   0
#define     GEAR_POS_TM_N                   0
#define     GEAR_POS_TM_1                   1
#define     GEAR_POS_TM_2                   2
#define     GEAR_POS_TM_3                   3
#define     GEAR_POS_TM_4                   4
#define     GEAR_POS_TM_5                   5
	#if (__HMC_CAN_VER==CAN_2_0)
#define     GEAR_POS_TM_R                  14
	#else
#define     GEAR_POS_TM_R                   7
	#endif

#if (__CAR_MAKER==SSANGYONG)
	#define     GEAR_POS_TM_R                  11	
#endif
	
#endif
