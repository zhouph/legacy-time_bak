/*******************************************************************/
/*  Program ID: MGH-40 Beta Estimation                             */
/*  Program description: Logging Data for Beta and Bank Estimation */
/*  Input files:                                                   */
/*                                                                 */
/*  Output files:                                                  */
/*                                                                 */
/*  Special notes: Will be applicated to KMC HM(SOP.07.08)         */
/*******************************************************************/
/*  Modification   Log                                             */
/*  Date           Author           Description                    */
/*  -------       ----------      ---------------------------      */
/*  05.12.09      JinKoo Lee      Initial Release                  */
/*******************************************************************/


#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LSESPCallBetaLogData
	#include "Mdyn_autosar.h"
#endif                                                                   
/* Includes ********************************************************/
#include "LSESPCallBetaLogData.h"
#include "LDESPEstBeta.h"
#include "LDESPCalVehicleModel.h"
#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
	#include "LSESPCalSensorOffset.h"
	#include "LSESPFilterEspSensor.h"
#endif


//#if __LOGGER && (LOGGER_DATA_FORMAT==1)
#if defined(__LOGGER)
    #if __LOGGER_DATA_TYPE==1
    #include "LSCallLogData.h"
    #endif
#endif

//#if __LOGGER && (LOGGER_DATA_FORMAT==1)
#if defined(__LOGGER)
    #if __LOGGER_DATA_TYPE==1
#if __VDC && __MGH40_Beta_Estimation
                                                                     
/* Local Definition  ***********************************************/
                                                                     
                                                                     
/* Variables Definition*********************************************/


/* Local Function prototype ****************************************/

void LSESP_vCallBetaLogData(void);

/* GlobalFunction prototype ****************************************/

//extern unsigned int8_t INHIBITION_INDICATOR(void);

/* Implementation **************************************************/

void LSESP_vCallBetaLogData(void)
{
    uint8_t i;
    
    CAN_LOG_DATA[0] = (uint8_t)system_loop_counter;   
    CAN_LOG_DATA[1] = (uint8_t)(system_loop_counter>>8);           // 1
    CAN_LOG_DATA[2] = (uint8_t)yaw_out;            
    CAN_LOG_DATA[3] = (uint8_t)(yaw_out>>8);                       // 2
    CAN_LOG_DATA[4] = (uint8_t)alat;            
    CAN_LOG_DATA[5] = (uint8_t)(alat>>8);                          // 3
    CAN_LOG_DATA[6] = (uint8_t)wstr;          
    CAN_LOG_DATA[7] = (uint8_t)(wstr>>8);                          // 4

    CAN_LOG_DATA[8]  = (uint8_t)vrad_fl;     
    CAN_LOG_DATA[9]  = (uint8_t)(vrad_fl>>8);                      // 5
    CAN_LOG_DATA[10] = (uint8_t)vrad_fr;     
    CAN_LOG_DATA[11] = (uint8_t)(vrad_fr>>8);                      // 6
    CAN_LOG_DATA[12] = (uint8_t)vrad_rl;     
    CAN_LOG_DATA[13] = (uint8_t)(vrad_rl>>8);                      // 7
    CAN_LOG_DATA[14] = (uint8_t)vrad_rr;      
    CAN_LOG_DATA[15] = (uint8_t)(vrad_rr>>8);                      // 8
#if (__YAW_OVERSHOOT_MODEL==ENABLE)
    CAN_LOG_DATA[16] = (uint8_t)ldesps16YawRfDynModel;
    CAN_LOG_DATA[17] = (uint8_t)(ldesps16YawRfDynModel>>8);                         // 9
#else
    CAN_LOG_DATA[16] = (uint8_t)0;
    CAN_LOG_DATA[17] = (uint8_t)(0>>8);                         // 9
#endif
    CAN_LOG_DATA[18] = (uint8_t)vref5;
    CAN_LOG_DATA[19] = (uint8_t)(vref5>>8);                        //10
    CAN_LOG_DATA[20] = (uint8_t)FL.pwm_duty_temp;                  //11
    CAN_LOG_DATA[21] = (uint8_t)FR.pwm_duty_temp;                  //12 
    CAN_LOG_DATA[22] = (uint8_t)RL.pwm_duty_temp;                  //13
    CAN_LOG_DATA[23] = (uint8_t)RR.pwm_duty_temp;                  //14

    CAN_LOG_DATA[24] = (uint8_t)(cal_torq/10);                     //15            
    CAN_LOG_DATA[25] = (uint8_t)(drive_torq/10);                   //16            
    CAN_LOG_DATA[26] = (uint8_t)(eng_torq/10);                     //17            
    CAN_LOG_DATA[27] = (uint8_t)mtp;                               //18            
#if (__BANK_DFC_CHK == ENABLE)
    CAN_LOG_DATA[28] = (uint8_t)ldesps16BankAlatRollGain;          //19
    CAN_LOG_DATA[29] = (uint8_t)ldesps16BankYawFilter;             //20
#else
    CAN_LOG_DATA[28] = (uint8_t)FL.flags;                          //19
    CAN_LOG_DATA[29] = (uint8_t)FR.flags;                          //20
#endif
#if (__BANK_DFC_CHK == ENABLE)
    CAN_LOG_DATA[30] = (uint8_t)(((ldesps16VrefMulYawmBank/50)>127)?127:(((ldesps16VrefMulYawmBank/50)<-127)?-127:(int8_t)(ldesps16VrefMulYawmBank/50)));                 //21
    CAN_LOG_DATA[31] = (uint8_t)(((ldesps16alatLPF3Bank/50)>127)?127:(((ldesps16alatLPF3Bank/50)<-127)?-127:(int8_t)(ldesps16alatLPF3Bank/50)));                          //22
#else
    CAN_LOG_DATA[30] = (uint8_t)RL.flags;                          //21
    CAN_LOG_DATA[31] = (uint8_t)RR.flags;                          //22
#endif

    CAN_LOG_DATA[32] = (uint8_t)(esp_mu/10);                       //23
    CAN_LOG_DATA[33] = (uint8_t)(Beta_Mu/10);                      //24   
#if (__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
   	if (lsespu1MpresByAHBgen3Invalid==0)
   	{
   		CAN_LOG_DATA[34] = (uint8_t)(lsesps16MpressByAHBgen3/10);  //25
   	}
   	else if( (fu1PedalErrDet==0)&&(lsespu1BrkAppSenInvalid==0) )
   	{
   		CAN_LOG_DATA[34] = (uint8_t)(lsesps16EstBrkPressByBrkPedalF/10);  //25
   	}
   	else
   	{
   		CAN_LOG_DATA[34] = (uint8_t)(0);
   	}
#else
    CAN_LOG_DATA[34] = (uint8_t)(mpress/10);                       //25
#endif
    CAN_LOG_DATA[35] = (uint8_t)(((Bank_Angle_Yaw/50)>127)?127:(((Bank_Angle_Yaw/50)<-127)?-127:(int8_t)(Bank_Angle_Yaw/50))); //26      
    CAN_LOG_DATA[36] = (uint8_t)(((Bank_Angle_Ay/50)>127)?127:(((Bank_Angle_Ay/50)<-127)?-127:(int8_t)(Bank_Angle_Ay/50)));    //27           
    CAN_LOG_DATA[37] = (uint8_t)(((Bank_Angle_K/50)>127)?127:(((Bank_Angle_K/50)<-127)?-127:(int8_t)(Bank_Angle_K/50)));       //28
    CAN_LOG_DATA[38] = (uint8_t)(INHIBITION_INDICATOR()); //29                
    CAN_LOG_DATA[39] = (uint8_t)Linear_Gain_F;                     //30   

    CAN_LOG_DATA[40] = (uint8_t)rf2;                             
    CAN_LOG_DATA[41] = (uint8_t)(rf2>>8); //limited_yaw_C          //31  
    CAN_LOG_DATA[42] = (uint8_t)delta_moment_q;               
    CAN_LOG_DATA[43] = (uint8_t)(delta_moment_q>>8);               //32    
    CAN_LOG_DATA[44] = (uint8_t)nosign_delta_yaw_first;               
    CAN_LOG_DATA[45] = (uint8_t)(nosign_delta_yaw_first>>8);       //33     
    CAN_LOG_DATA[46] = (uint8_t)nosign_delta_yaw;    
    CAN_LOG_DATA[47] = (uint8_t)(nosign_delta_yaw>>8);             //34       
    
    CAN_LOG_DATA[48] = (uint8_t)(((sign_o_delta_yaw_thres/10)>127)?127:(((sign_o_delta_yaw_thres/10)<-127)?-127:(int8_t)(sign_o_delta_yaw_thres/10))); //35
    CAN_LOG_DATA[49] = (uint8_t)(((sign_u_delta_yaw_thres/10)>127)?127:(((sign_u_delta_yaw_thres/10)<-127)?-127:(int8_t)(sign_u_delta_yaw_thres/10))); //36
    CAN_LOG_DATA[50] = (uint8_t)(det_alatc_fltr/10);                         //37
    CAN_LOG_DATA[51] = (uint8_t)(det_alatm_fltr/10);                         //38

#if (__BANK_ESC_IMPROVEMENT_CONCEPT == ENABLE)
    CAN_LOG_DATA[52] = (uint8_t)(ldesps16BankLowerSuspectCounter);                              //39
#else
	CAN_LOG_DATA[52] = (uint8_t)(0);                              //39
#endif

#if __4WD_DRIVE_TYPE==ENABLE
  if(lsu8DrvMode!=DM_2WD)
  {  
    CAN_LOG_DATA[53] = (uint8_t)((ax_Filter>127)?127:((ax_Filter<-127)?-127:(int8_t)ax_Filter));            //40
  }
  else
  {
    CAN_LOG_DATA[53] = (uint8_t)((afz>127)?127:((afz<-127)?-127:(int8_t)afz));                              //40    
  }
#else
 #if __4WD
    CAN_LOG_DATA[53] = (uint8_t)((ax_Filter>127)?127:((ax_Filter<-127)?-127:(int8_t)ax_Filter));            //40    
 #else
    CAN_LOG_DATA[53] = (uint8_t)((afz>127)?127:((afz<-127)?-127:(int8_t)afz));                              //40    
 #endif
#endif    
    CAN_LOG_DATA[54] = (uint8_t)(((Beta_K_Dot2/10)>127)?127:(((Beta_K_Dot2/10)<-127)?-127:(int8_t)(Beta_K_Dot2/10)));                     //41
    CAN_LOG_DATA[55] = (uint8_t)(((Bank_Angle_K_Dot/50)>127)?127:(((Bank_Angle_K_Dot/50)<-127)?-127:(int8_t)(Bank_Angle_K_Dot/50)));   //42
    
    CAN_LOG_DATA[56] = (uint8_t)FL.u8_Estimated_Active_Press;      //43 
    CAN_LOG_DATA[57] = (uint8_t)FR.u8_Estimated_Active_Press;      //44
    CAN_LOG_DATA[58] = (uint8_t)RL.u8_Estimated_Active_Press;      //45
    CAN_LOG_DATA[59] = (uint8_t)RR.u8_Estimated_Active_Press;      //46
    CAN_LOG_DATA[60] = (uint8_t)Beta_MD;                            
    CAN_LOG_DATA[61] = (uint8_t)(Beta_MD>>8);                      //47   
    CAN_LOG_DATA[62] = (uint8_t)Beta_Dyn;
    CAN_LOG_DATA[63] = (uint8_t)(Beta_Dyn>>8);                     //48
    
    CAN_LOG_DATA[64] = (uint8_t)Alpha_Gain;       
    CAN_LOG_DATA[65] = (uint8_t)(Alpha_Gain>>8);                   //49
    CAN_LOG_DATA[66] = (uint8_t)Bank_Angle_Final;                       
    CAN_LOG_DATA[67] = (uint8_t)(Bank_Angle_Final>>8);             //50
    CAN_LOG_DATA[68] = (uint8_t)Dynamic_Factor;  
    CAN_LOG_DATA[69] = (uint8_t)(Dynamic_Factor>>8);               //51
    CAN_LOG_DATA[70] = (uint8_t)det_wstr_dot;  
    CAN_LOG_DATA[71] = (uint8_t)(det_wstr_dot>>8);              //52
    
    i=0;                                  
    if (BLS                             ==1) i|=0x01; 
    if (ABS_fz                          ==1) i|=0x02; 
    if (AFZ_OK                          ==1) i|=0x04; 
    if (EBD_RA                          ==1) i|=0x08; 
    if (BTC_fz                          ==1) i|=0x10; 
    if (YAW_CDC_WORK                    ==1) i|=0x20; 
    if (ETCS_ON                         ==1) i|=0x40; 
    if (ESP_TCS_ON                      ==1) i|=0x80; 

    CAN_LOG_DATA[72] = i;               //53
    
    i=0;                                  
    if (MOT_ON_FLG                      ==1) i|=0x01;
    if (Beta_Small_Sensor_Offset_Flag   ==1) i|=0x02;
#if (__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
	if (lsespu1MpresByAHBgen3Invalid    ==1) i|=0x04; 
#else   	
    if (LFC_Split_flag                  ==1) i|=0x04; 
#endif
#if (__BANK_ESC_IMPROVEMENT_CONCEPT == ENABLE)
    if (ldespu1BankLowerSuspectFlag     ==1) i|=0x08; 
#else
    if (0                               ==1) i|=0x08;
#endif
    if (FLAG_BETA_UNDER_DRIFT           ==1) i|=0x10; 
    if (ESP_ROUGH_ROAD                  ==1) i|=0x20; 
    if (Rough_road_suspect_vehicle      ==1) i|=0x40; 
    if (Rough_road_detect_vehicle       ==1) i|=0x80;               

    CAN_LOG_DATA[73] = i;               //54    

    i = 0;                                   
    if (ABS_fl                          ==1) i|=0x01;
    if (FL.L_SLIP_CONTROL               ==1) i|=0x02;
    if (STABIL_fl                       ==1) i|=0x04;
    if (SPOLD_RESET_fl                  ==1) i|=0x08;
    if (ESP_BRAKE_CONTROL_FL            ==1) i|=0x10; 
    if (GMA_SLIP_fl                     ==1) i|=0x20;
    if (HV_VL_fl                        ==1) i|=0x40;
    if (AV_VL_fl                        ==1) i|=0x80;

    CAN_LOG_DATA[74] = i;               //55
    
    i=0;                                          
    if (ABS_fr                          ==1) i|=0x01; 
    if (FR.L_SLIP_CONTROL               ==1) i|=0x02; 
    if (STABIL_fr                       ==1) i|=0x04; 
    if (SPOLD_RESET_fr                  ==1) i|=0x08; 
    if (ESP_BRAKE_CONTROL_FR            ==1) i|=0x10; 
    if (GMA_SLIP_fr                     ==1) i|=0x20; 
    if (HV_VL_fr                        ==1) i|=0x40; 
    if (AV_VL_fr                        ==1) i|=0x80; 

    CAN_LOG_DATA[75] = i;               //56
    
    i=0;                                   
    if (ABS_rl                          ==1) i|=0x01;             
    if (RL.L_SLIP_CONTROL               ==1) i|=0x02;             
    if (STABIL_rl                       ==1) i|=0x04;             
    if (SPOLD_RESET_rl                  ==1) i|=0x08;           
    if (ESP_BRAKE_CONTROL_RL            ==1) i|=0x10; 
    if (RL.MSL_BASE                     ==1) i|=0x20;
    if (HV_VL_rl                        ==1) i|=0x40;
    if (AV_VL_rl                        ==1) i|=0x80;             

    CAN_LOG_DATA[76] = i;               //57

    i = 0;
    if(ABS_rr                           ==1) i|=0x01;
    if(RR.L_SLIP_CONTROL                ==1) i|=0x02;
    if(STABIL_rr                        ==1) i|=0x04;
    if(SPOLD_RESET_rr                   ==1) i|=0x08;
    if(ESP_BRAKE_CONTROL_RR             ==1) i|=0x10;           
    if(RR.MSL_BASE                      ==1) i|=0x20;         
    if(HV_VL_rr                         ==1) i|=0x40;       
    if(AV_VL_rr                         ==1) i|=0x80;          

    CAN_LOG_DATA[77] = i;               // 58

    i=0;
    if(TCL_DEMAND_fl                    ==1) i|=0x01;   
    if(TCL_DEMAND_fr                    ==1) i|=0x02;   
    if(S_VALVE_LEFT                     ==1) i|=0x04;
    if(S_VALVE_RIGHT                    ==1) i|=0x08;   
    if(Beta_Sensor_Offset_Suspect_Flag  ==1) i|=0x10;   
    if(Beta_Sensor_Offset_Detect_Flag   ==1) i|=0x20;   
    if(Beta_Bank_Susp_Flag              ==1) i|=0x40;   
    if(Beta_Bank_Comp_Flag              ==1) i|=0x80;   
    
    CAN_LOG_DATA[78] = i;               // 59
    
     i=0;
    if(INITIAL_BRAKE                    ==1) i|=0x01;   
    if(PARTIAL_BRAKE                    ==1) i|=0x02;   
    if(Bank_Susp_Flag                   ==1) i|=0x04; 
    if(Bank_Comp_Flag                   ==1) i|=0x08;   
    if(Beta_Mu_Saturation               ==1) i|=0x10;   
    if(Beta_Reset_Need_Flag             ==1) i|=0x20;   
    if(MedLowMu_Suspect_Flag            ==1) i|=0x40; //   FLAG_BANK_SUSPECT 
    if(MedLowMu_Detect_BySlip_Flag      ==1) i|=0x80; //   FLAG_BANK_DETECTED 

    CAN_LOG_DATA[79] = i;               // 60
}

#endif
#endif
#endif
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LSESPCallBetaLogData
	#include "Mdyn_autosar.h"
#endif
