#ifndef __LSABS2NDORDERLPF_H__
#define __LSABS2NDORDERLPF_H__

/*Includes *********************************************************************/
#include "LVarHead.h"
/*Global MACRO CONSTANT Definition *********************************************/


/*Global Extern Variable Declaration *******************************************/

struct ACC_FILTER {
       
       int16_t    lsesps16SignalInp;
       int16_t    lsesps16DeltalInp;       
       int16_t    lsesps16DeltalInpFilter;             
       int16_t    lsesps16SignalFilter;       
       int16_t    lsesps16SignalFilterRest;
       int16_t    lsesps16SignalFilterAccel;          
       int16_t    lsesps16SignalAccel;             
	
};
	 
extern struct	ACC_FILTER	*ACC_CALC;

/*Global Extern Functions Declaration ******************************************/
extern void	LDABS_vCalcEachFilterSignalAccel(int16_t rawdata, int16_t freq, int16_t zeta, int16_t deltaMulti, int16_t diffcoefCov);

#endif

