
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDESPCompDeltaYawDot
	#include "Mdyn_autosar.h"
#endif
#include "LVarHead.h"
#include "LCallMain.h"
#include "LCESPCalLpf.h"
#if __VDC
void LDESP_vCompDeltaYawDot(void);
void LDESP_vCompDeltaYaw2Dot(void);
void LDESP_vCompDeltaYawFirstDot(void);
void LDESP_vCompYawOutDot(void); 
void LDESP_vCompYawOutDot2(void); 


void LDESP_vCompDeltaYawDot(void)
{
    yaw_error_dot_old = yaw_error_dot;
    
    yaw_error_buf[3]=yaw_error_buf[2];
    yaw_error_buf[2]=yaw_error_buf[1];
    yaw_error_buf[1]=yaw_error_buf[0];
    yaw_error_buf[0]=delta_yaw;
    
  
    if(counter_deltayaw_signchange<2)
    {
        esp_tempW1 = (yaw_error_buf[0]+yaw_error_buf[2]);    /* r : 0.01*/       
    }
    else
    {
        esp_tempW1 = (yaw_error_buf[0]-yaw_error_buf[2]);               
    }
        
    if ( FLAG_DELTAYAW_SIGNCHANGE ==1) 
    {
    	esp_tempW2 = 100;        
    }
    else if ( (ESP_ROUGH_ROAD==1) || (ABS_fz==1) )      
    {
    	esp_tempW2 = L_U8FILTER_GAIN_10MSLOOP_3_5HZ;
    }
    else                       
    {
    	esp_tempW2 = L_U8FILTER_GAIN_10MSLOOP_8_5HZ;        
    }
  
    yaw_error_dot = LCESP_s16Lpf1Int( esp_tempW1, yaw_error_dot_old, (uint8_t)esp_tempW2);     

    /* Compute delta_yaw_dot          */
        
     /* delta_yaw_dot = (int16_t)( (((int32_t)yaw_error_dot)*100)/14);    // r : 0.1  3 Hz    */ 
    tempW2 = yaw_error_dot; tempW1 = 100; tempW0 = 20;
    s16muls16divs16();
    delta_yaw_dot = tempW3;
        
}

void LDESP_vCompDeltaYaw2Dot(void)  /* __MGH40_ESC_Concepts */
{ 
    yaw_error2_dot_old = yaw_error2_dot;
    
    yaw_error2_buf[3]=yaw_error2_buf[2];
    yaw_error2_buf[2]=yaw_error2_buf[1];
    yaw_error2_buf[1]=yaw_error2_buf[0];
    yaw_error2_buf[0]=delta_yaw2;
    
        esp_tempW1 = (yaw_error2_buf[0]-yaw_error2_buf[2]);   
        
    	esp_tempW2 = L_U8FILTER_GAIN_10MSLOOP_8_5HZ;       
  
    yaw_error2_dot = LCESP_s16Lpf1Int( esp_tempW1, yaw_error2_dot_old, (uint8_t)esp_tempW2);     

    /* Compute delta_yaw_dot          */
        
     /* delta_yaw_dot = (int16_t)( (((int32_t)yaw_error_dot)*100)/14);    // r : 0.1  3 Hz    */ 
    tempW2 = yaw_error2_dot; tempW1 = 100; tempW0 = 20;
    s16muls16divs16();
    delta_yaw2_dot = tempW3;
        
}

void LDESP_vCompDeltaYawFirstDot(void)
{

    /* Compute delta_under_yaw_dot          */ 
    yaw_error_under_dot_old = yaw_error_under_dot;

    yaw_error_under_buf[3]=yaw_error_under_buf[2];
    yaw_error_under_buf[2]=yaw_error_under_buf[1];
    yaw_error_under_buf[1]=yaw_error_under_buf[0];
    yaw_error_under_buf[0]=delta_yaw_first;

    if(counter_deltayaw_signchange<2) 
    {
        esp_tempW1=(yaw_error_under_buf[0]+yaw_error_under_buf[2]);     /* r: 0.01 ( *0.007 )*/    
    } 
	else 
	{
        esp_tempW1=(yaw_error_under_buf[0]-yaw_error_under_buf[2]);     /*  r: 0.01*/
    }
            
    if ( FLAG_DELTAYAW_SIGNCHANGE==1)
    {    	
        esp_tempW2 = 100;        
    }
    else if ( (ESP_ROUGH_ROAD==1) ||( ABS_fz==1) )
    {
        esp_tempW2 = L_U8FILTER_GAIN_10MSLOOP_3_5HZ; 
    }
    else
    {
        esp_tempW2 = L_U8FILTER_GAIN_10MSLOOP_8_5HZ;
    }
        
    yaw_error_under_dot = LCESP_s16Lpf1Int( esp_tempW1, yaw_error_under_dot_old, (uint8_t)esp_tempW2);     
    
    tempW2 = yaw_error_under_dot; tempW1 = 100; tempW0 = 20;
    s16muls16divs16();
    delta_yaw_first_dot = tempW3;
    
}
void LDESP_vCompYawOutDot(void)
{
    yaw_out_dot_old = yaw_out_dot;
    
    yaw_out_buf[3]=yaw_out_buf[2];
    yaw_out_buf[2]=yaw_out_buf[1];
    yaw_out_buf[1]=yaw_out_buf[0];
    yaw_out_buf[0]=McrAbs(yaw_out);
    
  
    if(counter_deltayaw_signchange<2)
    {
        esp_tempW1 = (yaw_out_buf[0]+yaw_out_buf[2]);    /* r : 0.01*/       
    }
    else
    {
        esp_tempW1 = (yaw_out_buf[0]-yaw_out_buf[2]);               
    }
        
    if ( FLAG_DELTAYAW_SIGNCHANGE ==1) 
    {
    	esp_tempW2 = 100;        
    }
    else if ( (ESP_ROUGH_ROAD==1) || (ABS_fz==1) )      
    {
    	esp_tempW2 = L_U8FILTER_GAIN_10MSLOOP_3_5HZ;
    }
    else                       
    {
    	esp_tempW2 = L_U8FILTER_GAIN_10MSLOOP_8_5HZ;        
    }
  
    yaw_out_dot = LCESP_s16Lpf1Int( esp_tempW1, yaw_out_dot_old, (uint8_t)esp_tempW2);     
        
}

void LDESP_vCompYawOutDot2(void)  /* __MGH40_ESC_Concepts */
{ 
    yaw_out_dot2_old = yaw_out_dot2;
    
    yaw_out_buf2[3]=yaw_out_buf2[2];
    yaw_out_buf2[2]=yaw_out_buf2[1];
    yaw_out_buf2[1]=yaw_out_buf2[0];
    yaw_out_buf2[0]=yaw_out;
    
    esp_tempW1 = (yaw_out_buf2[0]-yaw_out_buf2[2]);
        
    if ( (ESP_ROUGH_ROAD==1) || (ABS_fz==1) )      
    {
    	esp_tempW2 = L_U8FILTER_GAIN_10MSLOOP_3_5HZ;
    }
    else                       
    {
    	esp_tempW2 = L_U8FILTER_GAIN_10MSLOOP_8_5HZ;        
    }
  
    yaw_out_dot2 = LCESP_s16Lpf1Int( esp_tempW1, yaw_out_dot2_old, (uint8_t)esp_tempW2);     
        
}
#endif
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDESPCompDeltaYawDot
	#include "Mdyn_autosar.h"
#endif
