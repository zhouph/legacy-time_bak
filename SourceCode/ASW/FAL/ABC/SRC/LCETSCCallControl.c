
/* Includes ******************************************************************/



/* AUTOSAR --------------------------*/

/* AUTOSAR --------------------------*/
#include "LCESPCalInterpolation.h"
#include "LCETSCCallControl.h"
#include "LCTCSCallControl.h"

/* Local Definiton  **********************************************************/


/* Variables Definition*******************************************************/
U8_BIT_STRUCT_t ETSC_FLAG01, ETSC_FLAG02;
int16_t	letscs16_AvailableEngTorq;
int16_t	letscs16_PcntOfAvailTorq, letscs16_PcntOfAvailTorqAtSpdP1, letscs16_PcntOfAvailTorqAtSpdP2, letscs16_PcntOfAvailTorqAtSpdP3;
int16_t	letscs16_MaxAllowEngTorq;
int16_t letscs16_EngTorqStrCtlEndTimer;
int16_t letscs16_EngTorqStrCtlCmd;



/* LocalFunction prototype ***************************************************/
void LDETSC_vCallDetection(void);
void LCETSC_vCallControl(void);
static void LDETSC_vCalAvailableEngTorq(void);
static void LDETSC_vCalDsirdPcntOfAvailTorq(void);
static void LDETSC_vCalMaxAllowEngTorq(void);
static void LDETSC_vChkCtlInhibitCondition(void);
static void LDETSC_vSetEngTorqStrCtlFadeOut(void);
static void LDETSC_vActEngTorqStrCtl(void);
static void LDETSC_vSetEngTorqStrCtlFadeIn(void);
static void LDETSC_vEndEngTorqStrCtl(void);
static void LCETSC_vCalEngTorgCmd(void);
void LCETSC_vCordEngTorqCmd(void);

/* GlobalFunction prototype **************************************************/


/* Implementation*************************************************************/

void LDETSC_vCallDetection(void)
{
	LDETSC_vChkCtlInhibitCondition();
	LDETSC_vCalAvailableEngTorq();
	LDETSC_vCalDsirdPcntOfAvailTorq();
	LDETSC_vCalMaxAllowEngTorq();
	LDETSC_vActEngTorqStrCtl();
	LDETSC_vSetEngTorqStrCtlFadeIn();
	LDETSC_vSetEngTorqStrCtlFadeOut();	
	LDETSC_vEndEngTorqStrCtl();
}

void LCETSC_vCallControl(void)
{
	LCETSC_vCalEngTorgCmd();
	LCETSC_vCordEngTorqCmd();
}

static void LDETSC_vCalAvailableEngTorq(void)
{
	letscs16_AvailableEngTorq = LCESP_s16IInter7Point((int16_t)eng_rpm, S16_ETSC_ENG_RPM_P1,S16_ETSC_AVAIL_ENG_TORQ_P1,
																		S16_ETSC_ENG_RPM_P2,S16_ETSC_AVAIL_ENG_TORQ_P2,
																		S16_ETSC_ENG_RPM_P3,S16_ETSC_AVAIL_ENG_TORQ_P3,
																		S16_ETSC_ENG_RPM_P4,S16_ETSC_AVAIL_ENG_TORQ_P4,
																		S16_ETSC_ENG_RPM_P5,S16_ETSC_AVAIL_ENG_TORQ_P5,
																		S16_ETSC_ENG_RPM_P6,S16_ETSC_AVAIL_ENG_TORQ_P6,
																		S16_ETSC_ENG_RPM_P7,S16_ETSC_AVAIL_ENG_TORQ_P7);
}

static void LDETSC_vCalDsirdPcntOfAvailTorq(void)
{
	int16_t s16ETSC_temp01 = MacroFunctionAbs(wstr);
	letscs16_PcntOfAvailTorqAtSpdP1=LCESP_s16IInter7Point(s16ETSC_temp01,S16_ETSC_STR_ANG_P1, S16_ETSC_PCNT_AVAIL_TORQ_V1_SA1,
										 					             S16_ETSC_STR_ANG_P2, S16_ETSC_PCNT_AVAIL_TORQ_V1_SA2,
															             S16_ETSC_STR_ANG_P3, S16_ETSC_PCNT_AVAIL_TORQ_V1_SA3,
															             S16_ETSC_STR_ANG_P4, S16_ETSC_PCNT_AVAIL_TORQ_V1_SA4,
															             S16_ETSC_STR_ANG_P5, S16_ETSC_PCNT_AVAIL_TORQ_V1_SA5,
															             S16_ETSC_STR_ANG_P6, S16_ETSC_PCNT_AVAIL_TORQ_V1_SA6,
															             S16_ETSC_STR_ANG_P7, S16_ETSC_PCNT_AVAIL_TORQ_V1_SA7);
			 													   
	letscs16_PcntOfAvailTorqAtSpdP2=LCESP_s16IInter7Point(s16ETSC_temp01,S16_ETSC_STR_ANG_P1, S16_ETSC_PCNT_AVAIL_TORQ_V2_SA1,
															             S16_ETSC_STR_ANG_P2, S16_ETSC_PCNT_AVAIL_TORQ_V2_SA2,
															             S16_ETSC_STR_ANG_P3, S16_ETSC_PCNT_AVAIL_TORQ_V2_SA3,
															             S16_ETSC_STR_ANG_P4, S16_ETSC_PCNT_AVAIL_TORQ_V2_SA4,
															             S16_ETSC_STR_ANG_P5, S16_ETSC_PCNT_AVAIL_TORQ_V2_SA5,
															             S16_ETSC_STR_ANG_P6, S16_ETSC_PCNT_AVAIL_TORQ_V2_SA6,
															             S16_ETSC_STR_ANG_P7, S16_ETSC_PCNT_AVAIL_TORQ_V2_SA7);
															 
	letscs16_PcntOfAvailTorqAtSpdP3=LCESP_s16IInter7Point(s16ETSC_temp01,S16_ETSC_STR_ANG_P1, S16_ETSC_PCNT_AVAIL_TORQ_V3_SA1,
															             S16_ETSC_STR_ANG_P2, S16_ETSC_PCNT_AVAIL_TORQ_V3_SA2,
															             S16_ETSC_STR_ANG_P3, S16_ETSC_PCNT_AVAIL_TORQ_V3_SA3,
															             S16_ETSC_STR_ANG_P4, S16_ETSC_PCNT_AVAIL_TORQ_V3_SA4,
															             S16_ETSC_STR_ANG_P5, S16_ETSC_PCNT_AVAIL_TORQ_V3_SA5,
															             S16_ETSC_STR_ANG_P6, S16_ETSC_PCNT_AVAIL_TORQ_V3_SA6,
															             S16_ETSC_STR_ANG_P7, S16_ETSC_PCNT_AVAIL_TORQ_V3_SA7);
															 
		
	letscs16_PcntOfAvailTorq = LCESP_s16IInter3Point(vref, S16_ETSC_SPEED_P1, letscs16_PcntOfAvailTorqAtSpdP1,
														   S16_ETSC_SPEED_P2, letscs16_PcntOfAvailTorqAtSpdP2,
														   S16_ETSC_SPEED_P3, letscs16_PcntOfAvailTorqAtSpdP3);
}

static void LDETSC_vCalMaxAllowEngTorq(void)
{
	letscs16_MaxAllowEngTorq = (int16_t)((int32_t)letscs16_AvailableEngTorq * letscs16_PcntOfAvailTorq / 100);
}

static void LDETSC_vChkCtlInhibitCondition(void)
{
	letscu1_EngTorqStrCtlInhibitOld = letscu1_EngTorqStrCtlInhibit;
	/* Engine Torque Steer Control inhibition condition check */
	if 	( 
			(fu1WheelFLErrDet==1)||(fu1WheelFRErrDet==1)||(fu1WheelRLErrDet==1)||(fu1WheelRRErrDet==1)	/* Wheel speed sensor error */
		 || (fu1ECUHwErrDet==1)																			/* ECU hardware error */
		 || (fu1VoltageLowErrDet==1)||(fu1VoltageUnderErrDet==1)||(fu1VoltageOverErrDet==1)				/* Abnormal voltage error */
		 ||	(fu8EngineModeStep==SLEEP_MODE)||(fu8EngineModeStep==ENGINE_INACTIVE_RUN_MODE)				/* Engine is not runing */
		 || (fu1MainCanLineErrDet==1)																	/* Main CAN error */
		/*
		 || 0x0C9 error ����(EngineRPM)
		 || 0x1C3 error ����(EngineActualTorque)
		 || EngineActualTorqueInvalid
		 || ECM BUS off
		 || SAS BUS OFF
		 || SteeringAngleInvalid
		*/
		 || (S16_ETSC_FEATURE_ENABLE==ETSC_FEATURE_DISABLED)
		 || (eng_rpm < S16_ETSC_DISABLE_ENG_RPM)
		)
	{
		letscu1_EngTorqStrCtlInhibit = ETSC_INHIBITED;
	}
	else
	{
		letscu1_EngTorqStrCtlInhibit = ETSC_ALLOWED;
	}
}

static void LDETSC_vActEngTorqStrCtl(void)
{
	letscu1_EngTorqStrCtlStatusOld = letscu1_EngTorqStrCtlStatus;
	if	(letscu1_EngTorqStrCtlStatus == ETSC_NOT_ACTIVE)
	{
		if (letscu1_EngTorqStrCtlInhibit == ETSC_ALLOWED)
		{
			if (eng_torq > letscs16_MaxAllowEngTorq)
			{
				letscu1_EngTorqStrCtlStatus = ETSC_ACTIVE;
			}
			else
			{}
		}
		else 
		{}
		letscs16_EngTorqStrCtlEndTimer = 0;
	}
	else
	{
	}
}	

static void LDETSC_vSetEngTorqStrCtlFadeIn(void)
{
	if 		( (letscu1_EngTorqStrCtlStatusOld == ETSC_NOT_ACTIVE) && (letscu1_EngTorqStrCtlStatus == ETSC_ACTIVE) )
	{
		letscu1_EngTorqStrCtlFadeIn = ETSC_ACTIVE;
	}
	else if ( (letscu1_EngTorqStrCtlInhibit == ETSC_INHIBITED) ||
			  (letscs16_EngTorqStrCtlCmd <= letscs16_MaxAllowEngTorq)
			)
	{
		letscu1_EngTorqStrCtlFadeIn = ETSC_NOT_ACTIVE;
	}
	else
	{
	}
}

static void LDETSC_vSetEngTorqStrCtlFadeOut(void)
{
	if 		( (letscu1_EngTorqStrCtlStatus == ETSC_ACTIVE) && 
		      (letscu1_EngTorqStrCtlInhibitOld == ETSC_ALLOWED) && (letscu1_EngTorqStrCtlInhibit == ETSC_INHIBITED)
		    )
	{
		letscu1_EngTorqStrCtlFadeOut = ETSC_ACTIVE;
	}
	else if ( (letscu1_EngTorqStrCtlStatus == ETSC_NOT_ACTIVE) ||
			  (letscs16_EngTorqStrCtlCmd >= drive_torq)
			)
	{
		letscu1_EngTorqStrCtlFadeOut = ETSC_NOT_ACTIVE;
	}
	else
	{
	}
}

static void LDETSC_vEndEngTorqStrCtl(void)
{
	if (letscu1_EngTorqStrCtlStatus == ETSC_ACTIVE)
	{
		if ( (letscu1_EngTorqStrCtlInhibit == ETSC_INHIBITED) && (letscu1_EngTorqStrCtlFadeOut == ETSC_NOT_ACTIVE) )
		{
			letscu1_EngTorqStrCtlStatus = ETSC_NOT_ACTIVE;
			letscs16_EngTorqStrCtlEndTimer = 0;
		}
		else if ( (eng_torq <= (letscs16_MaxAllowEngTorq-S16_ETSC_ALLOWABLE_ENG_TRQ_OFFSET)) ||
			      (drive_torq <=letscs16_MaxAllowEngTorq)
			    )
		{
			letscs16_EngTorqStrCtlEndTimer = letscs16_EngTorqStrCtlEndTimer + 1;
			if (letscs16_EngTorqStrCtlEndTimer > S16_ETSC_END_TIMER)
			{
				letscu1_EngTorqStrCtlStatus = ETSC_NOT_ACTIVE;
				letscs16_EngTorqStrCtlEndTimer = 0;
			}
			else
			{
			}
		}
		else
		{
			letscs16_EngTorqStrCtlEndTimer = 0;			
		}
	}
	else
	{
	}
}

static void LCETSC_vCalEngTorgCmd(void)
{
	if (letscu1_EngTorqStrCtlStatus == ETSC_ACTIVE)
	{
		if ( (letscu1_EngTorqStrCtlStatusOld == ETSC_NOT_ACTIVE) && (letscu1_EngTorqStrCtlStatus == ETSC_ACTIVE) )
		{
			letscs16_EngTorqStrCtlCmd = eng_torq;
		}
		else if (letscu1_EngTorqStrCtlFadeIn==ETSC_ACTIVE)
		{
			letscs16_EngTorqStrCtlCmd = letscs16_EngTorqStrCtlCmd - S16_ETSC_TORQ_DEC_RATE_FADE_IN;
		}
		else if (letscu1_EngTorqStrCtlFadeOut==ETSC_ACTIVE)
		{
			letscs16_EngTorqStrCtlCmd = letscs16_EngTorqStrCtlCmd + S16_ETSC_TORQ_INC_RATE_FADE_OUT;
		}
		else
		{
			letscs16_EngTorqStrCtlCmd = letscs16_MaxAllowEngTorq;
		}
	}
	else
	{
		letscs16_EngTorqStrCtlCmd = S16_ETSC_MAX_TORQ_IN_SPEC;
	}
}

void LCETSC_vCordEngTorqCmd(void)
{
	cal_torq =LCTCS_s16IFindMinimum(cal_torq, letscs16_EngTorqStrCtlCmd);
}
