/******************************************************************************
* Project Name: Hydraulic Rear Wheel Boost control
* File: LCHRBCallControl.C
* Description: Hydraulic Rear Wheel Boost Controller
* Date: Dec. 11. 2006
******************************************************************************/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCHRBCallControl
	#include "Mdyn_autosar.h"
#endif
/* Includes ******************************************************************/
#include "LCHRBCallControl.h"
#include "LAABSCallMotorSpeedControl.h"
#include "LCABSDecideCtrlState.h"
#include "LDFBCCallDetection.h"


#if __HRB
/* Logcal Definiton  *********************************************************/
#define HRB_ENTER_PRESS   MPRESS_80BAR
#define HRB_ENTER_SLIP    LAM_3P

/* Variables Definition*******************************************************/

struct	U8_BIT_STRUCT_t HRBF0;
struct	U8_BIT_STRUCT_t HRBF1;

 

int8_t	lchrbs8CtrlMode;
/*int16_t     lchrbs16ApplyCnt;*/
int16_t    lchrbs16ApplyCntP;
int16_t    lchrbs16ApplyCntS;
int8_t	    lchrbs8ExitCnt;

uint8_t    lchrbu8MpThres;
uint8_t    lchrbu8dPThres;

/*int16_t    lchrbs16RearPressAtABS;*/
int16_t    lchrbs16EnterMpress;
int16_t    lchrbs16MpressSlope;
int16_t    lchrbs16FrontSkidPress;
int16_t    lchrbs16RearSkidPress;
int16_t    lchrbs16AssistPressure;
int16_t    lchrbs16TargetRearPress;
int16_t    lchrbs16MPAdaptation;
int16_t    lchrbs16SlipAdaptation;

int16_t    lchrbs16PriorMP;
int8_t     lchrbs8PriorSlip;

uint8_t    lchrbu8FirstApplyCnt;
uint8_t    lchrbu8PressReleaseCnt;
uint8_t    lchrbu8BrakeReleaseCnt;
int8_t     lchrbs8MPreleasingCnt;
int16_t    lchrbs16EnterRearPress;
uint8_t    lchrbu8LowSlipTim;

int16_t 	lchrbs16NeedBrkFluidCntP;
int16_t 	lchrbs16NeedBrkFluidCntS;
uint8_t    lchrbu8PulseUpHoldScans;

uint8_t    lchrbu8temp0,lchrbu8temp1;
int8_t 	lchrbs8temp0,lchrbs8temp1,lchrbs8temp2,lchrbs8temp3,lchrbs8temp4;

int8_t 	HRB_Slip_Check;

 #if __ADVANCED_MSC
int16_t hrb_msc_target_vol;
 #endif  

/* LocalFunction prototype ***************************************************/

static void	LCHRB_vChkInhitibion(void);
static void LCHRB_vDecideHRBCtrlState(void);
static void	LCHRB_vDecideExitHRB(void);
static void	LCHRB_vDecideHRBEnter(void);
static void LCHRB_vDecideEndRelease(void);
static void LCHRB_vCalcInitTargetP(void);
static void LCHRB_vCalcTargetP(void);
static void LCHRB_vCalcPReleaseRate(void);
static void	LCHRB_vPressModeRELEASE(void); 
static void	LCHRB_vPressModeBOOST(void);
static void	LCHRB_vPressModeHOLD(void);
static void	LCHRB_reset(void);
static void	LCHRB_vDctBrakeRelease(void);
static void	LCHRB_vCalcHRBPulseUpRate(void);
static void	LCHRB_vCalcEnterMPThres(void);
static void	LCHRB_vCalMPSlope(void);
static void	LCHRB_vCalcEnterdPThres(void);
static void	LCHRB_vDctReducingBrkInput(void);

void LCHRB_vResetCtrl(void);

/* Implementation*************************************************************/




/******************************************************************************
* FUNCTION NAME:      LCHRB_vCallControl
* CALLED BY:          LC_vCallMainControl()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        HRB controller main function
******************************************************************************/
void 	LCHRB_vCallControl(void)
{
	LCHRB_vChkInhitibion();
	LCHRB_vCalMPSlope();
	
	switch (lchrbs8CtrlMode)
	{
		/*-------------------------------------------------------------------------- */
		case S8_HRB_READY:
			if(lchrbu1Inhibit==1)
			{
				lchrbs8CtrlMode = S8_INHIBITION;
			}
			else {
				LCHRB_vDecideHRBEnter();
				if(lchrbu1EnterCtrl==1)
				{
                    lchrbs8CtrlMode = S8_HRB_PULSEUP; /* Pulse-up Rise Before Brake Boost */
                    lchrbu1ActiveFlag = 1;
                    
                    LCHRB_vCalcHRBPulseUpRate();
					
                    lchrbu1EnterCtrl = 0;
                }
                else { }
            }
            break;
        /*-------------------------------------------------------------------------- */
        case S8_HRB_PULSEUP: /* Wheel v/v actuation only */
            LCHRB_vDecideExitHRB(); /* HRB exit condition */
            LCHRB_vDctReducingBrkInput();

            if(lchrbu1ExitCtrl==1)
            {
                lchrbs8CtrlMode = S8_HRB_RELEASE;
                lchrbu1ExitCtrl = 0;
            }
            else
            {
                LCHRB_vCalcHRBPulseUpRate();

                if((lchrbu1StartBoost==1) && (lchrbu1MPreleasingState==0))
                {
					lchrbs8CtrlMode = S8_HRB_BOOST;
					
					LCHRB_vCalcInitTargetP();
					LCHRB_vPressModeBOOST();
					
                    lchrbu1StartBoost = 0;
				}
				else { }
			}
			break;
		/*-------------------------------------------------------------------------- */
		case S8_HRB_BOOST:
			LCHRB_vDecideExitHRB(); /* HRB exit condition */
			LCHRB_vDctReducingBrkInput();
			if(lchrbu1ExitCtrl==1)
			{
				lchrbs8CtrlMode = S8_HRB_RELEASE;
				lchrbu1ExitCtrl = 0;
				LCHRB_vCalcPReleaseRate();
				LCHRB_vPressModeRELEASE();
			}
			else
			{			    
			    LCHRB_vCalcTargetP();

			    if(lchrbu1MPreleasingState==1)
			    {
			        LCHRB_vPressModeHOLD();
			    }
			    else
			    {
			        LCHRB_vPressModeBOOST();
			    }
			}
			break;
		/*-------------------------------------------------------------------------- */
		case S8_HRB_RELEASE:			
			LCHRB_vDecideEndRelease();

            if(lchrbu1EndCtrl==1)
            {
                lchrbs8CtrlMode = S8_HRB_READY;
				LCHRB_reset();
            }
            else
            {
                lchrbs8CtrlMode = S8_HRB_RELEASE;
                LCHRB_vPressModeRELEASE();
            }

			break;	
		/*-------------------------------------------------------------------------- */
		case S8_INHIBITION:
			if(lchrbu1Inhibit==0)
			{
				lchrbs8CtrlMode = S8_HRB_READY;
			}
			else { }
			break;	

		/*-------------------------------------------------------------------------- */
		default:
			if(lchrbu1Inhibit==0)
			{
				lchrbs8CtrlMode = S8_HRB_READY;
			}
			else { }
			break;
	}	
}

/******************************************************************************
* FUNCTION NAME:      LCHRB_vChkInhitibion
* CALLED BY:          LCHRB_vCallControl()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        Check Inhibition of HRB Control
******************************************************************************/
static void	LCHRB_vChkInhitibion(void) 
{
  
  #if __GM_FailM	
	if((fu1MCPErrorDet==1) || (fu1ESCEcuHWErrDet==1) 
	  || (fu1WheelFLErrDet==1) || (fu1WheelFRErrDet==1) || (fu1WheelRLErrDet==1) || (fu1WheelRRErrDet==1)   /* WSS ERROR */
	  ||(fu1OnDiag==1)||(init_end_flg==0)
	 #if __BRAKE_FLUID_LEVEL
	  ||(fu1DelayedBrakeFluidLevel==1)
      ||(fu1LatchedBrakeFluidLevel==1)	/* 091030 for Bench Test */				  
     #endif
	)
	{
		lchrbu1Inhibit=1;
	}
    #if __INHIBIT_HRB_FOR_IGN_OFFON
	else if(fu8EngineModeStep<=1)
	{
	    lchrbu1Inhibit=1;
	}
    #endif
  #else
    if((ESP_ERROR_FLG==1)||(fu1OnDiag==1)||(init_end_flg==0))   
    {
    	lchrbu1Inhibit=1;
    }
  #endif
	else 
	{
		lchrbu1Inhibit=0;
	}
}

/******************************************************************************
* FUNCTION NAME:      LCHRB_vDecideExitHRB
* CALLED BY:          LCHRB_vCallControl()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        Check Exit conditions of HRB control
******************************************************************************/
static void	LCHRB_vDecideExitHRB(void)
{
    if(lchrbs16RearSkidPress==0)
    {
        if((ABS_rl==1) && (ABS_rr==1))
        {
            if(RR.s16_Estimated_Active_Press > RL.s16_Estimated_Active_Press)
            {
                lchrbs16RearSkidPress = RR.s16_Estimated_Active_Press;
            }
            else
            {
                lchrbs16RearSkidPress = RL.s16_Estimated_Active_Press;
            }
        }
        else if(ABS_rr==1)
        {
            lchrbs16RearSkidPress = RR.Estimated_Press_SKID;
        }
        else if(ABS_rl==1)
        {
            lchrbs16RearSkidPress = RL.Estimated_Press_SKID;
        }
        else
        {
            lchrbs16RearSkidPress = 0; /* rear pressure hasn't met its skid level */
        }
    }
    
    LCHRB_vDctBrakeRelease();
    
    if(vref < (int16_t)S16_HRB_EXIT_SPEED)
    {
        lchrbs8ExitCnt = L_U8_TIME_10MSLOOP_40MS; /* 10ms Task */ /* end control at low speed */
    }
    else if((lchrbs16RearSkidPress > 0) && ((lchrbs16RearSkidPress - mpress) < MPRESS_5BAR))
           /* mpress is close to skid level, thus HRB control is not needed */
    {
        if((lchrbs16RearSkidPress - mpress) < (-MPRESS_5BAR))
        {
            lchrbs8ExitCnt = lchrbs8ExitCnt + 2;
        }
        else
        {
            lchrbs8ExitCnt = lchrbs8ExitCnt + 1;
        }
        
        if((ABS_fl==0) && (ABS_fr==0))
        {
            lchrbs8ExitCnt = lchrbs8ExitCnt + 1;
        }
    }
    else if(lchrbu8BrakeReleaseCnt > 15)
    {
        lchrbs8ExitCnt = L_U8_TIME_10MSLOOP_40MS; /* 10ms Task */
    }
    else 
    {
        lchrbs8ExitCnt = 0;
    }    

  
    if((lchrbs8ExitCnt >= L_U8_TIME_10MSLOOP_40MS) || (lchrbu1Inhibit==1))    /* 10ms Task */
    { 
        lchrbu1ExitCtrl = 1;
    }
    else
    {
        lchrbu1ExitCtrl = 0;
    }
}


/******************************************************************************
* FUNCTION NAME:      LCHRB_vDecideHRBEnter
* CALLED BY:          LCHRB_vCallControl()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        Check Enter conditions of HRB control
******************************************************************************/
static void	LCHRB_vDecideHRBEnter(void)
{    
    int16_t Lower_Rear_Press;
    int16_t Higher_Rear_Press;
    int16_t REAR_Delta_P;
    
    LCHRB_vCalcEnterMPThres();
    LCHRB_vCalcEnterdPThres();
    
    if(RL.s16_Estimated_Active_Press < RR.s16_Estimated_Active_Press) {
        Lower_Rear_Press = RL.s16_Estimated_Active_Press;
        Higher_Rear_Press = RR.s16_Estimated_Active_Press;
    }
    else
    {
        Lower_Rear_Press = RL.s16_Estimated_Active_Press;
        Higher_Rear_Press = RL.s16_Estimated_Active_Press;
    }
    
    REAR_Delta_P = mpress - Lower_Rear_Press;    
    
    lchrbu1EnterCtrl = 0;
    
    if((PBA_ON==0)
   #if __FBC
    &&(FBC_ON==0)
   #endif
   #if __HBB
    &&(HBB_ON==0)
   #endif
	  )
	{
	    if(vref > (int16_t)(S16_HRB_ALLOW_SPEED))
	    {
            if((mpress >= ((int16_t)lchrbu8MpThres*10)) && (ABS_fl==1) && (ABS_fr==1) && (ABS_rl==0) && (ABS_rr==0))
            {
                if(vref < VREF_40_KPH)
                {
                    lchrbs8temp0  = HRB_ENTER_SLIP+((VREF_40_KPH-vref)/VREF_2_KPH);
                }
                else
                {
                    lchrbs8temp0  = HRB_ENTER_SLIP;
                }
                
                HRB_Slip_Check = lchrbs8temp0;
                
                if((Higher_Rear_Press < S16_HRB_REAR_MAX_PRESSURE) && (REAR_Delta_P < ((int16_t)lchrbu8dPThres*10)))
                {
                    if ((rel_lam_rl > -lchrbs8temp0) && (rel_lam_rr > -lchrbs8temp0))
                    {
                        lchrbu1EnterCtrl = 1;
                        lchrbs16EnterRearPress = Higher_Rear_Press;
                    }
                    else { }
                }
                else { }
            }
            else { }
        }
        else { }
	}
	else { }
}


/******************************************************************************
* FUNCTION NAME:      LCHRB_vCallControl
* CALLED BY:          LCHRB_vCalcHRBPulseUpRate()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        Calculate HRB Pulse-up Rate Before Brake Boost
******************************************************************************/

static void LCHRB_vCalcHRBPulseUpRate(void)
{
    if(lchrbu1EnterCtrl==1)
    {
        RL.lchrbu8PulseUpCounter=0;
        RR.lchrbu8PulseUpCounter=0;
    }

    if(RL.lchrbu8PulseUpCounter > RR.lchrbu8PulseUpCounter)
    {
        lchrbu8temp0 = RR.lchrbu8PulseUpCounter; /* pulse-up rate according to the lowest */
    }
    else
    {
        lchrbu8temp0 = RL.lchrbu8PulseUpCounter;
    }
    
    if(lchrbu8temp0 > U8_HRB_MAX_PULSE_UP)
    {
        lchrbu8PulseUpHoldScans = 0;
    }
    else if(lchrbu8temp0 <= 0)
    {
        lchrbu8PulseUpHoldScans = 8;
    }
    else
    {
        lchrbu8PulseUpHoldScans = (uint8_t)LCABS_s16Interpolation2P((int16_t)lchrbu8temp0,0,(int16_t)U8_HRB_MAX_PULSE_UP,(int16_t)U8_HRB_INITIAL_PULSEUP_HOLD,(int16_t)U8_HRB_FINAL_PULSEUP_HOLD);
    }
    
    if(lchrbu8PulseUpHoldScans==0)
    {
        lchrbu1StartBoost = 1;
    }
    else
    {
        lchrbu1StartBoost = 0;
    }
}


/******************************************************************************
* FUNCTION NAME:      LCHRB_vCalcEnterMPThres
* CALLED BY:          LCHRB_vDecideHRBEnter()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        Calculate Mpress threshold for HRB control
******************************************************************************/
static void	LCHRB_vCalcEnterMPThres(void)
{
    lchrbu8MpThres = (uint8_t)LCABS_s16Interpolation2P(lchrbs16MpressSlope,(int16_t)U8_HRB_DELTA_MP_AT_SPIKE_BRK,(int16_t)U8_HRB_DELTA_MP_AT_SLIGHT_BRK,(int16_t)U8_HRB_ENTER_PRESS_SPIKE,(int16_t)U8_HRB_ENTER_PRESS_SLIGHT);
}


/******************************************************************************
* FUNCTION NAME:      LCHRB_vCalMPSlope
* CALLED BY:          LCHRB_vCallControl()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        Calculate Mpress threshold for HRB control
******************************************************************************/
static void	LCHRB_vCalMPSlope(void)
{
    if(mpress>=MPRESS_1BAR)
    {
        lchrbs16MpressSlope = ((MCpress[0]-MCpress[3])/2) + (lchrbs16MpressSlope/2);
    }
    else
    {
        lchrbs16MpressSlope = 0;
    }
}


/******************************************************************************
* FUNCTION NAME:      LCHRB_vCalcEnterdPThres
* CALLED BY:          LCHRB_vDecideHRBEnter()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        Calculate Rear delta-P threshold for HRB control
******************************************************************************/
static void	LCHRB_vCalcEnterdPThres(void)
{
    if(mpress>=MPRESS_100BAR)
    {
        lchrbu8dPThres = (uint8_t)(((mpress/10)*U8_HRB_ENTER_DP_PERCENTAGE_H)/100); /* % of mpress in scale 1*/
    }
    else
    {
        lchrbu8dPThres = (uint8_t)(((mpress/10)*U8_HRB_ENTER_DP_PERCENTAGE_M)/100); /* % of mpress in scale 1*/
    }
}

/******************************************************************************
* FUNCTION NAME:      LCHRB_vDecideEndRelease
* CALLED BY:          LCHRB_vCallControl()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        Decide end of release mode
******************************************************************************/
static void LCHRB_vDecideEndRelease(void)
{
    if(lchrbu1Inhibit==1)
    {
        lchrbu1EndCtrl = 1;
    }
    else if((((mpress - RR.s16_Estimated_Active_Press) < MPRESS_10BAR) && ((mpress - RR.s16_Estimated_Active_Press) > -MPRESS_10BAR))
     &&(((mpress - RL.s16_Estimated_Active_Press) < MPRESS_10BAR) && ((mpress - RL.s16_Estimated_Active_Press) > -MPRESS_10BAR)))
    {
        lchrbu1EndCtrl = 1;
    }
    else
    {
        lchrbu1EndCtrl = 0;
    }
}

/******************************************************************************
* FUNCTION NAME:      LCHRB_vCalcInitTargetP
* CALLED BY:          LCHRB_vCallControl()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        Calculate initial target assist pressure
******************************************************************************/
static void LCHRB_vCalcInitTargetP(void)
{    
    lchrbs16EnterMpress = mpress;
    lchrbs16PriorMP = lchrbs16EnterMpress;
    
    if(FL.Estimated_Press_SKID < FR.Estimated_Press_SKID)
    {
        lchrbs16FrontSkidPress = FL.Estimated_Press_SKID;
    }
    else
    {
        lchrbs16FrontSkidPress = FR.Estimated_Press_SKID;
    }
    
/*
    lchrbs16TargetRearPress = mpress + ((lchrbs16FrontSkidPress*9)/10);
    
    if(lchrbs16TargetRearPress > S16_HRB_REAR_MAX_PRESSURE)
    {
        lchrbs16TargetRearPress = S16_HRB_REAR_MAX_PRESSURE;
    }
    
    lchrbs16AssistPressure = lchrbs16TargetRearPress - mpress;
*/

    lchrbs16AssistPressure = ((lchrbs16FrontSkidPress*9)/10);
        
    if(lchrbs16AssistPressure < MPRESS_10BAR)
    {
        lchrbs16AssistPressure = MPRESS_10BAR;
    }
    
    if(lchrbs16AssistPressure > (S16_HRB_REAR_MAX_PRESSURE-mpress))
    {
        lchrbs16AssistPressure = S16_HRB_REAR_MAX_PRESSURE-mpress;
    }

}

/******************************************************************************
* FUNCTION NAME:      LCHRB_vCalcTargetP
* CALLED BY:          LCHRB_vCallControl()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        Adaptation of target assist pressure
******************************************************************************/
static void LCHRB_vCalcTargetP(void)
{
    int8_t lchrbs8RefSlip;
    int8_t lchrbs8RearActualSlip;

    
    /********** Mpress Diff adaptation **************/

    if((McrAbs(mpress - lchrbs16PriorMP)) > MPRESS_10BAR)
    {
        if(mpress > lchrbs16PriorMP) /* Increasing */
        {
            lchrbs16MPAdaptation = (int16_t)((int8_t)((mpress - lchrbs16PriorMP)/MPRESS_10BAR)) * U8_HRB_MP_INC_GAIN; /* 7bar/10bar */
        }
        else
        {
            if(lchrbu1BoostFlag==0)
            {
                lchrbs16MPAdaptation = (int16_t)((int8_t)((mpress - lchrbs16PriorMP)/MPRESS_10BAR)) * U8_HRB_MP_DEC_GAIN; /* 7bar/10bar */
            }
            else
            {
                lchrbs16MPAdaptation = (int16_t)((int8_t)((mpress - lchrbs16PriorMP)/MPRESS_10BAR)) * U8_HRB_MP_DEC_GAIN_BOOST; /* 5bar/10bar */
            }
        }

        if(lchrbu1MPreleasingState == 1)
        {
            lchrbs16MPAdaptation = lchrbs16MPAdaptation - (int16_t)U8_HRB_MP_RELEASE_COMP; /* Reduce Faster and Increase Slower*/
        }
        
        lchrbs16PriorMP = mpress;
    }
    else
    {
        lchrbs16MPAdaptation = MPRESS_0BAR;
    }
    
    /********** Rear slip adaptation **************/
    
    if(rel_lam_rl < rel_lam_rr)
    {
        lchrbs8RearActualSlip = rel_lam_rl;
    }
    else
    {
        lchrbs8RearActualSlip = rel_lam_rr;
    }
    
/*
    if(vref > VREF_40_KPH)
    {
        lchrbs8RefSlip = -LAM_3P;
    }
    else
    {
        lchrbs8RefSlip = -LAM_5P;
    }
*/
    lchrbs8RefSlip = (int8_t)LCABS_s16Interpolation2P(vref,VREF_30_KPH,VREF_80_KPH,-LAM_10P,-LAM_3P);
    
    if(lchrbs8RearActualSlip < lchrbs8RefSlip)
    {
        if(lchrbu1SlipBwReference == 0) /* adaptation per slip */
        {
            lchrbu1SlipBwReference = 1;
            lchrbs16SlipAdaptation =((int16_t)(lchrbs8RearActualSlip - lchrbs8RefSlip))*(MPRESS_2BAR); /* 2bar/1% */
            lchrbs8PriorSlip = lchrbs8RearActualSlip;
        }
        else
        {
            if((McrAbs(lchrbs8RearActualSlip - lchrbs8PriorSlip)) > LAM_5P) /* adaptation per slip difference */
            {
                lchrbs16SlipAdaptation =((int16_t)(lchrbs8RearActualSlip - lchrbs8PriorSlip))*(MPRESS_2BAR); /* 2bar/1% */
                lchrbs8PriorSlip = lchrbs8RearActualSlip;
            }
            else
            {
                lchrbs16SlipAdaptation = MPRESS_0BAR;
            }
        }
    }
    else
    {
        lchrbu1SlipBwReference = 0;
        lchrbs16SlipAdaptation = MPRESS_0BAR;
    }
    
    /********** Rear Low slip adaptation **************/
    
    if((lchrbu1MPreleasingState == 0) && (lchrbu1SlipBwReference==0) && (ABS_rl==0) && (ABS_rr==0) && (lchrbu8FirstApplyCnt>=T_200_MS))
    {
        if(lchrbs8RearActualSlip >= (lchrbs8RefSlip+LAM_2P))
        {
            if(lchrbu8LowSlipTim <= T_50_MS)
            {
                lchrbu8LowSlipTim = lchrbu8LowSlipTim + 1;
            }
            else
            {
                lchrbu8LowSlipTim = 0;
            }
        }
        else
        {
            if(lchrbu8LowSlipTim > 0)
            {
                lchrbu8LowSlipTim = lchrbu8LowSlipTim - 1;
            }
            else {}
        }
    }
    else
    {
        lchrbu8LowSlipTim = 0;
    }
    
    if(lchrbu8LowSlipTim > T_50_MS)
    {
        lchrbs16SlipAdaptation = MPRESS_10BAR;
    }
    else
    {
        ;
    }
    /*************************************************/
    
    lchrbs16AssistPressure = lchrbs16AssistPressure + lchrbs16MPAdaptation + lchrbs16SlipAdaptation;
        
    if(lchrbs16AssistPressure < MPRESS_10BAR)
    {
        lchrbs16AssistPressure = MPRESS_10BAR;
    }
    
    if(lchrbs16AssistPressure > (S16_HRB_REAR_MAX_PRESSURE-mpress))
    {
        lchrbs16AssistPressure = S16_HRB_REAR_MAX_PRESSURE-mpress;
    }
}

/******************************************************************************
* FUNCTION NAME:      LCHRB_vCalcPReleaseRate
* CALLED BY:          LCHRB_vCallControl()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        Calculate Release Pressure Rate
******************************************************************************/
static void LCHRB_vCalcPReleaseRate(void)
{
    ;
}

/******************************************************************************
* FUNCTION NAME:      LCHRB_vPressModeRELEASE
* CALLED BY:          LCHRB_vCallControl()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        Pressure Release mode
******************************************************************************/
static void	LCHRB_vPressModeRELEASE(void)
{
    if(lchrbu8PressReleaseCnt < 255)
    {
        lchrbu8PressReleaseCnt++;
    }
    /*
    lchrbu1TCPrimary = 1;
    lchrbu1TCSecondary = 1;
    */
    lchrbu1TCPrimary = 0;
    lchrbu1TCSecondary = 0;
    
    lchrbu1ESVPrimary = 0;
    lchrbu1ESVSecondary = 0;
}


/******************************************************************************
* FUNCTION NAME:      LCHRB_vPressModeBOOST
* CALLED BY:          LCHRB_vCallControl()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        Pressure boost mode
******************************************************************************/
static void	LCHRB_vPressModeBOOST(void)
{
	lchrbu1TCPrimary = 1;
	lchrbu1TCSecondary = 1;
	
    if(lchrbu1StartBoost==1)
	{
	    lchrbu1FirstBoostP = 1;
	    lchrbu1FirstBoostS = 1;
	    lchrbu8FirstApplyCnt = 1;
	}
	else { }
	
    if(((lchrbu1FirstBoostP==0) && (AV_DUMP_fr==1)) || (AV_DUMP_rl==1))
    {
        lchrbs16NeedBrkFluidCntP = T_400_MS;
        lchrbs16ApplyCntP = 51;
    }
    else
    {
        if((FR.state==DETECTION) && (RL.state==DETECTION) && (lchrbs16NeedBrkFluidCntP>T_200_MS))
        {
            lchrbs16NeedBrkFluidCntP = T_200_MS;
        }
        else if(((FR.state==DETECTION) || (RL.state==DETECTION)) && (lchrbs16NeedBrkFluidCntP>0))
        {
            lchrbs16NeedBrkFluidCntP = lchrbs16NeedBrkFluidCntP-1;
        }
        else { }

        if(lchrbs16NeedBrkFluidCntP==0)
        {
            if((RL.state==DETECTION) && (RL.s16_Estimated_Active_Press <= (lchrbs16AssistPressure + mpress - MPRESS_20BAR)))
                                          /* Boost Pressure Until Estimated Press reaches target pressure level */
            {
                lchrbs16ApplyCntP = 0; /* need pressure apply */
            }
        }
    }

    if(((lchrbu1FirstBoostS==0) && (AV_DUMP_fl==1)) || (AV_DUMP_rr==1))
    {
        lchrbs16NeedBrkFluidCntS = T_400_MS;
        lchrbs16ApplyCntS = 51;
    }
    else
    {
        if((FL.state==DETECTION) && (RR.state==DETECTION) && (lchrbs16NeedBrkFluidCntS>T_200_MS))
        {
            lchrbs16NeedBrkFluidCntS = T_200_MS;
        }
        else if(((FL.state==DETECTION) || (RR.state==DETECTION)) && (lchrbs16NeedBrkFluidCntS>0))
        {
            lchrbs16NeedBrkFluidCntS = lchrbs16NeedBrkFluidCntS-1;
        }
        else { }

        if(lchrbs16NeedBrkFluidCntS==0)
        {
            if((RR.state==DETECTION) && (RR.s16_Estimated_Active_Press <= (lchrbs16AssistPressure + mpress - MPRESS_20BAR)))
                                          /* Boost Pressure Until Estimated Press reaches target pressure level */
            {
                lchrbs16ApplyCntS = 0; /* need pressure apply */
            }
        }
    }
        
	if(lchrbs16ApplyCntP<=50) {
		lchrbu1ESVPrimary = 1;
	}
	else
	{
	    lchrbu1ESVPrimary = 0;
	    lchrbu1FirstBoostP = 0;
	}
	
	if(lchrbs16ApplyCntS<=50) {
        lchrbu1ESVSecondary = 1;
	}
	else
	{
        lchrbu1ESVSecondary = 0;
        lchrbu1FirstBoostS = 0;
	}
      
	if(lchrbs16ApplyCntP<=T_1500_MS) {
		lchrbs16ApplyCntP++;
    }
    else { }
    
    if(lchrbs16ApplyCntS<=T_1500_MS) {
		lchrbs16ApplyCntS++;
    }
    else { }
    
    if((lchrbu1ESVPrimary==1)||(lchrbu1ESVSecondary==1))
    {
        lchrbu1BoostFlag=1;
    }
    else
    {
        lchrbu1BoostFlag=0;
    }

    if((lchrbu8FirstApplyCnt>=1) && (lchrbu8FirstApplyCnt<=T_200_MS))
    {
        lchrbu8FirstApplyCnt = lchrbu8FirstApplyCnt + 1;
    }
    else
    {
        ;
    }
}

/******************************************************************************
* FUNCTION NAME:      LCHRB_vPressModeHOLD
* CALLED BY:          LCHRB_vCallControl()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        TC Pressure Hold mode : No Pressure Apply
******************************************************************************/
static void	LCHRB_vPressModeHOLD(void)
{
	lchrbu1TCPrimary = 1;
	lchrbu1TCSecondary = 1;
	lchrbu1ESVPrimary = 0;
	lchrbu1ESVSecondary = 0;
}

/******************************************************************************
* FUNCTION NAME:      LCHRB_vDctReducedBrkInput
* CALLED BY:          LCHRB_vCallControl()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        Decide Release of MC Pressure
******************************************************************************/
static void	LCHRB_vDctReducingBrkInput(void)
{
    if(lchrbu1BoostFlag==0)
    {
        if(lchrbu1MPreleasingState==0)
        {
            if(lchrbs16MpressSlope < -10)
            {
                lchrbs8MPreleasingCnt = lchrbs8MPreleasingCnt+1;
            }
            else if(lchrbs16MpressSlope >=0)
            {
                lchrbs8MPreleasingCnt = 0;
            }
            else { }
    
            if(lchrbs8MPreleasingCnt >= 15)
            {
                lchrbs8MPreleasingCnt = 15;
                lchrbu1MPreleasingState = 1;
            }
            else
            {
                lchrbu1MPreleasingState = 0;
            }
        }
        else
        {
            if(lchrbs16MpressSlope > U8_HRB_DELTA_MP_AT_SPIKE_BRK)
            {
                lchrbs8MPreleasingCnt=lchrbs8MPreleasingCnt-3;
            }
            else if(lchrbs16MpressSlope > U8_HRB_DELTA_MP_AT_SLIGHT_BRK)
            {
                lchrbs8MPreleasingCnt=lchrbs8MPreleasingCnt-1;
            }
            else if(lchrbs16MpressSlope > 0)
            {
                ;
            }
            else
            {
                lchrbs8MPreleasingCnt = 15;
            }
            
            if(lchrbs8MPreleasingCnt<=0)
            {
                lchrbu1MPreleasingState = 0;
                lchrbs8MPreleasingCnt = 0;
            }
        }
    }
    else
    {
        lchrbu1MPreleasingState = 0;
        lchrbs8MPreleasingCnt = 0;
    }
}


/******************************************************************************
* FUNCTION NAME:      LCHRB_reset
* CALLED BY:          LCHRB_vCallControl()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        Reset HRB variables and flags
******************************************************************************/
static void	LCHRB_reset(void)
{
	lchrbu1ActiveFlag = 0;
	lchrbu1EnterCtrl = 0;
	lchrbu1ExitCtrl = 0;
	lchrbu1EndCtrl = 0;
	
	/* lchrbs16ApplyCnt = 0; */
	lchrbs16ApplyCntP = 0;
	lchrbs16ApplyCntS = 0;
	lchrbs8ExitCnt = 0;
	lchrbu8PressReleaseCnt = 0;
	
	lchrbs16EnterMpress = 0;
	lchrbu8LowSlipTim = 0;
	/*lchrbs16RearPressAtABS = 0;*/
	
	lchrbu1TCPrimary = 0;
	lchrbu1TCSecondary = 0;
	
	lchrbu1ESVPrimary = 0;
	lchrbu1ESVSecondary = 0;
	
	lchrbs16RearSkidPress = 0;
    lchrbu1MPreleasingState = 0;
    lchrbs8MPreleasingCnt = 0;
	/* MOTOR? */

	/*
	
	HSA_hold_counter = 0;
	HSA_complete_stop_counter = 0;
	HSA_rebraking_counter = 0;
	HSA_rebraking_start = 0;
	HSA_RAPID_REBRAKING_flag = 0;
	COMPLETE_STOP_flag = 0;
	HSA_EXIT_eng_torq = 0;
	HSA_ENTER_press = 0;
	HSA_retaining_press = 0;
	HSA_pressure_release_counter=0;
	HSA_S_VALVE_LEFT = 0;
	HSA_S_VALVE_RISHT = 0;
	HSA_MOTOR_ON = 0;
	HSA_accel_cnt = 0;
	HSA_accel_cnt2 = 0;
	HSA_phase_out_time = 0;
	HSA_phase_out_rate = 0;
  #if __HSA_2ND_HOLD	
	HSA_2nd_hold_timer = 0;
  #endif	
  */
}

static void LCHRB_vDctBrakeRelease(void)
{
    if(mpress < ((lchrbs16EnterMpress*4)/10))
    {
        if(mpress < MPRESS_10BAR)
        {
            lchrbu8BrakeReleaseCnt = lchrbu8BrakeReleaseCnt + 5;
        }
        else if(mpress < MPRESS_30BAR)
        {
            lchrbu8BrakeReleaseCnt = lchrbu8BrakeReleaseCnt + 2;
        }
        else
        {
            lchrbu8BrakeReleaseCnt = lchrbu8BrakeReleaseCnt + 1;
        }
    }
    else {lchrbu8BrakeReleaseCnt=0;}
}

/******************************************************************************
* FUNCTION NAME:      LCHRB_vResetCtrl
* CALLED BY:          ebd_off()
* Preconditions:      none 
* PARAMETER:          
* RETURN VALUE:       none
* Description:        Reset HRB Control
******************************************************************************/
void LCHRB_vResetCtrl(void)
{
    lchrbs8CtrlMode = S8_HRB_READY;
	lchrbu1ActiveFlag = 0;
	lchrbu1TCPrimary = 0;
	lchrbu1TCSecondary = 0;
	lchrbu1ESVPrimary = 0;
	lchrbu1ESVSecondary = 0;
}

/******************************************************************************
* FUNCTION NAME:      LCMSC_vSetHRBTargetVoltage
* CALLED BY:          LCMSC_vInterfaceTargetvol()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Set Motor Target BEMF Voltage for HRB
******************************************************************************/
    #if __ADVANCED_MSC
void LCMSC_vSetHRBTargetVoltage(void)
{

    HRB_MSC_MOTOR_ON = lchrbu1ActiveFlag;

    if(HRB_MSC_MOTOR_ON==1) 
    {
        if((lchrbu1FirstBoostP==1) || (lchrbu1FirstBoostS==1))
        {
            hrb_msc_target_vol = MSC_14_V; /* Fully On At First Boosting */
        }
        else
        {
            hrb_msc_target_vol = (int16_t)(((int32_t)U8_HRB_MSC_TARGET_VOL*MSC_1_V)/10); /* For Resolution Change */
        }
    }
    else 
    {
        hrb_msc_target_vol = 0;
    }
}
    #endif

#endif /* __HRB */

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCHRBCallControl
	#include "Mdyn_autosar.h"
#endif    
