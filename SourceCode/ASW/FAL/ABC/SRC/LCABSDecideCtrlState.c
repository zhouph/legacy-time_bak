/******************************************************************************
* Project Name: Anti-lock Braking System
* File: LCABSDecideControlState.C
* Description: ABS control algorithm
* Date: November. 21. 2005
******************************************************************************/

/* Includes ******************************************************************/
#if (__IDB_LOGIC == ENABLE)
	//#include "../../System/WSecondaryMCU.h"
#endif


/* AUTOSAR --------------------------*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCABSDecideCtrlState
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/

#include "LCABSCallControl.h"
#include "LCABSDecideCtrlState.h"
#include "LCABSDecideCtrlVariables.h"
#include "LCABSStrokeRecovery.h"
#include "LCEBDCallControl.h"
#include "LDABSDctVehicleStatus.h"
#include "LDABSDctWheelStatus.h"
#include "LDABSDctRoadSurface.h"
#include "LCHDCCallControl.h"
#include "LCACCCallControl.h"
#include "LCEPBCallControl.h"
#include "LCallMain.h"
#include "LACallMain.h"
#include "LDABSCallVrefEst.H"
#include "LDABSCallDctForVref.H"
#include "LSESPFilterEspSensor.H"
#include "LCABSCallESPCombination.H"
#include "LSABSCallSensorSignal.h"
#include "Hardware_Control.h"
#include "LSESPCalSensoroffset.h"
#include "LCESPCalInterpolation.h"
  #if __CBC
#include "LCCBCCallControl.h"
  #endif
  #if __PAC
#include "LCPACCallControl.h"
  #endif

  #if __VSM
#include "UCC_EPSi/LCEPSCallControl.h"
  #endif
  
  #if __SCC
#include "SCC/LCWPCCallControl.H"
  #endif


/*#include LCABSTuningHeader.h"*/

/* Local Definiton  **********************************************************/

#define __REAR_1ST_CYCLE_DV         1

#define __VARY_UR_HOLD_PER_DUMPSCANS    1

#if __SPLIT_STABILITY_IMPROVE_GMA
  #if __MGH60_ABS_CONCEPT_RELEASED
#define __FIRST_CYCLE_YAW_ADAPT             0
#define __YAW_STABILITY_IMPROVE             1
#define __UNDERSTEER_FRT_STB_DUMP_BFABS     0
#define __OVERSTEER_REAR_STB_DUMP_BFABS     0
#define __NON_SPLIT_STABILITY_IMPROVE       1
  #elif __MGH60_ABS_IMPROVE_CONCEPT
#define __FIRST_CYCLE_YAW_ADAPT             0
#define __YAW_STABILITY_IMPROVE             1
#define __UNDERSTEER_FRT_STB_DUMP_BFABS     0
#define __OVERSTEER_REAR_STB_DUMP_BFABS     1
#define __NON_SPLIT_STABILITY_IMPROVE       1
  #else
#define __FIRST_CYCLE_YAW_ADAPT             0
#define __YAW_STABILITY_IMPROVE             0
#define __UNDERSTEER_FRT_STB_DUMP_BFABS     0
#define __OVERSTEER_REAR_STB_DUMP_BFABS     0
#define __NON_SPLIT_STABILITY_IMPROVE       0
  #endif
#else
#define __FIRST_CYCLE_YAW_ADAPT             0
#define __YAW_STABILITY_IMPROVE             0
#define __UNDERSTEER_FRT_STB_DUMP_BFABS     0
#define __OVERSTEER_REAR_STB_DUMP_BFABS     0
#define __NON_SPLIT_STABILITY_IMPROVE       0
#endif/*__SPLIT_STABILITY_IMPROVE_GMA*/

/* Variables Definition*******************************************************/
ABS_FLAG_t  lcAbsBrakingFlg, lcAbsBrakingFlg2, lcAbsCtrlFlg0, lcAbsCtrlFlg1, lcAbsCtrlFlg2, lcAbsCtrlFlg3, lcAbsValidityFlg0;

uint8_t WHEEL_ACT_MODE;
uint8_t CONTROL_STATE;

uint8_t gma_flags;
uint8_t YMR_check_counter;
int8_t circuit_fail_cnt;

uint8_t abs_begin_timer;
uint8_t  abs_off_timer;
uint8_t  abs_off_timer2;
int16_t Slip_Gain;
int16_t Decel_Gain;
/*int16_t Pulse_Down_Gain;*/
int16_t	lcabss16MpressSlope, lcabss16MpressSlopePeak;

#if (__IDB_LOGIC==ENABLE)
int16_t lcabss16RefMpress, lcabss16RefMpressOld, lcabss16RefCircuitPress, lcabss16RefActiveTargetPress, lcabss16RefFinalTargetPres;
#else
int16_t lcabss16RefMpress, lcabss16RefCircuitPress, lcabss16RefMpressOld;
#endif

#if (__ABS_DESIRED_BOOST_TARGET_P == ENABLE)
int16_t lcabss16EstFrontWhlSkidPress;
int16_t lcabss16EstRearWhlSkidPress;
int16_t lcabss16TotalEstWhlSkidPress;
int16_t lcabss16InABSDesiredBoostTarP;
int16_t lcabss16InABSDesiredBoostTPOld;
#endif

static uint8_t lcabsu8AdaptHoldPerSlipDiff;

#if __VDC
 #if __ESP_SPLIT_2
    uint8_t yaw_hold_scan_ref;
 #endif
    uint8_t gma_hold_timer;       /* ESP+SPLIT '03sw */
#endif

  #if __UNSTABLE_REAPPLY_ENABLE
int8_t Min_dP_risetime;
  #endif

  #if __ABS_RBC_COOPERATION
int16_t lcabss16AbsOnCnt;
int16_t lcabss16AbsOnMcTargetPress;
int16_t lcabss16RBCPressBfABS;
uint8_t lcabsu8PressRecoveryDelayTime;
  #endif

int16_t lcabss16temp0, lcabss16temp1, lcabss16temp2, lcabss16temp3, lcabss16temp4;
int8_t lcabss8temp0,lcabss8temp1,lcabss8temp2,lcabss8temp3,lcabss8temp4;

  #if __SPLIT_STABILITY_IMPROVE_GMA
uint8_t lcabsu8DelaYawExcessCnt;
uint8_t lcabsu8DelaYawStableCnt; 
uint8_t GMA_STATE;
int16_t Yaw_Gain, Yaw_Dot_Gain, lcabss16TargetDeltaYaw, lcabss16TargetDeltaYaw2;
int16_t lcabss16YDFSplitLHcnt, lcabss16YDFSplitRHcnt;
  #endif

  #if defined (__STAB_DCT_LOGIC_DEBUG)
int16_t  AradLowerThresFL,AradLowerThresFR                   ;
int16_t  AradHigherThresFL,AradHigherThresFR                 ;
int16_t  StabDctdVThresFL,StabDctdVThresFR                   ;
int16_t  StabDctSlipThresFL,StabDctSlipThresFR               ;
int16_t  BendDiffCompensatedTempFL, BendDiffCompensatedTempFR;
int16_t  BendSlipCompensatedTempFL, BendSlipCompensatedTempFR;
   #endif /* defined (__STAB_DCT_LOGIC_DEBUG) */

  #if __SLIGHT_BRAKING_COMPENSATION
static uint8_t	lcabsu8slightbrkcnt;
  #endif

Whl_TP_t FL_WP, FR_WP, RL_WP, RR_WP, *WL_WP;

/* LocalFunction prototype ***************************************************/

void LCABS_vSetBrakingStateFlg(void);
void LCABS_vSetWheelDumpThreshold(void);
void LCABS_vSetWheelDumpThresWL(struct W_STRUCT *WL_temp);
void LCABS_vCapturePEAKCEL(void);
void LCABS_vCapturePEAKSLIP(void);
void LCABS_vSetTHRdV(void);
void LCABS_vSetTHRdVoutABS(void);
void LCABS_vSetTHRdVoutABSPerSlip(void);
void LCABS_vSetTHRdVinABS(void);
void LCABS_vCompTHRdVforRoughness(void);
  #if __dV_COMP_OVERSHOOT
void LCABS_vCompTHRdVforOvershoot(void);
  #endif
  #if __VDC
void LCABS_vCompTHRdVforESPcontrol(void);
  #endif
  #if __DUMP_THRES_COMP_for_Spold_update
void LCABS_vCompTHRdVforSpoldUpdate(void);
  #endif
void LCABS_vSetTHRDECEL(void);
void LCABS_vSetInitialTHRDECEL(void);
void LCABS_vAdaptTHRDECEL(void);
void LCABS_vSetSlipAtMinusB(void);
void LCABS_vSetMinusBSlipFlg(void);
  #if __THRDECEL_RESOLUTION_CHANGE
static int16_t LCABS_s16SetHigherLimitDECEL(void);
  #endif
void LCABS_vDctYMRforFrontOutABS(struct W_STRUCT *pFH, struct W_STRUCT *pFL, struct W_STRUCT *pRH, struct W_STRUCT *pRL);
void LCABS_vDctYMRforFrontInABS(struct W_STRUCT *pFH, struct W_STRUCT *pFL, struct W_STRUCT *pRH, struct W_STRUCT *pRL);
#if __SPLIT_STABILITY_IMPROVE_GMA
void LCABS_vAdaptDeltaYawForYMR(void);
void LCABS_vCalcDeltaYawFactorWL(struct W_STRUCT *OVER_INSIDE_WL, uint8_t YMR_CONTROL_STATE);
int16_t  LCABS_vCheckSplitTendency(int16_t SP_HIGH_FRONT_YDF, int16_t SP_LOW_FRONT_YDF,uint8_t BASE_WHEEL, int16_t SPLIT_CNT);
#endif

void LCABS_vResetABS(void);
void LCABS_vSetLFCHoldTimer(void);
void LCABS_vSetLFCHoldTimerWL(struct W_STRUCT *WL);
void LCABS_vDecideWheelCtrlState(void);
void LCABS_vDecideWheelCtrlStateWL(struct W_STRUCT *WL_temp);
void LCABS_vCheckABSExitSpeed(void);
void LCABS_vInitializeLFCCtrlVars(void);
void LCABS_vDecideSTABLEStateCtrl(void);
void LCABS_vDecideUNSTABLEByDecelndV(void);
int8_t LCABS_s8CheckSpoldClear(void);
void LCABS_vClearSpold(void);
int8_t LCABS_s8CheckDumpStartDelay(void);
void LCABS_vCheckDumpStart(void);
void LCABS_vIgnoreSlipByEngBraking(void);
void LCABS_vDecideUNSTABLEBySlip(void);
void LCABS_vDecideSTABLECtrlMode(void);
void LCABS_vDecideSTABLEReapply(void);
void LCABS_vCheckForcedHold(void);
void LCABS_vExtricateFromCASCADING(void);
int16_t LCABS_s16CheckFastestWheel(void);
void LCABS_vCheckABSForcedEnter(void);
void LCABS_vWPresModeFULLREAPPLY(void);
void LCABS_vWPresModeBUILTREAPPLY(void);
void LCABS_vWPresModeSTABLEHOLD(void);
void LCABS_vWPresModeStartDUMP(void);
void LCABS_vWPresModeGMADUMP(void);
  #if __STABLE_DUMPHOLD_ENABLE
static void LCABS_vCheckStbDumpHoldMode(void);
  #endif
void LCABS_vUpdateLFCVarsAtDumpStart(void);
int8_t LCABS_s8CheckMSLCtrlMode(void);
void LCABS_vCheckMSLSplitCtrl(struct W_STRUCT *pRH, struct W_STRUCT *pRL);
void LCABS_vDecideFrontHSideGMACtrl(void);
void LCABS_vDecideUNSTABLEStateCtrl(void);
void LCABS_vWPresModeUNSTABLEDUMP(void);
void LCABS_vWPresModeUNSTABLEHOLD(void);
#if __UNSTABLE_REAPPLY_ENABLE
void LCABS_vDecideDeltaPCtrlMode(void);
void LCABS_vCompdPParameterRef(void);
void LCABS_vCalcDeltaPCompTime(uint8_t dP_Dump_Ref,int8_t dP_Arad_Ref,uint8_t dP_Time_by_dump);
void LCABS_vCompDPTimPerSituation(void);
#if __DETECT_MU_FOR_UR
static void LCABS_vDctRoadStatusForUR(void);
#endif
uint8_t LCABS_U8ChkDeltaPCtrlState(void);
uint8_t LCABS_U8CheckUNSTABLEReapply(void);
void LCABS_vCheckMSLUnstableReapply(struct W_STRUCT*pNBASE, struct W_STRUCT*pBASE);
//void LCABS_vCheckUNSTABLEReapplyEnd(void);
void LCABS_vWPresModeUNSTABLEREAPPLY(void);
void LCABS_vWPresModeUnstReapplyHOLD(int16_t s16UnstRiseHldMod);
  #if __MP_COMP
static void LCABS_vDecideUNSTABLERiseTime(void);
  #endif
#endif
#if __PRES_RISE_DEFER_FOR_UR
static void LCABS_vDecideURPresLimitation(void);
static void LCABS_vDecURPresLimitationF(struct W_STRUCT *FI,struct W_STRUCT *FO);
#endif
int8_t LCABS_s8GetParamFromRef(uint8_t input, uint8_t Reference1, uint8_t Reference2, uint8_t Reference3, uint8_t OutParam1, uint8_t OutParam2, uint8_t OutParam3, uint8_t OutParamMax);
int16_t LCABS_s16GetParamFromRef(int16_t input, int16_t Reference1, int16_t Reference2, int16_t Reference3, int16_t OutParam1, int16_t OutParam2, int16_t OutParam3, int16_t OutParamMax);
void LCABS_vDesignatePulseDownPar(void);
void LCABS_vDesignatePulseDownParF(void);
void LCABS_vDesignatePulseDownParR(void);
void LCABS_vCalculatePulseDownFactor(void);
  #if __SDIFF_OFFSET
int16_t LCABS_s16GetSDIFFOffsetPerSlip(int16_t input,int8_t wheel_slip);
  #endif
void LCABS_vAdapt1stCyclePulseDown(void);
uint8_t LCABS_u8Get1stCycleMuFromHDF(int16_t input, int16_t REF_LOW, int16_t REF_MED);
int16_t LCABS_s16Interpolation2P(int16_t input,int16_t mu_1,int16_t mu_2,int16_t gain_1,int16_t gain_2);
int16_t LCABS_s16GetGainFromMulevel(int16_t input,int16_t gain_low,int16_t gain_lowmed, int16_t gain_med, int16_t gain_high);
void LCABS_vDecideSTABLEState(void);
  #if __USE_SIDE_VREF
static void LCABS_vDecideSTABLEBySideVref(void);
  #endif

  #if (__PRESS_EST && __EST_MU_FROM_SKID)
void LCABS_vGet1stCycMuFromPress(void);
int8_t LCABS_s8Get1stCycMuFromPressF(struct W_STRUCT*FI, struct W_STRUCT*FO, struct W_STRUCT*RI, struct W_STRUCT*RO);
int8_t LCABS_s8Get1stCycMuFromPressR(struct W_STRUCT*FI, struct W_STRUCT*FO, struct W_STRUCT*RI, struct W_STRUCT*RO);
int8_t LCABS_s8GetFactorFromParam(int16_t input,int16_t LMuParam, int16_t MHMuParam, int16_t HMuParam);
  #endif

  #if __OUT_ABS_P_RATE_COMPENSATION
static void LCABS_vDecideOutABSCtrlForYMR(void);
static void LCABS_vDecideOutABSYMRCtrlModeF(struct W_STRUCT *pFH, struct W_STRUCT *pFL);
  #endif

  #if __ABS_RBC_COOPERATION
static void LCABS_vCalcRBCdPCompTime(void);
void LCABS_vDecidePressRecoveryBfAbs(void);
static void LCABS_vCheckWheelState(void);
  #endif

  #if __SLIGHT_BRAKING_COMPENSATION
void LCABS_vDecideBrakingStatus(void);
  #endif

  #if (L_CONTROL_PERIOD>=2)
uint8_t LCABS_u8CountDumpNum(uint8_t u8DumpCounter, uint8_t s8DumpCase);
uint8_t LCABS_u8DecountStableDumpNum(uint8_t u8StableDumpCounter);
  #endif
  
static void LCABS_vDecideBuiltRisePMode(uint8_t u8BuiltHoldRef, uint8_t u8WheelCtrlMode);

#if (M_DP_BASE_BUILT_RISE == ENABLE)
static void LCABS_vInitializeMstPatternVars(void);
static int16_t LCABS_s16DecideWhlTarDeltaP(struct W_STRUCT *WL_temp, Whl_TP_t *WL_WP_temp);
static void LCABS_vCalcWhlTarPress(struct W_STRUCT *WL_temp, Whl_TP_t *WL_WP_temp);

static int16_t LCABS_s16DecideBaseTarDeltaP(uint8_t u8WhlCtrlMode);
static int16_t LCABS_s16DecideCompTarDeltaP(void);

static int16_t LCABS_s16DecideCommonBaseTarDP(int16_t s16DumpTargetDP, int16_t s16RiseTargetDP);
static int16_t LCABS_s16DecideYMRTarDP(void);
static int16_t LCABS_s16DecideUnstRiseTarDP(void);
static int16_t LCABS_s16DecideInABSTarDP(void);

static int16_t LCABS_s16DecideCyclicCompDP(void);
static int16_t LCABS_s16DecideSpecialCompDP(void);
static int16_t LCABS_s16DecideMPCyclicCompDP(void);

static int16_t LCABS_s16DecideYMRRiseDP(void);

static int16_t LCABS_s16DecideInABSRiseDP(void);
static int16_t LCABS_s16DecideInABSInitRiseDP(void);
static int16_t LCABS_s16CalcInABSInitRiseDP(void);
static int16_t LCABS_s16DecideDistbnInitRiseDP(int16_t s16InitialDeltaP);
static int16_t LCABS_s16DecideInABSNthRiseDP(void);
static void LCABS_vDecideDeltaPCtrlVariable(void);
static int16_t LCABS_s16InABSRiseCompFromEstP(void);
static int16_t LCABS_s16InABSRiseCompFromWhl(void);

static void LCABS_vDecideVarsAtRiseHld(int16_t s16RiseHoldMode);
static void LCABS_vUpdVarsAftDump(void);
static int16_t LCABS_s16DecideInABSRiseCompDP(void);
void LCABS_vDecideCtrlActuationVars(void);
static void LCABS_vDecideWhlActPattern(struct W_STRUCT *WL_temp);
#endif /*(M_DP_BASE_BUILT_RISE == ENABLE)*/

static void LCABS_vCheckForcedHldForStrRcvr(uint8_t u8ForcedholdForStrkrecvr);

#if __ABS_DESIRED_BOOST_TARGET_P == ENABLE
static void LCABS_s16EstWhlSkidPress(void);
static void LCABS_vDecideDesiredBoostTarP(void);
#endif

static uint8_t LCABS_u8DecideDiffRatioForRise(struct W_STRUCT *pNBASE, struct W_STRUCT *pBASE);
static uint8_t LCABS_u8DecideAllowDiffRearWP(struct W_STRUCT *pNBASE, struct W_STRUCT *pBASE);
static void LCABS_vLimitDiffTardPAtSplit(struct W_STRUCT* pNBASE, struct W_STRUCT* pBASE, Whl_TP_t *pNBASE_WP, Whl_TP_t *pBASE_WP);
static void LCABS_vLimitDiffTardPAtnSplit(struct W_STRUCT *pNBASE, struct W_STRUCT *pBASE, Whl_TP_t *pNBASE_WP, Whl_TP_t *pBASE_WP);
static void LCABS_vLimitDiffofRearTardP(void);
static void LCABS_vLimit1stCycleRearTardP(void);
/* GlobalFunction prototype **************************************************/

/* Implementation*************************************************************/



/******************************************************************************
* FUNCTION NAME:      LCABS_vSetBrakingStateFlg
* CALLED BY:          LCABS_vSetWheelDumpThreshold()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Decide Dump threshold for ABS control
******************************************************************************/
void LCABS_vSetBrakingStateFlg(void)
{
#if __VDC && __TCMF_CONTROL
    int16_t  s16MpThresActiveToNormalBrk;
    int16_t  s16TcPressMin = (la_PtcMfc.MFC_Target_Press>la_StcMfc.MFC_Target_Press) ? la_StcMfc.MFC_Target_Press : la_PtcMfc.MFC_Target_Press;
    s16TcPressMin = s16TcPressMin*MPRESS_1BAR;
#endif

  #if (__CONSIDER_BLS_FAULT == ENABLE)
    if((BLS==1)&&(lcabsu1FaultBLS==0))
  #else
    if(BLS==1)
  #endif
  	{
  		lcabsu1BrakeByBLS = 1;
	}
	else
	{
		lcabsu1BrakeByBLS = 0;
	}

  #if __VDC
    BRAKE_BY_MPRESS_FLG_OLD = BRAKE_BY_MPRESS_FLG;
    ACTIVE_BRAKE_FLG_OLD = ACTIVE_BRAKE_FLG;

  #if __BRK_SIG_MPS_TO_PEDAL_SEN
    if(lcabsu1ValidBrakeSensor==1)
    {
        if(lcabsu1BrakeSensorOn==1)
        {
            BRAKE_BY_MPRESS_FLG = 1;
        }
        else
        {
            BRAKE_BY_MPRESS_FLG = 0;
        }
    }
  #elif (__BLS_NO_WLAMP_FM==ENABLE)
    if((lcabsu1ValidBrakeSensor==1) 
    &&(((lsespu1MpressOffsetOK==1)&&(lcabsu1BrakeSensorOn==1)) || ((lsespu1MpressOffsetOK==0)&&(lcabss16RefMpress>=BRK_ON_PRESS_NO_OFFSET_COR)))
    )
    {
        BRAKE_BY_MPRESS_FLG = 1;
    }
  #else
    if((lcabsu1ValidBrakeSensor==1) && (lcabsu1BrakeSensorOn==1))
    {
        BRAKE_BY_MPRESS_FLG = 1;
    }
  #endif /* #if (__BLS_NO_WLAMP_FM==ENABLE) */
    else
    {
        if(lcabsu1ValidBrakeSensor==0)
        {
            BRAKE_BY_MPRESS_FLG = lcabsu1BrakeByBLS;
        }
        else
        {
            BRAKE_BY_MPRESS_FLG = 0;
        }
    }
#if(__IDB_LOGIC == ENABLE)
     #if(__REF_P_RESOL_CHANGED_10_TO_100 == ENABLE)
    if(S16_PRESS_RESOL_100_TO_10(lcabss16RefActiveTargetPress) > MPRESS_0BAR)
	 #else
    if(lcabss16RefActiveTargetPress > MPRESS_0BAR)
	 #endif
    {
        if(ACTIVE_BRAKE_FLG==0)
        {
        	if((BRAKE_BY_MPRESS_FLG==0) || (lcabss16RefMpress<=lcabss16RefActiveTargetPress))
        	{
                ACTIVE_BRAKE_FLG = 1;
            }
        }
        else
        {
        	if((BRAKE_BY_MPRESS_FLG==1) && (lcabss16RefMpress>=lcabss16RefActiveTargetPress))
        	{
        		ACTIVE_BRAKE_FLG = 0;
        	}
        }
    }
    else
    {
    	ACTIVE_BRAKE_FLG = 0;
    }
#else if __VDC && __TCMF_CONTROL
    if((0)
      #if __HDC
        || (lcu1HdcActiveFlg==1)
      #endif
      #if __ACC
        || (lcu1AccActiveFlg==1)
      #endif
      #if __EPB_INTERFACE
        || (lcu1EpbActiveFlg==1)
      #endif
      #if __SCC
        || (lcu8WpcActiveFlg==1)
      #endif
    )
    {
    	s16MpThresActiveToNormalBrk = s16TcPressMin;
    	if((MCpress[0]-MCpress[2])>=MPRESS_3BAR)
    	{
    		s16MpThresActiveToNormalBrk -= (MCpress[0]-MCpress[2]);
    		if(s16MpThresActiveToNormalBrk <= MPRESS_10BAR)
    		{
    			s16MpThresActiveToNormalBrk = (s16TcPressMin>MPRESS_10BAR) ? MPRESS_10BAR : s16TcPressMin;
    		}
    	}
    	
        if(ACTIVE_BRAKE_FLG==0)
        {
        	if((BRAKE_BY_MPRESS_FLG==0) || (lcabss16RefMpress<=(s16MpThresActiveToNormalBrk-MPRESS_5BAR)))
        	{
                ACTIVE_BRAKE_FLG = 1;
            }
        }
        else
        {
        	if((BRAKE_BY_MPRESS_FLG==1) && (lcabss16RefMpress>=s16MpThresActiveToNormalBrk))
        	{
        		ACTIVE_BRAKE_FLG = 0;
        	}
        }
        	
    }
    else
    {
        ACTIVE_BRAKE_FLG = 0;
    }
#endif /*__VDC && __TCMF_CONTROL*/

    if(ABS_fz==1)
    {
        if((ABS_BEFORE_MPRESS_BRAKE==0) && (ABS_DURING_MPRESS_FLG==0))
        {
            if(BRAKE_BY_MPRESS_FLG_OLD==1)
            {
                ABS_DURING_MPRESS_FLG = 1;
            }
            else
            {
                ABS_BEFORE_MPRESS_BRAKE = 1;
            }
        }
        else
        {
            if(ABS_DURING_MPRESS_FLG==1)
            {
                if((BRAKE_BY_MPRESS_FLG_OLD==1) && (BRAKE_BY_MPRESS_FLG==0))
                {
                    MPRESS_RELEASE_DURING_ABS = 1;
                }
                else
                {
                    MPRESS_RELEASE_DURING_ABS = 0;
                }
            }
        }

        if(ABS_DURING_ACTIVE_BRAKE_FLG==0)
        {
            if((ACTIVE_BRAKE_FLG_OLD==1) && (BRAKE_BY_MPRESS_FLG_OLD==0) && (ABS_DURING_MPRESS_FLG==0))
            {
                ABS_DURING_ACTIVE_BRAKE_FLG = 1;
            }
        }
        else
        {
            if((ACTIVE_BRAKE_FLG==0) && (BRAKE_BY_MPRESS_FLG==0))
            {
                ACTIVE_BRAKE_RELEASE_DURING_ABS = 1;
            }
            else
            {
                ACTIVE_BRAKE_RELEASE_DURING_ABS = 0;
            }
        }
    
    /* PPR 2009-209 */    
        if(lcabsu1MaintainABSatBlsStart==0)
        {
        	if(BLS_K==1)
        	{
				 #if(__REF_P_RESOL_CHANGED_10_TO_100 == ENABLE)
		        if((ABS_DURING_MPRESS_FLG==1) && (lcabsu1ValidBrakeSensor==1) && (S16_PRESS_RESOL_100_TO_10(lcabss16RefMpress)>=MPRESS_30BAR))
				 #else
		        if((ABS_DURING_MPRESS_FLG==1) && (lcabsu1ValidBrakeSensor==1) && (lcabss16RefMpress>=MPRESS_30BAR))
				 #endif
		        {
	        		lcabsu1MaintainABSatBlsStart = 1;
	        	}
	        	else if(lcabsu1FaultBLS==1)
	        	{
	        		lcabsu1MaintainABSatBlsStart = 1;
	        	}
	        	else { }
	        }
	    }
	    else
	    {
	    	if(bls_k_timer<=0)
	    	{
	    		lcabsu1MaintainABSatBlsStart = 0;
	    	}
			 #if(__REF_P_RESOL_CHANGED_10_TO_100 == ENABLE)
	    	else if((lcabsu1ValidBrakeSensor==0) || (S16_PRESS_RESOL_100_TO_10(lcabss16RefMpress)<MPRESS_15BAR))
			 #else
		    else if((lcabsu1ValidBrakeSensor==0) || (lcabss16RefMpress<MPRESS_15BAR))
			 #endif
	    	{
	    		lcabsu1MaintainABSatBlsStart = 0;
	    	}
	    	else{}
	    }
	    
	  #if __VDC && __HDC && (__HDC_ABS_IMPROVE==ENABLE)
	    if(lcabsu1ReAbsByMpresDuringHDC==0)
	    {
        	if(BLS_K==1)
        	{
		        if((ABS_DURING_ACTIVE_BRAKE_FLG==1) && (lcu1HdcActiveFlg==1))
		        {
	        		lcabsu1ReAbsByMpresDuringHDC = 1;
	        		lcabsu8ReAbsbyMpInHdcOffTimer = L_TIME_200MS;
	        	}
	        }
	        else
	        {
	        	lcabsu8ReAbsbyMpInHdcOffTimer = 0;
	        }
	    }
	    else
	    {    	
	    	if((AFZ_OK==1) && (FL.FSF==0) && (FR.FSF==0))
	    	{
	    		lcabsu1ReAbsByMpresDuringHDC = 0;
	    		lcabsu8ReAbsbyMpInHdcOffTimer = 0;
	    	}
	    }
      #endif
    }
    else
    {
        ABS_DURING_MPRESS_FLG = 0;
        MPRESS_RELEASE_DURING_ABS = 0;
        ABS_BEFORE_MPRESS_BRAKE = 0;
        ABS_DURING_ACTIVE_BRAKE_FLG = 0;
        ACTIVE_BRAKE_RELEASE_DURING_ABS = 0;
        lcabsu1MaintainABSatBlsStart = 0;
      #if __VDC && __HDC && (__HDC_ABS_IMPROVE==ENABLE)
      	if(lcabsu1ReAbsByMpresDuringHDC==1)
      	{
      		if(lcabsu8ReAbsbyMpInHdcOffTimer>0)
        	{
        		lcabsu8ReAbsbyMpInHdcOffTimer = lcabsu8ReAbsbyMpInHdcOffTimer - 1;
        	}
      		else if(lcabsu8ReAbsbyMpInHdcOffTimer==0)
      		{
      			lcabsu1ReAbsByMpresDuringHDC = 0;
      		}
      		else {}
      	}
      	else
      	{
      		lcabsu1ReAbsByMpresDuringHDC = 0;
      	}
      #endif
    }
  #endif

}

/******************************************************************************
* FUNCTION NAME:      LCABS_vSetWheelDumpThreshold
* CALLED BY:          LCABS_vCallControl()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Decide Dump threshold for ABS control
******************************************************************************/
void LCABS_vSetWheelDumpThreshold(void)
{
    LCABS_vSetBrakingStateFlg();
    LCABS_vSetWheelDumpThresWL(&FL);
    LCABS_vSetWheelDumpThresWL(&FR);
    LCABS_vSetWheelDumpThresWL(&RL);
    LCABS_vSetWheelDumpThresWL(&RR);
}

void LCABS_vSetWheelDumpThresWL(struct W_STRUCT *WL_temp)
{
    WL=WL_temp;                         /* for Compile */
    LCABS_vCapturePEAKCEL();            /* capture the peak acceleration and deceleration */
    LCABS_vCapturePEAKSLIP();           /* capture peak_slip */
    LCABS_vSetTHRdV();                  /* determine vfiltn */
    LCABS_vSetTHRDECEL();               /* determine thres_decel */
}

void LCABS_vCapturePEAKCEL(void)
{
    tempF0=0;
    if(ABS_fz==0) {
        tempF0=1;
        WL->peak_acc_afz=0;
        WL->peak_dec_max=0;
        WL->max_arad_instbl=0;
    }
    else {
        if(WL->state == DETECTION) {
            if(RESET_PEAKCEL_wl == 0) {
                if(WL->arad <= 0) {
                    tempF0=1;
                    RESET_PEAKCEL_wl=1;
                }
            }
        }
    }

    if(tempF0==1) {
        WL->peak_acc=0;
        WL->peak_dec=-1;
        WL->peak_dec_alt=-1;
    }

    if(ABS_wl==1) {
        if(WL->arad >= 0) {
            if(WL->arad > WL->peak_acc) {
                WL->peak_acc=WL->arad;
                WL->peak_acc_alt=WL->peak_acc;
                WL->peak_dec=-1;

                if(STABIL_wl==0) {WL->max_arad_instbl=WL->peak_acc;}
            }
        }
        else {
            if(WL->arad < WL->peak_dec) {
                WL->peak_dec=WL->arad;
                WL->peak_dec_alt=WL->peak_dec;
                WL->peak_acc=0;
            }
            if(WL->arad<WL->peak_dec_max) {WL->peak_dec_max=WL->arad;}
        }

        if(WL->arad >= 0) {
            if(WL->arad < WL-> peak_acc) {PEAK_ACC_PASS_wl=1;}

            if(WL->zyklus_z < 1) {
                if(WL->peak_acc_afz < WL->peak_acc) {
                    WL->peak_acc_afz=(uint8_t)WL->peak_acc;
                }
            }
        }
        else {
            if((WL->state == UNSTABLE) && (WL->s_diff<=0)) {PEAK_ACC_PASS_wl=0;}
        }
    }
    
    if(WL->s_diff>0)
    {
        if(WL->s_diff >= WL->MaxPositiveSdiff)
        {
            WL->MaxPositiveSdiff=WL->s_diff;
        }
        else { }
    }
    else
    {
        WL->MaxPositiveSdiff=0;
    }
}

void LCABS_vCapturePEAKSLIP(void)
{
    if((ABS_wl==0) || (STAB_A_wl==1) || (WL->SL_Dump_Start==1)) {   /* Added by CSH '02.NZ Winter */
        WL->peak_slip_old=WL->peak_slip;
        WL->peak_slip=0;

/*      WL->peak_slip_reserved=0; */
    }

    if(WL->state != DETECTION) {
        WL->slip_recovery_counter = WL->slip_recovery_counter + 1;
    }
    if(WL->peak_slip > WL->rel_lam) {   /* Added by CSH '02.NZ Winter */
        WL->peak_slip=WL->rel_lam;
        WL->slip_recovery_counter=0;
    }

    else {                              /* Added by CSH '03.NZ Winter */
/*
        if(WL->peak_slip_reserved!=WL->peak_slip) {
            WL->peak_slip_reserved=WL->peak_slip;
        }
*/
    }

    if(WL->slip_recovery_counter>200) {WL->slip_recovery_counter=200;}

    if(INSIDE_ABS_fz==1) {
        if(FSF_wl==0) {ABS_wl=1;}
    }
}

void LCABS_vSetTHRdV(void)
{
    if(ABS_wl==0) {LCABS_vSetTHRdVoutABS();}
    else 
    {
		LCABS_vSetTHRdVinABS();
    }
    LCABS_vCompTHRdVforRoughness();
  #if __dV_COMP_OVERSHOOT
    LCABS_vCompTHRdVforOvershoot();
  #endif
  #if __VDC
    LCABS_vCompTHRdVforESPcontrol();
  #endif
}

void LCABS_vSetTHRdVoutABS(void)
{
    tempW2=vref;                        /* calculate vfiltn */
/*    WL->vfilt_timer=0; */
    RES_FRQ_wl=0;
    HOLD_RES_wl=0;
    ARAD_3G_POS_wl=0;

    if(REAR_WHEEL_wl==0) {
        if((ABS_fl==1)||(ABS_fr==1)) {
            tempW1=DV_BASE_PRO_FOR_FRONT_IN_ABS4;

            if(GMA_SLIP_wl==1) {
                /* dV% at clearing GMA_SLIP_wl */
                if(WL->s0_reapply_counter >= U8_GMA_WL_DV_SET_COUNTER_REF){
                    tempW1 = (int16_t)U8_DV_BASE_PRO_FOR_GMA_WL;
                }
                else {
                    tempW1=DV_BASE_PRO_FOR_FRONT_IN_ABS8;
                }
            }

        }
        else {
            LCABS_vSetTHRdVoutABSPerSlip();
            /*
            if(LAM_5P_MINUS_wl==1) {tempW1=DV_BASE_PRO_FOR_FRONT_OUT_ABS2;}
            else if(LAM_4P_MINUS_wl==1) {tempW1=DV_BASE_PRO_FOR_FRONT_OUT_ABS3;}
            else if(LAM_3P_MINUS_wl==1) {tempW1=DV_BASE_PRO_FOR_FRONT_OUT_ABS4;}
            else if(LAM_2P_MINUS_wl==1) {tempW1=DV_BASE_PRO_FOR_FRONT_OUT_ABS5;}
            else if(LAM_1P_MINUS_wl==1) {tempW1=DV_BASE_PRO_FOR_FRONT_OUT_ABS6;}
            else {tempW1=DV_BASE_PRO_FOR_FRONT_OUT_ABS7;}
            */
        }
              #if (__OUT_ABS_DV_THRESHOLD_CHANGE==ENABLE)
            tempW4 = LCABS_s16Interpolation2P(vref,VREF_20_KPH,VREF_50_KPH,U8_F_OUT_DV_COMP_LOW_SPD,U8_F_OUT_DV_COMP_HI_SPD);
            tempW1 = (tempW1 * tempW4)/10;
              #endif
    }
    else {
        if(ABS_fz==1) {     /* 6803 by KGY */
          #if __REAR_1ST_CYCLE_DV
            LCABS_vSetTHRdVoutABSPerSlip();
            /*
            if(LAM_5P_MINUS_wl==1) {tempW1=DV_BASE_PRO_FOR_FRONT_OUT_ABS2;}
            else if(LAM_4P_MINUS_wl==1) {tempW1=DV_BASE_PRO_FOR_FRONT_OUT_ABS3;}
            else if(LAM_3P_MINUS_wl==1) {tempW1=DV_BASE_PRO_FOR_FRONT_OUT_ABS4;}
            else if(LAM_2P_MINUS_wl==1) {tempW1=DV_BASE_PRO_FOR_FRONT_OUT_ABS5;}
            else if(LAM_1P_MINUS_wl==1) {tempW1=DV_BASE_PRO_FOR_FRONT_OUT_ABS6;}
            else {tempW1=DV_BASE_PRO_FOR_FRONT_OUT_ABS7;}
            */

            if(vref > (int16_t)VREF_80_KPH) {tempW9=DV_BASE_PRO_FOR_REAR_OUT_ABS4;}
            else if(vref <= (int16_t)VREF_50_KPH)
            {
              #if (__BUMP_COMP_IMPROVE==ENABLE)
				if(PossibleABSForBumpVehicle==1)
				{
		            tempW4 = LCABS_s16Interpolation2P(vref,VREF_20_KPH,VREF_50_KPH,U8_R_DV_COMP_LOW_SPD_BMP,U8_R_DV_COMP_HI_SPD_BMP);
		            tempW1 = (tempW1 * tempW4)/10;
		            tempW9=64;/*V_BASE_PRO_FOR_REAR_OUT_ABS25*/
	        	}
	        	else if((FL.PossibleABSForBump==1)||(FR.PossibleABSForBump==1))
	        	{
	        		tempW4 = LCABS_s16Interpolation2P(vref,VREF_20_KPH,VREF_50_KPH,U8_R_DV_COMP_LOW_SPD_WHL_BMP,U8_R_DV_COMP_HI_SPD_WHL_BMP);
		            tempW1 = (tempW1 * tempW4)/10;
		            tempW9=51;/*V_BASE_PRO_FOR_REAR_OUT_ABS20*/
	        	}
	        	else
	        	{
	        		tempW9=DV_BASE_PRO_FOR_REAR_OUT_ABS6;
	        	}
              #else
            	tempW9=DV_BASE_PRO_FOR_REAR_OUT_ABS6;
              #endif
            }            
            else
            {tempW9= (((VREF_80_KPH-vref)*(DV_BASE_PRO_FOR_REAR_OUT_ABS6-DV_BASE_PRO_FOR_REAR_OUT_ABS4))/VREF_30_KPH) + DV_BASE_PRO_FOR_REAR_OUT_ABS4;}

            if(tempW1 > tempW9) {tempW1=tempW9;}
          #else
            if(vref > (int16_t)VREF_80_KPH) {tempW1=DV_BASE_PRO_FOR_REAR_OUT_ABS4;}
            else if(vref <= (int16_t)VREF_50_KPH) {tempW1=DV_BASE_PRO_FOR_REAR_OUT_ABS6;}
            else
            {tempW1= (((VREF_80_KPH-vref)*(DV_BASE_PRO_FOR_REAR_OUT_ABS6-DV_BASE_PRO_FOR_REAR_OUT_ABS4))/VREF_30_KPH) + DV_BASE_PRO_FOR_REAR_OUT_ABS4;}
          #endif

        }
        else {tempW1=DV_BASE_PRO_FOR_REAR_OUT_ABS6;}
    }

    u16mul16div();

    if(REAR_WHEEL_wl==0) {
        tempW0=(int16_t)U8_DV_BASE_LOW_FOR_F_OUT_ABS;                  /* 3 kph */
                
        if(vref < (int16_t)VREF_40_KPH) {
            tempW0=(int16_t)U8_DV_BASE_LOW_F_OUT_ABS_B40KPH;  		   /* 2 kph */
        }
        if((gma_counter_fl >= L_TIME_210MS) || (gma_counter_fr >= L_TIME_210MS)) {
            if(((LEFT_WHEEL_wl==1)&&(MSL_BASE_rr==1)) || ((LEFT_WHEEL_wl==0)&&(MSL_BASE_rl==1))) {  /* 03 USA High Mu */
                tempW0=(int16_t)U8_DV_BASE_LOW_FOR_GMA;                /* 1 kph */
            }
        }
        tempW1=(int16_t)U8_DV_BASE_HIGH_FOR_F_OUT_ABS;                 /* 6 kph */

      #if __PAC
		if(lcpacu1PacActiveFlg==1)
		{
			if((LCPACFL.u8ConvenReapplyTime >= U8_PRE_ABS_MIN_ALLOWED_PULSE_UP)||(LCPACFR.u8ConvenReapplyTime >= U8_PRE_ABS_MIN_ALLOWED_PULSE_UP))
			{
				tempW3 = (int16_t)U8_DV_PAC_F_OUT_ABS;			
				tempW0 = (int16_t)U8_DV_BASE_LOW_FOR_PAC;
			}
		}
      #endif        
    }
    else {
        tempW0=(int16_t)U8_DV_BASE_LOW_FOR_R_OUT_ABS;                   /* 2.5 kph */
        tempW1=(int16_t)U8_DV_BASE_HIGH_FOR_R_OUT_ABS;                  /* 4 kph */
    }

    if(ARAD_AT_MINUS_B_wl==0) {
      #if (ABS_CONTROL_MIN_SPEED_5KPH==ENABLE)
        if((vref <= (int16_t)(VREF_10_KPH))&&(ABS_fz==0))
        {
        	if((Rough_road_detect_vehicle==0)&&(Rough_road_suspect_vehicle==0)&&(lcabsu1BrakeByBLS==1)
        	#if __ECU==ESP_ECU_1
        	&&(BRAKE_BY_MPRESS_FLG==1)
        	#endif
        	)
        	{
        		WL->vfilt_comp = 0;
        	}
        	else
        	{
        		WL->vfilt_comp=VREF_4_KPH;
        	}
        }
        else
        {
        	WL->vfilt_comp=VREF_4_KPH;
        }
      #else
  		WL->vfilt_comp=VREF_4_KPH;
	  #endif
    }
    else {
        WL->vfilt_comp -= VREF_1_KPH;
        if(WL->vfilt_comp < 0) {WL->vfilt_comp=0;}
    }

/*
    tempW3 += WL->vfilt_comp;

    if(tempW3 < tempW0) {tempW3=tempW0;}
    else if(tempW3 > tempW1) {tempW3=tempW1;}
    else { }
    WL->vfiltn=tempW3;
*/
  #if __VDC && __HDC && (__HDC_ABS_IMPROVE==ENABLE)
    if((lcabsu1ReAbsByMpresDuringHDC==1) && (Rough_road_detect_vehicle==0) && (Rough_road_suspect_vehicle==0))
    {
	    WL->vfiltn=LCABS_s16LimitMinMax((tempW3+WL->vfilt_comp),tempW0,WL->vfiltn);
	}
	else
  #endif /* __VDC && __HDC && (__HDC_ABS_IMPROVE==ENABLE) */
    {
	    WL->vfiltn=LCABS_s16LimitMinMax((tempW3+WL->vfilt_comp),tempW0,tempW1);
	}
	
  #if __VDC
    if(ABS_fz==0)
    {
        if(ACTIVE_BRAKE_FLG==0)
        {
  			if((BRAKE_BY_MPRESS_FLG==0) && (lcabsu1BrakeByBLS==0))
            {
                WL->vfiltn = WL->vfiltn + (tempW3/2);
            }
            else if(BRAKE_BY_MPRESS_FLG==0)
            {
                WL->vfiltn = WL->vfiltn + (tempW3/3);
            }
  			else if(lcabsu1BrakeByBLS==0)
            {
                WL->vfiltn = WL->vfiltn + (tempW3/5);
            }
            else { }
        }
        else
        {
            if((Rough_road_detect_vehicle==0) && (Rough_road_suspect_vehicle==0))
            {
                if(vref<=VREF_10_KPH)
                {
                    if(WL->vfiltn >= VREF_2_5_KPH) {WL->vfiltn = VREF_2_5_KPH;}
                    else { }
                }
                else
                {
                    if(WL->vfiltn >= (vref/4)) {WL->vfiltn = (vref/4);}
                    else { }
                }
            }
        }
    }
  #else
  
    if((ABS_fz==0) && (lcabsu1BrakeByBLS==0))
    {
        WL->vfiltn = WL->vfiltn + (tempW3/3);
    }
    else { }
  #endif

  #if (__4WD==1)||(__4WD_VARIANT_CODE==ENABLE)
    #if(__4WD_VARIANT_CODE==ENABLE)
    if((lsu8DrvMode==DM_AUTO) || (lsu8DrvMode==DM_4H))
    #endif
    {
        if((ABS_fz==0) && (REAR_WHEEL_wl==0)) {
            if(LONG_ACCEL_STATE==1) {
                WL->vfilt_Accel2ABS_comp=(vref*3)/10;
            }

/*            if((vref <= VREF_10_KPH) || ((LONG_ACCEL_STATE==0) && ((WL->vrad-vref) > VREF_1_KPH))) */

            if((LONG_ACCEL_STATE==0) && ((WL->vrad-vref) < VREF_1_KPH))
            {
                WL->vfilt_Accel2ABS_comp=0;
            }
        }
        else
        {
            WL->vfilt_Accel2ABS_comp=0;
        }

        WL->vfiltn = WL->vfiltn + WL->vfilt_Accel2ABS_comp;
    }
  #endif

}

void LCABS_vSetTHRdVoutABSPerSlip(void)
{
    if(LAM_5P_MINUS_wl==1) {tempW1=DV_BASE_PRO_FOR_FRONT_OUT_ABS2;}
    else if(LAM_4P_MINUS_wl==1) {tempW1=DV_BASE_PRO_FOR_FRONT_OUT_ABS3;}
    else if(LAM_3P_MINUS_wl==1) {tempW1=DV_BASE_PRO_FOR_FRONT_OUT_ABS4;}
    else if(LAM_2P_MINUS_wl==1) {tempW1=DV_BASE_PRO_FOR_FRONT_OUT_ABS5;}
    else if(LAM_1P_MINUS_wl==1) {tempW1=DV_BASE_PRO_FOR_FRONT_OUT_ABS6;}
    else {tempW1=DV_BASE_PRO_FOR_FRONT_OUT_ABS7;}
}

void LCABS_vSetTHRdVinABS(void)
{

    int16_t Ref_Mu;
    Ref_Mu=afz;
    tempW2=vref;                        /* calculate vfiltn */
    tempW5=tempW1=tempW7=tempW8=0;

    if(GMA_SLIP_wl==1) {
        if(vref < (int16_t)VREF_15_KPH) {
            WL->vfiltn=VREF_1_5_KPH;
        }
        else {
/*          tempW1=DV_BASE_PRO_FOR_FRONT_IN_ABS8; */
            if(WL->s0_reapply_counter >= U8_GMA_WL_DV_SET_COUNTER_REF)
            {
                tempW1 = (int16_t)U8_DV_BASE_PRO_FOR_GMA_WL;
            }
            else {
                tempW1=DV_BASE_PRO_FOR_FRONT_IN_ABS8;
            }
            tempW2=vref;
            u16mul16div();
/*
        if(tempW3>(int16_t)U8_dV_Higher_Limit_F) {WL->vfiltn=(int16_t)U8_dV_Higher_Limit_F;}
        else {WL->vfiltn=(int16_t)tempW3;}
*/    
            WL->vfiltn = LCABS_s16LimitMinMax((int16_t)tempW3,VREF_1_KPH,(int16_t)U8_dV_Higher_Limit_F);
        }
    }
    else {
        if(REAR_WHEEL_wl==0) { /* front wheel */
            if(((gma_counter_fl >= L_TIME_210MS) || (gma_counter_fr >= L_TIME_210MS)|| (LFC_Split_flag==1)) &&
              (((LEFT_WHEEL_wl==1)&&(MSL_BASE_rr==1)) || ((LEFT_WHEEL_wl==0)&&(MSL_BASE_rl==1)))) {  /* 03 USA High Mu   //GMA */
                if(vref < (int16_t)VREF_10_KPH) {
                    WL->vfiltn=(int16_t)U8_dV_SpMu_10kph_F;
                }
                else if(vref < (int16_t)VREF_20_KPH) {
                    WL->vfiltn=LCABS_s16Interpolation2P(vref,VREF_10_KPH,VREF_20_KPH,(int16_t)U8_dV_SpMu_10kph_F,(int16_t)U8_dV_SpMu_20kph_F);
                }
                else if(vref < (int16_t)VREF_40_KPH) {
                    WL->vfiltn=LCABS_s16Interpolation2P(vref,VREF_20_KPH,VREF_40_KPH,(int16_t)U8_dV_SpMu_20kph_F,(int16_t)U8_dV_SpMu_40kph_F);
                }
                else if(vref < (int16_t)VREF_60_KPH) {
                    WL->vfiltn=LCABS_s16Interpolation2P(vref,VREF_40_KPH,VREF_60_KPH,(int16_t)U8_dV_SpMu_40kph_F,(int16_t)U8_dV_SpMu_60kph_F);
                }
                else if(vref < (int16_t)VREF_80_KPH) {
                    WL->vfiltn=LCABS_s16Interpolation2P(vref,VREF_60_KPH,VREF_80_KPH,(int16_t)U8_dV_SpMu_60kph_F,(int16_t)U8_dV_SpMu_80kph_F);
                }
                else if(vref < (int16_t)VREF_100_KPH) {
                    WL->vfiltn=LCABS_s16Interpolation2P(vref,VREF_80_KPH,VREF_100_KPH,(int16_t)U8_dV_SpMu_80kph_F,(int16_t)U8_dV_SpMu_100kph_F);
                }
                else {
                    tempW5=(int16_t)U8_dV_SpMu_100kph_F;  /* lower limit */

                    tempW1=(int16_t)U8_dV_Pro_High_Vel_SpMu_F;
                    tempW2=vref;
                    u16mul16div();

                    if(tempW5>tempW3) {
                        WL->vfiltn=tempW5;
                    }
                    else {
                        WL->vfiltn=tempW3;
                    }
                }
            }
            else {
                if((LOW_to_HIGH_suspect==1) || (WL->LOW_to_HIGH_suspect2==1) 
              #if(__L2H_IMPROVE_BY_WHEEL_PRESS==ENABLE)
                || (lcabsu1L2hSuspectByWhPres==1)
              #endif
                )
                {
                    Ref_Mu = -(int16_t)U8_PULSE_HIGH_MU;
                }
              #if __REDUCE_SPLIT_LOWSIDE_SLIP
                else if((CERTAIN_SPLIT_flag==1)&&(LFC_Split_flag==1)&&
                  (((LEFT_WHEEL_wl==0)&&(MSL_BASE_rr==1))||((LEFT_WHEEL_wl==1)&&(MSL_BASE_rl==1))))
                {
                   #if (__PRESS_EST && __EST_MU_FROM_SKID)
                    if(WL->lcabsu8PossibleMuFromSkid > 0)
                    {
                        if(WL->lcabsu8PossibleMuFromSkid <= U8_PULSE_LOW_MU)
                        {
                            Ref_Mu=-(int16_t)U8_PULSE_LOW_MU;
                        }
                        else if(WL->lcabsu8PossibleMuFromSkid <= U8_PULSE_UNDER_LOW_MED_MU)
                        {
                            Ref_Mu=-(int16_t)U8_PULSE_UNDER_LOW_MED_MU;
                        }
                        else { }
                    }
                    else
                   #endif
                    {
                      #if __PRESS_EST_ABS
                        if(WL->LOW_MU_SUSPECT_by_IndexMu==1)
                        {
                            Ref_Mu = -(int16_t)U8_PULSE_LOW_MU;
                        }
                      #endif
                    }
                }
              #endif
                else { }

                if(AFZ_OK==1) {
                    if(vref < (int16_t)VREF_10_KPH) {
                        WL->vfiltn=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)U8_dV_LMu_10kph_F,(int16_t)U8_dV_LMMu_10kph_F,(int16_t)U8_dV_MMu_10kph_F,(int16_t)U8_dV_HMu_10kph_F);
                    }
                    else if(vref < (int16_t)VREF_20_KPH) {
                        tempW1=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)U8_dV_LMu_10kph_F,(int16_t)U8_dV_LMMu_10kph_F,(int16_t)U8_dV_MMu_10kph_F,(int16_t)U8_dV_HMu_10kph_F);
                        tempW2=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)U8_dV_LMu_20kph_F,(int16_t)U8_dV_LMMu_20kph_F,(int16_t)U8_dV_MMu_20kph_F,(int16_t)U8_dV_HMu_20kph_F);

                        WL->vfiltn=LCABS_s16Interpolation2P(vref,VREF_10_KPH,VREF_20_KPH,tempW1,tempW2);
                    }
                    else if(vref < (int16_t)VREF_40_KPH) {
                        tempW1=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)U8_dV_LMu_20kph_F,(int16_t)U8_dV_LMMu_20kph_F,(int16_t)U8_dV_MMu_20kph_F,(int16_t)U8_dV_HMu_20kph_F);
                        tempW2=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)U8_dV_LMu_40kph_F,(int16_t)U8_dV_LMMu_40kph_F,(int16_t)U8_dV_MMu_40kph_F,(int16_t)U8_dV_HMu_40kph_F);

                        WL->vfiltn=LCABS_s16Interpolation2P(vref,VREF_20_KPH,VREF_40_KPH,tempW1,tempW2);
                    }
                    else if(vref < (int16_t)VREF_60_KPH) {
                        tempW1=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)U8_dV_LMu_40kph_F,(int16_t)U8_dV_LMMu_40kph_F,(int16_t)U8_dV_MMu_40kph_F,(int16_t)U8_dV_HMu_40kph_F);
                        tempW2=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)U8_dV_LMu_60kph_F,(int16_t)U8_dV_LMMu_60kph_F,(int16_t)U8_dV_MMu_60kph_F,(int16_t)U8_dV_HMu_60kph_F);

                        WL->vfiltn=LCABS_s16Interpolation2P(vref,VREF_40_KPH,VREF_60_KPH,tempW1,tempW2);
                    }
                    else if(vref < (int16_t)VREF_80_KPH) {
                        tempW1=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)U8_dV_LMu_60kph_F,(int16_t)U8_dV_LMMu_60kph_F,(int16_t)U8_dV_MMu_60kph_F,(int16_t)U8_dV_HMu_60kph_F);
                        tempW2=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)U8_dV_LMu_80kph_F,(int16_t)U8_dV_LMMu_80kph_F,(int16_t)U8_dV_MMu_80kph_F,(int16_t)U8_dV_HMu_80kph_F);

                        WL->vfiltn=LCABS_s16Interpolation2P(vref,VREF_60_KPH,VREF_80_KPH,tempW1,tempW2);
                    }
                    else if(vref < (int16_t)VREF_100_KPH) {
                        tempW1=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)U8_dV_LMu_80kph_F,(int16_t)U8_dV_LMMu_80kph_F,(int16_t)U8_dV_MMu_80kph_F,(int16_t)U8_dV_HMu_80kph_F);
                        tempW2=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)U8_dV_LMu_100kph_F,(int16_t)U8_dV_LMMu_100kph_F,(int16_t)U8_dV_MMu_100kph_F,(int16_t)U8_dV_HMu_100kph_F);

                        WL->vfiltn=LCABS_s16Interpolation2P(vref,VREF_80_KPH,VREF_100_KPH,tempW1,tempW2);
                    }
                    else {
                        tempW5=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)U8_dV_LMu_100kph_F,(int16_t)U8_dV_LMMu_100kph_F,(int16_t)U8_dV_MMu_100kph_F,(int16_t)U8_dV_HMu_100kph_F);  /* lower limit */

                        tempW1=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)U8_dV_Pro_High_Vel_LMu_F,(int16_t)U8_dV_Pro_High_Vel_LMMu_F,(int16_t)U8_dV_Pro_High_Vel_MMu_F,(int16_t)U8_dV_Pro_High_Vel_HMu_F);
                        tempW2=vref;
                        u16mul16div();

                        if(tempW5>tempW3) {
                            WL->vfiltn=tempW5;
                        }
                        else {
                            WL->vfiltn=tempW3;
                        }
                    }
                }
                else {/* before AFZ_OK */
        /*         if (((gma_counter_fl >= 30) || (gma_counter_fr >= 30)|| (LFC_Split_flag==1)) &&
                   (((LEFT_WHEEL_wl==1)&&(MSL_BASE_rr==1)) || ((LEFT_WHEEL_wl==0)&&(MSL_BASE_rl==1)))) {  // 03 USA High Mu   //GMA
                    if(vref < (int16_t)VREF_15_KPH) {
                        WL->vfiltn=dV_Split_Mu_Below_15kph_Before_AFZOK_Front;
                    }
                    else if(vref < (int16_t)VREF_35_KPH) {
                        WL->vfiltn=LCABS_s16Interpolation2P(vref,VREF_15_KPH,VREF_35_KPH,dV_Split_Mu_Below_15kph_Before_AFZOK_Front,dV_Split_Mu_20kph_Before_AFZOK_Front);
                    }
                    else if(vref < (int16_t)VREF_50_KPH) {
                        WL->vfiltn=LCABS_s16Interpolation2P(vref,VREF_35_KPH,VREF_50_KPH,dV_Split_Mu_20kph_Before_AFZOK_Front,dV_Split_Mu_40kph_Before_AFZOK_Front);
                    }
                    else if(vref < (int16_t)VREF_80_KPH) {
                        WL->vfiltn=LCABS_s16Interpolation2P(vref,VREF_50_KPH,VREF_80_KPH,dV_Split_Mu_40kph_Before_AFZOK_Front,dV_Split_Mu_60kph_Before_AFZOK_Front);
                    }
                    else if(vref < (int16_t)VREF_100_KPH) {
                        WL->vfiltn=LCABS_s16Interpolation2P(vref,VREF_80_KPH,VREF_100_KPH,dV_Split_Mu_60kph_Before_AFZOK_Front,dV_Split_Mu_80kph_Before_AFZOK_Front);
                    }
                    else {
                        tempW1=High_Velocity_dV_Percent_Split_Mu_Front;
                        tempW2=vref;
                        u16mul16div();

                        if(tempW3>(int16_t)U8_dV_Higher_Limit_F) tempW3=(int16_t)U8_dV_Higher_Limit_F;

                        if(tempW5>tempW3) {
                            WL->vfiltn=tempW5;
                        }
                        else {
                            WL->vfiltn=tempW3;
                        }
                    }
                }
                else */
                    if(vref < (int16_t)VREF_10_KPH) {
                        WL->vfiltn=(int16_t)U8_dV_BF_AFZOK_10kph_F;
                    }
                    else if(vref < (int16_t)VREF_20_KPH) {
                        WL->vfiltn=LCABS_s16Interpolation2P(vref,VREF_10_KPH,VREF_20_KPH,(int16_t)U8_dV_BF_AFZOK_10kph_F,(int16_t)U8_dV_BF_AFZOK_20kph_F);
                    }
                    else if(vref < (int16_t)VREF_40_KPH) {
                        WL->vfiltn=LCABS_s16Interpolation2P(vref,VREF_20_KPH,VREF_40_KPH,(int16_t)U8_dV_BF_AFZOK_20kph_F,(int16_t)U8_dV_BF_AFZOK_40kph_F);
                    }
                    else if(vref < (int16_t)VREF_60_KPH) {
                        WL->vfiltn=LCABS_s16Interpolation2P(vref,VREF_40_KPH,VREF_60_KPH,(int16_t)U8_dV_BF_AFZOK_40kph_F,(int16_t)U8_dV_BF_AFZOK_60kph_F);
                    }
                    else if(vref < (int16_t)VREF_80_KPH) {
                        WL->vfiltn=LCABS_s16Interpolation2P(vref,VREF_60_KPH,VREF_80_KPH,(int16_t)U8_dV_BF_AFZOK_60kph_F,(int16_t)U8_dV_BF_AFZOK_80kph_F);
                    }
                    else if(vref < (int16_t)VREF_100_KPH) {
                        WL->vfiltn=LCABS_s16Interpolation2P(vref,VREF_80_KPH,VREF_100_KPH,(int16_t)U8_dV_BF_AFZOK_80kph_F,(int16_t)U8_dV_BF_AFZOK_100kph_F);
                    }
                    else {
                        tempW5=(int16_t)U8_dV_BF_AFZOK_100kph_F;

                        tempW1=(int16_t)U8_High_Vel_dV_Pro_BF_AFZOK_F;
                        tempW2=vref;
                        u16mul16div();

                        if(tempW5>tempW3) {
                            WL->vfiltn=tempW5;
                        }
                        else {
                            WL->vfiltn=tempW3;
                        }
                    }
                }
            }
            /*
            if(WL->vfiltn>(int16_t)U8_dV_Higher_Limit_F) {WL->vfiltn=(int16_t)U8_dV_Higher_Limit_F;}
            else if(WL->vfiltn<(int16_t)VREF_1_KPH) {WL->vfiltn=(int16_t)VREF_1_KPH;}
            else{}
            */
            
            WL->vfiltn = LCABS_s16LimitMinMax(WL->vfiltn,VREF_1_KPH,(int16_t)U8_dV_Higher_Limit_F);
        }
        else { /* rear wheel */
            if((LOW_to_HIGH_suspect==1) || (WL->LOW_to_HIGH_suspect2==1) 
          #if(__L2H_IMPROVE_BY_WHEEL_PRESS==ENABLE)  
            || (lcabsu1L2hSuspectByWhPres==1)
          #endif  
            )
            {
                Ref_Mu = -(int16_t)U8_PULSE_HIGH_MU;
            }
          #if __REDUCE_SPLIT_LOWSIDE_SLIP
            else if((CERTAIN_SPLIT_flag==1)&&(LFC_Split_flag==1)&&
              (((LEFT_WHEEL_wl==0)&&(MSL_BASE_rr==1))||((LEFT_WHEEL_wl==1)&&(MSL_BASE_rl==1))))
            {
               #if (__PRESS_EST && __EST_MU_FROM_SKID)
                if(WL->lcabsu8PossibleMuFromSkid > 0)
                {
                    if(WL->lcabsu8PossibleMuFromSkid <= U8_PULSE_LOW_MU)
                    {
                        Ref_Mu=-(int16_t)U8_PULSE_LOW_MU;
                    }
                    else if(WL->lcabsu8PossibleMuFromSkid <= U8_PULSE_UNDER_LOW_MED_MU)
                    {
                        Ref_Mu=-(int16_t)U8_PULSE_UNDER_LOW_MED_MU;
                    }
                    else { }
                }
                else
               #endif
                {
                  #if __PRESS_EST_ABS
                    if(WL->LOW_MU_SUSPECT_by_IndexMu==1)
                    {
                        Ref_Mu = -(int16_t)U8_PULSE_LOW_MU;
                    }
                  #endif
                }
            }
          #endif
        	else{}

            if (AFZ_OK==1) {
                if(Ref_Mu <-(int16_t)(U8_PULSE_UNDER_LOW_MED_MU))
                {
                    if(UN_GMA_wl==1) {Ref_Mu=-(int16_t)(U8_PULSE_UNDER_LOW_MED_MU);}
                }

                if (vref < (int16_t)VREF_10_KPH) {
                    WL->vfiltn=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)U8_dV_LMu_10kph_R,(int16_t)U8_dV_LMMu_10kph_R,(int16_t)U8_dV_MMu_10kph_R,(int16_t)U8_dV_HMu_10kph_R);
                }
                else if (vref < (int16_t)VREF_20_KPH) {
                    tempW1=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)U8_dV_LMu_10kph_R,(int16_t)U8_dV_LMMu_10kph_R,(int16_t)U8_dV_MMu_10kph_R,(int16_t)U8_dV_HMu_10kph_R);
                    tempW2=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)U8_dV_LMu_20kph_R,(int16_t)U8_dV_LMMu_20kph_R,(int16_t)U8_dV_MMu_20kph_R,(int16_t)U8_dV_HMu_20kph_R);

                    WL->vfiltn=LCABS_s16Interpolation2P(vref,VREF_10_KPH,VREF_20_KPH,tempW1,tempW2);
                }
                else if (vref < (int16_t)VREF_40_KPH) {
                    tempW1=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)U8_dV_LMu_20kph_R,(int16_t)U8_dV_LMMu_20kph_R,(int16_t)U8_dV_MMu_20kph_R,(int16_t)U8_dV_HMu_20kph_R);
                    tempW2=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)U8_dV_LMu_40kph_R,(int16_t)U8_dV_LMMu_40kph_R,(int16_t)U8_dV_MMu_40kph_R,(int16_t)U8_dV_HMu_40kph_R);

                    WL->vfiltn=LCABS_s16Interpolation2P(vref,VREF_20_KPH,VREF_40_KPH,tempW1,tempW2);
                }
                else if (vref < (int16_t)VREF_60_KPH) {
                    tempW1=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)U8_dV_LMu_40kph_R,(int16_t)U8_dV_LMMu_40kph_R,(int16_t)U8_dV_MMu_40kph_R,(int16_t)U8_dV_HMu_40kph_R);
                    tempW2=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)U8_dV_LMu_60kph_R,(int16_t)U8_dV_LMMu_60kph_R,(int16_t)U8_dV_MMu_60kph_R,(int16_t)U8_dV_HMu_60kph_R);

                    WL->vfiltn=LCABS_s16Interpolation2P(vref,VREF_40_KPH,VREF_60_KPH,tempW1,tempW2);
                }
                else if (vref < (int16_t)VREF_80_KPH) {
                    tempW1=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)U8_dV_LMu_60kph_R,(int16_t)U8_dV_LMMu_60kph_R,(int16_t)U8_dV_MMu_60kph_R,(int16_t)U8_dV_HMu_60kph_R);
                    tempW2=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)U8_dV_LMu_80kph_R,(int16_t)U8_dV_LMMu_80kph_R,(int16_t)U8_dV_MMu_80kph_R,(int16_t)U8_dV_HMu_80kph_R);

                    WL->vfiltn=LCABS_s16Interpolation2P(vref,VREF_60_KPH,VREF_80_KPH,tempW1,tempW2);
                }
                else if (vref < (int16_t)VREF_100_KPH) {
                    tempW1=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)U8_dV_LMu_80kph_R,(int16_t)U8_dV_LMMu_80kph_R,(int16_t)U8_dV_MMu_80kph_R,(int16_t)U8_dV_HMu_80kph_R);
                    tempW2=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)U8_dV_LMu_100kph_R,(int16_t)U8_dV_LMMu_100kph_R,(int16_t)U8_dV_MMu_100kph_R,(int16_t)U8_dV_HMu_100kph_R);

                    WL->vfiltn=LCABS_s16Interpolation2P(vref,VREF_80_KPH,VREF_100_KPH,tempW1,tempW2);
                }
                else {
                    tempW5=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)U8_dV_LMu_100kph_R,(int16_t)U8_dV_LMMu_100kph_R,(int16_t)U8_dV_MMu_100kph_R,(int16_t)U8_dV_HMu_100kph_R);  /* lower limit */
                    tempW1=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)U8_dV_Pro_High_Vel_LMu_R,(int16_t)U8_dV_Pro_High_Vel_LMMu_R,(int16_t)U8_dV_Pro_High_Vel_MMu_R,(int16_t)U8_dV_Pro_High_Vel_HMu_R);
                    tempW2=vref;
                    u16mul16div();

                    if (tempW5>tempW3) {
                        WL->vfiltn=tempW5;
                    }
                    else {
                        WL->vfiltn=tempW3;
                    }
                }
            }
            else { /* before AFZ_OK */
                if (vref < (int16_t)VREF_10_KPH) {
                    WL->vfiltn=(int16_t)U8_dV_BF_AFZOK_10kph_R;
                }
                else if (vref < (int16_t)VREF_20_KPH) {
                    WL->vfiltn=LCABS_s16Interpolation2P(vref,VREF_10_KPH,VREF_20_KPH,(int16_t)U8_dV_BF_AFZOK_10kph_R,(int16_t)U8_dV_BF_AFZOK_20kph_R);
                }
                else if (vref < (int16_t)VREF_40_KPH) {
                    WL->vfiltn=LCABS_s16Interpolation2P(vref,VREF_20_KPH,VREF_40_KPH,(int16_t)U8_dV_BF_AFZOK_20kph_R,(int16_t)U8_dV_BF_AFZOK_40kph_R);
                }
                else if (vref < (int16_t)VREF_60_KPH) {
                    WL->vfiltn=LCABS_s16Interpolation2P(vref,VREF_40_KPH,VREF_60_KPH,(int16_t)U8_dV_BF_AFZOK_40kph_R,(int16_t)U8_dV_BF_AFZOK_60kph_R);
                }
                else if (vref < (int16_t)VREF_80_KPH) {
                    WL->vfiltn=LCABS_s16Interpolation2P(vref,VREF_60_KPH,VREF_80_KPH,(int16_t)U8_dV_BF_AFZOK_60kph_R,(int16_t)U8_dV_BF_AFZOK_80kph_R);
                }
                else if (vref < (int16_t)VREF_100_KPH) {
                    WL->vfiltn=LCABS_s16Interpolation2P(vref,VREF_80_KPH,VREF_100_KPH,(int16_t)U8_dV_BF_AFZOK_80kph_R,(int16_t)U8_dV_BF_AFZOK_100kph_R);
                }
                else {
                    tempW5=(int16_t)U8_dV_BF_AFZOK_100kph_R;

                    tempW1=(int16_t)U8_High_Vel_dV_Pro_BF_AFZOK_R;
                    tempW2=vref;
                    u16mul16div();

                    if (tempW5>tempW3) {
                        WL->vfiltn=tempW5;
                    }
                    else {
                        WL->vfiltn=tempW3;
                    }
                }
            }
			WL->vfiltn = LCABS_s16LimitMinMax(WL->vfiltn,VREF_1_KPH,(int16_t)U8_dV_Higher_Limit_R);
        }
        /*
        if (WL->vfiltn>(int16_t)U8_dV_Higher_Limit_R) {WL->vfiltn=(int16_t)U8_dV_Higher_Limit_R;}
        else if(WL->vfiltn<(int16_t)VREF_1_KPH) {WL->vfiltn=(int16_t)VREF_1_KPH;}
        else{}
        */
    }
}

void LCABS_vCompTHRdVforRoughness(void)
{
    if(STABIL_wl==1) {
        if(WL->arad > 0) {
            if(ABS_wl==0) {
                if(EBD_RA==0) {WL->holding_cycle_timer=L_TIME_60MS;}
            }

            if(ABS_wl==0) {
                if(vref > (int16_t)VREF_80_KPH)
                {
                    tempW0= (int16_t)WL->arad * (int16_t)VFILT_ROUGH_PER_PEAK_ACC;/* 0.5 kph/ 1g */
                }
                  #if (__OUT_ABS_DV_THRESHOLD_CHANGE==ENABLE)
                else if(vref > (int16_t)VREF_40_KPH)
                {
                	tempW0 = (int16_t)WL->arad * (int16_t)VFILT_ROUGH_PER_OUTABS;/* 3 kph/ 1g */
                }
                else
                {
                	tempW0 = (int16_t)WL->arad * (int16_t)VFILT_ROUGH_PER_OUTABS_LOW_SPD;/* 4.5 kph/ 1g */
                }
                  #else
                else
                {
                	tempW0 = (int16_t)WL->arad * (int16_t)VFILT_ROUGH_PER_OUTABS_LOW_SPD;/* 4.5 kph/ 1g */
                }
                  #endif    /*__OUT_ABS_DV_THRESHOLD_CHANGE==ENABLE)*/             
                         
                if(tempW0 > WL->vfilt_rough) {
                    WL->vfilt_rough=tempW0;

                    if((ABS_fl==1)||(ABS_fr==1)) {WL->vfilt_rough=0;}

                    if(ABS_fz==1) {
                        if((HIGH_VFILT_fl==0)&&(HIGH_VFILT_fr==0)) {WL->vfilt_rough=0;}
                    }
                }
            }
        }
        else {
            if(ABS_wl==0) {
                if(vref >= (int16_t)VREF_80_KPH) {   /* 00,winter,xd_40,snow/hi_speed */
                    if(REAR_WHEEL_wl==0) {WL->holding_cycle_timer=0;}
                }

                if(WL->holding_cycle_timer >= L_1SCAN) {WL->holding_cycle_timer = WL->holding_cycle_timer - L_1SCAN;}
                else {
                    WL->vfilt_rough -= VFILT_ROUGH_DEC;
                    if(WL->vfilt_rough < 0) {WL->vfilt_rough=0;}
                }
            }
        }

    }
    else {
        WL->holding_cycle_timer=0;
    }

#if __REAR_MI
    if((vfilt_rough_fl + vfilt_rough_fr) < (int16_t)(VREF_15_KPH)) {    /* mi03b */
#endif
        if(ABS_wl==0) {
            if(REAR_WHEEL_wl==1) {
                if((ABS_fl==1) || (ABS_fr==1)) {WL->vfilt_rough=0;}
            }
        }
#if __REAR_MI
    }
#endif

    if(WL->vfilt_rough > VREF_12_KPH) {WL->vfilt_rough = VREF_12_KPH;} /* Higher Limit of dV before ABS_fz */

/* #if __PWM */ /* 2006.06.15  K.G.Y */
/*******************************************************************************************/
    if(ABS_wl == 1){
        WL->vfilt_rough=0;

      #if __UCC_COOPERATION_CONTROL
        if(UCC_DETECT_BUMP_Vehicle_flag==1) WL->vfilt_rough = VREF_3_KPH;
      #endif

        if(REAR_WHEEL_wl==0) {
            tempB0 = S8_Rough_road_detector_F_level0;
            tempB1 = S8_Rough_road_detector_F_level1;
            tempB2 = S8_Rough_road_detector_F_level2;
        }
        else {
            tempB0 = S8_Rough_road_detector_R_level0;
            tempB1 = S8_Rough_road_detector_R_level1;
            tempB2 = S8_Rough_road_detector_R_level2;
        }
      #if (__ROUGH_COMP_IMPROVE	== ENABLE)
        if(REAR_WHEEL_wl==0)
        {
        	tempW4 = LCABS_s16Interpolation2P(vref,((int16_t)U8_ROUGH_SPEED_LEVEL1*8),((int16_t)U8_ROUGH_SPEED_LEVEL2*8),10,(int16_t)U8_ROUGH_THRES_GAIN_LEVEL2);
			tempW4 = ((int16_t)S8_Counter_threshold_Upper_Lim_F*tempW4)/10 ;            	                                  
        }
        else
        {
        	tempW4 = LCABS_s16Interpolation2P(vref,((int16_t)U8_ROUGH_SPEED_LEVEL1*8),((int16_t)U8_ROUGH_SPEED_LEVEL2*8),10,(int16_t)U8_ROUGH_THRES_GAIN_LEVEL2_R);
        	tempW4 = ((int16_t)S8_Counter_threshold_Upper_Lim_R*tempW4)/10 ;  
        }           
      #else
        tempW4 = LCABS_s16Interpolation2P(vref,((int16_t)U8_ROUGH_SPEED_LEVEL1*8),((int16_t)U8_ROUGH_SPEED_LEVEL2*8),10,(int16_t)U8_ROUGH_THRES_GAIN_LEVEL2);
        tempW4 = ((int16_t)S8_Counter_threshold_Upper_Lim_F*tempW4)/10 ;                                
      #endif/*__ROUGH_COMP_IMPROVE*/

        if((Rough_road_detect_vehicle == 1)||(Rough_road_suspect_vehicle == 1)){
            if(WL->Rough_road_detector5 >= tempB2)      {WL->vfilt_rough = VREF_17_KPH;}
            else if(WL->Rough_road_detector5 >= tempB1) {WL->vfilt_rough = VREF_15_KPH;}
            else if(WL->Rough_road_detector4 >= tempB2) {WL->vfilt_rough = VREF_12_KPH;}
            else if(WL->Rough_road_detector4 >= tempB1) {WL->vfilt_rough = VREF_10_KPH;}
            else if(WL->Rough_road_detector3 >= tempB2) {WL->vfilt_rough = VREF_6_KPH;}
            else if(WL->Rough_road_detector3 >= tempB1) {WL->vfilt_rough = VREF_5_KPH;}
            else if(WL->Rough_road_detector2 >= tempB2) {WL->vfilt_rough = VREF_4_KPH;}
            else if(WL->Rough_road_detector2 >= tempB1) {WL->vfilt_rough = VREF_3_KPH;}
            else if(WL->Rough_road_detector  >= tempB2) {WL->vfilt_rough = VREF_2_KPH+tempW4;}
            else if(WL->Rough_road_detector  >= tempB1) {WL->vfilt_rough = VREF_1_KPH+tempW4;}
            else {WL->vfilt_rough = VREF_2_KPH;}
        }
        else{
            if(WL->Rough_road_detector5 >= tempB2)      {WL->vfilt_rough = VREF_15_KPH;}
            else if(WL->Rough_road_detector4 >= tempB2) {WL->vfilt_rough = VREF_10_KPH;}
            else if(WL->Rough_road_detector3 >= tempB2) {WL->vfilt_rough = VREF_5_KPH;}
            else if(WL->Rough_road_detector2 >= tempB2) {WL->vfilt_rough = VREF_3_KPH;}
            else if(WL->Rough_road_detector  >= tempB2) {WL->vfilt_rough = VREF_1_KPH+tempW4;}
            else if(WL->Rough_road_detector  >= tempB1) {WL->vfilt_rough = VREF_0_5_KPH+tempW4;}
            else { }
        }

        if((vref<=VREF_10_KPH)&&(WL->vfilt_rough>VREF_3_KPH)) {WL->vfilt_rough = VREF_3_KPH;}
        if((LFC_Split_flag==1)&&(REAR_WHEEL_wl==0)) {
            if(((LEFT_WHEEL_wl==1) && (UN_GMA_fr==1)) || ((LEFT_WHEEL_wl==0) && (UN_GMA_fl==1))) {WL->vfilt_rough=0;}
        }
    }

/*******************************************************************************************/
/* #endif */ /* 2006.06.15  K.G.Y */

    WL->vfiltn += WL->vfilt_rough;

    tempW2=vref;
    tempW1=VFILTN_MAX_PRO;
    u16mul16div();

  #if (__4WD==1)||(__4WD_VARIANT_CODE==ENABLE)
    if(WL->vfilt_Accel2ABS_comp>0)
    {
        if(tempW3<VREF_15_KPH) {tempW3=VREF_15_KPH;}
    }
  #endif

    if(tempW3 > VREF_1_KPH) {
    if(WL->vfiltn > tempW3) {WL->vfiltn=tempW3;}
    }
    else if(WL->vfiltn < VREF_1_KPH) {WL->vfiltn=VREF_1_KPH;}
    else { }
}

  #if __dV_COMP_OVERSHOOT
void LCABS_vCompTHRdVforOvershoot(void)
{
    uint8_t u8dVOvershootTimer;
  #if __REDUCE_ABS_TIME_AT_BUMP
    uint8_t u8dVOvershootMax;
  #endif

  #if __REDUCE_ABS_TIME_AT_BUMP
    if(WL->PossibleABSForBump==1)
    {
        /*u8dVOvershootTimer = T_200_MS;*/
	  #if (__BUMP_COMP_IMPROVE==ENABLE)
        u8dVOvershootTimer=(uint8_t)LCABS_s16Interpolation2P((int16_t)vref,S16_BUMP_MED_SPD,S16_BUMP_HI_SPD,L_TIME_200MS,L_TIME_400MS); 
 	  #else
		u8dVOvershootTimer=L_TIME_200MS;
	  #endif/*__BUMP_COMP_IMPROVE*/
    }
    else
    {
        u8dVOvershootTimer = L_TIME_100MS;
    }
  #else /*__REDUCE_ABS_TIME_AT_BUMP*/

    u8dVOvershootTimer = L_TIME_80MS; /* 84 ms*/

  #endif /*__REDUCE_ABS_TIME_AT_BUMP*/
    
   #if (__UNSTABLE_REAPPLY_dV_COMP && __UNSTABLE_REAPPLY_ENABLE)
/*    if((WL->Reapply_Accel_cycle==1)&&(WL->LFC_STABIL_counter<=L_TIME_100MS)&&(WL->vfiltn < VREF_3_KPH))
    {
        if(REAR_WHEEL_wl==0) {
            if(WL->High_dump_factor < 2500)
            {
                WL->vfilt_comp_overshoot = dV_COMP_At_dP_Ctrl_CYCLE_F;
            }
            else
            {
                WL->vfilt_comp_overshoot = 0;
            }
        }
        else {
            if(WL->High_dump_factor < 2000)
            {
                WL->vfilt_comp_overshoot = dV_COMP_At_dP_Ctrl_CYCLE_R;
            }
            else
            {
                WL->vfilt_comp_overshoot = 0;
            }
        }

        if(WL->vfilt_comp_overshoot > (VREF_3_KPH - WL->vfiltn))
        {
            WL->vfilt_comp_overshoot = VREF_3_KPH - WL->vfiltn;
        }
    }
    else
    {
        WL->vfilt_comp_overshoot = 0;
    }
*/
  #endif

    if(WL->arad >= 0) {
        if(WL->Dec_after_Overshoot==0) {
            if((WL->vfilt_comp_overshoot==0) && (WL->LFC_STABIL_counter<=L_TIME_50MS))
            {
                if(WL->max_arad_instbl>WL->arad)
                {
                    lcabss16temp0=(int16_t)(WL->max_arad_instbl);
                }
                else
                {
                    lcabss16temp0=(int16_t)(WL->arad);
                }
                
                if((WL->max_arad_instbl >= ARAD_12G0) || (WL->arad >= ARAD_12G0))				                	
                {
                    WL->vfilt_comp_overshoot=(lcabss16temp0/2); /* 0.25KPH / 1g */
                    /*
                    if(WL->max_arad_instbl>WL->arad)
                    {
                        WL->vfilt_comp_overshoot=((int16_t)(WL->max_arad_instbl)/2); // 0.25KPH / 1g
                    }
                    else
                    {
                        WL->vfilt_comp_overshoot=((int16_t)(WL->arad)/2); // 0.25KPH / 1g
                    }*/
                }
              #if __UNSTABLE_REAPPLY_ENABLE
                else if((WL->Reapply_Accel_cycle==1) && (WL->LFC_zyklus_z>1))
                {
                    if((AFZ_OK==1) && (afz>=-(int16_t)U8_PULSE_LOW_MED_MU))
                    {
                        WL->vfilt_comp_overshoot=(lcabss16temp0/4); /* 0.125KPH / 1g */
                        /*
                        if(WL->max_arad_instbl>WL->arad)
                        {
                            WL->vfilt_comp_overshoot=((int16_t)(WL->max_arad_instbl)/4); // 0.25KPH / 1g
                        }
                        else
                        {
                            WL->vfilt_comp_overshoot=((int16_t)(WL->arad)/4); // 0.25KPH / 1g
                        }
                        */
                    }
                    else
                    {
                        WL->vfilt_comp_overshoot=(lcabss16temp0/2); /* 0.25KPH / 1g */
                        /*
                        if(WL->max_arad_instbl>WL->arad)
                        {
                            WL->vfilt_comp_overshoot=((int16_t)(WL->max_arad_instbl)/2); // 0.25KPH / 1g
                        }
                        else
                        {
                            WL->vfilt_comp_overshoot=((int16_t)(WL->arad)/2); // 0.25KPH / 1g
                        }
                        */
                    }
                }
              #endif
              #if (__REDUCE_ABS_TIME_AT_BUMP == ENABLE)
              	else if((WL->PossibleABSForBump==1)&&((WL->max_arad_instbl >= S8_BUMP_PEAK_ACC_REF_1_DUMP) || (WL->arad >= S8_BUMP_PEAK_ACC_REF_1_DUMP)))
			  	{
			  		WL->vfilt_comp_overshoot=(lcabss16temp0/2); /* 0.25KPH / 1g */
			  	}
              #endif              
                else { }
			  #if (__BUMP_COMP_IMPROVE	== ENABLE)
			  	if(((WL->PossibleABSForBump==1)||(PossibleABSForBumpVehicle==1))&&(WL->Reapply_Accel_cycle==0))
			  	{
			  		lcabss16temp1 = LCABS_s16Interpolation2P(vref,(int16_t)VREF_30_KPH,(int16_t)VREF_50_KPH,(int16_t)VREF_3_KPH,(int16_t)VREF_5_KPH);
			  	}
			  #else
			  	lcabss16temp1 = VREF_3_KPH;
			  #endif  
                if(WL->vfilt_comp_overshoot>lcabss16temp1) {WL->vfilt_comp_overshoot=lcabss16temp1;}                
            }
            
	      #if __REDUCE_ABS_TIME_AT_BUMP
	        if(REAR_WHEEL_wl==1)
	        {
	            if(PossibleABSForBumpVehicle==1)
	            {
	                WL->vfilt_comp_overshoot = (WL->vfiltn*(int16_t)U8_DV_INC_RATIO_AT_BUMP_R)/10;
	                u8dVOvershootMax = U8_DV_OVERSHOOT_MAX_AT_BUMP_R;
	            }
	            else if((FL.PossibleABSForBump==1)||(FR.PossibleABSForBump==1))
	            {
	            	WL->vfilt_comp_overshoot = (WL->vfiltn*(int16_t)U8_DV_INC_RATIO_AT_BUMP_R)/20;/*50% dV of vehicle bump*/
	            	u8dVOvershootMax = (U8_DV_OVERSHOOT_MAX_AT_BUMP_R*3)/4;/*75% of max. overshoot dV*/
	            }
	            else
	            {
	                u8dVOvershootMax = VREF_3_KPH;
	            }
	        }
	        else
	        {
	            if((WL->PossibleABSForBump==1)||(PossibleABSForBumpVehicle==1))
	            {
	                WL->vfilt_comp_overshoot = (WL->vfilt_comp_overshoot*(int16_t)U8_DV_INC_RATIO_AT_BUMP_F)/10;
	                u8dVOvershootMax = U8_DV_OVERSHOOT_MAX_AT_BUMP_F;
	            }
	            else
	            {
	                u8dVOvershootMax = VREF_3_KPH;
	            }
	        }
	        
	        if(WL->vfilt_comp_overshoot>(int16_t)u8dVOvershootMax) {WL->vfilt_comp_overshoot=(int16_t)u8dVOvershootMax;}
	      #endif /*__REDUCE_ABS_TIME_AT_BUMP*/            
        }
        else {
            /* WL->vfilt_comp_overshoot=0; */
            WL->Dec_after_Overshoot = WL->Dec_after_Overshoot + L_1SCAN;
        }     
    }
    else {
        if((WL->vfilt_comp_overshoot > 0) && (WL->Dec_after_Overshoot <= u8dVOvershootTimer))
        {
            WL->Dec_after_Overshoot = WL->Dec_after_Overshoot + L_1SCAN;
            if((afz>=-(int16_t)U8_PULSE_LOW_MED_MU) && (WL->High_dump_factor > 2000) && (WL->vrad<vref))
            {
                if(WL->vfilt_comp_overshoot>VREF_0_5_KPH) {WL->vfilt_comp_overshoot = WL->vfilt_comp_overshoot - VREF_0_75_KPH;}/*VREF_0_5_KPH*/
                else {WL->vfilt_comp_overshoot = VREF_0_KPH;}
            }
            else if(WL->Dec_after_Overshoot>=(u8dVOvershootTimer/2))
            {
                if(WL->vfilt_comp_overshoot>VREF_0_25_KPH) {WL->vfilt_comp_overshoot = WL->vfilt_comp_overshoot - VREF_0_375_KPH;}/*VREF_0_25_KPH*/
                else {WL->vfilt_comp_overshoot = VREF_0_KPH;}
            }
            else { }
        }
        else {
            WL->vfilt_comp_overshoot=0;
            WL->Dec_after_Overshoot=0;
        }
    }

    WL->vfiltn += WL->vfilt_comp_overshoot;
/*
  #if __UNSTABLE_REAPPLY_dV_COMP
    if((WL->Reapply_Accel_cycle==1)&&(Rough_road_detect_vehicle == 0)&&(Rough_road_suspect_vehicle == 0)) {
        if(WL->vfiltn < VREF_3_KPH) {
            if(REAR_WHEEL_wl==0) {
                WL->vfiltn += dV_COMP_At_dP_Ctrl_CYCLE_F;
            }
            else {
                WL->vfiltn += dV_COMP_At_dP_Ctrl_CYCLE_R;
            }

            if(WL->vfiltn > VREF_3_KPH) WL->vfiltn = VREF_3_KPH;
        }
    }
  #endif
*/
}
  #endif

#if __VDC
void LCABS_vCompTHRdVforESPcontrol(void)
{
	int16_t	s16DvEscComp = WL->vfiltn;
  #if __ESP_ABS_COOPERATION_CONTROL
    if(lcabsu1ValidYawStrLatG==1)
    {        
        if(WL->lcabsu8AbsEscCoopLevel>0)
        {
        	if((Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1)||(AFZ_OK==0))
        	{
				/*keep the original dV*/
        	}
        	else if(afz<-(int16_t)U8_PULSE_MED_HIGH_MU)/*high mu*/
        	{
    			if(WL->LFC_s0_reapply_counter>2) 
            	{
            		if(WL->lcabsu8AbsEscCoopLevel>=U8_ABS_ESC_COOP_LEVEL2) 
            		{
            			s16DvEscComp=VREF_1_75_KPH;
            		}
            	}                
        	}
        	else
        	{
    			if(REAR_WHEEL_wl==0) 
    			{
    				if((WL->GMA_SLIP==0) || (WL->s0_reapply_counter>=U8_GMA_WL_DV_SET_COUNTER_REF)) 
    				{
		    			if(CERTAIN_SPLIT_flag==1)
		    			{
		    				if(WL->LFC_s0_reapply_counter>2)
		                	{
			        			if(WL->lcabsu8AbsEscCoopLevel>=U8_ABS_ESC_COOP_LEVEL2)  {s16DvEscComp=VREF_1_5_KPH;}			                	
			                	else{}
		                	}		                
		                }
		                else
		                {
		                	if(WL->LFC_s0_reapply_counter>2) 
		                	{
			        			if(WL->lcabsu8AbsEscCoopLevel>=U8_ABS_ESC_COOP_LEVEL2) {s16DvEscComp=VREF_0_5_KPH;}
			                	else if(WL->lcabsu8AbsEscCoopLevel>=U8_ABS_ESC_COOP_LEVEL1) {s16DvEscComp=VREF_1_KPH;}
			                	else{}
		                	}
		                }
		            }
    			}
    			else
    			{
    				if(WL->LFC_s0_reapply_counter>2)
					{    				
	        			if(WL->lcabsu8AbsEscCoopLevel>=U8_ABS_ESC_COOP_LEVEL2) {s16DvEscComp=VREF_0_5_KPH;}
		                else if(WL->lcabsu8AbsEscCoopLevel>=U8_ABS_ESC_COOP_LEVEL1) {s16DvEscComp=VREF_1_KPH;}
	                	else{}
	                }
    			}
        	}

	        WL->vfiltn=((s16DvEscComp)>=WL->vfiltn)?WL->vfiltn:s16DvEscComp;  	
    	}
    	else{}
    }
  #endif
  
  #if (__VDC && __ABS_CORNER_BRAKING_IMPROVE)
    if(WL->lcabsu1CornerRearOutCtrl==1)
    {	
    	if(vref>=VREF_60_KPH)
    	{
    		if(WL->vfiltn>VREF_2_KPH) {WL->vfiltn=VREF_2_KPH;}
    	}	
    	else if(vref>=VREF_20_KPH)
        {
        	if(WL->vfiltn>VREF_1_KPH) {WL->vfiltn=VREF_1_KPH;}
        }
        else{}
    }
    else{}
  #endif  
}
#endif

#if     __DUMP_THRES_COMP_for_Spold_update
void LCABS_vCompTHRdVforSpoldUpdate(void)
{
    int16_t TARGET_DUMP_dV;

    tempW0=0;
    WL->spold_comp=0;

    if ((AFZ_OK==1) && (BEND_DETECT2 == 0)&&(Rough_road_detect_vehicle == 0)&&(Rough_road_suspect_vehicle == 0)) {
/*
        tempW1=WL->vfiltn;
        tempW2=100;
        tempW0=vref;
        s16muls16divs16();
        tempB1=(int8_t)(tempW3);
*/
        if(WL->rel_lam < (-LAM_3P)) {
            if(afz>=-(int16_t)U8_PULSE_UNDER_LOW_MED_MU)
            {
                TARGET_DUMP_dV = VREF_1_KPH + (vref/33); /*3%*/
            }
            else if(afz>=-(int16_t)U8_PULSE_MED_HIGH_MU)
            {
                TARGET_DUMP_dV = VREF_1_KPH + (vref/20); /*5%*/
            }
            else
            {
                TARGET_DUMP_dV = VREF_1_KPH + (vref/25); /*4%*/
            }

            WL->spold_comp = vref-WL->vrad+WL->vfiltn - TARGET_DUMP_dV;

            if(WL->spold_comp >= VREF_1_5_KPH) {WL->spold_comp = VREF_1_5_KPH;}
            else if(WL->spold_comp < VREF_0_KPH) {WL->spold_comp = VREF_0_KPH;}
            else { }

/*
                if (vref < (int16_t)VREF_15_KPH) {
                    tempW1= (int16_t)(WL->rel_lam + tempB1 + LAM_10P);
                    if((tempW1 - (int16_t)tempB1) > 0) {tempW1 = (int16_t)(tempB1 - LAM_1P);}
                }
                else if (vref < (int16_t)VREF_35_KPH) {
                    tempW1= (int16_t)(WL->rel_lam + tempB1 + LAM_8P);
                    if((tempW1 - (int16_t)tempB1) > 0) {tempW1 = (int16_t)(tempB1 - LAM_1P);}
                }
                else if (vref < (int16_t)VREF_50_KPH) {
                    tempW1= (int16_t)(WL->rel_lam + tempB1 + LAM_3P);
                    if((tempW1 - (int16_t)tempB1) > 0) {tempW1 = (int16_t)(tempB1 - LAM_1P);}
                }
                else if (vref < (int16_t)VREF_80_KPH) {
                    tempW1= (int16_t)(WL->rel_lam + tempB1 + LAM_3P);
                    if((tempW1 - (int16_t)tempB1 > 0)) {tempW1 = (int16_t)(tempB1 - LAM_2P);}
                }
                else if (vref < (int16_t)VREF_100_KPH) {
                    tempW1= (int16_t)(WL->rel_lam + tempB1 + LAM_2P);
                    if((tempW1 - (int16_t)tempB1) > 0) {tempW1 = (int16_t)(tempB1 - LAM_2P);}
                }
                else {
                    tempW1= (int16_t)(WL->rel_lam + tempB1 + LAM_1P);
                    if((tempW1 - (int16_t)tempB1 > 0)) {tempW1 = (int16_t)(tempB1 - LAM_3P);}
                }
*/
        }
        else
        {
            WL->spold_comp = VREF_0_KPH;
        }




/*      tempW1 = WL->rel_lam + tempB1 + LAM_2P; */
/*      if(tempW1 - (int16_t)tempB1 < 0) tempW1 = (int16_t)tempB1 - LAM_1P; */
/*
        if (tempW1 < 0) {tempW1=0;}
        tempW2=vref;
        u16mul16div();
*/
/*        WL->vfiltn=(int16_t)(tempW3); */
/*      WL->spold_comp = WL->vfiltn - tempW3; */
    }
    else {WL->spold_comp = 0;}
}
#endif

void LCABS_vSetTHRDECEL(void)
{
    if((ABS_wl==0)||(WL->zyklus_z<=1)) 
    {
   		LCABS_vSetInitialTHRDECEL();
    }
    else {LCABS_vAdaptTHRDECEL();}
    LCABS_vSetSlipAtMinusB();
    
  #if (__THRDECEL_RESOLUTION_CHANGE==1)
    WL->thres_decel = (int8_t)(WL->lcabss16ThresDecel32/8);
  #endif
}

#if (__THRDECEL_RESOLUTION_CHANGE==0)
void LCABS_vSetInitialTHRDECEL(void)
{
    if(vref > (int16_t)(VREF_60_KPH)) {
      #if __REDUCE_REAR_SLIP_AT_LMU
        if((AFZ_OK==1) && (afz > -(int16_t)U8_PULSE_LOW_MED_MU))
        {
            if((REAR_WHEEL_wl==1)&&(vref>VREF_80_KPH))
            {
                tempB0 = (int8_t)(-ARAD_0G75);
            }
            else if(afz > -(int16_t)U8_LOW_MU)
            {
                tempB0 = (int8_t)(-ARAD_1G0);
            }
            else
            {
                tempB0 = (int8_t)(-ARAD_1G75);
            }
        }
        else {tempB0 = (int8_t)(-ARAD_1G75);}
      #else /* __REDUCE_REAR_SLIP_AT_LMU */
        if((AFZ_OK==1) && (afz > -(int16_t)U8_LOW_MU)) {
            tempB0 = (int8_t)(-ARAD_1G0);
        }
        else {tempB0 = (int8_t)(-ARAD_1G75);}
      #endif /* __REDUCE_REAR_SLIP_AT_LMU */
    }
    else {
        if(AFZ_OK==0) {tempB0 = (int8_t)(-ARAD_1G5);}
        else if(afz > -(int16_t)U8_LOW_MU) {tempB0 = (int8_t)(-ARAD_1G0);}
        else if(afz <= -(int16_t)U8_HIGH_MU) {tempB0 = (int8_t)(-ARAD_1G5);}
        else {
            if((LFC_Split_flag==1) && (REAR_WHEEL_wl==0) &&
              (((LEFT_WHEEL_wl==1)&&(MSL_BASE_rr==1)) || ((LEFT_WHEEL_wl==0)&&(MSL_BASE_rl==1)))) {
                 tempB0 = (int8_t)(-ARAD_1G0);
            }
            else {tempB0 = (int8_t)(-ARAD_1G5);}
        }
    }
    WL->thres_decel = tempB0;
}

void LCABS_vAdaptTHRDECEL(void)
{

  /* Set Lower Limit */
    tempB1 = (int8_t)(-ARAD_2G0);

  /* Set Higher Limit */
    if(AFZ_OK==0) {
        if(vref <= (int16_t)(VREF_40_KPH)) {tempB2 = (int8_t)(-ARAD_0G75);}
        else {tempB2 = (int8_t)(-ARAD_1G25);}
        if((REAR_WHEEL_wl==1) && (SCHNELLSTE_VREF_wl==1)) {tempB2=tempB2+ARAD_0G25;}
      #if __PRESS_EST_ABS
        if((REAR_WHEEL_wl==1) && (WL->LOW_MU_SUSPECT_by_IndexMu==1)) {tempB2 = (int8_t)(-ARAD_0G5);}
      #endif
    }
    else if(afz > -(int16_t)U8_LOW_MU) {
        tempB2 = (int8_t)(-ARAD_0G75);
      #if __PRESS_EST_ABS
        if((REAR_WHEEL_wl==1) && ((SCHNELLSTE_VREF_wl==1) || (WL->LOW_MU_SUSPECT_by_IndexMu==1))) {tempB2=tempB2+ARAD_0G25;}
      #else
        if((REAR_WHEEL_wl==1) && (SCHNELLSTE_VREF_wl==1)) {tempB2=tempB2+ARAD_0G25;}
      #endif
    }
    else if(afz > -(int16_t)U8_HIGH_MU) {
        if((vref > (int16_t)(VREF_60_KPH)) && (LFC_Split_flag==1) && (REAR_WHEEL_wl==0) &&
          (((LEFT_WHEEL_wl==1)&&(MSL_BASE_rr==1)) || ((LEFT_WHEEL_wl==0)&&(MSL_BASE_rl==1)))) {
            tempB2 = (int8_t)(-ARAD_1G0);
        }
        else {
            if(vref <= (int16_t)(VREF_40_KPH)) {tempB2 = (int8_t)(-ARAD_0G75);}
            else {tempB2 = (int8_t)(-ARAD_1G25);}
            if((REAR_WHEEL_wl==1) && (SCHNELLSTE_VREF_wl==1)) {tempB2=tempB2+ARAD_0G25;}

          #if __PRESS_EST_ABS
            if((WL->LOW_MU_SUSPECT_by_IndexMu==1) &&
             ((CERTAIN_SPLIT_flag==1)&&(LFC_Split_flag==1)&&(((LEFT_WHEEL_wl==0)&&(MSL_BASE_rr==1))||((LEFT_WHEEL_wl==1)&&(MSL_BASE_rl==1)))))
            {
                if(tempB2<(int8_t)(-ARAD_0G75)) {tempB2 = (int8_t)(-ARAD_0G75);}
            }
          #endif
        }
    }
    else {
        if(vref > (int16_t)(VREF_60_KPH)) {tempB2 = (int8_t)(-ARAD_1G75);}
        else {
            if(vref <= (int16_t)(VREF_40_KPH)) {tempB2 = (int8_t)(-ARAD_0G75);}
            else {tempB2 = (int8_t)(-ARAD_1G25);}
            if((REAR_WHEEL_wl==1) && (SCHNELLSTE_VREF_wl==1)) {tempB2=tempB2+ARAD_0G25;}
        }
    }

    if(STAB_K_wl==1) {
        tempB0 = WL->thres_decel;
      #if __REDUCE_REAR_SLIP_AT_LMU
        if((REAR_WHEEL_wl==1)&&(AFZ_OK==1)&&(afz>-(int16_t)(U8_PULSE_UNDER_LOW_MED_MU))&&(vref>VREF_80_KPH))
        {
            if(WL->peak_slip < (int8_t)(-LAM_5P)) {tempB0=tempB0+ARAD_0G25;}
            else if(WL->peak_slip >= (int8_t)(-LAM_3P)) {tempB0=tempB0-ARAD_0G25;}
            else { }
        }
        else
        {
            if(WL->peak_slip < (int8_t)(-LAM_10P)) {tempB0=tempB0+ARAD_0G25;}
            else if(WL->peak_slip >= (int8_t)(-LAM_5P)) {tempB0=tempB0-ARAD_0G25;}
            else { }
        }
      #else /*__REDUCE_REAR_SLIP_AT_LMU*/
        if(WL->peak_slip < (int8_t)(-LAM_10P)) {tempB0=tempB0+ARAD_0G25;}
        else if(WL->peak_slip >= (int8_t)(-LAM_5P)) {tempB0=tempB0-ARAD_0G25;}
        else { }
      #endif /*__REDUCE_REAR_SLIP_AT_LMU*/

/*
        if(tempB0 > tempB2) {tempB0=tempB2;}
        else if(tempB0 < tempB1) {tempB0=tempB1;}
        else { }
        WL->thres_decel=tempB0;
*/        
        WL->thres_decel = (int8_t)LCABS_s16LimitMinMax((int16_t)tempB0,(int16_t)tempB1,(int16_t)tempB2);
    }

    if((LOW_to_HIGH_suspect==1) || (WL->LOW_to_HIGH_suspect2==1)) 
    {
        if((AFZ_OK==1) && (afz>-(int16_t)(U8_HIGH_MU))) {
            if(REAR_WHEEL_wl==0) {
                if(WL->thres_decel>(-ARAD_1G5)) {WL->thres_decel = (-ARAD_1G5);}
            }
            else {
                if(WL->thres_decel>(-ARAD_1G25)) {WL->thres_decel = (-ARAD_1G25);}
            }
        }
    }
    else
    {
        tempB0 = WL->thres_decel;
        if((AFZ_OK==1) && (afz <= -(int16_t)U8_PULSE_LOW_MED_MU))
        {
            if(SPOLD_RESET_wl==1)
            {
                if(tempB0 <= -ARAD_1G25) {WL->thres_decel = tempB0 + ARAD_0G25;}
            }
        }
    }
}

#else /*(__THRDECEL_RESOLUTION_CHANGE==1)*/
void LCABS_vSetInitialTHRDECEL(void)
{
/*  
    int Ref_Mu;
    Ref_Mu=afz;
    tempW0=tempW1=tempW2=0;
        
    if(AFZ_OK==0)
    {                         
        if(vref < (int16_t)VREF_20_KPH) 
        {
            tempW0=(int16_t)(-ARAD32_1G5);          
        }
        else if(vref < (int16_t)VREF_40_KPH) 
        {
            tempW0=(int16_t)(-ARAD32_1G5);
        }
        else if(vref < (int16_t)VREF_60_KPH) 
        {
            tempW0=(int16_t)(-ARAD32_1G5);
        }
        else if(vref < (int16_t)VREF_80_KPH) 
        {
            tempW0=(int16_t)(-ARAD32_1G75);
        }
        else if(vref < (int16_t)VREF_100_KPH) 
        {
            tempW0=(int16_t)(-ARAD32_1G75);
        }
        else if(vref < (int16_t)VREF_150_KPH) 
        {
            tempW0=(int16_t)(-ARAD32_1G75);
        }
        else
        {
            tempW0=(int16_t)(-ARAD32_1G75);
        }
    }
    else     // AFZ_OK==1
    {                                
        if(vref < (int16_t)VREF_20_KPH) 
        {
            tempW0=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)(-ARAD32_1G0),(int16_t)(-ARAD32_1G5),(int16_t)(-ARAD32_1G5),(int16_t)(-ARAD32_1G5));
        }
        else if(vref < (int16_t)VREF_40_KPH) 
        {
            tempW0=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)(-ARAD32_1G0),(int16_t)(-ARAD32_1G5),(int16_t)(-ARAD32_1G5),(int16_t)(-ARAD32_1G5));
        }                   
        else if(vref < (int16_t)VREF_60_KPH) 
        {
            tempW0=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)(-ARAD32_1G0),(int16_t)(-ARAD32_1G5),(int16_t)(-ARAD32_1G5),(int16_t)(-ARAD32_1G5));
        }
        else if(vref < (int16_t)VREF_80_KPH) 
        {
            tempW0=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)(-ARAD32_1G0),(int16_t)(-ARAD32_1G75),(int16_t)(-ARAD32_1G75),(int16_t)(-ARAD32_1G75));
        }                       
        else if(vref < (int16_t)VREF_100_KPH) 
        {
            tempW0=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)(-ARAD32_1G0),(int16_t)(-ARAD32_1G75),(int16_t)(-ARAD32_1G75),(int16_t)(-ARAD32_1G75));
        }
        else  
        {
            tempW1=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)(-ARAD32_1G0),(int16_t)(-ARAD32_1G75),(int16_t)(-ARAD32_1G75),(int16_t)(-ARAD32_1G75));
            tempW2=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)(-ARAD32_1G0),(int16_t)(-ARAD32_1G75),(int16_t)(-ARAD32_1G75),(int16_t)(-ARAD32_1G75));
    
            tempW0=LCABS_s16Interpolation2P(vref,VREF_100_KPH,VREF_150_KPH,tempW1,tempW2);
        }
    }
    WL->lcabss16ThresDecel32 = tempW0;
    */

    tempW0=0;
  #if (ABS_CONTROL_MIN_SPEED_5KPH==ENABLE)
    if(vref <= (int16_t)(VREF_10_KPH))
    {      
    	if((Rough_road_detect_vehicle==0)&&(Rough_road_suspect_vehicle==0))
    	{
    		tempW0 = -ARAD32_1G0;
    	}
    	else
    	{
    		tempW0 = -ARAD32_1G5;
    	}      
    }  
    else if(vref <= (int16_t)(VREF_60_KPH)) 
    {
        tempW0 = -ARAD32_1G5;
    }
  #else
	if(vref <= (int16_t)(VREF_60_KPH))
	{
    	tempW0 = -ARAD32_1G5;
    }
  #endif
    else if(vref <= (int16_t)(VREF_100_KPH)) 
    {
        tempW0 = -ARAD32_1G75;
    }
    else 
    {
        tempW0 = -ARAD32_2G0;
    }
    
    if(AFZ_OK==1)
    {
      #if __REDUCE_REAR_SLIP_AT_LMU
        if((REAR_WHEEL_wl==1)&&(vref>VREF_80_KPH)&&(afz > -(int16_t)U8_PULSE_LOW_MED_MU))
        {
            tempW0 = (int8_t)(-ARAD_0G75);
        }
        else
      #endif /* __REDUCE_REAR_SLIP_AT_LMU */        
        if(afz > -(int16_t)U8_LOW_MU)
        {
            tempW0 = -ARAD32_1G0;
        }
        else if(afz > -(int16_t)U8_HIGH_MU)
        {
            if((LFC_Split_flag==1) && (REAR_WHEEL_wl==0) &&
              (((LEFT_WHEEL_wl==1)&&(MSL_BASE_rr==1)) || ((LEFT_WHEEL_wl==0)&&(MSL_BASE_rl==1)))) 
            {
                 tempW0 = -ARAD32_1G0;
            }
        }
        else
        {
            ;
        }
    }
    
  #if __VDC && __PRESS_EST && __EST_MU_FROM_SKID
    if((ABS_fl==1) && (ABS_fr==1) && (FL.lcabsu8PossibleMuFromSkid==U8_PULSE_LOW_MU) && (FR.lcabsu8PossibleMuFromSkid==U8_PULSE_LOW_MU) 
      && ((REAR_WHEEL_wl==1)&&(WL->rel_lam<=-LAM_7P))         
    )
    {
        tempW0 = -ARAD32_0G5;
    }
  #endif
  
  #if __VDC && __HDC && (__HDC_ABS_IMPROVE==ENABLE)
    if((lcabsu1ReAbsByMpresDuringHDC==1) && (Rough_road_detect_vehicle==0) && (Rough_road_suspect_vehicle==0))
    {
    	if(WL->lcabss16ThresDecel32 < tempW0)
    	{
    		WL->lcabss16ThresDecel32 = tempW0;
    	}
    }
    else
  #endif /* __VDC && __HDC && (__HDC_ABS_IMPROVE==ENABLE) */
    {
	    WL->lcabss16ThresDecel32 = tempW0;
	}  	
}

void LCABS_vAdaptTHRDECEL(void)
{
    tempW4 = -ARAD32_2G0;                    /* lower limit */
    tempW5 = 0;                         
    tempW5 = LCABS_s16SetHigherLimitDECEL(); /* higher limit */
        
    if(STAB_K_wl==1) 
    {
        tempW0 = WL->lcabss16ThresDecel32;        

        tempB4 = (int8_t)(LAM_2P + (((int32_t)VREF_1_5_KPH*100)/vref));
        tempB5 = (int8_t)(LAM_3P + (((int32_t)VREF_2_KPH  *100)/vref));
        tempB6 = (int8_t)(LAM_5P + (((int32_t)VREF_2_5_KPH*100)/vref));
        tempB7 = (int8_t)(LAM_6P + (((int32_t)VREF_3_5_KPH*100)/vref));
        
      #if __REDUCE_REAR_SLIP_ADAPT_AT_LMU
        if((REAR_WHEEL_wl==1)&&(AFZ_OK==1)&&(afz>-(int16_t)(U8_PULSE_UNDER_LOW_MED_MU))&&(vref>VREF_80_KPH))
        {
            tempB7 = tempB6;
            tempB6 = tempB5;
            tempB5 = tempB4;
            tempB4 = (int8_t)(LAM_2P + (((int32_t)VREF_0_5_KPH*100)/vref));            
        }
      #endif /*__REDUCE_REAR_SLIP_AT_LMU*/

        if(WL->peak_slip < -tempB7) 
        {
            tempW0+=ARAD32_0G25;
        }
        else if(WL->peak_slip < -tempB6) 
        {
            tempW0+=ARAD32_0G125;
        }
        else if(WL->peak_slip >= -tempB4) 
        {
            tempW0-=ARAD32_0G25;
        }
        else if(WL->peak_slip >= -tempB5) 
        {
            tempW0-=ARAD32_0G125;
        }
        else { }

        if(tempW0 > tempW5) 
        {
            tempW0=tempW5;
        }
        else if(tempW0 < tempW4)
        {
            tempW0=tempW4;
        }
        else { }
        
        WL->lcabss16ThresDecel32=tempW0;
    }

    if((LOW_to_HIGH_suspect==1) || (WL->LOW_to_HIGH_suspect2==1)) 
    {
        if((AFZ_OK==1) && (afz>-(int16_t)(U8_PULSE_MED_HIGH_MU))) 
        {
            if(REAR_WHEEL_wl==0) 
            {
                if(WL->lcabss16ThresDecel32>(-ARAD32_1G5)) 
                {
                    WL->lcabss16ThresDecel32 = (-ARAD32_1G5);
                }
            }
            else 
            {
                if(WL->lcabss16ThresDecel32>(-ARAD32_1G25)) 
                {
                    WL->lcabss16ThresDecel32 = (-ARAD32_1G25);
                }
            }
        }
    }
    else
    {
        if((AFZ_OK==1) && (afz <= -(int16_t)U8_PULSE_LOW_MED_MU))  /*U8_PULSE_LOW_MED_MU=-0.3g*/
        {
            if(SPOLD_RESET_wl==1)       
            {
                if(WL->lcabss16ThresDecel32 <= -ARAD32_1G25) 
                {
                    WL->lcabss16ThresDecel32 = WL->lcabss16ThresDecel32 + ARAD32_0G25;
                }
            }
        }
    }
}

static int16_t LCABS_s16SetHigherLimitDECEL(void)
{
/*  
    int16_t Ref_Mu;
    Ref_Mu=afz;
    tempW1=tempW2=tempW3=0;
    
    if(AFZ_OK==0)
    {                         
        if(vref < (int16_t)VREF_20_KPH) 
        {
            tempW3=(int16_t)(-ARAD32_0G75);
        }
        else if(vref < (int16_t)VREF_40_KPH) 
        {
            tempW3=(int16_t)(-ARAD32_0G75);
        }
        else if(vref < (int16_t)VREF_60_KPH) 
        {
            tempW3=(int16_t)(-ARAD32_1G25);
        }
        else if(vref < (int16_t)VREF_80_KPH) 
        {
            tempW3=(int16_t)(-ARAD32_1G25);
        }
        else if(vref < (int16_t)VREF_100_KPH) 
        {
            tempW3=(int16_t)(-ARAD32_1G25);
        }
        else if(vref < (int16_t)VREF_150_KPH) 
        {
            tempW3=(int16_t)(-ARAD32_1G25);
        }
        else
        {
            tempW3=(int16_t)(-ARAD32_1G25);
        }
        
        if((REAR_WHEEL_wl==1) && (SCHNELLSTE_VREF_wl==1)) 
        {
            tempW3+=ARAD32_0G25;
        }
        #if __PRESS_EST_ABS
        if((REAR_WHEEL_wl==1) && (WL->LOW_MU_SUSPECT_by_IndexMu==1)) 
        {
            tempW3 = (int16_t)(-ARAD32_0G5);
        }
        #endif
    }
    
    else    // AFZ_OK==1
    {   

        if(vref < (int16_t)VREF_20_KPH) 
        {
            tempW3=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)(-ARAD32_0G75),(int16_t)(-ARAD32_0G75),(int16_t)(-ARAD32_0G75),(int16_t)(-ARAD32_0G75));
        }
        else if(vref < (int16_t)VREF_40_KPH) 
        {
            tempW3=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)(-ARAD32_0G75),(int16_t)(-ARAD32_0G75),(int16_t)(-ARAD32_0G75),(int16_t)(-ARAD32_0G75));
        }                   
        else if(vref < (int16_t)VREF_60_KPH) 
        {
            tempW3=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)(-ARAD32_0G75),(int16_t)(-ARAD32_1G25),(int16_t)(-ARAD32_1G25),(int16_t)(-ARAD32_1G25));
        }
        else if(vref < (int16_t)VREF_80_KPH) 
        {
            tempW3=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)(-ARAD32_0G75),(int16_t)(-ARAD32_1G25),(int16_t)(-ARAD32_1G25),(int16_t)(-ARAD32_1G75));
        }                       
        else if(vref < (int16_t)VREF_100_KPH) 
        {
            tempW3=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)(-ARAD32_0G75),(int16_t)(-ARAD32_1G25),(int16_t)(-ARAD32_1G25),(int16_t)(-ARAD32_1G75));
        }
        else
        {
            tempW1=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)(-ARAD32_0G75),(int16_t)(-ARAD32_1G25),(int16_t)(-ARAD32_1G25),(int16_t)(-ARAD32_1G75));
            tempW2=LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)(-ARAD32_0G75),(int16_t)(-ARAD32_1G25),(int16_t)(-ARAD32_1G25),(int16_t)(-ARAD32_1G75));
    
            tempW3=LCABS_s16Interpolation2P(vref,VREF_100_KPH,VREF_150_KPH,tempW1,tempW2);
        }
        
        
        if(REAR_WHEEL_wl==1)
        {
            if(SCHNELLSTE_VREF_wl==1)
            {
                if(Ref_Mu > -(int16_t)U8_PULSE_UNDER_LOW_MED_MU)
                {
                    tempW3+=ARAD32_0G25;        
                }
                else if(Ref_Mu > -(int16_t)U8_PULSE_MED_HIGH_MU)
                {
                    tempW3+=ARAD32_0G25;
                }
                else if((Ref_Mu <= -(int16_t)U8_PULSE_MED_HIGH_MU)&&(vref < (int16_t)VREF_60_KPH))
                {   
                    tempW3+=ARAD32_0G25;        
                }
                else
                { 
                    ;
                }
            }
            #if __PRESS_EST_ABS
            else
            {
                if(Ref_Mu > -(int16_t)U8_PULSE_UNDER_LOW_MED_MU)
                {
                    if(WL->LOW_MU_SUSPECT_by_IndexMu==1)
                    {
                        tempW3+=ARAD32_0G25;
                    }
                }
            }
            #endif
        }
                
        if(Ref_Mu > -(int16_t)U8_PULSE_MED_HIGH_MU)
        {
            if((vref > (int16_t)(VREF_60_KPH)) && (LFC_Split_flag==1) && (REAR_WHEEL_wl==0) &&
              (((LEFT_WHEEL_wl==1)&&(MSL_BASE_rr==1)) || ((LEFT_WHEEL_wl==0)&&(MSL_BASE_rl==1)))) 
            {
                tempW3 = (int16_t)(-ARAD32_1G0);
            }
            #if __PRESS_EST_ABS
            else if((WL->LOW_MU_SUSPECT_by_IndexMu==1) &&
            ((CERTAIN_SPLIT_flag==1)&&(LFC_Split_flag==1)&&(((LEFT_WHEEL_wl==0)&&(MSL_BASE_rr==1))||((LEFT_WHEEL_wl==1)&&(MSL_BASE_rl==1)))))
            {
                if(tempW3<(int16_t)(-ARAD32_0G75)) 
                {
                    tempW3 = (int16_t)(-ARAD32_0G75);
                }
            }
            #endif
            else { }
        }
        else
        {
            ;
        }

    }
    return tempW3;
*/
    tempW3=0;
    tempW1=((vref/8)>200)?200:(vref/8);
    
/*===========================================================
= -b higher limit matching considering afz and vref
===========================================================*/

    if(AFZ_OK==1)
    {
        if(afz >- (int16_t)U8_PULSE_LOW_MU)
        {
            tempW3 = -(int16_t)LCABS_s8GetParamFromRef((uint8_t)tempW1, 40, 60, 100, ARAD32_0G75, ARAD32_0G5, ARAD32_0G5, ARAD32_0G5);
        }
        else if(afz >- (int16_t)U8_PULSE_UNDER_LOW_MED_MU)
        {
            tempW3 = -(int16_t)LCABS_s8GetParamFromRef((uint8_t)tempW1, 40, 60, 100, ARAD32_0G75, ARAD32_0G75, ARAD32_0G5, ARAD32_0G5);
        }
        else if(afz >- (int16_t)U8_PULSE_LOW_MED_MU)
        {
            tempW3 = -(int16_t)LCABS_s8GetParamFromRef((uint8_t)tempW1, 40, 60, 100, ARAD32_0G75, ARAD32_1G0, ARAD32_0G75, ARAD32_0G5);
        }
        else if(afz >- (int16_t)U8_PULSE_MEDIUM_MU)
        {
            tempW3 = -(int16_t)LCABS_s8GetParamFromRef((uint8_t)tempW1, 40, 60, 100, ARAD32_0G75, ARAD32_1G25, ARAD32_1G0, ARAD32_0G75);
        }
        else if(afz >- (int16_t)U8_PULSE_MED_HIGH_MU)
        {
            tempW3 = -(int16_t)LCABS_s8GetParamFromRef((uint8_t)tempW1, 40, 60, 100, ARAD32_1G0, ARAD32_1G25, ARAD32_1G25, ARAD32_1G0);
        }
        else
        {
            tempW3 = -(int16_t)LCABS_s8GetParamFromRef((uint8_t)tempW1, 40, 60, 100, ARAD32_1G0, ARAD32_1G25, ARAD32_1G75, ARAD32_1G75);
        }    
    }
    else
    {
        tempW3 = -(int16_t)LCABS_s8GetParamFromRef((uint8_t)tempW1, 40, 60, 100, ARAD32_0G75, ARAD32_1G25, ARAD32_1G25, ARAD32_1G25);
    }

/*===========================================================
= special cases of -b higher limit 
===========================================================*/

    if((REAR_WHEEL_wl==1) && (SCHNELLSTE_VREF_wl==1))
    {
        if((AFZ_OK==1) && (afz <= -(int16_t)U8_HIGH_MU) && (vref > (int16_t)(VREF_60_KPH)))
        {
            ;
        }
        else
        {
            tempW3 = tempW3 + ARAD32_0G25;
        }
    }
  
    if(AFZ_OK==1)
    {
        if(afz > -(int16_t)U8_LOW_MU)
        {
            ;
          #if __VDC 
            if((lcabsu1ValidYawStrLatG==1) && 
              (((yaw_out>=YAW_3DEG) && (LEFT_WHEEL_wl==1)) || ((yaw_out<=-YAW_3DEG) && (LEFT_WHEEL_wl==0))))
            {
                if(tempW3 < -ARAD32_0G5) {tempW3 = -ARAD32_0G5;}
            }   
          #endif    
        }
        else if(afz > -(int16_t)U8_HIGH_MU)
        {
            if((LFC_Split_flag==1) && (REAR_WHEEL_wl==0) && (vref > (int16_t)(VREF_60_KPH)) &&
              (((LEFT_WHEEL_wl==1)&&(MSL_BASE_rr==1)) || ((LEFT_WHEEL_wl==0)&&(MSL_BASE_rl==1)))) 
            {
                if(tempW3 < -ARAD32_1G0) {tempW3 = -ARAD32_1G0;}
            }
        }
        else { }
    }
    
  #if __PRESS_EST_ABS  
    if(WL->LOW_MU_SUSPECT_by_IndexMu==1)
    {
        if((AFZ_OK==0) || (afz > -(int16_t)U8_LOW_MU))
        {
            if(REAR_WHEEL_wl==1) 
            {
                if(tempW3 < -ARAD32_0G5) {tempW3 = -ARAD32_0G5;}
            }
        }
        else if(afz > -(int16_t)U8_HIGH_MU)
        {
            if((LFC_Split_flag==1) && (CERTAIN_SPLIT_flag==1) && (tempW3 < -ARAD32_0G75) &&
              (((LEFT_WHEEL_wl==0)&&(MSL_BASE_rr==1)) || ((LEFT_WHEEL_wl==1)&&(MSL_BASE_rl==1)))) 
            {
                 tempW3 = -ARAD32_0G75;
            }
        }
        else
        {
            ;
        }
    }
  #endif

    if(tempW3 > S16_MINUS_B_MAX) {tempW3 = S16_MINUS_B_MAX;}
                
    return tempW3;
}
#endif /* __THRDECEL_RESOLUTION_CHANGE */

void LCABS_vSetSlipAtMinusB(void)
{
    if(ABS_wl==0) {
        if(ARAD_AT_MINUS_B_wl==0) {

          #if __SPOLD_UPDATE
            LAM_1P_MINUS_wl=0;
            LAM_2P_MINUS_wl=0;
            LAM_3P_MINUS_wl=0;
            LAM_4P_MINUS_wl=0;
            LAM_5P_MINUS_wl=0;
          #endif

          #if (__THRDECEL_RESOLUTION_CHANGE==1)
            if(WL->arad_resol_change <= WL->lcabss16ThresDecel32) 
          #else
            if(WL->arad <= WL->thres_decel) 
          #endif
            {
                ARAD_AT_MINUS_B_wl=1;
                MINUS_B_SET_AGAIN_wl=1;
              #if __REAR_1ST_CYCLE_DV
                if((ABS_fz==1) && (REAR_WHEEL_wl==1))
                {
                    LCABS_vSetMinusBSlipFlg();
                    /*
                    if(WL->rel_lam <= (int8_t)(-LAM_5P)) {LAM_5P_MINUS_wl=1;}
                    else if(WL->rel_lam <= (int8_t)(-LAM_4P)) {LAM_4P_MINUS_wl=1;}
                    else if(WL->rel_lam <= (int8_t)(-LAM_3P)) {LAM_3P_MINUS_wl=1;}
                    else if(WL->rel_lam <= (int8_t)(-LAM_2P)) {LAM_2P_MINUS_wl=1;}
                    else if(WL->rel_lam <= (int8_t)(-LAM_1P)) {LAM_1P_MINUS_wl=1;}
                    else { }
                    */
                }
              #endif
            }
        }
        else {
            if(ABS_fz==0) {
                if(voptfz_diff1 > 0) {
                  #if (__THRDECEL_RESOLUTION_CHANGE==1)
                    if(WL->arad_resol_change > WL->lcabss16ThresDecel32) 
                  #else
                    if(WL->arad > WL->thres_decel) 
                  #endif 
                    {
                        ARAD_AT_MINUS_B_wl=0;

                        LAM_1P_MINUS_wl=0;
                        LAM_2P_MINUS_wl=0;
                        LAM_3P_MINUS_wl=0;
                        LAM_4P_MINUS_wl=0;
                        LAM_5P_MINUS_wl=0;
                    }
                }
            }

          #if (__THRDECEL_RESOLUTION_CHANGE==1)
            if(WL->arad_resol_change <= WL->lcabss16ThresDecel32) 
          #else
            if(WL->arad <= WL->thres_decel) 
          #endif
            {
                if(MINUS_B_SET_AGAIN_wl==0) {
                    MINUS_B_SET_AGAIN_wl=1;

                    LAM_1P_MINUS_wl=0;
                    LAM_2P_MINUS_wl=0;
                    LAM_3P_MINUS_wl=0;
                    LAM_4P_MINUS_wl=0;
                    LAM_5P_MINUS_wl=0;

                    LCABS_vSetMinusBSlipFlg();
                    /*
                    if(WL->rel_lam <= (int8_t)(-LAM_5P)) {LAM_5P_MINUS_wl=1;}
                    else if(WL->rel_lam <= (int8_t)(-LAM_4P)) {LAM_4P_MINUS_wl=1;}
                    else if(WL->rel_lam <= (int8_t)(-LAM_3P)) {LAM_3P_MINUS_wl=1;}
                    else if(WL->rel_lam <= (int8_t)(-LAM_2P)) {LAM_2P_MINUS_wl=1;}
                    else if(WL->rel_lam <= (int8_t)(-LAM_1P)) {LAM_1P_MINUS_wl=1;}
                    else { }
                    */
                }
            }
            else {MINUS_B_SET_AGAIN_wl=0;}
        }
    }
    else if(STABIL_wl==1) {
        if(ARAD_AT_MINUS_B_wl==0) {
          #if (__THRDECEL_RESOLUTION_CHANGE==1)
            if(WL->arad_resol_change <= WL->lcabss16ThresDecel32) 
          #else
            if(WL->arad <= WL->thres_decel) 
          #endif 
            {
                ARAD_AT_MINUS_B_wl=1;
                MINUS_B_SET_AGAIN_wl=1;
            }
        }
        else {
          #if (__THRDECEL_RESOLUTION_CHANGE==1)
            if(WL->arad_resol_change <= WL->lcabss16ThresDecel32) 
          #else
            if(WL->arad <= WL->thres_decel) 
          #endif
            {
                if(MINUS_B_SET_AGAIN_wl==0) {
                    MINUS_B_SET_AGAIN_wl=1;
                }
            }
            else {MINUS_B_SET_AGAIN_wl=0;}
        }
    }
    else {
        ARAD_AT_MINUS_B_wl=0;
        MINUS_B_SET_AGAIN_wl=0;
    }
}

void LCABS_vSetMinusBSlipFlg(void)
{
    if(WL->rel_lam <= (int8_t)(-LAM_5P)) {LAM_5P_MINUS_wl=1;}
    else if(WL->rel_lam <= (int8_t)(-LAM_4P)) {LAM_4P_MINUS_wl=1;}
    else if(WL->rel_lam <= (int8_t)(-LAM_3P)) {LAM_3P_MINUS_wl=1;}
    else if(WL->rel_lam <= (int8_t)(-LAM_2P)) {LAM_2P_MINUS_wl=1;}
    else if(WL->rel_lam <= (int8_t)(-LAM_1P)) {LAM_1P_MINUS_wl=1;}
    else { }
}


/* ====================================================================== */

void LCABS_vCallYawMomentReduction(void)
{
    uint8_t GMA_CONTROL_MODE=0;

    if(BLS_K==1) {abs_begin_timer=L_TIME_1200MS;}
    else if(ABS_fz==1) {
        if(abs_begin_timer >= L_1SCAN) {abs_begin_timer = abs_begin_timer - L_1SCAN;}
    }
    else {abs_begin_timer=L_TIME_1200MS;}

    if(ABS_fz==0) {
        gma_flags=0;

        YMR_check_counter=0;        /* Added by KGY 2002.11.20  */

        circuit_fail_cnt=0;

        CIRCUIT_FAIL=0;
        CERTAIN_SPLIT_flag = 0;
        /* goto CLEAR_GMA1; */
        GMA_CONTROL_MODE = CLEAR_GMA1;
    }
    else {

        if((ABS_fl==1)||(ABS_fr==1)){
            if(YMR_check_counter<=L_TIME_200MS) {YMR_check_counter = YMR_check_counter + L_1SCAN;}
            else { }

          #if (__SPLIT_TYPE==0)
            if((GMA_SLIP_fl==1)&&(FL.gma_control_counter<=L_TIME_350MS)) {   /* Added by KGY 030918 due to CIRCUIT_FAIL */
                if(FL.gma_control_counter==L_1ST_SCAN) {
                    if((RR.LFC_dump_counter+RR.EBD_Dump_counter)<=50) {
                        circuit_fail_cnt-=(int8_t)((RR.LFC_dump_counter+RR.EBD_Dump_counter)*2);
                    }
                    else {circuit_fail_cnt=-100;}
                }
            /*  if(RR.rel_lam>=-LAM_5P) */
                if((RR.rel_lam>=-LAM_5P) && (RR.rel_lam > RL.rel_lam))
                {
                    circuit_fail_cnt = circuit_fail_cnt + L_1SCAN;
                }
                else if((RR.rel_lam<=-LAM_10P)&&(circuit_fail_cnt>=-100)) {circuit_fail_cnt=circuit_fail_cnt-L_2SCAN;}
                else { }
            }
            else if((GMA_SLIP_fr==1)&&(FR.gma_control_counter<=L_TIME_350MS)) {
                if(FR.gma_control_counter==L_1ST_SCAN) {
                    if((RL.LFC_dump_counter+RL.EBD_Dump_counter)<=50) {
                        circuit_fail_cnt-=(int8_t)((RL.LFC_dump_counter+RL.EBD_Dump_counter)*2);
                    }
                    else {circuit_fail_cnt=-100;}
                }
            /*  if(RL.rel_lam>=-LAM_5P) */
                if((RL.rel_lam>=-LAM_5P) && (RL.rel_lam > RR.rel_lam))
                {
                    circuit_fail_cnt = circuit_fail_cnt + L_1SCAN;
                }
                else if((RL.rel_lam<=-LAM_10P)&&(circuit_fail_cnt>=-100)) {circuit_fail_cnt=circuit_fail_cnt-L_2SCAN;}
                else { }
            }
            else {
                circuit_fail_cnt=0;
            }
          #endif
        }

        if(((ABS_fl==1)||(ABS_fr==1)) && (circuit_fail_cnt>=L_TIME_300MS)) {
            CIRCUIT_FAIL=1;
            gma_flags=50;
            /* goto CLEAR_GMA1; */
            GMA_CONTROL_MODE = CLEAR_GMA1;
        }
        else if((afz <= (int16_t)(-AFZ_0G6))
          ||(Rough_road_detect_vehicle == 1)||((Rough_road_suspect_vehicle == 1) && (LFC_Split_flag==0))) {
            CIRCUIT_FAIL=0;
            /* goto CLEAR_GMA1; */
            GMA_CONTROL_MODE = CLEAR_GMA1;
        }

    #if __VDC
      #if __SPLIT_STABILITY_IMPROVE_GMA
        else if((YAW_CDC_WORK==1)&&(GMA_PULSE==0))
        {
            /* goto End_GMA; */
            GMA_CONTROL_MODE = End_GMA;
        }
      #else  
/*        if(((GMA_SLIP_fl)&&(SLIP_CONTROL_NEED_FL))||((GMA_SLIP_fr)&&(SLIP_CONTROL_NEED_FR))) {goto CLEAR_GMA;} */
        else if(((GMA_SLIP_fl==1)&&(ESP_BRAKE_CONTROL_FL==1))||((GMA_SLIP_fr==1)&&(ESP_BRAKE_CONTROL_FR==1)))
        {
            /* goto CLEAR_GMA; */
            GMA_CONTROL_MODE = CLEAR_GMA;
        }
        else if((YAW_CDC_WORK==1)&&(GMA_PULSE==0))
        {
            /* goto End_GMA; */
            GMA_CONTROL_MODE = End_GMA;
        }
      #endif
    #endif

        else {
        if(ABS_fl==0) {
            if(ABS_fr==1) {
                LCABS_vDctYMRforFrontOutABS(&FL, &FR, &RL, &RR); /* Modified by J.K.Lee because of code optimization */
            }
                else { }
        }
        else if(ABS_fr==0) {
                LCABS_vDctYMRforFrontOutABS(&FR, &FL, &RR, &RL); /* Modified by J.K.Lee because of code optimization */
        }

                     /* Added by KGY 2002.11.20 */
        else if(YMR_check_counter<=L_TIME_100MS) {     /* Changed by CSH 030226 */
            if(Rough_road_detect_vehicle==0){
                if((rel_lam_fl*2)>rel_lam_fr){        /* Changed By CSH 030225 */

                    LCABS_vDctYMRforFrontInABS(&FL, &FR, &RL, &RR);
                }
                else if((rel_lam_fr*2)>rel_lam_fl){

                    LCABS_vDctYMRforFrontInABS(&FR, &FL, &RR, &RL);
                }
                else { }
            }
        }

        else if(YMR_check_counter>=L_TIME_150MS) {        /* Changed by CSH 030226 */
            if(((GMA_SLIP_fl==1)&&(STABIL_fl==0)) || ((GMA_SLIP_fr==1)&&(STABIL_fr==0))){
                /* goto CLEAR_GMA; */
                GMA_CONTROL_MODE = CLEAR_GMA;
            }
        }
        else { }

/*//////////////////////////////////////////////////////////////////////////////////////// */
        if(YMR_check_counter <= (L_TIME_200MS-L_1SCAN)) {
            if(GMA_SLIP_fl==1){
                if((FR.max_arad_instbl>=S8_Clear_GMA_Max_Accel1) /* ARAD_15G0 */
                ||((FR.max_arad_instbl>=S8_Clear_GMA_Max_Accel2)&&(FR.LFC_dump_counter<=U8_DUMP_CNT_FOR_CLEAR_GMA_L1)&&(STABIL_fr==1))){
                    /* ARAD_10G0 */
                    LFC_Split_flag=0;
                    SPLIT_PULSE_rl=0;
                    gma_flags=9;
	                /* goto CLEAR_GMA1; */
	                GMA_CONTROL_MODE = CLEAR_GMA1;
                }
                    else { }
            }
            else if(GMA_SLIP_fr==1){
                if((FL.max_arad_instbl>=S8_Clear_GMA_Max_Accel1)
                ||((FL.max_arad_instbl>=S8_Clear_GMA_Max_Accel2)&&(FL.LFC_dump_counter<=U8_DUMP_CNT_FOR_CLEAR_GMA_L1)&&(STABIL_fl==1))){
                    LFC_Split_flag=0;
                    SPLIT_PULSE_rr=0;
                    gma_flags=10;
                    /* goto CLEAR_GMA1; */
                    GMA_CONTROL_MODE = CLEAR_GMA1;
                    }
                    else { }
                }
                else { }
            }
            else { }
/**********************************************************************************/

        }
    }


    if(GMA_CONTROL_MODE>=CLEAR_GMA)
    {
        if((GMA_SLIP_fl==1) && (FL.s0_reapply_counter_old<5)) {CERTAIN_SPLIT_flag = 0;}
        else if((GMA_SLIP_fr==1) && (FR.s0_reapply_counter_old<5)) {CERTAIN_SPLIT_flag = 0;}
        else { }

        GMA_PULSE=0;
        GMA_fl=0; GMA_fr=0; GMA_rl=0; GMA_rr=0;
        GMA_SLIP_fl=0; GMA_SLIP_fr=0; GMA_SLIP_rl=0; GMA_SLIP_rr=0;
	  #if __REAR_MI
        BOTH_GMA_SET_fl=0; BOTH_GMA_SET_fr=0;      /* NSM02 */
	  #else
        gma_dump_fl=0; gma_dump_fr=0; gma_dump_rl=0; gma_dump_rr=0;
	  #endif

        if(GMA_CONTROL_MODE==CLEAR_GMA1)
        {
            UN_GMA_fl=0; UN_GMA_fr=0; UN_GMA_rl=0; UN_GMA_rr=0;
            gma_counter_fl=0; gma_counter_fr=0;
            /*gma_counter_rl=0; gma_counter_rr=0;*/
            FL.gma_control_counter=0; FR.gma_control_counter=0;
          #if __ESP_SPLIT_2
            FL.GMA_ESP_Control=0; FR.GMA_ESP_Control=0;
            FL.gma_dump2=0; FR.gma_dump2=0;
          #endif
        }
        else { }
    }
    else if(GMA_CONTROL_MODE != End_GMA)
    {
        if(GMA_SLIP_fl==1){
            if(FL.gma_control_counter <= U8_TYPE_MAXNUM) {FL.gma_control_counter = FL.gma_control_counter + L_1SCAN; CERTAIN_SPLIT_flag = 1;}
        }
        else if(GMA_SLIP_fr==1){
            if(FR.gma_control_counter <= U8_TYPE_MAXNUM) {FR.gma_control_counter = FR.gma_control_counter + L_1SCAN; CERTAIN_SPLIT_flag = 1;}
        }
        else { }
    }
    else { }

  #if __OUT_ABS_P_RATE_COMPENSATION
    LCABS_vDecideOutABSCtrlForYMR();
  #endif

  #if __SPLIT_STABILITY_IMPROVE_GMA
    LCABS_vAdaptDeltaYawForYMR();
  #endif  

}

void LCABS_vDctYMRforFrontOutABS(struct W_STRUCT *pFH, struct W_STRUCT *pFL, struct W_STRUCT *pRH, struct W_STRUCT *pRL)
{
    pFH->GMA=1;

#if __REAR_MI
    if((pRH->ABS==0)&&(pRL->ABS==1)) {      /* NSM02 */
        tempB2=(pRH->rel_lam-pRL->rel_lam);
        tempW2=(pRH->vrad-pRL->vrad);
      #if __BEND_DETECT_using_SIDEVREF
        if(BEND_DETECT_flag==1)
        {
            tempW2 = tempW2 - VREF_10_KPH;
        }
      #endif

        if((pFH->BOTH_GMA_SET==1) && (vref>=(int16_t)VREF_40_KPH))
        {
            tempB1=(int8_t)LCABS_s16Interpolation2P(vref,VREF_50_KPH,VREF_100_KPH,LAM_15P,LAM_7P);
            if((tempB2>(int8_t)LAM_6P)&&(tempW2>(int16_t)VREF_3_KPH)&&
               ((pRL->peak_dec_alt<-ARAD_7G0)||((pRL->peak_dec_alt<-ARAD_2G0)&&(pRL->peak_slip<-tempB1))))
            {
                if(pFH->GMA_SLIP==0) {                      /* 02nz */
                    pFH->GMA_SLIP=1;          /* mi03e */
                  #if __ESP_SPLIT_2
                    pFH->GMA_ESP_Control=1;
                  #endif
                    if(BEND_DETECT2==0) {GMA_PULSE=1;}          /* Added by KGY 030331 */
                    pFL->UN_GMA=pRH->UN_GMA=pRL->UN_GMA=1;
                    gma_flags=1;

                    if(vref >= (int16_t)S16_GMA_H_SPEED)     {pFH->gma_dump=U8_GMA_D_F_H_SPEED;}
                    else if(vref >= (int16_t)S16_GMA_L_SPEED){pFH->gma_dump=U8_GMA_D_F_M_SPEED;} /* Added by shchoi 2002.02.14 */
                    else                                     {pFH->gma_dump=U8_GMA_D_F_L_SPEED;}
                    
                  #if __VSM
                    if(lcepsu8EPSCtrlMode == 1)
                    {
                        pFH->gma_dump=(uint8_t)((int16_t)(pFH->gma_dump*U8_EPS_GMA_DUMP_TIME_WEG)/100);
                    }
                  #endif
                }
            }
        }
    }
#endif

    if(pFH->gma_counter <= U8_TYPE_MAXNUM) {pFH->gma_counter = pFH->gma_counter + L_1SCAN;}
    tempB0=(pFH->rel_lam-pFL->rel_lam);
    tempW0=(pFH->vrad-pFL->vrad);
  #if __BEND_DETECT_using_SIDEVREF
    if(BEND_DETECT_flag==1)
    {
        tempW0 = tempW0 - VREF_10_KPH;
    }
  #endif

    if(pFH->s0_reapply_counter < 2) {

      #if __REAR_MI
        if((tempB0>(int8_t)LAM_12P)&&(tempW0>(int16_t)VREF_5_KPH)) {  /* NSM02 */
            tempB1=(int8_t)LCABS_s16Interpolation2P(vref,VREF_50_KPH,VREF_100_KPH,LAM_20P,LAM_10P);
            if((pFL->peak_dec_alt<-ARAD_10G0)||((pFL->peak_dec_alt<-ARAD_5G0)&&(pFL->peak_slip<-tempB1)))
            {
                if(BEND_DETECT2==0) {pFH->BOTH_GMA_SET=1;}
            }
        }
      #endif

        if(((vref>=(int16_t)S16_GMA_ALLOWED_LIMIT_SPEED)&&(vref<(int16_t)VREF_50_KPH)&&(tempB0>=(int8_t)S8_GMA_DCT_SLIP_DIFF_0_50_BABS)&&(tempW0>(int16_t)S16_GMA_DCT_SP_DIFF_0_50_BABS))||
           ((vref>=(int16_t)VREF_50_KPH)&&(vref<(int16_t)VREF_80_KPH)&&(tempB0>=(int8_t)S8_GMA_DCT_SLIP_DIFF_50_80_BABS)&&(tempW0>(int16_t)S16_GMA_DCT_SP_DIFF_50_80_BABS))||
           ((vref>=(int16_t)VREF_80_KPH)&&(tempB0>=(int8_t)S8_GMA_DCT_SLIP_DIFF_80_M_BABS)&&(tempW0>=(int16_t)S16_GMA_DCT_SP_DIFF_80_M_BABS)))
        {

            if(pFH->GMA_SLIP==0) {
                pFH->GMA_SLIP=1;
              #if __ESP_SPLIT_2
                pFH->GMA_ESP_Control=1;
              #endif
                if(BEND_DETECT2==0) {GMA_PULSE=1;}
                pFL->UN_GMA=pRH->UN_GMA=pRL->UN_GMA=1;
                gma_flags=2;

                if(vref>=(int16_t)S16_GMA_H_SPEED)     {pFH->gma_dump=U8_GMA_D_F_H_SPEED;}
                else if(vref>=(int16_t)S16_GMA_L_SPEED){pFH->gma_dump=U8_GMA_D_F_M_SPEED;} /* Added by shchoi 2002.02.14 */
                else                                   {pFH->gma_dump=U8_GMA_D_F_L_SPEED;}

              #if __VSM
                if(lcepsu8EPSCtrlMode == 1)
                {
                    pFH->gma_dump=(uint8_t)(((int16_t)pFH->gma_dump*U8_EPS_GMA_DUMP_TIME_WEG)/100);
                }
              #endif
            }
        }
    }
    
  #if __PWM_YMR_PLUS
    tempW1=(pRH->vrad - pRL->vrad);
    
    if((pFH->GMA_SLIP==0)&&(LFC_Split_flag==0)&&(LFC_Split_suspect_flag==0)&&(BEND_DETECT2==0))
    { 
        if((vref>=(int16_t)VREF_50_KPH)&&(tempB0>=(int8_t)LAM_10P)&&(tempW0>(int16_t)VREF_10_KPH)&&(tempW1>(int16_t)VREF_5_KPH))
        {
            pRH->MSL_BASE=0;
            pRL->MSL_BASE=1;
            pRL->SPLIT_PULSE=0;
            pRH->SPLIT_PULSE=1;
            LFC_Split_suspect_flag=1;
            pFH->LFC_maintain_split_flag=1;
            gma_flags=7;
        }
      #if __VDC && __HDC && (__HDC_ABS_IMPROVE==ENABLE)
        else if((lcu1HdcActiveFlg==1) && (ACTIVE_BRAKE_FLG==0)
         && (vref>=(int16_t)VREF_5_KPH)&&(tempB0>=(int8_t)LAM_40P)&&(tempW0>(int16_t)VREF_4_KPH))
        {
            pRH->MSL_BASE=0;
            pRL->MSL_BASE=1;
            pRL->SPLIT_PULSE=0;
            pRH->SPLIT_PULSE=1;
            LFC_Split_suspect_flag=1;
            pFH->LFC_maintain_split_flag=1;
            gma_flags=8;
        }
        else { }
      #endif /* (__HDC_ABS_IMPROVE == ENABLE) */
    }
    else { }
  #endif
}


void LCABS_vDctYMRforFrontInABS(struct W_STRUCT *pFH, struct W_STRUCT *pFL, struct W_STRUCT *pRH, struct W_STRUCT *pRL)
{
    tempB0=(pFH->rel_lam - pFL->rel_lam);
    tempW0=(pFH->vrad - pFL->vrad);
  #if __PWM_YMR_PLUS
    tempW1=(pRH->vrad - pRL->vrad);
  #endif
  #if __BEND_DETECT_using_SIDEVREF
    if(BEND_DETECT_flag==1)
    {
        tempW0 = tempW0 - VREF_10_KPH;
    }
  #endif

    if(YMR_check_counter<=L_TIME_50MS){
        if(((vref>=(int16_t)S16_GMA_ALLOWED_LIMIT_SPEED)&&(vref<(int16_t)VREF_50_KPH)&&(tempB0>=(int8_t)S8_GMA_DCT_SLIP_DIFF_0_50KPH)&&(tempW0>(int16_t)S16_GMA_DCT_SPEED_DIFF_0_50KPH))||
           ((vref>=(int16_t)VREF_50_KPH)&&(vref<(int16_t)VREF_70_KPH)&&(tempB0>=(int8_t)S8_GMA_DCT_SLIP_DIFF_50_70KPH)&&(tempW0>(int16_t)S16_GMA_DCT_SPEED_DIFF_50_70KPH))||
           ((vref>=(int16_t)VREF_70_KPH)&&(vref<(int16_t)VREF_90_KPH)&&(tempB0>=(int8_t)S8_GMA_DCT_SLIP_DIFF_70_90KPH)&&(tempW0>(int16_t)S16_GMA_DCT_SPEED_DIFF_70_90KPH))||
           ((vref>=(int16_t)VREF_90_KPH)&&(tempB0>=(int8_t)S8_GMA_DCT_SLIP_DIFF_90_M_KPH)&&(tempW0>(int16_t)S16_GMA_DCT_SPEED_DIFF_90_M_KPH)))
        {
            if(pFH->GMA_SLIP==0) {
                pFH->GMA_SLIP=1;
              #if __ESP_SPLIT_2
                pFH->GMA_ESP_Control=1;
              #endif
                pFL->UN_GMA = pRH->UN_GMA = pRL->UN_GMA = 1;
                gma_flags=5;
                if(BEND_DETECT2==0) {GMA_PULSE=1;}
                if(vref >= (int16_t)S16_GMA_H_SPEED) {
                    pFH->gma_dump=U8_GMA_D_F_H_SPEED;
                }
                else if(vref >= (int16_t)S16_GMA_L_SPEED) {
                    pFH->gma_dump=U8_GMA_D_F_M_SPEED;    /* Added by shchoi 2002.02.14 */
                }
                else {
                    pFH->gma_dump=U8_GMA_D_F_L_SPEED;
                }

              #if __VSM
                if(lcepsu8EPSCtrlMode == 1)
                {
                    pFH->gma_dump=(uint8_t)(((int16_t)pFH->gma_dump*U8_EPS_GMA_DUMP_TIME_WEG)/100);
                }
              #endif
            }
        }
    }

  #if __PWM_YMR_PLUS
    else if((LFC_Split_flag==0)&&(LFC_Split_suspect_flag==0)&&(BEND_DETECT2==0)){ /* Added by J.K.Lee 04.02.23 */
        if(((vref<(int16_t)VREF_50_KPH)&&(tempB0>=(int8_t)LAM_14P)&&(tempW0>(int16_t)VREF_5_KPH)&&(tempW1>(int16_t)VREF_3_KPH))||
          ((vref>=(int16_t)VREF_50_KPH)&&(tempB0>=(int8_t)LAM_10P)&&(tempW0>(int16_t)VREF_7_KPH)&&(tempW1>(int16_t)VREF_4_KPH)))
        {
            pRH->MSL_BASE=0;
            pRL->MSL_BASE=1;
            pRL->SPLIT_PULSE=0;
            pRH->SPLIT_PULSE=1;
            LFC_Split_suspect_flag=1;
            pFH->LFC_maintain_split_flag=1;
/*          pFL->UN_GMA = pRH->UN_GMA = pRL->UN_GMA = 1; */
            gma_flags=6;
        }
      #if __VDC && __HDC && (__HDC_ABS_IMPROVE==ENABLE)
        else if((lcu1HdcActiveFlg==1) && (ACTIVE_BRAKE_FLG==0)
         && (vref>=(int16_t)VREF_5_KPH)&&(tempB0>=(int8_t)LAM_30P)&&(tempW0>(int16_t)VREF_3_KPH))
        {
            pRH->MSL_BASE=0;
            pRL->MSL_BASE=1;
            pRL->SPLIT_PULSE=0;
            pRH->SPLIT_PULSE=1;
            LFC_Split_suspect_flag=1;
            pFH->LFC_maintain_split_flag=1;
            gma_flags=4;
        }
        else { }
      #endif /* (__HDC_ABS_IMPROVE == ENABLE) */
    }
  #endif
    else { }
}

#if __SPLIT_STABILITY_IMPROVE_GMA
void LCABS_vAdaptDeltaYawForYMR(void)
{
    if(ABS_fz==0)
    {
        GMA_STATE = 0;
        lcabsu8DelaYawExcessCnt = 0;
        lcabsu8DelaYawStableCnt = 0;
        FL.lcabss16YawDumpFactor =   FR.lcabss16YawDumpFactor =   RL.lcabss16YawDumpFactor =   RR.lcabss16YawDumpFactor =  0;
        FL.lcabsu8YawDumpHoldTime =  FR.lcabsu8YawDumpHoldTime =  RL.lcabsu8YawDumpHoldTime =  RR.lcabsu8YawDumpHoldTime = L_TIME_700MS;
        FL.lcabsu8YawRiseHoldTime =  FR.lcabsu8YawRiseHoldTime =  RL.lcabsu8YawRiseHoldTime =  RR.lcabsu8YawRiseHoldTime = 0;
        FL.lcabss16YawDumpFactor2 =  FR.lcabss16YawDumpFactor2 =  RL.lcabss16YawDumpFactor2 =  RR.lcabss16YawDumpFactor2 = 0;
        FL.lcabsu8YawRiseHoldTime2 = FR.lcabsu8YawRiseHoldTime2 = RL.lcabsu8YawRiseHoldTime2 = RR.lcabsu8YawRiseHoldTime2 =0;
        lcabsu1OverInsideFrontCtrl = 0;
        lcabsu1OverInsideRearCtrl = 0;
        lcabsu1UnderOutsideFrontCtrl = 0;
        lcabss16YDFSplitRHcnt = lcabss16YDFSplitLHcnt = 0;
    }
    else if(lcabsu1ValidYawStrLatG==0)
    {
        GMA_STATE = 0;
        lcabsu8DelaYawExcessCnt = 0;
        lcabsu8DelaYawStableCnt = 0;
        FL.lcabss16YawDumpFactor =   FR.lcabss16YawDumpFactor =   RL.lcabss16YawDumpFactor =   RR.lcabss16YawDumpFactor =  0;
        FL.lcabsu8YawDumpHoldTime =  FR.lcabsu8YawDumpHoldTime =  RL.lcabsu8YawDumpHoldTime =  RR.lcabsu8YawDumpHoldTime = L_TIME_700MS;
        FL.lcabsu8YawRiseHoldTime =  FR.lcabsu8YawRiseHoldTime =  RL.lcabsu8YawRiseHoldTime =  RR.lcabsu8YawRiseHoldTime = 0;
        FL.lcabss16YawDumpFactor2 =  FR.lcabss16YawDumpFactor2 =  RL.lcabss16YawDumpFactor2 =  RR.lcabss16YawDumpFactor2 = 0;
        FL.lcabsu8YawRiseHoldTime2 = FR.lcabsu8YawRiseHoldTime2 = RL.lcabsu8YawRiseHoldTime2 = RR.lcabsu8YawRiseHoldTime2 =0;
        lcabsu1OverInsideFrontCtrl = 0;
        lcabsu1OverInsideRearCtrl = 0;
        lcabsu1UnderOutsideFrontCtrl = 0;
        lcabss16YDFSplitRHcnt = lcabss16YDFSplitLHcnt = 0;
    }
    else 
    {
/*      lcabss16TargetDeltaYaw = LCABS_s16Interpolation2P(vref, VREF_100_KPH, VREF_40_KPH, YAW_4DEG, YAW_8DEG); */
        
        if((LFC_Split_flag==1) || (LFC_Split_suspect_flag==1))
        {
            /*lcabss16TargetDeltaYaw = LCABS_s16Interpolation2P(vref, S16_SPLIT_dYAW_SET_SPEED_H, S16_SPLIT_dYAW_SET_SPEED_L, S16_SPLIT_TARGET_dYAW_SPEED_H, S16_SPLIT_TARGET_dYAW_SPEED_L); // (((VREF_100_KPH - vref)/VREF_2_KPH*YAW_4DEG)/30) + YAW_3DEG; */
                        
            lcabss16TargetDeltaYaw = LCESP_s16IInter3Point(vref, S16_GMA_L_SPEED, S16_SPLIT_TARGET_dYAW_SPEED_L,
            						                             S16_GMA_M_SPEED, S16_SPLIT_TARGET_dYAW_SPEED_M,
            						                             S16_GMA_H_SPEED, S16_SPLIT_TARGET_dYAW_SPEED_H);

			lcabss16TargetDeltaYaw2 = LCESP_s16IInter3Point(vref, S16_GMA_L_SPEED, S16_SPLIT_TARGET_dYAW2_SPEED_L,
            						                              S16_GMA_M_SPEED, S16_SPLIT_TARGET_dYAW2_SPEED_M,
            						                              S16_GMA_H_SPEED, S16_SPLIT_TARGET_dYAW2_SPEED_H);


            
        }
        else
        {
            if((AFZ_OK==1) && (afz>=-(int16_t)(U8_PULSE_LOW_MU)))
            {
                /*lcabss16TargetDeltaYaw = LCABS_s16Interpolation2P(vref, S16_LMu_dYAW_SET_SPEED_H, S16_LMu_dYAW_SET_SPEED_L, S16_LMu_TARGET_dYAW_SPEED_H, S16_LMu_TARGET_dYAW_SPEED_L);*/
                lcabss16TargetDeltaYaw = LCESP_s16IInter3Point(vref, S16_GMA_L_SPEED, S16_LMu_TARGET_dYAW_SPEED_L,
            						                             	 S16_GMA_M_SPEED, S16_LMu_TARGET_dYAW_SPEED_M,
            						                             	 S16_GMA_H_SPEED, S16_LMu_TARGET_dYAW_SPEED_H);

				lcabss16TargetDeltaYaw2 = LCESP_s16IInter3Point(vref, S16_GMA_L_SPEED, S16_LMu_TARGET_dYAW2_SPEED_L,
            						                              	  S16_GMA_M_SPEED, S16_LMu_TARGET_dYAW2_SPEED_M,
            						                              	  S16_GMA_H_SPEED, S16_LMu_TARGET_dYAW2_SPEED_H);

            }
            else
            {
                /*lcabss16TargetDeltaYaw = LCABS_s16Interpolation2P(vref, S16_DEFAULT_dYAW_SET_SPEED_H, S16_DEFAULT_dYAW_SET_SPEED_L, S16_DEFAULT_TARGET_dYAW_SPEED_H, S16_DEFAULT_TARGET_dYAW_SPEED_L);*/
                lcabss16TargetDeltaYaw = LCESP_s16IInter3Point(vref, S16_GMA_L_SPEED, S16_DEFAULT_TARGET_dYAW_SPEED_L,
            						                             	 S16_GMA_M_SPEED, S16_DEFAULT_TARGET_dYAW_SPEED_M,
            						                             	 S16_GMA_H_SPEED, S16_DEFAULT_TARGET_dYAW_SPEED_H);

				lcabss16TargetDeltaYaw2 = LCESP_s16IInter3Point(vref, S16_GMA_L_SPEED, S16_DEFAULT_TARGET_dYAW2_SPEED_L,
            						                              	  S16_GMA_M_SPEED, S16_DEFAULT_TARGET_dYAW2_SPEED_M,
            						                              	  S16_GMA_H_SPEED, S16_DEFAULT_TARGET_dYAW2_SPEED_H);

            }
        }
        if(lcabss16TargetDeltaYaw>YAW_15DEG) {lcabss16TargetDeltaYaw = YAW_15DEG;}
        else if(lcabss16TargetDeltaYaw<YAW_3DEG) {lcabss16TargetDeltaYaw = YAW_3DEG;}
        else { }
        
        if(lcabss16TargetDeltaYaw2>YAW_15DEG) {lcabss16TargetDeltaYaw2 = YAW_15DEG;}
        else if(lcabss16TargetDeltaYaw2<YAW_1DEG) {lcabss16TargetDeltaYaw2 = YAW_1DEG;}
        else { }

        if(LFC_Split_flag==0)
        {
            if(((GMA_SLIP_fl==1)&&(FL.gma_control_counter==L_1ST_SCAN)) || ((GMA_SLIP_fr==1)&&(FR.gma_control_counter==L_1ST_SCAN))) 
            {
                GMA_STATE = 1;
            }
            else
            {
              #if __NON_SPLIT_STABILITY_IMPROVE
                GMA_STATE = 100;
              #else
                GMA_STATE = 0;
              #endif
            }
            lcabsu8DelaYawExcessCnt = 0;
            lcabsu8DelaYawStableCnt = 0;
        }
        else if(GMA_PULSE==1)  /* GMA_PULSE = GMA_SLIP_fl | GMA_SLIP_fr; */
        {
            if(GMA_STATE==1) 
            {
                if(lcabss16TargetDeltaYaw > YAW_5DEG)
                {
                    if(delta_yaw >= YAW_5DEG)
                    {
                        lcabsu8DelaYawExcessCnt = lcabsu8DelaYawExcessCnt + L_1SCAN;
                        if(lcabsu8DelaYawExcessCnt>=L_TIME_20MS)
                        {
                            GMA_STATE = 2;
                            lcabsu8DelaYawExcessCnt = 0;
                        }
    
                        lcabsu8DelaYawStableCnt = 0;
                    }
                    else 
                    {
                        if(delta_yaw_dot <= 0)
                        {
                            lcabsu8DelaYawStableCnt = lcabsu8DelaYawStableCnt + L_1SCAN;
                            if(lcabsu8DelaYawStableCnt >= L_TIME_100MS)
                            {
                                GMA_STATE = 3;
                                lcabsu8DelaYawStableCnt = 0;
                            }
                        }
                        else if(lcabsu8DelaYawStableCnt >= L_1SCAN)
                        {
                            lcabsu8DelaYawStableCnt = lcabsu8DelaYawStableCnt - L_1SCAN;
                        }
                        else
                        {
                            ;
                        }
                        lcabsu8DelaYawExcessCnt = 0;
                    }
                }
                else
                {
                    if(delta_yaw >= lcabss16TargetDeltaYaw)
                    {
                        lcabsu8DelaYawExcessCnt = lcabsu8DelaYawExcessCnt + L_1SCAN;
                        if(lcabsu8DelaYawExcessCnt>=L_TIME_20MS)
                        {
                            GMA_STATE = 2;
                            lcabsu8DelaYawExcessCnt = 0;
                        }
    
                        lcabsu8DelaYawStableCnt = 0;
                    }
                    else 
                    {
                        if(delta_yaw_dot <= 0)
                        {
                            lcabsu8DelaYawStableCnt = lcabsu8DelaYawStableCnt + L_1SCAN;
                            if(lcabsu8DelaYawStableCnt >= L_TIME_100MS)
                            {
                                GMA_STATE = 3;
                                lcabsu8DelaYawStableCnt = 0;
                            }
                        }
                        else if(lcabsu8DelaYawStableCnt >= L_1SCAN)
                        {
                            lcabsu8DelaYawStableCnt = lcabsu8DelaYawStableCnt - L_1SCAN;
                        }
                        else
                        {
                            ;
                        }
                        lcabsu8DelaYawExcessCnt = 0;
                    }
                }
            }
            else if(GMA_STATE==2)
            {
                if(lcabss16TargetDeltaYaw > YAW_5DEG)
                {
                    if(delta_yaw < YAW_5DEG)
                    {
                        lcabsu8DelaYawStableCnt = lcabsu8DelaYawStableCnt + L_1SCAN;
                        if(lcabsu8DelaYawStableCnt >= L_TIME_100MS)
                        {
                            GMA_STATE = 3;
                            lcabsu8DelaYawStableCnt = 0;
                        }
                    }
                }
                else
                {
                    if(delta_yaw < lcabss16TargetDeltaYaw)
                    {
                        lcabsu8DelaYawStableCnt = lcabsu8DelaYawStableCnt + L_1SCAN;
                        if(lcabsu8DelaYawStableCnt >= L_TIME_100MS)
                        {
                            GMA_STATE = 3;
                            lcabsu8DelaYawStableCnt = 0;
                        }
                    }
                    
                }
            }
            else 
            {
                GMA_STATE = 3;
            }
        }
        else /* if(CERTAIN_SPLIT_flag==1) */
        {
            GMA_STATE = 4;
            lcabsu8DelaYawExcessCnt = 0;
            lcabsu8DelaYawStableCnt = 0;
        } /*
        else
        {
            GMA_STATE = 0;
            lcabsu8DelaYawExcessCnt = 0;
            lcabsu8DelaYawStableCnt = 0;
        } */
        
      #if __YAW_STABILITY_IMPROVE
        lcabsu1OverInsideFrontCtrl = 0;
        
        if(yaw_out>=0)
        {
            if(delta_yaw2>=0)
            {
                lcabsu1OverInsideRearCtrl = 1;
                lcabsu1UnderOutsideFrontCtrl = 0;
                if((AFZ_OK==1) && (afz<=-(int16_t)U8_HIGH_MU))
                {
                    if((RL.peak_slip<=-LAM_20P) && (RL.rel_lam<=-LAM_5P) && (RL.LFC_UNSTABIL_counter>=L_TIME_300MS))
                    {
                        lcabsu1OverInsideFrontCtrl = 1;
                    }
                }
            }
            else
            {
            	if(LFC_GMA_Split_flag==1)/*(CERTAIN_SPLIT_flag==1)*/
            	{
            		lcabsu1OverInsideRearCtrl = 1;	
            	}
                else if((ABS_fl==1)&&(ABS_fr==1)&&(det_alatm<LAT_0G5G))
                {	
                    lcabsu1UnderOutsideFrontCtrl = 1;
                   	lcabsu1OverInsideRearCtrl = 0; 
                }
                else
                {
                	lcabsu1OverInsideRearCtrl = 0;                
            	}
        	}
        }
        else
        {
            if(delta_yaw2>=0)
            {
                if(LFC_GMA_Split_flag==1)/*(CERTAIN_SPLIT_flag==1)*/
            	{
            		lcabsu1OverInsideRearCtrl = 1;	
            	}
                else if((ABS_fl==1)&&(ABS_fr==1)&&(det_alatm<LAT_0G5G))
                {	
                    lcabsu1UnderOutsideFrontCtrl = 1;
                   	lcabsu1OverInsideRearCtrl = 0; 
                }
                else
                {
                	lcabsu1OverInsideRearCtrl = 0;
            	}
            }
            else
            {
                lcabsu1OverInsideRearCtrl = 1;
                lcabsu1UnderOutsideFrontCtrl = 0;
                if((AFZ_OK==1) && (afz<=-(int16_t)U8_HIGH_MU))
                {
                    if((RR.peak_slip<=-LAM_20P) && (RR.rel_lam<=-LAM_5P) && (RR.LFC_UNSTABIL_counter>=L_TIME_300MS))
                    {
                        lcabsu1OverInsideFrontCtrl = 1;
                    }
                }
            }
        }
        
        if((lcabsu1OverInsideRearCtrl==1) && (lcabsu1OverInsideFrontCtrl==0))
        {
            if((LFC_Split_flag==1) || (LFC_Split_suspect_flag==1))
            {
                lcabsu1OverInsideFrontCtrl = 1;
                lcabsu1OverInsideRearCtrl = 0;
            }
            else if((FL.LFC_zyklus_z<=2) && (FR.LFC_zyklus_z<=2)) /* Split Braking tendency !!! Need to improve */
            {
                lcabsu1OverInsideFrontCtrl = 1;
                if((ABS_rl==1) && (ABS_rr==1))
                {
/*                    lcabsu1OverInsideRearCtrl = 0; */
                }
            }
            else
            {
              /* #if __PRESS_EST   
                if((AFZ_OK==1) && (afz>=-(int16_t)U8_HIGH_MU))
                {
                    if(
                        ((FL.ABSCyle_Estimated_Press_Avg>=(FR.ABSCyle_Estimated_Press_Avg+MPRESS_15BAR)) && (FR.ABSCyle_Estimated_Press_Avg<=MPRESS_20BAR))
                     || ((FR.ABSCyle_Estimated_Press_Avg>=(FL.ABSCyle_Estimated_Press_Avg+MPRESS_15BAR)) && (FL.ABSCyle_Estimated_Press_Avg<=MPRESS_20BAR))
                    )
                    {
                        lcabsu1OverInsideFrontCtrl = 1;
                        lcabsu1OverInsideRearCtrl = 0;
                    }
                }
              #endif    'ABSCyle_Estimated_Press_Avg' is not defined*/
            }
        }
      #endif        

        if(GMA_STATE>=3)
        {
        	if(LFC_Split_flag==0)
        	{
	            Yaw_Gain = (int16_t)S8_HOMO_dYAW_ABS_GAIN;
	            Yaw_Dot_Gain = (int16_t)S8_HOMO_dYAW_DOT_ABS_GAIN;
	        }
	        else
	        {
	            Yaw_Gain = (int16_t)S8_SPLIT_dYAW_ABS_GAIN_2;
	            Yaw_Dot_Gain = (int16_t)S8_SPLIT_dYAW_DOT_ABS_GAIN_2;
	        }        	
            
            LCABS_vCalcDeltaYawFactorWL(&FL,GMA_STATE);
            LCABS_vCalcDeltaYawFactorWL(&FR,GMA_STATE);         
            LCABS_vCalcDeltaYawFactorWL(&RL,GMA_STATE);
            LCABS_vCalcDeltaYawFactorWL(&RR,GMA_STATE);         
            
            lcabss16YDFSplitLHcnt = LCABS_vCheckSplitTendency(FL.lcabss16YawDumpFactor, FR.lcabss16YawDumpFactor, (uint8_t)MSL_BASE_rr, lcabss16YDFSplitLHcnt);
            lcabss16YDFSplitRHcnt = LCABS_vCheckSplitTendency(FR.lcabss16YawDumpFactor, FL.lcabss16YawDumpFactor, (uint8_t)MSL_BASE_rl, lcabss16YDFSplitRHcnt);
        }
        else if(GMA_STATE>=1)
        {
            Yaw_Gain = (int16_t)S8_SPLIT_dYAW_ABS_GAIN_1;
            Yaw_Dot_Gain = (int16_t)S8_SPLIT_dYAW_DOT_ABS_GAIN_1;

            LCABS_vCalcDeltaYawFactorWL(&FL,GMA_STATE);
            LCABS_vCalcDeltaYawFactorWL(&FR,GMA_STATE);         
            LCABS_vCalcDeltaYawFactorWL(&RL,GMA_STATE);
            LCABS_vCalcDeltaYawFactorWL(&RR,GMA_STATE);         

            lcabss16YDFSplitLHcnt = LCABS_vCheckSplitTendency(FL.lcabss16YawDumpFactor, FR.lcabss16YawDumpFactor, (uint8_t)MSL_BASE_rr, lcabss16YDFSplitLHcnt);
            lcabss16YDFSplitRHcnt = LCABS_vCheckSplitTendency(FR.lcabss16YawDumpFactor, FL.lcabss16YawDumpFactor, (uint8_t)MSL_BASE_rl, lcabss16YDFSplitRHcnt);
        }
        else
        {
            Yaw_Gain = 0;
            Yaw_Dot_Gain = 0;

            FL.lcabss16YawDumpFactor =   FR.lcabss16YawDumpFactor =   RL.lcabss16YawDumpFactor =   RR.lcabss16YawDumpFactor =  0;
            FL.lcabsu8YawDumpHoldTime =  FR.lcabsu8YawDumpHoldTime =  RL.lcabsu8YawDumpHoldTime =  RR.lcabsu8YawDumpHoldTime = L_TIME_700MS;
            FL.lcabsu8YawRiseHoldTime =  FR.lcabsu8YawRiseHoldTime =  RL.lcabsu8YawRiseHoldTime =  RR.lcabsu8YawRiseHoldTime = 0;
            FL.lcabss16YawDumpFactor2 =  FR.lcabss16YawDumpFactor2 =  RL.lcabss16YawDumpFactor2 =  RR.lcabss16YawDumpFactor2 = 0;
            FL.lcabsu8YawRiseHoldTime2 = FR.lcabsu8YawRiseHoldTime2 = RL.lcabsu8YawRiseHoldTime2 = RR.lcabsu8YawRiseHoldTime2 =0;
            
            lcabss16YDFSplitRHcnt = lcabss16YDFSplitLHcnt = 0;
        }
        
    }
}

void LCABS_vCalcDeltaYawFactorWL(struct W_STRUCT *OVER_INSIDE_WL, uint8_t YMR_CONTROL_STATE)
{
    if(OVER_INSIDE_WL->LEFT_WHEEL == 1)
    {
        OVER_INSIDE_WL->lcabss16YawDumpFactor = (int16_t)(((int32_t)Yaw_Gain*(delta_yaw2-lcabss16TargetDeltaYaw))/10) + (int16_t)((int32_t)Yaw_Dot_Gain*(delta_yaw2_dot-50));
        /*OVER_INSIDE_WL->lcabss16YawDumpFactor2 = (int16_t)(((int32_t)Yaw_Gain*(delta_yaw2-YAW_1DEG))/10) + (int16_t)((int32_t)Yaw_Dot_Gain*(delta_yaw2_dot-50));*/
        OVER_INSIDE_WL->lcabss16YawDumpFactor2 = (int16_t)(((int32_t)Yaw_Gain*(delta_yaw2-lcabss16TargetDeltaYaw2))/10) + (int16_t)((int32_t)Yaw_Dot_Gain*(delta_yaw2_dot-50));
    }
    else
    {
        OVER_INSIDE_WL->lcabss16YawDumpFactor = -(int16_t)(((int32_t)Yaw_Gain*(delta_yaw2+lcabss16TargetDeltaYaw))/10) - (int16_t)((int32_t)Yaw_Dot_Gain*(delta_yaw2_dot+50));
        /*OVER_INSIDE_WL->lcabss16YawDumpFactor2 = -(int16_t)(((int32_t)Yaw_Gain*(delta_yaw2+YAW_1DEG))/10) - (int16_t)((int32_t)Yaw_Dot_Gain*(delta_yaw2_dot+50));*/
        OVER_INSIDE_WL->lcabss16YawDumpFactor2 = -(int16_t)(((int32_t)Yaw_Gain*(delta_yaw2+lcabss16TargetDeltaYaw2))/10) - (int16_t)((int32_t)Yaw_Dot_Gain*(delta_yaw2_dot+50));
    }
    
    if(OVER_INSIDE_WL->lcabss16YawDumpFactor <= 0)
    {
        OVER_INSIDE_WL->lcabsu8YawDumpHoldTime = U8_YDF_DUMPHOLD_T_MAX;
    }
    else if(OVER_INSIDE_WL->lcabss16YawDumpFactor <= S16_NEED_DEC_PRESS_YDF_L2)
    {
        OVER_INSIDE_WL->lcabsu8YawDumpHoldTime = (uint8_t)LCABS_s16Interpolation2P(OVER_INSIDE_WL->lcabss16YawDumpFactor, S16_NEED_DEC_PRESS_YDF_L1, S16_NEED_DEC_PRESS_YDF_L2, (int16_t)U8_YDF_DUMPHOLD_T_AT_DEC_L1, (int16_t)U8_YDF_DUMPHOLD_T_AT_DEC_L2);        
    }
    else
    {
        OVER_INSIDE_WL->lcabsu8YawDumpHoldTime = (uint8_t)LCABS_s16Interpolation2P(OVER_INSIDE_WL->lcabss16YawDumpFactor, S16_NEED_DEC_PRESS_YDF_L2, S16_NEED_DEC_PRESS_YDF_L3, (int16_t)U8_YDF_DUMPHOLD_T_AT_DEC_L2, (int16_t)U8_YDF_DUMPHOLD_T_AT_DEC_L3);
        
        lcabss16temp3 = LCABS_s16Interpolation2P((int16_t)OVER_INSIDE_WL->LFC_dump_counter, 3, 10, L_TIME_20MS, L_TIME_50MS);
        
        if(OVER_INSIDE_WL->lcabsu8YawDumpHoldTime < (uint8_t)lcabss16temp3)
        {
        	OVER_INSIDE_WL->lcabsu8YawDumpHoldTime = (uint8_t)lcabss16temp3;
        }
    }
    
    if(OVER_INSIDE_WL->lcabss16YawDumpFactor <= S16_NEED_INC_PRESS_YDF_L1)
    {
        OVER_INSIDE_WL->lcabsu8YawRiseHoldTime = (uint8_t)LCABS_s16Interpolation2P(OVER_INSIDE_WL->lcabss16YawDumpFactor, S16_NEED_INC_PRESS_YDF_L2, S16_NEED_INC_PRESS_YDF_L1, (int16_t)U8_YDF_RISEHOLD_T_AT_INC_L2, (int16_t)U8_YDF_RISEHOLD_T_AT_INC_L1);
    }
    else if(OVER_INSIDE_WL->lcabss16YawDumpFactor <= 0)
    {
        OVER_INSIDE_WL->lcabsu8YawRiseHoldTime = (uint8_t)LCABS_s16Interpolation2P(OVER_INSIDE_WL->lcabss16YawDumpFactor, S16_NEED_INC_PRESS_YDF_L1, 0, (int16_t)U8_YDF_RISEHOLD_T_AT_INC_L1, (int16_t)U8_YDF_RISEHOLD_T_AT_TARGETDYAW); /*((OVER_INSIDE_WL->lcabss16YawDumpFactor+400)/10); */
    }
    else
    {
        OVER_INSIDE_WL->lcabsu8YawRiseHoldTime = (uint8_t)LCABS_s16Interpolation2P(OVER_INSIDE_WL->lcabss16YawDumpFactor, 0, S16_YDF_RISEHOLD_T_MAX_YDF, (int16_t)U8_YDF_RISEHOLD_T_AT_TARGETDYAW, (int16_t)U8_YDF_RISEHOLD_T_MAX); /*((OVER_INSIDE_WL->lcabss16YawDumpFactor+400)/10); */
    }
    
    if(YMR_CONTROL_STATE>=3)
    {
        if(OVER_INSIDE_WL->lcabss16YawDumpFactor2 >= 0)
        {
	      #if __VDC && __HDC && (__HDC_ABS_IMPROVE==ENABLE)
            if((lcu1HdcActiveFlg==1) && (ACTIVE_BRAKE_FLG==0) && (vref>=(int16_t)VREF_5_KPH))
            {
	            if((lcabsu1OverInsideRearCtrl==1) && (lcabsu1OverInsideFrontCtrl==0) && (OVER_INSIDE_WL->REAR_WHEEL==0))
	            {	/* step #2. need to consider vref, Mu */
	                OVER_INSIDE_WL->lcabsu8YawRiseHoldTime2 = (uint8_t)LCABS_s16Interpolation2P(OVER_INSIDE_WL->lcabss16YawDumpFactor2, 0, YAW_10DEG, L_TIME_10MS, L_TIME_150MS);/*2, 20);*/
	            }
	            else
	            {    
	                OVER_INSIDE_WL->lcabsu8YawRiseHoldTime2 = (uint8_t)LCABS_s16Interpolation2P(OVER_INSIDE_WL->lcabss16YawDumpFactor2, 0, YAW_10DEG, L_TIME_10MS, L_TIME_300MS);/*2,40*/
	            }
            }
            else
	      #endif /*__VDC && __HDC && (__HDC_ABS_IMPROVE==ENABLE)*/
            if((lcabsu1OverInsideRearCtrl==1) && (lcabsu1OverInsideFrontCtrl==0) && (OVER_INSIDE_WL->REAR_WHEEL==0))
            {	/* step #2. need to consider vref, Mu */
                OVER_INSIDE_WL->lcabsu8YawRiseHoldTime2 = (uint8_t)LCABS_s16Interpolation2P(OVER_INSIDE_WL->lcabss16YawDumpFactor2, 0, YAW_10DEG, L_TIME_10MS, S16_YDF_STB_HLD_TIME_MAX_F_OINR);/*2, 10);*/
            }
            else
            {    
                OVER_INSIDE_WL->lcabsu8YawRiseHoldTime2 = (uint8_t)LCABS_s16Interpolation2P(OVER_INSIDE_WL->lcabss16YawDumpFactor2, 0, YAW_10DEG, L_TIME_10MS, S16_YDF_STB_HLD_TIME_MAX);/*2, 20);*/
            }
        }
        else
        {
            OVER_INSIDE_WL->lcabsu8YawRiseHoldTime2 = 0;
        }
    }
    else
    {
        OVER_INSIDE_WL->lcabsu8YawRiseHoldTime2 = 0;
    }               
}

int16_t LCABS_vCheckSplitTendency(int16_t SP_HIGH_FRONT_YDF, int16_t SP_LOW_FRONT_YDF,uint8_t BASE_WHEEL, int16_t SPLIT_CNT)
{
    lcabss16temp4 = SPLIT_CNT;
    
    if(BASE_WHEEL==1)
    {
        if(SP_LOW_FRONT_YDF < SP_HIGH_FRONT_YDF)
        {
            if((SP_HIGH_FRONT_YDF-SP_LOW_FRONT_YDF) >= 2000)
            {
                lcabss16temp4 = lcabss16temp4 + L_5SCAN;
            }
            else if((SP_HIGH_FRONT_YDF-SP_LOW_FRONT_YDF) >= 1000)
            {
                lcabss16temp4 = lcabss16temp4 + L_3SCAN;
            }
            else if((SP_HIGH_FRONT_YDF-SP_LOW_FRONT_YDF) >= 800)
            {
                lcabss16temp4 = lcabss16temp4 + L_1SCAN;
            }
            else if((SP_HIGH_FRONT_YDF-SP_LOW_FRONT_YDF) < 500)
            {
                lcabss16temp4 = lcabss16temp4 - L_1SCAN;
            }
            else
            {
                ;
            }
            
            if(lcabss16temp4 > 32000) {lcabss16temp4 = 32000;}
        }
        else
        {
            if((SP_HIGH_FRONT_YDF-SP_LOW_FRONT_YDF) >= - 200)
            {
                lcabss16temp4 = lcabss16temp4 - L_3SCAN;
            }
            else if((SP_HIGH_FRONT_YDF-SP_LOW_FRONT_YDF) >= - 500)
            {
                lcabss16temp4 = lcabss16temp4 - L_5SCAN;
            }
            else if((SP_HIGH_FRONT_YDF-SP_LOW_FRONT_YDF) >= -1000)
            {
                lcabss16temp4 = lcabss16temp4 - L_10SCAN;
            }
            else 
            {
                lcabss16temp4 = 0;
            }
            
            if(lcabss16temp4 < 0) {lcabss16temp4 = 0;}                  
        }               
    }
    else 
    {
        lcabss16temp4 = 0;  
    }
    
    return lcabss16temp4;
}
#endif

/* ====================================================================== */

#if __OUT_ABS_P_RATE_COMPENSATION
static void LCABS_vDecideOutABSCtrlForYMR(void)
{
    if(ABS_fz==0)
    {
        FL.OutABSYMRCtrlMode=0;
        FR.OutABSYMRCtrlMode=0;
    }
    else
    {
        if((ABS_fl==1) && (ABS_fr==0))
        {
            LCABS_vDecideOutABSYMRCtrlModeF(&FR,&FL);
        }
        else if((ABS_fl==0) && (ABS_fr==1))
        {
            LCABS_vDecideOutABSYMRCtrlModeF(&FL,&FR);
        }
        else
        {
            FL.OutABSYMRCtrlMode=0;
            FR.OutABSYMRCtrlMode=0;
        }
   }
}

static void LCABS_vDecideOutABSYMRCtrlModeF(struct W_STRUCT *pFH, struct W_STRUCT *pFL)
     /* Output : OutABSYMRCtrlMode, lcabsu8AdaptHoldPerSlipDiff */
{
    int8_t L1_SLIP_REF;
    int8_t L2_SLIP_REF;
    int8_t L3_SLIP_MAX;
    
    pFL->OutABSYMRCtrlMode=0;
    if((pFH->GMA_SLIP==0) && (pFL->STABIL==0) && (Rough_road_detect_vehicle==0) && (Rough_road_suspect_vehicle==0) && (vref > S16_OUTABS_YMR_ALLOW_SPEED) && (BEND_DETECT2==0))
    {
        lcabss8temp0 = pFL->peak_slip - pFH->peak_slip;
    
        L1_SLIP_REF = (int8_t)LCABS_s16Interpolation2P(vref,VREF_50_KPH,VREF_80_KPH,S8_L1_SLIP_DIFF_50_80,S8_L1_SLIP_DIFF_80_M);
        L2_SLIP_REF = (int8_t)LCABS_s16Interpolation2P(vref,VREF_50_KPH,VREF_80_KPH,S8_L2_SLIP_DIFF_50_80,S8_L2_SLIP_DIFF_80_M);
        L3_SLIP_MAX = (int8_t)LCABS_s16Interpolation2P(vref,VREF_50_KPH,VREF_80_KPH,S8_GMA_DCT_SLIP_DIFF_50_80_BABS,S8_GMA_DCT_SLIP_DIFF_80_M_BABS);
          
            
        if((vref > S16_OUTABS_YMR_DUMP_ALLOW_SPEED) && (lcabss8temp0 < (-L2_SLIP_REF)) && (pFL->peak_dec_alt<-S8_L2_LOWSIDE_PEAK_DEC))
        {
            if(pFH->OutABSYMRCtrlMode<LEVEL2)
            {
                pFH->OutABSYMRCtrlMode=LEVEL2;
            }
        }
        else if((lcabss8temp0 < (-L1_SLIP_REF)) && (pFL->peak_dec_alt<-S8_L1_LOWSIDE_PEAK_DEC))
        {
            if(pFH->OutABSYMRCtrlMode<LEVEL1)
            {
                pFH->OutABSYMRCtrlMode=LEVEL1;
            }
        }
        else { }

        if(pFH->OutABSYMRCtrlMode>=LEVEL2)
        {
            lcabsu8AdaptHoldPerSlipDiff=(uint8_t)LCABS_s16Interpolation2P((int16_t)lcabss8temp0,-(int16_t)L2_SLIP_REF,-(int16_t)L3_SLIP_MAX,L_TIME_80MS,L_TIME_120MS);
        }
        else if(pFH->OutABSYMRCtrlMode==LEVEL1)
        {
            lcabsu8AdaptHoldPerSlipDiff=(uint8_t)LCABS_s16Interpolation2P((int16_t)lcabss8temp0,-(int16_t)L1_SLIP_REF,-(int16_t)L2_SLIP_REF,L_TIME_30MS,L_TIME_60MS);
        }
        else
        {
            lcabsu8AdaptHoldPerSlipDiff=0;
        }
    }
    else
    {
        pFH->OutABSYMRCtrlMode=0;
    }
}
#endif /* __OUT_ABS_P_RATE_COMPENSATION*/

void LCABS_vResetABS(void)
{
    int16_t ref_vref, ref_vrad, REF_DV;

    RE_ABS_flag = 0;

    if(((lcabsu1BrakeByBLS==0)&&(BLS_EEPROM==0))  
        /*if((BLS==0)*/
          #if __VDC
        && (BRAKE_BY_MPRESS_FLG==0)             /* MPRESS_BRAKE_ON */
          #endif
        )
    {

            if(ABS_fz == 1) {
              #if   __REAR_D
                if(vrad_fl > vrad_fr) {tempW0 = vrad_fl; tempW1 = vrad_fr;}
                else {tempW0 = vrad_fr; tempW1 = vrad_fl;}
                if(vrad_rl > vrad_rr) {tempW2 = vrad_rl; tempW3 = vrad_rr;}
                else {tempW2 = vrad_rr; tempW3 = vrad_rl;}
              #else
                if(vrad_rl > vrad_rr) {tempW0 = vrad_rl; tempW1 = vrad_rr;}
                else {tempW0 = vrad_rr; tempW1 = vrad_rl;}
                if(vrad_fl > vrad_fr) {tempW2 = vrad_fl; tempW3 = vrad_fr;}
                else {tempW2 = vrad_fr; tempW3 = vrad_fl;}
              #endif

             #if (__4WD_VARIANT_CODE==ENABLE)
                if((lsu8DrvMode==DM_AUTO) || (lsu8DrvMode==DM_4H) || (lsu8DrvMode==DM_2H))
                {
                    ref_vref = vref2; ref_vrad = tempW1; REF_DV = VREF_2_5_KPH; /* Non driven, Slow Wheel */
                }
                else
                {
                    ref_vref = vref; ref_vrad = tempW0; REF_DV = VREF_5_KPH;    /* Non driven, Fast Wheel */
                }
             #else
              #if __4WD
                ref_vref = vref2; ref_vrad = tempW1; REF_DV = VREF_2_5_KPH;
              #else
                ref_vref = vref; ref_vrad = tempW0; REF_DV = VREF_5_KPH;
              #endif
             #endif

                if(ref_vref > tempW0) {
                    if(ref_vref > vref_alt) {
                        if(abs_off_timer<(U8_TYPE_MAXNUM-L_2SCAN)) {abs_off_timer+=L_2SCAN;}
                    }
                    else if(ref_vref == vref_alt) {
                        if(abs_off_timer<U8_TYPE_MAXNUM) {abs_off_timer+=L_1SCAN;}
                    }
                    else {abs_off_timer = 0;}
                }
                else {abs_off_timer = 0;}

                if(abs_off_timer > L_TIME_490MS) {
                    if((ref_vref-ref_vrad)>REF_DV) {
                        abs_off_timer = L_TIME_560MS;
                        RE_ABS_flag = 1;
                    }
                }

              #if (__4WD_VARIANT_CODE==ENABLE)
                if((lsu8DrvMode==DM_AUTO) || (lsu8DrvMode==DM_4H) || (lsu8DrvMode==DM_2H))
                {
                    abs_off_timer2 = 0;
                }
                else
                {
                    if(tempW3 > ref_vref) {
                        if(abs_off_timer2<U8_TYPE_MAXNUM) {abs_off_timer2 = abs_off_timer2 + L_1SCAN;}  /* Driven, Slow Wheel */
                    }
                    else {abs_off_timer2 = 0;}

                    if(abs_off_timer2 > L_TIME_250MS) {
                        if((ref_vref-ref_vrad)>REF_DV) {        /* Non driven, Fast Wheel */
                            abs_off_timer2 = L_TIME_280MS;
                            RE_ABS_flag = 1;
                        }
                    }
                }
              #else
                #if (!__4WD)
                {
                    if(tempW3 > ref_vref) {
                        if(abs_off_timer2<U8_TYPE_MAXNUM) {abs_off_timer2 = abs_off_timer2 + L_1SCAN;}  /* Driven, Slow Wheel */
                    }
                    else {abs_off_timer2 = 0;}

                    if(abs_off_timer2 > L_TIME_250MS) {
                        if((ref_vref-ref_vrad)>REF_DV) {        /* Non driven, Fast Wheel */
                            abs_off_timer2 = L_TIME_280MS;
                            RE_ABS_flag = 1;
                        }
                    }
                }
                #endif
              #endif /* (__4WD_VARIANT_CODE==ENABLE) */

              #if __TCS || __ETC        /* BY HJH */
                if(mtp > 10) {
                    if(abs_off_timer > L_TIME_40MS) {
                        RE_ABS_flag = 1;
                    }
                }
              #endif

            }
            else {
                abs_off_timer = 0;
              #if (__4WD_VARIANT_CODE==ENABLE)
                abs_off_timer2 = 0;
              #else
                #if !__4WD
                abs_off_timer2 = 0;
                #endif
              #endif
            }
    }

/*
    if(RE_ABS_flag==1) {    // Moved to LDABS_vCallVrefEst()
        voptfz1=voptfz_mittel1=vref=tempW0;
        voptfz2=voptfz_mittel2=vref2=tempW1;
        filter_out1=filter_out2=voptfz_rest1=voptfz_rest2=0;
            #if __WL_SPEED_RESOL_CHANGE
        FZ1.voptfz_resol_change=(tempW0*8);
        FZ1.voptfz_mittel_resol_change=(tempW0*8);
        vref_resol_change=(tempW0*8);
        FZ2.voptfz_resol_change=(tempW1*8);
        FZ2.voptfz_mittel_resol_change=(tempW1*8);
        FZ1.filter_out_resol_change=0;
        FZ2.filter_out_resol_change=0;
            #if __4WD &&__MGH_40_VREF
        FZ_for_4WD.voptfz_resol_change=FZ1.voptfz_resol_change;
        FZ_for_4WD.voptfz_mittel_resol_change=FZ1.voptfz_mittel_resol_change;
        FZ_for_4WD.filter_out_resol_change=0;
            #endif
        #endif

        #if __4WD &&__MGH_40_VREF
        FZ_for_4WD.voptfz=FZ1.voptfz;
        FZ_for_4WD.voptfz_mittel=FZ1.voptfz_mittel;
        FZ_for_4WD.filter_out=0;
        #endif
    }
*/
}

void LCABS_vSetLFCHoldTimer(void)
{
    LCABS_vSetLFCHoldTimerWL(&FL);
    LCABS_vSetLFCHoldTimerWL(&FR);
    LCABS_vSetLFCHoldTimerWL(&RL);
    LCABS_vSetLFCHoldTimerWL(&RR);
}

void LCABS_vSetLFCHoldTimerWL(struct W_STRUCT *WL)  /* Change Hold Timer due to Road State */
{
	uint8_t Hold_timer_new_ref2_Old = 0;

	Hold_timer_new_ref2_Old = WL->hold_timer_new_ref2;

  #if (__MGH60_CYCLE_TIME_OPT_ABS==ENABLE)
	if(ABS_fz==0)                   
  	{                               
  		WL->hold_timer_new_ref2 = U8_Hold_Timer_New_Ref_Default;
  		WL->hold_timer_new_ref = U8_Hold_Timer_New_Ref_Default; 
  	}                              
  	else 
  #endif
  #if __INGEAR_VIB
    if((WL->LFC_zyklus_z==1)||((WL->LFC_fictitious_cycle_flag==1)&&(WL->In_Gear_flag==0)))
  #else
    if((WL->LFC_zyklus_z==1)||(WL->LFC_fictitious_cycle_flag==1))
  #endif
    {
        if(REAR_WHEEL_wl==0) 
        {
            WL->hold_timer_new_ref2 = (uint8_t)U8_Hold_Timer_New_Ref_Initial;
            
            if((LFC_Split_flag==1) && (CERTAIN_SPLIT_flag==1)) 
            {
                if(((LEFT_WHEEL_wl==1)&&(MSL_BASE_rr==1)) || ((LEFT_WHEEL_wl==0)&&(MSL_BASE_rl==1)))
                {
                    WL->hold_timer_new_ref2 = (uint8_t)U8_Hold_Timer_New_Ref_SpHigh_F;
                }
                else
                {
                    WL->hold_timer_new_ref2 = (uint8_t)U8_Hold_Timer_New_Ref_Med;
                }
            }
        }
        else 
        {
            WL->hold_timer_new_ref2 = (uint8_t)U8_Hold_Timer_New_Ref_Initial;
        }

      #if (__SLIGHT_BRAKING_COMPENSATION)
        if((lcabsu1SlightBraking==1) && (vref < VREF_50_KPH))
        {
            if(REAR_WHEEL_wl==0)
            {
                if(WL->hold_timer_new_ref2 > L_TIME_10MS)
                {
                    WL->hold_timer_new_ref2 = L_TIME_10MS;
                }
            }
            else
            {
                if(WL->hold_timer_new_ref2 > L_TIME_20MS)
                {
                    WL->hold_timer_new_ref2 = L_TIME_20MS;
                }
            }
        }
      #endif  /* (__SLIGHT_BRAKING_COMPENSATION) */    

        if((WL->s0_reapply_counter >=4) && (WL->hold_timer_new_ref2 > Hold_timer_new_ref2_Old))
        {
        	WL->hold_timer_new_ref=Hold_timer_new_ref2_Old; /* if there is enough pulse-up, don't prolongue ref. hold time */
        }
        else
        {
        	WL->hold_timer_new_ref=WL->hold_timer_new_ref2;
        }
    }
    else{
        if(REAR_WHEEL_wl == 0) {            /* Front Wheel */
            if((AFZ_OK==1)&&(afz<=-(int16_t)U8_LOW_MU)) {
                if(afz<=-(int16_t)U8_HIGH_MU) {              /* High mu, afz < -0.55g */
                    WL->hold_timer_new_ref2 = (uint8_t)U8_Hold_Timer_New_Ref_High;
                }
                else {                              /* Medium mu, -0.55g<afz<-0.25g */
                    WL->hold_timer_new_ref2 = (uint8_t)U8_Hold_Timer_New_Ref_Med;

                    if((LFC_Split_flag==1) && (CERTAIN_SPLIT_flag==1) && (UN_GMA_wl==0)&&(UN_GMA_rl==1)&&(UN_GMA_rr==1))
                    {
                        if(((LEFT_WHEEL_wl==1)&&(MSL_BASE_rr==1)) || ((LEFT_WHEEL_wl==0)&&(MSL_BASE_rl==1)))
                        {
                            WL->hold_timer_new_ref2 = (uint8_t)U8_Hold_Timer_New_Ref_SpHigh_F;
                        }
                    }
                }
            }
            else {                                  /* Low mu, -0.25g<afz */
                if((AFZ_OK==1) || (WL->LFC_initial_deep_slip_comp_flag==1)) {
                    WL->hold_timer_new_ref2 = (uint8_t)U8_Hold_Timer_New_Ref_Low;
                }/*
                else if(WL->LFC_initial_deep_slip_comp_flag==1) {
                    WL->hold_timer_new_ref2 = (uint8_t)U8_Hold_Timer_New_Ref_Low;
                }*/
                else{
                    WL->hold_timer_new_ref2 = (uint8_t)U8_Hold_Timer_New_Ref_High;
                }
            }
        }
        else {                              /* Rear Wheel */
            if((AFZ_OK==1)&&(afz<=-(int16_t)U8_LOW_MU)) {
                if(afz<=-(int16_t)U8_HIGH_MU) {              /* High mu, afz < -0.55g */
                    WL->hold_timer_new_ref2 = (uint8_t)U8_Hold_Timer_New_Ref_High_R;
                }
                else {                              /* Medium mu, -0.55g<afz<-0.25g */
                    WL->hold_timer_new_ref2 = (uint8_t)U8_Hold_Timer_New_Ref_Med_R;
                }
            }
            else {                                  /* Low mu, -0.25g<afz */
                if((AFZ_OK==1) || (WL->LFC_initial_deep_slip_comp_flag==1)) {
                    WL->hold_timer_new_ref2 = (uint8_t)U8_Hold_Timer_New_Ref_Low_R;
                }/*
                else if(WL->LFC_initial_deep_slip_comp_flag==1) {
                    WL->hold_timer_new_ref2 = (uint8_t)U8_Hold_Timer_New_Ref_Low_R;
                }*/
                else{
                    WL->hold_timer_new_ref2 = (uint8_t)U8_Hold_Timer_New_Ref_High_R;
                }
            }
        }

        if((WL->s0_reapply_counter >=4) && (WL->hold_timer_new_ref2 > Hold_timer_new_ref2_Old))
        {
        	WL->hold_timer_new_ref=Hold_timer_new_ref2_Old; /* if there is enough pulse-up, don't prolongue ref. hold time */
        }
        else
        {
        	WL->hold_timer_new_ref=WL->hold_timer_new_ref2;
        }

        if((LOW_to_HIGH_suspect==1) && (ABS_LIMIT_SPEED_wl==0))
        {
        	if((WL->s16_Estimated_Active_Press < MPRESS_60BAR) || (WL->peak_slip > -LAM_10P))//&&(REAR_WHEEL==0))
        	{
        		WL->hold_timer_new_ref = 0;
        	}
        	else
        	{
        		WL->hold_timer_new_ref = U8_Hold_Timer_New_Ref_Initial;
        	}
        }
        else if(WL->LOW_to_HIGH_suspect2==1)
        {
		  #if(__L2H_IMPROVE_BY_WHEEL_PRESS==ENABLE)	
			if(lcabsu1L2hSuspectByWhPres==0)
		  #endif
			{
	        	if((WL->s16_Estimated_Active_Press < MPRESS_50BAR) || (WL->peak_slip > -LAM_7P))//&&(REAR_WHEEL==0))
	        	{
	        		WL->hold_timer_new_ref = 0;
	        	}
	        	else
	        	{
	        		WL->hold_timer_new_ref = U8_Hold_Timer_New_Ref_High;
	        	}
			}
        }
        else if(lcabsu1DriverPedalReapply==1)
        {
        	WL->hold_timer_new_ref = U8_Hold_Timer_New_Ref_Initial;
        }
        else{}
    }
    
  #if __REDUCE_ABS_TIME_AT_BUMP
    if(PossibleABSForBumpVehicle==1)
    {
        if(WL->hold_timer_new_ref > L_TIME_20MS)
        {
            WL->hold_timer_new_ref=L_TIME_20MS;
        }
    }
  #endif
  #if(__ROUGH_COMP_IMPROVE==ENABLE)
    if((Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1))
    {
    	if(WL->hold_timer_new_ref > L_TIME_20MS)
        {
            WL->hold_timer_new_ref=L_TIME_20MS;
        }
    }    
  #endif
    

  #if __UCC_COOPERATION_CONTROL
    if((REAR_WHEEL_wl==0) && (WL->ABS_PotHole_Stable_flg1==1)) {WL->hold_timer_new_ref = L_TIME_20MS;}
  #endif

}


/******************************************************************************
* FUNCTION NAME:      LCABS_vDecideWheelCtrlState
* CALLED BY:          LCABS_vCallControl()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Decide Dump threshold for ABS control
******************************************************************************/
void LCABS_vDecideWheelCtrlState(void)
{
  #if   __BTC
    if(BTC_fz==0)
  #endif
    {
        LCABS_vDecideWheelCtrlStateWL(&FL);                           /* determine state change */
        LCABS_vDecideWheelCtrlStateWL(&FR);                           /* determine state change */

        if(MSL_BASE_rr==1) {
            LCABS_vDecideWheelCtrlStateWL(&RR);                       /* determine state change */
            LCABS_vDecideWheelCtrlStateWL(&RL);                       /* determine state change */
        }
        else {
            LCABS_vDecideWheelCtrlStateWL(&RL);                       /* determine state change */
            LCABS_vDecideWheelCtrlStateWL(&RR);                       /* determine state change */
        }
        LCEBD_vDecideEBDCtrlState();
		#if __ABS_RBC_COOPERATION	/* D0506 jjino 추가 */
		LCABS_vCheckWheelState();
		#endif
    }
}

void LCABS_vDecideWheelCtrlStateWL(struct W_STRUCT *WL_temp)
{
    WL=WL_temp;                         /* for Compile */
    
  /*#if (__IDB_LOGIC==ENABLE)
    WL->LFC_Conven_OnOff_Flag=1;
  #else*/
    WL->WheelCtrlMode = 0;
#if __CHANGED_TO_DECIDE_ONOFF == ENABLE
#else
    WL->LFC_Conven_OnOff_Flag=0;
#endif
	WL->lcabsu1CaptureEstPressForSkid = 0;
	WL->lcabsu1CaptureEstPressAftDump = 0;
  /*#endif*/

  #if (L_CONTROL_PERIOD>=2)
    WL->lcabss8DumpCaseOld = WL->lcabss8DumpCase;
    WL->lcabss8DumpCase = U8_L_DUMP_CASE_00;
  #endif

  #if defined (__STAB_DCT_LOGIC_DEBUG)
    if(REAR_WHEEL_wl==0)
    {
        if(LEFT_WHEEL_wl==1)
        {
            AradLowerThresFL            = 0;
            AradHigherThresFL           = 0;
            StabDctdVThresFL            = 0;
            StabDctSlipThresFL          = 0;
            BendDiffCompensatedTempFL   = 0;
            BendSlipCompensatedTempFL   = 0;
        }
        else
        {
            AradLowerThresFR            = 0;
            AradHigherThresFR           = 0;
            StabDctdVThresFR            = 0;
            StabDctSlipThresFR          = 0;
            BendDiffCompensatedTempFR   = 0;
            BendSlipCompensatedTempFR   = 0;
        }
    }
  #endif /*defined (__STAB_DCT_LOGIC_DEBUG)*/

    LCABS_vCheckABSExitSpeed();

/*   #if __VDC
    if((ABS_LIMIT_SPEED_wl) || ((!ESP_ERROR_FLG) && (!MPRESS_BRAKE_ON)
      #if __HDC
        && (!lcu1HdcActiveFlg)
      #endif
      #if __ACC
        && (!lcu1AccActiveFlg)
      #endif
      #if __EPB_INTERFACE
        && (!lcu1EpbActiveFlg)
      #endif
    ))
   #else
    if(ABS_LIMIT_SPEED_wl)
   #endif
*/
    if(ABS_LIMIT_SPEED_wl==1)
    {
        if(SPOLD_RESET_wl==1) {
            WL->spold=0;
/*          WL->time_aft_thrdec=0; */
            WL->spold_rst_count=0;
            SPOLD_RESET_wl=0;
  /*****************************************************/
/*          WL->slip_at_vspold=0;    */           /* temp for Krohn */
  /*****************************************************/
        }
    }

    if(ABS_fz == 0) {
        FSF_wl=1;
/*
        FC_DUMP_wl=0;
        WL->increase_p_time=0;
        WL->holding_time=0; */
        LCABS_vInitializeLFCCtrlVars();
    }

    if(REAR_WHEEL_wl==1) {
        if(ABS_wl==1) {EBD_wl=0;}
    }

    if(WL->arad > (int8_t)ARAD_0G5) {JUMP_DOWN_wl=0;}
    else if(((vref >= (int16_t)VREF_90_KPH)&&(WL->rel_lam <= (int8_t)(-LAM_20P)))||
            ((vref >= (int16_t)VREF_50_KPH)&&(WL->rel_lam <= (int8_t)(-LAMUDA_2)))||
            ((vref >=(int16_t)VREF_30_KPH)&&(WL->rel_lam <= (int8_t)(-LAMUDA_3)))||
            (WL->rel_lam <= (int8_t)(-LAMUDA_4))) {JUMP_DOWN_wl=1;}
    else { }

    if(WL->arad >= (int8_t)ARAD_5G0) { /* when wheel acceleration is sufficient,*/
        CAS_wl=0;                    /* clear CAS and castim */
        WL->castim=0;
    }

/*
    tempF0=0;
    if(ABS_fz==1) {
        if(FSF_wl==1) {
            if(AFZ_OK_K==0) {
                if(arad_fl <= (int8_t)(-ARAD_1G0)) {
                    if(arad_fr <= (int8_t)(-ARAD_1G0)) {
                        if(arad_rl <= (int8_t)(-ARAD_1G0)) {
                            if(arad_rr <= (int8_t)(-ARAD_1G0)){
                                if(rel_lam_fl < (int8_t)(-LAM_2P)) {
                                    if(rel_lam_fr < (int8_t)(-LAM_2P)) {
                                        if(rel_lam_rl < (int8_t)(-LAM_2P)) {
                                            if(rel_lam_rr < (int8_t)(-LAM_2P)) {tempF0=1;}
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    if(tempF0==1) {
        tempF0=0;
        if(AV_DUMP_fl==1) {
            if(AV_DUMP_fr==1) {
                if(AV_DUMP_rl==1) {
                    if(AV_DUMP_rr==1) {
                        if(peak_dec_fl < (int8_t)(-ARAD_10G0)) {
                            if(peak_dec_fr < (int8_t)(-ARAD_10G0)) {FC_DUMP_wl=1;}
                        }
                    }
                }
            }
        }
    }

    if(FC_DUMP_wl==1) {
        if(WL->state==DETECTION) {
            if(WL->arad <= (int8_t)ARAD_0G5) {FC_DUMP_wl=0;}
        }
    }

*/
/*
    if(STABIL_wl==0) {
        if((ABS_fl==1) && (ABS_fr==1)) {
            if(afz < -(int16_t)U8_HIGH_MU) {
                if(WL->ab_z_pr > 5) {CHECK_PEAK_ACCEL_wl=1;}
            }
        }
    }
    if(afz >= -(int16_t)U8_HIGH_MU) {
        CHECK_PEAK_ACCEL_wl=0;
        FORCE_DUMP_wl=0;
        WL->force_dump_counter=0; 
    }
*/

    LCABS_vDesignatePulseDownPar();
    LCABS_vCalculatePulseDownFactor();



/*
    if(ABS_LIMIT_SPEED_wl==1) {WL->state=DETECTION;}
    if(RE_ABS_flag==1) {WL->state=DETECTION;}

    if(ABS_INHIBITION_flag==1) {WL->state=DETECTION;}
    else if((Front_ABS_INHIBITION_flag==1) && (REAR_WHEEL_wl==0)) {WL->state=DETECTION;}
    else if((REAR_ABS_INHIBITION_flag==1) && (REAR_WHEEL_wl==1)) {WL->state=DETECTION;}
    else if(ALG_INHIBIT_wl==1) {WL->state=DETECTION;}
    else { }

*/
    if( (ABS_LIMIT_SPEED_wl==1) || (RE_ABS_flag==1) || (ABS_INHIBITION_flag==1)
      || ((Front_ABS_INHIBITION_flag==1) && (REAR_WHEEL_wl==0))
      || ((REAR_ABS_INHIBITION_flag==1) && (REAR_WHEEL_wl==1))
      || ((REAR_WHEEL_wl==0)&&(((LEFT_WHEEL_wl==1)&&(fu1WheelFLErrDet==1))||((LEFT_WHEEL_wl==0)&&(fu1WheelFRErrDet==1))))
      || ((REAR_WHEEL_wl==1)&&(((LEFT_WHEEL_wl==1)&&(fu1WheelRLErrDet==1))||((LEFT_WHEEL_wl==0)&&(fu1WheelRRErrDet==1))))
    )
    {
        WL->state=DETECTION;
    }

  #if   __VDC && __ABS_COMB
    if((WL->L_SLIP_CONTROL==0) || (WL->ESP_ABS_SLIP_CONTROL==1) || (WL->CROSS_SLIP_CONTROL==1)) {
  #endif

        if(WL->state == DETECTION) {
            if(REAR_WHEEL_wl==1) {
                if(ABS_wl==0) {
                    LCEBD_vCallControl();
                    
                    if((ABS_INHIBITION_flag==0) && (ABS_fz==1)) {LCABS_vDecideSTABLEStateCtrl();}
                }
                else {
                    if((FSF_wl==1) && (WL->LFC_ABS_Forced_Enter_flag==1)) {FSF_wl=0;}
                  #if !__REAR_MI
                    if((STAB_A_rl ==1)||(STAB_A_rr==1)) {
                        SL_BASE_rl=0;
                        SL_BASE_rr=0;
                    }
                  #endif
                    LCABS_vDecideSTABLEStateCtrl();
                }
            }
            else {
                if((FSF_wl==1) && (WL->LFC_ABS_Forced_Enter_flag==1)) {FSF_wl=0;}
                LCABS_vDecideSTABLEStateCtrl();
            }
        }
        else {
            LCABS_vDecideSTABLEState();
            if(WL->state == DETECTION) {
                LCABS_vDecideSTABLEStateCtrl();
            }
            else if(WL->state == UNSTABLE) {LCABS_vDecideUNSTABLEStateCtrl();}
            else { }
        }
  #if   __VDC && __ABS_COMB
    }
    else {
        if((REAR_WHEEL_wl==0) && (FSF_wl==1) && (WL->LFC_ABS_Forced_Enter_flag==1)) {FSF_wl=0;}
        WL->WheelCtrlMode = WHEEL_CTRL_NON;
    }
  #endif



  #if   __VDC && __ABS_COMB
    if((WL->LFC_to_ESP_flag==1)&&(L_SLIP_CONTROL_wl==0)){
        if((WL->LFC_built_reapply_flag==1)||(ABS_wl==0)){
            WL->LFC_to_ESP_end_flag=1;
            WL->LFC_to_ESP_flag=0;
            WL->ESP_end_counter_final = WL->ESP_end_counter;
        }
    }
  #endif

}

void LCABS_vCheckABSExitSpeed(void)
{
/*
    tempW3=48;
    if(afz < -(int16_t)U8_HIGH_MU) {tempW3=64;}
    if(afz > -(int16_t)U8_LOW_MU) {tempW3=40;}
#if __REAR_MI
    if(LFC_Split_flag==1) {tempW3=48;}
#endif
    if(REAR_WHEEL_wl==1) {
        tempW3=40;
        if(afz < -(int16_t)U8_HIGH_MU) {tempW3=48;}
        // if(ABS_fz==1) tempW3=24;  // for RWD Acc. to brake 050730 NZ

        if((ABS_wl==0) && (ABS_fz==1)) {tempW3=24;}
    }
    if(Rough_road_detect_vehicle==1) {
        if(REAR_WHEEL_wl==1) {tempW3=64;}
        else {tempW3=72;}
    }
*/
    if(Rough_road_detect_vehicle==1) {
        if(REAR_WHEEL_wl==1) {tempW3=VREF_8_KPH;}
        else {tempW3=VREF_9_KPH;}
    }
    else {
        if(REAR_WHEEL_wl==1) {
            tempW3=VREF_5_KPH;
            if(afz < -(int16_t)U8_HIGH_MU) {tempW3=VREF_6_KPH;}
          #if __REAR_D
              #if (ABS_CONTROL_MIN_SPEED_5KPH==ENABLE)
            else if(afz >= -(int16_t)U8_LOW_MU) {tempW3=VREF_4_KPH;}
			  #else
			else if(afz >= -(int16_t)U8_LOW_MU) {tempW3=VREF_5_KPH;}
              #endif
          #else
            else if(afz >= -(int16_t)U8_LOW_MU) {tempW3=VREF_3_KPH;}
          #endif
            else { }
            /* if(ABS_fz==1) tempW3=24;  // for RWD Acc. to brake 050730 NZ*/

            if((ABS_wl==0) && (ABS_fz==1)) {tempW3=VREF_3_KPH;}
        }
        else {
            if(afz < -(int16_t)U8_HIGH_MU) {tempW3=VREF_8_KPH;}
          #if (ABS_CONTROL_MIN_SPEED_5KPH==ENABLE)
            else if(afz >= -(int16_t)U8_LOW_MU) 
			{
                if(ABS_fz==1) {tempW3=VREF_3_KPH;}
                else {tempW3=VREF_4_KPH;}
            }
          #else
			else if(afz > -(int16_t)U8_LOW_MU) {tempW3=VREF_5_KPH;}
          #endif
            else {tempW3=VREF_6_KPH;}

          #if __REAR_MI
            if(LFC_Split_flag==1) {tempW3=VREF_6_KPH;}
          #endif
        }
      #if __HDC                                         /******** 7811 NZ *********/
        if(lcu1HdcActiveFlg==1)
        {
            if(ABS_fz==0)
            {
                tempW3=VREF_4_KPH;
            }
            else
            {
                tempW3=VREF_2_KPH;
            }
        }
      #endif                                            /******** 7811 NZ *********/
    }
    if(vref <= tempW3) {ABS_LIMIT_SPEED_wl=1;}
    else {ABS_LIMIT_SPEED_wl=0;}
}

void LCABS_vInitializeLFCCtrlVars(void)
{
    if((FSF_wl==1)&&(ABS_fz==0)) {
		WL->MP_Cyclic_comp_duty = 0;
		WL->lcabss16FadeOutCnt = 0;
        WL->LFC_built_reapply_counter=0;
        WL->LFC_built_reapply_counter_old=0;
        WL->LFC_built_reapply_counter_delta=0;
        WL->LFC_built_reapply_flag=0;
        WL->LFC_built_reapply_flag_old=0;


        WL->LFC_three_built_check_flag=0;


        WL->LFC_s0_reapply_counter=0;
        WL->LFC_s0_reapply_counter_old=0;
        WL->LFC_s0_reapply_counter_old2=0;

        WL->Check_double_brake_flag=0;              /* Double_braking 030113 */
        WL->Check_double_brake_flag2=0;             /* Double_braking 030113 */

        WL->LFC_dump_counter=0;
        WL->LFC_dump_counter_old=0;
        WL->LFC_unstable_hold_counter=0;

      #if   __CHANGE_MU
        WL->Mu_change_Dump_counter=0;
        WL->Mu_change_Dump_counter_old=0;
      #endif

        WL->neg_arad_counter=0;
        WL->Pres_Rise_Defer_flag=0;

        WL->LFC_ABS_Forced_Enter_flag=0;

        WL->gma_dump1=0;
        WL->gma_dump=0;
        WL->gma_hold_counter=0;
/*
        WL->SL_DUMP=0;
*/
        if(EBD_RA==0) {WL->MSL_DUMP=0;}

      #if __UNSTABLE_REAPPLY_ENABLE
        WL->Reapply_Accel_flag=0;
        WL->Unstable_reapply_counter=0;
        WL->lcabss8URScansBfUnstDump=0;
        WL->Unst_Reapply_Scan_Ref=0;
        WL->Reapply_Accel_hold_timer=0;
        WL->Forced_Hold_After_Unst_Reapply=0;
       #if __DETECT_MU_FOR_UR
        WL->lcabsu1BumpOrWAForUR=0;
        WL->lcabsu1LMuForUR=0;
        WL->lcabsu1HMuForUR=0;
       #endif
      #endif
      #if __STABLE_DUMPHOLD_ENABLE
        WL->lcabsu1StabDump=0;
        WL->lcabsu1StabDumpAndHoldFlg=0;
        WL->lcabsu1HoldAfterStabDumpFlg=0;
        WL->lcabsu8HoldAfterStabDumpCnt=0;
        WL->lcabsu8StableDumpCnt=0;
        WL->lcabsu1StabDumpHoldCycle=0;
      #endif
      #if __MP_COMP
        if(WL->EBD_Dump_counter == 0){WL->Estimated_Press_SKID = 0;}
      #endif
        WL->lcabsu1SLDumpCycle=0;
        
        if(EBD_wl==0)
        {
            WL->hold_timer_new=0;
        }

        WL->lcabss16EstWhlPAftDump = 0;
    }
}

void LCABS_vDecideSTABLEStateCtrl(void)
{
    uint8_t   VEHICLE_DRIVE_MODE;
 #if (__4WD_VARIANT_CODE==ENABLE)
    if((lsu8DrvMode==DM_AUTO) || (lsu8DrvMode==DM_4H) || (lsu8DrvMode==DM_2H))
    {
        VEHICLE_DRIVE_MODE = 4;
    }
    else {VEHICLE_DRIVE_MODE = 2;}
 #else
  #if __4WD
    VEHICLE_DRIVE_MODE = 4;
  #else
    VEHICLE_DRIVE_MODE = 2;
  #endif
 #endif

/*
  #if   (__VDC && __PRESS_EST)
    if((REAR_WHEEL_wl==0) || (EBD_wl==0))
    {
    WL->LFC_Conven_OnOff_Flag=0;        // Added by CSH for LFC(02.01.11)
    }
  #endif
  --> Moved to LCABS_vDecideWheelCtrlStateWL()
*/

    WHEEL_ACT_MODE=0;

    if(ABS_fz==0) {
        HIGH_VFILT_wl=0;
    }

    if(RE_ABS_flag==1) {
        WL->flags=69;
        WL->WheelCtrlMode = WHEEL_CTRL_CBS;
        WHEEL_ACT_MODE = REAPPLY_MODE;
    }
  #if   (__BTC)
    else if((BTC_LOW_SLIP_wl==1) && (VEHICLE_DRIVE_MODE==2)) {
        WL->flags=71;
        WL->WheelCtrlMode = WHEEL_CTRL_CBS;
        WHEEL_ACT_MODE = REAPPLY_MODE;
    }
  #endif
/*
    else if((abs_error_flg==1) || ((REAR_WHEEL_wl==1) && ((ABS_fz==0) || ((EBD_wl==1)&&(ABS_LIMIT_SPEED_wl==1)))) && (VEHICLE_DRIVE_MODE==4))
    {
        WHEEL_ACT_MODE=0;       // wheel control command is not changed in DETECTF()
    }
    else if((REAR_WHEEL_wl==1) && (EBD_wl==1) && (ABS_LIMIT_SPEED_wl==1) && (VEHICLE_DRIVE_MODE==2))
    {
        WHEEL_ACT_MODE=0;       // wheel control command is not changed in DETECTF()
    }
*/
    else if(ABS_INHIBITION_flag==1)
    {
        WL->flags=90;
        WL->WheelCtrlMode = WHEEL_CTRL_CBS;
        WHEEL_ACT_MODE = REAPPLY_MODE;
    }
    else if((REAR_WHEEL_wl==0) && ((Front_ABS_INHIBITION_flag==1) || 
         ((LEFT_WHEEL_wl==1)&&(fu1WheelFLErrDet==1))||((LEFT_WHEEL_wl==0)&&(fu1WheelFRErrDet==1))))
    {
        WL->flags=91;
        WL->WheelCtrlMode = WHEEL_CTRL_CBS;
        WHEEL_ACT_MODE = REAPPLY_MODE;
    }
    else if((REAR_WHEEL_wl==1) && (REAR_ABS_INHIBITION_flag==1) && (ABS_LIMIT_SPEED_wl==0))
    {
        WL->flags=92;
        WHEEL_ACT_MODE = DUMP_START;

      #if (L_CONTROL_PERIOD>=2)
        WL->lcabss8DumpCase = U8_L_DUMP_CASE_11;
      #endif
    }
    else if((REAR_WHEEL_wl==1) && (EBD_wl==1) && (ABS_LIMIT_SPEED_wl==1))
    {
        WL->WheelCtrlMode = WHEEL_CTRL_CBS;
        WHEEL_ACT_MODE=0;       /* wheel control command is not changed in DETECTF() */
    }
/*    
    else if(!kph_7_flg) {
        WL->flags=70;
        WHEEL_ACT_MODE = REAPPLY_MODE;
    }
*/
    else if(lcabsu1LowSpeedInhibitFlag==0) 
    {
        WL->flags=70;
        WL->WheelCtrlMode = WHEEL_CTRL_CBS;
        WHEEL_ACT_MODE = REAPPLY_MODE;
    }

  #if __VDC
/*    if((vdc_on_flg)&&(!ESP_ERROR_FLG)) { */

/*
    else if((!ESP_ERROR_FLG) && (!MPRESS_BRAKE_ON)
      #if __HDC
        && (!lcu1HdcActiveFlg)
      #endif
      #if __ACC
        && (!lcu1AccActiveFlg)
      #endif
      #if __EPB_INTERFACE
        && (!lcu1EpbActiveFlg)
      #endif
    ) {
        WHEEL_ACT_MODE = REAPPLY_MODE;    // MAIN02
    }

*/
/*  else if((!ESP_ERROR_FLG) && (WL->L_SLIP_CONTROL==1)&&(ABS_fz==0)) {
        WL->flags=235;
        WHEEL_ACT_MODE = REAPPLY_MODE;
    }*/  /* Added by J.K.Lee at 040412 */
  #endif
    else if(ABS_LIMIT_SPEED_wl==1) {
        if(ABS_fz==0) {
        	WL->WheelCtrlMode = WHEEL_CTRL_CBS;
            WL->flags=72;
            WHEEL_ACT_MODE = REAPPLY_MODE;
        }
        else {
            /*if(U8_Hold_Timer_ABS_Limit_Speed > L_1SCAN) {
                WL->hold_timer_new_ref = U8_Hold_Timer_ABS_Limit_Speed;
            }
            else {WL->hold_timer_new_ref = L_1SCAN;}*/            
            /* WL->hold_timer_new_ref = U8_Hold_Timer_ABS_Limit_Speed; */
        	WL->WheelCtrlMode = WHEEL_CTRL_ABS_FADE_OUT;

			WL->lcabss16FadeOutCnt = WL->lcabss16FadeOutCnt + 1;
			
			if(lcabsu1InAbsAllWheelEntFadeOut == 0)
			{
				WL->hold_timer_new = 0;
			}
			else {}
			
        	LCABS_vDecideBuiltRisePMode(U8_Hold_Timer_ABS_Limit_Speed,WHEEL_CTRL_ABS_FADE_OUT);
        }
    }
    else {
        LCABS_vDecideUNSTABLEByDecelndV();
        if(WHEEL_ACT_MODE!=DUMP_START) {LCABS_vDecideUNSTABLEBySlip();}
        if(WHEEL_ACT_MODE!=DUMP_START) {LCABS_vDecideSTABLECtrlMode();}
    }

    WL->state = DETECTION;

    if(WHEEL_ACT_MODE==DUMP_START) {
    	WL->WheelCtrlMode = WHEEL_CTRL_NORMAL_ABS;
        LCABS_vWPresModeStartDUMP();
        LCABS_vUpdateLFCVarsAtDumpStart();
    }
    else if(WHEEL_ACT_MODE==REAPPLY_MODE) {
        LCABS_vWPresModeFULLREAPPLY();
    }
    else if(WHEEL_ACT_MODE==HOLD_MODE) {
        LCABS_vWPresModeSTABLEHOLD();
    }
    else if(WHEEL_ACT_MODE==BUILT_MODE) {
        LCABS_vWPresModeBUILTREAPPLY();
    }
    else if(WHEEL_ACT_MODE==DUMP_YMR) {
        LCABS_vWPresModeGMADUMP();
        if(WL->LFC_built_reapply_flag==1) {
            WL->SL_Dump_Start=1;
            LCABS_vUpdateLFCVarsAtDumpStart();
        }
        else {
            if(WL->GMA_SLIP==0)
            {
              #if (L_CONTROL_PERIOD>=2)
                WL->Mu_change_Dump_counter = LCABS_u8CountDumpNum(WL->Mu_change_Dump_counter, WL->lcabss8DumpCase);
              #else
                WL->Mu_change_Dump_counter = LCABS_u8CountDumpNum(WL->Mu_change_Dump_counter);
              #endif
            }
          #if (L_CONTROL_PERIOD>=2)
            WL->LFC_dump_counter = LCABS_u8CountDumpNum(WL->LFC_dump_counter, WL->lcabss8DumpCase);
          #else
            WL->LFC_dump_counter = LCABS_u8CountDumpNum(WL->LFC_dump_counter);
          #endif
          
          #if __PRESS_EST && __CALC_EFFECTIVE_DUMPSCANS
            if(lcabsu1ValidBrakeSensor==1)
            {
                if(WL->s16_Estimated_Active_Press > LOWEST_PRESS_LEVEL)
                {
                    WL->lcabsu8EffectiveDumpScans = WL->LFC_dump_counter;
                }
            }
            else
            {
                WL->lcabsu8EffectiveDumpScans = WL->LFC_dump_counter;
            }
          #endif

            WL->SL_Dump_Start=0;
               #if __UNSTABLE_REAPPLY_ENABLE
            if(WL->Reapply_Accel_flag==1)
            {
                WL->Reapply_Accel_flag=0;
                WL->Unstable_reapply_counter=0;
                WL->lcabss8URScansBfUnstDump=0;
                WL->Unst_Reapply_Scan_Ref=0;
                WL->Reapply_Accel_hold_timer=0;
                WL->Reapply_Accel_cycle=0;
            }
              #endif    
        }
        
        WL->lcabsu1SLDumpCycle=1;
    }
    else if(WHEEL_ACT_MODE==REAR_MSL_MODE) {
        if(MSL_BASE_rl==1) {LCABS_vCheckMSLSplitCtrl(&RR, &RL);}
        else if(MSL_BASE_rr==1) {LCABS_vCheckMSLSplitCtrl(&RL, &RR);}
        else { }

        if(WL->AV_DUMP==1) {
            if(WL->LFC_built_reapply_flag==1) {
                WL->SL_Dump_Start=1;
                LCABS_vUpdateLFCVarsAtDumpStart();
            }
            else {
              #if (L_CONTROL_PERIOD>=2)
                WL->LFC_dump_counter = LCABS_u8CountDumpNum(WL->LFC_dump_counter, WL->lcabss8DumpCase);
              #else
                WL->LFC_dump_counter = LCABS_u8CountDumpNum(WL->LFC_dump_counter);
              #endif
               #if __PRESS_EST && __CALC_EFFECTIVE_DUMPSCANS
                if(lcabsu1ValidBrakeSensor==1)
                {
                    if(WL->s16_Estimated_Active_Press > LOWEST_PRESS_LEVEL)
                    {
                        WL->lcabsu8EffectiveDumpScans = WL->LFC_dump_counter;
                    }
                }
                else
                {
                    WL->lcabsu8EffectiveDumpScans = WL->LFC_dump_counter;
                }
              #endif
                WL->SL_Dump_Start=0;
            }
            WL->Forced_Hold_After_Unst_Reapply=0;
        }
        else if(WL->BUILT==1) {
            WL->LFC_built_reapply_flag=1;
            WL->LFC_s0_reapply_counter++;
            WL->LFC_built_reapply_counter = WL->LFC_built_reapply_counter + L_1SCAN;
            if(WL->s0_reapply_counter==0)
            {
            	LCABS_vUpdVarsAftDump();
            }
            else {}

            WL->s0_reapply_counter++;
            if(WL->hold_timer_new>0) {WL->hold_timer_new=0;}
          #if __UNSTABLE_REAPPLY_ENABLE
            WL->Reapply_Accel_flag=0;
            WL->Unstable_reapply_counter=0;
            WL->Unst_Reapply_Scan_Ref=0;
            WL->Reapply_Accel_hold_timer=0;
          #endif
			#if __CHANGED_TO_DECIDE_ONOFF == ENABLE
			#else
            WL->LFC_Conven_OnOff_Flag = 1;
			#endif
        }
        else if(WL->AV_HOLD==1) {
			#if __CHANGED_TO_DECIDE_ONOFF == ENABLE
			#else
        	WL->LFC_Conven_OnOff_Flag = 1;
			#endif
            if(WL->LFC_built_reapply_flag==1) {WL->LFC_built_reapply_counter = WL->LFC_built_reapply_counter + L_1SCAN;}
        }
        else { }
    }
  #if   __VDC && __ABS_COMB
    else if(WHEEL_ACT_MODE==ESP_ABS_COOPERATION_MODE) {
    	WL->WheelCtrlMode = WHEEL_CTRL_ESC_COOP;
        LCABS_vForcedDumpHoldWheel();
        if(WL->AV_DUMP==1) {
            if(WL->LFC_built_reapply_flag==1) {
                WL->SL_Dump_Start=1;
                LCABS_vUpdateLFCVarsAtDumpStart();
            }
            else {
              #if (L_CONTROL_PERIOD>=2)
                WL->LFC_dump_counter = LCABS_u8CountDumpNum(WL->LFC_dump_counter, WL->lcabss8DumpCase);
              #else
                WL->LFC_dump_counter = LCABS_u8CountDumpNum(WL->LFC_dump_counter);
              #endif
               #if __PRESS_EST && __CALC_EFFECTIVE_DUMPSCANS
                if(lcabsu1ValidBrakeSensor==1)
                {
                    if(WL->s16_Estimated_Active_Press > LOWEST_PRESS_LEVEL)
                    {
                        WL->lcabsu8EffectiveDumpScans = WL->LFC_dump_counter;
                    }
                }
                else
                {
                    WL->lcabsu8EffectiveDumpScans = WL->LFC_dump_counter;
                }
              #endif
                WL->SL_Dump_Start=0;
            }
            WL->Forced_Hold_After_Unst_Reapply=0;
        }
        else if(WL->AV_HOLD==1) {
            if(WL->LFC_built_reapply_flag==1) {WL->LFC_built_reapply_counter = WL->LFC_built_reapply_counter + L_1SCAN;}
        }
        else { }
    }
  #endif
  #if __UNSTABLE_REAPPLY_ENABLE && (M_DP_BASE_BUILT_RISE==DISABLE)
    else if(WHEEL_ACT_MODE==UNSTABLE_REAPPLY_MODE) {
        WL->flags=202;
        LCABS_vWPresModeUNSTABLEREAPPLY();
    }
    else if(WHEEL_ACT_MODE==UNSTABLE_REAPPLY_HOLD_MODE) {
        WL->flags=206;
        LCABS_vWPresModeUnstReapplyHOLD(S16_HOLD_MODE_FORCED);
    }
  #endif
    else { }
}

void LCABS_vDecideUNSTABLEByDecelndV(void)
{
    CONTROL_STATE = 0;
  #if   __STRUCTURE_C2
/*      WL->ABS_Enter_Need_flag=0; */
  #endif

  #if (__THRDECEL_RESOLUTION_CHANGE==1)
    if(WL->arad_resol_change <= WL->lcabss16ThresDecel32) 
  #else
    if(WL->arad <= WL->thres_decel)
  #endif
    {
        WL->spold_rst_count=0;
        SPOLD_RESET_wl=1;
        tempB0 = LCABS_s8CheckDumpStartDelay();
        if(tempB0==0) {
            LCABS_vCheckDumpStart();
        }
    }
    else{
        if(SPOLD_RESET_wl==1) {
            tempB1 = LCABS_s8CheckSpoldClear();
            if(tempB1==0) {
                tempB0 = LCABS_s8CheckDumpStartDelay();
                if(tempB0==0) {
                    LCABS_vCheckDumpStart();
                }
            }
            else {
                LCABS_vClearSpold();
            }
        }
    }
}

int8_t LCABS_s8CheckSpoldClear(void)
{
    tempB7=0;

    if(ABS_wl==1) {
        if(afz >= -(int16_t)U8_HIGH_MU) {
            if(vref < (int16_t)VREF_40_KPH) {tempB7=1;}       /* '03 NZ Winter */
            else if((vref < (int16_t)VREF_70_KPH) && (WL->rel_lam > (-LAM_5P))) {
                tempB7=1;
            }
            else if((vref < (int16_t)VREF_90_KPH) && (WL->rel_lam > (-LAM_3P))) {
                tempB7=1;
            }
            else if ((WL->rel_lam > (-LAM_2P)) && ((vref-WL->vrad) < (int16_t)VREF_2_KPH)) {
                tempB7=1;
            }
            else { }
        }
        else {
            tempB0=(int8_t)(-ARAD_1G25);          /* '03 USA High-mu Test */
            if(afz < -(int16_t)U8_HIGH_MU) {tempB0=(int8_t)(-ARAD_1G75);}

            if(WL->arad > tempB0) {
                if(REAR_WHEEL_wl==0) {
                    if(WL->rel_lam > (-LAM_3P)) {tempB7=1;}
                }
                else {
                    if(WL->rel_lam > (-LAM_2P)) {tempB7=1;}

                    if(WL->arad > (int8_t)(-ARAD_1G25)) {
                        if(WL->spold_rst_count<0xff) {WL->spold_rst_count = WL->spold_rst_count + L_1SCAN;}
                        if(WL->spold_rst_count >= L_TIME_20MS) {tempB7=1;}
                    }
                    else {WL->spold_rst_count=0;}
                }
            }
        }
    }
    else {
        if(BEND_DETECT==0) {
            if(vref > (int16_t)VREF_70_KPH) {
                if(WL->rel_lam > (int8_t)(-LAM_4P)) {tempB7=1;}
            }
            else {
              #if __VDC
                if((WL->wvref-WL->vrad) < (int16_t)VREF_4_KPH) {tempB7=1;}
              #else
                if((vref-WL->vrad) < (int16_t)VREF_4_KPH) {tempB7=1;}
              #endif
            }
        }
        else {tempB7=1;}
    }

    return  tempB7;
}

void LCABS_vClearSpold(void)
{
    WL->spold=0;
/*    WL->time_aft_thrdec=0; */
    WL->spold_rst_count=0;
    SPOLD_RESET_wl=0;
  /*****************************************************/
/*    WL->slip_at_vspold=0;   */            /*temp for Krohn */
  /*****************************************************/
  #if   __DUMP_THRES_COMP_for_Spold_update
    WL->spold_comp=0;
    WL->SPOLD_COMP_FLG=0;
    if(FSF_wl==0) {WL->SPOLD_COMP_FLG1=1;}
  #endif
}

int8_t LCABS_s8CheckDumpStartDelay(void)
{
    tempB7=0;
    if(WL->rel_lam >= 0){
        if((FSF_wl==1)&&(EBD_RA==1)){
            if( vref>=VREF_80_KPH ){
                if( WL->rel_lam >= 1 ) {tempB7=1;}
                else { }
            }
            else if( WL->rel_lam >= 2 ) {tempB7=1;}
            else { }
        }
        else {tempB7=1;}
    }
    else { }
    return  tempB7;
}

void LCABS_vCheckDumpStart(void)
{
/*    WL->time_aft_thrdec++; */

    if(WL->spold == 0) {
        WL->spold=WL->speed;
        if(FSF_wl==1) {

          #if __VDC
            if(ESP_ERROR_FLG==0) {
                if(WL->speed >= WL->wvref) {WL->spold=WL->wvref;}
            }
            else {
                if(WL->speed >= vref2) {WL->spold=vref2;}
            }
          #else
            if(WL->speed >= vref2) {WL->spold=vref2;}
          #endif

            if(REAR_WHEEL_wl==1)  {
                if((vref2-WL->spold)>(int16_t)VREF_1_KPH) {WL->spold=(vref2-(int16_t)VREF_1_KPH);}
            }

            if(MINI_SPARE_WHEEL_wl==1) {WL->spold=WL->speed;}
        }

      #if __DUMP_THRES_COMP_for_Spold_update
        if((FSF_wl==0)&&(WL->SPOLD_COMP_FLG1==1))
        {
            LCABS_vCompTHRdVforSpoldUpdate();
            WL->spold=WL->spold+WL->spold_comp;
            WL->SPOLD_COMP_FLG1=0;
            if(WL->spold_comp > 0) {WL->SPOLD_COMP_FLG=1;}
            else {WL->SPOLD_COMP_FLG=0;}
        }
      #endif

        CONTROL_STATE = UNDECIDED;
    }
    else {
        LCABS_vIgnoreSlipByEngBraking();
        if (CONTROL_STATE != SETTLED) {

          #if __SPOLD_UPDATE       /*  Added by J.K.Lee at 030926 */
          /**********************************************************************/
            if((MINUS_B_SET_AGAIN_OLD_wl==0)&&(MINUS_B_SET_AGAIN_wl==1)){
                if((REAR_WHEEL_wl==0)&&(FSF_wl==1)){
/*                    if(vref >= (int16_t)VREF_100_KPH) {
                        if(WL->rel_lam >= (-LAM_4P)) {WL->spold=WL->speed;}
                    }
                    else {WL->spold=WL->speed;}
*/
                    if((vref-WL->vrad) <= (VREF_2_KPH+(vref/50))) {WL->spold=WL->speed;}
                }

            }
            MINUS_B_SET_AGAIN_OLD_wl = MINUS_B_SET_AGAIN_wl;
          /**********************************************************************/
          #endif

          #if (__THRDECEL_RESOLUTION_CHANGE==1)
            if(WL->arad_resol_change <= WL->lcabss16ThresDecel32) 
          #else
            if(WL->arad <= WL->thres_decel) 
          #endif
            {
              #if __DUMP_THRES_COMP_for_Spold_update
/*
                if ((FSF_wl==0)&&(WL->SPOLD_COMP_FLG==1)&&(WL->SPOLD_COMP_FLG1==1)){
                    DUMP_THRES_COMP_for_Spold_update();
                    WL->spold=WL->spold+WL->spold_comp;
                }

                WL->SPOLD_COMP_FLG=1;
*/
              #endif

                if((WL->spold-WL->speed) >= WL->vfiltn) {
                  #if  __HIGH_MU_DECEL_IMPROVE
                    if((BEND_DETECT2==1) || (BEND_DETECT_flag==1))
                    {
                       #if __VDC
                        if(((LEFT.ESP_BEND_flag==1)||(BEND_DETECT_LEFT==1)) && (REAR_WHEEL_wl==0) && (LEFT_WHEEL_wl==1))
                        {
                            tempB6 = (int8_t)LCABS_s16Interpolation2P(vref,S16_ABS_ALLOW_SLIP_REF_LSPD, S16_ABS_ALLOW_SLIP_REF_HSPD, S8_ABS_ALLOW_SLIP_LSPD_BEND_IN, S8_ABS_ALLOW_SLIP_HSPD_BEND_IN);
                        }
                        else if(((RIGHT.ESP_BEND_flag==1)||(BEND_DETECT_RIGHT==1)) && (REAR_WHEEL_wl==0) && (LEFT_WHEEL_wl==0))
                        {
                            tempB6 = (int8_t)LCABS_s16Interpolation2P(vref,S16_ABS_ALLOW_SLIP_REF_LSPD, S16_ABS_ALLOW_SLIP_REF_HSPD, S8_ABS_ALLOW_SLIP_LSPD_BEND_OUT, S8_ABS_ALLOW_SLIP_HSPD_BEND_OUT);
                        }
                        else
                       #endif 
                        {
                            tempB6 = (int8_t)LCABS_s16Interpolation2P(vref,S16_ABS_ALLOW_SLIP_REF_LSPD, S16_ABS_ALLOW_SLIP_REF_HSPD, S8_ABS_ALLOW_SLIP_LSPD_BEND, S8_ABS_ALLOW_SLIP_HSPD_BEND);
                        }
                    }
                    else
                    {
                        tempB6 = (int8_t)LCABS_s16Interpolation2P(vref,S16_ABS_ALLOW_SLIP_REF_LSPD, S16_ABS_ALLOW_SLIP_REF_HSPD, S8_ABS_ALLOW_SLIP_LSPD, S8_ABS_ALLOW_SLIP_HSPD);
                        if(ABS_fz==1) {tempB6 = (int8_t)LCABS_s16Interpolation2P(vref,S16_ABS_ALLOW_SLIP_REF_LSPD, S16_ABS_ALLOW_SLIP_REF_HSPD, S8_ABS_ALLOW_SLIP_LSPD_IN, S8_ABS_ALLOW_SLIP_HSPD_IN);}
                    }
                    
                    if((ABS_wl==1) || (REAR_WHEEL_wl==1) || ((GMA_SLIP_wl==1) && (GMA_PULSE==1)) ||
                       (WL->rel_lam<=-tempB6) || (U8_ABS_ENTRANCE_LIMIT_BY_SLIP==0)
                       #if __PAC
                       || ((lcpacu1PacActiveFlg==1)&&(WL->rel_lam<=-S8_PRE_ABS_OUT_TH_SLIP)
                           &&((LCPACFL.u8ConvenReapplyTime >= U8_PRE_ABS_MIN_ALLOWED_PULSE_UP)||(LCPACFR.u8ConvenReapplyTime >= U8_PRE_ABS_MIN_ALLOWED_PULSE_UP)))
                       #endif                       
                    )
                  #endif /* __HIGH_MU_DECEL_IMPROVE */
                    {
	                    if(REAR_WHEEL_wl==0) {
	                        if(vref < (int16_t)VREF_70_KPH) {
	                            if(vref >= (int16_t)VREF_20_KPH) {
	                                if(WL->vfiltn >= (int16_t)VREF_8_KPH) {HIGH_VFILT_wl=1;}
	                            }
	                            else {
	                                if(WL->vfiltn >= (int16_t)VREF_5_KPH) {HIGH_VFILT_wl=1;}
	                            }
	                        }
	                    }
	                    /*
	                    if(FSF_wl==1) {
	                        WL->flags=2;
	                        goto DUMP_DECEL;
	                    }
	                    else goto CHK_TIME_AFT_THRDEC;
	                    */
	                    CONTROL_STATE = SETTLED;
	                    WHEEL_ACT_MODE = DUMP_START;
	                    WL->flags=2;
	                  #if   __STRUCTURE_C2
	/*                  if(ABS_wl==0) WL->ABS_Enter_Need_flag=1; */
	                  #endif
                	}
                }
/*              else CONTROL_STATE = UNDECIDED; */
            }
/*          else CONTROL_STATE = UNDECIDED; */
        }
    }
    
    
  #if (L_CONTROL_PERIOD>=2)
    if(WHEEL_ACT_MODE == DUMP_START)
    {
        if(WL->Dump_hold_scan_ref>0)
        {
            WL->lcabss8DumpCase = U8_L_DUMP_CASE_10;
        }
        else
        {
            WL->lcabss8DumpCase = U8_L_DUMP_CASE_11;
        }
    }
  #endif /*(L_CONTROL_PERIOD>=2)*/
}

void LCABS_vIgnoreSlipByEngBraking(void)
{
/*    #if !__VDC  */
      #if __REAR_D
        if(REAR_WHEEL_wl==1) {                         /* ENGINE BRAKING */
            if((lcabsu1BrakeByBLS==0)
              #if __VDC
            && (BRAKE_BY_MPRESS_FLG==0)             /* MPRESS_BRAKE_ON */
              #endif
            )
            {
                if((rel_lam_fl >= (int8_t)(-LAM_2P))||(rel_lam_fr >= (int8_t)(-LAM_2P))) {
                    if(WL->rel_lam > (int8_t)(-LAM_25P)) {
                        if((ABS_rl==0) && (ABS_rr==0)) {
                            WL->flags=1;
                            CONTROL_STATE = SETTLED;
                            WHEEL_ACT_MODE = REAPPLY_MODE;
                        }
                    }
                }
            }
        }
      #else
        if(REAR_WHEEL_wl==0) {                         /* ENGINE BRAKING */
            if((lcabsu1BrakeByBLS==0)
              #if __VDC
            && (BRAKE_BY_MPRESS_FLG==0)             /* MPRESS_BRAKE_ON */
              #endif
            )
            {
                if((rel_lam_rl >= (int8_t)(-LAM_2P))||(rel_lam_rr >= (int8_t)(-LAM_2P))) {
                    if(WL->rel_lam > (int8_t)(-LAM_25P)) {
                        if((ABS_fl==0) && (ABS_fr==0)) {
                            WL->flags=1;
                            CONTROL_STATE = SETTLED;
                            WHEEL_ACT_MODE = REAPPLY_MODE;
                        }
                    }
                }
            }
        }
      #endif
/*    #endif */
}

void LCABS_vDecideUNSTABLEBySlip(void)
{

    tempW0=WL->speed;                               /* test slip */
  #if __VDC
    tempW0=WL->wvref-tempW0;
  #else
    if(SCHNELLSTE_VREF_wl==1) {tempW0=vref - tempW0;}
    else {tempW0=vref2-tempW0;}
  #endif
    if((lcabsu1BrakeByBLS==1) || (ABS_fz==1)
      #if __VDC
        || (BRAKE_BY_MPRESS_FLG==1)     /* MPRESS_BRAKE_ON */
        || (ACTIVE_BRAKE_FLG==1)
      #endif
      ) {
        if((tempW0 >= 0) && (WL->arad<=ARAD_0G0))          /* PPR 2009-118 */
        {
            if(REAR_WHEEL_wl==0) {
                if(tempW0 >= VREF_12_KPH) {
                    tempW2=vref2;
                    tempW1=ABS_DETECT_SLIP_THRESHOLD_PRO;
                    u16mul16div();
                    if(tempW0 >= (tempW3 + WL->vfiltn)) {
                        WL->flags=3;
                        WHEEL_ACT_MODE = DUMP_START;
                    }
                }
                if(tempW0 > VREF_4_KPH) {
                    if(WL->Rough_road_detector4>=S8_Rough_road_detector_F_level2){
                        if(WL->rel_lam <= (int8_t)(-LAM_50P)) {
                            if(AFZ_OK == 1) {
                                WL->flags=4;
                                WHEEL_ACT_MODE = DUMP_START;
                            }
                        }
                    }
                    else if((Rough_road_suspect_vehicle==1)||(Rough_road_detect_vehicle==1)){
                        if(WL->rel_lam <= (int8_t)(-LAM_30P)) {
                            if(AFZ_OK == 1) {
                                WL->flags=4;
                                WHEEL_ACT_MODE = DUMP_START;
                            }
                        }
                    }
                    else{
                        if(WL->rel_lam <= (int8_t)(-LAM_20P)) {
                            if(AFZ_OK == 1) {
                                WL->flags=4;
                                WHEEL_ACT_MODE = DUMP_START;
                            }
                        }
                    }
                }
                if((ABS_wl==0)&&(ABS_fz==1)&&(GMA_SLIP_wl==0)&&(BEND_DETECT2==0)) {
                  #if (__THRDECEL_RESOLUTION_CHANGE==1)
                    if(WL->arad_resol_change <= WL->lcabss16ThresDecel32) 
                  #else
                    if(WL->arad <= WL->thres_decel) 
                  #endif 
                     {
                        if((tempW0 >= VREF_2_KPH)&&(WL->rel_lam <= (-S8_dV_F_slip_after_ABS_fz))) {
                          #if __HIGH_MU_DECEL_IMPROVE && __VDC && __MP_COMP
                            tempB6 = S8_dV_F_slip_after_ABS_fz;
                            if(REAR_WHEEL_wl==0) 
                            {
                                if((LEFT_WHEEL_wl==1)&&(FR.Estimated_Press_SKID>=MPRESS_70BAR)&&((FR.Estimated_Press_SKID+MPRESS_5BAR)>FL.s16_Estimated_Active_Press))
                                {
                                    tempB6 = (int8_t)LCABS_s16Interpolation2P(vref,VREF_25_KPH, VREF_150_KPH, 8, 5);
                                }
                                else if((LEFT_WHEEL_wl==0)&&(FL.Estimated_Press_SKID>=MPRESS_70BAR)&&((FL.Estimated_Press_SKID+MPRESS_5BAR)>FR.s16_Estimated_Active_Press))
                                {
                                    tempB6 = (int8_t)LCABS_s16Interpolation2P(vref,VREF_25_KPH, VREF_150_KPH, 8, 5);
                                }
                                else { }
                            }
                              #if (__ROUGH_COMP_IMPROVE==ENABLE)
                            if((Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1))
                            {
                            	tempB6 = (int8_t)LCABS_s16Interpolation2P(vref,VREF_50_KPH, VREF_80_KPH, 10, 6);
                            }
                            else {}
                        	  #endif  /*__ROUGH_COMP_IMPROVE*/ 
                        	  
                        	  #if (__BUMP_COMP_IMPROVE == ENABLE)
                        	if((FL.PossibleABSForBump==1)||(FR.PossibleABSForBump==1))
                            {
                            	tempB6 = (int8_t)LCABS_s16Interpolation2P(vref,VREF_20_KPH, VREF_80_KPH, 14, 6);
                            }
                            else {}
                              #endif  /*__BUMP_COMP_IMPROVE*/
                            
                            if((WL->rel_lam <= (-tempB6)) || (U8_ABS_ENTRANCE_LIMIT_BY_SLIP==0))
                            {
                                WL->flags=234;
                                WHEEL_ACT_MODE = DUMP_START;
                            }
                          #else /* __HIGH_MU_DECEL_IMPROVE && __VDC && __MP_COMP */
                            tempB6 = S8_dV_F_slip_after_ABS_fz;/*3%*/
                              #if (__ROUGH_COMP_IMPROVE==ENABLE)
                            if((Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1))
                            {
                            	tempB6 = (int8_t)LCABS_s16Interpolation2P(vref,VREF_50_KPH, VREF_80_KPH, 10, 6);
                            }
                            else {}
                        	  #endif  /*__ROUGH_COMP_IMPROVE*/ 
                        	  
                        	  #if (__BUMP_COMP_IMPROVE == ENABLE)
                        	if((FL.PossibleABSForBump==1)||(FR.PossibleABSForBump==1))
                            {
                            	tempB6 = (int8_t)LCABS_s16Interpolation2P(vref,VREF_20_KPH, VREF_80_KPH, 14, 6);
                            }
                            else {}
                              #endif  /*__BUMP_COMP_IMPROVE*/
                            
                            if((WL->rel_lam <= (-tempB6)) || (U8_ABS_ENTRANCE_LIMIT_BY_SLIP==0))
                            {                                                        
                            	WL->flags=234;
                            	WHEEL_ACT_MODE = DUMP_START;                           
                            }
                          #endif /* __HIGH_MU_DECEL_IMPROVE && __VDC && __MP_COMP */
                        }
                    }
                }
              #if __VDC  /* 06 China Winter by J.K.Lee */
                if((BRAKE_BY_MPRESS_FLG==1)&&(ABS_wl==0)&&(GMA_SLIP_wl==0)&&(esp_mu<LAT_0G55G))
                {
/*
                    if(((LEFT_WHEEL_wl==1)&&(SLIP_CONTROL_NEED_FR==1))||
                       ((LEFT_WHEEL_wl==0)&&(SLIP_CONTROL_NEED_FL==1))) {
*/
                    if(((LEFT_WHEEL_wl==1)&&(ESP_BRAKE_CONTROL_FR==1))||
                       ((LEFT_WHEEL_wl==0)&&(ESP_BRAKE_CONTROL_FL==1))) {
                      #if (__THRDECEL_RESOLUTION_CHANGE==1)
                        if(WL->arad_resol_change <= WL->lcabss16ThresDecel32) 
                      #else
                        if(WL->arad <= WL->thres_decel) 
                      #endif   
                        {
                            if(((WL->spold-WL->speed) >= WL->vfiltn)||(tempW0 >= VREF_1_KPH)) {
                                WL->flags=250;
                                WHEEL_ACT_MODE = DUMP_START;
                            }
                        }
                    }
                }
              #endif
              #if __HDC                                         /******** 7811 NZ *********/
                if(lcu1HdcActiveFlg==1)
                {
                    if((tempW0 >= VREF_5_KPH) && (WL->rel_lam <= (int8_t)(-LAM_50P)))
                    {
                        WL->flags=251;
                        WHEEL_ACT_MODE = DUMP_START;
                    }
                }
              #endif                                            /******** 7811 NZ *********/
            }
            else {
                if((tempW0 >= VREF_5_KPH) && (tempW0 >= (int16_t)((uint16_t)(WL->vfiltn)<<2))) {
                    WL->flags=5;
                    WHEEL_ACT_MODE = DUMP_START;
                }
                else if((vref > VREF_8_KPH)&&(WL->rel_lam <= (int8_t)(-LAM_40P))) {                     /* NSM03 */
                    WL->flags=6;
                    WHEEL_ACT_MODE = DUMP_START;
                }
                else if(vref > VREF_25_KPH) {
                    if((WL->rel_lam <= (int8_t)(-LAM_20P)) && (tempW0 > VREF_4_KPH) && (AFZ_OK==1)) {
                        WL->flags=11;
                        WHEEL_ACT_MODE = DUMP_START;
                    }
/*                  else if((HOLD_RES_fl==0) && (HOLD_RES_fr==0)) {
                    else if((FL.Rough_road_detector<=5) && (FR.Rough_road_detector<=5)) {  */
                        if(ABS_wl==1) {

                            if((Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1)) {tempB7 = (int8_t)(-LAM_30P);}
                            else {tempB7 = (int8_t)(-LAM_15P);}

                            if(WL->rel_lam <= tempB7) {
                                WL->flags=8;
                                WHEEL_ACT_MODE = DUMP_START;
                            }
                            else if(WL->rel_lam <= (int8_t)(-LAM_7P)) {
                                if(vref >= (int16_t)VREF_80_KPH) {
                                    WL->flags=7;
                                    WHEEL_ACT_MODE = DUMP_START;
                                }
                            }
                            else { }
                        }
                        else {
                            if(ABS_fz==1) {
                                if((BEND_DETECT2==0)
                                  #if (__BUMP_COMP_IMPROVE==ENABLE)
                                &&(PossibleABSForBumpVehicle==0)
                                  #endif
                                )
                                {
                                	  #if (__BUMP_COMP_IMPROVE==ENABLE)
                                	tempW4 = LCABS_s16Interpolation2P(vref,VREF_80_KPH,VREF_30_KPH, LAM_7P, LAM_15P);
                                	tempW5 = LCABS_s16Interpolation2P(vref,VREF_60_KPH,VREF_30_KPH, LAM_10P, LAM_15P);
                                	  #else
                                	tempW4 = LAM_7P;
                                	tempW5 = LAM_10P; 
                                	  #endif/*(__BUMP_COMP_IMPROVE==ENABLE)*/
                                    if(vref >= (int16_t)VREF_80_KPH) {
                                        if(WL->rel_lam < (int8_t)(-LAM_5P)) {
                                          #if (__THRDECEL_RESOLUTION_CHANGE==1)
                                            if(WL->arad_resol_change <= WL->lcabss16ThresDecel32) 
                                          #else
                                            if(WL->arad <= WL->thres_decel) 
                                          #endif  
                                            {   /* STD17_GK */
                                                WL->flags=9;
                                                WHEEL_ACT_MODE = DUMP_START;
                                            }
                                        }
                                    }
                                    else {
                                        if(WL->rel_lam < (int8_t)(-tempW4)) {
                                            if(WL->arad <= (int8_t)(-ARAD_2G5)) { /* GK36 */
                                                WL->flags=10;
                                                WHEEL_ACT_MODE = DUMP_START;
                                            }
                                        }
                                    }
                                    if(WL->rel_lam < (int8_t)(-tempW5)) {
                                        if((vref-WL->vrad)>(int16_t)VREF_2_KPH) {
                                            WL->flags=240;
                                            WHEEL_ACT_MODE = DUMP_START;
                                        }
                                    }
                                }
                            }
                        }
/*                  }  */
                }
                else { }
              #if __HDC                                         /******** 7811 NZ *********/
                if(lcu1HdcActiveFlg==1)
                {
                    if((tempW0 >= VREF_5_KPH) && (WL->rel_lam <= (int8_t)(-LAM_50P)) && (ABS_fz==1))
                    {
                        WL->flags=251;
                        WHEEL_ACT_MODE = DUMP_START;
                    }
                }
              #endif                                            /******** 7811 NZ *********/
            }
        }
    }

    if(WHEEL_ACT_MODE==DUMP_START) 
    {
        CONTROL_STATE=SETTLED;

      #if (L_CONTROL_PERIOD>=2)
        if(WL->Dump_hold_scan_ref>0)
        {
            WL->lcabss8DumpCase = U8_L_DUMP_CASE_10;
        }
        else
        {
            WL->lcabss8DumpCase = U8_L_DUMP_CASE_11;
        }
      #endif /*(L_CONTROL_PERIOD>=2)*/
    }
  #if   __STRUCTURE_C2
/*  if(ABS_wl==0) WL->ABS_Enter_Need_flag=1; */
  #endif

}

void LCABS_vDecideSTABLECtrlMode(void)
{
    int8_t MSL_SPLIT_CONTROL;

    MSL_SPLIT_CONTROL=0;
    WL->lcabsu1OutsideABSCtrlDecided=0;

  #if   __VDC && __ABS_COMB
    if(REAR_WHEEL_wl==0) {
        if(delta_yaw>=YAW_20DEG) {
            if(WL->ESP_ABS_Over_Front_Inside==1) {
            	WL->WheelCtrlMode = WHEEL_CTRL_ESC_COOP;
                WHEEL_ACT_MODE = ESP_ABS_COOPERATION_MODE;
                CONTROL_STATE = SETTLED;
            }
        }
        else {WL->ESP_ABS_Front_Forced_Dump_timer=Forced_DUMP_Scan_Front;}
    }
    else {
        if(delta_yaw>=YAW_20DEG) {
            if(WL->ESP_ABS_Over_Rear_Inside==1) {
            	WL->WheelCtrlMode = WHEEL_CTRL_ESC_COOP;
                WHEEL_ACT_MODE = ESP_ABS_COOPERATION_MODE;
                CONTROL_STATE = SETTLED;
            }
        }
        else {WL->ESP_ABS_Rear_Forced_Dump_timer=Forced_DUMP_Scan_Rear;}

        if(WL->ESP_ABS_Under_Rear_Outside==1) {
        	WL->WheelCtrlMode = WHEEL_CTRL_ESC_COOP;
            WL->ESP_ABS_Rear_Forced_Dump_timer=0;
            WHEEL_ACT_MODE = ESP_ABS_COOPERATION_MODE;
            CONTROL_STATE = SETTLED;
        }
    }
  #endif /* __VDC && __ABS_COMB */

    if(CONTROL_STATE!=SETTLED) {

       #if __VDC && __ESP_SPLIT_2
        if((GMA_PULSE==1) && ((GMA_SLIP_wl==1) || (WL->GMA_ESP_Control==1))) {
       #else
        if((GMA_SLIP_wl==1) && (GMA_PULSE==1)) {
       #endif
        	WL->WheelCtrlMode = WHEEL_CTRL_YMR_ABS;
            LCABS_vDecideFrontHSideGMACtrl();
        }
        else if((ABS_wl==0)&&(REAR_WHEEL_wl==0)) {
            if((ABS_fl==1) || (ABS_fr==1)) {
                if(BEND_DETECT2==0) {
                  #if __ABS_RBC_COOPERATION
                    if(FSF_wl==1)
                    {
                    	if(lcabsu1FastPressRecoveryBfAbs==1)
                    	{
                            if(vref < (int16_t)VREF_60_KPH) {
                              #if __VDC
                                if((WL->wvref-WL->speed) >= (int16_t)VREF_4_KPH)
                              #else
                                if((vref-WL->speed) >= (int16_t)VREF_4_KPH)
                              #endif
                                {
                                    WL->lcabsu1OutsideABSCtrlDecided=1; /* Added by CSH for LFC(02.01.11) */
                                }
                            }
                            else {
                                if(WL->rel_lam < (int8_t)(-LAM_4P)) {
                                    WL->lcabsu1OutsideABSCtrlDecided=1; /* Added by CSH for LFC(02.01.11) */
                                }
                            }
                    	}
                    	else
                    	{
                    		WL->lcabsu1OutsideABSCtrlDecided=1;
                    	}
                    }
                  #else /* __ABS_RBC_COOPERATION */
                    if(FSF_wl==1) {WL->lcabsu1OutsideABSCtrlDecided=1;}
                  #endif  /* __ABS_RBC_COOPERATION */
                }
                else {
                    if(WL->s0_reapply_counter !=0) {
                        WL->lcabsu1OutsideABSCtrlDecided=1; /* Added by CSH for LFC(02.01.11) */
                    }
                    else {
                      #if __VDC && __OUT_ABS_PULSEUP_FOR_T2B_AT_LMU
                        if(esp_mu < LAT_0G55G) /* 0.4g low-med mu */
                        {
                            if(esp_mu < 250) /* 0.25g low mu */
                            {
                                WL->lcabsu1OutsideABSCtrlDecided=1;
                            }
                            else
                            {
                                if((WL->rel_lam <= (int8_t)(-LAM_2P))&&((vref-WL->vrad)>=VREF_1_5_KPH)
                                 #if (__PRESS_EST && __EST_MU_FROM_SKID)
                                 && (FL.lcabsu8PossibleMuFromSkid!=U8_PULSE_HIGH_MU)&&(FR.lcabsu8PossibleMuFromSkid!=U8_PULSE_HIGH_MU)
                                 #endif 
                                )
                                {
                                    WL->lcabsu1OutsideABSCtrlDecided=1;
                                }
                            }
                        }
                        else
                      #endif
                        {
                            if(vref < (int16_t)VREF_60_KPH) {
                              #if __VDC
                                if((WL->wvref-WL->speed) >= (int16_t)VREF_4_KPH)
                              #else
                                if((vref-WL->speed) >= (int16_t)VREF_4_KPH)
                              #endif
                                {
                                    WL->lcabsu1OutsideABSCtrlDecided=1; /* Added by CSH for LFC(02.01.11) */
                                }
                            }
                            else {
                                if(WL->rel_lam <= (int8_t)(-LAMUDA_1)) {
                                    WL->lcabsu1OutsideABSCtrlDecided=1; /* Added by CSH for LFC(02.01.11) */
                                }
                            }
                        }
                    }
                }
              #if (__FRONT_OUT_ABS_SL_DUMP==ENABLE)
                if(BEND_DETECT2==0) 
                {
                    MSL_SPLIT_CONTROL = LCABS_s8CheckMSLCtrlMode();
                }
              #endif
              
              #if __STABLE_DUMPHOLD_ENABLE && (__UNDERSTEER_FRT_STB_DUMP_BFABS || __OUT_ABS_STABLE_DUMP_ENABLE)
                LCABS_vCheckStbDumpHoldMode();
              #endif
            }           
            else { }
  
          #if (__FRONT_OUT_ABS_SL_DUMP==ENABLE)
            if(MSL_SPLIT_CONTROL>=1) 
            {
            	WL->WheelCtrlMode = WHEEL_CTRL_YMR_ABS;
                /*if(REAR_WHEEL_wl==0)*/
                {
                    WL->flags=241;
                    WHEEL_ACT_MODE = DUMP_YMR;
                }             
            }                
            else
          #endif
          #if __STABLE_DUMPHOLD_ENABLE && (__UNDERSTEER_FRT_STB_DUMP_BFABS || __OUT_ABS_STABLE_DUMP_ENABLE)
            if(WL->lcabsu1StabDumpAndHoldFlg==1)
            {
            	WL->WheelCtrlMode = WHEEL_CTRL_YMR_ABS;

                if(WL->lcabsu1StabDump == 1)
                {
                    WL->flags=205;
                    WHEEL_ACT_MODE = DUMP_YMR;
                }
                else
                {
                    WL->flags=207;
                    WHEEL_ACT_MODE = HOLD_MODE;
                }
				  #if (__IDB_LOGIC==ENABLE)
					#if __CHANGED_TO_DECIDE_ONOFF == ENABLE
					#else
				    WL->LFC_Conven_OnOff_Flag=1;
					#endif
				  #else
                WL->LFC_Conven_OnOff_Flag=0;
				  #endif
            }
            else
          #endif
            if(WL->lcabsu1OutsideABSCtrlDecided==1) {
                WL->WheelCtrlMode = WHEEL_CTRL_OUT_ABS;
				#if (__IDB_LOGIC==ENABLE)
					#if __CHANGED_TO_DECIDE_ONOFF == ENABLE
					#else
				  WL->LFC_Conven_OnOff_Flag=1;
					#endif
				#else
				  WL->LFC_Conven_OnOff_Flag=0;
				#endif

              #if __VDC
                if((ACTIVE_BRAKE_FLG==1) && (vref < VREF_35_KPH))
                {
                    WL->hold_timer_new_ref = 0;
                      /* to generate wheel pressure at HDC||ACC||EPB control */
                }
               #if __OUT_ABS_PULSEUP_FOR_T2B_AT_LMU
                else if(BEND_DETECT2==1)
                {
                    if((WL->rel_lam >= (int8_t)(-LAM_4P))||((vref-WL->vrad)<=VREF_2_5_KPH))
                    {
                        WL->hold_timer_new_ref = (uint8_t)LCABS_s16Interpolation2P(esp_mu,500,250, L_TIME_10MS, L_TIME_50MS);
                    }
                    else
                    {
                        WL->hold_timer_new_ref = L_TIME_50MS;
                    }
                }
               #endif/*__OUT_ABS_PULSEUP_FOR_T2B_AT_LMU*/
		       #if __VDC && __HDC && (__HDC_ABS_IMPROVE==ENABLE)
		        else if((lcu1HdcActiveFlg==1) && (ACTIVE_BRAKE_FLG==0)&&(WL->s0_reapply_counter>=4)
		         && (vref>=(int16_t)VREF_5_KPH)&&((LFC_Split_suspect_flag==1)||(LFC_Split_flag==1)))
		        {
		            WL->hold_timer_new_ref = (uint8_t)LCABS_s16Interpolation2P((int16_t)WL->s0_reapply_counter, 8, 12, L_TIME_120MS, L_TIME_180MS);
		        }
		       #endif /* (__HDC_ABS_IMPROVE == ENABLE) */
                else
              #endif/*__VDC*/
                {
                  #if (__SLIGHT_BRAKING_COMPENSATION)
                    if((lcabsu1SlightBraking==1) && ((WL->rel_lam >= (int8_t)(-LAM_4P))||((vref-WL->vrad)<=VREF_2_5_KPH)))
                    {
                        if(vref < VREF_40_KPH)
                        {
                            WL->hold_timer_new_ref = (uint8_t)LCABS_s16Interpolation2P(lcabss16RefCircuitPress,
                                                            S16_VERY_SLIGHT_BRAKING_PRESS,S16_SLIGHT_BRAKING_PRESS, L_TIME_0MS, L_TIME_60MS);
                        }
                        else
                        {
                            WL->hold_timer_new_ref = (uint8_t)LCABS_s16Interpolation2P(lcabss16RefCircuitPress,
                                                            S16_VERY_SLIGHT_BRAKING_PRESS,S16_SLIGHT_BRAKING_PRESS, L_TIME_30MS, L_TIME_60MS);
                        }
                    }
                    else
                    {
                        WL->hold_timer_new_ref = L_TIME_50MS;
                    }
                  #else
                    WL->hold_timer_new_ref = L_TIME_50MS;
                  #endif

				  #if (__BUMP_COMP_IMPROVE==ENABLE)
				    if((PossibleABSForBumpVehicle==1)&&(vref<=S16_BUMP_MED_SPD))
				    {
				        if(WL->hold_timer_new_ref > L_TIME_30MS)
				        {
				            WL->hold_timer_new_ref = L_TIME_30MS;
				        }
				    }
				  #endif                  

                  #if __OUT_ABS_P_RATE_COMPENSATION
                   /* Level 1 => hold time adaptation per slip*/
                    if(WL->OutABSYMRCtrlMode>=LEVEL1)
                    {
                        WL->hold_timer_new_ref = WL->hold_timer_new_ref + lcabsu8AdaptHoldPerSlipDiff;
                    }
                  #endif
                }

                if(WL->lcabss8WhlCtrlReqForStrkRec==U8_HOLD_DURING_STRK_RECVRY)
                {
                	LCABS_vCheckForcedHldForStrRcvr(WL->hold_timer_new>=WL->hold_timer_new_ref);
                	WL->lcabsu1WhlPForcedHoldCmd = 1;
                	WHEEL_ACT_MODE = HOLD_MODE;
                }
                else
                {
                	if((WL->lcabss8WhlCtrlReqForStrkRec==U8_RISE_BF_STRK_RECVRY)||(WL->lcabss8WhlCtrlReqForStrkRec==U8_RISE_AF_STRK_RECVRY))
                	{
                		WL->hold_timer_new_ref = 0;
                	}

                	LCABS_vDecideBuiltRisePMode(WL->hold_timer_new_ref,WHEEL_CTRL_OUT_ABS);
                }
            }
          #if   __VDC
            else if((l_SLIP_A_wl==1) && (ABS_fz==1)) {
            	WL->WheelCtrlMode = WHEEL_CTRL_CBS;
                LCABS_vCheckABSForcedEnter();
                WL->flags=51;
                WHEEL_ACT_MODE = REAPPLY_MODE;
            }
          #endif
            else {
            	WL->WheelCtrlMode = WHEEL_CTRL_CBS;
                WL->flags=52;
                WHEEL_ACT_MODE = REAPPLY_MODE;
            }
        }
        else if(ABS_wl==1) {


		#if (M_ON_OFF_BUILT_PATTERN==ENABLE) /* need to move */
			#if __CHANGED_TO_DECIDE_ONOFF == ENABLE
			#else
		  if(ABS_LIMIT_SPEED_wl==1)
		  {
			  WL->LFC_Conven_OnOff_Flag = 0;
		  }
		  else
		  {
			  WL->LFC_Conven_OnOff_Flag = 1;
		  }
		#endif
		#endif


            MSL_SPLIT_CONTROL = LCABS_s8CheckMSLCtrlMode();

          #if __STABLE_DUMPHOLD_ENABLE
            LCABS_vCheckStbDumpHoldMode();
          #endif

            if(MSL_SPLIT_CONTROL>=1) {
                if(REAR_WHEEL_wl==0) {
                	WL->WheelCtrlMode = WHEEL_CTRL_YMR_ABS;
                	WL->flags=241;
                    WHEEL_ACT_MODE = DUMP_YMR;
                }
                else {
                	WL->WheelCtrlMode = WHEEL_CTRL_MSL_REAR;
/*                  #if __UNSTABLE_REAPPLY_ENABLE
                    if(LEFT_WHEEL_wl==1) {WHEEL_ACT_MODE = LCABS_U8CheckMSLUnstableReapply(&RL, &RR);}
                    else                 {WHEEL_ACT_MODE = LCABS_U8CheckMSLUnstableReapply(&RR, &RL);}
                    if(WHEEL_ACT_MODE == 0)
                  #endif
*/                      {
							WHEEL_ACT_MODE = REAR_MSL_MODE;
					    }

                }
            }
          #if __STABLE_DUMPHOLD_ENABLE
            else if(WL->lcabsu1StabDumpAndHoldFlg==1)
            {
            	WL->WheelCtrlMode = WHEEL_CTRL_YMR_ABS;
            	if(WL->lcabsu1StabDump == 1)
                {
                    WL->flags=205;
                    WHEEL_ACT_MODE = DUMP_YMR;
                }
                else
                {
                    WL->flags=207;
                    WHEEL_ACT_MODE = HOLD_MODE;
                }
            }
          #endif
            else {
            	WL->WheelCtrlMode = WHEEL_CTRL_NORMAL_ABS;

                LCABS_vCheckForcedHold();

                LCABS_vExtricateFromCASCADING();

                if((WL->Hold_Duty_Comp_flag==0)&&(WL->Pres_Rise_Defer_flag==0))
                {
                    if(WL->lcabss8WhlCtrlReqForStrkRec==U8_HOLD_DURING_STRK_RECVRY)
					{
                	LCABS_vCheckForcedHldForStrRcvr(WL->hold_timer_new>=WL->hold_timer_new_ref);
                }
                    else {}
                }
				#if (M_DP_BASE_BUILT_RISE==DISABLE)
              #if __UNSTABLE_REAPPLY_ENABLE
                LCABS_vDecideDeltaPCtrlMode();
              #endif
				#endif

                if((WL->Hold_Duty_Comp_flag==1)||(WL->Pres_Rise_Defer_flag==1)||(WL->lcabss8WhlCtrlReqForStrkRec==U8_HOLD_DURING_STRK_RECVRY))
                {
                	WL->lcabsu1WhlPForcedHoldCmd = 1;
                	WHEEL_ACT_MODE = HOLD_MODE;
                }

              #if (__VDC && __ABS_CORNER_BRAKING_IMPROVE)  
                else if(WL->lcabsu1CornerRearOutHold==1) 
                {
                	WL->lcabsu1WhlPForcedHoldCmd = 1;
                    WHEEL_ACT_MODE = HOLD_MODE;
                    WL->flags=127;
                }
              #endif
              
               #if (__UNSTABLE_REAPPLY_ENABLE) && (M_DP_BASE_BUILT_RISE==DISABLE)
                else if(WL->Reapply_Accel_flag==1)
                {
                    if(WL->Forced_Hold_After_Unst_Reapply==1)
                    {
                        WL->flags=206;
                        WHEEL_ACT_MODE=UNSTABLE_REAPPLY_HOLD_MODE;
                    }
                    else
                    {
                        WL->flags=202;
                        WHEEL_ACT_MODE=UNSTABLE_REAPPLY_MODE;
                    }
                }
               #endif

                else {
                    if(WL->s0_reapply_counter==0) {
                        WL->hold_timer_new_ref = 0;
                    }
                    else if(WL->lcabsu1InABSInitialRiseDone==0) {
                    	WL->hold_timer_new_ref = U8_HOLD_TIMER_INIT_RISE_DISTBN;
                    }
                  #if __WP_DROP_COMP
                    else if(WL->s0_reapply_counter==1) {
                        if(WL->WP_Drop_comp_flag==1) {WL->hold_timer_new_ref = WL->Drop_Press_Comp_T;}
                    }
                  #endif
                    else if((WL->lcabss8WhlCtrlReqForStrkRec==U8_RISE_BF_STRK_RECVRY)||(WL->lcabss8WhlCtrlReqForStrkRec==U8_RISE_AF_STRK_RECVRY)) /* not forced hold */
                    {
                    	WL->hold_timer_new_ref = 0;
                    }
                    else if(WL->lcabsu1InABSNthReduceHoldTimReq == 1)
                    {
                    	if(WL->hold_timer_new_ref > U8_HOLD_TIMER_NTH_REDUCED_HOLD)
                    	{
                    		/*WL->hold_timer_new_ref = WL->hold_timer_new_ref - L_TIME_10MS; default : L_TIME_10MS*/

                    		/*WL->hold_timer_new_ref = U8_HOLD_TIMER_NTH_REDUCED_HOLD;*/ /*not apply to multiplex control, but apply onoff control*/
                    	}
                    	else { }
                    }
                    else { }

                    LCABS_vDecideBuiltRisePMode(WL->hold_timer_new_ref,WHEEL_CTRL_NORMAL_ABS);
                }
            }
            WL->LFC_ABS_Forced_Enter_flag=0;
        }
        else {
            /* ((ABS_wl==0) && (REAR_WHEEL_wl==1)) => END : EBD Control */
          #if __STABLE_DUMPHOLD_ENABLE && __OVERSTEER_REAR_STB_DUMP_BFABS
            LCABS_vCheckStbDumpHoldMode();
            if(WL->lcabsu1StabDumpAndHoldFlg==1)
            {
            	WL->WheelCtrlMode = WHEEL_CTRL_OUT_ABS;

                if(WL->lcabsu1StabDump == 1)
                {
                    WL->flags=205;
                    WHEEL_ACT_MODE = DUMP_YMR;
                }
                else
                {
                    WL->flags=207;
                    WHEEL_ACT_MODE = HOLD_MODE;
                }
				  #if (__IDB_LOGIC==ENABLE)
					#if __CHANGED_TO_DECIDE_ONOFF == ENABLE
					#else
				    WL->LFC_Conven_OnOff_Flag=1;
					#endif
				  #else
                	WL->LFC_Conven_OnOff_Flag=0; /* check purpose... to transfer from GMA to LFC after stable dump? */
				  #endif                
            }
            else
          #endif
            {
                MSL_SPLIT_CONTROL = LCABS_s8CheckMSLCtrlMode();

                if(MSL_SPLIT_CONTROL>=1)
                {
                	WL->WheelCtrlMode = WHEEL_CTRL_MSL_REAR;
                	WHEEL_ACT_MODE = REAR_MSL_MODE;
                }
                LCABS_vCheckABSForcedEnter();
            }
        }
    }

}
#if (M_ON_OFF_BUILT_PATTERN==ENABLE)
#else
void LCABS_vDecideSTABLEReapply(void) /*TODO cye*/
{
    if(WL->hold_timer_new >= WL->hold_timer_new_ref) {
        WL->hold_timer_new=0;
        if(WL->LFC_Conven_OnOff_Flag==1) {
            WL->flags=100;
            WHEEL_ACT_MODE = REAPPLY_MODE;
            LCABS_vCheckABSForcedEnter();
        }
        else {
            WL->flags=101;
            WHEEL_ACT_MODE = BUILT_MODE;
        }
    }
    else {
        WL->hold_timer_new = WL->hold_timer_new + L_1SCAN;
        WL->flags=110;
        WHEEL_ACT_MODE = HOLD_MODE;
    }
}
#endif

void LCABS_vCheckForcedHold(void)
{
/*    int8_t HOLD_ARAD=0; */
    int8_t HOLD_SLIP=0;
    int16_t HOLD_dV=0;
    uint8_t MINIMUM_RISE_SCAN = 0;
  
  #if __IMPROVE_DECEL_AT_CERTAIN_HMU
    int8_t s8StableForcedHoldScans = S8_MAXIMUM_FORCED_HOLD_SCAN;
  #endif

    WL->Hold_Duty_Comp_flag=0;

    if(REAR_WHEEL_wl==0) { /*Forced HOLD*/
        if(AFZ_OK==0) {
            HOLD_SLIP = -S8_FORCED_HOLD_SLIP_BFAFZOK_F;
        }
        else if(afz>=-(int16_t)U8_LOW_MU) {
            HOLD_SLIP = -S8_FORCED_HOLD_SLIP_LOW_F;
        }
        else if(afz>=-(int16_t)U8_HIGH_MU) {
            HOLD_SLIP = -S8_FORCED_HOLD_SLIP_MED_F;
        }
        else {
          #if __IMPROVE_DECEL_AT_CERTAIN_HMU
            if(lcabsu1CertainHMu==1)
            {
              #if (__STABLE_HOLD_THRESHOLD_BY_SPD == ENABLE)
                if(vref < (int16_t)VREF_40_KPH)
                {
                	HOLD_SLIP = -S8_FORCED_HLD_SLIP_CHMU_B40KPH_F;
                }
                else if(vref < (int16_t)VREF_80_KPH)
                {
                	HOLD_SLIP = -S8_FORCED_HLD_SLIP_CHMU_B80KPH_F;
                }
                else
                {
                	HOLD_SLIP = -S8_FORCED_HLD_SLIP_CHMU_A80KPH_F;
                }
              #else
                HOLD_SLIP = -S8_FORCED_HOLD_SLIP_HIGH_F + LAM_1P;
              #endif
            }
            else
          #endif
            {
                HOLD_SLIP = -S8_FORCED_HOLD_SLIP_HIGH_F;
            }
        }

        if((LFC_Split_flag==1) && (CERTAIN_SPLIT_flag==1) && (UN_GMA_wl==0)&&(UN_GMA_rl==1)&&(UN_GMA_rr==1))
        {
            if(((LEFT_WHEEL_wl==1)&&(MSL_BASE_rr==1)) || ((LEFT_WHEEL_wl==0)&&(MSL_BASE_rl==1)))
            {
                HOLD_SLIP = -S8_FORCED_HOLD_SLIP_SP_HIGH_F;
            }
        }
    }
    else {
        if(AFZ_OK==0) {
            HOLD_SLIP = -S8_FORCED_HOLD_SLIP_BFAFZOK_R;
        }
        else if(afz>=-(int16_t)U8_LOW_MU) {
            HOLD_SLIP = -S8_FORCED_HOLD_SLIP_LOW_R;
        }
        else if(afz>=-(int16_t)U8_HIGH_MU) {
            HOLD_SLIP = -S8_FORCED_HOLD_SLIP_MED_R;
        }
        else {
            HOLD_SLIP = -S8_FORCED_HOLD_SLIP_HIGH_R;
        }
    }

    if(vref<=VREF_20_KPH)
    {
        if(REAR_WHEEL_wl==0) {HOLD_SLIP = HOLD_SLIP - LAM_2P;}
        else {HOLD_SLIP = HOLD_SLIP - LAM_1P;}
    }
    else if(vref<=VREF_30_KPH)
    {
        HOLD_SLIP = HOLD_SLIP - LAM_1P;
    }
    else { }
    
    HOLD_SLIP = (int8_t)LCABS_s16LimitMinMax((int16_t)HOLD_SLIP,-LAM_10P,-LAM_1P);
    

    if(WL->Hold_Duty_Comp_flag_1st==0) {HOLD_dV = (int16_t)((WL->vfiltn*2)/5);}         /* '03 USA High-mu Test */
    else {HOLD_dV = (int16_t)((WL->vfiltn*3)/10); }

  #if __IMPROVE_DECEL_AT_CERTAIN_HMU
    if(lcabsu1CertainHMu==1)
    {
        HOLD_dV = (HOLD_dV>VREF_0_5_KPH)?VREF_0_5_KPH:HOLD_dV;
    }
    else if(lcabsu1CertainHMuPre==1)
    {
        HOLD_dV = (HOLD_dV>VREF_1_KPH)?VREF_1_KPH:HOLD_dV;
    }
    else
    {
        HOLD_dV = (HOLD_dV>VREF_1_5_KPH)?VREF_1_5_KPH:HOLD_dV;
    }
  #endif

    if(afz>=-(int16_t)U8_LOW_MU) {
        HOLD_dV = 0;
    }


    if((Rough_road_detect_vehicle==0) && (Rough_road_suspect_vehicle==0) && (vref>=VREF_10_KPH)) {
/*
        if(WL->spold==0) {
            if((WL->arad <= HOLD_ARAD) && (WL->rel_lam <= HOLD_SLIP)) {
                WL->Hold_Duty_Comp_flag=0;
                if(WL->Hold_Duty_Comp_flag_1st==0) WL->Hold_Duty_Comp_flag_1st=1;
                WL->flags=111;
            }
        }
*/
        if((WL->rel_lam <= HOLD_SLIP) && ((WL->spold-WL->vrad) >= HOLD_dV) &&
          #if (__THRDECEL_RESOLUTION_CHANGE==1)
            (WL->arad_resol_change <= (WL->lcabss16ThresDecel32 + ARAD32_0G5)) 
          #else
            (WL->arad <= (WL->thres_decel + 2))
          #endif
        )
        {
            WL->Hold_Duty_Comp_flag=1;
            if(WL->Hold_Duty_Comp_flag_1st==0) {WL->Hold_Duty_Comp_flag_1st=1;}
            WL->flags=112;
        }
        else if((WL->rel_lam <= (HOLD_SLIP)) && (vref>VREF_15_KPH) &&
          #if (__THRDECEL_RESOLUTION_CHANGE==1)
            (WL->arad_resol_change <= (WL->lcabss16ThresDecel32 + ARAD32_0G25))
          #else
            (WL->arad <= (WL->thres_decel + 1))
          #endif
        )
        {
            WL->Hold_Duty_Comp_flag=1;
            if(WL->Hold_Duty_Comp_flag_1st==0) {WL->Hold_Duty_Comp_flag_1st=1;}
            WL->flags=117;
        }
        else { }

    }

  #if __VDC && __HDC && (__HDC_ABS_IMPROVE==ENABLE)
    if((lcu1HdcActiveFlg==1) && (WL->Hold_Duty_Comp_flag==0))
    {
    	if(NUM_OF_STABIL_WHEELS <= 1)
    	{
    		WL->Hold_Duty_Comp_flag=1;
    		WL->flags=114;
    	}
    }
  #endif  
 
    tempB7=0;
    if(WL->Hold_Duty_Comp_flag==0) {
        if(WL->hold_timer_new>=WL->hold_timer_new_ref) { /*&& (Hold time 추가 강제 HOLD)*/
        
            lcabss8temp0 = L_TIME_20MS;

          #if __RISE_DELAY_SP_FHSIDE_FRONT
            
            lcabss8temp1 = (int8_t)LCABS_s16Interpolation2P(vref,S16_SP_FHSIDE_RISE_DELAY_SP_L,S16_SP_FHSIDE_RISE_DELAY_SP_H,
                                                               S8_SP_FHSIDE_RISE_DELAY_SLIP_L,S8_SP_FHSIDE_RISE_DELAY_SLIP_H);
            lcabss8temp2 = (int8_t)LCABS_s16Interpolation2P(vref,S16_SP_FHSIDE_RISE_DELAY_SP_L,S16_SP_FHSIDE_RISE_DELAY_SP_H,
                                                               S8_SP_FHSIDE_RISE_DELAY_ACCEL_L,S8_SP_FHSIDE_RISE_DELAY_ACCEL_H);
            
            lcabss8temp2 += (int8_t)LCABS_s16Interpolation2P(WL->rel_lam,-LAM_50P,-LAM_30P,ARAD_2G0,ARAD_0G0);

            if((LFC_Split_flag==1)&&(vref>=VREF_20_KPH)&&(REAR_WHEEL_wl==0))
            {
                if(LEFT_WHEEL_wl==0)
                {
                    if((FL.state==UNSTABLE)&&(FL.rel_lam<lcabss8temp1)&&(FL.peak_acc<lcabss8temp2)) 
                    {
                        tempB7=1;
                    }
                    else if((FL.state==UNSTABLE)&&(FL.rel_lam<(lcabss8temp1+LAM_5P))&&(FL.peak_acc<(lcabss8temp2+ARAD_1G0)))
                    {
                        tempB7=2;
                    }
                    else { }
                      #if __VDC
                    if(ESP_BRAKE_CONTROL_FL==1)
                    {
                        tempB7=10;
                    }
                      #endif
                }
                else
                {
                    if((FR.state==UNSTABLE)&&(FR.rel_lam<lcabss8temp1)&&(FR.peak_acc<lcabss8temp2)) 
                    {
                        tempB7=1;
                    }
                    else if((FR.state==UNSTABLE)&&(FR.rel_lam<(lcabss8temp1+LAM_5P))&&(FR.peak_acc<(lcabss8temp2+ARAD_1G0)))
                    {
                        tempB7=2;
                    }
                    else { }
                      #if __VDC
                    if(ESP_BRAKE_CONTROL_FR==1)
                    {
                        tempB7=10;
                    }
                      #endif
                }
            }
            else { }
            
            if(tempB7==1)
            {
                lcabss8temp0 = (int8_t)LCABS_s16Interpolation2P(vref,VREF_30_KPH,VREF_100_KPH,L_TIME_20MS,L_TIME_70MS);
            }
            else if(tempB7==2)
            {
                lcabss8temp0 = (int8_t)LCABS_s16Interpolation2P(vref,VREF_30_KPH,VREF_100_KPH,L_TIME_10MS,L_TIME_50MS);
            }
              #if __VDC
            else if(tempB7==10)
            {
                lcabss8temp0 = (int8_t)LCABS_s16Interpolation2P(vref,VREF_30_KPH,VREF_100_KPH,L_TIME_30MS,L_TIME_70MS);
            }
              #endif
            else { }
          #endif
            
          #if (__BIG_RISE_DETECT_ENABLE_MGH_80==1)
            if(((WL->LFC_Big_Rise_detect==1)||(tempB7>=1)) && (WL->Forced_Hold_counter<=(uint8_t)lcabss8temp0) && (WL->Forced_Hold_cnt<=(uint8_t)lcabss8temp0))
            {
                WL->Hold_Duty_Comp_flag=1;
                WL->Forced_Hold_counter = WL->Forced_Hold_counter + L_1SCAN;
                WL->flags=113;
                if(tempB7>=1) {WL->flags=126;}
            }
          #endif
        }
    }

  #if __RISE_DELAY_SP_FHSIDE_FRONT
    if(tempB7>=1)
    {
        if(vref>VREF_70_KPH)
        {
            MINIMUM_RISE_SCAN = 1;
        }
        else if(vref>VREF_30_KPH)
        {
            MINIMUM_RISE_SCAN = 1;
        }
        else
        {
            MINIMUM_RISE_SCAN = 2;
        }
        
      #if __UNSTABLE_REAPPLY_ENABLE  
        if(WL->Reapply_Accel_cycle==1)
        {
            if(WL->Unstable_reapply_counter>=L_TIME_30MS)
            {
                MINIMUM_RISE_SCAN = 0;
            }
            else if(MINIMUM_RISE_SCAN>0)
            {
                MINIMUM_RISE_SCAN = MINIMUM_RISE_SCAN - 1;
            }
            else { }
        }
      #endif  
      #if __VDC
        if(tempB7==10)
        {
            MINIMUM_RISE_SCAN = 0;
        } 
      #endif  
    }
    else
  #endif
    {
        if((AFZ_OK==1) && (afz>=-(int16_t)(U8_PULSE_LOW_MU))) {
            MINIMUM_RISE_SCAN = 2;
        }
        else
        {
          #if __IMPROVE_DECEL_AT_CERTAIN_HMU
            if(lcabsu1CertainHMu==1)
            {
                MINIMUM_RISE_SCAN = WL->LFC_dump_counter-1;
                MINIMUM_RISE_SCAN = (uint8_t)LCABS_s16LimitMinMax((int16_t)MINIMUM_RISE_SCAN,2,5);
            }
            else
            {
                MINIMUM_RISE_SCAN = 3;
            }
          #else
            MINIMUM_RISE_SCAN = 3;
          #endif
        }
    }

    if(WL->LFC_s0_reapply_counter<MINIMUM_RISE_SCAN) {WL->Hold_Duty_Comp_flag=0;}
    if(LOW_to_HIGH_suspect==1) {WL->Hold_Duty_Comp_flag=0;}
    else if(((WL->LOW_to_HIGH_suspect2==1)
	  	#if(__L2H_IMPROVE_BY_WHEEL_PRESS==ENABLE)
    		||(lcabsu1L2hSuspectByWhPres==1)
	  	#endif  
			)
      #if (__VDC && __PRESS_EST)
		#if (__IDB_LOGIC==ENABLE)
		 #if(__REF_P_RESOL_CHANGED_10_TO_100 == ENABLE)
		&& ((lcabsu1ValidBrakeSensor==1) && ((WL->s16_Estimated_Active_Press+MPRESS_10BAR)<=S16_PRESS_RESOL_100_TO_10(lcabss16RefFinalTargetPres)))
		 #else
		&& ((lcabsu1ValidBrakeSensor==1) && ((WL->s16_Estimated_Active_Press+MPRESS_10BAR)<=lcabss16RefFinalTargetPres))
		 #endif
		#else	
		&& ((lcabsu1ValidBrakeSensor==1) && ((WL->s16_Estimated_Active_Press+MPRESS_10BAR)<=lcabss16RefMpress))
		#endif
      #endif
    )
    {
        WL->Hold_Duty_Comp_flag=0;
    }
    else { }
    

  #if __IMPROVE_DECEL_AT_CERTAIN_HMU
    if((lcabsu1CertainHMu==1)||(lcabsu1CertainHMuPre==1))
    {
        s8StableForcedHoldScans = (int8_t)((WL->LFC_s0_reapply_counter)*(L_TIME_100MS/WL->hold_timer_new_ref));
        s8StableForcedHoldScans = (s8StableForcedHoldScans>S8_MAXIMUM_FORCED_HOLD_SCAN)?S8_MAXIMUM_FORCED_HOLD_SCAN:s8StableForcedHoldScans;
    }
    else
    {
        s8StableForcedHoldScans = S8_MAXIMUM_FORCED_HOLD_SCAN;
    }
    if(WL->Forced_Hold_cnt>=s8StableForcedHoldScans) {WL->Hold_Duty_Comp_flag=0;}    

  #else /*__IMPROVE_DECEL_AT_CERTAIN_HMU*/

    if(WL->Forced_Hold_cnt>=S8_MAXIMUM_FORCED_HOLD_SCAN) {WL->Hold_Duty_Comp_flag=0;}

  #endif /*__IMPROVE_DECEL_AT_CERTAIN_HMU*/


/*=========================================================================================*/
    if((WL->Hold_Duty_Comp_flag==0) && (WL->arad<=ARAD_4G0) && (WL->arad>=ARAD_0G0)) {
        tempC1=0;
        if(vref_limit_dec_flag==0) {
            tempC1=L_TIME_1000MS;
        }
        else {tempC1=L_TIME_40MS;}

        lcabss16temp1 = LCABS_s16CheckFastestWheel();
        if(lcabss16temp1==1)
        {
            if((REAR_WHEEL_wl==1)&&(vref_limit_dec_counter_rr>=(int16_t)tempC1))
            {
                WL->Hold_Duty_Comp_flag=1;
                WL->flags=115;
            }
            else if((REAR_WHEEL_wl==0)&&(vref_limit_dec_counter_frt>=(int16_t)tempC1))
            {
              #if __IMPROVE_DECEL_AT_CERTAIN_HMU
                if(((lcabsu1CertainHMu==1)||(lcabsu1CertainHMuPre==1))&&((WL->Reapply_Accel_flag==1) || (vref <= VREF_15_KPH)))
                {
                    ;
                }
                else
              #endif
                {
                    WL->Hold_Duty_Comp_flag=1;
                    WL->flags=115;
                }
            }
            else { }
        }
/*        
        if(REAR_WHEEL_wl==1) {
            if(vref_limit_dec_counter_rr>=(int16_t)tempC1) {
                if(LEFT_WHEEL_wl==1) {
                    if((vrad_rl>vrad_rr) && (vrad_rl>vrad_fl) && (vrad_rl>vrad_fr))  {
                        WL->Hold_Duty_Comp_flag=1;
                        WL->flags=115;
                    }
                }
                else {
                    if((vrad_rr>vrad_rl) && (vrad_rr>vrad_fl) && (vrad_rr>vrad_fr))  {
                        WL->Hold_Duty_Comp_flag=1;
                        WL->flags=115;
                    }
                }
            }
        }
        else {
            if(vref_limit_dec_counter_frt>=(int16_t)tempC1) {
                if(LEFT_WHEEL_wl==1) {
                    if((vrad_fl>vrad_rr) && (vrad_fl>vrad_rl) && (vrad_fl>vrad_fr))  {
                        WL->Hold_Duty_Comp_flag=1;
                        WL->flags=115;
                    }
                }
                else {
                    if((vrad_fr>vrad_rl) && (vrad_fr>vrad_fl) && (vrad_fr>vrad_rr))  {
                        WL->Hold_Duty_Comp_flag=1;
                        WL->flags=115;
                    }
                }
            }
        }
*/        
      #if __STABLE_DUMPHOLD_ENABLE
        if(WL->lcabsu1StabDumpHoldCycle==1)
        {
            WL->Hold_Duty_Comp_flag=0;
        }
      #endif    
        
    }
      
      #if __SPLIT_STABILITY_IMPROVE_GMA
    if(REAR_WHEEL_wl==0)
    {
        if((WL->lcabsu8YawRiseHoldTime2>0) && (WL->LFC_s0_reapply_counter>=1) && ((lcabsu1OverInsideFrontCtrl==1)||(lcabsu1OverInsideRearCtrl==1))
          && (WL->hold_timer_new>=WL->hold_timer_new_ref) && (WL->Forced_Hold_cnt<WL->lcabsu8YawRiseHoldTime2) && (vref>VREF_15_KPH)
            #if (__SLIGHT_BRAKING_COMPENSATION)
    	  &&(lcabsu1SlightBraking==0)
            #endif
          )
        {
            WL->Hold_Duty_Comp_flag=1;
            WL->flags=111;
        }
    }
    else
    {
        ;
    }
      #endif    
  #if __FIRST_CYCLE_YAW_ADAPT   
    if((FL.LFC_zyklus_z<1) && (FR.LFC_zyklus_z<1) && (FL.lcabsu8PossibleMuFromSkid==U8_PULSE_LOW_MU) && (FR.lcabsu8PossibleMuFromSkid==U8_PULSE_LOW_MU))
    {
        if((REAR_WHEEL_wl==0) && (WL->lcabss16YawDumpFactor2>=100) && (WL->LFC_STABIL_counter<=L_TIME_200MS))
        {
            if(LEFT_WHEEL_wl==1)
            {
                if((FR.rel_lam<-LAM_10P)&&(FR.Reapply_Accel_flag==0)&&(FR.LFC_built_reapply_flag==0))
                {
                    WL->Hold_Duty_Comp_flag=1;
                    WL->flags=125;
                }
            }       
            else
            {
                if((FL.rel_lam<-LAM_10P)&&(FL.Reapply_Accel_flag==0)&&(FL.LFC_built_reapply_flag==0))
                {
                    WL->Hold_Duty_Comp_flag=1;
                    WL->flags=125;
                }
            }
        }
    }
  #endif    
    
    if(WL->Hold_Duty_Comp_flag==1) {
        WL->Forced_Hold_cnt = WL->Forced_Hold_cnt + L_1SCAN;
    }
}

#if __RISE_DELAY_FOR_PREV_CAS
void LCABS_vExtricateFromCASCADING(void)
{
    int8_t    MAX_WHEEL_ACCEL;

    if(WL->peak_acc_alt > WL->max_arad_instbl) {MAX_WHEEL_ACCEL = WL->peak_acc_alt;}
    else {MAX_WHEEL_ACCEL = WL->max_arad_instbl;}

  #if   __REAR_D
    tempW5 = -(int16_t)U8_PULSE_LOW_MU;
  #else
    tempW5 = -(int16_t)U8_PULSE_LOW_MED_MU;
  #endif

  #if __IMP_RISEDELAY_AND_DUMP_FOR_CAS
    if((WL->CAS_SUSPECT_flag==1) && (MAX_WHEEL_ACCEL<ARAD_3G0))
    {
        tempW5 = -(int16_t)U8_PULSE_MED_HIGH_MU;
    }
  #endif 
  
    lcabss16temp1 = (int16_t)WL->flags;

    if(WL->s0_reapply_counter==0) 
    {
        tempB0 = 0; tempB7 = 0; tempB5 = 0; tempB4 = 0; tempB1 = 0;
        
        if((WL->LFC_zyklus_z<=0) &&                                             /* Non driven wheel 1st cycle */
          #if   __REAR_D
            (REAR_WHEEL_wl==0)
          #else
            (REAR_WHEEL_wl==1)
          #endif
        )
        {
            if(MAX_WHEEL_ACCEL>=ARAD_10G0) {tempB0 = L_TIME_20MS;}
            else if(MAX_WHEEL_ACCEL>=ARAD_8G0) {tempB0 = L_TIME_40MS;}
            else 
            {
              #if (__IMPROVE_DECEL_AT_CERTAIN_HMU==ENABLE) && !( __REAR_D )

             	if( ((lcabsu1CertainHMuPre==1)||(lcabsu1CertainHMu==1)) && (state_fl==DETECTION) && (state_fr==DETECTION) )
             	{
                    tempB0 = (int8_t)LCABS_s16Interpolation2P(vref, VREF_15_KPH, VREF_150_KPH, L_TIME_0MS, L_TIME_100MS);
                 	lcabss16temp1=131;
             	}
             	else
                {
                    tempB0 = (int8_t)LCABS_s16Interpolation2P(vref, VREF_15_KPH, VREF_150_KPH, L_TIME_0MS, L_TIME_150MS);//L_TIME_300MS
             	    lcabss16temp1=120;
             	}

              #else  /*__IMPROVE_DECEL_AT_CERTAIN_HMU==ENABLE*/

                tempB0 = (int8_t)LCABS_s16Interpolation2P(vref, VREF_15_KPH, VREF_150_KPH, L_TIME_0MS, L_TIME_300MS);
         	    lcabss16temp1=120;

         	  #endif /*__IMPROVE_DECEL_AT_CERTAIN_HMU==ENABLE*/
             	
              #if __IMP_1CYCLE_NON_DRIVEN_W_RISE_DELAY
                if((WL->CAS_SUSPECT_flag==1) && (MAX_WHEEL_ACCEL<ARAD_3G0)) 
                {
                    tempB0 = tempB0*2;
                    lcabss16temp1=125;
                }
              #endif
            }
        }
        else if(vref >= VREF_15_KPH)
        {
            if((AFZ_OK==1) && (afz>=tempW5)) 
            {
                tempB7 = (int8_t)LCABS_s16CheckFastestWheel();
            }
            
        #if __TEMP_for_CAS
            tempB5 = (int8_t)NUM_OF_STABIL_WHEELS;
        #else
          #if   __VDC
            if((YAW_CDC_WORK==1) || (ESP_PULSE_DUMP_FLAG==1)) 
            {
                if((ESP_BRAKE_CONTROL_FL==0) && (state_fl==DETECTION)) {tempB5 += 1;}
                if((ESP_BRAKE_CONTROL_FR==0) && (state_fr==DETECTION)) {tempB5 += 1;}
                if((ESP_BRAKE_CONTROL_RL==0) && (state_rl==DETECTION)) {tempB5 += 1;}
                if((ESP_BRAKE_CONTROL_RR==0) && (state_rr==DETECTION)) {tempB5 += 1;}
            }
            else
            {
                if(state_fl==DETECTION) {tempB5 += 1;}
                if(state_fr==DETECTION) {tempB5 += 1;}
                if(state_rl==DETECTION) {tempB5 += 1;}
                if(state_rr==DETECTION) {tempB5 += 1;}
            }
          #else
            if(state_fl==DETECTION) {tempB5 += 1;}
            if(state_fr==DETECTION) {tempB5 += 1;}
            if(state_rl==DETECTION) {tempB5 += 1;}
            if(state_rr==DETECTION) {tempB5 += 1;}
          #endif
        #endif

          #if   __TEMP_for_CAS2                                                /* Cascading Suspect Wheel */
              #if __IMP_N_CYCLE_RISE_DELAY
            if((WL->LFC_zyklus_z>0) && (WL->CAS_SUSPECT_flag==1) && (MAX_WHEEL_ACCEL<ARAD_3G0) && (AFZ_OK==1) && (afz>-(int16_t)U8_PULSE_HIGH_MU))
            {
                tempB0 = (int8_t)LCABS_s16Interpolation2P(vref, VREF_10_KPH, VREF_100_KPH, L_TIME_40MS, L_TIME_350MS);
                lcabss16temp1=121;
                #if ((__4WD==1)||(__4WD_VARIANT_CODE==ENABLE))
                  #if (__4WD==1)
                if(tempB7==1)
                  #else
                if(((lsu8DrvMode==DM_AUTO)||(lsu8DrvMode==DM_4H))&&(tempB7==1))
                  #endif
                {
                    tempB0 = tempB0 + L_TIME_70MS;
                    lcabss16temp1=128;
                    if(vref>=vref_alt) 
                    {
                        tempB0 = tempB0 + L_TIME_140MS;
                        lcabss16temp1=129;
                    }
            	}
            	#endif
            }
              #else
            if((WL->CAS_SUSPECT_flag==1) && (MAX_WHEEL_ACCEL<ARAD_3G0) && (AFZ_OK==1) && (afz>-(int16_t)U8_PULSE_HIGH_MU))
            {
                tempB0 = (int8_t)LCABS_s16Interpolation2P(vref, VREF_10_KPH, VREF_100_KPH, L_TIME_40MS, L_TIME_350MS);
                lcabss16temp1=121;
            }
              #endif
          #endif

            if((MAX_WHEEL_ACCEL<=ARAD_0G5) && (LOW_to_HIGH_suspect==0) && (BEND_DETECT2==0)) 
            {                                                                   /* Wheel slip does not recover */
                tempB4 = L_TIME_70MS;
                if(tempB0 < tempB4) 
                { 
                    tempB0 = tempB4;
                    lcabss16temp1=122;
                }
            }

            if(
              #if   __REAR_D
               ((MAX_WHEEL_ACCEL<ARAD_5G0) || (WL->arad_gauge<=0)) && (REAR_WHEEL_wl==0)
              #else
               ((MAX_WHEEL_ACCEL<ARAD_6G0) || (WL->arad_gauge<=0)) && (REAR_WHEEL_wl==1)
              #endif
            )
            {
                if(tempB5==1)                                                   /* Only Stable wheel */
                {
                    if((AFZ_OK==1) && (afz>=tempW5))
                    {
                        tempB4 = L_TIME_600MS;
                    }
                    else
                    {
                        tempB4 = L_TIME_60MS;
                    }
                    
                    if(tempB0 < tempB4) 
                    { 
                        tempB0 = tempB4;
                        lcabss16temp1=123;
                    }
                }
                
                if(tempB7==1)                                                   /* Low mu fastest wheel */
                {
                    tempB4 = L_TIME_70MS;
                    if(tempB0 < tempB4) 
                    { 
                        tempB0 = tempB4;
                        lcabss16temp1=124;
                    }
                }
            }
            
            if((tempB0 > 0) && (MAX_WHEEL_ACCEL<=ARAD_0G5))
            {
            	if(tempB0 < (S8_TYPE_MAXNUM-L_TIME_70MS))
            	{
            		tempB0 = tempB0 + L_TIME_70MS;
            	}
            }
        }
        else
        {
            ;
        }

  #if (__IMP_4WD_1CYCLE_DRIVEN_1W_RISE_DELAY==1)                        /* 4WD driven wheel 1st cycle rise delay */
      #if ((__4WD==1)||(__4WD_VARIANT_CODE==ENABLE))
        if((WL->LFC_zyklus_z<=0)&&(WL->CAS_SUSPECT_flag==1)&&(WL->peak_dec_max<-ARAD_6G0)&&(MAX_WHEEL_ACCEL<ARAD_3G0)&&(BEND_DETECT2==0)
          #if (__4WD_VARIANT_CODE==ENABLE)
          &&((lsu8DrvMode==DM_AUTO)||(lsu8DrvMode==DM_4H))
          #else
          #endif
        )
        {
          #if __REAR_D
            if(REAR_WHEEL_wl==1) 
            {
                if((LEFT_WHEEL_wl==1)&&(rel_lam_rl > rel_lam_rr)) {tempB1=1;}
                if((LEFT_WHEEL_wl==0)&&(rel_lam_rl < rel_lam_rr)) {tempB1=1;}
            }
          #else
            if(REAR_WHEEL_wl==0) 
            {
                if((LEFT_WHEEL_wl==1)&&(rel_lam_fl > rel_lam_fr)) {tempB1=1;}
                if((LEFT_WHEEL_wl==0)&&(rel_lam_fl < rel_lam_fr)) {tempB1=1;}
            }
          #endif
            if(tempB1==1) 
            {
                tempB4 = (int8_t)LCABS_s16Interpolation2P(vref, VREF_10_KPH, VREF_100_KPH, L_TIME_40MS, L_TIME_350MS);
                if(tempB0 < tempB4) 
                {
                    tempB0 = tempB4;
                    lcabss16temp1=130;
                }
            }
        }
      #endif
  #endif         
        
        if(tempB0 > 0)
        {
            if(WL->arad<0) {WL->neg_arad_counter = WL->neg_arad_counter + L_1SCAN;}
            else if((WL->arad>0) || (WL->s_diff>0)) {WL->neg_arad_counter=0;}
            else { }

            if(REAR_WHEEL_wl==0) 
            {
                tempB0 = (int8_t)(((int16_t)tempB0*F_RISE_DELAY_FOR_CASCADING)/10);
            }
            else 
            {
                tempB0 = (int8_t)(((int16_t)tempB0*R_RISE_DELAY_FOR_CASCADING)/10);
            }

            if((WL->neg_arad_counter<L_TIME_40MS) && (WL->LFC_STABIL_counter<=tempB0)) 
            {
                WL->Pres_Rise_Defer_flag=1;
                WL->flags = (uint8_t)lcabss16temp1;
            }
            else {WL->Pres_Rise_Defer_flag=0;}

          #if __STABLE_DUMPHOLD_ENABLE
            if(WL->lcabsu1StabDumpHoldCycle==1)
            {
                WL->Pres_Rise_Defer_flag=0;
            }
          #endif    
        }
        else {
            WL->Pres_Rise_Defer_flag=0;
            WL->neg_arad_counter=0;
        }
    }
    else {
        WL->Pres_Rise_Defer_flag=0;
        WL->neg_arad_counter=0;
    }
}
#else
void LCABS_vExtricateFromCASCADING(void)
{
    int8_t    MAX_WHEEL_ACCEL;

    if(WL->peak_acc_alt > WL->max_arad_instbl) {MAX_WHEEL_ACCEL = WL->peak_acc_alt;}
    else {MAX_WHEEL_ACCEL = WL->max_arad_instbl;}

  #if   __REAR_D
    tempW5 = -(int16_t)U8_PULSE_LOW_MU;
  #else
    tempW5 = -(int16_t)U8_PULSE_LOW_MED_MU;
  #endif

    if(WL->s0_reapply_counter==0) {

        tempB7=tempB6=tempB5=0;

        if((AFZ_OK==1) && (afz>=tempW5) && (vref>=VREF_15_KPH)) {
            tempB7 = (int8_t)LCABS_s16CheckFastestWheel();
/*
            if(REAR_WHEEL_wl==1) {
                if(LEFT_WHEEL_wl==1) {
                    if((vrad_rl>vrad_rr) && (vrad_rl>vrad_fl) && (vrad_rl>vrad_fr))  {
                        tempB7=1;
                    }
                }
                else {
                    if((vrad_rr>vrad_rl) && (vrad_rr>vrad_fl) && (vrad_rr>vrad_fr))  {
                        tempB7=1;
                    }
                }
            }
            else {
                if(LEFT_WHEEL_wl==1) {
                    if((vrad_fl>vrad_rr) && (vrad_fl>vrad_rl) && (vrad_fl>vrad_fr))  {
                        tempB7=1;
                    }
                }
                else {
                    if((vrad_fr>vrad_rl) && (vrad_fr>vrad_fl) && (vrad_fr>vrad_rr))  {
                        tempB7=1;
                    }
                }
            }
*/
        }

    #if __TEMP_for_CAS
        tempB5 = (int8_t)NUM_OF_STABIL_WHEELS;
    #else
      #if   __VDC
        if((YAW_CDC_WORK==1) || (ESP_PULSE_DUMP_FLAG==1)) {
            tempB5 = 0;
/*
            if((SLIP_CONTROL_NEED_FL==0) && (SLIP_CONTROL_NEED_FL_old==0) && (STABIL_fl==1)) {tempB5 += 1;}
            if((SLIP_CONTROL_NEED_FR==0) && (SLIP_CONTROL_NEED_FR_old==0) && (STABIL_fr==1)) {tempB5 += 1;}
            if((SLIP_CONTROL_NEED_RL==0) && (SLIP_CONTROL_NEED_RL_old==0) && (STABIL_rl==1)) {tempB5 += 1;}
            if((SLIP_CONTROL_NEED_RR==0) && (SLIP_CONTROL_NEED_RR_old==0) && (STABIL_rr==1)) {tempB5 += 1;}
*/
            if((ESP_BRAKE_CONTROL_FL==0) && (state_fl==DETECTION)) {tempB5 += 1;}
            if((ESP_BRAKE_CONTROL_FR==0) && (state_fr==DETECTION)) {tempB5 += 1;}
            if((ESP_BRAKE_CONTROL_RL==0) && (state_rl==DETECTION)) {tempB5 += 1;}
            if((ESP_BRAKE_CONTROL_RR==0) && (state_rr==DETECTION)) {tempB5 += 1;}
        }
        else
        {
            tempB5 = 0;
            if(state_fl==DETECTION) {tempB5 += 1;}
            if(state_fr==DETECTION) {tempB5 += 1;}
            if(state_rl==DETECTION) {tempB5 += 1;}
            if(state_rr==DETECTION) {tempB5 += 1;}
        }
      #else
        tempB5 = 0;
        if(state_fl==DETECTION) {tempB5 += 1;}
        if(state_fr==DETECTION) {tempB5 += 1;}
        if(state_rl==DETECTION) {tempB5 += 1;}
        if(state_rr==DETECTION) {tempB5 += 1;}
      #endif
    #endif

          #if   __REAR_D
        if(MAX_WHEEL_ACCEL>=ARAD_5G0)
          #else
        if(MAX_WHEEL_ACCEL>=ARAD_6G0)
          #endif
        {
            if(((vref>=VREF_15_KPH) && (WL->arad_gauge>0)) || (vref<VREF_15_KPH))
            {
                tempB7=0;
                if((tempB5<=1) && (MAX_WHEEL_ACCEL>=ARAD_7G0)) {tempB5=4;}
                else { }
            }
            else { }
        }

        if(
          #if   __REAR_D
            ((REAR_WHEEL_wl==0) && ((WL->LFC_zyklus_z<=0)||(tempB5<=1)||(tempB7==1)))
          #else
            ((REAR_WHEEL_wl==1) && ((WL->LFC_zyklus_z<=0)||(tempB5<=1)||(tempB7==1)))
          #endif
          #if   __TEMP_for_CAS2
            || ((WL->CAS_SUSPECT_flag==1) && (MAX_WHEEL_ACCEL<ARAD_3G0))
          #endif
            || (MAX_WHEEL_ACCEL<=ARAD_0G5)
        ){

            if(WL->arad<0) {WL->neg_arad_counter=WL->neg_arad_counter + L_1SCAN;}
            else if((WL->arad>0) || (WL->s_diff>0)) {WL->neg_arad_counter=0;}
            else { }

            if(WL->LFC_zyklus_z<=0) {
                if(vref>=VREF_50_KPH) {tempB0 = 20;}
                else {tempB0 = 5;}
                if(MAX_WHEEL_ACCEL>=ARAD_10G0) {tempB0 = 3;}
                else if(MAX_WHEEL_ACCEL>=ARAD_8G0) {tempB0 = 5;}
                else { }
            }
          #if   __TEMP_for_CAS2
            else if(WL->CAS_SUSPECT_flag==1) {
                if((AFZ_OK==1) && (afz>-(int16_t)U8_PULSE_HIGH_MU)) {
                  #if __REDUCE_REAR_SLIP_AT_LMU
                    if(vref>=VREF_80_KPH) {tempB0 = 40;}
                    else
                  #endif /*__REDUCE_REAR_SLIP_AT_LMU*/
                    if(vref>=VREF_50_KPH) {tempB0 = 30;}
                    else if(vref>=VREF_20_KPH) {tempB0 = 15;}
                    else {tempB0 = 0;}
                }
                if((tempB0>0) && (MAX_WHEEL_ACCEL<=ARAD_0G5)) {
                    tempB0 = tempB0 + 10;
                }
            }
          #endif
            else if(tempB7==1) { /* 네바퀴중 가장 빠른 바퀴 */
                tempB0 = 10;
                if(MAX_WHEEL_ACCEL<=ARAD_0G5) {tempB0 = 20;}
            }
            else if(tempB5<=1) { /* 유일한 stable 바퀴 */
                tempB0 = 10;
                if((AFZ_OK==1) && (afz>=tempW5)) {tempB6 = 1;}
                else {
                    if(MAX_WHEEL_ACCEL<=ARAD_0G5) {tempB0 = 20;}
                }
            }
            else { /* max_arad_instbl <= 0.5g */
                if((LOW_to_HIGH_suspect==0) && (BEND_DETECT2==0)) {tempB0 = 10;}
            }

            if(tempB6==0) {
                if((WL->neg_arad_counter<L_TIME_30MS) && (WL->LFC_STABIL_counter<=tempB0)) {WL->Pres_Rise_Defer_flag=1;}
                else {WL->Pres_Rise_Defer_flag=0;}
            }
            else {WL->Pres_Rise_Defer_flag=1;}

          #if __IMPROVE_DECEL_AT_CERTAIN_HMU
            if((lcabsu1CertainHMu==1)&&(REAR_WHEEL_wl==0)&&(WL->Reapply_Accel_cycle==1))
            {
                WL->Pres_Rise_Defer_flag=0;
            }
            else
          #endif
          #if __STABLE_DUMPHOLD_ENABLE
            if(WL->lcabsu1StabDumpHoldCycle==1)
            {
                WL->Pres_Rise_Defer_flag=0;
            }
          #endif

            if(WL->Pres_Rise_Defer_flag==1)
            {
                if(WL->LFC_zyklus_z<=0) {WL->flags=120;}
              #if   __TEMP_for_CAS2
                else if(WL->CAS_SUSPECT_flag==1) {WL->flags=121;}
              #endif
                else if(tempB7==1) {WL->flags=122;}
                else if(tempB5<=1) {WL->flags=123;}
                else {WL->flags=124;}
            }
        }
        else {
            WL->Pres_Rise_Defer_flag=0;
            WL->neg_arad_counter=0;
        }
    }
    else {
        WL->Pres_Rise_Defer_flag=0;
        WL->neg_arad_counter=0;
    }
}
#endif

int16_t LCABS_s16CheckFastestWheel(void)
{
    lcabss16temp0 = 0;
    if(REAR_WHEEL_wl==1) {
        if(LEFT_WHEEL_wl==1) {
            if((vrad_rl>vrad_rr) && (vrad_rl>vrad_fl) && (vrad_rl>vrad_fr))  {
                lcabss16temp0=1;
            }
        }
        else {
            if((vrad_rr>vrad_rl) && (vrad_rr>vrad_fl) && (vrad_rr>vrad_fr))  {
                lcabss16temp0=1;
            }
        }
    }
    else {
        if(LEFT_WHEEL_wl==1) {
            if((vrad_fl>vrad_rr) && (vrad_fl>vrad_rl) && (vrad_fl>vrad_fr))  {
                lcabss16temp0=1;
            }
        }
        else {
            if((vrad_fr>vrad_rl) && (vrad_fr>vrad_fl) && (vrad_fr>vrad_rr))  {
                lcabss16temp0=1;
            }
        }
    }
    return lcabss16temp0;
}

void LCABS_vCheckABSForcedEnter(void)
{
    if(REAR_WHEEL_wl==0) {
        if((ABS_wl==0)&&(GMA_PULSE==0)&&(BEND_DETECT2==0)) {
            if((FL.Check_double_brake_flag2==1)&&(FR.Check_double_brake_flag2==1)) {
                if(WL->s0_reapply_counter>=4) {
                    WL->LFC_ABS_Forced_Enter_flag=1;
                }
                else {WL->LFC_ABS_Forced_Enter_flag=0;}
            }
            else {
              #if __VDC
                if((ACTIVE_BRAKE_FLG==1) && (vref < VREF_35_KPH))
                {
                    WL->LFC_ABS_Forced_Enter_flag=0;
                      /* to generate wheel pressure at HDC||ACC||EPB control */
                }
                else
              #endif
                if((WL->s0_reapply_counter>=14)
                  #if (__BUMP_COMP_IMPROVE==ENABLE)
                &&(PossibleABSForBumpVehicle==0)
                  #endif/*#if (__BUMP_COMP_IMPROVE==ENABLE)*/
                )
                {
                    WL->LFC_ABS_Forced_Enter_flag=1;
                }
                else {WL->LFC_ABS_Forced_Enter_flag=0;}
            }
        }
      #if   __VDC
        else if(l_SLIP_A_wl==1) {WL->LFC_ABS_Forced_Enter_flag=1;}
      #endif
        else {WL->LFC_ABS_Forced_Enter_flag=0;}
    }
    else {
        if(ABS_wl==0) {
            if((((ABS_fl==1)&&(ABS_fr==1)) || (LFC_Split_flag==1) || (LFC_Split_suspect_flag==1))
              #if (__BUMP_COMP_IMPROVE==ENABLE)
            &&(PossibleABSForBumpVehicle==0)
              #endif/*#if (__BUMP_COMP_IMPROVE==ENABLE)*/
            )
            {  /* Changed by KGY 030918 due to CIRCUIT_FAIL */
                if((LEFT_WHEEL_wl==1) && ((ABS_rr==1) || (RR.state==UNSTABLE))) {
                    WL->LFC_ABS_Forced_Enter_flag=1;
                }
                else if((LEFT_WHEEL_wl==0) && ((ABS_rl==1) || (RL.state==UNSTABLE))) {
                    WL->LFC_ABS_Forced_Enter_flag=1;
                }
                else {WL->LFC_ABS_Forced_Enter_flag=0;}
            }
            else {WL->LFC_ABS_Forced_Enter_flag=0;}
        }
        else {WL->LFC_ABS_Forced_Enter_flag=0;}
    }
}

void LCABS_vWPresModeFULLREAPPLY(void)
{
    AV_REAPPLY_wl = 1;
    AV_DUMP_wl = 0;
    AV_HOLD_wl = 0;
    BUILT_wl = 0;
    WL->Hold_Duty_Comp_flag=0;      /* '03 USA High-mu Test */
    WL->Hold_Duty_Comp_flag_1st=0;
    WL->Forced_Hold_counter=0;
    WL->Forced_Hold_cnt=0;

    if(WL->s0_reapply_counter==0)
    {
    	LCABS_vUpdVarsAftDump();
    }
    else {}

	#if __CHANGED_TO_DECIDE_ONOFF == ENABLE
		WL->s0_reapply_counter = 0;
	#else
	    if((ABS_wl==0) && (WL->LFC_Conven_OnOff_Flag==0)) {WL->s0_reapply_counter = 0;}
	    else {WL->s0_reapply_counter++;}
	#endif


    if((ABS_wl==0) && (FSF_wl==1) && (WL->LFC_ABS_Forced_Enter_flag==1)) {
        WL->LFC_built_reapply_flag = 1;
        WL->LFC_s0_reapply_counter = 1;
        WL->LFC_built_reapply_counter = L_1ST_SCAN;
	  #if (__IDB_LOGIC==ENABLE)
		#if __CHANGED_TO_DECIDE_ONOFF == ENABLE
		#else
	    WL->LFC_Conven_OnOff_Flag=1;
		#endif
	  #else
        WL->LFC_Conven_OnOff_Flag = 0;
	  #endif
    }


    if(ABS_fz==1) { /* '03 NZ Winter - ABS Warning */
	  #if (__IDB_LOGIC==ENABLE)
	    if(ABS_wl==1) 
	    {
	    	BUILT_wl = 1;/*in order to check as the bulit mode*/
		    WL->LFC_built_reapply_flag = 1;/*Because of no MP/dP duty compensation*/
		    if(WL->LFC_s0_reapply_counter<=200) {WL->LFC_s0_reapply_counter++;}
		    WL->LFC_built_reapply_counter = WL->LFC_built_reapply_counter + L_1SCAN;
		    WL->LFC_three_built_check_flag=0;
		    WL->SL_Dump_Start=0;
		  #if (__IDB_LOGIC==ENABLE)
			#if __CHANGED_TO_DECIDE_ONOFF == ENABLE
			#else
		    WL->LFC_Conven_OnOff_Flag=1;
		  #endif
		  #endif
	    }
	    else
	    {
        if(WL->LFC_three_built_check_flag==0) {WL->LFC_three_built_check_flag=1;}
        else {
            WL->LFC_three_built_check_flag=0;
            if((FSF_wl==0)&&(WL->LFC_s0_reapply_counter>=1)) {
                if(WL->LFC_s0_reapply_counter<=200) {WL->LFC_s0_reapply_counter++;}
            }
        }
    }
	  #else        
        if(WL->LFC_three_built_check_flag==0) {WL->LFC_three_built_check_flag=1;}
        else {
            WL->LFC_three_built_check_flag=0;
            if((FSF_wl==0)&&(WL->LFC_s0_reapply_counter>=1)) {
                if(WL->LFC_s0_reapply_counter<=200) {WL->LFC_s0_reapply_counter++;}
            }
        }
      #endif/*(__IDB_LOGIC==ENABLE)*/
    }
    else {WL->LFC_three_built_check_flag=0;}

	 #if (M_DP_BASE_BUILT_RISE==ENABLE)
    WL->Reapply_Accel_flag = 0;
    WL->Unstable_reapply_counter = 0;
	 #endif

}

void LCABS_vWPresModeBUILTREAPPLY(void)
{
    AV_REAPPLY_wl = 1;
    AV_DUMP_wl = 0;
    AV_HOLD_wl = 0;
    BUILT_wl = 1;

    if((WL->WheelCtrlMode != WHEEL_CTRL_CBS) && (WL->WheelCtrlMode != WHEEL_CTRL_NON))
    {
		if(WL->s0_reapply_counter==0)
		{
			LCABS_vUpdVarsAftDump();
		}
		else {}
    }
    else {}

    WL->Hold_Duty_Comp_flag=0;      /* '03 USA High-mu Test */
    WL->s0_reapply_counter++;
    WL->hold_timer_new = 0;
    WL->Forced_Hold_counter=0;
    WL->Forced_Hold_cnt=0;
/*    WL->p_hold_time = 0; */

    WL->LFC_built_reapply_flag = 1;
    if(WL->LFC_s0_reapply_counter<=200) {WL->LFC_s0_reapply_counter++;}
    WL->LFC_built_reapply_counter = WL->LFC_built_reapply_counter + L_1SCAN;
    WL->LFC_three_built_check_flag=0;
    WL->SL_Dump_Start=0;
    
   #if (M_DP_BASE_BUILT_RISE == ENABLE)
    WL->lcabsu1UpdDeltaPRiseAftHld=0;
    WL->lcabsu1WhlPForcedHoldCmd=0;
    WL->Reapply_Accel_flag = 0;
    WL->Unstable_reapply_counter = 0;
   #endif /*(M_DP_BASE_BUILT_RISE == ENABLE)*/

    WL->lcabsu1ForcedHldForStrkRecvr = 0; /*for monitoring*/
}

void LCABS_vWPresModeSTABLEHOLD(void)
{
/*    if(WL->rel_lam < (int8_t)(-LAM_10P)) {WL->p_hold_time++;} */
    AV_HOLD_wl = 1;
    AV_REAPPLY_wl = 0;
    AV_DUMP_wl = 0;
    BUILT_wl = 0;

    if(WL->LFC_built_reapply_flag == 1) {
        if(WL->Hold_Duty_Comp_flag == 0) {
            if(REAR_WHEEL_wl==0) {WL->LFC_built_reapply_counter = WL->LFC_built_reapply_counter + L_1SCAN;}
            else if((WL->Duty_Limitation_flag==0) || (WL->lcabsu1TarDeltaPLimitFlg==0) || (WL->LFC_built_reapply_counter<=L_1ST_SCAN)) {WL->LFC_built_reapply_counter = WL->LFC_built_reapply_counter + L_1SCAN;}
            else { }
        }
        else if(WL->LFC_built_reapply_counter <= L_1ST_SCAN) {WL->LFC_built_reapply_counter = WL->LFC_built_reapply_counter + L_1SCAN;}
        else { }
    }
    WL->LFC_three_built_check_flag=0;
    WL->SL_Dump_Start=0;

    if((GMA_SLIP_wl==1) && (WL->gma_dump!=0))
    {
        WL->gma_hold_counter = WL->gma_hold_counter + L_1SCAN;
    }
    else
    {
        WL->gma_hold_counter = 0;
    }

  #if __STABLE_DUMPHOLD_ENABLE
    if(WL->lcabsu1StabDumpAndHoldFlg==1)
    {
        WL->lcabsu1HoldAfterStabDumpFlg = 1;
        WL->lcabsu8HoldAfterStabDumpCnt = WL->lcabsu8HoldAfterStabDumpCnt + L_1SCAN;
    }
  #endif

  #if (M_DP_BASE_BUILT_RISE==ENABLE)
    if(WL->lcabsu1WhlPForcedHoldCmd==1)
    {
        LCABS_vDecideVarsAtRiseHld(S16_HOLD_MODE_FORCED);
    }
    else
    {
        LCABS_vDecideVarsAtRiseHld(S16_HOLD_MODE_PATTERN);
    }

    WL->Reapply_Accel_flag = 0;
    WL->Unstable_reapply_counter = 0;
  #endif

}

void LCABS_vWPresModeStartDUMP(void)
{
    AV_DUMP_wl = 1;
    AV_REAPPLY_wl = 0;
    AV_HOLD_wl = 0;
    BUILT_wl = 0;

    ARAD_AT_MINUS_B_wl=0;
    RESET_PEAKCEL_wl = 0;

    SPOLD_RESET_wl= 0;
    WL->Hold_Duty_Comp_flag=0;      /* '03 USA High-mu Test */
    WL->Hold_Duty_Comp_flag_1st=0;
    WL->Forced_Hold_counter=0;
    WL->Forced_Hold_cnt=0;
    WL->slip_at_real_dump=WL->rel_lam;
/*    WL->arad_at_dump=WL->arad; */
    WL->hold_timer_new = 0;
    WL->spold = 0;
    WL->gma_hold_counter = 0;
/*    WL->p_hold_time=0; */
    WL->spold_rst_count=0;
    WL->Front_SL_dump_flag_K=0;

  #if (L_CONTROL_PERIOD>=2)
    WL->Dump_hold_scan_counter = L_CONTROL_PERIOD - LCABS_u8CountDumpNum(0, WL->lcabss8DumpCase); 
                                 /* DUMP_CASE_11 --> hold_counter=0; DUMP_CASE 10 or 01 -> hold_counter=1; */
  #else
    WL->Dump_hold_scan_counter = 0;
  #endif
    WL->state = UNSTABLE;
}

void LCABS_vWPresModeGMADUMP(void)
{
    AV_DUMP_wl = 1;
    AV_REAPPLY_wl = 0;
    AV_HOLD_wl = 0;
    BUILT_wl = 0;
    WL->hold_timer_new = 0;
    WL->gma_hold_counter = 0;

  #if __STABLE_DUMPHOLD_ENABLE
    if(WL->lcabsu1StabDump==1)
    {
        WL->lcabsu1StabDumpAndHoldFlg = 1;
        WL->lcabsu8HoldAfterStabDumpCnt = 0;
        WL->lcabsu8StableDumpCnt = WL->lcabsu8StableDumpCnt + 1;
        WL->lcabsu1StabDump=0;
	  #if __UNSTABLE_REAPPLY_ENABLE
	    WL->Reapply_Accel_flag=0;
	    WL->lcabss8URScansBfUnstDump=WL->Unstable_reapply_counter;
	    WL->Unstable_reapply_counter=0;
	    WL->Unst_Reapply_Scan_Ref=0;
	    WL->Reapply_Accel_hold_timer=0;
	    WL->Forced_Hold_After_Unst_Reapply=0;
	  #endif
    }
  #endif

    WL->Forced_Hold_After_Unst_Reapply=0;
}

#if __STABLE_DUMPHOLD_ENABLE
static void LCABS_vCheckStbDumpHoldMode(void)
{
	int16_t	s16FrontHighSideStbDmpThYDF;
    uint8_t u8OutABSYMRDumpScans = (uint8_t)LCABS_s16Interpolation2P(vref, S16_OUTABS_YMR_DUMP_ALLOW_SPEED, S16_OUTABS_YMR_DUMP_HIGH_SPEED, 
                                                                   		   S16_OUTABS_YMR_DUMP_MIN,         S16_OUTABS_YMR_DUMP_MAX);
    
    if(WL->lcabsu1StabDumpAndHoldFlg == 0)
    {
        /* Stable Dump Mode Enter Conditions */
       #if __SPLIT_STABILITY_IMPROVE_GMA
          #if !__YAW_STABILITY_IMPROVE
        if(REAR_WHEEL_wl==0) 
          #else
            #if __NON_SPLIT_STABILITY_IMPROVE
        if(((REAR_WHEEL_wl==0)&&((lcabsu1UnderOutsideFrontCtrl==1)||(lcabsu1OverInsideFrontCtrl==1))) || ((REAR_WHEEL_wl==1) && (lcabsu1OverInsideRearCtrl==1)))
            #else
        if((REAR_WHEEL_wl==0)&&((lcabsu1UnderOutsideFrontCtrl==1)||(lcabsu1OverInsideFrontCtrl==1)))
            #endif
          #endif
        {
          #if __UNDERSTEER_FRT_STB_DUMP_BFABS
            if((REAR_WHEEL_wl==0) && (ABS_wl==0) && (lcabsu1UnderOutsideFrontCtrl==1))
            {
                if(WL->lcabss16YawDumpFactor>1000)
                {
                    WL->lcabsu1StabDump=1;
                    WL->lcabsu1StabDumpAndHoldFlg = 1;
                }
                else { }
            }
            else
          #endif            
          #if __OVERSTEER_REAR_STB_DUMP_BFABS
            if((REAR_WHEEL_wl==1) && (ABS_wl==0) && (lcabsu1OverInsideRearCtrl==1))
            {
                if(WL->lcabss16YawDumpFactor>700)
                {
                    WL->lcabsu1StabDump=1;
                    WL->lcabsu1StabDumpAndHoldFlg = 1;
                }
                else { }
            }
            else
          #endif            
            if((REAR_WHEEL_wl==0)&&(lcabsu1OverInsideFrontCtrl==1)&&(lcabsu1OverInsideRearCtrl==0))/*split high side*/
            {
	        	if(vref>=S16_GMA_H_SPEED)
	        	{
	        		s16FrontHighSideStbDmpThYDF = S16_SP_HI_SIDE_STB_DMP_YDF_TH_H;
	        	}
	        	else if(vref>=S16_GMA_M_SPEED)
	        	{
	        		s16FrontHighSideStbDmpThYDF = S16_SP_HI_SIDE_STB_DMP_YDF_TH_M;
	        	}
	        	else if(vref>=S16_GMA_L_SPEED)
	        	{
	        		s16FrontHighSideStbDmpThYDF = S16_SP_HI_SIDE_STB_DMP_YDF_TH_L;
	        	}
	        	else
	        	{
	        		s16FrontHighSideStbDmpThYDF = S16_SP_HI_SIDE_STB_DMP_YDF_TH_BL;
	        	}
	        	
                if((WL->LFC_s0_reapply_counter>=5)&&(WL->lcabss16YawDumpFactor>=s16FrontHighSideStbDmpThYDF))
                {
                    WL->lcabsu1StabDump=1;
                    WL->lcabsu1StabDumpAndHoldFlg = 1;
                }
                else if((WL->lcabss16YawDumpFactor>=s16FrontHighSideStbDmpThYDF) && ((WL->LFC_s0_reapply_counter>=3)
                	    #if (__PRESS_EST == ENABLE)&& (__MPLESS_CONTROL_TYPE == DISABLE)
                	  ||(WL->s16_Estimated_Active_Press>=S16_HI_SIDE_HOLD_WH_PRESS)
                	    #endif
                	   ))
                {
                	WL->lcabsu1StabDumpAndHoldFlg = 1;
                }
                else {}	        	
	        }
	        else if(WL->LFC_s0_reapply_counter<=0)
            {
                if(WL->lcabss16YawDumpFactor>700)
                {
                    WL->lcabsu1StabDump=1;
                    WL->lcabsu1StabDumpAndHoldFlg = 1;
                }
                else if(WL->lcabss16YawDumpFactor>300)
                {
                    WL->lcabsu1StabDumpAndHoldFlg = 1;
                }
                else { }
            }
            else if(WL->LFC_s0_reapply_counter>=5)
            {
                if(WL->lcabss16YawDumpFactor>300)
                {
                    WL->lcabsu1StabDump=1;
                    WL->lcabsu1StabDumpAndHoldFlg = 1;
                }
            }
            else if(WL->LFC_s0_reapply_counter>=3)
            {
                if(WL->lcabss16YawDumpFactor>500)
                {
                    WL->lcabsu1StabDump=1;
                    WL->lcabsu1StabDumpAndHoldFlg = 1;
                }
            }
            else { }
        }
       #endif /* __SPLIT_STABILITY_IMPROVE_GMA */

      #if __OUT_ABS_P_RATE_COMPENSATION && __OUT_ABS_STABLE_DUMP_ENABLE
       #if __ABS_RBC_COOPERATION
        if(lcabsu1FastPressRecoveryBfAbs==0)
       #endif /* __ABS_RBC_COOPERATION */
        {
            if((WL->lcabsu1StabDump==0)&&(WL->lcabsu1StabDumpHoldCycle==0)&&(WL->OutABSYMRCtrlMode==LEVEL2))
            {
                WL->OutABSYMRCtrlMode=LEVEL2_DUMP;
                WL->lcabsu1StabDump=1;
                WL->lcabsu1StabDumpAndHoldFlg = 1;
            }
        }
      #endif /*#if __OUT_ABS_P_RATE_COMPENSATION && __OUT_ABS_STABLE_DUMP_ENABLE*/
        
        if((Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1))
        {
            WL->lcabsu1StabDump = 0;
            WL->lcabsu1StabDumpAndHoldFlg = 0;
        }
        else if(vref<=VREF_15_KPH)                     /*Speed limit*/
        {
            WL->lcabsu1StabDump = 0;
            WL->lcabsu1StabDumpAndHoldFlg = 0;
        }
        else { }
        	
        if(WL->lcabsu1StabDumpAndHoldFlg == 1)
        {
            WL->lcabsu8StableDumpCnt = 0;
        }
    }
    else
    {
      #if __OUT_ABS_P_RATE_COMPENSATION && __OUT_ABS_STABLE_DUMP_ENABLE
        if(WL->OutABSYMRCtrlMode==LEVEL2_DUMP)
        {
            if(WL->lcabsu8StableDumpCnt>=u8OutABSYMRDumpScans) /*1 -> later to be tunable dep. on vref and slip diff*/
            {
                WL->lcabsu1StabDumpAndHoldFlg = 0;
            }
            else
            {
                WL->lcabsu1StabDump = 1;
            }
        }
        else
      #endif /*#if __OUT_ABS_P_RATE_COMPENSATION && __OUT_ABS_STABLE_DUMP_ENABLE*/
        {
        /* Stable Dump Mode Inhibit/Exit Conditions */
       #if __SPLIT_STABILITY_IMPROVE_GMA
        if((LFC_Split_flag==1) || (LFC_Split_suspect_flag==1))
        {
            tempW1 = 200;
        }
        else if(AFZ_OK==0)
        {
            tempW1 = 200;
        }
        else
        {
            tempW1 = -((afz>0)?0:((afz<-120)?-120:afz)); /* if(afz<0) {tempB1 = |afz|} */
        }
            
        if((lcabsu1OverInsideFrontCtrl==1) && (lcabsu1OverInsideRearCtrl==1))
        {
            if(REAR_WHEEL_wl==0)
            {
                tempB7 = (int8_t)LCABS_s8GetParamFromRef((uint8_t)tempW1, U8_LOW_MU, U8_HIGH_MU, 120, U8_STB_DMP_MAX_LOW_BothOverInF, U8_STB_DMP_MAX_MED_BothOverInF, U8_STB_DMP_MAX_HIGH_BothOverInF, U8_STB_DMP_MAX_SP_BothOverInF);
            }
            else
            {
                tempB7 = (int8_t)LCABS_s8GetParamFromRef((uint8_t)tempW1, U8_LOW_MU, U8_HIGH_MU, 120, U8_STB_DMP_MAX_LOW_BothOverInR, U8_STB_DMP_MAX_MED_BothOverInR, U8_STB_DMP_MAX_HIGH_BothOverInR, U8_STB_DMP_MAX_SP_BothOverInR);
            }
        }
        else if((lcabsu1OverInsideFrontCtrl==1) && (REAR_WHEEL_wl==0))
        {
            tempB7 = (int8_t)LCABS_s8GetParamFromRef((uint8_t)tempW1, U8_LOW_MU, U8_HIGH_MU, 120, U8_STB_DMP_MAX_LOW_OverInF, U8_STB_DMP_MAX_MED_OverInF, U8_STB_DMP_MAX_HIGH_OverInF, U8_STB_DMP_MAX_SP_OverInF);
        }
        else if((lcabsu1OverInsideRearCtrl==1) && (REAR_WHEEL_wl==1))
        {
            tempB7 = (int8_t)LCABS_s8GetParamFromRef((uint8_t)tempW1, U8_LOW_MU, U8_HIGH_MU, 120, U8_STB_DMP_MAX_LOW_OverInR, U8_STB_DMP_MAX_MED_OverInR, U8_STB_DMP_MAX_HIGH_OverInR, U8_STB_DMP_MAX_SP_OverInR);
        }
        else if((lcabsu1UnderOutsideFrontCtrl==1) && (REAR_WHEEL_wl==0))
        {
          #if __UNDERSTEER_FRT_STB_DUMP_BFABS   
            if(ABS_wl==0)
            {
                tempB7 = S8_STABLE_DUMP_OUTSIDE_ABS_MAX;
            }               
            else               
          #endif    
            {
                tempB7 = (int8_t)LCABS_s8GetParamFromRef((uint8_t)tempW1, U8_LOW_MU, U8_HIGH_MU, 120, U8_STB_DMP_MAX_LOW_UnderOutF, U8_STB_DMP_MAX_MED_UnderOutF, U8_STB_DMP_MAX_HIGH_UnderOutF, U8_STB_DMP_MAX_SP_UnderOutF);
            }
        }
        else
	    {
            tempB7 = 0;
		}
		tempW6 = LCABS_s16Interpolation2P(vref, VREF_100_KPH, VREF_10_KPH, 20, 5);        
        tempB7 = (int8_t)(((int32_t)tempW6*tempB7)/10);            
       
        tempW7 = LCABS_s16Interpolation2P((int16_t)WL->lcabsu8StableDumpCnt, STB_DUMP_LEVEL_LOW, STB_DUMP_LEVEL_MED_HIGH, 300, 1500);
                
        tempW6 = LCABS_s16Interpolation2P((int16_t)WL->lcabsu8StableDumpCnt, STB_DUMP_LEVEL_LOW, STB_DUMP_LEVEL_MED_HIGH, 2, 4);
        tempW5 = ((int16_t)WL->lcabsu8YawRiseHoldTime/tempW6);
        
        tempW6 = (int16_t)LCABS_s8GetParamFromRef(WL->lcabsu8StableDumpCnt, STB_DUMP_LEVEL_LOW, STB_DUMP_LEVEL_MED, STB_DUMP_LEVEL_MED_HIGH, 1,2,3,4);        
        tempW8 = (int16_t)((int32_t)WL->lcabsu8YawDumpHoldTime*tempW6);
        if(tempW8 > L_TIME_700MS) {tempW8 = L_TIME_700MS;}
        
        if(YAW_CDC_WORK==1) 
        {
            tempW7 = (int16_t)(((int32_t)tempW7*3)/2);
            tempW8 = (int16_t)(((int32_t)tempW8*3)/2);
            tempB7 = (int8_t)(((int16_t)tempB7*2)/3);
        }
        

        if((WL->lcabss16YawDumpFactor>tempW7) && (WL->lcabsu8HoldAfterStabDumpCnt>(uint8_t)tempW8))
        {
            WL->lcabsu1StabDump=1;
            
            if(WL->lcabsu8StableDumpCnt>=(uint8_t)tempB7)
            {
                WL->lcabsu1StabDump=0;
                WL->lcabsu1StabDumpAndHoldFlg = 0;
            }
        }
        else if(WL->lcabsu8HoldAfterStabDumpCnt>=(uint8_t)tempW5)
        {
            WL->lcabsu1StabDumpAndHoldFlg = 0;
        }
        else { }
        
       #endif
    }

        if((Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1))
        {
            WL->lcabsu1StabDumpAndHoldFlg = 0;
        }
        else if(WL->LFC_built_reapply_counter==L_1ST_SCAN)
        {
            WL->lcabsu1StabDumpAndHoldFlg = 0;
        }
        else if(WL->lcabsu8HoldAfterStabDumpCnt > L_TIME_300MS) /*Max*/
        {
            WL->lcabsu1StabDumpAndHoldFlg = 0;
        }     
        else if(vref<=VREF_15_KPH)                    /*Speed limit*/
        {
            WL->lcabsu1StabDumpAndHoldFlg = 0;
        }     
        else { }
        
        if(WL->lcabsu1StabDumpAndHoldFlg == 0)
        {
            WL->lcabsu1StabDumpHoldCycle = 1;
        }
    }
  
}
#endif

void LCABS_vUpdateLFCVarsAtDumpStart(void)
{
    if(WL->LFC_built_reapply_flag==1)
    {
        WL->lcabsu1SLDumpCycle=0;
    }

    if(REAR_WHEEL_wl==0) {          /* front wheel               */
      #if __FRONT_SL_DUMP
/*        if((STAB_A_wl==1)||(WL->Front_SL_dump_flag_K==1)) {          // The end of stabil(starting dump point) */
      #else
/*        if(STAB_A_wl==1) {          // The end of stabil(starting dump point) */
      #endif
            WL->LFC_unstable_hold_counter=0;
            WL->LFC_dump_counter_old=WL->LFC_dump_counter;
            WL->Mu_change_Dump_counter_old=WL->Mu_change_Dump_counter;
              #if (L_CONTROL_PERIOD>=2)
            WL->LFC_dump_counter = LCABS_u8CountDumpNum(0, WL->lcabss8DumpCase);
            WL->Mu_change_Dump_counter=LCABS_u8CountDumpNum(0, WL->lcabss8DumpCase);
              #else
            WL->LFC_dump_counter=1;
            WL->Mu_change_Dump_counter=1;
              #endif  

          #if __PRESS_EST && __CALC_EFFECTIVE_DUMPSCANS
            WL->lcabsu8EffectiveDumpScans=WL->LFC_dump_counter;
          #endif

            if(WL->LFC_s0_reapply_counter >=1) {
                WL->LFC_s0_reapply_counter_old2=WL->LFC_s0_reapply_counter_old;
                WL->LFC_s0_reapply_counter_old=WL->LFC_s0_reapply_counter;
                WL->LFC_built_reapply_counter_old = WL->LFC_built_reapply_counter;
                WL->hold_timer_new_ref_old = WL->hold_timer_new_ref2;
                if(WL->LFC_maintain_split_flag==1) {WL->LFC_maintain_split_flag=0;}
              #if __MP_COMP
                WL->Estimated_Press_SKID = WL->s16_Estimated_Active_Press;
                WL->lcabsu1CaptureEstPressForSkid = 1;
              #endif
            }
			  #if (M_DP_BASE_BUILT_RISE==ENABLE)
            else if(WL->s0_reapply_counter >=1)
            {
                WL->Estimated_Press_SKID = WL->s16_Estimated_Active_Press;
                WL->lcabsu1CaptureEstPressForSkid = 1;
            }
			  #endif
              #if __MP_COMP
            else if((ABS_wl==0) && (FSF_wl==1))
            {
            	WL->Estimated_Press_SKID = WL->s16_Estimated_Active_Press;
            	WL->lcabsu1CaptureEstPressForSkid = 1;
            }
            else { }
              #endif
/*        } */
        WL->LFC_s0_reapply_counter=0;           /* Built Reapply 소요 Scan Reset. */
        WL->LFC_built_reapply_counter=0;        /* Built Reapply 소요시간 Reset. */
        WL->LFC_built_reapply_flag=0;
    }
    else {                          /* rear wheel */
    /*  #if __ESP_ABS_COOPERATION_CONTROL   */
    /*  if((WL->LFC_dump_start_flag==1)||(WL->ESP_ABS_Rear_Forced_Dump_K==1)){ */
    /*  #else   */
    /*  if(WL->LFC_dump_start_flag==1) {        // Starting point of Dump */

    /*  #endif */
            WL->LFC_unstable_hold_counter=0;
            WL->LFC_dump_counter_old=WL->LFC_dump_counter;
              #if (L_CONTROL_PERIOD>=2)
            WL->LFC_dump_counter = LCABS_u8CountDumpNum(0, WL->lcabss8DumpCase);
              #else
            WL->LFC_dump_counter=1;
              #endif  
            
          #if __PRESS_EST && __CALC_EFFECTIVE_DUMPSCANS
            WL->lcabsu8EffectiveDumpScans=WL->LFC_dump_counter;
          #endif

            if(WL->LFC_s0_reapply_counter >=1) {
                WL->LFC_s0_reapply_counter_old2=WL->LFC_s0_reapply_counter_old;
                WL->LFC_s0_reapply_counter_old=WL->LFC_s0_reapply_counter;
                WL->LFC_built_reapply_counter_old = WL->LFC_built_reapply_counter;
                WL->hold_timer_new_ref_old = WL->hold_timer_new_ref2;
              #if __MP_COMP
                WL->Estimated_Press_SKID = WL->s16_Estimated_Active_Press;
                WL->lcabsu1CaptureEstPressForSkid = 1;
              #endif
            }
			  #if (M_DP_BASE_BUILT_RISE==ENABLE)
			else if(WL->s0_reapply_counter >=1)
			{
			  WL->Estimated_Press_SKID = WL->s16_Estimated_Active_Press;
			  WL->lcabsu1CaptureEstPressForSkid = 1;
			}
			  #endif
              #if __MP_COMP
            else if((ABS_wl==0) && (FSF_wl==1))
            {
            	if(WL->EBD_Dump_counter == 0)
            	{
            		WL->Estimated_Press_SKID = WL->s16_Estimated_Active_Press;
            		WL->lcabsu1CaptureEstPressForSkid = 1;
            	}
            }
            else { }
              #endif
/*        } */
        WL->LFC_s0_reapply_counter=0;           /* Built Reapply 소요 Scan Reset. */
        WL->LFC_built_reapply_counter=0;        /* Built Reapply 소요시간 Reset. */
      #if __CHANGE_MU
        WL->Duty_Limitation_flag=0;
        WL->lcabsu1TarDeltaPLimitFlg = 0;
      #endif
        WL->LFC_built_reapply_flag=0;
    }

  #if (__IDB_LOGIC==ENABLE)
	#if __CHANGED_TO_DECIDE_ONOFF == ENABLE
	#else
    	WL->LFC_Conven_OnOff_Flag=1;
	#endif
  #else
    WL->LFC_Conven_OnOff_Flag=0;        /* Added by KGY for EBD pulse-up time control(06.07.21) */
  #endif

   #if __UNSTABLE_REAPPLY_ENABLE
    WL->Reapply_Accel_flag=0;
    WL->Unstable_reapply_counter=0;
    WL->lcabss8URScansBfUnstDump=0;
    WL->Unst_Reapply_Scan_Ref=0;
    WL->Reapply_Accel_hold_timer=0;

    WL->Reapply_Accel_cycle_old=WL->Reapply_Accel_cycle;
    WL->Reapply_Accel_cycle=0;
    WL->Unstable_Rise_Set_Cycle=0;
    WL->SL_Unstable_Rise_Cycle=0;
    WL->Forced_Hold_After_Unst_Reapply=0;

   #if __DETECT_MU_FOR_UR
    WL->lcabsu1BumpOrWAForUR=0;
    WL->lcabsu1LMuForUR=0;
    WL->lcabsu1HMuForUR=0;
   #endif
  #endif

  #if     __DUMP_THRES_COMP_for_Spold_update
    WL->SPOLD_COMP_FLG=0;
    WL->SPOLD_COMP_FLG1=0;
    WL->spold_comp=0; /*050406 */
  #endif

  #if __STABLE_DUMPHOLD_ENABLE
    WL->lcabsu1StabDumpHoldCycle = 0;
    WL->lcabsu1HoldAfterStabDumpFlg = 0;
  #endif

  #if __MP_COMP
    WL->Estimated_Press_Lower = WL->Estimated_Press_SKID;
  #endif

  #if (M_DP_BASE_BUILT_RISE==ENABLE)
    WL->lcabss16DumpDeltaWhlP = MPRESS_0BAR;
    WL->lcabsu1WhlPForcedHoldCmd = 0;
    WL->lcabss16RiseDeltaWhlPOld = WL->lcabss16RiseDeltaWhlP;
    WL->lcabss16RiseDeltaWhlP = 0;
  #endif /*(M_DP_BASE_BUILT_RISE==ENABLE)*/
}

int8_t LCABS_s8CheckMSLCtrlMode(void)
{
    tempB7 = 0;
    WL->Front_SL_dump_flag_K=0;

  #if (__VDC && __MGH40_ESP_BEND_DETECT)
    if((vref >= (int16_t)VREF_6_KPH) && (BEND_DETECT_ESP==0))
  #else
    if(vref >= (int16_t)VREF_6_KPH)
  #endif
    {
        if(REAR_WHEEL_wl==0) {
            if((LFC_H_to_Split_flag==1) && (SPLIT_JUMP_wl==1) && (WL->gma_dump1>0)) {

                if(WL->gma_dump1>=GMA_dumptime_at_H_to_Split) {WL->Front_SL_dump_flag_K=1;}
                WL->gma_dump1 = LCABS_u8DecountStableDumpNum(WL->gma_dump1);
                if(WL->gma_dump1==0) {SPLIT_JUMP_wl=0;}
                tempB7=1;
            }
            else {
                tempB7=0;
                SPLIT_JUMP_wl=0;
            }
        }
        else {
            if((LFC_Split_flag==1) ||(LFC_Split_suspect_flag==1) || (WL->LFC_zyklus_z>=1)
              #if __SELECT_LOW_FOR_LMU
               || (lcabsu1SelectLowforNonSplit==1)
              #endif /*__SELECT_LOW_FOR_LMU*/
              ) 
            {
                if(WL->MSL_BASE==0) {
                    if((LEFT_WHEEL_wl==1) && ((ABS_rr==1) || (RR.state == UNSTABLE))
                      && ((RR.SPLIT_JUMP==1) || (RL.SPLIT_PULSE==1)
                  #if __SELECT_LOW_FOR_LMU
                       || (lcabsu1SelectLowforNonSplit==1)
                  #endif /*__SELECT_LOW_FOR_LMU*/
                       )) {
                        if(RR.state == UNSTABLE) {tempB7=2;}
                        else if((RL.state == UNSTABLE) && (LFC_Split_flag==0) && (LFC_Split_suspect_flag==0))
                        {
                            tempB7=0;
                            if(RL.SPLIT_PULSE==1) {RL.SPLIT_PULSE=0;}
                        }
                        else {tempB7=1;}
                    }
                    else if((LEFT_WHEEL_wl==0) && ((ABS_rl==1) || (RL.state == UNSTABLE))
                      && ((RL.SPLIT_JUMP==1) || (RR.SPLIT_PULSE==1)
                  #if __SELECT_LOW_FOR_LMU
                       || (lcabsu1SelectLowforNonSplit==1)
                  #endif /*__SELECT_LOW_FOR_LMU*/
                        )) {
                        if(RL.state == UNSTABLE) {tempB7=2;}
                        else if((RR.state == UNSTABLE) && (LFC_Split_flag==0) && (LFC_Split_suspect_flag==0))
                        {
                            tempB7=0;
                            if(RR.SPLIT_PULSE==1) {RR.SPLIT_PULSE=0;}
                        }
                        else {tempB7=1;}
                    }
                    else {tempB7=0;}
                }
                else {tempB7=0;}
            }
            else {tempB7=0;}

        }
    }

    return tempB7;
}

void LCABS_vCheckMSLSplitCtrl(struct W_STRUCT *pRH, struct W_STRUCT *pRL)
{
	int16_t s16RearSLHoldThYDF;
    if((LFC_Split_flag==1)||(LFC_Split_suspect_flag==1)){
        if(vref >= (int16_t)S16_GMA_H_SPEED)     {tempB3 = (int8_t)U8_GMA_D_R_H_SPEED;}
        else if(vref >= (int16_t)S16_GMA_L_SPEED){tempB3 = (int8_t)U8_GMA_D_R_M_SPEED;} /* Added by shchoi 2002.02.14 */
        else                                 	 {tempB3 = (int8_t)U8_GMA_D_R_L_SPEED;}
    }
    else{
      #if __SELECT_LOW_FOR_LMU
        if(vref >= (int16_t)VREF_80_KPH) {tempB3 = 7;}
        else if(vref >= (int16_t)VREF_50_KPH) {tempB3 = 5;}
        else {tempB3 = 3;}
      #else /*__SELECT_LOW_FOR_LMU*/
        if(vref >= (int16_t)VREF_50_KPH) {tempB3 = 5;}
        else {tempB3 = 3;}
      #endif /*__SELECT_LOW_FOR_LMU*/
    }

  #if __VDC && __HDC && (__HDC_ABS_IMPROVE==ENABLE)
    if((lcu1HdcActiveFlg==1) && (ACTIVE_BRAKE_FLG==1))
    {   /* Active Brk, Low Speed */
    	if(tempB3 > U8_HDC_ABS_MSL_DUMP_MAX)
    	{
    		tempB3 = U8_HDC_ABS_MSL_DUMP_MAX;
    	}
    }
  #endif  

    if(pRL->SPLIT_JUMP==1) {
/*         if((pRH->AV_DUMP==0) && (pRH->SL_DUMP==0)) {              */
        pRH->AV_DUMP   = pRL->AV_DUMP;
        pRH->AV_HOLD   = pRL->AV_HOLD;
        pRH->AV_REAPPLY= pRL->AV_REAPPLY;
        pRH->BUILT     = pRL->BUILT;
        pRH->flags=244;
      #if (L_CONTROL_PERIOD>=2)
        pRH->lcabss8DumpCase = pRL->lcabss8DumpCase;
      #endif
/*         } */
        if((pRL->MSL_DUMP==1) &&(pRL->AV_REAPPLY==1)) {
            pRL->SPLIT_JUMP=pRL->SPLIT_PULSE=0;
            pRH->SPLIT_PULSE=1;
            pRH->gma_dump=0;
        }
    }
    else if((pRH->SPLIT_PULSE==1)
  #if __SELECT_LOW_FOR_LMU
    ||(lcabsu1SelectLowforNonSplit==1)
  #endif /*__SELECT_LOW_FOR_LMU*/
    ) {
        if((pRL->AV_DUMP==1)&&((int8_t)pRH->gma_dump<tempB3)) {
/*             pRH->AV_DUMP=pRH->SL_DUMP=pRL->AV_DUMP;        */
          #if __MIRC_IMPROVE && __VDC
            if((lcabsu1ValidYawStrLatG==1) && (pRL->LFC_zyklus_z>=U8_REAR_SL_START_CYCLE) && 
              ((U8_Usage_Level_of_ESC_Sen == FEEDBACK_FOR_MIRC)  ||  (U8_Usage_Level_of_ESC_Sen >= FEEDBACK_FOR_ALL_CTRL)))
            {
                if((LFC_Split_flag==1)||(LFC_Split_suspect_flag==1))
                {
                	if(vref>=S16_GMA_H_SPEED)
                	{
                		s16RearSLHoldThYDF = S16_REAR_SP_SL_HLD_THRES_YDF_H;
                	}
                	else if(vref>=S16_GMA_M_SPEED)
                	{
                		s16RearSLHoldThYDF = S16_REAR_SP_SL_HLD_THRES_YDF_M;
                	}
                	else if(vref>=S16_GMA_L_SPEED)
                	{
                		s16RearSLHoldThYDF = S16_REAR_SP_SL_HLD_THRES_YDF_L;
                	}
                	else
                	{
                		s16RearSLHoldThYDF = S16_REAR_SP_SL_HLD_THRES_YDF_B_L;
                	}
                	
                    if((pRH->rel_lam <= -S8_REAR_SP_SL_HOLD_THRES_SLIP) 
                         #if __SPLIT_STABILITY_IMPROVE_GMA
                      /*|| (pRH->lcabss16YawDumpFactor>S16_REAR_SP_SL_HOLD_THRES_YDF)*/
                      || (pRH->lcabss16YawDumpFactor>s16RearSLHoldThYDF)
                         #else
                      || (((delta_yaw_dot>=(YAW_0G5DEG+YAW_0G2DEG))||(yaw_out_dot>=YAW_1DEG))&&(delta_yaw>YAW_3DEG))
                      || (((delta_yaw_dot>=(YAW_0G2DEG))||(yaw_out_dot>=YAW_1DEG))&&(delta_yaw>YAW_5DEG))
                         #endif   
                    )
                    {
                        pRH->AV_DUMP=pRL->AV_DUMP;
                        pRH->AV_HOLD=pRH->AV_REAPPLY=pRH->BUILT=0;
                      #if (L_CONTROL_PERIOD>=2)
                        pRH->lcabss8DumpCase = pRL->lcabss8DumpCase;
                      	pRH->gma_dump = LCABS_u8CountDumpNum(pRH->gma_dump, pRH->lcabss8DumpCase);
                      #else
                      	pRH->gma_dump = LCABS_u8CountDumpNum(pRH->gma_dump);
                      #endif
                        pRH->flags=242;
                    }
                    else
                    {
                        pRH->AV_HOLD=1;
/*
                        pRH->AV_DUMP=pRH->SL_DUMP=pRH->AV_REAPPLY=pRH->BUILT=0;
*/
                        pRH->AV_DUMP=pRH->AV_REAPPLY=pRH->BUILT=0;
                        pRH->flags=247;
                    }
                }
                else
                {
                	 if(vref>=S16_GMA_H_SPEED)
                	{
                		s16RearSLHoldThYDF = S16_REAR_HOMO_SL_HLD_THR_YDF_H;
                	}
                	else if(vref>=S16_GMA_M_SPEED)
                	{
                		s16RearSLHoldThYDF = S16_REAR_HOMO_SL_HLD_THR_YDF_M;
                	}
                	else if(vref>=S16_GMA_L_SPEED)
                	{
                		s16RearSLHoldThYDF = S16_REAR_HOMO_SL_HLD_THR_YDF_L;
                	}
                	else
                	{
                		s16RearSLHoldThYDF = S16_REAR_HOMO_SL_HLD_THR_YDF_B_L;
                	}
                    if((pRH->rel_lam <= -S8_REAR_HOMO_SL_HOLD_THRES_SLIP)
                         #if __SPLIT_STABILITY_IMPROVE_GMA
                      /*|| (pRH->lcabss16YawDumpFactor>S16_REAR_HOMO_SL_HOLD_THRES_YDF)*/
                      || (pRH->lcabss16YawDumpFactor>s16RearSLHoldThYDF)
                         #else
                      || (((delta_yaw_dot>=(YAW_0G5DEG+YAW_0G2DEG))||(yaw_out_dot>=YAW_1DEG))&&(delta_yaw>YAW_3DEG))
                      || (((delta_yaw_dot>=(YAW_0G2DEG))||(yaw_out_dot>=YAW_1DEG))&&(delta_yaw>YAW_5DEG))
                         #endif
                    )
                    {
                        pRH->AV_DUMP=pRL->AV_DUMP;
                        pRH->AV_HOLD=pRH->AV_REAPPLY=pRH->BUILT=0;
                      #if (L_CONTROL_PERIOD>=2)
                        pRH->lcabss8DumpCase = pRL->lcabss8DumpCase;
                        pRH->gma_dump = LCABS_u8CountDumpNum(pRH->gma_dump, pRH->lcabss8DumpCase);
                      #else
                        pRH->gma_dump = LCABS_u8CountDumpNum(pRH->gma_dump);
                      #endif
                        pRH->flags=248;
                    }
                    else
                    {
                        pRH->AV_HOLD=1;
/*
                        pRH->AV_DUMP=pRH->SL_DUMP=pRH->AV_REAPPLY=pRH->BUILT=0;
*/
                        pRH->AV_DUMP=pRH->AV_REAPPLY=pRH->BUILT=0;
                        pRH->flags=247;
                    }
                }
            }
            else
            {
                pRH->AV_DUMP=pRL->AV_DUMP;
                pRH->AV_HOLD=pRH->AV_REAPPLY=pRH->BUILT=0;
              #if (L_CONTROL_PERIOD>=2)
                pRH->lcabss8DumpCase = pRL->lcabss8DumpCase;
	          	pRH->gma_dump = LCABS_u8CountDumpNum(pRH->gma_dump, pRH->lcabss8DumpCase);
	          #else
              	pRH->gma_dump = LCABS_u8CountDumpNum(pRH->gma_dump);
              #endif
                pRH->flags=242;
            }
          #else
            pRH->AV_DUMP=pRL->AV_DUMP;
            pRH->AV_HOLD=pRH->AV_REAPPLY=pRH->BUILT=0;
          #if (L_CONTROL_PERIOD>=2)
            pRH->lcabss8DumpCase = pRL->lcabss8DumpCase;
          	pRH->gma_dump = LCABS_u8CountDumpNum(pRH->gma_dump, pRH->lcabss8DumpCase);
          #else
          	pRH->gma_dump = LCABS_u8CountDumpNum(pRH->gma_dump);
          #endif
            pRH->flags=242;
          #endif  
        }
        else if(pRL->AV_DUMP==1) {
            pRH->AV_HOLD=1;
/*
            pRH->AV_DUMP=pRH->SL_DUMP=pRH->AV_REAPPLY=pRH->BUILT=0;
*/
            pRH->AV_DUMP=pRH->AV_REAPPLY=pRH->BUILT=0;
            pRH->flags=243;
        }
        else {
          #if __UNSTABLE_REAPPLY_ENABLE
            if(pRL->Reapply_Accel_flag==1)
            {
                if(pRL->LEFT_WHEEL==1)
                {
                    LCABS_vCheckMSLUnstableReapply(&RR,&RL);
                }
                else
                {
                    LCABS_vCheckMSLUnstableReapply(&RL,&RR);
                }
            }
            else {}

            if((pRL->Reapply_Accel_flag==1) && (pRH->SL_Unstable_Rise_flg==0))
            {
                pRH->AV_HOLD = 1;
/*
                pRH->AV_DUMP=pRH->SL_DUMP=pRH->AV_REAPPLY=pRH->BUILT=0;
*/
                pRH->AV_DUMP=pRH->AV_REAPPLY=pRH->BUILT=0;
                pRH->flags=249;
            }
            else
          #endif
            {
                pRH->AV_DUMP   = pRL->AV_DUMP;
                pRH->AV_HOLD   = pRL->AV_HOLD;
                pRH->AV_REAPPLY= pRL->AV_REAPPLY;
                pRH->BUILT     = pRL->BUILT;
              #if (L_CONTROL_PERIOD>=2)
                pRH->lcabss8DumpCase = pRL->lcabss8DumpCase;
              #endif
                pRH->flags=246;
                if(pRH->AV_REAPPLY==1) {pRH->gma_dump=0;}
            }
        }
    }
    else { }
}

void LCABS_vDecideFrontHSideGMACtrl(void)
{
#if (__SPLIT_STABILITY_IMPROVE_GMA==0)
    int8_t hold_timer_comp_ESP =0;
    uint8_t hold_timer_comp_ESP2 = 0;
    uint8_t GMA_ESP_DUMP_flag = 0;
    uint8_t hold_timer_comp_HSPEED;

    if(REAR_WHEEL_wl==0) {
      #if __VDC && __ESP_SPLIT_2
        yaw_hold_scan_ref=0;
        if((WL->GMA_ESP_Control==1) && (WL->gma_dump2==0)) {
            if((delta_yaw > YAW_8DEG) && (abs_yaw_out > YAW_3DEG)) {
                WL->gma_dump2=2; /* gma dump*/
            }
            else if((delta_yaw > YAW_5DEG) && (abs_yaw_out > YAW_2DEG)) {
                yaw_hold_scan_ref = 15; /*temp*/
                WL->gma_dump2=0;
            }
            else if((delta_yaw > YAW_3DEG) && (abs_yaw_out > YAW_1DEG)) {
                yaw_hold_scan_ref = 10; /*temp*/
                WL->gma_dump2=0;
            }
            else {
                yaw_hold_scan_ref = 0; /*temp*/
                WL->gma_dump2=0;
            }
        }

        if(WL->gma_dump2 > 1) {
            WL->gma_dump2--;
            WHEEL_ACT_MODE=DUMP_YMR;
            WL->flags=224;
        }
        else {
            if((GMA_SLIP_wl==1) && (GMA_PULSE==1)) {
      #endif /* __VDC && __ESP_SPLIT_2 */

			#if __CHANGED_TO_DECIDE_ONOFF == ENABLE
			#else
                WL->LFC_Conven_OnOff_Flag=1; /* Added by CSH for LFC(02.01.11) */
			#endif

            #if __VDC                                                   /*ESP+SPLIT */
              #if __ESP_SPLIT
                if(lcabsu1ValidYawStrLatG==1) /*(ESP_SPLIT==1)&&(ESP_ERROR_FLG==0)*/
                {
                    if((U8_Usage_Level_of_ESC_Sen == USE_INC_ONLY)||(U8_Usage_Level_of_ESC_Sen >= USE_INC_AND_DEC))
                    {
                        tempW4 = LCABS_s16Interpolation2P(vref, VREF_50_KPH, VREF_100_KPH, S16_GMA_HOLD_INC_dYdot_THR_L7, S16_GMA_HOLD_INC_H_dYdot_THR_L7);
                        tempW5 = LCABS_s16Interpolation2P(vref, VREF_50_KPH, VREF_100_KPH, S16_GMA_HOLD_INC_dYdot_THR_L6, S16_GMA_HOLD_INC_H_dYdot_THR_L6);
                        tempW6 = LCABS_s16Interpolation2P(vref, VREF_50_KPH, VREF_100_KPH, S16_GMA_HOLD_INC_dYdot_THR_L5, S16_GMA_HOLD_INC_H_dYdot_THR_L5);
                        
                        tempW7 = LCABS_s16Interpolation2P(vref, VREF_50_KPH, VREF_100_KPH, S16_GMA_HOLD_INC_dY_THR_L7, S16_GMA_HOLD_INC_H_dY_THR_L7);
                        tempW8 = LCABS_s16Interpolation2P(vref, VREF_50_KPH, VREF_100_KPH, S16_GMA_HOLD_INC_dY_THR_L6, S16_GMA_HOLD_INC_H_dY_THR_L6);
                        tempW9 = LCABS_s16Interpolation2P(vref, VREF_50_KPH, VREF_100_KPH, S16_GMA_HOLD_INC_dY_THR_L5, S16_GMA_HOLD_INC_H_dY_THR_L5);
                                            
                        if(delta_yaw_dot >= tempW4) /* YAW_1DEG */
                        {
                            if(delta_yaw >= tempW7) /*YAW_8DEG*/ 
                            {
                                if((gma_hold_timer==0) && (WL->gma_dump==0))
                                {
                                    gma_hold_timer=L_TIME_140MS;
                                    WL->gma_dump=1;
                                    GMA_ESP_DUMP_flag = 1;
                                }
                                hold_timer_comp_ESP = L_TIME_30MS;
                            }
                            else if(delta_yaw >= tempW8) {hold_timer_comp_ESP = L_TIME_110MS;} /*YAW_5DEG*/
                            else if(delta_yaw >= tempW9) {hold_timer_comp_ESP = L_TIME_70MS;} /*YAW_3DEG*/
                            else {gma_hold_timer=0; hold_timer_comp_ESP = L_TIME_30MS;}
                        }
                        else if(delta_yaw_dot >= tempW5) /*(YAW_0G5DEG + YAW_0G2DEG)*/
                        {
                            if(delta_yaw >= tempW7) {hold_timer_comp_ESP = L_TIME_70MS;} /*YAW_8DEG*/
                            else if(delta_yaw >= tempW8) {hold_timer_comp_ESP = L_TIME_50MS;} /*YAW_5DEG*/
                            else if(delta_yaw >= tempW9) {hold_timer_comp_ESP = L_TIME_30MS;} /*YAW_3DEG*/
                            else { }
                        }
                        else if(delta_yaw_dot >= tempW6) /*(YAW_0G5DEG + YAW_0G2DEG)*/
                        {
                            if(delta_yaw >= tempW7) {hold_timer_comp_ESP = L_TIME_30MS;} /*YAW_8DEG*/
                            else if(delta_yaw >= tempW8) {hold_timer_comp_ESP = L_TIME_20MS;} /*YAW_5DEG*/
                            else if(delta_yaw >= tempW9) {hold_timer_comp_ESP = L_TIME_10MS;} /*YAW_3DEG*/
                            else { }
                        }
                        else { }
                    }
                    
                    if((U8_Usage_Level_of_ESC_Sen == USE_DEC_ONLY)||(U8_Usage_Level_of_ESC_Sen >= USE_INC_AND_DEC))
                    {
                        if(delta_yaw_dot < S16_GMA_HOLD_INC_dYdot_THR_L5) /*(YAW_0G5DEG + YAW_0G2DEG)*/
                        {
                            if(yaw_out_dot < YAW_1DEG)
                            {
                                tempW4 = LCABS_s16Interpolation2P(vref, VREF_50_KPH, VREF_100_KPH, S16_GMA_HOLD_DEC_dYdot_THR_L1, S16_GMA_HOLD_DEC_H_dYdot_THR_L1);
                                tempW5 = LCABS_s16Interpolation2P(vref, VREF_50_KPH, VREF_100_KPH, S16_GMA_HOLD_DEC_dYdot_THR_L2, S16_GMA_HOLD_DEC_H_dYdot_THR_L2);
                                tempW6 = LCABS_s16Interpolation2P(vref, VREF_50_KPH, VREF_100_KPH, S16_GMA_HOLD_DEC_dYdot_THR_L3, S16_GMA_HOLD_DEC_H_dYdot_THR_L3);
                                
                                tempW7 = LCABS_s16Interpolation2P(vref, VREF_50_KPH, VREF_100_KPH, S16_GMA_HOLD_DEC_dY_THR_L1, S16_GMA_HOLD_DEC_H_dY_THR_L1);
                                tempW8 = LCABS_s16Interpolation2P(vref, VREF_50_KPH, VREF_100_KPH, S16_GMA_HOLD_DEC_dY_THR_L2, S16_GMA_HOLD_DEC_H_dY_THR_L2);
                                tempW9 = LCABS_s16Interpolation2P(vref, VREF_50_KPH, VREF_100_KPH, S16_GMA_HOLD_DEC_dY_THR_L3, S16_GMA_HOLD_DEC_H_dY_THR_L3);
                                
                                if(delta_yaw_dot < tempW4) /*0*/
                                {
                                    if(delta_yaw <= tempW7) {hold_timer_comp_ESP = -L_TIME_70MS;} /*YAW_3DEG*/
                                    else if(delta_yaw <= tempW8) {hold_timer_comp_ESP = -L_TIME_50MS;} /*YAW_5DEG*/
                                    else if(delta_yaw <= tempW9) {hold_timer_comp_ESP = -L_TIME_30MS;} /*YAW_8DEG*/
                                    else { }
                                }
                                else if(delta_yaw_dot <= tempW5) /*YAW_0G5DEG*/
                                {
                                    if(delta_yaw <= tempW7) {hold_timer_comp_ESP = -L_TIME_50MS;}
                                    else if(delta_yaw <= tempW8) {hold_timer_comp_ESP = -L_TIME_30MS;}
                                    else if(delta_yaw <= tempW9) {hold_timer_comp_ESP = -L_TIME_10MS;}
                                    else { }
                                }
                                else if(delta_yaw_dot <= tempW6) /*(YAW_0G5DEG + YAW_0G2DEG)*/
                                {
                                    if(delta_yaw <= tempW7) {hold_timer_comp_ESP = -L_TIME_30MS;}
                                    else if(delta_yaw <= tempW8) {hold_timer_comp_ESP = -L_TIME_20MS;}
                                    else if(delta_yaw <= tempW9) {hold_timer_comp_ESP = -L_TIME_10MS;}
                                    else { }
                                }
                                else { }
                            }
                            else { }
                        }
                        else { }
                    }

                    if(gma_hold_timer>=L_1SCAN)
                    {
                        gma_hold_timer = gma_hold_timer - L_1SCAN;
                    }
                }
              #endif
            #endif
           /********************************************************************************************/
                if(WL->gma_dump !=0) {
                    if(WL->LEFT_WHEEL==1) {
                      #if (__VDC && __ESP_SPLIT)
                        if(GMA_ESP_DUMP_flag==1) {
                            WL->gma_dump = LCABS_u8DecountStableDumpNum(WL->gma_dump);
                            WL->flags=227;
                            WHEEL_ACT_MODE = DUMP_YMR;
                        }
                        else
                      #endif
                        if(STABIL_fr==1) {
                            WL->gma_dump=0;
                            WL->hold_timer_new = WL->gma_hold_counter + L_1SCAN;
                            WL->flags=220;
                            WHEEL_ACT_MODE = HOLD_MODE;
                        }
                        else if(FR.LFC_dump_counter > FL.LFC_dump_counter) {
                            WL->gma_dump = LCABS_u8DecountStableDumpNum(WL->gma_dump);
                            WL->flags=12;
                            WHEEL_ACT_MODE = DUMP_YMR;
                        }
                      #if __VDC
                        else if((ESP_BRAKE_CONTROL_FR==1) && (HV_VL_fr==1) && (AV_VL_fr==1))
                        {
                            WL->gma_dump = LCABS_u8DecountStableDumpNum(WL->gma_dump);
                            WL->flags=228;
                            WHEEL_ACT_MODE = DUMP_YMR;
                        }
                      #endif
                        else {
                            if(((vref > VREF_70_KPH) && (WL->gma_hold_counter >= (U8_HOLD_0_H_SPEED+L_TIME_70MS))) 
                              || ((vref <= VREF_70_KPH) && (WL->gma_hold_counter >= (U8_HOLD_0+L_TIME_70MS))))
                            {
                                WL->gma_dump=0;
                                WL->hold_timer_new = WL->gma_hold_counter + L_1SCAN;
                                WL->flags=229;
                            }
                            else {WL->flags=221;}
                            WHEEL_ACT_MODE = HOLD_MODE;
                        }
                    }
                    else {
                      #if (__VDC && __ESP_SPLIT)
                        if(GMA_ESP_DUMP_flag==1) {
                            WL->gma_dump = LCABS_u8DecountStableDumpNum(WL->gma_dump);
                            WL->flags=227;
                            WHEEL_ACT_MODE = DUMP_YMR;
                        }
                        else
                      #endif
                        if(STABIL_fl==1) {
                            WL->hold_timer_new = WL->gma_hold_counter + L_1SCAN;
                            WL->gma_dump=0;
                            WL->flags=220;
                            WHEEL_ACT_MODE = HOLD_MODE;
                        }
                        else if(FL.LFC_dump_counter > FR.LFC_dump_counter) {
                            WL->gma_dump = LCABS_u8DecountStableDumpNum(WL->gma_dump);
                            WL->flags=12;
                            WHEEL_ACT_MODE = DUMP_YMR;
                        }
                      #if __VDC
                        else if((ESP_BRAKE_CONTROL_FL==1) && (HV_VL_fl==1) && (AV_VL_fl==1))
                        {
                            WL->gma_dump = LCABS_u8DecountStableDumpNum(WL->gma_dump);
                            WL->flags=228;
                            WHEEL_ACT_MODE = DUMP_YMR;
                        }
                      #endif
                        else {
                            if(((vref > VREF_70_KPH) && (WL->gma_hold_counter >= U8_HOLD_0_H_SPEED+L_TIME_70MS))
                              || ((vref <= VREF_70_KPH) && (WL->gma_hold_counter >= U8_HOLD_0+L_TIME_70MS)))
                            {
                                WL->gma_dump=0;
                                WL->hold_timer_new = WL->gma_hold_counter + L_1SCAN;
                                WL->flags=229;
                            }
                            else {WL->flags=221;}
                            WHEEL_ACT_MODE = HOLD_MODE;
                        }
                    }
                }
           /********************************************************************************************/
                else {
                        if(WL->s0_reapply_counter < 1)
                        {
                            WL->hold_timer_new_ref=(uint8_t)LCABS_s16Interpolation2P(vref, 
                                              S16_GMA_M_SPEED, S16_GMA_H_SPEED, (int16_t)U8_HOLD_0, (int16_t)U8_HOLD_0_H_SPEED);
                        }
                        else if(WL->s0_reapply_counter <= 3)
                        {
                            WL->hold_timer_new_ref=(uint8_t)LCABS_s16Interpolation2P(vref, 
                                              S16_GMA_M_SPEED, S16_GMA_H_SPEED, (int16_t)U8_HOLD13, (int16_t)U8_HOLD13_H_SPEED);
                        }
                        else if(WL->s0_reapply_counter <= 5)
                        {
                            WL->hold_timer_new_ref=(uint8_t)LCABS_s16Interpolation2P(vref, 
                                              S16_GMA_M_SPEED, S16_GMA_H_SPEED, (int16_t)U8_HOLD45, (int16_t)U8_HOLD45_H_SPEED);
                        }
                        else if(WL->s0_reapply_counter <= 7)
                        {
                            WL->hold_timer_new_ref=(uint8_t)LCABS_s16Interpolation2P(vref, 
                                              S16_GMA_M_SPEED, S16_GMA_H_SPEED, (int16_t)U8_HOLD67, (int16_t)U8_HOLD67_H_SPEED);
                        }
                        else
                        {
                            WL->hold_timer_new_ref=(uint8_t)LCABS_s16Interpolation2P(vref, 
                                              S16_GMA_M_SPEED, S16_GMA_H_SPEED, (int16_t)U8_HOLD8, (int16_t)U8_HOLD8_H_SPEED);
                        }
                    hold_timer_comp_HSPEED = (uint8_t)LCABS_s16Interpolation2P(vref, S16_GMA_H_OVER_SPEED, S16_GMA_H_MAX_SPEED, 0, (int16_t)U8_GMA_HOLD_Comp_for_HSPEED);
                    
                    WL->hold_timer_new_ref= WL->hold_timer_new_ref + hold_timer_comp_HSPEED;                            
                        
                    if(WL->hold_timer_new_ref > U8_GMA_HOLD_TIME_MAX)
                    {
                        WL->hold_timer_new_ref = U8_GMA_HOLD_TIME_MAX;
                    }
                        
                #if __VDC                           /*ESP+SPLIT */
                  #if __ESP_SPLIT
                    if((ESP_SPLIT==1) && (ESP_ERROR_FLG==0))
                    {
/*                        if(delta_yaw > YAW_8DEG) {
                            if(abs_yaw_out > YAW_5DEG) {
                                WL->hold_timer_new_ref = WL->hold_timer_new_ref +5;
                            }
                        }
*/
                        if(hold_timer_comp_ESP>0)
                        {
                            hold_timer_comp_ESP2 = (uint8_t)(hold_timer_comp_ESP);
                            WL->hold_timer_new_ref = WL->hold_timer_new_ref + hold_timer_comp_ESP2;
                        }
                        else if(hold_timer_comp_ESP<0)
                        {
                            hold_timer_comp_ESP2 = (uint8_t)(-hold_timer_comp_ESP);
                            if((hold_timer_comp_ESP2+L_TIME_30MS) > WL->hold_timer_new_ref)
                            {
                                WL->hold_timer_new_ref = L_TIME_30MS;
                            }
                            else
                            {
                                WL->hold_timer_new_ref = WL->hold_timer_new_ref - hold_timer_comp_ESP2;
                            }
                        }
                        else { }
                    }
                  #endif
                #endif

                  #if __VDC && __ESP_SPLIT                          /* ESP+SPLIT      */
                    if((ESP_SPLIT==1) && (ESP_ERROR_FLG==0))
                    {
                        if((WL->hold_timer_new<WL->hold_timer_new_ref) && (delta_yaw>YAW_5DEG) && (abs_yaw_out>YAW_5DEG))
                        {
                            WL->flags=210;
                            WHEEL_ACT_MODE = HOLD_MODE;
                        }
                        else {LCABS_vDecideBuiltRisePMode(WL->hold_timer_new,WHEEL_CTRL_YMR_ABS);}
                    }
                    else {LCABS_vDecideBuiltRisePMode(WL->hold_timer_new,WHEEL_CTRL_YMR_ABS);}
                  #else /* __VDC && __ESP_SPLIT */
                    LCABS_vDecideBuiltRisePMode(WL->hold_timer_new,WHEEL_CTRL_YMR_ABS);
                  #endif /* __VDC && __ESP_SPLIT */
                }
      #if __VDC && __ESP_SPLIT_2
            }
            else if((WL->GMA_ESP_Control==1) && (yaw_hold_scan_ref > L_TIME_30MS)) {

				#if __CHANGED_TO_DECIDE_ONOFF == ENABLE
				#else
                	WL->LFC_Conven_OnOff_Flag=1; /* MIN_HOLD_REFERENCE까지는 PULSE-UP */
				#endif
                WL->hold_timer_new_ref = yaw_hold_scan_ref;
                LCABS_vDecideBuiltRisePMode(WL->hold_timer_new,WHEEL_CTRL_YMR_ABS);
                WL->flags=225;
            }
            else {
                LCABS_vDecideBuiltRisePMode(WL->hold_timer_new,WHEEL_CTRL_YMR_ABS);
                WL->flags=226;
                WL->GMA_ESP_Control=0;
            }
        }
      #endif
    }
    else {WL->flags=223;}
#else /* (__SPLIT_STABILITY_IMPROVE_GMA==0) */
    uint8_t hold_timer_comp_HSPEED = 0;
	int16_t s16FrontStbDmpThYDF;
  #if (__SLIGHT_BRAKING_COMPENSATION)
    uint8_t u8HoldTimeCompForLowMP = 0;
  #endif

    if(REAR_WHEEL_wl==0) {
        
		#if __CHANGED_TO_DECIDE_ONOFF == ENABLE
		#else
        	WL->LFC_Conven_OnOff_Flag=1; /* Added by CSH for LFC(02.01.11) */
		#endif
        

        if(WL->gma_dump !=0) {
            gma_hold_timer = 0;
            if(WL->LEFT_WHEEL==1) {
                if(STABIL_fr==1) {
                    WL->gma_dump=0;
                    WL->hold_timer_new = WL->gma_hold_counter + L_1SCAN;
                    WL->flags=220;
                    WHEEL_ACT_MODE = HOLD_MODE;
                }
                else if(FR.LFC_dump_counter > FL.LFC_dump_counter) {
                    WL->gma_dump = LCABS_u8DecountStableDumpNum(WL->gma_dump);
                    WL->flags=12;
                    WHEEL_ACT_MODE = DUMP_YMR;
                }
              #if __VDC
                else if((ESP_BRAKE_CONTROL_FR==1) && (HV_VL_fr==1) && (AV_VL_fr==1)) 
                {
                    WL->gma_dump = LCABS_u8DecountStableDumpNum(WL->gma_dump);
                    WL->flags=228;
                    WHEEL_ACT_MODE = DUMP_YMR;
                }
              #endif
                else {
                    if(((vref > S16_GMA_H_SPEED) && (WL->gma_hold_counter >= U8_HOLD_0_H_SPEED)) 
                      || ((vref <= S16_GMA_H_SPEED) && (WL->gma_hold_counter >= U8_HOLD_0)))
                    {
                        WL->gma_dump=0;
                        WL->hold_timer_new = WL->gma_hold_counter + L_1SCAN;
                        WL->flags=229;
                    }
                    else {WL->flags=221;}
                    WHEEL_ACT_MODE = HOLD_MODE;
                }
            }
            else {
                if(STABIL_fl==1) {
                    WL->hold_timer_new = WL->gma_hold_counter + L_1SCAN;
                    WL->gma_dump=0;
                    WL->flags=220;
                    WHEEL_ACT_MODE = HOLD_MODE;
                }
                else if(FL.LFC_dump_counter > FR.LFC_dump_counter) {
                    WL->gma_dump = LCABS_u8DecountStableDumpNum(WL->gma_dump);
                    WL->flags=12;
                    WHEEL_ACT_MODE = DUMP_YMR;
                }
              #if __VDC
                else if((ESP_BRAKE_CONTROL_FL==1) && (HV_VL_fl==1) && (AV_VL_fl==1)) 
                {
                    WL->gma_dump = LCABS_u8DecountStableDumpNum(WL->gma_dump);
                    WL->flags=228;
                    WHEEL_ACT_MODE = DUMP_YMR;
                }
              #endif
                else {
                    if(((vref > S16_GMA_H_SPEED) && (WL->gma_hold_counter >= U8_HOLD_0_H_SPEED)) 
                      || ((vref <= S16_GMA_H_SPEED) && (WL->gma_hold_counter >= U8_HOLD_0)))
                    {
                        WL->gma_dump=0;
                        WL->hold_timer_new = WL->gma_hold_counter + L_1SCAN;
                        WL->flags=229;
                    }
                    else {WL->flags=221;}
                    WHEEL_ACT_MODE = HOLD_MODE;
                }
            }
        }
   /********************************************************************************************/
        else {
           if(WL->s0_reapply_counter < 1)
            {
                WL->hold_timer_new_ref=(uint8_t)LCABS_s16Interpolation2P(vref, 
                                  S16_GMA_M_SPEED, S16_GMA_H_SPEED, (int16_t)U8_HOLD_0, (int16_t)U8_HOLD_0_H_SPEED);
            }
            else if(WL->s0_reapply_counter <= 3)
            {
                WL->hold_timer_new_ref=(uint8_t)LCABS_s16Interpolation2P(vref, 
                                  S16_GMA_M_SPEED, S16_GMA_H_SPEED, (int16_t)U8_HOLD13, (int16_t)U8_HOLD13_H_SPEED);
            }
            else if(WL->s0_reapply_counter <= 5)
            {
                WL->hold_timer_new_ref=(uint8_t)LCABS_s16Interpolation2P(vref, 
                                  S16_GMA_M_SPEED, S16_GMA_H_SPEED, (int16_t)U8_HOLD45, (int16_t)U8_HOLD45_H_SPEED);
            }
            else if(WL->s0_reapply_counter <= 7)
            {
                WL->hold_timer_new_ref=(uint8_t)LCABS_s16Interpolation2P(vref, 
                                  S16_GMA_M_SPEED, S16_GMA_H_SPEED, (int16_t)U8_HOLD67, (int16_t)U8_HOLD67_H_SPEED);
            }
            else
            {
                WL->hold_timer_new_ref=(uint8_t)LCABS_s16Interpolation2P(vref, 
                                  S16_GMA_M_SPEED, S16_GMA_H_SPEED, (int16_t)U8_HOLD8, (int16_t)U8_HOLD8_H_SPEED);
            }
                    
            hold_timer_comp_HSPEED = (uint8_t)LCABS_s16Interpolation2P(vref, S16_GMA_H_OVER_SPEED, S16_GMA_H_MAX_SPEED, 0, (int16_t)U8_GMA_HOLD_Comp_for_HSPEED);
                    
          #if (__SLIGHT_BRAKING_COMPENSATION)
            if(lcabsu1SlightBraking==1)
            {
                u8HoldTimeCompForLowMP = (uint8_t)LCABS_s16Interpolation2P(lcabss16RefCircuitPress,
                                         S16_VERY_SLIGHT_BRAKING_PRESS,S16_SLIGHT_BRAKING_PRESS, (WL->hold_timer_new_ref/2), 0);
                WL->hold_timer_new_ref= WL->hold_timer_new_ref + hold_timer_comp_HSPEED - u8HoldTimeCompForLowMP;
            }
          #else  /* (__SLIGHT_BRAKING_COMPENSATION) */

            WL->hold_timer_new_ref = WL->hold_timer_new_ref + hold_timer_comp_HSPEED;

          #endif /* (__SLIGHT_BRAKING_COMPENSATION) */

            if(WL->hold_timer_new_ref > U8_GMA_HOLD_TIME_MAX)
            {
                WL->hold_timer_new_ref = U8_GMA_HOLD_TIME_MAX;
            }

        #if __VDC && __ESP_SPLIT                                                   /*ESP+SPLIT */
            if((lcabsu1ValidYawStrLatG==1) 
            && ((U8_Usage_Level_of_ESC_Sen == FEEDBACK_FOR_GMA)  ||  (U8_Usage_Level_of_ESC_Sen >= FEEDBACK_FOR_ALL_CTRL)))
            {
            	
	        	if(vref>=S16_GMA_H_SPEED)
	        	{
	        		s16FrontStbDmpThYDF = S16_FRNT_GMA_STB_DMP_THR_YDF_H;
	        	}
	        	else if(vref>=S16_GMA_M_SPEED)
	        	{
	        		s16FrontStbDmpThYDF = S16_FRNT_GMA_STB_DMP_THR_YDF_M;
	        	}
	        	else if(vref>=S16_GMA_L_SPEED)
	        	{
	        		s16FrontStbDmpThYDF = S16_FRNT_GMA_STB_DMP_THR_YDF_L;
	        	}
	        	else
	        	{
	        		s16FrontStbDmpThYDF = S16_FRNT_GMA_STB_DMP_THR_YDF_B_L;
	        	}   
        	         	
	    	    if((WL->lcabss16YawDumpFactor>s16FrontStbDmpThYDF) && (gma_hold_timer==0) && (WL->gma_dump==0))
                {
                    gma_hold_timer=WL->lcabsu8YawDumpHoldTime;
                    WHEEL_ACT_MODE = DUMP_YMR;
                    WL->flags=116;
                }
                else 
                {
                    WL->hold_timer_new_ref = WL->lcabsu8YawRiseHoldTime;
                  #if __VSM
                    if(lcepsu8EPSCtrlMode == 1)
                    {
                        WL->hold_timer_new_ref = (uint8_t)(((int16_t)WL->hold_timer_new_ref * U8_EPS_GMA_HOLD_TIME_WEG)/100);
                    }
                  #endif

                    if(WL->lcabss8WhlCtrlReqForStrkRec==U8_HOLD_DURING_STRK_RECVRY)
					{
						LCABS_vCheckForcedHldForStrRcvr(WL->hold_timer_new>=WL->hold_timer_new_ref);
						WL->lcabsu1WhlPForcedHoldCmd = 1;
						WHEEL_ACT_MODE = HOLD_MODE;
					}
					else
					{
						if((WL->lcabss8WhlCtrlReqForStrkRec==U8_RISE_BF_STRK_RECVRY)||(WL->lcabss8WhlCtrlReqForStrkRec==U8_RISE_AF_STRK_RECVRY))
						{
							WL->hold_timer_new_ref = 0;
						}
						else {}

						LCABS_vDecideBuiltRisePMode(WL->hold_timer_new_ref,WHEEL_CTRL_YMR_ABS);
					}
                }
                
                if(gma_hold_timer>0)
                {
                    gma_hold_timer = gma_hold_timer - L_1SCAN;
                    if(gma_hold_timer > WL->lcabsu8YawDumpHoldTime)
                    {
                        gma_hold_timer = WL->lcabsu8YawDumpHoldTime;
                    }
                }
            }
            else
            {
              #if __VSM
                if(lcepsu8EPSCtrlMode == 1)
                {
                    WL->hold_timer_new_ref = (uint8_t)(((int16_t)WL->hold_timer_new_ref * U8_EPS_GMA_HOLD_TIME_WEG)/100);
                }
              #endif

                if(WL->lcabss8WhlCtrlReqForStrkRec==U8_HOLD_DURING_STRK_RECVRY)
				{
					LCABS_vCheckForcedHldForStrRcvr(WL->hold_timer_new>=WL->hold_timer_new_ref);
					WL->lcabsu1WhlPForcedHoldCmd = 1;
					WHEEL_ACT_MODE = HOLD_MODE;
				}
				else
				{
					if((WL->lcabss8WhlCtrlReqForStrkRec==U8_RISE_BF_STRK_RECVRY)||(WL->lcabss8WhlCtrlReqForStrkRec==U8_RISE_AF_STRK_RECVRY))
					{
						WL->hold_timer_new_ref = 0;
					}
					else {}

					LCABS_vDecideBuiltRisePMode(WL->hold_timer_new_ref,WHEEL_CTRL_YMR_ABS);
				}
            }
        #else /* __VDC && __ESP_SPLIT */
            LCABS_vDecideBuiltRisePMode(WL->hold_timer_new,WHEEL_CTRL_YMR_ABS);
        #endif /* __VDC && __ESP_SPLIT */

        }
    }
    else {WL->flags=223;}

#endif /* (__SPLIT_STABILITY_IMPROVE_GMA==0) */

}

void LCABS_vDecideUNSTABLEStateCtrl(void)
{
    int8_t MSL_SPLIT_CONTROL;
    uint8_t u8AllowUnstableStateRise = 0;
    
    MSL_SPLIT_CONTROL=0;

    WHEEL_ACT_MODE=0;
    tempF0=0;
    WL->state = UNSTABLE;

    if(ABS_fz==0){
        WL->Dump_hold_scan_ref=0;
        WL->Dump_hold_scan_counter=0;
    }

/****************************************************************/
  #if !__ROUGH_ROAD_DUMP_IMPROVE
/****************************************************************/
#if __VDC
    if((Rough_road_suspect_vehicle==1)&&(YAW_CDC_WORK==0)){
#else
    if(Rough_road_suspect_vehicle==1){
#endif
        if((WL->rel_lam > -60)&&(WL->s_diff > -S16_SDIFF_12G0)) {
            if(WL->Dump_hold_scan_counter<L_TIME_20MS) {tempF0=1;}
        }
        else {
            if(WL->Dump_hold_scan_counter<L_TIME_10MS) {tempF0=1;}
        }
    }
#if __VDC
  #if __UCC_COOPERATION_CONTROL
    else if(((Rough_road_detect_vehicle==1)||(UCC_DETECT_BUMP_Vehicle_flag==1))&&(YAW_CDC_WORK==0)){
  #else
    else if((Rough_road_detect_vehicle==1)&&(YAW_CDC_WORK==0)){
  #endif
#else
    else if(Rough_road_detect_vehicle==1){
#endif
        if((WL->rel_lam > -60)&&(WL->s_diff > -S16_SDIFF_12G0)) {
            if(WL->Dump_hold_scan_counter<L_TIME_40MS) {tempF0=1;}
        }
        else {
            if(WL->Dump_hold_scan_counter<L_TIME_20MS) {tempF0=1;}
        }
    }
    else { }
/****************************************************************/
  #endif      
/****************************************************************/

    MSL_SPLIT_CONTROL=LCABS_s8CheckMSLCtrlMode();

  #if (M_DP_BASE_BUILT_RISE==ENABLE)   
    LCABS_vCompdPParameterRef(); /* unstable reapply detection carry over */
    u8AllowUnstableStateRise=LCABS_U8CheckUNSTABLEReapply(); /* unstable reapply detection carry over */
  #endif

  #if   __VDC && __ABS_COMB
    if(REAR_WHEEL_wl==0) {
        if(delta_yaw>=YAW_20DEG) {
            if(WL->ESP_ABS_Over_Front_Inside==1) {
                WHEEL_ACT_MODE = ESP_ABS_COOPERATION_MODE;
            }
        }
        else {WL->ESP_ABS_Front_Forced_Dump_timer=Forced_DUMP_Scan_Front;}
    }
    else {
        if(delta_yaw>=YAW_20DEG) {
            if(WL->ESP_ABS_Over_Rear_Inside==1) {
                WHEEL_ACT_MODE = ESP_ABS_COOPERATION_MODE;
            }
        }
        else {WL->ESP_ABS_Rear_Forced_Dump_timer=Forced_DUMP_Scan_Rear;}
        if(WL->ESP_ABS_Under_Rear_Outside==1) {
            WL->ESP_ABS_Rear_Forced_Dump_timer=0;
            WHEEL_ACT_MODE = ESP_ABS_COOPERATION_MODE;
        }
    }

    if(WHEEL_ACT_MODE == ESP_ABS_COOPERATION_MODE) {LCABS_vForcedDumpHoldWheel();}

    if((WHEEL_ACT_MODE == ESP_ABS_COOPERATION_MODE) && (WL->AV_DUMP==1)) {
    	WL->WheelCtrlMode = WHEEL_CTRL_ESC_COOP;
        if(GMA_SLIP_wl==1){
            if(WL->gma_dump > 0) {WL->gma_dump = LCABS_u8DecountStableDumpNum(WL->gma_dump);}
        }
        if((MSL_SPLIT_CONTROL==1) && (WL->SPLIT_PULSE==1))
        {
          #if (L_CONTROL_PERIOD>=2)
        	WL->gma_dump = LCABS_u8CountDumpNum(WL->gma_dump, WL->lcabss8DumpCase);
          #else
            WL->gma_dump = LCABS_u8CountDumpNum(WL->gma_dump);
          #endif
        }
        LCABS_vWPresModeUNSTABLEDUMP();
    }
    else {
  #endif

    if(tempF0==1) {
        if(MSL_SPLIT_CONTROL==2) {
        /*  if((REAR_WHEEL_wl==1) && (WL->MSL_BASE==0)) { */
        		WL->WheelCtrlMode = WHEEL_CTRL_MSL_REAR;
                if(LEFT_WHEEL_wl==1) {LCABS_vCheckMSLSplitCtrl(&RL, &RR);}
                else {LCABS_vCheckMSLSplitCtrl(&RR, &RL);}
                if(WL->AV_DUMP==1) {LCABS_vWPresModeUNSTABLEDUMP();}
                else if(WL->AV_HOLD==1) {LCABS_vWPresModeUNSTABLEHOLD();}
                /*else {LCABS_vWPresModeUNSTABLEHOLD();}     // BASE wheel Unstable Reapply -> need to change */
                else
                {
                	LCABS_vWPresModeUNSTABLEREAPPLY();
                }
        /*  } */
        }
      #if __VDC && __ABS_COMB
        else if(WHEEL_ACT_MODE == ESP_ABS_COOPERATION_MODE)
        {
            WL->flags=219;
        	WL->WheelCtrlMode = WHEEL_CTRL_ESC_COOP;
            LCABS_vWPresModeUNSTABLEHOLD();
        }
      #endif
        else {
          #if (M_DP_BASE_BUILT_RISE==ENABLE)
            if(u8AllowUnstableStateRise==1)
            {
            	WL->WheelCtrlMode = WHEEL_CTRL_UNSTABLE_REAPPLY;
                if(WL->Reapply_Accel_flag == 0)
                {
                	LCABS_vUpdVarsAftDump();
                }

                if(WL->lcabss8WhlCtrlReqForStrkRec==U8_HOLD_DURING_STRK_RECVRY)
                {
                	LCABS_vCheckForcedHldForStrRcvr(u8AllowUnstableStateRise);
					WL->lcabsu1WhlPForcedHoldCmd = 1;
					WHEEL_ACT_MODE = HOLD_MODE;
				}
				else
				{
					LCABS_vDecideBuiltRisePMode(WL->hold_timer_new_ref,WHEEL_CTRL_UNSTABLE_REAPPLY);
				}
                
                if(WHEEL_ACT_MODE==BUILT_MODE)
                {
                	WL->flags=202;
                    LCABS_vWPresModeUNSTABLEREAPPLY();
                }
                else
                {
                    WL->flags=206;
                    LCABS_vWPresModeUnstReapplyHOLD(S16_HOLD_MODE_PATTERN); /*rise-hold(pattern)*/
                }
            }
            else
            {
                if(WL->LFC_built_reapply_flag == 1)
                {
                    WL->flags=206;
                    WL->WheelCtrlMode = WHEEL_CTRL_UNSTABLE_REAPPLY;
                    LCABS_vWPresModeUnstReapplyHOLD(S16_HOLD_MODE_FORCED); /*rise-hold(forced)*/
                }
                else
                {
                    WL->flags=203;
                    WL->WheelCtrlMode = WHEEL_CTRL_NORMAL_ABS;
                    LCABS_vWPresModeUNSTABLEHOLD(); /*dump-hold*/
                }
            }
          #elif (M_DP_BASE_BUILT_RISE==DISABLE)/*__UNSTABLE_REAPPLY_ENABLE*/
            LCABS_vDecideDeltaPCtrlMode();

            if(WL->Reapply_Accel_flag==1)
            {
                if(WL->Forced_Hold_After_Unst_Reapply==1)
                {
                    WL->flags=206;
                    LCABS_vWPresModeUnstReapplyHOLD(S16_HOLD_MODE_FORCED);
                }
                else
                {
                    WL->flags=202;
                    LCABS_vWPresModeUNSTABLEREAPPLY();
                }
            }
            else
            {
                WL->flags=203;
                LCABS_vWPresModeUNSTABLEHOLD();
            }
          #else /*__UNSTABLE_REAPPLY_ENABLE*/
            WL->flags=203;
            LCABS_vWPresModeUNSTABLEHOLD();
          #endif
        }
    }
    else {
        if((WL->Dump_hold_scan_ref==U8_Max_hold_scan_ref) && ((WL->arad>=ARAD_1G0)||(WL->rel_lam>0))) {
            tempB6=1;
        }
        else {
            tempB6=0;
        }

      #if (L_CONTROL_PERIOD>=2)
        if((WL->Dump_hold_scan_counter >= (WL->Dump_hold_scan_ref-1)) && (tempB6==0))
      #else /*(L_CONTROL_PERIOD>=2)*/
        if((WL->Dump_hold_scan_counter >= WL->Dump_hold_scan_ref) && (tempB6==0))
      #endif
        {
        	WL->WheelCtrlMode = WHEEL_CTRL_NORMAL_ABS;

          #if (L_CONTROL_PERIOD>=2)
            if(WL->Dump_hold_scan_counter >= WL->Dump_hold_scan_ref)
            {
                if(WL->Dump_hold_scan_ref>0)
                {
                    WL->lcabss8DumpCase = U8_L_DUMP_CASE_10;
                    WL->flags=210;
                }
                else
                {
                    WL->lcabss8DumpCase = U8_L_DUMP_CASE_11;
                    WL->flags=200;
                    
                    if(GMA_SLIP_wl==1)
                    {
                        WL->gma_dump = (WL->gma_dump<=L_CONTROL_PERIOD)? L_CONTROL_PERIOD : WL->gma_dump;
                    }
                }
            }
            else
            {
                WL->lcabss8DumpCase = U8_L_DUMP_CASE_01;
                WL->flags=208;
            }
          #else /*(L_CONTROL_PERIOD>=2)*/
            WL->flags=200;
          #endif /*(L_CONTROL_PERIOD>=2)*/

            if(GMA_SLIP_wl==1)
            {
                WL->gma_dump = LCABS_u8DecountStableDumpNum(WL->gma_dump);
            }

            if((MSL_SPLIT_CONTROL==1) && (WL->SPLIT_PULSE==1)) 
            {
              #if (L_CONTROL_PERIOD>=2)
            	WL->gma_dump = LCABS_u8CountDumpNum(WL->gma_dump, WL->lcabss8DumpCase);
              #else
              	WL->gma_dump = LCABS_u8CountDumpNum(WL->gma_dump);
              #endif
            }

            LCABS_vWPresModeUNSTABLEDUMP();
        }
        else {
            if((GMA_SLIP_wl==1) && (GMA_PULSE==1)) {
            	WL->WheelCtrlMode = WHEEL_CTRL_YMR_ABS;
                LCABS_vDecideFrontHSideGMACtrl();
                if(WHEEL_ACT_MODE == DUMP_YMR) {
                    LCABS_vWPresModeGMADUMP();
                  #if (L_CONTROL_PERIOD>=2)
                    WL->LFC_dump_counter = LCABS_u8CountDumpNum(WL->LFC_dump_counter, WL->lcabss8DumpCase);
                  #else
                    WL->LFC_dump_counter = LCABS_u8CountDumpNum(WL->LFC_dump_counter);
                  #endif
                  
                  #if __PRESS_EST && __CALC_EFFECTIVE_DUMPSCANS
                    if(lcabsu1ValidBrakeSensor==1)
                    {
                        if(WL->s16_Estimated_Active_Press > LOWEST_PRESS_LEVEL)
                        {
                            WL->lcabsu8EffectiveDumpScans = WL->LFC_dump_counter;
                        }
                    }
                    else
                    {
                        WL->lcabsu8EffectiveDumpScans = WL->LFC_dump_counter;
                    }
                  #endif
                }
                else if(WHEEL_ACT_MODE == HOLD_MODE) {LCABS_vWPresModeSTABLEHOLD();}
                else if(WHEEL_ACT_MODE == REAPPLY_MODE) {LCABS_vWPresModeFULLREAPPLY();}
                else { }
            }
            else if(MSL_SPLIT_CONTROL==2) {
            	WL->WheelCtrlMode = WHEEL_CTRL_MSL_REAR;
                if(LEFT_WHEEL_wl==1) {LCABS_vCheckMSLSplitCtrl(&RL, &RR);}
                else {LCABS_vCheckMSLSplitCtrl(&RR, &RL);}
                if(WL->AV_DUMP==1) {LCABS_vWPresModeUNSTABLEDUMP();}
                else if(WL->AV_HOLD==1) {LCABS_vWPresModeUNSTABLEHOLD();}
                /*else {LCABS_vWPresModeUNSTABLEHOLD();}     // BASE wheel Unstable Reapply -> need to change */
                else {LCABS_vWPresModeUNSTABLEREAPPLY();}
            }
            else if((REAR_WHEEL_wl==0) && (MSL_SPLIT_CONTROL==1) && (WL->LFC_dump_counter<GMA_dumptime_at_H_to_Split))
            {
            	WL->WheelCtrlMode = WHEEL_CTRL_YMR_ABS;
                WL->flags=239;
                LCABS_vWPresModeGMADUMP();
              #if (L_CONTROL_PERIOD>=2)
                WL->LFC_dump_counter = LCABS_u8CountDumpNum(WL->LFC_dump_counter, WL->lcabss8DumpCase);
              #else
                WL->LFC_dump_counter = LCABS_u8CountDumpNum(WL->LFC_dump_counter);
              #endif
              
              #if __PRESS_EST && __CALC_EFFECTIVE_DUMPSCANS
                if(lcabsu1ValidBrakeSensor==1)
                {
                    if(WL->s16_Estimated_Active_Press > LOWEST_PRESS_LEVEL)
                    {
                        WL->lcabsu8EffectiveDumpScans = WL->LFC_dump_counter;
                    }
                }
                else
                {
                    WL->lcabsu8EffectiveDumpScans = WL->LFC_dump_counter;
                }
              #endif
                
              #if __UNSTABLE_REAPPLY_ENABLE
                if(WL->Reapply_Accel_flag==1)
                {
                    WL->Reapply_Accel_flag=0;
                    WL->Unstable_reapply_counter=0;
                    WL->lcabss8URScansBfUnstDump=0;
                    WL->Unst_Reapply_Scan_Ref=0;
                    WL->Reapply_Accel_hold_timer=0;
                    WL->Reapply_Accel_cycle=0;
                }
              #endif    
                WL->lcabsu1SLDumpCycle=1;
            }
          #if __VDC && __ABS_COMB
            else if(WHEEL_ACT_MODE == ESP_ABS_COOPERATION_MODE)
            {
            	WL->WheelCtrlMode = WHEEL_CTRL_ESC_COOP;

                WL->flags=219;
                LCABS_vWPresModeUNSTABLEHOLD();
            }
          #endif
            else {
              #if (M_DP_BASE_BUILT_RISE==ENABLE)   
                if(u8AllowUnstableStateRise==1)
                {
                	WL->WheelCtrlMode = WHEEL_CTRL_UNSTABLE_REAPPLY;

                    if(WL->Reapply_Accel_flag == 0)
                    {
                    	LCABS_vUpdVarsAftDump();
                    }

                    if(WL->lcabss8WhlCtrlReqForStrkRec==U8_HOLD_DURING_STRK_RECVRY)
                    {
                    	LCABS_vCheckForcedHldForStrRcvr(u8AllowUnstableStateRise);
    					WL->lcabsu1WhlPForcedHoldCmd = 1;
    					WHEEL_ACT_MODE = HOLD_MODE;
    				}
    				else
    				{
    					LCABS_vDecideBuiltRisePMode(WL->hold_timer_new_ref,WHEEL_CTRL_UNSTABLE_REAPPLY);
    				}
                    
                    if(WHEEL_ACT_MODE==BUILT_MODE)
                    {
                    	WL->flags=202;
                        LCABS_vWPresModeUNSTABLEREAPPLY();
                    }
                    else
                    {
                        WL->flags=206;
                        LCABS_vWPresModeUnstReapplyHOLD(S16_HOLD_MODE_PATTERN); /*rise-hold(pattern)*/
                    }
                }
                else
                {
                    if(WL->LFC_built_reapply_flag == 1)
                    {
                    	WL->WheelCtrlMode = WHEEL_CTRL_UNSTABLE_REAPPLY;
                        WL->flags=206;
                        LCABS_vWPresModeUnstReapplyHOLD(S16_HOLD_MODE_FORCED); /*rise-hold(forced)*/
                    }
                    else
                    {
                    	WL->WheelCtrlMode = WHEEL_CTRL_NORMAL_ABS;
                        WL->flags=201;
                        LCABS_vWPresModeUNSTABLEHOLD(); /*dump-hold*/
                    }
                }
              #elif __UNSTABLE_REAPPLY_ENABLE
                LCABS_vDecideDeltaPCtrlMode();

                if(WL->Reapply_Accel_flag==1)
                {
                    if(WL->Forced_Hold_After_Unst_Reapply==1)
                    {
                        WL->flags=206;
                        LCABS_vWPresModeUnstReapplyHOLD(S16_HOLD_MODE_FORCED);
                    }
                    else
                    {
                        WL->flags=202;
                        LCABS_vWPresModeUNSTABLEREAPPLY();
                    }
                }
                else
                {
                    WL->flags=201;
                    LCABS_vWPresModeUNSTABLEHOLD();
                }
              #else /*__UNSTABLE_REAPPLY_ENABLE*/
                WL->flags=201;
                LCABS_vWPresModeUNSTABLEHOLD();
              #endif
            /*  if((WL->s_diff>0) && (WL->Reapply_Accel_hold_timer==0) && (...)) {
                    WL->flags=202;
                    LCABS_vWPresModeUNSTABLEREAPPLY();
                }
                else {
                   WL->flags=201;
                   LCABS_vWPresModeUNSTABLEHOLD();
                } */
            }
        }
    }
#if   __VDC && __ABS_COMB
    }
#endif
}

void LCABS_vWPresModeUNSTABLEDUMP(void)
{
    AV_DUMP_wl = 1;
    AV_REAPPLY_wl = 0;
    AV_HOLD_wl = 0;
    BUILT_wl = 0;
/*    WL->p_hold_time=0; */
    
  #if (L_CONTROL_PERIOD>=2)
    if(WL->lcabss8DumpCase==U8_L_DUMP_CASE_10)
    {
        WL->Dump_hold_scan_counter = 1;
    }
    else if((WL->lcabss8DumpCase==U8_L_DUMP_CASE_11) || (WL->lcabss8DumpCase==U8_L_DUMP_CASE_01))
    {
        WL->Dump_hold_scan_counter = 0;
    }
    else
    {
        ;
    }
  #else
    WL->Dump_hold_scan_counter = 0;
  #endif

  #if (L_CONTROL_PERIOD>=2)
    WL->LFC_dump_counter = LCABS_u8CountDumpNum(WL->LFC_dump_counter, WL->lcabss8DumpCase);
  #else
    WL->LFC_dump_counter = LCABS_u8CountDumpNum(WL->LFC_dump_counter);
  #endif
  
  #if __PRESS_EST && __CALC_EFFECTIVE_DUMPSCANS
    if(lcabsu1ValidBrakeSensor==1)
    {
        if(WL->s16_Estimated_Active_Press > LOWEST_PRESS_LEVEL)
        {
            WL->lcabsu8EffectiveDumpScans = WL->LFC_dump_counter;
        }
    }
    else
    {
        WL->lcabsu8EffectiveDumpScans = WL->LFC_dump_counter;
    }
  #endif

    if((WL->Mu_change_Dump_counter<U8_TYPE_MAXNUM)&&(WL->GMA_SLIP==0)&&(WL->SPLIT_JUMP==0)) 
    {
      #if (L_CONTROL_PERIOD>=2)
    	WL->Mu_change_Dump_counter = LCABS_u8CountDumpNum(WL->Mu_change_Dump_counter, WL->lcabss8DumpCase);
      #else
    	WL->Mu_change_Dump_counter = LCABS_u8CountDumpNum(WL->Mu_change_Dump_counter);
      #endif
    }

  #if __UNSTABLE_REAPPLY_ENABLE
    WL->Reapply_Accel_flag=0;
    WL->lcabss8URScansBfUnstDump=WL->Unstable_reapply_counter;
    WL->Unstable_reapply_counter=0;
    WL->Unst_Reapply_Scan_Ref=0;
    WL->Reapply_Accel_hold_timer=0;
    WL->Forced_Hold_After_Unst_Reapply=0;
  #endif

   #if (M_DP_BASE_BUILT_RISE==ENABLE)
    if(WL->s0_reapply_counter > 0)
    {
		WL->Estimated_Press_SKID = WL->s16_Estimated_Active_Press;
		WL->lcabsu1CaptureEstPressForSkid = 1;
    }

    WL->s0_reapply_counter=0;
   #endif

}

void LCABS_vWPresModeUNSTABLEHOLD(void)
{
    AV_HOLD_wl = 1;
    AV_REAPPLY_wl = 0;
    AV_DUMP_wl = 0;
    BUILT_wl = 0;
/*    WL->p_hold_time++; */

    if(WL->Dump_hold_scan_counter<U8_TYPE_MAXNUM)
    {
        WL->Dump_hold_scan_counter = WL->Dump_hold_scan_counter + L_1SCAN;
    }

    if(WL->LFC_unstable_hold_counter<U8_TYPE_MAXNUM)
    {
        WL->LFC_unstable_hold_counter = WL->LFC_unstable_hold_counter + L_1SCAN;
    }
}

#if __UNSTABLE_REAPPLY_ENABLE
void LCABS_vWPresModeUNSTABLEREAPPLY(void)
{
    AV_REAPPLY_wl = 1;
    AV_HOLD_wl = 0;
    AV_DUMP_wl = 0;
    BUILT_wl = 0;
/*    WL->p_hold_time=0; */
    WL->Dump_hold_scan_counter=0;

  #if (__IDB_LOGIC==ENABLE)
	#if __CHANGED_TO_DECIDE_ONOFF == ENABLE
	#else
    WL->LFC_Conven_OnOff_Flag=1;
	#endif
  #else
    WL->LFC_Conven_OnOff_Flag=0;
  #endif

    if(WL->SL_Unstable_Rise_flg == 0)
	{    
	    if(WL->Unstable_reapply_counter>=Min_dP_risetime)
	    {
	        WL->Reapply_Accel_cycle=1;
	    }
	    WL->Unstable_Rise_Set_Cycle=1;
	
	  #if __UNSTABLE_REAPPLY_ADAPTATION
	    if(WL->Unstable_reapply_counter>=Min_dP_risetime)
	    {
	        WL->Unstable_Rise_Pres_Cycle_flg=1;
	    }
	  #endif
	}
	
   #if (M_DP_BASE_BUILT_RISE==ENABLE)

    if(WL->s0_reapply_counter==0)
    {
    	LCABS_vUpdVarsAftDump();
    }
    else {}

    WL->s0_reapply_counter++;

    WL->Unstable_reapply_counter = WL->Unstable_reapply_counter + L_1SCAN;

    if(WL->Reapply_Accel_flag == 0)
    {
    	WL->Reapply_Accel_flag = 1;
        WL->Reapply_Accel_1st_scan = 1;
    }
    else
    {
    	WL->Reapply_Accel_1st_scan = 0;
    }

   #endif
}

void LCABS_vWPresModeUnstReapplyHOLD(int16_t s16UnstRiseHldMod) /* Call for rise-hold in unstable state */
{
    AV_HOLD_wl = 1;
    AV_REAPPLY_wl = 0;
    AV_DUMP_wl = 0;
    BUILT_wl = 0;
    
  #if (M_DP_BASE_BUILT_RISE==ENABLE)
    if(WL->LFC_built_reapply_flag == 1) {
        if(WL->Hold_Duty_Comp_flag == 0) {
            if(REAR_WHEEL_wl==0) {WL->LFC_built_reapply_counter = WL->LFC_built_reapply_counter + L_1SCAN;}
            else if((WL->Duty_Limitation_flag==0) || (WL->LFC_built_reapply_counter<=L_1ST_SCAN)) {WL->LFC_built_reapply_counter = WL->LFC_built_reapply_counter + L_1SCAN;}
            else { }
        }
        else if(WL->LFC_built_reapply_counter <= L_1ST_SCAN) {WL->LFC_built_reapply_counter = WL->LFC_built_reapply_counter + L_1SCAN;}
        else { }
    }
    
    WL->Reapply_Accel_1st_scan = 0;
  #endif

    LCABS_vDecideVarsAtRiseHld(s16UnstRiseHldMod);

}

void LCABS_vDecideDeltaPCtrlMode(void)
{

    uint8_t dP_STATE=0;
    uint8_t StartUnstableReapply=0;


    if(REAR_WHEEL_wl==0)
    {
        Min_dP_risetime=(int8_t)U8_MINIMUM_dP_RISESCAN_F;
        
        if((AFZ_OK==1)&&(afz < (-(int16_t)U8_PULSE_LOW_MU)))
        {
            if((vref<=VREF_15_KPH) && (Min_dP_risetime < (int8_t)U8_MIN_dP_RISE_Bw_15kph_F))
            {
                Min_dP_risetime=(int8_t)U8_MIN_dP_RISE_Bw_15kph_F;
            }
        }

      #if (__SLIGHT_BRAKING_COMPENSATION)
        if(lcabsu1SlightBraking==1)
        {
            if(Min_dP_risetime < (int8_t)U8_MIN_dP_RISE_AT_LOW_MP_F)
            {
                Min_dP_risetime= (int8_t)U8_MIN_dP_RISE_AT_LOW_MP_F;
            }
        }
      #endif
    }
    else
    {
        Min_dP_risetime=(int8_t)U8_MINIMUM_dP_RISESCAN_R;
        
        if((AFZ_OK==1)&&(afz < (-(int16_t)U8_PULSE_LOW_MU)))
        {
            if((vref<=VREF_15_KPH) && (Min_dP_risetime < (int8_t)U8_MIN_dP_RISE_Bw_15kph_R))
            {
                Min_dP_risetime=(int8_t)U8_MIN_dP_RISE_Bw_15kph_R;
            }
        }

      #if (__SLIGHT_BRAKING_COMPENSATION)
        if(lcabsu1SlightBraking==1)
        {
            if(Min_dP_risetime < (int8_t)U8_MIN_dP_RISE_AT_LOW_MP_R)
            {
                Min_dP_risetime= (int8_t)U8_MIN_dP_RISE_AT_LOW_MP_R;
            }
        }
      #endif
    }

    WL->Reapply_Accel_1st_scan=0;
    WL->Forced_Hold_After_Unst_Reapply=0;
  #if defined (__dP_CONTROL_DEBUG)
    WL->check=0;
  #endif

  #if __PRES_RISE_DEFER_FOR_UR
    LCABS_vDecideURPresLimitation();
  #endif
    
    dP_STATE=LCABS_U8ChkDeltaPCtrlState();

  #if __PRES_RISE_DEFER_FOR_UR
    if(dP_STATE>=3) /* Pres_rise_Limitation for Front Wheels */
    {
        WL->Forced_Hold_After_Unst_Reapply=1;
    }
    else 
  #endif
    if(dP_STATE>=2) /* Pres_rise_defer or Hold_duty_comp_flag */
    {
        if(WL->Reapply_Accel_flag==1)
        {
            if(WL->Unstable_reapply_counter < WL->Unst_Reapply_Scan_Ref)
            {
/*                if(WL->Unst_Reapply_Scan_Ref < (WL->Unstable_reapply_counter+Min_dP_risetime))
                {
                    WL->check=1;
                    WL->Unstable_reapply_counter = WL->Unst_Reapply_Scan_Ref - Min_dP_risetime;
                }
*/
                if(WL->Unst_Reapply_Scan_Ref < (WL->Unstable_reapply_counter+L_2SCAN))
                {
                  #if defined (__dP_CONTROL_DEBUG)
                    WL->check=1;
                  #endif
                    WL->Unstable_reapply_counter = WL->Unst_Reapply_Scan_Ref - L_2SCAN;
                }
                  #if defined (__dP_CONTROL_DEBUG)
                else
                {
                    WL->check=2;
                }
                  #endif

                if(WL->Reapply_Accel_hold_timer>=L_1SCAN)
                {
                    WL->Reapply_Accel_hold_timer = WL->Reapply_Accel_hold_timer - L_1SCAN;
                }
                else{}
            }
            else
            {
                  #if defined (__dP_CONTROL_DEBUG)
                WL->check=3;
                  #endif
                WL->Reapply_Accel_flag=0;
                WL->Unstable_reapply_counter = WL->Unst_Reapply_Scan_Ref;
                WL->Reapply_Accel_hold_timer=0;
            }
        }
        else{}
    }
    else if(dP_STATE>=1) /* decide deltaP Control mode */
    {
        if((WL->Reapply_Accel_flag==0) && (WL->Unstable_reapply_counter==0)) {
          #if defined (__dP_CONTROL_DEBUG)
            WL->check=4;
          #endif    
          #if __PRES_RISE_DEFER_FOR_UR
            WL->Unst_Reapply_Scan_Ref_old = 0;
          #endif    
            LCABS_vCompdPParameterRef();/*need to move --> run every loop regardless of UR enable*/
            StartUnstableReapply=LCABS_U8CheckUNSTABLEReapply();
        }
        else
        {
          #if defined (__dP_CONTROL_DEBUG)
            WL->check=5;
          #endif    
          #if __PRES_RISE_DEFER_FOR_UR
            WL->Unst_Reapply_Scan_Ref_old = WL->Unstable_reapply_counter;
            LCABS_vCompdPParameterRef();
          #endif    
           #if __DETECT_MU_FOR_UR
            LCABS_vCompDPTimPerSituation();
           #endif
            StartUnstableReapply=LCABS_U8CheckUNSTABLEReapply();
        }

        if(StartUnstableReapply==1)
        {
            WL->Unstable_reapply_counter = WL->Unstable_reapply_counter + L_1SCAN;

            if(WL->Unstable_reapply_counter > Min_dP_risetime)
            {
              #if (__VARY_UR_HOLD_PER_DUMPSCANS==1)
				  #if (__IDB_LOGIC==ENABLE)
                lcabss16temp1 = L_TIME_50MS;
                lcabss16temp2 = L_TIME_100MS;				  
				  #else              
                lcabss16temp1 = (int16_t)(WL->Unstable_reapply_counter);
                lcabss16temp2 = ((int16_t)WL->Unstable_reapply_counter*3)/2;
                  #endif/*#if (__IDB_LOGIC==ENABLE)*/
                
                if((AFZ_OK==1) && (afz>=-(int16_t)U8_PULSE_LOW_MED_MU))
                {
                    WL->Reapply_Accel_hold_timer = (int8_t)LCABS_s16Interpolation2P((int16_t)WL->LFC_dump_counter,3,5,
                                                                                    lcabss16temp1,lcabss16temp2);
                }
                else
                {
                    WL->Reapply_Accel_hold_timer = (int8_t)LCABS_s16Interpolation2P((int16_t)WL->LFC_dump_counter,1,3,
                                                                                    lcabss16temp1,lcabss16temp2);
                }
              #else  /*(__VARY_UR_HOLD_PER_DUMPSCANS==1)*/
                WL->Reapply_Accel_hold_timer=WL->Unstable_reapply_counter;
              #endif /*(__VARY_UR_HOLD_PER_DUMPSCANS==1)*/
            }
            else
            {
                WL->Reapply_Accel_hold_timer=L_TIME_20MS;
            }

            if(WL->Reapply_Accel_flag==0)
            {
                WL->Reapply_Accel_1st_scan=1;
            }
            else{}

            WL->Reapply_Accel_flag=1;
        }
        else
        {
/*            WL->Unstable_reapply_counter = WL->Unst_Reapply_Scan_Ref;*/

            if(WL->Reapply_Accel_hold_timer >= L_1SCAN)
            {
                WL->Unstable_reapply_counter = WL->Unst_Reapply_Scan_Ref; /* 종료 06.07.18*/

                WL->Reapply_Accel_hold_timer = WL->Reapply_Accel_hold_timer - L_1SCAN;
                WL->Forced_Hold_After_Unst_Reapply=1;
            }
            else
            {
                WL->Reapply_Accel_hold_timer=0;
                WL->Reapply_Accel_flag=0;
            }
        }
    }
    else
    {
      #if defined (__dP_CONTROL_DEBUG)
        WL->check=6;
      #endif
        WL->Reapply_Accel_hold_timer=0;
        WL->Reapply_Accel_flag=0;
        WL->Unstable_reapply_counter=0;
    }
}

void LCABS_vCompdPParameterRef(void)
{
    int8_t UnstableReapply_Arad_Ref=0;
    uint8_t UnstableReapply_Dump_Cnt_Ref=0;
    /*int8_t Start_unstable_reapply=0;*/

/*    int8_t MU_factor=0; */
    uint8_t UNSTABLE_REAPPLY_TIME_by_dump=0;
    int16_t Ref_Mu=0;

  #if __PRESS_EST && __CALC_EFFECTIVE_DUMPSCANS
    if(FSF_wl==1)
    {
        tempC2 = WL->lcabsu8EffectiveDumpScans;
    }
    else
    {
        tempC2 = WL->LFC_dump_counter;
    }
  #else
    tempC2 = WL->LFC_dump_counter;
  #endif
    
    if((AFZ_OK==1) && (FSF_wl==0))
    {
        Ref_Mu = afz;
        if((LOW_to_HIGH_suspect==1) && (WL->max_arad_instbl > ARAD_7G0) && (WL->arad > 0))
        {
            Ref_Mu = -(int16_t)U8_PULSE_HIGH_MU;
        }
        else if(LOW_to_HIGH_suspect==0)
        {
        	if((LFC_Split_flag==1) && (((MSL_BASE_rl==1)&&(LEFT_WHEEL_wl==1))||((MSL_BASE_rr==1)&&(LEFT_WHEEL_wl==0))))
        	{
              #if __PRESS_EST_ABS
        		if(((WL->LOW_MU_SUSPECT_by_IndexMu==1) || ((WL->IndexMu<=IndexBasalt) && (FSF_wl==0))) && (CERTAIN_SPLIT_flag==1))
        		{
        			Ref_Mu = -(int16_t)U8_PULSE_LOW_MU;
        		}
        		else
              #endif  
              #if __INHIBIT_SPLIT_LOW_FRONT_UR    		
        		if((REAR_WHEEL_wl==0)&&(afz>=-(int16_t)U8_PULSE_MED_HIGH_MU)
        		  &&(WL->max_arad_instbl<SP_LOW_FRT_UR_ALLOW_ARAD)&&(WL->peak_acc_alt<SP_LOW_FRT_UR_ALLOW_ARAD)        		
        		)
        		{
        			Ref_Mu = -(int16_t)U8_PULSE_LOW_MU;
        		}
        		else
              #endif  
        		{
        			;
        		}
        	}
    	  #if __SPLIT_STABILITY_IMPROVE_GMA
	        if((REAR_WHEEL_wl==0)&&(LFC_H_to_Split_flag==1)&&
	         (((LEFT_WHEEL_wl==0)&&(SPLIT_JUMP_rr==1))||((LEFT_WHEEL_wl==1)&&(SPLIT_JUMP_rl==1))))
	        {
	            if(WL->arad < ARAD_10G0)
	            {
	                Ref_Mu = -(int16_t)U8_PULSE_LOW_MU;
	            }
	        }
	      #endif
        }
        else
        {
        	;
        }
    }
    else
    {
      #if (__PRESS_EST && __EST_MU_FROM_SKID)
        if(WL->lcabsu8PossibleMuFromSkid > 0)
        {
            Ref_Mu=-(int16_t)WL->lcabsu8PossibleMuFromSkid; /*code optimization*/
            /*
            if(WL->lcabsu8PossibleMuFromSkid == U8_PULSE_HIGH_MU)
            {
                Ref_Mu=-(int16_t)U8_PULSE_HIGH_MU;
            }
            else if(WL->lcabsu8PossibleMuFromSkid == U8_PULSE_UNDER_LOW_MED_MU)
            {
                Ref_Mu=-(int16_t)U8_PULSE_UNDER_LOW_MED_MU;
            }
            else if(WL->lcabsu8PossibleMuFromSkid == U8_PULSE_LOW_MU)
            {
                Ref_Mu=-(int16_t)U8_PULSE_LOW_MU;
            }
            else { }
            */
        }
        else { }
      #endif
    }

        if(REAR_WHEEL_wl==0) {
            if(((AFZ_OK==1) && (FSF_wl==0)) || (Ref_Mu<0)) {
                if((Ref_Mu<-(int16_t)(U8_PULSE_LOW_MU)) && (WL->LFC_n_th_deep_slip_comp_flag==0) && (WL->High_TO_Low_Suspect_flag==0)) {     /* -0.15g 이하*/
                    if(Ref_Mu<-(int16_t)(U8_PULSE_LOW_MED_MU)) {     /* -0.30g 이하*/
                        if(Ref_Mu<-(int16_t)(U8_PULSE_MEDIUM_MU)) {      /* -0.40g 이하*/
                            if(Ref_Mu<-(int16_t)(U8_PULSE_MED_HIGH_MU)) { /* -0.55g 이하*/
                                if(Ref_Mu<-(int16_t)(U8_PULSE_HIGH_MU)) { /* -0.70g 이하*/
                                    UnstableReapply_Arad_Ref = S8_dP_Arad_Ref_Ab_HMu_F;
                                    UnstableReapply_Dump_Cnt_Ref=U8_dP_Dump_cnt_Ref_Ab_HMu_F;
                                    /* MU_factor = 4; */
                                    UNSTABLE_REAPPLY_TIME_by_dump = (uint8_t)LCABS_s8GetParamFromRef(tempC2,
                                      U8_dPTim_Dump_Ref_Ab_HMu_1_F,U8_dPTim_Dump_Ref_Ab_HMu_2_F,U8_dPTim_Dump_Ref_Ab_HMu_3_F,
                                      U8_dPTim_by_Dump_Ab_HMu_1_F,U8_dPTim_by_Dump_Ab_HMu_2_F,U8_dPTim_by_Dump_Ab_HMu_3_F,U8_dPTim_by_Dump_Ab_HMu_Max_F);

                                }
                                else {
                                    UnstableReapply_Arad_Ref = S8_dP_Arad_Ref_MHMu_HMu_F;
                                    UnstableReapply_Dump_Cnt_Ref=U8_dP_Dump_cnt_Ref_MHMu_HMu_F;
                                    /* MU_factor = 4; */
                                    UNSTABLE_REAPPLY_TIME_by_dump = (uint8_t)LCABS_s8GetParamFromRef(tempC2,
                                      U8_dPTim_Dump_Ref_MHMu_HMu_1_F,U8_dPTim_Dump_Ref_MHMu_HMu_2_F,U8_dPTim_Dump_Ref_MHMu_HMu_3_F,
                                      U8_dPTim_by_Dump_MHMu_HMu_1_F,U8_dPTim_by_Dump_MHMu_HMu_2_F,U8_dPTim_by_Dump_MHMu_HMu_3_F,U8_dPTim_by_Dump_MHMu_HMu_Max_F);
                                }
                            }
                            else {
                                UnstableReapply_Arad_Ref = S8_dP_Arad_Ref_MMu_MHMu_F;
                                UnstableReapply_Dump_Cnt_Ref=U8_dP_Dump_cnt_Ref_MMu_MHMu_F;
                                /* MU_factor = 3; */
                                UNSTABLE_REAPPLY_TIME_by_dump = (uint8_t)LCABS_s8GetParamFromRef(tempC2,
                                  U8_dPTim_Dump_Ref_MMu_MHMu_1_F,U8_dPTim_Dump_Ref_MMu_MHMu_2_F,U8_dPTim_Dump_Ref_MMu_MHMu_3_F,
                                  U8_dPTim_by_Dump_MMu_MHMu_1_F,U8_dPTim_by_Dump_MMu_MHMu_2_F,U8_dPTim_by_Dump_MMu_MHMu_3_F,U8_dPTim_by_Dump_MMu_MHMu_Max_F);
                            }
                        }
                        else {
                            UnstableReapply_Arad_Ref = S8_dP_Arad_Ref_LMMu_MMu_F;
                            UnstableReapply_Dump_Cnt_Ref=U8_dP_Dump_cnt_Ref_LMMu_MMu_F;
                            /* MU_factor = 2; */
                            UNSTABLE_REAPPLY_TIME_by_dump = (uint8_t)LCABS_s8GetParamFromRef(tempC2,
                              U8_dPTim_Dump_Ref_LMMu_MMu_1_F,U8_dPTim_Dump_Ref_LMMu_MMu_2_F,U8_dPTim_Dump_Ref_LMMu_MMu_3_F,
                              U8_dPTim_by_Dump_LMMu_MMu_1_F,U8_dPTim_by_Dump_LMMu_MMu_2_F,U8_dPTim_by_Dump_LMMu_MMu_3_F,U8_dPTim_by_Dump_LMMu_MMu_Max_F);
                        }

                        if((LFC_Split_flag==1) && (REAR_WHEEL_wl==0) && (UN_GMA_rl==1) && (UN_GMA_rr==1) &&
                          (((LEFT_WHEEL_wl==1)&&(MSL_BASE_rr==1)) || ((LEFT_WHEEL_wl==0)&&(MSL_BASE_rl==1)))) {
                            UnstableReapply_Arad_Ref = S8_dP_Arad_Ref_SpHMu_F;
                            UnstableReapply_Dump_Cnt_Ref=U8_dP_Dump_cnt_Ref_SpHMu_F;
                            UNSTABLE_REAPPLY_TIME_by_dump = (uint8_t)LCABS_s8GetParamFromRef(tempC2,
                              U8_dPTim_Dump_Ref_SPHMu_1_F,U8_dPTim_Dump_Ref_SPHMu_2_F,U8_dPTim_Dump_Ref_SPHMu_3_F,
                              U8_dPTim_by_Dump_SPHMu_1_F,U8_dPTim_by_Dump_SPHMu_2_F,U8_dPTim_by_Dump_SPHMu_3_F,U8_dPTim_by_Dump_SPHMu_Max_F);
                        }

                    }
                    else {
                        UnstableReapply_Arad_Ref = S8_dP_Arad_Ref_LMu_LMMu_F;
                        UnstableReapply_Dump_Cnt_Ref=U8_dP_Dump_cnt_Ref_LMu_LMMu_F;
                        /* MU_factor = 1; */
                        UNSTABLE_REAPPLY_TIME_by_dump = (uint8_t)LCABS_s8GetParamFromRef(tempC2,
                          U8_dPTim_Dump_Ref_LMu_LMMu_1_F,U8_dPTim_Dump_Ref_LMu_LMMu_2_F,U8_dPTim_Dump_Ref_LMu_LMMu_3_F,
                          U8_dPTim_by_Dump_LMu_LMMu_1_F,U8_dPTim_by_Dump_LMu_LMMu_2_F,U8_dPTim_by_Dump_LMu_LMMu_3_F,U8_dPTim_by_Dump_LMu_LMMu_Max_F);
                    }
                  #if __LOW_SPEED_DECEL_IMPROVE
                    if((vref<VREF_30_KPH) && (WL->LFC_zyklus_z>=10) && (LFC_Split_flag==0))
                    {
                        if(tempC2 < UnstableReapply_Dump_Cnt_Ref)
                        {
                            UnstableReapply_Dump_Cnt_Ref = tempC2;
                            UNSTABLE_REAPPLY_TIME_by_dump = 4;
                            UnstableReapply_Arad_Ref = ARAD_0G5;
                        }
                    }
                  #endif
                    /*
                    if(UNSTABLE_REAPPLY_TIME_by_dump>U8_MAXIMUM_dP_RISESCAN_F) {UNSTABLE_REAPPLY_TIME_by_dump = U8_MAXIMUM_dP_RISESCAN_F;}
                    else if(UNSTABLE_REAPPLY_TIME_by_dump<Min_dP_risetime) {UNSTABLE_REAPPLY_TIME_by_dump = Min_dP_risetime;}
                    else { }
                    */

                }
                else { /* 0.15g 이하*/
                    UnstableReapply_Arad_Ref = S8_dP_Arad_Ref_Bw_LMu_F;
                    UnstableReapply_Dump_Cnt_Ref=U8_dP_Dump_cnt_Ref_Bw_LMu_F;

                    UNSTABLE_REAPPLY_TIME_by_dump = (uint8_t)LCABS_s8GetParamFromRef(tempC2,
                      U8_dPTim_Dump_Ref_Bw_LMu_1_F,U8_dPTim_Dump_Ref_Bw_LMu_2_F,U8_dPTim_Dump_Ref_Bw_LMu_3_F,
                      U8_dPTim_by_Dump_Bw_LMu_1_F,U8_dPTim_by_Dump_Bw_LMu_2_F,U8_dPTim_by_Dump_Bw_LMu_3_F,U8_dPTim_by_Dump_Bw_LMu_Max_F);

                  #if __LOW_SPEED_DECEL_IMPROVE
                    if((vref<VREF_30_KPH) && (WL->LFC_zyklus_z>=10))
                    {
                        if(tempC2 < UnstableReapply_Dump_Cnt_Ref)
                        {
                            UnstableReapply_Dump_Cnt_Ref = tempC2;
                            UNSTABLE_REAPPLY_TIME_by_dump = 4;
                            UnstableReapply_Arad_Ref = ARAD_0G5;
                        }
                    }
                  #endif
                    if(UNSTABLE_REAPPLY_TIME_by_dump>6) {UNSTABLE_REAPPLY_TIME_by_dump = 6;}
                    /*
                    else if(UNSTABLE_REAPPLY_TIME_by_dump<Min_dP_risetime) {UNSTABLE_REAPPLY_TIME_by_dump = Min_dP_risetime;}
                    else { }
                    */
                }

            }
            else
            { /* AFZ_OK==0 */}
/*
            if(UNSTABLE_REAPPLY_TIME_by_dump>U8_MAXIMUM_dP_RISESCAN_F) {UNSTABLE_REAPPLY_TIME_by_dump = U8_MAXIMUM_dP_RISESCAN_F;}
            else if(UNSTABLE_REAPPLY_TIME_by_dump<(uint8_t)Min_dP_risetime) {UNSTABLE_REAPPLY_TIME_by_dump = (uint8_t)Min_dP_risetime;}
            else { }
*/            
            UNSTABLE_REAPPLY_TIME_by_dump = (uint8_t)LCABS_s16LimitMinMax((int16_t)UNSTABLE_REAPPLY_TIME_by_dump,(int16_t)Min_dP_risetime,(int16_t)U8_MAXIMUM_dP_RISESCAN_F);
        }
        else { /* Rear wheels */
            if(((AFZ_OK==1) && (FSF_wl==0)) || (Ref_Mu<0)) {
                if(Ref_Mu<-(int16_t)(U8_PULSE_LOW_MU)) {     /* -0.15g 이하*/
                    if(Ref_Mu<-(int16_t)(U8_PULSE_LOW_MED_MU)) {     /* -0.30g 이하*/
                        if(Ref_Mu<-(int16_t)(U8_PULSE_MEDIUM_MU)) {      /* -0.40g 이하*/
                            if(Ref_Mu<-(int16_t)(U8_PULSE_MED_HIGH_MU)) { /* -0.55g 이하*/
                                if(Ref_Mu<-(int16_t)(U8_PULSE_HIGH_MU)) { /* -0.70g 이하*/
                                    UnstableReapply_Arad_Ref = S8_dP_Arad_Ref_Ab_HMu_R;
                                    UnstableReapply_Dump_Cnt_Ref=U8_dP_Dump_cnt_Ref_Ab_HMu_R;
                                    /* MU_factor = 4; */
                                    UNSTABLE_REAPPLY_TIME_by_dump = (uint8_t)LCABS_s8GetParamFromRef(tempC2,
                                      U8_dPTim_Dump_Ref_Ab_HMu_1_R,U8_dPTim_Dump_Ref_Ab_HMu_2_R,U8_dPTim_Dump_Ref_Ab_HMu_3_R,
                                      U8_dPTim_by_Dump_Ab_HMu_1_R,U8_dPTim_by_Dump_Ab_HMu_2_R,U8_dPTim_by_Dump_Ab_HMu_3_R,U8_dPTim_by_Dump_Ab_HMu_Max_R);
                                }
                                else {
                                    UnstableReapply_Arad_Ref = S8_dP_Arad_Ref_MHMu_HMu_R;
                                    UnstableReapply_Dump_Cnt_Ref=U8_dP_Dump_cnt_Ref_MHMu_HMu_R;
                                    /* MU_factor = 4; */
                                    UNSTABLE_REAPPLY_TIME_by_dump = (uint8_t)LCABS_s8GetParamFromRef(tempC2,
                                      U8_dPTim_Dump_Ref_MHMu_HMu_1_R,U8_dPTim_Dump_Ref_MHMu_HMu_2_R,U8_dPTim_Dump_Ref_MHMu_HMu_3_R,
                                      U8_dPTim_by_Dump_MHMu_HMu_1_R,U8_dPTim_by_Dump_MHMu_HMu_2_R,U8_dPTim_by_Dump_MHMu_HMu_3_R,U8_dPTim_by_Dump_MHMu_HMu_Max_R);
                                }
                            }
                            else {
                                UnstableReapply_Arad_Ref = S8_dP_Arad_Ref_MMu_MHMu_R;
                                UnstableReapply_Dump_Cnt_Ref=U8_dP_Dump_cnt_Ref_MMu_MHMu_R;
                                /* MU_factor = 3; */
                                UNSTABLE_REAPPLY_TIME_by_dump = (uint8_t)LCABS_s8GetParamFromRef(tempC2,
                                  U8_dPTim_Dump_Ref_MMu_MHMu_1_R,U8_dPTim_Dump_Ref_MMu_MHMu_2_R,U8_dPTim_Dump_Ref_MMu_MHMu_3_R,
                                  U8_dPTim_by_Dump_MMu_MHMu_1_R,U8_dPTim_by_Dump_MMu_MHMu_2_R,U8_dPTim_by_Dump_MMu_MHMu_3_R,U8_dPTim_by_Dump_MMu_MHMu_Max_R);
                            }
                        }
                        else {
                            UnstableReapply_Arad_Ref = S8_dP_Arad_Ref_LMMu_MMu_R;
                            UnstableReapply_Dump_Cnt_Ref=U8_dP_Dump_cnt_Ref_LMMu_MMu_R;
                            /* MU_factor = 2; */
                            UNSTABLE_REAPPLY_TIME_by_dump = (uint8_t)LCABS_s8GetParamFromRef(tempC2,
                              U8_dPTim_Dump_Ref_LMMu_MMu_1_R,U8_dPTim_Dump_Ref_LMMu_MMu_2_R,U8_dPTim_Dump_Ref_LMMu_MMu_3_R,
                              U8_dPTim_by_Dump_LMMu_MMu_1_R,U8_dPTim_by_Dump_LMMu_MMu_2_R,U8_dPTim_by_Dump_LMMu_MMu_3_R,U8_dPTim_by_Dump_LMMu_MMu_Max_R);
                        }
                    }
                    else {
                        UnstableReapply_Arad_Ref = S8_dP_Arad_Ref_LMu_LMMu_R;
                        UnstableReapply_Dump_Cnt_Ref=U8_dP_Dump_cnt_Ref_LMu_LMMu_R;
                        /* MU_factor = 1; */
                        UNSTABLE_REAPPLY_TIME_by_dump = (uint8_t)LCABS_s8GetParamFromRef(tempC2,
                          U8_dPTim_Dump_Ref_LMu_LMMu_1_R,U8_dPTim_Dump_Ref_LMu_LMMu_2_R,U8_dPTim_Dump_Ref_LMu_LMMu_3_R,
                          U8_dPTim_by_Dump_LMu_LMMu_1_R,U8_dPTim_by_Dump_LMu_LMMu_2_R,U8_dPTim_by_Dump_LMu_LMMu_3_R,U8_dPTim_by_Dump_LMu_LMMu_Max_R);
                    }
                  #if __LOW_SPEED_DECEL_IMPROVE
                    if(vref<VREF_30_KPH)
                    {
                        if(tempC2 < UnstableReapply_Dump_Cnt_Ref)
                        {
                            UnstableReapply_Dump_Cnt_Ref = tempC2;
                            UNSTABLE_REAPPLY_TIME_by_dump = 4;
                            UnstableReapply_Arad_Ref = ARAD_0G5;
                        }
                    }
                  #endif
                }
                else { /* 0.15g 이하*/
                    UnstableReapply_Arad_Ref = S8_dP_Arad_Ref_Bw_LMu_R;
                    UnstableReapply_Dump_Cnt_Ref=U8_dP_Dump_cnt_Ref_Bw_LMu_R;

                    UNSTABLE_REAPPLY_TIME_by_dump = (uint8_t)LCABS_s8GetParamFromRef(tempC2,
                      U8_dPTim_Dump_Ref_Bw_LMu_1_R,U8_dPTim_Dump_Ref_Bw_LMu_2_R,U8_dPTim_Dump_Ref_Bw_LMu_3_R,
                      U8_dPTim_by_Dump_Bw_LMu_1_R,U8_dPTim_by_Dump_Bw_LMu_2_R,U8_dPTim_by_Dump_Bw_LMu_3_R,U8_dPTim_by_Dump_Bw_LMu_Max_R);

                  #if __LOW_SPEED_DECEL_IMPROVE
                    if(vref<VREF_30_KPH)
                    {
                        if(tempC2 < UnstableReapply_Dump_Cnt_Ref)
                        {
                            UnstableReapply_Dump_Cnt_Ref = tempC2;
                            UNSTABLE_REAPPLY_TIME_by_dump = 4;
                            UnstableReapply_Arad_Ref = ARAD_0G5;
                        }
                    }
                  #endif

                    if(UNSTABLE_REAPPLY_TIME_by_dump>6) {UNSTABLE_REAPPLY_TIME_by_dump = 6;}
                    /*
                    else if(UNSTABLE_REAPPLY_TIME_by_dump<Min_dP_risetime) {UNSTABLE_REAPPLY_TIME_by_dump = Min_dP_risetime;}
                    else { }
                    */
                }

            }
            else
            { /* AFZ_OK==0 */}
/*
            if(UNSTABLE_REAPPLY_TIME_by_dump>U8_MAXIMUM_dP_RISESCAN_R) {UNSTABLE_REAPPLY_TIME_by_dump = U8_MAXIMUM_dP_RISESCAN_R;}
            else if(UNSTABLE_REAPPLY_TIME_by_dump<(uint8_t)Min_dP_risetime) {UNSTABLE_REAPPLY_TIME_by_dump = (uint8_t)Min_dP_risetime;}
            else { }
*/
            UNSTABLE_REAPPLY_TIME_by_dump = (uint8_t)LCABS_s16LimitMinMax((int16_t)UNSTABLE_REAPPLY_TIME_by_dump,(int16_t)Min_dP_risetime,(int16_t)U8_MAXIMUM_dP_RISESCAN_R);
        }
        
    LCABS_vCalcDeltaPCompTime(UnstableReapply_Dump_Cnt_Ref,UnstableReapply_Arad_Ref,UNSTABLE_REAPPLY_TIME_by_dump);
    
  #if __ABS_RBC_COOPERATION
    LCABS_vCalcRBCdPCompTime();
  #endif  

}

  #if __ABS_RBC_COOPERATION
static void LCABS_vCalcRBCdPCompTime(void)
{
	int8_t s8RBCdPRiseCompRatio; 
	int8_t s8RBCdPRiseScanRef;
	
    if((lcabss16RBCPressBfABS>0) && (WL->LFC_zyklus_z<=1))
    {
    	s8RBCdPRiseCompRatio = (int8_t)LCABS_s16Interpolation2P(WL->s16_Estimated_Active_Press, 
    	                                                         MPRESS_5BAR,  MPRESS_50BAR,
			    	                                             S16_RBC_DP_COMP_RATIO_1,  	S16_RBC_DP_COMP_RATIO_2 );
    	s8RBCdPRiseScanRef = (int8_t)(lcabss16RBCPressBfABS/s8RBCdPRiseCompRatio);
        if(lcabss16RefCircuitPress>=MPRESS_50BAR)
        {
        	s8RBCdPRiseScanRef = s8RBCdPRiseScanRef - (int8_t)((lcabss16RefCircuitPress-MPRESS_50BAR)/MPRESS_30BAR);
        }
        else
        {
        	;
        }
        
    	s8RBCdPRiseScanRef = (int8_t)LCABS_s16LimitMinMax(s8RBCdPRiseScanRef, U8_MIN_RBC_dP_RISESCAN, U8_MAX_RBC_dP_RISESCAN);
    	
        if(WL->Unst_Reapply_Scan_Ref < s8RBCdPRiseScanRef)
        {
            WL->Unst_Reapply_Scan_Ref = s8RBCdPRiseScanRef;
        }
        else
        {
        	;
        }        
    }
    else
    {
    	;
    }
}
  #endif  

void LCABS_vCalcDeltaPCompTime(uint8_t dP_Dump_Ref,int8_t dP_Arad_Ref,uint8_t dP_Time_by_dump)
{
    int8_t Wheel_Accel=0;
    int16_t Ref_Mu=0;
    
    WL->Unst_Reapply_Scan_Ref=0;

  #if __MP_COMP
    LCABS_vDecideUNSTABLERiseTime();
  #endif

    if(WL->state==DETECTION)
    {
        if(WL->max_arad_instbl > WL->peak_acc_alt)
        {
            Wheel_Accel = WL->max_arad_instbl;
        }
        else
        {
            Wheel_Accel = WL->peak_acc_alt;
        }
    }
    else
    {
        if(WL->arad > 0)
        {
            Wheel_Accel = WL->max_arad_instbl;
        }
        else
        {
            Wheel_Accel = WL->arad;
        }
    }

  #if (__PRESS_EST && __EST_MU_FROM_SKID)
    if((AFZ_OK==1) && (FSF_wl==0))
    {
        Ref_Mu = afz;
    }
    else
    {
        Ref_Mu=-(int16_t)WL->lcabsu8PossibleMuFromSkid; /*code optimization*/
        /*
        if(WL->lcabsu8PossibleMuFromSkid == U8_PULSE_HIGH_MU)
        {
            Ref_Mu=-(int16_t)U8_PULSE_HIGH_MU;
        }
        else if(WL->lcabsu8PossibleMuFromSkid == U8_PULSE_UNDER_LOW_MED_MU)
        {
            Ref_Mu=-(int16_t)U8_PULSE_UNDER_LOW_MED_MU;
        }
        else if(WL->lcabsu8PossibleMuFromSkid == U8_PULSE_LOW_MU)
        {
            Ref_Mu=-(int16_t)U8_PULSE_LOW_MU;
        }
        else { }
        */
    }
  #else
    Ref_Mu = 0;
  #endif

  #if __PRESS_EST && __CALC_EFFECTIVE_DUMPSCANS
    if(FSF_wl==1)
    {
        tempC2 = WL->lcabsu8EffectiveDumpScans;
    }
    else
    {
        tempC2 = WL->LFC_dump_counter;
    }
  #else
    tempC2 = WL->LFC_dump_counter;
  #endif

    if(REAR_WHEEL_wl==0) {
      #if __REDUCE_1ST_UR_PRESS_DIFF
        if((WL->AdaptURPerOppositeWheel==1) && (dP_Arad_Ref > ARAD_1G0))
        {
            dP_Arad_Ref = dP_Arad_Ref - ARAD_1G0;
        }
      #endif
      #if __IMPROVE_DECEL_AT_CERTAIN_HMU
        else if(lcabsu1CertainHMu==1)
        {
            dP_Arad_Ref = dP_Arad_Ref - ARAD_2G0;
            dP_Dump_Ref = 1;
            dP_Time_by_dump = (dP_Time_by_dump<(uint8_t)Min_dP_risetime)? (uint8_t)Min_dP_risetime:dP_Time_by_dump;
        }
        else if(lcabsu1CertainHMuPre==1)
        {
            dP_Arad_Ref = dP_Arad_Ref - ARAD_1G0;
            dP_Dump_Ref = 2;
            dP_Time_by_dump = (dP_Time_by_dump<(uint8_t)Min_dP_risetime)? (uint8_t)Min_dP_risetime:dP_Time_by_dump;
        }        
        else { }
      #endif
        
        dP_Arad_Ref = (dP_Arad_Ref<ARAD_0G25)? ARAD_0G25:dP_Arad_Ref;

        if(((AFZ_OK==1) && (FSF_wl==0))||(Ref_Mu < 0)) 
        {
            if((tempC2 >= dP_Dump_Ref) && (Wheel_Accel > dP_Arad_Ref)) {
                WL->Unst_Reapply_Scan_Ref=(int8_t)dP_Time_by_dump;
            }
            else {
                WL->Unst_Reapply_Scan_Ref=0;
            }

          #if __LOW_SPEED_DECEL_IMPROVE
            if((WL->LFC_zyklus_z>=10)&&(((Wheel_Accel>=ARAD_5G0)&&(vref<VREF_30_KPH))||((Wheel_Accel>=ARAD_3G0)&&(vref<VREF_15_KPH))))
            {
                if((WL->Unst_Reapply_Scan_Ref < (tempC2/2)) && (WL->Unst_Reapply_Scan_Ref>0))
                {
                    WL->Unst_Reapply_Scan_Ref = (int8_t)LCABS_s16LimitMinMax((int16_t)(tempC2/2),(S8_MIN_DP_RISETIME_LSPD*L_1SCAN),(S8_MAX_DP_RISETIME_LSPD*L_1SCAN)); /* 3,5 */
                }
            }
          #endif

        }
        else 
        { /* AFZ_OK==0 */
            if((tempC2>=U8_UNSTABLE_RISE_REF_DUMP_1st_F) && (Wheel_Accel > S8_UNSTABLE_RISE_REF_ARAD_1st_F))
            {
                LCABS_vCompDPTimPerSituation();
            }
          #if __IMPROVE_DECEL_AT_CERTAIN_HMU  
            else if((lcabsu1CertainHMuPre==1)&&
                   ((tempC2>=(U8_UNSTABLE_RISE_REF_DUMP_1st_F-1)) && (tempB5 > (S8_UNSTABLE_RISE_REF_ARAD_1st_F-ARAD_1G0))))
            {
            	LCABS_vCompDPTimPerSituation();
            }
          #endif            
            else
            {
                WL->Unst_Reapply_Scan_Ref=0;
            }
        }
    }
    else {
        if(((AFZ_OK==1) && (FSF_wl==0))||(Ref_Mu < 0)) 
        {
            if((tempC2 >= dP_Dump_Ref) && (Wheel_Accel>dP_Arad_Ref)) 
            {
                WL->Unst_Reapply_Scan_Ref=(int8_t)dP_Time_by_dump;
            }
            /*
            else if(Wheel_Accel >= ARAD_1G5) {
                WL->Unst_Reapply_Scan_Ref=3;
            }
            */
            else {
                WL->Unst_Reapply_Scan_Ref=0;
            }

            if(WL->Unst_Reapply_Scan_Ref>0)
            {
                if(vref<=VREF_15_KPH) {
                    WL->Unst_Reapply_Scan_Ref = WL->Unst_Reapply_Scan_Ref + L_TIME_5MS;
                }
                else { }
            }
        }
        else 
        { /* AFZ_OK==0 */
            if((tempC2>=U8_UNSTABLE_RISE_REF_DUMP_1st_R) && (tempB5 > S8_UNSTABLE_RISE_REF_ARAD_1st_R))
            {
                LCABS_vCompDPTimPerSituation();
            }
            else
            {
                WL->Unst_Reapply_Scan_Ref=0;
            }
        }
    }

  #if __UNSTABLE_REAPPLY_ADAPTATION
    if(WL->Unst_Reapply_Scan_Ref > 0)
    {
        if(WL->Unstable_Rise_Pres_Cycle_cnt>=2)
        {
            WL->Unst_Reapply_Scan_Ref = WL->Unst_Reapply_Scan_Ref-L_TIME_15MS;
        }
        else if(WL->Unstable_Rise_Pres_Cycle_cnt>=1)
        {
            WL->Unst_Reapply_Scan_Ref = WL->Unst_Reapply_Scan_Ref-L_TIME_5MS;
        }
        else { }
            
        if(WL->Unst_Reapply_Scan_Ref<L_TIME_10MS) {WL->Unst_Reapply_Scan_Ref = L_TIME_10MS;}
    }
  #else
    if((WL->Unst_Reapply_Scan_Ref >= Min_dP_risetime) && (WL->LFC_STABIL_counter_old < L_TIME_50MS))
    {
        WL->Unst_Reapply_Scan_Ref = WL->Unst_Reapply_Scan_Ref-L_TIME_5MS;
    }
   #endif

    /*
        if(((LFC_Split_flag==1) && ((MSL_BASE_wl==1) || ((REAR_WHEEL_wl==0) &&
            (((LEFT_WHEEL_wl==0)&&(MSL_BASE_rr==1)) || ((LEFT_WHEEL_wl==1)&&(MSL_BASE_rl==1))))))) {
        WL->Unst_Reapply_Scan_Ref=0;
    }
    */
  #if __PRES_RISE_DEFER_FOR_UR
    if((WL->Unst_Reapply_Scan_Ref_old>0) && (WL->Unst_Reapply_Scan_Ref_old > WL->Unst_Reapply_Scan_Ref))
    {
        WL->Unst_Reapply_Scan_Ref = WL->Unst_Reapply_Scan_Ref_old;
    }
  #endif 

}

void LCABS_vCompDPTimPerSituation(void)
{
    uint8_t u8TempDPRisePerDumpRatioLMu;
    uint8_t u8TempDPRisePerDumpRatioMMu;
    uint8_t u8TempDPRisePerDumpRatioHMu;
    uint8_t u8TempDumpCounter;
     
   #if __DETECT_MU_FOR_UR
    LCABS_vDctRoadStatusForUR();
   #endif

    if((AFZ_OK==0) || (FSF_wl==1))
    {
		if(REAR_WHEEL_wl==0)
    	{
    	    u8TempDPRisePerDumpRatioLMu = U8_1stDPRisePerDumpRatio_LMu_F; /*20%*/
    	    u8TempDPRisePerDumpRatioMMu = U8_1stDPRisePerDumpRatio_MMu_F; /*40%*/
    	    u8TempDPRisePerDumpRatioHMu = U8_1stDPRisePerDumpRatio_HMu_F; /*50%*/
    	}
    	else
    	{
    	    u8TempDPRisePerDumpRatioLMu = U8_1stDPRisePerDumpRatio_LMu_R; /*20%*/
    	    u8TempDPRisePerDumpRatioMMu = U8_1stDPRisePerDumpRatio_MMu_R; /*40%*/
    	    u8TempDPRisePerDumpRatioHMu = U8_1stDPRisePerDumpRatio_HMu_R; /*50%*/
    	}
    
 	  #if (__PRESS_EST && __CALC_EFFECTIVE_DUMPSCANS)
    	if(FSF_wl==1)
    	{
    	    u8TempDumpCounter = WL->lcabsu8EffectiveDumpScans;
    	}
    	else
    	{
    	    u8TempDumpCounter = WL->LFC_dump_counter;
    	}
  	  #else
    	u8TempDumpCounter = WL->LFC_dump_counter;
  	  #endif/*__PRESS_EST && __CALC_EFFECTIVE_DUMPSCANS  */
              
       #if __DETECT_MU_FOR_UR
        if(WL->lcabsu1BumpOrWAForUR==1)
        {
            WL->Unst_Reapply_Scan_Ref=(int8_t)(((int16_t)u8TempDumpCounter*(int16_t)u8TempDPRisePerDumpRatioHMu)/100);

            if(WL->Unst_Reapply_Scan_Ref < U8_MIN_DP_RISE_FOR_BUMP_OR_WA)
            {
                WL->Unst_Reapply_Scan_Ref = U8_MIN_DP_RISE_FOR_BUMP_OR_WA;
            }
            else { }
        }
        else
        {
            if(WL->lcabsu1LMuForUR==1)
            {
                WL->Unst_Reapply_Scan_Ref=(int8_t)(((int16_t)u8TempDumpCounter*(int16_t)u8TempDPRisePerDumpRatioLMu)/100);
            }
            else if(WL->lcabsu1HMuForUR==1)
            {
                WL->Unst_Reapply_Scan_Ref=(int8_t)(((int16_t)u8TempDumpCounter*(int16_t)u8TempDPRisePerDumpRatioHMu)/100);
            }
            else
            {
                WL->Unst_Reapply_Scan_Ref=(int8_t)(((int16_t)u8TempDumpCounter*(int16_t)u8TempDPRisePerDumpRatioMMu)/100);
            }
        }
       #else
        if(REAR_WHEEL_wl==0)
        {
        	WL->Unst_Reapply_Scan_Ref=(int8_t)(((int16_t)u8TempDumpCounter*(int16_t)u8TempDPRisePerDumpRatioHMu)/100);
        	
            if((Wheel_Accel>ARAD_7G0) && (WL->In_Gear_flag==0)) {
                if(WL->Unst_Reapply_Scan_Ref<U8_MIN_DP_RISE_FOR_BUMP_OR_WA) {WL->Unst_Reapply_Scan_Ref = U8_MIN_DP_RISE_FOR_BUMP_OR_WA;}
                else { }
            }
            else {
                if(Wheel_Accel<=(S8_UNSTABLE_RISE_REF_ARAD_1st_F+ARAD_1G0)) { /* => U8_dP_LMu_Dct_AradRef_1st_F = (S8_UNSTABLE_RISE_REF_ARAD_1st_F+ARAD_1G0) */
                    WL->Unst_Reapply_Scan_Ref=(int8_t)(((int16_t)u8TempDumpCounter*(int16_t)u8TempDPRisePerDumpRatioLMu)/100);
                }
                else if(Wheel_Accel<=(S8_UNSTABLE_RISE_REF_ARAD_1st_F+ARAD_3G0)) { /* => U8_dP_LMu_Dct_AradRef_1st_F = (S8_UNSTABLE_RISE_REF_ARAD_1st_F+ARAD_3G0) */
                    WL->Unst_Reapply_Scan_Ref=(int8_t)(((int16_t)u8TempDumpCounter*(int16_t)u8TempDPRisePerDumpRatioMMu)/100);
                }
                else { }
            }
        }
       #endif/*__DETECT_MU_FOR_UR*/
  		     
  	  #if __MP_COMP
  		if(WL->Unst_Reapply_Scan_Ref < WL->Unsable_Rise_time)
  		{
  		    WL->Unst_Reapply_Scan_Ref=WL->Unsable_Rise_time;
  		}
  		else { }
  	  #endif
  		
  		WL->Unst_Reapply_Scan_Ref = (int8_t)LCABS_s16LimitMinMax((int16_t)WL->Unst_Reapply_Scan_Ref,(int16_t)U8_1ST_CYCLE_UR_RISE_SCAN_MIN,(int16_t)U8_1ST_CYCLE_UR_RISE_SCAN_MAX);
  		
  		#if (__SLIGHT_BRAKING_COMPENSATION)
  		  if(lcabsu1SlightBraking==1)
  		  {
  		      if(REAR_WHEEL_wl==0)
  		      {
  		          if(WL->Unst_Reapply_Scan_Ref < U8_MIN_INIT_dP_RISE_AT_LOW_MP_F)
  		          {
  		              WL->Unst_Reapply_Scan_Ref = U8_MIN_INIT_dP_RISE_AT_LOW_MP_F;
  		          }
  		      }
  		      else
  		      {
  		          if(WL->Unst_Reapply_Scan_Ref < U8_MIN_INIT_dP_RISE_AT_LOW_MP_R)
  		          {
  		              WL->Unst_Reapply_Scan_Ref = U8_MIN_INIT_dP_RISE_AT_LOW_MP_R;
  		          }
  		      }
  		  }
  		#endif /*(__SLIGHT_BRAKING_COMPENSATION)*/
    }
    else { }
    
  #if __ABS_RBC_COOPERATION
    LCABS_vCalcRBCdPCompTime();
  #endif
}

 #if __DETECT_MU_FOR_UR
static void LCABS_vDctRoadStatusForUR(void)
{
  /****** Max Wheel Accel in previous cycle *****/
    tempB5 = 0;
    if(WL->state==DETECTION)
    {
        if(WL->max_arad_instbl > WL->peak_acc_alt)
        {
            tempB5 = WL->max_arad_instbl;
        }
        else
        {
            tempB5 = WL->peak_acc_alt;
        }
    }
    else
    {
        if(WL->arad > 0)
        {
            tempB5 = WL->max_arad_instbl;
        }
        else
        {
            tempB5 = WL->arad;
        }
    }

  /*** Decide if use or not APratio_avg ***/
  #if __DETECT_LOCK_IN_CYCLE
    tempF0=0;
    if((WL->Reapply_Accel_flag==0)&&(WL->lcabsu1PossibleWLLockInCycle==0))
    {
        if(((REAR_WHEEL_wl==0)&&(WL->peak_slip<-LAM_20P))||((REAR_WHEEL_wl==1)&&(WL->peak_slip<-LAM_10P)))
        {
            tempF0=1;
        }
        else { }
    }
    else { }
  #endif
    

  /******************** Detection ******************/
    if((Rough_road_detect_vehicle==0)&&(Rough_road_suspect_vehicle==0))
    {
        if((FSF_wl==1)||(AFZ_OK==0))
        {
          #if __PRESS_EST && __CALC_EFFECTIVE_DUMPSCANS
            if(FSF_wl==1)
            {
                tempC2 = WL->lcabsu8EffectiveDumpScans;
            }
            else
            {
                tempC2 = WL->LFC_dump_counter;
            }
          #else
            tempC2 = WL->LFC_dump_counter;
          #endif

          /* Bump or Hydroplaning Dct. thresholds */
            if((REAR_WHEEL_wl==0)&&(WL->In_Gear_flag==0))
            {
                tempW9 = LCABS_s16Interpolation2P((int16_t)tempC2,(int16_t)U8_dP_HMu_Dct_DumpRef_1st_F,(int16_t)U8_dP_LMu_Dct_DumpRef_1st_F,(int16_t)BumpDct_ARef_At_LDump,(int16_t)BumpDct_ARef_At_HDump);
                
                if(((LEFT_WHEEL_wl==0)&&(FL.lcabsu1BumpOrWAForUR==1))||((LEFT_WHEEL_wl==1)&&(FR.lcabsu1BumpOrWAForUR==1)))
                {
                    tempW9 = tempW9 - ARAD_3G0;  /* Loosen Thresholds */
                }
            }

          /* Low-Mu and High-Mu Dct. thresholds */
            if(REAR_WHEEL_wl==0)
            {
                if(U8_dP_HMu_Dct_AradRef_1st_F > ARAD_4G0)
                {
                    tempB4 = (int8_t)U8_dP_HMu_Dct_AradRef_1st_F-ARAD_3G0;
                }
                else
                {
                    tempB4 = ARAD_1G0;
                }
                
               /* Detect Low Mu */
                tempW7 = LCABS_s16Interpolation2P((int16_t)tempC2,(int16_t)U8_dP_HMu_Dct_DumpRef_1st_F,(int16_t)U8_dP_LMu_Dct_DumpRef_1st_F,(int16_t)tempB4,(int16_t)U8_dP_LMu_Dct_AradRef_1st_F);
               /* Detect High Mu */
                tempW8 = LCABS_s16Interpolation2P((int16_t)tempC2,(int16_t)U8_dP_HMu_Dct_DumpRef_1st_F,(int16_t)U8_dP_LMu_Dct_DumpRef_1st_F,(int16_t)U8_dP_HMu_Dct_AradRef_1st_F,(int16_t)(U8_dP_LMu_Dct_AradRef_1st_F+ARAD_5G0));
            }
            else
            {
                if(U8_dP_HMu_Dct_AradRef_1st_R > ARAD_4G0)
                {
                    tempB4 = (int8_t)U8_dP_HMu_Dct_AradRef_1st_R-ARAD_3G0;
                }
                else
                {
                    tempB4 = ARAD_1G0;
                }
                
               /* Detect Low Mu */
                tempW7 = LCABS_s16Interpolation2P((int16_t)tempC2,(int16_t)U8_dP_HMu_Dct_DumpRef_1st_R,(int16_t)U8_dP_LMu_Dct_DumpRef_1st_R,(int16_t)tempB4,(int16_t)U8_dP_LMu_Dct_AradRef_1st_R);

               /* Detect High Mu */
                tempW8 = LCABS_s16Interpolation2P((int16_t)tempC2,(int16_t)U8_dP_HMu_Dct_DumpRef_1st_R,(int16_t)U8_dP_LMu_Dct_DumpRef_1st_R,(int16_t)U8_dP_HMu_Dct_AradRef_1st_R,(int16_t)(U8_dP_LMu_Dct_AradRef_1st_R+ARAD_5G0));

                if(((LEFT_WHEEL_wl==1)&&(FL.lcabsu1LMuForUR==1))||((LEFT_WHEEL_wl==0)&&(FR.lcabsu1LMuForUR==1)))
                { /* Loosen Low Mu Dct. threshold if same side front WL. LMu Detected */
                    tempW7 = tempW7 + ARAD_1G0;
                    tempW8 = tempW8 + ARAD_1G0;
                }
            }
            
          #if __DETECT_LOCK_IN_CYCLE
           if(tempF0==1)
           {
                if((WL->APratio_avg>=APAVG_BUMP)&&(tempW9 > ARAD_2G0))
                {
                    tempW9 = tempW9 - ARAD_2G0;  /* Loosen Thresholds for Bump */
                }
                else if(WL->APratio_avg<=APAVG_LMU)
                {
                    tempW7 = tempW7 + ARAD_1G0;  /* Loosen Thresholds for LMu */
                    tempW8 = tempW8 + ARAD_1G0;  /* Tighten Thresholds for HMu */
                }
                else { }
            }
          #endif

            WL->lcabsu1BumpOrWAForUR=0;
            WL->lcabsu1LMuForUR=0;
            WL->lcabsu1HMuForUR=0;

            if((REAR_WHEEL_wl==0)&&(WL->In_Gear_flag==0)&&(tempB5>(int8_t)tempW9))
            {
                WL->lcabsu1BumpOrWAForUR=1;
            }
          #if (__PRESS_EST && __EST_MU_FROM_SKID)
            else if(WL->lcabsu8PossibleMuFromSkid > 0)
            {
                if(WL->lcabsu8PossibleMuFromSkid >= U8_PULSE_MED_HIGH_MU)
                {
                    WL->lcabsu1HMuForUR=1;
                }
                else if(WL->lcabsu8PossibleMuFromSkid <= U8_PULSE_UNDER_LOW_MED_MU)
                {
                    WL->lcabsu1LMuForUR=1;
                }
                else { }
            }
          #endif
            else if(tempB5 >= (int8_t)tempW8)
            {
                WL->lcabsu1HMuForUR=1;
            }
            else if(tempB5 <= (int8_t)tempW7)
            {
                WL->lcabsu1LMuForUR=1;
            }
            else { }
        }
      #if __SPLIT_STABILITY_IMPROVE_GMA  
        else 
        {
            if((REAR_WHEEL_wl==0)&&(LFC_H_to_Split_flag==1)&&
              (((LEFT_WHEEL_wl==0)&&(SPLIT_JUMP_rr==1))||((LEFT_WHEEL_wl==1)&&(SPLIT_JUMP_rl==1))))
            {
                if(tempB5 < ARAD_10G0)
                {
                    WL->lcabsu1LMuForUR=1;
                }
            } 
        }
      #endif  
    }
    else { }
}
 #endif
 
uint8_t LCABS_U8CheckUNSTABLEReapply(void)
{
    uint8_t AllowUnstableReapply=0;
    int16_t Sdiff_Term=0;

    int16_t Slip_Term=0;
    int16_t Slip_Ratio=0;
    
    
  /************************** calculate sdiff term ******************************/
    tempW5 = S16_SDIFF_10G0;
    tempW7 = vref/VREF_4_KPH; /* compensate 2.5% per 0.125kph w.r.t tempW5 */
    
    if(((AFZ_OK==1)&&(afz<-(int16_t)U8_PULSE_HIGH_MU))             
      #if (__PRESS_EST && __EST_MU_FROM_SKID)
       || ((AFZ_OK==0)&&(WL->lcabsu8PossibleMuFromSkid>=U8_PULSE_HIGH_MU))
      #endif
       )
    {
        if(WL->s_diff <= S16_SDIFF_10G0)
        {
            if(vref<VREF_30_KPH)
            {
                tempW5 = S16_SDIFF_2G0;
                tempW7 = vref/VREF_5_KPH; /* compensate 2% per 0.125kph w.r.t tempW5 */
            }
            else
            {
                tempW5 = S16_SDIFF_4G0;
                tempW7 = vref/VREF_5_KPH; /* compensate 2% per 0.125kph w.r.t tempW5 */
            }
        }
    }
    else if((LFC_Split_flag==1) && (CERTAIN_SPLIT_flag==1) && (UN_GMA_wl==0)&&(UN_GMA_rl==1)&&(UN_GMA_rr==1))
    {
        if(((LEFT_WHEEL_wl==1)&&(MSL_BASE_rr==1)) || ((LEFT_WHEEL_wl==0)&&(MSL_BASE_rl==1)))
        {        
            if(WL->s_diff <= S16_SDIFF_10G0)
            {
                if(vref<VREF_30_KPH)
                {
                    tempW5 = S16_SDIFF_2G0;
                    tempW7 = vref/VREF_5_KPH; /* compensate 2% per 0.125kph w.r.t tempW5 */
                }
                else
                {
                    tempW5 = S16_SDIFF_4G0;
                    tempW7 = vref/VREF_5_KPH; /* compensate 2% per 0.125kph w.r.t tempW5 */
                }
            }
        }
    }
    else if((AFZ_OK==1)&&(afz<-(int16_t)U8_PULSE_LOW_MU))
    {
        if((vref<VREF_30_KPH) && (WL->s_diff <= S16_SDIFF_10G0))
        {
            tempW5 = S16_SDIFF_2G0;
            tempW7 = vref/VREF_5_KPH; /* compensate 2% per 0.125kph w.r.t tempW5 */
        }
        else { }
    }
    else { }
    
    if(WL->s_diff > tempW5)
    {
        Sdiff_Term = (WL->s_diff - tempW5)*tempW7;

        if(Sdiff_Term > ((vref*9)/10))
        {
            Sdiff_Term = (vref*9)/10;
        }
        else { }
    }
    else
    {
        Sdiff_Term = 0;
    }

  #if __PRESS_EST_ABS
    if((WL->LOW_MU_SUSPECT_by_IndexMu==1) ||
     ((CERTAIN_SPLIT_flag==1)&&(LFC_Split_flag==1)&&(((LEFT_WHEEL_wl==0)&&(MSL_BASE_rr==1))||((LEFT_WHEEL_wl==1)&&(MSL_BASE_rl==1)))))
    {
        if(REAR_WHEEL_wl==0)
        {
            Sdiff_Term = Sdiff_Term - VREF_1_KPH - ((vref*2)/100);
        }
        else
        {
            Sdiff_Term = Sdiff_Term - VREF_0_5_KPH - ((vref*1)/100);
        }
    }
  #endif


  /************************** calculate slip term ******************************/
    if((AFZ_OK==1) && (FSF_wl==0)) 
    {
        if(REAR_WHEEL_wl==0)
        {
            if(WL->state==DETECTION)
            {
                Slip_Ratio = LCABS_s16Interpolation2P(vref, VREF_30_KPH, VREF_5_KPH, 2, 20); /* ((vref*7)/100) */
                Slip_Term = ((vref*Slip_Ratio)/100) + VREF_0_5_KPH;
            }
            else
            {
	            Slip_Ratio = LCABS_s16Interpolation2P(vref, VREF_30_KPH, VREF_5_KPH, 7, 50); /* ((vref*7)/100) */
	            Slip_Term = ((vref*Slip_Ratio)/100) + VREF_1_KPH;
	        }
        }
        else
        {
            if(WL->state==DETECTION)
            {
                Slip_Ratio = LCABS_s16Interpolation2P(vref, VREF_30_KPH, VREF_5_KPH, 1, 20); /* ((vref*7)/100) */
                Slip_Term = ((vref*Slip_Ratio)/100);
            }
	        else
	        {
	            Slip_Ratio = LCABS_s16Interpolation2P(vref, VREF_30_KPH, VREF_5_KPH, 5, 50); /* ((vref*7)/100) */
	            Slip_Term = ((vref*Slip_Ratio)/100) + VREF_0_5_KPH;
	        }
	    }
    }
    else
    {
        if(REAR_WHEEL_wl==0)
        {
            if(WL->state==DETECTION)
            {
                Slip_Ratio = LCABS_s16Interpolation2P(vref, VREF_30_KPH, VREF_5_KPH, 1, 20); /* ((vref*7)/100) */
                Slip_Term = ((vref*Slip_Ratio)/100) + VREF_0_5_KPH;
            }
            else
            {
	            Slip_Ratio = LCABS_s16Interpolation2P(vref, VREF_30_KPH, VREF_5_KPH, 5, 20); /* ((vref*7)/100) */
	            Slip_Term = ((vref*Slip_Ratio)/100) + VREF_1_5_KPH;
	        }
          #if __REDUCE_1ST_UR_PRESS_DIFF
            if(WL->AdaptURPerOppositeWheel==1)
            {
                Slip_Term = Slip_Term + VREF_1_KPH;
            }
          #endif
        }
        else
        {
            if(WL->state==DETECTION)
            {
                Slip_Ratio = LCABS_s16Interpolation2P(vref, VREF_30_KPH, VREF_5_KPH, 1, 20); /* ((vref*7)/100) */
                Slip_Term = ((vref*Slip_Ratio)/100);
            }
	        else
	        {
	            Slip_Ratio = LCABS_s16Interpolation2P(vref, VREF_30_KPH, VREF_5_KPH, 5, 10); /* ((vref*7)/100) */
	            Slip_Term = ((vref*Slip_Ratio)/100) + VREF_0_5_KPH;
	        }
        }
        
    }
  #if (__SLIGHT_BRAKING_COMPENSATION)
    if((REAR_WHEEL_wl==0) && ((lcabsu1SlightBraking==1)
      #if __ABS_RBC_COOPERATION
       || (lcabss16RBCPressBfABS>0)
      #endif  
    )) 
    {
    	Slip_Ratio = Slip_Ratio + LCABS_s16Interpolation2P(vref, VREF_60_KPH, VREF_10_KPH, 3, 5);
    	Slip_Term  = Slip_Term + VREF_1_KPH;
    }
  #endif  
   
   /************************** Decide U.R. point ******************************/
  #if (M_DP_BASE_BUILT_RISE==ENABLE)
    if(WL->Unst_Reapply_Scan_Ref > 0) /* U.R. point decided */
  #else
    if(WL->Unstable_reapply_counter < WL->Unst_Reapply_Scan_Ref)
  #endif
    {
        if((AFZ_OK==1) && (FSF_wl==0)) {
            if((WL->state==DETECTION) && ((vref-WL->vrad)<=(Slip_Term+Sdiff_Term))) 
            {
                  #if defined (__dP_CONTROL_DEBUG)
                WL->check=10;
                  #endif    
                AllowUnstableReapply = 1;
            }
            else {
                if(REAR_WHEEL_wl==0) {
                  #if defined (__dP_CONTROL_DEBUG)  
                    WL->check=7;
                  #endif  
                    if((vref-WL->vrad)<=(Slip_Term + Sdiff_Term)) {
                        if(afz>=-(int16_t)(U8_PULSE_LOW_MU)) {
                            if((WL->arad>=ARAD_0G25) || (((WL->arad - WL->arad_alt) >= (-ARAD_0G75)) &&
                              ((vref-WL->vrad)<=(((vref*3)/100) + VREF_1_KPH + Sdiff_Term)))) 
                            {
                              #if defined (__dP_CONTROL_DEBUG)  
                                WL->check=8;
                              #endif  
                                AllowUnstableReapply = 1;
                            }
                        }
                        else if((WL->arad>=ARAD_1G0) || ((WL->arad - WL->arad_alt) >= (-ARAD_1G0))) 
                        {
                          #if defined (__dP_CONTROL_DEBUG)  
                            WL->check=9;
                          #endif  
                            AllowUnstableReapply = 1;
                        }
                        else 
                        {
                          #if defined (__dP_CONTROL_DEBUG)  
                            WL->check=11; 
                          #else
                            ;
                          #endif  
                        }
                    }
                }
                else {
                  #if defined (__dP_CONTROL_DEBUG)  
                    WL->check=7;
                  #endif  
                    if((vref-WL->vrad)<=(Slip_Term + Sdiff_Term)) {
                        if(afz>=-(int16_t)(U8_PULSE_LOW_MU)) {
                            if((WL->arad>=ARAD_0G25) || (((WL->arad - WL->arad_alt) >= (-ARAD_0G75)) &&
                              ((vref-WL->vrad)<=(((vref*3)/100) + VREF_1_KPH + Sdiff_Term)))) 
                            {
                              #if defined (__dP_CONTROL_DEBUG)  
                                WL->check=8;
                              #endif  
                                AllowUnstableReapply = 1;
                            }
                        }
                        else if((WL->arad>=ARAD_1G0) || ((WL->arad - WL->arad_alt) >= (-ARAD_1G0))) 
                        {
                          #if defined (__dP_CONTROL_DEBUG)  
                            WL->check=9;
                          #endif  
                            AllowUnstableReapply = 1;
                        }
                        else 
                        {
                          #if defined (__dP_CONTROL_DEBUG)  
                            WL->check=11; 
                          #else
                            ;
                          #endif
                        }
                    }
                }
            }
        }
        else {
            if((WL->state==DETECTION) && ((vref-WL->vrad)<=(Slip_Term+Sdiff_Term))) 
            {
               #if defined (__dP_CONTROL_DEBUG)
                WL->check=12;
               #endif
                AllowUnstableReapply = 1;
            }
            else {
                if(REAR_WHEEL_wl==0) {
                    /*if(((vref-WL->vrad)<=(((vref*5)/100) + VREF_1_5_KPH)) && ((RL.LFC_zyklus_z>=1) || (RR.LFC_zyklus_z>=1)))*/
                   #if defined (__dP_CONTROL_DEBUG)
                    WL->check=14;
                   #endif
                    if(((vref-WL->vrad)<=(Slip_Term + Sdiff_Term)))
                    {
                       #if defined (__dP_CONTROL_DEBUG)
                        WL->check=15;
                       #endif
                        if((WL->arad>=ARAD_0G5) || ((WL->arad - WL->arad_alt) >= (-ARAD_1G0))) 
                        {
                           #if defined (__dP_CONTROL_DEBUG)
                            WL->check=13;
                           #endif
                            if(((RL.state==DETECTION) || (RL.rel_lam >= (-LAM_5P))) || ((RR.state==DETECTION) || (RR.rel_lam >= (-LAM_5P)))) {
                               #if defined (__dP_CONTROL_DEBUG)
                                WL->check=16;
                               #endif
                                AllowUnstableReapply = 1;
                            }
                           #if __DETECT_MU_FOR_UR
                            else if((WL->lcabsu1HMuForUR==1) || (WL->lcabsu1BumpOrWAForUR==1))
                            {
                               #if defined (__dP_CONTROL_DEBUG)
                                WL->check=17;
                               #endif
                                AllowUnstableReapply = 1;
                            }
                           #endif
                           #if __EST_MU_FROM_SKID
                            else if(WL->lcabsu8PossibleMuFromSkid>=U8_PULSE_MEDIUM_MU)
                            {
                               #if defined (__dP_CONTROL_DEBUG)
                                WL->check=18;
                               #endif
                                AllowUnstableReapply = 1;
                            }
                           #endif
                           #if __ABS_RBC_COOPERATION
                            else if(lcabss16RBCPressBfABS>0)
                            {
                            	if((RL.rel_lam >= (-LAM_10P)) || (RR.rel_lam >= (-LAM_10P)) || (FL.rel_lam >= (-LAM_10P)) || (FR.rel_lam >= (-LAM_10P)))
                            	{
	                               #if defined (__dP_CONTROL_DEBUG)
	                                WL->check=23;
	                               #endif
	                                AllowUnstableReapply = 1;
	                            }
	                        }
                           #endif /* __ABS_RBC_COOPERATION */
	                        else { }
                        }
                    }
                }
                else {
                   #if defined (__dP_CONTROL_DEBUG)
                    WL->check=14;
                   #endif
                    if((vref-WL->vrad)<=(Slip_Term + Sdiff_Term)) {
                       #if defined (__dP_CONTROL_DEBUG)
                        WL->check=15;
                       #endif
                        if((WL->arad>=ARAD_0G5) || ((WL->arad - WL->arad_alt) >= (-ARAD_1G0))) 
                        {
                           #if defined (__dP_CONTROL_DEBUG)
                            WL->check=13;
                           #endif
                            AllowUnstableReapply = 1;
                        }
                    }
                }
            }
        }
    }
    else 
    {
       #if defined (__dP_CONTROL_DEBUG)
        WL->check=19;
       #else
        ;
       #endif
    }

      #if __ABS_RBC_COOPERATION
    if((lcabss16RBCPressBfABS>0) && ((WL->max_arad_instbl>=ARAD_2G0) || (WL->peak_acc_alt>=ARAD_2G0)))
    {
    	;
    }
    else
      #endif  /* __ABS_RBC_COOPERATION */
    if((WL->CAS_SUSPECT_flag==1) && (WL->max_arad_instbl<ARAD_3G0) && (WL->peak_acc_alt<ARAD_3G0))
    {
       #if defined (__dP_CONTROL_DEBUG)
        WL->check=20; 
       #endif
        AllowUnstableReapply = 0;
    }
    else if((REAR_WHEEL_wl==1) && (LFC_H_to_Split_flag==1) && (SPLIT_JUMP_wl==1))
    {
       #if defined (__dP_CONTROL_DEBUG)
        WL->check=22; 
       #endif
        AllowUnstableReapply = 0;
    }
    else if((WL->lcabsu1SLDumpCycle==1) && ((LFC_Split_flag==1)||(LFC_Split_suspect_flag==1)))
    {
       #if defined (__dP_CONTROL_DEBUG)
        WL->check=25; 
       #endif
        AllowUnstableReapply = 0;
    }
    else if(WL->SL_Unstable_Rise_flg==1)
    {
       #if defined (__dP_CONTROL_DEBUG)
        WL->check=21; 
       #endif
        AllowUnstableReapply = 0;
        WL->SL_Unstable_Rise_flg=0;
    }
  #if __INHIBIT_SPLIT_LOW_FRONT_UR
    else if((LFC_Split_flag==1)&&(REAR_WHEEL_wl==0)&&(afz>=-(int16_t)U8_PULSE_MED_HIGH_MU)&&(UN_GMA_wl==1))
    {
    	if((LEFT_WHEEL_wl==1) && ((FR.peak_dec_max<=-ARAD_10G0)||((vref-FR.vrad)>=(VREF_5_KPH+(vref/10)))))
    	{
    	  #if defined (__dP_CONTROL_DEBUG)	
    		WL->check=30; 
    	  #endif	
    		AllowUnstableReapply = 0;
    	}
    	else if((LEFT_WHEEL_wl==0) && ((FL.peak_dec_max<=-ARAD_10G0)||((vref-FL.vrad)>=(VREF_5_KPH+(vref/10)))))
    	{
    	  #if defined (__dP_CONTROL_DEBUG)	
    		WL->check=30; 
    	  #endif	
    		AllowUnstableReapply = 0;
    	}
    	else if((WL->state!=DETECTION) && ((vref-WL->vrad)>=(VREF_1_KPH+(vref/50))))
    	{
    	  #if defined (__dP_CONTROL_DEBUG)	
    		WL->check=31; 
    	  #endif	
    		AllowUnstableReapply = 0;
    	}
    	else { }    	
    }
  #endif  
    else {}

  #if __FIRST_CYCLE_YAW_ADAPT   
    if((FL.LFC_zyklus_z<1) && (FR.LFC_zyklus_z<1) && (FL.lcabsu8PossibleMuFromSkid==U8_PULSE_LOW_MU) && (FR.lcabsu8PossibleMuFromSkid==U8_PULSE_LOW_MU))
    {
        if((REAR_WHEEL_wl==0) && (WL->lcabss16YawDumpFactor2>=100) && (WL->LFC_STABIL_counter<=L_TIME_200MS))
        {
            if(LEFT_WHEEL_wl==1)
            {
                if((FR.rel_lam<-LAM_10P)&&(FR.Reapply_Accel_flag==0)&&(FR.LFC_built_reapply_flag==0))
                {
                  #if defined (__dP_CONTROL_DEBUG)
                    WL->check=24; 
                  #endif

                    AllowUnstableReapply = 0;
                }
            }       
            else
            {
                if((FL.rel_lam<-LAM_10P)&&(FL.Reapply_Accel_flag==0)&&(FL.LFC_built_reapply_flag==0))
                {
                   #if defined (__dP_CONTROL_DEBUG)
                    WL->check=24; 
                   #endif
                    AllowUnstableReapply = 0;
                }
            }
        }
    }
  #endif    

    return AllowUnstableReapply;
}

uint8_t LCABS_U8ChkDeltaPCtrlState(void)
{
    uint8_t DctDeltaP=0;

    if(WL->state==UNSTABLE)
    {
        #if __PRES_RISE_DEFER_FOR_UR
        if(WL->lcabsu1LimitURPresRise==1)
        {
            DctDeltaP=3;
        }
        else
       #endif
        {
            DctDeltaP=1;
        }
    }
    else
    {
      #if (__VDC && __ABS_CORNER_BRAKING_IMPROVE)  
        if((WL->Hold_Duty_Comp_flag==1) || (WL->Pres_Rise_Defer_flag==1)
          || (WL->lcabsu1CornerRearOutHold==1))	
      #else
        if((WL->Hold_Duty_Comp_flag==1) || (WL->Pres_Rise_Defer_flag==1)) 
      #endif  	
        {
            DctDeltaP=2;
        }
       #if __PRES_RISE_DEFER_FOR_UR
        else if(WL->lcabsu1LimitURPresRise==1)
        {
            DctDeltaP=3;
        }
       #endif
        else if((WL->s0_reapply_counter==0) && (WL->LFC_built_reapply_flag==0))
        {
            DctDeltaP=1;
        }
        else { }
    }
    return DctDeltaP;
}

void LCABS_vCheckMSLUnstableReapply(struct W_STRUCT*pNBASE, struct W_STRUCT*pBASE)
{
    int8_t SL_Unstable_Rise_Ref;

    if(pNBASE->Unstable_Rise_Set_Cycle==0)
    {
        pNBASE->SL_Unstable_Rise_flg=1;
        pNBASE->Reapply_Accel_hold_timer=L_TIME_20MS;
    }
    else
    {
        if(pNBASE->Reapply_Accel_flag==1)
        {
            if(pBASE->Unst_Reapply_Scan_Ref > pNBASE->Unst_Reapply_Scan_Ref)
            {
                SL_Unstable_Rise_Ref=pNBASE->Unst_Reapply_Scan_Ref;
            }
            else
            {
                SL_Unstable_Rise_Ref=pBASE->Unst_Reapply_Scan_Ref;
            }

            if(pNBASE->Unstable_reapply_counter < (SL_Unstable_Rise_Ref-pBASE->Unstable_reapply_counter))
            {
                pNBASE->SL_Unstable_Rise_flg=1;
                pNBASE->Reapply_Accel_hold_timer=L_TIME_20MS;
            }
            else
            {
                pNBASE->SL_Unstable_Rise_flg=0;

              /* end */
                if(pNBASE->Reapply_Accel_hold_timer >= L_1SCAN)
                {
                    pNBASE->Unstable_reapply_counter = pNBASE->Unst_Reapply_Scan_Ref;

                    pNBASE->Reapply_Accel_hold_timer = pNBASE->Reapply_Accel_hold_timer - L_1SCAN;
                    pNBASE->Forced_Hold_After_Unst_Reapply=1;

                }
                else
                {
                    pNBASE->Reapply_Accel_hold_timer=0;
                    pNBASE->Reapply_Accel_flag=0;
                }
              /* end */
            }
        }
        else
        {
            pNBASE->SL_Unstable_Rise_flg=0; /* gap */
        }
    }

    if(pNBASE->SL_Unstable_Rise_flg==1)
    {
        pNBASE->SL_Unstable_Rise_Cycle=1;
    }
    else
    {
        pNBASE->SL_Unstable_Rise_Cycle=0;
    }
}

  #if __MP_COMP
static void LCABS_vDecideUNSTABLERiseTime(void)
{
    int16_t delta_press;
    
    if(((lcabsu1ValidBrakeSensor==1) && (ABS_DURING_MPRESS_FLG==1)))
    {
        if(STABIL_wl==0) {
            if(WL->Estimated_Press_Lower > WL->s16_Estimated_Active_Press) {
                WL->Estimated_Press_Lower = WL->s16_Estimated_Active_Press;
            }
            delta_press = WL->Estimated_Press_SKID - WL->Estimated_Press_Lower;

            if(REAR_WHEEL_wl==0) {
                WL->Unsable_Rise_time = (int8_t)(delta_press/100);
                if((delta_press%100) >= 70) {WL->Unsable_Rise_time += 1;}
            }
            else {
                WL->Unsable_Rise_time = (int8_t)(delta_press/120);
                if((delta_press%120) >= 80) {WL->Unsable_Rise_time += 1;}
            }
            if(FSF_wl==1) {
                if(WL->Estimated_Press_Lower<=MPRESS_10BAR) {
                    WL->Unsable_Rise_time = (int8_t)(((int16_t)WL->Unsable_Rise_time*4)/5);
                }
                else
                {
                    WL->Unsable_Rise_time = (int8_t)(((int16_t)WL->Unsable_Rise_time*5)/6);
                }
            }                

            WL->Unsable_Rise_time = (int8_t)LCABS_s16LimitMinMax((int16_t)WL->Unsable_Rise_time,(int16_t)U8_1ST_CYCLE_UR_RISE_SCAN_MIN,(int16_t)U8_1ST_CYCLE_UR_RISE_SCAN_MAX);
        }
        else {WL->Estimated_Press_Lower = WL->s16_Estimated_Active_Press;}
    }
    else {WL->Unsable_Rise_time = (int8_t)U8_1ST_CYCLE_UR_RISE_SCAN_MIN;}
}
  #endif
  
#if __PRES_RISE_DEFER_FOR_UR
static void LCABS_vDecideURPresLimitation(void)
{
    if(REAR_WHEEL_wl==0)
    {
        if(LEFT_WHEEL_wl==1)
        {
            LCABS_vDecURPresLimitationF(&FL,&FR);
        }
        else
        {
            LCABS_vDecURPresLimitationF(&FR,&FL);
        }
    }
}

static void LCABS_vDecURPresLimitationF(struct W_STRUCT *FI,struct W_STRUCT *FO)
{    
    if((FI->LFC_zyklus_z<=0)||(AFZ_OK==0))
    {
        if((FI->Reapply_Accel_flag==1) && (FI->Reapply_Accel_cycle==1))
        {
            if((FO->state==UNSTABLE)||(FO->Unstable_reapply_counter > Min_dP_risetime))
            {
              #if __PRESS_EST && __CALC_EFFECTIVE_DUMPSCANS
                tempB5=(FO->lcabsu8EffectiveDumpScans-FO->Unstable_reapply_counter-FO->lcabss8URScansBfUnstDump);
                tempB4=(FI->lcabsu8EffectiveDumpScans-FI->Unstable_reapply_counter-FI->lcabss8URScansBfUnstDump);
              #else
                tempB5=(FO->LFC_dump_counter-FO->Unstable_reapply_counter-FO->lcabss8URScansBfUnstDump);
                tempB4=(FI->LFC_dump_counter-FI->Unstable_reapply_counter-FI->lcabss8URScansBfUnstDump);
              #endif
                
                if(FI->lcabss8LimitURPresRiseCnt>0)
                {
                    tempB5=tempB5-3; /* Rise-(10scan Hold)-Rise시 3scan Rise 허용, 이후 다시 hold */
                }
                
                if(FI->lcabsu1LimitURPresRise==0)
                {
                    if((tempB5-tempB4)>=5)
                    {
                        FI->lcabsu1LimitURPresRise=1;
                        FI->lcabss8LimitURPresRiseCnt=0;
                    }
                    else { }
                }
                else
                {
                    if((tempB5-tempB4)<=0)
                    {
                        FI->lcabsu1LimitURPresRise=0;
                        FI->lcabss8LimitURPresRiseCnt=0;
                    }
                    else 
                    {
                        if(FI->lcabss8LimitURPresRiseCnt >= 8)
                        {
                            FI->lcabsu1LimitURPresRise=0;
                        }
                        else
                        {
                            FI->lcabss8LimitURPresRiseCnt=FI->lcabss8LimitURPresRiseCnt+1;
                        }
                    }
                }
            
                /*
                if(FI->lcabsu1IncURTime==0)
                {
                    if(((tempB5-tempB4)<=(-5))&&((tempB5-FI->Unst_Reapply_Scan_Ref)<=(-5)))
                    {
                        FI->lcabsu1IncURTime=1;
                    }
                    else { }
                }
                else
                {
                    if(((tempB5-tempB4)>3)&&((tempB5-FI->Unst_Reapply_Scan_Ref)>3))
                    {
                        FI->lcabsu1IncURTime=0;
                    }
                    else { }
                }
                */
            }
            else
            {
                FI->lcabsu1LimitURPresRise=0;
                FI->lcabss8LimitURPresRiseCnt=0;
                /*FI->lcabsu1IncURTime=0;*/
            }
        }
        else
        {
            FI->lcabsu1LimitURPresRise=0;
            FI->lcabss8LimitURPresRiseCnt=0;
            /*FI->lcabsu1IncURTime=0;*/
        }
    }
    else
    {
        FI->lcabsu1LimitURPresRise=0;
        FI->lcabss8LimitURPresRiseCnt=0;
        /*FI->lcabsu1IncURTime=0;*/
    }
}
#endif
    
#endif

int8_t LCABS_s8GetParamFromRef(uint8_t input, uint8_t Reference1, uint8_t Reference2, uint8_t Reference3, uint8_t OutParam1, uint8_t OutParam2, uint8_t OutParam3, uint8_t OutParamMax)
{
    if(input<=Reference1)
    {
        return (int8_t)OutParam1;
    }
    else if(input<=Reference2)
    {
        return (int8_t)OutParam2;
    }
    else if(input<=Reference3)
    {
        return (int8_t)OutParam3;
    }
    else
    {
        return (int8_t)OutParamMax;
    }
}

int16_t LCABS_s16GetParamFromRef(int16_t input, int16_t Reference1, int16_t Reference2, int16_t Reference3, int16_t OutParam1, int16_t OutParam2, int16_t OutParam3, int16_t OutParamMax)
{
    int16_t s16GetParameter;

    if(input<=Reference1)
    {
        s16GetParameter = OutParam1;
    }
    else if(input<=Reference2)
    {
        s16GetParameter = OutParam2;
    }
    else if(input<=Reference3)
    {
        s16GetParameter = OutParam3;
    }
    else
    {
        s16GetParameter = OutParamMax;
    }
    return s16GetParameter;
}

void LCABS_vDesignatePulseDownPar(void)
{
  #if (__PRESS_EST && __EST_MU_FROM_SKID)
    LCABS_vGet1stCycMuFromPress();
  #endif
               
    if(REAR_WHEEL_wl==0) {LCABS_vDesignatePulseDownParF();}
    else {LCABS_vDesignatePulseDownParR();}

  if(ABS_fz==1)
  {
    /*    First cycle deep slip     */
    if((FSF_wl==1) && (WL->SPECIAL_1st_Pulsedown_need>=SPECIAL_FOR_MED) &&
       (WL->s_diff<S16_SDIFF_0G0) && (WL->rel_lam<(-LAM_40P)) && ((WL->LFC_dump_counter+WL->LFC_unstable_hold_counter)>=L_TIME_210MS)) { /* 검토*/
        Slip_Gain=Slip_Gain*2;
    }
    else if((FSF_wl==1) && (WL->SPECIAL_1st_Pulsedown_need<SPECIAL_FOR_MED) && (WL->s_diff<S16_SDIFF_0G0)) {
        Slip_Gain=Slip_Gain/2;
    }
    else if(WL->s_diff>=S16_SDIFF_0G5) {
        Slip_Gain=Slip_Gain/3;
    }
    else { }

  #if __CASCADING_WEAKEN
    if((AFZ_OK==1) && (afz>-AFZ_0G1)) {
        if(vref>=VREF_20_KPH) {Decel_Gain=Decel_Gain*5; Slip_Gain=Slip_Gain*2;}
        else if(vref>=VREF_10_KPH) {Decel_Gain=Decel_Gain*3; Slip_Gain=Slip_Gain;}
        else {Decel_Gain=Decel_Gain*2; Slip_Gain=Slip_Gain;}
    }
  #endif

  #if __EPB_INTERFACE
    if(lcu1EpbActiveFlg==1)
    {
    	if(((AFZ_OK==0) || (FSF_wl==1)) && (REAR_WHEEL_wl==1) && (WL->s_diff<S16_SDIFF_0G0))
    	{
    		if( ((LEFT_WHEEL_wl==1) && (FL.SPECIAL_1st_Pulsedown_need>=SPECIAL_FOR_MED))
    		 || ((LEFT_WHEEL_wl==0) && (FR.SPECIAL_1st_Pulsedown_need>=SPECIAL_FOR_MED)) )
    		{
    			Slip_Gain=LCABS_s16Interpolation2P(vref,VREF_40_KPH,VREF_80_KPH, Slip_Gain, (Slip_Gain*3));
    		}
    		else
    		{
    			Slip_Gain=LCABS_s16Interpolation2P(vref,VREF_50_KPH,VREF_80_KPH, Slip_Gain, (Slip_Gain*2));
    		}
			if(Slip_Gain > S16_Slip_Gain_LMu_60_100kph_R) {Slip_Gain = S16_Slip_Gain_LMu_60_100kph_R;}
    	}
    }
  #endif  
  }
}

void LCABS_vDesignatePulseDownParF(void)
{

    int16_t  s16DecelGainLowMu, s16SlipGainLowMu;    

    int16_t Ref_Mu;

  #if !__SPECIAL_1st_PULSEDOWN
    if((FSF_wl==1)||(AFZ_OK==0)) {
        if(vref < (int16_t)VREF_20_KPH) {
            Slip_Gain=(int16_t)S16_Slip_Gain_BFAFZOK_B_20_F;
            Decel_Gain=(int16_t)S16_Decel_Gain_BFAFZOK_B_20_F;
        }
        else if(vref < (int16_t)VREF_40_KPH) {
            Slip_Gain=(int16_t)S16_Slip_Gain_BFAFZOK_20_40_F;
            Decel_Gain=(int16_t)S16_Decel_Gain_BFAFZOK_20_40_F;
        }
        else if(vref < (int16_t)VREF_60_KPH) {
            Slip_Gain=(int16_t)S16_Slip_Gain_BFAFZOK_40_60_F;
            Decel_Gain=(int16_t)S16_Decel_Gain_BFAFZOK_40_60_F;
        }
        else if(vref < (int16_t)VREF_100_KPH) {
            Slip_Gain=(int16_t)S16_Slip_Gain_BFAFZOK_60_100_F;
            Decel_Gain=(int16_t)S16_Decel_Gain_BFAFZOK_60_100_F;
        }
        else if(vref < (int16_t)VREF_150_KPH) {
            Slip_Gain=(int16_t)S16_Slip_Gain_BFAFZOK_100_150_F;
            Decel_Gain=(int16_t)S16_Decel_Gain_BFAFZOK_100_150_F;
        }
        else {
            Slip_Gain=(int16_t)S16_Slip_Gain_BFAFZOK_Ab_150_F;
            Decel_Gain=(int16_t)S16_Decel_Gain_BFAFZOK_Ab_150_F;
        }
    }
    else {
        if(vref>=VREF_100_KPH) {tempB6=AFZ_0G12;}         /* Added due to BASALT cascading 030722 */
        else if(vref>=VREF_80_KPH) {tempB6=AFZ_0G1;}
        else if(vref>=VREF_50_KPH) {tempB6=AFZ_0G05;}
        else {tempB6=AFZ_0G0;}

        Ref_Mu=afz + (int16_t)tempB6;
        if((WL->LFC_n_th_deep_slip_comp_flag==1) || (WL->High_TO_Low_Suspect_flag==1)) {
            Ref_Mu = -(int16_t)U8_PULSE_LOW_MU;
        }
  #else
              
    if((FSF_wl==1)||((AFZ_OK==0)&&(WL->LFC_initial_deep_slip_comp_flag==0))){
      #if !(__PRESS_EST && __EST_MU_FROM_SKID)
        if (WL->SPECIAL_1st_Pulsedown_need==SPECIAL_FOR_LOW){
            Ref_Mu=-(int16_t)U8_PULSE_LOW_MU;
        }
        else if (WL->SPECIAL_1st_Pulsedown_need==SPECIAL_FOR_MED){
            Ref_Mu=-(int16_t)U8_PULSE_UNDER_LOW_MED_MU;
        }
        else {
            Ref_Mu=-(int16_t)U8_PULSE_HIGH_MU;
        }
      #else /* !(__PRESS_EST && __EST_MU_FROM_SKID)*/
        if(WL->lcabsu8PossibleMuFromSkid > 0)
        {
            Ref_Mu=-(int16_t)WL->lcabsu8PossibleMuFromSkid; /* code optimization */
        }
        else 
        {
            if (WL->SPECIAL_1st_Pulsedown_need==SPECIAL_FOR_LOW){
                Ref_Mu=-(int16_t)U8_PULSE_LOW_MU;
            }
            else if (WL->SPECIAL_1st_Pulsedown_need==SPECIAL_FOR_MED){
                Ref_Mu=-(int16_t)U8_PULSE_UNDER_LOW_MED_MU;
            }
            else {
                Ref_Mu=-(int16_t)U8_PULSE_HIGH_MU;
            }
        }
      #endif  /* !(__PRESS_EST && __EST_MU_FROM_SKID)*/
      
        if(vref<(int16_t)VREF_10_KPH) {
          #if __VDC && __HDC && (__HDC_ABS_IMPROVE==ENABLE)
            if(lcu1HdcActiveFlg==1)
            {
                s16SlipGainLowMu  = (int16_t)S8_Slip_Gain_LMu_B_10k_F_HDC;    
                s16DecelGainLowMu = (int16_t)S8_Decel_Gain_LMu_B_10k_F_HDC;
            }
            else
          #endif
            {
                s16SlipGainLowMu  = S16_Slip_Gain_LMu_B_10kph_F;    
                s16DecelGainLowMu = S16_Decel_Gain_LMu_B_10kph_F;
            }            
        	
            Slip_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)s16SlipGainLowMu,(int16_t)S16_Slip_Gain_LMMu_B_10kph_F, (int16_t)S16_Slip_Gain_MMu_B_10kph_F,(int16_t)S16_Slip_Gain_BFAFZOK_B_10_F);
            Decel_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)s16DecelGainLowMu,(int16_t)S16_Decel_Gain_LMMu_B_10kph_F,(int16_t)S16_Decel_Gain_MMu_B_10kph_F,(int16_t)S16_Decel_Gain_BFAFZOK_B_10_F);
        }
        else if(vref<(int16_t)VREF_20_KPH) {
          #if __VDC && __HDC && (__HDC_ABS_IMPROVE==ENABLE)
            if(lcu1HdcActiveFlg==1)
            {
                s16SlipGainLowMu  = (int16_t)S8_Slip_Gain_LMu_10_20k_F_HDC;    
                s16DecelGainLowMu = (int16_t)S8_Decel_Gain_LMu_10_20k_F_HDC;
            }
            else
          #endif
            {
                s16SlipGainLowMu  = S16_Slip_Gain_LMu_10_20kph_F;    
                s16DecelGainLowMu = S16_Decel_Gain_LMu_10_20kph_F;
            }            

            Slip_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)s16SlipGainLowMu,(int16_t)S16_Slip_Gain_LMMu_10_20kph_F, (int16_t)S16_Slip_Gain_MMu_10_20kph_F,(int16_t)S16_Slip_Gain_BFAFZOK_10_20_F);
            Decel_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)s16DecelGainLowMu,(int16_t)S16_Decel_Gain_LMMu_10_20kph_F,(int16_t)S16_Decel_Gain_MMu_10_20kph_F,(int16_t)S16_Decel_Gain_BFAFZOK_10_20_F);
        }
        else if(vref<(int16_t)VREF_40_KPH) {
            Slip_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Slip_Gain_LMu_20_40kph_F,(int16_t)S16_Slip_Gain_LMMu_20_40kph_F, (int16_t)S16_Slip_Gain_MMu_20_40kph_F,(int16_t)S16_Slip_Gain_BFAFZOK_20_40_F);
            Decel_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Decel_Gain_LMu_20_40kph_F,(int16_t)S16_Decel_Gain_LMMu_20_40kph_F,(int16_t)S16_Decel_Gain_MMu_20_40kph_F,(int16_t)S16_Decel_Gain_BFAFZOK_20_40_F);
        }
        else if(vref<(int16_t)VREF_60_KPH) {
            Slip_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Slip_Gain_LMu_40_60kph_F,(int16_t)S16_Slip_Gain_LMMu_40_60kph_F, (int16_t)S16_Slip_Gain_MMu_40_60kph_F,(int16_t)S16_Slip_Gain_BFAFZOK_40_60_F);
            Decel_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Decel_Gain_LMu_40_60kph_F,(int16_t)S16_Decel_Gain_LMMu_40_60kph_F,(int16_t)S16_Decel_Gain_MMu_40_60kph_F,(int16_t)S16_Decel_Gain_BFAFZOK_40_60_F);
        }
        else if(vref<(int16_t)VREF_100_KPH) {
            Slip_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Slip_Gain_LMu_60_100kph_F,(int16_t)S16_Slip_Gain_LMMu_60_100kph_F, (int16_t)S16_Slip_Gain_MMu_60_100kph_F,(int16_t)S16_Slip_Gain_BFAFZOK_60_100_F);
            Decel_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Decel_Gain_LMu_60_100kph_F,(int16_t)S16_Decel_Gain_LMMu_60_100kph_F,(int16_t)S16_Decel_Gain_MMu_60_100kph_F,(int16_t)S16_Decel_Gain_BFAFZOK_60_100_F);
        }
        else if(vref<(int16_t)VREF_150_KPH) {
            Slip_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Slip_Gain_LMu_100_150kph_F,(int16_t)S16_Slip_Gain_LMMu_100_150kph_F, (int16_t)S16_Slip_Gain_MMu_100_150kph_F,(int16_t)S16_Slip_Gain_BFAFZOK_100_150_F);
            Decel_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Decel_Gain_LMu_100_150kph_F,(int16_t)S16_Decel_Gain_LMMu_100_150kph_F,(int16_t)S16_Decel_Gain_MMu_100_150kph_F,(int16_t)S16_Decel_Gain_BFAFZOK_100_150_F);
        }
        else {
            Slip_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Slip_Gain_LMu_Ab_150kph_F,(int16_t)S16_Slip_Gain_LMMu_Ab_150kph_F, (int16_t)S16_Slip_Gain_MMu_Ab_150kph_F,(int16_t)S16_Slip_Gain_BFAFZOK_Ab_150_F);
            Decel_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Decel_Gain_LMu_Ab_150kph_F,(int16_t)S16_Decel_Gain_LMMu_Ab_150kph_F,(int16_t)S16_Decel_Gain_MMu_Ab_150kph_F,(int16_t)S16_Decel_Gain_BFAFZOK_Ab_150_F);
        }
    }
    else {
        if(vref>=VREF_100_KPH) {tempB6=AFZ_0G12;}         /* Added due to BASALT cascading 030722 */
        else if(vref>=VREF_80_KPH) {tempB6=AFZ_0G1;}
        else if(vref>=VREF_50_KPH) {tempB6=AFZ_0G05;}
        else {tempB6=AFZ_0G0;}

        Ref_Mu=afz + (int16_t)tempB6;

        if((WL->LFC_n_th_deep_slip_comp_flag==1) || (WL->High_TO_Low_Suspect_flag==1)) {
            Ref_Mu = -(int16_t)U8_PULSE_LOW_MU;
        }
        else if((AFZ_OK==0)&&(WL->LFC_initial_deep_slip_comp_flag==1)) {
            Ref_Mu = -(int16_t)U8_PULSE_LOW_MU;
        }
        else { }

        if((LFC_Split_flag==1) && (UN_GMA_wl==1) && (CERTAIN_SPLIT_flag==1) && (LOW_to_HIGH_suspect==0))
        {
          #if __PRESS_EST_ABS
            if((WL->LOW_MU_SUSPECT_by_IndexMu==1) || ((WL->IndexMu<=IndexBasalt) && (FSF_wl==0)))
            {
                Ref_Mu = -(int16_t)U8_PULSE_LOW_MU;
            }
          #endif
          #if __REDUCE_SPLIT_LOWSIDE_SLIP
           #if (__PRESS_EST && __EST_MU_FROM_SKID)
            if(WL->lcabsu8PossibleMuFromSkid > 0)
            {
                if(WL->lcabsu8PossibleMuFromSkid <= U8_PULSE_LOW_MU)
                {
                    Ref_Mu=-(int16_t)U8_PULSE_LOW_MU;
                }
                else if(WL->lcabsu8PossibleMuFromSkid <= U8_PULSE_UNDER_LOW_MED_MU)
                {
                    Ref_Mu=-(int16_t)U8_PULSE_UNDER_LOW_MED_MU;
                }
                else { }
            }
           #endif
          #endif
        }
        else { }
/*    }   */
  #endif

        if(vref<(int16_t)VREF_10_KPH) {
            Slip_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Slip_Gain_LMu_B_10kph_F,(int16_t)S16_Slip_Gain_LMMu_B_10kph_F, (int16_t)S16_Slip_Gain_MMu_B_10kph_F,(int16_t)S16_Slip_Gain_HMu_B_10kph_F);
            Decel_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Decel_Gain_LMu_B_10kph_F,(int16_t)S16_Decel_Gain_LMMu_B_10kph_F,(int16_t)S16_Decel_Gain_MMu_B_10kph_F,(int16_t)S16_Decel_Gain_HMu_B_10kph_F);
        }
        else if(vref<(int16_t)VREF_20_KPH) {
            Slip_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Slip_Gain_LMu_10_20kph_F,(int16_t)S16_Slip_Gain_LMMu_10_20kph_F, (int16_t)S16_Slip_Gain_MMu_10_20kph_F,(int16_t)S16_Slip_Gain_HMu_10_20kph_F);
            Decel_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Decel_Gain_LMu_10_20kph_F,(int16_t)S16_Decel_Gain_LMMu_10_20kph_F,(int16_t)S16_Decel_Gain_MMu_10_20kph_F,(int16_t)S16_Decel_Gain_HMu_10_20kph_F);
        }
        else if(vref<(int16_t)VREF_40_KPH) {
            Slip_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Slip_Gain_LMu_20_40kph_F,(int16_t)S16_Slip_Gain_LMMu_20_40kph_F, (int16_t)S16_Slip_Gain_MMu_20_40kph_F,(int16_t)S16_Slip_Gain_HMu_20_40kph_F);
            Decel_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Decel_Gain_LMu_20_40kph_F,(int16_t)S16_Decel_Gain_LMMu_20_40kph_F,(int16_t)S16_Decel_Gain_MMu_20_40kph_F,(int16_t)S16_Decel_Gain_HMu_20_40kph_F);
        }
        else if(vref<(int16_t)VREF_60_KPH) {
            Slip_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Slip_Gain_LMu_40_60kph_F,(int16_t)S16_Slip_Gain_LMMu_40_60kph_F, (int16_t)S16_Slip_Gain_MMu_40_60kph_F,(int16_t)S16_Slip_Gain_HMu_40_60kph_F);
            Decel_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Decel_Gain_LMu_40_60kph_F,(int16_t)S16_Decel_Gain_LMMu_40_60kph_F,(int16_t)S16_Decel_Gain_MMu_40_60kph_F,(int16_t)S16_Decel_Gain_HMu_40_60kph_F);
        }
        else if(vref<(int16_t)VREF_100_KPH) {
            Slip_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Slip_Gain_LMu_60_100kph_F,(int16_t)S16_Slip_Gain_LMMu_60_100kph_F, (int16_t)S16_Slip_Gain_MMu_60_100kph_F,(int16_t)S16_Slip_Gain_HMu_60_100kph_F);
            Decel_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Decel_Gain_LMu_60_100kph_F,(int16_t)S16_Decel_Gain_LMMu_60_100kph_F,(int16_t)S16_Decel_Gain_MMu_60_100kph_F,(int16_t)S16_Decel_Gain_HMu_60_100kph_F);
        }
        else if(vref<(int16_t)VREF_150_KPH) {
            Slip_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Slip_Gain_LMu_100_150kph_F,(int16_t)S16_Slip_Gain_LMMu_100_150kph_F, (int16_t)S16_Slip_Gain_MMu_100_150kph_F,(int16_t)S16_Slip_Gain_HMu_100_150kph_F);
            Decel_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Decel_Gain_LMu_100_150kph_F,(int16_t)S16_Decel_Gain_LMMu_100_150kph_F,(int16_t)S16_Decel_Gain_MMu_100_150kph_F,(int16_t)S16_Decel_Gain_HMu_100_150kph_F);
        }
        else {
            Slip_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Slip_Gain_LMu_Ab_150kph_F,(int16_t)S16_Slip_Gain_LMMu_Ab_150kph_F, (int16_t)S16_Slip_Gain_MMu_Ab_150kph_F,(int16_t)S16_Slip_Gain_HMu_Ab_150kph_F);
            Decel_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Decel_Gain_LMu_Ab_150kph_F,(int16_t)S16_Decel_Gain_LMMu_Ab_150kph_F,(int16_t)S16_Decel_Gain_MMu_Ab_150kph_F,(int16_t)S16_Decel_Gain_HMu_Ab_150kph_F);
        }

      #if __SPLIT_VERIFY
        if((LFC_Split_flag==1)&&(WL->PULSE_DOWN_Forbid_flag==0))
      #else
        if(LFC_Split_flag==1)
      #endif
        {
            if(((LEFT_WHEEL_wl==1)&&(MSL_BASE_rr==1)) || ((LEFT_WHEEL_wl==0)&&(MSL_BASE_rl==1))) {
                if(vref < (int16_t)VREF_10_KPH) {
                    Slip_Gain=(int16_t)S16_Slip_Gain_SpHMu_B_10kph_F;
                    Decel_Gain=(int16_t)S16_Decel_Gain_SpHMu_B_10kph_F;
                }
                else if(vref < (int16_t)VREF_20_KPH) {
                    Slip_Gain=(int16_t)S16_Slip_Gain_SpHMu_10_20kph_F;
                    Decel_Gain=(int16_t)S16_Decel_Gain_SpHMu_10_20kph_F;
                }
                else if(vref < (int16_t)VREF_40_KPH) {
                    Slip_Gain=(int16_t)S16_Slip_Gain_SpHMu_20_40kph_F;
                    Decel_Gain=(int16_t)S16_Decel_Gain_SpHMu_20_40kph_F;
                }
                else if(vref < (int16_t)VREF_60_KPH) {
                    Slip_Gain=(int16_t)S16_Slip_Gain_SpHMu_40_60kph_F;
                    Decel_Gain=(int16_t)S16_Decel_Gain_SpHMu_40_60kph_F;
                }
                else if(vref < (int16_t)VREF_100_KPH) {
                    Slip_Gain=(int16_t)S16_Slip_Gain_SpHMu_60_100kph_F;
                    Decel_Gain=(int16_t)S16_Decel_Gain_SpHMu_60_100kph_F;
                }
                else if(vref < (int16_t)VREF_150_KPH) {
                    Slip_Gain=(int16_t)S16_Slip_Gain_SpHMu_100_150_F;
                    Decel_Gain=(int16_t)S16_Decel_Gain_SpHMu_100_150_F;
                }
                else {
                    Slip_Gain=(int16_t)S16_Slip_Gain_SpHMu_Ab_150kph_F;
                    Decel_Gain=(int16_t)S16_Decel_Gain_SpHMu_Ab_150kph_F;
                }
            }
        }
/*  #if !__SPECIAL_1st_PULSEDOWN */
    }
/*  #endif */
/****************************************************************/
  #if __ROUGH_ROAD_DUMP_IMPROVE
    if((Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1))
    {
        Decel_Gain = (Decel_Gain/3);
    }
    else if(WL->Rough_road_detect_flag2==1)
    {
        Decel_Gain = (Decel_Gain/2);
    }
    else if(WL->Rough_road_detect_flag1==1)
    {
        Decel_Gain = (Decel_Gain*3/4);
    }
    else { }
  #endif      
/****************************************************************/
      
}


int16_t LCABS_s16Interpolation2P(int16_t input,int16_t mu_1,int16_t mu_2,int16_t gain_1,int16_t gain_2)
{
/*
    int16_t temp_inter_0=0;
    int16_t temp_inter_1=0;
    int16_t temp_inter_2=0;
    int16_t temp_inter_fin=0;

    temp_inter_2=(gain_2-gain_1); temp_inter_1=(input-mu_1); temp_inter_0=(mu_2-mu_1);
    if(temp_inter_0==0) {temp_inter_fin =  gain_1;}
    else {temp_inter_fin = gain_1+(int16_t)((temp_inter_2*temp_inter_1)/temp_inter_0);}
    return temp_inter_fin;
*/
    
    if(mu_1 > mu_2)
    {
        lcabss16temp0 = mu_1;
        lcabss16temp1 = mu_2;
        lcabss16temp2 = gain_1;
        lcabss16temp3 = gain_2;
    }
    else
    {
        lcabss16temp0 = mu_2;
        lcabss16temp1 = mu_1;
        lcabss16temp2 = gain_2;
        lcabss16temp3 = gain_1;
    }
    
    if(input >= lcabss16temp0)
    {
        lcabss16temp4 = lcabss16temp2;
    }
    else if(input <= lcabss16temp1)
    {
        lcabss16temp4 = lcabss16temp3;
    }
    else
    {
        lcabss16temp4 = lcabss16temp3 + (int16_t)(((int32_t)(lcabss16temp2-lcabss16temp3) * (input-lcabss16temp1)) / (lcabss16temp0-lcabss16temp1));
    }
        
    return lcabss16temp4;
}

void LCABS_vDesignatePulseDownParR(void)
{
    int16_t s16DecelGainLowMu, s16SlipGainLowMu;
    int16_t Ref_Mu;
    Ref_Mu=afz;
    
  #if   __MGH40_EBD
    if(EBD_wl==1) {
        if(vref<(int16_t)VREF_20_KPH) {
            Slip_Gain=S16_Slip_Gain_EBD_B_20kph_R;
            Decel_Gain=S16_Decel_Gain_EBD_B_20kph_R;
        }
        else if(vref<(int16_t)VREF_40_KPH) {
            Slip_Gain=S16_Slip_Gain_EBD_20_40kph_R;
            Decel_Gain=S16_Decel_Gain_EBD_20_40kph_R;
        }
        else if(vref<(int16_t)VREF_60_KPH) {
            Slip_Gain=S16_Slip_Gain_EBD_40_60kph_R;
            Decel_Gain=S16_Decel_Gain_EBD_40_60kph_R;
        }
        else if(vref<(int16_t)VREF_100_KPH) {
            Slip_Gain=S16_Slip_Gain_EBD_60_100kph_R;
            Decel_Gain=S16_Decel_Gain_EBD_60_100kph_R;
        }
        else if(vref<(int16_t)VREF_150_KPH) {
            Slip_Gain=S16_Slip_Gain_EBD_100_150kph_R;
            Decel_Gain=S16_Decel_Gain_EBD_100_150kph_R;
        }
        else {
            Slip_Gain=S16_Slip_Gain_EBD_Ab_150kph_R;
            Decel_Gain=S16_Decel_Gain_EBD_Ab_150kph_R;
        }
      #if ((__CBC==0) && (__BEND_DETECT_using_SIDEVREF==1))
        if(((BEND_DETECT_RIGHT==1)&&(LEFT_WHEEL_wl==0))||((BEND_DETECT_LEFT==1)&&(LEFT_WHEEL_wl==1)))
         /* inside wheel */
        {
            Decel_Gain = Decel_Gain*2;
        }
      #endif
    }
    else {
  #endif
      #if !__SPECIAL_1st_PULSEDOWN
        if((FSF_wl==1)||(AFZ_OK==0)) {
            if(vref < (int16_t)VREF_20_KPH) {
                Slip_Gain=(int16_t)S16_Slip_Gain_BFAFZOK_B_20_R;
                Decel_Gain=(int16_t)S16_Decel_Gain_BFAFZOK_B_20_R;
            }
            else if(vref < (int16_t)VREF_40_KPH) {
                Slip_Gain=(int16_t)S16_Slip_Gain_BFAFZOK_20_40_R;
                Decel_Gain=(int16_t)S16_Decel_Gain_BFAFZOK_20_40_R;
            }
            else if(vref < (int16_t)VREF_60_KPH) {
                Slip_Gain=(int16_t)S16_Slip_Gain_BFAFZOK_40_60_R;
                Decel_Gain=(int16_t)S16_Decel_Gain_BFAFZOK_40_60_R;
            }
            else if(vref < (int16_t)VREF_100_KPH) {
                Slip_Gain=(int16_t)S16_Slip_Gain_BFAFZOK_60_100_R;
                Decel_Gain=(int16_t)S16_Decel_Gain_BFAFZOK_60_100_R;
            }
            else if(vref < (int16_t)VREF_150_KPH) {
                Slip_Gain=(int16_t)S16_Slip_Gain_BFAFZOK_100_150_R;
                Decel_Gain=(int16_t)S16_Decel_Gain_BFAFZOK_100_150_R;
            }
            else {
                Slip_Gain=(int16_t)S16_Slip_Gain_BFAFZOK_Ab_150_R;
                Decel_Gain=(int16_t)S16_Decel_Gain_BFAFZOK_Ab_150_R;
            }
        }
        else {
            if(vref>=VREF_100_KPH) {tempB6=AFZ_0G12;}         /* Added due to BASALT cascading 030722 */
            else if(vref>=VREF_80_KPH) {tempB6=AFZ_0G1;}
            else if(vref>=VREF_50_KPH) {tempB6=AFZ_0G05;}
            else {tempB6=AFZ_0G0;}
            Ref_Mu = afz + (int16_t)tempB6;

            if((WL->LFC_n_th_deep_slip_comp_flag==1) ||
               ((FL.High_TO_Low_Suspect_flag==1) && (FR.High_TO_Low_Suspect_flag==1))) {
                Ref_Mu = -(int16_t)U8_PULSE_LOW_MU;
            }

          #if   __CASCADING_WEAKEN
            if(LOW_to_HIGH_suspect==0)
             {
                if(AFZ_LOW_to_HIGH_flag==1) {Ref_Mu = -(int16_t)U8_LOW_MU;}
                else if(AFZ_LOW_to_MEDIUM_flag==1) {Ref_Mu = -(int16_t)U8_LOW_MU;}
                else if(AFZ_MEDIUM_to_HIGH_flag==1) {Ref_Mu = -(int16_t)U8_HIGH_MU;}
                else { }
            }
            else { }
          #endif

      #else     /* __SPECIAL_1st_PULSEDOWN */

        if ((FSF_wl==1)||((AFZ_OK==0)&&(WL->LFC_initial_deep_slip_comp_flag==0)))
        {
          #if !(__PRESS_EST && __EST_MU_FROM_SKID)

            if (WL->SPECIAL_1st_Pulsedown_need==SPECIAL_FOR_LOW){
                Ref_Mu=-(int16_t)U8_PULSE_LOW_MU;
            }
            else if (WL->SPECIAL_1st_Pulsedown_need==SPECIAL_FOR_MED){
                Ref_Mu=-(int16_t)U8_PULSE_UNDER_LOW_MED_MU;
            }
            else {
                Ref_Mu=-(int16_t)U8_PULSE_HIGH_MU;
            }

          #else /* !(__PRESS_EST && __EST_MU_FROM_SKID) */

            if(WL->lcabsu8PossibleMuFromSkid > 0)
            {
                Ref_Mu=-(int16_t)WL->lcabsu8PossibleMuFromSkid; /* code optimization */
            }
            else 
            {
                if (WL->SPECIAL_1st_Pulsedown_need==SPECIAL_FOR_LOW){
                    Ref_Mu=-(int16_t)U8_PULSE_LOW_MU;
                }
                else if (WL->SPECIAL_1st_Pulsedown_need==SPECIAL_FOR_MED){
                    Ref_Mu=-(int16_t)U8_PULSE_UNDER_LOW_MED_MU;
                }
                else {
                    Ref_Mu=-(int16_t)U8_PULSE_HIGH_MU;
                }
            }
          #endif /* !(__PRESS_EST && __EST_MU_FROM_SKID) */

            if(vref<(int16_t)VREF_10_KPH) {
                Slip_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Slip_Gain_LMu_B_10kph_R,(int16_t)S16_Slip_Gain_LMMu_B_10kph_R, (int16_t)S16_Slip_Gain_MMu_B_10kph_R,(int16_t)S16_Slip_Gain_BFAFZOK_B_10_R);
                Decel_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Decel_Gain_LMu_B_10kph_R,(int16_t)S16_Decel_Gain_LMMu_B_10kph_R,(int16_t)S16_Decel_Gain_MMu_B_10kph_R,(int16_t)S16_Decel_Gain_BFAFZOK_B_10_R);
            }
            else if(vref<(int16_t)VREF_20_KPH) {
                Slip_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Slip_Gain_LMu_10_20kph_R,(int16_t)S16_Slip_Gain_LMMu_10_20kph_R, (int16_t)S16_Slip_Gain_MMu_10_20kph_R,(int16_t)S16_Slip_Gain_BFAFZOK_10_20_R);
                Decel_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Decel_Gain_LMu_10_20kph_R,(int16_t)S16_Decel_Gain_LMMu_10_20kph_R,(int16_t)S16_Decel_Gain_MMu_10_20kph_R,(int16_t)S16_Decel_Gain_BFAFZOK_10_20_R);
            }
            else if(vref<(int16_t)VREF_40_KPH) {
                Slip_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Slip_Gain_LMu_20_40kph_R,(int16_t)S16_Slip_Gain_LMMu_20_40kph_R, (int16_t)S16_Slip_Gain_MMu_20_40kph_R,(int16_t)S16_Slip_Gain_BFAFZOK_20_40_R);
                Decel_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Decel_Gain_LMu_20_40kph_R,(int16_t)S16_Decel_Gain_LMMu_20_40kph_R,(int16_t)S16_Decel_Gain_MMu_20_40kph_R,(int16_t)S16_Decel_Gain_BFAFZOK_20_40_R);
            }
            else if(vref<(int16_t)VREF_60_KPH) {
                Slip_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Slip_Gain_LMu_40_60kph_R,(int16_t)S16_Slip_Gain_LMMu_40_60kph_R, (int16_t)S16_Slip_Gain_MMu_40_60kph_R,(int16_t)S16_Slip_Gain_BFAFZOK_40_60_R);
                Decel_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Decel_Gain_LMu_40_60kph_R,(int16_t)S16_Decel_Gain_LMMu_40_60kph_R,(int16_t)S16_Decel_Gain_MMu_40_60kph_R,(int16_t)S16_Decel_Gain_BFAFZOK_40_60_R);
            }
            else if(vref<(int16_t)VREF_100_KPH) {
                Slip_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Slip_Gain_LMu_60_100kph_R,(int16_t)S16_Slip_Gain_LMMu_60_100kph_R, (int16_t)S16_Slip_Gain_MMu_60_100kph_R,(int16_t)S16_Slip_Gain_BFAFZOK_60_100_R);
                Decel_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Decel_Gain_LMu_60_100kph_R,(int16_t)S16_Decel_Gain_LMMu_60_100kph_R,(int16_t)S16_Decel_Gain_MMu_60_100kph_R,(int16_t)S16_Decel_Gain_BFAFZOK_60_100_R);
            }
            else if(vref<(int16_t)VREF_150_KPH) {
                Slip_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Slip_Gain_LMu_100_150kph_R,(int16_t)S16_Slip_Gain_LMMu_100_150kph_R, (int16_t)S16_Slip_Gain_MMu_100_150kph_R,(int16_t)S16_Slip_Gain_BFAFZOK_100_150_R);
                Decel_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Decel_Gain_LMu_100_150kph_R,(int16_t)S16_Decel_Gain_LMMu_100_150kph_R,(int16_t)S16_Decel_Gain_MMu_100_150kph_R,(int16_t)S16_Decel_Gain_BFAFZOK_100_150_R);
            }
            else {
                Slip_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Slip_Gain_LMu_Ab_150kph_R,(int16_t)S16_Slip_Gain_LMMu_Ab_150kph_R, (int16_t)S16_Slip_Gain_MMu_Ab_150kph_R,(int16_t)S16_Slip_Gain_BFAFZOK_Ab_150_R);
                Decel_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Decel_Gain_LMu_Ab_150kph_R,(int16_t)S16_Decel_Gain_LMMu_Ab_150kph_R,(int16_t)S16_Decel_Gain_MMu_Ab_150kph_R,(int16_t)S16_Decel_Gain_BFAFZOK_Ab_150_R );
            }
        }
        else {
            if(vref>=VREF_100_KPH) {tempB6=AFZ_0G12;}         /* Added due to BASALT cascading 030722 */
            else if(vref>=VREF_80_KPH) {tempB6=AFZ_0G1;}
            else if(vref>=VREF_50_KPH) {tempB6=AFZ_0G05;}
            else {tempB6=AFZ_0G0;}
            Ref_Mu=afz + (int16_t)tempB6;

            if((WL->LFC_n_th_deep_slip_comp_flag==1) ||
               ((FL.High_TO_Low_Suspect_flag==1) && (FR.High_TO_Low_Suspect_flag==1))) {
                Ref_Mu = -(int16_t)U8_PULSE_LOW_MU;
            }
            else if((AFZ_OK==0)&&(WL->LFC_initial_deep_slip_comp_flag==1)) {
                Ref_Mu = -(int16_t)U8_PULSE_LOW_MU;
            }
            else if((LFC_Split_flag==1) && (MSL_BASE_wl==1) && (UN_GMA_wl==1) && (CERTAIN_SPLIT_flag==1)) { /*(LFC_Split_flag==1)*/
                Ref_Mu = -(int16_t)U8_PULSE_LOW_MU;
             #if __PRESS_EST_ABS
                if(WL->IndexMu>IndexBasalt)
                {
                    Ref_Mu = -(int16_t)U8_PULSE_UNDER_LOW_MED_MU;
                }
             #endif
             #if __REDUCE_SPLIT_LOWSIDE_SLIP
              #if (__PRESS_EST && __EST_MU_FROM_SKID)
                if(WL->lcabsu8PossibleMuFromSkid > 0)
                {
                    if(WL->lcabsu8PossibleMuFromSkid <= U8_PULSE_LOW_MU)
                    {
                        Ref_Mu=-(int16_t)U8_PULSE_LOW_MU;
                    }
                    else if(WL->lcabsu8PossibleMuFromSkid <= U8_PULSE_UNDER_LOW_MED_MU)
                    {
                        Ref_Mu=-(int16_t)U8_PULSE_UNDER_LOW_MED_MU;
                    }
                    else { }
                }
              #endif
             #endif
            }
            else { }

          #if   __CASCADING_WEAKEN
            if(LOW_to_HIGH_suspect==0)
            {
                if(AFZ_LOW_to_HIGH_flag==1) {Ref_Mu = -(int16_t)U8_LOW_MU;}
                else if(AFZ_LOW_to_MEDIUM_flag==1) {Ref_Mu = -(int16_t)U8_LOW_MU;}
                else if(AFZ_MEDIUM_to_HIGH_flag==1) {Ref_Mu = -(int16_t)U8_HIGH_MU;}
                else { }
            }
            else { }
          #endif
/*        }  */
      #endif

            if(vref<(int16_t)VREF_10_KPH) {
              #if __VDC && __HDC && (__HDC_ABS_IMPROVE==ENABLE)
                if(lcu1HdcActiveFlg==1)
                {
                    s16SlipGainLowMu  = (int16_t)S8_Slip_Gain_LMu_B_10k_R_HDC;    
                    s16DecelGainLowMu = (int16_t)S8_Decel_Gain_LMu_B_10k_R_HDC;
                }
                else
              #endif
                {
                    s16SlipGainLowMu  = S16_Slip_Gain_LMu_B_10kph_R;    
                    s16DecelGainLowMu = S16_Slip_Gain_LMu_B_10kph_R;
                }            
	        	
                Slip_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)s16SlipGainLowMu,(int16_t)S16_Slip_Gain_LMMu_B_10kph_R, (int16_t)S16_Slip_Gain_MMu_B_10kph_R,(int16_t)S16_Slip_Gain_HMu_B_10kph_R);
                Decel_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)s16DecelGainLowMu,(int16_t)S16_Decel_Gain_LMMu_B_10kph_R,(int16_t)S16_Decel_Gain_MMu_B_10kph_R,(int16_t)S16_Decel_Gain_HMu_B_10kph_R);
            }
            else if(vref<(int16_t)VREF_20_KPH) {
              #if __VDC && __HDC && (__HDC_ABS_IMPROVE==ENABLE)
                if(lcu1HdcActiveFlg==1)
                {
                    s16SlipGainLowMu  = (int16_t)S8_Slip_Gain_LMu_10_20k_R_HDC;    
                    s16DecelGainLowMu = (int16_t)S8_Decel_Gain_LMu_10_20k_R_HDC;
                }
                else
              #endif
                {
                    s16SlipGainLowMu  = S16_Slip_Gain_LMu_10_20kph_R;    
                    s16DecelGainLowMu = S16_Decel_Gain_LMu_10_20kph_R;
                }            

                Slip_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)s16SlipGainLowMu,(int16_t)S16_Slip_Gain_LMMu_10_20kph_R, (int16_t)S16_Slip_Gain_MMu_10_20kph_R,(int16_t)S16_Slip_Gain_HMu_10_20kph_R);
                Decel_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)s16DecelGainLowMu,(int16_t)S16_Decel_Gain_LMMu_10_20kph_R,(int16_t)S16_Decel_Gain_MMu_10_20kph_R,(int16_t)S16_Decel_Gain_HMu_10_20kph_R);
            }
            else if(vref<(int16_t)VREF_40_KPH) {
                Slip_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Slip_Gain_LMu_20_40kph_R,(int16_t)S16_Slip_Gain_LMMu_20_40kph_R, (int16_t)S16_Slip_Gain_MMu_20_40kph_R,(int16_t)S16_Slip_Gain_HMu_20_40kph_R);
                Decel_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Decel_Gain_LMu_20_40kph_R,(int16_t)S16_Decel_Gain_LMMu_20_40kph_R,(int16_t)S16_Decel_Gain_MMu_20_40kph_R,(int16_t)S16_Decel_Gain_HMu_20_40kph_R);
            }
            else if(vref<(int16_t)VREF_60_KPH) {
                Slip_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Slip_Gain_LMu_40_60kph_R,(int16_t)S16_Slip_Gain_LMMu_40_60kph_R, (int16_t)S16_Slip_Gain_MMu_40_60kph_R,(int16_t)S16_Slip_Gain_HMu_40_60kph_R);
                Decel_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Decel_Gain_LMu_40_60kph_R,(int16_t)S16_Decel_Gain_LMMu_40_60kph_R,(int16_t)S16_Decel_Gain_MMu_40_60kph_R,(int16_t)S16_Decel_Gain_HMu_40_60kph_R);
            }
            else if(vref<(int16_t)VREF_100_KPH) {
                Slip_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Slip_Gain_LMu_60_100kph_R,(int16_t)S16_Slip_Gain_LMMu_60_100kph_R, (int16_t)S16_Slip_Gain_MMu_60_100kph_R,(int16_t)S16_Slip_Gain_HMu_60_100kph_R);
                Decel_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Decel_Gain_LMu_60_100kph_R,(int16_t)S16_Decel_Gain_LMMu_60_100kph_R,(int16_t)S16_Decel_Gain_MMu_60_100kph_R,(int16_t)S16_Decel_Gain_HMu_60_100kph_R);
            }
            else if(vref<(int16_t)VREF_150_KPH) {
                Slip_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Slip_Gain_LMu_100_150kph_R,(int16_t)S16_Slip_Gain_LMMu_100_150kph_R, (int16_t)S16_Slip_Gain_MMu_100_150kph_R,(int16_t)S16_Slip_Gain_HMu_100_150kph_R);
                Decel_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Decel_Gain_LMu_100_150kph_R,(int16_t)S16_Decel_Gain_LMMu_100_150kph_R,(int16_t)S16_Decel_Gain_MMu_100_150kph_R,(int16_t)S16_Decel_Gain_HMu_100_150kph_R);
            }
            else {
                Slip_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Slip_Gain_LMu_Ab_150kph_R,(int16_t)S16_Slip_Gain_LMMu_Ab_150kph_R, (int16_t)S16_Slip_Gain_MMu_Ab_150kph_R,(int16_t)S16_Slip_Gain_HMu_Ab_150kph_R);
                Decel_Gain=(int16_t)LCABS_s16GetGainFromMulevel(Ref_Mu,(int16_t)S16_Decel_Gain_LMu_Ab_150kph_R,(int16_t)S16_Decel_Gain_LMMu_Ab_150kph_R,(int16_t)S16_Decel_Gain_MMu_Ab_150kph_R,(int16_t)S16_Decel_Gain_HMu_Ab_150kph_R );
            }
/*      #if !__SPECIAL_1st_PULSEDOWN  */
        }
/*      #endif */
  #if __MGH40_EBD
    }
  #endif
/****************************************************************/
  #if __ROUGH_ROAD_DUMP_IMPROVE
    if((Rough_road_detect_vehicle==1)||(Rough_road_suspect_vehicle==1))
    {
        Decel_Gain = (Decel_Gain*2/5);
    }
    else if(WL->Rough_road_detect_flag2==1)
    {
        Decel_Gain = (Decel_Gain*3/5);
    }
    else if(WL->Rough_road_detect_flag1==1)
    {
        Decel_Gain = (Decel_Gain*3/4);
    }
    else { }
  #endif      
/****************************************************************/

}

void LCABS_vCalculatePulseDownFactor(void)
{
    int16_t S_DIFF_OFFSET = 0;
    int32_t HDF_temp;
    int16_t Pulse_Down_Gain;

    if(REAR_WHEEL_wl==0) {
        Pulse_Down_Gain=(int16_t)S8_Pulse_Down_Gain_Front_Decel;
    }
    else {
        Pulse_Down_Gain=(int16_t)S8_Pulse_Down_Gain_Rear_Decel;
    }

  #if __CBC
    if((ABS_fz==0)&&(((LEFT_WHEEL_wl==1)&&(CBC_ACTIVE_rl==1))||((LEFT_WHEEL_wl==0)&&(CBC_ACTIVE_rr==1))))
    {
        Decel_Gain=(Decel_Gain*2);
    }
    else
    {
        ;
    }
  #endif

    /*              S_DIFF_OFFSET_CALC              */
  #if __SDIFF_OFFSET
    S_DIFF_OFFSET = LCABS_s16GetSDIFFOffsetPerSlip(afz,WL->rel_lam);
    if((WL->s_diff>0) || (WL->s_diff <= -S16_SDIFF_6G0)) {S_DIFF_OFFSET = S16_SDIFF_0G0;}
    if(S_DIFF_OFFSET >= S16_SDIFF_4G0) {Slip_Gain = (Slip_Gain*2)/3;}
    else if(S_DIFF_OFFSET >= S16_SDIFF_2G0) {Slip_Gain = (Slip_Gain*3)/4;}
    else if(S_DIFF_OFFSET >= S16_SDIFF_1G0) {Slip_Gain = (Slip_Gain*4)/5;}
    else { }

	if(WL->s_diff < -VREF_3_KPH)
	{
		WL->s_diff = -VREF_3_KPH;
	}
	else{}
    
		#if (!__REORGANIZE_FOR_MGH60_CYCLETIM)
    HDF_temp=(-(int32_t)(Pulse_Down_Gain)*(((int32_t)Slip_Gain*WL->rel_lam) + ((int32_t)Decel_Gain*(WL->s_diff-(S_DIFF_OFFSET)))));
		#else
    HDF_temp=(-(((int32_t)Slip_Gain*WL->rel_lam) + ((int32_t)Decel_Gain*(WL->s_diff-(S_DIFF_OFFSET))))*(int32_t)(Pulse_Down_Gain));
		#endif
  #else
    HDF_temp=(-(int32_t)(Pulse_Down_Gain)*(int32_t)((Slip_Gain*WL->rel_lam) + (Decel_Gain*WL->s_diff)));
  #endif

  #if __TEMP_for_CAS
    if(WL->dump_gain_mult_flag==1) {
        HDF_temp=(HDF_temp)*2;
    }
   #if  __TEMP_for_CAS2
    else if(WL->dump_gain_mult_flag2==1) {
        HDF_temp=(HDF_temp)*2;
    }
   #endif
    else {
  #endif
      #if __VDC && __ESP_ABS_COOPERATION_CONTROL
/*      if((ABS_fz==1)&&(PARTIAL_BRAKE==0)&&((SLIP_CONTROL_NEED_FL_old==1) || (SLIP_CONTROL_NEED_FR_old==1)))  */
        if((ABS_fz==1)&&(PARTIAL_BRAKE==0)&&(LFC_Split_flag==0)&&(Rough_road_detect_vehicle==0)&&(Rough_road_suspect_vehicle==0)){
            if((ESP_BRAKE_CONTROL_FL==1)||(ESP_BRAKE_CONTROL_FR==1)){
                if((AFZ_OK==0) || (afz>=-(int16_t)U8_PULSE_MED_HIGH_MU)) {
                    if(WL->ESP_ABS_Over_Front_Inside==1) {HDF_temp=(HDF_temp)*2;}
                    else if(WL->ESP_ABS_Over_Rear_Inside==1) {HDF_temp=(HDF_temp)*3;}
                    else if(WL->ESP_ABS_Over_Rear_Outside==1) {HDF_temp=(HDF_temp)*3;}
                    else { }
                }
                else{
                    if(WL->ESP_ABS_Over_Front_Inside==1) {HDF_temp=(HDF_temp)*1;}
                    else if(WL->ESP_ABS_Over_Rear_Inside==1) {HDF_temp=(HDF_temp)*2;}
                    else if(WL->ESP_ABS_Over_Rear_Outside==1) {HDF_temp=(HDF_temp)*2;}
                    else { }
                }
            }
            else if((McrAbs(wstr)>WSTR_20_DEG)&&(delta_yaw>=YAW_4DEG)) {
                if(WL->ESP_ABS_Over_Front_Inside==1) {HDF_temp=(HDF_temp)*1;}
                else if(WL->ESP_ABS_Over_Rear_Inside==1) {HDF_temp=(HDF_temp)*2;}
                else if(WL->ESP_ABS_Over_Rear_Outside==1) {HDF_temp=(HDF_temp)*2;}
                else { }
            }
            else { }
        }
      #endif

        if(WL->s_diff<=0)
        {
            if((AFZ_OK==0) || ((AFZ_OK==1) && (afz<=-(int16_t)U8_PULSE_MEDIUM_MU))
              || ( (LFC_Split_flag==1) && (REAR_WHEEL_wl==0)
                   && (((LEFT_WHEEL_wl==1)&&(MSL_BASE_rr==1)) || ((LEFT_WHEEL_wl==0)&&(MSL_BASE_rl==1))) )
            )
            {
                if(REAR_WHEEL_wl==0)
                {
                    if(vref>=VREF_20_KPH) {tempB1 = -LAM_60P;}
                    else if(vref>=VREF_10_KPH) {tempB1 = -LAM_60P + (int8_t)(((vref-VREF_20_KPH)*LAM_30P)/VREF_10_KPH);}
                    else {tempB1 = -LAM_90P;}
                }
                else
                {
                    tempB1 = -LAM_60P;
                }

                if(((vref-WL->vrad) <= (VREF_15_KPH + (vref/2))) && (WL->rel_lam>=tempB1) && (WL->LFC_UNSTABIL_counter<=L_TIME_150MS))
                {
                    if((int16_t)WL->s_diff_old < WL->s_diff)
                    {
                        if((WL->s_diff >=-S16_SDIFF_8G0) || (((int16_t)WL->s_diff_old - WL->s_diff) <= -S16_SDIFF_8G0))
                        {
                            tempW9 =(int16_t)((HDF_temp*7)/10);
                            HDF_temp = tempW9;
                        }
                    }
                }
            }
        }

  #if __TEMP_for_CAS
    }
  #endif
    
    if(HDF_temp > 10000)
    {
        HDF_temp = 10000;
    }
    else if(HDF_temp < -10000)
    {
        HDF_temp = -10000;
    }
    else { }
    
    WL->High_dump_factor = (int16_t)HDF_temp;
    
    
    if(ABS_fz==0) {
        WL->Dump_hold_scan_ref=0;
    }
    else {
        if(WL->High_dump_factor > S16_High_dump_factor_Upper_Limit) {WL->Dump_hold_scan_ref=0;}
        else if(WL->High_dump_factor < S16_High_dump_factor_Lower_Limit) {WL->Dump_hold_scan_ref=U8_Max_hold_scan_ref;}
        else {WL->Dump_hold_scan_ref=(uint8_t)(((S16_High_dump_factor_Upper_Limit-WL->High_dump_factor)/S16_High_dump_factor_Numerator)+1);}
    }
    #if __SPECIAL_1st_PULSEDOWN
        LCABS_vAdapt1stCyclePulseDown();
    #endif
}

  #if __SDIFF_OFFSET
int16_t LCABS_s16GetSDIFFOffsetPerSlip(int16_t input,int8_t wheel_slip)
{
    int16_t output = 0;
    int8_t slip_ref_1 = 0;
    int8_t slip_ref_2 = 0;

    if((AFZ_OK==1) && (FSF_wl==0)) {
        if ((input<=-(int16_t)(U8_PULSE_LOW_MU)) && (WL->LFC_n_th_deep_slip_comp_flag==0)) {
            if (input<=-(int16_t)U8_PULSE_LOW_MED_MU) {
                if(input<=-(int16_t)U8_PULSE_MEDIUM_MU) {
                    if(input<=-(int16_t)U8_PULSE_MED_HIGH_MU){ /* -0.55g 이하 */
                        slip_ref_1=LAM_15P;
                        slip_ref_2=LAM_20P;
                    }
                    else { /* -0.4g ~ -0.55g */
                        slip_ref_1=LAM_12P;
                        slip_ref_2=LAM_17P;
                    }
                }
                else { /* -0.3g ~ -0.4g */
                    slip_ref_1=LAM_15P;
                    slip_ref_2=LAM_20P;
                }
            }
            else { /* -0.15g ~ -0.3g */
                slip_ref_1=LAM_20P;
                slip_ref_2=LAM_30P;
            }
        }
        else { /* -0.15 이상 */
            slip_ref_1=LAM_25P;
            slip_ref_2=LAM_35P;
        }

    }
    else {
        slip_ref_1=LAM_20P;
        slip_ref_2=LAM_25P;
        output = S16_SDIFF_0G0; /* 첫사이클에서는? 보류! */
    }
    if(REAR_WHEEL_wl==0) {
        slip_ref_1 = slip_ref_1 + LAM_5P;
        slip_ref_2 = slip_ref_2 + LAM_10P;
    }

    if(vref<=VREF_20_KPH) {
        slip_ref_1 = (int8_t)(((int16_t)slip_ref_1 * 13)/10);
        slip_ref_2 = (int8_t)(((int16_t)slip_ref_2 * 15)/10);
    }

    if     (wheel_slip<-(slip_ref_2)) {output = S16_SDIFF_1G0;}
    else if(wheel_slip<-(slip_ref_1)) {output = S16_SDIFF_0G5;}
    else                              {output = S16_SDIFF_0G0;}

    if((vref>=VREF_20_KPH)&&(wheel_slip<-(LAM_70P))) {output = S16_SDIFF_2G0;}
    else if((vref>=VREF_50_KPH)&&(wheel_slip<-(LAM_60P))) {output = S16_SDIFF_2G0;}
    else { }

    return output;
}
  #endif

#if __SPECIAL_1st_PULSEDOWN
void LCABS_vAdapt1stCyclePulseDown(void)
{
    if ((FSF_wl==1)||(AFZ_OK==0)) {
        WL->SPECIAL_1st_Pulsedown_need_old=WL->SPECIAL_1st_Pulsedown_need;

        if (WL->LFC_UNSTABIL_counter>=L_1SCAN) {
            if (REAR_WHEEL_wl==0){
                if (WL->LFC_UNSTABIL_counter==L_1ST_SCAN) {
                    tempW1=0;
                }
                else if (WL->LFC_UNSTABIL_counter==L_2SCAN) {
                    if(WL->SPECIAL_1st_Pulsedown_need==0) {tempW1=S16_Additional_HDF_ref_F*3;}
                    else {tempW1=S16_Additional_HDF_ref_F;}
                }
                else {
                    if(WL->SPECIAL_1st_Pulsedown_need==0) {tempW1=S16_Additional_HDF_ref_F*5;}
                    else {tempW1=S16_Additional_HDF_ref_F*6;}
                }

                if(vref<(int16_t)VREF_40_KPH) {
                    tempC2=LCABS_u8Get1stCycleMuFromHDF(WL->High_dump_factor,(S16_HDF_REF_LMu_B_40kph_F+tempW1),(S16_HDF_REF_MMu_B_40kph_F+tempW1));
/*                    
                    if(tempC2 >= (WL->SPECIAL_1st_Pulsedown_need_old+2)) {WL->SPECIAL_1st_Pulsedown_need = WL->SPECIAL_1st_Pulsedown_need_old+1;}
                    else if(tempC2 > WL->SPECIAL_1st_Pulsedown_need_old) {WL->SPECIAL_1st_Pulsedown_need = tempC2;}
                    else {WL->SPECIAL_1st_Pulsedown_need = WL->SPECIAL_1st_Pulsedown_need_old;}
*/
                }
                else if (vref<(int16_t)VREF_60_KPH){
                    tempC2=LCABS_u8Get1stCycleMuFromHDF(WL->High_dump_factor,(S16_HDF_REF_LMu_40_60kph_F+tempW1),(S16_HDF_REF_MMu_40_60kph_F+tempW1));
/*                    
                    if(tempC2 >= (WL->SPECIAL_1st_Pulsedown_need_old+2)) {WL->SPECIAL_1st_Pulsedown_need = WL->SPECIAL_1st_Pulsedown_need_old+1;}
                    else if(tempC2 > WL->SPECIAL_1st_Pulsedown_need_old) {WL->SPECIAL_1st_Pulsedown_need = tempC2;}
                    else {WL->SPECIAL_1st_Pulsedown_need = WL->SPECIAL_1st_Pulsedown_need_old;}
*/
                }
                else {
                    tempC2=LCABS_u8Get1stCycleMuFromHDF(WL->High_dump_factor,(S16_HDF_REF_LMu_Ab_60kph_F+tempW1),(S16_HDF_REF_MMu_Ab_60kph_F+tempW1));
/*                    
                    if(tempC2 >= (WL->SPECIAL_1st_Pulsedown_need_old+2)) {WL->SPECIAL_1st_Pulsedown_need = WL->SPECIAL_1st_Pulsedown_need_old+1;}
                    else if(tempC2 > WL->SPECIAL_1st_Pulsedown_need_old) {WL->SPECIAL_1st_Pulsedown_need = tempC2;}
                    else {WL->SPECIAL_1st_Pulsedown_need = WL->SPECIAL_1st_Pulsedown_need_old;}
*/
                }
            }
            else {
                if (WL->LFC_UNSTABIL_counter==L_1ST_SCAN) {
                    tempW1=0;
                }
                else if (WL->LFC_UNSTABIL_counter==L_2SCAN) {
                    tempW1=S16_Additional_HDF_ref_R;
                }
                else {
                    tempW1=(int16_t)((uint16_t)S16_Additional_HDF_ref_R<<1);
                }

                if (vref<(int16_t)VREF_40_KPH) {
                    tempC2=LCABS_u8Get1stCycleMuFromHDF(WL->High_dump_factor,(S16_HDF_REF_LMu_B_40kph_R+tempW1),(S16_HDF_REF_MMu_B_40kph_R+tempW1));
/*                    
                    if(tempC2 >= (WL->SPECIAL_1st_Pulsedown_need_old+2)) {WL->SPECIAL_1st_Pulsedown_need = WL->SPECIAL_1st_Pulsedown_need_old+1;}
                    else if(tempC2 > WL->SPECIAL_1st_Pulsedown_need_old) {WL->SPECIAL_1st_Pulsedown_need = tempC2;}
                    else {WL->SPECIAL_1st_Pulsedown_need = WL->SPECIAL_1st_Pulsedown_need_old;}
*/
                }
                else if (vref<(int16_t)VREF_60_KPH){
                    tempC2=LCABS_u8Get1stCycleMuFromHDF(WL->High_dump_factor,(S16_HDF_REF_LMu_40_60kph_R+tempW1),(S16_HDF_REF_MMu_40_60kph_R+tempW1));
/*                    
                    if(tempC2 >= (WL->SPECIAL_1st_Pulsedown_need_old+2)) {WL->SPECIAL_1st_Pulsedown_need = WL->SPECIAL_1st_Pulsedown_need_old+1;}
                    else if(tempC2 > WL->SPECIAL_1st_Pulsedown_need_old) {WL->SPECIAL_1st_Pulsedown_need = tempC2;}
                    else {WL->SPECIAL_1st_Pulsedown_need = WL->SPECIAL_1st_Pulsedown_need_old;}
*/
                }
                else {
                    tempC2=LCABS_u8Get1stCycleMuFromHDF(WL->High_dump_factor,(S16_HDF_REF_LMu_Ab_60kph_R+tempW1),(S16_HDF_REF_MMu_Ab_60kph_R+tempW1));
/*                    
                    if(tempC2 >= (WL->SPECIAL_1st_Pulsedown_need_old+2)) {WL->SPECIAL_1st_Pulsedown_need = WL->SPECIAL_1st_Pulsedown_need_old+1;}
                    else if(tempC2 > WL->SPECIAL_1st_Pulsedown_need_old) {WL->SPECIAL_1st_Pulsedown_need = tempC2;}
                    else {WL->SPECIAL_1st_Pulsedown_need = WL->SPECIAL_1st_Pulsedown_need_old;}
*/
                }
            }

            if(tempC2 >= (WL->SPECIAL_1st_Pulsedown_need_old+2)) {WL->SPECIAL_1st_Pulsedown_need = WL->SPECIAL_1st_Pulsedown_need_old+1;}
            else if(tempC2 > WL->SPECIAL_1st_Pulsedown_need_old) {WL->SPECIAL_1st_Pulsedown_need = tempC2;}
            else {WL->SPECIAL_1st_Pulsedown_need = WL->SPECIAL_1st_Pulsedown_need_old;}
        }
        else {
            WL->SPECIAL_1st_Pulsedown_need=0;
        }
    }
    else {
        WL->SPECIAL_1st_Pulsedown_need=0;
    }
}

uint8_t LCABS_u8Get1stCycleMuFromHDF(int16_t input, int16_t REF_LOW, int16_t REF_MED) /* estimate whether low or med mu */
{
    if (input >= (int16_t)REF_LOW) {
        return SPECIAL_FOR_LOW;
    }
    else if (input >= (int16_t)REF_MED) {
        return SPECIAL_FOR_MED;
    }
    else {
        return 0;
    }
}
#endif /* __SPECIAL_1st_PULSEDOWN */


int16_t LCABS_s16GetGainFromMulevel(int16_t input,int16_t gain_low,int16_t gain_lowmed, int16_t gain_med, int16_t gain_high)
{
    int16_t temp_result=0;

    if ((input<=-(int16_t)(U8_PULSE_LOW_MU)) && (WL->LFC_n_th_deep_slip_comp_flag==0)) {
        if (input<=-(int16_t)(U8_PULSE_UNDER_LOW_MED_MU)){
            if (input<=-(int16_t)U8_PULSE_LOW_MED_MU) {
                if(input<=-(int16_t)U8_PULSE_MEDIUM_MU) {
                    if(input<=-(int16_t)U8_PULSE_MED_HIGH_MU){
                        if(input<=-(int16_t)U8_PULSE_HIGH_MU){
                            temp_result=(int16_t)gain_high;
                        }
                        else{
                            temp_result=(int16_t)LCABS_s16Interpolation2P(input,-(int16_t)U8_PULSE_MED_HIGH_MU,-(int16_t)U8_PULSE_HIGH_MU,(int16_t)gain_med,(int16_t)gain_high);
                        }
                    }
                    else{
                        temp_result=(int16_t)gain_med;
                    }
                }
                else{
                    temp_result=(int16_t)LCABS_s16Interpolation2P(input,-(int16_t)U8_PULSE_LOW_MED_MU,-(int16_t)U8_PULSE_MEDIUM_MU,(int16_t)gain_lowmed,(int16_t)gain_med);
                }
            }
            else{
                temp_result=(int16_t)gain_lowmed;
            }
        }
        else{
            temp_result=(int16_t)LCABS_s16Interpolation2P(input,-(int16_t)U8_PULSE_LOW_MU,-(int16_t)U8_PULSE_UNDER_LOW_MED_MU,(int16_t)gain_low,(int16_t)gain_lowmed);
        }
    }
    else{
        temp_result=(int16_t)gain_low;
    }
    return temp_result;
}

#if __USE_SIDE_VREF
static void LCABS_vDecideSTABLEBySideVref(void)
{
        /* int8_t Slip_by_Side_Vref; */
    int8_t Slip_Thres1;
    int8_t Slip_Thres2;
    int8_t Slip_Thres3;
    int8_t Cnt_Ref1;
    int8_t Cnt_Ref2;
    int8_t Cnt_Ref3;
    int16_t Side_Vref;

    int16_t dV_Thres1;
    int16_t dV_Thres2;
    int16_t dV_Thres3;

    int8_t Slip_REF1;
    int8_t Slip_REF2;

    if(STAB_A_wl==0) {
        if(REAR_WHEEL_wl==0) {
            Slip_REF1 = -LAM_15P;
            Slip_REF2 = -LAM_25P;

            Slip_Thres1=-LAM_5P;
            Slip_Thres2=-LAM_8P;
            Slip_Thres3=-LAM_10P;

            dV_Thres1=VREF_2_KPH;
            dV_Thres2=VREF_5_KPH;
            dV_Thres3=VREF_8_KPH;

            Cnt_Ref1=42;/*MGH60:60*/
            Cnt_Ref2=56;/*MGH60:80*/
            Cnt_Ref3=63;/*MGH60:90*/
        }
        else {
            Slip_REF1 = -LAM_25P;
            Slip_REF2 = -LAM_35P;

            Slip_Thres1=-LAM_5P;
            Slip_Thres2=-LAM_10P;
            Slip_Thres3=-LAM_15P;

            dV_Thres1=VREF_2_KPH;
            dV_Thres2=VREF_3_KPH;
            dV_Thres3=VREF_8_KPH;

            Cnt_Ref1=42;/*MGH60:60*/
            Cnt_Ref2=53;/*MGH60:75*/
            Cnt_Ref3=63;/*MGH60:90*/
        }

        if(LEFT_WHEEL_wl==1) {
            Side_Vref=vref_L;
        }
        else {
            Side_Vref=vref_R;
        }

        tempW2=WL->vrad_crt-Side_Vref;

        WL->Slip_by_Side_Vref = (int8_t)(((int32_t)(WL->vrad_crt-Side_Vref)*100)/Side_Vref);

    /*  if(!((vref_L_diff > 0) || (vref_R_diff > 0))) { */

        if((vref_L_diff <= 0) && (vref_R_diff <= 0)) {
            if(WL->rel_lam > (Slip_REF2)) {
                if(WL->rel_lam > (Slip_REF1)) {
                    if((WL->arad > (-ARAD_2G5)) && (WL->arad < (ARAD_2G5))) {
                        if((WL->arad > (-ARAD_1G0)) && (WL->arad < (ARAD_1G0))) {
                                if((WL->Slip_by_Side_Vref > Slip_Thres3) || (tempW2 > -dV_Thres3)) {
                                    if(WL->STABLE_cnt_using_SIDE_VREF3 < U8_TYPE_MAXNUM) {WL->STABLE_cnt_using_SIDE_VREF3 = WL->STABLE_cnt_using_SIDE_VREF3 + 1;}
                                    if((WL->Slip_by_Side_Vref > Slip_Thres2) || (tempW2 > -dV_Thres2)) {
                                        if(WL->STABLE_cnt_using_SIDE_VREF2 < U8_TYPE_MAXNUM) {WL->STABLE_cnt_using_SIDE_VREF2 = WL->STABLE_cnt_using_SIDE_VREF2 + 1;}
                                        if((WL->Slip_by_Side_Vref > Slip_Thres1) || (tempW2 > -dV_Thres1)) {
                                            if(WL->STABLE_cnt_using_SIDE_VREF1 < U8_TYPE_MAXNUM) {WL->STABLE_cnt_using_SIDE_VREF1 = WL->STABLE_cnt_using_SIDE_VREF1 + 1;}
                                        }
                                        else
                                        {
                                            WL->STABLE_cnt_using_SIDE_VREF1 = (uint8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->STABLE_cnt_using_SIDE_VREF1,-2);
                                        }
/*                                        
                                        else if(WL->STABLE_cnt_using_SIDE_VREF1 >= 2) {WL->STABLE_cnt_using_SIDE_VREF1 -=2;}
                                        else { }
*/
                                    }
                                    else
                                    {
                                        WL->STABLE_cnt_using_SIDE_VREF2 = (uint8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->STABLE_cnt_using_SIDE_VREF2,-2);
                                    }
/*
                                    else if(WL->STABLE_cnt_using_SIDE_VREF2 >= 2) {WL->STABLE_cnt_using_SIDE_VREF2 -=2;}
                                    else { }
*/
                                }
                                else
                                {
                                    WL->STABLE_cnt_using_SIDE_VREF3 = (uint8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->STABLE_cnt_using_SIDE_VREF3,-2);
                                }
/*
                                else if(WL->STABLE_cnt_using_SIDE_VREF3 >= 2) {WL->STABLE_cnt_using_SIDE_VREF3 -=2;}
                                         else { }
*/
                        }
                        else {
                                ; /* 보류 */
                        }
                    }
                    else {
                        /* -- */
                        WL->STABLE_cnt_using_SIDE_VREF1 = (uint8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->STABLE_cnt_using_SIDE_VREF1,-1);
                        WL->STABLE_cnt_using_SIDE_VREF2 = (uint8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->STABLE_cnt_using_SIDE_VREF2,-1);
                        WL->STABLE_cnt_using_SIDE_VREF3 = (uint8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->STABLE_cnt_using_SIDE_VREF3,-1);
/*                        
                        if(WL->STABLE_cnt_using_SIDE_VREF1 >= 1) {WL->STABLE_cnt_using_SIDE_VREF1 --;}
                        if(WL->STABLE_cnt_using_SIDE_VREF2 >= 1) {WL->STABLE_cnt_using_SIDE_VREF2 --;}
                        if(WL->STABLE_cnt_using_SIDE_VREF3 >= 1) {WL->STABLE_cnt_using_SIDE_VREF3 --;}
*/
                    }
                }
                else {
                    if((WL->arad > (-ARAD_2G0)) && (WL->arad < (ARAD_2G0))) {
                        if((WL->arad > (-ARAD_1G0)) && (WL->arad < (ARAD_1G0))) {
                                ;/* 보류 */
                        }
                        else {
                            /* -- */
                            WL->STABLE_cnt_using_SIDE_VREF1 = (uint8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->STABLE_cnt_using_SIDE_VREF1,-1);
                            WL->STABLE_cnt_using_SIDE_VREF2 = (uint8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->STABLE_cnt_using_SIDE_VREF2,-1);
                            WL->STABLE_cnt_using_SIDE_VREF3 = (uint8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->STABLE_cnt_using_SIDE_VREF3,-1);
/*                        
                            if(WL->STABLE_cnt_using_SIDE_VREF1 >= 1) {WL->STABLE_cnt_using_SIDE_VREF1 --;}
                            if(WL->STABLE_cnt_using_SIDE_VREF2 >= 1) {WL->STABLE_cnt_using_SIDE_VREF2 --;}
                            if(WL->STABLE_cnt_using_SIDE_VREF3 >= 1) {WL->STABLE_cnt_using_SIDE_VREF3 --;}
*/
                        }
                    }
                    else {
                        /* -=2 */
                        WL->STABLE_cnt_using_SIDE_VREF1 = (uint8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->STABLE_cnt_using_SIDE_VREF1,-2);
                        WL->STABLE_cnt_using_SIDE_VREF2 = (uint8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->STABLE_cnt_using_SIDE_VREF2,-2);
                        WL->STABLE_cnt_using_SIDE_VREF3 = (uint8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->STABLE_cnt_using_SIDE_VREF3,-2);
/*
                        if(WL->STABLE_cnt_using_SIDE_VREF1 >= 2) {WL->STABLE_cnt_using_SIDE_VREF1 -=2;}
                        if(WL->STABLE_cnt_using_SIDE_VREF2 >= 2) {WL->STABLE_cnt_using_SIDE_VREF2 -=2;}
                        if(WL->STABLE_cnt_using_SIDE_VREF3 >= 2) {WL->STABLE_cnt_using_SIDE_VREF3 -=2;}
*/
                    }
                }
            }
            else {
                if((WL->arad > (-ARAD_2G0)) && (WL->arad < (ARAD_2G0))) {
                    /* -=2 */
                    WL->STABLE_cnt_using_SIDE_VREF1 = (uint8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->STABLE_cnt_using_SIDE_VREF1,-2);
                    WL->STABLE_cnt_using_SIDE_VREF2 = (uint8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->STABLE_cnt_using_SIDE_VREF2,-2);
                    WL->STABLE_cnt_using_SIDE_VREF3 = (uint8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->STABLE_cnt_using_SIDE_VREF3,-2);
/*
                    if(WL->STABLE_cnt_using_SIDE_VREF1 >= 2) {WL->STABLE_cnt_using_SIDE_VREF1 -=2;}
                    if(WL->STABLE_cnt_using_SIDE_VREF2 >= 2) {WL->STABLE_cnt_using_SIDE_VREF2 -=2;}
                    if(WL->STABLE_cnt_using_SIDE_VREF3 >= 2) {WL->STABLE_cnt_using_SIDE_VREF3 -=2;}
*/
                }
                else {
                    /* 전부 reset */
                    WL->STABLE_cnt_using_SIDE_VREF1 =0;
                    WL->STABLE_cnt_using_SIDE_VREF2 =0;
                    WL->STABLE_cnt_using_SIDE_VREF3 =0;
                }
            }
        }
        else {
            WL->STABLE_cnt_using_SIDE_VREF1 = (uint8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->STABLE_cnt_using_SIDE_VREF1,-1);
            WL->STABLE_cnt_using_SIDE_VREF2 = (uint8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->STABLE_cnt_using_SIDE_VREF2,-1);
            WL->STABLE_cnt_using_SIDE_VREF3 = (uint8_t)LCABS_s16DecreaseUnsignedCnt((int16_t)WL->STABLE_cnt_using_SIDE_VREF3,-1);
/*
            if(WL->STABLE_cnt_using_SIDE_VREF1 >= 1) {WL->STABLE_cnt_using_SIDE_VREF1 --;}
            if(WL->STABLE_cnt_using_SIDE_VREF2 >= 1) {WL->STABLE_cnt_using_SIDE_VREF2 --;}
            if(WL->STABLE_cnt_using_SIDE_VREF3 >= 1) {WL->STABLE_cnt_using_SIDE_VREF3 --;}
*/
        }

        if((WL->STABLE_cnt_using_SIDE_VREF3 >= Cnt_Ref3) || (WL->STABLE_cnt_using_SIDE_VREF2 >= Cnt_Ref2) || (WL->STABLE_cnt_using_SIDE_VREF1 >= Cnt_Ref1)) {
            WL->STABLE_for_SIDE_VREF_flag=1;
        }
    }
    else {
        WL->STABLE_cnt_using_SIDE_VREF1 =0;
        WL->STABLE_cnt_using_SIDE_VREF2 =0;
        WL->STABLE_cnt_using_SIDE_VREF3 =0;
        WL->STABLE_for_SIDE_VREF_flag=0;
    }
}


#endif /* moved for ABS compile */

#if (__REORGANIZE_STABLE_DCT == DISABLE)

void LCABS_vDecideSTABLEState(void)
{
    int16_t bend_diff_compensated=0;
    int16_t bend_slip_compensated=0;

    int8_t Clear_State=0;

    tempF0=0;

  #if __BEND_DETECT_using_SIDEVREF
   #if  __TEMP_for_CAS2
    if((vref_limit_dec_flag==1) && (vref>=VREF_30_KPH)) {
        if((AFZ_OK==1)&&(afz>-(int16_t)U8_PULSE_HIGH_MU)) {
          #if   __REAR_D
            if((REAR_WHEEL_wl==0) && (FSF_wl==0) && (vref_limit_dec_counter_frt>=L_TIME_70MS)) {tempF0 = 1;}/*10*/
          #else
            if((REAR_WHEEL_wl==1) && (FSF_wl==0) && (vref_limit_dec_counter_rr>=L_TIME_70MS)) {tempF0 = 1;}/*10*/
          #endif
        }
        if(tempF0==1) {tempW6 =(int16_t)(((int32_t)(WL->vrad_crt - vref)*100)/(int32_t)vref);}
        else {tempW6 = 0;}
    }
   #endif
    if(((AFZ_OK==1)&&(afz<=-(int16_t)U8_PULSE_MEDIUM_MU)) && (BEND_DETECT_flag==1)) {
        if(BEND_DETECT_LEFT==1) {
            if(LEFT_WHEEL_wl==1) {
              #if   __TEMP_for_CAS2
                if(tempF0==0) {
                    bend_diff_compensated = (vref - WL->vrad_crt) + ((Side_vref_diff*4)/5);
                    bend_slip_compensated = (int16_t)(WL->rel_lam + bend_slip_comp);
                }
                else {
                    bend_diff_compensated = vref - WL->vrad_crt;
                    if(tempW6<(int16_t)WL->rel_lam) {bend_slip_compensated = tempW6;}
                    else {bend_slip_compensated = (int16_t)WL->rel_lam;}
                }
              #else
                bend_diff_compensated = (vref - WL->vrad_crt) + ((Side_vref_diff*4)/5);
                bend_slip_compensated = (int16_t)(WL->rel_lam + bend_slip_comp);
              #endif
            }
            else {
                bend_diff_compensated = vref - WL->vrad_crt;
              #if   __TEMP_for_CAS2
                if(tempF0==0) {bend_slip_compensated = (int16_t)WL->rel_lam;}
                else {
                    if(tempW6<(int16_t)WL->rel_lam) {bend_slip_compensated = tempW6;}
                    else {bend_slip_compensated = (int16_t)WL->rel_lam;}
                }
              #else
                bend_slip_compensated = (int16_t)(WL->rel_lam + bend_slip_comp);
              #endif
            }
        }
        else if(BEND_DETECT_RIGHT==1) {
            if(LEFT_WHEEL_wl==0) {
              #if   __TEMP_for_CAS2
                if(tempF0==0) {
                    bend_diff_compensated = (vref - WL->vrad_crt) - ((Side_vref_diff*4)/5);
                    bend_slip_compensated = (int16_t)(WL->rel_lam + bend_slip_comp);
                }
                else {
                    bend_diff_compensated = vref - WL->vrad_crt;
                    if(tempW6<(int16_t)WL->rel_lam) {bend_slip_compensated = tempW6;}
                    else {bend_slip_compensated = (int16_t)WL->rel_lam;}
                }
              #else
                bend_diff_compensated = (vref - WL->vrad_crt) - ((Side_vref_diff*4)/5);
                bend_slip_compensated = (int16_t)(WL->rel_lam + bend_slip_comp);
              #endif
            }
            else {
                bend_diff_compensated = vref - WL->vrad_crt;
              #if   __TEMP_for_CAS2
                if(tempF0==0) {bend_slip_compensated = (int16_t)WL->rel_lam;}
                else {
                    if(tempW6<(int16_t)WL->rel_lam) {bend_slip_compensated = tempW6;}
                    else {bend_slip_compensated = (int16_t)WL->rel_lam;}
                }
              #else
                bend_slip_compensated = (int16_t)(WL->rel_lam + bend_slip_comp);
              #endif
            }
        }
        else {
            bend_diff_compensated = vref - WL->vrad_crt;
          #if   __TEMP_for_CAS2
            if(tempF0==0) {bend_slip_compensated = (int16_t)WL->rel_lam;}
            else {
                if(tempW6<(int16_t)WL->rel_lam) {bend_slip_compensated = tempW6;}
                else {bend_slip_compensated = (int16_t)WL->rel_lam;}
            }
          #else
            bend_slip_compensated = (int16_t)(WL->rel_lam + bend_slip_comp);
          #endif
        }
    }
    else {
        bend_diff_compensated = vref - WL->vrad_crt;
        if(BEND_DETECT==1) {
          #if   __TEMP_for_CAS2
            if(tempF0==0) {bend_slip_compensated = (int16_t)(WL->rel_lam + bend_slip_comp);}
            else {
                if(tempW6<(int16_t)WL->rel_lam) {bend_slip_compensated = tempW6;}
                else {bend_slip_compensated = (int16_t)WL->rel_lam;}
            }
          #else
            bend_slip_compensated = (int16_t)(WL->rel_lam + bend_slip_comp);
          #endif
        }
        else {
          #if   __TEMP_for_CAS2
            if(tempF0==0) {bend_slip_compensated = (int16_t)WL->rel_lam;}
            else {
                if(tempW6<(int16_t)WL->rel_lam) {bend_slip_compensated = tempW6;}
                else {bend_slip_compensated = (int16_t)WL->rel_lam;}
            }
          #else
            bend_slip_compensated = (int16_t)(WL->rel_lam + bend_slip_comp);
          #endif
        }
    }

    if(REAR_WHEEL_wl==0) {
        if(CAS_wl==0) {
            if(vref>VREF_100_KPH) {tempB0 = -(int8_t)(LAM_2P);}
            else if(vref>VREF_50_KPH) {tempB0 = -(int8_t)(LAM_3P);}
            else if(vref>VREF_35_KPH) {tempB0 = -(int8_t)(LAM_4P);}
            else  {tempB0 = -(int8_t)(LAM_5P);}

              #if (__TCS || __ETC || __EDC) && (__RESET_ABS_BY_ACCEL)
                if(lcabsu1SuspctAccelIntention==1)
                {
                  #if __REAR_D
                   #if (__4WD_VARIANT_CODE==ENABLE)
                    if((lsu8DrvMode==DM_2WD) || (lsu8DrvMode==DM_2H))
                    {
                        tempW0 = LCABS_s16Interpolation2P(mtp,MTP_10_P,MTP_90_P,LAM_5P,LAM_50P);
                        tempB0 = tempB0 - (int8_t)tempW0;             
                    }
                   #elif !__4WD
                        tempW0 = LCABS_s16Interpolation2P(mtp,MTP_10_P,MTP_90_P,LAM_5P,LAM_50P);
                        tempB0 = tempB0 - (int8_t)tempW0;             
                   #endif
                  #endif 
                }
              #endif    

          #if __IMPROVE_DECEL_AT_CERTAIN_HMU

            if(lcabsu1CertainHMu==1)
            {
                tempB4 = (int8_t)((vref/50) + VREF_2_KPH);
            }
            else if(((AFZ_OK==1) && (afz<-(int16_t)U8_PULSE_HIGH_MU))||(lcabsu1CertainHMuPre==1))
            {
                tempB4 = (int8_t)((vref/50) + VREF_1_5_KPH);
            }
            else
            {
                tempB4 = (int8_t)((vref/50) + VREF_1_KPH);
            }

          #else

            tempB4 = (int8_t)((vref/50) + VREF_1_KPH);

          #endif

            if((bend_diff_compensated <= (int16_t)tempB4) || (bend_slip_compensated>=(int16_t)tempB0)) {
                if(AFZ_OK==0) {
                    if(WL->arad >= -(int8_t)ARAD_1G0) {Clear_State=1;}
                }
                else if(afz>=-(int16_t)U8_PULSE_LOW_MU) {
                    if(WL->arad >= -(int8_t)ARAD_0G5) {Clear_State=1;}
                }
                else if(afz>=-(int16_t)U8_PULSE_LOW_MED_MU) {
                    if(WL->arad >= -(int8_t)ARAD_1G0) {Clear_State=1;}
                }
                else {
                  #if (__THRDECEL_RESOLUTION_CHANGE==1)
                    if(WL->arad_resol_change >= WL->lcabss16ThresDecel32) {Clear_State=1;}
                  #else
                    if(WL->arad >= WL->thres_decel) {Clear_State=1;}
                  #endif                   
                }

                if((AFZ_OK==0) || (afz>=-(int16_t)U8_PULSE_LOW_MED_MU)) {
                    if((vrselect1-WL->vrad_crt)<=0) {                              /* Changed due to BASALT Cascading 030716 */
                        if((WL->arad >= (int8_t)ARAD_0G0) && (WL->arad < (int8_t)ARAD_3G0)) {Clear_State=0;}
                    }
                }
            }
          #if !__UNSTABLE_REAPPLY_ENABLE
            else if(bend_diff_compensated <= (int16_t)(VREF_2_KPH + (vref/50))) {
                if((AFZ_OK==1) && (afz<-(int16_t)U8_PULSE_LOW_MU)) {
                    if(WL->arad >= (int8_t)ARAD_3G0) {Clear_State=1;}
                }
            }
            else if(bend_diff_compensated <= (int16_t)(VREF_4_KPH + (vref/50))) {
                if((AFZ_OK==1) && (afz<-(int16_t)U8_LOW_MU)) {
                    if(WL->arad >= (int8_t)ARAD_5G0) {Clear_State=1;}
                }
            }
            else if(bend_diff_compensated <= (int16_t)(VREF_6_KPH + (vref/50))) {
                if((AFZ_OK==1) && (afz<-(int16_t)U8_LOW_MU)) {
                    if(WL->arad >= (int8_t)ARAD_8G0) {Clear_State=1;}
                }
            }
          #endif
            else { }
        }
        else {
            tempB0 = -(int8_t)(LAM_3P);

          #if (__TCS || __ETC || __EDC) && (__RESET_ABS_BY_ACCEL)
            if(lcabsu1SuspctAccelIntention==1)
            {
              #if __REAR_D
               #if (__4WD_VARIANT_CODE==ENABLE)
                if((lsu8DrvMode==DM_2WD) || (lsu8DrvMode==DM_2H))
                {
                    tempW0 = LCABS_s16Interpolation2P(mtp,MTP_10_P,MTP_90_P,LAM_2P,LAM_30P);
                    tempB0 = tempB0 - (int8_t)tempW0;             
                }
               #elif !__4WD
                    tempW0 = LCABS_s16Interpolation2P(mtp,MTP_10_P,MTP_90_P,LAM_2P,LAM_30P);
                    tempB0 = tempB0 - (int8_t)tempW0;             
               #endif
              #endif 
            }
          #endif    

            if((WL->rel_lam + bend_slip_comp)>= tempB0) {
                if((vrselect1-WL->vrad_crt)<=0) {                              /* Changed due to BASALT Cascading 030716 */
                    if(WL->arad <= (int8_t)ARAD_0G0) {Clear_State=1;}
                }
                else if((WL->arad < (int8_t)ARAD_1G0) && (WL->arad > -(int8_t)ARAD_0G5)) {Clear_State=1;}
                else { }
            }
        }
    }
    else {
         if(CAS_wl==0) {
            if(vref>=VREF_40_KPH) {tempB0 = -(int8_t)(LAM_2P);}
            else if(vref>=VREF_20_KPH) {tempB0 = -(int8_t)(LAM_3P);}
            else {tempB0 = -(int8_t)(LAM_5P);}

            if((WL->peak_slip>=-LAM_20P) || (afz<=-(int16_t)(U8_PULSE_HIGH_MU)))    /******************/
            {
                if(vref>VREF_100_KPH) {tempB0 = -(int8_t)(LAM_2P);}
                else if(vref>VREF_50_KPH) {tempB0 = -(int8_t)(LAM_3P);}
                else if(vref>VREF_35_KPH) {tempB0 = -(int8_t)(LAM_4P);}
                else  {tempB0 = -(int8_t)(LAM_5P);}
            }

          #if (__TCS || __ETC || __EDC) && (__RESET_ABS_BY_ACCEL)
            if(lcabsu1SuspctAccelIntention==1)
            {
              #if !__REAR_D
               #if (__4WD_VARIANT_CODE==ENABLE)
                if((lsu8DrvMode==DM_2WD) || (lsu8DrvMode==DM_2H))
                {
                    tempW0 = LCABS_s16Interpolation2P(mtp,MTP_10_P,MTP_90_P,LAM_5P,LAM_50P);
                    tempB0 = tempB0 - (int8_t)tempW0;             
                }
               #elif !__4WD
                    tempW0 = LCABS_s16Interpolation2P(mtp,MTP_10_P,MTP_90_P,LAM_5P,LAM_50P);
                    tempB0 = tempB0 - (int8_t)tempW0;             
               #endif
              #endif 
            }
              #endif    
    
/*          if((WL->rel_lam + bend_slip_comp) >= (int8_t)(-LAM_2P)) { */
            if((bend_diff_compensated <= (int16_t)(VREF_1_KPH + (vref/50))) || (bend_slip_compensated >= (int16_t)tempB0)) {
                if(AFZ_OK==0) {
/*                    if(WL->arad >= -(int8_t)ARAD_0G5) {Clear_State=1;} */
        			if(((FL.rel_lam<-LAM_20P)&&(FR.rel_lam<-LAM_20P)) || (RR.rel_lam<-LAM_20P) || (RL.rel_lam<-LAM_20P))
        			{
        				if((WL->arad >= (int8_t)ARAD_0G25) || ((WL->arad >= -(int8_t)ARAD_0G5)&&(WL->max_arad_instbl >= (int8_t)ARAD_3G0))) {Clear_State=1;}
        			}
        			else
        			{
        				if(WL->arad >= -(int8_t)ARAD_0G5) {Clear_State=1;}
        			}
                }
                else if(afz>=-(int16_t)U8_PULSE_LOW_MU) {
                    if(WL->arad >= -(int8_t)ARAD_0G5) {Clear_State=1;}
                }
                else if(afz>=-(int16_t)U8_PULSE_LOW_MED_MU) {
                    if(WL->arad >= -(int8_t)ARAD_1G0) {Clear_State=1;}
                }
                else {
                  #if (__THRDECEL_RESOLUTION_CHANGE==1)
                    if(WL->arad_resol_change >= WL->lcabss16ThresDecel32) {Clear_State=1;}
                  #else
                    if(WL->arad >= WL->thres_decel) {Clear_State=1;}
                  #endif                   
                }

                if((vrselect1-WL->vrad_crt)<=0) {                              /* Changed due to BASALT Cascading 030716 */
                    if((WL->arad >= (int8_t)ARAD_0G0) && (WL->arad < (int8_t)ARAD_3G0)) {Clear_State=0;}
                }
            }
         #if !__UNSTABLE_REAPPLY_ENABLE
            else if(bend_slip_compensated >= (int16_t)(-LAM_5P)) {
                if(AFZ_OK==0) {
                    if(WL->arad >= (int8_t)ARAD_3G0) {Clear_State=1;}
                }
                else if(afz>=-(int16_t)U8_PULSE_LOW_MU) {

                    if(WL->arad >= (int8_t)ARAD_3G0) {Clear_State=1;}
                }
                else if(afz>=-(int16_t)U8_PULSE_LOW_MED_MU) {
                    if(WL->arad >= (int8_t)ARAD_3G0) {Clear_State=1;}
                }
                else {
                    if(WL->arad >= ARAD_3G0) {Clear_State=1;}
                }

                if((vrselect1-WL->vrad_crt)<=0) {                              /* Changed due to BASALT Cascading 030716 */
                    Clear_State=0;
                }
            }
        #endif
            else { }
        }
        else {
            tempB0 = -(int8_t)(LAM_2P);

          #if (__TCS || __ETC || __EDC) && (__RESET_ABS_BY_ACCEL)
            if(lcabsu1SuspctAccelIntention==1)
            {
              #if !__REAR_D
               #if (__4WD_VARIANT_CODE==ENABLE)
                if((lsu8DrvMode==DM_2WD) || (lsu8DrvMode==DM_2H))
                {
                    tempW0 = LCABS_s16Interpolation2P(mtp,MTP_10_P,MTP_90_P,LAM_2P,LAM_30P);
                    tempB0 = tempB0 - (int8_t)tempW0;             
                }
               #elif !__4WD
                    tempW0 = LCABS_s16Interpolation2P(mtp,MTP_10_P,MTP_90_P,LAM_2P,LAM_30P);
                    tempB0 = tempB0 - (int8_t)tempW0;             
               #endif
              #endif 
            }
          #endif    

            if(bend_slip_compensated >= (int16_t)(tempB0)) {
                if((vrselect1-WL->vrad_crt)<=0) {                              /* Changed due to BASALT Cascading 030716 */
                    if(WL->arad <= (int8_t)ARAD_0G0) {Clear_State=1;}
                }
                else if((WL->arad < (int8_t)ARAD_0G5) && (WL->arad > -(int8_t)ARAD_0G5)) {Clear_State=1;}
                else { }
            }
        }
    }

  #if __USE_SIDE_VREF
    LCABS_vDecideSTABLEBySideVref();
    if(WL->STABLE_for_SIDE_VREF_flag==1) {Clear_State=1;}
  #endif  

 #else   /* __BEND_DETECT_using_SIDEVREF */
    if(REAR_WHEEL_wl==0) {
        if(CAS_wl==0) {
            if((vref-WL->vrad) <= (int16_t)(VREF_1_KPH + (vref/50))) {
                if(AFZ_OK==0) {
/*                  if((WL->arad <= (int8_t)ARAD_1G5) && (WL->arad >= -(int8_t)ARAD_1G5)) Clear_State=1; */
                    if(WL->arad >= -(int8_t)ARAD_1G0) {Clear_State=1;}
                }
                else if(afz>=-(int16_t)U8_PULSE_LOW_MU) {
/*                  if((WL->arad <= (int8_t)ARAD_2G0) && (WL->arad >= -(int8_t)ARAD_0G5)) Clear_State=1; */
                    if(WL->arad >= -(int8_t)ARAD_0G5) {Clear_State=1;}
                }
                else if(afz>=-(int16_t)U8_PULSE_LOW_MED_MU) {
/*                  if((WL->arad <= (int8_t)ARAD_2G0) && (WL->arad >= -(int8_t)ARAD_1G0)) Clear_State=1; */
                    if(WL->arad >= -(int8_t)ARAD_1G0) {Clear_State=1;}
                }
                else {
/*                  if((WL->arad <= (int8_t)ARAD_2G0) && (WL->arad >= -(int8_t)ARAD_1G0)) Clear_State=1; */
                  #if (__THRDECEL_RESOLUTION_CHANGE==1)
                    if(WL->arad_resol_change >= WL->lcabss16ThresDecel32) {Clear_State=1;}
                  #else
                    if(WL->arad >= WL->thres_decel) {Clear_State=1;}
                  #endif                   
                }

                if((AFZ_OK==0) || (afz>=-(int16_t)U8_PULSE_LOW_MED_MU)) {
                    if((vrselect1-WL->vrad_crt)<=0) {                              /* Changed due to BASALT Cascading 030716 */
                        if((WL->arad >= (int8_t)ARAD_0G0) && (WL->arad < (int8_t)ARAD_3G0)) {Clear_State=0;}
                    }
                }
            }
            else if((vref-WL->vrad) <= (int16_t)(VREF_2_KPH + (vref/50))) {
                if((AFZ_OK==1) && (afz<-(int16_t)U8_PULSE_LOW_MU)) {
                    if(WL->arad >= (int8_t)ARAD_3G0) {Clear_State=1;}
                }
            }
            else if((vref-WL->vrad) <= (int16_t)(VREF_4_KPH + (vref/50))) {
                if((AFZ_OK==1) && (afz<-(int16_t)U8_LOW_MU)) {
                    if(WL->arad >= (int8_t)ARAD_5G0) {Clear_State=1;}
                }
            }
            else if((vref-WL->vrad) <= (int16_t)(VREF_6_KPH + (vref/50))) {
                if((AFZ_OK==1) && (afz<-(int16_t)U8_LOW_MU)) {
                    if(WL->arad >= (int8_t)ARAD_8G0) {Clear_State=1;}
                }
            }
        }
        else { /*end of CAS */
            if(WL->rel_lam >= (int8_t)(-LAM_3P)) {
                if((vrselect1-WL->vrad_crt)<=0) {                              /* Changed due to BASALT Cascading 030716 */
                    if(WL->arad <= (int8_t)ARAD_0G0) {Clear_State=1;}
                }
                else if((WL->arad < (int8_t)ARAD_1G0) && (WL->arad > -(int8_t)ARAD_0G5)) {Clear_State=1;}
                else { }
            }
        }
    }
    else {
        if(CAS_wl==0) {
            if(WL->rel_lam >= (int8_t)(-LAM_2P)) {
                if(AFZ_OK==0) {
/*                  if((WL->arad <= (int8_t)ARAD_0G5) && (WL->arad >= -(int8_t)ARAD_0G5)) Clear_State=1; */
                    if(WL->arad >= -(int8_t)ARAD_0G5) {Clear_State=1;}
                }
                else if(afz>=-(int16_t)U8_PULSE_LOW_MU) {
/*                  if((WL->arad <= (int8_t)ARAD_0G5) && (WL->arad >= -(int8_t)ARAD_0G5)) Clear_State=1; */
                    if(WL->arad >= -(int8_t)ARAD_0G5) {Clear_State=1;}
                }
                else if(afz>=-(int16_t)U8_PULSE_LOW_MED_MU) {
/*                  if((WL->arad <= (int8_t)ARAD_1G5) && (WL->arad >= -(int8_t)ARAD_1G0)) Clear_State=1; */
                    if(WL->arad >= -(int8_t)ARAD_1G0) {Clear_State=1;}
                }
                else {
/*                  if((WL->arad <= (int8_t)ARAD_1G75) && (WL->arad >= -(int8_t)ARAD_1G0)) Clear_State=1; */
                  #if (__THRDECEL_RESOLUTION_CHANGE==1)
                    if(WL->arad_resol_change >= WL->lcabss16ThresDecel32) {Clear_State=1;}
                  #else
                    if(WL->arad >= WL->thres_decel) {Clear_State=1;}
                  #endif                   
                }

                if((vrselect1-WL->vrad_crt)<=0) {                              /* Changed due to BASALT Cascading 030716 */
                    if((WL->arad >= (int8_t)ARAD_0G0) && (WL->arad < (int8_t)ARAD_3G0)) {Clear_State=0;}
                }
            }
            else if(WL->rel_lam >= (int8_t)(-LAM_5P)) {
                if(AFZ_OK==0) {
/*                  if((WL->arad <= (int8_t)ARAD_0G5) && (WL->arad >= -(int8_t)ARAD_0G5)) Clear_State=1; */
                    if(WL->arad >= (int8_t)ARAD_3G0) {Clear_State=1;}
                }
                else if(afz>=-(int16_t)U8_PULSE_LOW_MU) {
/*                  if((WL->arad <= (int8_t)ARAD_0G5) && (WL->arad >= -(int8_t)ARAD_0G5)) Clear_State=1; */
                    if(WL->arad >= (int8_t)ARAD_3G0) {Clear_State=1;}
                }
                else if(afz>=-(int16_t)U8_PULSE_LOW_MED_MU) {
/*                  if((WL->arad <= (int8_t)ARAD_1G5) && (WL->arad >= -(int8_t)ARAD_1G0)) Clear_State=1; */
                    if(WL->arad >= (int8_t)ARAD_3G0) {Clear_State=1;}
                }
                else {
/*                  if((WL->arad <= (int8_t)ARAD_1G75) && (WL->arad >= -(int8_t)ARAD_1G0)) Clear_State=1; */
                    if(WL->arad >= ARAD_3G0) {Clear_State=1;}
                }

                if((vrselect1-WL->vrad_crt)<=0) {                              /* Changed due to BASALT Cascading 030716 */
                    Clear_State=0;
                }
            }
        }
        else {
            if(WL->rel_lam >= (int8_t)(-LAM_2P)) {
                if((vrselect1-WL->vrad_crt)<=0) {                              /* Changed due to BASALT Cascading 030716 */
                    if(WL->arad <= (int8_t)ARAD_0G0) {Clear_State=1;}
                }
                else if((WL->arad < (int8_t)ARAD_0G5) && (WL->arad > -(int8_t)ARAD_0G5)) {Clear_State=1;}
                else { }
            }
        }
    }
 #endif /* __BEND_DETECT_using_SIDEVREF */

    if((Clear_State==1) && (((REAR_WHEEL_wl==0)&&(WL->LFC_UNSTABIL_counter>=L_TIME_30MS)) || ((REAR_WHEEL_wl==1)&&(WL->LFC_UNSTABIL_counter>=L_TIME_70MS))))
    {

/*
        if(CHECK_PEAK_ACCEL_wl==1) {
            if(WL->peak_acc_alt < (int8_t)ARAD_7G0) {
                FORCE_DUMP_wl=1;
            }
            CHECK_PEAK_ACCEL_wl=0;
        }
*/
      #if __AQUA_ENABLE  
        V_INST_80=0;
      #endif

      #if __REAR_MI
        FSF_wl=0;

    /*    if(SPLIT_FSF_wl==1) SPLIT_FSF_wl=0;     //mi03_slip */
      #else
        if(REAR_WHEEL_wl==1) {
            FSF_rl=FSF_rr=0;
        }
        else {FSF_wl=0;}
      #endif

        CAS_wl=0;

        if(GMA_SLIP_wl==0)
        {
            WL->hold_timer_new=0;
        }
/*        WL->time_aft_thrdec=0; */
        WL->state=DETECTION;

        WL->Dump_hold_scan_counter=0;


      #if __STABLE_DUMPHOLD_ENABLE
        WL->lcabsu1StabDumpAndHoldFlg = 0;
      #endif

    }
}

#else /*(__REORGANIZE_STABLE_DCT == DISABLE)*/

void LCABS_vDecideSTABLEState(void)
{
    int16_t bend_diff_compensated=0;
    int16_t bend_slip_compensated=0;
    int8_t Clear_State=0;

    int16_t s16StableDctDVThres;
    int16_t s16AradLowerThres;
    int16_t s16AradHigherThres;

    tempF0=0;

   #if  __TEMP_for_CAS2
    if((vref_limit_dec_flag==1) && (vref>=VREF_30_KPH)) {
        if((AFZ_OK==1)&&(afz>-(int16_t)U8_PULSE_HIGH_MU)) {
          #if   __REAR_D
            if((REAR_WHEEL_wl==0) && (FSF_wl==0) && (vref_limit_dec_counter_frt>=L_TIME_70MS)) {tempF0 = 1;}/*10*/
          #else
            if((REAR_WHEEL_wl==1) && (FSF_wl==0) && (vref_limit_dec_counter_rr>=L_TIME_70MS)) {tempF0 = 1;}/*10*/
          #endif
        }
        if(tempF0==1) {tempW6 =(int16_t)(((int32_t)(WL->vrad_crt - vref)*100)/(int32_t)vref);}
        else {tempW6 = 0;}
    }
   #endif
    if(((AFZ_OK==1)&&(afz<=-(int16_t)U8_PULSE_MEDIUM_MU)) && (BEND_DETECT_flag==1)) {
        if(BEND_DETECT_LEFT==1) {
            if(LEFT_WHEEL_wl==1) {
              #if   __TEMP_for_CAS2
                if(tempF0==0) {
                    bend_diff_compensated = vref - WL->vrad_crt + ((Side_vref_diff*4)/5);
                    bend_slip_compensated = (int16_t)(WL->rel_lam + bend_slip_comp);
                }
                else {
                    bend_diff_compensated = vref - WL->vrad_crt;
                    if(tempW6<(int16_t)WL->rel_lam) {bend_slip_compensated = tempW6;}
                    else {bend_slip_compensated = (int16_t)WL->rel_lam;}
                }
              #else
                bend_diff_compensated = vref - WL->vrad_crt + ((Side_vref_diff*4)/5);
                bend_slip_compensated = (int16_t)(WL->rel_lam + bend_slip_comp);
              #endif
            }
            else {
                bend_diff_compensated = vref - WL->vrad_crt;
              #if   __TEMP_for_CAS2
                if(tempF0==0) {bend_slip_compensated = (int16_t)WL->rel_lam;}
                else {
                    if(tempW6<(int16_t)WL->rel_lam) {bend_slip_compensated = tempW6;}
                    else {bend_slip_compensated = (int16_t)WL->rel_lam;}
                }
              #else
                bend_slip_compensated = (int16_t)(WL->rel_lam + bend_slip_comp);
              #endif
            }
        }
        else if(BEND_DETECT_RIGHT==1) {
            if(LEFT_WHEEL_wl==0) {
              #if   __TEMP_for_CAS2
                if(tempF0==0) {
                    bend_diff_compensated = vref - WL->vrad_crt - ((Side_vref_diff*4)/5);
                    bend_slip_compensated = (int16_t)(WL->rel_lam + bend_slip_comp);
                }
                else {
                    bend_diff_compensated = vref - WL->vrad_crt;
                    if(tempW6<(int16_t)WL->rel_lam) {bend_slip_compensated = tempW6;}
                    else {bend_slip_compensated = (int16_t)WL->rel_lam;}
                }
              #else
                bend_diff_compensated = vref - WL->vrad_crt;
                bend_slip_compensated = (int16_t)(WL->rel_lam + bend_slip_comp);
              #endif
            }
            else {
                bend_diff_compensated = vref - WL->vrad_crt;
              #if   __TEMP_for_CAS2
                if(tempF0==0) {bend_slip_compensated = (int16_t)WL->rel_lam;}
                else {
                    if(tempW6<(int16_t)WL->rel_lam) {bend_slip_compensated = tempW6;}
                    else {bend_slip_compensated = (int16_t)WL->rel_lam;}
                }
              #else
                bend_slip_compensated = (int16_t)(WL->rel_lam + bend_slip_comp);
              #endif
            }
        }
        else {
            bend_diff_compensated = vref - WL->vrad_crt;
          #if   __TEMP_for_CAS2
            if(tempF0==0) {bend_slip_compensated = (int16_t)WL->rel_lam;}
            else {
                if(tempW6<(int16_t)WL->rel_lam) {bend_slip_compensated = tempW6;}
                else {bend_slip_compensated = (int16_t)WL->rel_lam;}
            }
          #else
            bend_slip_compensated = (int16_t)(WL->rel_lam + bend_slip_comp);
          #endif
        }
    }
    else {
        bend_diff_compensated = vref - WL->vrad_crt;
        if(BEND_DETECT==1) {
          #if   __TEMP_for_CAS2
            if(tempF0==0) {bend_slip_compensated = (int16_t)(WL->rel_lam + bend_slip_comp);}
            else {
                if(tempW6<(int16_t)WL->rel_lam) {bend_slip_compensated = tempW6;}
                else {bend_slip_compensated = (int16_t)WL->rel_lam;}
            }
          #else
            bend_slip_compensated = (int16_t)(WL->rel_lam + bend_slip_comp);
          #endif
        }
        else {
          #if   __TEMP_for_CAS2
            if(tempF0==0) {bend_slip_compensated = (int16_t)WL->rel_lam;}
            else {
                if(tempW6<(int16_t)WL->rel_lam) {bend_slip_compensated = tempW6;}
                else {bend_slip_compensated = (int16_t)WL->rel_lam;}
            }
          #else
            bend_slip_compensated = (int16_t)(WL->rel_lam + bend_slip_comp);
          #endif
        }
    }

    /* --- default slip thres ----*/
    tempB0 = -(int8_t)(LAM_2P);
    s16StableDctDVThres = ((vref/50) + VREF_1_KPH);
    
    /* --- default arad thres ----*/
    s16AradLowerThres = (int16_t)WL->thres_decel;
    s16AradHigherThres = 160;/*ARAD_40G0; // Check stable state if arad is inside valid range : discard wheel vibration or oveshoot */
    
    /* --- slip and arad thres calculation per wheel or CAS_wl ----*/
    if(REAR_WHEEL_wl==0) 
    {
        if(CAS_wl==0)
        {
            /* --- Slip in percentage thres ----*/
            if(vref>VREF_100_KPH) {tempB0 = -(int8_t)(LAM_2P);}
            else if(vref>VREF_50_KPH) {tempB0 = -(int8_t)(LAM_3P);}
            else if(vref>VREF_35_KPH) {tempB0 = -(int8_t)(LAM_4P);}
            else  {tempB0 = -(int8_t)(LAM_5P);}

            /* --- Slip in kph thres ----*/
          #if __IMPROVE_DECEL_AT_CERTAIN_HMU

            if(lcabsu1CertainHMu==1)
            {
                s16StableDctDVThres = ((vref/50) + VREF_2_KPH);
            }
            else if((AFZ_OK==1) && (afz<-(int16_t)U8_PULSE_HIGH_MU))
            {
                s16StableDctDVThres = ((vref/50) + VREF_1_5_KPH);
            }
            else
            {
                s16StableDctDVThres = ((vref/50) + VREF_1_KPH);
            }

          #endif

            /* --- arad thres ----*/
            if(AFZ_OK==0)
            {
                s16AradLowerThres = -ARAD_1G0;
            }
            else if(afz>=-(int16_t)U8_PULSE_LOW_MU) {
                s16AradLowerThres = -ARAD_0G5;
            }
            else if(afz>=-(int16_t)U8_PULSE_LOW_MED_MU) {
                s16AradLowerThres = -ARAD_1G0;
            }
            else {
                ;/*s16AradLowerThres = WL->lcabss16ThresDecel32;*/
            }

            if((AFZ_OK==0) || (afz>=-(int16_t)U8_PULSE_LOW_MED_MU))
            {
                if((vrselect1-WL->vrad_crt)<=0) /* Changed due to BASALT Cascading 030716 */
                {
                    if(WL->arad < 0)
                    {
                        s16AradHigherThres = ARAD_0G0;  /* Detection range : s16AradLowerThres ~ arad 0g */
                    }
                    else
                    {
                        s16AradLowerThres = (ARAD_3G0); /* Detection range : s16AradHigherThres */
                    }
                    /*if((WL->arad >= (int8_t)ARAD_0G0) && (WL->arad < (int8_t)ARAD_3G0)) {Clear_State=0;}*/
                }
            }
        }
        else  /* Front wheel, CAS_wl==1 */
        {
            /* --- Slip in percentage thres ----*/
            tempB0 = -(int8_t)(LAM_3P);

            /* --- arad thres ----*/
            if((vrselect1-WL->vrad_crt)<=0) /* Changed due to BASALT Cascading 030716 */
            {
                s16AradLowerThres = -ARAD_2G0;
                s16AradHigherThres = ARAD_0G0;
            }
            else
            {
              #if (__IMPROVE_DECEL_AT_CERTAIN_HMU == ENABLE)
                if(lcabsu1CertainHMu==1)
                {
                    /* divide 1st cycle, nth cycle */
                    s16AradLowerThres =  ARAD_0G0;
                    s16AradHigherThres = ARAD_6G0;
                }
                else
              #endif /*(__IMPROVE_DECEL_AT_CERTAIN_HMU == ENABLE)*/
                {
                    s16AradLowerThres = -ARAD_0G5;
                    s16AradHigherThres = ARAD_1G0;
                }
            }
        }
    }
    else /* Rear wheel */
    {
         if(CAS_wl==0) 
         {
            /* --- Slip in percentage thres ----*/
            if(vref>=VREF_40_KPH) {tempB0 = -(int8_t)(LAM_2P);}
            else if(vref>=VREF_20_KPH) {tempB0 = -(int8_t)(LAM_3P);}
            else {tempB0 = -(int8_t)(LAM_5P);}

            if((WL->peak_slip>=-LAM_20P) || (afz<=-(int16_t)(U8_PULSE_HIGH_MU)))    /******************/
            {
                if(vref>VREF_100_KPH) {tempB0 = -(int8_t)(LAM_2P);}
                else if(vref>VREF_50_KPH) {tempB0 = -(int8_t)(LAM_3P);}
                else if(vref>VREF_35_KPH) {tempB0 = -(int8_t)(LAM_4P);}
                else  {tempB0 = -(int8_t)(LAM_5P);}
            }

            /* --- arad thres ----*/
            if(AFZ_OK==0)
            {
/*                    if(WL->arad >= -(int8_t)ARAD_0G5) {Clear_State=1;} */
    			if(((FL.rel_lam<-LAM_20P)&&(FR.rel_lam<-LAM_20P)) || (RR.rel_lam<-LAM_20P) || (RL.rel_lam<-LAM_20P))
    			{
    			    if(WL->max_arad_instbl >= (int8_t)ARAD_3G0)
    			    {
    			        s16AradLowerThres = -ARAD_0G5;
    			    }
    			    else
    			    {
    			        s16AradLowerThres = ARAD_0G25;
    			    }
    			}
    			else
    			{
    				s16AradLowerThres = -ARAD_0G5;
    			}
            }
            else if(afz>=-(int16_t)U8_PULSE_LOW_MU) {
                s16AradLowerThres = -ARAD_0G5;
            }
            else if(afz>=-(int16_t)U8_PULSE_LOW_MED_MU) {
                s16AradLowerThres = -ARAD_1G0;
            }
            else {
                ; /*s16AradLowerThres = WL->lcabss16ThresDecel32;*/
            }
            
            if((vrselect1-WL->vrad_crt)<=0)
            {                              /* Changed due to BASALT Cascading 030716 */
                if(WL->arad < 0)
                {
                    s16AradHigherThres = ARAD_0G0;  /* Detection range : s16AradLowerThres ~ arad 0g */
                }
                else
                {
                    s16AradLowerThres = (ARAD_3G0); /* Detection range : s16AradHigherThres */
                }
                
                /*if((WL->arad >= (int8_t)ARAD_0G0) && (WL->arad < (int8_t)ARAD_3G0)) {Clear_State=0;}*/
            }
        }
        else /* Rear wheel, CAS_wl==1 */
        {
            /* --- Slip in percentage thres ----*/
            tempB0 = -(int8_t)(LAM_2P);

            /* --- arad thres ----*/
            if((vrselect1-WL->vrad_crt)<=0) {                              /* Changed due to BASALT Cascading 030716 */
                s16AradLowerThres = -ARAD_2G0;
                s16AradHigherThres = ARAD_0G0;
            }
            else
            {
                s16AradLowerThres = -ARAD_0G5;
                s16AradHigherThres = ARAD_0G5;
            }
        }
    }

  #if (__TCS || __ETC || __EDC) && (__RESET_ABS_BY_ACCEL)
    if(lcabsu1SuspctAccelIntention==1)
    {
        if(CAS_wl==0)
        {
            tempW0 = LCABS_s16Interpolation2P(mtp,MTP_10_P,MTP_90_P,LAM_5P,LAM_50P);
        }
        else
        {
            tempW0 = LCABS_s16Interpolation2P(mtp,MTP_10_P,MTP_90_P,LAM_2P,LAM_30P);
        }
        
        tempB0 = (tempB0 - (int8_t)tempW0);
    }
  #endif /*(__TCS || __ETC || __EDC) && (__RESET_ABS_BY_ACCEL)*/

    if( ((bend_diff_compensated <= s16StableDctDVThres) || (bend_slip_compensated>=(int16_t)tempB0))
        && (WL->arad >= s16AradLowerThres) && (WL->arad < s16AradHigherThres) )
    {
        Clear_State=1;
    }

    LCABS_vDecideSTABLEBySideVref();

    if(((Clear_State==1) || (WL->STABLE_for_SIDE_VREF_flag==1)) && (((REAR_WHEEL_wl==0)&&(WL->LFC_UNSTABIL_counter>=L_TIME_30MS)) || ((REAR_WHEEL_wl==1)&&(WL->LFC_UNSTABIL_counter>=L_TIME_70MS))))
    {

/*
        if(CHECK_PEAK_ACCEL_wl==1) {
            if(WL->peak_acc_alt < (int8_t)ARAD_7G0) {
                FORCE_DUMP_wl=1;
            }
            CHECK_PEAK_ACCEL_wl=0;
        }
*/
      #if __AQUA_ENABLE  
        V_INST_80=0;
      #endif

      #if __REAR_MI
        FSF_wl=0;

    /*    if(SPLIT_FSF_wl==1) SPLIT_FSF_wl=0;     //mi03_slip */
      #else
        if(REAR_WHEEL_wl==1) {
            FSF_rl=FSF_rr=0;
        }
        else {FSF_wl=0;}
      #endif

        CAS_wl=0;

        if(GMA_SLIP_wl==0)
        {
            WL->hold_timer_new=0;
        }
/*        WL->time_aft_thrdec=0; */
        WL->state=DETECTION;

        WL->Dump_hold_scan_counter=0;


      #if __STABLE_DUMPHOLD_ENABLE
        WL->lcabsu1StabDumpAndHoldFlg = 0;
      #endif

    }
    
  #if defined (__STAB_DCT_LOGIC_DEBUG)
    if(REAR_WHEEL_wl==0)
    {
        if(LEFT_WHEEL_wl==1)
        {
            AradLowerThresFL            = s16AradLowerThres;
            AradHigherThresFL           = s16AradHigherThres;
            StabDctdVThresFL            = s16StableDctDVThres;
            StabDctSlipThresFL          = tempB0;
            BendDiffCompensatedTempFL   = bend_diff_compensated;
            BendSlipCompensatedTempFL   = bend_slip_compensated;
        }
        else
        {
            AradLowerThresFR            = s16AradLowerThres;
            AradHigherThresFR           = s16AradHigherThres;
            StabDctdVThresFR            = s16StableDctDVThres;
            StabDctSlipThresFR          = tempB0;
            BendDiffCompensatedTempFR   = bend_diff_compensated;
            BendSlipCompensatedTempFR   = bend_slip_compensated;
        }
    }
  #endif /*defined (__STAB_DCT_LOGIC_DEBUG)*/

}
#endif /*(__REORGANIZE_STABLE_DCT == DISABLE)*/

#if (__PRESS_EST && __EST_MU_FROM_SKID)
void LCABS_vGet1stCycMuFromPress(void)
{
  /******** 1st Cycle Skid Press Calculation *******/
    if(WL->ABS==0)
    {
        WL->lcabsu81stCycSkid = (uint8_t)(WL->s16_Estimated_Active_Press/10);
    }
    else { }
  /*************************************************/
  
    if(lcabsu1ValidBrakeSensor==1)
    {
        if(WL->ABS==1)
        {
            if((WL->LFC_zyklus_z<=0) || (AFZ_OK==0))
            {
                if(WL->state==UNSTABLE)
                {
                    if(REAR_WHEEL_wl==0)
                    {
                        if(LEFT_WHEEL_wl==1)
                        {
                            tempB5 = LCABS_s8Get1stCycMuFromPressF(&FL, &FR, &RL, &RR);
                        }
                        else
                        {
                            tempB5 = LCABS_s8Get1stCycMuFromPressF(&FR, &FL, &RR, &RL);
                        }
                    }
                    else
                    {
                        if(LEFT_WHEEL_wl==1)
                        {
                            tempB5 = LCABS_s8Get1stCycMuFromPressR(&FL, &FR, &RL, &RR);
                        }
                        else
                        {
                            tempB5 = LCABS_s8Get1stCycMuFromPressR(&FR, &FL, &RR, &RL);
                        }
                    }
                    
                    if(tempB5 >= 42)       /* 42 ~= Max - (Max-Min)/4 */
                    {
                        WL->lcabsu8PossibleMuFromSkid=U8_PULSE_HIGH_MU;
                    }
                    else if(tempB5 >= 33) /* 33 ~= Max - ((Max-Min)/4)*2 */
                    {
                        WL->lcabsu8PossibleMuFromSkid=U8_PULSE_MED_HIGH_MU;
                    }
                    else if(tempB5 >= 27)
                    {
                        WL->lcabsu8PossibleMuFromSkid=U8_PULSE_MEDIUM_MU; /* Not Decided */
                    }
                    else if(tempB5 >= 22) /* 22 ~= Max - ((Max-Min)/4)*3 */
                    {
                        WL->lcabsu8PossibleMuFromSkid=U8_PULSE_UNDER_LOW_MED_MU; /* Not Decided */
                    }
                    else
                    {
                        WL->lcabsu8PossibleMuFromSkid=U8_PULSE_LOW_MU;
                    }
                }
                else
                {
                    ;
                }
            }
            else
            { /* After 1st cycle and AFZ_OK */
                ;
            }
        }
        else
        { /* ABS_wl==0 */
            WL->lcabsu8PossibleMuFromSkid=0;
        }
    }
    else
    {
        /* If mp sensor error */
        WL->lcabsu8PossibleMuFromSkid=0;
    }
}

int8_t LCABS_s8Get1stCycMuFromPressF(struct W_STRUCT *FI, struct W_STRUCT *FO, struct W_STRUCT *RI, struct W_STRUCT *RO)
{
   /*Skid_MuLevel*/
    tempB0 = LCABS_s8GetFactorFromParam((int16_t)FI->lcabsu81stCycSkid,(int16_t)U8_LOW_MU_SKID_F,(int16_t)U8_HIGH_MU_SKID_F,(int16_t)(U8_HIGH_MU_SKID_F+10));
   /*LPress_MuLevel*/
    tempB1 = LCABS_s8GetFactorFromParam(FI->s16_Estimated_Active_Press,MPRESS_10BAR,MPRESS_40BAR,MPRESS_70BAR);
   /*Slip_MuLevel, Dep. on Skid Press; if Skid Press is high, slip thresholds are lower*/
    if(tempB0>=4)
    {
        tempB2 = LCABS_s8GetFactorFromParam((int16_t)FI->peak_slip,(int16_t)(-S8_LOW_MU_SLIP_F-LAM_15P),(int16_t)(-S8_HIGH_MU_SLIP_F-LAM_15P),(int16_t)(-S8_HIGH_MU_SLIP_F-LAM_5P));
    }
    else if(tempB0>=3)
    {
        tempB2 = LCABS_s8GetFactorFromParam((int16_t)FI->peak_slip,(int16_t)(-S8_LOW_MU_SLIP_F-LAM_10P),(int16_t)(-S8_HIGH_MU_SLIP_F-LAM_10P),(int16_t)(-S8_HIGH_MU_SLIP_F));
    }
    else 
    {
        tempB2 = LCABS_s8GetFactorFromParam((int16_t)FI->peak_slip,(int16_t)(-S8_LOW_MU_SLIP_F),(int16_t)(-S8_HIGH_MU_SLIP_F),(int16_t)(-S8_HIGH_MU_SLIP_F+LAM_5P));
    }
    
   /*Arad_MuLevel, Dep. on Wl. slip*/
    if(FI->peak_slip > -(LAM_10P))
    {
        tempB3 = LCABS_s8GetFactorFromParam(FI->max_arad_instbl,((int16_t)U8_dP_LMu_Dct_AradRef_1st_F-ARAD_6G0),((int16_t)U8_dP_HMu_Dct_AradRef_1st_F-ARAD_1G0),(int16_t)U8_dP_HMu_Dct_AradRef_1st_F);
    }
    else if(FI->peak_slip > -(S8_HIGH_MU_SLIP_F))
    {
        tempB3 = LCABS_s8GetFactorFromParam(FI->max_arad_instbl,((int16_t)U8_dP_LMu_Dct_AradRef_1st_F-ARAD_4G0),(int16_t)U8_dP_HMu_Dct_AradRef_1st_F,((int16_t)U8_dP_HMu_Dct_AradRef_1st_F+ARAD_2G0));
    }
    else if(FI->peak_slip > -(S8_LOW_MU_SLIP_F))
    {
        tempB3 = LCABS_s8GetFactorFromParam(FI->max_arad_instbl,((int16_t)U8_dP_LMu_Dct_AradRef_1st_F-ARAD_2G0),((int16_t)U8_dP_HMu_Dct_AradRef_1st_F+ARAD_2G0),((int16_t)U8_dP_HMu_Dct_AradRef_1st_F+ARAD_3G0));
    }
    else if(FI->peak_slip > -(S8_LOW_MU_SLIP_F+LAM_30P))
    {
        tempB3 = LCABS_s8GetFactorFromParam(FI->max_arad_instbl,(int16_t)U8_dP_LMu_Dct_AradRef_1st_F,((int16_t)U8_dP_HMu_Dct_AradRef_1st_F+ARAD_3G0),((int16_t)U8_dP_HMu_Dct_AradRef_1st_F+ARAD_5G0));
    }
    else
    {
        tempB3 = LCABS_s8GetFactorFromParam(FI->max_arad_instbl,((int16_t)U8_dP_LMu_Dct_AradRef_1st_F+ARAD_2G0),((int16_t)U8_dP_HMu_Dct_AradRef_1st_F+ARAD_5G0),((int16_t)U8_dP_HMu_Dct_AradRef_1st_F+ARAD_7G0));
    }

   /* Adaptation Per Other Front Wheel */
    if((FO->ABS==1)&&(tempB2<=2)&&(tempB0>=3)&&(FO->lcabsu81stCycSkid > U8_HIGH_MU_SKID_F))
    {
        if((tempB0>=4) && (FO->lcabsu81stCycSkid > (U8_HIGH_MU_SKID_F+10)))
        {
            if(((FO->peak_slip > -(S8_HIGH_MU_SLIP_F+LAM_30P))&&((FI->peak_slip+FO->peak_slip) > -((S8_HIGH_MU_SLIP_F*2)+LAM_40P))))
            {
                tempB2 = tempB2+2;
            }
            else { }
        }
        else
        {
            if(((FO->peak_slip > -(S8_HIGH_MU_SLIP_F+LAM_20P))&&((FI->peak_slip+FO->peak_slip) > -((S8_HIGH_MU_SLIP_F*2)+LAM_20P))))
            {
                tempB2 = tempB2+1;
            }
            else { }
        }
    }
    else { }

   /* Factor Calculation */
    if(FI->max_arad_instbl > 0)
    {
        lcabss8temp0 = (tempB0*5)+(tempB1*2)+(tempB2*2)+(tempB3*4);
    }
    else
    {
        lcabss8temp0 = (tempB0*7)+(tempB1*4)+(tempB2*2);
    }                                       /* tempB0 : Skid_MuLevel 
                                               tempB1 : LPress_MuLevel 
                                               tempB2 : Slip_MuLevel 
                                               tempB3 : Arad_MuLevel   */
    return lcabss8temp0;
}

int8_t LCABS_s8Get1stCycMuFromPressR(struct W_STRUCT*FI, struct W_STRUCT*FO, struct W_STRUCT*RI, struct W_STRUCT*RO)
{
   /*Skid_MuLevel*/
    tempB0 = LCABS_s8GetFactorFromParam((int16_t)RI->lcabsu81stCycSkid,(int16_t)U8_LOW_MU_SKID_R,(int16_t)U8_HIGH_MU_SKID_R,(int16_t)(U8_HIGH_MU_SKID_R+10));
   /*LPress_MuLevel*/
    tempB1 = LCABS_s8GetFactorFromParam(RI->s16_Estimated_Active_Press,MPRESS_10BAR,MPRESS_30BAR,MPRESS_50BAR);
   /*Slip_MuLevel*/
    if(tempB0>=4)
    {
        tempB2 = LCABS_s8GetFactorFromParam((int16_t)RI->peak_slip,(int16_t)(-S8_LOW_MU_SLIP_F-LAM_15P),(int16_t)(-S8_HIGH_MU_SLIP_F-LAM_15P),(int16_t)(-S8_HIGH_MU_SLIP_F-LAM_5P));
    }
    else if(tempB0>=3)
    {
        tempB2 = LCABS_s8GetFactorFromParam((int16_t)RI->peak_slip,(int16_t)(-S8_LOW_MU_SLIP_F-LAM_10P),(int16_t)(-S8_HIGH_MU_SLIP_F-LAM_10P),(int16_t)(-S8_HIGH_MU_SLIP_F));
    }
    else 
    {
        tempB2 = LCABS_s8GetFactorFromParam((int16_t)RI->peak_slip,(int16_t)(-S8_LOW_MU_SLIP_F),(int16_t)(-S8_HIGH_MU_SLIP_F),(int16_t)(-S8_HIGH_MU_SLIP_F+LAM_5P));
    }
    
   /*Arad_MuLevel*/
    if(RI->peak_slip > -(S8_HIGH_MU_SLIP_R))
    {
        tempB3 = LCABS_s8GetFactorFromParam(RI->max_arad_instbl,ARAD_1G0,ARAD_3G0,ARAD_5G0);
    }
    else if(RI->peak_slip > -(S8_LOW_MU_SLIP_R))
    {
        tempB3 = LCABS_s8GetFactorFromParam(RI->max_arad_instbl,ARAD_3G0,ARAD_5G0,ARAD_7G0);
    }
    else if(RI->peak_slip > -(S8_LOW_MU_SLIP_R+LAM_30P))
    {
        tempB3 = LCABS_s8GetFactorFromParam(RI->max_arad_instbl,ARAD_5G0,ARAD_7G0,ARAD_10G0);
    }
    else
    {
        tempB3 = LCABS_s8GetFactorFromParam(RI->max_arad_instbl,ARAD_7G0,ARAD_10G0,ARAD_15G0);
    }

   /* FrontWheel_MuLevel */
    tempB4 = LCABS_s8GetFactorFromParam((int16_t)FI->lcabsu8PossibleMuFromSkid,LMU_FACTOR,MHMU_FACTOR,HMU_FACTOR);

   /* Factor Calculation */
    if(RI->max_arad_instbl > 0)
    {
        lcabss8temp0 = (tempB0*4)+(tempB1*2)+(tempB2*2)+(tempB3*4)+tempB4;
    }
    else
    {
        lcabss8temp0 = (tempB0*6)+(tempB1*4)+(tempB2*2)+tempB4;
    }                                   /* tempB0 : Skid_MuLevel 
                                           tempB1 : LPress_MuLevel 
                                           tempB2 : Slip_MuLevel 
                                           tempB3 : Arad_MuLevel
                                           tempB4 : FrontWheel_MuLevel   */
    return lcabss8temp0;
}

int8_t LCABS_s8GetFactorFromParam(int16_t input,int16_t LMuParam, int16_t MHMuParam, int16_t HMuParam)
{
    if(input >= HMuParam)
    {
        lcabss8temp4 = 4;
    }
    else if(input >= MHMuParam)
    {
        lcabss8temp4 = 3;
    }
    else if(input >= LMuParam)
    {
        lcabss8temp4 = 2;
    }
    else
    {
        lcabss8temp4 = 1;
    }
    
    return lcabss8temp4;
}

#endif

#if __ABS_RBC_COOPERATION
void LCABS_vDecidePressRecoveryBfAbs(void)
{
	int16_t s16FastRecoverThresPress = LCABS_s16LimitMinMax((int16_t)(lcans16AhbMcTargetPress/10), (int16_t)MPRESS_3BAR, (int16_t)MPRESS_20BAR);
	int16_t s16RefMpress,s16WLPressDelayRef;
	int16_t s16DctMPFastRecovery;
		
  #if(__HEV_MP_MOVING_AVG==ENABLE)
	s16RefMpress = laabss16MPAvg;
  #else
	s16RefMpress = lcabss16RefCircuitPress;
  #endif		
	
	#if (__AHB_SYSTEM==ENABLE) && (__AHB_GEN3_SYSTEM == DISABLE)
	if((ccu1AHBSubTimeout_susFlg == 0) && (fu1SubCanBusOff == 0))
	#endif /* __AHB_SYSTEM==ENABLE */
	{
		if(ABS_fz==1)
		{
			lcabss16AbsOnCnt = LCABS_s16LimitMinMax((int16_t)(lcabss16AbsOnCnt+L_1SCAN), 0, (int16_t)L_TIME_1MIN);
			if(lcabss16AbsOnMcTargetPress==0) 
			{
				lcabss16AbsOnMcTargetPress = LCABS_s16LimitMinMax(s16RefMpress, (int16_t)MPRESS_1BAR, (int16_t)MPRESS_200BAR);
				lcabss16AbsOnMcTargetPress = lcabss16AbsOnMcTargetPress + lcans16AhbRbcPress;
			}
		}
		else
		{
			lcabss16AbsOnCnt = 0;
			lcabss16AbsOnMcTargetPress = 0;
		}
		

		s16DctMPFastRecovery = 0;

		if((ABS_fz==1) && (lcabsu1FastPressRecoveryBfAbs==1))
		{
    		s16WLPressDelayRef = LCABS_s16Interpolation2P(s16RefMpress,MPRESS_20BAR,MPRESS_80BAR,L_TIME_50MS,L_TIME_0MS);

		    if( (s16RefMpress>=lcans16AhbMcTargetPress) || (s16RefMpress>=lcabss16AbsOnMcTargetPress) )
		    {
		        if(lcabsu8PressRecoveryDelayTime >= s16WLPressDelayRef)
		        {
		            lcabsu8PressRecoveryDelayTime = 0;
		            s16DctMPFastRecovery = 1;
		        }
		        else
		        {
		            lcabsu8PressRecoveryDelayTime = lcabsu8PressRecoveryDelayTime + 1;
		        }
		    }
		    else
		    {
		        lcabsu8PressRecoveryDelayTime = 0;
		    }
    	}
    	else
    	{
		    lcabsu8PressRecoveryDelayTime = 0;
    	}
		
		if(lcabsu1FastPressRecoveryBfAbs==1)
		{
			if(ABS_fz==1)
			{
				if((lcabss16AbsOnCnt>=L_TIME_1000MS) || (s16DctMPFastRecovery==1))
				{
					lcabsu1FastPressRecoveryBfAbs = 0;
				}
			}
			else
			{
				if(lcanu1AhbRbcAct==0)
				{
					lcabsu1FastPressRecoveryBfAbs = 0;
				}
				else
				{
					if((lcans16AhbMcTargetPress-s16RefMpress)>=s16FastRecoverThresPress)
					{
						;
					}
					else
					{
						lcabsu1FastPressRecoveryBfAbs = 0;
					}
				}
			}
		}
		else
		{
			if(lcanu1AhbRbcAct==0)
			{
				;
			}
			else
			{
				if((lcans16AhbMcTargetPress-s16RefMpress)>=s16FastRecoverThresPress)
				{
					lcabsu1FastPressRecoveryBfAbs = 1;
				}
				else
				{
					;
				}
			}
		}
	
		if(ABS_fz==0)
		{
			if(lcabsu1FastPressRecoveryBfAbs==1)
			{
				lcabss16RBCPressBfABS = LCABS_s16LimitMinMax(lcans16AhbRbcPress, MPRESS_1BAR, MPRESS_30BAR);			
			}
			else
			{
				lcabss16RBCPressBfABS = 0;
			}
		}
		else
		{
			if((FL.LFC_zyklus_z>=1) && (FR.LFC_zyklus_z>=1))
			{
				lcabss16RBCPressBfABS = 0;
			}
			else if(lcabss16AbsOnCnt>=L_TIME_1000MS)
			{
				lcabss16RBCPressBfABS = 0;
			}
			else
			{
				;
			}
		}
	}
	#if (__AHB_SYSTEM==ENABLE) && (__AHB_GEN3_SYSTEM == DISABLE)
	else
	{
		lcabsu1FastPressRecoveryBfAbs = 0;
		lcabss16AbsOnCnt = 0;
		lcabss16AbsOnMcTargetPress = 0;
		lcabss16RBCPressBfABS = 0;
	}
	#endif /* (__AHB_SYSTEM==ENABLE) && (__AHB_GEN3_SYSTEM == DISABLE) */
}
#endif	/* #if __ABS_RBC_COOPERATION */

#if __SLIGHT_BRAKING_COMPENSATION
void LCABS_vDecideBrakingStatus(void)
{
    if((lcabsu1ValidBrakeSensor==1) && (BRAKE_BY_MPRESS_FLG==1))
    {	
        if(lsesps16EstBrkPressByBrkPedalF <= S16_SLIGHT_BRAKING_PRESS)
        {
            if(lcabsu8slightbrkcnt < U8_TYPE_MAXNUM)
            {
            	lcabsu8slightbrkcnt = lcabsu8slightbrkcnt + 1;
            	if(lcabsu8slightbrkcnt >= U8_SLIGHT_BRAKING_DET_TIME)
            	{
            		lcabsu1SlightBraking = 1;
            	}
            	else{}
            }
            else{}

            if((lsesps16EstBrkPressByBrkPedalF <= S16_VERY_SLIGHT_BRAKING_PRESS) && (lcabsu1SlightBraking==1))
            {
                lcabsu1VerySlightBraking = 1;
            }
            else if (lsesps16EstBrkPressByBrkPedalF >= (S16_VERY_SLIGHT_BRAKING_PRESS+MPRESS_5BAR))
            {
                lcabsu1VerySlightBraking = 0;
            }
            else { }
        }
        else if (lsesps16EstBrkPressByBrkPedalF >= (S16_SLIGHT_BRAKING_PRESS+MPRESS_5BAR))
        {
            lcabsu1SlightBraking = 0;
            lcabsu1VerySlightBraking = 0;
            lcabsu8slightbrkcnt = 0;
        }
        else{}
    }
    else
    {
        lcabsu1SlightBraking = 0;
        lcabsu1VerySlightBraking = 0;
        lcabsu8slightbrkcnt = 0;
    }
}
#endif	/* #if __SLIGHT_BRAKING_COMPENSATION */

  #if __VDC
void LCABS_vCheckMpressSlope(void)
{
    if(lcabsu1ValidBrakeSensor==1)
    {
    	lcabss16MpressSlope = (MCpress[0]-MCpress[2]);
    	if(lcabss16MpressSlope>0)
    	{
    		if(lcabss16MpressSlope>lcabss16MpressSlopePeak)
    		{
    			lcabss16MpressSlopePeak = lcabss16MpressSlope;
    		}
    	}
    	else
    	{
    		lcabss16MpressSlopePeak = 0;
    	}
    }
    else
    {
        lcabss16MpressSlope = 0;
        lcabss16MpressSlopePeak = 0;
    }
}
  #endif /*__VDC*/

  #if (L_CONTROL_PERIOD>=2)
uint8_t LCABS_u8CountDumpNum(uint8_t u8DumpCounter, uint8_t s8DumpCase)
{
    if(u8DumpCounter < U8_TYPE_MAXNUM)
    {
        if(s8DumpCase==U8_L_DUMP_CASE_11)
        {
            u8DumpCounter = u8DumpCounter + L_CONTROL_PERIOD;
        }
        else if((s8DumpCase==U8_L_DUMP_CASE_10)||(s8DumpCase==U8_L_DUMP_CASE_01))
        {
        	u8DumpCounter = u8DumpCounter + (L_CONTROL_PERIOD/2);
        }
        else
        {
            ;/*u8DumpCounter = u8DumpCounter;*/
        }
    }
    
    return u8DumpCounter;
}

uint8_t LCABS_u8DecountStableDumpNum(uint8_t u8StableDumpCounter)
{
    if(u8StableDumpCounter>=L_CONTROL_PERIOD)
    {
        u8StableDumpCounter = u8StableDumpCounter - L_CONTROL_PERIOD;
        WL->lcabss8DumpCase=U8_L_DUMP_CASE_11;
    }
    else if (u8StableDumpCounter>=(L_CONTROL_PERIOD/2))
    {
        u8StableDumpCounter = u8StableDumpCounter - (L_CONTROL_PERIOD/2);
        WL->lcabss8DumpCase=U8_L_DUMP_CASE_10;
    }
    else { }
    
    return u8StableDumpCounter;
}
  #endif /*(L_CONTROL_PERIOD>=2)*/

  #if __ABS_RBC_COOPERATION
static void LCABS_vCheckWheelState(void)
{
	int16_t s16LeftWheelSpdDiff, s16RightWheelSpdDiff;

	s16LeftWheelSpdDiff = vrad_crt_rl - vrad_crt_fl;
	s16RightWheelSpdDiff = vrad_crt_rr - vrad_crt_fr;
	
	if((BRAKE_BY_MPRESS_FLG==1) && (lcabsu1FrontWheelSlip==0))
	{
		if(arad_fl<=-ARAD_1G0)
		{
			if((s16LeftWheelSpdDiff>=lis8FRONT_SLIP_DET_THRES_VDIFF) || ((vref >= VREF_30_KPH) && (rel_lam_fl<=-lis8FRONT_SLIP_DET_THRES_SLIP)))
			{
				lcabsu1FrontWheelSlip = 1;
			}
			else{}
		}
		else{}

		if(arad_fr<=-ARAD_1G0)
		{
			if((s16RightWheelSpdDiff>=lis8FRONT_SLIP_DET_THRES_VDIFF) || ((vref >= VREF_30_KPH) && (rel_lam_fr<=-lis8FRONT_SLIP_DET_THRES_SLIP)))
			{
				lcabsu1FrontWheelSlip = 1;
			}
			else{}
		}
		else{}
	}
	else if(BRAKE_BY_MPRESS_FLG==0)
	{
		lcabsu1FrontWheelSlip = 0;
	}
	else{}
}
  #endif

static void LCABS_vDecideBuiltRisePMode(uint8_t u8BuiltHoldRef, uint8_t u8WheelCtrlMode)
{
    int8_t u8BuiltHoldRefTemp = 0;

	/* Decide pulse-up pattern per wheel ctrl mode strategy */

    if(u8WheelCtrlMode == WHEEL_CTRL_YMR_ABS)
	{
		u8BuiltHoldRefTemp = u8BuiltHoldRef;
	}
	else
	{
		if(WL->s0_reapply_counter==0)
		{
			u8BuiltHoldRefTemp = 0;
		}
		else
		{
			u8BuiltHoldRefTemp = u8BuiltHoldRef;
		}
	}


	/* ABS pulse-up pattern  */

	if(WL->hold_timer_new >= u8BuiltHoldRefTemp)
    {
        WL->hold_timer_new=0;
        WL->flags=101;
        WHEEL_ACT_MODE = BUILT_MODE;
    }
    else {
        WL->hold_timer_new = WL->hold_timer_new + L_1SCAN;
        WL->flags=110;
        WHEEL_ACT_MODE = HOLD_MODE;
    }
}

static void LCABS_vDecideVarsAtRiseHld(int16_t s16RiseHoldMode)
{
    /* Update variables at every hold after rise */
    
    WL->lcabss16RiseDeltaWhlP = WL->s16_Estimated_Active_Press - WL->lcabss16EstWhlPAftDump;

    /* If forced hold is needed, set commands for actuation & hold variables for further rise */
    if(s16RiseHoldMode==S16_HOLD_MODE_FORCED)
    {
            /* If there is plenty of remaining rise from target, compensate further rise */
        if((WL->LFC_s0_reapply_counter==1) && ((WL->lcabss16TarDeltaWhlP - WL->lcabss16RiseDeltaWhlP) > MPRESS_10BAR))
        {
            WL->hold_timer_new = WL->hold_timer_new_ref;
            WL->LFC_s0_reapply_counter = 0;
            WL->lcabsu1UpdDeltaPRiseAftHld = 1;
        }
        else
        {
            WL->hold_timer_new = WL->hold_timer_new + L_1SCAN; /* What to do with hold time...? */
            WL->lcabsu1UpdDeltaPRiseAftHld = 0;
        }
    }
    else
    {
        ;
    }
}

static void LCABS_vUpdVarsAftDump(void)
{
    if(lcabsu1ValidBrakeSensor==1)
    {
        if(WL->s16_Estimated_Active_Press < WL->Estimated_Press_SKID)
        {
            WL->lcabss16EstWhlPAftDump = WL->s16_Estimated_Active_Press;
            WL->lcabsu1CaptureEstPressAftDump = 1;
            WL->lcabss16DumpDeltaWhlP = WL->Estimated_Press_SKID - WL->lcabss16EstWhlPAftDump;
        }
        else
        {
            ; /*hold values*/
        }
    }
    else
    {
        WL->lcabss16EstWhlPAftDump = MPRESS_0BAR; /* default value */
        WL->lcabss16DumpDeltaWhlP = MPRESS_30BAR; /* default value */
    }
}

static void LCABS_vCheckForcedHldForStrRcvr(uint8_t u8ForcedholdForStrkrecvr)
{
 	if(WL->lcabsu8ForcedHldForStrkRecvrCnt < L_10SCAN)
	{
		if(u8ForcedholdForStrkrecvr == 1)
		{
			WL->lcabsu1ForcedHldForStrkRecvr = 1; /*for monitoring*/
			WL->hold_timer_new = 0;
			WL->lcabsu8ForcedHldForStrkRecvrCnt = (WL->lcabsu8ForcedHldForStrkRecvrCnt < 5) ? (WL->lcabsu8ForcedHldForStrkRecvrCnt + 1) : WL->lcabsu8ForcedHldForStrkRecvrCnt;
		}
		else {}
	}
 	else {}
}

static void LCABS_vInitializeMstPatternVars(void)
{
	WL_WP->lcabss16WhlTarDP = MPRESS_0BAR;
	WL_WP->lcabss16WhlBaseTarDP = MPRESS_0BAR; /*Mon*/
	WL_WP->lcabss16InitTotalDeltaP = MPRESS_0BAR;
	WL_WP->lcabss16WhlDesriedTarDP = MPRESS_0BAR;
	WL_WP->lcabss16InABSInitRiseDPRefEstP = MPRESS_0BAR; /*Mon*/
	WL_WP->lcabss16InABSInitRiseDPRefDump = MPRESS_0BAR; /*Mon*/
	WL_WP->lcabss16WhlAccumulatedTarDP = MPRESS_0BAR; /*Mon*/
	WL_WP->lcabss16WhlCompTarDP = MPRESS_0BAR; /*Mon*/
	WL_WP->lcabss16WhlCyclicCompDP = MPRESS_0BAR;
	WL_WP->lcabss16WhlSpecialCompDP = MPRESS_0BAR; /*Mon*/
	WL_WP->lcabss16WhlMPCyclicCompDP = MPRESS_0BAR; /*Mon*/
	WL_WP->lcabss16InABSNthPressError = MPRESS_0BAR;
	WL_WP->lcabss16WhlInABSDesiredNthTarDP = MPRESS_0BAR;

	WL_WP->lcabsu1InABSInitialRiseDone = 0;
	WL_WP->lcabsu1InABSNthReduceHoldTimReq = 0;
	WL_WP->lcabsu16InABSNthRiseCnt= 0;
}

/******************************************************************************
* FUNCTION NAME:      LCABS_vDecideWhlTargetP
* CALLED BY:          LCABS_vCallControl()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       WL->lcabss16TarDeltaWhlP, WL->lcabss16TarWhlP
* Description:        Decide Delta Press by Delta Press
******************************************************************************/
void LCABS_vDecideWhlTargetP(void)
{
	FL_WP.lcabss16WhlTarDP = LCABS_s16DecideWhlTarDeltaP(&FL, &FL_WP);
	FR_WP.lcabss16WhlTarDP = LCABS_s16DecideWhlTarDeltaP(&FR, &FR_WP);
	RL_WP.lcabss16WhlTarDP = LCABS_s16DecideWhlTarDeltaP(&RL, &RL_WP);
	RR_WP.lcabss16WhlTarDP = LCABS_s16DecideWhlTarDeltaP(&RR, &RR_WP);

	LCABS_vLimitDiffofRearTardP();

	FL.lcabss16TarDeltaWhlP = FL_WP.lcabss16WhlTarDP;
	FR.lcabss16TarDeltaWhlP = FR_WP.lcabss16WhlTarDP;
	RL.lcabss16TarDeltaWhlP = RL_WP.lcabss16WhlTarDP;
	RR.lcabss16TarDeltaWhlP = RR_WP.lcabss16WhlTarDP;

	FL.lcabsu1InABSInitialRiseDone = FL_WP.lcabsu1InABSInitialRiseDone;
	FR.lcabsu1InABSInitialRiseDone = FR_WP.lcabsu1InABSInitialRiseDone;
	RL.lcabsu1InABSInitialRiseDone = RL_WP.lcabsu1InABSInitialRiseDone;
	RR.lcabsu1InABSInitialRiseDone = RR_WP.lcabsu1InABSInitialRiseDone;

	FL.lcabsu1InABSNthReduceHoldTimReq = FL_WP.lcabsu1InABSNthReduceHoldTimReq;
	FR.lcabsu1InABSNthReduceHoldTimReq = FR_WP.lcabsu1InABSNthReduceHoldTimReq;
	RL.lcabsu1InABSNthReduceHoldTimReq = RL_WP.lcabsu1InABSNthReduceHoldTimReq;
	RR.lcabsu1InABSNthReduceHoldTimReq = RR_WP.lcabsu1InABSNthReduceHoldTimReq;

	LCABS_vCalcWhlTarPress(&FL, &FL_WP);
	LCABS_vCalcWhlTarPress(&FR, &FR_WP);
	LCABS_vCalcWhlTarPress(&RL, &RL_WP);
	LCABS_vCalcWhlTarPress(&RR, &RR_WP);

	FL.lcabss16TarWhlP = FL_WP.lcabss16WhlTarPress;
	FR.lcabss16TarWhlP = FR_WP.lcabss16WhlTarPress;
	RL.lcabss16TarWhlP = RL_WP.lcabss16WhlTarPress;
	RR.lcabss16TarWhlP = RR_WP.lcabss16WhlTarPress;

}

/******************************************************************************
* FUNCTION NAME:      LCABS_s16DecideWhlTarDeltaP
* CALLED BY:          LCABS_vDecideWhlTargetP()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       s16WhlTarDP
* Description:        Decide Delta Press by Delta Press
******************************************************************************/
static int16_t LCABS_s16DecideWhlTarDeltaP(struct W_STRUCT *WL_temp, Whl_TP_t *WL_WP_temp)
{
	int16_t s16BaseTarDP = MPRESS_0BAR;
	int16_t s16CompTarDP = MPRESS_0BAR;
	int16_t s16WhlTarDP = MPRESS_0BAR;

	WL = WL_temp;
	WL_WP = WL_WP_temp;

	if((ABS_fz == 0) || (WL->WheelCtrlMode == WHEEL_CTRL_ABS_FADE_OUT) || (WL->WheelCtrlMode == WHEEL_CTRL_NON))
	{
		LCABS_vInitializeMstPatternVars();
	}

	s16BaseTarDP = LCABS_s16DecideBaseTarDeltaP(WL->WheelCtrlMode);
	WL_WP->lcabss16WhlBaseTarDP = s16BaseTarDP; /*Mon*/

	s16CompTarDP = LCABS_s16DecideCompTarDeltaP();
	WL_WP->lcabss16WhlCompTarDP = s16CompTarDP;  /*Mon*/

	s16WhlTarDP = s16BaseTarDP + s16CompTarDP;

	return s16WhlTarDP;
}

/******************************************************************************
* FUNCTION NAME:      LCABS_s16DecideBaseTarDeltaP
* CALLED BY:          LCABS_s16DecideWhlTarDeltaP()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       s16BaseTarDeltaP
* Description:        Decide Base Delta Press by Delta Press
******************************************************************************/
static int16_t LCABS_s16DecideBaseTarDeltaP(uint8_t u8WhlCtrlMode)
{
    int16_t s16BaseTarDeltaP = MPRESS_0BAR;

	switch(u8WhlCtrlMode)
	{
		case WHEEL_CTRL_ESC_COOP:
			s16BaseTarDeltaP = LCABS_s16DecideCommonBaseTarDP(MPRESS_0BAR, MPRESS_5BAR);
		break;

		case WHEEL_CTRL_EBD:
	    	s16BaseTarDeltaP = LCABS_s16DecideCommonBaseTarDP(MPRESS_0BAR, S8_LFC_Conven_Reapply_DP_R_EBD); /*done / TODO Consider EBD LFC Rise*/
		break;

		case WHEEL_CTRL_YMR_ABS:
	    	s16BaseTarDeltaP = LCABS_s16DecideYMRTarDP();
		break;

		case WHEEL_CTRL_MSL_REAR:
	    	s16BaseTarDeltaP = LCABS_s16DecideCommonBaseTarDP(MPRESS_0BAR, MPRESS_5BAR);
		break;

		case WHEEL_CTRL_OUT_ABS:
	    	s16BaseTarDeltaP = LCABS_s16DecideCommonBaseTarDP(MPRESS_0BAR, MPRESS_5BAR);
		break;

		case WHEEL_CTRL_UNSTABLE_REAPPLY:
	    	s16BaseTarDeltaP = LCABS_s16DecideUnstRiseTarDP();
		break;

		case WHEEL_CTRL_NORMAL_ABS:
	    	s16BaseTarDeltaP = LCABS_s16DecideInABSTarDP();
		break;

		case WHEEL_CTRL_ABS_FADE_OUT:
	    	s16BaseTarDeltaP = LCABS_s16DecideCommonBaseTarDP(MPRESS_0BAR, MPRESS_0BAR);
		break;

		case WHEEL_CTRL_CBS:
	    	s16BaseTarDeltaP = LCABS_s16DecideCommonBaseTarDP(MPRESS_0BAR, MPRESS_0BAR);
		break;

		case WHEEL_CTRL_NON:
	    	s16BaseTarDeltaP = LCABS_s16DecideCommonBaseTarDP(MPRESS_0BAR, MPRESS_0BAR);
		break;

	    default:
	    	s16BaseTarDeltaP = LCABS_s16DecideCommonBaseTarDP(MPRESS_0BAR, MPRESS_0BAR);
	    break;
	}

    LCABS_vDecideDeltaPCtrlVariable();

    return s16BaseTarDeltaP;
}

/******************************************************************************
* FUNCTION NAME:      LCABS_vDecideDeltaPCtrlVariable
* CALLED BY:          LCABS_s16DecideBaseTarDeltaP()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       void
* Description:        Decide delta Pressure control variables
******************************************************************************/
static void LCABS_vDecideDeltaPCtrlVariable(void)
{
    if((WL->WheelCtrlMode != WHEEL_CTRL_NORMAL_ABS) && (WL->WheelCtrlMode != WHEEL_CTRL_UNSTABLE_REAPPLY))
    {
    	WL_WP->lcabsu1InABSNthReduceHoldTimReq = 0;
    	WL_WP->lcabss16InABSNthPressError = MPRESS_0BAR;
    }

    if((WL->WheelCtrlMode == WHEEL_CTRL_NON) || (WL->WheelCtrlMode == WHEEL_CTRL_CBS) || (WL->WheelCtrlMode == WHEEL_CTRL_ABS_FADE_OUT))
    {
    	WL->lcabsu8ForcedHldForStrkRecvrCnt = 0;
    }

	if(AV_DUMP_wl==1)
	{
		WL->lcabsu8ForcedHldForStrkRecvrCnt = 0;
		WL_WP->lcabss16InABSInitRiseDPRefDump = MPRESS_0BAR; /*Mon*/
		WL_WP->lcabss16InABSInitRiseDPRefEstP = MPRESS_0BAR; /*Mon*/
		WL_WP->lcabss16WhlSpecialCompDP = MPRESS_0BAR; /*Mon*/

		if((WL->WheelCtrlMode == WHEEL_CTRL_NORMAL_ABS) || (WL->WheelCtrlMode == WHEEL_CTRL_UNSTABLE_REAPPLY))
		{
			;
		}
		else
		{
			WL_WP->lcabss16InABSNthPressError = MPRESS_0BAR;
		}
	}
	else if(AV_HOLD_wl==1)
	{
	    WL_WP->lcabss16InABSInitRiseDPRefDump = MPRESS_0BAR; /*Mon*/
	    WL_WP->lcabss16InABSInitRiseDPRefEstP = MPRESS_0BAR; /*Mon*/
	    WL_WP->lcabss16WhlSpecialCompDP = MPRESS_0BAR; /*Mon*/
	    
	    if((WL->WheelCtrlMode == WHEEL_CTRL_NORMAL_ABS) || (WL->WheelCtrlMode == WHEEL_CTRL_UNSTABLE_REAPPLY))
		{
	    	if(WL->s0_reapply_counter >= 1)
	    	{
	    		WL_WP->lcabss16InABSNthPressError = (WL->lcabss16EstWhlPAftDump + WL_WP->lcabss16WhlDesriedTarDP) - WL->s16_Estimated_Active_Press;
	    	}
	    	else {}
		}
		else
		{
			WL_WP->lcabss16InABSNthPressError = MPRESS_0BAR;
		}
	}
	else
	{
			WL->lcabsu8ForcedHldForStrkRecvrCnt = 0;
		}
}

/******************************************************************************
* FUNCTION NAME:      LCABS_s16DecideCommonBaseTarDP
* CALLED BY:          LCABS_s16DecideBaseTarDeltaP()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       s16CommonTarDeltaWhlP
* Description:        Decide Target Delta Press During Cooperate With ESC by Delta Press
******************************************************************************/
static int16_t LCABS_s16DecideCommonBaseTarDP(int16_t s16DumpTargetDP, int16_t s16RiseTargetDP)
{
	int16_t s16CommonTarDeltaWhlP = MPRESS_0BAR;

	if(AV_DUMP_wl==1)
	{
		s16CommonTarDeltaWhlP = s16DumpTargetDP;
	}
	else if(AV_HOLD_wl==1)
	{
		s16CommonTarDeltaWhlP = MPRESS_0BAR;
	}
	else
	{
		s16CommonTarDeltaWhlP = s16RiseTargetDP;
	}

	return s16CommonTarDeltaWhlP;
}

/******************************************************************************
* FUNCTION NAME:      LCABS_s16DecideYMRRiseDP
* CALLED BY:          LCABS_s16DecideBaseTarDeltaP()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       s16YMRBaseTarDeltaWhlP
* Description:        Decide YMR Target Delta Press by Delta Press
******************************************************************************/
static int16_t LCABS_s16DecideYMRTarDP(void)
{
	int16_t s16YMRBaseTarDeltaWhlP = MPRESS_0BAR;

	if(AV_DUMP_wl==1)
	{
		s16YMRBaseTarDeltaWhlP = MPRESS_0BAR;
	}
	else if(AV_HOLD_wl==1)
	{
		s16YMRBaseTarDeltaWhlP = MPRESS_0BAR;
	}
	else
	{
		s16YMRBaseTarDeltaWhlP = LCABS_s16DecideYMRRiseDP();
	}

	return s16YMRBaseTarDeltaWhlP;
}

/******************************************************************************
* FUNCTION NAME:      LCABS_s16DecideYMRRiseDP
* CALLED BY:          LCABS_s16DecideBaseTarDeltaP()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       s16YMRRiseDP
* Description:        Decide YMR Rise Target Delta Press by Delta Press
******************************************************************************/
static int16_t LCABS_s16DecideYMRRiseDP(void)
{
	int16_t s16YMRRiseDP = MPRESS_0BAR;

	if(vref>=S16_GMA_H_SPEED)
	{
		if(WL->s0_reapply_counter <= 1)
		{
			s16YMRRiseDP = S8_GMA_RiseDP_H_SPEED_PHASE1; /*S8_GMA_RiseTime_H_SPEED_PHASE1*/
		}
		else if(WL->s0_reapply_counter <= 4)
		{
			s16YMRRiseDP = S8_GMA_RiseDP_H_SPEED_PHASE2; /*S8_GMA_RiseTime_H_SPEED_PHASE2*/
		}
		else
		{
			s16YMRRiseDP = S8_GMA_RiseDP_H_SPEED_PHASE3; /*S8_GMA_RiseTime_H_SPEED_PHASE3*/;
		}
	}
	else if(vref>=S16_GMA_L_SPEED)
	{
		if(WL->s0_reapply_counter <= 1)
		{
			s16YMRRiseDP = S8_GMA_RiseDP_ML_SPEED_PHASE1; /*S8_GMA_RiseTime_ML_SPEED_PHASE1*/
		}
		else if(WL->s0_reapply_counter <= 4)
		{
			s16YMRRiseDP = S8_GMA_RiseDP_ML_SPEED_PHASE2; /*S8_GMA_RiseTime_ML_SPEED_PHASE2*/
		}
		else
		{
			s16YMRRiseDP = S8_GMA_RiseDP_ML_SPEED_PHASE3; /*S8_GMA_RiseTime_ML_SPEED_PHASE3*/
		}
	}
	else
	{
		s16YMRRiseDP = S8_LFC_Conven_Reapply_DP_F; /*S8_LFC_Conven_Reapply_Time_F*/
	}

	return s16YMRRiseDP;
}

/******************************************************************************
* FUNCTION NAME:      LCABS_s16DecideUnstRiseTarDP
* CALLED BY:          LCABS_s16DecideBaseTarDeltaP()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       s16URBaseTarDeltaWhlP
* Description:        Decide Unstable Rise Target Delta Press by Delta Press
******************************************************************************/
static int16_t LCABS_s16DecideUnstRiseTarDP(void)
{
	int16_t s16UnstRiseBaseTarDeltaWhlP = MPRESS_0BAR;

	if(AV_DUMP_wl==1)
	{
		s16UnstRiseBaseTarDeltaWhlP = MPRESS_0BAR;
	}
	else if(AV_HOLD_wl==1)
	{
		s16UnstRiseBaseTarDeltaWhlP = MPRESS_0BAR;
	}
	else
	{
		s16UnstRiseBaseTarDeltaWhlP = LCABS_s16DecideInABSRiseDP();
	}

	return s16UnstRiseBaseTarDeltaWhlP;
}

/******************************************************************************
* FUNCTION NAME:      LCABS_s16DecideInABSTarDP
* CALLED BY:          LCABS_s16DecideBaseTarDeltaP()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       s16InABSBaseTarDeltaWhlP
* Description:        Decide Target Delta Press Inside ABS by Delta Press
******************************************************************************/
static int16_t LCABS_s16DecideInABSTarDP(void)
{
	int16_t s16InABSBaseTarDeltaWhlP = MPRESS_0BAR;

	if(AV_DUMP_wl==1)
	{
		s16InABSBaseTarDeltaWhlP = MPRESS_0BAR;
	}
	else if(AV_HOLD_wl==1)
	{
		s16InABSBaseTarDeltaWhlP = MPRESS_0BAR;
	}
	else
	{
		s16InABSBaseTarDeltaWhlP = LCABS_s16DecideInABSRiseDP();
	}

	return s16InABSBaseTarDeltaWhlP;
}

/******************************************************************************
* FUNCTION NAME:      LCABS_s16DecideInABSRiseDP
* CALLED BY:          LCABS_s16DecideInABSTarDP()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       s16InABSRiseDP
* Description:        Decide Rise Target Delta Press Inside ABS by Delta Press
******************************************************************************/
static int16_t LCABS_s16DecideInABSRiseDP(void)
{
    int16_t s16InABSRiseDP = MPRESS_0BAR;
    int16_t s16InABSRiseDPBase = MPRESS_0BAR;
    int16_t s16InABSRiseDPComp = MPRESS_0BAR;

	/* initialize master pattern variable */
	if(WL->s0_reapply_counter==1)
	{
		WL_WP->lcabss16InitTotalDeltaP = 0;
		WL_WP->lcabss16WhlAccumulatedTarDP = 0; /*Mon*/
		WL_WP->lcabss16WhlInABSDesiredNthTarDP = 0;
		WL_WP->lcabsu1InABSInitialRiseDone = 0;
		WL_WP->lcabsu16InABSNthRiseCnt= 0;
		WL_WP->lcabss16WhlDesriedTarDP = 0;
	}

    if(WL_WP->lcabsu1InABSInitialRiseDone == 0)
    {
    	WL_WP->lcabsu1InABSNthReduceHoldTimReq = 0;
    	
    	s16InABSRiseDPBase = LCABS_s16DecideInABSInitRiseDP();
    	
    	WL_WP->lcabss16WhlDesriedTarDP = WL_WP->lcabss16WhlDesriedTarDP + s16InABSRiseDPBase;
    }
    else
    {
    	s16InABSRiseDPBase = LCABS_s16DecideInABSNthRiseDP();
    	
		WL_WP->lcabss16WhlDesriedTarDP = WL_WP->lcabss16WhlDesriedTarDP + s16InABSRiseDPBase;
    }
    
    s16InABSRiseDPComp = LCABS_s16DecideInABSRiseCompDP();
    
    s16InABSRiseDP = s16InABSRiseDPBase + s16InABSRiseDPComp;
    
    s16InABSRiseDP = LCABS_s16LimitMinMax(s16InABSRiseDP,MPRESS_2BAR,MPRESS_30BAR);
    
    WL_WP->lcabss16WhlAccumulatedTarDP = WL_WP->lcabss16WhlAccumulatedTarDP + s16InABSRiseDP; /*Mon*/

    return s16InABSRiseDP;
}

/******************************************************************************
* FUNCTION NAME:      LCABS_s16DecideInABSInitRiseDP
* CALLED BY:          LCABS_s16DecideInABSRiseDP()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       s16InABSNthRiseDP
* Description:        Decide 1st Rise Target Delta Press Inside ABS by Delta Press
******************************************************************************/
static int16_t LCABS_s16DecideInABSInitRiseDP(void)
{
	int16_t s16InABSInitRiseFinalDP = DEFAULT_PULSE_UP_DELTAP;

	if(WL->s0_reapply_counter==1)
	{
		WL_WP->lcabss16InitTotalDeltaP = LCABS_s16CalcInABSInitRiseDP();
	}
	else {}

	s16InABSInitRiseFinalDP = LCABS_s16DecideDistbnInitRiseDP(WL_WP->lcabss16InitTotalDeltaP);

	s16InABSInitRiseFinalDP = LCABS_s16LimitMinMax(s16InABSInitRiseFinalDP,MPRESS_2BAR,MPRESS_30BAR);

	return s16InABSInitRiseFinalDP;
}

/******************************************************************************
* FUNCTION NAME:      LCABS_s16CalcInABSInitRiseDP
* CALLED BY:          LCABS_s16DecideInABSInitRiseDP()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       s16InABSNthRiseDP
* Description:        Decide 1st Rise Target Delta Press Inside ABS by Delta Press
*******************************************************************************/
static int16_t LCABS_s16CalcInABSInitRiseDP(void)
{
	int16_t s16DeltaPCompPercent = 3; /* 30% */
	int16_t s16InABSTotalInitRiseDP = DEFAULT_PULSE_UP_DELTAP;
	int16_t s16InABSInitRiseDPRefEstPress = DEFAULT_PULSE_UP_DELTAP;

	int16_t s16InitRiseMinDPRefMu = 0;
	int16_t s16InitRiseMaxDPRefMu = 0;

#if __INABS_INIT_RISE_DP_FROM_DUMP == ENABLE
	int16_t s16InABSInitRiseDPRefDump = 0;
	int16_t s16InitRiseDPGainRefDump = 0;
	int16_t s16DumpCntGain = 0;
#endif
    if(lcabsu1ValidBrakeSensor==1)
    {
        if(REAR_WHEEL_wl==0)
        {
            if(AFZ_OK==0)
            {
            	if(WL->PossibleABSForBump == 1)
            	{
					s16DeltaPCompPercent = 80;
					s16InitRiseMinDPRefMu = MPRESS_5BAR;
					s16InitRiseMaxDPRefMu = MPRESS_30BAR;
            	}
            	else
            	{
					if(WL->SPECIAL_1st_Pulsedown_need == 1)  /*150401*/
					{
						s16DeltaPCompPercent = 30;
						s16InitRiseMinDPRefMu = MPRESS_3BAR;
						s16InitRiseMaxDPRefMu = MPRESS_5BAR;
					}
					else
					{
						s16DeltaPCompPercent = LCABS_s16GetGainFromMulevel(WL->lcabsu8PossibleMuFromSkid,40,50,60,70);
						s16InitRiseMinDPRefMu = LCABS_s16GetGainFromMulevel(WL->lcabsu8PossibleMuFromSkid,MPRESS_3BAR,MPRESS_3BAR,MPRESS_5BAR,MPRESS_5BAR);
						s16InitRiseMaxDPRefMu = LCABS_s16GetGainFromMulevel(WL->lcabsu8PossibleMuFromSkid,MPRESS_10BAR,MPRESS_20BAR,MPRESS_30BAR,MPRESS_30BAR);
					}
            	}
            }
            else
            {
				s16DeltaPCompPercent = LCABS_s16GetGainFromMulevel(afz,60,60,70,70);
				s16InitRiseMinDPRefMu = LCABS_s16GetGainFromMulevel(afz,MPRESS_3BAR,MPRESS_3BAR,MPRESS_5BAR,MPRESS_5BAR);
				s16InitRiseMaxDPRefMu = LCABS_s16GetGainFromMulevel(afz,MPRESS_10BAR,MPRESS_20BAR,MPRESS_30BAR,MPRESS_30BAR);
            }
        }
        else
        {
            if(AFZ_OK==0)
            {
            	if(WL->PossibleABSForBump == 1)
            	{
					s16DeltaPCompPercent = 60;
					s16InitRiseMinDPRefMu = MPRESS_5BAR;
					s16InitRiseMaxDPRefMu = MPRESS_30BAR;
            	}
            	else
            	{
					if(WL->SPECIAL_1st_Pulsedown_need == 1)  /*150401*/
					{
						s16DeltaPCompPercent = 30;
						s16InitRiseMinDPRefMu = MPRESS_3BAR;
						s16InitRiseMaxDPRefMu = MPRESS_5BAR;
					}
					else
					{
						s16DeltaPCompPercent = LCABS_s16GetGainFromMulevel(WL->lcabsu8PossibleMuFromSkid,40,50,60,70);
						s16InitRiseMinDPRefMu = LCABS_s16GetGainFromMulevel(WL->lcabsu8PossibleMuFromSkid,MPRESS_3BAR,MPRESS_3BAR,MPRESS_5BAR,MPRESS_5BAR);
						s16InitRiseMaxDPRefMu = LCABS_s16GetGainFromMulevel(WL->lcabsu8PossibleMuFromSkid,MPRESS_10BAR,MPRESS_20BAR,MPRESS_20BAR,MPRESS_30BAR);
					}
            	}
            }
            else
            {
				s16DeltaPCompPercent = LCABS_s16GetGainFromMulevel(afz,50,50,60,60);
				s16InitRiseMinDPRefMu = LCABS_s16GetGainFromMulevel(afz,MPRESS_3BAR,MPRESS_3BAR,MPRESS_5BAR,MPRESS_5BAR);
				s16InitRiseMaxDPRefMu = LCABS_s16GetGainFromMulevel(afz,MPRESS_10BAR,MPRESS_15BAR,MPRESS_20BAR,MPRESS_25BAR);
            }
        }

		  #if __INABS_INIT_RISE_DP_FROM_DUMP == ENABLE  /*TODO CONSIDER condition refer to dumpscantime*/
        if(AFZ_OK==0)
        {
        	s16InitRiseDPGainRefDump = MPRESS_5BAR;
        }
        else
        {
            if(REAR_WHEEL_wl==0)
            {
				s16InitRiseDPGainRefDump = LCESP_s16IInter6Point(afz,  (-U8_PULSE_HIGH_MU),            MPRESS_10BAR,
																			(-U8_PULSE_MED_HIGH_MU), 		MPRESS_8BAR,
																			(-U8_PULSE_MEDIUM_MU),       	MPRESS_8BAR,
																			(-U8_PULSE_LOW_MED_MU),         MPRESS_6BAR,
																			(-U8_PULSE_UNDER_LOW_MED_MU),   MPRESS_5BAR,
																			(-U8_PULSE_LOW_MU),             MPRESS_2BAR);
            }
            else
            {
				s16InitRiseDPGainRefDump = LCESP_s16IInter6Point(afz,  (-U8_PULSE_HIGH_MU),            MPRESS_8BAR,
																			(-U8_PULSE_MED_HIGH_MU), 		MPRESS_8BAR,
																			(-U8_PULSE_MEDIUM_MU),       	MPRESS_8BAR,
																			(-U8_PULSE_LOW_MED_MU),         MPRESS_5BAR,
																			(-U8_PULSE_UNDER_LOW_MED_MU),   MPRESS_5BAR,
																			(-U8_PULSE_LOW_MU),             MPRESS_3BAR);
            }
        }
		  #endif

        s16InABSInitRiseDPRefEstPress = (int16_t)(((int32_t)WL->lcabss16DumpDeltaWhlP*s16DeltaPCompPercent)/100);

        WL_WP->lcabss16InABSInitRiseDPRefEstP = s16InABSInitRiseDPRefEstPress; /*Mon*/

		  #if __INABS_INIT_RISE_DP_FROM_DUMP == ENABLE

        	if(WL->lcabsu8EffectiveDumpScans < 10)
        	{
            	s16DumpCntGain = (((int16_t)WL->lcabsu8EffectiveDumpScans+1)*50)/10;
        	}
        	else
        	{
        		s16DumpCntGain = (((int16_t)(WL->lcabsu8EffectiveDumpScans-9)*20)+500)/10;
        	}

        	s16InABSInitRiseDPRefDump = (int16_t)(((((int32_t)s16DumpCntGain*s16InitRiseDPGainRefDump)/100)*s16DeltaPCompPercent)/10);  /*TODO Refer to Dumpscantime*/

        	WL_WP->lcabss16InABSInitRiseDPRefDump = s16InABSInitRiseDPRefDump;  /*Mon*/

        if(s16InABSInitRiseDPRefEstPress > s16InABSInitRiseDPRefDump)  /*TODO Compromise portion*/
        {
        	s16InABSTotalInitRiseDP = s16InABSInitRiseDPRefEstPress;
        }
        else
        {
        	s16InABSTotalInitRiseDP = s16InABSInitRiseDPRefDump;
        }
		  #else
			s16InABSTotalInitRiseDP = s16InABSInitRiseDPRefEstPress;
		  #endif

		 #if __ABS_RBC_COOPERATION
        if(lcabsu1FastPressRecoveryBfAbs == 1)
        {
        	if(lcabss16RBCPressBfABS > MPRESS_0BAR)
        	{
        		s16InABSTotalInitRiseDP = s16InABSTotalInitRiseDP + lcabss16RBCPressBfABS;
        	}
        }
        else {}
		 #endif

        s16InABSTotalInitRiseDP = LCABS_s16LimitMinMax(s16InABSTotalInitRiseDP,s16InitRiseMinDPRefMu,s16InitRiseMaxDPRefMu);

        if(WL->lcabsu1UpdDeltaPRiseAftHld==1)
        {
        	s16InABSTotalInitRiseDP = s16InABSTotalInitRiseDP + MPRESS_1BAR; /* If there is plenty of remaining rise from target, compensate further rise */
        }
    }
    else
    {
    	s16InABSTotalInitRiseDP = DEFAULT_PULSE_UP_DELTAP;
    }

    return s16InABSTotalInitRiseDP;
}

/******************************************************************************
* FUNCTION NAME:      LCABS_s16DecideDistbnInitRiseDP
* CALLED BY:          LCABS_s16DecideInABSInitRiseDP()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       s16InABSNthRiseDP
* Description:        Decide 1st Rise Target Delta Press Inside ABS by Delta Press
*****************************************************************
*******************************************************************************/
static int16_t LCABS_s16DecideDistbnInitRiseDP(int16_t s16InitTotalDeltaP)
{
    int16_t s161stDPPortion = 6; /*default*/ /*TODO Consider Mu*/
    int16_t s16InitDeltaP = DEFAULT_PULSE_UP_DELTAP;
    int16_t s16InitDistbnPressRef = S16_INIT_RISE_DISTBN_F_TH_HIGH;

     /* 15SWD DEMO */
    if(WL->lcabss8WhlCtrlReqForStrkRec==U8_RISE_AF_STRK_RECVRY)
	{
    	s161stDPPortion = 4;
	}
    else
    {
    	s161stDPPortion = 6;
    }
     /* 15SWD DEMO */

#if __DISTRIBUTION_INIT_RISE_INABS == ENABLE
		if(REAR_WHEEL_wl==0)
		{
			if(afz > (-U8_PULSE_LOW_MU))
            {
				s16InitDistbnPressRef = S16_INIT_RISE_DISTBN_F_TH_LOW;
            }
            else
            {
            	s16InitDistbnPressRef = S16_INIT_RISE_DISTBN_F_TH_HIGH;
            }
		}
		else
		{
			if(afz > (-U8_PULSE_LOW_MU))
            {
				s16InitDistbnPressRef = S16_INIT_RISE_DISTBN_R_TH_LOW;
            }
            else
            {
            	s16InitDistbnPressRef = S16_INIT_RISE_DISTBN_R_TH_HIGH;
            }
		}

    	if((s16InitTotalDeltaP >= s16InitDistbnPressRef) || /* 15SWD DEMO */ (WL->lcabss8WhlCtrlReqForStrkRec==U8_RISE_AF_STRK_RECVRY))
    	{
			if(WL->s0_reapply_counter==1)
			{
				s16InitDeltaP = (s16InitTotalDeltaP*s161stDPPortion)/10;
				WL_WP->lcabsu1InABSInitialRiseDone = 0;
			}
			else if(WL->s0_reapply_counter==2)
			{
				s16InitDeltaP = (s16InitTotalDeltaP*(10-s161stDPPortion))/10;
				WL_WP->lcabsu1InABSInitialRiseDone = 1;
			}
			else
			{
				s16InitDeltaP = DEFAULT_PULSE_UP_DELTAP;
				WL_WP->lcabsu1InABSInitialRiseDone = 1;
			}
    	}
    	else
    	{
    		s16InitDeltaP = s16InitTotalDeltaP;
    		WL_WP->lcabsu1InABSInitialRiseDone = 1;
    	}
#else
		s16InitDeltaP = s16InitTotalDeltaP;
		WL_WP->lcabsu1InABSInitialRiseDone = 1;
#endif

    return s16InitDeltaP;
}

/******************************************************************************
* FUNCTION NAME:      LCABS_s16DecideInABSNthRiseDP
* CALLED BY:          LCABS_s16DecideInABSRiseDP()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       s16InABSNthRiseDP
* Description:        Decide Nth Rise Target Delta Press Inside ABS by Delta Press
******************************************************************************/
static int16_t LCABS_s16DecideInABSNthRiseDP(void)
{
    int16_t s16InABSNthRiseDP = DEFAULT_PULSE_UP_DELTAP;
    int16_t s16InABSDesiredNthPulseUpNum = 0;
    int16_t s16InABSNthTotalTarDP = 0;
	int16_t s16NthRiseMinDPFromMu = 0;

    if(lcabsu1ValidBrakeSensor==1)
    {
        if(REAR_WHEEL_wl==0)
        {
            if(AFZ_OK==0)
            {
				s16InABSDesiredNthPulseUpNum = 3;
				s16NthRiseMinDPFromMu = MPRESS_3BAR;
            }
            else if(afz > (-U8_PULSE_LOW_MU))
            {
				s16InABSDesiredNthPulseUpNum = 3;
				s16NthRiseMinDPFromMu = MPRESS_3BAR;
			}
			else if(afz > (-U8_PULSE_MEDIUM_MU))
			{
				s16InABSDesiredNthPulseUpNum = 3;
				s16NthRiseMinDPFromMu = MPRESS_3BAR;
			}
			else
			{
				s16InABSDesiredNthPulseUpNum = 2;
				s16NthRiseMinDPFromMu = MPRESS_3BAR;
			}
		}
		else
		{
			if(AFZ_OK==0)
			{
				s16InABSDesiredNthPulseUpNum = 3;
				s16NthRiseMinDPFromMu = MPRESS_3BAR;
			}
			else if(afz > (-U8_PULSE_LOW_MU))
			{
				s16InABSDesiredNthPulseUpNum = 3;
				s16NthRiseMinDPFromMu = MPRESS_3BAR;
			}
			else if(afz > (-U8_PULSE_MEDIUM_MU))
			{
				s16InABSDesiredNthPulseUpNum = 3;
				s16NthRiseMinDPFromMu = MPRESS_3BAR;
			}
			else
			{
				s16InABSDesiredNthPulseUpNum = 3;
				s16NthRiseMinDPFromMu = MPRESS_3BAR;
			}
		}
	}
	else
	{
		s16InABSDesiredNthPulseUpNum = 3;
		s16NthRiseMinDPFromMu = MPRESS_4BAR;
	}

	if(WL_WP->lcabsu16InABSNthRiseCnt == 0)
	{
		s16InABSNthTotalTarDP = (WL->lcabss16DumpDeltaWhlP - WL_WP->lcabss16WhlDesriedTarDP);
		
		WL_WP->lcabss16WhlInABSDesiredNthTarDP = (s16InABSNthTotalTarDP/s16InABSDesiredNthPulseUpNum);

		WL_WP->lcabss16WhlInABSDesiredNthTarDP = LCABS_s16LimitMinMax(WL_WP->lcabss16WhlInABSDesiredNthTarDP,s16NthRiseMinDPFromMu,MPRESS_30BAR);

		s16InABSNthRiseDP = WL_WP->lcabss16WhlInABSDesiredNthTarDP;
	}
	else
	{
		s16InABSNthRiseDP = WL_WP->lcabss16WhlInABSDesiredNthTarDP;
	}

    s16InABSNthRiseDP = LCABS_s16LimitMinMax(s16InABSNthRiseDP,MPRESS_1BAR,MPRESS_30BAR);

    WL_WP->lcabsu16InABSNthRiseCnt = WL_WP->lcabsu16InABSNthRiseCnt + 1;

/*
    if(lcabsu1ValidBrakeSensor==1)
    {
        if(REAR_WHEEL_wl==0)
        {
            if(AFZ_OK==0)
            {
                s16InABSNthRiseDP = DEFAULT_PULSE_UP_DELTAP;
            }
            else if(afz > (-U8_PULSE_LOW_MU))
            {
                s16InABSNthRiseDP = MPRESS_3BAR;
            }
            else if(afz > (-U8_PULSE_MEDIUM_MU))
            {
            	s16InABSNthRiseDP = MPRESS_3BAR;
            }
            else
            {
                s16InABSNthRiseDP = MPRESS_5BAR;
            }
        }
        else
        {
            if(AFZ_OK==0)
            {
                s16InABSNthRiseDP = DEFAULT_PULSE_UP_DELTAP;
            }
            else if(afz > (-U8_PULSE_LOW_MU))
            {
                s16InABSNthRiseDP = MPRESS_3BAR;
            }
            else if(afz > (-U8_PULSE_MEDIUM_MU))
            {
                s16InABSNthRiseDP = MPRESS_3BAR;
            }
            else
            {
                s16InABSNthRiseDP = MPRESS_4BAR;
            }
        }
    }
    else
    {
        ;
    }
*/

    return s16InABSNthRiseDP;
}

/******************************************************************************
* FUNCTION NAME:      LCABS_s16DecideInABSRiseCompDP
* CALLED BY:          LCABS_s16DecideInABSRiseDP()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       s16InABSRiseDPComp
* Description:        Decide Rise Comp Delta Press Inside ABS by Delta Press
******************************************************************************/
static int16_t LCABS_s16DecideInABSRiseCompDP(void)
{
	int16_t s16InABSRiseDPComp = MPRESS_0BAR;
	int16_t s16InABSRiseDPCompFromWhl = MPRESS_0BAR;
	int16_t s16InABSRiseDPCompFromEstP = MPRESS_0BAR;
	 	
	 #if __EST_PRESS_COMP_RISE_INABS == ENABLE
	s16InABSRiseDPCompFromEstP = LCABS_s16InABSRiseCompFromEstP();
	 #endif
	 
	 #if __WHEEL_CONDI_COMP_RISE_INABS == ENABLE
	s16InABSRiseDPCompFromWhl = LCABS_s16InABSRiseCompFromWhl();
	 #endif
	 
	s16InABSRiseDPComp = s16InABSRiseDPCompFromEstP + s16InABSRiseDPCompFromWhl;
	
	s16InABSRiseDPComp = LCABS_s16LimitMinMax(s16InABSRiseDPComp,-MPRESS_30BAR,MPRESS_30BAR);
	
	return s16InABSRiseDPComp;
}

/******************************************************************************
* FUNCTION NAME:      LCABS_s16InABSRiseCompFromEstP
* CALLED BY:          LCABS_s16DecideInABSRiseCompDP()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       s16RiseDPCompFromEstP
* Description:        Decide Rise Delta Press Comp From Estimated Pressure Inside ABS
******************************************************************************/
#if __EST_PRESS_COMP_RISE_INABS == ENABLE
static int16_t LCABS_s16InABSRiseCompFromEstP(void)
{
	int16_t s16RiseDPCompFromEstP = MPRESS_0BAR;

	if((WL_WP->lcabsu1InABSInitialRiseDone == 0) && (WL->s0_reapply_counter == 2))
	{
		 #if __EST_PRESS_COMP_INIT_RISE_INABS == ENABLE
		if(WL_WP->lcabss16InABSNthPressError > S8_1ST_RISE_EST_P_POS_COMP_TH)
		{
			s16RiseDPCompFromEstP = (WL_WP->lcabss16InABSNthPressError*S8_1ST_RISE_EST_P_ERR_POS_COMP)/10;
		}
		else if(WL_WP->lcabss16InABSNthPressError < S8_1ST_RISE_EST_P_NEG_COMP_TH)
		{
			s16RiseDPCompFromEstP = (WL_WP->lcabss16InABSNthPressError*S8_1ST_RISE_EST_P_ERR_NEG_COMP)/10;
		}
		else
		{
			s16RiseDPCompFromEstP = MPRESS_0BAR;
		}
		 #else
			s16RiseDPCompFromEstP = MPRESS_0BAR;
		 #endif
	}
	else if((WL_WP->lcabsu1InABSInitialRiseDone == 1) || (WL->s0_reapply_counter > 2))
	{
		 #if __EST_PRESS_COMP_NTH_RISE_INABS == ENABLE
		if(WL_WP->lcabss16InABSNthPressError > S8_NTH_RISE_EST_P_POS_COMP_TH)
		{
			s16RiseDPCompFromEstP = (WL_WP->lcabss16InABSNthPressError*S8_NTH_RISE_EST_P_ERR_POS_COMP)/10;
		}
		else if(WL_WP->lcabss16InABSNthPressError < S8_NTH_RISE_EST_P_NEG_COMP_TH)
		{
			s16RiseDPCompFromEstP = (WL_WP->lcabss16InABSNthPressError*S8_NTH_RISE_EST_P_ERR_NEG_COMP)/10;
		}
		else
		{
			s16RiseDPCompFromEstP = MPRESS_0BAR;
		}
		 #else
			s16RiseDPCompFromEstP = MPRESS_0BAR;
		 #endif
	}
	else 
	{
		s16RiseDPCompFromEstP = MPRESS_0BAR;	
	}

	s16RiseDPCompFromEstP = LCABS_s16LimitMinMax(s16RiseDPCompFromEstP,-MPRESS_30BAR,MPRESS_30BAR);
	 
	return s16RiseDPCompFromEstP;
}
#endif
/******************************************************************************
* FUNCTION NAME:      LCABS_s16InABSRiseCompFromWhl
* CALLED BY:          LCABS_s16DecideInABSRiseCompDP()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       s16InABSNthRiseDP
* Description:        Decide Nth Rise Comp Delta Press Inside ABS by Delta Press
******************************************************************************/
static int16_t LCABS_s16InABSRiseCompFromWhl(void)
{
	int16_t s16InABSNthRiseDPComp = 0;
	int16_t s16InABSNthBaseRiseDPComp = 0;
	int16_t s16SpeedDiff = 0;

	s16SpeedDiff = vref - WL->vrad;
	if(REAR_WHEEL_wl==0)
	{
		if(WL->s0_reapply_counter >= 7)
		{
			s16InABSNthBaseRiseDPComp = MPRESS_2BAR;
		}
		else if(WL->s0_reapply_counter >= 4)
		{
			s16InABSNthBaseRiseDPComp = MPRESS_1BAR;
		}
		else
		{
			s16InABSNthBaseRiseDPComp = MPRESS_0BAR;
		}
	}
	else
	{
		if(WL->s0_reapply_counter >= 7)
		{
			s16InABSNthBaseRiseDPComp = MPRESS_2BAR;
		}
		else if(WL->s0_reapply_counter >= 3)
		{
			s16InABSNthBaseRiseDPComp = MPRESS_1BAR;
		}
		else
		{
			s16InABSNthBaseRiseDPComp = MPRESS_0BAR;
		}
	}

	if(WL->s16_Estimated_Active_Press > (WL->Estimated_Press_SKID+MPRESS_1BAR))
	{
		if((WL->rel_lam < -LAM_1P) && (s16SpeedDiff < VREF_2_KPH) && (WL->arad <= (WL->thres_decel+ARAD_0G25)))
		{
			s16InABSNthRiseDPComp = MPRESS_0BAR;
		}
		else
		{
			if(WL->s0_reapply_counter >= 8)  /* 15SWD DEMO */ /*Improve L2H*/
			{
				s16InABSNthRiseDPComp = MPRESS_3BAR;
			}
			else if(WL->s0_reapply_counter >= 5)
			{
				s16InABSNthRiseDPComp = MPRESS_2BAR;
			}
			else {}
		}
	}
	else if(WL->s16_Estimated_Active_Press > (WL->Estimated_Press_SKID-MPRESS_5BAR))
	{
		if((WL->rel_lam < -LAM_1P) && (s16SpeedDiff < VREF_2_KPH) && (WL->arad <= (WL->thres_decel+ARAD_0G25)))
		{
			s16InABSNthRiseDPComp = -MPRESS_0G5BAR;
		}
		else
		{
			if(WL->s0_reapply_counter >= 6)
			{
				s16InABSNthRiseDPComp = MPRESS_2BAR;
			}
			else if(WL->s0_reapply_counter >= 5)
			{
				s16InABSNthRiseDPComp = MPRESS_1BAR;
			}
			else
			{
				;
			}
		}
	}
	else
	{
		if((WL->rel_lam < -LAM_1P) && (s16SpeedDiff < VREF_2_KPH) && (WL->arad <= (WL->thres_decel+ARAD_0G25)))
		{
			;
		}
		else
		{
			if(WL->s0_reapply_counter >= 8)  /* 15SWD DEMO */ /*Improve L2H*/
			{
				s16InABSNthRiseDPComp = MPRESS_3BAR;
			}
			else if(WL->s0_reapply_counter >= 6)
			{
				s16InABSNthRiseDPComp = MPRESS_2BAR;
			}
			else if(WL->s0_reapply_counter >= 5)
			{
				s16InABSNthRiseDPComp = MPRESS_1BAR;
			}
			else
			{
				;
			}
		}
	}

	if(s16InABSNthRiseDPComp < s16InABSNthBaseRiseDPComp)
	{
		s16InABSNthRiseDPComp = s16InABSNthBaseRiseDPComp;
	}
	else {}

	s16InABSNthRiseDPComp = LCABS_s16LimitMinMax(s16InABSNthRiseDPComp,-MPRESS_30BAR,MPRESS_30BAR);
	
    if(s16InABSNthRiseDPComp > MPRESS_0BAR)
	{
		WL_WP->lcabsu1InABSNthReduceHoldTimReq = 1;
	}
	else
	{
		WL_WP->lcabsu1InABSNthReduceHoldTimReq = 0;
	}

	return s16InABSNthRiseDPComp;
}

/******************************************************************************
* FUNCTION NAME:      LCABS_s16DecideCompTarDeltaP
* CALLED BY:          LCABS_s16DecideWhlTarDeltaP()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       s16CompDeltaP
* Description:        Decide Total Compensation by Delta Press
******************************************************************************/
static int16_t LCABS_s16DecideCompTarDeltaP(void)
{
	int16_t s16CompDeltaP = MPRESS_0BAR;
	int16_t s16CyclicCompDP = MPRESS_0BAR;
	int16_t s16SpecialCompDP = MPRESS_0BAR;
	int16_t s16MPCyclicCompDP = MPRESS_0BAR;

	if(AV_DUMP_wl==1)
	{
		s16CompDeltaP = MPRESS_0BAR;
	}
	else if(AV_HOLD_wl==1)
	{
		s16CompDeltaP = MPRESS_0BAR;
	}
	else
	{

		 #if __MASTER_PATTERN_CYCLIC_COMP == ENABLE
		s16CyclicCompDP = LCABS_s16DecideCyclicCompDP();
		 #endif

		 #if __MASTER_PATTERN_SPECIAL_COMP == ENABLE
		s16SpecialCompDP = LCABS_s16DecideSpecialCompDP();
		WL_WP->lcabss16WhlSpecialCompDP = s16SpecialCompDP; /*Mon*/
		#else
		WL_WP->lcabss16WhlSpecialCompDP = MPRESS_0BAR;
		#endif

		 #if __MASTER_PATTERN_MP_CYCLIC_COMP == ENABLE
		s16MPCyclicCompDP = LCABS_s16DecideMPCyclicCompDP();
		WL_WP->lcabss16WhlMPCyclicCompDP = s16MPCyclicCompDP;  /*Mon*/
		 #else
		WL_WP->lcabss16WhlMPCyclicCompDP = MPRESS_0BAR;  /*Mon*/
		 #endif
		s16CompDeltaP = s16CyclicCompDP + s16SpecialCompDP + s16MPCyclicCompDP;
	}

	s16CompDeltaP = LCABS_s16LimitMinMax(s16CompDeltaP,-MPRESS_30BAR,MPRESS_30BAR);

	return s16CompDeltaP;
}

/******************************************************************************
* FUNCTION NAME:      LCABS_s16DecideCyclicCompDP
* CALLED BY:          LCABS_s16DecideCompTarDeltaP()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       CyclicCompDeltaP
* Description:        Decide Cyclic Compensation by Delta Press
******************************************************************************/
static int16_t LCABS_s16DecideCyclicCompDP(void)
{
	int16_t s16CyclicCompDeltaP = MPRESS_0BAR;
	int16_t s16CyclicCompDeltaPTemp = MPRESS_0BAR;

	if((FSF_wl==1)&&(ABS_wl==0))
	{
		s16CyclicCompDeltaP = MPRESS_0BAR;
		WL_WP->lcabss16WhlCyclicCompDP = 0;
	}
	else if(((WL->WheelCtrlMode==WHEEL_CTRL_NORMAL_ABS) || (WL->WheelCtrlMode==WHEEL_CTRL_UNSTABLE_REAPPLY)) && (WL->s0_reapply_counter==1))
	{
	  #if   __VDC && __ABS_COMB
		if(WL->L_SLIP_CONTROL==0) {
	  #endif
		if((WL->LFC_zyklus_z<=1) || (WL->LFC_fictitious_cycle_flag==1))
		{
			s16CyclicCompDeltaP = MPRESS_0BAR;
			WL_WP->lcabss16WhlCyclicCompDP = 0;
		}
		else if(WL->Check_double_brake_flag==1)
		{
			s16CyclicCompDeltaP = MPRESS_0BAR;
			WL_WP->lcabss16WhlCyclicCompDP = 0;
		}
		else if(WL->LFC_Cyclic_Comp_flag==1)
		{
			if(WL_WP->lcabss16InABSNthPressError < -MPRESS_2BAR)  /*negative compensation*/
			{
				s16CyclicCompDeltaPTemp = (WL_WP->lcabss16InABSNthPressError*5/10);
				s16CyclicCompDeltaPTemp = LCABS_s16LimitMinMax(s16CyclicCompDeltaPTemp, -MPRESS_3BAR, MPRESS_3BAR);
				WL_WP->lcabss16WhlCyclicCompDP = WL_WP->lcabss16WhlCyclicCompDP + s16CyclicCompDeltaPTemp;
				WL_WP->lcabss16WhlCyclicCompDP = LCABS_s16LimitMinMax(WL_WP->lcabss16WhlCyclicCompDP, -MPRESS_5BAR, MPRESS_5BAR);
				s16CyclicCompDeltaP = WL_WP->lcabss16WhlCyclicCompDP;
			}
			else
			{
				s16CyclicCompDeltaP = MPRESS_0BAR;
			}
		}
		else
		{
			s16CyclicCompDeltaP = MPRESS_0BAR;
		}
	  #if   __VDC && __ABS_COMB
		}
	  #endif
	}
	else
	{
		s16CyclicCompDeltaP = MPRESS_0BAR;
	}

	return s16CyclicCompDeltaP;
}

/******************************************************************************
* FUNCTION NAME:      LCABS_s16DecideSpecialCompDP
* CALLED BY:          LCABS_s16DecideCompTarDeltaP()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       SpecialCompDeltaP
* Description:        Decide Special Compensation by Delta Press
******************************************************************************/
static int16_t LCABS_s16DecideSpecialCompDP(void)
{
	int16_t s16SpecialCompDeltaP = MPRESS_0BAR;

	if(AV_DUMP_wl==1)
	{
		s16SpecialCompDeltaP = MPRESS_0BAR;
	}
	else if(AV_HOLD_wl==1)
	{
		s16SpecialCompDeltaP = MPRESS_0BAR;
	}
	else
	{

		if(lcabsu1DriverPedalReapply==1)
		{
			s16SpecialCompDeltaP = MPRESS_15BAR;
		}
		else if((LOW_to_HIGH_suspect==1) ||(WL->LOW_to_HIGH_suspect2==1))
		{
			s16SpecialCompDeltaP = MPRESS_15BAR;
		}
		else
		{
		    if(WL->lcabss8WhlCtrlReqForStrkRec==U8_RISE_BF_STRK_RECVRY) /*TODO Review Location Comp Stroke Recovery*/
		    {
		    	s16SpecialCompDeltaP = MPRESS_0BAR;
		    }
		    else if(WL->lcabss8WhlCtrlReqForStrkRec==U8_RISE_AF_STRK_RECVRY)
		    {
		    	s16SpecialCompDeltaP = S16_RISE_COMP_AFT_STRK_RCVR; /* 15SWD DEMO */
		    }
		    else
		    {
		    	s16SpecialCompDeltaP = MPRESS_0BAR;
		    }
		}
	}

		s16SpecialCompDeltaP = LCABS_s16LimitMinMax(s16SpecialCompDeltaP,-MPRESS_10BAR,MPRESS_10BAR);

	return s16SpecialCompDeltaP;
}

/******************************************************************************
* FUNCTION NAME:      LCABS_s16DecideMPCyclicCompDP
* CALLED BY:          LCABS_s16DecideCompTarDeltaP()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       s16MPCyclicCompDP
* Description:        Decide MP Cyclic Compensation by Delta Press
******************************************************************************/
static int16_t LCABS_s16DecideMPCyclicCompDP(void)
{
	int16_t s16MPCyclicCompDP = MPRESS_0BAR;

	return s16MPCyclicCompDP;
}

#if (__ABS_DESIRED_BOOST_TARGET_P == ENABLE)
#if(__REF_P_RESOL_CHANGED_10_TO_100 == ENABLE)
static void LCABS_s16EstWhlSkidPress(void)
{
	if(AFZ_OK == 1)
	{
		lcabss16EstFrontWhlSkidPress = LCESP_s16IInter6Point(afz, (-80), MPRESS_90BAR,
																		(-40), MPRESS_40BAR,
																		(-30), MPRESS_30BAR,
																		(-25), MPRESS_25BAR,
																		(-10), MPRESS_15BAR,
																		( -5), MPRESS_3BAR);

		lcabss16EstRearWhlSkidPress = LCESP_s16IInter6Point(afz, (-80), MPRESS_80BAR,		
																		(-40), MPRESS_40BAR,
																		(-30), MPRESS_30BAR,
																		(-25), MPRESS_20BAR,
																		(-10), MPRESS_10BAR,
																		( -5), MPRESS_3BAR);
	}
	else
	{
		lcabss16EstFrontWhlSkidPress = (FL.Estimated_Press_SKID + FR.Estimated_Press_SKID)/2;
		lcabss16EstRearWhlSkidPress = (RL.Estimated_Press_SKID + RR.Estimated_Press_SKID)/2;
	}

	lcabss16TotalEstWhlSkidPress = (lcabss16EstFrontWhlSkidPress + lcabss16EstRearWhlSkidPress)/2; /* TODO Need to consider bend, split mu */

	lcabss16EstFrontWhlSkidPress = LCABS_s16LimitMinMax(S16_PRESS_RESOL_10_TO_100(lcabss16EstFrontWhlSkidPress),S16_PCTRL_PRESS_0_BAR,S16_PCTRL_PRESS_150_BAR);
	lcabss16EstRearWhlSkidPress = LCABS_s16LimitMinMax(S16_PRESS_RESOL_10_TO_100(lcabss16EstRearWhlSkidPress),S16_PCTRL_PRESS_0_BAR,S16_PCTRL_PRESS_150_BAR);
	lcabss16TotalEstWhlSkidPress = LCABS_s16LimitMinMax(S16_PRESS_RESOL_10_TO_100(lcabss16TotalEstWhlSkidPress),S16_PCTRL_PRESS_0_BAR,S16_PCTRL_PRESS_150_BAR);
}

 #if (__IDB_LOGIC==ENABLE)
static void LCABS_vDecideDesiredBoostTarP(void)
{
	int16_t s16desiredDeltaPBetMcpAndSkid = 0;
	int8_t s8DesiredBoostTarPExitCondiFlg = 0;
    int16_t s16PrssureDiffSize = 0;
    int16_t s16BoostedPressRef = 0;
	static int16_t lcabss16BoostedPressRefOld = 0;

	s16desiredDeltaPBetMcpAndSkid = S16_PCTRL_PRESS_70_BAR;

	s16BoostedPressRef = lcabss16RefFinalTargetPres;

	if((FL.WheelCtrlMode == WHEEL_CTRL_CBS) || (FL.WheelCtrlMode == WHEEL_CTRL_CBS) || (FL.WheelCtrlMode == WHEEL_CTRL_CBS) || (FL.WheelCtrlMode == WHEEL_CTRL_CBS))
	{
		s8DesiredBoostTarPExitCondiFlg = 1;
	}
	else if(lcabsu1BrkPedalRelDuringABS == 1)  /*TODO Pedal Effort Change*/
	{
		s8DesiredBoostTarPExitCondiFlg = 1;
	}
	else {}

	if(lcabsu1InAbsAllWheelEntFadeOut == 1)
	{
		lcabsu1InABSDesiredBoostTarPFlg = 0;
		lcabss16InABSDesiredBoostTarP = s16BoostedPressRef;
	}
	else if(s8DesiredBoostTarPExitCondiFlg == 1)
	{
		lcabsu1InABSDesiredBoostTarPFlg = 0;
		lcabss16InABSDesiredBoostTarP = s16BoostedPressRef;
	}
	else
	{
		if(AFZ_OK == 1)
		{
			if((LFC_Split_flag == 1) || (LFC_Split_suspect_flag == 1))
			{
				lcabsu1InABSDesiredBoostTarPFlg = 0;
				lcabss16InABSDesiredBoostTarP = s16BoostedPressRef;
			}
			else
			{
				lcabsu1InABSDesiredBoostTarPFlg = 1;
				lcabss16InABSDesiredBoostTarP = lcabss16TotalEstWhlSkidPress + s16desiredDeltaPBetMcpAndSkid;
			}
		}
		else
		{
			lcabsu1InABSDesiredBoostTarPFlg = 0;
			lcabss16InABSDesiredBoostTarP = s16BoostedPressRef;
		}
	}

    s16PrssureDiffSize = LCESP_s16IInter2Point(McrAbs(s16BoostedPressRef - lcabss16InABSDesiredBoostTarP),
    																		S16_PCTRL_PRESS_5_BAR,  S16_PCTRL_PRESS_2_BAR,
    																		S16_PCTRL_PRESS_50_BAR, S16_PCTRL_PRESS_20_BAR);

    if(s16BoostedPressRef != lcabss16InABSDesiredBoostTarP)  /* need to refine */
    {
    	lcabss16InABSDesiredBoostTarP = L_s16LimitDiff(lcabss16InABSDesiredBoostTarP,lcabss16InABSDesiredBoostTPOld,s16PrssureDiffSize,s16PrssureDiffSize);/*KYB*/
    }
    else
    {
    	if(lcabss16BoostedPressRefOld != lcabss16InABSDesiredBoostTPOld) /* need to refine */
    	{
    		lcabss16InABSDesiredBoostTarP = L_s16LimitDiff(lcabss16InABSDesiredBoostTarP,lcabss16InABSDesiredBoostTPOld,s16PrssureDiffSize,s16PrssureDiffSize);
    	}
    	else
    	{
    		;
    	}
    }

	lcabss16InABSDesiredBoostTarP = (s16BoostedPressRef>lcabss16InABSDesiredBoostTarP) ? lcabss16InABSDesiredBoostTarP : s16BoostedPressRef;

	lcabss16InABSDesiredBoostTPOld = lcabss16InABSDesiredBoostTarP;

	lcabss16BoostedPressRefOld = s16BoostedPressRef;

}
 #endif
#else /*#if(__REF_P_RESOL_CHANGED_10_TO_100 == ENABLE)*/
static void LCABS_s16EstWhlSkidPress(void)
{

	if(AFZ_OK == 1)
	{
		lcabss16EstFrontWhlSkidPress = LCESP_s16IInter6Point(afz, (-80), MPRESS_90BAR,
																		(-40), MPRESS_40BAR, /*MPRESS_30BAR*/
																		(-30), MPRESS_30BAR,
																		(-25), MPRESS_25BAR,
																		(-10), MPRESS_15BAR,
																		( -5), MPRESS_3BAR);

		lcabss16EstRearWhlSkidPress = LCESP_s16IInter6Point(afz, (-80), MPRESS_80BAR,		/*MPRESS_60BAR*/
																		(-40), MPRESS_40BAR, /*MPRESS_30BAR*/
																		(-30), MPRESS_30BAR,
																		(-25), MPRESS_20BAR,
																		(-10), MPRESS_10BAR,
																		( -5), MPRESS_3BAR);
	}
	else
	{
		lcabss16EstFrontWhlSkidPress = (FL.Estimated_Press_SKID + FR.Estimated_Press_SKID)/2;
		lcabss16EstRearWhlSkidPress = (RL.Estimated_Press_SKID + RR.Estimated_Press_SKID)/2;
	}

	lcabss16TotalEstWhlSkidPress = (lcabss16EstFrontWhlSkidPress + lcabss16EstRearWhlSkidPress)/2; /* TODO Need to consider bend, split mu */

	lcabss16EstFrontWhlSkidPress = LCABS_s16LimitMinMax(lcabss16EstFrontWhlSkidPress,MPRESS_0BAR,MPRESS_150BAR);
	lcabss16EstRearWhlSkidPress = LCABS_s16LimitMinMax(lcabss16EstRearWhlSkidPress,MPRESS_0BAR,MPRESS_150BAR);
	lcabss16TotalEstWhlSkidPress = LCABS_s16LimitMinMax(lcabss16TotalEstWhlSkidPress,MPRESS_0BAR,MPRESS_150BAR);
}

 #if (__IDB_LOGIC==ENABLE)
static void LCABS_vDecideDesiredBoostTarP(void)
{
	int16_t s16desiredDeltaPBetMcpAndSkid = 0;
	int8_t s8DesiredBoostTarPExitCondiFlg = 0;
    int16_t s16PrssureDiffSize = 0;
    int16_t s16BoostedPressRef = 0;
	static int16_t lcabss16BoostedPressRefOld = 0;

	s16desiredDeltaPBetMcpAndSkid = MPRESS_70BAR;

	s16BoostedPressRef = lcabss16RefFinalTargetPres;

	if((FL.WheelCtrlMode == WHEEL_CTRL_CBS) || (FL.WheelCtrlMode == WHEEL_CTRL_CBS) || (FL.WheelCtrlMode == WHEEL_CTRL_CBS) || (FL.WheelCtrlMode == WHEEL_CTRL_CBS))
	{
		s8DesiredBoostTarPExitCondiFlg = 1;
	}
	else if(lcabsu1BrkPedalRelDuringABS == 1)  /*TODO Pedal Effort Change*/
	{
		s8DesiredBoostTarPExitCondiFlg = 1;
	}
	else {}

	if(lcabsu1InAbsAllWheelEntFadeOut == 1)
	{
		lcabsu1InABSDesiredBoostTarPFlg = 0;
		lcabss16InABSDesiredBoostTarP = s16BoostedPressRef;
	}
	else if(s8DesiredBoostTarPExitCondiFlg == 1)
	{
		lcabsu1InABSDesiredBoostTarPFlg = 0;
		lcabss16InABSDesiredBoostTarP = s16BoostedPressRef;
	}
	else
	{
		if(AFZ_OK == 1)
		{
			if((LFC_Split_flag == 1) || (LFC_Split_suspect_flag == 1))
			{
				lcabsu1InABSDesiredBoostTarPFlg = 0;
				lcabss16InABSDesiredBoostTarP = s16BoostedPressRef;
			}
			else
			{
				lcabsu1InABSDesiredBoostTarPFlg = 1;
				lcabss16InABSDesiredBoostTarP = lcabss16TotalEstWhlSkidPress + s16desiredDeltaPBetMcpAndSkid;
			}
		}
		else
		{
			lcabsu1InABSDesiredBoostTarPFlg = 0;
			lcabss16InABSDesiredBoostTarP = s16BoostedPressRef;
		}
	}

    s16PrssureDiffSize = LCESP_s16IInter2Point(McrAbs(s16BoostedPressRef - lcabss16InABSDesiredBoostTarP),
																			MPRESS_5BAR,  MPRESS_2BAR,
																			MPRESS_50BAR, MPRESS_20BAR);

    if(s16BoostedPressRef != lcabss16InABSDesiredBoostTarP)  /* need to refine */
    {
    	lcabss16InABSDesiredBoostTarP = L_s16LimitDiff(lcabss16InABSDesiredBoostTarP,lcabss16InABSDesiredBoostTPOld,s16PrssureDiffSize,s16PrssureDiffSize);/*KYB*/
    }
    else
    {
    	if(lcabss16BoostedPressRefOld != lcabss16InABSDesiredBoostTPOld) /* need to refine */
    	{
    		lcabss16InABSDesiredBoostTarP = L_s16LimitDiff(lcabss16InABSDesiredBoostTarP,lcabss16InABSDesiredBoostTPOld,s16PrssureDiffSize,s16PrssureDiffSize);
    	}
    	else
    	{
    		;
    	}
    }

	lcabss16InABSDesiredBoostTarP = (s16BoostedPressRef>lcabss16InABSDesiredBoostTarP) ? lcabss16InABSDesiredBoostTarP : s16BoostedPressRef;

	lcabss16InABSDesiredBoostTPOld = lcabss16InABSDesiredBoostTarP;

	lcabss16BoostedPressRefOld = s16BoostedPressRef;

}
 #endif
#endif /*#if(__REF_P_RESOL_CHANGED_10_TO_100 == ENABLE)*/
#endif /*(__ABS_DESIRED_BOOST_TARGET_P == ENABLE)*/
 
void LCABS_vDecideCtrlActuationVars(void)
{

#if (__ABS_DESIRED_BOOST_TARGET_P == ENABLE)
	LCABS_s16EstWhlSkidPress();
     #if (__IDB_LOGIC==ENABLE)
	LCABS_vDecideDesiredBoostTarP();
     #endif
#endif

    if((FL.WheelCtrlMode == WHEEL_CTRL_ABS_FADE_OUT) && (FR.WheelCtrlMode == WHEEL_CTRL_ABS_FADE_OUT) && (RL.WheelCtrlMode == WHEEL_CTRL_ABS_FADE_OUT) && (RR.WheelCtrlMode == WHEEL_CTRL_ABS_FADE_OUT))
    {
    	lcabsu1InAbsAllWheelEntFadeOut = 1;
    }
    else
    {
    	lcabsu1InAbsAllWheelEntFadeOut = 0;
    }

#if __CHANGED_TO_DECIDE_ONOFF == ENABLE
    LCABS_vDecideWhlActPattern(&FL);
    LCABS_vDecideWhlActPattern(&FR);
    LCABS_vDecideWhlActPattern(&RL);
    LCABS_vDecideWhlActPattern(&RR);
#endif
}

static void LCABS_vDecideWhlActPattern(struct W_STRUCT *WL_temp)
{
	WL = WL_temp;

    if((WL->WheelCtrlMode == WHEEL_CTRL_ABS_FADE_OUT) || (WL->WheelCtrlMode == WHEEL_CTRL_CBS) || (WL->WheelCtrlMode == WHEEL_CTRL_NON))
    {
    	WL->LFC_Conven_OnOff_Flag = 0;
    }
	else if((WL->WheelCtrlMode == WHEEL_CTRL_EBD) && (WL->WheelCtrlMode == WHEEL_CTRL_EBD_FADE_OUT))
	{
		WL->LFC_Conven_OnOff_Flag = 0;
	}
    else
    {
    	WL->LFC_Conven_OnOff_Flag = 1;
    }
}

/******************************************************************************
* FUNCTION NAME:      LCABS_s16CalcWhlTarPress
* CALLED BY:          LCABS_vDecideWhlTargetP()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Calculate Wheel Target Press
******************************************************************************/
static void LCABS_vCalcWhlTarPress(struct W_STRUCT *WL_temp, Whl_TP_t *WL_WP_temp)
{
	WL = WL_temp;
	WL_WP = WL_WP_temp;

	if(AV_DUMP_OLD_wl==1) /* temporary capture estimated Press when dump act */
	{
		if((AV_REAPPLY_wl==1) || (BUILT_wl==1))
		{
			WL_WP->lcabss16WhlTarPress = WL_WP->lcabss16WhlTarPress + S16_PRESS_RESOL_10_TO_100(WL_WP->lcabss16WhlTarDP);
		}
		else
		{
			 #if(__REF_P_RESOL_CHANGED_10_TO_100 == ENABLE)
			WL_WP->lcabss16WhlTarPress = WL->Estimated_Active_Press;
			 #else
			WL_WP->lcabss16WhlTarPress = WL->s16_Estimated_Active_Press; /* temporary capture estimated Press when dump act */
			 #endif
		}
		WL_WP->lcabss16WhlTarPRate = -S16_P_RATE_1000_BPS;
	}
	else if((AV_REAPPLY_wl==1) && (BUILT_wl==1))
	{
		if(WL->LFC_Conven_OnOff_Flag == 1)
		{
			WL_WP->lcabss16WhlTarPress = WL_WP->lcabss16WhlTarPress + S16_PRESS_RESOL_10_TO_100(WL_WP->lcabss16WhlTarDP);
		}
		else
		{
			if(lcabss16RefFinalTargetPres > WL_WP->lcabss16WhlTarPress)
			{
				 #if(__REF_P_RESOL_CHANGED_10_TO_100 == ENABLE)
				WL_WP->lcabss16WhlTarPress = WL_WP->lcabss16WhlTarPress + S16_PCTRL_PRESS_2_BAR;
				 #else
				WL_WP->lcabss16WhlTarPress = WL_WP->lcabss16WhlTarPress + MPRESS_2BAR;
				 #endif
				/* If forced hold is needed, set commands for actuation & hold variables for further rise */
				/* If there is plenty of remaining rise from target, compensate further rise */
			}
			else
			{
				WL_WP->lcabss16WhlTarPress = lcabss16RefFinalTargetPres;
			}
		}
 		 #if(__REF_P_RESOL_CHANGED_10_TO_100 == ENABLE)
		if(REAR_WHEEL_wl==0)
		{
			WL_WP->lcabss16WhlTarPRate = LCESP_s16IInter3Point(WL_WP->lcabss16WhlTarPress, S16_PCTRL_PRESS_5_BAR, S16_P_RATE_100_BPS,
					                                                                       S16_PCTRL_PRESS_10_BAR, S16_P_RATE_300_BPS,
					                                                                       S16_PCTRL_PRESS_20_BAR, S16_P_RATE_800_BPS);
		}
		else
		{
			WL_WP->lcabss16WhlTarPRate = LCESP_s16IInter3Point(WL_WP->lcabss16WhlTarPress, S16_PCTRL_PRESS_5_BAR, S16_P_RATE_200_BPS,
					                                                                       S16_PCTRL_PRESS_10_BAR, S16_P_RATE_400_BPS,
					                                                                       S16_PCTRL_PRESS_20_BAR, S16_P_RATE_800_BPS);
		}
		 #else
		if(REAR_WHEEL_wl==0)
		{
			WL_WP->lcabss16WhlTarPRate = LCESP_s16IInter3Point(WL_WP->lcabss16WhlTarPress, MPRESS_5BAR, S16_P_RATE_100_BPS,
					                                                                       MPRESS_10BAR, S16_P_RATE_300_BPS,
					                                                                       MPRESS_20BAR, S16_P_RATE_800_BPS);
		}
		else
		{
			WL_WP->lcabss16WhlTarPRate = LCESP_s16IInter3Point(WL_WP->lcabss16WhlTarPress, MPRESS_5BAR, S16_P_RATE_200_BPS,
					                                                                       MPRESS_10BAR, S16_P_RATE_400_BPS,
					                                                                       MPRESS_20BAR, S16_P_RATE_800_BPS);
		}
		 #endif
	}
	else if(AV_REAPPLY_wl==1)
	{
		if(WL->WheelCtrlMode == WHEEL_CTRL_CBS)
		{
			WL_WP->lcabss16WhlTarPress = lcabss16RefFinalTargetPres;
		}
		else
		{
			WL_WP->lcabss16WhlTarPress = WL_WP->lcabss16WhlTarPress + S16_PRESS_RESOL_10_TO_100(WL_WP->lcabss16WhlTarDP);
		}
 		 #if(__REF_P_RESOL_CHANGED_10_TO_100 == ENABLE)
		if(REAR_WHEEL_wl==0)
		{
			WL_WP->lcabss16WhlTarPRate = LCESP_s16IInter3Point(WL_WP->lcabss16WhlTarPress, S16_PCTRL_PRESS_5_BAR, S16_P_RATE_100_BPS,
					                                                                       S16_PCTRL_PRESS_10_BAR, S16_P_RATE_300_BPS,
					                                                                       S16_PCTRL_PRESS_20_BAR, S16_P_RATE_800_BPS);
		}
		else
		{
			WL_WP->lcabss16WhlTarPRate = LCESP_s16IInter3Point(WL_WP->lcabss16WhlTarPress, S16_PCTRL_PRESS_5_BAR, S16_P_RATE_200_BPS,
					                                                                       S16_PCTRL_PRESS_10_BAR, S16_P_RATE_400_BPS,
					                                                                       S16_PCTRL_PRESS_20_BAR, S16_P_RATE_800_BPS);
		}
		 #else
		if(REAR_WHEEL_wl==0)
		{
			WL_WP->lcabss16WhlTarPRate = LCESP_s16IInter3Point(WL_WP->lcabss16WhlTarPress, MPRESS_5BAR, S16_P_RATE_100_BPS,
					                                                                       MPRESS_10BAR, S16_P_RATE_300_BPS,
					                                                                       MPRESS_20BAR, S16_P_RATE_800_BPS);
		}
		else
		{
			WL_WP->lcabss16WhlTarPRate = LCESP_s16IInter3Point(WL_WP->lcabss16WhlTarPress, MPRESS_5BAR, S16_P_RATE_200_BPS,
					                                                                       MPRESS_10BAR, S16_P_RATE_400_BPS,
					                                                                       MPRESS_20BAR, S16_P_RATE_800_BPS);
		}
		 #endif
	}
	else
	{
		WL_WP->lcabss16WhlTarPRate = S16_P_RATE_0_BPS;
	}

	AV_DUMP_OLD_wl = AV_DUMP_wl;

	 #if(__REF_P_RESOL_CHANGED_10_TO_100 == ENABLE)
	WL_WP->lcabss16WhlTarPress = LCABS_s16LimitMinMax(WL_WP->lcabss16WhlTarPress,S16_PCTRL_PRESS_0_BAR,lcabss16RefFinalTargetPres);
	 #else
	WL_WP->lcabss16WhlTarPress = LCABS_s16LimitMinMax(WL_WP->lcabss16WhlTarPress,MPRESS_0BAR,lcabss16RefFinalTargetPres);
	 #endif
/*
	if(WL->state==UNSTABLE)
	{
		if( (AV_DUMP_wl==1) || (AV_HOLD_wl) )
		{
			WL->lcabsu1CmdPriority = TRUE;
		}
		else
		{
			WL->lcabsu1CmdPriority = FALSE;
		}
	}
	else
	{
		WL->lcabsu1CmdPriority = FALSE;
	}

	if( (AV_DUMP_wl==1) //|| (AV_HOLD_wl) )
	{
		WL_WP->lcabsu1CmdPriority = TRUE;
	}
	else
	{
		WL_WP->lcabsu1CmdPriority = FALSE;
	}
*/
}


static uint8_t LCABS_u8DecideAllowDiffRearWP(struct W_STRUCT *pNBASE, struct W_STRUCT *pBASE)
{
	uint8_t u8AllowDiffP = 0;

	if((LFC_Split_flag==1) || (LFC_Split_suspect_flag==1))
	{
		u8AllowDiffP = MPRESS_5BAR;
	}
	else
	{
		if(AFZ_OK==1)
		{
			if (afz>=-(int16_t)U8_PULSE_LOW_MU)
			{
				u8AllowDiffP = MPRESS_2BAR;
			}
			else if(afz>=-(int16_t)U8_PULSE_MEDIUM_MU)
			{
				u8AllowDiffP = MPRESS_4BAR;
			}
			else /* U8_PULSE_HIGH_MU */
			{
				u8AllowDiffP = MPRESS_8BAR;
			}
		}
	}

	return u8AllowDiffP;
}

static void LCABS_vLimitDiffTardPAtSplit(struct W_STRUCT* pNBASE, struct W_STRUCT* pBASE, Whl_TP_t *pNBASE_WP, Whl_TP_t *pBASE_WP)
{
	uint8_t u8AbsAllowPDiff4RiseAtSplit;
	int16_t s16AbsDiffofRearWhlP;
	/*int16_t s16AbsNbaseTargetWhlP, s16AbsBaseTargetWhlP; */

	if(pBASE_WP->lcabss16WhlTarDP > 0) /* When BASE wheel rise*/
	{
		u8AbsAllowPDiff4RiseAtSplit = LCABS_u8DecideAllowDiffRearWP(pNBASE,pBASE); /* Need to move to limit diff. main function */

		pNBASE_WP->lcabss16WhlTarDP = pBASE_WP->lcabss16WhlTarDP; /* First set, default : Copy base wheel target delta p */	
		s16AbsDiffofRearWhlP		= pNBASE->s16_Estimated_Active_Press - pBASE->s16_Estimated_Active_Press;
		/*s16AbsNbaseTargetWhlP		= pNBASE->s16_Estimated_Active_Press + pNBASE_WP->lcabss16WhlTarDP;
		s16AbsBaseTargetWhlP		= pBASE->s16_Estimated_Active_Press + pBASE_WP->lcabss16WhlTarDP;*/

		if(s16AbsDiffofRearWhlP <= u8AbsAllowPDiff4RiseAtSplit)
		{
			pNBASE_WP->lcabss16WhlTarDP = pBASE_WP->lcabss16WhlTarDP;
		}
		else
		{
			pNBASE_WP->lcabss16WhlTarDP = pNBASE_WP->lcabss16WhlTarDP - s16AbsDiffofRearWhlP + u8AbsAllowPDiff4RiseAtSplit;
			pNBASE_WP->lcabss16WhlTarDP = (pNBASE_WP->lcabss16WhlTarDP < 0) ? 0 : pNBASE_WP->lcabss16WhlTarDP;
		}
	}
	else{}
}

static void LCABS_vLimitDiffTardPAtnSplit(struct W_STRUCT *pNBASE, struct W_STRUCT *pBASE, Whl_TP_t *pNBASE_WP, Whl_TP_t *pBASE_WP)
{
    int16_t BASE_Rep_TargetP;
	uint8_t u8AbsAllowPDiff4RiseAtnSplit;

	/* Non base wheel's reapply target pressure standard is skid pressure of base wheel */
	if(pBASE->Estimated_Press_SKID==0)
	{
		BASE_Rep_TargetP = 0;
	}
	else
	{
		BASE_Rep_TargetP = (pBASE->Estimated_Press_SKID > pBASE->s16_Estimated_Active_Press) ? pBASE->Estimated_Press_SKID : pBASE->s16_Estimated_Active_Press;
	}

	if((pNBASE->s16_Estimated_Active_Press + pNBASE_WP->lcabss16WhlTarDP) >= BASE_Rep_TargetP) /* NBASE wheel get over skid pressure in current cycle */
	{
		u8AbsAllowPDiff4RiseAtnSplit = LCABS_u8DecideAllowDiffRearWP(pNBASE,pBASE);

		if(((pNBASE->s16_Estimated_Active_Press - pBASE->s16_Estimated_Active_Press) + (pNBASE_WP->lcabss16WhlTarDP - pBASE_WP->lcabss16WhlTarDP)) > u8AbsAllowPDiff4RiseAtnSplit)
		{
			pNBASE_WP->lcabss16WhlTarDP = ((pNBASE->s16_Estimated_Active_Press - pBASE->s16_Estimated_Active_Press) > u8AbsAllowPDiff4RiseAtnSplit) ? MPRESS_0BAR : u8AbsAllowPDiff4RiseAtnSplit - (pNBASE->s16_Estimated_Active_Press - pBASE->s16_Estimated_Active_Press);
		}
		else{} /*NBASE wheel rise within remaining pressure difference on both sides*/
		
		pNBASE->lcabsu1TarDeltaPLimitFlg = 1;
	}
	else
	{
		pNBASE->lcabsu1TarDeltaPLimitFlg = 0;
	} /* Before recover skid pressure, NBASE wheel target delta P keep one's value */

	
	if((pBASE->STABIL==0) && (pBASE->s_diff<=0))
	{
		pNBASE->SPLIT_PULSE = (pNBASE->lcabsu1TarDeltaPLimitFlg == 1) ? 1 : pNBASE->SPLIT_PULSE;
    }
    else
	{
		pNBASE->SPLIT_PULSE = (pNBASE->SPLIT_PULSE == 1) ? 0 : pNBASE->SPLIT_PULSE;
    }

}


static void LCABS_vLimitDiffofRearTardP(void)
{
	if((RL.LFC_zyklus_z<=1)&&(RR.LFC_zyklus_z<=1)&&(LFC_Split_flag==0)&&(LFC_Split_suspect_flag==0))
	{
		/* 1st cycle && Non split */
		LCABS_vLimit1stCycleRearTardP();
	}
	else
	{
		if ((MSL_BASE_rl==1) && (RR.LFC_built_reapply_flag==1))
		{
			if ((LFC_Split_flag==1)|| (LFC_Split_suspect_flag==1) || (lcabsu1SelectLowforNonSplit==1))
			{
				LCABS_vLimitDiffTardPAtSplit(&RR,&RL,&RR_WP,&RL_WP);
			}
			else
			{
				LCABS_vLimitDiffTardPAtnSplit(&RR,&RL,&RR_WP,&RL_WP);
			}
		}
		else if ((MSL_BASE_rr==1) && (RL.LFC_built_reapply_flag==1))
		{
			if ((LFC_Split_flag==1) ||(LFC_Split_suspect_flag==1) || (lcabsu1SelectLowforNonSplit==1))
			{
				LCABS_vLimitDiffTardPAtSplit(&RL,&RR,&RL_WP,&RR_WP);
			}
			else
			{
				LCABS_vLimitDiffTardPAtnSplit(&RL,&RR,&RL_WP,&RR_WP);
			}
		}
		else /* regardless of base wheel and Non base wheel is unstable state  */
		{ /* Need to check */

			if((RL.LFC_built_reapply_flag==0) && (RL.SL_Unstable_Rise_flg==1))			/* RL : NBASE */
			{
				RL_WP.lcabss16WhlTarDP = RR_WP.lcabss16WhlTarDP;
				RL.SL_Unstable_Rise_flg = 0;
			}
			else if ((RR.LFC_built_reapply_flag==0) && (RR.SL_Unstable_Rise_flg==1))	/* RR : NBASE */
			{
				RR_WP.lcabss16WhlTarDP = RL_WP.lcabss16WhlTarDP;
				RR.SL_Unstable_Rise_flg = 0;
			}
			else{}
		}
	}
}

static void LCABS_vLimit1stCycleRearTardP(void)
{

	if ((RL.LFC_built_reapply_flag==1)&&(RR.LFC_built_reapply_flag==1))
	{
		if (lcabsu1Rear1stCycleRiseDelay==1)
		{
			if (MSL_BASE_rl==1)
			{
				if(RL.LFC_s0_reapply_counter<=3)
				{
					RR_WP.lcabss16WhlTarDP = MPRESS_0BAR;
				}
				else
				{
					LCABS_vLimitDiffTardPAtnSplit(&RR,&RL,&RR_WP,&RL_WP);
				}
			}
			else if(MSL_BASE_rr==1)
			{
				if(RR.LFC_s0_reapply_counter<=3)
				{
					RL_WP.lcabss16WhlTarDP = MPRESS_0BAR;
				}
				else
				{
					LCABS_vLimitDiffTardPAtnSplit(&RL,&RR,&RL_WP,&RR_WP);
				}
			}
			else{}
		}
		else {}
	}
	else if ((RL.LFC_built_reapply_flag==1) && (RR.LFC_zyklus_z<=0) && (ABS_rr==1) && (vref>=VREF_10_KPH))
	{
		if((RR.Reapply_Accel_flag==1) && (RR_WP.lcabss16WhlTarDP>0))
        {
            RL_WP.lcabss16WhlTarDP = RR_WP.lcabss16WhlTarDP;
        }
        else
        {
            RL_WP.lcabss16WhlTarDP = MPRESS_0BAR;
        }
		
		if(RL.LFC_built_reapply_counter>=L_2SCAN) {RL.LFC_built_reapply_counter = L_2SCAN;}
	}
	else if ((RR.LFC_built_reapply_flag==1) && (RL.LFC_zyklus_z<=0) && (ABS_rl==1) && (vref>=VREF_10_KPH))
	{
		if((RL.Reapply_Accel_flag==1) && (RL_WP.lcabss16WhlTarDP>0))
        {
            RR_WP.lcabss16WhlTarDP = RL_WP.lcabss16WhlTarDP;
        }
        else
        {
            RR_WP.lcabss16WhlTarDP = MPRESS_0BAR;
        }

		if(RR.LFC_built_reapply_counter>=L_2SCAN) {RR.LFC_built_reapply_counter = L_2SCAN;}
	}
	else{}
}



/* AUTOSAR --------------------------*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCABSDecideCtrlState
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/
