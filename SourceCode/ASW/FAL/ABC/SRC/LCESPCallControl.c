
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCESPCallControl
	#include "Mdyn_autosar.h"
#endif


//#define __MOMENT_TO_TARGET_SLIP_NEW 1

#include "LCHDCCallControl.h"
#include "LCACCCallControl.h"
#include "LCEPBCallControl.h"
#include "LCHSACallControl.h"
#include "LCTCSCallControl.h"
#include "LDESPEstBeta.h"
#include "LCESPCallPressureControl.h"
#include "LCESPCalInterpolation.h"
#include "LCESPInterfaceSlipController.h"
#include "LSESPCalSlipRatio.h"
#include "LCallMain.h"
#include "LAEPCCallActHW.h"
#include "LDROPCallDetection.h"
#include "LDESPCalVehicleModel.h"
#include "LDRTACallDetection.h"
#include "LCESPCallControl.h"
#include "LDESPDetectVehicleStatus.h"
#include "LCDECCallControl.h"
#include "LCESPCalLpf.h"
#if	(__BRK_SIG_MPS_TO_PEDAL_SEN == 1)
#include "LSESPFilterEspSensor.h"
#include "LSESPCalSensorOffset.h"
#endif
#if (__ESC_TCMF_CONTROL ==1)
#include"LCESPInterfaceTCMF.h"

uint8_t lcespu8TcmfUnderToOverCompTime;
#endif

int16_t esp_need_count_rise_fl ;
int16_t esp_need_count_rise_fr ;

int16_t moment_under_limit_Q ;
int16_t delta_moment_q_os, delta_moment_q_us ;
int16_t delta_moment_q_under;
int16_t ldesps16YawcOffset; 

int16_t lcesps16Yaw3rdMoment; 
int16_t wheel_number;
int16_t speed_depend_front_slip_gain, speed_depend_rear_slip_gain;
int16_t esp_on_counter_f,esp_on_counter_r;
int16_t esp_on_counter_fl;
int16_t esp_on_counter_fr;
int16_t esp_on_counter_rl;
int16_t esp_on_counter_rr;
int16_t C1_front, C1_rear;
int16_t C2_front, C2_rear;
int16_t lcesps16OVER2WH_CompCount;
#if __SMOOTH_REAR_DEL_M_GAIN
int16_t lcesps16RearSpeedGain;
#endif
int16_t tire_radius; 

///*11SWD*/
//static int16_t s16_pressure_gain_rear;
//static uint8_t Initial_Brake_set_counter;

    /* ROP_REQ_ACT   */
    #define ROLL_OVER_W_2_REAR_PER          0
    #define O_U_PERMIT_THR_FOR_ROLL_OVER    1000 
    
    #define Lfront    (pEspModel->S16_LENGTH_CG_TO_FRONT)/10   /* m*100 */
    #define Lrear     (pEspModel->S16_LENGTH_CG_TO_REAR)/10    /* m*100 */
    #define Mass      (pEspModel->S16_VEHICLE_MASS_FL + pEspModel->S16_VEHICLE_MASS_FR + pEspModel->S16_VEHICLE_MASS_RL + pEspModel->S16_VEHICLE_MASS_RR)  /* Kg */
    #define Iz         pEspModel->S16_MOMENT_OF_INERTIA        /* Kg*m */
    #define Str_ratio  pEspModel->S16_STEERING_RATIO_10        /* ratio*10 */
  
#if __HSA
extern U8_BIT_STRUCT_t HSAF0;
#endif
#if __BDW
#include "LCBDWCallControl.h"
extern struct   U8_BIT_STRUCT_t BDWC1;
#endif

#if __VDC
uint16_t LCESP_u16CompSqrt(uint16_t, uint16_t);
uint16_t LCESP_u16CompSqrtNe(uint16_t, uint16_t);

#if __FBC
#include "LCFBCCallControl.h"
#endif

#if __TVBB
#include "LCTVBBCallControl.h"
#endif

#if (__TCMF_UNDERSTEER_CONTROL == ENABLE)
U8_BIT_STRUCT_t LAESC00;
#endif

void LCESP_DisableYaw(void);

void LAESP_vActuateEsvAfterEsp(void);
void LDESP_vCompDeltaYawDot(void);
void LDESP_vCompDeltaYaw2Dot(void);
void LDESP_vCompDeltaYawFirstDot(void);
void LDESP_vCompYawOutDot(void);
void LDESP_vCompYawOutDot2(void);
void LAESP_vCalMototTime(void);
void LAESP_vResetValve(void);  

void LCESP_vCoordinateControl(void);
void LCESP_vCallControlStatus(void);

void LCESP_vCompThreshold(void);
void LCESP_vDetectControlSignal(void);
void LCESP_vDetectLowSpeed(void);
void LCESP_vDetectWstrJump(void);
void LAESP_vTestValveMode(void);
 
void LCESP_vCalSpeedDepGain(void); 
void LCESP_vCalSpeedDepBetaGain(void);

void LCESP_vControlYaw(void);
void LDESP_vCalYawError(void);
void LCESP_vCalYawGain(void);
void LCESP_vCalOverSteerYawGain(void);
void LCESP_vCalUnderSteerYawGain(void);

extern void LCESP_vAddTargetSlip(void);
extern void LCESP_vInterfaceSlipController(void);

void LCESP_vCalDeltaMoment(void);
void LCESP_vCalDeltaBeta2(void);
void LCESP_vCalCntrStatusESC(void);
void LCESP_vCalDelTargetSlip(void); 
void LCESP_vCalTargetPressPara(void);

void LCESP_vMonitoringESC(void);
void LCESP_vSetRearspeedDepMU(void);
void LCESP_vCalSpeedDepGainRear(void);
void LCESP_vInterface2WheelOverCtrl(void);

#if(__ESC_TCMF_CONTROL ==1)
void LCESP_vSetTcmfESCMode(struct ESC_STRUCT *WL_ESC);
#endif

//static int16_t target_slip_f_0;
static int16_t target_moment_f_0;
//static int16_t target_moment_f;
int16_t target_moment_f;

//static int16_t target_slip_r_0;
//static int16_t target_moment_r_0;
//static int16_t target_moment_r;
//static int16_t cal_target_slip_rear;
//static int16_t cal_target_slip_front;
int16_t target_slip_r_0;
int16_t target_moment_r_0;
int16_t target_moment_r;
int16_t cal_target_slip_rear;
int16_t cal_target_slip_front;

static int16_t yaw_control_P_gain_rear_limit;
static int16_t yaw_control_D_gain_rear_limit;

int16_t rear_high_speed;
int16_t rear_med1_speed;
int16_t rear_med2_speed;
int16_t rear_low_speed;

int16_t rear_low_speed_gain;
int16_t rear_med_speed_gain;
int16_t rear_high_speed_gain;    

 

/*****************************************************/

void LCESP_vCalEnterCondition(void);
void LCESP_vCalExitCondition(void);

void LCESP_vRememberCurrentSlip(void);
void LDESP_CompDetBetaDotForSlip(void);
void LDESP_DetectDriftForSlip(void);
void LCESP_vCalDutyCompensation(void);
  
#if __PARTIAL_PERFORMANCE
extern void LDESPPartial_vVariableSet(void);
extern void LDESPPartial_vDetectFault(void);
#endif

#if defined(__ADDON_TCMF)
extern void LDESP_vSetActiveBooster(void);
#endif

int16_t LCESP_s16MuldivInt(int16_t temp_local2,int16_t temp_local1,int16_t temp_local0);
int16_t LCESP_s16LimitInt(int16_t x_input, int16_t temp_local1, int16_t temp_local2);
int16_t LCESP_s16FindRoadDependatGain( int16_t road_mu, int16_t gain_high, int16_t gain_med, int16_t gain_low );
void LCESP_vFindSpeedDependantGain( void );

extern void LDESP_vDet1stCycle(void);

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
extern void LDESP_vDet1stCycleWheelF(struct ESC_STRUCT *WL_ESC);
extern void LDESP_vDet1stCycleWheelR(struct ESC_STRUCT *WL_ESC);
#else
extern void LDESP_vDet1stCycleWheel(void);
#endif

#if defined (__ADDON_TCMF)
extern void LCESP_vCallActiveBooster(void);
#endif
#if MGH60_NEW_TARGET_SLIP
extern void LCESP_vInterfaceControllerMgh60(void);
 
	int16_t m1;
	int16_t m2;
	int16_t m3;
	int16_t m4;
	int16_t m5;
	int16_t m6;
	int16_t m7;
	int16_t m8;
	int16_t m9;
	int16_t alpha_f;
	int16_t alpha_r;
	int16_t slip_0;
 
#endif 

int16_t delta_pressure;
int16_t wheelpress;
uint8_t motor_pressure_need;
int8_t delta_compensation_duty;
int8_t press_compensation_duty;
int16_t wheelpress_fl, wheelpress_fr, wheelpress_rl,wheelpress_rr;
int8_t comp_duty_fl, comp_duty_fr, comp_duty_rl, comp_duty_rr;
int16_t ABSenterpress_fl, ABSenterpress_fr, ABSenterpress_rl, ABSenterpress_rr;
int16_t esp_pressure_fl, esp_pressure_fr, esp_pressure_rl, esp_pressure_rr;
int16_t delta_pressure_fl, delta_pressure_fr, delta_pressure_rl, delta_pressure_rr;
uint8_t partial_brake_fl_exit_cnt, partial_brake_fr_exit_cnt, partial_brake_rl_exit_cnt, partial_brake_rr_exit_cnt;
 
#if ESC_PULSE_UP_RISE
void LCESP_vCalDeltaPressure4PUR(void);
void LCESP_vCalCompensationDutyFront4PUR(void);
void LCESP_vCalCompensationDutyRear4PUR(void); 

int16_t delta_pressure4PU;
int16_t wheelpress4PU;
uint8_t motor_pressure_need4PU;
int8_t delta_compensation_duty4PU;
int8_t press_compensation_duty4PU;

int16_t delta_pressure4PU_fl, delta_pressure4PU_fr, delta_pressure4PU_rl, delta_pressure4PU_rr;
int16_t ABSenterpress4PU_fl, ABSenterpress4PU_fr, ABSenterpress4PU_rl, ABSenterpress4PU_rr;
int16_t esp_pressure4PU_fl, esp_pressure4PU_fr, esp_pressure4PU_rl, esp_pressure4PU_rr;
int8_t comp_duty4PU_fl, comp_duty4PU_fr, comp_duty4PU_rl, comp_duty4PU_rr;
uint8_t partial_brake_fl_exit_cnt4PU, partial_brake_fr_exit_cnt4PU, partial_brake_rl_exit_cnt4PU, partial_brake_rr_exit_cnt4PU;
  
U8_BIT_STRUCT_t ABS_4PU;
#define ABS_fl_old_4PU		ABS_4PU.bit0
#define ABS_fr_old_4PU		ABS_4PU.bit1
#define ABS_rl_old_4PU		ABS_4PU.bit2
#define ABS_rr_old_4PU		ABS_4PU.bit3
#define not_use_local_esc_01				ABS_4PU.bit4
#define not_use_local_esc_02				ABS_4PU.bit5
#define not_use_local_esc_03				ABS_4PU.bit6
#define not_use_local_esc_04				ABS_4PU.bit7

#endif

  #if (__YAW_M_OVERSHOOT_DETECTION == 1)          
int16_t lcesps16YawOverShootPgainWeight, lcesps16YawOverShootDgainWeight;
  #endif	

#if ESC_PULSE_UP_RISE
void LCESP_vCalEscRiseRate(void);
void LCESP_vCalEscRiseRateLimitDuty(int16_t current_duty, int16_t min_duty_const, int16_t max_duty_const, int8_t whl_slt);
void LCESP_vCalEscRiseRateDuty(void);  
 
    /*11SWD*/
extern int16_t pre_act_action_fl_count_on;
extern int16_t pre_act_action_fr_count_on; 
	/*11SWD*/ 
#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//  int16_t lcs16EscInitialPulseupDutyfl;
//	int16_t lcs16EscInitialPulseupDutyfl_old;
#else
	int16_t lcs16EscInitialPulseupDutyfl;
	int16_t lcs16EscInitialPulseupDutyfl_old;
#endif
	
	int16_t esc_pulse_up_rise_scan_fl;
	
#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//  int16_t lcs16EsctTimePulseupDutyfl;	
//	int16_t lcs16EscInitialPulseupDutyfr;
//	int16_t lcs16EscInitialPulseupDutyfr_old;
#else
	int16_t lcs16EsctTimePulseupDutyfl;	
	int16_t lcs16EscInitialPulseupDutyfr;
	int16_t lcs16EscInitialPulseupDutyfr_old;
#endif	
	
	int16_t esc_pulse_up_rise_scan_fr;
	
#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//  int16_t lcs16EsctTimePulseupDutyfr;
//	int16_t lcs16EscInitialPulseupDutyrl;
#else
	int16_t lcs16EsctTimePulseupDutyfr;
	int16_t lcs16EscInitialPulseupDutyrl;
#endif
		
	int16_t esc_pulse_up_rise_scan_rl;

	int8_t lcs8EscCloseDuty_RL, lcs8EscCloseDuty_RR ;
	int8_t lcs8EscCloseDutyStatusRR, lcs8EscCloseDutyStatusRL ;
	

	int16_t lcs16OVER2WH_count;
  int16_t lcs16OVER2WH_FadeCount;	

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//  int16_t lcs16EscInitialPulseupDutyrr;
#else
	int16_t lcs16EscInitialPulseupDutyrr;
#endif
	
	int16_t esc_pulse_up_rise_scan_rr;


	int16_t esc_pulse_up_rise_scan_fl;
	int16_t esc_pulse_up_rise_scan_fr;
	int16_t esc_pulse_up_rise_scan_rl;
	int16_t esc_pulse_up_rise_scan_rr;

  int16_t temp_ini_duty_f;
  int16_t temp_duty_add_cycle_f;
  int16_t temp_duty_add_duty_f;
  int16_t temp_ini_duty_r;
  int16_t temp_duty_add_cycle_r;
  int16_t temp_duty_add_duty_r;
  int16_t pu_full_T_fl;
  int16_t pu_full_T_fr;
  int16_t pu_full_T_rl;
  int16_t pu_full_T_rr;
   
#endif

/* ESP Engine Control */
void LCESP_vCallMainForEEC(void);
void LCESP_vInitializeEEC(void);
/* ESP Engine Control */
   
#if __TCMF_CONTROL
void TCNO_CURRENT_CONTROL(void);
#endif

#if MGH60_NEW_TARGET_SLIP
void LCESP_vCalDelTargetSlipMgh60(void);
#endif

#if (__CRAM == 1)
void LCCRAM_vCallControl(void); 
void LCCRAM_vChkRequest(void);
 
int16_t moment_pre_front_cram;
int16_t moment_pre_rear_cram;
int16_t moment_cram_delv;
#endif

#if (__TCMF_UNDERSTEER_CONTROL == ENABLE)
int16_t lcescs16TCMFUnderControlCnt;
#endif

#if(DEL_M_IMPROVEMENT_CONCEPT)
   #define q_front_enter_gain_limit -2 
   #define q_rear_enter_gain_limit  -2
   #define q_front_exit_gain_limit  -1
   #define q_rear_exit_gain_limit   -1    
#endif
void LCESP_vCallControl(void)
{
    LCESP_vCompThreshold(); 
    
/***********************************************************************/
/* VDC Control Output                                                  */
/***********************************************************************/
    LCESP_vCalDutyCompensation();

    LCESP_vControlYaw();

    LCESP_vCoordinateControl();

    /* ESP Engine Control */

    #if __GM_FailM      
    
        if( (fu1ESCDisabledBySW==0)
            ||((fu1ESCDisabledBySW==1)&&(Flg_ESC_SW_ABS_OK==1))               
            ||((fu1ESCDisabledBySW==1)&&(Flg_ESC_OFF_SLEEP_MODE_CBS_OK==1))               
            ||((fu1ESCDisabledBySW==1)&&(Flg_ESC_SW_ROP_OK==1))                  
            ||((fu1ESCDisabledBySW==1)&&(RTA_FL.ldabsu1DetSpareTire==1))
            ||((fu1ESCDisabledBySW==1)&&(RTA_FR.ldabsu1DetSpareTire==1))     
            ||((fu1ESCDisabledBySW==1)&&(RTA_RL.ldabsu1DetSpareTire==1))    
            ||((fu1ESCDisabledBySW==1)&&(RTA_RR.ldabsu1DetSpareTire==1))
            ||((fu1ESCDisabledBySW==1)&&(Flg_Flat_Tire_Detect_OK == 1)))    /* '08.03.28 : for Bench_Test, 10SWD : minitire flag change, GM failM */
        {
                          
            if(tcs_error_flg==0)     
            { 
                LCESP_vCallMainForEEC();                
            }
            else
            {
                LCESP_vInitializeEEC();
            }
        }
        else
        {                               
           LCESP_vInitializeEEC();
        } 
        
    #else         
    
			#if (__RWD_SPORTSMODE==SPORTMODE_1) /*__SPORTSMODE==1*/    
            if((fu1ESCDisabledBySW==0)&&(tcs_error_flg==0)&&(fu1TCSDisabledBySW==0))
      		#elif (__RWD_SPORTSMODE==SPORTMODE_2) /*__SPORTSMODE==2*/
            if((((fu1ESCDisabledBySW==0)&&(fu1TCSDisabledBySW==0))
            ||(((fu1ESCDisabledBySW==1)||(fu1TCSDisabledBySW==1))&&(Flg_SPORTSMODE_1_ESP_OK==1)))
            &&(tcs_error_flg==0))	  
      #else
            if((fu1ESCDisabledBySW==0)&&(tcs_error_flg==0))
            #endif	
            { 
               LCESP_vCallMainForEEC(); 
            }
            else
            {                               
               LCESP_vInitializeEEC();
            }                 
     
    #endif           
        
    /* ESP Engine Control */

    if((SLIP_CONTROL_NEED_FL==1)||(SLIP_CONTROL_NEED_FL_old==1))
    {
        ESP_BRAKE_CONTROL_FL=1;
        FL_ESC.ESP_BRK_CTRL = 1;
    }
    else
    {
        ESP_BRAKE_CONTROL_FL =0;
        FL_ESC.ESP_BRK_CTRL = 0;
    }
    if((SLIP_CONTROL_NEED_FR==1)||(SLIP_CONTROL_NEED_FR_old==1))
    {
        ESP_BRAKE_CONTROL_FR= 1;
        FR_ESC.ESP_BRK_CTRL = 1;
    }
    else
    {
        ESP_BRAKE_CONTROL_FR= 0;
        FR_ESC.ESP_BRK_CTRL = 0;
    }
    if((SLIP_CONTROL_NEED_RL==1)||(SLIP_CONTROL_NEED_RL_old==1))
    {
        ESP_BRAKE_CONTROL_RL=1;
        RL_ESC.ESP_BRK_CTRL = 1;
    }
    else
    {
        ESP_BRAKE_CONTROL_RL =0;
        RL_ESC.ESP_BRK_CTRL = 0;
    }
    if((SLIP_CONTROL_NEED_RR==1)||(SLIP_CONTROL_NEED_RR_old==1))
    {
        ESP_BRAKE_CONTROL_RR=1;
        RR_ESC.ESP_BRK_CTRL = 1;
    }
    else
    {
        ESP_BRAKE_CONTROL_RR=0;
        RR_ESC.ESP_BRK_CTRL = 0;
    }

    if ((ESP_BRAKE_CONTROL_FL==1)||(ESP_BRAKE_CONTROL_FR==1)||(ESP_BRAKE_CONTROL_RL==1)||(ESP_BRAKE_CONTROL_RR==1))
    {
        ESP_BRAKE_CONTROL = 1;
    }
    else
    {
        ESP_BRAKE_CONTROL = 0;
    }
    
    #if(__ESC_TCMF_CONTROL ==1)    
    if (((SLIP_CONTROL_BRAKE_RL_OLD == 1) && (SLIP_CONTROL_BRAKE_RL == 0))
       ||((SLIP_CONTROL_BRAKE_RR_OLD == 1) && (SLIP_CONTROL_BRAKE_RR == 0))
       )
    {
    	lcespu8TcmfUnderToOverCompTime = 0;
    }
    else
    {
    	lcespu8TcmfUnderToOverCompTime = lcespu8TcmfUnderToOverCompTime + 1;
    	
    	if(lcespu8TcmfUnderToOverCompTime > L_U8_TIME_10MSLOOP_2S)
    	{
    		lcespu8TcmfUnderToOverCompTime = L_U8_TIME_10MSLOOP_2S;
    	}
    	else
    	{
    		;
    	}
    }
        
    if((ESP_BRAKE_CONTROL_FL ==1)&&(lcu1US2WHControlFlag_RL ==0))
    {
    	LCESP_vSetTcmfESCMode(&FL_ESC);
    }
    else
    {
    	;
    }
    
    if((ESP_BRAKE_CONTROL_FR ==1)&&(lcu1US2WHControlFlag_RR ==0))
    {
    	LCESP_vSetTcmfESCMode(&FR_ESC);
    }
    else
    {
    	;
    }    
    #endif
    
      #if (__TCMF_UNDERSTEER_CONTROL == ENABLE)
    if (  
        ((SLIP_CONTROL_NEED_FL == 0)||(lcu1US2WHControlFlag_RL == 1))
      &&(SLIP_CONTROL_NEED_FR == 0)  
      &&(ESP_BRAKE_CONTROL_RL == 1)  
      &&(SLIP_CONTROL_NEED_RR == 0)
      &&(ABS_fz == 0)
      #if !__SPLIT_TYPE
      &&((BTCS_fr == 0)&&(BTCS_rl == 0))
      #else
      &&((BTCS_rr == 0)&&(BTCS_rl == 0))
      #endif
      
      #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
      &&(lsespu1AHBGEN3MpresBrkOn == 0)
      #else
      &&(MPRESS_BRAKE_ON == 0)
      #endif
       )
    {
        lcescu1TCMFUnderControlRl = 1;
        lcescu1TCMFUnderControlRr = 0;
    }
    else if (
             (SLIP_CONTROL_NEED_FL == 0)
           &&((SLIP_CONTROL_NEED_FR == 0)||(lcu1US2WHControlFlag_RR == 1))
           &&(SLIP_CONTROL_NEED_RL == 0)
           &&(ESP_BRAKE_CONTROL_RR == 1)
           &&(ABS_fz == 0)
           #if !__SPLIT_TYPE
           &&((BTCS_fl == 0)&&(BTCS_rr == 0))
           #else
           &&((BTCS_rl == 0)&&(BTCS_rr == 0))
           #endif
           #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
           &&(lsespu1AHBGEN3MpresBrkOn == 0)
           #else
           &&(MPRESS_BRAKE_ON == 0)
           #endif
            )
    {
        lcescu1TCMFUnderControlRl = 0;
        lcescu1TCMFUnderControlRr = 1;
    }
    else
    {
        lcescu1TCMFUnderControlRl = 0;
        lcescu1TCMFUnderControlRr = 0;
    }
    if ((lcescu1TCMFUnderControlRl == 1) || (lcescu1TCMFUnderControlRr == 1))
    {
          #if __TVBB
        if ((lctvbbu1TVBB_ON == 1)&&(ldtvbbu1FrontWhlCtl == 0))
        {
            lcescu1TCMFUCSafterTVBBTC = 1; 
            lcescu1TCMFUCSafterTVBBMotor = 1;
        }
        else
        {
            ;
        }  
          #else
        lcescu1TCMFUCSafterTVBBTC = 0; 
        lcescu1TCMFUCSafterTVBBMotor = 0; 
          #endif
        
          #if !__SPLIT_TYPE
        if (
            ((lcescu1TCMFUnderControlRl == 1) && (ESP_BRAKE_CONTROL_FR == 1))
          ||((lcescu1TCMFUnderControlRr == 1) && (ESP_BRAKE_CONTROL_FL == 1))
           )
        {
            lcescs16TCMFUnderControlCnt = S16_TCMF_USC_INITIAL_FULL_TIME;
        }
        else
        {        
            lcescs16TCMFUnderControlCnt = lcescs16TCMFUnderControlCnt+1;
        }
          #else
        lcescs16TCMFUnderControlCnt = lcescs16TCMFUnderControlCnt+1;
          #endif
        
        if (  (S16_TCMF_USC_INITIAL_FULL_TIME > 1) && (lcescs16TCMFUnderControlCnt == 1)&&
              (((lcescu1TCMFUnderControlRl == 1)&&(RL.s16_Estimated_Active_Press > MPRESS_0BAR))
             ||((lcescu1TCMFUnderControlRr == 1)&&(RR.s16_Estimated_Active_Press > MPRESS_0BAR))))
        {
            lcescs16TCMFUnderControlCnt = S16_TCMF_USC_INITIAL_FULL_TIME - 1;
        }
        else
        {
            ;
        }
        
        if (lcescs16TCMFUnderControlCnt >= L_U16_TIME_10MSLOOP_1MIN)
        {
            lcescs16TCMFUnderControlCnt = L_U16_TIME_10MSLOOP_1MIN;
        }
        else
        {
            ;
        }
    }
    else
    {
        lcescs16TCMFUnderControlCnt = 0;
        lcescu1TCMFUCSafterTVBBTC = 0;
        lcescu1TCMFUCSafterTVBBMotor = 0;
    }
      #endif
}

#if(__ESC_TCMF_CONTROL ==1)
void LCESP_vSetTcmfESCMode(struct ESC_STRUCT *WL_ESC)
{
	WL_ESC->lcespu1TCMFControl_Old      = WL_ESC->lcespu1TCMFControl;
    WL_ESC->lcespu1TCMFConOld_IniFull   = WL_ESC->lcespu1TCMFCon_IniFull;
	WL_ESC->lcespu1TCMFConOld_Rise      = WL_ESC->lcespu1TCMFCon_Rise;
	WL_ESC->lcespu1TCMFConOld_Hold      = WL_ESC->lcespu1TCMFCon_Hold;
	WL_ESC->lcespu1TCMFConOld_Dump      = WL_ESC->lcespu1TCMFCon_Dump;
	WL_ESC->lcespu1TCMFConOld_FadeOut   = WL_ESC->lcespu1TCMFCon_FadeOut;
	WL_ESC->lcespu1TCMFCon_Rise_STEPOld = WL_ESC->lcespu1TCMFCon_Rise_STEP;
	WL_ESC->lcesps16TCMFCon_STEP_RegOld = WL_ESC->lcesps16TCMFCon_STEP_Reg;
	WL_ESC->lcespu1TCMFInhibitOld       = WL_ESC->lcespu1TCMFInhibit;
	
	if((WL_ESC->lcespu1TCMFInhibitbyMpress ==0)
	 &&(WL_ESC->MFC_Current_Inter ==S16_TCMF_TC_DUTY_L_LIMIT)
	 &&((WL_ESC->lcespu1TCMFCon_Dump ==1)||(WL_ESC->lcespu1TCMFCon_FadeOut ==1))
	 &&(MPRESS_BRAKE_ON ==1)
	 &&(mpress_slop >=0))
	{
		WL_ESC->lcespu1TCMFInhibitbyMpress =1;
	}
	else if((WL_ESC->lcespu1TCMFInhibitbyMpress ==1)
		  &&(MPRESS_BRAKE_ON ==1)
		  &&(ABS_fz ==0))
	{
		WL_ESC->lcespu1TCMFInhibitbyMpress =1;
	}
	else
	{
		WL_ESC->lcespu1TCMFInhibitbyMpress =0;
	}
	
	if((ABS_fz ==1)||(WL_ESC->lcespu1TCMFInhibitbyMpress ==1))
	{
		WL_ESC->lcespu1TCMFInhibit = 1;
	}
	else
	{
		WL_ESC->lcespu1TCMFInhibit = 0;
	}
	
	if(WL_ESC->lcespu1TCMFInhibit ==0)
    {
    	WL_ESC->lcespu1TCMFControl = 1;
		
    	if((WL_ESC->lcespu1TCMFControl_Old ==0)&&(WL_ESC->lcespu1TCMFControl ==1))
    	{
    		if(lcespu8TcmfUnderToOverCompTime < U8_TCMF_UNDER_COMP_TIME)
			{
				WL_ESC->lcespu1TcmfUnderToOverCompFlag = 1;
			}
			else
			{
				WL_ESC->lcespu1TcmfUnderToOverCompFlag = 0;
			}
    	}
    	else
    	{
    		;
    	}    	
    	
    	if((WL_ESC->lcespu1TCMFControl_Old ==0)&&(WL_ESC->lcespu1TCMFControl ==1)
    		&&(WL_ESC->MFC_Current_Inter > S16_TCMF_RISE_DUTY_CBS_START))
    	{
    		WL_ESC->lcesps16TCMFControlCnt = (int16_t)U8_TCMF_INITIAL_FULL_TIME;
    	}
    	else
    	{
    		;
    	}
    	
		WL_ESC->lcesps16TCMFControlCnt = WL_ESC->lcesps16TCMFControlCnt + 1;
		
		if(MPRESS_BRAKE_ON ==1)
		{
			WL_ESC->lcespu1TCMFConCBSCombFlag = 1;
		}
		else
		{
			WL_ESC->lcespu1TCMFConCBSCombFlag = 0;
		}
    }
    else
    {
    	WL_ESC->lcespu1TCMFControl = 0;
		WL_ESC->lcesps16TCMFControlCnt = 0;
		WL_ESC->lcespu1TCMFConCBSCombFlag = 0;
    }
	
	if(WL_ESC->lcespu1TCMFControl ==1)
    {
    	if(WL_ESC->ESP_PULSE_DUMP ==1)
    	{
    		WL_ESC->lcespu1TCMFCon_IniFull = 0;
			WL_ESC->lcespu1TCMFCon_Rise    = 0;
			WL_ESC->lcespu1TCMFCon_Hold    = 0;
			WL_ESC->lcespu1TCMFCon_Dump    = 0;
			WL_ESC->lcespu1TCMFCon_FadeOut = 1;
			
			WL_ESC->lcespu1TCMFCon_Rise_MAX = 0;
			WL_ESC->lcespu1TCMFCon_Rise_STEP = 0;
			WL_ESC->lcespu1TCMFCon_Dump_SLOW = 0;
			WL_ESC->lcespu1TCMFCon_Dump_FAST = 0;
			
			WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg3 = 0;
			WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg2 = 0;
			WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg1 = 0;
			WL_ESC->lcesps16TCMFCon_STEP_Reg = 0;
    	}
    	else if(WL_ESC->lcesps16TCMFControlCnt <= 2)
    	{
    		WL_ESC->lcespu1TCMFCon_IniFull = 1;
			WL_ESC->lcespu1TCMFCon_Rise    = 0;
			WL_ESC->lcespu1TCMFCon_Hold    = 0;
			WL_ESC->lcespu1TCMFCon_Dump    = 0;
			WL_ESC->lcespu1TCMFCon_FadeOut = 0;
			
			WL_ESC->lcespu1TCMFCon_Rise_MAX = 0;
			WL_ESC->lcespu1TCMFCon_Rise_STEP = 0;
			WL_ESC->lcespu1TCMFCon_Dump_SLOW = 0;
			WL_ESC->lcespu1TCMFCon_Dump_FAST = 0;
			
			WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg3 = 0;
			WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg2 = 0;
			WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg1 = 0;
			WL_ESC->lcesps16TCMFCon_STEP_Reg = 0;
    	}
    	else if(WL_ESC->esp_control_mode ==1)
		{			
    		if((WL_ESC->lcesps16TCMFControlCnt <= (int16_t)U8_TCMF_INITIAL_FULL_TIME)&&(WL_ESC->lcespu1TCMFConCBSCombFlag ==0)
    			&&(WL_ESC->lcespu1TcmfUnderToOverCompFlag ==0))
    		{
    			WL_ESC->lcespu1TCMFCon_IniFull = 1;
				WL_ESC->lcespu1TCMFCon_Rise    = 0;
				WL_ESC->lcespu1TCMFCon_Hold    = 0;
				WL_ESC->lcespu1TCMFCon_Dump    = 0;
				WL_ESC->lcespu1TCMFCon_FadeOut = 0;
				
				WL_ESC->lcespu1TCMFCon_Rise_MAX = 0;
				WL_ESC->lcespu1TCMFCon_Rise_STEP = 0;
				WL_ESC->lcespu1TCMFCon_Dump_SLOW = 0;
				WL_ESC->lcespu1TCMFCon_Dump_FAST = 0;
				
				WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg3 = 0;
				WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg2 = 0;
				WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg1 = 0;
				WL_ESC->lcesps16TCMFCon_STEP_Reg = 0;
    		}
    		else
    		{
    			WL_ESC->lcespu1TCMFCon_IniFull = 0;
				WL_ESC->lcespu1TCMFCon_Rise    = 1;
				WL_ESC->lcespu1TCMFCon_Hold    = 0;
				WL_ESC->lcespu1TCMFCon_Dump    = 0;
				WL_ESC->lcespu1TCMFCon_FadeOut = 0;
				
				if((WL_ESC->lcesps16TCMFControlCnt <= (int16_t)U8_TCMF_INITIAL_FULL_TIME)&&(WL_ESC->lcespu1TCMFConCBSCombFlag ==0)
    				&&(WL_ESC->lcespu1TcmfUnderToOverCompFlag ==1))
				{
					WL_ESC->lcespu1TCMFCon_Rise_MAX = 0;
					WL_ESC->lcespu1TCMFCon_Rise_STEP = 1;
					
					WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg3 = 1;
					WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg2 = 0;
					WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg1 = 0;
					WL_ESC->lcesps16TCMFCon_STEP_Reg = 3;
				}
				else if((WL_ESC->slip_control < WL_ESC->lcesps16TCMFMaxRiseThr)&&(WL_ESC->lcespu1TCMFConCBSCombFlag ==0))
				{
					WL_ESC->lcespu1TCMFCon_Rise_MAX = 1;
					WL_ESC->lcespu1TCMFCon_Rise_STEP = 0;
					
					WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg3 = 0;
					WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg2 = 0;
					WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg1 = 0;
					WL_ESC->lcesps16TCMFCon_STEP_Reg = 0;
				}
				else if(WL_ESC->slip_control < WL_ESC->lcesps16TCMFStepRiseThrReg3)
				{
					WL_ESC->lcespu1TCMFCon_Rise_MAX = 0;
					WL_ESC->lcespu1TCMFCon_Rise_STEP = 1;
					
					WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg3 = 1;
					WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg2 = 0;
					WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg1 = 0;
					WL_ESC->lcesps16TCMFCon_STEP_Reg = 3;
				}
				else if(WL_ESC->slip_control < WL_ESC->lcesps16TCMFStepRiseThrReg2)
				{
					WL_ESC->lcespu1TCMFCon_Rise_MAX = 0;
					WL_ESC->lcespu1TCMFCon_Rise_STEP = 1;
					
					WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg3 = 0;
					WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg2 = 1;
					WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg1 = 0;
					WL_ESC->lcesps16TCMFCon_STEP_Reg = 2;
				}
				else
				{
					WL_ESC->lcespu1TCMFCon_Rise_MAX = 0;
					WL_ESC->lcespu1TCMFCon_Rise_STEP = 1;
					
					WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg3 = 0;
					WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg2 = 0;
					WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg1 = 1;
					WL_ESC->lcesps16TCMFCon_STEP_Reg = 1;
				}
					
				esp_tempW5 = LCESP_s16FindRoadDependatGain(esp_mu, S16_TCMF_RISE_STEP_USC_DOT_HM
					                                             , S16_TCMF_RISE_STEP_USC_DOT_MM
					                                             , S16_TCMF_RISE_STEP_USC_DOT_LM);
				
				if(WL_ESC->slip_control_dot > esp_tempW5)
				{
					WL_ESC->lcespu1TCMFCon_Rise_MAX = 0;
					WL_ESC->lcespu1TCMFCon_Rise_STEP = 1;
					
					WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg3 = 0;
					WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg2 = 0;
					WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg1 = 1;
					WL_ESC->lcesps16TCMFCon_STEP_Reg = 1;
				}
				else
				{
					;
				}
				
				if((WL_ESC->lcespu1TCMFCon_Rise_STEPOld ==1)
					&&(WL_ESC->lcesps16TCMFCon_STEP_RegOld == WL_ESC->lcesps16TCMFCon_STEP_Reg)) //To prevent flag set in transition phase from MAX to Step
				{
					WL_ESC->lcespu1TCMFConSTEP_RegTrans = 0;
				}
				else
				{
					WL_ESC->lcespu1TCMFConSTEP_RegTrans = 1;
				}

				WL_ESC->lcespu1TCMFCon_Dump_SLOW = 0;
				WL_ESC->lcespu1TCMFCon_Dump_FAST = 0;
    		}
    	}
    	else if(WL_ESC->esp_control_mode ==3)
    	{
    		WL_ESC->lcespu1TCMFCon_IniFull = 0;
			WL_ESC->lcespu1TCMFCon_Rise    = 0;
			WL_ESC->lcespu1TCMFCon_Hold    = 1;
			WL_ESC->lcespu1TCMFCon_Dump    = 0;
			WL_ESC->lcespu1TCMFCon_FadeOut = 0;
			
			WL_ESC->lcespu1TCMFCon_Rise_MAX = 0;
			WL_ESC->lcespu1TCMFCon_Rise_STEP = 0;
			WL_ESC->lcespu1TCMFCon_Dump_SLOW = 0;
			WL_ESC->lcespu1TCMFCon_Dump_FAST = 0;
			
			WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg3 = 0;
			WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg2 = 0;
			WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg1 = 0;
			WL_ESC->lcespu1TCMFConSTEP_RegTrans = 0;
			WL_ESC->lcesps16TCMFCon_STEP_Reg = 0;
    	}
    	else if(WL_ESC->esp_control_mode ==4)
    	{
    		WL_ESC->lcespu1TCMFCon_IniFull = 0;
			WL_ESC->lcespu1TCMFCon_Rise    = 0;
			WL_ESC->lcespu1TCMFCon_Hold    = 0;
			WL_ESC->lcespu1TCMFCon_Dump    = 1;
			WL_ESC->lcespu1TCMFCon_FadeOut = 0;
			
			if(WL_ESC->slip_control > WL_ESC->lcesps16TCMFFastDumpThr)
			{
				WL_ESC->lcespu1TCMFCon_Dump_SLOW = 0;
				WL_ESC->lcespu1TCMFCon_Dump_FAST = 1;
			}
			else
			{
				WL_ESC->lcespu1TCMFCon_Dump_SLOW = 1;
				WL_ESC->lcespu1TCMFCon_Dump_FAST = 0;
			}
				  			
			WL_ESC->lcespu1TCMFCon_Rise_MAX = 0;
			WL_ESC->lcespu1TCMFCon_Rise_STEP = 0;
			
			WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg3 = 0;
			WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg2 = 0;
			WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg1 = 0;
			WL_ESC->lcespu1TCMFConSTEP_RegTrans = 0;
			WL_ESC->lcesps16TCMFCon_STEP_Reg = 0;
    	}
    	else
    	{
    		;
    	}
    }
    else
    {
    	WL_ESC->lcespu1TCMFCon_IniFull = 0;
		WL_ESC->lcespu1TCMFCon_Rise    = 0;
		WL_ESC->lcespu1TCMFCon_Hold    = 0;
		WL_ESC->lcespu1TCMFCon_Dump    = 0;
		WL_ESC->lcespu1TCMFCon_FadeOut = 0;
		
		WL_ESC->lcespu1TCMFCon_Rise_MAX = 0;
		WL_ESC->lcespu1TCMFCon_Rise_STEP = 0;
		WL_ESC->lcespu1TCMFCon_Dump_SLOW = 0;
		WL_ESC->lcespu1TCMFCon_Dump_FAST = 0;
		
		WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg3 = 0;
		WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg2 = 0;
		WL_ESC->lcespu1TCMFCon_Rise_STEP_Reg1 = 0;
		WL_ESC->lcesps16TCMFCon_STEP_Reg = 0;
    }
}
#endif

int16_t LCESP_s16MuldivInt(int16_t temp_local2,int16_t temp_local1,int16_t temp_local0){        /*2004.KIM YONG KIL*/
    #if defined (__MATLAB_COMPILE)
        tempW3 = (tempW2*tempW1)/tempW0;
    #else 
		tempW3 = (int16_t)((((int32_t)tempW2)*tempW1)/tempW0);
/*
        tempW2 = temp_local2;
        tempW1 = temp_local1;
        tempW0 = temp_local0; 
        s16muls16divs16(); 
*/
    #endif
    return tempW3;

}
int16_t LCESP_s16LimitInt(int16_t x_input, int16_t temp_local1, int16_t temp_local2)
{             /*2004.KIM YONG KIL*/
    int16_t temp_local3;
    temp_local3=x_input;
    if (temp_local1<=temp_local2)
    {
        if (x_input>=temp_local2)
        {
            temp_local3=temp_local2;
        }
        else
        {
            ;
        }
        if (x_input<temp_local1)
        {
            temp_local3=temp_local1;
        }
        else
        {
            ;
        }
    }
    else{
        if (x_input>=temp_local1)
        {
            temp_local3=temp_local1;
        }
        else
        {
            ;
        }
        if (x_input<temp_local2)
        {
            temp_local3=temp_local2;
        }
        else
        {
            ;
        }
    }
    return temp_local3;
}
int16_t LCESP_s16FindRoadDependatGain( int16_t road_mu, int16_t gain_high, int16_t gain_med, int16_t gain_low )
{

    if( road_mu <= lsesps16MuLow )
    {
        tempW9 = gain_low;
    }
    else if ( road_mu <= lsesps16MuMed )
    {
        tempW0 = lsesps16MuMed-lsesps16MuLow;
        if ( tempW0==0 )
        {
            tempW0=1;
        }
        else
        {
            ;
        }
        
        tempW9 = gain_low + (int16_t)((((int32_t)(road_mu-lsesps16MuLow))*(gain_med-gain_low))/tempW0);
    }
    else if ( road_mu <= lsesps16MuMedHigh )
    {
        tempW9 = gain_med;
    }
    else if ( road_mu <= lsesps16MuHigh )
    {
        tempW0 = lsesps16MuHigh-lsesps16MuMedHigh;
        if ( tempW0==0 )
        {
            tempW0=1;
        }
        else
        {
            ;
        }
        tempW9 = gain_med + (int16_t)((((int32_t)(road_mu-lsesps16MuMedHigh))*(gain_high-gain_med))/tempW0);
    }
    else
    {
        tempW9 = gain_high;
    }

    return tempW9;
}


void LCESP_vFindSpeedDependantGain( void )
{
    int16_t v_v1 = S16_YC_SPEED_DEP_VLOW_V;
    int16_t v_v2 = S16_YC_SPEED_DEP_LOW_V;
    int16_t v_v3 = S16_YC_SPEED_DEP_MED1_V;
    int16_t v_v4 = S16_YC_SPEED_DEP_MED2_V;
    int16_t v_v5 = S16_YC_SPEED_DEP_HIG_V;
    int16_t v_v6 = S16_YC_SPEED_DEP_HIG2_V;
    int16_t v_v7 = S16_YC_SPEED_DEP_VER_HIG_V;
    int16_t v_g1;
    int16_t v_g2;
    int16_t v_g5;
    int16_t v_g3=100;
    int16_t v_g7;

    if(ABS_fz==1)
    {
    	
		 		if(ESP_SPLIT==1) 
		 		{
	        v_g1 = S16_YC_SP_DEP_VLOW_ABS_SPLIT_GAIN  ;
	        v_g2 = S16_YC_SP_DEP_LOW_ABS_SPLIT_GAIN ;
	        v_g3 = S16_YC_SP_DEP_MED_ABS_SPLIT_GAIN ;
	        v_g5 = S16_YC_SP_DEP_HIG_ABS_SPLIT_GAIN ;
	        v_g7 = S16_YC_SP_DEP_V_HIG_ABS_SPLIT_GAIN ;
		 		}
		 		else
		 		{   
	        v_g1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_SP_DEP_VLOW_ABS_H_GAIN,
	                                S16_YC_SP_DEP_VLOW_ABS_GAIN, S16_YC_SP_DEP_VLOW_ABS_L_GAIN);
	        v_g2 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_SP_DEP_LOW_ABS_H_GAIN,
	                                S16_YC_SP_DEP_LOW_ABS_GAIN, S16_YC_SP_DEP_LOW_ABS_L_GAIN);
	        v_g3 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_SP_DEP_MED_ABS_H_GAIN,
	                                S16_YC_SP_DEP_MED_ABS_GAIN, S16_YC_SP_DEP_MED_ABS_L_GAIN);
	        v_g5 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_SP_DEP_HIG_ABS_H_GAIN,
	                                S16_YC_SP_DEP_HIG_ABS_GAIN, S16_YC_SP_DEP_HIG_ABS_L_GAIN);
	        v_g7 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_SP_DEP_V_HIG_ABS_H_GAIN,
	                                S16_YC_SP_DEP_V_HIG_ABS_GAIN, S16_YC_SP_DEP_V_HIG_ABS_L_GAIN); 
        }                        
        
    }
    else
    {
        v_g1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_SPEED_DEP_VLOW_H_GAIN,
                                S16_YC_SPEED_DEP_VLOW_M_GAIN, S16_YC_SPEED_DEP_VLOW_L_GAIN);
        if ( (ESP_MINI_TIRE_SUSPECT==1) && (v_g1<60) && (esp_mu < lsesps16MuMedHigh) )
        {
             v_g1 = 60;
        }
        else
        {
            ;
        }
        v_g2 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_SPEED_DEP_LOW_H_GAIN,
                                S16_YC_SPEED_DEP_LOW_M_GAIN, S16_YC_SPEED_DEP_LOW_L_GAIN);
        if ( (ESP_MINI_TIRE_SUSPECT==1)&& (v_g2<80) && (esp_mu < lsesps16MuMedHigh) )
        {
            v_g2 = 80;
        }
        else
        {
            ;
        }
        v_g5 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_SPEED_DEP_HIG_H_GAIN,
                                S16_YC_SPEED_DEP_HIG_M_GAIN, S16_YC_SPEED_DEP_HIG_L_GAIN);
        v_g7 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_SPEED_DEP_VER_HIG_H_GAIN,
                                S16_YC_SPEED_DEP_VER_HIG_M_GAIN, S16_YC_SPEED_DEP_VER_HIG_L_GAIN);
    }

    if( vrefk <= v_v1 )
    {
        gain_speed_depend = v_g1;
    }
    else if ( vrefk < v_v2 )
    {
        tempW0 = (v_v2-v_v1);
        if ( tempW0==0 )
        {
            tempW0=1;
        }
        else
        {
            ;
        }
        gain_speed_depend = v_g1 + (int16_t)((((int32_t)(v_g2-v_g1))*(vrefk-v_v1))/tempW0);
    }
    else if ( vrefk < v_v3 )
    {
        tempW0 = (v_v3-v_v2);
        if ( tempW0==0 )
        {
            tempW0=1;
        }
        else
        {
            ;
        }
        gain_speed_depend = v_g2 + (int16_t)((((int32_t)(v_g3-v_g2))*(vrefk-v_v2))/tempW0);
    }
    else if ( vrefk < v_v4 )
    {
        gain_speed_depend = v_g3;
    }
    else if ( vrefk < v_v5 )
    {
        tempW0 = (v_v5-v_v4);
        if ( tempW0==0 )
        {
            tempW0=1;
        }
        else
        {
            ;
        }
        gain_speed_depend = v_g3 + (int16_t)((((int32_t)(v_g5-v_g3))*(vrefk-v_v4))/tempW0);
    }
    else if ( vrefk < v_v6 )
    {
       gain_speed_depend = v_g5;
    }
    else if ( vrefk < v_v7 )
    {
        tempW0 = (v_v7 - v_v6);
        if ( tempW0==0 )
        {
            tempW0=1;
        }
        else
        {
            ;
        }
        gain_speed_depend = v_g5 + (int16_t)((((int32_t)(v_g7 - v_g5))*(vrefk - v_v6))/tempW0);
    }
    else
    {
        gain_speed_depend = v_g7;
    }

}

uint16_t LCESP_u16CompSqrt(uint16_t tempW11, uint16_t tempW12)
{
        tempUW2 = tempW11+5;
        tempUW1 = 1;
        tempUW0 = 10;
        u16mulu16divu16();
        esp_tempW5 = (int16_t)(tempUW3);

        tempUW2 = (uint16_t)(esp_tempW5);
        tempUW1 = (uint16_t)(esp_tempW5);
        tempUW0 = 1;
        u16mulu16divu16();
        esp_tempW3 = (int16_t)(tempUW3);

        tempUW2 = tempW12;
        tempUW1 = tempW12;
        tempUW0 =1;
        u16mulu16divu16();
        esp_tempW4 = (int16_t)(tempUW3);

    esp_tempW1 = esp_tempW3 + esp_tempW4;

    if ( esp_tempW1 <= 500 )
    {
        tempUW2 = (uint16_t)(esp_tempW1);
        tempUW1 = 308;
        tempUW0 = 10000;
        u16mulu16divu16();
        esp_tempW2 = (int16_t)(tempUW3 + 7);
    }
    else if  ( esp_tempW1 <= 1000 )
    {
        tempUW2 = (uint16_t)(esp_tempW1);
        tempUW1 = 185;
        tempUW0 = 10000;
        u16mulu16divu16();
        esp_tempW2 = (int16_t)(tempUW3 + 13);
    }
    else if  ( esp_tempW1 <= 2000 )
    {
        tempUW2 = (uint16_t)(esp_tempW1);
        tempUW1 = 139;
        tempUW0 = 10000;
        u16mulu16divu16();
        esp_tempW2 = (int16_t)(tempUW3 + 18);
    }
    else if  ( esp_tempW1 <= 5000 )
    {
        tempUW2 = (uint16_t)(esp_tempW1);
        tempUW1 = 86;
        tempUW0 = 10000;
        u16mulu16divu16();
        esp_tempW2 = (int16_t)(tempUW3 + 29);
    }
    else if  ( esp_tempW1 <= 10000)
    {
        tempUW2 = (uint16_t)(esp_tempW1);
        tempUW1 = 58;
        tempUW0 = 10000;
        u16mulu16divu16();
        esp_tempW2 = (int16_t)(tempUW3 + 43);
    }
    else if  ( esp_tempW1 <= 20000)
    {
        tempUW2 = (uint16_t)(esp_tempW1);
        tempUW1 = 41;
        tempUW0 = 10000;
        u16mulu16divu16();
        esp_tempW2 = (int16_t)(tempUW3 + 61);
    }
    else
    {
        esp_tempW2 = 141;
    }

    esp_tempW3 = (esp_tempW2 * 10) + 10;

    return (uint16_t)(esp_tempW3);
}

uint16_t LCESP_u16CompSqrtNe(uint16_t tempW11, uint16_t tempW12)
{
    esp_tempW5 = (int16_t)((tempW11+5)/10);
    esp_tempW3 = (int16_t)( (uint32_t)esp_tempW5*esp_tempW5 );
    esp_tempW4 = (int16_t)( (uint32_t)tempW12*tempW12 );

    esp_tempW1 = esp_tempW3 - esp_tempW4;

    if ( esp_tempW1 <= 500 )
    {
        tempUW2 = (uint16_t)(esp_tempW1);
        tempUW1 = 308;
        tempUW0 = 10000;
        u16mulu16divu16();
        esp_tempW2 = (int16_t)(tempUW3 + 7);
    }
    else if  ( esp_tempW1 <= 1000 )
    {
        tempUW2 = (uint16_t)(esp_tempW1);
        tempUW1 = 185;
        tempUW0 = 10000;
        u16mulu16divu16();
        esp_tempW2 = (int16_t)(tempUW3 + 13);
    }
    else if  ( esp_tempW1 <= 2000 )
    {
        tempUW2 = (uint16_t)(esp_tempW1);
        tempUW1 = 139;
        tempUW0 = 10000;
        u16mulu16divu16();
        esp_tempW2 = (int16_t)(tempUW3 + 18);
    }
    else if  ( esp_tempW1 <= 5000 )
    {
       tempUW2 = (uint16_t)(esp_tempW1);
       tempUW1 = 86;
       tempUW0 = 10000;
        u16mulu16divu16();
        esp_tempW2 = (int16_t)(tempUW3 + 29);
    }
    else if  ( esp_tempW1 <= 10000)
    {
        tempUW2 = (uint16_t)(esp_tempW1);
        tempUW1 = 58;
        tempUW0 = 10000;
        u16mulu16divu16();
        esp_tempW2 = (int16_t)(tempUW3 + 43);
    }
    else if  ( esp_tempW1 <= 20000)
    {
        tempUW2 = (uint16_t)(esp_tempW1);
        tempUW1 = 41;
        tempUW0 = 10000;
        u16mulu16divu16();
        esp_tempW2 = (int16_t)(tempUW3 + 61);
    }
    else
    {
        esp_tempW2 = 141;
    }

    esp_tempW3 = (esp_tempW2 * 10) + 10;

    return (uint16_t)(esp_tempW3);
}

void LCESP_DisableYaw( void )
{
    
/* '10.12.24 : ESC_OFF --> ROP_ACT enable */	
	
#if  __ESC_SW_OFF_ROP_ACT_ENABLE == 1 

    if (fu1ESCDisabledBySW==1) 
    {
        if ((Flg_ESC_SW_ROP_OK==0)&&(ROP_REQ_ACT==1)&&(vrefk>=S16_ROP_IN_ESC_OFF_VEL_ENT_TH))
        {
            Flg_ESC_SW_ROP_OK = 1 ; 
        }
        else if ( (Flg_ESC_SW_ROP_OK==1)&&(ROP_REQ_ACT==1) )
        {
            Flg_ESC_SW_ROP_OK = 1 ; 
        }
        else
        {
            Flg_ESC_SW_ROP_OK = 0 ;       /* ESC Disable Condition */
        }            
  
    }
    else
    {
            Flg_ESC_SW_ROP_OK = 0 ;       /* ESC Disable Condition */         
    }   
    
#else 

		Flg_ESC_SW_ROP_OK = 0 ;       /* ESC Disable Condition */ 
		
#endif 

#if __ESC_SW_OFF_SLEEP_MODE_ENABLE == 1

    if (fu1ESCDisabledBySW == 1)
    {
        if (K_ESC_Temporary_Enable_Pressure < MPRESS_5BAR)
        {
            esp_tempW0 = MPRESS_5BAR;
        }
        else
        {
            esp_tempW0 = K_ESC_Temporary_Enable_Pressure;
        }
        
      #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
        if ( (Flg_ESC_OFF_SLEEP_MODE_CBS_OK==0)&&((vrefk/10)>K_ESC_Temporary_Enable_Speed)&&(((BLS == 1)&&(BLS_ERROR == 0))||(lsesps16AHBGEN3mpress > esp_tempW0)) )
      #else
        if ( (Flg_ESC_OFF_SLEEP_MODE_CBS_OK==0)&&((vrefk/10)>K_ESC_Temporary_Enable_Speed)&&(((BLS == 1)&&(BLS_ERROR == 0))||(mpress > esp_tempW0)) )
      #endif
        {
            Flg_ESC_OFF_SLEEP_MODE_CBS_OK = 1 ; 
        }
      #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
        else if ( (Flg_ESC_OFF_SLEEP_MODE_CBS_OK==1)&&(((BLS == 1)&&(BLS_ERROR == 0))||(lsesps16AHBGEN3mpress > (esp_tempW0-MPRESS_1BAR))) )
      #else
        else if ( (Flg_ESC_OFF_SLEEP_MODE_CBS_OK==1)&&(((BLS == 1)&&(BLS_ERROR == 0))||(mpress > (esp_tempW0-MPRESS_1BAR))) )
      #endif  
        {
            Flg_ESC_OFF_SLEEP_MODE_CBS_OK = 1 ; 
        }
        else
        {
            Flg_ESC_OFF_SLEEP_MODE_CBS_OK = 0 ;       /* ESC Disable Condition */
        }             
        
    }
    else
    {
        Flg_ESC_OFF_SLEEP_MODE_CBS_OK = 0;
    }
#else
    Flg_ESC_OFF_SLEEP_MODE_CBS_OK = 0;    
#endif

#if (__RWD_SPORTSMODE==SPORTMODE_2) /*__SPORTSMODE==2*/
		if ((tcs_error_flg==0)&&((fu1TCSDisabledBySW==0)||((fu1TCSDisabledBySW==1)&&(vref5 > VREF_40_KPH) )))
			{
				Flg_SPORTSMODE_1_ESP_OK = 1;
			}
		else
			{
			if((tcs_error_flg==0)&&(Flg_SPORTSMODE_1_ESP_OK==1)&&(vref5 > VREF_35_KPH)) 
			{
					Flg_SPORTSMODE_1_ESP_OK = 1;
				    }
			else
			{
					Flg_SPORTSMODE_1_ESP_OK = 0;
				    }
			}
#endif
/* '09.09.18 : GM_SSTS update */

    if (fu1ESCDisabledBySW==1) 
    {
      #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
        if ( (Flg_ESC_SW_ABS_OK==0)&&(ABS_fz==1)&&((vrefk/10)>K_ESC_Temporary_Enable_Speed)&&(lsesps16AHBGEN3mpress>K_ESC_Temporary_Enable_Pressure) )
      #else
        if ( (Flg_ESC_SW_ABS_OK==0)&&(ABS_fz==1)&&((vrefk/10)>K_ESC_Temporary_Enable_Speed)&&(mpress>K_ESC_Temporary_Enable_Pressure) )
      #endif  
        {
            Flg_ESC_SW_ABS_OK = 1 ; 
        }
        else if ( (Flg_ESC_SW_ABS_OK==1)&&(ABS_fz==1) )
        {
            Flg_ESC_SW_ABS_OK = 1 ; 
        }
        else
        {
            Flg_ESC_SW_ABS_OK = 0 ;       /* ESC Disable Condition */
        }            
#if __GM_FailM
        if((K_Tire_Low_ESC_Override==1)&&																			/* (K_Tire_Low_ESC_Override) = “True” */
         	(lespu8TirePressureStatFL!=0)&&
					(lespu8TirePressureStatFR!=0)&&
					(lespu8TirePressureStatRL!=0)&&
					(lespu8TirePressureStatRR!=0)&&
           	   ((lespu16TirePressureFL<=S16_TIRE_LOW_PRESSURE_THRESHOLD)						/* Pressure <= (Tire_Low_Pressure_Threshold)*/
           	 ||((lespu8TirePressureStatFL>=2)&&(lespu8TirePressureStatFL<=4))			/* Status = “Low - Service Now”, “Low”, or “Low - Extended Mobility” */
             ||(lespu16TirePressureFR<=S16_TIRE_LOW_PRESSURE_THRESHOLD)
             ||((lespu8TirePressureStatFR>=2)&&(lespu8TirePressureStatFR<=4))
             ||(lespu16TirePressureRL<=S16_TIRE_LOW_PRESSURE_THRESHOLD)
             ||((lespu8TirePressureStatRL>=2)&&(lespu8TirePressureStatRL<=5))
             ||(lespu16TirePressureRR<=S16_TIRE_LOW_PRESSURE_THRESHOLD)
             ||((lespu8TirePressureStatRR>=2)&&(lespu8TirePressureStatRR<=5))))  
        {
        	Flg_Flat_Tire_Detect_OK = 1;
        }
        else
        {
        	Flg_Flat_Tire_Detect_OK = 0;
        }
#endif 
    }
    else
    {
            Flg_ESC_SW_ABS_OK = 0 ;             
        		Flg_Flat_Tire_Detect_OK = 0;
    }   
    
    VDC_DISABLE_FLG=0;

    if ( vrefk < S16_UNDER_CONTROL_DISABLE_SPEED )
    {
        yaw_control_P_gain_rear=0;
        yaw_control_D_gain_rear=0;
        slip_target_limit_rear=0;
    }
    else if(vrefk >= S16_YAW_CONTROL_HIGH_SPEED)
    {
        yaw_control_P_gain_rear=0;
        yaw_control_D_gain_rear=0;
        slip_target_limit_rear=0;
    }
    else
    {
        ;
    }

    if (((PARTIAL_PERFORM==1)&&(VDC_UNDER_STR_ERR==1))||(SAS_CHECK_OK==0))
    {
                yaw_control_P_gain_rear=0;
                yaw_control_D_gain_rear=0;
                slip_target_limit_rear=0;
    }
    else
    {
        ;
    }
    
#if  __BANK_DETECT_ESC_CTRL_ENABLE == 1 
    if((ESP_ERROR_FLG==1)||(VDC_LOW_SPEED==1)||(BACK_DIR==1)||(PARTIAL_PERFORM==1)   
#else 
    if((ESP_ERROR_FLG==1)||(VDC_LOW_SPEED==1)||(FLAG_BANK_DETECTED==1)||(BACK_DIR==1)||(PARTIAL_PERFORM==1) 
#endif     
        ||(SAS_CHECK_OK==0)||
   #if !__ESP_BTCS_CONTROL
		
		#if	(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
    ((lsespu1AHBGEN3MpresBrkOn==0)&&(ESP_SPLIT==1))||
		#else
    ((MPRESS_BRAKE_ON==0)&&(ESP_SPLIT==1))||
    #endif
    
   #endif
                     (cbit_process_step==1)||(KYAW.SUSPECT_FLG==1)||(KSTEER.SUSPECT_FLG==1)
                      
            #if __GM_FailM      
            
                    ||((fu1ESCDisabledBySW==1)
                    	&&(RTA_FL.ldabsu1DetSpareTire==0)&&(RTA_FR.ldabsu1DetSpareTire==0)
											&&(RTA_RL.ldabsu1DetSpareTire==0)&&(RTA_RR.ldabsu1DetSpareTire==0)
											&&(Flg_ESC_SW_ABS_OK==0)&&(Flg_Flat_Tire_Detect_OK == 0)&&(Flg_ESC_SW_ROP_OK==0)&&(Flg_ESC_OFF_SLEEP_MODE_CBS_OK == 0))         																				/* '08.03.28 : for Bench_Test, 10SWD : minitire flag change, GM failM */
                    ||(ABS_PERMIT_BY_BRAKE==1)||(fu1VoltageLowErrDet==1) 	/* woong 2011.04.06 fu1VoltageLowErrDet 조건 추가 */ 
                
            #else          
								#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
                     ||((fu1ESCDisabledBySW==1)&&(Flg_ESC_SW_ROP_OK==0)&&(lsespu1AHBGEN3MpresBrkOn==0))||(ABS_PERMIT_BY_BRAKE==1) 
								#else
                     ||((fu1ESCDisabledBySW==1)&&(Flg_ESC_SW_ROP_OK==0)&&(MPRESS_BRAKE_ON==0))||(ABS_PERMIT_BY_BRAKE==1) 
            #endif                   
            #endif                   
                     
                     ||(KYAW.EEPROM_SUSPECT_FLG==1)||(__ESP_DISABLE==1) )
   {
        delta_moment_q = 0 ;
        delta_moment_beta = 0;
        moment_q_front_old = 0;
        moment_q_rear_old = 0;
        yaw_control_P_gain_front=0;
        yaw_control_D_gain_front=0;
        yaw_control_P_gain_rear=0;
        yaw_control_D_gain_rear=0;
        slip_target_limit_front=0;
        slip_target_limit_rear=0;
        VDC_DISABLE_FLG=1;
    }
    else
    {
        ;
    }

    if(
        #if __AX_SENSOR
        (fu1AxSenPwr1sOk==0)||
        #endif
        (fu1YawSenPwr1sOk==0)||(fu1AySenPwr1sOk==0)||(fu1SteerSenPwr1sOk==0)||(fu1McpSenPwr1sOk==0) )
    {
            delta_moment_q = 0 ;
            delta_moment_beta = 0;
            moment_q_front_old = 0; moment_q_rear_old = 0;
            yaw_control_P_gain_front=0;
            yaw_control_D_gain_front=0;
            yaw_control_P_gain_rear=0;
            yaw_control_D_gain_rear=0;
            slip_target_limit_front=0;
            slip_target_limit_rear=0;
            VDC_DISABLE_FLG=1;
    }
    else
    {
        ;
    }
	
}

void LCESP_vCalDutyCompensation(void)
{
  #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    mpress_diff=(lsesps16AHBGEN3mpress-lsesps16AHBGEN3mpressOld);
  #else
    mpress_diff=(mpress-mpress_old);
  #endif

#if __SPLIT_TYPE==0
	#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    if(lsespu1AHBGEN3MpresBrkOn==1)
	#else
    if(MPRESS_BRAKE_ON==1)
	#endif
    {
        if(mpress_diff>12)
        {
            if(INITIAL_BRAKE_old==0)
            {
                INITIAL_BRAKE=1;
            }
            else
            {
                ;
            }
        }
        else if((INITIAL_BRAKE_old==0)&&(INITIAL_BRAKE==1)&&(Initial_Brake_reset_counter<=15))
        {
            Initial_Brake_reset_counter++;
        }
        else
        {
            INITIAL_BRAKE=0;
/*            INITIAL_BRAKE_old=1;*/
            Initial_Brake_reset_counter=0;
        }

        if(((SLIP_CONTROL_NEED_FL==0)&&(slip_m_fl < (-WHEEL_SLIP_7) ))
            ||((SLIP_CONTROL_NEED_FR==0)&&(slip_m_fr < (-WHEEL_SLIP_7) )))
        {
            INITIAL_BRAKE = 0;
            INITIAL_BRAKE_old=1;
            Initial_Brake_reset_counter=0;
        }
      #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
        else if((PARTIAL_BRAKE==0)||(lsesps16AHBGEN3mpress>=MPRESS_90BAR)
        ||(((SLIP_CONTROL_NEED_FL==0)&&(SLIP_CONTROL_NEED_FR==0)&&(ABS_fl==1)&&(ABS_fr==1))
            ||(((SLIP_CONTROL_NEED_FL==1)&&(ABS_fr==1))||((SLIP_CONTROL_NEED_FR==1)&&(ABS_fl==1)))))
      #else
        else if((PARTIAL_BRAKE==0)||(mpress>=MPRESS_90BAR)
        ||(((SLIP_CONTROL_NEED_FL==0)&&(SLIP_CONTROL_NEED_FR==0)&&(ABS_fl==1)&&(ABS_fr==1))
            ||(((SLIP_CONTROL_NEED_FL==1)&&(ABS_fr==1))||((SLIP_CONTROL_NEED_FR==1)&&(ABS_fl==1)))))
      #endif  
        {
            INITIAL_BRAKE=0;
            INITIAL_BRAKE_old=1;
            Initial_Brake_reset_counter=0;
        }
        else
        {
            ;
        }
    }
    else
    {
        INITIAL_BRAKE=0;
        INITIAL_BRAKE_old=0;
        Initial_Brake_reset_counter=0;
    }
#else

	#if	(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
    if(lsespu1AHBGEN3MpresBrkOn==1)
	#else
    if(MPRESS_BRAKE_ON==1)
	#endif
    {
        if(mpress_diff>12)
        {
            Initial_Brake_set_counter++;

          #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
            if((INITIAL_BRAKE_old==0)
                &&((Initial_Brake_set_counter >= 14)||(lsesps16AHBGEN3mpress >= MPRESS_20BAR)))
          #else
            if((INITIAL_BRAKE_old==0)
                &&((Initial_Brake_set_counter >= 14)||(mpress >= MPRESS_20BAR)))
          #endif
            {
                INITIAL_BRAKE=1;
            }
            else
            {
                ;
            }
        }
        else if((INITIAL_BRAKE_old==0)&&(INITIAL_BRAKE==1)&&(Initial_Brake_reset_counter<=21))
        {
            Initial_Brake_reset_counter++;

            if(Initial_Brake_reset_counter >= 22)
            {
                INITIAL_BRAKE_old = 1;
                Initial_Brake_set_counter = 0;
            }
            else
            {
                INITIAL_BRAKE_old = 0;
            }
        }
        else
        {
            INITIAL_BRAKE=0;
/*            INITIAL_BRAKE_old=1;*/
            Initial_Brake_reset_counter=0;
        }

        if(((SLIP_CONTROL_NEED_FL==0)&&(slip_m_fl < (-WHEEL_SLIP_7) ))
            ||((SLIP_CONTROL_NEED_FR==0)&&(slip_m_fr < (-WHEEL_SLIP_7) )))
        {
            INITIAL_BRAKE = 0;
            INITIAL_BRAKE_old=1;
            Initial_Brake_reset_counter=0;
            Initial_Brake_set_counter = 0;
        }
      #if(__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE)
        else if((PARTIAL_BRAKE==0)||(lsesps16AHBGEN3mpress>=MPRESS_90BAR)||(ABS_fl==1)||(ABS_fr==1))
      #else
        else if((PARTIAL_BRAKE==0)||(mpress>=MPRESS_90BAR)||(ABS_fl==1)||(ABS_fr==1))
      #endif
        {
            INITIAL_BRAKE=0;
            INITIAL_BRAKE_old=1;
            Initial_Brake_reset_counter=0;
            Initial_Brake_set_counter = 0;
        }
        else
        {
            ;
        }
    }
    else
    {
        INITIAL_BRAKE=0;
        INITIAL_BRAKE_old=0;
        Initial_Brake_reset_counter=0;
        Initial_Brake_set_counter = 0;
    }
#endif 
}

#if ESC_PULSE_UP_RISE
void LCESP_vCalDeltaPressure4PUR(void)
{
	#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    if(lsespu1AHBGEN3MpresBrkOn == 1)
	#else
    if(MPRESS_BRAKE_ON == 1)
	#endif
    {
        if((FSF_fl==1)&&(AV_VL_fl==1)&&(ABS_fl_old_4PU==0)&&(ABS_fl==1))
        {
         #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
          ABSenterpress4PU_fl = lsesps16AHBGEN3mpress;
         #else
          ABSenterpress4PU_fl = mpress;
         #endif
          if((ABS_fz==1)&&(ABSenterpress4PU_fr==1))
          {
              ABSenterpress4PU_fl = ABSenterpress4PU_fr;
          }
	        else
	        {
	        	;
	        }
        }
        else
        {
        	;
        }
        if((FSF_fr==1)&&(AV_VL_fr==1)&&(ABS_fr_old_4PU==0)&&(ABS_fr==1))
        {
         #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
          ABSenterpress4PU_fr = lsesps16AHBGEN3mpress;
         #else
          ABSenterpress4PU_fr = mpress;
         #endif
          if((ABS_fz==1)&&(ABSenterpress4PU_fl==1))
          {
              ABSenterpress4PU_fr = ABSenterpress4PU_fl;
          }
          else
          {
          	;
          }
        }
        else
        {
        	;
        }
    }
    if((ABS_fl_old_4PU==1)&&(ABS_fl==0))
    {
        ABSenterpress4PU_fl = 0;
    }
    else
    {
    	;
    }
    if((ABS_fr_old_4PU==1)&&(ABS_fr==0))
    {
        ABSenterpress4PU_fr = 0;
    }
    else
    {
    	;
    }

	#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    if((lsespu1AHBGEN3MpresBrkOn==1)&&(ABS_fz==1))
	#else
    if((MPRESS_BRAKE_ON==1)&&(ABS_fz==1))
	#endif
    {
        if(SLIP_CONTROL_NEED_FL==1)
        {
            if((ABS_fl==0)&&(ABS_fr==1))
            {
                ABSenterpress4PU_fl = ABSenterpress4PU_fr;
            }
            else
            {
            	;
            }
            wheelpress4PU = ABSenterpress4PU_fl;
        }
        else
        {
            if((ABS_fl==0)&&(ABS_fr==1))
            {
                ABSenterpress4PU_fl = ABSenterpress4PU_fr;
            }
		        else
		        {
		        	;
		        }
            wheelpress4PU = ABSenterpress4PU_fl;
          
          #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
            if(wheelpress4PU>lsesps16AHBGEN3mpress)
            {
                wheelpress4PU = lsesps16AHBGEN3mpress;
            }
		        else
		        {
		        	;
		        }
          #else
            if(wheelpress4PU>mpress)
            {
                wheelpress4PU = mpress;
            }
		        else
		        {
		        	;
		        }
          #endif            
        }
    }
    else
    {
        ABSenterpress4PU_fl = 0;
        if(SLIP_CONTROL_NEED_FL==1)
        {
            if(esp_control_mode_fl==1)
            {
                esp_pressure4PU_fl = esp_pressure_count_fl * 14;   /* for 5ms cycle time, *10 -> *7(100/14)  */
            }
            else if(esp_control_mode_fl==2)
            {
                esp_pressure4PU_fl = esp_pressure_count_fl * 14;   /* for 5ms cycle time, *10 -> *7(100/14)  */
            }
            else if(esp_control_mode_fl==3)
            {
                esp_pressure4PU_fl = esp_pressure_count_fl * 14;   /* for 5ms cycle time, *10 -> *7(100/14)  */
            }
            else
            {
	            esp_pressure4PU_fl = esp_pressure_count_fl * 14;   /* for 5ms cycle time, *10 -> *7(100/14)  */
	            if(esp_pressure4PU_fl<0)
	            {
	                esp_pressure4PU_fl = 0;
	            }
	            else
	            {
	            	;
            	}
            }
            wheelpress4PU = esp_pressure4PU_fl;
        }
        else if(SLIP_CONTROL_NEED_FL_old==1)
        {
            esp_pressure4PU_fl = esp_pressure4PU_fl - 100;
            if(esp_pressure4PU_fl<0)
            {
                esp_pressure4PU_fl = 0;
            }
		        else
		        {
		        	;
		        }
            wheelpress4PU = esp_pressure4PU_fl;
        }
        else
        {
            esp_pressure4PU_fl = 0;
          
          #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
            wheelpress4PU = lsesps16AHBGEN3mpress;
          #else
            wheelpress4PU = mpress;
          #endif
        }
    }
    #if __PRESS_EST_ACTIVE
        wheelpress4PU = FL.s16_Estimated_Active_Press;
    #endif

   #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    delta_pressure4PU = lsesps16AHBGEN3mpress - wheelpress4PU;
   #else
    delta_pressure4PU = mpress - wheelpress4PU;
   #endif    
    
    if(delta_pressure4PU<0)
    {
    	delta_pressure4PU = 0;
    }
    else
    {
    	;
    }

    LCESP_vCalCompensationDutyFront4PUR();
    if((ABS_fl)&&(motor_pressure_need4PU==0))
    {
    	press_compensation_duty4PU=0;
    }
    else
    {
    	;
    }

    wheelpress_fl = wheelpress4PU;
    delta_pressure4PU_fl = delta_pressure4PU;
  
    if((SLIP_CONTROL_NEED_FL==1)&&(FLAG_1ST_CYCLE_FL==1)&&(FLAG_1ST_CYCLE_WHEEL_FL==1))
	  {	
				esp_tempW1 = McrAbs(delta_moment_q_os);
				
				if ( esp_tempW1 <= 1 )
				{
					esp_tempW1 = 1 ;
				}
				else
				{
					;      
				} 					
 
//    		esp_tempW2 = LCESP_s16IInter2Point( esp_tempW1, 1800, 0,   // 03.19 final
                                   	       		    			//2500, 10 );  // del_M <-> Open_Duty
                                   	       		    			
    		esp_tempW2 = LCESP_s16IInter2Point( esp_tempW1, S16_VAR_RISE_DEL_M_LOW, S16_VAR_RISE_ADD_DUTY_MIN,
                                   	       		    			S16_VAR_RISE_DEL_M_HIGH, S16_VAR_RISE_ADD_DUTY_MAX );  // del_M <-> Open_Duty                                   	       		    			
  
		}
		else
		{
			  esp_tempW2 = 0 ;
		}  

    comp_duty4PU_fl = delta_compensation_duty4PU + press_compensation_duty4PU + (signed char)esp_tempW2 ;


	#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    if((lsespu1AHBGEN3MpresBrkOn == 1)&&(ABS_fz == 1))
	#else
    if((MPRESS_BRAKE_ON == 1)&&(ABS_fz == 1))
	#endif
    {
        if(SLIP_CONTROL_NEED_FR == 1)
        {
            if((ABS_fr == 0)&&(ABS_fl == 1))
            {
                ABSenterpress4PU_fr = ABSenterpress4PU_fl;
            }
		        else
		        {
		        	;
		        }
            wheelpress4PU = ABSenterpress4PU_fr;
        }
        else
        {
            if((ABS_fr == 0)&&(ABS_fl == 1))
            {
                ABSenterpress4PU_fr = ABSenterpress4PU_fl;
            }
		        else
		        {
		        	;
		        }
            wheelpress4PU = ABSenterpress4PU_fr;

          #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
            if(wheelpress4PU > lsesps16AHBGEN3mpress)
            {
                wheelpress4PU = lsesps16AHBGEN3mpress;
            }
		        else
		        {
		        	;
		        }
          #else
            if(wheelpress4PU > mpress)
            {
                wheelpress4PU = mpress;
            }
		        else
		        {
		        	;
		        }
          #endif
        }
    }
    else
    {
        ABSenterpress4PU_fr = 0;
        if(SLIP_CONTROL_NEED_FR == 1)
        {
            if(esp_control_mode_fr == 1)
            {
                esp_pressure4PU_fr = esp_pressure_count_fr * 14;   /* for 5ms cycle time, *10 -> *7(100/14)  */
            }
            else if(esp_control_mode_fr == 2)
            {
                esp_pressure4PU_fr = esp_pressure_count_fr * 14;   /* for 5ms cycle time, *10 -> *7(100/14)  */
            }
            else if(esp_control_mode_fr == 3)
            {
                esp_pressure4PU_fr = esp_pressure_count_fr * 14;   /* for 5ms cycle time, *10 -> *7(100/14)  */
            }
            else
            {
                esp_pressure4PU_fr = esp_pressure_count_fr * 14;   /* for 5ms cycle time, *10 -> *7(100/14)  */
                if(esp_pressure4PU_fr<0)
                {
                esp_pressure4PU_fr = 0;
                }
                else
                {
                	;
                }
            }
            wheelpress4PU = esp_pressure4PU_fr;
        }
        else if(SLIP_CONTROL_NEED_FR_old == 1)
        {
            esp_pressure4PU_fr = esp_pressure4PU_fr - 100;
            if(esp_pressure4PU_fr<0)
            {
                esp_pressure4PU_fr = 0;
            }
		        else
		        {
		        	;
		        }
            wheelpress4PU = esp_pressure4PU_fr;
        }
        else
        {
            esp_pressure4PU_fr = 0;
          
          #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
            wheelpress4PU = lsesps16AHBGEN3mpress;
          #else
            wheelpress4PU = mpress;
          #endif
        }
    }

    #if __PRESS_EST_ACTIVE
        wheelpress4PU = FR.s16_Estimated_Active_Press;
    #endif

   #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    delta_pressure4PU = lsesps16AHBGEN3mpress - wheelpress4PU;
   #else
    delta_pressure4PU = mpress - wheelpress4PU;
   #endif
    
    if(delta_pressure4PU<0)
    {
    	delta_pressure4PU = 0;
    }
    else
    {
    	;
    }

    LCESP_vCalCompensationDutyFront4PUR();
    if((ABS_fr)&&(motor_pressure_need4PU==0))
    {
    	press_compensation_duty4PU=0;
    }
    else
    {
    	;
    }

    wheelpress_fr = wheelpress4PU;
    delta_pressure4PU_fr = delta_pressure4PU;
 
	  if((SLIP_CONTROL_NEED_FR==1)&&(FLAG_1ST_CYCLE_FR==1)&&(FLAG_1ST_CYCLE_WHEEL_FR==1))
	  {	
				esp_tempW1 = McrAbs(delta_moment_q_os);
				
				if ( esp_tempW1 <= 1 )
				{
					esp_tempW1 = 1 ;
				}
				else
				{
					;      
				} 					
 
    		esp_tempW2 = LCESP_s16IInter2Point( esp_tempW1, S16_VAR_RISE_DEL_M_LOW, S16_VAR_RISE_ADD_DUTY_MIN,
                                   	       		    			S16_VAR_RISE_DEL_M_HIGH, S16_VAR_RISE_ADD_DUTY_MAX ) ;       	 
  
		}
		else
		{
			  esp_tempW2 = 0 ;
		}  
		
    comp_duty4PU_fr = delta_compensation_duty4PU + press_compensation_duty4PU + (signed char)esp_tempW2 ;


	#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    if(lsespu1AHBGEN3MpresBrkOn == 1)
	#else
    if(MPRESS_BRAKE_ON == 1)
	#endif
    {
        if((ABS_rl_old_4PU == 0)&&(ABS_rl == 1))
        {
          #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
            ABSenterpress4PU_rl = lsesps16AHBGEN3mpress;
          #else
            ABSenterpress4PU_rl = mpress;
          #endif
        }
        else
        {
        	;
        }
        if(ABS_rl == 1)
        {
            wheelpress4PU = ABSenterpress4PU_rl;
          
          #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
            if(wheelpress4PU > lsesps16AHBGEN3mpress)
            {
                wheelpress4PU = lsesps16AHBGEN3mpress;
            }
            else
            {
            	;
            }
          #else
            if(wheelpress4PU > mpress)
            {
                wheelpress4PU = mpress;
            }
            else
            {
            	;
            }
          #endif            
        }
        else if(SLIP_CONTROL_NEED_RL == 1)
        {
            if(esp_control_mode_rl == 2)
            {
                esp_pressure4PU_rl = esp_pressure_count_rl * 14;   /* for 5ms cycle time, *10 -> *7(100/14)  */
            }
            else
            {
                esp_pressure4PU_rl = esp_pressure_count_rl * 14;   /* for 5ms cycle time, *10 -> *7(100/14)  */
            }
            wheelpress4PU = esp_pressure4PU_rl;
        }
        else
        {
          #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
            wheelpress4PU = lsesps16AHBGEN3mpress;
          #else
            wheelpress4PU = mpress;
          #endif
        }
    }
    else
    {
        ABSenterpress4PU_rl = 0;
        if(SLIP_CONTROL_NEED_RL == 1)
        {
            if(esp_control_mode_rl == 1)
            {
                esp_pressure4PU_rl = esp_pressure_count_rl * 14;   /* for 5ms cycle time, *10 -> *7(100/14)  */
            }
            else if(esp_control_mode_rl == 2)
            {
                esp_pressure4PU_rl = esp_pressure_count_rl * 14;   /* for 5ms cycle time, *10 -> *7(100/14)  */
            }
            else if(esp_control_mode_rl == 3)
            {
                esp_pressure4PU_rl = esp_pressure_count_rl * 14;   /* for 5ms cycle time, *10 -> *7(100/14)  */
            }
            else
            {
                esp_pressure4PU_rl = esp_pressure_count_rl * 14;   /* for 5ms cycle time, *10 -> *7(100/14)  */
                if(esp_pressure4PU_rl < 0)
                {
                	esp_pressure4PU_rl = 0;
                }
				        else
				        {
				        	;
				        }
            }
            wheelpress4PU = esp_pressure4PU_rl;
        }
        else if(SLIP_CONTROL_NEED_RL_old == 1)
        {
            esp_pressure4PU_rl = esp_pressure4PU_rl - 100;
            if(esp_pressure4PU_rl < 0)
            {
                esp_pressure4PU_rl = 0;
            }
		        else
		        {
		        	;
		        }
            wheelpress4PU = esp_pressure4PU_rl;
        }
        else
        {
            esp_pressure4PU_rl = 0;
          
          #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
            wheelpress4PU = lsesps16AHBGEN3mpress;
          #else
            wheelpress4PU = mpress;
          #endif
        }
    }

    #if __PRESS_EST_ACTIVE
        wheelpress4PU = RL.s16_Estimated_Active_Press;
    #endif

   #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    delta_pressure4PU = lsesps16AHBGEN3mpress - wheelpress4PU;
   #else
    delta_pressure4PU = mpress - wheelpress4PU;
   #endif
    
    if(delta_pressure4PU < 0)
    {
    	delta_pressure4PU = 0;
    }
    else
    {
    	;
    }

    LCESP_vCalCompensationDutyRear4PUR();
    if((ABS_rl == 1)&&(motor_pressure_need4PU == 0))
    {
    	press_compensation_duty4PU=0;
    }
    else
    {
    	;
    }

    wheelpress_rl = wheelpress4PU;
    delta_pressure4PU_rl = delta_pressure4PU;

  #if (__Ext_Under_Ctrl == 1)  
  
    if(EXTREME_UNDER_CONTROL ==1)    
    { 
    	comp_duty4PU_rl = delta_compensation_duty4PU + press_compensation_duty4PU + S16_ESC_P_UP_INI_DUTY_R_EXT_UND ;  
    }           
    else
  	{
    	comp_duty4PU_rl = delta_compensation_duty4PU + press_compensation_duty4PU;
  	}      
  	
	#else
	
    comp_duty4PU_rl = delta_compensation_duty4PU + press_compensation_duty4PU;	
	  	
  #endif 


	#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    if(lsespu1AHBGEN3MpresBrkOn == 1)
	#else
    if(MPRESS_BRAKE_ON == 1)
	#endif
    {
        if((ABS_rr_old_4PU == 0)&&(ABS_rr == 1))
        {
          #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
            ABSenterpress4PU_rr = lsesps16AHBGEN3mpress;
          #else
            ABSenterpress4PU_rr = mpress;
          #endif
        }
        if(ABS_rr == 1)
        {
            wheelpress4PU = ABSenterpress4PU_rr;
          
          #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
            if(wheelpress4PU>lsesps16AHBGEN3mpress)
            {
                wheelpress4PU = lsesps16AHBGEN3mpress;
            }
            else
            {
            	;
            }
          #else
            if(wheelpress4PU>mpress)
            {
                wheelpress4PU = mpress;
            }
            else
            {
            	;
            }
          #endif
        }
        else if(SLIP_CONTROL_NEED_RR == 1)
        {
            if(esp_control_mode_rr == 2)
            {
                esp_pressure4PU_rr = esp_pressure_count_rr * 14;   /* for 5ms cycle time, *10 -> *7(100/14)  */
            }
            else
            {
                esp_pressure4PU_rr = esp_pressure_count_rr * 14;   /* for 5ms cycle time, *10 -> *7(100/14)  */
            }
            wheelpress4PU = esp_pressure4PU_rr;
        }
        else
        {
          #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
            wheelpress4PU = lsesps16AHBGEN3mpress;
          #else
            wheelpress4PU = mpress;
          #endif
        }
    }
    else
    {
        ABSenterpress4PU_rr = 0;
        if(SLIP_CONTROL_NEED_RR == 1)
        {
            if(esp_control_mode_rr == 1)
            {
                esp_pressure4PU_rr = esp_pressure_count_rr * 14;   /* for 5ms cycle time, *10 -> *7(100/14)  */
            }
            else if(esp_control_mode_rr == 2)
            {
                esp_pressure4PU_rr = esp_pressure_count_rr * 14;   /* for 5ms cycle time, *10 -> *7(100/14)  */
            }
            else if(esp_control_mode_rr == 3)
            {
                esp_pressure4PU_rr = esp_pressure_count_rr * 14;   /* for 5ms cycle time, *10 -> *7(100/14)  */
            }
            else
            {
                esp_pressure4PU_rr = esp_pressure_count_rr * 14;   /* for 5ms cycle time, *10 -> *7(100/14)  */
            }
            wheelpress4PU = esp_pressure4PU_rr;
        }
        else if(SLIP_CONTROL_NEED_RR_old == 1)
        {
            esp_pressure4PU_rr = esp_pressure4PU_rr - 100;
            if(esp_pressure4PU_rr < 0)
            {
                esp_pressure4PU_rr = 0;
            }
            wheelpress4PU = esp_pressure4PU_rr;
        }
        else
        {
            esp_pressure4PU_rr = 0;
          
          #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
            wheelpress4PU = lsesps16AHBGEN3mpress;
          #else
            wheelpress4PU = mpress;
          #endif
        }
    }

    #if __PRESS_EST_ACTIVE
        wheelpress4PU = RR.s16_Estimated_Active_Press;
    #endif

    #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    delta_pressure4PU = lsesps16AHBGEN3mpress - wheelpress4PU;
    #else
    delta_pressure4PU = mpress - wheelpress4PU;
    #endif
    
    if(delta_pressure4PU<0)
    {
    	delta_pressure4PU = 0;
    }
    else
    {
    	;
    }

    LCESP_vCalCompensationDutyRear4PUR();
    if((ABS_rr == 1)&&(motor_pressure_need4PU==0))
    {
    	press_compensation_duty4PU=0;
    }
    else
    {
    	;
    }

    wheelpress_rr = wheelpress4PU;
    delta_pressure4PU_rr = delta_pressure4PU;

  #if (__Ext_Under_Ctrl == 1)  
  
    if(EXTREME_UNDER_CONTROL ==1)    
    { 
    	comp_duty4PU_rr = delta_compensation_duty4PU + press_compensation_duty4PU + S16_ESC_P_UP_INI_DUTY_R_EXT_UND ;  
    }           
    else
  	{
    	comp_duty4PU_rr = delta_compensation_duty4PU + press_compensation_duty4PU;
  	}      
  	
	#else
	
    comp_duty4PU_rr = delta_compensation_duty4PU + press_compensation_duty4PU;	
	  	
  #endif

    ABS_fl_old_4PU = ABS_fl;
    ABS_fr_old_4PU = ABS_fr;
    ABS_rl_old_4PU = ABS_rl;
    ABS_rr_old_4PU = ABS_rr;


    if(PARTIAL_BRAKE_fl==1)
    {
        if(delta_pressure4PU_fl>(DELTA_PRESS_L_TH_FRONT+100))
        {
            partial_brake_fl_exit_cnt4PU++;
            if(SLIP_CONTROL_NEED_FL == 1)
            {
                if((ABS_fz==1)&&(partial_brake_fl_exit_cnt4PU>14))
                {
                    PARTIAL_BRAKE_fl=0;
                }
                else if(delta_pressure4PU_fl>(DELTA_PRESS_L_TH_FRONT+300))
                {
                    PARTIAL_BRAKE_fl=0;
                }
                else
                {
                    ;
                }
            }
            else
            {
                PARTIAL_BRAKE_fl=0;
                partial_brake_fl_exit_cnt4PU=0;
            }
        }
        else
        {
            partial_brake_fl_exit_cnt4PU=0;
        }
    }
    else
    {
        if(delta_pressure4PU_fl<=DELTA_PRESS_L_TH_FRONT)
        {
            PARTIAL_BRAKE_fl=1;
        }
        else
        {
            PARTIAL_BRAKE_fl=0;
            partial_brake_fl_exit_cnt4PU=0;
        }
    }

    if(PARTIAL_BRAKE_fr==1)
    {
        if(delta_pressure4PU_fr>(DELTA_PRESS_L_TH_FRONT+100))
        {
            partial_brake_fr_exit_cnt4PU++;
            if(SLIP_CONTROL_NEED_FR == 1)
            {
                if((ABS_fz==1)&&(partial_brake_fr_exit_cnt4PU>14))
                {
                    PARTIAL_BRAKE_fr=0;
                }
                else if(delta_pressure4PU_fr>(DELTA_PRESS_L_TH_FRONT+300))
                {
                    PARTIAL_BRAKE_fr=0;
                }
                else
                {
                    ;
                }
            }
            else
            {
                PARTIAL_BRAKE_fr=0;
                partial_brake_fr_exit_cnt4PU=0;
            }
        }
        else
        {
            partial_brake_fr_exit_cnt4PU=0;
        }
    }
    else
    {
        if(delta_pressure4PU_fr<=DELTA_PRESS_L_TH_FRONT)
        {
            PARTIAL_BRAKE_fr=1;
        }
        else
        {
            PARTIAL_BRAKE_fr=0;
            partial_brake_fr_exit_cnt4PU=0;
        }
    }

    if(PARTIAL_BRAKE_rl==1)
    {
        if(delta_pressure4PU_rl>(DELTA_PRESS_L_TH_REAR+100))
        {
            partial_brake_rl_exit_cnt4PU++;
            if(SLIP_CONTROL_NEED_RL == 1)
            {
                if((ABS_fz==1)&&(partial_brake_rl_exit_cnt4PU>14))
                {
                    PARTIAL_BRAKE_rl=0;
                }
                else if(delta_pressure4PU_rl>(DELTA_PRESS_L_TH_REAR+300))
                {
                    PARTIAL_BRAKE_rl=0;
                }
                else
                {
                    ;
                }
            }
            else
            {
                PARTIAL_BRAKE_rl=0;
                partial_brake_rl_exit_cnt4PU=0;
            }
        }
        else
        {
            partial_brake_rl_exit_cnt4PU=0;
        }
    }
    else
    {
        if(delta_pressure4PU_rl<=DELTA_PRESS_L_TH_REAR)
        {
            PARTIAL_BRAKE_rl=1;
        }
        else
        {
            PARTIAL_BRAKE_rl=0;
            partial_brake_rl_exit_cnt4PU=0;
        }
    }

    if(PARTIAL_BRAKE_rr==1)
    {
        if(delta_pressure4PU_rr>(DELTA_PRESS_L_TH_REAR+100))
        {
            partial_brake_rr_exit_cnt4PU++;
            if(SLIP_CONTROL_NEED_RR)
            {
                if((ABS_fz==1)&&(partial_brake_rr_exit_cnt4PU>14))
                {
                    PARTIAL_BRAKE_rr=0;
                }
                else if(delta_pressure4PU_rr>(DELTA_PRESS_L_TH_REAR+300))
                {
                    PARTIAL_BRAKE_rr=0;
                }
                else
                {
                    ;
                }
            }
            else
            {
                PARTIAL_BRAKE_rr=0;
                partial_brake_rr_exit_cnt4PU=0;
            }
        }
        else
        {
            partial_brake_rr_exit_cnt4PU=0;
        }
    }
    else
    {
        if(delta_pressure4PU_rr<=DELTA_PRESS_L_TH_REAR)
        {
            PARTIAL_BRAKE_rr=1;
        }
        else
        {
            PARTIAL_BRAKE_rr=0;
            partial_brake_rr_exit_cnt4PU=0;
        }
    }

    if((SLIP_CONTROL_NEED_FL == 1)||(SLIP_CONTROL_NEED_FL_old == 1))
    {
        PARTIAL_BRAKE = PARTIAL_BRAKE_fl;
    }
    else if((SLIP_CONTROL_NEED_FR == 1)||(SLIP_CONTROL_NEED_FR_old == 1))
    {
        PARTIAL_BRAKE = PARTIAL_BRAKE_fr;
    }
    else if((SLIP_CONTROL_NEED_RL == 1)||(SLIP_CONTROL_NEED_RL_old == 1))
    {
        PARTIAL_BRAKE = PARTIAL_BRAKE_rl;
    }
    else if((SLIP_CONTROL_NEED_RR == 1)||(SLIP_CONTROL_NEED_RR_old == 1))
    {
        PARTIAL_BRAKE = PARTIAL_BRAKE_rr;
    }
    else
    {
        PARTIAL_BRAKE = 0;
    }
}

void LCESP_vCalCompensationDutyFront4PUR(void)
{
    if(ABS_fz == 0)
    {
        motor_pressure_need4PU = 1;
        delta_compensation_duty4PU = 0;
    }
    else if(delta_pressure4PU<DELTA_PRESS_L_TH_FRONT)
    {
        motor_pressure_need4PU = 1;
        delta_compensation_duty4PU = 0;
    }
    else if(delta_pressure4PU<DELTA_PRESS_U_TH_FRONT)
    {
        motor_pressure_need4PU = 0;

        delta_compensation_duty4PU =(int8_t)
        LCESP_s16IInter3Point( delta_pressure4PU, DELTA_PRESS_L_TH_FRONT, DELTA_COMPENSATION_DUTY_PU_L_FRONT,
                                          DELTA_PRESS_M_TH_FRONT, DELTA_COMPENSATION_DUTY_PU_M_FRONT,
                                          DELTA_PRESS_U_TH_FRONT, DELTA_COMPENSATION_DUTY_PU_U_FRONT);

        if(delta_compensation_duty4PU<-10)
        {
        	delta_compensation_duty4PU=-10;
        }
        else
        {
        	;
        }
        if(delta_compensation_duty4PU>10)
        {
        	delta_compensation_duty4PU=10;
        }
        else
        {
        	;
        }
        
      #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
        if(lsesps16AHBGEN3mpress<10)
      #else
        if(mpress<10)
      #endif        
        {
        	delta_compensation_duty4PU=0;
        }
        else
        {
        	;
        }
    }
    else
    {
        motor_pressure_need4PU = 0;
        delta_compensation_duty4PU = DELTA_COMPENSATION_DUTY_PU_U_FRONT;
    }

  #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    if((lsesps16AHBGEN3mpress<100)
  #else
    if((mpress<100)
  #endif
        #if (__ACC)
            &&(lcu1AccActiveFlg==0)
        #endif
        #if (__HDC)
            &&(lcu1HdcActiveFlg==0)
        #endif
        )
    {
        press_compensation_duty4PU = 0;
    }
    else
    {
        press_compensation_duty4PU =(int8_t)
        LCESP_s16IInter2Point( wheelpress4PU, ESP_PRESS_L_TH_FRONT, PRESS_COMPENSATION_DUTY_PU_L_FRONT,
                                          ESP_PRESS_U_TH_FRONT, PRESS_COMPENSATION_DUTY_PU_U_FRONT);
    }

    if(press_compensation_duty4PU>0)
    {
    	press_compensation_duty4PU=0;
    }
    else
    {
    	;
    }
    if(press_compensation_duty4PU<-10)
    {
    	press_compensation_duty4PU=-10;
    }
    else
    {
    	;
    }
}

void LCESP_vCalCompensationDutyRear4PUR(void)
{
    if(ABS_fz == 0)
    {
        motor_pressure_need4PU = 1;
        delta_compensation_duty4PU = 0;
    }
    else if(delta_pressure4PU<DELTA_PRESS_L_TH_REAR)
    {
        motor_pressure_need4PU = 1;
        delta_compensation_duty4PU = 0;
    }
    else if(delta_pressure4PU<DELTA_PRESS_U_TH_REAR)
    {
        motor_pressure_need4PU = 0;
        delta_compensation_duty4PU =(int8_t)
        LCESP_s16IInter3Point( delta_pressure4PU, DELTA_PRESS_L_TH_REAR, DELTA_COMPENSATION_DUTY_PU_L_REAR,
                                          DELTA_PRESS_M_TH_REAR, DELTA_COMPENSATION_DUTY_PU_M_REAR,
                                          DELTA_PRESS_U_TH_REAR, DELTA_COMPENSATION_DUTY_PU_U_REAR);

        if(delta_compensation_duty4PU<-10)
        {
        	delta_compensation_duty4PU=-10;
        }
        else
        {
        	;
        }
        if(delta_compensation_duty4PU>10)
        {
        	delta_compensation_duty4PU=10;
        }
        else
        {
        	;
        }

      #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
        if(lsesps16AHBGEN3mpress<10)
      #else
        if(mpress<10)
      #endif        
        {
        	delta_compensation_duty4PU=0;
        }
        else
        {
        	;
        }
    }
    else
    {
        motor_pressure_need4PU = 0;
        delta_compensation_duty4PU = DELTA_COMPENSATION_DUTY_U_REAR;
    }

  #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    if((lsesps16AHBGEN3mpress<100)
  #else
    if((mpress<100)
  #endif
        #if (__ACC)
            &&(lcu1AccActiveFlg==0)
        #endif
        #if (__HDC)
            &&(lcu1HdcActiveFlg==0)
        #endif
        #if (__Ext_Under_Ctrl)
            &&(EXTREME_UNDER_CONTROL==0)
        #endif        
        )    	 
    {
        press_compensation_duty4PU =(int8_t)
        LCESP_s16IInter2Point( wheelpress4PU, ESP_PRESS_L_TH_REAR, PRESS_COMPENSATION_DUTY_PU_L_REAR,
                                          ESP_PRESS_U_TH_REAR, PRESS_COMPENSATION_DUTY_PU_U_REAR);                                        
    }
    else
    {
    	press_compensation_duty4PU = 0;
    }

    if(press_compensation_duty4PU>0)
    {
    	press_compensation_duty4PU=0;
    }
    else
    {
    	;
    }
    if(press_compensation_duty4PU<-10)
    {
    	press_compensation_duty4PU=-10;
    }
    else
    {
    	;
    }
}
#endif

#if ESC_PULSE_UP_RISE
void LCESP_vCalEscRiseRate(void)
{
	
#if	( __Ext_Under_Ctrl == 1 )	

  if( EXTREME_UNDER_CONTROL ==1 )
  {	
		esp_tempW0 = S16_EXT_UND_PULSE_UP_CYCLE_SCAN_F ;
	}
	else
	{
		esp_tempW0 = S16_ESC_PULSE_UP_CYCLE_SCAN_F ;
	}
	
#else
	
		esp_tempW0 = S16_ESC_PULSE_UP_CYCLE_SCAN_F ;
  
#endif	
	
	lcs16EscInitialPulseupDutyfl_old = lcs16EscInitialPulseupDutyfl;
	lcs16EscInitialPulseupDutyfr_old = lcs16EscInitialPulseupDutyfr;
	
  /* determine (initial duty) & (add duty) & (cycle) */                                                         
	LCESP_vCalEscRiseRateDuty();
	
/*****    FL    *****/
      #if (__TCMF_UNDERSTEER_CONTROL == ENABLE) 
    if((SLIP_CONTROL_NEED_FL == 1) && (lcu1US2WHFadeout_RL == 0) && (lcu1US2WHControlFlag_RL == 0) 
    	#if(__ESC_TCMF_CONTROL ==1)
    	&& (FL_ESC.lcespu1TCMFControl ==0)
    	#endif
    	)
      #else	
	if(SLIP_CONTROL_NEED_FL == 1)
	  #endif
	{
		
		#if	( __Ext_Under_Ctrl == 1 )	
		
		  if(( EXTREME_UNDER_CONTROL ==1 )&&( EXTR_UNDER_FL_IN == 1 ))
		  {	
				temp_ini_duty_f = S16_EX_UND_PULSE_UP_INI_DUTY_F_IN ;
			}
			else if (( EXTREME_UNDER_CONTROL ==1 )&&( EXTR_UNDER_FL_IN == 0 ))
			{ 
				temp_ini_duty_f = S16_EX_UND_PULSE_UP_INI_DUTY_F_OUT ;				
			}
			else
			{
				  ;
			}
			
		#endif	 
				
		if((esp_pressure_count_fl >= pu_full_T_fl)&&(esc_pulse_up_rise_scan_fl==0)&&(esp_control_mode_fl < 3))
		{
			esc_pulse_up_rise_scan_fl++;		
 
			lcs16EscInitialPulseupDutyfl = temp_ini_duty_f + comp_duty4PU_fl;

	   	pulseup_cycle_scan_time_fl = esp_tempW0;

			ESC_PULSE_UP_RISE_FL = 1;
			pu_full_T_fl = 0;
								
		}	
		else if((esc_pulse_up_rise_scan_fl > 0)&&(esp_control_mode_fl < 3))
		{
			esc_pulse_up_rise_scan_fl++;
			
			ESC_PULSE_UP_RISE_FL = 1;
			pu_full_T_fl = 0;
			
		/*   Duty addition each tuning scan time   */
		/*   (current duty)=(initial duty)+(Duty change by scan)+(Duty compensation by cycle change) */
			lcs16EscInitialPulseupDutyfl = temp_ini_duty_f
									+ ((int16_t)(esc_pulse_up_rise_scan_fl/temp_duty_add_cycle_f)*temp_duty_add_duty_f) 
									+ lcespCalRiserateAddDutyfl + comp_duty4PU_fl;

		}
  #if ( __Ext_Under_Ctrl == 1 )			
		else if((esc_pulse_up_rise_scan_fl > 0)&&(((esp_control_mode_fl <= 4)&&( EXTREME_UNDER_CONTROL==1))||(esp_control_mode_fl <= 4)))
	#else			  
		else if((esc_pulse_up_rise_scan_fl > 0)&&(esp_control_mode_fl < 4))
	#endif	
		{
			ESC_PULSE_UP_RISE_FL = 1;   
					
		}
		else
		{
			lcs16EscInitialPulseupDutyfl = temp_ini_duty_f;
			esc_pulse_up_rise_scan_fl = 0;
	    pulseup_cycle_scan_time_fl = esp_tempW0;
		
		}		
		
		/*   Duty limitation    */
			LCESP_vCalEscRiseRateLimitDuty(lcs16EscInitialPulseupDutyfl, 
			              S16_ESC_PULSE_UP_MIN_DUTY_F, S16_ESC_PULSE_UP_MAX_DUTY_F, 1);


		/* In case of increase in variable_rise_scan with ROP or vehicle spin  */
  #if ( __Ext_Under_Ctrl == 1 )			
    #if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
    if(((esp_pressure_count_fl < pu_full_T_fl) ||(FL_ESC.FULL_RISE_MODE==1))&&( EXTREME_UNDER_CONTROL==0))
    #else
		if(((esp_pressure_count_fl < pu_full_T_fl) ||(FULL_RISE_MODE==1))&&( EXTREME_UNDER_CONTROL==0))
    #endif	
	#else
	  #if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
    if((esp_pressure_count_fl < pu_full_T_fl) ||(FL_ESC.FULL_RISE_MODE==1))
	#else			  
		if((esp_pressure_count_fl < pu_full_T_fl) ||(FULL_RISE_MODE==1))
	#endif	
	#endif	
		{
			lcs16EscInitialPulseupDutyfl = esp_tempW0*10;			
			esc_pulse_up_rise_scan_fl = 0;
			pulseup_cycle_scan_time_fl = esp_tempW0;
			ESC_PULSE_UP_RISE_FL = 0;
		}
		else
		{
			;
		}

	}
	else
	{
   	    pulseup_cycle_scan_time_fl = esp_tempW0;
		esc_pulse_up_rise_scan_fl = 0;

		lcs16EscInitialPulseupDutyfl = temp_ini_duty_f;
		pu_full_T_fl = 0;
		ESC_PULSE_UP_RISE_FL = 0;		
		
				
	}

/*****    FR    *****/	
      #if (__TCMF_UNDERSTEER_CONTROL == ENABLE)
    if((SLIP_CONTROL_NEED_FR == 1) && (lcu1US2WHFadeout_RR == 0) && (lcu1US2WHControlFlag_RR == 0)
      	#if(__ESC_TCMF_CONTROL ==1)
    	&& (FR_ESC.lcespu1TCMFControl ==0)
    	#endif
	)
      #else   
	if(SLIP_CONTROL_NEED_FR==1)
	  #endif
	{

		#if	( __Ext_Under_Ctrl == 1 )	
		
		  if(( EXTREME_UNDER_CONTROL ==1 )&&( EXTR_UNDER_FR_IN == 1 ))
		  {	
				temp_ini_duty_f = S16_EX_UND_PULSE_UP_INI_DUTY_F_IN ;
			}
			else if (( EXTREME_UNDER_CONTROL ==1 )&&( EXTR_UNDER_FR_IN == 0 ))
			{ 
				temp_ini_duty_f = S16_EX_UND_PULSE_UP_INI_DUTY_F_OUT ;				
			}
			else
			{
				  ;
			}
			
		#endif			    
		    
		if((esp_pressure_count_fr >= pu_full_T_fr)&&(esc_pulse_up_rise_scan_fr==0)&&(esp_control_mode_fr<3))
		{
			esc_pulse_up_rise_scan_fr++;		
  
			lcs16EscInitialPulseupDutyfr = temp_ini_duty_f + comp_duty4PU_fr;

	    pulseup_cycle_scan_time_fr = esp_tempW0;

			ESC_PULSE_UP_RISE_FR = 1;

			pu_full_T_fr = 0;
			
							
		}	
		else if((esc_pulse_up_rise_scan_fr > 0)&&(esp_control_mode_fr<3))
		{
			esc_pulse_up_rise_scan_fr++;
			
			ESC_PULSE_UP_RISE_FR = 1;
    	    
    	    pu_full_T_fr = 0;   

		/*   Duty addition each tuning scan time   */
			lcs16EscInitialPulseupDutyfr = temp_ini_duty_f
									+ ((int16_t)(esc_pulse_up_rise_scan_fr/temp_duty_add_cycle_f)*temp_duty_add_duty_f) 
									+ lcespCalRiserateAddDutyfr + comp_duty4PU_fr;			

		}
  #if ( __Ext_Under_Ctrl == 1 )			
		else if((esc_pulse_up_rise_scan_fr > 0)&&(((esp_control_mode_fr <= 4)&&( EXTREME_UNDER_CONTROL==1))||(esp_control_mode_fr <= 4)))
	#else			  
		else if((esc_pulse_up_rise_scan_fr > 0)&&(esp_control_mode_fr<4))
	#endif
		{
			ESC_PULSE_UP_RISE_FR = 1;
							
    }
		else
		{
			lcs16EscInitialPulseupDutyfr = temp_ini_duty_f;
			esc_pulse_up_rise_scan_fr = 0;
	    pulseup_cycle_scan_time_fr = esp_tempW0;

	/*		ESC_PULSE_UP_RISE_FRONT = 0; */
	
					
		}		
	
		/*   Duty limitation    */
			LCESP_vCalEscRiseRateLimitDuty(lcs16EscInitialPulseupDutyfr, 
			              S16_ESC_PULSE_UP_MIN_DUTY_F, S16_ESC_PULSE_UP_MAX_DUTY_F, 2);

		/* In case of increase in variable_rise_scan with ROP or vehicle spin  */
  #if ( __Ext_Under_Ctrl == 1 )			
    #if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
    if(((esp_pressure_count_fr < pu_full_T_fr) ||(FR_ESC.FULL_RISE_MODE==1))&&( EXTREME_UNDER_CONTROL==0))
    #else
		if(((esp_pressure_count_fr < pu_full_T_fr) ||(FULL_RISE_MODE==1))&&( EXTREME_UNDER_CONTROL==0))
    #endif	
	#else
	  #if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
    if((esp_pressure_count_fr < pu_full_T_fr) ||(FR_ESC.FULL_RISE_MODE==1))
	#else			  
		if((esp_pressure_count_fr < pu_full_T_fr) ||(FULL_RISE_MODE==1))
	#endif
	#endif
		{
			lcs16EscInitialPulseupDutyfr = esp_tempW0*10;			
			esc_pulse_up_rise_scan_fr = 0;
			pulseup_cycle_scan_time_fr = esp_tempW0;
			ESC_PULSE_UP_RISE_FR = 0;
		}
		else
		{
			;
		}

	}
	else
	{
		pulseup_cycle_scan_time_fr = esp_tempW0;
		esc_pulse_up_rise_scan_fr = 0;

		lcs16EscInitialPulseupDutyfr = temp_ini_duty_f;
		pu_full_T_fr = 0;
		ESC_PULSE_UP_RISE_FR = 0;
		
						
	}
       	
	if((ESC_PULSE_UP_RISE_FL == 1) || (ESC_PULSE_UP_RISE_FR == 1))
	{
		ESC_PULSE_UP_RISE_FRONT = 1;
	}
	else
	{
		ESC_PULSE_UP_RISE_FRONT = 0;
	}


/*****    RL    *****/
      #if (__TCMF_UNDERSTEER_CONTROL == ENABLE)
  if((SLIP_CONTROL_NEED_RL == 1) && (lcescu1TCMFUnderControlRl == 0) && (lcu1OVER2WHControlFlag_RL == 0)&&(lcu1OVER2WHFadeout_RL == 0)&& (lcu1US3WHControlFlag == 0)) 
      #else  
	if(SLIP_CONTROL_NEED_RL == 1)
	  #endif
	{
		if((esc_pulse_up_rise_scan_rl==0)&&(esp_control_mode_rl<3))
		{
			esc_pulse_up_rise_scan_rl++;		

			lcs16EscInitialPulseupDutyrl = temp_ini_duty_r + comp_duty4PU_rl;

	    pulseup_cycle_scan_time_rl = S16_ESC_PULSE_UP_CYCLE_SCAN_R;

			ESC_PULSE_UP_RISE_RL = 1;

		}	
		else if((esc_pulse_up_rise_scan_rl > 0)&&(esp_control_mode_rl<3))
		{
			esc_pulse_up_rise_scan_rl++;
			
			ESC_PULSE_UP_RISE_RL = 1;
			
		/*   Duty addition each tuning scan time   */
			lcs16EscInitialPulseupDutyrl = temp_ini_duty_r 
										+ ((int16_t)(esc_pulse_up_rise_scan_rl/temp_duty_add_cycle_r)*temp_duty_add_duty_r) 
										+ comp_duty4PU_rl;
		
		}
		else if(((esc_pulse_up_rise_scan_rl > 0)&&(esp_control_mode_rl<4)) || (RL.u8_Estimated_Active_Press >= 0))
		{
			ESC_PULSE_UP_RISE_RL = 1;
		}
		else
		{
			lcs16EscInitialPulseupDutyrl = temp_ini_duty_r;
			esc_pulse_up_rise_scan_rl = 0;
	    pulseup_cycle_scan_time_rl = S16_ESC_PULSE_UP_CYCLE_SCAN_R;
			
		}		
	
		/*   Duty limitation    */
			LCESP_vCalEscRiseRateLimitDuty(lcs16EscInitialPulseupDutyrl, 
			              S16_ESC_PULSE_UP_MIN_DUTY_R, S16_ESC_PULSE_UP_MAX_DUTY_R, 3);
			              
		/*  '11.04 GSUV Opel   */
					              
			lcs8EscCloseDuty_RL = lcs16EscInitialPulseupDutyrl ;					              
	}
	  #if (__TCMF_UNDERSTEER_CONTROL == ENABLE)
	else if (((SLIP_CONTROL_NEED_RL == 1)||(SLIP_CONTROL_NEED_RL_old == 1)) && ((lcescu1TCMFUnderControlRl == 1)||(lcu1OVER2WHControlFlag_RL == 1)||(lcu1OVER2WHFadeout_RL == 1)||(lcu1US3WHControlFlag == 1)))
	{
	    pulseup_cycle_scan_time_rl = S16_ESC_PULSE_UP_CYCLE_SCAN_R;
	    esc_pulse_up_rise_scan_rl = 0;
	    lcs16EscInitialPulseupDutyrl = S16_ESC_PULSE_UP_CYCLE_SCAN_R * 10; /*10msec*/
	    ESC_PULSE_UP_RISE_RL = 0;
	}
	  #endif
	else
	{
        pulseup_cycle_scan_time_rl = S16_ESC_PULSE_UP_CYCLE_SCAN_R;
		esc_pulse_up_rise_scan_rl = 0;

		lcs16EscInitialPulseupDutyrl = temp_ini_duty_r;
			ESC_PULSE_UP_RISE_RL = 0;		
	}

/*****    RR    *****/
	  #if (__TCMF_UNDERSTEER_CONTROL == ENABLE)
    if((SLIP_CONTROL_NEED_RR == 1)&&(lcescu1TCMFUnderControlRr == 0)&&(lcu1OVER2WHControlFlag_RR == 0)&&(lcu1OVER2WHFadeout_RR == 0)&&(lcu1US3WHControlFlag == 0)) 
      #else  
	if(SLIP_CONTROL_NEED_RR==1)
	  #endif
	{
		if((esc_pulse_up_rise_scan_rr==0)&&(esp_control_mode_rr<3))
		{
			esc_pulse_up_rise_scan_rr++;		

			lcs16EscInitialPulseupDutyrr = lcs16EscInitialPulseupDutyrr + comp_duty4PU_rr;

	    pulseup_cycle_scan_time_rr = S16_ESC_PULSE_UP_CYCLE_SCAN_R;

			ESC_PULSE_UP_RISE_RR = 1;

		}	
		else if((esc_pulse_up_rise_scan_rr > 0)&&(esp_control_mode_rr<3))
		{
			esc_pulse_up_rise_scan_rr++;
			
			ESC_PULSE_UP_RISE_RR = 1;
			
		/*   Duty addition each tuning scan time   */
			lcs16EscInitialPulseupDutyrr = temp_ini_duty_r 
												+ ((int16_t)(esc_pulse_up_rise_scan_rr/temp_duty_add_cycle_r)*temp_duty_add_duty_r)
												+ comp_duty4PU_rr;

		}
		else if(((esc_pulse_up_rise_scan_rr > 0)&&(esp_control_mode_rr<4)) || (RR.u8_Estimated_Active_Press >= 0))
		{
			ESC_PULSE_UP_RISE_RR = 1;
		}
		else
		{
			lcs16EscInitialPulseupDutyrr = temp_ini_duty_r;
			esc_pulse_up_rise_scan_rr = 0;
   		pulseup_cycle_scan_time_rr = S16_ESC_PULSE_UP_CYCLE_SCAN_R;
		}		
		
		/*   Duty limitation    */
			LCESP_vCalEscRiseRateLimitDuty(lcs16EscInitialPulseupDutyrr, 
			              S16_ESC_PULSE_UP_MIN_DUTY_R, S16_ESC_PULSE_UP_MAX_DUTY_R, 4);
			              
		/*  '11.04 GSUV Opel   */
					              
			lcs8EscCloseDuty_RR = lcs16EscInitialPulseupDutyrr ;		
						              
	}
 	  #if (__TCMF_UNDERSTEER_CONTROL == ENABLE)
	else if (((SLIP_CONTROL_NEED_RR == 1)||(SLIP_CONTROL_NEED_RR_old == 1)) && ((lcescu1TCMFUnderControlRr == 1)||(lcu1OVER2WHControlFlag_RR == 1)||(lcu1OVER2WHFadeout_RR == 1)||(lcu1US3WHControlFlag == 1)))
	{
	    pulseup_cycle_scan_time_rr = S16_ESC_PULSE_UP_CYCLE_SCAN_R;
	    esc_pulse_up_rise_scan_rr = 0;
	    lcs16EscInitialPulseupDutyrr = S16_ESC_PULSE_UP_CYCLE_SCAN_R * 10; /*10msec*/
	    ESC_PULSE_UP_RISE_RR = 0;
	}
	  #endif
	else
	{
 		pulseup_cycle_scan_time_rr = S16_ESC_PULSE_UP_CYCLE_SCAN_R;
 		esc_pulse_up_rise_scan_rr = 0;

		lcs16EscInitialPulseupDutyrr = temp_ini_duty_r;
		ESC_PULSE_UP_RISE_RR = 0;		
	}
	
	if((ESC_PULSE_UP_RISE_RL == 1) || (ESC_PULSE_UP_RISE_RR == 1))	
	{
		ESC_PULSE_UP_RISE_REAR = 1;
	}
	else
	{
		ESC_PULSE_UP_RISE_REAR = 0;
	}

}

void LCESP_vCalEscRiseRateLimitDuty(int16_t current_duty, int16_t min_duty_const, int16_t max_duty_const, int8_t whl_slt)
{
	
#if	( __Ext_Under_Ctrl == 1 )	

  if( EXTREME_UNDER_CONTROL ==1 )
  {	
		esp_tempW0 = S16_EXT_UND_PULSE_UP_CYCLE_SCAN_F ;
	}
	else
	{
		esp_tempW0 = S16_ESC_PULSE_UP_CYCLE_SCAN_F ;
	}
	
#else
	
		esp_tempW0 = S16_ESC_PULSE_UP_CYCLE_SCAN_F ;
  
#endif	
	
	if(whl_slt <= 2) 
	{
		tempB3 = esp_tempW0;

    #if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    	if((lsespu1AHBGEN3MpresBrkOn == 1)
    #else
    	if((MPRESS_BRAKE_ON == 1)
    #endif
      
    #if __TSP
             || (TSP_ON == 1)
    #endif
             
    #if __HDC
    	       ||(lcu1HdcActiveFlg == 1)
    #endif
        	       
    #if __ACC
            || (lcu1AccActiveFlg == 1)
    #endif
                
    #if ( __Ext_Under_Ctrl == 1 )	
            || ( EXTREME_UNDER_CONTROL ==1 )          
    #endif
          )
    	{ 
    			tempB0 = S16_ESC_PULSE_UP_MIN_DUTY_F; 
    	} 
  		else
  		{
  			  tempB0 = S16_ESC_PULSE_UP_MIN_DUTY_ONLY_F ;
  		}
	}
	else
	{
		tempB3 = S16_ESC_PULSE_UP_CYCLE_SCAN_R;
 
    tempB0 = S16_ESC_PULSE_UP_MIN_DUTY_R; 
	}
	
	if(min_duty_const < tempB0)
	{
		min_duty_const = tempB0;
	}
	else
	{
		;
	}	 		
	if(max_duty_const > (tempB3*10))
	{
		max_duty_const = (tempB3*10);
	}
	else
	{
		;
	}	 		

	if(current_duty < min_duty_const) 
	{
		current_duty = min_duty_const;
	}
	else 
	{
		;
	}	

	if(current_duty > max_duty_const)
	{
		current_duty = max_duty_const;
	}
	else
	{
		;
	}
		
	switch(whl_slt)
	{
		case 1:
			lcs16EscInitialPulseupDutyfl = current_duty;
			break;
		case 2:
			lcs16EscInitialPulseupDutyfr = current_duty;
			break;
		case 3:
			lcs16EscInitialPulseupDutyrl = current_duty;
			break;
		case 4:
			lcs16EscInitialPulseupDutyrr = current_duty;
			break;
		default :
			break;
	}

}

void LCESP_vCalEscRiseRateDuty(void)
{

#if	( __Ext_Under_Ctrl == 1 )	

  if( EXTREME_UNDER_CONTROL ==1 )
  {	
		esp_tempW0 = S16_EXT_UND_PULSE_UP_CYCLE_SCAN_F ;
	}
	else
	{
		esp_tempW0 = S16_ESC_PULSE_UP_CYCLE_SCAN_F ;
	}
	
#else
	
		esp_tempW0 = S16_ESC_PULSE_UP_CYCLE_SCAN_F ;
  
#endif	       
       
    
    if (esp_tempW0 < 1)
    {
        esp_tempW0 = 1;
    }
    else
    {
        ;
    }
        	       
       
#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
	if((lsespu1AHBGEN3MpresBrkOn == 1)
#else
	if((MPRESS_BRAKE_ON == 1)
#endif
  
#if __TSP
         || (TSP_ON == 1)
#elif __HDC
	       ||(lcu1HdcActiveFlg == 1)
#elif __ACC
        || (lcu1AccActiveFlg == 1)
#endif
      )
	{
        if((ABS_fz == 1) && (PARTIAL_BRAKE == 0))
        {
			temp_ini_duty_f = S16_ESC_CAL_PULSE_UP_INI_DUTY_FRONT_ABS;
			temp_duty_add_cycle_f = ((int16_t)(S16_ESC_PULSE_UP_ADD_CYCLE_FRONT_ABS/esp_tempW0))*esp_tempW0;
			temp_duty_add_duty_f = S16_ESC_PULSE_UP_ADD_DUTY_FRONT_ABS;

			temp_ini_duty_r = S16_ESC_CAL_PULSE_UP_INI_DUTY_REAR_ABS;
			temp_duty_add_cycle_r = ((int16_t)(S16_ESC_PULSE_UP_ADD_CYCLE_REAR_ABS/S16_ESC_PULSE_UP_CYCLE_SCAN_R))*S16_ESC_PULSE_UP_CYCLE_SCAN_R;
			temp_duty_add_duty_r = S16_ESC_PULSE_UP_ADD_DUTY_REAR_ABS;
		}
        else
        {
			temp_ini_duty_f = S16_ESC_CAL_PULSE_UP_INI_DUTY_FRONT_CBS;
			temp_duty_add_cycle_f = ((int16_t)(S16_ESC_PULSE_UP_ADD_CYCLE_FRONT_CBS/esp_tempW0))*esp_tempW0;
			temp_duty_add_duty_f = S16_ESC_PULSE_UP_ADD_DUTY_FRONT_CBS;

			temp_ini_duty_r = S16_ESC_CAL_PULSE_UP_INI_DUTY_REAR_CBS;
			temp_duty_add_cycle_r = ((int16_t)(S16_ESC_PULSE_UP_ADD_CYCLE_REAR_CBS/S16_ESC_PULSE_UP_CYCLE_SCAN_R))*S16_ESC_PULSE_UP_CYCLE_SCAN_R;
			temp_duty_add_duty_r = S16_ESC_PULSE_UP_ADD_DUTY_REAR_CBS;
        }
		pu_full_T_fl = 0;				/*  FL  */
		pu_full_T_fr = 0;				/*  FR  */
    }
	else
	{
	    		
		temp_ini_duty_f = LCESP_s16FindRoadDependatGain( esp_mu, S16_ESC_CAL_PULSE_UP_INI_DUTY_FRONT_H,
                            S16_ESC_CAL_PULSE_UP_INI_DUTY_FRONT_M, S16_ESC_CAL_PULSE_UP_INI_DUTY_FRONT_L);
		 
		temp_duty_add_cycle_f = ((int16_t)(S16_ESC_PULSE_UP_ADD_CYCLE_FRONT/esp_tempW0))*esp_tempW0;
		temp_duty_add_duty_f = S16_ESC_PULSE_UP_ADD_DUTY_FRONT;

		temp_ini_duty_r = LCESP_s16FindRoadDependatGain( esp_mu, S16_ESC_CAL_PULSE_UP_INI_DUTY_REAR_H,
  	                				S16_ESC_CAL_PULSE_UP_INI_DUTY_REAR_M, S16_ESC_CAL_PULSE_UP_INI_DUTY_REAR_L);
  	                				
 
 		/*  '11 04 GSUV Opel */
 		    
        if((lcs8EscCloseDutyStatusRL!=2)&&(ESP_PULSE_DUMP_RL==0)&&(SLIP_CONTROL_NEED_RL==1))
        { 
        	lcs8EscCloseDutyStatusRL = 1  ;   
        }   
        else if (((ESP_PULSE_DUMP_RL==1)&&(SLIP_CONTROL_NEED_RL==0))||((RL.u8_Estimated_Active_Press)>3))
  	    {
  	    	lcs8EscCloseDutyStatusRL = 2  ;   
  	    }
        else if ((lcs8EscCloseDutyStatusRL==2)&&(ESP_PULSE_DUMP_RL==0)&&(SLIP_CONTROL_NEED_RL==1))
  	    {
  	    	lcs8EscCloseDutyStatusRL = 2  ;   
  	    }
  	    else
		{
  		lcs8EscCloseDutyStatusRL = 0  ;   
		}

        if((lcs8EscCloseDutyStatusRR!=2)&&(ESP_PULSE_DUMP_RR==0)&&(SLIP_CONTROL_NEED_RR==1))
        { 
        	lcs8EscCloseDutyStatusRR = 1  ;   
        }   
        else if (((ESP_PULSE_DUMP_RR==1)&&(SLIP_CONTROL_NEED_RR==0))||((RR.u8_Estimated_Active_Press)>3))
  	    {
  	    	lcs8EscCloseDutyStatusRR = 2  ;   
  	    }
        else if ((lcs8EscCloseDutyStatusRR==2)&&(ESP_PULSE_DUMP_RR==0)&&(SLIP_CONTROL_NEED_RR==1))
  	    {
  	    	lcs8EscCloseDutyStatusRR = 2  ;   
  	    }
  	    else
		{
  		    lcs8EscCloseDutyStatusRR = 0  ;   
		}
 		   
        if((lcs8EscCloseDutyStatusRR==2)||(lcs8EscCloseDutyStatusRL==2))
        { 
        	temp_ini_duty_r = S16_ESC_PULSE_UP_MIN_DUTY_R  ;  
        }   
        else
  	    {
  	    	;
  	    }
  	 		  
 		  
	    #if (__CRAM == 1)
        if(ldu1espDetectCRAM==1)
        { 
        	temp_ini_duty_r = S16_CRAM_PULSE_UP_INI_DUTY_REAR ;  
        }   
        else
  	    {
  	    	;
  	    }
  #endif
  
  #if (__Pro_Active_Roll_Ctrl == 1)  
    #if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
    if((ROP_REQ_ACT == 1)&&((RL_ESC.lcu1ropRearCtrlReq==1)||(RR_ESC.lcu1ropRearCtrlReq==1)))
    #else
    if((ROP_REQ_ACT == 1)&&((lcu1ropRearCtrlReqRL==1)||(lcu1ropRearCtrlReqRR==1)))
    #endif
    { 
    	temp_ini_duty_r = S16_ROP_PULSE_UP_INI_DUTY_REAR ;  
    }             
    else
  	{
  		;
  	}    
  #endif    
  
	temp_duty_add_cycle_r = ((int16_t)(S16_ESC_PULSE_UP_ADD_CYCLE_REAR/S16_ESC_PULSE_UP_CYCLE_SCAN_R))*S16_ESC_PULSE_UP_CYCLE_SCAN_R;
	temp_duty_add_duty_r = S16_ESC_PULSE_UP_ADD_DUTY_REAR;
	
	if(SLIP_CONTROL_NEED_FL == 1) 
	{

      if(esp_control_mode_fl < 3)   /* Rise-mode */
      {        
        esp_need_count_rise_fl = esp_need_count_rise_fl + 1 ;  
        
    			if( esp_need_count_rise_fl > 1000 )
    			{
    				esp_need_count_rise_fl = 1000 ; 
    			}  
      }  
      

			if( esp_need_count_rise_fl <= variable_rise_scan )
			{
				pu_full_T_fl = variable_rise_scan ; 
			}
			else
			{
				pu_full_T_fl = 0 ; 
			}     
		}
		else
		{
			pu_full_T_fl = 0;
      		esp_need_count_rise_fl = 0; 
					
		}

		if(SLIP_CONTROL_NEED_FR == 1) 
		{

      if(esp_control_mode_fr < 3)   /* Rise-mode */
      {        
        esp_need_count_rise_fr = esp_need_count_rise_fr + 1 ;  
        
    			if( esp_need_count_rise_fr > 1000 )
    			{
    				esp_need_count_rise_fr = 1000 ; 
    			}  
      }  
      

			if( esp_need_count_rise_fr <= variable_rise_scan )
			{
				pu_full_T_fr = variable_rise_scan ;
						
			}
			else
			{
				pu_full_T_fr = 0 ; 
			}


		}
		else
		{
			pu_full_T_fr = 0;
			esp_need_count_rise_fr = 0 ;
					
		}

	}
	if (temp_duty_add_cycle_f < esp_tempW0)
	{
	    temp_duty_add_cycle_f = esp_tempW0;
	}
	else
	{
	    ;
	}
	if (temp_duty_add_cycle_r < S16_ESC_PULSE_UP_CYCLE_SCAN_R)
	{
	    temp_duty_add_cycle_r = S16_ESC_PULSE_UP_CYCLE_SCAN_R;
	}
	else
	{
	    ;
	}
	
}
#endif

void LCESP_vControlYaw(void)
{
    LDESP_vCalYawError();
    LCESP_vCalYawGain();
    LCESP_vCalDeltaMoment(); 
}

void LCESP_vCoordinateControl(void)
{
/********************************************************************/
/*  2004.06.17  suspect-> ESP off    KIM.YONG.KIL*/
    YAW_CDC_WORK_OLD=YAW_CDC_WORK;
/********************************************************************/

    LCESP_vCallControlStatus();
#if (__CRAM == 1) && (__CRAM_PRE_FRONT_W_CRTL == 1) 
	LDESP_vCheckPreCRAM();
#endif

    LCESP_vCalCntrStatusESC();

	#if (__ESC_CYCLE_DETECTION)
    LDESP_vDetESCCycle();
  #endif    

#if (__CRAM == 1)
  LCCRAM_vCallControl();
#endif

    LCESP_vRememberCurrentSlip();

	#if MGH60_NEW_TARGET_SLIP  ==1
	  LCESP_vCalDelTargetSlipMgh60(); 
    #endif 
    
    LCESP_vCalTargetPressPara();
 
    LCESP_vMonitoringESC();

 #if __PARTIAL_PERFORMANCE    /* --> Signal_conditioning.c / ESP_Sensor_Signal_Processing();*/
    LDESPPartial_vVariableSet();
    LDESPPartial_vDetectFault();
 #endif

#if ESC_PULSE_UP_RISE
	LCESP_vCalDeltaPressure4PUR();
#endif

    #if defined(__ADDON_TCMF)
    LDESP_vSetActiveBooster();
    #endif

    LCESP_vAddTargetSlip();
    LDESP_vDet1stCycle();
    
#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
    LDESP_vDet1stCycleWheelF(&FL_ESC);
    LDESP_vDet1stCycleWheelF(&FR_ESC);
    LDESP_vDet1stCycleWheelR(&RL_ESC);
    LDESP_vDet1stCycleWheelR(&RR_ESC);
#else
    LDESP_vDet1stCycleWheel();
#endif  
    
    LCESP_vInterface2WheelOverCtrl();
    
    LCESP_vInterfaceSlipController();
  
    FLAG_ESP_CONTROL_ACTION = ((YAW_CDC_WORK==1)||(ESP_PULSE_DUMP_FLAG==1)||(SLIP_CONTROL_NEED_FL_old==1)
                              ||(SLIP_CONTROL_NEED_FR_old==1)||(SLIP_CONTROL_NEED_RL_old==1)
                              ||(SLIP_CONTROL_NEED_RR_old==1)
                              ||(EPC_ON==1)||(PBA_ON==1)
                            #if __FBC
                              ||(FBC_ON==1)
                            #endif
                            #if __TSP
                              ||(TSP_ON==1)
                            #endif
                              ||(BTCS_ON==1)||(ETCS_ON==1));

}

void LCESP_vInterface2WheelOverCtrl(void)
{ 	
	  lcu1OVER2WHControlOld = lcu1OVER2WHControlFlag ;
	
	lcu1OVER2WHControlOld_RL = lcu1OVER2WHControlFlag_RL;
	lcu1OVER2WHControlOld_RR = lcu1OVER2WHControlFlag_RR;
	
	  lcu1OVER2WHFadeoutOld_RL = lcu1OVER2WHFadeout_RL;
    lcu1OVER2WHFadeoutOld_RR = lcu1OVER2WHFadeout_RR;
	
	if((FLAG_TWO_WHEEL_RL == 1)&&(SLIP_CONTROL_NEED_FL==1)&&(ABS_fz==0))
	{
		  lcu1OVER2WHControlFlag_RL = 1;
	}
	else if((FLAG_TWO_WHEEL_RR == 1)&&(SLIP_CONTROL_NEED_FR==1)&&(ABS_fz==0))
	{
		  lcu1OVER2WHControlFlag_RR = 1;
	}
	else
	{
		  lcu1OVER2WHControlFlag_RL = 0;
		  lcu1OVER2WHControlFlag_RR = 0;
	} 
	
/* Inhibition */	

    /*
    if((VDC_DISABLE_FLG==1)||(MPRESS_BRAKE_ON==1)||(EXTREME_UNDER_CONTROL==1))
    {
        ldu1OVER2WHInhibitFlag = 1;
    }
    else
    {
        ldu1OVER2WHInhibitFlag = 0;
    }        	
	*/
                            
    
    if ( (ldu1OVER2WHInhibitFlag == 0)&&((lcu1OVER2WHControlFlag_RL == 1)||(lcu1OVER2WHControlFlag_RR == 1)) )
    {
        lcu1OVER2WHControlFlag = 1;        
        
        if (lcs16OVER2WH_count < L_U16_TIME_10MSLOOP_10S)
        {
        	  lcs16OVER2WH_count = lcs16OVER2WH_count + 1;
        }
        else
        {
        	  lcs16OVER2WH_count = L_U16_TIME_10MSLOOP_10S;
        }
         
    }
    else
    {
    		lcu1OVER2WHControlFlag = 0;
    		lcs16OVER2WH_count = 0;
        lcu1OVER2WHControlFlag_RL = 0 ;
        lcu1OVER2WHControlFlag_RR = 0 ;
    }  

/* Fade-out */

    esp_tempW5  = LCESP_s16FindRoadDependatGain( esp_mu, S16_OVER_2WH_FRT_FADE_OUT_HM,  S16_OVER_2WH_FRT_FADE_OUT_MM,  S16_OVER_2WH_FRT_FADE_OUT_LM ); /* Fade_out time */



	/* FADE OUT RL*/
	if((ldu1OVER2WHInhibitFlag==0)&&(BTCS_ON==0)&&(lcu1OVER2WHControlOld_RL==1)&&(lcu1OVER2WHControlFlag_RL==0)
        &&(lcu1OVER2WHFadeout_RL==0)&&((SLIP_CONTROL_BRAKE_FL==1)&&(lcs16OVER2WH_FadeCount<esp_tempW5)))  
    {
        if(SLIP_CONTROL_BRAKE_FR==1)
    	{
            lcu1OVER2WHFadeout_RL = 0;
    	}
    	else
    	{
            lcu1OVER2WHFadeout_RL = 1;
    	}
    }
    else if((ldu1OVER2WHInhibitFlag==0)&&(BTCS_ON==0)&&(lcu1OVER2WHControlFlag_RL==0)
        &&(lcu1OVER2WHFadeout_RL==1)&&((SLIP_CONTROL_BRAKE_FL==1)&&(lcs16OVER2WH_FadeCount<esp_tempW5)))  
	{
		if(SLIP_CONTROL_BRAKE_FR==1)
    	{
            lcu1OVER2WHFadeout_RL = 0;
    	}
    	else
    	{
            lcu1OVER2WHFadeout_RL = 1;
    	}
	}
    else
    {
        lcu1OVER2WHFadeout_RL = 0 ;
    }
	
	/* FADE OUT RR*/
	if((ldu1OVER2WHInhibitFlag==0)&&(BTCS_ON==0)&&(lcu1OVER2WHControlOld_RR==1)&&(lcu1OVER2WHControlFlag_RR==0)
        &&(lcu1OVER2WHFadeout_RR==0)&&((SLIP_CONTROL_BRAKE_FR==1)&&(lcs16OVER2WH_FadeCount<esp_tempW5)))  
    {
    	if(SLIP_CONTROL_BRAKE_FL==1)
    	{
            lcu1OVER2WHFadeout_RR = 0;
    	}
    	else
    	{
            lcu1OVER2WHFadeout_RR = 1;
    	}
    }
    else if((ldu1OVER2WHInhibitFlag==0)&&(BTCS_ON==0)&&(lcu1OVER2WHControlFlag_RR==0)
        &&(lcu1OVER2WHFadeout_RR==1)&&((SLIP_CONTROL_BRAKE_FR==1)&&(lcs16OVER2WH_FadeCount<esp_tempW5)))  
	{
		if(SLIP_CONTROL_BRAKE_FL==1)
		{
            lcu1OVER2WHFadeout_RR = 0;
		}
		else
		{
            lcu1OVER2WHFadeout_RR = 1;
		}
	}
    else
    {
        lcu1OVER2WHFadeout_RR = 0 ;         
    }
	
	if((lcu1OVER2WHFadeout_RL ==1)||(lcu1OVER2WHFadeout_RR ==1))
	{
		lcs16OVER2WH_FadeCount = lcs16OVER2WH_FadeCount + 1 ;

        if(lcs16OVER2WH_FadeCount>L_U16_TIME_10MSLOOP_10S)
        {  
        	lcs16OVER2WH_FadeCount = L_U16_TIME_10MSLOOP_10S ;
        }
        else
        {
        	;
        }
	}
	else
	{
		lcs16OVER2WH_FadeCount = 0;
	}
	/*
    if((ldu1OVER2WHInhibitFlag==0)&&(BTCS_ON==0)&&(lcu1OVER2WHControlOld==1)&&(lcu1OVER2WHControlFlag==0)
        &&(lcu1OVER2WHFadeout_RL==0)&&(lcu1OVER2WHFadeout_RR==0)
        &&(((SLIP_CONTROL_BRAKE_FL==1)||(SLIP_CONTROL_BRAKE_FR==1))&&(lcs16OVER2WH_FadeCount<esp_tempW5)))  
    {          
        if((SLIP_CONTROL_BRAKE_FL==1)&&(SLIP_CONTROL_BRAKE_FR==0))
        {
        	  lcu1OVER2WHFadeout_RL = 1 ;
            lcu1OVER2WHFadeout_RR = 0 ;        
        }
        else if((SLIP_CONTROL_BRAKE_FL==0)&&(SLIP_CONTROL_BRAKE_FR==1))
        {
        	  lcu1OVER2WHFadeout_RL = 0 ;
            lcu1OVER2WHFadeout_RR = 1 ;
        }
        else if((SLIP_CONTROL_BRAKE_FL==1)&&(SLIP_CONTROL_BRAKE_FR==1))
        {
        	  lcu1OVER2WHFadeout_RL = 0 ;
            lcu1OVER2WHFadeout_RR = 0 ;
        }
        else
        {
        	  ;
        }
        
        lcs16OVER2WH_FadeCount = lcs16OVER2WH_FadeCount + 1 ;                 
                        
    }
    else if((ldu1OVER2WHInhibitFlag==0)&&(BTCS_ON==0)&&(lcu1OVER2WHControlFlag==0)
        &&((lcu1OVER2WHFadeout_RL==1)||(lcu1OVER2WHFadeout_RR==1))
        &&(((SLIP_CONTROL_BRAKE_FL==1)||(SLIP_CONTROL_BRAKE_FR==1))&&(lcs16OVER2WH_FadeCount<esp_tempW5)))  
	  {        
        if((SLIP_CONTROL_BRAKE_FL==1)&&(SLIP_CONTROL_BRAKE_FR==0))
        {
        	  lcu1OVER2WHFadeout_RL = 1 ;
            lcu1OVER2WHFadeout_RR = 0 ;        
        }
        else if((SLIP_CONTROL_BRAKE_FL==0)&&(SLIP_CONTROL_BRAKE_FR==1))
        {
        	  lcu1OVER2WHFadeout_RL = 0 ;
            lcu1OVER2WHFadeout_RR = 1 ;
        }
        else if((SLIP_CONTROL_BRAKE_FL==1)&&(SLIP_CONTROL_BRAKE_FR==1))
        {
        	  lcu1OVER2WHFadeout_RL = 0 ;
            lcu1OVER2WHFadeout_RR = 0 ;
        }
        else
        {
        	  ;
        }
        
        lcs16OVER2WH_FadeCount = lcs16OVER2WH_FadeCount + 1 ;
        
        if (lcs16OVER2WH_FadeCount>L_U16_TIME_10MSLOOP_10S)
        {  
        	  lcs16OVER2WH_FadeCount = L_U16_TIME_10MSLOOP_10S ;
        }   
	  }
    else
    {
    	    lcs16OVER2WH_FadeCount = 0;
          lcu1OVER2WHFadeout_RL = 0 ;
          lcu1OVER2WHFadeout_RR = 0 ;       
    }
    */
    
    if(((lcu1OVER2WHControlFlag_RL == 1)&&(del_target_slip_rl ==0))
    	||((lcu1OVER2WHControlFlag_RR == 1)&&(del_target_slip_rr ==0)))
    {
    	  lcesps16OVER2WH_CompCount = lcesps16OVER2WH_CompCount + 1;

        if (lcesps16OVER2WH_CompCount>L_U8_TIME_10MSLOOP_500MS)
        {  
            lcesps16OVER2WH_CompCount = L_U8_TIME_10MSLOOP_500MS ;
        }
    	else
    	{
        	;
        }
    }  
    else
        {  
            lcesps16OVER2WH_CompCount = 0 ;
    }
    
    if(((lcu1OVER2WHFadeoutOld_RL ==1)||(lcu1OVER2WHFadeoutOld_RR ==1))&&(lcu1OVER2WHControlFlag ==1))
    {
    	  lcs16OVER2WH_count = S16_OVER_2WH_USC_INITIAL_FULL_TIME + 1;
    }
    else
    {
    	  ;
    }

}

void LCESP_vCallControlStatus(void)
{
    Flag_UNDER_CONTROL = 0 ;

    if((Flag_UNDER_CONTROL==1) && (ROP_REQ_ACT==0))
    {

        moment_q_front = 0;
        moment_q_rear = 0;
        moment_q_front_old = 0;
        moment_q_rear_old = 0;
        ACT_TWO_WHEEL = 0;
        ACT_UNDER_YAW_2WHEEL = 0;
        YAW_CDC_WORK_O = 0;
        
#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
        RL_ESC.REAR_PRESSURE_HOLD = 0;
        RR_ESC.REAR_PRESSURE_HOLD = 0;        
#else
        REAR_PRESSURE_HOLD = 0;
#endif        

    }
    else
    {

        moment_q_front_under = 0;
        moment_q_rear_under = 0;
        moment_q_rear_under_old = 0;
        ACT_UNDER_2WHEEL = 0;
        YAW_CDC_WORK_U = 0;

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)

#else
        FRONT_PRESSURE_HOLD = 0;
#endif

    }
}


void LDESP_vCalYawError(void)
{ 
    LDESP_vCompDeltaYawDot( );
    LDESP_vCompDeltaYaw2Dot( );
    LDESP_vCompDeltaYawFirstDot( );
    LDESP_vCompYawOutDot( );
    LDESP_vCompYawOutDot2( );

    if ((YAW_CHANGE_LIMIT_ACT==1) && (k_mu==0) )
    {
        Cntr_Yaw_Lmt_Act = Cntr_Yaw_Lmt_Act + 1;

        if ( Cntr_Yaw_Lmt_Act > (L_U8_TIME_10MSLOOP_100MS + 7))
        {
            Cntr_Yaw_Lmt_Act = (L_U8_TIME_10MSLOOP_100MS + 7);
        }
        else
        {
            ;
        }

        if ( Cntr_Yaw_Lmt_Act <= L_U8_TIME_10MSLOOP_100MS )
        {
            if( McrAbs(yaw_error2_dot-yaw_out_dot2) > YAW_0G1DEG )
            {
                yaw_error_dot  = yaw_out_dot;
                yaw_error2_dot = yaw_out_dot2;
            }
            else
            {
                ;
            }
        }
        else
        {
            ;
        }
    }
    else
    {
        Cntr_Yaw_Lmt_Act = 0 ;
    }

    yaw_error=delta_yaw;
    yaw_measurement=yaw_out;

}
void LCESP_vCalSpeedDepGain( void )
{

    yaw_control_P_gain_front_limit = (int16_t)( (((int32_t)yaw_control_P_gain_front)*10)/100 ); // 30->10 2011.06.03 by shinae

    yaw_control_D_gain_front_limit = (int16_t)( (((int32_t)yaw_control_D_gain_front)*10)/100 );  // 30->10  2011.06.03 by shinae

        LCESP_vFindSpeedDependantGain();

		esp_tempW1 = (int16_t)( (((int32_t)yaw_control_P_gain_front)*gain_speed_depend)/100 );
		
		esp_tempW2 = (int16_t)( (((int32_t)yaw_control_D_gain_front)*gain_speed_depend)/100 );

/*  '06.04.17 : D-gain weighting   */

    if(ABS_fz==1)
    {
      esp_tempW9 = LCESP_s16IInter4Point(vrefk, S16_D_GAIN_V_LOW_SPD , S16_ABS_V_LOW_D_WEG,
                                                S16_D_GAIN_LOW_SPD ,   S16_ABS_LOW_D_WEG,
                                                S16_D_GAIN_MED_SPD,    S16_ABS_MED_D_WEG,
                                                S16_D_GAIN_HIGH_SPD,   S16_ABS_HIGH_D_WEG );
    }
    else
    {
      esp_tempW5 = LCESP_s16FindRoadDependatGain( esp_mu, S16_H_MU_V_LOW_D_WEG,
                                S16_M_MU_V_LOW_D_WEG, S16_L_MU_V_LOW_D_WEG);
      esp_tempW6 = LCESP_s16FindRoadDependatGain( esp_mu, S16_H_MU_LOW_D_WEG,
                                S16_M_MU_LOW_D_WEG, S16_L_MU_LOW_D_WEG);
      esp_tempW7 = LCESP_s16FindRoadDependatGain( esp_mu, S16_H_MU_MED_D_WEG,
                                S16_M_MU_MED_D_WEG, S16_L_MU_MED_D_WEG);
      esp_tempW8 = LCESP_s16FindRoadDependatGain( esp_mu, S16_H_MU_HIGH_D_WEG,
                                S16_M_MU_HIGH_D_WEG, S16_L_MU_HIGH_D_WEG);

      esp_tempW9 = LCESP_s16IInter4Point(vrefk, S16_D_GAIN_V_LOW_SPD , esp_tempW5,
                                                S16_D_GAIN_LOW_SPD ,   esp_tempW6,
                                                S16_D_GAIN_MED_SPD,    esp_tempW7,
                                                S16_D_GAIN_HIGH_SPD,   esp_tempW8 );
    }


    if ( yaw_error_dot > 0 )  /* Inc.*/
    {
        esp_tempW2 = (int16_t)( (((int32_t)esp_tempW2)*esp_tempW9)/100 );
    }
    else
    {
      ;
    }

    if ( McrAbs(esp_tempW1) > McrAbs(yaw_control_P_gain_front_limit) )
    {
            yaw_control_P_gain_front = esp_tempW1;
    }
    else
    {
            yaw_control_P_gain_front = yaw_control_P_gain_front_limit;
    }

    if ( McrAbs(esp_tempW2) > McrAbs(yaw_control_D_gain_front_limit) )
    {
            yaw_control_D_gain_front = esp_tempW2;
    }
    else
    {
            yaw_control_D_gain_front = yaw_control_D_gain_front_limit;
    }

} 

void LCESP_vCalYawGain(void)
{                  

    if(yaw_error>0)
        {
        LCESP_vCalOverSteerYawGain();           /*OVER Control*/															
        }
        else
        {
        LCESP_vCalUnderSteerYawGain();			/* UNDER Control*/												
        }

    LCESP_vCalSpeedDepGain();

    	LCESP_vCalSpeedDepGainRear();
#if (__ESC_COMPETITIVE_CONTROL == 1)

   	if(ldespu1CompetitiveModeESC==1)
        {
  		yaw_control_P_gain_front = (int16_t)((((int32_t)yaw_control_P_gain_front)*U8_COMP_MODE_YAW_P_GAIN_WEG_F)/100);
  		yaw_control_D_gain_front = (int16_t)((((int32_t)yaw_control_D_gain_front)*U8_COMP_MODE_YAW_D_GAIN_WEG_F)/100);

  		yaw_control_P_gain_rear = (int16_t)((((int32_t)yaw_control_P_gain_rear)*U8_COMP_MODE_YAW_P_GAIN_WEG_R)/100);
  		yaw_control_D_gain_rear = (int16_t)((((int32_t)yaw_control_D_gain_rear)*U8_COMP_MODE_YAW_D_GAIN_WEG_R)/100);
        }
        else
        {
            ;
        }

#endif

/*   Trailer    */
 	#if __TSP	

      if((ldtspu8TrlrHtchSwAtv==1)&&(ldtspu1TrlrHtchSwAtvValid==0))
                {
 
        esp_tempW7 = LCESP_s16IInter2Point( vref5, VREF_100_KPH, 90,
                                                   VREF_120_KPH, 90 );

        yaw_control_P_gain_front =  (int16_t)((((int32_t)yaw_control_P_gain_front)*esp_tempW7)/100 );
        yaw_control_D_gain_front =  (int16_t)((((int32_t)yaw_control_D_gain_front)*esp_tempW7)/100 ); 
        
                }
                else
                {
                    ;
                }
               		  #endif

/*********************** yaw gain weighting for mini-tire *********************9604 */
                
    if((RTA_FL.ldabsu1DetSpareTire==1)||(RTA_FR.ldabsu1DetSpareTire==1)||(RTA_RL.ldabsu1DetSpareTire==1)||(RTA_RR.ldabsu1DetSpareTire==1))
                {
        esp_tempW1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_WEG_RTA_FRONT_20KPH_H_MU,   /*front, low-speed*/  
                                                            S16_WEG_RTA_FRONT_20KPH_M_MU, 
                                                            S16_WEG_RTA_FRONT_20KPH_L_MU ) ;   
                                                                        
        esp_tempW2 = LCESP_s16FindRoadDependatGain( esp_mu, S16_WEG_RTA_FRONT_80KPH_H_MU,   /*front, high-speed*/
                                                            S16_WEG_RTA_FRONT_80KPH_M_MU, 
                                                            S16_WEG_RTA_FRONT_80KPH_L_MU ) ;

	  	esp_tempW7 = LCESP_s16IInter2Point(vref5,VREF_20_KPH,esp_tempW1,
												 VREF_80_KPH,esp_tempW2);																                                        

        esp_tempW5 = LCESP_s16FindRoadDependatGain( esp_mu, S16_WEG_RTA_REAR_20KPH_H_MU,   /*rear, low-speed*/
                                                            S16_WEG_RTA_REAR_20KPH_M_MU, 
                                                            S16_WEG_RTA_REAR_20KPH_L_MU ) ;   
                                                                        
        esp_tempW6 = LCESP_s16FindRoadDependatGain( esp_mu, S16_WEG_RTA_REAR_80KPH_H_MU,   /*rear, high-speed*/
                                                            S16_WEG_RTA_REAR_80KPH_M_MU, 
                                                            S16_WEG_RTA_REAR_80KPH_L_MU ) ;

		esp_tempW8 = LCESP_s16IInter2Point(vref5,VREF_20_KPH,esp_tempW5,
												 VREF_80_KPH,esp_tempW6);

        yaw_control_P_gain_front =  (int16_t)((((int32_t)yaw_control_P_gain_front)*esp_tempW7)/100 );
        yaw_control_D_gain_front =  (int16_t)((((int32_t)yaw_control_D_gain_front)*esp_tempW7)/100 );

        yaw_control_P_gain_rear =  (int16_t)((((int32_t)yaw_control_P_gain_rear)*esp_tempW8)/100 );
        yaw_control_D_gain_rear =  (int16_t)((((int32_t)yaw_control_D_gain_rear)*esp_tempW8)/100 );
                }
                else
                {
                    ;
                }
                
 #if (__MGH60_ESC_IMPROVE_CONCEPT==1) 
    if	( FLAG_BANK_DETECTED==1 )	    /* '11. NZ */   
 #else
    if	( (FLAG_BANK_DETECTED==1) || (FLAG_BANK_SUSPECT==1) )	 /* '10.04.26 : GM_CPG Improvement */ 
 #endif
                {
	    yaw_control_P_gain_front =  (int16_t)((((int32_t)yaw_control_P_gain_front)*S16_BANK_YAW_P_GAIN_WEG)/100 );   
        yaw_control_D_gain_front =  (int16_t)((((int32_t)yaw_control_D_gain_front)*S16_BANK_YAW_D_GAIN_WEG)/100 );    
                }
                else
                {
                    ;
                }
      
  #if (__MGH60_ESC_IMPROVE_CONCEPT==1)
        det_Beta_MD_old = det_Beta_MD ; 
            	
	if ( det_Beta_MD <= McrAbs(Beta_MD) )
                {
		det_Beta_MD = LCESP_s16Lpf1Int(McrAbs(Beta_MD), det_Beta_MD_old, 120);
                }
                else
                {
		det_Beta_MD = LCESP_s16Lpf1Int(McrAbs(Beta_MD), det_Beta_MD_old, 1);      
                }
                
    	esp_tempW2 = LCESP_s16IInter3Point( vrefk, S16_YC_PERMIT_BETA_LOW_SPD,  S16_YC_PERMIT_BETA_L_Spd_Lmu,
                                    	           S16_YC_PERMIT_BETA_MED_SPD,  S16_YC_PERMIT_BETA_M_Spd_Lmu,
                                   	             S16_YC_PERMIT_BETA_HIGH_SPD, S16_YC_PERMIT_BETA_H_Spd_Lmu );   	/* Low-Mu */

     	esp_tempW3 = LCESP_s16IInter3Point( vrefk, S16_YC_PERMIT_BETA_LOW_SPD,  S16_YC_PERMIT_BETA_L_Spd_Mmu,
                                    	           S16_YC_PERMIT_BETA_MED_SPD,  S16_YC_PERMIT_BETA_M_Spd_Mmu,
                                   	             S16_YC_PERMIT_BETA_HIGH_SPD, S16_YC_PERMIT_BETA_H_Spd_Mmu );   	/* Med-Mu */
            
    	esp_tempW1 = LCESP_s16IInter2Point( esp_mu, lsesps16MuLow,  esp_tempW2,
                                    	            lsesps16MuMed,  esp_tempW3  );   	                        /* 11.SWD OPEL*/                                   	             

    	esp_tempW5 = LCESP_s16IInter2Point( esp_mu, lsesps16MuLow,  S16_YC_PERMIT_BETA_DIFF_Lmu,
                                    	            lsesps16MuMed,  S16_YC_PERMIT_BETA_DIFF_Mmu  );   	        /* 11.SWD OPEL*/                                   	             
  
    	esp_tempW6 = LCESP_s16IInter2Point( esp_mu, lsesps16MuLow,  (int16_t)S16_YC_PERMIT_BETA_MIN_GAIN_WEG_Lmu,
                                    	            lsesps16MuMed,  (int16_t)S16_YC_PERMIT_BETA_MIN_GAIN_WEG_Mmu  );   	/* 11.SWD OPEL*/                                   	             
   
				if ( det_Beta_MD <= 1 )
                {
					det_Beta_MD = 1 ;
                }
                else
                {
                    ;
                }
				
	      esp_tempW0 = LCESP_s16IInter2Point( det_Beta_MD, (esp_tempW1-esp_tempW5), esp_tempW6, esp_tempW1, 100);   
  
	      esp_tempW0 = LCESP_s16IInter2Point( esp_mu, lsesps16MuMedHigh, esp_tempW0, (lsesps16MuMedHigh+100), 100);  /* 11 NZ */
	      
			if(MPRESS_BRAKE_ON == 0)
                {
 				yaw_control_P_gain_front =  (int16_t)((((int32_t)yaw_control_P_gain_front)*esp_tempW0)/100 );   
      	yaw_control_D_gain_front =  (int16_t)((((int32_t)yaw_control_D_gain_front)*esp_tempW0)/100 );    
             }
             else
             {
				;
             }

			      if( yaw_out > 0 ) 
            {
              esp_tempW2 = LCESP_s16IInter3Point( ay_vx_dot_sig_plus, 0, (int16_t)S16_YC_AY_VX_DOT_P_WEG, 
            	  											          ay_vx_dot_sig_1st_th, (int16_t)S16_YC_AY_VX_DOT_P_WEG_1ST,						                      
            			                					    ay_vx_dot_sig_2nd_th, (int16_t)S16_YC_AY_VX_DOT_P_WEG_2ND ) ;	
        
              esp_tempW3 = LCESP_s16IInter3Point( ay_vx_dot_sig_plus, 0, (int16_t)S16_YC_AY_VX_DOT_D_WEG, 
            	  											          ay_vx_dot_sig_1st_th, (int16_t)S16_YC_AY_VX_DOT_D_WEG_1ST,						                      
            			                					    ay_vx_dot_sig_2nd_th, (int16_t)S16_YC_AY_VX_DOT_D_WEG_2ND ) ;	
            			                					     
            	esp_tempW4 = LCESP_s16IInter3Point( ay_vx_dot_sig_plus, 0, (int16_t)S16_YC_AY_VX_DOT_P_WEG_Mmu,          		                			 
            	  								                ay_vx_dot_sig_1st_th_Mmu, (int16_t)S16_YC_AY_VX_DOT_P_WEG_1ST_Mmu,				                			 
            			                			        ay_vx_dot_sig_2nd_th_Mmu, (int16_t)S16_YC_AY_VX_DOT_P_WEG_2ND_Mmu ) ;   		                			 
            			                					                                                		                			 
            	esp_tempW5 = LCESP_s16IInter3Point( ay_vx_dot_sig_plus, 0, (int16_t)S16_YC_AY_VX_DOT_D_WEG_Mmu,          		                			 
            	  									              ay_vx_dot_sig_1st_th_Mmu, (int16_t)S16_YC_AY_VX_DOT_D_WEG_1ST_Mmu,				                			 
            			                			        ay_vx_dot_sig_2nd_th_Mmu, (int16_t)S16_YC_AY_VX_DOT_D_WEG_2ND_Mmu ) ;   		                			 

    	}
    	else
	    {       
            	esp_tempW2 = LCESP_s16IInter3Point( ay_vx_dot_sig_minus, 0, (int16_t)S16_YC_AY_VX_DOT_P_WEG, 
            	  												        ay_vx_dot_sig_1st_th, (int16_t)S16_YC_AY_VX_DOT_P_WEG_1ST,						                      
            			                						  ay_vx_dot_sig_2nd_th, (int16_t)S16_YC_AY_VX_DOT_P_WEG_2ND ) ;		
           
              esp_tempW3 = LCESP_s16IInter3Point( ay_vx_dot_sig_minus, 0, (int16_t)S16_YC_AY_VX_DOT_D_WEG, 
            	  											          ay_vx_dot_sig_1st_th, (int16_t)S16_YC_AY_VX_DOT_D_WEG_1ST,						                      
            			                					    ay_vx_dot_sig_2nd_th, (int16_t)S16_YC_AY_VX_DOT_D_WEG_2ND ) ;	

            	esp_tempW4 = LCESP_s16IInter3Point( ay_vx_dot_sig_minus, 0, (int16_t)S16_YC_AY_VX_DOT_P_WEG_Mmu,          			                			  
            	  								                ay_vx_dot_sig_1st_th_Mmu, (int16_t)S16_YC_AY_VX_DOT_P_WEG_1ST_Mmu,				                			  
            			                			        ay_vx_dot_sig_2nd_th_Mmu, (int16_t)S16_YC_AY_VX_DOT_P_WEG_2ND_Mmu ) ;   		                			  
            
            	esp_tempW5 = LCESP_s16IInter3Point( ay_vx_dot_sig_minus, 0, (int16_t)S16_YC_AY_VX_DOT_D_WEG_Mmu,          			                			  
            	  									                ay_vx_dot_sig_1st_th_Mmu, (int16_t)S16_YC_AY_VX_DOT_D_WEG_1ST_Mmu,				                			  
            			                			          ay_vx_dot_sig_2nd_th_Mmu, (int16_t)S16_YC_AY_VX_DOT_D_WEG_2ND_Mmu ) ;   		                			              			                						  

        }

            if(delta_yaw_first < S16_YC_AY_VX_DOT_DCT_OS_YAW)
            {
             	  esp_tempW2 = LCESP_s16FindRoadDependatGain(esp_mu, esp_tempW2, esp_tempW4, 100); 
            	  esp_tempW3 = LCESP_s16FindRoadDependatGain(esp_mu, esp_tempW3, esp_tempW5, 100);                  		
            }
            else
            {
				        esp_tempW2 = 100;
				        esp_tempW3 = 100;
    }

            yaw_control_P_gain_front =  (int16_t)((((int32_t)yaw_control_P_gain_front)*esp_tempW2)/100 ); 	   
            yaw_control_D_gain_front =  (int16_t)((((int32_t)yaw_control_D_gain_front)*esp_tempW3)/100 );

        #if (__YAW_M_OVERSHOOT_DETECTION == 1)          
        
            if (ldespu1YawRateOverShoot == 1)
        {
                esp_tempW2 = LCESP_s16IInter3Point(abs_wstr, S16_3RD_ST_DM_REF_WSTR_L_HMU, S16_3RD_ST_DM_WT_BY_WSTR_L_HMU, 
                                                             S16_3RD_ST_DM_REF_WSTR_M_HMU, S16_3RD_ST_DM_WT_BY_WSTR_M_HMU, 
                                                             S16_3RD_ST_DM_REF_WSTR_H_HMU, S16_3RD_ST_DM_WT_BY_WSTR_H_HMU);
            
                esp_tempW3 = LCESP_s16IInter3Point(abs_wstr_dot2, S16_3RD_ST_DM_REF_WSTR_DOT_L_HMU, S16_3RD_ST_DM_WT_BY_WSTRD_L_HMU, 
                                                                 S16_3RD_ST_DM_REF_WSTR_DOT_M_HMU, S16_3RD_ST_DM_WT_BY_WSTRD_M_HMU, 
                                                                 S16_3RD_ST_DM_REF_WSTR_DOT_H_HMU, S16_3RD_ST_DM_WT_BY_WSTRD_H_HMU);
        
                esp_tempW4 = LCESP_s16IInter3Point(abs_wstr, S16_3RD_ST_DM_REF_WSTR_L_MMU, S16_3RD_ST_DM_WT_BY_WSTR_L_MMU, 
                                                             S16_3RD_ST_DM_REF_WSTR_M_MMU, S16_3RD_ST_DM_WT_BY_WSTR_M_MMU, 
                                                             S16_3RD_ST_DM_REF_WSTR_H_MMU, S16_3RD_ST_DM_WT_BY_WSTR_H_MMU);
        
                esp_tempW5 = LCESP_s16IInter3Point(abs_wstr_dot2, S16_3RD_ST_DM_REF_WSTR_DOT_L_MMU, S16_3RD_ST_DM_WT_BY_WSTRD_L_MMU, 
                                                                 S16_3RD_ST_DM_REF_WSTR_DOT_M_MMU, S16_3RD_ST_DM_WT_BY_WSTRD_M_MMU, 
                                                                 S16_3RD_ST_DM_REF_WSTR_DOT_H_MMU, S16_3RD_ST_DM_WT_BY_WSTRD_H_MMU);
        
                esp_tempW6 = LCESP_s16IInter3Point(abs_wstr, S16_3RD_ST_DM_REF_WSTR_L_LMU, S16_3RD_ST_DM_WT_BY_WSTR_L_LMU, 
                                                             S16_3RD_ST_DM_REF_WSTR_M_LMU, S16_3RD_ST_DM_WT_BY_WSTR_M_LMU, 
                                                             S16_3RD_ST_DM_REF_WSTR_H_LMU, S16_3RD_ST_DM_WT_BY_WSTR_H_LMU);

                esp_tempW7 = LCESP_s16IInter3Point(abs_wstr_dot2, S16_3RD_ST_DM_REF_WSTR_DOT_L_LMU, S16_3RD_ST_DM_WT_BY_WSTRD_L_LMU, 
                                                                 S16_3RD_ST_DM_REF_WSTR_DOT_M_LMU, S16_3RD_ST_DM_WT_BY_WSTRD_M_LMU, 
                                                                 S16_3RD_ST_DM_REF_WSTR_DOT_H_LMU, S16_3RD_ST_DM_WT_BY_WSTRD_H_LMU);                                                                                                  
                
                if (esp_tempW2 > esp_tempW3)
             {
                    esp_tempW3 = esp_tempW2;
            }
                else
            {
                    ;
           }
                if (esp_tempW4 > esp_tempW5)
                {
                    esp_tempW5 = esp_tempW4;
        }
        else
        {
                    ;
                }
                if (esp_tempW6 > esp_tempW7)
             {
                    esp_tempW7 = esp_tempW6;
            }
                else
            {
                    ;
            }
            
                lcesps16YawOverShootPgainWeight = LCESP_s16FindRoadDependatGain(esp_mu, esp_tempW3, esp_tempW5, esp_tempW7);
                lcesps16YawOverShootDgainWeight = LCESP_s16FindRoadDependatGain(esp_mu, esp_tempW3, esp_tempW5, esp_tempW7);
                yaw_control_P_gain_front =  (int16_t)((((int32_t)yaw_control_P_gain_front)*lcesps16YawOverShootPgainWeight)/100 ); 	   
                yaw_control_D_gain_front =  (int16_t)((((int32_t)yaw_control_D_gain_front)*lcesps16YawOverShootDgainWeight)/100 );
            }
            else
            {
                ;
        }
        #endif  
        
        #endif   

#if __ESC_SW_OFF_ROP_ACT_ENABLE ==1 

	        if(Flg_ESC_SW_ROP_OK==1)
            {               
				yaw_control_P_gain_front =  (int16_t)((((int32_t)yaw_control_P_gain_front)*S16_ROP_IN_ESC_OFF_P_GAIN_WEG)/100 );   
  	            yaw_control_D_gain_front =  (int16_t)((((int32_t)yaw_control_D_gain_front)*S16_ROP_IN_ESC_OFF_D_GAIN_WEG)/100 );    
  
            }
            else
            {
                ;
            }

     
#endif
            
/*********************** yaw gain weighting for GM function (Low Pressure) ********************* 10SWD */         																           
   #if __GM_FailM
			if((lespu8TirePressureStatFL==5)||(lespu8TirePressureStatFR==5)||(lespu8TirePressureStatRL==5)||(lespu8TirePressureStatRR==5))	/* GM pressure is Flat mode */
            { 
				esp_tempW3 = S16_Common_Temp4; // (int16_t)((((int32_t)esp_tempW7)*S16_Common_Temp4)/100);																			           
            }
			else if((lespu8TirePressureStatFL==4)||(lespu8TirePressureStatFR==4)||(lespu8TirePressureStatRL==4)||(lespu8TirePressureStatRR==4))	/* GM pressure is Low-Extended Mobility mode */
            { 
				esp_tempW3 = S16_Common_Temp3; // (int16_t)((((int32_t)esp_tempW7)*S16_Common_Temp3)/100);																			           
            }     
			else if((lespu8TirePressureStatFL==2)||(lespu8TirePressureStatFR==2)||(lespu8TirePressureStatRL==2)||(lespu8TirePressureStatRR==2))	/* GM pressure is Low-Service Now mode */
             {
				esp_tempW3 = S16_Common_Temp2; // (int16_t)((((int32_t)esp_tempW7)*S16_Common_Temp2)/100);
            }
			else if((lespu8TirePressureStatFL==3)||(lespu8TirePressureStatFR==3)||(lespu8TirePressureStatRL==3)||(lespu8TirePressureStatRR==3))	/* GM pressure is Low mode */
            {
				esp_tempW3 = S16_Common_Temp1; // (int16_t)((((int32_t)esp_tempW7)*S16_Common_Temp1)/100);
        }
        else
        {
				esp_tempW3 = 100;
            }

      if(esp_tempW3!=100) 
            {
  			yaw_control_P_gain_front =  (int16_t)((((int32_t)yaw_control_P_gain_front)*esp_tempW3)/100 );
        yaw_control_D_gain_front =  (int16_t)((((int32_t)yaw_control_D_gain_front)*esp_tempW3)/100 );
        yaw_control_P_gain_rear  =  (int16_t)((((int32_t)yaw_control_P_gain_rear)*esp_tempW3)/100 );
        yaw_control_D_gain_rear  =  (int16_t)((((int32_t)yaw_control_D_gain_rear)*esp_tempW3)/100 ); 
            }
  #endif      
/*********************************************************************************************************/
        }

 void LCESP_vCalOverSteerYawGain(void)
{


        esp_tempW5 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_OU_GAIN_F_OS_HIGH, S16_YC_OU_GAIN_F_OS, S16_YC_OU_GAIN_F_OS_LOW);

/* 06 LPG : Throttle Off   */

        esp_tempW1 =  LCESP_s16IInter3Point(mtp, 0 , 10, 10 , 5,  30 , 0); /* '06 LPG */

        esp_tempW5 =  ( esp_tempW5 + esp_tempW1 )/on_center_gain;


/* 06 SWD : D-gain  ( med-mu ) */

        #if __CAR == FORD_C214
            esp_tempW9 = LCESP_s16IInter3Point(vrefk, 200 , 20, 400 , 70,  500 , 100);
        #else
            esp_tempW9 = 100 ;
        #endif

        esp_tempW5 = (int16_t)( (((int32_t)esp_tempW9)*esp_tempW5)/100 );

        esp_tempW7 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_D_GAIN_F_OS_IN_H, S16_YC_D_GAIN_F_OS_IN, S16_YC_D_GAIN_F_OS_IN_L);

        /*  '07.05.08 : D-gain Limitation by Issue_2007-369 */

        if ( esp_tempW7 > 20 )
        {
            esp_tempW7 = 20;
        }
        else
        {
            ;
        }

        esp_tempW7 =  esp_tempW7 / on_center_gain;

        esp_tempW7 = (int16_t)( (((int32_t)esp_tempW9)*esp_tempW7)/100 );

        if(counter_deltayaw_signchange<2)
        {
            esp_tempW7=0;
        }
        else
        {
            ;
        }

        if(delta_yaw>o_delta_yaw_thres)
        {

            if ( yaw_error_dot > 0 )
            {      /*Oversteer Inc.*/
/*                pd_ymc_gain = 3 ; */
                esp_tempW0 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_P_GAIN_F_OS_OUT_INC_H, S16_YC_P_GAIN_F_OS_OUT_INC, S16_YC_P_GAIN_F_OS_OUT_INC_L);
                yaw_control_P_gain_front= -esp_tempW0;
                esp_tempW0 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_D_GAIN_F_OS_OUT_INC_H, S16_YC_D_GAIN_F_OS_OUT_INC, S16_YC_D_GAIN_F_OS_OUT_INC_L);
                esp_tempW0 =  (int16_t)( ((int32_t)esp_tempW0)/on_center_gain );
                if ( esp_tempW0 < esp_tempW7 )
                {
                    esp_tempW0 = esp_tempW7;
                }
                else
                {
                    ;
                }
                yaw_control_D_gain_front= -esp_tempW0;
                if( (FLAG_SLALOM==1)  && (esp_mu > lsesps16MuLow) )   /* 06 SWD */
                {
                    yaw_control_P_gain_front =  (int16_t)( (((int32_t)yaw_control_P_gain_front)*S16_SLALOM_OVER_GAIN)/100 );
                    yaw_control_D_gain_front =  (int16_t)( (((int32_t)yaw_control_D_gain_front)*S16_SLALOM_OVER_GAIN)/100 );
                }
                else
                {
                    ;
                }

                	yaw_control_P_gain_front =  (INT)( (((LONG)yaw_control_P_gain_front)*s16_yaw_accel_fb_gain)/100 );
               		yaw_control_D_gain_front =  (INT)( (((LONG)yaw_control_D_gain_front)*s16_yaw_accel_fb_gain)/100 );

            }
            else
            {                        /* Oversteer Dec.*/
                    esp_tempW0 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_P_GAIN_F_OS_OUT_DEC_H, S16_YC_P_GAIN_F_OS_OUT_DEC, S16_YC_P_GAIN_F_OS_OUT_DEC_L);
                    yaw_control_P_gain_front= -esp_tempW0;
                    esp_tempW0 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_D_GAIN_F_OS_OUT_DEC_H, S16_YC_D_GAIN_F_OS_OUT_DEC, S16_YC_D_GAIN_F_OS_OUT_DEC_L);
                    yaw_control_D_gain_front= -esp_tempW0;              
                
                if(ABS_fz==1)
                {
                    yaw_control_D_gain_front = 0;
                }
                else
                {
                    ;
                }
                if( (FLAG_SLALOM==1)  && (esp_mu > lsesps16MuLow) )
                {
                    yaw_control_P_gain_front =  (int16_t)( (((int32_t)yaw_control_P_gain_front)*S16_SLALOM_OVER_GAIN)/100 );
                    yaw_control_D_gain_front =  (int16_t)( (((int32_t)yaw_control_D_gain_front)*100)/S16_SLALOM_OVER_GAIN );
                }
                else
                {
                    ;
                }
                	yaw_control_P_gain_front =  (INT)( (((LONG)yaw_control_P_gain_front)*s16_yaw_accel_fb_gain)/100 );
               		yaw_control_D_gain_front =  (INT)( (((LONG)yaw_control_D_gain_front)*100)/s16_yaw_accel_fb_gain );               		               
                
            }

            yaw_control_P_gain_rear=-YC_P_GAIN_R_OVERSTEER_OUT;
            yaw_control_D_gain_rear=-YC_D_GAIN_R_OVERSTEER_OUT;
            
        }
        else
        {   /* oversteer inside*/
            yaw_control_P_gain_front=-YC_P_GAIN_F_OVERSTEER_IN;

            if ( yaw_error_dot > 0 )   /*Oversteer Inc.*/
            {
         	 if( (YAW_CHANGE_LIMIT==1) &&((SLIP_CONTROL_NEED_FL==1)||(SLIP_CONTROL_NEED_FR==1)) )
             {
                esp_tempW0 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_D_GAIN_F_OS_OUT_INC_H, S16_YC_D_GAIN_F_OS_OUT_INC, S16_YC_D_GAIN_F_OS_OUT_INC_L);
                esp_tempW0 =  (int16_t)( ((int32_t)esp_tempW0)/on_center_gain );
                if ( esp_tempW0 < esp_tempW7 )
                {
                    esp_tempW0 = esp_tempW7;
                }
                else
                {
                    ;
                }
                yaw_control_D_gain_front= -esp_tempW0;
                if( (FLAG_SLALOM==1)  && (esp_mu > lsesps16MuLow) )   /* 06 SWD */
                {
                    yaw_control_P_gain_front =  (int16_t)( (((int32_t)yaw_control_P_gain_front)*S16_SLALOM_OVER_GAIN)/100 );
                    yaw_control_D_gain_front =  (int16_t)( (((int32_t)yaw_control_D_gain_front)*S16_SLALOM_OVER_GAIN)/100 );
                }
             }
             else
             {
                yaw_control_D_gain_front=-esp_tempW7;
             }


            }
            else        /*Oversteer dec.*/
            {
             yaw_control_D_gain_front= 0 ;
            }
        
    #if  __BANK_DETECT_ESC_CTRL_ENABLE == 1    

        if	( (FLAG_BANK_DETECTED==1) || (FLAG_BANK_SUSPECT==1) )	
    	{			             
            yaw_control_D_gain_front =  (int16_t)( (((int32_t)yaw_control_D_gain_front)*S16_BANK_CTRL_INS_D_WEG)/100 );            
    	}
    	else
	    {       
	        ;
	    }
           
    #endif      

            yaw_control_P_gain_rear=-YC_P_GAIN_R_OVERSTEER_IN;
            yaw_control_D_gain_rear=-YC_D_GAIN_R_OVERSTEER_IN; 
            

        }

 /**************BETA CONTROL******/

            esp_tempW6 = LCESP_s16FindRoadDependatGain( det_alatm, 0, 3, 3);
            if((FLAG_DETECT_DRIFT==0) && (ESP_MINI_TIRE_SUSPECT==1))
            {
                yaw_control_P_gain_front=yaw_control_P_gain_front - esp_tempW6;
            }
            else
            {
                ;
            }
    }
void LCESP_vCalUnderSteerYawGain(void)
{
    if(delta_yaw<u_delta_yaw_thres)
    {
        /*  P_gain = +
            D_gain_dec = +
            D_gain_inc = +
            del_yaw = -
            del_yaw_dot_dec = -
            del_yaw_dot_inc = +   --> M_Q_R = -

          ( __MGH40_ESC_Concepts )
            P_gain' = -
            D_gain_dec' = -
            D_gain_inc' = -
            del_yaw = -
            del_yaw_dot_dec = -
            del_yaw_dot_inc = +   --> del_M +  */

            yaw_control_P_gain_front=YC_P_GAIN_F_UNDERSTEER_OUT;
            yaw_control_D_gain_front=YC_D_GAIN_F_UNDERSTEER_OUT;

        #if (__MGH60_ESC_IMPROVE_CONCEPT == 1) 	   
        
        if ( yaw_error_dot > 0 )    /*Understeer Inc.*/
        {
            esp_tempW1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_P_GAIN_R_US_OUT_INC_H, S16_YC_P_GAIN_R_US_OUT_INC, S16_YC_P_GAIN_R_US_OUT_INC_L);
            yaw_control_P_gain_rear=-esp_tempW1;
            
            esp_tempW1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_D_GAIN_R_US_OUT_INC_H, S16_YC_D_GAIN_R_US_OUT_INC, S16_YC_D_GAIN_R_US_OUT_INC_L);
            yaw_control_D_gain_rear = esp_tempW1;
        }
        else                        /*Understeer Dec.*/
        {
           esp_tempW1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_P_GAIN_R_US_OUT_H, S16_YC_P_GAIN_R_US_OUT, S16_YC_P_GAIN_R_US_OUT_L);
           yaw_control_P_gain_rear=-esp_tempW1;
        
           esp_tempW1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_D_GAIN_R_US_OUT_DEC_H, S16_YC_D_GAIN_R_US_OUT_DEC, S16_YC_D_GAIN_R_US_OUT_DEC_L);
           yaw_control_D_gain_rear = esp_tempW1;
        }     
        
        #else  
        
        if ( (Beta_Moment_Ctrl_Flg==0)&&(Reverse_Steer_Flg==0)&&(YAW_CHANGE_LIMIT_ACT==1))      /* '06.07.20 CYJ */
        {
            esp_tempW1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_P_GAIN_R_US_IN_LMT_H,
                                                S16_YC_P_GAIN_R_US_IN_LMT, S16_YC_P_GAIN_R_US_IN_LMT_L);
            yaw_control_P_gain_rear=-esp_tempW1;

            if ( yaw_error_dot > 0 )    /*Understeer Inc.*/
             {
                esp_tempW1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_D_GAIN_R_US_IN_LMT_INC_H,
                                            S16_YC_D_GAIN_R_US_IN_LMT_INC, S16_YC_D_GAIN_R_US_IN_LMT_INC_L);
                yaw_control_D_gain_rear = esp_tempW1;  /* - */
            }
            else                        /*Understeer Dec.*/
            {
               esp_tempW1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_D_GAIN_R_US_IN_LMT_DEC_H, S16_YC_D_GAIN_R_US_IN_LMT_DEC, S16_YC_D_GAIN_R_US_IN_LMT_DEC_L);
                 yaw_control_D_gain_rear = esp_tempW1;  /* - */
           }
        }
        else
        {
            esp_tempW1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_P_GAIN_R_US_OUT_H, S16_YC_P_GAIN_R_US_OUT, S16_YC_P_GAIN_R_US_OUT_L);
            yaw_control_P_gain_rear=-esp_tempW1;

            if ( yaw_error_dot > 0 )    /*Understeer Inc.*/
             {
                esp_tempW1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_D_GAIN_R_US_OUT_INC_H, S16_YC_D_GAIN_R_US_OUT_INC, S16_YC_D_GAIN_R_US_OUT_INC_L);
                yaw_control_D_gain_rear = esp_tempW1;
            }
            else                        /*Understeer Dec.*/
            {
               esp_tempW1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_D_GAIN_R_US_OUT_DEC_H, S16_YC_D_GAIN_R_US_OUT_DEC, S16_YC_D_GAIN_R_US_OUT_DEC_L);
               yaw_control_D_gain_rear = esp_tempW1;
            }
        }
        
        #endif   


            if( (FLAG_SLALOM==1)  && (esp_mu > lsesps16MuLow) )
            {               
                yaw_control_P_gain_rear =  (int16_t)( ((int32_t)yaw_control_P_gain_rear)/2 );
                yaw_control_D_gain_rear =  (int16_t)( ((int32_t)yaw_control_D_gain_rear)/2 );
            }
            else
            {
                ;
            }
        }
        else  /* Understeer Inside */
        {
            yaw_control_P_gain_front=YC_P_GAIN_F_UNDERSTEER_IN;
            yaw_control_D_gain_front=YC_D_GAIN_F_UNDERSTEER_IN;


     #if (__MGH60_ESC_IMPROVE_CONCEPT == 1) 	  
     
            yaw_control_P_gain_rear =-YC_P_GAIN_R_UNDERSTEER_IN;
            
            if ( yaw_error_dot > 0 )    /*Understeer Inc.*/
            { 
                 yaw_control_D_gain_rear = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_D_GAIN_R_US_IN_INC_H, S16_YC_D_GAIN_R_US_IN_INC, S16_YC_D_GAIN_R_US_IN_INC_L);
            }
            else                        /*Understeer Dec.*/
            { 
                 yaw_control_D_gain_rear=-S16_YC_D_GAIN_R_US_IN;  
            }     
            
     #else    
     
        if ( (Beta_Moment_Ctrl_Flg==0)&&(Reverse_Steer_Flg==0)&&(YAW_CHANGE_LIMIT_ACT==1) )      /* '06.07.20 CYJ */
        {
            esp_tempW1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_P_GAIN_R_US_IN_LMT_H, S16_YC_P_GAIN_R_US_IN_LMT, S16_YC_P_GAIN_R_US_IN_LMT_L);
            yaw_control_P_gain_rear=-esp_tempW1;

            if ( yaw_error_dot > 0 )    /*Understeer Inc.*/
             {
                esp_tempW1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_D_GAIN_R_US_IN_LMT_INC_H, S16_YC_D_GAIN_R_US_IN_LMT_INC, S16_YC_D_GAIN_R_US_IN_LMT_INC_L);
                yaw_control_D_gain_rear = esp_tempW1;
            }
            else                        /*Understeer Dec.*/
            {
               esp_tempW1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_D_GAIN_R_US_IN_LMT_DEC_H, S16_YC_D_GAIN_R_US_IN_LMT_DEC, S16_YC_D_GAIN_R_US_IN_LMT_DEC_L);
               yaw_control_D_gain_rear = esp_tempW1;
            }
        }
        else
        {
            yaw_control_P_gain_rear =-YC_P_GAIN_R_UNDERSTEER_IN;
            if ( yaw_error_dot > 0 )    /*Understeer Inc.*/
            {
                 yaw_control_D_gain_rear = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_D_GAIN_R_US_IN_INC_H, S16_YC_D_GAIN_R_US_IN_INC, S16_YC_D_GAIN_R_US_IN_INC_L);
            }
            else                        /*Understeer Dec.*/
            {
                 yaw_control_D_gain_rear=-S16_YC_D_GAIN_R_US_IN;  /* Minus --> Del_M(+) */
            }
        }
     #endif     
     }
}

void LCESP_vCalSpeedDepGainRear(void)
{
    LCESP_vSetRearspeedDepMU();
		
    yaw_control_P_gain_rear_limit = (int16_t)( (((int32_t)yaw_control_P_gain_rear)*10)/100 );

    yaw_control_D_gain_rear_limit = (int16_t)( (((int32_t)yaw_control_D_gain_rear)*10)/100 ); 
    
   /* '06 TRC */
   if(ABS_fz==1) 
   {
   	  rear_low_speed_gain =LCESP_s16IInter6Point( esp_mu, S16_MU_LOW,      S16_LMU_REAR_LOW_SPD_GAIN_ABS,
                                                          S16_MU_MED,      S16_MMU_REAR_LOW_SPD_GAIN_ABS,
                                                          S16_MU_MED_HIGH, S16_MMU_REAR_LOW_SPD_GAIN_ABS,
                                                          S16_MU_HIGH,     S16_HMU_REAR_LOW_SPD_GAIN_ABS,
                                                          S16_MU_HIGH2,    S16_HMU_REAR_LOW_SPD_GAIN_ABS,                                                                                                                   
                                                          S16_MU_V_HIGH,   S16_VHMU_REAR_LOW_SPD_GAIN_ABS);
      
      rear_med_speed_gain =LCESP_s16IInter6Point( esp_mu, S16_MU_LOW,      S16_LMU_REAR_MED_SPD_GAIN_ABS,
                                                          S16_MU_MED,      S16_MMU_REAR_MED_SPD_GAIN_ABS,
                                                          S16_MU_MED_HIGH, S16_MMU_REAR_MED_SPD_GAIN_ABS,
                                                          S16_MU_HIGH,     S16_HMU_REAR_MED_SPD_GAIN_ABS,
                                                          S16_MU_HIGH2,    S16_HMU_REAR_MED_SPD_GAIN_ABS,                                                                                                                   
                                                          S16_MU_V_HIGH,   S16_VHMU_REAR_MED_SPD_GAIN_ABS);
      
      rear_high_speed_gain =LCESP_s16IInter6Point( esp_mu, S16_MU_LOW,      S16_LMU_REAR_HIG_SPD_GAIN_ABS,
                                                           S16_MU_MED,      S16_MMU_REAR_HIG_SPD_GAIN_ABS,
                                                           S16_MU_MED_HIGH, S16_MMU_REAR_HIG_SPD_GAIN_ABS,
                                                           S16_MU_HIGH,     S16_HMU_REAR_HIG_SPD_GAIN_ABS,
                                                           S16_MU_HIGH2,    S16_HMU_REAR_HIG_SPD_GAIN_ABS,                                                                                                                   
                                                           S16_MU_V_HIGH,   S16_VHMU_REAR_HIGH_SPD_GAIN_ABS);
   	/*
    rear_low_speed_gain =LCESP_s16FindRoadDependatGain( esp_mu, S16_HMU_REAR_LOW_SPD_GAIN_ABS,
                                S16_MMU_REAR_LOW_SPD_GAIN_ABS, S16_LMU_REAR_LOW_SPD_GAIN_ABS);

    rear_med_speed_gain =LCESP_s16FindRoadDependatGain( esp_mu, S16_HMU_REAR_MED_SPD_GAIN_ABS,
                                S16_MMU_REAR_MED_SPD_GAIN_ABS, S16_LMU_REAR_MED_SPD_GAIN_ABS);

    rear_high_speed_gain =LCESP_s16FindRoadDependatGain( esp_mu, S16_HMU_REAR_HIG_SPD_GAIN_ABS,
                                S16_MMU_REAR_HIG_SPD_GAIN_ABS, S16_LMU_REAR_HIG_SPD_GAIN_ABS);
    */
   }
   else
   {
   	  rear_low_speed_gain =LCESP_s16IInter6Point( esp_mu, S16_MU_LOW,      S16_LMU_REAR_LOW_SPD_GAIN_WEG,
                                                          S16_MU_MED,      S16_MMU_REAR_LOW_SPD_GAIN_WEG,
                                                          S16_MU_MED_HIGH, S16_MMU_REAR_LOW_SPD_GAIN_WEG,
                                                          S16_MU_HIGH,     S16_HMU_REAR_LOW_SPD_GAIN_WEG,
                                                          S16_MU_HIGH2,    S16_HMU_REAR_LOW_SPD_GAIN_WEG,                                                                                                                   
                                                          S16_MU_V_HIGH,   S16_VHMU_REAR_LOW_SPD_GAIN_WEG);
      
      rear_med_speed_gain =LCESP_s16IInter6Point( esp_mu, S16_MU_LOW,      S16_LMU_REAR_MED_SPD_GAIN_WEG,
                                                          S16_MU_MED,      S16_MMU_REAR_MED_SPD_GAIN_WEG,
                                                          S16_MU_MED_HIGH, S16_MMU_REAR_MED_SPD_GAIN_WEG,
                                                          S16_MU_HIGH,     S16_HMU_REAR_MED_SPD_GAIN_WEG,
                                                          S16_MU_HIGH2,    S16_HMU_REAR_MED_SPD_GAIN_WEG,                                                                                                                   
                                                          S16_MU_V_HIGH,   S16_VHMU_REAR_MED_SPD_GAIN_WEG);
      
      rear_high_speed_gain =LCESP_s16IInter6Point( esp_mu, S16_MU_LOW,      S16_LMU_REAR_HIGH_SPD_GAIN_WEG,
                                                           S16_MU_MED,      S16_MMU_REAR_HIGH_SPD_GAIN_WEG,
                                                           S16_MU_MED_HIGH, S16_MMU_REAR_HIGH_SPD_GAIN_WEG,
                                                           S16_MU_HIGH,     S16_HMU_REAR_HIGH_SPD_GAIN_WEG,
                                                           S16_MU_HIGH2,    S16_HMU_REAR_HIGH_SPD_GAIN_WEG,                                                                                                                   
                                                           S16_MU_V_HIGH,   S16_VHMU_REAR_HIGH_SPD_GAIN_WEG);
   	/*
    rear_low_speed_gain =LCESP_s16FindRoadDependatGain( esp_mu, S16_HMU_REAR_LOW_SPD_GAIN_WEG,
                                S16_MMU_REAR_LOW_SPD_GAIN_WEG, S16_LMU_REAR_LOW_SPD_GAIN_WEG);

    rear_med_speed_gain =LCESP_s16FindRoadDependatGain( esp_mu, S16_HMU_REAR_MED_SPD_GAIN_WEG,
                                S16_MMU_REAR_MED_SPD_GAIN_WEG, S16_LMU_REAR_MED_SPD_GAIN_WEG);

    rear_high_speed_gain =LCESP_s16FindRoadDependatGain( esp_mu, S16_HMU_REAR_HIGH_SPD_GAIN_WEG,
                                S16_MMU_REAR_HIGH_SPD_GAIN_WEG, S16_LMU_REAR_HIGH_SPD_GAIN_WEG);
    */
   }
      #if __SMOOTH_REAR_DEL_M_GAIN
    lcesps16RearSpeedGain = LCESP_s16IInter4Point(vrefk, rear_low_speed ,  rear_low_speed_gain,
                                                        rear_med1_speed , rear_med_speed_gain,
                                                        rear_med2_speed , rear_med_speed_gain,
                                                        rear_high_speed , rear_high_speed_gain); 
    if (lcesps16RearSpeedGain < 10) 
    {
        lcesps16RearSpeedGain = 10;
    }
    else
    {
        ;
    }
      #else                                                   
    esp_tempW0 = LCESP_s16IInter4Point(vrefk, rear_low_speed , rear_low_speed_gain,
                          rear_med1_speed , rear_med_speed_gain,
                          rear_med2_speed , rear_med_speed_gain,
                          rear_high_speed , rear_high_speed_gain);

    esp_tempW1 = (int16_t)( (((int32_t)yaw_control_P_gain_rear)*esp_tempW0)/100 );

    esp_tempW2 = (int16_t)( (((int32_t)yaw_control_D_gain_rear)*esp_tempW0)/100 );


    if ( McrAbs(esp_tempW1) > McrAbs(yaw_control_P_gain_rear_limit) )
    {
            yaw_control_P_gain_rear = esp_tempW1;
    }
    else
    {
            yaw_control_P_gain_rear = yaw_control_P_gain_rear_limit;
    }

    if ( McrAbs(esp_tempW2) > McrAbs(yaw_control_D_gain_rear_limit) )
    {
            yaw_control_D_gain_rear = esp_tempW2;
    }
    else
    {
            yaw_control_D_gain_rear = yaw_control_D_gain_rear_limit;
    }
      #endif
}

void LCESP_vCalSpeedDepBetaGain(void)
{
    /* v_v1 = 6, v_v2 = 15, v_v3 = 70, v_v4 = 90, v_v5 = 120*/
    int16_t v_v1 = ESP_BETA_SPEED_GAIN_DEPENDANT_S_V;
    int16_t v_v2 = ESP_BETA_SPEED_GAIN_DEPENDANT_L_V;
    int16_t v_v3 = ESP_BETA_SPEED_GAIN_DEPENDANT_M_V;
    int16_t v_v4 = ESP_BETA_SPEED_GAIN_DEPENDANT_H_V;
    int16_t v_g1;
    int16_t v_g2;
    int16_t v_g3;

    v_g1 = LCESP_s16FindRoadDependatGain( esp_mu, ESP_BETA_SPEED_H_MU_GAIN_DEPENDANT_L,
                                ESP_BETA_SPEED_M_MU_GAIN_DEPENDANT_L, ESP_BETA_SPEED_L_MU_GAIN_DEPENDANT_L);
    v_g2 = LCESP_s16FindRoadDependatGain( esp_mu, ESP_BETA_SPEED_H_MU_GAIN_DEPENDANT_M,
                                ESP_BETA_SPEED_M_MU_GAIN_DEPENDANT_M,  ESP_BETA_SPEED_L_MU_GAIN_DEPENDANT_M);
    v_g3 = LCESP_s16FindRoadDependatGain( esp_mu, ESP_BETA_SPEED_H_MU_GAIN_DEPENDANT_H,
                                ESP_BETA_SPEED_M_MU_GAIN_DEPENDANT_H, ESP_BETA_SPEED_L_MU_GAIN_DEPENDANT_H);

    if( vrefk <= v_v1 )
    {
        gain_speed_beta_depend = v_g1;
    }
    else if ( vrefk < v_v2 )
    {
        if( v_v2==v_v1 )
        {
            v_v2 =  v_v2 + 1;
        }
        else
        {
            ;
        }
        gain_speed_beta_depend = v_g1 + (int16_t)( (((int32_t)(v_g2-v_g1))*(vrefk-v_v1))/(v_v2-v_v1) );
    }
    else if(vrefk < v_v3)
    {
        gain_speed_beta_depend = v_g2;
    }
   else if ( vrefk < v_v4 )
   {
        if( v_v4==v_v3 )
        {
            v_v4 =  v_v4 + 1;
        }
        else
        {
            ;
        }
        gain_speed_beta_depend = v_g2 + (int16_t)( (((int32_t)(v_g3-v_g2))*(vrefk-v_v3))/(v_v4-v_v3) );
    }
    else
    {
        gain_speed_beta_depend = v_g3;
    }

}

void LCESP_vCalDeltaBeta2(void)
{
/*  delta_beta2 = Beta_MD - Beta_Dyn ;*/

    esp_tempW5 = LCESP_s16FindRoadDependatGain(esp_mu, S16_BETA_THRES_LMT_H , S16_BETA_THRES_LMT, S16_BETA_THRES_LMT_L);

    esp_tempW3 = LCESP_s16FindRoadDependatGain(esp_mu, S16_BETA_THRES_Lspd_Hmu , S16_BETA_THRES_Lspd_Mmu, S16_BETA_THRES_Lspd_Lmu);

    esp_tempW4 = LCESP_s16FindRoadDependatGain(esp_mu, S16_BETA_THRES_Mspd_Hmu , S16_BETA_THRES_Mspd_Mmu, S16_BETA_THRES_Mspd_Lmu);


    esp_tempW6 = LCESP_s16IInter3Point( vrefk, S16_BETA_THRES_Low_Spd, esp_tempW3,
                                               S16_BETA_THRES_Med_Spd, esp_tempW4,
                                               S16_BETA_THRES_Hig_Spd, esp_tempW5 );

    esp_tempW3 = LCESP_s16IInter3Point( vrefk, S16_BETA_THRES_Low_Spd, 700,
                                               S16_BETA_THRES_Med_Spd, 500,
                                               S16_BETA_THRES_Hig_Spd, 400 ); /* enter beta_K_dot  r: 0.01*/

    delta_beta2 = McrAbs(Beta_MD)-esp_tempW6  ;   /* '06.07.20 CYJ */

    esp_tempW9 = McrAbs(Beta_K_Dot) ;

    if ( (( SLIP_CONTROL_NEED_FR==1 ) && ( Beta_MD < 0 ) && ( yaw_sign > 0 ))
         || (( SLIP_CONTROL_NEED_FL==1 ) && ( Beta_MD > 0 ) && ( yaw_sign < 0 )) )
/*         || (( YAW_CHANGE_LIMIT_ACT==1 ) && ( Beta_MD < 0 ) && ( yaw_sign > 0 )) */ /* '07.2.21 Beta_control Robustness */
/*         || (( YAW_CHANGE_LIMIT_ACT==1 ) && ( Beta_MD > 0 ) && ( yaw_sign < 0 )) )*/
    {
        esp_tempW8 = 1 ;
    }
    else
    {
        esp_tempW8 = -1 ;
    }

    if ( (Beta_Moment_Ctrl_Flg==0) && ( esp_tempW8 >= 0 ) && ( delta_beta2 >= 0 )
         && ((( Beta_MD > 0 )&&( Beta_K_Dot >= esp_tempW3 ))||(( Beta_MD < 0 )&&( Beta_K_Dot <= -esp_tempW3 ))) )
   {
        delta_beta2 = delta_beta2 ;
        Beta_Moment_Ctrl_Flg = 1 ;
    }
    else if ( (Beta_Moment_Ctrl_Flg==1) && ( esp_tempW8 >= 0 ) && ( delta_beta2 >= 0 ) )
    {
        delta_beta2 = delta_beta2 ;
        Beta_Moment_Ctrl_Flg = 1 ;
    }
    else
    {
        delta_beta2 = 0 ;
        Beta_Moment_Ctrl_Flg = 0 ;
    }

#if  __BANK_DETECT_ESC_CTRL_ENABLE == 1 

    if ( (FLAG_BANK_DETECTED==1) || (FLAG_BANK_SUSPECT==1) )
    {
        delta_beta2 = 0 ;
        Beta_Moment_Ctrl_Flg = 0 ;
    }
    else
    {
       ;
    } 
              
#endif 

    if ( Beta_MD >= 0 )
    {
        esp_tempW1 = 1 ;
    }
    else
    {
        esp_tempW1 = -1 ;
    }

    esp_tempW4= LCESP_s16FindRoadDependatGain(esp_mu, YAW_CONTORL_P_GAIN_BETA_H , YAW_CONTORL_P_GAIN_BETA_M, YAW_CONTORL_P_GAIN_BETA_L);

/*  Beta_Err :  '06.10.20 */

    esp_tempW6 = esp_tempW4*2 ;         /* P_gain * 2 */
    esp_tempW7 = LCESP_s16IInter2Point( delta_beta2, 0, esp_tempW4,
                                            esp_tempW5, esp_tempW6 );  /* Err, P_gain */

    esp_tempW4= esp_tempW7;
		

    	LCESP_vCalSpeedDepBetaGain();/*Beta P gain speed weight  - yaw_control_P_gain_beta2*/

		
    esp_tempW4 = (int16_t)( (((int32_t)gain_speed_beta_depend)*esp_tempW4)/100 );

    if( ROP_REQ_ACT==1 )
    {
/*        esp_tempW5= 100 ; */
        esp_tempW5= LCESP_s16IInter3Point( delta_yaw, S16_BETA_GAIN_DEP_DYAW_L, 50,
                                                  S16_BETA_GAIN_DEP_DYAW_M, 100,
                                                  S16_BETA_GAIN_DEP_DYAW_H, 100 );
    }
    else
    {
        esp_tempW5= LCESP_s16IInter3Point( delta_yaw, S16_BETA_GAIN_DEP_DYAW_L, S16_BETA_GAIN_DEP_DYAW_WEG_L,
                                                  S16_BETA_GAIN_DEP_DYAW_M, S16_BETA_GAIN_DEP_DYAW_WEG_M,
                                                  S16_BETA_GAIN_DEP_DYAW_H, S16_BETA_GAIN_DEP_DYAW_WEG_H );
    }
    /* esp_tempW4 : mu, speed  Pgain, esp_tempW5 : del_yaw weighting */
    esp_tempW4 = (int16_t)( (((int32_t)esp_tempW4)*esp_tempW5)/100 );

/********************************************************************************************/
    delta_moment_beta = (int16_t)( ((int32_t)delta_beta2*esp_tempW4)/10 )*esp_tempW1;
/*  Beta_M_Limit : '07.11.14  */

    esp_tempW3 = LCESP_s16FindRoadDependatGain(esp_mu, S16_BETA_M_LMT_Lspd_Hmu , S16_BETA_M_LMT_Lspd_Mmu, S16_BETA_M_LMT_Lspd_Lmu);
    esp_tempW4 = LCESP_s16FindRoadDependatGain(esp_mu, S16_BETA_M_LMT_Mspd_Hmu , S16_BETA_M_LMT_Mspd_Mmu, S16_BETA_M_LMT_Mspd_Lmu);
    esp_tempW5 = LCESP_s16FindRoadDependatGain(esp_mu, S16_BETA_M_LMT_Hspd_Hmu , S16_BETA_M_LMT_Hspd_Mmu, S16_BETA_M_LMT_Hspd_Lmu);

    esp_tempW6 = LCESP_s16IInter3Point( vrefk, S16_BETA_THRES_Low_Spd, esp_tempW3,
                                               S16_BETA_THRES_Med_Spd, esp_tempW4,
                                               S16_BETA_THRES_Hig_Spd, esp_tempW5 );

    if ( (McrAbs(delta_moment_beta)) > esp_tempW6 )
    {
            if ( delta_moment_beta < 0 )
            {
                delta_moment_beta = -esp_tempW6;
            }
            else
            {
                delta_moment_beta = esp_tempW6;
            }
    }
    else
    {
        ;
    }
     /* '06 TRC high speed Beta  */
    esp_tempW9 = LCESP_s16IInter3Point(vrefk, VREF_K_90_KPH , 100
                                            , VREF_K_100_KPH, 70
                                            , VREF_K_120_KPH, 1);
    if ( on_center_gain > 1 )
    {
        esp_tempW1 = on_center_gain;
    }
    else
    {
        esp_tempW1 = 1;
    }
    delta_moment_beta = (int16_t)( (((int32_t)delta_moment_beta)*esp_tempW9)/(100*esp_tempW1) );

}

#if (__CRAM == 1)
  
void LCCRAM_vCallControl(void)
{ 
    LCCRAM_vChkRequest(); 
}

void LCCRAM_vChkRequest(void)
{
        moment_cram_delv = (int16_t)(((int32_t)lds16espDelVcram)*10);                                                              
     
        if ((ldu1espDetectCRAM_FL==1)||(ldu1espDetectCRAM_FR==1))
        {
            
            if (yaw_sign > 0)
            { 

                moment_pre_front_cram = -moment_cram_delv ;   /* Positive --> FL_wheel */
                
                if(moment_pre_front_cram > S16_CRAM_IN_FRT_SLIP_LMT)
                {
                    moment_pre_front_cram = S16_CRAM_IN_FRT_SLIP_LMT;
                }
                else if(moment_pre_front_cram < 0)
                {
                    moment_pre_front_cram = 0;
                }
                else
                {
                    ;
                }                
                
            } 
            else 
            {  
                moment_pre_front_cram = moment_cram_delv ;   /* Negative --> FR_wheel*/
                
                if(moment_pre_front_cram > 0)
                {
                    moment_pre_front_cram = 0;
                }
                else if(moment_pre_front_cram < -S16_CRAM_IN_FRT_SLIP_LMT)
                {
                    moment_pre_front_cram = -S16_CRAM_IN_FRT_SLIP_LMT;
                }
                else
                {
                    ;
                }                     
            }  
        
        } 
        else 
        {
            moment_pre_front_cram = 0; 
        }   
        
        if (ldu1espDetectCRAM==1)   
        { 
            
	            moment_pre_rear_cram =  McrAbs(moment_cram_delv);   

				if(lcs16espCramCount < S16_CRAM_REAR_1ST_CYCLE_TIME)
				{
		            esp_tempW3 = S16_CRAM_OUT_REAR_SLIP_LMT_1ST;			
				}
				else							
				{
					esp_tempW3 = S16_CRAM_OUTSIDE_REAR_SLIP_LMT;     
  	            }

	            if(moment_pre_rear_cram > esp_tempW3)
	            {
	                moment_pre_rear_cram = esp_tempW3;
	            }
	            else if(moment_pre_rear_cram < 0)
	            {
	                moment_pre_rear_cram = 0;
	            }
	            else
	            {
	                ;
	            }              
            	
        } 
        else 
        {
            moment_pre_rear_cram = 0; 
        }               
 
}

#endif

void LCESP_vSetRearspeedDepMU(void)
{
    /***********High speed****************/
    rear_high_speed = LCESP_s16FindRoadDependatGain( esp_mu, S16_REAR_HMU_SPD_H,
                 S16_REAR_MMU_SPD_H, S16_REAR_LMU_SPD_H);
    /***********Med speed****************/
    rear_med2_speed = LCESP_s16FindRoadDependatGain( esp_mu, S16_REAR_HMU_SPD_M2,
                 S16_REAR_MMU_SPD_M2, S16_REAR_LMU_SPD_M2);

    rear_med1_speed = LCESP_s16FindRoadDependatGain( esp_mu, S16_REAR_HMU_SPD_M1,
                 S16_REAR_MMU_SPD_M1, S16_REAR_LMU_SPD_M1);
    /***********Low speed****************/
    rear_low_speed = LCESP_s16FindRoadDependatGain( esp_mu, S16_REAR_HMU_SPD_L,
                 S16_REAR_MMU_SPD_L, S16_REAR_LMU_SPD_L);
}
void LCESP_vCalDeltaMoment(void)
{

    /* #if ( __ROP == ENABLE )  '12/3/30 -> change a location

         LDROP_vCallDetection();
         LCROP_vCallControl();

    #endif  */

    LCESP_vCalDeltaBeta2();     /* del_beta calculation */

    esp_tempW1 = (int16_t)( (((int32_t)delta_yaw2)*yaw_control_P_gain_front)/10 );

    esp_tempW2 = (int16_t)( ((int32_t)yaw_error2_dot)*yaw_control_D_gain_front );

    esp_tempW3 = (int16_t)( (((int32_t)delta_yaw2)*yaw_control_P_gain_rear)/10 );

    esp_tempW4 = (int16_t)( ((int32_t)yaw_error2_dot)*yaw_control_D_gain_rear );
      #if __SMOOTH_REAR_DEL_M_GAIN
    esp_tempW3 = (int16_t)( (((int32_t)esp_tempW3)*lcesps16RearSpeedGain)/100 );
    esp_tempW4 = (int16_t)( (((int32_t)esp_tempW4)*lcesps16RearSpeedGain)/100 );  
      #endif

    delta_moment_q = (esp_tempW1 + esp_tempW2 + esp_tempW3 + esp_tempW4 + delta_moment_beta) ;

#if __ROP

     if ( ROP_REQ_ACT ==1 ) {

        esp_tempW7 = 1 ;

        if ( yaw_sign >= 0 )
        {
            if ( delta_moment_q <= 0 )      /* Oversteer */
            {
                delta_moment_q = delta_moment_q - moment_q_front_rop*esp_tempW7 ;
            }
            else                            /* Understeer */
            {
                delta_moment_q = -moment_q_front_rop*esp_tempW7 ;
            }
        }
        else
        {
            if ( delta_moment_q <= 0 )      /* Understeer */
            {
                delta_moment_q = moment_q_front_rop*esp_tempW7 ;
            }
            else                             /* Oversteer */
            {
                delta_moment_q = (delta_moment_q + (moment_q_front_rop*esp_tempW7)) ;
            }
        }
    }
    else
    {
        delta_moment_q = delta_moment_q  ;
    }
#else

	delta_moment_q = delta_moment_q  ;

#endif

    LCESP_DisableYaw();

    if ( yaw_sign >= 0 )   /* 06.07.20  */
    {
        if ( delta_moment_q <= 0 )      /* Oversteer */
        {
            moment_q_front = delta_moment_q/100;
            moment_q_rear = 0 ;
        }
        else
        {
            moment_q_front = 0 ;
            moment_q_rear = -delta_moment_q/100 ;
        }
    }
    else
    {
        if ( delta_moment_q <= 0 )      /* Understeer */
        {
            moment_q_front = 0 ;
            moment_q_rear = delta_moment_q/100 ;
        }
        else
        {
            moment_q_front = -delta_moment_q/100;
            moment_q_rear = 0 ;
        }
    }

	if((ROP_REQ_ACT==0)&&(MGH60_YAWC_OFFSET_UNDER_CTRL   ==1))	  
	{

/* -------------  08. SWD : YawC_Offset Control ------------- */

/* Step.1 : Del_Yaw_First Moment.. */

    if( delta_yaw_first  < u_delta_yaw_thres )
    {

#if	( __Ext_Under_Ctrl == 1 )	

    #if (__MGH60_ESC_IMPROVE_CONCEPT == 1) 	   
                                 
  	  if( EXTREME_UNDER_CONTROL ==1 )  /* Extreme Under */
  	  {		          
        if ( yaw_error_under_dot > 0 )    /*Understeer Inc.*/
        {
         esp_tempW1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_EU_REAR_P_OUT_INC_H, S16_EU_REAR_P_OUT_INC_M, S16_EU_REAR_P_OUT_INC_L);
		}
        else                        /*Understeer Dec.*/
		{
         esp_tempW1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_EU_REAR_P_OUT_DEC_H, S16_EU_REAR_P_OUT_DEC_M, S16_EU_REAR_P_OUT_DEC_L);
        }  
      }
      else                        /* Under_Control_Under */
      { 	     
        if ( yaw_error_under_dot > 0 )    /*Understeer Inc.*/
        {
         esp_tempW1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_P_GAIN_R_US_OUT_INC_H, S16_YC_P_GAIN_R_US_OUT_INC, S16_YC_P_GAIN_R_US_OUT_INC_L);
		} 
        else                        /*Understeer Dec.*/
        { 
         esp_tempW1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_P_GAIN_R_US_OUT_H, S16_YC_P_GAIN_R_US_OUT, S16_YC_P_GAIN_R_US_OUT_L);
        }   
      } 
  #else  
		
  	  if( EXTREME_UNDER_CONTROL ==1 )  /* Extreme Under */
  	  {		               
         esp_tempW1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_EU_REAR_P_OUT_DEC_H, S16_EU_REAR_P_OUT_DEC_M, S16_EU_REAR_P_OUT_DEC_L);
  	  }
      else                        /* Under_Control_Under */
      { 
         esp_tempW1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_P_GAIN_R_US_OUT_H, S16_YC_P_GAIN_R_US_OUT, S16_YC_P_GAIN_R_US_OUT_L);
      } 
  #endif    
    
      if( EXTREME_UNDER_CONTROL ==1 )  /* Extreme Under */
  	  {		               
        if ( yaw_error_under_dot > 0 )
        {
            esp_tempW2 = -LCESP_s16FindRoadDependatGain( esp_mu, S16_EU_REAR_D_OUT_INC_H, S16_EU_REAR_D_OUT_INC_M, S16_EU_REAR_D_OUT_INC_L);
        }
        else
        {
            esp_tempW2 = -LCESP_s16FindRoadDependatGain( esp_mu, S16_EU_REAR_D_OUT_DEC_H, S16_EU_REAR_D_OUT_DEC_M, S16_EU_REAR_D_OUT_DEC_L);
        
        }      	    
  	  }
      else                        /* Under_Control_Under */
      {  
        if ( yaw_error_under_dot > 0 )
        {
            esp_tempW2 = -LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_D_GAIN_R_US_OUT_INC_H, S16_YC_D_GAIN_R_US_OUT_INC, S16_YC_D_GAIN_R_US_OUT_INC_L);
        }
        else
        {
            esp_tempW2 = -LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_D_GAIN_R_US_OUT_DEC_H, S16_YC_D_GAIN_R_US_OUT_DEC, S16_YC_D_GAIN_R_US_OUT_DEC_L);
            
        }    
      }
            
 #else     
    
        #if (__MGH60_ESC_IMPROVE_CONCEPT == 1) 	   
        
        if ( yaw_error_under_dot > 0 )    /*Understeer Inc.*/
            	  {		   
         esp_tempW1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_P_GAIN_R_US_OUT_INC_H, S16_YC_P_GAIN_R_US_OUT_INC, S16_YC_P_GAIN_R_US_OUT_INC_L);
            		}
        else                        /*Understeer Dec.*/
            		{
         esp_tempW1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_P_GAIN_R_US_OUT_H, S16_YC_P_GAIN_R_US_OUT, S16_YC_P_GAIN_R_US_OUT_L);
            		} 
            		
        #else 
            
         esp_tempW1 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_P_GAIN_R_US_OUT_H, S16_YC_P_GAIN_R_US_OUT, S16_YC_P_GAIN_R_US_OUT_L);

        #endif   
            
        if ( yaw_error_under_dot > 0 )
        {
            esp_tempW2 = -LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_D_GAIN_R_US_OUT_INC_H, S16_YC_D_GAIN_R_US_OUT_INC, S16_YC_D_GAIN_R_US_OUT_INC_L);
        }
        else
        {
            esp_tempW2 = -LCESP_s16FindRoadDependatGain( esp_mu, S16_YC_D_GAIN_R_US_OUT_DEC_H, S16_YC_D_GAIN_R_US_OUT_DEC, S16_YC_D_GAIN_R_US_OUT_DEC_L);
        }
 #endif 
               
        /* High-mu Weg  */  
        esp_tempW3 = LCESP_s16IInter4Point(vrefk,   S16_REAR_HMU_SPD_L  , S16_UNDER_CTRL_PD_WEG_HMU_LSP,
                                                    S16_REAR_HMU_SPD_M1 , S16_UNDER_CTRL_PD_WEG_HMU_MSP,
                                                    S16_REAR_HMU_SPD_M2 , S16_UNDER_CTRL_PD_WEG_HMU_MSP,
                                                    S16_REAR_HMU_SPD_H  , S16_UNDER_CTRL_PD_WEG_HMU_HSP );         
      
        /* Med-mu */ 
        esp_tempW4 = LCESP_s16IInter4Point(vrefk,   S16_REAR_MMU_SPD_L  , S16_UNDER_CTRL_PD_WEG_MMU_LSP,
                                                    S16_REAR_MMU_SPD_M1 , S16_UNDER_CTRL_PD_WEG_MMU_MSP,
                                                    S16_REAR_MMU_SPD_M2 , S16_UNDER_CTRL_PD_WEG_MMU_MSP,
                                                    S16_REAR_MMU_SPD_H  , S16_UNDER_CTRL_PD_WEG_MMU_HSP );                                                       

        /*  Low-mu */                                                  
        esp_tempW5 = LCESP_s16IInter4Point(vrefk,   S16_REAR_LMU_SPD_L  , S16_UNDER_CTRL_PD_WEG_LMU_LSP,
                                                    S16_REAR_LMU_SPD_M1 , S16_UNDER_CTRL_PD_WEG_LMU_MSP,
                                                    S16_REAR_LMU_SPD_M2 , S16_UNDER_CTRL_PD_WEG_LMU_MSP,
                                                    S16_REAR_LMU_SPD_H  , S16_UNDER_CTRL_PD_WEG_LMU_HSP );                                                      
                                                  
        esp_tempW6 = LCESP_s16FindRoadDependatGain( esp_mu,   esp_tempW3,  esp_tempW4,  esp_tempW5 ); 
   
        esp_tempW1 = (int16_t)( (((int32_t)esp_tempW1)*esp_tempW6)/100 );
     
        esp_tempW2 = (int16_t)( (((int32_t)esp_tempW2)*esp_tempW6)/100 );

        esp_tempW3 = (int16_t)( (((int32_t)delta_yaw_first)*esp_tempW1)/10 );
    
        esp_tempW4 = (int16_t)( ((int32_t)yaw_error_under_dot)*esp_tempW2 );
        
        delta_moment_q_under = ( esp_tempW3 + esp_tempW4 ) ;   
        
        if( delta_moment_q_under  > 0 )
        {
            delta_moment_q_under = 0 ;
        }
        else
        {
            ;
        }
    }
    else
    {
        delta_moment_q_under = 0 ;   
    }        
                                
    ldesps16YawcOffset = McrAbs(delta_moment_q_under) ;     

/* Step.3 : Moment_Determination  */

	#if ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
    if ( ( lsespu1AHBGEN3MpresBrkOn == 0 ) && ((YAW_CHANGE_LIMIT_ACT==1)||(nosign_delta_yaw!=nosign_delta_yaw_first))&& ( VDC_DISABLE_FLG == 0 ))  		 
	#else
    if ( ( MPRESS_BRAKE_ON == 0 ) && ((YAW_CHANGE_LIMIT_ACT==1)||(nosign_delta_yaw!=nosign_delta_yaw_first))&& ( VDC_DISABLE_FLG == 0 ))   // ABS_fz ???      
	#endif
    {  
        lcesps16Yaw3rdMoment = ldesps16YawcOffset ;         
        
        /*if ( lcesps16Yaw3rdMoment < 0 )
        {
            lcesps16Yaw3rdMoment = 0; 
        }        
        else
        {
           ;
        }*/      
    }
    else
    {   
        lcesps16Yaw3rdMoment = 0 ;     
    }       
     
/* Step.4 : Lmt 설정 */

    /* High mu : Alpha_R, MQ_under 100% */
    esp_tempW1 = LCESP_s16IInter4Point(vrefk, S16_REAR_HMU_SPD_L ,  S16_UNDER_MQ_LMT_HMU_LSP,
                                              S16_REAR_HMU_SPD_M1 , S16_UNDER_MQ_LMT_HMU_MSP,
                                              S16_REAR_HMU_SPD_M2 , S16_UNDER_MQ_LMT_HMU_MSP,
                                              S16_REAR_HMU_SPD_H ,  S16_UNDER_MQ_LMT_HMU_HSP );         
     
    /* Med mu : Alpha_R, MQ_under 100% */                                       
    esp_tempW2 = LCESP_s16IInter4Point(vrefk, S16_REAR_MMU_SPD_L ,  S16_UNDER_MQ_LMT_MMU_LSP,
                                              S16_REAR_MMU_SPD_M1 , S16_UNDER_MQ_LMT_MMU_MSP,
                                              S16_REAR_MMU_SPD_M2 , S16_UNDER_MQ_LMT_MMU_MSP,
                                              S16_REAR_MMU_SPD_H ,  S16_UNDER_MQ_LMT_MMU_HSP );                                   
                          
    /* Low mu : Alpha_R, MQ_under 100% */                                  
    esp_tempW3 = LCESP_s16IInter4Point(vrefk, S16_REAR_LMU_SPD_L ,  S16_UNDER_MQ_LMT_LMU_LSP,
                                              S16_REAR_LMU_SPD_M1 , S16_UNDER_MQ_LMT_LMU_MSP,
                                              S16_REAR_LMU_SPD_M2 , S16_UNDER_MQ_LMT_LMU_MSP,
                                              S16_REAR_LMU_SPD_H ,  S16_UNDER_MQ_LMT_LMU_HSP );                                      
      
/*    esp_tempW4  = LCESP_s16FindRoadDependatGain( esp_mu,   esp_tempW1,  esp_tempW2,  esp_tempW3 );    */                                        
    moment_under_limit_Q  = LCESP_s16FindRoadDependatGain( esp_mu,   esp_tempW1,  esp_tempW2,  esp_tempW3 );    

#if	( __Ext_Under_Ctrl == 1 )	

	  if( EXTREME_UNDER_CONTROL ==1 )
	  {		  
                                 
    	esp_tempW1  = LCESP_s16FindRoadDependatGain( esp_mu,   S16_EXT_UND_MQ_LMT_WEG_Hmu,  S16_EXT_UND_MQ_LMT_WEG_Mmu,  S16_EXT_UND_MQ_LMT_WEG_Lmu );    	  	
	  	moment_under_limit_Q = (int16_t)( (((int32_t)moment_under_limit_Q)*esp_tempW1)/100 );            
		}
		else
		{
			  ;
		} 
		
#endif	  
        
    if ( lcesps16Yaw3rdMoment > moment_under_limit_Q )
    {
        lcesps16Yaw3rdMoment = moment_under_limit_Q ;
    }        
    else
    {
        ;
    }        
     
/* Step.5 : Entrance condition... */

    /*  High_mu  */
    esp_tempW1 = LCESP_s16IInter4Point(vrefk, S16_REAR_HMU_SPD_L ,  S16_UNDER_ENT_DEL_YAW_HMU_LSP,
                                              S16_REAR_HMU_SPD_M1 , S16_UNDER_ENT_DEL_YAW_HMU_MSP,
                                              S16_REAR_HMU_SPD_M2 , S16_UNDER_ENT_DEL_YAW_HMU_MSP,
                                              S16_REAR_HMU_SPD_H ,  S16_UNDER_ENT_DEL_YAW_HMU_HSP );  
                                              
    /* Med mu  */                                       
    esp_tempW2 = LCESP_s16IInter4Point(vrefk, S16_REAR_MMU_SPD_L ,  S16_UNDER_ENT_DEL_YAW_MMU_LSP,
                                              S16_REAR_MMU_SPD_M1 , S16_UNDER_ENT_DEL_YAW_MMU_MSP,
                                              S16_REAR_MMU_SPD_M2 , S16_UNDER_ENT_DEL_YAW_MMU_MSP,
                                              S16_REAR_MMU_SPD_H ,  S16_UNDER_ENT_DEL_YAW_MMU_HSP );                                   
                          
    /* Low mu   */                                  
    esp_tempW3 = LCESP_s16IInter4Point(vrefk, S16_REAR_LMU_SPD_L ,  S16_UNDER_ENT_DEL_YAW_LMU_LSP,
                                              S16_REAR_LMU_SPD_M1 , S16_UNDER_ENT_DEL_YAW_LMU_MSP,
                                              S16_REAR_LMU_SPD_M2 , S16_UNDER_ENT_DEL_YAW_LMU_MSP,
                                              S16_REAR_LMU_SPD_H ,  S16_UNDER_ENT_DEL_YAW_LMU_HSP );     
                                              
    esp_tempW4  = LCESP_s16FindRoadDependatGain( esp_mu,   esp_tempW1,  esp_tempW2,  esp_tempW3 ); 
    
    /*  High_mu  */
    esp_tempW6 = LCESP_s16IInter4Point(vrefk, S16_REAR_HMU_SPD_L ,  S16_UNDER_EXT_DEL_YAW_HMU_LSP,
                                              S16_REAR_HMU_SPD_M1 , S16_UNDER_EXT_DEL_YAW_HMU_MSP,
                                              S16_REAR_HMU_SPD_M2 , S16_UNDER_EXT_DEL_YAW_HMU_MSP,
                                              S16_REAR_HMU_SPD_H ,  S16_UNDER_EXT_DEL_YAW_HMU_HSP );  
                                              
    /* Med mu  */                                       
    esp_tempW7 = LCESP_s16IInter4Point(vrefk, S16_REAR_MMU_SPD_L ,  S16_UNDER_EXT_DEL_YAW_MMU_LSP,
                                              S16_REAR_MMU_SPD_M1 , S16_UNDER_EXT_DEL_YAW_MMU_MSP,
                                              S16_REAR_MMU_SPD_M2 , S16_UNDER_EXT_DEL_YAW_MMU_MSP,
                                              S16_REAR_MMU_SPD_H ,  S16_UNDER_EXT_DEL_YAW_MMU_HSP );                                   
                          
    /* Low mu   */                                  
    esp_tempW8 = LCESP_s16IInter4Point(vrefk, S16_REAR_LMU_SPD_L ,  S16_UNDER_EXT_DEL_YAW_LMU_LSP,
                                              S16_REAR_LMU_SPD_M1 , S16_UNDER_EXT_DEL_YAW_LMU_MSP,
                                              S16_REAR_LMU_SPD_M2 , S16_UNDER_EXT_DEL_YAW_LMU_MSP,
                                              S16_REAR_LMU_SPD_H ,  S16_UNDER_EXT_DEL_YAW_LMU_HSP );     
                                              
    esp_tempW9  = LCESP_s16FindRoadDependatGain( esp_mu,   esp_tempW6,  esp_tempW7,  esp_tempW8 );     
     
#if	( __Ext_Under_Ctrl == 1 ) 

    if( EXTREME_UNDER_CONTROL ==1 )
    { 
    	esp_tempW4 =	esp_tempW4 + 600 ; 
			esp_tempW9 =  esp_tempW9 + 600 ; 
		}
		
    if( (EXTREME_UNDER_CTRL_FADE_OUT ==1 )||(lcu1US2WHControlFlag==1))
    {      
  	
	    if ( (lcu1espActive3rdFlg==0) && ( moment_q_front >= -3 ) 
	          && ( vrefk > S16_UNDER_CTRL_ENTER_VEL ) && ( (McrAbs(lcesps16Yaw3rdMoment)) >= S16_UNDER_CTRL_ENTER_MQ ) && (delta_yaw < esp_tempW4) )
	   {
	        lcu1espActive3rdFlg = 1 ; 
	    }
	    else if ( (lcu1espActive3rdFlg==1) && ( moment_q_front >= -9 ) 
	              && ( vrefk > S16_UNDER_CTRL_EXIT_VEL ) && ( (McrAbs(lcesps16Yaw3rdMoment)) >= S16_UNDER_CTRL_EXIT_MQ ) && (delta_yaw < esp_tempW9) )
	    {
	        lcu1espActive3rdFlg = 1 ;      
	    }
	    else
	    {
	        lcesps16Yaw3rdMoment = 0 ;
	        lcu1espActive3rdFlg = 0 ;
	    }  	
	    
	  }
	  else
  	{
  		
	    if ( (SLIP_CONTROL_NEED_FL==0) && (SLIP_CONTROL_NEED_FR==0) && (lcu1espActive3rdFlg==0) && ( moment_q_front >= -3 ) 
	          && ( vrefk > S16_UNDER_CTRL_ENTER_VEL ) && ( (McrAbs(lcesps16Yaw3rdMoment)) >= S16_UNDER_CTRL_ENTER_MQ ) && (delta_yaw < esp_tempW4) )
	   {
	        lcu1espActive3rdFlg = 1 ; 
	    }
	    else if ( (SLIP_CONTROL_NEED_FL==0) && (SLIP_CONTROL_NEED_FR==0) && (lcu1espActive3rdFlg==1) && ( moment_q_front >= -9 ) 
	              && ( vrefk > S16_UNDER_CTRL_EXIT_VEL ) && ( (McrAbs(lcesps16Yaw3rdMoment)) >= S16_UNDER_CTRL_EXIT_MQ ) && (delta_yaw < esp_tempW9) )
	    {
	        lcu1espActive3rdFlg = 1 ;      
	    }
	    else
	    {
	        lcesps16Yaw3rdMoment = 0 ;
	        lcu1espActive3rdFlg = 0 ;
	    }  		
  		
  	}
  	
#else  
     	
    if(lcu1US2WHControlFlag==1)
    {      
  	
	    if ( (lcu1espActive3rdFlg==0) && ( moment_q_front >= -3 ) 
	          && ( vrefk > S16_UNDER_CTRL_ENTER_VEL ) && ( (McrAbs(lcesps16Yaw3rdMoment)) >= S16_UNDER_CTRL_ENTER_MQ ) && (delta_yaw < esp_tempW4) )
	   {
	        lcu1espActive3rdFlg = 1 ; 
	    }
	    else if ( (lcu1espActive3rdFlg==1) && ( moment_q_front >= -9 ) 
	              && ( vrefk > S16_UNDER_CTRL_EXIT_VEL ) && ( (McrAbs(lcesps16Yaw3rdMoment)) >= S16_UNDER_CTRL_EXIT_MQ ) && (delta_yaw < esp_tempW9) )
	    {
	        lcu1espActive3rdFlg = 1 ;      
	    }
	    else
	    {
	        lcesps16Yaw3rdMoment = 0 ;
	        lcu1espActive3rdFlg = 0 ;
	    }  	
	    
	}
	else
  	{     
	    if ( (SLIP_CONTROL_NEED_FL==0) && (SLIP_CONTROL_NEED_FR==0) && (lcu1espActive3rdFlg==0) && ( moment_q_front >= -3 ) 
	          && ( vrefk > S16_UNDER_CTRL_ENTER_VEL ) && ( (McrAbs(lcesps16Yaw3rdMoment)) >= S16_UNDER_CTRL_ENTER_MQ ) && (delta_yaw < esp_tempW4) )
	    {
	        lcu1espActive3rdFlg = 1 ; 
	    }
	    else if ( (SLIP_CONTROL_NEED_FL==0) && (SLIP_CONTROL_NEED_FR==0) && (lcu1espActive3rdFlg==1) && ( moment_q_front >= -9 ) 
	              && ( vrefk > S16_UNDER_CTRL_EXIT_VEL ) && ( (McrAbs(lcesps16Yaw3rdMoment)) >= S16_UNDER_CTRL_EXIT_MQ ) && (delta_yaw < esp_tempW9) )
	    {
	        lcu1espActive3rdFlg = 1 ;      
	    }
	    else
	    {
	        lcesps16Yaw3rdMoment = 0 ;
	        lcu1espActive3rdFlg = 0 ;
	    }
	}
#endif  


/******** 11 SWD : Del_M_OS & Del_M_US monitoring ********/
 
        if((delta_moment_q<0)&&(yaw_sign >= 0))
        {
            delta_moment_q_os = -(McrAbs(delta_moment_q)) ; 

        }
        else if((delta_moment_q >= 0)&&(yaw_sign < 0))
        {
            delta_moment_q_os = -(McrAbs(delta_moment_q)) ; 
        }
        else
        {
            delta_moment_q_os = 0 ; 
        }
        
/* 11 SWD : Limit U/S Moment_Q */        

        if((delta_moment_q > 0)&&(yaw_sign >= 0))
        {
        	  if (delta_moment_q > moment_under_limit_Q)
        	  {
        			delta_moment_q = moment_under_limit_Q ;
        		}
        		
        }
        else if((delta_moment_q < 0)&&(yaw_sign < 0))
        {

        	  if (delta_moment_q < -moment_under_limit_Q)
        	  {
        			delta_moment_q = -moment_under_limit_Q ;
        		}
        	        	
        }
        else
        {
        	;
        } 

/******** 11 SWD : Del_M_US = Del_M_US + K*Del_M_OS   ********/        
        
        if( lcu1espActive3rdFlg == 1 )
        {
        	  
        	  esp_tempW1 = McrAbs(delta_moment_q_os) ;
        	
    				esp_tempW2 = (int16_t)( (((int32_t)esp_tempW1)*S16_DEC_OS_M_WEG_ON_US_DELM)/100 );
            	
            lcesps16Yaw3rdMoment = lcesps16Yaw3rdMoment - esp_tempW2 ; 
            
            if(lcesps16Yaw3rdMoment<0)
		        {
		            lcesps16Yaw3rdMoment = 0 ;  
		        }
		        else
	        	{
	        		;
	        	} 
	        	
        } 
        else
        {
             ; 
        }        

    if ( lcu1espActive3rdFlg == 1 )
    {
        if ( yaw_sign < 0 )    
        {     
        	lcesps16Yaw3rdMoment = -lcesps16Yaw3rdMoment;
        	
        	    if ( lcesps16Yaw3rdMoment < delta_moment_q )  /* Comparison of Tracking control & YawC_Offset Under.    */
                {
                    delta_moment_q = lcesps16Yaw3rdMoment ;   /* Maximun under_control */
                }        
                else
                {
                    ;
                }       
        }
        else
        {
        	    if ( lcesps16Yaw3rdMoment > delta_moment_q )  /* Comparison of Tracking control & YawC_Offset Under.    */
                {
                    delta_moment_q = lcesps16Yaw3rdMoment ;   /* Maximun under_control */
                }        
                else
                {
                    ;
                }       
        }        
    }
    else
    {   
        ;
    }        
 
#if (__MGH60_ESC_IMPROVE_CONCEPT == 1)   /* 11 NZ : Del_M_OS weighting of U/S situation */ 
    if((yaw_error < 0) && (((delta_moment_q<0)&&(yaw_sign >= 0))||((delta_moment_q >= 0)&&(yaw_sign < 0))))   
    {	

      esp_tempW1 =LCESP_s16FindRoadDependatGain( esp_mu, S16_DELM_HM_SPD_L_GAIN,
                                  S16_DELM_MM_SPD_L_GAIN, S16_DELM_LM_SPD_L_GAIN);
  
      esp_tempW2 =LCESP_s16FindRoadDependatGain( esp_mu, S16_DELM_HM_SPD_M_GAIN,
                                  S16_DELM_MM_SPD_M_GAIN, S16_DELM_LM_SPD_M_GAIN);
  
      esp_tempW3 =LCESP_s16FindRoadDependatGain( esp_mu, S16_DELM_HM_SPD_H_GAIN,
                                  S16_DELM_MM_SPD_H_GAIN, S16_DELM_LM_SPD_H_GAIN); 
                                   
      esp_tempW0 = LCESP_s16IInter4Point(vrefk, rear_low_speed ,  esp_tempW1,
                                                rear_med1_speed , esp_tempW2,
                                                rear_med2_speed , esp_tempW2,
                                                rear_high_speed , esp_tempW3 );     
           
      delta_moment_q = (int16_t)( (((int32_t)delta_moment_q)*esp_tempW0)/100 );  
      
    }
    else
    {
      ;
    }

#endif

/* 11 SWD : Del_M_OS & Del_M_US monitoring */  
 
            delta_moment_q_us = McrAbs(delta_moment_q_under) ;  
        
/* ----------------------------------------------------------- */    
	}
	else
	{
		;
	}

}

//#if(DEL_M_IMPROVEMENT_CONCEPT)
void LCESP_vCalEnterCondition(void)
{
	if(ABS_fz == 1)
	{
		  /*FRONT ENTER CONDITION*/
    esp_tempW11 = LCESP_s16IInter3Point(vref5, S16_DEL_M_L_SPEED, (int16_t)U8_DEL_M_THR_ENTER_FRONT_HM_LS_ABS, 
                                               S16_DEL_M_M_SPEED, (int16_t)U8_DEL_M_THR_ENTER_FRONT_HM_MS_ABS, 
                                               S16_DEL_M_H_SPEED, (int16_t)U8_DEL_M_THR_ENTER_FRONT_HM_HS_ABS);
                                          
    esp_tempW12 = LCESP_s16IInter3Point(vref5, S16_DEL_M_L_SPEED, (int16_t)U8_DEL_M_THR_ENTER_FRONT_MM_LS_ABS,
                                               S16_DEL_M_M_SPEED, (int16_t)U8_DEL_M_THR_ENTER_FRONT_MM_MS_ABS,
                                               S16_DEL_M_H_SPEED, (int16_t)U8_DEL_M_THR_ENTER_FRONT_MM_HS_ABS);
                                          
    esp_tempW13 = LCESP_s16IInter3Point(vref5, S16_DEL_M_L_SPEED, (int16_t)U8_DEL_M_THR_ENTER_FRONT_LM_LS_ABS,
                                               S16_DEL_M_M_SPEED, (int16_t)U8_DEL_M_THR_ENTER_FRONT_LM_MS_ABS,
                                               S16_DEL_M_H_SPEED, (int16_t)U8_DEL_M_THR_ENTER_FRONT_LM_HS_ABS);

    q_front_enter_gain = -LCESP_s16FindRoadDependatGain(esp_mu, esp_tempW11, esp_tempW12, esp_tempW13);
  
  /*REAR ENTER CONDITION*/  
    esp_tempW21 = LCESP_s16IInter3Point(vref5, S16_DEL_M_L_SPEED, (int16_t)U8_DEL_M_THR_ENTER_REAR_HM_LS_ABS, 
                                               S16_DEL_M_M_SPEED, (int16_t)U8_DEL_M_THR_ENTER_REAR_HM_MS_ABS, 
                                               S16_DEL_M_H_SPEED, (int16_t)U8_DEL_M_THR_ENTER_REAR_HM_HS_ABS);
                                          
    esp_tempW22 = LCESP_s16IInter3Point(vref5, S16_DEL_M_L_SPEED, (int16_t)U8_DEL_M_THR_ENTER_REAR_MM_LS_ABS,
                                               S16_DEL_M_M_SPEED, (int16_t)U8_DEL_M_THR_ENTER_REAR_MM_MS_ABS,
                                               S16_DEL_M_H_SPEED, (int16_t)U8_DEL_M_THR_ENTER_REAR_MM_HS_ABS);
                                          
    esp_tempW23 = LCESP_s16IInter3Point(vref5, S16_DEL_M_L_SPEED, (int16_t)U8_DEL_M_THR_ENTER_REAR_LM_LS_ABS,
                                               S16_DEL_M_M_SPEED, (int16_t)U8_DEL_M_THR_ENTER_REAR_LM_MS_ABS,
                                               S16_DEL_M_H_SPEED, (int16_t)U8_DEL_M_THR_ENTER_REAR_LM_HS_ABS);

    q_rear_enter_gain = -LCESP_s16FindRoadDependatGain(esp_mu, esp_tempW21, esp_tempW22, esp_tempW23);
    
    if(q_rear_enter_gain>=q_rear_enter_gain_limit)
    {
    	q_rear_enter_gain = q_rear_enter_gain_limit;    	
    }
    else
    {
    	q_rear_enter_gain = q_rear_enter_gain;
    }
	}
	else
	{
	/*FRONT ENTER CONDITION*/
    esp_tempW11 = LCESP_s16IInter3Point(vref5, S16_DEL_M_L_SPEED, (int16_t)U8_DEL_M_THR_ENTER_FRONT_HM_LS, 
                                               S16_DEL_M_M_SPEED, (int16_t)U8_DEL_M_THR_ENTER_FRONT_HM_MS, 
                                               S16_DEL_M_H_SPEED, (int16_t)U8_DEL_M_THR_ENTER_FRONT_HM_HS);
                                          
    esp_tempW12 = LCESP_s16IInter3Point(vref5, S16_DEL_M_L_SPEED, (int16_t)U8_DEL_M_THR_ENTER_FRONT_MM_LS,
                                               S16_DEL_M_M_SPEED, (int16_t)U8_DEL_M_THR_ENTER_FRONT_MM_MS,
                                               S16_DEL_M_H_SPEED, (int16_t)U8_DEL_M_THR_ENTER_FRONT_MM_HS);
                                          
    esp_tempW13 = LCESP_s16IInter3Point(vref5, S16_DEL_M_L_SPEED, (int16_t)U8_DEL_M_THR_ENTER_FRONT_LM_LS,
                                               S16_DEL_M_M_SPEED, (int16_t)U8_DEL_M_THR_ENTER_FRONT_LM_MS,
                                               S16_DEL_M_H_SPEED, (int16_t)U8_DEL_M_THR_ENTER_FRONT_LM_HS);

    q_front_enter_gain = -LCESP_s16FindRoadDependatGain(esp_mu, esp_tempW11, esp_tempW12, esp_tempW13);

  /*REAR ENTER CONDITION*/  
    esp_tempW21 = LCESP_s16IInter3Point(vref5, S16_DEL_M_L_SPEED, (int16_t)U8_DEL_M_THR_ENTER_REAR_HM_LS, 
                                               S16_DEL_M_M_SPEED, (int16_t)U8_DEL_M_THR_ENTER_REAR_HM_MS, 
                                               S16_DEL_M_H_SPEED, (int16_t)U8_DEL_M_THR_ENTER_REAR_HM_HS);
                                          
    esp_tempW22 = LCESP_s16IInter3Point(vref5, S16_DEL_M_L_SPEED, (int16_t)U8_DEL_M_THR_ENTER_REAR_MM_LS,
                                               S16_DEL_M_M_SPEED, (int16_t)U8_DEL_M_THR_ENTER_REAR_MM_MS,
                                               S16_DEL_M_H_SPEED, (int16_t)U8_DEL_M_THR_ENTER_REAR_MM_HS);
                                          
    esp_tempW23 = LCESP_s16IInter3Point(vref5, S16_DEL_M_L_SPEED, (int16_t)U8_DEL_M_THR_ENTER_REAR_LM_LS,
                                               S16_DEL_M_M_SPEED, (int16_t)U8_DEL_M_THR_ENTER_REAR_LM_MS,
                                               S16_DEL_M_H_SPEED, (int16_t)U8_DEL_M_THR_ENTER_REAR_LM_HS);

    q_rear_enter_gain = -LCESP_s16FindRoadDependatGain(esp_mu, esp_tempW21, esp_tempW22, esp_tempW23);
    
    if(q_rear_enter_gain>=q_rear_enter_gain_limit)
    {
    	q_rear_enter_gain = q_rear_enter_gain_limit;    	
    }
    else
    {
    	q_rear_enter_gain = q_rear_enter_gain;
    }
	}
    if(q_front_enter_gain>=q_front_enter_gain_limit)
    {
    	q_front_enter_gain = q_front_enter_gain_limit;
    }
    else
    {
    	q_front_enter_gain = q_front_enter_gain;
    }
   
}
/*
#else
void LCESP_vCalEnterCondition(void)
{
            // front gain 
    if( esp_mu <= lsesps16MuMedHigh )
    {
        tempW6 = 0;
    }
    else if ( esp_mu <lsesps16MuHigh )
    {
        tempW6 = (int16_t)( (((int32_t)(esp_mu-lsesps16MuMedHigh))*(-5))/(lsesps16MuHigh-lsesps16MuMedHigh) );
    }
    else
    {
        tempW6 = -5;
    }

    if ( det_wstr_dot >= 1000 )
    {
        esp_tempW1 = 0;                     // front hysteresis gain
    }
    else if ( det_wstr_dot > 300 )
    {
        esp_tempW1 = (int16_t)( (((int32_t)(det_wstr_dot-1000))*10)/(-700) );
    }
    else
    {
        esp_tempW1 = 10;

    }
    esp_tempW1 = LCESP_s16IInter2Point( det_abs_wstr, 450, esp_tempW1, 900, 0 );
    esp_tempW4 = (int16_t)( (((int32_t)tempW6)*esp_tempW1)/10 );

// 06 SWD 
    esp_tempW6 = vrefm;
    if( esp_tempW6 <= 83 )  esp_tempW6 = 83;                    //  8.3*3.6 = 30 kph  

    //*  0.1g_yaw = 0.2g *562 / vrefm 
    esp_tempW7 = (int16_t)( (((int32_t)LAT_0G1G)*562)/esp_tempW6 );		//  r : 0.01 , vrefm >= 2.7(*3.6=9.7kph) 

    //*  0.2g_yaw = 0.2g *562 / vrefm                                        
    esp_tempW8 = (int16_t)( (((int32_t)LAT_0G2G)*562)/esp_tempW6 );   //  r : 0.01 , vrefm >= 2.7(*3.6=9.7kph),  100kph : 4'/s  

    esp_tempW9=McrAbs(yaw_out);
    esp_tempW9 = LCESP_s16IInter2Point( esp_tempW9 , esp_tempW7, 10, esp_tempW8, 0 );

    esp_tempW4 = (int16_t)( (((int32_t)esp_tempW4)*esp_tempW9)/10 );

    if((ABS_fz==1)||(FLAG_DETECT_DRIFT==1))
    {
        esp_tempW0 = SLIP_TARGET_THRESHOLD;
    }
    else
    {
        esp_tempW0 = SLIP_TARGET_THRESHOLD + esp_tempW4;
    }

    q_front_enter_gain = esp_tempW0 ; // 06 SWD, for Circle enter_condition, esp_tempW0;  
    
#if (__MGH60_ESC_IMPROVE_CONCEPT == 1)
       
        esp_tempW9 = SLIP_TARGET_THRESHOLD ;
        esp_tempW8 = SLIP_TARGET_THRESHOLD + S16_YAW_CTRL_REAR_ENTER_MM ;
        esp_tempW7 = SLIP_TARGET_THRESHOLD + S16_YAW_CTRL_REAR_ENTER_LM ;
     
        q_rear_enter_gain = LCESP_s16FindRoadDependatGain(esp_mu,esp_tempW9, esp_tempW8,esp_tempW7 );

#else       
        q_rear_enter_gain = SLIP_TARGET_THRESHOLD ;

#endif     
    
}
#endif
*/

//#if(DEL_M_IMPROVEMENT_CONCEPT)
void LCESP_vCalExitCondition(void)
{
	if(ABS_fz == 1)
	{
		  /*FRONT EXIT CONDITION*/
    esp_tempW11 = LCESP_s16IInter3Point(vref5, S16_DEL_M_L_SPEED, (int16_t)U8_DEL_M_THR_EXIT_FRONT_HM_LS_ABS, 
                                               S16_DEL_M_M_SPEED, (int16_t)U8_DEL_M_THR_EXIT_FRONT_HM_MS_ABS, 
                                               S16_DEL_M_H_SPEED, (int16_t)U8_DEL_M_THR_EXIT_FRONT_HM_HS_ABS);
                                          
    esp_tempW12 = LCESP_s16IInter3Point(vref5, S16_DEL_M_L_SPEED, (int16_t)U8_DEL_M_THR_EXIT_FRONT_MM_LS_ABS,
                                               S16_DEL_M_M_SPEED, (int16_t)U8_DEL_M_THR_EXIT_FRONT_MM_MS_ABS,
                                               S16_DEL_M_H_SPEED, (int16_t)U8_DEL_M_THR_EXIT_FRONT_MM_HS_ABS);
                                          
    esp_tempW13 = LCESP_s16IInter3Point(vref5, S16_DEL_M_L_SPEED, (int16_t)U8_DEL_M_THR_EXIT_FRONT_LM_LS_ABS,
                                               S16_DEL_M_M_SPEED, (int16_t)U8_DEL_M_THR_EXIT_FRONT_LM_MS_ABS,
                                               S16_DEL_M_H_SPEED, (int16_t)U8_DEL_M_THR_EXIT_FRONT_LM_HS_ABS);

    q_front_exit_gain = -LCESP_s16FindRoadDependatGain(esp_mu, esp_tempW11, esp_tempW12, esp_tempW13);
    
  /*REAR EXIT CONDITION*/  
    esp_tempW21 = LCESP_s16IInter3Point(vref5, S16_DEL_M_L_SPEED, (int16_t)U8_DEL_M_THR_EXIT_REAR_HM_LS_ABS, 
                                               S16_DEL_M_M_SPEED, (int16_t)U8_DEL_M_THR_EXIT_REAR_HM_MS_ABS, 
                                               S16_DEL_M_H_SPEED, (int16_t)U8_DEL_M_THR_EXIT_REAR_HM_HS_ABS);
                                          
    esp_tempW22 = LCESP_s16IInter3Point(vref5, S16_DEL_M_L_SPEED, (int16_t)U8_DEL_M_THR_EXIT_REAR_MM_LS_ABS,
                                               S16_DEL_M_M_SPEED, (int16_t)U8_DEL_M_THR_EXIT_REAR_MM_MS_ABS,
                                               S16_DEL_M_H_SPEED, (int16_t)U8_DEL_M_THR_EXIT_REAR_MM_HS_ABS);
                                          
    esp_tempW23 = LCESP_s16IInter3Point(vref5, S16_DEL_M_L_SPEED, (int16_t)U8_DEL_M_THR_EXIT_REAR_LM_LS_ABS,
                                               S16_DEL_M_M_SPEED, (int16_t)U8_DEL_M_THR_EXIT_REAR_LM_MS_ABS,
                                               S16_DEL_M_H_SPEED, (int16_t)U8_DEL_M_THR_EXIT_REAR_LM_HS_ABS);

    q_rear_exit_gain = -LCESP_s16FindRoadDependatGain(esp_mu, esp_tempW21, esp_tempW22, esp_tempW23);
    
    if(q_rear_exit_gain>=q_rear_exit_gain_limit)
    {
    	q_rear_exit_gain = q_rear_exit_gain_limit;    	
    }
    else
    {
    	q_rear_exit_gain = q_rear_exit_gain;
    }
	}
	else
	{
	/*FRONT EXIT CONDITION*/
    esp_tempW11 = LCESP_s16IInter3Point(vref5, S16_DEL_M_L_SPEED, (int16_t)U8_DEL_M_THR_EXIT_FRONT_HM_LS, 
                                               S16_DEL_M_M_SPEED, (int16_t)U8_DEL_M_THR_EXIT_FRONT_HM_MS, 
                                               S16_DEL_M_H_SPEED, (int16_t)U8_DEL_M_THR_EXIT_FRONT_HM_HS);
                                          
    esp_tempW12 = LCESP_s16IInter3Point(vref5, S16_DEL_M_L_SPEED, (int16_t)U8_DEL_M_THR_EXIT_FRONT_MM_LS,
                                               S16_DEL_M_M_SPEED, (int16_t)U8_DEL_M_THR_EXIT_FRONT_MM_MS,
                                               S16_DEL_M_H_SPEED, (int16_t)U8_DEL_M_THR_EXIT_FRONT_MM_HS);
                                          
    esp_tempW13 = LCESP_s16IInter3Point(vref5, S16_DEL_M_L_SPEED, (int16_t)U8_DEL_M_THR_EXIT_FRONT_LM_LS,
                                               S16_DEL_M_M_SPEED, (int16_t)U8_DEL_M_THR_EXIT_FRONT_LM_MS,
                                               S16_DEL_M_H_SPEED, (int16_t)U8_DEL_M_THR_EXIT_FRONT_LM_HS);

    q_front_exit_gain = -LCESP_s16FindRoadDependatGain(esp_mu, esp_tempW11, esp_tempW12, esp_tempW13);
    
  /*REAR EXIT CONDITION*/  
    esp_tempW21 = LCESP_s16IInter3Point(vref5, S16_DEL_M_L_SPEED, (int16_t)U8_DEL_M_THR_EXIT_REAR_HM_LS, 
                                               S16_DEL_M_M_SPEED, (int16_t)U8_DEL_M_THR_EXIT_REAR_HM_MS, 
                                               S16_DEL_M_H_SPEED, (int16_t)U8_DEL_M_THR_EXIT_REAR_HM_HS);
                                          
    esp_tempW22 = LCESP_s16IInter3Point(vref5, S16_DEL_M_L_SPEED, (int16_t)U8_DEL_M_THR_EXIT_REAR_MM_LS,
                                               S16_DEL_M_M_SPEED, (int16_t)U8_DEL_M_THR_EXIT_REAR_MM_MS,
                                               S16_DEL_M_H_SPEED, (int16_t)U8_DEL_M_THR_EXIT_REAR_MM_HS);
                                          
    esp_tempW23 = LCESP_s16IInter3Point(vref5, S16_DEL_M_L_SPEED, (int16_t)U8_DEL_M_THR_EXIT_REAR_LM_LS,
                                               S16_DEL_M_M_SPEED, (int16_t)U8_DEL_M_THR_EXIT_REAR_LM_MS,
                                               S16_DEL_M_H_SPEED, (int16_t)U8_DEL_M_THR_EXIT_REAR_LM_HS);

    q_rear_exit_gain = -LCESP_s16FindRoadDependatGain(esp_mu, esp_tempW21, esp_tempW22, esp_tempW23);
    
    if(q_rear_exit_gain>=q_rear_exit_gain_limit)
    {
    	q_rear_exit_gain = q_rear_exit_gain_limit;    	
    }
    else
    {
    	q_rear_exit_gain = q_rear_exit_gain;
    }
	}
  if(q_front_exit_gain>=q_front_exit_gain_limit)
  {
  	q_front_exit_gain = q_front_exit_gain_limit;    	
  }
  else
  {
  	q_front_exit_gain = q_front_exit_gain;
  }
			
}
/*
#else
void LCESP_vCalExitCondition(void)
{
#if (__MGH60_ESC_IMPROVE_CONCEPT == 1) 

 				esp_tempW3 = S16_YAW_CONTROL_EXIT_H ;
       	esp_tempW4 = S16_YAW_CONTROL_EXIT_M ;                 
       	esp_tempW5 = S16_YAW_CONTROL_EXIT_L ;               

        esp_tempW1 = LCESP_s16FindRoadDependatGain(esp_mu,esp_tempW3,esp_tempW4,esp_tempW5 );
#else    

        esp_tempW9 = LCESP_s16FindRoadDependatGain( esp_mu, S16_YAW_CONTROL_EXIT_H,
                                  S16_YAW_CONTROL_EXIT_M, S16_YAW_CONTROL_EXIT_L);
        esp_tempW8 = LCESP_s16IInter3Point(det_wstr_dot_lf, S16_WSTR_DOT_LS,S16_YAW_CONTROL_EXIT_WSTR_L,
                                 S16_WSTR_DOT_MS, S16_YAW_CONTROL_EXIT_WSTR_M,
                                 S16_WSTR_DOT_HS, esp_tempW9);

        esp_tempW7 = esp_tempW8-2;

        esp_tempW1 = LCESP_s16IInter2Point(det_abs_wstr, 450, esp_tempW7 , 900, esp_tempW8);
      
#endif 
        
        if(esp_tempW1<1)
        {
            esp_tempW1=1;

        }
        else if(esp_tempW1>8)
        {
            esp_tempW1=8;
        }
        else
        {
            ;
        }

        if((ABS_fz==1)&&(det_alatm>lsesps16MuHigh))
        {
            esp_tempW1 = 1 ;
        }
        else
        {
            ;
        }
        q_front_exit_gain =SLIP_TARGET_THRESHOLD+esp_tempW1;
       
#if (__MGH60_ESC_IMPROVE_CONCEPT == 1)
       
 				esp_tempW3 = SLIP_TARGET_THRESHOLD + S16_YAW_CTRL_REAR_EXIT_HM ;
       	esp_tempW4 = SLIP_TARGET_THRESHOLD + S16_YAW_CTRL_REAR_EXIT_MM ;                // rear     
       	esp_tempW5 = SLIP_TARGET_THRESHOLD + S16_YAW_CTRL_REAR_EXIT_LM ;                // rear     

        q_rear_exit_gain = LCESP_s16FindRoadDependatGain(esp_mu,esp_tempW3,esp_tempW4,esp_tempW5 );

#else       
         esp_tempW4 = SLIP_TARGET_THRESHOLD ;             // rear     
         esp_tempW2 = 4;                                  // rear hysteresis  gain  
         q_rear_exit_gain = esp_tempW4 + esp_tempW2 ;     // rear  threshold

#endif         

}
#endif
*/

void LCESP_vDetectControlSignal(void)
{
    /* ESP CONTROL SIGNAL*/
   if(ESP_ON==0)
   {
        esp_on_counter=0;
        if((YAW_CDC_WORK==1)||(ESP_TCS_ON==1))
        {
            ESP_ON=1;
        }
        else
        {
            ;
        }

        esp_on_counter_1=0;
        esp_on_counter_f=0;
        esp_on_counter_r=0;

        esp_on_counter_fl = 0;
        esp_on_counter_fr = 0;
        esp_on_counter_rl = 0;
        esp_on_counter_rr = 0;
    }
    else
    {
        if((YAW_CDC_WORK==0)&&(ESP_TCS_ON==0)&&(ETO_SUSTAIN_1SEC==0))
        {
            esp_on_counter = esp_on_counter+1;
            if(esp_on_counter>L_U8_TIME_10MSLOOP_1000MS)
            {
                ESP_ON=0;
            }
            else
            {
                ;
            }
        }
        else
        {
            esp_on_counter=0;
        }

        if((YAW_CDC_WORK==0))
        {
            if(esp_on_counter_1<255)
            {
                esp_on_counter_1=esp_on_counter_1+1;
            }
            else
            {
                ;
            }
        }
        else
        {
            esp_on_counter_1=0;
        }

        if((SLIP_CONTROL_NEED_FL==0)&&(SLIP_CONTROL_NEED_FL_old==1))
        {
            esp_on_counter_f=esp_on_counter_f+1;
        }
        else if((SLIP_CONTROL_NEED_FR==0)&&(SLIP_CONTROL_NEED_FR_old==1))
        {
            esp_on_counter_f=esp_on_counter_f+1;
        }
        else
        {
            esp_on_counter_f=0;
        }

        if((SLIP_CONTROL_NEED_FL==0)&&(SLIP_CONTROL_NEED_FL_old==1))
        {
            esp_on_counter_fl=esp_on_counter_fl+1;

            if( esp_on_counter_fl >= 255)
            {
                esp_on_counter_fl = 255;
            }
            else
            {
                ;
            }
        }
        else
        {
            esp_on_counter_fl=0;
        }

        if((SLIP_CONTROL_NEED_FR==0)&&(SLIP_CONTROL_NEED_FR_old==1))
        {
            esp_on_counter_fr=esp_on_counter_fr+1;

            if( esp_on_counter_fr >= 255)
            {
                esp_on_counter_fr = 255;
            }
            else
            {
                ;
            }
        }
        else
        {
            esp_on_counter_fr=0;
        }

        if((SLIP_CONTROL_NEED_RL==0)&&(SLIP_CONTROL_NEED_RL_old==1))
        {
            esp_on_counter_r=esp_on_counter_r+1;
        }
        else if((SLIP_CONTROL_NEED_RR==0)&&(SLIP_CONTROL_NEED_RR_old==1))
        {
            esp_on_counter_r=esp_on_counter_r+1;
        }
        else
        {
            esp_on_counter_r=0;
        }
       if((SLIP_CONTROL_NEED_RL==0)&&(SLIP_CONTROL_NEED_RL_old==1))
        {
            esp_on_counter_rl=esp_on_counter_rl+1;

            if( esp_on_counter_rl >= 255)
            {
                esp_on_counter_rl = 255;
            }
            else
            {
                ;
            }
        }
        else
        {
            esp_on_counter_rl=0;
        }

        if((SLIP_CONTROL_NEED_RR==0)&&(SLIP_CONTROL_NEED_RR_old==1))
        {
            esp_on_counter_rr=esp_on_counter_rr+1;

            if( esp_on_counter_rr >= 255)
            {
                esp_on_counter_rr = 255;
            }
            else
            {
                ;
            }
        }
        else
        {
            esp_on_counter_rr=0;
        }
    }

    if(YAW_CDC_WORK==1)
    {
        yaw_cdc_work_on_counter=yaw_cdc_work_on_counter+1;
    }
    else
    {
        yaw_cdc_work_on_counter=0;
    }
    if(SLIP_CONTROL_NEED_FL==1)
    {
        slip_control_need_counter_fl=slip_control_need_counter_fl+1;
    }
    else
    {
        slip_control_need_counter_fl=0;
    }

    if(SLIP_CONTROL_NEED_FR==1)
    {
        slip_control_need_counter_fr=slip_control_need_counter_fr+1;
    }
    else
    {
        slip_control_need_counter_fr=0;
    }

    if(SLIP_CONTROL_NEED_RL==1)
    {
        slip_control_need_counter_rl=slip_control_need_counter_rl+1;
    }
    else
    {
        slip_control_need_counter_rl=0;
    }

    if(SLIP_CONTROL_NEED_RR==1)
    {
        slip_control_need_counter_rr=slip_control_need_counter_rr+1;
    }
    else
    {
        slip_control_need_counter_rr=0;
    }
}

void LCESP_vDetectLowSpeed(void)
{
/* ESP CONTROL Limit using Vehicle speed*/

    if((max_speed<=VREF_2_KPH_RESOL_CHANGE)&&(YAW_CDC_WORK==0))
    {
        KPH_15_FLG=0;
    }
    else
    {
        ;
    }
#if __ESP_LowSpeedChange
    if(min_speed>=VREF_10_KPH_RESOL_CHANGE)
    {
        KPH_15_FLG=1;
    }
    else
    {
        ;
    }
#else

#if (__MGH60_ESC_IMPROVE_CONCEPT == 1) 

    esp_tempW0 = S16_ESC_BRAKE_ENABLE_SPEED*8 ;

    if(min_speed>=esp_tempW0)
    {
        KPH_15_FLG=1;
    }
    else
    {
        ;
    }    
    
#else    
    if(min_speed>=VREF_15_KPH_RESOL_CHANGE)
    {
        KPH_15_FLG=1;
    }
    else
    {
        ;
    }

#endif       
    
#endif
/*-------------------------------------------------------------------------------*/

    VDC_LOW_SPEED=0;
    PRESSURE_DUMP_NEED=0;
    if(ESP_ON==1)
    {
        if(vdc_vref<=VREF_6_KPH)
        {
            VDC_LOW_SPEED=1;
            PRESSURE_DUMP_NEED=1;
        }
        else
        {
            ;
        }
    }
#if __ESP_LowSpeedChange
    else if((vdc_vref<=VREF_10_KPH)||(KPH_15_FLG==0))
    {
        VDC_LOW_SPEED=1;
    }
    else
    {
        ;
    }
#else

#if (__MGH60_ESC_IMPROVE_CONCEPT == 1)  
    
    else if((vdc_vref<=S16_ESC_BRAKE_ENABLE_SPEED)||(KPH_15_FLG==0))
    {
        VDC_LOW_SPEED=1;
    }
    else
    {
        ;
    }

#else    
    else if((vdc_vref<=VREF_15_KPH)||(KPH_15_FLG==0))
    {
        VDC_LOW_SPEED=1;
    }
    else
    {
        ;
    }
#endif  
#endif
}

void LCESP_vDetectWstrJump(void)
{

    if((McrAbs(steer_offset_360_old-steer_offset_360)>=STEER_360DEG)&&(WSTR_360_CHECK==0))
    {
        WSTR_360_JUMP=1;
    }
    else
    {
        ;
    }

    if (WSTR_360_JUMP==1)
    {
        esp_tempW9=L_U8_TIME_10MSLOOP_800MS;
        if(WSTR_360_JUMP_on_cnt<L_U16_TIME_10MSLOOP_5S)
        {
            WSTR_360_JUMP_on_cnt=WSTR_360_JUMP_on_cnt+1;
        }
        else
        {
            ;
        }
    }
    else
    {
        esp_tempW9=L_U8_TIME_10MSLOOP_800MS;
        WSTR_360_JUMP_on_cnt=0;
    }

    if(WSTR_360_CHECK==0)
    {
        if(STR_OFFSET_360_OK==1)
        {
            #if __STEER_SENSOR_TYPE==ANALOG_TYPE/*060602 for HM reconstruction by eslim*/
            if(wstr_stable_count++>esp_tempW9)
            {
                WSTR_360_CHECK=1;
                WSTR_360_JUMP=0;                                /* 2004.05.13 KIM YONG KIL  */
            }
            else
            {
                ;
            }
            #else
                WSTR_360_CHECK=1;                               /* 2005.05.16 jehong */
                WSTR_360_JUMP=0;
            #endif
        }
        else
        {
            wstr_stable_count=0;
        }
    }
    else
    {
        ;
    }

#if __CAR==AUDI_A6 || __CAR==BMW_X5 || __CAR==SYC_RAXTON_MECU || __CAR==ASTRA_MECU
    WSTR_360_CHECK=1;
#endif
}

#if __MGH40_ESC_Concepts == 1

#if MGH60_NEW_TARGET_SLIP
void LCESP_vCalDelTargetSlipMgh60(void)
{

#if __MOMENT_TO_TARGET_SLIP_NEW

    if(SLIP_CONTROL_BRAKE_FL == 1)
    {          
#else 

	alpha_f = McrAbs(Alpha_front);
	alpha_r = McrAbs(Alpha_rear);   
		
    	esp_tempW8 = DEL_TAR_SLIP_F_HMU_ALPHA_Min;   /* 500 */
    	esp_tempW7 = DEL_TAR_SLIP_F_HMU_ALPHA_Max;   /* 1500 */
    	                                                 
    	esp_tempW5 = DEL_TAR_SLIP_F_MMU_ALPHA_Min;   /* 300 */
    	esp_tempW4 = DEL_TAR_SLIP_F_MMU_ALPHA_Max;   /* 1000 */
    	
    	if(esp_tempW7 < esp_tempW8) esp_tempW7 =esp_tempW8+100;
     	if(esp_tempW4 < esp_tempW5) esp_tempW4 =esp_tempW5+100;
     	    	
    	if(esp_tempW5 < 100) esp_tempW5 =100;
    	if(esp_tempW8 < 100) esp_tempW8 =100;
  
     	if(esp_tempW4 > 2000) esp_tempW4 =2000;
    	if(esp_tempW7 > 2000) esp_tempW7 =2000;
    	   	   	    	   	   	
    	m1 = LCESP_s16IInter2Point( alpha_f, esp_tempW8, 3000, esp_tempW7, 2000); /*20%,  high mu model*/ 
    	m2 = LCESP_s16IInter2Point( alpha_f, esp_tempW8, 4000, esp_tempW7, 3000); /*40%*/
    	m3 = LCESP_s16IInter2Point( alpha_f, esp_tempW8, 6000, esp_tempW7, 4000); /*2-wheel */
    	
    	m4 = LCESP_s16IInter2Point( alpha_f, esp_tempW5, 3000, esp_tempW4, 2000); /*20%,  low mu model*/ 
    	m5 = LCESP_s16IInter2Point( alpha_f, esp_tempW5, 4000, esp_tempW4, 3000); /*40%*/
    	m6 = LCESP_s16IInter2Point( alpha_f, esp_tempW5, 6000, esp_tempW4, 4000); /*2-wheel */
                                                
        /* High-speed ( H_mu, M_mu, L_mu )  */                
        esp_tempW1 = LCESP_s16FindRoadDependatGain(esp_mu, S16_TARGET_SLIP_WEG_ALPHA_F_HMU_HSP,
                                                           S16_TARGET_SLIP_WEG_ALPHA_F_MMU_HSP,  
                                                           S16_TARGET_SLIP_WEG_ALPHA_F_LMU_HSP);
                                 
        /* Med-speed ( H_mu, M_mu, L_mu )  */                
        esp_tempW2 = LCESP_s16FindRoadDependatGain(esp_mu, S16_TARGET_SLIP_WEG_ALPHA_F_HMU_MSP,
                                                           S16_TARGET_SLIP_WEG_ALPHA_F_MMU_MSP,  
                                                           S16_TARGET_SLIP_WEG_ALPHA_F_LMU_MSP);
                                                                                                                              
        /* Low-speed ( H_mu, M_mu, L_mu )  */                                                   
        esp_tempW3 = LCESP_s16FindRoadDependatGain(esp_mu, S16_TARGET_SLIP_WEG_ALPHA_F_HMU_LSP,
                                                           S16_TARGET_SLIP_WEG_ALPHA_F_MMU_LSP,  
                                                           S16_TARGET_SLIP_WEG_ALPHA_F_LMU_LSP);   
                                                                                                                    
        esp_tempW7 = LCESP_s16IInter4Point( vrefk,  S16_ESP_SLIP_SPEED_GAIN_DEP_S_V,  esp_tempW3,
                                                    S16_ESP_SLIP_SPEED_GAIN_DEP_L_V,  esp_tempW2,
                                                    S16_ESP_SLIP_SPEED_GAIN_DEP_M_V,  esp_tempW2,
                                                    S16_ESP_SLIP_SPEED_GAIN_DEP_H_V,  esp_tempW1 );                                                 
                                                 
        esp_tempW1 = LCESP_s16IInter2Point( Alpha_front,  -500 , esp_tempW7, esp_tempW8 , 100);    /* High-mu model Weighting */ 	   	   	
    	   	   	 
        m1 = (int16_t)( (((int32_t)m1)*esp_tempW1)/100 );    	   
        m2 = (int16_t)( (((int32_t)m2)*esp_tempW1)/100 );    
        m3 = (int16_t)( (((int32_t)m3)*esp_tempW1)/100 );    
                                                         
        esp_tempW2 = LCESP_s16IInter2Point( Alpha_front,  -500 , esp_tempW7, esp_tempW5 , 100);    /* Low-mu model Weighting */ 	   	   	
    	   	   	 
        m4 = (int16_t)( (((int32_t)m4)*esp_tempW2)/100 );    	   
        m5 = (int16_t)( (((int32_t)m5)*esp_tempW2)/100 );    
        m6 = (int16_t)( (((int32_t)m6)*esp_tempW2)/100 );            
    	
			m7 = LCESP_s16IInter2Point( det_alatm, lsesps16MuMedHigh, m4, lsesps16MuHigh, m1); /*20%*/
    	m8 = LCESP_s16IInter2Point( det_alatm, lsesps16MuMedHigh, m5, lsesps16MuHigh, m2); /*40%*/
    	m9 = LCESP_s16IInter2Point( det_alatm, lsesps16MuMedHigh, m6, lsesps16MuHigh, m3); /*2 wheel */

    if(SLIP_CONTROL_BRAKE_FL == 1)
    {  
        target_slip_f_0 = McrAbs(slip_m_fl_current);
        if(slip_m_fl_current < 0)
        {
            target_moment_f_0 = LCESP_s16IInter3Point( target_slip_f_0, 0, 0, WHEEL_SLIP_20, m7, WHEEL_SLIP_40, m8);
        }
        else
        {
            target_moment_f_0 = 0;
        }

#endif  

/******************************************************/
        if( delta_moment_q > 0)
        {
            esp_tempW6 = McrAbs(delta_moment_q) ;
        }
        else
        {
        	esp_tempW6 = 0;
        }
        
	
		/********** Exteme Understeer **********/
    #if ( __Ext_Under_Ctrl == 1 )
    
        if (EXTREME_UNDER_CONTROL == 1) 
        {
            esp_tempW6 = McrAbs(Extr_Under_Frt_Moment_FL) ;
        }
        else
        {
            ;
        }
    #endif     
                
        if (lcu1US2WHControlFlag_RL == 1) 
        {
            esp_tempW6 = McrAbs(lcs16US2WH_FrtDelM) ;
        }
        else
        {
            ;
        }                    
                
		/**********ROP  Front**********/
    #if __ROP
        if ( (ROP_REQ_FRT_Pre_FL == 1) && ( (McrAbs(moment_pre_front_rop)) > esp_tempW6 ) )
        {
            esp_tempW6 = McrAbs(moment_pre_front_rop) ;
        }
        else
        {
            ;
        }

      #if (__Pro_Active_ROP_Decel == 1)      
        if (lcu1ropFrtCtrlReqFL==1)
        {
            esp_tempW6 = S8_DR_2ND_F_LMT_DEL_SLIP  ;
        }                  
        else
        {
            ;
        }
      #endif           
        
    #endif                              
    
    /********** CRAM **********/
    #if (__CRAM == 1)
    	#if (__CRAM_PRE_FRONT_W_CRTL == 1)
        if ( (lcu1espCramPreactFRT == 1) && ( (McrAbs(moment_pre_front_cram_preact)) > esp_tempW6 ) )
        {
            esp_tempW6 = McrAbs(moment_pre_front_cram_preact) ;
        }
        else
        {
            ;
        }
      #endif  
     
     	#if (__CRAM_FRONT_2WH_ENABLE == 1)     
        if ( (ldu1espDetectCRAM_FL  == 1) && ( (McrAbs(moment_pre_front_cram)) > esp_tempW6 ) )
        {
            esp_tempW6 = McrAbs(moment_pre_front_cram) ;
        }
        else
        {
            ;
        }
      #endif
    #endif           

				if((esp_tempW6==0) && ( delta_moment_q < 0))
        {
            esp_tempW6 = McrAbs(delta_moment_q) ;
        }
				else
				{
						;
				}



#if (__CRAM == 1)
				esp_tempW7 = 0;
	#if(__CRAM_FRONT_2WH_ENABLE == 1) 
        if ((ldu1espDetectCRAM==1)&&(ldu1espDetectCRAM_FL==0))
        {
            esp_tempW7 = (int16_t)( (((int32_t)S16_CRAM_OUT_FRT_SLIP_ADD_WEG)*(McrAbs(moment_pre_front_cram)))/100 );  
        }
        else
        {
            ;
        }
  #endif
        
     		esp_tempW8 = (int16_t)( (((int32_t)S16_CRAM_OUT_FRT_SLIP_ADD_WEG)*(McrAbs(moment_pre_rear_cram)))/100 ); 
        if((ldu1espDetectCRAM==1) && (esp_tempW8 > esp_tempW7))
        {
        		esp_tempW7 = esp_tempW8;
        }
        else
        {
            ;
        }
        
        target_moment_f = target_moment_f_0 + esp_tempW6 + esp_tempW7 ;
#else
        target_moment_f = target_moment_f_0 + esp_tempW6;
#endif

 
#if __MOMENT_TO_TARGET_SLIP_NEW
        cal_target_slip_front = esp_tempW6 ;
#else
        cal_target_slip_front = LCESP_s16IInter3Point( target_moment_f, 0, 0, m7, WHEEL_SLIP_20, m8, WHEEL_SLIP_40);
#endif 

/*****************2wheel *********************/
/*
#if __MOMENT_TO_TARGET_SLIP_NEW 

        esp_tempW0 = LCESP_s16FindRoadDependatGain(esp_mu, S16_OS_2_WHEEL_QM_ENT_H, S16_OS_2_WHEEL_QM_ENT_M, S16_OS_2_WHEEL_QM_ENT_L);   // Max.Frt Target_Slip
    
//      esp_tempW0 = LCESP_s16FindRoadDependatGain(esp_mu, WHEEL_SLIP_30, WHEEL_SLIP_30, WHEEL_SLIP_30);   // Max.Frt Target_Slip
        esp_tempW9 = esp_tempW6 - esp_tempW0;    
        
        esp_tempW0 = LCESP_s16FindRoadDependatGain(esp_mu,WHEEL_SLIP_5 , WHEEL_SLIP_10, WHEEL_SLIP_10);    // Frt Current Slip           

        esp_tempW3 = LCESP_s16FindRoadDependatGain(esp_mu, S16_TWO_HMU_REAR_TAR_SLIP_WEG,
                                                           S16_TWO_MMU_REAR_TAR_SLIP_WEG,  
                                                           S16_TWO_LMU_REAR_TAR_SLIP_WEG);           

#else 
        cal_target_2wheel = LCESP_s16IInter3Point( target_moment_f, 0, 0, m7, WHEEL_SLIP_20, m9, WHEEL_SLIP_60);

        esp_tempW9 = cal_target_2wheel - cal_target_slip_front;

        esp_tempW0 = LCESP_s16FindRoadDependatGain(esp_mu,WHEEL_SLIP_3 , WHEEL_SLIP_5, WHEEL_SLIP_5);            

#endif  

        esp_tempW1 = LCESP_s16FindRoadDependatGain(esp_mu,WHEEL_SLIP_2 , WHEEL_SLIP_2 , WHEEL_SLIP_3);

        esp_tempW2 = LCESP_s16FindRoadDependatGain(esp_mu,S16_2WH_LMU_REAR_SLIP_MAX,
                            S16_2WH_MMU_REAR_SLIP_MAX, S16_2WH_HMU_REAR_SLIP_MAX);

	#if (__CRAM == 1)
        if((ldu1espDetectCRAM==1)&&(ldu1espDetectCRAM_FL==0))    // Over-, Outside Rear
        { 
        		
        		esp_tempW4 = (int16_t)( (((int32_t)esp_tempW9)*esp_tempW3)/100 );
            if(esp_tempW4 <= 0)
            {
                esp_tempW4 = 0;
            }
            else if(esp_tempW4 > esp_tempW2)
            {
                esp_tempW4 = esp_tempW2;
            }
            else
            {
                ;
            }
        		if(moment_pre_rear_cram > esp_tempW4)
        		{
            	del_target_2wheel_slip =  moment_pre_rear_cram ;    
            }
            else
            {
            	del_target_2wheel_slip  = esp_tempW4;
            }
            
        }        
    #if (__Pro_Active_Roll_Ctrl == 1)        
      #if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
        else if((ROP_REQ_ACT == 1)&&(RL_ESC.lcu1ropRearCtrlReq == 1))
      #else
        else if((ROP_REQ_ACT == 1)&&(lcu1ropRearCtrlReqRL == 1))
      #endif
        {
            del_target_2wheel_slip =  S16_ROP_PROACTIVE_DEL_SLIP_R ;  // Tuning parameter...

            if(del_target_2wheel_slip <= 0)
            {
                del_target_2wheel_slip = 0;
            } 
            else
            {
                ;
            }
        }
    #endif                          
        else if((MPRESS_BRAKE_ON == 0)&&(lcu1US2WHControlFlag==0)		//        else if((ABS_fz==0)
//           &&(((FLAG_TWO_WHEEL == 0)&&(slip_m_fl < (-esp_tempW0))&&(esp_tempW9 > 0))
           &&(((FLAG_TWO_WHEEL == 0)&&(esp_pressure_count_fl > 20)&&(esp_tempW9 > 0))
           ||(FLAG_TWO_WHEEL == 1)))
        {
            del_target_2wheel_slip = (int16_t)( (((int32_t)esp_tempW9)*esp_tempW3)/100 );

            if(del_target_2wheel_slip <= 0)
            {
                del_target_2wheel_slip = 0;
            }
            else if(del_target_2wheel_slip > esp_tempW2)
            {
                del_target_2wheel_slip = esp_tempW2;
            }
            else
            {
                ;
            }
        }
        else
        {
            del_target_2wheel_slip = 0;
        }
	#else
	
    #if (__Pro_Active_Roll_Ctrl == 1)        
      #if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
        if((ROP_REQ_ACT == 1)&&(RL_ESC.lcu1ropRearCtrlReq == 1))
      #else
        if((ROP_REQ_ACT == 1)&&(lcu1ropRearCtrlReqRL == 1))
      #endif
        {
            del_target_2wheel_slip =  S16_ROP_PROACTIVE_DEL_SLIP_R ;  // Tuning parameter...

            if(del_target_2wheel_slip <= 0)
            {
                del_target_2wheel_slip = 0;
            } 
            else
            {
                ;
            }
        }
        
		#else        
        if(ROP_REQ_FRT_Pre == 1)
        {
            del_target_2wheel_slip = 0;
        }
    #endif 
        else if((MPRESS_BRAKE_ON == 0)&&(lcu1US2WHControlFlag==0)		//        else if((ABS_fz==0)
           &&(((FLAG_TWO_WHEEL == 0)&&(slip_m_fl < (-esp_tempW0))&&(esp_tempW9 > 0))
           ||(FLAG_TWO_WHEEL == 1)))
        {

            del_target_2wheel_slip = (int16_t)( (((int32_t)esp_tempW9)*esp_tempW3)/100 );

            if(del_target_2wheel_slip <= 0)
            {
                del_target_2wheel_slip = 0;
            }
            else if(del_target_2wheel_slip > esp_tempW2)
            {
                del_target_2wheel_slip = esp_tempW2;
            }
            else
            {
                ;
            }
        }
        else
        {
            del_target_2wheel_slip = 0;
        }
  #endif      
*/
#if __MOMENT_TO_TARGET_SLIP_NEW
        del_target_slip_fl = -(cal_target_slip_front);
#else
        del_target_slip_fl = -(cal_target_slip_front - target_slip_f_0);
#endif 

        if((ESP_SPLIT==1)&&(MSL_BASE_rl==1))
        {
            if(del_target_slip_fl < (-WHEEL_SLIP_10))
            {
                del_target_slip_fl = -WHEEL_SLIP_10;
            }
            else
            {
                ;
            }
        }
        else
        {
            ;
        }
/*
#if (__CRAM == 1)
		    if(ldu1espDetectCRAM==0)
		    {  
		        if(del_target_2wheel_slip > 0)
		        {
		            FLAG_TWO_WHEEL = 1; 
		            FLAG_TWO_WHEEL_RL = 1;            
		        }
		        else
		        {
		            FLAG_TWO_WHEEL = 0;
		            FLAG_TWO_WHEEL_RL = 0;            
		        }
		    }
		    else
		    {
		        if(del_target_2wheel_slip > 0)
		        {
		            FLAG_TWO_WHEEL = 1;
		            FLAG_TWO_WHEEL_RL = 1;  
		        }
		        else  
		        {
		            FLAG_TWO_WHEEL = 0;
		            FLAG_TWO_WHEEL_RL = 0;  
		            
		            SLIP_CONTROL_BRAKE_RL = 0;
		        }       
		    }
		        
        if(FLAG_TWO_WHEEL_RL == 1)
        {
            del_target_slip_rl = -del_target_2wheel_slip; 
            
            if(ldu1espDetectCRAM==0)
                {
            del_target_slip_rr = 0;
                }

            SLIP_CONTROL_BRAKE_RL = 1;
        }
        else
        {
            ;
        }
#else
	
    #if (__Pro_Active_Roll_Ctrl == 1)        
        
      #if (__Pro_Active_ROP_Decel == 1)       
      
          if(del_target_2wheel_slip > 0)    
  		#else          
  		
  	      if((del_target_2wheel_slip > 0)&&(ROP_REQ_FRT_Pre_FL==0))   
      #endif  
        
		#else         
	     if(del_target_2wheel_slip > 0)
	        
    #endif  
        {
            FLAG_TWO_WHEEL = 1;
        }
    #if (__Pro_Active_Roll_Ctrl == 1)        
        else if((FLAG_TWO_WHEEL == 1)&&(SLIP_CONTROL_NEED_FL==1)&&(ABS_fz==0)&&(ROP_REQ_FRT_Pre_FL==0))
    #else         
        else if((FLAG_TWO_WHEEL == 1)&&(SLIP_CONTROL_NEED_FL==1)&&(ABS_fz==0))
    #endif          
        {
            FLAG_TWO_WHEEL = 1;
        }
        else
        {
            FLAG_TWO_WHEEL = 0;
        }

        if(FLAG_TWO_WHEEL == 1)
        {
            del_target_slip_rl = -del_target_2wheel_slip;
            del_target_slip_rr = 0;

            SLIP_CONTROL_BRAKE_RL = 1;
        }
        else
        {
            ;
        }
#endif
*/
    }
    else
    {
        del_target_slip_fl = 0;
    }

#if __MOMENT_TO_TARGET_SLIP_NEW

    if(SLIP_CONTROL_BRAKE_FR == 1)
    {
      
#else 

    if(SLIP_CONTROL_BRAKE_FR == 1)
    {
        target_slip_f_0 = McrAbs(slip_m_fr_current);
        if(slip_m_fr_current < 0)
        {
            target_moment_f_0 = LCESP_s16IInter3Point( target_slip_f_0, 0, 0, WHEEL_SLIP_20, m7, WHEEL_SLIP_40, m8);

        }
        else
        {
            target_moment_f_0 = 0;
        }

#endif  
/******************************************************/
        if( delta_moment_q < 0)
        {
            esp_tempW6 = McrAbs(delta_moment_q) ;
        }
        else
        {
        		esp_tempW6 = 0;
        }
        
		/********** Exteme Understeer **********/
    #if ( __Ext_Under_Ctrl == 1 )
    
        if (EXTREME_UNDER_CONTROL == 1) 
        {
            esp_tempW6 = McrAbs(Extr_Under_Frt_Moment_FR) ;
        }
        else
        {
            ;
        }
    #endif            
    
        if (lcu1US2WHControlFlag_RR == 1) 
        {
            esp_tempW6 = McrAbs(lcs16US2WH_FrtDelM) ;
        }
        else
        {
            ;
        }       
            
		/**********ROP  Front**********/
    #if __ROP
        if ( (ROP_REQ_FRT_Pre_FR == 1) && ( (McrAbs(moment_pre_front_rop)) > esp_tempW6 ) )
        {
            esp_tempW6 = McrAbs(moment_pre_front_rop) ;
        }
        else
        {
            ;
        }
        
      #if (__Pro_Active_ROP_Decel == 1)      
        if (lcu1ropFrtCtrlReqFR==1)
        {
            esp_tempW6 = S8_DR_2ND_F_LMT_DEL_SLIP  ;
        }                  
        else
        {
            ;
        }
      #endif            
        
    #endif                              
    
    /********** CRAM **********/
    #if (__CRAM == 1)
    	#if (__CRAM_PRE_FRONT_W_CRTL == 1)
        if ( (lcu1espCramPreactFRT == 1) && ( (McrAbs(moment_pre_front_cram_preact)) > esp_tempW6 ) )
        {
            esp_tempW6 = McrAbs(moment_pre_front_cram_preact) ;
        }
        else
        {
            ;
        }
     #endif
     
     	#if (__CRAM_FRONT_2WH_ENABLE == 1)     
        if ( (ldu1espDetectCRAM_FR  == 1) && ( (McrAbs(moment_pre_front_cram)) > esp_tempW6 ) )
        {
            esp_tempW6 = McrAbs(moment_pre_front_cram) ;
        }
        else
        {
            ;
        }
      #endif
    #endif           

				if((esp_tempW6==0) && ( delta_moment_q > 0))
        {
            esp_tempW6 = McrAbs(delta_moment_q) ;
        }
				else
				{
						;
				}


#if (__CRAM == 1) 
				esp_tempW7 = 0;
	#if (__CRAM_FRONT_2WH_ENABLE == 1) 
        if ((ldu1espDetectCRAM==1)&&(ldu1espDetectCRAM_FR==0))
        {
            esp_tempW7 = (int16_t)( (((int32_t)S16_CRAM_OUT_FRT_SLIP_ADD_WEG)*(abs(moment_pre_front_cram)))/100 );  
        }
        else
        {
            esp_tempW7 = 0 ;
        }
  #endif

     		esp_tempW8 = (int16_t)( (((int32_t)S16_CRAM_OUT_FRT_SLIP_ADD_WEG)*(McrAbs(moment_pre_rear_cram)))/100 ); 
        if((ldu1espDetectCRAM==1) && (esp_tempW8 > esp_tempW7))
        {
        		esp_tempW7 = esp_tempW8;
        }
        else
        {
            ;
        }

        target_moment_f = target_moment_f_0 + esp_tempW6 + esp_tempW7; 
#else
        target_moment_f = target_moment_f_0 + esp_tempW6;
#endif

#if __MOMENT_TO_TARGET_SLIP_NEW
        cal_target_slip_front = esp_tempW6 ;
#else
        cal_target_slip_front = LCESP_s16IInter3Point( target_moment_f, 0, 0, m7, WHEEL_SLIP_20, m8, WHEEL_SLIP_40);
#endif  

/*****************2wheel *********************/
/*
#if __MOMENT_TO_TARGET_SLIP_NEW 

        esp_tempW0 = LCESP_s16FindRoadDependatGain(esp_mu, S16_OS_2_WHEEL_QM_ENT_H, S16_OS_2_WHEEL_QM_ENT_M, S16_OS_2_WHEEL_QM_ENT_L);   // Max.Frt Target_Slip
        esp_tempW9 = esp_tempW6 - esp_tempW0;    
        
        esp_tempW0 = LCESP_s16FindRoadDependatGain(esp_mu,WHEEL_SLIP_5 , WHEEL_SLIP_10, WHEEL_SLIP_10);    // Frt Current Slip 
                
        esp_tempW3 = LCESP_s16FindRoadDependatGain(esp_mu, S16_TWO_HMU_REAR_TAR_SLIP_WEG,
                                                           S16_TWO_MMU_REAR_TAR_SLIP_WEG,  
                                                           S16_TWO_LMU_REAR_TAR_SLIP_WEG);              
        
#else  
        cal_target_2wheel = LCESP_s16IInter3Point( target_moment_f, 0, 0, m7, WHEEL_SLIP_20, m9, WHEEL_SLIP_60);

        esp_tempW9 = cal_target_2wheel - cal_target_slip_front;

        esp_tempW0 = LCESP_s16FindRoadDependatGain(esp_mu,WHEEL_SLIP_3 , WHEEL_SLIP_5, WHEEL_SLIP_5);            

#endif 
        esp_tempW1 = LCESP_s16FindRoadDependatGain(esp_mu,WHEEL_SLIP_2, WHEEL_SLIP_2, WHEEL_SLIP_3);

        esp_tempW2 = LCESP_s16FindRoadDependatGain(esp_mu,S16_2WH_LMU_REAR_SLIP_MAX,
                            S16_2WH_MMU_REAR_SLIP_MAX, S16_2WH_HMU_REAR_SLIP_MAX);
                            
#if (__CRAM == 1)
        if((ldu1espDetectCRAM==1)&&(ldu1espDetectCRAM_FR==0))    // Over-, Outside Rear
        { 
        		
        		esp_tempW4 = (int16_t)( (((int32_t)esp_tempW9)*esp_tempW3)/100 );
            if(esp_tempW4 <= 0)
            {
                esp_tempW4 = 0;
            }
            else if(esp_tempW4 > esp_tempW2)
            {
                esp_tempW4 = esp_tempW2;
            }
            else
            {
                ;
            }
        		if(moment_pre_rear_cram > esp_tempW4)
        		{
            	del_target_2wheel_slip =  moment_pre_rear_cram ;    
            }
            else
            {
            	del_target_2wheel_slip  = esp_tempW4;
            }
            
        }        
        
    #if (__Pro_Active_Roll_Ctrl == 1)        
      #if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
        else if((ROP_REQ_ACT == 1)&&(RR_ESC.lcu1ropRearCtrlReq == 1))
      #else
        else if((ROP_REQ_ACT == 1)&&(lcu1ropRearCtrlReqRR == 1))
      #endif   
        {
            del_target_2wheel_slip =  S16_ROP_PROACTIVE_DEL_SLIP_R ;  // Tuning parameter...

            if(del_target_2wheel_slip <= 0)
            {
                del_target_2wheel_slip = 0;
            } 
            else
            {
                ;
            }
        }
    #endif 
    
        else if((MPRESS_BRAKE_ON == 0)&&(lcu1US2WHControlFlag==0)		//        else if((ABS_fz==0)
//           &&(((FLAG_TWO_WHEEL == 0)&&(slip_m_fr < (-esp_tempW0))&&(esp_tempW9 > 0))
           &&(((FLAG_TWO_WHEEL == 0)&&(esp_pressure_count_fr > 20)&&(esp_tempW9 > 0))           
           ||(FLAG_TWO_WHEEL == 1)))
        {
            del_target_2wheel_slip = (int16_t)( (((int32_t)esp_tempW9)*esp_tempW3)/100 );

            if(del_target_2wheel_slip <= 0)
            {
                del_target_2wheel_slip = 0;
            }
            else if(del_target_2wheel_slip > esp_tempW2)
            {
                del_target_2wheel_slip = esp_tempW2;
            }
            else
            {
                ;
            }
            
        }
        else
        {
            del_target_2wheel_slip = 0;
        }
#else
    #if (__Pro_Active_Roll_Ctrl == 1)        
      #if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
        if((ROP_REQ_ACT == 1)&&(RR_ESC.lcu1ropRearCtrlReq == 1))
      #else
        if((ROP_REQ_ACT == 1)&&(lcu1ropRearCtrlReqRR == 1))
      #endif        
        {
            del_target_2wheel_slip =  S16_ROP_PROACTIVE_DEL_SLIP_R ;  // Tuning parameter...

            if(del_target_2wheel_slip <= 0)
            {
                del_target_2wheel_slip = 0;
            } 
            else
            {
                ;
            }
        }
        
		#else        
        if(ROP_REQ_FRT_Pre == 1)
        {
            del_target_2wheel_slip = 0;
        }
    #endif 
   
        else if((MPRESS_BRAKE_ON == 0)&&(lcu1US2WHControlFlag==0)		//        else if((ABS_fz==0)
           &&(((FLAG_TWO_WHEEL == 0)&&(slip_m_fr < (-esp_tempW0))&&(esp_tempW9 > 0))
           ||(FLAG_TWO_WHEEL == 1)))
        {
            del_target_2wheel_slip = (int16_t)( (((int32_t)esp_tempW9)*esp_tempW3)/100 );

            if(del_target_2wheel_slip <= 0)
            {
                del_target_2wheel_slip = 0;
            }
            else if(del_target_2wheel_slip > esp_tempW2)
            {
                del_target_2wheel_slip = esp_tempW2;
            }
            else
            {
                ;
            }
        }
        else
        {
            del_target_2wheel_slip = 0;
        }
 #endif
*/

#if __MOMENT_TO_TARGET_SLIP_NEW
        del_target_slip_fr =  -(cal_target_slip_front);
#else
        del_target_slip_fr =  -(cal_target_slip_front - target_slip_f_0);
#endif   

        if((ESP_SPLIT==1)&&(MSL_BASE_rr==1))
        {
            if(del_target_slip_fr < (-WHEEL_SLIP_10))
            {
                del_target_slip_fr = -WHEEL_SLIP_10;
            }
            else
            {
                ;
            }
        }
        else
        {
            ;
        }
/*
#if (__CRAM == 1)
		    if(ldu1espDetectCRAM==0)
		    {  
		        if(del_target_2wheel_slip > 0)
		        {
		            FLAG_TWO_WHEEL = 1;
		            FLAG_TWO_WHEEL_RR = 1;
		        }
		        else
		        {
		            FLAG_TWO_WHEEL = 0;
		            FLAG_TWO_WHEEL_RR = 0;
		        }
		    }
		    else
		    {
		        if(del_target_2wheel_slip > 0)
		        {
		            FLAG_TWO_WHEEL = 1;
		            FLAG_TWO_WHEEL_RR = 1;
		        }
		        else  
		        {
		            FLAG_TWO_WHEEL = 0; 
		            FLAG_TWO_WHEEL_RR = 0;
		            SLIP_CONTROL_BRAKE_RR = 0;
		        }       
		    } 
    
        if(FLAG_TWO_WHEEL_RR == 1)
        {
            del_target_slip_rr = -del_target_2wheel_slip;
            
            if(ldu1espDetectCRAM==0)
                {
            del_target_slip_rl = 0;
                }
        
            SLIP_CONTROL_BRAKE_RR = 1;
        }
        else
        {
            ;
        }
#else
	
    #if (__Pro_Active_Roll_Ctrl == 1)        
    
      #if (__Pro_Active_ROP_Decel == 1)       
      
          if(del_target_2wheel_slip > 0)    
  		#else          
  		
  	      if((del_target_2wheel_slip > 0)&&(ROP_REQ_FRT_Pre_FR==0))   
      #endif  

    #else         
        if(del_target_2wheel_slip > 0)
            
    #endif  
        {
            FLAG_TWO_WHEEL = 1;
        } 
    #if (__Pro_Active_Roll_Ctrl == 1)        
        else if((FLAG_TWO_WHEEL == 1)&&(SLIP_CONTROL_NEED_FR==1)&&(ABS_fz==0)&&(ROP_REQ_FRT_Pre_FR==0))

    #else         
        else if((FLAG_TWO_WHEEL == 1)&&(SLIP_CONTROL_NEED_FR==1)&&(ABS_fz==0))
            
    #endif         
        	
        {
            FLAG_TWO_WHEEL = 1;
        }
        else
        {
            FLAG_TWO_WHEEL = 0;
        }
        if(FLAG_TWO_WHEEL == 1)
        {
            del_target_slip_rr = -del_target_2wheel_slip;
            del_target_slip_rl = 0;

            SLIP_CONTROL_BRAKE_RR = 1;
        }
        else
        {
            ;
        }
#endif
*/
    }
    else
    {
        del_target_slip_fr = 0;
    }

/*
    if((SLIP_CONTROL_BRAKE_FL ==0)&&(SLIP_CONTROL_BRAKE_FR==0))
    {
        FLAG_TWO_WHEEL = 0;
        
			#if (__CRAM == 1)&&( __Ext_Under_Ctrl == 0 )&&(__Pro_Active_Roll_Ctrl==0)
        FLAG_TWO_WHEEL_RL = 0;
        FLAG_TWO_WHEEL_RR = 0;
        del_target_slip_rl = 0;
        del_target_slip_rr = 0;
      #endif
    }
    else
    {
        ;
    }
*/    
/************************* rear ********************************/

#if __MOMENT_TO_TARGET_SLIP_NEW
 
  #if (__CRAM == 1)
    if((SLIP_CONTROL_BRAKE_RL == 1)&&(FLAG_TWO_WHEEL_RL == 0))
  #else
    if((SLIP_CONTROL_BRAKE_RL == 1)&&(FLAG_TWO_WHEEL == 0))
  #endif
    { 
        del_target_slip_rl =  -(McrAbs(delta_moment_q)); 
        
#else  
    	esp_tempW8 = DEL_TAR_SLIP_R_HMU_ALPHA_Min;
    	esp_tempW7 = DEL_TAR_SLIP_R_HMU_ALPHA_Max;
    	
    	esp_tempW5 = DEL_TAR_SLIP_R_MMU_ALPHA_Min;
    	esp_tempW4 = DEL_TAR_SLIP_R_MMU_ALPHA_Max;
    	
    	if(esp_tempW7 < esp_tempW8) esp_tempW7 =esp_tempW8+100;
     	if(esp_tempW4 < esp_tempW5) esp_tempW4 =esp_tempW5+100;
     	    	
    	if(esp_tempW5 < 100) esp_tempW5 =100;
    	if(esp_tempW8 < 100) esp_tempW8 =100;
  
     	if(esp_tempW4 > 2000) esp_tempW4 =2000;
    	if(esp_tempW7 > 2000) esp_tempW7 =2000;
    	   	   
    	m1 = LCESP_s16IInter2Point( alpha_r, esp_tempW8, 1000, esp_tempW7, 2000); //10 %,  high mu model 
    	m2 = LCESP_s16IInter2Point( alpha_r, esp_tempW8, 4000, esp_tempW7, 6000); //20%
   	
    	m4 = LCESP_s16IInter2Point( alpha_r, esp_tempW5, 1000, esp_tempW4, 2000); //10%,  low mu model 
    	m5 = LCESP_s16IInter2Point( alpha_r, esp_tempW5, 4000, esp_tempW4, 6000); //20%
    	
    	m7 = LCESP_s16IInter2Point( det_alatm, lsesps16MuMedHigh, m4, lsesps16MuHigh, m1); //10%
    	m8 = LCESP_s16IInter2Point( det_alatm, lsesps16MuMedHigh, m5, lsesps16MuHigh, m2); //20%
    	
#if (__CRAM == 1)
    if((SLIP_CONTROL_BRAKE_RL == 1)&&(FLAG_TWO_WHEEL_RL == 0))
#else
    if((SLIP_CONTROL_BRAKE_RL == 1)&&(FLAG_TWO_WHEEL == 0))
#endif
    {
   	
        target_slip_r_0 = McrAbs(slip_m_rl_current);
        if(slip_m_rl_current < 0)
        {
	            target_moment_r_0 = LCESP_s16IInter3Point( target_slip_r_0, 0, 0, WHEEL_SLIP_10, m7, WHEEL_SLIP_20, m8);

        }
        else
        {
            target_moment_r_0 = 0;
        }

        target_moment_r = target_moment_r_0+McrAbs(delta_moment_q);

        cal_target_slip_rear = LCESP_s16IInter3Point( target_moment_r, 0, 0, m7, WHEEL_SLIP_10, m8, WHEEL_SLIP_20);

        del_target_slip_rl =  -(cal_target_slip_rear - target_slip_r_0);
#endif    

    }
    else
    {
#if (__CRAM == 1)
        if( FLAG_TWO_WHEEL_RL ==0)
#else
        if( FLAG_TWO_WHEEL ==0)
#endif
        {
            del_target_slip_rl = 0;
        }
        else
        {
            ;
        }
    }
   
#if __MOMENT_TO_TARGET_SLIP_NEW

#if (__CRAM == 1)
   if((SLIP_CONTROL_BRAKE_RR == 1)&&(FLAG_TWO_WHEEL_RR == 0))
#else
   if((SLIP_CONTROL_BRAKE_RR == 1)&&(FLAG_TWO_WHEEL == 0))
#endif
    {
        del_target_slip_rr =  -(McrAbs(delta_moment_q));
#else     
    
#if (__CRAM == 1)
   if((SLIP_CONTROL_BRAKE_RR == 1)&&(FLAG_TWO_WHEEL_RR == 0))
#else
   if((SLIP_CONTROL_BRAKE_RR == 1)&&(FLAG_TWO_WHEEL == 0))
#endif
    {
        target_slip_r_0 = McrAbs(slip_m_rr_current);
        if(slip_m_rr_current < 0)
        {
            target_moment_r_0 = LCESP_s16IInter3Point( target_slip_r_0, 0, 0, WHEEL_SLIP_10, m7, WHEEL_SLIP_20, m8);

        }
        else
        {
            target_moment_r_0 = 0;
        }

        target_moment_r = target_moment_r_0+McrAbs(delta_moment_q);

        cal_target_slip_rear = LCESP_s16IInter3Point( target_moment_r, 0, 0, m7, WHEEL_SLIP_10, m8, WHEEL_SLIP_20);

        del_target_slip_rr =  -(cal_target_slip_rear - target_slip_r_0);
#endif    
    }
    else
    {
#if (__CRAM == 1)
        if( FLAG_TWO_WHEEL_RR ==0)
#else
        if( FLAG_TWO_WHEEL ==0)
#endif
        {
            del_target_slip_rr = 0;
        }
        else
        {
            ;
        }
    }
    
}

#endif

void LCESP_vCalTargetPressPara(void)
{ 
    C1_front = 142;
    C1_rear  = 162;
    
    C2_front = 228;
    C2_rear  = 213;   /* T300 Base */
} 

void LCESP_vCalCntrStatusESC(void)
{
	SLIP_CONTROL_BRAKE_FL_OLD = SLIP_CONTROL_BRAKE_FL;
	SLIP_CONTROL_BRAKE_FR_OLD = SLIP_CONTROL_BRAKE_FR;
	#if(__ESC_TCMF_CONTROL ==1)
	SLIP_CONTROL_BRAKE_RL_OLD = SLIP_CONTROL_BRAKE_RL;
	SLIP_CONTROL_BRAKE_RR_OLD = SLIP_CONTROL_BRAKE_RR;
	#endif
	
    LCESP_vCalEnterCondition();
    LCESP_vCalExitCondition();

    esp_tempW8 = McrAbs(q_front_enter_gain) * 100;
    esp_tempW7 = McrAbs(q_front_exit_gain) * 100;

#if (__MGH60_ESC_IMPROVE_CONCEPT == 1)
    esp_tempW10 = McrAbs(q_rear_enter_gain) * 100;
    esp_tempW11 = McrAbs(q_rear_exit_gain) * 100;
#endif

#if (__MGH80_ESC_ENTER_EXIT_IMPROVE == 1)
    /*** FL Control ***/
    if ( (SLIP_CONTROL_BRAKE_FL == 0)
       &&(delta_moment_q > 0)
       &&(yaw_sign < 0)
       &&(McrAbs(delta_moment_q) >= esp_tempW8) )
    {
        SLIP_CONTROL_BRAKE_FL = 1;
    }
    else if ( (SLIP_CONTROL_BRAKE_FL == 1)
            &&(delta_moment_q > 0)
            &&(yaw_sign < 0)
            &&(McrAbs(delta_moment_q) >= esp_tempW7) )
    {
        SLIP_CONTROL_BRAKE_FL = 1;
    }
    else
    {
        SLIP_CONTROL_BRAKE_FL = 0;
    }
    
    /*** FR Control ***/
    if ( (SLIP_CONTROL_BRAKE_FR == 0)
       &&(delta_moment_q < 0)
       &&(yaw_sign >= 0)
       &&(McrAbs(delta_moment_q) >= esp_tempW8) )
    {
        SLIP_CONTROL_BRAKE_FR = 1;
    }
    else if ( (SLIP_CONTROL_BRAKE_FR == 1)
            &&(delta_moment_q < 0)
            &&(yaw_sign >= 0)
            &&(McrAbs(delta_moment_q) >= esp_tempW7) )
    {
        SLIP_CONTROL_BRAKE_FR = 1;
    }
    else
    {
        SLIP_CONTROL_BRAKE_FR = 0;
    }
    
    /*** RL Control ***/
    if ( (SLIP_CONTROL_BRAKE_FL == 0)
       &&(SLIP_CONTROL_BRAKE_FR == 0)
       &&(SLIP_CONTROL_BRAKE_RL == 0)
       &&(delta_moment_q > 0)
       &&(yaw_sign >= 0)
       &&(McrAbs(delta_moment_q) >= esp_tempW10) )
    {
        SLIP_CONTROL_BRAKE_RL = 1;
    }
    else if ( (SLIP_CONTROL_BRAKE_FL == 0)
            &&(SLIP_CONTROL_BRAKE_FR == 0)
            &&(SLIP_CONTROL_BRAKE_RL == 1)
            &&(delta_moment_q > 0)
            &&(yaw_sign >= 0)
            &&(McrAbs(delta_moment_q) >= esp_tempW11) )
    {    
        SLIP_CONTROL_BRAKE_RL = 1;
    }     
    else   
    {      
        SLIP_CONTROL_BRAKE_RL = 0;
    }
    
    /*** RR Control ***/    
    if ( (SLIP_CONTROL_BRAKE_FL == 0)
       &&(SLIP_CONTROL_BRAKE_FR == 0)
       &&(SLIP_CONTROL_BRAKE_RR == 0)
       &&(delta_moment_q < 0)
       &&(yaw_sign < 0)
       &&(McrAbs(delta_moment_q) >= esp_tempW10) )
    {
        SLIP_CONTROL_BRAKE_RR = 1;
    }
    else if ( (SLIP_CONTROL_BRAKE_FL == 0)
            &&(SLIP_CONTROL_BRAKE_FR == 0)
            &&(SLIP_CONTROL_BRAKE_RR == 1)
            &&(delta_moment_q < 0)
            &&(yaw_sign < 0)
            &&(McrAbs(delta_moment_q) >= esp_tempW11)
            )
    {    
        SLIP_CONTROL_BRAKE_RR = 1;
    }     
    else   
    {      
        SLIP_CONTROL_BRAKE_RR = 0;
    }
    
    /* Over 2wheel */
    if((FLAG_TWO_WHEEL_RL ==1)||(lcu1OVER2WHFadeout_RL ==1)||(lcu1OVER2WHControlOld ==1))
    {
    	  if(FLAG_TWO_WHEEL_RL ==1) //((SLIP_CONTROL_BRAKE_FL ==1)&&(FLAG_TWO_WHEEL_RL==1))
        {
        	  SLIP_CONTROL_BRAKE_RL = 1;
        }
        else if((lcu1OVER2WHFadeout_RL ==1)||(lcu1OVER2WHControlOld ==1))
        {
        	  SLIP_CONTROL_BRAKE_RL = 0;
        }
        else
        {
        	  ;
        }
    }
    else
    {
    	  ;
    }
    
    if((FLAG_TWO_WHEEL_RR ==1)||(lcu1OVER2WHFadeout_RR ==1)||(lcu1OVER2WHControlOld ==1))
    {
    	  if(FLAG_TWO_WHEEL_RR ==1) //((SLIP_CONTROL_BRAKE_FR ==1)&&(FLAG_TWO_WHEEL_RR==1))
        {    
            SLIP_CONTROL_BRAKE_RR = 1;
        }     
        else if((lcu1OVER2WHFadeout_RR ==1)||(lcu1OVER2WHControlOld ==1))
        {      
            SLIP_CONTROL_BRAKE_RR = 0;
        }
        else
        {
        	  ;
        }
    }
    else   
    {      
        ;
    }
         
#else

    if(OUT_BRAKE_CONTROL == 1)
    {
        esp_tempW0 = esp_tempW7;
    }
    else
    {
        esp_tempW0 = esp_tempW8;
    }

#if (__MGH60_ESC_IMPROVE_CONCEPT == 1)
    if(IN_BRAKE_CONTROL==1)
    {
        esp_tempW1 = esp_tempW11;

    }
    else
    {
        esp_tempW1 = esp_tempW10;
    }
#else
    if(IN_BRAKE_CONTROL==1)
    {
        esp_tempW1 = esp_tempW7;

    }
    else
    {
        esp_tempW1 = esp_tempW8;
    }
#endif
    
    if(McrAbs(delta_moment_q) >= esp_tempW0)
    {
        if((delta_moment_q<0)&&(yaw_sign >= 0))
        {
            OUT_BRAKE_CONTROL =1 ;

            SLIP_CONTROL_BRAKE_FL=0;
            SLIP_CONTROL_BRAKE_FR=1;

        }
        else if((delta_moment_q > 0)&&(yaw_sign < 0))
        {
            OUT_BRAKE_CONTROL =1 ;

            SLIP_CONTROL_BRAKE_FL=1;
            SLIP_CONTROL_BRAKE_FR=0;
        }
        else
        {
            OUT_BRAKE_CONTROL = 0;
            SLIP_CONTROL_BRAKE_FL=0;
            SLIP_CONTROL_BRAKE_FR=0;
        }
    }
    else
    {
        OUT_BRAKE_CONTROL = 0;
        SLIP_CONTROL_BRAKE_FL=0;
        SLIP_CONTROL_BRAKE_FR=0;
    }

    if((McrAbs(delta_moment_q) >= esp_tempW1)&&(OUT_BRAKE_CONTROL==0))
    {
        if((delta_moment_q > 0)&&(yaw_sign >= 0))
        {
            IN_BRAKE_CONTROL = 1 ;

            SLIP_CONTROL_BRAKE_RL=1;
            SLIP_CONTROL_BRAKE_RR=0;
        }
        else if((delta_moment_q < 0)&&(yaw_sign < 0))
        {
            IN_BRAKE_CONTROL = 1 ;

            SLIP_CONTROL_BRAKE_RL=0;
            SLIP_CONTROL_BRAKE_RR=1;
        }
        else
        {
            IN_BRAKE_CONTROL = 0 ;
            SLIP_CONTROL_BRAKE_RL=0;
            SLIP_CONTROL_BRAKE_RR=0;
        }
    }
    else
    {
        IN_BRAKE_CONTROL = 0 ;
        SLIP_CONTROL_BRAKE_RL=0;
        SLIP_CONTROL_BRAKE_RR=0;
    }
#endif

#if (__CRAM == 1)
    if(ldu1espDetectCRAM == 1)
    {
        if(moment_pre_front_cram > 0)
        {
            SLIP_CONTROL_BRAKE_FL = 1;
        }
        else if(moment_pre_front_cram < 0)
        {
            SLIP_CONTROL_BRAKE_FR = 1;
        }
        else
        {
            ;
        }
    }
    else
    {
        ;
    }

	#if (__CRAM_PRE_FRONT_W_CRTL == 1) 
    if(lcu1espCramPreactFRT == 1)
    {
        if(moment_pre_front_cram_preact > 0)
        {
            SLIP_CONTROL_BRAKE_FL = 1;
        }
        else if(moment_pre_front_cram_preact < 0)
        {
            SLIP_CONTROL_BRAKE_FR = 1;
        }
        else
        {
            ;
        }
    }
    else
    {
        ;
    }
	#endif		/* __CRAM_PRE_FRONT_W_CRTL */
#endif			/* __CRAM */

#if __ROP
    if(ROP_REQ_FRT_Pre == 1)
    {
        if(moment_pre_front_rop > 0)
        {
            SLIP_CONTROL_BRAKE_FL = 1;
        }
        else if(moment_pre_front_rop < 0)
        {
            SLIP_CONTROL_BRAKE_FR = 1;
        }
        else
        {
            ;
        }
    }
    else
    {
        ;
    }
    
#if (__Pro_Active_ROP_Decel == 1)  
    
        if(lcu1ropFrtCtrlReqFL==1)
        {
            SLIP_CONTROL_BRAKE_FL = 1;
        }
        else if(lcu1ropFrtCtrlReqFR==1)
        {
            SLIP_CONTROL_BRAKE_FR = 1;
        }
        else
        {
            ;
        }    
      
#endif
    
#endif

#if ( __Ext_Under_Ctrl == 1 )

    if(EXTREME_UNDER_CONTROL == 1)
    { 

        	if ( Extr_Under_Frt_Moment_FL >= 500 )
      		{    	
          	SLIP_CONTROL_BRAKE_FL = 1; 
      		}
          else
        	{
        		;
        	}
        	
        	if ( Extr_Under_Frt_Moment_FR >= 500 )
      		{    	
          	SLIP_CONTROL_BRAKE_FR = 1; 
      		}
          else
        	{
        		;
        	}        	
        		
//            SLIP_CONTROL_BRAKE_FL = 1; 
//            SLIP_CONTROL_BRAKE_FR = 1; 
    }
    else
    {
    	;
            //SLIP_CONTROL_BRAKE_FL = 0; 
            //SLIP_CONTROL_BRAKE_FR = 0; 
    }
#endif

    if(lcu1US2WHControlFlag == 1)
    { 

    	if ( lcu1US2WHControlFlag_RL == 1 )
  		{    	
      		SLIP_CONTROL_BRAKE_FL = 1; 
  		}
      	else
    	{
    		;
    	}
    	
    	if ( lcu1US2WHControlFlag_RR == 1 )
  		{    	
      		SLIP_CONTROL_BRAKE_FR = 1; 
  		}
      	else
    	{
    		;
    	}     	 
    }
    else
    {
    	;  
    }

}

void LCESP_vMonitoringESC(void)
{
    slip_target_fl = del_target_slip_fl;
    slip_target_fr = del_target_slip_fr;
    slip_target_rl = del_target_slip_rl;
    slip_target_rr = del_target_slip_rr;

#if (__CRAM == 1)
    FLAG_TWO_WHEEL = FLAG_TWO_WHEEL_RL | FLAG_TWO_WHEEL_RR ; 
#endif

    ACT_TWO_WHEEL = FLAG_TWO_WHEEL;

    SLIP_CONTROL_NEED_FL = SLIP_CONTROL_BRAKE_FL;
    SLIP_CONTROL_NEED_FR = SLIP_CONTROL_BRAKE_FR;
    SLIP_CONTROL_NEED_RL = SLIP_CONTROL_BRAKE_RL;
    SLIP_CONTROL_NEED_RR = SLIP_CONTROL_BRAKE_RR;
    
#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
    FL_ESC.SLIP_CONTROL_NEED = SLIP_CONTROL_BRAKE_FL;
    FR_ESC.SLIP_CONTROL_NEED = SLIP_CONTROL_BRAKE_FR;
    RL_ESC.SLIP_CONTROL_NEED = SLIP_CONTROL_BRAKE_RL;
    RR_ESC.SLIP_CONTROL_NEED = SLIP_CONTROL_BRAKE_RR;
#endif

    YAW_CDC_WORK_O=SLIP_CONTROL_NEED_FL|SLIP_CONTROL_NEED_FR|SLIP_CONTROL_NEED_RL|SLIP_CONTROL_NEED_RR;
    YAW_CDC_WORK=YAW_CDC_WORK_O;

}

#endif
#endif

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCESPCallControl
	#include "Mdyn_autosar.h"
#endif
