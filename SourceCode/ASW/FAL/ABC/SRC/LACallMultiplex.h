#ifndef __LACALLMULTIPLEX_H__
#define __LACALLMULTIPLEX_H__

/*Includes *********************************************************************/
#include "LVarHead.h"

/*Global MACRO CONSTANT Definition *********************************************/

#define	PRIMARY_TARGET_FIRST	1
#define	SECONDARY_TARGET_FIRST	2

/*Global MACRO FUNCTION Definition *********************************************/

/*Global Type Declaration ******************************************************/

typedef struct
{
	uint8_t u8ApplyMode;
	uint8_t u8DumpMode;
	uint8_t u8HoldMode;

}WL_MULTIPLEX;

/*Global Extern Variable Declaration *******************************************/
extern	WL_MULTIPLEX	FL_MULTI, FR_MULTI, RL_MULTI, RR_MULTI;

extern	int16_t laidbs16PriTP, laidbs16SecTP, laidbs16MasterTP;
extern	uint8_t	laidbu8Priority;

/*Global Extern Functions Declaration ******************************************/
extern	void LA_vMultiplexMain(void);
extern	void LA_vCircMultiMain(void);


#endif /*__LACALLMULTIPLEX_H__*/
