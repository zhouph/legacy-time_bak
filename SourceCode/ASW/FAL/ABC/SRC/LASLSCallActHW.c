/******************************************************************************
* Project Name: Straight Line Stability Control
* File: LDSLSCallActHW.C
* Date: 06 March 2008
******************************************************************************/

/* Includes ******************************************************************/

/* AUTOSAR --------------------------*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START                       
	#define idx_FILE	idx_CL_LASLSCallActHW
	#include "Mdyn_autosar.h"               
#endif
/* AUTOSAR --------------------------*/


#include "LCSLSCallControl.h"
#include "LASLSCallActHW.h"
#include "LACallMain.h"
#include "LAABSGenerateLFCDuty.h" 
 
#if __SLS
/* Logcal Definiton  *********************************************************/
#define SLS_HV_VL_rl                        laslsValveRL.u1SlsHvVlAct
#define SLS_AV_VL_rl                        laslsValveRL.u1SlsAvVlAct
#define SLS_HV_VL_rr                        laslsValveRR.u1SlsHvVlAct
#define SLS_AV_VL_rr                        laslsValveRR.u1SlsAvVlAct

/* Variables Definition*******************************************************/

LASLS_VALVE_t laslsValveRL, laslsValveRR;
WHEEL_VV_OUTPUT_t lasls_RL1HP, lasls_RL2HP, lasls_RR1HP, lasls_RR2HP;

/* LocalFunction prototype ***************************************************/
void LASLS_vCallActHW(void);
static void LASLS_vCallActValve(void);
static void LASLS_vDecideValveOutput(int16_t s16SlsActiveWl, int16_t s16SlsFadeOutWl, int8_t s8RateCnt, int16_t s16FadeOutCnt, 
                                     LASLS_VALVE_t *laSlsValve, WHEEL_VV_OUTPUT_t *laWL1HP,  WHEEL_VV_OUTPUT_t *laWL2HP);

/* Implementation*************************************************************/



/******************************************************************************
* FUNCTION NAME:      LASLS_vCallActHW
* CALLED BY:          LA_vCallMainValve()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Valve/Motor actuaction by Straight Line Stability Control
******************************************************************************/
void    LASLS_vCallActHW(void)
{
#if __SLS_ENABLE_BY_CALIBRATION == ENABLE
	if(lcslsu1CtrlEnable == 1)
#endif  

	{
	    LASLS_vDecideValveOutput(SLS_ACTIVE_rl, SLS_FADE_OUT_rl, SLS_rate_counter_rl, SLS_fade_counter_rl, 
	                             &laslsValveRL, &lasls_RL1HP, &lasls_RL2HP);
		
	    LASLS_vDecideValveOutput(SLS_ACTIVE_rr, SLS_FADE_OUT_rr, SLS_rate_counter_rr, SLS_fade_counter_rr,  
	                             &laslsValveRR, &lasls_RR1HP, &lasls_RR2HP);
		
	    LASLS_vCallActValve();
	}
}

static void LASLS_vCallActValve(void)
{
    
/*    if (((SLS_ACTIVE_rl==1)||(SLS_FADE_OUT_rl==1))&&(RL.EBD_MODE!=DUMP_START)) */
    if ((SLS_ACTIVE_rl==1)||(SLS_FADE_OUT_rl==1))   
    {
        HV_VL_rl = SLS_HV_VL_rl;
        AV_VL_rl = SLS_AV_VL_rl;
        
        /* Temporary until duty determination module restructuring */
        if((HV_VL_rl==0) && (AV_VL_rl==0))
        {
        	RL.LFC_Conven_OnOff_Flag = 1;
    }
    else
    {
        	RL.LFC_Conven_OnOff_Flag = 0;
        }
        /* Temporary until duty determination module restructuring */

		la_RL1HP = lasls_RL1HP; 
		la_RL2HP = lasls_RL2HP;       
    }
    else{}

/*    if (((SLS_ACTIVE_rr==1)||(SLS_FADE_OUT_rr==1))&&(RR.EBD_MODE!=DUMP_START)) */
    if ((SLS_ACTIVE_rr==1)||(SLS_FADE_OUT_rr==1))
    {
        HV_VL_rr = SLS_HV_VL_rr;
        AV_VL_rr = SLS_AV_VL_rr;
        
        /* Temporary until duty determination module restructuring */
        if((HV_VL_rr==0) && (AV_VL_rr==0))
        {
        	RR.LFC_Conven_OnOff_Flag = 1;
    }
    else
    {
        	RR.LFC_Conven_OnOff_Flag = 0;
        }
        /* Temporary until duty determination module restructuring */

		la_RR1HP = lasls_RR1HP; 
		la_RR2HP = lasls_RR2HP;       
    }
    else{}
}

static void LASLS_vDecideValveOutput(int16_t s16SlsActiveWl, int16_t s16SlsFadeOutWl, int8_t s8RateCnt, int16_t s16FadeOutCnt, 
                                     LASLS_VALVE_t *laSlsValve, WHEEL_VV_OUTPUT_t *laWL1HP,  WHEEL_VV_OUTPUT_t *laWL2HP)
{
	
	if(s16SlsActiveWl == 1)
	{		
        if (s8RateCnt == 1)  
        {
    		laSlsValve->u1SlsHvVlAct = 1;
    		laSlsValve->u1SlsAvVlAct = 1;
    		
    		LA_vSetWheelVvOnOffDumpOutput(laWL1HP, laWL2HP, U8_SLS_DUMPSCAN_TIME_R);
    	}
    	else
        {
    		laSlsValve->u1SlsHvVlAct = 1;
    		laSlsValve->u1SlsAvVlAct = 0;
    		LA_vSetWheelVvOnOffHoldOutput(laWL1HP, laWL2HP);
    	}
	}
	else if(s16SlsFadeOutWl == 1)
	{	  
        if (SLS_target_fade_cnt == 0)
        {
            laSlsValve->u1SlsHvVlAct = 0;
            laSlsValve->u1SlsAvVlAct = 0;
            LA_vSetWheelVvOnOffRiseOutput(laWL1HP, laWL2HP, (U8_BASE_CTRLTIME*2));
        }	        
        else if (s16FadeOutCnt == 1)
        {
            laSlsValve->u1SlsHvVlAct = 0;
            laSlsValve->u1SlsAvVlAct = 0;
            LA_vSetWheelVvOnOffRiseOutput(laWL1HP, laWL2HP, (uint8_t)S8_SLS_REAPPLY_TIME_FADEOUT_R);
        }
        else
        {
            laSlsValve->u1SlsHvVlAct = 1;
            laSlsValve->u1SlsAvVlAct = 0;
	        LA_vSetWheelVvOnOffHoldOutput(laWL1HP, laWL2HP);
        }   		
	}
	else 
	{
		laSlsValve->u1SlsHvVlAct = 0;
		laSlsValve->u1SlsAvVlAct = 0;
		LA_vResetWheelValvePwmDuty(laWL1HP);
		LA_vResetWheelValvePwmDuty(laWL2HP);
	}

  #if defined (__ACTUATION_DEBUG)
	laWL1HP->u8debugger = 34;
	laWL2HP->u8debugger = 34;
  #endif /* defined (__ACTUATION_DEBUG) */

}
#endif

/* AUTOSAR --------------------------*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LASLSCallActHW
	#include "Mdyn_autosar.h"               
#endif
/* AUTOSAR --------------------------*/
