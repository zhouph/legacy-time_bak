#ifndef __LCEBPCALLCONTROL_H__
#define __LCEBPCALLCONTROL_H__

/*includes********************************************************************/
#include "LVarHead.h"

  #if __EBP
/*Global Type Declaration ****************************************************/
extern struct	U8_BIT_STRUCT_t EBPD0,EBPD1;

#define EBP_HV_VL_fl                        EBPC0.bit0
#define EBP_AV_VL_fl                        EBPC0.bit1
#define EBP_HV_VL_fr                        EBPC0.bit2
#define EBP_AV_VL_fr                        EBPC0.bit3
#define EBP_HV_VL_rl                        EBPC0.bit4
#define EBP_AV_VL_rl                        EBPC0.bit5
#define EBP_HV_VL_rr                        EBPC0.bit6
#define EBP_AV_VL_rr                        EBPC0.bit7

#define EBP_S_VALVE_PRIMARY                 EBPC1.bit0
#define EBP_S_VALVE_SECONDARY               EBPC1.bit1
#define EBP_TCL_DEMAND_PRIMARY              EBPC1.bit2
#define EBP_TCL_DEMAND_SECONDARY            EBPC1.bit3
#define EBP_MOTOR_ON                        EBPC1.bit4
#define EBP_ON                              EBPC1.bit5
#define EBP_ON_ESP                          EBPC1.bit6
#define EBP_flag_03_7                       EBPC1.bit7


/*Global Extern Variable  Declaration*****************************************/
extern U8_BIT_STRUCT_t EBPC0,EBPC1;
     #if __AHB_GEN3_SYSTEM == ENABLE || __IDB_LOGIC == ENABLE
extern int16_t lcebps16CtrlTargetPress;
     #endif /* __AHB_GEN3_SYSTEM == ENABLE */	

/*Global Extern Functions  Declaration****************************************/

  #endif
#endif
