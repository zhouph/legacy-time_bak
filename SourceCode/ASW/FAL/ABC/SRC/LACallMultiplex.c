/*******************************************************************************
* Project Name:     IDB
* File Name:        LACallMultiplex.c
* Description:      Multiplex Control
* Logic version:    
********************************************************************************
*  Modification Log
*  Date         Author          Description
*  -----        -----           -----
*  
********************************************************************************/

/* Includes ********************************************************************/

/* AUTOSAR --------------------------*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START                       
	#define idx_FILE	idx_CL_LACallMultiplex
	#include "Mdyn_autosar.h"               
#endif
/* AUTOSAR --------------------------*/

#include "LACallMultiplex.h"
#include "LACallMain.h"

/* Local Definiton ***********************************************************/
struct Multiplex_Flag_1
{
    uBitType u1MultiplexPri    :1;
    uBitType u1MultiplexSec    :1;
    uBitType u1SwitchTP    :1;
    uBitType bit4    :1;
    uBitType bit3    :1;
    uBitType bit2    :1;
    uBitType bit1    :1;
    uBitType bit0    :1;
}MP_Flag_1;	/*HSH_COMPILE Multiple Definition*/

#define	lau1MultiplexPri	MP_Flag_1.u1MultiplexPri
#define	lau1MultiplexSec	MP_Flag_1.u1MultiplexSec
#define	lau1SwitchTP	MP_Flag_1.u1SwitchTP

/* Global Variables Declaration************************************************/
WL_MULTIPLEX	FL_MULTI, FR_MULTI, RL_MULTI, RR_MULTI;

int16_t laidbs16PriTP, laidbs16SecTP, laidbs16MasterTP;
uint8_t	laidbu8Priority, laidbu8Priority_old, laidbu8Priority_sw, laidbu8Priority_tp, laidbu8Priority_tp_old;

/* Local Function prototype ***************************************************/
static	void LA_vDetWLPressStatus(struct W_STRUCT *WL, WL_MULTIPLEX *WL_MULTI);
static	void LA_vDetMultiplexControl(void);
static	void LA_vActMultiplexControl(void);
//static	void LA_vActMultiplexControlABS(void); 미구현
//static	void LA_vActMultiplexControlTCS(void); 미구현
//static	void LA_vActMultiplexControlESC(void); 미구현
static	void LA_vActMultiplexRiseFirst(void);
static	void LA_vActMultiplexDumpFirst(void);

static void LAIDB_vDecPriority(void);
uint8_t LAIDB_u8CntSwitch(uint16_t u16Cycle);
int16_t LAIDB_s16SelectTP(int16_t s16PriTP, int16_t s16SecTP, uint8_t u8Priority);

/* GlobalFunction prototype **************************************************/
void LA_vMultiplexMain(void);
void LA_vCircMultiMain(void);

/* Implementation *************************************************************/

/*******************************************************************************
* FUNCTION NAME:        LA_vMultiplexMain
* CALLED BY:            LA_vCallMainValve
* Preconditions:        
* PARAMETER:            
* RETURN VALUE:         
* Description:          
********************************************************************************/
void LA_vMultiplexMain(void)
{
	LA_vDetWLPressStatus(&FL, &FL_MULTI);
	LA_vDetWLPressStatus(&FR, &FR_MULTI);
	LA_vDetWLPressStatus(&RL, &RL_MULTI);
	LA_vDetWLPressStatus(&RR, &RR_MULTI);
	
	LA_vDetMultiplexControl();
	
	LA_vActMultiplexControl();
}

void LA_vDetWLPressStatus(struct W_STRUCT *WL, WL_MULTIPLEX *WL_MULTI)
{
	if((ABS_fz==1)||(BTCS_ON==1)||(YAW_CDC_WORK==1))
	{
		if((WL->AV_VL==0)&&(WL->HV_VL==0))
		{
			WL_MULTI->u8ApplyMode = 1;
			WL_MULTI->u8DumpMode = 0;
			WL_MULTI->u8HoldMode = 0;
		}
		else if((WL->AV_VL==0)&&(WL->HV_VL==1))
		{
			WL_MULTI->u8ApplyMode = 0;
			WL_MULTI->u8HoldMode = 1;
			WL_MULTI->u8DumpMode = 0;
		}
		else if((WL->AV_VL==1)&&(WL->HV_VL==1))
		{
			WL_MULTI->u8ApplyMode = 0;
			WL_MULTI->u8HoldMode = 0;
			WL_MULTI->u8DumpMode = 1;
		}
		else
		{
			WL_MULTI->u8ApplyMode = 0;
			WL_MULTI->u8HoldMode = 0;
			WL_MULTI->u8DumpMode = 0;
		}
	}
	else
	{
		WL_MULTI->u8ApplyMode = 0;
		WL_MULTI->u8HoldMode = 0;
		WL_MULTI->u8DumpMode = 0;
	}
		
}

void LA_vDetMultiplexControl(void)
{
	if(((FR_MULTI.u8ApplyMode==1)&&(RL_MULTI.u8DumpMode==1))
		||((FR_MULTI.u8DumpMode==1)&&(RL_MULTI.u8ApplyMode==1)))
	{
		lau1MultiplexPri=1;
	}
	else
	{
		lau1MultiplexPri=0;
	}
	
	if(((FL_MULTI.u8ApplyMode==1)&&(RR_MULTI.u8DumpMode==1))
			||((FL_MULTI.u8DumpMode==1)&&(RR_MULTI.u8ApplyMode==1)))
	{
		lau1MultiplexSec=1;
	}
	else
	{
		lau1MultiplexSec=0;
	}
}

static	void LA_vActMultiplexControl(void)
{
	if(BTCS_ON==1)
	{
		//LA_vActMultiplexControlTCS(); 미구현
		LA_vActMultiplexRiseFirst();
	}
	else{}
	
	if(YAW_CDC_WORK==1)
	{
		//LA_vActMultiplexControlESC(); 미구현
		LA_vActMultiplexRiseFirst();
	}
	else{}
	
	if(ABS_fz==1)
	{
		//LA_vActMultiplexControlABS(); 미구현
		LA_vActMultiplexDumpFirst();
	}
	else{}
}

static	void LA_vActMultiplexDumpFirst(void)
{
	/* X split */
	if(lau1MultiplexPri==1)
	{
		if(RL_MULTI.u8ApplyMode==1)
		{
			LA_vSetWheelVvOnOffHoldOutput(&la_RL1HP, &la_RL2HP);
			RL.HV_VL = 1;
			RL.AV_VL = 0;
		}
		else if(FR_MULTI.u8ApplyMode==1)
		{
			LA_vSetWheelVvOnOffHoldOutput(&la_FR1HP, &la_FR2HP);
			FR.HV_VL = 1;
			FR.AV_VL = 0;
		}
		else{}
	}
	
	if(lau1MultiplexSec==1)
	{
		if(RR_MULTI.u8ApplyMode==1)
		{
			LA_vSetWheelVvOnOffHoldOutput(&la_RR1HP, &la_RR2HP);
			RR.HV_VL = 1;
			RR.AV_VL = 0;
		}
		else if(FL_MULTI.u8ApplyMode==1)
		{
			LA_vSetWheelVvOnOffHoldOutput(&la_FL1HP, &la_FL2HP);
			FL.HV_VL = 1;
			FL.AV_VL = 0;
		}
		else{}
	}
}

static	void LA_vActMultiplexRiseFirst(void)
{
	/* X split */
	if(lau1MultiplexPri==1)
	{
		if(RL_MULTI.u8DumpMode==1)
		{
			LA_vSetWheelVvOnOffHoldOutput(&la_RL1HP, &la_RL2HP);
			RL.HV_VL = 1;
			RL.AV_VL = 0;
		}
		else if(FR_MULTI.u8DumpMode==1)
		{
			LA_vSetWheelVvOnOffHoldOutput(&la_FR1HP, &la_FR2HP);
			FR.HV_VL = 1;
			FR.AV_VL = 0;
		}
		else{}
	}
			
	if(lau1MultiplexSec==1)
	{
		if(RR_MULTI.u8DumpMode==1)
		{
			LA_vSetWheelVvOnOffHoldOutput(&la_RR1HP, &la_RR2HP);
			RR.HV_VL = 1;
			RR.AV_VL = 0;
		}
		else if(FL_MULTI.u8DumpMode==1)
		{
			LA_vSetWheelVvOnOffHoldOutput(&la_FL1HP, &la_FL2HP);
			FL.HV_VL = 1;
			FL.AV_VL = 0;
		}
		else{}
	}	
}

void LA_vCircMultiMain(void)
{
	LAIDB_vDecPriority();
	laidbs16MasterTP = LAIDB_s16SelectTP(laidbs16PriTP, laidbs16SecTP, laidbu8Priority);
}

static void LAIDB_vDecPriority(void)
{
	static	uint16_t	u16PriorityCnt;
	
	laidbu8Priority_old = laidbu8Priority;
	laidbu8Priority_tp_old = laidbu8Priority_tp;
	/* Decision Module */
	if(laidbs16PriTP>=laidbs16SecTP)
	{
		laidbu8Priority_tp = PRIMARY_TARGET_FIRST;
	}
	else if(laidbs16PriTP<laidbs16SecTP)
	{
		laidbu8Priority_tp = SECONDARY_TARGET_FIRST;
	}
	else{}
	
	/* Switching Module */
	if(laidbu8Priority_tp_old==laidbu8Priority_tp)
	{
		u16PriorityCnt = u16PriorityCnt + 1;
		if(u16PriorityCnt>=L_U8_TIME_10MSLOOP_500MS)
		{
			u16PriorityCnt=0;
			lau1SwitchTP^=1;
			if(laidbu8Priority==SECONDARY_TARGET_FIRST)
			{
				laidbu8Priority_sw = PRIMARY_TARGET_FIRST;
			}
				else if(laidbu8Priority==PRIMARY_TARGET_FIRST)
			{
				laidbu8Priority_sw = SECONDARY_TARGET_FIRST;
			}
			else{}
		}
		else{}
	}
	else
	{
		u16PriorityCnt = 0;
		lau1SwitchTP = 0;
	}
	
	if(lau1SwitchTP==1)
	{
		laidbu8Priority = laidbu8Priority_sw;
	}
	else
	{
		laidbu8Priority = laidbu8Priority_tp;
	}
}

uint8_t LAIDB_u8CntSwitch(uint16_t u16Cycle)
{
	uint8_t	u8Out=0;
	
	static	uint16_t	u16CntSwitch;
	
	u16CntSwitch = u16CntSwitch + 1;
	
	if(u16CntSwitch>=u16Cycle)
	{
		u8Out = 1;
		u16CntSwitch = 0;
	}
	
	if(u16Cycle==0)
	{
		u16CntSwitch = 0;
		u8Out = 0;
	}
	
	return u8Out;
}

int16_t LAIDB_s16SelectTP(int16_t s16PriTP, int16_t s16SecTP, uint8_t u8Priority)
{	
	int16_t s16TargetTP;
	
	if(u8Priority==PRIMARY_TARGET_FIRST)
	{
		s16TargetTP = s16PriTP;
	}
	else if(u8Priority==SECONDARY_TARGET_FIRST)
	{
		s16TargetTP = s16SecTP;
	}
	else{}
	
	return s16TargetTP;
}

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LACallMultiplex
	#include "Mdyn_autosar.h"               
#endif
