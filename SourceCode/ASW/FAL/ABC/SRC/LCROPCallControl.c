
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LCROPCallControl
	#include "Mdyn_autosar.h"
#endif


#include "LCESPCalInterpolation.h"
#include "LCESPCalLpf.h"
#include "LCallMain.h"
 
#include "LDROPCallDetection.h"
#include "LCESPInterfaceSlipController.h"

#if __ROP
void LCROP_vCallControl(void);
void LCROP_vCompROPMoment(void);
void LCROP_vInterfaceMoment(void);
void LCROP_vControlEngine(void);

   

int16_t    rop_torq ;
int16_t    m_q_ay, m_q_ay_old ;
//int16_t    abs_delta_alat, alat_dot_org, alat_dot_lf_old, alat_dot_lf, alat_dot_rop ; 
int16_t    abs_alat_rop_lf, abs_alat_rop_lf_old, abs_alat_rop, rop_frt_pre_ctrl_fl, rop_frt_pre_ctrl_fr ;

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//int8_t   rop_rear_ctrl_mode_rl, rop_rear_ctrl_mode_rr ;
#else
int8_t   rop_rear_ctrl_mode_rl, rop_rear_ctrl_mode_rr ;
#endif 

 

    #define ST_LIMIT_F_OVERSTEER_OUT_ROP    40
         
    #define ROP_TORQ_DOWN_RATE		20	 /*2.0% engine torque down per 7 msec*/
    #define ROP_TORQ_RISE_RATE		5	 /*0.5% engine torque rise per 7 msec*/

#endif	

#if __ROP
void LCROP_vCallControl(void)
{
    LCROP_vControlEngine();		                 
    LCROP_vCompROPMoment();
}

void LCROP_vCompROPMoment(void)
{          
    abs_alat_rop_lf_old = abs_alat_rop_lf ;
    alat_dot_lf_old = alat_dot_lf ;     
    
    abs_alat_rop = McrAbs(alat) ;
         
    if ( abs_alat_rop >= abs_alat_rop_lf )
    {
        abs_alat_rop_lf = LCESP_s16Lpf1Int(abs_alat_rop, abs_alat_rop_lf_old,117);           
    }
    else
    {
        abs_alat_rop_lf = LCESP_s16Lpf1Int(abs_alat_rop, abs_alat_rop_lf_old,15);   
    }        

    /* tempW2 = abs_alat_rop_lf - abs_alat_rop_lf_old ; tempW1 = 1000; tempW0 = 7 ;     
    s16muls16divs16();
    alat_dot_org = tempW3;          */
    alat_dot_org = (int16_t)( (((int32_t)(abs_alat_rop_lf - abs_alat_rop_lf_old))*1000)/10 );  /*  r: 0.001 [g/sec] */
    
    if ( ROP_DYN_STABLE_PHASE == 1 )
    {
        esp_tempW1 = LCESP_s16IInter5Point( vrefk,  VREF_K_20_KPH,  S16_AY_REF_DYN_WEG_VEL_20K, 	
        						                    VREF_K_40_KPH,  S16_AY_REF_DYN_WEG_VEL_40K,
    					                            VREF_K_60_KPH,  S16_AY_REF_DYN_WEG_VEL_60K,
    					                            VREF_K_80_KPH,  S16_AY_REF_DYN_WEG_VEL_80K,					                            
    					                            VREF_K_100_KPH,  S16_AY_REF_DYN_WEG_VEL_100K ) ;	               
    }
    else
    {
        esp_tempW1 = LCESP_s16IInter5Point( vrefk,  VREF_K_20_KPH,  S16_AY_REF_WEG_VEL_20K, 	
        						                    VREF_K_40_KPH,  S16_AY_REF_WEG_VEL_40K,
    					                            VREF_K_60_KPH,  S16_AY_REF_WEG_VEL_60K,
    					                            VREF_K_80_KPH,  S16_AY_REF_WEG_VEL_80K,					                            
    					                            VREF_K_100_KPH,  S16_AY_REF_WEG_VEL_100K ) ;	     
    }        
    
/*   TTL --> Ay_ref Offset Determine... */    

    esp_tempW4 = S16_ROP_MU_LEVEL_AY - S16_AY_REF_MIN_OFFSET ; 
    esp_tempW5 = S16_ROP_MU_LEVEL_AY + S16_AY_REF_MAX_OFFSET ;
    
    esp_tempW6 = LCESP_s16IInter3Point( TTL_time,  S16_TTL_min,     esp_tempW4, 	
        						                   S16_TTL_thres,   S16_ROP_MU_LEVEL_AY,			                            
    					                           S16_TTL_max,     esp_tempW5 ) ;	
           
    /* tempW2 = esp_tempW6; 
    tempW1 = esp_tempW1; 
    tempW0 = 100 ;     
    s16muls16divs16();
    esp_tempW2 = tempW3;    	  */ 
    esp_tempW2 = (int16_t)( (((int32_t)esp_tempW6)*esp_tempW1)/100 ); 
    
    abs_delta_alat = abs_alat_rop_lf - esp_tempW2 ;	 
    
    alat_dot_lf = LCESP_s16Lpf1Int(alat_dot_org, alat_dot_lf_old,L_U8FILTER_GAIN_10MSLOOP_1HZ);  /* Fc = 2 Hz, 80 msec */
    
    if ( abs_delta_alat >= 0 )          /* Outside */
    {                
        if ( alat_dot_lf >= 0 )         /* Increase */
        {        
            esp_tempW7 = S16_AYC_P_GAIN_OUTSIDE_INC ;   /* P-gain */
            esp_tempW8 = S16_AYC_D_GAIN_OUTSIDE_INC ;   /* D-gain */
            
            if (Flg_1st_cycle_Ay==1)
            {
                /* tempW2 = esp_tempW7 ; tempW1 = S16_AYC_1ST_CYCLE_P_OUT_WEG; tempW0 = 100 ;   
                s16muls16divs16();
                esp_tempW7 = tempW3;         */ 
                esp_tempW7 = (int16_t)( (((int32_t)esp_tempW7)*S16_AYC_1ST_CYCLE_P_OUT_WEG)/100 );  /* P-gain */     
                   
                /* tempW2 = esp_tempW8 ; tempW1 = S16_AYC_1ST_CYCLE_D_OUT_WEG; tempW0 = 100 ;     
                s16muls16divs16();
                esp_tempW8 = tempW3;         */
                esp_tempW8 = (int16_t)( (((int32_t)esp_tempW8)*S16_AYC_1ST_CYCLE_D_OUT_WEG)/100 );  /* D-gain  */
            }
            else
            {
                ;
            }                
            
        }
        else                            /* Decrease */
        {
            esp_tempW7 = S16_AYC_P_GAIN_OUTSIDE_DEC ;   /* P-gain  */          
            esp_tempW8 = S16_AYC_D_GAIN_OUTSIDE_DEC ;   /* D-gain  */
        }        
    }
    else        /* inside */
    {   
        if ( alat_dot_lf >= 0 )         /* Increase */
        {        
            esp_tempW7 = S16_AYC_P_GAIN_INSIDE_INC ;   /* P-gain */
            esp_tempW8 = S16_AYC_D_GAIN_INSIDE_INC ;   /* D-gain */
            
            if (Flg_1st_cycle_Ay==1)
            {
                /* tempW2 = esp_tempW7 ; tempW1 = S16_AYC_1ST_CYCLE_P_IN_WEG; tempW0 = 100 ;   
                s16muls16divs16();
                esp_tempW7 = tempW3;            */  
                esp_tempW7 = (int16_t)( (((int32_t)esp_tempW7)*S16_AYC_1ST_CYCLE_P_IN_WEG)/100 ); /* P-gain */ 
                   
                /* tempW2 = esp_tempW8 ; tempW1 = S16_AYC_1ST_CYCLE_D_IN_WEG; tempW0 = 100 ;   
                s16muls16divs16();
                esp_tempW8 = tempW3;         */
                esp_tempW8 = (int16_t)( (((int32_t)esp_tempW8)*S16_AYC_1ST_CYCLE_D_IN_WEG)/100 );  /* D-gain */
            }
            else
            {
                ;
            }       
            
        }
        else                            /* Decrease */
        {
            esp_tempW7 = S16_AYC_P_GAIN_INSIDE_DEC ;   /* P-gain  */
            esp_tempW8 = S16_AYC_D_GAIN_INSIDE_DEC ;   /* D-gain  */
        }       
    }
    
    /* tempW2 = abs_delta_alat ; tempW1 = esp_tempW7; tempW0 = 10 ;  
    s16muls16divs16();
    esp_tempW5 = tempW3;   */
    esp_tempW5 = (int16_t)( (((int32_t)abs_delta_alat)*esp_tempW7)/10 );  /* P-Control */
    
    /* tempW2 = alat_dot_lf ; tempW1 = esp_tempW8; tempW0 = 100 ;  
    s16muls16divs16();
    esp_tempW6 = tempW3;   */
    esp_tempW6 = (int16_t)( (((int32_t)alat_dot_lf)*esp_tempW8)/100 );  /* D-Control */
    
    /* tempW2 = McrAbs(alat) ; tempW1 = S16_AY_P_GAIN_FED_FOWR; tempW0 = 50 ;  
    s16muls16divs16();
    esp_tempW9 = tempW3;        */
    esp_tempW9 = (int16_t)( (((int32_t)(McrAbs(alat)))*S16_AY_P_GAIN_FED_FOWR)/50 );  /* Feed-Forward P-Control */
     
/*    m_q_ay = esp_tempW5 + esp_tempW6 + esp_tempW9 ;    */
    esp_tempW7 = esp_tempW5 + esp_tempW6 + esp_tempW9 ;   
    
    if( esp_tempW7 >= 2000 ) 
    {
        esp_tempW7 = 2000 ;
    }      
    else if ( esp_tempW7 <= 0 )
    {
        esp_tempW7 = 0 ;
    }           
    else
    {
        esp_tempW7 = esp_tempW7 ;
    }
        
    m_q_ay_old = m_q_ay ;   
      
/* '06.11.08 : Vel & Steer_Limit  */    

    esp_tempW9 = LCESP_s16IInter3Point( vrefk, VREF_K_30_KPH, 1800, 
        						               VREF_K_40_KPH, 1600,	    
        						               VREF_K_50_KPH, 1500  ) ;	
    
    if ( (Reverse_Steer_Flg==0) 
          && ( ROP_REQ_ACT==1 ) 
          && ( vrefk >= VREF_K_30_KPH ) 
          && ( (McrAbs(wstr))>=esp_tempW9 ) 
          && ( abs_wstr_dot <= 100) )      /* '06.10.24 HM */
     {                                  
        Flg_Make_Oversteer_By_Ay = 1 ;
        
        if ( esp_tempW7 >= m_q_ay )
        {
            m_q_ay = LCESP_s16Lpf1Int(esp_tempW7, m_q_ay_old,117);           
        }
        else
        {

            esp_tempW5 = LCESP_s16IInter3Point( vrefk, VREF_K_20_KPH, 117,	           
        						                       VREF_K_30_KPH, L_U8FILTER_GAIN_10MSLOOP_6_5HZ,	    
        						                       VREF_K_40_KPH, 1 ) ;	    						                       
        						                               
            m_q_ay = LCESP_s16Lpf1Int(esp_tempW7, m_q_ay_old,(uint8_t)esp_tempW5);          /* 12.25 for HM  */
        }                                                       
    }    
    else
    {
            Flg_Make_Oversteer_By_Ay = 0 ;        
            m_q_ay = LCESP_s16Lpf1Int(esp_tempW7, m_q_ay_old,117);           
    }
    
    if ( ROP_REQ_ACT==1 ) 
    {              
        
        esp_tempW5 = LCESP_s16IInter5Point( vrefk,  VREF_K_20_KPH,  S16_AYC_MQ_WEG_VEL_20K, 	
        						                    VREF_K_40_KPH,  S16_AYC_MQ_WEG_VEL_40K,
    					                            VREF_K_60_KPH,  S16_AYC_MQ_WEG_VEL_60K,
    					                            VREF_K_80_KPH,  S16_AYC_MQ_WEG_VEL_80K,					                            
    					                            VREF_K_100_KPH,  S16_AYC_MQ_WEG_VEL_100K ) ;	             						                       
        						                   
        /* tempW2 = m_q_ay ; tempW1 = esp_tempW5; tempW0 = 100 ;   
        s16muls16divs16();
        moment_q_front_rop = tempW3;  */   
        moment_q_front_rop = (int16_t)( (((int32_t)m_q_ay)*esp_tempW5)/100 );
	} 
	else 
	{		
        moment_q_front_rop = 0 ;
	}	  
                
    if( moment_q_front_rop < 0 ) 
    {
        moment_q_front_rop = 0 ;
    } 
    else 
    { 
        moment_q_front_rop = moment_q_front_rop ;
    }       
      
#if (__Pro_Active_Roll_Ctrl == 1)  
        
/* '10.3.30 : ROP Improvement */      

    if ( (ROP_DYN_STABLE_PHASE==0)&&(SLIP_CONTROL_NEED_FL==1)&&(lcu1ropPressLmt1stFL==0) ) 
    {                
        lcu1ropPressLmt1stFL = 1 ;  
    }
    else if ( (SLIP_CONTROL_NEED_FL==1)&&(lcu1ropPressLmt1stFL==1) ) 
    {                
        lcu1ropPressLmt1stFL = 1 ;  
    }
    else 
    {                
        lcu1ropPressLmt1stFL = 0 ;  
    }                
        
    if ( (ROP_DYN_STABLE_PHASE==1)&&(SLIP_CONTROL_NEED_FL==1)&&(lcu1ropPressLmt2ndFL==0)&&(lcu1ropPressLmt1stFL==0) ) 
    {                
        lcu1ropPressLmt2ndFL = 1 ;  
          lcu1ropFrtCtrlReqFR = 0 ;                
    }
    else if ( (SLIP_CONTROL_NEED_FL==1)&&(lcu1ropPressLmt2ndFL==1) ) 
    {                
        lcu1ropPressLmt2ndFL = 1 ;  

#if (__Pro_Active_ROP_Decel == 1)  
         
        if (((FL.u8_Estimated_Active_Press)>=S8_DR_2ND_F_ENT_PRESS_OF_OUT_F)&&(lcu1ropFrtCtrlReqFR==0) ) 
        {
          lcu1ropFrtCtrlReqFR = 0 ;  //1
        }
        else if (((FL.u8_Estimated_Active_Press)>=(S8_DR_2ND_F_ENT_PRESS_OF_OUT_F-5))&&(lcu1ropFrtCtrlReqFR==1) ) 
        {  
          lcu1ropFrtCtrlReqFR = 0 ;  //1
        } 
        else 
        {
          lcu1ropFrtCtrlReqFR = 0 ;
        }         
#else
          lcu1ropFrtCtrlReqFR = 0 ;
#endif
        
    }
    else 
    {                
        lcu1ropPressLmt2ndFL = 0 ;  
          lcu1ropFrtCtrlReqFR = 0 ;                
    }            
        
    if ( (ROP_DYN_STABLE_PHASE==0)&&(SLIP_CONTROL_NEED_FR==1)&&(lcu1ropPressLmt1stFR==0) ) 
    {                
        lcu1ropPressLmt1stFR = 1 ;  
    }
    else if ( (SLIP_CONTROL_NEED_FR==1)&&(lcu1ropPressLmt1stFR==1) ) 
    {                
        lcu1ropPressLmt1stFR = 1 ;  
    }
    else 
    {                
        lcu1ropPressLmt1stFR = 0 ;  
    }                
    if ( (ROP_DYN_STABLE_PHASE==1)&&(SLIP_CONTROL_NEED_FR==1)&&(lcu1ropPressLmt2ndFR==0)&&(lcu1ropPressLmt1stFR==0) ) 
    {                
        lcu1ropPressLmt2ndFR = 1 ;  
          lcu1ropFrtCtrlReqFL = 0 ;                
    }
    else if ( (SLIP_CONTROL_NEED_FR==1)&&(lcu1ropPressLmt2ndFR==1) ) 
    {                
        lcu1ropPressLmt2ndFR = 1 ;  
        
#if (__Pro_Active_ROP_Decel == 1)  
         
        if (((FR.u8_Estimated_Active_Press)>=S8_DR_2ND_F_ENT_PRESS_OF_OUT_F)&&(lcu1ropFrtCtrlReqFL==0) ) 
        {
          lcu1ropFrtCtrlReqFL = 0 ; //1
        }
        else if (((FR.u8_Estimated_Active_Press)>=(S8_DR_2ND_F_ENT_PRESS_OF_OUT_F-5))&&(lcu1ropFrtCtrlReqFL==1) ) 
        {  
          lcu1ropFrtCtrlReqFL = 0 ; //1
        } 
        else 
        {
          lcu1ropFrtCtrlReqFL = 0 ;
        }         
#else
          lcu1ropFrtCtrlReqFL = 0 ;
#endif
        
    }
    else 
    {                
        lcu1ropPressLmt2ndFR = 0 ; 
          lcu1ropFrtCtrlReqFL = 0 ;                
    }    
    
	if(ROP_REQ_FRT_Pre_FL==1)
	{
		rop_frt_pre_ctrl_fl = rop_frt_pre_ctrl_fl + 1 ;
	} 
	else
	{
		rop_frt_pre_ctrl_fl = 0 ;
	}    
 
	if(ROP_REQ_FRT_Pre_FR==1)
	{
		rop_frt_pre_ctrl_fr = rop_frt_pre_ctrl_fr + 1 ;
	} 
	else
	{
		rop_frt_pre_ctrl_fr = 0 ;
	} 

    if ( (lcu1ropPressLmt1stFL==1)&&(FL.u8_Estimated_Active_Press>=S16_ROP_FRT_PRESS_MIN)&&(RL_ESC.lcu1ropRearCtrlReq==0)&&(SLIP_CONTROL_NEED_FR==0) ) 
    {                
        RL_ESC.lcu1ropRearCtrlReq = 1 ; 

        if ((rop_frt_pre_ctrl_fr<=S16_ROP_PRE_FRT_CTRL_MIN)&&(RL.u8_Estimated_Active_Press<=S16_ROP_REAR_RISE_PRESS_BAR))
        {                
            RL_ESC.rop_rear_ctrl_mode = 1 ;  /* Rise_mode */
        } 
        else 
        {                
            RL_ESC.rop_rear_ctrl_mode = 2 ;  /* Hold_mode */ 
        }          
        
    } 
    else if ( (lcu1ropPressLmt1stFL==1)&&(RL_ESC.lcu1ropRearCtrlReq==1)&&(SLIP_CONTROL_NEED_FR==0) ) 
    {                
        RL_ESC.lcu1ropRearCtrlReq = 1 ; 
        
        if ((rop_frt_pre_ctrl_fr<=S16_ROP_PRE_FRT_CTRL_MIN)&&(RL.u8_Estimated_Active_Press<=S16_ROP_REAR_RISE_PRESS_BAR))
        {                
            RL_ESC.rop_rear_ctrl_mode = 1 ;  /* Rise_mode */
        } 
        else 
        {                
            RL_ESC.rop_rear_ctrl_mode = 2 ;  /* Hold_mode */ 
        }           
    }    
    else 
    {                
      #if (__Pro_Active_ROP_Decel == 1)        
        if (lcu1ropPressLmt2ndFL==1)
        {                 
            
//            if ((lcu1ropPressLmt2ndFL==1)&&(FL.u8_Estimated_Active_Press>=S8_DR_2ND_R_ENT_PRESS_OF_OUT_F)&&(RL.u8_Estimated_Active_Press<=S8_DR_2ND_R_LMT_PRESS))
            if ((RL_ESC.lcu1ropRearCtrlReq==0)&&(lcu1ropPressLmt2ndFL==1)&&(FL.u8_Estimated_Active_Press>=S8_DR_2ND_R_ENT_PRESS_OF_OUT_F)&&(SLIP_CONTROL_NEED_FR==0))
            {                
                RL_ESC.rop_rear_ctrl_mode = 1 ;  /* Rise_mode */ 
                RL_ESC.lcu1ropRearCtrlReq = 1 ; 
            } 
//            else if((lcu1ropPressLmt2ndFL==1)&&(FL.u8_Estimated_Active_Press>=S8_DR_2ND_R_EXT_PRESS_OF_OUT_F)&&(RL.u8_Estimated_Active_Press>S8_DR_2ND_R_LMT_PRESS))
            else if((RL_ESC.lcu1ropRearCtrlReq==1)&&(lcu1ropPressLmt2ndFL==1)&&(FL.u8_Estimated_Active_Press>=S8_DR_2ND_R_EXT_PRESS_OF_OUT_F)&&(SLIP_CONTROL_NEED_FR==0))
            {                
                RL_ESC.rop_rear_ctrl_mode = 2 ;  /* Hold_mode */
                RL_ESC.lcu1ropRearCtrlReq = 1 ; 
            }        
            else 
            {                
                RL_ESC.rop_rear_ctrl_mode = 3 ;  /* Dump_mode */  
                RL_ESC.lcu1ropRearCtrlReq = 0 ; 
            }           
        }        
        else
        {
          RL_ESC.lcu1ropRearCtrlReq  = 0 ; 
          RL_ESC.rop_rear_ctrl_mode = 3 ;  /* Dump_mode */                 
        }
        
      #else
      
        RL_ESC.lcu1ropRearCtrlReq  = 0 ; 
        RL_ESC.rop_rear_ctrl_mode = 3 ;  /* Dump_mode */ 
        
      #endif         

    }
    
    if ( (lcu1ropPressLmt1stFR==1)&&(FR.u8_Estimated_Active_Press>=S16_ROP_FRT_PRESS_MIN)&&(RR_ESC.lcu1ropRearCtrlReq==0)&&(SLIP_CONTROL_NEED_FL==0) ) 
    {                
        RR_ESC.lcu1ropRearCtrlReq = 1 ; 

        if ((rop_frt_pre_ctrl_fl<=S16_ROP_PRE_FRT_CTRL_MIN)&&(RR.u8_Estimated_Active_Press<=S16_ROP_REAR_RISE_PRESS_BAR))
        {                
            RR_ESC.rop_rear_ctrl_mode = 1 ;  /* Rise_mode */
        } 
        else 
        {                
            RR_ESC.rop_rear_ctrl_mode = 2 ;  /* Hold_mode */ 
        }    
                
    } 
    else if ( (lcu1ropPressLmt1stFR==1)&&(RR_ESC.lcu1ropRearCtrlReq==1)&&(SLIP_CONTROL_NEED_FL==0) ) 
    {                
        RR_ESC.lcu1ropRearCtrlReq = 1 ; 
        
        if ((rop_frt_pre_ctrl_fl<=S16_ROP_PRE_FRT_CTRL_MIN)&&(RR.u8_Estimated_Active_Press<=S16_ROP_REAR_RISE_PRESS_BAR))
        {                
            RR_ESC.rop_rear_ctrl_mode = 1 ;  /* Rise_mode */
        } 
        else 
        {                
            RR_ESC.rop_rear_ctrl_mode = 2 ;  /* Hold_mode */ 
        }    
             
    }    
    else 
    {                
      #if (__Pro_Active_ROP_Decel == 1)  
     
        if (lcu1ropPressLmt2ndFR==1)
        {                 
            
//            if ((lcu1ropPressLmt2ndFR==1)&&(FR.u8_Estimated_Active_Press>=S8_DR_2ND_R_ENT_PRESS_OF_OUT_F)&&(RR.u8_Estimated_Active_Press<=S8_DR_2ND_R_LMT_PRESS))
            if ((RR_ESC.lcu1ropRearCtrlReq==0)&&(lcu1ropPressLmt2ndFR==1)&&(FR.u8_Estimated_Active_Press>=S8_DR_2ND_R_ENT_PRESS_OF_OUT_F)&&(SLIP_CONTROL_NEED_FL==0))
            {                
                RR_ESC.rop_rear_ctrl_mode = 1 ;  /* Rise_mode */ 
                RR_ESC.lcu1ropRearCtrlReq = 1 ; 
            } 
//            else if((lcu1ropPressLmt2ndFR==1)&&(FR.u8_Estimated_Active_Press>=S8_DR_2ND_R_EXT_PRESS_OF_OUT_F)&&(RR.u8_Estimated_Active_Press>S8_DR_2ND_R_LMT_PRESS))
            else if((RR_ESC.lcu1ropRearCtrlReq==1)&&(lcu1ropPressLmt2ndFR==1)&&(FR.u8_Estimated_Active_Press>=S8_DR_2ND_R_EXT_PRESS_OF_OUT_F)&&(SLIP_CONTROL_NEED_FL==0))
            {                
                RR_ESC.rop_rear_ctrl_mode = 2 ;  /* Hold_mode */
                RR_ESC.lcu1ropRearCtrlReq = 1 ; 
            }        
            else 
            {                
                RR_ESC.rop_rear_ctrl_mode = 3 ;  /* Dump_mode */  
                RR_ESC.lcu1ropRearCtrlReq = 0 ; 
            }           
        }        
        else
        {
          RR_ESC.lcu1ropRearCtrlReq  = 0 ; 
          RR_ESC.rop_rear_ctrl_mode = 3 ;  /* Dump_mode */                 
        } 

      #else
      
        RR_ESC.lcu1ropRearCtrlReq = 0 ; 
        RR_ESC.rop_rear_ctrl_mode = 3 ;  /* Dump_mode */ 
        
      #endif            

    }
      
    if ( (lcu1ropPressLmt1stFL==1)&&(esp_pressure_count_fl>=S16_ROP_PRESS_RISE_1ST_LMT) ) 
    {                
        lcu1ropFrtPressHold_FL = 1 ; 
    } 
    else if ( (lcu1ropPressLmt2ndFL==1)&&(esp_pressure_count_fl>=S16_ROP_PRESS_RISE_2ND_LMT) ) 
    {                
        lcu1ropFrtPressHold_FL = 1 ; 
    }    
    else 
    {                
        lcu1ropFrtPressHold_FL = 0 ; 
    }        
             
    if ( (lcu1ropPressLmt1stFR==1)&&(esp_pressure_count_fr>=S16_ROP_PRESS_RISE_1ST_LMT) ) 
    {                
        lcu1ropFrtPressHold_FR = 1 ; 
    } 
    else if ( (lcu1ropPressLmt2ndFR==1)&&(esp_pressure_count_fr>=S16_ROP_PRESS_RISE_2ND_LMT) ) 
    {                
        lcu1ropFrtPressHold_FR = 1 ; 
    }    
    else 
    {                
        lcu1ropFrtPressHold_FR = 0 ; 
    }         

#else // #if (__Pro_Active_Roll_Ctrl == 1)  


    lcu1ropPressLmt1stFL = 0 ;  
		lcu1ropPressLmt2ndFL = 0 ;  
		lcu1ropPressLmt1stFR = 0 ;  
		lcu1ropPressLmt2ndFR = 0 ; 
		RL_ESC.lcu1ropRearCtrlReq = 0 ; 
		RR_ESC.lcu1ropRearCtrlReq = 0 ;
		lcu1ropFrtPressHold_FL = 0 ;
		lcu1ropFrtPressHold_FR = 0 ;

#endif			
 
}

void LCROP_vInterfaceMoment(void)
{		             
    
 esp_tempW7 = LCESP_s16IInter3Point( alat_dot_rop, 0, 0, 1000, -5, 2000, -10 ) ; 
 esp_tempW8 = LCESP_s16IInter3Point( McrAbs(alat), 800, slip_target_limit_front, 
                                       900, slip_target_limit_front - 10, 
                                       1000, slip_target_limit_front - 20 ) ;  
                                       
 esp_tempW9 = esp_tempW7 + esp_tempW8 ;                                      
                                       
    if ( esp_tempW9 < -ST_LIMIT_F_OVERSTEER_OUT_ROP ) {          
        esp_tempW9 = -ST_LIMIT_F_OVERSTEER_OUT_ROP ;  
	  } else {		
       ; 
	  }	   

    if ( ROP_REQ_ACT==1 ) {          
        slip_target_limit_front = esp_tempW9 ;
		moment_q_front = moment_q_front - moment_q_front_rop ; 				
	} else {				
		slip_target_limit_front = slip_target_limit_front ; 
		moment_q_front = moment_q_front ; 		  	
	}	        
		
}

void LCROP_vControlEngine(void)
{
   	if( ROP_REQ_ACT==1 ) {

		if(mtp>2) {
            rop_torq = rop_torq - ROP_TORQ_DOWN_RATE ;			
		} else {
			ROP_REQ_ENG = 0 ;	
			rop_torq = cal_torq ;			
		}		

		if(rop_torq < fric_torq) 
		{
		    rop_torq = fric_torq ;
		}
		else
		{
		    ;
		}
		
		if(rop_torq > drive_torq) 
		{ 
		   rop_torq = drive_torq ;
		}
		else
		{
		    ;
		}
		
   	} else {
   	    
		if( ROP_REQ_ENG==1 ) {		    
		    
		    if(mtp>2) {		    
			
    			if(rop_torq >= (drive_torq-TORQ_PERCENT_5PRO)) {
    				ROP_REQ_ENG = 0 ;
    			    rop_torq = cal_torq ;				
    			} else {			    
    			    rop_torq = rop_torq + ROP_TORQ_RISE_RATE ;	
                }
            			    
    		} else {
    			ROP_REQ_ENG = 0 ;	
    			rop_torq = cal_torq ;			
    		}	  	    
            			    
		} else {		
		    ROP_REQ_ENG = 0 ;		    
			rop_torq = cal_torq ;
		}	
		
		if(rop_torq < fric_torq)  
		{
		    rop_torq = fric_torq ;
		}
		else
		{
		    ;
		}
		
		if(rop_torq > drive_torq) 
		{
		    rop_torq = drive_torq ;
		}
		else
		{
		    ;
		}
		
	}			
}
#endif
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LCROPCallControl
	#include "Mdyn_autosar.h"
#endif
