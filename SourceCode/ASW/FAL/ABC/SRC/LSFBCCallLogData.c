/*******************************************************************************
* Project Name:		MGH40_ESP
* File Name:		LSFBCCallLogData.c
* Description:		Logging Data of FBC
* Logic version:	HV26
********************************************************************************
*  Modification Log
*  Date			Author			Description
*  -----		-----			-----
*  5C15			eslim			Initial Release
*  6113	        eslim           Cycle time adding, decel adding
********************************************************************************/


#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LSFBCCallLogData
	#include "Mdyn_autosar.h"
#endif
/* Includes ********************************************************************/
#include "LSFBCCallLogData.h"

#if __FBC  
//#if __LOGGER && (LOGGER_DATA_FORMAT==1)
#if defined(__LOGGER)
    #if __LOGGER_DATA_TYPE==1


#include "LDFBCCallDetection.h"
#include "LCFBCCallControl.h"

#include "LDBDWCallDetection.h"
#include "LCBDWCallControl.h"
#include "LABDWCallActHW.h"

#include "LABACallActHW.h"

#include "LCHRBCallControl.h"
#include "LDABSCallEstVehDecel.H"
#include "LDESPEstDiscTemp.h"
#include "LCPBACallControl.h"
 
/* Logcal Definiton ************************************************************/

/* Variables Definition ********************************************************/

//extern int8_t lu8EbpCompMtp;
/* Local Function prototype ****************************************************/
void LSFBC_vCallFBCLogData(void);

/* Implementation **************************************************************/

/*******************************************************************************
* FUNCTION NAME:		LSFBC_vCallFBCLogData
* CALLED BY:			LS_vCallLogData()
* Preconditions:		none
* PARAMETER:			none
* RETURN VALUE:			none
* Description:			Logging Data of FBC
********************************************************************************/
void LSFBC_vCallFBCLogData(void)
{
    UCHAR i;
    
    CAN_LOG_DATA[0] = (UCHAR)system_loop_counter;   
    CAN_LOG_DATA[1] = (UCHAR)(system_loop_counter>>8);           // 1
    CAN_LOG_DATA[2] = (UCHAR)yaw_out;            
    CAN_LOG_DATA[3] = (UCHAR)(yaw_out>>8);                       // 2
    CAN_LOG_DATA[4] = (UCHAR)alat;            
    CAN_LOG_DATA[5] = (UCHAR)(alat>>8);                          // 3
    CAN_LOG_DATA[6] = (UCHAR)wstr;          
    CAN_LOG_DATA[7] = (UCHAR)(wstr>>8);                          // 4

    CAN_LOG_DATA[8]  = (UCHAR)vrad_fl;     
    CAN_LOG_DATA[9]  = (UCHAR)(vrad_fl>>8);                      // 5
    CAN_LOG_DATA[10] = (UCHAR)vrad_fr;     
    CAN_LOG_DATA[11] = (UCHAR)(vrad_fr>>8);                      // 6
    CAN_LOG_DATA[12] = (UCHAR)vrad_rl;     
    CAN_LOG_DATA[13] = (UCHAR)(vrad_rl>>8);                      // 7
    CAN_LOG_DATA[14] = (UCHAR)vrad_rr;      
    CAN_LOG_DATA[15] = (UCHAR)(vrad_rr>>8);                      // 8

    CAN_LOG_DATA[16] = (UCHAR)vref;
    CAN_LOG_DATA[17] = (UCHAR)(vref>>8);                         // 9
    CAN_LOG_DATA[18] = (UCHAR)vref5;
    CAN_LOG_DATA[19] = (UCHAR)(vref5>>8);                        //10
    CAN_LOG_DATA[20] = (UCHAR)FL.pwm_duty_temp;                  //11
    CAN_LOG_DATA[21] = (UCHAR)FR.pwm_duty_temp;                  //12 
    CAN_LOG_DATA[22] = (UCHAR)RL.pwm_duty_temp;                  //13
    CAN_LOG_DATA[23] = (UCHAR)RR.pwm_duty_temp;                  //14

  #if __BDW  
    CAN_LOG_DATA[24] = (UCHAR)(bdw_msc_target_vol/100);          //15            
  #else
    CAN_LOG_DATA[24] = (UCHAR)(0);                               //15    
  #endif                                               
    CAN_LOG_DATA[25] = (UCHAR)((afz>127)?127:((afz<-127)?-127:(CHAR)afz));              //16            
    CAN_LOG_DATA[26] = (UCHAR)(eng_torq/10);                     //17
    CAN_LOG_DATA[27] = (UCHAR)mtp;                               //18          
    CAN_LOG_DATA[28] = (UCHAR)FL.flags;                          //19
    CAN_LOG_DATA[29] = (UCHAR)FR.flags;                          //20
    CAN_LOG_DATA[30] = (UCHAR)RL.flags;                          //21
    CAN_LOG_DATA[31] = (UCHAR)RR.flags;                          //22

    CAN_LOG_DATA[32] = (UCHAR)(target_vol/100);                  //23
    CAN_LOG_DATA[33] = (UCHAR)FBC_dct_cnt;                       //24
    CAN_LOG_DATA[34] = (UCHAR)FBC_mode;                          //25
    CAN_LOG_DATA[35] = (UCHAR)PBA_mode;                          //26
    CAN_LOG_DATA[36] = (UCHAR)(fu16CalVoltMOTOR/100);            //27
  #if __ESC_MOTOR_PWM_CONTROL  
    CAN_LOG_DATA[37] = (UCHAR)lau8MscDuty;                       //28
  #else
    CAN_LOG_DATA[37] = (UCHAR)(0);
  #endif  
    CAN_LOG_DATA[38] = (UCHAR)(pba_msc_target_vol/100);          //29                
    CAN_LOG_DATA[39] = (UCHAR)(fbc_msc_target_vol/100);          //30     

    CAN_LOG_DATA[40] = (UCHAR)PBA_hold_timer;                              
    CAN_LOG_DATA[41] = (UCHAR)(PBA_hold_timer>>8);               //31  
    CAN_LOG_DATA[42] = (UCHAR)mpress;               
    CAN_LOG_DATA[43] = (UCHAR)(mpress>>8);                       //32    
    CAN_LOG_DATA[44] = (UCHAR)ebd_refilt_grv;               
    CAN_LOG_DATA[45] = (UCHAR)(ebd_refilt_grv>>8);               //33     
    CAN_LOG_DATA[46] = (UCHAR)along;    
    CAN_LOG_DATA[47] = (UCHAR)(along>>8);                        //34       
    
    CAN_LOG_DATA[48] = (UCHAR)(((sign_o_delta_yaw_thres/10)>127)?127:(((sign_o_delta_yaw_thres/10)<-127)?-127:(CHAR)(sign_o_delta_yaw_thres/10))); //35
    CAN_LOG_DATA[49] = (UCHAR)(((sign_u_delta_yaw_thres/10)>127)?127:(((sign_u_delta_yaw_thres/10)<-127)?-127:(CHAR)(sign_u_delta_yaw_thres/10))); //36
    CAN_LOG_DATA[50] = (UCHAR)(((nosign_delta_yaw_first/10)>127)?127:(((nosign_delta_yaw_first/10)<-127)?-127:(CHAR)(nosign_delta_yaw_first/10))); //37 
    CAN_LOG_DATA[51] = (UCHAR)(((nosign_delta_yaw/10)>127)?127:(((nosign_delta_yaw/10)<-127)?-127:(CHAR)(nosign_delta_yaw/10)));                   //38
    CAN_LOG_DATA[52] = (UCHAR)FL.u8_Estimated_Active_Press;      //39
    CAN_LOG_DATA[53] = (UCHAR)FR.u8_Estimated_Active_Press;      //40    
    CAN_LOG_DATA[54] = (UCHAR)RL.u8_Estimated_Active_Press;      //41
    CAN_LOG_DATA[55] = (UCHAR)RR.u8_Estimated_Active_Press;      //42
    
    CAN_LOG_DATA[56] = (UCHAR)(((lsabss16DecelRefiltByVref5/10)>127)?127:(((lsabss16DecelRefiltByVref5/10)<-127)?-127:(CHAR)(lsabss16DecelRefiltByVref5/10)));   //43 
  #if  __HOB 
    CAN_LOG_DATA[57] = (UCHAR)(lcs16HobMscTargetV/100);                                 //44 
  #else
    CAN_LOG_DATA[57] = (UCHAR)(0);
  #endif    
    CAN_LOG_DATA[58] = (UCHAR)0;      //45 
    CAN_LOG_DATA[59] = (UCHAR)0;      //46  
    CAN_LOG_DATA[60] = (UCHAR)FBC_decel_sum;                   
    CAN_LOG_DATA[61] = (UCHAR)(FBC_decel_sum>>8);               //47   
    CAN_LOG_DATA[62] = (UCHAR)mpress_slop_filt;
    CAN_LOG_DATA[63] = (UCHAR)(mpress_slop_filt>>8);              //48
    
    CAN_LOG_DATA[64] = (UCHAR)btc_tmp_fl;       
    CAN_LOG_DATA[65] = (UCHAR)(btc_tmp_fl>>8);                       //49
    CAN_LOG_DATA[66] = (UCHAR)btc_tmp_fr;                       
    CAN_LOG_DATA[67] = (UCHAR)(btc_tmp_fr>>8);                       //50
    CAN_LOG_DATA[68] = (UCHAR)btc_tmp_rl;  
    CAN_LOG_DATA[69] = (UCHAR)(btc_tmp_rl>>8);                //51
    CAN_LOG_DATA[70] = (UCHAR)btc_tmp_rr;  
    CAN_LOG_DATA[71] = (UCHAR)(btc_tmp_rr>>8);                //52
    
    i=0;                                  
    if (BLS                             ==1) i|=0x01; 
    if (ABS_fz                          ==1) i|=0x02; 
    if (AFZ_OK                          ==1) i|=0x04; 
    if (EBD_RA                          ==1) i|=0x08; 
    if (BTC_fz                          ==1) i|=0x10; 
    if (YAW_CDC_WORK                    ==1) i|=0x20; 
    if (ETCS_ON                         ==1) i|=0x40; 
    if (ESP_TCS_ON                      ==1) i|=0x80; 

    CAN_LOG_DATA[72] = i;               //53
    
    i=0;                                  
    if (MOT_ON_FLG                      ==1) i|=0x01;
    if (FBC_WORK                        ==1) i|=0x02;
    if (FBC_ON                          ==1) i|=0x04; 
    if (FBC_ACTIVE                      ==1) i|=0x08; 
    if ((PBA_ON==1) || (PBA_pulsedown  ==1)) i|=0x10; 
    if (PBA_BIG_SLOP                    ==1) i|=0x20; 
    if (PBA_WORK                        ==1) i|=0x40; 
    if (PBA_ACT                         ==1) i|=0x80;               

    CAN_LOG_DATA[73] = i;               //54    

    i = 0;                                   
    if (ABS_fl                          ==1) i|=0x01;
  #if  __HOB  
    if (lcu1HobOnFlag                   ==1) i|=0x02; 
  #else
    if (0                               ==1) i|=0x02;
  #endif  
    if (STABIL_fl                       ==1) i|=0x04;
    if (SPOLD_RESET_fl                  ==1) i|=0x08;
    if (ESP_BRAKE_CONTROL_FL            ==1) i|=0x10; 
    if (GMA_SLIP_fl                     ==1) i|=0x20;
    if (HV_VL_fl                        ==1) i|=0x40;
    if (AV_VL_fl                        ==1) i|=0x80;

    CAN_LOG_DATA[74] = i;               //55
    
    i=0;                                          
    if (ABS_fr                          ==1) i|=0x01; 
  #if  __HOB  
    if (lcu1HobMpInitBigSlopOK          ==1) i|=0x02;
  #else
    if (0                               ==1) i|=0x02;
  #endif   	 
    if (STABIL_fr                       ==1) i|=0x04; 
    if (SPOLD_RESET_fr                  ==1) i|=0x08; 
    if (ESP_BRAKE_CONTROL_FR            ==1) i|=0x10; 
    if (GMA_SLIP_fr                     ==1) i|=0x20; 
    if (HV_VL_fr                        ==1) i|=0x40; 
    if (AV_VL_fr                        ==1) i|=0x80; 

    CAN_LOG_DATA[75] = i;               //56
    
    i=0;                                   
    if (ABS_rl                          ==1) i|=0x01;             
    if (0                               ==1) i|=0x02;             
    if (STABIL_rl                       ==1) i|=0x04;             
    if (SPOLD_RESET_rl                  ==1) i|=0x08;           
    if (ESP_BRAKE_CONTROL_RL            ==1) i|=0x10; 
    if (RL.MSL_BASE                     ==1) i|=0x20;
    if (HV_VL_rl                        ==1) i|=0x40;
    if (AV_VL_rl                        ==1) i|=0x80;             

    CAN_LOG_DATA[76] = i;               //57

    i = 0;
    if(ABS_rr                           ==1) i|=0x01;
    if(0                    ==1) i|=0x02;
    if(STABIL_rr                        ==1) i|=0x04;
    if(SPOLD_RESET_rr                   ==1) i|=0x08;
    if(ESP_BRAKE_CONTROL_RR             ==1) i|=0x10;           
    if(RR.MSL_BASE                      ==1) i|=0x20;         
    if(HV_VL_rr                         ==1) i|=0x40;       
    if(AV_VL_rr                         ==1) i|=0x80;          

    CAN_LOG_DATA[77] = i;               // 58

    i=0;
    if(TCL_DEMAND_fl                    ==1) i|=0x01;   
    if(TCL_DEMAND_fr                    ==1) i|=0x02;   
    if(S_VALVE_LEFT                     ==1) i|=0x04;
    if(S_VALVE_RIGHT                    ==1) i|=0x08;   
  #if __BDW  
    if(BDW_ON                           ==1) i|=0x10;   
  #else
    if(0                                ==1) i|=0x10;
  #endif  
    if(BDW_ACT_CONDTION                 ==1) i|=0x20;   
    if(DETECT_RAIN                      ==1) i|=0x40;   
    if(DISC_CLEAN_NEED                  ==1) i|=0x80;   
      
    CAN_LOG_DATA[78] = i;               // 59
    
     i=0;
    if(BDW_INIHIBIT                     ==1) i|=0x01;   
    if(lsbdwu1WiperIntSW                ==1) i|=0x02;   
    if(lsbdwu1WiperLow                  ==1) i|=0x04;   
    if(lsbdwu1WiperHigh                 ==1) i|=0x08;  
    if(lsbdwu1Clu2WiperMsgRxOkFlg       ==1) i|=0x10;   
    if(0                                ==1) i|=0x20;   
    if(0                                ==1) i|=0x40;  
  #if __HRB  
    if(lchrbu1ActiveFlag                ==1) i|=0x80;
  #else
    if(0                                ==1) i|=0x80;
  #endif  	 

    CAN_LOG_DATA[79] = i;               // 60
}

#endif
#endif
#endif
	
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LSFBCCallLogData
	#include "Mdyn_autosar.h"
#endif
