

/* AUTOSAR --------------------------*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LDTCSCallDetection
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/


#include "LCESPCalInterpolation.h"
#include "LCESPCalLpf.h"
#include "LCallMain.h"
#include "LCTCSCallControl.h"
#include "LDTCSCallDetection.h"
#include "LDTCSCallCalculation.h"
#include "LDBTCSCallDetection.h"
#include "LCRDCMCallControl.h"
#include "LCHSACallControl.h"
#include "hm_logic_var.h"
#include "LCTCSCallBrakeControl.h"
#include "tcs_var.h"
#include "LDRTACallDetection.h"
#include "LDESPEstDiscTemp.h"
#include "LAABSCallMotorSpeedControl.h"
#include "Hardware_Control.h"

#if __BTC   /* TAG1 */
  #if	defined(BTCS_TCMF_CONTROL) /* TAG2 */
/* GlobalFunction prototype **************************************************/
	void LDTCS_vCallCalculation(void);
    
/* LocalFunction prototype ***************************************************/
	static void LDTCS_vCalCdtESCBrkCtlFlagCopy(void);
	static void LDTCS_vCalCdtBTCReduceFlgInESC(struct TCS_WL_STRUCT *Wl_ESC, struct TCS_WL_STRUCT *Wl_TCS);
	
	static void LDTCS_vCalBaseBrkCtlStTh(void); 
  	static void LDTCS_vCalBaseBrkCtlTurnTh(void);
  	static void LDTCS_vCoordinateBaseCtlTh(void);   
    static void LDTCS_vCalBrkCtlTh4Asym(struct TCS_WL_STRUCT *Wl_TCS, struct W_STRUCT *W_L, struct RTA_STRUCT *RTA_W_L); 
    static void LDTCS_vCalBrkCtlTh4Sym(void); 
    static void LDTCS_vCalVehSpd4TCS(void);
    static void LCTCS_vCalWhlSpdMovingAvg(struct TCS_WL_STRUCT *Wl_TCS, struct W_STRUCT *W_L); /*기존 계산부 Detection으로 이동해 1scan delay 없앰*/
    static void LCTCS_vCalWhlSpdMovingAvgDiff(struct TCS_WL_STRUCT *Wl_TCS);
    static void LCTCS_vCalWhlSpdMovingAvgCopy(struct TCS_WL_STRUCT *Wl_TCS, struct W_STRUCT *W_L);
  	static void LCTCS_vCalWhlSpdMovingAvgFlt(struct TCS_WL_STRUCT *Wl_TCS, struct W_STRUCT *W_L); 
  	static void LCTCS_vCalWhlSpdAvg4Axle(struct TCS_AXLE_STRUCT *Axle_TCS, struct W_STRUCT *WL_LEFT, struct W_STRUCT *WL_RIGHT);	
  	static void LCTCS_vCalWhlSpinAvg4Axle(struct TCS_AXLE_STRUCT *Axle_TCS);
  	static void LCTCS_vCalWhlAccAvg4Axle(struct TCS_AXLE_STRUCT *Axle_TCS, struct W_STRUCT *WL_LEFT, struct W_STRUCT *WL_RIGHT);
	static void LDTCS_vCalWhlSpinF(struct TCS_WL_STRUCT *Wl_TCS, struct W_STRUCT *W_L);
	static void LDTCS_vCalWhlSpinNF(struct TCS_WL_STRUCT *Wl_TCS, struct W_STRUCT *W_L);
	static void LDTCS_vCalWhlSpinDiffOfLAndR(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT);
	static void LDTCS_vCalWhlSpinAvgOfLAndR(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT);
	static void LDTCS_vCalWhlSpinDiffOfLAndRNF(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT);
	static void LDTCS_vCalChngRateOfWhlSpinDiff(struct TCS_AXLE_STRUCT *Axle_TCS);
	static void LDTCS_vCalMassOfOneCorner(void);
	static void LDTCS_vCalWhlRolRadius(void);
	static void LCTCS_vCalWhlMovAvgDiffLAndR(struct TCS_AXLE_STRUCT *Axle_TCS, struct W_STRUCT *WL_LEFT , struct W_STRUCT *WL_RIGHT); 
	static void LDTCS_vSlopeOfSpinDiff (struct TCS_AXLE_STRUCT *Axle_TCS);
	static void LDTCS_vCalIndexOfVehTurn(void);
	static void LDTCS_vCalBrkWrkUntHghMuWhlUnst(struct TCS_AXLE_STRUCT *Axle_TCS);
	static void LDTCS_vCalDistanceOfStopNgo(void);
	
	/*vibration control*/                                                     
  static void LDTCS_vVIBCalWhlSpdCounter(struct TCS_WL_STRUCT *Wl_TCS, struct W_STRUCT *W_L);
  static void LDTCS_vVIBDetectWhlSpdRiseFall(struct TCS_WL_STRUCT *Wl_TCS);
  static void LDTCS_vVIBCalFreqAmp(struct TCS_WL_STRUCT *Wl_TCS);
  /*vibration control*/
	
/* Implementation  **********************************************************/
void LDTCS_vCallCalculation(void) /*TAG3*/
{
    LDTCS_vCalVehSpd4TCS(); 
    
    LCTCS_vCalWhlSpdMovingAvg(&FL_TCS,&FL);
	LCTCS_vCalWhlSpdMovingAvg(&FR_TCS,&FR);
	LCTCS_vCalWhlSpdMovingAvg(&RL_TCS,&RL);
	LCTCS_vCalWhlSpdMovingAvg(&RR_TCS,&RR);
	
    LCTCS_vCalWhlSpdMovingAvgDiff(&FL_TCS);
    LCTCS_vCalWhlSpdMovingAvgDiff(&FR_TCS);
    LCTCS_vCalWhlSpdMovingAvgDiff(&RL_TCS);
    LCTCS_vCalWhlSpdMovingAvgDiff(&RR_TCS);
	
	LCTCS_vCalWhlSpdMovingAvgCopy(&FL_TCS,&FL);
	LCTCS_vCalWhlSpdMovingAvgCopy(&FR_TCS,&FR);
	LCTCS_vCalWhlSpdMovingAvgCopy(&RL_TCS,&RL);
	LCTCS_vCalWhlSpdMovingAvgCopy(&RR_TCS,&RR);
            
	LCTCS_vCalWhlSpdMovingAvgFlt(&FL_TCS,&FL);
	LCTCS_vCalWhlSpdMovingAvgFlt(&FR_TCS,&FR);
	LCTCS_vCalWhlSpdMovingAvgFlt(&RL_TCS,&RL);
	LCTCS_vCalWhlSpdMovingAvgFlt(&RR_TCS,&RR);
	  
	LCTCS_vCalWhlSpdAvg4Axle(&FA_TCS,&FL,&FR);
	LCTCS_vCalWhlSpdAvg4Axle(&RA_TCS,&RL,&RR);	
	  
	LCTCS_vCalWhlSpinAvg4Axle(&FA_TCS);
	LCTCS_vCalWhlSpinAvg4Axle(&RA_TCS);
	  
	LCTCS_vCalWhlAccAvg4Axle(&FA_TCS,&FL,&FR);
	LCTCS_vCalWhlAccAvg4Axle(&RA_TCS,&RL,&RR);
	  
    LDTCS_vCalWhlSpinF(&FL_TCS,&FL); 
    LDTCS_vCalWhlSpinF(&FR_TCS,&FR); 
    LDTCS_vCalWhlSpinF(&RL_TCS,&RL); 
    LDTCS_vCalWhlSpinF(&RR_TCS,&RR);      
      
    LDTCS_vCalWhlSpinNF(&FL_TCS,&FL);
	LDTCS_vCalWhlSpinNF(&FR_TCS,&FR);
	LDTCS_vCalWhlSpinNF(&RL_TCS,&RL);
	LDTCS_vCalWhlSpinNF(&RR_TCS,&RR);
	    
    LDTCS_vCalWhlSpinDiffOfLAndR(&FA_TCS,&FL_TCS,&FR_TCS); 
    LDTCS_vCalWhlSpinDiffOfLAndR(&RA_TCS,&RL_TCS,&RR_TCS);
    LDTCS_vCalWhlSpinAvgOfLAndR(&FA_TCS,&FL_TCS,&FR_TCS); 
    LDTCS_vCalWhlSpinAvgOfLAndR(&RA_TCS,&RL_TCS,&RR_TCS);
    LDTCS_vCalWhlSpinDiffOfLAndRNF(&FA_TCS,&FL_TCS,&FR_TCS); 
    LDTCS_vCalWhlSpinDiffOfLAndRNF(&RA_TCS,&RL_TCS,&RR_TCS); 
    
    LDTCS_vCalChngRateOfWhlSpinDiff(&FA_TCS);
    LDTCS_vCalChngRateOfWhlSpinDiff(&RA_TCS);
    
    LDTCS_vSlopeOfSpinDiff (&FA_TCS);
    LDTCS_vSlopeOfSpinDiff (&RA_TCS);
    
    LDTCS_vCalMassOfOneCorner();
    LDTCS_vCalWhlRolRadius();
        
    LCTCS_vCalWhlMovAvgDiffLAndR(&FA_TCS,&FL,&FR); 
	LCTCS_vCalWhlMovAvgDiffLAndR(&RA_TCS,&RL,&RR);
	
	LDTCS_vCalIndexOfVehTurn();  
	  
	LDTCS_vCalBrkWrkUntHghMuWhlUnst(&FA_TCS);
	LDTCS_vCalBrkWrkUntHghMuWhlUnst(&RA_TCS);
	 
	LDTCS_vCalDistanceOfStopNgo();

	  /*Vibration control*/
	LDTCS_vVIBCalWhlSpdCounter(&FL_TCS,&FL);
	LDTCS_vVIBCalWhlSpdCounter(&FR_TCS,&FR);
	LDTCS_vVIBCalWhlSpdCounter(&RL_TCS,&RL);
	LDTCS_vVIBCalWhlSpdCounter(&RR_TCS,&RR);
    LDTCS_vVIBDetectWhlSpdRiseFall(&FL_TCS);
    LDTCS_vVIBDetectWhlSpdRiseFall(&FR_TCS);
    LDTCS_vVIBDetectWhlSpdRiseFall(&RL_TCS);
    LDTCS_vVIBDetectWhlSpdRiseFall(&RR_TCS);
    LDTCS_vVIBCalFreqAmp(&FL_TCS);
    LDTCS_vVIBCalFreqAmp(&FR_TCS);
    LDTCS_vVIBCalFreqAmp(&RL_TCS);
    LDTCS_vVIBCalFreqAmp(&RR_TCS);
    /*Vibration control*/
	  
	LDTCS_vCalCdtESCBrkCtlFlagCopy();

	#if (__SPLIT_TYPE==0)     /*X-Split 일 경우*/ 
	/*Same Axle Wheel Brake Control in ESC*/
	LDTCS_vCalCdtBTCReduceFlgInESC(&FL_TCS,&FR_TCS);
	LDTCS_vCalCdtBTCReduceFlgInESC(&FR_TCS,&FL_TCS);
	LDTCS_vCalCdtBTCReduceFlgInESC(&RL_TCS,&RR_TCS);
	LDTCS_vCalCdtBTCReduceFlgInESC(&RR_TCS,&RL_TCS);
	#else					  /*FR-Split 일 경우 */	
	/*Cross Wheel Brake Control in ESC*/
	LDTCS_vCalCdtBTCReduceFlgInESC(&FL_TCS,&RR_TCS);
	LDTCS_vCalCdtBTCReduceFlgInESC(&FR_TCS,&RL_TCS);
	LDTCS_vCalCdtBTCReduceFlgInESC(&RL_TCS,&FR_TCS);
	LDTCS_vCalCdtBTCReduceFlgInESC(&RR_TCS,&FL_TCS);
	#endif		
	  
	LDTCS_vCalBaseBrkCtlStTh(); 
	LDTCS_vCalBaseBrkCtlTurnTh();
	LDTCS_vCoordinateBaseCtlTh();
	LDTCS_vCalBrkCtlTh4Asym(&FL_TCS,&FL,&RTA_FL);
	LDTCS_vCalBrkCtlTh4Asym(&FR_TCS,&FR,&RTA_FR);
	LDTCS_vCalBrkCtlTh4Asym(&RL_TCS,&RL,&RTA_RL);
	LDTCS_vCalBrkCtlTh4Asym(&RR_TCS,&RR,&RTA_RR);
	LDTCS_vCalBrkCtlTh4Sym();
}   /*TAG3*/

static void LDTCS_vCalCdtESCBrkCtlFlagCopy(void)
{
	FL_TCS.lctcsu1ESCWhlAtv = ESP_BRAKE_CONTROL_FL;
	FR_TCS.lctcsu1ESCWhlAtv = ESP_BRAKE_CONTROL_FR;
	RL_TCS.lctcsu1ESCWhlAtv = ESP_BRAKE_CONTROL_RL;
	RR_TCS.lctcsu1ESCWhlAtv = ESP_BRAKE_CONTROL_RR;
}

static void LDTCS_vCalCdtBTCReduceFlgInESC(struct TCS_WL_STRUCT *Wl_ESC, struct TCS_WL_STRUCT *Wl_TCS)
{
	if ( Wl_ESC->lctcsu1ESCWhlAtv == 1)
	{
		Wl_TCS->lctcsu1BrkTqFactorModeInESC = 1;
	}
	else
	{
		Wl_TCS->lctcsu1BrkTqFactorModeInESC = 0;
	}
}

static void LDTCS_vCalBaseBrkCtlStTh(void)  
{   
	if	(lctcss16BrkCtrlMode == S16_TCS_CTRL_MODE_DP)
    {
		 /*Driveline Protection시 Asym spin 진입  Threshold량*/
    	lctcss16BaseAsymSpnCtlThInSt= LCESP_s16IInter5Point(lctcss16VehSpd4TCS, (int16_t)U16_TCS_SPEED1, (int16_t)S16TCSCpThOfWhlSpnDiffDPInSt_1,  
							    			  	                     		    (int16_t)U16_TCS_SPEED2, (int16_t)S16TCSCpThOfWhlSpnDiffDPInSt_2,  
									                                  		    (int16_t)U16_TCS_SPEED3, (int16_t)S16TCSCpThOfWhlSpnDiffDPInSt_3,  
									                                  		    (int16_t)U16_TCS_SPEED4, (int16_t)S16TCSCpThOfWhlSpnDiffDPInSt_4,  
									                                  		    (int16_t)U16_TCS_SPEED5, (int16_t)S16TCSCpThOfWhlSpnDiffDPInSt_5); 
		 /*Driveline Protection시 sym spin 진입  Threshold량*/
    	lctcss16BaseSymSpnCtlTh= LCESP_s16IInter5Point(lctcss16VehSpd4TCS, (int16_t)U16_TCS_SPEED1, (int16_t)S16TCSCpThOfSymSpnCtlDP_1,  
																		   (int16_t)U16_TCS_SPEED2, (int16_t)S16TCSCpThOfSymSpnCtlDP_2,  
																		   (int16_t)U16_TCS_SPEED3, (int16_t)S16TCSCpThOfSymSpnCtlDP_3,  
																		   (int16_t)U16_TCS_SPEED4, (int16_t)S16TCSCpThOfSymSpnCtlDP_4,  
																		   (int16_t)U16_TCS_SPEED5, (int16_t)S16TCSCpThOfSymSpnCtlDP_5); 									                                  		
    }
	else   
	{
		/*Normal 제어모드*/
		lctcss16BaseAsymSpnCtlThInSt= LCESP_s16IInter5Point(lctcss16VehSpd4TCS, (int16_t)U16_TCS_SPEED1, (int16_t)S16TCSCpThOfWhlSpnDiffInSt_1,  
																			    (int16_t)U16_TCS_SPEED2, (int16_t)S16TCSCpThOfWhlSpnDiffInSt_2,    
																			    (int16_t)U16_TCS_SPEED3, (int16_t)S16TCSCpThOfWhlSpnDiffInSt_3,    
																			    (int16_t)U16_TCS_SPEED4, (int16_t)S16TCSCpThOfWhlSpnDiffInSt_4,    
																			    (int16_t)U16_TCS_SPEED5, (int16_t)S16TCSCpThOfWhlSpnDiffInSt_5); 
																			
		lctcss16BaseSymSpnCtlTh= LCESP_s16IInter5Point(lctcss16VehSpd4TCS, (int16_t)U16_TCS_SPEED1, (int16_t)S16TCSCpThOfSymSpnCtl_1,  
																		   (int16_t)U16_TCS_SPEED2, (int16_t)S16TCSCpThOfSymSpnCtl_2,    
																		   (int16_t)U16_TCS_SPEED3, (int16_t)S16TCSCpThOfSymSpnCtl_3,    
																		   (int16_t)U16_TCS_SPEED4, (int16_t)S16TCSCpThOfSymSpnCtl_4,    
																		   (int16_t)U16_TCS_SPEED5, (int16_t)S16TCSCpThOfSymSpnCtl_5);          				        			                                  
	}
}
    
static void LDTCS_vCalBaseBrkCtlTurnTh(void)
{
			lctcss16BaseAsymSpnCtlThInTurn=LCESP_s16IInter5Point(lctcss16VehSpd4TCS, (int16_t)U16_TCS_SPEED1, (int16_t)S16TCSCpThOfAsymSpCtlInTurn_1,  
																					 (int16_t)U16_TCS_SPEED2, (int16_t)S16TCSCpThOfAsymSpCtlInTurn_2,    
																					 (int16_t)U16_TCS_SPEED3, (int16_t)S16TCSCpThOfAsymSpCtlInTurn_3,    
																					 (int16_t)U16_TCS_SPEED4, (int16_t)S16TCSCpThOfAsymSpCtlInTurn_4,    
																					 (int16_t)U16_TCS_SPEED5, (int16_t)S16TCSCpThOfAsymSpCtlInTurn_5);
}

static void LDTCS_vCoordinateBaseCtlTh(void)
{
		    lctcss16BaseAsymSpnCtlTh = LCESP_s16IInter2Point(lctcss16VehTurnIndex, 0, lctcss16BaseAsymSpnCtlThInSt,
                                                                       100, lctcss16BaseAsymSpnCtlThInTurn);                                                                           
}

static void LDTCS_vCalBrkCtlTh4Asym(struct TCS_WL_STRUCT *Wl_TCS, struct W_STRUCT *W_L, struct RTA_STRUCT *RTA_W_L)
{	    
	if (lctcsu1RoughRoad==1)                
	{
		/* Rough road 감지 시 제어 진입 threshold 증대 */
		lctcss16AsymSpnCtlThInRough = (int16_t)S16TCSCpADDTh4RoughRoad;
	}	
	else	
	{
		lctcss16AsymSpnCtlThInRough = 0;   
	}
		
    if ((W_L->MINI_SPARE_WHEEL==1) || (RTA_W_L->RTA_SUSPECT_WL_FOR_BTCS == 1))
	{
		/* RTA 또는 Mini Tire 감지 시 제어 진입 threshold 증대 */
		Wl_TCS->lctcss16AsymSpnCtlThInRTA = (int16_t)S16TCSCpADDTh4RTA;
	}
	else
	{
		Wl_TCS->lctcss16AsymSpnCtlThInRTA = 0;
	}	
	
	if (Wl_TCS->lctcsu1BrkTqFactorModeInESC==1)
	{
		/* ESC Brake 제어 중에는 제어 진입 둔감화 */
		Wl_TCS->lctcss16AsymSpnCtlThInESCBrk = (int16_t)S16TCSCpADDTh4ESC;
	}
	else	
	{
		Wl_TCS->lctcss16AsymSpnCtlThInESCBrk = 0;

	}
	
	if (lctcss16BrkCtrlMode == S16_TCS_CTRL_MODE_SP)
	{
		lctcss16AsymSpnCtlThAddCompMd = S16TCSCpADDBTCEntTh4ComPMod ;
	}
	else
	{
		lctcss16AsymSpnCtlThAddCompMd = 0;
	}

	/* Final BTCS Threshold */
	Wl_TCS->lctcss16AsymSpnCtlTh = lctcss16BaseAsymSpnCtlTh + lctcss16AsymSpnCtlThInRough + Wl_TCS->lctcss16AsymSpnCtlThInRTA + Wl_TCS->lctcss16AsymSpnCtlThInESCBrk + lctcss16AsymSpnCtlThAddCompMd ;
}

static void LDTCS_vCalBrkCtlTh4Sym(void)
{    
	if (lctcsu1RoughRoad==1)                
	{
		/* Rough road 감지 시 제어 진입 threshold 증대 */
		lctcss16SymSpnCtlThInRough = (int16_t)S16TCSCpADDTh4RoughRoad;
	}	
	else	
	{
		lctcss16SymSpnCtlThInRough = 0;   
	}	
	
    if ((lctcsu1InvalidWhlSpdBfRTADetect == 1) || (lctcsu1WhlSpdCompByRTADetect == 1))
	{
		/* RTA 또는 Mini Tire 감지 시 제어 진입 threshold 증대 */
		lctcss16SymSpnCtlThInRTA = (int16_t)S16TCSCpADDTh4RTA;  
	}
	else
	{
		lctcss16SymSpnCtlThInRTA = 0;
	}	
	
	if ( (YAW_CDC_WORK==1)||(ESP_PULSE_DUMP_FLAG==1) )
	{
		/* ESC Brake 제어 중에는 제어 진입 둔감화 */
		lctcss16SymSpnCtlThInESCBrk = (int16_t)S16TCSCpADDTh4ESC;
	}
	else	
	{
		lctcss16SymSpnCtlThInESCBrk = 0;
	}
	
	if (lctcss16BrkCtrlMode == S16_TCS_CTRL_MODE_SP)
	{
		lctcss16AsymSpnCtlThAddCompMd = S16TCSCpADDBTCEntTh4ComPMod ;
	}
	else
	{
		lctcss16AsymSpnCtlThAddCompMd = 0;
	}
	
	/* Final BTCS Threshold */
	lctcss16SymSpnCtlTh = lctcss16BaseSymSpnCtlTh + lctcss16SymSpnCtlThInRough + lctcss16SymSpnCtlThInRTA + lctcss16SymSpnCtlThInESCBrk + lctcss16AsymSpnCtlThAddCompMd;
}

static void LDTCS_vCalVehSpd4TCS(void)
{
	if((fu1YawSenPwr1sOk==0)||(fu1YawErrorDet==1)||(fu1YawSusDet==1)    /* Yaw Sensor Error */
		||(__ECU==ABS_ECU_1))                                           /* 또는 ABS Only System일 경우    */
	{
		lctcss16VehSpd4TCS = vref;
	}
	else
	{
		lctcss16VehSpd4TCS = vref5;
	}
}	

static void LCTCS_vCalWhlSpdMovingAvg(struct TCS_WL_STRUCT *Wl_TCS, struct W_STRUCT *W_L)
{
	/*=============================================================================*/
	/* Purpose :  각 Wheel Speed의 Moving Average 계산 							   */
	/* Input Variable : W_L_TCS->ltcss32SumOfMovingWindow, W_L_TCS->ltcss16WheelSpeedBuffer, */
	/*                  W_L->vrad_crt											   */
	/* Output Variable : Wl_TCS->ltcss32SumOfMovingWindow, Wl_TCS->ltcss16Index4MovingAvgTCMF,*/
	/*					 Wl_TCS->ltcss16MovingAvgWhlSpdTCMFOld,					   */
	/*					 Wl_TCS->ltcss16MovingAvgWhlSpdTCMF		   				   */
	/* 2006. 12. 19 By Jongtak												       */
	/*=============================================================================*/
	Wl_TCS->ltcss32SumOfMovingWindow = ((Wl_TCS->ltcss32SumOfMovingWindow - ((int32_t)Wl_TCS->ltcss16WheelSpeedBuffer[Wl_TCS->ltcss16Index4MovingAvgTCMF])) + (int32_t)W_L->vrad_crt);
	Wl_TCS->ltcss16WheelSpeedBuffer[Wl_TCS->ltcss16Index4MovingAvgTCMF] = W_L->vrad_crt;

	Wl_TCS->ltcss16MovingAvgWhlSpdTCMFOld = Wl_TCS->ltcss16MovingAvgWhlSpdTCMF;
	tcs_tempW0= LCTCS_s16ILimitRange((int16_t)U8_TCS_MOVING_WINDOW_LENGTH, 1, MOVING_WINDOW_MAX);
	Wl_TCS->ltcss16MovingAvgWhlSpdTCMF = (int16_t)(Wl_TCS->ltcss32SumOfMovingWindow / (int32_t)tcs_tempW0);

	if (Wl_TCS->ltcss16Index4MovingAvgTCMF == (tcs_tempW0 - 1))
	{
		Wl_TCS->ltcss16Index4MovingAvgTCMF = 0;
	}
	else
	{
		Wl_TCS->ltcss16Index4MovingAvgTCMF++;
	}
}

static void LCTCS_vCalWhlSpdMovingAvgDiff(struct TCS_WL_STRUCT *Wl_TCS)
{
	Wl_TCS->ltcss16MovingAvgWhlSpdTCMFDiff = Wl_TCS->ltcss16MovingAvgWhlSpdTCMF - Wl_TCS->ltcss16MovingAvgWhlSpdTCMFOld ;
}


static void LCTCS_vCalWhlSpdMovingAvgCopy(struct TCS_WL_STRUCT *Wl_TCS, struct W_STRUCT *W_L)
{
	/*기존에 Engine 에서 참조하는 모듈을 위해 copy해 준다.*/
	W_L->ltcss16MovingAveragedWhlSpd = Wl_TCS->ltcss16MovingAvgWhlSpdTCMF ;
	W_L->ltcss16MovingAveragedWhlSpdOld = Wl_TCS->ltcss16MovingAvgWhlSpdTCMFOld ;
	W_L->ltcss16Index4MovingAvrg = Wl_TCS->ltcss16Index4MovingAvgTCMF ;
}
	
static void LCTCS_vCalWhlSpdMovingAvgFlt(struct TCS_WL_STRUCT *Wl_TCS, struct W_STRUCT *W_L)
{
  if (Wl_TCS->lctcsu1VIBFlag_tcmf == 1)  /*Vibration발생시 MovingAvg 둔감화 Filtering*/
	{
		tcs_tempW1=L_U8FILTER_GAIN_10MSLOOP_1HZ;  /* 1HZ: 8 */
    	W_L->ltcss16MovingAveragedWhlSpd = LCESP_s16Lpf1Int(W_L->ltcss16MovingAveragedWhlSpd , W_L->ltcss16MovingAveragedWhlSpdOld, (uint8_t)tcs_tempW1);
	}
	else if (lctcsu1GearShiftFlag==1) 
	{
		tcs_tempW1 = LCESP_s16IInter2Point(lctcss16VehTurnIndex, 0, (int16_t)U8TCSCpFltGain4GearShiftSt,
                                                               100, (int16_t)U8TCSCpFltGain4GearShiftTurn);
    	W_L->ltcss16MovingAveragedWhlSpd = LCESP_s16Lpf1Int(W_L->ltcss16MovingAveragedWhlSpd , W_L->ltcss16MovingAveragedWhlSpdOld, (uint8_t)tcs_tempW1);
	}
	else
	{
		;
	}
}

static void LCTCS_vCalWhlSpdAvg4Axle(struct TCS_AXLE_STRUCT *Axle_TCS, struct W_STRUCT *WL_LEFT, struct W_STRUCT *WL_RIGHT)	
{
	Axle_TCS->lctcss16WhlSpdAxleAvg = (WL_LEFT->ltcss16MovingAveragedWhlSpd + WL_RIGHT->ltcss16MovingAveragedWhlSpd)/2;
}

static void LCTCS_vCalWhlSpinAvg4Axle(struct TCS_AXLE_STRUCT *Axle_TCS)
{
	Axle_TCS->lctcss16WhlSpinAxleAvg = Axle_TCS->lctcss16WhlSpdAxleAvg - lctcss16VehSpd4TCS;
	Axle_TCS->lctcss16WhlSpinAxleAvg = LCTCS_s16ILimitMinimum( Axle_TCS->lctcss16WhlSpinAxleAvg, 0 );
}

static void LCTCS_vCalWhlAccAvg4Axle(struct TCS_AXLE_STRUCT *Axle_TCS, struct W_STRUCT *WL_LEFT, struct W_STRUCT *WL_RIGHT)	
{
	Axle_TCS->ltcss16WhlAccAvg4AxleRunCnt = Axle_TCS->ltcss16WhlAccAvg4AxleRunCnt + 1;
	Axle_TCS->ltcss16WhlAccAvg4AxleRunCnt = LCTCS_s16ILimitMaximum(Axle_TCS->ltcss16WhlAccAvg4AxleRunCnt, 6);
	
	Axle_TCS->ltcss16WhlSpdAvgBuf[Axle_TCS->ltcss16WhlAccAvgIndex] = Axle_TCS->lctcss16WhlSpdAxleAvg;
    
    if(Axle_TCS->ltcss16WhlAccAvg4AxleRunCnt < 6)
    {
    	Axle_TCS->ltcss16WhlAccAvg = 0;
    }
    else
    {
		/* 7 = 0.125 kph * 1000 m/3600 s *1/0.05 s *  1 g/9.8 m/s^2    * 100(Scale Factor)   */ 
    	Axle_TCS->ltcss16WhlAccAvg=(Axle_TCS->ltcss16WhlSpdAvgBuf[Axle_TCS->ltcss16WhlAccAvgIndex] - Axle_TCS->ltcss16WhlSpdAvgBuf[Axle_TCS->ltcss16WhlAccAvg50msIndex]) * 7;
    	/* Resolution of ltcss16WhlAccAvg is 0.01g */
	}
	
	if (Axle_TCS->ltcss16WhlAccAvgIndex == 5)    /*10ms로직에서 50ms이전값 총6개 buffer 크기 필요 [0]~[5]   Index는 Buffer선언값-1 주의 */
	{
		Axle_TCS->ltcss16WhlAccAvgIndex = 0;
	}
	else
	{
		Axle_TCS->ltcss16WhlAccAvgIndex = Axle_TCS->ltcss16WhlAccAvgIndex + 1;
	}

	if (Axle_TCS->ltcss16WhlAccAvg50msIndex == 5)  
	{
	    Axle_TCS->ltcss16WhlAccAvg50msIndex = 0;
	}
	else
	{
		Axle_TCS->ltcss16WhlAccAvg50msIndex = Axle_TCS->ltcss16WhlAccAvgIndex + 1;
	}		
}

static void LDTCS_vCalWhlSpinF(struct TCS_WL_STRUCT *Wl_TCS, struct W_STRUCT *W_L)
{
  #if __VDC
	Wl_TCS->lctcss16WhlSpinF = (int16_t)W_L->ltcss16MovingAveragedWhlSpd - W_L->wvref;
  #else	
	Wl_TCS->lctcss16WhlSpinF = (int16_t)W_L->ltcss16MovingAveragedWhlSpd - vref;
  #endif
	Wl_TCS->lctcss16WhlSpinF = LCTCS_s16ILimitMinimum(Wl_TCS->lctcss16WhlSpinF, 0 );  
}

static void LDTCS_vCalWhlSpinNF(struct TCS_WL_STRUCT *Wl_TCS, struct W_STRUCT *W_L)
{
  #if __VDC
	Wl_TCS->lctcss16WhlSpinNF= W_L->vrad_crt - W_L->wvref;
  #else	
	Wl_TCS->lctcss16WhlSpinNF = W_L->vrad_crt - vref;
  #endif
	Wl_TCS->lctcss16WhlSpinNF = LCTCS_s16ILimitMinimum(Wl_TCS->lctcss16WhlSpinNF, 0 );    
}

static void LDTCS_vCalWhlSpinDiffOfLAndR(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT)
{
    Axle_TCS->ltcss16WhlSpinDiff = (int16_t)WL_LEFT->lctcss16WhlSpinF - (int16_t)WL_RIGHT->lctcss16WhlSpinF;  
}

static void LDTCS_vCalWhlSpinAvgOfLAndR(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT)
{   
    Axle_TCS->ltcss16WhlSpinAvg = ((int16_t)WL_LEFT->lctcss16WhlSpinF + (int16_t)WL_RIGHT->lctcss16WhlSpinF)/2;  
}

static void LDTCS_vCalWhlSpinDiffOfLAndRNF(struct TCS_AXLE_STRUCT *Axle_TCS, struct TCS_WL_STRUCT *WL_LEFT, struct TCS_WL_STRUCT *WL_RIGHT)
{
    Axle_TCS->ltcss16WhlSpinDiffNF = (int16_t)WL_LEFT->lctcss16WhlSpinNF - (int16_t)WL_RIGHT->lctcss16WhlSpinNF;  
}

static void LDTCS_vCalChngRateOfWhlSpinDiff(struct TCS_AXLE_STRUCT *Axle_TCS)
{
	Axle_TCS->ltcss8SupCROfWSCnt=Axle_TCS->ltcss8SupCROfWSCnt +1;  /*50ms이전값을 이용한 SpinDiff계산은 함수가
	                                                                                     6회 호출된 때부터 정상값 내보내므로 이전값은 초기화*/
	Axle_TCS->ltcss8SupCROfWSCnt=(int8_t)LCTCS_u16ILimitMaximum(((uint16_t)Axle_TCS->ltcss8SupCROfWSCnt),6);
	
	Axle_TCS->ltcss16WhlSpinDiffBuf[Axle_TCS->ltcss16WhlSpinDiffIndex]=Axle_TCS->ltcss16WhlSpinDiff;
	Axle_TCS->ltcss16WhlSpinDiffBufNF[Axle_TCS->ltcss16WhlSpinDiffIndex]=Axle_TCS->ltcss16WhlSpinDiffNF;
    
    if(Axle_TCS->ltcss8SupCROfWSCnt<6)
    {
    	Axle_TCS->ltcss16ChngRateWhlSpinDiff=0;
    	Axle_TCS->ltcss16ChngRateWhlSpinDiffNF=0;
    }
    else
    {
    	/* 7 = 0.125 kph * 1000 m/3600 s *1/0.05 s *  1 g/9.8 m/s^2    * 100 Scale Factor   */
    	Axle_TCS->ltcss16ChngRateWhlSpinDiff=( McrAbs(Axle_TCS->ltcss16WhlSpinDiffBuf[Axle_TCS->ltcss16WhlSpinDiffIndex])-McrAbs(Axle_TCS->ltcss16WhlSpinDiffBuf[(Axle_TCS->ltcss16WhlSpinDiffB50msIndex)]))*7;	                        
		/* Resolution of ltcss16ChngRateWhlSpinDiff is 0.01g */
        Axle_TCS->ltcss16ChngRateWhlSpinDiffNF=( McrAbs(Axle_TCS->ltcss16WhlSpinDiffBufNF[Axle_TCS->ltcss16WhlSpinDiffIndex])-McrAbs(Axle_TCS->ltcss16WhlSpinDiffBufNF[(Axle_TCS->ltcss16WhlSpinDiffB50msIndex)]))*7; 	                         
	}
	
	if (Axle_TCS->ltcss16WhlSpinDiffIndex==5 )    /*10ms로직에서 50ms이전값 총6개 buffer 크기 필요 [0]~[5]   Index는 Buffer선언값-1 주의 */
	{
		Axle_TCS->ltcss16WhlSpinDiffIndex = 0;
	}
	else
	{
		Axle_TCS->ltcss16WhlSpinDiffIndex=Axle_TCS->ltcss16WhlSpinDiffIndex+1;
	}
	
	Axle_TCS->ltcss16WhlSpinDiffB50msIndex=Axle_TCS->ltcss16WhlSpinDiffIndex+1;  /*6개 Buffer가 Circle로 돌기 때문에 현재Index값 +1 값이 50ms 이전 값이 된다. */
		
	if (Axle_TCS->ltcss16WhlSpinDiffB50msIndex == 6)  
	{
	    Axle_TCS->ltcss16WhlSpinDiffB50msIndex=0;		
	}
	else
	{
	    ;
	}		
}

static void LDTCS_vCalMassOfOneCorner(void)
{
    FA_TCS.lctcss16MassOfOneCorn	=  (pEspModel->S16_VEHICLE_MASS_FL + pEspModel->S16_VEHICLE_MASS_FR) /2 ;
    RA_TCS.lctcss16MassOfOneCorn	=  (pEspModel->S16_VEHICLE_MASS_RL + pEspModel->S16_VEHICLE_MASS_RR) / 2 ;
}

static void LDTCS_vCalWhlRolRadius(void)
{
	FA_TCS.lctcss16WhlRolR=(int16_t)(((int32_t)U16_TIRE_L*10)/62);     /* 2 * phi * r = TIRE_L      phi=3.1가정*/
	RA_TCS.lctcss16WhlRolR=(int16_t)(((int32_t)U16_TIRE_L_R*10)/62);
} 

static void LCTCS_vCalWhlMovAvgDiffLAndR(struct TCS_AXLE_STRUCT *Axle_TCS, struct W_STRUCT *WL_LEFT , struct W_STRUCT *WL_RIGHT)
{
	  Axle_TCS->ltcss16MovAvgDiffLAndR = McrAbs(WL_LEFT->ltcss16MovingAveragedWhlSpd - WL_RIGHT->ltcss16MovingAveragedWhlSpd);
}

static void LDTCS_vSlopeOfSpinDiff (struct TCS_AXLE_STRUCT *Axle_TCS)
{
	if(Axle_TCS->lctcsu1BrkCtlActAxle==1)
	{
		if(Axle_TCS->ltcss16ChngRateWhlSpinDiff > 0)
		{
			Axle_TCS->ltcsu8SpinDiffIncCnt=Axle_TCS->ltcsu8SpinDiffIncCnt + 1;
			Axle_TCS->ltcsu8SpinDiffIncCnt = (uint8_t)LCTCS_u16ILimitMaximum((uint16_t)Axle_TCS->ltcsu8SpinDiffIncCnt , 10 );
			
			if(Axle_TCS->ltcsu8SpinDiffDecCnt >0)
			{
				Axle_TCS->ltcsu8SpinDiffDecCnt = Axle_TCS->ltcsu8SpinDiffDecCnt - 1;
			}
			else
			{
				Axle_TCS->ltcsu8SpinDiffDecCnt = 0;
			}
		}
		else if (Axle_TCS->ltcss16ChngRateWhlSpinDiff < 0)
		{
			Axle_TCS->ltcsu8SpinDiffDecCnt=Axle_TCS->ltcsu8SpinDiffDecCnt + 1;
			Axle_TCS->ltcsu8SpinDiffDecCnt = (uint8_t)LCTCS_u16ILimitMaximum( (uint16_t)Axle_TCS->ltcsu8SpinDiffDecCnt , 10 );
			
			if(Axle_TCS->ltcsu8SpinDiffIncCnt >0)
			{
				Axle_TCS->ltcsu8SpinDiffIncCnt = Axle_TCS->ltcsu8SpinDiffIncCnt - 1;
			}
			else
			{
				Axle_TCS->ltcsu8SpinDiffIncCnt = 0;
			}
	    }
	    else
	    {
	    	;	    	
	    }
	    
	    if(Axle_TCS->ltcsu8SpinDiffIncCnt>= U8TCSCpThOfCnt4SpnDiffSlopeDct)
	    {
	    	Axle_TCS->ltcsu1SpinDiffInc=1;   /*Slope Increase Flag 설정*/
	    }
	    else if(Axle_TCS->ltcsu8SpinDiffIncCnt==0)
	    {
	    	Axle_TCS->ltcsu1SpinDiffInc=0;  
	    }
	    else
	    {
	    	;
	    }
	    
	    if(Axle_TCS->ltcsu8SpinDiffDecCnt>= U8TCSCpThOfCnt4SpnDiffSlopeDct)
	    {
	    	Axle_TCS->ltcsu1SpinDiffDec=1;   /*Slope Decrease Flag 설정*/
	    }
	    else if(Axle_TCS->ltcsu8SpinDiffDecCnt==0)
	    {
	    	Axle_TCS->ltcsu1SpinDiffDec=0;  
	    }
	    else
	    {
	    	;
	    }    	    
	}
	else
	{
		Axle_TCS->ltcsu8SpinDiffIncCnt=0;
		Axle_TCS->ltcsu8SpinDiffDecCnt=0;
		Axle_TCS->ltcsu1SpinDiffInc=0;
		Axle_TCS->ltcsu1SpinDiffDec=0;
	}
}

static void LDTCS_vCalIndexOfVehTurn(void)
{
	  if ((fu1YawErrorDet==0) && (fu1AyErrorDet==0) )
	  {
	      lctcss16DctTurnAyEnterTh= LCESP_s16IInter5Point(lctcss16VehSpd4TCS, (int16_t)U16_TCS_SPEED1, (int16_t)S16TCSCpDctTurnIndexEnterAyTh_1,  
			    			    			  	                     		  (int16_t)U16_TCS_SPEED2, (int16_t)S16TCSCpDctTurnIndexEnterAyTh_2,  
				    					                                  	  (int16_t)U16_TCS_SPEED3, (int16_t)S16TCSCpDctTurnIndexEnterAyTh_3,  
				    					                                  	  (int16_t)U16_TCS_SPEED4, (int16_t)S16TCSCpDctTurnIndexEnterAyTh_4,  
				    					                                  	  (int16_t)U16_TCS_SPEED5, (int16_t)S16TCSCpDctTurnIndexEnterAyTh_5);
				    					                                  		               
          lctcss16DctTurnAyFinalTh= LCESP_s16IInter5Point(lctcss16VehSpd4TCS, (int16_t)U16_TCS_SPEED1, (int16_t)S16TCSCpDctTurnIndexFinalAyTh_1,  
			      			    			  	                     		  (int16_t)U16_TCS_SPEED2, (int16_t)S16TCSCpDctTurnIndexFinalAyTh_2,  
				    					                                  	  (int16_t)U16_TCS_SPEED3, (int16_t)S16TCSCpDctTurnIndexFinalAyTh_3,  
				    					                                  	  (int16_t)U16_TCS_SPEED4, (int16_t)S16TCSCpDctTurnIndexFinalAyTh_4,  
				    					                                  	  (int16_t)U16_TCS_SPEED5, (int16_t)S16TCSCpDctTurnIndexFinalAyTh_5); 
				    					                                  		               
		
		#if (__VEHICLE_TURN_INDEX_BY_MOMENT==ENABLE)
		lctcss16VehTurnIndexOld = lctcss16VehTurnIndex;
		tcs_tempW0 = McrAbs(nor_alat);
		if (tcs_tempW0 > S16TCSCpDctTurnIndexBaseAyTh)
		{
			if ( ((nor_alat < 0)&&((FL_TCS.lctcsu1BTCSWhlAtv==1)||(RL_TCS.lctcsu1BTCSWhlAtv==1))) || ((nor_alat > 0)&&((FR_TCS.lctcsu1BTCSWhlAtv==1)||(RR_TCS.lctcsu1BTCSWhlAtv==1))) )
			{
				lctcss16VehTurnIndex = 0;
				lctcss16VehTurnIndex = LCESP_s16Lpf1Int(lctcss16VehTurnIndex, lctcss16VehTurnIndexOld, L_U8FILTER_GAIN_10MSLOOP_3HZ);
			}
			else
			{
				lctcss16VehTurnIndex=LCESP_s16IInter2Point(tcs_tempW0, lctcss16DctTurnAyEnterTh, 0,
				                                                       lctcss16DctTurnAyFinalTh, 100);				
			}
		}
		else
		{
			lctcss16VehTurnIndex = 0;
			lctcss16VehTurnIndex = LCESP_s16Lpf1Int(lctcss16VehTurnIndex, lctcss16VehTurnIndexOld, L_U8FILTER_GAIN_10MSLOOP_3HZ);
		}
		#else
          lctcss16VehTurnIndex=LCESP_s16IInter2Point(det_alatc, lctcss16DctTurnAyEnterTh, 0,
                                                                lctcss16DctTurnAyFinalTh, 100);
		#endif		                                              
	  }
	  else
	  {
	      lctcss16VehTurnIndex = 0;
	  }
}

static void LDTCS_vCalBrkWrkUntHghMuWhlUnst(struct TCS_AXLE_STRUCT *Axle_TCS)
{
	if (Axle_TCS->lctcsu1BrkCtlActAxle==1)
	{
		if (
			 (Axle_TCS->lctcsu1HghMuWhlUnstb2Any == 1) ||
			 (Axle_TCS->lctcsu8StatWhlOnHghMu == TCSHighMuNotDcted)
		   )
		{
			Axle_TCS->lctcss16BrkWrkUntHghMuWhlUnst = 0;
			Axle_TCS->lctcss16EstBrkTorqOnLowMuWhlMem = 0;
		}
		else if	(Axle_TCS->lctcsu8StatWhlOnHghMu == TCSHighMuStable)
		{
			/* Work[n] = Work[n-1] + Axle_TCS->lctcss16EstBrkTorqOnLowMuWhl[Nm] * 0.01[sec] */
			/* 0.01sec means control cycle 10ms */
			
			if ( Axle_TCS->lctcss16EstBrkTorqOnLowMuWhlMem < 100 )  /*누적 torque 제어량에서 100Nm 이하가 divide되며 삭제됨을 방지*/
			{
				 Axle_TCS->lctcss16EstBrkTorqOnLowMuWhlMem = Axle_TCS->lctcss16EstBrkTorqOnLowMuWhlMem + Axle_TCS->lctcss16EstBrkTorqOnLowMuWhl;
				 Axle_TCS->tcs_axle_tempW0 = Axle_TCS->lctcss16EstBrkTorqOnLowMuWhl;
			}
			else
			{
				Axle_TCS->tcs_axle_tempW0 = Axle_TCS->lctcss16EstBrkTorqOnLowMuWhl + Axle_TCS->lctcss16EstBrkTorqOnLowMuWhlMem;
				Axle_TCS->lctcss16EstBrkTorqOnLowMuWhlMem = 0;
			}
			
			Axle_TCS->lctcss16BrkWrkUntHghMuWhlUnst = Axle_TCS->lctcss16BrkWrkUntHghMuWhlUnst + (Axle_TCS->tcs_axle_tempW0/100);
			Axle_TCS->lctcss16BrkWrkUntHghMuWhlUnst = LCTCS_s16ILimitRange(Axle_TCS->lctcss16BrkWrkUntHghMuWhlUnst, 0, 10000);
		}
		else
		{	/* lctcsu8StatWhlOnHghMu 가 unstable 일 경우는 현 상태 유지 */
			;
		}
	}
	else
	{
		Axle_TCS->lctcss16BrkWrkUntHghMuWhlUnst = 0;
		Axle_TCS->lctcss16EstBrkTorqOnLowMuWhlMem = 0;
	}	
}

static void LDTCS_vCalDistanceOfStopNgo(void)
{
	if (( lctcss16VehSpd4TCS>=VREF_0_5_KPH ) && (lctcsu16BTCStpNMovDistance < TCSHillMovDistanceMAX))/*(차량의 위치가 이동되면 계산)   */
	{
		if(lctcsu16BTCStpNMovCnt < (L_U8_TIME_10MSLOOP_100MS - 1) )
		{
			lctcsu16BTCStpNMovCnt = lctcsu16BTCStpNMovCnt + 1;
		}
		else
		{	
			lctcsu16BTCStpNMovCnt = 0 ;
			
			tcs_tempW1 = (lctcss16VehSpd4TCS * 7)/2;    /* (이동거리   kph -> m/10 scan   1000/(3600*8*10)      ) */
		    	                                        /*                            = Factor 0.0035*/
		                                                /* 1m = 1000 의 Scale 로 변경 하면 Factor 3.5 = 7/2 */           
			lctcsu16BTCStpNMovBsDistance = lctcsu16BTCStpNMovBsDistance + tcs_tempW1;  
			
			lctcsu16BTCStpNMovDistance = TCSStpNMovDistanceOffSet10Cm + lctcsu16BTCStpNMovBsDistance ;
		lctcsu16BTCStpNMovDistance = LCTCS_u16ILimitMaximum(lctcsu16BTCStpNMovDistance, TCSHillMovDistanceMAX);  /*Max 60미터*/
	}
	}
	else if ( lctcss16VehSpd4TCS <= VREF_0_125_KPH)
	{
		lctcsu16BTCStpNMovDistance = 0;
		lctcsu16BTCStpNMovBsDistance = 0;
		lctcsu16BTCStpNMovCnt = 0 ;
	}
	else
	{
		;
	}
}

static void LDTCS_vVIBCalWhlSpdCounter(struct TCS_WL_STRUCT *Wl_TCS, struct W_STRUCT *W_L)      /*Vibration control*/     
{	 
	/*=============================================================================*/
	/* Purpose : 각 Wheel Speed 증가 및 감소시 Counter 적용                        */
	/* Input Variable  : lctcsu16VIBvrad_crtold, lctcsu16VIBvrad_crt, vrad_crt     */
	/* Output Variable : lctcsu8VIBWhlIncreseCounter, lctcsu8VIBWhlDecreseCounter  */
	/* 2009. 10. 23 By ByoungJin Park            							       */
	/*=============================================================================*/	
   
	Wl_TCS->lctcsu16VIBvrad_crtold_tcmf = Wl_TCS->lctcsu16VIBvrad_crt_tcmf;
	Wl_TCS->lctcsu16VIBvrad_crt_tcmf = (uint16_t)W_L->vrad_crt; 
	
	/* Wheel Speed 증가 경향 */	
	if (Wl_TCS->lctcsu16VIBvrad_crtold_tcmf < Wl_TCS->lctcsu16VIBvrad_crt_tcmf)
	{	
		if (Wl_TCS->lctcsu8VIBWhlIncreseCnt_tcmf < U8_TCS_VIB_CNT_SCAN)
		{
			Wl_TCS->lctcsu8VIBWhlIncreseCnt_tcmf = Wl_TCS->lctcsu8VIBWhlIncreseCnt_tcmf +1;			
		}
		else
		{
   	 		;
		}		
		
		if (Wl_TCS->lctcsu8VIBWhlDecreseCnt_tcmf > 0)
		{
			Wl_TCS->lctcsu8VIBWhlDecreseCnt_tcmf = 0;
		}
		else
		{
			;
		}
	}
	/* Wheel Speed 감소 경향 */
	else if (Wl_TCS->lctcsu16VIBvrad_crtold_tcmf > Wl_TCS->lctcsu16VIBvrad_crt_tcmf)
	{	
		if (Wl_TCS->lctcsu8VIBWhlIncreseCnt_tcmf > 0)
		{
			Wl_TCS->lctcsu8VIBWhlIncreseCnt_tcmf = 0;
		}
		else
		{
			;
		}
		if (Wl_TCS->lctcsu8VIBWhlDecreseCnt_tcmf < U8_TCS_VIB_CNT_SCAN)
		{
			Wl_TCS->lctcsu8VIBWhlDecreseCnt_tcmf = Wl_TCS->lctcsu8VIBWhlDecreseCnt_tcmf +1;						
		}
		else
		{
			;
		}				
	}
	else
	{
		;
	}
}        /*Vibration control*/

static void LDTCS_vVIBDetectWhlSpdRiseFall(struct TCS_WL_STRUCT *Wl_TCS)  /*Vibration control*/
{
	/*=============================================================================*/
	/* Purpose : 각 Wheel Speed 파형에 대한 Peak Point Rising 및 Falling 구간 판단 */
	/* Input Variable : lctcsu8VIBWhlIncreseCounter, lctcsu8VIBWhlDecreseCounter   */
	/* Output Variable : lctcsu1VIBRisingold, lctcsu1VIBRising,                    */
    /*                   lctcsu1VIBFallingold, lctcsu1VIBFalling                   */
	/*					 lctcsu16VIBAmplitudeold, lctcsu16VIBAmplitude             */
	/*                   lctcsu16DeltaTime, lctcsu8VIBCycleTimecnt                 */
	/*                   lctcsu1VIBAmplitudeUpdCnt,                                */
	/*                   U8_TCS_VIB_CNT_SCAN	                                   */
	/* 2009. 10. 23 By ByoungJin Park            							       */
	/*=============================================================================*/	
	
   	Wl_TCS->lctcsu1VIBRisingold_tcmf = Wl_TCS->lctcsu1VIBRising_tcmf;
   	Wl_TCS->lctcsu1VIBFallingold_tcmf = Wl_TCS->lctcsu1VIBFalling_tcmf;
	
   	/* Rising & Falling 판단 */  
   	/* 판단 안된 부분 */
   	if   ( (Wl_TCS->lctcsu1VIBRising_tcmf == 0) && (Wl_TCS->lctcsu1VIBFalling_tcmf == 0) )
	{			
		if	(Wl_TCS->lctcsu8VIBWhlIncreseCnt_tcmf >= U8_TCS_VIB_CNT_SCAN )
		{
			Wl_TCS->lctcsu1VIBRising_tcmf = 1;
			Wl_TCS->lctcsu1VIBFalling_tcmf = 0;
		}
		else if (Wl_TCS->lctcsu8VIBWhlDecreseCnt_tcmf >= U8_TCS_VIB_CNT_SCAN )
		{
			Wl_TCS->lctcsu1VIBRising_tcmf = 0;
			Wl_TCS->lctcsu1VIBFalling_tcmf = 1;
		}
		else
		{
			;
		}
	}
	/* Rising 판단 */
   	else if	( (Wl_TCS->lctcsu1VIBRising_tcmf == 1) && (Wl_TCS->lctcsu1VIBFalling_tcmf == 0) )
	{
		if (Wl_TCS->lctcsu8VIBWhlDecreseCnt_tcmf >= U8_TCS_VIB_CNT_SCAN )
		{
			Wl_TCS->lctcsu1VIBRising_tcmf = 0;
			Wl_TCS->lctcsu1VIBFalling_tcmf = 1;
		}
		else
		{
			;
		}		
	}
	/* Falling 판단 */
	else if ( (Wl_TCS->lctcsu1VIBRising_tcmf == 0) && (Wl_TCS->lctcsu1VIBFalling_tcmf == 1) )
	{
		if (Wl_TCS->lctcsu8VIBWhlIncreseCnt_tcmf >= U8_TCS_VIB_CNT_SCAN )
		{
			Wl_TCS->lctcsu1VIBRising_tcmf = 1;
			Wl_TCS->lctcsu1VIBFalling_tcmf = 0;
		}
		else
		{
			;
		}
	}
	else
	{
		; 
	}
	
	/* Update Amplitude 및 Time 설정 */
	Wl_TCS->lctcsu16VIBWhlAmplitudeold_tcmf = Wl_TCS->lctcsu16VIBWhlAmplitude_tcmf;
	
	/* Update Amplitude 설정 */
	if ( ((Wl_TCS->lctcsu1VIBRisingold_tcmf == 0) && (Wl_TCS->lctcsu1VIBRising_tcmf == 1)) 
	|| ( (Wl_TCS->lctcsu1VIBFallingold_tcmf == 0) && (Wl_TCS->lctcsu1VIBFalling_tcmf == 1) ) )
	{
		Wl_TCS->lctcsu16VIBWhlAmplitude_tcmf = Wl_TCS->lctcsu16VIBvrad_crt_tcmf;
	}
	else
	{
		;
	}
	
	/* Wheel Speed(Under Peak Point)와 Vref의 차이값 설정 */
	if  ((Wl_TCS->lctcsu1VIBRisingold_tcmf == 0) && (Wl_TCS->lctcsu1VIBRising_tcmf == 1)) 
   	{
   		Wl_TCS->lctcsu16VIBvradErrvref_tcmf = (uint16_t)McrAbs((int16_t)Wl_TCS->lctcsu16VIBvrad_crt_tcmf - lctcss16VehSpd4TCS);
   	}
   	else
   	{
   		;
   	}		
	
	if ( ((Wl_TCS->lctcsu1VIBFallingold_tcmf == 0) && (Wl_TCS->lctcsu1VIBFalling_tcmf == 1)) )
	{
		Wl_TCS->lctcsu1VIBAmplitudeUpdCnt_tcmf = 1; /* Frequency 계산하기 위한 Counter */
		Wl_TCS->lctcsu1VIBCycleStartTime_tcmf = 1; /* 1주기별 시간을 계산하기 위한 Counter 1=0.01s */
	}
	else
	{
		Wl_TCS->lctcsu1VIBAmplitudeUpdCnt_tcmf = 0;	
	}		
	
	if ( (Wl_TCS->lctcsu1VIBCycleStartTime_tcmf == 1) )
	{
		Wl_TCS->lctcsu8VIBCycleTimecnt_tcmf = Wl_TCS->lctcsu8VIBCycleTimecnt_tcmf +1;
	}
	else
	{
		;
	}
}                                                 /*Vibration control*/

static void LDTCS_vVIBCalFreqAmp(struct TCS_WL_STRUCT *Wl_TCS)           /*Vibration control*/
{	
	/*=============================================================================*/
	/* Purpose : Wheel Speed 파형의 주기별 Frequency 및 Amplitude 계산             */
	/* Input Variable : lctcsu1VIBAmplitudeUpdCnt, lctcsu16DeltaTime               */
	/*                  lctcsu16VIBWhlAmplitudeold, lctcsu16VIBWhlAmplitude        */
	/* Output Variable : lctcsu16VIBAmplitudeold, lctcsu16VIBAmplitude       	   */
	/*                   lctcsu8Frequencycounter, lctcsu8VIBFreqency               */
	/*                   lctcsu8VIBAmplitudeUpdCnt                                 */
	/* 2009. 10. 23 By ByoungJin Park            							       */
	/*=============================================================================*/	
	/*Amplitude & Frequency 크기 셋팅 & 같은 값 나올시 "0"이 되므로 그 전값 유지*/
	
	if ( (Wl_TCS->lctcsu16VIBWhlAmplitudeold_tcmf == 0) )
	{	
		Wl_TCS->lctcsu16VIBWhlAmplitudeold_tcmf = Wl_TCS->lctcsu16VIBWhlAmplitude_tcmf;
	}
	else 
	{
		;
	}
	
	Wl_TCS->lctcsu16VIBAmplitudeold_tcmf = Wl_TCS->lctcsu16VIBAmplitude_tcmf;
	Wl_TCS->lctcsu16VIBAmplitude_tcmf = (uint16_t)McrAbs((int16_t)(Wl_TCS->lctcsu16VIBWhlAmplitude_tcmf - Wl_TCS->lctcsu16VIBWhlAmplitudeold_tcmf));
	
	if ( (Wl_TCS->lctcsu16VIBAmplitude_tcmf == 0) )
   	{
   		Wl_TCS->lctcsu16VIBAmplitude_tcmf = Wl_TCS->lctcsu16VIBAmplitudeold_tcmf;
   	}
   	else
   	{
   		;
   	}
   	
   	if ( (Wl_TCS->lctcsu1VIBAmplitudeUpdCnt_tcmf == 1) )
   	{
   		Wl_TCS->lctcsu8VIBAmplitudeUpdCnt_tcmf = Wl_TCS->lctcsu8VIBAmplitudeUpdCnt_tcmf +1;
   		if ( (Wl_TCS->lctcsu8VIBAmplitudeUpdCnt_tcmf == 2) )
   		{
   			/* Frequency를 Hz 단위로 변환, 분모가 "0"일시 값이 없으므로 "0"으로 셋팅 */
   	      if ( Wl_TCS->lctcsu8VIBCycleTimecnt_tcmf == 0 )
	        {
	        	Wl_TCS->lctcsu8VIBFreqency_tcmf = 0;
	        }
	        else
	        {
	        	Wl_TCS->lctcsu8VIBFreqencyold_tcmf = Wl_TCS->lctcsu8VIBFreqency_tcmf;
	        	Wl_TCS->lctcsu8VIBFreqency_tcmf = (uint8_t)(1000 / ((Wl_TCS->lctcsu8VIBCycleTimecnt_tcmf)*LOOP_10MS));
	        	Wl_TCS->lctcsu8VIBCycleTime_tcmf = Wl_TCS->lctcsu8VIBCycleTimecnt_tcmf;
	        }
   			Wl_TCS->lctcsu8VIBCycleTimecnt_tcmf = 0;
   			Wl_TCS->lctcsu8VIBAmplitudeUpdCnt_tcmf = 1;
   		}
   		else
   		{
   			;
   		}
   	}
   	else
   	{
   		;
   	}
   	
   	/* Special Amplitudemag Check */
   	/* 전 스텝의 Frequency가 길시 다음 스텝 단계에 영향(당시 Frequency가 작더라도 영향)*/
   	if ( (Wl_TCS->lctcsu8VIBCycleTimecnt_tcmf > U8_TCS_VIB_OVERFLOW_SCAN) )
   	{
   		/*초기값으로 셋팅하는 거 대입*/
   		Wl_TCS->lctcsu1VIBCycleStartTime_tcmf = 0;
   		Wl_TCS->lctcsu16VIBWhlAmplitude_tcmf = 0;
		Wl_TCS->lctcsu16VIBAmplitude_tcmf = 0;
		Wl_TCS->lctcsu8VIBCycleTimecnt_tcmf = 0;
		Wl_TCS->lctcsu8VIBFreqency_tcmf = 0;
		Wl_TCS->lctcsu8VIBAmplitudeUpdCnt_tcmf = 0;
		Wl_TCS->lctcsu1VIBFlag_tcmf = 0;		
   		Wl_TCS->lctcsu8VIBFlagCycleCnt_tcmf = 0;
   		Wl_TCS->lctcsu8VIBFlagoutCycleCnt_tcmf = 0;  
	}
	else
	{
		;
	}
}                                                /*Vibration control*/
  #endif 	/* TAG2 */ 
#endif  /* TAG1*/

/* AUTOSAR --------------------------*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LDTCSCallDetection
	#include "Mdyn_autosar.h"
#endif
/* AUTOSAR --------------------------*/

