#ifndef __LAARBCALLACTHW_H__
#define __LAARBCALLACTHW_H__

/*includes********************************************************************/
#include "LVarHead.h"


  #if __HEV
/*Global Type Declaration ****************************************************/



/*Global Extern Variable  Declaration*****************************************/



/*Global Extern Functions  Declaration****************************************/
extern void	LAARB_vCallActHW(void);
extern void	LAARB_vCallActHWOffEBD(void);
  #endif
#endif
