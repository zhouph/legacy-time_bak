
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LSESPCalSlipRatio
	#include "Mdyn_autosar.h"
#endif
#include "LSESPCalSlipRatio.h"
#include "LCESPCalLpf.h"
#include "LCallMain.h"
#include "LCESPCallControl.h"
#if	__VDC
   #if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
     void 	LSESP_vCompWheelSlip(struct ESC_STRUCT *WL_ESC);
   #else
void 	LSESP_vCompWheelSlip(void);
#endif
#endif

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
void LSESP_vCalWheelSpeedrate(struct ESC_STRUCT *WL_ESC);
#else
void LSESP_vCalWheelSpeedrate(void);
#endif
 
#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//int16_t lcescs16CalWheelSpeedRateflold ; 
//int16_t lcescs16CalWheelSpeedRatefrold ; 
//int16_t lcescs16CalWheelSpeedRaterlold ; 
//int16_t lcescs16CalWheelSpeedRaterrold ; 

//int16_t lcescs16CalWheelSpeedRatefl ; 
//int16_t lcescs16CalWheelSpeedRatefr ; 
//int16_t lcescs16CalWheelSpeedRaterl ; 
//int16_t lcescs16CalWheelSpeedRaterr ; 

//int16_t lcescs16CalWheelSpeedRateflar[4] ; 
//int16_t lcescs16CalWheelSpeedRatefrar[4] ; 
//int16_t lcescs16CalWheelSpeedRaterlar[4] ; 
//int16_t lcescs16CalWheelSpeedRaterrar[4] ; 

//int16_t lcescs16CalDelWheelSpeedRatefl;
//int16_t lcescs16CalDelWheelSpeedRatefr;
//int16_t lcescs16CalDelWheelSpeedRaterl;
//int16_t lcescs16CalDelWheelSpeedRaterr;

//int16_t slip_m_fl_old, slip_m_fl;
//int16_t slip_m_fr_old, slip_m_fr;
//int16_t slip_m_rl_old, slip_m_rl;
//int16_t slip_m_rr_old, slip_m_rr;
#else
int16_t lcescs16CalWheelSpeedRateflold ; 
int16_t lcescs16CalWheelSpeedRatefrold ; 
int16_t lcescs16CalWheelSpeedRaterlold ; 
int16_t lcescs16CalWheelSpeedRaterrold ; 

int16_t lcescs16CalWheelSpeedRatefl ; 
int16_t lcescs16CalWheelSpeedRatefr ; 
int16_t lcescs16CalWheelSpeedRaterl ; 
int16_t lcescs16CalWheelSpeedRaterr ; 

int16_t lcescs16CalWheelSpeedRateflar[4] ; 
int16_t lcescs16CalWheelSpeedRatefrar[4] ; 
int16_t lcescs16CalWheelSpeedRaterlar[4] ; 
int16_t lcescs16CalWheelSpeedRaterrar[4] ; 

int16_t lcescs16CalDelWheelSpeedRatefl;
int16_t lcescs16CalDelWheelSpeedRatefr;
int16_t lcescs16CalDelWheelSpeedRaterl;
int16_t lcescs16CalDelWheelSpeedRaterr;

int16_t slip_m_fl_old, slip_m_fl;
int16_t slip_m_fr_old, slip_m_fr;
int16_t slip_m_rl_old, slip_m_rl;
int16_t slip_m_rr_old, slip_m_rr;
#endif

int16_t slip_filter_gain;

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
//int16_t del_speed_fl, del_speed_fr, del_speed_rl, del_speed_rr;
#else
int16_t del_speed_fl, del_speed_fr, del_speed_rl, del_speed_rr;
#endif


 

/* ====================================================================== */
#if	__VDC

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
void LSESP_vCompWheelSlip(struct ESC_STRUCT *WL_ESC)
{
    WL_ESC->slip_m_old = WL_ESC->slip_m;
    
    if(ESP_ROUGH_ROAD==1) 
    {
        slip_filter_gain = ESP_SLIP_FILTER_GAIN_ROUGH;
    } 
    else if(ESP_PULSE_DUMP_FLAG==1) 
    {
        slip_filter_gain=L_U8FILTER_GAIN_10MSLOOP_12_5HZ;
    }
    else 
    {
        slip_filter_gain = ESP_SLIP_FILTER_GAIN;
    }           

    if ( WL_ESC->wvref > VREF_2_KPH ) 
    {
        
        #if __WL_SPEED_RESOL_CHANGE
        tempW2 = (WL_ESC->vrad_crt_resol_change-WL_ESC->wvref_resol_change);
        tempW1 = 10000; 
        if ( tempW2 > 0 ) 
        {
        	tempW0 = WL_ESC->vrad_crt_resol_change;
        }
        else 
        {
        	tempW0 = WL_ESC->wvref_resol_change; 
        }       
        #else
        tempW2 = (WL_ESC->vrad_crt-WL_ESC->wvref); 
        tempW1 = 10000; 
        if ( tempW2 > 0 ) 
        {
        	tempW0 = WL_ESC->vrad_crt;
        }
        else 
        {
        	tempW0 = WL_ESC->wvref;
        }
        #endif
        s16muls16divs16();
        
        if ( tempW3 > 10000 ) 
        {
        	tempW3 = 10000;
        }
        else if ( tempW3 < -10000 ) 
        {
        	tempW3 = -10000;
        }
        else
        {
        	;
        }
        esp_tempW2 = tempW3;    /* r : 0.01 */

        if ( (ABS_fz==1) && (esp_tempW2<(-ESP_SLIP_FILTER_CHANGE_SLIP)))
        {
            slip_filter_gain_varying = ESP_SLIP_FILTER_GAIN_DEEP_SLIP_ABS;
        }
        else if ( esp_tempW2<(-ESP_SLIP_FILTER_CHANGE_SLIP))
        {
            slip_filter_gain_varying = ESP_SLIP_FILTER_GAIN_DEEP_SLIP;  
        }
        else 
        {
            slip_filter_gain_varying = slip_filter_gain;       
        }             
        
        WL_ESC->slip_m = LCESP_s16Lpf1Int(esp_tempW2, WL_ESC->slip_m_old, (uint8_t)slip_filter_gain_varying);  /* r : 0.01, 3 Hz */
    }
    else
    {
      	;
    }           

}
#else
void LSESP_vCompWheelSlip(void)
{
    slip_m_fl_old = slip_m_fl;
    slip_m_fr_old = slip_m_fr;
    slip_m_rl_old = slip_m_rl;
    slip_m_rr_old = slip_m_rr;
    
    if(ESP_ROUGH_ROAD==1) 
    {
        slip_filter_gain = ESP_SLIP_FILTER_GAIN_ROUGH;
    } 
    else if(ESP_PULSE_DUMP_FLAG==1) 
    {
        slip_filter_gain=L_U8FILTER_GAIN_10MSLOOP_12_5HZ;
    }
    else 
    {
        slip_filter_gain = ESP_SLIP_FILTER_GAIN;
    }           

    if ( wvref_fl > VREF_2_KPH ) 
    {
        
        #if __WL_SPEED_RESOL_CHANGE
        tempW2 = (FL.vrad_crt_resol_change-FL.wvref_resol_change);
        tempW1 = 10000; 
        if ( tempW2 > 0 ) 
        {
        	tempW0 = FL.vrad_crt_resol_change;
        }
        else 
        {
        	tempW0 = FL.wvref_resol_change; 
        }       
        #else
        tempW2 = (vrad_crt_fl-wvref_fl); 
        tempW1 = 10000; 
        if ( tempW2 > 0 ) 
        {
        	tempW0 = vrad_crt_fl;
        }
        else 
        {
        	tempW0 = wvref_fl;
        }
        #endif
        s16muls16divs16();
        
        if ( tempW3 > 10000 ) 
        {
        	tempW3 = 10000;
        }
        else if ( tempW3 < -10000 ) 
        {
        	tempW3 = -10000;
        }
        else
        {
        	;
        }
        esp_tempW2 = tempW3;    /* r : 0.01 */

        if ( (ABS_fz==1) && (esp_tempW2<(-ESP_SLIP_FILTER_CHANGE_SLIP)))
        {
            slip_filter_gain_varying = ESP_SLIP_FILTER_GAIN_DEEP_SLIP_ABS;
        }
        else if ( esp_tempW2<(-ESP_SLIP_FILTER_CHANGE_SLIP))
        {
            slip_filter_gain_varying = ESP_SLIP_FILTER_GAIN_DEEP_SLIP;  
        }
        else 
        {
            slip_filter_gain_varying = slip_filter_gain;       
        }             
        
        slip_m_fl = LCESP_s16Lpf1Int(esp_tempW2, slip_m_fl_old, (uint8_t)slip_filter_gain_varying);  /* r : 0.01, 3 Hz */
    }
    else
    {
      	;
    }    
    
    if ( wvref_fr > VREF_2_KPH ) 
    {
        #if __WL_SPEED_RESOL_CHANGE
        tempW2 = (FR.vrad_crt_resol_change-FR.wvref_resol_change);
        tempW1 = 10000; 
        if ( tempW2 > 0 ) 
        {
        	tempW0 = FR.vrad_crt_resol_change;
        }
        else 
        {
        	tempW0 = FR.wvref_resol_change;
        }
        #else
        tempW2 = (vrad_crt_fr-wvref_fr); 
        tempW1 = 10000; 
        if ( tempW2 > 0 ) 
        {
        	tempW0 = vrad_crt_fr;
        }
        else 
        {
        	tempW0 = wvref_fr;
        }
        #endif
        
        s16muls16divs16();
        if ( tempW3 > 10000 ) 
        {
        	tempW3 = 10000;
        }
        else if ( tempW3 < -10000 ) 
        {
        	tempW3 = -10000;
        }
        else
        {
        	;
        }
        esp_tempW2 = tempW3;    /* r : 0.01 %*/

        if ((ABS_fz==1) && (esp_tempW2<(-ESP_SLIP_FILTER_CHANGE_SLIP)))
        {
            slip_filter_gain_varying = ESP_SLIP_FILTER_GAIN_DEEP_SLIP_ABS;
        }
        else if ( esp_tempW2<(-ESP_SLIP_FILTER_CHANGE_SLIP))
        {
            slip_filter_gain_varying = ESP_SLIP_FILTER_GAIN_DEEP_SLIP;  
        }
        else
        { 
            slip_filter_gain_varying = slip_filter_gain;    
        }                

        slip_m_fr = LCESP_s16Lpf1Int(esp_tempW2, slip_m_fr_old, (uint8_t)slip_filter_gain_varying);  /* r : 0.01, 3 Hz  */    
    }
    else
    {
       	;
    }        


    if ( wvref_rl > VREF_2_KPH ) 
    {
        #if __WL_SPEED_RESOL_CHANGE
        tempW2 = (RL.vrad_crt_resol_change-RL.wvref_resol_change); 
        tempW1 = 10000; 
        if ( tempW2 > 0 ) 
        {
        	tempW0 = RL.vrad_crt_resol_change;
        }
        else 
        {
        	tempW0 = RL.wvref_resol_change;      
        }
        #else
        tempW2 = (vrad_crt_rl-wvref_rl); 
        tempW1 = 10000; 
        if ( tempW2 > 0 ) 
        {
        	tempW0 = vrad_crt_rl;
        }
        else 
        {
        	tempW0 = wvref_rl;
        }
        #endif
        s16muls16divs16();
        if ( tempW3 > 10000 ) 
        {
        	tempW3 = 10000;
        }
        else if ( tempW3 < -10000 ) 
        {
        	tempW3 = -10000;
        }
        else
        {
        	;
        }
        esp_tempW2 = tempW3;    /* r : 0.01 */

        if ( (ABS_fz==1) && (esp_tempW2<(-ESP_SLIP_FILTER_CHANGE_SLIP)))
        {
            slip_filter_gain_varying = ESP_SLIP_FILTER_GAIN_DEEP_SLIP_ABS;
        }
        else if ( esp_tempW2<(-ESP_SLIP_FILTER_CHANGE_SLIP))
        {
            slip_filter_gain_varying = ESP_SLIP_FILTER_GAIN_DEEP_SLIP;  
        }
        else
        { 
            slip_filter_gain_varying = slip_filter_gain;               
        }     

        slip_m_rl = LCESP_s16Lpf1Int(esp_tempW2, slip_m_rl_old, (uint8_t)slip_filter_gain_varying);  /*  r : 0.01, 3 Hz */   
    }
    else
    {
        ;
    }        

    if ( wvref_rr > VREF_2_KPH ) 
    {
        #if __WL_SPEED_RESOL_CHANGE
        tempW2 = (RR.vrad_crt_resol_change-RR.wvref_resol_change); 
        tempW1 = 10000; 
        if ( tempW2 > 0 ) 
        {
        	tempW0 = RR.vrad_crt_resol_change;
        }
        else 
        {
        	tempW0 = RR.wvref_resol_change;
        }
        #else
        tempW2 = (vrad_crt_rr-wvref_rr);
        tempW1 = 10000; 
        if ( tempW2 > 0 ) 
        {
        	tempW0 = vrad_crt_rr;
        }
        else 
        {
        	tempW0 = wvref_rr;
        }
        #endif
        
        s16muls16divs16();
        if ( tempW3 > 10000 )
        {
        	tempW3 = 10000;
        }
        else if ( tempW3 < -10000 ) 
        {
        	tempW3 = -10000;
        }
        else
        {
        	;
        }
        esp_tempW2 = tempW3;    /* r : 0.01 %*/

        if ( (ABS_fz==1) && (esp_tempW2<(-ESP_SLIP_FILTER_CHANGE_SLIP)))
        {
            slip_filter_gain_varying = ESP_SLIP_FILTER_GAIN_DEEP_SLIP_ABS;
        }
        else if ( esp_tempW2<(-ESP_SLIP_FILTER_CHANGE_SLIP))
        {
            slip_filter_gain_varying = ESP_SLIP_FILTER_GAIN_DEEP_SLIP;  
        }
        else 
        {
            slip_filter_gain_varying = slip_filter_gain;     
        }   
        slip_m_rr = LCESP_s16Lpf1Int(esp_tempW2, slip_m_rr_old, (uint8_t)slip_filter_gain_varying);  /*r : 0.01, 3 Hz  */
    } 
    else
    {
      	;
    }           

}
#endif

#if (ESC_WHEEL_STRUCTURE_ENABLE == 1)
void LSESP_vCalWheelSpeedrate(struct ESC_STRUCT *WL_ESC)
{

  if(ESP_ROUGH_ROAD==1) 
  {
      esp_tempW9= ESP_SLIP_FILTER_GAIN_ROUGH;		/* 17 */
  } 
  else 
  {
      esp_tempW9 = ESP_SLIP_FILTER_GAIN;			/*  17  */
  }           


	if ( (ABS_fz==1) && (WL_ESC->slip_m<(-ESP_SLIP_FILTER_CHANGE_SLIP)))
	{
	    esp_tempW1 = ESP_SLIP_FILTER_GAIN_DEEP_SLIP_ABS;
	}
	else if ( WL_ESC->slip_m<(-ESP_SLIP_FILTER_CHANGE_SLIP))
	{
	    esp_tempW1 = ESP_SLIP_FILTER_GAIN_DEEP_SLIP;  				/* 64  */
	}
	else 
	{
	    esp_tempW1 = esp_tempW9;       
	}             
	
	WL_ESC->lcescs16CalWheelSpeedRateold = WL_ESC->lcescs16CalWheelSpeedRate;
	WL_ESC->lcescs16CalWheelSpeedRatear[3] = WL_ESC->lcescs16CalWheelSpeedRatear[2];
	WL_ESC->lcescs16CalWheelSpeedRatear[2] = WL_ESC->lcescs16CalWheelSpeedRatear[1];
	WL_ESC->lcescs16CalWheelSpeedRatear[1] = WL_ESC->lcescs16CalWheelSpeedRatear[0];
	
  #if __WL_SPEED_RESOL_CHANGE
  tempW2 = WL_ESC->vrad_crt_resol_change; tempW1 = 10; tempW0 = 64;   /*  r = 0.1 kph */
  #else
  tempW2 = WL_ESC->vrad_crt; tempW1 = 10; tempW0 = 8;
  #endif
	s16muls16divs16();
	
	WL_ESC->lcescs16CalWheelSpeedRatear[0] = tempW3; 

	esp_tempW0 = WL_ESC->lcescs16CalWheelSpeedRatear[0] - WL_ESC->lcescs16CalWheelSpeedRatear[3];
    tempW2 = esp_tempW0; tempW1 = 1000; tempW0 = 15; 
	s16muls16divs16();
 	WL_ESC->lcescs16CalWheelSpeedRate = tempW3;
  	
	WL_ESC->lcescs16CalWheelSpeedRate = LCESP_s16Lpf1Int(WL_ESC->lcescs16CalWheelSpeedRate, WL_ESC->lcescs16CalWheelSpeedRateold, esp_tempW1);  /* r : 0.01, 3 Hz */ /* 08.02.10 SWD YJH */
	
	WL_ESC->lcescs16CalDelWheelSpeedRate = WL_ESC->lcescs16CalWheelSpeedRate - WL_ESC->lcescs16CalWheelSpeedRateold;
 			
}
#else
void LSESP_vCalWheelSpeedrate(void)
{

  if(ESP_ROUGH_ROAD==1) 
  {
      esp_tempW9= ESP_SLIP_FILTER_GAIN_ROUGH;		/* 17 */
  } 
  else 
  {
      esp_tempW9 = ESP_SLIP_FILTER_GAIN;			/*  17  */
  }           

/*******************************FL*************************************************************/
  /**********  temp   ************/
	if ( (ABS_fz==1) && (slip_m_fl<(-ESP_SLIP_FILTER_CHANGE_SLIP)))
	{
	    esp_tempW1 = ESP_SLIP_FILTER_GAIN_DEEP_SLIP_ABS;
	}
	else if ( slip_m_fl<(-ESP_SLIP_FILTER_CHANGE_SLIP))
	{
	    esp_tempW1 = ESP_SLIP_FILTER_GAIN_DEEP_SLIP;  				/* 64  */
	}
	else 
	{
	    esp_tempW1 = esp_tempW9;       
	}             
	
	lcescs16CalWheelSpeedRateflold = lcescs16CalWheelSpeedRatefl;
	lcescs16CalWheelSpeedRateflar[3] = lcescs16CalWheelSpeedRateflar[2];
	lcescs16CalWheelSpeedRateflar[2] = lcescs16CalWheelSpeedRateflar[1];
	lcescs16CalWheelSpeedRateflar[1] = lcescs16CalWheelSpeedRateflar[0];
	
  #if __WL_SPEED_RESOL_CHANGE
  tempW2 = FL.vrad_crt_resol_change; tempW1 = 10; tempW0 = 64;   /*  r = 0.1 kph */
  #else
  tempW2 = vrad_crt_fl; tempW1 = 10; tempW0 = 8;
  #endif
	s16muls16divs16();
	
	lcescs16CalWheelSpeedRateflar[0] = tempW3; 

	esp_tempW0 = lcescs16CalWheelSpeedRateflar[0] - lcescs16CalWheelSpeedRateflar[3];
    tempW2 = esp_tempW0; tempW1 = 1000; tempW0 = 15; 
	s16muls16divs16();
 	lcescs16CalWheelSpeedRatefl = tempW3;
  	
	lcescs16CalWheelSpeedRatefl = LCESP_s16Lpf1Int(lcescs16CalWheelSpeedRatefl, lcescs16CalWheelSpeedRateflold, esp_tempW1);  /* r : 0.01, 3 Hz */ /* 08.02.10 SWD YJH */
	
	lcescs16CalDelWheelSpeedRatefl = lcescs16CalWheelSpeedRatefl - lcescs16CalWheelSpeedRateflold;

/*******************************FR*************************************************************/
  /*  temp   */
	if ( (ABS_fz==1) && (slip_m_fr<(-ESP_SLIP_FILTER_CHANGE_SLIP)))
	{
	    esp_tempW1 = ESP_SLIP_FILTER_GAIN_DEEP_SLIP_ABS;
	}
	else if ( slip_m_fr<(-ESP_SLIP_FILTER_CHANGE_SLIP))
	{
	    esp_tempW1 = ESP_SLIP_FILTER_GAIN_DEEP_SLIP;  				/* 64  */
	}
	else 
	{
	    esp_tempW1 = esp_tempW9;       
	}             
	      
	lcescs16CalWheelSpeedRatefrold = lcescs16CalWheelSpeedRatefr;
	lcescs16CalWheelSpeedRatefrar[3] = lcescs16CalWheelSpeedRatefrar[2];
	lcescs16CalWheelSpeedRatefrar[2] = lcescs16CalWheelSpeedRatefrar[1];
	lcescs16CalWheelSpeedRatefrar[1] = lcescs16CalWheelSpeedRatefrar[0];	
	
    #if __WL_SPEED_RESOL_CHANGE
    tempW2 = FR.vrad_crt_resol_change; tempW1 = 10; tempW0 = 64;//*tire_radius;         
    #else
    tempW2 = vrad_crt_fr; tempW1 = 10; tempW0 = 8;//*tire_radius; 
    #endif
	s16muls16divs16();
	lcescs16CalWheelSpeedRatefrar[0] = tempW3; 
	
	esp_tempW0 = lcescs16CalWheelSpeedRatefrar[0] - lcescs16CalWheelSpeedRatefrar[3];
	
  tempW2 = esp_tempW0; tempW1 = 1000; tempW0 = 15; 
	s16muls16divs16();
 	lcescs16CalWheelSpeedRatefr = tempW3; 	
 	esp_k7 = lcescs16CalWheelSpeedRatefr;
	lcescs16CalWheelSpeedRatefr = LCESP_s16Lpf1Int(lcescs16CalWheelSpeedRatefr, lcescs16CalWheelSpeedRatefrold, esp_tempW1);  /* r : 0.01, 3 Hz */ /* 08.02.10 SWD YJH */

	lcescs16CalDelWheelSpeedRatefr = lcescs16CalWheelSpeedRatefr - lcescs16CalWheelSpeedRatefrold;
	
/*******************************RL*************************************************************/
	if ( (ABS_fz==1) && (slip_m_rl<(-ESP_SLIP_FILTER_CHANGE_SLIP)))
	{
	    esp_tempW1 = ESP_SLIP_FILTER_GAIN_DEEP_SLIP_ABS;
	}
	else if ( slip_m_rl<(-ESP_SLIP_FILTER_CHANGE_SLIP))
	{
	    esp_tempW1 = ESP_SLIP_FILTER_GAIN_DEEP_SLIP;  				/* 64  */
	}
	else 
	{
	    esp_tempW1 = esp_tempW9;       
	}             
	
	lcescs16CalWheelSpeedRaterlold = lcescs16CalWheelSpeedRaterl;
	lcescs16CalWheelSpeedRaterlar[3] = lcescs16CalWheelSpeedRaterlar[2];
	lcescs16CalWheelSpeedRaterlar[2] = lcescs16CalWheelSpeedRaterlar[1];
	lcescs16CalWheelSpeedRaterlar[1] = lcescs16CalWheelSpeedRaterlar[0];
	    
    #if __WL_SPEED_RESOL_CHANGE
    tempW2 = RL.vrad_crt_resol_change; tempW1 = 10; tempW0 = 64;
    #else
    tempW2 = vrad_crt_rl; tempW1 = 10; tempW0 = 8;
    #endif
	s16muls16divs16();
	lcescs16CalWheelSpeedRaterlar[0] = tempW3; 
	esp_k8 = tempW3;
	
	esp_tempW0 = lcescs16CalWheelSpeedRaterlar[0] - lcescs16CalWheelSpeedRaterlar[3];
    tempW2 = esp_tempW0; tempW1 = 1000; tempW0 = 15; 
	s16muls16divs16();
	esp_k9 = tempW3;
	
 	lcescs16CalWheelSpeedRaterl = tempW3;
 	
 	lcescs16CalWheelSpeedRaterl = LCESP_s16Lpf1Int(lcescs16CalWheelSpeedRaterl, lcescs16CalWheelSpeedRaterlold, esp_tempW1);  /* r : 0.01, 3 Hz */ /* 08.02.10 SWD YJH */

	lcescs16CalDelWheelSpeedRaterl = lcescs16CalWheelSpeedRaterl - lcescs16CalWheelSpeedRaterlold;
	
	
/*******************************RR*************************************************************/
	if ( (ABS_fz==1) && (slip_m_rr<(-ESP_SLIP_FILTER_CHANGE_SLIP)))
	{
	    esp_tempW1 = ESP_SLIP_FILTER_GAIN_DEEP_SLIP_ABS;
	}
	else if ( slip_m_rr<(-ESP_SLIP_FILTER_CHANGE_SLIP))
	{
	    esp_tempW1 = ESP_SLIP_FILTER_GAIN_DEEP_SLIP;  				/* 64  */
	}
	else 
	{
	    esp_tempW1 = esp_tempW9;       
	}             
	
	lcescs16CalWheelSpeedRaterrold = lcescs16CalWheelSpeedRaterr;
	lcescs16CalWheelSpeedRaterrar[3] = lcescs16CalWheelSpeedRaterrar[2];
	lcescs16CalWheelSpeedRaterrar[2] = lcescs16CalWheelSpeedRaterrar[1];
	lcescs16CalWheelSpeedRaterrar[1] = lcescs16CalWheelSpeedRaterrar[0];


    #if __WL_SPEED_RESOL_CHANGE
    tempW2 = RR.vrad_crt_resol_change; tempW1 = 10; tempW0 = 64;         
    #else
    tempW2 = vrad_crt_rr; tempW1 = 10; tempW0 = 8; 
    #endif
	s16muls16divs16();
	lcescs16CalWheelSpeedRaterrar[0] = tempW3; 

	esp_tempW0 = lcescs16CalWheelSpeedRaterrar[0] - lcescs16CalWheelSpeedRaterrar[3];
    tempW2 = esp_tempW0; tempW1 = 1000; tempW0 = 15; 
	s16muls16divs16();	
 	lcescs16CalWheelSpeedRaterr = tempW3;	
 	
 	lcescs16CalWheelSpeedRaterr = LCESP_s16Lpf1Int(lcescs16CalWheelSpeedRaterr, lcescs16CalWheelSpeedRaterrold, esp_tempW1);  /* r : 0.01, 3 Hz */ /* 08.02.10 SWD YJH */

	lcescs16CalDelWheelSpeedRaterr = lcescs16CalWheelSpeedRaterr - lcescs16CalWheelSpeedRaterrold;	
/* ====================================================================== */

	tempW2 = arad_fl; tempW1 = 100; tempW0 = 4; 
	s16muls16divs16();	
 	tempW2 = tempW3; tempW1 = 100; tempW0 = tire_radius; 
	s16muls16divs16();	
 	esp_tempW0 = tempW3;
 			
}
#endif

#endif
/* ====================================================================== */


#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LSESPCalSlipRatio
	#include "Mdyn_autosar.h"
#endif
