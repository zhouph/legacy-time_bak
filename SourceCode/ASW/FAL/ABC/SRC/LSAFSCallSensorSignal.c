
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_CL_LSAFSCallSensorSignal
	#include "Mdyn_autosar.h"
#endif

/* Includes ********************************************************/
#include "LVarHead.h"
/* Local Definition  ***********************************************/


/* Variables Definition*********************************************/


/* Local Function prototype ****************************************/

  #if defined(__UCC_1)
	void LSAFS_vCallSensorSignal(void);
	void STR_PRE_PROCESSOING();
  #endif

/* Implementation***************************************************/
  #if defined(__UCC_1)

void LSAFS_vCallSensorSignal(void)
{
			    
	STR_PRE_PROCESSOING();

}

void STR_PRE_PROCESSOING(void)
{
    
}   
  #endif

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LSAFSCallSensorSignal
	#include "Mdyn_autosar.h"
#endif
