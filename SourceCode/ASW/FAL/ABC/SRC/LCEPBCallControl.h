#ifndef __LCEPBCALLCONTROL_H__
#define __LCEPBCALLCONTROL_H__

/*includes**************************************************/
#include "LVarHead.h"

/*Global MACRO CONSTANT Definition******************************************************/
  #if __EPB_INTERFACE
#define     S8_EPB_STANDBY              1
#define     S8_EPB_ACTIVATION           2
#define     S8_EPB_TERMINATION          3

/* define EPB state */
#define EPBI_EPB_UKNOWN             0 
#define EPBI_EPB_RELEASED           1
#define EPBI_EPB_CLAMPING           2
#define EPBI_EPB_CLAMPED            3
#define EPBI_EPB_RELEASING          4
#define EPBI_EPB_DYNAMIC_BRK        5

  #if (__CAR_MAKER==HMC_KMC)
#define	EPBI_EPB_RELEASED_STATE	     1
#define	EPBI_EPB_CLAMPED_STATE	     2
#define	EPBI_EPB_RELEASING_STATE     4
#define	EPBI_EPB_CLAMPING_STATE	     3
#define	EPBI_EPB_DYNAMIC_BRK_STATE   5
  #else /* other car maker */
#define	EPBI_EPB_RELEASED_STATE	     1
#define	EPBI_EPB_CLAMPED_STATE	     2
#define	EPBI_EPB_RELEASING_STATE     4
#define	EPBI_EPB_CLAMPING_STATE	     3
#define	EPBI_EPB_DYNAMIC_BRK_STATE   5
  #endif

  #endif

/*Global MACRO FUNCTION Definition******************************************************/
#define     __EPB_TEST_MODE_ENABLE        0
  
#define	    __EPB_FF_Ctrl_Enable	                  1

#define     __DISABLE_DITHER_EPBI                     1 



/* Ʃ�� �Ķ���� */ 
#define     U8_EPBI_ACT_ENABLE_IGN_OFF                 0

/*
#define     S16_EPBI_TARGET_DECEL                    -50
#define     S16_EPBI_FORCED_HOLD_SPD                 VREF_3_KPH
#define     S16_EPBI_INITIAL_CURRENT                 550
#define     S16_EPBI_MAX_CURRENT                     800
*/
/* Ʃ�� �Ķ���� */ 

/*Global Type Declaration *******************************************************************/

/*Global Extern Variable  Declaration*******************************************************************/
  #if __EPB_INTERFACE || __AVH
extern struct  U8_BIT_STRUCT_t EPB00;
extern struct  U8_BIT_STRUCT_t EPB01;

#define lcu1epbRearBrakeLight			EPB00.bit0
#define lcu1Epb_not_used_bit_00_1       EPB00.bit1
#define lcu1epbAckESPReq				EPB00.bit2
#define lcu1Epb_not_used_bit_00_3       EPB00.bit3
#define lcu1Epb_not_used_bit_00_4		EPB00.bit4
#define lcu1Epb_not_used_bit_00_5		EPB00.bit5
#define lcu1Epb_not_used_bit_00_6		EPB00.bit6
#define lcu1Epb_not_used_bit_00_7		EPB00.bit7

#define lcu1EpbActiveFlg				EPB01.bit0
#define lcu1EpbInhibitFlg				EPB01.bit1
#define lcu1EpbiBrkLampOnReq    		EPB01.bit2
#define lcu1EpbiDitherDisable      		EPB01.bit3
#define lcu1Epb_not_used_bit_01_4		EPB01.bit4
#define lcu1Epb_not_used_bit_01_5		EPB01.bit5
#define lcu1Epb_not_used_bit_01_6		EPB01.bit6
#define lcu1Epb_not_used_bit_01_7		EPB01.bit7

/********** To */
 

extern int8_t   	lcs8EpbState;
extern int16_t   	lcs16EpbTargetG;
extern uint8_t      lcu8EpbCtrlReq;

extern uint8_t 		lcu8epbInfoLamp;
extern uint8_t 		lcu8epbFailureLamp;
extern uint8_t 		lcu8epbAudioOutput;
extern uint8_t 		lcu8epbOutputCluster;
extern uint8_t 		lcu8epbStatusCtrlSW;
extern uint8_t 		lcu8epbStatusEPB;
extern uint8_t 		lcu8epbStatusError;
extern uint8_t 		lcu8epbReqDecel;
extern uint8_t      lcu8epbDynamicReq;
extern uint8_t      lcu8epbFail;
extern uint8_t      lcu8epbLdmstate;
extern int16_t		lcs16epbActFroce;
extern uint8_t      lcu8EpbCtrlReq, lcu8EpbFailureDar;
extern uint16_t     clu16EpbObserver;
extern uint8_t      lcu8EpbiEpbState, lcu8EpbiDiableDitherCnt;
extern uint8_t      lcu8EpbVrefCnt; 
extern int16_t      laepbs16TcTargetPress;


/********** From */
   #if __CAR==GM_DAT_MAGNUS
extern uint8_t  ccu8_EPB510_BrakeRequest, ccu8_EPB510_FailureDAR,;
extern struct   CC_EPB5_FLG_t  EPB5;
   #endif
  #endif /* (__EPB_INTERFACE) || __AVH */

/*Global Extern Functions  Declaration******************************************************/
  #if __EPB_INTERFACE
extern void LCEPB_vCallControl(void);
extern void LCEPB_vResetCtrlVariable(void);
  #endif
#endif