#ifndef __LSCALLMAIN_H__
#define __LSCALLMAIN_H__
/*Includes *********************************************************************/
#include "LVarHead.h"

/*Global MACRO CONSTANT Definition *********************************************/

/*Global MACRO FUNCTION Definition *********************************************/

/*Global Type Declaration ******************************************************/

typedef struct
{
    uint16_t LS_BIT_07     :1;
    uint16_t LS_BIT_06     :1;
    uint16_t LS_BIT_05     :1;
    uint16_t LS_BIT_04     :1;
    uint16_t LS_BIT_03     :1;
    uint16_t LS_BIT_02     :1;
    uint16_t LS_BIT_01     :1;
    uint16_t LS_BIT_00     :1;
}LS_FLAG_t;

#define lsu1EngCalLoaded        lsMainFlag1.LS_BIT_07

/*Global Extern Variable Declaration *******************************************/

extern LS_FLAG_t lsMainFlag1;

extern uint8_t lsu8LoadedDrvModeOld;
extern uint8_t Ivss_ModeESCOld;
extern uint8_t lstcsu8LoadEngCalCheckCnt;
extern uint8_t lstcsu8LoadEngCalForChanVarCnt;

/*Global Extern Functions Declaration ******************************************/
extern void LS_vCallMainSensorSignal(void);

#if __VDC
extern void	LSESP_vCallSensorSignal(void);
extern void	LSPBA_vCallSensorSignal(void);
#endif

#if defined(__CDC)
//extern void	LS_CalcFaultInput(void);
extern void	LSCDC_vCallSensorSignal(void);
extern void	LS_CalcSportModeInput(void);
#endif

#if __VSM
extern void LSEPS_vCallSensorSignal(void);                            
#endif

#endif
