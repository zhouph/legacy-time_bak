#ifndef __LDABSDCTWHEELSTATUS_H__
#define __LDABSDCTWHEELSTATUS_H__

/*includes********************************************************************/
#include "LVarHead.h"


/*Global Type Declaration ****************************************************/
#if __PRESS_EST_ABS
#define     DumpSetLimitFrt     3
#define     DumpSetLimitRr      3
#define     DumpLowMuCount      5
#define     IndexHighMu         600
#define     IndexMidMu          400
  #if (__CAR==HMC_PA) || (__CAR==KMC_SA)
#define     IndexBasalt         70
#define     IndexCeramic        40
  #else
#define     IndexBasalt         60
#define     IndexCeramic        30
  #endif
#endif

/* C615_MGH80_BKT_JJJ */
#if (__VDC && __PRESS_EST_ACTIVE)

#define     __WPE_VOLUME_MODEL              ENABLE  /*  ENABLE or DISABLE   */

#else

#define     __WPE_VOLUME_MODEL              DISABLE /*  ENABLE or DISABLE   */

#endif 

/*Global Extern Variable  Declaration*****************************************/
extern  U8_BIT_STRUCT_t WPE00;

#if __ECU==ESP_ECU_1
 
#endif

extern int16_t 	Tc_under_pressure_s,Tc_under_pressure_p;

extern int16_t Primary_TC_hold_press,Secondary_TC_hold_press;

/* 2012_SWD_KJS */
extern int16_t    ldabss16BrakeFluidVolume_P;
extern int16_t    ldabss16BrakeFluidVolume_S;
extern int16_t    ldabss16MotorRPM_NEW;
extern uint16_t   ldabsu16DeltaVolume;
extern uint16_t   ldabsu16PistonArea;
extern int16_t    ldabss16MotorEfficiency_P;
extern int16_t    ldabss16MotorEfficiency_S;
/* 2012_SWD_KJS */

/* swd12_RPM_kjs */
extern uint16_t ldabsu16WPEMotorVolt;
extern uint16_t ldabsu16WPEMotorVolt_Old;
/* swd12_RPM_kjs */

/* 2012_SWD_KJS_TCMF */
extern int16_t ldabss16WPETCvvHoldPress_Pri;
extern int16_t ldabss16WPETCvvHoldPress_Scn;
/* 2012_SWD_KJS_TCMF */

extern int16_t ldabss16WPEMpress_1_100bar;

#if  ((__AHB_GEN3_SYSTEM == ENABLE) || (__IDB_LOGIC == ENABLE))
extern int16_t ldabss16WPECircuitPress_Pri;
extern int16_t ldabss16WPECircuitPress_Scn;
#endif


#if __ECU==ESP_ECU_1
 
#endif

#define EST_WHEEL_PRESS_OK_FLG           	WPE00.bit0
/* SWD11_KJS */
#define ldabsu1EscPulseUpRiseMode         WPE00.bit1
/* SWD11_KJS */
/* 2012_SWD_KJS */
#define ldabsu1CircuitTCvalveAct          WPE00.bit2
/* 2012_SWD_KJS */
/* Straight Control Compensation (Alat_TCS_HDC) */
#define ldabsu1WPEStraightControl         WPE00.bit3
/* Straight Control Compensation (Alat_TCS_HDC) */
#define not_used_bit_WPE_00_4               WPE00.bit4
#define not_used_bit_WPE_00_5               WPE00.bit5
#define not_used_bit_WPE_00_6               WPE00.bit6
#define not_used_bit_WPE_00_7               WPE00.bit7


/*Global Extern Functions  Declaration****************************************/
extern void LDABS_vDctWheelStatus(void);
extern uint16_t LDABS_u16FitSQRT(uint16_t tempW12);


  #if __CAR == HMC_HM
#define	Press_Dec_gain_Front_below_5bar	    2
#define	Press_Dec_gain_Front_5_10bar	 	6
#define	Press_Dec_gain_Front_10_70bar	 	11
#define	Press_Dec_gain_Front_Above_70bar 	14

#define	Press_Dec_gain_Rear_below_5bar	 	2
#define	Press_Dec_gain_Rear_5_10bar		 	5
#define	Press_Dec_gain_Rear_10_70bar	 	9
#define	Press_Dec_gain_Rear_Above_70bar	 	10
  #else
#define	Press_Dec_gain_Front_below_5bar	    2
#define	Press_Dec_gain_Front_5_10bar	 	6
#define	Press_Dec_gain_Front_10_70bar	 	11
#define	Press_Dec_gain_Front_Above_70bar 	14

#define	Press_Dec_gain_Rear_below_5bar	 	2
#define	Press_Dec_gain_Rear_5_10bar		 	6
#define	Press_Dec_gain_Rear_10_70bar	 	12
#define	Press_Dec_gain_Rear_Above_70bar	 	14
  #endif

#endif


