/******************************************************************************
* Project Name: CORNER BRAKE CONTROL
* File: LDCBCCallActHW.C
* Date: April. 17. 2006
******************************************************************************/

/* Includes ******************************************************************/

/* AUTOSAR --------------------------*/

#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START                       
	#define idx_FILE	idx_CL_LACBCCallActHW
	#include "Mdyn_autosar.h"               
#endif
/* AUTOSAR --------------------------*/

#include "LCCBCCallControl.h"
#include "LACBCCallActHw.h"
#include "LACallMain.h"
#include "LCABSDecideCtrlState.h"

#if __CBC
/* Logcal Definiton  *********************************************************/

#define CBC_HV_VL_fl                        lacbcValveFL.u1CbcHvVlAct
#define CBC_AV_VL_fl                        lacbcValveFL.u1CbcAvVlAct
#define CBC_HV_VL_fr                        lacbcValveFR.u1CbcHvVlAct
#define CBC_AV_VL_fr                        lacbcValveFR.u1CbcAvVlAct
#define CBC_HV_VL_rl                        lacbcValveRL.u1CbcHvVlAct
#define CBC_AV_VL_rl                        lacbcValveRL.u1CbcAvVlAct
#define CBC_HV_VL_rr                        lacbcValveRR.u1CbcHvVlAct
#define CBC_AV_VL_rr                        lacbcValveRR.u1CbcAvVlAct


#define U8_CBC_REAR_CTRL                    1
#define U8_CBC_FRONT_CTRL                   0


/* Variables Definition*******************************************************/

LACBC_VALVE_t lacbcValveFL, lacbcValveFR, lacbcValveRL, lacbcValveRR;

WHEEL_VV_OUTPUT_t lacbc_FL1HP, lacbc_FL2HP, lacbc_FR1HP, lacbc_FR2HP;
WHEEL_VV_OUTPUT_t lacbc_RL1HP, lacbc_RL2HP, lacbc_RR1HP, lacbc_RR2HP;

/* LocalFunction prototype ***************************************************/
void LACBC_vCallActHW(void);
void LACBC_vCallActValve(void);

static void LACBC_vDecideValveOutput(CBC_WHL_CTRL_t *pCbcWL, int8_t s8DumpHoldCnt, int8_t s8RiseHoldCnt,  
                                     LACBC_VALVE_t *laCbcValve, WHEEL_VV_OUTPUT_t *laWL1HP,  WHEEL_VV_OUTPUT_t *laWL2HP, uint8_t u8CtrlAxle);



/* Implementation*************************************************************/



/******************************************************************************
* FUNCTION NAME:      LACBC_vCallActHW
* CALLED BY:          LA_vCallMainValve()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Valve/Motor actuaction by CORNER BRAKE CONTROL
******************************************************************************/
void LACBC_vCallActHW(void)
{
   #if __FRONT_CBC_ENABLE
    if ((CBC_ACTIVE_fl==1)||(CBC_FADE_OUT_fl==1))
    {
	    LACBC_vDecideValveOutput(&lccbcFL, lccbcs8DumpHoldCntFront, lccbcs8RiseHoldCntFront, 
	                             &lacbcValveFL, &lacbc_FL1HP, &lacbc_FL2HP, U8_CBC_FRONT_CTRL);
	}
	else
	{
		CBC_HV_VL_fl = 0;
		CBC_AV_VL_fl = 0;
		LA_vResetWheelValvePwmDuty(&lacbc_FL1HP);
		LA_vResetWheelValvePwmDuty(&lacbc_FL2HP);
	}
	
    if ((CBC_ACTIVE_fr==1)||(CBC_FADE_OUT_fr==1))
    {
	    LACBC_vDecideValveOutput(&lccbcFR, lccbcs8DumpHoldCntFront, lccbcs8RiseHoldCntFront, 
	                             &lacbcValveFR, &lacbc_FR1HP, &lacbc_FR2HP, U8_CBC_FRONT_CTRL);
	}
	else
	{
		CBC_HV_VL_fr = 0;
		CBC_AV_VL_fr = 0;
		LA_vResetWheelValvePwmDuty(&lacbc_FR1HP);
		LA_vResetWheelValvePwmDuty(&lacbc_FR2HP);
	}
		
   #endif
   
    if ((CBC_ACTIVE_rl==1)||(CBC_FADE_OUT_rl==1))
    {
	    LACBC_vDecideValveOutput(&lccbcRL, lccbcs8DumpHoldCnt, lccbcs8RiseHoldCnt, 
	                             &lacbcValveRL, &lacbc_RL1HP, &lacbc_RL2HP, U8_CBC_REAR_CTRL);
	}
	else
	{
		CBC_HV_VL_rl = 0;
		CBC_AV_VL_rl = 0;
		LA_vResetWheelValvePwmDuty(&lacbc_RL1HP);
		LA_vResetWheelValvePwmDuty(&lacbc_RL2HP);
	}
    
    if ((CBC_ACTIVE_rr==1)||(CBC_FADE_OUT_rr==1))
    {
	    LACBC_vDecideValveOutput(&lccbcRR, lccbcs8DumpHoldCnt, lccbcs8RiseHoldCnt, 
	                             &lacbcValveRR, &lacbc_RR1HP, &lacbc_RR2HP, U8_CBC_REAR_CTRL);
	}
	else
	{
		CBC_HV_VL_rr = 0;
		CBC_AV_VL_rr = 0;
		LA_vResetWheelValvePwmDuty(&lacbc_RR1HP);
		LA_vResetWheelValvePwmDuty(&lacbc_RR2HP);
	} 
	
    LACBC_vCallActValve();
  #if __VDC && __CBC_FADE_OUT_LFC_RISE
    if((CBC_FADE_OUT_rl==1)||(CBC_FADE_OUT_rr==1)||(CBC_ACTIVE_rl==1)||(CBC_ACTIVE_rr==1)
    #if __FRONT_CBC_ENABLE  	
     ||(CBC_FADE_OUT_fl==1)||(CBC_FADE_OUT_fr==1)||(CBC_ACTIVE_fl==1)||(CBC_ACTIVE_fr==1)
    #endif
      )  	  
  	{
  	  	VALVE_ACT = 1;	
  	}
  #endif
}

void LACBC_vCallActValve(void)
{
  #if __FRONT_CBC_ENABLE
    if ((CBC_ACTIVE_fl==1)||(CBC_FADE_OUT_fl==1))
    {
        HV_VL_fl = CBC_HV_VL_fl;
        AV_VL_fl = CBC_AV_VL_fl;
        /* Temporary until duty determination module restructuring */
        if((HV_VL_fl==0) && (AV_VL_fl==0) && (lccbcs8FadeOutModeFL!=CBC_FADEOUT_LFCRISE))
        {
        	FL.LFC_Conven_OnOff_Flag = 1;
        }
        else
        {
        	FL.LFC_Conven_OnOff_Flag = 0;
        }
        /* Temporary until duty determination module restructuring */

		la_FL1HP = lacbc_FL1HP; 
		la_FL2HP = lacbc_FL2HP; 
    }
    else
    {
        ;
    }

    if ((CBC_ACTIVE_fr==1)||(CBC_FADE_OUT_fr==1))
    {
        HV_VL_fr = CBC_HV_VL_fr;
        AV_VL_fr = CBC_AV_VL_fr;
        /* Temporary until duty determination module restructuring */
        if((HV_VL_fr==0) && (AV_VL_fr==0) && (lccbcs8FadeOutModeFR!=CBC_FADEOUT_LFCRISE))
        {
        	FR.LFC_Conven_OnOff_Flag = 1;
        }
        else
        {
        	FR.LFC_Conven_OnOff_Flag = 0;
        }
        /* Temporary until duty determination module restructuring */

		la_FR1HP = lacbc_FR1HP; 
		la_FR2HP = lacbc_FR2HP;       
    }
    else
    {
        ;
    }
  #endif

    if (((CBC_ACTIVE_rl==1)||(CBC_FADE_OUT_rl==1))&&(RL.EBD_MODE!=DUMP_START)) /* RL.EBD_MODE!=DUMP_START */ 
    {
        HV_VL_rl = CBC_HV_VL_rl;
        AV_VL_rl = CBC_AV_VL_rl;
        /* Temporary until duty determination module restructuring */
        if((HV_VL_rl==0) && (AV_VL_rl==0) && (lccbcs8FadeOutModeRL!=CBC_FADEOUT_LFCRISE))
        {
        	RL.LFC_Conven_OnOff_Flag = 1;
        }
        else
        {
        	RL.LFC_Conven_OnOff_Flag = 0;
        }        	
        /* Temporary until duty determination module restructuring */
		la_RL1HP = lacbc_RL1HP; 
		la_RL2HP = lacbc_RL2HP;       
    }
    else
    {
        ;
    }

    if (((CBC_ACTIVE_rr==1)||(CBC_FADE_OUT_rr==1))&&(RR.EBD_MODE!=DUMP_START)) /* RR.EBD_MODE!=DUMP_START */
    {
        HV_VL_rr = CBC_HV_VL_rr;
        AV_VL_rr = CBC_AV_VL_rr;
        /* Temporary until duty determination module restructuring */
        if((HV_VL_rr==0) && (AV_VL_rr==0) && (lccbcs8FadeOutModeRR!=CBC_FADEOUT_LFCRISE))
        {
        	RR.LFC_Conven_OnOff_Flag = 1;
        }
        else
        {
        	RR.LFC_Conven_OnOff_Flag = 0;
        }        	
        /* Temporary until duty determination module restructuring */
		la_RR1HP = lacbc_RR1HP; 
		la_RR2HP = lacbc_RR2HP;       
    }
    else
    {
        ;
    }
}

static void LACBC_vDecideValveOutput(CBC_WHL_CTRL_t *pCbcWL, int8_t s8DumpHoldCnt, int8_t s8RiseHoldCnt,
                                     LACBC_VALVE_t *laCbcValve, WHEEL_VV_OUTPUT_t *laWL1HP,  WHEEL_VV_OUTPUT_t *laWL2HP, uint8_t u8CtrlAxle)
{
	int16_t s16FadeOutHoldScan;
	uint8_t u8CbcReapplyTime;
  #if __VDC && __CBC_FADE_OUT_LFC_RISE
	uint8_t u8cbcinitduty, u8cbcdutycomp;
  #endif
	if(pCbcWL->u8PresCtrlMode==U8_CBC_PRESS_DUMP)
	{		
        if (s8DumpHoldCnt==0)  /* 시점만 작동하기 */
        {
    		laCbcValve->u1CbcHvVlAct = 1;
    		laCbcValve->u1CbcAvVlAct = 1;
    		LA_vSetWheelVvOnOffDumpOutput(laWL1HP, laWL2HP, U8_CBC_DUMPSCAN_TIME);
    	}
    	else
        {
    		laCbcValve->u1CbcHvVlAct = 1;
    		laCbcValve->u1CbcAvVlAct = 0;
    		LA_vSetWheelVvOnOffHoldOutput(laWL1HP, laWL2HP);
    	}
	}
	else if(pCbcWL->u8PresCtrlMode==U8_CBC_PRESS_RISE)
	{
		if(u8CtrlAxle == U8_CBC_REAR_CTRL)
		{
		  #if __VDC
		    if(lccbcu8CbcMode == U8_ESC_CBC)
		    {
		    	u8CbcReapplyTime = (uint8_t)S8_CBC_REAPPLY_TIME_R;
		    }
		    else
		  #endif
		    {
		    	u8CbcReapplyTime = (uint8_t)S8_CBC_REAPPLY_TIME_ABS_R;
		    }
		}
		else
		{
			u8CbcReapplyTime = U8_BASE_CTRLTIME;
		}

        if (s8RiseHoldCnt==0)  /* 시점만 작동하기 */
        {
    		laCbcValve->u1CbcHvVlAct = 0;
    		laCbcValve->u1CbcAvVlAct = 0;
    		LA_vSetWheelVvOnOffRiseOutput(laWL1HP, laWL2HP, u8CbcReapplyTime);
    	}
    	else
        {
    		laCbcValve->u1CbcHvVlAct = 1;
    		laCbcValve->u1CbcAvVlAct = 0;
    		LA_vSetWheelVvOnOffHoldOutput(laWL1HP, laWL2HP);
    	}
	}
	else if(pCbcWL->u8PresCtrlMode==U8_CBC_PRESS_HOLD)
	{
		laCbcValve->u1CbcHvVlAct = 1;
		laCbcValve->u1CbcAvVlAct = 0;
		LA_vSetWheelVvOnOffHoldOutput(laWL1HP, laWL2HP);
	}
	else if(pCbcWL->u8PresCtrlMode==U8_CBC_PRESS_FADEOUT)
	{
		if(pCbcWL->s8FadeOutMode==CBC_FADEOUT_PULSEUP)
		{
	  	  #if __VDC
	  	    if(lccbcu8CbcMode == U8_ESC_CBC)  
        	{
        		s16FadeOutHoldScan = (S16_CBC_FADE_OUT_HOLD_SCAN > 0) ? S16_CBC_FADE_OUT_HOLD_SCAN : 0;
        	}
	  	    else
	  	  #endif
	    	{
	    		s16FadeOutHoldScan = (S16_CBC_FADE_OUT_HOLD_SCAN_ABS > 0) ? S16_CBC_FADE_OUT_HOLD_SCAN_ABS : 0;
	    	}
		}
		else if(pCbcWL->s8FadeOutMode==CBC_FADEOUT_FULLRISE)
		{
			s16FadeOutHoldScan = 0;
		}
		else /* U8_CBC_FADEOUT_LFCrise */
		{
			s16FadeOutHoldScan = 0;
		}
		
		if((pCbcWL->s8FadeOutMode==CBC_FADEOUT_PULSEUP)||(pCbcWL->s8FadeOutMode==CBC_FADEOUT_FULLRISE)) 
		{
			if(u8CtrlAxle == U8_CBC_REAR_CTRL)
			{
			  #if __VDC  
			    u8CbcReapplyTime = (uint8_t)S8_CBC_REAPPLY_TIME_FADEOUT_R;
			  #else
			    u8CbcReapplyTime = (uint8_t)S8_CBC_REAPPLY_TIME_FADEOUT_ABS_R;
			  #endif
			}
			else
			{
				u8CbcReapplyTime = U8_BASE_CTRLTIME;
			}
			
        	if (s16FadeOutHoldScan == 0)
        	{
        		pCbcWL->s8PressRiseCnt++;
        	    laCbcValve->u1CbcHvVlAct = 0;
        	    laCbcValve->u1CbcAvVlAct = 0;
        	    LA_vSetWheelVvOnOffRiseOutput(laWL1HP, laWL2HP, (U8_BASE_CTRLTIME*2));
        	}	        
        	else if ((pCbcWL->s16FadeOutCnt%(s16FadeOutHoldScan+1))<s16FadeOutHoldScan)
        	{
        		pCbcWL->s8PressRiseCnt=0;
        	    laCbcValve->u1CbcHvVlAct = 1;
        	    laCbcValve->u1CbcAvVlAct = 0;
	    	    LA_vSetWheelVvOnOffHoldOutput(laWL1HP, laWL2HP);
        	}
        	else
        	{
        		pCbcWL->s8PressRiseCnt=0;
        	    laCbcValve->u1CbcHvVlAct = 0;
        	    laCbcValve->u1CbcAvVlAct = 0;
        	    LA_vSetWheelVvOnOffRiseOutput(laWL1HP, laWL2HP, u8CbcReapplyTime);
        	}   		
		}
	  #if __VDC && __CBC_FADE_OUT_LFC_RISE
        else if(pCbcWL->s8FadeOutMode==CBC_FADEOUT_LFCRISE)
        {
        	pCbcWL->s8PressRiseCnt=0;
        	if(pCbcWL->s16FadeOutCnt==1)
			{
				if(pCbcWL->u1CbcRearWL==1)
				{
					u8cbcinitduty = LCABS_s16Interpolation2P(lccbcs16ReardeltaP,MPRESS_10BAR,MPRESS_100BAR,U8_Initial_Duty_Ratio_CBC_MIN,U8_Initial_Duty_Ratio_CBC_MAX );
				}
				else
				{
					u8cbcinitduty = LCABS_s16Interpolation2P(lccbcs16FrontdeltaP,MPRESS_10BAR,MPRESS_100BAR,U8_Initial_Duty_Ratio_CBC_MIN,U8_Initial_Duty_Ratio_CBC_MAX );
				}
			  	u8cbcdutycomp = LCABS_s16Interpolation2P(vref,S16_CBC_THRES_LOW_SPEED,S16_CBC_THRES_HIGH_SPEED,U8_Init_Duty_Comp_CBC_Min_vref,U8_Init_Duty_Comp_CBC_Max_vref );
				pCbcWL->u8CbcPwmDuty = u8cbcinitduty + u8cbcdutycomp;
			}
			else{}
			if(((pCbcWL->s16FadeOutCnt%(U8_Hold_Timer_New_Ref_Default))==1)&&(pCbcWL->u8CbcPwmDuty>U8_PWM_DUTY_LIMIT_CBC))
			{
				pCbcWL->u8CbcPwmDuty = pCbcWL->u8CbcPwmDuty - 1;
				laCbcValve->u1CbcHvVlAct = 0;
        	    laCbcValve->u1CbcAvVlAct = 0;
			}
			else
			{
				laCbcValve->u1CbcHvVlAct = 1;
        	    laCbcValve->u1CbcAvVlAct = 0;	
			}
		    laWL1HP->u8PwmDuty = pCbcWL->u8CbcPwmDuty;
		    laWL2HP->u8PwmDuty = pCbcWL->u8CbcPwmDuty;
			laWL1HP->u8OutletOnTime = 0;
			laWL2HP->u8OutletOnTime = 0;	
        }
      #endif
        else{}   		
	}
	else 
	{
		laCbcValve->u1CbcHvVlAct = 0;
		laCbcValve->u1CbcAvVlAct = 0;
		LA_vResetWheelValvePwmDuty(laWL1HP);
		LA_vResetWheelValvePwmDuty(laWL2HP);
	}

  #if defined (__ACTUATION_DEBUG)
	laWL1HP->u8debugger = 33;
	laWL2HP->u8debugger = 33;
  #endif /* defined (__ACTUATION_DEBUG) */

}
#endif

/* AUTOSAR --------------------------*/
#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_FILE	idx_CL_LACBCCallActHW
	#include "Mdyn_autosar.h"               
#endif
/* AUTOSAR --------------------------*/
