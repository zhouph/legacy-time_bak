#ifndef __LAPACCALLACTHW_H__
#define __LAPACCALLACTHW_H__

/*includes********************************************************************/
#include "LVarHead.h"

#if __PAC
/*Global Type Declaration ****************************************************/
typedef struct
{
    UINT    u1PacHvVlAct     :1;
    UINT    u1PacAvVlAct     :1;
}LAPAC_VALVE_t;

/*Global Extern Variable  Declaration*****************************************/
extern LAPAC_VALVE_t lapacValveFL, lapacValveFR;

/*Global Extern Functions  Declaration****************************************/
extern void LAPMC_vCallActHW(void);

#endif/*__PAC*/

#endif
