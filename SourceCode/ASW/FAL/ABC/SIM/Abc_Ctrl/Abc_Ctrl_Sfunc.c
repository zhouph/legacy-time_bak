#define S_FUNCTION_NAME      Abc_Ctrl_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          251
#define WidthOutputPort         222

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Abc_Ctrl.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Abc_CtrlEemFailData.Eem_Fail_BBSSol = input[0];
    Abc_CtrlEemFailData.Eem_Fail_ESCSol = input[1];
    Abc_CtrlEemFailData.Eem_Fail_FrontSol = input[2];
    Abc_CtrlEemFailData.Eem_Fail_RearSol = input[3];
    Abc_CtrlEemFailData.Eem_Fail_Motor = input[4];
    Abc_CtrlEemFailData.Eem_Fail_MPS = input[5];
    Abc_CtrlEemFailData.Eem_Fail_MGD = input[6];
    Abc_CtrlEemFailData.Eem_Fail_BBSValveRelay = input[7];
    Abc_CtrlEemFailData.Eem_Fail_ESCValveRelay = input[8];
    Abc_CtrlEemFailData.Eem_Fail_ECUHw = input[9];
    Abc_CtrlEemFailData.Eem_Fail_ASIC = input[10];
    Abc_CtrlEemFailData.Eem_Fail_OverVolt = input[11];
    Abc_CtrlEemFailData.Eem_Fail_UnderVolt = input[12];
    Abc_CtrlEemFailData.Eem_Fail_LowVolt = input[13];
    Abc_CtrlEemFailData.Eem_Fail_LowerVolt = input[14];
    Abc_CtrlEemFailData.Eem_Fail_SenPwr_12V = input[15];
    Abc_CtrlEemFailData.Eem_Fail_SenPwr_5V = input[16];
    Abc_CtrlEemFailData.Eem_Fail_Yaw = input[17];
    Abc_CtrlEemFailData.Eem_Fail_Ay = input[18];
    Abc_CtrlEemFailData.Eem_Fail_Ax = input[19];
    Abc_CtrlEemFailData.Eem_Fail_Str = input[20];
    Abc_CtrlEemFailData.Eem_Fail_CirP1 = input[21];
    Abc_CtrlEemFailData.Eem_Fail_CirP2 = input[22];
    Abc_CtrlEemFailData.Eem_Fail_SimP = input[23];
    Abc_CtrlEemFailData.Eem_Fail_BLS = input[24];
    Abc_CtrlEemFailData.Eem_Fail_ESCSw = input[25];
    Abc_CtrlEemFailData.Eem_Fail_HDCSw = input[26];
    Abc_CtrlEemFailData.Eem_Fail_AVHSw = input[27];
    Abc_CtrlEemFailData.Eem_Fail_BrakeLampRelay = input[28];
    Abc_CtrlEemFailData.Eem_Fail_EssRelay = input[29];
    Abc_CtrlEemFailData.Eem_Fail_GearR = input[30];
    Abc_CtrlEemFailData.Eem_Fail_Clutch = input[31];
    Abc_CtrlEemFailData.Eem_Fail_ParkBrake = input[32];
    Abc_CtrlEemFailData.Eem_Fail_PedalPDT = input[33];
    Abc_CtrlEemFailData.Eem_Fail_PedalPDF = input[34];
    Abc_CtrlEemFailData.Eem_Fail_BrakeFluid = input[35];
    Abc_CtrlEemFailData.Eem_Fail_TCSTemp = input[36];
    Abc_CtrlEemFailData.Eem_Fail_HDCTemp = input[37];
    Abc_CtrlEemFailData.Eem_Fail_SCCTemp = input[38];
    Abc_CtrlEemFailData.Eem_Fail_TVBBTemp = input[39];
    Abc_CtrlEemFailData.Eem_Fail_MainCanLine = input[40];
    Abc_CtrlEemFailData.Eem_Fail_SubCanLine = input[41];
    Abc_CtrlEemFailData.Eem_Fail_EMSTimeOut = input[42];
    Abc_CtrlEemFailData.Eem_Fail_FWDTimeOut = input[43];
    Abc_CtrlEemFailData.Eem_Fail_TCUTimeOut = input[44];
    Abc_CtrlEemFailData.Eem_Fail_HCUTimeOut = input[45];
    Abc_CtrlEemFailData.Eem_Fail_MCUTimeOut = input[46];
    Abc_CtrlEemFailData.Eem_Fail_VariantCoding = input[47];
    Abc_CtrlEemFailData.Eem_Fail_WssFL = input[48];
    Abc_CtrlEemFailData.Eem_Fail_WssFR = input[49];
    Abc_CtrlEemFailData.Eem_Fail_WssRL = input[50];
    Abc_CtrlEemFailData.Eem_Fail_WssRR = input[51];
    Abc_CtrlEemFailData.Eem_Fail_SameSideWss = input[52];
    Abc_CtrlEemFailData.Eem_Fail_DiagonalWss = input[53];
    Abc_CtrlEemFailData.Eem_Fail_FrontWss = input[54];
    Abc_CtrlEemFailData.Eem_Fail_RearWss = input[55];
    Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Cbs = input[56];
    Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Ebd = input[57];
    Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Abs = input[58];
    Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Edc = input[59];
    Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Btcs = input[60];
    Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Etcs = input[61];
    Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Tcs = input[62];
    Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Vdc = input[63];
    Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Hsa = input[64];
    Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Hdc = input[65];
    Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Pba = input[66];
    Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Avh = input[67];
    Abc_CtrlEemCtrlInhibitData.Eem_CtrlIhb_Moc = input[68];
    Abc_CtrlEemCtrlInhibitData.Eem_BBS_AllControlInhibit = input[69];
    Abc_CtrlEemCtrlInhibitData.Eem_BBS_DiagControlInhibit = input[70];
    Abc_CtrlEemCtrlInhibitData.Eem_BBS_DegradeModeFail = input[71];
    Abc_CtrlEemCtrlInhibitData.Eem_BBS_DefectiveModeFail = input[72];
    Abc_CtrlCanRxAccelPedlInfo.AccelPedlVal = input[73];
    Abc_CtrlCanRxAccelPedlInfo.AccelPedlValErr = input[74];
    Abc_CtrlCanRxIdbInfo.TarGearPosi = input[75];
    Abc_CtrlCanRxIdbInfo.GearSelDisp = input[76];
    Abc_CtrlCanRxIdbInfo.TcuSwiGs = input[77];
    Abc_CtrlCanRxEngTempInfo.EngTemp = input[78];
    Abc_CtrlCanRxEngTempInfo.EngTempErr = input[79];
    Abc_CtrlCanRxEscInfo.Ax = input[80];
    Abc_CtrlCanRxEscInfo.YawRate = input[81];
    Abc_CtrlCanRxEscInfo.SteeringAngle = input[82];
    Abc_CtrlCanRxEscInfo.Ay = input[83];
    Abc_CtrlCanRxEscInfo.PbSwt = input[84];
    Abc_CtrlCanRxEscInfo.ClutchSwt = input[85];
    Abc_CtrlCanRxEscInfo.GearRSwt = input[86];
    Abc_CtrlCanRxEscInfo.EngActIndTq = input[87];
    Abc_CtrlCanRxEscInfo.EngRpm = input[88];
    Abc_CtrlCanRxEscInfo.EngIndTq = input[89];
    Abc_CtrlCanRxEscInfo.EngFrictionLossTq = input[90];
    Abc_CtrlCanRxEscInfo.EngStdTq = input[91];
    Abc_CtrlCanRxEscInfo.TurbineRpm = input[92];
    Abc_CtrlCanRxEscInfo.ThrottleAngle = input[93];
    Abc_CtrlCanRxEscInfo.TpsResol1000 = input[94];
    Abc_CtrlCanRxEscInfo.PvAvCanResol1000 = input[95];
    Abc_CtrlCanRxEscInfo.EngChr = input[96];
    Abc_CtrlCanRxEscInfo.EngVol = input[97];
    Abc_CtrlCanRxEscInfo.GearType = input[98];
    Abc_CtrlCanRxEscInfo.EngClutchState = input[99];
    Abc_CtrlCanRxEscInfo.EngTqCmdBeforeIntv = input[100];
    Abc_CtrlCanRxEscInfo.MotEstTq = input[101];
    Abc_CtrlCanRxEscInfo.MotTqCmdBeforeIntv = input[102];
    Abc_CtrlCanRxEscInfo.TqIntvTCU = input[103];
    Abc_CtrlCanRxEscInfo.TqIntvSlowTCU = input[104];
    Abc_CtrlCanRxEscInfo.TqIncReq = input[105];
    Abc_CtrlCanRxEscInfo.DecelReq = input[106];
    Abc_CtrlCanRxEscInfo.RainSnsStat = input[107];
    Abc_CtrlCanRxEscInfo.EngSpdErr = input[108];
    Abc_CtrlCanRxEscInfo.AtType = input[109];
    Abc_CtrlCanRxEscInfo.MtType = input[110];
    Abc_CtrlCanRxEscInfo.CvtType = input[111];
    Abc_CtrlCanRxEscInfo.DctType = input[112];
    Abc_CtrlCanRxEscInfo.HevAtTcu = input[113];
    Abc_CtrlCanRxEscInfo.TurbineRpmErr = input[114];
    Abc_CtrlCanRxEscInfo.ThrottleAngleErr = input[115];
    Abc_CtrlCanRxEscInfo.TopTrvlCltchSwtAct = input[116];
    Abc_CtrlCanRxEscInfo.TopTrvlCltchSwtActV = input[117];
    Abc_CtrlCanRxEscInfo.HillDesCtrlMdSwtAct = input[118];
    Abc_CtrlCanRxEscInfo.HillDesCtrlMdSwtActV = input[119];
    Abc_CtrlCanRxEscInfo.WiperIntSW = input[120];
    Abc_CtrlCanRxEscInfo.WiperLow = input[121];
    Abc_CtrlCanRxEscInfo.WiperHigh = input[122];
    Abc_CtrlCanRxEscInfo.WiperValid = input[123];
    Abc_CtrlCanRxEscInfo.WiperAuto = input[124];
    Abc_CtrlCanRxEscInfo.TcuFaultSts = input[125];
    Abc_CtrlMotRotgAgSigInfo.StkPosnMeasd = input[126];
    Abc_CtrlEscSwtStInfo.EscDisabledBySwt = input[127];
    Abc_CtrlEscSwtStInfo.TcsDisabledBySwt = input[128];
    Abc_CtrlWhlSpdInfo.FlWhlSpd = input[129];
    Abc_CtrlWhlSpdInfo.FrWhlSpd = input[130];
    Abc_CtrlWhlSpdInfo.RlWhlSpd = input[131];
    Abc_CtrlWhlSpdInfo.RrlWhlSpd = input[132];
    Abc_CtrlSasCalInfo.DiagSasCaltoAppl = input[133];
    Abc_CtrlBrkPedlStatusInfo.DrvrIntendToRelsBrk = input[134];
    Abc_CtrlCircPFildInfo.PrimCircPFild_1_100Bar = input[135];
    Abc_CtrlCircPFildInfo.SecdCircPFild_1_100Bar = input[136];
    Abc_CtrlCircPOffsCorrdInfo.PrimCircPOffsCorrd = input[137];
    Abc_CtrlCircPOffsCorrdInfo.SecdCircPOffsCorrd = input[138];
    Abc_CtrlEstimdWhlPInfo.FrntLeEstimdWhlP = input[139];
    Abc_CtrlEstimdWhlPInfo.FrntRiEstimdWhlP = input[140];
    Abc_CtrlEstimdWhlPInfo.ReLeEstimdWhlP = input[141];
    Abc_CtrlEstimdWhlPInfo.ReRiEstimdWhlP = input[142];
    Abc_CtrlPedlSimrPOffsCorrdInfo.PedlSimrPOffsCorrd = input[143];
    Abc_CtrlPedlTrvlOffsCorrdInfo.PdfRawOffsCorrd = input[144];
    Abc_CtrlPedlTrvlOffsCorrdInfo.PdtRawOffsCorrd = input[145];
    Abc_CtrlPistPFildInfo.PistPFild_1_100Bar = input[146];
    Abc_CtrlPistPOffsCorrdInfo.PistPOffsCorrd = input[147];
    Abc_CtrlRgnBrkCoopWithAbsInfo.FrntSlipDetThdSlip = input[148];
    Abc_CtrlRgnBrkCoopWithAbsInfo.FrntSlipDetThdUDiff = input[149];
    Abc_CtrlStkRecvryActnIfInfo.RecommendStkRcvrLvl = input[150];
    Abc_CtrlStkRecvryActnIfInfo.StkRecvryCtrlState = input[151];
    Abc_CtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlFrntLe = input[152];
    Abc_CtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlFrntRi = input[153];
    Abc_CtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlReLe = input[154];
    Abc_CtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlReRi = input[155];
    Abc_CtrlTxESCSensorInfo.EstimatedYaw = input[156];
    Abc_CtrlTxESCSensorInfo.EstimatedAY = input[157];
    Abc_CtrlTxESCSensorInfo.A_long_1_1000g = input[158];
    Abc_CtrlSenPwrMonitorData.SenPwrM_12V_Stable = input[159];
    Abc_CtrlEemSuspectData.Eem_Suspect_WssFL = input[160];
    Abc_CtrlEemSuspectData.Eem_Suspect_WssFR = input[161];
    Abc_CtrlEemSuspectData.Eem_Suspect_WssRL = input[162];
    Abc_CtrlEemSuspectData.Eem_Suspect_WssRR = input[163];
    Abc_CtrlEemSuspectData.Eem_Suspect_SameSideWss = input[164];
    Abc_CtrlEemSuspectData.Eem_Suspect_DiagonalWss = input[165];
    Abc_CtrlEemSuspectData.Eem_Suspect_FrontWss = input[166];
    Abc_CtrlEemSuspectData.Eem_Suspect_RearWss = input[167];
    Abc_CtrlEemSuspectData.Eem_Suspect_BBSSol = input[168];
    Abc_CtrlEemSuspectData.Eem_Suspect_ESCSol = input[169];
    Abc_CtrlEemSuspectData.Eem_Suspect_FrontSol = input[170];
    Abc_CtrlEemSuspectData.Eem_Suspect_RearSol = input[171];
    Abc_CtrlEemSuspectData.Eem_Suspect_Motor = input[172];
    Abc_CtrlEemSuspectData.Eem_Suspect_MPS = input[173];
    Abc_CtrlEemSuspectData.Eem_Suspect_MGD = input[174];
    Abc_CtrlEemSuspectData.Eem_Suspect_BBSValveRelay = input[175];
    Abc_CtrlEemSuspectData.Eem_Suspect_ESCValveRelay = input[176];
    Abc_CtrlEemSuspectData.Eem_Suspect_ECUHw = input[177];
    Abc_CtrlEemSuspectData.Eem_Suspect_ASIC = input[178];
    Abc_CtrlEemSuspectData.Eem_Suspect_OverVolt = input[179];
    Abc_CtrlEemSuspectData.Eem_Suspect_UnderVolt = input[180];
    Abc_CtrlEemSuspectData.Eem_Suspect_LowVolt = input[181];
    Abc_CtrlEemSuspectData.Eem_Suspect_SenPwr_12V = input[182];
    Abc_CtrlEemSuspectData.Eem_Suspect_SenPwr_5V = input[183];
    Abc_CtrlEemSuspectData.Eem_Suspect_Yaw = input[184];
    Abc_CtrlEemSuspectData.Eem_Suspect_Ay = input[185];
    Abc_CtrlEemSuspectData.Eem_Suspect_Ax = input[186];
    Abc_CtrlEemSuspectData.Eem_Suspect_Str = input[187];
    Abc_CtrlEemSuspectData.Eem_Suspect_CirP1 = input[188];
    Abc_CtrlEemSuspectData.Eem_Suspect_CirP2 = input[189];
    Abc_CtrlEemSuspectData.Eem_Suspect_SimP = input[190];
    Abc_CtrlEemSuspectData.Eem_Suspect_BS = input[191];
    Abc_CtrlEemSuspectData.Eem_Suspect_BLS = input[192];
    Abc_CtrlEemSuspectData.Eem_Suspect_ESCSw = input[193];
    Abc_CtrlEemSuspectData.Eem_Suspect_HDCSw = input[194];
    Abc_CtrlEemSuspectData.Eem_Suspect_AVHSw = input[195];
    Abc_CtrlEemSuspectData.Eem_Suspect_BrakeLampRelay = input[196];
    Abc_CtrlEemSuspectData.Eem_Suspect_EssRelay = input[197];
    Abc_CtrlEemSuspectData.Eem_Suspect_GearR = input[198];
    Abc_CtrlEemSuspectData.Eem_Suspect_Clutch = input[199];
    Abc_CtrlEemSuspectData.Eem_Suspect_ParkBrake = input[200];
    Abc_CtrlEemSuspectData.Eem_Suspect_PedalPDT = input[201];
    Abc_CtrlEemSuspectData.Eem_Suspect_PedalPDF = input[202];
    Abc_CtrlEemSuspectData.Eem_Suspect_BrakeFluid = input[203];
    Abc_CtrlEemSuspectData.Eem_Suspect_TCSTemp = input[204];
    Abc_CtrlEemSuspectData.Eem_Suspect_HDCTemp = input[205];
    Abc_CtrlEemSuspectData.Eem_Suspect_SCCTemp = input[206];
    Abc_CtrlEemSuspectData.Eem_Suspect_TVBBTemp = input[207];
    Abc_CtrlEemSuspectData.Eem_Suspect_MainCanLine = input[208];
    Abc_CtrlEemSuspectData.Eem_Suspect_SubCanLine = input[209];
    Abc_CtrlEemSuspectData.Eem_Suspect_EMSTimeOut = input[210];
    Abc_CtrlEemSuspectData.Eem_Suspect_FWDTimeOut = input[211];
    Abc_CtrlEemSuspectData.Eem_Suspect_TCUTimeOut = input[212];
    Abc_CtrlEemSuspectData.Eem_Suspect_HCUTimeOut = input[213];
    Abc_CtrlEemSuspectData.Eem_Suspect_MCUTimeOut = input[214];
    Abc_CtrlEemSuspectData.Eem_Suspect_VariantCoding = input[215];
    Abc_CtrlEemEceData.Eem_Ece_Wss = input[216];
    Abc_CtrlEemEceData.Eem_Ece_Yaw = input[217];
    Abc_CtrlEemEceData.Eem_Ece_Ay = input[218];
    Abc_CtrlEemEceData.Eem_Ece_Ax = input[219];
    Abc_CtrlEemEceData.Eem_Ece_Cir1P = input[220];
    Abc_CtrlEemEceData.Eem_Ece_Cir2P = input[221];
    Abc_CtrlEemEceData.Eem_Ece_SimP = input[222];
    Abc_CtrlEemEceData.Eem_Ece_Bls = input[223];
    Abc_CtrlEemEceData.Eem_Ece_Pedal = input[224];
    Abc_CtrlEemEceData.Eem_Ece_Motor = input[225];
    Abc_CtrlEemEceData.Eem_Ece_Vdc_Sw = input[226];
    Abc_CtrlEemEceData.Eem_Ece_Hdc_Sw = input[227];
    Abc_CtrlEemEceData.Eem_Ece_GearR_Sw = input[228];
    Abc_CtrlEemEceData.Eem_Ece_Clutch_Sw = input[229];
    Abc_CtrlYAWMSerialInfo.YAWM_SerialNumOK_Flg = input[230];
    Abc_CtrlYAWMSerialInfo.YAWM_SerialUnMatch_Flg = input[231];
    Abc_CtrlYAWMOutInfo.YAWM_Temperature_Err = input[232];
    Abc_CtrlFinalTarPInfo.FinalMaxCircuitTp = input[233];
    Abc_CtrlFSBbsVlvInitEndFlg = input[234];
    Abc_CtrlEcuModeSts = input[235];
    Abc_CtrlIgnOnOffSts = input[236];
    Abc_CtrlVBatt1Mon = input[237];
    Abc_CtrlFSEscVlvInitEndFlg = input[238];
    Abc_CtrlDiagSci = input[239];
    Abc_CtrlCanRxGearSelDispErrInfo = input[240];
    Abc_CtrlPCtrlAct = input[241];
    Abc_CtrlBlsSwt = input[242];
    Abc_CtrlEscSwt = input[243];
    Abc_CtrlBrkPRednForBaseBrkCtrlr = input[244];
    Abc_CtrlPedlTrvlFinal = input[245];
    Abc_CtrlRgnBrkCtrlrActStFlg = input[246];
    Abc_CtrlFinalTarPFromPCtrl = input[247];
    Abc_CtrlTarPFromBaseBrkCtrlr = input[248];
    Abc_CtrlTarPFromBrkPedl = input[249];
    Abc_CtrlMTRInitEndFlg = input[250];

    Abc_Ctrl();


    output[0] = Abc_CtrlWhlVlvReqAbcInfo.FlIvReqData[0];
    output[1] = Abc_CtrlWhlVlvReqAbcInfo.FlIvReqData[1];
    output[2] = Abc_CtrlWhlVlvReqAbcInfo.FlIvReqData[2];
    output[3] = Abc_CtrlWhlVlvReqAbcInfo.FlIvReqData[3];
    output[4] = Abc_CtrlWhlVlvReqAbcInfo.FlIvReqData[4];
    output[5] = Abc_CtrlWhlVlvReqAbcInfo.FlIvReqData[5];
    output[6] = Abc_CtrlWhlVlvReqAbcInfo.FlIvReqData[6];
    output[7] = Abc_CtrlWhlVlvReqAbcInfo.FlIvReqData[7];
    output[8] = Abc_CtrlWhlVlvReqAbcInfo.FlIvReqData[8];
    output[9] = Abc_CtrlWhlVlvReqAbcInfo.FlIvReqData[9];
    output[10] = Abc_CtrlWhlVlvReqAbcInfo.FrIvReqData[0];
    output[11] = Abc_CtrlWhlVlvReqAbcInfo.FrIvReqData[1];
    output[12] = Abc_CtrlWhlVlvReqAbcInfo.FrIvReqData[2];
    output[13] = Abc_CtrlWhlVlvReqAbcInfo.FrIvReqData[3];
    output[14] = Abc_CtrlWhlVlvReqAbcInfo.FrIvReqData[4];
    output[15] = Abc_CtrlWhlVlvReqAbcInfo.FrIvReqData[5];
    output[16] = Abc_CtrlWhlVlvReqAbcInfo.FrIvReqData[6];
    output[17] = Abc_CtrlWhlVlvReqAbcInfo.FrIvReqData[7];
    output[18] = Abc_CtrlWhlVlvReqAbcInfo.FrIvReqData[8];
    output[19] = Abc_CtrlWhlVlvReqAbcInfo.FrIvReqData[9];
    output[20] = Abc_CtrlWhlVlvReqAbcInfo.RlIvReqData[0];
    output[21] = Abc_CtrlWhlVlvReqAbcInfo.RlIvReqData[1];
    output[22] = Abc_CtrlWhlVlvReqAbcInfo.RlIvReqData[2];
    output[23] = Abc_CtrlWhlVlvReqAbcInfo.RlIvReqData[3];
    output[24] = Abc_CtrlWhlVlvReqAbcInfo.RlIvReqData[4];
    output[25] = Abc_CtrlWhlVlvReqAbcInfo.RlIvReqData[5];
    output[26] = Abc_CtrlWhlVlvReqAbcInfo.RlIvReqData[6];
    output[27] = Abc_CtrlWhlVlvReqAbcInfo.RlIvReqData[7];
    output[28] = Abc_CtrlWhlVlvReqAbcInfo.RlIvReqData[8];
    output[29] = Abc_CtrlWhlVlvReqAbcInfo.RlIvReqData[9];
    output[30] = Abc_CtrlWhlVlvReqAbcInfo.RrIvReqData[0];
    output[31] = Abc_CtrlWhlVlvReqAbcInfo.RrIvReqData[1];
    output[32] = Abc_CtrlWhlVlvReqAbcInfo.RrIvReqData[2];
    output[33] = Abc_CtrlWhlVlvReqAbcInfo.RrIvReqData[3];
    output[34] = Abc_CtrlWhlVlvReqAbcInfo.RrIvReqData[4];
    output[35] = Abc_CtrlWhlVlvReqAbcInfo.RrIvReqData[5];
    output[36] = Abc_CtrlWhlVlvReqAbcInfo.RrIvReqData[6];
    output[37] = Abc_CtrlWhlVlvReqAbcInfo.RrIvReqData[7];
    output[38] = Abc_CtrlWhlVlvReqAbcInfo.RrIvReqData[8];
    output[39] = Abc_CtrlWhlVlvReqAbcInfo.RrIvReqData[9];
    output[40] = Abc_CtrlWhlVlvReqAbcInfo.FlOvReqData[0];
    output[41] = Abc_CtrlWhlVlvReqAbcInfo.FlOvReqData[1];
    output[42] = Abc_CtrlWhlVlvReqAbcInfo.FlOvReqData[2];
    output[43] = Abc_CtrlWhlVlvReqAbcInfo.FlOvReqData[3];
    output[44] = Abc_CtrlWhlVlvReqAbcInfo.FlOvReqData[4];
    output[45] = Abc_CtrlWhlVlvReqAbcInfo.FlOvReqData[5];
    output[46] = Abc_CtrlWhlVlvReqAbcInfo.FlOvReqData[6];
    output[47] = Abc_CtrlWhlVlvReqAbcInfo.FlOvReqData[7];
    output[48] = Abc_CtrlWhlVlvReqAbcInfo.FlOvReqData[8];
    output[49] = Abc_CtrlWhlVlvReqAbcInfo.FlOvReqData[9];
    output[50] = Abc_CtrlWhlVlvReqAbcInfo.FrOvReqData[0];
    output[51] = Abc_CtrlWhlVlvReqAbcInfo.FrOvReqData[1];
    output[52] = Abc_CtrlWhlVlvReqAbcInfo.FrOvReqData[2];
    output[53] = Abc_CtrlWhlVlvReqAbcInfo.FrOvReqData[3];
    output[54] = Abc_CtrlWhlVlvReqAbcInfo.FrOvReqData[4];
    output[55] = Abc_CtrlWhlVlvReqAbcInfo.FrOvReqData[5];
    output[56] = Abc_CtrlWhlVlvReqAbcInfo.FrOvReqData[6];
    output[57] = Abc_CtrlWhlVlvReqAbcInfo.FrOvReqData[7];
    output[58] = Abc_CtrlWhlVlvReqAbcInfo.FrOvReqData[8];
    output[59] = Abc_CtrlWhlVlvReqAbcInfo.FrOvReqData[9];
    output[60] = Abc_CtrlWhlVlvReqAbcInfo.RlOvReqData[0];
    output[61] = Abc_CtrlWhlVlvReqAbcInfo.RlOvReqData[1];
    output[62] = Abc_CtrlWhlVlvReqAbcInfo.RlOvReqData[2];
    output[63] = Abc_CtrlWhlVlvReqAbcInfo.RlOvReqData[3];
    output[64] = Abc_CtrlWhlVlvReqAbcInfo.RlOvReqData[4];
    output[65] = Abc_CtrlWhlVlvReqAbcInfo.RlOvReqData[5];
    output[66] = Abc_CtrlWhlVlvReqAbcInfo.RlOvReqData[6];
    output[67] = Abc_CtrlWhlVlvReqAbcInfo.RlOvReqData[7];
    output[68] = Abc_CtrlWhlVlvReqAbcInfo.RlOvReqData[8];
    output[69] = Abc_CtrlWhlVlvReqAbcInfo.RlOvReqData[9];
    output[70] = Abc_CtrlWhlVlvReqAbcInfo.RrOvReqData[0];
    output[71] = Abc_CtrlWhlVlvReqAbcInfo.RrOvReqData[1];
    output[72] = Abc_CtrlWhlVlvReqAbcInfo.RrOvReqData[2];
    output[73] = Abc_CtrlWhlVlvReqAbcInfo.RrOvReqData[3];
    output[74] = Abc_CtrlWhlVlvReqAbcInfo.RrOvReqData[4];
    output[75] = Abc_CtrlWhlVlvReqAbcInfo.RrOvReqData[5];
    output[76] = Abc_CtrlWhlVlvReqAbcInfo.RrOvReqData[6];
    output[77] = Abc_CtrlWhlVlvReqAbcInfo.RrOvReqData[7];
    output[78] = Abc_CtrlWhlVlvReqAbcInfo.RrOvReqData[8];
    output[79] = Abc_CtrlWhlVlvReqAbcInfo.RrOvReqData[9];
    output[80] = Abc_CtrlWhlVlvReqAbcInfo.FlIvReq;
    output[81] = Abc_CtrlWhlVlvReqAbcInfo.FrIvReq;
    output[82] = Abc_CtrlWhlVlvReqAbcInfo.RlIvReq;
    output[83] = Abc_CtrlWhlVlvReqAbcInfo.RrIvReq;
    output[84] = Abc_CtrlWhlVlvReqAbcInfo.FlOvReq;
    output[85] = Abc_CtrlWhlVlvReqAbcInfo.FrOvReq;
    output[86] = Abc_CtrlWhlVlvReqAbcInfo.RlOvReq;
    output[87] = Abc_CtrlWhlVlvReqAbcInfo.RrOvReq;
    output[88] = Abc_CtrlWhlVlvReqAbcInfo.FlIvDataLen;
    output[89] = Abc_CtrlWhlVlvReqAbcInfo.FrIvDataLen;
    output[90] = Abc_CtrlWhlVlvReqAbcInfo.RlIvDataLen;
    output[91] = Abc_CtrlWhlVlvReqAbcInfo.RrIvDataLen;
    output[92] = Abc_CtrlWhlVlvReqAbcInfo.FlOvDataLen;
    output[93] = Abc_CtrlWhlVlvReqAbcInfo.FrOvDataLen;
    output[94] = Abc_CtrlWhlVlvReqAbcInfo.RlOvDataLen;
    output[95] = Abc_CtrlWhlVlvReqAbcInfo.RrOvDataLen;
    output[96] = Abc_CtrlCanTxInfo.TqIntvTCS;
    output[97] = Abc_CtrlCanTxInfo.TqIntvMsr;
    output[98] = Abc_CtrlCanTxInfo.TqIntvSlowTCS;
    output[99] = Abc_CtrlCanTxInfo.MinGear;
    output[100] = Abc_CtrlCanTxInfo.MaxGear;
    output[101] = Abc_CtrlCanTxInfo.TcsReq;
    output[102] = Abc_CtrlCanTxInfo.TcsCtrl;
    output[103] = Abc_CtrlCanTxInfo.AbsAct;
    output[104] = Abc_CtrlCanTxInfo.TcsGearShiftChr;
    output[105] = Abc_CtrlCanTxInfo.EspCtrl;
    output[106] = Abc_CtrlCanTxInfo.MsrReq;
    output[107] = Abc_CtrlCanTxInfo.TcsProductInfo;
    output[108] = Abc_CtrlAbsCtrlInfo.AbsActFlg;
    output[109] = Abc_CtrlAbsCtrlInfo.AbsDefectFlg;
    output[110] = Abc_CtrlAbsCtrlInfo.AbsTarPFrntLe;
    output[111] = Abc_CtrlAbsCtrlInfo.AbsTarPFrntRi;
    output[112] = Abc_CtrlAbsCtrlInfo.AbsTarPReLe;
    output[113] = Abc_CtrlAbsCtrlInfo.AbsTarPReRi;
    output[114] = Abc_CtrlAbsCtrlInfo.FrntWhlSlip;
    output[115] = Abc_CtrlAbsCtrlInfo.AbsDesTarP;
    output[116] = Abc_CtrlAbsCtrlInfo.AbsDesTarPReqFlg;
    output[117] = Abc_CtrlAbsCtrlInfo.AbsCtrlFadeOutFlg;
    output[118] = Abc_CtrlAbsCtrlInfo.AbsCtrlModeFrntLe;
    output[119] = Abc_CtrlAbsCtrlInfo.AbsCtrlModeFrntRi;
    output[120] = Abc_CtrlAbsCtrlInfo.AbsCtrlModeReLe;
    output[121] = Abc_CtrlAbsCtrlInfo.AbsCtrlModeReRi;
    output[122] = Abc_CtrlAbsCtrlInfo.AbsTarPRateFrntLe;
    output[123] = Abc_CtrlAbsCtrlInfo.AbsTarPRateFrntRi;
    output[124] = Abc_CtrlAbsCtrlInfo.AbsTarPRateReLe;
    output[125] = Abc_CtrlAbsCtrlInfo.AbsTarPRateReRi;
    output[126] = Abc_CtrlAbsCtrlInfo.AbsPrioFrntLe;
    output[127] = Abc_CtrlAbsCtrlInfo.AbsPrioFrntRi;
    output[128] = Abc_CtrlAbsCtrlInfo.AbsPrioReLe;
    output[129] = Abc_CtrlAbsCtrlInfo.AbsPrioReRi;
    output[130] = Abc_CtrlAbsCtrlInfo.AbsDelTarPFrntLe;
    output[131] = Abc_CtrlAbsCtrlInfo.AbsDelTarPFrntRi;
    output[132] = Abc_CtrlAbsCtrlInfo.AbsDelTarPReLe;
    output[133] = Abc_CtrlAbsCtrlInfo.AbsDelTarPReRi;
    output[134] = Abc_CtrlAvhCtrlInfo.AvhActFlg;
    output[135] = Abc_CtrlAvhCtrlInfo.AvhDefectFlg;
    output[136] = Abc_CtrlAvhCtrlInfo.AvhTarP;
    output[137] = Abc_CtrlBaCtrlInfo.BaActFlg;
    output[138] = Abc_CtrlBaCtrlInfo.BaCDefectFlg;
    output[139] = Abc_CtrlBaCtrlInfo.BaTarP;
    output[140] = Abc_CtrlEbdCtrlInfo.EbdActFlg;
    output[141] = Abc_CtrlEbdCtrlInfo.EbdDefectFlg;
    output[142] = Abc_CtrlEbdCtrlInfo.EbdTarPFrntLe;
    output[143] = Abc_CtrlEbdCtrlInfo.EbdTarPFrntRi;
    output[144] = Abc_CtrlEbdCtrlInfo.EbdTarPReLe;
    output[145] = Abc_CtrlEbdCtrlInfo.EbdTarPReRi;
    output[146] = Abc_CtrlEbdCtrlInfo.EbdTarPRateReLe;
    output[147] = Abc_CtrlEbdCtrlInfo.EbdTarPRateReRi;
    output[148] = Abc_CtrlEbdCtrlInfo.EbdCtrlModeReLe;
    output[149] = Abc_CtrlEbdCtrlInfo.EbdCtrlModeReRi;
    output[150] = Abc_CtrlEbdCtrlInfo.EbdDelTarPReLe;
    output[151] = Abc_CtrlEbdCtrlInfo.EbdDelTarPReRi;
    output[152] = Abc_CtrlEbpCtrlInfo.EbpActFlg;
    output[153] = Abc_CtrlEbpCtrlInfo.EbpDefectFlg;
    output[154] = Abc_CtrlEbpCtrlInfo.EbpTarP;
    output[155] = Abc_CtrlEpbiCtrlInfo.EpbiActFlg;
    output[156] = Abc_CtrlEpbiCtrlInfo.EpbiDefectFlg;
    output[157] = Abc_CtrlEpbiCtrlInfo.EpbiTarP;
    output[158] = Abc_CtrlEscCtrlInfo.EscActFlg;
    output[159] = Abc_CtrlEscCtrlInfo.EscDefectFlg;
    output[160] = Abc_CtrlEscCtrlInfo.EscTarPFrntLe;
    output[161] = Abc_CtrlEscCtrlInfo.EscTarPFrntRi;
    output[162] = Abc_CtrlEscCtrlInfo.EscTarPReLe;
    output[163] = Abc_CtrlEscCtrlInfo.EscTarPReRi;
    output[164] = Abc_CtrlEscCtrlInfo.EscCtrlModeFrntLe;
    output[165] = Abc_CtrlEscCtrlInfo.EscCtrlModeFrntRi;
    output[166] = Abc_CtrlEscCtrlInfo.EscCtrlModeReLe;
    output[167] = Abc_CtrlEscCtrlInfo.EscCtrlModeReRi;
    output[168] = Abc_CtrlEscCtrlInfo.EscTarPRateFrntLe;
    output[169] = Abc_CtrlEscCtrlInfo.EscTarPRateFrntRi;
    output[170] = Abc_CtrlEscCtrlInfo.EscTarPRateReLe;
    output[171] = Abc_CtrlEscCtrlInfo.EscTarPRateReRi;
    output[172] = Abc_CtrlEscCtrlInfo.EscPrioFrntLe;
    output[173] = Abc_CtrlEscCtrlInfo.EscPrioFrntRi;
    output[174] = Abc_CtrlEscCtrlInfo.EscPrioReLe;
    output[175] = Abc_CtrlEscCtrlInfo.EscPrioReRi;
    output[176] = Abc_CtrlEscCtrlInfo.EscPreCtrlModeFrntLe;
    output[177] = Abc_CtrlEscCtrlInfo.EscPreCtrlModeFrntRi;
    output[178] = Abc_CtrlEscCtrlInfo.EscPreCtrlModeReLe;
    output[179] = Abc_CtrlEscCtrlInfo.EscPreCtrlModeReRi;
    output[180] = Abc_CtrlEscCtrlInfo.EscDelTarPFrntLe;
    output[181] = Abc_CtrlEscCtrlInfo.EscDelTarPFrntRi;
    output[182] = Abc_CtrlEscCtrlInfo.EscDelTarPReLe;
    output[183] = Abc_CtrlEscCtrlInfo.EscDelTarPReRi;
    output[184] = Abc_CtrlHdcCtrlInfo.HdcActFlg;
    output[185] = Abc_CtrlHdcCtrlInfo.HdcDefectFlg;
    output[186] = Abc_CtrlHdcCtrlInfo.HdcTarP;
    output[187] = Abc_CtrlHsaCtrlInfo.HsaActFlg;
    output[188] = Abc_CtrlHsaCtrlInfo.HsaDefectFlg;
    output[189] = Abc_CtrlHsaCtrlInfo.HsaTarP;
    output[190] = Abc_CtrlSccCtrlInfo.SccActFlg;
    output[191] = Abc_CtrlSccCtrlInfo.SccDefectFlg;
    output[192] = Abc_CtrlSccCtrlInfo.SccTarP;
    output[193] = Abc_CtrlStkRecvryCtrlIfInfo.StkRcvrEscAllowedTime;
    output[194] = Abc_CtrlStkRecvryCtrlIfInfo.StkRcvrAbsAllowedTime;
    output[195] = Abc_CtrlStkRecvryCtrlIfInfo.StkRcvrTcsAllowedTime;
    output[196] = Abc_CtrlTcsCtrlInfo.TcsActFlg;
    output[197] = Abc_CtrlTcsCtrlInfo.TcsDefectFlg;
    output[198] = Abc_CtrlTcsCtrlInfo.TcsTarPFrntLe;
    output[199] = Abc_CtrlTcsCtrlInfo.TcsTarPFrntRi;
    output[200] = Abc_CtrlTcsCtrlInfo.TcsTarPReLe;
    output[201] = Abc_CtrlTcsCtrlInfo.TcsTarPReRi;
    output[202] = Abc_CtrlTcsCtrlInfo.BothDrvgWhlBrkCtlReqFlg;
    output[203] = Abc_CtrlTcsCtrlInfo.BrkCtrlFctFrntLeByWspc;
    output[204] = Abc_CtrlTcsCtrlInfo.BrkCtrlFctFrntRiByWspc;
    output[205] = Abc_CtrlTcsCtrlInfo.BrkCtrlFctReLeByWspc;
    output[206] = Abc_CtrlTcsCtrlInfo.BrkCtrlFctReRiByWspc;
    output[207] = Abc_CtrlTcsCtrlInfo.BrkTqGrdtReqFrntLeByWspc;
    output[208] = Abc_CtrlTcsCtrlInfo.BrkTqGrdtReqFrntRiByWspc;
    output[209] = Abc_CtrlTcsCtrlInfo.BrkTqGrdtReqReLeByWspc;
    output[210] = Abc_CtrlTcsCtrlInfo.BrkTqGrdtReqReRiByWspc;
    output[211] = Abc_CtrlTcsCtrlInfo.TcsDelTarPFrntLe;
    output[212] = Abc_CtrlTcsCtrlInfo.TcsDelTarPFrntRi;
    output[213] = Abc_CtrlTcsCtrlInfo.TcsDelTarPReLe;
    output[214] = Abc_CtrlTcsCtrlInfo.TcsDelTarPReRi;
    output[215] = Abc_CtrlTvbbCtrlInfo.TvbbActFlg;
    output[216] = Abc_CtrlTvbbCtrlInfo.TvbbDefectFlg;
    output[217] = Abc_CtrlTvbbCtrlInfo.TvbbTarP;
    output[218] = Abc_CtrlFunctionLamp;
    output[219] = Abc_CtrlActvBrkCtrlrActFlg;
    output[220] = Abc_CtrlAy;
    output[221] = Abc_CtrlVehSpd;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Abc_Ctrl_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
