Abc_CtrlEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_BBSSol'
    'Eem_Fail_ESCSol'
    'Eem_Fail_FrontSol'
    'Eem_Fail_RearSol'
    'Eem_Fail_Motor'
    'Eem_Fail_MPS'
    'Eem_Fail_MGD'
    'Eem_Fail_BBSValveRelay'
    'Eem_Fail_ESCValveRelay'
    'Eem_Fail_ECUHw'
    'Eem_Fail_ASIC'
    'Eem_Fail_OverVolt'
    'Eem_Fail_UnderVolt'
    'Eem_Fail_LowVolt'
    'Eem_Fail_LowerVolt'
    'Eem_Fail_SenPwr_12V'
    'Eem_Fail_SenPwr_5V'
    'Eem_Fail_Yaw'
    'Eem_Fail_Ay'
    'Eem_Fail_Ax'
    'Eem_Fail_Str'
    'Eem_Fail_CirP1'
    'Eem_Fail_CirP2'
    'Eem_Fail_SimP'
    'Eem_Fail_BLS'
    'Eem_Fail_ESCSw'
    'Eem_Fail_HDCSw'
    'Eem_Fail_AVHSw'
    'Eem_Fail_BrakeLampRelay'
    'Eem_Fail_EssRelay'
    'Eem_Fail_GearR'
    'Eem_Fail_Clutch'
    'Eem_Fail_ParkBrake'
    'Eem_Fail_PedalPDT'
    'Eem_Fail_PedalPDF'
    'Eem_Fail_BrakeFluid'
    'Eem_Fail_TCSTemp'
    'Eem_Fail_HDCTemp'
    'Eem_Fail_SCCTemp'
    'Eem_Fail_TVBBTemp'
    'Eem_Fail_MainCanLine'
    'Eem_Fail_SubCanLine'
    'Eem_Fail_EMSTimeOut'
    'Eem_Fail_FWDTimeOut'
    'Eem_Fail_TCUTimeOut'
    'Eem_Fail_HCUTimeOut'
    'Eem_Fail_MCUTimeOut'
    'Eem_Fail_VariantCoding'
    'Eem_Fail_WssFL'
    'Eem_Fail_WssFR'
    'Eem_Fail_WssRL'
    'Eem_Fail_WssRR'
    'Eem_Fail_SameSideWss'
    'Eem_Fail_DiagonalWss'
    'Eem_Fail_FrontWss'
    'Eem_Fail_RearWss'
    };
Abc_CtrlEemFailData = CreateBus(Abc_CtrlEemFailData, DeList);
clear DeList;

Abc_CtrlEemCtrlInhibitData = Simulink.Bus;
DeList={
    'Eem_CtrlIhb_Cbs'
    'Eem_CtrlIhb_Ebd'
    'Eem_CtrlIhb_Abs'
    'Eem_CtrlIhb_Edc'
    'Eem_CtrlIhb_Btcs'
    'Eem_CtrlIhb_Etcs'
    'Eem_CtrlIhb_Tcs'
    'Eem_CtrlIhb_Vdc'
    'Eem_CtrlIhb_Hsa'
    'Eem_CtrlIhb_Hdc'
    'Eem_CtrlIhb_Pba'
    'Eem_CtrlIhb_Avh'
    'Eem_CtrlIhb_Moc'
    'Eem_BBS_AllControlInhibit'
    'Eem_BBS_DiagControlInhibit'
    'Eem_BBS_DegradeModeFail'
    'Eem_BBS_DefectiveModeFail'
    };
Abc_CtrlEemCtrlInhibitData = CreateBus(Abc_CtrlEemCtrlInhibitData, DeList);
clear DeList;

Abc_CtrlCanRxAccelPedlInfo = Simulink.Bus;
DeList={
    'AccelPedlVal'
    'AccelPedlValErr'
    };
Abc_CtrlCanRxAccelPedlInfo = CreateBus(Abc_CtrlCanRxAccelPedlInfo, DeList);
clear DeList;

Abc_CtrlCanRxIdbInfo = Simulink.Bus;
DeList={
    'TarGearPosi'
    'GearSelDisp'
    'TcuSwiGs'
    };
Abc_CtrlCanRxIdbInfo = CreateBus(Abc_CtrlCanRxIdbInfo, DeList);
clear DeList;

Abc_CtrlCanRxEngTempInfo = Simulink.Bus;
DeList={
    'EngTemp'
    'EngTempErr'
    };
Abc_CtrlCanRxEngTempInfo = CreateBus(Abc_CtrlCanRxEngTempInfo, DeList);
clear DeList;

Abc_CtrlCanRxEscInfo = Simulink.Bus;
DeList={
    'Ax'
    'YawRate'
    'SteeringAngle'
    'Ay'
    'PbSwt'
    'ClutchSwt'
    'GearRSwt'
    'EngActIndTq'
    'EngRpm'
    'EngIndTq'
    'EngFrictionLossTq'
    'EngStdTq'
    'TurbineRpm'
    'ThrottleAngle'
    'TpsResol1000'
    'PvAvCanResol1000'
    'EngChr'
    'EngVol'
    'GearType'
    'EngClutchState'
    'EngTqCmdBeforeIntv'
    'MotEstTq'
    'MotTqCmdBeforeIntv'
    'TqIntvTCU'
    'TqIntvSlowTCU'
    'TqIncReq'
    'DecelReq'
    'RainSnsStat'
    'EngSpdErr'
    'AtType'
    'MtType'
    'CvtType'
    'DctType'
    'HevAtTcu'
    'TurbineRpmErr'
    'ThrottleAngleErr'
    'TopTrvlCltchSwtAct'
    'TopTrvlCltchSwtActV'
    'HillDesCtrlMdSwtAct'
    'HillDesCtrlMdSwtActV'
    'WiperIntSW'
    'WiperLow'
    'WiperHigh'
    'WiperValid'
    'WiperAuto'
    'TcuFaultSts'
    };
Abc_CtrlCanRxEscInfo = CreateBus(Abc_CtrlCanRxEscInfo, DeList);
clear DeList;

Abc_CtrlMotRotgAgSigInfo = Simulink.Bus;
DeList={
    'StkPosnMeasd'
    };
Abc_CtrlMotRotgAgSigInfo = CreateBus(Abc_CtrlMotRotgAgSigInfo, DeList);
clear DeList;

Abc_CtrlEscSwtStInfo = Simulink.Bus;
DeList={
    'EscDisabledBySwt'
    'TcsDisabledBySwt'
    };
Abc_CtrlEscSwtStInfo = CreateBus(Abc_CtrlEscSwtStInfo, DeList);
clear DeList;

Abc_CtrlWhlSpdInfo = Simulink.Bus;
DeList={
    'FlWhlSpd'
    'FrWhlSpd'
    'RlWhlSpd'
    'RrlWhlSpd'
    };
Abc_CtrlWhlSpdInfo = CreateBus(Abc_CtrlWhlSpdInfo, DeList);
clear DeList;

Abc_CtrlSasCalInfo = Simulink.Bus;
DeList={
    'DiagSasCaltoAppl'
    };
Abc_CtrlSasCalInfo = CreateBus(Abc_CtrlSasCalInfo, DeList);
clear DeList;

Abc_CtrlBrkPedlStatusInfo = Simulink.Bus;
DeList={
    'DrvrIntendToRelsBrk'
    };
Abc_CtrlBrkPedlStatusInfo = CreateBus(Abc_CtrlBrkPedlStatusInfo, DeList);
clear DeList;

Abc_CtrlCircPFildInfo = Simulink.Bus;
DeList={
    'PrimCircPFild_1_100Bar'
    'SecdCircPFild_1_100Bar'
    };
Abc_CtrlCircPFildInfo = CreateBus(Abc_CtrlCircPFildInfo, DeList);
clear DeList;

Abc_CtrlCircPOffsCorrdInfo = Simulink.Bus;
DeList={
    'PrimCircPOffsCorrd'
    'SecdCircPOffsCorrd'
    };
Abc_CtrlCircPOffsCorrdInfo = CreateBus(Abc_CtrlCircPOffsCorrdInfo, DeList);
clear DeList;

Abc_CtrlEstimdWhlPInfo = Simulink.Bus;
DeList={
    'FrntLeEstimdWhlP'
    'FrntRiEstimdWhlP'
    'ReLeEstimdWhlP'
    'ReRiEstimdWhlP'
    };
Abc_CtrlEstimdWhlPInfo = CreateBus(Abc_CtrlEstimdWhlPInfo, DeList);
clear DeList;

Abc_CtrlPedlSimrPOffsCorrdInfo = Simulink.Bus;
DeList={
    'PedlSimrPOffsCorrd'
    };
Abc_CtrlPedlSimrPOffsCorrdInfo = CreateBus(Abc_CtrlPedlSimrPOffsCorrdInfo, DeList);
clear DeList;

Abc_CtrlPedlTrvlOffsCorrdInfo = Simulink.Bus;
DeList={
    'PdfRawOffsCorrd'
    'PdtRawOffsCorrd'
    };
Abc_CtrlPedlTrvlOffsCorrdInfo = CreateBus(Abc_CtrlPedlTrvlOffsCorrdInfo, DeList);
clear DeList;

Abc_CtrlPistPFildInfo = Simulink.Bus;
DeList={
    'PistPFild_1_100Bar'
    };
Abc_CtrlPistPFildInfo = CreateBus(Abc_CtrlPistPFildInfo, DeList);
clear DeList;

Abc_CtrlPistPOffsCorrdInfo = Simulink.Bus;
DeList={
    'PistPOffsCorrd'
    };
Abc_CtrlPistPOffsCorrdInfo = CreateBus(Abc_CtrlPistPOffsCorrdInfo, DeList);
clear DeList;

Abc_CtrlRgnBrkCoopWithAbsInfo = Simulink.Bus;
DeList={
    'FrntSlipDetThdSlip'
    'FrntSlipDetThdUDiff'
    };
Abc_CtrlRgnBrkCoopWithAbsInfo = CreateBus(Abc_CtrlRgnBrkCoopWithAbsInfo, DeList);
clear DeList;

Abc_CtrlStkRecvryActnIfInfo = Simulink.Bus;
DeList={
    'RecommendStkRcvrLvl'
    'StkRecvryCtrlState'
    };
Abc_CtrlStkRecvryActnIfInfo = CreateBus(Abc_CtrlStkRecvryActnIfInfo, DeList);
clear DeList;

Abc_CtrlMuxCmdExecStInfo = Simulink.Bus;
DeList={
    'MuxCmdExecStOfWhlFrntLe'
    'MuxCmdExecStOfWhlFrntRi'
    'MuxCmdExecStOfWhlReLe'
    'MuxCmdExecStOfWhlReRi'
    };
Abc_CtrlMuxCmdExecStInfo = CreateBus(Abc_CtrlMuxCmdExecStInfo, DeList);
clear DeList;

Abc_CtrlTxESCSensorInfo = Simulink.Bus;
DeList={
    'EstimatedYaw'
    'EstimatedAY'
    'A_long_1_1000g'
    };
Abc_CtrlTxESCSensorInfo = CreateBus(Abc_CtrlTxESCSensorInfo, DeList);
clear DeList;

Abc_CtrlSenPwrMonitorData = Simulink.Bus;
DeList={
    'SenPwrM_12V_Stable'
    };
Abc_CtrlSenPwrMonitorData = CreateBus(Abc_CtrlSenPwrMonitorData, DeList);
clear DeList;

Abc_CtrlEemSuspectData = Simulink.Bus;
DeList={
    'Eem_Suspect_WssFL'
    'Eem_Suspect_WssFR'
    'Eem_Suspect_WssRL'
    'Eem_Suspect_WssRR'
    'Eem_Suspect_SameSideWss'
    'Eem_Suspect_DiagonalWss'
    'Eem_Suspect_FrontWss'
    'Eem_Suspect_RearWss'
    'Eem_Suspect_BBSSol'
    'Eem_Suspect_ESCSol'
    'Eem_Suspect_FrontSol'
    'Eem_Suspect_RearSol'
    'Eem_Suspect_Motor'
    'Eem_Suspect_MPS'
    'Eem_Suspect_MGD'
    'Eem_Suspect_BBSValveRelay'
    'Eem_Suspect_ESCValveRelay'
    'Eem_Suspect_ECUHw'
    'Eem_Suspect_ASIC'
    'Eem_Suspect_OverVolt'
    'Eem_Suspect_UnderVolt'
    'Eem_Suspect_LowVolt'
    'Eem_Suspect_SenPwr_12V'
    'Eem_Suspect_SenPwr_5V'
    'Eem_Suspect_Yaw'
    'Eem_Suspect_Ay'
    'Eem_Suspect_Ax'
    'Eem_Suspect_Str'
    'Eem_Suspect_CirP1'
    'Eem_Suspect_CirP2'
    'Eem_Suspect_SimP'
    'Eem_Suspect_BS'
    'Eem_Suspect_BLS'
    'Eem_Suspect_ESCSw'
    'Eem_Suspect_HDCSw'
    'Eem_Suspect_AVHSw'
    'Eem_Suspect_BrakeLampRelay'
    'Eem_Suspect_EssRelay'
    'Eem_Suspect_GearR'
    'Eem_Suspect_Clutch'
    'Eem_Suspect_ParkBrake'
    'Eem_Suspect_PedalPDT'
    'Eem_Suspect_PedalPDF'
    'Eem_Suspect_BrakeFluid'
    'Eem_Suspect_TCSTemp'
    'Eem_Suspect_HDCTemp'
    'Eem_Suspect_SCCTemp'
    'Eem_Suspect_TVBBTemp'
    'Eem_Suspect_MainCanLine'
    'Eem_Suspect_SubCanLine'
    'Eem_Suspect_EMSTimeOut'
    'Eem_Suspect_FWDTimeOut'
    'Eem_Suspect_TCUTimeOut'
    'Eem_Suspect_HCUTimeOut'
    'Eem_Suspect_MCUTimeOut'
    'Eem_Suspect_VariantCoding'
    };
Abc_CtrlEemSuspectData = CreateBus(Abc_CtrlEemSuspectData, DeList);
clear DeList;

Abc_CtrlEemEceData = Simulink.Bus;
DeList={
    'Eem_Ece_Wss'
    'Eem_Ece_Yaw'
    'Eem_Ece_Ay'
    'Eem_Ece_Ax'
    'Eem_Ece_Cir1P'
    'Eem_Ece_Cir2P'
    'Eem_Ece_SimP'
    'Eem_Ece_Bls'
    'Eem_Ece_Pedal'
    'Eem_Ece_Motor'
    'Eem_Ece_Vdc_Sw'
    'Eem_Ece_Hdc_Sw'
    'Eem_Ece_GearR_Sw'
    'Eem_Ece_Clutch_Sw'
    };
Abc_CtrlEemEceData = CreateBus(Abc_CtrlEemEceData, DeList);
clear DeList;

Abc_CtrlYAWMSerialInfo = Simulink.Bus;
DeList={
    'YAWM_SerialNumOK_Flg'
    'YAWM_SerialUnMatch_Flg'
    };
Abc_CtrlYAWMSerialInfo = CreateBus(Abc_CtrlYAWMSerialInfo, DeList);
clear DeList;

Abc_CtrlYAWMOutInfo = Simulink.Bus;
DeList={
    'YAWM_Temperature_Err'
    };
Abc_CtrlYAWMOutInfo = CreateBus(Abc_CtrlYAWMOutInfo, DeList);
clear DeList;

Abc_CtrlFinalTarPInfo = Simulink.Bus;
DeList={
    'FinalMaxCircuitTp'
    };
Abc_CtrlFinalTarPInfo = CreateBus(Abc_CtrlFinalTarPInfo, DeList);
clear DeList;

Abc_CtrlFSBbsVlvInitEndFlg = Simulink.Bus;
DeList={'Abc_CtrlFSBbsVlvInitEndFlg'};
Abc_CtrlFSBbsVlvInitEndFlg = CreateBus(Abc_CtrlFSBbsVlvInitEndFlg, DeList);
clear DeList;

Abc_CtrlEcuModeSts = Simulink.Bus;
DeList={'Abc_CtrlEcuModeSts'};
Abc_CtrlEcuModeSts = CreateBus(Abc_CtrlEcuModeSts, DeList);
clear DeList;

Abc_CtrlIgnOnOffSts = Simulink.Bus;
DeList={'Abc_CtrlIgnOnOffSts'};
Abc_CtrlIgnOnOffSts = CreateBus(Abc_CtrlIgnOnOffSts, DeList);
clear DeList;

Abc_CtrlVBatt1Mon = Simulink.Bus;
DeList={'Abc_CtrlVBatt1Mon'};
Abc_CtrlVBatt1Mon = CreateBus(Abc_CtrlVBatt1Mon, DeList);
clear DeList;

Abc_CtrlFSEscVlvInitEndFlg = Simulink.Bus;
DeList={'Abc_CtrlFSEscVlvInitEndFlg'};
Abc_CtrlFSEscVlvInitEndFlg = CreateBus(Abc_CtrlFSEscVlvInitEndFlg, DeList);
clear DeList;

Abc_CtrlDiagSci = Simulink.Bus;
DeList={'Abc_CtrlDiagSci'};
Abc_CtrlDiagSci = CreateBus(Abc_CtrlDiagSci, DeList);
clear DeList;

Abc_CtrlCanRxGearSelDispErrInfo = Simulink.Bus;
DeList={'Abc_CtrlCanRxGearSelDispErrInfo'};
Abc_CtrlCanRxGearSelDispErrInfo = CreateBus(Abc_CtrlCanRxGearSelDispErrInfo, DeList);
clear DeList;

Abc_CtrlPCtrlAct = Simulink.Bus;
DeList={'Abc_CtrlPCtrlAct'};
Abc_CtrlPCtrlAct = CreateBus(Abc_CtrlPCtrlAct, DeList);
clear DeList;

Abc_CtrlBlsSwt = Simulink.Bus;
DeList={'Abc_CtrlBlsSwt'};
Abc_CtrlBlsSwt = CreateBus(Abc_CtrlBlsSwt, DeList);
clear DeList;

Abc_CtrlEscSwt = Simulink.Bus;
DeList={'Abc_CtrlEscSwt'};
Abc_CtrlEscSwt = CreateBus(Abc_CtrlEscSwt, DeList);
clear DeList;

Abc_CtrlBrkPRednForBaseBrkCtrlr = Simulink.Bus;
DeList={'Abc_CtrlBrkPRednForBaseBrkCtrlr'};
Abc_CtrlBrkPRednForBaseBrkCtrlr = CreateBus(Abc_CtrlBrkPRednForBaseBrkCtrlr, DeList);
clear DeList;

Abc_CtrlPedlTrvlFinal = Simulink.Bus;
DeList={'Abc_CtrlPedlTrvlFinal'};
Abc_CtrlPedlTrvlFinal = CreateBus(Abc_CtrlPedlTrvlFinal, DeList);
clear DeList;

Abc_CtrlRgnBrkCtrlrActStFlg = Simulink.Bus;
DeList={'Abc_CtrlRgnBrkCtrlrActStFlg'};
Abc_CtrlRgnBrkCtrlrActStFlg = CreateBus(Abc_CtrlRgnBrkCtrlrActStFlg, DeList);
clear DeList;

Abc_CtrlFinalTarPFromPCtrl = Simulink.Bus;
DeList={'Abc_CtrlFinalTarPFromPCtrl'};
Abc_CtrlFinalTarPFromPCtrl = CreateBus(Abc_CtrlFinalTarPFromPCtrl, DeList);
clear DeList;

Abc_CtrlTarPFromBaseBrkCtrlr = Simulink.Bus;
DeList={'Abc_CtrlTarPFromBaseBrkCtrlr'};
Abc_CtrlTarPFromBaseBrkCtrlr = CreateBus(Abc_CtrlTarPFromBaseBrkCtrlr, DeList);
clear DeList;

Abc_CtrlTarPFromBrkPedl = Simulink.Bus;
DeList={'Abc_CtrlTarPFromBrkPedl'};
Abc_CtrlTarPFromBrkPedl = CreateBus(Abc_CtrlTarPFromBrkPedl, DeList);
clear DeList;

Abc_CtrlMTRInitEndFlg = Simulink.Bus;
DeList={'Abc_CtrlMTRInitEndFlg'};
Abc_CtrlMTRInitEndFlg = CreateBus(Abc_CtrlMTRInitEndFlg, DeList);
clear DeList;

Abc_CtrlWhlVlvReqAbcInfo = Simulink.Bus;
DeList={
    'FlIvReqData_array_0'
    'FlIvReqData_array_1'
    'FlIvReqData_array_2'
    'FlIvReqData_array_3'
    'FlIvReqData_array_4'
    'FlIvReqData_array_5'
    'FlIvReqData_array_6'
    'FlIvReqData_array_7'
    'FlIvReqData_array_8'
    'FlIvReqData_array_9'
    'FrIvReqData_array_0'
    'FrIvReqData_array_1'
    'FrIvReqData_array_2'
    'FrIvReqData_array_3'
    'FrIvReqData_array_4'
    'FrIvReqData_array_5'
    'FrIvReqData_array_6'
    'FrIvReqData_array_7'
    'FrIvReqData_array_8'
    'FrIvReqData_array_9'
    'RlIvReqData_array_0'
    'RlIvReqData_array_1'
    'RlIvReqData_array_2'
    'RlIvReqData_array_3'
    'RlIvReqData_array_4'
    'RlIvReqData_array_5'
    'RlIvReqData_array_6'
    'RlIvReqData_array_7'
    'RlIvReqData_array_8'
    'RlIvReqData_array_9'
    'RrIvReqData_array_0'
    'RrIvReqData_array_1'
    'RrIvReqData_array_2'
    'RrIvReqData_array_3'
    'RrIvReqData_array_4'
    'RrIvReqData_array_5'
    'RrIvReqData_array_6'
    'RrIvReqData_array_7'
    'RrIvReqData_array_8'
    'RrIvReqData_array_9'
    'FlOvReqData_array_0'
    'FlOvReqData_array_1'
    'FlOvReqData_array_2'
    'FlOvReqData_array_3'
    'FlOvReqData_array_4'
    'FlOvReqData_array_5'
    'FlOvReqData_array_6'
    'FlOvReqData_array_7'
    'FlOvReqData_array_8'
    'FlOvReqData_array_9'
    'FrOvReqData_array_0'
    'FrOvReqData_array_1'
    'FrOvReqData_array_2'
    'FrOvReqData_array_3'
    'FrOvReqData_array_4'
    'FrOvReqData_array_5'
    'FrOvReqData_array_6'
    'FrOvReqData_array_7'
    'FrOvReqData_array_8'
    'FrOvReqData_array_9'
    'RlOvReqData_array_0'
    'RlOvReqData_array_1'
    'RlOvReqData_array_2'
    'RlOvReqData_array_3'
    'RlOvReqData_array_4'
    'RlOvReqData_array_5'
    'RlOvReqData_array_6'
    'RlOvReqData_array_7'
    'RlOvReqData_array_8'
    'RlOvReqData_array_9'
    'RrOvReqData_array_0'
    'RrOvReqData_array_1'
    'RrOvReqData_array_2'
    'RrOvReqData_array_3'
    'RrOvReqData_array_4'
    'RrOvReqData_array_5'
    'RrOvReqData_array_6'
    'RrOvReqData_array_7'
    'RrOvReqData_array_8'
    'RrOvReqData_array_9'
    'FlIvReq'
    'FrIvReq'
    'RlIvReq'
    'RrIvReq'
    'FlOvReq'
    'FrOvReq'
    'RlOvReq'
    'RrOvReq'
    'FlIvDataLen'
    'FrIvDataLen'
    'RlIvDataLen'
    'RrIvDataLen'
    'FlOvDataLen'
    'FrOvDataLen'
    'RlOvDataLen'
    'RrOvDataLen'
    };
Abc_CtrlWhlVlvReqAbcInfo = CreateBus(Abc_CtrlWhlVlvReqAbcInfo, DeList);
clear DeList;

Abc_CtrlCanTxInfo = Simulink.Bus;
DeList={
    'TqIntvTCS'
    'TqIntvMsr'
    'TqIntvSlowTCS'
    'MinGear'
    'MaxGear'
    'TcsReq'
    'TcsCtrl'
    'AbsAct'
    'TcsGearShiftChr'
    'EspCtrl'
    'MsrReq'
    'TcsProductInfo'
    };
Abc_CtrlCanTxInfo = CreateBus(Abc_CtrlCanTxInfo, DeList);
clear DeList;

Abc_CtrlAbsCtrlInfo = Simulink.Bus;
DeList={
    'AbsActFlg'
    'AbsDefectFlg'
    'AbsTarPFrntLe'
    'AbsTarPFrntRi'
    'AbsTarPReLe'
    'AbsTarPReRi'
    'FrntWhlSlip'
    'AbsDesTarP'
    'AbsDesTarPReqFlg'
    'AbsCtrlFadeOutFlg'
    'AbsCtrlModeFrntLe'
    'AbsCtrlModeFrntRi'
    'AbsCtrlModeReLe'
    'AbsCtrlModeReRi'
    'AbsTarPRateFrntLe'
    'AbsTarPRateFrntRi'
    'AbsTarPRateReLe'
    'AbsTarPRateReRi'
    'AbsPrioFrntLe'
    'AbsPrioFrntRi'
    'AbsPrioReLe'
    'AbsPrioReRi'
    'AbsDelTarPFrntLe'
    'AbsDelTarPFrntRi'
    'AbsDelTarPReLe'
    'AbsDelTarPReRi'
    };
Abc_CtrlAbsCtrlInfo = CreateBus(Abc_CtrlAbsCtrlInfo, DeList);
clear DeList;

Abc_CtrlAvhCtrlInfo = Simulink.Bus;
DeList={
    'AvhActFlg'
    'AvhDefectFlg'
    'AvhTarP'
    };
Abc_CtrlAvhCtrlInfo = CreateBus(Abc_CtrlAvhCtrlInfo, DeList);
clear DeList;

Abc_CtrlBaCtrlInfo = Simulink.Bus;
DeList={
    'BaActFlg'
    'BaCDefectFlg'
    'BaTarP'
    };
Abc_CtrlBaCtrlInfo = CreateBus(Abc_CtrlBaCtrlInfo, DeList);
clear DeList;

Abc_CtrlEbdCtrlInfo = Simulink.Bus;
DeList={
    'EbdActFlg'
    'EbdDefectFlg'
    'EbdTarPFrntLe'
    'EbdTarPFrntRi'
    'EbdTarPReLe'
    'EbdTarPReRi'
    'EbdTarPRateReLe'
    'EbdTarPRateReRi'
    'EbdCtrlModeReLe'
    'EbdCtrlModeReRi'
    'EbdDelTarPReLe'
    'EbdDelTarPReRi'
    };
Abc_CtrlEbdCtrlInfo = CreateBus(Abc_CtrlEbdCtrlInfo, DeList);
clear DeList;

Abc_CtrlEbpCtrlInfo = Simulink.Bus;
DeList={
    'EbpActFlg'
    'EbpDefectFlg'
    'EbpTarP'
    };
Abc_CtrlEbpCtrlInfo = CreateBus(Abc_CtrlEbpCtrlInfo, DeList);
clear DeList;

Abc_CtrlEpbiCtrlInfo = Simulink.Bus;
DeList={
    'EpbiActFlg'
    'EpbiDefectFlg'
    'EpbiTarP'
    };
Abc_CtrlEpbiCtrlInfo = CreateBus(Abc_CtrlEpbiCtrlInfo, DeList);
clear DeList;

Abc_CtrlEscCtrlInfo = Simulink.Bus;
DeList={
    'EscActFlg'
    'EscDefectFlg'
    'EscTarPFrntLe'
    'EscTarPFrntRi'
    'EscTarPReLe'
    'EscTarPReRi'
    'EscCtrlModeFrntLe'
    'EscCtrlModeFrntRi'
    'EscCtrlModeReLe'
    'EscCtrlModeReRi'
    'EscTarPRateFrntLe'
    'EscTarPRateFrntRi'
    'EscTarPRateReLe'
    'EscTarPRateReRi'
    'EscPrioFrntLe'
    'EscPrioFrntRi'
    'EscPrioReLe'
    'EscPrioReRi'
    'EscPreCtrlModeFrntLe'
    'EscPreCtrlModeFrntRi'
    'EscPreCtrlModeReLe'
    'EscPreCtrlModeReRi'
    'EscDelTarPFrntLe'
    'EscDelTarPFrntRi'
    'EscDelTarPReLe'
    'EscDelTarPReRi'
    };
Abc_CtrlEscCtrlInfo = CreateBus(Abc_CtrlEscCtrlInfo, DeList);
clear DeList;

Abc_CtrlHdcCtrlInfo = Simulink.Bus;
DeList={
    'HdcActFlg'
    'HdcDefectFlg'
    'HdcTarP'
    };
Abc_CtrlHdcCtrlInfo = CreateBus(Abc_CtrlHdcCtrlInfo, DeList);
clear DeList;

Abc_CtrlHsaCtrlInfo = Simulink.Bus;
DeList={
    'HsaActFlg'
    'HsaDefectFlg'
    'HsaTarP'
    };
Abc_CtrlHsaCtrlInfo = CreateBus(Abc_CtrlHsaCtrlInfo, DeList);
clear DeList;

Abc_CtrlSccCtrlInfo = Simulink.Bus;
DeList={
    'SccActFlg'
    'SccDefectFlg'
    'SccTarP'
    };
Abc_CtrlSccCtrlInfo = CreateBus(Abc_CtrlSccCtrlInfo, DeList);
clear DeList;

Abc_CtrlStkRecvryCtrlIfInfo = Simulink.Bus;
DeList={
    'StkRcvrEscAllowedTime'
    'StkRcvrAbsAllowedTime'
    'StkRcvrTcsAllowedTime'
    };
Abc_CtrlStkRecvryCtrlIfInfo = CreateBus(Abc_CtrlStkRecvryCtrlIfInfo, DeList);
clear DeList;

Abc_CtrlTcsCtrlInfo = Simulink.Bus;
DeList={
    'TcsActFlg'
    'TcsDefectFlg'
    'TcsTarPFrntLe'
    'TcsTarPFrntRi'
    'TcsTarPReLe'
    'TcsTarPReRi'
    'BothDrvgWhlBrkCtlReqFlg'
    'BrkCtrlFctFrntLeByWspc'
    'BrkCtrlFctFrntRiByWspc'
    'BrkCtrlFctReLeByWspc'
    'BrkCtrlFctReRiByWspc'
    'BrkTqGrdtReqFrntLeByWspc'
    'BrkTqGrdtReqFrntRiByWspc'
    'BrkTqGrdtReqReLeByWspc'
    'BrkTqGrdtReqReRiByWspc'
    'TcsDelTarPFrntLe'
    'TcsDelTarPFrntRi'
    'TcsDelTarPReLe'
    'TcsDelTarPReRi'
    };
Abc_CtrlTcsCtrlInfo = CreateBus(Abc_CtrlTcsCtrlInfo, DeList);
clear DeList;

Abc_CtrlTvbbCtrlInfo = Simulink.Bus;
DeList={
    'TvbbActFlg'
    'TvbbDefectFlg'
    'TvbbTarP'
    };
Abc_CtrlTvbbCtrlInfo = CreateBus(Abc_CtrlTvbbCtrlInfo, DeList);
clear DeList;

Abc_CtrlFunctionLamp = Simulink.Bus;
DeList={'Abc_CtrlFunctionLamp'};
Abc_CtrlFunctionLamp = CreateBus(Abc_CtrlFunctionLamp, DeList);
clear DeList;

Abc_CtrlActvBrkCtrlrActFlg = Simulink.Bus;
DeList={'Abc_CtrlActvBrkCtrlrActFlg'};
Abc_CtrlActvBrkCtrlrActFlg = CreateBus(Abc_CtrlActvBrkCtrlrActFlg, DeList);
clear DeList;

Abc_CtrlAy = Simulink.Bus;
DeList={'Abc_CtrlAy'};
Abc_CtrlAy = CreateBus(Abc_CtrlAy, DeList);
clear DeList;

Abc_CtrlVehSpd = Simulink.Bus;
DeList={'Abc_CtrlVehSpd'};
Abc_CtrlVehSpd = CreateBus(Abc_CtrlVehSpd, DeList);
clear DeList;

