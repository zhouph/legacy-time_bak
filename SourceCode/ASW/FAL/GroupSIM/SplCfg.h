#ifndef __SPLCFG_H__
#define __SPLCFG_H__

#define FAL_PATH(_FILENAME_)		                STRINGIFY(../_FILENAME_)
#define ASW_PATH(_FILENAME_)		                STRINGIFY(../../_FILENAME_)
#define IDBSignalProcessing_PATH(_FILENAME_)		STRINGIFY(../IDBSignalProcessing/_FILENAME_)
#define IDBDetection_PATH(_FILENAME_)		        STRINGIFY(../IDBDetection/_FILENAME_)
#define BBC_PATH(_FILENAME_)                        STRINGIFY(../BaseBrakeControl/_FILENAME_)
#define IDBTargetPressArbitrator_PATH(_FILENAME_)   STRINGIFY(../IDBTargetPressArbitrator/_FILENAME_)
#define RBC_PATH(_FILENAME_)                        STRINGIFY(../RegenBrakeCooperation/_FILENAME_)
#define PressureControl_PATH(_FILENAME_)            STRINGIFY(../PressureControl/_FILENAME_)
#define IDBValveActuation_PATH(_FILENAME_)          STRINGIFY(../IDBValveActuation/_FILENAME_)
#define MotorCurrentControl_PATH(_FILENAME_)        STRINGIFY(../MotorCurrentControl/_FILENAME_)


#define LIB_PATH(_FILENAME_)		                STRINGIFY(../../../LIB/_FILENAME_)
#define RTE_PATH(_FILENAME_)		                STRINGIFY(../../../RTE/_FILENAME_)



#include RTE_PATH(Rte_Asw.c)
#include LIB_PATH(SwcCommonFunction.c)
#include LIB_PATH(L_CommonFunction.c)



#include IDBDetection_PATH(SRC/IDBDetection.c)
#include IDBDetection_PATH(CAL/IDBDetection_Cal.c)
#include IDBDetection_PATH(SRC/LDIDB_DetVehicleState.c)
#include IDBDetection_PATH(SRC/LDIDB_DecidePedalTravel.c)
#include IDBDetection_PATH(SRC/LDIDB_CalcSignalChangeRate.c)

#include IDBSignalProcessing_PATH(SRC/IDBSignalProcessing.c)
#include IDBSignalProcessing_PATH(CAL/IDBSignalProcessing_Cal.c)
#include IDBSignalProcessing_PATH(SRC/LSIDB_CalcSensorSignalOffset.c)
#include IDBSignalProcessing_PATH(SRC/LSIDB_FilterSensorSignal.c)

#include BBC_PATH(SRC/BaseBrakeControl.c)
#include BBC_PATH(CAL/BaseBrakeControl_Cal.c)
#include BBC_PATH(SRC/LCBBC_DecideCtrlSt.c)
#include BBC_PATH(SRC/LCBBC_DecidePedlSimrPGen.c)
#include BBC_PATH(SRC/LCBBC_DecideVehSts.c)
#include BBC_PATH(SRC/LCBBC_EstDrvrIntdTP.c)
#include BBC_PATH(SRC/LCBBC_LimBrkTP.c)


#include IDBTargetPressArbitrator_PATH(SRC/IDBTargetPressArbitrator.c)
#include IDBTargetPressArbitrator_PATH(CAL/IDBTargetPressArbitrator_Cal.c)
#include IDBTargetPressArbitrator_PATH(SRC/LCIDB_ArbitrateTargetPress.c)

#include RBC_PATH(SRC/RegenBrakeCooperation.c)
#include RBC_PATH(CAL/RegenBrakeCooperation_Cal.c)
#include RBC_PATH(SRC/RBC_DetControlState.c)
#include RBC_PATH(SRC/RBC_CalcPossibleRegenTq.c)
#include RBC_PATH(SRC/LSRBC_ConvActualRegenTqToPress.c)

#include PressureControl_PATH(SRC/PressureControl.c)
#include PressureControl_PATH(CAL/PressureControl_Cal.c)
#include PressureControl_PATH(SRC/LAIDB_DecideMtrCtrlMode.c)
#include PressureControl_PATH(SRC/LAIDB_DecidePressCtrlMode.c)
#include PressureControl_PATH(SRC/LAIDB_DecideValveCtrlMode.c)
#include PressureControl_PATH(SRC/LAIDB_LoadMapData.c)

#include IDBValveActuation_PATH(SRC/IDBValveActuation.c)
#include IDBValveActuation_PATH(CAL/IDBValveActuation_Cal.c)
#include IDBValveActuation_PATH(SRC/LAIDB_DecideValveActuation.c)

#include MotorCurrentControl_PATH(SRC/MotorCurrentControl.c)
#include MotorCurrentControl_PATH(CAL/MotorCurrentControl_Cal.c)
#include MotorCurrentControl_PATH(SRC/LAMTR_CalcCurrentRef.c)
#include MotorCurrentControl_PATH(SRC/LAMTR_DecideMtrCtrlMode.c)
#include MotorCurrentControl_PATH(SRC/LAMTR_SigProc.c)
#include MotorCurrentControl_PATH(SRC/mclib_functions.c)
#endif
