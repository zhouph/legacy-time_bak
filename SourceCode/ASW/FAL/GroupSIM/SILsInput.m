clc
clear all
close all

load ABS_01.mat
AY_SCALE=1000;
VRAD_SCALE=8;
PEDAL_SCALE=10;
PRESS_SCALE=100;
%ARAD_SCALE=4;
b=length(Aalat);
Ts=0.005;
    % Active Brake Input(53) ********************************************************************/
    
    
    ActvBrkCtrlrIfInfo.Ay = Aalat*AY_SCALE;%input[0];
    ActvBrkCtrlrIfInfo.ActvBrkCtrlrActFlg = EscActiveFlg;%input[1];
    ActvBrkCtrlrIfInfo.StkRecvryCtrlIfInfo.StkRecvryCtrlReq = StrkRecvrCtrlReq;%input[2];

    ActvBrkCtrlrIfInfo.EbdCtrlInfo.EbdActFlg = EbdAct;%input[3];
    ActvBrkCtrlrIfInfo.EbdCtrlInfo.EbdDefectFlg = zeros([1 b]);%input[4];
    ActvBrkCtrlrIfInfo.EbdCtrlInfo.EbdTarPFrntLe =zeros([1 b]);% input[5];
    ActvBrkCtrlrIfInfo.EbdCtrlInfo.EbdTarPFrntRi = zeros([1 b]);%input[6];
    ActvBrkCtrlrIfInfo.EbdCtrlInfo.EbdTarPReLe = zeros([1 b]);%input[7];
    ActvBrkCtrlrIfInfo.EbdCtrlInfo.EbdTarPReRi = zeros([1 b]);%input[8];

    inp_vec=find(EbdAct==1);
    ActvBrkCtrlrIfInfo.EbdCtrlInfo.EbdTarPFrntLe(inp_vec)=TP_out(inp_vec);
    ActvBrkCtrlrIfInfo.EbdCtrlInfo.EbdTarPFrntRi(inp_vec)=TP_out(inp_vec);
    ActvBrkCtrlrIfInfo.EbdCtrlInfo.EbdTarPReLe(inp_vec)=TP_out(inp_vec);
    ActvBrkCtrlrIfInfo.EbdCtrlInfo.EbdTarPReRi(inp_vec)=TP_out(inp_vec);
             
    ActvBrkCtrlrIfInfo.AbsCtrlInfo.AbsActFlg = AABSfz;%input[9];
    ActvBrkCtrlrIfInfo.AbsCtrlInfo.AbsDefectFlg = zeros([1 b]);%input[10];
    ActvBrkCtrlrIfInfo.AbsCtrlInfo.AbsTarPFrntLe = zeros([1 b]);%input[11];
    ActvBrkCtrlrIfInfo.AbsCtrlInfo.AbsTarPFrntRi = zeros([1 b]);%input[12];
    ActvBrkCtrlrIfInfo.AbsCtrlInfo.AbsTarPReLe = zeros([1 b]);%input[13];
    ActvBrkCtrlrIfInfo.AbsCtrlInfo.AbsTarPReRi = zeros([1 b]);%input[14];
    ActvBrkCtrlrIfInfo.AbsCtrlInfo.FrntWhlSlip = zeros([1 b]);%input[15];
    
    inp_vec=find(AABSfz==1);
    ActvBrkCtrlrIfInfo.AbsCtrlInfo.AbsTarPFrntLe(inp_vec)=TP_out(inp_vec);
    ActvBrkCtrlrIfInfo.AbsCtrlInfo.AbsTarPFrntRi(inp_vec)=TP_out(inp_vec);
    ActvBrkCtrlrIfInfo.AbsCtrlInfo.AbsTarPReLe(inp_vec)=TP_out(inp_vec);
    ActvBrkCtrlrIfInfo.AbsCtrlInfo.AbsTarPReRi(inp_vec)=TP_out(inp_vec);
    
    
 	s16LeftWheelSpdDiff = AvrdRL - AvrdFL;
 	s16RightWheelSpdDiff = AvrdRR - AvrdFR;
    VREF_30_KPH=30;
    ARAD_1G0=1;
    S8_FRONT_SLIP_DET_THRES_SLIP=5;
    S8_FRONT_SLIP_DET_THRES_VDIFF=20/8;
    for n=1:b
        if n==1
            lcabsu1FrontWheelSlip(n)=0;       
        else
            lcabsu1FrontWheelSlip(n)=lcabsu1FrontWheelSlip(n-1);
        	if((Alcabsu1BrakeSensorOn(n)==1) && (lcabsu1FrontWheelSlip(n)==0))
	
                if(AardFL(n)<=-ARAD_1G0)

                    if((s16LeftWheelSpdDiff(n)>=S8_FRONT_SLIP_DET_THRES_VDIFF) || ((Avref(n) >= VREF_30_KPH) && (ArelFL(n)<=-S8_FRONT_SLIP_DET_THRES_SLIP)))

                        lcabsu1FrontWheelSlip(n) = 1;
                    end
                end
% 
%                 if(AardFR(n)<=-ARAD_1G0)
%                     if((s16RightWheelSpdDiff(n)>=S8_FRONT_SLIP_DET_THRES_VDIFF) || ((Avref(n) >= VREF_30_KPH) && (ArelFR(n)<=-S8_FRONT_SLIP_DET_THRES_SLIP)))
% 
%                         lcabsu1FrontWheelSlip(n) = 1;
%                     end
%                 end
	
            elseif(Alcabsu1BrakeSensorOn(n)==0)
	
                lcabsu1FrontWheelSlip(n) = 0;
            else
            end
        end
    end


    ActvBrkCtrlrIfInfo.TcsCtrlInfo.TcsActFlg = TcsCtl;%input[16];
    ActvBrkCtrlrIfInfo.TcsCtrlInfo.TcsDefectFlg = zeros([1 b]);%input[17];
    ActvBrkCtrlrIfInfo.TcsCtrlInfo.TcsTarPFrntLe = zeros([1 b]);%input[18];
    ActvBrkCtrlrIfInfo.TcsCtrlInfo.TcsTarPFrntRi =zeros([1 b]);% input[19];
    ActvBrkCtrlrIfInfo.TcsCtrlInfo.TcsTarPReLe = zeros([1 b]);%input[20];
    ActvBrkCtrlrIfInfo.TcsCtrlInfo.TcsTarPReRi = zeros([1 b]);%input[21];
    
    inp_vec=find(TcsCtl==1);
    ActvBrkCtrlrIfInfo.TcsCtrlInfo.TcsTarPFrntLe(inp_vec)=TP_out(inp_vec);
    ActvBrkCtrlrIfInfo.TcsCtrlInfo.TcsTarPFrntRi(inp_vec)=TP_out(inp_vec);
    ActvBrkCtrlrIfInfo.TcsCtrlInfo.TcsTarPReLe(inp_vec)=TP_out(inp_vec);
    ActvBrkCtrlrIfInfo.TcsCtrlInfo.TcsTarPReRi(inp_vec)=TP_out(inp_vec);
    
    ActvBrkCtrlrIfInfo.EscCtrlInfo.EscActFlg = EscCtl;%;input[22];
    ActvBrkCtrlrIfInfo.EscCtrlInfo.EscDefectFlg = zeros([1 b]);%input[23];
    ActvBrkCtrlrIfInfo.EscCtrlInfo.EscTarPFrntLe = zeros([1 b]);%input[24];
    ActvBrkCtrlrIfInfo.EscCtrlInfo.EscTarPFrntRi = zeros([1 b]);%input[25];
    ActvBrkCtrlrIfInfo.EscCtrlInfo.EscTarPReLe = zeros([1 b]);%input[26];
    ActvBrkCtrlrIfInfo.EscCtrlInfo.EscTarPReRi =zeros([1 b]);% input[27];

    inp_vec=find(EscCtl==1);
    ActvBrkCtrlrIfInfo.EscCtrlInfo.EscTarPFrntLe(inp_vec)=TP_out(inp_vec);
    ActvBrkCtrlrIfInfo.EscCtrlInfo.EscTarPFrntRi(inp_vec)=TP_out(inp_vec);
    ActvBrkCtrlrIfInfo.EscCtrlInfo.EscTarPReLe(inp_vec)=TP_out(inp_vec);
    ActvBrkCtrlrIfInfo.EscCtrlInfo.EscTarPReRi(inp_vec)=TP_out(inp_vec);
    
    
    ActvBrkCtrlrIfInfo.BaCtrlInfo.BaActFlg = zeros([1 b]);%input[28];
    ActvBrkCtrlrIfInfo.BaCtrlInfo.BaCDefectFlg =zeros([1 b]);% input[29];
    ActvBrkCtrlrIfInfo.BaCtrlInfo.BaTarP =zeros([1 b]);% input[30];

    ActvBrkCtrlrIfInfo.HsaCtrlInfo.HsaActFlg = zeros([1 b]);%input[31];
    ActvBrkCtrlrIfInfo.HsaCtrlInfo.HsaDefectFlg = zeros([1 b]);%input[32];
    ActvBrkCtrlrIfInfo.HsaCtrlInfo.HsaTarP = zeros([1 b]);%input[33];

    ActvBrkCtrlrIfInfo.AvhCtrlInfo.AvhActFlg = zeros([1 b]);%input[34];
    ActvBrkCtrlrIfInfo.AvhCtrlInfo.AvhDefectFlg = zeros([1 b]);%input[35];
    ActvBrkCtrlrIfInfo.AvhCtrlInfo.AvhTarP = zeros([1 b]);%input[36];

    ActvBrkCtrlrIfInfo.HdcCtrlInfo.HdcActFlg = zeros([1 b]);%input[37];
    ActvBrkCtrlrIfInfo.HdcCtrlInfo.HdcDefectFlg =zeros([1 b]);% input[38];
    ActvBrkCtrlrIfInfo.HdcCtrlInfo.HdcTarP = zeros([1 b]);%input[39];

    ActvBrkCtrlrIfInfo.SccCtrlInfo.SccActFlg = zeros([1 b]);%input[40];
    ActvBrkCtrlrIfInfo.SccCtrlInfo.SccDefectFlg =zeros([1 b]);% input[41];
    ActvBrkCtrlrIfInfo.SccCtrlInfo.SccTarP = zeros([1 b]);%input[42];

    ActvBrkCtrlrIfInfo.TvbbCtrlInfo.TvbbActFlg = zeros([1 b]);%input[43];
    ActvBrkCtrlrIfInfo.TvbbCtrlInfo.TvbbDefectFlg =zeros([1 b]);% input[44];
    ActvBrkCtrlrIfInfo.TvbbCtrlInfo.TvbbTarP =zeros([1 b]);% input[45];

    ActvBrkCtrlrIfInfo.EpbiCtrlInfo.EpbiActFlg = zeros([1 b]);%input[46];
    ActvBrkCtrlrIfInfo.EpbiCtrlInfo.EpbiDefectFlg = zeros([1 b]);%input[47];
    ActvBrkCtrlrIfInfo.EpbiCtrlInfo.EpbiTarP = zeros([1 b]);%input[48];

    ActvBrkCtrlrIfInfo.EbpCtrlInfo.EbpActFlg = zeros([1 b]);%input[49];
    ActvBrkCtrlrIfInfo.EbpCtrlInfo.EbpDefectFlg = zeros([1 b]);%input[50];
    ActvBrkCtrlrIfInfo.EbpCtrlInfo.EbpTarP = zeros([1 b]);%input[51];

    ActvBrkCtrlrIfInfo.VehSpd = Avref*VRAD_SCALE;%input[52];

    % IDB Motor Control(8) ************************************************************************/
    IdbMotCtrlrIfInfo.MotFbAcIPhaA = zeros([1 b]);%input[53];
    IdbMotCtrlrIfInfo.MotFbAcIPhaB = zeros([1 b]);%input[54];
    IdbMotCtrlrIfInfo.MotFbAcIPhaC = zeros([1 b]);%input[55];

    IdbMotCtrlrIfInfo.MotElecRotgAg = zeros([1 b]);%input[56];
    IdbMotCtrlrIfInfo.MotMeclAngle = zeros([1 b]);%input[57];
    IdbMotCtrlrIfInfo.MotCtrlrStInfo.TBD = zeros([1 b]);%input[58];

    IdbMotCtrlrIfInfo.VdcLinkRaw =VDClink;% input[59];
    IdbMotCtrlrIfInfo.VIvtrDcLink =zeros([1 b]) ;%input[60];

    % Pedal Travel Offset EEP(12) *********************************************************************/
%     PedlTrvlOffsEepInfo.PdtOffsEolWrReqCmpldFlg = input[61];
%     PedlTrvlOffsEepInfo.PdtOffsEolReadInvldFlg = input[62];
%     PedlTrvlOffsEepInfo.PdtOffsEolReadVal = input[63];
% 
%     PedlTrvlOffsEepInfo.PdfOffsEolWrReqCmpldFlg = input[64];
%     PedlTrvlOffsEepInfo.PdfOffsEolReadInvldFlg = input[65];
%     PedlTrvlOffsEepInfo.PdfOffsEolReadVal = input[66];
% 
%     PedlTrvlOffsEepInfo.PdtOffsDrvgWrReqCmpldFlg = input[67];
%     PedlTrvlOffsEepInfo.PdtOffsDrvgReadInvldFlg = input[68];
%     PedlTrvlOffsEepInfo.PdtOffsDrvgReadVal = input[69];
% 
%     PedlTrvlOffsEepInfo.PdfOffsDrvgWrReqCmpldFlg = input[70];
%     PedlTrvlOffsEepInfo.PdfOffsDrvgReadInvldFlg = input[71];
%     PedlTrvlOffsEepInfo.PdfOffsDrvgReadVal = input[72];

    % Pressure Offset EEP(24) **********************************************************************/
%     PedlSimrPOffsEepInfo.PedlSimrPOffsEolWrReqCmpldFlg = input[73];
%     PedlSimrPOffsEepInfo.PedlSimrPOffsEolReadInvldFlg = input[74];
%     PedlSimrPOffsEepInfo.PedlSimrPOffsEolReadVal = input[75];
% 
%     PedlSimrPOffsEepInfo.PedlSimrPOffsDrvgWrReqCmpldFlg = input[76];
%     PedlSimrPOffsEepInfo.PedlSimrPOffsDrvgReadInvldFlg = input[77];
%     PedlSimrPOffsEepInfo.PedlSimrPOffsDrvgReadVal = input[78];
% 
%     PistPOffsEepInfo.PistPOffsEolWrReqCmpldFlg = input[79];
%     PistPOffsEepInfo.PistPOffsEolReadInvldFlg = input[80];
%     PistPOffsEepInfo.PistPOffsEolReadVal = input[81];
% 
%     PistPOffsEepInfo.PistPOffsDrvgWrReqCmpldFlg = input[82];
%     PistPOffsEepInfo.PistPOffsDrvgReadInvldFlg = input[83];
%     PistPOffsEepInfo.PistPOffsDrvgReadVal = input[84];
% 
%     CircPOffsEepInfo.PrimCircPOffsEolWrReqCmpldFlg = input[85];
%     CircPOffsEepInfo.PrimCircPOffsEolReadInvldFlg = input[86];
%     CircPOffsEepInfo.PrimCircPOffsEolReadVal = input[87];
% 
%     CircPOffsEepInfo.PrimCircPOffsDrvgWrReqCmpldFlg = input[88];
%     CircPOffsEepInfo.PrimCircPOffsDrvgReadInvldFlg = input[89];
%     CircPOffsEepInfo.PrimCircPOffsDrvgReadVal = input[90];
% 
%     CircPOffsEepInfo.SecdCircPOffsEolWrReqCmpldFlg = input[91];
%     CircPOffsEepInfo.SecdCircPOffsEolReadInvldFlg = input[92];
%     CircPOffsEepInfo.SecdCircPOffsEolReadVal = input[93];
% 
%     CircPOffsEepInfo.SecdCircPOffsDrvgWrReqCmpldFlg = input[94];
%     CircPOffsEepInfo.SecdCircPOffsDrvgReadInvldFlg = input[95];
%     CircPOffsEepInfo.SecdCircPOffsDrvgReadVal = input[96];
% 
    % Snsr Invalid flg
%     PedlTrvlSigInvldInfo.PdtSigFaultFlg = input[103];
%     PedlTrvlSigInvldInfo.PdtSigSuspcFlg = input[104];
%     PedlTrvlSigInvldInfo.PdfSigFaultFlg = input[106];
%     PedlTrvlSigInvldInfo.PdfSigSuspcFlg = input[107];  
%     PedlSimrPInvldInfo.PedlSimrPSigFaultFlg = input[109];
%     PedlSimrPInvldInfo.PedlSimrPSigSuspcFlg = input[110]; 
%     PistPInvldInfo.PistPSigFaultFlg = input[117];
%     PistPInvldInfo.PistPSigSuspcFlg = input[118];
%     CircPInvldInfo.PrimCircPSigFaultFlg = input[125];
%     CircPInvldInfo.PrimCircPSigSuspcFlg = input[126];
%     CircPInvldInfo.SecdCircPSigFaultFlg = input[133];
%     CircPInvldInfo.SecdCircPSigSuspcFlg = input[134];
%     
%     WhlSpdInvldInfo.WhlSpdInvldFrntLe = input[139];
%     WhlSpdInvldInfo.WhlSpdInvldFrntRi = input[140];
%     WhlSpdInvldInfo.WhlSpdInvldReLe = input[141];
%     WhlSpdInvldInfo.WhlSpdInvldReRi = input[142];
%     
%     BlsInvldInfo.BlsSigFault = input[146];
%     BlsInvldInfo.BlsSigSuspc = input[147];
    
    % Pedal/Pressure Sensor Raw(26) *************************************************************************************/

    PedlTrvlRawInfo.PdtRaw = PedalTravelFilt*PEDAL_SCALE;%input[97];
    PedalDiff=PedalTravelFilt(2:end)-PedalTravelFilt(1:end-1);
   PedalDiff=[PedalDiff;0];
    PedlTrvlRawInfo.PdtRaw1ms0 = (PedalTravelFilt+PedalDiff*0/5)*PEDAL_SCALE;%input[98];
    PedlTrvlRawInfo.PdtRaw1ms1 = (PedalTravelFilt+PedalDiff*1/5)*PEDAL_SCALE;%input[99];
    PedlTrvlRawInfo.PdtRaw1ms2 = (PedalTravelFilt+PedalDiff*2/5)*PEDAL_SCALE;%input[100];
    PedlTrvlRawInfo.PdtRaw1ms3 = (PedalTravelFilt+PedalDiff*3/5)*PEDAL_SCALE;%input[101];
    PedlTrvlRawInfo.PdtRaw1ms4 = (PedalTravelFilt+PedalDiff*4/5)*PEDAL_SCALE;%input[102];

    PedlTrvlRawInfo.PdfRaw = PedalTravelFilt*PEDAL_SCALE;%input[105];

    PedlSimrPRaw = PSpresFilt*PRESS_SCALE;%input[108];

    PistPRawInfo.PistPRaw = CircuitPress1x100*PRESS_SCALE;%input[111];
    PressDiff=CircuitPress1x100(2:end)-CircuitPress1x100(1:end-1);
    PressDiff=[PressDiff;0];
    PistPRawInfo.PistPRaw1ms0 = (CircuitPress1x100+PressDiff*0/5)*PRESS_SCALE;%input[112];
    PistPRawInfo.PistPRaw1ms1 = (CircuitPress1x100+PressDiff*1/5)*PRESS_SCALE;%input[113];
    PistPRawInfo.PistPRaw1ms2 = (CircuitPress1x100+PressDiff*2/5)*PRESS_SCALE;%input[114];
    PistPRawInfo.PistPRaw1ms3 = (CircuitPress1x100+PressDiff*3/5)*PRESS_SCALE;%input[115];
    PistPRawInfo.PistPRaw1ms4 = (CircuitPress1x100+PressDiff*4/5)*PRESS_SCALE;%input[116];

    CircPRawInfo.PrimCircPRaw = CircuitPress1x100*PRESS_SCALE;%input[119];
    CircPRawInfo.PrimCircPRaw1ms0 = (CircuitPress1x100+PressDiff*0/5)*PRESS_SCALE;%input[120];
    CircPRawInfo.PrimCircPRaw1ms1 = (CircuitPress1x100+PressDiff*1/5)*PRESS_SCALE;%input[121];
    CircPRawInfo.PrimCircPRaw1ms2 = (CircuitPress1x100+PressDiff*2/5)*PRESS_SCALE;%input[122];
    CircPRawInfo.PrimCircPRaw1ms3 = (CircuitPress1x100+PressDiff*3/5)*PRESS_SCALE;%input[123];
    CircPRawInfo.PrimCircPRaw1ms4 = (CircuitPress1x100+PressDiff*4/5)*PRESS_SCALE;%input[124];

    
    CircPRawInfo.SecdCircPRaw = CircuitPress2x100*PRESS_SCALE;%input[127];
    
    PressDiff2=CircuitPress2x100(2:end)-CircuitPress2x100(1:end-1);
    PressDiff2=[PressDiff2;0];
    CircPRawInfo.SecdCircPRaw1ms0 = (CircuitPress2x100+PressDiff2*0/5)*PRESS_SCALE;%input[128];
    CircPRawInfo.SecdCircPRaw1ms1 = (CircuitPress2x100+PressDiff2*1/5)*PRESS_SCALE;%input[129];
    CircPRawInfo.SecdCircPRaw1ms2 = (CircuitPress2x100+PressDiff2*2/5)*PRESS_SCALE;% input[130];
    CircPRawInfo.SecdCircPRaw1ms3 = (CircuitPress2x100+PressDiff2*3/5)*PRESS_SCALE;%input[131];
    CircPRawInfo.SecdCircPRaw1ms4 = (CircuitPress2x100+PressDiff2*4/5)*PRESS_SCALE;%input[132];

    % Wheel Speed Raw(8) *************************************************************************************/
    WhlSpdRawInfo.WhlSpdRawFrntLe = AvrdFL*VRAD_SCALE;%input[135];
    WhlSpdRawInfo.WhlSpdRawFrntRi = AvrdFR*VRAD_SCALE;%input[136];
    WhlSpdRawInfo.WhlSpdRawReLe = AvrdRL*VRAD_SCALE;%input[137];
    WhlSpdRawInfo.WhlSpdRawReRi = AvrdRR*VRAD_SCALE;%input[138];

    % Regen Brake controller(2) **********************************************************************/
%     RgnBrkCtrlrIfInfo.ActRgnBrkTqFromMotDrvgSys = input[143];
%     RgnBrkCtrlrIfInfo.ActRgnBrkTqChk = input[144];

    % BLS Invalid Info(3) **********************************************************************/
    BlsRaw = BLS; %input[145];


    % CAN Interface Info(17) **********************************************************************/
%     CANIfInfo.AccrPedlPosn = input[148];
%     CANIfInfo.AccrPedlPosnSigFaultFlg = input[149];
%     CANIfInfo.AccrPedlPosnSigSuspcFlg = input[150];
% 
%     CANIfInfo.FlexBrkInfo.FlexBrkSwtFaultDet = input[151];
%     CANIfInfo.FlexBrkInfo.FlexBrkSwtRaw1 = input[152];
%     CANIfInfo.FlexBrkInfo.FlexBrkSwtRaw2 = input[153];
% 
%     CANIfInfo.GearStSeld = input[154];
%     CANIfInfo.GearStPosn = input[155];
%     CANIfInfo.GearStInvldFlg = input[156];
% 
%     CANIfInfo.HCUServiceMod = input[157];
%     CANIfInfo.LFU = input[158];
% 
%     CANIfInfo.TempInfo.CooltT = input[159];
%     CANIfInfo.TempInfo.CooltTSigFaultFlg = input[160];
%     CANIfInfo.TempInfo.OutdSnsrT = input[161];
%     CANIfInfo.TempInfo.OutdSnsrTSigFaultFlg = input[162];
% 
%     CANIfInfo.SubCanBusSigFaultFlg = input[163];
%     CANIfInfo.EngMsgFaultFlg = input[164];

    % Diag Info(2) *************************************************************************************/
%     IdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg = input[165];
%     IdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg = input[166];
%    
    % ETC(1) *************************************************************************************/
    VehIgnSt = IgnStat;%input[167];

    
ActvBrkCtrlrIfInfo_DataSet(1,:)=[0:b-1]*Ts;                                              
ActvBrkCtrlrIfInfo_DataSet(2,:)=ActvBrkCtrlrIfInfo.Ay;
ActvBrkCtrlrIfInfo_DataSet(3,:)=ActvBrkCtrlrIfInfo.ActvBrkCtrlrActFlg;
ActvBrkCtrlrIfInfo_DataSet(4,:)=ActvBrkCtrlrIfInfo.StkRecvryCtrlIfInfo.StkRecvryCtrlReq;                                           
ActvBrkCtrlrIfInfo_DataSet(5,:)=ActvBrkCtrlrIfInfo.EbdCtrlInfo.EbdActFlg;
ActvBrkCtrlrIfInfo_DataSet(6,:)=ActvBrkCtrlrIfInfo.EbdCtrlInfo.EbdDefectFlg;
ActvBrkCtrlrIfInfo_DataSet(7,:)=ActvBrkCtrlrIfInfo.EbdCtrlInfo.EbdTarPFrntLe; 
ActvBrkCtrlrIfInfo_DataSet(8,:)=ActvBrkCtrlrIfInfo.EbdCtrlInfo.EbdTarPFrntRi; 
ActvBrkCtrlrIfInfo_DataSet(9,:)=ActvBrkCtrlrIfInfo.EbdCtrlInfo.EbdTarPReLe; 
ActvBrkCtrlrIfInfo_DataSet(10,:)=ActvBrkCtrlrIfInfo.EbdCtrlInfo.EbdTarPReRi;                                         
ActvBrkCtrlrIfInfo_DataSet(11,:)=ActvBrkCtrlrIfInfo.AbsCtrlInfo.AbsActFlg;
ActvBrkCtrlrIfInfo_DataSet(12,:)=ActvBrkCtrlrIfInfo.AbsCtrlInfo.AbsDefectFlg;
ActvBrkCtrlrIfInfo_DataSet(13,:)=ActvBrkCtrlrIfInfo.AbsCtrlInfo.AbsTarPFrntLe; 
ActvBrkCtrlrIfInfo_DataSet(14,:)=ActvBrkCtrlrIfInfo.AbsCtrlInfo.AbsTarPFrntRi; 
ActvBrkCtrlrIfInfo_DataSet(15,:)=ActvBrkCtrlrIfInfo.AbsCtrlInfo.AbsTarPReLe; 
ActvBrkCtrlrIfInfo_DataSet(16,:)=ActvBrkCtrlrIfInfo.AbsCtrlInfo.AbsTarPReRi; 
ActvBrkCtrlrIfInfo_DataSet(17,:)=ActvBrkCtrlrIfInfo.AbsCtrlInfo.FrntWhlSlip;                                         
ActvBrkCtrlrIfInfo_DataSet(18,:)=ActvBrkCtrlrIfInfo.TcsCtrlInfo.TcsActFlg;
ActvBrkCtrlrIfInfo_DataSet(19,:)=ActvBrkCtrlrIfInfo.TcsCtrlInfo.TcsDefectFlg;
ActvBrkCtrlrIfInfo_DataSet(20,:)=ActvBrkCtrlrIfInfo.TcsCtrlInfo.TcsTarPFrntLe; 
ActvBrkCtrlrIfInfo_DataSet(21,:)=ActvBrkCtrlrIfInfo.TcsCtrlInfo.TcsTarPFrntRi; 
ActvBrkCtrlrIfInfo_DataSet(22,:)=ActvBrkCtrlrIfInfo.TcsCtrlInfo.TcsTarPReLe; 
ActvBrkCtrlrIfInfo_DataSet(23,:)=ActvBrkCtrlrIfInfo.TcsCtrlInfo.TcsTarPReRi;                                           
ActvBrkCtrlrIfInfo_DataSet(24,:)=ActvBrkCtrlrIfInfo.EscCtrlInfo.EscActFlg;
ActvBrkCtrlrIfInfo_DataSet(25,:)=ActvBrkCtrlrIfInfo.EscCtrlInfo.EscDefectFlg;
ActvBrkCtrlrIfInfo_DataSet(26,:)=ActvBrkCtrlrIfInfo.EscCtrlInfo.EscTarPFrntLe; 
ActvBrkCtrlrIfInfo_DataSet(27,:)=ActvBrkCtrlrIfInfo.EscCtrlInfo.EscTarPFrntRi; 
ActvBrkCtrlrIfInfo_DataSet(28,:)=ActvBrkCtrlrIfInfo.EscCtrlInfo.EscTarPReLe;
ActvBrkCtrlrIfInfo_DataSet(29,:)=ActvBrkCtrlrIfInfo.EscCtrlInfo.EscTarPReRi;                                      
ActvBrkCtrlrIfInfo_DataSet(30,:)=ActvBrkCtrlrIfInfo.BaCtrlInfo.BaActFlg;
ActvBrkCtrlrIfInfo_DataSet(31,:)=ActvBrkCtrlrIfInfo.BaCtrlInfo.BaCDefectFlg; 
ActvBrkCtrlrIfInfo_DataSet(32,:)=ActvBrkCtrlrIfInfo.BaCtrlInfo.BaTarP;                                        
ActvBrkCtrlrIfInfo_DataSet(33,:)=ActvBrkCtrlrIfInfo.HsaCtrlInfo.HsaActFlg;
ActvBrkCtrlrIfInfo_DataSet(34,:)=ActvBrkCtrlrIfInfo.HsaCtrlInfo.HsaDefectFlg;
ActvBrkCtrlrIfInfo_DataSet(35,:)=ActvBrkCtrlrIfInfo.HsaCtrlInfo.HsaTarP;                                           
ActvBrkCtrlrIfInfo_DataSet(36,:)=ActvBrkCtrlrIfInfo.AvhCtrlInfo.AvhActFlg;
ActvBrkCtrlrIfInfo_DataSet(37,:)=ActvBrkCtrlrIfInfo.AvhCtrlInfo.AvhDefectFlg;
ActvBrkCtrlrIfInfo_DataSet(38,:)=ActvBrkCtrlrIfInfo.AvhCtrlInfo.AvhTarP;                                       
ActvBrkCtrlrIfInfo_DataSet(39,:)=ActvBrkCtrlrIfInfo.HdcCtrlInfo.HdcActFlg;
ActvBrkCtrlrIfInfo_DataSet(40,:)=ActvBrkCtrlrIfInfo.HdcCtrlInfo.HdcDefectFlg;
ActvBrkCtrlrIfInfo_DataSet(41,:)=ActvBrkCtrlrIfInfo.HdcCtrlInfo.HdcTarP;                                           
ActvBrkCtrlrIfInfo_DataSet(42,:)=ActvBrkCtrlrIfInfo.SccCtrlInfo.SccActFlg;
ActvBrkCtrlrIfInfo_DataSet(43,:)=ActvBrkCtrlrIfInfo.SccCtrlInfo.SccDefectFlg;
ActvBrkCtrlrIfInfo_DataSet(44,:)=ActvBrkCtrlrIfInfo.SccCtrlInfo.SccTarP;                                        
ActvBrkCtrlrIfInfo_DataSet(45,:)=ActvBrkCtrlrIfInfo.TvbbCtrlInfo.TvbbActFlg; 
ActvBrkCtrlrIfInfo_DataSet(46,:)=ActvBrkCtrlrIfInfo.TvbbCtrlInfo.TvbbDefectFlg;
ActvBrkCtrlrIfInfo_DataSet(47,:)=ActvBrkCtrlrIfInfo.TvbbCtrlInfo.TvbbTarP;                                           
ActvBrkCtrlrIfInfo_DataSet(48,:)=ActvBrkCtrlrIfInfo.EpbiCtrlInfo.EpbiActFlg; 
ActvBrkCtrlrIfInfo_DataSet(49,:)=ActvBrkCtrlrIfInfo.EpbiCtrlInfo.EpbiDefectFlg;
ActvBrkCtrlrIfInfo_DataSet(50,:)=ActvBrkCtrlrIfInfo.EpbiCtrlInfo.EpbiTarP;                                           
ActvBrkCtrlrIfInfo_DataSet(51,:)=ActvBrkCtrlrIfInfo.EbpCtrlInfo.EbpActFlg;
ActvBrkCtrlrIfInfo_DataSet(52,:)=ActvBrkCtrlrIfInfo.EbpCtrlInfo.EbpDefectFlg;
ActvBrkCtrlrIfInfo_DataSet(53,:)=ActvBrkCtrlrIfInfo.EbpCtrlInfo.EbpTarP;                                         
ActvBrkCtrlrIfInfo_DataSet(54,:)=ActvBrkCtrlrIfInfo.VehSpd;       

ActvBrkCtrlrIfInfo_DataSet=ActvBrkCtrlrIfInfo_DataSet';

IdbMotCtrlrIfInfo_DataSet(1,:)=[0:b-1]*Ts;                                              
IdbMotCtrlrIfInfo_DataSet(2,:)=  IdbMotCtrlrIfInfo.MotFbAcIPhaA;
IdbMotCtrlrIfInfo_DataSet(3,:)=  IdbMotCtrlrIfInfo.MotFbAcIPhaB;
IdbMotCtrlrIfInfo_DataSet(4,:)=  IdbMotCtrlrIfInfo.MotFbAcIPhaC;                                     
IdbMotCtrlrIfInfo_DataSet(5,:)=  IdbMotCtrlrIfInfo.MotElecRotgAg;
IdbMotCtrlrIfInfo_DataSet(6,:)=  IdbMotCtrlrIfInfo.MotMeclAngle;
IdbMotCtrlrIfInfo_DataSet(7,:)=  IdbMotCtrlrIfInfo.MotCtrlrStInfo.TBD;                                    
IdbMotCtrlrIfInfo_DataSet(8,:)=  IdbMotCtrlrIfInfo.VdcLinkRaw;
IdbMotCtrlrIfInfo_DataSet(9,:)=  IdbMotCtrlrIfInfo.VIvtrDcLink;

IdbMotCtrlrIfInfo_DataSet=IdbMotCtrlrIfInfo_DataSet';

WhlSpdRawInfo_DataSet(1,:)=[0:b-1]*Ts; 
WhlSpdRawInfo_DataSet(2,:)=WhlSpdRawInfo.WhlSpdRawFrntLe;
WhlSpdRawInfo_DataSet(3,:)=WhlSpdRawInfo.WhlSpdRawFrntRi;
WhlSpdRawInfo_DataSet(4,:)=WhlSpdRawInfo.WhlSpdRawReLe;
WhlSpdRawInfo_DataSet(5,:)=WhlSpdRawInfo.WhlSpdRawReRi;
      
WhlSpdRawInfo_DataSet=WhlSpdRawInfo_DataSet';

IdbSensorRawSigInfo_DataSet(1,:)=[0:b-1]*Ts; 
IdbSensorRawSigInfo_DataSet(2,:)= PedlTrvlRawInfo.PdtRaw;            
IdbSensorRawSigInfo_DataSet(3,:)= PedlTrvlRawInfo.PdtRaw1ms0;   
IdbSensorRawSigInfo_DataSet(4,:)= PedlTrvlRawInfo.PdtRaw1ms1;   
IdbSensorRawSigInfo_DataSet(5,:)= PedlTrvlRawInfo.PdtRaw1ms2;      
IdbSensorRawSigInfo_DataSet(6,:)= PedlTrvlRawInfo.PdtRaw1ms3;     
IdbSensorRawSigInfo_DataSet(7,:)= PedlTrvlRawInfo.PdtRaw1ms4;     
IdbSensorRawSigInfo_DataSet(8,:)= PedlTrvlRawInfo.PdfRaw;           
IdbSensorRawSigInfo_DataSet(9,:)= PedlSimrPRaw;                     
IdbSensorRawSigInfo_DataSet(10,:)= PistPRawInfo.PistPRaw;             
IdbSensorRawSigInfo_DataSet(11,:)= PistPRawInfo.PistPRaw1ms0;       
IdbSensorRawSigInfo_DataSet(12,:)= PistPRawInfo.PistPRaw1ms1;      
IdbSensorRawSigInfo_DataSet(13,:)=PistPRawInfo.PistPRaw1ms2;      
IdbSensorRawSigInfo_DataSet(14,:)= PistPRawInfo.PistPRaw1ms3;      
IdbSensorRawSigInfo_DataSet(15,:)= PistPRawInfo.PistPRaw1ms4;      
IdbSensorRawSigInfo_DataSet(16,:)= CircPRawInfo.PrimCircPRaw;        
IdbSensorRawSigInfo_DataSet(17,:)= CircPRawInfo.PrimCircPRaw1ms0;  
IdbSensorRawSigInfo_DataSet(18,:)= CircPRawInfo.PrimCircPRaw1ms1;  
IdbSensorRawSigInfo_DataSet(19,:)= CircPRawInfo.PrimCircPRaw1ms2;  
IdbSensorRawSigInfo_DataSet(20,:)= CircPRawInfo.PrimCircPRaw1ms3;  
IdbSensorRawSigInfo_DataSet(21,:)= CircPRawInfo.PrimCircPRaw1ms4;  
IdbSensorRawSigInfo_DataSet(22,:)= CircPRawInfo.SecdCircPRaw;        
IdbSensorRawSigInfo_DataSet(23,:)= CircPRawInfo.SecdCircPRaw1ms0;  
IdbSensorRawSigInfo_DataSet(24,:)= CircPRawInfo.SecdCircPRaw1ms1;  
IdbSensorRawSigInfo_DataSet(25,:)= CircPRawInfo.SecdCircPRaw1ms2;  
IdbSensorRawSigInfo_DataSet(26,:)= CircPRawInfo.SecdCircPRaw1ms3;  
IdbSensorRawSigInfo_DataSet(27,:)= CircPRawInfo.SecdCircPRaw1ms4;  
              
IdbSensorRawSigInfo_DataSet=IdbSensorRawSigInfo_DataSet';

BLS_IgnSt_DataSet(1,:)=[0:b-1]*Ts; 
BLS_IgnSt_DataSet(2,:)= VehIgnSt;
BLS_IgnSt_DataSet(3,:)= BlsRaw;
BLS_IgnSt_DataSet=BLS_IgnSt_DataSet';

%% Ouput Dataset for check SILs result
BBC_Output(1,:)=[0:b-1]*Ts; 
BBC_Output(2,:)=AHBOn;
BBC_Output(3,:)=PedalSimPressGenOn;
BBC_Output(4,:)=StandstillCtrl;
BBC_Output(5,:)=zeros([1 b]);% LowSpdCtrlInhibitFlg
% BBC_Output(5,:)=zeros([1 b]);


Det_Output(1,:)=[0:b-1]*Ts; 
Det_Output(2,:)=PedalTravelRateMmPSec;
Det_Output(3,:)=PedalSigTag;
Det_Output(4,:)=PedalTravelFilt;
Det_Output(5,:)=InitFastApplyFlg;
Det_Output(6,:)=PedalReleaseFlag1;
Det_Output(7,:)=PedalReleaseFlag2;



CVV2_Act
CVV_Act
CVV_TargetCurrent
CircuitPressRate10ms
CutCloseDelayForQuickBrk
DelS_Target

InitQuickBrkDetectFlg
MtrTargetPosi
PCtrlBoostMode


PedalSigTag
PCtrlState

Posi32bit
RLV1_Act
RLV1_TargetCurrent
%RLV2_Act
SIMV_Act
SIMV_TargetCurrent
SpeedInvalidFlg
TargetPressRate10ms
TargetPressX100
idRefIn1ms
iqRefIn1ms
wRpmMechRawIn1ms