#ifndef __CORE2_TASK_SCHEDULER_C__
#define __CORE2_TASK_SCHEDULER_C__

#include "Core2_Task_Scheduler.h"
#include SCHEDULER_PATH(fifo.h)
#include SCHEDULER_PATH(LibScheduler.h)
#include SCHEDULER_PATH(Scheduler.h)

OS_t	OS2;
CORE_t	CORE2;
TASK_t  CORE2_TASK_INFO[CORE2_NUMBER_OF_TASK];
TASK_t  CORE2_TASK[CORE2_NUMBER_OF_TASK];
FIFO_BUFFER	  CORE2_PRIO_QUEUE[ CORE2_NUMBER_OF_QUEUE ];

unsigned char  CORE2_DATA_QUEUE_0[ FIFO_BUFFER_SIZE ];
unsigned char  CORE2_DATA_QUEUE_1[ FIFO_BUFFER_SIZE ];
unsigned char  CORE2_DATA_QUEUE_2[ FIFO_BUFFER_SIZE ];
unsigned char  CORE2_DATA_QUEUE_3[ FIFO_BUFFER_SIZE ];
unsigned char  CORE2_DATA_QUEUE_4[ FIFO_BUFFER_SIZE ];
unsigned char  CORE2_DATA_QUEUE_5[ FIFO_BUFFER_SIZE ];
unsigned char  CORE2_DATA_QUEUE_6[ FIFO_BUFFER_SIZE ];
unsigned char  CORE2_DATA_QUEUE_7[ FIFO_BUFFER_SIZE ];
unsigned char  CORE2_DATA_QUEUE_8[ FIFO_BUFFER_SIZE ];
unsigned char  CORE2_DATA_QUEUE_9[ FIFO_BUFFER_SIZE ];
unsigned char  CORE2_DATA_QUEUE_10[ FIFO_BUFFER_SIZE ];
unsigned char  CORE2_DATA_QUEUE_11[ FIFO_BUFFER_SIZE ];
unsigned char  CORE2_DATA_QUEUE_12[ FIFO_BUFFER_SIZE ];
unsigned char  CORE2_DATA_QUEUE_13[ FIFO_BUFFER_SIZE ];
unsigned char  CORE2_DATA_QUEUE_14[ FIFO_BUFFER_SIZE ];
unsigned char  CORE2_DATA_QUEUE_15[ FIFO_BUFFER_SIZE ];

#ifdef CORE2_TASK_NAME_0
TASK_t CORE2_REGISTER_TASK_0 ={
	0,//unsigned char ID;                        
	0,//TaskRunModeState_t	RunMode;             
	0,//TaskState_t TaskState;        
	0,//TaskState_t OldTaskState;                   
	0,//unsigned int PeriodCounter;              
	0,//unsigned int TaskExecTimer;              
	CORE2_TASK_PRIO_0,//unsigned char TaskPrio;                  
	
	CORE2_TASK_NICK_NAME_NUM_0,//unsigned char TASK_NICKNAME;             
	CORE2_TASK_PERIOD_TIME_0,//unsigned int TASK_PERIOD_TIME;           
	CORE2_TASK_RUN_START_OFFSET_TIME_0,//unsigned int TASK_RUN_START_POINT;       
	CORE2_TASK_EXECUTION_TIME_0,//unsigned int TASK_EXEC_TIME;             
	
	CORE2_TASK_RUN_0,//SCH_TASK_POINTER	pFcnTask;              
	CORE2_TASK_READ_0,//SCH_RTE_READ_EVENT_POINTER pFcnRteRead;  
	CORE2_TASK_WRITE_0//SCH_RTE_WRITE_EVENT_POINTER pFcnRteWrite;
};
#endif

#ifdef CORE2_TASK_NAME_1
TASK_t CORE2_REGISTER_TASK_1 ={
	1,//unsigned char ID;                        
	0,//TaskRunModeState_t	RunMode;             
	0,//TaskState_t TaskState;                   
	0,//TaskState_t OldTaskState;                   
	0,//unsigned int PeriodCounter;              
	0,//unsigned int TaskExecTimer;              
	CORE2_TASK_PRIO_1,//unsigned char TaskPrio;                  
	
	CORE2_TASK_NICK_NAME_NUM_1,//unsigned char TASK_NICKNAME;             
	CORE2_TASK_PERIOD_TIME_1,//unsigned int TASK_PERIOD_TIME;           
	CORE2_TASK_RUN_START_OFFSET_TIME_1,//unsigned int TASK_RUN_START_POINT;       
	CORE2_TASK_EXECUTION_TIME_1,//unsigned int TASK_EXEC_TIME;             
	
	CORE2_TASK_RUN_1,//SCH_TASK_POINTER	pFcnTask;              
	CORE2_TASK_READ_1,//SCH_RTE_READ_EVENT_POINTER pFcnRteRead;  
	CORE2_TASK_WRITE_1//SCH_RTE_WRITE_EVENT_POINTER pFcnRteWrite;
};
#endif

#ifdef CORE2_TASK_NAME_2
TASK_t CORE2_REGISTER_TASK_2 ={
	2,//unsigned char ID;                        
	0,//TaskRunModeState_t	RunMode;             
	0,//TaskState_t TaskState;                   
	0,//TaskState_t OldTaskState;                   
	0,//unsigned int PeriodCounter;              
	0,//unsigned int TaskExecTimer;              
	CORE2_TASK_PRIO_2,//unsigned char TaskPrio;                  
	
	CORE2_TASK_NICK_NAME_NUM_2,//unsigned char TASK_NICKNAME;             
	CORE2_TASK_PERIOD_TIME_2,//unsigned int TASK_PERIOD_TIME;           
	CORE2_TASK_RUN_START_OFFSET_TIME_2,//unsigned int TASK_RUN_START_POINT;       
	CORE2_TASK_EXECUTION_TIME_2,//unsigned int TASK_EXEC_TIME;             
	
	CORE2_TASK_RUN_2,//SCH_TASK_POINTER	pFcnTask;              
	CORE2_TASK_READ_2,//SCH_RTE_READ_EVENT_POINTER pFcnRteRead;  
	CORE2_TASK_WRITE_2//SCH_RTE_WRITE_EVENT_POINTER pFcnRteWrite;
};
#endif

#ifdef TASK_NAME_3
TASK_t CORE2_REGISTER_TASK_3 ={
	3,//unsigned char ID;                        
	0,//TaskRunModeState_t	RunMode;             
	0,//TaskState_t TaskState;                   
	0,//TaskState_t OldTaskState;                   
	0,//unsigned int PeriodCounter;              
	0,//unsigned int TaskExecTimer;              
	CORE2_TASK_PRIO_3,//unsigned char TaskPrio;                  
	
	CORE2_TASK_NICK_NAME_NUM_3,//unsigned char TASK_NICKNAME;             
	CORE2_TASK_PERIOD_TIME_3,//unsigned int TASK_PERIOD_TIME;           
	CORE2_TASK_RUN_START_OFFSET_TIME_3,//unsigned int TASK_RUN_START_POINT;       
	CORE2_TASK_EXECUTION_TIME_3,//unsigned int TASK_EXEC_TIME;             
	
	CORE2_TASK_RUN_3,//SCH_TASK_POINTER	pFcnTask;              
	CORE2_TASK_READ_3,//SCH_RTE_READ_EVENT_POINTER pFcnRteRead;  
	CORE2_TASK_WRITE_3//SCH_RTE_WRITE_EVENT_POINTER pFcnRteWrite;
};
#endif

void Core2TaskInit(void)
{
	static unsigned char run=0;
	unsigned int i;
	if(run==0)
	{
		#ifdef CORE2_TASK_NAME_0
			CORE2_TASK[0] = *(TASK_t *)&CORE2_REGISTER_TASK_0;
			AdjustTaskRegister(&CORE2_TASK[0], TICK_PERIOD);
		#endif
				
		#ifdef CORE2_TASK_NAME_1
			CORE2_TASK[1] = *(TASK_t *)&CORE2_REGISTER_TASK_1;
			AdjustTaskRegister(&CORE2_TASK[1], TICK_PERIOD);
		#endif
		
		for(i=1; i<=FIFO_BUFFER_SIZE; i++)
		{
			CORE2_DATA_QUEUE_0[i-1]=0;
			CORE2_DATA_QUEUE_1[i-1]=0;
			CORE2_DATA_QUEUE_2[i-1]=0;
			CORE2_DATA_QUEUE_3[i-1]=0;
			CORE2_DATA_QUEUE_4[i-1]=0;
			CORE2_DATA_QUEUE_5[i-1]=0;
			CORE2_DATA_QUEUE_6[i-1]=0;
			CORE2_DATA_QUEUE_7[i-1]=0;
			CORE2_DATA_QUEUE_8[i-1]=0;
			CORE2_DATA_QUEUE_9[i-1]=0;
			CORE2_DATA_QUEUE_10[i-1]=0;
			CORE2_DATA_QUEUE_11[i-1]=0;
			CORE2_DATA_QUEUE_12[i-1]=0;
			CORE2_DATA_QUEUE_13[i-1]=0;
			CORE2_DATA_QUEUE_14[i-1]=0;
			CORE2_DATA_QUEUE_15[i-1]=0;
		}
		//CORE1_PRIO_QUEUE[13].id=13;
		/*
		q = CORE1_PRIO_QUEUE;
		for(i=0; i<CORE1_NUMBER_OF_QUEUE; i++)
		{
			FIFO_Init((q+i), CORE1_DATA_QUEUE_0, sizeof(CORE1_DATA_QUEUE_0));
		}
		*/

		FIFO_Init(&CORE2_PRIO_QUEUE[0], CORE2_DATA_QUEUE_0, sizeof(CORE2_DATA_QUEUE_0));
		FIFO_Init(&CORE2_PRIO_QUEUE[1], CORE2_DATA_QUEUE_1, sizeof(CORE2_DATA_QUEUE_1));
		FIFO_Init(&CORE2_PRIO_QUEUE[2], CORE2_DATA_QUEUE_2, sizeof(CORE2_DATA_QUEUE_2));
		FIFO_Init(&CORE2_PRIO_QUEUE[3], CORE2_DATA_QUEUE_3, sizeof(CORE2_DATA_QUEUE_3));
		FIFO_Init(&CORE2_PRIO_QUEUE[4], CORE2_DATA_QUEUE_4, sizeof(CORE2_DATA_QUEUE_4));
		FIFO_Init(&CORE2_PRIO_QUEUE[5], CORE2_DATA_QUEUE_5, sizeof(CORE2_DATA_QUEUE_5));
		FIFO_Init(&CORE2_PRIO_QUEUE[6], CORE2_DATA_QUEUE_6, sizeof(CORE2_DATA_QUEUE_6));
		FIFO_Init(&CORE2_PRIO_QUEUE[7], CORE2_DATA_QUEUE_7, sizeof(CORE2_DATA_QUEUE_7));
		FIFO_Init(&CORE2_PRIO_QUEUE[8], CORE2_DATA_QUEUE_8, sizeof(CORE2_DATA_QUEUE_8));
		FIFO_Init(&CORE2_PRIO_QUEUE[9], CORE2_DATA_QUEUE_9, sizeof(CORE2_DATA_QUEUE_9));
		FIFO_Init(&CORE2_PRIO_QUEUE[10], CORE2_DATA_QUEUE_10, sizeof(CORE2_DATA_QUEUE_10));
		FIFO_Init(&CORE2_PRIO_QUEUE[11], CORE2_DATA_QUEUE_11, sizeof(CORE2_DATA_QUEUE_11));
		FIFO_Init(&CORE2_PRIO_QUEUE[12], CORE2_DATA_QUEUE_12, sizeof(CORE2_DATA_QUEUE_12));
		FIFO_Init(&CORE2_PRIO_QUEUE[13], CORE2_DATA_QUEUE_13, sizeof(CORE2_DATA_QUEUE_13));
		FIFO_Init(&CORE2_PRIO_QUEUE[14], CORE2_DATA_QUEUE_14, sizeof(CORE2_DATA_QUEUE_14));
		FIFO_Init(&CORE2_PRIO_QUEUE[15], CORE2_DATA_QUEUE_15, sizeof(CORE2_DATA_QUEUE_15));
		
		run=1;
	}
}



#endif

