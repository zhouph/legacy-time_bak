#ifndef __CORE1_TASK_SCHEDULER_H__
#define __CORE1_TASK_SCHEDULER_H__

#include "Core1_Task_SchedulerCfg.h"

#include SCHEDULER_PATH(fifo.h)
#include SCHEDULER_PATH(LibScheduler.h)
#include SCHEDULER_PATH(Scheduler.h)

#define CORE1_NUMBER_OF_QUEUE		16

extern FIFO_BUFFER	  CORE1_PRIO_QUEUE[ CORE1_NUMBER_OF_QUEUE ];
extern unsigned char  CORE1_DATA_QUEUE[ CORE1_NUMBER_OF_QUEUE ][ FIFO_BUFFER_SIZE ];

extern OS_t		OS1;
extern CORE_t	CORE1;
extern TASK_t CORE1_TASK_INFO[CORE1_NUMBER_OF_TASK];
extern TASK_t CORE1_TASK[CORE1_NUMBER_OF_TASK];

#endif
