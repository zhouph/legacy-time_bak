#define S_FUNCTION_NAME	 	 FAL_Sfunction
#define S_FUNCTION_LEVEL 2

#include "simstruc.h"
#include <stdio.h>
#define SAMPLE_TIME				0.0005
#define msec(x)                 x*2
#define TICK_PERIOD				1

#define NUM_PARAMS				0
#define WidthInputPort			1//168
#define WidthOutputPort			2//211

//int ArrayCnt;
//int test_cnt;

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



//#define MULTI_OS_ENA
#ifndef STRINGIFY
#define STRINGIFY(x) #x
#endif /* STRINGIFY */
#define SCHEDULER_PATH(_FILENAME_)	STRINGIFY(../../../MDL/OS/_FILENAME_)

#include "SplCfg.h"
#include "Core1_Task_SchedulerCfg.h"

#include SCHEDULER_PATH(fifo.c)
#include SCHEDULER_PATH(LibScheduler.c)
#include SCHEDULER_PATH(Scheduler.c)
#include "Core1_Task_Scheduler.c"

#ifdef MULTI_OS_ENA
#include "Core2_Task_SchedulerCfg.h"
#include "Core2_Task_Scheduler.c"
#endif




static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
	int i;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
	InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);
    
	  
    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

			
//        PedlTrvlRawInfo.PdtRaw1ms[ArrayCnt]		= input[97];
//        PistPRawInfo.PistPRaw1ms[ArrayCnt]		= input[111];
//        CircPRawInfo.PrimCircPRaw1ms[ArrayCnt] 	= input[119];
//        CircPRawInfo.SecdCircPRaw1ms[ArrayCnt] 	= input[127];
//
//        ArrayCnt++;
//        if(ArrayCnt>=5) ArrayCnt=0;




		Core1TaskInit();


		Sch_CoreTickTok(&CORE1, &CORE1_TASK, CORE1_NUMBER_OF_TASK);
		Sch_PreProcCoreTask(CORE1_NUMBER_OF_TASK, &CORE1_TASK, &CORE1_PRIO_QUEUE);
		Sch_ProcCoreTask(CORE1_NUMBER_OF_QUEUE, &CORE1_PRIO_QUEUE, &CORE1_TASK);
	
		#ifdef MULTI_OS_ENA
		Core2TaskInit();
		Sch_CoreTickTok(&CORE2, &CORE2_TASK, CORE2_NUMBER_OF_TASK);
		Sch_PreProcCoreTask(CORE2_NUMBER_OF_TASK, &CORE2_TASK, &CORE2_PRIO_QUEUE);
		Sch_ProcCoreTask(CORE2_NUMBER_OF_QUEUE, &CORE2_PRIO_QUEUE, &CORE2_TASK);
		#endif	
		
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
