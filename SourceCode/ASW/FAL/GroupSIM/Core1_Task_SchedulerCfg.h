#ifndef __CORE1_TASK_SCHEDULERCFG_H__
#define __CORE1_TASK_SCHEDULERCFG_H__



/*==================================================================================*/


#define CORE1_NUMBER_OF_TASK		            2

#define TASK_NAME_0
#define TASK_NICK_NAME_NUM_0					1
#define TASK_PRIO_0								10
#define	TASK_PERIOD_TIME_0						msec(1)//1000
#define	TASK_EXECUTION_TIME_0					msec(0.5)
#define TASK_RUN_START_OFFSET_TIME_0	        0
#define TASK_RUN_0								Task1msec
#define TASK_READ_0								RteReadTask1msec
#define TASK_WRITE_0  							RteWriteTask1msec

#ifdef CORE1_NUMBER_OF_TASK >= 2
#define TASK_NAME_1
#endif
#define TASK_NICK_NAME_NUM_1					2
#define TASK_PRIO_1								9
#define	TASK_PERIOD_TIME_1						msec(5)//5000
#define	TASK_EXECUTION_TIME_1					msec(1)//1000
#define TASK_RUN_START_OFFSET_TIME_1	        0
#define TASK_RUN_1								Task5msec
#define TASK_READ_1								RteReadTask5msec
#define TASK_WRITE_1							RteWriteTask5msec




/*==================================================================================*/
int16_t Task1msChkCnt;
int16_t Task5msChkCnt;
int16_t ArrayCnt;

#define PEDLTRVLRAWINFOINPUT_PORT  97
#define PISTPRAWINFOINPUT_PORT  111
#define PRIMPRAWINFOINPUT_PORT  119
#define SECDYPRAWINFOINPUT_PORT  127

void Task1msec(void)
{
	LAMTR_vCallMotIRefGenn1ms();
	Task1msChkCnt++;

	PedlTrvlRawInfo.PdtRaw1ms[ArrayCnt]		= input[PEDLTRVLRAWINFOINPUT_PORT];
	PistPRawInfo.PistPRaw1ms[ArrayCnt]		= input[PISTPRAWINFOINPUT_PORT];
	CircPRawInfo.PrimCircPRaw1ms[ArrayCnt] 	= input[PRIMPRAWINFOINPUT_PORT];
	CircPRawInfo.SecdCircPRaw1ms[ArrayCnt] 	= input[SECDYPRAWINFOINPUT_PORT];

	ArrayCnt++;
	if(ArrayCnt>=5) ArrayCnt=0;
}

void Task5msec(void)
{
   LSIDB_vCallMainSenSigProcessing();
   LDIDB_vCallMainDetn();
   LCBBC_vCallMain();
   RgnBrkCopvCtrlMain();
   RgnBrkCopnActRgnBrkP();
   LCIDB_vCallMainTarPArbr();
   LAIDB_vCallPressureCtrl();
   LAIDB_vCallMainVlvActn();
   LAMTR_vCallMotIRefGenn5ms();

   Task5msChkCnt++;
}



/* Active Brake Input(53) ********************************************************************/

void RteReadTask1msec(void)
{
	//SIGPROCbus.PedlTrvlRawInfo.PdtRaw= input[0];
    /* Input */

	// motor ctrl 1ms input
	int16_t Mot1msCtrlIfInfo_Start;

	Mot1msCtrlIfInfo_Start=0;//check

	IdbMotCtrlrIfInfo.MotMeclAngle=input[Mot1msCtrlIfInfo_Start+7];
	IdbMotCtrlrIfInfo.VdcLinkRaw=input[Mot1msCtrlIfInfo_Start+8];

}

void RteWriteTask1msec(void)
{

	// motor ctrl 1ms output
	int16_t Mot1msCtrlOutpInfo_Start;
	Mot1msCtrlOutpInfo_Start=0;

	output[Mot1msCtrlOutpInfo_Start+0] =  IdbMotPosnInfo.MotMeasdPosn;
	output[Mot1msCtrlOutpInfo_Start+1] =  IdbMotPosnInfo.MotTarPosn;
	output[Mot1msCtrlOutpInfo_Start+2] =  IqRefFinal;
	output[Mot1msCtrlOutpInfo_Start+3] =  IdRefFinal;
	output[Mot1msCtrlOutpInfo_Start+4] =  VdcLinkFild;


	output[Mot1msCtrlOutpInfo_Start+5]=Task1msChkCnt;

}



void RteReadTask5msec(void)
{
//    /* Active Brake Input(53) ********************************************************************/
//    static int16_t  ActvBrkCtrlrIfInfo_Start;
//    static int16_t  IdbMotCtrlrIfInfo_Start;
//    static int16_t  IdbSnsrEepInfo_Start;
//    static int16_t IdbSnsrInvalidInfo_Start;
//    static int16_t CANIfInfo_Start;
//    static int16_t RgnBrkCtrlrIfInfo_Start;
//    static int16_t EepDiagIfInfo_Start;
//    static int16_t IdbSnsrRawSigInfo_Start;
//    static int16_t BlsIgnStInfo_Start;
//
//    ActvBrkCtrlrIfInfo_Start=0;
//s
//	ActvBrkCtrlrIfInfo.Ay = input[ActvBrkCtrlrIfInfo_Start+0];
//    ActvBrkCtrlrIfInfo.ActvBrkCtrlrActFlg = input[ActvBrkCtrlrIfInfo_Start+1];
//    ActvBrkCtrlrIfInfo.StkRecvryCtrlIfInfo.StkRecvryCtrlReq = input[ActvBrkCtrlrIfInfo_Start+2];
//
//    ActvBrkCtrlrIfInfo.EbdCtrlInfo.EbdActFlg = input[ActvBrkCtrlrIfInfo_Start+3];
//    ActvBrkCtrlrIfInfo.EbdCtrlInfo.EbdDefectFlg = input[ActvBrkCtrlrIfInfo_Start+4];
//    ActvBrkCtrlrIfInfo.EbdCtrlInfo.EbdTarPFrntLe = input[ActvBrkCtrlrIfInfo_Start+5];
//    ActvBrkCtrlrIfInfo.EbdCtrlInfo.EbdTarPFrntRi = input[ActvBrkCtrlrIfInfo_Start+6];
//    ActvBrkCtrlrIfInfo.EbdCtrlInfo.EbdTarPReLe = input[ActvBrkCtrlrIfInfo_Start+7];
//    ActvBrkCtrlrIfInfo.EbdCtrlInfo.EbdTarPReRi = input[ActvBrkCtrlrIfInfo_Start+8];
//
//    ActvBrkCtrlrIfInfo.AbsCtrlInfo.AbsActFlg = input[ActvBrkCtrlrIfInfo_Start+9];
//    ActvBrkCtrlrIfInfo.AbsCtrlInfo.AbsDefectFlg = input[ActvBrkCtrlrIfInfo_Start+10];
//    ActvBrkCtrlrIfInfo.AbsCtrlInfo.AbsTarPFrntLe = input[ActvBrkCtrlrIfInfo_Start+11];
//    ActvBrkCtrlrIfInfo.AbsCtrlInfo.AbsTarPFrntRi = input[ActvBrkCtrlrIfInfo_Start+12];
//    ActvBrkCtrlrIfInfo.AbsCtrlInfo.AbsTarPReLe = input[ActvBrkCtrlrIfInfo_Start+13];
//    ActvBrkCtrlrIfInfo.AbsCtrlInfo.AbsTarPReRi = input[ActvBrkCtrlrIfInfo_Start+14];
//    ActvBrkCtrlrIfInfo.AbsCtrlInfo.FrntWhlSlip = input[ActvBrkCtrlrIfInfo_Start+15];
//
//    ActvBrkCtrlrIfInfo.TcsCtrlInfo.TcsActFlg = input[ActvBrkCtrlrIfInfo_Start+16];
//    ActvBrkCtrlrIfInfo.TcsCtrlInfo.TcsDefectFlg = input[ActvBrkCtrlrIfInfo_Start+17];
//    ActvBrkCtrlrIfInfo.TcsCtrlInfo.TcsTarPFrntLe = input[ActvBrkCtrlrIfInfo_Start+18];
//    ActvBrkCtrlrIfInfo.TcsCtrlInfo.TcsTarPFrntRi = input[ActvBrkCtrlrIfInfo_Start+19];
//    ActvBrkCtrlrIfInfo.TcsCtrlInfo.TcsTarPReLe = input[ActvBrkCtrlrIfInfo_Start+20];
//    ActvBrkCtrlrIfInfo.TcsCtrlInfo.TcsTarPReRi = input[ActvBrkCtrlrIfInfo_Start+21];
//
//    ActvBrkCtrlrIfInfo.EscCtrlInfo.EscActFlg = input[ActvBrkCtrlrIfInfo_Start+22];
//    ActvBrkCtrlrIfInfo.EscCtrlInfo.EscDefectFlg = input[ActvBrkCtrlrIfInfo_Start+23];
//    ActvBrkCtrlrIfInfo.EscCtrlInfo.EscTarPFrntLe = input[ActvBrkCtrlrIfInfo_Start+24];
//    ActvBrkCtrlrIfInfo.EscCtrlInfo.EscTarPFrntRi = input[ActvBrkCtrlrIfInfo_Start+25];
//    ActvBrkCtrlrIfInfo.EscCtrlInfo.EscTarPReLe = input[ActvBrkCtrlrIfInfo_Start+26];
//    ActvBrkCtrlrIfInfo.EscCtrlInfo.EscTarPReRi = input[ActvBrkCtrlrIfInfo_Start+27];
//
//    ActvBrkCtrlrIfInfo.BaCtrlInfo.BaActFlg = input[ActvBrkCtrlrIfInfo_Start+28];
//    ActvBrkCtrlrIfInfo.BaCtrlInfo.BaCDefectFlg = input[ActvBrkCtrlrIfInfo_Start+29];
//    ActvBrkCtrlrIfInfo.BaCtrlInfo.BaTarP = input[ActvBrkCtrlrIfInfo_Start+30];
//
//    ActvBrkCtrlrIfInfo.HsaCtrlInfo.HsaActFlg = input[ActvBrkCtrlrIfInfo_Start+31];
//    ActvBrkCtrlrIfInfo.HsaCtrlInfo.HsaDefectFlg = input[ActvBrkCtrlrIfInfo_Start+32];
//    ActvBrkCtrlrIfInfo.HsaCtrlInfo.HsaTarP = input[ActvBrkCtrlrIfInfo_Start+33];
//
//    ActvBrkCtrlrIfInfo.AvhCtrlInfo.AvhActFlg = input[ActvBrkCtrlrIfInfo_Start+34];
//    ActvBrkCtrlrIfInfo.AvhCtrlInfo.AvhDefectFlg = input[ActvBrkCtrlrIfInfo_Start+35];
//    ActvBrkCtrlrIfInfo.AvhCtrlInfo.AvhTarP = input[ActvBrkCtrlrIfInfo_Start+36];
//
//    ActvBrkCtrlrIfInfo.HdcCtrlInfo.HdcActFlg = input[ActvBrkCtrlrIfInfo_Start+37];
//    ActvBrkCtrlrIfInfo.HdcCtrlInfo.HdcDefectFlg = input[ActvBrkCtrlrIfInfo_Start+38];
//    ActvBrkCtrlrIfInfo.HdcCtrlInfo.HdcTarP = input[ActvBrkCtrlrIfInfo_Start+39];
//
//    ActvBrkCtrlrIfInfo.SccCtrlInfo.SccActFlg = input[ActvBrkCtrlrIfInfo_Start+40];
//    ActvBrkCtrlrIfInfo.SccCtrlInfo.SccDefectFlg = input[ActvBrkCtrlrIfInfo_Start+41];
//    ActvBrkCtrlrIfInfo.SccCtrlInfo.SccTarP = input[ActvBrkCtrlrIfInfo_Start+42];
//
//    ActvBrkCtrlrIfInfo.TvbbCtrlInfo.TvbbActFlg = input[ActvBrkCtrlrIfInfo_Start+43];
//    ActvBrkCtrlrIfInfo.TvbbCtrlInfo.TvbbDefectFlg = input[ActvBrkCtrlrIfInfo_Start+44];
//    ActvBrkCtrlrIfInfo.TvbbCtrlInfo.TvbbTarP = input[ActvBrkCtrlrIfInfo_Start+45];
//
//    ActvBrkCtrlrIfInfo.EpbiCtrlInfo.EpbiActFlg = input[ActvBrkCtrlrIfInfo_Start+46];
//    ActvBrkCtrlrIfInfo.EpbiCtrlInfo.EpbiDefectFlg = input[ActvBrkCtrlrIfInfo_Start+47];
//    ActvBrkCtrlrIfInfo.EpbiCtrlInfo.EpbiTarP = input[ActvBrkCtrlrIfInfo_Start+48];
//
//    ActvBrkCtrlrIfInfo.EbpCtrlInfo.EbpActFlg = input[ActvBrkCtrlrIfInfo_Start+49];
//    ActvBrkCtrlrIfInfo.EbpCtrlInfo.EbpDefectFlg = input[ActvBrkCtrlrIfInfo_Start+50];
//    ActvBrkCtrlrIfInfo.EbpCtrlInfo.EbpTarP = input[ActvBrkCtrlrIfInfo_Start+51];
//
//    ActvBrkCtrlrIfInfo.VehSpd = input[ActvBrkCtrlrIfInfo_Start+52];
//
//    /* IDB Motor Control(8) ************************************************************************/
//
//    IdbMotCtrlrIfInfo_Start=53;
//
//    IdbMotCtrlrIfInfo.MotFbAcIPhaA = input[IdbMotCtrlrIfInfo_Start+0];
//    IdbMotCtrlrIfInfo.MotFbAcIPhaB = input[IdbMotCtrlrIfInfo_Start+1];
//    IdbMotCtrlrIfInfo.MotFbAcIPhaC = input[IdbMotCtrlrIfInfo_Start+2];
//
//    IdbMotCtrlrIfInfo.MotElecRotgAg = input[IdbMotCtrlrIfInfo_Start+3];
//    IdbMotCtrlrIfInfo.MotMeclAngle = input[IdbMotCtrlrIfInfo_Start+4];
//    IdbMotCtrlrIfInfo.MotCtrlrStInfo.TBD = input[IdbMotCtrlrIfInfo_Start+5];
//
//    IdbMotCtrlrIfInfo.VdcLinkRaw = input[IdbMotCtrlrIfInfo_Start+6];
//    IdbMotCtrlrIfInfo.VIvtrDcLink = input[IdbMotCtrlrIfInfo_Start+7];
//
//    /* Pedal Travel Offset EEP(12) *********************************************************************/
//
//    IdbSnsrEepInfo_Start=61;
//
//    PedlTrvlOffsEepInfo.PdtOffsEolWrReqCmpldFlg = input[IdbSnsrEepInfo_Start+0];
//    PedlTrvlOffsEepInfo.PdtOffsEolReadInvldFlg = input[IdbSnsrEepInfo_Start+1];
//    PedlTrvlOffsEepInfo.PdtOffsEolReadVal = input[IdbSnsrEepInfo_Start+2];
//
//    PedlTrvlOffsEepInfo.PdfOffsEolWrReqCmpldFlg = input[IdbSnsrEepInfo_Start+3];
//    PedlTrvlOffsEepInfo.PdfOffsEolReadInvldFlg = input[IdbSnsrEepInfo_Start+4];
//    PedlTrvlOffsEepInfo.PdfOffsEolReadVal = input[IdbSnsrEepInfo_Start+5];
//
//    PedlTrvlOffsEepInfo.PdtOffsDrvgWrReqCmpldFlg = input[IdbSnsrEepInfo_Start+6];
//    PedlTrvlOffsEepInfo.PdtOffsDrvgReadInvldFlg = input[IdbSnsrEepInfo_Start+7];
//    PedlTrvlOffsEepInfo.PdtOffsDrvgReadVal = input[IdbSnsrEepInfo_Start+8];
//
//    PedlTrvlOffsEepInfo.PdfOffsDrvgWrReqCmpldFlg = input[IdbSnsrEepInfo_Start+9];
//    PedlTrvlOffsEepInfo.PdfOffsDrvgReadInvldFlg = input[IdbSnsrEepInfo_Start+10];
//    PedlTrvlOffsEepInfo.PdfOffsDrvgReadVal = input[IdbSnsrEepInfo_Start+11];
//
//    /* Pressure Offset EEP(24) **********************************************************************/
//    PedlSimrPOffsEepInfo.PedlSimrPOffsEolWrReqCmpldFlg = input[IdbSnsrEepInfo_Start+12];
//    PedlSimrPOffsEepInfo.PedlSimrPOffsEolReadInvldFlg = input[IdbSnsrEepInfo_Start+13];
//    PedlSimrPOffsEepInfo.PedlSimrPOffsEolReadVal = input[IdbSnsrEepInfo_Start+14];
//
//    PedlSimrPOffsEepInfo.PedlSimrPOffsDrvgWrReqCmpldFlg = input[IdbSnsrEepInfo_Start+15];
//    PedlSimrPOffsEepInfo.PedlSimrPOffsDrvgReadInvldFlg = input[IdbSnsrEepInfo_Start+16];
//    PedlSimrPOffsEepInfo.PedlSimrPOffsDrvgReadVal = input[IdbSnsrEepInfo_Start+17];
//
//    PistPOffsEepInfo.PistPOffsEolWrReqCmpldFlg = input[IdbSnsrEepInfo_Start+18];
//    PistPOffsEepInfo.PistPOffsEolReadInvldFlg = input[IdbSnsrEepInfo_Start+19];
//    PistPOffsEepInfo.PistPOffsEolReadVal = input[IdbSnsrEepInfo_Start+20];
//
//    PistPOffsEepInfo.PistPOffsDrvgWrReqCmpldFlg = input[IdbSnsrEepInfo_Start+21];
//    PistPOffsEepInfo.PistPOffsDrvgReadInvldFlg = input[IdbSnsrEepInfo_Start+22];
//    PistPOffsEepInfo.PistPOffsDrvgReadVal = input[IdbSnsrEepInfo_Start+23];
//
//    CircPOffsEepInfo.PrimCircPOffsEolWrReqCmpldFlg = input[IdbSnsrEepInfo_Start+24];
//    CircPOffsEepInfo.PrimCircPOffsEolReadInvldFlg = input[IdbSnsrEepInfo_Start+25];
//    CircPOffsEepInfo.PrimCircPOffsEolReadVal = input[IdbSnsrEepInfo_Start+26];
//
//    CircPOffsEepInfo.PrimCircPOffsDrvgWrReqCmpldFlg = input[IdbSnsrEepInfo_Start+27];
//    CircPOffsEepInfo.PrimCircPOffsDrvgReadInvldFlg = input[IdbSnsrEepInfo_Start+28];
//    CircPOffsEepInfo.PrimCircPOffsDrvgReadVal = input[IdbSnsrEepInfo_Start+29];
//
//    CircPOffsEepInfo.SecdCircPOffsEolWrReqCmpldFlg = input[IdbSnsrEepInfo_Start+30];
//    CircPOffsEepInfo.SecdCircPOffsEolReadInvldFlg = input[IdbSnsrEepInfo_Start+31];
//    CircPOffsEepInfo.SecdCircPOffsEolReadVal = input[IdbSnsrEepInfo_Start+32];
//
//    CircPOffsEepInfo.SecdCircPOffsDrvgWrReqCmpldFlg = input[IdbSnsrEepInfo_Start+33];
//    CircPOffsEepInfo.SecdCircPOffsDrvgReadInvldFlg = input[IdbSnsrEepInfo_Start+34];
//    CircPOffsEepInfo.SecdCircPOffsDrvgReadVal = input[IdbSnsrEepInfo_Start+35];
//
//    /*Sensor Invalid*/
//
//    IdbSnsrInvalidInfo_Start=97;
//    PedlTrvlSigInvldInfo.PdtSigFaultFlg = input[IdbSnsrInvalidInfo_Start+0];
//    PedlTrvlSigInvldInfo.PdtSigSuspcFlg = input[IdbSnsrInvalidInfo_Start+1];//8
//    PedlTrvlSigInvldInfo.PdfSigFaultFlg = input[IdbSnsrInvalidInfo_Start+2];
//    PedlTrvlSigInvldInfo.PdfSigSuspcFlg = input[IdbSnsrInvalidInfo_Start+3];//3
//    PedlSimrPInvldInfo.PedlSimrPSigFaultFlg = input[IdbSnsrInvalidInfo_Start+4];
//    PedlSimrPInvldInfo.PedlSimrPSigSuspcFlg = input[IdbSnsrInvalidInfo_Start+5];//3
//
//    PistPInvldInfo.PistPSigFaultFlg = input[IdbSnsrInvalidInfo_Start+6];
//    PistPInvldInfo.PistPSigSuspcFlg = input[IdbSnsrInvalidInfo_Start+7];//8
//    CircPInvldInfo.PrimCircPSigFaultFlg = input[IdbSnsrInvalidInfo_Start+8];
//    CircPInvldInfo.PrimCircPSigSuspcFlg = input[IdbSnsrInvalidInfo_Start+9];//8
//    CircPInvldInfo.SecdCircPSigFaultFlg = input[IdbSnsrInvalidInfo_Start+10];
//    CircPInvldInfo.SecdCircPSigSuspcFlg = input[IdbSnsrInvalidInfo_Start+11];//8
//
//    BlsInvldInfo.BlsSigFault = input[IdbSnsrInvalidInfo_Start+12];
//    BlsInvldInfo.BlsSigSuspc = input[IdbSnsrInvalidInfo_Start+13];
//
//    WhlSpdInvldInfo.WhlSpdInvldFrntLe = input[IdbSnsrInvalidInfo_Start+14];
//    WhlSpdInvldInfo.WhlSpdInvldFrntRi = input[IdbSnsrInvalidInfo_Start+15];
//    WhlSpdInvldInfo.WhlSpdInvldReLe = input[IdbSnsrInvalidInfo_Start+16];
//    WhlSpdInvldInfo.WhlSpdInvldReRi = input[IdbSnsrInvalidInfo_Start+17];
//    /* CAN Interface Info(17) **********************************************************************/
//
//    CANIfInfo_Start=115;
//
//    CANIfInfo.AccrPedlPosn = input[CANIfInfo_Start+0];
//    CANIfInfo.AccrPedlPosnSigFaultFlg = input[CANIfInfo_Start+1];
//    CANIfInfo.AccrPedlPosnSigSuspcFlg = input[CANIfInfo_Start+2];
//
//    CANIfInfo.FlexBrkInfo.FlexBrkSwtFaultDet = input[CANIfInfo_Start+3];
//    CANIfInfo.FlexBrkInfo.FlexBrkSwtRaw1 = input[CANIfInfo_Start+4];
//    CANIfInfo.FlexBrkInfo.FlexBrkSwtRaw2 = input[CANIfInfo_Start+5];
//
//    CANIfInfo.GearStSeld = input[CANIfInfo_Start+6];
//    CANIfInfo.GearStPosn = input[CANIfInfo_Start+7];
//    CANIfInfo.GearStInvldFlg = input[CANIfInfo_Start+8];
//
//    CANIfInfo.HCUServiceMod = input[CANIfInfo_Start+9];
//    CANIfInfo.LFU = input[CANIfInfo_Start+10];
//
//    CANIfInfo.TempInfo.CooltT = input[CANIfInfo_Start+11];
//    CANIfInfo.TempInfo.CooltTSigFaultFlg = input[CANIfInfo_Start+12];
//    CANIfInfo.TempInfo.OutdSnsrT = input[CANIfInfo_Start+13];
//    CANIfInfo.TempInfo.OutdSnsrTSigFaultFlg = input[CANIfInfo_Start+14];
//
//    CANIfInfo.SubCanBusSigFaultFlg = input[CANIfInfo_Start+15];
//    CANIfInfo.EngMsgFaultFlg = input[CANIfInfo_Start+16];
//    /* Regen Brake controller(2) **********************************************************************/
//
//    RgnBrkCtrlrIfInfo_Start=132;
//    RgnBrkCtrlrIfInfo.ActRgnBrkTqFromMotDrvgSys = input[RgnBrkCtrlrIfInfo_Start+0];
//    RgnBrkCtrlrIfInfo.ActRgnBrkTqChk = input[RgnBrkCtrlrIfInfo_Start+1];
//
//    /* Diag Info(2) *************************************************************************************/
//
//    EepDiagIfInfo_Start=134;
//    IdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg = input[EepDiagIfInfo_Start+0];
//    IdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg = input[EepDiagIfInfo_Start+1];
//
//    /* Pedal/Pressure Sensor Raw(26) *************************************************************************************/
//
//
//    IdbSnsrRawSigInfo_Start=136;
//
//    PedlTrvlRawInfo.PdtRaw = input[IdbSnsrRawSigInfo_Start+0];
//    PedlTrvlRawInfo.PdtRaw1ms[0] = input[IdbSnsrRawSigInfo_Start+1];
//    PedlTrvlRawInfo.PdtRaw1ms[1] = input[IdbSnsrRawSigInfo_Start+2];
//    PedlTrvlRawInfo.PdtRaw1ms[2] = input[IdbSnsrRawSigInfo_Start+3];
//    PedlTrvlRawInfo.PdtRaw1ms[3] = input[IdbSnsrRawSigInfo_Start+4];
//    PedlTrvlRawInfo.PdtRaw1ms[4] = input[IdbSnsrRawSigInfo_Start+5];
//    PedlTrvlRawInfo.PdfRaw = input[IdbSnsrRawSigInfo_Start+6];
//    PedlSimrPRaw = input[IdbSnsrRawSigInfo_Start+7];
//    PistPRawInfo.PistPRaw = input[IdbSnsrRawSigInfo_Start+8];
//    PistPRawInfo.PistPRaw1ms[0] = input[IdbSnsrRawSigInfo_Start+9];
//    PistPRawInfo.PistPRaw1ms[1] = input[IdbSnsrRawSigInfo_Start+10];
//    PistPRawInfo.PistPRaw1ms[2] = input[IdbSnsrRawSigInfo_Start+11];
//    PistPRawInfo.PistPRaw1ms[3] = input[IdbSnsrRawSigInfo_Start+12];
//    PistPRawInfo.PistPRaw1ms[4] = input[IdbSnsrRawSigInfo_Start+13];
//    CircPRawInfo.PrimCircPRaw = input[IdbSnsrRawSigInfo_Start+14];
//    CircPRawInfo.PrimCircPRaw1ms[0] = input[IdbSnsrRawSigInfo_Start+15];
//    CircPRawInfo.PrimCircPRaw1ms[1] = input[IdbSnsrRawSigInfo_Start+16];
//    CircPRawInfo.PrimCircPRaw1ms[2] = input[IdbSnsrRawSigInfo_Start+17];
//    CircPRawInfo.PrimCircPRaw1ms[3] = input[IdbSnsrRawSigInfo_Start+18];
//    CircPRawInfo.PrimCircPRaw1ms[4] = input[IdbSnsrRawSigInfo_Start+19];
//    CircPRawInfo.SecdCircPRaw = input[IdbSnsrRawSigInfo_Start+20];
//    CircPRawInfo.SecdCircPRaw1ms[0] = input[IdbSnsrRawSigInfo_Start+21];
//    CircPRawInfo.SecdCircPRaw1ms[1] = input[IdbSnsrRawSigInfo_Start+22];
//    CircPRawInfo.SecdCircPRaw1ms[2] = input[IdbSnsrRawSigInfo_Start+23];
//    CircPRawInfo.SecdCircPRaw1ms[3] = input[IdbSnsrRawSigInfo_Start+24];
//    CircPRawInfo.SecdCircPRaw1ms[4] = input[IdbSnsrRawSigInfo_Start+25];
//
//    /* Wheel Speed Raw(8) *************************************************************************************/
//    WhlSpdRawInfo.WhlSpdRawFrntLe = input[IdbSnsrRawSigInfo_Start+26];
//    WhlSpdRawInfo.WhlSpdRawFrntRi = input[IdbSnsrRawSigInfo_Start+27];
//    WhlSpdRawInfo.WhlSpdRawReLe = input[IdbSnsrRawSigInfo_Start+28];
//    WhlSpdRawInfo.WhlSpdRawReRi = input[IdbSnsrRawSigInfo_Start+29];
//
//    /* BLS Invalid Info(3) **********************************************************************/
//
//
//    BlsIgnStInfo_Start=166;
//    BlsRaw = input[BlsIgnStInfo_Start+0];
//
//    /* ETC(1) *************************************************************************************/
//    VehIgnSt = input[BlsIgnStInfo_Start+1];


	/*Motor 5ms*/


}
void RteWriteTask5msec(void)
{
	output[1]=Task5msChkCnt;
//    /* BBC(7) */
//    output[0] = BaseBrkCtrlrActFlg;
//    output[1] = VlvActtnReqByBaseBrkCtrlrInfo.PedlSimrPGendOnFlg+2;
//    output[2] = BaseBrkCtrlModInfo.VehStandStillStFlg;
//    output[3] = BaseBrkCtrlModInfo.LowSpdCtrlInhibitFlg+2;
//    output[4] = BaseBrkCtrlModInfo.StopdVehCtrlModFlg+4;
//    output[5] = TarPFromBrkPedl;
//    output[6] = TarPFromBaseBrkCtrlr;
//
//    /* Detection(59) */
//    output[7] = PedlTrvlRateInfo.PedlTrvRate1ms[0]         ;
//    output[8] = PedlTrvlRateInfo.PedlTrvRate1ms[1]         ;
//    output[9] = PedlTrvlRateInfo.PedlTrvRate1ms[2]         ;
//    output[10] = PedlTrvlRateInfo.PedlTrvRate1ms[3]         ;
//    output[11] = PedlTrvlRateInfo.PedlTrvRate1ms[4]         ;
//    output[12] = PedlTrvlRateInfo.PedlTrvlRate              ;
//    output[13] = PedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec;//3
//
//    output[14] = PistPRateInfo.PistPChgDurg10ms        ;
//    output[15] = PistPRateInfo.PistPChgDurg5ms         ;
//    output[16] = PistPRateInfo.PistPRate               ;
//    output[17] = PistPRateInfo.PistPRateBarPerSec      ;
//    output[18] = PistPRateInfo.PistPRate1ms_1_100Bar[0];
//    output[19] = PistPRateInfo.PistPRate1ms_1_100Bar[1];
//    output[20] = PistPRateInfo.PistPRate1ms_1_100Bar[2];
//    output[21] = PistPRateInfo.PistPRate1ms_1_100Bar[3];
//    output[22] = PistPRateInfo.PistPRate1ms_1_100Bar[4];
//    output[23] = PistPRateInfo.PistPRate1msAvg_1_100Bar;//6
//
//    output[24] = CircPRateInfo.PrimCircPChgDurg10ms        ;
//    output[25] = CircPRateInfo.PrimCircPChgDurg5ms         ;
//    output[26] = CircPRateInfo.PrimCircPRate               ;
//    output[27] = CircPRateInfo.PrimCircPRateBarPerSec      ;
//    output[28] = CircPRateInfo.PrimCircPRate1ms_1_100Bar[0];
//    output[29] = CircPRateInfo.PrimCircPRate1ms_1_100Bar[1];
//    output[30] = CircPRateInfo.PrimCircPRate1ms_1_100Bar[2];
//    output[31] = CircPRateInfo.PrimCircPRate1ms_1_100Bar[3];
//    output[32] = CircPRateInfo.PrimCircPRate1ms_1_100Bar[4];
//    output[33] = CircPRateInfo.PrimCircPRate1msAvg_1_100Bar;//6
//
//    output[34] = CircPRateInfo.SecdCircPChgDurg10ms        ;
//    output[35] = CircPRateInfo.SecdCircPChgDurg5ms         ;
//    output[36] = CircPRateInfo.SecdCircPRate               ;
//    output[37] = CircPRateInfo.SecdCircPRateBarPerSec      ;
//    output[38] = CircPRateInfo.SecdCircPRate1ms_1_100Bar[0];
//    output[39] = CircPRateInfo.SecdCircPRate1ms_1_100Bar[1];
//    output[40] = CircPRateInfo.SecdCircPRate1ms_1_100Bar[2];
//    output[41] = CircPRateInfo.SecdCircPRate1ms_1_100Bar[3];
//    output[42] = CircPRateInfo.SecdCircPRate1ms_1_100Bar[4];
//    output[43] = CircPRateInfo.SecdCircPRate1msAvg_1_100Bar;//6
//
//    output[44] = PedlTrvlTagInfo.PdtSigNoiseSuspcFlgH   ;
//    output[45] = PedlTrvlTagInfo.PdtSigNoiseSuspcFlgL   ;
//    output[46] = PedlTrvlTagInfo.PedlSimrPSigNoiseSuspcFlgH  ;
//    output[47] = PedlTrvlTagInfo.PdfSigNoiseSuspcFlgH  ;
//    output[48] = PedlTrvlTagInfo.PdfSigNoiseSuspcFlgL;
//    output[49] = PedlTrvlTagInfo.PedlSimrPSigNoiseSuspcFlgL;//6
//
//    output[50] = PedlTrvlTagInfo.PdtSigTag             ;
//    output[51] = PedlTrvlTagInfo.PdfSigTag             ;
//    output[52] = PedlTrvlTagInfo.PedlInfoSigTag        ;
//    output[53] = PedlTrvlTagInfo.PedlSigTag            ;
//    output[54] = PedlTrvlTagInfo.PedlSimrPSigTag       ;//5
//
//    output[55] = PedlTrvlFinal;
//
//    output[56] = VehStopStInfo.VehStandStillStFlg;
//    output[57] = VehStopStInfo.VehStopStFlg;
//    output[58] = VehSpdFild;//3
//
//    output[59] = BrkPedlStatusInfo.BrkPedlStatus;
//    output[60] = BrkPedlStatusInfo.PanicBrkSt1msFlg;
//    output[61] = BrkPedlStatusInfo.PanicBrkStFlg;
//    output[62] = BrkPedlStatusInfo.PedlReldStFlg1;
//    output[63] = BrkPedlStatusInfo.PedlReldStFlg2;
//    output[64] = BrkPedlStatusInfo.DrvrIntendToRelsBrk;
//    output[65] = BrkPedlStatusInfo.PedlReldStUsingPedlSimrPFlg;
//
//    /* Signal Processing(81) */
//    output[66] = PedlTrvlOffsCorrdInfo.PdtRawOffsCorrd        ;
//    output[67] = PedlTrvlOffsCorrdInfo.PdtRawOffsCorrd1ms[0]  ;
//    output[68] = PedlTrvlOffsCorrdInfo.PdtRawOffsCorrd1ms[1] ;
//    output[69] = PedlTrvlOffsCorrdInfo.PdtRawOffsCorrd1ms[2] ;
//    output[70] = PedlTrvlOffsCorrdInfo.PdtRawOffsCorrd1ms[3] ;
//    output[71] = PedlTrvlOffsCorrdInfo.PdtRawOffsCorrd1ms[4];
//    output[72] = PedlTrvlOffsCorrdInfo.PdfRawOffsCorrd  ;
//
//    output[73] = PedlTrvlOffsCorrdInfo.PdtOffsCorrdStOkFlg  ;
//    output[74] = PedlTrvlOffsCorrdInfo.PdfOffsCorrdStOkFlg ;//9
//
//    output[75] = PedlTrvlFildInfo.PdtFild       ;
//    output[76] = PedlTrvlFildInfo.PdfFild       ;
//    output[77] = PedlTrvlFildInfo.PdtFild1ms[0] ;
//    output[78] = PedlTrvlFildInfo.PdtFild1ms[1] ;
//    output[79] = PedlTrvlFildInfo.PdtFild1ms[2] ;
//    output[80] = PedlTrvlFildInfo.PdtFild1ms[3] ;
//    output[81] = PedlTrvlFildInfo.PdtFild1ms[4] ;//7
//
//    output[82] = PedlSimrPOffsCorrdInfo.PedlSimrPOffsCorrd;
//    output[83] = PedlSimrPOffsCorrdInfo.PedlSimrPOffsCorrdStOkFlg;
//    output[84] = PedlSimrPFildInfo.PedlSimrPFild;
//    output[85] = PedlSimrPFildInfo.PedlSimrPFild_1_100Bar;//4
//
//    output[86] = PistPOffsCorrdInfo.PistPOffsCorrd ;
//    output[87] = PistPOffsCorrdInfo.PistPOffsCorrd1ms[0];
//    output[88] = PistPOffsCorrdInfo.PistPOffsCorrd1ms[1];
//    output[89] = PistPOffsCorrdInfo.PistPOffsCorrd1ms[2];
//    output[90] = PistPOffsCorrdInfo.PistPOffsCorrd1ms[3];
//    output[91] = PistPOffsCorrdInfo.PistPOffsCorrd1ms[4] ;
//    output[92] = PistPOffsCorrdInfo.PistPOffsCorrdStOkFlg ;//
//
//    output[93] = PistPFildInfo.PistPFild         ;
//    output[94] = PistPFildInfo.PistPFild_1_100Bar;
//    output[95] = PistPFildInfo.PistPFild1ms[0]   ;
//    output[96] = PistPFildInfo.PistPFild1ms[1]   ;
//    output[97] = PistPFildInfo.PistPFild1ms[2]   ;
//    output[98] = PistPFildInfo.PistPFild1ms[3]   ;
//    output[99] = PistPFildInfo.PistPFild1ms[4]   ;//14
//
//    output[100] = CircPOffsCorrdInfo.PrimCircPOffsCorrd    ;
//    output[101] = CircPOffsCorrdInfo.PrimCircPOffsCorrd1ms[0] ;
//    output[102] = CircPOffsCorrdInfo.PrimCircPOffsCorrd1ms[1] ;
//    output[103] = CircPOffsCorrdInfo.PrimCircPOffsCorrd1ms[2]  ;
//    output[104] = CircPOffsCorrdInfo.PrimCircPOffsCorrd1ms[3] ;
//    output[105] = CircPOffsCorrdInfo.PrimCircPOffsCorrd1ms[4] ;
//
//    output[106] = CircPFildInfo.PrimCircPFild         ;
//    output[107] = CircPFildInfo.PrimCircPFild_1_100Bar;
//    output[108] = CircPFildInfo.PrimCircPFild1ms[0]   ;
//    output[109] = CircPFildInfo.PrimCircPFild1ms[1]   ;
//    output[110] = CircPFildInfo.PrimCircPFild1ms[2]   ;
//    output[111] = CircPFildInfo.PrimCircPFild1ms[3]   ;
//    output[112] = CircPFildInfo.PrimCircPFild1ms[4]   ;
//
//    output[113] = CircPOffsCorrdInfo.PrimCircPOffsCorrdStOkFlg;
//    output[114] = 0;//CircPOffsCorrdInfo.PrimCircPOffsCorrdStOkFlg ;//15
//
//    output[115] = CircPOffsCorrdInfo.SecdCircPOffsCorrd  ;
//    output[116] = CircPOffsCorrdInfo.SecdCircPOffsCorrd1ms[0]  ;
//    output[117] = CircPOffsCorrdInfo.SecdCircPOffsCorrd1ms[1] ;
//    output[118] = CircPOffsCorrdInfo.SecdCircPOffsCorrd1ms[2]  ;
//    output[119] = CircPOffsCorrdInfo.SecdCircPOffsCorrd1ms[3] ;
//    output[120] = CircPOffsCorrdInfo.SecdCircPOffsCorrd1ms[4] ;
//
//    output[121] = CircPFildInfo.SecdCircPFild         ;
//    output[122] = CircPFildInfo.SecdCircPFild_1_100Bar;
//    output[123] = CircPFildInfo.SecdCircPFild1ms[0]   ;
//    output[124] = CircPFildInfo.SecdCircPFild1ms[1]   ;
//    output[125] = CircPFildInfo.SecdCircPFild1ms[2]   ;
//    output[126] = CircPFildInfo.SecdCircPFild1ms[3]   ;
//    output[127] = CircPFildInfo.SecdCircPFild1ms[4]   ;
//
//    output[128] = CircPOffsCorrdInfo.SecdCircPOffsCorrdStOkFlg;
//    output[129] = 0;//CircPOffsCorrdInfo.SecdCircPOffsCorrdStOkFlg;//15
//
//    output[130] = IdbSnsrOffsEepWrReqInfo.PdtOffsEolEepWrReqFlg       ;
//    output[131] = IdbSnsrOffsEepWrReqInfo.PdfOffsEolEepWrReqFlg       ;
//    output[132] = IdbSnsrOffsEepWrReqInfo.PdtOffsDrvgEepWrReqFlg      ;
//    output[133] = IdbSnsrOffsEepWrReqInfo.PdfOffsDrvgEepWrReqFlg      ;
//
//    output[134] = IdbSnsrOffsEepWrReqInfo.PedlSimrPOffsEolEepWrReqFlg ;
//    output[135] = IdbSnsrOffsEepWrReqInfo.PedlSimrPOffsDrvgEepWrReqFlg;
//    output[136] = IdbSnsrOffsEepWrReqInfo.PistPOffsEolEepWrReqFlg ;
//    output[137] = IdbSnsrOffsEepWrReqInfo.PistPOffsDrvgEepWrReqFlg;
//    output[138] = IdbSnsrOffsEepWrReqInfo.PrimCircPOffsEolEepWrReqFlg ;
//    output[139] = IdbSnsrOffsEepWrReqInfo.PrimCircPOffsDrvgEepWrReqFlg;
//    output[140] = IdbSnsrOffsEepWrReqInfo.SecdCircPOffsEolEepWrReqFlg ;
//    output[141] = IdbSnsrOffsEepWrReqInfo.SecdCircPOffsDrvgEepWrReqFlg;//12
//
//    output[142] = WhlSpdFildInfo.WhlSpdFildFrntLe;
//    output[143] = WhlSpdFildInfo.WhlSpdFildFrntRi;
//    output[144] = WhlSpdFildInfo.WhlSpdFildReLe  ;
//    output[145] = WhlSpdFildInfo.WhlSpdFildReRi  ;//4
//
//    output[146] = BlsFild                        ;//1
//
//    /* Arbitrator(13) */
//    output[147] = FinalTarPInfo.FinalTarPOfPrim ;
//    output[148] = FinalTarPInfo.FinalTarPOfSecdy;
//
//    output[149] = FinalTarPInfo.FinalTarPOfWhlFrntLe;
//    output[150] = FinalTarPInfo.FinalTarPOfWhlFrntRi ;
//    output[151] = FinalTarPInfo.FinalTarPOfWhlReLe;
//    output[152] = FinalTarPInfo.FinalTarPOfWhlReRi;
//
//    output[153] = FinalTarPInfo.FinalTarPRateInfo.FinalTarPRateOfPrim10ms;
//    output[154] = FinalTarPInfo.FinalTarPRateInfo.FinalTarPRateOfSecdy10ms;
//    output[155] = FinalTarPInfo.FinalTarPRateInfo.FinalTarPRateOfPrim5ms ;
//    output[156] = FinalTarPInfo.FinalTarPRateInfo.FinalTarPRateOfSecdy5ms ;
//
//    output[157] = FastBrkPRespRqrdModInfo.NeedFastRespForAvh ;
//    output[158] = FastBrkPRespRqrdModInfo.NeedFastRespForEsc;
//    output[159] = SeldMapOfBrkPedlToBrkP;
//
//    /* Valve Actuation(25) */
//    output[160] = IdbVlvDrvReqInfo.CircVlv1DrvReqOutpI[0];
//    output[161] = IdbVlvDrvReqInfo.CircVlv1DrvReqOutpI[1];
//    output[162] = IdbVlvDrvReqInfo.CircVlv1DrvReqOutpI[2];
//    output[163] = IdbVlvDrvReqInfo.CircVlv1DrvReqOutpI[3];
//    output[164] = IdbVlvDrvReqInfo.CircVlv1DrvReqOutpI[4];
//
//    output[165] = IdbVlvDrvReqInfo.CircVlv2DrvReqOutpI[0];
//    output[166] = IdbVlvDrvReqInfo.CircVlv2DrvReqOutpI[1];
//    output[167] = IdbVlvDrvReqInfo.CircVlv2DrvReqOutpI[2];
//    output[168] = IdbVlvDrvReqInfo.CircVlv2DrvReqOutpI[3];
//    output[169] = IdbVlvDrvReqInfo.CircVlv2DrvReqOutpI[4];
//
//    output[170] = IdbVlvDrvReqInfo.CutVlv1DrvReqOutpI[0] ;
//    output[171] = IdbVlvDrvReqInfo.CutVlv1DrvReqOutpI[1] ;
//    output[172] = IdbVlvDrvReqInfo.CutVlv1DrvReqOutpI[2] ;
//    output[173] = IdbVlvDrvReqInfo.CutVlv1DrvReqOutpI[3] ;
//    output[174] = IdbVlvDrvReqInfo.CutVlv1DrvReqOutpI[4] ;
//
//    output[175] = IdbVlvDrvReqInfo.CutVlv2DrvReqOutpI[0] ;
//    output[176] = IdbVlvDrvReqInfo.CutVlv2DrvReqOutpI[1] ;
//    output[177] = IdbVlvDrvReqInfo.CutVlv2DrvReqOutpI[2] ;
//    output[178] = IdbVlvDrvReqInfo.CutVlv2DrvReqOutpI[3] ;
//    output[179] = IdbVlvDrvReqInfo.CutVlv2DrvReqOutpI[4] ;
//
//    output[180] = IdbVlvDrvReqInfo.SimrVlv1DrvReqOutpI[0];
//    output[181] = IdbVlvDrvReqInfo.SimrVlv1DrvReqOutpI[1];
//    output[182] = IdbVlvDrvReqInfo.SimrVlv1DrvReqOutpI[2];
//    output[183] = IdbVlvDrvReqInfo.SimrVlv1DrvReqOutpI[3];
//    output[184] = IdbVlvDrvReqInfo.SimrVlv1DrvReqOutpI[4];
//
//    /* Pressure Control(17) */
//    output[185] = TarDeltaStk;
//    output[186] = PCtrlBoostMod;
//    output[186] = PCtrlSt;
//    output[186] = InitQuickBrkDctFlg;
//    output[186] = VlvActtnReqByBaseBrkCtrlrInfo.CutVlvCloseDlyForQuickBrk;
//
//    output[186] = StkRecvryActnIfInfo.StkRecvryEmgyEntryReqFlg;
//    output[186] = StkRecvryActnIfInfo.StkRecvryReqCmpldFlg;
//    output[186] = StkRecvryActnIfInfo.MotActnReqForStkRecvry;
//
//    output[187] = IdbCircVlv1CtrlStInfo.VlvActFlg;
//    output[188] = IdbCircVlv1CtrlStInfo.VlvOnTi;
//    output[189] = IdbCircVlv1CtrlStInfo.VlvCtrlTarI;
//
//    output[190] = IdbCircVlv2CtrlStInfo.VlvActFlg ;
//    output[191] = IdbCircVlv2CtrlStInfo.VlvOnTi ;
//    output[192] = IdbCircVlv2CtrlStInfo.VlvCtrlTarI;
//
//    output[193] = IdbCutVlv1CtrlStInfo.VlvActFlg ;
//    output[194] = IdbCutVlv1CtrlStInfo.VlvOnTi;
//    output[195] = IdbCutVlv1CtrlStInfo.VlvCtrlTarI ;
//
//    output[196] = IdbCutVlv2CtrlStInfo.VlvActFlg ;
//    output[197] = IdbCutVlv2CtrlStInfo.VlvOnTi ;
//    output[198] = IdbCutVlv2CtrlStInfo.VlvCtrlTarI ;
//
//    output[199] = IdbSimrVlvCtrlStInfo.VlvActFlg ;
//    output[200] = IdbSimrVlvCtrlStInfo.VlvOnTi ;
//    output[201] = IdbSimrVlvCtrlStInfo.VlvCtrlTarI ;
//
//    /* RBC(4) */
//    output[202] = BrkPRednForBaseBrkCtrlr;
//    output[203] = RgnBrkCtrlrActStFlg ;
//    output[204] = TarRgnBrkTqInfo.TarRgnBrkTq;
//    output[205] = RgnBrkCtlrBlendgFlg;
//


//    /* Debug */
//    output[206] = PdtOffsEolCalc.lsahbs16AHBofsFirstCal;
//    output[207] = PdtOffsEolEep.lsahbs16AHBEolEEPofsReadRaw;
//    output[208] = PdtOffsEolCalc.lsahbu1EEPofsCalcFWStartFlg;
//    output[209] = PdtOffsEolCalc.lsahbu1AHBEepOfsCalOkFlg;
//    output[210] = PdtOffsEolCalc.lsahbu1AHBEepOfsCalEndFlg;
//
}
    
#endif
