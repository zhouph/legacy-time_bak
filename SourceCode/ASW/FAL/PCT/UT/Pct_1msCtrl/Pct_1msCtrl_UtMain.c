#include "unity.h"
#include "unity_fixture.h"
#include "Pct_1msCtrl.h"
#include "Pct_1msCtrl_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Rtesint32 UtInput_Pct_1msCtrlPedlTrvlFild1msInfo_PdtFild1ms[MAX_STEP] = PCT_1MSCTRLPEDLTRVLFILD1MSINFO_PDTFILD1MS;
const Rtesint32 UtInput_Pct_1msCtrlPedlTrvlRate1msInfo_PedlTrvlRate1msMmPerSec[MAX_STEP] = PCT_1MSCTRLPEDLTRVLRATE1MSINFO_PEDLTRVLRATE1MSMMPERSEC;
const Mom_HndlrEcuModeSts_t UtInput_Pct_1msCtrlEcuModeSts[MAX_STEP] = PCT_1MSCTRLECUMODESTS;
const Pct_5msCtrlPCtrlSt_t UtInput_Pct_1msCtrlPCtrlSt[MAX_STEP] = PCT_1MSCTRLPCTRLST;

const Pct_1msCtrlPreBoostMod_t UtExpected_Pct_1msCtrlPreBoostMod[MAX_STEP] = PCT_1MSCTRLPREBOOSTMOD;



TEST_GROUP(Pct_1msCtrl);
TEST_SETUP(Pct_1msCtrl)
{
    Pct_1msCtrl_Init();
}

TEST_TEAR_DOWN(Pct_1msCtrl)
{   /* Postcondition */

}

TEST(Pct_1msCtrl, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Pct_1msCtrlPedlTrvlFild1msInfo.PdtFild1ms = UtInput_Pct_1msCtrlPedlTrvlFild1msInfo_PdtFild1ms[i];
        Pct_1msCtrlPedlTrvlRate1msInfo.PedlTrvlRate1msMmPerSec = UtInput_Pct_1msCtrlPedlTrvlRate1msInfo_PedlTrvlRate1msMmPerSec[i];
        Pct_1msCtrlEcuModeSts = UtInput_Pct_1msCtrlEcuModeSts[i];
        Pct_1msCtrlPCtrlSt = UtInput_Pct_1msCtrlPCtrlSt[i];

        Pct_1msCtrl();

        TEST_ASSERT_EQUAL(Pct_1msCtrlPreBoostMod, UtExpected_Pct_1msCtrlPreBoostMod[i]);
    }
}

TEST_GROUP_RUNNER(Pct_1msCtrl)
{
    RUN_TEST_CASE(Pct_1msCtrl, All);
}
