/**
 * @defgroup Pct_Types Pct_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Pct_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PCT_TYPES_H_
#define PCT_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Asw_Types.h" /* HSH */
#include "Bsw_Types.h"
#include "Rte_Types.h"
#include "RteDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

typedef struct
{
	uint8_t  u8ValveAct;
	uint8_t  u8ValveOnTime;
	uint16_t u16Current;
}LA_VALVE_CMD_t;

typedef struct
{
/* Input Data Element */
    Spc_1msCtrlPedlTrvlFild1msInfo_t Pct_1msCtrlPedlTrvlFild1msInfo;
    Det_1msCtrlPedlTrvlRate1msInfo_t Pct_1msCtrlPedlTrvlRate1msInfo;
    Mom_HndlrEcuModeSts_t Pct_1msCtrlEcuModeSts;
    Pct_5msCtrlPCtrlSt_t Pct_1msCtrlPCtrlSt;

/* Output Data Element */
    Pct_1msCtrlPreBoostMod_t Pct_1msCtrlPreBoostMod;
}Pct_1msCtrl_HdrBusType;

typedef struct
{
/* Input Data Element */
    Abc_CtrlWhlVlvReqAbcInfo_t Pct_5msCtrlWhlVlvReqAbcInfo;
    Eem_MainEemFailData_t Pct_5msCtrlEemFailData;
    Spc_1msCtrlPedlTrvlFild1msInfo_t Pct_5msCtrlPedlTrvlFild1msInfo;
    Proxy_RxCanRxAccelPedlInfo_t Pct_5msCtrlCanRxAccelPedlInfo;
    Msp_CtrlMotRotgAgSigInfo_t Pct_5msCtrlMotRotgAgSigInfo;
    Abc_CtrlAbsCtrlInfo_t Pct_5msCtrlAbsCtrlInfo;
    Abc_CtrlAvhCtrlInfo_t Pct_5msCtrlAvhCtrlInfo;
    Abc_CtrlEscCtrlInfo_t Pct_5msCtrlEscCtrlInfo;
    Abc_CtrlHsaCtrlInfo_t Pct_5msCtrlHsaCtrlInfo;
    Abc_CtrlStkRecvryCtrlIfInfo_t Pct_5msCtrlStkRecvryCtrlIfInfo;
    Abc_CtrlTcsCtrlInfo_t Pct_5msCtrlTcsCtrlInfo;
    Bbc_CtrlBaseBrkCtrlModInfo_t Pct_5msCtrlBaseBrkCtrlModInfo;
    Det_5msCtrlBrkPedlStatusInfo_t Pct_5msCtrlBrkPedlStatusInfo;
    Det_1msCtrlBrkPedlStatus1msInfo_t Pct_5msCtrlBrkPedlStatus1msInfo;
    Spc_5msCtrlCircPFildInfo_t Pct_5msCtrlCircPFildInfo;
    Det_5msCtrlCircPRateInfo_t Pct_5msCtrlCircPRateInfo;
    Det_1msCtrlPedlTrvlRate1msInfo_t Pct_5msCtrlPedlTrvlRate1msInfo;
    Det_5msCtrlEstimdWhlPInfo_t Pct_5msCtrlEstimdWhlPInfo;
    Arb_CtrlFinalTarPInfo_t Pct_5msCtrlFinalTarPInfo;
    Mcc_1msCtrlIdbMotPosnInfo_t Pct_5msCtrlIdbMotPosnInfo;
    Arb_CtrlWhlOutVlvCtrlTarInfo_t Pct_5msCtrlWhlOutVlvCtrlTarInfo;
    Arb_CtrlWhlPreFillInfo_t Pct_5msCtrlWhlPreFillInfo;
    Vat_CtrlIdbVlvActInfo_t Pct_5msCtrlIdbVlvActInfo;
    Spc_5msCtrlPedlSimrPFildInfo_t Pct_5msCtrlPedlSimrPFildInfo;
    Det_5msCtrlPedlTrvlRateInfo_t Pct_5msCtrlPedlTrvlRateInfo;
    Det_5msCtrlPedlTrvlTagInfo_t Pct_5msCtrlPedlTrvlTagInfo;
    Spc_5msCtrlPistPFildInfo_t Pct_5msCtrlPistPFildInfo;
    Det_5msCtrlPistPRateInfo_t Pct_5msCtrlPistPRateInfo;
    Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo_t Pct_5msCtrlVlvActtnReqByBaseBrkCtrlrInfo;
    Spc_5msCtrlWhlSpdFildInfo_t Pct_5msCtrlWhlSpdFildInfo;
    Eem_MainEemSuspectData_t Pct_5msCtrlEemSuspectData;
    Mom_HndlrEcuModeSts_t Pct_5msCtrlEcuModeSts;
    Mcc_1msCtrlMotCtrlMode_t Pct_5msCtrlMotCtrlMode;
    Eem_SuspcDetnFuncInhibitPctrlSts_t Pct_5msCtrlFuncInhibitPctrlSts;
    Bbc_CtrlBaseBrkCtrlrActFlg_t Pct_5msCtrlBaseBrkCtrlrActFlg;
    Spc_5msCtrlBlsFild_t Pct_5msCtrlBlsFild;
    Rbc_CtrlBrkPRednForBaseBrkCtrlr_t Pct_5msCtrlBrkPRednForBaseBrkCtrlr;
    Mcc_1msCtrlMotICtrlFadeOutState_t Pct_5msCtrlMotICtrlFadeOutState;
    Pct_1msCtrlPreBoostMod_t Pct_5msCtrlPreBoostMod;
    Rbc_CtrlRgnBrkCtlrBlendgFlg_t Pct_5msCtrlRgnBrkCtlrBlendgFlg;
    Det_5msCtrlVehSpdFild_t Pct_5msCtrlVehSpdFild;
    Mcc_1msCtrlStkRecvryStabnEndOK_t Pct_5msCtrlStkRecvryStabnEndOK;

/* Output Data Element */
    Pct_5msCtrlIdbPCtrllVlvCtrlStInfo_t Pct_5msCtrlIdbPCtrllVlvCtrlStInfo;
    Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo_t Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo;
    Pct_5msCtrlWhlInVlvCtrlTarInfo_t Pct_5msCtrlWhlnVlvCtrlTarInfo;
    Pct_5msCtrlStkRecvryActnIfInfo_t Pct_5msCtrlStkRecvryActnIfInfo;
    Pct_5msCtrlMuxCmdExecStInfo_t Pct_5msCtrlMuxCmdExecStInfo;
    Pct_5msCtrlPCtrlAct_t Pct_5msCtrlPCtrlAct;
    Pct_5msCtrlInitQuickBrkDctFlg_t Pct_5msCtrlInitQuickBrkDctFlg;
    Pct_5msCtrlTgtDeltaStkType_t Pct_5msCtrlTgtDeltaStkType;
    Pct_5msCtrlPCtrlBoostMod_t Pct_5msCtrlPCtrlBoostMod;
    Pct_5msCtrlPCtrlFadeoutSt_t Pct_5msCtrlPCtrlFadeoutSt;
    Pct_5msCtrlPCtrlSt_t Pct_5msCtrlPCtrlSt;
    Pct_5msCtrlInVlvAllCloseReq_t Pct_5msCtrlInVlvAllCloseReq;
    Pct_5msCtrlPChamberVolume_t Pct_5msCtrlPChamberVolume;
    Pct_5msCtrlTarDeltaStk_t Pct_5msCtrlTarDeltaStk;
    Pct_5msCtrlFinalTarPFromPCtrl_t Pct_5msCtrlFinalTarPFromPCtrl;
}Pct_5msCtrl_HdrBusType;

typedef struct
{
	int8_t   s8PCtrlState;
	int8_t   s8PCtrlBoostMode;
    int16_t  s16CircuitPressureFail;

    int16_t  s16DeltaStkTar;
	int16_t  s8PChamberVolume;

    int8_t   s8PCtrlFadeoutState;
    uint16_t u16ValveFadeoutEndOK;
    uint16_t u16BoostToFadeOutReadyFlg;
    uint16_t u16FadeoutState1EndOK;
    uint16_t u16FadeoutState2EndOK;
    uint16_t u16MotorFadeoutEndOK;
    int16_t  s16InABSCtrlEnabled;
    int32_t  s32TatEstStrkPosiOfWhlP;
    int16_t  s16CVVOpenFlg;

    /* Added MUX Interface - Begin */
    uint8_t  u8PCtrlAct;				/* Target Pressure Valid																			*/
    int16_t  s16MuxPwrPistonTarP;		/* Power piston target pressure 																	*/
    int16_t  s16MuxPwrPistonTarPRate;	/* Power piston target pressure rate 																*/
    int16_t  s16MuxPwrPistonTarDP;		/* Delta target pressure of power piston 															*/
    int16_t  s16MuxWhlTarPFL;			/* Target wheel pressure - FL 																		*/
    int16_t  s16MuxWhlTarPFR;			/* Target wheel pressure - FR 																		*/
    int16_t  s16MuxWhlTarPRL;			/* Target wheel pressure - RL 																		*/
    int16_t  s16MuxWhlTarPRR;			/* Target wheel pressure - RR 																		*/
    int16_t  s16MuxWhlTarPRateFL;		/* Target wheel pressure rate - FL 																	*/
    int16_t  s16MuxWhlTarPRateFR;		/* Target wheel pressure rate - FR 																	*/
    int16_t  s16MuxWhlTarPRateRL;		/* Target wheel pressure rate - RL 																	*/
    int16_t  s16MuxWhlTarPRateRR;		/* Target wheel pressure rate - RR 																	*/
    int16_t  s16MuxWhlTarDelPFL;		/* Delta target pressure of wheels - FL 															*/
    int16_t  s16MuxWhlTarDelPFR;		/* Delta target pressure of wheels - FR 															*/
    int16_t  s16MuxWhlTarDelPRL;		/* Delta target pressure of wheels - RL 															*/
    int16_t  s16MuxWhlTarDelPRR;		/* Delta target pressure of wheels - RR 															*/
    uint16_t u16MuxWheelVolumeState;	/* Wheel volume state 																				*/
    uint8_t  u8MuxCircCtrlReqPri;		/* Circuit control request - Primary(Balance Valve Open Request) 									*/
    uint8_t  u8MuxCircCtrlReqSec;		/* Circuit control request - Secondary(Balance Valve Open Request) 									*/
    LA_VALVE_CMD_t st_MuxInVCmdFL;		/* Inlet valve actuation request - FL 																*/
    LA_VALVE_CMD_t st_MuxInVCmdFR;		/* Inlet valve actuation request - FR 																*/
    LA_VALVE_CMD_t st_MuxInVCmdRL;		/* Inlet valve actuation request - RL 																*/
    LA_VALVE_CMD_t st_MuxInVCmdRR;		/* Inlet valve actuation request - RR 																*/
    uint8_t  u8MultiplexCtrlActive;		/* Multiplex Control Active Flag																	*/
    int16_t s16MuxMode;					/* Multiplex mode - Multiplex channel informateion ex) 1, 2, 4 channel / default : 0(1channel) 		*/
    uint8_t u8AbsMtrCtrlMode;			/* Motor control mode request during ABS : Position or RPM Control @ 0, Torque Control @ 1 			*/
    int8_t  s8MuxCtrlState;				/* Multiplex control state : 	S8_MUX_MODE_NONCONTROL: 0
																		S8_MUX_MODE_PCTRL: 1
																		S8_MUX_MODE_PCTRL_TRANSITION: 2
																		   -. Time during Inlet valve close delay
																		S8_MUX_MODE_INHIBITION: -1											*/
    uint8_t u8WhlCtrlIndex;				/* Wheel Control Index : non -> 0, Pri -> 1, Sec -> 2, Both -> 3
    															 Front : 0x0- ~ 0x3-
    															 Rear  : 0x-0 ~ 0x-3
    															 Considering : Merge with wheel volume state								*/
	int16_t  s16ReqdPCtrlBoostMod;
    /* Added MUX Interface - End */

    /* Added stroke recovery Interface - Begin */
    int16_t s16StkRcvrFinalAllowedTime;		/*stroke recovery final allowed time															*/
    int16_t s16AllowedRcvrStk_mm;				/*stroke recovery final allowed Stroke															*/
    int16_t s16FwdRcvrMaxPosn_mm;				/*stroke recovery USL position																	*/
    int16_t s16BackwRcvrMinPosn_mm;			/*stroke recovery LSL position																	*/
    int16_t s16StkPosn_mm;					/*stroke converted pulse to millimeter															*/

    /* Added stroke recovery Interface - End */
}PressureControl_LocalVar;
/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PCT_TYPES_H_ */
/** @} */
