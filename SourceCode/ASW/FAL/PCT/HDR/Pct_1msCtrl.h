/**
 * @defgroup Pct_1msCtrl Pct_1msCtrl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Pct_1msCtrl.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PCT_1MSCTRL_H_
#define PCT_1MSCTRL_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Pct_Types.h"
#include "Pct_Cfg.h"
#include "Pct_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define PCT_1MSCTRL_MODULE_ID      (0)
 #define PCT_1MSCTRL_MAJOR_VERSION  (2)
 #define PCT_1MSCTRL_MINOR_VERSION  (0)
 #define PCT_1MSCTRL_PATCH_VERSION  (0)
 #define PCT_1MSCTRL_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Pct_1msCtrl_HdrBusType Pct_1msCtrlBus;

/* Version Info */
extern const SwcVersionInfo_t Pct_1msCtrlVersionInfo;

/* Input Data Element */
extern Spc_1msCtrlPedlTrvlFild1msInfo_t Pct_1msCtrlPedlTrvlFild1msInfo;
extern Det_1msCtrlPedlTrvlRate1msInfo_t Pct_1msCtrlPedlTrvlRate1msInfo;
extern Mom_HndlrEcuModeSts_t Pct_1msCtrlEcuModeSts;
extern Pct_5msCtrlPCtrlSt_t Pct_1msCtrlPCtrlSt;

/* Output Data Element */
extern Pct_1msCtrlPreBoostMod_t Pct_1msCtrlPreBoostMod;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Pct_1msCtrl_Init(void);
extern void Pct_1msCtrl(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PCT_1MSCTRL_H_ */
/** @} */
