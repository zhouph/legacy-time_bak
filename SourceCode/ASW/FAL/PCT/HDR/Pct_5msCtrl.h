/**
 * @defgroup Pct_5msCtrl Pct_5msCtrl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Pct_5msCtrl.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PCT_5MSCTRL_H_
#define PCT_5MSCTRL_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Pct_Types.h"
#include "Pct_Cfg.h"
#include "Pct_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define PCT_5MSCTRL_MODULE_ID      (0)
 #define PCT_5MSCTRL_MAJOR_VERSION  (2)
 #define PCT_5MSCTRL_MINOR_VERSION  (0)
 #define PCT_5MSCTRL_PATCH_VERSION  (0)
 #define PCT_5MSCTRL_BRANCH_VERSION (0)


#define M_BOOST_MODE_ORIGIN				0
#define M_BOOST_MODE_NEW				1

#define M_BOOST_MODE_SELECT				M_BOOST_MODE_NEW

#define S16_RECOVERY_MIN_ACT_TIME_PCT5MS	(S16_RECOVERY_MIN_ACT_TIME_MS/5)
#define S16_RECOVERY_END_REQ_TIME_PCT5MS    (S16_RECOVERY_END_REQ_TIME_MS/5)
#define S16_RECOVERY_DONT_CARE_TIME_PCT5MS	(S16_RECOVERY_DONT_CARE_TIME_MS/5)

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Pct_5msCtrl_HdrBusType Pct_5msCtrlBus;

/* Internal Logic Bus */
extern PressureControl_LocalVar      PressCTRLLocalBus;
/* Version Info */
extern const SwcVersionInfo_t Pct_5msCtrlVersionInfo;

/* Input Data Element */
extern Abc_CtrlWhlVlvReqAbcInfo_t Pct_5msCtrlWhlVlvReqAbcInfo;
extern Eem_MainEemFailData_t Pct_5msCtrlEemFailData;
extern Spc_1msCtrlPedlTrvlFild1msInfo_t Pct_5msCtrlPedlTrvlFild1msInfo;
extern Proxy_RxCanRxAccelPedlInfo_t Pct_5msCtrlCanRxAccelPedlInfo;
extern Msp_CtrlMotRotgAgSigInfo_t Pct_5msCtrlMotRotgAgSigInfo;
extern Abc_CtrlAbsCtrlInfo_t Pct_5msCtrlAbsCtrlInfo;
extern Abc_CtrlAvhCtrlInfo_t Pct_5msCtrlAvhCtrlInfo;
extern Abc_CtrlEscCtrlInfo_t Pct_5msCtrlEscCtrlInfo;
extern Abc_CtrlHsaCtrlInfo_t Pct_5msCtrlHsaCtrlInfo;
extern Abc_CtrlStkRecvryCtrlIfInfo_t Pct_5msCtrlStkRecvryCtrlIfInfo;
extern Abc_CtrlTcsCtrlInfo_t Pct_5msCtrlTcsCtrlInfo;
extern Bbc_CtrlBaseBrkCtrlModInfo_t Pct_5msCtrlBaseBrkCtrlModInfo;
extern Det_5msCtrlBrkPedlStatusInfo_t Pct_5msCtrlBrkPedlStatusInfo;
extern Det_1msCtrlBrkPedlStatus1msInfo_t Pct_5msCtrlBrkPedlStatus1msInfo;
extern Spc_5msCtrlCircPFildInfo_t Pct_5msCtrlCircPFildInfo;
extern Det_5msCtrlCircPRateInfo_t Pct_5msCtrlCircPRateInfo;
extern Det_1msCtrlPedlTrvlRate1msInfo_t Pct_5msCtrlPedlTrvlRate1msInfo;
extern Det_5msCtrlEstimdWhlPInfo_t Pct_5msCtrlEstimdWhlPInfo;
extern Arb_CtrlFinalTarPInfo_t Pct_5msCtrlFinalTarPInfo;
extern Mcc_1msCtrlIdbMotPosnInfo_t Pct_5msCtrlIdbMotPosnInfo;
extern Arb_CtrlWhlOutVlvCtrlTarInfo_t Pct_5msCtrlWhlOutVlvCtrlTarInfo;
extern Arb_CtrlWhlPreFillInfo_t Pct_5msCtrlWhlPreFillInfo;
extern Vat_CtrlIdbVlvActInfo_t Pct_5msCtrlIdbVlvActInfo;
extern Spc_5msCtrlPedlSimrPFildInfo_t Pct_5msCtrlPedlSimrPFildInfo;
extern Det_5msCtrlPedlTrvlRateInfo_t Pct_5msCtrlPedlTrvlRateInfo;
extern Det_5msCtrlPedlTrvlTagInfo_t Pct_5msCtrlPedlTrvlTagInfo;
extern Spc_5msCtrlPistPFildInfo_t Pct_5msCtrlPistPFildInfo;
extern Det_5msCtrlPistPRateInfo_t Pct_5msCtrlPistPRateInfo;
extern Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo_t Pct_5msCtrlVlvActtnReqByBaseBrkCtrlrInfo;
extern Spc_5msCtrlWhlSpdFildInfo_t Pct_5msCtrlWhlSpdFildInfo;
extern Eem_MainEemSuspectData_t Pct_5msCtrlEemSuspectData;
extern Mom_HndlrEcuModeSts_t Pct_5msCtrlEcuModeSts;
extern Mcc_1msCtrlMotCtrlMode_t Pct_5msCtrlMotCtrlMode;
extern Eem_SuspcDetnFuncInhibitPctrlSts_t Pct_5msCtrlFuncInhibitPctrlSts;
extern Bbc_CtrlBaseBrkCtrlrActFlg_t Pct_5msCtrlBaseBrkCtrlrActFlg;
extern Spc_5msCtrlBlsFild_t Pct_5msCtrlBlsFild;
extern Rbc_CtrlBrkPRednForBaseBrkCtrlr_t Pct_5msCtrlBrkPRednForBaseBrkCtrlr;
extern Mcc_1msCtrlMotICtrlFadeOutState_t Pct_5msCtrlMotICtrlFadeOutState;
extern Pct_1msCtrlPreBoostMod_t Pct_5msCtrlPreBoostMod;
extern Rbc_CtrlRgnBrkCtlrBlendgFlg_t Pct_5msCtrlRgnBrkCtlrBlendgFlg;
extern Det_5msCtrlVehSpdFild_t Pct_5msCtrlVehSpdFild;
extern Mcc_1msCtrlStkRecvryStabnEndOK_t Pct_5msCtrlStkRecvryStabnEndOK;

/* Output Data Element */
extern Pct_5msCtrlIdbPCtrllVlvCtrlStInfo_t Pct_5msCtrlIdbPCtrllVlvCtrlStInfo;
extern Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo_t Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo;
extern Pct_5msCtrlWhlInVlvCtrlTarInfo_t Pct_5msCtrlWhlnVlvCtrlTarInfo;
extern Pct_5msCtrlStkRecvryActnIfInfo_t Pct_5msCtrlStkRecvryActnIfInfo;
extern Pct_5msCtrlMuxCmdExecStInfo_t Pct_5msCtrlMuxCmdExecStInfo;
extern Pct_5msCtrlPCtrlAct_t Pct_5msCtrlPCtrlAct;
extern Pct_5msCtrlInitQuickBrkDctFlg_t Pct_5msCtrlInitQuickBrkDctFlg;
extern Pct_5msCtrlTgtDeltaStkType_t Pct_5msCtrlTgtDeltaStkType;
extern Pct_5msCtrlPCtrlBoostMod_t Pct_5msCtrlPCtrlBoostMod;
extern Pct_5msCtrlPCtrlFadeoutSt_t Pct_5msCtrlPCtrlFadeoutSt;
extern Pct_5msCtrlPCtrlSt_t Pct_5msCtrlPCtrlSt;
extern Pct_5msCtrlInVlvAllCloseReq_t Pct_5msCtrlInVlvAllCloseReq;
extern Pct_5msCtrlPChamberVolume_t Pct_5msCtrlPChamberVolume;
extern Pct_5msCtrlTarDeltaStk_t Pct_5msCtrlTarDeltaStk;
extern Pct_5msCtrlFinalTarPFromPCtrl_t Pct_5msCtrlFinalTarPFromPCtrl;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Pct_5msCtrl_Init(void);
extern void Pct_5msCtrl(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PCT_5MSCTRL_H_ */
/** @} */
