/*******************************************************************************
* Project Name:     IDB
* File Name:        LAPCT_DeltaStrokePerror.c
* Description:
* Logic version:
********************************************************************************
*  Modification Log
*  Date         Author          Description
*  -----        -----           -----
********************************************************************************/
/* Includes ******************************************************************/
#include "Pct_5msCtrl.h"
#include "LAPCT_DeltaStrokePerror.h"


#include "LAPCT_DecideMtrCtrlMode.h"
#include "LAPCT_LoadMapData.h"
#include "LAPCT_DeltaStrokeInterface.h"

/* Logcal Definition  *********************************************************/
#define S16_PRESS_ERR_CMP_GAIN_RESOL					(100)		/* Resolution : 1% */
#define S16_PRESS_ERR_CMP_GAIN_P_LVL1					(10)
#define S16_PRESS_ERR_CMP_GAIN_P_LVL2					(5)
#define S16_PRESS_ERR_CMP_GAIN_P_LVL3					(2)

#define S16_P_ERROR_COMP_LIMIT_RATIO					(15)
#define S16_P_ERROR_COMP_LIMIT_PRESS_THD				(S16_PCTRL_PRESS_15_BAR)
/* Logcal Type Definition  ****************************************************/

typedef struct {
	int16_t 	s16TargetP;
	int16_t 	s16CircuitPress;
	uint16_t 	u16WheelVolumeState;
	int32_t 	s32MtrTargetPosi;
	int32_t 	s32MtrCurrPosi;
	int16_t		s16PressError;
	int16_t 	s16PerrCompGainWithPerrBand;
	int16_t		s16TPRate;
	int16_t		s16CPRate;
}stPRESS_ERROR_CMP_R_t;

typedef struct {
	int16_t 	s16PerrorCompMax;
	int16_t 	s16PerrorCompMin;
}stPERROR_COMP_LIMIT_t;

#if (M_LOGGER_VARIABLE_DEF == ENABLE)
extern int16_t lapcts16DeltaStrokePerrorRaw;
#endif

/* GlobalFunction prototype **************************************************/

/* LocalFunction prototype ***************************************************/
static stPERROR_COMP_LIMIT_t LAPCT_s16DecidePerrorCompLimit(int16_t  s16TargetPress, uint16_t u16WheelVolumeState);
static int16_t * LAPCT_s16SelectPerrorCompBand(int8_t s8PCtrlBoostMode, int16_t*const s16PerrorBandQuick, int16_t*const s16PerrorBand);
static int16_t * LAPCT_s16SelectPerrorCompGain(int8_t s8PCtrlBoostMode,  int16_t*const s16QuickPressCompGain,  int16_t*const s16SafetyCompGain,  int16_t*const s16PressCompGain);
static int16_t LAPCT_s16CalcPerrorCompStroke(const stPRESS_ERROR_CMP_R_t stPressErrorComp_r, const int16_t s16PressCompGain[], uint8_t u8PRateErrCompEnableFlg);
static int16_t LAPCT_s16GetPerrorCompGain(int16_t s16PressError, int8_t s8PCtrlBoostMode, int16_t s16TPRate, int16_t s16CircuitPRate10ms, const int16_t s16PerrorBand[]);

/* Implementation*************************************************************/
/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16GetDeltaStrokePerror
* CALLED BY:            LAPCT_vCalcDeltaStroke()
* Preconditions:        none
* PARAMETER:            s16CircuitP, s16PressError, s16SafetyControlActFlg
* RETURN VALUE:         s16DeltaStrokePerror
* Description:          Get Delta Stroke for Pressure Error Compensation
********************************************************************************/
int16_t LAPCT_s16GetDeltaStrokePerror(int16_t s16CircuitP, int16_t s16CircuitPRate10ms, int16_t s16PressError, uint8_t u8PRateErrCompEnableFlg)
{
	int16_t  s16TargetP										= lidss16TargetP;	/* Instead of TargetP */

	uint16_t u16WheelVolumeState							= lidsu16WheelVolumeState;
	int8_t   s8PCtrlState									= lidss8PCtrlState;
	int8_t   s8PCtrlBoostMode								= lidss8MtrPCtrlBoostMode;
	int8_t   s8PCtrlFadeoutState							= lidss8PCtrlFadeoutState;

	int32_t  s32MtrTargetPosi								= lidss32MtrTargetPosi;
	int32_t  s32CurrentPosi									= lidss32CurrentPosi;

	int16_t  s16TargetPRate10ms								= lidss16TargetPRate10ms;

	int16_t *s16PerrorCompBand 								= S16_AP_CAL_PRESS_ERR_GAIN->s16PerrorBand;
	int16_t *s16PerrorCompGain 								= S16_AP_CAL_PRESS_ERR_GAIN->s16PressCompGain;
	stPRESS_ERROR_CMP_R_t 			stPressErrorComp_r		= {0, 0, 0, 0, 0, 0, 0, 0, 0};
	stPERROR_COMP_LIMIT_t 			stPerrorCompLimit       = {0,0};

	int16_t s16DeltaStrokePerror = 0;

	if((s8PCtrlState == S8_PRESS_BOOST) || ((s8PCtrlState == S8_PRESS_FADE_OUT) && (s8PCtrlFadeoutState == S8_PCTRL_FADEOUT_STATE_1)))
	{
		if((s8PCtrlBoostMode != S8_MTR_BOOST_MODE_TORQUE) && (s8PCtrlBoostMode != S8_MTR_BOOST_MODE_NOT_CONTROL) && (s8PCtrlBoostMode != S8_MTR_BOOST_MODE_ABS))		/* S8_MTR_BOOST_MODE_TORQUE : ABS Torque Control Mode */
		{
			int16_t  s16DeltaStrokeTarget							= 0;
			int16_t  s16CircuitPressureFail							= 0;

			stPerrorCompLimit = LAPCT_s16DecidePerrorCompLimit(s16TargetP, u16WheelVolumeState);

	/*********************************************************************************************************/
	/* Calculate Pressure Error Compensation Delta Stroke												Begin*/
	/*********************************************************************************************************/
			s16PerrorCompBand = LAPCT_s16SelectPerrorCompBand(s8PCtrlBoostMode,
																S16_AP_CAL_PRESS_ERR_GAIN->s16PerrorBandQuick,
																S16_AP_CAL_PRESS_ERR_GAIN->s16PerrorBand);
			s16PerrorCompGain = LAPCT_s16SelectPerrorCompGain(s8PCtrlBoostMode,
																S16_AP_CAL_PRESS_ERR_GAIN->s16QuickPressCompGain,
																S16_AP_CAL_PRESS_ERR_GAIN->s16SafetyCompGain,
																S16_AP_CAL_PRESS_ERR_GAIN->s16PressCompGain);

			stPressErrorComp_r.s16TargetP					= s16TargetP;
			stPressErrorComp_r.s16CircuitPress				= s16CircuitP;
			stPressErrorComp_r.u16WheelVolumeState			= u16WheelVolumeState;
			stPressErrorComp_r.s32MtrTargetPosi				= s32MtrTargetPosi;
			stPressErrorComp_r.s32MtrCurrPosi				= s32CurrentPosi;
			stPressErrorComp_r.s16PressError				= s16PressError;
			stPressErrorComp_r.s16PerrCompGainWithPerrBand	= LAPCT_s16GetPerrorCompGain(s16PressError, s8PCtrlBoostMode, s16TargetPRate10ms, s16CircuitPRate10ms, s16PerrorCompBand);
			stPressErrorComp_r.s16TPRate					= s16TargetPRate10ms;
			stPressErrorComp_r.s16CPRate					= s16CircuitPRate10ms;

			s16DeltaStrokePerror = LAPCT_s16CalcPerrorCompStroke(stPressErrorComp_r, s16PerrorCompGain, u8PRateErrCompEnableFlg);

			s16DeltaStrokePerror = L_s16LimitMinMax(s16DeltaStrokePerror, stPerrorCompLimit.s16PerrorCompMin, stPerrorCompLimit.s16PerrorCompMax);

	/*********************************************************************************************************/
	/* Calculate Pressure Error Compensation Delta Stroke												End  */
	/*********************************************************************************************************/
		}
		else { /* Do Nothing */ }
	}
	else { /* Do Nothing */ }

	return s16DeltaStrokePerror;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16SelectPerrorCompBand
* CALLED BY:            LAPCT_s16GetDeltaStrokePerror()
* Preconditions:        none
* PARAMETER:            s8PCtrlBoostMode, s16PerrorBandQuick, s16PerrorBand
* RETURN VALUE:         s16PerrorCompBand
* Description:          Get Pressure Error Band
********************************************************************************/
static int16_t * LAPCT_s16SelectPerrorCompBand(int8_t s8PCtrlBoostMode, int16_t*const s16PerrorBandQuick, int16_t*const s16PerrorBand)
{
	int16_t* s16PerrorCompBand = s16PerrorBand;

	if (s8PCtrlBoostMode == S8_MTR_BOOST_MODE_QUICK)		/* Quick Braking , AEB : 400bar/s 								*/
	{
		s16PerrorCompBand = s16PerrorBandQuick;
	}
	else
	{
		s16PerrorCompBand = s16PerrorBand;
	}

	return s16PerrorCompBand;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16SelectPerrorCompGain
* CALLED BY:            LAPCT_s16GetDeltaStrokePerror()
* Preconditions:        none
* PARAMETER:            s8PCtrlBoostMode, s16QuickPressCompGain, s16SafetyCompGain, s16PressCompGain
* RETURN VALUE:         s16PerrorCompGain
* Description:          Get Pressure Error Compensation Gain
********************************************************************************/
static int16_t * LAPCT_s16SelectPerrorCompGain(int8_t s8PCtrlBoostMode,  int16_t*const s16QuickPressCompGain,  int16_t*const s16SafetyCompGain,  int16_t*const s16PressCompGain)
{
	int16_t* s16PerrorCompGain = s16PressCompGain;


	if (s8PCtrlBoostMode == S8_MTR_BOOST_MODE_QUICK)		/* Quick Braking , AEB : 400bar/s 								*/
	{
		s16PerrorCompGain = s16QuickPressCompGain;
	}
	else
	{
		if (s8PCtrlBoostMode == S8_MTR_BOOST_MODE_SAFETY_HIGH_RESPONSE)	/* ESC oversteer (Include ROP), ABS, TCS_1_mode : 300bar/s 		*/
		{
			s16PerrorCompGain = s16SafetyCompGain;
		}
		else
		{
			s16PerrorCompGain = s16PressCompGain;
		}
	}

	return s16PerrorCompGain;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16CalcPerrorCompStroke
* CALLED BY:            LAPCT_s16GetDeltaStrokePerror()
* Preconditions:        none
* PARAMETER:            stPressErrorComp_r, s16PressCompGain[]
* RETURN VALUE:         s16DeltaStrokePerror
* Description:          Calculate Pressure Error Compensation Stroke
********************************************************************************/
static int16_t LAPCT_s16CalcPerrorCompStroke(const stPRESS_ERROR_CMP_R_t stPressErrorComp_r, const int16_t s16PressCompGain[], uint8_t u8PRateErrCompEnableFlg)
{
	int16_t s16DeltaStrokePerror       = 0;
	int16_t s16DeltaStrokePerrorRaw    = 0;
	int16_t s16DeltaStrokePerrorRawMax = 0;
	int16_t s16DeltaStrokePerrorSum    = 0;
	int16_t s16ErrorCompGain           = S16_PRESS_ERR_CMP_GAIN_P_LVL1;
	int16_t s16PosiError               = (int16_t)(stPressErrorComp_r.s32MtrTargetPosi - stPressErrorComp_r.s32MtrCurrPosi);
	int16_t s16TPBandIndex             = 0;
	int16_t s16PPBandIndex             = 0;
	int16_t s16IndexDiff               = 0;
	int16_t s16LoopCnt                 = 0;
	int16_t s16LoopCntMax              = 0;
	int16_t s16TPStroke                = 0;
	int16_t s16PPStroke                = 0;
	int16_t s16TPRate                  = stPressErrorComp_r.s16TPRate;
	int16_t s16CPRate                  = stPressErrorComp_r.s16CPRate;
	int16_t s16CompThreshold           = 0;

#if (M_LOGGER_VARIABLE_DEF == ENABLE)
	int16_t s16DeltaStrokeRawSum = 0;
#endif

	/* Calculate Delta Stroke for Pressure Error */
	if(u8PRateErrCompEnableFlg == TRUE)
	{
		if ( s16TPRate < -25)
		{
			s16CompThreshold = L_s16IInter2Point((s16TPRate - s16CPRate), S16_P_RATE_0_BPS, S16_PCTRL_PRESS_0_1_BAR, S16_P_RATE_20_BPS, S16_PCTRL_PRESS_0_5_BAR);
		}
		else if ( s16TPRate > 25)
		{
			s16CompThreshold = -L_s16IInter2Point((s16CPRate - s16TPRate), S16_P_RATE_0_BPS, S16_PCTRL_PRESS_0_1_BAR, S16_P_RATE_20_BPS, S16_PCTRL_PRESS_0_5_BAR);
		}
		else { }
	}
#if (WHEEL_DEPENDENT_MAP == DISABLE)
	s16DeltaStrokePerrorRaw = LAPCT_s16CalcStorkeByPress(stPressErrorComp_r.s16TargetP + s16CompThreshold, stPressErrorComp_r.u16WheelVolumeState) - LAPCT_s16CalcStorkeByPress(stPressErrorComp_r.s16CircuitPress, stPressErrorComp_r.u16WheelVolumeState);
#else
	s16DeltaStrokePerrorRaw = (int16_t)L_s32LimitMinMax(LAPCT_s32CalcStorkeByPress(stPressErrorComp_r.s16TargetP + s16CompThreshold, stPressErrorComp_r.u16WheelVolumeState) - LAPCT_s32CalcStorkeByPress(stPressErrorComp_r.s16CircuitPress, stPressErrorComp_r.u16WheelVolumeState), ASW_S16_MIN, ASW_S16_MAX);
#endif

	s16ErrorCompGain = L_s16IInter6Point(stPressErrorComp_r.s16CircuitPress, S16_PRESS_BAND(1),s16PressCompGain[0],S16_PRESS_BAND(2),s16PressCompGain[1],S16_PRESS_BAND(3),s16PressCompGain[2],S16_PRESS_BAND(4),s16PressCompGain[3],S16_PRESS_BAND(5),s16PressCompGain[4],S16_PRESS_BAND(6),s16PressCompGain[5]);
	s16DeltaStrokePerror = (int16_t)(((int32_t)s16ErrorCompGain * s16DeltaStrokePerrorRaw ) / S16_PRESS_ERR_CMP_GAIN_RESOL);	/* Calculate Stroke for Pressure Error without Position Error */

#if (M_LOGGER_VARIABLE_DEF == ENABLE)
	s16DeltaStrokeRawSum = s16DeltaStrokePerror + s16DeltaStrokePerrorRaw;
#endif
	s16DeltaStrokePerror = (int16_t)((((int32_t)s16DeltaStrokePerror) * (stPressErrorComp_r.s16PerrCompGainWithPerrBand))/S16_PRESS_ERR_CMP_GAIN_RESOL);
	/* Need Perror Comp Limitation */

	/* For logging */
#if (M_LOGGER_VARIABLE_DEF == ENABLE)
	lapcts16DeltaStrokePerrorRaw = s16DeltaStrokeRawSum;
#endif

	return s16DeltaStrokePerror;

}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16GetPerrorCompGain
* CALLED BY:            LAPCT_s16GetDeltaStrokePerror()
* Preconditions:        none
* PARAMETER:            s16PressError, s16TPRate, s16PerrorBand[]
* RETURN VALUE:         s16PerrorCompGain
* Description:          Get Pressure Error Compensation Gain
********************************************************************************/
static int16_t LAPCT_s16GetPerrorCompGain(int16_t s16PressError, int8_t s8PCtrlBoostMode, int16_t s16TPRate, int16_t s16CircuitPRate10ms, const int16_t s16PerrorBand[])
{
	int16_t s16PerrorCompGain = S16_PRESS_CMP_GAIN_P_ERR_BAND(2);
	if((s16TPRate > S16_P_RATE_1_BPS) || (s8PCtrlBoostMode == S8_MTR_BOOST_MODE_QUICK))
	{
		s16PerrorCompGain = L_s16IInter5Point(s16PressError,
											  s16PerrorBand[0], S16_PRESS_CMP_GAIN_P_ERR_BAND(0),
											  s16PerrorBand[1], S16_PRESS_CMP_GAIN_P_ERR_BAND(1),
											  s16PerrorBand[2], S16_PRESS_CMP_GAIN_P_ERR_BAND(2),
											  s16PerrorBand[3], S16_PRESS_CMP_GAIN_P_ERR_BAND(3),
											  s16PerrorBand[4], S16_PRESS_CMP_GAIN_P_ERR_BAND(4));
	}
	else if(s16TPRate < -S16_P_RATE_1_BPS)
	{
		s16PerrorCompGain = L_s16IInter5Point(s16PressError,
											  S16_P_ERROR_BAND(0), S16_PRESS_CMP_GAIN_P_ERR_BAND(4),
											  S16_P_ERROR_BAND(1), S16_PRESS_CMP_GAIN_P_ERR_BAND(3),
											  S16_P_ERROR_BAND(2), S16_PRESS_CMP_GAIN_P_ERR_BAND(2),
											  S16_P_ERROR_BAND(3), S16_PRESS_CMP_GAIN_P_ERR_BAND(1),
											  S16_P_ERROR_BAND(4), S16_PRESS_CMP_GAIN_P_ERR_BAND(0));
	}
	else
	{	/* Normal Gain 100% */
		if (s16PressError - (s16CircuitPRate10ms/2) > s16PerrorBand[4])		/* TP - (CP + CPRate/2) > Perror Max Band */	/* x BPS : x * 100 / 200 (bar/scan) bar : 100Resol */
		{
			s16PerrorCompGain = L_s16IInter5Point(s16PressError,
												  s16PerrorBand[0], S16_PRESS_CMP_GAIN_P_ERR_BAND(0),
												  s16PerrorBand[1], S16_PRESS_CMP_GAIN_P_ERR_BAND(1),
												  s16PerrorBand[2], S16_PRESS_CMP_GAIN_P_ERR_BAND(2),
												  s16PerrorBand[3], S16_PRESS_CMP_GAIN_P_ERR_BAND(3),
												  s16PerrorBand[4], S16_PRESS_CMP_GAIN_P_ERR_BAND(4));
		}
		else if (s16PressError - (s16CircuitPRate10ms/2) < s16PerrorBand[0])		/* TP - (CP + CPRate/2) > Perror Max Band */	/* x BPS : x * 100 / 200 (bar/scan) bar : 100Resol */
		{
			s16PerrorCompGain = L_s16IInter5Point(s16PressError,
												  S16_P_ERROR_BAND(0), S16_PRESS_CMP_GAIN_P_ERR_BAND(4),
												  S16_P_ERROR_BAND(1), S16_PRESS_CMP_GAIN_P_ERR_BAND(3),
												  S16_P_ERROR_BAND(2), S16_PRESS_CMP_GAIN_P_ERR_BAND(2),
												  S16_P_ERROR_BAND(3), S16_PRESS_CMP_GAIN_P_ERR_BAND(1),
												  S16_P_ERROR_BAND(4), S16_PRESS_CMP_GAIN_P_ERR_BAND(0));
		}
		else
		{
			s16PerrorCompGain = S16_PRESS_CMP_GAIN_P_ERR_BAND(2);
		}
	}
	return s16PerrorCompGain;
}



/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16DecidePerrorCompLimit
* CALLED BY:            LAPCT_s16GetDeltaStrokePerror()
* Preconditions:        none
* PARAMETER:            s16TargetPress, u16WheelVolumeState
* RETURN VALUE:         stPerrorCompLimitOut
* Description:          Decide pressure error compensation limit
********************************************************************************/
static stPERROR_COMP_LIMIT_t LAPCT_s16DecidePerrorCompLimit(int16_t  s16TargetPress, uint16_t u16WheelVolumeState)
{
	stPERROR_COMP_LIMIT_t stPerrorCompLimitOut = {0,0};
	int16_t s16PerrorCompLimitTPThresd = 0;
	int16_t s16TargetPressThresd = 0;
	int16_t s16PosPerrorCompLimit = 0;
	int16_t s16NegPerrorCompLimit = 0;

	s16TargetPressThresd = (int16_t)((int32_t)(s16TargetPress*S16_P_ERROR_COMP_LIMIT_RATIO)/100);

	if(s16TargetPressThresd < S16_P_ERROR_COMP_LIMIT_PRESS_THD)
	{
		s16PerrorCompLimitTPThresd = S16_P_ERROR_COMP_LIMIT_PRESS_THD;
	}
	else
	{
		s16PerrorCompLimitTPThresd = s16TargetPressThresd;
	}
	s16NegPerrorCompLimit = s16TargetPress + s16PerrorCompLimitTPThresd;
	s16PosPerrorCompLimit = ((s16TargetPress - s16PerrorCompLimitTPThresd) > S16_PCTRL_PRESS_0_BAR) ? (s16TargetPress - s16PerrorCompLimitTPThresd) : S16_PCTRL_PRESS_0_BAR;

	stPerrorCompLimitOut.s16PerrorCompMax = (int16_t)L_s32LimitMinMax(LAPCT_s32CalcStorkeByPress(s16TargetPress, u16WheelVolumeState) - LAPCT_s32CalcStorkeByPress(s16PosPerrorCompLimit, u16WheelVolumeState), ASW_S16_MIN, ASW_S16_MAX);
	stPerrorCompLimitOut.s16PerrorCompMin = (int16_t)L_s32LimitMinMax(LAPCT_s32CalcStorkeByPress(s16TargetPress, u16WheelVolumeState) - LAPCT_s32CalcStorkeByPress(s16NegPerrorCompLimit, u16WheelVolumeState), ASW_S16_MIN, ASW_S16_MAX);

	return stPerrorCompLimitOut;
}

