/*******************************************************************************
* Project Name:     IDB
* File Name:        LAPCT_DeltaStrokePRateError.h
* Description:
* Logic version:
********************************************************************************
*  Modification Log
*  Date         Author          Description
*  -----        -----           -----
********************************************************************************/

#ifndef LAPCT_DELTASTROKERATEERROR_H_
#define LAPCT_DELTASTROKERATEERROR_H_

/* Includes ******************************************************************/

/* Global Definition *********************************************************/
#define U16_RATE_CMP_P_BAND_ARY_MAX					(5)
#define U16_RATE_CMP_THD_P_BND_ARY_MAX				(4)

/* Calibration Type Definition************************************************/


/* Global Extern Variable ****************************************************/

/* Global Function ***********************************************************/
extern int16_t LAPCT_s16GetDeltaStrkPRateError(int16_t s16CircuitP, int16_t s16CircuitPRate10ms, int16_t s16PressError);

#endif /* LAPCT_DELTASTROKERATEERROR_H_ */
