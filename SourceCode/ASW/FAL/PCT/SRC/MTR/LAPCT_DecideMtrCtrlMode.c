/**
 * @defgroup LAPCT_DecideMtrCtrlMode LAPCT_DecideMtrCtrlMode
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LAPCT_DecideMtrCtrlMode.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Pct_5msCtrl.h"
#include "LAPCT_DecideMtrCtrlMode.h"
#include "LAPCT_DeltaStrokeInterface.h"
#include "LAPCT_DeltaStrokeMap.h"
#include "LAPCT_DeltaStrokePerror.h"
#include "LAPCT_DeltaStrokePRateError.h"
#include "LAPCT_LoadMapData.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

#define S16_PRESS_ERR_CMP_GAIN_RESOL				(100)		/* Resolution : 1% */
#define S16_PRESS_ERR_CMP_GAIN_P_LVL1				(10)
#define S16_PRESS_ERR_CMP_GAIN_P_LVL2				(5)
#define S16_PRESS_ERR_CMP_GAIN_P_LVL3				(2)
#define S16_RATE_ERR_CMP_GAIN_RESOL					(100)		/* Resolution : 1% */

#define S16_STOP_PERROR_LIMIT_GAIN					(5)			/* Perror Compensation rate is 50% at Standstill state */

#define MOVING_AVG_RESET_ACT						TRUE
#define MOVING_AVG_MAINTAIN							FALSE

#if DISABLE
/*  #if (__GM_SSTS_AHB_PRESS_CTRL_TEST == ENABLE)*/
#define S16_RTN_ORIGIN_DEL_STROKE					(-100)
  #else
#define S16_RTN_ORIGIN_DEL_STROKE					(-50)
  #endif
#define S16_POSI_ERROR_FACTOR						(4)

#define S16_NORMAL_STATE							(0)
#define S16_EMERGENCY_STATE							(1)
#define S16_ALAM_STATE								(2)

#define S16_INIT_INEFFECTIVE_STRK_COMP_SCAN			(10)
#define S16_INIT_INEFFECTIVE_STRK_COMP_SCAN_ESC		(2)

#define S16_TARGET_STRK_MOVING_AVG_ARR_2			(2)
#define S16_TARGET_STRK_MOVING_AVG_ARR_5			(5)
#define S16_TARGET_STRK_MOVING_AVG_ARR_10			(10)

#define U16_PRESS_BAND_ARRAY_MAX					(7)
#define U16_PERROR_BAND_ARRAY_MAX					(5)
#define U16_SPEED_BAND_ARRAY_MAX					(3)
#define U16_RATE_CMP_P_BAND_ARY_MAX					(5)
#define U16_RATE_CMP_THD_P_BND_ARY_MAX				(4)
#define U16_RATE_CMP_P_ERROR_BAND_MAX				(4)

#define U16_LIM_DEL_STRK_TAR_ARY_MAX				(5)
#define S16_LIM_DEL_STRK_FOR_MOTOR_MAX				(1197) /* 1rpm = 0.171 pulse / scan, 1197 ==> 7000 rpm reference */
#define S16_LIM_DEL_STRK_FOR_MOTOR_MIN				(-592) /* TODO: Adjust release rate limit */
#define S16_LIM_DEL_STRK_TARGET_MAX					S16_LIM_DEL_STRK_FOR_MOTOR_MAX

#if (__CAR == HMC_HGE)
#define S16_LIM_DEL_STRK_MAX_SAFETY_F					(600)
#define S16_LIM_DEL_STRK_MAX_SAFETY_R					(300)
#elif (__CAR == HMC_YFE)
#define S16_LIM_DEL_STRK_MAX_SAFETY_F					(750)
#define S16_LIM_DEL_STRK_MAX_SAFETY_R					(260)
#elif (__CAR == KMC_TFE)
#define S16_LIM_DEL_STRK_MAX_SAFETY_F					(600)
#define S16_LIM_DEL_STRK_MAX_SAFETY_R					(300)
#else
#define S16_LIM_DEL_STRK_MAX_SAFETY_F					(750)
#define S16_LIM_DEL_STRK_MAX_SAFETY_R					(490)
#endif

/* Motor Fadeout & Fadein Parameter - Begin */
#define S32_ORIGIN_SET_MAX_THR						(35)
#define S32_ORIGIN_SET_MIN_THR						(-35)
#define S32_MTR_FADEOUT_OFF_MAX_THR					(100)
#define S32_MTR_FADEOUT_OFF_MIN_THR					(-100)
#define S16_FADEOUT_OK_THR							(S16_PCTRL_PRESS_0_3_BAR)

#define S16_FADEOUT_POSITION_L5						(3000)
#define S16_FADEOUT_POSITION_L4						(1000)
#define S16_FADEOUT_POSITION_L3						(500)
#define S16_FADEOUT_POSITION_L2						(200)
#define S16_FADEOUT_POSITION_L1						(100)
#define S16_FADEOUT_POSITION_L0						(50)

#define M_ESC_END_DCT_SCAN							(U8_T_100_MS)

#define S16_FADEOUT_DEL_STROKE_MAX_SPD				S16_SPEED_40_KPH
#define S16_FADEOUT_DEL_STROKE_AFTER_ESC			(-150)
#define S16_FADEOUT_DEL_STROKE_MAX					(-120)
#define S16_FADEOUT_DEL_STROKE_L5					(-70)
#define S16_FADEOUT_DEL_STROKE_L4					(-50)
#define S16_FADEOUT_DEL_STROKE_L3					(-30)
#define S16_FADEOUT_DEL_STROKE_L2					(-20)
#define S16_FADEOUT_DEL_STROKE_L1					(-10)
#define S16_FADEOUT_DEL_STROKE_L0					(-5)

#define S16_FADEOUT_DEL_STROKE_OLD_L3				(-10)
#define S16_FADEOUT_DEL_STROKE_OLD_L2				(-20)
#define S16_FADEOUT_DEL_STROKE_OLD_L1				(-50)
#define S16_FADEOUT_DEL_STROKE_OLD_L0				(-100)

#define S16_FADEOUT_DEL_STROKE_DIFF_L3				(1)
#define S16_FADEOUT_DEL_STROKE_DIFF_L2				(5)
#define S16_FADEOUT_DEL_STROKE_DIFF_L1				(10)
#define S16_FADEOUT_DEL_STROKE_DIFF_L0				(10)

#define S16_FADEIN_POSITION_L3						(-50)
#define S16_FADEIN_POSITION_L2						(-100)
#define S16_FADEIN_POSITION_L1						(-200)
#define S16_FADEIN_POSITION_L0						(-500)

#define S16_FADEIN_DEL_STROKE_L3					(5)
#define S16_FADEIN_DEL_STROKE_L2					(10)
#define S16_FADEIN_DEL_STROKE_L1					(20)
#define S16_FADEIN_DEL_STROKE_L0					(50)

#define S16_FADEIN_DEL_STROKE_OLD_L3				(100)
#define S16_FADEIN_DEL_STROKE_OLD_L2				(50)
#define S16_FADEIN_DEL_STROKE_OLD_L1				(20)
#define S16_FADEIN_DEL_STROKE_OLD_L0				(10)

#define S16_FADEIN_DEL_STROKE_DIFF_L3				(4)
#define S16_FADEIN_DEL_STROKE_DIFF_L2				(3)
#define S16_FADEIN_DEL_STROKE_DIFF_L1				(2)
#define S16_FADEIN_DEL_STROKE_DIFF_L0				(1)

#define S16_MOTOR_POSI_MIN_LIMIT					(-500)
#define S32_RELEASE_POSI_THRESHOLD					(200)

#define S16_APPLY_HOLD_RATE_THD						(200)
#define S16_RELEASE_HOLD_RATE_THD					(-200)
#define S16_APPLY_HOLD_DEL_STRK_DIFF_LIM			(30)
#define S16_RELEASE_HOLD_DEL_STRK_DIFF_LIM			(30)

#define S16_NEGATIVE_COMP_NEGATIVE_RATE_THR			(-20)
#define S16_NEGATIVE_COMP_POSITIVE_RATE_THR			(20)

#define S16_INIT_TP_COMP_DEL_STROKE					(46)

#define S16_REDUCE_GAIN_OF_RELEASE_MAP 				(8)
#define S16_REDUCE_GAIN_OF_APPLY_MAP 				(9)
#define S16_GAIN_OF_APPLY_MAP_INIT_FAST				(10)

#define S16_ABS_FADE_OUT_DAMPING_TIME				(10)
#define S16_ABS_FADE_OUT_RISE_RATE					S16_PCTRL_PRESS_0_3_BAR

#define lapctu1PreFadeoutFlg			 		lapctDelStrkActFlg2.bit3
#define lapctu1MotorFadeoutEndOK  				lapctDelStrkActFlg2.bit2
#define lapctu1FadeoutState1EndOK		 		lapctDelStrkActFlg2.bit1
#define lapctu1FadeoutState2EndOK		 		lapctDelStrkActFlg2.bit0

#define WHL_CTRL_INDEX_NON			0
#define WHL_CTRL_INDEX_PRIMARY		1
#define WHL_CTRL_INDEX_SECONDARY	2
#define WHL_CTRL_INDEX_BOTH			3

#define WHL_CTRL_SEL_WHL_P_FL	0
#define WHL_CTRL_SEL_WHL_P_FR	1
#define WHL_CTRL_SEL_WHL_P_RL	2
#define WHL_CTRL_SEL_WHL_P_RR	3


#define U8_PRESS_VOLUME_RATIO   3

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct {
	int8_t		s8PCtrlState;
	int16_t		s16CircuitP;
	int16_t		s16TPRate;
	int16_t		s16CPPRate10ms;
	int16_t		s16Perror;
	int16_t		s16TargetP;
	int16_t 	s8PCtrlBoostMode;
	int16_t 	s16CVVOpenFlg;
}stNEGATIVE_COMP_R_t;

typedef struct {
	int16_t s16NegativeCompDisableFlg;
	int16_t s16TargetP;
	int16_t s16TargetPRate10ms;
	int16_t s16DeltaStrokeErrorSum;
	int16_t s16DeltaStrokeErrorSumOld;
	int16_t s16SafetyControlActFlg;
	int8_t  s8PCtrlBoostMode;
	uint16_t u16SafetyControlCompOnFlg;
	int16_t s16CircuitP;
}stP_ERROR_COMP_LIMIT_R_t;

typedef struct
{
	int16_t s16DeltaStrokeMap;
	int16_t s16DeltaStrokeErrorSum;
	int16_t s16NegativeCompDisableFlg;
	int16_t s16LimDelStrokeMin;
	int16_t s16LimDelStrkTargetMax;
}stBOOST_DEL_STRK_TARGET_t;

typedef struct
{
	int8_t  s8PCtrlState;
	int8_t  s8PCtrlFadeoutState;
	int8_t  s8PCtrlBoostMode;
}stPCTL_INFO_t;

typedef struct
{
	int16_t s16DeltaStrokeMap;
	int32_t s32MtrTargetPosi;
	int16_t s16DeltaStrokeTargetOld;
	uint16_t u16FadeoutState1EndOK;
	int32_t s32CurrentPosi;
	int16_t s16PosiError;
}stFADEOUT_ST1_DEL_S_TAR_t;

typedef struct{
    int16_t s16DeltaStkTar;
    uint16_t u16FadeoutState1EndOK;
}stCalcDeltaStkOutpInfo_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_CONST_UNSPECIFIED
#include "Pct_MemMap.h"
/** Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Pct_MemMap.h"

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
#if (M_LOGGER_VARIABLE_DEF == ENABLE)
/* Temp Global Variable Definition for Log************************************/
int16_t lapcts16DeltaStrokeMap;
int16_t lapcts16DeltaStrokeMapAvg;
int16_t lapcts16DeltaStrokeMapFinal;
int16_t lapcts16DeltaStrokePerrorRaw;
int16_t lapcts16DeltaStrokePerror;
int16_t lapcts16DeltaStrokeRateErrorRaw;
int16_t lapcts16DeltaStrokeRateCompGain;
int16_t lapcts16PressPointTargetRate;
int16_t lapcts16PressPointPRate;
int16_t lapcts16RateCompThres1;
int16_t lapcts16RateCompThres2;
int16_t lapcts16DeltaStrokeTagLimMax;
int16_t lapcts16TargetPressGend;
uint16_t lapctu16FadeoutState1EndOK;
int16_t lapcts16DeltaStrokePrateError;
int16_t lapcts16OverUnderShootComp;
int16_t lapcts16DeltaStrokePerrorFinal;
int16_t lapcts16IntegralErrorComp;
int16_t lapcts16TPOld;
int16_t lapcts16StrokeErr_TPOld;
int16_t lapcts16StrokeErr_MCP;
uint16_t lapctu16WheelVolumeState;
uint16_t lapctu16DctPressFailCnt;
int16_t lapcts16DeltaStrokeTargetWoLtd;
int16_t lapcts16Fadeout1TimerLog;
U8_BIT_STRUCT_t lapctDelStrkActFlg;
U8_BIT_STRUCT_t lapctDelStrkActFlg2;
int16_t lapcts16TargetPressOldlog = 0;
int16_t lapcts16InitInEffStrkComp ;

#endif
#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_START_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/** Variable Section (32BIT)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Pct_MemMap.h"

/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
FAL_STATIC  stCalcDeltaStkOutpInfo_t          stCalcDeltaStkOutpInfo = {0,0};

/*
static int16_t lapcts16DeltaStrokeTarget = 0;
static int16_t lapcts16CircuitPressureFail = FALSE;
*/
#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_START_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/** Variable Section (32BIT)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Pct_MemMap.h"

/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_CODE
#include "Pct_MemMap.h"

static stCalcDeltaStkOutpInfo_t   LAPCT_stCalMtrCtrlTarget(void);
static void Set_CalcDeltaStkOutpInfo(stCalcDeltaStkOutpInfo_t stCalcDeltaStkResult);

static int16_t LAPCT_s16LimitPErrorComp(stP_ERROR_COMP_LIMIT_R_t stPErrorCompLimit_r, stPCTL_INFO_t stPCtrlInfo_r);
static int16_t LAPCT_s16GenerateDeltaStrokeTarget(int16_t s16DeltaStrokeMap, int16_t s16DeltaStrokeErrorSum, int16_t s16NegativeCompDisableFlg);
static int16_t LAPCT_s16DctNegativeCompDisable(stNEGATIVE_COMP_R_t stNegativeComp_r);
#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
#else
static int16_t LAPCT_s16PickCircuitPressure(uint8_t u8WhlCtrlIndex, int16_t s16PriCircuitP, int16_t s16SecCircuitP);
static int16_t LAPCT_s16PickCircuitPRate(uint8_t u8WhlCtrlIndex, int16_t s16PriPRate10ms, int16_t s16SecPRate10ms);
#endif
static int16_t LAPCT_s16SelLimDelStrkTargetMax(int8_t s8PCtrlBoostMode,	uint16_t u16WheelVolumeState, int16_t s16TargetPRate10ms);
static int16_t LAPCT_s16FadeoutDelStrokeEsc(int8_t s8PCtrlState, int8_t s8PCtrlBoostMode, int8_t s8PCtrlFadeoutState, int32_t s32MtrTargetPosi, int16_t lapcts16DeltaStrokeTargetOld);
static uint16_t LAPCT_u16DecideSafetyBreakComp(int16_t s16TargetP, int16_t s16TargetPressOld, uint16_t u16SafetyControlCompOnFlg);
#if DISABLE
static int16_t LAPCT_s16GetMaxEstWheelPress(const stEstWheelPress_t* const stEstWheelPress_r);
#endif
static int16_t LAPCT_s16GetEstWheelPOnSafety(uint16_t u16WheelVolumeState, uint8_t u8WhlCtrlIndex, const stEstWheelPress_t* stEstWheelPress_r);
static int16_t LAPCT_s16DctIneffectiveStroke(int16_t s16IneffectiveStrokeCompFlg, stINEFF_STROKE_CMP_R_t stIneffStrokeComp_r);
static uint16_t LAPCT_u16MtrEndOkReset(int8_t s8PCtrlState, uint16_t u16MotorFadeoutEndOK);
static uint16_t LAPCT_u16GetSafetyCtrlCompOnFlg(int8_t s8PCtrlBoostMode, int16_t s16TargetP, int16_t s16TargetPressOld);
static int16_t LAPCT_s16GetCirPOnSafetyCtrl(int8_t s8PCtrlBoostMode, uint16_t u16SafetyControlCompOnFlg, int16_t s16TargetPressOld, int16_t s16CircuitP, int16_t s16EstWhlP);
static int16_t LAPCT_s16GetDelStrkErrorSum(int8_t s8PCtrlBoostMode, int16_t s16IneffectiveStrokeCompFlg, int16_t s16DeltaStrokePerror, int16_t s16DeltaStrokePRateError);
static int16_t LAPCT_s16GetPreFadeOutFlg(stPCTL_INFO_t stPCtrlInfo_r, int16_t s16DeltaStrokeMap);
static int16_t LAPCT_s16GetBoostDelStrkTarget(stPCTL_INFO_t stPCtrlInfo_r, stBOOST_DEL_STRK_TARGET_t stBoostDelStrkTarget);
static uint16_t LAPCT_u16GetFadeOutSt1EndOk(stPCTL_INFO_t stPCtrlInfo_r, int16_t s16CircuitP);
static int16_t LAPCT_s16GetFinalDelStrkTarget(stPCTL_INFO_t stPCtrlInfo_r, int16_t s16BoostDeltaStrokeTarget, int16_t s16FadeOutSt1DeltaStrokeTarget, int16_t s16FadeOutEscDeltaStrokeTarget);
/* FadeOut Function*/
static int16_t LAPCT_s16GetFadeoutSt1DelSTar(stPCTL_INFO_t stPCtrlInfo_r, stFADEOUT_ST1_DEL_S_TAR_t stFadeOutSt1DelStr_r);
static uint16_t LAPCT_s16FadeoutSt1TimeoutEndOk(int16_t s16CircuitP);
static uint8_t LAPCT_u8DecidePRateErrComp(int8_t s8PCtrlState, int8_t s8PCtrlBoostMode);
static int16_t LAPCT_s16GetCircuitPStableCnt(int16_t s16CircuitP, int16_t s16PStableCnt);
static int16_t LAPCT_s16MtrPosiFadeOut(int32_t s32MtrTargetPosi, int16_t s16DeltaStrokeTargetOld, int32_t s32CurrentPosi, int16_t s16PosiError);
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void LAPCT_vDecideMtrCtrlMode(void)
{
	LAPCT_vDeltaSTrokeInterface();

    stCalcDeltaStkOutpInfo = LAPCT_stCalMtrCtrlTarget();

    /* LAPCT_vCalcDeltaStk Runnable Output Interface */
    Set_CalcDeltaStkOutpInfo(stCalcDeltaStkOutpInfo);

}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

/*******************************************************************************
* FUNCTION NAME:        LAPCT_stCalMtrCtrlTarget
* CALLED BY:            ()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Delta Stroke Target Calculation Module
********************************************************************************/
static stCalcDeltaStkOutpInfo_t LAPCT_stCalMtrCtrlTarget(void)
{

	/*********************************************************************************************************/
	/* Local Variable for SWC Input Interface															Begin*/
	/*********************************************************************************************************/
#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
	int16_t  s16PistonP										= lidss16PistonP;
#else
	int16_t  s16PriCircuitP									= lidss16PriCircuitP;
	int16_t  s16SecCircuitP									= lidss16SecCircuitP;
#endif

	int16_t  s16TargetP										= lidss16TargetP;
	int16_t  s16TargetPressOld								= lidss16TarrgetPOld;
	int16_t  s16TargetPRate10ms								= lidss16TargetPRate10ms;

#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
	int16_t  s16PistPRate10ms								= lidss16PistPRate10ms;
#else
	int16_t  s16PriPRate10ms								= lidss16PriPRate10ms;
	int16_t  s16SecPRate10ms								= lidss16SecPRate10ms;
#endif

	int16_t  s16PedalTravelRateMmPSec						= lidss16PedalTravelRateMmPSec;

	uint16_t u16WheelVolumeState							= lidsu16WheelVolumeState;
	int8_t   s8PCtrlState									= lidss8PCtrlState;
	int8_t   s8PCtrlBoostMode								= lidss8MtrPCtrlBoostMode;
	int8_t   s8PCtrlFadeoutState							= lidss8PCtrlFadeoutState;

	int32_t  s32MtrTargetPosi								= lidss32MtrTargetPosi;
	int32_t  s32CurrentPosi									= lidss32CurrentPosi;
	int32_t  s32TatEstStrkPosiOfWhlP						= lidss32TatEstStrkPosiOfWhlP;

	int16_t s16VehicleSpeed									= lidss16VehicleSpeed;
	int16_t s16CVVOpenFlg									= lids16CVVOpenFlg;

	int16_t s16EstWheelPressFL								= lidss16EstWheelPressFL;
	int16_t s16EstWheelPressFR								= lidss16EstWheelPressFR;
	int16_t s16EstWheelPressRL								= lidss16EstWheelPressRL;
	int16_t s16EstWheelPressRR								= lidss16EstWheelPressRR;

	uint8_t u8WhlCtrlIndex									= lidsu8WhlCtrlIndex;			/* 4wheel control :
																												front 0x0-, rear 0x-0
																												non - 0, pri - 1, sec - 2, Both - 3  */
	/*********************************************************************************************************/
	/* Local Variable for SWC Input Interface															End  */
	/*********************************************************************************************************/

	/*********************************************************************************************************/
	/* Local Variable for SWC Output Interface															Begin*/
	/*********************************************************************************************************/
	int16_t  s16DeltaStrokeTarget							= 0;
	int16_t  s16BoostDeltaStrokeTarget						= 0;
	int16_t  s16FadeOutSt1DeltaStrokeTarget					= 0;
	int16_t  s16FadeOutEscDeltaStrokeTarget					= 0;
	int16_t  s16CircuitPressureFail							= 0;
	/*********************************************************************************************************/
	/* Local Variable for SWC Output Interface															End  */
	/*********************************************************************************************************/

	/*********************************************************************************************************/
	/* Static Local Variable Define																		Begin*/
	/*********************************************************************************************************/
	static int16_t lapcts16DeltaStrokeTargetOld				= 0;
	static int16_t lapcts16LimDelStrokeMin					= S16_LIM_DEL_STRK_FOR_MOTOR_MIN;
	static int16_t lapcts16DeltaStrokeErrorSumOld			= 0;
	static int16_t lapcts16IneffectiveStrokeCompFlg			= FALSE;
	/*********************************************************************************************************/
	/* Static Local Variable Define																		End  */
	/*********************************************************************************************************/

	/*********************************************************************************************************/
	/* Local Variable Define																			Begin*/
	/*********************************************************************************************************/
	int16_t s16DeltaStrokeMap								= 0;
	int16_t s16DeltaStrokeCompIneff							= 0;
	int16_t s16DeltaStrokePerror							= 0;
	int16_t s16DeltaStrokePRateError						= 0;
	int16_t s16DeltaStrokeErrorSum							= 0;
	int16_t s16DeltaStrokeABSFadeOutComp					= 0;
	int16_t s16CircuitP										= S16_PCTRL_PRESS_0_BAR; /* Put in below */
	int16_t s16CircuitPToLimitDelSMin						= S16_PCTRL_PRESS_0_BAR; /* Put in below */
	int16_t s16EstWhlP										= S16_PCTRL_PRESS_0_BAR;
	int16_t s16CircuitPRate10ms								= S16_P_RATE_0_BPS;
	int16_t s16PressError									= S16_PCTRL_PRESS_0_BAR; /* Put in below */
	int16_t s16PosiError									= (int16_t)(s32MtrTargetPosi - s32CurrentPosi);
	int16_t s16PerrCompGainWithPerrBand						= 100;
	int16_t s16NegativeCompDisableFlg						= FALSE;
	int16_t s16PosiForReleaseP								= S16_PCTRL_PRESS_0_BAR;
	int16_t s16PreFadeoutFlg								= FALSE;

	uint16_t u16FadeoutState1EndOK 							= FALSE;

	int16_t s16LimDelStrkTargetMax							= S16_LIM_DEL_STRK_FOR_MOTOR_MAX;
	int16_t s16MovingAvgSel									= S16_MOVING_AVG_10;		/* Default Moving Average Size : 10 */
	int16_t s16MovAvgBuffResetFlg							= FALSE;
	uint16_t u16SafetyControlCompOnFlg						= FALSE;
	uint8_t u8PRateErrCompEnableFlg							= FALSE;

	stINEFF_STROKE_CMP_R_t			stIneffStrokeComp_r		= {0, 0, 0, 0, 0, 0};
	stNEGATIVE_COMP_R_t				stNegativeComp_r		= {0, 0, 0, 0, 0, 0, 0, 0};
	stP_ERROR_COMP_LIMIT_R_t 		stPErrorCompLimit_r		= {0, 0, 0, 0, 0, 0, 0, 0};
	stEstWheelPress_t 				stEstWheelPress_r		= {0, 0, 0, 0};
	stBOOST_DEL_STRK_TARGET_t		stBoostDelStrkTarget_r	= {0, 0, 0, 0, 0};
	stPCTL_INFO_t					stPCtrlInfo_r			= {0, 0, 0};
	stFADEOUT_ST1_DEL_S_TAR_t		stFadeOutSt1DelStr_r	= {0, 0, 0, 0, 0, 0};
	/*********************************************************************************************************/
	/* Local Variable Define																			End  */
	/*********************************************************************************************************/

	/*********************************************************************************************************/
	/* Variable Initialize																				Begin*/
	/*********************************************************************************************************/
	stEstWheelPress_r.s16EstWheelPressFL					= s16EstWheelPressFL;
	stEstWheelPress_r.s16EstWheelPressFR					= s16EstWheelPressFR;
	stEstWheelPress_r.s16EstWheelPressRL					= s16EstWheelPressRL;
	stEstWheelPress_r.s16EstWheelPressRR					= s16EstWheelPressRR;

	stPCtrlInfo_r.s8PCtrlState								= s8PCtrlState;
	stPCtrlInfo_r.s8PCtrlFadeoutState						= s8PCtrlFadeoutState;
	stPCtrlInfo_r.s8PCtrlBoostMode							= s8PCtrlBoostMode;

	/*********************************************************************************************************/
	/* Variable Initialize																				End  */
	/*********************************************************************************************************/

	u16SafetyControlCompOnFlg = LAPCT_u16GetSafetyCtrlCompOnFlg(s8PCtrlBoostMode, s16TargetP, s16TargetPressOld);

	/*********************************************************************************************************/
	/* Circuit Pressure Value Update																	Begin*/
	/*********************************************************************************************************/

	s16EstWhlP = LAPCT_s16GetEstWheelPOnSafety(u16WheelVolumeState, u8WhlCtrlIndex, &stEstWheelPress_r);

#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
	s16CircuitP = s16PistonP;
	s16CircuitPRate10ms = s16PistPRate10ms;
#else
	s16CircuitP = LAPCT_s16PickCircuitPressure(u8WhlCtrlIndex, s16PriCircuitP, s16SecCircuitP);
	s16CircuitPRate10ms = LAPCT_s16PickCircuitPRate(u8WhlCtrlIndex, s16PriPRate10ms, s16SecPRate10ms);
#endif

	s16CircuitP = LAPCT_s16GetCirPOnSafetyCtrl(s8PCtrlBoostMode, u16SafetyControlCompOnFlg, s16TargetPressOld, s16CircuitP, s16EstWhlP);
	/*********************************************************************************************************/
	/* Circuit Pressure Valve Update																	End  */
	/*********************************************************************************************************/

	s16PressError = s16TargetP - s16CircuitP;

	/********************************************************************************************************/
	/* Limit Delta Stroke Target Max depends on Current Wheel Volume State									*/
	/********************************************************************************************************/
	s16LimDelStrkTargetMax = LAPCT_s16SelLimDelStrkTargetMax(s8PCtrlBoostMode, u16WheelVolumeState, s16TargetPRate10ms);

	/********************************************************************************************************/
	/* Delta Stroke Target Min consider brake fluid inflow		 											*/
	/********************************************************************************************************/
#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
	s16CircuitPToLimitDelSMin = s16PistonP;
#else
	s16CircuitPToLimitDelSMin = (s16PriCircuitP < s16SecCircuitP)? s16PriCircuitP : s16SecCircuitP;
#endif
#if DISABLE
	lapcts16LimDelStrokeMin = L_s16IInter6Point(s16CircuitP, S16_PCTRL_PRESS_1_BAR, -83,
															 S16_PCTRL_PRESS_5_BAR, -187,
															 S16_PCTRL_PRESS_10_BAR, -265,
															 S16_PCTRL_PRESS_20_BAR, -375,
															 S16_PCTRL_PRESS_40_BAR, -530,
															 S16_PCTRL_PRESS_50_BAR, S16_LIM_DEL_STRK_FOR_MOTOR_MIN);
#else
	lapcts16LimDelStrokeMin = L_s16IInter6Point(s16CircuitPToLimitDelSMin, S16_PCTRL_PRESS_1_BAR, -66,
															               S16_PCTRL_PRESS_5_BAR, -150,
															               S16_PCTRL_PRESS_10_BAR, -212,
															               S16_PCTRL_PRESS_20_BAR, -300,
															               S16_PCTRL_PRESS_40_BAR, -424,
															               S16_PCTRL_PRESS_50_BAR, S16_LIM_DEL_STRK_FOR_MOTOR_MIN);
#endif
	/*********************************************************************************************************/
	/* Compensate orifice effect																		Begin*/
	/*********************************************************************************************************/
	stNegativeComp_r.s8PCtrlState = s8PCtrlState;
	stNegativeComp_r.s16CircuitP = s16CircuitP;
	stNegativeComp_r.s16TPRate = s16TargetPRate10ms;
#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
	stNegativeComp_r.s16CPPRate10ms = s16PistPRate10ms;
#else
	stNegativeComp_r.s16CPPRate10ms = s16PriPRate10ms;
#endif
	stNegativeComp_r.s16Perror = s16PressError;
	stNegativeComp_r.s16TargetP = s16TargetP;
	stNegativeComp_r.s8PCtrlBoostMode = s8PCtrlBoostMode;
	stNegativeComp_r.s16CVVOpenFlg = s16CVVOpenFlg;
	s16NegativeCompDisableFlg = LAPCT_s16DctNegativeCompDisable(stNegativeComp_r);
	/*********************************************************************************************************/
	/* Compensate orifice effect																		End  */
	/*********************************************************************************************************/

	/*********************************************************************************************************/
	/* Detect Ineffective Stroke Compensation Condition													Begin*/
	/*********************************************************************************************************/
	stIneffStrokeComp_r.s8PCtrlState       = s8PCtrlState            ;
	stIneffStrokeComp_r.s16PdtRateMmPSec   = s16PedalTravelRateMmPSec;
	stIneffStrokeComp_r.s16TargetP         = s16TargetP   ;
	stIneffStrokeComp_r.s16CircuitP        = s16CircuitP;
	stIneffStrokeComp_r.u16WheelVolumeState = u16WheelVolumeState;
	lapcts16IneffectiveStrokeCompFlg = LAPCT_s16DctIneffectiveStroke(lapcts16IneffectiveStrokeCompFlg, stIneffStrokeComp_r);
	lapctu1DelStrkPErrorCompFlg1 = lapcts16IneffectiveStrokeCompFlg;
	/*********************************************************************************************************/
	/* Detect Ineffective Stroke Compensation Condition													End  */
	/*********************************************************************************************************/

	/*********************************************************************************************************/
	/* Pressure Control State																			Begin*/
	/* S8_PRESS_BOOST : Normal Boost + ABS																	 */
	/* S8_PCTRL_FADEOUT_STATE_1 : Pressure Error Compensation Only & Pressure reachs to 0bar				 */
	/*********************************************************************************************************/
	s16DeltaStrokeMap 			= LAPCT_s16GetDeltaStrokeMap(s16CircuitP, lapcts16IneffectiveStrokeCompFlg, s16LimDelStrkTargetMax, lapcts16LimDelStrokeMin, s16TargetPressOld);

	u8PRateErrCompEnableFlg 	= LAPCT_u8DecidePRateErrComp(s8PCtrlState, s8PCtrlBoostMode);

	s16DeltaStrokePerror 		= LAPCT_s16GetDeltaStrokePerror(s16CircuitP, s16CircuitPRate10ms, s16PressError, u8PRateErrCompEnableFlg);

	if(u8PRateErrCompEnableFlg == TRUE)
	{
		s16DeltaStrokePRateError 	= LAPCT_s16GetDeltaStrkPRateError(s16CircuitP, s16CircuitPRate10ms, s16PressError);
	}
	else {	}

	/*********************************************************************************************************/
	/* Limit Delta Stroke Error Compensation															Begin*/
	/*********************************************************************************************************/
	stPErrorCompLimit_r.s16NegativeCompDisableFlg	= s16NegativeCompDisableFlg;
	stPErrorCompLimit_r.s16TargetP					= s16TargetP;
	stPErrorCompLimit_r.s16CircuitP					= s16CircuitP;
	stPErrorCompLimit_r.s16TargetPRate10ms			= s16TargetPRate10ms;
	stPErrorCompLimit_r.s16DeltaStrokeErrorSum		= LAPCT_s16GetDelStrkErrorSum(s8PCtrlBoostMode, lapcts16IneffectiveStrokeCompFlg, s16DeltaStrokePerror, s16DeltaStrokePRateError);
	stPErrorCompLimit_r.s16DeltaStrokeErrorSumOld	= lapcts16DeltaStrokeErrorSumOld;
	stPErrorCompLimit_r.s8PCtrlBoostMode 			= s8PCtrlBoostMode ;
	stPErrorCompLimit_r.u16SafetyControlCompOnFlg	= u16SafetyControlCompOnFlg;

	/*TODO: Disable delta stroke limit for quick brake test, so that it might cause a big overshoot of pressure if ABS is not engaged */
	s16DeltaStrokeErrorSum = LAPCT_s16LimitPErrorComp(stPErrorCompLimit_r, stPCtrlInfo_r);
#if (M_LOGGER_VARIABLE_DEF == ENABLE)
	lapcts16DeltaStrokePerrorFinal = s16DeltaStrokeErrorSum;
#endif
	/*********************************************************************************************************/
	/* Limit Delta Stroke Error Compensation															End  */
	/*********************************************************************************************************/

	s16PreFadeoutFlg = LAPCT_s16GetPreFadeOutFlg(stPCtrlInfo_r, s16DeltaStrokeMap);

	stBoostDelStrkTarget_r.s16DeltaStrokeMap 			= s16DeltaStrokeMap;
	stBoostDelStrkTarget_r.s16DeltaStrokeErrorSum 		= s16DeltaStrokeErrorSum;
	stBoostDelStrkTarget_r.s16NegativeCompDisableFlg 	= s16NegativeCompDisableFlg;
	stBoostDelStrkTarget_r.s16LimDelStrokeMin 			= lapcts16LimDelStrokeMin;
	stBoostDelStrkTarget_r.s16LimDelStrkTargetMax 		= s16LimDelStrkTargetMax;

	s16BoostDeltaStrokeTarget = LAPCT_s16GetBoostDelStrkTarget(stPCtrlInfo_r, stBoostDelStrkTarget_r);

	u16FadeoutState1EndOK = LAPCT_u16GetFadeOutSt1EndOk(stPCtrlInfo_r, s16CircuitP);

	/*********************************************************************************************************/
	/* Calculate DeltaStroke for FadeoutState1 : Pressure reach to 0bar									Begin*/
	/*********************************************************************************************************/
	stFadeOutSt1DelStr_r.s16DeltaStrokeMap				= s16DeltaStrokeMap;
	stFadeOutSt1DelStr_r.s32MtrTargetPosi 				= s32MtrTargetPosi;
	stFadeOutSt1DelStr_r.s16DeltaStrokeTargetOld 		= lapcts16DeltaStrokeTargetOld;
	stFadeOutSt1DelStr_r.u16FadeoutState1EndOK 			= u16FadeoutState1EndOK;
	stFadeOutSt1DelStr_r.s32CurrentPosi 				= s32CurrentPosi;
	stFadeOutSt1DelStr_r.s16PosiError 					= s16PosiError;

	s16FadeOutSt1DeltaStrokeTarget = LAPCT_s16GetFadeoutSt1DelSTar(stPCtrlInfo_r, stFadeOutSt1DelStr_r);
	/*********************************************************************************************************/
	/* Calculate DeltaStroke for FadeoutState1 : Pressure reach to 0bar									End  */
	/*********************************************************************************************************/

	/*********************************************************************************************************/
	/* Motor Fadeout State for ESC																		Begin*/
	/*********************************************************************************************************/
	s16FadeOutEscDeltaStrokeTarget = LAPCT_s16FadeoutDelStrokeEsc(s8PCtrlState, s8PCtrlBoostMode, s8PCtrlFadeoutState, s32MtrTargetPosi, lapcts16DeltaStrokeTargetOld);
	/*********************************************************************************************************/
	/* Motor Fadeout State for ESC																		End  */
	/*********************************************************************************************************/



	s16DeltaStrokeTarget = LAPCT_s16GetFinalDelStrkTarget(stPCtrlInfo_r, s16BoostDeltaStrokeTarget, s16FadeOutSt1DeltaStrokeTarget, s16FadeOutEscDeltaStrokeTarget);

	PressCTRLLocalBus.s16DeltaStkTar = s16DeltaStrokeTarget;

	LAPCT_vDecidePressSourceMain();

	s16DeltaStrokeTarget = PressCTRLLocalBus.s16DeltaStkTar;

	if(PressCTRLLocalBus.s8PChamberVolume == U8_BOOST_BACKWARD)
	{
		s16DeltaStrokeTarget =  - (int16_t)((int32_t)s16DeltaStrokeTarget * U8_PRESS_VOLUME_RATIO);  /*  s16DeltaStrokeTarget =  s16DeltaStrokeTarget X 3 */
	}
	else if(PressCTRLLocalBus.s8PChamberVolume == U8_BOOST_HIGH_PRESS)
	{
		s16DeltaStrokeTarget =  (int16_t)(((int32_t)s16DeltaStrokeTarget * U8_PRESS_VOLUME_RATIO) / 2); /*  s16DeltaStrokeTarget =  s16DeltaStrokeTarget X 1.5 */
	}
	else { }


	/*********************************************************************************************************/
	/* Old Variable Update																				Begin*/
	/*********************************************************************************************************/
	lapcts16DeltaStrokeTargetOld = s16DeltaStrokeTarget;
	lapcts16DeltaStrokeErrorSumOld = s16DeltaStrokeErrorSum;

	/*********************************************************************************************************/
	/* Old Variable Update																				End  */

    /* SWC Output Variable Interface - Start*/
    stCalcDeltaStkOutpInfo.s16DeltaStkTar = s16DeltaStrokeTarget;
    stCalcDeltaStkOutpInfo.u16FadeoutState1EndOK = u16FadeoutState1EndOK;
    /*lapcts16CircuitPressureFail = s16CircuitPressureFail;*/

    /* SWC Output Variable Interface - End*/
#if (M_LOGGER_VARIABLE_DEF == ENABLE)
	lapcts16DeltaStrokePerror = s16DeltaStrokePerror;

	lapctu16WheelVolumeState       = u16WheelVolumeState;
	lapctu1NegativeCompDisableFlg  = s16NegativeCompDisableFlg ;
	lapctu1PreFadeoutFlg = s16PreFadeoutFlg;
/*	lapcts16Fadeout1TimerLog = lapcts16Fadeout1Timer;*/
#endif

    return stCalcDeltaStkOutpInfo;
}

static void Set_CalcDeltaStkOutpInfo(stCalcDeltaStkOutpInfo_t stCalcDeltaStkResult)
{
	Pct_5msCtrlBus.Pct_5msCtrlTarDeltaStk = stCalcDeltaStkResult.s16DeltaStkTar;
    PressCTRLLocalBus.u16FadeoutState1EndOK = stCalcDeltaStkResult.u16FadeoutState1EndOK;
#if (M_LOGGER_VARIABLE_DEF == ENABLE)
    lapctu16FadeoutState1EndOK =  PressCTRLLocalBus.u16FadeoutState1EndOK;
#endif
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16LimitPErrorComp
* CALLED BY:            LAPCT_stCalMtrCtrlTarget
* Preconditions:        none
* PARAMETER:            stPErrorCompLimit_r
* RETURN VALUE:         s16DeltaStrokeErrorSum
* Description:          Limit delta stroke error comp final
*   Metrics
*  55 (CountLine)
*  52 (CountLineCode)
*   2 (CountLineComment)
*   0 (CountLineInactive)
*   8 (Cyclomatic)
********************************************************************************/
static int16_t LAPCT_s16LimitPErrorComp(stP_ERROR_COMP_LIMIT_R_t stPErrorCompLimit_r, stPCTL_INFO_t stPCtrlInfo_r)
{
	static int16_t lapcts16PErrorCompCnt = 0;

	int16_t s16NegativeCompDisableFlg       = stPErrorCompLimit_r.s16NegativeCompDisableFlg ;
	int16_t s16TargetP                      = stPErrorCompLimit_r.s16TargetP				 ;
	int16_t s16CircuitP                      = stPErrorCompLimit_r.s16CircuitP				 ;
	int16_t s16TargetPRate10ms              = stPErrorCompLimit_r.s16TargetPRate10ms		 ;
	int16_t s16DeltaStrokeErrorSum          = stPErrorCompLimit_r.s16DeltaStrokeErrorSum	 ;
	int16_t s16DeltaStrokeErrorSumOld       = stPErrorCompLimit_r.s16DeltaStrokeErrorSumOld;
	int16_t s16DeltaStrokeErrorSumLtd       = 0;
	int16_t s16DeltaStrokeErrorSumLtdRef    = 0;
	int16_t s16DelStrokeErrLimitDiffSize 	= 0;

	if((stPCtrlInfo_r.s8PCtrlState == S8_PRESS_BOOST) || ((stPCtrlInfo_r.s8PCtrlState == S8_PRESS_FADE_OUT) && (stPCtrlInfo_r.s8PCtrlFadeoutState == S8_PCTRL_FADEOUT_STATE_1)))
	{
		if((stPCtrlInfo_r.s8PCtrlBoostMode == S8_MTR_BOOST_MODE_TORQUE)||(stPErrorCompLimit_r.s8PCtrlBoostMode  == S8_MTR_BOOST_MODE_TORQUE_TO_NORMAL)||(stPCtrlInfo_r.s8PCtrlBoostMode == S8_MTR_BOOST_MODE_NOT_CONTROL))
		{
			/* S8_MTR_BOOST_MODE_TORQUE : ABS Torque Control Mode */
			/* S8_MTR_BOOST_MODE_TORQUE_TO_NORMAL : ABS Torque to Normal   */
			s16DeltaStrokeErrorSumLtd = 0;
		}
		else/*if((stPCtrlInfo_r.s8PCtrlBoostMode != S8_BOOST_MODE_IN_ABS) && (stPCtrlInfo_r.s8PCtrlBoostMode != S8_MTR_BOOST_MODE_NOT_CONTROL))*/
		{
			if(stPErrorCompLimit_r.s8PCtrlBoostMode  == S8_MTR_BOOST_MODE_SAFETY_HIGH_RESPONSE)		/* ESC oversteer (Include ROP), ABS, TCS_1_mode : 300bar/s 		*/
			{
				s16DeltaStrokeErrorSumLtd = (int16_t)L_s32LimitMinMax(((int32_t)s16DeltaStrokeErrorSum * 10) / 10, ASW_S16_MIN, ASW_S16_MAX);
			}
			else if(stPErrorCompLimit_r.s8PCtrlBoostMode  == S8_MTR_BOOST_MODE_QUICK)
			{
				if(s16TargetP <= S16_PCTRL_PRESS_100_BAR)
				{
					s16DeltaStrokeErrorSumLtd = (s16DeltaStrokeErrorSum > 0) ? s16DeltaStrokeErrorSum : 0;	/* Negative Comp Disable */
				}
				else
				{
					s16DeltaStrokeErrorSumLtd = s16DeltaStrokeErrorSum;
				}
			}
			else
			{
				if((s16NegativeCompDisableFlg==1)&&(s16TargetP < S16_PCTRL_PRESS_5_BAR)&&((s16TargetP - s16CircuitP)<S16_PCTRL_PRESS_2_BAR))
				{
					s16DeltaStrokeErrorSumLtdRef = L_s16IInter2Point(s16TargetPRate10ms, 10, 50, 20, 30);
					s16DeltaStrokeErrorSumLtd = L_s16LimitMinMax(s16DeltaStrokeErrorSum, 0, s16DeltaStrokeErrorSumLtdRef);
					lapcts16PErrorCompCnt = 0;
				}
				else
				{
					lapcts16PErrorCompCnt = lapcts16PErrorCompCnt + 1;
					if(lapcts16PErrorCompCnt < 10)
					{
						s16DelStrokeErrLimitDiffSize = (L_s16Abs(s16DeltaStrokeErrorSum - s16DeltaStrokeErrorSumOld)) / 5; /* step by 20% */
						s16DelStrokeErrLimitDiffSize = (s16DelStrokeErrLimitDiffSize < 10) ? 10 : s16DelStrokeErrLimitDiffSize;
						s16DeltaStrokeErrorSumLtd = L_s16LimitDiff(s16DeltaStrokeErrorSum, s16DeltaStrokeErrorSumOld, s16DelStrokeErrLimitDiffSize, s16DelStrokeErrLimitDiffSize);
					}
					else
					{
						lapcts16PErrorCompCnt = 10;
						s16DeltaStrokeErrorSumLtd = s16DeltaStrokeErrorSum;
					}
				}

				if(stPCtrlInfo_r.s8PCtrlBoostMode == S8_MTR_BOOST_MODE_STOP)					/* Decel control : BBC, RBC : 200 bar/s Stop Speed				*/
				{
					s16DeltaStrokeErrorSumLtd = (int16_t)L_s32LimitMinMax(((int32_t)s16DeltaStrokeErrorSumLtd * S16_STOP_PERROR_LIMIT_GAIN) / 10, ASW_S16_MIN, ASW_S16_MAX);
				}
				else { }
			}

		}
	}
	else { /* Do Nothing */ }

	return s16DeltaStrokeErrorSumLtd;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16GenerateDeltaStrokeTarget
* CALLED BY:            LAPCT_stCalMtrCtrlTarget
* Preconditions:        none
* PARAMETER:            u16ControlOn, s16DeltaStrokeMap,
*                       s16DeltaStrokeErrorSum, s16PressError
* RETURN VALUE:         s16DeltaStrokeTarget
* Description:          Delta Stroke Target Generation Function
* Metrics
*  14 (CountLine)
*  11 (CountLineCode)
*   1 (CountLineComment)
*   0 (CountLineInactive)
*   2 (Cyclomatic)
********************************************************************************/
static int16_t LAPCT_s16GenerateDeltaStrokeTarget(int16_t s16DeltaStrokeMap, int16_t s16DeltaStrokeErrorSum, int16_t s16NegativeCompDisableFlg)
{
	int16_t s16DeltaStrokeTarget = 0;

/* Ineffective code */

	if ((s16NegativeCompDisableFlg == TRUE) && (s16DeltaStrokeErrorSum < 0))
	{
		s16DeltaStrokeErrorSum = 0;
	}
	else { /* Do nothing */ }

	s16DeltaStrokeTarget = s16DeltaStrokeMap + s16DeltaStrokeErrorSum;

#if (M_LOGGER_VARIABLE_DEF == ENABLE)
		lapcts16DeltaStrokePerrorFinal = s16DeltaStrokeErrorSum;
#endif
	return s16DeltaStrokeTarget;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16DctNegativeCompDisable
* CALLED BY:            LAPCT_stCalMtrCtrlTarget
* Preconditions:        none
* PARAMETER:            stNegativeComp_r
* RETURN VALUE:         s16CompDisableFlg
* Description:          Detect Negative Compensation Disable
* Metrics
*  132 (CountLine)
*  115 (CountLineCode)
*    9 (CountLineComment)
*    1 (CountLineInactive)
*   15 (Cyclomatic)
********************************************************************************/
static int16_t LAPCT_s16DctNegativeCompDisable(stNEGATIVE_COMP_R_t stNegativeComp_r)
{
	static int16_t lapcts16NegativeCompDisableFlg = FALSE;
	static int16_t lapcts16TPRateDiffCnt = 0;
	static int16_t lapcts16TPRateOld = 0;
	static int16_t lapcts16CompOffByTPRateFlg = FALSE;
	static int8_t lapcts8PCtrlStateOld = S8_PRESS_READY;
	static int16_t ladibs16StablePressCnt = 0;
	static int16_t lapcts16PressureReleasedCnt = 0;

	int16_t s16CompDisableFlg = FALSE;
	int16_t s16TPRateDiff = stNegativeComp_r.s16TPRate - lapcts16TPRateOld;
	int16_t s16PerrorThreshold = (int16_t)((int32_t)(stNegativeComp_r.s16TargetP * 10) / 100);
	int16_t s16TPRateError = stNegativeComp_r.s16TPRate - stNegativeComp_r.s16CPPRate10ms;

	s16PerrorThreshold = L_s16LimitMinMax(s16PerrorThreshold, S16_PCTRL_PRESS_0_3_BAR, S16_PCTRL_PRESS_2_BAR);

	if(stNegativeComp_r.s8PCtrlBoostMode == S8_MTR_BOOST_MODE_QUICK)			/* Quick Braking , AEB : 400bar/s 								*/
	{
		lapcts16NegativeCompDisableFlg = TRUE;
		lapcts16CompOffByTPRateFlg = TRUE;
	}
	else
	{
		if((lapcts16NegativeCompDisableFlg == FALSE) || (lapcts16CompOffByTPRateFlg==FALSE))
		{
			if ((stNegativeComp_r.s8PCtrlState == S8_PRESS_BOOST)&&(lapcts8PCtrlStateOld != S8_PRESS_BOOST))
			{
				if(stNegativeComp_r.s16CVVOpenFlg==TRUE)
				{
					/* If cut valve is open, we don't need to consider remained pressure */
					lapcts16NegativeCompDisableFlg = TRUE;
					lapcts16CompOffByTPRateFlg = TRUE;
				}
				else
				{
					if((stNegativeComp_r.s16CircuitP < S16_PCTRL_PRESS_0_5_BAR) || (lapcts16PressureReleasedCnt >= U8_T_100_MS))
					{
						lapcts16NegativeCompDisableFlg = TRUE;
						lapcts16CompOffByTPRateFlg = TRUE;
						lapcts16PressureReleasedCnt = -1;
					}
					else { /* If remained pressure exists, negative compensation is needed */ }
				}
			}
			else { /* Do nothing */ }
		}
		else
		{
			if ((stNegativeComp_r.s16Perror > -s16PerrorThreshold) && (stNegativeComp_r.s16Perror < s16PerrorThreshold)
#if DISABLE
					/*#if (__GM_SSTS_AHB_PRESS_CTRL_TEST == ENABLE)*/
					&&(stNegativeComp_r.s16CircuitP > S16_PCTRL_PRESS_1_BAR)
#endif
			)
			{
				if ((S16_NEGATIVE_COMP_POSITIVE_RATE_THR < s16TPRateError) || (s16TPRateError < S16_NEGATIVE_COMP_NEGATIVE_RATE_THR))
				{
					ladibs16StablePressCnt = ladibs16StablePressCnt - 2;
				}
				else
				{
					ladibs16StablePressCnt = ladibs16StablePressCnt + 1;
				}
			}
			else
			{
				ladibs16StablePressCnt = ladibs16StablePressCnt - 1;
			}

			/* lapcts16NegativeCompDisableFlg Condition Detection - Begin */
			if (ladibs16StablePressCnt >= 10)
			{
				lapcts16NegativeCompDisableFlg = FALSE;
				ladibs16StablePressCnt = 0;
			}
			else if(ladibs16StablePressCnt <= 0)
			{
				ladibs16StablePressCnt = 0;
			}
			else { /* Do Nothing */ }
			/* lapcts16NegativeCompDisableFlg Condition Detection - End */

			if((s16TPRateDiff < 0) && (stNegativeComp_r.s16TPRate >= 0))
			{
				lapcts16TPRateDiffCnt = lapcts16TPRateDiffCnt + 1;
			}
			else
			{
				lapcts16TPRateDiffCnt = lapcts16TPRateDiffCnt - 1;
			}

			/* lapcts16CompOffByTPRateFlg Condition Detection - Begin */
			if (lapcts16TPRateDiffCnt >= 5)
			{
				lapcts16CompOffByTPRateFlg = FALSE;
				lapcts16TPRateDiffCnt = 0;
			}
			else if(lapcts16TPRateDiffCnt <= 0)
			{
				lapcts16TPRateDiffCnt = 0;
			}
			else { /* Do Nothing */ }
			/* lapcts16CompOffByTPRateFlg Condition Detection - End */
		}
	}

	if(stNegativeComp_r.s8PCtrlState != S8_PRESS_BOOST)
	{
		if(stNegativeComp_r.s16CircuitP < S16_PCTRL_PRESS_0_5_BAR)
		{
			lapcts16PressureReleasedCnt = lapcts16PressureReleasedCnt + 1;
		}
		else
		{
			lapcts16PressureReleasedCnt = lapcts16PressureReleasedCnt - 1;
		}
		lapcts16PressureReleasedCnt = L_s16LimitMinMax(lapcts16PressureReleasedCnt , 0, U8_T_150_MS);
	}
	else
	{
		lapcts16PressureReleasedCnt = 0;
	}

	lapcts16TPRateOld = stNegativeComp_r.s16TPRate;
	lapcts8PCtrlStateOld = stNegativeComp_r.s8PCtrlState;

	s16CompDisableFlg = (
			(lapcts16NegativeCompDisableFlg == TRUE) && (lapcts16CompOffByTPRateFlg == TRUE)
			&& (stNegativeComp_r.s8PCtrlBoostMode != S8_MTR_BOOST_MODE_SAFETY_HIGH_RESPONSE) /*&& (stNegativeComp_r.s8PCtrlBoostMode != S8_BOOST_MODE_AEB)*/
			);										/* ESC oversteer (Include ROP), ABS, TCS_1_mode : 300bar/s 		*/

	lapctu1NegCompEnableByPerror = lapcts16NegativeCompDisableFlg;
	lapctu1NegCompEnableByTPRate = lapcts16CompOffByTPRateFlg;
	return s16CompDisableFlg;
}

#if (__P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE)
#else
/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16PickCircuitPressure
* CALLED BY:            LAPCT_stCalMtrCtrlTarget
* Preconditions:        none
* PARAMETER:            u8WhlCtrlIndex, s16PriCircuitP, s16SecCircuitP
* RETURN VALUE:         s16CircuitP
* Description:          Select Circuit Pressure Depend on Control Circuit
* Metrics
*   26 (CountLine)
*   24 (CountLineCode)
*    0 (CountLineComment)
*    0 (CountLineInactive)
*    4 (Cyclomatic)
********************************************************************************/
static int16_t LAPCT_s16PickCircuitPressure(uint8_t u8WhlCtrlIndex, int16_t s16PriCircuitP, int16_t s16SecCircuitP)
{
	int16_t s16CircuitP = S16_PCTRL_PRESS_0_BAR;
	static uint8_t lapctu8SelEstWhlPOld = WHL_CTRL_SEL_WHL_P_FR;
	uint8_t u8FrontWhlCtrlIndex = (u8WhlCtrlIndex / 10);
	uint8_t u8RearWhlCtrlIndex = (u8WhlCtrlIndex % 10);

	if(u8FrontWhlCtrlIndex > 0)
	{
		if ((u8FrontWhlCtrlIndex == WHL_CTRL_INDEX_BOTH) || (u8FrontWhlCtrlIndex == WHL_CTRL_INDEX_PRIMARY))
		{
			s16CircuitP = L_s16LimitMinMax(s16PriCircuitP, S16_PCTRL_PRESS_0_BAR, (S16_PCTRL_PRESS_200_BAR));
			lapctu8SelEstWhlPOld = WHL_CTRL_SEL_WHL_P_FR;
		}
		else
		{
			s16CircuitP = L_s16LimitMinMax(s16SecCircuitP, S16_PCTRL_PRESS_0_BAR, (S16_PCTRL_PRESS_200_BAR));
			lapctu8SelEstWhlPOld = WHL_CTRL_SEL_WHL_P_FL;
		}
	}
	else if (u8RearWhlCtrlIndex > 0)
	{
		if ((u8RearWhlCtrlIndex == WHL_CTRL_INDEX_BOTH) || (u8RearWhlCtrlIndex == WHL_CTRL_INDEX_PRIMARY))
		{
			s16CircuitP = L_s16LimitMinMax(s16PriCircuitP, S16_PCTRL_PRESS_0_BAR, (S16_PCTRL_PRESS_200_BAR));
			lapctu8SelEstWhlPOld = WHL_CTRL_SEL_WHL_P_RL;
		}
		else
		{
			s16CircuitP = L_s16LimitMinMax(s16SecCircuitP, S16_PCTRL_PRESS_0_BAR, (S16_PCTRL_PRESS_200_BAR));
			lapctu8SelEstWhlPOld = WHL_CTRL_SEL_WHL_P_RR;
		}
	}
	else
	{
		/* Maintain Last selected Estimate Wheel Pressure */
		if ((lapctu8SelEstWhlPOld == WHL_CTRL_SEL_WHL_P_FL) || (lapctu8SelEstWhlPOld == WHL_CTRL_SEL_WHL_P_RR))
		{
			s16CircuitP = L_s16LimitMinMax(s16SecCircuitP, S16_PCTRL_PRESS_0_BAR, (S16_PCTRL_PRESS_200_BAR));
		}
		else /* ((lapctu8SelEstWhlPOld == WHL_CTRL_SEL_WHL_P_FR) || (lapctu8SelEstWhlPOld == WHL_CTRL_SEL_WHL_P_RL)) */
		{
			s16CircuitP = L_s16LimitMinMax(s16PriCircuitP, S16_PCTRL_PRESS_0_BAR, (S16_PCTRL_PRESS_200_BAR));
		}
	}

	return s16CircuitP;
}
#endif	/* (__P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE) */

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16SelLimDelStrkTargetMax
* CALLED BY:            LAPCT_stCalMtrCtrlTarget
* Preconditions:        none
* PARAMETER:            s8PCtrlBoostMode, u16WheelVolumeState, s16TargetPRate10ms
* RETURN VALUE:         s16LimDelStrkTargetMax
* Description:          Choose Delta Stroke Target Max Limit with PCtrlBoostMode
* Metrics
*   48 (CountLine)
*   24 (CountLineCode)
*    3 (CountLineComment)
*   19 (CountLineInactive)
*    4 (Cyclomatic)
********************************************************************************/
static int16_t LAPCT_s16SelLimDelStrkTargetMax(int8_t s8PCtrlBoostMode,	uint16_t u16WheelVolumeState, int16_t s16TargetPRate10ms)
{
	static int16_t s16LimDelStrkTargetMax = S16_LIM_DEL_STRK_FOR_MOTOR_MAX;

	if (s8PCtrlBoostMode == S8_MTR_BOOST_MODE_SAFETY_HIGH_RESPONSE)		/* ESC oversteer (Include ROP), ABS, TCS_1_mode : 300bar/s 		*/
	{
		if ((u16WheelVolumeState == U16_SEL_F1WHEEL) || (u16WheelVolumeState == U16_SEL_F2WHEEL))/* Front 1Wheel */
		{
			s16LimDelStrkTargetMax = S16_LIM_DEL_STRK_MAX_SAFETY_F;
		}
		else if (u16WheelVolumeState == U16_SEL_R1WHEEL)/* Rear 1Wheel */
		{
			s16LimDelStrkTargetMax = S16_LIM_DEL_STRK_MAX_SAFETY_R;
		}
		else
		{
			s16LimDelStrkTargetMax = S16_LIM_DEL_STRK_FOR_MOTOR_MAX;
		}
	}
	else
	{
#if DISABLE /*(M_VARY_STRK_MAX == ENABLE)*/
		if (s8PCtrlBoostMode == S8_MTR_BOOST_MODE_QUICK)			/* Quick Braking , AEB : 400bar/s 								*/
		{
			s16LimDelStrkTargetMax = S16_LIM_DEL_STRK_FOR_MOTOR_MAX_FAST;
		}
		else
		{
			if(s16LimDelStrkTargetMax > S16_LIM_DEL_STRK_FOR_MOTOR_MAX)
			{
				if(s16TargetPRate10ms < 0)
				{
					s16LimDelStrkTargetMax = S16_LIM_DEL_STRK_FOR_MOTOR_MAX;
				}
				else{ }
			}
			else
			{
				s16LimDelStrkTargetMax = S16_LIM_DEL_STRK_FOR_MOTOR_MAX;
			}
		}
#else
		s16LimDelStrkTargetMax = S16_LIM_DEL_STRK_FOR_MOTOR_MAX;
#endif /* (M_VARY_STRK_MAX == ENABLE) */
	}

	return s16LimDelStrkTargetMax;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16FadeoutDelStrokeEsc
* CALLED BY:            LAPCT_stCalMtrCtrlTarget
* Preconditions:        none
* PARAMETER:            s8PCtrlState, s8PCtrlBoostMode, s8PCtrlFadeoutState, s32MtrTargetPosi, lapcts16DeltaStrokeTargetOld
* RETURN VALUE:         s16DeltaStrokeTarget
* Description:          FadeOut Delta Stroke After ESC Control
* Metrics
*   66 (CountLine)
*   61 (CountLineCode)
*    4 (CountLineComment)
*    0 (CountLineInactive)
*   11 (Cyclomatic)
********************************************************************************/
static int16_t LAPCT_s16FadeoutDelStrokeEsc(int8_t s8PCtrlState, int8_t s8PCtrlBoostMode, int8_t s8PCtrlFadeoutState, int32_t s32MtrTargetPosi, int16_t lapcts16DeltaStrokeTargetOld)
{
	int16_t s16DeltaStrokeTarget = 0;
	static uint16_t lapctu16ESCTriggeredFlg = FALSE;
	static uint16_t lapctu16EscEndCnt = 0;

	/* ESC End Trigger Stage Detection - Begin */
	if (s8PCtrlState == S8_PRESS_BOOST)
	{
		if (s8PCtrlBoostMode == S8_MTR_BOOST_MODE_SAFETY_HIGH_RESPONSE)			/* ESC oversteer (Include ROP), ABS, TCS_1_mode : 300bar/s 		*/
		{
			lapctu16ESCTriggeredFlg = TRUE;
			lapctu16EscEndCnt = M_ESC_END_DCT_SCAN - 1; /* Reset ESCEndCnt */
		}
		else
		{
			lapctu16EscEndCnt = (lapctu16EscEndCnt > 0) ? (lapctu16EscEndCnt - 1) : 0;
		}
	}
	else
	{
		if (lapctu16ESCTriggeredFlg == TRUE)
		{
			if ((s8PCtrlState == S8_PRESS_FADE_OUT) && (s8PCtrlFadeoutState == S8_PCTRL_FADEOUT_STATE_1))
			{
				if (lapctu16EscEndCnt == 0)
				{
					lapctu16ESCTriggeredFlg = FALSE;
				}
				else { /* Maintain lapctu16ESCTriggeredFlg */ }
			}
			else
			{
				lapctu16ESCTriggeredFlg = FALSE;
			}

		}
		else { }

		if (lapctu16ESCTriggeredFlg == TRUE)
		{
			if ((s8PCtrlState == S8_PRESS_FADE_OUT) && (s8PCtrlFadeoutState == S8_PCTRL_FADEOUT_STATE_1))
			{
				if (s32MtrTargetPosi > S16_FADEOUT_POSITION_L5)
				{
					if (lapcts16DeltaStrokeTargetOld > 0)
					{
						s16DeltaStrokeTarget = L_s16LimitDiff( S16_FADEOUT_DEL_STROKE_AFTER_ESC, lapcts16DeltaStrokeTargetOld, 100, 100);
					}
					else
					{
						s16DeltaStrokeTarget = L_s16LimitDiff(S16_FADEOUT_DEL_STROKE_AFTER_ESC, lapcts16DeltaStrokeTargetOld, 100, 100);
					}
				}
				else { /* Maintain Original DeltaStrokeTarget for Fadeout */ }
			}
			else
			{
				lapctu16ESCTriggeredFlg = FALSE;
			}
		}
		else { }
	}

	return s16DeltaStrokeTarget;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_u16DecideSafetyBreakComp
* CALLED BY:            LAPCT_u16GetSafetyCtrlCompOnFlg
* Preconditions:        none
* PARAMETER:            s16TargetP, s16TargetPressOld, u16SafetyControlCompOnFlg
* RETURN VALUE:         u16SafetyControlCompOnFlg
* Description:          Detect Compensation Enable Condition during Safety Control(TCS/ESC)
* Metrics
*   14 (CountLine)
*   13 (CountLineCode)
*    1 (CountLineComment)
*    0 (CountLineInactive)
*    3 (Cyclomatic)
********************************************************************************/
static uint16_t LAPCT_u16DecideSafetyBreakComp(int16_t s16TargetP, int16_t s16TargetPressOld, uint16_t u16SafetyControlCompOnFlg)
{
	if ((s16TargetP - s16TargetPressOld) > 0)
	{
		u16SafetyControlCompOnFlg = TRUE;
	}
	else if ((s16TargetP - s16TargetPressOld) < 0)
	{
		u16SafetyControlCompOnFlg = FALSE;
	}
	else { /* Maintain u16SafetyControlCompOnFlg */ }

	return u16SafetyControlCompOnFlg;
}

#if DISABLE
/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16GetMaxEstWheelPress
* CALLED BY:            LAPCT_s16GetEstWheelPOnSafety
* Preconditions:        none
* PARAMETER:            stEstWheelPress_r
* RETURN VALUE:         s16MaxEstWheelPress
* Description:          Choose Estimated Wheel Pressure
* Metrics
*   35 (CountLine)
*   31 (CountLineCode)
*    0 (CountLineComment)
*    0 (CountLineInactive)
*    4 (Cyclomatic)
********************************************************************************/
static int16_t LAPCT_s16GetMaxEstWheelPress(const stEstWheelPress_t*  stEstWheelPress_r)
{
	int16_t s16MaxEstWheelPressF = S16_PCTRL_PRESS_0_BAR;
	int16_t s16MaxEstWheelPressR = S16_PCTRL_PRESS_0_BAR;
	int16_t s16MaxEstWheelPress = S16_PCTRL_PRESS_0_BAR;

	if (stEstWheelPress_r->s16EstWheelPressFL >= stEstWheelPress_r->s16EstWheelPressFR)
	{
		s16MaxEstWheelPressF = stEstWheelPress_r->s16EstWheelPressFL;
	}
	else
	{
		s16MaxEstWheelPressF = stEstWheelPress_r->s16EstWheelPressFR;
	}

	if (stEstWheelPress_r->s16EstWheelPressRR >= stEstWheelPress_r->s16EstWheelPressRL)
	{
		s16MaxEstWheelPressR = stEstWheelPress_r->s16EstWheelPressRR;
	}
	else
	{
		s16MaxEstWheelPressR = stEstWheelPress_r->s16EstWheelPressRL;
	}

	if (s16MaxEstWheelPressF >= s16MaxEstWheelPressR)
	{
		s16MaxEstWheelPress = s16MaxEstWheelPressF;
	}
	else
	{
		s16MaxEstWheelPress = s16MaxEstWheelPressR;
	}

	return s16MaxEstWheelPress;
}
#endif

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16GetEstWheelPOnSafety
* CALLED BY:            LAPCT_stCalMtrCtrlTarget
* Preconditions:        none
* PARAMETER:            u16WheelVolumeState, u8WhlCtrlIndex, stEstWheelPress_r
* RETURN VALUE:         s16CircuitP
* Description:          Choose Estimated Wheel Pressure for Safety Control(ESC/TCS)
* Metrics
*   33 (CountLine)
*   31 (CountLineCode)
*    0 (CountLineComment)
*    0 (CountLineInactive)
*    5 (Cyclomatic)
********************************************************************************/
static int16_t LAPCT_s16GetEstWheelPOnSafety(uint16_t u16WheelVolumeState, uint8_t u8WhlCtrlIndex, const stEstWheelPress_t* stEstWheelPress_r)
{
	static uint8_t lapctu8SelEstWhlPOld = WHL_CTRL_SEL_WHL_P_FR;
	int16_t s16CircuitP = S16_PCTRL_PRESS_0_BAR;
	uint8_t u8FrontWhlCtrlIndex = (u8WhlCtrlIndex / 10);
	uint8_t u8RearWhlCtrlIndex = (u8WhlCtrlIndex % 10);

	if(u8FrontWhlCtrlIndex > 0)
	{
		if ((u8FrontWhlCtrlIndex == WHL_CTRL_INDEX_BOTH) || (u8FrontWhlCtrlIndex == WHL_CTRL_INDEX_PRIMARY))
		{
			s16CircuitP = stEstWheelPress_r->s16EstWheelPressFR;
			lapctu8SelEstWhlPOld = WHL_CTRL_SEL_WHL_P_FR;
		}
		else
		{
			s16CircuitP = stEstWheelPress_r->s16EstWheelPressFL;
			lapctu8SelEstWhlPOld = WHL_CTRL_SEL_WHL_P_FL;
		}
	}
	else if (u8RearWhlCtrlIndex > 0)
	{
		if ((u8RearWhlCtrlIndex == WHL_CTRL_INDEX_BOTH) || (u8RearWhlCtrlIndex == WHL_CTRL_INDEX_PRIMARY))
		{
			s16CircuitP = stEstWheelPress_r->s16EstWheelPressRL;
			lapctu8SelEstWhlPOld = WHL_CTRL_SEL_WHL_P_RL;
		}
		else
		{
			s16CircuitP = stEstWheelPress_r->s16EstWheelPressRR;
			lapctu8SelEstWhlPOld = WHL_CTRL_SEL_WHL_P_RR;
		}
	}
	else
	{
		/* Maintain Last selected Estimate Wheel Pressure */
		if (lapctu8SelEstWhlPOld == WHL_CTRL_SEL_WHL_P_FL)
		{
			s16CircuitP = stEstWheelPress_r->s16EstWheelPressFL;
		}
		else if (lapctu8SelEstWhlPOld == WHL_CTRL_SEL_WHL_P_FR)
		{
			s16CircuitP = stEstWheelPress_r->s16EstWheelPressFR;
		}
		else if (lapctu8SelEstWhlPOld == WHL_CTRL_SEL_WHL_P_RL)
		{
			s16CircuitP = stEstWheelPress_r->s16EstWheelPressRL;
		}
		else	/* (lapctu8SelEstWhlPOld == WHL_CTRL_SEL_WHL_P_RR) */
		{
			s16CircuitP = stEstWheelPress_r->s16EstWheelPressRR;
		}

	}

	return s16CircuitP;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16DctIneffectiveStroke
* CALLED BY:            LAPCT_stCalMtrCtrlTarget
* Preconditions:        none
* PARAMETER:            s16IneffectiveStrokeCompFlg, stIneffStrokeComp_r
* RETURN VALUE:         s16IneffectiveStrokeCompFlg
* Description:          Detect Ineffective Stroke Compensation Condition
* Metrics
*   74 (CountLine)
*   67 (CountLineCode)
*    2 (CountLineComment)
*    0 (CountLineInactive)
*   11 (Cyclomatic)
********************************************************************************/
static int16_t LAPCT_s16DctIneffectiveStroke(int16_t s16IneffectiveStrokeCompFlg, stINEFF_STROKE_CMP_R_t stIneffStrokeComp_r)
{
	static int16_t lapcts16DetectCnt = 0;
	static int16_t lapcts16TimeOut = U16_T_1_S;
	static int16_t lapcts16CircuitPOld = 0;

	if(s16IneffectiveStrokeCompFlg==FALSE)
	{
		lapcts16TimeOut = U16_T_1_S;
		if((stIneffStrokeComp_r.s8PCtrlState != S8_PRESS_BOOST)
				|| (stIneffStrokeComp_r.s16TargetP < S16_PCTRL_PRESS_0_5_BAR))
		{
			if(stIneffStrokeComp_r.s16CircuitP < S16_PCTRL_PRESS_0_5_BAR)
			{
				lapcts16DetectCnt = lapcts16DetectCnt + 1;
				if(stIneffStrokeComp_r.s16CircuitP < S16_PCTRL_PRESS_0_1_BAR)
				{
					lapcts16DetectCnt = lapcts16DetectCnt + 2;
				}
				else { }

				if(lapcts16DetectCnt >= U8_T_150_MS)
				{
					s16IneffectiveStrokeCompFlg = TRUE;
					lapcts16DetectCnt = 0;
				}
				else { }
			}
			else
			{
				lapcts16DetectCnt = 0;
			}
		}
		else
		{
			lapcts16DetectCnt = 0;
		}
	}
	else
	{
		if((stIneffStrokeComp_r.s8PCtrlState == S8_PRESS_BOOST) && (stIneffStrokeComp_r.s16TargetP >= S16_PCTRL_PRESS_0_5_BAR))
		{
			lapcts16TimeOut = lapcts16TimeOut - 1;
			if((lapcts16CircuitPOld > stIneffStrokeComp_r.s16CircuitP) && (stIneffStrokeComp_r.s16CircuitP >= S16_PCTRL_PRESS_0_5_BAR))
			{
				lapcts16DetectCnt = lapcts16DetectCnt + 1;
			}
			else { }

			if(lapcts16DetectCnt >= 2)
			{
				if(stIneffStrokeComp_r.s16CircuitP < (stIneffStrokeComp_r.s16TargetP + S16_PCTRL_PRESS_2_BAR))
				{
					s16IneffectiveStrokeCompFlg = FALSE;
					lapcts16DetectCnt = 0;
				}
				else { }
			}
			else { }
		}
		else { }

		if(lapcts16TimeOut <= 0)
		{
			s16IneffectiveStrokeCompFlg = FALSE;
			lapcts16DetectCnt = 0;
		}
	}

	lapcts16CircuitPOld = stIneffStrokeComp_r.s16CircuitP;

	return s16IneffectiveStrokeCompFlg;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_u16GetSafetyCtrlCompOnFlg
* CALLED BY:            LAPCT_stCalMtrCtrlTarget
* Preconditions:        none
* PARAMETER:            s8PCtrlBoostMode, s16TargetP, s16TargetPressOld
* RETURN VALUE:         lapctu16SafetyControlCompOnFlg
* Description:          Return Safety Control Compensation Active Flag
* Metrics
*   16 (CountLine)
*   13 (CountLineCode)
*    1 (CountLineComment)
*    0 (CountLineInactive)
*    2 (Cyclomatic)
********************************************************************************/
static uint16_t LAPCT_u16GetSafetyCtrlCompOnFlg(int8_t s8PCtrlBoostMode, int16_t s16TargetP, int16_t s16TargetPressOld)
{
	static uint16_t lapctu16SafetyControlCompOnFlg = FALSE;

	if (s8PCtrlBoostMode == S8_MTR_BOOST_MODE_SAFETY_HIGH_RESPONSE)			/* ESC oversteer (Include ROP), ABS, TCS_1_mode : 300bar/s 		*/
	{
		lapctu16SafetyControlCompOnFlg = LAPCT_u16DecideSafetyBreakComp(s16TargetP, s16TargetPressOld, lapctu16SafetyControlCompOnFlg);
	}
	else
	{
		/* Using the biggest Circuit Pressure for Normal Braking */
		lapctu16SafetyControlCompOnFlg = FALSE;
	}

	return lapctu16SafetyControlCompOnFlg;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16GetCirPOnSafetyCtrl
* CALLED BY:            LAPCT_stCalMtrCtrlTarget
* Preconditions:        none
* PARAMETER:            s8PCtrlBoostMode, u16SafetyControlCompOnFlg, s16TargetPressOld, s16CircuitP, s16EstWhlP
* RETURN VALUE:         s16CircuitP
* Description:          Select Estimated Wheel Pressure instead of Circuit Pressure
*                       when Pressure Compensation is activated for Safety Mode(ESC/TCS) and TargetPressure is rising
* Metrics
*   15 (CountLine)
*   13 (CountLineCode)
*    3 (CountLineComment)
*    0 (CountLineInactive)
*    3 (Cyclomatic)
********************************************************************************/
static int16_t LAPCT_s16GetCirPOnSafetyCtrl(int8_t s8PCtrlBoostMode, uint16_t u16SafetyControlCompOnFlg, int16_t s16TargetPressOld, int16_t s16CircuitP, int16_t s16EstWhlP)
{
	if (s8PCtrlBoostMode == S8_MTR_BOOST_MODE_SAFETY_HIGH_RESPONSE)			/* ESC oversteer (Include ROP), ABS, TCS_1_mode : 300bar/s 		*/
	{
		if ((u16SafetyControlCompOnFlg == TRUE) && (s16TargetPressOld > 0))
		{
			/* Using Estimated Wheel Pressure instead of Circuit Pressure if Safety Function Act and Target Pressure rising */
			s16CircuitP = s16EstWhlP;
		}
		else { /* Maintain Picked Circuit Pressure */ }
	}
	else{ /* Maintain Picked Circuit Pressure */ }

	return s16CircuitP;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16GetDelStrkErrorSum
* CALLED BY:            LAPCT_stCalMtrCtrlTarget
* Preconditions:        none
* PARAMETER:            s16SafetyControlActFlg, s16IneffectiveStrokeCompFlg, s16DeltaStrokePerror, s16DeltaStrokePRateError
* RETURN VALUE:         s16DeltaStrokeErrorSum
* Description:          Return Delta Stroke Error(Pressure Error, Pressure Rate Error) Sum
* Metrics
*   22 (CountLine)
*   20 (CountLineCode)
*    0 (CountLineComment)
*    0 (CountLineInactive)
*    5 (Cyclomatic)
********************************************************************************/
static int16_t LAPCT_s16GetDelStrkErrorSum(int8_t s8PCtrlBoostMode, int16_t s16IneffectiveStrokeCompFlg, int16_t s16DeltaStrokePerror, int16_t s16DeltaStrokePRateError)
{
	int16_t s16DeltaStrokeErrorSum = 0;

	if (s8PCtrlBoostMode == S8_MTR_BOOST_MODE_SAFETY_HIGH_RESPONSE)			/* ESC oversteer (Include ROP), ABS, TCS_1_mode : 300bar/s 		*/
	{
		if (s16IneffectiveStrokeCompFlg == FALSE)
		{
			s16DeltaStrokeErrorSum = (s16DeltaStrokePerror > 0) ? s16DeltaStrokePerror : (s16DeltaStrokePerror / 3);
		}
		else
		{
			s16DeltaStrokeErrorSum = (s16DeltaStrokePerror > 0) ? s16DeltaStrokePerror : 0;
		}
	}
	else if(s8PCtrlBoostMode == S8_MTR_BOOST_MODE_QUICK/*S8_BOOST_MODE_AEB*/)			/* Quick Braking , AEB : 400bar/s 								*/
	{
		s16DeltaStrokeErrorSum = (s16DeltaStrokePerror > 0) ? s16DeltaStrokePerror : (s16DeltaStrokePerror / 3);
	}
	else
	{
		s16DeltaStrokeErrorSum = s16DeltaStrokePerror + s16DeltaStrokePRateError;
	}

	return s16DeltaStrokeErrorSum;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16GetPreFadeOutFlg
* CALLED BY:            LAPCT_stCalMtrCtrlTarget
* Preconditions:        none
* PARAMETER:            stPCtrlInfo_r, s16DeltaStrokeMap
* RETURN VALUE:         s16PreFadeoutFlg
* Description:          Detect PrefadeOut Condition
* Metrics
*   12 (CountLine)
*   10 (CountLineCode)
*    1 (CountLineComment)
*    0 (CountLineInactive)
*    3 (Cyclomatic)
********************************************************************************/
static int16_t LAPCT_s16GetPreFadeOutFlg(stPCTL_INFO_t stPCtrlInfo_r, int16_t s16DeltaStrokeMap)
{
	int16_t s16PreFadeoutFlg = FALSE;

	if ((stPCtrlInfo_r.s8PCtrlState == S8_PRESS_FADE_OUT) && (stPCtrlInfo_r.s8PCtrlFadeoutState == S8_PCTRL_FADEOUT_STATE_1))
	{
		s16PreFadeoutFlg = (s16DeltaStrokeMap < S16_FADEOUT_DEL_STROKE_L5) ? TRUE : FALSE;
	}
	else { /* Do Nothing */ }

	return s16PreFadeoutFlg;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16GetBoostDelStrkTarget
* CALLED BY:            LAPCT_stCalMtrCtrlTarget
* Preconditions:        none
* PARAMETER:            stPCtrlInfo_r, stBoostDelStrkTarget
* RETURN VALUE:         s16BoostDeltaStrokeTarget
* Description:          Return Final Delta Stroke Target
* Metrics
*   23 (CountLine)
*   16 (CountLineCode)
*    3 (CountLineComment)
*    0 (CountLineInactive)
*    3 (Cyclomatic)
********************************************************************************/
static int16_t LAPCT_s16GetBoostDelStrkTarget(stPCTL_INFO_t stPCtrlInfo_r, stBOOST_DEL_STRK_TARGET_t stBoostDelStrkTarget)
{
	int16_t s16BoostDeltaStrokeTarget = 0;

	if (stPCtrlInfo_r.s8PCtrlState == S8_PRESS_BOOST)
	{
		if(stPCtrlInfo_r.s8PCtrlBoostMode != S8_MTR_BOOST_MODE_NOT_CONTROL)
		{
			s16BoostDeltaStrokeTarget = LAPCT_s16GenerateDeltaStrokeTarget(stBoostDelStrkTarget.s16DeltaStrokeMap, stBoostDelStrkTarget.s16DeltaStrokeErrorSum, stBoostDelStrkTarget.s16NegativeCompDisableFlg);
			/*s16BoostDeltaStrokeTarget = (stBoostDelStrkTarget.s16DeltaStrokeMap + stBoostDelStrkTarget.s16DeltaStrokeErrorSum);*/

#if (M_LOGGER_VARIABLE_DEF == ENABLE)
			lapcts16DeltaStrokeTargetWoLtd = s16BoostDeltaStrokeTarget;
#endif
			/*TODO: Disable delta stroke limit for quick brake test, so that it might cause a big overshoot of pressure if ABS is not engaged */
			s16BoostDeltaStrokeTarget = L_s16LimitMinMax(s16BoostDeltaStrokeTarget, stBoostDelStrkTarget.s16LimDelStrokeMin, stBoostDelStrkTarget.s16LimDelStrkTargetMax);

		}
		else { /* Do Nothing */ }
	}
	else { /* Do Nothing */ }

	return s16BoostDeltaStrokeTarget;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_u16GetFadeOutSt1EndOk
* CALLED BY:            LAPCT_stCalMtrCtrlTarget
* Preconditions:        none
* PARAMETER:            stPCtrlInfo_r, stBoostDelStrkTarget
* RETURN VALUE:         u16FadeoutState1EndOK
* Description:          Detect Fadeout State 1 End condition
* Metrics
*   42 (CountLine)
*   35 (CountLineCode)
*    2 (CountLineComment)
*    0 (CountLineInactive)
*    4 (Cyclomatic)
********************************************************************************/
static uint16_t LAPCT_u16GetFadeOutSt1EndOk(stPCTL_INFO_t stPCtrlInfo_r, int16_t s16CircuitP)
{
	uint16_t u16FadeoutState1EndOK = FALSE;
	uint16_t u16FadeoutState1PStable = FALSE;
	uint16_t u16FadeoutState1Timeout = FALSE;

	static int16_t lapcts16PStableCnt = 0;
	static int16_t lapcts16PStableCntOld = 0;
	static int16_t lapcts16Fadeout1Timer = 0;

	if ((stPCtrlInfo_r.s8PCtrlState == S8_PRESS_FADE_OUT) && (stPCtrlInfo_r.s8PCtrlFadeoutState == S8_PCTRL_FADEOUT_STATE_1))
	{
		lapcts16PStableCntOld = lapcts16PStableCnt;
		lapcts16Fadeout1Timer = lapcts16Fadeout1Timer + 1;

		lapcts16PStableCnt = LAPCT_s16GetCircuitPStableCnt(s16CircuitP, lapcts16PStableCnt);

		if ((lapcts16PStableCntOld >= 4) && (lapcts16PStableCnt == 0))
		{
			u16FadeoutState1PStable = TRUE;
		}
		else { /* Do Nothing */ }

		u16FadeoutState1Timeout = LAPCT_s16FadeoutSt1TimeoutEndOk(lapcts16Fadeout1Timer);

		if ((u16FadeoutState1PStable == TRUE) || (u16FadeoutState1Timeout == TRUE))
		{
			u16FadeoutState1EndOK = TRUE;
			lapcts16PStableCnt = 0;
			lapcts16Fadeout1Timer = 0;
		}
		else { /* Do Nothing */ }
	}
	else
	{
		lapcts16PStableCnt = 0;
		lapcts16PStableCntOld = 0;
		lapcts16Fadeout1Timer = 0;
	}

	return u16FadeoutState1EndOK;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16GetFinalDelStrkTarget
* CALLED BY:            LAPCT_stCalMtrCtrlTarget
* Preconditions:        none
* PARAMETER:            stPCtrlInfo_r, s16BoostDeltaStrokeTarget, s16FadeOutSt1DeltaStrokeTarget, s16FadeOutEscDeltaStrokeTarget
* RETURN VALUE:         s16FianlDeltaStrokeTarget
* Description:          Pick Delta Stroke Target for each Control state
* Metrics
*   23 (CountLine)
*   21 (CountLineCode)
*    0 (CountLineComment)
*    0 (CountLineInactive)
*    4 (Cyclomatic)
********************************************************************************/
static int16_t LAPCT_s16GetFinalDelStrkTarget(stPCTL_INFO_t stPCtrlInfo_r, int16_t s16BoostDeltaStrokeTarget, int16_t s16FadeOutSt1DeltaStrokeTarget, int16_t s16FadeOutEscDeltaStrokeTarget)
{
	int16_t s16FianlDeltaStrokeTarget = 0;

	if (s16FadeOutEscDeltaStrokeTarget != 0)
	{
		s16FianlDeltaStrokeTarget = s16FadeOutEscDeltaStrokeTarget;
	}
	else if(s16FadeOutSt1DeltaStrokeTarget != 0)
	{
		s16FianlDeltaStrokeTarget = s16FadeOutSt1DeltaStrokeTarget;
	}
	else
	{
		s16FianlDeltaStrokeTarget = s16BoostDeltaStrokeTarget;
	}

	return s16FianlDeltaStrokeTarget;
}

/* FadeOut State 1 */
/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16GetFadeoutSt1DelSTar
* CALLED BY:            LAPCT_stCalMtrCtrlTarget
* Preconditions:        none
* PARAMETER:            stPCtrlInfo_r, stFadeOutSt1DelStr_r
* RETURN VALUE:         s16DeltaStrokeTarget
* Description:          Return Delta Stroke for FadeOut State 1
* Metrics
*   35 (CountLine)
*   32 (CountLineCode)
*    1 (CountLineComment)
*    0 (CountLineInactive)
*    6 (Cyclomatic)
********************************************************************************/
static int16_t LAPCT_s16GetFadeoutSt1DelSTar(stPCTL_INFO_t stPCtrlInfo_r, stFADEOUT_ST1_DEL_S_TAR_t stFadeOutSt1DelStr_r)
{
	int16_t s16DeltaStrokeTarget = 0;

	if( (stPCtrlInfo_r.s8PCtrlState == S8_PRESS_FADE_OUT) && (stPCtrlInfo_r.s8PCtrlFadeoutState==S8_PCTRL_FADEOUT_STATE_1))
	{
		if (stFadeOutSt1DelStr_r.s32MtrTargetPosi > S16_FADEOUT_POSITION_L5)
		{
			if (stFadeOutSt1DelStr_r.s16DeltaStrokeTargetOld > 0)
			{
				s16DeltaStrokeTarget = L_s16LimitDiff(S16_FADEOUT_DEL_STROKE_L5, stFadeOutSt1DelStr_r.s16DeltaStrokeTargetOld, 100, 100);
			}
			else
			{
				s16DeltaStrokeTarget = L_s16LimitDiff(S16_FADEOUT_DEL_STROKE_L5, stFadeOutSt1DelStr_r.s16DeltaStrokeTargetOld, 10, 10);
			}
		}
		else
		{
			if (stFadeOutSt1DelStr_r.u16FadeoutState1EndOK == FALSE)
			{
				s16DeltaStrokeTarget = LAPCT_s16MtrPosiFadeOut(stFadeOutSt1DelStr_r.s32MtrTargetPosi, stFadeOutSt1DelStr_r.s16DeltaStrokeTargetOld, stFadeOutSt1DelStr_r.s32CurrentPosi, stFadeOutSt1DelStr_r.s16PosiError);
			}
			else
			{
				s16DeltaStrokeTarget = L_s16LimitDiff(S16_FADEOUT_DEL_STROKE_L4, stFadeOutSt1DelStr_r.s16DeltaStrokeTargetOld, 10, 10);
			}
		}
		s16DeltaStrokeTarget =  (stFadeOutSt1DelStr_r.s16DeltaStrokeMap < s16DeltaStrokeTarget) ? stFadeOutSt1DelStr_r.s16DeltaStrokeMap : s16DeltaStrokeTarget;
	}
	else { /* Do Nothing */ }


	return s16DeltaStrokeTarget;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16FadeoutSt1TimeoutEndOk
* CALLED BY:            LAPCT_u16GetFadeOutSt1EndOk
* Preconditions:        none
* PARAMETER:            s16CircuitP
* RETURN VALUE:         u16FadeoutState1EndOK
* Description:          Detect Timout of Fadeout State1
* Metrics
*   19 (CountLine)
*   16 (CountLineCode)
*    1 (CountLineComment)
*    0 (CountLineInactive)
*    3 (Cyclomatic)
********************************************************************************/
static uint16_t LAPCT_s16FadeoutSt1TimeoutEndOk(int16_t s16CircuitP)
{
	static int16_t lapcts16Fadeout1Timer = 0;
	uint16_t u16FadeoutState1EndOK = FALSE;

	lapcts16Fadeout1Timer = lapcts16Fadeout1Timer + 1;

	if (s16CircuitP > S16_FADEOUT_OK_THR)
	{
		if (lapcts16Fadeout1Timer >= U8_T_500_MS)
		{
			u16FadeoutState1EndOK = TRUE;
			lapcts16Fadeout1Timer = 0;
		}
		else { /* Do Nothing */ }
	}

	return u16FadeoutState1EndOK;
}


static uint8_t LAPCT_u8DecidePRateErrComp(int8_t s8PCtrlState, int8_t s8PCtrlBoostMode)
{
	uint8_t u8PRateErrCompEnable = FALSE;

	if(s8PCtrlState == S8_PRESS_BOOST)
	{
		if((s8PCtrlBoostMode == S8_MTR_BOOST_MODE_NORMAL)
		|| (s8PCtrlBoostMode == S8_MTR_BOOST_MODE_STOP)) /* other mode may be added, if needed */
		{
			u8PRateErrCompEnable = TRUE;
		}
		else { }
	}
	else { }

	return u8PRateErrCompEnable;
}

static int16_t LAPCT_s16PickCircuitPRate(uint8_t u8WhlCtrlIndex, int16_t s16PriPRate10ms, int16_t s16SecPRate10ms)
{
	int16_t s16CircuitPRate = S16_P_RATE_0_BPS;
	static uint8_t lapctu8SelectedWhlOld = WHL_CTRL_SEL_WHL_P_FR;
	uint8_t u8FrontWhlCtrlIndex = (u8WhlCtrlIndex / 10);
	uint8_t u8RearWhlCtrlIndex = (u8WhlCtrlIndex % 10);

	if(u8FrontWhlCtrlIndex > 0)
	{
		if ((u8FrontWhlCtrlIndex == WHL_CTRL_INDEX_BOTH) || (u8FrontWhlCtrlIndex == WHL_CTRL_INDEX_PRIMARY))
		{
			s16CircuitPRate = L_s16LimitMinMax(s16PriPRate10ms, S16_P_RATE_0_BPS, (S16_P_RATE_1000_BPS));
			lapctu8SelectedWhlOld = WHL_CTRL_SEL_WHL_P_FR;
		}
		else
		{
			s16CircuitPRate = L_s16LimitMinMax(s16SecPRate10ms, S16_P_RATE_0_BPS, (S16_P_RATE_1000_BPS));
			lapctu8SelectedWhlOld = WHL_CTRL_SEL_WHL_P_FL;
		}
	}
	else if (u8RearWhlCtrlIndex > 0)
	{
		if ((u8RearWhlCtrlIndex == WHL_CTRL_INDEX_BOTH) || (u8RearWhlCtrlIndex == WHL_CTRL_INDEX_PRIMARY))
		{
			s16CircuitPRate = L_s16LimitMinMax(s16PriPRate10ms, S16_P_RATE_0_BPS, (S16_P_RATE_1000_BPS));
			lapctu8SelectedWhlOld = WHL_CTRL_SEL_WHL_P_RL;
		}
		else
		{
			s16CircuitPRate = L_s16LimitMinMax(s16SecPRate10ms, S16_P_RATE_0_BPS, (S16_P_RATE_1000_BPS));
			lapctu8SelectedWhlOld = WHL_CTRL_SEL_WHL_P_RR;
		}
	}
	else
	{
		/* Maintain Last selected Estimate Wheel Pressure */
		if ((lapctu8SelectedWhlOld == WHL_CTRL_SEL_WHL_P_FL) || (lapctu8SelectedWhlOld == WHL_CTRL_SEL_WHL_P_RR))
		{
			s16CircuitPRate = L_s16LimitMinMax(s16SecPRate10ms, S16_P_RATE_0_BPS, (S16_P_RATE_1000_BPS));
		}
		else /* ((lapctu8SelEstWhlPOld == WHL_CTRL_SEL_WHL_P_FR) || (lapctu8SelEstWhlPOld == WHL_CTRL_SEL_WHL_P_RL)) */
		{
			s16CircuitPRate = L_s16LimitMinMax(s16PriPRate10ms, S16_P_RATE_0_BPS, (S16_P_RATE_1000_BPS));
		}
	}

	return s16CircuitPRate;
}

static int16_t LAPCT_s16GetCircuitPStableCnt(int16_t s16CircuitP, int16_t s16PStableCnt)
{
	if (s16CircuitP <= S16_FADEOUT_OK_THR)
	{
		s16PStableCnt = s16PStableCnt + 1;

		if (s16PStableCnt >= 5)
		{
			s16PStableCnt = 0;
		}
		else { /* Do Nothing */ }
	}
	else { /* Do Nothing */ }

	return s16PStableCnt;
}

static int16_t LAPCT_s16MtrPosiFadeOut(int32_t s32MtrTargetPosi, int16_t s16DeltaStrokeTargetOld, int32_t s32CurrentPosi, int16_t s16PosiError)
{
	int16_t s16DeltaStrokeTarget = 0;
	int16_t s16FadeoutDelStroke = 0;
	int16_t s16FadeoutDelStrokeIncLimit = 0;

	s16FadeoutDelStroke = (int16_t)L_s32IInter6Point(s32MtrTargetPosi, S16_FADEOUT_POSITION_L0, S16_FADEOUT_DEL_STROKE_L0,
																S16_FADEOUT_POSITION_L1, S16_FADEOUT_DEL_STROKE_L1,
																S16_FADEOUT_POSITION_L2, S16_FADEOUT_DEL_STROKE_L2,
																S16_FADEOUT_POSITION_L3, S16_FADEOUT_DEL_STROKE_L3,
																S16_FADEOUT_POSITION_L4, S16_FADEOUT_DEL_STROKE_L4,
																S16_FADEOUT_POSITION_L5, S16_FADEOUT_DEL_STROKE_L5);

	s16FadeoutDelStrokeIncLimit = (int16_t)L_s32IInter4Point(s16DeltaStrokeTargetOld, S16_FADEOUT_DEL_STROKE_OLD_L0, S16_FADEOUT_DEL_STROKE_DIFF_L0,
																		S16_FADEOUT_DEL_STROKE_OLD_L1, S16_FADEOUT_DEL_STROKE_DIFF_L1,
																		S16_FADEOUT_DEL_STROKE_OLD_L2, S16_FADEOUT_DEL_STROKE_DIFF_L2,
																		S16_FADEOUT_DEL_STROKE_OLD_L3, S16_FADEOUT_DEL_STROKE_DIFF_L3);

	s16DeltaStrokeTarget = L_s16LimitDiff(s16FadeoutDelStroke, s16DeltaStrokeTargetOld, s16FadeoutDelStrokeIncLimit, 10);

	if ((s32MtrTargetPosi <= 0) && (s32CurrentPosi <= S32_ORIGIN_SET_MAX_THR))
	{
		if(s16DeltaStrokeTarget >= S16_FADEOUT_DEL_STROKE_L0)
		{
			s16DeltaStrokeTarget = 0;	/* END of Fade-out state 2 */
		}
		else{ /* Do Nothing */ }
	}
	else
	{
		if ( s16DeltaStrokeTarget >= 0)
		{
			s16DeltaStrokeTarget = -5;
		}
		else{ /* Do Nothing */ }
	}

	return s16DeltaStrokeTarget;
}

#define PCT_5MSCTRL_STOP_SEC_CODE
#include "Pct_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
