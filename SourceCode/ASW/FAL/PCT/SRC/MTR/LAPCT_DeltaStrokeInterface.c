/*
 * LAPCT_DeltaStrokeInterface.c
 *
 *  Created on: 2015. 3. 18.
 *      Author: Seokjong.kim@halla.com
 */
#include "Pct_5msCtrl.h"
#include "LAPCT_DeltaStrokeInterface.h"

#define M_TMP_BOOST_TRANSLATION						DISABLE
#define M_TARGET_SHAPE_GEN							ENABLE
#define U8_TARGET_SHAPE_MATCH_TIME					U8_T_50_MS
#define U8_TARGET_SHAP_MATCH_PATTERN				{10, 20, 30, 40, 50, 60, 70, 80, 90, 100}

#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
int16_t  lidss16PistonP;
#else
int16_t  lidss16PriCircuitP;
int16_t  lidss16SecCircuitP;
#endif

int16_t  lidss16TargetP;
int16_t lidss16TarrgetPOld;
int16_t  lidss16TargetPRate10ms;

#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
int16_t  lidss16PistPRate10ms;
#else
int16_t  lidss16PriPRate10ms;
int16_t  lidss16SecPRate10ms;
#endif

int16_t  lidss16PedalTravelRateMmPSec;

uint16_t lidsu16WheelVolumeState;
uint16_t lidsu16AHBOnFlg;
int8_t   lidss8PCtrlState;
int8_t   lidss8PCtrlBoostMode;
int8_t   lidss8MtrPCtrlBoostMode;
int8_t   lidss8PCtrlFadeoutState;

uint8_t  lidsu8PreboostActFlg;
/*uint8_t  lidsu8PreboostEscActFlg;*/

int32_t  lidss32MtrTargetPosi;
int32_t  lidss32CurrentPosi;
int32_t  lidss32TatEstStrkPosiOfWhlP;

int16_t  lidss16InitFastApplyFlg;
int16_t  lidss16StandstillCtrl;


int16_t  lidss16VehicleSpeed;
int16_t  lids16CVVOpenFlg;

int16_t  lidss16EstWheelPressFL;
int16_t  lidss16EstWheelPressFR;
int16_t  lidss16EstWheelPressRL;
int16_t  lidss16EstWheelPressRR;

uint8_t  lidsu8WhlCtrlIndex;

uint8_t lidsu8TargetShapeGenFlg;
static int8_t LAPCT_s8TranslateBoostMode(int8_t s8PCtrlBoostModeOrg);
static int16_t LAPCT_s16TargetShapeGen(int16_t s16TargetP, int8_t s8PCtrlState, int8_t s8PCtrlBoostMode, uint8_t u8PreboostActFlg);

void LAPCT_vDeltaSTrokeInterface(void)
{
	static int8_t lidss8PCtrlStateOld = S8_PRESS_READY;
	static int16_t lidss16TarrgetPOldTmp = S16_PCTRL_PRESS_0_BAR;
	int16_t s16TargetP = S16_PCTRL_PRESS_0_BAR;

#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
	lidss16PistonP            					= (int16_t) Pct_5msCtrlBus.Pct_5msCtrlPistPFildInfo.PistPFild_1_100Bar;
#else
	lidss16PriCircuitP            					= (int16_t) Pct_5msCtrlBus.Pct_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar;
	lidss16SecCircuitP            					= (int16_t) Pct_5msCtrlBus.Pct_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar;
#endif

	lidss16TargetP                					= PressCTRLLocalBus.s16MuxPwrPistonTarP;
	lidss16TarrgetPOld								= lidss16TargetP - PressCTRLLocalBus.s16MuxPwrPistonTarDP;
	lidss16TargetPRate10ms        					= PressCTRLLocalBus.s16MuxPwrPistonTarPRate;

#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
	lidss16PistPRate10ms          					= (int16_t)Pct_5msCtrlBus.Pct_5msCtrlPistPRateInfo.PistPChgDurg10ms;
#else
	lidss16PriPRate10ms          					= (int16_t)Pct_5msCtrlBus.Pct_5msCtrlCircPRateInfo.PrimCircPChgDurg10ms;
	lidss16SecPRate10ms          					= (int16_t)Pct_5msCtrlBus.Pct_5msCtrlCircPRateInfo.SecdCircPChgDurg10ms;
#endif

	lidss16PedalTravelRateMmPSec  					= (int16_t)Pct_5msCtrlBus.Pct_5msCtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec;

	lidsu16WheelVolumeState       					= PressCTRLLocalBus.u16MuxWheelVolumeState;
	lidsu16AHBOnFlg               					= PressCTRLLocalBus.u8PCtrlAct;
	lidss8PCtrlState              					= PressCTRLLocalBus.s8PCtrlState;
	lidss8PCtrlBoostMode          					= PressCTRLLocalBus.s8PCtrlBoostMode;
	lidss8MtrPCtrlBoostMode							= LAPCT_s8TranslateBoostMode(PressCTRLLocalBus.s8PCtrlBoostMode);
	lidss8PCtrlFadeoutState       					= PressCTRLLocalBus.s8PCtrlFadeoutState;

	lidsu8PreboostActFlg							= Pct_5msCtrlBus.Pct_5msCtrlPreBoostMod;
	/*lidsu8PreboostEscActFlg 						= (uint8_t)(liu8EscPreActFL | liu8EscPreActFR | liu8EscPreActRL | liu8EscPreActRR);*/
	lidss32MtrTargetPosi          					= Pct_5msCtrlBus.Pct_5msCtrlIdbMotPosnInfo.MotTarPosn;
	lidss32CurrentPosi            					= Pct_5msCtrlBus.Pct_5msCtrlMotRotgAgSigInfo.StkPosnMeasd;
	lidss32TatEstStrkPosiOfWhlP   					= PressCTRLLocalBus.s32TatEstStrkPosiOfWhlP;	/* Stroke Recovery */

	lidss16InitFastApplyFlg       					= (int16_t)((Pct_5msCtrlBus.Pct_5msCtrlBrkPedlStatusInfo.PanicBrkStFlg==TRUE) || (Pct_5msCtrlBus.Pct_5msCtrlBrkPedlStatus1msInfo.PanicBrkSt1msFlg==TRUE));
	lidss16StandstillCtrl         					= (int16_t)Pct_5msCtrlBus.Pct_5msCtrlBaseBrkCtrlModInfo.VehStandStillStFlg;


	lidss16VehicleSpeed           					= (int16_t)Pct_5msCtrlBus.Pct_5msCtrlVehSpdFild;
	lids16CVVOpenFlg								= Pct_5msCtrlBus.Pct_5msCtrlIdbVlvActInfo.CutVlvOpenFlg;

	lidss16EstWheelPressFL        					= (int16_t)Pct_5msCtrlBus.Pct_5msCtrlEstimdWhlPInfo.FrntLeEstimdWhlP;
	lidss16EstWheelPressFR        					= (int16_t)Pct_5msCtrlBus.Pct_5msCtrlEstimdWhlPInfo.FrntRiEstimdWhlP;
	lidss16EstWheelPressRL        					= (int16_t)Pct_5msCtrlBus.Pct_5msCtrlEstimdWhlPInfo.ReLeEstimdWhlP;
	lidss16EstWheelPressRR        					= (int16_t)Pct_5msCtrlBus.Pct_5msCtrlEstimdWhlPInfo.ReRiEstimdWhlP;

	lidsu8WhlCtrlIndex								= PressCTRLLocalBus.u8WhlCtrlIndex;

#if (M_TARGET_SHAPE_GEN == ENABLE)
/*********************************************************************************************************/
/* TargetPress Shape Generate																		Begin*/
/*********************************************************************************************************/
	s16TargetP = LAPCT_s16TargetShapeGen(lidss16TargetP, PressCTRLLocalBus.s8PCtrlState, lidss8MtrPCtrlBoostMode, lidsu8PreboostActFlg);

	lidsu8TargetShapeGenFlg = (lidss16TargetP != s16TargetP) ? TRUE : FALSE;

	if (lidsu8TargetShapeGenFlg == TRUE)
	{
		if ((lidss8PCtrlState == S8_PRESS_BOOST) && (lidss8PCtrlStateOld != S8_PRESS_BOOST))
		{
			lidss16TarrgetPOldTmp = S16_PCTRL_PRESS_0_BAR;
		}
		else {}

		lidss16TarrgetPOld = lidss16TarrgetPOldTmp;
	}
	else {}


	lidss16TargetP = s16TargetP;

	lidss8PCtrlStateOld = lidss8PCtrlState;
	lidss16TarrgetPOldTmp = lidss16TargetP;
/*********************************************************************************************************/
/* TargetPress Shape Generate																		End  */
/*********************************************************************************************************/
#endif	/* (M_TARGET_SHAPE_GEN == ENABLE) */
}

#if DISABLE
#define S8_BOOST_MODE_NOT_CONTROL			0
#define S8_BOOST_MODE_RESPONSE_L0			1		/* ABS Torque Control Mode 										*/
#define S8_BOOST_MODE_RESPONSE_L1			2		/* Quick Braking , AEB : 400bar/s 								*/
#define S8_BOOST_MODE_RESPONSE_L2			3		/* ESC oversteer (Include ROP), ABS, TCS_1_mode : 300bar/s 		*/
#define S8_BOOST_MODE_RESPONSE_L3			4		/* TCS_2_mode, TSP, ESC understeer : 200bar/s  					*/
#define S8_BOOST_MODE_RESPONSE_L4			5		/* Decel control : SCC : 200 bar/s dependent on speed			*/
#define S8_BOOST_MODE_RESPONSE_L5			6		/* Decel control : BBC, RBC : 200 bar/s Normal Speed			*/
#define S8_BOOST_MODE_RESPONSE_L6			7		/* Decel control : BBC, RBC : 200 bar/s Low Speed				*/
#define S8_BOOST_MODE_RESPONSE_L7			8		/* Decel control : BBC, RBC : 200 bar/s Stop Speed				*/
#define S8_BOOST_MODE_RESPONSE_L8			9       /* CBC SLS, EBD 200bar/s Reduce Pressure Control function		*/
#define S8_BOOST_MODE_RESPONSE_L9			10		/* TCS_4_mode, Hold function(HSA, AVH)AC Hold 100bar/s			*/
#define S8_BOOST_MODE_RESPONSE_L10			11		/* Release of Hold function										*/
#define S8_BOOST_MODE_RESPONSE_L11			12		/* ABS Torque to Normal											*/
#define S8_BOOST_MODE_RESPONSE_L12			13		/* PreBoost Mode												*/

#define S8_MTR_BOOST_MODE_NOT_CONTROL  			0
#define S8_MTR_BOOST_MODE_STOP					1
#define S8_MTR_BOOST_MODE_NORMAL				2
#define S8_MTR_BOOST_MODE_SAFETY_LOW_RESPONSE	3
#define S8_MTR_BOOST_MODE_SAFETY_HIGH_RESPONSE	4
#define S8_MTR_BOOST_MODE_ACTIVE				5
#define S8_MTR_BOOST_MODE_TORQUE				6
#define S8_MTR_BOOST_MODE_TORQUE_TO_NORMAL		7
#define S8_MTR_BOOST_MODE_AEB					8
#define S8_MTR_BOOST_MODE_QUICK					9
#define S8_MTR_BOOST_MODE_PREBOOST				10
#endif

static int8_t LAPCT_s8TranslateBoostMode(int8_t s8PCtrlBoostModeOrg)
{
	int8_t s8PCtrlBoostMode = S8_BOOST_MODE_NOT_CONTROL;

#if (M_TMP_BOOST_TRANSLATION == ENABLE)
/* Temp Translate Logic - Start */
/* This temp Logic will be deleted After BoostMode matching with PressCtrlMode Module */
	int8_t s8PCtrlBoostModeTmp = S8_BOOST_MODE_NOT_CONTROL;
	switch(s8PCtrlBoostModeOrg)
	{
		case S8_BOOST_MODE_NOT_CONTROL:
			s8PCtrlBoostModeTmp = S8_BOOST_MODE_NOT_CONTROL;
			break;
		case S8_BOOST_MODE_QUICK:
		case S8_BOOST_MODE_AEB:
			s8PCtrlBoostModeTmp = S8_BOOST_MODE_RESPONSE_L1;
			break;
		case S8_BOOST_MODE_IN_ABS:
			s8PCtrlBoostModeTmp = S8_BOOST_MODE_RESPONSE_L13;
			break;
		case S8_BOOST_MODE_SAFETY:
			s8PCtrlBoostModeTmp = S8_BOOST_MODE_RESPONSE_L2;
			break;
		case S8_BOOST_MODE_NORMAL:
			s8PCtrlBoostModeTmp = S8_BOOST_MODE_RESPONSE_L5;
			break;
		case S8_BOOST_MODE_CREEP:
			s8PCtrlBoostModeTmp = S8_BOOST_MODE_RESPONSE_L6;
			break;
		case S8_BOOST_MODE_STOP:
			s8PCtrlBoostModeTmp = S8_BOOST_MODE_RESPONSE_L7;
			break;
		case S8_BOOST_MODE_ACTIVE:
			s8PCtrlBoostModeTmp = S8_BOOST_MODE_RESPONSE_L9;
			break;
		case S8_BOOST_MODE_ABS_TO_NORMAL:
			s8PCtrlBoostModeTmp = S8_BOOST_MODE_RESPONSE_L11;
			break;
		default:
			s8PCtrlBoostModeTmp = s8PCtrlBoostModeOrg;
			break;
	}

	if (lidss16InitFastApplyFlg == TRUE)
	{
		s8PCtrlBoostModeTmp = S8_BOOST_MODE_RESPONSE_L1;
	}
	else { }
	s8PCtrlBoostModeOrg = s8PCtrlBoostModeTmp;
/* Temp Translate Logic - End */
#endif

	switch(s8PCtrlBoostModeOrg)
	{
	case S8_BOOST_MODE_NOT_CONTROL:
		s8PCtrlBoostMode = S8_MTR_BOOST_MODE_NOT_CONTROL;
		break;
	case S8_BOOST_MODE_RESPONSE_L0:	/* ABS Torque Control Mode 										*/
		s8PCtrlBoostMode = S8_MTR_BOOST_MODE_TORQUE;
		break;
	case S8_BOOST_MODE_RESPONSE_L1:	/* Quick Braking , AEB : 400bar/s 								*/
		s8PCtrlBoostMode = S8_MTR_BOOST_MODE_QUICK;
		break;
	case S8_BOOST_MODE_RESPONSE_L2:	/* ESC oversteer (Include ROP), ABS, TCS_1_mode : 300bar/s 		*/
		s8PCtrlBoostMode = S8_MTR_BOOST_MODE_SAFETY_HIGH_RESPONSE;
		break;
	case S8_BOOST_MODE_RESPONSE_L3:	/* TCS_2_mode, TSP, ESC understeer : 200bar/s  					*/
		s8PCtrlBoostMode = S8_MTR_BOOST_MODE_SAFETY_LOW_RESPONSE;
		break;
	case S8_BOOST_MODE_RESPONSE_L4:	/* Decel control : SCC : 200 bar/s dependent on speed			*/
	case S8_BOOST_MODE_RESPONSE_L5:	/* Decel control : BBC, RBC : 200 bar/s Normal Speed			*/
	case S8_BOOST_MODE_RESPONSE_L6:	/* Decel control : BBC, RBC : 200 bar/s Low Speed				*/
		s8PCtrlBoostMode = S8_MTR_BOOST_MODE_NORMAL;
		break;
	case S8_BOOST_MODE_RESPONSE_L7:	/* Decel control : BBC, RBC : 200 bar/s Stop Speed				*/
		s8PCtrlBoostMode = S8_MTR_BOOST_MODE_STOP;
		break;
	case S8_BOOST_MODE_RESPONSE_L8:	/* temp location : CBC SLS, EBD 200bar/s Reduce Pressure Control function		*/
		s8PCtrlBoostMode = S8_MTR_BOOST_MODE_NORMAL;
		break;
	case S8_BOOST_MODE_RESPONSE_L10:/* temp location : Release of Hold function										*/
		s8PCtrlBoostMode = S8_MTR_BOOST_MODE_NORMAL;
		break;
	case S8_BOOST_MODE_RESPONSE_L9:	/* CBC SLS, EBD 200bar/s Reduce Pressure Control function		*/
		s8PCtrlBoostMode = S8_MTR_BOOST_MODE_ACTIVE;
		break;
	case S8_BOOST_MODE_RESPONSE_L11:/* ABS Torque to Normal											*/
		s8PCtrlBoostMode = S8_MTR_BOOST_MODE_TORQUE_TO_NORMAL;
		break;
	case S8_BOOST_MODE_RESPONSE_L12:/* PreBoost Mode												*/
		s8PCtrlBoostMode = S8_MTR_BOOST_MODE_PREBOOST;
		break;
	case S8_BOOST_MODE_RESPONSE_L13:
		s8PCtrlBoostMode = S8_MTR_BOOST_MODE_ABS;
		break;
	default:
		s8PCtrlBoostMode = s8PCtrlBoostModeOrg;
		break;
	}

	return s8PCtrlBoostMode;

}

#if (M_TARGET_SHAPE_GEN == ENABLE)
/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16TargetShapeGen
* CALLED BY:            LAPCT_s16GetFadeOutSt1EndOk
* Preconditions:        none
* PARAMETER:            s16TargetP, s8PCtrlState, s8PCtrlBoostMode
* RETURN VALUE:         s16TargetPGen
* Description:          Target Shape Matching duing 50ms on Normal BBC Boost Mode
* Metrics
*   - (CountLine)
*   - (CountLineCode)
*    0 (CountLineComment)
*    0 (CountLineInactive)
*    5 (Cyclomatic)
********************************************************************************/
static int16_t LAPCT_s16TargetShapeGen(int16_t s16TargetP, int8_t s8PCtrlState, int8_t s8PCtrlBoostMode, uint8_t u8PreboostActFlg)
{
	static uint8_t lapctu8MatchRatio[U8_TARGET_SHAPE_MATCH_TIME] = U8_TARGET_SHAP_MATCH_PATTERN;
	static uint8_t lapctu8ShapeMatchCnt = 0;
	static int8_t lapcts8PCtrlStateOld = S8_PRESS_READY;
	static uint8_t lapctu8PreboostEnterFlg = FALSE;

	int16_t s16TargetPGen = s16TargetP;

	if (s8PCtrlState == S8_PRESS_READY)
	{
		s16TargetPGen = S16_PCTRL_PRESS_0_BAR;
		lapctu8PreboostEnterFlg = FALSE;
	}
	else if (s8PCtrlState == S8_PRESS_PREBOOST)
	{
		if (u8PreboostActFlg == TRUE)
		{
			lapctu8PreboostEnterFlg = TRUE;
			s16TargetPGen = S16_PREBOOST_ST2_PRESSURE;
		}
		else /* if(u8PreboostEscActFlg == TRUE) */
		{
			s16TargetPGen = S16_PREBOOST_ESC_PRESSURE;
		}
		lapctu8ShapeMatchCnt = 0;
	}
	else if (s8PCtrlState == S8_PRESS_BOOST)
	{
		if ((s8PCtrlBoostMode == S8_MTR_BOOST_MODE_NORMAL) || (s8PCtrlBoostMode == S8_MTR_BOOST_MODE_STOP))
		{
			if (lapctu8ShapeMatchCnt < U8_TARGET_SHAPE_MATCH_TIME)
			{
				if (lapctu8PreboostEnterFlg == TRUE )
				{
					s16TargetPGen = (int16_t)L_s32LimitMinMax((int32_t)s16TargetP * lapctu8MatchRatio[lapctu8ShapeMatchCnt] / 100, -S16_PCTRL_PRESS_180_BAR, S16_PCTRL_PRESS_180_BAR);
					if (s16TargetPGen < S16_PREBOOST_ST2_PRESSURE)
					{
						s16TargetPGen = S16_PREBOOST_ST2_PRESSURE;
					}
					else {}
				}
				else
				{
					s16TargetPGen = (int16_t)L_s32LimitMinMax((int32_t)s16TargetP * lapctu8MatchRatio[lapctu8ShapeMatchCnt] / 100, -S16_PCTRL_PRESS_180_BAR, S16_PCTRL_PRESS_180_BAR);
				}

				lapctu8ShapeMatchCnt = lapctu8ShapeMatchCnt + 1;
			}
			else
			{
				s16TargetPGen = s16TargetP;
			}
		}
		else
		{
			/* Do Not Use Target Shape matching */
			lapctu8ShapeMatchCnt = U8_TARGET_SHAPE_MATCH_TIME;
			s16TargetPGen = s16TargetP;
		}
	}
	else
	{
		lapctu8PreboostEnterFlg = FALSE;
		lapctu8ShapeMatchCnt = 0;
	}

	lapcts8PCtrlStateOld = s8PCtrlState;

	return s16TargetPGen;
}
#endif /* (M_TARGET_SHAPE_GEN == ENABLE) */
