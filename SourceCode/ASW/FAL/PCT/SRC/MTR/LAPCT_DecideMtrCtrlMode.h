/**
 * @defgroup LAPCT_DecideMtrCtrlMode LAPCT_DecideMtrCtrlMode
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LAPCT_DecideMtrCtrlMode.h
 * @brief       Local Header File
 * @date        20YY. MM. DD.
 ******************************************************************************/
 
#ifndef LAPCT_DECIDEMTRCTRLMODE_H_
#define LAPCT_DECIDEMTRCTRLMODE_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/


/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
#define M_LOGGER_VARIABLE_DEF                   (ENABLE)
#define M_MOVING_AVG_TARGET_STROKE              (ENABLE)

#define U16_P_CTRL_MODE_STROKE                  (0)
#define U16_P_CTRL_MODE_TORQUE                  (1)

#if (M_LOGGER_VARIABLE_DEF == ENABLE)
#define lapctu1DelStrkPrateErrorCompFlg1		lapctDelStrkActFlg.bit7
#define lapctu1DelStrkPErrorCompFlg1			lapctDelStrkActFlg.bit6
#define lapctu1DelStrkPrateErrorCompFlg2		lapctDelStrkActFlg.bit5
#define lapctu1DelStrkPErrorCompFlg2			lapctDelStrkActFlg.bit4
#define lapctu1DelStrkPErrorCompLimited			lapctDelStrkActFlg.bit3
#define lapctu1NegativeCompDisableFlg 			lapctDelStrkActFlg.bit2
#define lapctu1NegCompEnableByPerror 			lapctDelStrkActFlg.bit1
#define lapctu1NegCompEnableByTPRate  			lapctDelStrkActFlg.bit0
#endif
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct{
	int16_t s16EstWheelPressFL;
	int16_t s16EstWheelPressFR;
	int16_t s16EstWheelPressRL;
	int16_t s16EstWheelPressRR;
}stEstWheelPress_t;

typedef struct {
	int8_t   s8PCtrlState;
	int16_t  s16CircuitP;
	int16_t  s16PdtRateMmPSec;
	int16_t  s16StandstillCtrl;
	uint16_t u16WheelVolumeState;
	int16_t  s16TargetP;
} stINEFF_STROKE_CMP_R_t;
/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
#if (M_LOGGER_VARIABLE_DEF == ENABLE)
/* Temp Global Variable Extern for Log************************************/
extern int16_t lapcts16DeltaStrokeMap;
extern int16_t lapcts16DeltaStrokePerror;
extern U8_BIT_STRUCT_t lapctDelStrkActFlg;
#endif

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void LAPCT_vDecideMtrCtrlMode(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* LAPCT_DECIDEMTRCTRLMODE_H_ */
/** @} */
