/*
 * LAPCT_DeltaStrokeInterface.h
 *
 *  Created on: 2015. 3. 18.
 *      Author: Seokjong.kim@halla.com
 */

#ifndef LAPCT_DELTASTROKEINTERFACE_H_
#define LAPCT_DELTASTROKEINTERFACE_H_

#define S8_MTR_BOOST_MODE_NOT_CONTROL  			0
#define S8_MTR_BOOST_MODE_STOP					1
#define S8_MTR_BOOST_MODE_NORMAL				2
#define S8_MTR_BOOST_MODE_SAFETY_LOW_RESPONSE	3
#define S8_MTR_BOOST_MODE_SAFETY_HIGH_RESPONSE	4
#define S8_MTR_BOOST_MODE_ACTIVE				5
#define S8_MTR_BOOST_MODE_TORQUE				6
#define S8_MTR_BOOST_MODE_TORQUE_TO_NORMAL		7
#define S8_MTR_BOOST_MODE_AEB					8
#define S8_MTR_BOOST_MODE_QUICK					9
#define S8_MTR_BOOST_MODE_PREBOOST				10
#define S8_MTR_BOOST_MODE_ABS					11

#define S16_PREBOOST_ST1_PRESSURE					S16_PCTRL_PRESS_0_15_BAR
#define S16_PREBOOST_ST2_PRESSURE					S16_PCTRL_PRESS_0_3_BAR
#define S16_PREBOOST_ESC_PRESSURE					S16_PCTRL_PRESS_1_BAR /* max 3bar */

#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
extern int16_t  lidss16PistonP;
#else
extern int16_t  lidss16PriCircuitP;
extern int16_t  lidss16SecCircuitP;
#endif

extern int16_t  lidss16TargetP;
extern int16_t  lidss16TarrgetPOld;
extern int16_t  lidss16TargetPRate10ms;

#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
extern int16_t  lidss16PistPRate10ms;
#else

extern int16_t  lidss16PriPRate10ms;
extern int16_t  lidss16SecPRate10ms;
#endif

extern int16_t  lidss16PedalTravelRateMmPSec;

extern uint16_t lidsu16WheelVolumeState;
extern uint16_t lidsu16AHBOnFlg;
extern int8_t   lidss8PCtrlState;
extern int8_t   lidss8PCtrlBoostMode;
extern int8_t   lidss8MtrPCtrlBoostMode;
extern int8_t   lidss8PCtrlFadeoutState;

extern int32_t  lidss32MtrTargetPosi;
extern int32_t  lidss32CurrentPosi;
extern int32_t  lidss32TatEstStrkPosiOfWhlP;

extern int16_t  lidss16InitFastApplyFlg;
extern int16_t  lidss16StandstillCtrl;

extern int16_t  lidss16VehicleSpeed;
extern int16_t  lids16CVVOpenFlg;

extern int16_t  lidss16EstWheelPressFL;
extern int16_t  lidss16EstWheelPressFR;
extern int16_t  lidss16EstWheelPressRL;
extern int16_t  lidss16EstWheelPressRR;

extern uint8_t  lidsu8WhlCtrlIndex;

extern uint8_t lidsu8TargetShapeGenFlg;

extern void LAPCT_vDeltaSTrokeInterface(void);

#endif /* LAPCT_DELTASTROKEINTERFACE_H_ */
