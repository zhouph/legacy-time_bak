/*******************************************************************************
* Project Name:     IDB
* File Name:        LAPCT_DeltaStrokeMap.c
* Description:
* Logic version:
********************************************************************************
*  Modification Log
*  Date         Author          Description
*  -----        -----           -----
********************************************************************************/
/* Includes ******************************************************************/
#include "Pct_5msCtrl.h"
#include "LAPCT_DeltaStrokeMap.h"
#include "LAPCT_DecideMtrCtrlMode.h"
#include "LAPCT_LoadMapData.h"
#include "LAPCT_DeltaStrokeInterface.h"

/* Logcal Definiton  *********************************************************/
#define M_MOVING_AVG_TARGET_STROKE				(ENABLE)

#define MOVING_AVG_RESET_ACT						TRUE
#define MOVING_AVG_MAINTAIN							FALSE

#define S16_INIT_TP_COMP_DEL_STROKE					(46)

#define S16_REDUCE_GAIN_OF_RELEASE_MAP 				(9)
#define S16_REDUCE_GAIN_OF_APPLY_MAP 				(9)
#define S16_GAIN_OF_APPLY_MAP_INIT_FAST				(10)

#define S16_TARGET_STRK_MOVING_AVG_ARR_1			(1)
#define S16_TARGET_STRK_MOVING_AVG_ARR_2			(2)
#define S16_TARGET_STRK_MOVING_AVG_ARR_5			(5)
#define S16_TARGET_STRK_MOVING_AVG_ARR_10			(10)

#define S16_INIT_INEFFECTIVE_STRK_COMP_SCAN			(10)
#define S16_INIT_INEFFECTIVE_STRK_COMP_SCAN_ESC		(4)

#define S16_ABS_FADE_OUT_DAMPING_TIME				(10)
#define S16_ABS_FADE_OUT_RISE_RATE					S16_PCTRL_PRESS_0_3_BAR

#define S16_INEFFECTIVE_STROKE						(0)

#if (M_LOGGER_VARIABLE_DEF == ENABLE)
extern int16_t lapcts16TargetPressOldlog;
extern int16_t lapcts16DeltaStrokeMapAvg;
#endif
int16_t lapcts16MovAvgBuffResetFlg;
int16_t lapcts16MovingAvgSel;
/* GlobalFunction prototype **************************************************/

/* LocalFunction prototype ***************************************************/
static int16_t LAPCT_s16CalcDeltaStrokeMap(int16_t s16TargetP, int16_t s16TargetPressOld, uint16_t u16WheelVolumeState, int16_t s16MaxStrokeMapSelFlg);
static int16_t LAPCT_s16CalcDeltaStrokeMapESC(int16_t s16TargetP, int16_t s16TargetPressOld, int16_t s16CircuitP, uint16_t u16WheelVolumeState, int16_t s16IdenticalCircuitFlg);
#if M_MOVING_AVG_TARGET_STROKE == ENABLE
static int16_t LAPCT_s16MovAvgTargetStrokeAll(int16_t s16DelStrokeTarget, int16_t s16ResetStaticVarFlg, int16_t s16CircuitP, int16_t s16MovingAvgSize);
#endif
static int16_t LAPCT_s16GetLimitedDelStroke(int16_t s16DeltaStroke, int16_t s16LimDelStrkTargetMax, int16_t s16LimDelStrokeMin, int16_t s16MovAvgBuffResetFlg);
static int16_t LAPCT_s16GetAbsFadeOutPressRef(int8_t s8PCtrlBoostModeOld, const stEstWheelPress_t* stEstWheelPress_r, int16_t s16AbsFadeOutPressRef);
static int32_t LAPCT_s32ClacAbsFadeOutStrkTot(int8_t s8PCtrlBoostModeOld, int32_t lapcts32AbsFadeOutStrkTot, int16_t s16TargetP, int32_t s32TatEstStrkPosiOfWhlP);
static int16_t LAPCT_s16GetABSFadeOutDelSMap(int32_t s32AbsFadeOutStrkTot, int16_t s16AbsFadeOutPressRef, int16_t s16TargetP, int16_t s16MaxStrokeMapSelFlg);
static uint16_t LAPCT_u16UpdateInABSFadeOutCnt(int32_t s32AbsFadeOutStrkTot, int16_t s16AbsFadeOutPressRef, int16_t s16TargetP, uint16_t u16InAbsFadeOutCnt);
static int32_t LAPCT_s32GetABSFadeOutStrkTot(int32_t s32AbsFadeOutStrkTot, int16_t s16AbsFadeOutPressRef, int16_t s16TargetP, int16_t s16DeltaStrokeMap, int16_t s16DeltaStrokeABSFadeOutComp);
static int16_t LAPCT_s16GetMovAvgSel(int8_t s8PCtrlBoostMode, int16_t s16MovingAvgSel, int16_t s16InitFastApplyFlg);
static int16_t LAPCT_s16GetMovAvgBuffResetFlg(int16_t s8PCtrlStateOld, int8_t s8PCtrlFadeoutStateOld, int8_t s8PCtrlState, int8_t s8PCtrlBoostMode, int16_t s16ResetWheelVolume);

/* Implementation*************************************************************/
/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16GetDeltaStrokeMap
* CALLED BY:            LAPCT_vCalcDeltaStroke
* Preconditions:        none
* PARAMETER:            s16CircuitP, s16IneffectiveStrokeCompFlg, s16LimDelStrkTargetMax, s16LimDelStrokeMin, s16TargetPressOld
* RETURN VALUE:         s16DeltaStrokeMap
* Description:          Return Final Delta Stroke Map
*   Metrics
*   211 (CountLine)
*    95 (CountLineCode)
*    72 (CountLineComment)
*     1 (CountLineInactive)
*     5 (Cyclomatic)
********************************************************************************/
int16_t LAPCT_s16GetDeltaStrokeMap(int16_t s16CircuitP, int16_t s16IneffectiveStrokeCompFlg, int16_t s16LimDelStrkTargetMax, int16_t s16LimDelStrokeMin, int16_t s16TargetPressOld)
{
	int16_t  s16DeltaStrokeTarget							= 0;
	int16_t  s16CircuitPressureFail							= 0;

	/*********************************************************************************************************/
	/* Local Variable for SWC Input Interface															Begin*/
	/*********************************************************************************************************/

	int16_t  s16TargetP										= lidss16TargetP;	/* Instead of TargetP */

	uint16_t u16WheelVolumeState							= lidsu16WheelVolumeState;
	int8_t   s8PCtrlState									= lidss8PCtrlState;
	int8_t   s8PCtrlBoostMode								= lidss8MtrPCtrlBoostMode;
	int8_t   s8PCtrlFadeoutState							= lidss8PCtrlFadeoutState;
	int16_t  s16PedalTravelRateMmPSec 						= lidss16PedalTravelRateMmPSec;
	int32_t  s32TatEstStrkPosiOfWhlP						= lidss32TatEstStrkPosiOfWhlP;

	int16_t s16InitFastApplyFlg								= lidss16InitFastApplyFlg;
	int16_t s16StandstillCtrl								= lidss16StandstillCtrl;

	int16_t s16EstWheelPressFL								= lidss16EstWheelPressFL;
	int16_t s16EstWheelPressFR								= lidss16EstWheelPressFR;
	int16_t s16EstWheelPressRL								= lidss16EstWheelPressRL;
	int16_t s16EstWheelPressRR								= lidss16EstWheelPressRR;

	uint8_t u8WhlCtrlIndex									= lidsu8WhlCtrlIndex;
	/*********************************************************************************************************/
	/* Local Variable for SWC Input Interface															End  */
	/*********************************************************************************************************/
	/*********************************************************************************************************/
	/* Local Variable for SWC Output Interface															Begin*/
	/*********************************************************************************************************/
	int16_t s16DeltaStrokeMap								= 0;
	int16_t s16IdenticalCircuitFlg 							= FALSE;
	int16_t s16ResetWheelVolume								= FALSE;

	/*********************************************************************************************************/
	/* Local Variable for SWC Output Interface															End  */
	/*********************************************************************************************************/

	/*********************************************************************************************************/
	/* Static Local Variable Define		Begin*/
	/*********************************************************************************************************/
	static uint16_t lapctu16InAbsFadeOutCnt					= 0;
	static int32_t lapcts32AbsFadeOutStrkTot				= 0;
	static int16_t lapcts16AbsFadeOutPressRef				= 0;
	static int8_t lapcts8PCtrlBoostModeOld					= S8_MTR_BOOST_MODE_NOT_CONTROL;
	static int16_t lapcts8PCtrlStateOld						= S8_PRESS_READY;
	static int8_t  lapcts8PCtrlFadeoutStateOld				= S8_PCTRL_NOT_FADEOUT_STATE;
	static uint8_t lapctu8WhlCtrlIndexOld					= 33;	/* 4wheel control :
																		sum of (FL(10), FR(20), RL(1), RR(2))   */

	/*********************************************************************************************************/
	/* Static Local Variable Define																		End  */
	/*********************************************************************************************************/

	/*********************************************************************************************************/
	/* Local Variable Define																			Begin*/
	/*********************************************************************************************************/
	/*int16_t s16DeltaStrokeCompIneff							= 0;*/
	int16_t s16IneffectiveStroke							= S16_INEFFECTIVE_STROKE;
	int16_t s16DeltaStrokeABSFadeOutComp					= 0;

	int16_t s16MovingAvgSel									= S16_MOVING_AVG_10;	/*	 Default Moving Average Size : 10*/
	int16_t s16MaxStrokeMapSelFlg							= FALSE;

	int16_t s16MovAvgBuffResetFlg							= FALSE;

	stEstWheelPress_t 				stEstWheelPress_r		= {0, 0, 0, 0};
	/*stINEFF_STROKE_CMP_R_t			stIneffStrokeComp_r		= {0, 0, 0, 0, 0, 0, 0, 0};*/
	/*********************************************************************************************************/
	/* Local Variable Define																			End  */
	/*********************************************************************************************************/

	/*********************************************************************************************************/
	/* Variable Initialize																				Begin*/
	/*********************************************************************************************************/
	stEstWheelPress_r.s16EstWheelPressFL = s16EstWheelPressFL;
	stEstWheelPress_r.s16EstWheelPressFR = s16EstWheelPressFR;
	stEstWheelPress_r.s16EstWheelPressRL = s16EstWheelPressRL;
	stEstWheelPress_r.s16EstWheelPressRR = s16EstWheelPressRR;

	if( (s8PCtrlBoostMode == S8_MTR_BOOST_MODE_QUICK) || (s8PCtrlBoostMode == S8_MTR_BOOST_MODE_SAFETY_HIGH_RESPONSE) || (s8PCtrlBoostMode == S8_MTR_BOOST_MODE_STOP) || (s8PCtrlBoostMode == S8_MTR_BOOST_MODE_PREBOOST))
	{
		s16MaxStrokeMapSelFlg = TRUE;
	}
	else {}
#if (M_LOGGER_VARIABLE_DEF == ENABLE)
	lapcts16DeltaStrokeMap    = 0;
	lapcts16DeltaStrokeMapAvg = 0;
#endif
	/*********************************************************************************************************/
	/* Variable Initialize																				End  */
	/*********************************************************************************************************/

	/*********************************************************************************************************/
	/* Pressure Control State																			Begin*/
	/* S8_PRESS_BOOST : Normal Boost + ABS																	 */
	/* S8_PCTRL_FADEOUT_STATE_1 : Pressure Error Compensation Only & Pressure reaches to 0bar				 */
	/*********************************************************************************************************/
	if((s8PCtrlState == S8_PRESS_PREBOOST) || (s8PCtrlState == S8_PRESS_BOOST) || ((s8PCtrlState == S8_PRESS_FADE_OUT) && (s8PCtrlFadeoutState == S8_PCTRL_FADEOUT_STATE_1)))
	{
		if(s8PCtrlBoostMode == S8_MTR_BOOST_MODE_TORQUE)		/* ABS Torque Control Mode */
		{
			lapctu16InAbsFadeOutCnt = S16_ABS_FADE_OUT_DAMPING_TIME;
		
		}
		else
		{
	/*********************************************************************************************************/
	/* Get Delta Stroke Map for each control case(ABS Fadeout, ESC/TCS, Normal							Begin*/
	/*********************************************************************************************************/
			if((s8PCtrlBoostMode == S8_MTR_BOOST_MODE_TORQUE_TO_NORMAL) && (lapctu16InAbsFadeOutCnt>0))		/* ABS Torque to Normal											*/
			{
				/********************************************************************************************************/
				/* ABS Fadeout control State																			*/
				/********************************************************************************************************/
				lapcts16AbsFadeOutPressRef = LAPCT_s16GetAbsFadeOutPressRef(lapcts8PCtrlBoostModeOld,  &stEstWheelPress_r, lapcts16AbsFadeOutPressRef);

				lapcts32AbsFadeOutStrkTot = LAPCT_s32ClacAbsFadeOutStrkTot(lapcts8PCtrlBoostModeOld, lapcts32AbsFadeOutStrkTot, s16TargetP, s32TatEstStrkPosiOfWhlP);

				s16DeltaStrokeMap = LAPCT_s16GetABSFadeOutDelSMap( lapcts32AbsFadeOutStrkTot, lapcts16AbsFadeOutPressRef, s16TargetP, s16MaxStrokeMapSelFlg);

				lapctu16InAbsFadeOutCnt = LAPCT_u16UpdateInABSFadeOutCnt(lapcts32AbsFadeOutStrkTot, lapcts16AbsFadeOutPressRef, s16TargetP, lapctu16InAbsFadeOutCnt);

				s16DeltaStrokeABSFadeOutComp = LAPCT_s16CalcDeltaStrokeMap(s16TargetP, s16TargetPressOld, u16WheelVolumeState, s16MaxStrokeMapSelFlg);

				lapcts32AbsFadeOutStrkTot = LAPCT_s32GetABSFadeOutStrkTot(lapcts32AbsFadeOutStrkTot, lapcts16AbsFadeOutPressRef, s16TargetP, s16DeltaStrokeMap, s16DeltaStrokeABSFadeOutComp);

				#if (M_LOGGER_VARIABLE_DEF == ENABLE)
				lapcts16TargetPressOldlog = s16TargetPressOld;
				#endif
			}
			else if(s8PCtrlBoostMode == S8_MTR_BOOST_MODE_ABS)
			{
				s16DeltaStrokeMap = LAPCT_s16CalcDeltaStrokeMap(s16TargetP, s16TargetPressOld, u16WheelVolumeState, s16MaxStrokeMapSelFlg);

				#if (M_LOGGER_VARIABLE_DEF == ENABLE)
				lapcts16TargetPressOldlog = s16TargetPressOld;
				#endif
			}
			else if(s8PCtrlBoostMode == S8_MTR_BOOST_MODE_SAFETY_HIGH_RESPONSE)		/* ESC oversteer (Include ROP), ABS, TCS_1_mode : 300bar/s 		*/
			{
				/********************************************************************************************************/
				/* ESC/TCS control state																				*/
				/********************************************************************************************************/
				s16IdenticalCircuitFlg = (u8WhlCtrlIndex == lapctu8WhlCtrlIndexOld) ? FALSE : TRUE;
				s16DeltaStrokeMap = LAPCT_s16CalcDeltaStrokeMapESC(s16TargetP, s16TargetPressOld, s16CircuitP, u16WheelVolumeState, s16IdenticalCircuitFlg);
			}
			else
			{
				/********************************************************************************************************/
				/* Normal control state																					*/
				/********************************************************************************************************/
				s16DeltaStrokeMap = LAPCT_s16CalcDeltaStrokeMap(s16TargetP, s16TargetPressOld, u16WheelVolumeState, s16MaxStrokeMapSelFlg);

			  #if (M_LOGGER_VARIABLE_DEF == ENABLE)
				lapcts16TargetPressOldlog = s16TargetPressOld;
			  #endif
			}
		}

		  #if (M_LOGGER_VARIABLE_DEF == ENABLE)
			lapcts16DeltaStrokeMap         = s16DeltaStrokeMap;
		  #endif
	/*********************************************************************************************************/
	/* Delta Stroke Map Moving Average Process															Begin*/
	/*********************************************************************************************************/
			/* Check: Moving Average Update Logic Change and Verify */
			#if M_MOVING_AVG_TARGET_STROKE == ENABLE
			s16ResetWheelVolume = (lapctu8WhlCtrlIndexOld == u8WhlCtrlIndex) ? FALSE : TRUE;
			s16MovingAvgSel = LAPCT_s16GetMovAvgSel(s8PCtrlBoostMode, s16MovingAvgSel, s16InitFastApplyFlg);
			s16MovAvgBuffResetFlg = LAPCT_s16GetMovAvgBuffResetFlg(lapcts8PCtrlStateOld, lapcts8PCtrlFadeoutStateOld, s8PCtrlState, s8PCtrlBoostMode, s16ResetWheelVolume);
			lapcts16MovAvgBuffResetFlg = s16MovAvgBuffResetFlg;

			s16DeltaStrokeMap = LAPCT_s16MovAvgTargetStrokeAll(s16DeltaStrokeMap, s16MovAvgBuffResetFlg, s16CircuitP, s16MovingAvgSel);

			#endif /* M_MOVING_AVG_TARGET_STROKE == ENABLE */

	/*********************************************************************************************************/
	/* Delta Stroke Map Moving Average Process															End  */
	/*********************************************************************************************************/
	/*********************************************************************************************************/
	/* Get Final Delta Stroke Map with Limit															Begin*/
	/*********************************************************************************************************/
			s16DeltaStrokeMap = LAPCT_s16GetLimitedDelStroke(s16DeltaStrokeMap, s16LimDelStrkTargetMax, s16LimDelStrokeMin, s16MovAvgBuffResetFlg);
#if (M_LOGGER_VARIABLE_DEF == ENABLE)
	lapcts16DeltaStrokeMapAvg = s16DeltaStrokeMap;
#endif
	/*********************************************************************************************************/
	/* Get Final Delta Stroke Map with Limit															End  */
	/*********************************************************************************************************/
	}
	/*********************************************************************************************************/
	/* Get Delta Stroke Map for each control case(ABS Fadeout, ESC/TCS, Normal							End  */
	/*********************************************************************************************************/

	lapcts8PCtrlStateOld = s8PCtrlState;
	lapcts8PCtrlFadeoutStateOld = s8PCtrlFadeoutState;
	lapcts8PCtrlBoostModeOld = s8PCtrlBoostMode;
	lapctu8WhlCtrlIndexOld = u8WhlCtrlIndex;

	return s16DeltaStrokeMap;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16CalcDeltaStrokeMap
* CALLED BY:            LAPCT_s16GetABSFadeOutDelSMap, LAPCT_s16GetDeltaStrokeMap
* Preconditions:        none
* PARAMETER:            s16TargetP, s16TargetPressOld, u16WheelVolumeState, s16MaxStrokeMapSelFlg
* RETURN VALUE:         s16DeltaStroke
* Description:          Get Delta Stroke From Absolute Stroke Map
*   Metrics
*    30 (CountLine)
*    23 (CountLineCode)
*     0 (CountLineComment)
*     1 (CountLineInactive)
*     3 (Cyclomatic)
********************************************************************************/
static int16_t LAPCT_s16CalcDeltaStrokeMap(int16_t s16TargetP, int16_t s16TargetPressOld, uint16_t u16WheelVolumeState, int16_t s16MaxStrokeMapSelFlg)
{
	int16_t s16DeltaStroke = 0;
	int16_t s16MapGain = S16_REDUCE_GAIN_OF_APPLY_MAP;
#if (WHEEL_DEPENDENT_MAP == DISABLE)
	s16DeltaStroke = LAPCT_s16CalcStorkeByPress(s16TargetP, u16WheelVolumeState) - LAPCT_s16CalcStorkeByPress(s16TargetPressOld, u16WheelVolumeState);
#else
	s16DeltaStroke = (int16_t)L_s32LimitMinMax((LAPCT_s32CalcStorkeByPress(s16TargetP, u16WheelVolumeState) - LAPCT_s32CalcStorkeByPress(s16TargetPressOld, u16WheelVolumeState)), ASW_S16_MIN, ASW_S16_MAX);
#endif

	if (s16MaxStrokeMapSelFlg == TRUE)
	{
		s16MapGain = S16_GAIN_OF_APPLY_MAP_INIT_FAST;
	}
	else
	{
		if(s16DeltaStroke < 0)
		{
			s16MapGain = S16_REDUCE_GAIN_OF_RELEASE_MAP;
		}
		else
		{
			s16MapGain = S16_REDUCE_GAIN_OF_APPLY_MAP;
		}
	}

	s16DeltaStroke = (int16_t)(((int32_t)s16DeltaStroke * s16MapGain) / 10);

	return s16DeltaStroke;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16CalcDeltaStrokeMapESC
* CALLED BY:            LAPCT_s16GetDeltaStrokeMap
* Preconditions:        none
* PARAMETER:            s16TargetP, s16TargetPressOld, s16CircuitP, u16WheelVolumeState, s16IdenticalCircuitFlg
* RETURN VALUE:         s16DeltaStroke
* Description:          Calculate Delta Stroke Map for ESC/TCS
*   Metrics
*    30 (CountLine)
*    23 (CountLineCode)
*     0 (CountLineComment)
*     1 (CountLineInactive)
*     3 (Cyclomatic)
********************************************************************************/
static int16_t LAPCT_s16CalcDeltaStrokeMapESC(int16_t s16TargetP, int16_t s16TargetPressOld, int16_t s16CircuitP, uint16_t u16WheelVolumeState, int16_t s16IdenticalCircuitFlg)
{
	int16_t s16DeltaStroke = 0;
	lapctu1DelStrkPErrorCompLimited = 0;
	/* For Considering volume-pressure relation, refer present wheel pressure(estimated), only with ESC or TCS activated */
	if(s16TargetPressOld > S16_PCTRL_PRESS_0_BAR)
	{
		s16TargetP = (s16TargetP - s16TargetPressOld) + s16CircuitP;
		s16TargetPressOld = s16CircuitP;
		lapctu1DelStrkPErrorCompLimited = 1;
	}
	else { /* Do Nothing */ }
  #if (M_LOGGER_VARIABLE_DEF == ENABLE)
	lapcts16TargetPressOldlog = s16TargetPressOld;
  #endif

	#if (WHEEL_DEPENDENT_MAP == DISABLE)
	s16DeltaStroke = LAPCT_s16CalcStorkeByPress(s16TargetP, u16WheelVolumeState) - LAPCT_s16CalcStorkeByPress(s16TargetPressOld, u16WheelVolumeState);
#else
	s16DeltaStroke = (int16_t)L_s32LimitMinMax((LAPCT_s32CalcStorkeByPress(s16TargetP, u16WheelVolumeState) - LAPCT_s32CalcStorkeByPress(s16TargetPressOld, u16WheelVolumeState)), ASW_S16_MIN, ASW_S16_MAX);
#endif

	if(s16TargetPressOld == S16_PCTRL_PRESS_0_BAR)
	{
		if(s16IdenticalCircuitFlg==TRUE)
		{
			s16DeltaStroke = s16DeltaStroke / 2;
		}
		else { /* Do Nothing */ }
	}
	else { /* Do Nothing */ }

	s16DeltaStroke = (int16_t)((((int32_t)s16DeltaStroke) * 9)/10);

	return s16DeltaStroke;
}

#if M_MOVING_AVG_TARGET_STROKE == ENABLE

static int16_t LAPCT_s16MovAvgTargetStrokeAll(int16_t s16DelStrokeTarget, int16_t s16ResetStaticVarFlg, int16_t s16CircuitP, int16_t s16MovingAvgSize)
{
        static int16_t lapcts16MovingCount = 0;
        static int32_t lapcts32StrokeSum = 0;
        static int32_t lapcts32TargetStrokeAry[S16_TARGET_STRK_MOVING_AVG_ARR_10] = {0,0,0,0,0,0,0,0,0,0};
        static int32_t lapcts32RemainStroke = 0;
        static int16_t lapcts16MovingAvgSizeOld = S16_TARGET_STRK_MOVING_AVG_ARR_10;

        int16_t s16InitInEffStrkComp = 0;
        int16_t s16MovingAvgTargetStroke = 0;

        s16MovingAvgSize = L_s16LimitMinMax(s16MovingAvgSize, S16_TARGET_STRK_MOVING_AVG_ARR_1, S16_TARGET_STRK_MOVING_AVG_ARR_10);

        if(s16ResetStaticVarFlg==1)
        {
               lapcts16MovingCount = 0;
               lapcts32StrokeSum = 0;
               lapcts32RemainStroke = 0;
               lapcts32TargetStrokeAry[0] = 0;
               lapcts32TargetStrokeAry[1] = 0;
               lapcts32TargetStrokeAry[2] = 0;
               lapcts32TargetStrokeAry[3] = 0;
               lapcts32TargetStrokeAry[4] = 0;
               lapcts32TargetStrokeAry[5] = 0;
               lapcts32TargetStrokeAry[6] = 0;
               lapcts32TargetStrokeAry[7] = 0;
               lapcts32TargetStrokeAry[8] = 0;
               lapcts32TargetStrokeAry[9] = 0;
        }
        else { /* Do Nothing */ }

        if (lapcts16MovingAvgSizeOld != s16MovingAvgSize)
        {
               lapcts16MovingCount = 1;
               lapcts32StrokeSum = lapcts32RemainStroke;
               lapcts32TargetStrokeAry[0] = lapcts32RemainStroke;
               lapcts32TargetStrokeAry[1] = 0;
               lapcts32TargetStrokeAry[2] = 0;
               lapcts32TargetStrokeAry[3] = 0;
               lapcts32TargetStrokeAry[4] = 0;
               lapcts32TargetStrokeAry[5] = 0;
               lapcts32TargetStrokeAry[6] = 0;
               lapcts32TargetStrokeAry[7] = 0;
               lapcts32TargetStrokeAry[8] = 0;
               lapcts32TargetStrokeAry[9] = 0;
        }
        else { /* Do Nothing */ }

        if (s16MovingAvgSize > S16_TARGET_STRK_MOVING_AVG_ARR_1)
        {
               lapcts32StrokeSum = (lapcts32StrokeSum - lapcts32TargetStrokeAry[lapcts16MovingCount])+ s16DelStrokeTarget ;
               s16MovingAvgTargetStroke = (int16_t)(lapcts32StrokeSum / s16MovingAvgSize);
               lapcts32RemainStroke = (lapcts32RemainStroke + s16DelStrokeTarget) - s16MovingAvgTargetStroke;

               lapcts32TargetStrokeAry[lapcts16MovingCount] = s16DelStrokeTarget;
               lapcts16MovingCount = (lapcts16MovingCount + 1) % s16MovingAvgSize;
        }
        else
        {
               s16MovingAvgTargetStroke = s16DelStrokeTarget + lapcts32RemainStroke;
               lapcts32RemainStroke = 0;
        }

        lapcts16MovingAvgSizeOld = s16MovingAvgSize;

        return s16MovingAvgTargetStroke;
}
#endif /* #if M_MOVING_AVG_TARGET_STROKE == ENABLE */

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16GetLimitedDelStroke
* CALLED BY:            LAPCT_vCalcDeltaStroke()
* Preconditions:
* PARAMETER:            s16DeltaStroke
* RETURN VALUE:         s16DeltaStroke
* Description:          Limit Delta Stroke regarding motor maximum rpm
********************************************************************************/
static int16_t LAPCT_s16GetLimitedDelStroke(int16_t s16DeltaStroke, int16_t s16LimDelStrkTargetMax, int16_t s16LimDelStrokeMin, int16_t s16MovAvgBuffResetFlg)
{
	static int32_t lapcts32DelStrokeSum = 0;

	if(s16MovAvgBuffResetFlg==TRUE)
	{
		lapcts32DelStrokeSum = s16DeltaStroke;
	}
	else
	{
		lapcts32DelStrokeSum = lapcts32DelStrokeSum + (int32_t)s16DeltaStroke;
	}

/*	s16DeltaStroke = (int16_t) L_s32LimitMinMax(lapcts32DelStrokeSum, S16_LIM_DEL_STRK_FOR_MOTOR_MIN, S16_LIM_DEL_STRK_FOR_MOTOR_MAX);*/
	/*TODO: Disable delta stroke limit for quick brake test, so that it might cause a big overshoot of pressure if ABS is not engaged */
	s16DeltaStroke = (int16_t) L_s32LimitMinMax(lapcts32DelStrokeSum, s16LimDelStrokeMin, s16LimDelStrkTargetMax);

	lapcts32DelStrokeSum = lapcts32DelStrokeSum - (int32_t)s16DeltaStroke;

	return s16DeltaStroke;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16GetAbsFadeOutPressRef
* CALLED BY:            LAPCT_s16GetDeltaStrokeMap()
* Preconditions:
* PARAMETER:            s8PCtrlBoostModeOld, stEstWheelPress_r, s16AbsFadeOutPressRef
* RETURN VALUE:         s16AbsFadeOutPressRef
* Description:          Get ABS Fadeout Pressure Reference
********************************************************************************/
static int16_t LAPCT_s16GetAbsFadeOutPressRef(int8_t s8PCtrlBoostModeOld, const stEstWheelPress_t* stEstWheelPress_r, int16_t s16AbsFadeOutPressRef)
{
	/* ABS Fade-out control State */
	if (s8PCtrlBoostModeOld == S8_MTR_BOOST_MODE_TORQUE_TO_NORMAL)		/* ABS Torque to Normal											*/
	{
		s16AbsFadeOutPressRef = s16AbsFadeOutPressRef + S16_ABS_FADE_OUT_RISE_RATE;
	}
	else
	{
		s16AbsFadeOutPressRef = (stEstWheelPress_r->s16EstWheelPressFL + stEstWheelPress_r->s16EstWheelPressFR + stEstWheelPress_r->s16EstWheelPressRL + stEstWheelPress_r->s16EstWheelPressRR) / 4;
	}

	return s16AbsFadeOutPressRef;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s32ClacAbsFadeOutStrkTot
* CALLED BY:            LAPCT_s16GetDeltaStrokeMap()
* Preconditions:
* PARAMETER:            s8PCtrlBoostModeOld, lapcts32AbsFadeOutStrkTot, s16TargetP, s32TatEstStrkPosiOfWhlP
* RETURN VALUE:         lapcts32AbsFadeOutStrkTot
* Description:          Get ABS Fadeout Total Stroke
********************************************************************************/
static int32_t LAPCT_s32ClacAbsFadeOutStrkTot(int8_t s8PCtrlBoostModeOld, int32_t lapcts32AbsFadeOutStrkTot, int16_t s16TargetP, int32_t s32TatEstStrkPosiOfWhlP)
{
	if (s8PCtrlBoostModeOld != S8_MTR_BOOST_MODE_TORQUE_TO_NORMAL)		/* S8_MTR_BOOST_MODE_TORQUE_TO_NORMAL: ABS Torque to Normal */
	{
		lapcts32AbsFadeOutStrkTot = LAPCT_s32CalcStorkeByPress(s16TargetP, U16_SEL_4WHEEL) - s32TatEstStrkPosiOfWhlP;
	}
	else { /* Maintain lapcts32AbsFadeOutStrkTot */ }

	return lapcts32AbsFadeOutStrkTot;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16GetABSFadeOutDelSMap
* CALLED BY:            LAPCT_s16GetDeltaStrokeMap()
* Preconditions:
* PARAMETER:            lapcts32AbsFadeOutStrkTot, lapcts16AbsFadeOutPressRef, s16TargetP, s16MaxStrokeMapSelFlg
* RETURN VALUE:         s16DeltaStrokeMap
* Description:          Get ABS Fadeout Delta Stroke Map
********************************************************************************/
static int16_t LAPCT_s16GetABSFadeOutDelSMap(int32_t s32AbsFadeOutStrkTot, int16_t s16AbsFadeOutPressRef, int16_t s16TargetP, int16_t s16MaxStrokeMapSelFlg)
{
	int16_t s16DeltaStrokeMap = 0;

	if ((s32AbsFadeOutStrkTot > 100) && (s16AbsFadeOutPressRef < s16TargetP))
	{
		s16DeltaStrokeMap = LAPCT_s16CalcDeltaStrokeMap( (s16AbsFadeOutPressRef + S16_ABS_FADE_OUT_RISE_RATE), s16AbsFadeOutPressRef, U16_SEL_4WHEEL, s16MaxStrokeMapSelFlg);

  #if (M_LOGGER_VARIABLE_DEF == ENABLE)
		lapcts16TargetPressOldlog = s16AbsFadeOutPressRef;
  #endif
	}
	else { /* Do Nothing */ }

	return s16DeltaStrokeMap;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_u16UpdateInABSFadeOutCnt
* CALLED BY:            LAPCT_s16GetDeltaStrokeMap()
* Preconditions:
* PARAMETER:            s32AbsFadeOutStrkTot, s16AbsFadeOutPressRef, s16TargetP, u16InAbsFadeOutCnt
* RETURN VALUE:         u16InAbsFadeOutCnt
* Description:          InABS FadeOout Counter update
********************************************************************************/
static uint16_t LAPCT_u16UpdateInABSFadeOutCnt(int32_t s32AbsFadeOutStrkTot, int16_t s16AbsFadeOutPressRef, int16_t s16TargetP, uint16_t u16InAbsFadeOutCnt)
{
	if ((s32AbsFadeOutStrkTot <= 100) || (s16AbsFadeOutPressRef >= s16TargetP))
	{
		u16InAbsFadeOutCnt = u16InAbsFadeOutCnt - 1;
	}
	else { /* Maintain lapctu16InAbsFadeOutCnt */ }

	return u16InAbsFadeOutCnt;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s32GetABSFadeOutStrkTot
* CALLED BY:            LAPCT_s16GetDeltaStrokeMap()
* Preconditions:
* PARAMETER:            s32AbsFadeOutStrkTot, s16AbsFadeOutPressRef, s16TargetP, s16DeltaStrokeMap, s16DeltaStrokeABSFadeOutComp
* RETURN VALUE:         s32AbsFadeOutStrkTot
* Description:          Get ABS FadeOut Total Stroke
********************************************************************************/
static int32_t LAPCT_s32GetABSFadeOutStrkTot(int32_t s32AbsFadeOutStrkTot, int16_t s16AbsFadeOutPressRef, int16_t s16TargetP, int16_t s16DeltaStrokeMap, int16_t s16DeltaStrokeABSFadeOutComp)
{
	if ((s32AbsFadeOutStrkTot > 100) && (s16AbsFadeOutPressRef < s16TargetP))
	{
		s32AbsFadeOutStrkTot = s32AbsFadeOutStrkTot - ((int32_t) s16DeltaStrokeMap - s16DeltaStrokeABSFadeOutComp);
	}
	else { /* Do Nothing */ }

	return s32AbsFadeOutStrkTot;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16GetMovAvgSel
* CALLED BY:            LAPCT_s16GetDeltaStrokeMap()
* Preconditions:
* PARAMETER:            s16SafetyControlActFlg, s16MovingAvgSel, s16InitFastApplyFlg
* RETURN VALUE:         s16MovingAvgSel
* Description:          Get Moving Average Index depend on Pressure Control Mode
********************************************************************************/
static int16_t LAPCT_s16GetMovAvgSel(int8_t s8PCtrlBoostMode, int16_t s16MovingAvgSel, int16_t s16InitFastApplyFlg)
{
	if (s8PCtrlBoostMode == S8_MTR_BOOST_MODE_PREBOOST)			/* PreBoost Mode												*/
	{
		s16MovingAvgSel = S16_MOVING_AVG_2;
	}
	else if (s8PCtrlBoostMode == S8_MTR_BOOST_MODE_ABS)
	{
		s16MovingAvgSel = S16_MOVING_AVG_1;
	}
	else if (s8PCtrlBoostMode == S8_MTR_BOOST_MODE_SAFETY_HIGH_RESPONSE)	/* ESC oversteer (Include ROP), ABS, TCS_1_mode : 300bar/s 		*/
	{
		s16MovingAvgSel = S16_MOVING_AVG_2;
	}
	else if (s8PCtrlBoostMode == S8_MTR_BOOST_MODE_QUICK)		/* Quick Braking , AEB : 400bar/s 								*/
	{
		s16MovingAvgSel = S16_MOVING_AVG_5;
	}
	else
	{
		s16MovingAvgSel = S16_MOVING_AVG_10;
	}

	return s16MovingAvgSel;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16GetMovAvgBuffResetFlg
* CALLED BY:            LAPCT_s16GetDeltaStrokeMap()
* Preconditions:
* PARAMETER:            s8PCtrlStateOld, s8PCtrlFadeoutStateOld, s8PCtrlState, s8PCtrlBoostMode, s16ResetWheelVolume
* RETURN VALUE:         s16MovAvgBuffResetFlg
* Description:          Get Moving Average Buffer Reset Flag depend on Pressure Control state change
********************************************************************************/
static int16_t LAPCT_s16GetMovAvgBuffResetFlg(int16_t s8PCtrlStateOld, int8_t s8PCtrlFadeoutStateOld, int8_t s8PCtrlState, int8_t s8PCtrlBoostMode, int16_t s16ResetWheelVolume)
{
	int16_t s16MovAvgBuffResetFlg = MOVING_AVG_MAINTAIN;
	static int8_t s8PCtrlBoostModeOld = S8_MTR_BOOST_MODE_NOT_CONTROL;

	if (s8PCtrlState == S8_PRESS_BOOST)
	{
		if (s8PCtrlStateOld == S8_PRESS_READY)
		{
			s16MovAvgBuffResetFlg = MOVING_AVG_RESET_ACT;
		}
		else if ((s8PCtrlStateOld == S8_PRESS_FADE_OUT) && (s8PCtrlFadeoutStateOld != S8_PCTRL_FADEOUT_STATE_1))
		{
			s16MovAvgBuffResetFlg = MOVING_AVG_RESET_ACT;
		}
		else if (s8PCtrlBoostMode == S8_MTR_BOOST_MODE_SAFETY_HIGH_RESPONSE)		/* ESC oversteer (Include ROP), ABS, TCS_1_mode : 300bar/s 		*/
		{
			if (s16ResetWheelVolume == TRUE)
			{
				s16MovAvgBuffResetFlg = MOVING_AVG_RESET_ACT;
			}
			else { /* Do Nothing */ }
		}
		else { /* Do Nothing */ }
	}
	else if ((s8PCtrlState == S8_PRESS_FADE_OUT) && (s8PCtrlStateOld == S8_PRESS_BOOST))
	{
		s16MovAvgBuffResetFlg = MOVING_AVG_RESET_ACT;
	}
	else { /* Do Nothing */ }

	s8PCtrlBoostModeOld = s8PCtrlBoostMode;

	return s16MovAvgBuffResetFlg;
}
