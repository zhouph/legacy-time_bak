/*******************************************************************************
* Project Name:     IDB
* File Name:        LAPCT_DeltaStrokePRateError.c
* Description:
* Logic version:
********************************************************************************
*  Modification Log
*  Date         Author          Description
*  -----        -----           -----
********************************************************************************/

/* Includes ******************************************************************/
#include "Pct_5msCtrl.h"
#include "LAPCT_DeltaStrokePRateError.h"

#include "LAPCT_DecideMtrCtrlMode.h"
#include "LAPCT_LoadMapData.h"
#include "LAPCT_DeltaStrokeInterface.h"

/* Logical Definition  *********************************************************/
#define S16_RATE_ERR_CMP_GAIN_RESOL						(100)		/* Resolution : 1% */
#define S16_P_RATE_ERROR_COMP_THD 						(S16_PCTRL_PRESS_0_5_BAR) /* 100bar/sec = 0.5bar/scan */

/* Logical Type Definition  ****************************************************/
typedef struct {
	int16_t 	s16CircuitPressRate;
	int16_t 	s16TargetPressRate;
	int16_t 	s16PressError;
	int16_t 	s16TargetP;
	int16_t 	s16CircuitPress;
	uint16_t 	u16WheelVolumeState;
	int16_t 	s16RateError;
}stPRESS_ERROR_RATE_CMP_R_t;

typedef struct {
	int16_t 	s16PRateErrorCompMax;
	int16_t 	s16PRateErrorCompMin;
}stP_RATE_ERROR_COMP_LIMIT_t;


#if (M_LOGGER_VARIABLE_DEF == ENABLE)
extern int16_t lapcts16RateCompThres1;
extern int16_t lapcts16DeltaStrokePrateError;
extern int16_t lapcts16DeltaStrokeRateErrorRaw;
extern int16_t lapcts16DeltaStrokeRateCompGain;
extern int16_t lapcts16PressPointTargetRate;
extern int16_t lapcts16PressPointPRate;
#endif

/* GlobalFunction prototype **************************************************/

/* LocalFunction prototype ***************************************************/
static int16_t LAPCT_s16CompPressRateErr(const stPRESS_ERROR_RATE_CMP_R_t stPressErrorRateComp_r);
static int16_t LAPCT_s16GetRateErrorCompGain(const stPRESS_ERROR_RATE_CMP_R_t stPressErrorRateComp_r);
static stP_RATE_ERROR_COMP_LIMIT_t LAPCT_s16DecidePRateErrorCompLimit(int16_t  s16CircuitPress, uint16_t u16WheelVolumeState);

/* Implementation*************************************************************/

int16_t LAPCT_s16GetDeltaStrkPRateError(int16_t s16CircuitP, int16_t s16CircuitPRate10ms, int16_t s16PressError)
{
	int8_t   s8PCtrlState									= lidss8PCtrlState;
	int8_t   s8PCtrlBoostMode								= lidss8MtrPCtrlBoostMode;
	int8_t   s8PCtrlFadeoutState							= lidss8PCtrlFadeoutState;
	int16_t  s16TargetP										= lidss16TargetP;
	int16_t  s16TargetPRate10ms								= lidss16TargetPRate10ms;
	uint16_t u16WheelVolumeState							= lidsu16WheelVolumeState;

	int16_t s16DeltaStrokePRateError						= S16_PCTRL_PRESS_0_BAR;

	stPRESS_ERROR_RATE_CMP_R_t		stPressErrorRateComp_r	= {0, 0, 0, 0, 0, 0, 0};
	stP_RATE_ERROR_COMP_LIMIT_t 	stPRateErrorCompLimit 	= {0,0};
	if((s8PCtrlState == S8_PRESS_BOOST) || ((s8PCtrlState == S8_PRESS_FADE_OUT) && (s8PCtrlFadeoutState == S8_PCTRL_FADEOUT_STATE_1)))
	{
		if((s8PCtrlBoostMode != S8_MTR_BOOST_MODE_TORQUE) && (s8PCtrlBoostMode != S8_MTR_BOOST_MODE_NOT_CONTROL) && (s8PCtrlBoostMode != S8_MTR_BOOST_MODE_ABS))	/* S8_MTR_BOOST_MODE_TORQUE : ABS Torque Control Mode */
		{

			stPRateErrorCompLimit = LAPCT_s16DecidePRateErrorCompLimit(s16CircuitP, u16WheelVolumeState);
	/*********************************************************************************************************/
	/* Calculate Pressure Rate Error Compensation Delta Stroke											Begin*/
	/*********************************************************************************************************/
			/* Pressure Rate Error Compensation - Begin */
			stPressErrorRateComp_r.s16CircuitPress			= s16CircuitP;
			stPressErrorRateComp_r.s16CircuitPressRate		= s16CircuitPRate10ms;
			stPressErrorRateComp_r.s16TargetP				= s16TargetP;
			stPressErrorRateComp_r.s16TargetPressRate		= s16TargetPRate10ms;
			stPressErrorRateComp_r.s16PressError			= s16PressError;
			stPressErrorRateComp_r.u16WheelVolumeState		= u16WheelVolumeState;
			stPressErrorRateComp_r.s16RateError				= s16TargetPRate10ms - s16CircuitPRate10ms;

			s16DeltaStrokePRateError = LAPCT_s16CompPressRateErr(stPressErrorRateComp_r);

			s16DeltaStrokePRateError = L_s16LimitMinMax(s16DeltaStrokePRateError, stPRateErrorCompLimit.s16PRateErrorCompMin, stPRateErrorCompLimit.s16PRateErrorCompMax);

	/*********************************************************************************************************/
	/* Calculate Pressure Rate Error Compensation Delta Stroke											End  */
	/*********************************************************************************************************/
		}
		else { /* Don't Compensate Pressure Rate Error */ }
	}
	else { /* Don't Compensate Pressure Rate Error */ }

	return s16DeltaStrokePRateError;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16CompPressRateErr
* CALLED BY:            LAPCT_vCalcDeltaStroke()
* Preconditions:        none
* PARAMETER:            stPressErrorRateComp_r
* RETURN VALUE:         s16DeltaStrokeRateErrorComp
* Description:          Delta Stroke Target Compensation for Pressure Rate Error
********************************************************************************/
static int16_t LAPCT_s16CompPressRateErr(const stPRESS_ERROR_RATE_CMP_R_t stPressErrorRateComp_r)
{
	int16_t s16DeltaStrokeRateErrorComp = 0;
	int16_t s16PressPointTargetRate     = 0;
	int16_t s16PressPointPRate          = 0;
	int16_t s16DeltaStrokeRateErrorRaw  = 0;
	int16_t s16RateCompGain             = S16_RATE_CMP_A_GAIN_BY_P_ERR_0(0);
	int16_t s16RateCompThres            = 0;
	static uint8_t lapctu8RateCompFlg   = 0;

#if (M_LOGGER_VARIABLE_DEF == ENABLE)
	int16_t s16DeltaStrokeRawSum = 0;
/*	lapctu1DelStrkPrateErrorCompFlg1 = FALSE;
	lapctu1DelStrkPrateErrorCompFlg2 = FALSE;*/
#endif

	/* Decide Engage & Disengage threshold */
	if((stPressErrorRateComp_r.s16TargetPressRate) > (stPressErrorRateComp_r.s16CircuitPressRate))
	{
		s16RateCompThres = L_s16IInter4Point( (-stPressErrorRateComp_r.s16PressError),
											  S16_RATE_CMP_A_THD_P_ERR_LVL(0), S16_RATE_CMP_A_THD_BY_P_ERR(0),
											  S16_RATE_CMP_A_THD_P_ERR_LVL(1), S16_RATE_CMP_A_THD_BY_P_ERR(1),
											  S16_RATE_CMP_A_THD_P_ERR_LVL(2), S16_RATE_CMP_A_THD_BY_P_ERR(2),
											  S16_RATE_CMP_A_THD_P_ERR_LVL(3), S16_RATE_CMP_A_THD_BY_P_ERR(3));
#if (M_LOGGER_VARIABLE_DEF == ENABLE)
		lapcts16RateCompThres1 = (stPressErrorRateComp_r.s16CircuitPressRate) + s16RateCompThres;
#endif
	}
	else
	{
		s16RateCompThres = L_s16IInter4Point( (stPressErrorRateComp_r.s16PressError),
											  S16_RATE_CMP_R_THD_P_ERR_LVL(0), S16_RATE_CMP_R_THD_BY_P_ERR(0),
											  S16_RATE_CMP_R_THD_P_ERR_LVL(1), S16_RATE_CMP_R_THD_BY_P_ERR(1),
											  S16_RATE_CMP_R_THD_P_ERR_LVL(2), S16_RATE_CMP_R_THD_BY_P_ERR(2),
											  S16_RATE_CMP_R_THD_P_ERR_LVL(3), S16_RATE_CMP_R_THD_BY_P_ERR(3));
#if (M_LOGGER_VARIABLE_DEF == ENABLE)
		lapcts16RateCompThres1 = (stPressErrorRateComp_r.s16CircuitPressRate) - s16RateCompThres;
#endif
	}

	/* Detect if rate error comp is needed or not */
	if(lapctu8RateCompFlg==0)
	{
		if((stPressErrorRateComp_r.s16TargetPressRate) > ((stPressErrorRateComp_r.s16CircuitPressRate) + s16RateCompThres))
		{
			lapctu8RateCompFlg= 1;
			lapctu1DelStrkPrateErrorCompFlg1 = 1;
		}
		else if((stPressErrorRateComp_r.s16CircuitPressRate) < ((stPressErrorRateComp_r.s16CircuitPressRate) - s16RateCompThres))
		{
			lapctu8RateCompFlg= 1;
			lapctu1DelStrkPrateErrorCompFlg2 = 1;
		}
		else {/* DO NOT Compensate for Rate error */}
	}
	else
	{
		/* Hysteresis: Seperate disengage thres from engage thres */
		s16RateCompThres = s16RateCompThres / 2;
		if( ((stPressErrorRateComp_r.s16TargetPressRate) < ((stPressErrorRateComp_r.s16CircuitPressRate) + s16RateCompThres))
			&& ((stPressErrorRateComp_r.s16TargetPressRate) > ((stPressErrorRateComp_r.s16CircuitPressRate) - s16RateCompThres)) )
		{
			lapctu8RateCompFlg= 0;
			lapctu1DelStrkPrateErrorCompFlg1 = 0;
			lapctu1DelStrkPrateErrorCompFlg2 = 0;
		}
		else{/* Maintain Compensation for Rate error */	}
	}

	if(lapctu8RateCompFlg == 1)
	{
		/* Convert rate error to pressure error EXPECTED */
		s16PressPointTargetRate = stPressErrorRateComp_r.s16CircuitPress + ((stPressErrorRateComp_r.s16TargetPressRate)/2); /* Divided by 2 means: CONVERT_RATE_TO_PRESS_1_100BAR */
		s16PressPointTargetRate = (s16PressPointTargetRate > 0)? s16PressPointTargetRate : S16_PCTRL_PRESS_0_BAR;
		s16PressPointPRate 		= stPressErrorRateComp_r.s16CircuitPress + ((stPressErrorRateComp_r.s16CircuitPressRate)/2); /* Divided by 2 means: CONVERT_RATE_TO_PRESS_1_100BAR */
		s16PressPointPRate = (s16PressPointPRate > 0)? s16PressPointPRate : S16_PCTRL_PRESS_0_BAR;

		s16RateCompGain = LAPCT_s16GetRateErrorCompGain(stPressErrorRateComp_r);
#if (WHEEL_DEPENDENT_MAP == DISABLE)
		s16DeltaStrokeRateErrorRaw = LAPCT_s16CalcStorkeByPress(s16PressPointTargetRate, stPressErrorRateComp_r.u16WheelVolumeState) - LAPCT_s16CalcStorkeByPress(s16PressPointPRate, stPressErrorRateComp_r.u16WheelVolumeState);
#else
		s16DeltaStrokeRateErrorRaw = (int16_t)L_s32LimitMinMax(LAPCT_s32CalcStorkeByPress(s16PressPointTargetRate, stPressErrorRateComp_r.u16WheelVolumeState) - LAPCT_s32CalcStorkeByPress(s16PressPointPRate, stPressErrorRateComp_r.u16WheelVolumeState), ASW_S16_MIN, ASW_S16_MAX);
#endif
		s16DeltaStrokeRateErrorComp = (int16_t)(((int32_t)s16RateCompGain * (s16DeltaStrokeRateErrorRaw)) / S16_RATE_ERR_CMP_GAIN_RESOL);
	}
	else{ /* DO NOT Compensate for Rate error */ }

	/* For logging */
#if (M_LOGGER_VARIABLE_DEF == ENABLE)
	lapcts16DeltaStrokePrateError = s16DeltaStrokeRateErrorComp;
	lapcts16DeltaStrokeRateErrorRaw = s16DeltaStrokeRateErrorRaw;
	lapcts16DeltaStrokeRateCompGain = s16RateCompGain;
	lapcts16PressPointTargetRate = s16PressPointTargetRate;
	lapcts16PressPointPRate = s16PressPointPRate;
#endif

	return s16DeltaStrokeRateErrorComp;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16GetRateErrorCompGain
* CALLED BY:            LAPCT_s16CompPressRateErr()
* Preconditions:        none
* PARAMETER:            stPressErrorRateComp_r
* RETURN VALUE:         s16RateCompGain
* Description:          Set gain for Rate error compensation
********************************************************************************/
static int16_t LAPCT_s16GetRateErrorCompGain(const stPRESS_ERROR_RATE_CMP_R_t stPressErrorRateComp_r)
{
	int16_t s16RateCompGain = 0;
	/* Gain by Piston Press rate */
	int16_t s16GainByPistPRate[4]= {0, 0, 0, 0};
	int16_t s16PistPRate = stPressErrorRateComp_r.s16CircuitPressRate;
	int16_t s16PistonPressure = stPressErrorRateComp_r.s16CircuitPress;
	int16_t s16PressError = stPressErrorRateComp_r.s16PressError;
	int16_t s16RateError = stPressErrorRateComp_r.s16RateError;
	int16_t s16TPrate = stPressErrorRateComp_r.s16TargetPressRate;

	if( s16PressError > S16_PCTRL_PRESS_0_BAR)
	{
		if(s16RateError > S16_P_RATE_0_BPS)
		{
			if(s16TPrate < 0)
			{
				/* Hold releasing */
				s16RateCompGain = L_s16IInter5Point(s16PistPRate,
										 S16_RATE_CMP_R_GAIN_P_LVL(4), S16_RATE_CMP_R_GAIN_BY_P_ERR_0(4),
										 S16_RATE_CMP_R_GAIN_P_LVL(3), S16_RATE_CMP_R_GAIN_BY_P_ERR_0(3),
										 S16_RATE_CMP_R_GAIN_P_LVL(2), S16_RATE_CMP_R_GAIN_BY_P_ERR_0(2),
										 S16_RATE_CMP_R_GAIN_P_LVL(1), S16_RATE_CMP_R_GAIN_BY_P_ERR_0(1),
										 S16_RATE_CMP_R_GAIN_P_LVL(0), S16_RATE_CMP_R_GAIN_BY_P_ERR_0(0));
			}
			else
			{
				/* Apply */
				s16RateCompGain = L_s16IInter4Point(s16PressError,
													S16_PCTRL_PRESS_0_BAR,	15, /* Magic numbers need to be replace with calibration parameter */
													S16_PCTRL_PRESS_1_BAR,	30, /* Magic numbers need to be replace with calibration parameter */
													S16_PCTRL_PRESS_5_BAR,	50, /* Magic numbers need to be replace with calibration parameter */
													S16_PCTRL_PRESS_10_BAR,	70); /* Magic numbers need to be replace with calibration parameter */
			}
		}
		else
		{
			/* Hold applying */
			s16GainByPistPRate[0] = L_s16IInter5Point(s16PistPRate,
									 S16_RATE_CMP_A_GAIN_P_LVL(0), S16_RATE_CMP_A_GAIN_BY_P_ERR_0(0),
									 S16_RATE_CMP_A_GAIN_P_LVL(1), S16_RATE_CMP_A_GAIN_BY_P_ERR_0(1),
									 S16_RATE_CMP_A_GAIN_P_LVL(2), S16_RATE_CMP_A_GAIN_BY_P_ERR_0(2),
									 S16_RATE_CMP_A_GAIN_P_LVL(3), S16_RATE_CMP_A_GAIN_BY_P_ERR_0(3),
									 S16_RATE_CMP_A_GAIN_P_LVL(4), S16_RATE_CMP_A_GAIN_BY_P_ERR_0(4));
			s16GainByPistPRate[1] = L_s16IInter5Point(s16PistPRate,
									 S16_RATE_CMP_A_GAIN_P_LVL(0), S16_RATE_CMP_A_GAIN_BY_P_ERR_1(0),
									 S16_RATE_CMP_A_GAIN_P_LVL(1), S16_RATE_CMP_A_GAIN_BY_P_ERR_1(1),
									 S16_RATE_CMP_A_GAIN_P_LVL(2), S16_RATE_CMP_A_GAIN_BY_P_ERR_1(2),
									 S16_RATE_CMP_A_GAIN_P_LVL(3), S16_RATE_CMP_A_GAIN_BY_P_ERR_1(3),
									 S16_RATE_CMP_A_GAIN_P_LVL(4), S16_RATE_CMP_A_GAIN_BY_P_ERR_1(4));
			s16GainByPistPRate[2] = L_s16IInter5Point(s16PistPRate,
									 S16_RATE_CMP_A_GAIN_P_LVL(0), S16_RATE_CMP_A_GAIN_BY_P_ERR_2(0),
									 S16_RATE_CMP_A_GAIN_P_LVL(1), S16_RATE_CMP_A_GAIN_BY_P_ERR_2(1),
									 S16_RATE_CMP_A_GAIN_P_LVL(2), S16_RATE_CMP_A_GAIN_BY_P_ERR_2(2),
									 S16_RATE_CMP_A_GAIN_P_LVL(3), S16_RATE_CMP_A_GAIN_BY_P_ERR_2(3),
									 S16_RATE_CMP_A_GAIN_P_LVL(4), S16_RATE_CMP_A_GAIN_BY_P_ERR_2(4));
			s16GainByPistPRate[3] = L_s16IInter5Point(s16PistPRate,
									 S16_RATE_CMP_A_GAIN_P_LVL(0), S16_RATE_CMP_A_GAIN_BY_P_ERR_3(0),
									 S16_RATE_CMP_A_GAIN_P_LVL(1), S16_RATE_CMP_A_GAIN_BY_P_ERR_3(1),
									 S16_RATE_CMP_A_GAIN_P_LVL(2), S16_RATE_CMP_A_GAIN_BY_P_ERR_3(2),
									 S16_RATE_CMP_A_GAIN_P_LVL(3), S16_RATE_CMP_A_GAIN_BY_P_ERR_3(3),
									 S16_RATE_CMP_A_GAIN_P_LVL(4), S16_RATE_CMP_A_GAIN_BY_P_ERR_3(4));
			s16RateCompGain = L_s16IInter4Point(s16PressError,
												S16_PCTRL_PRESS_0_BAR,	s16GainByPistPRate[0],
												S16_PCTRL_PRESS_1_BAR,	s16GainByPistPRate[1],
												S16_PCTRL_PRESS_5_BAR,	s16GainByPistPRate[2],
												S16_PCTRL_PRESS_10_BAR,	s16GainByPistPRate[3]);

			/* Prohibit extra-comp for holding Apply */
			if(s16PressError > S16_PCTRL_PRESS_10_BAR)
			{
				s16RateCompGain = 0;
			}
		}
	}
	else
	{
	/* ( s16PressError < S16_PCTRL_PRESS_0_BAR) */
		if(s16RateError > S16_P_RATE_0_BPS)
		{
			/* Hold releasing */
			s16GainByPistPRate[0] = L_s16IInter5Point(s16PistPRate,
									 S16_RATE_CMP_R_GAIN_P_LVL(4), S16_RATE_CMP_R_GAIN_BY_P_ERR_0(4),
									 S16_RATE_CMP_R_GAIN_P_LVL(3), S16_RATE_CMP_R_GAIN_BY_P_ERR_0(3),
									 S16_RATE_CMP_R_GAIN_P_LVL(2), S16_RATE_CMP_R_GAIN_BY_P_ERR_0(2),
									 S16_RATE_CMP_R_GAIN_P_LVL(1), S16_RATE_CMP_R_GAIN_BY_P_ERR_0(1),
									 S16_RATE_CMP_R_GAIN_P_LVL(0), S16_RATE_CMP_R_GAIN_BY_P_ERR_0(0));
			s16GainByPistPRate[1] = L_s16IInter5Point(s16PistPRate,
									 S16_RATE_CMP_R_GAIN_P_LVL(4), S16_RATE_CMP_R_GAIN_BY_P_ERR_1(4),
									 S16_RATE_CMP_R_GAIN_P_LVL(3), S16_RATE_CMP_R_GAIN_BY_P_ERR_1(3),
									 S16_RATE_CMP_R_GAIN_P_LVL(2), S16_RATE_CMP_R_GAIN_BY_P_ERR_1(2),
									 S16_RATE_CMP_R_GAIN_P_LVL(1), S16_RATE_CMP_R_GAIN_BY_P_ERR_1(1),
									 S16_RATE_CMP_R_GAIN_P_LVL(0), S16_RATE_CMP_R_GAIN_BY_P_ERR_1(0));
			s16GainByPistPRate[2] = L_s16IInter5Point(s16PistPRate,
									 S16_RATE_CMP_R_GAIN_P_LVL(4), S16_RATE_CMP_R_GAIN_BY_P_ERR_2(4),
									 S16_RATE_CMP_R_GAIN_P_LVL(3), S16_RATE_CMP_R_GAIN_BY_P_ERR_2(3),
									 S16_RATE_CMP_R_GAIN_P_LVL(2), S16_RATE_CMP_R_GAIN_BY_P_ERR_2(2),
									 S16_RATE_CMP_R_GAIN_P_LVL(1), S16_RATE_CMP_R_GAIN_BY_P_ERR_2(1),
									 S16_RATE_CMP_R_GAIN_P_LVL(0), S16_RATE_CMP_R_GAIN_BY_P_ERR_2(0));
			s16GainByPistPRate[3] = L_s16IInter5Point(s16PistPRate,
									 S16_RATE_CMP_R_GAIN_P_LVL(4), S16_RATE_CMP_R_GAIN_BY_P_ERR_3(4),
									 S16_RATE_CMP_R_GAIN_P_LVL(3), S16_RATE_CMP_R_GAIN_BY_P_ERR_3(3),
									 S16_RATE_CMP_R_GAIN_P_LVL(2), S16_RATE_CMP_R_GAIN_BY_P_ERR_3(2),
									 S16_RATE_CMP_R_GAIN_P_LVL(1), S16_RATE_CMP_R_GAIN_BY_P_ERR_3(1),
									 S16_RATE_CMP_R_GAIN_P_LVL(0), S16_RATE_CMP_R_GAIN_BY_P_ERR_3(0));

			s16RateCompGain = L_s16IInter4Point( (-s16PressError),
												S16_PCTRL_PRESS_0_BAR,	s16GainByPistPRate[0],
												S16_PCTRL_PRESS_1_BAR,	s16GainByPistPRate[1],
												S16_PCTRL_PRESS_5_BAR,	s16GainByPistPRate[2],
												S16_PCTRL_PRESS_10_BAR,	s16GainByPistPRate[3]);

			/* Prohibit extra-comp for holding release */
			if((s16PressError < -S16_PCTRL_PRESS_10_BAR) || (stPressErrorRateComp_r.s16TargetP <= S16_PCTRL_PRESS_0_5_BAR))
			{
				s16RateCompGain = 0;
			}
		}
		else
		{
		/* (s16RateError < S16_P_RATE_0_BPS) */
			if(s16TPrate > 0)
			{
				/* Hold applying */
				s16RateCompGain = L_s16IInter5Point(s16PistPRate,
										 S16_RATE_CMP_A_GAIN_P_LVL(0), S16_RATE_CMP_A_GAIN_BY_P_ERR_0(0),
										 S16_RATE_CMP_A_GAIN_P_LVL(1), S16_RATE_CMP_A_GAIN_BY_P_ERR_0(1),
										 S16_RATE_CMP_A_GAIN_P_LVL(2), S16_RATE_CMP_A_GAIN_BY_P_ERR_0(2),
										 S16_RATE_CMP_A_GAIN_P_LVL(3), S16_RATE_CMP_A_GAIN_BY_P_ERR_0(3),
										 S16_RATE_CMP_A_GAIN_P_LVL(4), S16_RATE_CMP_A_GAIN_BY_P_ERR_0(4));
			}
			else
			{
				/* Release */
				s16RateCompGain = L_s16IInter4Point( (-s16PressError),
													S16_PCTRL_PRESS_0_BAR,	15,
													S16_PCTRL_PRESS_1_BAR,	30,
													S16_PCTRL_PRESS_5_BAR,	40,
													S16_PCTRL_PRESS_10_BAR,	50);
			}
		}
	}

	if (( -S16_P_RATE_15_BPS < (stPressErrorRateComp_r.s16TargetPressRate) ) && ((stPressErrorRateComp_r.s16TargetPressRate) < S16_P_RATE_15_BPS))
	{
		s16RateCompGain = 0;
	}
	else { /* Do Nothing */ }

	return s16RateCompGain ;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16DecidePRateErrorCompLimit
* CALLED BY:            LAPCT_s16GetDeltaStrkPRateError()
* Preconditions:        none
* PARAMETER:            s16TargetPress, u16WheelVolumeState
* RETURN VALUE:         stPRateErrorCompLimitOut
* Description:          Decide pressure rate error compensation limit
********************************************************************************/
static stP_RATE_ERROR_COMP_LIMIT_t LAPCT_s16DecidePRateErrorCompLimit(int16_t  s16CircuitPress, uint16_t u16WheelVolumeState)
{
	stP_RATE_ERROR_COMP_LIMIT_t stPRateErrorCompLimitOut = {0,0};
	int16_t s16PosPerrorCompLimit = 0;
	int16_t s16NegPerrorCompLimit = 0;

	s16PosPerrorCompLimit = s16CircuitPress + S16_P_RATE_ERROR_COMP_THD;
	s16NegPerrorCompLimit = ((s16CircuitPress - S16_P_RATE_ERROR_COMP_THD) > S16_PCTRL_PRESS_0_BAR) ? (s16CircuitPress - S16_P_RATE_ERROR_COMP_THD) : S16_PCTRL_PRESS_0_BAR;

	stPRateErrorCompLimitOut.s16PRateErrorCompMax = (int16_t)L_s32LimitMinMax(LAPCT_s32CalcStorkeByPress(s16PosPerrorCompLimit, u16WheelVolumeState) - LAPCT_s32CalcStorkeByPress(s16CircuitPress, u16WheelVolumeState), ASW_S16_MIN, ASW_S16_MAX);
	stPRateErrorCompLimitOut.s16PRateErrorCompMin = (int16_t)L_s32LimitMinMax(LAPCT_s32CalcStorkeByPress(s16NegPerrorCompLimit, u16WheelVolumeState) - LAPCT_s32CalcStorkeByPress(s16CircuitPress, u16WheelVolumeState), ASW_S16_MIN, ASW_S16_MAX);

	return stPRateErrorCompLimitOut;
}

