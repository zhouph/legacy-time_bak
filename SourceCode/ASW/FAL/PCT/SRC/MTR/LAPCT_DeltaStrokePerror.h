/*******************************************************************************
* Project Name:     IDB
* File Name:        LAPCT_DeltaStrokePerror.h
* Description:
* Logic version:
********************************************************************************
*  Modification Log
*  Date         Author          Description
*  -----        -----           -----
********************************************************************************/
#ifndef LAPCT_DELTASTROKEPERROR_H_
#define LAPCT_DELTASTROKEPERROR_H_
/* Includes ******************************************************************/

/* Global Definition *********************************************************/
#define U16_PRESS_BAND_ARRAY_MAX					(7)
#define U16_PERROR_BAND_ARRAY_MAX					(5)

/* Calibration Type Definition************************************************/


/* Global Extern Variable ****************************************************/

/* Global Function ***********************************************************/
int16_t LAPCT_s16GetDeltaStrokePerror(int16_t s16CircuitP, int16_t s16CircuitPRate10ms, int16_t s16PressError, uint8_t u8PRateErrCompEnableFlg);

#endif /* LAPCT_DELTASTROKEPERROR_H_ */
