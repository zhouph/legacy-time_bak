/*******************************************************************************
* Project Name:     IDB
* File Name:        LAPCT_DeltaStrokeMap.h
* Description:
* Logic version:
********************************************************************************
*  Modification Log
*  Date         Author          Description
*  -----        -----           -----
********************************************************************************/
#ifndef LAPCT_DELTASTROKEMAP_H_
#define LAPCT_DELTASTROKEMAP_H_
/* Includes ******************************************************************/

/* Global Definition *********************************************************/
#define S16_MOVING_AVG_1							(1)
#define S16_MOVING_AVG_2							(2)
#define S16_MOVING_AVG_5							(5)
#define S16_MOVING_AVG_10							(10)

/* Global Function ***********************************************************/
extern int16_t LAPCT_s16GetDeltaStrokeMap(int16_t s16CircuitP, int16_t s16IneffectiveStrokeCompFlg, int16_t s16LimDelStrkTargetMax, int16_t s16LimDelStrokeMin, int16_t s16TargetPressOld);

#endif /* LAPCT_DELTASTROKEMAP_H_ */
