/**
 * @defgroup LAPCT_DecidePressSource LAPCT_DecidePressSource
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LAPCT_DecidePressCtrlMode.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Pct_5msCtrl.h"
#include "LAPCT_DecidePressSource.h"
#include "LAPCT_ChkPosnForStkRcvr.h"
#include "LAPCT_DecideStkRcvrState.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define S16_LOW_TO_HIGH_PRESSURE_THR			S16_PCTRL_PRESS_135_BAR	/* S16_PCTRL_PRESS_135_BAR */
#define S16_HIGH_TO_LOW_PRESSURE_THR			S16_PCTRL_PRESS_5_BAR


#define S16_PRESS_SRC_FWD_STK_MARGIN	300
#define S16_PRESS_SRC_BACKW_STK_MARGIN	300

#define U8_STABLE_STROKE  200

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
U32_BIT_STRUCT_t	PCT_DECIDE_PRESS_SOURCE;

#define lapctu1AbsActFlg                	PCT_DECIDE_PRESS_SOURCE.bit31
#define lapctu1EscActFlg	  				PCT_DECIDE_PRESS_SOURCE.bit30
#define lapctu1TcsActFlg		  			PCT_DECIDE_PRESS_SOURCE.bit29
#define lapctu1AbsDefectFlg     			PCT_DECIDE_PRESS_SOURCE.bit28
#define lapctu1EscDefectFlg		      		PCT_DECIDE_PRESS_SOURCE.bit27
#define lapctu1TcsDefectFlg		      		PCT_DECIDE_PRESS_SOURCE.bit26
#define lapcts16ContinuouslyBoostFlg		PCT_DECIDE_PRESS_SOURCE.bit25
#define lapcts16ContinuouslyBoostOldFlg		PCT_DECIDE_PRESS_SOURCE.bit24

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_CONST_UNSPECIFIED
#include "Pct_MemMap.h"
/** Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Pct_MemMap.h"

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"


#define PCT_5MSCTRL_START_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/** Variable Section (32BIT)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

/*Local Use(Local Define Local Use) - Start*/

int16_t laCals16LowToHighPressThd = S16_LOW_TO_HIGH_PRESSURE_THR;

FAL_STATIC int8_t lapcts8PChamberVolume;
FAL_STATIC int8_t   laptcs8PCtrlState;
FAL_STATIC int8_t   lapcts8PCtrlBoostMode;
/*
static int8_t   lapcts8BackwardPiston;
static int8_t  lapcts8MovForwardPiston;
*/

FAL_STATIC int8_t   lapcts8BackwardPiston;
FAL_STATIC int8_t  lapcts8MovForwardPiston;
FAL_STATIC int8_t lapcts8DetBackwardDirection;
FAL_STATIC int8_t lapcts8DetForwardDirection;

FAL_STATIC int16_t laptcs16TargetDeltaStroke;


FAL_STATIC int8_t  laptcs8PressHoldByValve;
FAL_STATIC int16_t  laptcs16TransitionPress;

FAL_STATIC int16_t laptcs16PwrPistonTarP;
FAL_STATIC int16_t laptcs16PwrPistonTarDelP;

FAL_STATIC int8_t  laptcs8SelectLowPress;

FAL_STATIC int8_t  laptcs8SelectHighPress;
FAL_STATIC int8_t laptcs8CheckMotorPowerLevel;

FAL_STATIC int16_t lapcts16StkPosn_mm;
FAL_STATIC int16_t lapcts16BackwRcvrMinPosn_mm;
FAL_STATIC int16_t lapcts16FwdRcvrMaxPosn_mm;

/*Local Use(Local Define Local Use) - End*/

#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"


#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"


#define PCT_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"


#define PCT_5MSCTRL_START_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/** Variable Section (32BIT)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_CODE
#include "Pct_MemMap.h"

static void LAPCT_vDecidePressSourceInput(void);
static void LAPCT_vDecidePressSourceOutput(void);
static void LAPCT_vSelctPressChamberVolume(void);
static int8_t LAPCT_s8PChamVolmeFromReady(int8_t s8PCtrlState);
static int8_t LAPCT_s8PChamVolmeFromLowP(int8_t s8PressHoldByValve, int8_t s8HihgPress, int8_t s8PCtrlState);
static int8_t LAPCT_s8PChamVolmeFromHighP(int8_t s8LowPress);
static int8_t LAPCT_s8PChamVolmeFromBackward(int8_t s8LowPress );
static void LAPCT_vDetectBackWardPistion(void);
static void LAPCT_vDetectForwardPistion(void);

static int8_t LAPCT_s8PChamVolmeFromHoldP(int8_t s8PressHoldByValve );

static void LAPCT_vDetectLowPressArea(void);
static void LAPCT_vDetectHighPressArea(void);

static void LAPCT_vDecideBoostDirection(void);
static void LAPCT_vDcdBoostDirAtAbsHiBoost(void);
static void LAPCT_vDcdBoostDirAtAbsLowBoost(void);
static void LAPCT_vDcdBoostDirAtNorBoost(void);
void LAPCT_vDecidePressSourceMain(void);



/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void LAPCT_vDecidePressSourceMain(void)
{
	LAPCT_vDecidePressSourceInput();
	LAPCT_vSelctPressChamberVolume();
	LAPCT_vDecidePressSourceOutput();
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

static void LAPCT_vDecidePressSourceInput(void)
{


	laptcs8PCtrlState = PressCTRLLocalBus.s8PCtrlState ;
	lapcts8PCtrlBoostMode = PressCTRLLocalBus.s8PCtrlBoostMode;
	laptcs16TargetDeltaStroke =   PressCTRLLocalBus.s16DeltaStkTar;

	laptcs16PwrPistonTarP =PressCTRLLocalBus.s16MuxPwrPistonTarP;
	laptcs16PwrPistonTarDelP = PressCTRLLocalBus.s16MuxPwrPistonTarDP;
	laptcs8CheckMotorPowerLevel=0 ;
	lapcts16StkPosn_mm = PressCTRLLocalBus.s16StkPosn_mm;
	lapcts16BackwRcvrMinPosn_mm = PressCTRLLocalBus.s16BackwRcvrMinPosn_mm;
	lapcts16FwdRcvrMaxPosn_mm = PressCTRLLocalBus.s16FwdRcvrMaxPosn_mm;

	lapctu1AbsActFlg = (Pct_5msCtrlBus.Pct_5msCtrlAbsCtrlInfo.AbsActFlg > 0 ) ? TRUE : FALSE ;
	lapctu1EscActFlg = (Pct_5msCtrlBus.Pct_5msCtrlEscCtrlInfo.EscActFlg > 0 ) ? TRUE : FALSE ;
	lapctu1TcsActFlg = (Pct_5msCtrlBus.Pct_5msCtrlTcsCtrlInfo.TcsActFlg > 0 ) ? TRUE : FALSE ;
	lapctu1AbsDefectFlg = Pct_5msCtrlBus.Pct_5msCtrlAbsCtrlInfo.AbsDefectFlg;
	lapctu1EscDefectFlg = Pct_5msCtrlBus.Pct_5msCtrlEscCtrlInfo.EscDefectFlg;
	lapctu1TcsDefectFlg = Pct_5msCtrlBus.Pct_5msCtrlTcsCtrlInfo.TcsDefectFlg;




}

static void LAPCT_vDecidePressSourceOutput(void)
{
	/*
	     poutput[0]= lapcts8PChamberVolume ;
	     poutput[1]= laptcs8SelectHighPress;
	     poutput[2]= laptcs8SelectLowPress;
	     poutput[3]= laptcs16TransitionPress/100;
*/
	PressCTRLLocalBus.s16DeltaStkTar = laptcs16TargetDeltaStroke;
	PressCTRLLocalBus.s8PChamberVolume = lapcts8PChamberVolume;
	Pct_5msCtrlBus.Pct_5msCtrlPChamberVolume = lapcts8PChamberVolume;


}



static void LAPCT_vSelctPressChamberVolume(void)
{

	LAPCT_vDetectHighPressArea();
	LAPCT_vDetectLowPressArea();
	LAPCT_vDecideBoostDirection();




	switch(lapcts8PChamberVolume)
	{
		case U8_BOOST_READY:
			
			lapcts8PChamberVolume = LAPCT_s8PChamVolmeFromReady(laptcs8PCtrlState);

			
			break;

		case U8_BOOST_LOW_PRESS:
			
			lapcts8PChamberVolume = LAPCT_s8PChamVolmeFromLowP(laptcs8PressHoldByValve,laptcs8SelectHighPress,laptcs8PCtrlState);

			break;
			
		case U8_BOOST_HIGH_PRESS:
			

			lapcts8PChamberVolume = LAPCT_s8PChamVolmeFromHighP(laptcs8SelectLowPress);

			break;
			
			
		case U8_BOOST_BACKWARD:
			
			lapcts8PChamberVolume = LAPCT_s8PChamVolmeFromBackward(laptcs8SelectLowPress);

			break;
			
						
		case U8_BOOST_HOLD_PRESS:
			
			lapcts8PChamberVolume = LAPCT_s8PChamVolmeFromHoldP(laptcs8PressHoldByValve);

			break;
			

		default:
			lapcts8PChamberVolume = U8_BOOST_READY;
			break;
	}
			


}



static int8_t LAPCT_s8PChamVolmeFromReady(int8_t s8PCtrlState)
{
	int8_t s8ReturnValue;

   	if((s8PCtrlState == S8_PRESS_BOOST)|| (s8PCtrlState==S8_PRESS_PREBOOST)) 
	{
   		s8ReturnValue = U8_BOOST_LOW_PRESS;
	}
	else
	{
		s8ReturnValue = U8_BOOST_READY;
	}
	return s8ReturnValue;
}

static int8_t LAPCT_s8PChamVolmeFromLowP(int8_t s8PressHoldByValve, int8_t s8HihgPress, int8_t s8PCtrlState)
{
	int8_t s8ReturnValue;
	
	if(lapcts8DetBackwardDirection == TRUE)
	{
		s8ReturnValue = U8_BOOST_BACKWARD;
	}
   	else if(s8HihgPress == TRUE )
	{
   		s8ReturnValue = U8_BOOST_HIGH_PRESS;
	}
	else if(s8PressHoldByValve == TRUE)
	{
		s8ReturnValue = U8_BOOST_HOLD_PRESS;
	}
	else if((s8PCtrlState == S8_PRESS_READY)||(s8PCtrlState == S8_PRESS_INHIBIT))
	{
		s8ReturnValue = U8_BOOST_READY;
	}
	else
	{
		s8ReturnValue = U8_BOOST_LOW_PRESS ;
	}
	return s8ReturnValue;
}


static int8_t LAPCT_s8PChamVolmeFromHighP(int8_t s8LowPress)
{
	int8_t s8ReturnValue;

	if (lapcts8DetBackwardDirection == TRUE)
	{
		s8ReturnValue = U8_BOOST_BACKWARD;
	}
   	else if(s8LowPress == TRUE ) 
	{
   		s8ReturnValue = U8_BOOST_LOW_PRESS;
	}
	else 
	{
		s8ReturnValue = U8_BOOST_HIGH_PRESS;
	}
	return s8ReturnValue;
}

static int8_t LAPCT_s8PChamVolmeFromBackward(int8_t s8LowPress )
{
	int8_t s8ReturnValue;

	if(lapcts8DetForwardDirection == TRUE)
	{
		if(s8LowPress == TRUE ) 
		{
	   		s8ReturnValue = U8_BOOST_LOW_PRESS;
		}
		else
		{
			s8ReturnValue = U8_BOOST_HIGH_PRESS;
		}
	}  
	else 
	{
		s8ReturnValue = U8_BOOST_BACKWARD;
	}
	return s8ReturnValue;
}



static int8_t LAPCT_s8PChamVolmeFromHoldP(int8_t s8PressHoldByValve )
{
	int8_t s8ReturnValue;

	if(s8PressHoldByValve == FALSE)
	{
		s8ReturnValue = U8_BOOST_LOW_PRESS;
	}  
	else 
	{
		;
	}
	return s8ReturnValue;
}

static void LAPCT_vDetectHighPressArea(void)
{
	static int8_t laptcs8SelectHighPressOld=0;

	if((laptcs16PwrPistonTarP > laCals16LowToHighPressThd)&&(laptcs16PwrPistonTarDelP > 0 ))
	{
		laptcs8SelectHighPress = 1;
		laptcs8SelectLowPress = 0;
	}
	else if(laptcs8CheckMotorPowerLevel >= S8_MTR_PWR_LEVEL_2) /* need to decide motor power level condition, */
	{
		if(laptcs16PwrPistonTarP > S16_PCTRL_PRESS_100_BAR)
		{
			laptcs8SelectHighPress = 1;	
			laptcs8SelectLowPress = 0;
		}
		else
		{
			laptcs8SelectHighPress = 0;	
		}	
	}
	else
	{
		laptcs8SelectHighPress = 0;
	}
	
	if((laptcs8SelectHighPressOld == 0) && (laptcs8SelectHighPress == 1))
	{
		laptcs16TransitionPress = laptcs16PwrPistonTarP;
		
	}
	else
	{
		;
	}
	
	laptcs8SelectHighPressOld = laptcs8SelectHighPress;
}

static void LAPCT_vDetectLowPressArea(void)
{
	static int8_t lapcts8MovForwardPistonOld = 0;
	static int16_t laptcs16TransitionPressMargin = S16_HIGH_TO_LOW_PRESSURE_THR;

	/*laptcs16TransitionPressMargin = S16_PCTRL_PRESS_5_BAR;*/ /*laptcs16TransitionPress /20;*/

	if((laptcs8PCtrlState == S8_PRESS_READY)||(laptcs8PCtrlState == S8_PRESS_INHIBIT))
	{
		laptcs8SelectLowPress = 0;
		laptcs8SelectHighPress = 0;

	}
	else if(lapcts8PCtrlBoostMode == S8_BOOST_MODE_RESPONSE_L1)
	{
		laptcs8SelectLowPress = 0;
		laptcs8SelectHighPress = 0;
	}
	else if((laptcs16PwrPistonTarP < (laptcs16TransitionPress + laptcs16TransitionPressMargin))&&(laptcs16PwrPistonTarDelP < 0 ))
	{
		laptcs8SelectLowPress = 1;
		laptcs8SelectHighPress = 0;	
	}
	else if(laptcs16PwrPistonTarP < laptcs16TransitionPress)
	{		
		laptcs8SelectLowPress = 1;
	}
	else
	{
		;
	}
	
	if((lapcts8MovForwardPistonOld==0)&&(lapcts8MovForwardPiston==1))
	{
		if((laptcs16PwrPistonTarP < laCals16LowToHighPressThd) || (laptcs16PwrPistonTarDelP < 0 ))
		{
			laptcs8SelectLowPress = 1;
		}
		else{ }
	}
	else { }
}

static void LAPCT_vDecideBoostDirection(void)
{
	int8_t u8AllowedBoostDirectionChange = 0;
	
	if(((lapctu1AbsActFlg == 1) && (lapctu1AbsDefectFlg == 0))
	|| ((lapctu1EscActFlg == 1) && (lapctu1EscDefectFlg == 0))
	|| ((lapctu1TcsActFlg == 1) && (lapctu1TcsDefectFlg == 0)))
	{
		u8AllowedBoostDirectionChange = 1;
	}
	else
	{
		u8AllowedBoostDirectionChange = 0;
	}
	
	if(u8AllowedBoostDirectionChange == 1) /*need to change switch*/
	{
		if((lapcts8PChamberVolume == U8_BOOST_HIGH_PRESS) || (lapcts8PChamberVolume == U8_BOOST_BACKWARD))
		{
			LAPCT_vDcdBoostDirAtAbsHiBoost();
		}
		else
		{
			LAPCT_vDcdBoostDirAtAbsLowBoost();
		}
	}
	else
	{
		LAPCT_vDcdBoostDirAtNorBoost();
	}
	
	LAPCT_vDetectBackWardPistion();
	LAPCT_vDetectForwardPistion();

	lapcts16ContinuouslyBoostOldFlg = lapcts16ContinuouslyBoostFlg;
}

static void LAPCT_vDcdBoostDirAtAbsHiBoost(void)
{
	if(lapcts16StkPosn_mm > S16_EMGY_STRK_RCVR_ENT_POSI) /*forward direction H/W limit position*/
	{
		lapcts8MovForwardPiston = 0;
		lapcts8BackwardPiston = 1;
	}
	else if(lapcts16StkPosn_mm < (lapcts16BackwRcvrMinPosn_mm + S16_PRESS_SRC_BACKW_STK_MARGIN))
	{
		lapcts8MovForwardPiston = 1;
		lapcts8BackwardPiston = 0;
	}
	else
	{
		; /* Keep Piston direction */
	}
}

static void LAPCT_vDcdBoostDirAtAbsLowBoost(void)
{
	lapcts16ContinuouslyBoostFlg = 0;

	if(lapcts16ContinuouslyBoostFlg == 1)
	{
		if(lapcts16ContinuouslyBoostOldFlg == 0)
		{
			if(lapcts16StkPosn_mm < (lapcts16FwdRcvrMaxPosn_mm - S16_PRESS_SRC_FWD_STK_MARGIN))
			{
				lapcts8MovForwardPiston = 1;
				lapcts8BackwardPiston = 0;
			}
			else if(lapcts16StkPosn_mm >= S16_EMGY_STRK_RCVR_ENT_POSI)
			{
				lapcts8MovForwardPiston = 0;
				lapcts8BackwardPiston = 1;
			}
			else {} /* Keep Piston direction */
		}
		else
		{
			if(lapcts8MovForwardPiston == 1)
			{
				if(lapcts16StkPosn_mm < S16_EMGY_STRK_RCVR_ENT_POSI)
				{
					lapcts8MovForwardPiston = 1;
					lapcts8BackwardPiston = 0;
				}
				else
				{
					lapcts8MovForwardPiston = 0;
					lapcts8BackwardPiston = 1;
				}
			}
			else if(lapcts8BackwardPiston == 1)
			{
				if(lapcts16StkPosn_mm < (lapcts16BackwRcvrMinPosn_mm + S16_PRESS_SRC_BACKW_STK_MARGIN))
				{
					lapcts8MovForwardPiston = 1;
					lapcts8BackwardPiston = 0;
				}
				else {} /* Keep Piston direction */
			}
			else {} /* Keep Piston direction */

		}
	}
	else
	{		
		if(lapcts16StkPosn_mm < (lapcts16BackwRcvrMinPosn_mm + S16_PRESS_SRC_BACKW_STK_MARGIN))
		{
			lapcts8MovForwardPiston = 1;
			lapcts8BackwardPiston = 0;
		}
		else if(lapcts16StkPosn_mm > (lapcts16FwdRcvrMaxPosn_mm - S16_PRESS_SRC_FWD_STK_MARGIN))
		{
			lapcts8MovForwardPiston = 0;
			lapcts8BackwardPiston = 1;
		}
		else {} /* Keep Piston direction */
	}
}

static void LAPCT_vDcdBoostDirAtNorBoost(void)
{
	lapcts8MovForwardPiston = 1;
	lapcts8BackwardPiston = 0;
}



static void LAPCT_vDetectBackWardPistion(void)
{
	static int16_t lapcts16MtrStableCnt = 0;


	if(lapcts8BackwardPiston == TRUE)
	{
		if( lapcts8DetBackwardDirection ==0)
		{
			if (laptcs16TargetDeltaStroke <= 0 )
			{
				lapcts8DetBackwardDirection = 1;
			}
			else
			{
				laptcs16TargetDeltaStroke = laptcs16TargetDeltaStroke - U8_STABLE_STROKE;
				laptcs16TargetDeltaStroke =  (laptcs16TargetDeltaStroke < 0) ? 0 : laptcs16TargetDeltaStroke;
			}

			if(lapcts16MtrStableCnt > 5)
			{
				lapcts16MtrStableCnt = 0;
				lapcts8DetBackwardDirection = 1;
			}
			else
			{
				lapcts16MtrStableCnt = lapcts16MtrStableCnt + 1;
			}
		}
		else
		{
			lapcts16MtrStableCnt = 0;
		}

	}
	else
	{
		lapcts8DetBackwardDirection = 0;
		lapcts16MtrStableCnt =0;
	}

}

static void LAPCT_vDetectForwardPistion(void)
{
	static int16_t lapcts16MtrStableCnt = 0;


	if(lapcts8MovForwardPiston == TRUE)
	{
		if( (lapcts8DetForwardDirection == 0))
		{
			if (laptcs16TargetDeltaStroke >= 0 )
			{
				lapcts8DetForwardDirection = 1;
			}
			else
			{
				laptcs16TargetDeltaStroke = laptcs16TargetDeltaStroke + U8_STABLE_STROKE;

				laptcs16TargetDeltaStroke =  (laptcs16TargetDeltaStroke > 0) ? 0 : laptcs16TargetDeltaStroke;

			}

			if(lapcts16MtrStableCnt > 5)
			{
				lapcts16MtrStableCnt = 0;
				lapcts8DetForwardDirection = 1;
			}
			else
			{
				lapcts16MtrStableCnt = lapcts16MtrStableCnt + 1;
			}
		}
		else
		{
			lapcts16MtrStableCnt = 0;
		}

	}
	else
	{
		lapcts8DetForwardDirection = 0;
		lapcts16MtrStableCnt =0;
	}
}

