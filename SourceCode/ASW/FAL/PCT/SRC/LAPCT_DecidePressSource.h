/**
 * @defgroup LAPCT_DecidePressSource LAPCT_DecidePressSource
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LAPCT_DecidePressSource.h
 * @brief       Local Header File
 * @date        20YY. MM. DD.
 ******************************************************************************/
 
#ifndef LAPCT_DECIDEPRESSSOURE_H_
#define LAPCT_DECIDEPRESSSOURE_H_




/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/


/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/


/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void LAPCT_vDecidePressSourceMain(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* LAPCT_DECIDEPRESSSOURE_H_ */
/** @} */
