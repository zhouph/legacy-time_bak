/**
 * @defgroup Pct_1msCtrl Pct_1msCtrl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Pct_1msCtrl.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Pct_1msCtrl.h"
#include "Pct_1msCtrl_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define M_PREBOOST_CONTROL					DISABLE		/* ENABLE DISABLE */

#if (M_PREBOOST_CONTROL == ENABLE)
#define S16_PREBOOST_DCT_CNT_THR			15
#define S16_PREBOOST_DCT_PEDAL_RATE_THR		5		/* 10mm/s - scale 100 */
#define S16_PREBOOST_DCT_PEDAL_LOW_THR		10		/* 1mm  - scale 10 */
#define S16_PREBOOST_DCT_PEDAL_MID_THR		20		/* 2mm  - scale 10 */
#define S16_PREBOOST_DCT_PEDAL_HIGH_THR		30		/* 3mm  - scale 10 */
#endif /* (M_PREBOOST_CONTROL == ENABLE) */
/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PCT_1MSCTRL_START_SEC_CONST_UNSPECIFIED
#include "Pct_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PCT_1MSCTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Pct_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PCT_1MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Pct_1msCtrl_HdrBusType Pct_1msCtrlBus;

/* Version Info */
const SwcVersionInfo_t Pct_1msCtrlVersionInfo = 
{   
    PCT_1MSCTRL_MODULE_ID,           /* Pct_1msCtrlVersionInfo.ModuleId */
    PCT_1MSCTRL_MAJOR_VERSION,       /* Pct_1msCtrlVersionInfo.MajorVer */
    PCT_1MSCTRL_MINOR_VERSION,       /* Pct_1msCtrlVersionInfo.MinorVer */
    PCT_1MSCTRL_PATCH_VERSION,       /* Pct_1msCtrlVersionInfo.PatchVer */
    PCT_1MSCTRL_BRANCH_VERSION       /* Pct_1msCtrlVersionInfo.BranchVer */
};
    
/* Input Data Element */
Spc_1msCtrlPedlTrvlFild1msInfo_t Pct_1msCtrlPedlTrvlFild1msInfo;
Det_1msCtrlPedlTrvlRate1msInfo_t Pct_1msCtrlPedlTrvlRate1msInfo;
Mom_HndlrEcuModeSts_t Pct_1msCtrlEcuModeSts;
Pct_5msCtrlPCtrlSt_t Pct_1msCtrlPCtrlSt;

/* TODO: RTE Interface */
#if (M_PREBOOST_CONTROL == ENABLE)
extern uint8_t lapctu8CutVVLongOpenFlg;
#endif /* (M_PREBOOST_CONTROL == ENABLE) */
/* Output Data Element */
Pct_1msCtrlPreBoostMod_t Pct_1msCtrlPreBoostMod;

uint32 Pct_1msCtrl_Timer_Start;
uint32 Pct_1msCtrl_Timer_Elapsed;

#define PCT_1MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
#define PCT_1MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PCT_1MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
#define PCT_1MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PCT_1MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
#define PCT_1MSCTRL_START_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/** Variable Section (32BIT)**/


#define PCT_1MSCTRL_STOP_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PCT_1MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PCT_1MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
#define PCT_1MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PCT_1MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
#define PCT_1MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PCT_1MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
#define PCT_1MSCTRL_START_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/** Variable Section (32BIT)**/


#define PCT_1MSCTRL_STOP_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#if (M_PREBOOST_CONTROL == ENABLE)
static uint8_t LAPCT_u8DctPreboostMode(int8_t s8PCtrlState, int16_t s16PedlTrvlRate1msMmPerSec, int16_t s16PdtFild1ms, uint8_t u8CutVVLongOpenFlg);
static int16_t LAPCT_s16CalcPreboostDctCnt(int16_t s16PedalTravel1ms, int16_t s16PedalRate1ms, uint8_t u8PreboostDctFlg);
#endif /* (M_PREBOOST_CONTROL == ENABLE) */
#define PCT_1MSCTRL_START_SEC_CODE
#include "Pct_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Pct_1msCtrl_Init(void)
{
    /* Initialize internal bus */
    Pct_1msCtrlBus.Pct_1msCtrlPedlTrvlFild1msInfo.PdtFild1ms = 0;
    Pct_1msCtrlBus.Pct_1msCtrlPedlTrvlRate1msInfo.PedlTrvlRate1msMmPerSec = 0;
    Pct_1msCtrlBus.Pct_1msCtrlEcuModeSts = 0;
    Pct_1msCtrlBus.Pct_1msCtrlPCtrlSt = 0;
    Pct_1msCtrlBus.Pct_1msCtrlPreBoostMod = 0;
}

void Pct_1msCtrl(void)
{
    Pct_1msCtrl_Timer_Start = STM0_TIM0.U;

    /* Input */
    Pct_1msCtrl_Read_Pct_1msCtrlPedlTrvlFild1msInfo(&Pct_1msCtrlBus.Pct_1msCtrlPedlTrvlFild1msInfo);
    /*==============================================================================
    * Members of structure Pct_1msCtrlPedlTrvlFild1msInfo 
     : Pct_1msCtrlPedlTrvlFild1msInfo.PdtFild1ms;
     =============================================================================*/
    
    Pct_1msCtrl_Read_Pct_1msCtrlPedlTrvlRate1msInfo(&Pct_1msCtrlBus.Pct_1msCtrlPedlTrvlRate1msInfo);
    /*==============================================================================
    * Members of structure Pct_1msCtrlPedlTrvlRate1msInfo 
     : Pct_1msCtrlPedlTrvlRate1msInfo.PedlTrvlRate1msMmPerSec;
     =============================================================================*/
    
    Pct_1msCtrl_Read_Pct_1msCtrlEcuModeSts(&Pct_1msCtrlBus.Pct_1msCtrlEcuModeSts);
    Pct_1msCtrl_Read_Pct_1msCtrlPCtrlSt(&Pct_1msCtrlBus.Pct_1msCtrlPCtrlSt);

    /* Process */
#if (M_PREBOOST_CONTROL == ENABLE)
    Pct_1msCtrlBus.Pct_1msCtrlPreBoostMod = LAPCT_u8DctPreboostMode(Pct_1msCtrlBus.Pct_1msCtrlPCtrlSt,
    																Pct_1msCtrlBus.Pct_1msCtrlPedlTrvlRate1msInfo.PedlTrvlRate1msMmPerSec,
    																Pct_1msCtrlBus.Pct_1msCtrlPedlTrvlFild1msInfo.PdtFild1ms,
    																lapctu8CutVVLongOpenFlg);
#else
    Pct_1msCtrlBus.Pct_1msCtrlPreBoostMod = FALSE;
#endif /* (M_PREBOOST_CONTROL == ENABLE) */
    /* Output */
    Pct_1msCtrl_Write_Pct_1msCtrlPreBoostMod(&Pct_1msCtrlBus.Pct_1msCtrlPreBoostMod);

    Pct_1msCtrl_Timer_Elapsed = STM0_TIM0.U - Pct_1msCtrl_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
#if (M_PREBOOST_CONTROL == ENABLE)
static uint8_t LAPCT_u8DctPreboostMode(int8_t s8PCtrlState, int16_t s16PedlTrvlRate1msMmPerSec, int16_t s16PdtFild1ms, uint8_t u8CutVVLongOpenFlg)
{
	static uint8_t lsahbu8PreboostDctFlg = FALSE;
	static int16_t lapcts16PreboostDctCnt = 0;
	static uint16_t lapctu16PreboostMntCnt = 0;
	static uint8_t lapctu8PreboostPossibleFlg = FALSE;
	static uint8_t lapctu8MaintainCntOnFlg = FALSE;
	static uint8_t lapctu8LoopCnt = 4;

	lapctu8LoopCnt = (lapctu8LoopCnt + 1) % 5;

	if ((lapctu8PreboostPossibleFlg == FALSE) && (s8PCtrlState == S8_PRESS_READY) && (u8CutVVLongOpenFlg == TRUE) && (lapctu8MaintainCntOnFlg == FALSE) )
	{
		lapctu8PreboostPossibleFlg = TRUE;
	}
	else
	{
		if (s8PCtrlState == S8_PRESS_BOOST)
		{
			lapctu8PreboostPossibleFlg = FALSE;
			lapctu16PreboostMntCnt = 0;
			lapctu8MaintainCntOnFlg = FALSE;
		}
		else if (s8PCtrlState == S8_PRESS_FADE_OUT)
		{
			lapctu8PreboostPossibleFlg = FALSE;
		}
		else { /* Maintain lapctu8PreboostPossibleFlg */ }
	}

	if (lapctu8PreboostPossibleFlg == TRUE)
	{
		lapcts16PreboostDctCnt = lapcts16PreboostDctCnt + LAPCT_s16CalcPreboostDctCnt(s16PdtFild1ms, s16PedlTrvlRate1msMmPerSec, lsahbu8PreboostDctFlg);
		lapcts16PreboostDctCnt = L_s16LimitMinMax(lapcts16PreboostDctCnt, 0, 100);

		if ((lapctu8LoopCnt == 4) && (lsahbu8PreboostDctFlg == FALSE ) && (lapcts16PreboostDctCnt >= S16_PREBOOST_DCT_CNT_THR))
		{
			lsahbu8PreboostDctFlg = TRUE;
			lapctu8MaintainCntOnFlg = TRUE;
		}
		else { }
	}
	else
	{
		lsahbu8PreboostDctFlg = FALSE;
		lapcts16PreboostDctCnt = 0;

	}

	if (lapctu8MaintainCntOnFlg == TRUE)
	{
		if (lapctu16PreboostMntCnt < U16_T_50_S)	/* U16_T_50_S : 10sec in 1ms Ctrl */
		{
			lapctu16PreboostMntCnt = lapctu16PreboostMntCnt + 1;
		}
		else
		{
			lapctu16PreboostMntCnt = 0;
			lapctu8MaintainCntOnFlg = FALSE;
			lsahbu8PreboostDctFlg = FALSE;
		}
	}
	else { }

	return lsahbu8PreboostDctFlg;
}

static int16_t LAPCT_s16CalcPreboostDctCnt(int16_t s16PedalTravel1ms, int16_t s16PedalRate1ms, uint8_t u8PreboostDctFlg)
{
	int16_t s16PreboostDctCnt = 0;

	if (u8PreboostDctFlg == FALSE)
	{
		if (s16PedalRate1ms >= S16_PREBOOST_DCT_PEDAL_RATE_THR)
		{
			if ((S16_PREBOOST_DCT_PEDAL_LOW_THR <= s16PedalTravel1ms) && (s16PedalTravel1ms < S16_PREBOOST_DCT_PEDAL_MID_THR))
			{
				s16PreboostDctCnt = 3;
			}
			else if ((S16_PREBOOST_DCT_PEDAL_MID_THR <= s16PedalTravel1ms) && (s16PedalTravel1ms < S16_PREBOOST_DCT_PEDAL_HIGH_THR))
			{
				s16PreboostDctCnt = 5;
			}
			else
			{
				s16PreboostDctCnt = -20;
			}
		}
		else
		{
			if ((s16PedalRate1ms >= 0) && (S16_PREBOOST_DCT_PEDAL_LOW_THR+5 <= s16PedalTravel1ms) && (s16PedalTravel1ms < S16_PREBOOST_DCT_PEDAL_HIGH_THR))
			{
				s16PreboostDctCnt = 1;
			}
			else
			{
				s16PreboostDctCnt = -20;
			}
		}
	}
	else
	{
		if ((s16PedalRate1ms >= 0) && (S16_PREBOOST_DCT_PEDAL_LOW_THR <= s16PedalTravel1ms) && (s16PedalTravel1ms < S16_PREBOOST_DCT_PEDAL_HIGH_THR))
		{
			s16PreboostDctCnt = 1;
		}
		else
		{
			s16PreboostDctCnt = -20;
		}
	}

	return s16PreboostDctCnt;
}
#endif /* (M_PREBOOST_CONTROL == ENABLE) */
#define PCT_1MSCTRL_STOP_SEC_CODE
#include "Pct_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
