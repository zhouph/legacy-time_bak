/**
 * @defgroup LAPCT_DecidePressCtrlMode LAPCT_DecidePressCtrlMode
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LAPCT_DecidePressCtrlMode.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Pct_5msCtrl.h"
#include "LAPCT_DecidePressCtrlMode.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define U8_BOOST_EXIT_WAIT_TIME	      		U8_T_50_MS
#define U8_BOOST_EXIT_WAIT_T_INVALID_TP     U8_T_50_MS
#define S8_MUX_MODE_NONCONTROL			 0
#define S8_MUX_MODE_PCTRL				 1
#define S8_MUX_MODE_PCTRL_TRANSITION	 2
#define S8_MUX_MODE_INHIBITION			-1


/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
	int16_t  s16InhibitFlg;
	uint16_t u16TargetPressValidFlg;
	int16_t  s16TargetPress;
	int16_t  s16DriverIntendedTP;
	int16_t  s16StandstillCtrl;
	int16_t  s16LowSpeedCtrlInhibit;
	int16_t  s16SpeedInvalidFlg;
	int16_t  s16VehicleSpeed;
	int16_t  s16WheelSpeedRL;
	int16_t  s16WheelSpeedRR;
	int16_t  s16PedalReleaseFlg2;
	int16_t  s16DriverIntendToRelBrk;
	uint16_t u16FadeOutBfResBrkFlg;
	uint16_t u16MotorFadeoutEndOK;
	uint16_t u16ValveFadeoutEndOK;
	int16_t  s16AbsActiveFlg;
	int16_t  s16TcsActiveFlg;
	int16_t  s16EscActiveFlg;
	int16_t  s16AebActiveFlg;
	int16_t  s16AvhActiveBrkTP;
	int16_t  s166HsaActiveBrkTP;
	uint16_t u16InAbsAllWheelEntFadeOut;
	uint16_t u16FadeoutState1EndOK;
	uint16_t u16FadeoutState2EndOK;
	uint8_t  u8MultiplexCtrlActive;
	int16_t  s16ReqdPCtrlBoostMod;
	uint8_t u8InitFastApplyFlg;
	uint8_t u8PreboostActFlg;

}stDECIDE_PRESS_CTRL_MODE_INPUT_t;

typedef struct
{
	int8_t   s8PCtrlState;
	int8_t   s8PCtrlBoostMode;
	int8_t   s8PCtrlFadeoutState;
	int16_t  s16InABSCtrlEnabled;
}stDECIDE_PRESS_CTRL_MODE_OUTPUT_t;


typedef struct {
	int16_t 	s16SpeedInvalidFlg;
	int16_t 	s16VehicleSpeed;
	int16_t 	s16WheelSpeedRL;
	int16_t 	s16WheelSpeedRR;
	int16_t 	s16PedalReleaseFlg2;
	int16_t 	s16DriverIntendToRelBrk;
}stDECIDE_PCTRL_VARIABLE_R_t;

typedef struct {
	int16_t 	s16VehicleSpeedState;
	int16_t 	s16InABSCtrlEnabled;
}stDECIDE_PCTRL_VARIABLE_W_t;

typedef struct
{
	uint16_t u16PressCtrlInhibitFlg;
	uint16_t u16FadeOutBfResBrkFlg;
	uint16_t u16TargetPressValidFlg;
	int16_t s16TargetPress;
	int16_t s16DriverIntendedTP;
}stCHECK_FADEOUT_READY_R_t;

typedef struct
{
	int8_t s8PCtrlStateOld;
	uint16_t u16PressCtrlInhibitFlg;
	uint16_t u16TargetPressValidFlg;
	uint16_t u16FadeOutBfResBrkFlg;
	int16_t s16TargetPress;
	int16_t s16DriverIntendedTP;
	uint16_t u16MotorFadeoutEndOK;
	uint16_t u16ValveFadeoutEndOK;
	uint8_t u8PreboostActFlg;
	uint8_t u8PreboostEnterFlg;
	uint16_t u16FadeoutState1EndOK;
}stDECIDE_PCTRL_STATE_R_t;

typedef struct
{
	int16_t s16AbsActiveFlg;
	int16_t s16TcsActiveFlg;
	int16_t s16EscActiveFlg;
	int16_t s16AebActiveFlg;
	uint8_t u8InitFastApplyFlg;
	uint8_t u8MultiplexActive;
	int16_t s16AvhActiveBrkTP;
	int16_t s166HsaActiveBrkTP;
	uint16_t u16InAbsAllWheelEntFadeOut;
	uint8_t u8PreboostActFlg;
}stDECIDE_PCTRL_BOOST_MODE_R_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_CONST_UNSPECIFIED
#include "Pct_MemMap.h"
/** Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Pct_MemMap.h"

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"


#define PCT_5MSCTRL_START_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/** Variable Section (32BIT)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Pct_MemMap.h"

extern uint8_t u8IdbCtrlInhibitFlg;
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

/*Local Use(Local Define Local Use) - Start*/

/*Local Use(Local Define Local Use) - End*/

#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"


#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"


#define PCT_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"


#define PCT_5MSCTRL_START_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/** Variable Section (32BIT)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_CODE
#include "Pct_MemMap.h"
static stDECIDE_PRESS_CTRL_MODE_INPUT_t LAPCT_stGetDecidePCtrlInpInfo(void);
static void LAPCT_SetDecidePCtrlOutpInfo(stDECIDE_PRESS_CTRL_MODE_OUTPUT_t stDecidePressCtrlModeLocVal);
static stDECIDE_PRESS_CTRL_MODE_OUTPUT_t LAPCT_stDecidePressCtrlMain(stDECIDE_PRESS_CTRL_MODE_INPUT_t DecidePressCtrlModeInput);
static int8_t LAPCT_s8DecidePCtrlState(const stDECIDE_PCTRL_STATE_R_t* const stDecidePCtrlState_r);
static int8_t LAPCT_s8PCtrlStateFromReady(int16_t s16TargetPress, uint16_t u16TargetPressValidFlg, uint8_t u8PreboostActFlg);
static int8_t LAPCT_s8PCtrlStateFromPreboost(uint16_t u16PressCtrlInhibitFlg, uint16_t u16FadeOutBfResBrkFlg, int16_t s16TargetPress, uint16_t u16TargetPressValidFlg, uint8_t u8PreboostActFlg);
static int8_t LAPCT_s8PCtrlStateFromFadeout(const stDECIDE_PCTRL_STATE_R_t* const stDecidePCtrlState_r);
static int8_t LAPCT_s8PCtrlStateFromBoost(uint16_t u16PressCtrlInhibitFlg, uint16_t u16FadeOutBfResBrkFlg, uint16_t u16TargetPressValidFlg, int16_t s16TargetPress, int16_t s16DriverIntendedTP);
static int16_t LAPCT_s16CheckVehicleSpeedState(int16_t s16StandstillCtrl, int16_t s16LowSpeedCtrlInhibit, int16_t s16VehicleSpeed);
static int16_t LAPCT_s16CheckEnableInABSCtrl(const stDECIDE_PCTRL_VARIABLE_R_t* const stDecidePCtrlVarialbe_r);
#if (M_BOOST_MODE_SELECT == M_BOOST_MODE_ORIGIN)
static int8_t LAPCT_s8DecidePCtrlMode(int8_t s8PCtrlState, const stDECIDE_PCTRL_VARIABLE_W_t* const stDecidePCtrlVarialbe_w, const stDECIDE_PCTRL_BOOST_MODE_R_t* const stDecidePCtrlBoostMode_r);
static int8_t LAPCT_s8DecidePCtrlBoostMode(const stDECIDE_PCTRL_VARIABLE_W_t* const stDecidePCtrlVarialbe_w, const stDECIDE_PCTRL_BOOST_MODE_R_t* const stDecidePCtrlBoostMode_r);
#else
static int8_t LAPCT_s8DecidePCtrlMode(int8_t s8PCtrlState, int16_t s16VehicleSpeedState, int16_t s16ReqdPCtrlBoostMod);
static int8_t LAPCT_s8DecidePCtrlBoostMode(s16VehicleSpeedState, s16ReqdPCtrlBoostMod);
#endif
static uint16_t LAPCT_u16DecidePressCtrlInhibit(int16_t s16InhibitFlg);
static int8_t LAPCT_s8DecidePCtrlFadeoutState(int8_t s8PCtrlState, uint16_t u16FadeoutState1EndOK, uint16_t u16FadeoutState2EndOK);
static int8_t LAPCT_s8DecideStateWithCond(int8_t s8Condition, int8_t s8TrueValue, int8_t s8FalseValue);
static uint8_t LAPCT_u8PreboostEnterDetect(int8_t s8PCtrlState, uint8_t u8PreboostEnterFlg);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void LAPCT_vDecidePressCtrlMode(void)
{
	stDECIDE_PRESS_CTRL_MODE_INPUT_t 	stDecidePressCtrlModeInput;
	stDECIDE_PRESS_CTRL_MODE_OUTPUT_t 	stDecidePressCtrlModeOutput;

	stDecidePressCtrlModeInput = LAPCT_stGetDecidePCtrlInpInfo();

	stDecidePressCtrlModeOutput = LAPCT_stDecidePressCtrlMain(stDecidePressCtrlModeInput);

	LAPCT_SetDecidePCtrlOutpInfo(stDecidePressCtrlModeOutput);

}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static stDECIDE_PRESS_CTRL_MODE_INPUT_t LAPCT_stGetDecidePCtrlInpInfo(void)
{
	stDECIDE_PRESS_CTRL_MODE_INPUT_t 	stDecidePressCtrlModeLocVal = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

	stDecidePressCtrlModeLocVal.u16TargetPressValidFlg   = (uint16_t)PressCTRLLocalBus.u8PCtrlAct;
	stDecidePressCtrlModeLocVal.s16TargetPress           = (int16_t) PressCTRLLocalBus.s16MuxPwrPistonTarP;
	stDecidePressCtrlModeLocVal.s16DriverIntendedTP      = (int16_t) PressCTRLLocalBus.s16MuxPwrPistonTarP;
	/*For Mux Refactoring*/
	stDecidePressCtrlModeLocVal.s16ReqdPCtrlBoostMod     = (int16_t) PressCTRLLocalBus.s16ReqdPCtrlBoostMod;
	stDecidePressCtrlModeLocVal.s16StandstillCtrl        = (int16_t) Pct_5msCtrlBus.Pct_5msCtrlBaseBrkCtrlModInfo.VehStandStillStFlg;
	stDecidePressCtrlModeLocVal.s16VehicleSpeed          = (int16_t) Pct_5msCtrlBus.Pct_5msCtrlVehSpdFild;
	stDecidePressCtrlModeLocVal.s16WheelSpeedRL          = (int16_t) Pct_5msCtrlBus.Pct_5msCtrlWhlSpdFildInfo.WhlSpdFildReLe;
	stDecidePressCtrlModeLocVal.s16WheelSpeedRR          = (int16_t) Pct_5msCtrlBus.Pct_5msCtrlWhlSpdFildInfo.WhlSpdFildReRi;
	stDecidePressCtrlModeLocVal.s16PedalReleaseFlg2      = (int16_t) Pct_5msCtrlBus.Pct_5msCtrlBrkPedlStatusInfo.PedlReldStFlg2;
	stDecidePressCtrlModeLocVal.s16DriverIntendToRelBrk  = (int16_t) Pct_5msCtrlBus.Pct_5msCtrlBrkPedlStatusInfo.DrvrIntendToRelsBrk;
	stDecidePressCtrlModeLocVal.u16MotorFadeoutEndOK     = (uint16_t) Pct_5msCtrlBus.Pct_5msCtrlMotICtrlFadeOutState;
	stDecidePressCtrlModeLocVal.u16ValveFadeoutEndOK     = (uint16_t) Pct_5msCtrlBus.Pct_5msCtrlIdbVlvActInfo.VlvFadeoutEndOK;
	stDecidePressCtrlModeLocVal.u16FadeoutState1EndOK    = (uint16_t) PressCTRLLocalBus.u16FadeoutState1EndOK;
	stDecidePressCtrlModeLocVal.u16FadeoutState2EndOK    = (uint16_t) Pct_5msCtrlBus.Pct_5msCtrlIdbVlvActInfo.FadeoutSt2EndOk;

	stDecidePressCtrlModeLocVal.s16AvhActiveBrkTP        = (int16_t) Pct_5msCtrlBus.Pct_5msCtrlAvhCtrlInfo.AvhTarP;
	stDecidePressCtrlModeLocVal.s166HsaActiveBrkTP       = (int16_t) Pct_5msCtrlBus.Pct_5msCtrlHsaCtrlInfo.HsaTarP;

	/* Should be Check - Begin*/
	stDecidePressCtrlModeLocVal.s16InhibitFlg            = (int16_t) u8IdbCtrlInhibitFlg;/*Pct_5msCtrlBus.Pct_5msCtrlBaseBrkCtrlModInfo.InhibitFlg;  v1.2.2 공식 배포시 삭제요망 */
	stDecidePressCtrlModeLocVal.s16LowSpeedCtrlInhibit   = Pct_5msCtrlBus.Pct_5msCtrlBaseBrkCtrlModInfo.LowSpdCtrlInhibitFlg;
	stDecidePressCtrlModeLocVal.s16SpeedInvalidFlg       = FALSE;
	stDecidePressCtrlModeLocVal.u16FadeOutBfResBrkFlg    = FALSE;	/* FM */
	/* Should be Check - End*/

    /* Should be Check - End*/
    if ((Pct_5msCtrlBus.Pct_5msCtrlAbsCtrlInfo.AbsActFlg > 0) && (Pct_5msCtrlBus.Pct_5msCtrlAbsCtrlInfo.AbsDefectFlg == FALSE))
    {
    	stDecidePressCtrlModeLocVal.s16AbsActiveFlg = TRUE;
    }
    else
    {
    	stDecidePressCtrlModeLocVal.s16AbsActiveFlg = FALSE;
    }

    if ((Pct_5msCtrlBus.Pct_5msCtrlEscCtrlInfo.EscActFlg > 0) && (Pct_5msCtrlBus.Pct_5msCtrlEscCtrlInfo.EscDefectFlg == FALSE))
    {
    	stDecidePressCtrlModeLocVal.s16EscActiveFlg = TRUE;
    }
    else
    {
    	stDecidePressCtrlModeLocVal.s16EscActiveFlg = FALSE;
    }

    if ((Pct_5msCtrlBus.Pct_5msCtrlTcsCtrlInfo.TcsActFlg > 0) && (Pct_5msCtrlBus.Pct_5msCtrlTcsCtrlInfo.TcsDefectFlg == FALSE))
    {
    	stDecidePressCtrlModeLocVal.s16TcsActiveFlg = TRUE;
    }
    else
    {
    	stDecidePressCtrlModeLocVal.s16TcsActiveFlg = FALSE;
    }

    /* Need Interface */
    stDecidePressCtrlModeLocVal.s16AebActiveFlg = FALSE;
    stDecidePressCtrlModeLocVal.u16InAbsAllWheelEntFadeOut = (uint16_t)Pct_5msCtrlBus.Pct_5msCtrlAbsCtrlInfo.AbsCtrlFadeOutFlg;

    stDecidePressCtrlModeLocVal.u8MultiplexCtrlActive = PressCTRLLocalBus.u8MultiplexCtrlActive;
    stDecidePressCtrlModeLocVal.u8InitFastApplyFlg = ((uint8_t)Pct_5msCtrlBus.Pct_5msCtrlBrkPedlStatusInfo.PanicBrkStFlg | (uint8_t)Pct_5msCtrlBus.Pct_5msCtrlBrkPedlStatus1msInfo.PanicBrkSt1msFlg);
    stDecidePressCtrlModeLocVal.u8PreboostActFlg = Pct_5msCtrlBus.Pct_5msCtrlPreBoostMod;

    return stDecidePressCtrlModeLocVal;
}

static void LAPCT_SetDecidePCtrlOutpInfo(stDECIDE_PRESS_CTRL_MODE_OUTPUT_t stDecidePressCtrlModeLocVal)
{
	PressCTRLLocalBus.s8PCtrlState 	= stDecidePressCtrlModeLocVal.s8PCtrlState;
    PressCTRLLocalBus.s8PCtrlBoostMode	= stDecidePressCtrlModeLocVal.s8PCtrlBoostMode;
    PressCTRLLocalBus.s8PCtrlFadeoutState	= stDecidePressCtrlModeLocVal.s8PCtrlFadeoutState;
    PressCTRLLocalBus.s16InABSCtrlEnabled   = stDecidePressCtrlModeLocVal.s16InABSCtrlEnabled;
    Pct_5msCtrlBus.Pct_5msCtrlPCtrlSt		= stDecidePressCtrlModeLocVal.s8PCtrlState;
    Pct_5msCtrlBus.Pct_5msCtrlPCtrlBoostMod 	= stDecidePressCtrlModeLocVal.s8PCtrlBoostMode;
    Pct_5msCtrlBus.Pct_5msCtrlPCtrlFadeoutSt	= stDecidePressCtrlModeLocVal.s8PCtrlFadeoutState;

}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_stDecidePressCtrlMain
* CALLED BY:            LAPCT_vDecidePressCtrlMode
* Preconditions:        none
* PARAMETER:            DecidePressCtrlModeInput
* RETURN VALUE:         DecidePressCtrlModeOutput
* Description:          Determine Pressure Control mode
********************************************************************************/
static stDECIDE_PRESS_CTRL_MODE_OUTPUT_t LAPCT_stDecidePressCtrlMain(stDECIDE_PRESS_CTRL_MODE_INPUT_t DecidePressCtrlModeInput)
{
	/* Local Variable Definition - Begin */
	static int8_t lapcts8PCtrlStateOld = S8_PRESS_READY;
	static uint8_t lapctu8PreboostEnterFlg = FALSE;

	int8_t s8PCtrlState = S8_PRESS_READY;
	int8_t s8PCtrlFadeoutState = S8_PCTRL_NOT_FADEOUT_STATE;
	int8_t s8PCtrlBoostMode = S8_BOOST_MODE_NOT_CONTROL;
	uint16_t u16PressCtrlInhibitFlg = FALSE;

	stDECIDE_PCTRL_VARIABLE_R_t stDecidePCtrlVarialbe_r = {0,0,0,0,0,0};
	stDECIDE_PCTRL_VARIABLE_W_t stDecidePCtrlVarialbe_w = {0,0};
	stDECIDE_PCTRL_STATE_R_t stDecidePCtrlState_r = {0,0,0,0,0,0,0,0,0,0,0};
	stDECIDE_PCTRL_BOOST_MODE_R_t stDecidePCtrlBoostMode_r = {0,0,0,0,0,0,0,0,0};

	/* Global Input - Begin */
	int16_t s16InhibitFlg               = DecidePressCtrlModeInput.s16InhibitFlg;
	uint16_t u16TargetPressValidFlg     = DecidePressCtrlModeInput.u16TargetPressValidFlg; /*temporarily interfaced lcahbu1AHBOn, should be checked after making arbitrator function*/
	int16_t s16TargetPress              = DecidePressCtrlModeInput.s16TargetPress;
	int16_t s16DriverIntendedTP         = DecidePressCtrlModeInput.s16DriverIntendedTP;
	int16_t s16StandstillCtrl           = DecidePressCtrlModeInput.s16StandstillCtrl;
	int16_t s16LowSpeedCtrlInhibit      = DecidePressCtrlModeInput.s16LowSpeedCtrlInhibit;
	int16_t s16SpeedInvalidFlg          = DecidePressCtrlModeInput.s16SpeedInvalidFlg;
	int16_t s16VehicleSpeed             = DecidePressCtrlModeInput.s16VehicleSpeed;
	int16_t s16WheelSpeedRL             = DecidePressCtrlModeInput.s16WheelSpeedRL;
	int16_t s16WheelSpeedRR             = DecidePressCtrlModeInput.s16WheelSpeedRR;
	int16_t s16PedalReleaseFlg2         = DecidePressCtrlModeInput.s16PedalReleaseFlg2;
	int16_t s16DriverIntendToRelBrk     = DecidePressCtrlModeInput.s16DriverIntendToRelBrk;
	uint16_t u16FadeOutBfResBrkFlg      = DecidePressCtrlModeInput.u16FadeOutBfResBrkFlg;
	uint16_t u16MotorFadeoutEndOK       = DecidePressCtrlModeInput.u16MotorFadeoutEndOK;
	uint16_t u16ValveFadeoutEndOK       = DecidePressCtrlModeInput.u16ValveFadeoutEndOK;
	int16_t s16AbsActiveFlg             = DecidePressCtrlModeInput.s16AbsActiveFlg;
	int16_t s16TcsActiveFlg             = DecidePressCtrlModeInput.s16TcsActiveFlg;
	int16_t s16EscActiveFlg             = DecidePressCtrlModeInput.s16EscActiveFlg;
	int16_t s16AebActiveFlg             = DecidePressCtrlModeInput.s16AebActiveFlg;
	int16_t s16AvhActiveBrkTP           = DecidePressCtrlModeInput.s16AvhActiveBrkTP;
	int16_t s166HsaActiveBrkTP          = DecidePressCtrlModeInput.s166HsaActiveBrkTP;
	uint16_t u16InAbsAllWheelEntFadeOut = DecidePressCtrlModeInput.u16InAbsAllWheelEntFadeOut;
	uint16_t u16FadeoutState1EndOK      = DecidePressCtrlModeInput.u16FadeoutState1EndOK;
	uint16_t u16FadeoutState2EndOK      = DecidePressCtrlModeInput.u16FadeoutState2EndOK;
	uint8_t u8MultiplexActive          = DecidePressCtrlModeInput.u8MultiplexCtrlActive;
	int16_t s16ReqdPCtrlBoostMod        = DecidePressCtrlModeInput.s16ReqdPCtrlBoostMod;
	uint8_t u8InitFastApplyFlg          = DecidePressCtrlModeInput.u8InitFastApplyFlg;
	uint8_t u8PreboostActflg            = DecidePressCtrlModeInput.u8PreboostActFlg;
	/* Global Input - End */

	/* Local Variable for SWC Output Interface - Begin */
	stDECIDE_PRESS_CTRL_MODE_OUTPUT_t stDecidePressCtrlModeLocVal = {0,0,0,0};
	/* Local Variable for SWC Output Interface - End */

	/* Local Variable Definition - End */

	/* Check Vehicle Speed State - Begin */
	stDecidePCtrlVarialbe_w.s16VehicleSpeedState = LAPCT_s16CheckVehicleSpeedState(s16StandstillCtrl, s16LowSpeedCtrlInhibit, s16VehicleSpeed);
	/* Check Vehicle Speed State - End */

	/* Check Enable InABS Control - Begin */
	stDecidePCtrlVarialbe_r.s16SpeedInvalidFlg = s16SpeedInvalidFlg;
	stDecidePCtrlVarialbe_r.s16VehicleSpeed = s16VehicleSpeed;
	stDecidePCtrlVarialbe_r.s16WheelSpeedRL = s16WheelSpeedRL;
	stDecidePCtrlVarialbe_r.s16WheelSpeedRR = s16WheelSpeedRR;
	stDecidePCtrlVarialbe_r.s16PedalReleaseFlg2 = s16PedalReleaseFlg2;
	stDecidePCtrlVarialbe_r.s16DriverIntendToRelBrk = s16DriverIntendToRelBrk;
	stDecidePCtrlVarialbe_w.s16InABSCtrlEnabled = LAPCT_s16CheckEnableInABSCtrl(&stDecidePCtrlVarialbe_r);
	/* Check Enable InABS Control - End */

	u16PressCtrlInhibitFlg = LAPCT_u16DecidePressCtrlInhibit(s16InhibitFlg);

	/* Decide Pressure Control State - Begin */
	stDecidePCtrlState_r.s8PCtrlStateOld = lapcts8PCtrlStateOld;
	stDecidePCtrlState_r.u16PressCtrlInhibitFlg = u16PressCtrlInhibitFlg;
	stDecidePCtrlState_r.u16TargetPressValidFlg = u16TargetPressValidFlg;
	stDecidePCtrlState_r.u16FadeOutBfResBrkFlg = u16FadeOutBfResBrkFlg;
	stDecidePCtrlState_r.s16TargetPress = s16TargetPress;
	stDecidePCtrlState_r.s16DriverIntendedTP = s16DriverIntendedTP;
	stDecidePCtrlState_r.u16MotorFadeoutEndOK = u16MotorFadeoutEndOK;
	stDecidePCtrlState_r.u16ValveFadeoutEndOK = u16ValveFadeoutEndOK;
	stDecidePCtrlState_r.u8PreboostActFlg = u8PreboostActflg;
	stDecidePCtrlState_r.u8PreboostEnterFlg = lapctu8PreboostEnterFlg;
	stDecidePCtrlState_r.u16FadeoutState1EndOK = u16FadeoutState1EndOK;
	s8PCtrlState = LAPCT_s8DecidePCtrlState(&stDecidePCtrlState_r);
	/* Decide Pressure Control State - End */

	/* LAPCT_vDecidePCtrlDegradation(); after decide fail management policy */

	/* Decide Pressure Control Mode - Begin */

	stDecidePCtrlBoostMode_r.s16AbsActiveFlg            = s16AbsActiveFlg;
	stDecidePCtrlBoostMode_r.s16TcsActiveFlg            = s16TcsActiveFlg;
	stDecidePCtrlBoostMode_r.s16EscActiveFlg            = s16EscActiveFlg;
	stDecidePCtrlBoostMode_r.s16AebActiveFlg            = s16AebActiveFlg;
	stDecidePCtrlBoostMode_r.u8InitFastApplyFlg         = u8InitFastApplyFlg;
	stDecidePCtrlBoostMode_r.u8MultiplexActive          = u8MultiplexActive;
	stDecidePCtrlBoostMode_r.s16AvhActiveBrkTP          = s16AvhActiveBrkTP;
	stDecidePCtrlBoostMode_r.s166HsaActiveBrkTP         = s166HsaActiveBrkTP;
	stDecidePCtrlBoostMode_r.u16InAbsAllWheelEntFadeOut = u16InAbsAllWheelEntFadeOut;
	stDecidePCtrlBoostMode_r.u8PreboostActFlg           = u8PreboostActflg;

#if (M_BOOST_MODE_SELECT == M_BOOST_MODE_ORIGIN)
	s8PCtrlBoostMode = LAPCT_s8DecidePCtrlMode(s8PCtrlState, &stDecidePCtrlVarialbe_w, &stDecidePCtrlBoostMode_r);
#else
	s8PCtrlBoostMode = LAPCT_s8DecidePCtrlMode(s8PCtrlState, stDecidePCtrlVarialbe_w.s16VehicleSpeedState, s16ReqdPCtrlBoostMod);
#endif
	/* Decide Pressure Control Mode - End */

	s8PCtrlFadeoutState = LAPCT_s8DecidePCtrlFadeoutState(s8PCtrlState, u16FadeoutState1EndOK, u16FadeoutState2EndOK);

	lapctu8PreboostEnterFlg = LAPCT_u8PreboostEnterDetect(s8PCtrlState, lapctu8PreboostEnterFlg);

	/* Old data update - Begin */
	lapcts8PCtrlStateOld = s8PCtrlState;
	/* Old data update - End */

	/* Output */
	stDecidePressCtrlModeLocVal.s8PCtrlState              = s8PCtrlState;
	stDecidePressCtrlModeLocVal.s8PCtrlBoostMode          = s8PCtrlBoostMode;
	stDecidePressCtrlModeLocVal.s8PCtrlFadeoutState       = s8PCtrlFadeoutState;
	stDecidePressCtrlModeLocVal.s16InABSCtrlEnabled       = stDecidePCtrlVarialbe_w.s16InABSCtrlEnabled;

	return stDecidePressCtrlModeLocVal;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s8DecidePCtrlState
* CALLED BY:            LAPCT_stDecidePressCtrlMain
* Preconditions:        none
* PARAMETER:            *stDecidePCtrlState_r
* RETURN VALUE:         s8PCtrlState
* Description:          Determine Pressure Control mode
********************************************************************************/
static int8_t LAPCT_s8DecidePCtrlState(const stDECIDE_PCTRL_STATE_R_t* const stDecidePCtrlState_r)
{
	int8_t s8PCtrlState = S8_PRESS_READY;

	switch(stDecidePCtrlState_r->s8PCtrlStateOld)
	{
		case S8_PRESS_READY:

			if(stDecidePCtrlState_r->u16PressCtrlInhibitFlg == TRUE)
			{
				s8PCtrlState = S8_PRESS_INHIBIT;
			}
			else
			{
				s8PCtrlState = LAPCT_s8PCtrlStateFromReady(stDecidePCtrlState_r->s16TargetPress, stDecidePCtrlState_r->u16TargetPressValidFlg, stDecidePCtrlState_r->u8PreboostActFlg);
			}

			break;

		case S8_PRESS_PREBOOST:

			s8PCtrlState = LAPCT_s8PCtrlStateFromPreboost(stDecidePCtrlState_r->u16PressCtrlInhibitFlg, stDecidePCtrlState_r->u16FadeOutBfResBrkFlg, stDecidePCtrlState_r->s16TargetPress, stDecidePCtrlState_r->u16TargetPressValidFlg, stDecidePCtrlState_r->u8PreboostActFlg);

			break;

		case S8_PRESS_BOOST:   /*there is a function refer to apply & release state, it's base current(iq) setting part during abs*/

			s8PCtrlState = LAPCT_s8PCtrlStateFromBoost(stDecidePCtrlState_r->u16PressCtrlInhibitFlg, stDecidePCtrlState_r->u16FadeOutBfResBrkFlg, stDecidePCtrlState_r->u16TargetPressValidFlg,  stDecidePCtrlState_r->s16TargetPress, stDecidePCtrlState_r->s16DriverIntendedTP);

			break;

		case S8_PRESS_FADE_OUT:

			if(stDecidePCtrlState_r->u16PressCtrlInhibitFlg == TRUE)
			{
				s8PCtrlState = LAPCT_s8DecideStateWithCond((int8_t)stDecidePCtrlState_r->u16FadeOutBfResBrkFlg, S8_PRESS_FADE_OUT, S8_PRESS_INHIBIT);
			}
			else
			{
				s8PCtrlState = LAPCT_s8PCtrlStateFromFadeout(stDecidePCtrlState_r);
			}

			break;

		case S8_PRESS_INHIBIT:
			s8PCtrlState = LAPCT_s8DecideStateWithCond((int8_t)stDecidePCtrlState_r->u16PressCtrlInhibitFlg, S8_PRESS_INHIBIT, S8_PRESS_READY);

			break;

		default:
			s8PCtrlState = S8_PRESS_READY;
			break;
	}

	return s8PCtrlState;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s8DecideStateWithCond
* CALLED BY:            LAPCT_s8DecidePCtrlState
* Preconditions:        none
* PARAMETER:            s8Condition, s8TrueValue, s8FalseValue
* RETURN VALUE:         s8ReturnValue
* Description:          Condition Check and Return Value
********************************************************************************/
static int8_t LAPCT_s8DecideStateWithCond(int8_t s8Condition, int8_t s8TrueValue, int8_t s8FalseValue)
{
	int8_t s8ReturnValue;

	if(s8Condition == TRUE)
	{
		s8ReturnValue = s8TrueValue;
	}
	else
	{
		s8ReturnValue = s8FalseValue;
	}

	return s8ReturnValue;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s8PCtrlStateFromReady
* CALLED BY:            LAPCT_vDecidePCtrlState
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         s8PCtrlState
* Description:          Check to transfer Ready to Boost mode of pressure control
********************************************************************************/
static int8_t LAPCT_s8PCtrlStateFromReady(int16_t s16TargetPress, uint16_t u16TargetPressValidFlg, uint8_t u8PreboostActFlg)
{
	int8_t s8PCtrlState = S8_PRESS_READY;

   	if((s16TargetPress > S16_PCTRL_PRESS_0_BAR) && (u16TargetPressValidFlg == TRUE))
	{
   		s8PCtrlState = S8_PRESS_BOOST;
	}
   	else if(u8PreboostActFlg == TRUE)
   	{
   		s8PCtrlState = S8_PRESS_PREBOOST;
   	}
	else
	{
		s8PCtrlState = S8_PRESS_READY;
	}

	return s8PCtrlState;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s8PCtrlStateFromReady
* CALLED BY:            LAPCT_vDecidePCtrlState
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         s8PCtrlState
* Description:          Check to transfer Ready to Boost mode of pressure control
********************************************************************************/
static int8_t LAPCT_s8PCtrlStateFromPreboost(uint16_t u16PressCtrlInhibitFlg, uint16_t u16FadeOutBfResBrkFlg, int16_t s16TargetPress, uint16_t u16TargetPressValidFlg, uint8_t u8PreboostActFlg)
{
	int8_t s8PCtrlState = S8_PRESS_PREBOOST;

    if((u16PressCtrlInhibitFlg==1)||(u16FadeOutBfResBrkFlg==1))
    {
    	s8PCtrlState = S8_PRESS_FADE_OUT;
    }
    else if((s16TargetPress > S16_PCTRL_PRESS_0_BAR) && (u16TargetPressValidFlg == TRUE))
	{
   		s8PCtrlState = S8_PRESS_BOOST;
	}
   	else if(u8PreboostActFlg == FALSE)
   	{
   		s8PCtrlState = S8_PRESS_FADE_OUT;
   	}
	else
	{
		s8PCtrlState = S8_PRESS_PREBOOST;
	}

	return s8PCtrlState;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s8PCtrlStateFromFadeout
* CALLED BY:            LAPCT_vDecidePCtrlState
* Preconditions:        none
* PARAMETER:            stDecidePCtrlState_r
* RETURN VALUE:         s8FadeoutToReadySatisfy
* Description:          Check to transfer Fadeout to Ready mode of pressure control
********************************************************************************/
static int8_t LAPCT_s8PCtrlStateFromFadeout(const stDECIDE_PCTRL_STATE_R_t* const stDecidePCtrlState_r)
{
	int8_t s8PCtrlState = S8_PRESS_FADE_OUT;

	if((stDecidePCtrlState_r->s16TargetPress > S16_PCTRL_PRESS_0_BAR) && (stDecidePCtrlState_r->u16TargetPressValidFlg ==1))
	{
		s8PCtrlState = S8_PRESS_BOOST;
	}
	else if ((stDecidePCtrlState_r->u8PreboostEnterFlg == TRUE) && (stDecidePCtrlState_r->u16FadeoutState1EndOK))
	{
		s8PCtrlState = S8_PRESS_READY;
	}
	else if ((stDecidePCtrlState_r->u16MotorFadeoutEndOK == TRUE) && (stDecidePCtrlState_r->u16ValveFadeoutEndOK == TRUE))
	{
		s8PCtrlState = S8_PRESS_READY;
	}
	else
	{
		s8PCtrlState = S8_PRESS_FADE_OUT;
	}

	return s8PCtrlState;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s8PCtrlStateFromBoost
* CALLED BY:            LAPCT_vDecidePCtrlState
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         s8BoostToFadeOutSatisfy
* Description:          Check to transfer Boost to Fadeout mode of pressure control
********************************************************************************/
static int8_t LAPCT_s8PCtrlStateFromBoost(uint16_t u16PressCtrlInhibitFlg, uint16_t u16FadeOutBfResBrkFlg, uint16_t u16TargetPressValidFlg, int16_t s16TargetPress, int16_t s16DriverIntendedTP)
{
	static uint8_t lapctu8BoostEndCnt = 0;
	static uint8_t lapct8PressCtrlEndforFailCnt = 0;
	int8_t s8PCtrlState = S8_PRESS_BOOST;

    if((u16PressCtrlInhibitFlg==1)||(u16FadeOutBfResBrkFlg==1))
    {
    	s8PCtrlState = S8_PRESS_FADE_OUT;
        lapctu8BoostEndCnt = 0;
        lapct8PressCtrlEndforFailCnt = 0;
    }
    else
    {
		if(u16TargetPressValidFlg ==1)
		{
			if((s16TargetPress == S16_PCTRL_PRESS_0_BAR) && (s16DriverIntendedTP == S16_MPRESS_0_BAR))
	        {
	        	lapctu8BoostEndCnt = lapctu8BoostEndCnt + 1;

	        	if(lapctu8BoostEndCnt >= U8_BOOST_EXIT_WAIT_TIME)
	        	{
	        		s8PCtrlState = S8_PRESS_FADE_OUT;
	        		lapctu8BoostEndCnt = 0;
	        	}
	        	else { /* Do Nothing */ }
			}
	        else
	        {
				lapctu8BoostEndCnt = 0;
	        }

	        lapct8PressCtrlEndforFailCnt = 0;
	    }
	    else
	    {
	    	lapct8PressCtrlEndforFailCnt = lapct8PressCtrlEndforFailCnt + 1;

	    	if(lapct8PressCtrlEndforFailCnt >= U8_BOOST_EXIT_WAIT_T_INVALID_TP)
	    	{
	    		s8PCtrlState = S8_PRESS_FADE_OUT;
	    		lapctu8BoostEndCnt = 0;
	    		lapct8PressCtrlEndforFailCnt = 0;
	    	}
	    	else { /* Do Nothing */ }
	    }
	}

    return s8PCtrlState;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_u16DecidePressCtrlInhibit
* CALLED BY:            LAPCT_stDecidePressCtrlMain
* Preconditions:        none
* PARAMETER:            s16InhibitFlg
* RETURN VALUE:         u16PressCtrlInhibitFlg
* Description:          Decide inhibit of pressure control mode
********************************************************************************/
static uint16_t LAPCT_u16DecidePressCtrlInhibit(int16_t s16InhibitFlg)
{
	uint16_t u16PressCtrlInhibitFlg = FALSE;

	if(s16InhibitFlg==1)
	{
		u16PressCtrlInhibitFlg = TRUE;
	}
	else
	{
		u16PressCtrlInhibitFlg = FALSE;
	}

	return u16PressCtrlInhibitFlg;
}
/*******************************************************************************
* FUNCTION NAME:        LAPCT_s8DecidePCtrlMode
* CALLED BY:            LAPCT_stDecidePressCtrlMain
* Preconditions:        none
* PARAMETER:            s8PCtrlState, *stDecidePCtrlVarialbe_w, *stDecidePCtrlBoostMode_r
* RETURN VALUE:         s8PCtrlBoostMode
* Description:          Determine Pressure Control Boosting mode
********************************************************************************/
#if (M_BOOST_MODE_SELECT == M_BOOST_MODE_ORIGIN)
static int8_t LAPCT_s8DecidePCtrlMode(int8_t s8PCtrlState, const stDECIDE_PCTRL_VARIABLE_W_t* const stDecidePCtrlVarialbe_w, const stDECIDE_PCTRL_BOOST_MODE_R_t* const stDecidePCtrlBoostMode_r)
{
	int8_t s8PCtrlBoostMode = S8_BOOST_MODE_NOT_CONTROL;

	if((s8PCtrlState == S8_PRESS_PREBOOST) || (s8PCtrlState == S8_PRESS_BOOST) || (s8PCtrlState == S8_PRESS_FADE_OUT))
	{
		s8PCtrlBoostMode = LAPCT_s8DecidePCtrlBoostMode(stDecidePCtrlVarialbe_w, stDecidePCtrlBoostMode_r);
	}
	else
	{
		s8PCtrlBoostMode = S8_BOOST_MODE_NOT_CONTROL;
	}

	return s8PCtrlBoostMode;
}
#else
static int8_t LAPCT_s8DecidePCtrlMode(int8_t s8PCtrlState, int16_t s16VehicleSpeedState, int16_t s16ReqdPCtrlBoostMod)
{
	int8_t s8PCtrlBoostMode = S8_BOOST_MODE_NOT_CONTROL;

	if((s8PCtrlState == S8_PRESS_BOOST) || (s8PCtrlState == S8_PRESS_FADE_OUT))
	{
		s8PCtrlBoostMode = LAPCT_s8DecidePCtrlBoostMode(s16VehicleSpeedState, s16ReqdPCtrlBoostMod);
	}
	else
	{
		s8PCtrlBoostMode = S8_BOOST_MODE_NOT_CONTROL;
	}

	return s8PCtrlBoostMode;
}
#endif
/*******************************************************************************
* FUNCTION NAME:        LAPCT_s8DecidePCtrlFadeoutState
* CALLED BY:            LAPCT_stDecidePressCtrlMain
* Preconditions:        none
* PARAMETER:            s8PCtrlState
* RETURN VALUE:         s8PCtrlFadeoutState
* Description:          Determine Pressure Control Fadeout State
********************************************************************************/
static int8_t LAPCT_s8DecidePCtrlFadeoutState(int8_t s8PCtrlState, uint16_t u16FadeoutState1EndOK, uint16_t u16FadeoutState2EndOK)
{
	static int8_t lapcts8PCtrlFadeoutStateOld = S8_PCTRL_NOT_FADEOUT_STATE;
	int8_t s8PCtrlFadeoutState = S8_PCTRL_NOT_FADEOUT_STATE;

	if(s8PCtrlState == S8_PRESS_FADE_OUT)
	{
		switch(lapcts8PCtrlFadeoutStateOld)
		{
			case S8_PCTRL_NOT_FADEOUT_STATE :

				s8PCtrlFadeoutState = S8_PCTRL_FADEOUT_STATE_1;

			break;

		    case S8_PCTRL_FADEOUT_STATE_1:
		    	/* F217 미확인
		    	if(u16FadeoutState2EndOK == 1)
		    	{
		    		s8PCtrlFadeoutState = S8_PCTRL_FADEOUT_STATE_3;
		    	}
		    	else if(u16FadeoutState1EndOK == 1)
		    	 */
		    	if(u16FadeoutState1EndOK == 1)
		    	{
		    		s8PCtrlFadeoutState = S8_PCTRL_FADEOUT_STATE_2;
		    	}
		    	else
		    	{
		    		s8PCtrlFadeoutState = S8_PCTRL_FADEOUT_STATE_1;
		    	}

		    break;

		    case S8_PCTRL_FADEOUT_STATE_2:

		    	if(u16FadeoutState2EndOK == 1)
		    	{
		    		s8PCtrlFadeoutState = S8_PCTRL_FADEOUT_STATE_3;
		    	}
		    	else
		    	{
		    		s8PCtrlFadeoutState = S8_PCTRL_FADEOUT_STATE_2;
		    	}


		    break;

		    case S8_PCTRL_FADEOUT_STATE_3:

		    	s8PCtrlFadeoutState = S8_PCTRL_FADEOUT_STATE_3;

		    break;

		    default:

		    	s8PCtrlFadeoutState = S8_PCTRL_NOT_FADEOUT_STATE;

		    break;
		}
	}
	else
	{
		s8PCtrlFadeoutState = S8_PCTRL_NOT_FADEOUT_STATE;
	}

	lapcts8PCtrlFadeoutStateOld = s8PCtrlFadeoutState;

	return s8PCtrlFadeoutState;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s16CheckVehicleSpeedState
* CALLED BY:            LAPCT_stDecidePressCtrlMain
* Preconditions:        none
* PARAMETER:            s16StandstillCtrl, s16LowSpeedCtrlInhibit, s16VehicleSpeed
* RETURN VALUE:         s16SpeedState
* Description:          Check vehicle speed state
********************************************************************************/
static int16_t LAPCT_s16CheckVehicleSpeedState(int16_t s16StandstillCtrl, int16_t s16LowSpeedCtrlInhibit, int16_t s16VehicleSpeed)
{
	int16_t s16SpeedState = S16_SPEED_STATE_NORMAL;

	if(s16StandstillCtrl == 1)
	{
		s16SpeedState = S16_SPEED_STATE_STOP;
	}
	else if(s16LowSpeedCtrlInhibit == 0)
	{
		if(s16VehicleSpeed <= U8_VEHICLE_CREEPING_SPEED)
		{
			s16SpeedState = S16_SPEED_STATE_CREEP;
		}
		else
		{
			s16SpeedState = S16_SPEED_STATE_NORMAL;
		}
	}
	else
	{
		s16SpeedState = S16_SPEED_STATE_NORMAL;
	}

	return s16SpeedState;
}


 /*******************************************************************************
 * FUNCTION NAME:        LAPCT_s16CheckEnableInABSCtrl
 * CALLED BY:            LAPCT_stDecidePressCtrlMain
 * Preconditions:        none
 * PARAMETER:            *stDecidePCtrlVarialbe_r
 * RETURN VALUE:         s16InABSCtrlEnabled
 * Description:          Check whether vehicle is ABS braking  or not
 ********************************************************************************/
static int16_t LAPCT_s16CheckEnableInABSCtrl(const stDECIDE_PCTRL_VARIABLE_R_t* const stDecidePCtrlVarialbe_r)
{
	static int8_t  lapcts8InABSCtrlLimitSpdCnt = 0;
	int16_t s16InABSCtrlEnabled = FALSE;

    if((stDecidePCtrlVarialbe_r->s16SpeedInvalidFlg==0) && (stDecidePCtrlVarialbe_r->s16VehicleSpeed <= S16_ABS_LIMIT_SPEED)
    && (stDecidePCtrlVarialbe_r->s16WheelSpeedRL <= S16_ABS_LIMIT_SPEED) && (stDecidePCtrlVarialbe_r->s16WheelSpeedRR <= S16_ABS_LIMIT_SPEED)) /* Non-Driven wheel Speed Condition */
    {
        if(lapcts8InABSCtrlLimitSpdCnt < U8_T_200_MS)
        {
	        lapcts8InABSCtrlLimitSpdCnt = lapcts8InABSCtrlLimitSpdCnt + 1;
        }
    }
    else
    {
    	lapcts8InABSCtrlLimitSpdCnt = 0;
    }

    if(lapcts8InABSCtrlLimitSpdCnt >= U8_T_100_MS)
    {
    	s16InABSCtrlEnabled = FALSE;
    }
    else if(stDecidePCtrlVarialbe_r->s16PedalReleaseFlg2==1)
    {
    	s16InABSCtrlEnabled = FALSE;
    }
    else if(stDecidePCtrlVarialbe_r->s16DriverIntendToRelBrk==1)
    {
    	s16InABSCtrlEnabled = FALSE;
    }
    else
    {
    	s16InABSCtrlEnabled = TRUE;
    }

    return s16InABSCtrlEnabled;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s8DecidePCtrlBoostMode
* CALLED BY:            LAPCT_s8DecidePCtrlMode
* Preconditions:        none
* PARAMETER:            *stDecidePCtrlVarialbe_w, *stDecidePCtrlBoostMode_r
* RETURN VALUE:         s8PctrlBoostMode
* Description:          Determine Pressure Control Boost Mode
********************************************************************************/
#if (M_BOOST_MODE_SELECT == M_BOOST_MODE_ORIGIN)
static int8_t LAPCT_s8DecidePCtrlBoostMode(const stDECIDE_PCTRL_VARIABLE_W_t* const stDecidePCtrlVarialbe_w, const stDECIDE_PCTRL_BOOST_MODE_R_t* const stDecidePCtrlBoostMode_r)
{
	int8_t s8PctrlBoostMode = S8_BOOST_MODE_NORMAL;

	if(stDecidePCtrlBoostMode_r->s16AbsActiveFlg == TRUE)
	{
		if(stDecidePCtrlBoostMode_r->u16InAbsAllWheelEntFadeOut == TRUE)
		{
			s8PctrlBoostMode = S8_BOOST_MODE_ABS_TO_NORMAL;
		}
		else
		{
			s8PctrlBoostMode = S8_BOOST_MODE_IN_ABS;
		}
	}
	else if((stDecidePCtrlBoostMode_r->s16TcsActiveFlg == TRUE) || (stDecidePCtrlBoostMode_r->s16EscActiveFlg == TRUE))
	{
		s8PctrlBoostMode = S8_BOOST_MODE_SAFETY;
	}
	else if (stDecidePCtrlBoostMode_r->u8InitFastApplyFlg == TRUE)
	{
		s8PctrlBoostMode = S8_BOOST_MODE_QUICK;
	}
	else if (stDecidePCtrlBoostMode_r->s16AebActiveFlg == TRUE)
	{
		s8PctrlBoostMode = S8_BOOST_MODE_AEB;
	}
	else if((stDecidePCtrlBoostMode_r->s16AvhActiveBrkTP > 0) || (stDecidePCtrlBoostMode_r->s166HsaActiveBrkTP > 0))	/* TODO : condition check : Consider - Act & defective flag */
	{
		s8PctrlBoostMode = S8_BOOST_MODE_ACTIVE;
	}
	else if(stDecidePCtrlBoostMode_r->u8PreboostActFlg == TRUE)
	{
		s8PctrlBoostMode = S8_BOOST_MODE_RESPONSE_L12;
	}
	else if(stDecidePCtrlVarialbe_w->s16VehicleSpeedState == S16_SPEED_STATE_STOP)
	{
		s8PctrlBoostMode = S8_BOOST_MODE_STOP;
	}
	else if(stDecidePCtrlVarialbe_w->s16VehicleSpeedState == S16_SPEED_STATE_CREEP)
	{
		s8PctrlBoostMode = S8_BOOST_MODE_CREEP;
	}
	else if(stDecidePCtrlVarialbe_w->s16VehicleSpeedState == S16_SPEED_STATE_NORMAL)
	{
		s8PctrlBoostMode = S8_BOOST_MODE_NORMAL;
	}
	else
	{
		s8PctrlBoostMode = S8_BOOST_MODE_NORMAL;
	}

	return s8PctrlBoostMode;
}
#else
static int8_t LAPCT_s8DecidePCtrlBoostMode(s16VehicleSpeedState, s16ReqdPCtrlBoostMod)
{
	int8_t s8PctrlBoostMode = S8_BOOST_MODE_RESPONSE_L5;

	switch(s16ReqdPCtrlBoostMod)
	{
	case S8_BOOST_MODE_REQ_L0	: 									/* ABS Torque Control Mode 										*/
		s8PctrlBoostMode = S8_BOOST_MODE_RESPONSE_L0;
		break;

	case S8_BOOST_MODE_REQ_L1	: 									/* Quick Braking , AEB : 400bar/s 								*/
		s8PctrlBoostMode = S8_BOOST_MODE_RESPONSE_L1;
		break;

	case S8_BOOST_MODE_REQ_L2	: 									/* ESC oversteer (Include ROP), ABS, TCS_1_mode : 300bar/s 		*/
		s8PctrlBoostMode = S8_BOOST_MODE_RESPONSE_L2;
		break;

	case S8_BOOST_MODE_REQ_L3	: 									/* TCS_2_mode, TSP, ESC understeer : 200bar/s  					*/
		s8PctrlBoostMode = S8_BOOST_MODE_RESPONSE_L3;
		break;

	case S8_BOOST_MODE_REQ_L4	: 									/* Decel control : SCC : 200 bar/s dependent on speed			*/
		s8PctrlBoostMode = S8_BOOST_MODE_RESPONSE_L4;
		break;

	case S8_BOOST_MODE_REQ_L5	:

		if(s16VehicleSpeedState == S16_SPEED_STATE_NORMAL)			/* Decel control : BBC, RBC : 200 bar/s Normal Speed			*/
		{
			s8PctrlBoostMode = S8_BOOST_MODE_RESPONSE_L5;
		}
		else if(s16VehicleSpeedState == S16_SPEED_STATE_CREEP)		/* Decel control : BBC, RBC : 200 bar/s Low Speed				*/
		{
			s8PctrlBoostMode = S8_BOOST_MODE_RESPONSE_L6;
		}
		else 														/* Decel control : BBC, RBC : 200 bar/s Stop Speed				*/
		{/*(s16VehicleSpeedState == S16_SPEED_STATE_STOP)*/
			s8PctrlBoostMode = S8_BOOST_MODE_RESPONSE_L7;
		}

		break;

	case S8_BOOST_MODE_REQ_L6	:									/* CBC SLS, EBD 200bar/s 감압 제어 function							*/
		s8PctrlBoostMode = S8_BOOST_MODE_RESPONSE_L8;
		break;

	case S8_BOOST_MODE_REQ_L7	:									/* TCS_4_mode, Hold function(HSA, AVH)의 Hold 100bar/s			*/
		s8PctrlBoostMode = S8_BOOST_MODE_RESPONSE_L9;
		break;

	case S8_BOOST_MODE_REQ_L8	:									/* Hold function의 압력 release										*/
		s8PctrlBoostMode = S8_BOOST_MODE_RESPONSE_L10;
		break;
	case S8_BOOST_MODE_REQ_L9	:
		s8PctrlBoostMode = S8_BOOST_MODE_RESPONSE_L13;				/* ABS Pressure COntrol */
		break;
	}

		return s8PctrlBoostMode;
}
#endif

/*******************************************************************************
* FUNCTION NAME:        LAPCT_u8PreboostEnterDetect
* CALLED BY:            LAPCT_vDecidePressCtrlMode
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         u8PreboostEnterFlg
* Description:          Determine Preboost State Enter
********************************************************************************/
static uint8_t LAPCT_u8PreboostEnterDetect(int8_t s8PCtrlState, uint8_t u8PreboostEnterFlg)
{

	if (s8PCtrlState == S8_PRESS_PREBOOST)
	{
		u8PreboostEnterFlg = TRUE;
	}
	else
	{
		if ((s8PCtrlState == S8_PRESS_READY) || (s8PCtrlState == S8_PRESS_BOOST))
		{
			u8PreboostEnterFlg = FALSE;
		}
		else { }
	}

	return u8PreboostEnterFlg;
}


#define PCT_5MSCTRL_STOP_SEC_CODE
#include "Pct_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
