/**
 * @defgroup LAIDB_DecideValveCtrlMode LAIDB_DecideValveCtrlMode
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LAIDB_DecideValveCtrlMode.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "../HDR/Pct_5msCtrl.h"
#include "LAPCT_DecideStkRcvrTime.h"
#include "MTR/LAPCT_LoadMapData.h"

/*#include <Mando_Std_Types.h>*/

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/


/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
	int16_t lapcts16StkRcvrEscAllowedTime;
	int16_t lapcts16StkRcvrTcsAllowedTime;
	int16_t lapcts16StkRcvrAbsAllowedTime;
}GetStkRcvrTimeInfo_t;

typedef struct
{
    int16_t lapcts16StkRcvrFinalAllowedTime;
}SetStkRcvrTimeInfo_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_CONST_UNSPECIFIED
#include "Pct_MemMap.h"
/** Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Pct_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_START_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/** Variable Section (32BIT)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


FAL_STATIC GetStkRcvrTimeInfo_t				GetStkRcvrTime;
FAL_STATIC SetStkRcvrTimeInfo_t				SetStkRcvrTime;


#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
#define PCT_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PCT_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
#define PCT_5MSCTRL_START_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/** Variable Section (32BIT)**/


#define PCT_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Pct_MemMap.h"


/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_CODE
#include "Pct_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/

static void LAPCT_vDecideStkRcvrTime(void);
static int16_t lapcts16CheckCompareAllowedTime(int16_t s16FunctionAllowedTime, int16_t s16CurrentTempAllowedTime);

static void LAPCT_vCheckStkRcvrTimeInput(void);
static void LAPCT_vCheckStkRcvrTimeOutput(void);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void LAPCT_vCallStrkRcvrTime(void)
{
	LAPCT_vCheckStkRcvrTimeInput();

	LAPCT_vDecideStkRcvrTime();

    LAPCT_vCheckStkRcvrTimeOutput();
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static void LAPCT_vCheckStkRcvrTimeInput(void)
{
	GetStkRcvrTime.lapcts16StkRcvrEscAllowedTime = (int16_t) Pct_5msCtrlBus.Pct_5msCtrlStkRecvryCtrlIfInfo.StkRcvrEscAllowedTime/5;
	GetStkRcvrTime.lapcts16StkRcvrTcsAllowedTime = (int16_t) Pct_5msCtrlBus.Pct_5msCtrlStkRecvryCtrlIfInfo.StkRcvrTcsAllowedTime/5;
	GetStkRcvrTime.lapcts16StkRcvrAbsAllowedTime = (int16_t) Pct_5msCtrlBus.Pct_5msCtrlStkRecvryCtrlIfInfo.StkRcvrAbsAllowedTime/5;
}

static void LAPCT_vCheckStkRcvrTimeOutput(void)
{
	PressCTRLLocalBus.s16StkRcvrFinalAllowedTime = SetStkRcvrTime.lapcts16StkRcvrFinalAllowedTime;
}
/******************************************************************************
* FUNCTION NAME:      LCABS_vDecideStrkRcvrTime
* CALLED BY:          LCABS_vStrokeRecoveryMain()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Decide Stroke Recovery Limited Time
******************************************************************************/
static void LAPCT_vDecideStkRcvrTime(void)
{
	int16_t s16TempAllowedTime = 0;

	if((GetStkRcvrTime.lapcts16StkRcvrEscAllowedTime == S16_RECOVERY_END_REQ_TIME_PCT5MS) || (GetStkRcvrTime.lapcts16StkRcvrTcsAllowedTime == S16_RECOVERY_END_REQ_TIME_PCT5MS)
		|| (GetStkRcvrTime.lapcts16StkRcvrAbsAllowedTime == S16_RECOVERY_END_REQ_TIME_PCT5MS))
	{
		SetStkRcvrTime.lapcts16StkRcvrFinalAllowedTime = S16_RECOVERY_END_REQ_TIME_PCT5MS;
	}
	else
	{
		s16TempAllowedTime = lapcts16CheckCompareAllowedTime(GetStkRcvrTime.lapcts16StkRcvrEscAllowedTime,s16TempAllowedTime);
		s16TempAllowedTime = lapcts16CheckCompareAllowedTime(GetStkRcvrTime.lapcts16StkRcvrTcsAllowedTime,s16TempAllowedTime);
		s16TempAllowedTime = lapcts16CheckCompareAllowedTime(GetStkRcvrTime.lapcts16StkRcvrAbsAllowedTime,s16TempAllowedTime);

		if(s16TempAllowedTime >= S16_RECOVERY_MIN_ACT_TIME_PCT5MS)
		{
			SetStkRcvrTime.lapcts16StkRcvrFinalAllowedTime = s16TempAllowedTime;
		}
		else
		{
			SetStkRcvrTime.lapcts16StkRcvrFinalAllowedTime = S16_RECOVERY_DONT_CARE_TIME_PCT5MS;
		}
	}
}

/******************************************************************************
* FUNCTION NAME:      lapcts16CheckCompareAllowedTime
* CALLED BY:          LAPCT_vDecideStkRcvrTime()
* Preconditions:      none
* PARAMETER:		  s16FunctionAllowedTime, s16CurrentTempAllowedTime
* RETURN VALUE:       s16TempAllowedTime
* Description:        Compare stroke recovery allowed time
******************************************************************************/
static int16_t lapcts16CheckCompareAllowedTime(int16_t s16FunctionAllowedTime, int16_t s16CurrentTempAllowedTime)
{
	int16_t s16TempAllowedTime = 0;

	if(s16FunctionAllowedTime >= S16_RECOVERY_MIN_ACT_TIME_PCT5MS)
	{
		if(s16CurrentTempAllowedTime >= S16_RECOVERY_MIN_ACT_TIME_PCT5MS)
		{
			s16TempAllowedTime = (s16FunctionAllowedTime > s16CurrentTempAllowedTime) ? s16FunctionAllowedTime : s16CurrentTempAllowedTime;
		}
		else
		{
			s16TempAllowedTime = s16FunctionAllowedTime;
		}
	}
	else
	{
		s16TempAllowedTime = s16CurrentTempAllowedTime;
	}

	return s16TempAllowedTime;
}

#define PCT_CTRL_STOP_SEC_CODE
#include "Pct_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
