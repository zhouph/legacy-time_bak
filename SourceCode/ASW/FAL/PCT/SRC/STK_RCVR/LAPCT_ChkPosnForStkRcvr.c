/**
 * @defgroup LAIDB_DecideValveCtrlMode LAIDB_DecideValveCtrlMode
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LAIDB_DecideValveCtrlMode.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "../HDR/Pct_5msCtrl.h"
#include "LAPCT_ChkPosnForStkRcvr.h"
#include "MTR/LAPCT_LoadMapData.h"

/*#include <Mando_Std_Types.h>*/

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
	int16_t lapcts16EstimatedPressFL;
	int16_t lapcts16EstimatedPressFR;
	int16_t lapcts16EstimatedPressRL;
	int16_t lapcts16EstimatedPressRR;
	int32_t lapcts32StkPosn;
}GetPosnForStkRcvrInfo_t;

typedef struct
{
    int16_t lapcts16AllowedRcvrStk_mm;
    int16_t lapcts16FwdRcvrMaxPosn_mm;
    int16_t lapcts16BackwRcvrMinPosn_mm;
    int16_t lapcts16StkPosn_mm;
}SetPosnForStkRcvrInfo_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
=============================================================================*/
#define PCT_5MSCTRL_START_SEC_CONST_UNSPECIFIED
#include "Pct_MemMap.h"
/** Constant Section (UNSPECIFIED)**/

/*==============================================================================
*                  LOCAL CONSTANT DEFINITIONS
=============================================================================*/
#define PCT_5MSCTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Pct_MemMap.h"
/*==============================================================================
*                  GLOBAL VARIABLE DEFINITIONS
=============================================================================*/
#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_START_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/** Variable Section (32BIT)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/*==============================================================================
*                  LOCAL VARIABLE DEFINITIONS
=============================================================================*/
#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"


FAL_STATIC int32_t lapcts32TatEstStkPosnOfWhlP;
FAL_STATIC int32_t lapcts32AllowedRcvrStk;
FAL_STATIC int32_t lapcts32FwdMaxPosnForRcvr;
FAL_STATIC int32_t lapcts32BackwMinPosnForRcvr;

FAL_STATIC GetPosnForStkRcvrInfo_t		GetPosnForStkRcvr;
FAL_STATIC SetPosnForStkRcvrInfo_t		SetPosnForStkRcvr;

/* Local Use(Local Define Local Use) - End */

#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
#define PCT_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PCT_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
#define PCT_5MSCTRL_START_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/** Variable Section (32BIT)**/


#define PCT_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/*==============================================================================
*                  GLOBAL FUNCTION PROTOTYPES
=============================================================================*/
#define PCT_5MSCTRL_START_SEC_CODE
#include "Pct_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/

static int32_t LAPCT_s32CalcStkByEstimdWhlP(int16_t s16Pressure, uint16_t u16WheelVolumeState);
static int32_t LAPCT_s32CalcEstStkFromEstP(int16_t s16PressFL, int16_t s16PressFR, int16_t s16PressRL, int16_t s16PressRR);
static int32_t LAPCT_s32CalcAllowedRecrStk(int32_t s32StkPosn, int32_t s32TatEstStkPosnOfWhlP);
static int32_t LAPCT_s32CalcFwdMaxPosnForRcvr(int32_t s32TatEstStkPosnOfWhlP);
static void LAPCT_vChkPosnInfoForStkRcvr(void);
static void LAPCT_vChkPosnForStkRcvrInput(void);
static void LAPCT_vChkPosnForStkRcvrOutput(void);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void LAPCT_vCallPosnForStkRcvr(void)
{
	LAPCT_vChkPosnForStkRcvrInput();

	LAPCT_vChkPosnInfoForStkRcvr();

	LAPCT_vChkPosnForStkRcvrOutput();
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static void LAPCT_vChkPosnInfoForStkRcvr(void)
{
	int16_t s16EstimatedPressFL = GetPosnForStkRcvr.lapcts16EstimatedPressFL;
	int16_t s16EstimatedPressFR = GetPosnForStkRcvr.lapcts16EstimatedPressFR;
	int16_t s16EstimatedPressRL = GetPosnForStkRcvr.lapcts16EstimatedPressRL;
	int16_t s16EstimatedPressRR = GetPosnForStkRcvr.lapcts16EstimatedPressRR;

	lapcts32TatEstStkPosnOfWhlP = LAPCT_s32CalcEstStkFromEstP(s16EstimatedPressFL, s16EstimatedPressFR, s16EstimatedPressRL, s16EstimatedPressRR);

	lapcts32BackwMinPosnForRcvr = lapcts32TatEstStkPosnOfWhlP;

	lapcts32AllowedRcvrStk = LAPCT_s32CalcAllowedRecrStk(GetPosnForStkRcvr.lapcts32StkPosn, lapcts32TatEstStkPosnOfWhlP);

	lapcts32FwdMaxPosnForRcvr = LAPCT_s32CalcFwdMaxPosnForRcvr(lapcts32TatEstStkPosnOfWhlP);
}


static void LAPCT_vChkPosnForStkRcvrInput(void)
{
	GetPosnForStkRcvr.lapcts16EstimatedPressFL = (int16_t) Pct_5msCtrlBus.Pct_5msCtrlEstimdWhlPInfo.FrntLeEstimdWhlP;
	GetPosnForStkRcvr.lapcts16EstimatedPressFR = (int16_t) Pct_5msCtrlBus.Pct_5msCtrlEstimdWhlPInfo.FrntRiEstimdWhlP;
	GetPosnForStkRcvr.lapcts16EstimatedPressRL = (int16_t) Pct_5msCtrlBus.Pct_5msCtrlEstimdWhlPInfo.ReLeEstimdWhlP;
	GetPosnForStkRcvr.lapcts16EstimatedPressRR = (int16_t) Pct_5msCtrlBus.Pct_5msCtrlEstimdWhlPInfo.ReRiEstimdWhlP;
	GetPosnForStkRcvr.lapcts32StkPosn      = Pct_5msCtrlBus.Pct_5msCtrlMotRotgAgSigInfo.StkPosnMeasd;
}

static void LAPCT_vChkPosnForStkRcvrOutput(void)
{
	int32_t s32PosnPulse = L_s32LimitMinMax(GetPosnForStkRcvr.lapcts32StkPosn, 0, S32_POSITION_PULSE_MAX);

	SetPosnForStkRcvr.lapcts16StkPosn_mm = (int16_t)((s32PosnPulse*100)/S16_POSN_1_MM_TO_PULSE);
	SetPosnForStkRcvr.lapcts16AllowedRcvrStk_mm = (int16_t)((lapcts32AllowedRcvrStk*100)/S16_POSN_1_MM_TO_PULSE);
	SetPosnForStkRcvr.lapcts16FwdRcvrMaxPosn_mm = (int16_t)((lapcts32FwdMaxPosnForRcvr*100)/S16_POSN_1_MM_TO_PULSE);
	SetPosnForStkRcvr.lapcts16BackwRcvrMinPosn_mm = (int16_t)((lapcts32BackwMinPosnForRcvr*100)/S16_POSN_1_MM_TO_PULSE);

	PressCTRLLocalBus.s16AllowedRcvrStk_mm = SetPosnForStkRcvr.lapcts16AllowedRcvrStk_mm;
	PressCTRLLocalBus.s16FwdRcvrMaxPosn_mm = SetPosnForStkRcvr.lapcts16FwdRcvrMaxPosn_mm;
	PressCTRLLocalBus.s16BackwRcvrMinPosn_mm = SetPosnForStkRcvr.lapcts16BackwRcvrMinPosn_mm;
	PressCTRLLocalBus.s16StkPosn_mm = SetPosnForStkRcvr.lapcts16StkPosn_mm;
}


/*******************************************************************************
* FUNCTION NAME:        LAPCT_s32CalcStkByEstimdWhlP
* CALLED BY:
* Preconditions:        none
* PARAMETER:            s16Pressure, u16WheelVolumeState
* RETURN VALUE:         s32Stroke
* Description:          Extract Absolute Stroke based on Stroke-Pressure Map by estimated wheel pressure
*******************************************************************************/
static int32_t LAPCT_s32CalcStkByEstimdWhlP(int16_t s16Pressure, uint16_t u16WheelVolumeState)
{
	int32_t s32Stroke = 0;

	s32Stroke = LAPCT_s32CalcStorkeByPress(s16Pressure, u16WheelVolumeState);

	s32Stroke = (s32Stroke*S16_ESTIMD_STOROKE_GAIN_TEMP/S8_PERCENT_100_PRO);

	return (s32Stroke);
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s32CalcEstStkFromEstP
* CALLED BY:            LAPCT_vChkPosnInfoForStkRcvr
* Preconditions:        none
* PARAMETER:            s16PressFL, s16PressFR, s16PressRL, s16PressRR
* RETURN VALUE:         s32TotalEstStkPosn
* Description:          Calculate estimated stroke about estimated wheel pressure
********************************************************************************/
static int32_t LAPCT_s32CalcEstStkFromEstP(int16_t s16PressFL, int16_t s16PressFR, int16_t s16PressRL, int16_t s16PressRR)
{
	int16_t s16EstStkPosnFL = 0;
	int16_t s16EstStkPosnFR = 0;
	int16_t s16EstStkPosnRL = 0;
	int16_t s16EstStkPosnRR = 0;
	int32_t s32TotalEstStkPosn = 0;

	s16EstStkPosnFL = (int16_t)LAPCT_s32CalcStkByEstimdWhlP(s16PressFL, U16_SEL_F1WHEEL);
	s16EstStkPosnFR = (int16_t)LAPCT_s32CalcStkByEstimdWhlP(s16PressFR, U16_SEL_F1WHEEL);
	s16EstStkPosnRL = (int16_t)LAPCT_s32CalcStkByEstimdWhlP(s16PressRL, U16_SEL_R1WHEEL);
	s16EstStkPosnRR = (int16_t)LAPCT_s32CalcStkByEstimdWhlP(s16PressRR, U16_SEL_R1WHEEL);

	s32TotalEstStkPosn = ((int32_t)s16EstStkPosnFL + (int32_t)s16EstStkPosnFR + (int32_t)s16EstStkPosnRL + (int32_t)s16EstStkPosnRR);

	s32TotalEstStkPosn = L_s32LimitMinMax(s32TotalEstStkPosn,0,S32_POSITION_PULSE_MAX);

	return s32TotalEstStkPosn;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s32CalcAllowedRecrStk
* CALLED BY:            LAPCT_vChkPosnInfoForStkRcvr
* Preconditions:        none
* PARAMETER:            s32StkPosn, s32TatEstStkPosnOfWhlP
* RETURN VALUE:         s32TotalEstStkPosn
* Description:          Calculate allowed recovery stroke
********************************************************************************/
static int32_t LAPCT_s32CalcAllowedRecrStk(int32_t s32StkPosn, int32_t s32TatEstStkPosnOfWhlP)
{
	int32_t s32AllowedRecoveryStroke = 0;

	s32AllowedRecoveryStroke = s32StkPosn - s32TatEstStkPosnOfWhlP;

	s32AllowedRecoveryStroke = L_s32LimitMinMax(s32AllowedRecoveryStroke,0,S32_POSITION_PULSE_MAX);

	return s32AllowedRecoveryStroke;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s32CalcFwdMaxPosnForRcvr
* CALLED BY:            LAPCT_vChkPosnInfoForStkRcvr
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Calculate forward maximum position for stroke recovery
********************************************************************************/
static int32_t LAPCT_s32CalcFwdMaxPosnForRcvr(int32_t s32TatEstStkPosnOfWhlP)
{
	int32_t s32FwdMaxPosnForRcvr = 0;
	int32_t s32PosnToReachDesiredPress = 0;
	int32_t s32DeltaStkToReachDesiredP = 0;
	int32_t s32StkRcvrMaxLimdPosn = S32_STK_RCVR_MAX_LIMD_POSN;

	s32PosnToReachDesiredPress = (int32_t)LAPCT_s32CalcStorkeByPress(S16_PCTRL_PRESS_150_BAR, U16_SEL_4WHEEL);

	s32DeltaStkToReachDesiredP = s32PosnToReachDesiredPress - s32TatEstStkPosnOfWhlP;

	s32DeltaStkToReachDesiredP = L_s32LimitMinMax(s32DeltaStkToReachDesiredP,0,S32_POSITION_PULSE_MAX);

	s32FwdMaxPosnForRcvr =  s32StkRcvrMaxLimdPosn - s32DeltaStkToReachDesiredP;

	s32FwdMaxPosnForRcvr = L_s32LimitMinMax(s32FwdMaxPosnForRcvr,-S32_POSITION_PULSE_MAX,S32_POSITION_PULSE_MAX);

	return s32FwdMaxPosnForRcvr;
}


#define PCT_CTRL_STOP_SEC_CODE
#include "Pct_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
