/**
 * @defgroup LAPCT_PedalFeelGenVlvCtrlMode LAPCT_PedalFeelGenVlvCtrlMode
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LAPCT_PedalFeelGenVlvCtrlMode.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

#include "../../HDR/Pct_5msCtrl.h"
#include "LAPCT_PressureVlvCtrlMode.h"
#include "../LAPCT_DecidePressSource.h"

/*#include <Mando_Std_Types.h>*/

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

#define S8_4CH_MUX      4
#define S8_2CH_MUX      2
#define S8_1CH_MUX      1
#define S8_0CH_MUX      0

#define S16_CV_VALVE_CLOSE_HOLD_TIME          U16_T_5_S
#define S16_RV_VALVE_CLOSE_HOLD_TIME          U16_T_5_S

#define U8_VALVE_MAX_HOLD_TIME_CV			U8_T_500_MS
#define U8_VALVE_MAX_HOLD_TIME_RLV			U8_T_0_MS
#define U8_VALVE_MAX_HOLD_TIME_PDV			U8_T_500_MS
#define U8_VALVE_MAX_HOLD_TIME_BALV			U8_T_500_MS

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
	uint16_t lau16ValveActState     ;

	uint8_t  lau8ValveOnTime ;

	uint16_t lau16Inhibit;

}LA_VALVE_ACT_t;

typedef struct
{
	uint8_t lau8ValveCtrlState     ;

	uint8_t  lau8ValveNoiseState ;

	uint8_t lau8ValveHoldTime;

}LA_P_CTRL_VALVE_t;

typedef struct
{
	int8_t s8PCtrlState;
	int8_t s8PChamberVolumeVvCtrl;
}PRESS_VALVE_CTRL_MODE_INPUT_t;
typedef struct
{

	LA_P_CTRL_VALVE_t laPTC_RLV;
	LA_P_CTRL_VALVE_t laPTC_CV;
	LA_P_CTRL_VALVE_t laPTC_PDV;
	LA_P_CTRL_VALVE_t laPTC_BALV;

}PRESS_VALVE_CTRL_MODE_OUTPUT_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_CONST_UNSPECIFIED
#include "Pct_MemMap.h"
/** Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Pct_MemMap.h"

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"


#define PCT_5MSCTRL_START_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/** Variable Section (32BIT)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"

FAL_STATIC PRESS_VALVE_CTRL_MODE_INPUT_t stPressVlvCtrlInput;
FAL_STATIC PRESS_VALVE_CTRL_MODE_OUTPUT_t stPressVlvCtrlOutput;



#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"


#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"


#define PCT_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"


#define PCT_5MSCTRL_START_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/** Variable Section (32BIT)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_CODE
#include "Pct_MemMap.h"

static LA_P_CTRL_VALVE_t LAPCT_stSetCircuitVlvState(LA_P_CTRL_VALVE_t* pstCV, int8_t s8PCtrlStateInput);
static uint8_t LAPCT_u8DecideCvCtrlStateFromReady(int8_t s8PCtrlStateInput);
static uint8_t LAPCT_u8DecideCvCtrlStFromInitial(int8_t s8PCtrlStateInput);
static uint8_t LAPCT_u8DecideCvCtrlStateFromActive(int8_t s8PCtrlStateInput);
static uint8_t LAPCT_u8DecideCvNoiseStInActive(int8_t s8PCtrlStateInput);


static LA_P_CTRL_VALVE_t LAPCT_stSetReleaseVlvState(LA_P_CTRL_VALVE_t * pstRLV, int8_t s8PCtrlStateInput, int8_t s8PChamberVolumeVvCtrlInput);
static uint8_t LAPCT_u8DecideRlvCtrlStFromReady(int8_t s8PCtrlStateInput, int8_t s8PChamberVolumeVvCtrlInput);
static uint8_t LAPCT_u8DecideRlvCtrlStFromInitial(int8_t s8PCtrlStateInput, int8_t s8PChamberVolumeVvCtrlInput);
static uint8_t LAPCT_u8DecideRlvCtrlStFromActive(int8_t s8PCtrlStateInput, int8_t s8PChamberVolumeVvCtrlInput);
static uint8_t LAPCT_u8DecideRlvNoiseStInActive(uint8_t u8RlVCtrlState);


static LA_P_CTRL_VALVE_t LAPCT_stSetPressureDumpVlvState(LA_P_CTRL_VALVE_t* pstPDV, int8_t s8PCtrlStateInput, int8_t s8PChamberVolumeVvCtrlInput, uint8_t lau8BALVlvCtrlStateInput);
static uint8_t LAPCT_u8DecidePdvCtrlStFromReady(int8_t s8PCtrlStateInput, int8_t s8PChamberVolumeVvCtrlInput);
static uint8_t LAPCT_u8DecidePdvCtrlStFromInitial(int8_t s8PCtrlStateInput, int8_t s8PChamberVolumeVvCtrlInput);
static uint8_t LAPCT_u8DecidePdvCtrlStFromActive(int8_t s8PCtrlStateInput, int8_t s8PChamberVolumeVvCtrlInput, uint8_t lau8BALVlvCtrlStateInput);



static LA_P_CTRL_VALVE_t LAPCT_stSetBalanceVlvState(LA_P_CTRL_VALVE_t* pstBALV, int8_t s8PCtrlStateInput, int8_t s8PChamberVolumeVvCtrlInput, uint8_t lau8PDVlvCtrlStateInput);
static uint8_t LAPCT_u8DecideBalvCtrlStFromReady(int8_t s8PCtrlStateInput, int8_t s8PChamberVolumeVvCtrlInput);
static uint8_t LAPCT_u8DecideBalvCtrlStFromInitial(int8_t s8PCtrlStateInput, int8_t s8PChamberVolumeVvCtrlInput, uint8_t lau8PDVlvCtrlStateInput);
static uint8_t LAPCT_u8DecideBalvCtrlStFromActive(int8_t s8PCtrlStateInput, int8_t s8PChamberVolumeVvCtrlInput);
static uint8_t LAPCT_u8DecideBalvNoiseStAtInitial(int8_t s8PCtrlStateInput, int8_t s8PChamberVolumeVvCtrlInput, uint8_t lau8PDVlvCtrlStateInput);


static void LAPCT_vInputPressVlvCtrlInfo(void);
static void LAPCT_vOutputPressVlvCtrlInfo(void);
static void LAPCT_vPressureVlvCtrlMain(void);





/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void LAPCT_PressureVlvCtrlMode(void)
{
	/*    interface input*/
	LAPCT_vInputPressVlvCtrlInfo();

	LAPCT_vPressureVlvCtrlMain();

	/*    interface output*/
	LAPCT_vOutputPressVlvCtrlInfo();

}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

static void LAPCT_vInputPressVlvCtrlInfo(void)
{
	stPressVlvCtrlInput.s8PCtrlState           = PressCTRLLocalBus.s8PCtrlState;
	stPressVlvCtrlInput.s8PChamberVolumeVvCtrl = PressCTRLLocalBus.s8PChamberVolume;
}

static void LAPCT_vPressureVlvCtrlMain(void)
{
	int8_t s8PCtrlState           = stPressVlvCtrlInput.s8PCtrlState           ;
	int8_t s8PChamberVolumeVvCtrl = stPressVlvCtrlInput.s8PChamberVolumeVvCtrl ;

	static LA_P_CTRL_VALVE_t stCV   = {U8_VALVE_STATE_READY, U8_VLV_RESPONSE_LV_NORMAL, U8_VALVE_MAX_HOLD_TIME_CV};
	static LA_P_CTRL_VALVE_t stRLV  = {U8_VALVE_STATE_READY, U8_VLV_RESPONSE_LV_NORMAL, U8_VALVE_MAX_HOLD_TIME_RLV};
	static LA_P_CTRL_VALVE_t stPDV  = {U8_VALVE_STATE_READY, U8_VLV_RESPONSE_LV_NORMAL, U8_VALVE_MAX_HOLD_TIME_PDV};
	static LA_P_CTRL_VALVE_t stBALV = {U8_VALVE_STATE_READY, U8_VLV_RESPONSE_LV_NORMAL, U8_VALVE_MAX_HOLD_TIME_BALV};

	stCV = LAPCT_stSetCircuitVlvState(&stCV, s8PCtrlState);
	stRLV = LAPCT_stSetReleaseVlvState(&stRLV, s8PCtrlState, s8PChamberVolumeVvCtrl);
	stPDV = LAPCT_stSetPressureDumpVlvState(&stPDV, s8PCtrlState, s8PChamberVolumeVvCtrl, stBALV.lau8ValveCtrlState);
	stBALV = LAPCT_stSetBalanceVlvState(&stBALV, s8PCtrlState, s8PChamberVolumeVvCtrl, stPDV.lau8ValveCtrlState);

	stPressVlvCtrlOutput.laPTC_CV   = stCV;
	stPressVlvCtrlOutput.laPTC_RLV  = stRLV;
	stPressVlvCtrlOutput.laPTC_PDV  = stPDV;
	stPressVlvCtrlOutput.laPTC_BALV = stBALV;

}

static void LAPCT_vOutputPressVlvCtrlInfo(void)
{
	Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.CvVlvSt  = stPressVlvCtrlOutput.laPTC_CV.lau8ValveCtrlState;
	Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.RlVlvSt  = stPressVlvCtrlOutput.laPTC_RLV.lau8ValveCtrlState;
	Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.PdVlvSt  = stPressVlvCtrlOutput.laPTC_PDV.lau8ValveCtrlState;
	Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.BalVlvSt = stPressVlvCtrlOutput.laPTC_BALV.lau8ValveCtrlState;

	Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.CvVlvNoiseCtrlSt  = stPressVlvCtrlOutput.laPTC_CV.lau8ValveNoiseState;
	Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.RlVlvNoiseCtrlSt  = stPressVlvCtrlOutput.laPTC_RLV.lau8ValveNoiseState;
	Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.PdVlvNoiseCtrlSt  = stPressVlvCtrlOutput.laPTC_PDV.lau8ValveNoiseState;
	Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.BalVlvNoiseCtrlSt = stPressVlvCtrlOutput.laPTC_BALV.lau8ValveNoiseState;

	Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.CvVlvHoldTime  = stPressVlvCtrlOutput.laPTC_CV.lau8ValveHoldTime;
	Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.RlVlvHoldTime  = stPressVlvCtrlOutput.laPTC_RLV.lau8ValveHoldTime;
	Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.PdVlvHoldTime  = stPressVlvCtrlOutput.laPTC_PDV.lau8ValveHoldTime;
	Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.BalVlvHoldTime = stPressVlvCtrlOutput.laPTC_BALV.lau8ValveHoldTime;

	/* Local Variable Update - Begin */

	/* Sync with RTE Variable and Local Variable  - End*/
}

static uint8_t LAPCT_u8DecideCvCtrlStateFromReady(int8_t s8PCtrlStateInput)
{
	uint8_t u8CVlvCtrlState = U8_VALVE_STATE_READY;

	if((s8PCtrlStateInput==S8_PRESS_PREBOOST)||(s8PCtrlStateInput==S8_PRESS_BOOST))
	{
		u8CVlvCtrlState = U8_VALVE_STATE_INITIAL;
	}
	else { }

	return u8CVlvCtrlState;
}

static LA_P_CTRL_VALVE_t LAPCT_stSetCircuitVlvState(LA_P_CTRL_VALVE_t* pstCV, int8_t s8PCtrlStateInput)
{
	LA_P_CTRL_VALVE_t stCV;
	stCV = *pstCV;
	stCV.lau8ValveHoldTime = U8_VALVE_MAX_HOLD_TIME_CV;

	switch(pstCV->lau8ValveCtrlState)
	{
	case U8_VALVE_STATE_READY:

		stCV.lau8ValveCtrlState =  LAPCT_u8DecideCvCtrlStateFromReady(s8PCtrlStateInput);
		stCV.lau8ValveNoiseState = U8_VLV_RESPONSE_LV_NORMAL;
		break;


	case U8_VALVE_STATE_INITIAL:

		stCV.lau8ValveCtrlState = LAPCT_u8DecideCvCtrlStFromInitial(s8PCtrlStateInput);
		stCV.lau8ValveNoiseState = U8_VLV_RESPONSE_LV_NORMAL;
		break;

	case U8_VALVE_STATE_ACTIVE:

		stCV.lau8ValveCtrlState = LAPCT_u8DecideCvCtrlStateFromActive(s8PCtrlStateInput);
		stCV.lau8ValveNoiseState = LAPCT_u8DecideCvNoiseStInActive(s8PCtrlStateInput);
		break;

	default:
		stCV.lau8ValveCtrlState = U8_VALVE_STATE_READY;
		stCV.lau8ValveNoiseState = U8_VLV_RESPONSE_LV_NORMAL;
		break;
	}
	return stCV;
}

static uint8_t LAPCT_u8DecideCvCtrlStFromInitial(int8_t s8PCtrlStateInput)
{
	int8_t u8CVlvCtrlState;

	if((s8PCtrlStateInput==S8_PRESS_INHIBIT)||(s8PCtrlStateInput==S8_PRESS_READY))
	{
		u8CVlvCtrlState = U8_VALVE_STATE_READY;
	}
	else if(s8PCtrlStateInput==S8_PRESS_BOOST)
	{
		u8CVlvCtrlState = U8_VALVE_STATE_ACTIVE;
	}
	else
	{
		u8CVlvCtrlState = U8_VALVE_STATE_INITIAL;
	}

	return u8CVlvCtrlState;
}

static uint8_t LAPCT_u8DecideCvCtrlStateFromActive(int8_t s8PCtrlStateInput)
{
	/* u8VlvActHoldOnRequestFlg 30sec */

	uint8_t u8CVlvCtrlState;
	uint8_t u8VlvHoldOnRequestFlg;
	static uint16_t laptcu16ValveCloseCnt;

	if ((s8PCtrlStateInput==S8_PRESS_BOOST) || (s8PCtrlStateInput==S8_PRESS_FADE_OUT) )
	{
		laptcu16ValveCloseCnt = S16_CV_VALVE_CLOSE_HOLD_TIME;
		u8VlvHoldOnRequestFlg =0;
	}
	else
	{
		if (laptcu16ValveCloseCnt > 0)
		{
			laptcu16ValveCloseCnt = laptcu16ValveCloseCnt - 1;
		}
		else
		{
			laptcu16ValveCloseCnt = 0;
			u8VlvHoldOnRequestFlg =1;
		}
	}

	if(s8PCtrlStateInput==S8_PRESS_INHIBIT)
	{
		u8CVlvCtrlState = U8_VALVE_STATE_READY;
	}
	else if  (u8VlvHoldOnRequestFlg ==1)
	{
		u8CVlvCtrlState = U8_VALVE_STATE_READY;
	}
	else
	{
		u8CVlvCtrlState = U8_VALVE_STATE_ACTIVE;
	}
	return u8CVlvCtrlState;
}

static uint8_t LAPCT_u8DecideCvNoiseStInActive(int8_t s8PCtrlStateInput)
{
	uint8_t u8CVResponseLv = U8_VLV_RESPONSE_LV_SLOW;

	if(s8PCtrlStateInput==S8_PRESS_INHIBIT)
	{
		u8CVResponseLv = U8_VLV_RESPONSE_LV_NORMAL;
	}
	else
	{
		u8CVResponseLv = U8_VLV_RESPONSE_LV_SLOW;
	}
	return u8CVResponseLv;
}

static LA_P_CTRL_VALVE_t LAPCT_stSetReleaseVlvState(LA_P_CTRL_VALVE_t * pstRLV, int8_t s8PCtrlStateInput, int8_t s8PChamberVolumeVvCtrlInput)
{
	LA_P_CTRL_VALVE_t stRLV;
	stRLV = *pstRLV;
	stRLV.lau8ValveHoldTime = U8_VALVE_MAX_HOLD_TIME_RLV;

	switch(pstRLV->lau8ValveCtrlState)
	{
	case U8_VALVE_STATE_READY:

		stRLV.lau8ValveCtrlState =  LAPCT_u8DecideRlvCtrlStFromReady(s8PCtrlStateInput, s8PChamberVolumeVvCtrlInput);
		stRLV.lau8ValveNoiseState = U8_VLV_RESPONSE_LV_NORMAL;

		break;

	case U8_VALVE_STATE_INITIAL:

		stRLV.lau8ValveCtrlState =  LAPCT_u8DecideRlvCtrlStFromInitial(s8PCtrlStateInput, s8PChamberVolumeVvCtrlInput);
		stRLV.lau8ValveNoiseState = U8_VLV_RESPONSE_LV_NORMAL;
		break;

	case U8_VALVE_STATE_ACTIVE:

		stRLV.lau8ValveCtrlState = LAPCT_u8DecideRlvCtrlStFromActive(s8PCtrlStateInput, s8PChamberVolumeVvCtrlInput);
		stRLV.lau8ValveNoiseState = LAPCT_u8DecideRlvNoiseStInActive(stRLV.lau8ValveCtrlState);
		break;

	default:

		stRLV.lau8ValveCtrlState = U8_VALVE_STATE_READY;
		stRLV.lau8ValveNoiseState = U8_VLV_RESPONSE_LV_NORMAL;
		break;
	}

	return stRLV;
}

static uint8_t LAPCT_u8DecideRlvCtrlStFromReady(int8_t s8PCtrlStateInput, int8_t s8PChamberVolumeVvCtrlInput)
{
	uint8_t u8ReturnValue;
	int8_t s8BoostCheck;
	int8_t s8PressChamberCheck;


	s8BoostCheck = ((s8PCtrlStateInput==S8_PRESS_PREBOOST)||(s8PCtrlStateInput==S8_PRESS_BOOST)) ;
	s8PressChamberCheck = ((s8PChamberVolumeVvCtrlInput== U8_BOOST_LOW_PRESS) ||(s8PChamberVolumeVvCtrlInput== U8_BOOST_HIGH_PRESS));


	if((s8BoostCheck == TRUE) && (s8PressChamberCheck ==TRUE))
	{
		u8ReturnValue = U8_VALVE_STATE_INITIAL;

	}
	else
	{
		u8ReturnValue = U8_VALVE_STATE_READY;
	}

	return u8ReturnValue;
}

static uint8_t LAPCT_u8DecideRlvCtrlStFromInitial(int8_t s8PCtrlStateInput, int8_t s8PChamberVolumeVvCtrlInput)
{
	uint8_t u8ReturnValue;

	if((s8PCtrlStateInput==S8_PRESS_INHIBIT)||(s8PCtrlStateInput==S8_PRESS_READY))
	{
		u8ReturnValue = U8_VALVE_STATE_READY;
	}
	else if((s8PCtrlStateInput==S8_PRESS_BOOST) &&((s8PChamberVolumeVvCtrlInput== U8_BOOST_LOW_PRESS) ||(s8PChamberVolumeVvCtrlInput== U8_BOOST_HIGH_PRESS)))
	{
		u8ReturnValue = U8_VALVE_STATE_ACTIVE;
	}
	else
	{
		u8ReturnValue = U8_VALVE_STATE_INITIAL;
	}

	return u8ReturnValue;
}

static uint8_t LAPCT_u8DecideRlvCtrlStFromActive(int8_t s8PCtrlStateInput, int8_t s8PChamberVolumeVvCtrlInput)
{
	/* u8VlvActHoldOnRequestFlg 30sec */

	uint8_t u8ReturnValue;
	uint8_t u8VlvHoldOnRequestFlg;
	static uint16_t laptcu16ValveCloseCnt;

	if ((s8PCtrlStateInput==S8_PRESS_BOOST) || (s8PCtrlStateInput==S8_PRESS_FADE_OUT) )
	{
		laptcu16ValveCloseCnt = S16_RV_VALVE_CLOSE_HOLD_TIME;
		u8VlvHoldOnRequestFlg =0;
	}
	else
	{
		if (laptcu16ValveCloseCnt > 0)
		{
			laptcu16ValveCloseCnt = laptcu16ValveCloseCnt - 1;
		}
		else
		{
			laptcu16ValveCloseCnt = 0;
			u8VlvHoldOnRequestFlg =1;
		}
	}

	if(s8PCtrlStateInput==S8_PRESS_INHIBIT)
	{
		u8ReturnValue = U8_VALVE_STATE_READY;
	}
	else if((s8PChamberVolumeVvCtrlInput== U8_BOOST_BACKWARD) ||(s8PChamberVolumeVvCtrlInput== U8_BOOST_HOLD_PRESS))
	{
		u8ReturnValue = U8_VALVE_STATE_READY;
	}
	else if(u8VlvHoldOnRequestFlg == TRUE )
	{
		u8ReturnValue = U8_VALVE_STATE_READY;
	}
	else
	{
		u8ReturnValue = U8_VALVE_STATE_ACTIVE;
	}

	return u8ReturnValue;

}

static uint8_t LAPCT_u8DecideRlvNoiseStInActive(uint8_t u8RlVCtrlState)
{
	uint8_t u8RlVResponseLv = U8_VLV_RESPONSE_LV_NORMAL;

	if(u8RlVCtrlState==U8_VALVE_STATE_READY)
	{
		u8RlVResponseLv = U8_VLV_RESPONSE_LV_NORMAL;
	}
	else
	{
		u8RlVResponseLv = U8_VLV_RESPONSE_LV_MID;
	}

	return u8RlVResponseLv;
}

static LA_P_CTRL_VALVE_t LAPCT_stSetPressureDumpVlvState(LA_P_CTRL_VALVE_t* pstPDV, int8_t s8PCtrlStateInput, int8_t s8PChamberVolumeVvCtrlInput, uint8_t lau8BALVlvCtrlStateInput)
{
	LA_P_CTRL_VALVE_t stPDV;
	stPDV = *pstPDV;
	stPDV.lau8ValveHoldTime = U8_VALVE_MAX_HOLD_TIME_PDV;

	switch(pstPDV->lau8ValveCtrlState)
	{
	case U8_VALVE_STATE_READY:

		stPDV.lau8ValveCtrlState =  LAPCT_u8DecidePdvCtrlStFromReady(s8PCtrlStateInput, s8PChamberVolumeVvCtrlInput);
		stPDV.lau8ValveNoiseState = U8_VLV_RESPONSE_LV_NORMAL;

		break;

	case U8_VALVE_STATE_INITIAL:

		stPDV.lau8ValveCtrlState =  LAPCT_u8DecidePdvCtrlStFromInitial(s8PCtrlStateInput, s8PChamberVolumeVvCtrlInput);
		stPDV.lau8ValveNoiseState = U8_VLV_RESPONSE_LV_NORMAL;

		break;


	case U8_VALVE_STATE_ACTIVE:

		stPDV.lau8ValveCtrlState =  LAPCT_u8DecidePdvCtrlStFromActive(s8PCtrlStateInput, s8PChamberVolumeVvCtrlInput, lau8BALVlvCtrlStateInput);
		stPDV.lau8ValveNoiseState = U8_VLV_RESPONSE_LV_NORMAL;

		break;

		/*    	case U8_VALVE_STATE_MAINTAIN:
			break;	

		case U8_VALVE_STATE_SUSPECT:
			break;	
		 */

	default:

		stPDV.lau8ValveCtrlState = U8_VALVE_STATE_READY;
		stPDV.lau8ValveNoiseState = U8_VLV_RESPONSE_LV_NORMAL;

		break;
	}
	return stPDV;
}

static uint8_t LAPCT_u8DecidePdvCtrlStFromReady(int8_t s8PCtrlStateInput, int8_t s8PChamberVolumeVvCtrlInput)
{
	uint8_t u8ReturnValue;

	if((s8PCtrlStateInput==S8_PRESS_BOOST) && ((s8PChamberVolumeVvCtrlInput== U8_BOOST_BACKWARD) ||(s8PChamberVolumeVvCtrlInput== U8_BOOST_HIGH_PRESS)))
	{
		u8ReturnValue = U8_VALVE_STATE_INITIAL;

	}
	else
	{
		u8ReturnValue = U8_VALVE_STATE_READY;
	}

	return u8ReturnValue;
}


static uint8_t LAPCT_u8DecidePdvCtrlStFromInitial(int8_t s8PCtrlStateInput, int8_t s8PChamberVolumeVvCtrlInput)
{
	uint8_t u8ReturnValue;

	if((s8PCtrlStateInput==S8_PRESS_INHIBIT)||(s8PCtrlStateInput==S8_PRESS_READY))
	{
		u8ReturnValue = U8_VALVE_STATE_READY;
	}

	else if((s8PCtrlStateInput==S8_PRESS_BOOST) && ((s8PChamberVolumeVvCtrlInput== U8_BOOST_BACKWARD) ||(s8PChamberVolumeVvCtrlInput== U8_BOOST_HIGH_PRESS)))
	{
		u8ReturnValue = U8_VALVE_STATE_ACTIVE;
	}
	else
	{
		u8ReturnValue = U8_VALVE_STATE_INITIAL;
	}

	return u8ReturnValue;

}
static uint8_t LAPCT_u8DecidePdvCtrlStFromActive(int8_t s8PCtrlStateInput, int8_t s8PChamberVolumeVvCtrlInput, uint8_t lau8BALVlvCtrlStateInput)
{
	uint8_t u8ReturnValue;
	static lapctu16CntAfBalvClosed = 0;

	if(s8PCtrlStateInput==S8_PRESS_INHIBIT)
	{
		u8ReturnValue = U8_VALVE_STATE_READY;
	}
	else if((s8PChamberVolumeVvCtrlInput== U8_BOOST_LOW_PRESS)||(s8PCtrlStateInput==S8_PRESS_READY))
	{
		if(lau8BALVlvCtrlStateInput == U8_VALVE_STATE_READY)
		{
			lapctu16CntAfBalvClosed = lapctu16CntAfBalvClosed + 1;
			if(lapctu16CntAfBalvClosed > U16_PDV_OPEN_DELAY_AF_BALV_CLOSED)
			{
				u8ReturnValue = U8_VALVE_STATE_READY;
				lapctu16CntAfBalvClosed = 0;
			}
			else
			{
				u8ReturnValue = U8_VALVE_STATE_ACTIVE;
			}

		}
		else
		{
			u8ReturnValue = U8_VALVE_STATE_ACTIVE;
		}
	}
	else
	{
		u8ReturnValue = U8_VALVE_STATE_ACTIVE;
	}

	return u8ReturnValue;

}

static LA_P_CTRL_VALVE_t LAPCT_stSetBalanceVlvState(LA_P_CTRL_VALVE_t* pstBALV, int8_t s8PCtrlStateInput, int8_t s8PChamberVolumeVvCtrlInput, uint8_t lau8PDVlvCtrlStateInput)
{
	LA_P_CTRL_VALVE_t stBALV;
	stBALV = *pstBALV;
	stBALV.lau8ValveHoldTime = U8_VALVE_MAX_HOLD_TIME_BALV;

	switch(pstBALV->lau8ValveCtrlState)
	{
	case U8_VALVE_STATE_READY:

		stBALV.lau8ValveCtrlState =  LAPCT_u8DecideBalvCtrlStFromReady(s8PCtrlStateInput, s8PChamberVolumeVvCtrlInput);
		stBALV.lau8ValveNoiseState = U8_VLV_RESPONSE_LV_NORMAL;

		break;

	case U8_VALVE_STATE_INITIAL:

		stBALV.lau8ValveCtrlState =  LAPCT_u8DecideBalvCtrlStFromInitial(s8PCtrlStateInput, s8PChamberVolumeVvCtrlInput, lau8PDVlvCtrlStateInput);
		stBALV.lau8ValveNoiseState = LAPCT_u8DecideBalvNoiseStAtInitial(s8PCtrlStateInput, s8PChamberVolumeVvCtrlInput, lau8PDVlvCtrlStateInput);
		break;

	case U8_VALVE_STATE_ACTIVE:

		stBALV.lau8ValveCtrlState =  LAPCT_u8DecideBalvCtrlStFromActive(s8PCtrlStateInput, s8PChamberVolumeVvCtrlInput);
		stBALV.lau8ValveNoiseState = U8_VLV_RESPONSE_LV_NORMAL;
		break;

		/*    	case U8_VALVE_STATE_MAINTAIN:
			break;	

		case U8_VALVE_STATE_SUSPECT:
			break;	
		 */
	default:

		stBALV.lau8ValveCtrlState = U8_VALVE_STATE_READY;
		stBALV.lau8ValveNoiseState = U8_VLV_RESPONSE_LV_NORMAL;
		break;
	}

	return stBALV;
}


static uint8_t LAPCT_u8DecideBalvCtrlStFromReady(int8_t s8PCtrlStateInput, int8_t s8PChamberVolumeVvCtrlInput)
{
	uint8_t u8ReturnValue;

	if((s8PCtrlStateInput==S8_PRESS_BOOST) && ((s8PChamberVolumeVvCtrlInput== U8_BOOST_BACKWARD) ||(s8PChamberVolumeVvCtrlInput== U8_BOOST_HIGH_PRESS)))
	{
		u8ReturnValue = U8_VALVE_STATE_INITIAL;
	}
	else
	{
		u8ReturnValue = U8_VALVE_STATE_READY;
	}

	return u8ReturnValue;
}

static uint8_t LAPCT_u8DecideBalvCtrlStFromInitial(int8_t s8PCtrlStateInput, int8_t s8PChamberVolumeVvCtrlInput, uint8_t lau8PDVlvCtrlStateInput)
{
	uint8_t u8ReturnValue;
	static int8_t s8ActCnt =0;

	if((s8PCtrlStateInput==S8_PRESS_INHIBIT)||(s8PCtrlStateInput==S8_PRESS_READY))
	{
		u8ReturnValue = U8_VALVE_STATE_READY;
	}

	else if((s8PCtrlStateInput==S8_PRESS_BOOST) && ((s8PChamberVolumeVvCtrlInput== U8_BOOST_BACKWARD) ||(s8PChamberVolumeVvCtrlInput== U8_BOOST_HIGH_PRESS)))
	{
		if(lau8PDVlvCtrlStateInput == U8_VALVE_STATE_ACTIVE)
		{
			if(s8ActCnt < 1)
			{
				s8ActCnt = s8ActCnt +1;
				u8ReturnValue = U8_VALVE_STATE_INITIAL;
			}
			else
			{
				u8ReturnValue = U8_VALVE_STATE_ACTIVE;
				s8ActCnt =0;
			}
		}
		else
		{
			u8ReturnValue = U8_VALVE_STATE_INITIAL;
		}
	}
	else
	{
		u8ReturnValue = U8_VALVE_STATE_INITIAL;
	}

	return u8ReturnValue;
}

static uint8_t LAPCT_u8DecideBalvCtrlStFromActive(int8_t s8PCtrlStateInput, int8_t s8PChamberVolumeVvCtrlInput)
{
	uint8_t u8ReturnValue;


	if((s8PCtrlStateInput==S8_PRESS_INHIBIT)||(s8PCtrlStateInput==S8_PRESS_READY))
	{
		u8ReturnValue = U8_VALVE_STATE_READY;
	}

	else if(s8PChamberVolumeVvCtrlInput== U8_BOOST_LOW_PRESS)
	{
		u8ReturnValue = U8_VALVE_STATE_READY;
	}
	else
	{
		u8ReturnValue = U8_VALVE_STATE_ACTIVE;
	}

	return u8ReturnValue;
}

static uint8_t LAPCT_u8DecideBalvNoiseStAtInitial(int8_t s8PCtrlStateInput, int8_t s8PChamberVolumeVvCtrlInput, uint8_t lau8PDVlvCtrlStateInput)
{
	uint8_t u8BalVResponseLv = U8_VLV_RESPONSE_LV_NORMAL;

	if((s8PCtrlStateInput==S8_PRESS_INHIBIT)||(s8PCtrlStateInput==S8_PRESS_READY))
	{
		u8BalVResponseLv = U8_VLV_RESPONSE_LV_NORMAL;
	}
	else if((s8PCtrlStateInput==S8_PRESS_BOOST) && ((s8PChamberVolumeVvCtrlInput== U8_BOOST_BACKWARD) ||(s8PChamberVolumeVvCtrlInput== U8_BOOST_HIGH_PRESS)))
	{
		if(lau8PDVlvCtrlStateInput == U8_VALVE_STATE_ACTIVE)
		{
			u8BalVResponseLv = U8_VLV_RESPONSE_LV_MID;
		}
		else
		{
			u8BalVResponseLv = U8_VLV_RESPONSE_LV_NORMAL;
		}
	}
	else
	{
		u8BalVResponseLv = U8_VLV_RESPONSE_LV_NORMAL;
	}
	return u8BalVResponseLv;
}

#define PCT_CTRL_STOP_SEC_CODE
#include "Pct_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
