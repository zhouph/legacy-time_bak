/**
 * @defgroup LAPCT_PedalFeelGenVlvCtrlMode LAPCT_PedalFeelGenVlvCtrlMode
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LAPCT_PedalFeelGenVlvCtrlMode.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

#include "../../HDR/Pct_5msCtrl.h"
#include "./LAPCT_PedalFeelGenVlvCtrlMode.h"

/*#include <Mando_Std_Types.h>*/

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define __AHB_ACTIVE_BRAKING_ENABLE
#define __CVV_CLOSE_DELAY_FOR_QUICK_BRK     ENABLE


#define S8_4CH_MUX      4
#define S8_2CH_MUX      2
#define S8_1CH_MUX      1
#define S8_0CH_MUX      0

#define U8_CV_ON_TIME_MIN                   U8_T_50_MS

#define S16_VALVE_NOISE_REDUCE_SPEED		S16_SPEED_40_KPH
#define S16_CLOSE_CV_FAST_APPLY_REF			S16_TRAVEL_10_MM
#define S16_SIMV_NOT_OPEN_DECTECT_THRES		S16_MPRESS_10_BAR
#define U16_CUT_VLV_LONG_OPEN_DCT_TIME		U16_T_5_S

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
    uint16_t lau16ValveActState     ;

    uint8_t  lau8ValveOnTime ;
    
    uint16_t lau16Inhibit;
    
}LA_VALVE_ACT_t;

typedef struct
{
    uint8_t lau8ValveCtrlState     ;

    uint8_t  lau8ValveNoiseState ;

    uint8_t lau8ValveHoldTime;

}LA_P_CTRL_VALVE_t;

typedef struct
{
	uint8_t			u1PedalSimPressGenOn         ;
	int8_t 			s8PCtrlState                 ;
	int8_t 			s8PCtrlBoostMode             ;
	int8_t 			s8PCtrlFadeoutState          ;
	int16_t 		s16MC1PressFilt              ;
	int16_t 		s16MC2PressFilt              ;
	uint8_t 		u1BCpres1InvalidFlg          ;
	uint8_t 		u1BCpres2InvalidFlg          ;
	uint8_t			u1cEMSAccPedDep_Pc_Err_Flg   ;
	uint8_t			u1cEMSAccPedDep_Pc_Sus       ;
	int16_t			s16cEMSAccPedDep             ;
	int16_t 		s16PSPressFilt               ;
	uint8_t 		u1fPSPSenFaultDet            ;
	uint8_t 		u1fPSPSuspectSet             ;
	uint8_t			u1BLS                        ;
	uint8_t			u1BLSFaultDet                ;
	uint8_t			u8PedalSigTag                ;
	uint8_t			u1PedalSimPressGenOnEsc     ;
	uint8_t			u1PedalSimPressGenOnEscOld   ;
	uint8_t			u1PedalSimPressGenOnForFM    ;
	int16_t			s16FastApply1msFlg           ;
	uint8_t			u8InitFastApplyFlg           ;
	int16_t			s16PedalTravelRateMmPSec     ;
	int8_t			s8StrkRcvrCtrlState          ;
	int16_t			s16VehicleSpeed              ;
	uint8_t			u8FadeOutBfResBrkFlg         ;
	uint8_t			u8MotorFadeoutEndOK          ;
	uint16_t		IdbPCtrlActFlg               ;
	int16_t			s16TargetPress               ;

	int16_t 		s16ESCActiveFlg	             ;
	int16_t 		s16TCSActiveFlg	             ;
	int16_t			s16MultiplexCtrlActive		 ;
	int16_t 		s16SafetyControlActFlg		 ;
	uint8_t			u8PdlVlvActHoldOnRequestFlg  ;
	uint8_t			u8CutCloseDelayForQuickBrk 	 ;
	uint8_t			u8MultiplexCtrlActive;
    LA_VALVE_CMD_t	stMuxInVCmdFL				;	/* Inlet valve actuation request - FL 																*/
    LA_VALVE_CMD_t	stMuxInVCmdFR				;	/* Inlet valve actuation request - FR 																*/
    LA_VALVE_CMD_t	stMuxInVCmdRL				;	/* Inlet valve actuation request - RL 																*/
    LA_VALVE_CMD_t	stMuxInVCmdRR				;	/* Inlet valve actuation request - RR 																*/
    uint8_t			u8MuxCircCtrlReqPri			;
    uint8_t			u8MuxCircCtrlReqSec			;
    int16_t			s16MuxMode					;
    uint8_t			u8AbsMtrCtrlMode			;
    int16_t			s16MuxPwrPistonTarP			;
    uint8_t			u8ValveHoldReq				;
    uint8_t			u8InhibitFlg				;
}DECIDE_VALVE_CTRL_MODE_INPUT_t;

typedef struct
{
	uint8_t			u8Cut1VlvCtrlState;
	uint8_t			u8Cut2VlvCtrlState;
	uint8_t			u8SimVlvCtrlState;

	uint8_t			u8Cut1VlvCtrlRespLv;
	uint8_t			u8Cut2VlvCtrlRespLv;
	uint8_t			u8SimVlvCtrlRespLv;

	uint8_t			u8Cut1VlvMaxholdTime;
	uint8_t			u8Cut2VlvMaxholdTime;
	uint8_t			u8SimVlvMaxholdTime;

	uint8_t 		u8CutVVLongOpenFlg;
}DECIDE_VALVE_CTRL_MODE_OUTPUT_t;

typedef struct
{
	uint8_t		u1PedalSimPressGenOn		;
	int8_t 		s8PCtrlState 				;
	int8_t 		s8PCtrlBoostMode 			;
	int8_t 		s8PCtrlFadeoutState 		;
	int16_t 	s16MC1PressFilt 			;
	int16_t 	s16MC2PressFilt 			;
	uint8_t 	u1BCpres1InvalidFlg 		;
	uint8_t 	u1BCpres2InvalidFlg 		;
	uint8_t		u1cEMSAccPedDep_Pc_Err_Flg	;
	uint8_t		u1cEMSAccPedDep_Pc_Sus		;
	int16_t		s16cEMSAccPedDep			;
	int16_t		s16VehicleSpeed				;
}stDECIDE_VLV_ACT_HOLD_ON_R_t;

typedef struct
{
	int8_t 		s8PCtrlBoostMode 			;
	int16_t 	s16MC1PressFilt 			;
	int16_t 	s16MC2PressFilt 			;
	int16_t 	s16PSPressFilt 				;
	uint8_t 	u1fPSPSenFaultDet 			;
	uint8_t 	u1fPSPSuspectSet 			;
	uint16_t	u16AHBOnByPedalCnt			;
	uint8_t		u8InitFastApplyFlg 			;
}stCHECK_CUT_VALVE_CLOSE_DELAY_R_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_CONST_UNSPECIFIED
#include "Pct_MemMap.h"
/** Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/

#define PCT_5MSCTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Pct_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"

/* Temp RTE Variable Definition : Pct_1msCtrl.c */
uint8_t lapctu8CutVVLongOpenFlg;
/* Temp Global Variable Definition for Log************************************/

/* Temp Global Variable Definition for Log************************************/

#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_START_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/** Variable Section (32BIT)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Global Use(Local Define Extern Use) - Start*/
FAL_STATIC  DECIDE_VALVE_CTRL_MODE_INPUT_t stPdlVlvCtrlInput;
uint8_t lapctu8PdlVlvActHoldOnRequestFlg = FALSE;
/* Global Use(Local Define Extern Use) - End*/

/* Local Use(Local Define Local Use) - Start */

/* Local Use(Local Define Local Use) - End */
#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_START_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/** Variable Section (32BIT)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_CODE
#include "Pct_MemMap.h"

static void  Get_DecideVlvCtrlInpInfo(void);
static void Set_DecideVlvCtrlOutpInfo(DECIDE_VALVE_CTRL_MODE_OUTPUT_t stDecideValveCtrlMainOutput);
static DECIDE_VALVE_CTRL_MODE_OUTPUT_t LAPCT_stPedalFeelGenVlvCtrlMain(void);
static uint8_t LAPCT_u8DecideCutValveCtrlState(uint8_t u8CutVlvStateOld, DECIDE_VALVE_CTRL_MODE_INPUT_t * stVlvCtrlInfo_r );
static uint8_t LAPCT_u8DecideSimValveCtrlState(uint8_t u8SimVlvStateOld, DECIDE_VALVE_CTRL_MODE_INPUT_t * stVlvCtrlInfo_r );
static uint8_t LAPCT_u8DecideVlvActHoldOn(const stDECIDE_VLV_ACT_HOLD_ON_R_t* const pstDecideVlvActHoldOn_r);
static uint16_t LAPCT_u16CheckStopVlvActHoldOn(const stDECIDE_VLV_ACT_HOLD_ON_R_t* const pstDecideVlvActHoldOn_r, uint16_t u16VlvActHoldOnCnt);
static uint8_t LAPCT_u8CheckCutValveCloseDelay(const stCHECK_CUT_VALVE_CLOSE_DELAY_R_t* const pstCheckCutValveCloseDelay_r);
static uint16_t LAPCT_u16CalcAHBOnByPedalCnt(uint16_t u16AHBOn, int16_t s16TargetPress);
static uint8_t LAPCT_u8GetPdlVlvStFromReady(DECIDE_VALVE_CTRL_MODE_INPUT_t* stVlvCtrlInfo_r);
static uint8_t LAPCT_u8GetPdlVlvStFromInitial(DECIDE_VALVE_CTRL_MODE_INPUT_t* stVlvCtrlInfo_r);
static uint8_t LAPCT_u8GetPdlVlvStFromActive(DECIDE_VALVE_CTRL_MODE_INPUT_t* stVlvCtrlInfo_r);
static uint8_t LAPCT_u8GetPdlVlvStFromMaintain(DECIDE_VALVE_CTRL_MODE_INPUT_t* stVlvCtrlInfo_r);
static uint8_t LAPCT_u8DecideCutValveRespLv(DECIDE_VALVE_CTRL_MODE_INPUT_t * stVlvCtrlInfo_r );
static uint8_t LAPCT_u8GetCutVlvRespLvByPdl(DECIDE_VALVE_CTRL_MODE_INPUT_t* stVlvCtrlInfo_r);
static uint8_t LAPCT_u8DecideSimValveRespLv(DECIDE_VALVE_CTRL_MODE_INPUT_t * stVlvCtrlInfo_r );
static uint8_t LAPCT_u8GetSimVlvRespLvByPdl(DECIDE_VALVE_CTRL_MODE_INPUT_t* stVlvCtrlInfo_r);
static uint16_t LAPCT_u16GetSimValveMaxHoldTime(uint8_t u8SimVlvCtrlState, int16_t s16CircuitP, int16_t s16PSPress);
static uint8_t LAPCT_u8DctCutVLVLongOpen(uint8_t u8Cut1VlvCtrlState, uint8_t u8Cut2VlvCtrlState);
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void LAPCT_PedalFeelGenVlvCtrlMode(void)
{
	static DECIDE_VALVE_CTRL_MODE_OUTPUT_t stDecideValveCtrlMainOutput;
/*    interface input*/
	Get_DecideVlvCtrlInpInfo();


	stDecideValveCtrlMainOutput = LAPCT_stPedalFeelGenVlvCtrlMain();

/*    interface output*/
    Set_DecideVlvCtrlOutpInfo(stDecideValveCtrlMainOutput);


}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static DECIDE_VALVE_CTRL_MODE_OUTPUT_t LAPCT_stPedalFeelGenVlvCtrlMain(void)
{
	uint8_t u8VlvActHoldOnRequestFlg = FALSE;
	static DECIDE_VALVE_CTRL_MODE_OUTPUT_t stDecideValveCtrlMain_w;
	static uint16_t lapctu16Cut1VlvOpenCnt = U8_T_0_MS;
	static uint16_t lapctu16Cut2VlvOpenCnt = U8_T_0_MS;

	stDECIDE_VLV_ACT_HOLD_ON_R_t 		stDecideVlvActHoldOn_r			= {0,0,0,0,0,0,0,0,0,0,0,0};
	stCHECK_CUT_VALVE_CLOSE_DELAY_R_t 	stCheckCutValveCloseDelay_r 	= {0,0,0,0,0,0,0,0};
#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
	int16_t s16CircuitP													= stPdlVlvCtrlInput.s16MC1PressFilt;
#else
	int16_t s16CircuitP													= (stPdlVlvCtrlInput.s16MC1PressFilt > stPdlVlvCtrlInput.s16MC2PressFilt) ? stPdlVlvCtrlInput.s16MC1PressFilt : stPdlVlvCtrlInput.s16MC2PressFilt;
#endif
	stDecideVlvActHoldOn_r.u1PedalSimPressGenOn			= stPdlVlvCtrlInput.u1PedalSimPressGenOn		 ;
	stDecideVlvActHoldOn_r.s8PCtrlState					= stPdlVlvCtrlInput.s8PCtrlState				 ;
	stDecideVlvActHoldOn_r.s8PCtrlBoostMode				= stPdlVlvCtrlInput.s8PCtrlBoostMode			 ;
	stDecideVlvActHoldOn_r.s8PCtrlFadeoutState			= stPdlVlvCtrlInput.s8PCtrlFadeoutState		 ;
	stDecideVlvActHoldOn_r.s16MC1PressFilt				= stPdlVlvCtrlInput.s16MC1PressFilt			 ;
	stDecideVlvActHoldOn_r.s16MC2PressFilt				= stPdlVlvCtrlInput.s16MC2PressFilt			 ;
	stDecideVlvActHoldOn_r.u1BCpres1InvalidFlg			= stPdlVlvCtrlInput.u1BCpres1InvalidFlg		 ;
	stDecideVlvActHoldOn_r.u1BCpres2InvalidFlg			= stPdlVlvCtrlInput.u1BCpres2InvalidFlg		 ;
	stDecideVlvActHoldOn_r.u1cEMSAccPedDep_Pc_Err_Flg	= stPdlVlvCtrlInput.u1cEMSAccPedDep_Pc_Err_Flg;
	stDecideVlvActHoldOn_r.u1cEMSAccPedDep_Pc_Sus		= stPdlVlvCtrlInput.u1cEMSAccPedDep_Pc_Sus	 ;
	stDecideVlvActHoldOn_r.s16cEMSAccPedDep				= stPdlVlvCtrlInput.s16cEMSAccPedDep			 ;
	stDecideVlvActHoldOn_r.s16VehicleSpeed				= stPdlVlvCtrlInput.s16VehicleSpeed			 ;

	stCheckCutValveCloseDelay_r.s8PCtrlBoostMode	    = stPdlVlvCtrlInput.s8PCtrlBoostMode	;
	stCheckCutValveCloseDelay_r.s16MC1PressFilt 	    = stPdlVlvCtrlInput.s16MC1PressFilt 	;
	stCheckCutValveCloseDelay_r.s16MC2PressFilt 	    = stPdlVlvCtrlInput.s16MC2PressFilt 	;
	stCheckCutValveCloseDelay_r.s16PSPressFilt 		    = stPdlVlvCtrlInput.s16PSPressFilt 		;
	stCheckCutValveCloseDelay_r.u1fPSPSenFaultDet 	    = stPdlVlvCtrlInput.u1fPSPSenFaultDet 	;
	stCheckCutValveCloseDelay_r.u1fPSPSuspectSet 	    = stPdlVlvCtrlInput.u1fPSPSuspectSet 	;
	stCheckCutValveCloseDelay_r.u8InitFastApplyFlg 	    = (uint8_t)((stPdlVlvCtrlInput.u8InitFastApplyFlg==TRUE) || (stPdlVlvCtrlInput.s16FastApply1msFlg==TRUE));
	stCheckCutValveCloseDelay_r.u16AHBOnByPedalCnt      = LAPCT_u16CalcAHBOnByPedalCnt(stPdlVlvCtrlInput.IdbPCtrlActFlg, stPdlVlvCtrlInput.s16TargetPress);

	stPdlVlvCtrlInput.u8PdlVlvActHoldOnRequestFlg = LAPCT_u8DecideVlvActHoldOn(&stDecideVlvActHoldOn_r);
	lapctu8PdlVlvActHoldOnRequestFlg = stPdlVlvCtrlInput.u8PdlVlvActHoldOnRequestFlg;
	stPdlVlvCtrlInput.u8CutCloseDelayForQuickBrk = LAPCT_u8CheckCutValveCloseDelay(&stCheckCutValveCloseDelay_r);

	stDecideValveCtrlMain_w.u8Cut1VlvCtrlState = LAPCT_u8DecideCutValveCtrlState(stDecideValveCtrlMain_w.u8Cut1VlvCtrlState, &stPdlVlvCtrlInput);
	stDecideValveCtrlMain_w.u8Cut2VlvCtrlState = LAPCT_u8DecideCutValveCtrlState(stDecideValveCtrlMain_w.u8Cut2VlvCtrlState, &stPdlVlvCtrlInput);
	stDecideValveCtrlMain_w.u8SimVlvCtrlState  = LAPCT_u8DecideSimValveCtrlState(stDecideValveCtrlMain_w.u8SimVlvCtrlState, &stPdlVlvCtrlInput);

	stDecideValveCtrlMain_w.u8Cut1VlvCtrlRespLv = LAPCT_u8DecideCutValveRespLv(&stPdlVlvCtrlInput);
	stDecideValveCtrlMain_w.u8Cut2VlvCtrlRespLv = stDecideValveCtrlMain_w.u8Cut1VlvCtrlRespLv;
	stDecideValveCtrlMain_w.u8SimVlvCtrlRespLv  = LAPCT_u8DecideSimValveRespLv(&stPdlVlvCtrlInput);

	stDecideValveCtrlMain_w.u8Cut1VlvMaxholdTime = U8_T_0_MS;
	stDecideValveCtrlMain_w.u8Cut2VlvMaxholdTime = U8_T_0_MS;
	stDecideValveCtrlMain_w.u8SimVlvMaxholdTime  = (uint8_t)LAPCT_u16GetSimValveMaxHoldTime(stDecideValveCtrlMain_w.u8SimVlvCtrlState, stPdlVlvCtrlInput.s16MC1PressFilt, stPdlVlvCtrlInput.s16PSPressFilt);

	stDecideValveCtrlMain_w.u8CutVVLongOpenFlg = LAPCT_u8DctCutVLVLongOpen(stDecideValveCtrlMain_w.u8Cut1VlvCtrlState, stDecideValveCtrlMain_w.u8Cut2VlvCtrlState);

	return stDecideValveCtrlMain_w;
}


static void  Get_DecideVlvCtrlInpInfo(void)
{
	/* TODO: Produce Old flag by NeedFastRespForEsc, PedlSimrPGendOnFlg */
	static uint8_t lapctu8PedalSimPressGenOnEscOld 	= FALSE;

	stPdlVlvCtrlInput.u1PedalSimPressGenOn		    = (uint8_t) Pct_5msCtrlBus.Pct_5msCtrlVlvActtnReqByBaseBrkCtrlrInfo.PedlSimrPGendOnFlg;
	stPdlVlvCtrlInput.s8PCtrlState 				= (int8_t ) Pct_5msCtrlBus.Pct_5msCtrlPCtrlSt;
	stPdlVlvCtrlInput.s8PCtrlBoostMode 			= (int8_t ) Pct_5msCtrlBus.Pct_5msCtrlPCtrlBoostMod;
	stPdlVlvCtrlInput.s8PCtrlFadeoutState 		    = (int8_t ) Pct_5msCtrlBus.Pct_5msCtrlPCtrlFadeoutSt;

#if __P_SENSOR_MEASURE_TYPE== EACH_CIRCUIT_PRESSURE
	stPdlVlvCtrlInput.s16MC1PressFilt 			= (int16_t) Pct_5msCtrlBus.Pct_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar;
	stPdlVlvCtrlInput.s16MC2PressFilt 			= (int16_t) Pct_5msCtrlBus.Pct_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar;
#else
	stPdlVlvCtrlInput.s16MC1PressFilt			= (int16_t) Pct_5msCtrlBus.Pct_5msCtrlPistPFildInfo.PistPFild_1_100Bar;
	stPdlVlvCtrlInput.s16MC2PressFilt 			= (int16_t) Pct_5msCtrlBus.Pct_5msCtrlPistPFildInfo.PistPFild_1_100Bar;
#endif
	stPdlVlvCtrlInput.u1BCpres1InvalidFlg 		    = (uint8_t) FALSE;
	stPdlVlvCtrlInput.u1BCpres2InvalidFlg 		    = (uint8_t) FALSE;

	stPdlVlvCtrlInput.u1cEMSAccPedDep_Pc_Err_Flg	= (uint8_t) Pct_5msCtrlBus.Pct_5msCtrlCanRxAccelPedlInfo.AccelPedlValErr;		/*liu1cEMSAccPedDep_Pc_Err_Flg*/
	stPdlVlvCtrlInput.u1cEMSAccPedDep_Pc_Sus		= (uint8_t) Pct_5msCtrlBus.Pct_5msCtrlCanRxAccelPedlInfo.AccelPedlValErr;		/*liu1cEMSAccPedDep_Pc_Sus*/
	stPdlVlvCtrlInput.s16cEMSAccPedDep			    = (int16_t) Pct_5msCtrlBus.Pct_5msCtrlCanRxAccelPedlInfo.AccelPedlVal;				/*lis16cEMSAccPedDep*/

	stPdlVlvCtrlInput.s16PSPressFilt 				= (int16_t) Pct_5msCtrlBus.Pct_5msCtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar;
	stPdlVlvCtrlInput.u1fPSPSenFaultDet 			= (uint8_t) Pct_5msCtrlBus.Pct_5msCtrlEemFailData.Eem_Fail_SimP;
	stPdlVlvCtrlInput.u1fPSPSuspectSet 			= (uint8_t) Pct_5msCtrlBus.Pct_5msCtrlEemSuspectData.Eem_Suspect_SimP;

	stPdlVlvCtrlInput.u1BLS						= (uint8_t) Pct_5msCtrlBus.Pct_5msCtrlBlsFild;
	stPdlVlvCtrlInput.u1BLSFaultDet				= (uint8_t) Pct_5msCtrlBus.Pct_5msCtrlEemFailData.Eem_Fail_BLS;
	stPdlVlvCtrlInput.u8PedalSigTag				= (uint8_t) Pct_5msCtrlBus.Pct_5msCtrlPedlTrvlTagInfo.PedlSigTag;


	stPdlVlvCtrlInput.u1PedalSimPressGenOnEscOld	= (uint8_t) lapctu8PedalSimPressGenOnEscOld;
	stPdlVlvCtrlInput.u1PedalSimPressGenOnForFM	= (uint8_t) FALSE;			/*lcahbu1PedalSimPressGenOnForFM*/

	stPdlVlvCtrlInput.s16FastApply1msFlg			= (int16_t) Pct_5msCtrlBus.Pct_5msCtrlBrkPedlStatus1msInfo.PanicBrkSt1msFlg; /*ldahbs16FastApply1msCnt*/
	stPdlVlvCtrlInput.u8InitFastApplyFlg			= (uint8_t) Pct_5msCtrlBus.Pct_5msCtrlBrkPedlStatusInfo.PanicBrkStFlg;		/*lcahbu8InitFastApplyFlg*/

	stPdlVlvCtrlInput.s16PedalTravelRateMmPSec 	= (int16_t) Pct_5msCtrlBus.Pct_5msCtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec;

	stPdlVlvCtrlInput.s8StrkRcvrCtrlState			= (int16_t) Pct_5msCtrlBus.Pct_5msCtrlStkRecvryActnIfInfo.StkRecvryCtrlState;
	stPdlVlvCtrlInput.s16VehicleSpeed				= (int16_t) Pct_5msCtrlBus.Pct_5msCtrlVehSpdFild;
	stPdlVlvCtrlInput.u8FadeOutBfResBrkFlg			= (uint8_t) FALSE;
	stPdlVlvCtrlInput.u8MotorFadeoutEndOK			= (uint8_t) PressCTRLLocalBus.u16MotorFadeoutEndOK;/*PressCTRLbus.MotICtrlFadeOutCmpldFlg;*/;
	stPdlVlvCtrlInput.IdbPCtrlActFlg				= (uint16_t)Pct_5msCtrlBus.Pct_5msCtrlPCtrlAct;
	/*For Mux Refactoring*/

	stPdlVlvCtrlInput.s16ESCActiveFlg	            = (uint16_t)Pct_5msCtrlBus.Pct_5msCtrlEscCtrlInfo.EscActFlg;
	stPdlVlvCtrlInput.s16TCSActiveFlg	            = (uint16_t)Pct_5msCtrlBus.Pct_5msCtrlTcsCtrlInfo.TcsActFlg;
	stPdlVlvCtrlInput.s16SafetyControlActFlg		= (uint16_t)(stPdlVlvCtrlInput.s16ESCActiveFlg)|(stPdlVlvCtrlInput.s16TCSActiveFlg);
	/*For Mux Refactoring*/
	stPdlVlvCtrlInput.s16MultiplexCtrlActive		= PressCTRLLocalBus.u8MultiplexCtrlActive;
	stPdlVlvCtrlInput.stMuxInVCmdFL				= PressCTRLLocalBus.st_MuxInVCmdFL;
	stPdlVlvCtrlInput.stMuxInVCmdFR				= PressCTRLLocalBus.st_MuxInVCmdFR;
	stPdlVlvCtrlInput.stMuxInVCmdRL				= PressCTRLLocalBus.st_MuxInVCmdRL;
	stPdlVlvCtrlInput.stMuxInVCmdRR				= PressCTRLLocalBus.st_MuxInVCmdRR;

	stPdlVlvCtrlInput.u8MuxCircCtrlReqPri			= PressCTRLLocalBus.u8MuxCircCtrlReqPri;
	stPdlVlvCtrlInput.u8MuxCircCtrlReqSec			= PressCTRLLocalBus.u8MuxCircCtrlReqSec;

	stPdlVlvCtrlInput.s16MuxMode					= PressCTRLLocalBus.s16MuxMode;
	stPdlVlvCtrlInput.u8AbsMtrCtrlMode				= PressCTRLLocalBus.u8AbsMtrCtrlMode;
	
	stPdlVlvCtrlInput.s16MuxPwrPistonTarP			= PressCTRLLocalBus.s16MuxPwrPistonTarP;
	
	lapctu8PedalSimPressGenOnEscOld				= stPdlVlvCtrlInput.u1PedalSimPressGenOnEsc;

}

static void Set_DecideVlvCtrlOutpInfo(DECIDE_VALVE_CTRL_MODE_OUTPUT_t stDecideValveCtrlMainOutput_r)
{
	/* Local Variable Update - Begin */
	Pct_5msCtrlBus.Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvSt = stDecideValveCtrlMainOutput_r.u8Cut1VlvCtrlState;
	Pct_5msCtrlBus.Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvSt = stDecideValveCtrlMainOutput_r.u8Cut2VlvCtrlState;
	Pct_5msCtrlBus.Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SimVlvSt     = stDecideValveCtrlMainOutput_r.u8SimVlvCtrlState;

	Pct_5msCtrlBus.Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvNoiseCtrlSt = stDecideValveCtrlMainOutput_r.u8Cut1VlvCtrlRespLv;
	Pct_5msCtrlBus.Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvNoiseCtrlSt = stDecideValveCtrlMainOutput_r.u8Cut2VlvCtrlRespLv;
	Pct_5msCtrlBus.Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SimVlvNoiseCtrlSt     = stDecideValveCtrlMainOutput_r.u8SimVlvCtrlRespLv;

	Pct_5msCtrlBus.Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvHoldTime = stDecideValveCtrlMainOutput_r.u8Cut1VlvMaxholdTime;
	Pct_5msCtrlBus.Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvHoldTime = stDecideValveCtrlMainOutput_r.u8Cut2VlvMaxholdTime;
	Pct_5msCtrlBus.Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SimVlvHoldTime     = stDecideValveCtrlMainOutput_r.u8SimVlvMaxholdTime;

	Pct_5msCtrlBus.Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.CutVlvLongOpen = stDecideValveCtrlMainOutput_r.u8CutVVLongOpenFlg;
	lapctu8CutVVLongOpenFlg = stDecideValveCtrlMainOutput_r.u8CutVVLongOpenFlg;	/* TODO: Pct_1msCtrl.c RTE Interface */

	/* Local Variable Update - End */

}

static uint8_t LAPCT_u8DecideCutValveCtrlState(uint8_t u8CutVlvStateOld, DECIDE_VALVE_CTRL_MODE_INPUT_t * stVlvCtrlInfo_r )
{
	uint8_t u8DecideCutVlvState = U8_VALVE_STATE_READY;

	switch(u8CutVlvStateOld)
	{
	case U8_VALVE_STATE_READY:
		u8DecideCutVlvState = LAPCT_u8GetPdlVlvStFromReady(stVlvCtrlInfo_r);
		break;

	case U8_VALVE_STATE_INITIAL:
		u8DecideCutVlvState = LAPCT_u8GetPdlVlvStFromInitial(stVlvCtrlInfo_r);
		break;

	case U8_VALVE_STATE_ACTIVE:
		u8DecideCutVlvState = LAPCT_u8GetPdlVlvStFromActive(stVlvCtrlInfo_r);
		break;

	case U8_VALVE_STATE_MAINTAIN:
		u8DecideCutVlvState = LAPCT_u8GetPdlVlvStFromMaintain(stVlvCtrlInfo_r);
		break;

	case U8_VALVE_STATE_SUSPECT:
		break;

	default:
		u8DecideCutVlvState = U8_VALVE_STATE_READY;
		break;
	}

	return u8DecideCutVlvState;
}

static uint8_t LAPCT_u8DecideSimValveCtrlState(uint8_t u8SimVlvStateOld, DECIDE_VALVE_CTRL_MODE_INPUT_t * stVlvCtrlInfo_r )
{
	uint8_t u8DecideSimVlvState = U8_VALVE_STATE_READY;

	switch(u8SimVlvStateOld)
	{
	case U8_VALVE_STATE_READY:
		u8DecideSimVlvState = LAPCT_u8GetPdlVlvStFromReady(stVlvCtrlInfo_r);
		break;

	case U8_VALVE_STATE_INITIAL:
		u8DecideSimVlvState = LAPCT_u8GetPdlVlvStFromInitial(stVlvCtrlInfo_r);
		break;

	case U8_VALVE_STATE_ACTIVE:
		u8DecideSimVlvState = LAPCT_u8GetPdlVlvStFromActive(stVlvCtrlInfo_r);
		break;

	case U8_VALVE_STATE_MAINTAIN:
		u8DecideSimVlvState = LAPCT_u8GetPdlVlvStFromMaintain(stVlvCtrlInfo_r);
		break;

	case U8_VALVE_STATE_SUSPECT:
		break;

	default:
		u8DecideSimVlvState = U8_VALVE_STATE_READY;
		break;
	}

	return u8DecideSimVlvState;
}
/*******************************************************************************
* FUNCTION NAME:        LAPCT_u8DecideVlvActHoldOn
* CALLED BY:            LAPCT_vDecideValveCtrlMain
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         u8VlvActHoldOnRequestFlg
* Description:          Decide valve maintain time
********************************************************************************/
static uint8_t LAPCT_u8DecideVlvActHoldOn(const stDECIDE_VLV_ACT_HOLD_ON_R_t* const pstDecideVlvActHoldOn_r)
{
	static uint16_t lapctu16VlvActHoldOnCnt = 0;
	uint8_t u8VlvActHoldOnRequestFlg = 0;
	int8_t s8PCtrlBoostMode = pstDecideVlvActHoldOn_r->s8PCtrlBoostMode;

	if(pstDecideVlvActHoldOn_r->u1PedalSimPressGenOn == 1)
	{
		if(s8PCtrlBoostMode == S8_BOOST_MODE_RESPONSE_L7)
		{
			lapctu16VlvActHoldOnCnt = U16_CV_ON_TIME_P_GEAR;
		}
		else if(pstDecideVlvActHoldOn_r->s16VehicleSpeed < S16_VALVE_NOISE_REDUCE_SPEED)
		{
			lapctu16VlvActHoldOnCnt = U16_CV_ON_TIME_LSD;
		}
		else
		{
			lapctu16VlvActHoldOnCnt = (uint16_t)U8_CV_ON_TIME_MIN;
		}

		lapctu16VlvActHoldOnCnt = (lapctu16VlvActHoldOnCnt < (uint16_t)U8_CV_ON_TIME_MIN) ? U8_CV_ON_TIME_MIN : lapctu16VlvActHoldOnCnt;
	}
	else if(lapctu16VlvActHoldOnCnt>0)
	{
		if(pstDecideVlvActHoldOn_r->s8PCtrlState == S8_PRESS_INHIBIT)
		{
			lapctu16VlvActHoldOnCnt = 0;
		}
		else if((pstDecideVlvActHoldOn_r->u1cEMSAccPedDep_Pc_Err_Flg==0) && (pstDecideVlvActHoldOn_r->u1cEMSAccPedDep_Pc_Sus==0) && (pstDecideVlvActHoldOn_r->s16cEMSAccPedDep > S16_ACCEL_03_PERCENT))
		{
			lapctu16VlvActHoldOnCnt = 0;
		}
		else
		{
			lapctu16VlvActHoldOnCnt = lapctu16VlvActHoldOnCnt - 1;
		}
	}
	else
	{
		lapctu16VlvActHoldOnCnt = 0;
	}

	lapctu16VlvActHoldOnCnt = LAPCT_u16CheckStopVlvActHoldOn(pstDecideVlvActHoldOn_r, lapctu16VlvActHoldOnCnt);

#if DISABLE
	if(pstDecideVlvActHoldOn_r->u1PedalSimPressGenOn == 1)
	{
		u8VlvActHoldOnRequestFlg = 0;
	}
	else
	{
		if(lapctu16VlvActHoldOnCnt > 0)
		{
			u8VlvActHoldOnRequestFlg = TRUE;
		}
		else
		{
			u8VlvActHoldOnRequestFlg = FALSE;
		}
	}
#else
	if(lapctu16VlvActHoldOnCnt > 0)
	{
		u8VlvActHoldOnRequestFlg = TRUE;
	}
	else
	{
		u8VlvActHoldOnRequestFlg = FALSE;
	}

#endif
	return u8VlvActHoldOnRequestFlg;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_u16CheckStopVlvActHoldOn
* CALLED BY:            LAPCT_u8DecideVlvActHoldOn
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         u16VlvActHoldOnCnt
* Description:          Check to stop to maintain valve act
********************************************************************************/
static uint16_t LAPCT_u16CheckStopVlvActHoldOn(const stDECIDE_VLV_ACT_HOLD_ON_R_t* const pstDecideVlvActHoldOn_r, uint16_t u16VlvActHoldOnCnt)
{
    static int16_t lcahbs16BCpresFadeOutMinP = 0;
    static int16_t lcahbs16BCpresFadeOutMinS = 0;
    static int16_t lapcts16FadeOutTime = 0;
    int16_t s16CVVMaintainEndBcp = S16_PCTRL_PRESS_0_BAR;

    if(pstDecideVlvActHoldOn_r->s8PCtrlState == S8_PRESS_FADE_OUT)
    {
    	lapcts16FadeOutTime = lapcts16FadeOutTime + 1;
        if(pstDecideVlvActHoldOn_r->s8PCtrlFadeoutState == S8_PCTRL_FADEOUT_STATE_1)
		{
			if(lapcts16FadeOutTime <= 1)  /* initial capture of circuit pressure beginning of fadeout mode */
			{
				lcahbs16BCpresFadeOutMinP = pstDecideVlvActHoldOn_r->s16MC1PressFilt;
				lcahbs16BCpresFadeOutMinS = pstDecideVlvActHoldOn_r->s16MC2PressFilt;
			}
			else
			{
				if(lcahbs16BCpresFadeOutMinP > pstDecideVlvActHoldOn_r->s16MC1PressFilt)
	            {
					lcahbs16BCpresFadeOutMinP = pstDecideVlvActHoldOn_r->s16MC1PressFilt;
				}
				if(lcahbs16BCpresFadeOutMinS > pstDecideVlvActHoldOn_r->s16MC2PressFilt)
	            {
					lcahbs16BCpresFadeOutMinS = pstDecideVlvActHoldOn_r->s16MC2PressFilt;
				}
			}
		}
        else
        {
            /* Limit CVV Maintain for vv leak */
            if(u16VlvActHoldOnCnt > 0)
            {
            	if(pstDecideVlvActHoldOn_r->s8PCtrlBoostMode == S8_BOOST_MODE_RESPONSE_L7)
                {
                    s16CVVMaintainEndBcp = S16_PCTRL_PRESS_1_BAR;
                }
                else
                {
                    s16CVVMaintainEndBcp = S16_CVV_MNT_END_BCP_REF*10;
                }

                if(
    				(pstDecideVlvActHoldOn_r->s16MC1PressFilt >= s16CVVMaintainEndBcp) ||
    				(pstDecideVlvActHoldOn_r->s16MC2PressFilt >= s16CVVMaintainEndBcp) ||
                	(pstDecideVlvActHoldOn_r->s16MC1PressFilt >= (lcahbs16BCpresFadeOutMinP + S16_CVV_MNT_END_BCP_DIFF_REF*10)) ||
                    (pstDecideVlvActHoldOn_r->s16MC2PressFilt >= (lcahbs16BCpresFadeOutMinS + S16_CVV_MNT_END_BCP_DIFF_REF*10)) ||
 					(pstDecideVlvActHoldOn_r->u1BCpres1InvalidFlg==1) || (pstDecideVlvActHoldOn_r->u1BCpres2InvalidFlg==1) )
                {
                	u16VlvActHoldOnCnt = 0;
                }
            }
			else
			{
			    lcahbs16BCpresFadeOutMinP = S16_PCTRL_PRESS_0_BAR;
        		lcahbs16BCpresFadeOutMinS = S16_PCTRL_PRESS_0_BAR;
                lapcts16FadeOutTime = 0;
			}
        }
    }
    else
    {
        lcahbs16BCpresFadeOutMinP = S16_PCTRL_PRESS_0_BAR;
        lcahbs16BCpresFadeOutMinS = S16_PCTRL_PRESS_0_BAR;
        lapcts16FadeOutTime = 0;
    }

    return u16VlvActHoldOnCnt;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_u8CheckCutValveCloseDelay
* CALLED BY:            LAPCT_vDecideValveCtrlMain
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Set Cut valve delayed-open for apply performance
********************************************************************************/
static uint8_t LAPCT_u8CheckCutValveCloseDelay(const stCHECK_CUT_VALVE_CLOSE_DELAY_R_t* const pstCheckCutValveCloseDelay_r)
{
	static int16_t laahbs16PSpresFiltOld = 0;
	uint8_t u8CutCloseDelayForQuickBrk = 0;
	int16_t s16PSpresFilt = pstCheckCutValveCloseDelay_r->s16PSPressFilt;
	int16_t s16MC1PressFilt = pstCheckCutValveCloseDelay_r->s16MC1PressFilt;
	int16_t s16MC2PressFilt = pstCheckCutValveCloseDelay_r->s16MC2PressFilt;

	if((pstCheckCutValveCloseDelay_r->u1fPSPSenFaultDet==0) && (pstCheckCutValveCloseDelay_r->u1fPSPSuspectSet==0))
	{
		if(pstCheckCutValveCloseDelay_r->u16AHBOnByPedalCnt<=U8_T_50_MS)
		{
			if(pstCheckCutValveCloseDelay_r->s8PCtrlBoostMode == S8_BOOST_MODE_RESPONSE_L7)
			{
				u8CutCloseDelayForQuickBrk = 0;
			}
			else
			{
			if(pstCheckCutValveCloseDelay_r->u8InitFastApplyFlg==TRUE)
			{
				if((s16PSpresFilt > S16_CVV_CLOSE_DELAY_PSP_THR_H*10)
					&& ((s16PSpresFilt > (s16MC1PressFilt + S16_PCTRL_PRESS_10_BAR))/* || (lcahbu1BCpres1InvalidFlg==1)*/)  /*Need to consider Mcp signal validity */
					&& ((s16PSpresFilt > (s16MC2PressFilt + S16_PCTRL_PRESS_10_BAR))/* || (lcahbu1BCpres2InvalidFlg==1)*/))  /*Need to consider Mcp signal validity */
				{
						u8CutCloseDelayForQuickBrk = 1;
				}
				else if((s16PSpresFilt > S16_CVV_CLOSE_DELAY_PSP_THR_L*10)
				&& (s16PSpresFilt > (laahbs16PSpresFiltOld + S16_PCTRL_PRESS_3_BAR))
					&& ((s16PSpresFilt > (s16MC1PressFilt + S16_PCTRL_PRESS_2_BAR))/* || (lcahbu1BCpres1InvalidFlg==1)*/)	/*Need to consider Mcp signal validity */
					&& ((s16PSpresFilt > (s16MC2PressFilt + S16_PCTRL_PRESS_2_BAR))/* || (lcahbu1BCpres2InvalidFlg==1)*/))  /*Need to consider Mcp signal validity */
				{
						u8CutCloseDelayForQuickBrk = 1;
				}
				else
				{
						u8CutCloseDelayForQuickBrk = 0;
				}
			}
			else
			{
					u8CutCloseDelayForQuickBrk = 0;
				}
			}
		}
		else
		{
			u8CutCloseDelayForQuickBrk = 0;
		}
	}
	else
	{
		u8CutCloseDelayForQuickBrk = 0;
	}

	laahbs16PSpresFiltOld = s16PSpresFilt;

	return u8CutCloseDelayForQuickBrk;
}

static uint16_t LAPCT_u16CalcAHBOnByPedalCnt(uint16_t u16AHBOn, int16_t s16TargetPress)
{
	static uint16_t lapctu16AHBOnByPedalCnt = 0;
	static int16_t lapcts16TargetPressOld = S16_PCTRL_PRESS_0_BAR;

	if(u16AHBOn == TRUE)
	{
		if ((s16TargetPress - lapcts16TargetPressOld) > -S16_PCTRL_PRESS_0_1_BAR)
		{
			lapctu16AHBOnByPedalCnt = lapctu16AHBOnByPedalCnt + 1;
			if (lapctu16AHBOnByPedalCnt > U8_T_100_MS)
			{
				lapctu16AHBOnByPedalCnt = U8_T_100_MS;
			}
		}
		else { /* Do Nothing */ }
	}
	else
	{
		lapctu16AHBOnByPedalCnt = 0;
	}


	lapcts16TargetPressOld = s16TargetPress;

	return lapctu16AHBOnByPedalCnt;
}

static uint8_t LAPCT_u8GetPdlVlvStFromReady(DECIDE_VALVE_CTRL_MODE_INPUT_t* stVlvCtrlInfo_r)
{
	uint8_t u8DecidePdlVlvState = U8_VALVE_STATE_READY;

	if (stVlvCtrlInfo_r->u8InhibitFlg == TRUE)
	{
		u8DecidePdlVlvState = U8_VALVE_STATE_READY;
	}
	else if (  (stVlvCtrlInfo_r->u1PedalSimPressGenOn == TRUE)
			|| (stVlvCtrlInfo_r->s8PCtrlState == S8_PRESS_PREBOOST)
			|| (stVlvCtrlInfo_r->s8PCtrlState == S8_PRESS_BOOST)
			|| (((stVlvCtrlInfo_r->u1BLS == 1) && (stVlvCtrlInfo_r->u1BLSFaultDet == 0)) && ((stVlvCtrlInfo_r->u8PedalSigTag == 2) || (stVlvCtrlInfo_r->u8PedalSigTag == 7))))
	{
		u8DecidePdlVlvState = U8_VALVE_STATE_INITIAL;
	}
	else
	{
		u8DecidePdlVlvState = U8_VALVE_STATE_READY;
	}

	return u8DecidePdlVlvState;
}

static uint8_t LAPCT_u8GetPdlVlvStFromInitial(DECIDE_VALVE_CTRL_MODE_INPUT_t* stVlvCtrlInfo_r)
{
	uint8_t u8DecidePdlVlvState = U8_VALVE_STATE_READY;

	if (stVlvCtrlInfo_r->u8InhibitFlg == TRUE)
	{
		u8DecidePdlVlvState = U8_VALVE_STATE_READY;
	}
	else if ((stVlvCtrlInfo_r->s8PCtrlState == S8_PRESS_PREBOOST) || (stVlvCtrlInfo_r->s8PCtrlState == S8_PRESS_BOOST))
	{
		u8DecidePdlVlvState = U8_VALVE_STATE_ACTIVE;
	}
	else if (stVlvCtrlInfo_r->u1PedalSimPressGenOn == FALSE)
	{
		u8DecidePdlVlvState = U8_VALVE_STATE_READY;
	}
	else
	{
		u8DecidePdlVlvState = U8_VALVE_STATE_INITIAL;
	}

	return u8DecidePdlVlvState;
}

static uint8_t LAPCT_u8GetPdlVlvStFromActive(DECIDE_VALVE_CTRL_MODE_INPUT_t* stVlvCtrlInfo_r)
{
	uint8_t u8DecidePdlVlvState = U8_VALVE_STATE_READY;

	if (stVlvCtrlInfo_r->u8InhibitFlg == TRUE)
	{
		u8DecidePdlVlvState = U8_VALVE_STATE_READY;
	}
	else if ((stVlvCtrlInfo_r->s8PCtrlState == S8_PRESS_PREBOOST)
			|| (stVlvCtrlInfo_r->s8PCtrlState == S8_PRESS_BOOST)
			|| (((stVlvCtrlInfo_r->u1BLS == 1) && (stVlvCtrlInfo_r->u1BLSFaultDet == 0)) && ((stVlvCtrlInfo_r->u8PedalSigTag == 2) || (stVlvCtrlInfo_r->u8PedalSigTag == 7))))
	{
		u8DecidePdlVlvState = U8_VALVE_STATE_ACTIVE;
	}
	else if (stVlvCtrlInfo_r->u1PedalSimPressGenOnEscOld == TRUE)
	{
		/* Quick Valve Current Off */
		u8DecidePdlVlvState = U8_VALVE_STATE_READY;
	}
	else if (stVlvCtrlInfo_r->u8PdlVlvActHoldOnRequestFlg == TRUE)
	{
		u8DecidePdlVlvState = U8_VALVE_STATE_MAINTAIN;
	}
	else
	{
		u8DecidePdlVlvState = U8_VALVE_STATE_READY;
	}

	return u8DecidePdlVlvState;
}

static uint8_t LAPCT_u8GetPdlVlvStFromMaintain(DECIDE_VALVE_CTRL_MODE_INPUT_t* stVlvCtrlInfo_r)
{
	uint8_t u8DecidePdlVlvState = U8_VALVE_STATE_READY;

	if (stVlvCtrlInfo_r->u8InhibitFlg == TRUE)
	{
		u8DecidePdlVlvState = U8_VALVE_STATE_READY;
	}
	else if (stVlvCtrlInfo_r->u8PdlVlvActHoldOnRequestFlg == TRUE)
	{
		u8DecidePdlVlvState = U8_VALVE_STATE_MAINTAIN;
	}
	else if ((stVlvCtrlInfo_r->s8PCtrlState == S8_PRESS_PREBOOST)
			|| (stVlvCtrlInfo_r->s8PCtrlState == S8_PRESS_BOOST)
			|| (((stVlvCtrlInfo_r->u1BLS == 1) && (stVlvCtrlInfo_r->u1BLSFaultDet == 0)) && ((stVlvCtrlInfo_r->u8PedalSigTag == 2) || (stVlvCtrlInfo_r->u8PedalSigTag == 7))))
	{
		u8DecidePdlVlvState = U8_VALVE_STATE_ACTIVE;
	}
	else
	{
		u8DecidePdlVlvState = U8_VALVE_STATE_READY;
	}

	return u8DecidePdlVlvState;
}

static uint8_t LAPCT_u8DecideCutValveRespLv(DECIDE_VALVE_CTRL_MODE_INPUT_t * stVlvCtrlInfo_r )
{
	uint8_t u8DecideCutVlvRespLv = U8_VLV_RESPONSE_LV_NORMAL;
	uint8_t u8DecideCutVlvRespLvByPdl = U8_VLV_RESPONSE_LV_NORMAL;

	switch (stVlvCtrlInfo_r->s8PCtrlBoostMode)
	{
	case S8_BOOST_MODE_RESPONSE_L2:/* ESC oversteer (Include ROP), TCS_1_mode : 300bar/s 			*/
	case S8_BOOST_MODE_RESPONSE_L3:/* TCS_2_mode, TSP, ESC understeer : 200bar/s  					*/
		u8DecideCutVlvRespLv = U8_VLV_RESPONSE_LV_QUICK;
		break;

	case S8_BOOST_MODE_RESPONSE_L0:/* ABS Torque Control Mode 										*/
	case S8_BOOST_MODE_RESPONSE_L1:/* Quick Braking , AEB : 400bar/s 								*/
	case S8_BOOST_MODE_RESPONSE_L13:/* ABS/EBD : Map Only											*/
	case S8_BOOST_MODE_RESPONSE_L5:/* Decel control : BBC, RBC : 200 bar/s Normal Speed			*/
	case S8_BOOST_MODE_RESPONSE_L8:/* CBC SLS, EBD 200bar/s Reduce Pressure Control function		*/
	case S8_BOOST_MODE_RESPONSE_L10:/* Release of Hold function										*/
	case S8_BOOST_MODE_RESPONSE_L11:/* ABS Torque to Normal											*/
		u8DecideCutVlvRespLv = U8_VLV_RESPONSE_LV_NORMAL;
		break;

	case S8_BOOST_MODE_RESPONSE_L4:/* Decel control : SCC : 200 bar/s dependent on speed			*/
	case S8_BOOST_MODE_RESPONSE_L6:/* Decel control : BBC, RBC : 200 bar/s Low Speed(Creep)			*/
	case S8_BOOST_MODE_RESPONSE_L9:/* TCS_4_mode, Hold function(HSA, AVH)AC Hold 100bar/s			*/
	case S8_BOOST_MODE_RESPONSE_L12:/* PreBoost Mode												*/
		u8DecideCutVlvRespLv = U8_VLV_RESPONSE_LV_MID;
		break;

	case S8_BOOST_MODE_RESPONSE_L7:/* Decel control : BBC, RBC : 200 bar/s Stop Speed				*/
		u8DecideCutVlvRespLv = U8_VLV_RESPONSE_LV_SLOW;
		break;
	}

	u8DecideCutVlvRespLvByPdl = LAPCT_u8GetCutVlvRespLvByPdl(stVlvCtrlInfo_r);

	if (u8DecideCutVlvRespLvByPdl == U8_VLV_RESPONSE_LV_SPECIAL)
	{
		u8DecideCutVlvRespLv = U8_VLV_RESPONSE_LV_SPECIAL;
	}
	else if (u8DecideCutVlvRespLvByPdl > u8DecideCutVlvRespLv)
	{
		u8DecideCutVlvRespLv = u8DecideCutVlvRespLvByPdl;
	}
	else { }

	return u8DecideCutVlvRespLv;
}

static uint8_t LAPCT_u8GetCutVlvRespLvByPdl(DECIDE_VALVE_CTRL_MODE_INPUT_t* stVlvCtrlInfo_r)
{
	uint8_t u8DecideCutVlvRespLvByPdl = U8_VLV_RESPONSE_LV_NORMAL;

	if (stVlvCtrlInfo_r->s16VehicleSpeed < S16_VALVE_NOISE_REDUCE_SPEED)
	{
		if (stVlvCtrlInfo_r->s16PedalTravelRateMmPSec < S16_TRAVEL_10_MM)
		{
			u8DecideCutVlvRespLvByPdl = U8_VLV_RESPONSE_LV_SLOW;
		}
		else if (stVlvCtrlInfo_r->s16PedalTravelRateMmPSec < S16_TRAVEL_15_MM)
		{
			u8DecideCutVlvRespLvByPdl = U8_VLV_RESPONSE_LV_MID;
		}
		else
		{
			u8DecideCutVlvRespLvByPdl = U8_VLV_RESPONSE_LV_NORMAL;
		}
	}
	else
	{
		u8DecideCutVlvRespLvByPdl = U8_VLV_RESPONSE_LV_NORMAL;
	}

	if (stVlvCtrlInfo_r->u8CutCloseDelayForQuickBrk == TRUE)
	{
		u8DecideCutVlvRespLvByPdl = U8_VLV_RESPONSE_LV_SPECIAL;
	}
	else { }

	return u8DecideCutVlvRespLvByPdl;
}

static uint8_t LAPCT_u8DecideSimValveRespLv(DECIDE_VALVE_CTRL_MODE_INPUT_t * stVlvCtrlInfo_r )
{
	uint8_t u8DecideSimVlvRespLv = U8_VLV_RESPONSE_LV_NORMAL;
	uint8_t u8DecideSimVlvRespLvByPdl = U8_VLV_RESPONSE_LV_NORMAL;

	switch (stVlvCtrlInfo_r->s8PCtrlBoostMode)
	{
	case S8_BOOST_MODE_RESPONSE_L2:/* ESC oversteer (Include ROP), TCS_1_mode : 300bar/s 			*/
	case S8_BOOST_MODE_RESPONSE_L3:/* TCS_2_mode, TSP, ESC understeer : 200bar/s  					*/
		u8DecideSimVlvRespLv = U8_VLV_RESPONSE_LV_NORMAL;
		break;

	case S8_BOOST_MODE_RESPONSE_L0:/* ABS Torque Control Mode 										*/
	case S8_BOOST_MODE_RESPONSE_L1:/* Quick Braking , AEB : 400bar/s 								*/
	case S8_BOOST_MODE_RESPONSE_L5:/* Decel control : BBC, RBC : 200 bar/s Normal Speed			*/
	case S8_BOOST_MODE_RESPONSE_L8:/* CBC SLS, EBD 200bar/s Reduce Pressure Control function		*/
	case S8_BOOST_MODE_RESPONSE_L10:/* Release of Hold function										*/
	case S8_BOOST_MODE_RESPONSE_L11:/* ABS Torque to Normal											*/
	case S8_BOOST_MODE_RESPONSE_L13:/* ABS/EBD : Map Only											*/
		u8DecideSimVlvRespLv = U8_VLV_RESPONSE_LV_NORMAL;
		break;

	case S8_BOOST_MODE_RESPONSE_L4:/* Decel control : SCC : 200 bar/s dependent on speed			*/
	case S8_BOOST_MODE_RESPONSE_L6:/* Decel control : BBC, RBC : 200 bar/s Low Speed(Creep)			*/
	case S8_BOOST_MODE_RESPONSE_L9:/* TCS_4_mode, Hold function(HSA, AVH)AC Hold 100bar/s			*/
	case S8_BOOST_MODE_RESPONSE_L12:/* PreBoost Mode												*/
		u8DecideSimVlvRespLv = U8_VLV_RESPONSE_LV_MID;
		break;

	case S8_BOOST_MODE_RESPONSE_L7:/* Decel control : BBC, RBC : 200 bar/s Stop Speed				*/
		u8DecideSimVlvRespLv = U8_VLV_RESPONSE_LV_SLOW;
		break;
	}

	u8DecideSimVlvRespLvByPdl = LAPCT_u8GetSimVlvRespLvByPdl(stVlvCtrlInfo_r);

	if (u8DecideSimVlvRespLvByPdl == U8_VLV_RESPONSE_LV_SPECIAL)
	{
		u8DecideSimVlvRespLv = U8_VLV_RESPONSE_LV_SPECIAL;
	}
	else if (u8DecideSimVlvRespLvByPdl > u8DecideSimVlvRespLv)
	{
		u8DecideSimVlvRespLv = u8DecideSimVlvRespLvByPdl;
	}
	else { }

	return u8DecideSimVlvRespLv;
}

static uint8_t LAPCT_u8GetSimVlvRespLvByPdl(DECIDE_VALVE_CTRL_MODE_INPUT_t* stVlvCtrlInfo_r)
{
	uint8_t u8DecideSimVlvRespLvByPdl = U8_VLV_RESPONSE_LV_NORMAL;

	if (stVlvCtrlInfo_r->u8CutCloseDelayForQuickBrk == TRUE)
	{
		u8DecideSimVlvRespLvByPdl = U8_VLV_RESPONSE_LV_SPECIAL;
	}
	else if (stVlvCtrlInfo_r->s16PedalTravelRateMmPSec >= S16_CLOSE_CV_FAST_APPLY_REF)
	{
		u8DecideSimVlvRespLvByPdl = U8_VLV_RESPONSE_LV_SPECIAL;
	}
	else { }

	return u8DecideSimVlvRespLvByPdl;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_u16GetSimValveMaxHoldTime
* CALLED BY:            LAPCT_stPedalFeelGenVlvCtrlMain
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Set sim valve max Hold Time
********************************************************************************/
static uint16_t LAPCT_u16GetSimValveMaxHoldTime(uint8_t u8SimVlvCtrlState, int16_t s16CircuitP, int16_t s16PSPress)
{
	static uint16_t lapctu16SimCurrentMaxCnt = 0;

	if( (s16CircuitP + S16_SIMV_NOT_OPEN_DECTECT_THRES) < s16PSPress )
	{
		lapctu16SimCurrentMaxCnt = (uint16_t)L_s16IInter2Point(s16PSPress, S16_MPRESS_90_BAR, U8_T_100_MS, S16_MPRESS_120_BAR, U8_T_500_MS);
	}
	else
	{
		lapctu16SimCurrentMaxCnt = U8_T_100_MS;
	}

    return lapctu16SimCurrentMaxCnt;
}

static uint8_t LAPCT_u8DctCutVLVLongOpen(uint8_t u8Cut1VlvCtrlState, uint8_t u8Cut2VlvCtrlState)
{
	static uint16_t lapctu16Cut1VlvOpenCnt = 0;
	static uint16_t lapctu16Cut2VlvOpenCnt = 0;
	uint8_t u8CutVVLongOpenFlg = FALSE;

	if (u8Cut1VlvCtrlState == U8_VALVE_STATE_READY)
	{
		lapctu16Cut1VlvOpenCnt = lapctu16Cut1VlvOpenCnt + U8_T_5_MS;
		lapctu16Cut1VlvOpenCnt = (lapctu16Cut1VlvOpenCnt >= U16_CUT_VLV_LONG_OPEN_DCT_TIME) ? U16_CUT_VLV_LONG_OPEN_DCT_TIME : lapctu16Cut1VlvOpenCnt;
	}
	else
	{
		lapctu16Cut1VlvOpenCnt = U8_T_0_MS;
	}

	if (u8Cut2VlvCtrlState == U8_VALVE_STATE_READY)
	{
		lapctu16Cut2VlvOpenCnt = lapctu16Cut2VlvOpenCnt + U8_T_5_MS;
		lapctu16Cut2VlvOpenCnt = (lapctu16Cut2VlvOpenCnt >= U16_CUT_VLV_LONG_OPEN_DCT_TIME) ? U16_CUT_VLV_LONG_OPEN_DCT_TIME : lapctu16Cut2VlvOpenCnt;
	}
	else
	{
		lapctu16Cut2VlvOpenCnt = U8_T_0_MS;
	}
	u8CutVVLongOpenFlg = ((lapctu16Cut1VlvOpenCnt >= U16_CUT_VLV_LONG_OPEN_DCT_TIME) && (lapctu16Cut2VlvOpenCnt >= U16_CUT_VLV_LONG_OPEN_DCT_TIME)) ? TRUE : FALSE;

	return u8CutVVLongOpenFlg;
}
#define PCT_5MSCTRL_STOP_SEC_CODE
#include "Pct_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
