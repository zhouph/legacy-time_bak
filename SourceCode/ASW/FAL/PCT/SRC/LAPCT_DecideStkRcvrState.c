/**
 * @defgroup LAIDB_DecideValveCtrlMode LAIDB_DecideValveCtrlMode
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LAIDB_DecideValveCtrlMode.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "../HDR/Pct_5msCtrl.h"
#include "LAPCT_DecideStkRcvrTime.h"
#include "LAPCT_ChkPosnForStkRcvr.h"
#include "LAPCT_DecideStkRcvrState.h"
#include "MTR/LAPCT_LoadMapData.h"

/*#include <Mando_Std_Types.h>*/

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
	int16_t lapcts16StkRcvrAllowedTime;
    int16_t lapcts16AllowedRcvrStk_mm;
    int16_t lapcts16FwdRcvrMaxPosn_mm;
    int16_t lapcts16BackwRcvrMinPosn_mm;
    int16_t lapcts16StkPosn_mm;
    int32_t lapcts32StkRecvryStabnEndOK;
	#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
	int16_t lapcts16PistonP;
	#else
	int16_t lapcts16PriCircuitP;
	int16_t lapcts16SecCircuitP;
	#endif

    uint32_t lapctu1AbsActFlg				:1;
    uint32_t lapctu1EscActFlg				:1;
    uint32_t lapctu1TcsActFlg				:1;
    uint32_t lapctu1AbsDefectFlg			:1;
    uint32_t lapctu1EscDefectFlg			:1;
    uint32_t lapctu1TcsDefectFlg			:1;
}GetStkRcvrStateInfo_t;

typedef struct
{
    int8_t lapcts8RecommendStkRcvrLvl;
    int8_t lapcts8StkRecvryCtrlState;
    int16_t lapcts16StkRecvryRequestedTime;
}SetStkRcvrStateInfo_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_CONST_UNSPECIFIED
#include "Pct_MemMap.h"
/** Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Pct_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_START_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/** Variable Section (32BIT)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

FAL_STATIC GetStkRcvrStateInfo_t			        GetStkRcvrState;
FAL_STATIC SetStkRcvrStateInfo_t			        SetStkRcvrState;
FAL_STATIC int16_t lapcts16StrkRcvrActCnt = 0;
FAL_STATIC int16_t lapcts16StabnTargetP = 0;
FAL_STATIC int16_t lapcts16PistonPOld = 0;
FAL_STATIC int16_t lapcts16StrkRecvrStabnPressCnt = 0;
FAL_STATIC int16_t lapcts16StrkRcvrAftActCnt = 0;
FAL_STATIC int16_t lapcts16StrkStbn1StEndTime = 0;
FAL_STATIC int16_t lapcts16StrkRcvr2StEndTime = 0;

FAL_STATIC U32_BIT_STRUCT_t	PCT_DECIDE_STK_RCVR_ST;

#define lapctu1InhibitRcvr                PCT_DECIDE_STK_RCVR_ST.bit31
#define lapctu1EmgyStrkRcvrRequestFlg	  PCT_DECIDE_STK_RCVR_ST.bit30
#define lapctu1RcvrFastEndForFwdDir		  PCT_DECIDE_STK_RCVR_ST.bit29
#define lapctu1RcvrFastEndForBackwDir     PCT_DECIDE_STK_RCVR_ST.bit28
#define lapctu1MotorRcvrEndOk		      PCT_DECIDE_STK_RCVR_ST.bit27
#define lapctu1HaltStkRcvrFlg		      PCT_DECIDE_STK_RCVR_ST.bit26

#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
#define PCT_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PCT_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
#define PCT_5MSCTRL_START_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/** Variable Section (32BIT)**/


#define PCT_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Pct_MemMap.h"


/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_CODE
#include "Pct_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/

static void LAPCT_vCallStkRcvrCtrl(void);
static void LAPCT_vDecideStkRecoveryCtrl(void);
static void LAPCT_vDcdStrkRecvrInhibit(void);
static void LAPCT_vCheckStrkRcvrEmgyStrkCdn(void);
static void LAPCT_vCheckStrkRcvrQuickEndCdn(void);
static void LAPCT_vCheckStrkRcvrHaltCdn(void);
static int8_t LAPCT_s8DecideStrkRecoveryState(void);
static int8_t LAPCT_s8CheckReadyToRecoverySt(void);
static int8_t LAPCT_s8CheckRecoveryToStabn1St(void);
 #if __NEED_TO_STABN2_STATE == ENABLE
static int8_t LAPCT_s8CheckStabn1ToStabn2St(void);
static int8_t LAPCT_s8CheckStabn2ToReadySt(void);
 #else
static int8_t LAPCT_s8CheckStabn1ToReadySt(void);
 #endif

 #if __DECIDE_STABILIZE_REF_PRESS ==ENABLE
static uint8_t LCABS_u8CheckStabnEndOkRefPress(void);
 #endif

static int8_t LAPCT_s8DcdStkRcvrRecommendLvl(int16_t s16PosnUSL,int16_t s16StkPosn);
static int16_t LAPCT_s16DcdStkRcvrRequestTime(int8_t s8StkRcvrCtrlState, int16_t s16StkRcvrAllowedTime);

static void LAPCT_vCallStkRcvrCtrlInput(void);
static void LAPCT_vCallStkRcvrCtrlOutput(void);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void LAPCT_vCallStkRcvrMain(void)
{
	LAPCT_vCallPosnForStkRcvr();

	LAPCT_vCallStrkRcvrTime();

	LAPCT_vCallStkRcvrCtrl();
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static void LAPCT_vCallStkRcvrCtrl(void)
{
	LAPCT_vCallStkRcvrCtrlInput();

	LAPCT_vDecideStkRecoveryCtrl();

    LAPCT_vCallStkRcvrCtrlOutput();
}

static void LAPCT_vDecideStkRecoveryCtrl(void)
{
	LAPCT_vDcdStrkRecvrInhibit(); /* to check condition to do stroke recovery */

	LAPCT_vCheckStrkRcvrEmgyStrkCdn();

	LAPCT_vCheckStrkRcvrHaltCdn();

	LAPCT_vCheckStrkRcvrQuickEndCdn();

	SetStkRcvrState.lapcts16StkRecvryRequestedTime = LAPCT_s16DcdStkRcvrRequestTime(SetStkRcvrState.lapcts8StkRecvryCtrlState, GetStkRcvrState.lapcts16StkRcvrAllowedTime);

	SetStkRcvrState.lapcts8StkRecvryCtrlState = LAPCT_s8DecideStrkRecoveryState();

	SetStkRcvrState.lapcts8RecommendStkRcvrLvl = LAPCT_s8DcdStkRcvrRecommendLvl(GetStkRcvrState.lapcts16FwdRcvrMaxPosn_mm, GetStkRcvrState.lapcts16StkPosn_mm);

#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
	lapcts16PistonPOld = GetStkRcvrState.lapcts16PistonP;
#else
	if(GetStkRcvrState.lapcts16PriCircuitP > GetStkRcvrState.lapcts16SecCircuitP)
	{
		lapcts16PistonPOld = GetStkRcvrState.lapcts16PriCircuitP;
	}
	else
	{
		lapcts16PistonPOld = GetStkRcvrState.lapcts16SecCircuitP;
	}
#endif
}


static void LAPCT_vCallStkRcvrCtrlInput(void)
{
	GetStkRcvrState.lapcts16StkRcvrAllowedTime = PressCTRLLocalBus.s16StkRcvrFinalAllowedTime;

	GetStkRcvrState.lapcts16AllowedRcvrStk_mm = PressCTRLLocalBus.s16AllowedRcvrStk_mm;
	GetStkRcvrState.lapcts16FwdRcvrMaxPosn_mm = PressCTRLLocalBus.s16FwdRcvrMaxPosn_mm;
	GetStkRcvrState.lapcts16BackwRcvrMinPosn_mm = PressCTRLLocalBus.s16BackwRcvrMinPosn_mm;

	GetStkRcvrState.lapctu1AbsActFlg = Pct_5msCtrlBus.Pct_5msCtrlAbsCtrlInfo.AbsActFlg;
	GetStkRcvrState.lapctu1EscActFlg = Pct_5msCtrlBus.Pct_5msCtrlEscCtrlInfo.EscActFlg;
	GetStkRcvrState.lapctu1TcsActFlg = Pct_5msCtrlBus.Pct_5msCtrlTcsCtrlInfo.TcsActFlg;
	GetStkRcvrState.lapctu1AbsDefectFlg = Pct_5msCtrlBus.Pct_5msCtrlAbsCtrlInfo.AbsDefectFlg;
	GetStkRcvrState.lapctu1EscDefectFlg = Pct_5msCtrlBus.Pct_5msCtrlEscCtrlInfo.EscDefectFlg;
	GetStkRcvrState.lapctu1TcsDefectFlg = Pct_5msCtrlBus.Pct_5msCtrlTcsCtrlInfo.TcsDefectFlg;

	GetStkRcvrState.lapcts16StkPosn_mm = PressCTRLLocalBus.s16StkPosn_mm;
	GetStkRcvrState.lapcts32StkRecvryStabnEndOK = Pct_5msCtrlBus.Pct_5msCtrlStkRecvryStabnEndOK;
	#if __DECIDE_STABILIZE_REF_PRESS == ENABLE

	 #if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
	GetStkRcvrState.lapcts16PistonP            			= (int16_t) Pct_5msCtrlBus.Pct_5msCtrlPistPFildInfo.PistPFild;;
	 #else
	GetStkRcvrState.lapcts16PriCircuitP     			= (int16_t) Pct_5msCtrlBus.Pct_5msCtrlCircPFildInfo.PrimCircPFild;
	GetStkRcvrState.lapcts16SecCircuitP     			= (int16_t) Pct_5msCtrlBus.Pct_5msCtrlCircPFildInfo.PrimCircPFild;
	 #endif

	#endif
}

static void LAPCT_vCallStkRcvrCtrlOutput(void)
{
	Pct_5msCtrlBus.Pct_5msCtrlStkRecvryActnIfInfo.RecommendStkRcvrLvl = SetStkRcvrState.lapcts8RecommendStkRcvrLvl;
	Pct_5msCtrlBus.Pct_5msCtrlStkRecvryActnIfInfo.StkRecvryCtrlState  = SetStkRcvrState.lapcts8StkRecvryCtrlState;

	if((SetStkRcvrState.lapcts8StkRecvryCtrlState == S8_STRK_RCVR_READY_STATE) || (SetStkRcvrState.lapcts8StkRecvryCtrlState == S8_STRK_RCVR_INHIBIT_STATE))
	{
		Pct_5msCtrlBus.Pct_5msCtrlStkRecvryActnIfInfo.StkRecvryRequestedTime = U8_T_0_MS;
	}
	else
	{
		Pct_5msCtrlBus.Pct_5msCtrlStkRecvryActnIfInfo.StkRecvryRequestedTime = SetStkRcvrState.lapcts16StkRecvryRequestedTime;
	}
}

/******************************************************************************
* FUNCTION NAME:      LAPCT_vDcdStrkRecvrInhibit
* CALLED BY:          LAPCT_vDecideStkRecoveryCtrl()
* Preconditions:      none
* PARAMETER:		  none
* RETURN VALUE:       none
* Description:        Decide inhibition of the stroke recovery control
******************************************************************************/
static void LAPCT_vDcdStrkRecvrInhibit(void)
{
	lapctu1InhibitRcvr = 0;
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_vCheckStrkRcvrEmgyStrkCdn
* CALLED BY:            LAPCT_vDecideStkRecoveryCtrl
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:			none
* Description:          Check Stroke Recovery Emergency Start Condition
********************************************************************************/
static void LAPCT_vCheckStrkRcvrEmgyStrkCdn(void)
{
	if(GetStkRcvrState.lapcts16StkPosn_mm > S16_EMGY_STRK_RCVR_ENT_POSI)
	{
		lapctu1EmgyStrkRcvrRequestFlg = 1;
	}
	else if(GetStkRcvrState.lapcts16StkPosn_mm < S16_EMGY_STRK_RCVR_ENT_POSI - S16_RECOVERY_MIN_STROKE)
	{
		lapctu1EmgyStrkRcvrRequestFlg = 0;
	}
	else {}
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_vCheckStrkRcvrQuickEndCdn
* CALLED BY:            LAPCT_vDecideStkRecoveryCtrl
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:			none
* Description:          Check Stroke Recovery Quick End Condition
********************************************************************************/
static void LAPCT_vCheckStrkRcvrQuickEndCdn(void)
{
	uint8_t u8DriverIntendToRelBrkEdge = 0;

	if(GetStkRcvrState.lapcts16StkRcvrAllowedTime == S16_RECOVERY_END_REQ_TIME_PCT5MS)
	{
		lapctu1RcvrFastEndForFwdDir = 1;
		lapctu1RcvrFastEndForBackwDir = 0;
	}
	else if(lapctu1HaltStkRcvrFlg == 1)
	{
		lapctu1RcvrFastEndForFwdDir = 1;
		lapctu1RcvrFastEndForBackwDir = 0;
	}
	else if((GetStkRcvrState.lapcts16StkPosn_mm < GetStkRcvrState.lapcts16BackwRcvrMinPosn_mm) || (GetStkRcvrState.lapcts16StkPosn_mm < S16_EMGY_STRK_RCVR_EXIT_POSI))
	{
		lapctu1RcvrFastEndForFwdDir = 1;
		lapctu1RcvrFastEndForBackwDir = 0;
	}
	else
	{
		lapctu1RcvrFastEndForFwdDir = 0;
		lapctu1RcvrFastEndForBackwDir = 0;
	}
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_vCheckStrkRcvrHaltCdn
* CALLED BY:            LAPCT_vDecideStkRecoveryCtrl
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:			none
* Description:          Check Stroke Recovery Quick End Condition
********************************************************************************/
static void LAPCT_vCheckStrkRcvrHaltCdn(void)
{
	if(((GetStkRcvrState.lapctu1AbsActFlg == 1) && (GetStkRcvrState.lapctu1AbsDefectFlg == 0))
		|| ((GetStkRcvrState.lapctu1EscActFlg == 1) && (GetStkRcvrState.lapctu1EscDefectFlg == 0))
		|| ((GetStkRcvrState.lapctu1TcsActFlg == 1) && (GetStkRcvrState.lapctu1TcsDefectFlg == 0)))
	{
		lapctu1HaltStkRcvrFlg = 0;
	}
	else
	{
		lapctu1HaltStkRcvrFlg = 1;
	}
}

/*******************************************************************************
* FUNCTION NAME:        LAPCT_s8DecideStrkRecoveryState
* CALLED BY:            LAPCT_vDecideStkRecoveryCtrl
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         s8StkRcvrCtrlState
* Description:          Decide Stroke Recovery State
********************************************************************************/
static int8_t LAPCT_s8DecideStrkRecoveryState(void)
{
	int8_t s8StkRcvrCtrlStateOld = SetStkRcvrState.lapcts8StkRecvryCtrlState;
	int8_t s8StkRcvrCtrlState = S8_STRK_RCVR_READY_STATE;
	int8_t s8ReadyToRecoveryOK = 0;
	int8_t s8RecoveryToStabn1OK = 0;
	 #if __NEED_TO_STABN2_STATE == ENABLE
	int8_t s8Stabn1ToStabn2OK = 0;
	int8_t s8Stabn2ToReadyOK = 0;
	 #else
	int8_t s8Stabn1ToReadyOK = 0;
	 #endif

	switch(s8StkRcvrCtrlStateOld)
	{
		case S8_STRK_RCVR_READY_STATE:

	    	if(lapctu1InhibitRcvr == 1)
	    	{
	    		s8StkRcvrCtrlState = S8_STRK_RCVR_INHIBIT_STATE;
	    	}
			else
			{
				s8ReadyToRecoveryOK = LAPCT_s8CheckReadyToRecoverySt();

				if(s8ReadyToRecoveryOK == 1)
				{
					s8StkRcvrCtrlState = S8_STRK_RCVR_RECOVERY1_STATE;
					#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
					lapcts16StabnTargetP = GetStkRcvrState.lapcts16PistonP;
					#else
					if(GetStkRcvrState.lapcts16PriCircuitP > GetStkRcvrState.lapcts16SecCircuitP)
					{
						lapcts16StabnTargetP = GetStkRcvrState.lapcts16PriCircuitP;
					}
					else
					{
						lapcts16StabnTargetP = GetStkRcvrState.lapcts16SecCircuitP;
					}
					#endif
				}
				else
				{
					s8StkRcvrCtrlState = S8_STRK_RCVR_READY_STATE;
					lapcts16StabnTargetP = 0;
				}
			}

		break;

	    case S8_STRK_RCVR_RECOVERY1_STATE:

	    	if(lapctu1InhibitRcvr == 1)
	    	{
	    		s8StkRcvrCtrlState = S8_STRK_RCVR_INHIBIT_STATE;
	    	}
	    	else
	    	{
	    		if((lapctu1EmgyStrkRcvrRequestFlg == 0) && ((lapctu1RcvrFastEndForBackwDir == 1) || (lapctu1RcvrFastEndForFwdDir == 1)))
	    		{
	    			s8StkRcvrCtrlState = S8_STRK_RCVR_READY_STATE;
	    		}
	    		else if(lapcts16StrkRcvrActCnt >= U8_T_20_MS)
	    		{
	    			s8StkRcvrCtrlState = S8_STRK_RCVR_RECOVERY2_STATE;
	    		}
	    		else
	    		{
	    			s8StkRcvrCtrlState = S8_STRK_RCVR_RECOVERY1_STATE;
	    		}

	    	}

	    break;

	    case S8_STRK_RCVR_RECOVERY2_STATE:

	    	lapcts16StrkRcvr2StEndTime = 0;

	    	if(lapctu1InhibitRcvr == 1)
	    	{
	    		s8StkRcvrCtrlState = S8_STRK_RCVR_INHIBIT_STATE;
	    	}
	    	else
	    	{
	    		s8RecoveryToStabn1OK = LAPCT_s8CheckRecoveryToStabn1St();

	    		if(s8RecoveryToStabn1OK == 1)
	    		{
	    			s8StkRcvrCtrlState = S8_STRK_RCVR_STABILIZING1_STATE;
	    			lapcts16StrkRcvr2StEndTime = lapcts16StrkRcvrActCnt;
	    		}
	    		else
	    		{
	    			s8StkRcvrCtrlState = S8_STRK_RCVR_RECOVERY2_STATE;
	    		}
		    }

	    break;

	    case S8_STRK_RCVR_STABILIZING1_STATE:
			 #if __NEED_TO_STABN2_STATE == ENABLE
	    	lapcts16StrkStbn1StEndTime = 0;

	    	if(lapctu1InhibitRcvr == 1)
	    	{
	    		s8StkRcvrCtrlState = S8_STRK_RCVR_INHIBIT_STATE;
	    	}
	    	else
			{
	    		s8Stabn1ToStabn2OK = LAPCT_s8CheckStabn1ToStabn2St();

		    	if(s8Stabn1ToStabn2OK == 1)
		    	{
		    		s8StkRcvrCtrlState = S8_STRK_RCVR_STABILIZING2_STATE;
		    		lapcts16StrkStbn1StEndTime = lapcts16StrkRcvrActCnt;
		    	}
		    	else
		    	{
		    		s8StkRcvrCtrlState = S8_STRK_RCVR_STABILIZING1_STATE;
		    	}
		    }
			 #else
	    	if(lapctu1InhibitRcvr == 1)
	    	{
	    		s8StkRcvrCtrlState = S8_STRK_RCVR_INHIBIT_STATE;
	    	}
	    	else
			{
	    		s8Stabn1ToReadyOK = LAPCT_s8CheckStabn1ToReadySt();

		    	if(s8Stabn1ToReadyOK == 1)
		    	{
		    		s8StkRcvrCtrlState = S8_STRK_RCVR_READY_STATE;
		    	}
		    	else
		    	{
		    		s8StkRcvrCtrlState = S8_STRK_RCVR_STABILIZING1_STATE;
		    	}
		    }
			 #endif
	    break;
		 #if __NEED_TO_STABN2_STATE == ENABLE
	    case S8_STRK_RCVR_STABILIZING2_STATE:

	    	if(lapctu1InhibitRcvr == 1)
	    	{
	    		s8StkRcvrCtrlState = S8_STRK_RCVR_INHIBIT_STATE;
	    	}
	    	else
			{
	    		s8Stabn2ToReadyOK = LAPCT_s8CheckStabn2ToReadySt();

		    	if(s8Stabn2ToReadyOK == 1)
		    	{
		    		s8StkRcvrCtrlState = S8_STRK_RCVR_READY_STATE;
		    	}
		    	else
		    	{
		    		s8StkRcvrCtrlState = S8_STRK_RCVR_STABILIZING2_STATE;
		    	}
		    }

	    break;
		 #endif
	    case S8_STRK_RCVR_INHIBIT_STATE:

	    	if(lapctu1InhibitRcvr == 1)
	    	{
	    		s8StkRcvrCtrlState = S8_STRK_RCVR_INHIBIT_STATE;
	    	}
	    	else
	    	{
		    	s8StkRcvrCtrlState = S8_STRK_RCVR_READY_STATE;
	    	}

	    break;

	    default:

	    	s8StkRcvrCtrlState = S8_STRK_RCVR_READY_STATE;

	    break;
	}

	if(s8StkRcvrCtrlState == S8_STRK_RCVR_READY_STATE)
	{
		lapcts16StrkRcvrActCnt = 0;
		lapcts16StrkRcvrAftActCnt = (lapcts16StrkRcvrAftActCnt <= 0) ? 0 : (lapcts16StrkRcvrAftActCnt - U8_T_5_MS);
		lapcts16StrkRcvr2StEndTime = 0;
		lapcts16StrkStbn1StEndTime = 0;
	}
	else
	{
		lapcts16StrkRcvrActCnt = lapcts16StrkRcvrActCnt + U8_T_5_MS;
		lapcts16StrkRcvrAftActCnt = U8_T_150_MS;
	}

	return s8StkRcvrCtrlState;
}

/******************************************************************************
* FUNCTION NAME:      LAPCT_s8CheckReadyToRecoverySt
* CALLED BY:          LCABS_vDecideStrkRecoveryState()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       s8Ready2RcvrSatisfy
* Description:        Check Start to Stroke Recovery
******************************************************************************/
static int8_t LAPCT_s8CheckReadyToRecoverySt(void)
{
	int8_t s8Ready2RcvrSatisfy = 0;
	int8_t s8NeedToStkRcvrForSystemFail = 0;


	if(s8NeedToStkRcvrForSystemFail == 1)
	{
		if((lapctu1EmgyStrkRcvrRequestFlg == 1) && (GetStkRcvrState.lapcts16AllowedRcvrStk_mm > S16_RECOVERY_MIN_STROKE)) /*should be considered to refer to GetStkRcvrState.lapcts16AllowedRcvrStk_mm?*/
		{
			s8Ready2RcvrSatisfy = 1;
		}
		else if(lapctu1HaltStkRcvrFlg == 1)
		{
			s8Ready2RcvrSatisfy = 0;
		}
		else if(lapcts16StrkRcvrAftActCnt > U8_T_0_MS)
		{
			s8Ready2RcvrSatisfy = 0;
		}
		else if((GetStkRcvrState.lapcts16AllowedRcvrStk_mm > S16_RECOVERY_MIN_STROKE) && (GetStkRcvrState.lapcts16StkPosn_mm > (S16_EMGY_STRK_RCVR_EXIT_POSI+S16_RECOVERY_MIN_STROKE))
				&& (SetStkRcvrState.lapcts16StkRecvryRequestedTime >= S16_RECOVERY_MIN_ACT_TIME_PCT5MS))
		{
			s8Ready2RcvrSatisfy = 1;
		}
		else
		{
			s8Ready2RcvrSatisfy = 0;
		}
	}
	else
	{
		s8Ready2RcvrSatisfy = 0;
	}

	return s8Ready2RcvrSatisfy;
}

/******************************************************************************
* FUNCTION NAME:      LAPCT_s8CheckRecoveryToStabn1St
* CALLED BY:          LCABS_vDecideStrkRecoveryState()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       s8Rcvr2Stabn1Satisfy
* Description:        Check Recovery Stabilizing State
******************************************************************************/
static int8_t LAPCT_s8CheckRecoveryToStabn1St(void)
{
	int8_t s8Rcvr2Stabn1Satisfy = 0;

	if((lapctu1RcvrFastEndForFwdDir == 1) && (lapctu1EmgyStrkRcvrRequestFlg == 0))
	{
		s8Rcvr2Stabn1Satisfy = 1;
	}
	else
	{
		if(lapctu1EmgyStrkRcvrRequestFlg == 1)
		{
			s8Rcvr2Stabn1Satisfy = 0;
		}
		else
		{
			 #if __NEED_TO_STABN2_STATE == ENABLE
			if(lapcts16StrkRcvrActCnt < (SetStkRcvrState.lapcts16StkRecvryRequestedTime-S16_RECOVERY_STABN1_ACT_MAX_TIME-S16_RECOVERY_STABN2_ACT_MAX_TIME))
			{
				s8Rcvr2Stabn1Satisfy = 0;
			}
			else
			{
				s8Rcvr2Stabn1Satisfy = 1;
			}
			 #else
			if(lapcts16StrkRcvrActCnt < SetStkRcvrState.lapcts16StkRecvryRequestedTime)
			{
				if(lapctu1MotorRcvrEndOk == 1) /*TODO interface motor controller*/
				{
					s8Rcvr2Stabn1Satisfy = 1;
				}
				else
				{
					s8Rcvr2Stabn1Satisfy = 0;
				}
			}
			else
			{
				s8Rcvr2Stabn1Satisfy = 1;
			}
			 #endif
		}
	}

	return s8Rcvr2Stabn1Satisfy;
}

#if __NEED_TO_STABN2_STATE == ENABLE
/******************************************************************************
* FUNCTION NAME:      LAPCT_s8CheckStabn1ToStabn2St
* CALLED BY:          LCABS_vDecideStrkRecoveryState()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       s8Stabn1ToStabn2Satisfy
* Description:        Check Recovery Stabilizing2 State
******************************************************************************/
static int8_t LAPCT_s8CheckStabn1ToStabn2St(void)
{
	int8_t s8Stabn1ToStabn2Satisfy = 0;
	int8_t u8StrkRcvrStabnEndOk = 0;

	 #if __DECIDE_STABILIZE_REF_PRESS ==ENABLE
	u8StrkRcvrStabnEndOk = LCABS_u8CheckStabnEndOkRefPress();
	 #else
	u8StrkRcvrStabnEndOk = GetStkRcvrState.lapcts32StkRecvryStabnEndOK;
	 #endif

	if(lapcts16StrkRcvrActCnt >= (lapcts16StrkRcvr2StEndTime + S16_RECOVERY_STABN1_ACT_MAX_TIME))
	{
		s8Stabn1ToStabn2Satisfy = 1;
		lapcts16StrkRecvrStabnPressCnt = 0;
	}
	else
	{
		if(u8StrkRcvrStabnEndOk == 1)
		{
			s8Stabn1ToStabn2Satisfy = 1;
		}
		else
		{
			s8Stabn1ToStabn2Satisfy = 0;
		}
	}

	return s8Stabn1ToStabn2Satisfy;
}

/******************************************************************************
* FUNCTION NAME:      LAPCT_s8CheckStabn1ToStabn2St
* CALLED BY:          LCABS_vDecideStrkRecoveryState()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       s8Stabn2ToReadySatisfy
* Description:        Check Recovery Stabilizing2 to Ready State
******************************************************************************/
static int8_t LAPCT_s8CheckStabn2ToReadySt(void)
{
	int8_t s8Stabn2ToReadySatisfy = 0;

	if(lapcts16StrkRcvrActCnt >= (lapcts16StrkStbn1StEndTime + S16_RECOVERY_STABN2_ACT_MAX_TIME))
	{
		s8Stabn2ToReadySatisfy = 1;
	}
	else
	{
		s8Stabn2ToReadySatisfy = 0;
	}

	return s8Stabn2ToReadySatisfy;
}

#else
/******************************************************************************
* FUNCTION NAME:      LAPCT_s8CheckStabn1ToReadySt
* CALLED BY:          LCABS_vDecideStrkRecoveryState()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       s8Stabn1ToStabn2Satisfy
* Description:        Check Stabilizing1 to Ready State
******************************************************************************/
static int8_t LAPCT_s8CheckStabn1ToReadySt(void)
{
	int8_t s8Stabn1ToReadySatisfy = 0;
	int8_t u8StrkRcvrStabnEndOk = 0;

	 #if __DECIDE_STABILIZE_REF_PRESS ==ENABLE
	u8StrkRcvrStabnEndOk = LCABS_u8CheckStabnEndOkRefPress();
	 #else
	u8StrkRcvrStabnEndOk = GetStkRcvrState.lapcts32StkRecvryStabnEndOK;
	 #endif

	if(lapcts16StrkRcvrActCnt >= (lapcts16StrkRcvr2StEndTime + S16_RECOVERY_STABN1_ACT_MAX_TIME))
	{
		s8Stabn1ToReadySatisfy = 1;
		lapcts16StrkRecvrStabnPressCnt = 0;
	}
	else
	{
		if(u8StrkRcvrStabnEndOk == 1)
		{
			s8Stabn1ToReadySatisfy = 1;
		}
		else
		{
			s8Stabn1ToReadySatisfy = 0;
		}
	}

	return s8Stabn1ToReadySatisfy;
}
#endif
/******************************************************************************
* FUNCTION NAME:      LAPCT_s8CheckStabn1ToStabn2St
* CALLED BY:          LCABS_vDecideStrkRecoveryState()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       s8Stabn2ToReadySatisfy
* Description:        Check Recovery Stabilizing2 to Ready State
******************************************************************************/
#if __DECIDE_STABILIZE_REF_PRESS ==ENABLE
static uint8_t LCABS_u8CheckStabnEndOkRefPress(void)
{
	uint8_t u8StrkRcvrStabnEndOk = 0;
	int16_t s16StrkRcvrRefPress = 0;
	int16_t s16StrkRcvrDiffPress = 0;

	 #if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
	s16StrkRcvrRefPress = GetStkRcvrState.lapcts16PistonP;
	 #else
	if(GetStkRcvrState.lapcts16PriCircuitP > GetStkRcvrState.lapcts16SecCircuitP)
	{
		s16StrkRcvrRefPress = GetStkRcvrState.lapcts16PriCircuitP;
	}
	else
	{
		s16StrkRcvrRefPress = GetStkRcvrState.lapcts16SecCircuitP;
	}
	 #endif

	s16StrkRcvrDiffPress = s16StrkRcvrRefPress - lapcts16PistonPOld;

	if(s16StrkRcvrRefPress >= (int16_t)(((int32_t)lapcts16StabnTargetP * S16_STRKRCVR_STABN_PRESS_RATIO)/10))
	{
		lapcts16StrkRecvrStabnPressCnt = (lapcts16StrkRecvrStabnPressCnt > U8_T_40_MS) ? U8_T_40_MS : (lapcts16StrkRecvrStabnPressCnt + 1);

		if(lapcts16StrkRecvrStabnPressCnt >= U8_T_40_MS)
		{
			u8StrkRcvrStabnEndOk = 1;
			lapcts16StrkRecvrStabnPressCnt = 0;
		}
		else
		{
			u8StrkRcvrStabnEndOk = 0;
		}
	}
	else
	{
		if(s16StrkRcvrDiffPress >= S16_STRKRCVR_STABN_DIFF_P_TH)
		{
			lapcts16StrkRecvrStabnPressCnt = (lapcts16StrkRecvrStabnPressCnt > U8_T_20_MS) ? U8_T_20_MS : (lapcts16StrkRecvrStabnPressCnt + 1);
		}
		else { }
	}

	return u8StrkRcvrStabnEndOk;
}
#endif

/******************************************************************************
* FUNCTION NAME:      LAPCT_s8DcdStkRcvrRecommendLvl
* CALLED BY:          LAPCT_vDecideStkRecoveryCtrl()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       s8RecommendStkRcvrLvl
* Description:        Decide stroke recovery recommend level
******************************************************************************/
static int8_t LAPCT_s8DcdStkRcvrRecommendLvl(int16_t s16PosnUSL,int16_t s16StkPosn)
{
	int8_t s8RecommendStkRcvrLvl = 0;

	if(s16StkPosn < s16PosnUSL)
	{
		s8RecommendStkRcvrLvl = S8_RCVR_RECOMMEND_LVL_1;
	}
	else
	{
		s8RecommendStkRcvrLvl = S8_RCVR_RECOMMEND_LVL_2;
	}

	return s8RecommendStkRcvrLvl;
}

/******************************************************************************
* FUNCTION NAME:      LAPCT_s16DcdStkRcvrRequestTime
* CALLED BY:          LAPCT_vDecideStkRecoveryCtrl()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       s16StkRecvryRequestedTime
* Description:        Decide stroke recovery requested time
******************************************************************************/
static int16_t LAPCT_s16DcdStkRcvrRequestTime(int8_t s8StkRcvrCtrlState, int16_t s16StkRcvrAllowedTime)
{
	int16_t s16StkRecvryRequestedTime = 0;

	if(s8StkRcvrCtrlState == S8_STRK_RCVR_INHIBIT_STATE)
	{
		s16StkRecvryRequestedTime = U8_T_0_MS;
	}
	else if(s8StkRcvrCtrlState == S8_STRK_RCVR_READY_STATE)
	{
		s16StkRecvryRequestedTime = s16StkRcvrAllowedTime;
	}
	else
	{
		if(s16StkRcvrAllowedTime < S16_RECOVERY_MIN_ACT_TIME_PCT5MS)
		{
			if(s16StkRcvrAllowedTime == S16_RECOVERY_END_REQ_TIME_PCT5MS)
			{
				s16StkRecvryRequestedTime = S16_RECOVERY_END_REQ_TIME_PCT5MS;
			}
			else
			{
				s16StkRecvryRequestedTime = S16_RECOVERY_MIN_ACT_TIME_PCT5MS;
			}
		}
		else
		{
			s16StkRecvryRequestedTime = s16StkRcvrAllowedTime;
		}
	}

	return s16StkRecvryRequestedTime;
}
#define PCT_CTRL_STOP_SEC_CODE
#include "Pct_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
