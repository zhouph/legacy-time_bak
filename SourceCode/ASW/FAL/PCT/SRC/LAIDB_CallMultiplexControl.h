/*
 * LAIDBCallMultiplexControl.h
 *
 *------------------------------------------------------------------------------
 *      Date       Version      Author       Description
 * -------------   -------   ------------  -------------------------------------
 * 2014. 12. 29.    1.0.0     Dolores Kim    Initial version
 */

#ifndef LAIDB_CALLMULTIPLEXCONTROL_H_
#define LAIDB_CALLMULTIPLEXCONTROL_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Pct_5msCtrl.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/*#define __MUX_DEBUG*/

#define	U8_MUXVLV_DEFAULT   	(0)
#define	U8_MUXVLV_ACT		   	(1)

/* ---- Inlet valve settings per valve type ----- */
#define	U8_MUXVLV_OPEN			(U8_MUXVLV_DEFAULT)	/* CHANGE per inlet valve type */
#define	U8_MUXVLV_CLOSE			(U8_MUXVLV_ACT)     /* CHANGE per inlet valve type */
/* NO VLV: OPEN is default , NC VLV: default CLOSE is default */

/* ABS MCC MODE SELECT: 0-POSI, 1-TQ */
#define U8_MUX_ABS_MCC_TQ_MODE			(1)
#define U8_MUX_ABS_MCC_POSI_MODE		(0)
#define U8_MUX_ABS_MCC_SELECTED_MODE	(U8_MUX_ABS_MCC_POSI_MODE)


/* Multiplex Execute state */
#define U8_MUX_ST_NONE				0
#define U8_MUX_ST_NOT_EXECUTED		0
#define U8_MUX_ST_EXECUTING			1
#define U8_MUX_ST_TRANSITION		2
#define U8_MUX_ST_EXECUTED			3
#define U8_MUX_ST_FIRST_EXECUTED	4
/* Multiplex control state */
#define U8_MULTIPLEX_NONCONTROL		0
#define U8_MULTIPLEX_CONTROL		1
#define U8_MULTIPLEX_FADEOUT		2
#define U8_MULTIPLEX_INHIBITION		3
/* Multiplex control mode */
#define S8_MUX_MODE_NONCONTROL			0
#define S8_MUX_MODE_STROKE_RECOVERY		5 /*  5 */
#define S8_MUX_MODE_PCTRL				1 /*  1 */
#define S8_MUX_MODE_PCTRL_TRANSITION	2 /*  2 */
#define S8_MUX_MODE_SR_TRANSITION		6 /*  6 */
#define S8_MUX_MODE_INHIBITION		   -1 /* -1 */
/* Multiplex Executing time decision */
#define U16_WHL_SWITCH_DELAY_TIME		U8_T_5_MS /*U8_T_10_MS*/
#define U16_RESOLCHG_BARPSEC_TO_SCAN 	(2) /* U16_T_1_S / S16_PCTRL_PRESS_1_BAR */
#define U16_RESOLCHG_BARPSEC_TO_MS		(10)
#define	U8_IDB_LOOPTIME					(5)

/* Multiplex wheel valve commands */
#define	U8_MUXWHLCTRL_NONE			(0)	/* apply */
#define	U8_MUXWHLCTRL_APPLY			(1)	/* apply */
#define	U8_MUXWHLCTRL_HOLD			(2)	/* hold */
#define U8_MUXWHLCTRL_FULLRISE		(0)

/* Multiplex wheel valve duty ratio level */
#define U16_MUX_VV_DUTY_RATIO_MIN		(0)		/* default - full open */
#define U16_MUX_VV_DUTY_RATIO_LV0       (50)    /* close min duty, resol 0.1% */
#define U16_MUX_VV_DUTY_RATIO_LV1       (100)   /* not decide */
#define U16_MUX_VV_DUTY_RATIO_LV2       (100)	/* not decide */
#define U16_MUX_VV_DUTY_RATIO_LV3       (100)	/* not decide */
#define U16_MUX_VV_DUTY_RATIO_LV4       (100)	/* not decide */
#define U16_MUX_VV_DUTY_RATIO_LV5       (100)	/* not decide */
#define U16_MUX_VV_DUTY_RATIO_MAX  		(1000)  /* perfect close duty */
/* Delta Press (CircuitP - WheelP) level for selecting the duty ratio */
#define U16_MUX_VV_DUTY_RATIO_DELP_L0	(S16_PCTRL_PRESS_10_BAR)  /* Delta Press point */
#define U16_MUX_VV_DUTY_RATIO_DELP_L1	(S16_PCTRL_PRESS_20_BAR)
#define U16_MUX_VV_DUTY_RATIO_DELP_L2   (S16_PCTRL_PRESS_40_BAR)
#define U16_MUX_VV_DUTY_RATIO_DELP_L3   (S16_PCTRL_PRESS_60_BAR)
#define U16_MUX_VV_DUTY_RATIO_DELP_L4   (S16_PCTRL_PRESS_80_BAR)
#define U16_MUX_VV_DUTY_RATIO_DELP_L5   (S16_PCTRL_PRESS_100_BAR)
#define U16_MUX_VV_DUTY_RATIO_DELP_L6   (S16_PCTRL_PRESS_150_BAR)

#define lapctu1InAbsCtrlFadeOut     lapctst_MuxCtrlFlg.u1InAbsCtrlFadeOut
#define lapctu1InAbsCtrlActive      lapctst_MuxCtrlFlg.u1InAbsCtrlActive
#define lapctu1SafetyControlActive	lapctst_MuxCtrlFlg.u1SafetyControlActive
#define lapctu1PCtrlActFlg  		lapctst_MuxCtrlFlg.u1PCtrlActFlg
#define lapctu1EbdPulseUpActive 	lapctst_MuxCtrlFlg.u1EbdPulseUpActive
#define lapctu1EbdActive 	        lapctst_MuxCtrlFlg.u1EbdActive
#define lapctu1AbsMtrTqCtrlActive	lapctst_MuxCtrlFlg.u1AbsMtrTqCtrlActive
#define lapctu1EscTwoChannel		lapctst_MuxCtrlFlg.u1EscTwoChannel

#define lapcts16MuxPwrPistonTarP    PressCTRLLocalBus.s16MuxPwrPistonTarP
#define lapcts16MuxPwrPistonTarDP   PressCTRLLocalBus.s16MuxPwrPistonTarDP
#define lapctu8WhlCtrlIndex         PressCTRLLocalBus.u8WhlCtrlIndex
#define lau8MultiplexCtrlActive		PressCTRLLocalBus.u8MultiplexCtrlActive
#define lapctu8AbsMtrCtrlMode       PressCTRLLocalBus.u8AbsMtrCtrlMode
#define lapctu8MuxCircCtrlReqPri    PressCTRLLocalBus.u8MuxCircCtrlReqPri
#define lapctu8MuxCircCtrlReqSec    PressCTRLLocalBus.u8MuxCircCtrlReqSec

#define lau1MuxStrkRecvrCtrlNeed  	lapctst_MuxWhlCtrlFlg.u1MuxStrkRecvrCtrlNeed /* for logging - temporary */
#define lau1MuxCtrlStrategy		  	lapctst_MuxWhlCtrlFlg.u1MuxCtrlStrategy
#define lau1MuxCmdAllocated		  	lapctst_MuxWhlCtrlFlg.u1MuxCmdAllocated
#define lau1MuxPCtrlStrategy	  	lapctst_MuxWhlCtrlFlg.u1MuxPCtrlStrategy
#define lau1MuxPCtrlNeed			lapctst_MuxWhlCtrlFlg.u1MuxPCtrlNeed
#define lau1MuxCurCmdIdChanged		lapctst_MuxWhlCtrlFlg.u1MuxCurCmdIdChanged
#define lau1MuxNChannelCtrlActive 	lapctst_MuxWhlCtrlFlg.u1MuxNChannelCtrlActive /* for logging - temporary */
#define lapctu1NewReqDetected       lapctst_MuxWhlCtrlFlg.u1NewReqDetected /* for logging - temporary */

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

typedef struct {
	unsigned u1InAbsCtrlActive          :1;
	unsigned u1InAbsCtrlFadeOut         :1;
	unsigned u1SafetyControlActive		:1;
	unsigned u1PCtrlActFlg				:1;
	unsigned u1EbdPulseUpActive			:1;
	unsigned u1EbdActive			    :1;
	unsigned u1AbsMtrTqCtrlActive		:1;
	unsigned u1EscTwoChannel			:1;		
}LAMUXSYSTCTRLFLGS_t;

typedef struct {
	unsigned u1MuxCtrlStrategy			:1; /* 1: FIFO  / 0: Highest delta-p */
	unsigned u1MuxCmdAllocated			:1;
	unsigned u1MuxStrkRecvrCtrlNeed		:1;
	unsigned u1MuxPCtrlStrategy			:1; /* 1: Delta-P / 0: Target P */
	unsigned u1MuxPCtrlNeed				:1;
	unsigned u1MuxCurCmdIdChanged		:1;
	/*
	unsigned u1HaltCurMuxCmd			:1;
	unsigned u1HaltCurMuxScan			:1;
	*/
	unsigned u1MuxNChannelCtrlActive	:1; /* 1: needed n-channel multiplex control / 0: 1-channel multiplex control */
	unsigned u1NewReqDetected 			:1;

}LAMUXWHLCTRLFLGS_t;

typedef struct
{
	uint8_t  u8MuxWhlCtrlMode;
	uint8_t  u8WhlCmdDelayTime;
	uint16_t u16WhlCmdExecTime;
	uint16_t u16MuxWhlVlvActRatio;
}LAMUX_WHL_VV_CMD_t;

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

extern LAMUXSYSTCTRLFLGS_t lapctst_MuxCtrlFlg;    /* to LAPCT_DecideValveCtrlMode.c*/
extern LAMUXWHLCTRLFLGS_t lapctst_MuxWhlCtrlFlg;  /* to LAIDB_DecideTargetPress.c */
extern int16_t lapcts16MuxWheelVolumeState;   /* to LAPCT_DecideValveCtrlMode.c */
extern int16_t laidbs16MuxTarPress;           /* to LAPCT_DecideValveCtrlMode.c */
extern int16_t lapcts16MuxPwrPistonTarPInput; /* to LAPCT_DecideValveCtrlMode.c */
extern int16_t lapcts16MuxPwrPistTarPRateInp; /* to LAPCT_DecideValveCtrlMode.c */
extern int16_t lapcts16MuxTarDelPress;

extern LAMUX_WHL_VV_CMD_t	laMux_FLCMD, laMux_FRCMD, laMux_RLCMD, laMux_RRCMD; /* to LAIDB_CallMultiplexValveActuation.c*/

/* for logging */
/* 
extern uint8_t u8DebuggerInfo, u8DebuggerInfo02, u8DebuggerInfo03, u8DebuggerInfo04, u8DebuggerInfo05, u8DebuggerInfo06, u8DebuggerInfo07, u8DebuggerInfo08, u8DebuggerInfo09;
extern int16_t s16DebuggerInfo, s16DebuggerInfo02, s16DebuggerInfo03, s16DebuggerInfo04, s16DebuggerInfo05, s16DebuggerInfo06, s16DebuggerInfo07, s16DebuggerInfo08, s16DebuggerInfo09;
extern uint16_t u16DebuggerInfo00, u16DebuggerInfo01, u16DebuggerInfo02;
extern int32_t s32DebuggerInfo00, s32DebuggerInfo01, s32DebuggerInfo02, s32DebuggerInfo03, s32DebuggerInfo04; 
*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void LAIDB_vCallMultiplexCtrl(void);

#endif /* LAIDB_CALLMULTIPLEXCONTROL_H_ */
