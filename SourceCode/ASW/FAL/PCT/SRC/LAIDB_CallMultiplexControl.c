/**
 * @defgroup LAIDBCallMultiplexControl LAIDBCallMultiplexControl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LAIDBCallMultiplexControl.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "LAIDB_CallMultiplexControl.h"
#include "./MUX/LAIDB_LibMuxFIFO.h"
#include "./MUX/LAIDB_CallMultiplexValveActuation.h"
#include "./MUX/LAIDB_DecideTargetPress.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
/*
#define laidbu1HaltCurMuxCmd		MuxWhlCtrlFlg.u1HaltCurMuxCmd
#define laidbu1HaltCurMuxCmdScan	MuxWhlCtrlFlg.u1HaltCurMuxScan
*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

typedef struct {
	unsigned u1Priority	  :1;
	unsigned u1PriorityCmdPerWheel  :1;
	unsigned u1CmdHalt  			:1;
	unsigned u1BuffFIFOAdded		:1;
	unsigned u1BuffRemoved  		:1;
	unsigned u1BuffInserted 			:1;

	uint8_t u8id		       ; /* type of Mux commands: currently there will be 6 types*/
	uint8_t u8State			   ; /* state of Mux command: executed, executing, not-executed, first-executed */
	/*uint8_t u8OldState		;*/ /* state of Mux command: executed, executing, not-executed, first-executed */
	uint16_t u16CmdExecTimeMs  ; 	/* Mux command executing time: max 20 seconds*/
	uint16_t u16CmdExecTime	   ; 	/* Mux command executing time: max 20 seconds*/
	/*uint16_t u16CmdExecTimer;*/ /* Mux command executing timer: max 20 seconds*/
	int16_t  s16WhlTargetP     ;
	int16_t  s16WhlTargetDeltaP;
	int16_t  s16WhlTargetPRate ;
	int16_t  s16EstWhlP		   ;
}LAMUXCTRLSTATE_t; /* multiplex output commands */

typedef struct {
	unsigned u1Priority	  :1;
	unsigned u1CtrlReq    :1;
	unsigned u1PriorityCmdPerWheel  :1;
	unsigned u1CmdHalt  			:1;
	uint8_t  u8id			   ;
	int16_t  s16WhlTargetP     ;
	int16_t  s16WhlTargetDeltaP;
	int16_t  s16WhlTargetPRate ;
	int16_t  s16EstWhlP		   ;
	uint8_t  u8ArbCtrlMod;
}LAMUXCTRLCMD_t; /* multiplex input commands */

typedef struct
{
    int16_t s16PressRate10ms;
    int16_t s16PressRate5ms;
    int16_t s16PressBuff[5];
    int16_t s16BuffCnt;
}LAMUXPRESSRATECALC_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_CONST_UNSPECIFIED
#include "Pct_MemMap.h"
/** Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Pct_MemMap.h"

/* Multiplex wheel or stroke recovery cmd identify */
#define U8_MUX_CMD_NONE			0
#define U8_MUX_CMD_FL_RISE		1
#define U8_MUX_CMD_FR_RISE		2
#define U8_MUX_CMD_RL_RISE		3
#define U8_MUX_CMD_RR_RISE		4
#define U8_MUX_CMD_STRK_RECVR   5

#define S8_MUX_STRK_RCVR_READY	0

#define U1_MUX_STRATEGY_FIFO	0
#define U1_MUX_STRATEGY_MAXDP	1

#define U8_MUX_PCTRL_DELTAP_BASE	1
#define U8_MUX_PCTRL_TARP_BASE 		0

#define S16_MUX_RISE_NEED_PERROR_LIMIT  (MPRESS_2BAR) /* Mux request is set if p error is at least this value */
#define S16_MAX_PCTRL_RATE_BPS          (S16_P_RATE_1000_BPS)
#define S16_MUX_TP_FAST_APROACH_REF     (S16_PCTRL_PRESS_10_BAR)

#define U8_MUX_ABS_ON	3
#define U8_MUX_SAFETY_CTRL_ON	20

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

LAMUXSYSTCTRLFLGS_t lapctst_MuxCtrlFlg;    /* to LAPCT_DecideValveCtrlMode.c*/
LAMUXWHLCTRLFLGS_t lapctst_MuxWhlCtrlFlg;  /* to LAIDB_DecideTargetPress.c */
int16_t lapcts16MuxWheelVolumeState;   /* to LAPCT_DecideValveCtrlMode.c */
int16_t laidbs16MuxTarPress;           /* to LAPCT_DecideValveCtrlMode.c */
int16_t lapcts16HighestWhlTarP;        
int16_t lapcts16MuxPwrPistonTarPInput; /* to LAPCT_DecideValveCtrlMode.c */
int16_t lapcts16MuxPwrPistTarPRateInp; /* to LAPCT_DecideValveCtrlMode.c */
int16_t lapcts16MuxTarDelPress;

uint8_t lapctu8OutVVPriorFL;
uint8_t lapctu8OutVVPriorFR;
uint8_t lapctu8OutVVPriorRL;
uint8_t lapctu8OutVVPriorRR;

LAMUX_WHL_VV_CMD_t	laMux_FLCMD, laMux_FRCMD, laMux_RLCMD, laMux_RRCMD; /* to LAIDB_CallMultiplexValveActuation.c*/
/*LAMUX_WHL_VV_OUTPUT_t  laMux_FL, 	laMux_FR, 	 laMux_RL,	  laMux_RR;*/
/*uint8_t u8MuxFLCtrlMode, u8MuxFRCtrlMode, u8MuxRLCtrlMode, u8MuxRRCtrlMode; // for logging
uint16_t u16CtrlWhlExecTimeMs;
*/
uint8_t u8MuxSeqdebugger00;
uint8_t u8MuxExeTdebugger00, u8MuxExeTdebugger01;
/* for logging */
/* 
uint8_t u8DebuggerInfo, u8DebuggerInfo02, u8DebuggerInfo03, u8DebuggerInfo04, u8DebuggerInfo05, u8DebuggerInfo06, u8DebuggerInfo07, u8DebuggerInfo08, u8DebuggerInfo09;
int16_t s16DebuggerInfo, s16DebuggerInfo02, s16DebuggerInfo03, s16DebuggerInfo04, s16DebuggerInfo05, s16DebuggerInfo06, s16DebuggerInfo07, s16DebuggerInfo08, s16DebuggerInfo09;
uint16_t u16DebuggerInfo00, u16DebuggerInfo01, u16DebuggerInfo02;
int32_t s32DebuggerInfo00, s32DebuggerInfo01, s32DebuggerInfo02, s32DebuggerInfo03, s32DebuggerInfo04; 
*/
uint8_t lapctu8AllowSwitchingDelay = 0;

extern int16_t lis16DriverIntendedTP;

#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_START_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/** Variable Section (32BIT)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Pct_MemMap.h"

/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

/*static LAMUXCTRLCMD_t MuxInitCmd;*/
FAL_STATIC LAMUXCTRLCMD_t MuxCmdFrntLe;
FAL_STATIC LAMUXCTRLCMD_t MuxCmdFrntRi;
FAL_STATIC LAMUXCTRLCMD_t MuxCmdReLe  ;
FAL_STATIC LAMUXCTRLCMD_t MuxCmdReRi  ;
FAL_STATIC LAMUXCTRLCMD_t MuxCmdStrkRecvry;

/*LAMUXCTRLSTATE_t MuxInitSt;*/
FAL_STATIC LAMUXCTRLSTATE_t MuxStFrntLe;
FAL_STATIC LAMUXCTRLSTATE_t MuxStFrntRi;
FAL_STATIC LAMUXCTRLSTATE_t MuxStReLe;
FAL_STATIC LAMUXCTRLSTATE_t MuxStReRi;
FAL_STATIC LAMUXCTRLSTATE_t MuxStStrkRecvry;
FAL_STATIC LAMUXCTRLSTATE_t MuxCurCmd;
/* FAL_STATIC LAMUXCTRLSTATE_t MuxStClear = {0,};*/ /*0,0,0,0,0*/

FAL_STATIC int16_t las16MuxCtrlState;
FAL_STATIC int16_t las16MuxCtrlMode;
FAL_STATIC int16_t laidbs16MuxTarPressOld;
FAL_STATIC int16_t laidbs16MuxTarPressRate;

FAL_STATIC uint16_t laidbu16CmdExecTimer;
FAL_STATIC uint16_t las16MuxFadeOutCnt;
FAL_STATIC uint16_t las16MuxFadeOutStrTP;
FAL_STATIC int16_t  las16FadeoutTPDiffLimit;

FAL_STATIC int16_t lapcts16MuxWhlTargetPOldFL=0;
FAL_STATIC int16_t lapcts16MuxWhlTargetPOldFR=0;
FAL_STATIC int16_t lapcts16MuxWhlTargetPOldRL=0;
FAL_STATIC int16_t lapcts16MuxWhlTargetPOldRR=0;
FAL_STATIC int8_t  lapcts8MuxStrkRcvrCtrlState = 0; /*lis8StrkRcvrCtrlState*/
FAL_STATIC int16_t lapcts16MuxPistP = 0; /*lsahbs16MC1presFilt*/
#if __P_SENSOR_MEASURE_TYPE== EACH_CIRCUIT_PRESSURE
FAL_STATIC int16_t lapcts16MuxPistPSecd = 0; /*lsahbs16MC2presFilt*/
#endif

FAL_STATIC LAMUXPRESSRATECALC_t stMuxTarPRateInfo;


#if defined (__MUX_DEBUG)
unsigned char InitAdrLogRem[U8_MUX_FIFO_BUFFER_SIZE];
MUX_FIFO_BUFFER TempBuffLogRem = {0,0,0,0,0};
#endif
#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
#define PCT_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PCT_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
#define PCT_5MSCTRL_START_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/** Variable Section (32BIT)**/


#define PCT_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Pct_MemMap.h"



/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_CODE
#include "Pct_MemMap.h"

/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/

static void LAIDB_vCallMuxCtrlr(void);
static void LAIDB_vDecideMuxActuatorCmd(void);
static void LAIDB_vGetMuxInputVars(void);
static void LAIDB_vSetMuxOutputVars(void);

static void LAIDB_vDecideMuxWhlCtrlStrategy(void);
static void LAIDB_vCalcMuxCtrlVars(void);
static void LAIDB_vDecideWhlCtrlSequence(void);
static void LAIDB_vDecideMuxCtrlState(void);
static void LAIDB_vDecideMuxCtrlMode(void);

static void LAIDB_vDecideMuxMtrTarget(void);
static void LAIDB_vDecideEachWhlCtrlMode(void);
static uint8_t LAPCT_u8MuxSetWhlCtrlCmd1Ch(const LAMUXCTRLCMD_t *pWhlActCmd, LAMUXCTRLSTATE_t *pWhlStInfo, LAMUX_WHL_VV_CMD_t *pOutputCmd);
static uint8_t LAPCT_u8MuxSetWhlCtrlCmdNCh(const LAMUXCTRLSTATE_t *pCurCmd, LAMUXCTRLSTATE_t *pWhlStInfo, LAMUX_WHL_VV_CMD_t *pOutputCmd);

static void LAIDB_vInitializeMuxWheelStVars(void);
static void LAIDB_vInitializeMuxCmdVars(void);
static void LAIDB_vInitializeMuxSequence(void);
static uint16_t LAIDB_u16RunMuxCmdExecCounter(LAMUXCTRLSTATE_t *pCmd, uint16_t u16Timer);
static uint8_t LAIDB_u8SetMuxCtrlState(uint16_t u16CmdExecTimeRef, uint16_t u16Timer);
static void LAIDB_vAllocMuxCmdsPerFIFO(LAMUXCTRLCMD_t *pMuxCmd, LAMUXCTRLSTATE_t *pMuxSt);
static void LAIDB_vAllocMuxCmdInfo(LAMUXCTRLCMD_t *pMuxCmd, LAMUXCTRLSTATE_t *pMuxSt);
static void LAIDB_vCalCmdExecTimeWhl(LAMUXCTRLSTATE_t *pMuxCmd);
static uint16_t LAIDB_u16CalcExecTimeFromDeltaP(int32_t s32TarDeltaP, int32_t s32TarPressRateBarPSec);
static uint16_t LAIDB_u16CalcWhlCmdExecTimePhs1(LAMUXCTRLSTATE_t *pMuxCmd);
static uint16_t LAIDB_u16CompExecTimePerDelay(uint16_t u16ExecTimeMs, uint8_t u8CmdId);
/*static int16_t LAIDB_s16CheckMuxCtrlRequest(void);*/
static int16_t LAIDB_s16DecideMultiplexCtrlAct(void);
static int16_t LAIDB_s16CheckMuxCtrlFadeOutEnd(void);
static void LAIDB_vCopyMuxCmdInfo(LAMUXCTRLSTATE_t *pCurInfo);
static uint8_t LAIDB_u8UpdateCurCmd(uint8_t u8InfoCopied, LAMUXCTRLSTATE_t *pCurInfo, LAMUXCTRLSTATE_t  *pInputInfo);
static uint32_t LAIDB_u32DecideMuxRequestPerDeltaP(LAMUXCTRLCMD_t *pInpCmd,int16_t s16TargetPOld,uint8_t u8OutVlvAct);
static uint32_t LAIDB_u32DecideMuxRequestPerPerror(LAMUXCTRLCMD_t *pInpCmd,int16_t s16TargetPOld,uint8_t u8OutVlvAct);
static void LAIDB_vUpdateMuxCtrlStateInfo(void);
static uint8_t LAIDB_u8UpdateMuxWhlState(uint8_t u8InfoCopied, LAMUXCTRLSTATE_t *pCurInfo, LAMUXCTRLSTATE_t  *pInputInfo);
static void LAIDB_vCheckNewCmdPriority(void);
static uint8_t LAIDB_u8CheckNewCmdPriorityPerWhl(uint8_t u8CurCmdPriority, LAMUXCTRLSTATE_t *pCurInfo, LAMUXCTRLCMD_t *pMuxCmd, LAMUXCTRLSTATE_t  *pInputInfo);
static uint8_t LAIDB_u8CheckNewCmdHaltPerWhl(uint8_t u8CurCmdHalt, LAMUXCTRLSTATE_t *pCurInfo, LAMUXCTRLCMD_t *pMuxCmd);
static uint32_t LAIDB_u32CheckPriorityCmdHaltPerWhl(const LAMUXCTRLCMD_t *pCmdInfo,  LAMUXCTRLSTATE_t *pCurInfo, uint8_t u8OutVlvReq);

static void LAIDB_vDecideEachWhlValveActRatio(void);
static void LAPCT_u16CalcMuxWhlVVActRatio1Ch(const LAMUXCTRLCMD_t *pWhlActCmd, int16_t s16CircuitPress, LAMUX_WHL_VV_CMD_t *pMuxWLCMD);
static void LAPCT_u16CalcMuxWhlVVActRatioNch(const LAMUXCTRLSTATE_t *pCurCmd, int16_t s16CircuitPress, LAMUX_WHL_VV_CMD_t *pMuxWLCMD);

static int16_t  LCIDB_vCalcStdTargetPressRate(int16_t s16Press, LAMUXPRESSRATECALC_t *pPrateInfo);
static void LAPCT_vDecidePCtrlActive(void);
static void LAPCT_vSelectReqdPCtrlBoostMod(void);


/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void LAIDB_vCallMultiplexCtrl(void)
{
	LAIDB_vGetMuxInputVars();

	LAIDB_vCallMuxCtrlr();

	LAIDB_vDecideMuxActuatorCmd();

	LAIDB_vSetMuxOutputVars();
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static void LAIDB_vGetMuxInputVars(void)
{
	int16_t s16PriTP = 0;
	int16_t s16SecTP = 0;
	int16_t s16PriTPRate = 0;
	int16_t s16SecTPRate = 0;

    /* set default command variables */
	MuxCmdFrntLe.u8id 	  = U8_MUX_CMD_FL_RISE;
	MuxCmdFrntRi.u8id 	  = U8_MUX_CMD_FR_RISE;
	MuxCmdReLe.u8id   	  = U8_MUX_CMD_RL_RISE;
	MuxCmdReRi.u8id   	  = U8_MUX_CMD_RR_RISE;
	MuxCmdStrkRecvry.u8id = U8_MUX_CMD_STRK_RECVR;

	MuxStFrntLe.u8id 	  = U8_MUX_CMD_FL_RISE;
	MuxStFrntRi.u8id 	  = U8_MUX_CMD_FR_RISE;
	MuxStReLe.u8id 		  = U8_MUX_CMD_RL_RISE;
	MuxStReRi.u8id 		  = U8_MUX_CMD_RR_RISE;
	MuxStStrkRecvry.u8id  = U8_MUX_CMD_STRK_RECVR;

	MuxFIFO.u32BufferLen  = U8_MUX_FIFO_BUFFER_SIZE;
    /* set default command variables */

	/* calc target press old */
	lapcts16MuxWhlTargetPOldFL = MuxCmdFrntLe.s16WhlTargetP; /*lapcts16MuxWhlTarPFL*/
	lapcts16MuxWhlTargetPOldFR = MuxCmdFrntRi.s16WhlTargetP; /*lapcts16MuxWhlTarPFR*/
	lapcts16MuxWhlTargetPOldRL = MuxCmdReLe.s16WhlTargetP  ; /*lapcts16MuxWhlTarPRL*/
	lapcts16MuxWhlTargetPOldRR = MuxCmdReRi.s16WhlTargetP  ; /*lapcts16MuxWhlTarPRR*/

	/*MuxCmdFL. Mux Commands from arbitrator*/

	MuxCmdFrntLe.s16EstWhlP = (int16_t)Pct_5msCtrlBus.Pct_5msCtrlEstimdWhlPInfo.FrntLeEstimdWhlP; /*resol 0.01bar*/
	MuxCmdFrntRi.s16EstWhlP = (int16_t)Pct_5msCtrlBus.Pct_5msCtrlEstimdWhlPInfo.FrntRiEstimdWhlP; /*resol 0.01bar*/
	MuxCmdReLe.s16EstWhlP   = (int16_t)Pct_5msCtrlBus.Pct_5msCtrlEstimdWhlPInfo.ReLeEstimdWhlP;   /*resol 0.01bar*/
	MuxCmdReRi.s16EstWhlP   = (int16_t)Pct_5msCtrlBus.Pct_5msCtrlEstimdWhlPInfo.ReRiEstimdWhlP;   /*resol 0.01bar*/

	MuxCmdFrntLe.s16WhlTargetP = (int16_t)Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlFrntLe; /*resol 0.01bar*/
	MuxCmdFrntRi.s16WhlTargetP = (int16_t)Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlFrntRi; /*resol 0.01bar*/
	MuxCmdReLe.s16WhlTargetP   = (int16_t)Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlReLe;   /*resol 0.01bar*/
	MuxCmdReRi.s16WhlTargetP   = (int16_t)Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlReRi;   /*resol 0.01bar*/

	MuxCmdFrntLe.s16WhlTargetPRate = (int16_t)Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPRateOfWhlFrntLe;
	MuxCmdFrntRi.s16WhlTargetPRate = (int16_t)Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPRateOfWhlFrntRi;
	MuxCmdReLe.s16WhlTargetPRate   = (int16_t)Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPRateOfWhlReLe;
	MuxCmdReRi.s16WhlTargetPRate   = (int16_t)Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPRateOfWhlReRi;

	MuxCmdFrntLe.s16WhlTargetDeltaP = MuxCmdFrntLe.s16WhlTargetP-lapcts16MuxWhlTargetPOldFL; /*resol 0.01bar*/
	MuxCmdFrntRi.s16WhlTargetDeltaP = MuxCmdFrntRi.s16WhlTargetP-lapcts16MuxWhlTargetPOldFR; /*resol 0.01bar*/
	MuxCmdReLe.s16WhlTargetDeltaP   = MuxCmdReLe.s16WhlTargetP  -lapcts16MuxWhlTargetPOldRL; /*resol 0.01bar*/
	MuxCmdReRi.s16WhlTargetDeltaP   = MuxCmdReRi.s16WhlTargetP  -lapcts16MuxWhlTargetPOldRR; /*resol 0.01bar*/

	/* stroke recovery state */
	lapcts8MuxStrkRcvrCtrlState = (int8_t)Pct_5msCtrlBus.Pct_5msCtrlStkRecvryActnIfInfo.StkRecvryCtrlState; /*lis8StrkRcvrCtrlState*/

	/* Rise priority interface to priority */
	MuxCmdFrntLe.u1Priority = Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.PCtrlPrioOfWhlFrntLe;
	MuxCmdFrntRi.u1Priority = Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.PCtrlPrioOfWhlFrntRi;
	MuxCmdReLe.u1Priority   = Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.PCtrlPrioOfWhlReLe;
	MuxCmdReRi.u1Priority   = Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.PCtrlPrioOfWhlReRi;

	/* Outlet valve act flag for the dump priority */
	lapctu8OutVVPriorFL = (Pct_5msCtrlWhlOutVlvCtrlTarInfo.FlOutVlvCtrlTar[0]>0)? TRUE:FALSE;
	lapctu8OutVVPriorFR = (Pct_5msCtrlWhlOutVlvCtrlTarInfo.FrOutVlvCtrlTar[0]>0)? TRUE:FALSE;
	lapctu8OutVVPriorRL = (Pct_5msCtrlWhlOutVlvCtrlTarInfo.RlOutVlvCtrlTar[0]>0)? TRUE:FALSE;
	lapctu8OutVVPriorRR = (Pct_5msCtrlWhlOutVlvCtrlTarInfo.RrOutVlvCtrlTar[0]>0)? TRUE:FALSE;

	MuxCmdFrntLe.u8ArbCtrlMod = (uint8_t)Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.CtrlModOfWhlFrntLe;   /* temporary -> later to use the local variable */
	MuxCmdFrntRi.u8ArbCtrlMod = (uint8_t)Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.CtrlModOfOfWhlFrntRi;
	MuxCmdReLe.u8ArbCtrlMod   = (uint8_t)Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.CtrlModOfWhlReLe;
	MuxCmdReRi.u8ArbCtrlMod   = (uint8_t)Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.CtrlModOfWhlReRi;

	/*lsahbs16MC1presFilt*/
#if __P_SENSOR_MEASURE_TYPE== EACH_CIRCUIT_PRESSURE
	lapcts16MuxPistP = (int16_t)Pct_5msCtrlBus.Pct_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar; /* resol 0.01bar */ /* temporary -> later to use the local variable */
	lapcts16MuxPistPSecd = (int16_t)Pct_5msCtrlBus.Pct_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar; /* resol 0.01bar */ /* temporary -> later to use the local variable */
#else
	lapcts16MuxPistP = (int16_t)Pct_5msCtrlBus.Pct_5msCtrlPistPFildInfo.PistPFild_1_100Bar; /* resol 0.01bar */ /* temporary -> later to use the local variable */
#endif

	lapctu1InAbsCtrlFadeOut = (uint8_t)Pct_5msCtrlBus.Pct_5msCtrlAbsCtrlInfo.AbsCtrlFadeOutFlg ; /* liu1InAbsAllWheelEntFadeOut temporary -> later to use the local variable */

	if(((MuxCmdFrntLe.u8ArbCtrlMod/10)==U8_MUX_ABS_ON) || ((MuxCmdFrntRi.u8ArbCtrlMod/10)==U8_MUX_ABS_ON) || ((MuxCmdReLe.u8ArbCtrlMod/10)==U8_MUX_ABS_ON) || ((MuxCmdReRi.u8ArbCtrlMod/10)==U8_MUX_ABS_ON))
	{
		lapctu1InAbsCtrlActive = TRUE;
	}
	else
	{
		lapctu1InAbsCtrlActive = 0;
	}


/* Arbitrator mode reference for N-channel multiplexing --> Target Press compare */
/* Temporary  */
	if((MuxCmdReLe.u8ArbCtrlMod==23) || (MuxCmdReRi.u8ArbCtrlMod==23)) /* EBD rise */
	{
		lapctu1EbdPulseUpActive = TRUE;
	}
	else
	{
		lapctu1EbdPulseUpActive = FALSE;
	}

	if( ((MuxCmdReLe.u8ArbCtrlMod/10)==2) || ((MuxCmdReRi.u8ArbCtrlMod/10)==2) )
	{
		lapctu1EbdActive = TRUE;
	}
	else
	{
		lapctu1EbdActive = FALSE;
	}


	if(((MuxCmdFrntLe.u8ArbCtrlMod)>=U8_MUX_SAFETY_CTRL_ON) || ((MuxCmdFrntRi.u8ArbCtrlMod)>=U8_MUX_SAFETY_CTRL_ON) || ((MuxCmdReLe.u8ArbCtrlMod)>=U8_MUX_SAFETY_CTRL_ON) || ((MuxCmdReRi.u8ArbCtrlMod)>=U8_MUX_SAFETY_CTRL_ON))
	{
		lapctu1SafetyControlActive = TRUE;
	}
	else
	{
		lapctu1SafetyControlActive = 0;
	}	
	
	if(((MuxCmdFrntLe.u8ArbCtrlMod==41)&&((MuxCmdReLe.u8ArbCtrlMod==42)||(lis16DriverIntendedTP >S16_PCTRL_PRESS_0_5_BAR)))
		 ||((MuxCmdFrntRi.u8ArbCtrlMod==41)&&((MuxCmdReRi.u8ArbCtrlMod==42)||(lis16DriverIntendedTP >S16_PCTRL_PRESS_0_5_BAR)))
		 ||((MuxCmdReLe.u8ArbCtrlMod==42)&&(lis16DriverIntendedTP >S16_PCTRL_PRESS_0_5_BAR))
		 ||((MuxCmdReRi.u8ArbCtrlMod==42)&&(lis16DriverIntendedTP >S16_PCTRL_PRESS_0_5_BAR)))
	{
		lapctu1EscTwoChannel = TRUE;
	}
	else
	{
		lapctu1EscTwoChannel = FALSE;
	}

	/* Select Power Piston Target input */
	s16PriTP   = (MuxCmdReLe.s16WhlTargetP<MuxCmdFrntRi.s16WhlTargetP) ? MuxCmdFrntRi.s16WhlTargetP : MuxCmdReLe.s16WhlTargetP;
	s16SecTP   = (MuxCmdReRi.s16WhlTargetP<MuxCmdFrntLe.s16WhlTargetP) ? MuxCmdFrntLe.s16WhlTargetP : MuxCmdReRi.s16WhlTargetP;
	lapcts16HighestWhlTarP = (s16PriTP > s16SecTP)? s16PriTP : s16SecTP; /* Highest of 4 wheels */

	if( (s16PriTP>0) && (s16SecTP>0) )
	{
		lapcts16MuxPwrPistonTarPInput = (s16PriTP + s16SecTP)/2;
	}
	else if( (s16PriTP>0) && (s16SecTP<=0))
	{
		lapcts16MuxPwrPistonTarPInput = s16PriTP;
	}
	else
	{
		lapcts16MuxPwrPistonTarPInput = s16SecTP;
	}

	/* Select Power Piston Target Rate input */
	s16PriTPRate   = (MuxCmdReLe.s16WhlTargetPRate<MuxCmdFrntRi.s16WhlTargetPRate) ? MuxCmdFrntRi.s16WhlTargetPRate : MuxCmdReLe.s16WhlTargetPRate;
	s16SecTPRate   = (MuxCmdReRi.s16WhlTargetPRate<MuxCmdFrntLe.s16WhlTargetPRate) ? MuxCmdFrntLe.s16WhlTargetPRate : MuxCmdReRi.s16WhlTargetPRate;
	lapcts16MuxPwrPistTarPRateInp = (s16PriTPRate<s16SecTPRate) ? s16SecTPRate : s16PriTPRate;

}

static uint32_t LAIDB_u32DecideMuxRequestPerDeltaP(LAMUXCTRLCMD_t *pInpCmd,int16_t s16TargetPOld,uint8_t u8OutVlvAct)
{
/*	int16_t WhlPresError = 0;
	int16_t DeltaWhlPres = 0;
	int16_t WhlErrorAllowThres = 0;
	uint8_t WhlMuxReq = (uint8_t)pInpCmd->u1CtrlReq;
	DeltaWhlPres = pInpCmd->s16WhlTargetP - s16TargetPOld;
	*/
	uint8_t u8WhlMuxReq = (uint8_t)pInpCmd->u1CtrlReq;

	/*
	if(lau1MuxNChannelCtrlActive==TRUE)
	{
	*/
		if((pInpCmd->s16WhlTargetDeltaP > S16_PCTRL_PRESS_0_BAR) && (pInpCmd->s16WhlTargetP > S16_PCTRL_PRESS_0_BAR))
		{
			u8WhlMuxReq = TRUE;
		}
		else
		{
			u8WhlMuxReq = FALSE;
		}
	/*
	}
	else
	{
		u8WhlMuxReq = FALSE;
	}
	*/
	return (uint32_t)u8WhlMuxReq;
}

static uint32_t LAIDB_u32DecideMuxRequestPerPerror(LAMUXCTRLCMD_t *pInpCmd,int16_t s16TargetPOld,uint8_t u8OutVlvAct)
{
	int16_t WhlPresError = 0;
	int16_t TargetWhlPres = 0;
	int16_t WhlErrorAllowThres = 0;
	uint8_t WhlMuxReq = (uint8_t)pInpCmd->u1CtrlReq;

	TargetWhlPres = pInpCmd->s16WhlTargetP;
	/*WhlPresError = TargetWhlPres - pInpCmd->s16EstWhlP;*/
	/*
	WhlErrorAllowThres = ((int32_t)pInpCmd->s16WhlTargetP*1)/10; // to reach 90% of target pressure

	if(WhlErrorAllowThres <= S16_MUX_RISE_NEED_PERROR_LIMIT )
	{
	    WhlErrorAllowThres = MPRESS_2BAR;
	}
	*/

	/*if(lau1MuxNChannelCtrlActive==TRUE)*/
	{
		if(WhlMuxReq == FALSE)
		{
			if(u8OutVlvAct==TRUE)
			{
				WhlMuxReq = FALSE;
			}
			else if(TargetWhlPres>=S16_PCTRL_PRESS_0_5_BAR)  /*only for rise commands, including rise-hold */ /*(WhlPresError > WhlErrorAllowThres) -> to consider later*/
			{
				WhlMuxReq = TRUE;
			}
			else
			{
				; /* hold prev. value for hysteresis */
			}
		}
		else
		{
			if(u8OutVlvAct==TRUE)
			{
				WhlMuxReq = FALSE;
			}
			else if(TargetWhlPres<=S16_PCTRL_PRESS_0_BAR) /*only for rise commands */ /* || WhlPresError <= MPRESS_0BAR(DeltaWhlPres<MPRESS_0BAR))*/
			{
				WhlMuxReq = FALSE;
			}
			else
			{
				; /* hold prev. value for hysteresis */
			}
		}
	}
	/*
	else
	{
		WhlMuxReq = FALSE;
	}
	*/

	if(pInpCmd->u8ArbCtrlMod==24)
	{
		WhlMuxReq = FALSE;
	}

	return WhlMuxReq;
}

static void LAIDB_vSetMuxOutputVars(void)
{
	LAPCT_vDecidePCtrlActive();

	LAPCT_vSelectReqdPCtrlBoostMod();

	/*----------- local bus ---------*/
	PressCTRLLocalBus.u8PCtrlAct          = (uint8_t)lapctu1PCtrlActFlg;
	PressCTRLLocalBus.s16MuxWhlTarPFL     = MuxCmdFrntLe.s16WhlTargetP;
	PressCTRLLocalBus.s16MuxWhlTarPFR     = MuxCmdFrntRi.s16WhlTargetP;
	PressCTRLLocalBus.s16MuxWhlTarPRL     = MuxCmdReLe.s16WhlTargetP  ;
	PressCTRLLocalBus.s16MuxWhlTarPRR     = MuxCmdReRi.s16WhlTargetP  ;
	PressCTRLLocalBus.s16MuxWhlTarPRateFL = MuxCmdFrntLe.s16WhlTargetPRate;
	PressCTRLLocalBus.s16MuxWhlTarPRateFR = MuxCmdFrntRi.s16WhlTargetPRate;
	PressCTRLLocalBus.s16MuxWhlTarPRateRL = MuxCmdReLe.s16WhlTargetPRate  ;
	PressCTRLLocalBus.s16MuxWhlTarPRateRR = MuxCmdReRi.s16WhlTargetPRate  ;
	PressCTRLLocalBus.s16MuxWhlTarDelPFL  = MuxCmdFrntLe.s16WhlTargetDeltaP;
	PressCTRLLocalBus.s16MuxWhlTarDelPFR  = MuxCmdFrntRi.s16WhlTargetDeltaP;
	PressCTRLLocalBus.s16MuxWhlTarDelPRL  = MuxCmdReLe.s16WhlTargetDeltaP;
	PressCTRLLocalBus.s16MuxWhlTarDelPRR  = MuxCmdReRi.s16WhlTargetDeltaP;

	PressCTRLLocalBus.s16MuxPwrPistonTarPRate = lapcts16MuxPwrPistTarPRateInp;
	/*PressCTRLLocalBus.s16MuxPwrPistonTarP        // LAPCT_DecidePressCtrlMode.c */
	/* PressCTRLLocalBus.s16MuxPwrPistonTarDP;     // LAPCT_DecidePressCtrlMode.c */
	/* PressCTRLLocalBus.u16MuxWheelVolumeState;   // LAPCT_DecidePressCtrlMode.c */
	/* PressCTRLLocalBus.u8MultiplexCtrlActive      define */
	/* PressCTRLLocalBus.u8WhlCtrlIndex            define */
	/* PressCTRLLocalBus.u8AbsMtrCtrlMode;          define */
	/* PressCTRLLocalBus.u8MuxCircCtrlReqPri;       define */
	/* PressCTRLLocalBus.u8MuxCircCtrlReqSec;       define */

	PressCTRLLocalBus.s16MuxMode    =0;
	PressCTRLLocalBus.s8MuxCtrlState=0;

	/*lapcts16MuxTarDelPress = MuxCurCmd.s16WhlTargetDeltaP;*/

	/*----------- RTE output -----------*/

	/* Piston P */
	Pct_5msCtrlBus.Pct_5msCtrlFinalTarPFromPCtrl =  (int32_t) PressCTRLLocalBus.s16MuxPwrPistonTarP;

	/* PCtrl Act */
	Pct_5msCtrlBus.Pct_5msCtrlPCtrlAct = (uint8_t)lapctu1PCtrlActFlg;

	Pct_5msCtrlBus.Pct_5msCtrlWhlnVlvCtrlTarInfo.FlInVlvCtrlTar[0] = FlInVlvCtrlTar[0];
	Pct_5msCtrlBus.Pct_5msCtrlWhlnVlvCtrlTarInfo.FlInVlvCtrlTar[1] = FlInVlvCtrlTar[1];
	Pct_5msCtrlBus.Pct_5msCtrlWhlnVlvCtrlTarInfo.FlInVlvCtrlTar[2] = FlInVlvCtrlTar[2];
	Pct_5msCtrlBus.Pct_5msCtrlWhlnVlvCtrlTarInfo.FlInVlvCtrlTar[3] = FlInVlvCtrlTar[3];
	Pct_5msCtrlBus.Pct_5msCtrlWhlnVlvCtrlTarInfo.FlInVlvCtrlTar[4] = FlInVlvCtrlTar[4];

	Pct_5msCtrlBus.Pct_5msCtrlWhlnVlvCtrlTarInfo.FrInVlvCtrlTar[0] = FrInVlvCtrlTar[0];
	Pct_5msCtrlBus.Pct_5msCtrlWhlnVlvCtrlTarInfo.FrInVlvCtrlTar[1] = FrInVlvCtrlTar[1];
	Pct_5msCtrlBus.Pct_5msCtrlWhlnVlvCtrlTarInfo.FrInVlvCtrlTar[2] = FrInVlvCtrlTar[2];
	Pct_5msCtrlBus.Pct_5msCtrlWhlnVlvCtrlTarInfo.FrInVlvCtrlTar[3] = FrInVlvCtrlTar[3];
	Pct_5msCtrlBus.Pct_5msCtrlWhlnVlvCtrlTarInfo.FrInVlvCtrlTar[4] = FrInVlvCtrlTar[4];

	Pct_5msCtrlBus.Pct_5msCtrlWhlnVlvCtrlTarInfo.RlInVlvCtrlTar[0] = RlInVlvCtrlTar[0];
	Pct_5msCtrlBus.Pct_5msCtrlWhlnVlvCtrlTarInfo.RlInVlvCtrlTar[1] = RlInVlvCtrlTar[1];
	Pct_5msCtrlBus.Pct_5msCtrlWhlnVlvCtrlTarInfo.RlInVlvCtrlTar[2] = RlInVlvCtrlTar[2];
	Pct_5msCtrlBus.Pct_5msCtrlWhlnVlvCtrlTarInfo.RlInVlvCtrlTar[3] = RlInVlvCtrlTar[3];
	Pct_5msCtrlBus.Pct_5msCtrlWhlnVlvCtrlTarInfo.RlInVlvCtrlTar[4] = RlInVlvCtrlTar[4];

	Pct_5msCtrlBus.Pct_5msCtrlWhlnVlvCtrlTarInfo.RrInVlvCtrlTar[0] = RrInVlvCtrlTar[0];
	Pct_5msCtrlBus.Pct_5msCtrlWhlnVlvCtrlTarInfo.RrInVlvCtrlTar[1] = RrInVlvCtrlTar[1];
	Pct_5msCtrlBus.Pct_5msCtrlWhlnVlvCtrlTarInfo.RrInVlvCtrlTar[2] = RrInVlvCtrlTar[2];
	Pct_5msCtrlBus.Pct_5msCtrlWhlnVlvCtrlTarInfo.RrInVlvCtrlTar[3] = RrInVlvCtrlTar[3];
	Pct_5msCtrlBus.Pct_5msCtrlWhlnVlvCtrlTarInfo.RrInVlvCtrlTar[4] = RrInVlvCtrlTar[4];

}

static void LAIDB_vCallMuxCtrlr(void)
{
	LAIDB_vDecideMuxWhlCtrlStrategy();

	LAIDB_vCalcMuxCtrlVars();

	LAIDB_vDecideWhlCtrlSequence();

	LAIDB_vDecideMuxCtrlState();

	LAIDB_vDecideMuxCtrlMode();

}

/******************************************************************************
* FUNCTION NAME:      LAIDB_vDecideMuxWhlCtrlStrategy
* CALLED BY:          LAIDB_vCallMuxCtrlr()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Decide Target P base or Delta Target P base
******************************************************************************/
static void LAIDB_vDecideMuxWhlCtrlStrategy(void)
{
	int16_t s16FrontTarPTemp, s16RearTarPTemp;
	uint8_t u8FrontAxleSameTarP, u8RearAxleSameTarP;
	
	if(lapctu1InAbsCtrlActive==TRUE)
	{
		if(lapctu1InAbsCtrlFadeOut==TRUE)
		{
			lau1MuxPCtrlStrategy = U8_MUX_PCTRL_TARP_BASE;
		}
		else
		{
			/* ABS */
			lau1MuxPCtrlStrategy = U8_MUX_PCTRL_DELTAP_BASE ; /* 1: Delta-P / 0: Target P */
		}
	}
	else if(lapctu1EbdActive==TRUE)
	{
		lau1MuxPCtrlStrategy = U8_MUX_PCTRL_TARP_BASE;
	}       
	else    
	{
		/* None ABS */
		lau1MuxPCtrlStrategy = U8_MUX_PCTRL_TARP_BASE ; /* 1: Delta-P / 0: Target P */
		
		if(MuxCmdFrntLe.s16WhlTargetP == MuxCmdFrntRi.s16WhlTargetP)
		{
			u8FrontAxleSameTarP = (MuxCmdFrntLe.s16WhlTargetP > 0) ? 20 : 0;
		}
		else
		{
			if(MuxCmdFrntLe.s16WhlTargetP > MuxCmdFrntRi.s16WhlTargetP)
			{
				u8FrontAxleSameTarP = (MuxCmdFrntRi.s16WhlTargetP > 0) ? 30 : 12; 				
			}
			else
			{
				u8FrontAxleSameTarP = (MuxCmdFrntLe.s16WhlTargetP > 0) ? 30 : 11; 			
			}
		}

		if(MuxCmdReLe.s16WhlTargetP == MuxCmdReRi.s16WhlTargetP)
		{
			u8RearAxleSameTarP = (MuxCmdReLe.s16WhlTargetP > 0) ? 20 : 0;
		}
		else
		{
			if(MuxCmdReLe.s16WhlTargetP > MuxCmdReRi.s16WhlTargetP)
			{
				u8RearAxleSameTarP = (MuxCmdReRi.s16WhlTargetP > 0) ? 30 : 11; // lau1MuxPCtrlStrategy = U8_MUX_PCTRL_DELTAP_BASE ;					
			}
			else
			{
				u8RearAxleSameTarP = (MuxCmdReLe.s16WhlTargetP > 0) ? 30 : 12; // lau1MuxPCtrlStrategy = U8_MUX_PCTRL_DELTAP_BASE ;				
			}
		}
		
		if( (u8FrontAxleSameTarP == 30) || (u8RearAxleSameTarP == 30) )
		{
			lau1MuxPCtrlStrategy = U8_MUX_PCTRL_DELTAP_BASE ;	
		}
		else if( (u8FrontAxleSameTarP == 20) && (u8RearAxleSameTarP == 20) )
		{
			if(MuxCmdFrntLe.s16WhlTargetP == MuxCmdReLe.s16WhlTargetP)
			{
				lau1MuxPCtrlStrategy = U8_MUX_PCTRL_TARP_BASE;
			}
			else
			{
				lau1MuxPCtrlStrategy = U8_MUX_PCTRL_DELTAP_BASE ;
			}
		}
		else
		{
			if(u8FrontAxleSameTarP > 0)
			{
				if(u8RearAxleSameTarP > 0)
				{
					/*
					s16FrontTarPTemp = (u8FrontAxleSameTarP > 11) ? MuxCmdFrntLe.s16WhlTargetP : MuxCmdFrntRi.s16WhlTargetP;
					s16RearTarPTemp  = (u8RearAxleSameTarP  > 11) ? MuxCmdReRi.s16WhlTargetP   : MuxCmdReLe.s16WhlTargetP;
					if(s16FrontTarPTemp == s16RearTarPTemp)
					{
						lau1MuxPCtrlStrategy = U8_MUX_PCTRL_TARP_BASE;
					}
					else
					{
						lau1MuxPCtrlStrategy = U8_MUX_PCTRL_DELTAP_BASE ;
					}
					*/
					lau1MuxPCtrlStrategy = U8_MUX_PCTRL_DELTAP_BASE ;
				}
				else
				{
					lau1MuxPCtrlStrategy = U8_MUX_PCTRL_TARP_BASE;
				}
			}
			else
			{
				lau1MuxPCtrlStrategy = U8_MUX_PCTRL_TARP_BASE;
			}
		}		
	}

	lapctu8AbsMtrCtrlMode = U8_MUX_ABS_MCC_SELECTED_MODE; /*U8_MUX_ABS_MCC_TQ_MODE U8_MUX_ABS_MCC_POSI_MODE*/

	if( (lapctu1InAbsCtrlActive==TRUE) && (lapctu8AbsMtrCtrlMode == U8_MUX_ABS_MCC_TQ_MODE) )
	{
		lapctu1AbsMtrTqCtrlActive = TRUE;
	}
	else
	{
		lapctu1AbsMtrTqCtrlActive = FALSE;
	}

}

static void LAIDB_vCalcMuxCtrlVars(void)
{
	/* Check Stroke Recovery Control*/
	if(lapcts8MuxStrkRcvrCtrlState!=S8_MUX_STRK_RCVR_READY)
	{
		lau1MuxStrkRecvrCtrlNeed = 1;
	}
	else
	{
		lau1MuxStrkRecvrCtrlNeed = 0;
	}

	lau1MuxNChannelCtrlActive = (uint8_t)LAIDB_s16DecideMultiplexCtrlAct();

	if(lapctu1SafetyControlActive==TRUE)
	{
		lau8MultiplexCtrlActive = 1;
	}
	else
	{
		lau8MultiplexCtrlActive = 0;
	}

	if(lau1MuxPCtrlStrategy==U8_MUX_PCTRL_DELTAP_BASE)
	{
		MuxCmdFrntLe.u1CtrlReq = LAIDB_u32DecideMuxRequestPerDeltaP(&MuxCmdFrntLe,lapcts16MuxWhlTargetPOldFL, lapctu8OutVVPriorFL);
		MuxCmdFrntRi.u1CtrlReq = LAIDB_u32DecideMuxRequestPerDeltaP(&MuxCmdFrntRi,lapcts16MuxWhlTargetPOldFR, lapctu8OutVVPriorFR);
		MuxCmdReLe.u1CtrlReq =   LAIDB_u32DecideMuxRequestPerDeltaP(&MuxCmdReLe  ,lapcts16MuxWhlTargetPOldRL, lapctu8OutVVPriorRL);
		MuxCmdReRi.u1CtrlReq =   LAIDB_u32DecideMuxRequestPerDeltaP(&MuxCmdReRi  ,lapcts16MuxWhlTargetPOldRR, lapctu8OutVVPriorRR);
	}
	else
	{
		MuxCmdFrntLe.u1CtrlReq = LAIDB_u32DecideMuxRequestPerPerror(&MuxCmdFrntLe,lapcts16MuxWhlTargetPOldFL, lapctu8OutVVPriorFL);
		MuxCmdFrntRi.u1CtrlReq = LAIDB_u32DecideMuxRequestPerPerror(&MuxCmdFrntRi,lapcts16MuxWhlTargetPOldFR, lapctu8OutVVPriorFR);
		MuxCmdReLe.u1CtrlReq =   LAIDB_u32DecideMuxRequestPerPerror(&MuxCmdReLe  ,lapcts16MuxWhlTargetPOldRL, lapctu8OutVVPriorRL);
		MuxCmdReRi.u1CtrlReq =   LAIDB_u32DecideMuxRequestPerPerror(&MuxCmdReRi  ,lapcts16MuxWhlTargetPOldRR, lapctu8OutVVPriorRR);
	}

	MuxCmdFrntLe.u1CmdHalt = LAIDB_u32CheckPriorityCmdHaltPerWhl(&MuxCmdFrntLe, &MuxCurCmd, lapctu8OutVVPriorFL);
    MuxCmdFrntRi.u1CmdHalt = LAIDB_u32CheckPriorityCmdHaltPerWhl(&MuxCmdFrntRi, &MuxCurCmd, lapctu8OutVVPriorFR);
    MuxCmdReLe.u1CmdHalt =   LAIDB_u32CheckPriorityCmdHaltPerWhl(&MuxCmdReLe  , &MuxCurCmd, lapctu8OutVVPriorRL);
    MuxCmdReRi.u1CmdHalt =   LAIDB_u32CheckPriorityCmdHaltPerWhl(&MuxCmdReRi  , &MuxCurCmd, lapctu8OutVVPriorRR);

}

static void LAIDB_vDecideWhlCtrlSequence(void)
{
	uint8_t u8CurCmdState = U8_MUX_ST_NOT_EXECUTED;
	uint8_t u8CurCmdIdOld = MuxCurCmd.u8id;

	lau1MuxCurCmdIdChanged = 0;
	lapctu1NewReqDetected  = 0;
	/* u8DebuggerInfo = 0; */
	u8MuxSeqdebugger00 = 0;

	if(lau1MuxNChannelCtrlActive==TRUE)
	{
		/*LAIDB_vAllocMuxSeqPerPriority();*/

		LAIDB_vAllocMuxCmdsPerFIFO(&MuxCmdFrntLe,&MuxStFrntLe);
		LAIDB_vAllocMuxCmdsPerFIFO(&MuxCmdFrntRi,&MuxStFrntRi);
		LAIDB_vAllocMuxCmdsPerFIFO(&MuxCmdReLe  ,&MuxStReLe  );
		LAIDB_vAllocMuxCmdsPerFIFO(&MuxCmdReRi  ,&MuxStReRi  );

		LAIDB_vCheckNewCmdPriority();

		if(lau1MuxStrkRecvrCtrlNeed==1)
		{
			MuxCurCmd.u8id = U8_MUX_CMD_STRK_RECVR;
			LAIDB_vInitializeMuxSequence();
			u8MuxSeqdebugger00 = 1;
		}
		else if(MuxCurCmd.u1CmdHalt == TRUE)
		{
			u8MuxSeqdebugger00 = 7;
			if(LAIDB_u8MuxFIFO_Empty(&MuxFIFO)==FALSE)
			{
				if((MuxCurCmd.u8id==U8_MUX_CMD_NONE) || (MuxCurCmd.u8State==U8_MUX_ST_EXECUTED))
				{
					MuxCurCmd.u8id = LAIDB_u8MuxFIFO_Get(&MuxFIFO); /* update buffer address */
					LAIDB_vCopyMuxCmdInfo(&MuxCurCmd);
					lapctu1NewReqDetected = 1;
				}
				else
				{
					;
				}
			}
			LAIDB_vCalCmdExecTimeWhl(&MuxCurCmd); /* not needed for all wheels -> just for MuxCurCmd */
		}
		else if(MuxCurCmd.u1Priority == TRUE)
		{
			u8MuxSeqdebugger00 = 8;
			MuxCurCmd.u8id = LAIDB_u8MuxFIFO_Get(&MuxFIFO); /* update buffer address */
			LAIDB_vCopyMuxCmdInfo(&MuxCurCmd);  /* update target variables */
			LAIDB_vCalCmdExecTimeWhl(&MuxCurCmd);
			lapctu1NewReqDetected = 1;
		}
		else if(LAIDB_u8MuxFIFO_Empty(&MuxFIFO)==FALSE) /* there is at least one mux command */
		{
			u8MuxSeqdebugger00 = 2;
			if((MuxCurCmd.u8id==U8_MUX_CMD_NONE) || (MuxCurCmd.u8State==U8_MUX_ST_EXECUTED)/* || (MuxCurCmd.u8id==U8_MUX_CMD_STRK_RECVR)*/)
			{
				u8MuxSeqdebugger00 = 3;
				MuxCurCmd.u8id = LAIDB_u8MuxFIFO_Get(&MuxFIFO); /* update buffer address */
				LAIDB_vCopyMuxCmdInfo(&MuxCurCmd);
				LAIDB_vCalCmdExecTimeWhl(&MuxCurCmd); /* not needed for all wheels -> just for MuxCurCmd */
				lapctu1NewReqDetected = 1;
			} /*
			else if(MuxCurCmd.u1Priority == TRUE)
			{
				u8DebuggerInfo02 = 7;
				LAIDB_vCopyMuxCmdInfo(&MuxCurCmd);
				LAIDB_vCalCmdExecTimeWhl(&MuxCurCmd);
			}*/
			else
			{
				;
			}
		}
		else /* no multiplex request or last command is still processing */
		{
			u8MuxSeqdebugger00 = 4;
			if(MuxCurCmd.u8State==U8_MUX_ST_EXECUTED) /* last command terminated */
			{
				u8MuxSeqdebugger00 = 5;
				LAIDB_vInitializeMuxSequence();
			}
			else
			{
				; /* process remaining commands */
			}
		}
	}
	else
	{
		LAIDB_vInitializeMuxSequence();
	}

	if(MuxCurCmd.u8id!=U8_MUX_CMD_NONE)
	{
		lau1MuxCmdAllocated = 1;
		if((MuxCurCmd.u8id==U8_MUX_CMD_FL_RISE) || (MuxCurCmd.u8id==U8_MUX_CMD_FR_RISE) || (MuxCurCmd.u8id==U8_MUX_CMD_RL_RISE) || (MuxCurCmd.u8id==U8_MUX_CMD_RR_RISE))
		{
			lau1MuxPCtrlNeed = 1;
		}
		else
		{
			lau1MuxPCtrlNeed = 0;
		}
	}
	else
	{
		lau1MuxCmdAllocated = 1;
		lau1MuxPCtrlNeed = 0;
	}

	if(u8CurCmdIdOld!=MuxCurCmd.u8id)
	{
		lau1MuxCurCmdIdChanged = 1;
	}
	else
	{
		;
	}
}

static void LAIDB_vCopyMuxCmdInfo(LAMUXCTRLSTATE_t *pCurInfo)
/*static LAMUXCTRLSTATE_t LAIDB_vCopyMuxCmdInfo(LAMUXCTRLSTATE_t *pCurInfo)*/
{
	uint8_t u8InfoCopied = FALSE;
	u8InfoCopied = LAIDB_u8UpdateCurCmd(u8InfoCopied,pCurInfo,&MuxStFrntLe);
	u8InfoCopied = LAIDB_u8UpdateCurCmd(u8InfoCopied,pCurInfo,&MuxStFrntRi);
	u8InfoCopied = LAIDB_u8UpdateCurCmd(u8InfoCopied,pCurInfo,&MuxStReLe  );
	u8InfoCopied = LAIDB_u8UpdateCurCmd(u8InfoCopied,pCurInfo,&MuxStReRi  );

	/*u8DebuggerInfo = u8InfoCopied;*/

	/*return (*pCurInfo);  return data */
}

static uint8_t LAIDB_u8UpdateCurCmd(uint8_t u8InfoCopied, LAMUXCTRLSTATE_t *pCurInfo, LAMUXCTRLSTATE_t  *pInputInfo)
{
	if(u8InfoCopied==FALSE) /* copy info only once */
	{
		if((pCurInfo->u8id) == (pInputInfo->u8id))
		{
			pCurInfo->u8State = pInputInfo->u8State;
			pCurInfo->s16WhlTargetDeltaP = pInputInfo->s16WhlTargetDeltaP;
			pCurInfo->s16WhlTargetP = pInputInfo->s16WhlTargetP;
			pCurInfo->s16WhlTargetPRate = pInputInfo->s16WhlTargetPRate;
			pCurInfo->s16EstWhlP = pInputInfo->s16EstWhlP;
			pCurInfo->u1CmdHalt = pInputInfo->u1CmdHalt;
			/* copy structure? */

			u8InfoCopied = TRUE;
		}
		else
		{
			; /* Do Nothing */
		}
	}
	else
	{
		; /* Do Nothing */
	}
	return u8InfoCopied;
}

static void LAIDB_vInitializeMuxWheelStVars(void)
{
	LAMUXCTRLSTATE_t FLInitvars = {0,0,0,0,0,0,U8_MUX_CMD_FL_RISE,U8_MUX_CMD_NONE,};
	LAMUXCTRLSTATE_t FRInitvars = {0,0,0,0,0,0,U8_MUX_CMD_FR_RISE,U8_MUX_CMD_NONE,};
	LAMUXCTRLSTATE_t RLInitvars = {0,0,0,0,0,0,U8_MUX_CMD_RL_RISE,U8_MUX_CMD_NONE,};
	LAMUXCTRLSTATE_t RRInitvars = {0,0,0,0,0,0,U8_MUX_CMD_RR_RISE,U8_MUX_CMD_NONE,};

	MuxStFrntLe 	= FLInitvars;
	MuxStFrntRi 	= FRInitvars;
	MuxStReLe    	= RLInitvars;
	MuxStReRi   	= RRInitvars;
}
	
static void LAIDB_vInitializeMuxCmdVars(void)
{
	LAMUXCTRLSTATE_t MUXCmdInitvars = {0,0,0,0,0,0,U8_MUX_CMD_NONE,U8_MUX_CMD_NONE,};
	MuxCurCmd 		= MUXCmdInitvars;
	laidbu16CmdExecTimer = 0;
}

static void LAIDB_vInitializeMuxSequence(void)
{
	uint8_t u8InitErr;

	u8InitErr = LAIDB_u8MuxFIFO_Init(&MuxFIFO,0,U8_MUX_FIFO_BUFFER_SIZE);

	/*
	(*(MuxFIFO.buffer+0)) = 0;
	(*(MuxFIFO.buffer+1)) = 0;
	(*(MuxFIFO.buffer+2)) = 0;
	(*(MuxFIFO.buffer+3)) = 0;
	(*(MuxFIFO.buffer+4)) = 0;
	(*(MuxFIFO.buffer+5)) = 0;
	*/
}

static void LAIDB_vAllocMuxCmdsPerFIFO(LAMUXCTRLCMD_t *pMuxCmd, LAMUXCTRLSTATE_t *pMuxSt)
{
	uint8_t u8CmdAdded = 0;
	uint8_t u8CmdInserted = 0;
	uint8_t u8CmdRemoved  = 0;
	uint8_t u8CmdWithPriority = 0;
	uint8_t u8CurCmdCheck = 0;

    #if defined (__MUX_DEBUG)
    	TempBuffLogRem = MuxFIFO;
    #endif
    
	if(MuxCurCmd.u8id == pMuxCmd->u8id)
	{
		u8CurCmdCheck = TRUE;
	}

	if((pMuxCmd->u1CtrlReq==TRUE) || (/*(u8CurCmdCheck==FALSE) && */(pMuxCmd->u1CmdHalt)))
	{
		LAIDB_vAllocMuxCmdInfo(pMuxCmd, pMuxSt); /* update every scan while req==1 */

		if(pMuxSt->u1CmdHalt==TRUE)
		{
			u8CmdRemoved = LAIDB_u8MuxRemoveFromBuffer(&MuxFIFO, pMuxSt->u8id);
		}
		else if(pMuxSt->u1Priority == TRUE)
		{
			u8CmdWithPriority = pMuxSt->u8id;
			/*
			if(pMuxSt->u1CmdHalt==TRUE)
		    {
				// if(pMuxSt->u8State == U8_MUX_ST_EXECUTED) // remove after end transition 
				{
					u8CmdRemoved = LAIDB_u8MuxRemoveFromBuffer(&MuxFIFO, u8CmdWithPriority);
				}
		    }
			else
			*/
			{
				/* if(pMuxSt->u8State == U8_MUX_ST_EXECUTED) // remove after end transition */
				{
					u8CmdRemoved = LAIDB_u8MuxRemoveFromBuffer(&MuxFIFO, u8CmdWithPriority);
				}

				/* if(u8CmdRemoved == u8CmdWithPriority) */
				{
					u8CmdInserted = LAIDB_u8MuxInsertIntoBuffer(&MuxFIFO, u8CmdWithPriority);
				}
		    }
		}/*
		else if(pMuxSt->u1CmdHalt==TRUE)
		{
			u8CmdRemoved = LAIDB_u8MuxRemoveFromBuffer(&MuxFIFO, pMuxSt->u8id);
		}*/
		else
		{
			if((pMuxSt->u8State == U8_MUX_ST_NONE) || (pMuxSt->u8State == U8_MUX_ST_EXECUTED)) /* put only when this command is not in buffer */
			{
				u8CmdAdded = LAIDB_u8MuxFIFO_Put(&MuxFIFO, pMuxSt->u8id);
			}
		}
	}

	pMuxSt->u1BuffFIFOAdded = (u8CmdAdded>0)?    TRUE : FALSE;
	pMuxSt->u1BuffRemoved   = (u8CmdRemoved>0)?  TRUE : FALSE;
	pMuxSt->u1BuffInserted  = (u8CmdInserted>0)? TRUE : FALSE;
}

static void LAIDB_vAllocMuxCmdInfo(LAMUXCTRLCMD_t *pMuxCmd, LAMUXCTRLSTATE_t *pMuxSt)
{
	pMuxSt->s16WhlTargetDeltaP	 = pMuxCmd->s16WhlTargetDeltaP;
	pMuxSt->s16WhlTargetP 		 = pMuxCmd->s16WhlTargetP;
	pMuxSt->s16EstWhlP			 = pMuxCmd->s16EstWhlP;
	pMuxSt->s16WhlTargetPRate	 = pMuxCmd->s16WhlTargetPRate;
	pMuxSt->u1Priority			 = pMuxCmd->u1Priority;
	pMuxSt->u1PriorityCmdPerWheel= pMuxCmd->u1PriorityCmdPerWheel;
	pMuxSt->u1CmdHalt            = pMuxCmd->u1CmdHalt;
}

static void LAIDB_vCalCmdExecTimeWhl(LAMUXCTRLSTATE_t *pMuxCmd)
{
	int16_t s16MuxTarRiseWhlP = 0;
	uint16_t u16ExecTimePhase1 = 0; /* Approaching to Est wheel pressure*/
	uint16_t u16ExecTimePhase2 = 0; /* Approaching to Target wheel pressure*/
	uint16_t u16TotalExecTimeFromDeltaP = 0;
	uint16_t u16ExecTimeConsSystDelay = 0;
	u8MuxExeTdebugger00 = 60;
	u8MuxExeTdebugger01 = 70;

	if(pMuxCmd->u8id == U8_MUX_CMD_STRK_RECVR)
	{
		pMuxCmd->u16CmdExecTimeMs = 20; /* 20ms */
	}
	else if(pMuxCmd->u1CmdHalt==TRUE)
	{
		pMuxCmd->u16CmdExecTimeMs = 0;
	}
	else
	{
		if(lau1MuxPCtrlStrategy==U8_MUX_PCTRL_DELTAP_BASE)
		{
			s16MuxTarRiseWhlP = pMuxCmd->s16WhlTargetDeltaP;
		}
		else
		{
			s16MuxTarRiseWhlP = pMuxCmd->s16WhlTargetP - pMuxCmd->s16EstWhlP;
		}

		u16ExecTimePhase1 = 0;/*LAIDB_u16CalcWhlCmdExecTimePhs1(pMuxCmd);*/
		u16ExecTimePhase2 = LAIDB_u16CalcExecTimeFromDeltaP(s16MuxTarRiseWhlP,pMuxCmd->s16WhlTargetPRate);
		u8MuxExeTdebugger00 = u16ExecTimePhase2	;
		u16TotalExecTimeFromDeltaP = u16ExecTimePhase1 + u16ExecTimePhase2;

		u16ExecTimeConsSystDelay = LAIDB_u16CompExecTimePerDelay(u16TotalExecTimeFromDeltaP, pMuxCmd->u8id);
		u8MuxExeTdebugger01 = u16ExecTimeConsSystDelay;

		pMuxCmd->u16CmdExecTimeMs = L_u16LimitMinMax(u16ExecTimeConsSystDelay,0,50/*1000*/); /* max limit 50ms */
	}

	if(pMuxCmd->u1CmdHalt==TRUE)
	{
		pMuxCmd->u16CmdExecTime = (laidbu16CmdExecTimer>U16_WHL_SWITCH_DELAY_TIME)?(laidbu16CmdExecTimer-U16_WHL_SWITCH_DELAY_TIME):0;
	}
	else
	{
		if((pMuxCmd->u16CmdExecTimeMs % U8_IDB_LOOPTIME) > 0)
		{
			pMuxCmd->u16CmdExecTime = (pMuxCmd->u16CmdExecTimeMs/U8_IDB_LOOPTIME) + 1; /* round up */
		}
		else
		{
			pMuxCmd->u16CmdExecTime = (pMuxCmd->u16CmdExecTimeMs/U8_IDB_LOOPTIME);
		}
	}
}
static uint16_t LAIDB_u16CalcWhlCmdExecTimePhs1(LAMUXCTRLSTATE_t *pMuxCmd)
{
	int16_t s16MuxEstWhlPDiff = 0;
	uint16_t u16ExecTimePhase1 = 0;

#if __P_SENSOR_MEASURE_TYPE== EACH_CIRCUIT_PRESSURE
	if((pMuxCmd->u8id==U8_MUX_CMD_FR_RISE) || (pMuxCmd->u8id==U8_MUX_CMD_RL_RISE))
	{
		s16MuxEstWhlPDiff = pMuxCmd->s16EstWhlP - lapcts16MuxPistP; /*(laidbs16MuxTarPress/S16_PCTRL_PRESS_0_1_BAR)*/
	}
	else if((pMuxCmd->u8id==U8_MUX_CMD_FL_RISE) || (pMuxCmd->u8id==U8_MUX_CMD_RR_RISE))
	{
		s16MuxEstWhlPDiff = pMuxCmd->s16EstWhlP - lapcts16MuxPistPSecd; /*(laidbs16MuxTarPress/S16_PCTRL_PRESS_0_1_BAR)*/
	}
#else
	s16MuxEstWhlPDiff = pMuxCmd->s16EstWhlP - lapcts16MuxPistP; /*(laidbs16MuxTarPress/S16_PCTRL_PRESS_0_1_BAR)*/
#endif

	if(s16MuxEstWhlPDiff < 0)
	{
		s16MuxEstWhlPDiff = -s16MuxEstWhlPDiff;
	}

	if(s16MuxEstWhlPDiff > S16_MUX_TP_FAST_APROACH_REF)
	{
		u16ExecTimePhase1 = LAIDB_u16CalcExecTimeFromDeltaP(s16MuxEstWhlPDiff,S16_MAX_PCTRL_RATE_BPS); /* max rate */
		u16ExecTimePhase1 = L_u16LimitMinMax(u16ExecTimePhase1,0,5/*20*/);
	}
	else
	{
		u16ExecTimePhase1 = 0;
	}

	return u16ExecTimePhase1;
}

static uint16_t LAIDB_u16CompExecTimePerDelay(uint16_t u16ExecTimeMs, uint8_t u8CmdId)
{
	/* limit min time Execute Inlet Valve */
	if((u8CmdId==U8_MUX_CMD_FL_RISE) || (u8CmdId==U8_MUX_CMD_FR_RISE))
	{
		/* Reduce Rise Time considering valve delay time */
		if(u16ExecTimeMs > U8_INF_VV_CLOSE_DELAY)
		{
			u16ExecTimeMs = u16ExecTimeMs - U8_INF_VV_CLOSE_DELAY;
		}

		/* Limit to Front Valve min. execute time */
		u16ExecTimeMs = (u16ExecTimeMs<U8_INF_VV_MIN_OPEN_T)?(U8_INF_VV_MIN_OPEN_T):(u16ExecTimeMs);
	}
	else if((u8CmdId==U8_MUX_CMD_RL_RISE) || (u8CmdId==U8_MUX_CMD_RR_RISE))
	{
		/* Reduce Rise Time considering valve delay time */
		if(u16ExecTimeMs > U8_INR_VV_CLOSE_DELAY)
		{
			u16ExecTimeMs = u16ExecTimeMs - U8_INR_VV_CLOSE_DELAY;
		}

		/* Limit to Front Valve min. execute time */
		u16ExecTimeMs = (u16ExecTimeMs<U8_INR_VV_MIN_OPEN_T)?(U8_INR_VV_MIN_OPEN_T):(u16ExecTimeMs);
	}
	else
	{
		; /* stroke recovery no limit */
	}

	return u16ExecTimeMs;
}

static void LAIDB_vCheckNewCmdPriority(void)
{
	uint8_t u8CurCmdHalt = FALSE;
	uint8_t u8CurCmdPriority = FALSE;

	u8CurCmdPriority = LAIDB_u8CheckNewCmdPriorityPerWhl(u8CurCmdPriority, &MuxCurCmd, &MuxCmdFrntLe, &MuxStFrntLe);
	u8CurCmdPriority = LAIDB_u8CheckNewCmdPriorityPerWhl(u8CurCmdPriority, &MuxCurCmd, &MuxCmdFrntRi, &MuxStFrntRi);
	u8CurCmdPriority = LAIDB_u8CheckNewCmdPriorityPerWhl(u8CurCmdPriority, &MuxCurCmd, &MuxCmdReLe  , &MuxStReLe  );
	u8CurCmdPriority = LAIDB_u8CheckNewCmdPriorityPerWhl(u8CurCmdPriority, &MuxCurCmd, &MuxCmdReRi  , &MuxStReRi  );

	if(u8CurCmdPriority==TRUE)
	{
		MuxCurCmd.u1Priority = TRUE;
	}
	else
	{
		MuxCurCmd.u1Priority = FALSE;
	}

	u8CurCmdHalt = LAIDB_u8CheckNewCmdHaltPerWhl(u8CurCmdHalt, &MuxCurCmd, &MuxCmdFrntLe);
	u8CurCmdHalt = LAIDB_u8CheckNewCmdHaltPerWhl(u8CurCmdHalt, &MuxCurCmd, &MuxCmdFrntRi);
	u8CurCmdHalt = LAIDB_u8CheckNewCmdHaltPerWhl(u8CurCmdHalt, &MuxCurCmd, &MuxCmdReLe  );
	u8CurCmdHalt = LAIDB_u8CheckNewCmdHaltPerWhl(u8CurCmdHalt, &MuxCurCmd, &MuxCmdReRi  );

	if(u8CurCmdHalt==TRUE)
	{
		MuxCurCmd.u1CmdHalt = TRUE;
	}
	else
	{
		MuxCurCmd.u1CmdHalt = FALSE;
	}
}

static uint32_t LAIDB_u32CheckPriorityCmdHaltPerWhl(const LAMUXCTRLCMD_t *pCmdInfo, LAMUXCTRLSTATE_t *pCurInfo, uint8_t u8OutVlvReq)
{
    uint32_t u32CmdHalt = 0;
    uint32_t u32CmdHaltCondi1 = FALSE;
    uint32_t u32CmdHaltCondi2 = FALSE;

	if(lau1MuxNChannelCtrlActive==TRUE)
    /*if(pCmdInfo->u1Priority==TRUE)*/
	{
		/*u32CmdHaltCondi1 = ((pCmdInfo->u1Priority==TRUE)&&(pCmdInfo->u8id==pCurInfo->u8id))?TRUE:FALSE;*/
		u32CmdHaltCondi2 = (u8OutVlvReq == TRUE)?TRUE:FALSE;

		if((u32CmdHaltCondi1==TRUE)||(u32CmdHaltCondi2==TRUE))
		{
			u32CmdHalt = TRUE;
		}
		else
		{
			u32CmdHalt = FALSE;
		}
	}

	return u32CmdHalt;
}

static uint8_t LAIDB_u8CheckNewCmdPriorityPerWhl(uint8_t u8CurCmdPriority, LAMUXCTRLSTATE_t *pCurInfo, LAMUXCTRLCMD_t *pMuxCmd, LAMUXCTRLSTATE_t  *pInputInfo)
{
	if(u8CurCmdPriority==FALSE)
	{
		if(((pInputInfo->u8id == pCurInfo->u8id)&&(pInputInfo->u1Priority == TRUE))&&(pMuxCmd->u1CtrlReq==TRUE))
		{
			u8CurCmdPriority = TRUE;
		}
	}

	return u8CurCmdPriority;
}

static uint8_t LAIDB_u8CheckNewCmdHaltPerWhl(uint8_t u8CurCmdHalt, LAMUXCTRLSTATE_t *pCurInfo, LAMUXCTRLCMD_t *pMuxCmd)
{
	if(u8CurCmdHalt==FALSE)
	{
		if((pMuxCmd->u8id == pCurInfo->u8id) && (pMuxCmd->u1CmdHalt == TRUE))
		{
			u8CurCmdHalt = TRUE;
		}
	}

	return u8CurCmdHalt;
}

static void LAIDB_vDecideMuxActuatorCmd(void)
{
	LAIDB_vDecideEachWhlCtrlMode();

	LAIDB_vDecideEachWhlValveActRatio();
	LAIDB_vDecideMuxWhlVlvCmd();

	LAIDB_vDecideMuxMtrTarget(); /* Need to change name */

	LAIDB_vDecideFinalTargetPress(); /* Represent target press */
}

static void LAIDB_vDecideMuxCtrlState(void)
{
	int16_t s16MuxCtrlFadeOutEndDct;

	/* Failure check
	if(las16MuxCtrlReq==NEEDED)
	{
		las16MuxCtrlState = INHIBITION;
	}
	 */

	switch (las16MuxCtrlState)
	{
	case U8_MULTIPLEX_NONCONTROL:

		if(lau1MuxNChannelCtrlActive==TRUE)
		{
			las16MuxCtrlState = U8_MULTIPLEX_CONTROL;
		}

		break;

	case U8_MULTIPLEX_CONTROL:

		if(lau1MuxNChannelCtrlActive==FALSE)
		{
			las16MuxCtrlState = U8_MULTIPLEX_FADEOUT;
			las16MuxFadeOutCnt = 0;
		}

		break;

	case U8_MULTIPLEX_FADEOUT:

		s16MuxCtrlFadeOutEndDct = LAIDB_s16CheckMuxCtrlFadeOutEnd();

		if(s16MuxCtrlFadeOutEndDct==TRUE)
		{
			las16MuxCtrlState = U8_MULTIPLEX_NONCONTROL;
		}
		break;

	case U8_MULTIPLEX_INHIBITION:

		las16MuxCtrlState = U8_MULTIPLEX_NONCONTROL; /* FM 에 따라서 내용 추가 필요  - 정상 상황 check 구문 필요*/

		break;

	default:

		las16MuxCtrlState = U8_MULTIPLEX_NONCONTROL;

		break;
	}
}

static int16_t LAIDB_s16DecideMultiplexCtrlAct(void)
{
	int16_t s16MultiplexActive = 0;
	int8_t s8MuxAbsCtrlActive = 0;

	if( (lapctu1InAbsCtrlActive==TRUE) && (lapctu8AbsMtrCtrlMode == U8_MUX_ABS_MCC_POSI_MODE) )
	{
		if(lapctu1InAbsCtrlFadeOut==TRUE)
		{
			; /* FALSE */
		}
		else
		{
			s16MultiplexActive = TRUE;
		}
	}
	else if(lapctu1EbdPulseUpActive==TRUE)
	{
		s16MultiplexActive = TRUE;
	}
	else if(lapctu1EscTwoChannel==TRUE)
	{
		s16MultiplexActive = TRUE;
	}	
	else
	{
		; /* FALSE */
	}

	return s16MultiplexActive;
}


static uint16_t LAIDB_u16CalcExecTimeFromDeltaP(int32_t s32TarDeltaP, int32_t s32TarPressRateBarPSec)
{
	uint16_t u16TarTimeMs = 0;

	if((s32TarDeltaP > S16_PCTRL_PRESS_0_BAR) && (s32TarPressRateBarPSec > 0))
	{
		u16TarTimeMs = (uint16_t)((s32TarDeltaP*U16_RESOLCHG_BARPSEC_TO_MS)/s32TarPressRateBarPSec);
	}
	else
	{
		u16TarTimeMs = 0; /* Multiplex is only for rise commands */
	}

	return u16TarTimeMs;
}

static int16_t LAIDB_s16CheckMuxCtrlFadeOutEnd(void)
{
	int16_t s16MuxFadeOutTimeRef = U8_T_25_MS;

	/* to add fade out strategy */
	if(las16MuxCtrlState == U8_MULTIPLEX_CONTROL) /* fade out 1st scan */
	{
		las16MuxFadeOutCnt = 0;
		las16MuxFadeOutStrTP = 0;
	}
	else
	{
		las16MuxFadeOutCnt = las16MuxFadeOutCnt + 1;
	}

	if(las16MuxFadeOutCnt==1)
	{
		las16MuxFadeOutStrTP = laidbs16MuxTarPress; /* reset: fadeoutcnt와 sync */
	}
	else
	{
		; /* Do Nothing */
	}

	if(s16MuxFadeOutTimeRef == 0)
	{
		las16FadeoutTPDiffLimit = 0;
	}
	else
	{
		las16FadeoutTPDiffLimit = (lapcts16MuxPwrPistonTarPInput - las16MuxFadeOutStrTP)/s16MuxFadeOutTimeRef;
		las16FadeoutTPDiffLimit = (las16FadeoutTPDiffLimit<S16_PCTRL_PRESS_0_BAR)?(-las16FadeoutTPDiffLimit):las16FadeoutTPDiffLimit;
		las16FadeoutTPDiffLimit = (las16FadeoutTPDiffLimit<S16_PCTRL_PRESS_3_BAR)?S16_PCTRL_PRESS_3_BAR:las16FadeoutTPDiffLimit;
	}

	if(las16MuxFadeOutCnt>=s16MuxFadeOutTimeRef)
	{
		las16MuxFadeOutCnt = 0;
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

static void LAIDB_vDecideMuxCtrlMode(void)
{
	if(las16MuxCtrlState==U8_MULTIPLEX_NONCONTROL)
	{
		las16MuxCtrlMode = S8_MUX_MODE_NONCONTROL;
	}
	else if(las16MuxCtrlState==U8_MULTIPLEX_INHIBITION)
	{
		las16MuxCtrlMode = S8_MUX_MODE_INHIBITION;
	}
	else
	{
		;
	}

	switch (las16MuxCtrlMode)
	{
	case S8_MUX_MODE_NONCONTROL:

		if(las16MuxCtrlState==U8_MULTIPLEX_CONTROL)
		{
			if(MuxCurCmd.u8id==U8_MUX_CMD_STRK_RECVR)
			{
				las16MuxCtrlMode = S8_MUX_MODE_STROKE_RECOVERY;
				laidbu16CmdExecTimer = LAIDB_u16RunMuxCmdExecCounter(&MuxCurCmd,laidbu16CmdExecTimer);
				MuxCurCmd.u8State = LAIDB_u8SetMuxCtrlState(MuxCurCmd.u16CmdExecTime,laidbu16CmdExecTimer);
			}
			else if(lau1MuxPCtrlNeed==TRUE)
			{
				las16MuxCtrlMode = S8_MUX_MODE_PCTRL;
				laidbu16CmdExecTimer = LAIDB_u16RunMuxCmdExecCounter(&MuxCurCmd,laidbu16CmdExecTimer);
				MuxCurCmd.u8State = LAIDB_u8SetMuxCtrlState(MuxCurCmd.u16CmdExecTime,laidbu16CmdExecTimer);
			}
			else
			{
				;
			}
		}
		else
		{
			LAIDB_vInitializeMuxWheelStVars();
			LAIDB_vInitializeMuxCmdVars();
		}
		break;

	case S8_MUX_MODE_STROKE_RECOVERY:

		if(lau1MuxStrkRecvrCtrlNeed==0)/*(MuxCurCmd.id!=U8_MUX_CMD_STRK_RECVR)*/
		{
			las16MuxCtrlMode = S8_MUX_MODE_SR_TRANSITION;
			laidbu16CmdExecTimer = 0;
			MuxCurCmd.u8State = U8_MUX_ST_EXECUTED;
		}
		else
		{
			;
		}
		/*stroke recovery state 에 따라 transition mode*/

		break;

	case S8_MUX_MODE_PCTRL:

		laidbu16CmdExecTimer = LAIDB_u16RunMuxCmdExecCounter(&MuxCurCmd,laidbu16CmdExecTimer);
		MuxCurCmd.u8State = LAIDB_u8SetMuxCtrlState(MuxCurCmd.u16CmdExecTime,laidbu16CmdExecTimer);

		if(MuxCurCmd.u8id==U8_MUX_CMD_STRK_RECVR)
		{
			las16MuxCtrlMode = S8_MUX_MODE_STROKE_RECOVERY;
			LAIDB_vInitializeMuxWheelStVars();
		}
		else if((MuxCurCmd.u8State==U8_MUX_ST_TRANSITION) || ((MuxCurCmd.u8State==U8_MUX_ST_EXECUTED)&&(lapctu8AllowSwitchingDelay==1)))
		{
			las16MuxCtrlMode = S8_MUX_MODE_PCTRL_TRANSITION;
		}
		else
		{
			;
		}

		break;

	case S8_MUX_MODE_PCTRL_TRANSITION:

		laidbu16CmdExecTimer = LAIDB_u16RunMuxCmdExecCounter(&MuxCurCmd,laidbu16CmdExecTimer);
		MuxCurCmd.u8State = LAIDB_u8SetMuxCtrlState(MuxCurCmd.u16CmdExecTime,laidbu16CmdExecTimer);

		if(MuxCurCmd.u8id==U8_MUX_CMD_STRK_RECVR)
		{
			las16MuxCtrlMode = S8_MUX_MODE_STROKE_RECOVERY;
			LAIDB_vInitializeMuxWheelStVars();
		}
		else if(MuxCurCmd.u8State==U8_MUX_ST_FIRST_EXECUTED)
		{
			las16MuxCtrlMode = S8_MUX_MODE_PCTRL;
		}
		else
		{
			;
		}

		break;

	case S8_MUX_MODE_SR_TRANSITION:

		/*las16MuxCtrlMode = MUX_MODE_PCTRL;*/
		/*
		if(laidbu16SRTransTime>=(U16_WHL_SWITCH_DELAY_TIME))
		{
			las16MuxCtrlMode = MUX_MODE_PCTRL;
		}
		laidbu16SRTransTime++;
		*/

		laidbu16CmdExecTimer = LAIDB_u16RunMuxCmdExecCounter(&MuxCurCmd,laidbu16CmdExecTimer);
		MuxCurCmd.u8State = LAIDB_u8SetMuxCtrlState(MuxCurCmd.u16CmdExecTime,laidbu16CmdExecTimer);

		if(MuxCurCmd.u8id==U8_MUX_CMD_STRK_RECVR)
		{
			las16MuxCtrlMode = S8_MUX_MODE_STROKE_RECOVERY;
			LAIDB_vInitializeMuxWheelStVars();
		}
		else if(MuxCurCmd.u8State==U8_MUX_ST_FIRST_EXECUTED)
		{
			las16MuxCtrlMode = S8_MUX_MODE_PCTRL;
		}
		else
		{
			;
		}

		break;

	case S8_MUX_MODE_INHIBITION:

		LAIDB_vInitializeMuxWheelStVars();
		LAIDB_vInitializeMuxCmdVars();


		if(las16MuxCtrlState==U8_MULTIPLEX_NONCONTROL)
		{
			las16MuxCtrlMode = S8_MUX_MODE_NONCONTROL;
		}
		else
		{
			; /* Do nothing */
		}


		break;
	default:

		las16MuxCtrlMode = S8_MUX_MODE_PCTRL;

		break;

	}
	
	LAIDB_vUpdateMuxCtrlStateInfo();
}

static void LAIDB_vUpdateMuxCtrlStateInfo(void)
{
    uint8_t u8InfoCopied = FALSE;
    
	u8InfoCopied = LAIDB_u8UpdateMuxWhlState(u8InfoCopied,&MuxStFrntLe,&MuxCurCmd);
	u8InfoCopied = LAIDB_u8UpdateMuxWhlState(u8InfoCopied,&MuxStFrntRi,&MuxCurCmd);
	u8InfoCopied = LAIDB_u8UpdateMuxWhlState(u8InfoCopied,&MuxStReLe  ,&MuxCurCmd);
	u8InfoCopied = LAIDB_u8UpdateMuxWhlState(u8InfoCopied,&MuxStReRi  ,&MuxCurCmd);
}

static uint8_t LAIDB_u8UpdateMuxWhlState(uint8_t u8InfoCopied, LAMUXCTRLSTATE_t *pCurInfo, LAMUXCTRLSTATE_t  *pInputInfo)
{
	if(u8InfoCopied==FALSE) /* copy info only once */
	{
		if((pCurInfo->u8id) == (pInputInfo->u8id))
		{
			pCurInfo->u8State = pInputInfo->u8State;
			/* copy structure? */

			u8InfoCopied = TRUE;
		}
		else
		{
			; /* Do Nothing */
		}
	}
	else
	{
		; /* Do Nothing */
	}
	return u8InfoCopied;
}

static uint16_t LAIDB_u16RunMuxCmdExecCounter(LAMUXCTRLSTATE_t *pCmd, uint16_t u16Timer)
{
	uint16_t u16WhlSwitchTime = 0;

	if(lapctu1NewReqDetected==1)
	{
		u16Timer = 0;
	}
	else
	{
		; /* Do Nothing */
	}

	u16Timer = u16Timer + 1;

	return u16Timer;
}


static uint8_t LAIDB_u8SetMuxCtrlState(uint16_t u16CmdExecTimeRef, uint16_t u16Timer)
{
	uint16_t u16WhlSwitchTime = 0;
	uint8_t  u8MuxCtrlState = 0;

	lapctu8AllowSwitchingDelay = 0;

	if(U16_WHL_SWITCH_DELAY_TIME > 0)
	{
		u16WhlSwitchTime = U16_WHL_SWITCH_DELAY_TIME;
	}
	else
	{
		; /* if U16_WHL_SWITCH_DELAY_TIME is 0, u16WhlSwitchTime is 5ms */
	}

	if((u16Timer>=(u16CmdExecTimeRef + u16WhlSwitchTime)) || (u16Timer==0))
	{
		u8MuxCtrlState = U8_MUX_ST_EXECUTED;
		if((u16WhlSwitchTime>0) || (u16Timer>(u16CmdExecTimeRef + u16WhlSwitchTime)))
		{
			lapctu8AllowSwitchingDelay = 1;
		}
	}
	else if(u16Timer>=(u16CmdExecTimeRef+1))
	{
		u8MuxCtrlState = U8_MUX_ST_TRANSITION;
	}
	else if(u16Timer==1)
	{
		u8MuxCtrlState = U8_MUX_ST_FIRST_EXECUTED;
	}
	else
	{
		u8MuxCtrlState = U8_MUX_ST_EXECUTING;
	}

	return u8MuxCtrlState;
}


static void LAIDB_vDecideEachWhlCtrlMode(void)
{
	uint8_t u8CtrlWhlID;
	uint8_t u8MuxFLCtrlMode, u8MuxFRCtrlMode, u8MuxRLCtrlMode, u8MuxRRCtrlMode;
	int16_t s16MuxWheelVolumeState = U16_SEL_4WHEEL;

	if(lau1MuxNChannelCtrlActive==TRUE)
	{
		u8CtrlWhlID = MuxCurCmd.u8id;

		u8MuxFLCtrlMode = LAPCT_u8MuxSetWhlCtrlCmdNCh(&MuxCurCmd, &MuxStFrntLe ,&laMux_FLCMD);
		u8MuxFRCtrlMode = LAPCT_u8MuxSetWhlCtrlCmdNCh(&MuxCurCmd, &MuxStFrntRi ,&laMux_FRCMD);
		u8MuxRLCtrlMode = LAPCT_u8MuxSetWhlCtrlCmdNCh(&MuxCurCmd, &MuxStReLe   ,&laMux_RLCMD);
		u8MuxRRCtrlMode = LAPCT_u8MuxSetWhlCtrlCmdNCh(&MuxCurCmd, &MuxStReRi   ,&laMux_RRCMD);

		if((u8MuxFLCtrlMode==U8_MUXWHLCTRL_APPLY) || (u8MuxFRCtrlMode==U8_MUXWHLCTRL_APPLY))
		{
			s16MuxWheelVolumeState = U16_SEL_F1WHEEL; /* Later to be removed */
		}
		else if((u8MuxRLCtrlMode==U8_MUXWHLCTRL_APPLY) || (u8MuxRRCtrlMode==U8_MUXWHLCTRL_APPLY))
		{
			s16MuxWheelVolumeState = U16_SEL_R1WHEEL; /* Later to be removed */
		}
		else
		{
			;
		}
	}
	else
	{
		u8MuxFLCtrlMode = LAPCT_u8MuxSetWhlCtrlCmd1Ch(&MuxCmdFrntLe, &MuxStFrntLe ,&laMux_FLCMD);
		u8MuxFRCtrlMode = LAPCT_u8MuxSetWhlCtrlCmd1Ch(&MuxCmdFrntRi, &MuxStFrntRi ,&laMux_FRCMD);
		u8MuxRLCtrlMode = LAPCT_u8MuxSetWhlCtrlCmd1Ch(&MuxCmdReLe  , &MuxStReLe   ,&laMux_RLCMD);
		u8MuxRRCtrlMode = LAPCT_u8MuxSetWhlCtrlCmd1Ch(&MuxCmdReRi  , &MuxStReRi   ,&laMux_RRCMD);

		s16MuxWheelVolumeState = U16_SEL_4WHEEL; /* Later to be removed */

	}

	lapctu8WhlCtrlIndex = 0;

	if((u8MuxFLCtrlMode==U8_MUXWHLCTRL_APPLY) || (u8MuxFLCtrlMode==U8_MUXWHLCTRL_FULLRISE))
	{
		lapctu8WhlCtrlIndex += 20; /*secondary*/
	}
	if((u8MuxFRCtrlMode==U8_MUXWHLCTRL_APPLY) || (u8MuxFRCtrlMode==U8_MUXWHLCTRL_FULLRISE))
	{
		lapctu8WhlCtrlIndex += 10; /*primary*/
	}
	if((u8MuxRLCtrlMode==U8_MUXWHLCTRL_APPLY) || (u8MuxRLCtrlMode==U8_MUXWHLCTRL_FULLRISE))
	{
		lapctu8WhlCtrlIndex += 1; /*primary*/
	}
	if((u8MuxRRCtrlMode==U8_MUXWHLCTRL_APPLY) || (u8MuxRRCtrlMode==U8_MUXWHLCTRL_FULLRISE))
	{
		lapctu8WhlCtrlIndex += 2; /*secondary*/
	}

	/*lapctu8WhlCtrlIndex = ((u8MuxFRCtrlMode + (u8MuxFLCtrlMode*2))*10) + (u8MuxRLCtrlMode + (u8MuxRRCtrlMode*2));*/ /* Front: decimal 0~3 / Rear: unit 0~3 */

	lapcts16MuxWheelVolumeState = s16MuxWheelVolumeState; /* Later to be substituted by lapctu8WhlCtrlIndex */

}

static uint8_t LAPCT_u8MuxSetWhlCtrlCmd1Ch(const LAMUXCTRLCMD_t *pWhlActCmd, LAMUXCTRLSTATE_t *pWhlStInfo, LAMUX_WHL_VV_CMD_t *pOutputCmd)
{
	uint16_t u16WhlCmdApplyTime = 0;
	uint8_t  u8WhlCtrlMode = U8_MUXWHLCTRL_NONE;

	if(lau8MultiplexCtrlActive==TRUE)
	{
		if(pWhlActCmd->u1CtrlReq==TRUE)
		{
			u8WhlCtrlMode = U8_MUXWHLCTRL_APPLY;
			pWhlStInfo->u16CmdExecTimeMs = 5; /* Temporary set */
			u16WhlCmdApplyTime = pWhlStInfo->u16CmdExecTimeMs;
		}
		else
		{
			u8WhlCtrlMode = U8_MUXWHLCTRL_HOLD;
			pWhlStInfo->u16CmdExecTimeMs = 5; /* Temporary set */
			u16WhlCmdApplyTime = pWhlStInfo->u16CmdExecTimeMs;
		}
	}
	else
	{
		u8WhlCtrlMode = U8_MUXWHLCTRL_FULLRISE;
		pWhlStInfo->u16CmdExecTimeMs = 0; /* Temporary set */
		u16WhlCmdApplyTime = pWhlStInfo->u16CmdExecTimeMs;
	}

	pOutputCmd->u8WhlCmdDelayTime = 0;

	pOutputCmd->u16WhlCmdExecTime = pOutputCmd->u8WhlCmdDelayTime + u16WhlCmdApplyTime;

	pOutputCmd->u8MuxWhlCtrlMode = u8WhlCtrlMode;

	return u8WhlCtrlMode;
}

static uint8_t LAPCT_u8MuxSetWhlCtrlCmdNCh(const LAMUXCTRLSTATE_t *pCurCmd, LAMUXCTRLSTATE_t *pWhlStInfo, LAMUX_WHL_VV_CMD_t *pOutputCmd)
{
	uint16_t u16WhlCmdApplyTime = 0;
	uint8_t  u8WhlCtrlMode = U8_MUXWHLCTRL_NONE;

	if(pCurCmd->u8id==pWhlStInfo->u8id)
	{
		if(las16MuxCtrlMode==S8_MUX_MODE_PCTRL)
		{
			u8WhlCtrlMode = U8_MUXWHLCTRL_APPLY;
			pWhlStInfo->u16CmdExecTimeMs = pCurCmd->u16CmdExecTimeMs; /* Update Exec Time cmd of Each wheel state struct... is it necessary? */
			u16WhlCmdApplyTime = pCurCmd->u16CmdExecTimeMs;
		}
		else
		{
			u8WhlCtrlMode = U8_MUXWHLCTRL_HOLD;
			pWhlStInfo->u16CmdExecTimeMs = 0;/* is it necessary? */
			u16WhlCmdApplyTime = 0;
		}
	}
	else
	{
		u8WhlCtrlMode = U8_MUXWHLCTRL_HOLD;
		pWhlStInfo->u16CmdExecTimeMs = 0;/* is it necessary? */
		u16WhlCmdApplyTime = 0;
	}

	pOutputCmd->u8WhlCmdDelayTime = 0;

	pOutputCmd->u16WhlCmdExecTime = pOutputCmd->u8WhlCmdDelayTime + u16WhlCmdApplyTime;

	pOutputCmd->u8MuxWhlCtrlMode = u8WhlCtrlMode;

	return u8WhlCtrlMode;
}

static void LAIDB_vDecideEachWhlValveActRatio(void)
{
	if(lau1MuxNChannelCtrlActive==TRUE)
	{
		/* Primary */
		LAPCT_u16CalcMuxWhlVVActRatioNch(&MuxCurCmd, lapcts16MuxPistP    , &laMux_FRCMD);
		LAPCT_u16CalcMuxWhlVVActRatioNch(&MuxCurCmd, lapcts16MuxPistP    , &laMux_RLCMD);

		/* Secondary */
		  #if __P_SENSOR_MEASURE_TYPE== EACH_CIRCUIT_PRESSURE
		LAPCT_u16CalcMuxWhlVVActRatioNch(&MuxCurCmd, lapcts16MuxPistPSecd, &laMux_FLCMD);
		LAPCT_u16CalcMuxWhlVVActRatioNch(&MuxCurCmd, lapcts16MuxPistPSecd, &laMux_RRCMD);
		  #else
		LAPCT_u16CalcMuxWhlVVActRatioNch(&MuxCurCmd, lapcts16MuxPistP, &laMux_FLCMD);
		LAPCT_u16CalcMuxWhlVVActRatioNch(&MuxCurCmd, lapcts16MuxPistP, &laMux_RRCMD);
		  #endif
	}
	else
	{
		/* Primary */
		LAPCT_u16CalcMuxWhlVVActRatio1Ch(&laMux_FRCMD, lapcts16MuxPistP    , &laMux_FRCMD);
		LAPCT_u16CalcMuxWhlVVActRatio1Ch(&laMux_RLCMD, lapcts16MuxPistP    , &laMux_RLCMD);

		/* Secondary */
		  #if __P_SENSOR_MEASURE_TYPE== EACH_CIRCUIT_PRESSURE
		LAPCT_u16CalcMuxWhlVVActRatio1Ch(&laMux_FLCMD, lapcts16MuxPistPSecd, &laMux_FLCMD);
		LAPCT_u16CalcMuxWhlVVActRatio1Ch(&laMux_RRCMD, lapcts16MuxPistPSecd, &laMux_RRCMD);
		  #else
		LAPCT_u16CalcMuxWhlVVActRatio1Ch(&laMux_FLCMD, lapcts16MuxPistP, &laMux_FLCMD);
		LAPCT_u16CalcMuxWhlVVActRatio1Ch(&laMux_RRCMD, lapcts16MuxPistP, &laMux_RRCMD);
		  #endif
	}
}

static void LAPCT_u16CalcMuxWhlVVActRatio1Ch(const LAMUXCTRLCMD_t *pWhlActCmd, int16_t s16CircuitPress, LAMUX_WHL_VV_CMD_t *pMuxWLCMD)
{
	int16_t s16CircPress = 0;
	int16_t s16WheelPress = 0;
	int16_t s16CirctoWhlDelP = 0;
	uint16_t u16WhlVVDutyRatio = U16_MUX_VV_DUTY_RATIO_MIN;

	/* Circuit press calc */
	s16CircPress = s16CircuitPress;

	/* Wheel press calc */
	s16WheelPress = pWhlActCmd->s16EstWhlP;

	/* Delta press calc */
	s16CirctoWhlDelP = (int16_t)(s16CircPress - s16WheelPress);
	s16CirctoWhlDelP = (s16CirctoWhlDelP>S16_PCTRL_PRESS_0_BAR)?s16CirctoWhlDelP:S16_PCTRL_PRESS_0_BAR;

	/* Duty ratio calc */
	if(pMuxWLCMD->u8MuxWhlCtrlMode==U8_MUXWHLCTRL_APPLY)
	{
		u16WhlVVDutyRatio = 0;
		/*u16WhlVVDutyRatio = L_s16IInter6Point(s16CirctoWhlDelP, U16_MUX_VV_DUTY_RATIO_DELP_L0, U16_MUX_VV_DUTY_RATIO_LV0, U16_MUX_VV_DUTY_RATIO_DELP_L1, U16_MUX_VV_DUTY_RATIO_LV1, U16_MUX_VV_DUTY_RATIO_DELP_L2, U16_MUX_VV_DUTY_RATIO_LV2, U16_MUX_VV_DUTY_RATIO_DELP_L3, U16_MUX_VV_DUTY_RATIO_LV3, U16_MUX_VV_DUTY_RATIO_DELP_L4, U16_MUX_VV_DUTY_RATIO_LV4, U16_MUX_VV_DUTY_RATIO_DELP_L5, U16_MUX_VV_DUTY_RATIO_LV5);*/
	}
	else if(pMuxWLCMD->u8MuxWhlCtrlMode==U8_MUXWHLCTRL_HOLD)
	{
		u16WhlVVDutyRatio = U16_MUX_VV_DUTY_RATIO_MAX; /* NO valve full close */
	}
	else if(pMuxWLCMD->u8MuxWhlCtrlMode==U8_MUXWHLCTRL_FULLRISE)
	{
		u16WhlVVDutyRatio = 0; /* NO valve full open*/
	}
	else
	{
		u16WhlVVDutyRatio = 0; /* default */
	}

	pMuxWLCMD->u16MuxWhlVlvActRatio = u16WhlVVDutyRatio;
}

static void LAPCT_u16CalcMuxWhlVVActRatioNch(const LAMUXCTRLSTATE_t *pCurCmd, int16_t s16CircuitPress, LAMUX_WHL_VV_CMD_t *pMuxWLCMD)
{
	int16_t s16CircPress = 0;
	int16_t s16WheelPress = 0;
	int16_t s16CirctoWhlDelP = 0;
	uint16_t u16WhlVVDutyRatio = U16_MUX_VV_DUTY_RATIO_MIN;

	/* Circuit press calc */
	s16CircPress = s16CircuitPress;

	/* Wheel press calc */
	s16WheelPress = pCurCmd->s16EstWhlP;

	/* Delta press calc */
	s16CirctoWhlDelP = (int16_t)(s16CircPress - s16WheelPress);
	s16CirctoWhlDelP = (s16CirctoWhlDelP>S16_PCTRL_PRESS_0_BAR)?s16CirctoWhlDelP:S16_PCTRL_PRESS_0_BAR;

	/* Duty ratio calc */
	if(pMuxWLCMD->u8MuxWhlCtrlMode==U8_MUXWHLCTRL_APPLY)
	{
		if(MuxCurCmd.u8State==U8_MUX_ST_FIRST_EXECUTED)
		{
			u16WhlVVDutyRatio = L_s16IInter6Point(s16CirctoWhlDelP, U16_MUX_VV_DUTY_RATIO_DELP_L0, U16_MUX_VV_DUTY_RATIO_LV0, U16_MUX_VV_DUTY_RATIO_DELP_L1, U16_MUX_VV_DUTY_RATIO_LV1, U16_MUX_VV_DUTY_RATIO_DELP_L2, U16_MUX_VV_DUTY_RATIO_LV2, U16_MUX_VV_DUTY_RATIO_DELP_L3, U16_MUX_VV_DUTY_RATIO_LV3, U16_MUX_VV_DUTY_RATIO_DELP_L4, U16_MUX_VV_DUTY_RATIO_LV4, U16_MUX_VV_DUTY_RATIO_DELP_L5, U16_MUX_VV_DUTY_RATIO_LV5);
		}
		else
		{
			u16WhlVVDutyRatio = pMuxWLCMD->u16MuxWhlVlvActRatio; /* maintain initial ratio*/
		}
	}
	else if(pMuxWLCMD->u8MuxWhlCtrlMode==U8_MUXWHLCTRL_HOLD)
	{
		u16WhlVVDutyRatio = U16_MUX_VV_DUTY_RATIO_MAX; /* NO valve full close */
	}
	else if(pMuxWLCMD->u8MuxWhlCtrlMode==U8_MUXWHLCTRL_FULLRISE)
	{
		u16WhlVVDutyRatio = 0; /* NO valve full open*/
	}
	else
	{
		u16WhlVVDutyRatio = 0; /* default */
	}

	pMuxWLCMD->u16MuxWhlVlvActRatio = u16WhlVVDutyRatio;
}

static void LAIDB_vDecideMuxMtrTarget(void)
{
	int16_t s16MuxTarPRaw   = laidbs16MuxTarPress;
	int16_t s16MuxDeltaTarPLim = S16_PCTRL_PRESS_3_BAR;
	int16_t s16MuxTarPRateRaw = lapcts16MuxPwrPistTarPRateInp;
	int16_t s16MuxTarDelPressRaw = S16_PCTRL_PRESS_0_BAR;

	if(lau1MuxNChannelCtrlActive==TRUE)
	{
		if(lapctu1NewReqDetected==1)
		{
			laidbs16MuxTarPressOld = MuxCurCmd.s16EstWhlP;
			s16MuxTarDelPressRaw = MuxCurCmd.s16WhlTargetDeltaP;
		}
		else
		{
			/*laidbs16MuxTarPressOld = MuxCurCmd.s16EstWhlP;*/
			s16MuxTarDelPressRaw = S16_PCTRL_PRESS_0_BAR;
		}

		if(lau1MuxPCtrlStrategy==U8_MUX_PCTRL_DELTAP_BASE)
		{
			if(las16MuxCtrlMode == S8_MUX_MODE_PCTRL)
			{
				if(MuxCurCmd.u8State==U8_MUX_ST_FIRST_EXECUTED)
				{
					s16MuxTarPRaw    = lapcts16HighestWhlTarP;
					/*s16MuxTarPRaw = MuxCurCmd.s16EstWhlP + MuxCurCmd.s16WhlTargetDeltaP;*/
					/*s16MuxDeltaTarPLim = S16_MAX_PCTRL_RATE_BPS;*/
				}
				else
				{
					s16MuxTarPRaw = laidbs16MuxTarPress; /* hold */
					/*s16MuxDeltaTarPLim = MuxCurCmd.s16WhlTargetPRate/U16_RESOLCHG_BARPSEC_TO_SCAN;*/ /* resol change from bar/sec to 0.01bar/scan */
				}
			}
			else if(las16MuxCtrlMode == S8_MUX_MODE_PCTRL_TRANSITION)
			{
				s16MuxTarPRaw = laidbs16MuxTarPress; /* hold */
			}
			else
			{
				s16MuxTarPRaw = laidbs16MuxTarPress; /* hold */
			}
			s16MuxTarPRateRaw = MuxCurCmd.s16WhlTargetPRate;
		}
	    else
	    {
	    	if(las16MuxCtrlMode == S8_MUX_MODE_STROKE_RECOVERY)
	    	{
	    		s16MuxTarPRaw = laidbs16MuxTarPress; /* hold */
	    		s16MuxTarPRateRaw = S16_P_RATE_0_BPS;
	    		s16MuxTarDelPressRaw = S16_PCTRL_PRESS_0_BAR;
	    	}
	    	else if(las16MuxCtrlMode == S8_MUX_MODE_PCTRL)
			{
				s16MuxTarPRaw    = lapcts16HighestWhlTarP;
				/*s16MuxTarPRaw = MuxCurCmd.s16WhlTargetP;*/ /*resol change 0.1 -> 0.01bar*/
				s16MuxTarPRateRaw = MuxCurCmd.s16WhlTargetPRate;
				s16MuxTarDelPressRaw = MuxCurCmd.s16WhlTargetDeltaP;
			}
			else
			{
				s16MuxTarPRaw    = lapcts16HighestWhlTarP;
				s16MuxTarPRateRaw = MuxCurCmd.s16WhlTargetPRate;
				/*s16MuxTarPRateRaw = LCIDB_vCalcStdTargetPressRate(lapcts16MuxPwrPistonTarPInput,&stMuxTarPRateInfo);*/
				s16MuxTarDelPressRaw = MuxCurCmd.s16WhlTargetDeltaP;
			}
		}

		laidbs16MuxTarPress = s16MuxTarPRaw;
		lapcts16MuxTarDelPress = s16MuxTarDelPressRaw;
		laidbs16MuxTarPressRate = s16MuxTarPRateRaw;
		/*laidbs16MuxTarPress = L_s16LimitDiff(s16MuxTarPRaw,laidbs16MuxTarPress,S16_PCTRL_PRESS_100_BARs16MuxDeltaTarPLim,S16_PCTRL_PRESS_100_BAR);  backward: max rate */
	}
	else
	{
		laidbs16MuxTarPress = lapcts16HighestWhlTarP;
		lapcts16MuxTarDelPress = S16_PCTRL_PRESS_0_BAR;
		laidbs16MuxTarPressRate = S16_P_RATE_0_BPS; /* bar/sec */
		laidbs16MuxTarPressOld = S16_PCTRL_PRESS_0_BAR;
	}

}

static int16_t LCIDB_vCalcStdTargetPressRate(int16_t s16Press, LAMUXPRESSRATECALC_t *pPrateInfo)
{
	uint8_t u8TGPressBuffCntBf2scan = 0;
	int16_t s16FilterInput = 0;
	int32_t s32FilterInputTemp = 0;

	pPrateInfo->s16PressBuff[pPrateInfo->s16BuffCnt] = s16Press;

	pPrateInfo->s16BuffCnt = pPrateInfo->s16BuffCnt + 1;
    if(pPrateInfo->s16BuffCnt>=5)
    {
    	pPrateInfo->s16BuffCnt = 0;
    }
    else
    {
        ;
    }

	/**********************************************************/
	/* Target Press rate (1 = 0.01bar/10ms = 1bar/s)          */
	/**********************************************************/
    if(pPrateInfo->s16BuffCnt>=3)
    {
    	u8TGPressBuffCntBf2scan = pPrateInfo->s16BuffCnt - 3;
    }
    else
    {
    	u8TGPressBuffCntBf2scan = pPrateInfo->s16BuffCnt + 2;
    }

	s32FilterInputTemp = ((int32_t)s16Press - (int32_t)pPrateInfo->s16PressBuff[u8TGPressBuffCntBf2scan]); /* target press variation for 2scan. changed algorithm for simplicity in Feb. 2015 -> already checked both algorithms*/

	if(s32FilterInputTemp >= ASW_S16_MAX)
	{
		s16FilterInput = ASW_S16_MAX;
	}
	else if (s32FilterInputTemp <= ASW_S16_MIN)
	{
		s16FilterInput = ASW_S16_MIN;
	}
	else
	{
		s16FilterInput = (int16_t)s32FilterInputTemp;
	}
	pPrateInfo->s16PressRate10ms =(L_s16Lpf1Int(s16FilterInput, pPrateInfo->s16PressRate10ms, U8_LPF_7HZ_128G)); /* 1 bar/sec*/

	/**********************************************************/
	/* Target Press rate (1 = 0.01bar/5ms *2 = 1bar/s)         */
	/**********************************************************/
    if(pPrateInfo->s16BuffCnt>=2)
    {
    	u8TGPressBuffCntBf2scan = pPrateInfo->s16BuffCnt - 2;
    }
    else
    {
    	u8TGPressBuffCntBf2scan = pPrateInfo->s16BuffCnt + 3;
    }

	s32FilterInputTemp = (int32_t)s16Press - (int32_t)pPrateInfo->s16PressBuff[u8TGPressBuffCntBf2scan]; /* target press variation for 1 scan */


	if(s32FilterInputTemp >= ASW_S16_MAX)
	{
		s16FilterInput = ASW_S16_MAX;
	}
	else if (s32FilterInputTemp <= ASW_S16_MIN)
	{
		s16FilterInput = ASW_S16_MIN;
	}
	else
	{
		s16FilterInput = (int16_t)s32FilterInputTemp;
	}

	pPrateInfo->s16PressRate5ms = s16FilterInput * 2;          /* not filtered */

	return pPrateInfo->s16PressRate10ms;
}

static void LAPCT_vDecidePCtrlActive(void)
{
	if((lapcts16MuxPwrPistonTarP > S16_PCTRL_PRESS_0_5_BAR) || (lau1MuxNChannelCtrlActive==TRUE))
	{
		lapctu1PCtrlActFlg = TRUE;
	}
	else
	{
		lapctu1PCtrlActFlg = FALSE;
	}
}

static void LAPCT_vSelectReqdPCtrlBoostMod(void)
{
	if(lapctu1AbsMtrTqCtrlActive==TRUE)
	{
		/* Motor torque control needed */
		PressCTRLLocalBus.s16ReqdPCtrlBoostMod = S8_BOOST_MODE_REQ_L0;
	}
	else if(lau1MuxNChannelCtrlActive==TRUE)
	{
		/* Motor control: Only feed-forward */
		PressCTRLLocalBus.s16ReqdPCtrlBoostMod = S8_BOOST_MODE_REQ_L9;
	}
	else
	{
		if((lapctu1InAbsCtrlActive==TRUE) && (lapctu1InAbsCtrlFadeOut==TRUE))
		{
			PressCTRLLocalBus.s16ReqdPCtrlBoostMod = S8_BOOST_MODE_REQ_L5;
		}
		else
		{
			/* Arb output bypass */
			PressCTRLLocalBus.s16ReqdPCtrlBoostMod = Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.ReqdPCtrlBoostMod;
		}
	}


}


#define PCT_5MSCTRL_STOP_SEC_CODE
#include "Pct_MemMap.h"

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */

