/**
 * @defgroup LCIDB_DetValveControlSate LCIDB_DetValveControlSate
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LCIDB_DetValveControlSate.h
 * @brief       Local Header File
 * @date        20YY. MM. DD.
 ******************************************************************************/
 
#ifndef LAPCT_DECIDESTKRCVRSTATE_H_
#define LAPCT_DECIDESTKRCVRSTATE_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/


/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
#define __DECIDE_STABILIZE_REF_PRESS 		DISABLE
#define __NEED_TO_STABN2_STATE				ENABLE

#define S16_POSITION_10_PERCENT_RUN			(450)  // 5.5mm  /*10 percent of 55mm*/
#define S16_POSITION_20_PERCENT_RUN			(((S16_POSITION_10_PERCENT_RUN)*40)/20)
#define S16_POSITION_30_PERCENT_RUN			(((S16_POSITION_10_PERCENT_RUN)*60)/20)
#define S16_POSITION_40_PERCENT_RUN			(((S16_POSITION_10_PERCENT_RUN)*80)/20)
#define S16_POSITION_50_PERCENT_RUN			(((S16_POSITION_10_PERCENT_RUN)*100)/20)
#define S16_POSITION_60_PERCENT_RUN			(((S16_POSITION_10_PERCENT_RUN)*120)/20)
#define S16_POSITION_70_PERCENT_RUN			(((S16_POSITION_10_PERCENT_RUN)*140)/20)
#define S16_POSITION_80_PERCENT_RUN			(((S16_POSITION_10_PERCENT_RUN)*160)/20)
#define S16_POSITION_90_PERCENT_RUN			(((S16_POSITION_10_PERCENT_RUN)*180)/20)
#define S16_POSITION_95_PERCENT_RUN			(((S16_POSITION_10_PERCENT_RUN)*190)/20)

#define S16_EMGY_STRK_RCVR_ENT_POSI 		S16_POSITION_90_PERCENT_RUN
#define S16_EMGY_STRK_RCVR_EXIT_POSI		S16_POSITION_10_PERCENT_RUN

#define S16_RECOVERY_MIN_STROKE				(600)

#if __DECIDE_STABILIZE_REF_PRESS == ENABLE
#define S16_STRKRCVR_STABN_PRESS_RATIO		7
#define S16_STRKRCVR_STABN_DIFF_P_TH		(S16_MPRESS_2_BAR)
 #endif

#define S16_RECOVERY_STABN1_ACT_MAX_TIME	(U8_T_30_MS)
#define S16_RECOVERY_STABN2_ACT_MAX_TIME	(U8_T_10_MS)
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void LAPCT_vCallStkRcvrMain(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* LSTROKERECOVERY_H_ */
/** @} */
