/*
 * LAIDB_LibMuxFIFO.h
 *
 *  Created on: 2015. 1. 9.
 *      Author: dolores.kim
 */

#ifndef LAIDB_LIBMUXFIFO_H_
#define LAIDB_LIBMUXFIFO_H_

#include "./LAIDB_CallMultiplexControl.h"

#define U8_MUX_FIFO_BUFFER_SIZE	6

struct fifo_buffer_t {
         unsigned u32id;
         unsigned u32head;         /* first byte of data */
         unsigned u32tail;         /* last byte of data */
         unsigned char u8buffer[U8_MUX_FIFO_BUFFER_SIZE]; /* block of memory or array of data */
         unsigned u32BufferLen;  			/* length of the data */
};
typedef struct fifo_buffer_t MUX_FIFO_BUFFER;

extern unsigned LAIDB_u32MuxFIFO_Count(MUX_FIFO_BUFFER  *b);
extern unsigned char LAIDB_u8MuxFIFO_Full(MUX_FIFO_BUFFER *b);
extern unsigned char LAIDB_u8MuxFIFO_Empty( MUX_FIFO_BUFFER *b);
extern unsigned char LAIDB_u8MuxFIFO_Look(MUX_FIFO_BUFFER const *b);
extern unsigned char LAIDB_u8MuxFIFO_Get(MUX_FIFO_BUFFER *b);
extern unsigned char LAIDB_u8MuxFIFO_Put(MUX_FIFO_BUFFER *b, unsigned char data_byte);
extern unsigned char LAIDB_u8MuxFIFO_Init(MUX_FIFO_BUFFER *b,unsigned char u8buffer[U8_MUX_FIFO_BUFFER_SIZE], unsigned buffer_len);
extern uint8_t LAIDB_u8MuxRemoveFromBuffer(MUX_FIFO_BUFFER *pBuff, uint8_t u8DataToRemove);
extern uint8_t LAIDB_u8MuxInsertIntoBuffer(MUX_FIFO_BUFFER *pBuff, uint8_t u8DataToInsert);

extern MUX_FIFO_BUFFER	MuxFIFO;
extern unsigned char InitAdr[U8_MUX_FIFO_BUFFER_SIZE];
/*
extern MUX_FIFO_BUFFER TempBuffLogRem;
extern MUX_FIFO_BUFFER TempBuffLogIns;
*/


/* note: 버퍼 길이는 2의 배수여야 한다. */

#endif /* LAIDB_LIBMUXFIFO_H_ */
