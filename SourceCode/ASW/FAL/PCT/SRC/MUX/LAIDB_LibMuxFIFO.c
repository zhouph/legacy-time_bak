/*
 * LAIDB_LibMuxFIFO.c
 *
 *  Created on: 2015. 1. 9.
 *      Author: dolores.kim
 */
#include "LAIDB_LibMuxFIFO.h"

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"

unsigned char InitAdr[U8_MUX_FIFO_BUFFER_SIZE];
unsigned char InitAdrIns[U8_MUX_FIFO_BUFFER_SIZE];
MUX_FIFO_BUFFER	MuxFIFO = {0,0,0,0,0};
#if defined (__MUX_DEBUG)
MUX_FIFO_BUFFER TempBuffLogIns = {0,0,0,0,0};
#endif


#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
#define PCT_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PCT_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
#define PCT_5MSCTRL_START_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/** Variable Section (32BIT)**/


#define PCT_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Pct_MemMap.h"


/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

#define PCT_5MSCTRL_START_SEC_CODE
#include "Pct_MemMap.h"

unsigned LAIDB_u32MuxFIFO_Count(MUX_FIFO_BUFFER  *b)
{
	return (b->u32head - b->u32tail);
}

unsigned char LAIDB_u8MuxFIFO_Full(MUX_FIFO_BUFFER *b)
{
	return (b ? (LAIDB_u32MuxFIFO_Count(b) == b->u32BufferLen) : 1);
}

unsigned char LAIDB_u8MuxFIFO_Empty( MUX_FIFO_BUFFER *b)
{
	if(LAIDB_u32MuxFIFO_Count(b)==0)	return 1;
	else					return 0;
}

unsigned char LAIDB_u8MuxFIFO_Look(MUX_FIFO_BUFFER const *b)
{

	if(b)
	{
		return (b->u8buffer[b->u32tail % b->u32BufferLen]);
	}
	
	return 0;
}

unsigned char LAIDB_u8MuxFIFO_Get(MUX_FIFO_BUFFER *b)
{
    unsigned char data_byte = 0;

    if (!LAIDB_u8MuxFIFO_Empty(b))
    {
    	data_byte = b->u8buffer[b->u32tail % b->u32BufferLen];
        b->u32tail++;
    }
    return data_byte;
}

unsigned char LAIDB_u8MuxFIFO_Put(MUX_FIFO_BUFFER *b, unsigned char data_byte)
{
    unsigned char status = 0;        /* return value */

	if(b)
	{
		/* limit the ring to prevent overwriting */
		if (!LAIDB_u8MuxFIFO_Full(b))
		{
			b->u8buffer[b->u32head % b->u32BufferLen] = data_byte;
			b->u32head++;
			status = data_byte;
		}
	}

    return data_byte;
}

unsigned char LAIDB_u8MuxFIFO_Init(MUX_FIFO_BUFFER *b,unsigned char u8buffer[U8_MUX_FIFO_BUFFER_SIZE], unsigned u32BufferLen)
{
	unsigned char err;
	uint8_t u8i;
	err = 0;

	if (b){
		b->u32head = 0;
		b->u32tail = 0;

		for(u8i=0; u8i<u32BufferLen; u8i++)
		{
			b->u8buffer[u8i] = 0;
		}

		b->u32BufferLen = u32BufferLen;
	}
	else
	{
		err=1;
	}
	return err;

}


/*******************************************************************************
* FUNCTION NAME:        LAIDB_u32MuxSearchInBuffer
* CALLED BY:            -
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         Buffer index
* Description:          Search Cmd in buffer
********************************************************************************/
uint32_t LAIDB_u32MuxSearchInBuffer(MUX_FIFO_BUFFER *pBuff, uint8_t u8DataToSearch)
{
	uint32_t u32SearchBuff = 0;
	uint32_t u32BuffIndexFound = 0;

	for(u32SearchBuff=pBuff->u32tail; u32SearchBuff<pBuff->u32head; u32SearchBuff++) /* max 6 times repeating */
	{
		if(pBuff->u8buffer[u32SearchBuff % pBuff->u32BufferLen] == u8DataToSearch)
		{
			u32BuffIndexFound = u32SearchBuff;
		}
	}

	return u32BuffIndexFound;
}

/*******************************************************************************
* FUNCTION NAME:        LAIDB_u8MuxRemoveFromBuffer
* CALLED BY:            -
* Preconditions:        none
* PARAMETER:            Buffer structure, Data to remove
* RETURN VALUE:
* Description:          Remove a Cmd Id from buffer. search Cmd and rearrange
********************************************************************************/
uint8_t LAIDB_u8MuxRemoveFromBuffer(MUX_FIFO_BUFFER *pBuff, uint8_t u8DataToRemove)
{
	uint8_t u8Removed = 0;
	uint32_t u32SearchBuff;
	uint32_t u32RearrangedBuff;
	uint32_t u32BufferIndexToRemove = 0;
	MUX_FIFO_BUFFER TempBuff_t = (*pBuff);

	if(!LAIDB_u8MuxFIFO_Empty(pBuff))
	{
		
		u32RearrangedBuff = pBuff->u32tail;
		
		/* Search Buffer with data */
		for(u32SearchBuff=pBuff->u32tail; u32SearchBuff<pBuff->u32head; u32SearchBuff++) /* max 6 times repeating */
		{
			if(pBuff->u8buffer[u32SearchBuff % pBuff->u32BufferLen] == u8DataToRemove)
			{
				;
			}
			else
			{
				TempBuff_t.u8buffer[u32RearrangedBuff % pBuff->u32BufferLen] = pBuff->u8buffer[u32SearchBuff % pBuff->u32BufferLen];
				u32RearrangedBuff = u32RearrangedBuff + 1;
			}
		}
		
		/* Update buffer */
		if(u32SearchBuff > u32RearrangedBuff)
		{
			TempBuff_t.u32head = TempBuff_t.u32head - (u32SearchBuff - u32RearrangedBuff);
			(*pBuff) = TempBuff_t;
			u8Removed = u8DataToRemove;
		}
#if DISABLE		
		u32BufferIndexToRemove = LAIDB_u32MuxSearchInBuffer(pBuff, u8DataToRemove);

		/* Rearrange buffer */
		if(u32BufferIndexToRemove > 0)
		{
			for(u32SearchBuff=u32BufferIndexToRemove; u32SearchBuff < pBuff->u32head; u32SearchBuff++)
			{
				TempBuff_t.u8buffer[u32SearchBuff % pBuff->u32BufferLen] = pBuff->u8buffer[ (u32SearchBuff+1) % pBuff->u32BufferLen ];
			}
			TempBuff_t.u32head = pBuff->u32head - 1;

			(*pBuff) = TempBuff_t;
			u8Removed = u8DataToRemove;
		}
#endif
	}

	return u8Removed;
}


/*******************************************************************************
* FUNCTION NAME:        LAIDB_u8MuxInsertIntoBuffer
* CALLED BY:            -
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Insert a Cmd in buffer, to the current index
********************************************************************************/
uint8_t LAIDB_u8MuxInsertIntoBuffer(MUX_FIFO_BUFFER *pBuff, uint8_t u8DataToInsert)
{
	uint8_t u8CmdInserted = 0;
	uint32_t u32InsertBuff;
	MUX_FIFO_BUFFER TempBuff_t = (*pBuff); /* use a temporary buffer structure */

#if DISABLE
	if(!LAIDB_u8MuxFIFO_Full(pBuff))
	{
		/* Insert into first buffer */
		TempBuff_t.u8buffer[(pBuff->u32tail-1) % pBuff->u32BufferLen] = u8DataToInsert;
		TempBuff_t.u32tail = pBuff->u32tail - 1;

		(*pBuff) = TempBuff_t; /* Copy elements to input buffer */
		u8CmdInserted = u8DataToInsert;
	}
#endif

	if(pBuff->u32tail > 0)
	{
		/* Insert into first buffer */
		TempBuff_t.u8buffer[(pBuff->u32tail-1) % pBuff->u32BufferLen] = u8DataToInsert;
		if(LAIDB_u8MuxFIFO_Full(pBuff)) 
		{
			TempBuff_t.u32head = pBuff->u32head - 1;
		}
		TempBuff_t.u32tail = pBuff->u32tail - 1;
	}
	else
	{
		for(u32InsertBuff=pBuff->u32tail; u32InsertBuff<pBuff->u32head; u32InsertBuff++) /* max 6 times repeating */
		{
			TempBuff_t.u8buffer[(u32InsertBuff+1) % pBuff->u32BufferLen] = pBuff->u8buffer[u32InsertBuff % pBuff->u32BufferLen];
		}
		TempBuff_t.u8buffer[pBuff->u32tail] = u8DataToInsert;		
		if(!LAIDB_u8MuxFIFO_Full(pBuff)) 
		{
			TempBuff_t.u32head = pBuff->u32head + 1;
		}
	}
	
	(*pBuff) = TempBuff_t; /* Copy elements to input buffer */
	u8CmdInserted = u8DataToInsert;

  #if defined (__MUX_DEBUG)
	TempBuffLogIns  = TempBuff_t;
  #endif

	return u8CmdInserted;
}

#define PCT_5MSCTRL_STOP_SEC_CODE
#include "Pct_MemMap.h"

