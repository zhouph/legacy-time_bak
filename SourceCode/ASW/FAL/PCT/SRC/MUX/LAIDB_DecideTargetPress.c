/**
 * @defgroup LAIDB_DecideTargetPress LAIDB_DecideTargetPress
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LAIDB_DecideTargetPress.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "../LAIDB_CallMultiplexControl.h"
#include "LAIDB_DecideTargetPress.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define S8_ALL_WHEEL                  (4)
#define S8_SELECT_WHEEL               (1)

#define S16_CIR_CTRL_ACTIVE           (1)
#define S16_CIR_CTRL_NONACTIVE        (0)

#define U16_FRONT_1WL_CNT             (1)
#define U16_REAR_1WL_CNT              (4)

#define S16_FIRST_WHEEL               (0)
#define S16_SECOND_WHEEL              (1)
#define S16_BOTH_WHEEL                (2)


/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

typedef struct
{
	int16_t s16FLWheelTargetP;
	int16_t s16FRWheelTargetP;
	int16_t s16RLWheelTargetP;
	int16_t s16RRWheelTargetP ;
	int16_t s16TargetPress;
}IdbTarP_t;



/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_CONST_UNSPECIFIED
#include "Pct_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PCT_5MSCTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Pct_MemMap.h"

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


FAL_STATIC int16_t ldidbs16TCSF2WhlTargetP;

FAL_STATIC int16_t ldidbu8WheelVolumeCtrlMode ;

FAL_STATIC uint8_t ldidbu8AllWheelControl;
FAL_STATIC uint8_t ldidbu8OneWheelControl;
FAL_STATIC uint8_t ldidbu8MultiWheelControl;
FAL_STATIC uint8_t ldidbu8SelectWheelControl;

FAL_STATIC int16_t ldidbs16DeltaFLWhlTP;
FAL_STATIC int16_t ldidbs16DeltaFRWhlTP;
FAL_STATIC int16_t ldidbs16DeltaRLWhlTP;
FAL_STATIC int16_t ldidbs16DeltaRRWhlTP;
FAL_STATIC int16_t ldahbs16DelTargetPressX10;
FAL_STATIC int16_t ldidbs16DeltaTCSF2WhlTP;

/*FAL_STATIC*/ int16_t ldidbs16RepresentWheelTargetP;
FAL_STATIC int16_t ldidbs16RepresentDelWhlTargetP;
FAL_STATIC int16_t ldidbs16calRepresentDelWhlTP;
FAL_STATIC int16_t ldidbs16ResetWheelVolume;




FAL_STATIC int16_t ldidbs16PriCircuitControl;
FAL_STATIC int16_t ldidbs16SecCircuitControl;
FAL_STATIC int16_t ldidbs16InABSCtrlActive;
FAL_STATIC int16_t ldidbs16TCSFront2WhlCtrlFlg;

FAL_STATIC int16_t ldidbs16PriCircuitVVCloseReq; /* not needed */
FAL_STATIC int16_t ldidbs16SecCircuitVVCloseReq; /* not needed */

FAL_STATIC uint16_t ldidbu16WheelVolumeState;
FAL_STATIC uint16_t ldidbu16WheelVolumeStateOld;
FAL_STATIC const uint16_t ldahbu16WheelVolumeArray[3] = {U16_SEL_F1WHEEL, U16_SEL_R1WHEEL, U16_SEL_F1R1WHEEL};

FAL_STATIC int16_t ldidbs16ABSBoostTargetPress;
FAL_STATIC int16_t ldidbs16ReqChangeTPFromABSFlg;
FAL_STATIC int16_t ldidbs16InABSBoostTargetP;

FAL_STATIC int16_t laidbs8PCtrlState;

FAL_STATIC IdbTarP_t GetIDBTarP;

FAL_STATIC int16_t ldidbs16IdenticalCircuitFlg;

#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"


/* ---------------------------------------------------*/
#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_START_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/** Variable Section (32BIT)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
#define PCT_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PCT_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
#define PCT_5MSCTRL_START_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/** Variable Section (32BIT)**/


#define PCT_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Pct_MemMap.h"

/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_CODE
#include "Pct_MemMap.h"

static void LDIDB_vMainControlInput(void);
static void LDIDB_vDetPControlMode(void);
static void LDIDB_vDetAllWheelControl(void);
static void LDIDB_vCalDeltaWhlTargetPress(void);
static void LDIDB_vDetCtrlCircuit(void);
static void LDIDB_vReqCircuitValve(void);  /* not needed */
static void LDIDB_vDetWheelTarget(void);
static void LDIDB_vDetWheelVolumeChange(void);
static void LDIDB_vEstABSTargetPress(void);

static uint16_t LDIDB_u16CntActiveWheelVolume(int16_t FL_TargetP, int16_t FR_TargetP,  int16_t RL_TargetP,  int16_t RR_TargetP);

static int16_t LDIDB_s16SelectOneWheelControl(int16_t s161stWheelValue, int16_t s162ndWheelValue, int16_t s16WheelSelOld);
static int16_t LDIDB_s16SelectMultiWheelControl(int16_t s161stWheelTP, int16_t s162ndWheelTP, int16_t s161stDeltaWheelTP, int16_t s162ndDeltalWheelTP, int16_t s16WheelSelOld);
static int16_t LDIDB_s16SelWheelTP(int16_t s16SelWheelVolume, int16_t s161stWheelTP, int16_t s162ndWheelTP);
static int16_t LDIDB_s16SelDelWheelTP(int16_t s16SelWheelVolume, int16_t s161stDeltaWheelTP, int16_t s162ndDeltalWheelTP);
/*static uint16_t LDIDB_u16MaintainVolumeState(uint16_t u16VolumeState, uint16_t u16VolumeStateOld, int16_t s16CtrlTargetP);*/

static int16_t LDIDB_s16SelectMaxValue(int16_t s16Xinput, int16_t sYinput);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void LAIDB_vDecideFinalTargetPress(void)
{
	LDIDB_vMainControlInput();

	LDIDB_vDetPControlMode();
	
	LDIDB_vEstABSTargetPress();

	if((ldidbs16InABSCtrlActive == TRUE) && (lapctu8AbsMtrCtrlMode == U8_MUX_ABS_MCC_TQ_MODE)) /* ABS torque mode */
	{
		ldidbs16RepresentWheelTargetP =  (int16_t) ldidbs16ABSBoostTargetPress;
	}
	else
	{
		ldidbs16RepresentWheelTargetP =  (int16_t) ldidbs16RepresentWheelTargetP;
	}

	PressCTRLLocalBus.s16MuxPwrPistonTarP  = ldidbs16RepresentWheelTargetP;
	PressCTRLLocalBus.s16MuxPwrPistonTarDP = ldidbs16calRepresentDelWhlTP;
	PressCTRLLocalBus.u16MuxWheelVolumeState = ldidbu16WheelVolumeState;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

/*******************************************************************************
* FUNCTION NAME:        LDIDB_vMainControlInput
* CALLED BY:            LD_vCallMainControlIDB
* Preconditions:        none
* PARAMETER:
* RETURN VALUE:
* Description:          INPUT Paramter
********************************************************************************/
static void LDIDB_vMainControlInput(void)
{
	int16_t PriTarP = 0;
	int16_t SecTarP = 0;

	GetIDBTarP.s16FLWheelTargetP = (int16_t)Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlFrntLe ;
	GetIDBTarP.s16FRWheelTargetP = (int16_t)Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlFrntRi;
	GetIDBTarP.s16RLWheelTargetP = (int16_t)Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlReLe ;
	GetIDBTarP.s16RRWheelTargetP = (int16_t)Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlReRi ;
	GetIDBTarP.s16TargetPress = (int16_t)lapcts16MuxPwrPistonTarPInput;

	ldidbs16InABSCtrlActive = (int16_t)lapctu1InAbsCtrlActive;

	ldidbs16TCSF2WhlTargetP = (int16_t)L_s32LimitMinMax(((int32_t)(Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlFrntLe  + Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlFrntRi)/2), S16_MIN, S16_MAX);

/*	laidbs8PCtrlState = (uint8_t)ARBbus.PCtrlSt;*/

/*
	ldidbs16TCSFront2WhlCtrlFlg = liu1SymSpinBrkCtl;
*/
	ldidbs16TCSFront2WhlCtrlFlg =   (int16_t)Pct_5msCtrlBus.Pct_5msCtrlTcsCtrlInfo.BothDrvgWhlBrkCtlReqFlg;
	ldidbs16ReqChangeTPFromABSFlg = (int16_t)Pct_5msCtrlBus.Pct_5msCtrlAbsCtrlInfo.AbsDesTarPReqFlg;
	ldidbs16InABSBoostTargetP =     (int16_t)Pct_5msCtrlBus.Pct_5msCtrlAbsCtrlInfo.AbsDesTarP;

}

static void LDIDB_vDetPControlMode(void)
{
	LDIDB_vCalDeltaWhlTargetPress();
	LDIDB_vDetAllWheelControl();

    switch(ldidbu8WheelVolumeCtrlMode)
    {
    	case S8_ALL_WHEEL: 
    		
    		if(ldidbu8SelectWheelControl == TRUE)
        	{
        		/* Need to develop cotrol volume transition */
        		ldidbu8WheelVolumeCtrlMode = S8_SELECT_WHEEL;

        	}
        	else{ }

    		
    	    break;

        case S8_SELECT_WHEEL:
        	
        	if(ldidbu8AllWheelControl == TRUE)
        	{
        		/* Need to develop cotrol volume transition */
        		ldidbu8WheelVolumeCtrlMode = S8_ALL_WHEEL;
        	}
        	else{ }

    	    break;

        default: 
        	ldidbu8WheelVolumeCtrlMode = S8_ALL_WHEEL;
       	    break;
    }

	LDIDB_vDetCtrlCircuit();
	LDIDB_vReqCircuitValve();  /* not needed */
    LDIDB_vDetWheelTarget();
    LDIDB_vDetWheelVolumeChange();
}


static void LDIDB_vCalDeltaWhlTargetPress(void)
{	
	static int16_t ldidbs16FLWheelTargetPOld = 0;
	static int16_t ldidbs16FRWheelTargetPOld = 0;
	static int16_t ldidbs16RLWheelTargetPOld = 0;
	static int16_t ldidbs16RRWheelTargetPOld = 0;
	static int16_t ldahbs16TargetPressX10Old = 0;
	static int16_t ldidbs16TCSF2WhlTargetPOld = 0;


	
	ldidbs16DeltaFLWhlTP      = GetIDBTarP.s16FLWheelTargetP - ldidbs16FLWheelTargetPOld;  /* later to be deleted */
	ldidbs16DeltaFRWhlTP      = GetIDBTarP.s16FRWheelTargetP - ldidbs16FRWheelTargetPOld;	/* later to be deleted */
	ldidbs16DeltaRLWhlTP      = GetIDBTarP.s16RLWheelTargetP - ldidbs16RLWheelTargetPOld;	/* later to be deleted */
	ldidbs16DeltaRRWhlTP      = GetIDBTarP.s16RRWheelTargetP - ldidbs16RRWheelTargetPOld;	/* later to be deleted */

	ldahbs16DelTargetPressX10 = GetIDBTarP.s16TargetPress - ldahbs16TargetPressX10Old;

	ldidbs16DeltaTCSF2WhlTP = ldidbs16TCSF2WhlTargetP - ldidbs16TCSF2WhlTargetPOld;

    /* Need to consider non-choosen wheel */
	ldidbs16FLWheelTargetPOld = GetIDBTarP.s16FLWheelTargetP ;
    ldidbs16FRWheelTargetPOld = GetIDBTarP.s16FRWheelTargetP ;
    ldidbs16RLWheelTargetPOld = GetIDBTarP.s16RLWheelTargetP ;
    ldidbs16RRWheelTargetPOld = GetIDBTarP.s16RRWheelTargetP ;
    ldahbs16TargetPressX10Old = GetIDBTarP.s16TargetPress ;
    ldidbs16TCSF2WhlTargetPOld = ldidbs16TCSF2WhlTargetP;
}

static void LDIDB_vDetAllWheelControl(void)
{
	int16_t s16PrimaryCheck = 0;
	int16_t s16SecondaryCheck = 0;
	int16_t s16FrontWheelCheck = 0;
	int16_t s16AllWheelCheck = 0;


	s16PrimaryCheck    = (GetIDBTarP.s16FRWheelTargetP==GetIDBTarP.s16RLWheelTargetP);
	s16SecondaryCheck  = (GetIDBTarP.s16FLWheelTargetP==GetIDBTarP.s16RRWheelTargetP);
    s16FrontWheelCheck = (GetIDBTarP.s16FLWheelTargetP==GetIDBTarP.s16FRWheelTargetP);
	s16AllWheelCheck   = (s16PrimaryCheck==1)&&(s16SecondaryCheck==1)&&(s16FrontWheelCheck==1);

	if(s16AllWheelCheck == TRUE)
	{
		ldidbu8AllWheelControl = TRUE;
		ldidbu8SelectWheelControl = FALSE;
	}
	else
	{
		ldidbu8AllWheelControl = FALSE;
		ldidbu8SelectWheelControl = TRUE;
	}
}

static void LDIDB_vDetOneWheelControl(void)
{
	uint16_t u16DetectCnt = 0;

	u16DetectCnt = LDIDB_u16CntActiveWheelVolume(GetIDBTarP.s16FLWheelTargetP, GetIDBTarP.s16FRWheelTargetP, GetIDBTarP.s16RLWheelTargetP, GetIDBTarP.s16RRWheelTargetP);

	if((u16DetectCnt==U16_FRONT_1WL_CNT) || (u16DetectCnt==U16_REAR_1WL_CNT))
	{
		ldidbu8OneWheelControl = TRUE;
		ldidbu8MultiWheelControl = FALSE;
	}
	else
	{
		ldidbu8OneWheelControl = FALSE;
		ldidbu8MultiWheelControl = TRUE;
	}
}


static void LDIDB_vDetCtrlCircuit(void)
{	
	int16_t s16PrimaryCheck = 0;
	int16_t s16SecondaryCheck = 0;
	int16_t ldidbs16PriDeltaWhlTP = 0;
	int16_t ldidbs16SecDeltaWhlTP = 0;

	s16PrimaryCheck   = (GetIDBTarP.s16FRWheelTargetP==GetIDBTarP.s16RLWheelTargetP);
	s16SecondaryCheck = (GetIDBTarP.s16FLWheelTargetP==GetIDBTarP.s16RRWheelTargetP);

    ldidbs16PriDeltaWhlTP = LDIDB_s16SelectMaxValue(ldidbs16DeltaFRWhlTP,ldidbs16DeltaRLWhlTP);
    ldidbs16SecDeltaWhlTP = LDIDB_s16SelectMaxValue (ldidbs16DeltaFLWhlTP,ldidbs16DeltaRRWhlTP);

	LDIDB_vDetOneWheelControl();

	if((ldidbu8WheelVolumeCtrlMode==S8_ALL_WHEEL) || (ldidbs16InABSCtrlActive==1) || (lau1MuxNChannelCtrlActive==1))
		/*need to change Abs Active condition. ex)MotorTorque control */
	{
		ldidbs16PriCircuitControl = S16_CIR_CTRL_ACTIVE;
		ldidbs16SecCircuitControl = S16_CIR_CTRL_ACTIVE;
	}
	else if(ldidbu8OneWheelControl == 1)
	{
		if(s16PrimaryCheck == FALSE)
		{
			ldidbs16PriCircuitControl = S16_CIR_CTRL_ACTIVE;
			ldidbs16SecCircuitControl = S16_CIR_CTRL_NONACTIVE;
		}
		else if(s16SecondaryCheck == FALSE)
		{
			ldidbs16PriCircuitControl = S16_CIR_CTRL_NONACTIVE;
			ldidbs16SecCircuitControl = S16_CIR_CTRL_ACTIVE;
		}
		else
		{
			ldidbs16PriCircuitControl = S16_CIR_CTRL_ACTIVE;
			ldidbs16SecCircuitControl = S16_CIR_CTRL_ACTIVE;
		}
	}
	else
	{
		/* Need to develop CircuitControl transition */
		if(ldidbs16PriDeltaWhlTP > ldidbs16SecDeltaWhlTP)
		{
			ldidbs16PriCircuitControl = S16_CIR_CTRL_ACTIVE;
			ldidbs16SecCircuitControl = S16_CIR_CTRL_NONACTIVE;
		}
		else if(ldidbs16PriDeltaWhlTP < ldidbs16SecDeltaWhlTP)
		{
			ldidbs16PriCircuitControl = S16_CIR_CTRL_NONACTIVE;
			ldidbs16SecCircuitControl = S16_CIR_CTRL_ACTIVE;
		}
		else
		{
			; /* Maintain CircuitControl */
		}
	}

	if(lau1MuxNChannelCtrlActive == TRUE)
	{
		lapctu8MuxCircCtrlReqPri = (uint8_t)0;
		lapctu8MuxCircCtrlReqSec = (uint8_t)0;
	}
	else
	{
		lapctu8MuxCircCtrlReqPri = (uint8_t)s16PrimaryCheck;
		lapctu8MuxCircCtrlReqSec = (uint8_t)s16SecondaryCheck;
	}
}


static void LDIDB_vReqCircuitValve(void)
{
	ldidbs16PriCircuitVVCloseReq = (ldidbs16PriCircuitControl == S16_CIR_CTRL_ACTIVE) ? S16_CIR_CTRL_NONACTIVE : S16_CIR_CTRL_ACTIVE;  /* not needed */
	ldidbs16SecCircuitVVCloseReq = (ldidbs16SecCircuitControl == S16_CIR_CTRL_ACTIVE) ? S16_CIR_CTRL_NONACTIVE : S16_CIR_CTRL_ACTIVE;  /* not needed */
}

static int16_t LDIDB_s16SelectOneWheelControl(int16_t s161stWheelValue, int16_t s162ndWheelValue, int16_t s16WheelSelOld)
{
	int16_t s16WheelSel = S16_FIRST_WHEEL;

	if (s161stWheelValue > s162ndWheelValue)
	{
		s16WheelSel = S16_FIRST_WHEEL;
	}
	else if (s161stWheelValue < s162ndWheelValue)
	{
		s16WheelSel = S16_SECOND_WHEEL;
	}
	else
	{
		s16WheelSel = s16WheelSelOld;
	}

	return s16WheelSel;
}

static int16_t LDIDB_s16SelectMultiWheelControl(int16_t s161stWheelTP, int16_t s162ndWheelTP, int16_t s161stDeltaWheelTP, int16_t s162ndDeltalWheelTP, int16_t s16WheelSelOld)
{
	int16_t s16WheelSel = S16_FIRST_WHEEL;

	if (s161stWheelTP == s162ndWheelTP)
	{
		if (s161stDeltaWheelTP == s162ndDeltalWheelTP)
		{
			s16WheelSel = S16_BOTH_WHEEL;
		}
		else
		{
			s16WheelSel = S16_BOTH_WHEEL;
			/* Need to consider difference btw wheel pressures */
		}
	}
	else
	{
		if (s161stDeltaWheelTP > s162ndDeltalWheelTP)
		{
			s16WheelSel = S16_FIRST_WHEEL;
		}
		else if (s161stDeltaWheelTP < s162ndDeltalWheelTP)
		{
			s16WheelSel = S16_SECOND_WHEEL;
		}
		else
		{
			/* In a circuit, prohibit independent 2 wheel control */
			/* Maintain old control state */
			s16WheelSel = s16WheelSelOld;
		}
	}

	return s16WheelSel;
}

static int16_t LDIDB_s16SelWheelTP(int16_t s16SelWheelVolume, int16_t s161stWheelTP, int16_t s162ndWheelTP)
{
	int16_t s16WheelTarget = 0;

	if ((s16SelWheelVolume == S16_FIRST_WHEEL) || (s16SelWheelVolume == S16_BOTH_WHEEL))
	{
		s16WheelTarget = s161stWheelTP;
	}
	else
	{
		s16WheelTarget = s162ndWheelTP;
	}

	return s16WheelTarget;
}

static int16_t LDIDB_s16SelDelWheelTP(int16_t s16SelWheelVolume, int16_t s161stDeltaWheelTP, int16_t s162ndDeltalWheelTP)
{
	int16_t s16DeltaWhlTP;

	if ((s16SelWheelVolume == S16_FIRST_WHEEL) || (s16SelWheelVolume == S16_BOTH_WHEEL))
	{
		s16DeltaWhlTP = s161stDeltaWheelTP;
	}
	else
	{
		s16DeltaWhlTP = s162ndDeltalWheelTP;
	}

	return s16DeltaWhlTP;
}

static void LDIDB_vDetWheelTarget(void)
{
	static int16_t ldidbss16SelWheelPOld = S16_FIRST_WHEEL;

	int16_t s16DeltaWhlTP;
	int16_t s16WheelTargetP;
	int16_t s16SelWheelP = ldidbss16SelWheelPOld;

	if((ldidbs16PriCircuitControl==S16_CIR_CTRL_ACTIVE) && (ldidbs16SecCircuitControl==S16_CIR_CTRL_ACTIVE))
	{
/*		ldahbu16WheelVolumeState = LDIDB_u16DctWheelVolumeState(GetIDBTarP.s16FLWheelTargetP,GetIDBTarP.s16FRWheelTargetP,GetIDBTarP.s16RLWheelTargetP,GetIDBTarP.s16RRWheelTargetP);*/
/*		ldidbs16RepresentWheelTargetP = (GetIDBTarP.s16FLWheelTargetP+GetIDBTarP.s16FRWheelTargetP+GetIDBTarP.s16RLWheelTargetP+GetIDBTarP.s16RRWheelTargetP)/4; */
		if(ldidbs16TCSFront2WhlCtrlFlg == TRUE)
		{
			ldidbu16WheelVolumeState = U16_SEL_F2WHEEL;

			ldidbs16RepresentWheelTargetP = L_s16LimitMinMax(ldidbs16TCSF2WhlTargetP, S16_PCTRL_PRESS_0_BAR, (S16_PCTRL_PRESS_200_BAR) );

			ldidbs16calRepresentDelWhlTP = ldidbs16DeltaTCSF2WhlTP;
		}
		else if(lau1MuxNChannelCtrlActive == TRUE)
		{
			ldidbu16WheelVolumeState = lapcts16MuxWheelVolumeState;
			ldidbs16RepresentWheelTargetP = L_s16LimitMinMax(laidbs16MuxTarPress, S16_PCTRL_PRESS_0_BAR, (S16_PCTRL_PRESS_200_BAR) );

			ldidbs16calRepresentDelWhlTP = lapcts16MuxTarDelPress;
		}
		else
		{
			ldidbu16WheelVolumeState = U16_SEL_4WHEEL;
			ldidbs16RepresentWheelTargetP = L_s16LimitMinMax(GetIDBTarP.s16TargetPress, S16_PCTRL_PRESS_0_BAR, (S16_PCTRL_PRESS_200_BAR) );
			ldidbs16calRepresentDelWhlTP = ldahbs16DelTargetPressX10;
		}
	}
	else
	{
		if(ldidbs16PriCircuitControl == S16_CIR_CTRL_ACTIVE)
		{
			if(ldidbu8OneWheelControl == TRUE)
			{
				s16SelWheelP = LDIDB_s16SelectOneWheelControl(GetIDBTarP.s16FRWheelTargetP, GetIDBTarP.s16RLWheelTargetP, ldidbss16SelWheelPOld);
			}
			else
			{
				s16SelWheelP = LDIDB_s16SelectMultiWheelControl(GetIDBTarP.s16FRWheelTargetP, GetIDBTarP.s16RLWheelTargetP, ldidbs16DeltaFRWhlTP, ldidbs16DeltaRLWhlTP, ldidbss16SelWheelPOld);
			}

			s16WheelTargetP = LDIDB_s16SelWheelTP(s16SelWheelP, GetIDBTarP.s16FRWheelTargetP, GetIDBTarP.s16RLWheelTargetP);

			s16DeltaWhlTP = LDIDB_s16SelDelWheelTP(s16SelWheelP, ldidbs16DeltaFRWhlTP, ldidbs16DeltaRLWhlTP);

		    ldidbu16WheelVolumeState = ldahbu16WheelVolumeArray[s16SelWheelP]; /* s16SelWheelP:0~2, ldahbu16WheelVolumeArray[3] = {U16_SEL_F1WHEEL, U16_SEL_R1WHEEL, U16_SEL_F1R1WHEEL}; */

		    ldidbss16SelWheelPOld = s16SelWheelP;
		}
		else if(ldidbs16SecCircuitControl == S16_CIR_CTRL_ACTIVE)
		{
			if(ldidbu8OneWheelControl == TRUE)
			{
				s16SelWheelP = LDIDB_s16SelectOneWheelControl(GetIDBTarP.s16FLWheelTargetP, GetIDBTarP.s16RRWheelTargetP, ldidbss16SelWheelPOld);
			}
			else
			{
				s16SelWheelP = LDIDB_s16SelectMultiWheelControl(GetIDBTarP.s16FLWheelTargetP, GetIDBTarP.s16RRWheelTargetP, ldidbs16DeltaFLWhlTP, ldidbs16DeltaRRWhlTP, ldidbss16SelWheelPOld);
			}

			s16WheelTargetP = LDIDB_s16SelWheelTP(s16SelWheelP, GetIDBTarP.s16FLWheelTargetP, GetIDBTarP.s16RRWheelTargetP);

			s16DeltaWhlTP = LDIDB_s16SelDelWheelTP(s16SelWheelP, ldidbs16DeltaFLWhlTP, ldidbs16DeltaRRWhlTP);

			ldidbu16WheelVolumeState = ldahbu16WheelVolumeArray[s16SelWheelP]; /* s16SelWheelP:0~2, ldahbu16WheelVolumeArray[3] = {U16_SEL_F1WHEEL, U16_SEL_R1WHEEL, U16_SEL_F1R1WHEEL}; */

		    ldidbss16SelWheelPOld = s16SelWheelP;
		}
		else
		{
			s16WheelTargetP = S16_MPRESS_0_BAR;
			s16DeltaWhlTP = S16_MPRESS_0_BAR;
			ldidbss16SelWheelPOld = S16_FIRST_WHEEL;
			ldidbu16WheelVolumeState = U16_SEL_4WHEEL;
		}

	    ldidbs16RepresentWheelTargetP = s16WheelTargetP;
	    ldidbs16calRepresentDelWhlTP = s16DeltaWhlTP;
	}

/*	ldidbu16WheelVolumeState = LDIDB_u16MaintainVolumeState(ldidbu16WheelVolumeState,ldidbu16WheelVolumeStateOld,ldidbs16RepresentWheelTargetP);*/
}

/*
static uint16_t LDIDB_u16MaintainVolumeState(uint16_t u16VolumeState, uint16_t u16VolumeStateOld, int16_t s16CtrlTargetP)
{
	uint16_t u16WheelVolume = u16VolumeState;

	if((u16VolumeState==U16_SEL_4WHEEL) && (u16VolumeStateOld!=U16_SEL_4WHEEL))
	{
		if((s16CtrlTargetP==S16_PCTRL_PRESS_0_BAR) && (laidbs8PCtrlState==S8_PRESS_BOOST))
		{
			u16WheelVolume =u16VolumeStateOld;
		}
		else
		{
			u16WheelVolume = u16VolumeState;
		}
	}
	else
	{
		u16WheelVolume = u16VolumeState;
	}

	return u16WheelVolume;
}
*/


static void LDIDB_vDetWheelVolumeChange(void)
{
	static int16_t ldidbs16PriCircuitControlOld = 0;
	static int16_t ldidbs16SecCircuitControlOld = 0;


	int16_t s16CheckPriCircuit = 0;
	int16_t s16CheckSecCircuit = 0;
	int16_t s16CheckWheelVolume = 0;

	s16CheckPriCircuit = ((ldidbs16PriCircuitControl==1) && (ldidbs16PriCircuitControlOld==0)) || ((ldidbs16PriCircuitControl==0) && (ldidbs16PriCircuitControlOld==1));
	s16CheckSecCircuit = ((ldidbs16SecCircuitControl==1) && (ldidbs16SecCircuitControlOld==0)) || ((ldidbs16SecCircuitControl==0) && (ldidbs16SecCircuitControlOld==1));
	s16CheckWheelVolume = (ldidbu16WheelVolumeStateOld == ldidbu16WheelVolumeState);


	if(s16CheckWheelVolume == FALSE)
	{
		ldidbs16ResetWheelVolume = TRUE;
	}
	else
	{
		if((s16CheckPriCircuit==TRUE) || (s16CheckSecCircuit==TRUE))
		{
			ldidbs16ResetWheelVolume = TRUE;
		}
		else
		{
			ldidbs16ResetWheelVolume = FALSE;
		}
	}
	
	if((ldidbs16ResetWheelVolume == TRUE) && ((ldidbs16PriCircuitControlOld==ldidbs16PriCircuitControl) || (ldidbs16SecCircuitControlOld==ldidbs16SecCircuitControl)))
	{
		if((ldidbu16WheelVolumeStateOld == U16_SEL_F1WHEEL) &&(ldidbu16WheelVolumeState == U16_SEL_R1WHEEL))
		{
			ldidbs16IdenticalCircuitFlg = TRUE;
		}
		else { /* Reset ldidbs16IdenticalCircuitFlg */ }

	}
	else
	{
		ldidbs16IdenticalCircuitFlg = FALSE;
	}

	ldidbs16PriCircuitControlOld = ldidbs16PriCircuitControl;
	ldidbs16SecCircuitControlOld = ldidbs16SecCircuitControl;
	ldidbu16WheelVolumeStateOld  = ldidbu16WheelVolumeState;
}


static uint16_t LDIDB_u16CntActiveWheelVolume(int16_t FL_TargetP, int16_t FR_TargetP,  int16_t RL_TargetP,  int16_t RR_TargetP)
{
	uint16_t u16VolumeCnt = 0;
		if (FL_TargetP>0)
		{
			u16VolumeCnt = u16VolumeCnt + 1;
		}
		else { /* Do Nothing */ }

		if (FR_TargetP>0)
		{
			u16VolumeCnt = u16VolumeCnt + 1;
		}
		else { /* Do Nothing */ }

		if (RL_TargetP>0)
		{
			u16VolumeCnt = u16VolumeCnt + 4;
		}
		else { /* Do Nothing */ }

		if (RR_TargetP>0)
		{
			u16VolumeCnt = u16VolumeCnt + 4;
		}
		else { /* Do Nothing */ }

	return u16VolumeCnt;
}


static int16_t LDIDB_s16SelectMaxValue(int16_t s16Xinput, int16_t s16Yinput)
{
	int16_t s16ReturnValue = 0;

	if(s16Xinput>s16Yinput)
	{
		s16ReturnValue = s16Xinput;
	}
	else
	{
		s16ReturnValue = s16Yinput;
	}

	return s16ReturnValue;
}


static void LDIDB_vEstABSTargetPress(void)
{
    static int16_t ldidbs16ABSTargetPressOld = S16_MPRESS_0_BAR;
    int16_t s16PrssureDiffSize = S16_MPRESS_1_BAR;

    s16PrssureDiffSize = L_s16IInter2Point(L_s16Abs(GetIDBTarP.s16TargetPress - ldidbs16InABSBoostTargetP),
																		S16_PCTRL_PRESS_5_BAR,  S16_PCTRL_PRESS_1_BAR,
																		S16_PCTRL_PRESS_50_BAR, S16_PCTRL_PRESS_10_BAR);

    if(ldidbs16ReqChangeTPFromABSFlg == FALSE)
    {
    	ldidbs16ABSBoostTargetPress = GetIDBTarP.s16TargetPress;
    }
    else
    {
    	ldidbs16ABSBoostTargetPress = L_s16LimitDiff(ldidbs16InABSBoostTargetP,ldidbs16ABSTargetPressOld,s16PrssureDiffSize,s16PrssureDiffSize);
    }

	ldidbs16ABSTargetPressOld = ldidbs16ABSBoostTargetPress;
}

#define PCT_5MSCTRL_STOP_SEC_CODE
#include "Pct_MemMap.h"
