/*******************************************************************************
* Project Name:		IDB
* File Name:		LAIDBCallMultiplexControl.c
* Description:		Main Code of Multiplexer
* Logic version:
********************************************************************************
*  Modification Log
*  Date			        Author			Description
*  2014. 12. 29.		dolores.kim		Initial release
********************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "../LAIDB_CallMultiplexControl.h"
#include "LAIDB_CallMultiplexValveActuation.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

#define U16_INV_CLOSE_RATIO_MAX			 1000
#define U16_CONV_PWMDUTY_TO_RATIO(input) (input*5)

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct {
	uint32_t lau1ValveAct:  1;
	uint8_t  lau8ValveOnTime;
	/*uint16_t lau16Current; For IDB '15 NZ A,B sample */
	uint16_t lau16ValveActRatio;
	uint16_t lau16ValveActCnt;
}LA_MUXVLV_CMD_VAR_t;


typedef struct {
	uint16_t u16ValveActRatio[5];
}LA_MUXVLV_ACT_t;

typedef struct {
	uint32_t u1ValveActIn1st5ms :1;
}LA_MUXVLV_FLG_t;

/*------------temporary for inlet valve input--------------*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_CONST_UNSPECIFIED
#include "Pct_MemMap.h"
/** Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Pct_MemMap.h"

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

uint16_t FlInVlvCtrlTar[5]; /* rte output to VAT */
uint16_t FrInVlvCtrlTar[5]; /* rte output to VAT */
uint16_t RlInVlvCtrlTar[5]; /* rte output to VAT */
uint16_t RrInVlvCtrlTar[5]; /* rte output to VAT */

#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"



#define PCT_5MSCTRL_START_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/** Variable Section (32BIT)**/
#define PCT_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Pct_MemMap.h"

/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

FAL_STATIC LA_MUXVLV_CMD_VAR_t lapctst_MuxInVCtrlVarFL;
FAL_STATIC LA_MUXVLV_CMD_VAR_t lapctst_MuxInVCtrlVarFR;
FAL_STATIC LA_MUXVLV_CMD_VAR_t lapctst_MuxInVCtrlVarRL;
FAL_STATIC LA_MUXVLV_CMD_VAR_t lapctst_MuxInVCtrlVarRR;

FAL_STATIC LA_MUXVLV_ACT_t lapctst_MuxInVActFL;
FAL_STATIC LA_MUXVLV_ACT_t lapctst_MuxInVActFR;
FAL_STATIC LA_MUXVLV_ACT_t lapctst_MuxInVActRL;
FAL_STATIC LA_MUXVLV_ACT_t lapctst_MuxInVActRR;

FAL_STATIC LA_MUXVLV_FLG_t lapctst_MuxVlvActFlag;

#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
#define PCT_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PCT_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
#define PCT_5MSCTRL_START_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/** Variable Section (32BIT)**/


#define PCT_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Pct_MemMap.h"


/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_CODE
#include "Pct_MemMap.h"

/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
static void LAIDB_vDecideMuxVlvCmd(LAMUX_WHL_VV_CMD_t *pMuxWLCMD, LA_MUXVLV_CMD_VAR_t *pMuxVlvSt);
static void LAPCT_vAllocMuxVlvCurrentArray( const LA_MUXVLV_CMD_VAR_t* const la_VV,  LA_MUXVLV_ACT_t *pOutDrvArray);
static void LAPCT_vOutputMuxVlvTargets(void);

static void LAIDB_vDecideMuxApplyCommand(LAMUX_WHL_VV_CMD_t *pMuxWLCMD, LA_MUXVLV_CMD_VAR_t *pMuxVlvSt, uint16_t u16VlvActRatio);
static void LAPCT_vAllocAbcVlvCurrentArray(void);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void LAIDB_vDecideMuxWhlVlvCmd(void)
{
	uint16_t u16VlvActRatio = 0; /* 80 */

	LAIDB_vDecideMuxVlvCmd(&laMux_FLCMD, &lapctst_MuxInVCtrlVarFL); /* Secondary Circuit */
	LAIDB_vDecideMuxVlvCmd(&laMux_FRCMD, &lapctst_MuxInVCtrlVarFR); /* Primary Circuit */
	LAIDB_vDecideMuxVlvCmd(&laMux_RLCMD, &lapctst_MuxInVCtrlVarRL);
	LAIDB_vDecideMuxVlvCmd(&laMux_RRCMD, &lapctst_MuxInVCtrlVarRR);

	LAPCT_vAllocMuxVlvCurrentArray(&lapctst_MuxInVCtrlVarFL, &lapctst_MuxInVActFL);
	LAPCT_vAllocMuxVlvCurrentArray(&lapctst_MuxInVCtrlVarFR, &lapctst_MuxInVActFR);
	LAPCT_vAllocMuxVlvCurrentArray(&lapctst_MuxInVCtrlVarRL, &lapctst_MuxInVActRL);
	LAPCT_vAllocMuxVlvCurrentArray(&lapctst_MuxInVCtrlVarRR, &lapctst_MuxInVActRR);

	LAPCT_vOutputMuxVlvTargets();
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

/******************************************************************************
* FUNCTION NAME:      LAIDB_vDecideMuxApplyCommand
* CALLED BY:          LAIDB_vDecideMuxWhlVlvCmd()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Set valve commands per mux wheel control mode
******************************************************************************/
static void LAIDB_vDecideMuxVlvCmd(LAMUX_WHL_VV_CMD_t *pMuxWLCMD, LA_MUXVLV_CMD_VAR_t *pMuxVlvSt)
{
	uint16_t u16ValveActCntRef = U8_FULL_CYCLETIME;
	uint16_t u16VlvActRatio = pMuxWLCMD->u16MuxWhlVlvActRatio;

	switch(pMuxWLCMD->u8MuxWhlCtrlMode)
	{
		case U8_MUXWHLCTRL_APPLY:

			if(pMuxVlvSt->lau16ValveActCnt==0)
			{
				/* Decide Wheel Valve Command */
				/* INPUT: u8WhlCmdDelayTime, u16WhlCmdExecTime; */
				/* OUTPUT: u8PwmDuty, u8InletOnTime u8ValveDelayTimeBfOn u8OutletOnTime */
				LAIDB_vDecideMuxApplyCommand(pMuxWLCMD, pMuxVlvSt, u16VlvActRatio);
				u16ValveActCntRef = (uint16_t)(((pMuxWLCMD->u8WhlCmdDelayTime) + (pMuxWLCMD->u16WhlCmdExecTime))/U8_FULL_CYCLETIME + 1);
				pMuxVlvSt->lau16ValveActCnt = pMuxVlvSt->lau16ValveActCnt + 1;
			}
			else
			{
				/* Decide Wheel Valve Command */
				/* INPUT: u8WhlCmdDelayTime, u16WhlCmdExecTime; */
				/* OUTPUT: u8PwmDuty, u8InletOnTime u8ValveDelayTimeBfOn u8OutletOnTime */
				LAIDB_vDecideMuxApplyCommand(pMuxWLCMD, pMuxVlvSt, u16VlvActRatio);
				u16ValveActCntRef = (uint16_t)((pMuxWLCMD->u16WhlCmdExecTime)/U8_FULL_CYCLETIME + 1);
				/*pMuxVlvSt->lau8ValveDelayTimeBfOn = 0;*/
				pMuxVlvSt->lau16ValveActCnt = pMuxVlvSt->lau16ValveActCnt + 1;
			}

			if(pMuxVlvSt->lau16ValveActCnt>=u16ValveActCntRef)
			{
				pMuxVlvSt->lau16ValveActCnt = 0;
			}
			break;

		case U8_MUXWHLCTRL_HOLD:
			pMuxVlvSt->lau1ValveAct = U8_MUXVLV_CLOSE;
			/*pMuxVlvSt->lau8ValveDelayTimeBfOn = 0;*/
			pMuxVlvSt->lau8ValveOnTime = U8_FULL_CYCLETIME;
			pMuxVlvSt->lau16ValveActRatio = U16_INV_CLOSE_RATIO_MAX;
			break;
/*
		case U8_MUXWHLCTRL_FULLRISE:
			pMuxVlvSt->lau1ValveAct = U8_MUXVLV_DEFAULT;
			pMuxVlvSt->lau8ValveDelayTimeBfOn = 0;
			pMuxVlvSt->lau8ValveOnTime = U8_FULL_CYCLETIME;
			pMuxVlvSt->lau16ValveActRatio = 0;
			break;
*/
		default:
			pMuxVlvSt->lau1ValveAct = U8_MUXVLV_DEFAULT;
			/*pMuxVlvSt->lau8ValveDelayTimeBfOn = 0;*/
			pMuxVlvSt->lau8ValveOnTime = U8_FULL_CYCLETIME;
			pMuxVlvSt->lau16ValveActRatio = 0;
			break;
	}
/*
	la_VV->u8ValveAct = (uint8_t)pMuxVlvSt->lau1ValveAct;
	la_VV->u8ValveOnTime = pMuxVlvSt->lau8ValveOnTime;
	la_VV->u16Current = pMuxVlvSt->lau16Current;
*/
}


/******************************************************************************
* FUNCTION NAME:      LAIDB_vDecideMuxApplyCommand
* CALLED BY:          LAIDB_vDecideMuxWhlVlvCmd()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Set pressure apply valve on-time and actuation request
******************************************************************************/
static void LAIDB_vDecideMuxApplyCommand(LAMUX_WHL_VV_CMD_t *pMuxWLCMD, LA_MUXVLV_CMD_VAR_t *pMuxVlvSt, uint16_t u16VlvActRatio)
{
	uint16_t u16MuxWheelCmdExecTime = 0;
	uint8_t  u8MuxWheelCmdDelayTime = 0;
	uint8_t  u8NoDelayTimeBefApply = 0;
	uint8_t  u8ApplyTime = 0;
	uint16_t u16NoVlvRatio = 0;

	u16MuxWheelCmdExecTime = pMuxWLCMD->u16WhlCmdExecTime;
	pMuxWLCMD->u8WhlCmdDelayTime = ((pMuxWLCMD->u8WhlCmdDelayTime>U8_FULL_CYCLETIME)?U8_FULL_CYCLETIME:(pMuxWLCMD->u8WhlCmdDelayTime));
	u8MuxWheelCmdDelayTime = pMuxWLCMD->u8WhlCmdDelayTime;

	if((u8MuxWheelCmdDelayTime>0) && (pMuxVlvSt->lau16ValveActCnt==0))
	{
		u8NoDelayTimeBefApply = u8MuxWheelCmdDelayTime;
		if(u16MuxWheelCmdExecTime>0)
		{
			u8ApplyTime = (pMuxWLCMD->u16WhlCmdExecTime>(U8_FULL_CYCLETIME-pMuxWLCMD->u8WhlCmdDelayTime))?(U8_FULL_CYCLETIME-pMuxWLCMD->u8WhlCmdDelayTime):pMuxWLCMD->u16WhlCmdExecTime;
			u16NoVlvRatio = u16VlvActRatio;
		}
		else
		{
			u8ApplyTime = U8_FULL_CYCLETIME;
			u16NoVlvRatio = U16_INV_CLOSE_RATIO_MAX;
		}

	}
	else
	{
		u8NoDelayTimeBefApply = 0;
		if(u16MuxWheelCmdExecTime>0)
		{
			u8ApplyTime = (pMuxWLCMD->u16WhlCmdExecTime>U8_FULL_CYCLETIME)?U8_FULL_CYCLETIME:pMuxWLCMD->u16WhlCmdExecTime;
			u16NoVlvRatio = u16VlvActRatio;
		}
		else
		{
	    	u8ApplyTime = U8_FULL_CYCLETIME;
	    	u16NoVlvRatio = U16_INV_CLOSE_RATIO_MAX;
		}
	}

	if(u16NoVlvRatio > 0)
	{
		pMuxVlvSt->lau1ValveAct = TRUE;
	}
	else
	{
		pMuxVlvSt->lau1ValveAct = FALSE;
	}
	/*pMuxVlvSt->lau8ValveDelayTimeBfOn = u8NoDelayTimeBefApply;*/
	pMuxVlvSt->lau8ValveOnTime = u8ApplyTime;
	pMuxVlvSt->lau16ValveActRatio = u16VlvActRatio;
}


/******************************************************************************
* FUNCTION NAME:      LAPCT_vAllocMuxVlvCurrentArray
* CALLED BY:          LAIDB_vDecideMuxWhlVlvCmd()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Set 1ms array per Inlet valve on time
******************************************************************************/
static void LAPCT_vAllocMuxVlvCurrentArray( const LA_MUXVLV_CMD_VAR_t* const la_VV,  LA_MUXVLV_ACT_t *pOutDrvArray)
{
    uint8_t CurrTim;

    if(la_VV->lau8ValveOnTime>=U8_FULL_CYCLETIME)
    {
    	for(CurrTim=0; CurrTim<U8_FULL_CYCLETIME; CurrTim++)
    	{
    		pOutDrvArray->u16ValveActRatio[CurrTim] = la_VV->lau16ValveActRatio;
    	}
    }
    else
    {
    	for(CurrTim=0; CurrTim<(la_VV->lau8ValveOnTime); CurrTim++)
    	{
    		pOutDrvArray->u16ValveActRatio[CurrTim] = la_VV->lau16ValveActRatio;
    	}

    	for(CurrTim=la_VV->lau8ValveOnTime; CurrTim<U8_FULL_CYCLETIME; CurrTim++)
    	{
    		pOutDrvArray->u16ValveActRatio[CurrTim] = U16_INV_CLOSE_RATIO_MAX;
    	}
    }
}


/******************************************************************************
* FUNCTION NAME:      LAPCT_vOutputMuxVlvTargets
* CALLED BY:          LAIDB_vDecideMuxWhlVlvCmd()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:        Copy actuation array to output structure
******************************************************************************/
static void LAPCT_vOutputMuxVlvTargets(void)
{
	uint8_t u8CurrTim;
	uint8_t u8AllocNum = 0;

	if(lapctu1AbsMtrTqCtrlActive == TRUE)
	{
		LAPCT_vAllocAbcVlvCurrentArray();
	}
	else
	{
		FlInVlvCtrlTar[0] = lapctst_MuxInVActFL.u16ValveActRatio[0];
		FlInVlvCtrlTar[1] = lapctst_MuxInVActFL.u16ValveActRatio[1];
		FlInVlvCtrlTar[2] = lapctst_MuxInVActFL.u16ValveActRatio[2];
		FlInVlvCtrlTar[3] = lapctst_MuxInVActFL.u16ValveActRatio[3];
		FlInVlvCtrlTar[4] = lapctst_MuxInVActFL.u16ValveActRatio[4];

		FrInVlvCtrlTar[0] = lapctst_MuxInVActFR.u16ValveActRatio[0];
		FrInVlvCtrlTar[1] = lapctst_MuxInVActFR.u16ValveActRatio[1];
		FrInVlvCtrlTar[2] = lapctst_MuxInVActFR.u16ValveActRatio[2];
		FrInVlvCtrlTar[3] = lapctst_MuxInVActFR.u16ValveActRatio[3];
		FrInVlvCtrlTar[4] = lapctst_MuxInVActFR.u16ValveActRatio[4];

		RlInVlvCtrlTar[0] = lapctst_MuxInVActRL.u16ValveActRatio[0];
		RlInVlvCtrlTar[1] = lapctst_MuxInVActRL.u16ValveActRatio[1];
		RlInVlvCtrlTar[2] = lapctst_MuxInVActRL.u16ValveActRatio[2];
		RlInVlvCtrlTar[3] = lapctst_MuxInVActRL.u16ValveActRatio[3];
		RlInVlvCtrlTar[4] = lapctst_MuxInVActRL.u16ValveActRatio[4];

		RrInVlvCtrlTar[0] = lapctst_MuxInVActRR.u16ValveActRatio[0];
		RrInVlvCtrlTar[1] = lapctst_MuxInVActRR.u16ValveActRatio[1];
		RrInVlvCtrlTar[2] = lapctst_MuxInVActRR.u16ValveActRatio[2];
		RrInVlvCtrlTar[3] = lapctst_MuxInVActRR.u16ValveActRatio[3];
		RrInVlvCtrlTar[4] = lapctst_MuxInVActRR.u16ValveActRatio[4];
	}
}


static void LAPCT_vAllocAbcVlvCurrentArray(void)
{
	uint8_t u8CurrTim = 0;
	uint8_t u8GetFromAbcStartNum = 0;

	if(lapctst_MuxVlvActFlag.u1ValveActIn1st5ms==0)
	{
		lapctst_MuxVlvActFlag.u1ValveActIn1st5ms = 1;
		u8GetFromAbcStartNum = 0;
	}
	else
	{
		lapctst_MuxVlvActFlag.u1ValveActIn1st5ms = 0;
		u8GetFromAbcStartNum = U8_FULL_CYCLETIME;
	}



	if(Pct_5msCtrlBus.Pct_5msCtrlWhlVlvReqAbcInfo.FlIvReq == 1)
	{
		for(u8CurrTim=0; u8CurrTim<U8_FULL_CYCLETIME; u8CurrTim++)
		{
			FlInVlvCtrlTar[u8CurrTim] = Pct_5msCtrlBus.Pct_5msCtrlWhlVlvReqAbcInfo.FlIvReqData [u8CurrTim + u8GetFromAbcStartNum]; /* Task_10ms_Abc_CtrlWhlVlvReqAbcInfo.FlIvReqData */
		}
	}
	else
	{
		FlInVlvCtrlTar[0] = 0;
		FlInVlvCtrlTar[1] = 0;
		FlInVlvCtrlTar[2] = 0;
		FlInVlvCtrlTar[3] = 0;
		FlInVlvCtrlTar[4] = 0;
	}

	if(Pct_5msCtrlBus.Pct_5msCtrlWhlVlvReqAbcInfo.FrIvReq == 1)
	{
		for(u8CurrTim=0; u8CurrTim<U8_FULL_CYCLETIME; u8CurrTim++)
		{
			FrInVlvCtrlTar[u8CurrTim] = Pct_5msCtrlBus.Pct_5msCtrlWhlVlvReqAbcInfo.FrIvReqData [u8CurrTim + u8GetFromAbcStartNum]; /* Task_10ms_Abc_CtrlWhlVlvReqAbcInfo.FlIvReqData */
		}
	}
	else
	{
		FrInVlvCtrlTar[0] = 0;
		FrInVlvCtrlTar[1] = 0;
		FrInVlvCtrlTar[2] = 0;
		FrInVlvCtrlTar[3] = 0;
		FrInVlvCtrlTar[4] = 0;
	}

	if(Pct_5msCtrlBus.Pct_5msCtrlWhlVlvReqAbcInfo.RlIvReq == 1)
	{
		for(u8CurrTim=0; u8CurrTim<U8_FULL_CYCLETIME; u8CurrTim++)
		{
			RlInVlvCtrlTar[u8CurrTim] = Pct_5msCtrlBus.Pct_5msCtrlWhlVlvReqAbcInfo.RlIvReqData [u8CurrTim + u8GetFromAbcStartNum]; /* Task_10ms_Abc_CtrlWhlVlvReqAbcInfo.FlIvReqData */
		}
	}
	else
	{
		RlInVlvCtrlTar[0] = 0;
		RlInVlvCtrlTar[1] = 0;
		RlInVlvCtrlTar[2] = 0;
		RlInVlvCtrlTar[3] = 0;
		RlInVlvCtrlTar[4] = 0;
	}

	if(Pct_5msCtrlBus.Pct_5msCtrlWhlVlvReqAbcInfo.RrIvReq == 1)
	{
		for(u8CurrTim=0; u8CurrTim<U8_FULL_CYCLETIME; u8CurrTim++)
		{
			RrInVlvCtrlTar[u8CurrTim] = Pct_5msCtrlBus.Pct_5msCtrlWhlVlvReqAbcInfo.RrIvReqData [u8CurrTim + u8GetFromAbcStartNum]; /* Abc_CtrlBus.Abc_CtrlWhlVlvReqAbcInfo.FlIvReqData */
		}
	}
	else
	{
		RrInVlvCtrlTar[0] = 0;
		RrInVlvCtrlTar[1] = 0;
		RrInVlvCtrlTar[2] = 0;
		RrInVlvCtrlTar[3] = 0;
		RrInVlvCtrlTar[4] = 0;
	}
}

#define PCT_5MSCTRL_STOP_SEC_CODE
#include "Pct_MemMap.h"


/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
