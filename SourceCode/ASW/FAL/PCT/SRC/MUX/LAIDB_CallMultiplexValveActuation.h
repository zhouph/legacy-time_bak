/*
 * LAIDB_CallMultiplexValveActuation.h
 *
 *  Created on: 2015. 2. 3.
 *      Author: dolores.kim
 */

#ifndef LAIDB_CALLMULTIPLEXVALVEACTUATION_H_
#define LAIDB_CALLMULTIPLEXVALVEACTUATION_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/



/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/
#define U8_INF_VV_CLOSE_DELAY 	0
#define U8_INF_VV_MIN_OPEN_T  	5
#define U8_INR_VV_CLOSE_DELAY   0
#define U8_INR_VV_MIN_OPEN_T    5


/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* extern LAMUX_WHL_VV_CMD_t laMux_FLCMD, laMux_FRCMD, laMux_RLCMD, laMux_RRCMD; */


/*
extern LA_VALVE_CMD_t lapctst_MuxInVCmdFL;  local output
extern LA_VALVE_CMD_t lapctst_MuxInVCmdFR;  local output
extern LA_VALVE_CMD_t lapctst_MuxInVCmdRL;  local output
extern LA_VALVE_CMD_t lapctst_MuxInVCmdRR;  local output
*/

extern uint16_t FlInVlvCtrlTar[5]; /* rte output to VAT */
extern uint16_t FrInVlvCtrlTar[5]; /* rte output to VAT */
extern uint16_t RlInVlvCtrlTar[5]; /* rte output to VAT */
extern uint16_t RrInVlvCtrlTar[5]; /* rte output to VAT */

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void LAIDB_vDecideMuxWhlVlvCmd(void);

#endif /* LAIDB_CALLMULTIPLEXVALVEACTUATION_H_ */
