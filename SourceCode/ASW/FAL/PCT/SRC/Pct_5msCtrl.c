/**
 * @defgroup Pct_5msCtrl Pct_5msCtrl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Pct_5msCtrl.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Pct_5msCtrl.h"
#include "Pct_5msCtrl_Ifa.h"
#include "IfxStm_reg.h"
#include "LAPCT_DecidePressCtrlMode.h"
#include "LAIDB_CallMultiplexControl.h"
#include "VLV/LAPCT_PressureVlvCtrlMode.h"
#include "VLV/LAPCT_PedalFeelGenVlvCtrlMode.h"
#include "./MTR/LAPCT_DecideMtrCtrlMode.h"
#include "LAPCT_DecideStkRcvrState.h"

#if (M_IDB_TEST == ENABLE)
#include "L_IdbTestMain.h"
#include "LAPCT_DecidePressSource.h"
#include "LAIDB_CallMultiplexValveActuation.h"
#include "LAPCT_DecidePressSource.h"
#endif
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_CONST_UNSPECIFIED
#include "Pct_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PCT_5MSCTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Pct_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Pct_5msCtrl_HdrBusType Pct_5msCtrlBus;

/* Internal Logic Bus */
PressureControl_LocalVar      PressCTRLLocalBus;
/* Version Info */
const SwcVersionInfo_t Pct_5msCtrlVersionInfo = 
{   
    PCT_5MSCTRL_MODULE_ID,           /* Pct_5msCtrlVersionInfo.ModuleId */
    PCT_5MSCTRL_MAJOR_VERSION,       /* Pct_5msCtrlVersionInfo.MajorVer */
    PCT_5MSCTRL_MINOR_VERSION,       /* Pct_5msCtrlVersionInfo.MinorVer */
    PCT_5MSCTRL_PATCH_VERSION,       /* Pct_5msCtrlVersionInfo.PatchVer */
    PCT_5MSCTRL_BRANCH_VERSION       /* Pct_5msCtrlVersionInfo.BranchVer */
};
    
/* Input Data Element */
Abc_CtrlWhlVlvReqAbcInfo_t Pct_5msCtrlWhlVlvReqAbcInfo;
Eem_MainEemFailData_t Pct_5msCtrlEemFailData;
Spc_1msCtrlPedlTrvlFild1msInfo_t Pct_5msCtrlPedlTrvlFild1msInfo;
Proxy_RxCanRxAccelPedlInfo_t Pct_5msCtrlCanRxAccelPedlInfo;
Msp_CtrlMotRotgAgSigInfo_t Pct_5msCtrlMotRotgAgSigInfo;
Abc_CtrlAbsCtrlInfo_t Pct_5msCtrlAbsCtrlInfo;
Abc_CtrlAvhCtrlInfo_t Pct_5msCtrlAvhCtrlInfo;
Abc_CtrlEscCtrlInfo_t Pct_5msCtrlEscCtrlInfo;
Abc_CtrlHsaCtrlInfo_t Pct_5msCtrlHsaCtrlInfo;
Abc_CtrlStkRecvryCtrlIfInfo_t Pct_5msCtrlStkRecvryCtrlIfInfo;
Abc_CtrlTcsCtrlInfo_t Pct_5msCtrlTcsCtrlInfo;
Bbc_CtrlBaseBrkCtrlModInfo_t Pct_5msCtrlBaseBrkCtrlModInfo;
Det_5msCtrlBrkPedlStatusInfo_t Pct_5msCtrlBrkPedlStatusInfo;
Det_1msCtrlBrkPedlStatus1msInfo_t Pct_5msCtrlBrkPedlStatus1msInfo;
Spc_5msCtrlCircPFildInfo_t Pct_5msCtrlCircPFildInfo;
Det_5msCtrlCircPRateInfo_t Pct_5msCtrlCircPRateInfo;
Det_1msCtrlPedlTrvlRate1msInfo_t Pct_5msCtrlPedlTrvlRate1msInfo;
Det_5msCtrlEstimdWhlPInfo_t Pct_5msCtrlEstimdWhlPInfo;
Arb_CtrlFinalTarPInfo_t Pct_5msCtrlFinalTarPInfo;
Mcc_1msCtrlIdbMotPosnInfo_t Pct_5msCtrlIdbMotPosnInfo;
Arb_CtrlWhlOutVlvCtrlTarInfo_t Pct_5msCtrlWhlOutVlvCtrlTarInfo;
Arb_CtrlWhlPreFillInfo_t Pct_5msCtrlWhlPreFillInfo;
Vat_CtrlIdbVlvActInfo_t Pct_5msCtrlIdbVlvActInfo;
Spc_5msCtrlPedlSimrPFildInfo_t Pct_5msCtrlPedlSimrPFildInfo;
Det_5msCtrlPedlTrvlRateInfo_t Pct_5msCtrlPedlTrvlRateInfo;
Det_5msCtrlPedlTrvlTagInfo_t Pct_5msCtrlPedlTrvlTagInfo;
Spc_5msCtrlPistPFildInfo_t Pct_5msCtrlPistPFildInfo;
Det_5msCtrlPistPRateInfo_t Pct_5msCtrlPistPRateInfo;
Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo_t Pct_5msCtrlVlvActtnReqByBaseBrkCtrlrInfo;
Spc_5msCtrlWhlSpdFildInfo_t Pct_5msCtrlWhlSpdFildInfo;
Eem_MainEemSuspectData_t Pct_5msCtrlEemSuspectData;
Mom_HndlrEcuModeSts_t Pct_5msCtrlEcuModeSts;
Mcc_1msCtrlMotCtrlMode_t Pct_5msCtrlMotCtrlMode;
Eem_SuspcDetnFuncInhibitPctrlSts_t Pct_5msCtrlFuncInhibitPctrlSts;
Bbc_CtrlBaseBrkCtrlrActFlg_t Pct_5msCtrlBaseBrkCtrlrActFlg;
Spc_5msCtrlBlsFild_t Pct_5msCtrlBlsFild;
Rbc_CtrlBrkPRednForBaseBrkCtrlr_t Pct_5msCtrlBrkPRednForBaseBrkCtrlr;
Mcc_1msCtrlMotICtrlFadeOutState_t Pct_5msCtrlMotICtrlFadeOutState;
Pct_1msCtrlPreBoostMod_t Pct_5msCtrlPreBoostMod;
Rbc_CtrlRgnBrkCtlrBlendgFlg_t Pct_5msCtrlRgnBrkCtlrBlendgFlg;
Det_5msCtrlVehSpdFild_t Pct_5msCtrlVehSpdFild;
Mcc_1msCtrlStkRecvryStabnEndOK_t Pct_5msCtrlStkRecvryStabnEndOK;

/* Output Data Element */
Pct_5msCtrlIdbPCtrllVlvCtrlStInfo_t Pct_5msCtrlIdbPCtrllVlvCtrlStInfo;
Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo_t Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo;
Pct_5msCtrlWhlInVlvCtrlTarInfo_t Pct_5msCtrlWhlnVlvCtrlTarInfo;
Pct_5msCtrlStkRecvryActnIfInfo_t Pct_5msCtrlStkRecvryActnIfInfo;
Pct_5msCtrlMuxCmdExecStInfo_t Pct_5msCtrlMuxCmdExecStInfo;
Pct_5msCtrlPCtrlAct_t Pct_5msCtrlPCtrlAct;
Pct_5msCtrlInitQuickBrkDctFlg_t Pct_5msCtrlInitQuickBrkDctFlg;
Pct_5msCtrlTgtDeltaStkType_t Pct_5msCtrlTgtDeltaStkType;
Pct_5msCtrlPCtrlBoostMod_t Pct_5msCtrlPCtrlBoostMod;
Pct_5msCtrlPCtrlFadeoutSt_t Pct_5msCtrlPCtrlFadeoutSt;
Pct_5msCtrlPCtrlSt_t Pct_5msCtrlPCtrlSt;
Pct_5msCtrlInVlvAllCloseReq_t Pct_5msCtrlInVlvAllCloseReq;
Pct_5msCtrlPChamberVolume_t Pct_5msCtrlPChamberVolume;
Pct_5msCtrlTarDeltaStk_t Pct_5msCtrlTarDeltaStk;
Pct_5msCtrlFinalTarPFromPCtrl_t Pct_5msCtrlFinalTarPFromPCtrl;

uint32 Pct_5msCtrl_Timer_Start;
uint32 Pct_5msCtrl_Timer_Elapsed;
#if ((M_IDB_TEST == ENABLE) &&\
	((M_IDB_TEST_MODE == M_IDB_GEN_TGTSTROKE_TEST) || (M_IDB_TEST_MODE == M_IDB_STROKE_MAP_TEST) || (M_IDB_TEST_MODE == M_IDB_STROKE_MAX_TEST)))
uint8_t ii;
#endif

#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
#define PCT_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PCT_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
#define PCT_5MSCTRL_START_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/** Variable Section (32BIT)**/


#define PCT_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
#define PCT_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PCT_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
#define PCT_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PCT_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
#define PCT_5MSCTRL_START_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/** Variable Section (32BIT)**/


#define PCT_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PCT_5MSCTRL_START_SEC_CODE
#include "Pct_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Pct_5msCtrl_Init(void)
{
    uint16 i;

    /* Initialize internal bus */
    for(i=0;i<10;i++) Pct_5msCtrlBus.Pct_5msCtrlWhlVlvReqAbcInfo.FlIvReqData[i] = 0;   
    for(i=0;i<10;i++) Pct_5msCtrlBus.Pct_5msCtrlWhlVlvReqAbcInfo.FrIvReqData[i] = 0;   
    for(i=0;i<10;i++) Pct_5msCtrlBus.Pct_5msCtrlWhlVlvReqAbcInfo.RlIvReqData[i] = 0;   
    for(i=0;i<10;i++) Pct_5msCtrlBus.Pct_5msCtrlWhlVlvReqAbcInfo.RrIvReqData[i] = 0;   
    Pct_5msCtrlBus.Pct_5msCtrlWhlVlvReqAbcInfo.FlIvReq = 0;
    Pct_5msCtrlBus.Pct_5msCtrlWhlVlvReqAbcInfo.FrIvReq = 0;
    Pct_5msCtrlBus.Pct_5msCtrlWhlVlvReqAbcInfo.RlIvReq = 0;
    Pct_5msCtrlBus.Pct_5msCtrlWhlVlvReqAbcInfo.RrIvReq = 0;
    Pct_5msCtrlBus.Pct_5msCtrlEemFailData.Eem_Fail_SimP = 0;
    Pct_5msCtrlBus.Pct_5msCtrlEemFailData.Eem_Fail_BLS = 0;
    Pct_5msCtrlBus.Pct_5msCtrlPedlTrvlFild1msInfo.PdtFild1ms = 0;
    Pct_5msCtrlBus.Pct_5msCtrlCanRxAccelPedlInfo.AccelPedlVal = 0;
    Pct_5msCtrlBus.Pct_5msCtrlCanRxAccelPedlInfo.AccelPedlValErr = 0;
    Pct_5msCtrlBus.Pct_5msCtrlMotRotgAgSigInfo.StkPosnMeasd = 0;
    Pct_5msCtrlBus.Pct_5msCtrlAbsCtrlInfo.AbsActFlg = 0;
    Pct_5msCtrlBus.Pct_5msCtrlAbsCtrlInfo.AbsDefectFlg = 0;
    Pct_5msCtrlBus.Pct_5msCtrlAbsCtrlInfo.AbsTarPFrntLe = 0;
    Pct_5msCtrlBus.Pct_5msCtrlAbsCtrlInfo.AbsTarPFrntRi = 0;
    Pct_5msCtrlBus.Pct_5msCtrlAbsCtrlInfo.AbsTarPReLe = 0;
    Pct_5msCtrlBus.Pct_5msCtrlAbsCtrlInfo.AbsTarPReRi = 0;
    Pct_5msCtrlBus.Pct_5msCtrlAbsCtrlInfo.FrntWhlSlip = 0;
    Pct_5msCtrlBus.Pct_5msCtrlAbsCtrlInfo.AbsDesTarP = 0;
    Pct_5msCtrlBus.Pct_5msCtrlAbsCtrlInfo.AbsDesTarPReqFlg = 0;
    Pct_5msCtrlBus.Pct_5msCtrlAbsCtrlInfo.AbsCtrlFadeOutFlg = 0;
    Pct_5msCtrlBus.Pct_5msCtrlAvhCtrlInfo.AvhActFlg = 0;
    Pct_5msCtrlBus.Pct_5msCtrlAvhCtrlInfo.AvhDefectFlg = 0;
    Pct_5msCtrlBus.Pct_5msCtrlAvhCtrlInfo.AvhTarP = 0;
    Pct_5msCtrlBus.Pct_5msCtrlEscCtrlInfo.EscActFlg = 0;
    Pct_5msCtrlBus.Pct_5msCtrlEscCtrlInfo.EscDefectFlg = 0;
    Pct_5msCtrlBus.Pct_5msCtrlEscCtrlInfo.EscTarPFrntLe = 0;
    Pct_5msCtrlBus.Pct_5msCtrlEscCtrlInfo.EscTarPFrntRi = 0;
    Pct_5msCtrlBus.Pct_5msCtrlEscCtrlInfo.EscTarPReLe = 0;
    Pct_5msCtrlBus.Pct_5msCtrlEscCtrlInfo.EscTarPReRi = 0;
    Pct_5msCtrlBus.Pct_5msCtrlHsaCtrlInfo.HsaActFlg = 0;
    Pct_5msCtrlBus.Pct_5msCtrlHsaCtrlInfo.HsaDefectFlg = 0;
    Pct_5msCtrlBus.Pct_5msCtrlHsaCtrlInfo.HsaTarP = 0;
    Pct_5msCtrlBus.Pct_5msCtrlStkRecvryCtrlIfInfo.StkRcvrEscAllowedTime = 0;
    Pct_5msCtrlBus.Pct_5msCtrlStkRecvryCtrlIfInfo.StkRcvrAbsAllowedTime = 0;
    Pct_5msCtrlBus.Pct_5msCtrlStkRecvryCtrlIfInfo.StkRcvrTcsAllowedTime = 0;
    Pct_5msCtrlBus.Pct_5msCtrlTcsCtrlInfo.TcsActFlg = 0;
    Pct_5msCtrlBus.Pct_5msCtrlTcsCtrlInfo.TcsDefectFlg = 0;
    Pct_5msCtrlBus.Pct_5msCtrlTcsCtrlInfo.TcsTarPFrntLe = 0;
    Pct_5msCtrlBus.Pct_5msCtrlTcsCtrlInfo.TcsTarPFrntRi = 0;
    Pct_5msCtrlBus.Pct_5msCtrlTcsCtrlInfo.TcsTarPReLe = 0;
    Pct_5msCtrlBus.Pct_5msCtrlTcsCtrlInfo.TcsTarPReRi = 0;
    Pct_5msCtrlBus.Pct_5msCtrlTcsCtrlInfo.BothDrvgWhlBrkCtlReqFlg = 0;
    Pct_5msCtrlBus.Pct_5msCtrlBaseBrkCtrlModInfo.VehStandStillStFlg = 0;
    Pct_5msCtrlBus.Pct_5msCtrlBrkPedlStatusInfo.DrvrIntendToRelsBrk = 0;
    Pct_5msCtrlBus.Pct_5msCtrlBrkPedlStatusInfo.PanicBrkStFlg = 0;
    Pct_5msCtrlBus.Pct_5msCtrlBrkPedlStatusInfo.PedlReldStFlg2 = 0;
    Pct_5msCtrlBus.Pct_5msCtrlBrkPedlStatus1msInfo.PanicBrkSt1msFlg = 0;
    Pct_5msCtrlBus.Pct_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar = 0;
    Pct_5msCtrlBus.Pct_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar = 0;
    Pct_5msCtrlBus.Pct_5msCtrlCircPRateInfo.PrimCircPChgDurg10ms = 0;
    Pct_5msCtrlBus.Pct_5msCtrlCircPRateInfo.SecdCircPChgDurg10ms = 0;
    Pct_5msCtrlBus.Pct_5msCtrlPedlTrvlRate1msInfo.PedlTrvlRate1msMmPerSec = 0;
    Pct_5msCtrlBus.Pct_5msCtrlEstimdWhlPInfo.FrntLeEstimdWhlP = 0;
    Pct_5msCtrlBus.Pct_5msCtrlEstimdWhlPInfo.FrntRiEstimdWhlP = 0;
    Pct_5msCtrlBus.Pct_5msCtrlEstimdWhlPInfo.ReLeEstimdWhlP = 0;
    Pct_5msCtrlBus.Pct_5msCtrlEstimdWhlPInfo.ReRiEstimdWhlP = 0;
    Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlFrntLe = 0;
    Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlFrntRi = 0;
    Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlReLe = 0;
    Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlReRi = 0;
    Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPRateOfWhlFrntLe = 0;
    Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPRateOfWhlFrntRi = 0;
    Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPRateOfWhlReLe = 0;
    Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPRateOfWhlReRi = 0;
    Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.PCtrlPrioOfWhlFrntLe = 0;
    Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.PCtrlPrioOfWhlFrntRi = 0;
    Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.PCtrlPrioOfWhlReLe = 0;
    Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.PCtrlPrioOfWhlReRi = 0;
    Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.CtrlModOfWhlFrntLe = 0;
    Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.CtrlModOfOfWhlFrntRi = 0;
    Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.CtrlModOfWhlReLe = 0;
    Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.CtrlModOfWhlReRi = 0;
    Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.ReqdPCtrlBoostMod = 0;
    Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalDelTarPOfWhlFrntLe = 0;
    Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalDelTarPOfWhlFrntRi = 0;
    Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalDelTarPOfWhlReLe = 0;
    Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalDelTarPOfWhlReRi = 0;
    Pct_5msCtrlBus.Pct_5msCtrlIdbMotPosnInfo.MotTarPosn = 0;
    for(i=0;i<5;i++) Pct_5msCtrlBus.Pct_5msCtrlWhlOutVlvCtrlTarInfo.FlOutVlvCtrlTar[i] = 0;   
    for(i=0;i<5;i++) Pct_5msCtrlBus.Pct_5msCtrlWhlOutVlvCtrlTarInfo.FrOutVlvCtrlTar[i] = 0;   
    for(i=0;i<5;i++) Pct_5msCtrlBus.Pct_5msCtrlWhlOutVlvCtrlTarInfo.RlOutVlvCtrlTar[i] = 0;   
    for(i=0;i<5;i++) Pct_5msCtrlBus.Pct_5msCtrlWhlOutVlvCtrlTarInfo.RrOutVlvCtrlTar[i] = 0;   
    Pct_5msCtrlBus.Pct_5msCtrlWhlPreFillInfo.FlPreFillActFlg = 0;
    Pct_5msCtrlBus.Pct_5msCtrlWhlPreFillInfo.FrPreFillActFlg = 0;
    Pct_5msCtrlBus.Pct_5msCtrlWhlPreFillInfo.RlPreFillActFlg = 0;
    Pct_5msCtrlBus.Pct_5msCtrlWhlPreFillInfo.RrPreFillActFlg = 0;
    Pct_5msCtrlBus.Pct_5msCtrlIdbVlvActInfo.VlvFadeoutEndOK = 0;
    Pct_5msCtrlBus.Pct_5msCtrlIdbVlvActInfo.CutVlvOpenFlg = 0;
    Pct_5msCtrlBus.Pct_5msCtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar = 0;
    Pct_5msCtrlBus.Pct_5msCtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec = 0;
    Pct_5msCtrlBus.Pct_5msCtrlPedlTrvlTagInfo.PedlSigTag = 0;
    Pct_5msCtrlBus.Pct_5msCtrlPistPFildInfo.PistPFild_1_100Bar = 0;
    Pct_5msCtrlBus.Pct_5msCtrlPistPRateInfo.PistPChgDurg10ms = 0;
    Pct_5msCtrlBus.Pct_5msCtrlVlvActtnReqByBaseBrkCtrlrInfo.PedlSimrPGendOnFlg = 0;
    Pct_5msCtrlBus.Pct_5msCtrlWhlSpdFildInfo.WhlSpdFildReLe = 0;
    Pct_5msCtrlBus.Pct_5msCtrlWhlSpdFildInfo.WhlSpdFildReRi = 0;
    Pct_5msCtrlBus.Pct_5msCtrlEemSuspectData.Eem_Suspect_SimP = 0;
    Pct_5msCtrlBus.Pct_5msCtrlEcuModeSts = 0;
    Pct_5msCtrlBus.Pct_5msCtrlMotCtrlMode = 0;
    Pct_5msCtrlBus.Pct_5msCtrlFuncInhibitPctrlSts = 0;
    Pct_5msCtrlBus.Pct_5msCtrlBaseBrkCtrlrActFlg = 0;
    Pct_5msCtrlBus.Pct_5msCtrlBlsFild = 0;
    Pct_5msCtrlBus.Pct_5msCtrlBrkPRednForBaseBrkCtrlr = 0;
    Pct_5msCtrlBus.Pct_5msCtrlMotICtrlFadeOutState = 0;
    Pct_5msCtrlBus.Pct_5msCtrlPreBoostMod = 0;
    Pct_5msCtrlBus.Pct_5msCtrlRgnBrkCtlrBlendgFlg = 0;
    Pct_5msCtrlBus.Pct_5msCtrlVehSpdFild = 0;
    Pct_5msCtrlBus.Pct_5msCtrlStkRecvryStabnEndOK = 0;
    Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.CvVlvSt = 0;
    Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.RlVlvSt = 0;
    Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.PdVlvSt = 0;
    Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.BalVlvSt = 0;
    Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.CvVlvNoiseCtrlSt = 0;
    Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.RlVlvNoiseCtrlSt = 0;
    Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.PdVlvNoiseCtrlSt = 0;
    Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.BalVlvNoiseCtrlSt = 0;
    Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.CvVlvHoldTime = 0;
    Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.RlVlvHoldTime = 0;
    Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.PdVlvHoldTime = 0;
    Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.BalVlvHoldTime = 0;
    Pct_5msCtrlBus.Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvSt = 0;
    Pct_5msCtrlBus.Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvSt = 0;
    Pct_5msCtrlBus.Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SimVlvSt = 0;
    Pct_5msCtrlBus.Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvNoiseCtrlSt = 0;
    Pct_5msCtrlBus.Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvNoiseCtrlSt = 0;
    Pct_5msCtrlBus.Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SimVlvNoiseCtrlSt = 0;
    Pct_5msCtrlBus.Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvHoldTime = 0;
    Pct_5msCtrlBus.Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvHoldTime = 0;
    Pct_5msCtrlBus.Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SimVlvHoldTime = 0;
    Pct_5msCtrlBus.Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.CutVlvLongOpen = 0;
    for(i=0;i<5;i++) Pct_5msCtrlBus.Pct_5msCtrlWhlnVlvCtrlTarInfo.FlInVlvCtrlTar[i] = 0;   
    for(i=0;i<5;i++) Pct_5msCtrlBus.Pct_5msCtrlWhlnVlvCtrlTarInfo.FrInVlvCtrlTar[i] = 0;   
    for(i=0;i<5;i++) Pct_5msCtrlBus.Pct_5msCtrlWhlnVlvCtrlTarInfo.RlInVlvCtrlTar[i] = 0;   
    for(i=0;i<5;i++) Pct_5msCtrlBus.Pct_5msCtrlWhlnVlvCtrlTarInfo.RrInVlvCtrlTar[i] = 0;   
    Pct_5msCtrlBus.Pct_5msCtrlStkRecvryActnIfInfo.RecommendStkRcvrLvl = 0;
    Pct_5msCtrlBus.Pct_5msCtrlStkRecvryActnIfInfo.StkRecvryCtrlState = 0;
    Pct_5msCtrlBus.Pct_5msCtrlStkRecvryActnIfInfo.StkRecvryRequestedTime = 0;
    Pct_5msCtrlBus.Pct_5msCtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlFrntLe = 0;
    Pct_5msCtrlBus.Pct_5msCtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlFrntRi = 0;
    Pct_5msCtrlBus.Pct_5msCtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlReLe = 0;
    Pct_5msCtrlBus.Pct_5msCtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlReRi = 0;
    Pct_5msCtrlBus.Pct_5msCtrlPCtrlAct = 0;
    Pct_5msCtrlBus.Pct_5msCtrlInitQuickBrkDctFlg = 0;
    Pct_5msCtrlBus.Pct_5msCtrlTgtDeltaStkType = 0;
    Pct_5msCtrlBus.Pct_5msCtrlPCtrlBoostMod = 0;
    Pct_5msCtrlBus.Pct_5msCtrlPCtrlFadeoutSt = 0;
    Pct_5msCtrlBus.Pct_5msCtrlPCtrlSt = 0;
    Pct_5msCtrlBus.Pct_5msCtrlInVlvAllCloseReq = 0;
    Pct_5msCtrlBus.Pct_5msCtrlPChamberVolume = 0;
    Pct_5msCtrlBus.Pct_5msCtrlTarDeltaStk = 0;
    Pct_5msCtrlBus.Pct_5msCtrlFinalTarPFromPCtrl = 0;
}

void Pct_5msCtrl(void)
{
    uint16 i;
    uint8_t u8InVlvAllCloseReq = FALSE;
	static uint8_t lapctu8WhlIndexOld = 33;
#if (M_IDB_TEST == ENABLE)
    static uint16_t	FlInVlvCtrlTarOld = 0;
    static uint16_t	FrInVlvCtrlTarOld = 0;
    static uint16_t	RlInVlvCtrlTarOld = 0;
    static uint16_t	RrInVlvCtrlTarOld = 0;
#endif

    Pct_5msCtrl_Timer_Start = STM0_TIM0.U;

    /* Input */
    /* Decomposed structure interface */
    Pct_5msCtrl_Read_Pct_5msCtrlWhlVlvReqAbcInfo_FlIvReqData(&Pct_5msCtrlBus.Pct_5msCtrlWhlVlvReqAbcInfo.FlIvReqData);
    Pct_5msCtrl_Read_Pct_5msCtrlWhlVlvReqAbcInfo_FrIvReqData(&Pct_5msCtrlBus.Pct_5msCtrlWhlVlvReqAbcInfo.FrIvReqData);
    Pct_5msCtrl_Read_Pct_5msCtrlWhlVlvReqAbcInfo_RlIvReqData(&Pct_5msCtrlBus.Pct_5msCtrlWhlVlvReqAbcInfo.RlIvReqData);
    Pct_5msCtrl_Read_Pct_5msCtrlWhlVlvReqAbcInfo_RrIvReqData(&Pct_5msCtrlBus.Pct_5msCtrlWhlVlvReqAbcInfo.RrIvReqData);
    Pct_5msCtrl_Read_Pct_5msCtrlWhlVlvReqAbcInfo_FlIvReq(&Pct_5msCtrlBus.Pct_5msCtrlWhlVlvReqAbcInfo.FlIvReq);
    Pct_5msCtrl_Read_Pct_5msCtrlWhlVlvReqAbcInfo_FrIvReq(&Pct_5msCtrlBus.Pct_5msCtrlWhlVlvReqAbcInfo.FrIvReq);
    Pct_5msCtrl_Read_Pct_5msCtrlWhlVlvReqAbcInfo_RlIvReq(&Pct_5msCtrlBus.Pct_5msCtrlWhlVlvReqAbcInfo.RlIvReq);
    Pct_5msCtrl_Read_Pct_5msCtrlWhlVlvReqAbcInfo_RrIvReq(&Pct_5msCtrlBus.Pct_5msCtrlWhlVlvReqAbcInfo.RrIvReq);

    /* Decomposed structure interface */
    Pct_5msCtrl_Read_Pct_5msCtrlEemFailData_Eem_Fail_SimP(&Pct_5msCtrlBus.Pct_5msCtrlEemFailData.Eem_Fail_SimP);
    Pct_5msCtrl_Read_Pct_5msCtrlEemFailData_Eem_Fail_BLS(&Pct_5msCtrlBus.Pct_5msCtrlEemFailData.Eem_Fail_BLS);

    Pct_5msCtrl_Read_Pct_5msCtrlPedlTrvlFild1msInfo(&Pct_5msCtrlBus.Pct_5msCtrlPedlTrvlFild1msInfo);
    /*==============================================================================
    * Members of structure Pct_5msCtrlPedlTrvlFild1msInfo 
     : Pct_5msCtrlPedlTrvlFild1msInfo.PdtFild1ms;
     =============================================================================*/
    
    Pct_5msCtrl_Read_Pct_5msCtrlCanRxAccelPedlInfo(&Pct_5msCtrlBus.Pct_5msCtrlCanRxAccelPedlInfo);
    /*==============================================================================
    * Members of structure Pct_5msCtrlCanRxAccelPedlInfo 
     : Pct_5msCtrlCanRxAccelPedlInfo.AccelPedlVal;
     : Pct_5msCtrlCanRxAccelPedlInfo.AccelPedlValErr;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Pct_5msCtrl_Read_Pct_5msCtrlMotRotgAgSigInfo_StkPosnMeasd(&Pct_5msCtrlBus.Pct_5msCtrlMotRotgAgSigInfo.StkPosnMeasd);

    /* Decomposed structure interface */
    Pct_5msCtrl_Read_Pct_5msCtrlAbsCtrlInfo_AbsActFlg(&Pct_5msCtrlBus.Pct_5msCtrlAbsCtrlInfo.AbsActFlg);
    Pct_5msCtrl_Read_Pct_5msCtrlAbsCtrlInfo_AbsDefectFlg(&Pct_5msCtrlBus.Pct_5msCtrlAbsCtrlInfo.AbsDefectFlg);
    Pct_5msCtrl_Read_Pct_5msCtrlAbsCtrlInfo_AbsTarPFrntLe(&Pct_5msCtrlBus.Pct_5msCtrlAbsCtrlInfo.AbsTarPFrntLe);
    Pct_5msCtrl_Read_Pct_5msCtrlAbsCtrlInfo_AbsTarPFrntRi(&Pct_5msCtrlBus.Pct_5msCtrlAbsCtrlInfo.AbsTarPFrntRi);
    Pct_5msCtrl_Read_Pct_5msCtrlAbsCtrlInfo_AbsTarPReLe(&Pct_5msCtrlBus.Pct_5msCtrlAbsCtrlInfo.AbsTarPReLe);
    Pct_5msCtrl_Read_Pct_5msCtrlAbsCtrlInfo_AbsTarPReRi(&Pct_5msCtrlBus.Pct_5msCtrlAbsCtrlInfo.AbsTarPReRi);
    Pct_5msCtrl_Read_Pct_5msCtrlAbsCtrlInfo_FrntWhlSlip(&Pct_5msCtrlBus.Pct_5msCtrlAbsCtrlInfo.FrntWhlSlip);
    Pct_5msCtrl_Read_Pct_5msCtrlAbsCtrlInfo_AbsDesTarP(&Pct_5msCtrlBus.Pct_5msCtrlAbsCtrlInfo.AbsDesTarP);
    Pct_5msCtrl_Read_Pct_5msCtrlAbsCtrlInfo_AbsDesTarPReqFlg(&Pct_5msCtrlBus.Pct_5msCtrlAbsCtrlInfo.AbsDesTarPReqFlg);
    Pct_5msCtrl_Read_Pct_5msCtrlAbsCtrlInfo_AbsCtrlFadeOutFlg(&Pct_5msCtrlBus.Pct_5msCtrlAbsCtrlInfo.AbsCtrlFadeOutFlg);

    Pct_5msCtrl_Read_Pct_5msCtrlAvhCtrlInfo(&Pct_5msCtrlBus.Pct_5msCtrlAvhCtrlInfo);
    /*==============================================================================
    * Members of structure Pct_5msCtrlAvhCtrlInfo 
     : Pct_5msCtrlAvhCtrlInfo.AvhActFlg;
     : Pct_5msCtrlAvhCtrlInfo.AvhDefectFlg;
     : Pct_5msCtrlAvhCtrlInfo.AvhTarP;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Pct_5msCtrl_Read_Pct_5msCtrlEscCtrlInfo_EscActFlg(&Pct_5msCtrlBus.Pct_5msCtrlEscCtrlInfo.EscActFlg);
    Pct_5msCtrl_Read_Pct_5msCtrlEscCtrlInfo_EscDefectFlg(&Pct_5msCtrlBus.Pct_5msCtrlEscCtrlInfo.EscDefectFlg);
    Pct_5msCtrl_Read_Pct_5msCtrlEscCtrlInfo_EscTarPFrntLe(&Pct_5msCtrlBus.Pct_5msCtrlEscCtrlInfo.EscTarPFrntLe);
    Pct_5msCtrl_Read_Pct_5msCtrlEscCtrlInfo_EscTarPFrntRi(&Pct_5msCtrlBus.Pct_5msCtrlEscCtrlInfo.EscTarPFrntRi);
    Pct_5msCtrl_Read_Pct_5msCtrlEscCtrlInfo_EscTarPReLe(&Pct_5msCtrlBus.Pct_5msCtrlEscCtrlInfo.EscTarPReLe);
    Pct_5msCtrl_Read_Pct_5msCtrlEscCtrlInfo_EscTarPReRi(&Pct_5msCtrlBus.Pct_5msCtrlEscCtrlInfo.EscTarPReRi);

    Pct_5msCtrl_Read_Pct_5msCtrlHsaCtrlInfo(&Pct_5msCtrlBus.Pct_5msCtrlHsaCtrlInfo);
    /*==============================================================================
    * Members of structure Pct_5msCtrlHsaCtrlInfo 
     : Pct_5msCtrlHsaCtrlInfo.HsaActFlg;
     : Pct_5msCtrlHsaCtrlInfo.HsaDefectFlg;
     : Pct_5msCtrlHsaCtrlInfo.HsaTarP;
     =============================================================================*/
    
    Pct_5msCtrl_Read_Pct_5msCtrlStkRecvryCtrlIfInfo(&Pct_5msCtrlBus.Pct_5msCtrlStkRecvryCtrlIfInfo);
    /*==============================================================================
    * Members of structure Pct_5msCtrlStkRecvryCtrlIfInfo 
     : Pct_5msCtrlStkRecvryCtrlIfInfo.StkRcvrEscAllowedTime;
     : Pct_5msCtrlStkRecvryCtrlIfInfo.StkRcvrAbsAllowedTime;
     : Pct_5msCtrlStkRecvryCtrlIfInfo.StkRcvrTcsAllowedTime;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Pct_5msCtrl_Read_Pct_5msCtrlTcsCtrlInfo_TcsActFlg(&Pct_5msCtrlBus.Pct_5msCtrlTcsCtrlInfo.TcsActFlg);
    Pct_5msCtrl_Read_Pct_5msCtrlTcsCtrlInfo_TcsDefectFlg(&Pct_5msCtrlBus.Pct_5msCtrlTcsCtrlInfo.TcsDefectFlg);
    Pct_5msCtrl_Read_Pct_5msCtrlTcsCtrlInfo_TcsTarPFrntLe(&Pct_5msCtrlBus.Pct_5msCtrlTcsCtrlInfo.TcsTarPFrntLe);
    Pct_5msCtrl_Read_Pct_5msCtrlTcsCtrlInfo_TcsTarPFrntRi(&Pct_5msCtrlBus.Pct_5msCtrlTcsCtrlInfo.TcsTarPFrntRi);
    Pct_5msCtrl_Read_Pct_5msCtrlTcsCtrlInfo_TcsTarPReLe(&Pct_5msCtrlBus.Pct_5msCtrlTcsCtrlInfo.TcsTarPReLe);
    Pct_5msCtrl_Read_Pct_5msCtrlTcsCtrlInfo_TcsTarPReRi(&Pct_5msCtrlBus.Pct_5msCtrlTcsCtrlInfo.TcsTarPReRi);
    Pct_5msCtrl_Read_Pct_5msCtrlTcsCtrlInfo_BothDrvgWhlBrkCtlReqFlg(&Pct_5msCtrlBus.Pct_5msCtrlTcsCtrlInfo.BothDrvgWhlBrkCtlReqFlg);

    /* Decomposed structure interface */
    Pct_5msCtrl_Read_Pct_5msCtrlBaseBrkCtrlModInfo_VehStandStillStFlg(&Pct_5msCtrlBus.Pct_5msCtrlBaseBrkCtrlModInfo.VehStandStillStFlg);

    /* Decomposed structure interface */
    Pct_5msCtrl_Read_Pct_5msCtrlBrkPedlStatusInfo_DrvrIntendToRelsBrk(&Pct_5msCtrlBus.Pct_5msCtrlBrkPedlStatusInfo.DrvrIntendToRelsBrk);
    Pct_5msCtrl_Read_Pct_5msCtrlBrkPedlStatusInfo_PanicBrkStFlg(&Pct_5msCtrlBus.Pct_5msCtrlBrkPedlStatusInfo.PanicBrkStFlg);
    Pct_5msCtrl_Read_Pct_5msCtrlBrkPedlStatusInfo_PedlReldStFlg2(&Pct_5msCtrlBus.Pct_5msCtrlBrkPedlStatusInfo.PedlReldStFlg2);

    Pct_5msCtrl_Read_Pct_5msCtrlBrkPedlStatus1msInfo(&Pct_5msCtrlBus.Pct_5msCtrlBrkPedlStatus1msInfo);
    /*==============================================================================
    * Members of structure Pct_5msCtrlBrkPedlStatus1msInfo 
     : Pct_5msCtrlBrkPedlStatus1msInfo.PanicBrkSt1msFlg;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Pct_5msCtrl_Read_Pct_5msCtrlCircPFildInfo_PrimCircPFild_1_100Bar(&Pct_5msCtrlBus.Pct_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar);
    Pct_5msCtrl_Read_Pct_5msCtrlCircPFildInfo_SecdCircPFild_1_100Bar(&Pct_5msCtrlBus.Pct_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar);

    /* Decomposed structure interface */
    Pct_5msCtrl_Read_Pct_5msCtrlCircPRateInfo_PrimCircPChgDurg10ms(&Pct_5msCtrlBus.Pct_5msCtrlCircPRateInfo.PrimCircPChgDurg10ms);
    Pct_5msCtrl_Read_Pct_5msCtrlCircPRateInfo_SecdCircPChgDurg10ms(&Pct_5msCtrlBus.Pct_5msCtrlCircPRateInfo.SecdCircPChgDurg10ms);

    Pct_5msCtrl_Read_Pct_5msCtrlPedlTrvlRate1msInfo(&Pct_5msCtrlBus.Pct_5msCtrlPedlTrvlRate1msInfo);
    /*==============================================================================
    * Members of structure Pct_5msCtrlPedlTrvlRate1msInfo 
     : Pct_5msCtrlPedlTrvlRate1msInfo.PedlTrvlRate1msMmPerSec;
     =============================================================================*/
    
    Pct_5msCtrl_Read_Pct_5msCtrlEstimdWhlPInfo(&Pct_5msCtrlBus.Pct_5msCtrlEstimdWhlPInfo);
    /*==============================================================================
    * Members of structure Pct_5msCtrlEstimdWhlPInfo 
     : Pct_5msCtrlEstimdWhlPInfo.FrntLeEstimdWhlP;
     : Pct_5msCtrlEstimdWhlPInfo.FrntRiEstimdWhlP;
     : Pct_5msCtrlEstimdWhlPInfo.ReLeEstimdWhlP;
     : Pct_5msCtrlEstimdWhlPInfo.ReRiEstimdWhlP;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_FinalTarPOfWhlFrntLe(&Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlFrntLe);
    Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_FinalTarPOfWhlFrntRi(&Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlFrntRi);
    Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_FinalTarPOfWhlReLe(&Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlReLe);
    Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_FinalTarPOfWhlReRi(&Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlReRi);
    Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_FinalTarPRateOfWhlFrntLe(&Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPRateOfWhlFrntLe);
    Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_FinalTarPRateOfWhlFrntRi(&Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPRateOfWhlFrntRi);
    Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_FinalTarPRateOfWhlReLe(&Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPRateOfWhlReLe);
    Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_FinalTarPRateOfWhlReRi(&Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPRateOfWhlReRi);
    Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_PCtrlPrioOfWhlFrntLe(&Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.PCtrlPrioOfWhlFrntLe);
    Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_PCtrlPrioOfWhlFrntRi(&Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.PCtrlPrioOfWhlFrntRi);
    Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_PCtrlPrioOfWhlReLe(&Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.PCtrlPrioOfWhlReLe);
    Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_PCtrlPrioOfWhlReRi(&Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.PCtrlPrioOfWhlReRi);
    Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_CtrlModOfWhlFrntLe(&Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.CtrlModOfWhlFrntLe);
    Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_CtrlModOfOfWhlFrntRi(&Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.CtrlModOfOfWhlFrntRi);
    Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_CtrlModOfWhlReLe(&Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.CtrlModOfWhlReLe);
    Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_CtrlModOfWhlReRi(&Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.CtrlModOfWhlReRi);
    Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_ReqdPCtrlBoostMod(&Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.ReqdPCtrlBoostMod);
    Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_FinalDelTarPOfWhlFrntLe(&Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalDelTarPOfWhlFrntLe);
    Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_FinalDelTarPOfWhlFrntRi(&Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalDelTarPOfWhlFrntRi);
    Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_FinalDelTarPOfWhlReLe(&Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalDelTarPOfWhlReLe);
    Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_FinalDelTarPOfWhlReRi(&Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalDelTarPOfWhlReRi);

    Pct_5msCtrl_Read_Pct_5msCtrlIdbMotPosnInfo(&Pct_5msCtrlBus.Pct_5msCtrlIdbMotPosnInfo);
    /*==============================================================================
    * Members of structure Pct_5msCtrlIdbMotPosnInfo 
     : Pct_5msCtrlIdbMotPosnInfo.MotTarPosn;
     =============================================================================*/
    
    Pct_5msCtrl_Read_Pct_5msCtrlWhlOutVlvCtrlTarInfo(&Pct_5msCtrlBus.Pct_5msCtrlWhlOutVlvCtrlTarInfo);
    /*==============================================================================
    * Members of structure Pct_5msCtrlWhlOutVlvCtrlTarInfo 
     : Pct_5msCtrlWhlOutVlvCtrlTarInfo.FlOutVlvCtrlTar;
     : Pct_5msCtrlWhlOutVlvCtrlTarInfo.FrOutVlvCtrlTar;
     : Pct_5msCtrlWhlOutVlvCtrlTarInfo.RlOutVlvCtrlTar;
     : Pct_5msCtrlWhlOutVlvCtrlTarInfo.RrOutVlvCtrlTar;
     =============================================================================*/
    
    Pct_5msCtrl_Read_Pct_5msCtrlWhlPreFillInfo(&Pct_5msCtrlBus.Pct_5msCtrlWhlPreFillInfo);
    /*==============================================================================
    * Members of structure Pct_5msCtrlWhlPreFillInfo 
     : Pct_5msCtrlWhlPreFillInfo.FlPreFillActFlg;
     : Pct_5msCtrlWhlPreFillInfo.FrPreFillActFlg;
     : Pct_5msCtrlWhlPreFillInfo.RlPreFillActFlg;
     : Pct_5msCtrlWhlPreFillInfo.RrPreFillActFlg;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Pct_5msCtrl_Read_Pct_5msCtrlIdbVlvActInfo(&Pct_5msCtrlBus.Pct_5msCtrlIdbVlvActInfo);
/*
    Pct_5msCtrl_Read_Pct_5msCtrlIdbVlvActInfo_VlvFadeoutEndOK(&Pct_5msCtrlBus.Pct_5msCtrlIdbVlvActInfo.VlvFadeoutEndOK);
    Pct_5msCtrl_Read_Pct_5msCtrlIdbVlvActInfo_CutVlvOpenFlg(&Pct_5msCtrlBus.Pct_5msCtrlIdbVlvActInfo.CutVlvOpenFlg);
*/

    /* Decomposed structure interface */
    Pct_5msCtrl_Read_Pct_5msCtrlPedlSimrPFildInfo_PedlSimrPFild_1_100Bar(&Pct_5msCtrlBus.Pct_5msCtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar);

    /* Decomposed structure interface */
    Pct_5msCtrl_Read_Pct_5msCtrlPedlTrvlRateInfo_PedlTrvlRateMilliMtrPerSec(&Pct_5msCtrlBus.Pct_5msCtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec);

    /* Decomposed structure interface */
    Pct_5msCtrl_Read_Pct_5msCtrlPedlTrvlTagInfo_PedlSigTag(&Pct_5msCtrlBus.Pct_5msCtrlPedlTrvlTagInfo.PedlSigTag);

    /* Decomposed structure interface */
    Pct_5msCtrl_Read_Pct_5msCtrlPistPFildInfo_PistPFild_1_100Bar(&Pct_5msCtrlBus.Pct_5msCtrlPistPFildInfo.PistPFild_1_100Bar);

    /* Decomposed structure interface */
    Pct_5msCtrl_Read_Pct_5msCtrlPistPRateInfo_PistPChgDurg10ms(&Pct_5msCtrlBus.Pct_5msCtrlPistPRateInfo.PistPChgDurg10ms);

    Pct_5msCtrl_Read_Pct_5msCtrlVlvActtnReqByBaseBrkCtrlrInfo(&Pct_5msCtrlBus.Pct_5msCtrlVlvActtnReqByBaseBrkCtrlrInfo);
    /*==============================================================================
    * Members of structure Pct_5msCtrlVlvActtnReqByBaseBrkCtrlrInfo 
     : Pct_5msCtrlVlvActtnReqByBaseBrkCtrlrInfo.PedlSimrPGendOnFlg;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Pct_5msCtrl_Read_Pct_5msCtrlWhlSpdFildInfo_WhlSpdFildReLe(&Pct_5msCtrlBus.Pct_5msCtrlWhlSpdFildInfo.WhlSpdFildReLe);
    Pct_5msCtrl_Read_Pct_5msCtrlWhlSpdFildInfo_WhlSpdFildReRi(&Pct_5msCtrlBus.Pct_5msCtrlWhlSpdFildInfo.WhlSpdFildReRi);

    /* Decomposed structure interface */
    Pct_5msCtrl_Read_Pct_5msCtrlEemSuspectData_Eem_Suspect_SimP(&Pct_5msCtrlBus.Pct_5msCtrlEemSuspectData.Eem_Suspect_SimP);

    Pct_5msCtrl_Read_Pct_5msCtrlEcuModeSts(&Pct_5msCtrlBus.Pct_5msCtrlEcuModeSts);
    Pct_5msCtrl_Read_Pct_5msCtrlMotCtrlMode(&Pct_5msCtrlBus.Pct_5msCtrlMotCtrlMode);
    Pct_5msCtrl_Read_Pct_5msCtrlFuncInhibitPctrlSts(&Pct_5msCtrlBus.Pct_5msCtrlFuncInhibitPctrlSts);
    Pct_5msCtrl_Read_Pct_5msCtrlBaseBrkCtrlrActFlg(&Pct_5msCtrlBus.Pct_5msCtrlBaseBrkCtrlrActFlg);
    Pct_5msCtrl_Read_Pct_5msCtrlBlsFild(&Pct_5msCtrlBus.Pct_5msCtrlBlsFild);
    Pct_5msCtrl_Read_Pct_5msCtrlBrkPRednForBaseBrkCtrlr(&Pct_5msCtrlBus.Pct_5msCtrlBrkPRednForBaseBrkCtrlr);
    Pct_5msCtrl_Read_Pct_5msCtrlMotICtrlFadeOutState(&Pct_5msCtrlBus.Pct_5msCtrlMotICtrlFadeOutState);
    Pct_5msCtrl_Read_Pct_5msCtrlPreBoostMod(&Pct_5msCtrlBus.Pct_5msCtrlPreBoostMod);
    Pct_5msCtrl_Read_Pct_5msCtrlRgnBrkCtlrBlendgFlg(&Pct_5msCtrlBus.Pct_5msCtrlRgnBrkCtlrBlendgFlg);
    Pct_5msCtrl_Read_Pct_5msCtrlVehSpdFild(&Pct_5msCtrlBus.Pct_5msCtrlVehSpdFild);
    Pct_5msCtrl_Read_Pct_5msCtrlStkRecvryStabnEndOK(&Pct_5msCtrlBus.Pct_5msCtrlStkRecvryStabnEndOK);

    /* Process */
#if (M_IDB_TEST == DISABLE)

    LAPCT_vCallStkRcvrMain();
    LAIDB_vCallMultiplexCtrl();
    LAPCT_vDecidePressCtrlMode();
    LAPCT_vDecidePressSourceMain();
    LAPCT_PedalFeelGenVlvCtrlMode();
    LAPCT_PressureVlvCtrlMode();
    LAPCT_vDecideMtrCtrlMode();

    if(U16_WHL_SWITCH_DELAY_TIME > 0)
    {
    	u8InVlvAllCloseReq = ((PressCTRLLocalBus.u8WhlCtrlIndex == 0) && (lapctu8WhlIndexOld == 0) && (PressCTRLLocalBus.s8PCtrlState == S8_PRESS_BOOST)) ? TRUE : FALSE;
	}
	else
	{
		u8InVlvAllCloseReq = ((PressCTRLLocalBus.u8WhlCtrlIndex == 0) && (PressCTRLLocalBus.s8PCtrlState == S8_PRESS_BOOST)) ? TRUE : FALSE;
	}

    Pct_5msCtrlBus.Pct_5msCtrlInVlvAllCloseReq = u8InVlvAllCloseReq;

    lapctu8WhlIndexOld = PressCTRLLocalBus.u8WhlCtrlIndex;

#else	/* (M_IDB_TEST == ENABLE) */
    L_vCallTestIDB();

	Pct_5msCtrlBus.Pct_5msCtrlVlvActtnReqByBaseBrkCtrlrInfo.PedlSimrPGendOnFlg = (stIdbTest_Output.m_ls16TestTargetPress > 0)?TRUE : FALSE;

	Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlFrntLe 		= (stIdbTest_Output.m_lstTestOverrideConfig.u1OverrideWheelTargetPressure == TRUE) ? stIdbTest_Output.m_ls16TestTP_FL : Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlFrntLe;
	Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlFrntRi 		= (stIdbTest_Output.m_lstTestOverrideConfig.u1OverrideWheelTargetPressure == TRUE) ? stIdbTest_Output.m_ls16TestTP_FR : Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlFrntRi;
	Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlReLe   		= (stIdbTest_Output.m_lstTestOverrideConfig.u1OverrideWheelTargetPressure == TRUE) ? stIdbTest_Output.m_ls16TestTP_RL : Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlReLe;
	Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlReRi   		= (stIdbTest_Output.m_lstTestOverrideConfig.u1OverrideWheelTargetPressure == TRUE) ? stIdbTest_Output.m_ls16TestTP_RR : Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlReRi;
	Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPRateOfWhlFrntLe 	= (stIdbTest_Output.m_lstTestOverrideConfig.u1OverrideWheelTargetPressure == TRUE) ? stIdbTest_Output.m_ls16TestTPRate_FL : Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPRateOfWhlFrntLe;
	Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPRateOfWhlFrntRi 	= (stIdbTest_Output.m_lstTestOverrideConfig.u1OverrideWheelTargetPressure == TRUE) ? stIdbTest_Output.m_ls16TestTPRate_FR : Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPRateOfWhlFrntRi;
	Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPRateOfWhlReLe   	= (stIdbTest_Output.m_lstTestOverrideConfig.u1OverrideWheelTargetPressure == TRUE) ? stIdbTest_Output.m_ls16TestTPRate_RL : Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPRateOfWhlReLe;
	Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPRateOfWhlReRi   	= (stIdbTest_Output.m_lstTestOverrideConfig.u1OverrideWheelTargetPressure == TRUE) ? stIdbTest_Output.m_ls16TestTPRate_RR : Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPRateOfWhlReRi;
	Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.CtrlModOfWhlFrntLe   		= (stIdbTest_Output.m_lstTestOverrideConfig.u1OverrideWheelTargetPressure == TRUE) ? ((stIdbTest_Output.m_ls16TestTP_FL > 0) ? 20 : 0) : Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.CtrlModOfWhlFrntLe;
	Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.CtrlModOfOfWhlFrntRi 		= (stIdbTest_Output.m_lstTestOverrideConfig.u1OverrideWheelTargetPressure == TRUE) ? ((stIdbTest_Output.m_ls16TestTP_FR > 0) ? 20 : 0) : Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.CtrlModOfOfWhlFrntRi;
	Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.CtrlModOfWhlReLe	 		= (stIdbTest_Output.m_lstTestOverrideConfig.u1OverrideWheelTargetPressure == TRUE) ? ((stIdbTest_Output.m_ls16TestTP_RL > 0) ? 20 : 0) : Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.CtrlModOfWhlReLe;
	Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.CtrlModOfWhlReRi	 		= (stIdbTest_Output.m_lstTestOverrideConfig.u1OverrideWheelTargetPressure == TRUE) ? ((stIdbTest_Output.m_ls16TestTP_RR > 0) ? 20 : 0) : Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.CtrlModOfWhlReRi;

	LAPCT_vCallStkRcvrMain();
	Pct_5msCtrlBus.Pct_5msCtrlStkRecvryActnIfInfo.StkRecvryCtrlState = S8_STRK_RCVR_READY_STATE;

	LAIDB_vCallMultiplexCtrl();

	PressCTRLLocalBus.s16MuxPwrPistonTarP 			= (stIdbTest_Output.m_lstTestOverrideConfig.u1OverrideTargetPressure == TRUE) ? stIdbTest_Output.m_ls16TestTargetPress : PressCTRLLocalBus.s16MuxPwrPistonTarP;
	PressCTRLLocalBus.s16MuxPwrPistonTarDP			= (stIdbTest_Output.m_lstTestOverrideConfig.u1OverrideTargetPressure == TRUE) ? stIdbTest_Output.m_ls16TestTargetPressDel : PressCTRLLocalBus.s16MuxPwrPistonTarDP;
	PressCTRLLocalBus.s16MuxPwrPistonTarPRate		= (stIdbTest_Output.m_lstTestOverrideConfig.u1OverrideTargetPressure == TRUE) ? stIdbTest_Output.m_ls16TestTargetPressRate : PressCTRLLocalBus.s16MuxPwrPistonTarPRate;
	Pct_5msCtrlBus.Pct_5msCtrlFinalTarPFromPCtrl	= (int32_t) PressCTRLLocalBus.s16MuxPwrPistonTarP;
	PressCTRLLocalBus.u8PCtrlAct 					= (stIdbTest_Output.m_lstTestOverrideConfig.u1OverrideTargetPressure == TRUE) ? stIdbTest_Output.m_lu8PCtrlActFlg : PressCTRLLocalBus.u8PCtrlAct;
	PressCTRLLocalBus.s16ReqdPCtrlBoostMod 			= (stIdbTest_Output.m_lstTestOverrideConfig.u1OverrideReqBoostMode == TRUE) ? stIdbTest_Output.m_ls8ReqBoostMode : PressCTRLLocalBus.s16ReqdPCtrlBoostMod;

	LAPCT_vDecidePressCtrlMode();

	Pct_5msCtrlBus.Pct_5msCtrlPCtrlSt      			= (stIdbTest_Output.m_lstTestOverrideConfig.u1OverridePCtrlState == TRUE) ? (int32_t)stIdbTest_Output.m_ls8PCtrlState : Pct_5msCtrlBus.Pct_5msCtrlPCtrlSt;
	Pct_5msCtrlBus.Pct_5msCtrlPCtrlBoostMod			= (stIdbTest_Output.m_lstTestOverrideConfig.u1OverridePCtrlBoostMode == TRUE) ? stIdbTest_Output.m_ls8PCtrlBoostMode : PressCTRLLocalBus.s8PCtrlBoostMode;
   	PressCTRLLocalBus.s8PCtrlState     				= Pct_5msCtrlBus.Pct_5msCtrlPCtrlSt;
   	PressCTRLLocalBus.s8PCtrlBoostMode 				= Pct_5msCtrlBus.Pct_5msCtrlPCtrlBoostMod;
	LAPCT_vDecidePressSourceMain();

	PressCTRLLocalBus.s8PChamberVolume 				= (stIdbTest_Output.m_lstTestOverrideConfig.u1OverrideChamberVolumeVlv == TRUE) ? stIdbTest_Output.m_lss8VLVPChamberVolume : PressCTRLLocalBus.s8PChamberVolume;
	LAPCT_PedalFeelGenVlvCtrlMode();
	LAPCT_PressureVlvCtrlMode();

	PressCTRLLocalBus.s8PChamberVolume = (stIdbTest_Output.m_lstTestOverrideConfig.u1OverrideChamberVolumeMtr == TRUE) ? stIdbTest_Output.m_lss8MTRPChamberVolume : PressCTRLLocalBus.s8PChamberVolume;
	Pct_5msCtrlBus.Pct_5msCtrlPChamberVolume = PressCTRLLocalBus.s8PChamberVolume;
	LAPCT_vDecideMtrCtrlMode();

	Pct_5msCtrlBus.Pct_5msCtrlTarDeltaStk = (stIdbTest_Output.m_lstTestOverrideConfig.u1OverrideDeltaStroke == TRUE)? stIdbTest_Output.m_ls16TestTgtDeltaStroke : Pct_5msCtrlBus.Pct_5msCtrlTarDeltaStk;

	Pct_5msCtrlBus.Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvSt   		 = (stIdbTest_Output.m_lstTestOverrideConfig.u1OverrideCut1VlvCtrl == TRUE) ? stIdbTest_Output.m_lsTest_CUT1.lau8VlvCtrlState  : Pct_5msCtrlBus.Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvSt;
	Pct_5msCtrlBus.Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvNoiseCtrlSt = (stIdbTest_Output.m_lstTestOverrideConfig.u1OverrideCut1VlvCtrl == TRUE) ? stIdbTest_Output.m_lsTest_CUT1.lau8VlvCtrlRespLv : Pct_5msCtrlBus.Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvNoiseCtrlSt;

	Pct_5msCtrlBus.Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvSt 		 = (stIdbTest_Output.m_lstTestOverrideConfig.u1OverrideCut2VlvCtrl == TRUE) ? stIdbTest_Output.m_lsTest_CUT2.lau8VlvCtrlState  : Pct_5msCtrlBus.Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvSt;
	Pct_5msCtrlBus.Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvNoiseCtrlSt = (stIdbTest_Output.m_lstTestOverrideConfig.u1OverrideCut2VlvCtrl == TRUE) ? stIdbTest_Output.m_lsTest_CUT2.lau8VlvCtrlRespLv : Pct_5msCtrlBus.Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvNoiseCtrlSt;

	Pct_5msCtrlBus.Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SimVlvSt 		 = (stIdbTest_Output.m_lstTestOverrideConfig.u1OverrideSimVlvCtrl == TRUE) ? stIdbTest_Output.m_lsTest_SIM.lau8VlvCtrlState  : Pct_5msCtrlBus.Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SimVlvSt;
	Pct_5msCtrlBus.Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SimVlvNoiseCtrlSt = (stIdbTest_Output.m_lstTestOverrideConfig.u1OverrideSimVlvCtrl == TRUE) ? stIdbTest_Output.m_lsTest_SIM.lau8VlvCtrlRespLv : Pct_5msCtrlBus.Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SimVlvNoiseCtrlSt;

	Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.CvVlvSt  = (stIdbTest_Output.m_lstTestOverrideConfig.u1OverrideCvVlvCtrl  == TRUE) ? stIdbTest_Output.m_lsTest_CV.lau8VlvCtrlState   : Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.CvVlvSt;
	Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.RlVlvSt  = (stIdbTest_Output.m_lstTestOverrideConfig.u1OverrideRelVlvCtrl == TRUE) ? stIdbTest_Output.m_lsTest_RELV.lau8VlvCtrlState : Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.RlVlvSt;
	Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.PdVlvSt  = (stIdbTest_Output.m_lstTestOverrideConfig.u1OverridePdVlvCtrl  == TRUE) ? stIdbTest_Output.m_lsTest_PDV.lau8VlvCtrlState  : Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.PdVlvSt;
	Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.BalVlvSt = (stIdbTest_Output.m_lstTestOverrideConfig.u1OverrideBalVlvCtrl == TRUE) ? stIdbTest_Output.m_lsTest_BalV.lau8VlvCtrlState : Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.BalVlvSt;

	Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.CvVlvNoiseCtrlSt  = (stIdbTest_Output.m_lstTestOverrideConfig.u1OverrideCvVlvCtrl  == TRUE) ? stIdbTest_Output.m_lsTest_CV.lau8VlvCtrlRespLv   : Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.CvVlvNoiseCtrlSt;
	Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.RlVlvNoiseCtrlSt  = (stIdbTest_Output.m_lstTestOverrideConfig.u1OverrideRelVlvCtrl == TRUE) ? stIdbTest_Output.m_lsTest_RELV.lau8VlvCtrlRespLv : Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.RlVlvNoiseCtrlSt;
	Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.PdVlvNoiseCtrlSt  = (stIdbTest_Output.m_lstTestOverrideConfig.u1OverridePdVlvCtrl  == TRUE) ? stIdbTest_Output.m_lsTest_PDV.lau8VlvCtrlRespLv  : Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.PdVlvNoiseCtrlSt;
	Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.BalVlvNoiseCtrlSt = (stIdbTest_Output.m_lstTestOverrideConfig.u1OverrideBalVlvCtrl == TRUE) ? stIdbTest_Output.m_lsTest_BalV.lau8VlvCtrlRespLv : Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.BalVlvNoiseCtrlSt;

	/* Wheel Inlet Valve Override - Begin */
	if (stIdbTest_Output.m_lstTestOverrideConfig.u1OverrideWheelInVlvCtrl == TRUE)
	{
		if(stIdbTest_Output.m_lu8TestInletActFlg_FL == TRUE)
		{
			for(ii=0; ii<5; ii++){Pct_5msCtrlBus.Pct_5msCtrlWhlnVlvCtrlTarInfo.FlInVlvCtrlTar[ii] = 1000;}
		}
		else
		{
			for(ii=0; ii<5; ii++){Pct_5msCtrlBus.Pct_5msCtrlWhlnVlvCtrlTarInfo.FlInVlvCtrlTar[ii] = 0;}
		}
		if(stIdbTest_Output.m_lu8TestInletActFlg_FR == TRUE)
		{
			for(ii=0; ii<5; ii++){Pct_5msCtrlBus.Pct_5msCtrlWhlnVlvCtrlTarInfo.FrInVlvCtrlTar[ii] = 1000;}
		}
		else
		{
			for(ii=0; ii<5; ii++){Pct_5msCtrlBus.Pct_5msCtrlWhlnVlvCtrlTarInfo.FrInVlvCtrlTar[ii] = 0;}
		}
		if(stIdbTest_Output.m_lu8TestInletActFlg_RL == TRUE)
		{
			for(ii=0; ii<5; ii++){Pct_5msCtrlBus.Pct_5msCtrlWhlnVlvCtrlTarInfo.RlInVlvCtrlTar[ii] = 1000;}
		}
		else
		{
			for(ii=0; ii<5; ii++){Pct_5msCtrlBus.Pct_5msCtrlWhlnVlvCtrlTarInfo.RlInVlvCtrlTar[ii] = 0;}
		}
		if(stIdbTest_Output.m_lu8TestInletActFlg_RR == TRUE)
		{
			for(ii=0; ii<5; ii++){Pct_5msCtrlBus.Pct_5msCtrlWhlnVlvCtrlTarInfo.RrInVlvCtrlTar[ii] = 1000;}
		}
		else
		{
			for(ii=0; ii<5; ii++){Pct_5msCtrlBus.Pct_5msCtrlWhlnVlvCtrlTarInfo.RrInVlvCtrlTar[ii] = 0;}
		}
	}
	/* Wheel Inlet Valve Override - End */

    if(U16_WHL_SWITCH_DELAY_TIME > 0)
    {
    	u8InVlvAllCloseReq = ((PressCTRLLocalBus.u8WhlCtrlIndex == 0) && (lapctu8WhlIndexOld == 0) && (PressCTRLLocalBus.s8PCtrlState == S8_PRESS_BOOST)) ? TRUE : FALSE;
	}
	else
	{
		u8InVlvAllCloseReq = ((PressCTRLLocalBus.u8WhlCtrlIndex == 0) && (PressCTRLLocalBus.s8PCtrlState == S8_PRESS_BOOST)) ? TRUE : FALSE;
	}
    Pct_5msCtrlBus.Pct_5msCtrlInVlvAllCloseReq = u8InVlvAllCloseReq;
    lapctu8WhlIndexOld = PressCTRLLocalBus.u8WhlCtrlIndex;
#endif	/* (M_IDB_TEST == DISABLE) */


    /* Output */
    Pct_5msCtrl_Write_Pct_5msCtrlIdbPCtrllVlvCtrlStInfo(&Pct_5msCtrlBus.Pct_5msCtrlIdbPCtrllVlvCtrlStInfo);
    /*==============================================================================
    * Members of structure Pct_5msCtrlIdbPCtrllVlvCtrlStInfo 
     : Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.CvVlvSt;
     : Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.RlVlvSt;
     : Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.PdVlvSt;
     : Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.BalVlvSt;
     : Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.CvVlvNoiseCtrlSt;
     : Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.RlVlvNoiseCtrlSt;
     : Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.PdVlvNoiseCtrlSt;
     : Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.BalVlvNoiseCtrlSt;
     : Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.CvVlvHoldTime;
     : Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.RlVlvHoldTime;
     : Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.PdVlvHoldTime;
     : Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.BalVlvHoldTime;
     =============================================================================*/
    
    Pct_5msCtrl_Write_Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo(&Pct_5msCtrlBus.Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo);
    /*==============================================================================
    * Members of structure Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo 
     : Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvSt;
     : Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvSt;
     : Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SimVlvSt;
     : Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvNoiseCtrlSt;
     : Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvNoiseCtrlSt;
     : Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SimVlvNoiseCtrlSt;
     : Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvHoldTime;
     : Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvHoldTime;
     : Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SimVlvHoldTime;
     : Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.CutVlvLongOpen;
     =============================================================================*/
    
    Pct_5msCtrl_Write_Pct_5msCtrlWhlnVlvCtrlTarInfo(&Pct_5msCtrlBus.Pct_5msCtrlWhlnVlvCtrlTarInfo);
    /*==============================================================================
    * Members of structure Pct_5msCtrlWhlnVlvCtrlTarInfo 
     : Pct_5msCtrlWhlnVlvCtrlTarInfo.FlInVlvCtrlTar;
     : Pct_5msCtrlWhlnVlvCtrlTarInfo.FrInVlvCtrlTar;
     : Pct_5msCtrlWhlnVlvCtrlTarInfo.RlInVlvCtrlTar;
     : Pct_5msCtrlWhlnVlvCtrlTarInfo.RrInVlvCtrlTar;
     =============================================================================*/
    
    Pct_5msCtrl_Write_Pct_5msCtrlStkRecvryActnIfInfo(&Pct_5msCtrlBus.Pct_5msCtrlStkRecvryActnIfInfo);
    /*==============================================================================
    * Members of structure Pct_5msCtrlStkRecvryActnIfInfo 
     : Pct_5msCtrlStkRecvryActnIfInfo.RecommendStkRcvrLvl;
     : Pct_5msCtrlStkRecvryActnIfInfo.StkRecvryCtrlState;
     : Pct_5msCtrlStkRecvryActnIfInfo.StkRecvryRequestedTime;
     =============================================================================*/
    
    Pct_5msCtrl_Write_Pct_5msCtrlMuxCmdExecStInfo(&Pct_5msCtrlBus.Pct_5msCtrlMuxCmdExecStInfo);
    /*==============================================================================
    * Members of structure Pct_5msCtrlMuxCmdExecStInfo 
     : Pct_5msCtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlFrntLe;
     : Pct_5msCtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlFrntRi;
     : Pct_5msCtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlReLe;
     : Pct_5msCtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlReRi;
     =============================================================================*/
    
    Pct_5msCtrl_Write_Pct_5msCtrlPCtrlAct(&Pct_5msCtrlBus.Pct_5msCtrlPCtrlAct);
    Pct_5msCtrl_Write_Pct_5msCtrlInitQuickBrkDctFlg(&Pct_5msCtrlBus.Pct_5msCtrlInitQuickBrkDctFlg);
    Pct_5msCtrl_Write_Pct_5msCtrlTgtDeltaStkType(&Pct_5msCtrlBus.Pct_5msCtrlTgtDeltaStkType);
    Pct_5msCtrl_Write_Pct_5msCtrlPCtrlBoostMod(&Pct_5msCtrlBus.Pct_5msCtrlPCtrlBoostMod);
    Pct_5msCtrl_Write_Pct_5msCtrlPCtrlFadeoutSt(&Pct_5msCtrlBus.Pct_5msCtrlPCtrlFadeoutSt);
    Pct_5msCtrl_Write_Pct_5msCtrlPCtrlSt(&Pct_5msCtrlBus.Pct_5msCtrlPCtrlSt);
    Pct_5msCtrl_Write_Pct_5msCtrlInVlvAllCloseReq(&Pct_5msCtrlBus.Pct_5msCtrlInVlvAllCloseReq);
    Pct_5msCtrl_Write_Pct_5msCtrlPChamberVolume(&Pct_5msCtrlBus.Pct_5msCtrlPChamberVolume);
    Pct_5msCtrl_Write_Pct_5msCtrlTarDeltaStk(&Pct_5msCtrlBus.Pct_5msCtrlTarDeltaStk);
    Pct_5msCtrl_Write_Pct_5msCtrlFinalTarPFromPCtrl(&Pct_5msCtrlBus.Pct_5msCtrlFinalTarPFromPCtrl);

    Pct_5msCtrl_Timer_Elapsed = STM0_TIM0.U - Pct_5msCtrl_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define PCT_5MSCTRL_STOP_SEC_CODE
#include "Pct_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
