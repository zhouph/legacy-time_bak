#define S_FUNCTION_NAME      Pct_5msCtrl_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          164
#define WidthOutputPort         59

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Pct_5msCtrl.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Pct_5msCtrlWhlVlvReqAbcInfo.FlIvReqData[0] = input[0];
    Pct_5msCtrlWhlVlvReqAbcInfo.FlIvReqData[1] = input[1];
    Pct_5msCtrlWhlVlvReqAbcInfo.FlIvReqData[2] = input[2];
    Pct_5msCtrlWhlVlvReqAbcInfo.FlIvReqData[3] = input[3];
    Pct_5msCtrlWhlVlvReqAbcInfo.FlIvReqData[4] = input[4];
    Pct_5msCtrlWhlVlvReqAbcInfo.FlIvReqData[5] = input[5];
    Pct_5msCtrlWhlVlvReqAbcInfo.FlIvReqData[6] = input[6];
    Pct_5msCtrlWhlVlvReqAbcInfo.FlIvReqData[7] = input[7];
    Pct_5msCtrlWhlVlvReqAbcInfo.FlIvReqData[8] = input[8];
    Pct_5msCtrlWhlVlvReqAbcInfo.FlIvReqData[9] = input[9];
    Pct_5msCtrlWhlVlvReqAbcInfo.FrIvReqData[0] = input[10];
    Pct_5msCtrlWhlVlvReqAbcInfo.FrIvReqData[1] = input[11];
    Pct_5msCtrlWhlVlvReqAbcInfo.FrIvReqData[2] = input[12];
    Pct_5msCtrlWhlVlvReqAbcInfo.FrIvReqData[3] = input[13];
    Pct_5msCtrlWhlVlvReqAbcInfo.FrIvReqData[4] = input[14];
    Pct_5msCtrlWhlVlvReqAbcInfo.FrIvReqData[5] = input[15];
    Pct_5msCtrlWhlVlvReqAbcInfo.FrIvReqData[6] = input[16];
    Pct_5msCtrlWhlVlvReqAbcInfo.FrIvReqData[7] = input[17];
    Pct_5msCtrlWhlVlvReqAbcInfo.FrIvReqData[8] = input[18];
    Pct_5msCtrlWhlVlvReqAbcInfo.FrIvReqData[9] = input[19];
    Pct_5msCtrlWhlVlvReqAbcInfo.RlIvReqData[0] = input[20];
    Pct_5msCtrlWhlVlvReqAbcInfo.RlIvReqData[1] = input[21];
    Pct_5msCtrlWhlVlvReqAbcInfo.RlIvReqData[2] = input[22];
    Pct_5msCtrlWhlVlvReqAbcInfo.RlIvReqData[3] = input[23];
    Pct_5msCtrlWhlVlvReqAbcInfo.RlIvReqData[4] = input[24];
    Pct_5msCtrlWhlVlvReqAbcInfo.RlIvReqData[5] = input[25];
    Pct_5msCtrlWhlVlvReqAbcInfo.RlIvReqData[6] = input[26];
    Pct_5msCtrlWhlVlvReqAbcInfo.RlIvReqData[7] = input[27];
    Pct_5msCtrlWhlVlvReqAbcInfo.RlIvReqData[8] = input[28];
    Pct_5msCtrlWhlVlvReqAbcInfo.RlIvReqData[9] = input[29];
    Pct_5msCtrlWhlVlvReqAbcInfo.RrIvReqData[0] = input[30];
    Pct_5msCtrlWhlVlvReqAbcInfo.RrIvReqData[1] = input[31];
    Pct_5msCtrlWhlVlvReqAbcInfo.RrIvReqData[2] = input[32];
    Pct_5msCtrlWhlVlvReqAbcInfo.RrIvReqData[3] = input[33];
    Pct_5msCtrlWhlVlvReqAbcInfo.RrIvReqData[4] = input[34];
    Pct_5msCtrlWhlVlvReqAbcInfo.RrIvReqData[5] = input[35];
    Pct_5msCtrlWhlVlvReqAbcInfo.RrIvReqData[6] = input[36];
    Pct_5msCtrlWhlVlvReqAbcInfo.RrIvReqData[7] = input[37];
    Pct_5msCtrlWhlVlvReqAbcInfo.RrIvReqData[8] = input[38];
    Pct_5msCtrlWhlVlvReqAbcInfo.RrIvReqData[9] = input[39];
    Pct_5msCtrlWhlVlvReqAbcInfo.FlIvReq = input[40];
    Pct_5msCtrlWhlVlvReqAbcInfo.FrIvReq = input[41];
    Pct_5msCtrlWhlVlvReqAbcInfo.RlIvReq = input[42];
    Pct_5msCtrlWhlVlvReqAbcInfo.RrIvReq = input[43];
    Pct_5msCtrlEemFailData.Eem_Fail_SimP = input[44];
    Pct_5msCtrlEemFailData.Eem_Fail_BLS = input[45];
    Pct_5msCtrlPedlTrvlFild1msInfo.PdtFild1ms = input[46];
    Pct_5msCtrlCanRxAccelPedlInfo.AccelPedlVal = input[47];
    Pct_5msCtrlCanRxAccelPedlInfo.AccelPedlValErr = input[48];
    Pct_5msCtrlMotRotgAgSigInfo.StkPosnMeasd = input[49];
    Pct_5msCtrlAbsCtrlInfo.AbsActFlg = input[50];
    Pct_5msCtrlAbsCtrlInfo.AbsDefectFlg = input[51];
    Pct_5msCtrlAbsCtrlInfo.AbsTarPFrntLe = input[52];
    Pct_5msCtrlAbsCtrlInfo.AbsTarPFrntRi = input[53];
    Pct_5msCtrlAbsCtrlInfo.AbsTarPReLe = input[54];
    Pct_5msCtrlAbsCtrlInfo.AbsTarPReRi = input[55];
    Pct_5msCtrlAbsCtrlInfo.FrntWhlSlip = input[56];
    Pct_5msCtrlAbsCtrlInfo.AbsDesTarP = input[57];
    Pct_5msCtrlAbsCtrlInfo.AbsDesTarPReqFlg = input[58];
    Pct_5msCtrlAbsCtrlInfo.AbsCtrlFadeOutFlg = input[59];
    Pct_5msCtrlAvhCtrlInfo.AvhActFlg = input[60];
    Pct_5msCtrlAvhCtrlInfo.AvhDefectFlg = input[61];
    Pct_5msCtrlAvhCtrlInfo.AvhTarP = input[62];
    Pct_5msCtrlEscCtrlInfo.EscActFlg = input[63];
    Pct_5msCtrlEscCtrlInfo.EscDefectFlg = input[64];
    Pct_5msCtrlEscCtrlInfo.EscTarPFrntLe = input[65];
    Pct_5msCtrlEscCtrlInfo.EscTarPFrntRi = input[66];
    Pct_5msCtrlEscCtrlInfo.EscTarPReLe = input[67];
    Pct_5msCtrlEscCtrlInfo.EscTarPReRi = input[68];
    Pct_5msCtrlHsaCtrlInfo.HsaActFlg = input[69];
    Pct_5msCtrlHsaCtrlInfo.HsaDefectFlg = input[70];
    Pct_5msCtrlHsaCtrlInfo.HsaTarP = input[71];
    Pct_5msCtrlStkRecvryCtrlIfInfo.StkRcvrEscAllowedTime = input[72];
    Pct_5msCtrlStkRecvryCtrlIfInfo.StkRcvrAbsAllowedTime = input[73];
    Pct_5msCtrlStkRecvryCtrlIfInfo.StkRcvrTcsAllowedTime = input[74];
    Pct_5msCtrlTcsCtrlInfo.TcsActFlg = input[75];
    Pct_5msCtrlTcsCtrlInfo.TcsDefectFlg = input[76];
    Pct_5msCtrlTcsCtrlInfo.TcsTarPFrntLe = input[77];
    Pct_5msCtrlTcsCtrlInfo.TcsTarPFrntRi = input[78];
    Pct_5msCtrlTcsCtrlInfo.TcsTarPReLe = input[79];
    Pct_5msCtrlTcsCtrlInfo.TcsTarPReRi = input[80];
    Pct_5msCtrlTcsCtrlInfo.BothDrvgWhlBrkCtlReqFlg = input[81];
    Pct_5msCtrlBaseBrkCtrlModInfo.VehStandStillStFlg = input[82];
    Pct_5msCtrlBrkPedlStatusInfo.DrvrIntendToRelsBrk = input[83];
    Pct_5msCtrlBrkPedlStatusInfo.PanicBrkStFlg = input[84];
    Pct_5msCtrlBrkPedlStatusInfo.PedlReldStFlg2 = input[85];
    Pct_5msCtrlBrkPedlStatus1msInfo.PanicBrkSt1msFlg = input[86];
    Pct_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar = input[87];
    Pct_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar = input[88];
    Pct_5msCtrlCircPRateInfo.PrimCircPChgDurg10ms = input[89];
    Pct_5msCtrlCircPRateInfo.SecdCircPChgDurg10ms = input[90];
    Pct_5msCtrlPedlTrvlRate1msInfo.PedlTrvlRate1msMmPerSec = input[91];
    Pct_5msCtrlEstimdWhlPInfo.FrntLeEstimdWhlP = input[92];
    Pct_5msCtrlEstimdWhlPInfo.FrntRiEstimdWhlP = input[93];
    Pct_5msCtrlEstimdWhlPInfo.ReLeEstimdWhlP = input[94];
    Pct_5msCtrlEstimdWhlPInfo.ReRiEstimdWhlP = input[95];
    Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlFrntLe = input[96];
    Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlFrntRi = input[97];
    Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlReLe = input[98];
    Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlReRi = input[99];
    Pct_5msCtrlFinalTarPInfo.FinalTarPRateOfWhlFrntLe = input[100];
    Pct_5msCtrlFinalTarPInfo.FinalTarPRateOfWhlFrntRi = input[101];
    Pct_5msCtrlFinalTarPInfo.FinalTarPRateOfWhlReLe = input[102];
    Pct_5msCtrlFinalTarPInfo.FinalTarPRateOfWhlReRi = input[103];
    Pct_5msCtrlFinalTarPInfo.PCtrlPrioOfWhlFrntLe = input[104];
    Pct_5msCtrlFinalTarPInfo.PCtrlPrioOfWhlFrntRi = input[105];
    Pct_5msCtrlFinalTarPInfo.PCtrlPrioOfWhlReLe = input[106];
    Pct_5msCtrlFinalTarPInfo.PCtrlPrioOfWhlReRi = input[107];
    Pct_5msCtrlFinalTarPInfo.CtrlModOfWhlFrntLe = input[108];
    Pct_5msCtrlFinalTarPInfo.CtrlModOfOfWhlFrntRi = input[109];
    Pct_5msCtrlFinalTarPInfo.CtrlModOfWhlReLe = input[110];
    Pct_5msCtrlFinalTarPInfo.CtrlModOfWhlReRi = input[111];
    Pct_5msCtrlFinalTarPInfo.ReqdPCtrlBoostMod = input[112];
    Pct_5msCtrlFinalTarPInfo.FinalDelTarPOfWhlFrntLe = input[113];
    Pct_5msCtrlFinalTarPInfo.FinalDelTarPOfWhlFrntRi = input[114];
    Pct_5msCtrlFinalTarPInfo.FinalDelTarPOfWhlReLe = input[115];
    Pct_5msCtrlFinalTarPInfo.FinalDelTarPOfWhlReRi = input[116];
    Pct_5msCtrlIdbMotPosnInfo.MotTarPosn = input[117];
    Pct_5msCtrlWhlOutVlvCtrlTarInfo.FlOutVlvCtrlTar[0] = input[118];
    Pct_5msCtrlWhlOutVlvCtrlTarInfo.FlOutVlvCtrlTar[1] = input[119];
    Pct_5msCtrlWhlOutVlvCtrlTarInfo.FlOutVlvCtrlTar[2] = input[120];
    Pct_5msCtrlWhlOutVlvCtrlTarInfo.FlOutVlvCtrlTar[3] = input[121];
    Pct_5msCtrlWhlOutVlvCtrlTarInfo.FlOutVlvCtrlTar[4] = input[122];
    Pct_5msCtrlWhlOutVlvCtrlTarInfo.FrOutVlvCtrlTar[0] = input[123];
    Pct_5msCtrlWhlOutVlvCtrlTarInfo.FrOutVlvCtrlTar[1] = input[124];
    Pct_5msCtrlWhlOutVlvCtrlTarInfo.FrOutVlvCtrlTar[2] = input[125];
    Pct_5msCtrlWhlOutVlvCtrlTarInfo.FrOutVlvCtrlTar[3] = input[126];
    Pct_5msCtrlWhlOutVlvCtrlTarInfo.FrOutVlvCtrlTar[4] = input[127];
    Pct_5msCtrlWhlOutVlvCtrlTarInfo.RlOutVlvCtrlTar[0] = input[128];
    Pct_5msCtrlWhlOutVlvCtrlTarInfo.RlOutVlvCtrlTar[1] = input[129];
    Pct_5msCtrlWhlOutVlvCtrlTarInfo.RlOutVlvCtrlTar[2] = input[130];
    Pct_5msCtrlWhlOutVlvCtrlTarInfo.RlOutVlvCtrlTar[3] = input[131];
    Pct_5msCtrlWhlOutVlvCtrlTarInfo.RlOutVlvCtrlTar[4] = input[132];
    Pct_5msCtrlWhlOutVlvCtrlTarInfo.RrOutVlvCtrlTar[0] = input[133];
    Pct_5msCtrlWhlOutVlvCtrlTarInfo.RrOutVlvCtrlTar[1] = input[134];
    Pct_5msCtrlWhlOutVlvCtrlTarInfo.RrOutVlvCtrlTar[2] = input[135];
    Pct_5msCtrlWhlOutVlvCtrlTarInfo.RrOutVlvCtrlTar[3] = input[136];
    Pct_5msCtrlWhlOutVlvCtrlTarInfo.RrOutVlvCtrlTar[4] = input[137];
    Pct_5msCtrlWhlPreFillInfo.FlPreFillActFlg = input[138];
    Pct_5msCtrlWhlPreFillInfo.FrPreFillActFlg = input[139];
    Pct_5msCtrlWhlPreFillInfo.RlPreFillActFlg = input[140];
    Pct_5msCtrlWhlPreFillInfo.RrPreFillActFlg = input[141];
    Pct_5msCtrlIdbVlvActInfo.VlvFadeoutEndOK = input[142];
    Pct_5msCtrlIdbVlvActInfo.CutVlvOpenFlg = input[143];
    Pct_5msCtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar = input[144];
    Pct_5msCtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec = input[145];
    Pct_5msCtrlPedlTrvlTagInfo.PedlSigTag = input[146];
    Pct_5msCtrlPistPFildInfo.PistPFild_1_100Bar = input[147];
    Pct_5msCtrlPistPRateInfo.PistPChgDurg10ms = input[148];
    Pct_5msCtrlVlvActtnReqByBaseBrkCtrlrInfo.PedlSimrPGendOnFlg = input[149];
    Pct_5msCtrlWhlSpdFildInfo.WhlSpdFildReLe = input[150];
    Pct_5msCtrlWhlSpdFildInfo.WhlSpdFildReRi = input[151];
    Pct_5msCtrlEemSuspectData.Eem_Suspect_SimP = input[152];
    Pct_5msCtrlEcuModeSts = input[153];
    Pct_5msCtrlMotCtrlMode = input[154];
    Pct_5msCtrlFuncInhibitPctrlSts = input[155];
    Pct_5msCtrlBaseBrkCtrlrActFlg = input[156];
    Pct_5msCtrlBlsFild = input[157];
    Pct_5msCtrlBrkPRednForBaseBrkCtrlr = input[158];
    Pct_5msCtrlMotICtrlFadeOutState = input[159];
    Pct_5msCtrlPreBoostMod = input[160];
    Pct_5msCtrlRgnBrkCtlrBlendgFlg = input[161];
    Pct_5msCtrlVehSpdFild = input[162];
    Pct_5msCtrlStkRecvryStabnEndOK = input[163];

    Pct_5msCtrl();


    output[0] = Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.CvVlvSt;
    output[1] = Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.RlVlvSt;
    output[2] = Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.PdVlvSt;
    output[3] = Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.BalVlvSt;
    output[4] = Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.CvVlvNoiseCtrlSt;
    output[5] = Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.RlVlvNoiseCtrlSt;
    output[6] = Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.PdVlvNoiseCtrlSt;
    output[7] = Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.BalVlvNoiseCtrlSt;
    output[8] = Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.CvVlvHoldTime;
    output[9] = Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.RlVlvHoldTime;
    output[10] = Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.PdVlvHoldTime;
    output[11] = Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.BalVlvHoldTime;
    output[12] = Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvSt;
    output[13] = Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvSt;
    output[14] = Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SimVlvSt;
    output[15] = Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvNoiseCtrlSt;
    output[16] = Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvNoiseCtrlSt;
    output[17] = Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SimVlvNoiseCtrlSt;
    output[18] = Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvHoldTime;
    output[19] = Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvHoldTime;
    output[20] = Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SimVlvHoldTime;
    output[21] = Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.CutVlvLongOpen;
    output[22] = Pct_5msCtrlWhlnVlvCtrlTarInfo.FlInVlvCtrlTar[0];
    output[23] = Pct_5msCtrlWhlnVlvCtrlTarInfo.FlInVlvCtrlTar[1];
    output[24] = Pct_5msCtrlWhlnVlvCtrlTarInfo.FlInVlvCtrlTar[2];
    output[25] = Pct_5msCtrlWhlnVlvCtrlTarInfo.FlInVlvCtrlTar[3];
    output[26] = Pct_5msCtrlWhlnVlvCtrlTarInfo.FlInVlvCtrlTar[4];
    output[27] = Pct_5msCtrlWhlnVlvCtrlTarInfo.FrInVlvCtrlTar[0];
    output[28] = Pct_5msCtrlWhlnVlvCtrlTarInfo.FrInVlvCtrlTar[1];
    output[29] = Pct_5msCtrlWhlnVlvCtrlTarInfo.FrInVlvCtrlTar[2];
    output[30] = Pct_5msCtrlWhlnVlvCtrlTarInfo.FrInVlvCtrlTar[3];
    output[31] = Pct_5msCtrlWhlnVlvCtrlTarInfo.FrInVlvCtrlTar[4];
    output[32] = Pct_5msCtrlWhlnVlvCtrlTarInfo.RlInVlvCtrlTar[0];
    output[33] = Pct_5msCtrlWhlnVlvCtrlTarInfo.RlInVlvCtrlTar[1];
    output[34] = Pct_5msCtrlWhlnVlvCtrlTarInfo.RlInVlvCtrlTar[2];
    output[35] = Pct_5msCtrlWhlnVlvCtrlTarInfo.RlInVlvCtrlTar[3];
    output[36] = Pct_5msCtrlWhlnVlvCtrlTarInfo.RlInVlvCtrlTar[4];
    output[37] = Pct_5msCtrlWhlnVlvCtrlTarInfo.RrInVlvCtrlTar[0];
    output[38] = Pct_5msCtrlWhlnVlvCtrlTarInfo.RrInVlvCtrlTar[1];
    output[39] = Pct_5msCtrlWhlnVlvCtrlTarInfo.RrInVlvCtrlTar[2];
    output[40] = Pct_5msCtrlWhlnVlvCtrlTarInfo.RrInVlvCtrlTar[3];
    output[41] = Pct_5msCtrlWhlnVlvCtrlTarInfo.RrInVlvCtrlTar[4];
    output[42] = Pct_5msCtrlStkRecvryActnIfInfo.RecommendStkRcvrLvl;
    output[43] = Pct_5msCtrlStkRecvryActnIfInfo.StkRecvryCtrlState;
    output[44] = Pct_5msCtrlStkRecvryActnIfInfo.StkRecvryRequestedTime;
    output[45] = Pct_5msCtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlFrntLe;
    output[46] = Pct_5msCtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlFrntRi;
    output[47] = Pct_5msCtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlReLe;
    output[48] = Pct_5msCtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlReRi;
    output[49] = Pct_5msCtrlPCtrlAct;
    output[50] = Pct_5msCtrlInitQuickBrkDctFlg;
    output[51] = Pct_5msCtrlTgtDeltaStkType;
    output[52] = Pct_5msCtrlPCtrlBoostMod;
    output[53] = Pct_5msCtrlPCtrlFadeoutSt;
    output[54] = Pct_5msCtrlPCtrlSt;
    output[55] = Pct_5msCtrlInVlvAllCloseReq;
    output[56] = Pct_5msCtrlPChamberVolume;
    output[57] = Pct_5msCtrlTarDeltaStk;
    output[58] = Pct_5msCtrlFinalTarPFromPCtrl;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Pct_5msCtrl_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
