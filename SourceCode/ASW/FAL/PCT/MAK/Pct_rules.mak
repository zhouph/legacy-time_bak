# \file
#
# \brief Pct
#
# This file contains the implementation of the SWC
# module Pct.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Pct_src

Pct_src_FILES        += $(Pct_SRC_PATH)\Pct_1msCtrl.c
Pct_src_FILES        += $(Pct_IFA_PATH)\Pct_1msCtrl_Ifa.c
Pct_src_FILES        += $(Pct_SRC_PATH)\Pct_5msCtrl.c
Pct_src_FILES        += $(Pct_SRC_PATH)\LAPCT_DecidePressCtrlMode.c
Pct_src_FILES        += $(Pct_SRC_PATH)\LAPCT_DecideStkRcvrState.c
Pct_src_FILES        += $(Pct_SRC_PATH)\LAIDB_CallMultiplexControl.c
Pct_src_FILES        += $(Pct_SRC_PATH)\LAPCT_DecidePressSource.c
Pct_src_FILES        += $(Pct_STK_RCVR_PATH)\LAPCT_ChkPosnForStkRcvr.c
Pct_src_FILES        += $(Pct_STK_RCVR_PATH)\LAPCT_DecideStkRcvrTime.c

Pct_src_FILES        += $(Pct_MTR_PATH)\LAPCT_DecideMtrCtrlMode.c
Pct_src_FILES        += $(Pct_MTR_PATH)\LAPCT_DeltaStrokeInterface.c
Pct_src_FILES        += $(Pct_MTR_PATH)\LAPCT_DeltaStrokeMap.c
Pct_src_FILES        += $(Pct_MTR_PATH)\LAPCT_DeltaStrokePerror.c
Pct_src_FILES        += $(Pct_MTR_PATH)\LAPCT_DeltaStrokePRateError.c
Pct_src_FILES        += $(Pct_MTR_PATH)\LAPCT_LoadMapData.c

Pct_src_FILES        += $(Pct_VLV_PATH)\LAPCT_PressureVlvCtrlMode.c
Pct_src_FILES        += $(Pct_VLV_PATH)\LAPCT_PedalFeelGenVlvCtrlMode.c


Pct_src_FILES        += $(Pct_MUX_PATH)\LAIDB_CallMultiplexValveActuation.c
Pct_src_FILES        += $(Pct_MUX_PATH)\LAIDB_DecideTargetPress.c
Pct_src_FILES        += $(Pct_MUX_PATH)\LAIDB_LibMuxFIFO.c
Pct_src_FILES        += $(Pct_IFA_PATH)\Pct_5msCtrl_Ifa.c
Pct_src_FILES        += $(Pct_CFG_PATH)\Pct_Cfg.c
Pct_src_FILES        += $(Pct_CAL_PATH)\Pct_Cal.c
# /* HSH */
ifeq ($(ICE_COMPILE),true)
# Library
Pct_src_FILES        += $(Pct_CORE_PATH)\virtual_main.c
Pct_src_FILES  += $(Pct_CORE_PATH)\ICE\CMN\LIB\SwcCommonFunction.c
Pct_src_FILES  += $(Pct_CORE_PATH)\ICE\CMN\LIB\L_CommonFunction.c
Pct_src_FILES  += $(Pct_CORE_PATH)\ICE\CMN\FBL_HDR\FBL_HMC\Fbl_Header.c
Pct_src_FILES  += $(Pct_CORE_PATH)\ICE\CMN\FBL_HDR\FBL_HMC\Fbl_Mtab.c
endif

ifeq ($(UNIT_TEST),true)
	Pct_src_FILES        += $(Pct_UNITY_PATH)\unity.c
	Pct_src_FILES        += $(Pct_UNITY_PATH)\unity_fixture.c	
	Pct_src_FILES        += $(Pct_UT_PATH)\main.c
	Pct_src_FILES        += $(Pct_UT_PATH)\Pct_1msCtrl\Pct_1msCtrl_UtMain.c
	Pct_src_FILES        += $(Pct_UT_PATH)\Pct_5msCtrl\Pct_5msCtrl_UtMain.c
endif