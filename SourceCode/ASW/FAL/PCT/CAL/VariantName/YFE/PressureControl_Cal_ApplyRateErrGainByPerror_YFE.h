DATA_PRESSRATEERRGAIN_t		apCalApplyRateErrGainByPerror =
{
{   /* P-error 0 bar */
		10, 												/* S16_RATE_CMP_A_GAIN_BY_P_ERR_0(0) */
		30,													/* S16_RATE_CMP_A_GAIN_BY_P_ERR_0(1) */
		50,													/* S16_RATE_CMP_A_GAIN_BY_P_ERR_0(2) */
		80,													/* S16_RATE_CMP_A_GAIN_BY_P_ERR_0(3) */
		100 													/* S16_RATE_CMP_A_GAIN_BY_P_ERR_0(4) */
    },
    {   /* P-error 1 bar */
		10, 												/* S16_RATE_CMP_A_GAIN_BY_P_ERR_1(0) */
		30,													/* S16_RATE_CMP_A_GAIN_BY_P_ERR_1(1) */
		50,													/* S16_RATE_CMP_A_GAIN_BY_P_ERR_1(2) */
		80,													/* S16_RATE_CMP_A_GAIN_BY_P_ERR_1(3) */
		100 													/* S16_RATE_CMP_A_GAIN_BY_P_ERR_1(4) */
    },
    {   /* P-error 5 bar */
		10, 												/* S16_RATE_CMP_A_GAIN_BY_P_ERR_2(0) */
		20,													/* S16_RATE_CMP_A_GAIN_BY_P_ERR_2(1) */
		40,													/* S16_RATE_CMP_A_GAIN_BY_P_ERR_2(2) */
		70,													/* S16_RATE_CMP_A_GAIN_BY_P_ERR_2(3) */
		90 													/* S16_RATE_CMP_A_GAIN_BY_P_ERR_2(4) */
    },
    {   /* P-error 10 bar */
		10, 												/* S16_RATE_CMP_A_GAIN_BY_P_ERR_3(0) */
		15,													/* S16_RATE_CMP_A_GAIN_BY_P_ERR_3(1) */
		30,													/* S16_RATE_CMP_A_GAIN_BY_P_ERR_3(2) */
		60,													/* S16_RATE_CMP_A_GAIN_BY_P_ERR_3(3) */
		80 													/* S16_RATE_CMP_A_GAIN_BY_P_ERR_3(4) */
    },
    {
		0,													/* S16_RATE_CMP_A_GAIN_P_LVL(0) */
		50,													/* S16_RATE_CMP_A_GAIN_P_LVL(1) */
		100,												/* S16_RATE_CMP_A_GAIN_P_LVL(2) */
		150,												/* S16_RATE_CMP_A_GAIN_P_LVL(3) */
		250,												/* S16_RATE_CMP_A_GAIN_P_LVL(4) */
    }
};
