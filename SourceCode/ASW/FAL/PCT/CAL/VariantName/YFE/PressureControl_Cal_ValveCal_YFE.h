DATA_VALVECAL_t               apCalValveCal =
{
    	/* uint16_t    	U16_CV_ON_TIME_LSD                    */		    1000,	/* Comment [ 占쎌젟筌∽옙 占쎌굢占쎈뮉 占쏙옙占쎈꺗占쎈퓠占쎄퐣 Cut Valve 占쎌�占쎌굙 占쎈뻻揶쏉옙(Noise 占쏙옙揶쏉옙) ] */
    	/* uint16_t    	U16_CV_ON_TIME_P_GEAR                 */		    2000,	/* idb : Org 6000*//* Comment [ P占쎈뼊 占쎈퓠占쎄퐣 cut valve 占쎌�占쎌굙占쎈뻻揶쏉옙 ] */
    																				/* ScaleVal[     30 sec ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ 0 <= X <= 327.675 ] */
    	/* int16_t     	S16_CVV_MNT_END_BCP_DIFF_REF          */		       3,	/* Comment [ Cut Valve �쑀�삁瑜� 醫낅즺�븯�뒗 Circuit Pressure �긽�듅 �븬�젰 ] */
    	/* int16_t     	S16_CVV_MNT_END_BCP_REF               */		      10,	/* Comment [ Cut Valve �쑀�삁瑜� 醫낅즺�븯�뒗 Circuit Pressure �븬�젰 ] */
    	/* uint16_t 	U16_PDV_OPEN_DELAY_AF_BALV_CLOSED	  */               1
};
