DATA_PCTRLCAL_t               apCalPCtrlCal =
{
    /* uchar8_t     U8_VEHICLE_CREEPING_SPEED             */              40,   /* Comment [ Vehicle creeping speed (default 5kph) ] */
                                                                                /* ScaleVal[      5 KPH ]   Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
    /* int16_t      S16_ABS_LIMIT_SPEED                   */              40,   /* Comment [ ABS limit speed(default 5kph) ] */
                                                                                /* ScaleVal[      5 KPH ]   Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
    /* int16_t      S16_CVV_CLOSE_DELAY_PSP_THR_H         */             400,   /* Comment [ Fast Apply 감지시 Cut V/V Close 유보 PSP High Threshold ] */
                                                                                /* ScaleVal[     40 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 150 ] */
    /* int16_t      S16_CVV_CLOSE_DELAY_PSP_THR_L         */              50,   /* Comment [ Fast Apply 감지시 Cut V/V Close 유보 PSP Low Threshold ] */
                                                                                /* ScaleVal[      5 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ 0 <= X <= 150 ] */
};
