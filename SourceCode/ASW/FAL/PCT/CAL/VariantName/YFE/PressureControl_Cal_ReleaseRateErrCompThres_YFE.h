DATA_RATEERRCOMPTHRES_t			apCalReleaseRateErrCompThres =
{
{
		S16_P_RATE_4_BPS,  									/* S16_RATE_CMP_R_THD_BY_P_ERR(0) */
		S16_P_RATE_5_BPS,  									/* S16_RATE_CMP_R_THD_BY_P_ERR(1) */
		S16_P_RATE_25_BPS, 									/* S16_RATE_CMP_R_THD_BY_P_ERR(2) */
		S16_P_RATE_100_BPS,									/* S16_RATE_CMP_R_THD_BY_P_ERR(3) */
    },
    {
		S16_P_RATE_0_BPS,  									/* S16_RATE_CMP_R_THD_BY_P_ERR_QCK(0) */
		S16_P_RATE_0_BPS,  									/* S16_RATE_CMP_R_THD_BY_P_ERR_QCK(1) */
		S16_P_RATE_0_BPS, 									/* S16_RATE_CMP_R_THD_BY_P_ERR_QCK(2) */
		S16_P_RATE_0_BPS,									/* S16_RATE_CMP_R_THD_BY_P_ERR_QCK(3) */
    },
    {
		S16_PCTRL_PRESS_0_BAR,								/* S16_RATE_CMP_R_THD_P_ERR_LVL(0) */
		S16_PCTRL_PRESS_1_BAR,                              /* S16_RATE_CMP_R_THD_P_ERR_LVL(1) */
		S16_PCTRL_PRESS_5_BAR,                              /* S16_RATE_CMP_R_THD_P_ERR_LVL(2) */
		S16_PCTRL_PRESS_10_BAR,                             /* S16_RATE_CMP_R_THD_P_ERR_LVL(3) */
    }
};
