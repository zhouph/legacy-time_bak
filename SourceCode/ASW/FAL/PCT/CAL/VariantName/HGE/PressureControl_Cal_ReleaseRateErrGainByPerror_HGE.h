DATA_PRESSRATEERRGAIN_t       apCalReleaseRateErrGainByPerror =
{
	{   /* P-error 0 bar */
		0,													/* S16_RATE_CMP_R_GAIN_BY_P_ERR_0(0) */
		30,													/* S16_RATE_CMP_R_GAIN_BY_P_ERR_0(1) */
		50,													/* S16_RATE_CMP_R_GAIN_BY_P_ERR_0(2) */
		70,													/* S16_RATE_CMP_R_GAIN_BY_P_ERR_0(3) */
		100 													/* S16_RATE_CMP_R_GAIN_BY_P_ERR_0(4) */
    },
    {   /* P-error 1 bar */
		0,													/* S16_RATE_CMP_R_GAIN_BY_P_ERR_1(0) */
		30,													/* S16_RATE_CMP_R_GAIN_BY_P_ERR_1(1) */
		50,													/* S16_RATE_CMP_R_GAIN_BY_P_ERR_1(2) */
		70,													/* S16_RATE_CMP_R_GAIN_BY_P_ERR_1(3) */
		100 													/* S16_RATE_CMP_R_GAIN_BY_P_ERR_1(4) */
    },
    {   /* P-error 5 bar */
		0, 													/* S16_RATE_CMP_R_GAIN_BY_P_ERR_2(0) */
		25,													/* S16_RATE_CMP_R_GAIN_BY_P_ERR_2(1) */
		45,													/* S16_RATE_CMP_R_GAIN_BY_P_ERR_2(2) */
		60,													/* S16_RATE_CMP_R_GAIN_BY_P_ERR_2(3) */
		90 													/* S16_RATE_CMP_R_GAIN_BY_P_ERR_2(4) */
    },
    {   /* P-error 10 bar */
		0, 													/* S16_RATE_CMP_R_GAIN_BY_P_ERR_3(0) */
		20,													/* S16_RATE_CMP_R_GAIN_BY_P_ERR_3(1) */
		40,													/* S16_RATE_CMP_R_GAIN_BY_P_ERR_3(2) */
		60,													/* S16_RATE_CMP_R_GAIN_BY_P_ERR_3(3) */
		90 													/* S16_RATE_CMP_R_GAIN_BY_P_ERR_3(4) */
    },
    {
		0,									/* S16_RATE_CMP_R_GAIN_P_LVL(0) */
		-100,								/* S16_RATE_CMP_R_GAIN_P_LVL(1) */
		-200,								/* S16_RATE_CMP_R_GAIN_P_LVL(2) */
		-300,								/* S16_RATE_CMP_R_GAIN_P_LVL(3) */
		-500,								/* S16_RATE_CMP_R_GAIN_P_LVL(4) */
    }
};
