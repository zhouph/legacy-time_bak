DATA_PRESSERRGAIN_t           apCalPressErrGain =
{
	{	/* Normal(BBS) Perror Compensation Rate - Percent Value[%] */
		3,													/* S16_PERROR_CMP_GAIN_L0 */
		5,													/* S16_PERROR_CMP_GAIN_L1 */
		5,													/* S16_PERROR_CMP_GAIN_L2 */
		5,													/* S16_PERROR_CMP_GAIN_L3 */
		3,													/* S16_PERROR_CMP_GAIN_L4 */
		3,													/* S16_PERROR_CMP_GAIN_L5 */
		3													/* S16_PERROR_CMP_GAIN_L6 */
	},
	{	/* Quick Break Perror Compensation Rate - Percent Value[%] */
		7,													/* S16_PERROR_CMP_GAIN_L0 */
		12,													/* S16_PERROR_CMP_GAIN_L1 */
		12,													/* S16_PERROR_CMP_GAIN_L2 */
		12,													/* S16_PERROR_CMP_GAIN_L3 */
		7,													/* S16_PERROR_CMP_GAIN_L4 */
		5,													/* S16_PERROR_CMP_GAIN_L5 */
		5													/* S16_PERROR_CMP_GAIN_L6 */
	},
	{	/* Safety Break(ESC/TCS) Perror Compensation Rate - Percent Value[%] */
		6,													/* S16_PERROR_CMP_GAIN_L0 */
		6,													/* S16_PERROR_CMP_GAIN_L1 */
		6/*6*/,													/* S16_PERROR_CMP_GAIN_L2 */
		6/*6*/,													/* S16_PERROR_CMP_GAIN_L3 */
		6/*4*/,													/* S16_PERROR_CMP_GAIN_L4 */
		3/*3*/,													/* S16_PERROR_CMP_GAIN_L5 */
		3/*3*/													/* S16_PERROR_CMP_GAIN_L6 */
	},
	{	/* Pressure Gain Band */
		S16_PCTRL_PRESS_0_BAR,								/* u16PressBand[0] */
		S16_PCTRL_PRESS_2_BAR,								/* u16PressBand[1] */
		S16_PCTRL_PRESS_5_BAR,								/* u16PressBand[2] */
		S16_PCTRL_PRESS_10_BAR,								/* u16PressBand[3] */
		S16_PCTRL_PRESS_15_BAR,								/* u16PressBand[4] */
		S16_PCTRL_PRESS_20_BAR,								/* u16PressBand[5] */
		S16_PCTRL_PRESS_40_BAR								/* u16PressBand[6] */
	},
	{	/* Pressure Error Compensation Gain with Perror Band[%] */
		110,													/* S16_PERROR_CMP_GAIN_L0 */
		100,													/* S16_PERROR_CMP_GAIN_L1 */
		50,														/* S16_PERROR_CMP_GAIN_L2 */
		100,													/* S16_PERROR_CMP_GAIN_L3 */
		150,													/* S16_PERROR_CMP_GAIN_L4 */
	},
	{	/* Pressure Error Band */
		-S16_PCTRL_PRESS_3_BAR,													/* S16_PERROR_CMP_GAIN_L0 */
		-S16_PCTRL_PRESS_1_BAR,													/* S16_PERROR_CMP_GAIN_L1 */
		S16_PCTRL_PRESS_0_BAR,													/* S16_PERROR_CMP_GAIN_L2 */
		S16_PCTRL_PRESS_1_BAR,													/* S16_PERROR_CMP_GAIN_L3 */
		S16_PCTRL_PRESS_3_BAR													/* S16_PERROR_CMP_GAIN_L4 */
	},
	{	/* Pressure Error Band - Quick*/
		-S16_PCTRL_PRESS_5_BAR,													/* S16_PERROR_CMP_GAIN_L0 */
		-S16_PCTRL_PRESS_2_BAR,													/* S16_PERROR_CMP_GAIN_L1 */
		S16_PCTRL_PRESS_0_BAR,													/* S16_PERROR_CMP_GAIN_L2 */
		S16_PCTRL_PRESS_1_BAR,													/* S16_PERROR_CMP_GAIN_L3 */
		S16_PCTRL_PRESS_3_BAR													/* S16_PERROR_CMP_GAIN_L4 */
	},
};
