/**
 * @defgroup Pct_Cal Pct_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Pct_Cal.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PCT_CAL_H_
#define PCT_CAL_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Pct_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
#define U16_PRESS_BAND_ARRAY_MAX					(7)
#define U16_PERROR_BAND_ARRAY_MAX					(5)
#define U16_SPEED_BAND_ARRAY_MAX					(3)
#define U16_RATE_CMP_P_BAND_ARY_MAX					(5)
#define U16_RATE_CMP_THD_P_BND_ARY_MAX				(4)
#define U16_RATE_CMP_P_ERROR_BAND_MAX				(4)
#define U16_LIM_DEL_STRK_TAR_ARY_MAX				(5)
#define U16_MAX_WHEEL_CONTROL_STATE						(5)
#define U16_MAX_MAP_PRESSURE_INDEX						(181)

/* Calibration Definition ***************************************************/
#define S16_AP_CAL_PCTRL_CAL                            (PressCTRLcalib.apCalPCtrlCal)
#define S16_AP_CAL_VALVE_CAL                            (PressCTRLcalib.apCalValveCal)
#define S16_AP_CAL_PRESS_ERR_GAIN                       (PressCTRLcalib.apCalPressErrGain)
#define S16_AP_CAL_APPLY_RATE_ERR_GAIN                  (PressCTRLcalib.apCalApplyRateErrGainByPerror)
#define S16_AP_CAL_RELEASE_RATE_ERR_GAIN                (PressCTRLcalib.apCalReleaseRateErrGainByPerror)
#define S16_AP_CAL_APPLY_RATE_ERR_THD                   (PressCTRLcalib.apCalApplyRateErrCompThres)
#define S16_AP_CAL_RELEASE_RATE_ERR_THD                 (PressCTRLcalib.apCalReleaseRateErrCompThres)
#define S16_AP_CAL_DEL_STRK_TAR_LIMIT                   (PressCTRLcalib.apCalDelStrokeTargetLimit)
#define U16_AP_CAL_DELTA_STRK_MAP                       (PressCTRLcalib.apCalPCtrlDeltaStrokeMap)

#define U8_VEHICLE_CREEPING_SPEED                       (S16_AP_CAL_PCTRL_CAL->u8VehicleCreepingSpeed)
#define S16_ABS_LIMIT_SPEED                             (S16_AP_CAL_PCTRL_CAL->s16AbsLimitSpeed)
#define S16_CVV_CLOSE_DELAY_PSP_THR_H                   (S16_AP_CAL_PCTRL_CAL->s16CvvCloseDelayPspThrH)
#define S16_CVV_CLOSE_DELAY_PSP_THR_L                   (S16_AP_CAL_PCTRL_CAL->s16CvvCloseDelayPspThrL)

#define	U16_CV_ON_TIME_LSD                              (S16_AP_CAL_VALVE_CAL->s16CvOnTimeLsd)
#define U16_CV_ON_TIME_P_GEAR                           (S16_AP_CAL_VALVE_CAL->u16CvOnTimePGear)
#define	S16_CVV_MNT_END_BCP_DIFF_REF                    (S16_AP_CAL_VALVE_CAL->s16CvvMntEndBcpDiffRef)
#define	S16_CVV_MNT_END_BCP_REF                         (S16_AP_CAL_VALVE_CAL->s16CvvMntEndBcpRef)
#define U16_PDV_OPEN_DELAY_AF_BALV_CLOSED				(S16_AP_CAL_VALVE_CAL->u16PdvOpenDelayAfBalvClosed)

#define S16_PRESS_CMP_GAIN(index)						(S16_AP_CAL_PRESS_ERR_GAIN->s16PressCompGain[index])
#define S16_QUICK_PRESS_CMP_GAIN(index)					(S16_AP_CAL_PRESS_ERR_GAIN->s16QuickPressCompGain[index])
#define S16_SAFETY_CMP_GAIN(index)						(S16_AP_CAL_PRESS_ERR_GAIN->s16SafetyCompGain[index])
#define S16_PRESS_BAND(index)							(S16_AP_CAL_PRESS_ERR_GAIN->s16PressBand[index])
#define S16_PRESS_CMP_GAIN_P_ERR_BAND(index)			(S16_AP_CAL_PRESS_ERR_GAIN->s16PressCompGainWithPerrorBand[index])
#define S16_P_ERROR_BAND(index)							(S16_AP_CAL_PRESS_ERR_GAIN->s16PerrorBand[index])
#define S16_P_ERROR_BAND_QUICK(index)					(S16_AP_CAL_PRESS_ERR_GAIN->s16PerrorBandQuick[index])

#define S16_RATE_CMP_A_GAIN_BY_P_ERR_0(index)			(S16_AP_CAL_APPLY_RATE_ERR_GAIN->s16GainByPressError0[index])
#define S16_RATE_CMP_A_GAIN_BY_P_ERR_1(index)			(S16_AP_CAL_APPLY_RATE_ERR_GAIN->s16GainByPressError1[index])
#define S16_RATE_CMP_A_GAIN_BY_P_ERR_2(index)			(S16_AP_CAL_APPLY_RATE_ERR_GAIN->s16GainByPressError2[index])
#define S16_RATE_CMP_A_GAIN_BY_P_ERR_3(index)			(S16_AP_CAL_APPLY_RATE_ERR_GAIN->s16GainByPressError3[index])
#define S16_RATE_CMP_A_GAIN_P_LVL(index)				(S16_AP_CAL_APPLY_RATE_ERR_GAIN->s16RateCompGainPressBand[index])

#define S16_RATE_CMP_R_GAIN_BY_P_ERR_0(index)			(S16_AP_CAL_RELEASE_RATE_ERR_GAIN->s16GainByPressError0[index])
#define S16_RATE_CMP_R_GAIN_BY_P_ERR_1(index)			(S16_AP_CAL_RELEASE_RATE_ERR_GAIN->s16GainByPressError1[index])
#define S16_RATE_CMP_R_GAIN_BY_P_ERR_2(index)			(S16_AP_CAL_RELEASE_RATE_ERR_GAIN->s16GainByPressError2[index])
#define S16_RATE_CMP_R_GAIN_BY_P_ERR_3(index)			(S16_AP_CAL_RELEASE_RATE_ERR_GAIN->s16GainByPressError3[index])
#define S16_RATE_CMP_R_GAIN_P_LVL(index)				(S16_AP_CAL_RELEASE_RATE_ERR_GAIN->s16RateCompGainPressBand[index])

#define S16_RATE_CMP_A_THD_BY_P_ERR(index)				(S16_AP_CAL_APPLY_RATE_ERR_THD->s16RateCmpThdByPerror[index])
#define S16_RATE_CMP_A_THD_BY_P_ERR_QCK(index)			(S16_AP_CAL_APPLY_RATE_ERR_THD->s16RateCmpThdByPerrQCK[index])
#define S16_RATE_CMP_A_THD_P_ERR_LVL(index)				(S16_AP_CAL_APPLY_RATE_ERR_THD->s16RateCmpThdPressLvl[index])

#define S16_RATE_CMP_R_THD_BY_P_ERR(index)				(S16_AP_CAL_RELEASE_RATE_ERR_THD->s16RateCmpThdByPerror[index])
#define S16_RATE_CMP_R_THD_BY_P_ERR_QCK(index)			(S16_AP_CAL_RELEASE_RATE_ERR_THD->s16RateCmpThdByPerrQCK[index])
#define S16_RATE_CMP_R_THD_P_ERR_LVL(index)				(S16_AP_CAL_RELEASE_RATE_ERR_THD->s16RateCmpThdPressLvl[index])

#define S16_DEL_STRK_TAR_LIM_P_ERR_THD(index)			(S16_AP_CAL_DEL_STRK_TAR_LIMIT->s16DelStrkTarLimThd[index])
#define S16_DEL_STRK_TAR_LIM_P_LVL(index)				(S16_AP_CAL_DEL_STRK_TAR_LIMIT->s16DelStrkTarLimPLvl[index])

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
    uint8_t        u8VehicleCreepingSpeed;          /* Comment [ Vehicle creeping speed (default 5kph) ] */
                                                        /* Group   [ /apCalAhb/Ahb Logic Tuning/Vehicle speed setting ] */
                                                        /* Scale   [ f(x) = (X + 0) * 0.125 ]  Range [ 0 <= X <= 31.875 ] */
    int16_t         s16AbsLimitSpeed;                /* Comment [ ABS limit speed(default 5kph) ] */
                                                        /* Group   [ /apCalAhb/Ahb Logic Tuning/Vehicle speed setting ] */
                                                        /* Scale   [ f(x) = (X + 0) * 0.125 ]  Range [ -4096 <= X <= 4095.875 ] */
    int16_t         s16CvvCloseDelayPspThrH;      /* Comment [ Fast Apply 감지시 Cut V/V Close 유보 PSP High Threshold ] */
                                                        /* Group   [ /apCalAhb/Ahb Logic Tuning/Fast Pedal Apply Threshold Tuning ] */
                                                        /* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ 0 <= X <= 15 ] */
    int16_t         s16CvvCloseDelayPspThrL;      /* Comment [ Fast Apply 감지시 Cut V/V Close 유보 PSP Low Threshold ] */
                                                        /* Group   [ /apCalAhb/Ahb Logic Tuning/Fast Pedal Apply Threshold Tuning ] */
                                                        /* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ 0 <= X <= 15 ] */

}DATA_PCTRLCAL_t;

typedef struct
{
	uint16_t    	s16CvOnTimeLsd;                	/* Comment [ 정차 또는 저속에서 Cut Valve 유예 시간(Noise 저감) ] */
	            	                                   	/* Group   [ /apCalAhb/Ahb Logic Tuning/AHB Controller Logic Tuning/AHB Press Remain ] */
	            	                                   	/* Scale   [ f(x) = (X + 0) * 0.005 ]  Range [ 0 <= X <= 327.675 ] */
	uint16_t    	u16CvOnTimePGear;             	/* Comment [ P단 에서 cut valve 유예시간 ] */
	            	                                   	/* Group   [ /apCalAhb/Ahb Logic Tuning/AHB Controller Logic Tuning/AHB Press Remain ] */
	            	                                   	/* Scale   [ f(x) = (X + 0) * 0.005 ]  Range [ 0 <= X <= 327.675 ] */
	int16_t     	s16CvvMntEndBcpDiffRef;      	/* Comment [ Cut Valve 유예를 종료하는 Circuit Pressure 상승 압력  ] */
	            	                                   	/* Group   [ /apCalAhb/Ahb Logic Tuning/AHB Controller Logic Tuning/AHB Press Remain ] */
	            	                                   	/* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ -3276.8 <= X <= 3276.7 ] */
	int16_t     	s16CvvMntEndBcpRef;           	/* Comment [ Cut Valve 유예를 종료하는 Circuit Pressure 압력 ] */
		            	                                   	/* Group   [ /apCalAhb/Ahb Logic Tuning/AHB Controller Logic Tuning/AHB Press Remain ] */
		            	                                   	/* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ -3276.8 <= X <= 3276.7 ] */
	uint16_t 		u16PdvOpenDelayAfBalvClosed;

} DATA_VALVECAL_t;

typedef struct {
	int16_t s16PressCompGain[U16_PRESS_BAND_ARRAY_MAX];
	int16_t s16QuickPressCompGain[U16_PRESS_BAND_ARRAY_MAX];
	int16_t s16SafetyCompGain[U16_PRESS_BAND_ARRAY_MAX];
	int16_t s16PressBand[U16_PRESS_BAND_ARRAY_MAX];
	int16_t s16PressCompGainWithPerrorBand[U16_PERROR_BAND_ARRAY_MAX];
	int16_t s16PerrorBand[U16_PERROR_BAND_ARRAY_MAX];
	int16_t s16PerrorBandQuick[U16_PERROR_BAND_ARRAY_MAX];
}DATA_PRESSERRGAIN_t;

typedef struct {
	int16_t s16GainByPressError0[U16_RATE_CMP_P_BAND_ARY_MAX];
	int16_t s16GainByPressError1[U16_RATE_CMP_P_BAND_ARY_MAX];
	int16_t s16GainByPressError2[U16_RATE_CMP_P_BAND_ARY_MAX];
	int16_t s16GainByPressError3[U16_RATE_CMP_P_BAND_ARY_MAX];
	int16_t s16RateCompGainPressBand[U16_RATE_CMP_P_BAND_ARY_MAX];
}DATA_PRESSRATEERRGAIN_t;

typedef struct {
	int16_t s16RateCmpThdByPerror[U16_RATE_CMP_THD_P_BND_ARY_MAX];
	int16_t s16RateCmpThdByPerrQCK[U16_RATE_CMP_THD_P_BND_ARY_MAX];
	int16_t s16RateCmpThdPressLvl[U16_RATE_CMP_THD_P_BND_ARY_MAX];
}DATA_RATEERRCOMPTHRES_t;

typedef struct {
	int16_t s16DelStrkTarLimThd[U16_LIM_DEL_STRK_TAR_ARY_MAX];
	int16_t s16DelStrkTarLimPLvl[U16_LIM_DEL_STRK_TAR_ARY_MAX];
}DATA_DELSTROKETARGETLIMIT_t;

typedef struct {
	uint16_t u16PressureMap[U16_MAX_WHEEL_CONTROL_STATE][U16_MAX_MAP_PRESSURE_INDEX];
}DATA_PCTRL_STROKE_MAP_t;

typedef struct
{
    DATA_PCTRLCAL_t *                 apCalPCtrlCal;
    DATA_VALVECAL_t *                 apCalValveCal;
    DATA_PRESSERRGAIN_t *             apCalPressErrGain;
    DATA_PRESSRATEERRGAIN_t *         apCalApplyRateErrGainByPerror;
    DATA_PRESSRATEERRGAIN_t *         apCalReleaseRateErrGainByPerror;
    DATA_RATEERRCOMPTHRES_t *         apCalApplyRateErrCompThres;
    DATA_RATEERRCOMPTHRES_t *         apCalReleaseRateErrCompThres;
    DATA_DELSTROKETARGETLIMIT_t *     apCalDelStrokeTargetLimit;
    DATA_PCTRL_STROKE_MAP_t *         apCalPCtrlDeltaStrokeMap;
} PressureControl_CalibType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
extern PressureControl_CalibType PressCTRLcalib;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PCT_CAL_H_ */
/** @} */
