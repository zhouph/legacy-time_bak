/**
 * @defgroup Pct_Cal Pct_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Pct_Cal.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Pct_Cal.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PCT_START_SEC_CALIB_UNSPECIFIED
#include "Pct_MemMap.h"
/* Global Calibration Section */


#define PCT_STOP_SEC_CALIB_UNSPECIFIED
#include "Pct_MemMap.h"

#define PCT_START_SEC_CONST_UNSPECIFIED
#include "Pct_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PCT_STOP_SEC_CONST_UNSPECIFIED
#include "Pct_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PCT_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PCT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
#define PCT_START_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PCT_STOP_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
#define PCT_START_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#if (__CAR == HMC_YFE)
    #include "YFE/PressureControl_Cal_PCtrl_YFE.h"
    #include "YFE/PressureControl_Cal_ValveCal_YFE.h"
    #include "YFE/PressureControl_Cal_PressErrorGain_YFE.h"
    #include "YFE/PressureControl_Cal_ApplyRateErrCompThres_YFE.h"
    #include "YFE/PressureControl_Cal_ReleaseRateErrCompThres_YFE.h"
    #include "YFE/PressureControl_Cal_ApplyRateErrGainByPerror_YFE.h"
    #include "YFE/PressureControl_Cal_ReleaseRateErrGainByPerror_YFE.h"
    #include "YFE/PressureControl_Cal_DelStrokeTargetLimit_YFE.h"
    #include "YFE/PressureControl_Cal_PCtrl_StrokeMap_YFE.h"
#elif (__CAR == KMC_TFE)
	#include "TFE/PressureControl_Cal_PCtrl_TFE.h"
    #include "TFE/PressureControl_Cal_ValveCal_TFE.h"
    #include "TFE/PressureControl_Cal_PressErrorGain_TFE.h"
    #include "TFE/PressureControl_Cal_ApplyRateErrCompThres_TFE.h"
    #include "TFE/PressureControl_Cal_ReleaseRateErrCompThres_TFE.h"
    #include "TFE/PressureControl_Cal_ApplyRateErrGainByPerror_TFE.h"
    #include "TFE/PressureControl_Cal_ReleaseRateErrGainByPerror_TFE.h"
    #include "TFE/PressureControl_Cal_DelStrokeTargetLimit_TFE.h"
    #include "TFE/PressureControl_Cal_PCtrl_StrokeMap_TFE.h"
#elif (__CAR == HMC_HGE)
	#include "HGE/PressureControl_Cal_PCtrl_HGE.h"
    #include "HGE/PressureControl_Cal_ValveCal_HGE.h"
    #include "HGE/PressureControl_Cal_PressErrorGain_HGE.h"
    #include "HGE/PressureControl_Cal_ApplyRateErrCompThres_HGE.h"
    #include "HGE/PressureControl_Cal_ReleaseRateErrCompThres_HGE.h"
    #include "HGE/PressureControl_Cal_ApplyRateErrGainByPerror_HGE.h"
    #include "HGE/PressureControl_Cal_ReleaseRateErrGainByPerror_HGE.h"
    #include "HGE/PressureControl_Cal_DelStrokeTargetLimit_HGE.h"
    #include "HGE/PressureControl_Cal_PCtrl_StrokeMap_HGE.h"
#endif

PressureControl_CalibType PressCTRLcalib =
{
    &apCalPCtrlCal,
    &apCalValveCal,
    &apCalPressErrGain,     /* calPRESS_ERR_GAIN        */
    &apCalApplyRateErrGainByPerror,
    &apCalReleaseRateErrGainByPerror,
    &apCalApplyRateErrCompThres,
    &apCalReleaseRateErrCompThres,
    &apCalDelStrokeTargetLimit,
    &apCalPCtrlDeltaStrokeMap
};
#define PCT_STOP_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
#define PCT_START_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/** Variable Section (32BIT)**/


#define PCT_STOP_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PCT_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PCT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Pct_MemMap.h"
#define PCT_START_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PCT_STOP_SEC_VAR_NOINIT_32BIT
#include "Pct_MemMap.h"
#define PCT_START_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PCT_STOP_SEC_VAR_UNSPECIFIED
#include "Pct_MemMap.h"
#define PCT_START_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/** Variable Section (32BIT)**/


#define PCT_STOP_SEC_VAR_32BIT
#include "Pct_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PCT_START_SEC_CODE
#include "Pct_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define PCT_STOP_SEC_CODE
#include "Pct_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
