Pct_5msCtrlWhlVlvReqAbcInfo = Simulink.Bus;
DeList={
    'FlIvReqData_array_0'
    'FlIvReqData_array_1'
    'FlIvReqData_array_2'
    'FlIvReqData_array_3'
    'FlIvReqData_array_4'
    'FlIvReqData_array_5'
    'FlIvReqData_array_6'
    'FlIvReqData_array_7'
    'FlIvReqData_array_8'
    'FlIvReqData_array_9'
    'FrIvReqData_array_0'
    'FrIvReqData_array_1'
    'FrIvReqData_array_2'
    'FrIvReqData_array_3'
    'FrIvReqData_array_4'
    'FrIvReqData_array_5'
    'FrIvReqData_array_6'
    'FrIvReqData_array_7'
    'FrIvReqData_array_8'
    'FrIvReqData_array_9'
    'RlIvReqData_array_0'
    'RlIvReqData_array_1'
    'RlIvReqData_array_2'
    'RlIvReqData_array_3'
    'RlIvReqData_array_4'
    'RlIvReqData_array_5'
    'RlIvReqData_array_6'
    'RlIvReqData_array_7'
    'RlIvReqData_array_8'
    'RlIvReqData_array_9'
    'RrIvReqData_array_0'
    'RrIvReqData_array_1'
    'RrIvReqData_array_2'
    'RrIvReqData_array_3'
    'RrIvReqData_array_4'
    'RrIvReqData_array_5'
    'RrIvReqData_array_6'
    'RrIvReqData_array_7'
    'RrIvReqData_array_8'
    'RrIvReqData_array_9'
    'FlIvReq'
    'FrIvReq'
    'RlIvReq'
    'RrIvReq'
    };
Pct_5msCtrlWhlVlvReqAbcInfo = CreateBus(Pct_5msCtrlWhlVlvReqAbcInfo, DeList);
clear DeList;

Pct_5msCtrlEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_SimP'
    'Eem_Fail_BLS'
    };
Pct_5msCtrlEemFailData = CreateBus(Pct_5msCtrlEemFailData, DeList);
clear DeList;

Pct_5msCtrlPedlTrvlFild1msInfo = Simulink.Bus;
DeList={
    'PdtFild1ms'
    };
Pct_5msCtrlPedlTrvlFild1msInfo = CreateBus(Pct_5msCtrlPedlTrvlFild1msInfo, DeList);
clear DeList;

Pct_5msCtrlCanRxAccelPedlInfo = Simulink.Bus;
DeList={
    'AccelPedlVal'
    'AccelPedlValErr'
    };
Pct_5msCtrlCanRxAccelPedlInfo = CreateBus(Pct_5msCtrlCanRxAccelPedlInfo, DeList);
clear DeList;

Pct_5msCtrlMotRotgAgSigInfo = Simulink.Bus;
DeList={
    'StkPosnMeasd'
    };
Pct_5msCtrlMotRotgAgSigInfo = CreateBus(Pct_5msCtrlMotRotgAgSigInfo, DeList);
clear DeList;

Pct_5msCtrlAbsCtrlInfo = Simulink.Bus;
DeList={
    'AbsActFlg'
    'AbsDefectFlg'
    'AbsTarPFrntLe'
    'AbsTarPFrntRi'
    'AbsTarPReLe'
    'AbsTarPReRi'
    'FrntWhlSlip'
    'AbsDesTarP'
    'AbsDesTarPReqFlg'
    'AbsCtrlFadeOutFlg'
    };
Pct_5msCtrlAbsCtrlInfo = CreateBus(Pct_5msCtrlAbsCtrlInfo, DeList);
clear DeList;

Pct_5msCtrlAvhCtrlInfo = Simulink.Bus;
DeList={
    'AvhActFlg'
    'AvhDefectFlg'
    'AvhTarP'
    };
Pct_5msCtrlAvhCtrlInfo = CreateBus(Pct_5msCtrlAvhCtrlInfo, DeList);
clear DeList;

Pct_5msCtrlEscCtrlInfo = Simulink.Bus;
DeList={
    'EscActFlg'
    'EscDefectFlg'
    'EscTarPFrntLe'
    'EscTarPFrntRi'
    'EscTarPReLe'
    'EscTarPReRi'
    };
Pct_5msCtrlEscCtrlInfo = CreateBus(Pct_5msCtrlEscCtrlInfo, DeList);
clear DeList;

Pct_5msCtrlHsaCtrlInfo = Simulink.Bus;
DeList={
    'HsaActFlg'
    'HsaDefectFlg'
    'HsaTarP'
    };
Pct_5msCtrlHsaCtrlInfo = CreateBus(Pct_5msCtrlHsaCtrlInfo, DeList);
clear DeList;

Pct_5msCtrlStkRecvryCtrlIfInfo = Simulink.Bus;
DeList={
    'StkRcvrEscAllowedTime'
    'StkRcvrAbsAllowedTime'
    'StkRcvrTcsAllowedTime'
    };
Pct_5msCtrlStkRecvryCtrlIfInfo = CreateBus(Pct_5msCtrlStkRecvryCtrlIfInfo, DeList);
clear DeList;

Pct_5msCtrlTcsCtrlInfo = Simulink.Bus;
DeList={
    'TcsActFlg'
    'TcsDefectFlg'
    'TcsTarPFrntLe'
    'TcsTarPFrntRi'
    'TcsTarPReLe'
    'TcsTarPReRi'
    'BothDrvgWhlBrkCtlReqFlg'
    };
Pct_5msCtrlTcsCtrlInfo = CreateBus(Pct_5msCtrlTcsCtrlInfo, DeList);
clear DeList;

Pct_5msCtrlBaseBrkCtrlModInfo = Simulink.Bus;
DeList={
    'VehStandStillStFlg'
    };
Pct_5msCtrlBaseBrkCtrlModInfo = CreateBus(Pct_5msCtrlBaseBrkCtrlModInfo, DeList);
clear DeList;

Pct_5msCtrlBrkPedlStatusInfo = Simulink.Bus;
DeList={
    'DrvrIntendToRelsBrk'
    'PanicBrkStFlg'
    'PedlReldStFlg2'
    };
Pct_5msCtrlBrkPedlStatusInfo = CreateBus(Pct_5msCtrlBrkPedlStatusInfo, DeList);
clear DeList;

Pct_5msCtrlBrkPedlStatus1msInfo = Simulink.Bus;
DeList={
    'PanicBrkSt1msFlg'
    };
Pct_5msCtrlBrkPedlStatus1msInfo = CreateBus(Pct_5msCtrlBrkPedlStatus1msInfo, DeList);
clear DeList;

Pct_5msCtrlCircPFildInfo = Simulink.Bus;
DeList={
    'PrimCircPFild_1_100Bar'
    'SecdCircPFild_1_100Bar'
    };
Pct_5msCtrlCircPFildInfo = CreateBus(Pct_5msCtrlCircPFildInfo, DeList);
clear DeList;

Pct_5msCtrlCircPRateInfo = Simulink.Bus;
DeList={
    'PrimCircPChgDurg10ms'
    'SecdCircPChgDurg10ms'
    };
Pct_5msCtrlCircPRateInfo = CreateBus(Pct_5msCtrlCircPRateInfo, DeList);
clear DeList;

Pct_5msCtrlPedlTrvlRate1msInfo = Simulink.Bus;
DeList={
    'PedlTrvlRate1msMmPerSec'
    };
Pct_5msCtrlPedlTrvlRate1msInfo = CreateBus(Pct_5msCtrlPedlTrvlRate1msInfo, DeList);
clear DeList;

Pct_5msCtrlEstimdWhlPInfo = Simulink.Bus;
DeList={
    'FrntLeEstimdWhlP'
    'FrntRiEstimdWhlP'
    'ReLeEstimdWhlP'
    'ReRiEstimdWhlP'
    };
Pct_5msCtrlEstimdWhlPInfo = CreateBus(Pct_5msCtrlEstimdWhlPInfo, DeList);
clear DeList;

Pct_5msCtrlFinalTarPInfo = Simulink.Bus;
DeList={
    'FinalTarPOfWhlFrntLe'
    'FinalTarPOfWhlFrntRi'
    'FinalTarPOfWhlReLe'
    'FinalTarPOfWhlReRi'
    'FinalTarPRateOfWhlFrntLe'
    'FinalTarPRateOfWhlFrntRi'
    'FinalTarPRateOfWhlReLe'
    'FinalTarPRateOfWhlReRi'
    'PCtrlPrioOfWhlFrntLe'
    'PCtrlPrioOfWhlFrntRi'
    'PCtrlPrioOfWhlReLe'
    'PCtrlPrioOfWhlReRi'
    'CtrlModOfWhlFrntLe'
    'CtrlModOfOfWhlFrntRi'
    'CtrlModOfWhlReLe'
    'CtrlModOfWhlReRi'
    'ReqdPCtrlBoostMod'
    'FinalDelTarPOfWhlFrntLe'
    'FinalDelTarPOfWhlFrntRi'
    'FinalDelTarPOfWhlReLe'
    'FinalDelTarPOfWhlReRi'
    };
Pct_5msCtrlFinalTarPInfo = CreateBus(Pct_5msCtrlFinalTarPInfo, DeList);
clear DeList;

Pct_5msCtrlIdbMotPosnInfo = Simulink.Bus;
DeList={
    'MotTarPosn'
    };
Pct_5msCtrlIdbMotPosnInfo = CreateBus(Pct_5msCtrlIdbMotPosnInfo, DeList);
clear DeList;

Pct_5msCtrlWhlOutVlvCtrlTarInfo = Simulink.Bus;
DeList={
    'FlOutVlvCtrlTar_array_0'
    'FlOutVlvCtrlTar_array_1'
    'FlOutVlvCtrlTar_array_2'
    'FlOutVlvCtrlTar_array_3'
    'FlOutVlvCtrlTar_array_4'
    'FrOutVlvCtrlTar_array_0'
    'FrOutVlvCtrlTar_array_1'
    'FrOutVlvCtrlTar_array_2'
    'FrOutVlvCtrlTar_array_3'
    'FrOutVlvCtrlTar_array_4'
    'RlOutVlvCtrlTar_array_0'
    'RlOutVlvCtrlTar_array_1'
    'RlOutVlvCtrlTar_array_2'
    'RlOutVlvCtrlTar_array_3'
    'RlOutVlvCtrlTar_array_4'
    'RrOutVlvCtrlTar_array_0'
    'RrOutVlvCtrlTar_array_1'
    'RrOutVlvCtrlTar_array_2'
    'RrOutVlvCtrlTar_array_3'
    'RrOutVlvCtrlTar_array_4'
    };
Pct_5msCtrlWhlOutVlvCtrlTarInfo = CreateBus(Pct_5msCtrlWhlOutVlvCtrlTarInfo, DeList);
clear DeList;

Pct_5msCtrlWhlPreFillInfo = Simulink.Bus;
DeList={
    'FlPreFillActFlg'
    'FrPreFillActFlg'
    'RlPreFillActFlg'
    'RrPreFillActFlg'
    };
Pct_5msCtrlWhlPreFillInfo = CreateBus(Pct_5msCtrlWhlPreFillInfo, DeList);
clear DeList;

Pct_5msCtrlIdbVlvActInfo = Simulink.Bus;
DeList={
    'VlvFadeoutEndOK'
    'CutVlvOpenFlg'
    };
Pct_5msCtrlIdbVlvActInfo = CreateBus(Pct_5msCtrlIdbVlvActInfo, DeList);
clear DeList;

Pct_5msCtrlPedlSimrPFildInfo = Simulink.Bus;
DeList={
    'PedlSimrPFild_1_100Bar'
    };
Pct_5msCtrlPedlSimrPFildInfo = CreateBus(Pct_5msCtrlPedlSimrPFildInfo, DeList);
clear DeList;

Pct_5msCtrlPedlTrvlRateInfo = Simulink.Bus;
DeList={
    'PedlTrvlRateMilliMtrPerSec'
    };
Pct_5msCtrlPedlTrvlRateInfo = CreateBus(Pct_5msCtrlPedlTrvlRateInfo, DeList);
clear DeList;

Pct_5msCtrlPedlTrvlTagInfo = Simulink.Bus;
DeList={
    'PedlSigTag'
    };
Pct_5msCtrlPedlTrvlTagInfo = CreateBus(Pct_5msCtrlPedlTrvlTagInfo, DeList);
clear DeList;

Pct_5msCtrlPistPFildInfo = Simulink.Bus;
DeList={
    'PistPFild_1_100Bar'
    };
Pct_5msCtrlPistPFildInfo = CreateBus(Pct_5msCtrlPistPFildInfo, DeList);
clear DeList;

Pct_5msCtrlPistPRateInfo = Simulink.Bus;
DeList={
    'PistPChgDurg10ms'
    };
Pct_5msCtrlPistPRateInfo = CreateBus(Pct_5msCtrlPistPRateInfo, DeList);
clear DeList;

Pct_5msCtrlVlvActtnReqByBaseBrkCtrlrInfo = Simulink.Bus;
DeList={
    'PedlSimrPGendOnFlg'
    };
Pct_5msCtrlVlvActtnReqByBaseBrkCtrlrInfo = CreateBus(Pct_5msCtrlVlvActtnReqByBaseBrkCtrlrInfo, DeList);
clear DeList;

Pct_5msCtrlWhlSpdFildInfo = Simulink.Bus;
DeList={
    'WhlSpdFildReLe'
    'WhlSpdFildReRi'
    };
Pct_5msCtrlWhlSpdFildInfo = CreateBus(Pct_5msCtrlWhlSpdFildInfo, DeList);
clear DeList;

Pct_5msCtrlEemSuspectData = Simulink.Bus;
DeList={
    'Eem_Suspect_SimP'
    };
Pct_5msCtrlEemSuspectData = CreateBus(Pct_5msCtrlEemSuspectData, DeList);
clear DeList;

Pct_5msCtrlEcuModeSts = Simulink.Bus;
DeList={'Pct_5msCtrlEcuModeSts'};
Pct_5msCtrlEcuModeSts = CreateBus(Pct_5msCtrlEcuModeSts, DeList);
clear DeList;

Pct_5msCtrlMotCtrlMode = Simulink.Bus;
DeList={'Pct_5msCtrlMotCtrlMode'};
Pct_5msCtrlMotCtrlMode = CreateBus(Pct_5msCtrlMotCtrlMode, DeList);
clear DeList;

Pct_5msCtrlFuncInhibitPctrlSts = Simulink.Bus;
DeList={'Pct_5msCtrlFuncInhibitPctrlSts'};
Pct_5msCtrlFuncInhibitPctrlSts = CreateBus(Pct_5msCtrlFuncInhibitPctrlSts, DeList);
clear DeList;

Pct_5msCtrlBaseBrkCtrlrActFlg = Simulink.Bus;
DeList={'Pct_5msCtrlBaseBrkCtrlrActFlg'};
Pct_5msCtrlBaseBrkCtrlrActFlg = CreateBus(Pct_5msCtrlBaseBrkCtrlrActFlg, DeList);
clear DeList;

Pct_5msCtrlBlsFild = Simulink.Bus;
DeList={'Pct_5msCtrlBlsFild'};
Pct_5msCtrlBlsFild = CreateBus(Pct_5msCtrlBlsFild, DeList);
clear DeList;

Pct_5msCtrlBrkPRednForBaseBrkCtrlr = Simulink.Bus;
DeList={'Pct_5msCtrlBrkPRednForBaseBrkCtrlr'};
Pct_5msCtrlBrkPRednForBaseBrkCtrlr = CreateBus(Pct_5msCtrlBrkPRednForBaseBrkCtrlr, DeList);
clear DeList;

Pct_5msCtrlMotICtrlFadeOutState = Simulink.Bus;
DeList={'Pct_5msCtrlMotICtrlFadeOutState'};
Pct_5msCtrlMotICtrlFadeOutState = CreateBus(Pct_5msCtrlMotICtrlFadeOutState, DeList);
clear DeList;

Pct_5msCtrlPreBoostMod = Simulink.Bus;
DeList={'Pct_5msCtrlPreBoostMod'};
Pct_5msCtrlPreBoostMod = CreateBus(Pct_5msCtrlPreBoostMod, DeList);
clear DeList;

Pct_5msCtrlRgnBrkCtlrBlendgFlg = Simulink.Bus;
DeList={'Pct_5msCtrlRgnBrkCtlrBlendgFlg'};
Pct_5msCtrlRgnBrkCtlrBlendgFlg = CreateBus(Pct_5msCtrlRgnBrkCtlrBlendgFlg, DeList);
clear DeList;

Pct_5msCtrlVehSpdFild = Simulink.Bus;
DeList={'Pct_5msCtrlVehSpdFild'};
Pct_5msCtrlVehSpdFild = CreateBus(Pct_5msCtrlVehSpdFild, DeList);
clear DeList;

Pct_5msCtrlStkRecvryStabnEndOK = Simulink.Bus;
DeList={'Pct_5msCtrlStkRecvryStabnEndOK'};
Pct_5msCtrlStkRecvryStabnEndOK = CreateBus(Pct_5msCtrlStkRecvryStabnEndOK, DeList);
clear DeList;

Pct_5msCtrlIdbPCtrllVlvCtrlStInfo = Simulink.Bus;
DeList={
    'CvVlvSt'
    'RlVlvSt'
    'PdVlvSt'
    'BalVlvSt'
    'CvVlvNoiseCtrlSt'
    'RlVlvNoiseCtrlSt'
    'PdVlvNoiseCtrlSt'
    'BalVlvNoiseCtrlSt'
    'CvVlvHoldTime'
    'RlVlvHoldTime'
    'PdVlvHoldTime'
    'BalVlvHoldTime'
    };
Pct_5msCtrlIdbPCtrllVlvCtrlStInfo = CreateBus(Pct_5msCtrlIdbPCtrllVlvCtrlStInfo, DeList);
clear DeList;

Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo = Simulink.Bus;
DeList={
    'PrimCutVlvSt'
    'SecdCutVlvSt'
    'SimVlvSt'
    'PrimCutVlvNoiseCtrlSt'
    'SecdCutVlvNoiseCtrlSt'
    'SimVlvNoiseCtrlSt'
    'PrimCutVlvHoldTime'
    'SecdCutVlvHoldTime'
    'SimVlvHoldTime'
    'CutVlvLongOpen'
    };
Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo = CreateBus(Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo, DeList);
clear DeList;

Pct_5msCtrlWhlnVlvCtrlTarInfo = Simulink.Bus;
DeList={
    'FlInVlvCtrlTar_array_0'
    'FlInVlvCtrlTar_array_1'
    'FlInVlvCtrlTar_array_2'
    'FlInVlvCtrlTar_array_3'
    'FlInVlvCtrlTar_array_4'
    'FrInVlvCtrlTar_array_0'
    'FrInVlvCtrlTar_array_1'
    'FrInVlvCtrlTar_array_2'
    'FrInVlvCtrlTar_array_3'
    'FrInVlvCtrlTar_array_4'
    'RlInVlvCtrlTar_array_0'
    'RlInVlvCtrlTar_array_1'
    'RlInVlvCtrlTar_array_2'
    'RlInVlvCtrlTar_array_3'
    'RlInVlvCtrlTar_array_4'
    'RrInVlvCtrlTar_array_0'
    'RrInVlvCtrlTar_array_1'
    'RrInVlvCtrlTar_array_2'
    'RrInVlvCtrlTar_array_3'
    'RrInVlvCtrlTar_array_4'
    };
Pct_5msCtrlWhlnVlvCtrlTarInfo = CreateBus(Pct_5msCtrlWhlnVlvCtrlTarInfo, DeList);
clear DeList;

Pct_5msCtrlStkRecvryActnIfInfo = Simulink.Bus;
DeList={
    'RecommendStkRcvrLvl'
    'StkRecvryCtrlState'
    'StkRecvryRequestedTime'
    };
Pct_5msCtrlStkRecvryActnIfInfo = CreateBus(Pct_5msCtrlStkRecvryActnIfInfo, DeList);
clear DeList;

Pct_5msCtrlMuxCmdExecStInfo = Simulink.Bus;
DeList={
    'MuxCmdExecStOfWhlFrntLe'
    'MuxCmdExecStOfWhlFrntRi'
    'MuxCmdExecStOfWhlReLe'
    'MuxCmdExecStOfWhlReRi'
    };
Pct_5msCtrlMuxCmdExecStInfo = CreateBus(Pct_5msCtrlMuxCmdExecStInfo, DeList);
clear DeList;

Pct_5msCtrlPCtrlAct = Simulink.Bus;
DeList={'Pct_5msCtrlPCtrlAct'};
Pct_5msCtrlPCtrlAct = CreateBus(Pct_5msCtrlPCtrlAct, DeList);
clear DeList;

Pct_5msCtrlInitQuickBrkDctFlg = Simulink.Bus;
DeList={'Pct_5msCtrlInitQuickBrkDctFlg'};
Pct_5msCtrlInitQuickBrkDctFlg = CreateBus(Pct_5msCtrlInitQuickBrkDctFlg, DeList);
clear DeList;

Pct_5msCtrlTgtDeltaStkType = Simulink.Bus;
DeList={'Pct_5msCtrlTgtDeltaStkType'};
Pct_5msCtrlTgtDeltaStkType = CreateBus(Pct_5msCtrlTgtDeltaStkType, DeList);
clear DeList;

Pct_5msCtrlPCtrlBoostMod = Simulink.Bus;
DeList={'Pct_5msCtrlPCtrlBoostMod'};
Pct_5msCtrlPCtrlBoostMod = CreateBus(Pct_5msCtrlPCtrlBoostMod, DeList);
clear DeList;

Pct_5msCtrlPCtrlFadeoutSt = Simulink.Bus;
DeList={'Pct_5msCtrlPCtrlFadeoutSt'};
Pct_5msCtrlPCtrlFadeoutSt = CreateBus(Pct_5msCtrlPCtrlFadeoutSt, DeList);
clear DeList;

Pct_5msCtrlPCtrlSt = Simulink.Bus;
DeList={'Pct_5msCtrlPCtrlSt'};
Pct_5msCtrlPCtrlSt = CreateBus(Pct_5msCtrlPCtrlSt, DeList);
clear DeList;

Pct_5msCtrlInVlvAllCloseReq = Simulink.Bus;
DeList={'Pct_5msCtrlInVlvAllCloseReq'};
Pct_5msCtrlInVlvAllCloseReq = CreateBus(Pct_5msCtrlInVlvAllCloseReq, DeList);
clear DeList;

Pct_5msCtrlPChamberVolume = Simulink.Bus;
DeList={'Pct_5msCtrlPChamberVolume'};
Pct_5msCtrlPChamberVolume = CreateBus(Pct_5msCtrlPChamberVolume, DeList);
clear DeList;

Pct_5msCtrlTarDeltaStk = Simulink.Bus;
DeList={'Pct_5msCtrlTarDeltaStk'};
Pct_5msCtrlTarDeltaStk = CreateBus(Pct_5msCtrlTarDeltaStk, DeList);
clear DeList;

Pct_5msCtrlFinalTarPFromPCtrl = Simulink.Bus;
DeList={'Pct_5msCtrlFinalTarPFromPCtrl'};
Pct_5msCtrlFinalTarPFromPCtrl = CreateBus(Pct_5msCtrlFinalTarPFromPCtrl, DeList);
clear DeList;

