Pct_1msCtrlPedlTrvlFild1msInfo = Simulink.Bus;
DeList={
    'PdtFild1ms'
    };
Pct_1msCtrlPedlTrvlFild1msInfo = CreateBus(Pct_1msCtrlPedlTrvlFild1msInfo, DeList);
clear DeList;

Pct_1msCtrlPedlTrvlRate1msInfo = Simulink.Bus;
DeList={
    'PedlTrvlRate1msMmPerSec'
    };
Pct_1msCtrlPedlTrvlRate1msInfo = CreateBus(Pct_1msCtrlPedlTrvlRate1msInfo, DeList);
clear DeList;

Pct_1msCtrlEcuModeSts = Simulink.Bus;
DeList={'Pct_1msCtrlEcuModeSts'};
Pct_1msCtrlEcuModeSts = CreateBus(Pct_1msCtrlEcuModeSts, DeList);
clear DeList;

Pct_1msCtrlPCtrlSt = Simulink.Bus;
DeList={'Pct_1msCtrlPCtrlSt'};
Pct_1msCtrlPCtrlSt = CreateBus(Pct_1msCtrlPCtrlSt, DeList);
clear DeList;

Pct_1msCtrlPreBoostMod = Simulink.Bus;
DeList={'Pct_1msCtrlPreBoostMod'};
Pct_1msCtrlPreBoostMod = CreateBus(Pct_1msCtrlPreBoostMod, DeList);
clear DeList;

