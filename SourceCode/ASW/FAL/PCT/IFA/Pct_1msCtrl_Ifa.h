/**
 * @defgroup Pct_1msCtrl_Ifa Pct_1msCtrl_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Pct_1msCtrl_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PCT_1MSCTRL_IFA_H_
#define PCT_1MSCTRL_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Pct_1msCtrl_Read_Pct_1msCtrlPedlTrvlFild1msInfo(data) do \
{ \
    *data = Pct_1msCtrlPedlTrvlFild1msInfo; \
}while(0);

#define Pct_1msCtrl_Read_Pct_1msCtrlPedlTrvlRate1msInfo(data) do \
{ \
    *data = Pct_1msCtrlPedlTrvlRate1msInfo; \
}while(0);

#define Pct_1msCtrl_Read_Pct_1msCtrlPedlTrvlFild1msInfo_PdtFild1ms(data) do \
{ \
    *data = Pct_1msCtrlPedlTrvlFild1msInfo.PdtFild1ms; \
}while(0);

#define Pct_1msCtrl_Read_Pct_1msCtrlPedlTrvlRate1msInfo_PedlTrvlRate1msMmPerSec(data) do \
{ \
    *data = Pct_1msCtrlPedlTrvlRate1msInfo.PedlTrvlRate1msMmPerSec; \
}while(0);

#define Pct_1msCtrl_Read_Pct_1msCtrlEcuModeSts(data) do \
{ \
    *data = Pct_1msCtrlEcuModeSts; \
}while(0);

#define Pct_1msCtrl_Read_Pct_1msCtrlPCtrlSt(data) do \
{ \
    *data = Pct_1msCtrlPCtrlSt; \
}while(0);


/* Set Output DE MAcro Function */
#define Pct_1msCtrl_Write_Pct_1msCtrlPreBoostMod(data) do \
{ \
    Pct_1msCtrlPreBoostMod = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PCT_1MSCTRL_IFA_H_ */
/** @} */
