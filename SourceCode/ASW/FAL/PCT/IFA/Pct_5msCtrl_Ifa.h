/**
 * @defgroup Pct_5msCtrl_Ifa Pct_5msCtrl_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Pct_5msCtrl_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PCT_5MSCTRL_IFA_H_
#define PCT_5MSCTRL_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Pct_5msCtrl_Read_Pct_5msCtrlWhlVlvReqAbcInfo(data) do \
{ \
    *data = Pct_5msCtrlWhlVlvReqAbcInfo; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlEemFailData(data) do \
{ \
    *data = Pct_5msCtrlEemFailData; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlPedlTrvlFild1msInfo(data) do \
{ \
    *data = Pct_5msCtrlPedlTrvlFild1msInfo; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlCanRxAccelPedlInfo(data) do \
{ \
    *data = Pct_5msCtrlCanRxAccelPedlInfo; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlMotRotgAgSigInfo(data) do \
{ \
    *data = Pct_5msCtrlMotRotgAgSigInfo; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlAbsCtrlInfo(data) do \
{ \
    *data = Pct_5msCtrlAbsCtrlInfo; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlAvhCtrlInfo(data) do \
{ \
    *data = Pct_5msCtrlAvhCtrlInfo; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlEscCtrlInfo(data) do \
{ \
    *data = Pct_5msCtrlEscCtrlInfo; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlHsaCtrlInfo(data) do \
{ \
    *data = Pct_5msCtrlHsaCtrlInfo; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlStkRecvryCtrlIfInfo(data) do \
{ \
    *data = Pct_5msCtrlStkRecvryCtrlIfInfo; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlTcsCtrlInfo(data) do \
{ \
    *data = Pct_5msCtrlTcsCtrlInfo; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlBaseBrkCtrlModInfo(data) do \
{ \
    *data = Pct_5msCtrlBaseBrkCtrlModInfo; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlBrkPedlStatusInfo(data) do \
{ \
    *data = Pct_5msCtrlBrkPedlStatusInfo; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlBrkPedlStatus1msInfo(data) do \
{ \
    *data = Pct_5msCtrlBrkPedlStatus1msInfo; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlCircPFildInfo(data) do \
{ \
    *data = Pct_5msCtrlCircPFildInfo; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlCircPRateInfo(data) do \
{ \
    *data = Pct_5msCtrlCircPRateInfo; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlPedlTrvlRate1msInfo(data) do \
{ \
    *data = Pct_5msCtrlPedlTrvlRate1msInfo; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlEstimdWhlPInfo(data) do \
{ \
    *data = Pct_5msCtrlEstimdWhlPInfo; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo(data) do \
{ \
    *data = Pct_5msCtrlFinalTarPInfo; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlIdbMotPosnInfo(data) do \
{ \
    *data = Pct_5msCtrlIdbMotPosnInfo; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlWhlOutVlvCtrlTarInfo(data) do \
{ \
    *data = Pct_5msCtrlWhlOutVlvCtrlTarInfo; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlWhlPreFillInfo(data) do \
{ \
    *data = Pct_5msCtrlWhlPreFillInfo; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlIdbVlvActInfo(data) do \
{ \
    *data = Pct_5msCtrlIdbVlvActInfo; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlPedlSimrPFildInfo(data) do \
{ \
    *data = Pct_5msCtrlPedlSimrPFildInfo; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlPedlTrvlRateInfo(data) do \
{ \
    *data = Pct_5msCtrlPedlTrvlRateInfo; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlPedlTrvlTagInfo(data) do \
{ \
    *data = Pct_5msCtrlPedlTrvlTagInfo; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlPistPFildInfo(data) do \
{ \
    *data = Pct_5msCtrlPistPFildInfo; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlPistPRateInfo(data) do \
{ \
    *data = Pct_5msCtrlPistPRateInfo; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlVlvActtnReqByBaseBrkCtrlrInfo(data) do \
{ \
    *data = Pct_5msCtrlVlvActtnReqByBaseBrkCtrlrInfo; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlWhlSpdFildInfo(data) do \
{ \
    *data = Pct_5msCtrlWhlSpdFildInfo; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlEemSuspectData(data) do \
{ \
    *data = Pct_5msCtrlEemSuspectData; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlWhlVlvReqAbcInfo_FlIvReqData(data) do \
{ \
    for(i=0;i<10;i++) *data[i] = Pct_5msCtrlWhlVlvReqAbcInfo.FlIvReqData[i]; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlWhlVlvReqAbcInfo_FrIvReqData(data) do \
{ \
    for(i=0;i<10;i++) *data[i] = Pct_5msCtrlWhlVlvReqAbcInfo.FrIvReqData[i]; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlWhlVlvReqAbcInfo_RlIvReqData(data) do \
{ \
    for(i=0;i<10;i++) *data[i] = Pct_5msCtrlWhlVlvReqAbcInfo.RlIvReqData[i]; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlWhlVlvReqAbcInfo_RrIvReqData(data) do \
{ \
    for(i=0;i<10;i++) *data[i] = Pct_5msCtrlWhlVlvReqAbcInfo.RrIvReqData[i]; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlWhlVlvReqAbcInfo_FlIvReq(data) do \
{ \
    *data = Pct_5msCtrlWhlVlvReqAbcInfo.FlIvReq; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlWhlVlvReqAbcInfo_FrIvReq(data) do \
{ \
    *data = Pct_5msCtrlWhlVlvReqAbcInfo.FrIvReq; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlWhlVlvReqAbcInfo_RlIvReq(data) do \
{ \
    *data = Pct_5msCtrlWhlVlvReqAbcInfo.RlIvReq; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlWhlVlvReqAbcInfo_RrIvReq(data) do \
{ \
    *data = Pct_5msCtrlWhlVlvReqAbcInfo.RrIvReq; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlEemFailData_Eem_Fail_SimP(data) do \
{ \
    *data = Pct_5msCtrlEemFailData.Eem_Fail_SimP; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlEemFailData_Eem_Fail_BLS(data) do \
{ \
    *data = Pct_5msCtrlEemFailData.Eem_Fail_BLS; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlPedlTrvlFild1msInfo_PdtFild1ms(data) do \
{ \
    *data = Pct_5msCtrlPedlTrvlFild1msInfo.PdtFild1ms; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlCanRxAccelPedlInfo_AccelPedlVal(data) do \
{ \
    *data = Pct_5msCtrlCanRxAccelPedlInfo.AccelPedlVal; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlCanRxAccelPedlInfo_AccelPedlValErr(data) do \
{ \
    *data = Pct_5msCtrlCanRxAccelPedlInfo.AccelPedlValErr; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlMotRotgAgSigInfo_StkPosnMeasd(data) do \
{ \
    *data = Pct_5msCtrlMotRotgAgSigInfo.StkPosnMeasd; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlAbsCtrlInfo_AbsActFlg(data) do \
{ \
    *data = Pct_5msCtrlAbsCtrlInfo.AbsActFlg; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlAbsCtrlInfo_AbsDefectFlg(data) do \
{ \
    *data = Pct_5msCtrlAbsCtrlInfo.AbsDefectFlg; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlAbsCtrlInfo_AbsTarPFrntLe(data) do \
{ \
    *data = Pct_5msCtrlAbsCtrlInfo.AbsTarPFrntLe; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlAbsCtrlInfo_AbsTarPFrntRi(data) do \
{ \
    *data = Pct_5msCtrlAbsCtrlInfo.AbsTarPFrntRi; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlAbsCtrlInfo_AbsTarPReLe(data) do \
{ \
    *data = Pct_5msCtrlAbsCtrlInfo.AbsTarPReLe; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlAbsCtrlInfo_AbsTarPReRi(data) do \
{ \
    *data = Pct_5msCtrlAbsCtrlInfo.AbsTarPReRi; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlAbsCtrlInfo_FrntWhlSlip(data) do \
{ \
    *data = Pct_5msCtrlAbsCtrlInfo.FrntWhlSlip; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlAbsCtrlInfo_AbsDesTarP(data) do \
{ \
    *data = Pct_5msCtrlAbsCtrlInfo.AbsDesTarP; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlAbsCtrlInfo_AbsDesTarPReqFlg(data) do \
{ \
    *data = Pct_5msCtrlAbsCtrlInfo.AbsDesTarPReqFlg; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlAbsCtrlInfo_AbsCtrlFadeOutFlg(data) do \
{ \
    *data = Pct_5msCtrlAbsCtrlInfo.AbsCtrlFadeOutFlg; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlAvhCtrlInfo_AvhActFlg(data) do \
{ \
    *data = Pct_5msCtrlAvhCtrlInfo.AvhActFlg; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlAvhCtrlInfo_AvhDefectFlg(data) do \
{ \
    *data = Pct_5msCtrlAvhCtrlInfo.AvhDefectFlg; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlAvhCtrlInfo_AvhTarP(data) do \
{ \
    *data = Pct_5msCtrlAvhCtrlInfo.AvhTarP; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlEscCtrlInfo_EscActFlg(data) do \
{ \
    *data = Pct_5msCtrlEscCtrlInfo.EscActFlg; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlEscCtrlInfo_EscDefectFlg(data) do \
{ \
    *data = Pct_5msCtrlEscCtrlInfo.EscDefectFlg; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlEscCtrlInfo_EscTarPFrntLe(data) do \
{ \
    *data = Pct_5msCtrlEscCtrlInfo.EscTarPFrntLe; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlEscCtrlInfo_EscTarPFrntRi(data) do \
{ \
    *data = Pct_5msCtrlEscCtrlInfo.EscTarPFrntRi; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlEscCtrlInfo_EscTarPReLe(data) do \
{ \
    *data = Pct_5msCtrlEscCtrlInfo.EscTarPReLe; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlEscCtrlInfo_EscTarPReRi(data) do \
{ \
    *data = Pct_5msCtrlEscCtrlInfo.EscTarPReRi; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlHsaCtrlInfo_HsaActFlg(data) do \
{ \
    *data = Pct_5msCtrlHsaCtrlInfo.HsaActFlg; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlHsaCtrlInfo_HsaDefectFlg(data) do \
{ \
    *data = Pct_5msCtrlHsaCtrlInfo.HsaDefectFlg; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlHsaCtrlInfo_HsaTarP(data) do \
{ \
    *data = Pct_5msCtrlHsaCtrlInfo.HsaTarP; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlStkRecvryCtrlIfInfo_StkRcvrEscAllowedTime(data) do \
{ \
    *data = Pct_5msCtrlStkRecvryCtrlIfInfo.StkRcvrEscAllowedTime; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlStkRecvryCtrlIfInfo_StkRcvrAbsAllowedTime(data) do \
{ \
    *data = Pct_5msCtrlStkRecvryCtrlIfInfo.StkRcvrAbsAllowedTime; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlStkRecvryCtrlIfInfo_StkRcvrTcsAllowedTime(data) do \
{ \
    *data = Pct_5msCtrlStkRecvryCtrlIfInfo.StkRcvrTcsAllowedTime; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlTcsCtrlInfo_TcsActFlg(data) do \
{ \
    *data = Pct_5msCtrlTcsCtrlInfo.TcsActFlg; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlTcsCtrlInfo_TcsDefectFlg(data) do \
{ \
    *data = Pct_5msCtrlTcsCtrlInfo.TcsDefectFlg; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlTcsCtrlInfo_TcsTarPFrntLe(data) do \
{ \
    *data = Pct_5msCtrlTcsCtrlInfo.TcsTarPFrntLe; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlTcsCtrlInfo_TcsTarPFrntRi(data) do \
{ \
    *data = Pct_5msCtrlTcsCtrlInfo.TcsTarPFrntRi; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlTcsCtrlInfo_TcsTarPReLe(data) do \
{ \
    *data = Pct_5msCtrlTcsCtrlInfo.TcsTarPReLe; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlTcsCtrlInfo_TcsTarPReRi(data) do \
{ \
    *data = Pct_5msCtrlTcsCtrlInfo.TcsTarPReRi; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlTcsCtrlInfo_BothDrvgWhlBrkCtlReqFlg(data) do \
{ \
    *data = Pct_5msCtrlTcsCtrlInfo.BothDrvgWhlBrkCtlReqFlg; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlBaseBrkCtrlModInfo_VehStandStillStFlg(data) do \
{ \
    *data = Pct_5msCtrlBaseBrkCtrlModInfo.VehStandStillStFlg; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlBrkPedlStatusInfo_DrvrIntendToRelsBrk(data) do \
{ \
    *data = Pct_5msCtrlBrkPedlStatusInfo.DrvrIntendToRelsBrk; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlBrkPedlStatusInfo_PanicBrkStFlg(data) do \
{ \
    *data = Pct_5msCtrlBrkPedlStatusInfo.PanicBrkStFlg; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlBrkPedlStatusInfo_PedlReldStFlg2(data) do \
{ \
    *data = Pct_5msCtrlBrkPedlStatusInfo.PedlReldStFlg2; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlBrkPedlStatus1msInfo_PanicBrkSt1msFlg(data) do \
{ \
    *data = Pct_5msCtrlBrkPedlStatus1msInfo.PanicBrkSt1msFlg; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlCircPFildInfo_PrimCircPFild_1_100Bar(data) do \
{ \
    *data = Pct_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlCircPFildInfo_SecdCircPFild_1_100Bar(data) do \
{ \
    *data = Pct_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlCircPRateInfo_PrimCircPChgDurg10ms(data) do \
{ \
    *data = Pct_5msCtrlCircPRateInfo.PrimCircPChgDurg10ms; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlCircPRateInfo_SecdCircPChgDurg10ms(data) do \
{ \
    *data = Pct_5msCtrlCircPRateInfo.SecdCircPChgDurg10ms; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlPedlTrvlRate1msInfo_PedlTrvlRate1msMmPerSec(data) do \
{ \
    *data = Pct_5msCtrlPedlTrvlRate1msInfo.PedlTrvlRate1msMmPerSec; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlEstimdWhlPInfo_FrntLeEstimdWhlP(data) do \
{ \
    *data = Pct_5msCtrlEstimdWhlPInfo.FrntLeEstimdWhlP; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlEstimdWhlPInfo_FrntRiEstimdWhlP(data) do \
{ \
    *data = Pct_5msCtrlEstimdWhlPInfo.FrntRiEstimdWhlP; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlEstimdWhlPInfo_ReLeEstimdWhlP(data) do \
{ \
    *data = Pct_5msCtrlEstimdWhlPInfo.ReLeEstimdWhlP; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlEstimdWhlPInfo_ReRiEstimdWhlP(data) do \
{ \
    *data = Pct_5msCtrlEstimdWhlPInfo.ReRiEstimdWhlP; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_FinalTarPOfWhlFrntLe(data) do \
{ \
    *data = Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlFrntLe; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_FinalTarPOfWhlFrntRi(data) do \
{ \
    *data = Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlFrntRi; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_FinalTarPOfWhlReLe(data) do \
{ \
    *data = Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlReLe; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_FinalTarPOfWhlReRi(data) do \
{ \
    *data = Pct_5msCtrlFinalTarPInfo.FinalTarPOfWhlReRi; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_FinalTarPRateOfWhlFrntLe(data) do \
{ \
    *data = Pct_5msCtrlFinalTarPInfo.FinalTarPRateOfWhlFrntLe; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_FinalTarPRateOfWhlFrntRi(data) do \
{ \
    *data = Pct_5msCtrlFinalTarPInfo.FinalTarPRateOfWhlFrntRi; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_FinalTarPRateOfWhlReLe(data) do \
{ \
    *data = Pct_5msCtrlFinalTarPInfo.FinalTarPRateOfWhlReLe; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_FinalTarPRateOfWhlReRi(data) do \
{ \
    *data = Pct_5msCtrlFinalTarPInfo.FinalTarPRateOfWhlReRi; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_PCtrlPrioOfWhlFrntLe(data) do \
{ \
    *data = Pct_5msCtrlFinalTarPInfo.PCtrlPrioOfWhlFrntLe; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_PCtrlPrioOfWhlFrntRi(data) do \
{ \
    *data = Pct_5msCtrlFinalTarPInfo.PCtrlPrioOfWhlFrntRi; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_PCtrlPrioOfWhlReLe(data) do \
{ \
    *data = Pct_5msCtrlFinalTarPInfo.PCtrlPrioOfWhlReLe; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_PCtrlPrioOfWhlReRi(data) do \
{ \
    *data = Pct_5msCtrlFinalTarPInfo.PCtrlPrioOfWhlReRi; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_CtrlModOfWhlFrntLe(data) do \
{ \
    *data = Pct_5msCtrlFinalTarPInfo.CtrlModOfWhlFrntLe; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_CtrlModOfOfWhlFrntRi(data) do \
{ \
    *data = Pct_5msCtrlFinalTarPInfo.CtrlModOfOfWhlFrntRi; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_CtrlModOfWhlReLe(data) do \
{ \
    *data = Pct_5msCtrlFinalTarPInfo.CtrlModOfWhlReLe; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_CtrlModOfWhlReRi(data) do \
{ \
    *data = Pct_5msCtrlFinalTarPInfo.CtrlModOfWhlReRi; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_ReqdPCtrlBoostMod(data) do \
{ \
    *data = Pct_5msCtrlFinalTarPInfo.ReqdPCtrlBoostMod; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_FinalDelTarPOfWhlFrntLe(data) do \
{ \
    *data = Pct_5msCtrlFinalTarPInfo.FinalDelTarPOfWhlFrntLe; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_FinalDelTarPOfWhlFrntRi(data) do \
{ \
    *data = Pct_5msCtrlFinalTarPInfo.FinalDelTarPOfWhlFrntRi; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_FinalDelTarPOfWhlReLe(data) do \
{ \
    *data = Pct_5msCtrlFinalTarPInfo.FinalDelTarPOfWhlReLe; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlFinalTarPInfo_FinalDelTarPOfWhlReRi(data) do \
{ \
    *data = Pct_5msCtrlFinalTarPInfo.FinalDelTarPOfWhlReRi; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlIdbMotPosnInfo_MotTarPosn(data) do \
{ \
    *data = Pct_5msCtrlIdbMotPosnInfo.MotTarPosn; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlWhlOutVlvCtrlTarInfo_FlOutVlvCtrlTar(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Pct_5msCtrlWhlOutVlvCtrlTarInfo.FlOutVlvCtrlTar[i]; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlWhlOutVlvCtrlTarInfo_FrOutVlvCtrlTar(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Pct_5msCtrlWhlOutVlvCtrlTarInfo.FrOutVlvCtrlTar[i]; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlWhlOutVlvCtrlTarInfo_RlOutVlvCtrlTar(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Pct_5msCtrlWhlOutVlvCtrlTarInfo.RlOutVlvCtrlTar[i]; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlWhlOutVlvCtrlTarInfo_RrOutVlvCtrlTar(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Pct_5msCtrlWhlOutVlvCtrlTarInfo.RrOutVlvCtrlTar[i]; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlWhlPreFillInfo_FlPreFillActFlg(data) do \
{ \
    *data = Pct_5msCtrlWhlPreFillInfo.FlPreFillActFlg; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlWhlPreFillInfo_FrPreFillActFlg(data) do \
{ \
    *data = Pct_5msCtrlWhlPreFillInfo.FrPreFillActFlg; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlWhlPreFillInfo_RlPreFillActFlg(data) do \
{ \
    *data = Pct_5msCtrlWhlPreFillInfo.RlPreFillActFlg; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlWhlPreFillInfo_RrPreFillActFlg(data) do \
{ \
    *data = Pct_5msCtrlWhlPreFillInfo.RrPreFillActFlg; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlIdbVlvActInfo_VlvFadeoutEndOK(data) do \
{ \
    *data = Pct_5msCtrlIdbVlvActInfo.VlvFadeoutEndOK; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlIdbVlvActInfo_CutVlvOpenFlg(data) do \
{ \
    *data = Pct_5msCtrlIdbVlvActInfo.CutVlvOpenFlg; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlPedlSimrPFildInfo_PedlSimrPFild_1_100Bar(data) do \
{ \
    *data = Pct_5msCtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlPedlTrvlRateInfo_PedlTrvlRateMilliMtrPerSec(data) do \
{ \
    *data = Pct_5msCtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlPedlTrvlTagInfo_PedlSigTag(data) do \
{ \
    *data = Pct_5msCtrlPedlTrvlTagInfo.PedlSigTag; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlPistPFildInfo_PistPFild_1_100Bar(data) do \
{ \
    *data = Pct_5msCtrlPistPFildInfo.PistPFild_1_100Bar; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlPistPRateInfo_PistPChgDurg10ms(data) do \
{ \
    *data = Pct_5msCtrlPistPRateInfo.PistPChgDurg10ms; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlVlvActtnReqByBaseBrkCtrlrInfo_PedlSimrPGendOnFlg(data) do \
{ \
    *data = Pct_5msCtrlVlvActtnReqByBaseBrkCtrlrInfo.PedlSimrPGendOnFlg; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlWhlSpdFildInfo_WhlSpdFildReLe(data) do \
{ \
    *data = Pct_5msCtrlWhlSpdFildInfo.WhlSpdFildReLe; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlWhlSpdFildInfo_WhlSpdFildReRi(data) do \
{ \
    *data = Pct_5msCtrlWhlSpdFildInfo.WhlSpdFildReRi; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlEemSuspectData_Eem_Suspect_SimP(data) do \
{ \
    *data = Pct_5msCtrlEemSuspectData.Eem_Suspect_SimP; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlEcuModeSts(data) do \
{ \
    *data = Pct_5msCtrlEcuModeSts; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlMotCtrlMode(data) do \
{ \
    *data = Pct_5msCtrlMotCtrlMode; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlFuncInhibitPctrlSts(data) do \
{ \
    *data = Pct_5msCtrlFuncInhibitPctrlSts; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlBaseBrkCtrlrActFlg(data) do \
{ \
    *data = Pct_5msCtrlBaseBrkCtrlrActFlg; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlBlsFild(data) do \
{ \
    *data = Pct_5msCtrlBlsFild; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlBrkPRednForBaseBrkCtrlr(data) do \
{ \
    *data = Pct_5msCtrlBrkPRednForBaseBrkCtrlr; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlMotICtrlFadeOutState(data) do \
{ \
    *data = Pct_5msCtrlMotICtrlFadeOutState; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlPreBoostMod(data) do \
{ \
    *data = Pct_5msCtrlPreBoostMod; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlRgnBrkCtlrBlendgFlg(data) do \
{ \
    *data = Pct_5msCtrlRgnBrkCtlrBlendgFlg; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlVehSpdFild(data) do \
{ \
    *data = Pct_5msCtrlVehSpdFild; \
}while(0);

#define Pct_5msCtrl_Read_Pct_5msCtrlStkRecvryStabnEndOK(data) do \
{ \
    *data = Pct_5msCtrlStkRecvryStabnEndOK; \
}while(0);


/* Set Output DE MAcro Function */
#define Pct_5msCtrl_Write_Pct_5msCtrlIdbPCtrllVlvCtrlStInfo(data) do \
{ \
    Pct_5msCtrlIdbPCtrllVlvCtrlStInfo = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo(data) do \
{ \
    Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlWhlnVlvCtrlTarInfo(data) do \
{ \
    Pct_5msCtrlWhlnVlvCtrlTarInfo = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlStkRecvryActnIfInfo(data) do \
{ \
    Pct_5msCtrlStkRecvryActnIfInfo = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlMuxCmdExecStInfo(data) do \
{ \
    Pct_5msCtrlMuxCmdExecStInfo = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlIdbPCtrllVlvCtrlStInfo_CvVlvSt(data) do \
{ \
    Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.CvVlvSt = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlIdbPCtrllVlvCtrlStInfo_RlVlvSt(data) do \
{ \
    Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.RlVlvSt = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlIdbPCtrllVlvCtrlStInfo_PdVlvSt(data) do \
{ \
    Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.PdVlvSt = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlIdbPCtrllVlvCtrlStInfo_BalVlvSt(data) do \
{ \
    Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.BalVlvSt = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlIdbPCtrllVlvCtrlStInfo_CvVlvNoiseCtrlSt(data) do \
{ \
    Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.CvVlvNoiseCtrlSt = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlIdbPCtrllVlvCtrlStInfo_RlVlvNoiseCtrlSt(data) do \
{ \
    Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.RlVlvNoiseCtrlSt = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlIdbPCtrllVlvCtrlStInfo_PdVlvNoiseCtrlSt(data) do \
{ \
    Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.PdVlvNoiseCtrlSt = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlIdbPCtrllVlvCtrlStInfo_BalVlvNoiseCtrlSt(data) do \
{ \
    Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.BalVlvNoiseCtrlSt = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlIdbPCtrllVlvCtrlStInfo_CvVlvHoldTime(data) do \
{ \
    Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.CvVlvHoldTime = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlIdbPCtrllVlvCtrlStInfo_RlVlvHoldTime(data) do \
{ \
    Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.RlVlvHoldTime = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlIdbPCtrllVlvCtrlStInfo_PdVlvHoldTime(data) do \
{ \
    Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.PdVlvHoldTime = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlIdbPCtrllVlvCtrlStInfo_BalVlvHoldTime(data) do \
{ \
    Pct_5msCtrlIdbPCtrllVlvCtrlStInfo.BalVlvHoldTime = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo_PrimCutVlvSt(data) do \
{ \
    Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvSt = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo_SecdCutVlvSt(data) do \
{ \
    Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvSt = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo_SimVlvSt(data) do \
{ \
    Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SimVlvSt = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo_PrimCutVlvNoiseCtrlSt(data) do \
{ \
    Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvNoiseCtrlSt = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo_SecdCutVlvNoiseCtrlSt(data) do \
{ \
    Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvNoiseCtrlSt = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo_SimVlvNoiseCtrlSt(data) do \
{ \
    Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SimVlvNoiseCtrlSt = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo_PrimCutVlvHoldTime(data) do \
{ \
    Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvHoldTime = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo_SecdCutVlvHoldTime(data) do \
{ \
    Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SecdCutVlvHoldTime = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo_SimVlvHoldTime(data) do \
{ \
    Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.SimVlvHoldTime = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo_CutVlvLongOpen(data) do \
{ \
    Pct_5msCtrlIdbPedlFeelVlvCtrlStInfo.CutVlvLongOpen = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlWhlnVlvCtrlTarInfo_FlInVlvCtrlTar(data) \
{ \
    for(i=0;i<5;i++) Pct_5msCtrlWhlnVlvCtrlTarInfo.FlInVlvCtrlTar[i] = *data[i]; \
}
#define Pct_5msCtrl_Write_Pct_5msCtrlWhlnVlvCtrlTarInfo_FrInVlvCtrlTar(data) \
{ \
    for(i=0;i<5;i++) Pct_5msCtrlWhlnVlvCtrlTarInfo.FrInVlvCtrlTar[i] = *data[i]; \
}
#define Pct_5msCtrl_Write_Pct_5msCtrlWhlnVlvCtrlTarInfo_RlInVlvCtrlTar(data) \
{ \
    for(i=0;i<5;i++) Pct_5msCtrlWhlnVlvCtrlTarInfo.RlInVlvCtrlTar[i] = *data[i]; \
}
#define Pct_5msCtrl_Write_Pct_5msCtrlWhlnVlvCtrlTarInfo_RrInVlvCtrlTar(data) \
{ \
    for(i=0;i<5;i++) Pct_5msCtrlWhlnVlvCtrlTarInfo.RrInVlvCtrlTar[i] = *data[i]; \
}
#define Pct_5msCtrl_Write_Pct_5msCtrlStkRecvryActnIfInfo_RecommendStkRcvrLvl(data) do \
{ \
    Pct_5msCtrlStkRecvryActnIfInfo.RecommendStkRcvrLvl = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlStkRecvryActnIfInfo_StkRecvryCtrlState(data) do \
{ \
    Pct_5msCtrlStkRecvryActnIfInfo.StkRecvryCtrlState = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlStkRecvryActnIfInfo_StkRecvryRequestedTime(data) do \
{ \
    Pct_5msCtrlStkRecvryActnIfInfo.StkRecvryRequestedTime = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlMuxCmdExecStInfo_MuxCmdExecStOfWhlFrntLe(data) do \
{ \
    Pct_5msCtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlFrntLe = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlMuxCmdExecStInfo_MuxCmdExecStOfWhlFrntRi(data) do \
{ \
    Pct_5msCtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlFrntRi = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlMuxCmdExecStInfo_MuxCmdExecStOfWhlReLe(data) do \
{ \
    Pct_5msCtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlReLe = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlMuxCmdExecStInfo_MuxCmdExecStOfWhlReRi(data) do \
{ \
    Pct_5msCtrlMuxCmdExecStInfo.MuxCmdExecStOfWhlReRi = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlPCtrlAct(data) do \
{ \
    Pct_5msCtrlPCtrlAct = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlInitQuickBrkDctFlg(data) do \
{ \
    Pct_5msCtrlInitQuickBrkDctFlg = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlTgtDeltaStkType(data) do \
{ \
    Pct_5msCtrlTgtDeltaStkType = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlPCtrlBoostMod(data) do \
{ \
    Pct_5msCtrlPCtrlBoostMod = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlPCtrlFadeoutSt(data) do \
{ \
    Pct_5msCtrlPCtrlFadeoutSt = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlPCtrlSt(data) do \
{ \
    Pct_5msCtrlPCtrlSt = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlInVlvAllCloseReq(data) do \
{ \
    Pct_5msCtrlInVlvAllCloseReq = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlPChamberVolume(data) do \
{ \
    Pct_5msCtrlPChamberVolume = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlTarDeltaStk(data) do \
{ \
    Pct_5msCtrlTarDeltaStk = *data; \
}while(0);

#define Pct_5msCtrl_Write_Pct_5msCtrlFinalTarPFromPCtrl(data) do \
{ \
    Pct_5msCtrlFinalTarPFromPCtrl = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PCT_5MSCTRL_IFA_H_ */
/** @} */
