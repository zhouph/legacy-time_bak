/* Virtual main for linking */
/* purpose : Generation of map file */
#include "Pct_1msCtrl.h"
#include "Pct_5msCtrl.h"

int main(void)
{
    Pct_1msCtrl_Init();
    Pct_5msCtrl_Init();

    while(1)
    {
        Pct_1msCtrl();
        Pct_5msCtrl();
    }
}