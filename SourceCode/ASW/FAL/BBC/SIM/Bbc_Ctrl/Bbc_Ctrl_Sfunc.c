#define S_FUNCTION_NAME      Bbc_Ctrl_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          28
#define WidthOutputPort         9

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Bbc_Ctrl.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Bbc_CtrlEemFailData.Eem_Fail_BLS = input[0];
    Bbc_CtrlEemFailData.Eem_Fail_WssFL = input[1];
    Bbc_CtrlEemFailData.Eem_Fail_WssFR = input[2];
    Bbc_CtrlEemFailData.Eem_Fail_WssRL = input[3];
    Bbc_CtrlEemFailData.Eem_Fail_WssRR = input[4];
    Bbc_CtrlCanRxIdbInfo.GearSelDisp = input[5];
    Bbc_CtrlCanRxIdbInfo.TcuSwiGs = input[6];
    Bbc_CtrlBrkPedlStatus1msInfo.PanicBrkSt1msFlg = input[7];
    Bbc_CtrlBrkPedlStatusInfo.PanicBrkStFlg = input[8];
    Bbc_CtrlBrkPedlStatusInfo.PedlReldStFlg1 = input[9];
    Bbc_CtrlBrkPedlStatusInfo.PedlReldStFlg2 = input[10];
    Bbc_CtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar = input[11];
    Bbc_CtrlPedlTrvlRateInfo.PedlTrvlRate = input[12];
    Bbc_CtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec = input[13];
    Bbc_CtrlVehStopStInfo.VehStopStFlg = input[14];
    Bbc_CtrlEemSuspectData.Eem_Suspect_WssFL = input[15];
    Bbc_CtrlEemSuspectData.Eem_Suspect_WssFR = input[16];
    Bbc_CtrlEemSuspectData.Eem_Suspect_WssRL = input[17];
    Bbc_CtrlEemSuspectData.Eem_Suspect_WssRR = input[18];
    Bbc_CtrlEcuModeSts = input[19];
    Bbc_CtrlIgnOnOffSts = input[20];
    Bbc_CtrlFuncInhibitBbcSts = input[21];
    Bbc_CtrlBlsFild = input[22];
    Bbc_CtrlBrkPRednForBaseBrkCtrlr = input[23];
    Bbc_CtrlFlexBrkSwtFild = input[24];
    Bbc_CtrlPedlTrvlFinal = input[25];
    Bbc_CtrlRgnBrkCtrlrActStFlg = input[26];
    Bbc_CtrlVehSpdFild = input[27];

    Bbc_Ctrl();


    output[0] = Bbc_CtrlBaseBrkCtrlModInfo.LowSpdCtrlInhibitFlg;
    output[1] = Bbc_CtrlBaseBrkCtrlModInfo.StopdVehCtrlModFlg;
    output[2] = Bbc_CtrlBaseBrkCtrlModInfo.VehStandStillStFlg;
    output[3] = Bbc_CtrlBBCCtrlInfo_t.BBCCtrlSt;
    output[4] = Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo.PedlSimrPGendOnFlg;
    output[5] = Bbc_CtrlBaseBrkCtrlrActFlg;
    output[6] = Bbc_CtrlTarPFromBaseBrkCtrlr;
    output[7] = Bbc_CtrlTarPRateFromBaseBrkCtrlr;
    output[8] = Bbc_CtrlTarPFromBrkPedl;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Bbc_Ctrl_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
