#include "unity.h"
#include "unity_fixture.h"
#include "Bbc_Ctrl.h"
#include "Bbc_Ctrl_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Saluint8 UtInput_Bbc_CtrlEemFailData_Eem_Fail_BLS[MAX_STEP] = BBC_CTRLEEMFAILDATA_EEM_FAIL_BLS;
const Saluint8 UtInput_Bbc_CtrlEemFailData_Eem_Fail_WssFL[MAX_STEP] = BBC_CTRLEEMFAILDATA_EEM_FAIL_WSSFL;
const Saluint8 UtInput_Bbc_CtrlEemFailData_Eem_Fail_WssFR[MAX_STEP] = BBC_CTRLEEMFAILDATA_EEM_FAIL_WSSFR;
const Saluint8 UtInput_Bbc_CtrlEemFailData_Eem_Fail_WssRL[MAX_STEP] = BBC_CTRLEEMFAILDATA_EEM_FAIL_WSSRL;
const Saluint8 UtInput_Bbc_CtrlEemFailData_Eem_Fail_WssRR[MAX_STEP] = BBC_CTRLEEMFAILDATA_EEM_FAIL_WSSRR;
const Saluint8 UtInput_Bbc_CtrlCanRxIdbInfo_GearSelDisp[MAX_STEP] = BBC_CTRLCANRXIDBINFO_GEARSELDISP;
const Saluint8 UtInput_Bbc_CtrlCanRxIdbInfo_TcuSwiGs[MAX_STEP] = BBC_CTRLCANRXIDBINFO_TCUSWIGS;
const Rtesint32 UtInput_Bbc_CtrlBrkPedlStatus1msInfo_PanicBrkSt1msFlg[MAX_STEP] = BBC_CTRLBRKPEDLSTATUS1MSINFO_PANICBRKST1MSFLG;
const Rtesint32 UtInput_Bbc_CtrlBrkPedlStatusInfo_PanicBrkStFlg[MAX_STEP] = BBC_CTRLBRKPEDLSTATUSINFO_PANICBRKSTFLG;
const Rtesint32 UtInput_Bbc_CtrlBrkPedlStatusInfo_PedlReldStFlg1[MAX_STEP] = BBC_CTRLBRKPEDLSTATUSINFO_PEDLRELDSTFLG1;
const Rtesint32 UtInput_Bbc_CtrlBrkPedlStatusInfo_PedlReldStFlg2[MAX_STEP] = BBC_CTRLBRKPEDLSTATUSINFO_PEDLRELDSTFLG2;
const Rtesint32 UtInput_Bbc_CtrlPedlSimrPFildInfo_PedlSimrPFild_1_100Bar[MAX_STEP] = BBC_CTRLPEDLSIMRPFILDINFO_PEDLSIMRPFILD_1_100BAR;
const Rtesint32 UtInput_Bbc_CtrlPedlTrvlRateInfo_PedlTrvlRate[MAX_STEP] = BBC_CTRLPEDLTRVLRATEINFO_PEDLTRVLRATE;
const Rtesint32 UtInput_Bbc_CtrlPedlTrvlRateInfo_PedlTrvlRateMilliMtrPerSec[MAX_STEP] = BBC_CTRLPEDLTRVLRATEINFO_PEDLTRVLRATEMILLIMTRPERSEC;
const Rtesint32 UtInput_Bbc_CtrlVehStopStInfo_VehStopStFlg[MAX_STEP] = BBC_CTRLVEHSTOPSTINFO_VEHSTOPSTFLG;
const Saluint8 UtInput_Bbc_CtrlEemSuspectData_Eem_Suspect_WssFL[MAX_STEP] = BBC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_WSSFL;
const Saluint8 UtInput_Bbc_CtrlEemSuspectData_Eem_Suspect_WssFR[MAX_STEP] = BBC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_WSSFR;
const Saluint8 UtInput_Bbc_CtrlEemSuspectData_Eem_Suspect_WssRL[MAX_STEP] = BBC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_WSSRL;
const Saluint8 UtInput_Bbc_CtrlEemSuspectData_Eem_Suspect_WssRR[MAX_STEP] = BBC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_WSSRR;
const Mom_HndlrEcuModeSts_t UtInput_Bbc_CtrlEcuModeSts[MAX_STEP] = BBC_CTRLECUMODESTS;
const Prly_HndlrIgnOnOffSts_t UtInput_Bbc_CtrlIgnOnOffSts[MAX_STEP] = BBC_CTRLIGNONOFFSTS;
const Eem_SuspcDetnFuncInhibitBbcSts_t UtInput_Bbc_CtrlFuncInhibitBbcSts[MAX_STEP] = BBC_CTRLFUNCINHIBITBBCSTS;
const Spc_5msCtrlBlsFild_t UtInput_Bbc_CtrlBlsFild[MAX_STEP] = BBC_CTRLBLSFILD;
const Rbc_CtrlBrkPRednForBaseBrkCtrlr_t UtInput_Bbc_CtrlBrkPRednForBaseBrkCtrlr[MAX_STEP] = BBC_CTRLBRKPREDNFORBASEBRKCTRLR;
const Spc_5msCtrlFlexBrkSwtFild_t UtInput_Bbc_CtrlFlexBrkSwtFild[MAX_STEP] = BBC_CTRLFLEXBRKSWTFILD;
const Det_5msCtrlPedlTrvlFinal_t UtInput_Bbc_CtrlPedlTrvlFinal[MAX_STEP] = BBC_CTRLPEDLTRVLFINAL;
const Rbc_CtrlRgnBrkCtrlrActStFlg_t UtInput_Bbc_CtrlRgnBrkCtrlrActStFlg[MAX_STEP] = BBC_CTRLRGNBRKCTRLRACTSTFLG;
const Det_5msCtrlVehSpdFild_t UtInput_Bbc_CtrlVehSpdFild[MAX_STEP] = BBC_CTRLVEHSPDFILD;

const Rtesint32 UtExpected_Bbc_CtrlBaseBrkCtrlModInfo_LowSpdCtrlInhibitFlg[MAX_STEP] = BBC_CTRLBASEBRKCTRLMODINFO_LOWSPDCTRLINHIBITFLG;
const Rtesint32 UtExpected_Bbc_CtrlBaseBrkCtrlModInfo_StopdVehCtrlModFlg[MAX_STEP] = BBC_CTRLBASEBRKCTRLMODINFO_STOPDVEHCTRLMODFLG;
const Rtesint32 UtExpected_Bbc_CtrlBaseBrkCtrlModInfo_VehStandStillStFlg[MAX_STEP] = BBC_CTRLBASEBRKCTRLMODINFO_VEHSTANDSTILLSTFLG;
const Rtesint32 UtExpected_Bbc_CtrlBBCCtrlInfo_BBCCtrlSt[MAX_STEP] = BBC_CTRLBBCCTRLINFO_BBCCTRLST;
const Rtesint32 UtExpected_Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo_PedlSimrPGendOnFlg[MAX_STEP] = BBC_CTRLVLVACTTNREQBYBASEBRKCTRLRINFO_PEDLSIMRPGENDONFLG;
const Bbc_CtrlBaseBrkCtrlrActFlg_t UtExpected_Bbc_CtrlBaseBrkCtrlrActFlg[MAX_STEP] = BBC_CTRLBASEBRKCTRLRACTFLG;
const Bbc_CtrlTarPFromBaseBrkCtrlr_t UtExpected_Bbc_CtrlTarPFromBaseBrkCtrlr[MAX_STEP] = BBC_CTRLTARPFROMBASEBRKCTRLR;
const Bbc_CtrlTarPRateFromBaseBrkCtrlr_t UtExpected_Bbc_CtrlTarPRateFromBaseBrkCtrlr[MAX_STEP] = BBC_CTRLTARPRATEFROMBASEBRKCTRLR;
const Bbc_CtrlTarPFromBrkPedl_t UtExpected_Bbc_CtrlTarPFromBrkPedl[MAX_STEP] = BBC_CTRLTARPFROMBRKPEDL;



TEST_GROUP(Bbc_Ctrl);
TEST_SETUP(Bbc_Ctrl)
{
    Bbc_Ctrl_Init();
}

TEST_TEAR_DOWN(Bbc_Ctrl)
{   /* Postcondition */

}

TEST(Bbc_Ctrl, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Bbc_CtrlEemFailData.Eem_Fail_BLS = UtInput_Bbc_CtrlEemFailData_Eem_Fail_BLS[i];
        Bbc_CtrlEemFailData.Eem_Fail_WssFL = UtInput_Bbc_CtrlEemFailData_Eem_Fail_WssFL[i];
        Bbc_CtrlEemFailData.Eem_Fail_WssFR = UtInput_Bbc_CtrlEemFailData_Eem_Fail_WssFR[i];
        Bbc_CtrlEemFailData.Eem_Fail_WssRL = UtInput_Bbc_CtrlEemFailData_Eem_Fail_WssRL[i];
        Bbc_CtrlEemFailData.Eem_Fail_WssRR = UtInput_Bbc_CtrlEemFailData_Eem_Fail_WssRR[i];
        Bbc_CtrlCanRxIdbInfo.GearSelDisp = UtInput_Bbc_CtrlCanRxIdbInfo_GearSelDisp[i];
        Bbc_CtrlCanRxIdbInfo.TcuSwiGs = UtInput_Bbc_CtrlCanRxIdbInfo_TcuSwiGs[i];
        Bbc_CtrlBrkPedlStatus1msInfo.PanicBrkSt1msFlg = UtInput_Bbc_CtrlBrkPedlStatus1msInfo_PanicBrkSt1msFlg[i];
        Bbc_CtrlBrkPedlStatusInfo.PanicBrkStFlg = UtInput_Bbc_CtrlBrkPedlStatusInfo_PanicBrkStFlg[i];
        Bbc_CtrlBrkPedlStatusInfo.PedlReldStFlg1 = UtInput_Bbc_CtrlBrkPedlStatusInfo_PedlReldStFlg1[i];
        Bbc_CtrlBrkPedlStatusInfo.PedlReldStFlg2 = UtInput_Bbc_CtrlBrkPedlStatusInfo_PedlReldStFlg2[i];
        Bbc_CtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar = UtInput_Bbc_CtrlPedlSimrPFildInfo_PedlSimrPFild_1_100Bar[i];
        Bbc_CtrlPedlTrvlRateInfo.PedlTrvlRate = UtInput_Bbc_CtrlPedlTrvlRateInfo_PedlTrvlRate[i];
        Bbc_CtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec = UtInput_Bbc_CtrlPedlTrvlRateInfo_PedlTrvlRateMilliMtrPerSec[i];
        Bbc_CtrlVehStopStInfo.VehStopStFlg = UtInput_Bbc_CtrlVehStopStInfo_VehStopStFlg[i];
        Bbc_CtrlEemSuspectData.Eem_Suspect_WssFL = UtInput_Bbc_CtrlEemSuspectData_Eem_Suspect_WssFL[i];
        Bbc_CtrlEemSuspectData.Eem_Suspect_WssFR = UtInput_Bbc_CtrlEemSuspectData_Eem_Suspect_WssFR[i];
        Bbc_CtrlEemSuspectData.Eem_Suspect_WssRL = UtInput_Bbc_CtrlEemSuspectData_Eem_Suspect_WssRL[i];
        Bbc_CtrlEemSuspectData.Eem_Suspect_WssRR = UtInput_Bbc_CtrlEemSuspectData_Eem_Suspect_WssRR[i];
        Bbc_CtrlEcuModeSts = UtInput_Bbc_CtrlEcuModeSts[i];
        Bbc_CtrlIgnOnOffSts = UtInput_Bbc_CtrlIgnOnOffSts[i];
        Bbc_CtrlFuncInhibitBbcSts = UtInput_Bbc_CtrlFuncInhibitBbcSts[i];
        Bbc_CtrlBlsFild = UtInput_Bbc_CtrlBlsFild[i];
        Bbc_CtrlBrkPRednForBaseBrkCtrlr = UtInput_Bbc_CtrlBrkPRednForBaseBrkCtrlr[i];
        Bbc_CtrlFlexBrkSwtFild = UtInput_Bbc_CtrlFlexBrkSwtFild[i];
        Bbc_CtrlPedlTrvlFinal = UtInput_Bbc_CtrlPedlTrvlFinal[i];
        Bbc_CtrlRgnBrkCtrlrActStFlg = UtInput_Bbc_CtrlRgnBrkCtrlrActStFlg[i];
        Bbc_CtrlVehSpdFild = UtInput_Bbc_CtrlVehSpdFild[i];

        Bbc_Ctrl();

        TEST_ASSERT_EQUAL(Bbc_CtrlBaseBrkCtrlModInfo.LowSpdCtrlInhibitFlg, UtExpected_Bbc_CtrlBaseBrkCtrlModInfo_LowSpdCtrlInhibitFlg[i]);
        TEST_ASSERT_EQUAL(Bbc_CtrlBaseBrkCtrlModInfo.StopdVehCtrlModFlg, UtExpected_Bbc_CtrlBaseBrkCtrlModInfo_StopdVehCtrlModFlg[i]);
        TEST_ASSERT_EQUAL(Bbc_CtrlBaseBrkCtrlModInfo.VehStandStillStFlg, UtExpected_Bbc_CtrlBaseBrkCtrlModInfo_VehStandStillStFlg[i]);
        TEST_ASSERT_EQUAL(Bbc_CtrlBBCCtrlInfo.BBCCtrlSt, UtExpected_Bbc_CtrlBBCCtrlInfo_BBCCtrlSt[i]);
        TEST_ASSERT_EQUAL(Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo.PedlSimrPGendOnFlg, UtExpected_Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo_PedlSimrPGendOnFlg[i]);
        TEST_ASSERT_EQUAL(Bbc_CtrlBaseBrkCtrlrActFlg, UtExpected_Bbc_CtrlBaseBrkCtrlrActFlg[i]);
        TEST_ASSERT_EQUAL(Bbc_CtrlTarPFromBaseBrkCtrlr, UtExpected_Bbc_CtrlTarPFromBaseBrkCtrlr[i]);
        TEST_ASSERT_EQUAL(Bbc_CtrlTarPRateFromBaseBrkCtrlr, UtExpected_Bbc_CtrlTarPRateFromBaseBrkCtrlr[i]);
        TEST_ASSERT_EQUAL(Bbc_CtrlTarPFromBrkPedl, UtExpected_Bbc_CtrlTarPFromBrkPedl[i]);
    }
}

TEST_GROUP_RUNNER(Bbc_Ctrl)
{
    RUN_TEST_CASE(Bbc_Ctrl, All);
}
