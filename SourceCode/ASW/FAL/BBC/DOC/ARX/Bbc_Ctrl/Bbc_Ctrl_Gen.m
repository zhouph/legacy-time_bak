Bbc_CtrlEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_BLS'
    'Eem_Fail_WssFL'
    'Eem_Fail_WssFR'
    'Eem_Fail_WssRL'
    'Eem_Fail_WssRR'
    };
Bbc_CtrlEemFailData = CreateBus(Bbc_CtrlEemFailData, DeList);
clear DeList;

Bbc_CtrlCanRxIdbInfo = Simulink.Bus;
DeList={
    'GearSelDisp'
    'TcuSwiGs'
    };
Bbc_CtrlCanRxIdbInfo = CreateBus(Bbc_CtrlCanRxIdbInfo, DeList);
clear DeList;

Bbc_CtrlBrkPedlStatus1msInfo = Simulink.Bus;
DeList={
    'PanicBrkSt1msFlg'
    };
Bbc_CtrlBrkPedlStatus1msInfo = CreateBus(Bbc_CtrlBrkPedlStatus1msInfo, DeList);
clear DeList;

Bbc_CtrlBrkPedlStatusInfo = Simulink.Bus;
DeList={
    'PanicBrkStFlg'
    'PedlReldStFlg1'
    'PedlReldStFlg2'
    };
Bbc_CtrlBrkPedlStatusInfo = CreateBus(Bbc_CtrlBrkPedlStatusInfo, DeList);
clear DeList;

Bbc_CtrlPedlSimrPFildInfo = Simulink.Bus;
DeList={
    'PedlSimrPFild_1_100Bar'
    };
Bbc_CtrlPedlSimrPFildInfo = CreateBus(Bbc_CtrlPedlSimrPFildInfo, DeList);
clear DeList;

Bbc_CtrlPedlTrvlRateInfo = Simulink.Bus;
DeList={
    'PedlTrvlRate'
    'PedlTrvlRateMilliMtrPerSec'
    };
Bbc_CtrlPedlTrvlRateInfo = CreateBus(Bbc_CtrlPedlTrvlRateInfo, DeList);
clear DeList;

Bbc_CtrlVehStopStInfo = Simulink.Bus;
DeList={
    'VehStopStFlg'
    };
Bbc_CtrlVehStopStInfo = CreateBus(Bbc_CtrlVehStopStInfo, DeList);
clear DeList;

Bbc_CtrlEemSuspectData = Simulink.Bus;
DeList={
    'Eem_Suspect_WssFL'
    'Eem_Suspect_WssFR'
    'Eem_Suspect_WssRL'
    'Eem_Suspect_WssRR'
    };
Bbc_CtrlEemSuspectData = CreateBus(Bbc_CtrlEemSuspectData, DeList);
clear DeList;

Bbc_CtrlEcuModeSts = Simulink.Bus;
DeList={'Bbc_CtrlEcuModeSts'};
Bbc_CtrlEcuModeSts = CreateBus(Bbc_CtrlEcuModeSts, DeList);
clear DeList;

Bbc_CtrlIgnOnOffSts = Simulink.Bus;
DeList={'Bbc_CtrlIgnOnOffSts'};
Bbc_CtrlIgnOnOffSts = CreateBus(Bbc_CtrlIgnOnOffSts, DeList);
clear DeList;

Bbc_CtrlFuncInhibitBbcSts = Simulink.Bus;
DeList={'Bbc_CtrlFuncInhibitBbcSts'};
Bbc_CtrlFuncInhibitBbcSts = CreateBus(Bbc_CtrlFuncInhibitBbcSts, DeList);
clear DeList;

Bbc_CtrlBlsFild = Simulink.Bus;
DeList={'Bbc_CtrlBlsFild'};
Bbc_CtrlBlsFild = CreateBus(Bbc_CtrlBlsFild, DeList);
clear DeList;

Bbc_CtrlBrkPRednForBaseBrkCtrlr = Simulink.Bus;
DeList={'Bbc_CtrlBrkPRednForBaseBrkCtrlr'};
Bbc_CtrlBrkPRednForBaseBrkCtrlr = CreateBus(Bbc_CtrlBrkPRednForBaseBrkCtrlr, DeList);
clear DeList;

Bbc_CtrlFlexBrkSwtFild = Simulink.Bus;
DeList={'Bbc_CtrlFlexBrkSwtFild'};
Bbc_CtrlFlexBrkSwtFild = CreateBus(Bbc_CtrlFlexBrkSwtFild, DeList);
clear DeList;

Bbc_CtrlPedlTrvlFinal = Simulink.Bus;
DeList={'Bbc_CtrlPedlTrvlFinal'};
Bbc_CtrlPedlTrvlFinal = CreateBus(Bbc_CtrlPedlTrvlFinal, DeList);
clear DeList;

Bbc_CtrlRgnBrkCtrlrActStFlg = Simulink.Bus;
DeList={'Bbc_CtrlRgnBrkCtrlrActStFlg'};
Bbc_CtrlRgnBrkCtrlrActStFlg = CreateBus(Bbc_CtrlRgnBrkCtrlrActStFlg, DeList);
clear DeList;

Bbc_CtrlVehSpdFild = Simulink.Bus;
DeList={'Bbc_CtrlVehSpdFild'};
Bbc_CtrlVehSpdFild = CreateBus(Bbc_CtrlVehSpdFild, DeList);
clear DeList;

Bbc_CtrlBaseBrkCtrlModInfo = Simulink.Bus;
DeList={
    'LowSpdCtrlInhibitFlg'
    'StopdVehCtrlModFlg'
    'VehStandStillStFlg'
    };
Bbc_CtrlBaseBrkCtrlModInfo = CreateBus(Bbc_CtrlBaseBrkCtrlModInfo, DeList);
clear DeList;

Bbc_CtrlBBCCtrlInfo_t = Simulink.Bus;
DeList={
    'BBCCtrlSt'
    };
Bbc_CtrlBBCCtrlInfo_t = CreateBus(Bbc_CtrlBBCCtrlInfo_t, DeList);
clear DeList;

Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo = Simulink.Bus;
DeList={
    'PedlSimrPGendOnFlg'
    };
Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo = CreateBus(Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo, DeList);
clear DeList;

Bbc_CtrlBaseBrkCtrlrActFlg = Simulink.Bus;
DeList={'Bbc_CtrlBaseBrkCtrlrActFlg'};
Bbc_CtrlBaseBrkCtrlrActFlg = CreateBus(Bbc_CtrlBaseBrkCtrlrActFlg, DeList);
clear DeList;

Bbc_CtrlTarPFromBaseBrkCtrlr = Simulink.Bus;
DeList={'Bbc_CtrlTarPFromBaseBrkCtrlr'};
Bbc_CtrlTarPFromBaseBrkCtrlr = CreateBus(Bbc_CtrlTarPFromBaseBrkCtrlr, DeList);
clear DeList;

Bbc_CtrlTarPRateFromBaseBrkCtrlr = Simulink.Bus;
DeList={'Bbc_CtrlTarPRateFromBaseBrkCtrlr'};
Bbc_CtrlTarPRateFromBaseBrkCtrlr = CreateBus(Bbc_CtrlTarPRateFromBaseBrkCtrlr, DeList);
clear DeList;

Bbc_CtrlTarPFromBrkPedl = Simulink.Bus;
DeList={'Bbc_CtrlTarPFromBrkPedl'};
Bbc_CtrlTarPFromBrkPedl = CreateBus(Bbc_CtrlTarPFromBrkPedl, DeList);
clear DeList;

