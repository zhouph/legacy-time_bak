# \file
#
# \brief Bbc
#
# This file contains the implementation of the SWC
# module Bbc.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Bbc_src

Bbc_src_FILES        += $(Bbc_SRC_PATH)\Bbc_Ctrl.c
Bbc_src_FILES        += $(Bbc_SRC_PATH)\LCBBC_DecideCtrlSt.c
Bbc_src_FILES        += $(Bbc_SRC_PATH)\LCBBC_DecidePedlSimrPGen.c
Bbc_src_FILES        += $(Bbc_SRC_PATH)\LCBBC_DecideVehSts.c
Bbc_src_FILES        += $(Bbc_SRC_PATH)\LCBBC_EstDrvrIntdTP.c
Bbc_src_FILES        += $(Bbc_SRC_PATH)\LCBBC_LimBrkTP.c
Bbc_src_FILES        += $(Bbc_SRC_PATH)\LCBBC_CalTargetPRate.c
Bbc_src_FILES        += $(Bbc_IFA_PATH)\Bbc_Ctrl_Ifa.c
Bbc_src_FILES        += $(Bbc_CFG_PATH)\Bbc_Cfg.c
Bbc_src_FILES        += $(Bbc_CAL_PATH)\Bbc_Cal.c

# /* HSH */
ifeq ($(ICE_COMPILE),true)
# Library
Bbc_src_FILES        += $(Bbc_CORE_PATH)\virtual_main.c
Bbc_src_FILES  += $(Bbc_CORE_PATH)\ICE\CMN\LIB\SwcCommonFunction.c
Bbc_src_FILES  += $(Bbc_CORE_PATH)\ICE\CMN\LIB\L_CommonFunction.c
Bbc_src_FILES  += $(Bbc_CORE_PATH)\ICE\CMN\FBL_HDR\FBL_HMC\Fbl_Header.c
Bbc_src_FILES  += $(Bbc_CORE_PATH)\ICE\CMN\FBL_HDR\FBL_HMC\Fbl_Mtab.c
endif

ifeq ($(UNIT_TEST),true)
	Bbc_src_FILES        += $(Bbc_UNITY_PATH)\unity.c
	Bbc_src_FILES        += $(Bbc_UNITY_PATH)\unity_fixture.c	
	Bbc_src_FILES        += $(Bbc_UT_PATH)\main.c
	Bbc_src_FILES        += $(Bbc_UT_PATH)\Bbc_Ctrl\Bbc_Ctrl_UtMain.c
endif