DATA_BASEBRAKE_TP_LIMIT_t   apCalBaseBrkTPLimit =
{
		/* int16_t      S16_LIMITED_TARGET_P_AT_DELTAP2       */             1000,

        /* int16_t      S16_LIMITED_TARGET_P_AT_DELTAP3       */             3000,

        /* int16_t      S16_LIMITED_TARGET_P_AT_DELTAP4       */             5000,

        /* int16_t      S16_LIMITED_TARGET_P_AT_DELTAP5       */            10000,

        /* int16_t      S16_MAX_LINEAR_TARGET_P_AT_0KPH       */             1000,

        /* int16_t      S16_ONSTOP_LIMITED_TARGET_P_MAX       */             9000,
};
