const DATA_BASEBRAKE_ENTEREXIT_t        apCalBaseBrkEnterExit =
{
        /* int16_t      S16_BBCON_PD_MIN                      */              30,

        /* uint16_t     U16_BBC_FADE_OUT_TIME_MAX             */             400,

        /* int16_t      S16_BBCON_PD_BF_EOL_CORRECTION        */             150,

        /* int16_t      S16_BBCON_PD_OFFST_INVALID            */             100,

        /* int16_t      S16_BBCON_PD_OFFST_INVALID_WBLS       */              30,
};
