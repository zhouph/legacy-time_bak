/**
 * @defgroup Bbc_Cal Bbc_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Bbc_Cal.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bbc_Cal.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define BBC_START_SEC_CALIB_UNSPECIFIED
#include "Bbc_MemMap.h"
/* Global Calibration Section */


#define BBC_STOP_SEC_CALIB_UNSPECIFIED
#include "Bbc_MemMap.h"

#define BBC_START_SEC_CONST_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define BBC_STOP_SEC_CONST_UNSPECIFIED
#include "Bbc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define BBC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

    #if (__CAR == KMC_TD)
        #include "TD/BaseBrakeControl_Target_Pressure_Map_Cal_TD.h"
        #include "TD/BaseBrakeControl_Target_Pressure_Limited_Cal_TD.h"
        #include "TD/BaseBrakeControl_EnterExit_Cal_TD.h"
        #include "TD/BaseBrakeControl_Simulate_Pedal_Pressure_TD.h"

    #elif (__CAR == HMC_BH)
        #include "BH/BaseBrakeControl_Target_Pressure_Map_Cal_BH.h"
        #include "BH/BaseBrakeControl_Target_Pressure_Limited_Cal_BH.h"
        #include "BH/BaseBrakeControl_EnterExit_Cal_BH.h"
        #include "BH/BaseBrakeControl_Simulate_Pedal_Pressure_BH.h"
    #elif (__CAR == HMC_YFE)
        #include "YFE/BaseBrakeControl_Target_Pressure_Map_Cal_YFE.h"
        #include "YFE/BaseBrakeControl_Target_Pressure_Limited_Cal_YFE.h"
        #include "YFE/BaseBrakeControl_EnterExit_Cal_YFE.h"
        #include "YFE/BaseBrakeControl_Simulate_Pedal_Pressure_YFE.h"
	#elif (__CAR == KMC_TFE)
        #include "TFE/BaseBrakeControl_Target_Pressure_Map_Cal_TFE.h"
        #include "TFE/BaseBrakeControl_Target_Pressure_Limited_Cal_TFE.h"
        #include "TFE/BaseBrakeControl_EnterExit_Cal_TFE.h"
        #include "TFE/BaseBrakeControl_Simulate_Pedal_Pressure_TFE.h"
	#elif (__CAR == HMC_HGE)
        #include "HGE/BaseBrakeControl_Target_Pressure_Map_Cal_HGE.h"
        #include "HGE/BaseBrakeControl_Target_Pressure_Limited_Cal_HGE.h"
        #include "HGE/BaseBrakeControl_EnterExit_Cal_HGE.h"
        #include "HGE/BaseBrakeControl_Simulate_Pedal_Pressure_HGE.h"

    #endif

BaseBrakeControl_CalibType BBCcalib =
{
	&apCalBaseBrkTPMap,
	&apCalBaseBrkTPLimit,
	&apCalBaseBrkEnterExit,
	&apCalBaseBrkPedalSimOn,

};

#define BBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define BBC_STOP_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define BBC_STOP_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (32BIT)**/


#define BBC_STOP_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define BBC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define BBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define BBC_STOP_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define BBC_STOP_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (32BIT)**/


#define BBC_STOP_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define BBC_START_SEC_CODE
#include "Bbc_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define BBC_STOP_SEC_CODE
#include "Bbc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
