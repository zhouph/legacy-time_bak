const DATA_BASEBRAKE_TP_MAP_t    apCalBaseBrkTPMap =
{

        /* int16_t      S16_TARGET_PRESS_SET_SPD_HIGH         */             800,

        /* int16_t      S16_TARGET_PRESS_SET_SPD_LOW          */             136,

        {
        /* int16_t      S16_PEDAL_TRAVEL_01                   */              15,

        /* int16_t      S16_PEDAL_TRAVEL_02                   */              60,

        /* int16_t      S16_PEDAL_TRAVEL_03                   */              80,

        /* int16_t      S16_PEDAL_TRAVEL_04                   */             100,

        /* int16_t      S16_PEDAL_TRAVEL_05                   */             120,

        /* int16_t      S16_PEDAL_TRAVEL_06                   */             150,

        /* int16_t      S16_PEDAL_TRAVEL_07                   */             180,

        /* int16_t      S16_PEDAL_TRAVEL_08                   */             200,

        /* int16_t      S16_PEDAL_TRAVEL_09                   */             250,

        /* int16_t      S16_PEDAL_TRAVEL_10                   */             300,

        /* int16_t      S16_PEDAL_TRAVEL_11                   */             350,

        /* int16_t      S16_PEDAL_TRAVEL_12                   */             400,

        /* int16_t      S16_PEDAL_TRAVEL_13                   */             500,

        /* int16_t      S16_PEDAL_TRAVEL_14                   */             600,

        /* int16_t      S16_PEDAL_TRAVEL_MAX                  */             700,

    },
    {
        /* int16_t      S16_PEDAL_TARGET_P_01                 */               50,

        /* int16_t      S16_PEDAL_TARGET_P_02                 */              120,

        /* int16_t      S16_PEDAL_TARGET_P_03                 */              140,

        /* int16_t      S16_PEDAL_TARGET_P_04                 */              200,

        /* int16_t      S16_PEDAL_TARGET_P_05                 */              220,

        /* int16_t      S16_PEDAL_TARGET_P_06                 */              370,

        /* int16_t      S16_PEDAL_TARGET_P_07                 */              540,

        /* int16_t      S16_PEDAL_TARGET_P_08                 */              770,

        /* int16_t      S16_PEDAL_TARGET_P_09                 */             1300,

        /* int16_t      S16_PEDAL_TARGET_P_10                 */             2080,

        /* int16_t      S16_PEDAL_TARGET_P_11                 */             3070,

        /* int16_t      S16_PEDAL_TARGET_P_12                 */             4500,

        /* int16_t      S16_PEDAL_TARGET_P_13                 */             7300,

        /* int16_t      S16_PEDAL_TARGET_P_14                 */            11000,

        /* int16_t      S16_PEDAL_TARGET_P_MAX                */            15000,

    },
    {
        /* int16_t      S16_PEDAL_TARGET_P_01_MED_SPD         */               50,

        /* int16_t      S16_PEDAL_TARGET_P_02_MED_SPD         */              120,

        /* int16_t      S16_PEDAL_TARGET_P_03_MED_SPD         */              140,

        /* int16_t      S16_PEDAL_TARGET_P_04_MED_SPD         */              200,

        /* int16_t      S16_PEDAL_TARGET_P_05_MED_SPD         */              220,

        /* int16_t      S16_PEDAL_TARGET_P_06_MED_SPD         */              350,

        /* int16_t      S16_PEDAL_TARGET_P_07_MED_SPD         */              500,

        /* int16_t      S16_PEDAL_TARGET_P_08_MED_SPD         */              730,

        /* int16_t      S16_PEDAL_TARGET_P_09_MED_SPD         */             1230,

        /* int16_t      S16_PEDAL_TARGET_P_10_MED_SPD         */             1980,

        /* int16_t      S16_PEDAL_TARGET_P_11_MED_SPD         */             2950,

        /* int16_t      S16_PEDAL_TARGET_P_12_MED_SPD         */             4500,

        /* int16_t      S16_PEDAL_TARGET_P_13_MED_SPD         */             7300,

        /* int16_t      S16_PEDAL_TARGET_P_14_MED_SPD         */            11000,

        /* int16_t      S16_PEDAL_TARGET_P_MAX_MED_SPD        */            15000,

    },
    {
        /* int16_t      S16_PEDAL_TARGET_P_01_LOW_SPD         */               50,

        /* int16_t      S16_PEDAL_TARGET_P_02_LOW_SPD         */              120,

        /* int16_t      S16_PEDAL_TARGET_P_03_LOW_SPD         */              140,

        /* int16_t      S16_PEDAL_TARGET_P_04_LOW_SPD         */              180,

        /* int16_t      S16_PEDAL_TARGET_P_05_LOW_SPD         */              220,

        /* int16_t      S16_PEDAL_TARGET_P_06_LOW_SPD         */              330,

        /* int16_t      S16_PEDAL_TARGET_P_07_LOW_SPD         */              450,

        /* int16_t      S16_PEDAL_TARGET_P_08_LOW_SPD         */              630,

        /* int16_t      S16_PEDAL_TARGET_P_09_LOW_SPD         */             1100,

        /* int16_t      S16_PEDAL_TARGET_P_10_LOW_SPD         */             1800,

        /* int16_t      S16_PEDAL_TARGET_P_11_LOW_SPD         */             2750,

        /* int16_t      S16_PEDAL_TARGET_P_12_LOW_SPD         */             4500,

        /* int16_t      S16_PEDAL_TARGET_P_13_LOW_SPD         */             7300,

        /* int16_t      S16_PEDAL_TARGET_P_14_LOW_SPD         */            11000,

        /* int16_t      S16_PEDAL_TARGET_P_MAX_LOW_SPD        */            15000,

    },
    {
        /* int16_t      S16_PEDAL_TARGET_P_01_EBD_FAIL        */               50,

        /* int16_t      S16_PEDAL_TARGET_P_02_EBD_FAIL        */              120,

        /* int16_t      S16_PEDAL_TARGET_P_03_EBD_FAIL        */              140,

        /* int16_t      S16_PEDAL_TARGET_P_04_EBD_FAIL        */              200,

        /* int16_t      S16_PEDAL_TARGET_P_05_EBD_FAIL        */              220,

        /* int16_t      S16_PEDAL_TARGET_P_06_EBD_FAIL        */              350,

        /* int16_t      S16_PEDAL_TARGET_P_07_EBD_FAIL        */              500,

        /* int16_t      S16_PEDAL_TARGET_P_08_EBD_FAIL        */              730,

        /* int16_t      S16_PEDAL_TARGET_P_09_EBD_FAIL        */             1230,

        /* int16_t      S16_PEDAL_TARGET_P_10_EBD_FAIL        */             1980,

        /* int16_t      S16_PEDAL_TARGET_P_11_EBD_FAIL        */             2950,

        /* int16_t      S16_PEDAL_TARGET_P_12_EBD_FAIL        */             4500,

        /* int16_t      S16_PEDAL_TARGET_P_13_EBD_FAIL        */             7300,

        /* int16_t      S16_PEDAL_TARGET_P_14_EBD_FAIL        */            11000,

        /* int16_t      S16_PEDAL_TARGET_P_MAX_EBD_FAIL       */            15000,

    },
    {
        /* int16_t      S16_PEDAL_TARGET_P_01_LFU             */              100,

        /* int16_t      S16_PEDAL_TARGET_P_02_LFU             */              300,

        /* int16_t      S16_PEDAL_TARGET_P_03_LFU             */              500,

        /* int16_t      S16_PEDAL_TARGET_P_04_LFU             */              600,

        /* int16_t      S16_PEDAL_TARGET_P_05_LFU             */              700,

        /* int16_t      S16_PEDAL_TARGET_P_06_LFU             */              900,

        /* int16_t      S16_PEDAL_TARGET_P_07_LFU             */             1100,

        /* int16_t      S16_PEDAL_TARGET_P_08_LFU             */             1300,

        /* int16_t      S16_PEDAL_TARGET_P_09_LFU             */             1800,

        /* int16_t      S16_PEDAL_TARGET_P_10_LFU             */             2500,

        /* int16_t      S16_PEDAL_TARGET_P_11_LFU             */             3400,

        /* int16_t      S16_PEDAL_TARGET_P_12_LFU             */             4400,

        /* int16_t      S16_PEDAL_TARGET_P_13_LFU             */             6500,

        /* int16_t      S16_PEDAL_TARGET_P_14_LFU             */             9500,

        /* int16_t      S16_PEDAL_TARGET_P_MAX_LFU            */            15000,

    },
    {
        /* int16_t      S16_PEDAL_TARGET_P_01_FLEX_1          */              100,

        /* int16_t      S16_PEDAL_TARGET_P_02_FLEX_1          */              300,

        /* int16_t      S16_PEDAL_TARGET_P_03_FLEX_1          */              500,

        /* int16_t      S16_PEDAL_TARGET_P_04_FLEX_1          */              600,

        /* int16_t      S16_PEDAL_TARGET_P_05_FLEX_1          */              700,

        /* int16_t      S16_PEDAL_TARGET_P_06_FLEX_1          */              900,

        /* int16_t      S16_PEDAL_TARGET_P_07_FLEX_1          */             1100,

        /* int16_t      S16_PEDAL_TARGET_P_08_FLEX_1          */             1300,

        /* int16_t      S16_PEDAL_TARGET_P_09_FLEX_1          */             1800,

        /* int16_t      S16_PEDAL_TARGET_P_10_FLEX_1          */             2500,

        /* int16_t      S16_PEDAL_TARGET_P_11_FLEX_1          */             3400,

        /* int16_t      S16_PEDAL_TARGET_P_12_FLEX_1          */             4400,

        /* int16_t      S16_PEDAL_TARGET_P_13_FLEX_1          */             6500,

        /* int16_t      S16_PEDAL_TARGET_P_14_FLEX_1          */             9500,

        /* int16_t      S16_PEDAL_TARGET_P_MAX_FLEX_1         */            15000,

    },
    {
        /* int16_t      S16_PEDAL_TARGET_P_01_FLEX_2          */              100,

        /* int16_t      S16_PEDAL_TARGET_P_02_FLEX_2          */              300,

        /* int16_t      S16_PEDAL_TARGET_P_03_FLEX_2          */              500,

        /* int16_t      S16_PEDAL_TARGET_P_04_FLEX_2          */              600,

        /* int16_t      S16_PEDAL_TARGET_P_05_FLEX_2          */              700,

        /* int16_t      S16_PEDAL_TARGET_P_06_FLEX_2          */              900,

        /* int16_t      S16_PEDAL_TARGET_P_07_FLEX_2          */             1100,

        /* int16_t      S16_PEDAL_TARGET_P_08_FLEX_2          */             1300,

        /* int16_t      S16_PEDAL_TARGET_P_09_FLEX_2          */             1800,

        /* int16_t      S16_PEDAL_TARGET_P_10_FLEX_2          */             2500,

        /* int16_t      S16_PEDAL_TARGET_P_11_FLEX_2          */             3400,

        /* int16_t      S16_PEDAL_TARGET_P_12_FLEX_2          */             4400,

        /* int16_t      S16_PEDAL_TARGET_P_13_FLEX_2          */             6500,

        /* int16_t      S16_PEDAL_TARGET_P_14_FLEX_2          */             9500,

        /* int16_t      S16_PEDAL_TARGET_P_MAX_FLEX_2         */            15000,

    }
};
