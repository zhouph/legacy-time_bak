/**
 * @defgroup Bbc_Cal Bbc_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Bbc_Cal.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef BBC_CAL_H_
#define BBC_CAL_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bbc_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
#define TP_MAP_POINT									15

#define S16_TARGET_PRESS_SET_SPD_HIGH					BBCcalib.apCalBaseBrkTPMap->TARGET_PRESS_SET_SPD_HIGH
#define S16_TARGET_PRESS_SET_SPD_LOW					BBCcalib.apCalBaseBrkTPMap->TARGET_PRESS_SET_SPD_LOW

#define S16_PEDAL_TRAVEL(index)							BBCcalib.apCalBaseBrkTPMap->s16BaseBrakePedalMap[index]
#define S16_PEDAL_TARGET_P_MAX							BBCcalib.apCalBaseBrkTPMap->s16BaseBrakeNormalTPMap[14]
#define S16_PEDAL_TARGET_P	                	        BBCcalib.apCalBaseBrkTPMap->s16BaseBrakeNormalTPMap
#define S16_PEDAL_TARGET_P_MED_SPD	    	            BBCcalib.apCalBaseBrkTPMap->s16BaseBrakeMedSpdTPMap
#define S16_PEDAL_TARGET_P_LOW_SPD		                BBCcalib.apCalBaseBrkTPMap->s16BaseBrakeLowSpdTPMap
#define S16_PEDAL_TARGET_P_EBD_FAIL		                BBCcalib.apCalBaseBrkTPMap->s16BaseBrakeEBDFailTPMap
#define S16_PEDAL_TARGET_P_LFU		                    BBCcalib.apCalBaseBrkTPMap->s16BaseBrakeLFUModeTPMap
#define S16_PEDAL_TARGET_P_FLEX1	                    BBCcalib.apCalBaseBrkTPMap->s16BaseBrakeFlex1ModeTPMap
#define S16_PEDAL_TARGET_P_FLEX2	                    BBCcalib.apCalBaseBrkTPMap->s16BaseBrakeFlex2ModeTPMap

#define S16_LIMITED_TARGET_P_AT_DELTAP2					BBCcalib.apCalBaseBrkTPLimit->LIMITED_TARGET_P_AT_DELTAP2
#define S16_LIMITED_TARGET_P_AT_DELTAP3					BBCcalib.apCalBaseBrkTPLimit->LIMITED_TARGET_P_AT_DELTAP3
#define S16_LIMITED_TARGET_P_AT_DELTAP4                 BBCcalib.apCalBaseBrkTPLimit->LIMITED_TARGET_P_AT_DELTAP4
#define S16_LIMITED_TARGET_P_AT_DELTAP5                 BBCcalib.apCalBaseBrkTPLimit->LIMITED_TARGET_P_AT_DELTAP5
#define S16_MAX_LINEAR_TARGET_P_AT_0KPH                 BBCcalib.apCalBaseBrkTPLimit->MAX_LINEAR_TARGET_P_AT_0KPH
#define S16_ONSTOP_LIMITED_TARGET_P_MAX                 BBCcalib.apCalBaseBrkTPLimit->ONSTOP_LIMITED_TARGET_P_MAX

#define S16_BBCON_PD_MIN								BBCcalib.apCalBaseBrkEnterExit->BBCON_PD_MIN
#define S16_BBC_FADE_OUT_TIME_MAX						BBCcalib.apCalBaseBrkEnterExit->BBC_FADE_OUT_TIME_MAX
#define S16_BBCON_PD_BF_EOL_CORRECTION                  BBCcalib.apCalBaseBrkEnterExit->BBCON_PD_BF_EOL_CORRECTION
#define S16_BBCON_PD_OFFST_INVALID                 		BBCcalib.apCalBaseBrkEnterExit->BBCON_PD_OFFST_INVALID
#define S16_BBCON_PD_OFFST_INVALID_WBLS                 BBCcalib.apCalBaseBrkEnterExit->BBCON_PD_OFFST_INVALID_WBLS

#define S16_CV_ON_PEDAL_TRAVEL							BBCcalib.apCalBaseBrkPedalSimOn->CV_ON_PEDAL_TRAVEL
#define S16_CV_OFF_PEDAL_TRAVEL							BBCcalib.apCalBaseBrkPedalSimOn->CV_OFF_PEDAL_TRAVEL
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

typedef struct
{
    int16_t         TARGET_PRESS_SET_SPD_HIGH;          /* Comment [ Normal Target Press Map이 적용되는 최저 속도 (default 20KPH) ] */
                                                        /* Group   [ /apCalAhb/Target Pressure Map/Target Press Set Speed ] */
                                                        /* Scale   [ f(x) = (X + 0) * 0.125 ]  Range [ -4096 <= X <= 4095.875 ] */
    int16_t         TARGET_PRESS_SET_SPD_LOW;           /* Comment [ Low Speed Target Press Map이 적용되는 최저 속도 (default 10KPH) ] */
                                                        /* Group   [ /apCalAhb/Target Pressure Map/Target Press Set Speed ] */
                                                        /* Scale   [ f(x) = (X + 0) * 0.125 ]  Range [ -4096 <= X <= 4095.875 ] */
    int16_t			s16BaseBrakePedalMap[TP_MAP_POINT];
    int16_t			s16BaseBrakeNormalTPMap[TP_MAP_POINT];
    int16_t			s16BaseBrakeMedSpdTPMap[TP_MAP_POINT];
    int16_t			s16BaseBrakeLowSpdTPMap[TP_MAP_POINT];
    int16_t			s16BaseBrakeEBDFailTPMap[TP_MAP_POINT];
    int16_t			s16BaseBrakeLFUModeTPMap[TP_MAP_POINT];
    int16_t			s16BaseBrakeFlex1ModeTPMap[TP_MAP_POINT];
    int16_t			s16BaseBrakeFlex2ModeTPMap[TP_MAP_POINT];
}DATA_BASEBRAKE_TP_MAP_t;

typedef struct
{
    int16_t         LIMITED_TARGET_P_AT_DELTAP2;        /* Comment [ Delta P2구간에서의 target P 최저 limit ] */
                                                        /* Group   [ /apCalAhb/Target Pressure Map/Target P Limitation ] */
                                                        /* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ -3276.8 <= X <= 3276.7 ] */
    int16_t         LIMITED_TARGET_P_AT_DELTAP3;        /* Comment [ Delta P3구간에서의 target P 최저 limit ] */
                                                        /* Group   [ /apCalAhb/Target Pressure Map/Target P Limitation ] */
                                                        /* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ -3276.8 <= X <= 3276.7 ] */
    int16_t         LIMITED_TARGET_P_AT_DELTAP4;        /* Comment [ Delta P4구간에서의 target P 최저 limit ] */
                                                        /* Group   [ /apCalAhb/Target Pressure Map/Target P Limitation ] */
                                                        /* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ -3276.8 <= X <= 3276.7 ] */
    int16_t         LIMITED_TARGET_P_AT_DELTAP5;        /* Comment [ Delta P5구간에서의 target P 최저 limit ] */
                                                        /* Group   [ /apCalAhb/Target Pressure Map/Target P Limitation ] */
                                                        /* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ -3276.8 <= X <= 3276.7 ] */
    int16_t         MAX_LINEAR_TARGET_P_AT_0KPH;        /* Comment [ Target Press limitation 적용 시작 press ] */
                                                        /* Group   [ /apCalAhb/Target Pressure Map/Target P Limitation ] */
                                                        /* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ -3276.8 <= X <= 3276.7 ] */
    int16_t         ONSTOP_LIMITED_TARGET_P_MAX;        /* Comment [ 정차중 Target P 제한 최대값 설정 ] */
                                                        /* Group   [ /apCalAhb/Target Pressure Map/Target P Limitation ] */
                                                        /* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ -3276.8 <= X <= 3276.7 ] */
}DATA_BASEBRAKE_TP_LIMIT_t;

typedef struct
{
    int16_t         BBCON_PD_MIN;                       /* Comment [ AHB 작동하는 최저 Pedal Travel 값 (Min 2mm, 3mm 이상 권장) ] */
                                                        /* Group   [ /apCalAhb/AHB Active Threshold ] */
                                                        /* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ -3276.8 <= X <= 3276.7 ] */
    uint16_t        BBC_FADE_OUT_TIME_MAX;              /* Comment [ Release 종료 시 RV Fade Out을 허용하는 최대 시간 ] */
                                                        /* Group   [ /apCalAhb/Ahb Logic Tuning/AHB Controller Logic Tuning/AHB Control Remain ] */
                                                        /* Scale   [ f(x) = (X + 0) * 5 ]  Range [ 0 <= X <= 327675 ] */
    int16_t         BBCON_PD_BF_EOL_CORRECTION;         /* Comment [ AHB 유압 제어 허용 최소 Pedal stroke before EOL offset calibration ] */
                                                        /* Group   [ /apCalAhb/Ahb Logic Tuning/AHB Controller Logic Tuning/AHB Active Threshold - Logic ] */
                                                        /* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ -3276.8 <= X <= 3276.7 ] */
    int16_t         BBCON_PD_OFFST_INVALID;             /* Comment [ AHB 유압 제어 허용 최소 Pedal stroke (w/o BLS) at Pedal offset invalid ] */
                                                        /* Group   [ /apCalAhb/Ahb Logic Tuning/AHB Controller Logic Tuning/AHB Active Threshold - Logic ] */
                                                        /* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ -3276.8 <= X <= 3276.7 ] */
    int16_t         BBCON_PD_OFFST_INVALID_WBLS;        /* Comment [ AHB 유압 제어 허용 최소 Pedal stroke (w BLS) at Pedal offset invalid ] */
                                                        /* Group   [ /apCalAhb/Ahb Logic Tuning/AHB Controller Logic Tuning/AHB Active Threshold - Logic ] */
                                                        /* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ -3276.8 <= X <= 3276.7 ] */
}DATA_BASEBRAKE_ENTEREXIT_t;

typedef struct
{
	int16_t     	CV_ON_PEDAL_TRAVEL;              	/* Comment [ Pedal Simulator에 압력을 형성하는 (Cut Valve를 Close하는) Pedal Travel 기준 값 ] */
	            	                                   	/* Group   [ /apCalAhb/Pedal Simulator Pressure generation ] */
	            	                                   	/* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ -3276.8 <= X <= 3276.7 ] */
	int16_t     	CV_OFF_PEDAL_TRAVEL;           		/* Comment [ Pedal Simulator의 압력을 해제하는 (Cut Valve를 Open하는) Pedal Travel 기준 값 ] */
	            	                                   	/* Group   [ /apCalAhb/Pedal Simulator Pressure generation ] */
	            	                                   	/* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ -3276.8 <= X <= 3276.7 ] */
}DATA_BASEBRAKE_PEDAL_SIM_ON_t;

typedef struct
{
    const DATA_BASEBRAKE_TP_MAP_t *             apCalBaseBrkTPMap;
    const DATA_BASEBRAKE_TP_LIMIT_t *         	apCalBaseBrkTPLimit;
    const DATA_BASEBRAKE_ENTEREXIT_t *          apCalBaseBrkEnterExit;
    const DATA_BASEBRAKE_PEDAL_SIM_ON_t *       apCalBaseBrkPedalSimOn;
} BaseBrakeControl_CalibType;
/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
extern BaseBrakeControl_CalibType BBCcalib;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* BBC_CAL_H_ */
/** @} */
