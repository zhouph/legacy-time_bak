        /* int16_t      S16_TARGET_PRESS_SET_SPD_HIGH         */             800,   /* Comment [ Normal Target Press Map이 적용되는 최저 속도 (default 20KPH) ] */
                                                                                    /* ScaleVal[    100 KPH ]   Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
        /* int16_t      S16_TARGET_PRESS_SET_SPD_LOW          */             136,   /* Comment [ Low Speed Target Press Map이 적용되는 최저 속도 (default 10KPH) ] */
                                                                                    /* ScaleVal[     17 KPH ]   Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
        /* int16_t      S16_PEDAL_TRAVEL_01                   */              15,   /* Comment [ Pedal Travel_01의 정의 ] */
                                                                                    /* ScaleVal[     1.5 mm ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TRAVEL_02                   */              60,   /* Comment [ Pedal Travel_02의 정의 ] */
                                                                                    /* ScaleVal[       6 mm ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TRAVEL_03                   */              80,   /* Comment [ Pedal Travel_03의 정의 ] */
                                                                                    /* ScaleVal[       8 mm ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TRAVEL_04                   */             100,   /* Comment [ Pedal Travel_04의 정의 ] */
                                                                                    /* ScaleVal[      10 mm ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TRAVEL_05                   */             120,   /* Comment [ Pedal Travel_05의 정의 ] */
                                                                                    /* ScaleVal[      12 mm ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TRAVEL_06                   */             150,   /* Comment [ Pedal Travel_06의 정의 ] */
                                                                                    /* ScaleVal[      15 mm ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TRAVEL_07                   */             180,   /* Comment [ Pedal Travel_07의 정의 ] */
                                                                                    /* ScaleVal[      18 mm ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TRAVEL_08                   */             200,   /* Comment [ Pedal Travel_08의 정의 ] */
                                                                                    /* ScaleVal[      20 mm ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TRAVEL_09                   */             250,   /* Comment [ Pedal Travel_09의 정의 ] */
                                                                                    /* ScaleVal[      25 mm ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TRAVEL_10                   */             300,   /* Comment [ Pedal Travel_10의 정의 ] */
                                                                                    /* ScaleVal[      30 mm ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TRAVEL_11                   */             350,   /* Comment [ Pedal Travel_11의 정의 ] */
                                                                                    /* ScaleVal[      35 mm ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TRAVEL_12                   */             400,   /* Comment [ Pedal Travel_12의 정의 ] */
                                                                                    /* ScaleVal[      40 mm ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TRAVEL_13                   */             500,   /* Comment [ Pedal Travel_13의 정의 ] */
                                                                                    /* ScaleVal[      50 mm ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TRAVEL_14                   */             600,   /* Comment [ Pedal Travel_14의 정의 ] */
                                                                                    /* ScaleVal[      60 mm ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TRAVEL_MAX                  */             700,   /* Comment [ Pedal Travel_MAX의 정의 ] */
                                                                                    /* ScaleVal[      70 mm ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_01                 */               5,   /* Comment [ Pedal이 Travel_1만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    0.5 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_02                 */              12,   /* Comment [ Pedal이 Travel_2만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    1.2 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_03                 */              14,   /* Comment [ Pedal이 Travel_3만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    1.4 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_04                 */              20,   /* Comment [ Pedal이 Travel_4만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[      2 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_05                 */              22,   /* Comment [ Pedal이 Travel_5만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    2.2 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_06                 */              37,   /* Comment [ Pedal이 Travel_6만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    3.7 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_07                 */              54,   /* Comment [ Pedal이 Travel_7만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    5.4 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_08                 */              77,   /* Comment [ Pedal이 Travel_8만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    7.7 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_09                 */             130,   /* Comment [ Pedal이 Travel_9만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     13 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_10                 */             208,   /* Comment [ Pedal이 Travel_10만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[   20.8 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_11                 */             307,   /* Comment [ Pedal이 Travel_11만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[   30.7 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_12                 */             450,   /* Comment [ Pedal이 Travel_12만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     45 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_13                 */             730,   /* Comment [ Pedal이 Travel_13만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     73 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_14                 */            1100,   /* Comment [ Pedal이 Travel_14만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    110 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_MAX                */            1500,   /* Comment [ Pedal이 Travel_MAX만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    150 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_01_MED_SPD         */               5,   /* Comment [ 중속에서 Pedal이 Travel_1만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    0.5 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_02_MED_SPD         */              12,   /* Comment [ 중속에서 Pedal이 Travel_2만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    1.2 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_03_MED_SPD         */              14,   /* Comment [ 중속에서 Pedal이 Travel_3만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    1.4 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_04_MED_SPD         */              20,   /* Comment [ 중속에서 Pedal이 Travel_4만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[      2 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_05_MED_SPD         */              22,   /* Comment [ 중속에서 Pedal이 Travel_5만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    2.2 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_06_MED_SPD         */              35,   /* Comment [ 중속에서 Pedal이 Travel_6만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    3.5 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_07_MED_SPD         */              50,   /* Comment [ 중속에서 Pedal이 Travel_7만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[      5 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_08_MED_SPD         */              73,   /* Comment [ 중속에서 Pedal이 Travel_8만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    7.3 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_09_MED_SPD         */             123,   /* Comment [ 중속에서 Pedal이 Travel_9만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[   12.3 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_10_MED_SPD         */             198,   /* Comment [ 중속에서 Pedal이 Travel_10만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[   19.8 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_11_MED_SPD         */             295,   /* Comment [ 중속에서 Pedal이 Travel_11만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[   29.5 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_12_MED_SPD         */             450,   /* Comment [ 중속에서 Pedal이 Travel_12만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     45 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_13_MED_SPD         */             730,   /* Comment [ 중속에서 Pedal이 Travel_13만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     73 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_14_MED_SPD         */            1100,   /* Comment [ 중속에서 Pedal이 Travel_14만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    110 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_MAX_MED_SPD        */            1500,   /* Comment [ 중속에서 Pedal이 Travel_Max만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    150 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_01_LOW_SPD         */               5,   /* Comment [ 저속에서 Pedal이 Travel_1만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    0.5 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_02_LOW_SPD         */              12,   /* Comment [ 저속에서 Pedal이 Travel_2만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    1.2 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_03_LOW_SPD         */              14,   /* Comment [ 저속에서 Pedal이 Travel_3만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    1.4 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_04_LOW_SPD         */              18,   /* Comment [ 저속에서 Pedal이 Travel_4만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    1.8 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_05_LOW_SPD         */              22,   /* Comment [ 저속에서 Pedal이 Travel_5만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    2.2 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_06_LOW_SPD         */              33,   /* Comment [ 저속에서 Pedal이 Travel_6만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    3.3 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_07_LOW_SPD         */              45,   /* Comment [ 저속에서 Pedal이 Travel_7만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    4.5 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_08_LOW_SPD         */              63,   /* Comment [ 저속에서 Pedal이 Travel_8만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    6.3 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_09_LOW_SPD         */             110,   /* Comment [ 저속에서 Pedal이 Travel_9만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     11 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_10_LOW_SPD         */             180,   /* Comment [ 저속에서 Pedal이 Travel_10만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     18 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_11_LOW_SPD         */             275,   /* Comment [ 저속에서 Pedal이 Travel_11만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[   27.5 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_12_LOW_SPD         */             450,   /* Comment [ 저속에서 Pedal이 Travel_12만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     45 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_13_LOW_SPD         */             730,   /* Comment [ 저속에서 Pedal이 Travel_13만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     73 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_14_LOW_SPD         */            1100,   /* Comment [ 저속에서 Pedal이 Travel_14만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    110 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_MAX_LOW_SPD        */            1500,   /* Comment [ 저속에서 Pedal이 Travel_MAX만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    150 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_01_EBD_FAIL        */               5,   /* Comment [ EBD Fail 시 Pedal이 Travel_01만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    0.5 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_02_EBD_FAIL        */              12,   /* Comment [ EBD Fail 시 Pedal이 Travel_02만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    1.2 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_03_EBD_FAIL        */              14,   /* Comment [ EBD Fail 시 Pedal이 Travel_03만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    1.4 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_04_EBD_FAIL        */              20,   /* Comment [ EBD Fail 시 Pedal이 Travel_04만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[      2 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_05_EBD_FAIL        */              22,   /* Comment [ EBD Fail 시 Pedal이 Travel_05만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    2.2 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_06_EBD_FAIL        */              35,   /* Comment [ EBD Fail 시 Pedal이 Travel_06만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    3.5 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_07_EBD_FAIL        */              50,   /* Comment [ EBD Fail 시 Pedal이 Travel_07만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[      5 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_08_EBD_FAIL        */              73,   /* Comment [ EBD Fail 시 Pedal이 Travel_08만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    7.3 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_09_EBD_FAIL        */             123,   /* Comment [ EBD Fail 시 Pedal이 Travel_09만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[   12.3 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_10_EBD_FAIL        */             198,   /* Comment [ EBD Fail 시 Pedal이 Travel_10만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[   19.8 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_11_EBD_FAIL        */             295,   /* Comment [ EBD Fail 시 Pedal이 Travel_11만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[   29.5 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_12_EBD_FAIL        */             450,   /* Comment [ EBD Fail 시 Pedal이 Travel_12만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     45 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_13_EBD_FAIL        */             730,   /* Comment [ EBD Fail 시 Pedal이 Travel_13만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     73 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_14_EBD_FAIL        */            1100,   /* Comment [ EBD Fail 시 Pedal이 Travel_14만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    110 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_MAX_EBD_FAIL       */            1500,   /* Comment [ EBD Fail 시 Pedal이 Travel_MAX만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    150 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_01_LFU             */              10,   /* Comment [ LFU Mode 시 Pedal이 Travel_01만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[      1 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_02_LFU             */              30,   /* Comment [ LFU Mode 시 Pedal이 Travel_02만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[      3 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_03_LFU             */              50,   /* Comment [ LFU Mode 시 Pedal이 Travel_03만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[      5 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_04_LFU             */              60,   /* Comment [ LFU Mode 시 Pedal이 Travel_04만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[      6 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_05_LFU             */              70,   /* Comment [ LFU Mode 시 Pedal이 Travel_05만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[      7 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_06_LFU             */              90,   /* Comment [ LFU Mode 시 Pedal이 Travel_06만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[      9 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_07_LFU             */             110,   /* Comment [ LFU Mode 시 Pedal이 Travel_07만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     11 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_08_LFU             */             130,   /* Comment [ LFU Mode 시 Pedal이 Travel_08만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     13 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_09_LFU             */             180,   /* Comment [ LFU Mode 시 Pedal이 Travel_09만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     18 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_10_LFU             */             250,   /* Comment [ LFU Mode 시 Pedal이 Travel_10만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     25 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_11_LFU             */             340,   /* Comment [ LFU Mode 시 Pedal이 Travel_11만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     34 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_12_LFU             */             440,   /* Comment [ LFU Mode 시 Pedal이 Travel_12만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     44 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_13_LFU             */             650,   /* Comment [ LFU Mode 시 Pedal이 Travel_13만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     65 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_14_LFU             */             950,   /* Comment [ LFU Mode 시 Pedal이 Travel_14만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     95 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_MAX_LFU            */            1500,   /* Comment [ LFU Mode 시 Pedal이 Travel_MAX만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    150 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_01_FLEX_1          */              10,   /* Comment [ FLEX Braking Mode 1 에서 Pedal이 Travel_01만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[      1 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_02_FLEX_1          */              30,   /* Comment [ FLEX Braking Mode 1 에서 Pedal이 Travel_02만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[      3 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_03_FLEX_1          */              50,   /* Comment [ FLEX Braking Mode 1 에서 Pedal이 Travel_03만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[      5 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_04_FLEX_1          */              60,   /* Comment [ FLEX Braking Mode 1 에서 Pedal이 Travel_04만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[      6 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_05_FLEX_1          */              70,   /* Comment [ FLEX Braking Mode 1 에서 Pedal이 Travel_05만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[      7 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_06_FLEX_1          */              90,   /* Comment [ FLEX Braking Mode 1 에서 Pedal이 Travel_06만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[      9 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_07_FLEX_1          */             110,   /* Comment [ FLEX Braking Mode 1 에서 Pedal이 Travel_07만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     11 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_08_FLEX_1          */             130,   /* Comment [ FLEX Braking Mode 1 에서 Pedal이 Travel_08만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     13 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_09_FLEX_1          */             180,   /* Comment [ FLEX Braking Mode 1 에서 Pedal이 Travel_09만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     18 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_10_FLEX_1          */             250,   /* Comment [ FLEX Braking Mode 1 에서 Pedal이 Travel_10만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     25 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_11_FLEX_1          */             340,   /* Comment [ FLEX Braking Mode 1 에서 Pedal이 Travel_11만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     34 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_12_FLEX_1          */             440,   /* Comment [ FLEX Braking Mode 1 에서 Pedal이 Travel_12만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     44 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_13_FLEX_1          */             650,   /* Comment [ FLEX Braking Mode 1 에서 Pedal이 Travel_13만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     65 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_14_FLEX_1          */             950,   /* Comment [ FLEX Braking Mode 1 에서 Pedal이 Travel_14만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     95 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_MAX_FLEX_1         */            1500,   /* Comment [ FLEX Braking Mode 1 에서 Pedal이 Travel_MAX만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    150 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_01_FLEX_2          */              10,   /* Comment [ FLEX Braking Mode 2 에서 Pedal이 Travel_01만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[      1 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_02_FLEX_2          */              30,   /* Comment [ FLEX Braking Mode 2 에서 Pedal이 Travel_02만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[      3 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_03_FLEX_2          */              50,   /* Comment [ FLEX Braking Mode 2 에서 Pedal이 Travel_03만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[      5 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_04_FLEX_2          */              60,   /* Comment [ FLEX Braking Mode 2 에서 Pedal이 Travel_04만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[      6 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_05_FLEX_2          */              70,   /* Comment [ FLEX Braking Mode 2 에서 Pedal이 Travel_05만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[      7 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_06_FLEX_2          */              90,   /* Comment [ FLEX Braking Mode 2 에서 Pedal이 Travel_06만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[      9 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_07_FLEX_2          */             110,   /* Comment [ FLEX Braking Mode 2 에서 Pedal이 Travel_07만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     11 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_08_FLEX_2          */             130,   /* Comment [ FLEX Braking Mode 2 에서 Pedal이 Travel_08만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     13 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_09_FLEX_2          */             180,   /* Comment [ FLEX Braking Mode 2 에서 Pedal이 Travel_09만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     18 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_10_FLEX_2          */             250,   /* Comment [ FLEX Braking Mode 2 에서 Pedal이 Travel_10만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     25 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_11_FLEX_2          */             340,   /* Comment [ FLEX Braking Mode 2 에서 Pedal이 Travel_11만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     34 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_12_FLEX_2          */             440,   /* Comment [ FLEX Braking Mode 2 에서 Pedal이 Travel_12만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     44 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_13_FLEX_2          */             650,   /* Comment [ FLEX Braking Mode 2 에서 Pedal이 Travel_13만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     65 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_14_FLEX_2          */             950,   /* Comment [ FLEX Braking Mode 2 에서 Pedal이 Travel_14만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[     95 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_PEDAL_TARGET_P_MAX_FLEX_2         */            1500,   /* Comment [ FLEX Braking Mode 2 에서 Pedal이 Travel_MAX만큼 눌렸을 때의 Target Boost Chamber Pressure ] */
                                                                                    /* ScaleVal[    150 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_LIMITED_TARGET_P_AT_DELTAP2       */             100,   /* Comment [ Delta P2구간에서의 target P 최저 limit ] */
                                                                                    /* ScaleVal[     10 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_LIMITED_TARGET_P_AT_DELTAP3       */             300,   /* Comment [ Delta P3구간에서의 target P 최저 limit ] */
                                                                                    /* ScaleVal[     30 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_LIMITED_TARGET_P_AT_DELTAP4       */             500,   /* Comment [ Delta P4구간에서의 target P 최저 limit ] */
                                                                                    /* ScaleVal[     50 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_LIMITED_TARGET_P_AT_DELTAP5       */            1000,   /* Comment [ Delta P5구간에서의 target P 최저 limit ] */
                                                                                    /* ScaleVal[    100 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_MAX_LINEAR_TARGET_P_AT_0KPH       */             100,   /* Comment [ Target Press limitation 적용 시작 press ] */
                                                                                    /* ScaleVal[     10 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_ONSTOP_LIMITED_TARGET_P_MAX       */             900,   /* Comment [ 정차중 Target P 제한 최대값 설정 ] */
                                                                                    /* ScaleVal[     90 bar ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_BBCON_PD_MIN                      */              30,   /* Comment [ AHB 작동하는 최저 Pedal Travel 값 (Min 2mm, 3mm 이상 권장) ] */
                                                                                    /* ScaleVal[       3 mm ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* uint16_t     U16_BBC_FADE_OUT_TIME_MAX             */             400,   /* Comment [ Release 종료 시 RV Fade Out을 허용하는 최대 시간 ] */
                                                                                    /* ScaleVal[    2000 ms ]   Scale [ f(x) = (X + 0) * 5 ]  Range   [ 0 <= X <= 327675 ] */
        /* int16_t      S16_BBCON_PD_BF_EOL_CORRECTION        */             150,   /* Comment [ AHB 유압 제어 허용 최소 Pedal stroke before EOL offset calibration ] */
                                                                                        /* ScaleVal[    15 mm/s ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_BBCON_PD_OFFST_INVALID            */             100,   /* Comment [ AHB 유압 제어 허용 최소 Pedal stroke (w/o BLS) at Pedal offset invalid ] */
                                                                                    /* ScaleVal[    10 mm/s ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
        /* int16_t      S16_BBCON_PD_OFFST_INVALID_WBLS       */              30,   /* Comment [ AHB 유압 제어 허용 최소 Pedal stroke (w BLS) at Pedal offset invalid ] */
                                                                                  /* ScaleVal[     3 mm/s ]   Scale [ f(x) = (X + 0) * 0.1 ]  Range   [ -3276.8 <= X <= 3276.7 ] */
