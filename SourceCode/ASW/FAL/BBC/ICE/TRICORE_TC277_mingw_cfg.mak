# \file
#
# \brief AUTOSAR ApplTemplates
#
# This file contains the implementation of the AUTOSAR
# module ApplTemplates.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

# Toolchain settings
# TOOLPATH_COMPILER:
# Path to the tasking compiler and linker.
# It is checked, if an environment variable
# $(SSC_TRICORE_277_TOOLPATH_COMPILER) exists.
# If so, this variable is taken for TOOLPATH_COMPILER
# If it not exists, TOOLPATH_COMPILER must be set by user
# (modify command in line following the ifeq).
#
# For example:
TOOLPATH_COMPILER ?= C:\Compiler\MinGW

# TASKING_MODE:
#
# To compile a C source file the C compiler and the assembler must
# be called. This can be done by one call of the control program cctc
# or by a call of the compiler ctc and the assembler astc. The variable
# TASKING_MODE allows to select the kind of the tool chain call.
# Valid values are CONTROL_PROGRAM and COMPILER_ASSEMBLER.
TASKING_MODE = CONTROL_PROGRAM


# Define the options for the compiler

# Set TriCore EABI compliant mode (don't use half-word alignment)
CC_OPT += 

# Define the default options for the assembler

# Use the TASKING preprocessor
ASM_OPT += 


# Define the options for the linker

# Tell linker about LINK_PLACE options
LINK_OPT += $(LINK_PLACE)



# Enable the usage of our own initialisation code - Mandatory with Safety Os
# Equivalent to --user-provided-initialization-code
#LINK_OPT += -i

# Linker archive options: insert-replace/create/update
AR_OPT += 

# EXT_LOCATOR_FILE:
# specify the name for an external locator file
# if no name is given, a default locator file $(BOARD).ldscript is taken
# which is composed in file <board>.mak
EXT_LOCATOR_FILE +=

# General path setup

# Path where the map file should get generated
MAP_FILE = $(BIN_OUTPUT_PATH)\$(PROJECT).map

# Path where the output file should get generated
OUT_FILE = $(BUILD_DIR)\$(PROJECT).out

# Define the options for preprocessing *.s files before being fed
# into the assembler.

# Add definition of the used Tricore architecture for the OS
ASS_OPT += 

