/**
 * @defgroup LCBBC_DecidePedlSimrPGen LCBBC_DecidePedlSimrPGen
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LCBBC_DecidePedlSimrPGen.h
 * @brief       Local Header File
 * @date        20YY. MM. DD.
 ******************************************************************************/
 
#ifndef LDBBC_DECIDEPEDLSIMRPGEN_H_
#define LDBBC_DECIDEPEDLSIMRPGEN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bbc_Ctrl.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
#define S16_PEDAL_RELEASE_RATE_FAST         -S16_TRAVEL_1_MM
#define U8_CV_ON_TIME_MIN                   U8_T_50_MS
#define S16_CV_ON_PS_PRESSURE_PDTA_ON		-S16_PCTRL_PRESS_0_1_BAR
#define S16_CV_OFF_PS_PRESSURE_PDTA_ON		-S16_PCTRL_PRESS_0_5_BAR
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void LCBBC_vCallPedlSimrPGen(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* LDBBC_DECIDEPEDLSIMRPGEN_H_ */
/** @} */
