/**
 * @defgroup LCBBC_LimBrkTP LCBBC_LimBrkTP
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LCBBC_LimitBrkTP.h
 * @brief       Local Header File
 * @date        20YY. MM. DD.
 ******************************************************************************/
 
#ifndef LCBBC_LIMBRKTP_H_
#define LCBBC_LIMBRKTP_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bbc_Ctrl.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
  #if __BBC_ONSTOP_TARGET_P_LIMIT
	#define S16_ONSTOP_LIMIT_DELTA_PRESS		(S16_PEDAL_TARGET_P_MAX - S16_MAX_LINEAR_TARGET_P_AT_0KPH)

	#define S16_ONSTOP_TP_LIMIT_P5_AT_0KPH      (S16_ONSTOP_LIMITED_TARGET_P_MAX - S16_MAX_LINEAR_TARGET_P_AT_0KPH)
	#define S16_ONSTOP_TP_LIMIT_P2_AT_0KPH      (S16_ONSTOP_TP_LIMIT_P5_AT_0KPH/4)
	#define S16_ONSTOP_TP_LIMIT_P3_AT_0KPH      (S16_ONSTOP_TP_LIMIT_P5_AT_0KPH/2)
	#define S16_ONSTOP_TP_LIMIT_P4_AT_0KPH      ((S16_ONSTOP_TP_LIMIT_P5_AT_0KPH*2)/3)

	#define S16_ONSTOP_TP_LIMIT_P2_AT_1KPH      (S16_ONSTOP_TP_LIMIT_P2_AT_0KPH + S16_PCTRL_PRESS_2_BAR)
	#define S16_ONSTOP_TP_LIMIT_P3_AT_1KPH      (S16_ONSTOP_TP_LIMIT_P3_AT_0KPH + S16_PCTRL_PRESS_5_BAR)
	#define S16_ONSTOP_TP_LIMIT_P4_AT_1KPH      (S16_ONSTOP_TP_LIMIT_P4_AT_0KPH + S16_PCTRL_PRESS_10_BAR)
	#define S16_ONSTOP_TP_LIMIT_P5_AT_1KPH      (S16_ONSTOP_TP_LIMIT_P5_AT_0KPH + S16_PCTRL_PRESS_20_BAR)
  #endif

  #if (__BBC_LOW_SPEED_TARGET_P_LIMIT==ENABLE)
	#define S16_TARGET_LIMIT_ALLOW_LSPD 		  S16_SPEED_10_KPH
	#define S16_STOP_SPEED_OFFSET_TP_LIMIT		  S16_SPEED_7_KPH
  #endif

	#define S16_TP_DIFF_LIMIT_THRES_PRESS        S16_PCTRL_PRESS_70_BAR
	#define S16_LIMITED_TP_DIFF_AT_HP            S16_PCTRL_PRESS_1_BAR
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void LCBBC_vCallBrkTarPLimn(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* LCBBC_LIMITBRKTP_H_ */
/** @} */
