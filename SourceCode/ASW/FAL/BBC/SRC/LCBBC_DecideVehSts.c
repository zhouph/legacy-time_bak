/**
 * @defgroup LDBBC_DecideVehSts LDBBC_DecideVehSts
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LCBBC_DecideVehSts.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "LCBBC_DecideVehSts.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
/* RTE Input Variable */

typedef struct
{
    uint8_t lcbbcu8cTcuGsSel;
    uint8_t lcbbcu8wIgnStat;
#if (__LFU_MODE_CTRL == ENABLE)
    uint8_t lcbbcu8cCF_Tcu_ShiftClass;
#endif
    int16_t lcbbcs16VehicleSpeed;


    uint32_t lcbbcu1FLSpeedInvalid          :1;
    uint32_t lcbbcu1FRSpeedInvalid          :1;
    uint32_t lcbbcu1RLSpeedInvalid          :1;
    uint32_t lcbbcu1RRSpeedInvalid          :1;
    uint32_t lcbbcu1VehicleStopFlg          :1;
#if (__LFU_MODE_CTRL == ENABLE)
    uint32_t lcbbcu1cTcu6RxOkFlg            :1;
    uint32_t lcbbcu1fRBCSFaultDet           :1;
#endif
}GetVehStsInfo_t;

typedef struct
{
    uint32_t lcbbcu1StandstillCtrl          :1;
    uint32_t lcbbcu1LowSpeedCtrlInhibit     :1;
    uint32_t lcbbcu1CtrlStoppedVehicle      :1;
    uint32_t lcbbcu1SpeedInvalidFlg         :1;
#if (__LFU_MODE_CTRL == ENABLE)
    uint32_t lcbbcu1RegenBrkEngageDelayFlg  :1;
#endif
}SetVehStsInfo_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define BBC_START_SEC_CONST_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Constant Section (UNSPECIFIED)**/
/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
#define BBC_STOP_SEC_CONST_UNSPECIFIED
#include "Bbc_MemMap.h"

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define BBC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define BBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define BBC_STOP_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define BBC_STOP_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (32BIT)**/
uint8_t u8IdbCtrlInhibitFlg;

#define BBC_STOP_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define BBC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
FAL_STATIC GetVehStsInfo_t  GetVehSts;
FAL_STATIC SetVehStsInfo_t  SetVehSts;

FAL_STATIC U32_BIT_STRUCT_t	BBC_DECIDE_VEHICLE_STATUS_1;

#define lcbbcu1IgnOffModeFlg              BBC_DECIDE_VEHICLE_STATUS_1.bit31
//#define lcbbcu8TimeAfterIgnOn             BBC_DECIDE_VEHICLE_STATUS_1.bit30
#define lcbbcu1ValidAftIgnOnFlg           BBC_DECIDE_VEHICLE_STATUS_1.bit29
uint8_t lcbbcu8TimeAfterIgnOn; /*TODO Temp KYB*/
#define BBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define BBC_STOP_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/




#define BBC_STOP_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (32BIT)**/


#define BBC_STOP_SEC_VAR_32BIT
#include "Bbc_MemMap.h"

/* KYB Temp */
extern Swt_SenEscSwtStInfo_t Swt_SenEscSwtStInfo;
extern uint8 gFlexSwState;/*NZ winter Emergency Stop Switch From EEM*/
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define BBC_START_SEC_CODE
#include "Bbc_MemMap.h"
static void LCBBC_vGetVehStsIf(void);
static void LCBBC_vChkSpdSigInvld(void);
static void LCBBC_vChkIgnOffMode(void);
static void LCBBC_vSetVehStsIf(void);
static void LCBBC_vDecideVehCtrlSts(void);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void LCBBC_vCallDecideVehSts(void)
{
	LCBBC_vGetVehStsIf();
	LCBBC_vChkSpdSigInvld();
	LCBBC_vChkIgnOffMode();
	LCBBC_vDecideVehCtrlSts();
	LCBBC_vSetVehStsIf();
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
/******************************************************************************
* FUNCTION NAME:      LCBBC_vGetVehStsIf
* CALLED BY:          LCBBC_vCallDecideVehSts()
* Preconditions:      none
* PARAMETER:          none
* RETURN VALUE:       none
* Description:		  Get interface bus variable
******************************************************************************/
static void LCBBC_vGetVehStsIf(void)
{
	GetVehSts.lcbbcu8cTcuGsSel = (uint8_t)Bbc_CtrlBus.Bbc_CtrlCanRxIdbInfo.GearSelDisp;
	GetVehSts.lcbbcu8wIgnStat = (uint8_t)Bbc_CtrlBus.Bbc_CtrlIgnOnOffSts;
	GetVehSts.lcbbcs16VehicleSpeed = (int16_t)Bbc_CtrlBus.Bbc_CtrlVehSpdFild;
	GetVehSts.lcbbcu1FLSpeedInvalid = Bbc_CtrlBus.Bbc_CtrlEemFailData.Eem_Fail_WssFL;
	GetVehSts.lcbbcu1FRSpeedInvalid = Bbc_CtrlBus.Bbc_CtrlEemFailData.Eem_Fail_WssFR;
	GetVehSts.lcbbcu1RLSpeedInvalid = Bbc_CtrlBus.Bbc_CtrlEemFailData.Eem_Fail_WssRL;
	GetVehSts.lcbbcu1RRSpeedInvalid = Bbc_CtrlBus.Bbc_CtrlEemFailData.Eem_Fail_WssRR;
	GetVehSts.lcbbcu1VehicleStopFlg = Bbc_CtrlBus.Bbc_CtrlVehStopStInfo.VehStopStFlg;
#if (__LFU_MODE_CTRL == ENABLE)
	GetVehSts.lcbbcu8cCF_Tcu_ShiftClass = 0; /*temporary, there is no RTE variable*/
	GetVehSts.lcbbcu1cTcu6RxOkFlg = 0; 		 /*temporary, there is no RTE variable*/
	GetVehSts.lcbbcu1fRBCSFaultDet = 0; 	 /*temporary, there is no RTE variable*/
#endif



}
/******************************************************************************
* FUNCTION NAME:      LCBBC_vSetVehStsIf
* CALLED BY:          LCBBC_vCallDecideVehSts()
* Preconditions:      none
* PARAMETER:          none
* RETURN VALUE:       none
* Description:		  Set interface bus variable
******************************************************************************/
static void LCBBC_vSetVehStsIf(void)
{
	Bbc_CtrlBus.Bbc_CtrlBaseBrkCtrlModInfo.VehStandStillStFlg = SetVehSts.lcbbcu1StandstillCtrl;
	Bbc_CtrlBus.Bbc_CtrlBaseBrkCtrlModInfo.LowSpdCtrlInhibitFlg = SetVehSts.lcbbcu1LowSpeedCtrlInhibit;
	Bbc_CtrlBus.Bbc_CtrlBaseBrkCtrlModInfo.StopdVehCtrlModFlg = SetVehSts.lcbbcu1CtrlStoppedVehicle;

	/*Bbc_CtrlBus.Bbc_CtrlBaseBrkCtrlModInfo.InhibitFlg  = ((Bbc_CtrlBus.Bbc_CtrlCanRxIdbInfo.GearSelDisp == 8) || (Swt_SenEscSwtStInfo.TcsDisabledBySwt == 1) || (gFlexSwState == 1));  v1.2.2 공식 배포시 삭제요망 */

	BBCsrcbus.SpeedInvalidFlg = SetVehSts.lcbbcu1SpeedInvalidFlg;
#if (__LFU_MODE_CTRL == ENABLE)
	BBCsrcbus.RegenBrkEngageDelayFlg = SetVehSts.lcbbcu1RegenBrkEngageDelayFlg;
#endif

	u8IdbCtrlInhibitFlg = 0;
#if M_INHIBIT_BY_MANUAL_GEAR == ENABLE
	u8IdbCtrlInhibitFlg |= (Bbc_CtrlBus.Bbc_CtrlCanRxIdbInfo.GearSelDisp == 8); /* M-Gear */
#endif
#if M_INHIBIT_BY_TCS_SWT == ENABLE
	u8IdbCtrlInhibitFlg |= (Swt_SenEscSwtStInfo.TcsDisabledBySwt == 1); /* TCS Disable Swt */
#endif
#if M_INHIBIT_BY_EMERGENCY_STOP_SWT == ENABLE
	u8IdbCtrlInhibitFlg |= (gFlexSwState == 1); /* Emergency Stop Swt */
#endif
}

/******************************************************************************
* FUNCTION NAME:      LCBBC_vChkSpdSigInvld
* CALLED BY:          LCBBC_vCallDecideVehSts()
* Preconditions:      none
* PARAMETER:		  none
* RETURN VALUE:       none
* Description:        Check Invalid Vehicle Speed Signal
******************************************************************************/
static void LCBBC_vChkSpdSigInvld(void)
{
	if( (GetVehSts.lcbbcu1FLSpeedInvalid==1)
	 && (GetVehSts.lcbbcu1FRSpeedInvalid==1)
	 && (GetVehSts.lcbbcu1RLSpeedInvalid==1)
	 && (GetVehSts.lcbbcu1RRSpeedInvalid==1) )
	{
		SetVehSts.lcbbcu1SpeedInvalidFlg = 1;
	}
	else
	{
		SetVehSts.lcbbcu1SpeedInvalidFlg = 0;
	}
}

/******************************************************************************
* FUNCTION NAME:      LCBBC_vChkIgnOffMode
* CALLED BY:          LCBBC_vCallDecideVehSts()
* Preconditions:      none
* PARAMETER:		  none
* RETURN VALUE:       none
* Description:        Set Ignition off control mode flag
******************************************************************************/
static void LCBBC_vChkIgnOffMode(void)
{
	static uint8_t lcbbcu8IgnOnTimer = 0;

    if(lcbbcu1IgnOffModeFlg==0)
    {
        if(GetVehSts.lcbbcu8wIgnStat==U8_IGN_OFF) /*ignition off*/
        {
        	/* need to defective flags about fail management */
            if(/*((liu1fWheelMsgFaultDet==0) || (liu1fSubWheelMsgFaultDet==0)) &&*/ (GetVehSts.lcbbcs16VehicleSpeed <= S16_SPEED_MIN))
            {
                lcbbcu1IgnOffModeFlg = 1;
            }
        }
        else
        {
            ;
        }
        lcbbcu8TimeAfterIgnOn = 0;
    }
    else
    {
        if(GetVehSts.lcbbcu8wIgnStat==U8_IGN_ON)
        {
            lcbbcu8TimeAfterIgnOn = lcbbcu8TimeAfterIgnOn + 1;

            if(lcbbcu8TimeAfterIgnOn > U8_STABILIZING_TIME_AFT_IGN_ON)
            {
                lcbbcu1IgnOffModeFlg = 0;
                lcbbcu8TimeAfterIgnOn = 0;
            }
        }
        else
        {
        	if(lcbbcu8TimeAfterIgnOn > 0)
        	{
            	lcbbcu8TimeAfterIgnOn = lcbbcu8TimeAfterIgnOn - 1;
            }
        }
    }

    if(GetVehSts.lcbbcu8wIgnStat==U8_IGN_OFF)
    {
    	lcbbcu8IgnOnTimer = U8_STABILIZING_TIME_AFT_IGN_ON;
    	lcbbcu1ValidAftIgnOnFlg = 0;
    }
    else
    {
    	if(lcbbcu8IgnOnTimer > 0)
    	{
    		lcbbcu8IgnOnTimer = lcbbcu8IgnOnTimer - 1;
    	}
    	else
    	{
    		lcbbcu1ValidAftIgnOnFlg = 1;
    	}
    }
}
/******************************************************************************
* FUNCTION NAME:      LCBBC_vDecideVehCtrlSts
* CALLED BY:          LCBBC_vCallDecideVehSts()
* Preconditions:      none
* PARAMETER:		  none
* RETURN VALUE:       none
* Description:		  Decide vehicle control state
******************************************************************************/
static void LCBBC_vDecideVehCtrlSts(void)
{
    if(lcbbcu1IgnOffModeFlg==1)
    {
        SetVehSts.lcbbcu1StandstillCtrl = 1;
    }
    else if((SetVehSts.lcbbcu1SpeedInvalidFlg==0) && (GetVehSts.lcbbcu1VehicleStopFlg==1))
    {
		SetVehSts.lcbbcu1StandstillCtrl = 1;
    }
    else
    {
        SetVehSts.lcbbcu1StandstillCtrl = 0;
    }

    /* need to defective flags about fail management */
	if((/*(liu1fEngineMsgFaultDet==0) && (liu1fTcu1MsgTimeOutError==0) &&*/ (GetVehSts.lcbbcu8cTcuGsSel==0)) || (lcbbcu1IgnOffModeFlg==1)) /* P Gear or IGN off */
    {
		SetVehSts.lcbbcu1CtrlStoppedVehicle = 1;
	}
	else
	{
		SetVehSts.lcbbcu1CtrlStoppedVehicle = 0;
	}

    if((SetVehSts.lcbbcu1SpeedInvalidFlg==1) && (lcbbcu1IgnOffModeFlg==0))
    {
        SetVehSts.lcbbcu1LowSpeedCtrlInhibit = 1;  /* speed info NOT reliable --> always high speed control */
    }
    else
    {
        SetVehSts.lcbbcu1LowSpeedCtrlInhibit = 0;  /* ignition off OR speed info reliable: enable low speed control */
    }

#if (__LFU_MODE_CTRL == ENABLE)
    if(GetVehSts.lcbbcu1fRBCSFaultDet==0)
    {
    	if((GetVehSts.lcbbcu1cTcu6RxOkFlg==1) && (GetVehSts.lcbbcu8cCF_Tcu_ShiftClass==2))
    	{
    		SetVehSts.lcbbcu1RegenBrkEngageDelayFlg = 1;
    	}
    	else
    	{
    		SetVehSts.lcbbcu1RegenBrkEngageDelayFlg = 0;
    	}
    }
    else
    {
    	SetVehSts.lcbbcu1RegenBrkEngageDelayFlg = 0;
    }
#endif
}


#define BBC_STOP_SEC_CODE
#include "Bbc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
