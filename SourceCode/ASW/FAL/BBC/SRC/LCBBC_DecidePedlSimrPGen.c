/**
 * @defgroup LCBBC_DecidePedlSimrPGen LCBBC_DecidePedlSimrPGen
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LCBBC_DecidePedlSimrPGen.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "LCBBC_DecidePedlSimrPGen.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/



/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
	int16_t lcbbcs16PedalTravelFilt;
    int16_t lcbbcs16PedalTravelRate;
    int16_t lcbbcs16PSpresFilt;

    uint32_t lcbbcu1Inhibit                 :1;
}GetDecidePedlSimrPGenInfo_t;

typedef struct
{
    int16_t lcbbcu1PedalSimPressGenOn;
}SetDecidePedlSimrPGenInfo_t;
/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define BBC_START_SEC_CONST_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Constant Section (UNSPECIFIED)**/
/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
#define BBC_STOP_SEC_CONST_UNSPECIFIED
#include "Bbc_MemMap.h"

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define BBC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define BBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define BBC_STOP_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define BBC_STOP_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (32BIT)**/


#define BBC_STOP_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define BBC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* RTE Input Variable */
FAL_STATIC uint8_t lcbbcu8PedalPressCnt;

FAL_STATIC GetDecidePedlSimrPGenInfo_t		GetPedlSimrPGen;
FAL_STATIC SetDecidePedlSimrPGenInfo_t		SetPedlSimrPGen;

FAL_STATIC U32_BIT_STRUCT_t	BBC_DECIDE_PEDL_SIMR_P_GEN_1;

extern uint8_t u8IdbCtrlInhibitFlg;

#define lcbbcu1PedalSimPForcedRelease             BBC_DECIDE_PEDL_SIMR_P_GEN_1.bit31

#define BBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define BBC_STOP_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/



#define BBC_STOP_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (32BIT)**/


#define BBC_STOP_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define BBC_START_SEC_CODE
#include "Bbc_MemMap.h"
static void LCBBC_vGetPedlSimrPGenIf(void);
static void LCBBC_vDecidePedlSimrPGen(void);
static void LCBBC_vSetPedlSimrPGenIf(void);
static int16_t LCBBC_vCheckGendSimnPPedlThres(void);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void LCBBC_vCallPedlSimrPGen(void)
{
    LCBBC_vGetPedlSimrPGenIf();
    LCBBC_vDecidePedlSimrPGen();
    LCBBC_vSetPedlSimrPGenIf();

}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
/******************************************************************************
* FUNCTION NAME:      LCBBC_vGetPedlSimrPGenIf
* CALLED BY:          LCBBC_vCallPedlSimrPGen()
* Preconditions:      none
* PARAMETER:          none
* RETURN VALUE:       none
* Description:		  Get interface bus variable
******************************************************************************/
static void LCBBC_vGetPedlSimrPGenIf(void)
{
   	GetPedlSimrPGen.lcbbcs16PedalTravelFilt = (int16_t)Bbc_CtrlBus.Bbc_CtrlPedlTrvlFinal;
 	GetPedlSimrPGen.lcbbcs16PedalTravelRate = (int16_t)Bbc_CtrlBus.Bbc_CtrlPedlTrvlRateInfo.PedlTrvlRate;
 	GetPedlSimrPGen.lcbbcs16PSpresFilt = (int16_t)Bbc_CtrlBus.Bbc_CtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar;
 	GetPedlSimrPGen.lcbbcu1Inhibit = u8IdbCtrlInhibitFlg;
}
    
/******************************************************************************
* FUNCTION NAME:      LCBBC_vSetPedlSimrPGenIf
* CALLED BY:          LCBBC_vCallPedlSimrPGen()
* Preconditions:      none
* PARAMETER:          none
* RETURN VALUE:       none
* Description:		  Set interface bus variable
******************************************************************************/
static void LCBBC_vSetPedlSimrPGenIf(void)
{
	Bbc_CtrlBus.Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo.PedlSimrPGendOnFlg = SetPedlSimrPGen.lcbbcu1PedalSimPressGenOn;
}

/******************************************************************************
* FUNCTION NAME:      LCBBC_vDecidePedlSimrPGen();
* CALLED BY:          LCBBC_vCallPedlSimrPGen()
* Preconditions:      none
* PARAMETER:		  none
* RETURN VALUE:       none
* Description:		  decide to generate simulate pressure
******************************************************************************/
static void LCBBC_vDecidePedlSimrPGen(void)
{
	static int16_t lcidbs16PedalThresCVon = 0;
    
	lcidbs16PedalThresCVon = LCBBC_vCheckGendSimnPPedlThres();

	if(SetPedlSimrPGen.lcbbcu1PedalSimPressGenOn==0)
	{
		if(GetPedlSimrPGen.lcbbcs16PedalTravelFilt>lcidbs16PedalThresCVon)
		{
			lcbbcu8PedalPressCnt = U8_CV_ON_TIME_MIN;
		}
		else
		{
			lcbbcu8PedalPressCnt = 0;
		}
	}
	else
	{
		if(GetPedlSimrPGen.lcbbcs16PedalTravelFilt>lcidbs16PedalThresCVon)
		{
			lcbbcu8PedalPressCnt = U8_CV_ON_TIME_MIN;
		}
		else if(lcbbcu8PedalPressCnt>0)
		{
			lcbbcu8PedalPressCnt = lcbbcu8PedalPressCnt - 1;
		}
		else
		{
			lcbbcu8PedalPressCnt = 0;
		}
	}

	if (lcbbcu1PedalSimPForcedRelease == 0)
	{
		if ((BBCsrcbus.lcbbcu1PedalAppIgnOnFlg == 1)
		    && (GetPedlSimrPGen.lcbbcs16PSpresFilt < S16_CV_OFF_PS_PRESSURE_PDTA_ON)
    		&& (GetPedlSimrPGen.lcbbcs16PedalTravelRate <= S16_TRAVEL_0_MM))
    	{
    		lcbbcu1PedalSimPForcedRelease = 1;
    	}
    	else
    	{
    		;
    	}
    }
    else
    {
    	if ((GetPedlSimrPGen.lcbbcs16PSpresFilt > S16_CV_ON_PS_PRESSURE_PDTA_ON)
    	|| (GetPedlSimrPGen.lcbbcs16PedalTravelRate >= S16_TRAVEL_0_5_MM))
    	{
    		lcbbcu1PedalSimPForcedRelease = 0;
    		BBCsrcbus.lcbbcu1PedalAppIgnOnFlg = 0;
    	}
    	else
    	{
    		;
    	}
}

	if((lcbbcu8PedalPressCnt>0) && (GetPedlSimrPGen.lcbbcu1Inhibit==0) && (lcbbcu1PedalSimPForcedRelease == 0))
    {
    	SetPedlSimrPGen.lcbbcu1PedalSimPressGenOn = 1;
    }
    else
    {
    	SetPedlSimrPGen.lcbbcu1PedalSimPressGenOn = 0;
    }
}

/******************************************************************************
* FUNCTION NAME:      LCBBC_vCheckGendSimnPPedlThres();
* CALLED BY:          LCBBC_vDecidePedlSimrPGen()
* Preconditions:      none
* PARAMETER:		  none
* RETURN VALUE:       none
* Description:		  check pedal threshold to generate simulate pressure
******************************************************************************/
static int16_t LCBBC_vCheckGendSimnPPedlThres(void)
{
	int16_t s16PedalThresOn;

	if(SetPedlSimrPGen.lcbbcu1PedalSimPressGenOn==0)
	{
		if(GetPedlSimrPGen.lcbbcs16PedalTravelRate>=0)
		{
			s16PedalThresOn = S16_CV_ON_PEDAL_TRAVEL;
		}
		else
		{
			s16PedalThresOn = S16_CV_ON_PEDAL_TRAVEL;
		}
	}
	else
	{
		if(GetPedlSimrPGen.lcbbcs16PedalTravelRate < S16_PEDAL_RELEASE_RATE_FAST)
		{
			s16PedalThresOn = S16_CV_OFF_PEDAL_TRAVEL;
		}
		else
{
			s16PedalThresOn = S16_CV_OFF_PEDAL_TRAVEL;
		}
	}
    
	return s16PedalThresOn;
}



#define BBC_STOP_SEC_CODE
#include "Bbc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
