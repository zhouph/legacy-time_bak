/**
 * @defgroup LCBBC_EstDrvrIntdTP LCBBC_EstDrvrIntdTP
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LCBBC_EstDrvrIntdTP.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "LCBBC_EstDrvrIntdTP.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define U8_TP_MAP_HIGH_SPD      1
#define U8_TP_MAP_MED_SPD       2
#define U8_TP_MAP_LOW_SPD       3
#define U8_TP_MAP_LFU           4
#define U8_TP_MAP_FLEX_1        5
#define U8_TP_MAP_FLEX_2        6
#define U8_TP_MAP_EBD_FAIL      7


/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
	int16_t lcbbcs16PedalTravelFilt;
    int16_t lcbbcs16VehicleSpeed;
#if (__FLEX_BRAKING_MODE == ENABLE)
    uint8_t lcbbcu8FlexBrakeSwitch;
#endif

#if (__LFU_MODE_CTRL == ENABLE)
    uint32_t lcbbcu1RegenBrkEngageDelay         :1;
#endif

#if (__EBD_FAIL_TARGET_P_LIMIT == ENABLE)
    uint32_t lcbbcu1AssistPLimitForEBDFail      :1;
#endif
    uint32_t lcbbcu1LowSpeedCtrlInhibit         :1;
    uint32_t lcbbcu1BBCOn                       :1;

}GetDrvrIntdTPInfo_t;

typedef struct
{
    int16_t lcbbcs16TargetPressurebypedal;
}SetDrvrIntdTPInfo_t;
/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define BBC_START_SEC_CONST_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Constant Section (UNSPECIFIED)**/
/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
#define BBC_STOP_SEC_CONST_UNSPECIFIED
#include "Bbc_MemMap.h"

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define BBC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define BBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define BBC_STOP_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define BBC_STOP_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (32BIT)**/


#define BBC_STOP_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define BBC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
FAL_STATIC GetDrvrIntdTPInfo_t			        GetDrvrIntdTP;
FAL_STATIC SetDrvrIntdTPInfo_t			        SetDrvrIntdTP;

#if (__FLEX_BRAKING_MODE == ENABLE)
FAL_STATIC uint8_t lcbbcu8FlexBrakeMode;
#endif

FAL_STATIC uint8_t lcbbcDriverTPMapSelection;

FAL_STATIC U32_BIT_STRUCT_t	BBC_EST_DRIVER_INTENDED_TP_1;

#define lcbbcu1SetMedSpeedTargetFlg     BBC_EST_DRIVER_INTENDED_TP_1.bit29
#define lcbbcu1SetHighSpeedTargetFlg    BBC_EST_DRIVER_INTENDED_TP_1.bit28

#if (__LFU_MODE_CTRL == ENABLE)
#define lcbbcu1CompRegenBrkEngageDelay  BBC_EST_DRIVER_INTENDED_TP_1.bit31
#endif

#if (__EBD_FAIL_TARGET_P_LIMIT == ENABLE)
#define lcbbcu1SetEBDFailTargetFlg      BBC_EST_DRIVER_INTENDED_TP_1.bit30
#endif

#define BBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define BBC_STOP_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/




#define BBC_STOP_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (32BIT)**/


#define BBC_STOP_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define BBC_START_SEC_CODE
#include "Bbc_MemMap.h"
static void LCBBC_vGetDrvrTarPIf(void);
static void LCBBC_vSetDrvrTarPIf(void);
static void LCBBC_vEstimDrvrReqFromPdtSig(void);
static void LCBBC_vDctTarPSeln(void);

#if (__FLEX_BRAKING_MODE == ENABLE)
static void LCBBC_vSelTarPByFlexBrk(void);
#endif

#if (__LFU_MODE_CTRL == ENABLE)
static void LCBBC_vSelTarPByLFU(void);
#endif

#if (__EBD_FAIL_TARGET_P_LIMIT == ENABLE)
static void LCBBC_vSelTarPByEBDFail(void);
#endif

static void LCBBC_vSelTarPBySpd(void);
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void LCBBC_vCallDrvrIntdTarP(void)
{
    LCBBC_vGetDrvrTarPIf();
    LCBBC_vDctTarPSeln();
    LCBBC_vEstimDrvrReqFromPdtSig();
    LCBBC_vSetDrvrTarPIf();
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
/******************************************************************************
* FUNCTION NAME:      LCBBC_vGetDrvrTarPIf
* CALLED BY:          LCBBC_vCallDrvrIntdTarP()
* Preconditions:      none
* PARAMETER:          none
* RETURN VALUE:       none
* Description:		  Get interface bus variable
******************************************************************************/
static void LCBBC_vGetDrvrTarPIf(void)
{
	GetDrvrIntdTP.lcbbcs16PedalTravelFilt = (int16_t)Bbc_CtrlBus.Bbc_CtrlPedlTrvlFinal;
	GetDrvrIntdTP.lcbbcs16VehicleSpeed = (int16_t)Bbc_CtrlBus.Bbc_CtrlVehSpdFild;
	GetDrvrIntdTP.lcbbcu1LowSpeedCtrlInhibit = Bbc_CtrlBus.Bbc_CtrlBaseBrkCtrlModInfo.LowSpdCtrlInhibitFlg;
	GetDrvrIntdTP.lcbbcu1BBCOn = Bbc_CtrlBus.Bbc_CtrlBaseBrkCtrlrActFlg;

	#if (__FLEX_BRAKING_MODE == ENABLE)
	GetDrvrIntdTP.lcbbcu8FlexBrakeSwitch = Bbc_CtrlBus.Bbc_CtrlFlexBrkSwtFild;
	#endif

	#if (__LFU_MODE_CTRL == ENABLE)
	GetDrvrIntdTP.lcbbcu1RegenBrkEngageDelay = BBCsrcbus.RegenBrkEngageDelayFlg;
	#endif

	#if (__EBD_FAIL_TARGET_P_LIMIT == ENABLE)
	GetDrvrIntdTP.lcbbcu1AssistPLimitForEBDFail = 0; /*temporary, there is no RTE variable*/
	#endif
}

/******************************************************************************
* FUNCTION NAME:      LCBBC_vSetDrvrTarPIf
* CALLED BY:          LCBBC_vCallDrvrIntdTarP()
* Preconditions:      none
* PARAMETER:          none
* RETURN VALUE:       none
* Description:		  Set interface bus variable
******************************************************************************/
static void LCBBC_vSetDrvrTarPIf(void)
{
	Bbc_CtrlBus.Bbc_CtrlTarPFromBrkPedl = SetDrvrIntdTP.lcbbcs16TargetPressurebypedal;
}

/******************************************************************************
* FUNCTION NAME:      LCBBC_vEstimDrvrReqFromPdtSig
* CALLED BY:          LCBBC_vCallDrvrIntdTarP()
* Preconditions:      none
* PARAMETER:          none
* RETURN VALUE:       none
* Description:		  Estimate driver requested target pressure from pedal signal
******************************************************************************/
static void LCBBC_vEstimDrvrReqFromPdtSig(void)
{
	static int16_t lcbbcs16DriverIntendedTPOld = 0;

	int16_t s16TPFilterGain = U8_LPF_7HZ_128G;
	int16_t *s16PedalTargetP = (int16_t *)S16_PEDAL_TARGET_P;

	lcbbcs16DriverIntendedTPOld = SetDrvrIntdTP.lcbbcs16TargetPressurebypedal;

	#if (__EBD_FAIL_TARGET_P_LIMIT == ENABLE)
	if(lcbbcDriverTPMapSelection == U8_TP_MAP_EBD_FAIL)
	{
		s16PedalTargetP[15] = (int16_t)S16_PEDAL_TARGET_P_EBD_FAIL;
	}
	else
	#endif

	#if (__FLEX_BRAKING_MODE == ENABLE)
	if(lcbbcDriverTPMapSelection == U8_TP_MAP_FLEX_1)
	{
		s16PedalTargetP[15] = (int16_t)S16_PEDAL_TARGET_P_FLEX1;
	}
	else if(lcbbcDriverTPMapSelection == U8_TP_MAP_FLEX_2)
	{
		s16PedalTargetP[15] = (int16_t)S16_PEDAL_TARGET_P_FLEX2;
	}
	else
	#endif /*  (_FLEX_BRAKING_MODE == ENABLE)  */

	#if (__LFU_MODE_CTRL == ENABLE)
	if(lcbbcDriverTPMapSelection == U8_TP_MAP_LFU)
	{
		s16PedalTargetP[15] = (int16_t)S16_PEDAL_TARGET_P_LFU;
	}
	else
	#endif

	/* Normal TP Map */
	if(lcbbcDriverTPMapSelection == U8_TP_MAP_HIGH_SPD)
	{
		s16PedalTargetP = (int16_t *)S16_PEDAL_TARGET_P;
	}
	/* Normal TP Map */

	else if(lcbbcDriverTPMapSelection == U8_TP_MAP_MED_SPD)
	{
		s16PedalTargetP = (int16_t *)S16_PEDAL_TARGET_P_MED_SPD;
	}
	else if(lcbbcDriverTPMapSelection == U8_TP_MAP_LOW_SPD)
	{
		s16PedalTargetP = (int16_t *)S16_PEDAL_TARGET_P_LOW_SPD;
	}
	/* Normal TP Map */
	else
	{
		s16PedalTargetP = (int16_t *)S16_PEDAL_TARGET_P;
	}
	/* Normal TP Map */

	if(GetDrvrIntdTP.lcbbcs16PedalTravelFilt<S16_PEDAL_TRAVEL(0))
	{
		SetDrvrIntdTP.lcbbcs16TargetPressurebypedal = (s16PedalTargetP[0] - S16_PCTRL_PRESS_0_BAR) * GetDrvrIntdTP.lcbbcs16PedalTravelFilt/(S16_PEDAL_TRAVEL(0));
	}
	else
	{
		SetDrvrIntdTP.lcbbcs16TargetPressurebypedal = L_s16IInter15Point(GetDrvrIntdTP.lcbbcs16PedalTravelFilt,
									S16_PEDAL_TRAVEL(0) , s16PedalTargetP[0],
									S16_PEDAL_TRAVEL(1) , s16PedalTargetP[1],
									S16_PEDAL_TRAVEL(2) , s16PedalTargetP[2],
									S16_PEDAL_TRAVEL(3) , s16PedalTargetP[3],
									S16_PEDAL_TRAVEL(4) , s16PedalTargetP[4],
									S16_PEDAL_TRAVEL(5) , s16PedalTargetP[5],
									S16_PEDAL_TRAVEL(6) , s16PedalTargetP[6],
									S16_PEDAL_TRAVEL(7) , s16PedalTargetP[7],
									S16_PEDAL_TRAVEL(8) , s16PedalTargetP[8],
									S16_PEDAL_TRAVEL(9) , s16PedalTargetP[9],
									S16_PEDAL_TRAVEL(10) , s16PedalTargetP[10],
									S16_PEDAL_TRAVEL(11) , s16PedalTargetP[11],
									S16_PEDAL_TRAVEL(12) , s16PedalTargetP[12],
									S16_PEDAL_TRAVEL(13) , s16PedalTargetP[13],
									S16_PEDAL_TRAVEL(14) , s16PedalTargetP[14]);
	}

	s16TPFilterGain = L_s16IInter2Point(GetDrvrIntdTP.lcbbcs16PedalTravelFilt,
														S16_TRAVEL_5_MM	 ,  U8_LPF_7HZ_128G,        /* 7Hz  */
														S16_TRAVEL_10_MM ,  U8_LPF_3HZ_128G );      /* 3Hz  */

	SetDrvrIntdTP.lcbbcs16TargetPressurebypedal = L_s16Lpf1Int(lcbbcs16DriverIntendedTPOld, SetDrvrIntdTP.lcbbcs16TargetPressurebypedal, (uint8_t)s16TPFilterGain);   /* 3Hz  */
}

/******************************************************************************
* FUNCTION NAME:      LCBBC_vCallDrvrIntdTarP
* CALLED BY:          LCBBC_vDctTarPSeln()
* Preconditions:      none
* PARAMETER:          none
* RETURN VALUE:       none
* Description:        Select Target Pressure
******************************************************************************/
static void LCBBC_vDctTarPSeln(void)
{
	if(GetDrvrIntdTP.lcbbcu1BBCOn==0)
	{
		  #if (__EBD_FAIL_TARGET_P_LIMIT == ENABLE)
		LCBBC_vSelTarPByEBDFail();
		  #endif

		  #if (__FLEX_BRAKING_MODE == ENABLE)
		LCBBC_vSelTarPByFlexBrk();
		  #endif

		  #if (__LFU_MODE_CTRL == ENABLE)
		LCBBC_vSelTarPByLFU();
		  #endif

		LCBBC_vSelTarPBySpd();
	}
	else
	{
		;
	}

	if(GetDrvrIntdTP.lcbbcu1BBCOn==0)
	{
		  #if (__EBD_FAIL_TARGET_P_LIMIT == ENABLE)
		if(lcbbcu1SetEBDFailTargetFlg==1)
		{
			lcbbcDriverTPMapSelection = U8_TP_MAP_EBD_FAIL;
		}
		else
     	  #endif /* __EBD_FAIL_TARGET_P_LIMIT == ENABLE*/

	  	  #if (__FLEX_BRAKING_MODE == ENABLE)
		if(lcbbcu8FlexBrakeMode == 1)
		{
			lcbbcDriverTPMapSelection = U8_TP_MAP_FLEX_1;
		}
		else if(lcbbcu8FlexBrakeMode == 2)
		{
			lcbbcDriverTPMapSelection = U8_TP_MAP_FLEX_2;
		}
		else
	  	  #endif /* _FLEX_BRAKING_MODE == ENABLE */

      	  #if (__LFU_MODE_CTRL == ENABLE)
		if(lcbbcu1CompRegenBrkEngageDelay==1)
		{
			lcbbcDriverTPMapSelection = U8_TP_MAP_LFU;
		}
		else
      	  #endif /* _LFU_MODE_CTRL == ENABLE */

		if(lcbbcu1SetHighSpeedTargetFlg == 1)
		{
			lcbbcDriverTPMapSelection = U8_TP_MAP_HIGH_SPD;
		}
		else if(lcbbcu1SetMedSpeedTargetFlg==1)
		{
			lcbbcDriverTPMapSelection = U8_TP_MAP_MED_SPD;
		}
		else
		{
			lcbbcDriverTPMapSelection = U8_TP_MAP_LOW_SPD;
		}
	}
	else
	{
		;
	}
}

/******************************************************************************
* FUNCTION NAME:      LCBBC_vSelTarPBySpd
* CALLED BY:          LCBBC_vDctTarPSeln()
* Preconditions:      none
* PARAMETER:          none
* RETURN VALUE:       none
* Description:        Check to select target Pressure by vehicle speed
******************************************************************************/
static void LCBBC_vSelTarPBySpd(void)
{
	if((GetDrvrIntdTP.lcbbcs16VehicleSpeed > S16_TARGET_PRESS_SET_SPD_HIGH) || (GetDrvrIntdTP.lcbbcu1LowSpeedCtrlInhibit == 1))
	{
		lcbbcu1SetHighSpeedTargetFlg = 1;
		lcbbcu1SetMedSpeedTargetFlg = 0;
	}
	else if(GetDrvrIntdTP.lcbbcs16VehicleSpeed > S16_TARGET_PRESS_SET_SPD_LOW)
	{
		lcbbcu1SetHighSpeedTargetFlg = 0;
		lcbbcu1SetMedSpeedTargetFlg = 1;
	}
	else
	{
		lcbbcu1SetHighSpeedTargetFlg = 0;
		lcbbcu1SetMedSpeedTargetFlg = 0;
	}
}
/******************************************************************************
* FUNCTION NAME:      LCBBC_vSelTarPByEBDFail
* CALLED BY:          LCBBC_vDctTarPSeln()
* Preconditions:      none
* PARAMETER:          none
* RETURN VALUE:       none
* Description:        Check to select target Pressure by EBD fail
******************************************************************************/
#if (__EBD_FAIL_TARGET_P_LIMIT == ENABLE)
static void LCBBC_vSelTarPByEBDFail(void)
{
	lcbbcu1SetEBDFailTargetFlg = GetDrvrIntdTP.lcbbcu1AssistPLimitForEBDFail;
}
#endif

/******************************************************************************
* FUNCTION NAME:      LCBBC_vSelTarPByFlexBrk
* CALLED BY:          LCBBC_vDctTarPSeln()
* Preconditions:      none
* PARAMETER:          none
* RETURN VALUE:       none
* Description:        Check to select target Pressure by flex brake switch
******************************************************************************/
#if (__FLEX_BRAKING_MODE == ENABLE)
static void LCBBC_vSelTarPByFlexBrk(void)
{
	if(GetDrvrIntdTP.lcbbcu1BBCOn == 0)
	{
		if(GetDrvrIntdTP.lcbbcu8FlexBrakeSwitch == 1)
		{
			lcbbcu8FlexBrakeMode = 1;
		}
		else if(GetDrvrIntdTP.lcbbcu8FlexBrakeSwitch == 2)
		{
			lcbbcu8FlexBrakeMode = 2;
		}
		else
		{
			lcbbcu8FlexBrakeMode = 0;
		}
	}
	else
	{
		;
	}
}
#endif /*#if (__FLEX_BRAKING_MODE == ENABLE)*/

/******************************************************************************
* FUNCTION NAME:      LCBBC_vSelTarPByLFU
* CALLED BY:          LCBBC_vDctTarPSeln()
* Preconditions:      none
* PARAMETER:          none
* RETURN VALUE:       none
* Description:        Check to select target Pressure by LFU
******************************************************************************/
  #if (__LFU_MODE_CTRL == ENABLE)
static void LCBBC_vSelTarPByLFU(void)
{
	lcbbcu1CompRegenBrkEngageDelay = GetDrvrIntdTP.lcbbcu1RegenBrkEngageDelay;
}
  #endif

#define BBC_STOP_SEC_CODE
#include "Bbc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
