/**
 * @defgroup LCBBC_LimitBrkTP LCBBC_LimitBrkTP
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LCBBC_LimitBrkTP.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "LCBBC_LimBrkTP.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
	int16_t lcbbcs16TargetPressurebypedal;
    int16_t lcbbcs16VehicleSpeed;

    uint32_t lcbbcu1LowSpeedCtrlInhibit         :1;
    uint32_t lcbbcu1AssistPressLimit            :1;
    uint32_t lcbbcu1StandstillCtrl              :1;
    uint32_t lcbbcu1SpeedInvalidFlg             :1;
}GetLimBrkTPInfo_t;

typedef struct
{
    int16_t lcbbcs16TargetPressurebyBaseBrk;
}SetLimBrkTPInfo_t;
/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define BBC_START_SEC_CONST_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
#define BBC_STOP_SEC_CONST_UNSPECIFIED
#include "Bbc_MemMap.h"
#define U8_P_LEVEL_2	0
#define U8_P_LEVEL_3	1
#define U8_P_LEVEL_4	2
#define U8_P_LEVEL_5	3

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define BBC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define BBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define BBC_STOP_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define BBC_STOP_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (32BIT)**/


#define BBC_STOP_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define BBC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
FAL_STATIC GetLimBrkTPInfo_t			        GetLimBrkTP = {0,0,0,0,0,0};
FAL_STATIC SetLimBrkTPInfo_t			        SetLimBrkTP = {0};

FAL_STATIC int16_t lcbbcs16TargetPressAtLimitStart = 0;
FAL_STATIC int16_t lcbbcs16TPDeltaP1 = 0;
FAL_STATIC int16_t lcbbcs16TPDeltaP2 = 0;
FAL_STATIC int16_t lcbbcs16TPDeltaP3 = 0;
FAL_STATIC int16_t lcbbcs16TPDeltaP4 = 0;
FAL_STATIC int16_t lcbbcs16TPDeltaP5 = 0;
FAL_STATIC int16_t lcbbcs16TPLimitP1 = 0;
FAL_STATIC int16_t lcbbcs16TPLimitP2 = 0;
FAL_STATIC int16_t lcbbcs16TPLimitP3 = 0;
FAL_STATIC int16_t lcbbcs16TPLimitP4 = 0;
FAL_STATIC int16_t lcbbcs16TPLimitP5 = 0;

FAL_STATIC U32_BIT_STRUCT_t	LIMIT_BRK_TP_1;
#define lcbbcu1TargetPLimitRequestFlg               LIMIT_BRK_TP_1.bit31
#define lcbbcu1TargetPLimitFlg                      LIMIT_BRK_TP_1.bit30

#define BBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define BBC_STOP_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define BBC_STOP_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (32BIT)**/


#define BBC_STOP_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define BBC_START_SEC_CODE
#include "Bbc_MemMap.h"
static void LCBBC_vGetBrkTarPLimnIf(void);
static void LCBBC_vLoadLimdTarPMap(void);
static int16_t LCBBC_vLimiTarP(int16_t s16TargetPInput);
static void LCBBC_vSetBrkTarPLimnIf(void);
static uint16_t LCBBC_u16DtmLimitTPCondition(uint8_t u8AssistPressLimit, uint8_t u8StandstillCtrl, int16_t s16VehicleSpeed, uint8_t u8SpeedInvalidFlg);
static int16_t LCBBC_s16DecideTargetPAtLimitStart(uint16_t u16LimitTPCondition, uint8_t u8TargetPLimitRequestFlg, uint8_t u8TargetPLimitFlg, int16_t s16TargetPInput, int16_t s16TargetPressAtLimitStart);
static int16_t LCBBC_s16DtmTPLimitRefSpd(uint8_t u8AssistPressLimit, uint8_t u8StandstillCtrl, uint8_t u8LowSpeedCtrlInhibit, int16_t s16VehicleSpeed);
static int16_t LCBBC_s16CalcTPLimitPLevel(int16_t s16TPLimitRefSpeed, int16_t s16TPLimitPOld, int16_t s16TPDeltaP, int16_t s16TPLimitDec, uint8_t u8PLevelIndex);
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void LCBBC_vCallBrkTarPLimn(void)
{
    LCBBC_vGetBrkTarPLimnIf();
    SetLimBrkTP.lcbbcs16TargetPressurebyBaseBrk = LCBBC_vLimiTarP(GetLimBrkTP.lcbbcs16TargetPressurebypedal);
    LCBBC_vSetBrkTarPLimnIf();
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
/******************************************************************************
* FUNCTION NAME:      LCBBC_vGetBrkTarPLimnIf
* CALLED BY:          LCBBC_vCallBrkTarPLimn()
* Preconditions:      none
* PARAMETER:          none
* RETURN VALUE:       none
* Description:		  Get interface bus variable
******************************************************************************/
static void LCBBC_vGetBrkTarPLimnIf(void)
{
	GetLimBrkTP.lcbbcs16TargetPressurebypedal = (int16_t)Bbc_CtrlBus.Bbc_CtrlTarPFromBrkPedl;
	GetLimBrkTP.lcbbcs16VehicleSpeed = (int16_t)Bbc_CtrlBus.Bbc_CtrlVehSpdFild;
	GetLimBrkTP.lcbbcu1StandstillCtrl = Bbc_CtrlBus.Bbc_CtrlBaseBrkCtrlModInfo.VehStandStillStFlg;
	GetLimBrkTP.lcbbcu1LowSpeedCtrlInhibit = Bbc_CtrlBus.Bbc_CtrlBaseBrkCtrlModInfo.LowSpdCtrlInhibitFlg;
	GetLimBrkTP.lcbbcu1SpeedInvalidFlg = BBCsrcbus.SpeedInvalidFlg;
	GetLimBrkTP.lcbbcu1AssistPressLimit = 0;  /*temporary, there is no RTE variable*/
}

/******************************************************************************
* FUNCTION NAME:      LCBBC_vSetBrkTarPLimnIf
* CALLED BY:          LCBBC_vCallBrkTarPLimn()
* Preconditions:      none
* PARAMETER:          none
* RETURN VALUE:       none
* Description:		  Set interface bus variable
******************************************************************************/
static void LCBBC_vSetBrkTarPLimnIf(void)
{
	Bbc_CtrlBus.Bbc_CtrlTarPFromBaseBrkCtrlr = SetLimBrkTP.lcbbcs16TargetPressurebyBaseBrk;
}

/******************************************************************************
* FUNCTION NAME:      LCBBC_vLimiTarP
* CALLED BY:          LCBBC_vCallBrkTarPLimn()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       lcbbcs16LimitedTP
* Description:
******************************************************************************/
static int16_t LCBBC_vLimiTarP(int16_t s16TargetPInput)
{

	int16_t lcbbcs16TargetPressAddition = 0;
	int16_t lcbbcs16TargetPressResidual = 0;
	static int16_t lcbbcs16LimitedTP = 0;
	static int16_t lcbbcs16LimitedTPOld = 0;
	static int16_t lcbbcs16TargetPInputOld = 0;

	int16_t  s16LimitedTPDiffMax, s16LimitedTPMax;
	int16_t  s16TargetPInputDiff;

	uint16_t u16LimitTPCondition = 0;

	u16LimitTPCondition = LCBBC_u16DtmLimitTPCondition(GetLimBrkTP.lcbbcu1AssistPressLimit, GetLimBrkTP.lcbbcu1StandstillCtrl, GetLimBrkTP.lcbbcs16VehicleSpeed, GetLimBrkTP.lcbbcu1SpeedInvalidFlg);

	lcbbcs16TargetPressAtLimitStart = LCBBC_s16DecideTargetPAtLimitStart(u16LimitTPCondition, lcbbcu1TargetPLimitRequestFlg, lcbbcu1TargetPLimitFlg, s16TargetPInput, lcbbcs16TargetPressAtLimitStart);

	lcbbcu1TargetPLimitRequestFlg = u16LimitTPCondition;

	LCBBC_vLoadLimdTarPMap();

	if ((lcbbcu1TargetPLimitRequestFlg == 1) || (lcbbcu1TargetPLimitFlg==1))
	{

		if (s16TargetPInput > lcbbcs16TargetPressAtLimitStart)
		{
			lcbbcs16TargetPressResidual = s16TargetPInput - lcbbcs16TargetPressAtLimitStart;

			lcbbcs16TargetPressAddition = L_s16IInter5Point(lcbbcs16TargetPressResidual,
					  									    lcbbcs16TPDeltaP1 , lcbbcs16TPLimitP1 ,
					  									    lcbbcs16TPDeltaP2 , lcbbcs16TPLimitP2 ,
					  									    lcbbcs16TPDeltaP3 , lcbbcs16TPLimitP3 ,
					  									    lcbbcs16TPDeltaP4 , lcbbcs16TPLimitP4 ,
														    lcbbcs16TPDeltaP5 , lcbbcs16TPLimitP5 );

			lcbbcs16LimitedTP = lcbbcs16TargetPressAtLimitStart + lcbbcs16TargetPressAddition;

		/**********************************************************/
		/* Limit Max value of Target Press for "DDUNG" noise      */
		/**********************************************************/

		  #if (__BBC_LOW_SPEED_TARGET_P_LIMIT==ENABLE)
			if(lcbbcu1TargetPLimitRequestFlg==1)
			{
		    	s16LimitedTPMax = L_s16IInter2Point(GetLimBrkTP.lcbbcs16VehicleSpeed,
	              					        S16_STOP_SPEED_OFFSET_TP_LIMIT, S16_ONSTOP_LIMITED_TARGET_P_MAX,
		                                    S16_TARGET_LIMIT_ALLOW_LSPD, S16_PEDAL_TARGET_P_MAX);
			    s16LimitedTPMax = (s16LimitedTPMax >= lcbbcs16LimitedTPOld) ? s16LimitedTPMax : lcbbcs16LimitedTPOld;

				lcbbcs16LimitedTP = L_s16LimitMinMax(lcbbcs16LimitedTP, S16_PCTRL_PRESS_0_BAR, s16LimitedTPMax);
			}
			else {}
		  #endif

		}
		else
		{
			lcbbcs16LimitedTP = s16TargetPInput;
		}

	  #if (__BBC_LOW_SPEED_TARGET_P_LIMIT==ENABLE)
		if((lcbbcu1TargetPLimitRequestFlg == 1) && (lcbbcs16LimitedTP>=S16_TP_DIFF_LIMIT_THRES_PRESS)&&(GetLimBrkTP.lcbbcs16VehicleSpeed <= S16_STOP_SPEED_OFFSET_TP_LIMIT))
		{
			if(lcbbcs16LimitedTP>=(lcbbcs16LimitedTPOld + S16_LIMITED_TP_DIFF_AT_HP))
			{
				lcbbcs16LimitedTP = lcbbcs16LimitedTPOld + S16_LIMITED_TP_DIFF_AT_HP;
			}
			else {}
		}
		else {}

/* need to review arbitrator*/
/*
  		if((ldahbu1EscActiveFlg == 1) || (ldahbu1TcsActiveFlg == 1))
		{
			s16LimitedTPDiffMax = S16_MPRESS_50_BAR;
		}
		else if(ldahbu1AbsActiveFlg == 1)
		{
			s16LimitedTPDiffMax = S16_MPRESS_2_BAR;
		}
		else
*/
	  #endif
		{
			s16LimitedTPDiffMax = L_s16IInter4Point(lcbbcs16LimitedTP, S16_MPRESS_10_BAR, S16_PCTRL_PRESS_1_BAR,
																S16_PCTRL_PRESS_30_BAR, S16_PCTRL_PRESS_0_5_BAR,
																S16_PCTRL_PRESS_50_BAR, S16_PCTRL_PRESS_0_3_BAR,
																S16_PCTRL_PRESS_70_BAR, S16_PCTRL_PRESS_0_2_BAR);
		}

		s16TargetPInputDiff = L_s16Abs(s16TargetPInput - lcbbcs16TargetPInputOld);

		if((s16TargetPInput>=lcbbcs16TargetPInputOld) && (lcbbcs16LimitedTP>=lcbbcs16LimitedTPOld))
		{
			s16LimitedTPDiffMax = (s16TargetPInputDiff>s16LimitedTPDiffMax)? s16TargetPInputDiff : s16LimitedTPDiffMax;
		}
		else if((s16TargetPInput<lcbbcs16TargetPInputOld) && (lcbbcs16LimitedTP<lcbbcs16LimitedTPOld))
		{
			s16LimitedTPDiffMax = (s16TargetPInputDiff>s16LimitedTPDiffMax)? s16TargetPInputDiff : s16LimitedTPDiffMax;
		}
		else
		{
			s16LimitedTPDiffMax = 0;
		}

		lcbbcs16LimitedTP = L_s16LimitDiff(lcbbcs16LimitedTP, lcbbcs16LimitedTPOld, s16LimitedTPDiffMax, s16LimitedTPDiffMax);

		if(lcbbcs16LimitedTP<s16TargetPInput)
		{
			lcbbcu1TargetPLimitFlg = 1;
		}
		else
		{
			lcbbcu1TargetPLimitFlg = 0;
		}
	}
	else
	{
		lcbbcs16TargetPressAtLimitStart = S16_PCTRL_PRESS_0_BAR;
		lcbbcu1TargetPLimitFlg = 0;

		lcbbcs16LimitedTP = s16TargetPInput;
	}

	lcbbcs16LimitedTPOld = lcbbcs16LimitedTP;
	lcbbcs16TargetPInputOld = s16TargetPInput;

	return lcbbcs16LimitedTP;
}

static uint16_t LCBBC_u16DtmLimitTPCondition(uint8_t u8AssistPressLimit, uint8_t u8StandstillCtrl, int16_t s16VehicleSpeed, uint8_t u8SpeedInvalidFlg)
{
	uint16_t u16TPLimitCondiSatisfied = 0;
	if( (u8AssistPressLimit==1)
	  #if __BBC_ONSTOP_TARGET_P_LIMIT
	 || (u8StandstillCtrl == 1)
	  #endif /*__BBC_ONSTOP_TARGET_P_LIMIT*/
      #if (__BBC_LOW_SPEED_TARGET_P_LIMIT==ENABLE)
	  /*need to review with arbitrator*/
	  /*||(((GetLimBrkTP.lcbbcs16VehicleSpeed <= S16_TARGET_LIMIT_ALLOW_LSPD)&&(GetLimBrkTP.lcbbcu1SpeedInvalidFlg==0)) && (ldahbu1AbsActiveFlg == 0) && (ldahbu1EscActiveFlg == 0) && (ldahbu1TcsActiveFlg == 0))*/
	  ||(((s16VehicleSpeed <= S16_TARGET_LIMIT_ALLOW_LSPD)&&(u8SpeedInvalidFlg==0)))
      #endif
	  )
	{
		u16TPLimitCondiSatisfied = 1;
	}

	return u16TPLimitCondiSatisfied;
}

static int16_t LCBBC_s16DecideTargetPAtLimitStart(uint16_t u16LimitTPCondition, uint8_t u8TargetPLimitRequestFlg, uint8_t u8TargetPLimitFlg, int16_t s16TargetPInput, int16_t s16TargetPressAtLimitStart)
{
	if(u16LimitTPCondition==TRUE)
	{
		if((u8TargetPLimitRequestFlg==0) && (u8TargetPLimitFlg==0))
		{
			if(s16TargetPInput<S16_MAX_LINEAR_TARGET_P_AT_0KPH)
			{
				s16TargetPressAtLimitStart = S16_MAX_LINEAR_TARGET_P_AT_0KPH;
			}
			else
			{
				s16TargetPressAtLimitStart = s16TargetPInput;
			}
		}
		else
		{
			if(s16TargetPInput<S16_MAX_LINEAR_TARGET_P_AT_0KPH)
			{
				s16TargetPressAtLimitStart = S16_MAX_LINEAR_TARGET_P_AT_0KPH;
			}
			else
			{
				;
			}
		}
	}
	else if(u8TargetPLimitFlg==1)
	{
		if(s16TargetPInput<S16_MAX_LINEAR_TARGET_P_AT_0KPH)
		{
			s16TargetPressAtLimitStart = S16_MAX_LINEAR_TARGET_P_AT_0KPH;
		}
	}
	else
	{
		s16TargetPressAtLimitStart = S16_PCTRL_PRESS_0_BAR;
	}

	return s16TargetPressAtLimitStart;
}
/******************************************************************************
* FUNCTION NAME:      LCBBC_vLoadLimdTarPMap
* CALLED BY:          LCBBC_vLimiTarP()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       none
* Description:
******************************************************************************/
static void LCBBC_vLoadLimdTarPMap(void)
{
	int16_t s16TPLimitRefSpeed = 0;
	int16_t s16LimitDeltaP = 0;
	int16_t s16TPLimitP = 0;
	int16_t s16TPLimitDec = 0;
	int16_t s16TPLimitPat0kph = 0;
	int16_t s16TPLimitPat1kph = 0;

	s16TPLimitRefSpeed = LCBBC_s16DtmTPLimitRefSpd(GetLimBrkTP.lcbbcu1AssistPressLimit, GetLimBrkTP.lcbbcu1StandstillCtrl, GetLimBrkTP.lcbbcu1LowSpeedCtrlInhibit, GetLimBrkTP.lcbbcs16VehicleSpeed);

	if ((lcbbcu1TargetPLimitRequestFlg == 1) || (lcbbcu1TargetPLimitFlg==1))
	{
		if(lcbbcs16TargetPressAtLimitStart < S16_MAX_LINEAR_TARGET_P_AT_0KPH)
		{
			s16LimitDeltaP = S16_PEDAL_TARGET_P_MAX - S16_MAX_LINEAR_TARGET_P_AT_0KPH;
		}
		else
		{
			s16LimitDeltaP = S16_PEDAL_TARGET_P_MAX - lcbbcs16TargetPressAtLimitStart;
		}

		lcbbcs16TPDeltaP1 = S16_MPRESS_0_BAR;
		lcbbcs16TPDeltaP2 = L_s16LimitMinMax((s16LimitDeltaP/10), S16_LIMITED_TARGET_P_AT_DELTAP2, S16_PEDAL_TARGET_P_MAX); /*S16_MPRESS_10_BAR */
		lcbbcs16TPDeltaP3 = L_s16LimitMinMax((s16LimitDeltaP/3),  S16_LIMITED_TARGET_P_AT_DELTAP3, S16_PEDAL_TARGET_P_MAX); /*S16_MPRESS_30_BAR */
		lcbbcs16TPDeltaP4 = L_s16LimitMinMax((s16LimitDeltaP/2),  S16_LIMITED_TARGET_P_AT_DELTAP4, S16_PEDAL_TARGET_P_MAX); /*S16_MPRESS_50_BAR */
		lcbbcs16TPDeltaP5 = L_s16LimitMinMax((s16LimitDeltaP),    S16_LIMITED_TARGET_P_AT_DELTAP5, S16_PEDAL_TARGET_P_MAX); /*S16_MPRESS_100_BAR*/

		if (lcbbcu1TargetPLimitFlg==1)
		{
			s16TPLimitDec = S16_PCTRL_PRESS_0_1_BAR;
		}
		else
		{
			s16TPLimitDec = S16_PCTRL_PRESS_10_BAR;
		}

		lcbbcs16TPLimitP1 = S16_PCTRL_PRESS_0_BAR;

		lcbbcs16TPLimitP2 = LCBBC_s16CalcTPLimitPLevel(s16TPLimitRefSpeed, lcbbcs16TPLimitP2, lcbbcs16TPDeltaP2, s16TPLimitDec, U8_P_LEVEL_2);
		lcbbcs16TPLimitP3 = LCBBC_s16CalcTPLimitPLevel(s16TPLimitRefSpeed, lcbbcs16TPLimitP3, lcbbcs16TPDeltaP3, s16TPLimitDec, U8_P_LEVEL_3);
		lcbbcs16TPLimitP4 = LCBBC_s16CalcTPLimitPLevel(s16TPLimitRefSpeed, lcbbcs16TPLimitP4, lcbbcs16TPDeltaP4, s16TPLimitDec, U8_P_LEVEL_4);
		lcbbcs16TPLimitP5 = LCBBC_s16CalcTPLimitPLevel(s16TPLimitRefSpeed, lcbbcs16TPLimitP5, lcbbcs16TPDeltaP5, s16TPLimitDec, U8_P_LEVEL_5);
	}
	else
	{
		lcbbcs16TPDeltaP1=0;
		lcbbcs16TPDeltaP2=0;
		lcbbcs16TPDeltaP3=0;
		lcbbcs16TPDeltaP4=0;
		lcbbcs16TPDeltaP5=0;
		lcbbcs16TPLimitP1=0;
		lcbbcs16TPLimitP2=0;
		lcbbcs16TPLimitP3=0;
		lcbbcs16TPLimitP4=0;
		lcbbcs16TPLimitP5=0;
	}
}
/******************************************************************************
* FUNCTION NAME:      LCBBC_s16DtmTPLimitRefSpd
* CALLED BY:          LCBBC_vLoadLimdTarPMap()
* Preconditions:      none
* PARAMETER:          u8AssistPressLimit, u8StandstillCtrl, u8LowSpeedCtrlInhibit, s16VehicleSpeed
* RETURN VALUE:       s16TPLimitRefSpeed
* Description:
******************************************************************************/
static int16_t LCBBC_s16DtmTPLimitRefSpd(uint8_t u8AssistPressLimit, uint8_t u8StandstillCtrl, uint8_t u8LowSpeedCtrlInhibit, int16_t s16VehicleSpeed)
{
	int16_t s16TPLimitRefSpeed = 0;

	if (u8AssistPressLimit == 1)
	{
		s16TPLimitRefSpeed = S16_SPEED_MIN;
	}
#if __BBC_ONSTOP_TARGET_P_LIMIT
	else if (u8StandstillCtrl == 1)
	{
		s16TPLimitRefSpeed = S16_SPEED_MIN;
	}
#endif /*__BBC_ONSTOP_TARGET_P_LIMIT*/
	else if (u8LowSpeedCtrlInhibit == 1)
	{
		s16TPLimitRefSpeed = S16_SPEED_MAX;
	}
	else
	{
#if (__BBC_LOW_SPEED_TARGET_P_LIMIT==ENABLE)
		/* need to review with arbitrator */
		/*if((s16VehicleSpeed <= S16_TARGET_LIMIT_ALLOW_LSPD) && (ldahbu1AbsActiveFlg == 0) && (ldahbu1EscActiveFlg == 0) && (ldahbu1TcsActiveFlg == 0))*/
		if (s16VehicleSpeed <= S16_TARGET_LIMIT_ALLOW_LSPD)
		{
			s16TPLimitRefSpeed = L_s16IInter2Point(s16VehicleSpeed, S16_STOP_SPEED_OFFSET_TP_LIMIT, S16_SPEED_MIN,
																	S16_TARGET_LIMIT_ALLOW_LSPD, s16VehicleSpeed);
		}
		else
		{
			s16TPLimitRefSpeed = s16VehicleSpeed;
		}
#else /* (__BBC_LOW_SPEED_TARGET_P_LIMIT==ENABLE) */
		s16TPLimitRefSpeed = s16VehicleSpeed;
#endif /* (__BBC_LOW_SPEED_TARGET_P_LIMIT==ENABLE) */
	}

	return s16TPLimitRefSpeed;
}

/******************************************************************************
* FUNCTION NAME:      LCBBC_s16CalcTPLimitPLevel
* CALLED BY:          LCBBC_vLoadLimdTarPMap()
* Preconditions:      none
* PARAMETER:          s16TPLimitRefSpeed, s16TPLimitPOld, s16TPDeltaP, s16TPLimitDec, u8PLevelIndex
* RETURN VALUE:       s16TPLimitPOutput
* Description:
******************************************************************************/
static int16_t LCBBC_s16CalcTPLimitPLevel(int16_t s16TPLimitRefSpeed, int16_t s16TPLimitPOld, int16_t s16TPDeltaP, int16_t s16TPLimitDec, uint8_t u8PLevelIndex)
{
	static int16_t lcbbcs16TPLimitOkph[4] = {0, 0, 0, 0};
	static int16_t lcbbcs16TPLimit1kph[4] = {0, 0, 0, 0};
	int16_t s16TPLimitPat0kph = 0;
	int16_t s16TPLimitPat1kph = 0;
	int16_t s16TPLimitP = 0;
	int16_t s16TPLimitPOutput = 0;
	lcbbcs16TPLimitOkph[0] = S16_ONSTOP_TP_LIMIT_P2_AT_0KPH;
	lcbbcs16TPLimit1kph[0] = S16_ONSTOP_TP_LIMIT_P2_AT_1KPH;
	lcbbcs16TPLimitOkph[1] = S16_ONSTOP_TP_LIMIT_P3_AT_0KPH;
	lcbbcs16TPLimit1kph[1] = S16_ONSTOP_TP_LIMIT_P3_AT_1KPH;
	lcbbcs16TPLimitOkph[2] = S16_ONSTOP_TP_LIMIT_P4_AT_0KPH;
	lcbbcs16TPLimit1kph[2] = S16_ONSTOP_TP_LIMIT_P4_AT_1KPH;
	lcbbcs16TPLimitOkph[3] = S16_ONSTOP_TP_LIMIT_P5_AT_0KPH;
	lcbbcs16TPLimit1kph[3] = S16_ONSTOP_TP_LIMIT_P5_AT_1KPH;

	s16TPLimitPat0kph = (lcbbcs16TPLimitOkph[u8PLevelIndex] < s16TPDeltaP) ? lcbbcs16TPLimitOkph[u8PLevelIndex] : s16TPDeltaP;
	s16TPLimitPat1kph = (lcbbcs16TPLimit1kph[u8PLevelIndex] < s16TPDeltaP) ? lcbbcs16TPLimit1kph[u8PLevelIndex] : s16TPDeltaP;
	s16TPLimitP = L_s16IInter3Point(s16TPLimitRefSpeed, S16_SPEED_MIN, s16TPLimitPat0kph,
														S16_SPEED_1_KPH, s16TPLimitPat1kph,
														S16_SPEED_10_KPH, s16TPDeltaP);
	if (s16TPLimitPOld <= S16_PCTRL_PRESS_0_BAR)
	{
		s16TPLimitPOutput = s16TPLimitP;
	}
	else
	{
		s16TPLimitPOutput = L_s16LimitDiff(s16TPLimitP, s16TPLimitPOld, S16_PCTRL_PRESS_10_BAR, s16TPLimitDec);
	}

	return s16TPLimitPOutput;
}


#define BBC_STOP_SEC_CODE
#include "Bbc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
