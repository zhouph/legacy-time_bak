/**
 * @defgroup LCBBC_CalTargetPRate LCBBC_CalTargetPRate
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LCBBC_CalTargetPRate.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "LCBBC_CalTargetPRate.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/


/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
	int16_t s16CtrlTarPFromBaseBrkCtrlr;
}stBBCTarPRateInput_t;

typedef struct
{
	int16_t s16CtrlTarPRateFromBaseBrkCtrlr;
}stBBCTarPRateOutput_t;

typedef struct{
	int16_t s16TarPRateOutput;
	int16_t s16TarPRateOutputX10;
	int16_t s16TarPRateOutputBarPSec;
	int16_t s16TarPRateOutput10ms;
} stCalcTarPRateOutput_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define BBC_START_SEC_CONST_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/

#define BBC_STOP_SEC_CONST_UNSPECIFIED
#include "Bbc_MemMap.h"

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define BBC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define BBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define BBC_STOP_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define BBC_STOP_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (32BIT)**/


#define BBC_STOP_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define BBC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define BBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define BBC_STOP_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define BBC_STOP_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (32BIT)**/

#define BBC_STOP_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define BBC_START_SEC_CODE
#include "Bbc_MemMap.h"

static stBBCTarPRateInput_t LCBBC_stGetBBCTarPRateInputVar(void);
static void LCBBC_vSetBBCTarPRateOutputVar(stBBCTarPRateOutput_t stBBCTarPRateOutput);
static stBBCTarPRateOutput_t LCBBC_stCalcTargetPressRate(stBBCTarPRateInput_t stBBCTarPRateInput);
static int16_t LCBBC_s16CalcStdTargetPressRate(int16_t s16TargetPressInput);
static int16_t LCBBC_s16CalcTarPRate4BarPSec(int16_t s16TargetPressInput);
static int16_t LCBBC_s16CalcTarPRateDot4BarPSec(int16_t s16TargetPressInput);
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void LCBBC_vCalcBBCTarPRate(void)
{
	stBBCTarPRateInput_t      stBBCTarPRateInput = {0};
	stBBCTarPRateOutput_t     stBBCTarPRateOutput = {0};

	stBBCTarPRateInput = LCBBC_stGetBBCTarPRateInputVar();

	stBBCTarPRateOutput = LCBBC_stCalcTargetPressRate(stBBCTarPRateInput);

	LCBBC_vSetBBCTarPRateOutputVar(stBBCTarPRateOutput);
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static stBBCTarPRateInput_t LCBBC_stGetBBCTarPRateInputVar(void)
{
	stBBCTarPRateInput_t      stBBCTarPRateInput;
	stBBCTarPRateInput.s16CtrlTarPFromBaseBrkCtrlr = (int16_t)Bbc_CtrlBus.Bbc_CtrlTarPFromBaseBrkCtrlr;
	return stBBCTarPRateInput;
}

static void LCBBC_vSetBBCTarPRateOutputVar(stBBCTarPRateOutput_t stBBCTarPRateOutput)
{
	Bbc_CtrlBus.Bbc_CtrlTarPRateFromBaseBrkCtrlr = stBBCTarPRateOutput.s16CtrlTarPRateFromBaseBrkCtrlr;
}

static stBBCTarPRateOutput_t LCBBC_stCalcTargetPressRate(stBBCTarPRateInput_t stBBCTarPRateInput)
{
	int16_t s16TargetPressInput = stBBCTarPRateInput.s16CtrlTarPFromBaseBrkCtrlr;
	stCalcTarPRateOutput_t stCalcTarPRateOutput = {0,0,0,0};

	stBBCTarPRateOutput_t stBBCTarPRateOutput;


	/**********************************************************/
	/* Target Press rate (1 = 0.1bar/25ms = 4bar/s)           */
	/**********************************************************/
	stCalcTarPRateOutput.s16TarPRateOutput = LCBBC_s16CalcTarPRate4BarPSec(s16TargetPressInput);
	/**********************************************************/
	/* Target Press rate (1 = 0.01bar/25ms = 0.4bar/s)        */
	/**********************************************************/
	stCalcTarPRateOutput.s16TarPRateOutputX10 = LCBBC_s16CalcTarPRateDot4BarPSec(s16TargetPressInput);

	/**********************************************************/
	/* Target Press rate (1 = 1bar/s)                         */
	/**********************************************************/
	stCalcTarPRateOutput.s16TarPRateOutputBarPSec = (stCalcTarPRateOutput.s16TarPRateOutputX10*2)/5;

	/**********************************************************/
	/* Target Press rate 10ms (1 = 1bar/s)                         */
	/**********************************************************/
	stCalcTarPRateOutput.s16TarPRateOutput10ms = LCBBC_s16CalcStdTargetPressRate(s16TargetPressInput);

	/* USE Target Press rate 10ms (1 = 1bar/s) */
	stBBCTarPRateOutput.s16CtrlTarPRateFromBaseBrkCtrlr = stCalcTarPRateOutput.s16TarPRateOutput10ms;

	return stBBCTarPRateOutput;
}

static int16_t LCBBC_s16CalcStdTargetPressRate(int16_t s16TargetPressInput)
{
	int16_t s16TGPressBuffCntBf2scan = 0;
	int16_t s16FilterInput = 0;
	int32_t s32FilterInputTemp = 0;

	static int16_t lcbbcs16PressBuff[5] = {0,0,0,0,0};
	static int16_t lcbbcs16BuffCnt = 0;
	static int16_t lcbbcs16PressRate10ms = 0;
	static int16_t lcbbcs16PressRate5ms = 0;

	lcbbcs16PressBuff[lcbbcs16BuffCnt] = s16TargetPressInput;

	lcbbcs16BuffCnt = lcbbcs16BuffCnt + 1;
    if(lcbbcs16BuffCnt>=5)
    {
    	lcbbcs16BuffCnt = 0;
    }
    else{ }

	/**********************************************************/
	/* Target Press rate (1 = 0.01bar/10ms = 1bar/s)          */
	/**********************************************************/
    if(lcbbcs16BuffCnt>=3)
    {
    	s16TGPressBuffCntBf2scan = lcbbcs16BuffCnt - 3;
    }
    else
    {
    	s16TGPressBuffCntBf2scan = lcbbcs16BuffCnt + 2;
    }

	s32FilterInputTemp = ((int32_t)s16TargetPressInput - (int32_t)lcbbcs16PressBuff[s16TGPressBuffCntBf2scan]); /* target press variation for 2scan. changed algorithm for simplicity in Feb. 2015 -> already checked both algorithms*/

	s16FilterInput = (int16_t)L_s32LimitMinMax(s32FilterInputTemp, ASW_S16_MIN, ASW_S16_MAX);

	lcbbcs16PressRate10ms =(L_s16Lpf1Int(s16FilterInput, lcbbcs16PressRate10ms, U8_LPF_7HZ_128G)); /* 1 bar/sec*/

	/**********************************************************/
	/* Target Press rate (1 = 0.01bar/5ms *2 = 1bar/s)         */
	/**********************************************************/
    if(lcbbcs16BuffCnt>=2)
    {
    	s16TGPressBuffCntBf2scan = lcbbcs16BuffCnt - 2;
    }
    else
    {
    	s16TGPressBuffCntBf2scan = lcbbcs16BuffCnt + 3;
    }

	s32FilterInputTemp = (int32_t)s16TargetPressInput - (int32_t)lcbbcs16PressBuff[s16TGPressBuffCntBf2scan]; /* target press variation for 1 scan */

	s16FilterInput = (int16_t)L_s32LimitMinMax(s32FilterInputTemp, ASW_S16_MIN, ASW_S16_MAX);

	lcbbcs16PressRate5ms = s16FilterInput * 2;          /* not filtered */

	return lcbbcs16PressRate10ms;
}

static int16_t LCBBC_s16CalcTarPRate4BarPSec(int16_t s16TargetPressInput)
{
	static int16_t lcbbcs16TarPRateOutput = 0;
	static int16_t lcbbcs16TargetPressAvg = 0;
	static int16_t lcbbcs16TargetPressSum = 0;
	static int16_t lcbbcs16TargetPressBuff[5] = {0,0,0,0,0};
	static uint8_t lcbbcu8TargetPressAvgCnt = 0;
	static int16_t lcbbcs16TargetPressAvgBuff[5] = {0,0,0,0,0};
	static uint8_t lcbbcu8TargetPressRateCnt = 0;

	lcbbcs16TargetPressSum = (lcbbcs16TargetPressSum - (lcbbcs16TargetPressBuff[lcbbcu8TargetPressAvgCnt])) + s16TargetPressInput;
	lcbbcs16TargetPressAvg = lcbbcs16TargetPressSum/5;

	lcbbcs16TargetPressBuff[lcbbcu8TargetPressAvgCnt] = s16TargetPressInput;
	lcbbcu8TargetPressAvgCnt = lcbbcu8TargetPressAvgCnt + 1;

	if(lcbbcu8TargetPressAvgCnt>=5)
	{
		lcbbcu8TargetPressAvgCnt=0;
	}
	else{ }

	lcbbcs16TarPRateOutput = L_s16Lpf1Int((int16_t)(lcbbcs16TargetPressAvg - lcbbcs16TargetPressAvgBuff[lcbbcu8TargetPressRateCnt]),(int16_t)lcbbcs16TarPRateOutput, (uint8_t)U8_LPF_7HZ_128G);
	lcbbcs16TargetPressAvgBuff[lcbbcu8TargetPressRateCnt] = lcbbcs16TargetPressAvg;

	lcbbcu8TargetPressRateCnt = lcbbcu8TargetPressRateCnt + 1;

	if(lcbbcu8TargetPressRateCnt>=5)
	{
		lcbbcu8TargetPressRateCnt = 0;
	}
	else{ }

	return lcbbcs16TarPRateOutput;
}

static int16_t LCBBC_s16CalcTarPRateDot4BarPSec(int16_t s16TargetPressInput)
{
	int16_t s16FilterInput = 0;
	static int16_t lcbbcs16TargetPressBuffX10[5] = {0,0,0,0,0};
	static int16_t lcbbcs16TarPRateOutputX10 = 0;
	static uint8_t lcbbcu8TargetPressBuffCnt = 0;

	s16FilterInput = s16TargetPressInput - lcbbcs16TargetPressBuffX10[lcbbcu8TargetPressBuffCnt]; /* target press variation for 5scan */

	lcbbcs16TarPRateOutputX10 = L_s16Lpf1Int(s16FilterInput, lcbbcs16TarPRateOutputX10, U8_LPF_7HZ_128G);          /* 7Hz */

	lcbbcs16TargetPressBuffX10[lcbbcu8TargetPressBuffCnt] = s16TargetPressInput;

	lcbbcu8TargetPressBuffCnt = lcbbcu8TargetPressBuffCnt + 1;
	if(lcbbcu8TargetPressBuffCnt>=5)
	{
		lcbbcu8TargetPressBuffCnt = 0;
	}
	else{ }

	return lcbbcs16TarPRateOutputX10;
}

#define BBC_STOP_SEC_CODE
#include "Bbc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
