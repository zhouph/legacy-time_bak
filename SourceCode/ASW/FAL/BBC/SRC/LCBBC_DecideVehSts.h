/**
 * @defgroup LCBBC_DecideVehSts LCBBC_DecideVehSts
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LCBBC_DecideVehSts.h
 * @brief       Local Header File
 * @date        20YY. MM. DD.
 ******************************************************************************/
 
#ifndef LDBBC_DECIDEVEHSTS_H_
#define LDBBC_DECIDEVEHSTS_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bbc_Ctrl.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
//#define U8_IGN_OFF         1
//#define U8_IGN_ON          0

#define U8_STABILIZING_TIME_AFT_IGN_ON      U8_T_500_MS
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void LCBBC_vCallDecideVehSts(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* LDBBC_DECIDEPEDLSIMRPGEN_H_ */
/** @} */
