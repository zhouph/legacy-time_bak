/**
 * @defgroup LCBBC_DecideCtrlSt LCBBC_DecideCtrlSt
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LCBBC_DecideCtrlSt.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "LCBBC_DecideCtrlSt.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define BBC_INHIBIT     0
#define BBC_READY       1
#define BBC_APPLY       2


/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
	int16_t lcbbcs16PedalTravelFilt;
    int16_t lcbbcs16PedalTravelRateMmPSec;
    int16_t lcbbcs16TargetPressurebypedal;
    int16_t lcbbcs16PSpresFilt;
    int16_t lcbbcs16TargetPressurebyBaseBrk;

    uint32_t lcbbcu1BLS                     :1;
    uint32_t lcbbcu1BLSFaultDet             :1;
    uint32_t lcbbcu1Inhibit                 :1;
    uint32_t lcbbcu1IncIDBEnterExitThres    :1;
    uint32_t lcbbcu1BfEOLPdOffsetCorrect    :1;
    uint32_t lcbbcu1PedalReleaseFlg1        :1;
    uint32_t lcbbcu1PedalReleaseFlg2        :1;
    uint32_t ldbbcu1InitFastPedalApplyFlg	 :1;
    uint32_t ldbbcu1InitFastPedalApply1msFlg :1;
}GetDecideCtrlStInfo_t;

typedef struct
{
    uint32_t lcbbcu1BBCOn                   :1;
    uint32_t lcbbcu1PedalAppIgnOnFlg        :1;
    uint32_t lcbbcu1InitFastApplyFlg		 :1;
}SetDecideCtrlStInfo_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define BBC_START_SEC_CONST_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/

#define BBC_STOP_SEC_CONST_UNSPECIFIED
#include "Bbc_MemMap.h"

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define BBC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define BBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define BBC_STOP_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define BBC_STOP_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (32BIT)**/


#define BBC_STOP_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define BBC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

FAL_STATIC GetDecideCtrlStInfo_t                    GetDecideCtrlSt;
FAL_STATIC SetDecideCtrlStInfo_t                    SetDecideCtrlSt;

FAL_STATIC uint8_t lcbbcu8ControlMode;
FAL_STATIC uint8_t  lcbbcu8BBCExitCnt;
FAL_STATIC uint16_t lcbbcu16BBCOnCnt, lcbbcu16BBCOnByPedalCnt;
FAL_STATIC uint16_t lcbbcu16BBCOffTimer;
FAL_STATIC uint16_t lcbbcu16BBCFadeOutTimer;

FAL_STATIC uint8_t lcbbcu8BBCState;

FAL_STATIC U32_BIT_STRUCT_t BBC_DET_CONTROL_STATE_1;

extern uint8_t u8IdbCtrlInhibitFlg;

#define lcbbcu1ApplySatisfy             BBC_DET_CONTROL_STATE_1.bit31
#define lcbbcu1ReadySatisfy             BBC_DET_CONTROL_STATE_1.bit30


#define BBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define BBC_STOP_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define BBC_STOP_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (32BIT)**/


#define BBC_STOP_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define BBC_START_SEC_CODE
#include "Bbc_MemMap.h"
static void LCBBC_vChkApplyCdn(void);
static void LCBBC_vChkRdyCdn(void);
static void LCBBC_vDecideCtrlSt(void);
static void LCBBC_vSetCtrlStIf(void);
static void LCBBC_vGetCtrlStIf(void);
static void LCBBC_vChkInitFastApplyCdn(void);
static void LCBBC_vDetBaseBrakeState(void);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void LCBBC_vCallCtrlSt(void)
{
    LCBBC_vGetCtrlStIf();
    LCBBC_vDecideCtrlSt();
    LCBBC_vChkInitFastApplyCdn();
    LCBBC_vDetBaseBrakeState();
    LCBBC_vSetCtrlStIf();
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
/******************************************************************************
* FUNCTION NAME:      LCBBC_vGetCtrlStIf
* CALLED BY:          LCBBC_vCallCtrlSt()
* Preconditions:      none
* PARAMETER:          none
* RETURN VALUE:       none
* Description:		  Get interface bus variable
******************************************************************************/
static void LCBBC_vGetCtrlStIf(void)
{
	GetDecideCtrlSt.lcbbcs16PedalTravelFilt = (int16_t)Bbc_CtrlBus.Bbc_CtrlPedlTrvlFinal;
	GetDecideCtrlSt.lcbbcs16PedalTravelRateMmPSec = (int16_t)Bbc_CtrlBus.Bbc_CtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec;
	GetDecideCtrlSt.lcbbcs16TargetPressurebypedal =(int16_t) Bbc_CtrlBus.Bbc_CtrlTarPFromBrkPedl;
	GetDecideCtrlSt.lcbbcs16PSpresFilt = (int16_t)Bbc_CtrlBus.Bbc_CtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar;
	GetDecideCtrlSt.lcbbcs16TargetPressurebyBaseBrk = (int16_t)Bbc_CtrlBus.Bbc_CtrlTarPFromBaseBrkCtrlr;

	GetDecideCtrlSt.lcbbcu1BLS = Bbc_CtrlBus.Bbc_CtrlBlsFild;
	GetDecideCtrlSt.lcbbcu1BLSFaultDet = Bbc_CtrlBus.Bbc_CtrlEemFailData.Eem_Fail_BLS;
	GetDecideCtrlSt.lcbbcu1Inhibit =  u8IdbCtrlInhibitFlg;
	GetDecideCtrlSt.lcbbcu1IncIDBEnterExitThres = 0; /*temporary, there is no RTE variable*/
	GetDecideCtrlSt.lcbbcu1BfEOLPdOffsetCorrect = 1; /*temporary, there is no RTE variable*/
	GetDecideCtrlSt.lcbbcu1PedalReleaseFlg1 = Bbc_CtrlBus.Bbc_CtrlBrkPedlStatusInfo.PedlReldStFlg1;
	GetDecideCtrlSt.lcbbcu1PedalReleaseFlg2 = Bbc_CtrlBus.Bbc_CtrlBrkPedlStatusInfo.PedlReldStFlg2;
	GetDecideCtrlSt.ldbbcu1InitFastPedalApplyFlg = Bbc_CtrlBus.Bbc_CtrlBrkPedlStatusInfo.PanicBrkStFlg;
	//GetDecideCtrlSt.ldbbcu1InitFastPedalApply1msFlg = Bbc_CtrlBus.Bbc_CtrlBrkPedlStatusInfo.PanicBrkSt1msFlg;
	GetDecideCtrlSt.ldbbcu1InitFastPedalApply1msFlg = Bbc_CtrlBus.Bbc_CtrlBrkPedlStatus1msInfo.PanicBrkSt1msFlg;
}

/******************************************************************************
* FUNCTION NAME:      LCBBC_vSetCtrlStIf
* CALLED BY:          LCBBC_vCallCtrlSt()
* Preconditions:      none
* PARAMETER:          none
* RETURN VALUE:       none
* Description:		  Set interface bus variable
******************************************************************************/
static void LCBBC_vSetCtrlStIf(void)
{
	Bbc_CtrlBus.Bbc_CtrlBaseBrkCtrlrActFlg = SetDecideCtrlSt.lcbbcu1BBCOn;
	Bbc_CtrlBus.Bbc_CtrlBBCCtrlInfo.BBCCtrlSt = lcbbcu8BBCState;
}

/******************************************************************************
* FUNCTION NAME:      LCBBC_vDecideCtrlSt
* CALLED BY:          LCBBC_vCallCtrlSt()
* Preconditions:      none
* PARAMETER:		  none
* RETURN VALUE:       none
* Description:		  decide BBC control state
******************************************************************************/
static void LCBBC_vDecideCtrlSt(void)
{
	switch (lcbbcu8ControlMode)
	{
		/*-------------------------------------------------------------------------- */
		case BBC_READY:
			if(GetDecideCtrlSt.lcbbcu1Inhibit == 1)
			{
				lcbbcu8ControlMode = BBC_INHIBIT;
				SetDecideCtrlSt.lcbbcu1BBCOn = 0;
				BBCsrcbus.lcbbcu1PedalAppIgnOnFlg = 0;
			}
			else
			{
				LCBBC_vChkApplyCdn();

				if(lcbbcu1ApplySatisfy==1)
				{
					lcbbcu8ControlMode = BBC_APPLY;
					SetDecideCtrlSt.lcbbcu1BBCOn = 1;

					if (GetDecideCtrlSt.lcbbcs16PedalTravelFilt >= S16_PEDAL_APP_IGN_ON_THRE_TRAVEL)
					{
						BBCsrcbus.lcbbcu1PedalAppIgnOnFlg = 1;
					}
					else
					{
						BBCsrcbus.lcbbcu1PedalAppIgnOnFlg = 0;
					}

					lcbbcu1ApplySatisfy = 0;
				}
				else
				{
					lcbbcu8ControlMode = BBC_READY;
					SetDecideCtrlSt.lcbbcu1BBCOn = 0;
					BBCsrcbus.lcbbcu1PedalAppIgnOnFlg = 0;
				}
			}

			break;
		/*-------------------------------------------------------------------------- */
		case BBC_APPLY:

			if(GetDecideCtrlSt.lcbbcu1Inhibit == 1)
			{
				lcbbcu8ControlMode = BBC_INHIBIT;
				SetDecideCtrlSt.lcbbcu1BBCOn = 0;
			}
			else
			{
				LCBBC_vChkRdyCdn();
				if (lcbbcu1ReadySatisfy == 1)
				{
					lcbbcu8ControlMode = BBC_READY;
					SetDecideCtrlSt.lcbbcu1BBCOn = 0;
					lcbbcu1ReadySatisfy = 0;
					lcbbcu8BBCExitCnt = 0;
				}
				else
				{
					lcbbcu8ControlMode = BBC_APPLY;
					SetDecideCtrlSt.lcbbcu1BBCOn = 1;
				}
			}
			break;

		/*-------------------------------------------------------------------------- */
		case BBC_INHIBIT:
			if(GetDecideCtrlSt.lcbbcu1Inhibit==0)
			{
				lcbbcu8ControlMode = BBC_READY;
				SetDecideCtrlSt.lcbbcu1BBCOn = 0;
				lcbbcu8BBCExitCnt = 0;
			}
			else
			{
				;
			}
			break;

		/*-------------------------------------------------------------------------- */
		default:
			if(GetDecideCtrlSt.lcbbcu1Inhibit==1)
			{
				lcbbcu8ControlMode = BBC_INHIBIT;
			}
			else
			{
				lcbbcu8ControlMode = BBC_READY;
				lcbbcu8BBCExitCnt = 0;
			}

			break;
	}
}

/******************************************************************************
* FUNCTION NAME:      LCBBC_vChkApplyCdn
* CALLED BY:          LCBBC_vDecideCtrlSt()
* Preconditions:      none
* PARAMETER:		  none
* RETURN VALUE:       none
* Description:		  Check apply condition
******************************************************************************/
static void LCBBC_vChkApplyCdn(void)
{
	if ((GetDecideCtrlSt.lcbbcs16TargetPressurebypedal>=S16_PCTRL_PRESS_0_5_BAR) && (GetDecideCtrlSt.lcbbcs16TargetPressurebyBaseBrk>=S16_PCTRL_PRESS_0_5_BAR))
	{
		if(GetDecideCtrlSt.lcbbcu1IncIDBEnterExitThres==0)
		{
			if((GetDecideCtrlSt.lcbbcs16PedalTravelFilt>=S16_BBCON_PD_MIN)
			  && (   (GetDecideCtrlSt.lcbbcs16PedalTravelRateMmPSec>=S16_TRAVEL_0_1_MM)
			      || (GetDecideCtrlSt.lcbbcs16PSpresFilt>=S16_PCTRL_PRESS_0_5_BAR)
			      || ((GetDecideCtrlSt.lcbbcu1BLS == 1) && (GetDecideCtrlSt.lcbbcu1BLSFaultDet==0))
			      || (GetDecideCtrlSt.lcbbcs16PedalTravelFilt>=S16_BBCON_PD_OFFST_INVALID)
			     )
			  )
			{
				lcbbcu1ApplySatisfy = 1;
			}
		}
		else
		{
			if(GetDecideCtrlSt.lcbbcu1BfEOLPdOffsetCorrect==1)
			{
				if((GetDecideCtrlSt.lcbbcu1BLS == 1) && (GetDecideCtrlSt.lcbbcu1BLSFaultDet==0) && (GetDecideCtrlSt.lcbbcs16PedalTravelFilt>=S16_BBCON_PD_OFFST_INVALID_WBLS))
				{
					lcbbcu1ApplySatisfy = 1;
				}
				else if(GetDecideCtrlSt.lcbbcs16PedalTravelFilt>=S16_BBCON_PD_BF_EOL_CORRECTION)
				{
					lcbbcu1ApplySatisfy = 1;
				}
				else
				{
					lcbbcu1ApplySatisfy = 0;
				}
			}
			else
			{
				if((GetDecideCtrlSt.lcbbcu1BLS == 1) && (GetDecideCtrlSt.lcbbcu1BLSFaultDet==0) && (GetDecideCtrlSt.lcbbcs16PedalTravelFilt>=S16_BBCON_PD_OFFST_INVALID_WBLS))
				{
					lcbbcu1ApplySatisfy = 1;
				}
				else if(GetDecideCtrlSt.lcbbcs16PedalTravelFilt>=S16_BBCON_PD_OFFST_INVALID)
				{
					lcbbcu1ApplySatisfy = 1;
				}
				else
				{
					lcbbcu1ApplySatisfy = 0;
				}
			}
		}
	}
	else
	{
		lcbbcu1ApplySatisfy = 0;
	}
}

/******************************************************************************
* FUNCTION NAME:      LCBBC_vChkRdyCdn
* CALLED BY:          LCBBC_vDecideCtrlSt()
* Preconditions:      none
* PARAMETER:   		  none
* RETURN VALUE:       none
* Description:		  Check ready condition
******************************************************************************/
static void LCBBC_vChkRdyCdn(void)
{
	if ((GetDecideCtrlSt.lcbbcs16TargetPressurebypedal<S16_PCTRL_PRESS_0_5_BAR) && (GetDecideCtrlSt.lcbbcs16TargetPressurebyBaseBrk<S16_PCTRL_PRESS_0_5_BAR))
	{

		lcbbcu8BBCExitCnt = lcbbcu8BBCExitCnt + 1;

		if(lcbbcu8BBCExitCnt>=U8_BBC_EXIT_WAIT_TIME)
		{
			lcbbcu1ReadySatisfy = 1;
			lcbbcu8BBCExitCnt = 0;
		}
		else
		{
			lcbbcu1ReadySatisfy = 0;
		}
	}
	else
	{
		if (GetDecideCtrlSt.lcbbcu1IncIDBEnterExitThres==1)       /* Extra Exit Condition for Pedal Offset Invalid */
		{
			if( (GetDecideCtrlSt.lcbbcu1BfEOLPdOffsetCorrect==1) &&
				(GetDecideCtrlSt.lcbbcu1BLS==0) && (GetDecideCtrlSt.lcbbcu1BLSFaultDet==0) && (GetDecideCtrlSt.lcbbcs16PedalTravelFilt<=(S16_BBCON_PD_BF_EOL_CORRECTION/2)) )
			{
				lcbbcu8BBCExitCnt = lcbbcu8BBCExitCnt + 1;
			}
			else if(GetDecideCtrlSt.lcbbcu1PedalReleaseFlg1==1)
			{
				lcbbcu8BBCExitCnt = lcbbcu8BBCExitCnt + 1;
			}
			else if(((GetDecideCtrlSt.lcbbcu1BLS==0)||(GetDecideCtrlSt.lcbbcu1BLSFaultDet==1)) && (GetDecideCtrlSt.lcbbcu1PedalReleaseFlg2==1))
			{
				lcbbcu8BBCExitCnt = lcbbcu8BBCExitCnt + 1;
			}
			else
			{
				if(lcbbcu8BBCExitCnt>0)
				{
					lcbbcu8BBCExitCnt = lcbbcu8BBCExitCnt - 1;
				}
			}

			if(lcbbcu8BBCExitCnt>=U8_BBC_EXIT_WAIT_TIME)
			{
				lcbbcu1ReadySatisfy = 1;
				lcbbcu8BBCExitCnt = 0;
			}
			else
			{
				lcbbcu1ReadySatisfy = 0;
			}
		}
		else
		{
			if(lcbbcu8BBCExitCnt>0)
			{
				lcbbcu8BBCExitCnt = lcbbcu8BBCExitCnt - 1;
			}
			lcbbcu1ReadySatisfy = 0;
		}
	}
}

static void LCBBC_vChkInitFastApplyCdn(void)
{
	static uint16_t lcahbu16BBCOnCnt = 0;
	static uint16_t lcahbu16BBCOnByPedalCnt = 0;

	if(SetDecideCtrlSt.lcbbcu1BBCOn==1)
	{
		if(lcahbu16BBCOnCnt < U16_T_1_S)
		{
			lcahbu16BBCOnCnt++;
		}
		else{ }

		if((lcahbu16BBCOnByPedalCnt < U16_T_1_S) && (lcbbcu8ControlMode==BBC_APPLY))
		{
			lcahbu16BBCOnByPedalCnt++;
		}
		else{ }


		if((lcahbu16BBCOnCnt < U16_T_1_S) && (lcahbu16BBCOnByPedalCnt <= U8_T_200_MS))
		{
			if(SetDecideCtrlSt.lcbbcu1InitFastApplyFlg==0)
			{
				if((GetDecideCtrlSt.ldbbcu1InitFastPedalApplyFlg==1) || (GetDecideCtrlSt.ldbbcu1InitFastPedalApply1msFlg==1))
				{
					SetDecideCtrlSt.lcbbcu1InitFastApplyFlg = 1;
				}
			}
			else
			{
				if((GetDecideCtrlSt.ldbbcu1InitFastPedalApplyFlg==0) && (GetDecideCtrlSt.ldbbcu1InitFastPedalApply1msFlg==0))
				{
					SetDecideCtrlSt.lcbbcu1InitFastApplyFlg = 0;
				}
			}
		}
		else
		{
			SetDecideCtrlSt.lcbbcu1InitFastApplyFlg = 0;
		}
	}
	else
	{
		lcahbu16BBCOnCnt = 0;
		lcahbu16BBCOnByPedalCnt = 0;
		SetDecideCtrlSt.lcbbcu1InitFastApplyFlg = 0;
	}
}

static void LCBBC_vDetBaseBrakeState(void)
{

	if(SetDecideCtrlSt.lcbbcu1BBCOn == 1)
	{
		if(SetDecideCtrlSt.lcbbcu1InitFastApplyFlg == 1)
		{
		    lcbbcu8BBCState = 2;
		}
		else
		{
			lcbbcu8BBCState = 1;
		}
	}
	else
	{
		lcbbcu8BBCState = 0;
	}

}





#define BBC_STOP_SEC_CODE
#include "Bbc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
