/**
 * @defgroup Bbc_Ctrl Bbc_Ctrl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Bbc_Ctrl.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bbc_Ctrl.h"
#include "Bbc_Ctrl_Ifa.h"
#include "IfxStm_reg.h"
#include "LCBBC_EstDrvrIntdTP.h"
#include "LCBBC_DecideCtrlSt.h"
#include "LCBBC_LimBrkTP.h"
#include "LCBBC_DecidePedlSimrPGen.h"
#include "LCBBC_DecideVehSts.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define BBC_CTRL_START_SEC_CONST_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define BBC_CTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Bbc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define BBC_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Bbc_Ctrl_HdrBusType Bbc_CtrlBus;

/* Internal Logic Bus */

BaseBrakeControl_SRCbusType    BBCsrcbus;

/* Version Info */
const SwcVersionInfo_t Bbc_CtrlVersionInfo = 
{   
    BBC_CTRL_MODULE_ID,           /* Bbc_CtrlVersionInfo.ModuleId */
    BBC_CTRL_MAJOR_VERSION,       /* Bbc_CtrlVersionInfo.MajorVer */
    BBC_CTRL_MINOR_VERSION,       /* Bbc_CtrlVersionInfo.MinorVer */
    BBC_CTRL_PATCH_VERSION,       /* Bbc_CtrlVersionInfo.PatchVer */
    BBC_CTRL_BRANCH_VERSION       /* Bbc_CtrlVersionInfo.BranchVer */
};
    
/* Input Data Element */
Eem_MainEemFailData_t Bbc_CtrlEemFailData;
Proxy_RxCanRxIdbInfo_t Bbc_CtrlCanRxIdbInfo;
Det_1msCtrlBrkPedlStatus1msInfo_t Bbc_CtrlBrkPedlStatus1msInfo;
Det_5msCtrlBrkPedlStatusInfo_t Bbc_CtrlBrkPedlStatusInfo;
Spc_5msCtrlPedlSimrPFildInfo_t Bbc_CtrlPedlSimrPFildInfo;
Det_5msCtrlPedlTrvlRateInfo_t Bbc_CtrlPedlTrvlRateInfo;
Det_5msCtrlVehStopStInfo_t Bbc_CtrlVehStopStInfo;
Eem_MainEemSuspectData_t Bbc_CtrlEemSuspectData;
Mom_HndlrEcuModeSts_t Bbc_CtrlEcuModeSts;
Prly_HndlrIgnOnOffSts_t Bbc_CtrlIgnOnOffSts;
Eem_SuspcDetnFuncInhibitBbcSts_t Bbc_CtrlFuncInhibitBbcSts;
Spc_5msCtrlBlsFild_t Bbc_CtrlBlsFild;
Rbc_CtrlBrkPRednForBaseBrkCtrlr_t Bbc_CtrlBrkPRednForBaseBrkCtrlr;
Spc_5msCtrlFlexBrkSwtFild_t Bbc_CtrlFlexBrkSwtFild;
Det_5msCtrlPedlTrvlFinal_t Bbc_CtrlPedlTrvlFinal;
Rbc_CtrlRgnBrkCtrlrActStFlg_t Bbc_CtrlRgnBrkCtrlrActStFlg;
Det_5msCtrlVehSpdFild_t Bbc_CtrlVehSpdFild;

/* Output Data Element */
Bbc_CtrlBaseBrkCtrlModInfo_t Bbc_CtrlBaseBrkCtrlModInfo;
Bbc_CtrlBBCCtrlInfo_t Bbc_CtrlBBCCtrlInfo;
Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo_t Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo;
Bbc_CtrlBaseBrkCtrlrActFlg_t Bbc_CtrlBaseBrkCtrlrActFlg;
Bbc_CtrlTarPFromBaseBrkCtrlr_t Bbc_CtrlTarPFromBaseBrkCtrlr;
Bbc_CtrlTarPRateFromBaseBrkCtrlr_t Bbc_CtrlTarPRateFromBaseBrkCtrlr;
Bbc_CtrlTarPFromBrkPedl_t Bbc_CtrlTarPFromBrkPedl;

uint32 Bbc_Ctrl_Timer_Start;
uint32 Bbc_Ctrl_Timer_Elapsed;

#define BBC_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define BBC_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
#define BBC_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define BBC_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_CTRL_START_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (32BIT)**/


#define BBC_CTRL_STOP_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define BBC_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define BBC_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define BBC_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
#define BBC_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define BBC_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_CTRL_START_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (32BIT)**/


#define BBC_CTRL_STOP_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define BBC_CTRL_START_SEC_CODE
#include "Bbc_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Bbc_Ctrl_Init(void)
{
    /* Initialize internal bus */
    Bbc_CtrlBus.Bbc_CtrlEemFailData.Eem_Fail_BLS = 0;
    Bbc_CtrlBus.Bbc_CtrlEemFailData.Eem_Fail_WssFL = 0;
    Bbc_CtrlBus.Bbc_CtrlEemFailData.Eem_Fail_WssFR = 0;
    Bbc_CtrlBus.Bbc_CtrlEemFailData.Eem_Fail_WssRL = 0;
    Bbc_CtrlBus.Bbc_CtrlEemFailData.Eem_Fail_WssRR = 0;
    Bbc_CtrlBus.Bbc_CtrlCanRxIdbInfo.GearSelDisp = 0;
    Bbc_CtrlBus.Bbc_CtrlCanRxIdbInfo.TcuSwiGs = 0;
    Bbc_CtrlBus.Bbc_CtrlBrkPedlStatus1msInfo.PanicBrkSt1msFlg = 0;
    Bbc_CtrlBus.Bbc_CtrlBrkPedlStatusInfo.PanicBrkStFlg = 0;
    Bbc_CtrlBus.Bbc_CtrlBrkPedlStatusInfo.PedlReldStFlg1 = 0;
    Bbc_CtrlBus.Bbc_CtrlBrkPedlStatusInfo.PedlReldStFlg2 = 0;
    Bbc_CtrlBus.Bbc_CtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar = 0;
    Bbc_CtrlBus.Bbc_CtrlPedlTrvlRateInfo.PedlTrvlRate = 0;
    Bbc_CtrlBus.Bbc_CtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec = 0;
    Bbc_CtrlBus.Bbc_CtrlVehStopStInfo.VehStopStFlg = 0;
    Bbc_CtrlBus.Bbc_CtrlEemSuspectData.Eem_Suspect_WssFL = 0;
    Bbc_CtrlBus.Bbc_CtrlEemSuspectData.Eem_Suspect_WssFR = 0;
    Bbc_CtrlBus.Bbc_CtrlEemSuspectData.Eem_Suspect_WssRL = 0;
    Bbc_CtrlBus.Bbc_CtrlEemSuspectData.Eem_Suspect_WssRR = 0;
    Bbc_CtrlBus.Bbc_CtrlEcuModeSts = 0;
    Bbc_CtrlBus.Bbc_CtrlIgnOnOffSts = 0;
    Bbc_CtrlBus.Bbc_CtrlFuncInhibitBbcSts = 0;
    Bbc_CtrlBus.Bbc_CtrlBlsFild = 0;
    Bbc_CtrlBus.Bbc_CtrlBrkPRednForBaseBrkCtrlr = 0;
    Bbc_CtrlBus.Bbc_CtrlFlexBrkSwtFild = 0;
    Bbc_CtrlBus.Bbc_CtrlPedlTrvlFinal = 0;
    Bbc_CtrlBus.Bbc_CtrlRgnBrkCtrlrActStFlg = 0;
    Bbc_CtrlBus.Bbc_CtrlVehSpdFild = 0;
    Bbc_CtrlBus.Bbc_CtrlBaseBrkCtrlModInfo.LowSpdCtrlInhibitFlg = 0;
    Bbc_CtrlBus.Bbc_CtrlBaseBrkCtrlModInfo.StopdVehCtrlModFlg = 0;
    Bbc_CtrlBus.Bbc_CtrlBaseBrkCtrlModInfo.VehStandStillStFlg = 0;
    Bbc_CtrlBus.Bbc_CtrlBBCCtrlInfo.BBCCtrlSt = 0;
    Bbc_CtrlBus.Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo.PedlSimrPGendOnFlg = 0;
    Bbc_CtrlBus.Bbc_CtrlBaseBrkCtrlrActFlg = 0;
    Bbc_CtrlBus.Bbc_CtrlTarPFromBaseBrkCtrlr = 0;
    Bbc_CtrlBus.Bbc_CtrlTarPRateFromBaseBrkCtrlr = 0;
    Bbc_CtrlBus.Bbc_CtrlTarPFromBrkPedl = 0;
}

void Bbc_Ctrl(void)
{
    Bbc_Ctrl_Timer_Start = STM0_TIM0.U;

    /* Input */
    /* Decomposed structure interface */
    Bbc_Ctrl_Read_Bbc_CtrlEemFailData_Eem_Fail_BLS(&Bbc_CtrlBus.Bbc_CtrlEemFailData.Eem_Fail_BLS);
    Bbc_Ctrl_Read_Bbc_CtrlEemFailData_Eem_Fail_WssFL(&Bbc_CtrlBus.Bbc_CtrlEemFailData.Eem_Fail_WssFL);
    Bbc_Ctrl_Read_Bbc_CtrlEemFailData_Eem_Fail_WssFR(&Bbc_CtrlBus.Bbc_CtrlEemFailData.Eem_Fail_WssFR);
    Bbc_Ctrl_Read_Bbc_CtrlEemFailData_Eem_Fail_WssRL(&Bbc_CtrlBus.Bbc_CtrlEemFailData.Eem_Fail_WssRL);
    Bbc_Ctrl_Read_Bbc_CtrlEemFailData_Eem_Fail_WssRR(&Bbc_CtrlBus.Bbc_CtrlEemFailData.Eem_Fail_WssRR);

    /* Decomposed structure interface */
    Bbc_Ctrl_Read_Bbc_CtrlCanRxIdbInfo_GearSelDisp(&Bbc_CtrlBus.Bbc_CtrlCanRxIdbInfo.GearSelDisp);
    Bbc_Ctrl_Read_Bbc_CtrlCanRxIdbInfo_TcuSwiGs(&Bbc_CtrlBus.Bbc_CtrlCanRxIdbInfo.TcuSwiGs);

    Bbc_Ctrl_Read_Bbc_CtrlBrkPedlStatus1msInfo(&Bbc_CtrlBus.Bbc_CtrlBrkPedlStatus1msInfo);
    /*==============================================================================
    * Members of structure Bbc_CtrlBrkPedlStatus1msInfo 
     : Bbc_CtrlBrkPedlStatus1msInfo.PanicBrkSt1msFlg;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Bbc_Ctrl_Read_Bbc_CtrlBrkPedlStatusInfo_PanicBrkStFlg(&Bbc_CtrlBus.Bbc_CtrlBrkPedlStatusInfo.PanicBrkStFlg);
    Bbc_Ctrl_Read_Bbc_CtrlBrkPedlStatusInfo_PedlReldStFlg1(&Bbc_CtrlBus.Bbc_CtrlBrkPedlStatusInfo.PedlReldStFlg1);
    Bbc_Ctrl_Read_Bbc_CtrlBrkPedlStatusInfo_PedlReldStFlg2(&Bbc_CtrlBus.Bbc_CtrlBrkPedlStatusInfo.PedlReldStFlg2);

    /* Decomposed structure interface */
    Bbc_Ctrl_Read_Bbc_CtrlPedlSimrPFildInfo_PedlSimrPFild_1_100Bar(&Bbc_CtrlBus.Bbc_CtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar);

    Bbc_Ctrl_Read_Bbc_CtrlPedlTrvlRateInfo(&Bbc_CtrlBus.Bbc_CtrlPedlTrvlRateInfo);
    /*==============================================================================
    * Members of structure Bbc_CtrlPedlTrvlRateInfo 
     : Bbc_CtrlPedlTrvlRateInfo.PedlTrvlRate;
     : Bbc_CtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Bbc_Ctrl_Read_Bbc_CtrlVehStopStInfo_VehStopStFlg(&Bbc_CtrlBus.Bbc_CtrlVehStopStInfo.VehStopStFlg);

    /* Decomposed structure interface */
    Bbc_Ctrl_Read_Bbc_CtrlEemSuspectData_Eem_Suspect_WssFL(&Bbc_CtrlBus.Bbc_CtrlEemSuspectData.Eem_Suspect_WssFL);
    Bbc_Ctrl_Read_Bbc_CtrlEemSuspectData_Eem_Suspect_WssFR(&Bbc_CtrlBus.Bbc_CtrlEemSuspectData.Eem_Suspect_WssFR);
    Bbc_Ctrl_Read_Bbc_CtrlEemSuspectData_Eem_Suspect_WssRL(&Bbc_CtrlBus.Bbc_CtrlEemSuspectData.Eem_Suspect_WssRL);
    Bbc_Ctrl_Read_Bbc_CtrlEemSuspectData_Eem_Suspect_WssRR(&Bbc_CtrlBus.Bbc_CtrlEemSuspectData.Eem_Suspect_WssRR);

    Bbc_Ctrl_Read_Bbc_CtrlEcuModeSts(&Bbc_CtrlBus.Bbc_CtrlEcuModeSts);
    Bbc_Ctrl_Read_Bbc_CtrlIgnOnOffSts(&Bbc_CtrlBus.Bbc_CtrlIgnOnOffSts);
    Bbc_Ctrl_Read_Bbc_CtrlFuncInhibitBbcSts(&Bbc_CtrlBus.Bbc_CtrlFuncInhibitBbcSts);
    Bbc_Ctrl_Read_Bbc_CtrlBlsFild(&Bbc_CtrlBus.Bbc_CtrlBlsFild);
    Bbc_Ctrl_Read_Bbc_CtrlBrkPRednForBaseBrkCtrlr(&Bbc_CtrlBus.Bbc_CtrlBrkPRednForBaseBrkCtrlr);
    Bbc_Ctrl_Read_Bbc_CtrlFlexBrkSwtFild(&Bbc_CtrlBus.Bbc_CtrlFlexBrkSwtFild);
    Bbc_Ctrl_Read_Bbc_CtrlPedlTrvlFinal(&Bbc_CtrlBus.Bbc_CtrlPedlTrvlFinal);
    Bbc_Ctrl_Read_Bbc_CtrlRgnBrkCtrlrActStFlg(&Bbc_CtrlBus.Bbc_CtrlRgnBrkCtrlrActStFlg);
    Bbc_Ctrl_Read_Bbc_CtrlVehSpdFild(&Bbc_CtrlBus.Bbc_CtrlVehSpdFild);

    /* Process */

    LCBBC_vCallDecideVehSts();

    LCBBC_vCallDrvrIntdTarP();

    LCBBC_vCallBrkTarPLimn();

    LCBBC_vCallCtrlSt();

    LCBBC_vCallPedlSimrPGen();

    LCBBC_vCalcBBCTarPRate();
    /* Output */
    Bbc_Ctrl_Write_Bbc_CtrlBaseBrkCtrlModInfo(&Bbc_CtrlBus.Bbc_CtrlBaseBrkCtrlModInfo);
    /*==============================================================================
    * Members of structure Bbc_CtrlBaseBrkCtrlModInfo 
     : Bbc_CtrlBaseBrkCtrlModInfo.LowSpdCtrlInhibitFlg;
     : Bbc_CtrlBaseBrkCtrlModInfo.StopdVehCtrlModFlg;
     : Bbc_CtrlBaseBrkCtrlModInfo.VehStandStillStFlg;
     =============================================================================*/
    
    Bbc_Ctrl_Write_Bbc_CtrlBBCCtrlInfo(&Bbc_CtrlBus.Bbc_CtrlBBCCtrlInfo);
    /*==============================================================================
    * Members of structure Bbc_CtrlBBCCtrlInfo 
     : Bbc_CtrlBBCCtrlInfo.BBCCtrlSt;
     =============================================================================*/
    
    Bbc_Ctrl_Write_Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo(&Bbc_CtrlBus.Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo);
    /*==============================================================================
    * Members of structure Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo 
     : Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo.PedlSimrPGendOnFlg;
     =============================================================================*/
    
    Bbc_Ctrl_Write_Bbc_CtrlBaseBrkCtrlrActFlg(&Bbc_CtrlBus.Bbc_CtrlBaseBrkCtrlrActFlg);
    Bbc_Ctrl_Write_Bbc_CtrlTarPFromBaseBrkCtrlr(&Bbc_CtrlBus.Bbc_CtrlTarPFromBaseBrkCtrlr);
    Bbc_Ctrl_Write_Bbc_CtrlTarPRateFromBaseBrkCtrlr(&Bbc_CtrlBus.Bbc_CtrlTarPRateFromBaseBrkCtrlr);
    Bbc_Ctrl_Write_Bbc_CtrlTarPFromBrkPedl(&Bbc_CtrlBus.Bbc_CtrlTarPFromBrkPedl);

    Bbc_Ctrl_Timer_Elapsed = STM0_TIM0.U - Bbc_Ctrl_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define BBC_CTRL_STOP_SEC_CODE
#include "Bbc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
