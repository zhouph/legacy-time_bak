/**
 * @defgroup Bbc_Ctrl_Ifa Bbc_Ctrl_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Bbc_Ctrl_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef BBC_CTRL_IFA_H_
#define BBC_CTRL_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Bbc_Ctrl_Read_Bbc_CtrlEemFailData(data) do \
{ \
    *data = Bbc_CtrlEemFailData; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlCanRxIdbInfo(data) do \
{ \
    *data = Bbc_CtrlCanRxIdbInfo; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlBrkPedlStatus1msInfo(data) do \
{ \
    *data = Bbc_CtrlBrkPedlStatus1msInfo; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlBrkPedlStatusInfo(data) do \
{ \
    *data = Bbc_CtrlBrkPedlStatusInfo; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlPedlSimrPFildInfo(data) do \
{ \
    *data = Bbc_CtrlPedlSimrPFildInfo; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlPedlTrvlRateInfo(data) do \
{ \
    *data = Bbc_CtrlPedlTrvlRateInfo; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlVehStopStInfo(data) do \
{ \
    *data = Bbc_CtrlVehStopStInfo; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlEemSuspectData(data) do \
{ \
    *data = Bbc_CtrlEemSuspectData; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlEemFailData_Eem_Fail_BLS(data) do \
{ \
    *data = Bbc_CtrlEemFailData.Eem_Fail_BLS; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlEemFailData_Eem_Fail_WssFL(data) do \
{ \
    *data = Bbc_CtrlEemFailData.Eem_Fail_WssFL; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlEemFailData_Eem_Fail_WssFR(data) do \
{ \
    *data = Bbc_CtrlEemFailData.Eem_Fail_WssFR; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlEemFailData_Eem_Fail_WssRL(data) do \
{ \
    *data = Bbc_CtrlEemFailData.Eem_Fail_WssRL; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlEemFailData_Eem_Fail_WssRR(data) do \
{ \
    *data = Bbc_CtrlEemFailData.Eem_Fail_WssRR; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlCanRxIdbInfo_GearSelDisp(data) do \
{ \
    *data = Bbc_CtrlCanRxIdbInfo.GearSelDisp; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlCanRxIdbInfo_TcuSwiGs(data) do \
{ \
    *data = Bbc_CtrlCanRxIdbInfo.TcuSwiGs; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlBrkPedlStatus1msInfo_PanicBrkSt1msFlg(data) do \
{ \
    *data = Bbc_CtrlBrkPedlStatus1msInfo.PanicBrkSt1msFlg; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlBrkPedlStatusInfo_PanicBrkStFlg(data) do \
{ \
    *data = Bbc_CtrlBrkPedlStatusInfo.PanicBrkStFlg; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlBrkPedlStatusInfo_PedlReldStFlg1(data) do \
{ \
    *data = Bbc_CtrlBrkPedlStatusInfo.PedlReldStFlg1; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlBrkPedlStatusInfo_PedlReldStFlg2(data) do \
{ \
    *data = Bbc_CtrlBrkPedlStatusInfo.PedlReldStFlg2; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlPedlSimrPFildInfo_PedlSimrPFild_1_100Bar(data) do \
{ \
    *data = Bbc_CtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlPedlTrvlRateInfo_PedlTrvlRate(data) do \
{ \
    *data = Bbc_CtrlPedlTrvlRateInfo.PedlTrvlRate; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlPedlTrvlRateInfo_PedlTrvlRateMilliMtrPerSec(data) do \
{ \
    *data = Bbc_CtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlVehStopStInfo_VehStopStFlg(data) do \
{ \
    *data = Bbc_CtrlVehStopStInfo.VehStopStFlg; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlEemSuspectData_Eem_Suspect_WssFL(data) do \
{ \
    *data = Bbc_CtrlEemSuspectData.Eem_Suspect_WssFL; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlEemSuspectData_Eem_Suspect_WssFR(data) do \
{ \
    *data = Bbc_CtrlEemSuspectData.Eem_Suspect_WssFR; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlEemSuspectData_Eem_Suspect_WssRL(data) do \
{ \
    *data = Bbc_CtrlEemSuspectData.Eem_Suspect_WssRL; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlEemSuspectData_Eem_Suspect_WssRR(data) do \
{ \
    *data = Bbc_CtrlEemSuspectData.Eem_Suspect_WssRR; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlEcuModeSts(data) do \
{ \
    *data = Bbc_CtrlEcuModeSts; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlIgnOnOffSts(data) do \
{ \
    *data = Bbc_CtrlIgnOnOffSts; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlFuncInhibitBbcSts(data) do \
{ \
    *data = Bbc_CtrlFuncInhibitBbcSts; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlBlsFild(data) do \
{ \
    *data = Bbc_CtrlBlsFild; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlBrkPRednForBaseBrkCtrlr(data) do \
{ \
    *data = Bbc_CtrlBrkPRednForBaseBrkCtrlr; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlFlexBrkSwtFild(data) do \
{ \
    *data = Bbc_CtrlFlexBrkSwtFild; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlPedlTrvlFinal(data) do \
{ \
    *data = Bbc_CtrlPedlTrvlFinal; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlRgnBrkCtrlrActStFlg(data) do \
{ \
    *data = Bbc_CtrlRgnBrkCtrlrActStFlg; \
}while(0);

#define Bbc_Ctrl_Read_Bbc_CtrlVehSpdFild(data) do \
{ \
    *data = Bbc_CtrlVehSpdFild; \
}while(0);


/* Set Output DE MAcro Function */
#define Bbc_Ctrl_Write_Bbc_CtrlBaseBrkCtrlModInfo(data) do \
{ \
    Bbc_CtrlBaseBrkCtrlModInfo = *data; \
}while(0);

#define Bbc_Ctrl_Write_Bbc_CtrlBBCCtrlInfo(data) do \
{ \
    Bbc_CtrlBBCCtrlInfo = *data; \
}while(0);

#define Bbc_Ctrl_Write_Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo(data) do \
{ \
    Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo = *data; \
}while(0);

#define Bbc_Ctrl_Write_Bbc_CtrlBaseBrkCtrlModInfo_LowSpdCtrlInhibitFlg(data) do \
{ \
    Bbc_CtrlBaseBrkCtrlModInfo.LowSpdCtrlInhibitFlg = *data; \
}while(0);

#define Bbc_Ctrl_Write_Bbc_CtrlBaseBrkCtrlModInfo_StopdVehCtrlModFlg(data) do \
{ \
    Bbc_CtrlBaseBrkCtrlModInfo.StopdVehCtrlModFlg = *data; \
}while(0);

#define Bbc_Ctrl_Write_Bbc_CtrlBaseBrkCtrlModInfo_VehStandStillStFlg(data) do \
{ \
    Bbc_CtrlBaseBrkCtrlModInfo.VehStandStillStFlg = *data; \
}while(0);

#define Bbc_Ctrl_Write_Bbc_CtrlBBCCtrlInfo_BBCCtrlSt(data) do \
{ \
    Bbc_CtrlBBCCtrlInfo.BBCCtrlSt = *data; \
}while(0);

#define Bbc_Ctrl_Write_Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo_PedlSimrPGendOnFlg(data) do \
{ \
    Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo.PedlSimrPGendOnFlg = *data; \
}while(0);

#define Bbc_Ctrl_Write_Bbc_CtrlBaseBrkCtrlrActFlg(data) do \
{ \
    Bbc_CtrlBaseBrkCtrlrActFlg = *data; \
}while(0);

#define Bbc_Ctrl_Write_Bbc_CtrlTarPFromBaseBrkCtrlr(data) do \
{ \
    Bbc_CtrlTarPFromBaseBrkCtrlr = *data; \
}while(0);

#define Bbc_Ctrl_Write_Bbc_CtrlTarPRateFromBaseBrkCtrlr(data) do \
{ \
    Bbc_CtrlTarPRateFromBaseBrkCtrlr = *data; \
}while(0);

#define Bbc_Ctrl_Write_Bbc_CtrlTarPFromBrkPedl(data) do \
{ \
    Bbc_CtrlTarPFromBrkPedl = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* BBC_CTRL_IFA_H_ */
/** @} */
