/**
 * @defgroup Bbc_Cfg Bbc_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Bbc_Cfg.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef BBC_CFG_H_
#define BBC_CFG_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bbc_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/** here **/
#if (__CAR == HMC_BH)
#define __EBD_FAIL_TARGET_P_LIMIT        ENABLE
#define __FLEX_BRAKING_MODE              ENABLE  /*DISABLE*/
#define __BBC_LOW_SPEED_TARGET_P_LIMIT   ENABLE
#define __BBC_ONSTOP_TARGET_P_LIMIT      ENABLE
#endif
#if (__CAR == KMC_TD)
#define _EBD_FAIL_TARGET_P_LIMIT        DISABLE
#define _LFU_MODE_CTRL                  DISABLE  /*DISABLE*/
#define __BBC_LOW_SPEED_TARGET_P_LIMIT  ENABLE
#define __BBC_ONSTOP_TARGET_P_LIMIT     ENABLE
#endif
#if (__CAR == HMC_YFE)
#define __EBD_FAIL_TARGET_P_LIMIT        DISABLE
#define __LFU_MODE_CTRL                  DISABLE  /*DISABLE*/
#define __BBC_LOW_SPEED_TARGET_P_LIMIT   ENABLE
#define __BBC_ONSTOP_TARGET_P_LIMIT      ENABLE
#endif
#if (__CAR == HMC_HGE)
#define __EBD_FAIL_TARGET_P_LIMIT        DISABLE
#define __LFU_MODE_CTRL                  DISABLE  /*DISABLE*/
#define __BBC_LOW_SPEED_TARGET_P_LIMIT   ENABLE
#define __BBC_ONSTOP_TARGET_P_LIMIT      ENABLE
#endif
#if (__CAR == KMC_TFE)
#define __EBD_FAIL_TARGET_P_LIMIT        DISABLE
#define __LFU_MODE_CTRL                  DISABLE  /*DISABLE*/
#define __BBC_LOW_SPEED_TARGET_P_LIMIT   ENABLE
#define __BBC_ONSTOP_TARGET_P_LIMIT      ENABLE
#endif
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* BBC_CFG_H_ */
/** @} */
