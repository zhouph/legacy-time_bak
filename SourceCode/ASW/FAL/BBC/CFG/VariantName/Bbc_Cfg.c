/**
 * @defgroup Bbc_Cfg Bbc_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Bbc_Cfg.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bbc_Cfg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define BBC_START_SEC_CONST_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define BBC_STOP_SEC_CONST_UNSPECIFIED
#include "Bbc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define BBC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define BBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define BBC_STOP_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define BBC_STOP_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (32BIT)**/


#define BBC_STOP_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define BBC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define BBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define BBC_STOP_SEC_VAR_NOINIT_32BIT
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define BBC_STOP_SEC_VAR_UNSPECIFIED
#include "Bbc_MemMap.h"
#define BBC_START_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/** Variable Section (32BIT)**/


#define BBC_STOP_SEC_VAR_32BIT
#include "Bbc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define BBC_START_SEC_CODE
#include "Bbc_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define BBC_STOP_SEC_CODE
#include "Bbc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
