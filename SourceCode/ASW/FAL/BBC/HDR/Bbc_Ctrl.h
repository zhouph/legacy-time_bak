/**
 * @defgroup Bbc_Ctrl Bbc_Ctrl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Bbc_Ctrl.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef BBC_CTRL_H_
#define BBC_CTRL_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bbc_Types.h"
#include "Bbc_Cfg.h"
#include "Bbc_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define BBC_CTRL_MODULE_ID      (0)
 #define BBC_CTRL_MAJOR_VERSION  (2)
 #define BBC_CTRL_MINOR_VERSION  (0)
 #define BBC_CTRL_PATCH_VERSION  (0)
 #define BBC_CTRL_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Bbc_Ctrl_HdrBusType Bbc_CtrlBus;


/* Internal Logic Bus */

extern BaseBrakeControl_SRCbusType    BBCsrcbus;



/* Version Info */
extern const SwcVersionInfo_t Bbc_CtrlVersionInfo;

/* Input Data Element */
extern Eem_MainEemFailData_t Bbc_CtrlEemFailData;
extern Proxy_RxCanRxIdbInfo_t Bbc_CtrlCanRxIdbInfo;
extern Det_1msCtrlBrkPedlStatus1msInfo_t Bbc_CtrlBrkPedlStatus1msInfo;
extern Det_5msCtrlBrkPedlStatusInfo_t Bbc_CtrlBrkPedlStatusInfo;
extern Spc_5msCtrlPedlSimrPFildInfo_t Bbc_CtrlPedlSimrPFildInfo;
extern Det_5msCtrlPedlTrvlRateInfo_t Bbc_CtrlPedlTrvlRateInfo;
extern Det_5msCtrlVehStopStInfo_t Bbc_CtrlVehStopStInfo;
extern Eem_MainEemSuspectData_t Bbc_CtrlEemSuspectData;
extern Mom_HndlrEcuModeSts_t Bbc_CtrlEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t Bbc_CtrlIgnOnOffSts;
extern Eem_SuspcDetnFuncInhibitBbcSts_t Bbc_CtrlFuncInhibitBbcSts;
extern Spc_5msCtrlBlsFild_t Bbc_CtrlBlsFild;
extern Rbc_CtrlBrkPRednForBaseBrkCtrlr_t Bbc_CtrlBrkPRednForBaseBrkCtrlr;
extern Spc_5msCtrlFlexBrkSwtFild_t Bbc_CtrlFlexBrkSwtFild;
extern Det_5msCtrlPedlTrvlFinal_t Bbc_CtrlPedlTrvlFinal;
extern Rbc_CtrlRgnBrkCtrlrActStFlg_t Bbc_CtrlRgnBrkCtrlrActStFlg;
extern Det_5msCtrlVehSpdFild_t Bbc_CtrlVehSpdFild;

/* Output Data Element */
extern Bbc_CtrlBaseBrkCtrlModInfo_t Bbc_CtrlBaseBrkCtrlModInfo;
extern Bbc_CtrlBBCCtrlInfo_t Bbc_CtrlBBCCtrlInfo;
extern Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo_t Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo;
extern Bbc_CtrlBaseBrkCtrlrActFlg_t Bbc_CtrlBaseBrkCtrlrActFlg;
extern Bbc_CtrlTarPFromBaseBrkCtrlr_t Bbc_CtrlTarPFromBaseBrkCtrlr;
extern Bbc_CtrlTarPRateFromBaseBrkCtrlr_t Bbc_CtrlTarPRateFromBaseBrkCtrlr;
extern Bbc_CtrlTarPFromBrkPedl_t Bbc_CtrlTarPFromBrkPedl;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Bbc_Ctrl_Init(void);
extern void Bbc_Ctrl(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* BBC_CTRL_H_ */
/** @} */
