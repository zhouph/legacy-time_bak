/**
 * @defgroup Bbc_Types Bbc_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Bbc_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef BBC_TYPES_H_
#define BBC_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Asw_Types.h" /* HSH */
#include "Bsw_Types.h"
#include "Rte_Types.h"
#include "RteDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Eem_MainEemFailData_t Bbc_CtrlEemFailData;
    Proxy_RxCanRxIdbInfo_t Bbc_CtrlCanRxIdbInfo;
    Det_1msCtrlBrkPedlStatus1msInfo_t Bbc_CtrlBrkPedlStatus1msInfo;
    Det_5msCtrlBrkPedlStatusInfo_t Bbc_CtrlBrkPedlStatusInfo;
    Spc_5msCtrlPedlSimrPFildInfo_t Bbc_CtrlPedlSimrPFildInfo;
    Det_5msCtrlPedlTrvlRateInfo_t Bbc_CtrlPedlTrvlRateInfo;
    Det_5msCtrlVehStopStInfo_t Bbc_CtrlVehStopStInfo;
    Eem_MainEemSuspectData_t Bbc_CtrlEemSuspectData;
    Mom_HndlrEcuModeSts_t Bbc_CtrlEcuModeSts;
    Prly_HndlrIgnOnOffSts_t Bbc_CtrlIgnOnOffSts;
    Eem_SuspcDetnFuncInhibitBbcSts_t Bbc_CtrlFuncInhibitBbcSts;
    Spc_5msCtrlBlsFild_t Bbc_CtrlBlsFild;
    Rbc_CtrlBrkPRednForBaseBrkCtrlr_t Bbc_CtrlBrkPRednForBaseBrkCtrlr;
    Spc_5msCtrlFlexBrkSwtFild_t Bbc_CtrlFlexBrkSwtFild;
    Det_5msCtrlPedlTrvlFinal_t Bbc_CtrlPedlTrvlFinal;
    Rbc_CtrlRgnBrkCtrlrActStFlg_t Bbc_CtrlRgnBrkCtrlrActStFlg;
    Det_5msCtrlVehSpdFild_t Bbc_CtrlVehSpdFild;

/* Output Data Element */
    Bbc_CtrlBaseBrkCtrlModInfo_t Bbc_CtrlBaseBrkCtrlModInfo;
    Bbc_CtrlBBCCtrlInfo_t Bbc_CtrlBBCCtrlInfo;
    Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo_t Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo;
    Bbc_CtrlBaseBrkCtrlrActFlg_t Bbc_CtrlBaseBrkCtrlrActFlg;
    Bbc_CtrlTarPFromBaseBrkCtrlr_t Bbc_CtrlTarPFromBaseBrkCtrlr;
    Bbc_CtrlTarPRateFromBaseBrkCtrlr_t Bbc_CtrlTarPRateFromBaseBrkCtrlr;
    Bbc_CtrlTarPFromBrkPedl_t Bbc_CtrlTarPFromBrkPedl;
}Bbc_Ctrl_HdrBusType;

typedef struct
{
	uint32_t RegenBrkEngageDelayFlg   			:1;
	uint32_t SpeedInvalidFlg			 		:1;
	uint32_t lcbbcu1PedalAppIgnOnFlg  			:1;
}BaseBrakeControl_SRCbusType;
/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* BBC_TYPES_H_ */
/** @} */
