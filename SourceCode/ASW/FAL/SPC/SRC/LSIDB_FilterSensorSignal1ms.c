/**
 * @defgroup LSIDB_FilterSensorSignal LSIDB_FilterSensorSignal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LSIDB_FilterSensorSignal1ms.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "LSIDB_FilterSensorSignal1ms.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define LPF_1_FIL_K_FC_20HZ_TS1MS   14
#define LPF_1_FIL_K_FC_15HZ_TS1MS   11
#define LPF_1_FIL_K_FC_10HZ_TS1MS   8


/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

typedef struct
{
	int16_t lsahbs16PDTRaw1ms;
	int16_t lsahbs16MCpresRaw1ms;
	int16_t lsahbs16MC2presRaw1ms;
	int16_t lsahbs16SystemOnCnt;
}GetSnsrSigFil1ms_t;/*Logger*/

typedef struct
{
    int16_t PSigRaw1ms_1_100Bar;
    int16_t PSigFilt1ms_1_100Bar;
    int16_t PSigFilt1ms_1_100Bar_Avg;
    int16_t PSigFilt1ms_Avg;
    int16_t PSigFilt1msBuf[5];
    int16_t PSigFilt1msBufCnt;
}press_filt_data_1ms_t;



/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SPC_1MSCTRL_START_SEC_CONST_UNSPECIFIED
#include "Spc_MemMap.h"
/** Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SPC_1MSCTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Spc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SPC_1MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
#define SPC_1MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spc_MemMap.h"

#define SPC_1MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Spc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define SPC_1MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Spc_MemMap.h"

#define SPC_1MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Spc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define SPC_1MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Spc_MemMap.h"

#define SPC_1MSCTRL_START_SEC_VAR_32BIT
#include "Spc_MemMap.h"
/** Variable Section (32BIT)**/
#define SPC_1MSCTRL_STOP_SEC_VAR_32BIT
#include "Spc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/


#define SPC_1MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

FAL_STATIC int16_t  s16PdtFilt1ms;

FAL_STATIC GetSnsrSigFil1ms_t GetSnsrSigFil1ms;

#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
FAL_STATIC press_filt_data_1ms_t PistP_Fil_1ms;
#elif  __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
FAL_STATIC press_filt_data_1ms_t PrimCircP_Fil_1ms;
FAL_STATIC press_filt_data_1ms_t SecdCircP_Fil_1ms;
#else
#endif


#define SPC_1MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spc_MemMap.h"

#define SPC_1MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Spc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/

#define SPC_1MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Spc_MemMap.h"

#define SPC_1MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Spc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define SPC_1MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Spc_MemMap.h"

#define SPC_1MSCTRL_START_SEC_VAR_32BIT
#include "Spc_MemMap.h"
/** Variable Section (32BIT)**/
#define SPC_1MSCTRL_STOP_SEC_VAR_32BIT
#include "Spc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SPC_1MSCTRL_START_SEC_CODE
#include "Spc_MemMap.h"


static void LSIDB_vFilPdt1msSig(void);
static void LSIDB_vFilP1msSig( press_filt_data_1ms_t *p_sPress_Fil_1ms);

static void LSIDB_vInpSigIfForSigFil1ms(void);
static void LSIDB_vOutpSigIfForSigFil1ms(void);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

void LSIDB_vCallSnsrSigFil1ms(void)
{
    LSIDB_vInpSigIfForSigFil1ms();

    LSIDB_vFilPdt1msSig();
#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
    LSIDB_vFilP1msSig((press_filt_data_1ms_t*)&PistP_Fil_1ms);
#elif  __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
    LSIDB_vFilP1msSig((press_filt_data_1ms_t *)&PrimCircP_Fil_1ms);
    LSIDB_vFilP1msSig((press_filt_data_1ms_t *)&SecdCircP_Fil_1ms);
#else
#endif
    LSIDB_vOutpSigIfForSigFil1ms();
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

static void LSIDB_vInpSigIfForSigFil1ms(void)
{
    GetSnsrSigFil1ms.lsahbs16SystemOnCnt	= (int16_t)SIGPROCsrcbus.SystemOnCnt1ms;
	GetSnsrSigFil1ms.lsahbs16PDTRaw1ms		= (int16_t)(Spc_1msCtrlBus.Spc_1msCtrlPdt1msRawInfo.PdtSig-SIGPROCsrcbus.PdtOffs_f);
#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
	GetSnsrSigFil1ms.lsahbs16MCpresRaw1ms	= (int16_t)(Spc_1msCtrlBus.Spc_1msCtrlPistP1msRawInfo.PistPSig-SIGPROCsrcbus.PistPOffs_f);
	PistP_Fil_1ms.PSigRaw1ms_1_100Bar		= GetSnsrSigFil1ms.lsahbs16MCpresRaw1ms;
#elif  __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
	GetSnsrSigFil1ms.lsahbs16MCpresRaw1ms	= (int16_t)(Spc_1msCtrlBus.Spc_1msCtrlCircP1msRawInfo.PrimCircPSig-SIGPROCsrcbus.PrimCircPOffs_f);
	GetSnsrSigFil1ms.lsahbs16MC2presRaw1ms	= (int16_t)(Spc_1msCtrlBus.Spc_1msCtrlCircP1msRawInfo.SecdCircPSig-SIGPROCsrcbus.SecdyCircPOffs_f);

	PrimCircP_Fil_1ms.PSigRaw1ms_1_100Bar	= GetSnsrSigFil1ms.lsahbs16MCpresRaw1ms;
	SecdCircP_Fil_1ms.PSigRaw1ms_1_100Bar	= GetSnsrSigFil1ms.lsahbs16MC2presRaw1ms;
#else
#endif
}

static void LSIDB_vOutpSigIfForSigFil1ms(void)
{
	Spc_1msCtrlBus.Spc_1msCtrlPedlTrvlFild1msInfo.PdtFild1ms   = s16PdtFilt1ms;

#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
	Spc_1msCtrlBus.Spc_1msCtrlPistPFild1msInfo.PistPFild1ms= PistP_Fil_1ms.PSigFilt1ms_1_100Bar;
    SIGPROCsrcbus.PistPFild1ms_1_100Bar_Avg = PistP_Fil_1ms.PSigFilt1ms_1_100Bar_Avg;/* selective 1ms or 5ms filtered sig*/
#elif  __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
    Spc_1msCtrlBus.Spc_1msCtrlCircPFild1msInfo.PrimCircPFild1ms= PrimCircP_Fil_1ms.PSigFilt1ms_1_100Bar;
    Spc_1msCtrlBus.Spc_1msCtrlCircPFild1msInfo.SecdCircPFild1ms= SecdCircP_Fil_1ms.PSigFilt1ms_1_100Bar;
    
    SIGPROCsrcbus.PrimCircPFild1ms_1_100Bar_Avg = PrimCircP_Fil_1ms.PSigFilt1ms_1_100Bar_Avg; /* selective 1ms or 5ms filtered sig*/
    SIGPROCsrcbus.SecdCircPFild1ms_1_100Bar_Avg = SecdCircP_Fil_1ms.PSigFilt1ms_1_100Bar_Avg;/* selective 1ms or 5ms filtered sig*/
#else
#endif

}

static void LSIDB_vFilPdt1msSig(void)
{
    uint8_t  U8PDTfiltCoeff=LPF_1_FIL_K_FC_20HZ_TS1MS;/*  14 = 20 Hz    */

    if(GetSnsrSigFil1ms.lsahbs16SystemOnCnt<=50)
    {
        s16PdtFilt1ms   = GetSnsrSigFil1ms.lsahbs16PDTRaw1ms;
    }
    else
    {
        s16PdtFilt1ms    = L_s16Lpf1Int1ms( GetSnsrSigFil1ms.lsahbs16PDTRaw1ms, s16PdtFilt1ms, U8PDTfiltCoeff );
    }
    
    s16PdtFilt1ms    = L_s16LimitMinMax1ms( s16PdtFilt1ms, S16_TRAVEL_0_MM, S16_TRAVEL_155_MM );
}

static void LSIDB_vFilP1msSig( press_filt_data_1ms_t *p_sPress_Fil_1ms)
{
    uint8_t  U8MCPfiltCoeff = LPF_1_FIL_K_FC_10HZ_TS1MS;

    if(GetSnsrSigFil1ms.lsahbs16SystemOnCnt<=50)
    {
    	p_sPress_Fil_1ms->PSigFilt1ms_1_100Bar= (p_sPress_Fil_1ms->PSigRaw1ms_1_100Bar) ;
    }
    else
    {
    	p_sPress_Fil_1ms->PSigFilt1ms_1_100Bar = L_s16Lpf1Int1ms( (p_sPress_Fil_1ms->PSigRaw1ms_1_100Bar), p_sPress_Fil_1ms->PSigFilt1ms_1_100Bar, U8MCPfiltCoeff );
    }

    p_sPress_Fil_1ms->PSigFilt1ms_1_100Bar = L_s16LimitMinMax1ms( p_sPress_Fil_1ms->PSigFilt1ms_1_100Bar, S16_CIRC_PRESS_0_BAR, M_P_SENSOR_MAX_PRESSURE );
    p_sPress_Fil_1ms->PSigFilt1msBuf[p_sPress_Fil_1ms->PSigFilt1msBufCnt]=p_sPress_Fil_1ms->PSigFilt1ms_1_100Bar;
    p_sPress_Fil_1ms->PSigFilt1msBufCnt++;

    if(p_sPress_Fil_1ms->PSigFilt1msBufCnt>=5)
    {
    	p_sPress_Fil_1ms->PSigFilt1msBufCnt=0;
    }
    p_sPress_Fil_1ms->PSigFilt1ms_1_100Bar_Avg = (int16_t)(((int32_t)p_sPress_Fil_1ms->PSigFilt1msBuf[0]
                                                               + p_sPress_Fil_1ms->PSigFilt1msBuf[1]
                                                               + p_sPress_Fil_1ms->PSigFilt1msBuf[2]
                                                               + p_sPress_Fil_1ms->PSigFilt1msBuf[3]
                                                               + p_sPress_Fil_1ms->PSigFilt1msBuf[4]) / 5);
    p_sPress_Fil_1ms->PSigFilt1ms_Avg      = p_sPress_Fil_1ms->PSigFilt1ms_1_100Bar_Avg;
}

#define SPC_1MSCTRL_STOP_SEC_CODE
#include "Spc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
