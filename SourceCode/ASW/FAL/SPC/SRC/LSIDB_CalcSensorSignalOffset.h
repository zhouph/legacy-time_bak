/**
 * @defgroup LSIDB_CalcSensorSignalOffset LSIDB_CalcSensorSignalOffset
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LSIDB_CalcSensorSignalOffset.h
 * @brief       Local Header File
 * @date        20YY. MM. DD.
 ******************************************************************************/
 
#ifndef LSIDB_CALCSENSORSIGNALOFFSET_H_
#define LSIDB_CALCSENSORSIGNALOFFSET_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Spc_5msCtrl.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void LSIDB_vCallSnsrOffsCalcn(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* LSIDB_CALCSENSORSIGNALOFFSET_H_ */
/** @} */
