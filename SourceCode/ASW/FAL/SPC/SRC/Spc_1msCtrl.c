/**
 * @defgroup Spc_1msCtrl Spc_1msCtrl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spc_1msCtrl.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Spc_1msCtrl.h"
#include "Spc_1msCtrl_Ifa.h"
#include "IfxStm_reg.h"
#include "LSIDB_FilterSensorSignal.h"
#include "LSIDB_FilterSensorSignal1ms.h"
#include "LSIDB_CalcSensorSignalOffset.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SPC_1MSCTRL_START_SEC_CONST_UNSPECIFIED
#include "Spc_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define SPC_1MSCTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Spc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SPC_1MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Spc_1msCtrl_HdrBusType Spc_1msCtrlBus;

/* Version Info */
const SwcVersionInfo_t Spc_1msCtrlVersionInfo = 
{   
    SPC_1MSCTRL_MODULE_ID,           /* Spc_1msCtrlVersionInfo.ModuleId */
    SPC_1MSCTRL_MAJOR_VERSION,       /* Spc_1msCtrlVersionInfo.MajorVer */
    SPC_1MSCTRL_MINOR_VERSION,       /* Spc_1msCtrlVersionInfo.MinorVer */
    SPC_1MSCTRL_PATCH_VERSION,       /* Spc_1msCtrlVersionInfo.PatchVer */
    SPC_1MSCTRL_BRANCH_VERSION       /* Spc_1msCtrlVersionInfo.BranchVer */
};
    
/* Input Data Element */
Press_SenCircP1msRawInfo_t Spc_1msCtrlCircP1msRawInfo;
Press_SenPistP1msRawInfo_t Spc_1msCtrlPistP1msRawInfo;
Pedal_SenPdt1msRawInfo_t Spc_1msCtrlPdt1msRawInfo;
Mom_HndlrEcuModeSts_t Spc_1msCtrlEcuModeSts;

/* Output Data Element */
Spc_1msCtrlCircPFild1msInfo_t Spc_1msCtrlCircPFild1msInfo;
Spc_1msCtrlPedlTrvlFild1msInfo_t Spc_1msCtrlPedlTrvlFild1msInfo;
Spc_1msCtrlPistPFild1msInfo_t Spc_1msCtrlPistPFild1msInfo;

uint32 Spc_1msCtrl_Timer_Start;
uint32 Spc_1msCtrl_Timer_Elapsed;

#define SPC_1MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spc_MemMap.h"
#define SPC_1MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Spc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SPC_1MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Spc_MemMap.h"
#define SPC_1MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Spc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SPC_1MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Spc_MemMap.h"
#define SPC_1MSCTRL_START_SEC_VAR_32BIT
#include "Spc_MemMap.h"
/** Variable Section (32BIT)**/


#define SPC_1MSCTRL_STOP_SEC_VAR_32BIT
#include "Spc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SPC_1MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SPC_1MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spc_MemMap.h"
#define SPC_1MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Spc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SPC_1MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Spc_MemMap.h"
#define SPC_1MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Spc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SPC_1MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Spc_MemMap.h"
#define SPC_1MSCTRL_START_SEC_VAR_32BIT
#include "Spc_MemMap.h"
/** Variable Section (32BIT)**/


#define SPC_1MSCTRL_STOP_SEC_VAR_32BIT
#include "Spc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SPC_1MSCTRL_START_SEC_CODE
#include "Spc_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Spc_1msCtrl_Init(void)
{
    /* Initialize internal bus */
    Spc_1msCtrlBus.Spc_1msCtrlCircP1msRawInfo.PrimCircPSig = 0;
    Spc_1msCtrlBus.Spc_1msCtrlCircP1msRawInfo.SecdCircPSig = 0;
    Spc_1msCtrlBus.Spc_1msCtrlPistP1msRawInfo.PistPSig = 0;
    Spc_1msCtrlBus.Spc_1msCtrlPdt1msRawInfo.PdtSig = 0;
    Spc_1msCtrlBus.Spc_1msCtrlEcuModeSts = 0;
    Spc_1msCtrlBus.Spc_1msCtrlCircPFild1msInfo.PrimCircPFild1ms = 0;
    Spc_1msCtrlBus.Spc_1msCtrlCircPFild1msInfo.SecdCircPFild1ms = 0;
    Spc_1msCtrlBus.Spc_1msCtrlPedlTrvlFild1msInfo.PdtFild1ms = 0;
    Spc_1msCtrlBus.Spc_1msCtrlPistPFild1msInfo.PistPFild1ms = 0;
}

void Spc_1msCtrl(void)
{
    Spc_1msCtrl_Timer_Start = STM0_TIM0.U;

    /* Input */
    Spc_1msCtrl_Read_Spc_1msCtrlCircP1msRawInfo(&Spc_1msCtrlBus.Spc_1msCtrlCircP1msRawInfo);
    /*==============================================================================
    * Members of structure Spc_1msCtrlCircP1msRawInfo 
     : Spc_1msCtrlCircP1msRawInfo.PrimCircPSig;
     : Spc_1msCtrlCircP1msRawInfo.SecdCircPSig;
     =============================================================================*/
    
    Spc_1msCtrl_Read_Spc_1msCtrlPistP1msRawInfo(&Spc_1msCtrlBus.Spc_1msCtrlPistP1msRawInfo);
    /*==============================================================================
    * Members of structure Spc_1msCtrlPistP1msRawInfo 
     : Spc_1msCtrlPistP1msRawInfo.PistPSig;
     =============================================================================*/
    
    Spc_1msCtrl_Read_Spc_1msCtrlPdt1msRawInfo(&Spc_1msCtrlBus.Spc_1msCtrlPdt1msRawInfo);
    /*==============================================================================
    * Members of structure Spc_1msCtrlPdt1msRawInfo 
     : Spc_1msCtrlPdt1msRawInfo.PdtSig;
     =============================================================================*/
    
    Spc_1msCtrl_Read_Spc_1msCtrlEcuModeSts(&Spc_1msCtrlBus.Spc_1msCtrlEcuModeSts);
    if(SIGPROCsrcbus.SystemOnCnt1ms<10000)
    {
        SIGPROCsrcbus.SystemOnCnt1ms++;
    }
    /* Process */
    LSIDB_vCallSnsrSigFil1ms();

    /* Output */
    Spc_1msCtrl_Write_Spc_1msCtrlCircPFild1msInfo(&Spc_1msCtrlBus.Spc_1msCtrlCircPFild1msInfo);
    /*==============================================================================
    * Members of structure Spc_1msCtrlCircPFild1msInfo 
     : Spc_1msCtrlCircPFild1msInfo.PrimCircPFild1ms;
     : Spc_1msCtrlCircPFild1msInfo.SecdCircPFild1ms;
     =============================================================================*/
    
    Spc_1msCtrl_Write_Spc_1msCtrlPedlTrvlFild1msInfo(&Spc_1msCtrlBus.Spc_1msCtrlPedlTrvlFild1msInfo);
    /*==============================================================================
    * Members of structure Spc_1msCtrlPedlTrvlFild1msInfo 
     : Spc_1msCtrlPedlTrvlFild1msInfo.PdtFild1ms;
     =============================================================================*/
    
    Spc_1msCtrl_Write_Spc_1msCtrlPistPFild1msInfo(&Spc_1msCtrlBus.Spc_1msCtrlPistPFild1msInfo);
    /*==============================================================================
    * Members of structure Spc_1msCtrlPistPFild1msInfo 
     : Spc_1msCtrlPistPFild1msInfo.PistPFild1ms;
     =============================================================================*/
    

    Spc_1msCtrl_Timer_Elapsed = STM0_TIM0.U - Spc_1msCtrl_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define SPC_1MSCTRL_STOP_SEC_CODE
#include "Spc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
