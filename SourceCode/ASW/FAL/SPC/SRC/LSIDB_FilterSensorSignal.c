/**
 * @defgroup LSIDB_FilterSensorSignal LSIDB_FilterSensorSignal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LSIDB_FilterSensorSignal.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "LSIDB_FilterSensorSignal.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

#define  BLS_RESET_COUNT          	U8_T_50_MS
#define FLEX_BRK_RESET_COUNT		U8_T_50_MS

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
	int16_t lsahbs16PDTRaw;
	int16_t lsahbs16PDFRaw;
	int16_t lsahbu8PDTSigTag;
	int16_t lsahbu8PDFSigTag;
	int16_t lsahbu1PDTNoiseSuspectFlgH;
	int16_t lsahbu1PDFNoiseSuspectFlgH;

	int16_t lsahbs16MCpresRaw;
	int16_t lsahbs16MC2presRaw;

	int16_t lsahbs16PSpresRaw;
	int16_t lsahbu1BrakeLightSigRaw;
	int16_t lsahbs16WheelSpeedFlRaw;
	int16_t lsahbs16WheelSpeedFrRaw;
	int16_t lsahbs16WheelSpeedRlRaw;
	int16_t lsahbs16WheelSpeedRrRaw;

	int16_t lsahbs16SystemOnCnt;

#if (__FLEX_BRAKING_MODE == ENABLE)
	int16_t lsahbu1FlexBrkSwitchRaw1;
	int16_t lsahbu1FlexBrkSwitchRaw2;
	int16_t fu1FlexBrakeSwitchFaultDet;
#endif
}GetSnsrSigFil_t;


typedef struct
{
    int16_t PSigRaw5ms;
    int16_t PSigFilt5ms;
    int16_t PSigFilt5ms_1_100Bar;
}press_filt_data_t;



/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SPC_5MSCTRL_START_SEC_CONST_UNSPECIFIED
#include "Spc_MemMap.h"
/** Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SPC_5MSCTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Spc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SPC_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
#define SPC_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spc_MemMap.h"

#define SPC_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Spc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define SPC_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Spc_MemMap.h"

#define SPC_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Spc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define SPC_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Spc_MemMap.h"

#define SPC_5MSCTRL_START_SEC_VAR_32BIT
#include "Spc_MemMap.h"
/** Variable Section (32BIT)**/
#define SPC_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Spc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/


#define SPC_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

FAL_STATIC int16_t  s16PDTFilt;
FAL_STATIC int16_t  s16PDFFilt;

FAL_STATIC int16_t  s16PSpresFilt;
FAL_STATIC int16_t  s16PSpresFilt_1_100Bar;

FAL_STATIC uint8_t  lsahbu8BLSResetCount;


FAL_STATIC int16_t  s16WheelSpeedFildFL;
FAL_STATIC int16_t  s16WheelSpeedFildFR;
FAL_STATIC int16_t  s16WheelSpeedFildRL;
FAL_STATIC int16_t  s16WheelSpeedFildRR;

#if (__FLEX_BRAKING_MODE == ENABLE)
FAL_STATIC uint8_t  lsahbu8FlexBrakeSwitch;
FAL_STATIC uint8_t  lsahbu8FlexBrkSwitModeRaw;
FAL_STATIC uint8_t  lsahbu8FlexBrkSwitModeRawOld;
FAL_STATIC uint8_t  lsahbu8FlexBrakeCount;
#endif

FAL_STATIC GetSnsrSigFil_t GetSnsrSigFil;
FAL_STATIC press_filt_data_t *pPress_Fil;
#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
FAL_STATIC press_filt_data_t PistP_Fil;
#elif  __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
FAL_STATIC press_filt_data_t PrimCircP_Fil;
FAL_STATIC press_filt_data_t SecdCircP_Fil;
#else
#endif

FAL_STATIC int32_t lsahbu1BLS;
FAL_STATIC int32_t lsahbu1BLSold;
FAL_STATIC int32_t lsahbu1BLSchgCheck;

#define SPC_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spc_MemMap.h"

#define SPC_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Spc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/

#define SPC_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Spc_MemMap.h"

#define SPC_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Spc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define SPC_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Spc_MemMap.h"

#define SPC_5MSCTRL_START_SEC_VAR_32BIT
#include "Spc_MemMap.h"
/** Variable Section (32BIT)**/
#define SPC_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Spc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SPC_5MSCTRL_START_SEC_CODE
#include "Spc_MemMap.h"

static void LSIDB_vInpSigIfForSigFil(void);
static void LSIDB_vOutpSigIfForSigFil(void);
static void LSIDB_vCallPedlSnsrSigFil(void);
static void LSIDB_vFilPdt5msSig(void);
static void LSIDB_vFilPdf5msSig(void);
static void LSIDB_vCallPSnsrSigFil(void);
static void LSIDB_vFilP5msSig( press_filt_data_t *p_sPress_Fil);
static void LSIDB_vFilPedlSimrP5msSig(void);
static void LSIDB_vCallSwtSigFil(void);
static void LSIDB_vFilWhlSpdSig(void);
static void LSIDB_vFilBlsSig(void);

#if (__FLEX_BRAKING_MODE == ENABLE)
static void LSIDB_vFilFlexBrkSig(void);
#endif


/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void LSIDB_vCallSnsrSigFil(void)
{
    LSIDB_vInpSigIfForSigFil();

    LSIDB_vCallPedlSnsrSigFil();
    LSIDB_vCallPSnsrSigFil();
    LSIDB_vCallSwtSigFil();
    LSIDB_vFilWhlSpdSig();

    LSIDB_vOutpSigIfForSigFil();
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static void LSIDB_vInpSigIfForSigFil(void)
{
	GetSnsrSigFil.lsahbs16PDTRaw	= (int16_t)Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdtRawOffsCorrd;
	GetSnsrSigFil.lsahbs16PDFRaw	= (int16_t)Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdfRawOffsCorrd;
	GetSnsrSigFil.lsahbu8PDTSigTag	= (int16_t)Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlTagInfo.PdtSigTag;
	GetSnsrSigFil.lsahbu8PDFSigTag	= (int16_t)Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlTagInfo.PdfSigTag;
	GetSnsrSigFil.lsahbu1PDTNoiseSuspectFlgH	= (int16_t)Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlTagInfo.PdtSigNoiseSuspcFlgH;
	GetSnsrSigFil.lsahbu1PDFNoiseSuspectFlgH	= (int16_t)Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlTagInfo.PdfSigNoiseSuspcFlgH;

#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE

	GetSnsrSigFil.lsahbs16MCpresRaw	= (int16_t)Spc_5msCtrlBus.Spc_5msCtrlPistPOffsCorrdInfo.PistPOffsCorrd;
#elif  __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
	GetSnsrSigFil.lsahbs16MCpresRaw		= (int16_t)Spc_5msCtrlBus.Spc_5msCtrlCircPOffsCorrdInfo.PrimCircPOffsCorrd;
	GetSnsrSigFil.lsahbs16MC2presRaw	= (int16_t)Spc_5msCtrlBus.Spc_5msCtrlCircPOffsCorrdInfo.SecdCircPOffsCorrd;

#else
#endif
	GetSnsrSigFil.lsahbs16PSpresRaw		= (int16_t)Spc_5msCtrlBus.Spc_5msCtrlPedlSimrPOffsCorrdInfo.PedlSimrPOffsCorrd;
	GetSnsrSigFil.lsahbu1BrakeLightSigRaw	= (int16_t)Spc_5msCtrlBus.Spc_5msCtrlBlsSwt;
	GetSnsrSigFil.lsahbs16WheelSpeedFrRaw	= (int16_t)Spc_5msCtrlBus.Spc_5msCtrlWhlSpdInfo.FrWhlSpd/8;
	GetSnsrSigFil.lsahbs16WheelSpeedRlRaw	= (int16_t)Spc_5msCtrlBus.Spc_5msCtrlWhlSpdInfo.RlWhlSpd/8;
	GetSnsrSigFil.lsahbs16WheelSpeedFlRaw	= (int16_t)Spc_5msCtrlBus.Spc_5msCtrlWhlSpdInfo.FlWhlSpd/8;
	GetSnsrSigFil.lsahbs16WheelSpeedRrRaw	= (int16_t)Spc_5msCtrlBus.Spc_5msCtrlWhlSpdInfo.RrlWhlSpd/8;

#if (__FLEX_BRAKING_MODE == ENABLE)
	GetSnsrSigFil.lsahbu1FlexBrkSwitchRaw1	= (int16_t)Spc_5msCtrlBus.Spc_5msCtrlSwtStsFlexBrkInfo.FlexBrkASwt;
	GetSnsrSigFil.lsahbu1FlexBrkSwitchRaw2	= (int16_t)Spc_5msCtrlBus.Spc_5msCtrlSwtStsFlexBrkInfo.FlexBrkBSwt;
	GetSnsrSigFil.fu1FlexBrakeSwitchFaultDet	= (int16_t)Spc_5msCtrlBus.Spc_5msCtrlSwtStsFSInfo.FlexBrkSwtFaultDet;
#endif
	GetSnsrSigFil.lsahbs16SystemOnCnt	= (int16_t)SIGPROCsrcbus.SystemOnCnt;
#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
    PistP_Fil.PSigRaw5ms            = GetSnsrSigFil.lsahbs16MCpresRaw;
#elif  __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
    PrimCircP_Fil.PSigRaw5ms            = GetSnsrSigFil.lsahbs16MCpresRaw;
    SecdCircP_Fil.PSigRaw5ms            = GetSnsrSigFil.lsahbs16MC2presRaw;
#else
#endif
}

static void LSIDB_vOutpSigIfForSigFil(void)
{
    Spc_5msCtrlBus.Spc_5msCtrlWhlSpdFildInfo.WhlSpdFildFrntLe  = s16WheelSpeedFildFL;
    Spc_5msCtrlBus.Spc_5msCtrlWhlSpdFildInfo.WhlSpdFildFrntRi  = s16WheelSpeedFildFR;
    Spc_5msCtrlBus.Spc_5msCtrlWhlSpdFildInfo.WhlSpdFildReLe    = s16WheelSpeedFildRL;
    Spc_5msCtrlBus.Spc_5msCtrlWhlSpdFildInfo.WhlSpdFildReRi    = s16WheelSpeedFildRR;

#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
    Spc_5msCtrlBus.Spc_5msCtrlPistPFildInfo.PistPFild          	   = PistP_Fil.PSigFilt5ms;
    Spc_5msCtrlBus.Spc_5msCtrlPistPFildInfo.PistPFild_1_100Bar     = SIGPROCsrcbus.PistPFild1ms_1_100Bar_Avg;
#elif  __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
    /*5ms 1ms sig selective*/
    Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.PrimCircPFild          = PrimCircP_Fil.PSigFilt5ms;
    Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar = SIGPROCsrcbus.PrimCircPFild1ms_1_100Bar_Avg;
    Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.SecdCircPFild          = SecdCircP_Fil.PSigFilt5ms;
    Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar = SIGPROCsrcbus.SecdCircPFild1ms_1_100Bar_Avg;
#else
#endif
    Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlFildInfo.PdfFild         = s16PDFFilt;
    Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlFildInfo.PdtFild         = s16PDTFilt;

    Spc_5msCtrlBus.Spc_5msCtrlPedlSimrPFildInfo.PedlSimrPFild = s16PSpresFilt;
    Spc_5msCtrlBus.Spc_5msCtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar = s16PSpresFilt_1_100Bar;
    Spc_5msCtrlBus.Spc_5msCtrlBlsFild                          = lsahbu1BLS;
#if (__FLEX_BRAKING_MODE == ENABLE)
    Spc_5msCtrlBus.Spc_5msCtrlFlexBrkSwtFild              = lsahbu8FlexBrakeSwitch;
#endif
}

static void LSIDB_vCallPedlSnsrSigFil(void)
{
    LSIDB_vFilPdt5msSig();
    LSIDB_vFilPdf5msSig();

}

static void LSIDB_vFilPdt5msSig(void)
{
    uint8_t  U8PDTfiltCoeff;

    if(GetSnsrSigFil.lsahbs16SystemOnCnt<=U8_T_50_MS)
    {
        s16PDTFilt     = GetSnsrSigFil.lsahbs16PDTRaw ;
    }
    else
    {
        if(s16PDTFilt > S16_TRAVEL_5_MM)
        {
            if((GetSnsrSigFil.lsahbu8PDTSigTag == 40) && (GetSnsrSigFil.lsahbu1PDTNoiseSuspectFlgH==1) )
            {
                U8PDTfiltCoeff = U8_LPF_3HZ_128G;            /* 3 Hz  */
            }
            else if(GetSnsrSigFil.lsahbu8PDTSigTag == 23 )
            {
                U8PDTfiltCoeff = U8_LPF_3HZ_128G;          /* 3 Hz   */
            }
            else
            {
                U8PDTfiltCoeff =U8_LPF_5HZ_128G;          /*  5 Hz   */
            }
        }
        else
        {
            U8PDTfiltCoeff = U8_LPF_5HZ_128G;            /*  5 Hz    */
        }
        s16PDTFilt         = L_s16Lpf1Int( GetSnsrSigFil.lsahbs16PDTRaw, s16PDTFilt   , U8PDTfiltCoeff );
    }
    s16PDTFilt    =L_s16LimitMinMax( s16PDTFilt, S16_TRAVEL_0_MM, S16_TRAVEL_155_MM );
}

static void LSIDB_vFilPdf5msSig(void)
{
        uint8_t  U8PDFfiltCoeff;

        if(GetSnsrSigFil.lsahbs16SystemOnCnt<=U8_T_50_MS)
        {
            s16PDFFilt     = GetSnsrSigFil.lsahbs16PDFRaw ;
        }
        else
        {
            if(s16PDFFilt > S16_TRAVEL_10_MM)
            {
                if((GetSnsrSigFil.lsahbu8PDFSigTag == 50) && (GetSnsrSigFil.lsahbu1PDFNoiseSuspectFlgH==1) )
                {
                    U8PDFfiltCoeff = U8_LPF_3HZ_128G;            /* 3 Hz  */
                }
                else if(GetSnsrSigFil.lsahbu8PDFSigTag == 27 )
                {
                    U8PDFfiltCoeff = U8_LPF_3HZ_128G;           /* 3 Hz   */
                }
                else
                {
                    U8PDFfiltCoeff = U8_LPF_5HZ_128G;          /*  5 Hz   */
                }
            }
            else
            {
                U8PDFfiltCoeff = U8_LPF_5HZ_128G;             /*  5 Hz    */
            }
            s16PDFFilt         = L_s16Lpf1Int( GetSnsrSigFil.lsahbs16PDFRaw, s16PDFFilt   , U8PDFfiltCoeff );
        }
        s16PDFFilt    =L_s16LimitMinMax( s16PDFFilt, S16_TRAVEL_0_MM, S16_TRAVEL_155_MM );
}

static void LSIDB_vCallPSnsrSigFil(void)
{
#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
    LSIDB_vFilP5msSig((press_filt_data_t*)&PistP_Fil);
#elif  __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
    LSIDB_vFilP5msSig((press_filt_data_t *)&PrimCircP_Fil);

    LSIDB_vFilP5msSig((press_filt_data_t *)&SecdCircP_Fil);
#else
#endif
    LSIDB_vFilPedlSimrP5msSig();
}


static void LSIDB_vFilP5msSig( press_filt_data_t *p_sPress_Fil)
{
    if(GetSnsrSigFil.lsahbs16SystemOnCnt<=U8_T_50_MS)
    {
    	p_sPress_Fil->PSigFilt5ms_1_100Bar = (p_sPress_Fil->PSigRaw5ms ) ;
         /* 7Hz  */
    }
    else
    {
    	p_sPress_Fil->PSigFilt5ms_1_100Bar = L_s16Lpf1Int( (p_sPress_Fil->PSigRaw5ms), p_sPress_Fil->PSigFilt5ms_1_100Bar, U8_LPF_7HZ_128G);           /* 7Hz  */
    }
    p_sPress_Fil->PSigFilt5ms =L_s16LimitMinMax( p_sPress_Fil->PSigFilt5ms_1_100Bar, S16_CIRC_PRESS_0_BAR, M_P_SENSOR_MAX_PRESSURE );
}

static void LSIDB_vFilPedlSimrP5msSig(void)
{
    if(GetSnsrSigFil.lsahbs16SystemOnCnt<=U8_T_50_MS)
    {
        s16PSpresFilt_1_100Bar = GetSnsrSigFil.lsahbs16PSpresRaw;          /* 7Hz  */
    }
    else
    {
        s16PSpresFilt_1_100Bar = L_s16Lpf1Int( GetSnsrSigFil.lsahbs16PSpresRaw, s16PSpresFilt_1_100Bar, 28 );          /* 7Hz  */
    }

    /*s16PSpresFilt = s16PSpresFilt_1_100Bar/10;*/
    s16PSpresFilt_1_100Bar =L_s16LimitMinMax(s16PSpresFilt_1_100Bar, S16_CIRC_PRESS_0_BAR, M_P_SENSOR_MAX_PRESSURE );
    s16PSpresFilt = s16PSpresFilt_1_100Bar;
}

static void LSIDB_vCallSwtSigFil(void)
{
    LSIDB_vFilBlsSig();
#if (__FLEX_BRAKING_MODE == ENABLE)
    LSIDB_vFilFlexBrkSig();
#endif
}

static void LSIDB_vFilBlsSig(void)
{
    lsahbu1BLSold = lsahbu1BLS;

    if(lsahbu1BLS==1)
    {

        if(GetSnsrSigFil.lsahbu1BrakeLightSigRaw==0 )
        {
            lsahbu8BLSResetCount++;
            if(lsahbu8BLSResetCount >= BLS_RESET_COUNT )
            {
                lsahbu1BLS=0;
            }
            else
            {
                ;
            }
        }
        else
        {
            lsahbu8BLSResetCount=0;
        }
    }
    else
    {
        if(GetSnsrSigFil.lsahbu1BrakeLightSigRaw==1)
        {
            lsahbu1BLS=1;
        }
        else
        {
            ;
        }
        lsahbu8BLSResetCount=0;
    }

    /* BLS Check Condition 0->1 */
    if( lsahbu1BLSchgCheck == 0  )
    {
        if( (lsahbu1BLSold==0) && (lsahbu1BLS==1) )
        {
            lsahbu1BLSchgCheck = 1;
        }
        else
        {
            ;
        }
    }
    else
    {
        ;
    }
}

#if (__FLEX_BRAKING_MODE == ENABLE)
static void LSIDB_vFilFlexBrkSig(void)
{
    if(fu1FlexBrakeSwitchFaultDet == 0)
    {

        if((lsahbu1FlexBrkSwitchRaw1 == 0)&&(lsahbu1FlexBrkSwitchRaw2==1))
        {
            lsahbu8FlexBrkSwitModeRaw = 1;
        }
        else if((lsahbu1FlexBrkSwitchRaw1 == 1)&&(lsahbu1FlexBrkSwitchRaw2==0))
        {
            lsahbu8FlexBrkSwitModeRaw = 2;
        }
        else
        {
            lsahbu8FlexBrkSwitModeRaw = 0;
        }

        if(lsahbu8FlexBrkSwitModeRawOld == lsahbu8FlexBrkSwitModeRaw)
        {
            if(lsahbu8FlexBrakeCount < FLEX_BRK_RESET_COUNT)
            {
                lsahbu8FlexBrakeCount=lsahbu8FlexBrakeCount+1;
            }
            else
            {
                lsahbu8FlexBrakeSwitch = lsahbu8FlexBrkSwitModeRaw;
            }
        }
        else
        {
            lsahbu8FlexBrakeCount=0;
        }
    }
    else
    {
        lsahbu8FlexBrakeSwitch=0;
    }

    lsahbu8FlexBrkSwitModeRawOld = lsahbu8FlexBrkSwitModeRaw;
}
#endif
static void LSIDB_vFilWhlSpdSig(void)
{
    if(GetSnsrSigFil.lsahbs16SystemOnCnt<=U8_T_50_MS)
    {
        s16WheelSpeedFildFL = (int16_t)GetSnsrSigFil.lsahbs16WheelSpeedFlRaw;
        s16WheelSpeedFildFR = (int16_t)GetSnsrSigFil.lsahbs16WheelSpeedFrRaw;
        s16WheelSpeedFildRL = (int16_t)GetSnsrSigFil.lsahbs16WheelSpeedRlRaw;
        s16WheelSpeedFildRR = (int16_t)GetSnsrSigFil.lsahbs16WheelSpeedRrRaw;
    }
    else
    {
        /*== FL */
        if(GetSnsrSigFil.lsahbs16WheelSpeedFlRaw > S16_SPEED_1_KPH )
        {
            s16WheelSpeedFildFL = L_s16Lpf1Int(GetSnsrSigFil.lsahbs16WheelSpeedFlRaw,s16WheelSpeedFildFL,49); /* 20Hz  */
        }
        else
        {
            s16WheelSpeedFildFL = L_s16Lpf1Int(GetSnsrSigFil.lsahbs16WheelSpeedFlRaw,s16WheelSpeedFildFL,71); /* 40Hz  */
        }
        /*== FR */
        if(GetSnsrSigFil.lsahbs16WheelSpeedFrRaw > S16_SPEED_1_KPH )
        {
            s16WheelSpeedFildFR = L_s16Lpf1Int(GetSnsrSigFil.lsahbs16WheelSpeedFrRaw,s16WheelSpeedFildFR,49); /* 20Hz  */
        }
        else
        {
            s16WheelSpeedFildFR = L_s16Lpf1Int(GetSnsrSigFil.lsahbs16WheelSpeedFrRaw,s16WheelSpeedFildFR,71); /* 40Hz  */
        }
        /*== RL */
        if(GetSnsrSigFil.lsahbs16WheelSpeedRlRaw > S16_SPEED_1_KPH )
        {
            s16WheelSpeedFildRL = L_s16Lpf1Int(GetSnsrSigFil.lsahbs16WheelSpeedRlRaw,s16WheelSpeedFildRL,49); /* 20Hz  */
        }
        else
        {
            s16WheelSpeedFildRL = L_s16Lpf1Int(GetSnsrSigFil.lsahbs16WheelSpeedRlRaw,s16WheelSpeedFildRL,71); /* 40Hz  */
        }
        /*== RR */
        if(GetSnsrSigFil.lsahbs16WheelSpeedRrRaw > S16_SPEED_1_KPH )
        {
            s16WheelSpeedFildRR = L_s16Lpf1Int(GetSnsrSigFil.lsahbs16WheelSpeedRrRaw,s16WheelSpeedFildRR,49); /* 20Hz  */
        }
        else
        {
            s16WheelSpeedFildRR = L_s16Lpf1Int(GetSnsrSigFil.lsahbs16WheelSpeedRrRaw,s16WheelSpeedFildRR,71); /* 40Hz  */
        }
    }

    s16WheelSpeedFildFL = L_s16LimitMinMax(s16WheelSpeedFildFL, S16_SPEED_MIN, S16_SPEED_MAX);
    s16WheelSpeedFildFR = L_s16LimitMinMax(s16WheelSpeedFildFR, S16_SPEED_MIN, S16_SPEED_MAX);
    s16WheelSpeedFildRL = L_s16LimitMinMax(s16WheelSpeedFildRL, S16_SPEED_MIN, S16_SPEED_MAX);
    s16WheelSpeedFildRR = L_s16LimitMinMax(s16WheelSpeedFildRR, S16_SPEED_MIN, S16_SPEED_MAX);
}
#define SPC_5MSCTRL_STOP_SEC_CODE
#include "Spc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
