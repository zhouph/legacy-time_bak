/**
 * @defgroup Spc_5msCtrl Spc_5msCtrl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spc_5msCtrl.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Spc_5msCtrl.h"
#include "Spc_5msCtrl_Ifa.h"
#include "IfxStm_reg.h"
#include "LSIDB_FilterSensorSignal.h"
#include "LSIDB_FilterSensorSignal1ms.h"
#include "LSIDB_CalcSensorSignalOffset.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SPC_5MSCTRL_START_SEC_CONST_UNSPECIFIED
#include "Spc_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define SPC_5MSCTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Spc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SPC_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Spc_5msCtrl_HdrBusType Spc_5msCtrlBus;
//Spc_5msCtrl_HdrBusType Spc_5msCtrlBusOld;

/* Internal Logic Bus */
IDBSignalProcessing_SRCbusType   SIGPROCsrcbus;


/* Version Info */
const SwcVersionInfo_t Spc_5msCtrlVersionInfo = 
{   
    SPC_5MSCTRL_MODULE_ID,           /* Spc_5msCtrlVersionInfo.ModuleId */
    SPC_5MSCTRL_MAJOR_VERSION,       /* Spc_5msCtrlVersionInfo.MajorVer */
    SPC_5MSCTRL_MINOR_VERSION,       /* Spc_5msCtrlVersionInfo.MinorVer */
    SPC_5MSCTRL_PATCH_VERSION,       /* Spc_5msCtrlVersionInfo.PatchVer */
    SPC_5MSCTRL_BRANCH_VERSION       /* Spc_5msCtrlVersionInfo.BranchVer */
};
    
/* Input Data Element */
Vat_CtrlNormVlvReqVlvActInfo_t Spc_5msCtrlNormVlvReqVlvActInfo;
Eem_MainEemFailData_t Spc_5msCtrlEemFailData;
Proxy_RxCanRxAccelPedlInfo_t Spc_5msCtrlCanRxAccelPedlInfo;
Pedal_SenSyncPdt5msRawInfo_t Spc_5msCtrlPdt5msRawInfo;
Pedal_SenSyncPdf5msRawInfo_t Spc_5msCtrlPdf5msRawInfo;
Swt_SenSwtStsFlexBrkInfo_t Spc_5msCtrlSwtStsFlexBrkInfo;
Wss_SenWhlSpdInfo_t Spc_5msCtrlWhlSpdInfo;
Press_SenSyncCircP5msRawInfo_t Spc_5msCtrlCircP5msRawInfo;
Press_SenSyncPistP5msRawInfo_t Spc_5msCtrlPistP5msRawInfo;
Abc_CtrlAbsCtrlInfo_t Spc_5msCtrlAbsCtrlInfo;
Abc_CtrlEscCtrlInfo_t Spc_5msCtrlEscCtrlInfo;
Eem_SuspcDetnSwtStsFSInfo_t Spc_5msCtrlSwtStsFSInfo;
Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_t Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo;
Det_5msCtrlPedlTrvlTagInfo_t Spc_5msCtrlPedlTrvlTagInfo;
Eem_MainEemSuspectData_t Spc_5msCtrlEemSuspectData;
Mom_HndlrEcuModeSts_t Spc_5msCtrlEcuModeSts;
Pct_5msCtrlPCtrlAct_t Spc_5msCtrlPCtrlAct;
Mcc_1msCtrlMotCtrlState_t Spc_5msCtrlMotCtrlState;
Swt_SenBlsSwt_t Spc_5msCtrlBlsSwt;
Press_SenSyncPedlSimPRaw_t Spc_5msCtrlPedlSimPRaw;
Abc_CtrlActvBrkCtrlrActFlg_t Spc_5msCtrlActvBrkCtrlrActFlg;
Det_5msCtrlPedlTrvlFinal_t Spc_5msCtrlPedlTrvlFinal;
Det_5msCtrlVehSpdFild_t Spc_5msCtrlVehSpdFild;

/* Output Data Element */
Spc_5msCtrlCircPFildInfo_t Spc_5msCtrlCircPFildInfo;
Spc_5msCtrlCircPOffsCorrdInfo_t Spc_5msCtrlCircPOffsCorrdInfo;
Spc_5msCtrlPedlSimrPFildInfo_t Spc_5msCtrlPedlSimrPFildInfo;
Spc_5msCtrlPedlSimrPOffsCorrdInfo_t Spc_5msCtrlPedlSimrPOffsCorrdInfo;
Spc_5msCtrlPedlTrvlFildInfo_t Spc_5msCtrlPedlTrvlFildInfo;
Spc_5msCtrlPedlTrvlOffsCorrdInfo_t Spc_5msCtrlPedlTrvlOffsCorrdInfo;
Spc_5msCtrlPistPFildInfo_t Spc_5msCtrlPistPFildInfo;
Spc_5msCtrlPistPOffsCorrdInfo_t Spc_5msCtrlPistPOffsCorrdInfo;
Spc_5msCtrlWhlSpdFildInfo_t Spc_5msCtrlWhlSpdFildInfo;
Spc_5msCtrlIdbSnsrEolOfsCalcInfo_t Spc_5msCtrlIdbSnsrEolOfsCalcInfo;
Spc_5msCtrlBlsFild_t Spc_5msCtrlBlsFild;
Spc_5msCtrlFlexBrkSwtFild_t Spc_5msCtrlFlexBrkSwtFild;

uint32 Spc_5msCtrl_Timer_Start;
uint32 Spc_5msCtrl_Timer_Elapsed;

#define SPC_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spc_MemMap.h"
#define SPC_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Spc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SPC_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Spc_MemMap.h"
#define SPC_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Spc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SPC_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Spc_MemMap.h"
#define SPC_5MSCTRL_START_SEC_VAR_32BIT
#include "Spc_MemMap.h"
/** Variable Section (32BIT)**/


#define SPC_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Spc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SPC_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SPC_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spc_MemMap.h"
#define SPC_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Spc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SPC_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Spc_MemMap.h"
#define SPC_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Spc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SPC_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Spc_MemMap.h"
#define SPC_5MSCTRL_START_SEC_VAR_32BIT
#include "Spc_MemMap.h"
/** Variable Section (32BIT)**/


#define SPC_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Spc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SPC_5MSCTRL_START_SEC_CODE
#include "Spc_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Spc_5msCtrl_Init(void)
{
    /* Initialize internal bus */
    Spc_5msCtrlBus.Spc_5msCtrlNormVlvReqVlvActInfo.PrimCutVlvReq = 0;
    Spc_5msCtrlBus.Spc_5msCtrlNormVlvReqVlvActInfo.SecdCutVlvReq = 0;
    Spc_5msCtrlBus.Spc_5msCtrlNormVlvReqVlvActInfo.PrimCircVlvReq = 0;
    Spc_5msCtrlBus.Spc_5msCtrlNormVlvReqVlvActInfo.SecdCircVlvReq = 0;
    Spc_5msCtrlBus.Spc_5msCtrlNormVlvReqVlvActInfo.SimVlvReq = 0;
    Spc_5msCtrlBus.Spc_5msCtrlEemFailData.Eem_Fail_CirP1 = 0;
    Spc_5msCtrlBus.Spc_5msCtrlEemFailData.Eem_Fail_CirP2 = 0;
    Spc_5msCtrlBus.Spc_5msCtrlEemFailData.Eem_Fail_SimP = 0;
    Spc_5msCtrlBus.Spc_5msCtrlEemFailData.Eem_Fail_BLS = 0;
    Spc_5msCtrlBus.Spc_5msCtrlEemFailData.Eem_Fail_PedalPDT = 0;
    Spc_5msCtrlBus.Spc_5msCtrlEemFailData.Eem_Fail_PedalPDF = 0;
    Spc_5msCtrlBus.Spc_5msCtrlCanRxAccelPedlInfo.AccelPedlVal = 0;
    Spc_5msCtrlBus.Spc_5msCtrlCanRxAccelPedlInfo.AccelPedlValErr = 0;
    Spc_5msCtrlBus.Spc_5msCtrlPdt5msRawInfo.PdtSig = 0;
    Spc_5msCtrlBus.Spc_5msCtrlPdf5msRawInfo.PdfSig = 0;
    Spc_5msCtrlBus.Spc_5msCtrlSwtStsFlexBrkInfo.FlexBrkASwt = 0;
    Spc_5msCtrlBus.Spc_5msCtrlSwtStsFlexBrkInfo.FlexBrkBSwt = 0;
    Spc_5msCtrlBus.Spc_5msCtrlWhlSpdInfo.FlWhlSpd = 0;
    Spc_5msCtrlBus.Spc_5msCtrlWhlSpdInfo.FrWhlSpd = 0;
    Spc_5msCtrlBus.Spc_5msCtrlWhlSpdInfo.RlWhlSpd = 0;
    Spc_5msCtrlBus.Spc_5msCtrlWhlSpdInfo.RrlWhlSpd = 0;
    Spc_5msCtrlBus.Spc_5msCtrlCircP5msRawInfo.PrimCircPSig = 0;
    Spc_5msCtrlBus.Spc_5msCtrlCircP5msRawInfo.SecdCircPSig = 0;
    Spc_5msCtrlBus.Spc_5msCtrlPistP5msRawInfo.PistPSig = 0;
    Spc_5msCtrlBus.Spc_5msCtrlAbsCtrlInfo.AbsActFlg = 0;
    Spc_5msCtrlBus.Spc_5msCtrlAbsCtrlInfo.AbsDefectFlg = 0;
    Spc_5msCtrlBus.Spc_5msCtrlAbsCtrlInfo.AbsTarPFrntLe = 0;
    Spc_5msCtrlBus.Spc_5msCtrlAbsCtrlInfo.AbsTarPFrntRi = 0;
    Spc_5msCtrlBus.Spc_5msCtrlAbsCtrlInfo.AbsTarPReLe = 0;
    Spc_5msCtrlBus.Spc_5msCtrlAbsCtrlInfo.AbsTarPReRi = 0;
    Spc_5msCtrlBus.Spc_5msCtrlAbsCtrlInfo.FrntWhlSlip = 0;
    Spc_5msCtrlBus.Spc_5msCtrlAbsCtrlInfo.AbsDesTarP = 0;
    Spc_5msCtrlBus.Spc_5msCtrlAbsCtrlInfo.AbsDesTarPReqFlg = 0;
    Spc_5msCtrlBus.Spc_5msCtrlAbsCtrlInfo.AbsCtrlFadeOutFlg = 0;
    Spc_5msCtrlBus.Spc_5msCtrlEscCtrlInfo.EscActFlg = 0;
    Spc_5msCtrlBus.Spc_5msCtrlEscCtrlInfo.EscDefectFlg = 0;
    Spc_5msCtrlBus.Spc_5msCtrlEscCtrlInfo.EscTarPFrntLe = 0;
    Spc_5msCtrlBus.Spc_5msCtrlEscCtrlInfo.EscTarPFrntRi = 0;
    Spc_5msCtrlBus.Spc_5msCtrlEscCtrlInfo.EscTarPReLe = 0;
    Spc_5msCtrlBus.Spc_5msCtrlEscCtrlInfo.EscTarPReRi = 0;
    Spc_5msCtrlBus.Spc_5msCtrlSwtStsFSInfo.FlexBrkSwtFaultDet = 0;
    Spc_5msCtrlBus.Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg = 0;
    Spc_5msCtrlBus.Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg = 0;
    Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlTagInfo.PdfSigNoiseSuspcFlgH = 0;
    Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlTagInfo.PdfSigTag = 0;
    Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlTagInfo.PdtSigNoiseSuspcFlgH = 0;
    Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlTagInfo.PdtSigTag = 0;
    Spc_5msCtrlBus.Spc_5msCtrlEemSuspectData.Eem_Suspect_CirP1 = 0;
    Spc_5msCtrlBus.Spc_5msCtrlEemSuspectData.Eem_Suspect_CirP2 = 0;
    Spc_5msCtrlBus.Spc_5msCtrlEemSuspectData.Eem_Suspect_SimP = 0;
    Spc_5msCtrlBus.Spc_5msCtrlEemSuspectData.Eem_Suspect_BLS = 0;
    Spc_5msCtrlBus.Spc_5msCtrlEemSuspectData.Eem_Suspect_PedalPDT = 0;
    Spc_5msCtrlBus.Spc_5msCtrlEemSuspectData.Eem_Suspect_PedalPDF = 0;
    Spc_5msCtrlBus.Spc_5msCtrlEcuModeSts = 0;
    Spc_5msCtrlBus.Spc_5msCtrlPCtrlAct = 0;
    Spc_5msCtrlBus.Spc_5msCtrlMotCtrlState = 0;
    Spc_5msCtrlBus.Spc_5msCtrlBlsSwt = 0;
    Spc_5msCtrlBus.Spc_5msCtrlPedlSimPRaw = 0;
    Spc_5msCtrlBus.Spc_5msCtrlActvBrkCtrlrActFlg = 0;
    Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlFinal = 0;
    Spc_5msCtrlBus.Spc_5msCtrlVehSpdFild = 0;
    Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.PrimCircPFild = 0;
    Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar = 0;
    Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.SecdCircPFild = 0;
    Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar = 0;
    Spc_5msCtrlBus.Spc_5msCtrlCircPOffsCorrdInfo.PrimCircPOffsCorrd = 0;
    Spc_5msCtrlBus.Spc_5msCtrlCircPOffsCorrdInfo.PrimCircPOffsCorrdStOkFlg = 0;
    Spc_5msCtrlBus.Spc_5msCtrlCircPOffsCorrdInfo.SecdCircPOffsCorrd = 0;
    Spc_5msCtrlBus.Spc_5msCtrlCircPOffsCorrdInfo.SecdCircPOffsCorrdStOkFlg = 0;
    Spc_5msCtrlBus.Spc_5msCtrlPedlSimrPFildInfo.PedlSimrPFild = 0;
    Spc_5msCtrlBus.Spc_5msCtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar = 0;
    Spc_5msCtrlBus.Spc_5msCtrlPedlSimrPOffsCorrdInfo.PedlSimrPOffsCorrd = 0;
    Spc_5msCtrlBus.Spc_5msCtrlPedlSimrPOffsCorrdInfo.PedlSimrPOffsCorrdStOkFlg = 0;
    Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlFildInfo.PdfFild = 0;
    Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlFildInfo.PdtFild = 0;
    Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdfOffsCorrdStOkFlg = 0;
    Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdfRawOffsCorrd = 0;
    Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdtOffsCorrdStOkFlg = 0;
    Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdtRawOffsCorrd = 0;
    Spc_5msCtrlBus.Spc_5msCtrlPistPFildInfo.PistPFild = 0;
    Spc_5msCtrlBus.Spc_5msCtrlPistPFildInfo.PistPFild_1_100Bar = 0;
    Spc_5msCtrlBus.Spc_5msCtrlPistPOffsCorrdInfo.PistPOffsCorrd = 0;
    Spc_5msCtrlBus.Spc_5msCtrlPistPOffsCorrdInfo.PistPOffsCorrdStOkFlg = 0;
    Spc_5msCtrlBus.Spc_5msCtrlWhlSpdFildInfo.WhlSpdFildFrntLe = 0;
    Spc_5msCtrlBus.Spc_5msCtrlWhlSpdFildInfo.WhlSpdFildFrntRi = 0;
    Spc_5msCtrlBus.Spc_5msCtrlWhlSpdFildInfo.WhlSpdFildReLe = 0;
    Spc_5msCtrlBus.Spc_5msCtrlWhlSpdFildInfo.WhlSpdFildReRi = 0;
    Spc_5msCtrlBus.Spc_5msCtrlIdbSnsrEolOfsCalcInfo.PedalEolOfsCalcOk = 0;
    Spc_5msCtrlBus.Spc_5msCtrlIdbSnsrEolOfsCalcInfo.PedalEolOfsCalcEnd = 0;
    Spc_5msCtrlBus.Spc_5msCtrlIdbSnsrEolOfsCalcInfo.PressEolOfsCalcOk = 0;
    Spc_5msCtrlBus.Spc_5msCtrlIdbSnsrEolOfsCalcInfo.PressEolOfsCalcEnd = 0;
    Spc_5msCtrlBus.Spc_5msCtrlBlsFild = 0;
    Spc_5msCtrlBus.Spc_5msCtrlFlexBrkSwtFild = 0;
}

void Spc_5msCtrl(void)
{
    Spc_5msCtrl_Timer_Start = STM0_TIM0.U;

    /* Input */
    /* Decomposed structure interface */
    Spc_5msCtrl_Read_Spc_5msCtrlNormVlvReqVlvActInfo_PrimCutVlvReq(&Spc_5msCtrlBus.Spc_5msCtrlNormVlvReqVlvActInfo.PrimCutVlvReq);
    Spc_5msCtrl_Read_Spc_5msCtrlNormVlvReqVlvActInfo_SecdCutVlvReq(&Spc_5msCtrlBus.Spc_5msCtrlNormVlvReqVlvActInfo.SecdCutVlvReq);
    Spc_5msCtrl_Read_Spc_5msCtrlNormVlvReqVlvActInfo_PrimCircVlvReq(&Spc_5msCtrlBus.Spc_5msCtrlNormVlvReqVlvActInfo.PrimCircVlvReq);
    Spc_5msCtrl_Read_Spc_5msCtrlNormVlvReqVlvActInfo_SecdCircVlvReq(&Spc_5msCtrlBus.Spc_5msCtrlNormVlvReqVlvActInfo.SecdCircVlvReq);
    Spc_5msCtrl_Read_Spc_5msCtrlNormVlvReqVlvActInfo_SimVlvReq(&Spc_5msCtrlBus.Spc_5msCtrlNormVlvReqVlvActInfo.SimVlvReq);

    /* Decomposed structure interface */
    Spc_5msCtrl_Read_Spc_5msCtrlEemFailData_Eem_Fail_CirP1(&Spc_5msCtrlBus.Spc_5msCtrlEemFailData.Eem_Fail_CirP1);
    Spc_5msCtrl_Read_Spc_5msCtrlEemFailData_Eem_Fail_CirP2(&Spc_5msCtrlBus.Spc_5msCtrlEemFailData.Eem_Fail_CirP2);
    Spc_5msCtrl_Read_Spc_5msCtrlEemFailData_Eem_Fail_SimP(&Spc_5msCtrlBus.Spc_5msCtrlEemFailData.Eem_Fail_SimP);
    Spc_5msCtrl_Read_Spc_5msCtrlEemFailData_Eem_Fail_BLS(&Spc_5msCtrlBus.Spc_5msCtrlEemFailData.Eem_Fail_BLS);
    Spc_5msCtrl_Read_Spc_5msCtrlEemFailData_Eem_Fail_PedalPDT(&Spc_5msCtrlBus.Spc_5msCtrlEemFailData.Eem_Fail_PedalPDT);
    Spc_5msCtrl_Read_Spc_5msCtrlEemFailData_Eem_Fail_PedalPDF(&Spc_5msCtrlBus.Spc_5msCtrlEemFailData.Eem_Fail_PedalPDF);

    Spc_5msCtrl_Read_Spc_5msCtrlCanRxAccelPedlInfo(&Spc_5msCtrlBus.Spc_5msCtrlCanRxAccelPedlInfo);
    /*==============================================================================
    * Members of structure Spc_5msCtrlCanRxAccelPedlInfo 
     : Spc_5msCtrlCanRxAccelPedlInfo.AccelPedlVal;
     : Spc_5msCtrlCanRxAccelPedlInfo.AccelPedlValErr;
     =============================================================================*/
    
    Spc_5msCtrl_Read_Spc_5msCtrlPdt5msRawInfo(&Spc_5msCtrlBus.Spc_5msCtrlPdt5msRawInfo);
    /*==============================================================================
    * Members of structure Spc_5msCtrlPdt5msRawInfo 
     : Spc_5msCtrlPdt5msRawInfo.PdtSig;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Spc_5msCtrl_Read_Spc_5msCtrlPdf5msRawInfo_PdfSig(&Spc_5msCtrlBus.Spc_5msCtrlPdf5msRawInfo.PdfSig);

    Spc_5msCtrl_Read_Spc_5msCtrlSwtStsFlexBrkInfo(&Spc_5msCtrlBus.Spc_5msCtrlSwtStsFlexBrkInfo);
    /*==============================================================================
    * Members of structure Spc_5msCtrlSwtStsFlexBrkInfo 
     : Spc_5msCtrlSwtStsFlexBrkInfo.FlexBrkASwt;
     : Spc_5msCtrlSwtStsFlexBrkInfo.FlexBrkBSwt;
     =============================================================================*/
    
    Spc_5msCtrl_Read_Spc_5msCtrlWhlSpdInfo(&Spc_5msCtrlBus.Spc_5msCtrlWhlSpdInfo);
    /*==============================================================================
    * Members of structure Spc_5msCtrlWhlSpdInfo 
     : Spc_5msCtrlWhlSpdInfo.FlWhlSpd;
     : Spc_5msCtrlWhlSpdInfo.FrWhlSpd;
     : Spc_5msCtrlWhlSpdInfo.RlWhlSpd;
     : Spc_5msCtrlWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/
    
    Spc_5msCtrl_Read_Spc_5msCtrlCircP5msRawInfo(&Spc_5msCtrlBus.Spc_5msCtrlCircP5msRawInfo);
    /*==============================================================================
    * Members of structure Spc_5msCtrlCircP5msRawInfo 
     : Spc_5msCtrlCircP5msRawInfo.PrimCircPSig;
     : Spc_5msCtrlCircP5msRawInfo.SecdCircPSig;
     =============================================================================*/
    
    Spc_5msCtrl_Read_Spc_5msCtrlPistP5msRawInfo(&Spc_5msCtrlBus.Spc_5msCtrlPistP5msRawInfo);
    /*==============================================================================
    * Members of structure Spc_5msCtrlPistP5msRawInfo 
     : Spc_5msCtrlPistP5msRawInfo.PistPSig;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Spc_5msCtrl_Read_Spc_5msCtrlAbsCtrlInfo_AbsActFlg(&Spc_5msCtrlBus.Spc_5msCtrlAbsCtrlInfo.AbsActFlg);
    Spc_5msCtrl_Read_Spc_5msCtrlAbsCtrlInfo_AbsDefectFlg(&Spc_5msCtrlBus.Spc_5msCtrlAbsCtrlInfo.AbsDefectFlg);
    Spc_5msCtrl_Read_Spc_5msCtrlAbsCtrlInfo_AbsTarPFrntLe(&Spc_5msCtrlBus.Spc_5msCtrlAbsCtrlInfo.AbsTarPFrntLe);
    Spc_5msCtrl_Read_Spc_5msCtrlAbsCtrlInfo_AbsTarPFrntRi(&Spc_5msCtrlBus.Spc_5msCtrlAbsCtrlInfo.AbsTarPFrntRi);
    Spc_5msCtrl_Read_Spc_5msCtrlAbsCtrlInfo_AbsTarPReLe(&Spc_5msCtrlBus.Spc_5msCtrlAbsCtrlInfo.AbsTarPReLe);
    Spc_5msCtrl_Read_Spc_5msCtrlAbsCtrlInfo_AbsTarPReRi(&Spc_5msCtrlBus.Spc_5msCtrlAbsCtrlInfo.AbsTarPReRi);
    Spc_5msCtrl_Read_Spc_5msCtrlAbsCtrlInfo_FrntWhlSlip(&Spc_5msCtrlBus.Spc_5msCtrlAbsCtrlInfo.FrntWhlSlip);
    Spc_5msCtrl_Read_Spc_5msCtrlAbsCtrlInfo_AbsDesTarP(&Spc_5msCtrlBus.Spc_5msCtrlAbsCtrlInfo.AbsDesTarP);
    Spc_5msCtrl_Read_Spc_5msCtrlAbsCtrlInfo_AbsDesTarPReqFlg(&Spc_5msCtrlBus.Spc_5msCtrlAbsCtrlInfo.AbsDesTarPReqFlg);
    Spc_5msCtrl_Read_Spc_5msCtrlAbsCtrlInfo_AbsCtrlFadeOutFlg(&Spc_5msCtrlBus.Spc_5msCtrlAbsCtrlInfo.AbsCtrlFadeOutFlg);

    /* Decomposed structure interface */
    Spc_5msCtrl_Read_Spc_5msCtrlEscCtrlInfo_EscActFlg(&Spc_5msCtrlBus.Spc_5msCtrlEscCtrlInfo.EscActFlg);
    Spc_5msCtrl_Read_Spc_5msCtrlEscCtrlInfo_EscDefectFlg(&Spc_5msCtrlBus.Spc_5msCtrlEscCtrlInfo.EscDefectFlg);
    Spc_5msCtrl_Read_Spc_5msCtrlEscCtrlInfo_EscTarPFrntLe(&Spc_5msCtrlBus.Spc_5msCtrlEscCtrlInfo.EscTarPFrntLe);
    Spc_5msCtrl_Read_Spc_5msCtrlEscCtrlInfo_EscTarPFrntRi(&Spc_5msCtrlBus.Spc_5msCtrlEscCtrlInfo.EscTarPFrntRi);
    Spc_5msCtrl_Read_Spc_5msCtrlEscCtrlInfo_EscTarPReLe(&Spc_5msCtrlBus.Spc_5msCtrlEscCtrlInfo.EscTarPReLe);
    Spc_5msCtrl_Read_Spc_5msCtrlEscCtrlInfo_EscTarPReRi(&Spc_5msCtrlBus.Spc_5msCtrlEscCtrlInfo.EscTarPReRi);

    Spc_5msCtrl_Read_Spc_5msCtrlSwtStsFSInfo(&Spc_5msCtrlBus.Spc_5msCtrlSwtStsFSInfo);
    /*==============================================================================
    * Members of structure Spc_5msCtrlSwtStsFSInfo 
     : Spc_5msCtrlSwtStsFSInfo.FlexBrkSwtFaultDet;
     =============================================================================*/
    
    Spc_5msCtrl_Read_Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo(&Spc_5msCtrlBus.Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo);
    /*==============================================================================
    * Members of structure Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo 
     : Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg;
     : Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Spc_5msCtrl_Read_Spc_5msCtrlPedlTrvlTagInfo_PdfSigNoiseSuspcFlgH(&Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlTagInfo.PdfSigNoiseSuspcFlgH);
    Spc_5msCtrl_Read_Spc_5msCtrlPedlTrvlTagInfo_PdfSigTag(&Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlTagInfo.PdfSigTag);
    Spc_5msCtrl_Read_Spc_5msCtrlPedlTrvlTagInfo_PdtSigNoiseSuspcFlgH(&Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlTagInfo.PdtSigNoiseSuspcFlgH);
    Spc_5msCtrl_Read_Spc_5msCtrlPedlTrvlTagInfo_PdtSigTag(&Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlTagInfo.PdtSigTag);

    /* Decomposed structure interface */
    Spc_5msCtrl_Read_Spc_5msCtrlEemSuspectData_Eem_Suspect_CirP1(&Spc_5msCtrlBus.Spc_5msCtrlEemSuspectData.Eem_Suspect_CirP1);
    Spc_5msCtrl_Read_Spc_5msCtrlEemSuspectData_Eem_Suspect_CirP2(&Spc_5msCtrlBus.Spc_5msCtrlEemSuspectData.Eem_Suspect_CirP2);
    Spc_5msCtrl_Read_Spc_5msCtrlEemSuspectData_Eem_Suspect_SimP(&Spc_5msCtrlBus.Spc_5msCtrlEemSuspectData.Eem_Suspect_SimP);
    Spc_5msCtrl_Read_Spc_5msCtrlEemSuspectData_Eem_Suspect_BLS(&Spc_5msCtrlBus.Spc_5msCtrlEemSuspectData.Eem_Suspect_BLS);
    Spc_5msCtrl_Read_Spc_5msCtrlEemSuspectData_Eem_Suspect_PedalPDT(&Spc_5msCtrlBus.Spc_5msCtrlEemSuspectData.Eem_Suspect_PedalPDT);
    Spc_5msCtrl_Read_Spc_5msCtrlEemSuspectData_Eem_Suspect_PedalPDF(&Spc_5msCtrlBus.Spc_5msCtrlEemSuspectData.Eem_Suspect_PedalPDF);

    Spc_5msCtrl_Read_Spc_5msCtrlEcuModeSts(&Spc_5msCtrlBus.Spc_5msCtrlEcuModeSts);
    Spc_5msCtrl_Read_Spc_5msCtrlPCtrlAct(&Spc_5msCtrlBus.Spc_5msCtrlPCtrlAct);
    Spc_5msCtrl_Read_Spc_5msCtrlMotCtrlState(&Spc_5msCtrlBus.Spc_5msCtrlMotCtrlState);
    Spc_5msCtrl_Read_Spc_5msCtrlBlsSwt(&Spc_5msCtrlBus.Spc_5msCtrlBlsSwt);
    Spc_5msCtrl_Read_Spc_5msCtrlPedlSimPRaw(&Spc_5msCtrlBus.Spc_5msCtrlPedlSimPRaw);
    Spc_5msCtrl_Read_Spc_5msCtrlActvBrkCtrlrActFlg(&Spc_5msCtrlBus.Spc_5msCtrlActvBrkCtrlrActFlg);
    Spc_5msCtrl_Read_Spc_5msCtrlPedlTrvlFinal(&Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlFinal);
    Spc_5msCtrl_Read_Spc_5msCtrlVehSpdFild(&Spc_5msCtrlBus.Spc_5msCtrlVehSpdFild);
	if(SIGPROCsrcbus.SystemOnCnt<U16_T_2_S)
    {
        SIGPROCsrcbus.SystemOnCnt++;
    }
    /* Process */
    LSIDB_vCallSnsrOffsCalcn();
    LSIDB_vCallSnsrSigFil();

    /* Output */
    Spc_5msCtrl_Write_Spc_5msCtrlCircPFildInfo(&Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo);
    /*==============================================================================
    * Members of structure Spc_5msCtrlCircPFildInfo 
     : Spc_5msCtrlCircPFildInfo.PrimCircPFild;
     : Spc_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar;
     : Spc_5msCtrlCircPFildInfo.SecdCircPFild;
     : Spc_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar;
     =============================================================================*/
    
    Spc_5msCtrl_Write_Spc_5msCtrlCircPOffsCorrdInfo(&Spc_5msCtrlBus.Spc_5msCtrlCircPOffsCorrdInfo);
    /*==============================================================================
    * Members of structure Spc_5msCtrlCircPOffsCorrdInfo 
     : Spc_5msCtrlCircPOffsCorrdInfo.PrimCircPOffsCorrd;
     : Spc_5msCtrlCircPOffsCorrdInfo.PrimCircPOffsCorrdStOkFlg;
     : Spc_5msCtrlCircPOffsCorrdInfo.SecdCircPOffsCorrd;
     : Spc_5msCtrlCircPOffsCorrdInfo.SecdCircPOffsCorrdStOkFlg;
     =============================================================================*/
    
    Spc_5msCtrl_Write_Spc_5msCtrlPedlSimrPFildInfo(&Spc_5msCtrlBus.Spc_5msCtrlPedlSimrPFildInfo);
    /*==============================================================================
    * Members of structure Spc_5msCtrlPedlSimrPFildInfo 
     : Spc_5msCtrlPedlSimrPFildInfo.PedlSimrPFild;
     : Spc_5msCtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar;
     =============================================================================*/
    
    Spc_5msCtrl_Write_Spc_5msCtrlPedlSimrPOffsCorrdInfo(&Spc_5msCtrlBus.Spc_5msCtrlPedlSimrPOffsCorrdInfo);
    /*==============================================================================
    * Members of structure Spc_5msCtrlPedlSimrPOffsCorrdInfo 
     : Spc_5msCtrlPedlSimrPOffsCorrdInfo.PedlSimrPOffsCorrd;
     : Spc_5msCtrlPedlSimrPOffsCorrdInfo.PedlSimrPOffsCorrdStOkFlg;
     =============================================================================*/
    
    Spc_5msCtrl_Write_Spc_5msCtrlPedlTrvlFildInfo(&Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlFildInfo);
    /*==============================================================================
    * Members of structure Spc_5msCtrlPedlTrvlFildInfo 
     : Spc_5msCtrlPedlTrvlFildInfo.PdfFild;
     : Spc_5msCtrlPedlTrvlFildInfo.PdtFild;
     =============================================================================*/
    
    Spc_5msCtrl_Write_Spc_5msCtrlPedlTrvlOffsCorrdInfo(&Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlOffsCorrdInfo);
    /*==============================================================================
    * Members of structure Spc_5msCtrlPedlTrvlOffsCorrdInfo 
     : Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdfOffsCorrdStOkFlg;
     : Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdfRawOffsCorrd;
     : Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdtOffsCorrdStOkFlg;
     : Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdtRawOffsCorrd;
     =============================================================================*/
    
    Spc_5msCtrl_Write_Spc_5msCtrlPistPFildInfo(&Spc_5msCtrlBus.Spc_5msCtrlPistPFildInfo);
    /*==============================================================================
    * Members of structure Spc_5msCtrlPistPFildInfo 
     : Spc_5msCtrlPistPFildInfo.PistPFild;
     : Spc_5msCtrlPistPFildInfo.PistPFild_1_100Bar;
     =============================================================================*/
    
    Spc_5msCtrl_Write_Spc_5msCtrlPistPOffsCorrdInfo(&Spc_5msCtrlBus.Spc_5msCtrlPistPOffsCorrdInfo);
    /*==============================================================================
    * Members of structure Spc_5msCtrlPistPOffsCorrdInfo 
     : Spc_5msCtrlPistPOffsCorrdInfo.PistPOffsCorrd;
     : Spc_5msCtrlPistPOffsCorrdInfo.PistPOffsCorrdStOkFlg;
     =============================================================================*/
    
    Spc_5msCtrl_Write_Spc_5msCtrlWhlSpdFildInfo(&Spc_5msCtrlBus.Spc_5msCtrlWhlSpdFildInfo);
    /*==============================================================================
    * Members of structure Spc_5msCtrlWhlSpdFildInfo 
     : Spc_5msCtrlWhlSpdFildInfo.WhlSpdFildFrntLe;
     : Spc_5msCtrlWhlSpdFildInfo.WhlSpdFildFrntRi;
     : Spc_5msCtrlWhlSpdFildInfo.WhlSpdFildReLe;
     : Spc_5msCtrlWhlSpdFildInfo.WhlSpdFildReRi;
     =============================================================================*/
    
    Spc_5msCtrl_Write_Spc_5msCtrlIdbSnsrEolOfsCalcInfo(&Spc_5msCtrlBus.Spc_5msCtrlIdbSnsrEolOfsCalcInfo);
    /*==============================================================================
    * Members of structure Spc_5msCtrlIdbSnsrEolOfsCalcInfo 
     : Spc_5msCtrlIdbSnsrEolOfsCalcInfo.PedalEolOfsCalcOk;
     : Spc_5msCtrlIdbSnsrEolOfsCalcInfo.PedalEolOfsCalcEnd;
     : Spc_5msCtrlIdbSnsrEolOfsCalcInfo.PressEolOfsCalcOk;
     : Spc_5msCtrlIdbSnsrEolOfsCalcInfo.PressEolOfsCalcEnd;
     =============================================================================*/
    
    Spc_5msCtrl_Write_Spc_5msCtrlBlsFild(&Spc_5msCtrlBus.Spc_5msCtrlBlsFild);
    Spc_5msCtrl_Write_Spc_5msCtrlFlexBrkSwtFild(&Spc_5msCtrlBus.Spc_5msCtrlFlexBrkSwtFild);

    Spc_5msCtrl_Timer_Elapsed = STM0_TIM0.U - Spc_5msCtrl_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define SPC_5MSCTRL_STOP_SEC_CODE
#include "Spc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
