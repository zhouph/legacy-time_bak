/**
 * @defgroup LSIDB_CalcSensorSignalOffs LSIDB_CalcSensorSignalOffs
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/*******************************************************************************
 * @file        LSIDB_CalcSensorSignalOffs.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/
/*==============================================================================
 *  Code Metric
 *  Function								Metric
 *
 =============================================================================*/
/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "LSIDB_CalcSensorSignalOffset.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*Redefinition of HDRbus for variable matching*/
#define NVM_PdtOffsEolReadVal			NvMIf_LogicEepData.Eep.PedlTrvlOffsEepInfo.PdtOffsEolReadVal
#define NVM_PdfOffsEolReadVal			NvMIf_LogicEepData.Eep.PedlTrvlOffsEepInfo.PdfOffsEolReadVal
#define NVM_PdtOffsDrvgReadVal			NvMIf_LogicEepData.Eep.PedlTrvlOffsEepInfo.PdtOffsDrvgReadVal
#define NVM_PdfOffsDrvgReadVal			NvMIf_LogicEepData.Eep.PedlTrvlOffsEepInfo.PdfOffsDrvgReadVal
#define NVM_PedlSimrPOffsEolReadVal		NvMIf_LogicEepData.Eep.PedlSimrPOffsEepInfo.PedlSimrPOffsEolReadVal
#define NVM_PedlSimrPOffsDrvgReadVal	NvMIf_LogicEepData.Eep.PedlSimrPOffsEepInfo.PedlSimrPOffsDrvgReadVal

#define NVM_PistPOffsEolReadVal			NvMIf_LogicEepData.Eep.PistPOffsEepInfo.PistPOffsEolReadVal
#define NVM_PrimCircPOffsEolReadVal		NvMIf_LogicEepData.Eep.CircPOffsEepInfo.PrimCircPOffsEolReadVal
#define NVM_SecdCircPOffsEolReadVal		NvMIf_LogicEepData.Eep.CircPOffsEepInfo.SecdCircPOffsEolReadVal

#define NVM_PistPOffsDrvgReadVal		NvMIf_LogicEepData.Eep.PistPOffsEepInfo.PistPOffsDrvgReadVal
#define NVM_PrimCircPOffsDrvgReadVal	NvMIf_LogicEepData.Eep.CircPOffsEepInfo.PrimCircPOffsDrvgReadVal
#define NVM_SecdCircPOffsDrvgReadVal	NvMIf_LogicEepData.Eep.CircPOffsEepInfo.SecdCircPOffsDrvgReadVal

#define NvM_EepDataReadInvldFlg			NvMIf_LogicEepData.Eep.ReadInvldFlg


#define NVM_PdtOffsEolReadVal2			NvMIf_LogicEepData.Eep.PedlTrvlOffsEepInfo2.PdtOffsEolReadVal
#define NVM_PdfOffsEolReadVal2			NvMIf_LogicEepData.Eep.PedlTrvlOffsEepInfo2.PdfOffsEolReadVal
#define NVM_PdtOffsDrvgReadVal2			NvMIf_LogicEepData.Eep.PedlTrvlOffsEepInfo2.PdtOffsDrvgReadVal
#define NVM_PdfOffsDrvgReadVal2			NvMIf_LogicEepData.Eep.PedlTrvlOffsEepInfo2.PdfOffsDrvgReadVal
#define NVM_PedlSimrPOffsEolReadVal2	NvMIf_LogicEepData.Eep.PedlSimrPOffsEepInfo2.PedlSimrPOffsEolReadVal
#define NVM_PedlSimrPOffsDrvgReadVal2	NvMIf_LogicEepData.Eep.PedlSimrPOffsEepInfo2.PedlSimrPOffsDrvgReadVal

#define NVM_PistPOffsEolReadVal2		NvMIf_LogicEepData.Eep.PistPOffsEepInfo2.PistPOffsEolReadVal
#define NVM_PrimCircPOffsEolReadVal2	NvMIf_LogicEepData.Eep.CircPOffsEepInfo2.PrimCircPOffsEolReadVal
#define NVM_SecdCircPOffsEolReadVal2	NvMIf_LogicEepData.Eep.CircPOffsEepInfo2.SecdCircPOffsEolReadVal

#define NVM_PistPOffsDrvgReadVal2		NvMIf_LogicEepData.Eep.PistPOffsEepInfo2.PistPOffsDrvgReadVal
#define NVM_PrimCircPOffsDrvgReadVal2	NvMIf_LogicEepData.Eep.CircPOffsEepInfo2.PrimCircPOffsDrvgReadVal
#define NVM_SecdCircPOffsDrvgReadVal2	NvMIf_LogicEepData.Eep.CircPOffsEepInfo2.SecdCircPOffsDrvgReadVal


/* RTE input variable*/
/* Pedal Signal interface */

/*RTE output variable*/

/*Constant --> move to cal*/
#define  EEP_RETRY                      1
#define  EEP_DELAY_T                    U8_T_1000_MS
#define  EEP_CALC_T                     U8_T_500_MS
#define  PDT1_OFS_LM_THR                S16_TRAVEL_25_MM
#define  PDT2_OFS_LM_THR                S16_TRAVEL_25_MM
#define  PDT1_OFS_MAX_MIN_THR           S16_TRAVEL_2_MM
#define  PDT2_OFS_MAX_MIN_THR           S16_TRAVEL_2_MM
#define  PRES_OFS_LM_THR                S16_CIRC_PRESS_15_BAR
#define  PRES_OFS_MAX_MIN_THR           S16_CIRC_PRESS_2_BAR
#define  PDT1_STB_THR                   S16_TRAVEL_1_MM
#define  PDT2_STB_THR                   S16_TRAVEL_2_MM
#define  PRES_STB_THR                   S16_CIRC_PRESS_1_BAR


#define  PDT_OFFSET_CALC_CASE           1
#define  PDF_OFFSET_CALC_CASE           2
#define  PEDAL_SIM_P_OFFSET_CALC_CASE   3
#define  PIST_P_OFFSET_CALC_CASE        4
#define  PRIM_P_OFFSET_CALC_CASE        5
#define  SECD_P_OFFSET_CALC_CASE        6

#define  PDTF_OFS_F_P_LIMIT             S16_TRAVEL_25_MM
#define  PRES_OFS_F_P_LIMIT             S16_CIRC_PRESS_15_BAR
#define  PDTF_OFS_F_M_LIMIT             -S16_TRAVEL_10_MM
#define  PRES_OFS_F_M_LIMIT             -S16_CIRC_PRESS_7_BAR

#define  PDTF_EEP_DRV_OFS_DV_THR        S16_TRAVEL_1_MM
#define  PDTF_DRV_DRV_OFS_DV_THR        S16_TRAVEL_1_MM
#define  PRES_EEP_DRV_OFS_DV_THR        S16_CIRC_PRESS_0_5_BAR
#define  PRES_DRV_DRV_OFS_DV_THR        S16_CIRC_PRESS_0_5_BAR

#define  PDTF_EEP_W_UPD_THR             S16_TRAVEL_1_MM
#define  PRES_EEP_W_UPD_THR             S16_CIRC_PRESS_0_5_BAR

#define  PDTF_EEP_W_UPD_LIMIT           S16_TRAVEL_25_MM
#define  PRES_EEP_W_UPD_LIMIT           S16_CIRC_PRESS_15_BAR

#define  NVM_RESET						(DISABLE) /*Temp*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
    uint8_t     lsahbu8AHBEEPofsMaxMinLMOverCnt;
    uint8_t     lsahbu8AHBofsFirstCnt;
    int32_t     lsahbs32AHBOfsFirstSum;
    int16_t     lsahbs16AHBofsFirstCalMax;
    int16_t     lsahbs16AHBofsFirstCalMin;
    int16_t     lsahbs16AHBofsFirstCal;
    uint8_t     lsahbu8EEPOfsDelyCnt;
    int16_t     SnsrRawSig;

    uint32_t    lsahbu1EEPofsCalcFWStartFlgOLD  :1;
    uint32_t    lsahbu1EEPofsCalcFWStartFlg     :1;
    uint32_t    lsahbu1EEPofsCalcFWStartFlgIni  :1;
    uint32_t    lsahbu1AHBeepEnterOK            :1;
    uint32_t    lsahbu1AHBeepCalcLoopOK         :1;
    uint32_t    lsahbu1AHBeepWhileOK            :1;
    uint32_t    lsahbu1EEPofsCalcCLStartFlg     :1;
    uint32_t    lsahbu1AHBEepOfsCalEndFlg       :1;
    uint32_t    lsahbu1EEPOfsEepReCalcOK        :1;
    uint32_t    lsahbu1EEPofsEEPLimitOK         :1;
    uint32_t    lsahbu1EEPofsEEPMaxMinOK        :1;
    uint32_t    lsahbu1AHBEepOfsCalOkFlg        :1;

}IdbSnsrOffsEolCalc_t;/* LogData Type 수정 필요 */

typedef struct
{
    int16_t     lsahbs16AHBEolEEPofsReadRaw;
    int16_t     lsahbs16AHBEEPofsRead;
    int16_t 	lsIdbs16EolOffsEepWr;
    uint32_t    lsahbu1AHBEepOfsWriteReq    :1;
    uint32_t    lsahbu1AHBEepOfsWriteOkFlg  :1;
    uint32_t    lsahbu1AHBEolOfsWriteReqOK  :1;
    uint32_t    lsahbu8AHBEEPofsReadInvalid :1;
    uint32_t    lsahbu1AHBEEPofsUseOK       :1;

}IdbSnsrOffsEolEep_t;/* LogData Type 수정 필요 */

typedef struct
{
    uint8_t lsahbu8AHBDrvOfsCnt;
    uint8_t lsahbu8AHBDrvOfsArrayCnt;
    int16_t lsahbs16AHBDrvOfsArrayMin;
    int16_t lsahbs16AHBDrvOfsArrayFilter;
    int16_t lsahbs16AHBDrvOfsArrayFConv;
    int16_t lsahbs16AHBDrvOfs_f;
    int16_t lsahbs16AHBDrvMin;
    uint8_t lsahbu8AHBDrvOfs10sArrayCnt;
    uint8_t ldahbu8AHBactAfterCNT;

    uint8_t lsahbu8AHBNormCondCnt;
    int16_t lsahbs16AHBNormCondMeanOld;
    int16_t lsahbs16AHBNormCondMean;
    int32_t lsahbs32AHBNormCondSum;

    uint8_t lsahbu8AHBDrvModeDeltTCnt;
    int16_t lsahbs16AHBNormCondMeanDiff;

    int16_t SnsrRawSig;
    int16_t s16SnsrRawSigLim;

    int32_t lsahbs32AHBDrvOfs1secSum;
    int32_t lsahbs32AHBDrvOfs1secSumArray[10];
    int32_t lsahbs32AHBDrvOfs1secArrayMax;
    int32_t lsahbs32AHBDrvOfs1secArrayMin;
    int32_t lsahbs32AHBDrvOfs1secArraySum;
    int32_t lsahbs32AHBDrvOfs1secArrayTrimSum;
    int32_t lsahbs32AHBDrvOfs1secArrayMean;
    int32_t lsahbs32AHBDrvofs10secSum;

    int16_t lsahbs16AHBDrvOfs60sSumOld;
    int16_t lsahbs16AHBDrvOfs60sSum;

    uint32_t    lsahbu1AHBDrv10secOfsOK     :1;
    uint32_t    lsahbu1AHBDrv1secOfsOK      :1;
    uint32_t    lsahbu1AHBDrv60secOfsReOK   :1;
    uint32_t    lsahbu1AHBDrv60secOfsOK     :1;
    uint32_t    lsahbu1AHBDrvOfsOK          :1;
    uint32_t    lsahbu1AHBact1secAfterOK    :1;
    uint32_t    lsahbu1NormCondFirstCheck   :1;
    uint32_t    lsahbu1SensorNormCond       :1;
    uint32_t    lsahbu1SensorDrvOfsCheckCond:1;
    uint32_t    lsahbu1SensorFaultDet		:1;
    uint32_t    lsahbu1SensorSusDet			:1;
}IdbSnsrOffsDrvgCalc_t;/* LogData Type 수정 필요 */

typedef struct
{
    int16_t lsahbs16DrvEEPofsReadRaw;
    int16_t lsahbs16AHBDrvEEPofsRead;

    uint32_t    lsahbu1AHBDrvEEPofsUseOK    :1;
    uint32_t    lsahbu8DrvEEPofsReadInvalid :1;
    uint32_t    lsahbu1AHBDrvOfsEepWriteReq :1;
    uint32_t    lsahbu1AHBeepWriteResetReq  :1;
    uint32_t    lsahbu1AHBDrveepWriteEnd    :1;
    int16_t 	lsIdbs16DrvgOffsEepWr;
}IdbSnsrOffsDrvgEep_t; /* LogData Type 수정 필요 */

typedef struct
{
    int16_t     lsahbs16AHBOfs_f;

    uint32_t    lsahbu1AHBFinalOfsOK    :1;
    uint32_t    lsahbu1AHBDrvEepOfsDrvUse:1;
    uint16_t    lsahbu1AHBFinalMonitor; /*F216 Debug TODO*/
}IdbSnsrOffsFinal_t;/* LogData Type 수정 필요 */

typedef struct
{
    int16_t     SnsrOffsEolMinMaxLim;
    int16_t     SnsrOffsEolMinMaxDifThd;
    int16_t     SnsrOffsDrvgDeMaxThd;
    uint8_t     SnsrOffsDrvgCalcCdnChkCase;
    int16_t     SnsrOffsEolEepReadValLim;
    int16_t     SnsrOffsDrvgEepReadValLim;
    int16_t     SnsrOffsDrvEepUseThd;
    int16_t     SnsrOffsDrvDrvUseThd;
    int16_t     SnsrOffsUpprLim;
    int16_t     SnsrOffsLowrLim;
    int16_t     SnsrOffsDrvEepUpdThd;
    int16_t     SnsrOffsDrvEepUpdLim;
}IdbSnsrOffsCal_t;

typedef struct
{
    int16_t lsahbs16PDFRaw;
    int16_t lsahbs16PDTRaw;

    int16_t ws16PdtSnsrOfset;
    int16_t ws16PdfSnsrOfset;

    int16_t ws16DrvPdtSnsrOfset;
    int16_t ws16DrvPdfSnsrOfset;


    int16_t lsahbs16PSpresRaw;
    int16_t ws16PSPressureOfset;

    int16_t ws16DrvPSPressureOfset;

    int16_t s16VehicleSpeed;

    int16_t lsahbs16PedalTravelFilt;
    int16_t lis16cEMSAccPedDep;

    #if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE

    int16_t lsahbs16MCpresRaw;
    int16_t ws16BCPressureOfset;

    int16_t ws16DrvBCPressureOfset;
    #elif __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
    int16_t lsahbs16MCpresRaw;
    int16_t lsahbs16MC2presRaw;

    int16_t ws16BCPressureOfset;
    int16_t ws16DrvBCPressureOfset;
    int16_t ws16BC2PressureOfset;
    int16_t ws16DrvBC2PressureOfset;
    #endif

    #if __BRAKE_SWITCH_ENABLE == ENABLE
    int32_t fu1BSSignal;
    int32_t fu1BsErrDet;
    int32_t fu1BsSusDet;
    #endif

    /*Flags*/

    int32_t lcahbu1AHBOn;
    int32_t liu1cEMSAccPedDep_Pc_Sus;
    int32_t ldahbu1AbsActiveFlg;
    int32_t ldahbu1EscActiveFlg;
    int32_t liu1EscActiveBrk;

    int32_t cdu1DiagPedalTravelSnrCalReq;
    int32_t cdu1DiagPressureSnrCalReq;
    int32_t fu1BLSErrDet;
    int32_t fu1BlsSusDet;
    int32_t lsahbu1BLS;
    uint8_t Idbu8CVVAct;
    uint8_t Idbu8CVV2Act;
    uint8_t Idbu8PressDumpVlvAct;

    int32_t wu1PdtSnsrOfSetInvalid;
    int32_t wu1PdfSnsrOfSetInvalid;
    int32_t wu1DrvPdtSnsrOfSetInvalid;
    int32_t wu1DrvPdfSnsrOfSetInvalid;

    int32_t fsu1PdtSenFaultDet;
    int32_t fdu1PdtSigSusSet;
    int32_t fsu1PdfSenFaultDet;
    int32_t fdu1PdfSigSusSet;
    int32_t wu1PSPressureOfsetInvalid;
    int32_t wu1DrvPSPressureOfsetInvalid;
    int32_t fsu1PSPSenFaultDet;
    int32_t fu1PspSusDet;
    int32_t cu8EMS2AccPedDep_Pc_Err_Flg;

#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
    int32_t wu1BCPressureOfsetInvalid;
    int32_t wu1DrvBCPressureOfsetInvalid;
/*	int32_t wu1PressureBcEepWrtOk;
	int32_t wu1IgnOffDrvBCPOfsStoreReq;*/
	int32_t fsu1BCP1SenFaultDet;
	int32_t fu1BcpPriSusDet;
#elif __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
    int32_t wu1BCPressureOfsetInvalid;
    int32_t wu1DrvBCPressureOfsetInvalid;
    int32_t wu1BC2PressureOfsetInvalid;
    int32_t wu1DrvBC2PressureOfsetInvalid;
    int32_t fsu1BCP1SenFaultDet;
    int32_t fu1BcpPriSusDet;
    int32_t fsu1BCP2SenFaultDet;
    int32_t fu1BcpSecSusDet;
#endif
}GetSnsrSigOffs_t;
/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SPC_5MSCTRL_START_SEC_CONST_UNSPECIFIED
#include "Spc_MemMap.h"
/** Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/

const IdbSnsrOffsCal_t PdtOffsCal =
{
        PDT1_OFS_LM_THR,
        PDT1_OFS_MAX_MIN_THR,
        PDT1_STB_THR,
        PDT_OFFSET_CALC_CASE,
        PDT1_OFS_LM_THR,
        PDT1_OFS_LM_THR,
        PDTF_EEP_DRV_OFS_DV_THR,
        PDTF_DRV_DRV_OFS_DV_THR,
        PDTF_OFS_F_P_LIMIT,
        PDTF_OFS_F_M_LIMIT,
        PDTF_EEP_W_UPD_THR,
        PDTF_EEP_W_UPD_LIMIT,
};

const  IdbSnsrOffsCal_t PdfOffsCal =
{
        PDT2_OFS_LM_THR,
        PDT2_OFS_MAX_MIN_THR,
        PDT2_STB_THR,
        PDF_OFFSET_CALC_CASE,
        PDT2_OFS_LM_THR,
        PDT2_OFS_LM_THR,
        PDTF_EEP_DRV_OFS_DV_THR,
        PDTF_DRV_DRV_OFS_DV_THR,
        PDTF_OFS_F_P_LIMIT,
        PDTF_OFS_F_M_LIMIT,
        PDTF_EEP_W_UPD_THR,
        PDTF_EEP_W_UPD_LIMIT,
};

const  IdbSnsrOffsCal_t PedlSimrPOffsCal =
{
        PRES_OFS_LM_THR,
        PRES_OFS_MAX_MIN_THR,
        PRES_STB_THR,
        PEDAL_SIM_P_OFFSET_CALC_CASE,
        PRES_OFS_LM_THR,
        PRES_OFS_LM_THR,
        PRES_EEP_DRV_OFS_DV_THR,
        PRES_DRV_DRV_OFS_DV_THR,
        PRES_OFS_F_P_LIMIT,
        PRES_OFS_F_M_LIMIT,
        PRES_EEP_W_UPD_THR,
        PRES_EEP_W_UPD_LIMIT,
};
#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
const  IdbSnsrOffsCal_t PistPOffsCal =
{
    PRES_OFS_LM_THR,
    PRES_OFS_MAX_MIN_THR,
    PRES_STB_THR,
    PIST_P_OFFSET_CALC_CASE,
    PRES_OFS_LM_THR,
    PRES_OFS_LM_THR,
    PRES_EEP_DRV_OFS_DV_THR,
    PRES_DRV_DRV_OFS_DV_THR,
    PRES_OFS_F_P_LIMIT,
    PRES_OFS_F_M_LIMIT,
    PRES_EEP_W_UPD_THR,
    PRES_EEP_W_UPD_LIMIT,
};
#elif __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
const  IdbSnsrOffsCal_t PrimPOffsCal =
{
    PRES_OFS_LM_THR,
    PRES_OFS_MAX_MIN_THR,
    PRES_STB_THR,
    PRIM_P_OFFSET_CALC_CASE,
    PRES_OFS_LM_THR,
    PRES_OFS_LM_THR,
    PRES_EEP_DRV_OFS_DV_THR,
    PRES_DRV_DRV_OFS_DV_THR,
    PRES_OFS_F_P_LIMIT,
    PRES_OFS_F_M_LIMIT,
    PRES_EEP_W_UPD_THR,
    PRES_EEP_W_UPD_LIMIT,
};

const IdbSnsrOffsCal_t SecdPOffsCal =
{
    PRES_OFS_LM_THR,
    PRES_OFS_MAX_MIN_THR,
    PRES_STB_THR,
    SECD_P_OFFSET_CALC_CASE,
    PRES_OFS_LM_THR,
    PRES_OFS_LM_THR,
    PRES_EEP_DRV_OFS_DV_THR,
    PRES_DRV_DRV_OFS_DV_THR,
    PRES_OFS_F_P_LIMIT,
    PRES_OFS_F_M_LIMIT,
    PRES_EEP_W_UPD_THR,
    PRES_EEP_W_UPD_LIMIT,
};
#else
#endif

#define SPC_5MSCTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Spc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SPC_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
#define SPC_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spc_MemMap.h"

#define SPC_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Spc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define SPC_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Spc_MemMap.h"

#define SPC_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Spc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define SPC_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Spc_MemMap.h"

#define SPC_5MSCTRL_START_SEC_VAR_32BIT
#include "Spc_MemMap.h"
/** Variable Section (32BIT)**/
#define SPC_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Spc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SPC_5MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

FAL_STATIC IdbSnsrOffsEolCalc_t     *pIdbSnsrOffsEolCalc;
FAL_STATIC IdbSnsrOffsEolEep_t      *pIdbSnsrOffsEolEep;
FAL_STATIC IdbSnsrOffsDrvgCalc_t    *pIdbSnsrOffsDrvgCalc;
FAL_STATIC IdbSnsrOffsDrvgEep_t     *pIdbSnsrOffsDrvgEep;
FAL_STATIC IdbSnsrOffsFinal_t       *pIdbSnsrOffsFinal;
FAL_STATIC const IdbSnsrOffsCal_t   *pIdbSnsrOffsCal;

FAL_STATIC IdbSnsrOffsEolCalc_t     PdtOffsEolCalc;
FAL_STATIC IdbSnsrOffsEolEep_t      PdtOffsEolEep;
FAL_STATIC IdbSnsrOffsDrvgCalc_t    PdtOffsDrvgCalc;
FAL_STATIC IdbSnsrOffsDrvgEep_t     PdtOffsDrvgEep;
FAL_STATIC IdbSnsrOffsFinal_t       PdtOffsFinal;


FAL_STATIC IdbSnsrOffsEolCalc_t     PdfOffsEolCalc;
FAL_STATIC IdbSnsrOffsEolEep_t      PdfOffsEolEep;
FAL_STATIC IdbSnsrOffsDrvgCalc_t    PdfOffsDrvgCalc;
FAL_STATIC IdbSnsrOffsDrvgEep_t     PdfOffsDrvgEep;
FAL_STATIC IdbSnsrOffsFinal_t       PdfOffsFinal;

FAL_STATIC IdbSnsrOffsEolCalc_t     PedlSimrPOffsEolCalc;
FAL_STATIC IdbSnsrOffsEolEep_t      PedlSimrPOffsEolEep;
FAL_STATIC IdbSnsrOffsDrvgCalc_t    PedlSimrPOffsDrvgCalc;
FAL_STATIC IdbSnsrOffsDrvgEep_t     PedlSimrPOffsDrvgEep;
FAL_STATIC IdbSnsrOffsFinal_t       PedlSimrPOffsFinal;

#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE

FAL_STATIC IdbSnsrOffsEolCalc_t     PistPOffsEolCalc;
FAL_STATIC IdbSnsrOffsEolEep_t      PistPOffsEolEep;
FAL_STATIC IdbSnsrOffsDrvgCalc_t    PistPOffsDrvgCalc;
FAL_STATIC IdbSnsrOffsDrvgEep_t     PistPOffsDrvgEep;
FAL_STATIC IdbSnsrOffsFinal_t       PistPOffsFinal;

#elif __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
FAL_STATIC IdbSnsrOffsEolCalc_t     PrimPOffsEolCalc;
FAL_STATIC IdbSnsrOffsEolCalc_t     SecdPOffsEolCalc;
FAL_STATIC IdbSnsrOffsEolEep_t      PrimPOffsEolEep;
FAL_STATIC IdbSnsrOffsEolEep_t      SecdPOffsEolEep;
FAL_STATIC IdbSnsrOffsDrvgCalc_t    PrimPOffsDrvgCalc;
FAL_STATIC IdbSnsrOffsDrvgCalc_t    SecdPOffsDrvgCalc;
FAL_STATIC IdbSnsrOffsDrvgEep_t     PrimPOffsDrvgEep;
FAL_STATIC IdbSnsrOffsDrvgEep_t     SecdPOffsDrvgEep;
FAL_STATIC IdbSnsrOffsFinal_t       PrimPOffsFinal;
FAL_STATIC IdbSnsrOffsFinal_t       SecdPOffsFinal;

#else
#endif

FAL_STATIC int32_t *p_s32NVMEolOffsReadVal;
FAL_STATIC int32_t *p_s32NVMEolOffsReadVal2;
FAL_STATIC int32_t *p_s32NVMDrvOffsReadVal;
FAL_STATIC int32_t *p_s32NVMDrvOffsReadVal2;

FAL_STATIC uint8_t Idbu8ActvBrkCtrlAct;
FAL_STATIC uint8_t Idbu8BrkSigChckOk;
FAL_STATIC uint8_t Idbu8ChckStsAftCtrl;
FAL_STATIC uint8_t Idbu8ChckStsInCtrl;
FAL_STATIC uint8_t Idbu8SnsrRawSigChckOk;

FAL_STATIC uint8_t Idbu8PedalEolOfsCalcEnd;
FAL_STATIC uint8_t Idbu8PressEolOfsCalcEnd;
FAL_STATIC uint8_t Idbu8PedalEolOfsCalcOk;
FAL_STATIC uint8_t Idbu8PressEolOfsCalcOk;

FAL_STATIC uint8_t Idbu8ReadBbsSnsrNVMDataEndFlg;

#define     SnsrOffsEolMinMaxLim        pIdbSnsrOffsCal->SnsrOffsEolMinMaxLim
#define     SnsrOffsEolMinMaxDifThd     pIdbSnsrOffsCal->SnsrOffsEolMinMaxDifThd
#define     SnsrOffsDrvgDeMaxThd        pIdbSnsrOffsCal->SnsrOffsDrvgDeMaxThd
#define     SnsrOffsDrvgCalcCdnChkCase  pIdbSnsrOffsCal->SnsrOffsDrvgCalcCdnChkCase
#define     SnsrOffsEolEepReadValLim    pIdbSnsrOffsCal->SnsrOffsEolEepReadValLim
#define     SnsrOffsDrvgEepReadValLim   pIdbSnsrOffsCal->SnsrOffsDrvgEepReadValLim
#define     SnsrOffsDrvEepUseThd        pIdbSnsrOffsCal->SnsrOffsDrvEepUseThd
#define     SnsrOffsDrvDrvUseThd        pIdbSnsrOffsCal->SnsrOffsDrvDrvUseThd
#define     SnsrOffsUpprLim             pIdbSnsrOffsCal->SnsrOffsUpprLim
#define     SnsrOffsLowrLim             pIdbSnsrOffsCal->SnsrOffsLowrLim
#define     SnsrOffsDrvEepUpdThd        pIdbSnsrOffsCal->SnsrOffsDrvEepUpdThd
#define     SnsrOffsDrvEepUpdLim        pIdbSnsrOffsCal->SnsrOffsDrvEepUpdLim

FAL_STATIC GetSnsrSigOffs_t GetSnsrSigOffs;
#define SPC_5MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spc_MemMap.h"

#define SPC_5MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Spc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define SPC_5MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Spc_MemMap.h"

#define SPC_5MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Spc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define SPC_5MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Spc_MemMap.h"

#define SPC_5MSCTRL_START_SEC_VAR_32BIT
#include "Spc_MemMap.h"
/** Variable Section (32BIT)**/
#define SPC_5MSCTRL_STOP_SEC_VAR_32BIT
#include "Spc_MemMap.h"

/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SPC_5MSCTRL_START_SEC_CODE
#include "Spc_MemMap.h"

static void LSIDB_vInpSigIfForSnsrOffsCalcn(void);
static void LSIDB_vOutpSigIfForSnsrOffsCalcn(void);
static void LSIDB_vCallPdtOffsCalcn(void);
static void LSIDB_vCallPdfOffsCalcn(void);
static void LSIDB_vCallPedlSimrPOffsCalcn(void);
#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
static void LSIDB_vCallPistPOffsCalcn(void);
#elif __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
static void LSIDB_vCallCircPOffsCalcn(void);
#endif
static void LSIDB_vCallSnsrOffsEolCalcn(void);
static void LSIDB_vDetSnsrOffsEol(void);
static void LSIDB_vCalcSnsrOffsEol(void);
static void LSIDB_vDlySnsrOffsEolCalcn(void);
static void LSIDB_vClrSnsrOffsEolData(void);
static void LSIDB_vClrSnsrOffsDrvgQuickData(void);

static void LSIDB_vWrReqSnsrOffsEol(void);

static void LSIDB_vCallSnsrOffsDrvgCalcn(void);
static void LSIDB_vDetSnsrOffsDrvgCalcCdn(void);
static void LSIDB_vDetSnsrOffsDrvgInhbCdn(void);
static void LSIDB_vDetSnsrSigNormCdn(void);

static void LSIDB_vCalcSnsrOffsDrvgQuick(void);
static void LSIDB_vDetSnsrOffsDrvg(void);
static void LSIDB_vCallSnsrOffsFinalDtmn(void);
static void LSIDB_vChkSnsrOffsEolEepSt(void);
static void LSIDB_vChkSnsrOffsDrvgEepSt(void);
static void LSIDB_vDetSnsrOffsFinal(void);
static void LSIDB_vWrReqSnsrOffsFinal(void);
static void LSIDB_vInterfaceSigOffsCalcn(void);

static uint8_t LSIDB_u8ChkNVMOffsReadInvld(uint16_t u16NVMReadOffsVal1, uint16_t u16NVMReadOffsData2);
static void LSIDB_vWrSnsrOffsToNVM(IdbSnsrOffsEolEep_t *p_IdbSnsrOffsEolEep, IdbSnsrOffsDrvgEep_t *p_IdbSnsrOffsDrvgEep);
static void LSIDB_vChkEolOffsCalcCdn(void);
static void LSIDB_vDecideEolOffsEndCdn(void);
static void LSIDB_vCalc1secDrvOffs(void);
static void LSIDB_vCalc60secDrvOffs(void);
static void LSIDB_vDetFinalOffsAfterDrv(void);
static void LSIDB_vDetFinalOffsAfterEol(void);
static void LSIDB_vDetFinalOffsBeforeCal(void);
static void LSIDB_vDetPdlOffsDrvgCalcCdn(void);
static void LSIDB_vDetPSPOffsDrvgCalcCdn(void);
static void LSIDB_vDetCircPOffsDrvgCalcCdn(void);
static void LSIDB_vDetEolCalEndCdn(void);
static uint8_t LSIDB_u8ChckSnsrRawSig(void);
static void LSIDB_vDetSnsrOffsDrvgCalcCmnCdn(void);
static void vIDB_u8ReadBbsSnsrNVMData(void);
#if (NVM_RESET == ENABLE)
static void LSIDB_vResetNVMData(void);
#endif
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void LSIDB_vCallSnsrOffsCalcn(void)
{
	#if(NVM_RESET == ENABLE)
	LSIDB_vResetNVMData();
	#endif
    LSIDB_vInpSigIfForSnsrOffsCalcn();

    LSIDB_vCallPdtOffsCalcn();
    LSIDB_vCallPdfOffsCalcn();
    LSIDB_vCallPedlSimrPOffsCalcn();
#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
    LSIDB_vCallPistPOffsCalcn();
#elif __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
    LSIDB_vCallCircPOffsCalcn();
#else
#endif
    LSIDB_vDetEolCalEndCdn();
    LSIDB_vOutpSigIfForSnsrOffsCalcn();
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
#if (NVM_RESET == ENABLE)
static void LSIDB_vResetNVMData(void)
{
	uint8_t i = 0;
	NvMIf_LogicEepData.Eep.PedlTrvlOffsEepInfo.PdtOffsEolReadVal            = 0;
	NvMIf_LogicEepData.Eep.PedlTrvlOffsEepInfo.PdfOffsEolReadVal            = 0;
	NvMIf_LogicEepData.Eep.PedlTrvlOffsEepInfo.PdtOffsDrvgReadVal           = 0;
	NvMIf_LogicEepData.Eep.PedlTrvlOffsEepInfo.PdfOffsDrvgReadVal           = 0;
	NvMIf_LogicEepData.Eep.PedlSimrPOffsEepInfo.PedlSimrPOffsEolReadVal     = 0;
	NvMIf_LogicEepData.Eep.PedlSimrPOffsEepInfo.PedlSimrPOffsDrvgReadVal    = 0;

	NvMIf_LogicEepData.Eep.PistPOffsEepInfo.PistPOffsEolReadVal             = 0;
	NvMIf_LogicEepData.Eep.CircPOffsEepInfo.PrimCircPOffsEolReadVal         = 0;
	NvMIf_LogicEepData.Eep.CircPOffsEepInfo.SecdCircPOffsEolReadVal         = 0;

	NvMIf_LogicEepData.Eep.PistPOffsEepInfo.PistPOffsDrvgReadVal            = 0;
	NvMIf_LogicEepData.Eep.CircPOffsEepInfo.PrimCircPOffsDrvgReadVal        = 0;
	NvMIf_LogicEepData.Eep.CircPOffsEepInfo.SecdCircPOffsDrvgReadVal        = 0;

	NvMIf_LogicEepData.Eep.ReadInvldFlg                                     = 0;

	NvMIf_LogicEepData.Eep.PedlTrvlOffsEepInfo2.PdtOffsEolReadVal           = 0;
	NvMIf_LogicEepData.Eep.PedlTrvlOffsEepInfo2.PdfOffsEolReadVal           = 0;
	NvMIf_LogicEepData.Eep.PedlTrvlOffsEepInfo2.PdtOffsDrvgReadVal          = 0;
	NvMIf_LogicEepData.Eep.PedlTrvlOffsEepInfo2.PdfOffsDrvgReadVal          = 0;
	NvMIf_LogicEepData.Eep.PedlSimrPOffsEepInfo2.PedlSimrPOffsEolReadVal    = 0;
	NvMIf_LogicEepData.Eep.PedlSimrPOffsEepInfo2.PedlSimrPOffsDrvgReadVal   = 0;

	NvMIf_LogicEepData.Eep.PistPOffsEepInfo2.PistPOffsEolReadVal            = 0;
	NvMIf_LogicEepData.Eep.CircPOffsEepInfo2.PrimCircPOffsEolReadVal        = 0;
	NvMIf_LogicEepData.Eep.CircPOffsEepInfo2.SecdCircPOffsEolReadVal        = 0;

	NvMIf_LogicEepData.Eep.PistPOffsEepInfo2.PistPOffsDrvgReadVal           = 0;
	NvMIf_LogicEepData.Eep.CircPOffsEepInfo2.PrimCircPOffsDrvgReadVal       = 0;
	NvMIf_LogicEepData.Eep.CircPOffsEepInfo2.SecdCircPOffsDrvgReadVal       = 0;

	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.KSTEER_offset_of_eeprom        = 0;
	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.KYAW_offset_of_eeprom          = 0;
	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.KLAT_offset_of_eeprom          = 0;
	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.fs_yaw_eeprom_offset           = 0;
	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.KYAW_eep_max                   = 0;
	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.KYAW_eep_min                   = 0;
	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.KSTEER_eep_max                 = 0;
	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.KSTEER_eep_min                 = 0;
	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.KLAT_eep_max                   = 0;
	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.KLAT_eep_min                   = 0;
	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.yaw_still_eep_max              = 0;
	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.yaw_still_eep_min              = 0;
	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.lsesps16EEPYawStandOfs         = 0;

	for(i = 0; i < 31 ; i++)
	{
		NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.YawTmpEEPMap[i]               = 0;
		NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEep2Info.YawTmpEEPMap_c[i]            = 0;
	}

	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEep2Info.KSTEER_offset_of_eeprom_c     = 0;
	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEep2Info.KYAW_offset_of_eeprom_c       = 0;
	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEep2Info.KLAT_offset_of_eeprom_c       = 0;
	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEep2Info.fs_yaw_eeprom_offset_c        = 0;
	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEep2Info.KYAW_eep_max_c                = 0;
	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEep2Info.KYAW_eep_min_c                = 0;
	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEep2Info.KSTEER_eep_max_c              = 0;
	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEep2Info.KSTEER_eep_min_c              = 0;
	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEep2Info.KLAT_eep_max_c                = 0;
	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEep2Info.KLAT_eep_min_c                = 0;
	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEep2Info.yaw_still_eep_max_c           = 0;
	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEep2Info.yaw_still_eep_min_c           = 0;
	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEep2Info.lsesps16EEPYawStandOfs_c      = 0;

	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.AdpvVehMdlVchEep               = 0;
	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.AdpvVehMdlVcrtRatEep           = 0;

	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.btc_tmp_fl_Eep                 = 0;
	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.btc_tmp_fr_Eep                 = 0;
	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.btc_tmp_rl_Eep                 = 0;
	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.btc_tmp_rr_Eep                 = 0;
	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.ee_btcs_data_Eep               = 0;

	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.LgtSnsrEolOffsEep              = 0;
	NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.LgtSnsrDrvgOffsEep             = 0;
}
#endif

static void LSIDB_vInpSigIfForSnsrOffsCalcn(void)
{
    GetSnsrSigOffs.lsahbs16PDFRaw                  = (int16_t)Spc_5msCtrlBus.Spc_5msCtrlPdf5msRawInfo.PdfSig;
    GetSnsrSigOffs.lsahbs16PDTRaw                  = (int16_t)Spc_5msCtrlBus.Spc_5msCtrlPdt5msRawInfo.PdtSig;

    GetSnsrSigOffs.fsu1PdtSenFaultDet              = Spc_5msCtrlBus.Spc_5msCtrlEemFailData.Eem_Fail_PedalPDT;
    GetSnsrSigOffs.fdu1PdtSigSusSet                = Spc_5msCtrlBus.Spc_5msCtrlEemSuspectData.Eem_Suspect_PedalPDT;
    GetSnsrSigOffs.fsu1PdfSenFaultDet              = Spc_5msCtrlBus.Spc_5msCtrlEemFailData.Eem_Fail_PedalPDF;
    GetSnsrSigOffs.fdu1PdfSigSusSet                = Spc_5msCtrlBus.Spc_5msCtrlEemSuspectData.Eem_Suspect_PedalPDF;
/* Pedal simulator pressure signal interface */
    GetSnsrSigOffs.lsahbs16PSpresRaw               = (int16_t)Spc_5msCtrlBus.Spc_5msCtrlPedlSimPRaw;

    GetSnsrSigOffs.fsu1PSPSenFaultDet              = Spc_5msCtrlBus.Spc_5msCtrlEemFailData.Eem_Fail_SimP;
    GetSnsrSigOffs.fu1PspSusDet                    = Spc_5msCtrlBus.Spc_5msCtrlEemSuspectData.Eem_Suspect_SimP;

/* Boost pressure signal interface */
    #if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
    GetSnsrSigOffs.lsahbs16MCpresRaw                  = (int16_t)Spc_5msCtrlBus.Spc_5msCtrlPistP5msRawInfo.PistPSig;

    GetSnsrSigOffs.fsu1BCP1SenFaultDet              = Spc_5msCtrlBus.Spc_5msCtrlEemFailData.Eem_Fail_CirP1;
    GetSnsrSigOffs.fu1BcpPriSusDet                  = Spc_5msCtrlBus.Spc_5msCtrlEemSuspectData.Eem_Suspect_CirP1;

    #elif __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
    GetSnsrSigOffs.lsahbs16MCpresRaw               = (int16_t)Spc_5msCtrlBus.Spc_5msCtrlCircP5msRawInfo.PrimCircPSig;
    GetSnsrSigOffs.lsahbs16MC2presRaw              = (int16_t)Spc_5msCtrlBus.Spc_5msCtrlCircP5msRawInfo.SecdCircPSig;

    GetSnsrSigOffs.fsu1BCP1SenFaultDet             = Spc_5msCtrlBus.Spc_5msCtrlEemFailData.Eem_Fail_CirP1;
    GetSnsrSigOffs.fu1BcpPriSusDet                 = Spc_5msCtrlBus.Spc_5msCtrlEemSuspectData.Eem_Suspect_CirP1;
    GetSnsrSigOffs.fsu1BCP2SenFaultDet             = Spc_5msCtrlBus.Spc_5msCtrlEemFailData.Eem_Fail_CirP2;
    GetSnsrSigOffs.fu1BcpSecSusDet                 = Spc_5msCtrlBus.Spc_5msCtrlEemSuspectData.Eem_Suspect_CirP2;
    #endif

    GetSnsrSigOffs.cdu1DiagPedalTravelSnrCalReq    = Spc_5msCtrlBus.Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg;
    GetSnsrSigOffs.cdu1DiagPressureSnrCalReq       = Spc_5msCtrlBus.Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg;

    /* BLS interface*/
    GetSnsrSigOffs.fu1BLSErrDet                    = Spc_5msCtrlBus.Spc_5msCtrlEemFailData.Eem_Fail_BLS;
    GetSnsrSigOffs.fu1BlsSusDet                    = Spc_5msCtrlBus.Spc_5msCtrlEemSuspectData.Eem_Suspect_BLS;
    GetSnsrSigOffs.lsahbu1BLS                      = Spc_5msCtrlBus.Spc_5msCtrlBlsFild;

    #if __BRAKE_SWITCH_ENABLE == ENABLE
    GetSnsrSigOffs.fu1BSSignal                     = SIGPROCbus.BsFild;
    GetSnsrSigOffs.fu1BsErrDet                     = SIGPROCbus.BsInvldInfo.BsFault;
    GetSnsrSigOffs.fu1BsSusDet                     = SIGPROCbus.BsInvldInfo.BsSuspc;
    #endif

    GetSnsrSigOffs.lcahbu1AHBOn                    = Spc_5msCtrlBus.Spc_5msCtrlPCtrlAct;
    GetSnsrSigOffs.s16VehicleSpeed                 = (int16_t)Spc_5msCtrlBus.Spc_5msCtrlVehSpdFild;
    GetSnsrSigOffs.cu8EMS2AccPedDep_Pc_Err_Flg     = Spc_5msCtrlBus.Spc_5msCtrlCanRxAccelPedlInfo.AccelPedlValErr;
    GetSnsrSigOffs.liu1cEMSAccPedDep_Pc_Sus        = Spc_5msCtrlBus.Spc_5msCtrlCanRxAccelPedlInfo.AccelPedlValErr;
    GetSnsrSigOffs.lis16cEMSAccPedDep              = (int16_t)Spc_5msCtrlBus.Spc_5msCtrlCanRxAccelPedlInfo.AccelPedlVal;

    GetSnsrSigOffs.ldahbu1AbsActiveFlg             = Spc_5msCtrlBus.Spc_5msCtrlAbsCtrlInfo.AbsActFlg;
    GetSnsrSigOffs.ldahbu1EscActiveFlg             = Spc_5msCtrlBus.Spc_5msCtrlEscCtrlInfo.EscActFlg;
    GetSnsrSigOffs.liu1EscActiveBrk                = Spc_5msCtrlBus.Spc_5msCtrlActvBrkCtrlrActFlg;

    GetSnsrSigOffs.lsahbs16PedalTravelFilt         = (int16_t)Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlFinal;

    GetSnsrSigOffs.Idbu8CVVAct					   = Spc_5msCtrlBus.Spc_5msCtrlNormVlvReqVlvActInfo.PrimCutVlvReq;
    GetSnsrSigOffs.Idbu8CVV2Act                    = Spc_5msCtrlBus.Spc_5msCtrlNormVlvReqVlvActInfo.SecdCutVlvReq;
    GetSnsrSigOffs.Idbu8PressDumpVlvAct 		   = Spc_5msCtrlBus.Spc_5msCtrlNormVlvReqVlvActInfo.PressDumpVlvReq;

    if(Idbu8ReadBbsSnsrNVMDataEndFlg == 0)
    {
    	vIDB_u8ReadBbsSnsrNVMData();/* Read NVM Data once after Ign 0ff->On */
    	Idbu8ReadBbsSnsrNVMDataEndFlg = 1;
    }

    LSIDB_vInterfaceSigOffsCalcn();
}
static void LSIDB_vInterfaceSigOffsCalcn(void)
{
    /*To be removed*/
    PdtOffsEolCalc.lsahbu1EEPofsCalcFWStartFlgOLD       = PdtOffsEolCalc.lsahbu1EEPofsCalcFWStartFlg ;
    PdtOffsEolCalc.lsahbu1EEPofsCalcFWStartFlg          = GetSnsrSigOffs.cdu1DiagPedalTravelSnrCalReq;
    PdtOffsEolCalc.SnsrRawSig                           = GetSnsrSigOffs.lsahbs16PDTRaw;
    PdtOffsEolEep.lsahbs16AHBEolEEPofsReadRaw           = GetSnsrSigOffs.ws16PdtSnsrOfset;
    PdtOffsEolEep.lsahbu8AHBEEPofsReadInvalid           = GetSnsrSigOffs.wu1PdtSnsrOfSetInvalid ;
    PdtOffsDrvgCalc.SnsrRawSig                          = GetSnsrSigOffs.lsahbs16PDTRaw;
    PdtOffsDrvgCalc.lsahbu1SensorFaultDet				= GetSnsrSigOffs.fsu1PdtSenFaultDet;
    PdtOffsDrvgCalc.lsahbu1SensorSusDet					= GetSnsrSigOffs.fdu1PdtSigSusSet;
    PdtOffsDrvgEep.lsahbs16DrvEEPofsReadRaw             = GetSnsrSigOffs.ws16DrvPdtSnsrOfset ;
    PdtOffsDrvgEep.lsahbu8DrvEEPofsReadInvalid          = GetSnsrSigOffs.wu1DrvPdtSnsrOfSetInvalid;
    PdtOffsDrvgCalc.s16SnsrRawSigLim					= PDT1_OFS_LM_THR;

    PdfOffsEolCalc.lsahbu1EEPofsCalcFWStartFlgOLD       = PdfOffsEolCalc.lsahbu1EEPofsCalcFWStartFlg ;
    PdfOffsEolCalc.lsahbu1EEPofsCalcFWStartFlg          = GetSnsrSigOffs.cdu1DiagPedalTravelSnrCalReq;
    PdfOffsEolCalc.SnsrRawSig                           = GetSnsrSigOffs.lsahbs16PDFRaw;
    PdfOffsEolEep.lsahbs16AHBEolEEPofsReadRaw           = GetSnsrSigOffs.ws16PdfSnsrOfset;
    PdfOffsEolEep.lsahbu8AHBEEPofsReadInvalid           = GetSnsrSigOffs.wu1PdfSnsrOfSetInvalid;
    PdfOffsDrvgEep.lsahbs16DrvEEPofsReadRaw             = GetSnsrSigOffs.ws16DrvPdfSnsrOfset;
    PdfOffsDrvgEep.lsahbu8DrvEEPofsReadInvalid          = GetSnsrSigOffs.wu1DrvPdfSnsrOfSetInvalid;
    PdfOffsDrvgCalc.SnsrRawSig                          = GetSnsrSigOffs.lsahbs16PDFRaw;
    PdfOffsDrvgCalc.lsahbu1SensorFaultDet				= GetSnsrSigOffs.fsu1PdfSenFaultDet;
    PdfOffsDrvgCalc.lsahbu1SensorSusDet					= GetSnsrSigOffs.fdu1PdfSigSusSet;
    PdfOffsDrvgCalc.s16SnsrRawSigLim					= PDT1_OFS_LM_THR;

    PedlSimrPOffsEolCalc.lsahbu1EEPofsCalcFWStartFlgOLD = PedlSimrPOffsEolCalc.lsahbu1EEPofsCalcFWStartFlg ;
    PedlSimrPOffsEolCalc.lsahbu1EEPofsCalcFWStartFlg    = GetSnsrSigOffs.cdu1DiagPressureSnrCalReq;
    PedlSimrPOffsEolCalc.SnsrRawSig                     = GetSnsrSigOffs.lsahbs16PSpresRaw;
    PedlSimrPOffsEolEep.lsahbs16AHBEolEEPofsReadRaw     = GetSnsrSigOffs.ws16PSPressureOfset;
    PedlSimrPOffsEolEep.lsahbu8AHBEEPofsReadInvalid     = GetSnsrSigOffs.wu1PSPressureOfsetInvalid;
    PedlSimrPOffsDrvgEep.lsahbs16DrvEEPofsReadRaw       = GetSnsrSigOffs.ws16DrvPSPressureOfset;
    PedlSimrPOffsDrvgEep.lsahbu8DrvEEPofsReadInvalid    = GetSnsrSigOffs.wu1DrvPSPressureOfsetInvalid;
    PedlSimrPOffsDrvgCalc.SnsrRawSig                    = GetSnsrSigOffs.lsahbs16PSpresRaw;
    PedlSimrPOffsDrvgCalc.lsahbu1SensorFaultDet			= GetSnsrSigOffs.fsu1PSPSenFaultDet;
    PedlSimrPOffsDrvgCalc.lsahbu1SensorSusDet			= GetSnsrSigOffs.fu1PspSusDet;
    PedlSimrPOffsDrvgCalc.s16SnsrRawSigLim				= S16_CIRC_PRESS_15_BAR;

#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
    PistPOffsEolCalc.lsahbu1EEPofsCalcFWStartFlgOLD     = PistPOffsEolCalc.lsahbu1EEPofsCalcFWStartFlg;
    PistPOffsEolCalc.lsahbu1EEPofsCalcFWStartFlg        = GetSnsrSigOffs.cdu1DiagPressureSnrCalReq;
    PistPOffsEolEep.lsahbs16AHBEolEEPofsReadRaw         = GetSnsrSigOffs.ws16BCPressureOfset;
    PistPOffsEolEep.lsahbu8AHBEEPofsReadInvalid         = GetSnsrSigOffs.wu1BCPressureOfsetInvalid;
    PistPOffsDrvgEep.lsahbs16DrvEEPofsReadRaw           = GetSnsrSigOffs.ws16DrvBCPressureOfset;
    PistPOffsDrvgEep.lsahbu8DrvEEPofsReadInvalid        = GetSnsrSigOffs.wu1DrvBCPressureOfsetInvalid;

    PistPOffsEolCalc.SnsrRawSig                         = GetSnsrSigOffs.lsahbs16MCpresRaw;
    PistPOffsDrvgCalc.SnsrRawSig                        = GetSnsrSigOffs.lsahbs16MCpresRaw;
    PistPOffsDrvgCalc.lsahbu1SensorFaultDet				= GetSnsrSigOffs.fsu1BCP1SenFaultDet;
    PistPOffsDrvgCalc.lsahbu1SensorSusDet				= GetSnsrSigOffs.fu1BcpPriSusDet;
    PistPOffsDrvgCalc.s16SnsrRawSigLim					= S16_CIRC_PRESS_15_BAR;
#elif __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE

    PrimPOffsEolCalc.lsahbu1EEPofsCalcFWStartFlgOLD     = PrimPOffsEolCalc.lsahbu1EEPofsCalcFWStartFlg;
    PrimPOffsEolCalc.lsahbu1EEPofsCalcFWStartFlg        = GetSnsrSigOffs.cdu1DiagPressureSnrCalReq;
    PrimPOffsEolEep.lsahbs16AHBEolEEPofsReadRaw         = GetSnsrSigOffs.ws16BCPressureOfset;
    PrimPOffsEolEep.lsahbu8AHBEEPofsReadInvalid         = GetSnsrSigOffs.wu1BCPressureOfsetInvalid;
    PrimPOffsDrvgEep.lsahbs16DrvEEPofsReadRaw           = GetSnsrSigOffs.ws16DrvBCPressureOfset;
    PrimPOffsDrvgEep.lsahbu8DrvEEPofsReadInvalid        = GetSnsrSigOffs.wu1DrvBCPressureOfsetInvalid;

    PrimPOffsEolCalc.SnsrRawSig                         = GetSnsrSigOffs.lsahbs16MCpresRaw;
    PrimPOffsDrvgCalc.SnsrRawSig                        = GetSnsrSigOffs.lsahbs16MCpresRaw;
    PrimPOffsDrvgCalc.lsahbu1SensorFaultDet				= GetSnsrSigOffs.fsu1BCP1SenFaultDet;
    PrimPOffsDrvgCalc.lsahbu1SensorSusDet				= GetSnsrSigOffs.fu1BcpPriSusDet;
    PrimPOffsDrvgCalc.s16SnsrRawSigLim					= S16_CIRC_PRESS_15_BAR;

    SecdPOffsEolCalc.lsahbu1EEPofsCalcFWStartFlgOLD     = SecdPOffsEolCalc.lsahbu1EEPofsCalcFWStartFlg;
    SecdPOffsEolCalc.lsahbu1EEPofsCalcFWStartFlg        = GetSnsrSigOffs.cdu1DiagPressureSnrCalReq;
    SecdPOffsEolEep.lsahbs16AHBEolEEPofsReadRaw         = GetSnsrSigOffs.ws16BC2PressureOfset;
    SecdPOffsEolEep.lsahbu8AHBEEPofsReadInvalid         = GetSnsrSigOffs.wu1BC2PressureOfsetInvalid;
    SecdPOffsDrvgEep.lsahbs16DrvEEPofsReadRaw           = GetSnsrSigOffs.ws16DrvBC2PressureOfset;
    SecdPOffsDrvgEep.lsahbu8DrvEEPofsReadInvalid        = GetSnsrSigOffs.wu1DrvBC2PressureOfsetInvalid;

    SecdPOffsEolCalc.SnsrRawSig                         = GetSnsrSigOffs.lsahbs16MC2presRaw;
    SecdPOffsDrvgCalc.SnsrRawSig                        = GetSnsrSigOffs.lsahbs16MC2presRaw;
    SecdPOffsDrvgCalc.lsahbu1SensorFaultDet				= GetSnsrSigOffs.fsu1BCP2SenFaultDet;
    SecdPOffsDrvgCalc.lsahbu1SensorSusDet				= GetSnsrSigOffs.fu1BcpSecSusDet;
    SecdPOffsDrvgCalc.s16SnsrRawSigLim					= S16_CIRC_PRESS_15_BAR;
#else
#endif
}

static void LSIDB_vOutpSigIfForSnsrOffsCalcn(void)
{
	Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdtRawOffsCorrd        = GetSnsrSigOffs.lsahbs16PDTRaw - PdtOffsFinal.lsahbs16AHBOfs_f;
	Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdfRawOffsCorrd        = GetSnsrSigOffs.lsahbs16PDFRaw - PdfOffsFinal.lsahbs16AHBOfs_f;
	Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdtOffsCorrdStOkFlg    = PdtOffsFinal.lsahbu1AHBFinalOfsOK;
	Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdfOffsCorrdStOkFlg    = PdfOffsFinal.lsahbu1AHBFinalOfsOK;
	Spc_5msCtrlBus.Spc_5msCtrlPedlSimrPOffsCorrdInfo.PedlSimrPOffsCorrd    = GetSnsrSigOffs.lsahbs16PSpresRaw - PedlSimrPOffsFinal.lsahbs16AHBOfs_f;
	Spc_5msCtrlBus.Spc_5msCtrlPedlSimrPOffsCorrdInfo.PedlSimrPOffsCorrdStOkFlg     = PedlSimrPOffsFinal.lsahbu1AHBFinalOfsOK;

#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
	Spc_5msCtrlBus.Spc_5msCtrlPistPOffsCorrdInfo.PistPOffsCorrd            = GetSnsrSigOffs.lsahbs16MCpresRaw-PistPOffsFinal.lsahbs16AHBOfs_f;
	Spc_5msCtrlBus.Spc_5msCtrlPistPOffsCorrdInfo.PistPOffsCorrdStOkFlg     = PistPOffsFinal.lsahbu1AHBFinalOfsOK;
#elif __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
    Spc_5msCtrlBus.Spc_5msCtrlCircPOffsCorrdInfo.PrimCircPOffsCorrd            = GetSnsrSigOffs.lsahbs16MCpresRaw -PrimPOffsFinal.lsahbs16AHBOfs_f;
    Spc_5msCtrlBus.Spc_5msCtrlCircPOffsCorrdInfo.SecdCircPOffsCorrd            = GetSnsrSigOffs.lsahbs16MC2presRaw -SecdPOffsFinal.lsahbs16AHBOfs_f;

    Spc_5msCtrlBus.Spc_5msCtrlCircPOffsCorrdInfo.PrimCircPOffsCorrdStOkFlg     = PrimPOffsFinal.lsahbu1AHBFinalOfsOK;
    Spc_5msCtrlBus.Spc_5msCtrlCircPOffsCorrdInfo.SecdCircPOffsCorrdStOkFlg     = SecdPOffsFinal.lsahbu1AHBFinalOfsOK;
#else
#endif

    Spc_5msCtrlBus.Spc_5msCtrlIdbSnsrEolOfsCalcInfo.PedalEolOfsCalcEnd = Idbu8PedalEolOfsCalcEnd;
    Spc_5msCtrlBus.Spc_5msCtrlIdbSnsrEolOfsCalcInfo.PedalEolOfsCalcOk = Idbu8PedalEolOfsCalcOk;
    Spc_5msCtrlBus.Spc_5msCtrlIdbSnsrEolOfsCalcInfo.PressEolOfsCalcEnd = Idbu8PressEolOfsCalcEnd;
    Spc_5msCtrlBus.Spc_5msCtrlIdbSnsrEolOfsCalcInfo.PressEolOfsCalcOk = Idbu8PressEolOfsCalcOk;

    p_s32NVMEolOffsReadVal = &NVM_PdtOffsEolReadVal;
    p_s32NVMEolOffsReadVal2 = &NVM_PdtOffsEolReadVal2;
    p_s32NVMDrvOffsReadVal = &NVM_PdtOffsDrvgReadVal;
    p_s32NVMDrvOffsReadVal2 = &NVM_PdtOffsDrvgReadVal2;
    LSIDB_vWrSnsrOffsToNVM(&PdtOffsEolEep, &PdtOffsDrvgEep);

    p_s32NVMEolOffsReadVal = &NVM_PdfOffsEolReadVal;
    p_s32NVMEolOffsReadVal2 = &NVM_PdfOffsEolReadVal2;
    p_s32NVMDrvOffsReadVal = &NVM_PdfOffsDrvgReadVal;
    p_s32NVMDrvOffsReadVal2 = &NVM_PdfOffsDrvgReadVal2;
    LSIDB_vWrSnsrOffsToNVM(&PdfOffsEolEep, &PdfOffsDrvgEep);

    p_s32NVMEolOffsReadVal = &NVM_PedlSimrPOffsEolReadVal;
    p_s32NVMEolOffsReadVal2 = &NVM_PedlSimrPOffsEolReadVal2;
    p_s32NVMDrvOffsReadVal = &NVM_PedlSimrPOffsDrvgReadVal;
    p_s32NVMDrvOffsReadVal2 = &NVM_PedlSimrPOffsDrvgReadVal2;
    LSIDB_vWrSnsrOffsToNVM(&PedlSimrPOffsEolEep, &PedlSimrPOffsDrvgEep);

#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
    p_s32NVMEolOffsReadVal = &NVM_PistPOffsEolReadVal;
    p_s32NVMEolOffsReadVal2 = &NVM_PistPOffsEolReadVal2;
    p_s32NVMDrvOffsReadVal = &NVM_PistPOffsDrvgReadVal;
    p_s32NVMDrvOffsReadVal2 = &NVM_PistPOffsDrvgReadVal2;
    LSIDB_vWrSnsrOffsToNVM(&PistPOffsEolEep, &PistPOffsDrvgEep);

#else
    p_s32NVMEolOffsReadVal = &NVM_PrimCircPOffsEolReadVal;
    p_s32NVMEolOffsReadVal2 = &NVM_PrimCircPOffsEolReadVal2;
    p_s32NVMDrvOffsReadVal = &NVM_PrimCircPOffsDrvgReadVal;
    p_s32NVMDrvOffsReadVal2 = &NVM_PrimCircPOffsDrvgReadVal2;
    LSIDB_vWrSnsrOffsToNVM(&PrimPOffsEolEep, &PrimPOffsDrvgEep);

    p_s32NVMEolOffsReadVal = &NVM_SecdCircPOffsEolReadVal;
    p_s32NVMEolOffsReadVal2 = &NVM_SecdCircPOffsEolReadVal2;
    p_s32NVMDrvOffsReadVal = &NVM_SecdCircPOffsDrvgReadVal;
    p_s32NVMDrvOffsReadVal2 = &NVM_SecdCircPOffsDrvgReadVal2;
    LSIDB_vWrSnsrOffsToNVM(&SecdPOffsEolEep, &SecdPOffsDrvgEep);
#endif

	SIGPROCsrcbus.PdtOffs_f=PdtOffsFinal.lsahbs16AHBOfs_f;
#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
	SIGPROCsrcbus.PistPOffs_f =PistPOffsFinal.lsahbs16AHBOfs_f;
#elif __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
	SIGPROCsrcbus.PrimCircPOffs_f=PrimPOffsFinal.lsahbs16AHBOfs_f;
	SIGPROCsrcbus.SecdyCircPOffs_f=SecdPOffsFinal.lsahbs16AHBOfs_f;
#else
#endif
}

static void LSIDB_vCallPdtOffsCalcn(void)
{
    pIdbSnsrOffsEolCalc     = &PdtOffsEolCalc;
    pIdbSnsrOffsEolEep      = &PdtOffsEolEep;
    pIdbSnsrOffsDrvgCalc    = &PdtOffsDrvgCalc;
    pIdbSnsrOffsDrvgEep     = &PdtOffsDrvgEep;
    pIdbSnsrOffsCal         = (const IdbSnsrOffsCal_t *)&PdtOffsCal;
    pIdbSnsrOffsFinal       = &PdtOffsFinal;

    LSIDB_vCallSnsrOffsEolCalcn();
    LSIDB_vCallSnsrOffsDrvgCalcn();
    LSIDB_vCallSnsrOffsFinalDtmn();
}
static void LSIDB_vCallPdfOffsCalcn(void)
{
    pIdbSnsrOffsEolCalc  = &PdfOffsEolCalc;
    pIdbSnsrOffsEolEep   = &PdfOffsEolEep;
    pIdbSnsrOffsDrvgCalc = &PdfOffsDrvgCalc;
    pIdbSnsrOffsDrvgEep  = &PdfOffsDrvgEep;
    pIdbSnsrOffsCal      = (const IdbSnsrOffsCal_t *) &PdfOffsCal;
    pIdbSnsrOffsFinal    = &PdfOffsFinal;

    LSIDB_vCallSnsrOffsEolCalcn();
    LSIDB_vCallSnsrOffsDrvgCalcn();
    LSIDB_vCallSnsrOffsFinalDtmn();
}
static void LSIDB_vCallPedlSimrPOffsCalcn(void)
{
    pIdbSnsrOffsEolCalc     = &PedlSimrPOffsEolCalc;
    pIdbSnsrOffsEolEep      = &PedlSimrPOffsEolEep;
    pIdbSnsrOffsDrvgCalc    = &PedlSimrPOffsDrvgCalc;
    pIdbSnsrOffsDrvgEep     = &PedlSimrPOffsDrvgEep;
    pIdbSnsrOffsCal         = (const IdbSnsrOffsCal_t *)&PedlSimrPOffsCal;
    pIdbSnsrOffsFinal       = &PedlSimrPOffsFinal;

    LSIDB_vCallSnsrOffsEolCalcn();
    LSIDB_vCallSnsrOffsDrvgCalcn();
    LSIDB_vCallSnsrOffsFinalDtmn();
}
#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
static void LSIDB_vCallPistPOffsCalcn(void)
{
    pIdbSnsrOffsEolCalc     = &PistPOffsEolCalc;
    pIdbSnsrOffsEolEep      = &PistPOffsEolEep;
    pIdbSnsrOffsDrvgCalc    = &PistPOffsDrvgCalc;
    pIdbSnsrOffsDrvgEep     = &PistPOffsDrvgEep;
    pIdbSnsrOffsCal         = (const IdbSnsrOffsCal_t *)&PistPOffsCal;
    pIdbSnsrOffsFinal       = &PistPOffsFinal;

    LSIDB_vCallSnsrOffsEolCalcn();
    LSIDB_vCallSnsrOffsDrvgCalcn();
    LSIDB_vCallSnsrOffsFinalDtmn();
}
#elif  __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
static void LSIDB_vCallCircPOffsCalcn(void)
{
    pIdbSnsrOffsEolEep      = &PrimPOffsEolEep;
    pIdbSnsrOffsEolCalc     = &PrimPOffsEolCalc;
    pIdbSnsrOffsDrvgCalc    = &PrimPOffsDrvgCalc;
    pIdbSnsrOffsDrvgEep     = &PrimPOffsDrvgEep;
    pIdbSnsrOffsCal         = (const IdbSnsrOffsCal_t *)&PrimPOffsCal;
    pIdbSnsrOffsFinal       = &PrimPOffsFinal;

    LSIDB_vCallSnsrOffsEolCalcn();
    LSIDB_vCallSnsrOffsDrvgCalcn();
    LSIDB_vCallSnsrOffsFinalDtmn();

    pIdbSnsrOffsEolEep      = &SecdPOffsEolEep;
    pIdbSnsrOffsEolCalc     = &SecdPOffsEolCalc;
    pIdbSnsrOffsDrvgCalc    = &SecdPOffsDrvgCalc;
    pIdbSnsrOffsDrvgEep     = &SecdPOffsDrvgEep;
    pIdbSnsrOffsCal         = (const IdbSnsrOffsCal_t *)&SecdPOffsCal;
    pIdbSnsrOffsFinal       = &SecdPOffsFinal;

    LSIDB_vCallSnsrOffsEolCalcn();
    LSIDB_vCallSnsrOffsDrvgCalcn();
    LSIDB_vCallSnsrOffsFinalDtmn();
}
#else
#endif

static void LSIDB_vCallSnsrOffsEolCalcn(void)
{
    LSIDB_vDetSnsrOffsEol();
    LSIDB_vWrReqSnsrOffsEol();
}

/*******************************************************************************
* FUNCTION NAME:        LSIDB_vDetSnsrOffsEol
* CALLED BY:            LSIDB_vCallSnsrOffsEolCalcn
* PARAMETER:			void
* RETURN VALUE:         void
* Description:			Main function for calculating EOL Offset
* Metrics
********************************************************************************/
static void LSIDB_vDetSnsrOffsEol(void)
{
	LSIDB_vChkEolOffsCalcCdn();

    /*========= EEPROM During Calc Loop Main Logic   ======== */
    if (pIdbSnsrOffsEolCalc->lsahbu1AHBeepCalcLoopOK == 1)
    {
        /*---------= EEPROM During Calc Condition  ----------- */
        if ((GetSnsrSigOffs.lsahbu1BLS == 1) && (GetSnsrSigOffs.fu1BLSErrDet == 0))
        {
            pIdbSnsrOffsEolCalc->lsahbu1AHBeepWhileOK = 0;
        }
        else
        {
            pIdbSnsrOffsEolCalc->lsahbu1AHBeepWhileOK = 1;
        }

        if(pIdbSnsrOffsEolCalc->lsahbu1EEPofsCalcCLStartFlg==1)
        {
            if( pIdbSnsrOffsEolCalc->lsahbu1AHBEepOfsCalEndFlg==1 )
            {
                pIdbSnsrOffsEolCalc->lsahbu1EEPofsCalcCLStartFlg= 0;
            }
            else
            {
                if((pIdbSnsrOffsEolCalc->lsahbu8AHBEEPofsMaxMinLMOverCnt <= 0) || (pIdbSnsrOffsEolCalc->lsahbu1EEPOfsEepReCalcOK==1))
                {
                	LSIDB_vCalcSnsrOffsEol();
                }
                else
                {
                	LSIDB_vDlySnsrOffsEolCalcn();
                }
            }
        }
        else
        {
            if( (pIdbSnsrOffsEolCalc->lsahbu1EEPofsCalcFWStartFlg ==1) && (pIdbSnsrOffsEolCalc->lsahbu1AHBeepEnterOK ==1 ) && (pIdbSnsrOffsEolCalc->lsahbu1EEPofsCalcFWStartFlgIni==1) )
            {
                pIdbSnsrOffsEolCalc->lsahbu1EEPofsCalcCLStartFlg=1;

                /* Clear Data before calculating */
                LSIDB_vClrSnsrOffsEolData();
                #if  __IDB_DRV_OFFSET == ENABLE
                LSIDB_vClrSnsrOffsDrvgQuickData();
                #endif
            }
            else
            {
                ;
            }
        }
    }
    else
    {
        ;
    }
}

/*******************************************************************************
* FUNCTION NAME:        LSIDB_vCalcSnsrOffsEol
* CALLED BY:            LSIDB_vDetSnsrOffsEol
* PARAMETER:			void
* RETURN VALUE:         void
* Description:			Main function for calculating EOL Offset
* Metrics
********************************************************************************/
static void LSIDB_vCalcSnsrOffsEol(void)
{
    int16_t  lsahbs16EEPOfsCalcMaxMinTemp;
    int16_t SnsrRawSigTmp = pIdbSnsrOffsEolCalc->SnsrRawSig;

    if((pIdbSnsrOffsEolCalc->lsahbu1EEPofsCalcFWStartFlg==1) && (pIdbSnsrOffsEolCalc->lsahbu1AHBeepWhileOK==1))  /* MUST Consider BLS Fail condition! */
    {
        if(pIdbSnsrOffsEolCalc->lsahbu8AHBofsFirstCnt < EEP_CALC_T) /* Add RawSig for 500ms */
        {
            if(pIdbSnsrOffsEolCalc->lsahbu8AHBofsFirstCnt==0)
            {
                pIdbSnsrOffsEolCalc->lsahbs32AHBOfsFirstSum     = 0;
                pIdbSnsrOffsEolCalc->lsahbs16AHBofsFirstCalMax  = SnsrRawSigTmp;
                pIdbSnsrOffsEolCalc->lsahbs16AHBofsFirstCalMin  = SnsrRawSigTmp;
                pIdbSnsrOffsEolCalc->lsahbs32AHBOfsFirstSum     = pIdbSnsrOffsEolCalc->lsahbs32AHBOfsFirstSum  + SnsrRawSigTmp;
            }
            else
            {
                pIdbSnsrOffsEolCalc->lsahbs32AHBOfsFirstSum=pIdbSnsrOffsEolCalc->lsahbs32AHBOfsFirstSum + SnsrRawSigTmp;

                pIdbSnsrOffsEolCalc->lsahbs16AHBofsFirstCalMax = (pIdbSnsrOffsEolCalc->lsahbs16AHBofsFirstCalMax < SnsrRawSigTmp) ? SnsrRawSigTmp : pIdbSnsrOffsEolCalc->lsahbs16AHBofsFirstCalMax;
                pIdbSnsrOffsEolCalc->lsahbs16AHBofsFirstCalMin = (pIdbSnsrOffsEolCalc->lsahbs16AHBofsFirstCalMin > SnsrRawSigTmp) ? SnsrRawSigTmp : pIdbSnsrOffsEolCalc->lsahbs16AHBofsFirstCalMin;

            }
            pIdbSnsrOffsEolCalc->lsahbu8AHBofsFirstCnt=pIdbSnsrOffsEolCalc->lsahbu8AHBofsFirstCnt+1;
        }
        else
        {
            pIdbSnsrOffsEolCalc->lsahbs16AHBofsFirstCal             = (int16_t)(pIdbSnsrOffsEolCalc->lsahbs32AHBOfsFirstSum/(EEP_CALC_T) );
            lsahbs16EEPOfsCalcMaxMinTemp                            = pIdbSnsrOffsEolCalc->lsahbs16AHBofsFirstCalMax-pIdbSnsrOffsEolCalc->lsahbs16AHBofsFirstCalMin ;

            /* Check Min/Max Range - Pedal : 25mm, Press : 15Bar*/
            if(  L_s16Abs(pIdbSnsrOffsEolCalc->lsahbs16AHBofsFirstCal)<= SnsrOffsEolMinMaxLim  )
            {
                pIdbSnsrOffsEolCalc->lsahbu1EEPofsEEPLimitOK=1;
            }
            else
            {
                pIdbSnsrOffsEolCalc->lsahbu1EEPofsEEPLimitOK=0;
            }

            /* Check Max-Min RawSignal Range - Pedal : 2mm, Press : 2Bar */
            if(  lsahbs16EEPOfsCalcMaxMinTemp <= SnsrOffsEolMinMaxDifThd )
            {
                pIdbSnsrOffsEolCalc->lsahbu1EEPofsEEPMaxMinOK   = 1;
            }
            else
            {
                pIdbSnsrOffsEolCalc->lsahbu1EEPofsEEPMaxMinOK=0;
            }
            pIdbSnsrOffsEolCalc->lsahbu8AHBEEPofsMaxMinLMOverCnt    = pIdbSnsrOffsEolCalc->lsahbu8AHBEEPofsMaxMinLMOverCnt + 1;/* Repeat Calculation again if not satisfy limit conditions  */

            LSIDB_vDecideEolOffsEndCdn();

            pIdbSnsrOffsEolCalc->lsahbu8AHBofsFirstCnt      = 0;
            pIdbSnsrOffsEolCalc->lsahbu8EEPOfsDelyCnt       = 0;
            pIdbSnsrOffsEolCalc->lsahbu1EEPOfsEepReCalcOK   = 0;
        }
    }
    else
    {
        pIdbSnsrOffsEolCalc->lsahbu1AHBEepOfsCalEndFlg = 1;
    }
}

/*******************************************************************************
* FUNCTION NAME:        LSIDB_vDlySnsrOffsEolCalcn
* CALLED BY:            LSIDB_vDetSnsrOffsEol
* PARAMETER:			void
* RETURN VALUE:         void
* Description:			Delay 1s after first calculation if limit conditions
* 						are not satisfied
* Metrics
********************************************************************************/
static void LSIDB_vDlySnsrOffsEolCalcn(void)
{
    pIdbSnsrOffsEolCalc->lsahbu8EEPOfsDelyCnt   = pIdbSnsrOffsEolCalc->lsahbu8EEPOfsDelyCnt +1 ;

    if(pIdbSnsrOffsEolCalc->lsahbu8EEPOfsDelyCnt > EEP_DELAY_T )
    {
        pIdbSnsrOffsEolCalc->lsahbu1EEPOfsEepReCalcOK =1 ;
    }
    else
    {
        ;
    }
}
static void LSIDB_vClrSnsrOffsEolData(void)
{
    pIdbSnsrOffsEolCalc->lsahbu8AHBEEPofsMaxMinLMOverCnt    = 0;
    pIdbSnsrOffsEolCalc->lsahbu8AHBofsFirstCnt              = 0;
    pIdbSnsrOffsEolCalc->lsahbu1EEPofsEEPLimitOK            = 0;
    pIdbSnsrOffsEolCalc->lsahbu1EEPofsEEPMaxMinOK           = 0;
    pIdbSnsrOffsEolCalc->lsahbu1AHBEepOfsCalEndFlg          = 0;
    pIdbSnsrOffsEolCalc->lsahbu1AHBEepOfsCalOkFlg           = 0;

    pIdbSnsrOffsEolCalc->lsahbu8EEPOfsDelyCnt               = 0;
    pIdbSnsrOffsEolCalc->lsahbu1EEPOfsEepReCalcOK           = 0;

    pIdbSnsrOffsEolEep->lsahbu1AHBEolOfsWriteReqOK          = 0;
}
static void LSIDB_vClrSnsrOffsDrvgQuickData(void)
{
    pIdbSnsrOffsDrvgCalc->lsahbu8AHBDrvOfsCnt            = 0;
    pIdbSnsrOffsDrvgCalc->lsahbu8AHBDrvOfsArrayCnt       = 0;
    pIdbSnsrOffsDrvgCalc->lsahbs16AHBDrvOfsArrayMin      = 0;
    pIdbSnsrOffsDrvgCalc->lsahbs16AHBDrvOfsArrayFilter   = 0;
    pIdbSnsrOffsDrvgCalc->lsahbs16AHBDrvOfsArrayFConv    = 0;
    pIdbSnsrOffsDrvgCalc->lsahbs16AHBDrvOfs_f            = 0;
    pIdbSnsrOffsDrvgCalc->lsahbs16AHBDrvMin              = 0;
    pIdbSnsrOffsDrvgCalc->lsahbu8AHBDrvOfs10sArrayCnt    = 0;
    pIdbSnsrOffsDrvgCalc->lsahbu1AHBDrv10secOfsOK        = 0;
    pIdbSnsrOffsDrvgCalc->lsahbu1AHBDrv1secOfsOK         = 0;
    pIdbSnsrOffsDrvgCalc->lsahbu1AHBDrv60secOfsReOK      = 0;
    pIdbSnsrOffsDrvgCalc->lsahbu1AHBDrv60secOfsOK        = 0;
    pIdbSnsrOffsDrvgCalc->lsahbu1AHBDrvOfsOK             = 0;

    pIdbSnsrOffsDrvgEep->lsahbu1AHBDrvEEPofsUseOK   =0;
}

/*******************************************************************************
* FUNCTION NAME:        LSIDB_vWrReqSnsrOffsEol
* CALLED BY:            LSIDB_vCallSnsrOffsEolCalcn
* PARAMETER:			void
* RETURN VALUE:         void
* Description:			Send Request to write Eol Calculation data to NVM
* Metrics
********************************************************************************/
static void LSIDB_vWrReqSnsrOffsEol(void)
{
    if(pIdbSnsrOffsEolEep->lsahbu1AHBEepOfsWriteReq ==1 )
    {
    	if(pIdbSnsrOffsEolEep->lsahbu1AHBEepOfsWriteOkFlg ==1 )
    	{
    		pIdbSnsrOffsEolEep->lsahbu1AHBEepOfsWriteReq    = 0;
    		pIdbSnsrOffsEolEep->lsahbu1AHBEolOfsWriteReqOK  = 1;
    	}

        pIdbSnsrOffsEolEep->lsIdbs16EolOffsEepWr=pIdbSnsrOffsEolCalc->lsahbs16AHBofsFirstCal; /*Timing Error To be removed*/
    }
    else
    {
        if( (pIdbSnsrOffsEolCalc->lsahbu1AHBEepOfsCalOkFlg ==1) && (pIdbSnsrOffsEolEep->lsahbu1AHBEolOfsWriteReqOK==0) )  /*start flag*/
        {
            pIdbSnsrOffsEolEep->lsahbu1AHBEepOfsWriteReq    = 1;
            pIdbSnsrOffsEolEep->lsIdbs16EolOffsEepWr = pIdbSnsrOffsEolCalc->lsahbs16AHBofsFirstCal;
        }
        else
        {
           ;
        }
    }
}

static void LSIDB_vCallSnsrOffsDrvgCalcn(void)
{
    LSIDB_vDetSnsrOffsDrvgInhbCdn();
    LSIDB_vDetSnsrOffsDrvgCalcCdn();
    LSIDB_vCalcSnsrOffsDrvgQuick();
    LSIDB_vDetSnsrOffsDrvg();

}
static void LSIDB_vDetSnsrOffsDrvgCalcCdn(void)
{
	LSIDB_vDetSnsrOffsDrvgCalcCmnCdn();
	LSIDB_vDetSnsrSigNormCdn();/* output : lsahbu1SensorNormCond, lsahbu1AHBact1secAfterOK */

    switch(SnsrOffsDrvgCalcCdnChkCase)
    {
        case PDT_OFFSET_CALC_CASE:
        case PDF_OFFSET_CALC_CASE:
        	LSIDB_vDetPdlOffsDrvgCalcCdn();
            break;

        case PEDAL_SIM_P_OFFSET_CALC_CASE:
        	LSIDB_vDetPSPOffsDrvgCalcCdn();
            break;


#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
        case PIST_P_OFFSET_CALC_CASE:
#elif __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
        case PRIM_P_OFFSET_CALC_CASE:
        case SECD_P_OFFSET_CALC_CASE:
#else
#endif
        	LSIDB_vDetCircPOffsDrvgCalcCdn();
            break;

        default:
        	break;
    }
}

/*******************************************************************************
* FUNCTION NAME:        LSIDB_vDetSnsrOffsDrvgInhbCdn
* CALLED BY:            LSIDB_vCallSnsrOffsDrvgCalcn
* PARAMETER:			void
* RETURN VALUE:         void
* Description:			Determine Actuation Control is end or not for 1s
* Metrics
********************************************************************************/
static void LSIDB_vDetSnsrOffsDrvgInhbCdn(void)
{
    /* -- AHB Act After 1sec OK --*/

    if(pIdbSnsrOffsDrvgCalc->lsahbu1AHBact1secAfterOK == 1)
    {
        if(GetSnsrSigOffs.lcahbu1AHBOn == 1)
        {
            pIdbSnsrOffsDrvgCalc->lsahbu1AHBact1secAfterOK  = 0;
        }
        else
        {
            ;
        }

        pIdbSnsrOffsDrvgCalc->ldahbu8AHBactAfterCNT = 0;
    }
    else
    {
        if(GetSnsrSigOffs.lcahbu1AHBOn == 0)
        {
            pIdbSnsrOffsDrvgCalc->ldahbu8AHBactAfterCNT = pIdbSnsrOffsDrvgCalc->ldahbu8AHBactAfterCNT+1;
            pIdbSnsrOffsDrvgCalc->ldahbu8AHBactAfterCNT = (uint8_t)L_s16LimitMinMax( (int16_t)pIdbSnsrOffsDrvgCalc->ldahbu8AHBactAfterCNT, 0, U16_T_1_S);

            if(pIdbSnsrOffsDrvgCalc->ldahbu8AHBactAfterCNT == U16_T_1_S )
            {
                pIdbSnsrOffsDrvgCalc->lsahbu1AHBact1secAfterOK  = 1;
                pIdbSnsrOffsDrvgCalc->ldahbu8AHBactAfterCNT     = 0;
            }
            else
            {
                ;
            }
        }
        else
        {
            pIdbSnsrOffsDrvgCalc->ldahbu8AHBactAfterCNT = 0;
        }
    }
}

/*******************************************************************************
* FUNCTION NAME:        LSIDB_vDetSnsrSigNormCdn
* CALLED BY:            LSIDB_vDetSnsrOffsDrvgCalcCdn
* PARAMETER:			void
* RETURN VALUE:         void
* Description:			Check Sensor Signal Normal Condition
* 						Sum Raw Signal for 250ms twice and check Range
* Metrics
********************************************************************************/
static void LSIDB_vDetSnsrSigNormCdn(void)
{
    /*-- Signal is within limitation --*/
     int16_t lsahbs16DiffSensorRawTemp;
     int16_t SnsrRawSigTmp = pIdbSnsrOffsDrvgCalc->SnsrRawSig;
     int16_t lsahbs16absAHBNormCondMeanDiff;
     int16_t lsahbs16absDiffSensorRawTemp;

     /*-- 250msec Signal Calculation --*/
     if(pIdbSnsrOffsDrvgCalc->lsahbu8AHBNormCondCnt > U8_T_250_MS)
     {
         pIdbSnsrOffsDrvgCalc->lsahbu8AHBNormCondCnt      = 0;
         pIdbSnsrOffsDrvgCalc->lsahbs16AHBNormCondMeanOld = pIdbSnsrOffsDrvgCalc->lsahbs16AHBNormCondMean;
         pIdbSnsrOffsDrvgCalc->lsahbs16AHBNormCondMean    = (int16_t)(pIdbSnsrOffsDrvgCalc->lsahbs32AHBNormCondSum/U8_T_250_MS) ;
         pIdbSnsrOffsDrvgCalc->lsahbs32AHBNormCondSum     = 0;

         pIdbSnsrOffsDrvgCalc->lsahbu8AHBDrvModeDeltTCnt = pIdbSnsrOffsDrvgCalc->lsahbu8AHBDrvModeDeltTCnt + 1;
         pIdbSnsrOffsDrvgCalc->lsahbu8AHBDrvModeDeltTCnt = (uint8_t)(L_s16LimitMinMax( ((int16_t)pIdbSnsrOffsDrvgCalc->lsahbu8AHBDrvModeDeltTCnt) , 0, 200) );

         if(pIdbSnsrOffsDrvgCalc->lsahbu1NormCondFirstCheck==0)
         {
             pIdbSnsrOffsDrvgCalc->lsahbu1NormCondFirstCheck   = 1;
             pIdbSnsrOffsDrvgCalc->lsahbs16AHBNormCondMeanDiff = 0;

             pIdbSnsrOffsDrvgCalc->lsahbs16AHBDrvMin  =  pIdbSnsrOffsDrvgCalc->lsahbs16AHBNormCondMean ;
         }
         else
         {
             pIdbSnsrOffsDrvgCalc->lsahbs16AHBNormCondMeanDiff =  pIdbSnsrOffsDrvgCalc->lsahbs16AHBNormCondMean - pIdbSnsrOffsDrvgCalc->lsahbs16AHBNormCondMeanOld ;

             /* Drv Min value Calculation */
             if(pIdbSnsrOffsDrvgCalc->lsahbs16AHBNormCondMean < pIdbSnsrOffsDrvgCalc->lsahbs16AHBDrvMin  )
             {
                 pIdbSnsrOffsDrvgCalc->lsahbs16AHBDrvMin = pIdbSnsrOffsDrvgCalc->lsahbs16AHBNormCondMean ;
             }
             else
             {
                 ;
             }
         }
     }
     else
     {
         pIdbSnsrOffsDrvgCalc->lsahbu8AHBNormCondCnt =  pIdbSnsrOffsDrvgCalc->lsahbu8AHBNormCondCnt+1 ;
         pIdbSnsrOffsDrvgCalc->lsahbs32AHBNormCondSum = pIdbSnsrOffsDrvgCalc->lsahbs32AHBNormCondSum + SnsrRawSigTmp;
     }


     lsahbs16DiffSensorRawTemp = SnsrRawSigTmp - pIdbSnsrOffsDrvgCalc->lsahbs16AHBNormCondMean;

     lsahbs16absAHBNormCondMeanDiff = L_s16Abs(pIdbSnsrOffsDrvgCalc->lsahbs16AHBNormCondMeanDiff);/* Pdt : 1mm, Pdf : 2mm, Press : 1bar */
     lsahbs16absDiffSensorRawTemp = L_s16Abs(lsahbs16DiffSensorRawTemp); /* Pdt : 1mm, Pdf : 2mm, Press : 1bar */

     /* -- 250msec Mean Difference , Stable Decision ---*/
     if( ( pIdbSnsrOffsDrvgCalc->lsahbu8AHBDrvModeDeltTCnt >=2)  &&
         (lsahbs16absAHBNormCondMeanDiff < SnsrOffsDrvgDeMaxThd )&& (lsahbs16absDiffSensorRawTemp < SnsrOffsDrvgDeMaxThd )  )
     {
         pIdbSnsrOffsDrvgCalc->lsahbu1SensorNormCond = 1;
     }
     else
     {
         pIdbSnsrOffsDrvgCalc->lsahbu1SensorNormCond = 0;
     }

}

static void LSIDB_vCalcSnsrOffsDrvgQuick(void)
{
    if( pIdbSnsrOffsDrvgCalc->lsahbu1SensorDrvOfsCheckCond ==1 )
    {
        if(pIdbSnsrOffsDrvgCalc->lsahbu8AHBDrvOfsCnt >= U16_T_1_S )
        {
        	LSIDB_vCalc1secDrvOffs();
        	LSIDB_vCalc60secDrvOffs();
        }
        else
        {
            pIdbSnsrOffsDrvgCalc->lsahbu8AHBDrvOfsCnt  = pIdbSnsrOffsDrvgCalc->lsahbu8AHBDrvOfsCnt+1;
            pIdbSnsrOffsDrvgCalc->lsahbs32AHBDrvOfs1secSum += pIdbSnsrOffsDrvgCalc->SnsrRawSig; /* Sum Raw Signal for 1s */
        }
    }
    else
    {
        pIdbSnsrOffsDrvgCalc->lsahbu8AHBDrvOfsCnt   = 0;
        pIdbSnsrOffsDrvgCalc->lsahbs32AHBDrvOfs1secSum = 0;
    }
}

static void LSIDB_vDetSnsrOffsDrvg(void)
{
    pIdbSnsrOffsDrvgCalc->lsahbu1AHBDrvOfsOK = pIdbSnsrOffsDrvgCalc->lsahbu1AHBDrv60secOfsOK ;

    if(pIdbSnsrOffsDrvgCalc->lsahbu1AHBDrvOfsOK ==1 )
    {
        pIdbSnsrOffsDrvgCalc->lsahbs16AHBDrvOfs_f = pIdbSnsrOffsDrvgCalc->lsahbs16AHBDrvOfsArrayFilter;
    }
    else
    {
        if(pIdbSnsrOffsDrvgCalc->lsahbu1AHBDrv1secOfsOK ==1)
        {
        	pIdbSnsrOffsDrvgCalc->lsahbs16AHBDrvOfs_f = pIdbSnsrOffsDrvgCalc->lsahbs32AHBDrvOfs1secArrayMean;
        }
        else
        {
            ;
        }
    }
}

static void LSIDB_vCallSnsrOffsFinalDtmn(void)
{
    LSIDB_vChkSnsrOffsEolEepSt();
    LSIDB_vChkSnsrOffsDrvgEepSt();
    LSIDB_vDetSnsrOffsFinal();
    LSIDB_vWrReqSnsrOffsFinal();
}

/*******************************************************************************
* FUNCTION NAME:        LSIDB_vChkSnsrOffsEolEepSt
* CALLED BY:            LSIDB_vCallSnsrOffsFinalDtmn
* PARAMETER:			void
* RETURN VALUE:         void
* Description:			Check Eol EepRead data Range
* Metrics
********************************************************************************/
static void LSIDB_vChkSnsrOffsEolEepSt(void)
{
    int16_t s16EEPReadOfsABSTemp;

    s16EEPReadOfsABSTemp    = L_s16Abs(pIdbSnsrOffsEolEep->lsahbs16AHBEolEEPofsReadRaw);

    /*---- EOL EEPROM OFFSET ------*/
    if(pIdbSnsrOffsEolEep->lsahbu8AHBEEPofsReadInvalid==0)
    {
        if( pIdbSnsrOffsEolEep->lsahbu1AHBEEPofsUseOK == 0 )
        {
            if( s16EEPReadOfsABSTemp< SnsrOffsEolEepReadValLim  ) /* Pedal : 25mm, Press : 150bar */
            {
                pIdbSnsrOffsEolEep->lsahbs16AHBEEPofsRead = pIdbSnsrOffsEolEep->lsahbs16AHBEolEEPofsReadRaw ;
                pIdbSnsrOffsEolEep->lsahbu1AHBEEPofsUseOK = 1;
            }
            else
            {
                pIdbSnsrOffsEolEep->lsahbs16AHBEEPofsRead = 0;
                pIdbSnsrOffsEolEep->lsahbu1AHBEEPofsUseOK = 0;
            }
        }
        else
        {
            ;
        }
    }
    else
    {
        pIdbSnsrOffsEolEep->lsahbu1AHBEEPofsUseOK = 0;
    }
}

/*******************************************************************************
* FUNCTION NAME:        LSIDB_vChkSnsrOffsDrvgEepSt
* CALLED BY:            LSIDB_vCallSnsrOffsFinalDtmn
* PARAMETER:			void
* RETURN VALUE:         void
* Description:			Check Drv EepRead data Range
* Metrics
********************************************************************************/
static void LSIDB_vChkSnsrOffsDrvgEepSt(void)
{
    int16_t s16DrvEEPReadOfsABSTemp;

    s16DrvEEPReadOfsABSTemp = L_s16Abs(pIdbSnsrOffsDrvgEep->lsahbs16DrvEEPofsReadRaw);


    /*---- Drive EEPROM OFFSET ------*/
    if(pIdbSnsrOffsDrvgEep->lsahbu8DrvEEPofsReadInvalid==0)
    {
        if( pIdbSnsrOffsDrvgEep->lsahbu1AHBDrvEEPofsUseOK == 0 )
        {
            if( s16DrvEEPReadOfsABSTemp< SnsrOffsDrvgEepReadValLim ) /* Pedal : 25mm, Press : 150bar */
            {
                pIdbSnsrOffsDrvgEep->lsahbs16AHBDrvEEPofsRead = pIdbSnsrOffsDrvgEep->lsahbs16DrvEEPofsReadRaw ;
                pIdbSnsrOffsDrvgEep->lsahbu1AHBDrvEEPofsUseOK = 1;
            }
            else
            {
                pIdbSnsrOffsDrvgEep->lsahbs16AHBDrvEEPofsRead = 0 ;
                pIdbSnsrOffsDrvgEep->lsahbu1AHBDrvEEPofsUseOK = 0;
            }
        }
        else
        {
            ;
        }
    }
    else
    {
        pIdbSnsrOffsDrvgEep->lsahbu1AHBDrvEEPofsUseOK = 0;
    }

}
static void LSIDB_vDetSnsrOffsFinal(void)
{
    /*================== IGN ON   condition ================================*/
    if(pIdbSnsrOffsEolCalc->lsahbu1AHBEepOfsCalOkFlg == 1)
    {
        LSIDB_vDetFinalOffsAfterEol();
    }
    else
    {
        if(pIdbSnsrOffsDrvgCalc->lsahbu1AHBDrvOfsOK ==1 )
        {
        	LSIDB_vDetFinalOffsAfterDrv();
        }
        else
        {
        	LSIDB_vDetFinalOffsBeforeCal();
        }

    }

    pIdbSnsrOffsFinal->lsahbs16AHBOfs_f = L_s16LimitMinMax(pIdbSnsrOffsFinal->lsahbs16AHBOfs_f, SnsrOffsLowrLim, SnsrOffsUpprLim);
}

/*******************************************************************************
* FUNCTION NAME:        LSIDB_vWrReqSnsrOffsFinal
* CALLED BY:            LSIDB_vCallSnsrOffsFinalDtmn
* PARAMETER:			void
* RETURN VALUE:         void
* Description:			Send request to write Drv Offset to NVM
* Metrics
********************************************************************************/
static void LSIDB_vWrReqSnsrOffsFinal(void)
{
    int16_t  s16AHBEepOfsFinOfsAbsDiff;

    if(pIdbSnsrOffsEolCalc->lsahbu1AHBEepOfsCalOkFlg == 1) /*DrvEEPData is set as EolCalcData*/
    {
    	if(pIdbSnsrOffsDrvgEep->lsahbu1AHBDrvOfsEepWriteReq  == 1)
    	{
    		if(pIdbSnsrOffsDrvgEep->lsahbu1AHBeepWriteResetReq==1) /* Unreachable condition */
    		{
    			pIdbSnsrOffsDrvgEep->lsahbu1AHBDrvOfsEepWriteReq    = 0;
    			pIdbSnsrOffsDrvgEep->lsahbu1AHBDrveepWriteEnd = 1;
    		}
    		pIdbSnsrOffsDrvgEep->lsIdbs16DrvgOffsEepWr=pIdbSnsrOffsFinal->lsahbs16AHBOfs_f;
    	}
    	else
    	{
    		if(pIdbSnsrOffsDrvgEep->lsahbu1AHBDrveepWriteEnd ==0)
    		{
    			pIdbSnsrOffsDrvgEep->lsahbu1AHBDrvOfsEepWriteReq  = 1;
    		}
    		else
    		{
    			;
    		}
    	}
    }
    else
    {
        if(pIdbSnsrOffsFinal->lsahbu1AHBFinalOfsOK==1)
        {
            s16AHBEepOfsFinOfsAbsDiff = L_s16Abs(pIdbSnsrOffsFinal->lsahbs16AHBOfs_f -  pIdbSnsrOffsDrvgEep->lsahbs16DrvEEPofsReadRaw ) ;

            if(pIdbSnsrOffsDrvgEep->lsahbu1AHBDrvOfsEepWriteReq  == 1)
            {
            	if(pIdbSnsrOffsDrvgEep->lsahbu1AHBeepWriteResetReq==1) /* Unreachable condition */
            	{
            		pIdbSnsrOffsDrvgEep->lsahbu1AHBDrvOfsEepWriteReq    = 0;
            		pIdbSnsrOffsDrvgEep->lsahbu1AHBDrveepWriteEnd = 1;
            	}
				pIdbSnsrOffsDrvgEep->lsIdbs16DrvgOffsEepWr=pIdbSnsrOffsFinal->lsahbs16AHBOfs_f;
            }
            else
            {
                if( (s16AHBEepOfsFinOfsAbsDiff > SnsrOffsDrvEepUpdThd) && ( s16AHBEepOfsFinOfsAbsDiff < SnsrOffsDrvEepUpdLim) /* Pedal : 1mm/25mm, Press : 0.5bar/15bar*/
                    && (pIdbSnsrOffsDrvgEep->lsahbu1AHBDrveepWriteEnd ==0) )
                {
                    pIdbSnsrOffsDrvgEep->lsahbu1AHBDrvOfsEepWriteReq  = 1;
                }
                else
                {
                	;
                }
            }
        }
        else
        {
            pIdbSnsrOffsDrvgEep->lsahbu1AHBDrvOfsEepWriteReq = 0 ;
        }
    }
}

static uint8_t LSIDB_u8ChkNVMOffsReadInvld(uint16_t u16NVMReadOffsVal1, uint16_t u16NVMReadOffsData2)
{
	uint8_t u8OffsetInvalidFlg = 0;

	if(NvM_EepDataReadInvldFlg==1)
	{
		u8OffsetInvalidFlg          = 1;
	}
	else if((u16NVMReadOffsVal1+u16NVMReadOffsData2)!=0xffff)
	{
		u8OffsetInvalidFlg          = 1;
	}
	else
	{
		u8OffsetInvalidFlg          = 0;
	}

	return u8OffsetInvalidFlg;
}

static void LSIDB_vWrSnsrOffsToNVM(IdbSnsrOffsEolEep_t *p_IdbSnsrOffsEolEep, IdbSnsrOffsDrvgEep_t *p_IdbSnsrOffsDrvgEep)
{
	/* Write EOL Offset */
	if(p_IdbSnsrOffsEolEep->lsahbu1AHBEepOfsWriteReq == 1)
	{
		*p_s32NVMEolOffsReadVal 	= (int32_t)(p_IdbSnsrOffsEolEep->lsIdbs16EolOffsEepWr);
		*p_s32NVMEolOffsReadVal2 	= (int32_t)(((uint16_t)(p_IdbSnsrOffsEolEep->lsIdbs16EolOffsEepWr))^0xffff);
		p_IdbSnsrOffsEolEep->lsahbu1AHBEepOfsWriteOkFlg = 1;
	}
	else
	{
		p_IdbSnsrOffsEolEep->lsahbu1AHBEepOfsWriteOkFlg = 0;
	}

	/* Write Drv Offset */
	if(p_IdbSnsrOffsDrvgEep->lsahbu1AHBDrvOfsEepWriteReq == 1)
	{
		*p_s32NVMDrvOffsReadVal 	= (int32_t)(p_IdbSnsrOffsDrvgEep->lsIdbs16DrvgOffsEepWr);
		*p_s32NVMDrvOffsReadVal2 	= (int32_t)(((uint16_t)(p_IdbSnsrOffsDrvgEep->lsIdbs16DrvgOffsEepWr))^0xffff);
	}
	else
	{
		;
	}

}

/*******************************************************************************
* FUNCTION NAME:        LSIDB_vChkEolOffsCalcCdn
* CALLED BY:            LSIDB_vDetSnsrOffsEol
* PARAMETER:			void
* RETURN VALUE:         void
* Description:			Determine Start Eol Offset Calculating
* Metrics
********************************************************************************/
static void LSIDB_vChkEolOffsCalcCdn(void)
{
	if ((pIdbSnsrOffsEolCalc->lsahbu1EEPofsCalcFWStartFlgOLD == 0)
			&& (pIdbSnsrOffsEolCalc->lsahbu1EEPofsCalcFWStartFlg == 1))
	{
		pIdbSnsrOffsEolCalc->lsahbu1EEPofsCalcFWStartFlgIni = 1; /*Rising Edge Trigger from ToolBrake*/
	}
	else
	{
		pIdbSnsrOffsEolCalc->lsahbu1EEPofsCalcFWStartFlgIni = 0;
	}

	/*------------- Check Brake Signal Condition ------------- */
	if (((GetSnsrSigOffs.lsahbu1BLS == 0) && (GetSnsrSigOffs.fu1BLSErrDet == 0)) || (GetSnsrSigOffs.fu1BLSErrDet == 1))
	{
		pIdbSnsrOffsEolCalc->lsahbu1AHBeepEnterOK = 1;
	}
	else
	{
		pIdbSnsrOffsEolCalc->lsahbu1AHBeepEnterOK = 0;
	}

	/*------------- Determine EOL Offset Calculation Start  ------------- */
	if(pIdbSnsrOffsEolCalc->lsahbu1AHBeepCalcLoopOK ==1 )
	{
		if(pIdbSnsrOffsEolCalc->lsahbu1AHBEepOfsCalEndFlg==1) /*Calculation End*/
		{
			pIdbSnsrOffsEolCalc->lsahbu1AHBeepCalcLoopOK = 0;
		}
		else
		{
			;
		}
	}
	else
	{
		if((pIdbSnsrOffsEolCalc->lsahbu1EEPofsCalcFWStartFlg ==1)
				&& (pIdbSnsrOffsEolCalc->lsahbu1AHBeepEnterOK ==1))
		{
			pIdbSnsrOffsEolCalc->lsahbu1AHBeepCalcLoopOK   = 1 ;
		}
		else
		{
			;
		}
	}
}

/*******************************************************************************
* FUNCTION NAME:        LSIDB_vDecideEolOffsEndCdn
* CALLED BY:            LSIDB_vCalcSnsrOffsEol
* PARAMETER:			void
* RETURN VALUE:         void
* Description:			Decide whether Eol Offset Calculation is End and successful
* Metrics
********************************************************************************/
static void LSIDB_vDecideEolOffsEndCdn(void)
{
	if( (pIdbSnsrOffsEolCalc->lsahbu1EEPofsEEPLimitOK==1) && (pIdbSnsrOffsEolCalc->lsahbu1EEPofsEEPMaxMinOK==1) )
	{
		pIdbSnsrOffsEolCalc->lsahbu1AHBEepOfsCalEndFlg  = 1;
		pIdbSnsrOffsEolCalc->lsahbu1AHBEepOfsCalOkFlg   = 1;
	}
	else if( (pIdbSnsrOffsEolCalc->lsahbu1EEPofsEEPLimitOK==1) && (pIdbSnsrOffsEolCalc->lsahbu1EEPofsEEPMaxMinOK==0) )
	{
		pIdbSnsrOffsEolCalc->lsahbu1AHBEepOfsCalEndFlg  = 0;
		pIdbSnsrOffsEolCalc->lsahbu1AHBEepOfsCalOkFlg   = 0;
	}
	else if( (pIdbSnsrOffsEolCalc->lsahbu1EEPofsEEPLimitOK==0) && (pIdbSnsrOffsEolCalc->lsahbu1EEPofsEEPMaxMinOK==1) )
	{
		pIdbSnsrOffsEolCalc->lsahbu1AHBEepOfsCalEndFlg  = 1;
		pIdbSnsrOffsEolCalc->lsahbu1AHBEepOfsCalOkFlg   = 0;
	}
	else
	{
		pIdbSnsrOffsEolCalc->lsahbu1AHBEepOfsCalEndFlg  = 1;
		pIdbSnsrOffsEolCalc->lsahbu1AHBEepOfsCalOkFlg   = 0;
	}

	if(pIdbSnsrOffsEolCalc->lsahbu8AHBEEPofsMaxMinLMOverCnt>= (EEP_RETRY+1) )
	{
		pIdbSnsrOffsEolCalc->lsahbu1AHBEepOfsCalEndFlg  = 1;
	}
	else
	{
		;
	}
}

/*******************************************************************************
* FUNCTION NAME:        LSIDB_vCalc1secDrvOffs
* CALLED BY:            LSIDB_vCalcSnsrOffsDrvgQuick
* PARAMETER:			void
* RETURN VALUE:         void
* Description:			Store 1sec Signal Sum for 10s and Add 10sec TrimSum
* Metrics
********************************************************************************/
static void LSIDB_vCalc1secDrvOffs(void)
{
	pIdbSnsrOffsDrvgCalc->lsahbu8AHBDrvOfsCnt   = 0;
	pIdbSnsrOffsDrvgCalc->lsahbs32AHBDrvOfs1secSumArray[pIdbSnsrOffsDrvgCalc->lsahbu8AHBDrvOfsArrayCnt] =  pIdbSnsrOffsDrvgCalc->lsahbs32AHBDrvOfs1secSum;
	pIdbSnsrOffsDrvgCalc->lsahbs32AHBDrvOfs1secSum = 0;

	if(pIdbSnsrOffsDrvgCalc->lsahbu8AHBDrvOfsArrayCnt==0)
	{
		pIdbSnsrOffsDrvgCalc->lsahbs32AHBDrvOfs1secArrayMax = pIdbSnsrOffsDrvgCalc->lsahbs32AHBDrvOfs1secSumArray[0];
		pIdbSnsrOffsDrvgCalc->lsahbs32AHBDrvOfs1secArrayMin = pIdbSnsrOffsDrvgCalc->lsahbs32AHBDrvOfs1secSumArray[0];
	}
	else
	{
		if(pIdbSnsrOffsDrvgCalc->lsahbs32AHBDrvOfs1secSumArray[pIdbSnsrOffsDrvgCalc->lsahbu8AHBDrvOfsArrayCnt] > pIdbSnsrOffsDrvgCalc->lsahbs32AHBDrvOfs1secArrayMax)
		{
			pIdbSnsrOffsDrvgCalc->lsahbs32AHBDrvOfs1secArrayMax = pIdbSnsrOffsDrvgCalc->lsahbs32AHBDrvOfs1secSumArray[pIdbSnsrOffsDrvgCalc->lsahbu8AHBDrvOfsArrayCnt];
		}
		else if(pIdbSnsrOffsDrvgCalc->lsahbs32AHBDrvOfs1secSumArray[pIdbSnsrOffsDrvgCalc->lsahbu8AHBDrvOfsArrayCnt] < pIdbSnsrOffsDrvgCalc->lsahbs32AHBDrvOfs1secArrayMin)
		{
			pIdbSnsrOffsDrvgCalc->lsahbs32AHBDrvOfs1secArrayMin = pIdbSnsrOffsDrvgCalc->lsahbs32AHBDrvOfs1secSumArray[pIdbSnsrOffsDrvgCalc->lsahbu8AHBDrvOfsArrayCnt];
		}
		else
		{
			;
		}
	}

	pIdbSnsrOffsDrvgCalc->lsahbs32AHBDrvOfs1secArraySum += pIdbSnsrOffsDrvgCalc->lsahbs32AHBDrvOfs1secSumArray[pIdbSnsrOffsDrvgCalc->lsahbu8AHBDrvOfsArrayCnt];

	if(pIdbSnsrOffsDrvgCalc->lsahbu8AHBDrvOfsArrayCnt >= 2)
	{
		pIdbSnsrOffsDrvgCalc->lsahbu1AHBDrv1secOfsOK = 1 ;
		pIdbSnsrOffsDrvgCalc->lsahbs32AHBDrvOfs1secArrayTrimSum = (pIdbSnsrOffsDrvgCalc->lsahbs32AHBDrvOfs1secArraySum - pIdbSnsrOffsDrvgCalc->lsahbs32AHBDrvOfs1secArrayMax - pIdbSnsrOffsDrvgCalc->lsahbs32AHBDrvOfs1secArrayMin);
		pIdbSnsrOffsDrvgCalc->lsahbs32AHBDrvOfs1secArrayMean = pIdbSnsrOffsDrvgCalc->lsahbs32AHBDrvOfs1secArrayTrimSum / (pIdbSnsrOffsDrvgCalc->lsahbu8AHBDrvOfsArrayCnt-1) / U16_T_1_S;
	}
	else
	{
		;
	}

	pIdbSnsrOffsDrvgCalc->lsahbu8AHBDrvOfsArrayCnt = pIdbSnsrOffsDrvgCalc->lsahbu8AHBDrvOfsArrayCnt + 1;

}

/*******************************************************************************
* FUNCTION NAME:        LSIDB_vCalc60secDrvOffs
* CALLED BY:            LSIDB_vCalcSnsrOffsDrvgQuick
* PARAMETER:			void
* RETURN VALUE:         void
* Description:			Calculate final drv offset
* Metrics
********************************************************************************/
static void LSIDB_vCalc60secDrvOffs(void)
{
	if(pIdbSnsrOffsDrvgCalc->lsahbu8AHBDrvOfsArrayCnt > 9)
	{
		pIdbSnsrOffsDrvgCalc->lsahbu8AHBDrvOfsArrayCnt  = 0;
		pIdbSnsrOffsDrvgCalc->lsahbu1AHBDrv10secOfsOK   = 1;
		pIdbSnsrOffsDrvgCalc->lsahbs32AHBDrvOfs1secArraySum = 0;
		pIdbSnsrOffsDrvgCalc->lsahbs32AHBDrvofs10secSum   += pIdbSnsrOffsDrvgCalc->lsahbs32AHBDrvOfs1secArrayTrimSum ;

		pIdbSnsrOffsDrvgCalc->lsahbu8AHBDrvOfs10sArrayCnt = pIdbSnsrOffsDrvgCalc->lsahbu8AHBDrvOfs10sArrayCnt + 1 ;
		if(pIdbSnsrOffsDrvgCalc->lsahbu8AHBDrvOfs10sArrayCnt >= 6 )   /*60sec offset */
		{
			pIdbSnsrOffsDrvgCalc->lsahbu8AHBDrvOfs10sArrayCnt  = 0 ;
			pIdbSnsrOffsDrvgCalc->lsahbs16AHBDrvOfs60sSumOld  = pIdbSnsrOffsDrvgCalc->lsahbs16AHBDrvOfs60sSum;
			pIdbSnsrOffsDrvgCalc->lsahbs16AHBDrvOfs60sSum = (int16_t)((pIdbSnsrOffsDrvgCalc->lsahbs32AHBDrvofs10secSum)/(((6*8)*U16_T_1_S)/10));
			pIdbSnsrOffsDrvgCalc->lsahbs32AHBDrvofs10secSum = 0 ;

			if(pIdbSnsrOffsDrvgCalc->lsahbu1AHBDrv60secOfsOK ==0 )
			{
				pIdbSnsrOffsDrvgCalc->lsahbs16AHBDrvOfsArrayFilter = pIdbSnsrOffsDrvgCalc->lsahbs16AHBDrvOfs60sSum/10 ;
				pIdbSnsrOffsDrvgCalc->lsahbs16AHBDrvOfsArrayFConv  = pIdbSnsrOffsDrvgCalc->lsahbs16AHBDrvOfs60sSum ;
			}
			else
			{
				pIdbSnsrOffsDrvgCalc->lsahbu1AHBDrv60secOfsReOK =1 ;
				pIdbSnsrOffsDrvgCalc->lsahbs16AHBDrvOfsArrayFConv  = L_s16Lpf1Int( (pIdbSnsrOffsDrvgCalc->lsahbs16AHBDrvOfs60sSum) , pIdbSnsrOffsDrvgCalc->lsahbs16AHBDrvOfsArrayFConv, 26  );
				pIdbSnsrOffsDrvgCalc->lsahbs16AHBDrvOfsArrayFilter = pIdbSnsrOffsDrvgCalc->lsahbs16AHBDrvOfsArrayFConv/10 ;
			}

			pIdbSnsrOffsDrvgCalc->lsahbu1AHBDrv60secOfsOK =1;
		}
		else
		{
			;
		}

	}
	else
	{
		;
	}
}

static void LSIDB_vDetFinalOffsAfterDrv(void)
{
	int16_t  s16AHBEepDrvAbsDiff;
	int16_t  s16AHBDrvDrvOldAbsDiff;

	pIdbSnsrOffsFinal->lsahbu1AHBFinalOfsOK=1;

	if(pIdbSnsrOffsDrvgEep->lsahbu1AHBDrvEEPofsUseOK == 1)
	{
		if(pIdbSnsrOffsFinal->lsahbu1AHBDrvEepOfsDrvUse ==1 )
		{
			pIdbSnsrOffsFinal->lsahbs16AHBOfs_f = pIdbSnsrOffsDrvgCalc->lsahbs16AHBDrvOfs_f ;
			pIdbSnsrOffsFinal->lsahbu1AHBFinalMonitor = 3;
		}
		else
		{
			s16AHBEepDrvAbsDiff = L_s16Abs( pIdbSnsrOffsDrvgEep->lsahbs16AHBDrvEEPofsRead  - pIdbSnsrOffsDrvgCalc->lsahbs16AHBDrvOfs_f );
			if(s16AHBEepDrvAbsDiff < SnsrOffsDrvEepUseThd)
			{
				pIdbSnsrOffsFinal->lsahbs16AHBOfs_f = pIdbSnsrOffsDrvgCalc->lsahbs16AHBDrvOfs_f ;
				pIdbSnsrOffsFinal->lsahbu1AHBDrvEepOfsDrvUse =1 ;
				pIdbSnsrOffsFinal->lsahbu1AHBFinalMonitor = 4;

			}
			else
			{
				s16AHBDrvDrvOldAbsDiff = L_s16Abs((pIdbSnsrOffsDrvgCalc->lsahbs16AHBDrvOfs60sSum/10) - (pIdbSnsrOffsDrvgCalc->lsahbs16AHBDrvOfs60sSumOld/10));

				if( (pIdbSnsrOffsDrvgCalc->lsahbu1AHBDrv60secOfsReOK==1) && (s16AHBDrvDrvOldAbsDiff< SnsrOffsDrvDrvUseThd) )
				{
					pIdbSnsrOffsFinal->lsahbs16AHBOfs_f = pIdbSnsrOffsDrvgCalc->lsahbs16AHBDrvOfs_f ;
					pIdbSnsrOffsFinal->lsahbu1AHBDrvEepOfsDrvUse =1 ;
					pIdbSnsrOffsFinal->lsahbu1AHBFinalMonitor = 5;
				}
				else
				{
					pIdbSnsrOffsFinal->lsahbs16AHBOfs_f = pIdbSnsrOffsDrvgEep->lsahbs16AHBDrvEEPofsRead ;
					pIdbSnsrOffsFinal->lsahbu1AHBFinalMonitor = 6;
				}
			}
		}
	}
	else
	{
		pIdbSnsrOffsFinal->lsahbs16AHBOfs_f = pIdbSnsrOffsDrvgCalc->lsahbs16AHBDrvOfs_f ; /* Think more */
		pIdbSnsrOffsFinal->lsahbu1AHBFinalMonitor = 7;
	}
}

static void LSIDB_vDetFinalOffsAfterEol(void)
{
	pIdbSnsrOffsFinal->lsahbu1AHBFinalOfsOK = 1;

	if(pIdbSnsrOffsDrvgCalc->lsahbu1AHBDrvOfsOK ==1)
	{
		pIdbSnsrOffsFinal->lsahbs16AHBOfs_f     = pIdbSnsrOffsDrvgCalc->lsahbs16AHBDrvOfs_f ;
		pIdbSnsrOffsFinal->lsahbu1AHBFinalMonitor = 1;
	}
	else
	{
		pIdbSnsrOffsFinal->lsahbs16AHBOfs_f     = pIdbSnsrOffsEolCalc->lsahbs16AHBofsFirstCal ;
		pIdbSnsrOffsFinal->lsahbu1AHBFinalMonitor = 2;
	}
}

static void LSIDB_vDetFinalOffsBeforeCal(void)
{
	if(pIdbSnsrOffsDrvgEep->lsahbu1AHBDrvEEPofsUseOK == 1)
	{
		pIdbSnsrOffsFinal->lsahbu1AHBFinalOfsOK = 1;

		pIdbSnsrOffsFinal->lsahbs16AHBOfs_f = pIdbSnsrOffsDrvgEep->lsahbs16AHBDrvEEPofsRead ;
		pIdbSnsrOffsFinal->lsahbu1AHBFinalMonitor = 8;
	}
	else
	{
		if(pIdbSnsrOffsEolEep->lsahbu1AHBEEPofsUseOK == 1)
		{
			pIdbSnsrOffsFinal->lsahbu1AHBFinalOfsOK=1;
			pIdbSnsrOffsFinal->lsahbs16AHBOfs_f = pIdbSnsrOffsEolEep->lsahbs16AHBEEPofsRead ;
			pIdbSnsrOffsFinal->lsahbu1AHBFinalMonitor = 9;
		}
		else
		{
			pIdbSnsrOffsFinal->lsahbu1AHBFinalOfsOK=0;
			pIdbSnsrOffsFinal->lsahbs16AHBOfs_f = 0;
			pIdbSnsrOffsFinal->lsahbu1AHBFinalMonitor = 10;
		}
	}
}

static void LSIDB_vDetPdlOffsDrvgCalcCdn(void)
{
	if(	(Idbu8ActvBrkCtrlAct == 0) && (Idbu8SnsrRawSigChckOk == 1) && (pIdbSnsrOffsDrvgCalc->lsahbu1SensorNormCond == 1))
	{
		if(Idbu8BrkSigChckOk == 1)
		{
			if(((Idbu8ChckStsAftCtrl == 1) || (Idbu8ChckStsInCtrl == 1)) && (GetSnsrSigOffs.s16VehicleSpeed > S16_SPEED_10_KPH))
			{
				pIdbSnsrOffsDrvgCalc->lsahbu1SensorDrvOfsCheckCond = 1;
			}
			else
			{
				pIdbSnsrOffsDrvgCalc->lsahbu1SensorDrvOfsCheckCond = 0;
			}
		}
		else
		{
			pIdbSnsrOffsDrvgCalc->lsahbu1SensorDrvOfsCheckCond = 0;
		}
	}
	else
	{
		pIdbSnsrOffsDrvgCalc->lsahbu1SensorDrvOfsCheckCond = 0;
	}
}

static void LSIDB_vDetPSPOffsDrvgCalcCdn(void)
{
	uint8_t u8ChckValveSts = 0;

	u8ChckValveSts = (GetSnsrSigOffs.Idbu8CVVAct == 0);

	if((Idbu8ActvBrkCtrlAct == 0) && (Idbu8SnsrRawSigChckOk == 1) && (pIdbSnsrOffsDrvgCalc->lsahbu1SensorNormCond == 1))
	{
		if(Idbu8BrkSigChckOk == 1)
		{
			if((Idbu8ChckStsAftCtrl == 1) && (GetSnsrSigOffs.lsahbs16PedalTravelFilt < S16_TRAVEL_20_MM ) && (u8ChckValveSts == 1))
			{
				pIdbSnsrOffsDrvgCalc->lsahbu1SensorDrvOfsCheckCond = 1;
			}
			else
			{
				pIdbSnsrOffsDrvgCalc->lsahbu1SensorDrvOfsCheckCond = 0;
			}
		}
		else
		{
			pIdbSnsrOffsDrvgCalc->lsahbu1SensorDrvOfsCheckCond = 0;
		}
	}
	else
	{
		pIdbSnsrOffsDrvgCalc->lsahbu1SensorDrvOfsCheckCond = 0;
	}
}

static void LSIDB_vDetCircPOffsDrvgCalcCdn(void)
{
	uint8_t u8ChckValveSts = 0;

	u8ChckValveSts = ((GetSnsrSigOffs.Idbu8CVVAct == 0) && (GetSnsrSigOffs.Idbu8CVV2Act == 0) && (GetSnsrSigOffs.Idbu8PressDumpVlvAct == 0));

	if((Idbu8ActvBrkCtrlAct == 0) && (Idbu8SnsrRawSigChckOk == 1) && (pIdbSnsrOffsDrvgCalc->lsahbu1SensorNormCond == 1))
	{
		if((Idbu8ChckStsAftCtrl == 1) && (u8ChckValveSts == 1))
		{
			pIdbSnsrOffsDrvgCalc->lsahbu1SensorDrvOfsCheckCond = 1;
		}
		else
		{
			pIdbSnsrOffsDrvgCalc->lsahbu1SensorDrvOfsCheckCond = 0;
		}
	}
	else
	{
		pIdbSnsrOffsDrvgCalc->lsahbu1SensorDrvOfsCheckCond = 0;
	}
}

static void LSIDB_vDetEolCalEndCdn(void)
{
	/* Pedal EOL Offset */
	if(Idbu8PedalEolOfsCalcEnd==0)
	{
		if((PdtOffsEolCalc.lsahbu1AHBEepOfsCalEndFlg == 1) && (PdfOffsEolCalc.lsahbu1AHBEepOfsCalEndFlg == 1))
		{
			Idbu8PedalEolOfsCalcEnd = 1;
			/*---- Must consider more ---*/
			if((PdtOffsEolCalc.lsahbu1AHBEepOfsCalOkFlg == 1))
			{
				Idbu8PedalEolOfsCalcOk = 1;
			}
			else
			{
				Idbu8PedalEolOfsCalcOk = 0;
			}
		}
	}
	else
	{
		if((PdtOffsEolCalc.lsahbu1AHBEepOfsCalEndFlg == 0) || (PdfOffsEolCalc.lsahbu1AHBEepOfsCalEndFlg == 0))
		{
			Idbu8PedalEolOfsCalcEnd = 0;
			Idbu8PedalEolOfsCalcOk = 0;
		}
	}

	/* Press EOL Offset */
	if(Idbu8PressEolOfsCalcEnd == 0)
	{
#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
		if((PedlSimrPOffsEolCalc.lsahbu1AHBEepOfsCalEndFlg == 1) && (PistPOffsEolCalc.lsahbu1AHBEepOfsCalEndFlg == 1) )
		{
			Idbu8PressEolOfsCalcEnd = 1;
			if( (PedlSimrPOffsEolCalc.lsahbu1AHBEepOfsCalOkFlg == 1) && (PistPOffsEolCalc.lsahbu1AHBEepOfsCalOkFlg == 1))
#else /*__P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE*/
		if((PedlSimrPOffsEolCalc.lsahbu1AHBEepOfsCalEndFlg == 1) && (PrimPOffsEolCalc.lsahbu1AHBEepOfsCalEndFlg == 1) && (SecdPOffsEolCalc.lsahbu1AHBEepOfsCalEndFlg == 1))
		{
			Idbu8PressEolOfsCalcEnd = 1;
			if( (PedlSimrPOffsEolCalc.lsahbu1AHBEepOfsCalOkFlg == 1) && (PrimPOffsEolCalc.lsahbu1AHBEepOfsCalOkFlg ==1) && (SecdPOffsEolCalc.lsahbu1AHBEepOfsCalOkFlg==1) )
#endif
			{
				Idbu8PressEolOfsCalcOk = 1;
			}
			else
			{
				Idbu8PressEolOfsCalcOk = 0;
			}
		}
	}
	else
	{
#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
		if((PedlSimrPOffsEolCalc.lsahbu1AHBEepOfsCalEndFlg == 0) || (PistPOffsEolCalc.lsahbu1AHBEepOfsCalEndFlg == 0))
#else /*__P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE*/
		if((PedlSimrPOffsEolCalc.lsahbu1AHBEepOfsCalEndFlg == 0) || (PrimPOffsEolCalc.lsahbu1AHBEepOfsCalEndFlg == 0) || (SecdPOffsEolCalc.lsahbu1AHBEepOfsCalEndFlg == 0))
#endif
		{
			Idbu8PressEolOfsCalcEnd = 0;
			Idbu8PressEolOfsCalcOk = 0;
		}
	}
}

/*******************************************************************************
* FUNCTION NAME:        LSIDB_u8ChckSnsrRawSig
* CALLED BY:            LSIDB_vDetSnsrOffsDrvgCalcCmnCdn
* PARAMETER:			void
* RETURN VALUE:         void
* Description:			Check Sensor Raw Signal during Drv Offset Calculation
* Metrics
********************************************************************************/
static uint8_t LSIDB_u8ChckSnsrRawSig(void)
{
	uint8_t u8SnsrRawSigOk = 0;

	if(pIdbSnsrOffsDrvgCalc->lsahbu1SensorFaultDet == 0)
	{
		if(pIdbSnsrOffsDrvgCalc->lsahbu1SensorSusDet == 0)
		{
			if(pIdbSnsrOffsDrvgCalc->SnsrRawSig < pIdbSnsrOffsDrvgCalc->s16SnsrRawSigLim) /* Pedal : 25mm, Press : 15Bar */
			{
				u8SnsrRawSigOk = 1;
			}
			else
			{
				u8SnsrRawSigOk = 0;
			}
		}
		else
		{
			u8SnsrRawSigOk = 0;
		}
	}
	else
	{
		u8SnsrRawSigOk = 0;
	}

	return u8SnsrRawSigOk;
}

/*******************************************************************************
* FUNCTION NAME:        LSIDB_vDetSnsrOffsDrvgCalcCmnCdn
* CALLED BY:            LSIDB_vDetSnsrOffsDrvgCalcCdn
* PARAMETER:			void
* RETURN VALUE:         void
* Description:			Check Common Conditions for Drive Offset Calculation
* Metrics
********************************************************************************/
static void LSIDB_vDetSnsrOffsDrvgCalcCmnCdn(void)
{
	Idbu8ActvBrkCtrlAct = ((GetSnsrSigOffs.ldahbu1AbsActiveFlg == 1) || (GetSnsrSigOffs.ldahbu1EscActiveFlg == 1) || (GetSnsrSigOffs.liu1EscActiveBrk == 1));
	Idbu8ChckStsAftCtrl = ((pIdbSnsrOffsDrvgCalc->lsahbu1AHBact1secAfterOK == 1) && (GetSnsrSigOffs.lis16cEMSAccPedDep > S16_ACCEL_10_PERCENT) && (GetSnsrSigOffs.cu8EMS2AccPedDep_Pc_Err_Flg == 0) && (GetSnsrSigOffs.liu1cEMSAccPedDep_Pc_Sus == 0));
	Idbu8ChckStsInCtrl = ((GetSnsrSigOffs.lcahbu1AHBOn == 1) && (GetSnsrSigOffs.lis16cEMSAccPedDep > S16_ACCEL_20_PERCENT) && (GetSnsrSigOffs.cu8EMS2AccPedDep_Pc_Err_Flg == 0) && (GetSnsrSigOffs.liu1cEMSAccPedDep_Pc_Sus == 0));

	if((GetSnsrSigOffs.lsahbu1BLS == 0) && (GetSnsrSigOffs.fu1BlsSusDet == 0) && (GetSnsrSigOffs.fu1BLSErrDet == 0))
	{
		Idbu8BrkSigChckOk = 1;
	}
#if __BRAKE_SWITCH_ENABLE == ENABLE
	else if((GetSnsrSigOffs.fu1BlsSusDet == 1) || (GetSnsrSigOffs.fu1BLSErrDet == 1) && (GetSnsrSigOffs.fu1BSSignal == 1) && (GetSnsrSigOffs.fu1BsSusDet == 0))
	{
		Idbu8BrkSigChckOk = 1;
	}
#endif
	else
	{
		Idbu8BrkSigChckOk = 0;
	}

	Idbu8SnsrRawSigChckOk = LSIDB_u8ChckSnsrRawSig();
}

static void vIDB_u8ReadBbsSnsrNVMData(void)
{
	/* Read PDT Offs */
	GetSnsrSigOffs.ws16PdtSnsrOfset                = (int16_t)NVM_PdtOffsEolReadVal;
	GetSnsrSigOffs.wu1PdtSnsrOfSetInvalid = LSIDB_u8ChkNVMOffsReadInvld((uint16_t)NVM_PdtOffsEolReadVal,(uint16_t)NVM_PdtOffsEolReadVal2);

	GetSnsrSigOffs.ws16DrvPdtSnsrOfset             = (int16_t)NVM_PdtOffsDrvgReadVal;
	GetSnsrSigOffs.wu1DrvPdtSnsrOfSetInvalid = LSIDB_u8ChkNVMOffsReadInvld((uint16_t)NVM_PdtOffsDrvgReadVal,(uint16_t)NVM_PdtOffsDrvgReadVal2);

	/* Read PDF Offs */
	GetSnsrSigOffs.ws16PdfSnsrOfset                = (int16_t)NVM_PdfOffsEolReadVal;
	GetSnsrSigOffs.wu1PdfSnsrOfSetInvalid = LSIDB_u8ChkNVMOffsReadInvld((uint16_t)NVM_PdfOffsEolReadVal,(uint16_t)NVM_PdfOffsEolReadVal2);

	GetSnsrSigOffs.ws16DrvPdfSnsrOfset             = (int16_t)NVM_PdfOffsDrvgReadVal;
	GetSnsrSigOffs.wu1DrvPdfSnsrOfSetInvalid = LSIDB_u8ChkNVMOffsReadInvld((uint16_t)NVM_PdfOffsDrvgReadVal,(uint16_t)NVM_PdfOffsDrvgReadVal2);

	/* Read PSP Offs */
	GetSnsrSigOffs.ws16PSPressureOfset             = (int16_t)NVM_PedlSimrPOffsEolReadVal;
	GetSnsrSigOffs.wu1PSPressureOfsetInvalid = LSIDB_u8ChkNVMOffsReadInvld((uint16_t)NVM_PedlSimrPOffsEolReadVal,(uint16_t)NVM_PedlSimrPOffsEolReadVal2);

	GetSnsrSigOffs.ws16DrvPSPressureOfset          = (int16_t)NVM_PedlSimrPOffsDrvgReadVal;
	GetSnsrSigOffs.wu1DrvPSPressureOfsetInvalid = LSIDB_u8ChkNVMOffsReadInvld((uint16_t)NVM_PedlSimrPOffsDrvgReadVal,(uint16_t)NVM_PedlSimrPOffsDrvgReadVal2);

#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
	/* Read PistP Offs */
	GetSnsrSigOffs.ws16BCPressureOfset              = (int16_t)NVM_PistPOffsEolReadVal;
	GetSnsrSigOffs.wu1BCPressureOfsetInvalid = LSIDB_u8ChkNVMOffsReadInvld((uint16_t)NVM_PistPOffsEolReadVal,(uint16_t)NVM_PistPOffsEolReadVal2);

	GetSnsrSigOffs.ws16DrvBCPressureOfset           = (int16_t)NVM_PistPOffsDrvgReadVal;
	GetSnsrSigOffs.wu1DrvBCPressureOfsetInvalid = LSIDB_u8ChkNVMOffsReadInvld((uint16_t)NVM_PistPOffsDrvgReadVal,(uint16_t)NVM_PistPOffsDrvgReadVal2);
#elif __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
	/* Read PrimCircP Offs */
	GetSnsrSigOffs.ws16BCPressureOfset             = (int16_t)NVM_PrimCircPOffsEolReadVal;
	GetSnsrSigOffs.wu1BCPressureOfsetInvalid = LSIDB_u8ChkNVMOffsReadInvld((uint16_t)NVM_PrimCircPOffsEolReadVal,(uint16_t)NVM_PrimCircPOffsEolReadVal2);

	GetSnsrSigOffs.ws16DrvBCPressureOfset          = (int16_t)NVM_PrimCircPOffsDrvgReadVal;
	GetSnsrSigOffs.wu1DrvBCPressureOfsetInvalid = LSIDB_u8ChkNVMOffsReadInvld((uint16_t)NVM_PrimCircPOffsDrvgReadVal,(uint16_t)NVM_PrimCircPOffsDrvgReadVal2);

	/* Read SecdCircP Offs */
	GetSnsrSigOffs.ws16BC2PressureOfset            = (int16_t)NVM_SecdCircPOffsEolReadVal;
	GetSnsrSigOffs.wu1BC2PressureOfsetInvalid = LSIDB_u8ChkNVMOffsReadInvld((uint16_t)NVM_SecdCircPOffsEolReadVal,(uint16_t)NVM_SecdCircPOffsEolReadVal2);

	GetSnsrSigOffs.ws16DrvBC2PressureOfset         = (int16_t)NVM_SecdCircPOffsDrvgReadVal;
	GetSnsrSigOffs.wu1DrvBC2PressureOfsetInvalid = LSIDB_u8ChkNVMOffsReadInvld((uint16_t)NVM_SecdCircPOffsDrvgReadVal,(uint16_t)NVM_SecdCircPOffsDrvgReadVal2);
#endif
}

#define SPC_5MSCTRL_STOP_SEC_CODE
#include "Spc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
