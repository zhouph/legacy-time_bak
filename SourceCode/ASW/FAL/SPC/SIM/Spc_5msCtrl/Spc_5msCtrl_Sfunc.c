#define S_FUNCTION_NAME      Spc_5msCtrl_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          61
#define WidthOutputPort         32

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Spc_5msCtrl.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Spc_5msCtrlNormVlvReqVlvActInfo.PrimCutVlvReq = input[0];
    Spc_5msCtrlNormVlvReqVlvActInfo.SecdCutVlvReq = input[1];
    Spc_5msCtrlNormVlvReqVlvActInfo.PrimCircVlvReq = input[2];
    Spc_5msCtrlNormVlvReqVlvActInfo.SecdCircVlvReq = input[3];
    Spc_5msCtrlNormVlvReqVlvActInfo.SimVlvReq = input[4];
    Spc_5msCtrlEemFailData.Eem_Fail_CirP1 = input[5];
    Spc_5msCtrlEemFailData.Eem_Fail_CirP2 = input[6];
    Spc_5msCtrlEemFailData.Eem_Fail_SimP = input[7];
    Spc_5msCtrlEemFailData.Eem_Fail_BLS = input[8];
    Spc_5msCtrlEemFailData.Eem_Fail_PedalPDT = input[9];
    Spc_5msCtrlEemFailData.Eem_Fail_PedalPDF = input[10];
    Spc_5msCtrlCanRxAccelPedlInfo.AccelPedlVal = input[11];
    Spc_5msCtrlCanRxAccelPedlInfo.AccelPedlValErr = input[12];
    Spc_5msCtrlPdt5msRawInfo.PdtSig = input[13];
    Spc_5msCtrlPdf5msRawInfo.PdfSig = input[14];
    Spc_5msCtrlSwtStsFlexBrkInfo.FlexBrkASwt = input[15];
    Spc_5msCtrlSwtStsFlexBrkInfo.FlexBrkBSwt = input[16];
    Spc_5msCtrlWhlSpdInfo.FlWhlSpd = input[17];
    Spc_5msCtrlWhlSpdInfo.FrWhlSpd = input[18];
    Spc_5msCtrlWhlSpdInfo.RlWhlSpd = input[19];
    Spc_5msCtrlWhlSpdInfo.RrlWhlSpd = input[20];
    Spc_5msCtrlCircP5msRawInfo.PrimCircPSig = input[21];
    Spc_5msCtrlCircP5msRawInfo.SecdCircPSig = input[22];
    Spc_5msCtrlPistP5msRawInfo.PistPSig = input[23];
    Spc_5msCtrlAbsCtrlInfo.AbsActFlg = input[24];
    Spc_5msCtrlAbsCtrlInfo.AbsDefectFlg = input[25];
    Spc_5msCtrlAbsCtrlInfo.AbsTarPFrntLe = input[26];
    Spc_5msCtrlAbsCtrlInfo.AbsTarPFrntRi = input[27];
    Spc_5msCtrlAbsCtrlInfo.AbsTarPReLe = input[28];
    Spc_5msCtrlAbsCtrlInfo.AbsTarPReRi = input[29];
    Spc_5msCtrlAbsCtrlInfo.FrntWhlSlip = input[30];
    Spc_5msCtrlAbsCtrlInfo.AbsDesTarP = input[31];
    Spc_5msCtrlAbsCtrlInfo.AbsDesTarPReqFlg = input[32];
    Spc_5msCtrlAbsCtrlInfo.AbsCtrlFadeOutFlg = input[33];
    Spc_5msCtrlEscCtrlInfo.EscActFlg = input[34];
    Spc_5msCtrlEscCtrlInfo.EscDefectFlg = input[35];
    Spc_5msCtrlEscCtrlInfo.EscTarPFrntLe = input[36];
    Spc_5msCtrlEscCtrlInfo.EscTarPFrntRi = input[37];
    Spc_5msCtrlEscCtrlInfo.EscTarPReLe = input[38];
    Spc_5msCtrlEscCtrlInfo.EscTarPReRi = input[39];
    Spc_5msCtrlSwtStsFSInfo.FlexBrkSwtFaultDet = input[40];
    Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg = input[41];
    Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg = input[42];
    Spc_5msCtrlPedlTrvlTagInfo.PdfSigNoiseSuspcFlgH = input[43];
    Spc_5msCtrlPedlTrvlTagInfo.PdfSigTag = input[44];
    Spc_5msCtrlPedlTrvlTagInfo.PdtSigNoiseSuspcFlgH = input[45];
    Spc_5msCtrlPedlTrvlTagInfo.PdtSigTag = input[46];
    Spc_5msCtrlEemSuspectData.Eem_Suspect_CirP1 = input[47];
    Spc_5msCtrlEemSuspectData.Eem_Suspect_CirP2 = input[48];
    Spc_5msCtrlEemSuspectData.Eem_Suspect_SimP = input[49];
    Spc_5msCtrlEemSuspectData.Eem_Suspect_BLS = input[50];
    Spc_5msCtrlEemSuspectData.Eem_Suspect_PedalPDT = input[51];
    Spc_5msCtrlEemSuspectData.Eem_Suspect_PedalPDF = input[52];
    Spc_5msCtrlEcuModeSts = input[53];
    Spc_5msCtrlPCtrlAct = input[54];
    Spc_5msCtrlMotCtrlState = input[55];
    Spc_5msCtrlBlsSwt = input[56];
    Spc_5msCtrlPedlSimPRaw = input[57];
    Spc_5msCtrlActvBrkCtrlrActFlg = input[58];
    Spc_5msCtrlPedlTrvlFinal = input[59];
    Spc_5msCtrlVehSpdFild = input[60];

    Spc_5msCtrl();


    output[0] = Spc_5msCtrlCircPFildInfo.PrimCircPFild;
    output[1] = Spc_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar;
    output[2] = Spc_5msCtrlCircPFildInfo.SecdCircPFild;
    output[3] = Spc_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar;
    output[4] = Spc_5msCtrlCircPOffsCorrdInfo.PrimCircPOffsCorrd;
    output[5] = Spc_5msCtrlCircPOffsCorrdInfo.PrimCircPOffsCorrdStOkFlg;
    output[6] = Spc_5msCtrlCircPOffsCorrdInfo.SecdCircPOffsCorrd;
    output[7] = Spc_5msCtrlCircPOffsCorrdInfo.SecdCircPOffsCorrdStOkFlg;
    output[8] = Spc_5msCtrlPedlSimrPFildInfo.PedlSimrPFild;
    output[9] = Spc_5msCtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar;
    output[10] = Spc_5msCtrlPedlSimrPOffsCorrdInfo.PedlSimrPOffsCorrd;
    output[11] = Spc_5msCtrlPedlSimrPOffsCorrdInfo.PedlSimrPOffsCorrdStOkFlg;
    output[12] = Spc_5msCtrlPedlTrvlFildInfo.PdfFild;
    output[13] = Spc_5msCtrlPedlTrvlFildInfo.PdtFild;
    output[14] = Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdfOffsCorrdStOkFlg;
    output[15] = Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdfRawOffsCorrd;
    output[16] = Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdtOffsCorrdStOkFlg;
    output[17] = Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdtRawOffsCorrd;
    output[18] = Spc_5msCtrlPistPFildInfo.PistPFild;
    output[19] = Spc_5msCtrlPistPFildInfo.PistPFild_1_100Bar;
    output[20] = Spc_5msCtrlPistPOffsCorrdInfo.PistPOffsCorrd;
    output[21] = Spc_5msCtrlPistPOffsCorrdInfo.PistPOffsCorrdStOkFlg;
    output[22] = Spc_5msCtrlWhlSpdFildInfo.WhlSpdFildFrntLe;
    output[23] = Spc_5msCtrlWhlSpdFildInfo.WhlSpdFildFrntRi;
    output[24] = Spc_5msCtrlWhlSpdFildInfo.WhlSpdFildReLe;
    output[25] = Spc_5msCtrlWhlSpdFildInfo.WhlSpdFildReRi;
    output[26] = Spc_5msCtrlIdbSnsrEolOfsCalcInfo.PedalEolOfsCalcOk;
    output[27] = Spc_5msCtrlIdbSnsrEolOfsCalcInfo.PedalEolOfsCalcEnd;
    output[28] = Spc_5msCtrlIdbSnsrEolOfsCalcInfo.PressEolOfsCalcOk;
    output[29] = Spc_5msCtrlIdbSnsrEolOfsCalcInfo.PressEolOfsCalcEnd;
    output[30] = Spc_5msCtrlBlsFild;
    output[31] = Spc_5msCtrlFlexBrkSwtFild;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Spc_5msCtrl_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
