/* Virtual main for linking */
/* purpose : Generation of map file */
#include "Spc_1msCtrl.h"
#include "Spc_5msCtrl.h"

int main(void)
{
    Spc_1msCtrl_Init();
    Spc_5msCtrl_Init();

    while(1)
    {
        Spc_1msCtrl();
        Spc_5msCtrl();
    }
}