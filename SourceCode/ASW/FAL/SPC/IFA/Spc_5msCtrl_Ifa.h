/**
 * @defgroup Spc_5msCtrl_Ifa Spc_5msCtrl_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spc_5msCtrl_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SPC_5MSCTRL_IFA_H_
#define SPC_5MSCTRL_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Spc_5msCtrl_Read_Spc_5msCtrlNormVlvReqVlvActInfo(data) do \
{ \
    *data = Spc_5msCtrlNormVlvReqVlvActInfo; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlEemFailData(data) do \
{ \
    *data = Spc_5msCtrlEemFailData; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlCanRxAccelPedlInfo(data) do \
{ \
    *data = Spc_5msCtrlCanRxAccelPedlInfo; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlPdt5msRawInfo(data) do \
{ \
    *data = Spc_5msCtrlPdt5msRawInfo; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlPdf5msRawInfo(data) do \
{ \
    *data = Spc_5msCtrlPdf5msRawInfo; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlSwtStsFlexBrkInfo(data) do \
{ \
    *data = Spc_5msCtrlSwtStsFlexBrkInfo; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlWhlSpdInfo(data) do \
{ \
    *data = Spc_5msCtrlWhlSpdInfo; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlCircP5msRawInfo(data) do \
{ \
    *data = Spc_5msCtrlCircP5msRawInfo; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlPistP5msRawInfo(data) do \
{ \
    *data = Spc_5msCtrlPistP5msRawInfo; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlAbsCtrlInfo(data) do \
{ \
    *data = Spc_5msCtrlAbsCtrlInfo; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlEscCtrlInfo(data) do \
{ \
    *data = Spc_5msCtrlEscCtrlInfo; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlSwtStsFSInfo(data) do \
{ \
    *data = Spc_5msCtrlSwtStsFSInfo; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo(data) do \
{ \
    *data = Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlPedlTrvlTagInfo(data) do \
{ \
    *data = Spc_5msCtrlPedlTrvlTagInfo; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlEemSuspectData(data) do \
{ \
    *data = Spc_5msCtrlEemSuspectData; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlNormVlvReqVlvActInfo_PrimCutVlvReq(data) do \
{ \
    *data = Spc_5msCtrlNormVlvReqVlvActInfo.PrimCutVlvReq; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlNormVlvReqVlvActInfo_SecdCutVlvReq(data) do \
{ \
    *data = Spc_5msCtrlNormVlvReqVlvActInfo.SecdCutVlvReq; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlNormVlvReqVlvActInfo_PrimCircVlvReq(data) do \
{ \
    *data = Spc_5msCtrlNormVlvReqVlvActInfo.PrimCircVlvReq; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlNormVlvReqVlvActInfo_SecdCircVlvReq(data) do \
{ \
    *data = Spc_5msCtrlNormVlvReqVlvActInfo.SecdCircVlvReq; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlNormVlvReqVlvActInfo_SimVlvReq(data) do \
{ \
    *data = Spc_5msCtrlNormVlvReqVlvActInfo.SimVlvReq; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlEemFailData_Eem_Fail_CirP1(data) do \
{ \
    *data = Spc_5msCtrlEemFailData.Eem_Fail_CirP1; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlEemFailData_Eem_Fail_CirP2(data) do \
{ \
    *data = Spc_5msCtrlEemFailData.Eem_Fail_CirP2; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlEemFailData_Eem_Fail_SimP(data) do \
{ \
    *data = Spc_5msCtrlEemFailData.Eem_Fail_SimP; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlEemFailData_Eem_Fail_BLS(data) do \
{ \
    *data = Spc_5msCtrlEemFailData.Eem_Fail_BLS; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlEemFailData_Eem_Fail_PedalPDT(data) do \
{ \
    *data = Spc_5msCtrlEemFailData.Eem_Fail_PedalPDT; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlEemFailData_Eem_Fail_PedalPDF(data) do \
{ \
    *data = Spc_5msCtrlEemFailData.Eem_Fail_PedalPDF; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlCanRxAccelPedlInfo_AccelPedlVal(data) do \
{ \
    *data = Spc_5msCtrlCanRxAccelPedlInfo.AccelPedlVal; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlCanRxAccelPedlInfo_AccelPedlValErr(data) do \
{ \
    *data = Spc_5msCtrlCanRxAccelPedlInfo.AccelPedlValErr; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlPdt5msRawInfo_PdtSig(data) do \
{ \
    *data = Spc_5msCtrlPdt5msRawInfo.PdtSig; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlPdf5msRawInfo_PdfSig(data) do \
{ \
    *data = Spc_5msCtrlPdf5msRawInfo.PdfSig; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlSwtStsFlexBrkInfo_FlexBrkASwt(data) do \
{ \
    *data = Spc_5msCtrlSwtStsFlexBrkInfo.FlexBrkASwt; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlSwtStsFlexBrkInfo_FlexBrkBSwt(data) do \
{ \
    *data = Spc_5msCtrlSwtStsFlexBrkInfo.FlexBrkBSwt; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlWhlSpdInfo_FlWhlSpd(data) do \
{ \
    *data = Spc_5msCtrlWhlSpdInfo.FlWhlSpd; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlWhlSpdInfo_FrWhlSpd(data) do \
{ \
    *data = Spc_5msCtrlWhlSpdInfo.FrWhlSpd; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlWhlSpdInfo_RlWhlSpd(data) do \
{ \
    *data = Spc_5msCtrlWhlSpdInfo.RlWhlSpd; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlWhlSpdInfo_RrlWhlSpd(data) do \
{ \
    *data = Spc_5msCtrlWhlSpdInfo.RrlWhlSpd; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlCircP5msRawInfo_PrimCircPSig(data) do \
{ \
    *data = Spc_5msCtrlCircP5msRawInfo.PrimCircPSig; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlCircP5msRawInfo_SecdCircPSig(data) do \
{ \
    *data = Spc_5msCtrlCircP5msRawInfo.SecdCircPSig; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlPistP5msRawInfo_PistPSig(data) do \
{ \
    *data = Spc_5msCtrlPistP5msRawInfo.PistPSig; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlAbsCtrlInfo_AbsActFlg(data) do \
{ \
    *data = Spc_5msCtrlAbsCtrlInfo.AbsActFlg; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlAbsCtrlInfo_AbsDefectFlg(data) do \
{ \
    *data = Spc_5msCtrlAbsCtrlInfo.AbsDefectFlg; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlAbsCtrlInfo_AbsTarPFrntLe(data) do \
{ \
    *data = Spc_5msCtrlAbsCtrlInfo.AbsTarPFrntLe; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlAbsCtrlInfo_AbsTarPFrntRi(data) do \
{ \
    *data = Spc_5msCtrlAbsCtrlInfo.AbsTarPFrntRi; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlAbsCtrlInfo_AbsTarPReLe(data) do \
{ \
    *data = Spc_5msCtrlAbsCtrlInfo.AbsTarPReLe; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlAbsCtrlInfo_AbsTarPReRi(data) do \
{ \
    *data = Spc_5msCtrlAbsCtrlInfo.AbsTarPReRi; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlAbsCtrlInfo_FrntWhlSlip(data) do \
{ \
    *data = Spc_5msCtrlAbsCtrlInfo.FrntWhlSlip; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlAbsCtrlInfo_AbsDesTarP(data) do \
{ \
    *data = Spc_5msCtrlAbsCtrlInfo.AbsDesTarP; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlAbsCtrlInfo_AbsDesTarPReqFlg(data) do \
{ \
    *data = Spc_5msCtrlAbsCtrlInfo.AbsDesTarPReqFlg; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlAbsCtrlInfo_AbsCtrlFadeOutFlg(data) do \
{ \
    *data = Spc_5msCtrlAbsCtrlInfo.AbsCtrlFadeOutFlg; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlEscCtrlInfo_EscActFlg(data) do \
{ \
    *data = Spc_5msCtrlEscCtrlInfo.EscActFlg; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlEscCtrlInfo_EscDefectFlg(data) do \
{ \
    *data = Spc_5msCtrlEscCtrlInfo.EscDefectFlg; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlEscCtrlInfo_EscTarPFrntLe(data) do \
{ \
    *data = Spc_5msCtrlEscCtrlInfo.EscTarPFrntLe; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlEscCtrlInfo_EscTarPFrntRi(data) do \
{ \
    *data = Spc_5msCtrlEscCtrlInfo.EscTarPFrntRi; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlEscCtrlInfo_EscTarPReLe(data) do \
{ \
    *data = Spc_5msCtrlEscCtrlInfo.EscTarPReLe; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlEscCtrlInfo_EscTarPReRi(data) do \
{ \
    *data = Spc_5msCtrlEscCtrlInfo.EscTarPReRi; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlSwtStsFSInfo_FlexBrkSwtFaultDet(data) do \
{ \
    *data = Spc_5msCtrlSwtStsFSInfo.FlexBrkSwtFaultDet; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo_PedlTrvlOffsEolCalcReqFlg(data) do \
{ \
    *data = Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo_PSnsrOffsEolCalcReqFlg(data) do \
{ \
    *data = Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlPedlTrvlTagInfo_PdfSigNoiseSuspcFlgH(data) do \
{ \
    *data = Spc_5msCtrlPedlTrvlTagInfo.PdfSigNoiseSuspcFlgH; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlPedlTrvlTagInfo_PdfSigTag(data) do \
{ \
    *data = Spc_5msCtrlPedlTrvlTagInfo.PdfSigTag; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlPedlTrvlTagInfo_PdtSigNoiseSuspcFlgH(data) do \
{ \
    *data = Spc_5msCtrlPedlTrvlTagInfo.PdtSigNoiseSuspcFlgH; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlPedlTrvlTagInfo_PdtSigTag(data) do \
{ \
    *data = Spc_5msCtrlPedlTrvlTagInfo.PdtSigTag; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlEemSuspectData_Eem_Suspect_CirP1(data) do \
{ \
    *data = Spc_5msCtrlEemSuspectData.Eem_Suspect_CirP1; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlEemSuspectData_Eem_Suspect_CirP2(data) do \
{ \
    *data = Spc_5msCtrlEemSuspectData.Eem_Suspect_CirP2; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlEemSuspectData_Eem_Suspect_SimP(data) do \
{ \
    *data = Spc_5msCtrlEemSuspectData.Eem_Suspect_SimP; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlEemSuspectData_Eem_Suspect_BLS(data) do \
{ \
    *data = Spc_5msCtrlEemSuspectData.Eem_Suspect_BLS; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlEemSuspectData_Eem_Suspect_PedalPDT(data) do \
{ \
    *data = Spc_5msCtrlEemSuspectData.Eem_Suspect_PedalPDT; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlEemSuspectData_Eem_Suspect_PedalPDF(data) do \
{ \
    *data = Spc_5msCtrlEemSuspectData.Eem_Suspect_PedalPDF; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlEcuModeSts(data) do \
{ \
    *data = Spc_5msCtrlEcuModeSts; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlPCtrlAct(data) do \
{ \
    *data = Spc_5msCtrlPCtrlAct; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlMotCtrlState(data) do \
{ \
    *data = Spc_5msCtrlMotCtrlState; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlBlsSwt(data) do \
{ \
    *data = Spc_5msCtrlBlsSwt; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlPedlSimPRaw(data) do \
{ \
    *data = Spc_5msCtrlPedlSimPRaw; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlActvBrkCtrlrActFlg(data) do \
{ \
    *data = Spc_5msCtrlActvBrkCtrlrActFlg; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlPedlTrvlFinal(data) do \
{ \
    *data = Spc_5msCtrlPedlTrvlFinal; \
}while(0);

#define Spc_5msCtrl_Read_Spc_5msCtrlVehSpdFild(data) do \
{ \
    *data = Spc_5msCtrlVehSpdFild; \
}while(0);


/* Set Output DE MAcro Function */
#define Spc_5msCtrl_Write_Spc_5msCtrlCircPFildInfo(data) do \
{ \
    Spc_5msCtrlCircPFildInfo = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlCircPOffsCorrdInfo(data) do \
{ \
    Spc_5msCtrlCircPOffsCorrdInfo = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlPedlSimrPFildInfo(data) do \
{ \
    Spc_5msCtrlPedlSimrPFildInfo = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlPedlSimrPOffsCorrdInfo(data) do \
{ \
    Spc_5msCtrlPedlSimrPOffsCorrdInfo = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlPedlTrvlFildInfo(data) do \
{ \
    Spc_5msCtrlPedlTrvlFildInfo = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlPedlTrvlOffsCorrdInfo(data) do \
{ \
    Spc_5msCtrlPedlTrvlOffsCorrdInfo = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlPistPFildInfo(data) do \
{ \
    Spc_5msCtrlPistPFildInfo = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlPistPOffsCorrdInfo(data) do \
{ \
    Spc_5msCtrlPistPOffsCorrdInfo = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlWhlSpdFildInfo(data) do \
{ \
    Spc_5msCtrlWhlSpdFildInfo = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlIdbSnsrEolOfsCalcInfo(data) do \
{ \
    Spc_5msCtrlIdbSnsrEolOfsCalcInfo = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlCircPFildInfo_PrimCircPFild(data) do \
{ \
    Spc_5msCtrlCircPFildInfo.PrimCircPFild = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlCircPFildInfo_PrimCircPFild_1_100Bar(data) do \
{ \
    Spc_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlCircPFildInfo_SecdCircPFild(data) do \
{ \
    Spc_5msCtrlCircPFildInfo.SecdCircPFild = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlCircPFildInfo_SecdCircPFild_1_100Bar(data) do \
{ \
    Spc_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlCircPOffsCorrdInfo_PrimCircPOffsCorrd(data) do \
{ \
    Spc_5msCtrlCircPOffsCorrdInfo.PrimCircPOffsCorrd = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlCircPOffsCorrdInfo_PrimCircPOffsCorrdStOkFlg(data) do \
{ \
    Spc_5msCtrlCircPOffsCorrdInfo.PrimCircPOffsCorrdStOkFlg = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlCircPOffsCorrdInfo_SecdCircPOffsCorrd(data) do \
{ \
    Spc_5msCtrlCircPOffsCorrdInfo.SecdCircPOffsCorrd = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlCircPOffsCorrdInfo_SecdCircPOffsCorrdStOkFlg(data) do \
{ \
    Spc_5msCtrlCircPOffsCorrdInfo.SecdCircPOffsCorrdStOkFlg = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlPedlSimrPFildInfo_PedlSimrPFild(data) do \
{ \
    Spc_5msCtrlPedlSimrPFildInfo.PedlSimrPFild = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlPedlSimrPFildInfo_PedlSimrPFild_1_100Bar(data) do \
{ \
    Spc_5msCtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlPedlSimrPOffsCorrdInfo_PedlSimrPOffsCorrd(data) do \
{ \
    Spc_5msCtrlPedlSimrPOffsCorrdInfo.PedlSimrPOffsCorrd = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlPedlSimrPOffsCorrdInfo_PedlSimrPOffsCorrdStOkFlg(data) do \
{ \
    Spc_5msCtrlPedlSimrPOffsCorrdInfo.PedlSimrPOffsCorrdStOkFlg = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlPedlTrvlFildInfo_PdfFild(data) do \
{ \
    Spc_5msCtrlPedlTrvlFildInfo.PdfFild = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlPedlTrvlFildInfo_PdtFild(data) do \
{ \
    Spc_5msCtrlPedlTrvlFildInfo.PdtFild = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlPedlTrvlOffsCorrdInfo_PdfOffsCorrdStOkFlg(data) do \
{ \
    Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdfOffsCorrdStOkFlg = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlPedlTrvlOffsCorrdInfo_PdfRawOffsCorrd(data) do \
{ \
    Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdfRawOffsCorrd = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlPedlTrvlOffsCorrdInfo_PdtOffsCorrdStOkFlg(data) do \
{ \
    Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdtOffsCorrdStOkFlg = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlPedlTrvlOffsCorrdInfo_PdtRawOffsCorrd(data) do \
{ \
    Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdtRawOffsCorrd = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlPistPFildInfo_PistPFild(data) do \
{ \
    Spc_5msCtrlPistPFildInfo.PistPFild = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlPistPFildInfo_PistPFild_1_100Bar(data) do \
{ \
    Spc_5msCtrlPistPFildInfo.PistPFild_1_100Bar = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlPistPOffsCorrdInfo_PistPOffsCorrd(data) do \
{ \
    Spc_5msCtrlPistPOffsCorrdInfo.PistPOffsCorrd = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlPistPOffsCorrdInfo_PistPOffsCorrdStOkFlg(data) do \
{ \
    Spc_5msCtrlPistPOffsCorrdInfo.PistPOffsCorrdStOkFlg = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlWhlSpdFildInfo_WhlSpdFildFrntLe(data) do \
{ \
    Spc_5msCtrlWhlSpdFildInfo.WhlSpdFildFrntLe = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlWhlSpdFildInfo_WhlSpdFildFrntRi(data) do \
{ \
    Spc_5msCtrlWhlSpdFildInfo.WhlSpdFildFrntRi = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlWhlSpdFildInfo_WhlSpdFildReLe(data) do \
{ \
    Spc_5msCtrlWhlSpdFildInfo.WhlSpdFildReLe = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlWhlSpdFildInfo_WhlSpdFildReRi(data) do \
{ \
    Spc_5msCtrlWhlSpdFildInfo.WhlSpdFildReRi = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlIdbSnsrEolOfsCalcInfo_PedalEolOfsCalcOk(data) do \
{ \
    Spc_5msCtrlIdbSnsrEolOfsCalcInfo.PedalEolOfsCalcOk = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlIdbSnsrEolOfsCalcInfo_PedalEolOfsCalcEnd(data) do \
{ \
    Spc_5msCtrlIdbSnsrEolOfsCalcInfo.PedalEolOfsCalcEnd = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlIdbSnsrEolOfsCalcInfo_PressEolOfsCalcOk(data) do \
{ \
    Spc_5msCtrlIdbSnsrEolOfsCalcInfo.PressEolOfsCalcOk = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlIdbSnsrEolOfsCalcInfo_PressEolOfsCalcEnd(data) do \
{ \
    Spc_5msCtrlIdbSnsrEolOfsCalcInfo.PressEolOfsCalcEnd = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlBlsFild(data) do \
{ \
    Spc_5msCtrlBlsFild = *data; \
}while(0);

#define Spc_5msCtrl_Write_Spc_5msCtrlFlexBrkSwtFild(data) do \
{ \
    Spc_5msCtrlFlexBrkSwtFild = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SPC_5MSCTRL_IFA_H_ */
/** @} */
