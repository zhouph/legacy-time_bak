/**
 * @defgroup Spc_1msCtrl_Ifa Spc_1msCtrl_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spc_1msCtrl_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SPC_1MSCTRL_IFA_H_
#define SPC_1MSCTRL_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Spc_1msCtrl_Read_Spc_1msCtrlCircP1msRawInfo(data) do \
{ \
    *data = Spc_1msCtrlCircP1msRawInfo; \
}while(0);

#define Spc_1msCtrl_Read_Spc_1msCtrlPistP1msRawInfo(data) do \
{ \
    *data = Spc_1msCtrlPistP1msRawInfo; \
}while(0);

#define Spc_1msCtrl_Read_Spc_1msCtrlPdt1msRawInfo(data) do \
{ \
    *data = Spc_1msCtrlPdt1msRawInfo; \
}while(0);

#define Spc_1msCtrl_Read_Spc_1msCtrlCircP1msRawInfo_PrimCircPSig(data) do \
{ \
    *data = Spc_1msCtrlCircP1msRawInfo.PrimCircPSig; \
}while(0);

#define Spc_1msCtrl_Read_Spc_1msCtrlCircP1msRawInfo_SecdCircPSig(data) do \
{ \
    *data = Spc_1msCtrlCircP1msRawInfo.SecdCircPSig; \
}while(0);

#define Spc_1msCtrl_Read_Spc_1msCtrlPistP1msRawInfo_PistPSig(data) do \
{ \
    *data = Spc_1msCtrlPistP1msRawInfo.PistPSig; \
}while(0);

#define Spc_1msCtrl_Read_Spc_1msCtrlPdt1msRawInfo_PdtSig(data) do \
{ \
    *data = Spc_1msCtrlPdt1msRawInfo.PdtSig; \
}while(0);

#define Spc_1msCtrl_Read_Spc_1msCtrlEcuModeSts(data) do \
{ \
    *data = Spc_1msCtrlEcuModeSts; \
}while(0);


/* Set Output DE MAcro Function */
#define Spc_1msCtrl_Write_Spc_1msCtrlCircPFild1msInfo(data) do \
{ \
    Spc_1msCtrlCircPFild1msInfo = *data; \
}while(0);

#define Spc_1msCtrl_Write_Spc_1msCtrlPedlTrvlFild1msInfo(data) do \
{ \
    Spc_1msCtrlPedlTrvlFild1msInfo = *data; \
}while(0);

#define Spc_1msCtrl_Write_Spc_1msCtrlPistPFild1msInfo(data) do \
{ \
    Spc_1msCtrlPistPFild1msInfo = *data; \
}while(0);

#define Spc_1msCtrl_Write_Spc_1msCtrlCircPFild1msInfo_PrimCircPFild1ms(data) do \
{ \
    Spc_1msCtrlCircPFild1msInfo.PrimCircPFild1ms = *data; \
}while(0);

#define Spc_1msCtrl_Write_Spc_1msCtrlCircPFild1msInfo_SecdCircPFild1ms(data) do \
{ \
    Spc_1msCtrlCircPFild1msInfo.SecdCircPFild1ms = *data; \
}while(0);

#define Spc_1msCtrl_Write_Spc_1msCtrlPedlTrvlFild1msInfo_PdtFild1ms(data) do \
{ \
    Spc_1msCtrlPedlTrvlFild1msInfo.PdtFild1ms = *data; \
}while(0);

#define Spc_1msCtrl_Write_Spc_1msCtrlPistPFild1msInfo_PistPFild1ms(data) do \
{ \
    Spc_1msCtrlPistPFild1msInfo.PistPFild1ms = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SPC_1MSCTRL_IFA_H_ */
/** @} */
