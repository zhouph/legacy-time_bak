/**
 * @defgroup Spc_1msCtrl Spc_1msCtrl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spc_1msCtrl.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SPC_1MSCTRL_H_
#define SPC_1MSCTRL_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Spc_Types.h"
#include "Spc_Cfg.h"
#include "Spc_Cal.h"
#include "L_CommonFunction_1ms.h"
/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define SPC_1MSCTRL_MODULE_ID      (0)
 #define SPC_1MSCTRL_MAJOR_VERSION  (2)
 #define SPC_1MSCTRL_MINOR_VERSION  (0)
 #define SPC_1MSCTRL_PATCH_VERSION  (0)
 #define SPC_1MSCTRL_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Spc_1msCtrl_HdrBusType Spc_1msCtrlBus;

/* Internal Logic Bus */
extern IDBSignalProcessing_SRCbusType   SIGPROCsrcbus;

/* Version Info */
extern const SwcVersionInfo_t Spc_1msCtrlVersionInfo;

/* Input Data Element */
extern Press_SenCircP1msRawInfo_t Spc_1msCtrlCircP1msRawInfo;
extern Press_SenPistP1msRawInfo_t Spc_1msCtrlPistP1msRawInfo;
extern Pedal_SenPdt1msRawInfo_t Spc_1msCtrlPdt1msRawInfo;
extern Mom_HndlrEcuModeSts_t Spc_1msCtrlEcuModeSts;

/* Output Data Element */
extern Spc_1msCtrlCircPFild1msInfo_t Spc_1msCtrlCircPFild1msInfo;
extern Spc_1msCtrlPedlTrvlFild1msInfo_t Spc_1msCtrlPedlTrvlFild1msInfo;
extern Spc_1msCtrlPistPFild1msInfo_t Spc_1msCtrlPistPFild1msInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Spc_1msCtrl_Init(void);
extern void Spc_1msCtrl(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SPC_1MSCTRL_H_ */
/** @} */
