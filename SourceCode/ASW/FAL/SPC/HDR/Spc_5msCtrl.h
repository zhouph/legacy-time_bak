/**
 * @defgroup Spc_5msCtrl Spc_5msCtrl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spc_5msCtrl.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SPC_5MSCTRL_H_
#define SPC_5MSCTRL_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Spc_Types.h"
#include "Spc_Cfg.h"
#include "Spc_Cal.h"
#include "../../../../BSW/NVM/HDR/NvMIf.h"
/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define SPC_5MSCTRL_MODULE_ID      (0)
 #define SPC_5MSCTRL_MAJOR_VERSION  (2)
 #define SPC_5MSCTRL_MINOR_VERSION  (0)
 #define SPC_5MSCTRL_PATCH_VERSION  (0)
 #define SPC_5MSCTRL_BRANCH_VERSION (0)

#define     U16_T_2_S               400 
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Spc_5msCtrl_HdrBusType Spc_5msCtrlBus;

/* Internal Logic Bus */
extern IDBSignalProcessing_SRCbusType   SIGPROCsrcbus;

/* Version Info */
extern const SwcVersionInfo_t Spc_5msCtrlVersionInfo;

/* Input Data Element */
extern Vat_CtrlNormVlvReqVlvActInfo_t Spc_5msCtrlNormVlvReqVlvActInfo;
extern Eem_MainEemFailData_t Spc_5msCtrlEemFailData;
extern Proxy_RxCanRxAccelPedlInfo_t Spc_5msCtrlCanRxAccelPedlInfo;
extern Pedal_SenSyncPdt5msRawInfo_t Spc_5msCtrlPdt5msRawInfo;
extern Pedal_SenSyncPdf5msRawInfo_t Spc_5msCtrlPdf5msRawInfo;
extern Swt_SenSwtStsFlexBrkInfo_t Spc_5msCtrlSwtStsFlexBrkInfo;
extern Wss_SenWhlSpdInfo_t Spc_5msCtrlWhlSpdInfo;
extern Press_SenSyncCircP5msRawInfo_t Spc_5msCtrlCircP5msRawInfo;
extern Press_SenSyncPistP5msRawInfo_t Spc_5msCtrlPistP5msRawInfo;
extern Abc_CtrlAbsCtrlInfo_t Spc_5msCtrlAbsCtrlInfo;
extern Abc_CtrlEscCtrlInfo_t Spc_5msCtrlEscCtrlInfo;
extern Eem_SuspcDetnSwtStsFSInfo_t Spc_5msCtrlSwtStsFSInfo;
extern Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_t Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo;
extern Det_5msCtrlPedlTrvlTagInfo_t Spc_5msCtrlPedlTrvlTagInfo;
extern Eem_MainEemSuspectData_t Spc_5msCtrlEemSuspectData;
extern Mom_HndlrEcuModeSts_t Spc_5msCtrlEcuModeSts;
extern Pct_5msCtrlPCtrlAct_t Spc_5msCtrlPCtrlAct;
extern Mcc_1msCtrlMotCtrlState_t Spc_5msCtrlMotCtrlState;
extern Swt_SenBlsSwt_t Spc_5msCtrlBlsSwt;
extern Press_SenSyncPedlSimPRaw_t Spc_5msCtrlPedlSimPRaw;
extern Abc_CtrlActvBrkCtrlrActFlg_t Spc_5msCtrlActvBrkCtrlrActFlg;
extern Det_5msCtrlPedlTrvlFinal_t Spc_5msCtrlPedlTrvlFinal;
extern Det_5msCtrlVehSpdFild_t Spc_5msCtrlVehSpdFild;

/* Output Data Element */
extern Spc_5msCtrlCircPFildInfo_t Spc_5msCtrlCircPFildInfo;
extern Spc_5msCtrlCircPOffsCorrdInfo_t Spc_5msCtrlCircPOffsCorrdInfo;
extern Spc_5msCtrlPedlSimrPFildInfo_t Spc_5msCtrlPedlSimrPFildInfo;
extern Spc_5msCtrlPedlSimrPOffsCorrdInfo_t Spc_5msCtrlPedlSimrPOffsCorrdInfo;
extern Spc_5msCtrlPedlTrvlFildInfo_t Spc_5msCtrlPedlTrvlFildInfo;
extern Spc_5msCtrlPedlTrvlOffsCorrdInfo_t Spc_5msCtrlPedlTrvlOffsCorrdInfo;
extern Spc_5msCtrlPistPFildInfo_t Spc_5msCtrlPistPFildInfo;
extern Spc_5msCtrlPistPOffsCorrdInfo_t Spc_5msCtrlPistPOffsCorrdInfo;
extern Spc_5msCtrlWhlSpdFildInfo_t Spc_5msCtrlWhlSpdFildInfo;
extern Spc_5msCtrlIdbSnsrEolOfsCalcInfo_t Spc_5msCtrlIdbSnsrEolOfsCalcInfo;
extern Spc_5msCtrlBlsFild_t Spc_5msCtrlBlsFild;
extern Spc_5msCtrlFlexBrkSwtFild_t Spc_5msCtrlFlexBrkSwtFild;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Spc_5msCtrl_Init(void);
extern void Spc_5msCtrl(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SPC_5MSCTRL_H_ */
/** @} */
