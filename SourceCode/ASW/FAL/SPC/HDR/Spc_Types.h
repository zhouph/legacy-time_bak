/**
 * @defgroup Spc_Types Spc_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spc_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SPC_TYPES_H_
#define SPC_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Asw_Types.h" /* HSH */
#include "Bsw_Types.h"
#include "Rte_Types.h"
#include "RteDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Press_SenCircP1msRawInfo_t Spc_1msCtrlCircP1msRawInfo;
    Press_SenPistP1msRawInfo_t Spc_1msCtrlPistP1msRawInfo;
    Pedal_SenPdt1msRawInfo_t Spc_1msCtrlPdt1msRawInfo;
    Mom_HndlrEcuModeSts_t Spc_1msCtrlEcuModeSts;

/* Output Data Element */
    Spc_1msCtrlCircPFild1msInfo_t Spc_1msCtrlCircPFild1msInfo;
    Spc_1msCtrlPedlTrvlFild1msInfo_t Spc_1msCtrlPedlTrvlFild1msInfo;
    Spc_1msCtrlPistPFild1msInfo_t Spc_1msCtrlPistPFild1msInfo;
}Spc_1msCtrl_HdrBusType;

typedef struct
{
/* Input Data Element */
    Vat_CtrlNormVlvReqVlvActInfo_t Spc_5msCtrlNormVlvReqVlvActInfo;
    Eem_MainEemFailData_t Spc_5msCtrlEemFailData;
    Proxy_RxCanRxAccelPedlInfo_t Spc_5msCtrlCanRxAccelPedlInfo;
    Pedal_SenSyncPdt5msRawInfo_t Spc_5msCtrlPdt5msRawInfo;
    Pedal_SenSyncPdf5msRawInfo_t Spc_5msCtrlPdf5msRawInfo;
    Swt_SenSwtStsFlexBrkInfo_t Spc_5msCtrlSwtStsFlexBrkInfo;
    Wss_SenWhlSpdInfo_t Spc_5msCtrlWhlSpdInfo;
    Press_SenSyncCircP5msRawInfo_t Spc_5msCtrlCircP5msRawInfo;
    Press_SenSyncPistP5msRawInfo_t Spc_5msCtrlPistP5msRawInfo;
    Abc_CtrlAbsCtrlInfo_t Spc_5msCtrlAbsCtrlInfo;
    Abc_CtrlEscCtrlInfo_t Spc_5msCtrlEscCtrlInfo;
    Eem_SuspcDetnSwtStsFSInfo_t Spc_5msCtrlSwtStsFSInfo;
    Diag_HndlrIdbSnsrOffsEolCalcReqByDiagInfo_t Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo;
    Det_5msCtrlPedlTrvlTagInfo_t Spc_5msCtrlPedlTrvlTagInfo;
    Eem_MainEemSuspectData_t Spc_5msCtrlEemSuspectData;
    Mom_HndlrEcuModeSts_t Spc_5msCtrlEcuModeSts;
    Pct_5msCtrlPCtrlAct_t Spc_5msCtrlPCtrlAct;
    Mcc_1msCtrlMotCtrlState_t Spc_5msCtrlMotCtrlState;
    Swt_SenBlsSwt_t Spc_5msCtrlBlsSwt;
    Press_SenSyncPedlSimPRaw_t Spc_5msCtrlPedlSimPRaw;
    Abc_CtrlActvBrkCtrlrActFlg_t Spc_5msCtrlActvBrkCtrlrActFlg;
    Det_5msCtrlPedlTrvlFinal_t Spc_5msCtrlPedlTrvlFinal;
    Det_5msCtrlVehSpdFild_t Spc_5msCtrlVehSpdFild;

/* Output Data Element */
    Spc_5msCtrlCircPFildInfo_t Spc_5msCtrlCircPFildInfo;
    Spc_5msCtrlCircPOffsCorrdInfo_t Spc_5msCtrlCircPOffsCorrdInfo;
    Spc_5msCtrlPedlSimrPFildInfo_t Spc_5msCtrlPedlSimrPFildInfo;
    Spc_5msCtrlPedlSimrPOffsCorrdInfo_t Spc_5msCtrlPedlSimrPOffsCorrdInfo;
    Spc_5msCtrlPedlTrvlFildInfo_t Spc_5msCtrlPedlTrvlFildInfo;
    Spc_5msCtrlPedlTrvlOffsCorrdInfo_t Spc_5msCtrlPedlTrvlOffsCorrdInfo;
    Spc_5msCtrlPistPFildInfo_t Spc_5msCtrlPistPFildInfo;
    Spc_5msCtrlPistPOffsCorrdInfo_t Spc_5msCtrlPistPOffsCorrdInfo;
    Spc_5msCtrlWhlSpdFildInfo_t Spc_5msCtrlWhlSpdFildInfo;
    Spc_5msCtrlIdbSnsrEolOfsCalcInfo_t Spc_5msCtrlIdbSnsrEolOfsCalcInfo;
    Spc_5msCtrlBlsFild_t Spc_5msCtrlBlsFild;
    Spc_5msCtrlFlexBrkSwtFild_t Spc_5msCtrlFlexBrkSwtFild;
}Spc_5msCtrl_HdrBusType;

typedef struct
{
    sint32 SystemOnCnt;
    int32_t SystemOnCnt1ms;
    sint32 PdtOffs_f;
    sint32 PistPOffs_f;
    sint32 PrimCircPOffs_f;
    sint32 SecdyCircPOffs_f;
    sint32 PrimCircPFild1ms_1_100Bar_Avg;
    sint32 SecdCircPFild1ms_1_100Bar_Avg;
    sint32 PistPFild1ms_1_100Bar_Avg;
}IDBSignalProcessing_SRCbusType;



/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SPC_TYPES_H_ */
/** @} */
