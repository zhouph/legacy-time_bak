# \file
#
# \brief AUTOSAR ApplTemplates
#
# This file contains the implementation of the AUTOSAR
# module ApplTemplates.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

# This file contains common compiler options for all TRICORE devices using
# the TASKING toolchain.


# TASKING_MODE:
#
# To compile a C source file the C compiler and the assembler must
# be called. This can be done by one call of the control program cctc
# or by a call of the compiler ctc and the assembler astc. The variable
# TASKING_MODE allows to select the kind of the tool chain call.
# Valid values are CONTROL_PROGRAM and COMPILER_ASSEMBLER.
TASKING_MODE = CONTROL_PROGRAM


# CC_OPT defines common options for the compiler.

# Set TriCore EABI compliancy mode:
# -D disallow an alternative DWARF version
# -f allow treat 'double' as 'float'; see also --no-double option
# -H disallow half-word alignment
# -N disallow the use of option --no-clear
# -S disallow structure-return optimization
CC_OPT += --eabi=DfHNS

# 0 byte threshold for putting data in __near field.
# This prevents the compiler from moving data out of sections,
# where the OS expects it to be located for its protection features.
CC_OPT += --default-near-size=0

# Generate symbolic debug information
CC_OPT += -g

# Disable warnings, which are considered harmless from Q-Records:
#  - 515: side effects of 'sizeof' operand will be ignored
#  - 529: overflow in constant expression
#  - 544: unreachable code
#  - 581: usage of pointers to different but compatible types
#  - 588: dead assignment
CC_OPT += -Wc-w515 -Wc-w529 -Wc-w544 -Wc-w581 -Wc-w588

# Due to false positive compiler warnings with -OP (disabled
# "Constant propagation") and -Ol (enabled "Loop transformation")
# suppress 549: condition is always true
# Tasking ticket link: http://issues.tasking.com/?issueid=160-37363
CC_OPT += -Wc-w549

# Define the default options for the assembler

# Use the TASKING preprocessor
ASM_OPT += -mt

# Define the options for the linker

# Tell linker about LINK_PLACE options
LINK_OPT += $(LINK_PLACE)

# Link against the runtime library
LINK_OPT += -lrt

# Link against the C library
LINK_OPT += -lc

# Specify include directory for linker scripts
LINK_OPT += -I$(BOARD_PROJECT_PATH)

# Create a map file
LINK_OPT += -M$(MAP_FILE)

# Linker archive options: insert-replace/create/update
AR_OPT += -rcu

# EXT_LOCATOR_FILE:
# specify the name for an external locator file
# if no name is given, a default locator file $(BOARD).ldscript is taken
# which is composed in file <board>.mak
EXT_LOCATOR_FILE +=

# General path setup

# Path where the map file should get generated
MAP_FILE = $(BIN_OUTPUT_PATH)\$(PROJECT).map

# Path where the output file should get generated
OUT_FILE = $(BUILD_DIR)\$(PROJECT).out
