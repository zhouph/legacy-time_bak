#include "unity.h"
#include "unity_fixture.h"
#include "Spc_1msCtrl.h"
#include "Spc_1msCtrl_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Salsint16 UtInput_Spc_1msCtrlCircP1msRawInfo_PrimCircPSig[MAX_STEP] = SPC_1MSCTRLCIRCP1MSRAWINFO_PRIMCIRCPSIG;
const Salsint16 UtInput_Spc_1msCtrlCircP1msRawInfo_SecdCircPSig[MAX_STEP] = SPC_1MSCTRLCIRCP1MSRAWINFO_SECDCIRCPSIG;
const Salsint16 UtInput_Spc_1msCtrlPistP1msRawInfo_PistPSig[MAX_STEP] = SPC_1MSCTRLPISTP1MSRAWINFO_PISTPSIG;
const Salsint16 UtInput_Spc_1msCtrlPdt1msRawInfo_PdtSig[MAX_STEP] = SPC_1MSCTRLPDT1MSRAWINFO_PDTSIG;
const Mom_HndlrEcuModeSts_t UtInput_Spc_1msCtrlEcuModeSts[MAX_STEP] = SPC_1MSCTRLECUMODESTS;

const Rtesint32 UtExpected_Spc_1msCtrlCircPFild1msInfo_PrimCircPFild1ms[MAX_STEP] = SPC_1MSCTRLCIRCPFILD1MSINFO_PRIMCIRCPFILD1MS;
const Rtesint32 UtExpected_Spc_1msCtrlCircPFild1msInfo_SecdCircPFild1ms[MAX_STEP] = SPC_1MSCTRLCIRCPFILD1MSINFO_SECDCIRCPFILD1MS;
const Rtesint32 UtExpected_Spc_1msCtrlPedlTrvlFild1msInfo_PdtFild1ms[MAX_STEP] = SPC_1MSCTRLPEDLTRVLFILD1MSINFO_PDTFILD1MS;
const Rtesint32 UtExpected_Spc_1msCtrlPistPFild1msInfo_PistPFild1ms[MAX_STEP] = SPC_1MSCTRLPISTPFILD1MSINFO_PISTPFILD1MS;



TEST_GROUP(Spc_1msCtrl);
TEST_SETUP(Spc_1msCtrl)
{
    Spc_1msCtrl_Init();
}

TEST_TEAR_DOWN(Spc_1msCtrl)
{   /* Postcondition */

}

TEST(Spc_1msCtrl, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Spc_1msCtrlCircP1msRawInfo.PrimCircPSig = UtInput_Spc_1msCtrlCircP1msRawInfo_PrimCircPSig[i];
        Spc_1msCtrlCircP1msRawInfo.SecdCircPSig = UtInput_Spc_1msCtrlCircP1msRawInfo_SecdCircPSig[i];
        Spc_1msCtrlPistP1msRawInfo.PistPSig = UtInput_Spc_1msCtrlPistP1msRawInfo_PistPSig[i];
        Spc_1msCtrlPdt1msRawInfo.PdtSig = UtInput_Spc_1msCtrlPdt1msRawInfo_PdtSig[i];
        Spc_1msCtrlEcuModeSts = UtInput_Spc_1msCtrlEcuModeSts[i];

        Spc_1msCtrl();

        TEST_ASSERT_EQUAL(Spc_1msCtrlCircPFild1msInfo.PrimCircPFild1ms, UtExpected_Spc_1msCtrlCircPFild1msInfo_PrimCircPFild1ms[i]);
        TEST_ASSERT_EQUAL(Spc_1msCtrlCircPFild1msInfo.SecdCircPFild1ms, UtExpected_Spc_1msCtrlCircPFild1msInfo_SecdCircPFild1ms[i]);
        TEST_ASSERT_EQUAL(Spc_1msCtrlPedlTrvlFild1msInfo.PdtFild1ms, UtExpected_Spc_1msCtrlPedlTrvlFild1msInfo_PdtFild1ms[i]);
        TEST_ASSERT_EQUAL(Spc_1msCtrlPistPFild1msInfo.PistPFild1ms, UtExpected_Spc_1msCtrlPistPFild1msInfo_PistPFild1ms[i]);
    }
}

TEST_GROUP_RUNNER(Spc_1msCtrl)
{
    RUN_TEST_CASE(Spc_1msCtrl, All);
}
