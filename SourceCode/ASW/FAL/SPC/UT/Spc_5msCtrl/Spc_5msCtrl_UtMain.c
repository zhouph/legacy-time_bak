#include "unity.h"
#include "unity_fixture.h"
#include "Spc_5msCtrl.h"
#include "Spc_5msCtrl_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Rteuint8 UtInput_Spc_5msCtrlNormVlvReqVlvActInfo_PrimCutVlvReq[MAX_STEP] = SPC_5MSCTRLNORMVLVREQVLVACTINFO_PRIMCUTVLVREQ;
const Rteuint8 UtInput_Spc_5msCtrlNormVlvReqVlvActInfo_SecdCutVlvReq[MAX_STEP] = SPC_5MSCTRLNORMVLVREQVLVACTINFO_SECDCUTVLVREQ;
const Rteuint8 UtInput_Spc_5msCtrlNormVlvReqVlvActInfo_PrimCircVlvReq[MAX_STEP] = SPC_5MSCTRLNORMVLVREQVLVACTINFO_PRIMCIRCVLVREQ;
const Rteuint8 UtInput_Spc_5msCtrlNormVlvReqVlvActInfo_SecdCircVlvReq[MAX_STEP] = SPC_5MSCTRLNORMVLVREQVLVACTINFO_SECDCIRCVLVREQ;
const Rteuint8 UtInput_Spc_5msCtrlNormVlvReqVlvActInfo_SimVlvReq[MAX_STEP] = SPC_5MSCTRLNORMVLVREQVLVACTINFO_SIMVLVREQ;
const Saluint8 UtInput_Spc_5msCtrlEemFailData_Eem_Fail_CirP1[MAX_STEP] = SPC_5MSCTRLEEMFAILDATA_EEM_FAIL_CIRP1;
const Saluint8 UtInput_Spc_5msCtrlEemFailData_Eem_Fail_CirP2[MAX_STEP] = SPC_5MSCTRLEEMFAILDATA_EEM_FAIL_CIRP2;
const Saluint8 UtInput_Spc_5msCtrlEemFailData_Eem_Fail_SimP[MAX_STEP] = SPC_5MSCTRLEEMFAILDATA_EEM_FAIL_SIMP;
const Saluint8 UtInput_Spc_5msCtrlEemFailData_Eem_Fail_BLS[MAX_STEP] = SPC_5MSCTRLEEMFAILDATA_EEM_FAIL_BLS;
const Saluint8 UtInput_Spc_5msCtrlEemFailData_Eem_Fail_PedalPDT[MAX_STEP] = SPC_5MSCTRLEEMFAILDATA_EEM_FAIL_PEDALPDT;
const Saluint8 UtInput_Spc_5msCtrlEemFailData_Eem_Fail_PedalPDF[MAX_STEP] = SPC_5MSCTRLEEMFAILDATA_EEM_FAIL_PEDALPDF;
const Salsint16 UtInput_Spc_5msCtrlCanRxAccelPedlInfo_AccelPedlVal[MAX_STEP] = SPC_5MSCTRLCANRXACCELPEDLINFO_ACCELPEDLVAL;
const Saluint8 UtInput_Spc_5msCtrlCanRxAccelPedlInfo_AccelPedlValErr[MAX_STEP] = SPC_5MSCTRLCANRXACCELPEDLINFO_ACCELPEDLVALERR;
const Salsint16 UtInput_Spc_5msCtrlPdt5msRawInfo_PdtSig[MAX_STEP] = SPC_5MSCTRLPDT5MSRAWINFO_PDTSIG;
const Salsint16 UtInput_Spc_5msCtrlPdf5msRawInfo_PdfSig[MAX_STEP] = SPC_5MSCTRLPDF5MSRAWINFO_PDFSIG;
const Saluint8 UtInput_Spc_5msCtrlSwtStsFlexBrkInfo_FlexBrkASwt[MAX_STEP] = SPC_5MSCTRLSWTSTSFLEXBRKINFO_FLEXBRKASWT;
const Saluint8 UtInput_Spc_5msCtrlSwtStsFlexBrkInfo_FlexBrkBSwt[MAX_STEP] = SPC_5MSCTRLSWTSTSFLEXBRKINFO_FLEXBRKBSWT;
const Saluint16 UtInput_Spc_5msCtrlWhlSpdInfo_FlWhlSpd[MAX_STEP] = SPC_5MSCTRLWHLSPDINFO_FLWHLSPD;
const Saluint16 UtInput_Spc_5msCtrlWhlSpdInfo_FrWhlSpd[MAX_STEP] = SPC_5MSCTRLWHLSPDINFO_FRWHLSPD;
const Saluint16 UtInput_Spc_5msCtrlWhlSpdInfo_RlWhlSpd[MAX_STEP] = SPC_5MSCTRLWHLSPDINFO_RLWHLSPD;
const Saluint16 UtInput_Spc_5msCtrlWhlSpdInfo_RrlWhlSpd[MAX_STEP] = SPC_5MSCTRLWHLSPDINFO_RRLWHLSPD;
const Salsint16 UtInput_Spc_5msCtrlCircP5msRawInfo_PrimCircPSig[MAX_STEP] = SPC_5MSCTRLCIRCP5MSRAWINFO_PRIMCIRCPSIG;
const Salsint16 UtInput_Spc_5msCtrlCircP5msRawInfo_SecdCircPSig[MAX_STEP] = SPC_5MSCTRLCIRCP5MSRAWINFO_SECDCIRCPSIG;
const Salsint16 UtInput_Spc_5msCtrlPistP5msRawInfo_PistPSig[MAX_STEP] = SPC_5MSCTRLPISTP5MSRAWINFO_PISTPSIG;
const Rtesint32 UtInput_Spc_5msCtrlAbsCtrlInfo_AbsActFlg[MAX_STEP] = SPC_5MSCTRLABSCTRLINFO_ABSACTFLG;
const Rtesint32 UtInput_Spc_5msCtrlAbsCtrlInfo_AbsDefectFlg[MAX_STEP] = SPC_5MSCTRLABSCTRLINFO_ABSDEFECTFLG;
const Rtesint32 UtInput_Spc_5msCtrlAbsCtrlInfo_AbsTarPFrntLe[MAX_STEP] = SPC_5MSCTRLABSCTRLINFO_ABSTARPFRNTLE;
const Rtesint32 UtInput_Spc_5msCtrlAbsCtrlInfo_AbsTarPFrntRi[MAX_STEP] = SPC_5MSCTRLABSCTRLINFO_ABSTARPFRNTRI;
const Rtesint32 UtInput_Spc_5msCtrlAbsCtrlInfo_AbsTarPReLe[MAX_STEP] = SPC_5MSCTRLABSCTRLINFO_ABSTARPRELE;
const Rtesint32 UtInput_Spc_5msCtrlAbsCtrlInfo_AbsTarPReRi[MAX_STEP] = SPC_5MSCTRLABSCTRLINFO_ABSTARPRERI;
const Rtesint32 UtInput_Spc_5msCtrlAbsCtrlInfo_FrntWhlSlip[MAX_STEP] = SPC_5MSCTRLABSCTRLINFO_FRNTWHLSLIP;
const Rtesint32 UtInput_Spc_5msCtrlAbsCtrlInfo_AbsDesTarP[MAX_STEP] = SPC_5MSCTRLABSCTRLINFO_ABSDESTARP;
const Rtesint32 UtInput_Spc_5msCtrlAbsCtrlInfo_AbsDesTarPReqFlg[MAX_STEP] = SPC_5MSCTRLABSCTRLINFO_ABSDESTARPREQFLG;
const Rtesint32 UtInput_Spc_5msCtrlAbsCtrlInfo_AbsCtrlFadeOutFlg[MAX_STEP] = SPC_5MSCTRLABSCTRLINFO_ABSCTRLFADEOUTFLG;
const Rtesint32 UtInput_Spc_5msCtrlEscCtrlInfo_EscActFlg[MAX_STEP] = SPC_5MSCTRLESCCTRLINFO_ESCACTFLG;
const Rtesint32 UtInput_Spc_5msCtrlEscCtrlInfo_EscDefectFlg[MAX_STEP] = SPC_5MSCTRLESCCTRLINFO_ESCDEFECTFLG;
const Rtesint32 UtInput_Spc_5msCtrlEscCtrlInfo_EscTarPFrntLe[MAX_STEP] = SPC_5MSCTRLESCCTRLINFO_ESCTARPFRNTLE;
const Rtesint32 UtInput_Spc_5msCtrlEscCtrlInfo_EscTarPFrntRi[MAX_STEP] = SPC_5MSCTRLESCCTRLINFO_ESCTARPFRNTRI;
const Rtesint32 UtInput_Spc_5msCtrlEscCtrlInfo_EscTarPReLe[MAX_STEP] = SPC_5MSCTRLESCCTRLINFO_ESCTARPRELE;
const Rtesint32 UtInput_Spc_5msCtrlEscCtrlInfo_EscTarPReRi[MAX_STEP] = SPC_5MSCTRLESCCTRLINFO_ESCTARPRERI;
const Salsint32 UtInput_Spc_5msCtrlSwtStsFSInfo_FlexBrkSwtFaultDet[MAX_STEP] = SPC_5MSCTRLSWTSTSFSINFO_FLEXBRKSWTFAULTDET;
const Salsint32 UtInput_Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo_PedlTrvlOffsEolCalcReqFlg[MAX_STEP] = SPC_5MSCTRLIDBSNSROFFSEOLCALCREQBYDIAGINFO_PEDLTRVLOFFSEOLCALCREQFLG;
const Salsint32 UtInput_Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo_PSnsrOffsEolCalcReqFlg[MAX_STEP] = SPC_5MSCTRLIDBSNSROFFSEOLCALCREQBYDIAGINFO_PSNSROFFSEOLCALCREQFLG;
const Rtesint32 UtInput_Spc_5msCtrlPedlTrvlTagInfo_PdfSigNoiseSuspcFlgH[MAX_STEP] = SPC_5MSCTRLPEDLTRVLTAGINFO_PDFSIGNOISESUSPCFLGH;
const Rtesint32 UtInput_Spc_5msCtrlPedlTrvlTagInfo_PdfSigTag[MAX_STEP] = SPC_5MSCTRLPEDLTRVLTAGINFO_PDFSIGTAG;
const Rtesint32 UtInput_Spc_5msCtrlPedlTrvlTagInfo_PdtSigNoiseSuspcFlgH[MAX_STEP] = SPC_5MSCTRLPEDLTRVLTAGINFO_PDTSIGNOISESUSPCFLGH;
const Rtesint32 UtInput_Spc_5msCtrlPedlTrvlTagInfo_PdtSigTag[MAX_STEP] = SPC_5MSCTRLPEDLTRVLTAGINFO_PDTSIGTAG;
const Saluint8 UtInput_Spc_5msCtrlEemSuspectData_Eem_Suspect_CirP1[MAX_STEP] = SPC_5MSCTRLEEMSUSPECTDATA_EEM_SUSPECT_CIRP1;
const Saluint8 UtInput_Spc_5msCtrlEemSuspectData_Eem_Suspect_CirP2[MAX_STEP] = SPC_5MSCTRLEEMSUSPECTDATA_EEM_SUSPECT_CIRP2;
const Saluint8 UtInput_Spc_5msCtrlEemSuspectData_Eem_Suspect_SimP[MAX_STEP] = SPC_5MSCTRLEEMSUSPECTDATA_EEM_SUSPECT_SIMP;
const Saluint8 UtInput_Spc_5msCtrlEemSuspectData_Eem_Suspect_BLS[MAX_STEP] = SPC_5MSCTRLEEMSUSPECTDATA_EEM_SUSPECT_BLS;
const Saluint8 UtInput_Spc_5msCtrlEemSuspectData_Eem_Suspect_PedalPDT[MAX_STEP] = SPC_5MSCTRLEEMSUSPECTDATA_EEM_SUSPECT_PEDALPDT;
const Saluint8 UtInput_Spc_5msCtrlEemSuspectData_Eem_Suspect_PedalPDF[MAX_STEP] = SPC_5MSCTRLEEMSUSPECTDATA_EEM_SUSPECT_PEDALPDF;
const Mom_HndlrEcuModeSts_t UtInput_Spc_5msCtrlEcuModeSts[MAX_STEP] = SPC_5MSCTRLECUMODESTS;
const Pct_5msCtrlPCtrlAct_t UtInput_Spc_5msCtrlPCtrlAct[MAX_STEP] = SPC_5MSCTRLPCTRLACT;
const Mcc_1msCtrlMotCtrlState_t UtInput_Spc_5msCtrlMotCtrlState[MAX_STEP] = SPC_5MSCTRLMOTCTRLSTATE;
const Swt_SenBlsSwt_t UtInput_Spc_5msCtrlBlsSwt[MAX_STEP] = SPC_5MSCTRLBLSSWT;
const Press_SenSyncPedlSimPRaw_t UtInput_Spc_5msCtrlPedlSimPRaw[MAX_STEP] = SPC_5MSCTRLPEDLSIMPRAW;
const Abc_CtrlActvBrkCtrlrActFlg_t UtInput_Spc_5msCtrlActvBrkCtrlrActFlg[MAX_STEP] = SPC_5MSCTRLACTVBRKCTRLRACTFLG;
const Det_5msCtrlPedlTrvlFinal_t UtInput_Spc_5msCtrlPedlTrvlFinal[MAX_STEP] = SPC_5MSCTRLPEDLTRVLFINAL;
const Det_5msCtrlVehSpdFild_t UtInput_Spc_5msCtrlVehSpdFild[MAX_STEP] = SPC_5MSCTRLVEHSPDFILD;

const Rtesint32 UtExpected_Spc_5msCtrlCircPFildInfo_PrimCircPFild[MAX_STEP] = SPC_5MSCTRLCIRCPFILDINFO_PRIMCIRCPFILD;
const Rtesint32 UtExpected_Spc_5msCtrlCircPFildInfo_PrimCircPFild_1_100Bar[MAX_STEP] = SPC_5MSCTRLCIRCPFILDINFO_PRIMCIRCPFILD_1_100BAR;
const Rtesint32 UtExpected_Spc_5msCtrlCircPFildInfo_SecdCircPFild[MAX_STEP] = SPC_5MSCTRLCIRCPFILDINFO_SECDCIRCPFILD;
const Rtesint32 UtExpected_Spc_5msCtrlCircPFildInfo_SecdCircPFild_1_100Bar[MAX_STEP] = SPC_5MSCTRLCIRCPFILDINFO_SECDCIRCPFILD_1_100BAR;
const Rtesint32 UtExpected_Spc_5msCtrlCircPOffsCorrdInfo_PrimCircPOffsCorrd[MAX_STEP] = SPC_5MSCTRLCIRCPOFFSCORRDINFO_PRIMCIRCPOFFSCORRD;
const Rtesint32 UtExpected_Spc_5msCtrlCircPOffsCorrdInfo_PrimCircPOffsCorrdStOkFlg[MAX_STEP] = SPC_5MSCTRLCIRCPOFFSCORRDINFO_PRIMCIRCPOFFSCORRDSTOKFLG;
const Rtesint32 UtExpected_Spc_5msCtrlCircPOffsCorrdInfo_SecdCircPOffsCorrd[MAX_STEP] = SPC_5MSCTRLCIRCPOFFSCORRDINFO_SECDCIRCPOFFSCORRD;
const Rtesint32 UtExpected_Spc_5msCtrlCircPOffsCorrdInfo_SecdCircPOffsCorrdStOkFlg[MAX_STEP] = SPC_5MSCTRLCIRCPOFFSCORRDINFO_SECDCIRCPOFFSCORRDSTOKFLG;
const Rtesint32 UtExpected_Spc_5msCtrlPedlSimrPFildInfo_PedlSimrPFild[MAX_STEP] = SPC_5MSCTRLPEDLSIMRPFILDINFO_PEDLSIMRPFILD;
const Rtesint32 UtExpected_Spc_5msCtrlPedlSimrPFildInfo_PedlSimrPFild_1_100Bar[MAX_STEP] = SPC_5MSCTRLPEDLSIMRPFILDINFO_PEDLSIMRPFILD_1_100BAR;
const Rtesint32 UtExpected_Spc_5msCtrlPedlSimrPOffsCorrdInfo_PedlSimrPOffsCorrd[MAX_STEP] = SPC_5MSCTRLPEDLSIMRPOFFSCORRDINFO_PEDLSIMRPOFFSCORRD;
const Rtesint32 UtExpected_Spc_5msCtrlPedlSimrPOffsCorrdInfo_PedlSimrPOffsCorrdStOkFlg[MAX_STEP] = SPC_5MSCTRLPEDLSIMRPOFFSCORRDINFO_PEDLSIMRPOFFSCORRDSTOKFLG;
const Rtesint32 UtExpected_Spc_5msCtrlPedlTrvlFildInfo_PdfFild[MAX_STEP] = SPC_5MSCTRLPEDLTRVLFILDINFO_PDFFILD;
const Rtesint32 UtExpected_Spc_5msCtrlPedlTrvlFildInfo_PdtFild[MAX_STEP] = SPC_5MSCTRLPEDLTRVLFILDINFO_PDTFILD;
const Rtesint32 UtExpected_Spc_5msCtrlPedlTrvlOffsCorrdInfo_PdfOffsCorrdStOkFlg[MAX_STEP] = SPC_5MSCTRLPEDLTRVLOFFSCORRDINFO_PDFOFFSCORRDSTOKFLG;
const Rtesint32 UtExpected_Spc_5msCtrlPedlTrvlOffsCorrdInfo_PdfRawOffsCorrd[MAX_STEP] = SPC_5MSCTRLPEDLTRVLOFFSCORRDINFO_PDFRAWOFFSCORRD;
const Rtesint32 UtExpected_Spc_5msCtrlPedlTrvlOffsCorrdInfo_PdtOffsCorrdStOkFlg[MAX_STEP] = SPC_5MSCTRLPEDLTRVLOFFSCORRDINFO_PDTOFFSCORRDSTOKFLG;
const Rtesint32 UtExpected_Spc_5msCtrlPedlTrvlOffsCorrdInfo_PdtRawOffsCorrd[MAX_STEP] = SPC_5MSCTRLPEDLTRVLOFFSCORRDINFO_PDTRAWOFFSCORRD;
const Rtesint32 UtExpected_Spc_5msCtrlPistPFildInfo_PistPFild[MAX_STEP] = SPC_5MSCTRLPISTPFILDINFO_PISTPFILD;
const Rtesint32 UtExpected_Spc_5msCtrlPistPFildInfo_PistPFild_1_100Bar[MAX_STEP] = SPC_5MSCTRLPISTPFILDINFO_PISTPFILD_1_100BAR;
const Rtesint32 UtExpected_Spc_5msCtrlPistPOffsCorrdInfo_PistPOffsCorrd[MAX_STEP] = SPC_5MSCTRLPISTPOFFSCORRDINFO_PISTPOFFSCORRD;
const Rtesint32 UtExpected_Spc_5msCtrlPistPOffsCorrdInfo_PistPOffsCorrdStOkFlg[MAX_STEP] = SPC_5MSCTRLPISTPOFFSCORRDINFO_PISTPOFFSCORRDSTOKFLG;
const Rtesint32 UtExpected_Spc_5msCtrlWhlSpdFildInfo_WhlSpdFildFrntLe[MAX_STEP] = SPC_5MSCTRLWHLSPDFILDINFO_WHLSPDFILDFRNTLE;
const Rtesint32 UtExpected_Spc_5msCtrlWhlSpdFildInfo_WhlSpdFildFrntRi[MAX_STEP] = SPC_5MSCTRLWHLSPDFILDINFO_WHLSPDFILDFRNTRI;
const Rtesint32 UtExpected_Spc_5msCtrlWhlSpdFildInfo_WhlSpdFildReLe[MAX_STEP] = SPC_5MSCTRLWHLSPDFILDINFO_WHLSPDFILDRELE;
const Rtesint32 UtExpected_Spc_5msCtrlWhlSpdFildInfo_WhlSpdFildReRi[MAX_STEP] = SPC_5MSCTRLWHLSPDFILDINFO_WHLSPDFILDRERI;
const Rteuint8 UtExpected_Spc_5msCtrlIdbSnsrEolOfsCalcInfo_PedalEolOfsCalcOk[MAX_STEP] = SPC_5MSCTRLIDBSNSREOLOFSCALCINFO_PEDALEOLOFSCALCOK;
const Rteuint8 UtExpected_Spc_5msCtrlIdbSnsrEolOfsCalcInfo_PedalEolOfsCalcEnd[MAX_STEP] = SPC_5MSCTRLIDBSNSREOLOFSCALCINFO_PEDALEOLOFSCALCEND;
const Rteuint8 UtExpected_Spc_5msCtrlIdbSnsrEolOfsCalcInfo_PressEolOfsCalcOk[MAX_STEP] = SPC_5MSCTRLIDBSNSREOLOFSCALCINFO_PRESSEOLOFSCALCOK;
const Rteuint8 UtExpected_Spc_5msCtrlIdbSnsrEolOfsCalcInfo_PressEolOfsCalcEnd[MAX_STEP] = SPC_5MSCTRLIDBSNSREOLOFSCALCINFO_PRESSEOLOFSCALCEND;
const Spc_5msCtrlBlsFild_t UtExpected_Spc_5msCtrlBlsFild[MAX_STEP] = SPC_5MSCTRLBLSFILD;
const Spc_5msCtrlFlexBrkSwtFild_t UtExpected_Spc_5msCtrlFlexBrkSwtFild[MAX_STEP] = SPC_5MSCTRLFLEXBRKSWTFILD;



TEST_GROUP(Spc_5msCtrl);
TEST_SETUP(Spc_5msCtrl)
{
    Spc_5msCtrl_Init();
}

TEST_TEAR_DOWN(Spc_5msCtrl)
{   /* Postcondition */

}

TEST(Spc_5msCtrl, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Spc_5msCtrlNormVlvReqVlvActInfo.PrimCutVlvReq = UtInput_Spc_5msCtrlNormVlvReqVlvActInfo_PrimCutVlvReq[i];
        Spc_5msCtrlNormVlvReqVlvActInfo.SecdCutVlvReq = UtInput_Spc_5msCtrlNormVlvReqVlvActInfo_SecdCutVlvReq[i];
        Spc_5msCtrlNormVlvReqVlvActInfo.PrimCircVlvReq = UtInput_Spc_5msCtrlNormVlvReqVlvActInfo_PrimCircVlvReq[i];
        Spc_5msCtrlNormVlvReqVlvActInfo.SecdCircVlvReq = UtInput_Spc_5msCtrlNormVlvReqVlvActInfo_SecdCircVlvReq[i];
        Spc_5msCtrlNormVlvReqVlvActInfo.SimVlvReq = UtInput_Spc_5msCtrlNormVlvReqVlvActInfo_SimVlvReq[i];
        Spc_5msCtrlEemFailData.Eem_Fail_CirP1 = UtInput_Spc_5msCtrlEemFailData_Eem_Fail_CirP1[i];
        Spc_5msCtrlEemFailData.Eem_Fail_CirP2 = UtInput_Spc_5msCtrlEemFailData_Eem_Fail_CirP2[i];
        Spc_5msCtrlEemFailData.Eem_Fail_SimP = UtInput_Spc_5msCtrlEemFailData_Eem_Fail_SimP[i];
        Spc_5msCtrlEemFailData.Eem_Fail_BLS = UtInput_Spc_5msCtrlEemFailData_Eem_Fail_BLS[i];
        Spc_5msCtrlEemFailData.Eem_Fail_PedalPDT = UtInput_Spc_5msCtrlEemFailData_Eem_Fail_PedalPDT[i];
        Spc_5msCtrlEemFailData.Eem_Fail_PedalPDF = UtInput_Spc_5msCtrlEemFailData_Eem_Fail_PedalPDF[i];
        Spc_5msCtrlCanRxAccelPedlInfo.AccelPedlVal = UtInput_Spc_5msCtrlCanRxAccelPedlInfo_AccelPedlVal[i];
        Spc_5msCtrlCanRxAccelPedlInfo.AccelPedlValErr = UtInput_Spc_5msCtrlCanRxAccelPedlInfo_AccelPedlValErr[i];
        Spc_5msCtrlPdt5msRawInfo.PdtSig = UtInput_Spc_5msCtrlPdt5msRawInfo_PdtSig[i];
        Spc_5msCtrlPdf5msRawInfo.PdfSig = UtInput_Spc_5msCtrlPdf5msRawInfo_PdfSig[i];
        Spc_5msCtrlSwtStsFlexBrkInfo.FlexBrkASwt = UtInput_Spc_5msCtrlSwtStsFlexBrkInfo_FlexBrkASwt[i];
        Spc_5msCtrlSwtStsFlexBrkInfo.FlexBrkBSwt = UtInput_Spc_5msCtrlSwtStsFlexBrkInfo_FlexBrkBSwt[i];
        Spc_5msCtrlWhlSpdInfo.FlWhlSpd = UtInput_Spc_5msCtrlWhlSpdInfo_FlWhlSpd[i];
        Spc_5msCtrlWhlSpdInfo.FrWhlSpd = UtInput_Spc_5msCtrlWhlSpdInfo_FrWhlSpd[i];
        Spc_5msCtrlWhlSpdInfo.RlWhlSpd = UtInput_Spc_5msCtrlWhlSpdInfo_RlWhlSpd[i];
        Spc_5msCtrlWhlSpdInfo.RrlWhlSpd = UtInput_Spc_5msCtrlWhlSpdInfo_RrlWhlSpd[i];
        Spc_5msCtrlCircP5msRawInfo.PrimCircPSig = UtInput_Spc_5msCtrlCircP5msRawInfo_PrimCircPSig[i];
        Spc_5msCtrlCircP5msRawInfo.SecdCircPSig = UtInput_Spc_5msCtrlCircP5msRawInfo_SecdCircPSig[i];
        Spc_5msCtrlPistP5msRawInfo.PistPSig = UtInput_Spc_5msCtrlPistP5msRawInfo_PistPSig[i];
        Spc_5msCtrlAbsCtrlInfo.AbsActFlg = UtInput_Spc_5msCtrlAbsCtrlInfo_AbsActFlg[i];
        Spc_5msCtrlAbsCtrlInfo.AbsDefectFlg = UtInput_Spc_5msCtrlAbsCtrlInfo_AbsDefectFlg[i];
        Spc_5msCtrlAbsCtrlInfo.AbsTarPFrntLe = UtInput_Spc_5msCtrlAbsCtrlInfo_AbsTarPFrntLe[i];
        Spc_5msCtrlAbsCtrlInfo.AbsTarPFrntRi = UtInput_Spc_5msCtrlAbsCtrlInfo_AbsTarPFrntRi[i];
        Spc_5msCtrlAbsCtrlInfo.AbsTarPReLe = UtInput_Spc_5msCtrlAbsCtrlInfo_AbsTarPReLe[i];
        Spc_5msCtrlAbsCtrlInfo.AbsTarPReRi = UtInput_Spc_5msCtrlAbsCtrlInfo_AbsTarPReRi[i];
        Spc_5msCtrlAbsCtrlInfo.FrntWhlSlip = UtInput_Spc_5msCtrlAbsCtrlInfo_FrntWhlSlip[i];
        Spc_5msCtrlAbsCtrlInfo.AbsDesTarP = UtInput_Spc_5msCtrlAbsCtrlInfo_AbsDesTarP[i];
        Spc_5msCtrlAbsCtrlInfo.AbsDesTarPReqFlg = UtInput_Spc_5msCtrlAbsCtrlInfo_AbsDesTarPReqFlg[i];
        Spc_5msCtrlAbsCtrlInfo.AbsCtrlFadeOutFlg = UtInput_Spc_5msCtrlAbsCtrlInfo_AbsCtrlFadeOutFlg[i];
        Spc_5msCtrlEscCtrlInfo.EscActFlg = UtInput_Spc_5msCtrlEscCtrlInfo_EscActFlg[i];
        Spc_5msCtrlEscCtrlInfo.EscDefectFlg = UtInput_Spc_5msCtrlEscCtrlInfo_EscDefectFlg[i];
        Spc_5msCtrlEscCtrlInfo.EscTarPFrntLe = UtInput_Spc_5msCtrlEscCtrlInfo_EscTarPFrntLe[i];
        Spc_5msCtrlEscCtrlInfo.EscTarPFrntRi = UtInput_Spc_5msCtrlEscCtrlInfo_EscTarPFrntRi[i];
        Spc_5msCtrlEscCtrlInfo.EscTarPReLe = UtInput_Spc_5msCtrlEscCtrlInfo_EscTarPReLe[i];
        Spc_5msCtrlEscCtrlInfo.EscTarPReRi = UtInput_Spc_5msCtrlEscCtrlInfo_EscTarPReRi[i];
        Spc_5msCtrlSwtStsFSInfo.FlexBrkSwtFaultDet = UtInput_Spc_5msCtrlSwtStsFSInfo_FlexBrkSwtFaultDet[i];
        Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg = UtInput_Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo_PedlTrvlOffsEolCalcReqFlg[i];
        Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg = UtInput_Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo_PSnsrOffsEolCalcReqFlg[i];
        Spc_5msCtrlPedlTrvlTagInfo.PdfSigNoiseSuspcFlgH = UtInput_Spc_5msCtrlPedlTrvlTagInfo_PdfSigNoiseSuspcFlgH[i];
        Spc_5msCtrlPedlTrvlTagInfo.PdfSigTag = UtInput_Spc_5msCtrlPedlTrvlTagInfo_PdfSigTag[i];
        Spc_5msCtrlPedlTrvlTagInfo.PdtSigNoiseSuspcFlgH = UtInput_Spc_5msCtrlPedlTrvlTagInfo_PdtSigNoiseSuspcFlgH[i];
        Spc_5msCtrlPedlTrvlTagInfo.PdtSigTag = UtInput_Spc_5msCtrlPedlTrvlTagInfo_PdtSigTag[i];
        Spc_5msCtrlEemSuspectData.Eem_Suspect_CirP1 = UtInput_Spc_5msCtrlEemSuspectData_Eem_Suspect_CirP1[i];
        Spc_5msCtrlEemSuspectData.Eem_Suspect_CirP2 = UtInput_Spc_5msCtrlEemSuspectData_Eem_Suspect_CirP2[i];
        Spc_5msCtrlEemSuspectData.Eem_Suspect_SimP = UtInput_Spc_5msCtrlEemSuspectData_Eem_Suspect_SimP[i];
        Spc_5msCtrlEemSuspectData.Eem_Suspect_BLS = UtInput_Spc_5msCtrlEemSuspectData_Eem_Suspect_BLS[i];
        Spc_5msCtrlEemSuspectData.Eem_Suspect_PedalPDT = UtInput_Spc_5msCtrlEemSuspectData_Eem_Suspect_PedalPDT[i];
        Spc_5msCtrlEemSuspectData.Eem_Suspect_PedalPDF = UtInput_Spc_5msCtrlEemSuspectData_Eem_Suspect_PedalPDF[i];
        Spc_5msCtrlEcuModeSts = UtInput_Spc_5msCtrlEcuModeSts[i];
        Spc_5msCtrlPCtrlAct = UtInput_Spc_5msCtrlPCtrlAct[i];
        Spc_5msCtrlMotCtrlState = UtInput_Spc_5msCtrlMotCtrlState[i];
        Spc_5msCtrlBlsSwt = UtInput_Spc_5msCtrlBlsSwt[i];
        Spc_5msCtrlPedlSimPRaw = UtInput_Spc_5msCtrlPedlSimPRaw[i];
        Spc_5msCtrlActvBrkCtrlrActFlg = UtInput_Spc_5msCtrlActvBrkCtrlrActFlg[i];
        Spc_5msCtrlPedlTrvlFinal = UtInput_Spc_5msCtrlPedlTrvlFinal[i];
        Spc_5msCtrlVehSpdFild = UtInput_Spc_5msCtrlVehSpdFild[i];

        Spc_5msCtrl();

        TEST_ASSERT_EQUAL(Spc_5msCtrlCircPFildInfo.PrimCircPFild, UtExpected_Spc_5msCtrlCircPFildInfo_PrimCircPFild[i]);
        TEST_ASSERT_EQUAL(Spc_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar, UtExpected_Spc_5msCtrlCircPFildInfo_PrimCircPFild_1_100Bar[i]);
        TEST_ASSERT_EQUAL(Spc_5msCtrlCircPFildInfo.SecdCircPFild, UtExpected_Spc_5msCtrlCircPFildInfo_SecdCircPFild[i]);
        TEST_ASSERT_EQUAL(Spc_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar, UtExpected_Spc_5msCtrlCircPFildInfo_SecdCircPFild_1_100Bar[i]);
        TEST_ASSERT_EQUAL(Spc_5msCtrlCircPOffsCorrdInfo.PrimCircPOffsCorrd, UtExpected_Spc_5msCtrlCircPOffsCorrdInfo_PrimCircPOffsCorrd[i]);
        TEST_ASSERT_EQUAL(Spc_5msCtrlCircPOffsCorrdInfo.PrimCircPOffsCorrdStOkFlg, UtExpected_Spc_5msCtrlCircPOffsCorrdInfo_PrimCircPOffsCorrdStOkFlg[i]);
        TEST_ASSERT_EQUAL(Spc_5msCtrlCircPOffsCorrdInfo.SecdCircPOffsCorrd, UtExpected_Spc_5msCtrlCircPOffsCorrdInfo_SecdCircPOffsCorrd[i]);
        TEST_ASSERT_EQUAL(Spc_5msCtrlCircPOffsCorrdInfo.SecdCircPOffsCorrdStOkFlg, UtExpected_Spc_5msCtrlCircPOffsCorrdInfo_SecdCircPOffsCorrdStOkFlg[i]);
        TEST_ASSERT_EQUAL(Spc_5msCtrlPedlSimrPFildInfo.PedlSimrPFild, UtExpected_Spc_5msCtrlPedlSimrPFildInfo_PedlSimrPFild[i]);
        TEST_ASSERT_EQUAL(Spc_5msCtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar, UtExpected_Spc_5msCtrlPedlSimrPFildInfo_PedlSimrPFild_1_100Bar[i]);
        TEST_ASSERT_EQUAL(Spc_5msCtrlPedlSimrPOffsCorrdInfo.PedlSimrPOffsCorrd, UtExpected_Spc_5msCtrlPedlSimrPOffsCorrdInfo_PedlSimrPOffsCorrd[i]);
        TEST_ASSERT_EQUAL(Spc_5msCtrlPedlSimrPOffsCorrdInfo.PedlSimrPOffsCorrdStOkFlg, UtExpected_Spc_5msCtrlPedlSimrPOffsCorrdInfo_PedlSimrPOffsCorrdStOkFlg[i]);
        TEST_ASSERT_EQUAL(Spc_5msCtrlPedlTrvlFildInfo.PdfFild, UtExpected_Spc_5msCtrlPedlTrvlFildInfo_PdfFild[i]);
        TEST_ASSERT_EQUAL(Spc_5msCtrlPedlTrvlFildInfo.PdtFild, UtExpected_Spc_5msCtrlPedlTrvlFildInfo_PdtFild[i]);
        TEST_ASSERT_EQUAL(Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdfOffsCorrdStOkFlg, UtExpected_Spc_5msCtrlPedlTrvlOffsCorrdInfo_PdfOffsCorrdStOkFlg[i]);
        TEST_ASSERT_EQUAL(Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdfRawOffsCorrd, UtExpected_Spc_5msCtrlPedlTrvlOffsCorrdInfo_PdfRawOffsCorrd[i]);
        TEST_ASSERT_EQUAL(Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdtOffsCorrdStOkFlg, UtExpected_Spc_5msCtrlPedlTrvlOffsCorrdInfo_PdtOffsCorrdStOkFlg[i]);
        TEST_ASSERT_EQUAL(Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdtRawOffsCorrd, UtExpected_Spc_5msCtrlPedlTrvlOffsCorrdInfo_PdtRawOffsCorrd[i]);
        TEST_ASSERT_EQUAL(Spc_5msCtrlPistPFildInfo.PistPFild, UtExpected_Spc_5msCtrlPistPFildInfo_PistPFild[i]);
        TEST_ASSERT_EQUAL(Spc_5msCtrlPistPFildInfo.PistPFild_1_100Bar, UtExpected_Spc_5msCtrlPistPFildInfo_PistPFild_1_100Bar[i]);
        TEST_ASSERT_EQUAL(Spc_5msCtrlPistPOffsCorrdInfo.PistPOffsCorrd, UtExpected_Spc_5msCtrlPistPOffsCorrdInfo_PistPOffsCorrd[i]);
        TEST_ASSERT_EQUAL(Spc_5msCtrlPistPOffsCorrdInfo.PistPOffsCorrdStOkFlg, UtExpected_Spc_5msCtrlPistPOffsCorrdInfo_PistPOffsCorrdStOkFlg[i]);
        TEST_ASSERT_EQUAL(Spc_5msCtrlWhlSpdFildInfo.WhlSpdFildFrntLe, UtExpected_Spc_5msCtrlWhlSpdFildInfo_WhlSpdFildFrntLe[i]);
        TEST_ASSERT_EQUAL(Spc_5msCtrlWhlSpdFildInfo.WhlSpdFildFrntRi, UtExpected_Spc_5msCtrlWhlSpdFildInfo_WhlSpdFildFrntRi[i]);
        TEST_ASSERT_EQUAL(Spc_5msCtrlWhlSpdFildInfo.WhlSpdFildReLe, UtExpected_Spc_5msCtrlWhlSpdFildInfo_WhlSpdFildReLe[i]);
        TEST_ASSERT_EQUAL(Spc_5msCtrlWhlSpdFildInfo.WhlSpdFildReRi, UtExpected_Spc_5msCtrlWhlSpdFildInfo_WhlSpdFildReRi[i]);
        TEST_ASSERT_EQUAL(Spc_5msCtrlIdbSnsrEolOfsCalcInfo.PedalEolOfsCalcOk, UtExpected_Spc_5msCtrlIdbSnsrEolOfsCalcInfo_PedalEolOfsCalcOk[i]);
        TEST_ASSERT_EQUAL(Spc_5msCtrlIdbSnsrEolOfsCalcInfo.PedalEolOfsCalcEnd, UtExpected_Spc_5msCtrlIdbSnsrEolOfsCalcInfo_PedalEolOfsCalcEnd[i]);
        TEST_ASSERT_EQUAL(Spc_5msCtrlIdbSnsrEolOfsCalcInfo.PressEolOfsCalcOk, UtExpected_Spc_5msCtrlIdbSnsrEolOfsCalcInfo_PressEolOfsCalcOk[i]);
        TEST_ASSERT_EQUAL(Spc_5msCtrlIdbSnsrEolOfsCalcInfo.PressEolOfsCalcEnd, UtExpected_Spc_5msCtrlIdbSnsrEolOfsCalcInfo_PressEolOfsCalcEnd[i]);
        TEST_ASSERT_EQUAL(Spc_5msCtrlBlsFild, UtExpected_Spc_5msCtrlBlsFild[i]);
        TEST_ASSERT_EQUAL(Spc_5msCtrlFlexBrkSwtFild, UtExpected_Spc_5msCtrlFlexBrkSwtFild[i]);
    }
}

TEST_GROUP_RUNNER(Spc_5msCtrl)
{
    RUN_TEST_CASE(Spc_5msCtrl, All);
}
