/**
 * @defgroup Spc_Cfg Spc_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Spc_Cfg.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Spc_Cfg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SPC_START_SEC_CONST_UNSPECIFIED
#include "Spc_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define SPC_STOP_SEC_CONST_UNSPECIFIED
#include "Spc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SPC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SPC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spc_MemMap.h"
#define SPC_START_SEC_VAR_NOINIT_32BIT
#include "Spc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SPC_STOP_SEC_VAR_NOINIT_32BIT
#include "Spc_MemMap.h"
#define SPC_START_SEC_VAR_UNSPECIFIED
#include "Spc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SPC_STOP_SEC_VAR_UNSPECIFIED
#include "Spc_MemMap.h"
#define SPC_START_SEC_VAR_32BIT
#include "Spc_MemMap.h"
/** Variable Section (32BIT)**/


#define SPC_STOP_SEC_VAR_32BIT
#include "Spc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SPC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SPC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Spc_MemMap.h"
#define SPC_START_SEC_VAR_NOINIT_32BIT
#include "Spc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SPC_STOP_SEC_VAR_NOINIT_32BIT
#include "Spc_MemMap.h"
#define SPC_START_SEC_VAR_UNSPECIFIED
#include "Spc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SPC_STOP_SEC_VAR_UNSPECIFIED
#include "Spc_MemMap.h"
#define SPC_START_SEC_VAR_32BIT
#include "Spc_MemMap.h"
/** Variable Section (32BIT)**/


#define SPC_STOP_SEC_VAR_32BIT
#include "Spc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SPC_START_SEC_CODE
#include "Spc_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define SPC_STOP_SEC_CODE
#include "Spc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
