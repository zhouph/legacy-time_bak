Spc_1msCtrlCircP1msRawInfo = Simulink.Bus;
DeList={
    'PrimCircPSig'
    'SecdCircPSig'
    };
Spc_1msCtrlCircP1msRawInfo = CreateBus(Spc_1msCtrlCircP1msRawInfo, DeList);
clear DeList;

Spc_1msCtrlPistP1msRawInfo = Simulink.Bus;
DeList={
    'PistPSig'
    };
Spc_1msCtrlPistP1msRawInfo = CreateBus(Spc_1msCtrlPistP1msRawInfo, DeList);
clear DeList;

Spc_1msCtrlPdt1msRawInfo = Simulink.Bus;
DeList={
    'PdtSig'
    };
Spc_1msCtrlPdt1msRawInfo = CreateBus(Spc_1msCtrlPdt1msRawInfo, DeList);
clear DeList;

Spc_1msCtrlEcuModeSts = Simulink.Bus;
DeList={'Spc_1msCtrlEcuModeSts'};
Spc_1msCtrlEcuModeSts = CreateBus(Spc_1msCtrlEcuModeSts, DeList);
clear DeList;

Spc_1msCtrlCircPFild1msInfo = Simulink.Bus;
DeList={
    'PrimCircPFild1ms'
    'SecdCircPFild1ms'
    };
Spc_1msCtrlCircPFild1msInfo = CreateBus(Spc_1msCtrlCircPFild1msInfo, DeList);
clear DeList;

Spc_1msCtrlPedlTrvlFild1msInfo = Simulink.Bus;
DeList={
    'PdtFild1ms'
    };
Spc_1msCtrlPedlTrvlFild1msInfo = CreateBus(Spc_1msCtrlPedlTrvlFild1msInfo, DeList);
clear DeList;

Spc_1msCtrlPistPFild1msInfo = Simulink.Bus;
DeList={
    'PistPFild1ms'
    };
Spc_1msCtrlPistPFild1msInfo = CreateBus(Spc_1msCtrlPistPFild1msInfo, DeList);
clear DeList;

