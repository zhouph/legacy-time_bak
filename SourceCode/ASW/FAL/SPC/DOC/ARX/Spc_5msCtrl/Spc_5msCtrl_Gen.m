Spc_5msCtrlNormVlvReqVlvActInfo = Simulink.Bus;
DeList={
    'PrimCutVlvReq'
    'SecdCutVlvReq'
    'PrimCircVlvReq'
    'SecdCircVlvReq'
    'SimVlvReq'
    };
Spc_5msCtrlNormVlvReqVlvActInfo = CreateBus(Spc_5msCtrlNormVlvReqVlvActInfo, DeList);
clear DeList;

Spc_5msCtrlEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_CirP1'
    'Eem_Fail_CirP2'
    'Eem_Fail_SimP'
    'Eem_Fail_BLS'
    'Eem_Fail_PedalPDT'
    'Eem_Fail_PedalPDF'
    };
Spc_5msCtrlEemFailData = CreateBus(Spc_5msCtrlEemFailData, DeList);
clear DeList;

Spc_5msCtrlCanRxAccelPedlInfo = Simulink.Bus;
DeList={
    'AccelPedlVal'
    'AccelPedlValErr'
    };
Spc_5msCtrlCanRxAccelPedlInfo = CreateBus(Spc_5msCtrlCanRxAccelPedlInfo, DeList);
clear DeList;

Spc_5msCtrlPdt5msRawInfo = Simulink.Bus;
DeList={
    'PdtSig'
    };
Spc_5msCtrlPdt5msRawInfo = CreateBus(Spc_5msCtrlPdt5msRawInfo, DeList);
clear DeList;

Spc_5msCtrlPdf5msRawInfo = Simulink.Bus;
DeList={
    'PdfSig'
    };
Spc_5msCtrlPdf5msRawInfo = CreateBus(Spc_5msCtrlPdf5msRawInfo, DeList);
clear DeList;

Spc_5msCtrlSwtStsFlexBrkInfo = Simulink.Bus;
DeList={
    'FlexBrkASwt'
    'FlexBrkBSwt'
    };
Spc_5msCtrlSwtStsFlexBrkInfo = CreateBus(Spc_5msCtrlSwtStsFlexBrkInfo, DeList);
clear DeList;

Spc_5msCtrlWhlSpdInfo = Simulink.Bus;
DeList={
    'FlWhlSpd'
    'FrWhlSpd'
    'RlWhlSpd'
    'RrlWhlSpd'
    };
Spc_5msCtrlWhlSpdInfo = CreateBus(Spc_5msCtrlWhlSpdInfo, DeList);
clear DeList;

Spc_5msCtrlCircP5msRawInfo = Simulink.Bus;
DeList={
    'PrimCircPSig'
    'SecdCircPSig'
    };
Spc_5msCtrlCircP5msRawInfo = CreateBus(Spc_5msCtrlCircP5msRawInfo, DeList);
clear DeList;

Spc_5msCtrlPistP5msRawInfo = Simulink.Bus;
DeList={
    'PistPSig'
    };
Spc_5msCtrlPistP5msRawInfo = CreateBus(Spc_5msCtrlPistP5msRawInfo, DeList);
clear DeList;

Spc_5msCtrlAbsCtrlInfo = Simulink.Bus;
DeList={
    'AbsActFlg'
    'AbsDefectFlg'
    'AbsTarPFrntLe'
    'AbsTarPFrntRi'
    'AbsTarPReLe'
    'AbsTarPReRi'
    'FrntWhlSlip'
    'AbsDesTarP'
    'AbsDesTarPReqFlg'
    'AbsCtrlFadeOutFlg'
    };
Spc_5msCtrlAbsCtrlInfo = CreateBus(Spc_5msCtrlAbsCtrlInfo, DeList);
clear DeList;

Spc_5msCtrlEscCtrlInfo = Simulink.Bus;
DeList={
    'EscActFlg'
    'EscDefectFlg'
    'EscTarPFrntLe'
    'EscTarPFrntRi'
    'EscTarPReLe'
    'EscTarPReRi'
    };
Spc_5msCtrlEscCtrlInfo = CreateBus(Spc_5msCtrlEscCtrlInfo, DeList);
clear DeList;

Spc_5msCtrlSwtStsFSInfo = Simulink.Bus;
DeList={
    'FlexBrkSwtFaultDet'
    };
Spc_5msCtrlSwtStsFSInfo = CreateBus(Spc_5msCtrlSwtStsFSInfo, DeList);
clear DeList;

Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo = Simulink.Bus;
DeList={
    'PedlTrvlOffsEolCalcReqFlg'
    'PSnsrOffsEolCalcReqFlg'
    };
Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo = CreateBus(Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo, DeList);
clear DeList;

Spc_5msCtrlPedlTrvlTagInfo = Simulink.Bus;
DeList={
    'PdfSigNoiseSuspcFlgH'
    'PdfSigTag'
    'PdtSigNoiseSuspcFlgH'
    'PdtSigTag'
    };
Spc_5msCtrlPedlTrvlTagInfo = CreateBus(Spc_5msCtrlPedlTrvlTagInfo, DeList);
clear DeList;

Spc_5msCtrlEemSuspectData = Simulink.Bus;
DeList={
    'Eem_Suspect_CirP1'
    'Eem_Suspect_CirP2'
    'Eem_Suspect_SimP'
    'Eem_Suspect_BLS'
    'Eem_Suspect_PedalPDT'
    'Eem_Suspect_PedalPDF'
    };
Spc_5msCtrlEemSuspectData = CreateBus(Spc_5msCtrlEemSuspectData, DeList);
clear DeList;

Spc_5msCtrlEcuModeSts = Simulink.Bus;
DeList={'Spc_5msCtrlEcuModeSts'};
Spc_5msCtrlEcuModeSts = CreateBus(Spc_5msCtrlEcuModeSts, DeList);
clear DeList;

Spc_5msCtrlPCtrlAct = Simulink.Bus;
DeList={'Spc_5msCtrlPCtrlAct'};
Spc_5msCtrlPCtrlAct = CreateBus(Spc_5msCtrlPCtrlAct, DeList);
clear DeList;

Spc_5msCtrlMotCtrlState = Simulink.Bus;
DeList={'Spc_5msCtrlMotCtrlState'};
Spc_5msCtrlMotCtrlState = CreateBus(Spc_5msCtrlMotCtrlState, DeList);
clear DeList;

Spc_5msCtrlBlsSwt = Simulink.Bus;
DeList={'Spc_5msCtrlBlsSwt'};
Spc_5msCtrlBlsSwt = CreateBus(Spc_5msCtrlBlsSwt, DeList);
clear DeList;

Spc_5msCtrlPedlSimPRaw = Simulink.Bus;
DeList={'Spc_5msCtrlPedlSimPRaw'};
Spc_5msCtrlPedlSimPRaw = CreateBus(Spc_5msCtrlPedlSimPRaw, DeList);
clear DeList;

Spc_5msCtrlActvBrkCtrlrActFlg = Simulink.Bus;
DeList={'Spc_5msCtrlActvBrkCtrlrActFlg'};
Spc_5msCtrlActvBrkCtrlrActFlg = CreateBus(Spc_5msCtrlActvBrkCtrlrActFlg, DeList);
clear DeList;

Spc_5msCtrlPedlTrvlFinal = Simulink.Bus;
DeList={'Spc_5msCtrlPedlTrvlFinal'};
Spc_5msCtrlPedlTrvlFinal = CreateBus(Spc_5msCtrlPedlTrvlFinal, DeList);
clear DeList;

Spc_5msCtrlVehSpdFild = Simulink.Bus;
DeList={'Spc_5msCtrlVehSpdFild'};
Spc_5msCtrlVehSpdFild = CreateBus(Spc_5msCtrlVehSpdFild, DeList);
clear DeList;

Spc_5msCtrlCircPFildInfo = Simulink.Bus;
DeList={
    'PrimCircPFild'
    'PrimCircPFild_1_100Bar'
    'SecdCircPFild'
    'SecdCircPFild_1_100Bar'
    };
Spc_5msCtrlCircPFildInfo = CreateBus(Spc_5msCtrlCircPFildInfo, DeList);
clear DeList;

Spc_5msCtrlCircPOffsCorrdInfo = Simulink.Bus;
DeList={
    'PrimCircPOffsCorrd'
    'PrimCircPOffsCorrdStOkFlg'
    'SecdCircPOffsCorrd'
    'SecdCircPOffsCorrdStOkFlg'
    };
Spc_5msCtrlCircPOffsCorrdInfo = CreateBus(Spc_5msCtrlCircPOffsCorrdInfo, DeList);
clear DeList;

Spc_5msCtrlPedlSimrPFildInfo = Simulink.Bus;
DeList={
    'PedlSimrPFild'
    'PedlSimrPFild_1_100Bar'
    };
Spc_5msCtrlPedlSimrPFildInfo = CreateBus(Spc_5msCtrlPedlSimrPFildInfo, DeList);
clear DeList;

Spc_5msCtrlPedlSimrPOffsCorrdInfo = Simulink.Bus;
DeList={
    'PedlSimrPOffsCorrd'
    'PedlSimrPOffsCorrdStOkFlg'
    };
Spc_5msCtrlPedlSimrPOffsCorrdInfo = CreateBus(Spc_5msCtrlPedlSimrPOffsCorrdInfo, DeList);
clear DeList;

Spc_5msCtrlPedlTrvlFildInfo = Simulink.Bus;
DeList={
    'PdfFild'
    'PdtFild'
    };
Spc_5msCtrlPedlTrvlFildInfo = CreateBus(Spc_5msCtrlPedlTrvlFildInfo, DeList);
clear DeList;

Spc_5msCtrlPedlTrvlOffsCorrdInfo = Simulink.Bus;
DeList={
    'PdfOffsCorrdStOkFlg'
    'PdfRawOffsCorrd'
    'PdtOffsCorrdStOkFlg'
    'PdtRawOffsCorrd'
    };
Spc_5msCtrlPedlTrvlOffsCorrdInfo = CreateBus(Spc_5msCtrlPedlTrvlOffsCorrdInfo, DeList);
clear DeList;

Spc_5msCtrlPistPFildInfo = Simulink.Bus;
DeList={
    'PistPFild'
    'PistPFild_1_100Bar'
    };
Spc_5msCtrlPistPFildInfo = CreateBus(Spc_5msCtrlPistPFildInfo, DeList);
clear DeList;

Spc_5msCtrlPistPOffsCorrdInfo = Simulink.Bus;
DeList={
    'PistPOffsCorrd'
    'PistPOffsCorrdStOkFlg'
    };
Spc_5msCtrlPistPOffsCorrdInfo = CreateBus(Spc_5msCtrlPistPOffsCorrdInfo, DeList);
clear DeList;

Spc_5msCtrlWhlSpdFildInfo = Simulink.Bus;
DeList={
    'WhlSpdFildFrntLe'
    'WhlSpdFildFrntRi'
    'WhlSpdFildReLe'
    'WhlSpdFildReRi'
    };
Spc_5msCtrlWhlSpdFildInfo = CreateBus(Spc_5msCtrlWhlSpdFildInfo, DeList);
clear DeList;

Spc_5msCtrlIdbSnsrEolOfsCalcInfo = Simulink.Bus;
DeList={
    'PedalEolOfsCalcOk'
    'PedalEolOfsCalcEnd'
    'PressEolOfsCalcOk'
    'PressEolOfsCalcEnd'
    };
Spc_5msCtrlIdbSnsrEolOfsCalcInfo = CreateBus(Spc_5msCtrlIdbSnsrEolOfsCalcInfo, DeList);
clear DeList;

Spc_5msCtrlBlsFild = Simulink.Bus;
DeList={'Spc_5msCtrlBlsFild'};
Spc_5msCtrlBlsFild = CreateBus(Spc_5msCtrlBlsFild, DeList);
clear DeList;

Spc_5msCtrlFlexBrkSwtFild = Simulink.Bus;
DeList={'Spc_5msCtrlFlexBrkSwtFild'};
Spc_5msCtrlFlexBrkSwtFild = CreateBus(Spc_5msCtrlFlexBrkSwtFild, DeList);
clear DeList;

