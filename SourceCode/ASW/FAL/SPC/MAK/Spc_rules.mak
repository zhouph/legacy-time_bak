# \file
#
# \brief Spc
#
# This file contains the implementation of the SWC
# module Spc.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Spc_src

Spc_src_FILES        += $(Spc_SRC_PATH)\Spc_1msCtrl.c
Spc_src_FILES        += $(Spc_IFA_PATH)\Spc_1msCtrl_Ifa.c
Spc_src_FILES        += $(Spc_SRC_PATH)\Spc_5msCtrl.c
Spc_src_FILES        += $(Spc_IFA_PATH)\Spc_5msCtrl_Ifa.c
Spc_src_FILES        += $(Spc_SRC_PATH)\LSIDB_CalcSensorSignalOffset.c
Spc_src_FILES        += $(Spc_SRC_PATH)\LSIDB_FilterSensorSignal.c
Spc_src_FILES        += $(Spc_SRC_PATH)\LSIDB_FilterSensorSignal1ms.c
Spc_src_FILES        += $(Spc_CFG_PATH)\Spc_Cfg.c
Spc_src_FILES        += $(Spc_CAL_PATH)\Spc_Cal.c
# /* HSH */
ifeq ($(ICE_COMPILE),true)
Spc_src_FILES        += $(Spc_CORE_PATH)\virtual_main.c
# Library
Spc_src_FILES  += $(Spc_CORE_PATH)\ICE\CMN\LIB\SwcCommonFunction.c
Spc_src_FILES  += $(Spc_CORE_PATH)\ICE\CMN\LIB\L_CommonFunction.c
Spc_src_FILES  += $(Spc_CORE_PATH)\ICE\CMN\LIB\L_CommonFunction_1ms.c
Spc_src_FILES  += $(Spc_CORE_PATH)\ICE\CMN\FBL_HDR\FBL_HMC\Fbl_Header.c
Spc_src_FILES  += $(Spc_CORE_PATH)\ICE\CMN\FBL_HDR\FBL_HMC\Fbl_Mtab.c
endif

ifeq ($(UNIT_TEST),true)
	Spc_src_FILES        += $(Spc_UNITY_PATH)\unity.c
	Spc_src_FILES        += $(Spc_UNITY_PATH)\unity_fixture.c	
	Spc_src_FILES        += $(Spc_UT_PATH)\main.c
	Spc_src_FILES        += $(Spc_UT_PATH)\Spc_1msCtrl\Spc_1msCtrl_UtMain.c
	Spc_src_FILES        += $(Spc_UT_PATH)\Spc_5msCtrl\Spc_5msCtrl_UtMain.c
endif