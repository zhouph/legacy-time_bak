# \file
#
# \brief Spc
#
# This file contains the implementation of the SWC
# module Spc.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Spc_CORE_PATH     := $(MANDO_FAL_ROOT)\Spc
Spc_CAL_PATH      := $(Spc_CORE_PATH)\CAL\$(Spc_VARIANT)
Spc_SRC_PATH      := $(Spc_CORE_PATH)\SRC
Spc_CFG_PATH      := $(Spc_CORE_PATH)\CFG\$(Spc_VARIANT)
Spc_HDR_PATH      := $(Spc_CORE_PATH)\HDR
Spc_IFA_PATH      := $(Spc_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Spc_CMN_PATH      := $(Spc_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	Spc_UT_PATH		:= $(Spc_CORE_PATH)\UT
	Spc_UNITY_PATH	:= $(Spc_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(Spc_UT_PATH)
	CC_INCLUDE_PATH		+= $(Spc_UNITY_PATH)
	Spc_1msCtrl_PATH 	:= Spc_UT_PATH\Spc_1msCtrl
	Spc_5msCtrl_PATH 	:= Spc_UT_PATH\Spc_5msCtrl
endif
CC_INCLUDE_PATH    += $(Spc_CAL_PATH)
CC_INCLUDE_PATH    += $(Spc_SRC_PATH)
CC_INCLUDE_PATH    += $(Spc_CFG_PATH)
CC_INCLUDE_PATH    += $(Spc_HDR_PATH)
CC_INCLUDE_PATH    += $(Spc_IFA_PATH)
CC_INCLUDE_PATH    += $(Spc_CMN_PATH)

# /* HSH */

ifeq ($(ICE_COMPILE), true)

## ASW include
CC_INCLUDE_PATH    += $(Spc_CORE_PATH)\ICE\CMN\include

## Library INCLUDE
CC_INCLUDE_PATH    += $(Spc_CORE_PATH)\ICE\CMN\LIB

## VIL INCLUDE
CC_INCLUDE_PATH    += $(Spc_CORE_PATH)\ICE\CMN\VIL
CC_INCLUDE_PATH    += $(Spc_CORE_PATH)\ICE\CMN\VIL\_PAR
CC_INCLUDE_PATH    += $(Spc_CORE_PATH)\ICE\CMN\VIL\_PAR\car
CC_INCLUDE_PATH    += $(Spc_CORE_PATH)\ICE\CMN\VIL\_PAR\version
CC_INCLUDE_PATH    += $(Spc_CORE_PATH)\ICE\CMN\VIL\APPL
CC_INCLUDE_PATH    += $(Spc_CORE_PATH)\ICE\CMN\VIL\APPL\BH\CAL
CC_INCLUDE_PATH    += $(Spc_CORE_PATH)\ICE\CMN\VIL\APPL\BH\Car_var
CC_INCLUDE_PATH    += $(Spc_CORE_PATH)\ICE\CMN\VIL\APPL\BH\Model
CC_INCLUDE_PATH    += $(Spc_CORE_PATH)\ICE\CMN\VIL\APPL\TD\CAL
CC_INCLUDE_PATH    += $(Spc_CORE_PATH)\ICE\CMN\VIL\APPL\TD\Car_var
CC_INCLUDE_PATH    += $(Spc_CORE_PATH)\ICE\CMN\VIL\APPL\TD\Model
CC_INCLUDE_PATH    += $(Spc_CORE_PATH)\ICE\CMN\VIL\APPL\YFE\CAL
CC_INCLUDE_PATH    += $(Spc_CORE_PATH)\ICE\CMN\VIL\APPL\YFE\Car_var
CC_INCLUDE_PATH    += $(Spc_CORE_PATH)\ICE\CMN\VIL\APPL\YFE\Model
CC_INCLUDE_PATH    += $(Spc_CORE_PATH)\ICE\CMN\VIL\HDR

CC_INCLUDE_PATH    += $(Spc_CORE_PATH)\ICE\CMN\LIB
CC_INCLUDE_PATH    += $(Spc_CORE_PATH)\ICE\CMN\FBL_HDR\FBL_HMC\

endif