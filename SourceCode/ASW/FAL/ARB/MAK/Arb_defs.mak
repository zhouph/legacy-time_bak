# \file
#
# \brief Arb
#
# This file contains the implementation of the SWC
# module Arb.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Arb_CORE_PATH     := $(MANDO_FAL_ROOT)\Arb
Arb_CAL_PATH      := $(Arb_CORE_PATH)\CAL\$(Arb_VARIANT)
Arb_SRC_PATH      := $(Arb_CORE_PATH)\SRC
Arb_CFG_PATH      := $(Arb_CORE_PATH)\CFG\$(Arb_VARIANT)
Arb_HDR_PATH      := $(Arb_CORE_PATH)\HDR
Arb_IFA_PATH      := $(Arb_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Arb_CMN_PATH      := $(Arb_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	Arb_UT_PATH		:= $(Arb_CORE_PATH)\UT
	Arb_UNITY_PATH	:= $(Arb_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(Arb_UT_PATH)
	CC_INCLUDE_PATH		+= $(Arb_UNITY_PATH)
	Arb_Ctrl_PATH 	:= Arb_UT_PATH\Arb_Ctrl
endif
CC_INCLUDE_PATH    += $(Arb_CAL_PATH)
CC_INCLUDE_PATH    += $(Arb_SRC_PATH)
CC_INCLUDE_PATH    += $(Arb_CFG_PATH)
CC_INCLUDE_PATH    += $(Arb_HDR_PATH)
CC_INCLUDE_PATH    += $(Arb_IFA_PATH)
CC_INCLUDE_PATH    += $(Arb_CMN_PATH)

# /* HSH */

ifeq ($(ICE_COMPILE), true)

## ASW include
CC_INCLUDE_PATH    += $(Arb_CORE_PATH)\ICE\CMN\include

## Library INCLUDE
CC_INCLUDE_PATH    += $(Arb_CORE_PATH)\ICE\CMN\LIB

## VIL INCLUDE
CC_INCLUDE_PATH    += $(Arb_CORE_PATH)\ICE\CMN\VIL
CC_INCLUDE_PATH    += $(Arb_CORE_PATH)\ICE\CMN\VIL\_PAR
CC_INCLUDE_PATH    += $(Arb_CORE_PATH)\ICE\CMN\VIL\_PAR\car
CC_INCLUDE_PATH    += $(Arb_CORE_PATH)\ICE\CMN\VIL\_PAR\version
CC_INCLUDE_PATH    += $(Arb_CORE_PATH)\ICE\CMN\VIL\APPL
CC_INCLUDE_PATH    += $(Arb_CORE_PATH)\ICE\CMN\VIL\APPL\BH\CAL
CC_INCLUDE_PATH    += $(Arb_CORE_PATH)\ICE\CMN\VIL\APPL\BH\Car_var
CC_INCLUDE_PATH    += $(Arb_CORE_PATH)\ICE\CMN\VIL\APPL\BH\Model
CC_INCLUDE_PATH    += $(Arb_CORE_PATH)\ICE\CMN\VIL\APPL\TD\CAL
CC_INCLUDE_PATH    += $(Arb_CORE_PATH)\ICE\CMN\VIL\APPL\TD\Car_var
CC_INCLUDE_PATH    += $(Arb_CORE_PATH)\ICE\CMN\VIL\APPL\TD\Model
CC_INCLUDE_PATH    += $(Arb_CORE_PATH)\ICE\CMN\VIL\APPL\YFE\CAL
CC_INCLUDE_PATH    += $(Arb_CORE_PATH)\ICE\CMN\VIL\APPL\YFE\Car_var
CC_INCLUDE_PATH    += $(Arb_CORE_PATH)\ICE\CMN\VIL\APPL\YFE\Model
CC_INCLUDE_PATH    += $(Arb_CORE_PATH)\ICE\CMN\VIL\HDR

CC_INCLUDE_PATH    += $(Arb_CORE_PATH)\ICE\CMN\LIB
CC_INCLUDE_PATH    += $(Arb_CORE_PATH)\ICE\CMN\FBL_HDR\FBL_HMC\

endif