# \file
#
# \brief Arb
#
# This file contains the implementation of the SWC
# module Arb.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Arb_src

Arb_src_FILES        += $(Arb_SRC_PATH)\Arb_Ctrl.c
Arb_src_FILES        += $(Arb_SRC_PATH)\LCARBCallMain.c
Arb_src_FILES        += $(Arb_SRC_PATH)\LCARBRBCCooperation.c
Arb_src_FILES        += $(Arb_SRC_PATH)\LCARBCalRBCCooperTargetPRate.c
Arb_src_FILES        += $(Arb_IFA_PATH)\Arb_Ctrl_Ifa.c
Arb_src_FILES        += $(Arb_CFG_PATH)\Arb_Cfg.c
Arb_src_FILES        += $(Arb_CAL_PATH)\Arb_Cal.c
# /* HSH */
ifeq ($(ICE_COMPILE),true)
# Library
Arb_src_FILES        += $(Arb_CORE_PATH)\virtual_main.c
Arb_src_FILES  += $(Arb_CORE_PATH)\ICE\CMN\LIB\SwcCommonFunction.c
Arb_src_FILES  += $(Arb_CORE_PATH)\ICE\CMN\LIB\L_CommonFunction.c
Arb_src_FILES  += $(Arb_CORE_PATH)\ICE\CMN\FBL_HDR\FBL_HMC\Fbl_Header.c
Arb_src_FILES  += $(Arb_CORE_PATH)\ICE\CMN\FBL_HDR\FBL_HMC\Fbl_Mtab.c
endif

ifeq ($(UNIT_TEST),true)
	Arb_src_FILES        += $(Arb_UNITY_PATH)\unity.c
	Arb_src_FILES        += $(Arb_UNITY_PATH)\unity_fixture.c	
	Arb_src_FILES        += $(Arb_UT_PATH)\main.c
	Arb_src_FILES        += $(Arb_UT_PATH)\Arb_Ctrl\Arb_Ctrl_UtMain.c
endif