/**
 * @defgroup Arb_Ctrl Arb_Ctrl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Arb_Ctrl.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

#include "Arb_Ctrl_Ifa.h"
#include "LCARBCallMain.h"
#include "LCARBRBCCooperation.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/


/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/


/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ARB_CTRL_START_SEC_CONST_UNSPECIFIED
#include "Arb_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ARB_CTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Arb_MemMap.h"

#define lcarbs16TpRate100	100
#define lcarbs16TpRate150	150
#define lcarbs16TpRate200	200
#define lcarbs16TpRate300	300
#define lcarbs16TpRate400	400

#define lcarbu8CntrModeBbc	10
#define lcarbu8CntrModeEbd	20
#define lcarbu8CntrModeCbc	21
#define lcarbu8CntrModeSls	22
#define lcarbu8CntrModeAbs	30
#define lcarbu8CntrModeEscOver	41
#define lcarbu8CntrModeEscUnder	42
#define lcarbu8CntrModeEscCross	43
#define lcarbu8CntrModeEscOther	44
#define lcarbu8CntrModeTcs	50
#define lcarbu8CntrModeTvbb	51
#define lcarbu8CntrModeScc	60
#define lcarbu8CntrModeEpbi	61
#define lcarbu8CntrModeTsp	62
#define lcarbu8CntrModeHsa	70
#define lcarbu8CntrModeAvh	71
#define lcarbu8CntrModeRbc	80
#define lcarbu8CntrModeBa	90

/*****/
/*#define S8_BOOST_MODE_REQ_L0				0		ABS Torque Control Mode 										*/
/*#define S8_BOOST_MODE_REQ_L1				1		Quick Braking , AEB : 400bar/s 								*/
/*#define S8_BOOST_MODE_REQ_L2				2		ESC oversteer (Include ROP), ABS, TCS_1_mode : 300bar/s 		*/
/*#define S8_BOOST_MODE_REQ_L3				3		TCS_2_mode, TSP, ESC understeer : 200bar/s  					*/
/*#define S8_BOOST_MODE_REQ_L4				4		Decel control : SCC : 200 bar/s dependent on speed			*/
/*#define S8_BOOST_MODE_REQ_L5				5		Decel control : BBC, RBC : 200 bar/s Normal Speed			*/
/*#define S8_BOOST_MODE_REQ_L6				6		CBC SLS, EBD 200bar/s Reduce Pressure function				*/
/*#define S8_BOOST_MODE_REQ_L7				7		TCS_4_mode, Hold function(HSA, AVH)AC Hold 100bar/s			*/
/*#define S8_BOOST_MODE_REQ_L8				8		Release of Hold function										*/
/*#define S8_BOOST_MODE_REQ_L9				9		ABS / EBD : 													*/

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ARB_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arb_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ARB_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arb_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
static uint8_t i;

static uint8_t lcarbu8GetAbsActFlag;
/*uint8_t lcarbu8GetAbsDefectFlg;*/		
/*int16_t lcarbs16GetFrntWhlSlip;  */	
/*int16_t lcarbs16GetAbsDesTarP;*/		
/*uint8_t lcarbu8GetAbsDesTarPReqFlg;*/	
/*uint8_t lcarbu8GetAbsCtrlFadeOutFlg;  */

/*uint8_t lcarbu8GetAvhActFlg;	  */ 
/*uint8_t lcarbu8GetAvhDefectFlg; */ 
/*uint16_t lcarbu16GetAvhTarP;	  */  

/*uint8_t lcarbu8GetBaActFlg;	  */ 
/*uint8_t lcarbu8GetBaCDefectFlg; */
/*uint16_t lcarbu16GetBaTarP;	  */ 

/*uint8_t lcarbu8GeEbpActFlg;	 */  
/*uint8_t lcarbu8GeEbpDefectFlg; */ 
/*uint16_t lcarbu16GeEbpTarP;	 */  
                    
/*uint8_t lcarbu8GetEpbiActFlg;	  */
/*uint8_t lcarbu8GetEpbiDefectFlg;*/ 
/*uint16_t lcarbu16GetEpbiTarP;	  */ 	 
                                
static uint8_t lcarbu8GetEscActFlg;	    
/*uint8_t lcarbu8GetEscDefectFlg; */ 	 

/*uint8_t lcarbu8GetHsaDefectFlg; */ 		
		 	
                                
                                
/*uint8_t lcarbu8GetSccActFlg;		*/	
/*uint8_t lcarbu8GetSccDefectFlg;	*/	
/*uint16_t lcarbu16GetSccTarP;		*/ 	
                                
static uint8_t  lcarbu8GetTcsActFlg;		 
/*uint8_t  lcarbu8GetTcsDefectFlg;	 */	 
/*int16_t  lcarbstGetBothDrvgWhlBrkCtlReqFlg;*/

/*uint8_t  lcarbu8GetTvbbActFlg;		*/ 
/*uint8_t  lcarbu8GetTvbbDefectFlg;     */
/*uint16_t lcarbu16GetTvbbTarP;		    */
 
/*static uint8_t  lcarbu8GetStandStillFlg;*/
/*static uint8_t  lcarbu8GetBaseBrkCtrlrActFlg;	*/
/*static uint16_t lcarbu16GetRednForBaseBrkCtrl;	*/
/*static uint8_t  lcarbu8GetPCtrlSt;			 	*/
/*static uint8_t  lcarbu8GetRgnBrkCtrlrActStFlg; 	*/
/*static uint16_t lcarbu16GetTarPFromBaseBrkCtrlr; 	*/
/*static uint16_t lcarbu16GetTarPRateFromBaseBrk; 	*/

static uint16_t lcarbu16FinalTPFrntLeOld;  
uint8_t  lcarbu8CntrModefourwh; 

static uint8_t lcArbu8BbcCtrlSt;

static uint16_t lcArbu16MaxCircuitTp;

ARB_struct ARB_FL, ARB_FR, ARB_RL, ARB_RR;

static int32_t  lcArbAr5FlOutVlaCtrlTar[5];
static int32_t  lcArbAr5FrOutVlaCtrlTar[5];
static int32_t  lcArbAr5RlOutVlaCtrlTar[5];
static int32_t  lcArbAr5RrOutVlaCtrlTar[5];
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ARB_CTRL_START_SEC_CODE
#include "Arb_MemMap.h"

static void LCARB_ReadEbdInfo(void);
static void LCARB_ReadAbsInfo(void);
static void LCARB_ReadAvhInfo(void);
static void LCARB_ReadBaInfo(void);
static void LCARB_ReadEbpInfo(void);
static void LCARB_ReadEscInfo(void);
static void LCARB_ReadHsaInfo(void);
static void LCARB_ReadSccInfo(void);
static void LCARB_ReadTcsInfo(void);
static void LCARB_ReadTvbbInfo(void);
static void LCARB_ReadAbcDataInfo(void);
static void LCARB_ReadVafDataInfo(void); 
static void LCARB_ReadBbcDataInfo(void);    
static void LCARB_ReadOutletValveInfo(void); 
static void LCARB_InitializeValve(void);
static void LCARB_InitialFinalPara(void);
static void LCARB_InitialEbdPara(void);
static void LCARB_InitialAbsPara(void);
static void LCARB_InitialEscPara(void);
static void LCARB_InitialTcsPara(void);
static void LCARB_InitialHsaPara(void);
static void LCARB_InitialBbcPara(void);
static void LCARB_SetCtrlMode(ARB_struct *ARB_WL);
static void LCARB_SetCtrlModeEscCross(ARB_struct *ARB_WL);
static void LCARB_SetCtrlModeEscOhter(ARB_struct *ARB_WL);
static void LCARB_SetCtrlModeWoEsc(ARB_struct *ARB_WL);
static void LCARB_SetCtrlModeWoEsc2(ARB_struct *ARB_WL);
static void LCARB_SetBoostMode(void);  
static void LCARB_SetBoostModenotStbFunc(void);   
static void LCARB_SetTcsBoostMode(void);                      
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void LCARB_InitializePara(void)
{
	LCARB_InitializeValve();
	LCARB_InitialFinalPara();
	LCARB_InitialEbdPara();
	LCARB_InitialAbsPara();
	LCARB_InitialEscPara();
	LCARB_InitialTcsPara();
	LCARB_InitialHsaPara();
	LCARB_InitialBbcPara();
}
void LCARB_SinalProcessing(void)
{
	/*Temp*/
	LCARB_ReadAbcDataInfo();
	LCARB_ReadVafDataInfo();
	LCARB_ReadBbcDataInfo();   
	LCARB_ReadOutletValveInfo();	

}
void LCARB_CalTarPressure(void)
{
	LCARB_SetCtrlMode(&ARB_FL);
	LCARB_SetCtrlMode(&ARB_FR);
	LCARB_SetCtrlMode(&ARB_RL);
	LCARB_SetCtrlMode(&ARB_RR);	
	
	lcarbu16FinalTPFrntLeOld = ARB_FL.lcarbu16SetTP;	
	
	LCARB_SetBoostMode();

	LCARB_vEstTargetPress(); /*TODOLDAHB_vEstTargetPress*/
}
void LCARB_SetDataInfo(void)
{       
	Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalTarPOfWhlFrntLe 		= (int32_t)ARB_FL.lcarbu16SetTP;
	Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalTarPOfWhlFrntRi 		= (int32_t)ARB_FR.lcarbu16SetTP;
	Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalTarPOfWhlReLe   		= (int32_t)ARB_RL.lcarbu16SetTP;
	Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalTarPOfWhlReRi   		= (int32_t)ARB_RR.lcarbu16SetTP;
	Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalTarPRateOfWhlFrntLe 	= (int32_t)ARB_FL.lcarbs16SetTPRate;
	Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalTarPRateOfWhlFrntRi 	= (int32_t)ARB_FR.lcarbs16SetTPRate;
	Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalTarPRateOfWhlReLe   	= (int32_t)ARB_RL.lcarbs16SetTPRate;
	Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalTarPRateOfWhlReRi   	= (int32_t)ARB_RR.lcarbs16SetTPRate;
	Arb_CtrlBus.Arb_CtrlFinalTarPInfo.PCtrlPrioOfWhlFrntLe 		= (int32_t)ARB_FL.lcarbu8SetPrionrity;
	Arb_CtrlBus.Arb_CtrlFinalTarPInfo.PCtrlPrioOfWhlFrntRi 		= (int32_t)ARB_FR.lcarbu8SetPrionrity;
	Arb_CtrlBus.Arb_CtrlFinalTarPInfo.PCtrlPrioOfWhlReLe   		= (int32_t)ARB_RL.lcarbu8SetPrionrity;
	Arb_CtrlBus.Arb_CtrlFinalTarPInfo.PCtrlPrioOfWhlReRi   		= (int32_t)ARB_RR.lcarbu8SetPrionrity;
	Arb_CtrlBus.Arb_CtrlFinalTarPInfo.CtrlModOfWhlFrntLe   		= (int32_t)ARB_FL.lcarbu8SetCntrMode;
	Arb_CtrlBus.Arb_CtrlFinalTarPInfo.CtrlModOfOfWhlFrntRi		= (int32_t)ARB_FR.lcarbu8SetCntrMode;
	Arb_CtrlBus.Arb_CtrlFinalTarPInfo.CtrlModOfWhlReLe     		= (int32_t)ARB_RL.lcarbu8SetCntrMode;
 	Arb_CtrlBus.Arb_CtrlFinalTarPInfo.CtrlModOfWhlReRi     		= (int32_t)ARB_RR.lcarbu8SetCntrMode;
 	Arb_CtrlBus.Arb_CtrlFinalTarPInfo.ReqdPCtrlBoostMod    		= (int32_t)lcarbu8CntrModefourwh;
 	
    Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalDelTarPOfWhlFrntLe = (int32_t)ARB_FL.lcarbs16SetDelTP;
    Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalDelTarPOfWhlFrntRi = (int32_t)ARB_FR.lcarbs16SetDelTP;
    Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalDelTarPOfWhlReLe = (int32_t)ARB_RL.lcarbs16SetDelTP;
    Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalDelTarPOfWhlReRi = (int32_t)ARB_RR.lcarbs16SetDelTP;
    
    Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalMaxCircuitTp = (int32_t)lcArbu16MaxCircuitTp;

 	for(i=0;i<5;i++){Arb_CtrlBus.Arb_CtrlWhlOutVlvCtrlTarInfo.FlOutVlvCtrlTar[i] = lcArbAr5FlOutVlaCtrlTar[i];}
 	for(i=0;i<5;i++){Arb_CtrlBus.Arb_CtrlWhlOutVlvCtrlTarInfo.FrOutVlvCtrlTar[i] = lcArbAr5FrOutVlaCtrlTar[i];}
 	for(i=0;i<5;i++){Arb_CtrlBus.Arb_CtrlWhlOutVlvCtrlTarInfo.RlOutVlvCtrlTar[i] = lcArbAr5RlOutVlaCtrlTar[i];}
 	for(i=0;i<5;i++){Arb_CtrlBus.Arb_CtrlWhlOutVlvCtrlTarInfo.RrOutVlvCtrlTar[i] = lcArbAr5RrOutVlaCtrlTar[i];}
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static void LCARB_InitializeValve(void) 
{	                          
	lcArbAr5FlOutVlaCtrlTar[0] = 0;  
	lcArbAr5FlOutVlaCtrlTar[1] = 0;  
	lcArbAr5FlOutVlaCtrlTar[2] = 0;  
	lcArbAr5FlOutVlaCtrlTar[3] = 0;  
	lcArbAr5FlOutVlaCtrlTar[4] = 0;  
	
	lcArbAr5FrOutVlaCtrlTar[0] = 0; 
	lcArbAr5FrOutVlaCtrlTar[1] = 0; 
	lcArbAr5FrOutVlaCtrlTar[2] = 0; 
	lcArbAr5FrOutVlaCtrlTar[3] = 0; 
	lcArbAr5FrOutVlaCtrlTar[4] = 0; 
	
	lcArbAr5RlOutVlaCtrlTar[0] = 0;  
	lcArbAr5RlOutVlaCtrlTar[1] = 0;  
	lcArbAr5RlOutVlaCtrlTar[2] = 0;  
	lcArbAr5RlOutVlaCtrlTar[3] = 0;  
	lcArbAr5RlOutVlaCtrlTar[4] = 0;  
	
	lcArbAr5RrOutVlaCtrlTar[0] = 0; 
	lcArbAr5RrOutVlaCtrlTar[1] = 0; 
	lcArbAr5RrOutVlaCtrlTar[2] = 0; 
	lcArbAr5RrOutVlaCtrlTar[3] = 0; 
	lcArbAr5RrOutVlaCtrlTar[4] = 0; 	 	

}  
static void LCARB_InitialFinalPara(void)
{
	ARB_FL.lcarbu16SetFnlTP = 0;
	ARB_FR.lcarbu16SetFnlTP = 0;
	ARB_RL.lcarbu16SetFnlTP = 0;
	ARB_RR.lcarbu16SetFnlTP = 0;
	ARB_FL.lcarbs16SetFnlTPRate = 0;
	ARB_FR.lcarbs16SetFnlTPRate = 0;	
	ARB_RL.lcarbs16SetFnlTPRate = 0;	
	ARB_RR.lcarbs16SetFnlTPRate = 0;		
	ARB_FL.lcarbu8SetFnlPrionrity = 0;	
	ARB_FR.lcarbu8SetFnlPrionrity = 0;
	ARB_RL.lcarbu8SetFnlPrionrity = 0;
	ARB_RR.lcarbu8SetFnlPrionrity = 0;
	ARB_FL.lcarbu8SetFnlCntrMode = 0;  
	ARB_FR.lcarbu8SetFnlCntrMode = 0;  
	ARB_RL.lcarbu8SetFnlCntrMode = 0;  
	ARB_RR.lcarbu8SetFnlCntrMode = 0;  
	
	ARB_FL.lcarbu16SetTP= 0; 
	ARB_FR.lcarbu16SetTP= 0; 
	ARB_RL.lcarbu16SetTP= 0; 
	ARB_RR.lcarbu16SetTP= 0; 
	ARB_FL.lcarbs16SetDelTP= 0;
	ARB_FR.lcarbs16SetDelTP= 0;
	ARB_RL.lcarbs16SetDelTP= 0;
	ARB_RR.lcarbs16SetDelTP= 0;
	ARB_FL.lcarbs16SetTPRate= 0; 		
	ARB_FR.lcarbs16SetTPRate= 0; 		
	ARB_RL.lcarbs16SetTPRate= 0; 		
	ARB_RR.lcarbs16SetTPRate= 0; 		
	ARB_FL.lcarbu8SetPrionrity= 0; 
	ARB_FR.lcarbu8SetPrionrity= 0; 	
	ARB_RL.lcarbu8SetPrionrity= 0; 
	ARB_RR.lcarbu8SetPrionrity= 0; 
	ARB_FL.lcarbu8SetCntrMode= 0;   
	ARB_FR.lcarbu8SetCntrMode= 0; 
	ARB_RL.lcarbu8SetCntrMode= 0; 
	ARB_RR.lcarbu8SetCntrMode= 0; 
} 
static void LCARB_InitialEbdPara(void)
{
	
	ARB_FL.lcarbu16GetEbdTarP= 0; 
	ARB_FR.lcarbu16GetEbdTarP= 0; 
	ARB_RL.lcarbu16GetEbdTarP= 0; 
	ARB_RR.lcarbu16GetEbdTarP= 0; 
	ARB_FL.lcarbs16GetEbdDelTarP= 0;
	ARB_FR.lcarbs16GetEbdDelTarP= 0;
	ARB_RL.lcarbs16GetEbdDelTarP= 0;
	ARB_RR.lcarbs16GetEbdDelTarP= 0;
	ARB_FL.lcarbs16GetEbdTarPRate= 0; 
	ARB_FR.lcarbs16GetEbdTarPRate= 0; 
	ARB_RL.lcarbs16GetEbdTarPRate= 0; 
	ARB_RR.lcarbs16GetEbdTarPRate= 0; 
	ARB_FL.lcarbu8GetEbdCntrMode= 0;   
	ARB_FR.lcarbu8GetEbdCntrMode= 0;  
	ARB_RL.lcarbu8GetEbdCntrMode= 0;  
	ARB_RR.lcarbu8GetEbdCntrMode= 0;  
	ARB_FL.lcarbu8GetEbdPrionrity= 0;
	ARB_FR.lcarbu8GetEbdPrionrity= 0;
	ARB_RL.lcarbu8GetEbdPrionrity= 0;
	ARB_RR.lcarbu8GetEbdPrionrity= 0;
}     
static void LCARB_InitialAbsPara(void)
{
	lcarbu8GetAbsActFlag = 0;
	
	ARB_FL.lcarbu16GetAbsTarP= 0; 
	ARB_FR.lcarbu16GetAbsTarP= 0; 
	ARB_RL.lcarbu16GetAbsTarP= 0; 
	ARB_RR.lcarbu16GetAbsTarP= 0; 
	ARB_FL.lcarbs16GetAbsDelTarP= 0;
	ARB_FR.lcarbs16GetAbsDelTarP= 0;
	ARB_RL.lcarbs16GetAbsDelTarP= 0;
	ARB_RR.lcarbs16GetAbsDelTarP= 0;
	ARB_FL.lcarbs16GetAbsTarPRate= 0;   
	ARB_FR.lcarbs16GetAbsTarPRate= 0;   
	ARB_RL.lcarbs16GetAbsTarPRate= 0;   
	ARB_RR.lcarbs16GetAbsTarPRate= 0;   
	ARB_FL.lcarbu8GetAbsCntrMode= 0;   
	ARB_FR.lcarbu8GetAbsCntrMode= 0;  
	ARB_RL.lcarbu8GetAbsCntrMode= 0;  
	ARB_RR.lcarbu8GetAbsCntrMode= 0;  
	ARB_FL.lcarbu8GetAbsPrionrity= 0; 
	ARB_FR.lcarbu8GetAbsPrionrity= 0; 
	ARB_RL.lcarbu8GetAbsPrionrity= 0; 	
	ARB_RR.lcarbu8GetAbsPrionrity= 0; 
} 
static void LCARB_InitialEscPara(void)
{
	lcarbu8GetEscActFlg = 0;                 
	
	ARB_FL.lcarbu16GetEscTarP= 0; 
	ARB_FR.lcarbu16GetEscTarP= 0; 
	ARB_RL.lcarbu16GetEscTarP= 0; 
	ARB_RR.lcarbu16GetEscTarP= 0;
	ARB_FL.lcarbs16GetEscDelTarP= 0;
	ARB_FR.lcarbs16GetEscDelTarP= 0;
	ARB_RL.lcarbs16GetEscDelTarP= 0;
	ARB_RR.lcarbs16GetEscDelTarP= 0; 
	ARB_FL.lcarbs16GetEscTarPRate= 0;   
	ARB_FR.lcarbs16GetEscTarPRate= 0;   
	ARB_RL.lcarbs16GetEscTarPRate= 0;   
	ARB_RR.lcarbs16GetEscTarPRate= 0;   
	ARB_FL.lcarbu8GetEscCntrMode= 0;   
	ARB_FR.lcarbu8GetEscCntrMode= 0;  
	ARB_RL.lcarbu8GetEscCntrMode= 0;  
	ARB_RR.lcarbu8GetEscCntrMode= 0;  
	ARB_FL.lcarbu8GetEscPrionrity= 0; 
	ARB_FR.lcarbu8GetEscPrionrity= 0; 	
	ARB_RL.lcarbu8GetEscPrionrity= 0; 		
	ARB_RR.lcarbu8GetEscPrionrity= 0; 	
}      
static void LCARB_InitialTcsPara(void)
{  		
	ARB_FL.lcarbu16GetTcsTarP= 0; 
	ARB_FR.lcarbu16GetTcsTarP= 0; 
	ARB_RL.lcarbu16GetTcsTarP= 0; 
	ARB_RR.lcarbu16GetTcsTarP= 0; 
	ARB_FL.lcarbs16GetTcsDelTarP= 0;
	ARB_FR.lcarbs16GetTcsDelTarP= 0;
	ARB_RL.lcarbs16GetTcsDelTarP= 0;
	ARB_RR.lcarbs16GetTcsDelTarP= 0;
	ARB_FL.lcarbs16GetTcsTarPRate= 0;   
	ARB_FR.lcarbs16GetTcsTarPRate= 0;   
	ARB_RL.lcarbs16GetTcsTarPRate= 0;   
	ARB_RR.lcarbs16GetTcsTarPRate= 0;   
	ARB_FL.lcarbu8GetTcsCntrMode= 0;   
	ARB_FR.lcarbu8GetTcsCntrMode= 0;  
	ARB_RL.lcarbu8GetTcsCntrMode= 0;  
	ARB_RR.lcarbu8GetTcsCntrMode= 0;  
	ARB_FL.lcarbu8GetTcsPrionrity= 0; 
	ARB_FR.lcarbu8GetTcsPrionrity= 0; 	
	ARB_RL.lcarbu8GetTcsPrionrity= 0; 		
	ARB_RR.lcarbu8GetTcsPrionrity= 0; 	
} 
static void LCARB_InitialHsaPara(void)
{  	
	ARB_FL.lcarbu16GetHsaTarP= 0;       
	ARB_FR.lcarbu16GetHsaTarP= 0;       
	ARB_RL.lcarbu16GetHsaTarP= 0;       
	ARB_RR.lcarbu16GetHsaTarP= 0;
	ARB_FL.lcarbs16GetHsaDelTarP= 0;
	ARB_FR.lcarbs16GetHsaDelTarP= 0;
	ARB_RL.lcarbs16GetHsaDelTarP= 0;
	ARB_RR.lcarbs16GetHsaDelTarP= 0;       
	ARB_FL.lcarbs16GetHsaTarPRate= 0;   
	ARB_FR.lcarbs16GetHsaTarPRate= 0;   
	ARB_RL.lcarbs16GetHsaTarPRate= 0;   
	ARB_RR.lcarbs16GetHsaTarPRate= 0;   
	ARB_FL.lcarbu8GetHsaCntrMode= 0;    
	ARB_FR.lcarbu8GetHsaCntrMode= 0;    
	ARB_RL.lcarbu8GetHsaCntrMode= 0;    
	ARB_RR.lcarbu8GetHsaCntrMode= 0;    
	ARB_FL.lcarbu8GetHsaPrionrity= 0; 
	ARB_FR.lcarbu8GetHsaPrionrity= 0; 
	ARB_RL.lcarbu8GetHsaPrionrity= 0; 
	ARB_RR.lcarbu8GetHsaPrionrity= 0; 
}
static void LCARB_InitialBbcPara(void)
{  		
	ARB_FL.lcarbu16GetBbcTarP= 0;       
	ARB_FR.lcarbu16GetBbcTarP= 0;       
	ARB_RL.lcarbu16GetBbcTarP= 0;       
	ARB_RR.lcarbu16GetBbcTarP= 0;  
	ARB_FL.lcarbs16GetBbcDelTarP= 0;
	ARB_FR.lcarbs16GetBbcDelTarP= 0;
	ARB_RL.lcarbs16GetBbcDelTarP= 0;
	ARB_RR.lcarbs16GetBbcDelTarP= 0;    	     
	ARB_FL.lcarbs16GetBbcTarPRate= 0;   
	ARB_FR.lcarbs16GetBbcTarPRate= 0;   
	ARB_RL.lcarbs16GetBbcTarPRate= 0;   
	ARB_RR.lcarbs16GetBbcTarPRate= 0;   
	ARB_FL.lcarbu8GetBbcCntrMode= 0;    
	ARB_FR.lcarbu8GetBbcCntrMode= 0;    
	ARB_RL.lcarbu8GetBbcCntrMode= 0;    
	ARB_RR.lcarbu8GetBbcCntrMode= 0;    
	ARB_FL.lcarbu8GetBbcPrionrity= 0; 
	ARB_FR.lcarbu8GetBbcPrionrity= 0; 
	ARB_RL.lcarbu8GetBbcPrionrity= 0; 
	ARB_RR.lcarbu8GetBbcPrionrity= 0; 
	lcArbu8BbcCtrlSt=0;
	lcArbu16MaxCircuitTp = 0;

}                                         
static void LCARB_ReadAbcDataInfo(void) 
{  
	LCARB_ReadEbdInfo();
	LCARB_ReadAbsInfo();
	LCARB_ReadEscInfo();
	LCARB_ReadTcsInfo();
	LCARB_ReadBaInfo();	
}
static void LCARB_ReadVafDataInfo(void) 
{
	LCARB_ReadAvhInfo();
	LCARB_ReadEbpInfo();
	LCARB_ReadHsaInfo();
	LCARB_ReadSccInfo();
	LCARB_ReadTvbbInfo();
}
static void LCARB_ReadEbdInfo(void)           
{
	ARB_FL.lcarbu16GetEbdTarP	=0;
    ARB_FR.lcarbu16GetEbdTarP	=0;
	ARB_RL.lcarbu16GetEbdTarP	=(uint16_t)Arb_CtrlEbdCtrlInfo.EbdTarPReLe;      
	ARB_RR.lcarbu16GetEbdTarP	=(uint16_t)Arb_CtrlEbdCtrlInfo.EbdTarPReRi; 

	ARB_FL.lcarbs16GetEbdDelTarP	=0;
    ARB_FR.lcarbs16GetEbdDelTarP	=0;
	ARB_RL.lcarbs16GetEbdDelTarP	=(int16_t)Arb_CtrlEbdCtrlInfo.EbdDelTarPReLe;      
	ARB_RR.lcarbs16GetEbdDelTarP	=(int16_t)Arb_CtrlEbdCtrlInfo.EbdDelTarPReLe; 
	
	ARB_FL.lcarbs16GetEbdTarPRate = 0;
    ARB_FR.lcarbs16GetEbdTarPRate = 0;
	ARB_RL.lcarbs16GetEbdTarPRate = (int16_t)Arb_CtrlEbdCtrlInfo.EbdTarPReLe;      
	ARB_RR.lcarbs16GetEbdTarPRate = (int16_t)Arb_CtrlEbdCtrlInfo.EbdTarPReRi; 
	
	ARB_FL.lcarbu8GetEbdCntrMode  = 0;
	ARB_FR.lcarbu8GetEbdCntrMode  = 0;
	ARB_RL.lcarbu8GetEbdCntrMode  = (uint8_t)Arb_CtrlEbdCtrlInfo.EbdCtrlModeReLe;
	ARB_RR.lcarbu8GetEbdCntrMode  = (uint8_t)Arb_CtrlEbdCtrlInfo.EbdCtrlModeReRi;
	
	ARB_FL.lcarbu8GetEbdPrionrity	=0;
	ARB_FR.lcarbu8GetEbdPrionrity	=0;
	ARB_RL.lcarbu8GetEbdPrionrity	=0;
	ARB_RR.lcarbu8GetEbdPrionrity	=0;
	

}
static void LCARB_ReadAbsInfo(void)
{
	lcarbu8GetAbsActFlag		=(uint8_t)Arb_CtrlAbsCtrlInfo.AbsActFlg;         
/*	lcarbu8GetAbsDefectFlg		=(uint8_t)Arb_CtrlAbsCtrlInfo.AbsDefectFlg;  */    
	ARB_FL.lcarbu16GetAbsTarP	=(uint16_t)Arb_CtrlAbsCtrlInfo.AbsTarPFrntLe;     
	ARB_FR.lcarbu16GetAbsTarP	=(uint16_t)Arb_CtrlAbsCtrlInfo.AbsTarPFrntRi;     
	ARB_RL.lcarbu16GetAbsTarP	=(uint16_t)Arb_CtrlAbsCtrlInfo.AbsTarPReLe;       
	ARB_RR.lcarbu16GetAbsTarP	=(uint16_t)Arb_CtrlAbsCtrlInfo.AbsTarPReRi;       
/*		lcarbs16GetFrntWhlSlip		=(uint16_t)Arb_CtrlAbsCtrlInfo.FrntWhlSlip;  */      
/*		lcarbs16GetAbsDesTarP		=(uint16_t)Arb_CtrlAbsCtrlInfo.AbsDesTarP;    */     
/*		lcarbu8GetAbsDesTarPReqFlg	=(uint8_t)Arb_CtrlAbsCtrlInfo.AbsDesTarPReqFlg;  */ 
/*		lcarbu8GetAbsCtrlFadeOutFlg	=(uint8_t)Arb_CtrlAbsCtrlInfo.AbsCtrlFadeOutFlg; */ 	

	ARB_FL.lcarbs16GetAbsDelTarP	=(int16_t)Arb_CtrlAbsCtrlInfo.AbsDelTarPFrntLe;
    ARB_FR.lcarbs16GetAbsDelTarP	=(int16_t)Arb_CtrlAbsCtrlInfo.AbsDelTarPFrntRi;
	ARB_RL.lcarbs16GetAbsDelTarP	=(int16_t)Arb_CtrlAbsCtrlInfo.AbsDelTarPReLe ;   
	ARB_RR.lcarbs16GetAbsDelTarP	=(int16_t)Arb_CtrlAbsCtrlInfo.AbsDelTarPReRi;
	
	ARB_FL.lcarbs16GetAbsTarPRate	=(int16_t)Arb_CtrlAbsCtrlInfo.AbsTarPRateFrntLe;  
	ARB_FR.lcarbs16GetAbsTarPRate	=(int16_t)Arb_CtrlAbsCtrlInfo.AbsTarPRateFrntRi;  
	ARB_RL.lcarbs16GetAbsTarPRate	=(int16_t)Arb_CtrlAbsCtrlInfo.AbsTarPRateReLe;
	ARB_RR.lcarbs16GetAbsTarPRate	=(int16_t)Arb_CtrlAbsCtrlInfo.AbsTarPRateReRi;

	ARB_FL.lcarbu8GetAbsCntrMode	=(uint8_t)Arb_CtrlAbsCtrlInfo.AbsCtrlModeFrntLe;
	ARB_FR.lcarbu8GetAbsCntrMode	=(uint8_t)Arb_CtrlAbsCtrlInfo.AbsCtrlModeFrntRi;
	ARB_RL.lcarbu8GetAbsCntrMode	=(uint8_t)Arb_CtrlAbsCtrlInfo.AbsCtrlModeReLe;
	ARB_RR.lcarbu8GetAbsCntrMode	=(uint8_t)Arb_CtrlAbsCtrlInfo.AbsCtrlModeReRi;

	ARB_FL.lcarbu8GetAbsPrionrity	=(uint8_t)Arb_CtrlAbsCtrlInfo.AbsPrioFrntLe;    
	ARB_FR.lcarbu8GetAbsPrionrity	=(uint8_t)Arb_CtrlAbsCtrlInfo.AbsPrioFrntRi;   
	ARB_RL.lcarbu8GetAbsPrionrity	=(uint8_t)Arb_CtrlAbsCtrlInfo.AbsPrioReLe;   
	ARB_RR.lcarbu8GetAbsPrionrity	=(uint8_t)Arb_CtrlAbsCtrlInfo.AbsPrioReRi;   
}
static void LCARB_ReadAvhInfo(void)
{
	/*lcarbu8GetAvhActFlg   =(uint8_t)Arb_CtrlAvhCtrlInfo.AvhActFlg;      */
	/*lcarbu8GetAvhDefectFlg =(uint8_t)Arb_CtrlAvhCtrlInfo.AvhDefectFlg;   */
	/*lcarbu16GetAvhTarP	   =(uint16_t)Arb_CtrlAvhCtrlInfo.AvhTarP;       */
}
static void LCARB_ReadBaInfo(void)
{
	/*lcarbu8GetBaActFlg	  	=(uint8_t)Arb_CtrlBaCtrlInfo.BaActFlg;      */
	/*lcarbu8GetBaCDefectFlg 	=(uint8_t)Arb_CtrlBaCtrlInfo.BaCDefectFlg;  */
	/*lcarbu16GetBaTarP	   	=(uint16_t)Arb_CtrlBaCtrlInfo.BaTarP;     	    */
}
static void LCARB_ReadEbpInfo(void)
{
	/*lcarbu8GeEbpActFlg	  	=(uint8_t)Arb_CtrlEbpCtrlInfo.EbpActFlg;     */  
	/*lcarbu8GeEbpDefectFlg 	=(uint8_t)Arb_CtrlEbpCtrlInfo.EbpDefectFlg;  */  
	/*lcarbu16GeEbpTarP	   	=(uint16_t)Arb_CtrlEbpCtrlInfo.EbpTarP;          */
	
	/*lcarbu8GetEpbiActFlg	 =(uint8_t)Arb_CtrlEpbiCtrlInfo.EpbiActFlg;      */
	/*lcarbu8GetEpbiDefectFlg  =(uint8_t)Arb_CtrlEpbiCtrlInfo.EpbiDefectFlg; */
/*	lcarbu16GetEpbiTarP	   	 =(uint16_t)Arb_CtrlEpbiCtrlInfo.EpbiTarP;  */      
}
static void LCARB_ReadEscInfo(void)
{
	lcarbu8GetEscActFlg		 =(uint8_t)Arb_CtrlEscCtrlInfo.EscActFlg;    
	/*lcarbu8GetEscDefectFlg	 =(uint8_t)Arb_CtrlEscCtrlInfo.EscDefectFlg;   */
	ARB_FL.lcarbu16GetEscTarP =(uint16_t)Arb_CtrlEscCtrlInfo.EscTarPFrntLe;
	ARB_FR.lcarbu16GetEscTarP =(uint16_t)Arb_CtrlEscCtrlInfo.EscTarPFrntRi;
	ARB_RL.lcarbu16GetEscTarP	 =(uint16_t)Arb_CtrlEscCtrlInfo.EscTarPReLe;  
	ARB_RR.lcarbu16GetEscTarP	 =(uint16_t)Arb_CtrlEscCtrlInfo.EscTarPReRi;
	  
	ARB_FL.lcarbs16GetEscDelTarP	=(int16_t)Arb_CtrlEscCtrlInfo.EscDelTarPFrntLe;
    ARB_FR.lcarbs16GetEscDelTarP	=(int16_t)Arb_CtrlEscCtrlInfo.EscDelTarPFrntRi;
	ARB_RL.lcarbs16GetEscDelTarP	=(int16_t)Arb_CtrlEscCtrlInfo.EscDelTarPReLe ;   
	ARB_RR.lcarbs16GetEscDelTarP	=(int16_t)Arb_CtrlEscCtrlInfo.EscDelTarPReRi;

	
	ARB_FL.lcarbs16GetEscTarPRate	=(int16_t)Arb_CtrlEscCtrlInfo.EscTarPRateFrntLe;  
	ARB_FR.lcarbs16GetEscTarPRate	=(int16_t)Arb_CtrlEscCtrlInfo.EscTarPRateFrntRi;  
	ARB_RL.lcarbs16GetEscTarPRate	=(int16_t)Arb_CtrlEscCtrlInfo.EscTarPRateReLe;
	ARB_RR.lcarbs16GetEscTarPRate	=(int16_t)Arb_CtrlEscCtrlInfo.EscTarPRateReRi;

	ARB_FL.lcarbu8GetEscCntrMode	=(uint8_t)Arb_CtrlEscCtrlInfo.EscCtrlModeFrntLe;
	ARB_FR.lcarbu8GetEscCntrMode	=(uint8_t)Arb_CtrlEscCtrlInfo.EscCtrlModeFrntRi;
	ARB_RL.lcarbu8GetEscCntrMode	=(uint8_t)Arb_CtrlEscCtrlInfo.EscCtrlModeReLe;
	ARB_RR.lcarbu8GetEscCntrMode	=(uint8_t)Arb_CtrlEscCtrlInfo.EscCtrlModeReRi;

	ARB_FL.lcarbu8GetEscPrionrity	=(uint8_t)Arb_CtrlEscCtrlInfo.EscPrioFrntLe;    
	ARB_FR.lcarbu8GetEscPrionrity	=(uint8_t)Arb_CtrlEscCtrlInfo.EscPrioFrntRi;   
	ARB_RL.lcarbu8GetEscPrionrity	=(uint8_t)Arb_CtrlEscCtrlInfo.EscPrioReLe;   
	ARB_RR.lcarbu8GetEscPrionrity	=(uint8_t)Arb_CtrlEscCtrlInfo.EscPrioReRi;   

}
static void LCARB_ReadHsaInfo(void)
{
	ARB_FL.lcarbu8GetHsaCntrMode	=(uint8_t)Arb_CtrlHsaCtrlInfo.HsaActFlg;      	
	ARB_FR.lcarbu8GetHsaCntrMode	=(uint8_t)Arb_CtrlHsaCtrlInfo.HsaActFlg;      	
	ARB_RL.lcarbu8GetHsaCntrMode	=(uint8_t)Arb_CtrlHsaCtrlInfo.HsaActFlg;      	
	ARB_RR.lcarbu8GetHsaCntrMode	=(uint8_t)Arb_CtrlHsaCtrlInfo.HsaActFlg;      	
	    
/*	 lcarbu8GetHsaDefectFlg		 =(uint8_t)Arb_CtrlHsaCtrlInfo.HsaDefectFlg; */
	ARB_FL.lcarbu16GetHsaTarP	=(uint16_t)Arb_CtrlHsaCtrlInfo.HsaTarP;      	
	ARB_FR.lcarbu16GetHsaTarP	=(uint16_t)Arb_CtrlHsaCtrlInfo.HsaTarP;      	
	ARB_RL.lcarbu16GetHsaTarP	=(uint16_t)Arb_CtrlHsaCtrlInfo.HsaTarP;      	
	ARB_RR.lcarbu16GetHsaTarP	=(uint16_t)Arb_CtrlHsaCtrlInfo.HsaTarP;      	

	ARB_FL.lcarbs16GetHsaDelTarP	=0;
    ARB_FR.lcarbs16GetHsaDelTarP	=0;
	ARB_RL.lcarbs16GetHsaDelTarP	=0;   
	ARB_RR.lcarbs16GetHsaDelTarP	=0;

}
static void LCARB_ReadSccInfo(void)
{
 	/*lcarbu8GetSccActFlg			 =(uint8_t)Arb_CtrlSccCtrlInfo.SccActFlg;        */
 	/*lcarbu8GetSccDefectFlg		 =(uint8_t)Arb_CtrlSccCtrlInfo.SccDefectFlg;     */
 	/*lcarbu16GetSccTarP		 	 =(uint16_t)Arb_CtrlSccCtrlInfo.SccTarP;     	 */
}
static void LCARB_ReadTcsInfo(void)
{
	lcarbu8GetTcsActFlg		 =(uint8_t)Arb_CtrlTcsCtrlInfo.TcsActFlg;                
/*	lcarbu8GetTcsDefectFlg	 =(uint8_t)Arb_CtrlTcsCtrlInfo.TcsDefectFlg;  */           
	ARB_FL.lcarbu16GetTcsTarP =(uint16_t)Arb_CtrlTcsCtrlInfo.TcsTarPFrntLe;            
	ARB_FR.lcarbu16GetTcsTarP =(uint16_t)Arb_CtrlTcsCtrlInfo.TcsTarPFrntRi;            
	ARB_RL.lcarbu16GetTcsTarP =(uint16_t)Arb_CtrlTcsCtrlInfo.TcsTarPReLe;              
	ARB_RR.lcarbu16GetTcsTarP =(uint16_t)Arb_CtrlTcsCtrlInfo.TcsTarPReRi;              
/*	lcarbstGetBothDrvgWhlBrkCtlReqFlg =(uint8_t)Arb_CtrlTcsCtrlInfo.BothDrvgWhlBrkCtlReqFlg;  */
			
	ARB_FL.lcarbs16GetTcsTarPRate	=(int16_t)Arb_CtrlTcsCtrlInfo.BrkTqGrdtReqFrntLeByWspc;  
	ARB_FR.lcarbs16GetTcsTarPRate	=(int16_t)Arb_CtrlTcsCtrlInfo.BrkTqGrdtReqFrntRiByWspc;  
	ARB_RL.lcarbs16GetTcsTarPRate	=(int16_t)Arb_CtrlTcsCtrlInfo.BrkTqGrdtReqReLeByWspc;
	ARB_RR.lcarbs16GetTcsTarPRate	=(int16_t)Arb_CtrlTcsCtrlInfo.BrkTqGrdtReqReRiByWspc;

	ARB_FL.lcarbu8GetTcsCntrMode	=(uint8_t)Arb_CtrlTcsCtrlInfo.BrkCtrlFctFrntLeByWspc;
	ARB_FR.lcarbu8GetTcsCntrMode	=(uint8_t)Arb_CtrlTcsCtrlInfo.BrkCtrlFctFrntRiByWspc;
	ARB_RL.lcarbu8GetTcsCntrMode	=(uint8_t)Arb_CtrlTcsCtrlInfo.BrkCtrlFctReLeByWspc;
	ARB_RR.lcarbu8GetTcsCntrMode	=(uint8_t)Arb_CtrlTcsCtrlInfo.BrkCtrlFctReRiByWspc;

	ARB_FL.lcarbu8GetTcsPrionrity	=0;/*(uint8_t)Arb_CtrlTcsCtrlInfo.XXXXXXXXXXXXXXX; */   
	ARB_FR.lcarbu8GetTcsPrionrity	=0;/*(uint8_t)Arb_CtrlTcsCtrlInfo.XXXXXXXXXXXXXXX; */  
	ARB_RL.lcarbu8GetTcsPrionrity	=0;/*(uint8_t)Arb_CtrlTcsCtrlInfo.XXXXXXXXXXXXXXX; */  
	ARB_RR.lcarbu8GetTcsPrionrity	=0;/*(uint8_t)Arb_CtrlTcsCtrlInfo.XXXXXXXXXXXXXXX; */  

	ARB_FL.lcarbs16GetTcsDelTarP	=(int16_t)Arb_CtrlTcsCtrlInfo.TcsDelTarPFrntLe;  
    ARB_FR.lcarbs16GetTcsDelTarP	=(int16_t)Arb_CtrlTcsCtrlInfo.TcsDelTarPFrntRi;  
	ARB_RL.lcarbs16GetTcsDelTarP	=(int16_t)Arb_CtrlTcsCtrlInfo.TcsDelTarPReLe;       
	ARB_RR.lcarbs16GetTcsDelTarP	=(int16_t)Arb_CtrlTcsCtrlInfo.TcsDelTarPReRi;    
}
static void LCARB_ReadTvbbInfo(void)
{
	 /*lcarbu8GetTvbbActFlg		 =(uint8_t)Arb_CtrlTvbbCtrlInfo.TvbbActFlg;          */
	 /*lcarbu8GetTvbbDefectFlg	 =(uint8_t)Arb_CtrlTvbbCtrlInfo.TvbbDefectFlg;       */
	 /*lcarbu16GetTvbbTarP		 =(uint16_t)Arb_CtrlTvbbCtrlInfo.TvbbTarP;      	 */
}
static void LCARB_ReadBbcDataInfo(void)
{
 /*	lcarbu8GetStandStillFlg =(uint8_t)Arb_CtrlBaseBrkCtrlModInfo.VehStandStillStFlg; */
/*lcarbu8GetEcuModeSts = Arb_CtrlEcuModeSts;*/
/*	lcarbu8GetIgnOnOffSts = Arb_CtrlIgnOnOffSts;*/
/*    lcarbu8GetFuncInhibitArbiSts =(uint8_t) Arb_CtrlFuncInhibitArbiSts;*/
/*    lcarbu8GetBaseBrkCtrlrActFlg 	=(uint8_t)Arb_CtrlBaseBrkCtrlrActFlg;*/
/*    lcarbu16GetRednForBaseBrkCtrl 	=(uint16_t)Arb_CtrlBrkPRednForBaseBrkCtrlr;*/ /* rbc reduction */
/*    lcarbu8GetPCtrlSt			 	=(uint8_t)Arb_CtrlPCtrlSt;*/
/*    lcarbu8GetRgnBrkCtrlrActStFlg 	=(uint8_t)Arb_CtrlRgnBrkCtrlrActStFlg;*//*rbc act flag*/
/*    lcarbu16GetTarPFromBaseBrkCtrlr =(uint16_t)Arb_CtrlTarPFromBaseBrkCtrlr;*/ /* Base brake tar*/
/*    lcarbu16GetTarPRateFromBaseBrk 	=(uint16_t)Arb_CtrlTarPRateFromBaseBrkCtrlr;*/ /*DRIVER target rate*/
/*    lcarbu8GetCtrlVehSpdFild = Arb_CtrlVehSpdFild;*/
	ARB_FL.lcarbu16GetBbcTarP= (uint16_t)Arb_CtrlTarPFromBaseBrkCtrlr;     
	ARB_FR.lcarbu16GetBbcTarP= (uint16_t)Arb_CtrlTarPFromBaseBrkCtrlr;     
	ARB_RL.lcarbu16GetBbcTarP= (uint16_t)Arb_CtrlTarPFromBaseBrkCtrlr;     
	ARB_RR.lcarbu16GetBbcTarP= (uint16_t)Arb_CtrlTarPFromBaseBrkCtrlr;     
	ARB_FL.lcarbs16GetBbcTarPRate= (int16_t)Arb_CtrlTarPRateFromBaseBrkCtrlr; 
	ARB_FR.lcarbs16GetBbcTarPRate= (int16_t)Arb_CtrlTarPRateFromBaseBrkCtrlr; 
	ARB_RL.lcarbs16GetBbcTarPRate=(int16_t) Arb_CtrlTarPRateFromBaseBrkCtrlr; 
	ARB_RR.lcarbs16GetBbcTarPRate=(int16_t) Arb_CtrlTarPRateFromBaseBrkCtrlr; 
	if(Arb_CtrlBaseBrkCtrlrActFlg > 0)
	{
		ARB_FL.lcarbu8GetBbcCntrMode= lcarbu8CntrModeBbc;  
		ARB_FR.lcarbu8GetBbcCntrMode= lcarbu8CntrModeBbc;  
		ARB_RL.lcarbu8GetBbcCntrMode= lcarbu8CntrModeBbc;  
		ARB_RR.lcarbu8GetBbcCntrMode= lcarbu8CntrModeBbc; 
	}
	else
	{
		;	
	} 
	ARB_FL.lcarbu8GetBbcPrionrity= 0; 
	ARB_FR.lcarbu8GetBbcPrionrity= 0; 
	ARB_RL.lcarbu8GetBbcPrionrity= 0; 
	ARB_RR.lcarbu8GetBbcPrionrity= 0; 
	lcArbu8BbcCtrlSt= (uint8_t)Arb_CtrlBus.Arb_CtrlBBCCtrlInfo.BBCCtrlSt;

} 
static void LCARB_ReadOutletValveInfo(void)
{
	static U8_BIT_STRUCT_t lcArbCntrlFlg;  
	#define lcArbOutValve5mchck  					lcArbCntrlFlg.bit0

	if(lcArbOutValve5mchck == 0)
	{	
		for(i=0;i<5;i++) 
		{
			lcArbAr5FlOutVlaCtrlTar[i] =Arb_CtrlWhlVlvReqAbcInfo.FlOvReqData[i];
			lcArbAr5FrOutVlaCtrlTar[i] =Arb_CtrlWhlVlvReqAbcInfo.FrOvReqData[i];
			lcArbAr5RlOutVlaCtrlTar[i] =Arb_CtrlWhlVlvReqAbcInfo.RlOvReqData[i];
			lcArbAr5RrOutVlaCtrlTar[i] =Arb_CtrlWhlVlvReqAbcInfo.RrOvReqData[i];
		}
		lcArbOutValve5mchck = 1;
	}
	else
	{
		for(i=0;i<5;i++) 
		{
			lcArbAr5FlOutVlaCtrlTar[i] =Arb_CtrlWhlVlvReqAbcInfo.FlOvReqData[i+5];
			lcArbAr5FrOutVlaCtrlTar[i] =Arb_CtrlWhlVlvReqAbcInfo.FrOvReqData[i+5];
			lcArbAr5RlOutVlaCtrlTar[i] =Arb_CtrlWhlVlvReqAbcInfo.RlOvReqData[i+5];
			lcArbAr5RrOutVlaCtrlTar[i] =Arb_CtrlWhlVlvReqAbcInfo.RrOvReqData[i+5];
		}
		lcArbOutValve5mchck = 0;
	}		
}
static void LCARB_SetCtrlMode(ARB_struct *ARB_WL)
{
	if(ARB_WL->lcarbu8GetEscCntrMode == 1)/*Oversteer */
	{
		ARB_WL->lcarbu16SetTP = ARB_WL->lcarbu16GetEscTarP;
		ARB_WL->lcarbs16SetTPRate = ARB_WL->lcarbs16GetEscTarPRate;
		ARB_WL->lcarbu8SetCntrMode = lcarbu8CntrModeEscOver;
		ARB_WL->lcarbu8SetPrionrity = ARB_WL->lcarbu8GetEscPrionrity;		
		ARB_WL->lcarbs16SetDelTP = ARB_WL->lcarbs16GetEscDelTarP;		
	}
	else if(ARB_WL->lcarbu8GetEscCntrMode == 2)/*Understeer */
	{
		if(ARB_WL->lcarbu16GetAbsTarP > ARB_WL->lcarbu16GetEscTarP)
		{
			ARB_WL->lcarbu16SetTP = ARB_WL->lcarbu16GetAbsTarP;
			ARB_WL->lcarbs16SetTPRate = ARB_WL->lcarbs16GetAbsTarPRate;
			ARB_WL->lcarbu8SetCntrMode = lcarbu8CntrModeAbs;
			ARB_WL->lcarbu8SetPrionrity = ARB_WL->lcarbu8GetAbsPrionrity;				
			ARB_WL->lcarbs16SetDelTP = ARB_WL->lcarbs16GetAbsDelTarP;		
		}
		else
		{
			ARB_WL->lcarbu16SetTP = ARB_WL->lcarbu16GetEscTarP;
			ARB_WL->lcarbs16SetTPRate = ARB_WL->lcarbs16GetEscTarPRate;
			ARB_WL->lcarbu8SetCntrMode = lcarbu8CntrModeEscUnder;
			ARB_WL->lcarbu8SetPrionrity = ARB_WL->lcarbu8GetEscPrionrity;			
			ARB_WL->lcarbs16SetDelTP = ARB_WL->lcarbs16GetEscDelTarP;	
		}	
	}
	else if(ARB_WL->lcarbu8GetEscCntrMode == 3)/*Cross */
	{
		LCARB_SetCtrlModeEscCross(ARB_WL);
	}
	else if(ARB_WL->lcarbu8GetEscCntrMode == 4)/*Other */
	{
		LCARB_SetCtrlModeEscOhter(ARB_WL);
	}	
	else
	{
		LCARB_SetCtrlModeWoEsc(ARB_WL);
	}		
}
static void LCARB_SetCtrlModeEscCross(ARB_struct *ARB_WL)
{
	if(ARB_WL->lcarbu8GetAbsCntrMode > 0 )
	{
		ARB_WL->lcarbu16SetTP = ARB_WL->lcarbu16GetAbsTarP;
		ARB_WL->lcarbs16SetTPRate = ARB_WL->lcarbs16GetAbsTarPRate;
		ARB_WL->lcarbu8SetCntrMode = lcarbu8CntrModeAbs;
		ARB_WL->lcarbu8SetPrionrity = ARB_WL->lcarbu8GetAbsPrionrity;				
		ARB_WL->lcarbs16SetDelTP = ARB_WL->lcarbs16GetAbsDelTarP;	
	}
	else if(ARB_WL->lcarbu8GetEbdCntrMode > 0 )
	{
		ARB_WL->lcarbu16SetTP = ARB_WL->lcarbu16GetEbdTarP;
		ARB_WL->lcarbs16SetTPRate = ARB_WL->lcarbs16GetEbdTarPRate;
		ARB_WL->lcarbu8SetCntrMode = lcarbu8CntrModeEbd + ARB_WL->lcarbu8GetEbdCntrMode;
		ARB_WL->lcarbu8SetPrionrity = ARB_WL->lcarbu8GetEbdPrionrity;				
		ARB_WL->lcarbs16SetDelTP = ARB_WL->lcarbs16GetEbdDelTarP;	
	}
	else if(ARB_WL->lcarbu8GetTcsCntrMode > 0 )
	{
		if(ARB_WL->lcarbu16GetTcsTarP > ARB_WL->lcarbu16GetEscTarP)
		{
			ARB_WL->lcarbu16SetTP = ARB_WL->lcarbu16GetTcsTarP;
			ARB_WL->lcarbs16SetTPRate = ARB_WL->lcarbs16GetTcsTarPRate;
			ARB_WL->lcarbu8SetCntrMode = lcarbu8CntrModeTcs;
			ARB_WL->lcarbu8SetPrionrity = ARB_WL->lcarbu8GetTcsPrionrity;				
			ARB_WL->lcarbs16SetDelTP = ARB_WL->lcarbs16GetTcsDelTarP;	
		}
		else
		{
			ARB_WL->lcarbu16SetTP = ARB_WL->lcarbu16GetEscTarP;
			ARB_WL->lcarbs16SetTPRate = ARB_WL->lcarbs16GetEscTarPRate;
			ARB_WL->lcarbu8SetCntrMode =lcarbu8CntrModeEscCross;
			ARB_WL->lcarbu8SetPrionrity = ARB_WL->lcarbu8GetEscPrionrity;			
			ARB_WL->lcarbs16SetDelTP = ARB_WL->lcarbs16GetEscDelTarP;	
		}				
	}		
	else
	{
		ARB_WL->lcarbu16SetTP = ARB_WL->lcarbu16GetEscTarP;
		ARB_WL->lcarbs16SetTPRate = ARB_WL->lcarbs16GetEscTarPRate;
		ARB_WL->lcarbu8SetCntrMode = lcarbu8CntrModeEscCross;
		ARB_WL->lcarbu8SetPrionrity = ARB_WL->lcarbu8GetEscPrionrity;			
		ARB_WL->lcarbs16SetDelTP = ARB_WL->lcarbs16GetEscDelTarP;	
	}			
}
static void LCARB_SetCtrlModeEscOhter(ARB_struct *ARB_WL)
{
	if(ARB_WL->lcarbu8GetAbsCntrMode > 0 )
	{
		ARB_WL->lcarbu16SetTP = ARB_WL->lcarbu16GetAbsTarP;
		ARB_WL->lcarbs16SetTPRate = ARB_WL->lcarbs16GetAbsTarPRate;
		ARB_WL->lcarbu8SetCntrMode =lcarbu8CntrModeAbs;
		ARB_WL->lcarbu8SetPrionrity = ARB_WL->lcarbu8GetAbsPrionrity;				
		ARB_WL->lcarbs16SetDelTP = ARB_WL->lcarbs16GetAbsDelTarP;	
	}
	else if(ARB_WL->lcarbu8GetEbdCntrMode > 0 )
	{
		ARB_WL->lcarbu16SetTP = ARB_WL->lcarbu16GetEbdTarP;
		ARB_WL->lcarbs16SetTPRate = ARB_WL->lcarbs16GetEbdTarPRate;
		ARB_WL->lcarbu8SetCntrMode = lcarbu8CntrModeEbd + ARB_WL->lcarbu8GetEbdCntrMode;
		ARB_WL->lcarbu8SetPrionrity = ARB_WL->lcarbu8GetEbdPrionrity;				
		ARB_WL->lcarbs16SetDelTP = ARB_WL->lcarbs16GetEbdDelTarP;	
	}
	else if(ARB_WL->lcarbu8GetTcsCntrMode > 0 )
	{
		if(ARB_WL->lcarbu16GetTcsTarP > ARB_WL->lcarbu16GetEscTarP)
		{
			ARB_WL->lcarbu16SetTP = ARB_WL->lcarbu16GetTcsTarP;
			ARB_WL->lcarbs16SetTPRate = ARB_WL->lcarbs16GetTcsTarPRate;
			ARB_WL->lcarbu8SetCntrMode = lcarbu8CntrModeTcs;
			ARB_WL->lcarbu8SetPrionrity = ARB_WL->lcarbu8GetTcsPrionrity;				
			ARB_WL->lcarbs16SetDelTP = ARB_WL->lcarbs16GetTcsDelTarP;	
		}
		else
		{
			ARB_WL->lcarbu16SetTP = ARB_WL->lcarbu16GetEscTarP;
			ARB_WL->lcarbs16SetTPRate = ARB_WL->lcarbs16GetEscTarPRate;
			ARB_WL->lcarbu8SetCntrMode = lcarbu8CntrModeEscOther;
			ARB_WL->lcarbu8SetPrionrity = ARB_WL->lcarbu8GetEscPrionrity;			
			ARB_WL->lcarbs16SetDelTP = ARB_WL->lcarbs16GetEscDelTarP;		
		}				
	}		
	else
	{
		ARB_WL->lcarbu16SetTP = ARB_WL->lcarbu16GetEscTarP;
		ARB_WL->lcarbs16SetTPRate = ARB_WL->lcarbs16GetEscTarPRate;
		ARB_WL->lcarbu8SetCntrMode = lcarbu8CntrModeEscOther;
		ARB_WL->lcarbu8SetPrionrity = ARB_WL->lcarbu8GetEscPrionrity;			
		ARB_WL->lcarbs16SetDelTP = ARB_WL->lcarbs16GetEscDelTarP;	

	}				
}
static void LCARB_SetCtrlModeWoEsc(ARB_struct *ARB_WL)
{
	if(ARB_WL->lcarbu8GetAbsCntrMode > 0 )
	{
		ARB_WL->lcarbu16SetTP = ARB_WL->lcarbu16GetAbsTarP;
		ARB_WL->lcarbs16SetTPRate = ARB_WL->lcarbs16GetAbsTarPRate;
		ARB_WL->lcarbu8SetCntrMode = lcarbu8CntrModeAbs;
		ARB_WL->lcarbu8SetPrionrity = ARB_WL->lcarbu8GetAbsPrionrity;				
		ARB_WL->lcarbs16SetDelTP = ARB_WL->lcarbs16GetAbsDelTarP;	
	}
	else if(ARB_WL->lcarbu8GetEbdCntrMode > 0 )
	{
		ARB_WL->lcarbu16SetTP = ARB_WL->lcarbu16GetEbdTarP;
		ARB_WL->lcarbs16SetTPRate = ARB_WL->lcarbs16GetEbdTarPRate;
		ARB_WL->lcarbu8SetCntrMode = lcarbu8CntrModeEbd + ARB_WL->lcarbu8GetEbdCntrMode;
		ARB_WL->lcarbu8SetPrionrity = ARB_WL->lcarbu8GetEbdPrionrity;				
		ARB_WL->lcarbs16SetDelTP = ARB_WL->lcarbs16GetEbdDelTarP;	
	}
	else if(ARB_WL->lcarbu8GetTcsCntrMode > 0 )
	{					
		ARB_WL->lcarbu16SetTP = ARB_WL->lcarbu16GetTcsTarP;
		ARB_WL->lcarbs16SetTPRate = ARB_WL->lcarbs16GetTcsTarPRate;
		ARB_WL->lcarbu8SetCntrMode = lcarbu8CntrModeTcs;
		ARB_WL->lcarbu8SetPrionrity = ARB_WL->lcarbu8GetTcsPrionrity;				
		ARB_WL->lcarbs16SetDelTP = ARB_WL->lcarbs16GetTcsDelTarP;	
	}	
	else
	{
		LCARB_SetCtrlModeWoEsc2(ARB_WL);
	}
}
static void LCARB_SetCtrlModeWoEsc2(ARB_struct *ARB_WL)
{
	if((ARB_WL->lcarbu8GetHsaCntrMode > 0 ) && (ARB_WL->lcarbu8GetBbcCntrMode > 0 ) && (ARB_WL->lcarbu16GetHsaTarP > ARB_WL->lcarbu16GetBbcTarP))
	{
		ARB_WL->lcarbu16SetTP = ARB_WL->lcarbu16GetHsaTarP;
		ARB_WL->lcarbs16SetTPRate = ARB_WL->lcarbs16GetHsaTarPRate;
		ARB_WL->lcarbu8SetCntrMode = lcarbu8CntrModeHsa;
		ARB_WL->lcarbu8SetPrionrity = ARB_WL->lcarbu8GetHsaPrionrity;
		ARB_WL->lcarbs16SetDelTP = ARB_WL->lcarbs16GetHsaDelTarP;	

	}	
	else if((ARB_WL->lcarbu8GetHsaCntrMode > 0 ) && (ARB_WL->lcarbu8GetBbcCntrMode > 0 ) && (ARB_WL->lcarbu16GetBbcTarP > ARB_WL->lcarbu16GetHsaTarP))

	{
		ARB_WL->lcarbu16SetTP = ARB_WL->lcarbu16GetBbcTarP;
		ARB_WL->lcarbs16SetTPRate = ARB_WL->lcarbs16GetBbcTarPRate;
		ARB_WL->lcarbu8SetCntrMode = lcarbu8CntrModeBbc;
		ARB_WL->lcarbu8SetPrionrity = ARB_WL->lcarbu8GetBbcPrionrity;
		ARB_WL->lcarbs16SetDelTP = ARB_WL->lcarbs16GetBbcDelTarP;	
	}
	else if(ARB_WL->lcarbu8GetHsaCntrMode > 0 )
	{		
		ARB_WL->lcarbu16SetTP = ARB_WL->lcarbu16GetHsaTarP;
		ARB_WL->lcarbs16SetTPRate = ARB_WL->lcarbs16GetHsaTarPRate;
		ARB_WL->lcarbu8SetCntrMode = lcarbu8CntrModeHsa;
		ARB_WL->lcarbu8SetPrionrity = ARB_WL->lcarbu8GetHsaPrionrity;								
		ARB_WL->lcarbs16SetDelTP = ARB_WL->lcarbs16GetHsaDelTarP;
	}		
	else if(ARB_WL->lcarbu8GetBbcCntrMode > 0 )
	{
		ARB_WL->lcarbu16SetTP = ARB_WL->lcarbu16GetBbcTarP;
		ARB_WL->lcarbs16SetTPRate = ARB_WL->lcarbs16GetBbcTarPRate;
		ARB_WL->lcarbu8SetCntrMode = lcarbu8CntrModeBbc;
		ARB_WL->lcarbu8SetPrionrity = ARB_WL->lcarbu8GetBbcPrionrity;			
		ARB_WL->lcarbs16SetDelTP = ARB_WL->lcarbs16GetBbcDelTarP;	
	}
	else
	{
		;	
	}	
}
static void LCARB_SetBoostMode(void)
{
	if((ARB_FL.lcarbu8SetCntrMode == lcarbu8CntrModeAbs)||(ARB_FR.lcarbu8SetCntrMode == lcarbu8CntrModeAbs)||(ARB_RL.lcarbu8SetCntrMode == lcarbu8CntrModeAbs)||(ARB_RR.lcarbu8SetCntrMode == lcarbu8CntrModeAbs))
	{
		lcarbu8CntrModefourwh = S8_BOOST_MODE_REQ_L9;
	}	
	else if((ARB_FL.lcarbu8SetCntrMode == lcarbu8CntrModeEscOver)||(ARB_FR.lcarbu8SetCntrMode == lcarbu8CntrModeEscOver))
	{
		lcarbu8CntrModefourwh = S8_BOOST_MODE_REQ_L2;
	}	  
	else if((ARB_RL.lcarbu8SetCntrMode == lcarbu8CntrModeEscUnder)||(ARB_RR.lcarbu8SetCntrMode == lcarbu8CntrModeEscUnder))
	{
		lcarbu8CntrModefourwh = S8_BOOST_MODE_REQ_L3;	
	}	
	else if((ARB_RL.lcarbu8SetCntrMode == (lcarbu8CntrModeEbd + ARB_RL.lcarbu8GetEbdCntrMode))||(ARB_RR.lcarbu8SetCntrMode == (lcarbu8CntrModeEbd + ARB_RL.lcarbu8GetEbdCntrMode)))
	{
		lcarbu8CntrModefourwh = S8_BOOST_MODE_REQ_L6;	
	}
	else
	{
		LCARB_SetBoostModenotStbFunc();

	}
}
static void LCARB_SetBoostModenotStbFunc(void)
{	
	if((ARB_FL.lcarbu8SetCntrMode == lcarbu8CntrModeTcs)||(ARB_FR.lcarbu8SetCntrMode == lcarbu8CntrModeTcs)||(ARB_RL.lcarbu8SetCntrMode == lcarbu8CntrModeTcs)||(ARB_RR.lcarbu8SetCntrMode == lcarbu8CntrModeTcs))
	{
		LCARB_SetTcsBoostMode();
	}
	else if((ARB_FL.lcarbu8SetCntrMode == lcarbu8CntrModeHsa)||(ARB_FR.lcarbu8SetCntrMode == lcarbu8CntrModeHsa))
	{
		if( ARB_FL.lcarbu16SetTP >= lcarbu16FinalTPFrntLeOld)
		{
			lcarbu8CntrModefourwh = S8_BOOST_MODE_REQ_L7;	
		}
		else
		{
			lcarbu8CntrModefourwh = S8_BOOST_MODE_REQ_L8;	
		}
	}
	else if((ARB_FL.lcarbu8SetCntrMode == lcarbu8CntrModeBbc)||(ARB_FR.lcarbu8SetCntrMode == lcarbu8CntrModeBbc)||(ARB_RL.lcarbu8SetCntrMode == lcarbu8CntrModeBbc)||(ARB_RR.lcarbu8SetCntrMode == lcarbu8CntrModeBbc))
	{
		if(lcArbu8BbcCtrlSt == 2)
		{
			lcarbu8CntrModefourwh = S8_BOOST_MODE_REQ_L1;	
		}
		else
		{
			lcarbu8CntrModefourwh = S8_BOOST_MODE_REQ_L5;	
		}
	}
	else
	{
		lcarbu8CntrModefourwh = S8_BOOST_MODE_REQ_L5;	
	}	
}
static void LCARB_SetTcsBoostMode(void)
{
	if(lcarbu8GetTcsActFlg ==6)
	{
		lcarbu8CntrModefourwh = S8_BOOST_MODE_REQ_L7;
	}
	else
	{
		if(ARB_FL.lcarbu8SetCntrMode == lcarbu8CntrModeTcs)
		{
			lcarbu8CntrModefourwh = ARB_FL.lcarbu8GetTcsCntrMode;	
		}
		else if(ARB_FR.lcarbu8SetCntrMode == lcarbu8CntrModeTcs)
		{
			lcarbu8CntrModefourwh = ARB_FR.lcarbu8GetTcsCntrMode;
		}
		else if(ARB_RL.lcarbu8SetCntrMode == lcarbu8CntrModeTcs)
		{
			lcarbu8CntrModefourwh = ARB_RL.lcarbu8GetTcsCntrMode;	
		}
		else 
		{
			lcarbu8CntrModefourwh = ARB_RR.lcarbu8GetTcsCntrMode;
		}		
	}			
}
#define ARB_CTRL_STOP_SEC_CODE
#include "Arb_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
