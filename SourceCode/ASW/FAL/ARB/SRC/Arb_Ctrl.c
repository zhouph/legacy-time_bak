/**
 * @defgroup Arb_Ctrl Arb_Ctrl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Arb_Ctrl.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Arb_Ctrl.h"
#include "Arb_Ctrl_Ifa.h"
#include "IfxStm_reg.h"
#include "LCARBCallMain.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ARB_CTRL_START_SEC_CONST_UNSPECIFIED
#include "Arb_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ARB_CTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Arb_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ARB_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arb_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Arb_Ctrl_HdrBusType Arb_CtrlBus;

/* Version Info */
const SwcVersionInfo_t Arb_CtrlVersionInfo = 
{   
    ARB_CTRL_MODULE_ID,           /* Arb_CtrlVersionInfo.ModuleId */
    ARB_CTRL_MAJOR_VERSION,       /* Arb_CtrlVersionInfo.MajorVer */
    ARB_CTRL_MINOR_VERSION,       /* Arb_CtrlVersionInfo.MinorVer */
    ARB_CTRL_PATCH_VERSION,       /* Arb_CtrlVersionInfo.PatchVer */
    ARB_CTRL_BRANCH_VERSION       /* Arb_CtrlVersionInfo.BranchVer */
};
    
/* Input Data Element */
Abc_CtrlWhlVlvReqAbcInfo_t Arb_CtrlWhlVlvReqAbcInfo;
Abc_CtrlAbsCtrlInfo_t Arb_CtrlAbsCtrlInfo;
Abc_CtrlAvhCtrlInfo_t Arb_CtrlAvhCtrlInfo;
Abc_CtrlBaCtrlInfo_t Arb_CtrlBaCtrlInfo;
Abc_CtrlEbdCtrlInfo_t Arb_CtrlEbdCtrlInfo;
Abc_CtrlEbpCtrlInfo_t Arb_CtrlEbpCtrlInfo;
Abc_CtrlEpbiCtrlInfo_t Arb_CtrlEpbiCtrlInfo;
Abc_CtrlEscCtrlInfo_t Arb_CtrlEscCtrlInfo;
Abc_CtrlHsaCtrlInfo_t Arb_CtrlHsaCtrlInfo;
Abc_CtrlSccCtrlInfo_t Arb_CtrlSccCtrlInfo;
Abc_CtrlTcsCtrlInfo_t Arb_CtrlTcsCtrlInfo;
Abc_CtrlTvbbCtrlInfo_t Arb_CtrlTvbbCtrlInfo;
Bbc_CtrlBaseBrkCtrlModInfo_t Arb_CtrlBaseBrkCtrlModInfo;
Bbc_CtrlBBCCtrlInfo_t Arb_CtrlBBCCtrlInfo;
Mom_HndlrEcuModeSts_t Arb_CtrlEcuModeSts;
Prly_HndlrIgnOnOffSts_t Arb_CtrlIgnOnOffSts;
Eem_SuspcDetnFuncInhibitArbiSts_t Arb_CtrlFuncInhibitArbiSts;
Bbc_CtrlBaseBrkCtrlrActFlg_t Arb_CtrlBaseBrkCtrlrActFlg;
Rbc_CtrlBrkPRednForBaseBrkCtrlr_t Arb_CtrlBrkPRednForBaseBrkCtrlr;
Pct_5msCtrlPCtrlSt_t Arb_CtrlPCtrlSt;
Rbc_CtrlRgnBrkCtrlrActStFlg_t Arb_CtrlRgnBrkCtrlrActStFlg;
Bbc_CtrlTarPFromBaseBrkCtrlr_t Arb_CtrlTarPFromBaseBrkCtrlr;
Bbc_CtrlTarPRateFromBaseBrkCtrlr_t Arb_CtrlTarPRateFromBaseBrkCtrlr;
Det_5msCtrlVehSpdFild_t Arb_CtrlVehSpdFild;

/* Output Data Element */
Arb_CtrlFinalTarPInfo_t Arb_CtrlFinalTarPInfo;
Arb_CtrlWhlOutVlvCtrlTarInfo_t Arb_CtrlWhlOutVlvCtrlTarInfo;
Arb_CtrlWhlPreFillInfo_t Arb_CtrlWhlPreFillInfo;

uint32 Arb_Ctrl_Timer_Start;
uint32 Arb_Ctrl_Timer_Elapsed;

#define ARB_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arb_MemMap.h"
#define ARB_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Arb_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ARB_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Arb_MemMap.h"
#define ARB_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Arb_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ARB_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Arb_MemMap.h"
#define ARB_CTRL_START_SEC_VAR_32BIT
#include "Arb_MemMap.h"
/** Variable Section (32BIT)**/


#define ARB_CTRL_STOP_SEC_VAR_32BIT
#include "Arb_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ARB_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arb_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ARB_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arb_MemMap.h"
#define ARB_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Arb_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ARB_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Arb_MemMap.h"
#define ARB_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Arb_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ARB_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Arb_MemMap.h"
#define ARB_CTRL_START_SEC_VAR_32BIT
#include "Arb_MemMap.h"
/** Variable Section (32BIT)**/


#define ARB_CTRL_STOP_SEC_VAR_32BIT
#include "Arb_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ARB_CTRL_START_SEC_CODE
#include "Arb_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Arb_Ctrl_Init(void)
{
    uint16 i;

    /* Initialize internal bus */
    for(i=0;i<10;i++) Arb_CtrlBus.Arb_CtrlWhlVlvReqAbcInfo.FlOvReqData[i] = 0;   
    for(i=0;i<10;i++) Arb_CtrlBus.Arb_CtrlWhlVlvReqAbcInfo.FrOvReqData[i] = 0;   
    for(i=0;i<10;i++) Arb_CtrlBus.Arb_CtrlWhlVlvReqAbcInfo.RlOvReqData[i] = 0;   
    for(i=0;i<10;i++) Arb_CtrlBus.Arb_CtrlWhlVlvReqAbcInfo.RrOvReqData[i] = 0;   
    Arb_CtrlBus.Arb_CtrlWhlVlvReqAbcInfo.FlOvReq = 0;
    Arb_CtrlBus.Arb_CtrlWhlVlvReqAbcInfo.FrOvReq = 0;
    Arb_CtrlBus.Arb_CtrlWhlVlvReqAbcInfo.RlOvReq = 0;
    Arb_CtrlBus.Arb_CtrlWhlVlvReqAbcInfo.RrOvReq = 0;
    Arb_CtrlBus.Arb_CtrlAbsCtrlInfo.AbsActFlg = 0;
    Arb_CtrlBus.Arb_CtrlAbsCtrlInfo.AbsDefectFlg = 0;
    Arb_CtrlBus.Arb_CtrlAbsCtrlInfo.AbsTarPFrntLe = 0;
    Arb_CtrlBus.Arb_CtrlAbsCtrlInfo.AbsTarPFrntRi = 0;
    Arb_CtrlBus.Arb_CtrlAbsCtrlInfo.AbsTarPReLe = 0;
    Arb_CtrlBus.Arb_CtrlAbsCtrlInfo.AbsTarPReRi = 0;
    Arb_CtrlBus.Arb_CtrlAbsCtrlInfo.FrntWhlSlip = 0;
    Arb_CtrlBus.Arb_CtrlAbsCtrlInfo.AbsDesTarP = 0;
    Arb_CtrlBus.Arb_CtrlAbsCtrlInfo.AbsDesTarPReqFlg = 0;
    Arb_CtrlBus.Arb_CtrlAbsCtrlInfo.AbsCtrlFadeOutFlg = 0;
    Arb_CtrlBus.Arb_CtrlAbsCtrlInfo.AbsCtrlModeFrntLe = 0;
    Arb_CtrlBus.Arb_CtrlAbsCtrlInfo.AbsCtrlModeFrntRi = 0;
    Arb_CtrlBus.Arb_CtrlAbsCtrlInfo.AbsCtrlModeReLe = 0;
    Arb_CtrlBus.Arb_CtrlAbsCtrlInfo.AbsCtrlModeReRi = 0;
    Arb_CtrlBus.Arb_CtrlAbsCtrlInfo.AbsTarPRateFrntLe = 0;
    Arb_CtrlBus.Arb_CtrlAbsCtrlInfo.AbsTarPRateFrntRi = 0;
    Arb_CtrlBus.Arb_CtrlAbsCtrlInfo.AbsTarPRateReLe = 0;
    Arb_CtrlBus.Arb_CtrlAbsCtrlInfo.AbsTarPRateReRi = 0;
    Arb_CtrlBus.Arb_CtrlAbsCtrlInfo.AbsPrioFrntLe = 0;
    Arb_CtrlBus.Arb_CtrlAbsCtrlInfo.AbsPrioFrntRi = 0;
    Arb_CtrlBus.Arb_CtrlAbsCtrlInfo.AbsPrioReLe = 0;
    Arb_CtrlBus.Arb_CtrlAbsCtrlInfo.AbsPrioReRi = 0;
    Arb_CtrlBus.Arb_CtrlAbsCtrlInfo.AbsDelTarPFrntLe = 0;
    Arb_CtrlBus.Arb_CtrlAbsCtrlInfo.AbsDelTarPFrntRi = 0;
    Arb_CtrlBus.Arb_CtrlAbsCtrlInfo.AbsDelTarPReLe = 0;
    Arb_CtrlBus.Arb_CtrlAbsCtrlInfo.AbsDelTarPReRi = 0;
    Arb_CtrlBus.Arb_CtrlAvhCtrlInfo.AvhActFlg = 0;
    Arb_CtrlBus.Arb_CtrlAvhCtrlInfo.AvhDefectFlg = 0;
    Arb_CtrlBus.Arb_CtrlAvhCtrlInfo.AvhTarP = 0;
    Arb_CtrlBus.Arb_CtrlBaCtrlInfo.BaActFlg = 0;
    Arb_CtrlBus.Arb_CtrlBaCtrlInfo.BaCDefectFlg = 0;
    Arb_CtrlBus.Arb_CtrlBaCtrlInfo.BaTarP = 0;
    Arb_CtrlBus.Arb_CtrlEbdCtrlInfo.EbdActFlg = 0;
    Arb_CtrlBus.Arb_CtrlEbdCtrlInfo.EbdDefectFlg = 0;
    Arb_CtrlBus.Arb_CtrlEbdCtrlInfo.EbdTarPFrntLe = 0;
    Arb_CtrlBus.Arb_CtrlEbdCtrlInfo.EbdTarPFrntRi = 0;
    Arb_CtrlBus.Arb_CtrlEbdCtrlInfo.EbdTarPReLe = 0;
    Arb_CtrlBus.Arb_CtrlEbdCtrlInfo.EbdTarPReRi = 0;
    Arb_CtrlBus.Arb_CtrlEbdCtrlInfo.EbdTarPRateReLe = 0;
    Arb_CtrlBus.Arb_CtrlEbdCtrlInfo.EbdTarPRateReRi = 0;
    Arb_CtrlBus.Arb_CtrlEbdCtrlInfo.EbdCtrlModeReLe = 0;
    Arb_CtrlBus.Arb_CtrlEbdCtrlInfo.EbdCtrlModeReRi = 0;
    Arb_CtrlBus.Arb_CtrlEbdCtrlInfo.EbdDelTarPReLe = 0;
    Arb_CtrlBus.Arb_CtrlEbdCtrlInfo.EbdDelTarPReRi = 0;
    Arb_CtrlBus.Arb_CtrlEbpCtrlInfo.EbpActFlg = 0;
    Arb_CtrlBus.Arb_CtrlEbpCtrlInfo.EbpDefectFlg = 0;
    Arb_CtrlBus.Arb_CtrlEbpCtrlInfo.EbpTarP = 0;
    Arb_CtrlBus.Arb_CtrlEpbiCtrlInfo.EpbiActFlg = 0;
    Arb_CtrlBus.Arb_CtrlEpbiCtrlInfo.EpbiDefectFlg = 0;
    Arb_CtrlBus.Arb_CtrlEpbiCtrlInfo.EpbiTarP = 0;
    Arb_CtrlBus.Arb_CtrlEscCtrlInfo.EscActFlg = 0;
    Arb_CtrlBus.Arb_CtrlEscCtrlInfo.EscDefectFlg = 0;
    Arb_CtrlBus.Arb_CtrlEscCtrlInfo.EscTarPFrntLe = 0;
    Arb_CtrlBus.Arb_CtrlEscCtrlInfo.EscTarPFrntRi = 0;
    Arb_CtrlBus.Arb_CtrlEscCtrlInfo.EscTarPReLe = 0;
    Arb_CtrlBus.Arb_CtrlEscCtrlInfo.EscTarPReRi = 0;
    Arb_CtrlBus.Arb_CtrlEscCtrlInfo.EscCtrlModeFrntLe = 0;
    Arb_CtrlBus.Arb_CtrlEscCtrlInfo.EscCtrlModeFrntRi = 0;
    Arb_CtrlBus.Arb_CtrlEscCtrlInfo.EscCtrlModeReLe = 0;
    Arb_CtrlBus.Arb_CtrlEscCtrlInfo.EscCtrlModeReRi = 0;
    Arb_CtrlBus.Arb_CtrlEscCtrlInfo.EscTarPRateFrntLe = 0;
    Arb_CtrlBus.Arb_CtrlEscCtrlInfo.EscTarPRateFrntRi = 0;
    Arb_CtrlBus.Arb_CtrlEscCtrlInfo.EscTarPRateReLe = 0;
    Arb_CtrlBus.Arb_CtrlEscCtrlInfo.EscTarPRateReRi = 0;
    Arb_CtrlBus.Arb_CtrlEscCtrlInfo.EscPrioFrntLe = 0;
    Arb_CtrlBus.Arb_CtrlEscCtrlInfo.EscPrioFrntRi = 0;
    Arb_CtrlBus.Arb_CtrlEscCtrlInfo.EscPrioReLe = 0;
    Arb_CtrlBus.Arb_CtrlEscCtrlInfo.EscPrioReRi = 0;
    Arb_CtrlBus.Arb_CtrlEscCtrlInfo.EscPreCtrlModeFrntLe = 0;
    Arb_CtrlBus.Arb_CtrlEscCtrlInfo.EscPreCtrlModeFrntRi = 0;
    Arb_CtrlBus.Arb_CtrlEscCtrlInfo.EscPreCtrlModeReLe = 0;
    Arb_CtrlBus.Arb_CtrlEscCtrlInfo.EscPreCtrlModeReRi = 0;
    Arb_CtrlBus.Arb_CtrlEscCtrlInfo.EscDelTarPFrntLe = 0;
    Arb_CtrlBus.Arb_CtrlEscCtrlInfo.EscDelTarPFrntRi = 0;
    Arb_CtrlBus.Arb_CtrlEscCtrlInfo.EscDelTarPReLe = 0;
    Arb_CtrlBus.Arb_CtrlEscCtrlInfo.EscDelTarPReRi = 0;
    Arb_CtrlBus.Arb_CtrlHsaCtrlInfo.HsaActFlg = 0;
    Arb_CtrlBus.Arb_CtrlHsaCtrlInfo.HsaDefectFlg = 0;
    Arb_CtrlBus.Arb_CtrlHsaCtrlInfo.HsaTarP = 0;
    Arb_CtrlBus.Arb_CtrlSccCtrlInfo.SccActFlg = 0;
    Arb_CtrlBus.Arb_CtrlSccCtrlInfo.SccDefectFlg = 0;
    Arb_CtrlBus.Arb_CtrlSccCtrlInfo.SccTarP = 0;
    Arb_CtrlBus.Arb_CtrlTcsCtrlInfo.TcsActFlg = 0;
    Arb_CtrlBus.Arb_CtrlTcsCtrlInfo.TcsDefectFlg = 0;
    Arb_CtrlBus.Arb_CtrlTcsCtrlInfo.TcsTarPFrntLe = 0;
    Arb_CtrlBus.Arb_CtrlTcsCtrlInfo.TcsTarPFrntRi = 0;
    Arb_CtrlBus.Arb_CtrlTcsCtrlInfo.TcsTarPReLe = 0;
    Arb_CtrlBus.Arb_CtrlTcsCtrlInfo.TcsTarPReRi = 0;
    Arb_CtrlBus.Arb_CtrlTcsCtrlInfo.BothDrvgWhlBrkCtlReqFlg = 0;
    Arb_CtrlBus.Arb_CtrlTcsCtrlInfo.BrkCtrlFctFrntLeByWspc = 0;
    Arb_CtrlBus.Arb_CtrlTcsCtrlInfo.BrkCtrlFctFrntRiByWspc = 0;
    Arb_CtrlBus.Arb_CtrlTcsCtrlInfo.BrkCtrlFctReLeByWspc = 0;
    Arb_CtrlBus.Arb_CtrlTcsCtrlInfo.BrkCtrlFctReRiByWspc = 0;
    Arb_CtrlBus.Arb_CtrlTcsCtrlInfo.BrkTqGrdtReqFrntLeByWspc = 0;
    Arb_CtrlBus.Arb_CtrlTcsCtrlInfo.BrkTqGrdtReqFrntRiByWspc = 0;
    Arb_CtrlBus.Arb_CtrlTcsCtrlInfo.BrkTqGrdtReqReLeByWspc = 0;
    Arb_CtrlBus.Arb_CtrlTcsCtrlInfo.BrkTqGrdtReqReRiByWspc = 0;
    Arb_CtrlBus.Arb_CtrlTcsCtrlInfo.TcsDelTarPFrntLe = 0;
    Arb_CtrlBus.Arb_CtrlTcsCtrlInfo.TcsDelTarPFrntRi = 0;
    Arb_CtrlBus.Arb_CtrlTcsCtrlInfo.TcsDelTarPReLe = 0;
    Arb_CtrlBus.Arb_CtrlTcsCtrlInfo.TcsDelTarPReRi = 0;
    Arb_CtrlBus.Arb_CtrlTvbbCtrlInfo.TvbbActFlg = 0;
    Arb_CtrlBus.Arb_CtrlTvbbCtrlInfo.TvbbDefectFlg = 0;
    Arb_CtrlBus.Arb_CtrlTvbbCtrlInfo.TvbbTarP = 0;
    Arb_CtrlBus.Arb_CtrlBaseBrkCtrlModInfo.VehStandStillStFlg = 0;
    Arb_CtrlBus.Arb_CtrlBBCCtrlInfo.BBCCtrlSt = 0;
    Arb_CtrlBus.Arb_CtrlEcuModeSts = 0;
    Arb_CtrlBus.Arb_CtrlIgnOnOffSts = 0;
    Arb_CtrlBus.Arb_CtrlFuncInhibitArbiSts = 0;
    Arb_CtrlBus.Arb_CtrlBaseBrkCtrlrActFlg = 0;
    Arb_CtrlBus.Arb_CtrlBrkPRednForBaseBrkCtrlr = 0;
    Arb_CtrlBus.Arb_CtrlPCtrlSt = 0;
    Arb_CtrlBus.Arb_CtrlRgnBrkCtrlrActStFlg = 0;
    Arb_CtrlBus.Arb_CtrlTarPFromBaseBrkCtrlr = 0;
    Arb_CtrlBus.Arb_CtrlTarPRateFromBaseBrkCtrlr = 0;
    Arb_CtrlBus.Arb_CtrlVehSpdFild = 0;
    Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalTarPOfWhlFrntLe = 0;
    Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalTarPOfWhlFrntRi = 0;
    Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalTarPOfWhlReLe = 0;
    Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalTarPOfWhlReRi = 0;
    Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalTarPRateOfWhlFrntLe = 0;
    Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalTarPRateOfWhlFrntRi = 0;
    Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalTarPRateOfWhlReLe = 0;
    Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalTarPRateOfWhlReRi = 0;
    Arb_CtrlBus.Arb_CtrlFinalTarPInfo.PCtrlPrioOfWhlFrntLe = 0;
    Arb_CtrlBus.Arb_CtrlFinalTarPInfo.PCtrlPrioOfWhlFrntRi = 0;
    Arb_CtrlBus.Arb_CtrlFinalTarPInfo.PCtrlPrioOfWhlReLe = 0;
    Arb_CtrlBus.Arb_CtrlFinalTarPInfo.PCtrlPrioOfWhlReRi = 0;
    Arb_CtrlBus.Arb_CtrlFinalTarPInfo.CtrlModOfWhlFrntLe = 0;
    Arb_CtrlBus.Arb_CtrlFinalTarPInfo.CtrlModOfOfWhlFrntRi = 0;
    Arb_CtrlBus.Arb_CtrlFinalTarPInfo.CtrlModOfWhlReLe = 0;
    Arb_CtrlBus.Arb_CtrlFinalTarPInfo.CtrlModOfWhlReRi = 0;
    Arb_CtrlBus.Arb_CtrlFinalTarPInfo.ReqdPCtrlBoostMod = 0;
    Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalDelTarPOfWhlFrntLe = 0;
    Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalDelTarPOfWhlFrntRi = 0;
    Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalDelTarPOfWhlReLe = 0;
    Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalDelTarPOfWhlReRi = 0;
    Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalMaxCircuitTp = 0;
    for(i=0;i<5;i++) Arb_CtrlBus.Arb_CtrlWhlOutVlvCtrlTarInfo.FlOutVlvCtrlTar[i] = 0;   
    for(i=0;i<5;i++) Arb_CtrlBus.Arb_CtrlWhlOutVlvCtrlTarInfo.FrOutVlvCtrlTar[i] = 0;   
    for(i=0;i<5;i++) Arb_CtrlBus.Arb_CtrlWhlOutVlvCtrlTarInfo.RlOutVlvCtrlTar[i] = 0;   
    for(i=0;i<5;i++) Arb_CtrlBus.Arb_CtrlWhlOutVlvCtrlTarInfo.RrOutVlvCtrlTar[i] = 0;   
    Arb_CtrlBus.Arb_CtrlWhlPreFillInfo.FlPreFillActFlg = 0;
    Arb_CtrlBus.Arb_CtrlWhlPreFillInfo.FrPreFillActFlg = 0;
    Arb_CtrlBus.Arb_CtrlWhlPreFillInfo.RlPreFillActFlg = 0;
    Arb_CtrlBus.Arb_CtrlWhlPreFillInfo.RrPreFillActFlg = 0;
}

void Arb_Ctrl(void)
{
    uint16 i;
    
    Arb_Ctrl_Timer_Start = STM0_TIM0.U;

    /* Input */
    /* Decomposed structure interface */
    Arb_Ctrl_Read_Arb_CtrlWhlVlvReqAbcInfo_FlOvReqData(&Arb_CtrlBus.Arb_CtrlWhlVlvReqAbcInfo.FlOvReqData);
    Arb_Ctrl_Read_Arb_CtrlWhlVlvReqAbcInfo_FrOvReqData(&Arb_CtrlBus.Arb_CtrlWhlVlvReqAbcInfo.FrOvReqData);
    Arb_Ctrl_Read_Arb_CtrlWhlVlvReqAbcInfo_RlOvReqData(&Arb_CtrlBus.Arb_CtrlWhlVlvReqAbcInfo.RlOvReqData);
    Arb_Ctrl_Read_Arb_CtrlWhlVlvReqAbcInfo_RrOvReqData(&Arb_CtrlBus.Arb_CtrlWhlVlvReqAbcInfo.RrOvReqData);
    Arb_Ctrl_Read_Arb_CtrlWhlVlvReqAbcInfo_FlOvReq(&Arb_CtrlBus.Arb_CtrlWhlVlvReqAbcInfo.FlOvReq);
    Arb_Ctrl_Read_Arb_CtrlWhlVlvReqAbcInfo_FrOvReq(&Arb_CtrlBus.Arb_CtrlWhlVlvReqAbcInfo.FrOvReq);
    Arb_Ctrl_Read_Arb_CtrlWhlVlvReqAbcInfo_RlOvReq(&Arb_CtrlBus.Arb_CtrlWhlVlvReqAbcInfo.RlOvReq);
    Arb_Ctrl_Read_Arb_CtrlWhlVlvReqAbcInfo_RrOvReq(&Arb_CtrlBus.Arb_CtrlWhlVlvReqAbcInfo.RrOvReq);

    Arb_Ctrl_Read_Arb_CtrlAbsCtrlInfo(&Arb_CtrlBus.Arb_CtrlAbsCtrlInfo);
    /*==============================================================================
    * Members of structure Arb_CtrlAbsCtrlInfo 
     : Arb_CtrlAbsCtrlInfo.AbsActFlg;
     : Arb_CtrlAbsCtrlInfo.AbsDefectFlg;
     : Arb_CtrlAbsCtrlInfo.AbsTarPFrntLe;
     : Arb_CtrlAbsCtrlInfo.AbsTarPFrntRi;
     : Arb_CtrlAbsCtrlInfo.AbsTarPReLe;
     : Arb_CtrlAbsCtrlInfo.AbsTarPReRi;
     : Arb_CtrlAbsCtrlInfo.FrntWhlSlip;
     : Arb_CtrlAbsCtrlInfo.AbsDesTarP;
     : Arb_CtrlAbsCtrlInfo.AbsDesTarPReqFlg;
     : Arb_CtrlAbsCtrlInfo.AbsCtrlFadeOutFlg;
     : Arb_CtrlAbsCtrlInfo.AbsCtrlModeFrntLe;
     : Arb_CtrlAbsCtrlInfo.AbsCtrlModeFrntRi;
     : Arb_CtrlAbsCtrlInfo.AbsCtrlModeReLe;
     : Arb_CtrlAbsCtrlInfo.AbsCtrlModeReRi;
     : Arb_CtrlAbsCtrlInfo.AbsTarPRateFrntLe;
     : Arb_CtrlAbsCtrlInfo.AbsTarPRateFrntRi;
     : Arb_CtrlAbsCtrlInfo.AbsTarPRateReLe;
     : Arb_CtrlAbsCtrlInfo.AbsTarPRateReRi;
     : Arb_CtrlAbsCtrlInfo.AbsPrioFrntLe;
     : Arb_CtrlAbsCtrlInfo.AbsPrioFrntRi;
     : Arb_CtrlAbsCtrlInfo.AbsPrioReLe;
     : Arb_CtrlAbsCtrlInfo.AbsPrioReRi;
     : Arb_CtrlAbsCtrlInfo.AbsDelTarPFrntLe;
     : Arb_CtrlAbsCtrlInfo.AbsDelTarPFrntRi;
     : Arb_CtrlAbsCtrlInfo.AbsDelTarPReLe;
     : Arb_CtrlAbsCtrlInfo.AbsDelTarPReRi;
     =============================================================================*/
    
    Arb_Ctrl_Read_Arb_CtrlAvhCtrlInfo(&Arb_CtrlBus.Arb_CtrlAvhCtrlInfo);
    /*==============================================================================
    * Members of structure Arb_CtrlAvhCtrlInfo 
     : Arb_CtrlAvhCtrlInfo.AvhActFlg;
     : Arb_CtrlAvhCtrlInfo.AvhDefectFlg;
     : Arb_CtrlAvhCtrlInfo.AvhTarP;
     =============================================================================*/
    
    Arb_Ctrl_Read_Arb_CtrlBaCtrlInfo(&Arb_CtrlBus.Arb_CtrlBaCtrlInfo);
    /*==============================================================================
    * Members of structure Arb_CtrlBaCtrlInfo 
     : Arb_CtrlBaCtrlInfo.BaActFlg;
     : Arb_CtrlBaCtrlInfo.BaCDefectFlg;
     : Arb_CtrlBaCtrlInfo.BaTarP;
     =============================================================================*/
    
    Arb_Ctrl_Read_Arb_CtrlEbdCtrlInfo(&Arb_CtrlBus.Arb_CtrlEbdCtrlInfo);
    /*==============================================================================
    * Members of structure Arb_CtrlEbdCtrlInfo 
     : Arb_CtrlEbdCtrlInfo.EbdActFlg;
     : Arb_CtrlEbdCtrlInfo.EbdDefectFlg;
     : Arb_CtrlEbdCtrlInfo.EbdTarPFrntLe;
     : Arb_CtrlEbdCtrlInfo.EbdTarPFrntRi;
     : Arb_CtrlEbdCtrlInfo.EbdTarPReLe;
     : Arb_CtrlEbdCtrlInfo.EbdTarPReRi;
     : Arb_CtrlEbdCtrlInfo.EbdTarPRateReLe;
     : Arb_CtrlEbdCtrlInfo.EbdTarPRateReRi;
     : Arb_CtrlEbdCtrlInfo.EbdCtrlModeReLe;
     : Arb_CtrlEbdCtrlInfo.EbdCtrlModeReRi;
     : Arb_CtrlEbdCtrlInfo.EbdDelTarPReLe;
     : Arb_CtrlEbdCtrlInfo.EbdDelTarPReRi;
     =============================================================================*/
    
    Arb_Ctrl_Read_Arb_CtrlEbpCtrlInfo(&Arb_CtrlBus.Arb_CtrlEbpCtrlInfo);
    /*==============================================================================
    * Members of structure Arb_CtrlEbpCtrlInfo 
     : Arb_CtrlEbpCtrlInfo.EbpActFlg;
     : Arb_CtrlEbpCtrlInfo.EbpDefectFlg;
     : Arb_CtrlEbpCtrlInfo.EbpTarP;
     =============================================================================*/
    
    Arb_Ctrl_Read_Arb_CtrlEpbiCtrlInfo(&Arb_CtrlBus.Arb_CtrlEpbiCtrlInfo);
    /*==============================================================================
    * Members of structure Arb_CtrlEpbiCtrlInfo 
     : Arb_CtrlEpbiCtrlInfo.EpbiActFlg;
     : Arb_CtrlEpbiCtrlInfo.EpbiDefectFlg;
     : Arb_CtrlEpbiCtrlInfo.EpbiTarP;
     =============================================================================*/
    
    Arb_Ctrl_Read_Arb_CtrlEscCtrlInfo(&Arb_CtrlBus.Arb_CtrlEscCtrlInfo);
    /*==============================================================================
    * Members of structure Arb_CtrlEscCtrlInfo 
     : Arb_CtrlEscCtrlInfo.EscActFlg;
     : Arb_CtrlEscCtrlInfo.EscDefectFlg;
     : Arb_CtrlEscCtrlInfo.EscTarPFrntLe;
     : Arb_CtrlEscCtrlInfo.EscTarPFrntRi;
     : Arb_CtrlEscCtrlInfo.EscTarPReLe;
     : Arb_CtrlEscCtrlInfo.EscTarPReRi;
     : Arb_CtrlEscCtrlInfo.EscCtrlModeFrntLe;
     : Arb_CtrlEscCtrlInfo.EscCtrlModeFrntRi;
     : Arb_CtrlEscCtrlInfo.EscCtrlModeReLe;
     : Arb_CtrlEscCtrlInfo.EscCtrlModeReRi;
     : Arb_CtrlEscCtrlInfo.EscTarPRateFrntLe;
     : Arb_CtrlEscCtrlInfo.EscTarPRateFrntRi;
     : Arb_CtrlEscCtrlInfo.EscTarPRateReLe;
     : Arb_CtrlEscCtrlInfo.EscTarPRateReRi;
     : Arb_CtrlEscCtrlInfo.EscPrioFrntLe;
     : Arb_CtrlEscCtrlInfo.EscPrioFrntRi;
     : Arb_CtrlEscCtrlInfo.EscPrioReLe;
     : Arb_CtrlEscCtrlInfo.EscPrioReRi;
     : Arb_CtrlEscCtrlInfo.EscPreCtrlModeFrntLe;
     : Arb_CtrlEscCtrlInfo.EscPreCtrlModeFrntRi;
     : Arb_CtrlEscCtrlInfo.EscPreCtrlModeReLe;
     : Arb_CtrlEscCtrlInfo.EscPreCtrlModeReRi;
     : Arb_CtrlEscCtrlInfo.EscDelTarPFrntLe;
     : Arb_CtrlEscCtrlInfo.EscDelTarPFrntRi;
     : Arb_CtrlEscCtrlInfo.EscDelTarPReLe;
     : Arb_CtrlEscCtrlInfo.EscDelTarPReRi;
     =============================================================================*/
    
    Arb_Ctrl_Read_Arb_CtrlHsaCtrlInfo(&Arb_CtrlBus.Arb_CtrlHsaCtrlInfo);
    /*==============================================================================
    * Members of structure Arb_CtrlHsaCtrlInfo 
     : Arb_CtrlHsaCtrlInfo.HsaActFlg;
     : Arb_CtrlHsaCtrlInfo.HsaDefectFlg;
     : Arb_CtrlHsaCtrlInfo.HsaTarP;
     =============================================================================*/
    
    Arb_Ctrl_Read_Arb_CtrlSccCtrlInfo(&Arb_CtrlBus.Arb_CtrlSccCtrlInfo);
    /*==============================================================================
    * Members of structure Arb_CtrlSccCtrlInfo 
     : Arb_CtrlSccCtrlInfo.SccActFlg;
     : Arb_CtrlSccCtrlInfo.SccDefectFlg;
     : Arb_CtrlSccCtrlInfo.SccTarP;
     =============================================================================*/
    
    Arb_Ctrl_Read_Arb_CtrlTcsCtrlInfo(&Arb_CtrlBus.Arb_CtrlTcsCtrlInfo);
    /*==============================================================================
    * Members of structure Arb_CtrlTcsCtrlInfo 
     : Arb_CtrlTcsCtrlInfo.TcsActFlg;
     : Arb_CtrlTcsCtrlInfo.TcsDefectFlg;
     : Arb_CtrlTcsCtrlInfo.TcsTarPFrntLe;
     : Arb_CtrlTcsCtrlInfo.TcsTarPFrntRi;
     : Arb_CtrlTcsCtrlInfo.TcsTarPReLe;
     : Arb_CtrlTcsCtrlInfo.TcsTarPReRi;
     : Arb_CtrlTcsCtrlInfo.BothDrvgWhlBrkCtlReqFlg;
     : Arb_CtrlTcsCtrlInfo.BrkCtrlFctFrntLeByWspc;
     : Arb_CtrlTcsCtrlInfo.BrkCtrlFctFrntRiByWspc;
     : Arb_CtrlTcsCtrlInfo.BrkCtrlFctReLeByWspc;
     : Arb_CtrlTcsCtrlInfo.BrkCtrlFctReRiByWspc;
     : Arb_CtrlTcsCtrlInfo.BrkTqGrdtReqFrntLeByWspc;
     : Arb_CtrlTcsCtrlInfo.BrkTqGrdtReqFrntRiByWspc;
     : Arb_CtrlTcsCtrlInfo.BrkTqGrdtReqReLeByWspc;
     : Arb_CtrlTcsCtrlInfo.BrkTqGrdtReqReRiByWspc;
     : Arb_CtrlTcsCtrlInfo.TcsDelTarPFrntLe;
     : Arb_CtrlTcsCtrlInfo.TcsDelTarPFrntRi;
     : Arb_CtrlTcsCtrlInfo.TcsDelTarPReLe;
     : Arb_CtrlTcsCtrlInfo.TcsDelTarPReRi;
     =============================================================================*/
    
    Arb_Ctrl_Read_Arb_CtrlTvbbCtrlInfo(&Arb_CtrlBus.Arb_CtrlTvbbCtrlInfo);
    /*==============================================================================
    * Members of structure Arb_CtrlTvbbCtrlInfo 
     : Arb_CtrlTvbbCtrlInfo.TvbbActFlg;
     : Arb_CtrlTvbbCtrlInfo.TvbbDefectFlg;
     : Arb_CtrlTvbbCtrlInfo.TvbbTarP;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Arb_Ctrl_Read_Arb_CtrlBaseBrkCtrlModInfo_VehStandStillStFlg(&Arb_CtrlBus.Arb_CtrlBaseBrkCtrlModInfo.VehStandStillStFlg);

    Arb_Ctrl_Read_Arb_CtrlBBCCtrlInfo(&Arb_CtrlBus.Arb_CtrlBBCCtrlInfo);
    /*==============================================================================
    * Members of structure Arb_CtrlBBCCtrlInfo 
     : Arb_CtrlBBCCtrlInfo.BBCCtrlSt;
     =============================================================================*/
    
    Arb_Ctrl_Read_Arb_CtrlEcuModeSts(&Arb_CtrlBus.Arb_CtrlEcuModeSts);
    Arb_Ctrl_Read_Arb_CtrlIgnOnOffSts(&Arb_CtrlBus.Arb_CtrlIgnOnOffSts);
    Arb_Ctrl_Read_Arb_CtrlFuncInhibitArbiSts(&Arb_CtrlBus.Arb_CtrlFuncInhibitArbiSts);
    Arb_Ctrl_Read_Arb_CtrlBaseBrkCtrlrActFlg(&Arb_CtrlBus.Arb_CtrlBaseBrkCtrlrActFlg);
    Arb_Ctrl_Read_Arb_CtrlBrkPRednForBaseBrkCtrlr(&Arb_CtrlBus.Arb_CtrlBrkPRednForBaseBrkCtrlr);
    Arb_Ctrl_Read_Arb_CtrlPCtrlSt(&Arb_CtrlBus.Arb_CtrlPCtrlSt);
    Arb_Ctrl_Read_Arb_CtrlRgnBrkCtrlrActStFlg(&Arb_CtrlBus.Arb_CtrlRgnBrkCtrlrActStFlg);
    Arb_Ctrl_Read_Arb_CtrlTarPFromBaseBrkCtrlr(&Arb_CtrlBus.Arb_CtrlTarPFromBaseBrkCtrlr);
    Arb_Ctrl_Read_Arb_CtrlTarPRateFromBaseBrkCtrlr(&Arb_CtrlBus.Arb_CtrlTarPRateFromBaseBrkCtrlr);
    Arb_Ctrl_Read_Arb_CtrlVehSpdFild(&Arb_CtrlBus.Arb_CtrlVehSpdFild);

    /* Process */
    LCARB_InitializePara();
	LCARB_SinalProcessing();
	LCARB_CalTarPressure();
	LCARB_SetDataInfo();

    /* Output */
    Arb_Ctrl_Write_Arb_CtrlFinalTarPInfo(&Arb_CtrlBus.Arb_CtrlFinalTarPInfo);
    /*==============================================================================
    * Members of structure Arb_CtrlFinalTarPInfo 
     : Arb_CtrlFinalTarPInfo.FinalTarPOfWhlFrntLe;
     : Arb_CtrlFinalTarPInfo.FinalTarPOfWhlFrntRi;
     : Arb_CtrlFinalTarPInfo.FinalTarPOfWhlReLe;
     : Arb_CtrlFinalTarPInfo.FinalTarPOfWhlReRi;
     : Arb_CtrlFinalTarPInfo.FinalTarPRateOfWhlFrntLe;
     : Arb_CtrlFinalTarPInfo.FinalTarPRateOfWhlFrntRi;
     : Arb_CtrlFinalTarPInfo.FinalTarPRateOfWhlReLe;
     : Arb_CtrlFinalTarPInfo.FinalTarPRateOfWhlReRi;
     : Arb_CtrlFinalTarPInfo.PCtrlPrioOfWhlFrntLe;
     : Arb_CtrlFinalTarPInfo.PCtrlPrioOfWhlFrntRi;
     : Arb_CtrlFinalTarPInfo.PCtrlPrioOfWhlReLe;
     : Arb_CtrlFinalTarPInfo.PCtrlPrioOfWhlReRi;
     : Arb_CtrlFinalTarPInfo.CtrlModOfWhlFrntLe;
     : Arb_CtrlFinalTarPInfo.CtrlModOfOfWhlFrntRi;
     : Arb_CtrlFinalTarPInfo.CtrlModOfWhlReLe;
     : Arb_CtrlFinalTarPInfo.CtrlModOfWhlReRi;
     : Arb_CtrlFinalTarPInfo.ReqdPCtrlBoostMod;
     : Arb_CtrlFinalTarPInfo.FinalDelTarPOfWhlFrntLe;
     : Arb_CtrlFinalTarPInfo.FinalDelTarPOfWhlFrntRi;
     : Arb_CtrlFinalTarPInfo.FinalDelTarPOfWhlReLe;
     : Arb_CtrlFinalTarPInfo.FinalDelTarPOfWhlReRi;
     : Arb_CtrlFinalTarPInfo.FinalMaxCircuitTp;
     =============================================================================*/
    
    Arb_Ctrl_Write_Arb_CtrlWhlOutVlvCtrlTarInfo(&Arb_CtrlBus.Arb_CtrlWhlOutVlvCtrlTarInfo);
    /*==============================================================================
    * Members of structure Arb_CtrlWhlOutVlvCtrlTarInfo 
     : Arb_CtrlWhlOutVlvCtrlTarInfo.FlOutVlvCtrlTar;
     : Arb_CtrlWhlOutVlvCtrlTarInfo.FrOutVlvCtrlTar;
     : Arb_CtrlWhlOutVlvCtrlTarInfo.RlOutVlvCtrlTar;
     : Arb_CtrlWhlOutVlvCtrlTarInfo.RrOutVlvCtrlTar;
     =============================================================================*/
    
    Arb_Ctrl_Write_Arb_CtrlWhlPreFillInfo(&Arb_CtrlBus.Arb_CtrlWhlPreFillInfo);
    /*==============================================================================
    * Members of structure Arb_CtrlWhlPreFillInfo 
     : Arb_CtrlWhlPreFillInfo.FlPreFillActFlg;
     : Arb_CtrlWhlPreFillInfo.FrPreFillActFlg;
     : Arb_CtrlWhlPreFillInfo.RlPreFillActFlg;
     : Arb_CtrlWhlPreFillInfo.RrPreFillActFlg;
     =============================================================================*/
    

    Arb_Ctrl_Timer_Elapsed = STM0_TIM0.U - Arb_Ctrl_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define ARB_CTRL_STOP_SEC_CODE
#include "Arb_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
