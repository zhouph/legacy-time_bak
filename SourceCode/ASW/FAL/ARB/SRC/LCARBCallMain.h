/**
 * @defgroup LCBBC_DecideCtrlSt LCBBC_DecideCtrlSt
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LCARBCallMain.h
 * @brief       Local Header File
 * @date        20YY. MM. DD.
 ******************************************************************************/
 
#ifndef LCARB_DECIDECTRLST_H_
#define LCARB_DECIDECTRLST_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Arb_Ctrl.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
	uint16_t lcarbu16SetFnlTP;
	int16_t  lcarbs16SetFnlTPRate;
	uint8_t  lcarbu8SetFnlPrionrity;
	uint8_t  lcarbu8SetFnlCntrMode;

	uint16_t lcarbu16SetTP;
	int16_t  lcarbs16SetDelTP;
	int16_t  lcarbs16SetTPRate;
	uint8_t  lcarbu8SetPrionrity;
	uint8_t  lcarbu8SetCntrMode;

	uint16_t lcarbu16GetEbdTarP;
	int16_t  lcarbs16GetEbdDelTarP;
	int16_t	 lcarbs16GetEbdTarPRate;
	uint8_t  lcarbu8GetEbdCntrMode;
	uint8_t  lcarbu8GetEbdPrionrity;

	uint16_t lcarbu16GetAbsTarP;
	int16_t  lcarbs16GetAbsDelTarP;
	int16_t	 lcarbs16GetAbsTarPRate;
	uint8_t  lcarbu8GetAbsCntrMode;
	uint8_t  lcarbu8GetAbsPrionrity;

	uint16_t lcarbu16GetEscTarP;
	int16_t  lcarbs16GetEscDelTarP;
	int16_t	 lcarbs16GetEscTarPRate;
	uint8_t  lcarbu8GetEscCntrMode;
	uint8_t  lcarbu8GetEscPrionrity;

	uint16_t lcarbu16GetTcsTarP;
	int16_t  lcarbs16GetTcsDelTarP;
	int16_t	 lcarbs16GetTcsTarPRate;
	uint8_t  lcarbu8GetTcsCntrMode;
	uint8_t  lcarbu8GetTcsPrionrity;

	uint16_t lcarbu16GetHsaTarP;
	int16_t  lcarbs16GetHsaDelTarP;
	int16_t	 lcarbs16GetHsaTarPRate;
	uint8_t  lcarbu8GetHsaCntrMode;
	uint8_t  lcarbu8GetHsaPrionrity;

	uint16_t lcarbu16GetBbcTarP;
	int16_t  lcarbs16GetBbcDelTarP;
	int16_t	 lcarbs16GetBbcTarPRate;
	uint8_t  lcarbu8GetBbcCntrMode;
	uint8_t  lcarbu8GetBbcPrionrity;

}ARB_struct;

extern ARB_struct ARB_FL, ARB_FR, ARB_RL, ARB_RR;

extern uint8_t  lcarbu8CntrModefourwh;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void LCARB_InitializePara(void);
extern void LCARB_SinalProcessing(void);
extern void LCARB_CalTarPressure(void);
extern void LCARB_SetDataInfo(void);
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* LCARB_DETCONTROLSTATE_H_ */
/** @} */
