/**
 * @defgroup Arb_Ctrl Arb_Ctrl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Arb_Ctrl.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

#include "LCARBCallMain.h"
#include "LCARBRBCCooperation.h"
#include "LCARBCalRBCCooperTargetPRate.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define U16_MAX_RB_TQ_PRESS_LIMIT				S16_PCTRL_PRESS_20_BAR

  #if (__SCC == ENABLE)
#define SCC_PAD_MU_COMP		DISABLE
  #endif /* (__SCC == ENABLE) */

  #if __AHB_SEAL_FRICTION_EST
#define __CHANGE_SEAL_FRICTION_COMP ENABLE
  #else
#define __COMP_TP_FOR_PAD_MU_CHANGE			ENABLE
#define __CHANGE_SEAL_FRICTION_COMP 		0
#define __COMP_TP_FOR_MIN_PRESS      		ENABLE	/*ENABLE DISABLE */
#define __COMP_TP_FOR_FLEX_PAD_MU_CHANGE	ENABLE
#define __COMP_TP_FOR_ONS_PAD_MU_CHANGE		ENABLE
  #endif
#define __AHB_LOW_SPEED_TARGET_P_LIMIT		ENABLE


#define U16_SCC_RB_ENABLE_SPEED_LIMIT           S16_SPEED_30_KPH
#define U16_SCC_RB_REDUCTION_START_SPEED        S16_SPEED_40_KPH
#define U16_SCC_RB_ENTER_SPEED_LIMIT            S16_SPEED_40_KPH
#define	U8_RB_REDUCTION_START_SPEED             S16_SPEED_17_KPH
#define U8_RB_ENABLE_SPEED_LIMIT                S16_SPEED_6_KPH
#define S16_MAX_PAD_MU_COMP_P_ONS               S16_PCTRL_PRESS_5_BAR
#define S16_MAX_PAD_MU_COMP_P                   S16_PCTRL_PRESS_4_BAR
#define S16_PAD_MU_COMP_P_RATIO_AT_10BAR        S8_PERCENT_60_PRO
#define U8_MAX_RB_TQ_PRESS_LIMIT                20
#define S16_RBC_MIN_PRESS						S16_PCTRL_PRESS_0_5_BAR

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
	uint16_t lcarbu16EstTP;
	uint16_t lcarbu16EstTPBfPadMuComp;
	int16_t  lcarbu16EstTPRate;

	uint16_t lcarbu16ARBSetTP;
	int16_t  lcarbs16ARBSetTPRate;

	int16_t  s16MaxTPReduction;
	int16_t  s16RBCTPReduction;
	uint16_t u16BrkTpAfPadMuCompOutput;

}stRBC_struct_t;

typedef struct
{
	int16_t s16PadMuCompTp;
	int16_t s16TotalBrakePress;
}stCALC_PAD_MU_COMP_TP_OUTPUT_t;

typedef struct
{
	int16_t s16RBCTPReduction;
	int16_t s16RBCMinSpeed;
	int16_t s16MaxPadMuCompP;
	int16_t s16TPwithoutComp;
	int16_t s16PadmuCompPecent;
	int16_t s16TPwithoutCompOld;
	int16_t s16WheelTargetPressOld;
	int16_t s16VehicleSpeed;
	int16_t s16DriverIntendedTPDiff;
	int16_t s16HSAActiveBrkTP;
	int16_t s16AVHActiveBrkTP;
}stBRK_PAD_MU_COMP_INPUT_t;

typedef struct
{
	uint8_t u8OverNightSoakingFlg;
	int16_t s16WheelTargetPress;
	int16_t s16BaseBrkTargetPress;
	int16_t s16RBCTPReduction;
	int16_t s16PadMuCmpResetCondFlag;
	int16_t s16TargetPressRateBarPSecP;
	int16_t s16VehicleSpeed;
	int16_t s16DriverIntendedTPDiff;
	int16_t s16HSAActiveBrkTP;
	int16_t s16AVHActiveBrkTP;
}stPAD_MU_COMP_DU_RBC_BLEND_R_t;

typedef struct
{
	uint8_t u8TPCompAfterPadMuChange;
	uint8_t u8TPCompForPadMuChange;
	int8_t  s8TPRatioAfPadMuChange;
	int8_t  s8TPCompCntForPadMuChange;
	int16_t s16VSpdAtPadMuCompStart;
	int16_t s16TotalBrakePress;
	int16_t s16PadmuCompPecent;
	int16_t s16MaxPadMuCompP;
	int16_t s16RBCMinSpeed;
	int16_t s16RBCMaxSpeed;
	int16_t s16RBCTPReductionOld;
	int16_t s16TPwithoutCompOld;
	int16_t s16WheelTargetPressOld;
	int16_t s16PadMuCompTPDuRBCBlend;
	int16_t s16PadMuCompTp;
}stPAD_MU_COMP_DU_RBC_BLEND_W_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ARB_CTRL_START_SEC_CONST_UNSPECIFIED
#include "Arb_MemMap.h"

/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/



/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ARB_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arb_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/

static uint16_t lcarbu16RBCReductionTP;

static uint16_t lcarbu8RBCActFlg;

static uint16_t lcarbu16BaseBrakeTP;
static int16_t  lcarbs16VehicleSpeed;
static uint8_t  lcarbu8OverNightSoakingFlg;
static int16_t  lcarbs16DriverIntendedTPDiff;
static int16_t  lcarbs16AVHActiveBrkTP;
static int16_t  lcarbs16HSAActiveBrkTP;
static int8_t   lcarbs8PCtrlState;
static int8_t   lcarbs8PCtrlBoostMode;


FAL_STATIC stRBC_struct_t RBC_FL = {0,0,0,0,0,0,0,0};
FAL_STATIC stRBC_struct_t RBC_FR = {0,0,0,0,0,0,0,0};
FAL_STATIC stRBC_struct_t RBC_RL = {0,0,0,0,0,0,0,0};
FAL_STATIC stRBC_struct_t RBC_RR = {0,0,0,0,0,0,0,0};




#define ARB_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arb_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ARB_CTRL_START_SEC_CODE
#include "Arb_MemMap.h"

static void LCARB_vRBCInput(void);
static void LCARB_vRBCOutput(void);

static stPAD_MU_COMP_DU_RBC_BLEND_W_t LCARB_u16RBCCooperation(stPAD_MU_COMP_DU_RBC_BLEND_R_t *stPadMuCompDuRBCBlend_r, stPAD_MU_COMP_DU_RBC_BLEND_W_t *stPadMuCompDuRBCBlend_w);
#if (__COMP_TP_FOR_PAD_MU_CHANGE == ENABLE)
static stPAD_MU_COMP_DU_RBC_BLEND_W_t LCARB_stPadMuCompDuRBCBlend(stPAD_MU_COMP_DU_RBC_BLEND_R_t *stPadMuCompDuRBCBlend_r, stPAD_MU_COMP_DU_RBC_BLEND_W_t *stPadMuCompDuRBCBlend_w);
static stPAD_MU_COMP_DU_RBC_BLEND_W_t LCARB_stCalcPadMuCompTP(stBRK_PAD_MU_COMP_INPUT_t *stBrkPadMuCompInput_r, stPAD_MU_COMP_DU_RBC_BLEND_W_t *stPadMuCompDuRBCBlend_w);
#endif /* (__COMP_TP_FOR_PAD_MU_CHANGE == ENABLE) */
/*static void LCARB_vPadMuCompDuRBCBlend(void);*/

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

void LCARB_vEstTargetPress(void)
{
	static stPAD_MU_COMP_DU_RBC_BLEND_W_t stPadMuCompDuRBCBlend_FL_w = {0,0,S8_PERCENT_100_PRO,0,0,0,0,0,0,0,0,0,0,0,0};
	static stPAD_MU_COMP_DU_RBC_BLEND_W_t stPadMuCompDuRBCBlend_FR_w = {0,0,S8_PERCENT_100_PRO,0,0,0,0,0,0,0,0,0,0,0,0};
	static stPAD_MU_COMP_DU_RBC_BLEND_W_t stPadMuCompDuRBCBlend_RL_w = {0,0,S8_PERCENT_100_PRO,0,0,0,0,0,0,0,0,0,0,0,0};
	static stPAD_MU_COMP_DU_RBC_BLEND_W_t stPadMuCompDuRBCBlend_RR_w = {0,0,S8_PERCENT_100_PRO,0,0,0,0,0,0,0,0,0,0,0,0};
	stPAD_MU_COMP_DU_RBC_BLEND_R_t        stPadMuCompDuRBCBlend_r    = {0,0,0,0,0,0,0,0,0,0};

	LCARB_vRBCInput();

	/* Pad Mu Comp Read Information  - Begin */
	stPadMuCompDuRBCBlend_r.s16BaseBrkTargetPress = lcarbu16BaseBrakeTP;
	stPadMuCompDuRBCBlend_r.s16VehicleSpeed = lcarbs16VehicleSpeed;
	stPadMuCompDuRBCBlend_r.s16RBCTPReduction = lcarbu16RBCReductionTP;
	stPadMuCompDuRBCBlend_r.u8OverNightSoakingFlg = lcarbu8OverNightSoakingFlg;
	stPadMuCompDuRBCBlend_r.s16DriverIntendedTPDiff = lcarbs16DriverIntendedTPDiff;
	stPadMuCompDuRBCBlend_r.s16AVHActiveBrkTP = lcarbs16AVHActiveBrkTP;
	stPadMuCompDuRBCBlend_r.s16HSAActiveBrkTP = lcarbs16HSAActiveBrkTP;

	/*stPadMuCompDuRBCBlend_r.s16PadMuCmpResetCondFlag = (int16_t)((lcahbu1AHBOn == FALSE) | (ldahbu1AbsActiveFlg == TRUE) | (ldahbu1TcsActiveFlg == TRUE) | (ldahbu1EscActiveFlg == TRUE));*/
	stPadMuCompDuRBCBlend_r.s16PadMuCmpResetCondFlag =	((lcarbs8PCtrlState != S8_PRESS_BOOST) && (lcarbs8PCtrlState != S8_PRESS_PREBOOST)) |
														(lcarbs8PCtrlBoostMode != S8_BOOST_MODE_REQ_L0) | (lcarbs8PCtrlBoostMode != S8_BOOST_MODE_REQ_L2) |
														(lcarbs8PCtrlBoostMode != S8_BOOST_MODE_REQ_L3) | (lcarbs8PCtrlBoostMode != S8_BOOST_MODE_REQ_L7) |
														(lcarbs8PCtrlBoostMode != S8_BOOST_MODE_REQ_L9);
#if DISABLE
#define S8_BOOST_MODE_REQ_L0				0		/* ABS Torque Control Mode 										*/
#define S8_BOOST_MODE_REQ_L2				2		/* ESC oversteer (Include ROP), ABS, TCS_1_mode : 300bar/s 		*/
#define S8_BOOST_MODE_REQ_L3				3		/* TCS_2_mode, TSP, ESC understeer : 200bar/s  					*/
#define S8_BOOST_MODE_REQ_L7				7		/* TCS_4_mode, Hold function(HSA, AVH)AC Hold 100bar/s			*/
#define S8_BOOST_MODE_REQ_L9				9		/* n-channel multiplexing mode */
#endif
	/* Pad Mu Comp Read Information  - End */

	stPadMuCompDuRBCBlend_r.s16WheelTargetPress = RBC_FL.lcarbu16ARBSetTP;
	stPadMuCompDuRBCBlend_r.s16TargetPressRateBarPSecP   = stRBCTarPRate.FLTarPRate ;						/* Should be update using each wheel Target Press Rate */
	stPadMuCompDuRBCBlend_FL_w = LCARB_u16RBCCooperation(&stPadMuCompDuRBCBlend_r, &stPadMuCompDuRBCBlend_FL_w);

	stPadMuCompDuRBCBlend_r.s16WheelTargetPress = RBC_FR.lcarbu16ARBSetTP;
	stPadMuCompDuRBCBlend_r.s16TargetPressRateBarPSecP  = stRBCTarPRate.FRTarPRate ;							/* Should be update using each wheel Target Press Rate */
	stPadMuCompDuRBCBlend_FR_w = LCARB_u16RBCCooperation(&stPadMuCompDuRBCBlend_r, &stPadMuCompDuRBCBlend_FR_w);

	stPadMuCompDuRBCBlend_r.s16WheelTargetPress = RBC_RL.lcarbu16ARBSetTP;
	stPadMuCompDuRBCBlend_r.s16TargetPressRateBarPSecP  = stRBCTarPRate.RLTarPRate ;							/* Should be update using each wheel Target Press Rate */
	stPadMuCompDuRBCBlend_RL_w = LCARB_u16RBCCooperation(&stPadMuCompDuRBCBlend_r, &stPadMuCompDuRBCBlend_RL_w);

	stPadMuCompDuRBCBlend_r.s16WheelTargetPress = RBC_RR.lcarbu16ARBSetTP;
	stPadMuCompDuRBCBlend_r.s16TargetPressRateBarPSecP  = stRBCTarPRate.RRTarPRate ;							/* Should be update using each wheel Target Press Rate */
	stPadMuCompDuRBCBlend_RR_w = LCARB_u16RBCCooperation(&stPadMuCompDuRBCBlend_r, &stPadMuCompDuRBCBlend_RR_w);

	ARB_FL.lcarbu16SetTP = stPadMuCompDuRBCBlend_FL_w.s16PadMuCompTPDuRBCBlend;
	ARB_FR.lcarbu16SetTP = stPadMuCompDuRBCBlend_FR_w.s16PadMuCompTPDuRBCBlend;
	ARB_RL.lcarbu16SetTP = stPadMuCompDuRBCBlend_RL_w.s16PadMuCompTPDuRBCBlend;
	ARB_RR.lcarbu16SetTP = stPadMuCompDuRBCBlend_RR_w.s16PadMuCompTPDuRBCBlend;

	LCARB_vRBCOutput();
}



/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static void LCARB_vRBCInput(void)
{
	static int16_t lcarbs16BaseBrakeTPOld = S16_PCTRL_PRESS_0_BAR;

    lcarbu16RBCReductionTP 	=(uint16_t)Arb_CtrlBrkPRednForBaseBrkCtrlr; /* rbc reduction �� */
    lcarbu8RBCActFlg 	=(uint8_t)Arb_CtrlRgnBrkCtrlrActStFlg;/*rbc act flag*/


	RBC_FL.lcarbu16ARBSetTP       =     ARB_FL.lcarbu16SetTP	;
	RBC_FR.lcarbu16ARBSetTP       =     ARB_FR.lcarbu16SetTP	;
	RBC_RL.lcarbu16ARBSetTP       =     ARB_RL.lcarbu16SetTP	;
	RBC_RR.lcarbu16ARBSetTP       =     ARB_RR.lcarbu16SetTP	;

	lcarbu16BaseBrakeTP           =     Arb_CtrlTarPFromBaseBrkCtrlr;
	lcarbs16VehicleSpeed          =     Arb_CtrlVehSpdFild;
	lcarbu8OverNightSoakingFlg    =     FALSE;
	lcarbs16AVHActiveBrkTP        =     S16_PCTRL_PRESS_0_BAR;
	lcarbs16HSAActiveBrkTP        =     S16_PCTRL_PRESS_0_BAR;
	lcarbs8PCtrlState             =     Arb_CtrlBus.Arb_CtrlPCtrlSt;
	lcarbs8PCtrlBoostMode         =     lcarbu8CntrModefourwh;

	lcarbs16DriverIntendedTPDiff = lcarbu16BaseBrakeTP - lcarbs16BaseBrakeTPOld;

	lcarbs16BaseBrakeTPOld = lcarbu16BaseBrakeTP;
}

static void LCARB_vRBCOutput(void)
{

}


static stPAD_MU_COMP_DU_RBC_BLEND_W_t LCARB_u16RBCCooperation(stPAD_MU_COMP_DU_RBC_BLEND_R_t *stPadMuCompDuRBCBlend_r, stPAD_MU_COMP_DU_RBC_BLEND_W_t *stPadMuCompDuRBCBlend_w)
{
	int16_t s16MaxTPReduction = S16_PCTRL_PRESS_0_BAR;
	int16_t s16RBCTPReduction = S16_PCTRL_PRESS_0_BAR;
	int16_t s16RBCMinPress = S16_RBC_MIN_PRESS;

	if (stPadMuCompDuRBCBlend_r->s16RBCTPReduction > S16_PCTRL_PRESS_0_BAR)
	{
		s16MaxTPReduction = L_s16LimitMinMax((stPadMuCompDuRBCBlend_r->s16BaseBrkTargetPress- s16RBCMinPress), S16_PCTRL_PRESS_0_BAR, U16_MAX_RB_TQ_PRESS_LIMIT);
	}
	else
	{
		s16MaxTPReduction  = S16_PCTRL_PRESS_0_BAR;
	}

	s16RBCTPReduction  = L_s16LimitMinMax((int16_t)stPadMuCompDuRBCBlend_r->s16RBCTPReduction, S16_PCTRL_PRESS_0_BAR, s16MaxTPReduction);


	stPadMuCompDuRBCBlend_r->s16WheelTargetPress = stPadMuCompDuRBCBlend_r->s16WheelTargetPress - s16RBCTPReduction;

	*stPadMuCompDuRBCBlend_w = LCARB_stPadMuCompDuRBCBlend(stPadMuCompDuRBCBlend_r, stPadMuCompDuRBCBlend_w);

	return (*stPadMuCompDuRBCBlend_w);

}

#if (__COMP_TP_FOR_PAD_MU_CHANGE == ENABLE)
/******************************************************************************
* FUNCTION NAME:      LDAHB_brktpGetTPAfterPadMuComp
* CALLED BY:          LDAHB_vEstTargetPress()
* Preconditions:      none
* PARAMETER:          s16TPwithoutComp, s16TPwithoutCompBC2, s16TargetPress, s16TargetPressBC2, s16RBCTPReduction
* RETURN VALUE:       ldahbbrktpTP
* Description:        Compensate TP for Pad Mu Change
******************************************************************************/
static stPAD_MU_COMP_DU_RBC_BLEND_W_t LCARB_stPadMuCompDuRBCBlend(stPAD_MU_COMP_DU_RBC_BLEND_R_t *stPadMuCompDuRBCBlend_r, stPAD_MU_COMP_DU_RBC_BLEND_W_t *stPadMuCompDuRBCBlend_w)
{
	/* Variable declare */
	stBRK_PAD_MU_COMP_INPUT_t      stBrkPadMuCompInput                = {0,0,0,0,0,0,0,0,0,0,0};
	int16_t                        s16TPwithoutComp                   = S16_PCTRL_PRESS_0_BAR;
	int16_t                        ldahbs16CompMaxPressforPadMu       = S16_PCTRL_PRESS_0_BAR;
	int16_t                        s16TPComppPressForPadMu            = S16_PCTRL_PRESS_0_BAR;

	s16TPwithoutComp = stPadMuCompDuRBCBlend_r->s16WheelTargetPress;

	/*s16PadMuCmpResetCondFlag = (int16_t)((lcahbu1AHBOn == FALSE) | (ldahbu1AbsActiveFlg == TRUE) | (ldahbu1TcsActiveFlg == TRUE) | (ldahbu1EscActiveFlg == TRUE));*/
	if(stPadMuCompDuRBCBlend_r->s16PadMuCmpResetCondFlag == TRUE)
	{
		/* Reset Pad Mu Comp */
		stPadMuCompDuRBCBlend_w->u8TPCompAfterPadMuChange = FALSE;
		stPadMuCompDuRBCBlend_w->u8TPCompForPadMuChange = FALSE;
		stPadMuCompDuRBCBlend_w->s8TPRatioAfPadMuChange = 0;
		stPadMuCompDuRBCBlend_w->s8TPRatioAfPadMuChange = S8_PERCENT_100_PRO;
		stPadMuCompDuRBCBlend_w->s8TPCompCntForPadMuChange = 0;
		stPadMuCompDuRBCBlend_w->s16TotalBrakePress = S16_PCTRL_PRESS_0_BAR;
	}
	else
	{
		if(stPadMuCompDuRBCBlend_w->u8TPCompForPadMuChange == FALSE)		/* Detect condition for Pad Mu Comp */
		{
		  #if (SCC_PAD_MU_COMP == ENABLE)
			if(ldrbcu1RbcSccRegenActFlg == TRUE)
			{
				/* SCC Control state */
				stPadMuCompDuRBCBlend_w->s16RBCMaxSpeed = (int16_t)(U16_SCC_RB_REDUCTION_START_SPEED);
				stPadMuCompDuRBCBlend_w->s16RBCMinSpeed = (int16_t)(U16_SCC_RB_ENABLE_SPEED_LIMIT);
			}
			else
			{
				/* Non SCC Control State */
				stPadMuCompDuRBCBlend_w->s16RBCMaxSpeed = (int16_t)(U8_RB_REDUCTION_START_SPEED);
				stPadMuCompDuRBCBlend_w->s16RBCMinSpeed = (int16_t)(U8_RB_ENABLE_SPEED_LIMIT);
			}
		  #else /* (SCC_PAD_MU_COMP == DISABLE) */
			/* Non SCC Control State */
			stPadMuCompDuRBCBlend_w->s16RBCMaxSpeed = (int16_t)(U8_RB_REDUCTION_START_SPEED);
			stPadMuCompDuRBCBlend_w->s16RBCMinSpeed = (int16_t)(U8_RB_ENABLE_SPEED_LIMIT);
		  #endif /* (SCC_PAD_MU_COMP == ENABLE) */
			/* Don't have to consider the transient condition for stPadMuCompDuRBCBlend_w->s16RBCMaxSpeed and stPadMuCompDuRBCBlend_w->s16RBCMinSpeed (0<= stPadMuCompDuRBCBlend_w->s8TPCompCntForPadMuChange < 4)*/
			/* because ldrbcu1RbcSccRegenActFlg is fixed value during one braking cycle */

			/* Capture Blending Condition for Pad Mu Comp */
			if((stPadMuCompDuRBCBlend_r->s16RBCTPReduction > S16_PCTRL_PRESS_3_BAR) && (stPadMuCompDuRBCBlend_r->s16VehicleSpeed >= (stPadMuCompDuRBCBlend_w->s16RBCMinSpeed + S16_SPEED_1_KPH)) && (stPadMuCompDuRBCBlend_r->s16VehicleSpeed <= stPadMuCompDuRBCBlend_w->s16RBCMaxSpeed))
			{
				if((stPadMuCompDuRBCBlend_r->s16TargetPressRateBarPSecP > S16_PCTRL_PRESS_0_BAR) && (stPadMuCompDuRBCBlend_r->s16RBCTPReduction < stPadMuCompDuRBCBlend_w->s16RBCTPReductionOld) )		/* RBC Fade-out Blending Condition(Hydrauric Pressure Build up */
				{
					stPadMuCompDuRBCBlend_w->s8TPCompCntForPadMuChange = stPadMuCompDuRBCBlend_w->s8TPCompCntForPadMuChange + 1;

					/* Blending Edge Detection */
					if(stPadMuCompDuRBCBlend_w->s8TPCompCntForPadMuChange >= 4)
					{
						stPadMuCompDuRBCBlend_w->s8TPCompCntForPadMuChange = 0;
						stPadMuCompDuRBCBlend_w->u8TPCompForPadMuChange = TRUE;
						stPadMuCompDuRBCBlend_w->s8TPRatioAfPadMuChange = stPadMuCompDuRBCBlend_r->s16VehicleSpeed;
						stPadMuCompDuRBCBlend_w->s16TotalBrakePress = s16TPwithoutComp + stPadMuCompDuRBCBlend_r->s16RBCTPReduction;

		  #if __COMP_TP_FOR_ONS_PAD_MU_CHANGE == ENABLE
						if(stPadMuCompDuRBCBlend_r->u8OverNightSoakingFlg == TRUE)
						{
							ldahbs16CompMaxPressforPadMu = L_s16LimitMinMax(S16_MAX_PAD_MU_COMP_P_ONS, S16_PCTRL_PRESS_0_BAR, S16_PCTRL_PRESS_5_BAR);
						}
						else
						{
							ldahbs16CompMaxPressforPadMu = L_s16LimitMinMax(S16_MAX_PAD_MU_COMP_P, S16_PCTRL_PRESS_0_BAR, S16_PCTRL_PRESS_5_BAR);
						}
		  #else

						ldahbs16CompMaxPressforPadMu = L_s16LimitMinMax(S16_MAX_PAD_MU_COMP_P, S16_PCTRL_PRESS_0_BAR, S16_PCTRL_PRESS_5_BAR);

		  #endif

		  #if __COMP_TP_FOR_FLEX_PAD_MU_CHANGE == ENABLE
						stPadMuCompDuRBCBlend_w->s16MaxPadMuCompP = L_s16IInter3Point(stPadMuCompDuRBCBlend_w->s16RBCTPReductionOld,		/* Using stPadMuCompDuRBCBlend_w->s16RBCTPReductionOld  instead of (ldahbs16RBCPAtPadMuCompStart = stPadMuCompDuRBCBlend_w->s16RBCTPReductionOld;) */
												S16_PCTRL_PRESS_0_BAR, S16_PCTRL_PRESS_0_BAR,
												S16_PCTRL_PRESS_10_BAR, (((int16_t)(ldahbs16CompMaxPressforPadMu * S16_PAD_MU_COMP_P_RATIO_AT_10BAR))/S8_PERCENT_100_PRO) ,
												((int16_t)U8_MAX_RB_TQ_PRESS_LIMIT * S16_PCTRL_PRESS_1_BAR), ldahbs16CompMaxPressforPadMu);
		  #else
						stPadMuCompDuRBCBlend_w->s16MaxPadMuCompP = L_s16IInter2Point(stPadMuCompDuRBCBlend_w->s16RBCTPReductionOld,
												S16_PCTRL_PRESS_0_BAR, S16_PCTRL_PRESS_0_BAR,
												((int16_t)U8_MAX_RB_TQ_PRESS_LIMIT*S16_PCTRL_PRESS_1_BAR), ldahbs16CompMaxPressforPadMu);
		  #endif
						stPadMuCompDuRBCBlend_w->s16PadmuCompPecent =  (int16_t)(S8_PERCENT_100_PRO) - ((stPadMuCompDuRBCBlend_w->s16MaxPadMuCompP * 100) / stPadMuCompDuRBCBlend_w->s16TotalBrakePress);

						stBrkPadMuCompInput.s16RBCTPReduction        = stPadMuCompDuRBCBlend_r->s16RBCTPReduction;
						stBrkPadMuCompInput.s16RBCMinSpeed           = stPadMuCompDuRBCBlend_w->s16RBCMinSpeed;
						stBrkPadMuCompInput.s16MaxPadMuCompP         = stPadMuCompDuRBCBlend_w->s16MaxPadMuCompP;
						stBrkPadMuCompInput.s16TPwithoutComp         = s16TPwithoutComp;
						stBrkPadMuCompInput.s16PadmuCompPecent       = stPadMuCompDuRBCBlend_w->s16PadmuCompPecent;
						stBrkPadMuCompInput.s16TPwithoutCompOld      = stPadMuCompDuRBCBlend_w->s16TPwithoutCompOld;
						stBrkPadMuCompInput.s16WheelTargetPressOld   = stPadMuCompDuRBCBlend_w->s16WheelTargetPressOld;
						stBrkPadMuCompInput.s16VehicleSpeed          = stPadMuCompDuRBCBlend_r->s16VehicleSpeed;

						*stPadMuCompDuRBCBlend_w = LCARB_stCalcPadMuCompTP(&stBrkPadMuCompInput, stPadMuCompDuRBCBlend_w);

						stPadMuCompDuRBCBlend_r->s16WheelTargetPress = stPadMuCompDuRBCBlend_w->s16PadMuCompTp;
					}
					else { }
				}
				else if( stPadMuCompDuRBCBlend_r->s16RBCTPReduction > stPadMuCompDuRBCBlend_w->s16RBCTPReductionOld )
				{
					stPadMuCompDuRBCBlend_w->s8TPCompCntForPadMuChange = stPadMuCompDuRBCBlend_w->s8TPCompCntForPadMuChange - 2;

					if(stPadMuCompDuRBCBlend_w->s8TPCompCntForPadMuChange < 0)
					{
						stPadMuCompDuRBCBlend_w->s8TPCompCntForPadMuChange = 0;
					}
					else { }
				}
				else { }

			}
			else { }
		}
		else		/* Pad Mu Comp Action */
		{
			stBrkPadMuCompInput.s16RBCTPReduction        = stPadMuCompDuRBCBlend_r->s16RBCTPReduction;
			stBrkPadMuCompInput.s16RBCMinSpeed           = stPadMuCompDuRBCBlend_w->s16RBCMinSpeed;
			stBrkPadMuCompInput.s16MaxPadMuCompP         = stPadMuCompDuRBCBlend_w->s16MaxPadMuCompP;
			stBrkPadMuCompInput.s16TPwithoutComp         = s16TPwithoutComp;
			stBrkPadMuCompInput.s16PadmuCompPecent       = stPadMuCompDuRBCBlend_w->s16PadmuCompPecent;
			stBrkPadMuCompInput.s16TPwithoutCompOld      = stPadMuCompDuRBCBlend_w->s16TPwithoutCompOld;
			stBrkPadMuCompInput.s16WheelTargetPressOld   = stPadMuCompDuRBCBlend_w->s16WheelTargetPressOld;
			stBrkPadMuCompInput.s16VehicleSpeed          = stPadMuCompDuRBCBlend_r->s16VehicleSpeed;
			stBrkPadMuCompInput.s16AVHActiveBrkTP        = 0;
			stBrkPadMuCompInput.s16HSAActiveBrkTP        = 0;
			stBrkPadMuCompInput.s16DriverIntendedTPDiff  = 0;

			*stPadMuCompDuRBCBlend_w = LCARB_stCalcPadMuCompTP(&stBrkPadMuCompInput, stPadMuCompDuRBCBlend_w);

			stPadMuCompDuRBCBlend_r->s16WheelTargetPress = stPadMuCompDuRBCBlend_w->s16PadMuCompTp;
		}
	}

	stPadMuCompDuRBCBlend_w->s16PadMuCompTPDuRBCBlend = stPadMuCompDuRBCBlend_r->s16WheelTargetPress;

	stPadMuCompDuRBCBlend_w->s16TPwithoutCompOld = s16TPwithoutComp;
	stPadMuCompDuRBCBlend_w->s16WheelTargetPressOld = stPadMuCompDuRBCBlend_r->s16WheelTargetPress;
	stPadMuCompDuRBCBlend_w->s16RBCTPReductionOld = stPadMuCompDuRBCBlend_r->s16RBCTPReduction;

	return (*stPadMuCompDuRBCBlend_w);
}


static stPAD_MU_COMP_DU_RBC_BLEND_W_t LCARB_stCalcPadMuCompTP(stBRK_PAD_MU_COMP_INPUT_t *stBrkPadMuCompInput_r, stPAD_MU_COMP_DU_RBC_BLEND_W_t *stPadMuCompDuRBCBlend_w)
{
	int16_t s16TPComppPressForPadMu = S16_PCTRL_PRESS_0_BAR;
	int16_t s16TargetMin            = S16_PCTRL_PRESS_0_BAR;

	/* This code will be seperated into module - Start*/
	if((stBrkPadMuCompInput_r->s16RBCTPReduction > S16_PCTRL_PRESS_0_BAR) && (stPadMuCompDuRBCBlend_w->u8TPCompAfterPadMuChange == FALSE))		/* Only RBC Fade-out Blending with Pad Mu Comp Stage */
	{
		/* During Pad Mu Comp Stage */
		s16TPComppPressForPadMu = L_s16IInter2Point(stBrkPadMuCompInput_r->s16VehicleSpeed,
								  stBrkPadMuCompInput_r->s16RBCMinSpeed, stBrkPadMuCompInput_r->s16MaxPadMuCompP,
								  stPadMuCompDuRBCBlend_w->s16VSpdAtPadMuCompStart,  S16_PCTRL_PRESS_0_5_BAR );
		s16TargetMin = (int16_t)(((int32_t)stBrkPadMuCompInput_r->s16TPwithoutComp * stBrkPadMuCompInput_r->s16PadmuCompPecent) / S8_PERCENT_100_PRO);
		stPadMuCompDuRBCBlend_w->s16PadMuCompTp = L_s16LimitMinMax((stBrkPadMuCompInput_r->s16TPwithoutComp - s16TPComppPressForPadMu), s16TargetMin, S16_MPRESS_200_BAR);

		if((stPadMuCompDuRBCBlend_w->s16PadMuCompTp + stBrkPadMuCompInput_r->s16RBCTPReduction) < s16TargetMin)
		{
			stPadMuCompDuRBCBlend_w->s16PadMuCompTp = s16TargetMin - stBrkPadMuCompInput_r->s16RBCTPReduction;
		}
		else { }

		if (stBrkPadMuCompInput_r->s16TPwithoutCompOld <= stBrkPadMuCompInput_r->s16TPwithoutComp)
		{
			if(stBrkPadMuCompInput_r->s16WheelTargetPressOld > stPadMuCompDuRBCBlend_w->s16PadMuCompTp)
			{
				stPadMuCompDuRBCBlend_w->s16PadMuCompTp = stBrkPadMuCompInput_r->s16WheelTargetPressOld;
			}
			else { }
		}
		else { }

		stPadMuCompDuRBCBlend_w->s8TPRatioAfPadMuChange = (int8_t)(((int32_t)(stPadMuCompDuRBCBlend_w->s16PadMuCompTp + 1) * S8_PERCENT_100_PRO) / stBrkPadMuCompInput_r->s16TPwithoutComp);
		stPadMuCompDuRBCBlend_w->s8TPRatioAfPadMuChange  = (int8_t)L_s16LimitMinMax((stPadMuCompDuRBCBlend_w->s8TPRatioAfPadMuChange), S8_PERCENT_70_PRO, S8_PERCENT_100_PRO);
	}
	else		/* After Pad Mu Comp Stage and RBC Re-enter stage*/
	{
		/* After Pad Mu Comp Stage */
		stPadMuCompDuRBCBlend_w->u8TPCompAfterPadMuChange = TRUE;

		  #if (__AHB_LOW_SPEED_TARGET_P_LIMIT==ENABLE)
		if(stPadMuCompDuRBCBlend_w->s8TPRatioAfPadMuChange < S8_PERCENT_100_PRO)
		{
			if((stBrkPadMuCompInput_r->s16DriverIntendedTPDiff > S16_PCTRL_PRESS_0_BAR) || ((stBrkPadMuCompInput_r->s16HSAActiveBrkTP > stPadMuCompDuRBCBlend_w->s16PadMuCompTp) || (stBrkPadMuCompInput_r->s16AVHActiveBrkTP > stPadMuCompDuRBCBlend_w->s16PadMuCompTp)))
			{
				stPadMuCompDuRBCBlend_w->s8TPRatioAfPadMuChange = stPadMuCompDuRBCBlend_w->s8TPRatioAfPadMuChange + 1;
			}
			else { }
		}
		else		/* s8TPRatioAfPadMuChange == 100 */
		{
			/* Reset Pad Mu Comp */
			stPadMuCompDuRBCBlend_w->u8TPCompAfterPadMuChange = FALSE;
			stPadMuCompDuRBCBlend_w->u8TPCompForPadMuChange = FALSE;
			stPadMuCompDuRBCBlend_w->s16VSpdAtPadMuCompStart = 0;
			stPadMuCompDuRBCBlend_w->s16TotalBrakePress = S16_PCTRL_PRESS_0_BAR;
		}
		  #endif /* (__AHB_LOW_SPEED_TARGET_P_LIMIT==ENABLE) */

		stPadMuCompDuRBCBlend_w->s16PadMuCompTp = (stBrkPadMuCompInput_r->s16TPwithoutComp * (int16_t)stPadMuCompDuRBCBlend_w->s8TPRatioAfPadMuChange) / S8_PERCENT_100_PRO;
	}

	/* This code will be seperated into module - End*/

	return *stPadMuCompDuRBCBlend_w;
}

#endif /* __COMP_TP_FOR_PAD_MU_CHANGE */


#define ARB_CTRL_STOP_SEC_CODE
#include "Arb_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
