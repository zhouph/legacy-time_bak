/**
 * @defgroup LCARBCalRBCCooperTargetPRate LCARBCalRBCCooperTargetPRate
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LCARB_CalTargetPRate.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "LCARBCalRBCCooperTargetPRate.h"
#include "LCARBCallMain.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/


/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
	int16_t s16CtrlFLTarPFromRbcCooper;
	int16_t s16CtrlFRTarPFromRbcCooper;
	int16_t s16CtrlRLTarPFromRbcCooper;
	int16_t s16CtrlRRTarPFromRbcCooper;
}stARBTarPRateInput_t;

typedef struct
{

	int16_t s16CtrlFLTarPRateFromRbcCooper;
	int16_t s16CtrlFRTarPRateFromRbcCooper;
	int16_t s16CtrlRLTarPRateFromRbcCooper;
	int16_t s16CtrlRRTarPRateFromRbcCooper;
}stARBTarPRateOutput_t;

typedef struct{
	int16_t s16FLTarPRateOutput;
	int16_t s16FLTarPRateOutputX10;
	int16_t s16FLTarPRateOutputBarPSec;
	int16_t s16FLTarPRateOutput10ms;

	int16_t s16FRTarPRateOutput;
	int16_t s16FRTarPRateOutputX10;
	int16_t s16FRTarPRateOutputBarPSec;
	int16_t s16FRTarPRateOutput10ms;

	int16_t s16RLTarPRateOutput;
	int16_t s16RLTarPRateOutputX10;
	int16_t s16RLTarPRateOutputBarPSec;
	int16_t s16RLTarPRateOutput10ms;

	int16_t s16RRTarPRateOutput;
	int16_t s16RRTarPRateOutputX10;
	int16_t s16RRTarPRateOutputBarPSec;
	int16_t s16RRTarPRateOutput10ms;
} stCalcTarPRateOutput_t;



/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ARB_CTRL_START_SEC_CONST_UNSPECIFIED
#include "Arb_MemMap.h"

/** Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/

#define ARB_STOP_SEC_CONST_UNSPECIFIED
#include "Arb_MemMap.h"

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ARB_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arb_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ARB_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arb_MemMap.h"
#define ARB_START_SEC_VAR_NOINIT_32BIT
#include "Arb_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ARB_STOP_SEC_VAR_NOINIT_32BIT
#include "Arb_MemMap.h"
#define ARB_START_SEC_VAR_UNSPECIFIED
#include "Arb_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ARB_STOP_SEC_VAR_UNSPECIFIED
#include "Arb_MemMap.h"
#define ARB_START_SEC_VAR_32BIT
#include "Arb_MemMap.h"
/** Variable Section (32BIT)**/


#define ARB_STOP_SEC_VAR_32BIT
#include "Arb_MemMap.h"


stRBCTarPRate_t      stRBCTarPRate = {0,0,0,0};

/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ARB_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arb_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ARB_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arb_MemMap.h"
#define ARB_START_SEC_VAR_NOINIT_32BIT
#include "Arb_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ARB_STOP_SEC_VAR_NOINIT_32BIT
#include "Arb_MemMap.h"
#define ARB_START_SEC_VAR_UNSPECIFIED
#include "Arb_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define ARB_STOP_SEC_VAR_UNSPECIFIED
#include "Arb_MemMap.h"
#define ARB_START_SEC_VAR_32BIT
#include "Arb_MemMap.h"
/** Variable Section (32BIT)**/

#define ARB_STOP_SEC_VAR_32BIT
#include "Arb_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ARB_START_SEC_CODE
#include "Arb_MemMap.h"

static stARBTarPRateInput_t LCARB_stGetRBCCooperTarPRateInputVar(void);
static void LCARB_vSetTarPRateOutputVar(stARBTarPRateOutput_t stARBTarPRateOutput);
static stARBTarPRateOutput_t LCARB_stCalcTargetPressRate(stARBTarPRateInput_t stARBTarPRateInput);
static int16_t LCARB_s16CalcStdTargetPressRate(int16_t s16TargetPressInput);
static int16_t LCARB_s16CalcTarPRate4BarPSec(int16_t s16TargetPressInput);
static int16_t LCARB_s16CalcTarPRateDot4BarPSec(int16_t s16TargetPressInput);
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void LCARB_vCalcRBCCooperTarPRate(void)
{
	stARBTarPRateInput_t      stARBTarPRateInput = {0,0,0,0};
	stARBTarPRateOutput_t     stARBTarPRateOutput = {0,0,0,0};

	stARBTarPRateInput = LCARB_stGetRBCCooperTarPRateInputVar();

	stARBTarPRateOutput = LCARB_stCalcTargetPressRate(stARBTarPRateInput);

	LCARB_vSetTarPRateOutputVar(stARBTarPRateOutput);
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static stARBTarPRateInput_t LCARB_stGetRBCCooperTarPRateInputVar(void)
{
	stARBTarPRateInput_t      stARBTarPRateInput;
	stARBTarPRateInput.s16CtrlFLTarPFromRbcCooper = (int16_t)ARB_FL.lcarbu16SetTP;
	stARBTarPRateInput.s16CtrlFRTarPFromRbcCooper = (int16_t)ARB_FR.lcarbu16SetTP;
	stARBTarPRateInput.s16CtrlFLTarPFromRbcCooper = (int16_t)ARB_RL.lcarbu16SetTP;
	stARBTarPRateInput.s16CtrlFRTarPFromRbcCooper = (int16_t)ARB_RR.lcarbu16SetTP;
	return stARBTarPRateInput;
}

static void LCARB_vSetTarPRateOutputVar(stARBTarPRateOutput_t stARBTarPRateOutput)
{

	stRBCTarPRate.FLTarPRate  = stARBTarPRateOutput.s16CtrlFLTarPRateFromRbcCooper;
	stRBCTarPRate.FRTarPRate  = stARBTarPRateOutput.s16CtrlFRTarPRateFromRbcCooper;
	stRBCTarPRate.RLTarPRate  = stARBTarPRateOutput.s16CtrlRLTarPRateFromRbcCooper;
	stRBCTarPRate.RRTarPRate   = stARBTarPRateOutput.s16CtrlRRTarPRateFromRbcCooper;

}

static stARBTarPRateOutput_t LCARB_stCalcTargetPressRate(stARBTarPRateInput_t stARBTarPRateInput)
{
	int16_t s16FLTargetPressInput = stARBTarPRateInput.s16CtrlFLTarPFromRbcCooper;
	int16_t s16FRTargetPressInput = stARBTarPRateInput.s16CtrlFRTarPFromRbcCooper;
	int16_t s16RLTargetPressInput = stARBTarPRateInput.s16CtrlFRTarPFromRbcCooper;
	int16_t s16RRTargetPressInput = stARBTarPRateInput.s16CtrlFRTarPFromRbcCooper;

	stCalcTarPRateOutput_t stCalcTarPRateOutput = {0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0};

	stARBTarPRateOutput_t stARBTarPRateOutput = {0,0,0,0};


	/**********************************************************/
	/* Target Press rate (1 = 0.1bar/25ms = 4bar/s)           */
	/**********************************************************/
	stCalcTarPRateOutput.s16FLTarPRateOutput = LCARB_s16CalcTarPRate4BarPSec(s16FLTargetPressInput);
	stCalcTarPRateOutput.s16FRTarPRateOutput = LCARB_s16CalcTarPRate4BarPSec(s16FRTargetPressInput);
	stCalcTarPRateOutput.s16RLTarPRateOutput = LCARB_s16CalcTarPRate4BarPSec(s16RLTargetPressInput);
	stCalcTarPRateOutput.s16RRTarPRateOutput = LCARB_s16CalcTarPRate4BarPSec(s16RRTargetPressInput);
	/**********************************************************/
	/* Target Press rate (1 = 0.01bar/25ms = 0.4bar/s)        */
	/**********************************************************/
	stCalcTarPRateOutput.s16FLTarPRateOutputX10 = LCARB_s16CalcTarPRateDot4BarPSec(s16FLTargetPressInput);
	stCalcTarPRateOutput.s16FRTarPRateOutputX10 = LCARB_s16CalcTarPRateDot4BarPSec(s16FRTargetPressInput);
	stCalcTarPRateOutput.s16RLTarPRateOutputX10 = LCARB_s16CalcTarPRateDot4BarPSec(s16RLTargetPressInput);
	stCalcTarPRateOutput.s16RRTarPRateOutputX10 = LCARB_s16CalcTarPRateDot4BarPSec(s16RRTargetPressInput);


	/**********************************************************/
	/* Target Press rate (1 = 1bar/s)                         */
	/**********************************************************/
	stCalcTarPRateOutput.s16FLTarPRateOutputBarPSec = (stCalcTarPRateOutput.s16FLTarPRateOutputX10*2)/5;
	stCalcTarPRateOutput.s16FRTarPRateOutputBarPSec = (stCalcTarPRateOutput.s16FRTarPRateOutputX10*2)/5;
	stCalcTarPRateOutput.s16RLTarPRateOutputBarPSec = (stCalcTarPRateOutput.s16RLTarPRateOutputX10*2)/5;
	stCalcTarPRateOutput.s16RRTarPRateOutputBarPSec = (stCalcTarPRateOutput.s16RRTarPRateOutputX10*2)/5;

	/**********************************************************/
	/* Target Press rate 10ms (1 = 1bar/s)                         */
	/**********************************************************/
	stCalcTarPRateOutput.s16FLTarPRateOutput10ms = LCARB_s16CalcStdTargetPressRate(s16FLTargetPressInput);
	stCalcTarPRateOutput.s16FRTarPRateOutput10ms = LCARB_s16CalcStdTargetPressRate(s16FRTargetPressInput);
	stCalcTarPRateOutput.s16RLTarPRateOutput10ms = LCARB_s16CalcStdTargetPressRate(s16RLTargetPressInput);
	stCalcTarPRateOutput.s16RRTarPRateOutput10ms = LCARB_s16CalcStdTargetPressRate(s16RRTargetPressInput);


	/* USE Target Press rate 10ms (1 = 1bar/s) */
	stARBTarPRateOutput.s16CtrlFLTarPRateFromRbcCooper = stCalcTarPRateOutput.s16FLTarPRateOutput10ms;
	stARBTarPRateOutput.s16CtrlFRTarPRateFromRbcCooper = stCalcTarPRateOutput.s16FRTarPRateOutput10ms;
	stARBTarPRateOutput.s16CtrlRLTarPRateFromRbcCooper = stCalcTarPRateOutput.s16RLTarPRateOutput10ms;
	stARBTarPRateOutput.s16CtrlRRTarPRateFromRbcCooper = stCalcTarPRateOutput.s16RRTarPRateOutput10ms;

	return stARBTarPRateOutput;
}

static int16_t LCARB_s16CalcStdTargetPressRate(int16_t s16TargetPressInput)
{
	int16_t s16TGPressBuffCntBf2scan = 0;
	int16_t s16FilterInput = 0;
	int32_t s32FilterInputTemp = 0;

	static int16_t lcRBCCoopers16PressBuff[5] = {0,0,0,0,0};
	static int16_t lcRBCCoopers16BuffCnt = 0;
	static int16_t lcRBCCoopers16PressRate10ms = 0;
	static int16_t lcRBCCoopers16PressRate5ms = 0;

	lcRBCCoopers16PressBuff[lcRBCCoopers16BuffCnt] = s16TargetPressInput;

	lcRBCCoopers16BuffCnt = lcRBCCoopers16BuffCnt + 1;
    if(lcRBCCoopers16BuffCnt>=5)
    {
    	lcRBCCoopers16BuffCnt = 0;
    }
    else{ }

	/**********************************************************/
	/* Target Press rate (1 = 0.01bar/10ms = 1bar/s)          */
	/**********************************************************/
    if(lcRBCCoopers16BuffCnt>=3)
    {
    	s16TGPressBuffCntBf2scan = lcRBCCoopers16BuffCnt - 3;
    }
    else
    {
    	s16TGPressBuffCntBf2scan = lcRBCCoopers16BuffCnt + 2;
    }

	s32FilterInputTemp = ((int32_t)s16TargetPressInput - (int32_t)lcRBCCoopers16PressBuff[s16TGPressBuffCntBf2scan]); /* target press variation for 2scan. changed algorithm for simplicity in Feb. 2015 -> already checked both algorithms*/

	s16FilterInput = (int16_t)L_s32LimitMinMax(s32FilterInputTemp, ASW_S16_MIN, ASW_S16_MAX);

	lcRBCCoopers16PressRate10ms =(L_s16Lpf1Int(s16FilterInput, lcRBCCoopers16PressRate10ms, U8_LPF_7HZ_128G)); /* 1 bar/sec*/

	/**********************************************************/
	/* Target Press rate (1 = 0.01bar/5ms *2 = 1bar/s)         */
	/**********************************************************/
    if(lcRBCCoopers16BuffCnt>=2)
    {
    	s16TGPressBuffCntBf2scan = lcRBCCoopers16BuffCnt - 2;
    }
    else
    {
    	s16TGPressBuffCntBf2scan = lcRBCCoopers16BuffCnt + 3;
    }

	s32FilterInputTemp = (int32_t)s16TargetPressInput - (int32_t)lcRBCCoopers16PressBuff[s16TGPressBuffCntBf2scan]; /* target press variation for 1 scan */

	s16FilterInput = (int16_t)L_s32LimitMinMax(s32FilterInputTemp, ASW_S16_MIN, ASW_S16_MAX);

	lcRBCCoopers16PressRate5ms = s16FilterInput * 2;          /* not filtered */

	return lcRBCCoopers16PressRate10ms;
}

static int16_t LCARB_s16CalcTarPRate4BarPSec(int16_t s16TargetPressInput)
{
	static int16_t lcRBCCoopers16TarPRateOutput = 0;
	static int16_t lcRBCCoopers16TargetPressAvg = 0;
	static int16_t lcRBCCoopers16TargetPressSum = 0;
	static int16_t lcRBCCoopers16TargetPressBuff[5] = {0,0,0,0,0};
	static uint8_t lcRBCCooperu8TargetPressAvgCnt = 0;
	static int16_t lcRBCCoopers16TargetPressAvgBuff[5] = {0,0,0,0,0};
	static uint8_t lcRBCCooperu8TargetPressRateCnt = 0;

	lcRBCCoopers16TargetPressSum = (lcRBCCoopers16TargetPressSum - (lcRBCCoopers16TargetPressBuff[lcRBCCooperu8TargetPressAvgCnt])) + s16TargetPressInput;
	lcRBCCoopers16TargetPressAvg = lcRBCCoopers16TargetPressSum/5;

	lcRBCCoopers16TargetPressBuff[lcRBCCooperu8TargetPressAvgCnt] = s16TargetPressInput;
	lcRBCCooperu8TargetPressAvgCnt = lcRBCCooperu8TargetPressAvgCnt + 1;

	if(lcRBCCooperu8TargetPressAvgCnt>=5)
	{
		lcRBCCooperu8TargetPressAvgCnt=0;
	}
	else{ }

	lcRBCCoopers16TarPRateOutput = L_s16Lpf1Int((int16_t)(lcRBCCoopers16TargetPressAvg - lcRBCCoopers16TargetPressAvgBuff[lcRBCCooperu8TargetPressRateCnt]),(int16_t)lcRBCCoopers16TarPRateOutput, (uint8_t)U8_LPF_7HZ_128G);
	lcRBCCoopers16TargetPressAvgBuff[lcRBCCooperu8TargetPressRateCnt] = lcRBCCoopers16TargetPressAvg;

	lcRBCCooperu8TargetPressRateCnt = lcRBCCooperu8TargetPressRateCnt + 1;

	if(lcRBCCooperu8TargetPressRateCnt>=5)
	{
		lcRBCCooperu8TargetPressRateCnt = 0;
	}
	else{ }

	return lcRBCCoopers16TarPRateOutput;
}

static int16_t LCARB_s16CalcTarPRateDot4BarPSec(int16_t s16TargetPressInput)
{
	int16_t s16FilterInput = 0;
	static int16_t lcRBCCoopers16TargetPressBuffX10[5] = {0,0,0,0,0};
	static int16_t lcRBCCoopers16TarPRateOutputX10 = 0;
	static uint8_t lcRBCCooperu8TargetPressBuffCnt = 0;

	s16FilterInput = s16TargetPressInput - lcRBCCoopers16TargetPressBuffX10[lcRBCCooperu8TargetPressBuffCnt]; /* target press variation for 5scan */

	lcRBCCoopers16TarPRateOutputX10 = L_s16Lpf1Int(s16FilterInput, lcRBCCoopers16TarPRateOutputX10, U8_LPF_7HZ_128G);          /* 7Hz */

	lcRBCCoopers16TargetPressBuffX10[lcRBCCooperu8TargetPressBuffCnt] = s16TargetPressInput;

	lcRBCCooperu8TargetPressBuffCnt = lcRBCCooperu8TargetPressBuffCnt + 1;
	if(lcRBCCooperu8TargetPressBuffCnt>=5)
	{
		lcRBCCooperu8TargetPressBuffCnt = 0;
	}
	else{ }

	return lcRBCCoopers16TarPRateOutputX10;
}

#define ARB_STOP_SEC_CODE
#include "ARB_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
