/**
 * @defgroup Arb_Types Arb_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Arb_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ARB_TYPES_H_
#define ARB_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Asw_Types.h" /* HSH */
#include "Bsw_Types.h"
#include "Rte_Types.h"
#include "RteDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Abc_CtrlWhlVlvReqAbcInfo_t Arb_CtrlWhlVlvReqAbcInfo;
    Abc_CtrlAbsCtrlInfo_t Arb_CtrlAbsCtrlInfo;
    Abc_CtrlAvhCtrlInfo_t Arb_CtrlAvhCtrlInfo;
    Abc_CtrlBaCtrlInfo_t Arb_CtrlBaCtrlInfo;
    Abc_CtrlEbdCtrlInfo_t Arb_CtrlEbdCtrlInfo;
    Abc_CtrlEbpCtrlInfo_t Arb_CtrlEbpCtrlInfo;
    Abc_CtrlEpbiCtrlInfo_t Arb_CtrlEpbiCtrlInfo;
    Abc_CtrlEscCtrlInfo_t Arb_CtrlEscCtrlInfo;
    Abc_CtrlHsaCtrlInfo_t Arb_CtrlHsaCtrlInfo;
    Abc_CtrlSccCtrlInfo_t Arb_CtrlSccCtrlInfo;
    Abc_CtrlTcsCtrlInfo_t Arb_CtrlTcsCtrlInfo;
    Abc_CtrlTvbbCtrlInfo_t Arb_CtrlTvbbCtrlInfo;
    Bbc_CtrlBaseBrkCtrlModInfo_t Arb_CtrlBaseBrkCtrlModInfo;
    Bbc_CtrlBBCCtrlInfo_t Arb_CtrlBBCCtrlInfo;
    Mom_HndlrEcuModeSts_t Arb_CtrlEcuModeSts;
    Prly_HndlrIgnOnOffSts_t Arb_CtrlIgnOnOffSts;
    Eem_SuspcDetnFuncInhibitArbiSts_t Arb_CtrlFuncInhibitArbiSts;
    Bbc_CtrlBaseBrkCtrlrActFlg_t Arb_CtrlBaseBrkCtrlrActFlg;
    Rbc_CtrlBrkPRednForBaseBrkCtrlr_t Arb_CtrlBrkPRednForBaseBrkCtrlr;
    Pct_5msCtrlPCtrlSt_t Arb_CtrlPCtrlSt;
    Rbc_CtrlRgnBrkCtrlrActStFlg_t Arb_CtrlRgnBrkCtrlrActStFlg;
    Bbc_CtrlTarPFromBaseBrkCtrlr_t Arb_CtrlTarPFromBaseBrkCtrlr;
    Bbc_CtrlTarPRateFromBaseBrkCtrlr_t Arb_CtrlTarPRateFromBaseBrkCtrlr;
    Det_5msCtrlVehSpdFild_t Arb_CtrlVehSpdFild;

/* Output Data Element */
    Arb_CtrlFinalTarPInfo_t Arb_CtrlFinalTarPInfo;
    Arb_CtrlWhlOutVlvCtrlTarInfo_t Arb_CtrlWhlOutVlvCtrlTarInfo;
    Arb_CtrlWhlPreFillInfo_t Arb_CtrlWhlPreFillInfo;
}Arb_Ctrl_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ARB_TYPES_H_ */
/** @} */
