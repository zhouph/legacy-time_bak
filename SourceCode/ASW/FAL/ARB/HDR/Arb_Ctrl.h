/**
 * @defgroup Arb_Ctrl Arb_Ctrl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Arb_Ctrl.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ARB_CTRL_H_
#define ARB_CTRL_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Arb_Types.h"
#include "Arb_Cfg.h"
#include "Arb_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define ARB_CTRL_MODULE_ID      (0)
 #define ARB_CTRL_MAJOR_VERSION  (2)
 #define ARB_CTRL_MINOR_VERSION  (0)
 #define ARB_CTRL_PATCH_VERSION  (0)
 #define ARB_CTRL_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Arb_Ctrl_HdrBusType Arb_CtrlBus;

/* Version Info */
extern const SwcVersionInfo_t Arb_CtrlVersionInfo;

/* Input Data Element */
extern Abc_CtrlWhlVlvReqAbcInfo_t Arb_CtrlWhlVlvReqAbcInfo;
extern Abc_CtrlAbsCtrlInfo_t Arb_CtrlAbsCtrlInfo;
extern Abc_CtrlAvhCtrlInfo_t Arb_CtrlAvhCtrlInfo;
extern Abc_CtrlBaCtrlInfo_t Arb_CtrlBaCtrlInfo;
extern Abc_CtrlEbdCtrlInfo_t Arb_CtrlEbdCtrlInfo;
extern Abc_CtrlEbpCtrlInfo_t Arb_CtrlEbpCtrlInfo;
extern Abc_CtrlEpbiCtrlInfo_t Arb_CtrlEpbiCtrlInfo;
extern Abc_CtrlEscCtrlInfo_t Arb_CtrlEscCtrlInfo;
extern Abc_CtrlHsaCtrlInfo_t Arb_CtrlHsaCtrlInfo;
extern Abc_CtrlSccCtrlInfo_t Arb_CtrlSccCtrlInfo;
extern Abc_CtrlTcsCtrlInfo_t Arb_CtrlTcsCtrlInfo;
extern Abc_CtrlTvbbCtrlInfo_t Arb_CtrlTvbbCtrlInfo;
extern Bbc_CtrlBaseBrkCtrlModInfo_t Arb_CtrlBaseBrkCtrlModInfo;
extern Bbc_CtrlBBCCtrlInfo_t Arb_CtrlBBCCtrlInfo;
extern Mom_HndlrEcuModeSts_t Arb_CtrlEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t Arb_CtrlIgnOnOffSts;
extern Eem_SuspcDetnFuncInhibitArbiSts_t Arb_CtrlFuncInhibitArbiSts;
extern Bbc_CtrlBaseBrkCtrlrActFlg_t Arb_CtrlBaseBrkCtrlrActFlg;
extern Rbc_CtrlBrkPRednForBaseBrkCtrlr_t Arb_CtrlBrkPRednForBaseBrkCtrlr;
extern Pct_5msCtrlPCtrlSt_t Arb_CtrlPCtrlSt;
extern Rbc_CtrlRgnBrkCtrlrActStFlg_t Arb_CtrlRgnBrkCtrlrActStFlg;
extern Bbc_CtrlTarPFromBaseBrkCtrlr_t Arb_CtrlTarPFromBaseBrkCtrlr;
extern Bbc_CtrlTarPRateFromBaseBrkCtrlr_t Arb_CtrlTarPRateFromBaseBrkCtrlr;
extern Det_5msCtrlVehSpdFild_t Arb_CtrlVehSpdFild;

/* Output Data Element */
extern Arb_CtrlFinalTarPInfo_t Arb_CtrlFinalTarPInfo;
extern Arb_CtrlWhlOutVlvCtrlTarInfo_t Arb_CtrlWhlOutVlvCtrlTarInfo;
extern Arb_CtrlWhlPreFillInfo_t Arb_CtrlWhlPreFillInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Arb_Ctrl_Init(void);
extern void Arb_Ctrl(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ARB_CTRL_H_ */
/** @} */
