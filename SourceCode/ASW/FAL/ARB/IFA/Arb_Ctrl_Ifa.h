/**
 * @defgroup Arb_Ctrl_Ifa Arb_Ctrl_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Arb_Ctrl_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ARB_CTRL_IFA_H_
#define ARB_CTRL_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Arb_Ctrl_Read_Arb_CtrlWhlVlvReqAbcInfo(data) do \
{ \
    *data = Arb_CtrlWhlVlvReqAbcInfo; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlAbsCtrlInfo(data) do \
{ \
    *data = Arb_CtrlAbsCtrlInfo; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlAvhCtrlInfo(data) do \
{ \
    *data = Arb_CtrlAvhCtrlInfo; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlBaCtrlInfo(data) do \
{ \
    *data = Arb_CtrlBaCtrlInfo; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEbdCtrlInfo(data) do \
{ \
    *data = Arb_CtrlEbdCtrlInfo; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEbpCtrlInfo(data) do \
{ \
    *data = Arb_CtrlEbpCtrlInfo; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEpbiCtrlInfo(data) do \
{ \
    *data = Arb_CtrlEpbiCtrlInfo; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEscCtrlInfo(data) do \
{ \
    *data = Arb_CtrlEscCtrlInfo; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlHsaCtrlInfo(data) do \
{ \
    *data = Arb_CtrlHsaCtrlInfo; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlSccCtrlInfo(data) do \
{ \
    *data = Arb_CtrlSccCtrlInfo; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlTcsCtrlInfo(data) do \
{ \
    *data = Arb_CtrlTcsCtrlInfo; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlTvbbCtrlInfo(data) do \
{ \
    *data = Arb_CtrlTvbbCtrlInfo; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlBaseBrkCtrlModInfo(data) do \
{ \
    *data = Arb_CtrlBaseBrkCtrlModInfo; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlBBCCtrlInfo(data) do \
{ \
    *data = Arb_CtrlBBCCtrlInfo; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlWhlVlvReqAbcInfo_FlOvReqData(data) do \
{ \
    for(i=0;i<10;i++) *data[i] = Arb_CtrlWhlVlvReqAbcInfo.FlOvReqData[i]; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlWhlVlvReqAbcInfo_FrOvReqData(data) do \
{ \
    for(i=0;i<10;i++) *data[i] = Arb_CtrlWhlVlvReqAbcInfo.FrOvReqData[i]; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlWhlVlvReqAbcInfo_RlOvReqData(data) do \
{ \
    for(i=0;i<10;i++) *data[i] = Arb_CtrlWhlVlvReqAbcInfo.RlOvReqData[i]; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlWhlVlvReqAbcInfo_RrOvReqData(data) do \
{ \
    for(i=0;i<10;i++) *data[i] = Arb_CtrlWhlVlvReqAbcInfo.RrOvReqData[i]; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlWhlVlvReqAbcInfo_FlOvReq(data) do \
{ \
    *data = Arb_CtrlWhlVlvReqAbcInfo.FlOvReq; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlWhlVlvReqAbcInfo_FrOvReq(data) do \
{ \
    *data = Arb_CtrlWhlVlvReqAbcInfo.FrOvReq; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlWhlVlvReqAbcInfo_RlOvReq(data) do \
{ \
    *data = Arb_CtrlWhlVlvReqAbcInfo.RlOvReq; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlWhlVlvReqAbcInfo_RrOvReq(data) do \
{ \
    *data = Arb_CtrlWhlVlvReqAbcInfo.RrOvReq; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlAbsCtrlInfo_AbsActFlg(data) do \
{ \
    *data = Arb_CtrlAbsCtrlInfo.AbsActFlg; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlAbsCtrlInfo_AbsDefectFlg(data) do \
{ \
    *data = Arb_CtrlAbsCtrlInfo.AbsDefectFlg; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlAbsCtrlInfo_AbsTarPFrntLe(data) do \
{ \
    *data = Arb_CtrlAbsCtrlInfo.AbsTarPFrntLe; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlAbsCtrlInfo_AbsTarPFrntRi(data) do \
{ \
    *data = Arb_CtrlAbsCtrlInfo.AbsTarPFrntRi; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlAbsCtrlInfo_AbsTarPReLe(data) do \
{ \
    *data = Arb_CtrlAbsCtrlInfo.AbsTarPReLe; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlAbsCtrlInfo_AbsTarPReRi(data) do \
{ \
    *data = Arb_CtrlAbsCtrlInfo.AbsTarPReRi; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlAbsCtrlInfo_FrntWhlSlip(data) do \
{ \
    *data = Arb_CtrlAbsCtrlInfo.FrntWhlSlip; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlAbsCtrlInfo_AbsDesTarP(data) do \
{ \
    *data = Arb_CtrlAbsCtrlInfo.AbsDesTarP; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlAbsCtrlInfo_AbsDesTarPReqFlg(data) do \
{ \
    *data = Arb_CtrlAbsCtrlInfo.AbsDesTarPReqFlg; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlAbsCtrlInfo_AbsCtrlFadeOutFlg(data) do \
{ \
    *data = Arb_CtrlAbsCtrlInfo.AbsCtrlFadeOutFlg; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlAbsCtrlInfo_AbsCtrlModeFrntLe(data) do \
{ \
    *data = Arb_CtrlAbsCtrlInfo.AbsCtrlModeFrntLe; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlAbsCtrlInfo_AbsCtrlModeFrntRi(data) do \
{ \
    *data = Arb_CtrlAbsCtrlInfo.AbsCtrlModeFrntRi; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlAbsCtrlInfo_AbsCtrlModeReLe(data) do \
{ \
    *data = Arb_CtrlAbsCtrlInfo.AbsCtrlModeReLe; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlAbsCtrlInfo_AbsCtrlModeReRi(data) do \
{ \
    *data = Arb_CtrlAbsCtrlInfo.AbsCtrlModeReRi; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlAbsCtrlInfo_AbsTarPRateFrntLe(data) do \
{ \
    *data = Arb_CtrlAbsCtrlInfo.AbsTarPRateFrntLe; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlAbsCtrlInfo_AbsTarPRateFrntRi(data) do \
{ \
    *data = Arb_CtrlAbsCtrlInfo.AbsTarPRateFrntRi; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlAbsCtrlInfo_AbsTarPRateReLe(data) do \
{ \
    *data = Arb_CtrlAbsCtrlInfo.AbsTarPRateReLe; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlAbsCtrlInfo_AbsTarPRateReRi(data) do \
{ \
    *data = Arb_CtrlAbsCtrlInfo.AbsTarPRateReRi; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlAbsCtrlInfo_AbsPrioFrntLe(data) do \
{ \
    *data = Arb_CtrlAbsCtrlInfo.AbsPrioFrntLe; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlAbsCtrlInfo_AbsPrioFrntRi(data) do \
{ \
    *data = Arb_CtrlAbsCtrlInfo.AbsPrioFrntRi; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlAbsCtrlInfo_AbsPrioReLe(data) do \
{ \
    *data = Arb_CtrlAbsCtrlInfo.AbsPrioReLe; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlAbsCtrlInfo_AbsPrioReRi(data) do \
{ \
    *data = Arb_CtrlAbsCtrlInfo.AbsPrioReRi; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlAbsCtrlInfo_AbsDelTarPFrntLe(data) do \
{ \
    *data = Arb_CtrlAbsCtrlInfo.AbsDelTarPFrntLe; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlAbsCtrlInfo_AbsDelTarPFrntRi(data) do \
{ \
    *data = Arb_CtrlAbsCtrlInfo.AbsDelTarPFrntRi; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlAbsCtrlInfo_AbsDelTarPReLe(data) do \
{ \
    *data = Arb_CtrlAbsCtrlInfo.AbsDelTarPReLe; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlAbsCtrlInfo_AbsDelTarPReRi(data) do \
{ \
    *data = Arb_CtrlAbsCtrlInfo.AbsDelTarPReRi; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlAvhCtrlInfo_AvhActFlg(data) do \
{ \
    *data = Arb_CtrlAvhCtrlInfo.AvhActFlg; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlAvhCtrlInfo_AvhDefectFlg(data) do \
{ \
    *data = Arb_CtrlAvhCtrlInfo.AvhDefectFlg; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlAvhCtrlInfo_AvhTarP(data) do \
{ \
    *data = Arb_CtrlAvhCtrlInfo.AvhTarP; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlBaCtrlInfo_BaActFlg(data) do \
{ \
    *data = Arb_CtrlBaCtrlInfo.BaActFlg; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlBaCtrlInfo_BaCDefectFlg(data) do \
{ \
    *data = Arb_CtrlBaCtrlInfo.BaCDefectFlg; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlBaCtrlInfo_BaTarP(data) do \
{ \
    *data = Arb_CtrlBaCtrlInfo.BaTarP; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEbdCtrlInfo_EbdActFlg(data) do \
{ \
    *data = Arb_CtrlEbdCtrlInfo.EbdActFlg; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEbdCtrlInfo_EbdDefectFlg(data) do \
{ \
    *data = Arb_CtrlEbdCtrlInfo.EbdDefectFlg; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEbdCtrlInfo_EbdTarPFrntLe(data) do \
{ \
    *data = Arb_CtrlEbdCtrlInfo.EbdTarPFrntLe; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEbdCtrlInfo_EbdTarPFrntRi(data) do \
{ \
    *data = Arb_CtrlEbdCtrlInfo.EbdTarPFrntRi; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEbdCtrlInfo_EbdTarPReLe(data) do \
{ \
    *data = Arb_CtrlEbdCtrlInfo.EbdTarPReLe; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEbdCtrlInfo_EbdTarPReRi(data) do \
{ \
    *data = Arb_CtrlEbdCtrlInfo.EbdTarPReRi; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEbdCtrlInfo_EbdTarPRateReLe(data) do \
{ \
    *data = Arb_CtrlEbdCtrlInfo.EbdTarPRateReLe; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEbdCtrlInfo_EbdTarPRateReRi(data) do \
{ \
    *data = Arb_CtrlEbdCtrlInfo.EbdTarPRateReRi; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEbdCtrlInfo_EbdCtrlModeReLe(data) do \
{ \
    *data = Arb_CtrlEbdCtrlInfo.EbdCtrlModeReLe; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEbdCtrlInfo_EbdCtrlModeReRi(data) do \
{ \
    *data = Arb_CtrlEbdCtrlInfo.EbdCtrlModeReRi; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEbdCtrlInfo_EbdDelTarPReLe(data) do \
{ \
    *data = Arb_CtrlEbdCtrlInfo.EbdDelTarPReLe; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEbdCtrlInfo_EbdDelTarPReRi(data) do \
{ \
    *data = Arb_CtrlEbdCtrlInfo.EbdDelTarPReRi; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEbpCtrlInfo_EbpActFlg(data) do \
{ \
    *data = Arb_CtrlEbpCtrlInfo.EbpActFlg; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEbpCtrlInfo_EbpDefectFlg(data) do \
{ \
    *data = Arb_CtrlEbpCtrlInfo.EbpDefectFlg; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEbpCtrlInfo_EbpTarP(data) do \
{ \
    *data = Arb_CtrlEbpCtrlInfo.EbpTarP; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEpbiCtrlInfo_EpbiActFlg(data) do \
{ \
    *data = Arb_CtrlEpbiCtrlInfo.EpbiActFlg; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEpbiCtrlInfo_EpbiDefectFlg(data) do \
{ \
    *data = Arb_CtrlEpbiCtrlInfo.EpbiDefectFlg; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEpbiCtrlInfo_EpbiTarP(data) do \
{ \
    *data = Arb_CtrlEpbiCtrlInfo.EpbiTarP; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEscCtrlInfo_EscActFlg(data) do \
{ \
    *data = Arb_CtrlEscCtrlInfo.EscActFlg; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEscCtrlInfo_EscDefectFlg(data) do \
{ \
    *data = Arb_CtrlEscCtrlInfo.EscDefectFlg; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEscCtrlInfo_EscTarPFrntLe(data) do \
{ \
    *data = Arb_CtrlEscCtrlInfo.EscTarPFrntLe; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEscCtrlInfo_EscTarPFrntRi(data) do \
{ \
    *data = Arb_CtrlEscCtrlInfo.EscTarPFrntRi; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEscCtrlInfo_EscTarPReLe(data) do \
{ \
    *data = Arb_CtrlEscCtrlInfo.EscTarPReLe; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEscCtrlInfo_EscTarPReRi(data) do \
{ \
    *data = Arb_CtrlEscCtrlInfo.EscTarPReRi; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEscCtrlInfo_EscCtrlModeFrntLe(data) do \
{ \
    *data = Arb_CtrlEscCtrlInfo.EscCtrlModeFrntLe; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEscCtrlInfo_EscCtrlModeFrntRi(data) do \
{ \
    *data = Arb_CtrlEscCtrlInfo.EscCtrlModeFrntRi; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEscCtrlInfo_EscCtrlModeReLe(data) do \
{ \
    *data = Arb_CtrlEscCtrlInfo.EscCtrlModeReLe; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEscCtrlInfo_EscCtrlModeReRi(data) do \
{ \
    *data = Arb_CtrlEscCtrlInfo.EscCtrlModeReRi; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEscCtrlInfo_EscTarPRateFrntLe(data) do \
{ \
    *data = Arb_CtrlEscCtrlInfo.EscTarPRateFrntLe; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEscCtrlInfo_EscTarPRateFrntRi(data) do \
{ \
    *data = Arb_CtrlEscCtrlInfo.EscTarPRateFrntRi; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEscCtrlInfo_EscTarPRateReLe(data) do \
{ \
    *data = Arb_CtrlEscCtrlInfo.EscTarPRateReLe; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEscCtrlInfo_EscTarPRateReRi(data) do \
{ \
    *data = Arb_CtrlEscCtrlInfo.EscTarPRateReRi; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEscCtrlInfo_EscPrioFrntLe(data) do \
{ \
    *data = Arb_CtrlEscCtrlInfo.EscPrioFrntLe; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEscCtrlInfo_EscPrioFrntRi(data) do \
{ \
    *data = Arb_CtrlEscCtrlInfo.EscPrioFrntRi; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEscCtrlInfo_EscPrioReLe(data) do \
{ \
    *data = Arb_CtrlEscCtrlInfo.EscPrioReLe; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEscCtrlInfo_EscPrioReRi(data) do \
{ \
    *data = Arb_CtrlEscCtrlInfo.EscPrioReRi; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEscCtrlInfo_EscPreCtrlModeFrntLe(data) do \
{ \
    *data = Arb_CtrlEscCtrlInfo.EscPreCtrlModeFrntLe; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEscCtrlInfo_EscPreCtrlModeFrntRi(data) do \
{ \
    *data = Arb_CtrlEscCtrlInfo.EscPreCtrlModeFrntRi; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEscCtrlInfo_EscPreCtrlModeReLe(data) do \
{ \
    *data = Arb_CtrlEscCtrlInfo.EscPreCtrlModeReLe; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEscCtrlInfo_EscPreCtrlModeReRi(data) do \
{ \
    *data = Arb_CtrlEscCtrlInfo.EscPreCtrlModeReRi; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEscCtrlInfo_EscDelTarPFrntLe(data) do \
{ \
    *data = Arb_CtrlEscCtrlInfo.EscDelTarPFrntLe; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEscCtrlInfo_EscDelTarPFrntRi(data) do \
{ \
    *data = Arb_CtrlEscCtrlInfo.EscDelTarPFrntRi; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEscCtrlInfo_EscDelTarPReLe(data) do \
{ \
    *data = Arb_CtrlEscCtrlInfo.EscDelTarPReLe; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEscCtrlInfo_EscDelTarPReRi(data) do \
{ \
    *data = Arb_CtrlEscCtrlInfo.EscDelTarPReRi; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlHsaCtrlInfo_HsaActFlg(data) do \
{ \
    *data = Arb_CtrlHsaCtrlInfo.HsaActFlg; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlHsaCtrlInfo_HsaDefectFlg(data) do \
{ \
    *data = Arb_CtrlHsaCtrlInfo.HsaDefectFlg; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlHsaCtrlInfo_HsaTarP(data) do \
{ \
    *data = Arb_CtrlHsaCtrlInfo.HsaTarP; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlSccCtrlInfo_SccActFlg(data) do \
{ \
    *data = Arb_CtrlSccCtrlInfo.SccActFlg; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlSccCtrlInfo_SccDefectFlg(data) do \
{ \
    *data = Arb_CtrlSccCtrlInfo.SccDefectFlg; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlSccCtrlInfo_SccTarP(data) do \
{ \
    *data = Arb_CtrlSccCtrlInfo.SccTarP; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlTcsCtrlInfo_TcsActFlg(data) do \
{ \
    *data = Arb_CtrlTcsCtrlInfo.TcsActFlg; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlTcsCtrlInfo_TcsDefectFlg(data) do \
{ \
    *data = Arb_CtrlTcsCtrlInfo.TcsDefectFlg; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlTcsCtrlInfo_TcsTarPFrntLe(data) do \
{ \
    *data = Arb_CtrlTcsCtrlInfo.TcsTarPFrntLe; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlTcsCtrlInfo_TcsTarPFrntRi(data) do \
{ \
    *data = Arb_CtrlTcsCtrlInfo.TcsTarPFrntRi; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlTcsCtrlInfo_TcsTarPReLe(data) do \
{ \
    *data = Arb_CtrlTcsCtrlInfo.TcsTarPReLe; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlTcsCtrlInfo_TcsTarPReRi(data) do \
{ \
    *data = Arb_CtrlTcsCtrlInfo.TcsTarPReRi; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlTcsCtrlInfo_BothDrvgWhlBrkCtlReqFlg(data) do \
{ \
    *data = Arb_CtrlTcsCtrlInfo.BothDrvgWhlBrkCtlReqFlg; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlTcsCtrlInfo_BrkCtrlFctFrntLeByWspc(data) do \
{ \
    *data = Arb_CtrlTcsCtrlInfo.BrkCtrlFctFrntLeByWspc; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlTcsCtrlInfo_BrkCtrlFctFrntRiByWspc(data) do \
{ \
    *data = Arb_CtrlTcsCtrlInfo.BrkCtrlFctFrntRiByWspc; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlTcsCtrlInfo_BrkCtrlFctReLeByWspc(data) do \
{ \
    *data = Arb_CtrlTcsCtrlInfo.BrkCtrlFctReLeByWspc; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlTcsCtrlInfo_BrkCtrlFctReRiByWspc(data) do \
{ \
    *data = Arb_CtrlTcsCtrlInfo.BrkCtrlFctReRiByWspc; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlTcsCtrlInfo_BrkTqGrdtReqFrntLeByWspc(data) do \
{ \
    *data = Arb_CtrlTcsCtrlInfo.BrkTqGrdtReqFrntLeByWspc; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlTcsCtrlInfo_BrkTqGrdtReqFrntRiByWspc(data) do \
{ \
    *data = Arb_CtrlTcsCtrlInfo.BrkTqGrdtReqFrntRiByWspc; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlTcsCtrlInfo_BrkTqGrdtReqReLeByWspc(data) do \
{ \
    *data = Arb_CtrlTcsCtrlInfo.BrkTqGrdtReqReLeByWspc; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlTcsCtrlInfo_BrkTqGrdtReqReRiByWspc(data) do \
{ \
    *data = Arb_CtrlTcsCtrlInfo.BrkTqGrdtReqReRiByWspc; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlTcsCtrlInfo_TcsDelTarPFrntLe(data) do \
{ \
    *data = Arb_CtrlTcsCtrlInfo.TcsDelTarPFrntLe; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlTcsCtrlInfo_TcsDelTarPFrntRi(data) do \
{ \
    *data = Arb_CtrlTcsCtrlInfo.TcsDelTarPFrntRi; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlTcsCtrlInfo_TcsDelTarPReLe(data) do \
{ \
    *data = Arb_CtrlTcsCtrlInfo.TcsDelTarPReLe; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlTcsCtrlInfo_TcsDelTarPReRi(data) do \
{ \
    *data = Arb_CtrlTcsCtrlInfo.TcsDelTarPReRi; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlTvbbCtrlInfo_TvbbActFlg(data) do \
{ \
    *data = Arb_CtrlTvbbCtrlInfo.TvbbActFlg; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlTvbbCtrlInfo_TvbbDefectFlg(data) do \
{ \
    *data = Arb_CtrlTvbbCtrlInfo.TvbbDefectFlg; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlTvbbCtrlInfo_TvbbTarP(data) do \
{ \
    *data = Arb_CtrlTvbbCtrlInfo.TvbbTarP; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlBaseBrkCtrlModInfo_VehStandStillStFlg(data) do \
{ \
    *data = Arb_CtrlBaseBrkCtrlModInfo.VehStandStillStFlg; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlBBCCtrlInfo_BBCCtrlSt(data) do \
{ \
    *data = Arb_CtrlBBCCtrlInfo.BBCCtrlSt; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlEcuModeSts(data) do \
{ \
    *data = Arb_CtrlEcuModeSts; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlIgnOnOffSts(data) do \
{ \
    *data = Arb_CtrlIgnOnOffSts; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlFuncInhibitArbiSts(data) do \
{ \
    *data = Arb_CtrlFuncInhibitArbiSts; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlBaseBrkCtrlrActFlg(data) do \
{ \
    *data = Arb_CtrlBaseBrkCtrlrActFlg; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlBrkPRednForBaseBrkCtrlr(data) do \
{ \
    *data = Arb_CtrlBrkPRednForBaseBrkCtrlr; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlPCtrlSt(data) do \
{ \
    *data = Arb_CtrlPCtrlSt; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlRgnBrkCtrlrActStFlg(data) do \
{ \
    *data = Arb_CtrlRgnBrkCtrlrActStFlg; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlTarPFromBaseBrkCtrlr(data) do \
{ \
    *data = Arb_CtrlTarPFromBaseBrkCtrlr; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlTarPRateFromBaseBrkCtrlr(data) do \
{ \
    *data = Arb_CtrlTarPRateFromBaseBrkCtrlr; \
}while(0);

#define Arb_Ctrl_Read_Arb_CtrlVehSpdFild(data) do \
{ \
    *data = Arb_CtrlVehSpdFild; \
}while(0);


/* Set Output DE MAcro Function */
#define Arb_Ctrl_Write_Arb_CtrlFinalTarPInfo(data) do \
{ \
    Arb_CtrlFinalTarPInfo = *data; \
}while(0);

#define Arb_Ctrl_Write_Arb_CtrlWhlOutVlvCtrlTarInfo(data) do \
{ \
    Arb_CtrlWhlOutVlvCtrlTarInfo = *data; \
}while(0);

#define Arb_Ctrl_Write_Arb_CtrlWhlPreFillInfo(data) do \
{ \
    Arb_CtrlWhlPreFillInfo = *data; \
}while(0);

#define Arb_Ctrl_Write_Arb_CtrlFinalTarPInfo_FinalTarPOfWhlFrntLe(data) do \
{ \
    Arb_CtrlFinalTarPInfo.FinalTarPOfWhlFrntLe = *data; \
}while(0);

#define Arb_Ctrl_Write_Arb_CtrlFinalTarPInfo_FinalTarPOfWhlFrntRi(data) do \
{ \
    Arb_CtrlFinalTarPInfo.FinalTarPOfWhlFrntRi = *data; \
}while(0);

#define Arb_Ctrl_Write_Arb_CtrlFinalTarPInfo_FinalTarPOfWhlReLe(data) do \
{ \
    Arb_CtrlFinalTarPInfo.FinalTarPOfWhlReLe = *data; \
}while(0);

#define Arb_Ctrl_Write_Arb_CtrlFinalTarPInfo_FinalTarPOfWhlReRi(data) do \
{ \
    Arb_CtrlFinalTarPInfo.FinalTarPOfWhlReRi = *data; \
}while(0);

#define Arb_Ctrl_Write_Arb_CtrlFinalTarPInfo_FinalTarPRateOfWhlFrntLe(data) do \
{ \
    Arb_CtrlFinalTarPInfo.FinalTarPRateOfWhlFrntLe = *data; \
}while(0);

#define Arb_Ctrl_Write_Arb_CtrlFinalTarPInfo_FinalTarPRateOfWhlFrntRi(data) do \
{ \
    Arb_CtrlFinalTarPInfo.FinalTarPRateOfWhlFrntRi = *data; \
}while(0);

#define Arb_Ctrl_Write_Arb_CtrlFinalTarPInfo_FinalTarPRateOfWhlReLe(data) do \
{ \
    Arb_CtrlFinalTarPInfo.FinalTarPRateOfWhlReLe = *data; \
}while(0);

#define Arb_Ctrl_Write_Arb_CtrlFinalTarPInfo_FinalTarPRateOfWhlReRi(data) do \
{ \
    Arb_CtrlFinalTarPInfo.FinalTarPRateOfWhlReRi = *data; \
}while(0);

#define Arb_Ctrl_Write_Arb_CtrlFinalTarPInfo_PCtrlPrioOfWhlFrntLe(data) do \
{ \
    Arb_CtrlFinalTarPInfo.PCtrlPrioOfWhlFrntLe = *data; \
}while(0);

#define Arb_Ctrl_Write_Arb_CtrlFinalTarPInfo_PCtrlPrioOfWhlFrntRi(data) do \
{ \
    Arb_CtrlFinalTarPInfo.PCtrlPrioOfWhlFrntRi = *data; \
}while(0);

#define Arb_Ctrl_Write_Arb_CtrlFinalTarPInfo_PCtrlPrioOfWhlReLe(data) do \
{ \
    Arb_CtrlFinalTarPInfo.PCtrlPrioOfWhlReLe = *data; \
}while(0);

#define Arb_Ctrl_Write_Arb_CtrlFinalTarPInfo_PCtrlPrioOfWhlReRi(data) do \
{ \
    Arb_CtrlFinalTarPInfo.PCtrlPrioOfWhlReRi = *data; \
}while(0);

#define Arb_Ctrl_Write_Arb_CtrlFinalTarPInfo_CtrlModOfWhlFrntLe(data) do \
{ \
    Arb_CtrlFinalTarPInfo.CtrlModOfWhlFrntLe = *data; \
}while(0);

#define Arb_Ctrl_Write_Arb_CtrlFinalTarPInfo_CtrlModOfOfWhlFrntRi(data) do \
{ \
    Arb_CtrlFinalTarPInfo.CtrlModOfOfWhlFrntRi = *data; \
}while(0);

#define Arb_Ctrl_Write_Arb_CtrlFinalTarPInfo_CtrlModOfWhlReLe(data) do \
{ \
    Arb_CtrlFinalTarPInfo.CtrlModOfWhlReLe = *data; \
}while(0);

#define Arb_Ctrl_Write_Arb_CtrlFinalTarPInfo_CtrlModOfWhlReRi(data) do \
{ \
    Arb_CtrlFinalTarPInfo.CtrlModOfWhlReRi = *data; \
}while(0);

#define Arb_Ctrl_Write_Arb_CtrlFinalTarPInfo_ReqdPCtrlBoostMod(data) do \
{ \
    Arb_CtrlFinalTarPInfo.ReqdPCtrlBoostMod = *data; \
}while(0);

#define Arb_Ctrl_Write_Arb_CtrlFinalTarPInfo_FinalDelTarPOfWhlFrntLe(data) do \
{ \
    Arb_CtrlFinalTarPInfo.FinalDelTarPOfWhlFrntLe = *data; \
}while(0);

#define Arb_Ctrl_Write_Arb_CtrlFinalTarPInfo_FinalDelTarPOfWhlFrntRi(data) do \
{ \
    Arb_CtrlFinalTarPInfo.FinalDelTarPOfWhlFrntRi = *data; \
}while(0);

#define Arb_Ctrl_Write_Arb_CtrlFinalTarPInfo_FinalDelTarPOfWhlReLe(data) do \
{ \
    Arb_CtrlFinalTarPInfo.FinalDelTarPOfWhlReLe = *data; \
}while(0);

#define Arb_Ctrl_Write_Arb_CtrlFinalTarPInfo_FinalDelTarPOfWhlReRi(data) do \
{ \
    Arb_CtrlFinalTarPInfo.FinalDelTarPOfWhlReRi = *data; \
}while(0);

#define Arb_Ctrl_Write_Arb_CtrlFinalTarPInfo_FinalMaxCircuitTp(data) do \
{ \
    Arb_CtrlFinalTarPInfo.FinalMaxCircuitTp = *data; \
}while(0);

#define Arb_Ctrl_Write_Arb_CtrlWhlOutVlvCtrlTarInfo_FlOutVlvCtrlTar(data) \
{ \
    for(i=0;i<5;i++) Arb_CtrlWhlOutVlvCtrlTarInfo.FlOutVlvCtrlTar[i] = *data[i]; \
}
#define Arb_Ctrl_Write_Arb_CtrlWhlOutVlvCtrlTarInfo_FrOutVlvCtrlTar(data) \
{ \
    for(i=0;i<5;i++) Arb_CtrlWhlOutVlvCtrlTarInfo.FrOutVlvCtrlTar[i] = *data[i]; \
}
#define Arb_Ctrl_Write_Arb_CtrlWhlOutVlvCtrlTarInfo_RlOutVlvCtrlTar(data) \
{ \
    for(i=0;i<5;i++) Arb_CtrlWhlOutVlvCtrlTarInfo.RlOutVlvCtrlTar[i] = *data[i]; \
}
#define Arb_Ctrl_Write_Arb_CtrlWhlOutVlvCtrlTarInfo_RrOutVlvCtrlTar(data) \
{ \
    for(i=0;i<5;i++) Arb_CtrlWhlOutVlvCtrlTarInfo.RrOutVlvCtrlTar[i] = *data[i]; \
}
#define Arb_Ctrl_Write_Arb_CtrlWhlPreFillInfo_FlPreFillActFlg(data) do \
{ \
    Arb_CtrlWhlPreFillInfo.FlPreFillActFlg = *data; \
}while(0);

#define Arb_Ctrl_Write_Arb_CtrlWhlPreFillInfo_FrPreFillActFlg(data) do \
{ \
    Arb_CtrlWhlPreFillInfo.FrPreFillActFlg = *data; \
}while(0);

#define Arb_Ctrl_Write_Arb_CtrlWhlPreFillInfo_RlPreFillActFlg(data) do \
{ \
    Arb_CtrlWhlPreFillInfo.RlPreFillActFlg = *data; \
}while(0);

#define Arb_Ctrl_Write_Arb_CtrlWhlPreFillInfo_RrPreFillActFlg(data) do \
{ \
    Arb_CtrlWhlPreFillInfo.RrPreFillActFlg = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ARB_CTRL_IFA_H_ */
/** @} */
