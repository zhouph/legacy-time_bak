/**
 * @defgroup Arb_Ctrl_Ifa Arb_Ctrl_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Arb_Ctrl_Ifa.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Arb_Ctrl_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ARB_CTRL_START_SEC_CONST_UNSPECIFIED
#include "Arb_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ARB_CTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Arb_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ARB_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arb_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ARB_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arb_MemMap.h"
#define ARB_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Arb_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ARB_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Arb_MemMap.h"
#define ARB_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Arb_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ARB_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Arb_MemMap.h"
#define ARB_CTRL_START_SEC_VAR_32BIT
#include "Arb_MemMap.h"
/** Variable Section (32BIT)**/


#define ARB_CTRL_STOP_SEC_VAR_32BIT
#include "Arb_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ARB_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arb_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ARB_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Arb_MemMap.h"
#define ARB_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Arb_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ARB_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Arb_MemMap.h"
#define ARB_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Arb_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ARB_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Arb_MemMap.h"
#define ARB_CTRL_START_SEC_VAR_32BIT
#include "Arb_MemMap.h"
/** Variable Section (32BIT)**/


#define ARB_CTRL_STOP_SEC_VAR_32BIT
#include "Arb_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ARB_CTRL_START_SEC_CODE
#include "Arb_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define ARB_CTRL_STOP_SEC_CODE
#include "Arb_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
