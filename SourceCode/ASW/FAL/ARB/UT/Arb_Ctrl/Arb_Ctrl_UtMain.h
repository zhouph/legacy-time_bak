#define UT_MAX_STEP 5

#define ARB_CTRLWHLVLVREQABCINFO_FLOVREQDATA_0   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_FLOVREQDATA_1   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_FLOVREQDATA_2   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_FLOVREQDATA_3   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_FLOVREQDATA_4   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_FLOVREQDATA_5   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_FLOVREQDATA_6   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_FLOVREQDATA_7   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_FLOVREQDATA_8   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_FLOVREQDATA_9   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_FROVREQDATA_0   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_FROVREQDATA_1   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_FROVREQDATA_2   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_FROVREQDATA_3   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_FROVREQDATA_4   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_FROVREQDATA_5   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_FROVREQDATA_6   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_FROVREQDATA_7   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_FROVREQDATA_8   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_FROVREQDATA_9   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_RLOVREQDATA_0   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_RLOVREQDATA_1   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_RLOVREQDATA_2   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_RLOVREQDATA_3   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_RLOVREQDATA_4   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_RLOVREQDATA_5   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_RLOVREQDATA_6   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_RLOVREQDATA_7   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_RLOVREQDATA_8   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_RLOVREQDATA_9   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_RROVREQDATA_0   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_RROVREQDATA_1   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_RROVREQDATA_2   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_RROVREQDATA_3   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_RROVREQDATA_4   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_RROVREQDATA_5   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_RROVREQDATA_6   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_RROVREQDATA_7   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_RROVREQDATA_8   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_RROVREQDATA_9   {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_FLOVREQ {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_FROVREQ {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_RLOVREQ {0,0,0,0,0}
#define ARB_CTRLWHLVLVREQABCINFO_RROVREQ {0,0,0,0,0}
#define ARB_CTRLABSCTRLINFO_ABSACTFLG {0,0,0,0,0}
#define ARB_CTRLABSCTRLINFO_ABSDEFECTFLG {0,0,0,0,0}
#define ARB_CTRLABSCTRLINFO_ABSTARPFRNTLE {0,0,0,0,0}
#define ARB_CTRLABSCTRLINFO_ABSTARPFRNTRI {0,0,0,0,0}
#define ARB_CTRLABSCTRLINFO_ABSTARPRELE {0,0,0,0,0}
#define ARB_CTRLABSCTRLINFO_ABSTARPRERI {0,0,0,0,0}
#define ARB_CTRLABSCTRLINFO_FRNTWHLSLIP {0,0,0,0,0}
#define ARB_CTRLABSCTRLINFO_ABSDESTARP {0,0,0,0,0}
#define ARB_CTRLABSCTRLINFO_ABSDESTARPREQFLG {0,0,0,0,0}
#define ARB_CTRLABSCTRLINFO_ABSCTRLFADEOUTFLG {0,0,0,0,0}
#define ARB_CTRLABSCTRLINFO_ABSCTRLMODEFRNTLE {0,0,0,0,0}
#define ARB_CTRLABSCTRLINFO_ABSCTRLMODEFRNTRI {0,0,0,0,0}
#define ARB_CTRLABSCTRLINFO_ABSCTRLMODERELE {0,0,0,0,0}
#define ARB_CTRLABSCTRLINFO_ABSCTRLMODERERI {0,0,0,0,0}
#define ARB_CTRLABSCTRLINFO_ABSTARPRATEFRNTLE {0,0,0,0,0}
#define ARB_CTRLABSCTRLINFO_ABSTARPRATEFRNTRI {0,0,0,0,0}
#define ARB_CTRLABSCTRLINFO_ABSTARPRATERELE {0,0,0,0,0}
#define ARB_CTRLABSCTRLINFO_ABSTARPRATERERI {0,0,0,0,0}
#define ARB_CTRLABSCTRLINFO_ABSPRIOFRNTLE {0,0,0,0,0}
#define ARB_CTRLABSCTRLINFO_ABSPRIOFRNTRI {0,0,0,0,0}
#define ARB_CTRLABSCTRLINFO_ABSPRIORELE {0,0,0,0,0}
#define ARB_CTRLABSCTRLINFO_ABSPRIORERI {0,0,0,0,0}
#define ARB_CTRLABSCTRLINFO_ABSDELTARPFRNTLE {0,0,0,0,0}
#define ARB_CTRLABSCTRLINFO_ABSDELTARPFRNTRI {0,0,0,0,0}
#define ARB_CTRLABSCTRLINFO_ABSDELTARPRELE {0,0,0,0,0}
#define ARB_CTRLABSCTRLINFO_ABSDELTARPRERI {0,0,0,0,0}
#define ARB_CTRLAVHCTRLINFO_AVHACTFLG {0,0,0,0,0}
#define ARB_CTRLAVHCTRLINFO_AVHDEFECTFLG {0,0,0,0,0}
#define ARB_CTRLAVHCTRLINFO_AVHTARP {0,0,0,0,0}
#define ARB_CTRLBACTRLINFO_BAACTFLG {0,0,0,0,0}
#define ARB_CTRLBACTRLINFO_BACDEFECTFLG {0,0,0,0,0}
#define ARB_CTRLBACTRLINFO_BATARP {0,0,0,0,0}
#define ARB_CTRLEBDCTRLINFO_EBDACTFLG {0,0,0,0,0}
#define ARB_CTRLEBDCTRLINFO_EBDDEFECTFLG {0,0,0,0,0}
#define ARB_CTRLEBDCTRLINFO_EBDTARPFRNTLE {0,0,0,0,0}
#define ARB_CTRLEBDCTRLINFO_EBDTARPFRNTRI {0,0,0,0,0}
#define ARB_CTRLEBDCTRLINFO_EBDTARPRELE {0,0,0,0,0}
#define ARB_CTRLEBDCTRLINFO_EBDTARPRERI {0,0,0,0,0}
#define ARB_CTRLEBDCTRLINFO_EBDTARPRATERELE {0,0,0,0,0}
#define ARB_CTRLEBDCTRLINFO_EBDTARPRATERERI {0,0,0,0,0}
#define ARB_CTRLEBDCTRLINFO_EBDCTRLMODERELE {0,0,0,0,0}
#define ARB_CTRLEBDCTRLINFO_EBDCTRLMODERERI {0,0,0,0,0}
#define ARB_CTRLEBDCTRLINFO_EBDDELTARPRELE {0,0,0,0,0}
#define ARB_CTRLEBDCTRLINFO_EBDDELTARPRERI {0,0,0,0,0}
#define ARB_CTRLEBPCTRLINFO_EBPACTFLG {0,0,0,0,0}
#define ARB_CTRLEBPCTRLINFO_EBPDEFECTFLG {0,0,0,0,0}
#define ARB_CTRLEBPCTRLINFO_EBPTARP {0,0,0,0,0}
#define ARB_CTRLEPBICTRLINFO_EPBIACTFLG {0,0,0,0,0}
#define ARB_CTRLEPBICTRLINFO_EPBIDEFECTFLG {0,0,0,0,0}
#define ARB_CTRLEPBICTRLINFO_EPBITARP {0,0,0,0,0}
#define ARB_CTRLESCCTRLINFO_ESCACTFLG {0,0,0,0,0}
#define ARB_CTRLESCCTRLINFO_ESCDEFECTFLG {0,0,0,0,0}
#define ARB_CTRLESCCTRLINFO_ESCTARPFRNTLE {0,0,0,0,0}
#define ARB_CTRLESCCTRLINFO_ESCTARPFRNTRI {0,0,0,0,0}
#define ARB_CTRLESCCTRLINFO_ESCTARPRELE {0,0,0,0,0}
#define ARB_CTRLESCCTRLINFO_ESCTARPRERI {0,0,0,0,0}
#define ARB_CTRLESCCTRLINFO_ESCCTRLMODEFRNTLE {0,0,0,0,0}
#define ARB_CTRLESCCTRLINFO_ESCCTRLMODEFRNTRI {0,0,0,0,0}
#define ARB_CTRLESCCTRLINFO_ESCCTRLMODERELE {0,0,0,0,0}
#define ARB_CTRLESCCTRLINFO_ESCCTRLMODERERI {0,0,0,0,0}
#define ARB_CTRLESCCTRLINFO_ESCTARPRATEFRNTLE {0,0,0,0,0}
#define ARB_CTRLESCCTRLINFO_ESCTARPRATEFRNTRI {0,0,0,0,0}
#define ARB_CTRLESCCTRLINFO_ESCTARPRATERELE {0,0,0,0,0}
#define ARB_CTRLESCCTRLINFO_ESCTARPRATERERI {0,0,0,0,0}
#define ARB_CTRLESCCTRLINFO_ESCPRIOFRNTLE {0,0,0,0,0}
#define ARB_CTRLESCCTRLINFO_ESCPRIOFRNTRI {0,0,0,0,0}
#define ARB_CTRLESCCTRLINFO_ESCPRIORELE {0,0,0,0,0}
#define ARB_CTRLESCCTRLINFO_ESCPRIORERI {0,0,0,0,0}
#define ARB_CTRLESCCTRLINFO_ESCPRECTRLMODEFRNTLE {0,0,0,0,0}
#define ARB_CTRLESCCTRLINFO_ESCPRECTRLMODEFRNTRI {0,0,0,0,0}
#define ARB_CTRLESCCTRLINFO_ESCPRECTRLMODERELE {0,0,0,0,0}
#define ARB_CTRLESCCTRLINFO_ESCPRECTRLMODERERI {0,0,0,0,0}
#define ARB_CTRLESCCTRLINFO_ESCDELTARPFRNTLE {0,0,0,0,0}
#define ARB_CTRLESCCTRLINFO_ESCDELTARPFRNTRI {0,0,0,0,0}
#define ARB_CTRLESCCTRLINFO_ESCDELTARPRELE {0,0,0,0,0}
#define ARB_CTRLESCCTRLINFO_ESCDELTARPRERI {0,0,0,0,0}
#define ARB_CTRLHSACTRLINFO_HSAACTFLG {0,0,0,0,0}
#define ARB_CTRLHSACTRLINFO_HSADEFECTFLG {0,0,0,0,0}
#define ARB_CTRLHSACTRLINFO_HSATARP {0,0,0,0,0}
#define ARB_CTRLSCCCTRLINFO_SCCACTFLG {0,0,0,0,0}
#define ARB_CTRLSCCCTRLINFO_SCCDEFECTFLG {0,0,0,0,0}
#define ARB_CTRLSCCCTRLINFO_SCCTARP {0,0,0,0,0}
#define ARB_CTRLTCSCTRLINFO_TCSACTFLG {0,0,0,0,0}
#define ARB_CTRLTCSCTRLINFO_TCSDEFECTFLG {0,0,0,0,0}
#define ARB_CTRLTCSCTRLINFO_TCSTARPFRNTLE {0,0,0,0,0}
#define ARB_CTRLTCSCTRLINFO_TCSTARPFRNTRI {0,0,0,0,0}
#define ARB_CTRLTCSCTRLINFO_TCSTARPRELE {0,0,0,0,0}
#define ARB_CTRLTCSCTRLINFO_TCSTARPRERI {0,0,0,0,0}
#define ARB_CTRLTCSCTRLINFO_BOTHDRVGWHLBRKCTLREQFLG {0,0,0,0,0}
#define ARB_CTRLTCSCTRLINFO_BRKCTRLFCTFRNTLEBYWSPC {0,0,0,0,0}
#define ARB_CTRLTCSCTRLINFO_BRKCTRLFCTFRNTRIBYWSPC {0,0,0,0,0}
#define ARB_CTRLTCSCTRLINFO_BRKCTRLFCTRELEBYWSPC {0,0,0,0,0}
#define ARB_CTRLTCSCTRLINFO_BRKCTRLFCTRERIBYWSPC {0,0,0,0,0}
#define ARB_CTRLTCSCTRLINFO_BRKTQGRDTREQFRNTLEBYWSPC {0,0,0,0,0}
#define ARB_CTRLTCSCTRLINFO_BRKTQGRDTREQFRNTRIBYWSPC {0,0,0,0,0}
#define ARB_CTRLTCSCTRLINFO_BRKTQGRDTREQRELEBYWSPC {0,0,0,0,0}
#define ARB_CTRLTCSCTRLINFO_BRKTQGRDTREQRERIBYWSPC {0,0,0,0,0}
#define ARB_CTRLTCSCTRLINFO_TCSDELTARPFRNTLE {0,0,0,0,0}
#define ARB_CTRLTCSCTRLINFO_TCSDELTARPFRNTRI {0,0,0,0,0}
#define ARB_CTRLTCSCTRLINFO_TCSDELTARPRELE {0,0,0,0,0}
#define ARB_CTRLTCSCTRLINFO_TCSDELTARPRERI {0,0,0,0,0}
#define ARB_CTRLTVBBCTRLINFO_TVBBACTFLG {0,0,0,0,0}
#define ARB_CTRLTVBBCTRLINFO_TVBBDEFECTFLG {0,0,0,0,0}
#define ARB_CTRLTVBBCTRLINFO_TVBBTARP {0,0,0,0,0}
#define ARB_CTRLBASEBRKCTRLMODINFO_VEHSTANDSTILLSTFLG {0,0,0,0,0}
#define ARB_CTRLBBCCTRLINFO_BBCCTRLST {0,0,0,0,0}
#define ARB_CTRLECUMODESTS  {0,0,0,0,0}
#define ARB_CTRLIGNONOFFSTS  {0,0,0,0,0}
#define ARB_CTRLFUNCINHIBITARBISTS  {0,0,0,0,0}
#define ARB_CTRLBASEBRKCTRLRACTFLG  {0,0,0,0,0}
#define ARB_CTRLBRKPREDNFORBASEBRKCTRLR  {0,0,0,0,0}
#define ARB_CTRLPCTRLST  {0,0,0,0,0}
#define ARB_CTRLRGNBRKCTRLRACTSTFLG  {0,0,0,0,0}
#define ARB_CTRLTARPFROMBASEBRKCTRLR  {0,0,0,0,0}
#define ARB_CTRLTARPRATEFROMBASEBRKCTRLR  {0,0,0,0,0}
#define ARB_CTRLVEHSPDFILD  {0,0,0,0,0}

#define ARB_CTRLFINALTARPINFO_FINALTARPOFWHLFRNTLE   {0,0,0,0,0}
#define ARB_CTRLFINALTARPINFO_FINALTARPOFWHLFRNTRI   {0,0,0,0,0}
#define ARB_CTRLFINALTARPINFO_FINALTARPOFWHLRELE   {0,0,0,0,0}
#define ARB_CTRLFINALTARPINFO_FINALTARPOFWHLRERI   {0,0,0,0,0}
#define ARB_CTRLFINALTARPINFO_FINALTARPRATEOFWHLFRNTLE   {0,0,0,0,0}
#define ARB_CTRLFINALTARPINFO_FINALTARPRATEOFWHLFRNTRI   {0,0,0,0,0}
#define ARB_CTRLFINALTARPINFO_FINALTARPRATEOFWHLRELE   {0,0,0,0,0}
#define ARB_CTRLFINALTARPINFO_FINALTARPRATEOFWHLRERI   {0,0,0,0,0}
#define ARB_CTRLFINALTARPINFO_PCTRLPRIOOFWHLFRNTLE   {0,0,0,0,0}
#define ARB_CTRLFINALTARPINFO_PCTRLPRIOOFWHLFRNTRI   {0,0,0,0,0}
#define ARB_CTRLFINALTARPINFO_PCTRLPRIOOFWHLRELE   {0,0,0,0,0}
#define ARB_CTRLFINALTARPINFO_PCTRLPRIOOFWHLRERI   {0,0,0,0,0}
#define ARB_CTRLFINALTARPINFO_CTRLMODOFWHLFRNTLE   {0,0,0,0,0}
#define ARB_CTRLFINALTARPINFO_CTRLMODOFOFWHLFRNTRI   {0,0,0,0,0}
#define ARB_CTRLFINALTARPINFO_CTRLMODOFWHLRELE   {0,0,0,0,0}
#define ARB_CTRLFINALTARPINFO_CTRLMODOFWHLRERI   {0,0,0,0,0}
#define ARB_CTRLFINALTARPINFO_REQDPCTRLBOOSTMOD   {0,0,0,0,0}
#define ARB_CTRLFINALTARPINFO_FINALDELTARPOFWHLFRNTLE   {0,0,0,0,0}
#define ARB_CTRLFINALTARPINFO_FINALDELTARPOFWHLFRNTRI   {0,0,0,0,0}
#define ARB_CTRLFINALTARPINFO_FINALDELTARPOFWHLRELE   {0,0,0,0,0}
#define ARB_CTRLFINALTARPINFO_FINALDELTARPOFWHLRERI   {0,0,0,0,0}
#define ARB_CTRLFINALTARPINFO_FINALMAXCIRCUITTP   {0,0,0,0,0}
#define ARB_CTRLWHLOUTVLVCTRLTARINFO_FLOUTVLVCTRLTAR_0  {0,0,0,0,0}
#define ARB_CTRLWHLOUTVLVCTRLTARINFO_FLOUTVLVCTRLTAR_1  {0,0,0,0,0}
#define ARB_CTRLWHLOUTVLVCTRLTARINFO_FLOUTVLVCTRLTAR_2  {0,0,0,0,0}
#define ARB_CTRLWHLOUTVLVCTRLTARINFO_FLOUTVLVCTRLTAR_3  {0,0,0,0,0}
#define ARB_CTRLWHLOUTVLVCTRLTARINFO_FLOUTVLVCTRLTAR_4  {0,0,0,0,0}
#define ARB_CTRLWHLOUTVLVCTRLTARINFO_FROUTVLVCTRLTAR_0  {0,0,0,0,0}
#define ARB_CTRLWHLOUTVLVCTRLTARINFO_FROUTVLVCTRLTAR_1  {0,0,0,0,0}
#define ARB_CTRLWHLOUTVLVCTRLTARINFO_FROUTVLVCTRLTAR_2  {0,0,0,0,0}
#define ARB_CTRLWHLOUTVLVCTRLTARINFO_FROUTVLVCTRLTAR_3  {0,0,0,0,0}
#define ARB_CTRLWHLOUTVLVCTRLTARINFO_FROUTVLVCTRLTAR_4  {0,0,0,0,0}
#define ARB_CTRLWHLOUTVLVCTRLTARINFO_RLOUTVLVCTRLTAR_0  {0,0,0,0,0}
#define ARB_CTRLWHLOUTVLVCTRLTARINFO_RLOUTVLVCTRLTAR_1  {0,0,0,0,0}
#define ARB_CTRLWHLOUTVLVCTRLTARINFO_RLOUTVLVCTRLTAR_2  {0,0,0,0,0}
#define ARB_CTRLWHLOUTVLVCTRLTARINFO_RLOUTVLVCTRLTAR_3  {0,0,0,0,0}
#define ARB_CTRLWHLOUTVLVCTRLTARINFO_RLOUTVLVCTRLTAR_4  {0,0,0,0,0}
#define ARB_CTRLWHLOUTVLVCTRLTARINFO_RROUTVLVCTRLTAR_0  {0,0,0,0,0}
#define ARB_CTRLWHLOUTVLVCTRLTARINFO_RROUTVLVCTRLTAR_1  {0,0,0,0,0}
#define ARB_CTRLWHLOUTVLVCTRLTARINFO_RROUTVLVCTRLTAR_2  {0,0,0,0,0}
#define ARB_CTRLWHLOUTVLVCTRLTARINFO_RROUTVLVCTRLTAR_3  {0,0,0,0,0}
#define ARB_CTRLWHLOUTVLVCTRLTARINFO_RROUTVLVCTRLTAR_4  {0,0,0,0,0}
#define ARB_CTRLWHLPREFILLINFO_FLPREFILLACTFLG   {0,0,0,0,0}
#define ARB_CTRLWHLPREFILLINFO_FRPREFILLACTFLG   {0,0,0,0,0}
#define ARB_CTRLWHLPREFILLINFO_RLPREFILLACTFLG   {0,0,0,0,0}
#define ARB_CTRLWHLPREFILLINFO_RRPREFILLACTFLG   {0,0,0,0,0}
