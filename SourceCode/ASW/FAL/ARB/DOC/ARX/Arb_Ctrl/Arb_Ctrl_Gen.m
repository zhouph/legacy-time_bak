Arb_CtrlWhlVlvReqAbcInfo = Simulink.Bus;
DeList={
    'FlOvReqData_array_0'
    'FlOvReqData_array_1'
    'FlOvReqData_array_2'
    'FlOvReqData_array_3'
    'FlOvReqData_array_4'
    'FlOvReqData_array_5'
    'FlOvReqData_array_6'
    'FlOvReqData_array_7'
    'FlOvReqData_array_8'
    'FlOvReqData_array_9'
    'FrOvReqData_array_0'
    'FrOvReqData_array_1'
    'FrOvReqData_array_2'
    'FrOvReqData_array_3'
    'FrOvReqData_array_4'
    'FrOvReqData_array_5'
    'FrOvReqData_array_6'
    'FrOvReqData_array_7'
    'FrOvReqData_array_8'
    'FrOvReqData_array_9'
    'RlOvReqData_array_0'
    'RlOvReqData_array_1'
    'RlOvReqData_array_2'
    'RlOvReqData_array_3'
    'RlOvReqData_array_4'
    'RlOvReqData_array_5'
    'RlOvReqData_array_6'
    'RlOvReqData_array_7'
    'RlOvReqData_array_8'
    'RlOvReqData_array_9'
    'RrOvReqData_array_0'
    'RrOvReqData_array_1'
    'RrOvReqData_array_2'
    'RrOvReqData_array_3'
    'RrOvReqData_array_4'
    'RrOvReqData_array_5'
    'RrOvReqData_array_6'
    'RrOvReqData_array_7'
    'RrOvReqData_array_8'
    'RrOvReqData_array_9'
    'FlOvReq'
    'FrOvReq'
    'RlOvReq'
    'RrOvReq'
    };
Arb_CtrlWhlVlvReqAbcInfo = CreateBus(Arb_CtrlWhlVlvReqAbcInfo, DeList);
clear DeList;

Arb_CtrlAbsCtrlInfo = Simulink.Bus;
DeList={
    'AbsActFlg'
    'AbsDefectFlg'
    'AbsTarPFrntLe'
    'AbsTarPFrntRi'
    'AbsTarPReLe'
    'AbsTarPReRi'
    'FrntWhlSlip'
    'AbsDesTarP'
    'AbsDesTarPReqFlg'
    'AbsCtrlFadeOutFlg'
    'AbsCtrlModeFrntLe'
    'AbsCtrlModeFrntRi'
    'AbsCtrlModeReLe'
    'AbsCtrlModeReRi'
    'AbsTarPRateFrntLe'
    'AbsTarPRateFrntRi'
    'AbsTarPRateReLe'
    'AbsTarPRateReRi'
    'AbsPrioFrntLe'
    'AbsPrioFrntRi'
    'AbsPrioReLe'
    'AbsPrioReRi'
    'AbsDelTarPFrntLe'
    'AbsDelTarPFrntRi'
    'AbsDelTarPReLe'
    'AbsDelTarPReRi'
    };
Arb_CtrlAbsCtrlInfo = CreateBus(Arb_CtrlAbsCtrlInfo, DeList);
clear DeList;

Arb_CtrlAvhCtrlInfo = Simulink.Bus;
DeList={
    'AvhActFlg'
    'AvhDefectFlg'
    'AvhTarP'
    };
Arb_CtrlAvhCtrlInfo = CreateBus(Arb_CtrlAvhCtrlInfo, DeList);
clear DeList;

Arb_CtrlBaCtrlInfo = Simulink.Bus;
DeList={
    'BaActFlg'
    'BaCDefectFlg'
    'BaTarP'
    };
Arb_CtrlBaCtrlInfo = CreateBus(Arb_CtrlBaCtrlInfo, DeList);
clear DeList;

Arb_CtrlEbdCtrlInfo = Simulink.Bus;
DeList={
    'EbdActFlg'
    'EbdDefectFlg'
    'EbdTarPFrntLe'
    'EbdTarPFrntRi'
    'EbdTarPReLe'
    'EbdTarPReRi'
    'EbdTarPRateReLe'
    'EbdTarPRateReRi'
    'EbdCtrlModeReLe'
    'EbdCtrlModeReRi'
    'EbdDelTarPReLe'
    'EbdDelTarPReRi'
    };
Arb_CtrlEbdCtrlInfo = CreateBus(Arb_CtrlEbdCtrlInfo, DeList);
clear DeList;

Arb_CtrlEbpCtrlInfo = Simulink.Bus;
DeList={
    'EbpActFlg'
    'EbpDefectFlg'
    'EbpTarP'
    };
Arb_CtrlEbpCtrlInfo = CreateBus(Arb_CtrlEbpCtrlInfo, DeList);
clear DeList;

Arb_CtrlEpbiCtrlInfo = Simulink.Bus;
DeList={
    'EpbiActFlg'
    'EpbiDefectFlg'
    'EpbiTarP'
    };
Arb_CtrlEpbiCtrlInfo = CreateBus(Arb_CtrlEpbiCtrlInfo, DeList);
clear DeList;

Arb_CtrlEscCtrlInfo = Simulink.Bus;
DeList={
    'EscActFlg'
    'EscDefectFlg'
    'EscTarPFrntLe'
    'EscTarPFrntRi'
    'EscTarPReLe'
    'EscTarPReRi'
    'EscCtrlModeFrntLe'
    'EscCtrlModeFrntRi'
    'EscCtrlModeReLe'
    'EscCtrlModeReRi'
    'EscTarPRateFrntLe'
    'EscTarPRateFrntRi'
    'EscTarPRateReLe'
    'EscTarPRateReRi'
    'EscPrioFrntLe'
    'EscPrioFrntRi'
    'EscPrioReLe'
    'EscPrioReRi'
    'EscPreCtrlModeFrntLe'
    'EscPreCtrlModeFrntRi'
    'EscPreCtrlModeReLe'
    'EscPreCtrlModeReRi'
    'EscDelTarPFrntLe'
    'EscDelTarPFrntRi'
    'EscDelTarPReLe'
    'EscDelTarPReRi'
    };
Arb_CtrlEscCtrlInfo = CreateBus(Arb_CtrlEscCtrlInfo, DeList);
clear DeList;

Arb_CtrlHsaCtrlInfo = Simulink.Bus;
DeList={
    'HsaActFlg'
    'HsaDefectFlg'
    'HsaTarP'
    };
Arb_CtrlHsaCtrlInfo = CreateBus(Arb_CtrlHsaCtrlInfo, DeList);
clear DeList;

Arb_CtrlSccCtrlInfo = Simulink.Bus;
DeList={
    'SccActFlg'
    'SccDefectFlg'
    'SccTarP'
    };
Arb_CtrlSccCtrlInfo = CreateBus(Arb_CtrlSccCtrlInfo, DeList);
clear DeList;

Arb_CtrlTcsCtrlInfo = Simulink.Bus;
DeList={
    'TcsActFlg'
    'TcsDefectFlg'
    'TcsTarPFrntLe'
    'TcsTarPFrntRi'
    'TcsTarPReLe'
    'TcsTarPReRi'
    'BothDrvgWhlBrkCtlReqFlg'
    'BrkCtrlFctFrntLeByWspc'
    'BrkCtrlFctFrntRiByWspc'
    'BrkCtrlFctReLeByWspc'
    'BrkCtrlFctReRiByWspc'
    'BrkTqGrdtReqFrntLeByWspc'
    'BrkTqGrdtReqFrntRiByWspc'
    'BrkTqGrdtReqReLeByWspc'
    'BrkTqGrdtReqReRiByWspc'
    'TcsDelTarPFrntLe'
    'TcsDelTarPFrntRi'
    'TcsDelTarPReLe'
    'TcsDelTarPReRi'
    };
Arb_CtrlTcsCtrlInfo = CreateBus(Arb_CtrlTcsCtrlInfo, DeList);
clear DeList;

Arb_CtrlTvbbCtrlInfo = Simulink.Bus;
DeList={
    'TvbbActFlg'
    'TvbbDefectFlg'
    'TvbbTarP'
    };
Arb_CtrlTvbbCtrlInfo = CreateBus(Arb_CtrlTvbbCtrlInfo, DeList);
clear DeList;

Arb_CtrlBaseBrkCtrlModInfo = Simulink.Bus;
DeList={
    'VehStandStillStFlg'
    };
Arb_CtrlBaseBrkCtrlModInfo = CreateBus(Arb_CtrlBaseBrkCtrlModInfo, DeList);
clear DeList;

Arb_CtrlBBCCtrlInfo = Simulink.Bus;
DeList={
    'BBCCtrlSt'
    };
Arb_CtrlBBCCtrlInfo = CreateBus(Arb_CtrlBBCCtrlInfo, DeList);
clear DeList;

Arb_CtrlEcuModeSts = Simulink.Bus;
DeList={'Arb_CtrlEcuModeSts'};
Arb_CtrlEcuModeSts = CreateBus(Arb_CtrlEcuModeSts, DeList);
clear DeList;

Arb_CtrlIgnOnOffSts = Simulink.Bus;
DeList={'Arb_CtrlIgnOnOffSts'};
Arb_CtrlIgnOnOffSts = CreateBus(Arb_CtrlIgnOnOffSts, DeList);
clear DeList;

Arb_CtrlFuncInhibitArbiSts = Simulink.Bus;
DeList={'Arb_CtrlFuncInhibitArbiSts'};
Arb_CtrlFuncInhibitArbiSts = CreateBus(Arb_CtrlFuncInhibitArbiSts, DeList);
clear DeList;

Arb_CtrlBaseBrkCtrlrActFlg = Simulink.Bus;
DeList={'Arb_CtrlBaseBrkCtrlrActFlg'};
Arb_CtrlBaseBrkCtrlrActFlg = CreateBus(Arb_CtrlBaseBrkCtrlrActFlg, DeList);
clear DeList;

Arb_CtrlBrkPRednForBaseBrkCtrlr = Simulink.Bus;
DeList={'Arb_CtrlBrkPRednForBaseBrkCtrlr'};
Arb_CtrlBrkPRednForBaseBrkCtrlr = CreateBus(Arb_CtrlBrkPRednForBaseBrkCtrlr, DeList);
clear DeList;

Arb_CtrlPCtrlSt = Simulink.Bus;
DeList={'Arb_CtrlPCtrlSt'};
Arb_CtrlPCtrlSt = CreateBus(Arb_CtrlPCtrlSt, DeList);
clear DeList;

Arb_CtrlRgnBrkCtrlrActStFlg = Simulink.Bus;
DeList={'Arb_CtrlRgnBrkCtrlrActStFlg'};
Arb_CtrlRgnBrkCtrlrActStFlg = CreateBus(Arb_CtrlRgnBrkCtrlrActStFlg, DeList);
clear DeList;

Arb_CtrlTarPFromBaseBrkCtrlr = Simulink.Bus;
DeList={'Arb_CtrlTarPFromBaseBrkCtrlr'};
Arb_CtrlTarPFromBaseBrkCtrlr = CreateBus(Arb_CtrlTarPFromBaseBrkCtrlr, DeList);
clear DeList;

Arb_CtrlTarPRateFromBaseBrkCtrlr = Simulink.Bus;
DeList={'Arb_CtrlTarPRateFromBaseBrkCtrlr'};
Arb_CtrlTarPRateFromBaseBrkCtrlr = CreateBus(Arb_CtrlTarPRateFromBaseBrkCtrlr, DeList);
clear DeList;

Arb_CtrlVehSpdFild = Simulink.Bus;
DeList={'Arb_CtrlVehSpdFild'};
Arb_CtrlVehSpdFild = CreateBus(Arb_CtrlVehSpdFild, DeList);
clear DeList;

Arb_CtrlFinalTarPInfo = Simulink.Bus;
DeList={
    'FinalTarPOfWhlFrntLe'
    'FinalTarPOfWhlFrntRi'
    'FinalTarPOfWhlReLe'
    'FinalTarPOfWhlReRi'
    'FinalTarPRateOfWhlFrntLe'
    'FinalTarPRateOfWhlFrntRi'
    'FinalTarPRateOfWhlReLe'
    'FinalTarPRateOfWhlReRi'
    'PCtrlPrioOfWhlFrntLe'
    'PCtrlPrioOfWhlFrntRi'
    'PCtrlPrioOfWhlReLe'
    'PCtrlPrioOfWhlReRi'
    'CtrlModOfWhlFrntLe'
    'CtrlModOfOfWhlFrntRi'
    'CtrlModOfWhlReLe'
    'CtrlModOfWhlReRi'
    'ReqdPCtrlBoostMod'
    'FinalDelTarPOfWhlFrntLe'
    'FinalDelTarPOfWhlFrntRi'
    'FinalDelTarPOfWhlReLe'
    'FinalDelTarPOfWhlReRi'
    'FinalMaxCircuitTp'
    };
Arb_CtrlFinalTarPInfo = CreateBus(Arb_CtrlFinalTarPInfo, DeList);
clear DeList;

Arb_CtrlWhlOutVlvCtrlTarInfo = Simulink.Bus;
DeList={
    'FlOutVlvCtrlTar_array_0'
    'FlOutVlvCtrlTar_array_1'
    'FlOutVlvCtrlTar_array_2'
    'FlOutVlvCtrlTar_array_3'
    'FlOutVlvCtrlTar_array_4'
    'FrOutVlvCtrlTar_array_0'
    'FrOutVlvCtrlTar_array_1'
    'FrOutVlvCtrlTar_array_2'
    'FrOutVlvCtrlTar_array_3'
    'FrOutVlvCtrlTar_array_4'
    'RlOutVlvCtrlTar_array_0'
    'RlOutVlvCtrlTar_array_1'
    'RlOutVlvCtrlTar_array_2'
    'RlOutVlvCtrlTar_array_3'
    'RlOutVlvCtrlTar_array_4'
    'RrOutVlvCtrlTar_array_0'
    'RrOutVlvCtrlTar_array_1'
    'RrOutVlvCtrlTar_array_2'
    'RrOutVlvCtrlTar_array_3'
    'RrOutVlvCtrlTar_array_4'
    };
Arb_CtrlWhlOutVlvCtrlTarInfo = CreateBus(Arb_CtrlWhlOutVlvCtrlTarInfo, DeList);
clear DeList;

Arb_CtrlWhlPreFillInfo = Simulink.Bus;
DeList={
    'FlPreFillActFlg'
    'FrPreFillActFlg'
    'RlPreFillActFlg'
    'RrPreFillActFlg'
    };
Arb_CtrlWhlPreFillInfo = CreateBus(Arb_CtrlWhlPreFillInfo, DeList);
clear DeList;

