#define S_FUNCTION_NAME      Arb_Ctrl_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          160
#define WidthOutputPort         46

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Arb_Ctrl.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Arb_CtrlWhlVlvReqAbcInfo.FlOvReqData[0] = input[0];
    Arb_CtrlWhlVlvReqAbcInfo.FlOvReqData[1] = input[1];
    Arb_CtrlWhlVlvReqAbcInfo.FlOvReqData[2] = input[2];
    Arb_CtrlWhlVlvReqAbcInfo.FlOvReqData[3] = input[3];
    Arb_CtrlWhlVlvReqAbcInfo.FlOvReqData[4] = input[4];
    Arb_CtrlWhlVlvReqAbcInfo.FlOvReqData[5] = input[5];
    Arb_CtrlWhlVlvReqAbcInfo.FlOvReqData[6] = input[6];
    Arb_CtrlWhlVlvReqAbcInfo.FlOvReqData[7] = input[7];
    Arb_CtrlWhlVlvReqAbcInfo.FlOvReqData[8] = input[8];
    Arb_CtrlWhlVlvReqAbcInfo.FlOvReqData[9] = input[9];
    Arb_CtrlWhlVlvReqAbcInfo.FrOvReqData[0] = input[10];
    Arb_CtrlWhlVlvReqAbcInfo.FrOvReqData[1] = input[11];
    Arb_CtrlWhlVlvReqAbcInfo.FrOvReqData[2] = input[12];
    Arb_CtrlWhlVlvReqAbcInfo.FrOvReqData[3] = input[13];
    Arb_CtrlWhlVlvReqAbcInfo.FrOvReqData[4] = input[14];
    Arb_CtrlWhlVlvReqAbcInfo.FrOvReqData[5] = input[15];
    Arb_CtrlWhlVlvReqAbcInfo.FrOvReqData[6] = input[16];
    Arb_CtrlWhlVlvReqAbcInfo.FrOvReqData[7] = input[17];
    Arb_CtrlWhlVlvReqAbcInfo.FrOvReqData[8] = input[18];
    Arb_CtrlWhlVlvReqAbcInfo.FrOvReqData[9] = input[19];
    Arb_CtrlWhlVlvReqAbcInfo.RlOvReqData[0] = input[20];
    Arb_CtrlWhlVlvReqAbcInfo.RlOvReqData[1] = input[21];
    Arb_CtrlWhlVlvReqAbcInfo.RlOvReqData[2] = input[22];
    Arb_CtrlWhlVlvReqAbcInfo.RlOvReqData[3] = input[23];
    Arb_CtrlWhlVlvReqAbcInfo.RlOvReqData[4] = input[24];
    Arb_CtrlWhlVlvReqAbcInfo.RlOvReqData[5] = input[25];
    Arb_CtrlWhlVlvReqAbcInfo.RlOvReqData[6] = input[26];
    Arb_CtrlWhlVlvReqAbcInfo.RlOvReqData[7] = input[27];
    Arb_CtrlWhlVlvReqAbcInfo.RlOvReqData[8] = input[28];
    Arb_CtrlWhlVlvReqAbcInfo.RlOvReqData[9] = input[29];
    Arb_CtrlWhlVlvReqAbcInfo.RrOvReqData[0] = input[30];
    Arb_CtrlWhlVlvReqAbcInfo.RrOvReqData[1] = input[31];
    Arb_CtrlWhlVlvReqAbcInfo.RrOvReqData[2] = input[32];
    Arb_CtrlWhlVlvReqAbcInfo.RrOvReqData[3] = input[33];
    Arb_CtrlWhlVlvReqAbcInfo.RrOvReqData[4] = input[34];
    Arb_CtrlWhlVlvReqAbcInfo.RrOvReqData[5] = input[35];
    Arb_CtrlWhlVlvReqAbcInfo.RrOvReqData[6] = input[36];
    Arb_CtrlWhlVlvReqAbcInfo.RrOvReqData[7] = input[37];
    Arb_CtrlWhlVlvReqAbcInfo.RrOvReqData[8] = input[38];
    Arb_CtrlWhlVlvReqAbcInfo.RrOvReqData[9] = input[39];
    Arb_CtrlWhlVlvReqAbcInfo.FlOvReq = input[40];
    Arb_CtrlWhlVlvReqAbcInfo.FrOvReq = input[41];
    Arb_CtrlWhlVlvReqAbcInfo.RlOvReq = input[42];
    Arb_CtrlWhlVlvReqAbcInfo.RrOvReq = input[43];
    Arb_CtrlAbsCtrlInfo.AbsActFlg = input[44];
    Arb_CtrlAbsCtrlInfo.AbsDefectFlg = input[45];
    Arb_CtrlAbsCtrlInfo.AbsTarPFrntLe = input[46];
    Arb_CtrlAbsCtrlInfo.AbsTarPFrntRi = input[47];
    Arb_CtrlAbsCtrlInfo.AbsTarPReLe = input[48];
    Arb_CtrlAbsCtrlInfo.AbsTarPReRi = input[49];
    Arb_CtrlAbsCtrlInfo.FrntWhlSlip = input[50];
    Arb_CtrlAbsCtrlInfo.AbsDesTarP = input[51];
    Arb_CtrlAbsCtrlInfo.AbsDesTarPReqFlg = input[52];
    Arb_CtrlAbsCtrlInfo.AbsCtrlFadeOutFlg = input[53];
    Arb_CtrlAbsCtrlInfo.AbsCtrlModeFrntLe = input[54];
    Arb_CtrlAbsCtrlInfo.AbsCtrlModeFrntRi = input[55];
    Arb_CtrlAbsCtrlInfo.AbsCtrlModeReLe = input[56];
    Arb_CtrlAbsCtrlInfo.AbsCtrlModeReRi = input[57];
    Arb_CtrlAbsCtrlInfo.AbsTarPRateFrntLe = input[58];
    Arb_CtrlAbsCtrlInfo.AbsTarPRateFrntRi = input[59];
    Arb_CtrlAbsCtrlInfo.AbsTarPRateReLe = input[60];
    Arb_CtrlAbsCtrlInfo.AbsTarPRateReRi = input[61];
    Arb_CtrlAbsCtrlInfo.AbsPrioFrntLe = input[62];
    Arb_CtrlAbsCtrlInfo.AbsPrioFrntRi = input[63];
    Arb_CtrlAbsCtrlInfo.AbsPrioReLe = input[64];
    Arb_CtrlAbsCtrlInfo.AbsPrioReRi = input[65];
    Arb_CtrlAbsCtrlInfo.AbsDelTarPFrntLe = input[66];
    Arb_CtrlAbsCtrlInfo.AbsDelTarPFrntRi = input[67];
    Arb_CtrlAbsCtrlInfo.AbsDelTarPReLe = input[68];
    Arb_CtrlAbsCtrlInfo.AbsDelTarPReRi = input[69];
    Arb_CtrlAvhCtrlInfo.AvhActFlg = input[70];
    Arb_CtrlAvhCtrlInfo.AvhDefectFlg = input[71];
    Arb_CtrlAvhCtrlInfo.AvhTarP = input[72];
    Arb_CtrlBaCtrlInfo.BaActFlg = input[73];
    Arb_CtrlBaCtrlInfo.BaCDefectFlg = input[74];
    Arb_CtrlBaCtrlInfo.BaTarP = input[75];
    Arb_CtrlEbdCtrlInfo.EbdActFlg = input[76];
    Arb_CtrlEbdCtrlInfo.EbdDefectFlg = input[77];
    Arb_CtrlEbdCtrlInfo.EbdTarPFrntLe = input[78];
    Arb_CtrlEbdCtrlInfo.EbdTarPFrntRi = input[79];
    Arb_CtrlEbdCtrlInfo.EbdTarPReLe = input[80];
    Arb_CtrlEbdCtrlInfo.EbdTarPReRi = input[81];
    Arb_CtrlEbdCtrlInfo.EbdTarPRateReLe = input[82];
    Arb_CtrlEbdCtrlInfo.EbdTarPRateReRi = input[83];
    Arb_CtrlEbdCtrlInfo.EbdCtrlModeReLe = input[84];
    Arb_CtrlEbdCtrlInfo.EbdCtrlModeReRi = input[85];
    Arb_CtrlEbdCtrlInfo.EbdDelTarPReLe = input[86];
    Arb_CtrlEbdCtrlInfo.EbdDelTarPReRi = input[87];
    Arb_CtrlEbpCtrlInfo.EbpActFlg = input[88];
    Arb_CtrlEbpCtrlInfo.EbpDefectFlg = input[89];
    Arb_CtrlEbpCtrlInfo.EbpTarP = input[90];
    Arb_CtrlEpbiCtrlInfo.EpbiActFlg = input[91];
    Arb_CtrlEpbiCtrlInfo.EpbiDefectFlg = input[92];
    Arb_CtrlEpbiCtrlInfo.EpbiTarP = input[93];
    Arb_CtrlEscCtrlInfo.EscActFlg = input[94];
    Arb_CtrlEscCtrlInfo.EscDefectFlg = input[95];
    Arb_CtrlEscCtrlInfo.EscTarPFrntLe = input[96];
    Arb_CtrlEscCtrlInfo.EscTarPFrntRi = input[97];
    Arb_CtrlEscCtrlInfo.EscTarPReLe = input[98];
    Arb_CtrlEscCtrlInfo.EscTarPReRi = input[99];
    Arb_CtrlEscCtrlInfo.EscCtrlModeFrntLe = input[100];
    Arb_CtrlEscCtrlInfo.EscCtrlModeFrntRi = input[101];
    Arb_CtrlEscCtrlInfo.EscCtrlModeReLe = input[102];
    Arb_CtrlEscCtrlInfo.EscCtrlModeReRi = input[103];
    Arb_CtrlEscCtrlInfo.EscTarPRateFrntLe = input[104];
    Arb_CtrlEscCtrlInfo.EscTarPRateFrntRi = input[105];
    Arb_CtrlEscCtrlInfo.EscTarPRateReLe = input[106];
    Arb_CtrlEscCtrlInfo.EscTarPRateReRi = input[107];
    Arb_CtrlEscCtrlInfo.EscPrioFrntLe = input[108];
    Arb_CtrlEscCtrlInfo.EscPrioFrntRi = input[109];
    Arb_CtrlEscCtrlInfo.EscPrioReLe = input[110];
    Arb_CtrlEscCtrlInfo.EscPrioReRi = input[111];
    Arb_CtrlEscCtrlInfo.EscPreCtrlModeFrntLe = input[112];
    Arb_CtrlEscCtrlInfo.EscPreCtrlModeFrntRi = input[113];
    Arb_CtrlEscCtrlInfo.EscPreCtrlModeReLe = input[114];
    Arb_CtrlEscCtrlInfo.EscPreCtrlModeReRi = input[115];
    Arb_CtrlEscCtrlInfo.EscDelTarPFrntLe = input[116];
    Arb_CtrlEscCtrlInfo.EscDelTarPFrntRi = input[117];
    Arb_CtrlEscCtrlInfo.EscDelTarPReLe = input[118];
    Arb_CtrlEscCtrlInfo.EscDelTarPReRi = input[119];
    Arb_CtrlHsaCtrlInfo.HsaActFlg = input[120];
    Arb_CtrlHsaCtrlInfo.HsaDefectFlg = input[121];
    Arb_CtrlHsaCtrlInfo.HsaTarP = input[122];
    Arb_CtrlSccCtrlInfo.SccActFlg = input[123];
    Arb_CtrlSccCtrlInfo.SccDefectFlg = input[124];
    Arb_CtrlSccCtrlInfo.SccTarP = input[125];
    Arb_CtrlTcsCtrlInfo.TcsActFlg = input[126];
    Arb_CtrlTcsCtrlInfo.TcsDefectFlg = input[127];
    Arb_CtrlTcsCtrlInfo.TcsTarPFrntLe = input[128];
    Arb_CtrlTcsCtrlInfo.TcsTarPFrntRi = input[129];
    Arb_CtrlTcsCtrlInfo.TcsTarPReLe = input[130];
    Arb_CtrlTcsCtrlInfo.TcsTarPReRi = input[131];
    Arb_CtrlTcsCtrlInfo.BothDrvgWhlBrkCtlReqFlg = input[132];
    Arb_CtrlTcsCtrlInfo.BrkCtrlFctFrntLeByWspc = input[133];
    Arb_CtrlTcsCtrlInfo.BrkCtrlFctFrntRiByWspc = input[134];
    Arb_CtrlTcsCtrlInfo.BrkCtrlFctReLeByWspc = input[135];
    Arb_CtrlTcsCtrlInfo.BrkCtrlFctReRiByWspc = input[136];
    Arb_CtrlTcsCtrlInfo.BrkTqGrdtReqFrntLeByWspc = input[137];
    Arb_CtrlTcsCtrlInfo.BrkTqGrdtReqFrntRiByWspc = input[138];
    Arb_CtrlTcsCtrlInfo.BrkTqGrdtReqReLeByWspc = input[139];
    Arb_CtrlTcsCtrlInfo.BrkTqGrdtReqReRiByWspc = input[140];
    Arb_CtrlTcsCtrlInfo.TcsDelTarPFrntLe = input[141];
    Arb_CtrlTcsCtrlInfo.TcsDelTarPFrntRi = input[142];
    Arb_CtrlTcsCtrlInfo.TcsDelTarPReLe = input[143];
    Arb_CtrlTcsCtrlInfo.TcsDelTarPReRi = input[144];
    Arb_CtrlTvbbCtrlInfo.TvbbActFlg = input[145];
    Arb_CtrlTvbbCtrlInfo.TvbbDefectFlg = input[146];
    Arb_CtrlTvbbCtrlInfo.TvbbTarP = input[147];
    Arb_CtrlBaseBrkCtrlModInfo.VehStandStillStFlg = input[148];
    Arb_CtrlBBCCtrlInfo.BBCCtrlSt = input[149];
    Arb_CtrlEcuModeSts = input[150];
    Arb_CtrlIgnOnOffSts = input[151];
    Arb_CtrlFuncInhibitArbiSts = input[152];
    Arb_CtrlBaseBrkCtrlrActFlg = input[153];
    Arb_CtrlBrkPRednForBaseBrkCtrlr = input[154];
    Arb_CtrlPCtrlSt = input[155];
    Arb_CtrlRgnBrkCtrlrActStFlg = input[156];
    Arb_CtrlTarPFromBaseBrkCtrlr = input[157];
    Arb_CtrlTarPRateFromBaseBrkCtrlr = input[158];
    Arb_CtrlVehSpdFild = input[159];

    Arb_Ctrl();


    output[0] = Arb_CtrlFinalTarPInfo.FinalTarPOfWhlFrntLe;
    output[1] = Arb_CtrlFinalTarPInfo.FinalTarPOfWhlFrntRi;
    output[2] = Arb_CtrlFinalTarPInfo.FinalTarPOfWhlReLe;
    output[3] = Arb_CtrlFinalTarPInfo.FinalTarPOfWhlReRi;
    output[4] = Arb_CtrlFinalTarPInfo.FinalTarPRateOfWhlFrntLe;
    output[5] = Arb_CtrlFinalTarPInfo.FinalTarPRateOfWhlFrntRi;
    output[6] = Arb_CtrlFinalTarPInfo.FinalTarPRateOfWhlReLe;
    output[7] = Arb_CtrlFinalTarPInfo.FinalTarPRateOfWhlReRi;
    output[8] = Arb_CtrlFinalTarPInfo.PCtrlPrioOfWhlFrntLe;
    output[9] = Arb_CtrlFinalTarPInfo.PCtrlPrioOfWhlFrntRi;
    output[10] = Arb_CtrlFinalTarPInfo.PCtrlPrioOfWhlReLe;
    output[11] = Arb_CtrlFinalTarPInfo.PCtrlPrioOfWhlReRi;
    output[12] = Arb_CtrlFinalTarPInfo.CtrlModOfWhlFrntLe;
    output[13] = Arb_CtrlFinalTarPInfo.CtrlModOfOfWhlFrntRi;
    output[14] = Arb_CtrlFinalTarPInfo.CtrlModOfWhlReLe;
    output[15] = Arb_CtrlFinalTarPInfo.CtrlModOfWhlReRi;
    output[16] = Arb_CtrlFinalTarPInfo.ReqdPCtrlBoostMod;
    output[17] = Arb_CtrlFinalTarPInfo.FinalDelTarPOfWhlFrntLe;
    output[18] = Arb_CtrlFinalTarPInfo.FinalDelTarPOfWhlFrntRi;
    output[19] = Arb_CtrlFinalTarPInfo.FinalDelTarPOfWhlReLe;
    output[20] = Arb_CtrlFinalTarPInfo.FinalDelTarPOfWhlReRi;
    output[21] = Arb_CtrlFinalTarPInfo.FinalMaxCircuitTp;
    output[22] = Arb_CtrlWhlOutVlvCtrlTarInfo.FlOutVlvCtrlTar[0];
    output[23] = Arb_CtrlWhlOutVlvCtrlTarInfo.FlOutVlvCtrlTar[1];
    output[24] = Arb_CtrlWhlOutVlvCtrlTarInfo.FlOutVlvCtrlTar[2];
    output[25] = Arb_CtrlWhlOutVlvCtrlTarInfo.FlOutVlvCtrlTar[3];
    output[26] = Arb_CtrlWhlOutVlvCtrlTarInfo.FlOutVlvCtrlTar[4];
    output[27] = Arb_CtrlWhlOutVlvCtrlTarInfo.FrOutVlvCtrlTar[0];
    output[28] = Arb_CtrlWhlOutVlvCtrlTarInfo.FrOutVlvCtrlTar[1];
    output[29] = Arb_CtrlWhlOutVlvCtrlTarInfo.FrOutVlvCtrlTar[2];
    output[30] = Arb_CtrlWhlOutVlvCtrlTarInfo.FrOutVlvCtrlTar[3];
    output[31] = Arb_CtrlWhlOutVlvCtrlTarInfo.FrOutVlvCtrlTar[4];
    output[32] = Arb_CtrlWhlOutVlvCtrlTarInfo.RlOutVlvCtrlTar[0];
    output[33] = Arb_CtrlWhlOutVlvCtrlTarInfo.RlOutVlvCtrlTar[1];
    output[34] = Arb_CtrlWhlOutVlvCtrlTarInfo.RlOutVlvCtrlTar[2];
    output[35] = Arb_CtrlWhlOutVlvCtrlTarInfo.RlOutVlvCtrlTar[3];
    output[36] = Arb_CtrlWhlOutVlvCtrlTarInfo.RlOutVlvCtrlTar[4];
    output[37] = Arb_CtrlWhlOutVlvCtrlTarInfo.RrOutVlvCtrlTar[0];
    output[38] = Arb_CtrlWhlOutVlvCtrlTarInfo.RrOutVlvCtrlTar[1];
    output[39] = Arb_CtrlWhlOutVlvCtrlTarInfo.RrOutVlvCtrlTar[2];
    output[40] = Arb_CtrlWhlOutVlvCtrlTarInfo.RrOutVlvCtrlTar[3];
    output[41] = Arb_CtrlWhlOutVlvCtrlTarInfo.RrOutVlvCtrlTar[4];
    output[42] = Arb_CtrlWhlPreFillInfo.FlPreFillActFlg;
    output[43] = Arb_CtrlWhlPreFillInfo.FrPreFillActFlg;
    output[44] = Arb_CtrlWhlPreFillInfo.RlPreFillActFlg;
    output[45] = Arb_CtrlWhlPreFillInfo.RrPreFillActFlg;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Arb_Ctrl_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
