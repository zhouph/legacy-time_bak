/**
 * @defgroup LAMTR_DecideMtrCtrlMode LAMTR_DecideMtrCtrlMode
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LAMTR_DecideMtrCtrlMode.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "LAMTR_DecideMtrCtrlMode.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

#define S16_MTR_CALC_DUTYCMD_NA	    	(0)
#define S16_MTR_CALC_DUTYCMD_INHIBIT	(1)
#define S16_MTR_CALC_DUTYCMD_READY		(2)
#define S16_MTR_CALC_DUTYCMD_FADEOUT	(3)

#define U16_MTR_FADE_OUT_STABLE_STROKE_THR    	(500)
#define U16_MTR_FADE_OUT_STABLE_SPEED_THR   	(50)
#define S32_MTR_FADEOUT3_POSI_MIN				(S32_MTRPOSI_5MM)

#define MCC_S16_BASE_CHG_CNT_MAX	(4)
#define MCC_S16_PID_CHG_CNT_MAX		(4)

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
	uint8_t lmccu8StrkRcvrCtrlState;
	uint8_t lmccu8StrkRecvrReqFlg;
	uint8_t lmccu8InVlvAllCloseReq;
	uint8_t lmccu8MtrActForStrkRcvrFlg;

	uint16_t lmccu16IdbSystemFailFlg;
	uint16_t lmccu16IdbOrgSetFailFlg;

	int8_t lmccs8PCtrlState;
	int8_t lmccs8PCtrlBoostMod;
	int8_t lmccs8PCtrlFadeoutState;
	int8_t lmccs8PChamberVolume;

	int16_t lmccs16CircuitPressureFail;

	int32_t lmccs32Posi;

}GetMotCtrlModDet_t;
/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define MCC_1MSCTRL_START_SEC_CONST_UNSPECIFIED
#include "Mcc_MemMap.h"
/** Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
#define MCC_1MSCTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Mcc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MCC_1MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mcc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
#define MCC_1MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mcc_MemMap.h"



#define MCC_1MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Mcc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define MCC_1MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Mcc_MemMap.h"



#define MCC_1MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Mcc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define MCC_1MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Mcc_MemMap.h"



#define MCC_1MSCTRL_START_SEC_VAR_32BIT
#include "Mcc_MemMap.h"
/** Variable Section (32BIT)**/
#define MCC_1MSCTRL_STOP_SEC_VAR_32BIT
#include "Mcc_MemMap.h"



/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MCC_1MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mcc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


FAL_STATIC uint8_t lamccu8PCtrlBoostMod;

FAL_STATIC uint16_t lmccu16FadeOut2EndFlg;
FAL_STATIC uint16_t lmccu16FadeOut3EndFlg;
FAL_STATIC uint16_t lmccu16MtrCtrlInhibitFlg;
FAL_STATIC uint16_t lmccu16FadeOut2SpdStbFlg;

FAL_STATIC int16_t lmccs16MtrCtrlMode;
FAL_STATIC int16_t lmccs16MtrCtrlModeOld;
FAL_STATIC int16_t lmccs16CalcIqRefBaseState;
FAL_STATIC int16_t lmccs16CalcIqRefBaseStateOld;
FAL_STATIC int16_t lmccs16CalcIqRefPidState;
FAL_STATIC int16_t lmccs16CalcIqRefPidStateOld;
FAL_STATIC int16_t lmccs16CalcIqRefCompState;
FAL_STATIC int16_t lmccs16CalcIqRefCompStateOld;
FAL_STATIC int16_t lmccs16CalcDutyCmdState;
FAL_STATIC int16_t lmccs16PidChgCnt;
FAL_STATIC int16_t lmccs16BaseChgCnt;

FAL_STATIC GetMotCtrlModDet_t GetMotCtrlModDet;

#define MCC_1MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mcc_MemMap.h"



#define MCC_1MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Mcc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define MCC_1MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Mcc_MemMap.h"



#define MCC_1MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Mcc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define MCC_1MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Mcc_MemMap.h"



#define MCC_1MSCTRL_START_SEC_VAR_32BIT
#include "Mcc_MemMap.h"
/** Variable Section (32BIT)**/
#define MCC_1MSCTRL_STOP_SEC_VAR_32BIT
#include "Mcc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/

#define MCC_1MSCTRL_START_SEC_CODE
#include "Mcc_MemMap.h"

static void Mcc_s8TranslateBoostMode(void);
static void Mcc_vDecideMtrCtrlMode(void);
static void Mcc_vDecideCalcCurrRefState(void);

static void Mcc_vDecideCalcIqRefBaseState(void);
static void Mcc_vDecideCalcIqRefPidState(void);
static void Mcc_vDecideCalcIqRefCompState(void);

static void Mcc_vDecideCalcIqRefBaseState4LP(void);
static void Mcc_vDecideCalcIqRefPidState4LP(void);
//static void Mcc_vDecideCalcIqRefCompState4LP(void);

static void Mcc_vDecideCalcIqRefBaseState4HP(void);
static void Mcc_vDecideCalcIqRefPidState4HP(void);
//static void Mcc_vDecideCalcIqRefCompState4HP(void);

static void Mcc_vDecideCalcIqRefBaseState4BW(void);
static void Mcc_vDecideCalcIqRefPidState4BW(void);
//static void Mcc_vDecideCalcIqRefCompState4BW(void);

static void Mcc_vCallInpIfMotCtrlModDet(void);
static void Mcc_vCallOutpIfMotCtrlModDet(void);

static void Mcc_vInitDecideMtrCtrlMode(void);
static void Mcc_vInitDecideCalcIqRefState(void);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

void Mcc_vCallMotCtrlModDet(void)
{
	Mcc_vCallInpIfMotCtrlModDet();
	Mcc_s8TranslateBoostMode();
	Mcc_vDecideMtrCtrlMode();
	Mcc_vDecideCalcCurrRefState();
	Mcc_vCallOutpIfMotCtrlModDet();
}


void Mcc_vInitMotCtrlModDet(void)
{
	Mcc_vInitDecideMtrCtrlMode();
	Mcc_vInitDecideCalcIqRefState();
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static void Mcc_vCallInpIfMotCtrlModDet(void)
{
	GetMotCtrlModDet.lmccu8StrkRcvrCtrlState 		= (uint8_t)Mcc_1msCtrlBus.Mcc_1msCtrlStkRecvryActnIfInfo.StkRecvryCtrlState;
	GetMotCtrlModDet.lmccu8InVlvAllCloseReq			= (uint8_t)Mcc_1msCtrlBus.Mcc_1msCtrlInVlvAllCloseReq;
	GetMotCtrlModDet.lmccu16IdbOrgSetFailFlg 		= 0;
	GetMotCtrlModDet.lmccu16IdbSystemFailFlg 		= (uint16_t)MTRCurCTRLsrcbus.lmccu16IdbSystemFailFlg;
	GetMotCtrlModDet.lmccs8PChamberVolume 			= (int8_t)Mcc_1msCtrlBus.Mcc_1msCtrlPChamberVolume;
	GetMotCtrlModDet.lmccs8PCtrlState 				= (int8_t)Mcc_1msCtrlBus.Mcc_1msCtrlPCtrlSt;
	GetMotCtrlModDet.lmccs8PCtrlBoostMod 			= (int8_t)Mcc_1msCtrlBus.Mcc_1msCtrlPCtrlBoostMod;
	GetMotCtrlModDet.lmccs8PCtrlFadeoutState 		= (int8_t)Mcc_1msCtrlBus.Mcc_1msCtrlPCtrlFadeoutSt;
	GetMotCtrlModDet.lmccs16CircuitPressureFail 	= 0;
	GetMotCtrlModDet.lmccs32Posi					= (int32_t)Mcc_1msCtrlBus.Mcc_1msCtrlMotRotgAgSigInfo.StkPosnMeasd;

}

static void Mcc_vCallOutpIfMotCtrlModDet(void)
{
	MTRCurCTRLsrcbus.lmccu8PCtrlBoostMod					= lamccu8PCtrlBoostMod;
	MTRCurCTRLsrcbus.lmccs16MtrCtrlModeOld 				= lmccs16MtrCtrlModeOld;
	MTRCurCTRLsrcbus.lmccs16MtrCtrlMode 					= lmccs16MtrCtrlMode;
	MTRCurCTRLsrcbus.lmccs16CalcIqRefBaseState 			= lmccs16CalcIqRefBaseState;
	MTRCurCTRLsrcbus.lmccs16CalcIqRefPidStateOld			= lmccs16CalcIqRefPidStateOld;
	MTRCurCTRLsrcbus.lmccs16CalcIqRefPidState 				= lmccs16CalcIqRefPidState;
	MTRCurCTRLsrcbus.lmccs16CalcIqRefCompState 			= lmccs16CalcIqRefCompState;
	MTRCurCTRLsrcbus.lmccs16BaseChgCnt						= lmccs16BaseChgCnt;
	MTRCurCTRLsrcbus.lmccs16PidChgCnt						= lmccs16PidChgCnt;

	Mcc_1msCtrlBus.Mcc_1msCtrlMotCtrlMode					= lmccs16MtrCtrlMode;
}

void Mcc_s8TranslateBoostMode(void)
{
	switch(GetMotCtrlModDet.lmccs8PCtrlBoostMod)
	{
	case S8_BOOST_MODE_NOT_CONTROL:
		lamccu8PCtrlBoostMod = MCC_S8_BOOST_MODE_NOT_CONTROL;
		break;

	case S8_BOOST_MODE_RESPONSE_L0:	/* ABS Torque Control Mode 										*/
		lamccu8PCtrlBoostMod = MCC_S8_BOOST_MODE_IN_ABS;
		break;

	case S8_BOOST_MODE_RESPONSE_L1:	/* Quick Braking , AEB : 400bar/s 								*/
		lamccu8PCtrlBoostMod = MCC_S8_BOOST_MODE_QUICK;
		break;

	case S8_BOOST_MODE_RESPONSE_L2:	/* ESC oversteer (Include ROP), ABS, TCS_1_mode : 300bar/s 		*/
		lamccu8PCtrlBoostMod = MCC_S8_BOOST_MODE_SAFETY;
		break;

	case S8_BOOST_MODE_RESPONSE_L3:	/* TCS_2_mode, TSP, ESC understeer : 200bar/s  					*/
		lamccu8PCtrlBoostMod = MCC_S8_BOOST_MODE_SAFETY;
		break;

	case S8_BOOST_MODE_RESPONSE_L4:	/* Decel control : SCC : 200 bar/s dependent on speed			*/
	case S8_BOOST_MODE_RESPONSE_L5:	/* Decel control : BBC, RBC : 200 bar/s Normal Speed			*/
	case S8_BOOST_MODE_RESPONSE_L6:	/* Decel control : BBC, RBC : 200 bar/s Low Speed				*/
		lamccu8PCtrlBoostMod = MCC_S8_BOOST_MODE_NORMAL;
		break;

	case S8_BOOST_MODE_RESPONSE_L7:	/* Decel control : BBC, RBC : 200 bar/s Low Speed				*/
		lamccu8PCtrlBoostMod = MCC_S8_BOOST_MODE_STOP;
		break;

	case S8_BOOST_MODE_RESPONSE_L8:	/* temp location : CBC SLS, EBD 200bar/s Reduce Pressure Control function		*/
		lamccu8PCtrlBoostMod = MCC_S8_BOOST_MODE_NORMAL;
		break;

	case S8_BOOST_MODE_RESPONSE_L10:/* temp location : Release of Hold function										*/
		lamccu8PCtrlBoostMod = MCC_S8_BOOST_MODE_NORMAL;
		break;

	case S8_BOOST_MODE_RESPONSE_L9:	/* CBC SLS, EBD 200bar/s Reduce Pressure Control function		*/
		lamccu8PCtrlBoostMod = MCC_S8_BOOST_MODE_ACTIVE;
		break;

	case S8_BOOST_MODE_RESPONSE_L11:/* ABS Torque to Normal											*/
		lamccu8PCtrlBoostMod = MCC_S8_BOOST_MODE_NORMAL;
		break;

	case S8_BOOST_MODE_RESPONSE_L12:/* PreBoost Mode												*/
		lamccu8PCtrlBoostMod = MCC_S8_BOOST_MODE_NORMAL;
		break;

	case S8_BOOST_MODE_RESPONSE_L13:/* PreBoost Mode												*/
		lamccu8PCtrlBoostMod = MCC_S8_BOOST_MODE_IN_ABS_MUX;
//		lamccu8PCtrlBoostMod = S8_BOOST_MODE_SAFETY;
		break;

	default:
		lamccu8PCtrlBoostMod = GetMotCtrlModDet.lmccs8PCtrlBoostMod;
		break;
	}
}

static void Mcc_vDecideMtrCtrlMode(void)
{
	lmccs16MtrCtrlModeOld = lmccs16MtrCtrlMode;

	if ((GetMotCtrlModDet.lmccs8PCtrlState == S8_PRESS_BOOST)
			|| (GetMotCtrlModDet.lmccs8PCtrlState == S8_PRESS_PREBOOST)
			|| (GetMotCtrlModDet.lmccs8PCtrlState == S8_PRESS_FADE_OUT))
	{
		lmccs16MtrCtrlMode = MCC_S16_CALC_CURR_REF;
	}
	else
	{
		lmccs16MtrCtrlMode = MCC_S16_CALC_DUTY_CMD;
	}

	if ((GetMotCtrlModDet.lmccu8StrkRcvrCtrlState == S8_STRK_RCVR_RECOVERY1_STATE)
			|| (GetMotCtrlModDet.lmccu8StrkRcvrCtrlState == S8_STRK_RCVR_RECOVERY2_STATE)
			|| (GetMotCtrlModDet.lmccu8StrkRcvrCtrlState == S8_STRK_RCVR_STABILIZING1_STATE)
			|| (GetMotCtrlModDet.lmccu8StrkRcvrCtrlState == S8_STRK_RCVR_STABILIZING2_STATE))
	{
		lmccs16MtrCtrlMode = MCC_S16_CALC_CURR_REF;
	}
	else if (GetMotCtrlModDet.lmccu8StrkRcvrCtrlState == S8_STRK_RCVR_INHIBIT_STATE)
	{
		lmccs16MtrCtrlMode = MCC_S16_CALC_DUTY_CMD;
	}
	else
	{
		;
	}

	if ((GetMotCtrlModDet.lmccu16IdbSystemFailFlg == 1)
			|| (GetMotCtrlModDet.lmccu16IdbOrgSetFailFlg == 1)
			|| (GetMotCtrlModDet.lmccs16CircuitPressureFail == 1))
	{
		lmccs16MtrCtrlMode = MCC_S16_CALC_DUTY_CMD;
	}
}

static void Mcc_vDecideCalcCurrRefState(void)
{
	if (lmccs16MtrCtrlMode == MCC_S16_CALC_DUTY_CMD)
	{
		lmccs16CalcIqRefBaseStateOld = MCC_S16_BASE_NA;
		lmccs16CalcIqRefPidStateOld = MCC_S16_PID_NA;
		lmccs16CalcIqRefCompStateOld = MCC_S16_COMP_NA;

		lmccs16CalcIqRefBaseState = MCC_S16_BASE_NA;
		lmccs16CalcIqRefPidState = MCC_S16_PID_NA;
		lmccs16CalcIqRefCompState = MCC_S16_COMP_NA;

		lmccs16BaseChgCnt = 0;
		lmccs16PidChgCnt = 0;
	}
	else
	{
		Mcc_vDecideCalcIqRefBaseState();
		Mcc_vDecideCalcIqRefPidState();
		Mcc_vDecideCalcIqRefCompState();
	}
}

static void Mcc_vDecideCalcIqRefBaseState(void)
{
	if (lmccs16BaseChgCnt == 0)
	{
		lmccs16CalcIqRefBaseStateOld = lmccs16CalcIqRefBaseState;
	}

	if (GetMotCtrlModDet.lmccs8PChamberVolume == U8_BOOST_LOW_PRESS)
	{
		Mcc_vDecideCalcIqRefBaseState4LP();
	}
	else if (GetMotCtrlModDet.lmccs8PChamberVolume == U8_BOOST_HIGH_PRESS)
	{
		Mcc_vDecideCalcIqRefBaseState4HP();
	}
	else if (GetMotCtrlModDet.lmccs8PChamberVolume == U8_BOOST_BACKWARD)
	{
		Mcc_vDecideCalcIqRefBaseState4BW();
	}
	else
	{
		lmccs16CalcIqRefBaseState = MCC_S16_BASE_NA;
	}

	if (lmccs16CalcIqRefBaseStateOld != lmccs16CalcIqRefBaseState)
	{
		lmccs16BaseChgCnt = 1;
	}
	else
	{
		if ((lmccs16BaseChgCnt >= 1) && (lmccs16BaseChgCnt < MCC_S16_BASE_CHG_CNT_MAX))
		{
			lmccs16BaseChgCnt++;
		}
		else
		{
			lmccs16BaseChgCnt = 0;
		}
	}
}

static void Mcc_vDecideCalcIqRefBaseState4LP(void)
{
	if (GetMotCtrlModDet.lmccs8PCtrlState == S8_PRESS_BOOST)
	{
		if ((lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_IN_ABS)
				|| (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_IN_ABS_MUX)
				|| (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_ACTIVE)
				|| (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_NORMAL)
				|| (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_CREEP)
				|| (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_STOP)
				|| (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_NOT_CONTROL))
		{
			lmccs16CalcIqRefBaseState = MCC_S16_BASE_FW_LP;
		}
		else
		{
			lmccs16CalcIqRefBaseState = MCC_S16_BASE_NA;
		}
	}
	else
	{
		lmccs16CalcIqRefBaseState = MCC_S16_BASE_NA;
	}

	if (GetMotCtrlModDet.lmccu8StrkRcvrCtrlState == S8_STRK_RCVR_RECOVERY1_STATE)
	{
		lmccs16CalcIqRefBaseState = MCC_S16_BASE_RS1_LP;
	}
	else if (GetMotCtrlModDet.lmccu8StrkRcvrCtrlState == S8_STRK_RCVR_RECOVERY2_STATE)
	{
		lmccs16CalcIqRefBaseState = MCC_S16_BASE_RS2_LP;
	}
	else if (GetMotCtrlModDet.lmccu8StrkRcvrCtrlState == S8_STRK_RCVR_STABILIZING1_STATE)
	{
		lmccs16CalcIqRefBaseState = MCC_S16_BASE_STB1_LP;
	}
	else if (GetMotCtrlModDet.lmccu8StrkRcvrCtrlState == S8_STRK_RCVR_STABILIZING2_STATE)
	{
		lmccs16CalcIqRefBaseState = MCC_S16_BASE_STB2_LP;
	}
	else if (GetMotCtrlModDet.lmccu8StrkRcvrCtrlState == S8_STRK_RCVR_INHIBIT_STATE)
	{
		lmccs16CalcIqRefBaseState = MCC_S16_BASE_NA;
	}
	else
	{
		;
	}
}

static void Mcc_vDecideCalcIqRefBaseState4HP(void)
{
	if (GetMotCtrlModDet.lmccs8PCtrlState == S8_PRESS_BOOST)
	{
		if ((lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_IN_ABS)
				|| (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_IN_ABS_MUX)
				|| (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_ACTIVE)
				|| (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_NORMAL)
				|| (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_CREEP)
				|| (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_STOP)
				|| (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_NOT_CONTROL))
		{
			lmccs16CalcIqRefBaseState = MCC_S16_BASE_FW_HP;
		}
		else
		{
			lmccs16CalcIqRefBaseState = MCC_S16_BASE_NA;
		}
	}
	else
	{
		lmccs16CalcIqRefBaseState = MCC_S16_BASE_NA;
	}

	if (GetMotCtrlModDet.lmccu8StrkRcvrCtrlState == S8_STRK_RCVR_RECOVERY1_STATE)
	{
		lmccs16CalcIqRefBaseState = MCC_S16_BASE_RS1_HP;
	}
	else if (GetMotCtrlModDet.lmccu8StrkRcvrCtrlState == S8_STRK_RCVR_RECOVERY2_STATE)
	{
		lmccs16CalcIqRefBaseState = MCC_S16_BASE_RS2_HP;
	}
	else if (GetMotCtrlModDet.lmccu8StrkRcvrCtrlState == S8_STRK_RCVR_STABILIZING1_STATE)
	{
		lmccs16CalcIqRefBaseState = MCC_S16_BASE_STB1_HP;
	}
	else if (GetMotCtrlModDet.lmccu8StrkRcvrCtrlState == S8_STRK_RCVR_STABILIZING2_STATE)
	{
		lmccs16CalcIqRefBaseState = MCC_S16_BASE_STB2_HP;
	}
	else if (GetMotCtrlModDet.lmccu8StrkRcvrCtrlState == S8_STRK_RCVR_INHIBIT_STATE)
	{
		lmccs16CalcIqRefBaseState = MCC_S16_BASE_NA;
	}
	else
	{
		;
	}
}

static void Mcc_vDecideCalcIqRefBaseState4BW(void)
{
	if (GetMotCtrlModDet.lmccs8PCtrlState == S8_PRESS_BOOST)
	{
		if ((lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_IN_ABS)
				|| (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_IN_ABS_MUX)
				|| (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_ACTIVE)
				|| (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_NORMAL)
				|| (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_CREEP)
				|| (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_STOP)
				|| (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_NOT_CONTROL))
		{
			lmccs16CalcIqRefBaseState = MCC_S16_BASE_FW_BW;
		}
		else
		{
			lmccs16CalcIqRefBaseState = MCC_S16_BASE_NA;
		}
	}
	else
	{
		lmccs16CalcIqRefBaseState = MCC_S16_BASE_NA;
	}

	if (GetMotCtrlModDet.lmccu8StrkRcvrCtrlState == S8_STRK_RCVR_RECOVERY1_STATE)
	{
		lmccs16CalcIqRefBaseState = MCC_S16_BASE_RS1_BW;
	}
	else if (GetMotCtrlModDet.lmccu8StrkRcvrCtrlState == S8_STRK_RCVR_RECOVERY2_STATE)
	{
		lmccs16CalcIqRefBaseState = MCC_S16_BASE_RS2_BW;
	}
	else if (GetMotCtrlModDet.lmccu8StrkRcvrCtrlState == S8_STRK_RCVR_STABILIZING1_STATE)
	{
		lmccs16CalcIqRefBaseState = MCC_S16_BASE_STB1_BW;
	}
	else if (GetMotCtrlModDet.lmccu8StrkRcvrCtrlState == S8_STRK_RCVR_STABILIZING2_STATE)
	{
		lmccs16CalcIqRefBaseState = MCC_S16_BASE_STB2_BW;
	}
	else if (GetMotCtrlModDet.lmccu8StrkRcvrCtrlState == S8_STRK_RCVR_INHIBIT_STATE)
	{
		lmccs16CalcIqRefBaseState = MCC_S16_BASE_NA;
	}
	else
	{
		;
	}
}

static void Mcc_vDecideCalcIqRefPidState(void)
{
	if (GetMotCtrlModDet.lmccs8PChamberVolume == U8_BOOST_LOW_PRESS)
	{
		Mcc_vDecideCalcIqRefPidState4LP();
	}
	else if (GetMotCtrlModDet.lmccs8PChamberVolume == U8_BOOST_HIGH_PRESS)
	{
		Mcc_vDecideCalcIqRefPidState4HP();
	}
	else if (GetMotCtrlModDet.lmccs8PChamberVolume == U8_BOOST_BACKWARD)
	{
		Mcc_vDecideCalcIqRefPidState4BW();
	}
	else
	{
		lmccs16CalcIqRefPidState = MCC_S16_PID_NA;
	}
}

static void Mcc_vDecideCalcIqRefPidState4LP(void)
{
	if (lmccs16PidChgCnt <= 0)
	{
		/* pid state is backed up only when pid state is not changing */
		lmccs16CalcIqRefPidStateOld = lmccs16CalcIqRefPidState;
	}

	if (GetMotCtrlModDet.lmccs8PCtrlState == S8_PRESS_BOOST)
	{
		if (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_IN_ABS)
		{
			lmccs16CalcIqRefPidState = MCC_S16_PID_SPEED_NORMAL;
		}
		else if (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_IN_ABS_MUX)
		{
#if MCC_S16_ABS_MUX_PATTERN == 1
			lmccs16CalcIqRefPidState = MCC_S16_PID_NA;
#else
			if (GetMotCtrlModDet.lmccu8InVlvAllCloseReq == 1)
			{
				/*lmccs16CalcIqRefPidState = MCC_S16_PID_POSI_ABS_MUX; // MCC_S16_PID_PRES_MUX;*/
				lmccs16CalcIqRefPidState = MCC_S16_PID_PRES_MUX;
			}
			else
			{
				lmccs16CalcIqRefPidState = MCC_S16_PID_POSI_ABS_MUX;
			}
#endif
		}
		else if (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_ACTIVE)
		{
			lmccs16CalcIqRefPidState = MCC_S16_PID_SPEED_ACTIVE;
		}
		else if (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_SAFETY)
		{
			lmccs16CalcIqRefPidState = MCC_S16_PID_SPEED_SAFETY;
		}
		else if (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_NORMAL)
		{
			lmccs16CalcIqRefPidState = MCC_S16_PID_SPEED_NORMAL;
		}
		else if (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_CREEP)
		{
			lmccs16CalcIqRefPidState = MCC_S16_PID_SPEED_CREEP;
		}
		else if (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_STOP)
		{
			lmccs16CalcIqRefPidState = MCC_S16_PID_SPEED_STOP;
		}
		else if (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_QUICK)
		{
			lmccs16CalcIqRefPidState = MCC_S16_PID_SPEED_NORMAL;
		}
		else
		{
			lmccs16CalcIqRefPidState = MCC_S16_PID_NA;
		}
	}
	else if (GetMotCtrlModDet.lmccs8PCtrlState == S8_PRESS_FADE_OUT)
	{
		if (GetMotCtrlModDet.lmccs8PCtrlFadeoutState == S8_PCTRL_FADEOUT_STATE_1)
		{
			lmccs16CalcIqRefPidState = MCC_S16_PID_SPEED_FADEOUT1;
		}
		else if (GetMotCtrlModDet.lmccs8PCtrlFadeoutState == S8_PCTRL_FADEOUT_STATE_2)
		{
			lmccs16CalcIqRefPidState = MCC_S16_PID_SPEED_FADEOUT2;
		}
		else if (GetMotCtrlModDet.lmccs8PCtrlFadeoutState == S8_PCTRL_FADEOUT_STATE_3)
		{
			lmccs16CalcIqRefPidState = MCC_S16_PID_SPEED_FADEOUT3;
		}
		else
		{
			;
		}
	}
	else
	{
		lmccs16CalcIqRefPidState = MCC_S16_PID_NA;
	}

	if ((lmccs16CalcIqRefPidState != lmccs16CalcIqRefPidStateOld)	&& (lmccs16PidChgCnt <= 0))
	{
		lmccs16PidChgCnt = 1;
	}
	else
	{
		if ((lmccs16PidChgCnt >= 1) && (lmccs16PidChgCnt < MCC_S16_PID_CHG_CNT_MAX))
		{
			lmccs16PidChgCnt = lmccs16PidChgCnt + 1;
		}
		else
		{
			lmccs16PidChgCnt = 0;
		}
	}

	if ((GetMotCtrlModDet.lmccu8StrkRcvrCtrlState == S8_STRK_RCVR_RECOVERY2_STATE)
			|| (GetMotCtrlModDet.lmccu8StrkRcvrCtrlState == S8_STRK_RCVR_STABILIZING1_STATE)
			|| (GetMotCtrlModDet.lmccu8StrkRcvrCtrlState == S8_STRK_RCVR_STABILIZING2_STATE)
			|| (GetMotCtrlModDet.lmccu8StrkRcvrCtrlState == S8_STRK_RCVR_INHIBIT_STATE))
	{
		lmccs16CalcIqRefPidState = MCC_S16_PID_NA;
		lmccs16PidChgCnt = -1;
	}
}

static void Mcc_vDecideCalcIqRefPidState4HP(void)
{
	if (lmccs16PidChgCnt <= 0)
	{
		/* pid state is backed up only when pid state is not changing */
		lmccs16CalcIqRefPidStateOld = lmccs16CalcIqRefPidState;
	}

	if (GetMotCtrlModDet.lmccs8PCtrlState == S8_PRESS_BOOST)
	{
		if (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_IN_ABS)
		{
			lmccs16CalcIqRefPidState = MCC_S16_PID_SPEED_NORMAL;
		}
		else if (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_IN_ABS_MUX)
		{
			if (GetMotCtrlModDet.lmccu8InVlvAllCloseReq == 1)
			{
				lmccs16CalcIqRefPidState = MCC_S16_PID_PRES_MUX;
			}
			else
			{
				lmccs16CalcIqRefPidState = MCC_S16_PID_POSI_ABS_MUX;
			}
		}
		else if (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_ACTIVE)
		{
			lmccs16CalcIqRefPidState = MCC_S16_PID_SPEED_ACTIVE;
		}
		else if (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_SAFETY)
		{
			lmccs16CalcIqRefPidState = MCC_S16_PID_SPEED_SAFETY;
		}
		else if (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_NORMAL)
		{
			lmccs16CalcIqRefPidState = MCC_S16_PID_SPEED_NORMAL;
		}
		else if (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_CREEP)
		{
			lmccs16CalcIqRefPidState = MCC_S16_PID_SPEED_CREEP;
		}
		else if (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_STOP)
		{
			lmccs16CalcIqRefPidState = MCC_S16_PID_SPEED_STOP;
		}
		else if (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_QUICK)
		{
			lmccs16CalcIqRefPidState = MCC_S16_PID_SPEED_NORMAL;
		}
		else
		{
			lmccs16CalcIqRefPidState = MCC_S16_PID_NA;
		}
	}
	else if (GetMotCtrlModDet.lmccs8PCtrlState == S8_PRESS_FADE_OUT)
	{
		if (GetMotCtrlModDet.lmccs8PCtrlFadeoutState == S8_PCTRL_FADEOUT_STATE_1)
		{
			lmccs16CalcIqRefPidState = MCC_S16_PID_SPEED_FADEOUT1;
		}
		else if (GetMotCtrlModDet.lmccs8PCtrlFadeoutState == S8_PCTRL_FADEOUT_STATE_2)
		{
			lmccs16CalcIqRefPidState = MCC_S16_PID_SPEED_FADEOUT2;
		}
		else if (GetMotCtrlModDet.lmccs8PCtrlFadeoutState == S8_PCTRL_FADEOUT_STATE_3)
		{
			lmccs16CalcIqRefPidState = MCC_S16_PID_SPEED_FADEOUT3;
		}
		else
		{
			;
		}
	}
	else
	{
		lmccs16CalcIqRefPidState = MCC_S16_PID_NA;
	}

	if ((lmccs16CalcIqRefPidState != lmccs16CalcIqRefPidStateOld)	&& (lmccs16PidChgCnt <= 0))
	{
		lmccs16PidChgCnt = 1;
	}
	else
	{
		if ((lmccs16PidChgCnt >= 1) && (lmccs16PidChgCnt < MCC_S16_PID_CHG_CNT_MAX))
		{
			lmccs16PidChgCnt = lmccs16PidChgCnt + 1;
		}
		else
		{
			lmccs16PidChgCnt = 0;
		}
	}

	/* exception */
	if ((GetMotCtrlModDet.lmccu8StrkRcvrCtrlState == S8_STRK_RCVR_RECOVERY2_STATE)
			|| (GetMotCtrlModDet.lmccu8StrkRcvrCtrlState == S8_STRK_RCVR_STABILIZING1_STATE)
			|| (GetMotCtrlModDet.lmccu8StrkRcvrCtrlState == S8_STRK_RCVR_STABILIZING2_STATE)
			|| (GetMotCtrlModDet.lmccu8StrkRcvrCtrlState == S8_STRK_RCVR_INHIBIT_STATE))
	{
		lmccs16CalcIqRefPidState = MCC_S16_PID_NA;
		lmccs16PidChgCnt = -1;
	}
}

static void Mcc_vDecideCalcIqRefPidState4BW(void)
{
	if (lmccs16PidChgCnt <= 0)
	{
		/* pid state is backed up only when pid state is not changing */
		lmccs16CalcIqRefPidStateOld = lmccs16CalcIqRefPidState;
	}

	if (GetMotCtrlModDet.lmccs8PCtrlState == S8_PRESS_BOOST)
	{
		if (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_IN_ABS)
		{
			lmccs16CalcIqRefPidState = MCC_S16_PID_SPEED_NORMAL;
		}
		else if (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_IN_ABS_MUX)
		{
			if (GetMotCtrlModDet.lmccu8InVlvAllCloseReq == 1)
			{
				lmccs16CalcIqRefPidState = MCC_S16_PID_PRES_MUX;
			}
			else
			{
				lmccs16CalcIqRefPidState = MCC_S16_PID_POSI_ABS_MUX;
			}
		}
		else if (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_ACTIVE)
		{
			lmccs16CalcIqRefPidState = MCC_S16_PID_SPEED_ACTIVE;
		}
		else if (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_SAFETY)
		{
			lmccs16CalcIqRefPidState = MCC_S16_PID_SPEED_SAFETY;
		}
		else if (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_NORMAL)
		{
			lmccs16CalcIqRefPidState = MCC_S16_PID_SPEED_NORMAL;
		}
		else if (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_CREEP)
		{
			lmccs16CalcIqRefPidState = MCC_S16_PID_SPEED_CREEP;
		}
		else if (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_STOP)
		{
			lmccs16CalcIqRefPidState = MCC_S16_PID_SPEED_STOP;
		}
		else if (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_QUICK)
		{
			lmccs16CalcIqRefPidState = MCC_S16_PID_SPEED_NORMAL;
		}
		else
		{
			lmccs16CalcIqRefPidState = MCC_S16_PID_NA;
		}
	}
	else if (GetMotCtrlModDet.lmccs8PCtrlState == S8_PRESS_FADE_OUT)
	{
		if (GetMotCtrlModDet.lmccs8PCtrlFadeoutState == S8_PCTRL_FADEOUT_STATE_1)
		{
			lmccs16CalcIqRefPidState = MCC_S16_PID_SPEED_FADEOUT1;
		}
		else if (GetMotCtrlModDet.lmccs8PCtrlFadeoutState == S8_PCTRL_FADEOUT_STATE_2)
		{
			lmccs16CalcIqRefPidState = MCC_S16_PID_SPEED_FADEOUT2;
		}
		else if (GetMotCtrlModDet.lmccs8PCtrlFadeoutState == S8_PCTRL_FADEOUT_STATE_3)
		{
			lmccs16CalcIqRefPidState = MCC_S16_PID_SPEED_FADEOUT3;
		}
		else
		{
			;
		}
	}
	else
	{
		lmccs16CalcIqRefPidState = MCC_S16_PID_NA;
	}

	if ((lmccs16CalcIqRefPidState != lmccs16CalcIqRefPidStateOld) && (lmccs16PidChgCnt <= 0))
	{
		lmccs16PidChgCnt = 1;
	}
	else
	{
		if ((lmccs16PidChgCnt >= 1) && (lmccs16PidChgCnt < MCC_S16_PID_CHG_CNT_MAX))
		{
			lmccs16PidChgCnt = lmccs16PidChgCnt + 1;
		}
		else
		{
			lmccs16PidChgCnt = 0;
		}
	}

	if ((GetMotCtrlModDet.lmccu8StrkRcvrCtrlState == S8_STRK_RCVR_RECOVERY2_STATE)
			|| (GetMotCtrlModDet.lmccu8StrkRcvrCtrlState == S8_STRK_RCVR_STABILIZING1_STATE)
			|| (GetMotCtrlModDet.lmccu8StrkRcvrCtrlState == S8_STRK_RCVR_STABILIZING2_STATE)
			|| (GetMotCtrlModDet.lmccu8StrkRcvrCtrlState == S8_STRK_RCVR_INHIBIT_STATE))
	{
		lmccs16CalcIqRefPidState = MCC_S16_PID_NA;
		lmccs16PidChgCnt = -1;
	}
}

static void Mcc_vDecideCalcIqRefCompState(void)
{
	if (lamccu8PCtrlBoostMod == MCC_S8_BOOST_MODE_IN_ABS_MUX)
	{
		lmccs16CalcIqRefCompState = MCC_S16_COMP_ABS_MUX_PATTERN;
	}
	else
	{
		lmccs16CalcIqRefCompState = MCC_S16_COMP_NA;
	}
}

static void Mcc_vInitDecideMtrCtrlMode(void)
{
	lmccs16MtrCtrlModeOld = MCC_S16_CALC_DUTY_CMD;
}

static void Mcc_vInitDecideCalcIqRefState(void)
{
	lmccs16CalcIqRefBaseState = MCC_S16_BASE_NA;
	lmccs16CalcIqRefPidState = MCC_S16_PID_NA;
	lmccs16CalcIqRefCompState = MCC_S16_COMP_NA;
}

#define MCC_1MSCTRL_STOP_SEC_CODE
#include "Mcc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
