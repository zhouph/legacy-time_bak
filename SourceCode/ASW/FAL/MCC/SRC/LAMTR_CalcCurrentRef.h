/**
 * @defgroup LAMTR_CalcCurrentRef LAMTR_CalcCurrentRef
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LAMTR_CalcCurrentRef.h
 * @brief       Local Header File
 * @date        20YY. MM. DD.
 ******************************************************************************/
 
#ifndef LAMTR_CALCCURRENTREF_H_
#define LAMTR_CALCCURRENTREF_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Mcc_1msCtrl.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
extern Msp_CtrlMotRotgAgSigInfo_t Msp_CtrlMotRotgAgSigInfo;/* F514 KYB �ӽ���ġ*/
/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Mcc_vCallMotIRefCalcn(void);
extern void Mcc_vInitMotIRefCalcn(void);

//extern int16_t Mcc_s16GetFadeoutDeltaTgtStrk(mcc_fo_delta_tgtstrk_t stFadeoutTgtStrk);
//extern uint16_t Mcc_u16DctMtrFadeoutEnd(int8_t s8PCtrlFadeoutState, uint16_t u16MotorFadeoutEndOK, int32_t s32CurrentPosi, int16_t s16DeltaStrokeTarget);
//extern int16_t LAMCC_s16GetCircuitPStableCnt(int16_t s16CircuitP, int16_t s16PStableCnt);
//extern int16_t Mcc_s16CalcFadeoutDTgtStrk(int32_t s32MtrTargetPosi, int16_t s16DeltaStrokeTargetOld, int32_t s32CurrentPosi, int16_t s16PosiError);
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* LAMTR_CALCCURRENTREF_H_ */
/** @} */
