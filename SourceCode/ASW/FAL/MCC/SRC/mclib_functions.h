#ifndef __MCLIB_FUNCTIONS_H__
#define __MCLIB_FUNCTIONS_H__



extern Frac16 LAMTR_f16Sin(int16_t x);
extern Frac16 LAMTR_f16Cos(int16_t x);
extern void MCC_vInitPid(mcc_pid_t *pPid);
extern int16_t Mcc_s16PIDController(int32_t xRef, int32_t x, mcc_pid_t *pParam);
extern void MCC_vChangePid(mcc_pid_t *pAfter, mcc_pid_t* pBefore);
#endif
