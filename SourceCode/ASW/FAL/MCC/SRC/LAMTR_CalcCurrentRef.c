/**
 * @defgroup LAMTR_CalcCurrentRef LAMTR_CalcCurrentRef
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LAMTR_CalcCurrentRef.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "LAMTR_CalcCurrentRef.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define MCC_S32_TGT_POSI_MAX   			(HW_MAX_STROKE_PULSE)		/* check msp's max position */
#define	MCC_S32_TGT_POSI_MIN	   		(-S32_MTRPOSI_0_5MM)		/* check msp's min position */

#define MCC_S16_RPM_SCF					(29)

#define	MCC_S16_DELTA_STRK_MIN			(-2000)
#define	MCC_S16_DELTA_STRK_MAX			(2000)

#define MCC_U16_FLUX_WEAKENING_CTRL				(1)
#define MCC_S16_FLUXWEAK_CONDITION_12V_99A	(1299)
#define MCC_S16_FLUXWEAK_CONDITION_12V_120A	(11120)
#define MCC_S16_FLUXWEAK_CONDITION			MCC_S16_FLUXWEAK_CONDITION_12V_99A

#if MCC_S16_FLUXWEAK_CONDITION == MCC_S16_FLUXWEAK_CONDITION_12V_99A
	#define MCC_S16_IQREF_MAX 						S16_MTRCURR_99A
	#define MCC_S16_IQREF_MIN 						(-S16_MTRCURR_99A)
#elif MCC_S16_FLUXWEAK_CONDITION == MCC_S16_FLUXWEAK_CONDITION_12V_120A
	#define MCC_S16_IQREF_MAX 						S16_MTRCURR_120A
	#define MCC_S16_IQREF_MIN 						(-S16_MTRCURR_120A)
#else
	#define MCC_S16_IQREF_MAX 						S16_MTRCURR_99A
	#define MCC_S16_IQREF_MIN 						(-S16_MTRCURR_99A)
#endif

#define	MCC_S16_BETA_RPM0_12V_99A		(2000)
#define	MCC_S16_BETA_RPM1_12V_99A		(2300)
#define	MCC_S16_BETA_RPM2_12V_99A		(2600)
#define	MCC_S16_BETA_RPM3_12V_99A		(3000)
#define	MCC_S16_BETA_RPM4_12V_99A		(3250)
#define	MCC_S16_BETA_RPM5_12V_99A		(3500)
#define	MCC_S16_BETA_RPM6_12V_99A		(3750)
#define	MCC_S16_BETA_RPM7_12V_99A		(4000)
#define	MCC_S16_BETA_RPM8_12V_99A		(4250)
#define	MCC_S16_BETA_RPM9_12V_99A		(4500)
#define	MCC_S16_BETA_RPM10_12V_99A		(4750)
#define	MCC_S16_BETA_RPM11_12V_99A		(5000)

#define	MCC_S16_BETA_ANGLE0_12V_99A		(0)
#define	MCC_S16_BETA_ANGLE1_12V_99A		(169)
#define	MCC_S16_BETA_ANGLE2_12V_99A		(310)
#define	MCC_S16_BETA_ANGLE3_12V_99A		(470)
#define	MCC_S16_BETA_ANGLE4_12V_99A		(554)
#define	MCC_S16_BETA_ANGLE5_12V_99A		(625)
#define	MCC_S16_BETA_ANGLE6_12V_99A		(684)
#define	MCC_S16_BETA_ANGLE7_12V_99A		(730)
#define	MCC_S16_BETA_ANGLE8_12V_99A		(764)
#define	MCC_S16_BETA_ANGLE9_12V_99A		(785)
#define	MCC_S16_BETA_ANGLE10_12V_99A	(794)
#define	MCC_S16_BETA_ANGLE11_12V_99A	(790)

#define	MCC_S16_BETA_RPM0_12V_120A		(2900)
#define	MCC_S16_BETA_RPM1_12V_120A		(3272)
#define	MCC_S16_BETA_RPM2_12V_120A		(3645)
#define	MCC_S16_BETA_RPM3_12V_120A		(4018)
#define	MCC_S16_BETA_RPM4_12V_120A		(4390)
#define	MCC_S16_BETA_RPM5_12V_120A		(4763)
#define	MCC_S16_BETA_RPM6_12V_120A		(5136)
#define	MCC_S16_BETA_RPM7_12V_120A		(5509)
#define	MCC_S16_BETA_RPM8_12V_120A		(5881)
#define	MCC_S16_BETA_RPM9_12V_120A		(6254)
#define	MCC_S16_BETA_RPM10_12V_120A		(6627)
#define	MCC_S16_BETA_RPM11_12V_120A		(7000)

#define	MCC_S16_BETA_ANGLE0_12V_120A	(0)
#define	MCC_S16_BETA_ANGLE1_12V_120A	(117)
#define	MCC_S16_BETA_ANGLE2_12V_120A	(207)
#define	MCC_S16_BETA_ANGLE3_12V_120A	(278)
#define	MCC_S16_BETA_ANGLE4_12V_120A	(338)
#define	MCC_S16_BETA_ANGLE5_12V_120A	(387)
#define	MCC_S16_BETA_ANGLE6_12V_120A	(430)
#define	MCC_S16_BETA_ANGLE7_12V_120A	(466)
#define	MCC_S16_BETA_ANGLE8_12V_120A	(499)
#define	MCC_S16_BETA_ANGLE9_12V_120A	(528)
#define	MCC_S16_BETA_ANGLE10_12V_120A	(553)
#define	MCC_S16_BETA_ANGLE11_12V_120A	(577)

#define MCC_S16_FADEOUT_OK_THR							(S16_PCTRL_PRESS_0_3_BAR)

#define MCC_S16_FADEOUT_POSITION_L5						(3000)
#define MCC_S16_FADEOUT_POSITION_L4						(1000)
#define MCC_S16_FADEOUT_POSITION_L3						(500)
#define MCC_S16_FADEOUT_POSITION_L2						(200)
#define MCC_S16_FADEOUT_POSITION_L1						(100)
#define MCC_S16_FADEOUT_POSITION_L0						(50)

#define MCC_S16_FADEOUT_DEL_STROKE_MAX_SPD				S16_SPEED_40_KPH
#define MCC_S16_FADEOUT_DEL_STROKE_AFTER_ESC			(-150)
#define MCC_S16_FADEOUT_DEL_STROKE_MAX					(-120)
#define MCC_S16_FADEOUT_DEL_STROKE_L5					(-70)
#define MCC_S16_FADEOUT_DEL_STROKE_L4					(-50)
#define MCC_S16_FADEOUT_DEL_STROKE_L3					(-30)
#define MCC_S16_FADEOUT_DEL_STROKE_L2					(-20)
#define MCC_S16_FADEOUT_DEL_STROKE_L1					(-10)
#define MCC_S16_FADEOUT_DEL_STROKE_L0					(-5)

#define	MCC_S16_TARGET_PRES_01			(0   )
#define	MCC_S16_TARGET_PRES_02			(100 )
#define	MCC_S16_TARGET_PRES_03			(200 )
#define	MCC_S16_TARGET_PRES_04			(300 )
#define	MCC_S16_TARGET_PRES_05			(400 )
#define	MCC_S16_TARGET_PRES_06			(500 )
#define	MCC_S16_TARGET_PRES_07			(600 )
#define	MCC_S16_TARGET_PRES_08			(800 )
#define	MCC_S16_TARGET_PRES_09			(1000)
#define	MCC_S16_TARGET_PRES_10			(1200)
#define	MCC_S16_TARGET_PRES_11			(1400)
#define	MCC_S16_TARGET_PRES_12			(1600)
#define	MCC_S16_TARGET_PRES_13			(1800)
#define	MCC_S16_TARGET_PRES_14			(2000)
#define	MCC_S16_TARGET_PRES_MAX			(2500)

#define MCC_S16_IQREFBASE_FW_01_LP		(0   )
#define MCC_S16_IQREFBASE_FW_02_LP		(381 )
#define MCC_S16_IQREFBASE_FW_03_LP      (847 )
#define MCC_S16_IQREFBASE_FW_04_LP      (1312)
#define MCC_S16_IQREFBASE_FW_05_LP      (1777)
#define MCC_S16_IQREFBASE_FW_06_LP      (2243)
#define MCC_S16_IQREFBASE_FW_07_LP      (2708)
#define MCC_S16_IQREFBASE_FW_08_LP      (3639)
#define MCC_S16_IQREFBASE_FW_09_LP      (4569)
#define MCC_S16_IQREFBASE_FW_10_LP      (5500)
#define MCC_S16_IQREFBASE_FW_11_LP      (6431)
#define MCC_S16_IQREFBASE_FW_12_LP      (7361)
#define MCC_S16_IQREFBASE_FW_13_LP      (8292)
#define MCC_S16_IQREFBASE_FW_14_LP      (9223)
#define MCC_S16_IQREFBASE_FW_MAX_LP     (11549)

#define MCC_S16_IQREFBASE_FW_01_HP		(0   )
#define MCC_S16_IQREFBASE_FW_02_HP      (130 )
#define MCC_S16_IQREFBASE_FW_03_HP      (228 )
#define MCC_S16_IQREFBASE_FW_04_HP      (446 )
#define MCC_S16_IQREFBASE_FW_05_HP      (604 )
#define MCC_S16_IQREFBASE_FW_06_HP      (762 )
#define MCC_S16_IQREFBASE_FW_07_HP      (920 )
#define MCC_S16_IQREFBASE_FW_08_HP      (1236)
#define MCC_S16_IQREFBASE_FW_09_HP      (1552)
#define MCC_S16_IQREFBASE_FW_10_HP      (1868)
#define MCC_S16_IQREFBASE_FW_11_HP      (2184)
#define MCC_S16_IQREFBASE_FW_12_HP      (2501)
#define MCC_S16_IQREFBASE_FW_13_HP      (2817)
#define MCC_S16_IQREFBASE_FW_14_HP      (3133)
#define MCC_S16_IQREFBASE_FW_MAX_HP     (3923)

#define MCC_S16_IQREFBASE_FW_01_BW		(0 )
#define MCC_S16_IQREFBASE_FW_02_BW      (252 )
#define MCC_S16_IQREFBASE_FW_03_BW      (559 )
#define MCC_S16_IQREFBASE_FW_04_BW      (866 )
#define MCC_S16_IQREFBASE_FW_05_BW      (1174)
#define MCC_S16_IQREFBASE_FW_06_BW      (1481)
#define MCC_S16_IQREFBASE_FW_07_BW      (1788)
#define MCC_S16_IQREFBASE_FW_08_BW      (2403)
#define MCC_S16_IQREFBASE_FW_09_BW      (3017)
#define MCC_S16_IQREFBASE_FW_10_BW      (3632)
#define MCC_S16_IQREFBASE_FW_11_BW      (4246)
#define MCC_S16_IQREFBASE_FW_12_BW      (4861)
#define MCC_S16_IQREFBASE_FW_13_BW      (5745)
#define MCC_S16_IQREFBASE_FW_14_BW      (6090)
#define MCC_S16_IQREFBASE_FW_MAX_BW     (7626)

#define MCC_S32_ORIGIN_SET_SENSOR_MARGIN				(10)
#define MCC_S32_ORIGIN_SET_MAX_THR						(35)
#define MCC_S32_ORIGIN_SET_MIN_THR						(-35)

#define MCC_S16_FADEOUT_DEL_STROKE_OLD_L3				(-10)
#define MCC_S16_FADEOUT_DEL_STROKE_OLD_L2				(-20)
#define MCC_S16_FADEOUT_DEL_STROKE_OLD_L1				(-50)
#define MCC_S16_FADEOUT_DEL_STROKE_OLD_L0				(-100)

#define MCC_S16_FADEOUT_DEL_STROKE_DIFF_L3				(1)
#define MCC_S16_FADEOUT_DEL_STROKE_DIFF_L2				(5)
#define MCC_S16_FADEOUT_DEL_STROKE_DIFF_L1				(10)
#define MCC_S16_FADEOUT_DEL_STROKE_DIFF_L0				(10)

#define MCC_S16_FADEIN_POSITION_L3						(-50)
#define MCC_S16_FADEIN_POSITION_L2						(-100)
#define MCC_S16_FADEIN_POSITION_L1						(-200)
#define MCC_S16_FADEIN_POSITION_L0						(-500)

#define MCC_S16_FADEIN_DEL_STROKE_L3					(5)
#define MCC_S16_FADEIN_DEL_STROKE_L2					(10)
#define MCC_S16_FADEIN_DEL_STROKE_L1					(20)
#define MCC_S16_FADEIN_DEL_STROKE_L0					(50)

#define MCC_S16_FADEIN_DEL_STROKE_OLD_L3				(100)
#define MCC_S16_FADEIN_DEL_STROKE_OLD_L2				(50)
#define MCC_S16_FADEIN_DEL_STROKE_OLD_L1				(20)
#define MCC_S16_FADEIN_DEL_STROKE_OLD_L0				(10)

#define MCC_S16_FADEIN_DEL_STROKE_DIFF_L3				(4)
#define MCC_S16_FADEIN_DEL_STROKE_DIFF_L2				(3)
#define MCC_S16_FADEIN_DEL_STROKE_DIFF_L1				(2)
#define MCC_S16_FADEIN_DEL_STROKE_DIFF_L0				(1)

#define U8_MCC_ESC_END_DCT_SCAN							(U8_T_100_MS)

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
	int16_t lmccs16MtrCtrlMode;
	int16_t lmccs16MtrCtrlModeOld;
	int16_t lmccs16DeltaTgtPosi5ms;
	int32_t lmccs32Posi;
	int8_t lmccs8PCtrlState;
	int8_t lmccs8PCtrlBoostMod;
	int8_t lmccs8PCtrlFadeoutState;

	int16_t lmccs16CalcIqRefBaseStateOld;
	int16_t lmccs16CalcIqRefBaseState;
	int16_t lmccs16CalcIqRefPidState;
	int16_t lmccs16CalcIqRefCompState;
	int16_t lmccs16CalcIqRefCompStateOld;
	int16_t lmccs16BaseChgCnt;

	int16_t lmccs16PidChgCnt;

	int16_t lmccs16WrpmMech;
	int16_t lmccs16CalcIqRefPidStateOld;
	int16_t lmccs16TargetPress;
	int16_t lmccs16MCpresFilt;
	int16_t lmccs16SecPressFilt;
	int16_t lmccs16VdcLink;

	int32_t lmccs32RLV1;
	int32_t lmccs32RLV2;

	uint8_t	lmccu8StrkRcvrCtrlState;
	int16_t lmccs16StkRecvryRequestedTime;
	int8_t lmccs8PChamberVolume;

}GetMotIRefCalcn_t;

typedef struct
{
	int16_t s16d;
	int16_t s16q;
} mcc_dq_t;

typedef struct{
	int8_t   s8PCtrlState;
	int8_t   s8PCtrlFadeoutState;
	int32_t  s32MtrTargetPosi;
	int16_t  s16DeltaStrokeTargetOld;
	int32_t  s32CurrentPosi;
	int16_t  s16PosiError;
	uint16_t u16MotorFadeoutEndOK;
	int16_t  s16VehicleSpeed;

} mcc_fo_delta_tgtstrk_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define MCC_1MSCTRL_START_SEC_CONST_UNSPECIFIED
#include "Mcc_MemMap.h"
/** Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/

#define MCC_1MSCTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Mcc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MCC_1MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mcc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
#define MCC_1MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mcc_MemMap.h"



#define MCC_1MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Mcc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define MCC_1MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Mcc_MemMap.h"



#define MCC_1MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Mcc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define MCC_1MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Mcc_MemMap.h"



#define MCC_1MSCTRL_START_SEC_VAR_32BIT
#include "Mcc_MemMap.h"
/** Variable Section (32BIT)**/
#define MCC_1MSCTRL_STOP_SEC_VAR_32BIT
#include "Mcc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MCC_1MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mcc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

FAL_STATIC uint8_t lmccu8StkRecvryStabnEndOK;
FAL_STATIC uint16_t lmccu16IdbSystemFailFlg;

int8_t lmccs8FluxWeakengFlgOld;
int8_t lmccs8FluxWeakengFlg;

FAL_STATIC int16_t lmccs16IdRef;
FAL_STATIC int16_t lmccs16IqRef;
FAL_STATIC int16_t lmccs16IqRefBase;
FAL_STATIC int16_t lmccs16IqRefBaseFw;
FAL_STATIC int16_t lmccs16IqRefBaseBw;
FAL_STATIC int16_t lmccs16IqRefPid;
FAL_STATIC int16_t lmccs16IqRefComp;
FAL_STATIC int16_t lmccs16IqRefCompCnt;
FAL_STATIC int16_t lmccs16IqRefCompCnt2;
FAL_STATIC int16_t lmccs16IqRefCompCntMax;
FAL_STATIC int16_t lmccs16IqRefCompCnt2Max;
FAL_STATIC int16_t lmccs16BetaAngle;
FAL_STATIC int16_t lmccs16BetaAngle12V;
FAL_STATIC int16_t lmccs16BetaAngle9V;
FAL_STATIC int16_t lmccs16TarPress4Base;
FAL_STATIC int16_t lmccs16StrkRcvr2StateCnt;

int16_t lmccs8PCtrlBoostModOld;

FAL_STATIC Frac16 lmccf16SinBetaAngle;
FAL_STATIC Frac16 lmccf16CosBetaAngle;

FAL_STATIC int32_t lmccs32TgtPosi;
FAL_STATIC int32_t lmccs16WrpmMechRef;

FAL_STATIC GetMotIRefCalcn_t GetMotIRefCalcn;

mcc_pid_t lmccstPidSpdNormal;
mcc_pid_t lmccstPidPresNormal;
mcc_pid_t lmccstPidPosiNormal;
mcc_pid_t lmccstPidPosiAbsMux;
mcc_pid_t lmccstPidAct;
mcc_pid_t lmccstPidActTmp;
mcc_pid_t lmccstPidNa;
mcc_pid_t lmccstPidSpdFadeOut;
mcc_pid_t lmccstPidSpdActive;
mcc_pid_t lmccstPidSpdSafty;
mcc_pid_t lmccstPidSpdNormal;
mcc_pid_t lmccstPidSpdCreep;
mcc_pid_t lmccstPidSpdStop;

mcc_dq_t lmccstIref;

uint8_t lmccu8InVlvAllCloseReq;
uint8_t lmccu8InVlvAllCloseReqOld;
uint8_t	lmccu8NeedMtrSpdDown;
uint8_t	lmccu8NeedMtrSpdDownOld;

int8_t	lmccs8TarDeltaUopdatedFlg;
int8_t	lmccs8Act1MaxCntMon;
int8_t	lmccs8Act1MaxCntMon;
int8_t	lmccs8Act2CntMon;
int8_t	lmccs8Act2MaxCntMon;

uint16_t lamccu16MotorFadeoutEndOK;

int16_t	lmccs16DeltaTgtPosi;
int16_t	lmatrs16RpmRefTemp;
int16_t	lmccs16DeltaTgtPosi5ms5msOld;
int16_t	lmccs16TarDeltaStkCnt;
int16_t lamccs16FadeOutTgtDStroke;
int16_t lamccs16FadeOutTgtDStrokeEsc;
int16_t lmccs16Mon0;
int16_t lmccs16Mon1;
int16_t lmccs16Mon2;

int32_t	lmccs32TgtPosiOld;

mcc_fo_delta_tgtstrk_t	lmccstFadeoutDTgtStrk;

#define MCC_1MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mcc_MemMap.h"



#define MCC_1MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Mcc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define MCC_1MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Mcc_MemMap.h"



#define MCC_1MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Mcc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define MCC_1MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Mcc_MemMap.h"



#define MCC_1MSCTRL_START_SEC_VAR_32BIT
#include "Mcc_MemMap.h"
/** Variable Section (32BIT)**/
#define MCC_1MSCTRL_STOP_SEC_VAR_32BIT
#include "Mcc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define MCC_1MSCTRL_START_SEC_CODE
#include "Mcc_MemMap.h"

static void Mcc_vCalcTgtStroke(void);
static void Mcc_vCalcTgtPres(void);

static void Mcc_vCalcIqRefBase(void);
static void Mcc_vCalcIdqRef(void);
static void Mcc_vCalcIqRefPid(void);
static void Mcc_vCalcIqRefComp(void);
static void Mcc_vCalcIdqRefFinal(void);
static int16_t Mcc_s16ChangePid(sint16 s16PidStatOld, mcc_pid_t *pPidStat, int16_t s16XrefStat, int16_t s16XStat);
static void Mcc_FWeakenigCtrl(mcc_dq_t* pStIref, int16_t s16IqRef, int16_t s16Wrpm);
static int32_t Mcc_s32Abs(int32_t x);
static int16_t Mcc_s16Sign(int32_t s32Xin);

static void Mcc_vInpIfMotIRefCalcn(void);
static void Mcc_vOutpIfMotIRefCalcn(void);

static void Mcc_vCalcDeltaStroke(void);
static void Mcc_vCalcSpeedRef(void);
static void Mcc_vCallInterface1ms(void);

static int16_t Mcc_s16MtrPosiFadeOut2(int32_t s32MtrTargetPosi, int16_t s16DeltaStrokeTargetOld, int32_t s32CurrentPosi, int16_t s16PosiError, int16_t s16VehicleSpeed);
static int16_t Mcc_s16MtrPosiFadeIn(int32_t s32MtrTargetPosi, int16_t s16DeltaStrokeTargetOld, int32_t s32CurrentPosi, int16_t s16PosiError);
static int16_t Mcc_s16FadeoutDelStrokeEsc(int8_t s8PCtrlState, int8_t s8PCtrlBoostMode, int8_t s8PCtrlFadeoutState, int32_t s32MtrTargetPosi, int16_t s16DeltaStrokeTargetOld);
static int16_t Mcc_s16GetFadeoutDeltaTgtStrk(mcc_fo_delta_tgtstrk_t lmccstFadeoutDTgtStrk);
static uint16_t Mcc_u16DctMtrFadeoutEnd(int8_t s8PCtrlFadeoutState, uint16_t u16MotorFadeoutEndOK, int32_t s32CurrentPosi, int16_t s16DeltaStrokeTarget);
static int16_t Mcc_s16CalcFadeoutDTgtStrk(int32_t s32MtrTargetPosi, int16_t s16DeltaStrokeTargetOld, int32_t s32CurrentPosi, int16_t s16PosiError);
static uint16_t Mcc_u16MtrEndOkReset(int8_t s8PCtrlState, uint16_t u16MotorFadeoutEndOK);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

void Mcc_vCallMotIRefCalcn(void)
{
	/*1msec loop*/
	Mcc_vInpIfMotIRefCalcn();
	Mcc_vCallInterface1ms();
	Mcc_vCalcDeltaStroke();
	Mcc_vCalcTgtStroke();
	Mcc_vCalcTgtPres();
	Mcc_vCalcSpeedRef();
	Mcc_vCalcIdqRef();
	Mcc_vOutpIfMotIRefCalcn();
}

void Mcc_vInitMotIRefCalcn(void)
{
	lmccstPidSpdNormal.s16Kp 			= 3;
	lmccstPidSpdNormal.f16Kp 			= 0;
	lmccstPidSpdNormal.s16KiTs 		= 0;
	lmccstPidSpdNormal.f16KiTs 		= MCC_FRAC16(0.005);
	lmccstPidSpdNormal.s16KdFs			= 5;
	lmccstPidSpdNormal.f16KdFs			= 0;
	lmccstPidSpdNormal.s16UpMax 		= MCC_S16_MAX;
	lmccstPidSpdNormal.s16UpMin 		= MCC_S16_MIN;
	lmccstPidSpdNormal.s16UiMax 		= MCC_S16_MAX;
	lmccstPidSpdNormal.s16UiMin 		= MCC_S16_MIN;
	lmccstPidSpdNormal.s16UdMax 		= MCC_S16_MAX;
	lmccstPidSpdNormal.s16UdMin 		= MCC_S16_MIN;
	lmccstPidSpdNormal.s16UoutMax		= MCC_S16_IQREF_MAX;
	lmccstPidSpdNormal.s16UoutMin		= MCC_S16_IQREF_MIN;
	lmccstPidSpdNormal.s8GainId		= 1;

	lmccstPidPresNormal.s16Kp 			= 3;
	lmccstPidPresNormal.f16Kp 			= 0;
	lmccstPidPresNormal.s16KiTs 		= 0;
	lmccstPidPresNormal.s16KdFs		= 0;
	lmccstPidPresNormal.f16KdFs		= 0;
	lmccstPidPresNormal.f16KiTs 		= MCC_FRAC16(0.005);
	lmccstPidPresNormal.s16UpMax 		= MCC_S16_MAX;
	lmccstPidPresNormal.s16UpMin 		= MCC_S16_MIN;
	lmccstPidPresNormal.s16UiMax 		= MCC_S16_MAX;
	lmccstPidPresNormal.s16UiMin 		= MCC_S16_MIN;
	lmccstPidPresNormal.s16UdMax 		= MCC_S16_MAX;
	lmccstPidPresNormal.s16UdMin 		= MCC_S16_MIN;
	lmccstPidPresNormal.s16UoutMax		= MCC_S16_IQREF_MAX;
	lmccstPidPresNormal.s16UoutMin		= MCC_S16_IQREF_MIN;
	lmccstPidPresNormal.s8GainId		= 11;

	lmccstPidPosiNormal.s16Kp 			= 5; // 40;
	lmccstPidPosiNormal.f16Kp 			= 0; // 25000;
	lmccstPidPosiNormal.s16KiTs 		= 0; // 0;
	lmccstPidPosiNormal.f16KiTs 		= MCC_FRAC16(0.005); // 1;
	lmccstPidPosiNormal.s16KdFs		= 5; // 170;
	lmccstPidPosiNormal.f16KdFs		= 0; // 0;
	lmccstPidPosiNormal.s16UpMax 		= MCC_S16_MAX;
	lmccstPidPosiNormal.s16UpMin 		= MCC_S16_MIN;
	lmccstPidPosiNormal.s16UiMax 		= MCC_S16_MAX;
	lmccstPidPosiNormal.s16UiMin 		= MCC_S16_MIN;
	lmccstPidPosiNormal.s16UdMax 		= MCC_S16_MAX;
	lmccstPidPosiNormal.s16UdMin 		= MCC_S16_MIN;
	lmccstPidPosiNormal.s16UoutMax		= MCC_S16_IQREF_MAX;
	lmccstPidPosiNormal.s16UoutMin		= MCC_S16_IQREF_MIN;
	lmccstPidPosiNormal.s8GainId		= 12;

	lmccstPidPosiAbsMux 				= lmccstPidPosiNormal;
	lmccstPidPosiAbsMux.s8GainId		= 13;

	lmccstPidSpdFadeOut.s16Kp 		= 7;
	lmccstPidSpdFadeOut.f16Kp 		= 0;
	lmccstPidSpdFadeOut.s16KiTs 		= 0;
	lmccstPidSpdFadeOut.f16KiTs 		= 0;
	lmccstPidSpdFadeOut.s16KdFs		= 0;
	lmccstPidSpdFadeOut.f16KdFs		= 0;
	lmccstPidSpdFadeOut.s16UpMax 		= MCC_S16_MAX;
	lmccstPidSpdFadeOut.s16UpMin 		= MCC_S16_MIN;
	lmccstPidSpdFadeOut.s16UiMax 		= MCC_S16_MAX;
	lmccstPidSpdFadeOut.s16UiMin 		= MCC_S16_MIN;
	lmccstPidSpdFadeOut.s16UdMax 		= MCC_S16_MAX;
	lmccstPidSpdFadeOut.s16UdMin 		= MCC_S16_MIN;
	lmccstPidSpdFadeOut.s16UoutMax	= MCC_S16_IQREF_MAX;
	lmccstPidSpdFadeOut.s16UoutMin	= MCC_S16_IQREF_MIN;
	lmccstPidSpdFadeOut.s8GainId		= 2;

	lmccstPidSpdActive 				= lmccstPidSpdNormal;
	lmccstPidSpdActive.s8GainId		= 3;

	lmccstPidSpdSafty  				= lmccstPidSpdNormal;
	lmccstPidSpdSafty.s8GainId		= 4;

	lmccstPidSpdNormal.s8GainId		= 5;

	lmccstPidSpdCreep  				= lmccstPidSpdNormal;
	lmccstPidSpdCreep.s8GainId		= 6;

	lmccstPidSpdStop 	 				= lmccstPidSpdNormal;
	lmccstPidSpdStop.s8GainId			= 7;
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static void Mcc_vInpIfMotIRefCalcn(void)
{
	GetMotIRefCalcn.lmccs8PCtrlFadeoutState		= (int8_t)Mcc_1msCtrlBus.Mcc_1msCtrlPCtrlFadeoutSt;
	GetMotIRefCalcn.lmccu8StrkRcvrCtrlState		= (uint8_t)Mcc_1msCtrlBus.Mcc_1msCtrlStkRecvryActnIfInfo.StkRecvryCtrlState;
	GetMotIRefCalcn.lmccs16StkRecvryRequestedTime = (sint16)Mcc_1msCtrlBus.Mcc_1msCtrlStkRecvryActnIfInfo.StkRecvryRequestedTime;
	GetMotIRefCalcn.lmccs8PCtrlState				= (int8_t)Mcc_1msCtrlBus.Mcc_1msCtrlPCtrlSt;
	GetMotIRefCalcn.lmccs8PChamberVolume 			= (int8_t)Mcc_1msCtrlBus.Mcc_1msCtrlPChamberVolume;
	lmccs8PCtrlBoostModOld = GetMotIRefCalcn.lmccs8PCtrlBoostMod;
	GetMotIRefCalcn.lmccs8PCtrlBoostMod 			= (int8_t)MTRCurCTRLsrcbus.lmccu8PCtrlBoostMod;
	GetMotIRefCalcn.lmccs16WrpmMech				= (int16_t)Mcc_1msCtrlBus.Mcc_1msCtrlMotRotgAgSigInfo.MotMechAngleSpdFild;
	GetMotIRefCalcn.lmccs16MCpresFilt				= (int16_t)Mcc_1msCtrlBus.Mcc_1msCtrlCircPFild1msInfo.PrimCircPFild1ms/10;
	GetMotIRefCalcn.lmccs16SecPressFilt			= (int16_t)Mcc_1msCtrlBus.Mcc_1msCtrlCircPFild1msInfo.SecdCircPFild1ms/10;
	lmccs16DeltaTgtPosi5ms5msOld 						= (int16_t)GetMotIRefCalcn.lmccs16DeltaTgtPosi5ms;
	GetMotIRefCalcn.lmccs16DeltaTgtPosi5ms 			= (int16_t)Mcc_1msCtrlBus.Mcc_1msCtrlTarDeltaStk;
	GetMotIRefCalcn.lmccs16TargetPress				= (int16_t)Mcc_1msCtrlBus.Mcc_1msCtrlFinalTarPFromPCtrl/10;
	GetMotIRefCalcn.lmccs32Posi					= (int32_t)Mcc_1msCtrlBus.Mcc_1msCtrlMotRotgAgSigInfo.StkPosnMeasd;

	GetMotIRefCalcn.lmccs16MtrCtrlModeOld			= MTRCurCTRLsrcbus.lmccs16MtrCtrlModeOld;
	GetMotIRefCalcn.lmccs16MtrCtrlMode				= MTRCurCTRLsrcbus.lmccs16MtrCtrlMode;
	GetMotIRefCalcn.lmccs16CalcIqRefBaseState		= MTRCurCTRLsrcbus.lmccs16CalcIqRefBaseState;
	GetMotIRefCalcn.lmccs16CalcIqRefPidStateOld	= MTRCurCTRLsrcbus.lmccs16CalcIqRefPidStateOld;
	GetMotIRefCalcn.lmccs16CalcIqRefPidState		= MTRCurCTRLsrcbus.lmccs16CalcIqRefPidState;
	GetMotIRefCalcn.lmccs16PidChgCnt				= MTRCurCTRLsrcbus.lmccs16PidChgCnt;
	GetMotIRefCalcn.lmccs16CalcIqRefCompStateOld	= MTRCurCTRLsrcbus.lmccs16CalcIqRefCompStateOld;
	GetMotIRefCalcn.lmccs16CalcIqRefCompState		= MTRCurCTRLsrcbus.lmccs16CalcIqRefCompState;
	GetMotIRefCalcn.lmccs16BaseChgCnt				= MTRCurCTRLsrcbus.lmccs16BaseChgCnt;

}

static void Mcc_vOutpIfMotIRefCalcn(void)
{
	Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn			= lmccs32TgtPosi;
	Mcc_1msCtrlBus.Mcc_1msCtrlStkRecvryStabnEndOK				= lmccu8StkRecvryStabnEndOK;
	Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo.IdRef 			= lmccs16IdRef;
	Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo.IqRef 			= lmccs16IqRef;
	Mcc_1msCtrlBus.Mcc_1msCtrlMotICtrlFadeOutState				= lamccu16MotorFadeoutEndOK;
	Mcc_1msCtrlBus.Mcc_1msCtrlFluxWeakengStInfo.FluxWeakengFlg		= lmccs8FluxWeakengFlg;
	Mcc_1msCtrlBus.Mcc_1msCtrlFluxWeakengStInfo.FluxWeakengFlgOld	= lmccs8FluxWeakengFlgOld;
	MTRCurCTRLsrcbus.lmccu16IdbSystemFailFlg					= lmccu16IdbSystemFailFlg;
}
static void Mcc_vCalcIdqRef(void)
{
	Mcc_vCalcIqRefBase();

	Mcc_vCalcIqRefPid();

	Mcc_vCalcIqRefComp();

	Mcc_vCalcIdqRefFinal();
}

static void Mcc_vCallInterface1ms(void)
{
	if ((lmccs8PCtrlBoostModOld != MCC_S16_PID_POSI_ABS_MUX) && (GetMotIRefCalcn.lmccs8PCtrlBoostMod ==MCC_S16_PID_POSI_ABS_MUX))
	{
		lmccs16DeltaTgtPosi = GetMotIRefCalcn.lmccs16DeltaTgtPosi5ms;
	}
	else if ((lmccs8PCtrlBoostModOld == MCC_S16_PID_POSI_ABS_MUX) && (GetMotIRefCalcn.lmccs8PCtrlBoostMod ==MCC_S16_PID_POSI_ABS_MUX))
	{
		lmccs16DeltaTgtPosi = 0;
	}
	else
	{
		lmccs16DeltaTgtPosi = GetMotIRefCalcn.lmccs16DeltaTgtPosi5ms/5;
	}
}

static void Mcc_vCalcDeltaStroke(void)
{
	if ( (GetMotIRefCalcn.lmccs8PCtrlState == S8_PRESS_FADE_OUT)
			&& ((GetMotIRefCalcn.lmccs8PCtrlFadeoutState == S8_PCTRL_FADEOUT_STATE_2) || (GetMotIRefCalcn.lmccs8PCtrlFadeoutState == S8_PCTRL_FADEOUT_STATE_3)) )
	{
		lmccstFadeoutDTgtStrk.s8PCtrlState 				= GetMotIRefCalcn.lmccs8PCtrlState;
		lmccstFadeoutDTgtStrk.s8PCtrlFadeoutState 		= GetMotIRefCalcn.lmccs8PCtrlFadeoutState;
		lmccstFadeoutDTgtStrk.s32MtrTargetPosi			= lmccs32TgtPosi;
		lmccstFadeoutDTgtStrk.s16DeltaStrokeTargetOld	= lmccs16DeltaTgtPosi5ms5msOld;
		lmccstFadeoutDTgtStrk.s32CurrentPosi			= GetMotIRefCalcn.lmccs32Posi;
		lmccstFadeoutDTgtStrk.s16PosiError				= lmccs32TgtPosi - GetMotIRefCalcn.lmccs32Posi;
		lmccstFadeoutDTgtStrk.u16MotorFadeoutEndOK		= lamccu16MotorFadeoutEndOK;
		lmccstFadeoutDTgtStrk.s16VehicleSpeed			= 480;	/* 60kph */

		lamccs16FadeOutTgtDStroke = Mcc_s16GetFadeoutDeltaTgtStrk(lmccstFadeoutDTgtStrk);

		/*********************************************************************************************************/
		/* Detect Motor Fadeout End OK																		Begin*/
		/*********************************************************************************************************/
		lamccu16MotorFadeoutEndOK = Mcc_u16DctMtrFadeoutEnd(GetMotIRefCalcn.lmccs8PCtrlFadeoutState, lamccu16MotorFadeoutEndOK, GetMotIRefCalcn.lmccs32Posi, GetMotIRefCalcn.lmccs16DeltaTgtPosi5ms);
		lamccu16MotorFadeoutEndOK = Mcc_u16MtrEndOkReset(GetMotIRefCalcn.lmccs8PCtrlState, lamccu16MotorFadeoutEndOK);
		/*********************************************************************************************************/
		/* Detect Motor Fadeout End OK																		End  */
		/*********************************************************************************************************/

		/*********************************************************************************************************/
		/* Motor Fadeout State																				End  */
		/*********************************************************************************************************/

		/*********************************************************************************************************/
		/* Motor Fadeout State for ESC																		Begin*/
		/*********************************************************************************************************/
		lamccs16FadeOutTgtDStrokeEsc = Mcc_s16FadeoutDelStrokeEsc(GetMotIRefCalcn.lmccs8PCtrlState, GetMotIRefCalcn.lmccs8PCtrlBoostMod, GetMotIRefCalcn.lmccs8PCtrlFadeoutState, lmccs32TgtPosi, lmccs16DeltaTgtPosi5ms5msOld);
		/*********************************************************************************************************/
		/* Motor Fadeout State for ESC																		End  */
		/*********************************************************************************************************/

		if (lamccs16FadeOutTgtDStrokeEsc != 0)
		{
			lmccs16DeltaTgtPosi = lamccs16FadeOutTgtDStrokeEsc;
		}
		else if (lamccs16FadeOutTgtDStroke != 0)
		{
			lmccs16DeltaTgtPosi = lamccs16FadeOutTgtDStroke;
		}
		else
		{
			lmccs16DeltaTgtPosi = 0;
		}
	}

	if ( (GetMotIRefCalcn.lmccs8PCtrlState == S8_PRESS_FADE_OUT)
			&& ((GetMotIRefCalcn.lmccs8PCtrlFadeoutState == S8_PCTRL_FADEOUT_STATE_1) || (GetMotIRefCalcn.lmccs8PCtrlFadeoutState == S8_PCTRL_FADEOUT_STATE_2)) )
	{
		if ( (lmccs32TgtPosi <= MCC_S32_TGT_POSI_MIN) && (lmccs16DeltaTgtPosi < 0) )
		{
			lmccs16DeltaTgtPosi = 0;
		}
	}
}

int16_t Mcc_s16GetFadeoutDeltaTgtStrk(mcc_fo_delta_tgtstrk_t lmccstFadeoutDTgtStrk)
{
	int16_t  s16DeltaStrokeTarget = 0;

	if ( (lmccstFadeoutDTgtStrk.s8PCtrlState == S8_PRESS_FADE_OUT)
			&& ((lmccstFadeoutDTgtStrk.s8PCtrlFadeoutState == S8_PCTRL_FADEOUT_STATE_2) || (lmccstFadeoutDTgtStrk.s8PCtrlFadeoutState == S8_PCTRL_FADEOUT_STATE_3)) )
	{
		if (lmccstFadeoutDTgtStrk.s8PCtrlFadeoutState == S8_PCTRL_FADEOUT_STATE_2)
		{
			s16DeltaStrokeTarget = Mcc_s16CalcFadeoutDTgtStrk(lmccstFadeoutDTgtStrk.s32MtrTargetPosi, lmccstFadeoutDTgtStrk.s16DeltaStrokeTargetOld, lmccstFadeoutDTgtStrk.s32CurrentPosi, lmccstFadeoutDTgtStrk.s16PosiError);

			/* Stop the motor condition should be changed for RPM control concept */
			if ((lmccstFadeoutDTgtStrk.s32MtrTargetPosi <= MCC_S32_TGT_POSI_MIN) && (lmccstFadeoutDTgtStrk.s32CurrentPosi < 0))
			{
				s16DeltaStrokeTarget = 0;
			}
		}
		else
		{
			if (lmccstFadeoutDTgtStrk.u16MotorFadeoutEndOK == FALSE)
			{
				if (lmccstFadeoutDTgtStrk.s32MtrTargetPosi >= MCC_S32_ORIGIN_SET_MAX_THR)
				{
					s16DeltaStrokeTarget = Mcc_s16MtrPosiFadeOut2(lmccstFadeoutDTgtStrk.s32MtrTargetPosi, lmccstFadeoutDTgtStrk.s16DeltaStrokeTargetOld, lmccstFadeoutDTgtStrk.s32CurrentPosi, lmccstFadeoutDTgtStrk.s16PosiError, lmccstFadeoutDTgtStrk.s16VehicleSpeed);
				}
				else if (lmccstFadeoutDTgtStrk.s32MtrTargetPosi >= S32_MTRPOSI_0MM)
				{
					s16DeltaStrokeTarget = 0;
				}
				else
				{
					s16DeltaStrokeTarget = Mcc_s16MtrPosiFadeIn(lmccstFadeoutDTgtStrk.s32MtrTargetPosi, lmccstFadeoutDTgtStrk.s16DeltaStrokeTargetOld, lmccstFadeoutDTgtStrk.s32CurrentPosi, lmccstFadeoutDTgtStrk.s16PosiError);
				}
			}
		}
	}

	return s16DeltaStrokeTarget;
}

uint16_t Mcc_u16DctMtrFadeoutEnd(int8_t s8PCtrlFadeoutState, uint16_t u16MotorFadeoutEndOK, int32_t s32CurrentPosi, int16_t s16DeltaStrokeTarget)
{
	if ((s8PCtrlFadeoutState == S8_PCTRL_FADEOUT_STATE_3) && (u16MotorFadeoutEndOK == FALSE))
	{
		if ((s32CurrentPosi <= (MCC_S32_ORIGIN_SET_MAX_THR + MCC_S32_ORIGIN_SET_SENSOR_MARGIN)) && (s32CurrentPosi >= (MCC_S32_ORIGIN_SET_MIN_THR - MCC_S32_ORIGIN_SET_SENSOR_MARGIN)) && (s16DeltaStrokeTarget == 0))
		{
			u16MotorFadeoutEndOK = TRUE;
		}
	}

	return u16MotorFadeoutEndOK;
}

static uint16_t Mcc_u16MtrEndOkReset(int8_t s8PCtrlState, uint16_t u16MotorFadeoutEndOK)
{
	/* Compensate orifice effect - End */
	if ((s8PCtrlState == S8_PRESS_BOOST) || (s8PCtrlState == S8_PRESS_READY))
	{
		u16MotorFadeoutEndOK = FALSE;
	}
	else {}

	return u16MotorFadeoutEndOK;
}

int16_t LAMCC_s16GetCircuitPStableCnt(int16_t s16CircuitP, int16_t s16PStableCnt)
{
	if (s16CircuitP <= MCC_S16_FADEOUT_OK_THR)
	{
		s16PStableCnt = s16PStableCnt + 1;

		if (s16PStableCnt >= 5)
		{
			s16PStableCnt = 0;
		}
	}

	return s16PStableCnt;
}

int16_t Mcc_s16CalcFadeoutDTgtStrk(int32_t s32MtrTargetPosi, int16_t s16DeltaStrokeTargetOld, int32_t s32CurrentPosi, int16_t s16PosiError)
{
	int16_t s16DeltaStrokeTarget = 0;
	int16_t s16FadeoutDelStroke = 0;
	int16_t s16FadeoutDelStrokeIncLimit = 0;

	s16FadeoutDelStroke = (int16_t)L_s32IInter6Point(s32MtrTargetPosi, MCC_S16_FADEOUT_POSITION_L0, MCC_S16_FADEOUT_DEL_STROKE_L0,
			MCC_S16_FADEOUT_POSITION_L1, MCC_S16_FADEOUT_DEL_STROKE_L1,
			MCC_S16_FADEOUT_POSITION_L2, MCC_S16_FADEOUT_DEL_STROKE_L2,
			MCC_S16_FADEOUT_POSITION_L3, MCC_S16_FADEOUT_DEL_STROKE_L3,
			MCC_S16_FADEOUT_POSITION_L4, MCC_S16_FADEOUT_DEL_STROKE_L4,
			MCC_S16_FADEOUT_POSITION_L5, MCC_S16_FADEOUT_DEL_STROKE_L5);

	s16FadeoutDelStrokeIncLimit = (int16_t)L_s32IInter4Point(s16DeltaStrokeTargetOld, MCC_S16_FADEOUT_DEL_STROKE_OLD_L0, MCC_S16_FADEOUT_DEL_STROKE_DIFF_L0,
			MCC_S16_FADEOUT_DEL_STROKE_OLD_L1, MCC_S16_FADEOUT_DEL_STROKE_DIFF_L1,
			MCC_S16_FADEOUT_DEL_STROKE_OLD_L2, MCC_S16_FADEOUT_DEL_STROKE_DIFF_L2,
			MCC_S16_FADEOUT_DEL_STROKE_OLD_L3, MCC_S16_FADEOUT_DEL_STROKE_DIFF_L3);

	s16DeltaStrokeTarget = L_s16LimitDiff(s16FadeoutDelStroke, s16DeltaStrokeTargetOld, s16FadeoutDelStrokeIncLimit, 10);

	if ((s32MtrTargetPosi <= 0) && (s32CurrentPosi <= MCC_S32_ORIGIN_SET_MAX_THR))
	{
		if(s16DeltaStrokeTarget >= MCC_S16_FADEOUT_DEL_STROKE_L0)
		{
			s16DeltaStrokeTarget = 0;	/* END of Fade-out state 2 */
		}
	}
	else
	{
		if ( s16DeltaStrokeTarget >= 0)
		{
			s16DeltaStrokeTarget = -5;
		}
	}

	return s16DeltaStrokeTarget;
}

static int16_t Mcc_s16MtrPosiFadeOut2(int32_t s32MtrTargetPosi, int16_t s16DeltaStrokeTargetOld, int32_t s32CurrentPosi, int16_t s16PosiError, int16_t s16VehicleSpeed)
{
	int16_t s16DeltaStrokeTarget = 0;
	int16_t s16FadeoutDelStroke = 0;
	int16_t s16FadeoutDelStrokeIncLimit = 0;
	int16_t s16FadeOutDelStrokeMax = 0;

	if(s16VehicleSpeed > MCC_S16_FADEOUT_DEL_STROKE_MAX_SPD)
	{
		s16FadeOutDelStrokeMax = MCC_S16_FADEOUT_DEL_STROKE_MAX;
	}
	else
	{
		s16FadeOutDelStrokeMax = L_s16IInter2Point(s16VehicleSpeed,
				S16_SPEED_10_KPH, MCC_S16_FADEOUT_DEL_STROKE_L5,
				MCC_S16_FADEOUT_DEL_STROKE_MAX_SPD, MCC_S16_FADEOUT_DEL_STROKE_MAX);
	}

	s16FadeoutDelStroke = (int16_t)L_s32IInter6Point(s32MtrTargetPosi, MCC_S16_FADEOUT_POSITION_L0, MCC_S16_FADEOUT_DEL_STROKE_L0,
			MCC_S16_FADEOUT_POSITION_L1, MCC_S16_FADEOUT_DEL_STROKE_L1,
			MCC_S16_FADEOUT_POSITION_L2, MCC_S16_FADEOUT_DEL_STROKE_L2,
			MCC_S16_FADEOUT_POSITION_L3, MCC_S16_FADEOUT_DEL_STROKE_L3,
			MCC_S16_FADEOUT_POSITION_L4, s16FadeOutDelStrokeMax,
			MCC_S16_FADEOUT_POSITION_L5, s16FadeOutDelStrokeMax);

	s16FadeoutDelStrokeIncLimit = (int16_t)L_s32IInter4Point(s16DeltaStrokeTargetOld, MCC_S16_FADEOUT_DEL_STROKE_OLD_L0, MCC_S16_FADEOUT_DEL_STROKE_DIFF_L0,
			MCC_S16_FADEOUT_DEL_STROKE_OLD_L1, MCC_S16_FADEOUT_DEL_STROKE_DIFF_L1,
			MCC_S16_FADEOUT_DEL_STROKE_OLD_L2, MCC_S16_FADEOUT_DEL_STROKE_DIFF_L2,
			MCC_S16_FADEOUT_DEL_STROKE_OLD_L3, MCC_S16_FADEOUT_DEL_STROKE_DIFF_L3);

	s16DeltaStrokeTarget = L_s16LimitDiff(s16FadeoutDelStroke, s16DeltaStrokeTargetOld, s16FadeoutDelStrokeIncLimit, 10);

	if ((s32MtrTargetPosi <= 0) && (s32CurrentPosi <= MCC_S32_ORIGIN_SET_MAX_THR))
	{
		if(s16DeltaStrokeTarget >= MCC_S16_FADEOUT_DEL_STROKE_L0)
		{
			s16DeltaStrokeTarget = 0;	/* END of Fade-out state 2 */
		}
	}
	else
	{
		if ( s16DeltaStrokeTarget >= 0)
		{
			s16DeltaStrokeTarget = -5;
		}
	}

	return s16DeltaStrokeTarget;
}

static int16_t Mcc_s16MtrPosiFadeIn(int32_t s32MtrTargetPosi, int16_t s16DeltaStrokeTargetOld, int32_t s32CurrentPosi, int16_t s16PosiError)
{
	int16_t s16DeltaStrokeTarget = 0;
	int16_t s16FadeinDelStroke = 0;
	int16_t s16FadeinDelStrokeDecLimit = 0;

	s16FadeinDelStroke = MCC_S16_FADEIN_DEL_STROKE_L2;
	s16FadeinDelStrokeDecLimit = (int16_t)L_s32IInter4Point(s16DeltaStrokeTargetOld, MCC_S16_FADEIN_DEL_STROKE_OLD_L0, MCC_S16_FADEIN_DEL_STROKE_DIFF_L0,
			MCC_S16_FADEIN_DEL_STROKE_OLD_L1, MCC_S16_FADEIN_DEL_STROKE_DIFF_L1,
			MCC_S16_FADEIN_DEL_STROKE_OLD_L2, MCC_S16_FADEIN_DEL_STROKE_DIFF_L2,
			MCC_S16_FADEIN_DEL_STROKE_OLD_L3, MCC_S16_FADEIN_DEL_STROKE_DIFF_L3);

	s16DeltaStrokeTarget = L_s16LimitDiff(s16FadeinDelStroke, s16DeltaStrokeTargetOld, 10, s16FadeinDelStrokeDecLimit);

	if ( (s32MtrTargetPosi <= MCC_S32_ORIGIN_SET_MAX_THR) && (s32MtrTargetPosi >= -MCC_S32_ORIGIN_SET_MAX_THR))
	{
		s16DeltaStrokeTarget = 0;
	}

	s16DeltaStrokeTarget = L_s16LimitDiff(s16FadeinDelStroke, s16DeltaStrokeTargetOld, 10, s16FadeinDelStrokeDecLimit);

	return s16DeltaStrokeTarget;
}

static int16_t Mcc_s16FadeoutDelStrokeEsc(int8_t s8PCtrlState, int8_t s8PCtrlBoostMode, int8_t s8PCtrlFadeoutState, int32_t s32MtrTargetPosi, int16_t s16DeltaStrokeTargetOld)
{
	int16_t s16DeltaStrokeTarget = 0;
	static uint16_t u16ESCTriggeredFlg = FALSE;
	static uint16_t u16EscEndCnt = 0;

	/* ESC End Trigger Stage Detection - Begin */
	if (s8PCtrlState == S8_PRESS_BOOST)
	{
		if (s8PCtrlBoostMode == MCC_S8_BOOST_MODE_SAFETY)			/* ESC oversteer (Include ROP), ABS, TCS_1_mode : 300bar/s 		*/
		{
			u16ESCTriggeredFlg = TRUE;
			u16EscEndCnt = U8_MCC_ESC_END_DCT_SCAN - 1; /* Reset ESCEndCnt */
		}
		else
		{
			u16EscEndCnt = (u16EscEndCnt > 0) ? (u16EscEndCnt - 1) : 0;
		}
	}
	else
	{
		if (u16ESCTriggeredFlg == TRUE)
		{
			if ((s8PCtrlState == S8_PRESS_FADE_OUT) && ((s8PCtrlFadeoutState == S8_PCTRL_FADEOUT_STATE_1) || (s8PCtrlFadeoutState == S8_PCTRL_FADEOUT_STATE_2)))
			{
				if (u16EscEndCnt == 0)
				{
					u16ESCTriggeredFlg = FALSE;
				}
			}
			else
			{
				u16ESCTriggeredFlg = FALSE;
			}

		}

		if (u16ESCTriggeredFlg == TRUE)
		{
			if ((s8PCtrlState == S8_PRESS_FADE_OUT) && ((s8PCtrlFadeoutState == S8_PCTRL_FADEOUT_STATE_1) || (s8PCtrlFadeoutState == S8_PCTRL_FADEOUT_STATE_2)))
			{
				if (s32MtrTargetPosi > MCC_S16_FADEOUT_POSITION_L5)
				{
					if (s16DeltaStrokeTargetOld > 0)
					{
						s16DeltaStrokeTarget = L_s16LimitDiff( MCC_S16_FADEOUT_DEL_STROKE_AFTER_ESC, s16DeltaStrokeTargetOld, 100, 100);
					}
					else
					{
						s16DeltaStrokeTarget = L_s16LimitDiff(MCC_S16_FADEOUT_DEL_STROKE_AFTER_ESC, s16DeltaStrokeTargetOld, 100, 100);
					}
				}
				else { /* Maintain Original DeltaStrokeTarget for Fadeout */ }
			}
			else
			{
				u16ESCTriggeredFlg = FALSE;
			}
		}
	}

	return s16DeltaStrokeTarget;
}

static void Mcc_vCalcSpeedRef(void)
{
	static int16_t 	s16PosiErrModiFildOld;
	static int16_t	s16PosiErrModiFild;
	static int16_t	s16PosiErrModi;
	static int16_t	s16WrpmMechRefFildOld;
	static int16_t	s16WrpmMechRefFild;
	static int32_t  s32PosiErr;
	static int32_t  s32WrpmMechRefRaw;
	static int32_t  s32WrpmMechRef;
	static int16_t	Kspd, Kpos;

	s32WrpmMechRefRaw = MCC_S16_RPM_SCF * L_s16LimitMinMax(lmccs16DeltaTgtPosi, MCC_S16_DELTA_STRK_MIN, MCC_S16_DELTA_STRK_MAX);
	s32WrpmMechRefRaw = (s32WrpmMechRefRaw > 10000) ? 10000 : ((s32WrpmMechRefRaw < -10000) ? -10000 : s32WrpmMechRefRaw);
	s16WrpmMechRefFildOld = s16WrpmMechRefFild;
	s32PosiErr = lmccs32TgtPosi - GetMotIRefCalcn.lmccs32Posi;

	if (GetMotIRefCalcn.lmccs16CalcIqRefPidState == MCC_S16_PID_SPEED_FADEOUT1)
	{
		s16WrpmMechRefFild = L_s16Lpf1Int1ms((int16_t)s32WrpmMechRefRaw, s16WrpmMechRefFildOld, LPF_1_FIL_K_FC_10HZ_TS1MS);

		s32PosiErr = (s32PosiErr > 5000) ? 5000 : ((s32PosiErr < -5000) ? -5000 : s32PosiErr);
		if ( ((s32WrpmMechRefRaw > 0) && (s32PosiErr > 0)) || ((s32WrpmMechRefRaw < 0) && (s32PosiErr < 0)) )
		{
			s16PosiErrModi = (s32PosiErr*MCC_S16_RPM_SCF)/50;
		}
		else
		{
			s16PosiErrModi = (s32PosiErr*MCC_S16_RPM_SCF)/100;
		}
		s16PosiErrModiFildOld = s16PosiErrModiFild;
		s16PosiErrModiFild = L_s16Lpf1Int1ms(s16PosiErrModi, s16PosiErrModiFildOld, LPF_1_FIL_K_FC_10HZ_TS1MS);

		s32WrpmMechRef = (int32_t)s16WrpmMechRefFild;
		lmccs16WrpmMechRef = (int16_t)((s32WrpmMechRef > ASW_S16_MAX) ? ASW_S16_MAX : ((s32WrpmMechRef < ASW_S16_MIN) ?  ASW_S16_MIN : s32WrpmMechRef));
	}
	else if((GetMotIRefCalcn.lmccs16CalcIqRefPidState >= MCC_S16_PID_PRES_STAT_MIN)
			&& (GetMotIRefCalcn.lmccs16CalcIqRefPidState <= MCC_S16_PID_PRES_STAT_MAX ))
	{

		lmccs16WrpmMechRef = GetMotIRefCalcn.lmccs16WrpmMech;
	}
	else if((GetMotIRefCalcn.lmccs16CalcIqRefPidState >= MCC_S16_PID_POSI_STAT_MIN)
			&& (GetMotIRefCalcn.lmccs16CalcIqRefPidState <= MCC_S16_PID_POSI_STAT_MAX ))
	{
		lmccs16WrpmMechRef = GetMotIRefCalcn.lmccs16WrpmMech;
	}
	else
	{
		s16WrpmMechRefFild = L_s16Lpf1Int1ms((int16_t)s32WrpmMechRefRaw, s16WrpmMechRefFildOld, LPF_1_FIL_K_FC_10HZ_TS1MS);

		s32PosiErr = (s32PosiErr > 5000) ? 5000 : ((s32PosiErr < -5000) ? -5000 : s32PosiErr);
		if ( ((s32WrpmMechRefRaw > 0) && (s32PosiErr > 0)) || ((s32WrpmMechRefRaw < 0) && (s32PosiErr < 0)) )
		{
			s16PosiErrModi = (s32PosiErr*MCC_S16_RPM_SCF)/50;
		}
		else
		{
			s16PosiErrModi = (s32PosiErr*MCC_S16_RPM_SCF)/100;
		}
		s16PosiErrModiFildOld = s16PosiErrModiFild;
		s16PosiErrModiFild = L_s16Lpf1Int1ms(s16PosiErrModi, s16PosiErrModiFildOld, LPF_1_FIL_K_FC_10HZ_TS1MS);

		s32WrpmMechRef = (int32_t)s16WrpmMechRefFild + (int32_t)s16PosiErrModiFild;
		lmccs16WrpmMechRef = (int16_t)((s32WrpmMechRef > ASW_S16_MAX) ? ASW_S16_MAX : ((s32WrpmMechRef < ASW_S16_MIN) ?  ASW_S16_MIN : s32WrpmMechRef));
	}
}



static void Mcc_vCalcTgtStroke(void)
{
	lmccs32TgtPosiOld = lmccs32TgtPosi;

	if ((GetMotIRefCalcn.lmccs16CalcIqRefPidState == MCC_S16_PID_POSI_NORMAL)
			|| (GetMotIRefCalcn.lmccs16CalcIqRefPidState == MCC_S16_PID_SPEED_FADEOUT1)
			|| (GetMotIRefCalcn.lmccs16CalcIqRefPidState == MCC_S16_PID_SPEED_FADEOUT2)
			|| (GetMotIRefCalcn.lmccs16CalcIqRefPidState == MCC_S16_PID_SPEED_FADEOUT3)
			|| (GetMotIRefCalcn.lmccs16CalcIqRefPidState == MCC_S16_PID_SPEED_ACTIVE)
			|| (GetMotIRefCalcn.lmccs16CalcIqRefPidState == MCC_S16_PID_SPEED_SAFETY)
			|| (GetMotIRefCalcn.lmccs16CalcIqRefPidState == MCC_S16_PID_SPEED_NORMAL)
			|| (GetMotIRefCalcn.lmccs16CalcIqRefPidState == MCC_S16_PID_SPEED_CREEP)
			|| (GetMotIRefCalcn.lmccs16CalcIqRefPidState == MCC_S16_PID_SPEED_STOP)
			|| (GetMotIRefCalcn.lmccs16CalcIqRefPidState == MCC_S16_PID_POSI_ABS_MUX))
	{
		lmccs32TgtPosi = lmccs32TgtPosi + (int32_t)lmccs16DeltaTgtPosi;
	}
	else
	{
		lmccs32TgtPosi = GetMotIRefCalcn.lmccs32Posi;
	}

/*	if ((GetMotIRefCalcn.lmccs16CalcIqRefPidState == MCC_S16_PID_POSI_NORMAL)
			|| (GetMotIRefCalcn.lmccs16CalcIqRefPidState == MCC_S16_PID_SPEED_FADEOUT1)
			|| (GetMotIRefCalcn.lmccs16CalcIqRefPidState == MCC_S16_PID_SPEED_FADEOUT2)
			|| (GetMotIRefCalcn.lmccs16CalcIqRefPidState == MCC_S16_PID_SPEED_FADEOUT3)
			|| (GetMotIRefCalcn.lmccs16CalcIqRefPidState == MCC_S16_PID_SPEED_ACTIVE)
			|| (GetMotIRefCalcn.lmccs16CalcIqRefPidState == MCC_S16_PID_SPEED_SAFETY)
			|| (GetMotIRefCalcn.lmccs16CalcIqRefPidState == MCC_S16_PID_SPEED_NORMAL)
			|| (GetMotIRefCalcn.lmccs16CalcIqRefPidState == MCC_S16_PID_SPEED_CREEP)
			|| (GetMotIRefCalcn.lmccs16CalcIqRefPidState == MCC_S16_PID_SPEED_STOP))
	{
		lmccs32TgtPosi = lmccs32TgtPosi + (int32_t)lmccs16DeltaTgtPosi;
	}
	else if (GetMotIRefCalcn.lmccs16CalcIqRefPidState == MCC_S16_PID_POSI_ABS_MUX)
	{
		if(lmccs16DeltaTgtPosi5ms5msOld != GetMotIRefCalcn.lmccs16DeltaTgtPosi5ms)
		{
			lmccs8TarDeltaUopdatedFlg = 1;
			lmccs16TarDeltaStkCnt = 0;
			lmccs32TgtPosi = lmccs32TgtPosi + (int32_t)(GetMotIRefCalcn.lmccs16DeltaTgtPosi5ms);
		}
		else if( (lmccs16DeltaTgtPosi5ms5msOld ==  GetMotIRefCalcn.lmccs16DeltaTgtPosi5ms)
				&& (lmccs16TarDeltaStkCnt >= 6) )
		{
			lmccs8TarDeltaUopdatedFlg = 1;
			lmccs16TarDeltaStkCnt = 0;
			lmccs32TgtPosi = lmccs32TgtPosi + (int32_t)(GetMotIRefCalcn.lmccs16DeltaTgtPosi5ms);
		}
		else
		{
			lmccs8TarDeltaUopdatedFlg = 0;
			lmccs16TarDeltaStkCnt++;
		}
	}
	else
	{
		lmccs32TgtPosi = GetMotIRefCalcn.lmccs32Posi;
	}*/

	lmccs32TgtPosi = L_s32LimitMinMax1ms(lmccs32TgtPosi, MCC_S32_TGT_POSI_MIN, MCC_S32_TGT_POSI_MAX);
}

static void Mcc_vCalcTgtPres(void)
{
	lmccs16TarPress4Base = GetMotIRefCalcn.lmccs16TargetPress; /*need to check map resolution 0.1 bar */
}

static void Mcc_vCalcIqRefBase(void)
{
	static int16_t s16IqBase0, s16IqBase1;

	if (GetMotIRefCalcn.lmccs16BaseChgCnt == 0)
	{
		GetMotIRefCalcn.lmccs16CalcIqRefBaseStateOld = GetMotIRefCalcn.lmccs16CalcIqRefBaseState;
	}

	switch (GetMotIRefCalcn.lmccs16CalcIqRefBaseStateOld)
	{
	case MCC_S16_BASE_FW_LP:
		s16IqBase0 = L_s16IInter15Point1ms(lmccs16TarPress4Base,
				MCC_S16_TARGET_PRES_01 , MCC_S16_IQREFBASE_FW_01_LP,
				MCC_S16_TARGET_PRES_02 , MCC_S16_IQREFBASE_FW_02_LP,
				MCC_S16_TARGET_PRES_03 , MCC_S16_IQREFBASE_FW_03_LP,
				MCC_S16_TARGET_PRES_04 , MCC_S16_IQREFBASE_FW_04_LP,
				MCC_S16_TARGET_PRES_05 , MCC_S16_IQREFBASE_FW_05_LP,
				MCC_S16_TARGET_PRES_06 , MCC_S16_IQREFBASE_FW_06_LP,
				MCC_S16_TARGET_PRES_07 , MCC_S16_IQREFBASE_FW_07_LP,
				MCC_S16_TARGET_PRES_08 , MCC_S16_IQREFBASE_FW_08_LP,
				MCC_S16_TARGET_PRES_09 , MCC_S16_IQREFBASE_FW_09_LP,
				MCC_S16_TARGET_PRES_10 , MCC_S16_IQREFBASE_FW_10_LP,
				MCC_S16_TARGET_PRES_11 , MCC_S16_IQREFBASE_FW_11_LP,
				MCC_S16_TARGET_PRES_12 , MCC_S16_IQREFBASE_FW_12_LP,
				MCC_S16_TARGET_PRES_13 , MCC_S16_IQREFBASE_FW_13_LP,
				MCC_S16_TARGET_PRES_14 , MCC_S16_IQREFBASE_FW_14_LP,
				MCC_S16_TARGET_PRES_MAX, MCC_S16_IQREFBASE_FW_MAX_LP);
		break;

	case MCC_S16_BASE_BW_LP:
		s16IqBase0 = L_s16IInter15Point1ms(lmccs16TarPress4Base,
				MCC_S16_TARGET_PRES_01 , MCC_S16_IQREFBASE_FW_01_LP,
				MCC_S16_TARGET_PRES_02 , MCC_S16_IQREFBASE_FW_02_LP,
				MCC_S16_TARGET_PRES_03 , MCC_S16_IQREFBASE_FW_03_LP,
				MCC_S16_TARGET_PRES_04 , MCC_S16_IQREFBASE_FW_04_LP,
				MCC_S16_TARGET_PRES_05 , MCC_S16_IQREFBASE_FW_05_LP,
				MCC_S16_TARGET_PRES_06 , MCC_S16_IQREFBASE_FW_06_LP,
				MCC_S16_TARGET_PRES_07 , MCC_S16_IQREFBASE_FW_07_LP,
				MCC_S16_TARGET_PRES_08 , MCC_S16_IQREFBASE_FW_08_LP,
				MCC_S16_TARGET_PRES_09 , MCC_S16_IQREFBASE_FW_09_LP,
				MCC_S16_TARGET_PRES_10 , MCC_S16_IQREFBASE_FW_10_LP,
				MCC_S16_TARGET_PRES_11 , MCC_S16_IQREFBASE_FW_11_LP,
				MCC_S16_TARGET_PRES_12 , MCC_S16_IQREFBASE_FW_12_LP,
				MCC_S16_TARGET_PRES_13 , MCC_S16_IQREFBASE_FW_13_LP,
				MCC_S16_TARGET_PRES_14 , MCC_S16_IQREFBASE_FW_14_LP,
				MCC_S16_TARGET_PRES_MAX, MCC_S16_IQREFBASE_FW_MAX_LP);
		break;

	case MCC_S16_BASE_RS1_LP:
		s16IqBase0 = L_s16IInter15Point1ms(lmccs16TarPress4Base,
				MCC_S16_TARGET_PRES_01 , MCC_S16_IQREFBASE_FW_01_LP,
				MCC_S16_TARGET_PRES_02 , MCC_S16_IQREFBASE_FW_02_LP,
				MCC_S16_TARGET_PRES_03 , MCC_S16_IQREFBASE_FW_03_LP,
				MCC_S16_TARGET_PRES_04 , MCC_S16_IQREFBASE_FW_04_LP,
				MCC_S16_TARGET_PRES_05 , MCC_S16_IQREFBASE_FW_05_LP,
				MCC_S16_TARGET_PRES_06 , MCC_S16_IQREFBASE_FW_06_LP,
				MCC_S16_TARGET_PRES_07 , MCC_S16_IQREFBASE_FW_07_LP,
				MCC_S16_TARGET_PRES_08 , MCC_S16_IQREFBASE_FW_08_LP,
				MCC_S16_TARGET_PRES_09 , MCC_S16_IQREFBASE_FW_09_LP,
				MCC_S16_TARGET_PRES_10 , MCC_S16_IQREFBASE_FW_10_LP,
				MCC_S16_TARGET_PRES_11 , MCC_S16_IQREFBASE_FW_11_LP,
				MCC_S16_TARGET_PRES_12 , MCC_S16_IQREFBASE_FW_12_LP,
				MCC_S16_TARGET_PRES_13 , MCC_S16_IQREFBASE_FW_13_LP,
				MCC_S16_TARGET_PRES_14 , MCC_S16_IQREFBASE_FW_14_LP,
				MCC_S16_TARGET_PRES_MAX, MCC_S16_IQREFBASE_FW_MAX_LP);
		break;

	case MCC_S16_BASE_RS2_LP:
		lmccs16StrkRcvr2StateCnt = (lmccs16StrkRcvr2StateCnt < ASW_S16_MAX) ? lmccs16StrkRcvr2StateCnt + 1 : ASW_S16_MAX;
		if(lmccs16StrkRcvr2StateCnt < 25)
		{
			s16IqBase0 = -S16_MTRCURR_70A; /*need to check MACRO*/
		}
		else
		{
			s16IqBase0 = S16_MTRCURR_0A; /*need to check MACRO*/
		}
		break;

	case MCC_S16_BASE_STB1_LP:
		s16IqBase0 = S16_MTRCURR_60A;
		if(GetMotIRefCalcn.lmccs16WrpmMech > 0)
		{
			lmccu8StkRecvryStabnEndOK = TRUE;
		}
		else
		{
			lmccu8StkRecvryStabnEndOK = FALSE;
		}
		lmccs16StrkRcvr2StateCnt = 0;
		break;

	case MCC_S16_BASE_STB2_LP:
		s16IqBase0 = S16_MTRCURR_60A;
		if(GetMotIRefCalcn.lmccs16WrpmMech > 0)
		{
			lmccu8StkRecvryStabnEndOK = TRUE;
		}
		else
		{
			lmccu8StkRecvryStabnEndOK = FALSE;
		}
		lmccs16StrkRcvr2StateCnt = 0;
		break;

	case MCC_S16_BASE_FW_HP:
		s16IqBase0 = L_s16IInter15Point1ms(lmccs16TarPress4Base,
				MCC_S16_TARGET_PRES_01 , MCC_S16_IQREFBASE_FW_01_HP,
				MCC_S16_TARGET_PRES_02 , MCC_S16_IQREFBASE_FW_02_HP,
				MCC_S16_TARGET_PRES_03 , MCC_S16_IQREFBASE_FW_03_HP,
				MCC_S16_TARGET_PRES_04 , MCC_S16_IQREFBASE_FW_04_HP,
				MCC_S16_TARGET_PRES_05 , MCC_S16_IQREFBASE_FW_05_HP,
				MCC_S16_TARGET_PRES_06 , MCC_S16_IQREFBASE_FW_06_HP,
				MCC_S16_TARGET_PRES_07 , MCC_S16_IQREFBASE_FW_07_HP,
				MCC_S16_TARGET_PRES_08 , MCC_S16_IQREFBASE_FW_08_HP,
				MCC_S16_TARGET_PRES_09 , MCC_S16_IQREFBASE_FW_09_HP,
				MCC_S16_TARGET_PRES_10 , MCC_S16_IQREFBASE_FW_10_HP,
				MCC_S16_TARGET_PRES_11 , MCC_S16_IQREFBASE_FW_11_HP,
				MCC_S16_TARGET_PRES_12 , MCC_S16_IQREFBASE_FW_12_HP,
				MCC_S16_TARGET_PRES_13 , MCC_S16_IQREFBASE_FW_13_HP,
				MCC_S16_TARGET_PRES_14 , MCC_S16_IQREFBASE_FW_14_HP,
				MCC_S16_TARGET_PRES_MAX, MCC_S16_IQREFBASE_FW_MAX_HP);

		s16IqBase0 = (int16_t)((int32_t)(s16IqBase0 * 3/2));

		break;

	case MCC_S16_BASE_BW_HP:
		s16IqBase0 = L_s16IInter15Point1ms(lmccs16TarPress4Base,
				MCC_S16_TARGET_PRES_01 , MCC_S16_IQREFBASE_FW_01_HP,
				MCC_S16_TARGET_PRES_02 , MCC_S16_IQREFBASE_FW_02_HP,
				MCC_S16_TARGET_PRES_03 , MCC_S16_IQREFBASE_FW_03_HP,
				MCC_S16_TARGET_PRES_04 , MCC_S16_IQREFBASE_FW_04_HP,
				MCC_S16_TARGET_PRES_05 , MCC_S16_IQREFBASE_FW_05_HP,
				MCC_S16_TARGET_PRES_06 , MCC_S16_IQREFBASE_FW_06_HP,
				MCC_S16_TARGET_PRES_07 , MCC_S16_IQREFBASE_FW_07_HP,
				MCC_S16_TARGET_PRES_08 , MCC_S16_IQREFBASE_FW_08_HP,
				MCC_S16_TARGET_PRES_09 , MCC_S16_IQREFBASE_FW_09_HP,
				MCC_S16_TARGET_PRES_10 , MCC_S16_IQREFBASE_FW_10_HP,
				MCC_S16_TARGET_PRES_11 , MCC_S16_IQREFBASE_FW_11_HP,
				MCC_S16_TARGET_PRES_12 , MCC_S16_IQREFBASE_FW_12_HP,
				MCC_S16_TARGET_PRES_13 , MCC_S16_IQREFBASE_FW_13_HP,
				MCC_S16_TARGET_PRES_14 , MCC_S16_IQREFBASE_FW_14_HP,
				MCC_S16_TARGET_PRES_MAX, MCC_S16_IQREFBASE_FW_MAX_HP);

		s16IqBase0 = (int16_t)((int32_t)(s16IqBase0 * 3/2));

		break;

	case MCC_S16_BASE_RS1_HP:
		s16IqBase0 = L_s16IInter15Point1ms(lmccs16TarPress4Base,
				MCC_S16_TARGET_PRES_01 , MCC_S16_IQREFBASE_FW_01_HP,
				MCC_S16_TARGET_PRES_02 , MCC_S16_IQREFBASE_FW_02_HP,
				MCC_S16_TARGET_PRES_03 , MCC_S16_IQREFBASE_FW_03_HP,
				MCC_S16_TARGET_PRES_04 , MCC_S16_IQREFBASE_FW_04_HP,
				MCC_S16_TARGET_PRES_05 , MCC_S16_IQREFBASE_FW_05_HP,
				MCC_S16_TARGET_PRES_06 , MCC_S16_IQREFBASE_FW_06_HP,
				MCC_S16_TARGET_PRES_07 , MCC_S16_IQREFBASE_FW_07_HP,
				MCC_S16_TARGET_PRES_08 , MCC_S16_IQREFBASE_FW_08_HP,
				MCC_S16_TARGET_PRES_09 , MCC_S16_IQREFBASE_FW_09_HP,
				MCC_S16_TARGET_PRES_10 , MCC_S16_IQREFBASE_FW_10_HP,
				MCC_S16_TARGET_PRES_11 , MCC_S16_IQREFBASE_FW_11_HP,
				MCC_S16_TARGET_PRES_12 , MCC_S16_IQREFBASE_FW_12_HP,
				MCC_S16_TARGET_PRES_13 , MCC_S16_IQREFBASE_FW_13_HP,
				MCC_S16_TARGET_PRES_14 , MCC_S16_IQREFBASE_FW_14_HP,
				MCC_S16_TARGET_PRES_MAX, MCC_S16_IQREFBASE_FW_MAX_HP);

		s16IqBase0 = (int16_t)((int32_t)(s16IqBase0 * 3/2));

		break;

	case MCC_S16_BASE_RS2_HP:
		lmccs16StrkRcvr2StateCnt = (lmccs16StrkRcvr2StateCnt < ASW_S16_MAX) ? lmccs16StrkRcvr2StateCnt + 1 : ASW_S16_MAX;
		if(lmccs16StrkRcvr2StateCnt < 25)
		{
			s16IqBase0 = -S16_MTRCURR_70A; /*need to check MACRO*/
		}
		else
		{
			s16IqBase0 = S16_MTRCURR_0A; /*need to check MACRO*/
		}
		break;

	case MCC_S16_BASE_STB1_HP:
		s16IqBase0 = S16_MTRCURR_60A;
		if(GetMotIRefCalcn.lmccs16WrpmMech > 0)
		{
			lmccu8StkRecvryStabnEndOK = TRUE;
		}
		else
		{
			lmccu8StkRecvryStabnEndOK = FALSE;
		}
		lmccs16StrkRcvr2StateCnt = 0;
		break;

	case MCC_S16_BASE_STB2_HP:
		s16IqBase0 = S16_MTRCURR_60A;
		if(GetMotIRefCalcn.lmccs16WrpmMech > 0)
		{
			lmccu8StkRecvryStabnEndOK = TRUE;
		}
		else
		{
			lmccu8StkRecvryStabnEndOK = FALSE;
		}
		lmccs16StrkRcvr2StateCnt = 0;
		break;

	case MCC_S16_BASE_FW_BW:
		s16IqBase0 = - L_s16IInter15Point1ms(lmccs16TarPress4Base,
				MCC_S16_TARGET_PRES_01 , MCC_S16_IQREFBASE_FW_01_BW,
				MCC_S16_TARGET_PRES_02 , MCC_S16_IQREFBASE_FW_02_BW,
				MCC_S16_TARGET_PRES_03 , MCC_S16_IQREFBASE_FW_03_BW,
				MCC_S16_TARGET_PRES_04 , MCC_S16_IQREFBASE_FW_04_BW,
				MCC_S16_TARGET_PRES_05 , MCC_S16_IQREFBASE_FW_05_BW,
				MCC_S16_TARGET_PRES_06 , MCC_S16_IQREFBASE_FW_06_BW,
				MCC_S16_TARGET_PRES_07 , MCC_S16_IQREFBASE_FW_07_BW,
				MCC_S16_TARGET_PRES_08 , MCC_S16_IQREFBASE_FW_08_BW,
				MCC_S16_TARGET_PRES_09 , MCC_S16_IQREFBASE_FW_09_BW,
				MCC_S16_TARGET_PRES_10 , MCC_S16_IQREFBASE_FW_10_BW,
				MCC_S16_TARGET_PRES_11 , MCC_S16_IQREFBASE_FW_11_BW,
				MCC_S16_TARGET_PRES_12 , MCC_S16_IQREFBASE_FW_12_BW,
				MCC_S16_TARGET_PRES_13 , MCC_S16_IQREFBASE_FW_13_BW,
				MCC_S16_TARGET_PRES_14 , MCC_S16_IQREFBASE_FW_14_BW,
				MCC_S16_TARGET_PRES_MAX, MCC_S16_IQREFBASE_FW_MAX_BW);
		break;

	case MCC_S16_BASE_BW_BW:
		s16IqBase0 = - L_s16IInter15Point1ms(lmccs16TarPress4Base,
				MCC_S16_TARGET_PRES_01 , MCC_S16_IQREFBASE_FW_01_BW,
				MCC_S16_TARGET_PRES_02 , MCC_S16_IQREFBASE_FW_02_BW,
				MCC_S16_TARGET_PRES_03 , MCC_S16_IQREFBASE_FW_03_BW,
				MCC_S16_TARGET_PRES_04 , MCC_S16_IQREFBASE_FW_04_BW,
				MCC_S16_TARGET_PRES_05 , MCC_S16_IQREFBASE_FW_05_BW,
				MCC_S16_TARGET_PRES_06 , MCC_S16_IQREFBASE_FW_06_BW,
				MCC_S16_TARGET_PRES_07 , MCC_S16_IQREFBASE_FW_07_BW,
				MCC_S16_TARGET_PRES_08 , MCC_S16_IQREFBASE_FW_08_BW,
				MCC_S16_TARGET_PRES_09 , MCC_S16_IQREFBASE_FW_09_BW,
				MCC_S16_TARGET_PRES_10 , MCC_S16_IQREFBASE_FW_10_BW,
				MCC_S16_TARGET_PRES_11 , MCC_S16_IQREFBASE_FW_11_BW,
				MCC_S16_TARGET_PRES_12 , MCC_S16_IQREFBASE_FW_12_BW,
				MCC_S16_TARGET_PRES_13 , MCC_S16_IQREFBASE_FW_13_BW,
				MCC_S16_TARGET_PRES_14 , MCC_S16_IQREFBASE_FW_14_BW,
				MCC_S16_TARGET_PRES_MAX, MCC_S16_IQREFBASE_FW_MAX_BW);
		break;

	case MCC_S16_BASE_RS1_BW:
		s16IqBase0 = - L_s16IInter15Point1ms(lmccs16TarPress4Base,
				MCC_S16_TARGET_PRES_01 , MCC_S16_IQREFBASE_FW_01_BW,
				MCC_S16_TARGET_PRES_02 , MCC_S16_IQREFBASE_FW_02_BW,
				MCC_S16_TARGET_PRES_03 , MCC_S16_IQREFBASE_FW_03_BW,
				MCC_S16_TARGET_PRES_04 , MCC_S16_IQREFBASE_FW_04_BW,
				MCC_S16_TARGET_PRES_05 , MCC_S16_IQREFBASE_FW_05_BW,
				MCC_S16_TARGET_PRES_06 , MCC_S16_IQREFBASE_FW_06_BW,
				MCC_S16_TARGET_PRES_07 , MCC_S16_IQREFBASE_FW_07_BW,
				MCC_S16_TARGET_PRES_08 , MCC_S16_IQREFBASE_FW_08_BW,
				MCC_S16_TARGET_PRES_09 , MCC_S16_IQREFBASE_FW_09_BW,
				MCC_S16_TARGET_PRES_10 , MCC_S16_IQREFBASE_FW_10_BW,
				MCC_S16_TARGET_PRES_11 , MCC_S16_IQREFBASE_FW_11_BW,
				MCC_S16_TARGET_PRES_12 , MCC_S16_IQREFBASE_FW_12_BW,
				MCC_S16_TARGET_PRES_13 , MCC_S16_IQREFBASE_FW_13_BW,
				MCC_S16_TARGET_PRES_14 , MCC_S16_IQREFBASE_FW_14_BW,
				MCC_S16_TARGET_PRES_MAX, MCC_S16_IQREFBASE_FW_MAX_BW);
		break;

	case MCC_S16_BASE_RS2_BW:
		lmccs16StrkRcvr2StateCnt = (lmccs16StrkRcvr2StateCnt < ASW_S16_MAX) ? lmccs16StrkRcvr2StateCnt + 1 : ASW_S16_MAX;
		if(lmccs16StrkRcvr2StateCnt < 25)
		{
			s16IqBase0 = -S16_MTRCURR_70A; /*need to check MACRO*/
		}
		else
		{
			s16IqBase0 = S16_MTRCURR_0A; /*need to check MACRO*/
		}
		break;

	case MCC_S16_BASE_STB1_BW:
		s16IqBase0 = S16_MTRCURR_60A;
		if(GetMotIRefCalcn.lmccs16WrpmMech > 0)
		{
			lmccu8StkRecvryStabnEndOK = TRUE;
		}
		else
		{
			lmccu8StkRecvryStabnEndOK = FALSE;
		}
		lmccs16StrkRcvr2StateCnt = 0;
		break;

	case MCC_S16_BASE_STB2_BW:
		s16IqBase0 = S16_MTRCURR_60A;
		if(GetMotIRefCalcn.lmccs16WrpmMech > 0)
		{
			lmccu8StkRecvryStabnEndOK = TRUE;
		}
		else
		{
			lmccu8StkRecvryStabnEndOK = FALSE;
		}
		lmccs16StrkRcvr2StateCnt = 0;
		break;

	case MCC_S16_BASE_NA:
		s16IqBase0 = 0;
		break;

	default:
		s16IqBase0 = 0;
		break;
	}

	switch (GetMotIRefCalcn.lmccs16CalcIqRefBaseState)
	{
	case MCC_S16_BASE_FW_LP:
		s16IqBase1 = L_s16IInter15Point1ms(lmccs16TarPress4Base,
				MCC_S16_TARGET_PRES_01 , MCC_S16_IQREFBASE_FW_01_LP,
				MCC_S16_TARGET_PRES_02 , MCC_S16_IQREFBASE_FW_02_LP,
				MCC_S16_TARGET_PRES_03 , MCC_S16_IQREFBASE_FW_03_LP,
				MCC_S16_TARGET_PRES_04 , MCC_S16_IQREFBASE_FW_04_LP,
				MCC_S16_TARGET_PRES_05 , MCC_S16_IQREFBASE_FW_05_LP,
				MCC_S16_TARGET_PRES_06 , MCC_S16_IQREFBASE_FW_06_LP,
				MCC_S16_TARGET_PRES_07 , MCC_S16_IQREFBASE_FW_07_LP,
				MCC_S16_TARGET_PRES_08 , MCC_S16_IQREFBASE_FW_08_LP,
				MCC_S16_TARGET_PRES_09 , MCC_S16_IQREFBASE_FW_09_LP,
				MCC_S16_TARGET_PRES_10 , MCC_S16_IQREFBASE_FW_10_LP,
				MCC_S16_TARGET_PRES_11 , MCC_S16_IQREFBASE_FW_11_LP,
				MCC_S16_TARGET_PRES_12 , MCC_S16_IQREFBASE_FW_12_LP,
				MCC_S16_TARGET_PRES_13 , MCC_S16_IQREFBASE_FW_13_LP,
				MCC_S16_TARGET_PRES_14 , MCC_S16_IQREFBASE_FW_14_LP,
				MCC_S16_TARGET_PRES_MAX, MCC_S16_IQREFBASE_FW_MAX_LP);
		break;

	case MCC_S16_BASE_BW_LP:
		s16IqBase1 = L_s16IInter15Point1ms(lmccs16TarPress4Base,
				MCC_S16_TARGET_PRES_01 , MCC_S16_IQREFBASE_FW_01_LP,
				MCC_S16_TARGET_PRES_02 , MCC_S16_IQREFBASE_FW_02_LP,
				MCC_S16_TARGET_PRES_03 , MCC_S16_IQREFBASE_FW_03_LP,
				MCC_S16_TARGET_PRES_04 , MCC_S16_IQREFBASE_FW_04_LP,
				MCC_S16_TARGET_PRES_05 , MCC_S16_IQREFBASE_FW_05_LP,
				MCC_S16_TARGET_PRES_06 , MCC_S16_IQREFBASE_FW_06_LP,
				MCC_S16_TARGET_PRES_07 , MCC_S16_IQREFBASE_FW_07_LP,
				MCC_S16_TARGET_PRES_08 , MCC_S16_IQREFBASE_FW_08_LP,
				MCC_S16_TARGET_PRES_09 , MCC_S16_IQREFBASE_FW_09_LP,
				MCC_S16_TARGET_PRES_10 , MCC_S16_IQREFBASE_FW_10_LP,
				MCC_S16_TARGET_PRES_11 , MCC_S16_IQREFBASE_FW_11_LP,
				MCC_S16_TARGET_PRES_12 , MCC_S16_IQREFBASE_FW_12_LP,
				MCC_S16_TARGET_PRES_13 , MCC_S16_IQREFBASE_FW_13_LP,
				MCC_S16_TARGET_PRES_14 , MCC_S16_IQREFBASE_FW_14_LP,
				MCC_S16_TARGET_PRES_MAX, MCC_S16_IQREFBASE_FW_MAX_LP);
		break;

	case MCC_S16_BASE_RS1_LP:
		s16IqBase1 = L_s16IInter15Point1ms(lmccs16TarPress4Base,
				MCC_S16_TARGET_PRES_01 , MCC_S16_IQREFBASE_FW_01_LP,
				MCC_S16_TARGET_PRES_02 , MCC_S16_IQREFBASE_FW_02_LP,
				MCC_S16_TARGET_PRES_03 , MCC_S16_IQREFBASE_FW_03_LP,
				MCC_S16_TARGET_PRES_04 , MCC_S16_IQREFBASE_FW_04_LP,
				MCC_S16_TARGET_PRES_05 , MCC_S16_IQREFBASE_FW_05_LP,
				MCC_S16_TARGET_PRES_06 , MCC_S16_IQREFBASE_FW_06_LP,
				MCC_S16_TARGET_PRES_07 , MCC_S16_IQREFBASE_FW_07_LP,
				MCC_S16_TARGET_PRES_08 , MCC_S16_IQREFBASE_FW_08_LP,
				MCC_S16_TARGET_PRES_09 , MCC_S16_IQREFBASE_FW_09_LP,
				MCC_S16_TARGET_PRES_10 , MCC_S16_IQREFBASE_FW_10_LP,
				MCC_S16_TARGET_PRES_11 , MCC_S16_IQREFBASE_FW_11_LP,
				MCC_S16_TARGET_PRES_12 , MCC_S16_IQREFBASE_FW_12_LP,
				MCC_S16_TARGET_PRES_13 , MCC_S16_IQREFBASE_FW_13_LP,
				MCC_S16_TARGET_PRES_14 , MCC_S16_IQREFBASE_FW_14_LP,
				MCC_S16_TARGET_PRES_MAX, MCC_S16_IQREFBASE_FW_MAX_LP);
		break;

	case MCC_S16_BASE_RS2_LP:

		lmccs16StrkRcvr2StateCnt = (lmccs16StrkRcvr2StateCnt < ASW_S16_MAX) ? lmccs16StrkRcvr2StateCnt + 1 : ASW_S16_MAX;
		if(lmccs16StrkRcvr2StateCnt < 25)
		{
			s16IqBase1 = -S16_MTRCURR_70A; /*need to check MACRO*/
		}
		else
		{
			s16IqBase1 = S16_MTRCURR_0A; /*need to check MACRO*/
		}
		break;

	case MCC_S16_BASE_STB1_LP:
		s16IqBase1 = S16_MTRCURR_60A;
		if(GetMotIRefCalcn.lmccs16WrpmMech > 0)
		{
			lmccu8StkRecvryStabnEndOK = TRUE;
		}
		else
		{
			lmccu8StkRecvryStabnEndOK = FALSE;
		}
		lmccs16StrkRcvr2StateCnt = 0;
		break;

	case MCC_S16_BASE_STB2_LP:
		s16IqBase1 = S16_MTRCURR_60A;
		if(GetMotIRefCalcn.lmccs16WrpmMech > 0)
		{
			lmccu8StkRecvryStabnEndOK = TRUE;
		}
		else
		{
			lmccu8StkRecvryStabnEndOK = FALSE;
		}
		lmccs16StrkRcvr2StateCnt = 0;
		break;

	case MCC_S16_BASE_FW_HP:
		s16IqBase1 = L_s16IInter15Point1ms(lmccs16TarPress4Base,
				MCC_S16_TARGET_PRES_01 , MCC_S16_IQREFBASE_FW_01_HP,
				MCC_S16_TARGET_PRES_02 , MCC_S16_IQREFBASE_FW_02_HP,
				MCC_S16_TARGET_PRES_03 , MCC_S16_IQREFBASE_FW_03_HP,
				MCC_S16_TARGET_PRES_04 , MCC_S16_IQREFBASE_FW_04_HP,
				MCC_S16_TARGET_PRES_05 , MCC_S16_IQREFBASE_FW_05_HP,
				MCC_S16_TARGET_PRES_06 , MCC_S16_IQREFBASE_FW_06_HP,
				MCC_S16_TARGET_PRES_07 , MCC_S16_IQREFBASE_FW_07_HP,
				MCC_S16_TARGET_PRES_08 , MCC_S16_IQREFBASE_FW_08_HP,
				MCC_S16_TARGET_PRES_09 , MCC_S16_IQREFBASE_FW_09_HP,
				MCC_S16_TARGET_PRES_10 , MCC_S16_IQREFBASE_FW_10_HP,
				MCC_S16_TARGET_PRES_11 , MCC_S16_IQREFBASE_FW_11_HP,
				MCC_S16_TARGET_PRES_12 , MCC_S16_IQREFBASE_FW_12_HP,
				MCC_S16_TARGET_PRES_13 , MCC_S16_IQREFBASE_FW_13_HP,
				MCC_S16_TARGET_PRES_14 , MCC_S16_IQREFBASE_FW_14_HP,
				MCC_S16_TARGET_PRES_MAX, MCC_S16_IQREFBASE_FW_MAX_HP);

		s16IqBase1 = (int16_t)((int32_t)(s16IqBase1 * 3/2));

		break;

	case MCC_S16_BASE_BW_HP:
		s16IqBase1 = L_s16IInter15Point1ms(lmccs16TarPress4Base,
				MCC_S16_TARGET_PRES_01 , MCC_S16_IQREFBASE_FW_01_HP,
				MCC_S16_TARGET_PRES_02 , MCC_S16_IQREFBASE_FW_02_HP,
				MCC_S16_TARGET_PRES_03 , MCC_S16_IQREFBASE_FW_03_HP,
				MCC_S16_TARGET_PRES_04 , MCC_S16_IQREFBASE_FW_04_HP,
				MCC_S16_TARGET_PRES_05 , MCC_S16_IQREFBASE_FW_05_HP,
				MCC_S16_TARGET_PRES_06 , MCC_S16_IQREFBASE_FW_06_HP,
				MCC_S16_TARGET_PRES_07 , MCC_S16_IQREFBASE_FW_07_HP,
				MCC_S16_TARGET_PRES_08 , MCC_S16_IQREFBASE_FW_08_HP,
				MCC_S16_TARGET_PRES_09 , MCC_S16_IQREFBASE_FW_09_HP,
				MCC_S16_TARGET_PRES_10 , MCC_S16_IQREFBASE_FW_10_HP,
				MCC_S16_TARGET_PRES_11 , MCC_S16_IQREFBASE_FW_11_HP,
				MCC_S16_TARGET_PRES_12 , MCC_S16_IQREFBASE_FW_12_HP,
				MCC_S16_TARGET_PRES_13 , MCC_S16_IQREFBASE_FW_13_HP,
				MCC_S16_TARGET_PRES_14 , MCC_S16_IQREFBASE_FW_14_HP,
				MCC_S16_TARGET_PRES_MAX, MCC_S16_IQREFBASE_FW_MAX_HP);

		s16IqBase1 = (int16_t)((int32_t)(s16IqBase1 * 3/2));

		break;

	case MCC_S16_BASE_RS1_HP:
		s16IqBase1 = L_s16IInter15Point1ms(lmccs16TarPress4Base,
				MCC_S16_TARGET_PRES_01 , MCC_S16_IQREFBASE_FW_01_HP,
				MCC_S16_TARGET_PRES_02 , MCC_S16_IQREFBASE_FW_02_HP,
				MCC_S16_TARGET_PRES_03 , MCC_S16_IQREFBASE_FW_03_HP,
				MCC_S16_TARGET_PRES_04 , MCC_S16_IQREFBASE_FW_04_HP,
				MCC_S16_TARGET_PRES_05 , MCC_S16_IQREFBASE_FW_05_HP,
				MCC_S16_TARGET_PRES_06 , MCC_S16_IQREFBASE_FW_06_HP,
				MCC_S16_TARGET_PRES_07 , MCC_S16_IQREFBASE_FW_07_HP,
				MCC_S16_TARGET_PRES_08 , MCC_S16_IQREFBASE_FW_08_HP,
				MCC_S16_TARGET_PRES_09 , MCC_S16_IQREFBASE_FW_09_HP,
				MCC_S16_TARGET_PRES_10 , MCC_S16_IQREFBASE_FW_10_HP,
				MCC_S16_TARGET_PRES_11 , MCC_S16_IQREFBASE_FW_11_HP,
				MCC_S16_TARGET_PRES_12 , MCC_S16_IQREFBASE_FW_12_HP,
				MCC_S16_TARGET_PRES_13 , MCC_S16_IQREFBASE_FW_13_HP,
				MCC_S16_TARGET_PRES_14 , MCC_S16_IQREFBASE_FW_14_HP,
				MCC_S16_TARGET_PRES_MAX, MCC_S16_IQREFBASE_FW_MAX_HP);

		s16IqBase1 = (int16_t)((int32_t)(s16IqBase1 * 3/2));

		break;

	case MCC_S16_BASE_RS2_HP:
		lmccs16StrkRcvr2StateCnt = (lmccs16StrkRcvr2StateCnt < ASW_S16_MAX) ? lmccs16StrkRcvr2StateCnt + 1 : ASW_S16_MAX;
		if(lmccs16StrkRcvr2StateCnt < 25)
		{
			s16IqBase1 = -S16_MTRCURR_70A; /*need to check MACRO*/
		}
		else
		{
			s16IqBase1 = S16_MTRCURR_0A; /*need to check MACRO*/
		}
		break;

	case MCC_S16_BASE_STB1_HP:
		s16IqBase1 = S16_MTRCURR_60A;
		if(GetMotIRefCalcn.lmccs16WrpmMech > 0)
		{
			lmccu8StkRecvryStabnEndOK = TRUE;
		}
		else
		{
			lmccu8StkRecvryStabnEndOK = FALSE;
		}
		lmccs16StrkRcvr2StateCnt = 0;
		break;

	case MCC_S16_BASE_STB2_HP:
		s16IqBase1 = S16_MTRCURR_60A;
		if(GetMotIRefCalcn.lmccs16WrpmMech > 0)
		{
			lmccu8StkRecvryStabnEndOK = TRUE;
		}
		else
		{
			lmccu8StkRecvryStabnEndOK = FALSE;
		}
		lmccs16StrkRcvr2StateCnt = 0;
		break;

	case MCC_S16_BASE_FW_BW:
		s16IqBase1 = - L_s16IInter15Point1ms(lmccs16TarPress4Base,
				MCC_S16_TARGET_PRES_01 , MCC_S16_IQREFBASE_FW_01_BW,
				MCC_S16_TARGET_PRES_02 , MCC_S16_IQREFBASE_FW_02_BW,
				MCC_S16_TARGET_PRES_03 , MCC_S16_IQREFBASE_FW_03_BW,
				MCC_S16_TARGET_PRES_04 , MCC_S16_IQREFBASE_FW_04_BW,
				MCC_S16_TARGET_PRES_05 , MCC_S16_IQREFBASE_FW_05_BW,
				MCC_S16_TARGET_PRES_06 , MCC_S16_IQREFBASE_FW_06_BW,
				MCC_S16_TARGET_PRES_07 , MCC_S16_IQREFBASE_FW_07_BW,
				MCC_S16_TARGET_PRES_08 , MCC_S16_IQREFBASE_FW_08_BW,
				MCC_S16_TARGET_PRES_09 , MCC_S16_IQREFBASE_FW_09_BW,
				MCC_S16_TARGET_PRES_10 , MCC_S16_IQREFBASE_FW_10_BW,
				MCC_S16_TARGET_PRES_11 , MCC_S16_IQREFBASE_FW_11_BW,
				MCC_S16_TARGET_PRES_12 , MCC_S16_IQREFBASE_FW_12_BW,
				MCC_S16_TARGET_PRES_13 , MCC_S16_IQREFBASE_FW_13_BW,
				MCC_S16_TARGET_PRES_14 , MCC_S16_IQREFBASE_FW_14_BW,
				MCC_S16_TARGET_PRES_MAX, MCC_S16_IQREFBASE_FW_MAX_BW);
		break;

	case MCC_S16_BASE_BW_BW:
		s16IqBase1 = - L_s16IInter15Point1ms(lmccs16TarPress4Base,
				MCC_S16_TARGET_PRES_01 , MCC_S16_IQREFBASE_FW_01_BW,
				MCC_S16_TARGET_PRES_02 , MCC_S16_IQREFBASE_FW_02_BW,
				MCC_S16_TARGET_PRES_03 , MCC_S16_IQREFBASE_FW_03_BW,
				MCC_S16_TARGET_PRES_04 , MCC_S16_IQREFBASE_FW_04_BW,
				MCC_S16_TARGET_PRES_05 , MCC_S16_IQREFBASE_FW_05_BW,
				MCC_S16_TARGET_PRES_06 , MCC_S16_IQREFBASE_FW_06_BW,
				MCC_S16_TARGET_PRES_07 , MCC_S16_IQREFBASE_FW_07_BW,
				MCC_S16_TARGET_PRES_08 , MCC_S16_IQREFBASE_FW_08_BW,
				MCC_S16_TARGET_PRES_09 , MCC_S16_IQREFBASE_FW_09_BW,
				MCC_S16_TARGET_PRES_10 , MCC_S16_IQREFBASE_FW_10_BW,
				MCC_S16_TARGET_PRES_11 , MCC_S16_IQREFBASE_FW_11_BW,
				MCC_S16_TARGET_PRES_12 , MCC_S16_IQREFBASE_FW_12_BW,
				MCC_S16_TARGET_PRES_13 , MCC_S16_IQREFBASE_FW_13_BW,
				MCC_S16_TARGET_PRES_14 , MCC_S16_IQREFBASE_FW_14_BW,
				MCC_S16_TARGET_PRES_MAX, MCC_S16_IQREFBASE_FW_MAX_BW);
		break;

	case MCC_S16_BASE_RS1_BW:
		s16IqBase1 = - L_s16IInter15Point1ms(lmccs16TarPress4Base,
				MCC_S16_TARGET_PRES_01 , MCC_S16_IQREFBASE_FW_01_BW,
				MCC_S16_TARGET_PRES_02 , MCC_S16_IQREFBASE_FW_02_BW,
				MCC_S16_TARGET_PRES_03 , MCC_S16_IQREFBASE_FW_03_BW,
				MCC_S16_TARGET_PRES_04 , MCC_S16_IQREFBASE_FW_04_BW,
				MCC_S16_TARGET_PRES_05 , MCC_S16_IQREFBASE_FW_05_BW,
				MCC_S16_TARGET_PRES_06 , MCC_S16_IQREFBASE_FW_06_BW,
				MCC_S16_TARGET_PRES_07 , MCC_S16_IQREFBASE_FW_07_BW,
				MCC_S16_TARGET_PRES_08 , MCC_S16_IQREFBASE_FW_08_BW,
				MCC_S16_TARGET_PRES_09 , MCC_S16_IQREFBASE_FW_09_BW,
				MCC_S16_TARGET_PRES_10 , MCC_S16_IQREFBASE_FW_10_BW,
				MCC_S16_TARGET_PRES_11 , MCC_S16_IQREFBASE_FW_11_BW,
				MCC_S16_TARGET_PRES_12 , MCC_S16_IQREFBASE_FW_12_BW,
				MCC_S16_TARGET_PRES_13 , MCC_S16_IQREFBASE_FW_13_BW,
				MCC_S16_TARGET_PRES_14 , MCC_S16_IQREFBASE_FW_14_BW,
				MCC_S16_TARGET_PRES_MAX, MCC_S16_IQREFBASE_FW_MAX_BW);
		break;

	case MCC_S16_BASE_RS2_BW:
		lmccs16StrkRcvr2StateCnt = (lmccs16StrkRcvr2StateCnt < ASW_S16_MAX) ? lmccs16StrkRcvr2StateCnt + 1 : ASW_S16_MAX;
		if(lmccs16StrkRcvr2StateCnt < 25)
		{
			s16IqBase1 = -S16_MTRCURR_70A; /*need to check MACRO*/
		}
		else
		{
			s16IqBase1 = S16_MTRCURR_0A; /*need to check MACRO*/
		}
		break;

	case MCC_S16_BASE_STB1_BW:
		s16IqBase1 = S16_MTRCURR_60A;
		if(GetMotIRefCalcn.lmccs16WrpmMech > 0)
		{
			lmccu8StkRecvryStabnEndOK = TRUE;
		}
		else
		{
			lmccu8StkRecvryStabnEndOK = FALSE;
		}
		lmccs16StrkRcvr2StateCnt = 0;
		break;

	case MCC_S16_BASE_STB2_BW:
		s16IqBase1 = S16_MTRCURR_60A;
		if(GetMotIRefCalcn.lmccs16WrpmMech > 0)
		{
			lmccu8StkRecvryStabnEndOK = TRUE;
		}
		else
		{
			lmccu8StkRecvryStabnEndOK = FALSE;
		}
		lmccs16StrkRcvr2StateCnt = 0;
		break;

	case MCC_S16_BASE_NA:
		s16IqBase1 = 0;
		break;

	default:
		s16IqBase1 = 0;
		break;
	}

	if ((GetMotIRefCalcn.lmccs16BaseChgCnt > 0) && (GetMotIRefCalcn.lmccs16BaseChgCnt <= MCC_S16_BASE_CHG_CNT_MAX))
	{
		lmccs16IqRefBase = ((MCC_S16_BASE_CHG_CNT_MAX - GetMotIRefCalcn.lmccs16BaseChgCnt)*s16IqBase0 + GetMotIRefCalcn.lmccs16BaseChgCnt*s16IqBase1)/MCC_S16_BASE_CHG_CNT_MAX;
	}
	else
	{
		lmccs16IqRefBase = s16IqBase1;
	}
}

static void Mcc_vCalcIqRefPid(void)
{
	static int16_t s16IqRef0, s16IqRef1;

	switch (GetMotIRefCalcn.lmccs16CalcIqRefPidState)
	{
	case MCC_S16_PID_SPEED_FADEOUT1:
		if (GetMotIRefCalcn.lmccs16PidChgCnt == 0)
		{
			lmccs16IqRefPid = Mcc_s16PIDController(lmccs16WrpmMechRef, GetMotIRefCalcn.lmccs16WrpmMech, &lmccstPidAct);
		}
		else
		{
			lmccs16IqRefPid = Mcc_s16ChangePid(GetMotIRefCalcn.lmccs16CalcIqRefPidStateOld,
					&lmccstPidSpdFadeOut, lmccs16WrpmMechRef, GetMotIRefCalcn.lmccs16WrpmMech);
		}
		break;

	case MCC_S16_PID_SPEED_FADEOUT2:
		if (GetMotIRefCalcn.lmccs16PidChgCnt == 0)
		{
			lmccs16IqRefPid = Mcc_s16PIDController(lmccs16WrpmMechRef, GetMotIRefCalcn.lmccs16WrpmMech, &lmccstPidAct);
		}
		else
		{
			lmccs16IqRefPid = Mcc_s16ChangePid(GetMotIRefCalcn.lmccs16CalcIqRefPidStateOld,
					&lmccstPidSpdFadeOut, lmccs16WrpmMechRef, GetMotIRefCalcn.lmccs16WrpmMech);
		}
		break;

	case MCC_S16_PID_SPEED_FADEOUT3:
		if (GetMotIRefCalcn.lmccs16PidChgCnt == 0)
		{
			lmccs16IqRefPid = Mcc_s16PIDController(lmccs16WrpmMechRef, GetMotIRefCalcn.lmccs16WrpmMech, &lmccstPidAct);
		}
		else
		{
			lmccs16IqRefPid = Mcc_s16ChangePid(GetMotIRefCalcn.lmccs16CalcIqRefPidStateOld,
					&lmccstPidSpdFadeOut, lmccs16WrpmMechRef, GetMotIRefCalcn.lmccs16WrpmMech);
		}
		break;

	case MCC_S16_PID_SPEED_ACTIVE:
		if (GetMotIRefCalcn.lmccs16PidChgCnt == 0)
		{
			lmccs16IqRefPid = Mcc_s16PIDController(lmccs16WrpmMechRef, GetMotIRefCalcn.lmccs16WrpmMech, &lmccstPidAct);
		}
		else
		{
			lmccs16IqRefPid = Mcc_s16ChangePid(GetMotIRefCalcn.lmccs16CalcIqRefPidStateOld,
					&lmccstPidSpdActive, lmccs16WrpmMechRef, GetMotIRefCalcn.lmccs16WrpmMech);
		}
		break;

	case MCC_S16_PID_SPEED_SAFETY:
		if (GetMotIRefCalcn.lmccs16PidChgCnt == 0)
		{
			lmccs16IqRefPid = Mcc_s16PIDController(lmccs16WrpmMechRef, GetMotIRefCalcn.lmccs16WrpmMech, &lmccstPidAct);
		}
		else
		{
			lmccs16IqRefPid = Mcc_s16ChangePid(GetMotIRefCalcn.lmccs16CalcIqRefPidStateOld,
					&lmccstPidSpdSafty, lmccs16WrpmMechRef, GetMotIRefCalcn.lmccs16WrpmMech);
		}
		break;

	case MCC_S16_PID_SPEED_NORMAL:
		if (GetMotIRefCalcn.lmccs16PidChgCnt == 0)
		{
			lmccs16IqRefPid = Mcc_s16PIDController(lmccs16WrpmMechRef, GetMotIRefCalcn.lmccs16WrpmMech, &lmccstPidAct);
		}
		else
		{
			lmccs16IqRefPid = Mcc_s16ChangePid(GetMotIRefCalcn.lmccs16CalcIqRefPidStateOld,
					&lmccstPidSpdNormal, lmccs16WrpmMechRef, GetMotIRefCalcn.lmccs16WrpmMech);
		}
		break;

	case MCC_S16_PID_SPEED_CREEP:
		if (GetMotIRefCalcn.lmccs16PidChgCnt == 0)
		{
			lmccs16IqRefPid = Mcc_s16PIDController(lmccs16WrpmMechRef, GetMotIRefCalcn.lmccs16WrpmMech, &lmccstPidAct);
		}
		else
		{
			lmccs16IqRefPid = Mcc_s16ChangePid(GetMotIRefCalcn.lmccs16CalcIqRefPidStateOld,
					&lmccstPidSpdCreep, lmccs16WrpmMechRef, GetMotIRefCalcn.lmccs16WrpmMech);
		}
		break;

	case MCC_S16_PID_SPEED_STOP:
		if (GetMotIRefCalcn.lmccs16PidChgCnt == 0)
		{
			lmccs16IqRefPid = Mcc_s16PIDController(lmccs16WrpmMechRef, GetMotIRefCalcn.lmccs16WrpmMech, &lmccstPidAct);
		}
		else
		{
			lmccs16IqRefPid = Mcc_s16ChangePid(GetMotIRefCalcn.lmccs16CalcIqRefPidStateOld,
					&lmccstPidSpdStop, lmccs16WrpmMechRef, GetMotIRefCalcn.lmccs16WrpmMech);
		}
		break;

	case MCC_S16_PID_POSI_NORMAL:
		if (GetMotIRefCalcn.lmccs16PidChgCnt == 0)
		{
			lmccs16IqRefPid = Mcc_s16PIDController(lmccs32TgtPosi, GetMotIRefCalcn.lmccs32Posi, &lmccstPidAct);
		}
		else
		{
			lmccs16IqRefPid = Mcc_s16ChangePid(GetMotIRefCalcn.lmccs16CalcIqRefPidStateOld,
					&lmccstPidPosiNormal, lmccs32TgtPosi, GetMotIRefCalcn.lmccs32Posi);
		}
		break;

	case MCC_S16_PID_POSI_ABS_MUX:
		if (GetMotIRefCalcn.lmccs16PidChgCnt == 0)
		{
			lmccs16IqRefPid = Mcc_s16PIDController(lmccs32TgtPosi, GetMotIRefCalcn.lmccs32Posi, &lmccstPidAct);
		}
		else
		{
			lmccs16IqRefPid = Mcc_s16ChangePid(GetMotIRefCalcn.lmccs16CalcIqRefPidStateOld,
					&lmccstPidPosiAbsMux, lmccs32TgtPosi, GetMotIRefCalcn.lmccs32Posi);
		}
		break;

	case MCC_S16_PID_PRES_NORMAL:
		if (GetMotIRefCalcn.lmccs16PidChgCnt == 0)
		{
			lmccs16IqRefPid = Mcc_s16PIDController(lmccs16TarPress4Base, GetMotIRefCalcn.lmccs16MCpresFilt, &lmccstPidAct);
		}
		else
		{
			lmccs16IqRefPid = Mcc_s16ChangePid(GetMotIRefCalcn.lmccs16CalcIqRefPidStateOld,
					&lmccstPidPresNormal, lmccs16TarPress4Base, GetMotIRefCalcn.lmccs16MCpresFilt);
		}
		break;

	case MCC_S16_PID_NA:
		if (GetMotIRefCalcn.lmccs16PidChgCnt == -1)
		{
			/* only for stroke recovery */
			if (lmccstPidAct.s8GainId != lmccstPidNa.s8GainId)
			{
				lmccstPidAct = lmccstPidNa;
			}

			lmccs16IqRefPid = 0;
		}
		else if (GetMotIRefCalcn.lmccs16PidChgCnt == 0)
		{
			lmccs16IqRefPid = 0;
		}
		else
		{
			lmccs16IqRefPid = Mcc_s16ChangePid(GetMotIRefCalcn.lmccs16CalcIqRefPidStateOld,
					&lmccstPidNa, 0, 0);
		}
		break;

	default:
		lmccs16IqRefPid = 0;
		break;
	}
}

static int16_t Mcc_s16ChangePid(sint16 s16PidStatOld, mcc_pid_t *pPidStat, int16_t s16XrefStat, int16_t s16XStat)
{
	static int16_t s16XrefStatOld, s16XStatOld, s16IqRef0, s16IqRef1, s16IqRefFin;

	/* to find information of previous pid state */
	if ((s16PidStatOld >= MCC_S16_PID_SPEED_STAT_MIN) && (s16PidStatOld <= MCC_S16_PID_SPEED_STAT_MAX))
	{
		s16XrefStatOld = lmccs16WrpmMechRef;
		s16XStatOld = GetMotIRefCalcn.lmccs16WrpmMech;
	}
	else if ((s16PidStatOld >= MCC_S16_PID_POSI_STAT_MIN) && (s16PidStatOld <= MCC_S16_PID_POSI_STAT_MAX))
	{
		s16XrefStatOld = lmccs32TgtPosi;
		s16XStatOld = GetMotIRefCalcn.lmccs32Posi;
	}
	else if ((s16PidStatOld >= MCC_S16_PID_PRES_STAT_MIN) && (s16PidStatOld <= MCC_S16_PID_PRES_STAT_MAX))
	{
		s16XrefStatOld = lmccs16TarPress4Base;
		s16XStatOld = GetMotIRefCalcn.lmccs16MCpresFilt;
	}
	else
	{
		s16XrefStatOld = 0;
		s16XStatOld = 0;
	}

	if (GetMotIRefCalcn.lmccs16PidChgCnt == 1)
	{
		lmccstPidActTmp = *pPidStat;

		lmccstPidActTmp.s32Up = 0;
		lmccstPidActTmp.s32Ui = (int32_t)lmccstPidAct.s32Ui;
		lmccstPidActTmp.s32Ud = 0;
		lmccstPidActTmp.s16Uout = (int32_t)lmccstPidAct.s16Uout;
		lmccstPidActTmp.s32Err = 0;
		lmccstPidActTmp.s32ErrOld = 0;
		lmccstPidActTmp.s32ErrUi = 0;
		lmccstPidActTmp.s32Ucalc = (int32_t)lmccstPidAct.s32Ucalc;
		lmccstPidActTmp.s32Udiff = 0;

		s16IqRef0 = Mcc_s16PIDController(s16XrefStatOld, s16XStatOld, &lmccstPidAct);
		s16IqRef1 = Mcc_s16PIDController(s16XrefStat, s16XStat, &lmccstPidActTmp);
		s16IqRefFin = (s16IqRef0*(MCC_S16_PID_CHG_CNT_MAX - 1) + s16IqRef1)/MCC_S16_PID_CHG_CNT_MAX;
	}
	else if ((GetMotIRefCalcn.lmccs16PidChgCnt > 1) && (GetMotIRefCalcn.lmccs16PidChgCnt < MCC_S16_PID_CHG_CNT_MAX))
	{
		s16IqRef0 = Mcc_s16PIDController(s16XrefStatOld, s16XStatOld, &lmccstPidAct);
		s16IqRef1 = Mcc_s16PIDController(s16XrefStat, s16XStat, &lmccstPidActTmp);
		s16IqRefFin = (s16IqRef0 * (MCC_S16_PID_CHG_CNT_MAX - GetMotIRefCalcn.lmccs16PidChgCnt)
				+ s16IqRef1 * (GetMotIRefCalcn.lmccs16PidChgCnt))/MCC_S16_PID_CHG_CNT_MAX;
	}
	else if (GetMotIRefCalcn.lmccs16PidChgCnt == MCC_S16_PID_CHG_CNT_MAX)
	{
		lmccstPidAct = lmccstPidActTmp;
		s16IqRefFin = Mcc_s16PIDController(s16XrefStat, s16XStat, &lmccstPidAct);
	}
	else
	{
		s16IqRefFin = 0;
		s16IqRef0 = 0;
		s16IqRef1 = 0;
	}

	return s16IqRefFin;
}


static void Mcc_vCalcIqRefComp(void)
{
	static int8_t	s8Act1Cnt;
	static int8_t	s8Act1MaxCnt = 2;

	static int8_t	s8Act2Cnt;
	static int8_t	s8Act2MaxCnt = 2;

#if MCC_S16_ABS_MUX_PATTERN == 1
	if (GetMotIRefCalcn.lmccs16CalcIqRefCompState == MCC_S16_COMP_ABS_MUX_PATTERN)
	{
		if (GetMotIRefCalcn.lmccs16CalcIqRefCompStateOld != MCC_S16_COMP_ABS_MUX_PATTERN)
		{
			s8Act1Cnt = 1;
		}
		else
		{
			if ( (s8Act1Cnt > 0) && (s8Act1Cnt <= s8Act1MaxCnt) )
			{
				s8Act1Cnt++;
			}
		}

		if ((GetMotIRefCalcn.lmccs32Posi < lmccs32TgtPosi*8/10) && (s8Act2Cnt == 0))
		{
			s8Act2Cnt = 1;
		}
		else
		{
			if ((s8Act2Cnt > 0 ) && ( s8Act2Cnt <= s8Act2MaxCnt))
			{
				s8Act2Cnt++;
			}
		}

		if ( (s8Act1Cnt > 0) && (s8Act1Cnt <= s8Act1MaxCnt) && (s8Act2Cnt == 0) )
		{
			lmccs16IqRefComp = S16_MTRCURR_40A;
		}
		else if ( (s8Act1Cnt > s8Act1MaxCnt) && (s8Act2Cnt == 0) )
		{
			lmccs16IqRefComp = S16_MTRCURR_30A;
		}
		else if ((s8Act2Cnt > 0) && (s8Act2Cnt <= s8Act2MaxCnt))
		{
			lmccs16IqRefComp = -S16_MTRCURR_40A;
		}
		else if (s8Act2Cnt > s8Act2MaxCnt)
		{
			lmccs16IqRefComp = S16_MTRCURR_30A;
		}
		else
		{
			lmccs16IqRefComp = 0;
		}

		if (GetMotIRefCalcn.lmccs8PChamberVolume == U8_BOOST_BACKWARD)
		{
			lmccs16IqRefComp = -lmccs16IqRefComp;
		}
	}
	else
	{
		s8Act1Cnt = 0;
		s8Act2Cnt = 0;
		lmccs16IqRefComp = 0;
	}
#else
	if(GetMotIRefCalcn.lmccs16CalcIqRefPidState == MCC_S16_PID_POSI_ABS_MUX)
	{
		if (lmccs8TarDeltaUopdatedFlg == 1)
		{
			s8Act1Cnt = 1;
		}
		else
		{
			if ((s8Act1Cnt > 0 ) && ( s8Act1Cnt <= s8Act1MaxCnt))
			{
				s8Act1Cnt = s8Act1Cnt + 1;
			}
		}

		if ((GetMotIRefCalcn.lmccs32Posi < lmccs32TgtPosi*8/10) && (s8Act2Cnt == 0))
		{
			s8Act2Cnt = 1;
		}
		else
		{
			if ((s8Act2Cnt > 0 ) && ( s8Act2Cnt <= s8Act2MaxCnt))
			{
				s8Act2Cnt = s8Act2Cnt + 1;
			}
		}

		if (lmccs32TgtPosiOld != lmccs32TgtPosi)
		{
			s8Act2Cnt = 0;
		}

		if ((s8Act2Cnt > 0 ) && ( s8Act2Cnt <= s8Act2MaxCnt))
		{
			lmccs16IqRefComp = -S16_MTRCURR_40A;
		}
		else if ((s8Act1Cnt > 0 ) && ( s8Act1Cnt <= s8Act1MaxCnt))
		{
			lmccs16IqRefComp = S16_MTRCURR_40A;
		}
		else
		{
			lmccs16IqRefComp = 0;
		}

		if (GetMotIRefCalcn.lmccs8PChamberVolume == U8_BOOST_BACKWARD)
		{
			lmccs16IqRefComp = -lmccs16IqRefComp;
		}
		else { }
	}
	else
	{
		lmccs16IqRefComp = 0;
	}
#endif
}


static void Mcc_vCalcIdqRefFinal(void)
{
	static int32_t s32IqRef;
	static int16_t s16IqRefMax;
	static int32_t s32IqRefAbs;
	static mcc_dq_t stIref;

	/* current fade-out limitation */
	static int16_t s16CurrLmtCnt;
	static int16_t s16CurrLmtCntThr = 8000;	/* 8sec */
	static int16_t s16MinOfCurrMax = S16_MTRCURR_80A;	/* 80A */
	static int16_t s16MaxOfCurrMin = -S16_MTRCURR_80A;	/* -80A, s16MaxOfCurrMin should be negative */
	static int16_t s16CurrMax;
	static int16_t s16CurrMin;
	/* current fade-out limitation */

	s32IqRef = 0;
	s16IqRefMax = 0;
	s32IqRefAbs = 0;

	s32IqRef = (int32_t)lmccs16IqRefBase + (int32_t)lmccs16IqRefPid + (int32_t)lmccs16IqRefComp;
	if (s32IqRef > MCC_S16_IQREF_MAX)
	{
		s32IqRef = MCC_S16_IQREF_MAX;
	}
	else if (s32IqRef < MCC_S16_IQREF_MIN)
	{
		s32IqRef = MCC_S16_IQREF_MIN;
	}
	else
	{
		;
	}

#if MCC_U16_FLUX_WEAKENING_CTRL == 1
	lmccs8FluxWeakengFlgOld = lmccs8FluxWeakengFlg;
	if( (GetMotIRefCalcn.lmccs8PCtrlBoostMod == MCC_S8_BOOST_MODE_QUICK) && (GetMotIRefCalcn.lmccs16WrpmMech > 0) )
	{
		lmccs8FluxWeakengFlg = TRUE;
	}
	else
	{
#if MCC_S16_FLUXWEAK_CONDITION == MCC_S16_FLUXWEAK_CONDITION_12V_99A
		if ((lmccs8FluxWeakengFlgOld == TRUE) && (GetMotIRefCalcn.lmccs16WrpmMech > MCC_S16_BETA_RPM0_12V_99A))
#elif MCC_S16_FLUXWEAK_CONDITION == MCC_S16_FLUXWEAK_CONDITION_12V_120A
		if ((lmccs8FluxWeakengFlgOld == TRUE) && (GetMotIRefCalcn.lmccs16WrpmMech > MCC_S16_BETA_RPM0_12V_120A))
#else
		if ((lmccs8FluxWeakengFlgOld == TRUE) && (GetMotIRefCalcn.lmccs16WrpmMech > MCC_S16_BETA_RPM0_12V_99A))
#endif
		{
			lmccs8FluxWeakengFlg = TRUE;
		}
		else
		{
			lmccs8FluxWeakengFlg = FALSE;
		}
	}

	if(lmccs8FluxWeakengFlg == 1)
	{
		Mcc_FWeakenigCtrl(&stIref, (int16_t)s32IqRef, GetMotIRefCalcn.lmccs16WrpmMech);
		s16CurrLmtCnt = 0;
	}
	else
	{
		stIref.s16d = 0;
		stIref.s16q = (int16_t)s32IqRef;

		if (s16CurrLmtCnt >= s16CurrLmtCntThr)
		{
			s16CurrMax = s16CurrMax - 1;
			s16CurrMin = s16CurrMin + 1;
			if (s16CurrMax < s16MinOfCurrMax)
			{
				s16CurrMax = s16MinOfCurrMax;
			}

			if (s16CurrMin > s16MaxOfCurrMin)
			{
				s16CurrMin = s16MaxOfCurrMin;
			}

			if (stIref.s16q > s16CurrMax)
			{
				stIref.s16q = s16CurrMax;
			}
			else if (stIref.s16q < s16CurrMin)
			{
				stIref.s16q = s16CurrMin;
			}
			else
			{
				s16CurrLmtCnt = s16CurrLmtCnt - 1;
			}
		}
		else
		{
			s16CurrMax = S16_MTRCURR_MAX;
			s16CurrMin = S16_MTRCURR_MIN;

			if (lmccstIref.s16q > s16CurrMax)
			{
				lmccstIref.s16q = s16CurrMax;
				s16CurrLmtCnt = s16CurrLmtCnt + 1;
			}
			else if (lmccstIref.s16q < s16CurrMin)
			{
				lmccstIref.s16q = s16CurrMin;
				s16CurrLmtCnt = s16CurrLmtCnt + 1;
			}
			else
			{
				s16CurrLmtCnt = s16CurrLmtCnt - 1;
			}

			if (s16CurrLmtCnt > s16CurrLmtCntThr)
			{
				s16CurrLmtCnt = s16CurrLmtCntThr;
			}
			else if (s16CurrLmtCnt < 0)
			{
				s16CurrLmtCnt = 0;
			}
			else
			{
				;
			}
		}
	}

	lmccs16IdRef = stIref.s16d;
	lmccs16IqRef = stIref.s16q;
#else
	lmccs16IdRef = 0;
	lmccs16IqRef = (int16_t)s32IqRef;
#endif

	if (GetMotIRefCalcn.lmccs16MtrCtrlMode == MCC_S16_CALC_DUTY_CMD)
	{
		lmccs16IdRef = 0;
		lmccs16IqRef = 0;
	}
}

static void Mcc_FWeakenigCtrl(mcc_dq_t* pStIref, int16_t s16IqRef, int16_t s16Wrpm)
{
	/* IdRef generation */
	static int16_t s16WrpmAbs;
	static Frac16 f16BetaAngle;
	static Frac16 f16SinBetaAngle;
	static Frac16 f16CosBetaAngle;
	static int16_t lmccs16IqRefMax;
	static int16_t s16IqRefAbs;

	s16WrpmAbs = Mcc_s32Abs(s16Wrpm);

#if (MCC_S16_FLUXWEAK_CONDITION == MCC_S16_FLUXWEAK_CONDITION_12V_99A)
	f16BetaAngle = L_s16IInter12Point(s16WrpmAbs,
			MCC_S16_BETA_RPM0_12V_99A, MCC_S16_BETA_ANGLE0_12V_99A,
			MCC_S16_BETA_RPM1_12V_99A, MCC_S16_BETA_ANGLE1_12V_99A,
			MCC_S16_BETA_RPM2_12V_99A, MCC_S16_BETA_ANGLE2_12V_99A,
			MCC_S16_BETA_RPM3_12V_99A, MCC_S16_BETA_ANGLE3_12V_99A,
			MCC_S16_BETA_RPM4_12V_99A, MCC_S16_BETA_ANGLE4_12V_99A,
			MCC_S16_BETA_RPM5_12V_99A, MCC_S16_BETA_ANGLE5_12V_99A,
			MCC_S16_BETA_RPM6_12V_99A, MCC_S16_BETA_ANGLE6_12V_99A,
			MCC_S16_BETA_RPM7_12V_99A, MCC_S16_BETA_ANGLE7_12V_99A,
			MCC_S16_BETA_RPM8_12V_99A, MCC_S16_BETA_ANGLE8_12V_99A,
			MCC_S16_BETA_RPM9_12V_99A, MCC_S16_BETA_ANGLE9_12V_99A,
			MCC_S16_BETA_RPM10_12V_99A, MCC_S16_BETA_ANGLE10_12V_99A,
			MCC_S16_BETA_RPM11_12V_99A, MCC_S16_BETA_ANGLE11_12V_99A);
#elif (MCC_S16_FLUXWEAK_CONDITION == MCC_S16_FLUXWEAK_CONDITION_12V_120A)
	f16BetaAngle = L_s16IInter12Point(s16WrpmAbs,
			MCC_S16_BETA_RPM0_12V_120A, MCC_S16_BETA_ANGLE0_12V_120A,
			MCC_S16_BETA_RPM1_12V_120A, MCC_S16_BETA_ANGLE1_12V_120A,
			MCC_S16_BETA_RPM2_12V_120A, MCC_S16_BETA_ANGLE2_12V_120A,
			MCC_S16_BETA_RPM3_12V_120A, MCC_S16_BETA_ANGLE3_12V_120A,
			MCC_S16_BETA_RPM4_12V_120A, MCC_S16_BETA_ANGLE4_12V_120A,
			MCC_S16_BETA_RPM5_12V_120A, MCC_S16_BETA_ANGLE5_12V_120A,
			MCC_S16_BETA_RPM6_12V_120A, MCC_S16_BETA_ANGLE6_12V_120A,
			MCC_S16_BETA_RPM7_12V_120A, MCC_S16_BETA_ANGLE7_12V_120A,
			MCC_S16_BETA_RPM8_12V_120A, MCC_S16_BETA_ANGLE8_12V_120A,
			MCC_S16_BETA_RPM9_12V_120A, MCC_S16_BETA_ANGLE9_12V_120A,
			MCC_S16_BETA_RPM10_12V_120A, MCC_S16_BETA_ANGLE10_12V_120A,
			MCC_S16_BETA_RPM11_12V_120A, MCC_S16_BETA_ANGLE11_12V_120A);
#else
	/* default */
	f16BetaAngle = L_s16IInter12Point(s16WrpmAbs,
			MCC_S16_BETA_RPM0_12V_99A, MCC_S16_BETA_ANGLE0_12V_99A,
			MCC_S16_BETA_RPM1_12V_99A, MCC_S16_BETA_ANGLE1_12V_99A,
			MCC_S16_BETA_RPM2_12V_99A, MCC_S16_BETA_ANGLE2_12V_99A,
			MCC_S16_BETA_RPM3_12V_99A, MCC_S16_BETA_ANGLE3_12V_99A,
			MCC_S16_BETA_RPM4_12V_99A, MCC_S16_BETA_ANGLE4_12V_99A,
			MCC_S16_BETA_RPM5_12V_99A, MCC_S16_BETA_ANGLE5_12V_99A,
			MCC_S16_BETA_RPM6_12V_99A, MCC_S16_BETA_ANGLE6_12V_99A,
			MCC_S16_BETA_RPM7_12V_99A, MCC_S16_BETA_ANGLE7_12V_99A,
			MCC_S16_BETA_RPM8_12V_99A, MCC_S16_BETA_ANGLE8_12V_99A,
			MCC_S16_BETA_RPM9_12V_99A, MCC_S16_BETA_ANGLE9_12V_99A,
			MCC_S16_BETA_RPM10_12V_99A, MCC_S16_BETA_ANGLE10_12V_99A,
			MCC_S16_BETA_RPM11_12V_99A, MCC_S16_BETA_ANGLE11_12V_99A);
#endif

	f16SinBetaAngle = LAMTR_f16Sin(f16BetaAngle);
	f16CosBetaAngle = LAMTR_f16Cos(f16BetaAngle);

	pStIref->s16d = -(int16_t)((int32_t)MCC_S16_IQREF_MAX * (int32_t)f16SinBetaAngle/MCC_F16_1);
	lmccs16IqRefMax = (int16_t)((int32_t)MCC_S16_IQREF_MAX * (int32_t)f16CosBetaAngle/MCC_F16_1);

	s16IqRefAbs = Mcc_s32Abs(s16IqRef);
	if (s16IqRefAbs > lmccs16IqRefMax)
	{
		pStIref->s16q = Mcc_s16Sign(s16IqRef)*lmccs16IqRefMax;
	}
	else
	{
		pStIref->s16q = s16IqRef;
	}
}

int32_t Mcc_s32Abs(int32_t x)
{
	int32_t y = 0;

	if (x >= 0)
	{
		y = x;
	}
	else
	{
		y = -x;
	}

	return y;
}

int16_t Mcc_s16Sign(int32_t s32Xin)
{
	int16_t s16Xout = 0;
	if (s32Xin > 0)
	{
		s16Xout = 1;
	}
	else if (s32Xin < 0)
	{
		s16Xout = -1;
	}
	else
	{
		;
	}

	return s16Xout;
}

#define MCC_1MSCTRL_STOP_SEC_CODE
#include "Mcc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
