/**
 * @defgroup Mcc_1msCtrl Mcc_1msCtrl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mcc_1msCtrl.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Mcc_1msCtrl.h"
#include "Mcc_1msCtrl_Ifa.h"
#include "IfxStm_reg.h"

#include "LAMTR_DecideMtrCtrlMode.h"
#include "LAMTR_CalcCurrentRef.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define MCC_1MSCTRL_START_SEC_CONST_UNSPECIFIED
#include "Mcc_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define MCC_1MSCTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Mcc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MCC_1MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mcc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Mcc_1msCtrl_HdrBusType Mcc_1msCtrlBus;

/* Internal Logic Bus */
MotorCurrentControl_SRCbusType	 MTRCurCTRLsrcbus;

/* Version Info */
const SwcVersionInfo_t Mcc_1msCtrlVersionInfo = 
{   
    MCC_1MSCTRL_MODULE_ID,           /* Mcc_1msCtrlVersionInfo.ModuleId */
    MCC_1MSCTRL_MAJOR_VERSION,       /* Mcc_1msCtrlVersionInfo.MajorVer */
    MCC_1MSCTRL_MINOR_VERSION,       /* Mcc_1msCtrlVersionInfo.MinorVer */
    MCC_1MSCTRL_PATCH_VERSION,       /* Mcc_1msCtrlVersionInfo.PatchVer */
    MCC_1MSCTRL_BRANCH_VERSION       /* Mcc_1msCtrlVersionInfo.BranchVer */
};
    
/* Input Data Element */
Spc_1msCtrlCircPFild1msInfo_t Mcc_1msCtrlCircPFild1msInfo;
Spc_1msCtrlPistPFild1msInfo_t Mcc_1msCtrlPistPFild1msInfo;
Msp_CtrlMotRotgAgSigInfo_t Mcc_1msCtrlMotRotgAgSigInfo;
Vat_CtrlIdbVlvActInfo_t Mcc_1msCtrlIdbVlvActInfo;
Pct_5msCtrlStkRecvryActnIfInfo_t Mcc_1msCtrlStkRecvryActnIfInfo;
Mom_HndlrEcuModeSts_t Mcc_1msCtrlEcuModeSts;
Eem_SuspcDetnFuncInhibitMccSts_t Mcc_1msCtrlFuncInhibitMccSts;
Pct_5msCtrlTgtDeltaStkType_t Mcc_1msCtrlTgtDeltaStkType;
Pct_5msCtrlPCtrlBoostMod_t Mcc_1msCtrlPCtrlBoostMod;
Pct_5msCtrlPCtrlFadeoutSt_t Mcc_1msCtrlPCtrlFadeoutSt;
Pct_5msCtrlPCtrlSt_t Mcc_1msCtrlPCtrlSt;
Pct_5msCtrlInVlvAllCloseReq_t Mcc_1msCtrlInVlvAllCloseReq;
Pct_5msCtrlPChamberVolume_t Mcc_1msCtrlPChamberVolume;
Pct_5msCtrlTarDeltaStk_t Mcc_1msCtrlTarDeltaStk;
Pct_5msCtrlFinalTarPFromPCtrl_t Mcc_1msCtrlFinalTarPFromPCtrl;

/* Output Data Element */
Mcc_1msCtrlMotDqIRefMccInfo_t Mcc_1msCtrlMotDqIRefMccInfo;
Mcc_1msCtrlIdbMotPosnInfo_t Mcc_1msCtrlIdbMotPosnInfo;
Mcc_1msCtrlFluxWeakengStInfo_t Mcc_1msCtrlFluxWeakengStInfo;
Mcc_1msCtrlMotCtrlMode_t Mcc_1msCtrlMotCtrlMode;
Mcc_1msCtrlMotCtrlState_t Mcc_1msCtrlMotCtrlState;
Mcc_1msCtrlMotICtrlFadeOutState_t Mcc_1msCtrlMotICtrlFadeOutState;
Mcc_1msCtrlStkRecvryStabnEndOK_t Mcc_1msCtrlStkRecvryStabnEndOK;

uint32 Mcc_1msCtrl_Timer_Start;
uint32 Mcc_1msCtrl_Timer_Elapsed;

#define MCC_1MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mcc_MemMap.h"
#define MCC_1MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Mcc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MCC_1MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Mcc_MemMap.h"
#define MCC_1MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Mcc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MCC_1MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Mcc_MemMap.h"
#define MCC_1MSCTRL_START_SEC_VAR_32BIT
#include "Mcc_MemMap.h"
/** Variable Section (32BIT)**/


#define MCC_1MSCTRL_STOP_SEC_VAR_32BIT
#include "Mcc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MCC_1MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mcc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define MCC_1MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mcc_MemMap.h"
#define MCC_1MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Mcc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MCC_1MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Mcc_MemMap.h"
#define MCC_1MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Mcc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MCC_1MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Mcc_MemMap.h"
#define MCC_1MSCTRL_START_SEC_VAR_32BIT
#include "Mcc_MemMap.h"
/** Variable Section (32BIT)**/


#define MCC_1MSCTRL_STOP_SEC_VAR_32BIT
#include "Mcc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define MCC_1MSCTRL_START_SEC_CODE
#include "Mcc_MemMap.h"
void LAMTR_vInitMotIRefGenn1ms(void);
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Mcc_1msCtrl_Init(void)
{
    /* Initialize internal bus */
    Mcc_1msCtrlBus.Mcc_1msCtrlCircPFild1msInfo.PrimCircPFild1ms = 0;
    Mcc_1msCtrlBus.Mcc_1msCtrlCircPFild1msInfo.SecdCircPFild1ms = 0;
    Mcc_1msCtrlBus.Mcc_1msCtrlPistPFild1msInfo.PistPFild1ms = 0;
    Mcc_1msCtrlBus.Mcc_1msCtrlMotRotgAgSigInfo.StkPosnMeasd = 0;
    Mcc_1msCtrlBus.Mcc_1msCtrlMotRotgAgSigInfo.MotMechAngleSpdFild = 0;
    Mcc_1msCtrlBus.Mcc_1msCtrlIdbVlvActInfo.FadeoutSt2EndOk = 0;
    Mcc_1msCtrlBus.Mcc_1msCtrlStkRecvryActnIfInfo.StkRecvryCtrlState = 0;
    Mcc_1msCtrlBus.Mcc_1msCtrlStkRecvryActnIfInfo.StkRecvryRequestedTime = 0;
    Mcc_1msCtrlBus.Mcc_1msCtrlEcuModeSts = 0;
    Mcc_1msCtrlBus.Mcc_1msCtrlFuncInhibitMccSts = 0;
    Mcc_1msCtrlBus.Mcc_1msCtrlTgtDeltaStkType = 0;
    Mcc_1msCtrlBus.Mcc_1msCtrlPCtrlBoostMod = 0;
    Mcc_1msCtrlBus.Mcc_1msCtrlPCtrlFadeoutSt = 0;
    Mcc_1msCtrlBus.Mcc_1msCtrlPCtrlSt = 0;
    Mcc_1msCtrlBus.Mcc_1msCtrlInVlvAllCloseReq = 0;
    Mcc_1msCtrlBus.Mcc_1msCtrlPChamberVolume = 0;
    Mcc_1msCtrlBus.Mcc_1msCtrlTarDeltaStk = 0;
    Mcc_1msCtrlBus.Mcc_1msCtrlFinalTarPFromPCtrl = 0;
    Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo.IdRef = 0;
    Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo.IqRef = 0;
    Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn = 0;
    Mcc_1msCtrlBus.Mcc_1msCtrlFluxWeakengStInfo.FluxWeakengFlg = 0;
    Mcc_1msCtrlBus.Mcc_1msCtrlFluxWeakengStInfo.FluxWeakengFlgOld = 0;
    Mcc_1msCtrlBus.Mcc_1msCtrlMotCtrlMode = 0;
    Mcc_1msCtrlBus.Mcc_1msCtrlMotCtrlState = 0;
    Mcc_1msCtrlBus.Mcc_1msCtrlMotICtrlFadeOutState = 0;
    Mcc_1msCtrlBus.Mcc_1msCtrlStkRecvryStabnEndOK = 0;
	LAMTR_vInitMotIRefGenn1ms();
}

void Mcc_1msCtrl(void)
{
    Mcc_1msCtrl_Timer_Start = STM0_TIM0.U;

    /* Input */
    Mcc_1msCtrl_Read_Mcc_1msCtrlCircPFild1msInfo(&Mcc_1msCtrlBus.Mcc_1msCtrlCircPFild1msInfo);
    /*==============================================================================
    * Members of structure Mcc_1msCtrlCircPFild1msInfo 
     : Mcc_1msCtrlCircPFild1msInfo.PrimCircPFild1ms;
     : Mcc_1msCtrlCircPFild1msInfo.SecdCircPFild1ms;
     =============================================================================*/
    
    Mcc_1msCtrl_Read_Mcc_1msCtrlPistPFild1msInfo(&Mcc_1msCtrlBus.Mcc_1msCtrlPistPFild1msInfo);
    /*==============================================================================
    * Members of structure Mcc_1msCtrlPistPFild1msInfo 
     : Mcc_1msCtrlPistPFild1msInfo.PistPFild1ms;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Mcc_1msCtrl_Read_Mcc_1msCtrlMotRotgAgSigInfo_StkPosnMeasd(&Mcc_1msCtrlBus.Mcc_1msCtrlMotRotgAgSigInfo.StkPosnMeasd);
    Mcc_1msCtrl_Read_Mcc_1msCtrlMotRotgAgSigInfo_MotMechAngleSpdFild(&Mcc_1msCtrlBus.Mcc_1msCtrlMotRotgAgSigInfo.MotMechAngleSpdFild);

    /* Decomposed structure interface */
    Mcc_1msCtrl_Read_Mcc_1msCtrlIdbVlvActInfo_FadeoutSt2EndOk(&Mcc_1msCtrlBus.Mcc_1msCtrlIdbVlvActInfo.FadeoutSt2EndOk);

    /* Decomposed structure interface */
    Mcc_1msCtrl_Read_Mcc_1msCtrlStkRecvryActnIfInfo_StkRecvryCtrlState(&Mcc_1msCtrlBus.Mcc_1msCtrlStkRecvryActnIfInfo.StkRecvryCtrlState);
    Mcc_1msCtrl_Read_Mcc_1msCtrlStkRecvryActnIfInfo_StkRecvryRequestedTime(&Mcc_1msCtrlBus.Mcc_1msCtrlStkRecvryActnIfInfo.StkRecvryRequestedTime);

    Mcc_1msCtrl_Read_Mcc_1msCtrlEcuModeSts(&Mcc_1msCtrlBus.Mcc_1msCtrlEcuModeSts);
    Mcc_1msCtrl_Read_Mcc_1msCtrlFuncInhibitMccSts(&Mcc_1msCtrlBus.Mcc_1msCtrlFuncInhibitMccSts);
    Mcc_1msCtrl_Read_Mcc_1msCtrlTgtDeltaStkType(&Mcc_1msCtrlBus.Mcc_1msCtrlTgtDeltaStkType);
    Mcc_1msCtrl_Read_Mcc_1msCtrlPCtrlBoostMod(&Mcc_1msCtrlBus.Mcc_1msCtrlPCtrlBoostMod);
    Mcc_1msCtrl_Read_Mcc_1msCtrlPCtrlFadeoutSt(&Mcc_1msCtrlBus.Mcc_1msCtrlPCtrlFadeoutSt);
    Mcc_1msCtrl_Read_Mcc_1msCtrlPCtrlSt(&Mcc_1msCtrlBus.Mcc_1msCtrlPCtrlSt);
    Mcc_1msCtrl_Read_Mcc_1msCtrlInVlvAllCloseReq(&Mcc_1msCtrlBus.Mcc_1msCtrlInVlvAllCloseReq);
    Mcc_1msCtrl_Read_Mcc_1msCtrlPChamberVolume(&Mcc_1msCtrlBus.Mcc_1msCtrlPChamberVolume);
    Mcc_1msCtrl_Read_Mcc_1msCtrlTarDeltaStk(&Mcc_1msCtrlBus.Mcc_1msCtrlTarDeltaStk);
    Mcc_1msCtrl_Read_Mcc_1msCtrlFinalTarPFromPCtrl(&Mcc_1msCtrlBus.Mcc_1msCtrlFinalTarPFromPCtrl);

    /* Process */
	Mcc_vCallMotCtrlModDet();	/*choi*/
	Mcc_vCallMotIRefCalcn();

    /* Output */
    Mcc_1msCtrl_Write_Mcc_1msCtrlMotDqIRefMccInfo(&Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo);
    /*==============================================================================
    * Members of structure Mcc_1msCtrlMotDqIRefMccInfo 
     : Mcc_1msCtrlMotDqIRefMccInfo.IdRef;
     : Mcc_1msCtrlMotDqIRefMccInfo.IqRef;
     =============================================================================*/
    
    Mcc_1msCtrl_Write_Mcc_1msCtrlIdbMotPosnInfo(&Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo);
    /*==============================================================================
    * Members of structure Mcc_1msCtrlIdbMotPosnInfo 
     : Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn;
     =============================================================================*/
    
    Mcc_1msCtrl_Write_Mcc_1msCtrlFluxWeakengStInfo(&Mcc_1msCtrlBus.Mcc_1msCtrlFluxWeakengStInfo);
    /*==============================================================================
    * Members of structure Mcc_1msCtrlFluxWeakengStInfo 
     : Mcc_1msCtrlFluxWeakengStInfo.FluxWeakengFlg;
     : Mcc_1msCtrlFluxWeakengStInfo.FluxWeakengFlgOld;
     =============================================================================*/
    
    Mcc_1msCtrl_Write_Mcc_1msCtrlMotCtrlMode(&Mcc_1msCtrlBus.Mcc_1msCtrlMotCtrlMode);
    Mcc_1msCtrl_Write_Mcc_1msCtrlMotCtrlState(&Mcc_1msCtrlBus.Mcc_1msCtrlMotCtrlState);
    Mcc_1msCtrl_Write_Mcc_1msCtrlMotICtrlFadeOutState(&Mcc_1msCtrlBus.Mcc_1msCtrlMotICtrlFadeOutState);
    Mcc_1msCtrl_Write_Mcc_1msCtrlStkRecvryStabnEndOK(&Mcc_1msCtrlBus.Mcc_1msCtrlStkRecvryStabnEndOK);

    Mcc_1msCtrl_Timer_Elapsed = STM0_TIM0.U - Mcc_1msCtrl_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
void LAMTR_vInitMotIRefGenn1ms(void)
{
	Mcc_vInitMotCtrlModDet();
	Mcc_vInitMotIRefCalcn();
}
#define MCC_1MSCTRL_STOP_SEC_CODE
#include "Mcc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
