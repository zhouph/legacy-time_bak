
#include "Mcc_1msCtrl.h"








void MCC_vChangePid(mcc_pid_t *pAfter, mcc_pid_t* pBefore)
{
	pAfter->s16Kp = pBefore->s16Kp;
	pAfter->f16Kp = pBefore->f16Kp;
	pAfter->s16KiTs = pBefore->s16KiTs;
	pAfter->f16KiTs = pBefore->f16KiTs;
	pAfter->s16KdFs = pBefore->s16KdFs;
	pAfter->f16KdFs = pBefore->f16KdFs;

	pAfter->s16UpMin = pBefore->s16UpMin;
	pAfter->s16UpMax = pBefore->s16UpMax;
	pAfter->s16UiMin = pBefore->s16UiMin;
	pAfter->s16UiMax = pBefore->s16UiMax;
	pAfter->s16UdMin = pBefore->s16UdMin;
	pAfter->s16UdMax = pBefore->s16UdMax;
	pAfter->s16UoutMin = pBefore->s16UoutMin;
	pAfter->s16UoutMax = pBefore->s16UoutMax;

	pAfter->s32Up = 0;
	pAfter->s32Ui = 0;
	pAfter->s32Ud = 0;
	pAfter->s16Uout = 0;
	pAfter->s32Err = 0;
	pAfter->s32ErrOld = 0;
	pAfter->s32ErrUi = 0;
	pAfter->s32Ucalc = 0;
	pAfter->s32Udiff = 0;
}



uint8 MCC_u8ChangePidGain(mcc_pid_t *pGainNow, mcc_pid_t *pGainFin, mcc_pid_t *pGainIni, sint16 s16StepMax)
{
	static uint8 u8GainChangedFlg;
	static sint32 s32Kp, s32KiTs, s32KdFs;
	static sint32 s32KpNow, s32KiTsNow, s32KdFsNow;
	static sint32 s32KpFin, s32KiTsFin, s32KdFsFin;
	static sint32 s32KpIni, s32KiTsIni, s32KdFsIni;
	static sint32 s32DeltaKp, s32DeltaKiTs, s32DeltaKdFs;
	static sint32 s32DeltaKpAbs, s32DeltaKiTsAbs, s32DeltaKdFsAbs;
	static sint32 s32KpGap, s32KiTsGap, s32KdFsGap;

	if (s16StepMax > 0)
	{
		s32KpNow = pGainNow->s16Kp*MCC_F16_1 + pGainNow->f16Kp;
		s32KiTsNow = pGainNow->s16KiTs*MCC_F16_1 + pGainNow->f16KiTs;
		s32KdFsNow = pGainNow->s16KdFs*MCC_F16_1 + pGainNow->f16KdFs;

		s32KpFin = pGainFin->s16Kp*MCC_F16_1 + pGainFin->f16Kp;
		s32KpIni = pGainIni->s16Kp*MCC_F16_1 + pGainIni->f16Kp;
		s32DeltaKp = (s32KpFin - s32KpIni)/s16StepMax;
		s32Kp = s32KpNow + s32DeltaKp;

		s32KiTsFin = pGainFin->s16KiTs*MCC_F16_1 + pGainFin->f16KiTs;
		s32KiTsIni = pGainIni->s16KiTs*MCC_F16_1 + pGainIni->f16KiTs;
		s32DeltaKiTs = (s32KiTsFin - s32KiTsIni)/s16StepMax;
		s32KiTs = s32KiTsNow + s32DeltaKiTs;

		s32KdFsFin = pGainFin->s16KdFs*MCC_F16_1 + pGainFin->f16KdFs;
		s32KdFsIni = pGainIni->s16KdFs*MCC_F16_1 + pGainIni->f16KdFs;
		s32DeltaKdFs = (s32KdFsFin - s32KdFsIni)/s16StepMax;
		s32KdFs = s32KdFsNow + s32DeltaKdFs;

		s32KpGap = s32KpFin - s32KpNow;
		s32KiTsGap = s32KiTsFin - s32KiTsNow;
		s32KdFsGap = s32KdFsFin - s32KdFsNow;


		if (s32DeltaKp >= 0)
		{
			s32DeltaKpAbs = s32DeltaKp;
		}
		else
		{
			s32DeltaKpAbs = -s32DeltaKp;
		}

		if (s32DeltaKiTs >= 0)
		{
			s32DeltaKiTsAbs = s32DeltaKiTs;
		}
		else
		{
			s32DeltaKiTsAbs = -s32DeltaKiTs;
		}

		if (s32DeltaKdFs >= 0)
		{
			s32DeltaKdFsAbs = s32DeltaKdFs;
		}
		else
		{
			s32DeltaKdFsAbs = -s32DeltaKdFs;
		}


		if ((s32KpGap > -s32DeltaKpAbs) && (s32KpGap < s32DeltaKpAbs))
		{
			pGainNow->s16Kp = pGainFin->s16Kp;
			pGainNow->f16Kp = pGainFin->f16Kp;
		}
		else
		{
			pGainNow->s16Kp = s32Kp/MCC_F16_1;
			pGainNow->f16Kp = s32Kp - pGainNow->s16Kp*MCC_F16_1;
		}


		if ((s32KiTsGap > -s32DeltaKiTsAbs) && (s32KiTsGap < s32DeltaKiTsAbs))
		{
			pGainNow->s16KiTs = pGainFin->s16KiTs;
			pGainNow->f16KiTs = pGainFin->f16KiTs;
		}
		else
		{
			pGainNow->s16KiTs = s32KiTs/MCC_F16_1;
			pGainNow->f16KiTs = s32KiTs - pGainNow->s16KiTs*MCC_F16_1;
		}


		if ((s32KdFsGap > -s32DeltaKdFsAbs) && (s32KdFsGap < s32DeltaKdFsAbs))
		{
			pGainNow->s16KdFs = pGainFin->s16KdFs;
			pGainNow->f16KdFs = pGainFin->f16KdFs;
		}
		else
		{
			pGainNow->s16KdFs = s32KdFs/MCC_F16_1;
			pGainNow->f16KdFs = s32KdFs - pGainNow->s16KdFs*MCC_F16_1;
		}


		if ((pGainNow->s16Kp == pGainFin->s16Kp)   && (pGainNow->f16Kp == pGainFin->f16Kp) &&
			(pGainNow->s16KiTs == pGainFin->s16KiTs) && (pGainNow->f16KiTs == pGainFin->f16KiTs) &&
			(pGainNow->s16KdFs == pGainFin->s16KdFs) && (pGainNow->f16KdFs == pGainFin->f16KdFs))
		{
			pGainNow->s16Kp = pGainFin->s16Kp;
			pGainNow->s16KiTs = pGainFin->s16KiTs;
			pGainNow->s16KdFs = pGainFin->s16KdFs;

			pGainNow->f16Kp = pGainFin->f16Kp;
			pGainNow->f16KiTs = pGainFin->f16KiTs;
			pGainNow->f16KdFs = pGainFin->f16KdFs;

			u8GainChangedFlg = 1;
		}
		else
		{
			u8GainChangedFlg = 0;
		}
	}
	else
	{
		pGainNow->s16Kp = pGainIni->s16Kp;
		pGainNow->s16KiTs = pGainIni->s16KiTs;
		pGainNow->s16KdFs = pGainIni->s16KdFs;

		pGainNow->f16Kp = pGainIni->f16Kp;
		pGainNow->f16KiTs = pGainIni->f16KiTs;
		pGainNow->f16KdFs = pGainIni->f16KdFs;

		u8GainChangedFlg = 0;
	}

	return u8GainChangedFlg;
}

void MCC_vInitPid(mcc_pid_t *pPid)
{
	pPid->s32Up = 0;
	pPid->s32Ui = 0;
	pPid->s32Ud = 0;
	pPid->s16Uout = 0;
	pPid->s32Err = 0;
	pPid->s32ErrOld = 0;
	pPid->s32ErrUi = 0;
	pPid->s32Ucalc = 0;
	pPid->s32Udiff = 0;
}

int16_t Mcc_s16PIDController(int32_t xRef, int32_t x, mcc_pid_t *pParam)
{
	static int32_t s32Tmp0;
	static int32_t s32Tmp1;
	static int32_t s32Tmp2;

	/* calculate err */
	pParam->s32ErrOld = pParam->s32Err;
	pParam->s32Err = xRef - x;
	if (pParam->s32Err > ASW_S16_MAX)
	{
		pParam->s32Err = ASW_S16_MAX;
	}
	else if (pParam->s32Err < ASW_S16_MIN)
	{
		pParam->s32Err = ASW_S16_MIN;
	}
	else
	{
		;
	}

	/* calculate Up */
	pParam->s32Up = (int32_t)pParam->s16Kp*pParam->s32Err + pParam->f16Kp*pParam->s32Err/MCC_F16_1;
	if (pParam->s32Up > pParam->s16UpMax)
	{
		pParam->s32Up = (int32_t)pParam->s16UpMax;
	}
	else if (pParam->s32Up < pParam->s16UpMin)
	{
		pParam->s32Up = (int32_t)pParam->s16UpMin;
	}
	else
	{
		;
	}
	/* calculate Up */

	/* back-calculation */
	pParam->s32Udiff = (int32_t)pParam->s16Uout - pParam->s32Ucalc;
	if (pParam->s32Udiff > ASW_S16_MAX)
	{
		pParam->s32Udiff = ASW_S16_MAX;
	}
	else if (pParam->s32Udiff < ASW_S16_MIN)
	{
		pParam->s32Udiff = ASW_S16_MIN;
	}
	else
	{
		;
	}

	s32Tmp0 = (int32_t)pParam->s16Kp*MCC_F16_1 + pParam->f16Kp;
	if ((s32Tmp0 != 0) && (pParam->s32Udiff != 0))
	{
		pParam->s32ErrUi = pParam->s32Err + (pParam->s32Udiff*MCC_F16_1)/s32Tmp0;
		if (pParam->s32ErrUi > ASW_S16_MAX)
		{
			pParam->s32ErrUi = ASW_S16_MAX;
		}
		else if (pParam->s32ErrUi < ASW_S16_MIN)
		{
			pParam->s32ErrUi = ASW_S16_MIN;
		}
		else
		{
			;
		}
	}
	else
	{
		pParam->s32ErrUi = pParam->s32Err;
	}
	/* back-calculation */

	/* calculate Ui */
	s32Tmp0 = (int32_t)pParam->s16KiTs*pParam->s32ErrUi;
	s32Tmp2 = (int32_t)pParam->f16KiTs*pParam->s32ErrUi;
	s32Tmp1 = s32Tmp2/MCC_F16_1;
	if ((s32Tmp2 != 0) && (s32Tmp1 == 0))
	{
		if (s32Tmp2 > 0)
		{
			pParam->s32Ui = pParam->s32Ui + s32Tmp0 + 1;
		}
		else if (s32Tmp2 < 0)
		{
			pParam->s32Ui = pParam->s32Ui + s32Tmp0 - 1;
		}
		else
		{
			pParam->s32Ui = pParam->s32Ui + s32Tmp0;
		}
	}
	else
	{
		pParam->s32Ui = pParam->s32Ui + s32Tmp0 + s32Tmp1;
	}

	if (pParam->s32Ui > pParam->s16UiMax)
	{
		pParam->s32Ui = (int32_t)pParam->s16UiMax;
	}
	else if (pParam->s32Ui < pParam->s16UiMin)
	{
		pParam->s32Ui = (int32_t)pParam->s16UiMin;
	}
	else
	{
		;
	}
	/* calculate Ui */

	/* calculate Ud */
	s32Tmp0 = pParam->s32Err - pParam->s32ErrOld;
	if (s32Tmp0 > ASW_S16_MAX)
	{
		s32Tmp0 = ASW_S16_MAX;
	}
	else if (s32Tmp0 < ASW_S16_MIN)
	{
		s32Tmp0 = ASW_S16_MIN;
	}
	else
	{
		;
	}
	pParam->s32Ud = pParam->s16KdFs*s32Tmp0 + pParam->f16KdFs*s32Tmp0/MCC_F16_1;
	if (pParam->s32Ud > pParam->s16UdMax)
	{
		pParam->s32Ud = (int32_t)pParam->s16UdMax;
	}
	else if (pParam->s32Ud < pParam->s16UdMin)
	{
		pParam->s32Ud = (int32_t)pParam->s16UdMin;
	}
	else
	{
		;
	}
	/* calculate Ud */

	/* constrain DeltaUout */
	pParam->s32Ucalc = pParam->s32Up + pParam->s32Ui + pParam->s32Ud;
	if (pParam->s32Ucalc > pParam->s16UoutMax)		/* calc s16Uout */
    {
		pParam->s16Uout = pParam->s16UoutMax;
    }
    else if (pParam->s32Ucalc < pParam->s16UoutMin)
    {
    	pParam->s16Uout = pParam->s16UoutMin;
    }
    else
    {
    	pParam->s16Uout = (int16_t)pParam->s32Ucalc;
    }
	/* constrain DeltaUout */

    return pParam->s16Uout;
}


const static Frac16 sin0To90DegArray[901] = {
		0,57,114,171,228,285,343,400,457,514,
		571,629,686,743,800,857,914,972,1029,1086,
		1143,1200,1257,1315,1372,1429,1486,1543,1600,1657,
		1714,1772,1829,1886,1943,2000,2057,2114,2171,2228,
		2285,2342,2399,2456,2513,2570,2627,2684,2741,2798,
		2855,2912,2969,3026,3083,3140,3197,3254,3311,3368,
		3425,3482,3538,3595,3652,3709,3766,3823,3879,3936,
		3993,4050,4106,4163,4220,4277,4333,4390,4447,4503,
		4560,4617,4673,4730,4786,4843,4899,4956,5013,5069,
		5126,5182,5238,5295,5351,5408,5464,5521,5577,5633,
		5690,5746,5802,5858,5915,5971,6027,6083,6140,6196,
		6252,6308,6364,6420,6476,6532,6588,6644,6700,6756,
		6812,6868,6924,6980,7036,7092,7148,7203,7259,7315,
		7371,7426,7482,7538,7593,7649,7705,7760,7816,7871,
		7927,7982,8038,8093,8149,8204,8259,8315,8370,8425,
		8480,8536,8591,8646,8701,8756,8811,8867,8922,8977,
		9032,9087,9141,9196,9251,9306,9361,9416,9470,9525,
		9580,9635,9689,9744,9798,9853,9908,9962,10017,10071,
		10125,10180,10234,10288,10343,10397,10451,10505,10560,10614,
		10668,10722,10776,10830,10884,10938,10992,11045,11099,11153,
		11207,11261,11314,11368,11422,11475,11529,11582,11636,11689,
		11743,11796,11849,11903,11956,12009,12062,12115,12168,12222,
		12275,12328,12381,12434,12486,12539,12592,12645,12698,12750,
		12803,12856,12908,12961,13013,13066,13118,13171,13223,13275,
		13327,13380,13432,13484,13536,13588,13640,13692,13744,13796,
		13848,13900,13951,14003,14055,14106,14158,14210,14261,14313,
		14364,14415,14467,14518,14569,14621,14672,14723,14774,14825,
		14876,14927,14978,15029,15079,15130,15181,15231,15282,15333,
		15383,15434,15484,15534,15585,15635,15685,15735,15786,15836,
		15886,15936,15986,16036,16085,16135,16185,16235,16284,16334,
		16383,16433,16482,16532,16581,16631,16680,16729,16778,16827,
		16876,16925,16974,17023,17072,17121,17169,17218,17267,17315,
		17364,17412,17461,17509,17557,17606,17654,17702,17750,17798,
		17846,17894,17942,17990,18038,18085,18133,18181,18228,18276,
		18323,18371,18418,18465,18512,18559,18607,18654,18701,18748,
		18794,18841,18888,18935,18981,19028,19075,19121,19167,19214,
		19260,19306,19352,19399,19445,19491,19537,19582,19628,19674,
		19720,19765,19811,19857,19902,19947,19993,20038,20083,20128,
		20173,20219,20264,20308,20353,20398,20443,20487,20532,20577,
		20621,20665,20710,20754,20798,20843,20887,20931,20975,21019,
		21062,21106,21150,21194,21237,21281,21324,21367,21411,21454,
		21497,21540,21583,21626,21669,21712,21755,21798,21840,21883,
		21926,21968,22010,22053,22095,22137,22179,22221,22263,22305,
		22347,22389,22431,22472,22514,22556,22597,22638,22680,22721,
		22762,22803,22844,22885,22926,22967,23008,23048,23089,23129,
		23170,23210,23251,23291,23331,23371,23411,23451,23491,23531,
		23571,23611,23650,23690,23729,23769,23808,23847,23886,23925,
		23964,24003,24042,24081,24120,24159,24197,24236,24274,24313,
		24351,24389,24427,24465,24503,24541,24579,24617,24655,24692,
		24730,24767,24805,24842,24879,24916,24954,24991,25028,25064,
		25101,25138,25175,25211,25248,25284,25320,25357,25393,25429,
		25465,25501,25537,25573,25608,25644,25680,25715,25750,25786,
		25821,25856,25891,25926,25961,25996,26031,26066,26100,26135,
		26169,26204,26238,26272,26306,26340,26374,26408,26442,26476,
		26509,26543,26576,26610,26643,26676,26710,26743,26776,26809,
		26841,26874,26907,26940,26972,27004,27037,27069,27101,27133,
		27165,27197,27229,27261,27293,27324,27356,27387,27419,27450,
		27481,27512,27543,27574,27605,27636,27666,27697,27728,27758,
		27788,27819,27849,27879,27909,27939,27969,27998,28028,28058,
		28087,28117,28146,28175,28204,28233,28262,28291,28320,28349,
		28377,28406,28434,28463,28491,28519,28547,28575,28603,28631,
		28659,28687,28714,28742,28769,28797,28824,28851,28878,28905,
		28932,28959,28985,29012,29039,29065,29091,29118,29144,29170,
		29196,29222,29248,29273,29299,29325,29350,29376,29401,29426,
		29451,29476,29501,29526,29551,29575,29600,29624,29649,29673,
		29697,29722,29746,29769,29793,29817,29841,29864,29888,29911,
		29935,29958,29981,30004,30027,30050,30072,30095,30118,30140,
		30163,30185,30207,30229,30251,30273,30295,30317,30338,30360,
		30381,30403,30424,30445,30466,30487,30508,30529,30550,30571,
		30591,30612,30632,30652,30672,30692,30712,30732,30752,30772,
		30791,30811,30830,30850,30869,30888,30907,30926,30945,30964,
		30982,31001,31019,31038,31056,31074,31092,31110,31128,31146,
		31164,31181,31199,31216,31234,31251,31268,31285,31302,31319,
		31336,31352,31369,31385,31402,31418,31434,31450,31466,31482,
		31498,31514,31529,31545,31560,31576,31591,31606,31621,31636,
		31651,31666,31680,31695,31709,31724,31738,31752,31766,31780,
		31794,31808,31822,31835,31849,31862,31875,31889,31902,31915,
		31928,31940,31953,31966,31978,31991,32003,32015,32027,32040,
		32051,32063,32075,32087,32098,32110,32121,32132,32143,32154,
		32165,32176,32187,32198,32208,32219,32229,32239,32250,32260,
		32270,32280,32289,32299,32309,32318,32327,32337,32346,32355,
		32364,32373,32382,32390,32399,32408,32416,32424,32432,32441,
		32449,32457,32464,32472,32480,32487,32495,32502,32509,32516,
		32523,32530,32537,32544,32550,32557,32563,32570,32576,32582,
		32588,32594,32600,32605,32611,32617,32622,32627,32633,32638,
		32643,32648,32653,32657,32662,32666,32671,32675,32680,32684,
		32688,32692,32695,32699,32703,32706,32710,32713,32716,32720,
		32723,32726,32728,32731,32734,32736,32739,32741,32743,32745,
		32748,32749,32751,32753,32755,32756,32758,32759,32760,32761,
		32763,32763,32764,32765,32766,32766,32767,32767,32767,32767,
		32767
};

Frac16 LAMTR_f16Sin(int16_t x)
{
	Frac16 y = 0;
	int16_t theta = 0;

	if ((x >= 0) && (x <= 900))
	{
		theta = x;
		y = sin0To90DegArray[theta];
	}
	else if ((x > 900) && (x <= 1800))
	{
		theta = 1800 - x;
		y = sin0To90DegArray[theta];
	}
	else if ((x > 1800) && (x <= 2700))
	{
		theta = x - 1800;
		y = -sin0To90DegArray[theta];
	}
	else if ((x > 2700) && (x <= 3600))
	{
		theta = 3600 - x;
		y = -sin0To90DegArray[theta];
	}
	else
	{
		theta = 0;
		y = sin0To90DegArray[theta];
	}

	return y;
}

Frac16 LAMTR_f16Cos(int16_t x)
{
	Frac16 y = 0;
	int16_t theta = 0;

	if ((x >= 0) && (x <= 900))
	{
		theta = 900 - x;
		y = sin0To90DegArray[theta];
	}
	else if ((x > 900) && (x <= 1800))
	{
		theta = x - 900;
		y = -sin0To90DegArray[theta];
	}
	else if ((x > 1800) && (x <= 2700))
	{
		theta = 2700 - x;
		y = -sin0To90DegArray[theta];
	}
	else if ((x > 2700) && (x <= 3600))
	{
		theta = x - 2700;
		y = sin0To90DegArray[theta];
	}
	else
	{
		theta = 0;
		y = sin0To90DegArray[theta];
	}

	return y;
}
