Mcc_1msCtrlCircPFild1msInfo = Simulink.Bus;
DeList={
    'PrimCircPFild1ms'
    'SecdCircPFild1ms'
    };
Mcc_1msCtrlCircPFild1msInfo = CreateBus(Mcc_1msCtrlCircPFild1msInfo, DeList);
clear DeList;

Mcc_1msCtrlPistPFild1msInfo = Simulink.Bus;
DeList={
    'PistPFild1ms'
    };
Mcc_1msCtrlPistPFild1msInfo = CreateBus(Mcc_1msCtrlPistPFild1msInfo, DeList);
clear DeList;

Mcc_1msCtrlMotRotgAgSigInfo = Simulink.Bus;
DeList={
    'StkPosnMeasd'
    'MotMechAngleSpdFild'
    };
Mcc_1msCtrlMotRotgAgSigInfo = CreateBus(Mcc_1msCtrlMotRotgAgSigInfo, DeList);
clear DeList;

Mcc_1msCtrlIdbVlvActInfo = Simulink.Bus;
DeList={
    'FadeoutSt2EndOk'
    };
Mcc_1msCtrlIdbVlvActInfo = CreateBus(Mcc_1msCtrlIdbVlvActInfo, DeList);
clear DeList;

Mcc_1msCtrlStkRecvryActnIfInfo = Simulink.Bus;
DeList={
    'StkRecvryCtrlState'
    'StkRecvryRequestedTime'
    };
Mcc_1msCtrlStkRecvryActnIfInfo = CreateBus(Mcc_1msCtrlStkRecvryActnIfInfo, DeList);
clear DeList;

Mcc_1msCtrlEcuModeSts = Simulink.Bus;
DeList={'Mcc_1msCtrlEcuModeSts'};
Mcc_1msCtrlEcuModeSts = CreateBus(Mcc_1msCtrlEcuModeSts, DeList);
clear DeList;

Mcc_1msCtrlFuncInhibitMccSts = Simulink.Bus;
DeList={'Mcc_1msCtrlFuncInhibitMccSts'};
Mcc_1msCtrlFuncInhibitMccSts = CreateBus(Mcc_1msCtrlFuncInhibitMccSts, DeList);
clear DeList;

Mcc_1msCtrlTgtDeltaStkType = Simulink.Bus;
DeList={'Mcc_1msCtrlTgtDeltaStkType'};
Mcc_1msCtrlTgtDeltaStkType = CreateBus(Mcc_1msCtrlTgtDeltaStkType, DeList);
clear DeList;

Mcc_1msCtrlPCtrlBoostMod = Simulink.Bus;
DeList={'Mcc_1msCtrlPCtrlBoostMod'};
Mcc_1msCtrlPCtrlBoostMod = CreateBus(Mcc_1msCtrlPCtrlBoostMod, DeList);
clear DeList;

Mcc_1msCtrlPCtrlFadeoutSt = Simulink.Bus;
DeList={'Mcc_1msCtrlPCtrlFadeoutSt'};
Mcc_1msCtrlPCtrlFadeoutSt = CreateBus(Mcc_1msCtrlPCtrlFadeoutSt, DeList);
clear DeList;

Mcc_1msCtrlPCtrlSt = Simulink.Bus;
DeList={'Mcc_1msCtrlPCtrlSt'};
Mcc_1msCtrlPCtrlSt = CreateBus(Mcc_1msCtrlPCtrlSt, DeList);
clear DeList;

Mcc_1msCtrlInVlvAllCloseReq = Simulink.Bus;
DeList={'Mcc_1msCtrlInVlvAllCloseReq'};
Mcc_1msCtrlInVlvAllCloseReq = CreateBus(Mcc_1msCtrlInVlvAllCloseReq, DeList);
clear DeList;

Mcc_1msCtrlPChamberVolume = Simulink.Bus;
DeList={'Mcc_1msCtrlPChamberVolume'};
Mcc_1msCtrlPChamberVolume = CreateBus(Mcc_1msCtrlPChamberVolume, DeList);
clear DeList;

Mcc_1msCtrlTarDeltaStk = Simulink.Bus;
DeList={'Mcc_1msCtrlTarDeltaStk'};
Mcc_1msCtrlTarDeltaStk = CreateBus(Mcc_1msCtrlTarDeltaStk, DeList);
clear DeList;

Mcc_1msCtrlFinalTarPFromPCtrl = Simulink.Bus;
DeList={'Mcc_1msCtrlFinalTarPFromPCtrl'};
Mcc_1msCtrlFinalTarPFromPCtrl = CreateBus(Mcc_1msCtrlFinalTarPFromPCtrl, DeList);
clear DeList;

Mcc_1msCtrlMotDqIRefMccInfo = Simulink.Bus;
DeList={
    'IdRef'
    'IqRef'
    };
Mcc_1msCtrlMotDqIRefMccInfo = CreateBus(Mcc_1msCtrlMotDqIRefMccInfo, DeList);
clear DeList;

Mcc_1msCtrlIdbMotPosnInfo = Simulink.Bus;
DeList={
    'MotTarPosn'
    };
Mcc_1msCtrlIdbMotPosnInfo = CreateBus(Mcc_1msCtrlIdbMotPosnInfo, DeList);
clear DeList;

Mcc_1msCtrlFluxWeakengStInfo = Simulink.Bus;
DeList={
    'FluxWeakengFlg'
    'FluxWeakengFlgOld'
    };
Mcc_1msCtrlFluxWeakengStInfo = CreateBus(Mcc_1msCtrlFluxWeakengStInfo, DeList);
clear DeList;

Mcc_1msCtrlMotCtrlMode = Simulink.Bus;
DeList={'Mcc_1msCtrlMotCtrlMode'};
Mcc_1msCtrlMotCtrlMode = CreateBus(Mcc_1msCtrlMotCtrlMode, DeList);
clear DeList;

Mcc_1msCtrlMotCtrlState = Simulink.Bus;
DeList={'Mcc_1msCtrlMotCtrlState'};
Mcc_1msCtrlMotCtrlState = CreateBus(Mcc_1msCtrlMotCtrlState, DeList);
clear DeList;

Mcc_1msCtrlMotICtrlFadeOutState = Simulink.Bus;
DeList={'Mcc_1msCtrlMotICtrlFadeOutState'};
Mcc_1msCtrlMotICtrlFadeOutState = CreateBus(Mcc_1msCtrlMotICtrlFadeOutState, DeList);
clear DeList;

Mcc_1msCtrlStkRecvryStabnEndOK = Simulink.Bus;
DeList={'Mcc_1msCtrlStkRecvryStabnEndOK'};
Mcc_1msCtrlStkRecvryStabnEndOK = CreateBus(Mcc_1msCtrlStkRecvryStabnEndOK, DeList);
clear DeList;

