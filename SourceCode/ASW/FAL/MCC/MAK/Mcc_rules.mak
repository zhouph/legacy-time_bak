# \file
#
# \brief Mcc
#
# This file contains the implementation of the SWC
# module Mcc.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Mcc_src

Mcc_src_FILES        += $(Mcc_SRC_PATH)\Mcc_1msCtrl.c
Mcc_src_FILES        += $(Mcc_IFA_PATH)\Mcc_1msCtrl_Ifa.c
Mcc_src_FILES        += $(Mcc_SRC_PATH)\LAMTR_CalcCurrentRef.c
Mcc_src_FILES        += $(Mcc_SRC_PATH)\LAMTR_DecideMtrCtrlMode.c

Mcc_src_FILES        += $(Mcc_SRC_PATH)\mclib_functions.c
Mcc_src_FILES        += $(Mcc_CFG_PATH)\Mcc_Cfg.c
Mcc_src_FILES        += $(Mcc_CAL_PATH)\Mcc_Cal.c

ifeq ($(ICE_COMPILE),true)
Mcc_src_FILES        += $(Mcc_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	Mcc_src_FILES        += $(Mcc_UNITY_PATH)\unity.c
	Mcc_src_FILES        += $(Mcc_UNITY_PATH)\unity_fixture.c	
	Mcc_src_FILES        += $(Mcc_UT_PATH)\main.c
	Mcc_src_FILES        += $(Mcc_UT_PATH)\Mcc_1msCtrl\Mcc_1msCtrl_UtMain.c
endif


# /* HSH */
ifeq ($(ICE_COMPILE), true)

# Library
Mcc_src_FILES  += $(Mcc_CORE_PATH)\ICE\CMN\LIB\SwcCommonFunction.c
Mcc_src_FILES  += $(Mcc_CORE_PATH)\ICE\CMN\LIB\L_CommonFunction.c
Mcc_src_FILES  += $(Mcc_CORE_PATH)\ICE\CMN\FBL_HDR\FBL_HMC\Fbl_Header.c
Mcc_src_FILES  += $(Mcc_CORE_PATH)\ICE\CMN\FBL_HDR\FBL_HMC\Fbl_Mtab.c

endif