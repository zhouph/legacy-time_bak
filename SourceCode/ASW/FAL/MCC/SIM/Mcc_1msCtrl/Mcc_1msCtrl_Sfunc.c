#define S_FUNCTION_NAME      Mcc_1msCtrl_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          18
#define WidthOutputPort         9

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Mcc_1msCtrl.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Mcc_1msCtrlCircPFild1msInfo.PrimCircPFild1ms = input[0];
    Mcc_1msCtrlCircPFild1msInfo.SecdCircPFild1ms = input[1];
    Mcc_1msCtrlPistPFild1msInfo.PistPFild1ms = input[2];
    Mcc_1msCtrlMotRotgAgSigInfo.StkPosnMeasd = input[3];
    Mcc_1msCtrlMotRotgAgSigInfo.MotMechAngleSpdFild = input[4];
    Mcc_1msCtrlIdbVlvActInfo.FadeoutSt2EndOk = input[5];
    Mcc_1msCtrlStkRecvryActnIfInfo.StkRecvryCtrlState = input[6];
    Mcc_1msCtrlStkRecvryActnIfInfo.StkRecvryRequestedTime = input[7];
    Mcc_1msCtrlEcuModeSts = input[8];
    Mcc_1msCtrlFuncInhibitMccSts = input[9];
    Mcc_1msCtrlTgtDeltaStkType = input[10];
    Mcc_1msCtrlPCtrlBoostMod = input[11];
    Mcc_1msCtrlPCtrlFadeoutSt = input[12];
    Mcc_1msCtrlPCtrlSt = input[13];
    Mcc_1msCtrlInVlvAllCloseReq = input[14];
    Mcc_1msCtrlPChamberVolume = input[15];
    Mcc_1msCtrlTarDeltaStk = input[16];
    Mcc_1msCtrlFinalTarPFromPCtrl = input[17];

    Mcc_1msCtrl();


    output[0] = Mcc_1msCtrlMotDqIRefMccInfo.IdRef;
    output[1] = Mcc_1msCtrlMotDqIRefMccInfo.IqRef;
    output[2] = Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn;
    output[3] = Mcc_1msCtrlFluxWeakengStInfo.FluxWeakengFlg;
    output[4] = Mcc_1msCtrlFluxWeakengStInfo.FluxWeakengFlgOld;
    output[5] = Mcc_1msCtrlMotCtrlMode;
    output[6] = Mcc_1msCtrlMotCtrlState;
    output[7] = Mcc_1msCtrlMotICtrlFadeOutState;
    output[8] = Mcc_1msCtrlStkRecvryStabnEndOK;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Mcc_1msCtrl_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
