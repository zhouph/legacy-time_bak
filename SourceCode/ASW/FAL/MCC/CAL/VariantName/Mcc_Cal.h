/**
 * @defgroup Mcc_Cal Mcc_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mcc_Cal.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MCC_CAL_H_
#define MCC_CAL_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Mcc_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
   int16_t  S16_MTR_TARGET_PRES_01 ;
   int16_t  S16_MTR_TARGET_PRES_02 ;
   int16_t  S16_MTR_TARGET_PRES_03 ;
   int16_t  S16_MTR_TARGET_PRES_04 ;
   int16_t  S16_MTR_TARGET_PRES_05 ;
   int16_t  S16_MTR_TARGET_PRES_06 ;
   int16_t  S16_MTR_TARGET_PRES_07 ;
   int16_t  S16_MTR_TARGET_PRES_08 ;
   int16_t  S16_MTR_TARGET_PRES_09 ;
   int16_t  S16_MTR_TARGET_PRES_10 ;
   int16_t  S16_MTR_TARGET_PRES_11 ;
   int16_t  S16_MTR_TARGET_PRES_12 ;
   int16_t  S16_MTR_TARGET_PRES_13 ;
   int16_t  S16_MTR_TARGET_PRES_14 ;
   int16_t  S16_MTR_TARGET_PRES_MAX;


} MotICtrlTarPMapCal_t;

typedef struct
{
	int16_t S16_MTR_IQREFBASE_FW_01 ;
	int16_t S16_MTR_IQREFBASE_FW_02 ;
	int16_t S16_MTR_IQREFBASE_FW_03 ;
	int16_t S16_MTR_IQREFBASE_FW_04 ;
	int16_t S16_MTR_IQREFBASE_FW_05 ;
	int16_t S16_MTR_IQREFBASE_FW_06 ;
	int16_t S16_MTR_IQREFBASE_FW_07 ;
	int16_t S16_MTR_IQREFBASE_FW_08 ;
	int16_t S16_MTR_IQREFBASE_FW_09 ;
	int16_t S16_MTR_IQREFBASE_FW_10 ;
	int16_t S16_MTR_IQREFBASE_FW_11 ;
	int16_t S16_MTR_IQREFBASE_FW_12 ;
	int16_t S16_MTR_IQREFBASE_FW_13 ;
	int16_t S16_MTR_IQREFBASE_FW_14 ;
	int16_t S16_MTR_IQREFBASE_FW_MAX;

	int16_t S16_MTR_IQREFBASE_BW_01 ;
	int16_t S16_MTR_IQREFBASE_BW_02	;
	int16_t S16_MTR_IQREFBASE_BW_03	;
	int16_t S16_MTR_IQREFBASE_BW_04	;
	int16_t S16_MTR_IQREFBASE_BW_05	;
	int16_t S16_MTR_IQREFBASE_BW_06	;
	int16_t S16_MTR_IQREFBASE_BW_07 ;
	int16_t S16_MTR_IQREFBASE_BW_08 ;
	int16_t S16_MTR_IQREFBASE_BW_09 ;
	int16_t S16_MTR_IQREFBASE_BW_10	;
	int16_t S16_MTR_IQREFBASE_BW_11 ;
	int16_t S16_MTR_IQREFBASE_BW_12 ;
	int16_t S16_MTR_IQREFBASE_BW_13 ;
	int16_t S16_MTR_IQREFBASE_BW_14 ;
	int16_t S16_MTR_IQREFBASE_BW_MAX;
}MotICtrlTqMapCal_t;


typedef struct
{
    const MotICtrlTarPMapCal_t *               apMotICtrlTarPMapCal;
    const MotICtrlTqMapCal_t *                 apMotICtrlTqMapCal;

} MotorCurrentControl_CalibType;


// moved to cal
#define S16_MTR_TARGET_PRES_01	MTRCurCTRLcalib.apMotICtrlTarPMapCal->S16_MTR_TARGET_PRES_01
#define S16_MTR_TARGET_PRES_02	MTRCurCTRLcalib.apMotICtrlTarPMapCal->S16_MTR_TARGET_PRES_02
#define S16_MTR_TARGET_PRES_03	MTRCurCTRLcalib.apMotICtrlTarPMapCal->S16_MTR_TARGET_PRES_03
#define S16_MTR_TARGET_PRES_04	MTRCurCTRLcalib.apMotICtrlTarPMapCal->S16_MTR_TARGET_PRES_04
#define S16_MTR_TARGET_PRES_05	MTRCurCTRLcalib.apMotICtrlTarPMapCal->S16_MTR_TARGET_PRES_05
#define S16_MTR_TARGET_PRES_06	MTRCurCTRLcalib.apMotICtrlTarPMapCal->S16_MTR_TARGET_PRES_06
#define S16_MTR_TARGET_PRES_07	MTRCurCTRLcalib.apMotICtrlTarPMapCal->S16_MTR_TARGET_PRES_07
#define S16_MTR_TARGET_PRES_08	MTRCurCTRLcalib.apMotICtrlTarPMapCal->S16_MTR_TARGET_PRES_08
#define S16_MTR_TARGET_PRES_09	MTRCurCTRLcalib.apMotICtrlTarPMapCal->S16_MTR_TARGET_PRES_09
#define S16_MTR_TARGET_PRES_10	MTRCurCTRLcalib.apMotICtrlTarPMapCal->S16_MTR_TARGET_PRES_10
#define S16_MTR_TARGET_PRES_11	MTRCurCTRLcalib.apMotICtrlTarPMapCal->S16_MTR_TARGET_PRES_11
#define S16_MTR_TARGET_PRES_12	MTRCurCTRLcalib.apMotICtrlTarPMapCal->S16_MTR_TARGET_PRES_12
#define S16_MTR_TARGET_PRES_13	MTRCurCTRLcalib.apMotICtrlTarPMapCal->S16_MTR_TARGET_PRES_13
#define S16_MTR_TARGET_PRES_14	MTRCurCTRLcalib.apMotICtrlTarPMapCal->S16_MTR_TARGET_PRES_14
#define S16_MTR_TARGET_PRES_MAX	MTRCurCTRLcalib.apMotICtrlTarPMapCal->S16_MTR_TARGET_PRES_MAX


#define S16_MTR_IQREFBASE_FW_01	MTRCurCTRLcalib.apMotICtrlTqMapCal->S16_MTR_IQREFBASE_FW_01
#define S16_MTR_IQREFBASE_FW_02 MTRCurCTRLcalib.apMotICtrlTqMapCal->S16_MTR_IQREFBASE_FW_02
#define S16_MTR_IQREFBASE_FW_03 MTRCurCTRLcalib.apMotICtrlTqMapCal->S16_MTR_IQREFBASE_FW_03
#define S16_MTR_IQREFBASE_FW_04 MTRCurCTRLcalib.apMotICtrlTqMapCal->S16_MTR_IQREFBASE_FW_04
#define S16_MTR_IQREFBASE_FW_05 MTRCurCTRLcalib.apMotICtrlTqMapCal->S16_MTR_IQREFBASE_FW_05
#define S16_MTR_IQREFBASE_FW_06 MTRCurCTRLcalib.apMotICtrlTqMapCal->S16_MTR_IQREFBASE_FW_06
#define S16_MTR_IQREFBASE_FW_07 MTRCurCTRLcalib.apMotICtrlTqMapCal->S16_MTR_IQREFBASE_FW_07
#define S16_MTR_IQREFBASE_FW_08 MTRCurCTRLcalib.apMotICtrlTqMapCal->S16_MTR_IQREFBASE_FW_08
#define S16_MTR_IQREFBASE_FW_09 MTRCurCTRLcalib.apMotICtrlTqMapCal->S16_MTR_IQREFBASE_FW_09
#define S16_MTR_IQREFBASE_FW_10 MTRCurCTRLcalib.apMotICtrlTqMapCal->S16_MTR_IQREFBASE_FW_10
#define S16_MTR_IQREFBASE_FW_11 MTRCurCTRLcalib.apMotICtrlTqMapCal->S16_MTR_IQREFBASE_FW_11
#define S16_MTR_IQREFBASE_FW_12 MTRCurCTRLcalib.apMotICtrlTqMapCal->S16_MTR_IQREFBASE_FW_12
#define S16_MTR_IQREFBASE_FW_13 MTRCurCTRLcalib.apMotICtrlTqMapCal->S16_MTR_IQREFBASE_FW_13
#define S16_MTR_IQREFBASE_FW_14 MTRCurCTRLcalib.apMotICtrlTqMapCal->S16_MTR_IQREFBASE_FW_14
#define S16_MTR_IQREFBASE_FW_MAX MTRCurCTRLcalib.apMotICtrlTqMapCal->S16_MTR_IQREFBASE_FW_MAX

#define S16_MTR_IQREFBASE_BW_01  MTRCurCTRLcalib.apMotICtrlTqMapCal->S16_MTR_IQREFBASE_BW_01
#define S16_MTR_IQREFBASE_BW_02  MTRCurCTRLcalib.apMotICtrlTqMapCal->S16_MTR_IQREFBASE_BW_02
#define S16_MTR_IQREFBASE_BW_03  MTRCurCTRLcalib.apMotICtrlTqMapCal->S16_MTR_IQREFBASE_BW_03
#define S16_MTR_IQREFBASE_BW_04  MTRCurCTRLcalib.apMotICtrlTqMapCal->S16_MTR_IQREFBASE_BW_04
#define S16_MTR_IQREFBASE_BW_05  MTRCurCTRLcalib.apMotICtrlTqMapCal->S16_MTR_IQREFBASE_BW_05
#define S16_MTR_IQREFBASE_BW_06  MTRCurCTRLcalib.apMotICtrlTqMapCal->S16_MTR_IQREFBASE_BW_06
#define S16_MTR_IQREFBASE_BW_07  MTRCurCTRLcalib.apMotICtrlTqMapCal->S16_MTR_IQREFBASE_BW_07
#define S16_MTR_IQREFBASE_BW_08  MTRCurCTRLcalib.apMotICtrlTqMapCal->S16_MTR_IQREFBASE_BW_08
#define S16_MTR_IQREFBASE_BW_09  MTRCurCTRLcalib.apMotICtrlTqMapCal->S16_MTR_IQREFBASE_BW_09
#define S16_MTR_IQREFBASE_BW_10  MTRCurCTRLcalib.apMotICtrlTqMapCal->S16_MTR_IQREFBASE_BW_10
#define S16_MTR_IQREFBASE_BW_11  MTRCurCTRLcalib.apMotICtrlTqMapCal->S16_MTR_IQREFBASE_BW_11
#define S16_MTR_IQREFBASE_BW_12  MTRCurCTRLcalib.apMotICtrlTqMapCal->S16_MTR_IQREFBASE_BW_12
#define S16_MTR_IQREFBASE_BW_13  MTRCurCTRLcalib.apMotICtrlTqMapCal->S16_MTR_IQREFBASE_BW_13
#define S16_MTR_IQREFBASE_BW_14  MTRCurCTRLcalib.apMotICtrlTqMapCal->S16_MTR_IQREFBASE_BW_14
#define S16_MTR_IQREFBASE_BW_MAX MTRCurCTRLcalib.apMotICtrlTqMapCal->S16_MTR_IQREFBASE_BW_MAX
/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
extern MotorCurrentControl_CalibType MTRCurCTRLcalib;
/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MCC_CAL_H_ */
/** @} */
