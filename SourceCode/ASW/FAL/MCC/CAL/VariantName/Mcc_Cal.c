/**
 * @defgroup Mcc_Cal Mcc_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mcc_Cal.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Mcc_Cal.h"
#include "Mcc_Cfg.h"			/*CAL*/

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define MCC_START_SEC_CALIB_UNSPECIFIED
#include "Mcc_MemMap.h"
/* Global Calibration Section */


#define MCC_STOP_SEC_CALIB_UNSPECIFIED
#include "Mcc_MemMap.h"

#define MCC_START_SEC_CONST_UNSPECIFIED
#include "Mcc_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define MCC_STOP_SEC_CONST_UNSPECIFIED
#include "Mcc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MCC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mcc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
MotICtrlTarPMapCal_t MotICtrlTarPMapCal =
{
	/*int32_t  S16_MTR_TARGET_PRES_01 */S16_MPRESS_0_BAR   ,
	/*int32_t  S16_MTR_TARGET_PRES_02 */S16_MPRESS_1_BAR   ,
	/*int32_t  S16_MTR_TARGET_PRES_03 */S16_MPRESS_2_BAR   ,
	/*int32_t  S16_MTR_TARGET_PRES_04 */S16_MPRESS_3_BAR   ,
	/*int32_t  S16_MTR_TARGET_PRES_05 */S16_MPRESS_4_BAR   ,
	/*int32_t  S16_MTR_TARGET_PRES_06 */S16_MPRESS_5_BAR   ,
	/*int32_t  S16_MTR_TARGET_PRES_07 */S16_MPRESS_10_BAR  ,
	/*int32_t  S16_MTR_TARGET_PRES_08 */S16_MPRESS_20_BAR  ,
	/*int32_t  S16_MTR_TARGET_PRES_09 */S16_MPRESS_30_BAR  ,
	/*int32_t  S16_MTR_TARGET_PRES_10 */S16_MPRESS_40_BAR  ,
	/*int32_t  S16_MTR_TARGET_PRES_11 */S16_MPRESS_50_BAR  ,
	/*int32_t  S16_MTR_TARGET_PRES_12 */S16_MPRESS_60_BAR  ,
	/*int32_t  S16_MTR_TARGET_PRES_13 */S16_MPRESS_70_BAR  ,
	/*int32_t  S16_MTR_TARGET_PRES_14 */S16_MPRESS_100_BAR ,
	/*int32_t  S16_MTR_TARGET_PRES_MAX*/S16_MPRESS_150_BAR ,

};

MotICtrlTqMapCal_t MotICtrlTqMapCal_450W =
{
	/*int32_t S16_MTR_IQREFBASE_FW_01 ;*/   (157 ),
	/*int32_t S16_MTR_IQREFBASE_FW_02 ;*/   (206 ),
	/*int32_t S16_MTR_IQREFBASE_FW_03 ;*/   (254 ),
	/*int32_t S16_MTR_IQREFBASE_FW_04 ;*/   (302 ),
	/*int32_t S16_MTR_IQREFBASE_FW_05 ;*/   (349 ),
	/*int32_t S16_MTR_IQREFBASE_FW_06 ;*/   (397 ),
	/*int32_t S16_MTR_IQREFBASE_FW_07 ;*/   (636 ),
	/*int32_t S16_MTR_IQREFBASE_FW_08 ;*/   (1110),
	/*int32_t S16_MTR_IQREFBASE_FW_09 ;*/   (1582),
	/*int32_t S16_MTR_IQREFBASE_FW_10 ;*/   (2053),
	/*int32_t S16_MTR_IQREFBASE_FW_11 ;*/   (2523),
	/*int32_t S16_MTR_IQREFBASE_FW_12 ;*/   (2992),
	/*int32_t S16_MTR_IQREFBASE_FW_13 ;*/   (3461),
	/*int32_t S16_MTR_IQREFBASE_FW_14 ;*/   (4867),
	/*int32_t S16_MTR_IQREFBASE_FW_MAX;*/   (7211),

	/*int32_t S16_MTR_IQREFBASE_BW_01 ;*/    (-138),
	/*int32_t S16_MTR_IQREFBASE_BW_02 ;*/    (-109),
	/*int32_t S16_MTR_IQREFBASE_BW_03 ;*/    (-81 ),
	/*int32_t S16_MTR_IQREFBASE_BW_04 ;*/    (-54 ),
	/*int32_t S16_MTR_IQREFBASE_BW_05 ;*/    (-27 ),
	/*int32_t S16_MTR_IQREFBASE_BW_06 ;*/    (2   ),
	/*int32_t S16_MTR_IQREFBASE_BW_07 ;*/    (139 ),
	/*int32_t S16_MTR_IQREFBASE_BW_08 ;*/    (414 ),
	/*int32_t S16_MTR_IQREFBASE_BW_09 ;*/    (687 ),
	/*int32_t S16_MTR_IQREFBASE_BW_10 ;*/    (960 ),
	/*int32_t S16_MTR_IQREFBASE_BW_11 ;*/    (1232),
	/*int32_t S16_MTR_IQREFBASE_BW_12 ;*/    (1504),
	/*int32_t S16_MTR_IQREFBASE_BW_13 ;*/    (1775),
	/*int32_t S16_MTR_IQREFBASE_BW_14 ;*/    (2589),
	/*int32_t S16_MTR_IQREFBASE_BW_MAX;*/    (3945),
};

MotICtrlTqMapCal_t MotICtrlTqMapCal_600W =
{
	/*int32_t S16_MTR_IQREFBASE_FW_01 ;*/   (271  ),
	/*int32_t S16_MTR_IQREFBASE_FW_02 ;*/   (352  ),
	/*int32_t S16_MTR_IQREFBASE_FW_03 ;*/   (432  ),
	/*int32_t S16_MTR_IQREFBASE_FW_04 ;*/   (510  ),
	/*int32_t S16_MTR_IQREFBASE_FW_05 ;*/   (589  ),
	/*int32_t S16_MTR_IQREFBASE_FW_06 ;*/   (669  ),
	/*int32_t S16_MTR_IQREFBASE_FW_07 ;*/   (1064 ),
	/*int32_t S16_MTR_IQREFBASE_FW_08 ;*/   (1850 ),
	/*int32_t S16_MTR_IQREFBASE_FW_09 ;*/   (2632 ),
	/*int32_t S16_MTR_IQREFBASE_FW_10 ;*/   (3412 ),
	/*int32_t S16_MTR_IQREFBASE_FW_11 ;*/   (4190 ),
	/*int32_t S16_MTR_IQREFBASE_FW_12 ;*/   (4968 ),
	/*int32_t S16_MTR_IQREFBASE_FW_13 ;*/   (5744 ),
	/*int32_t S16_MTR_IQREFBASE_FW_14 ;*/   (8074 ),
	/*int32_t S16_MTR_IQREFBASE_FW_MAX;*/   (11956),

	/*int32_t S16_MTR_IQREFBASE_BW_01 ;*/   (-235) ,
	/*int32_t S16_MTR_IQREFBASE_BW_02 ;*/   (-187) ,
	/*int32_t S16_MTR_IQREFBASE_BW_03 ;*/   (-141) ,
	/*int32_t S16_MTR_IQREFBASE_BW_04 ;*/   (-96 ) ,
	/*int32_t S16_MTR_IQREFBASE_BW_05 ;*/   (-50 ) ,
	/*int32_t S16_MTR_IQREFBASE_BW_06 ;*/   (-4  ) ,
	/*int32_t S16_MTR_IQREFBASE_BW_07 ;*/   (225 ) ,
	/*int32_t S16_MTR_IQREFBASE_BW_08 ;*/   (680 ) ,
	/*int32_t S16_MTR_IQREFBASE_BW_09 ;*/   (1132) ,
	/*int32_t S16_MTR_IQREFBASE_BW_10 ;*/   (1584) ,
	/*int32_t S16_MTR_IQREFBASE_BW_11 ;*/   (2034) ,
	/*int32_t S16_MTR_IQREFBASE_BW_12 ;*/   (2484) ,
	/*int32_t S16_MTR_IQREFBASE_BW_13 ;*/   (2934) ,
	/*int32_t S16_MTR_IQREFBASE_BW_14 ;*/   (4283) ,
	/*int32_t S16_MTR_IQREFBASE_BW_MAX;*/   (6530) ,
};

MotICtrlTqMapCal_t MotICtrlTqMapCal_SWD_AVG =
{
	/*int32_t S16_MTR_IQREFBASE_FW_01 ;*/   (0   ),
	/*int32_t S16_MTR_IQREFBASE_FW_02 ;*/   (41  ),
	/*int32_t S16_MTR_IQREFBASE_FW_03 ;*/   (83  ),
	/*int32_t S16_MTR_IQREFBASE_FW_04 ;*/   (100 ),
	/*int32_t S16_MTR_IQREFBASE_FW_05 ;*/   (163 ),
	/*int32_t S16_MTR_IQREFBASE_FW_06 ;*/   (207 ),
	/*int32_t S16_MTR_IQREFBASE_FW_07 ;*/   (415 ),
	/*int32_t S16_MTR_IQREFBASE_FW_08 ;*/   (817 ),
	/*int32_t S16_MTR_IQREFBASE_FW_09 ;*/   (1054),
	/*int32_t S16_MTR_IQREFBASE_FW_10 ;*/   (1558),
	/*int32_t S16_MTR_IQREFBASE_FW_11 ;*/   (1910),
	/*int32_t S16_MTR_IQREFBASE_FW_12 ;*/   (2205),
	/*int32_t S16_MTR_IQREFBASE_FW_13 ;*/   (2570),
	/*int32_t S16_MTR_IQREFBASE_FW_14 ;*/   (3713),
	/*int32_t S16_MTR_IQREFBASE_FW_MAX;*/   (5528),

	/*int32_t S16_MTR_IQREFBASE_BW_01 ;*/   (0   ) ,
	/*int32_t S16_MTR_IQREFBASE_BW_02 ;*/   (41  ) ,
	/*int32_t S16_MTR_IQREFBASE_BW_03 ;*/   (83  ) ,
	/*int32_t S16_MTR_IQREFBASE_BW_04 ;*/   (100 ) ,
	/*int32_t S16_MTR_IQREFBASE_BW_05 ;*/   (163 ) ,
	/*int32_t S16_MTR_IQREFBASE_BW_06 ;*/   (207 ) ,
	/*int32_t S16_MTR_IQREFBASE_BW_07 ;*/   (415 ) ,
	/*int32_t S16_MTR_IQREFBASE_BW_08 ;*/   (817 ) ,
	/*int32_t S16_MTR_IQREFBASE_BW_09 ;*/   (1054) ,
	/*int32_t S16_MTR_IQREFBASE_BW_10 ;*/   (1558) ,
	/*int32_t S16_MTR_IQREFBASE_BW_11 ;*/   (1910) ,
	/*int32_t S16_MTR_IQREFBASE_BW_12 ;*/   (2205) ,
	/*int32_t S16_MTR_IQREFBASE_BW_13 ;*/   (2570) ,
	/*int32_t S16_MTR_IQREFBASE_BW_14 ;*/   (3713) ,
	/*int32_t S16_MTR_IQREFBASE_BW_MAX;*/   (5528) ,
};

MotorCurrentControl_CalibType MTRCurCTRLcalib =
{
    &MotICtrlTarPMapCal,

#if S16_MTR_BASE_CURR		==	S16_MTR_BASE_CURR_600W
    &MotICtrlTqMapCal_600W,
#elif S16_MTR_BASE_CURR		==	S16_MTR_BASE_CURR_450W
    &MotICtrlTqMapCal_450W,
#elif S16_MTR_BASE_CURR		== S16_MTR_BASE_CURR_SWD_AVG
    &MotICtrlTqMapCal_SWD_AVG,
#endif
};

#define MCC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mcc_MemMap.h"
#define MCC_START_SEC_VAR_NOINIT_32BIT
#include "Mcc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MCC_STOP_SEC_VAR_NOINIT_32BIT
#include "Mcc_MemMap.h"
#define MCC_START_SEC_VAR_UNSPECIFIED
#include "Mcc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MCC_STOP_SEC_VAR_UNSPECIFIED
#include "Mcc_MemMap.h"
#define MCC_START_SEC_VAR_32BIT
#include "Mcc_MemMap.h"
/** Variable Section (32BIT)**/


#define MCC_STOP_SEC_VAR_32BIT
#include "Mcc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MCC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mcc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define MCC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mcc_MemMap.h"
#define MCC_START_SEC_VAR_NOINIT_32BIT
#include "Mcc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MCC_STOP_SEC_VAR_NOINIT_32BIT
#include "Mcc_MemMap.h"
#define MCC_START_SEC_VAR_UNSPECIFIED
#include "Mcc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MCC_STOP_SEC_VAR_UNSPECIFIED
#include "Mcc_MemMap.h"
#define MCC_START_SEC_VAR_32BIT
#include "Mcc_MemMap.h"
/** Variable Section (32BIT)**/


#define MCC_STOP_SEC_VAR_32BIT
#include "Mcc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define MCC_START_SEC_CODE
#include "Mcc_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define MCC_STOP_SEC_CODE
#include "Mcc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
