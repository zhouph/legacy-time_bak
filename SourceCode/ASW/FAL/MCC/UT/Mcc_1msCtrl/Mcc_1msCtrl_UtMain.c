#include "unity.h"
#include "unity_fixture.h"
#include "Mcc_1msCtrl.h"
#include "Mcc_1msCtrl_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Rtesint32 UtInput_Mcc_1msCtrlCircPFild1msInfo_PrimCircPFild1ms[MAX_STEP] = MCC_1MSCTRLCIRCPFILD1MSINFO_PRIMCIRCPFILD1MS;
const Rtesint32 UtInput_Mcc_1msCtrlCircPFild1msInfo_SecdCircPFild1ms[MAX_STEP] = MCC_1MSCTRLCIRCPFILD1MSINFO_SECDCIRCPFILD1MS;
const Rtesint32 UtInput_Mcc_1msCtrlPistPFild1msInfo_PistPFild1ms[MAX_STEP] = MCC_1MSCTRLPISTPFILD1MSINFO_PISTPFILD1MS;
const Salsint32 UtInput_Mcc_1msCtrlMotRotgAgSigInfo_StkPosnMeasd[MAX_STEP] = MCC_1MSCTRLMOTROTGAGSIGINFO_STKPOSNMEASD;
const Salsint32 UtInput_Mcc_1msCtrlMotRotgAgSigInfo_MotMechAngleSpdFild[MAX_STEP] = MCC_1MSCTRLMOTROTGAGSIGINFO_MOTMECHANGLESPDFILD;
const Rteuint8 UtInput_Mcc_1msCtrlIdbVlvActInfo_FadeoutSt2EndOk[MAX_STEP] = MCC_1MSCTRLIDBVLVACTINFO_FADEOUTST2ENDOK;
const Rtesint8 UtInput_Mcc_1msCtrlStkRecvryActnIfInfo_StkRecvryCtrlState[MAX_STEP] = MCC_1MSCTRLSTKRECVRYACTNIFINFO_STKRECVRYCTRLSTATE;
const Rtesint16 UtInput_Mcc_1msCtrlStkRecvryActnIfInfo_StkRecvryRequestedTime[MAX_STEP] = MCC_1MSCTRLSTKRECVRYACTNIFINFO_STKRECVRYREQUESTEDTIME;
const Mom_HndlrEcuModeSts_t UtInput_Mcc_1msCtrlEcuModeSts[MAX_STEP] = MCC_1MSCTRLECUMODESTS;
const Eem_SuspcDetnFuncInhibitMccSts_t UtInput_Mcc_1msCtrlFuncInhibitMccSts[MAX_STEP] = MCC_1MSCTRLFUNCINHIBITMCCSTS;
const Pct_5msCtrlTgtDeltaStkType_t UtInput_Mcc_1msCtrlTgtDeltaStkType[MAX_STEP] = MCC_1MSCTRLTGTDELTASTKTYPE;
const Pct_5msCtrlPCtrlBoostMod_t UtInput_Mcc_1msCtrlPCtrlBoostMod[MAX_STEP] = MCC_1MSCTRLPCTRLBOOSTMOD;
const Pct_5msCtrlPCtrlFadeoutSt_t UtInput_Mcc_1msCtrlPCtrlFadeoutSt[MAX_STEP] = MCC_1MSCTRLPCTRLFADEOUTST;
const Pct_5msCtrlPCtrlSt_t UtInput_Mcc_1msCtrlPCtrlSt[MAX_STEP] = MCC_1MSCTRLPCTRLST;
const Pct_5msCtrlInVlvAllCloseReq_t UtInput_Mcc_1msCtrlInVlvAllCloseReq[MAX_STEP] = MCC_1MSCTRLINVLVALLCLOSEREQ;
const Pct_5msCtrlPChamberVolume_t UtInput_Mcc_1msCtrlPChamberVolume[MAX_STEP] = MCC_1MSCTRLPCHAMBERVOLUME;
const Pct_5msCtrlTarDeltaStk_t UtInput_Mcc_1msCtrlTarDeltaStk[MAX_STEP] = MCC_1MSCTRLTARDELTASTK;
const Pct_5msCtrlFinalTarPFromPCtrl_t UtInput_Mcc_1msCtrlFinalTarPFromPCtrl[MAX_STEP] = MCC_1MSCTRLFINALTARPFROMPCTRL;

const Rtesint32 UtExpected_Mcc_1msCtrlMotDqIRefMccInfo_IdRef[MAX_STEP] = MCC_1MSCTRLMOTDQIREFMCCINFO_IDREF;
const Rtesint32 UtExpected_Mcc_1msCtrlMotDqIRefMccInfo_IqRef[MAX_STEP] = MCC_1MSCTRLMOTDQIREFMCCINFO_IQREF;
const Rtesint32 UtExpected_Mcc_1msCtrlIdbMotPosnInfo_MotTarPosn[MAX_STEP] = MCC_1MSCTRLIDBMOTPOSNINFO_MOTTARPOSN;
const Rtesint32 UtExpected_Mcc_1msCtrlFluxWeakengStInfo_FluxWeakengFlg[MAX_STEP] = MCC_1MSCTRLFLUXWEAKENGSTINFO_FLUXWEAKENGFLG;
const Rtesint32 UtExpected_Mcc_1msCtrlFluxWeakengStInfo_FluxWeakengFlgOld[MAX_STEP] = MCC_1MSCTRLFLUXWEAKENGSTINFO_FLUXWEAKENGFLGOLD;
const Mcc_1msCtrlMotCtrlMode_t UtExpected_Mcc_1msCtrlMotCtrlMode[MAX_STEP] = MCC_1MSCTRLMOTCTRLMODE;
const Mcc_1msCtrlMotCtrlState_t UtExpected_Mcc_1msCtrlMotCtrlState[MAX_STEP] = MCC_1MSCTRLMOTCTRLSTATE;
const Mcc_1msCtrlMotICtrlFadeOutState_t UtExpected_Mcc_1msCtrlMotICtrlFadeOutState[MAX_STEP] = MCC_1MSCTRLMOTICTRLFADEOUTSTATE;
const Mcc_1msCtrlStkRecvryStabnEndOK_t UtExpected_Mcc_1msCtrlStkRecvryStabnEndOK[MAX_STEP] = MCC_1MSCTRLSTKRECVRYSTABNENDOK;



TEST_GROUP(Mcc_1msCtrl);
TEST_SETUP(Mcc_1msCtrl)
{
    Mcc_1msCtrl_Init();
}

TEST_TEAR_DOWN(Mcc_1msCtrl)
{   /* Postcondition */

}

TEST(Mcc_1msCtrl, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Mcc_1msCtrlCircPFild1msInfo.PrimCircPFild1ms = UtInput_Mcc_1msCtrlCircPFild1msInfo_PrimCircPFild1ms[i];
        Mcc_1msCtrlCircPFild1msInfo.SecdCircPFild1ms = UtInput_Mcc_1msCtrlCircPFild1msInfo_SecdCircPFild1ms[i];
        Mcc_1msCtrlPistPFild1msInfo.PistPFild1ms = UtInput_Mcc_1msCtrlPistPFild1msInfo_PistPFild1ms[i];
        Mcc_1msCtrlMotRotgAgSigInfo.StkPosnMeasd = UtInput_Mcc_1msCtrlMotRotgAgSigInfo_StkPosnMeasd[i];
        Mcc_1msCtrlMotRotgAgSigInfo.MotMechAngleSpdFild = UtInput_Mcc_1msCtrlMotRotgAgSigInfo_MotMechAngleSpdFild[i];
        Mcc_1msCtrlIdbVlvActInfo.FadeoutSt2EndOk = UtInput_Mcc_1msCtrlIdbVlvActInfo_FadeoutSt2EndOk[i];
        Mcc_1msCtrlStkRecvryActnIfInfo.StkRecvryCtrlState = UtInput_Mcc_1msCtrlStkRecvryActnIfInfo_StkRecvryCtrlState[i];
        Mcc_1msCtrlStkRecvryActnIfInfo.StkRecvryRequestedTime = UtInput_Mcc_1msCtrlStkRecvryActnIfInfo_StkRecvryRequestedTime[i];
        Mcc_1msCtrlEcuModeSts = UtInput_Mcc_1msCtrlEcuModeSts[i];
        Mcc_1msCtrlFuncInhibitMccSts = UtInput_Mcc_1msCtrlFuncInhibitMccSts[i];
        Mcc_1msCtrlTgtDeltaStkType = UtInput_Mcc_1msCtrlTgtDeltaStkType[i];
        Mcc_1msCtrlPCtrlBoostMod = UtInput_Mcc_1msCtrlPCtrlBoostMod[i];
        Mcc_1msCtrlPCtrlFadeoutSt = UtInput_Mcc_1msCtrlPCtrlFadeoutSt[i];
        Mcc_1msCtrlPCtrlSt = UtInput_Mcc_1msCtrlPCtrlSt[i];
        Mcc_1msCtrlInVlvAllCloseReq = UtInput_Mcc_1msCtrlInVlvAllCloseReq[i];
        Mcc_1msCtrlPChamberVolume = UtInput_Mcc_1msCtrlPChamberVolume[i];
        Mcc_1msCtrlTarDeltaStk = UtInput_Mcc_1msCtrlTarDeltaStk[i];
        Mcc_1msCtrlFinalTarPFromPCtrl = UtInput_Mcc_1msCtrlFinalTarPFromPCtrl[i];

        Mcc_1msCtrl();

        TEST_ASSERT_EQUAL(Mcc_1msCtrlMotDqIRefMccInfo.IdRef, UtExpected_Mcc_1msCtrlMotDqIRefMccInfo_IdRef[i]);
        TEST_ASSERT_EQUAL(Mcc_1msCtrlMotDqIRefMccInfo.IqRef, UtExpected_Mcc_1msCtrlMotDqIRefMccInfo_IqRef[i]);
        TEST_ASSERT_EQUAL(Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn, UtExpected_Mcc_1msCtrlIdbMotPosnInfo_MotTarPosn[i]);
        TEST_ASSERT_EQUAL(Mcc_1msCtrlFluxWeakengStInfo.FluxWeakengFlg, UtExpected_Mcc_1msCtrlFluxWeakengStInfo_FluxWeakengFlg[i]);
        TEST_ASSERT_EQUAL(Mcc_1msCtrlFluxWeakengStInfo.FluxWeakengFlgOld, UtExpected_Mcc_1msCtrlFluxWeakengStInfo_FluxWeakengFlgOld[i]);
        TEST_ASSERT_EQUAL(Mcc_1msCtrlMotCtrlMode, UtExpected_Mcc_1msCtrlMotCtrlMode[i]);
        TEST_ASSERT_EQUAL(Mcc_1msCtrlMotCtrlState, UtExpected_Mcc_1msCtrlMotCtrlState[i]);
        TEST_ASSERT_EQUAL(Mcc_1msCtrlMotICtrlFadeOutState, UtExpected_Mcc_1msCtrlMotICtrlFadeOutState[i]);
        TEST_ASSERT_EQUAL(Mcc_1msCtrlStkRecvryStabnEndOK, UtExpected_Mcc_1msCtrlStkRecvryStabnEndOK[i]);
    }
}

TEST_GROUP_RUNNER(Mcc_1msCtrl)
{
    RUN_TEST_CASE(Mcc_1msCtrl, All);
}
