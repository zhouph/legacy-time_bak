/**
 * @defgroup Mcc_MemMap Mcc_MemMap
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mcc_MemMap.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MCC_MEMMAP_H_
#define MCC_MEMMAP_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/*------------------[MemMap error checking]----------------------------------*/

#if (defined MEMMAP_ERROR) /* to prevent double definition */
#error MEMMAP_ERROR defined, wrong MemMap.h usage
#endif /* if (!defined MEMMAP_ERROR) */

/** \brief Checks if this file has been correctly used
 **
 ** This definition checks if this file has been correctly included
 ** the last time.
 ** We do it here, before the big if-elif. */
#define MEMMAP_ERROR


#if 0
/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (MCC_START_SEC_CODE)
  #undef MCC_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_MCC_SEC_STARTED
    #error "MCC section not closed"
  #endif
  #define CHK_MCC_SEC_STARTED
  #define CHK_MCC_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (MCC_STOP_SEC_CODE)
  #undef MCC_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_MCC_SEC_CODE_STARTED
    #error "MCC_SEC_CODE not opened"
  #endif
  #undef CHK_MCC_SEC_STARTED
  #undef CHK_MCC_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (MCC_START_SEC_CONST_UNSPECIFIED)
  #undef MCC_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_MCC_SEC_STARTED
    #error "MCC section not closed"
  #endif
  #define CHK_MCC_SEC_STARTED
  #define CHK_MCC_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (MCC_STOP_SEC_CONST_UNSPECIFIED)
  #undef MCC_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_MCC_SEC_CONST_UNSPECIFIED_STARTED
    #error "MCC_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_MCC_SEC_STARTED
  #undef CHK_MCC_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (MCC_START_SEC_VAR_UNSPECIFIED)
  #undef MCC_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_MCC_SEC_STARTED
    #error "MCC section not closed"
  #endif
  #define CHK_MCC_SEC_STARTED
  #define CHK_MCC_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (MCC_STOP_SEC_VAR_UNSPECIFIED)
  #undef MCC_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_MCC_SEC_VAR_UNSPECIFIED_STARTED
    #error "MCC_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_MCC_SEC_STARTED
  #undef CHK_MCC_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (MCC_START_SEC_VAR_32BIT)
  #undef MCC_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_MCC_SEC_STARTED
    #error "MCC section not closed"
  #endif
  #define CHK_MCC_SEC_STARTED
  #define CHK_MCC_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (MCC_STOP_SEC_VAR_32BIT)
  #undef MCC_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_MCC_SEC_VAR_32BIT_STARTED
    #error "MCC_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_MCC_SEC_STARTED
  #undef CHK_MCC_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (MCC_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef MCC_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_MCC_SEC_STARTED
    #error "MCC section not closed"
  #endif
  #define CHK_MCC_SEC_STARTED
  #define CHK_MCC_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (MCC_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef MCC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_MCC_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "MCC_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_MCC_SEC_STARTED
  #undef CHK_MCC_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (MCC_START_SEC_VAR_NOINIT_32BIT)
  #undef MCC_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_MCC_SEC_STARTED
    #error "MCC section not closed"
  #endif
  #define CHK_MCC_SEC_STARTED
  #define CHK_MCC_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (MCC_STOP_SEC_VAR_NOINIT_32BIT)
  #undef MCC_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_MCC_SEC_VAR_NOINIT_32BIT_STARTED
    #error "MCC_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_MCC_SEC_STARTED
  #undef CHK_MCC_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (MCC_START_SEC_CALIB_UNSPECIFIED)
  #undef MCC_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_MCC_SEC_STARTED
    #error "MCC section not closed"
  #endif
  #define CHK_MCC_SEC_STARTED
  #define CHK_MCC_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (MCC_STOP_SEC_CALIB_UNSPECIFIED)
  #undef MCC_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_MCC_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "MCC_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_MCC_SEC_STARTED
  #undef CHK_MCC_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR



/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (MCC_1MSCTRL_START_SEC_CODE)
  #undef MCC_1MSCTRL_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_MCC_1MSCTRL_SEC_STARTED
    #error "MCC_1MSCTRL section not closed"
  #endif
  #define CHK_MCC_1MSCTRL_SEC_STARTED
  #define CHK_MCC_1MSCTRL_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (MCC_1MSCTRL_STOP_SEC_CODE)
  #undef MCC_1MSCTRL_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_MCC_1MSCTRL_SEC_CODE_STARTED
    #error "MCC_1MSCTRL_SEC_CODE not opened"
  #endif
  #undef CHK_MCC_1MSCTRL_SEC_STARTED
  #undef CHK_MCC_1MSCTRL_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (MCC_1MSCTRL_START_SEC_CONST_UNSPECIFIED)
  #undef MCC_1MSCTRL_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_MCC_1MSCTRL_SEC_STARTED
    #error "MCC_1MSCTRL section not closed"
  #endif
  #define CHK_MCC_1MSCTRL_SEC_STARTED
  #define CHK_MCC_1MSCTRL_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (MCC_1MSCTRL_STOP_SEC_CONST_UNSPECIFIED)
  #undef MCC_1MSCTRL_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_MCC_1MSCTRL_SEC_CONST_UNSPECIFIED_STARTED
    #error "MCC_1MSCTRL_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_MCC_1MSCTRL_SEC_STARTED
  #undef CHK_MCC_1MSCTRL_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (MCC_1MSCTRL_START_SEC_VAR_UNSPECIFIED)
  #undef MCC_1MSCTRL_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_MCC_1MSCTRL_SEC_STARTED
    #error "MCC_1MSCTRL section not closed"
  #endif
  #define CHK_MCC_1MSCTRL_SEC_STARTED
  #define CHK_MCC_1MSCTRL_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (MCC_1MSCTRL_STOP_SEC_VAR_UNSPECIFIED)
  #undef MCC_1MSCTRL_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_MCC_1MSCTRL_SEC_VAR_UNSPECIFIED_STARTED
    #error "MCC_1MSCTRL_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_MCC_1MSCTRL_SEC_STARTED
  #undef CHK_MCC_1MSCTRL_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (MCC_1MSCTRL_START_SEC_VAR_32BIT)
  #undef MCC_1MSCTRL_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_MCC_1MSCTRL_SEC_STARTED
    #error "MCC_1MSCTRL section not closed"
  #endif
  #define CHK_MCC_1MSCTRL_SEC_STARTED
  #define CHK_MCC_1MSCTRL_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (MCC_1MSCTRL_STOP_SEC_VAR_32BIT)
  #undef MCC_1MSCTRL_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_MCC_1MSCTRL_SEC_VAR_32BIT_STARTED
    #error "MCC_1MSCTRL_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_MCC_1MSCTRL_SEC_STARTED
  #undef CHK_MCC_1MSCTRL_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (MCC_1MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef MCC_1MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_MCC_1MSCTRL_SEC_STARTED
    #error "MCC_1MSCTRL section not closed"
  #endif
  #define CHK_MCC_1MSCTRL_SEC_STARTED
  #define CHK_MCC_1MSCTRL_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (MCC_1MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef MCC_1MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_MCC_1MSCTRL_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "MCC_1MSCTRL_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_MCC_1MSCTRL_SEC_STARTED
  #undef CHK_MCC_1MSCTRL_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (MCC_1MSCTRL_START_SEC_VAR_NOINIT_32BIT)
  #undef MCC_1MSCTRL_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_MCC_1MSCTRL_SEC_STARTED
    #error "MCC_1MSCTRL section not closed"
  #endif
  #define CHK_MCC_1MSCTRL_SEC_STARTED
  #define CHK_MCC_1MSCTRL_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (MCC_1MSCTRL_STOP_SEC_VAR_NOINIT_32BIT)
  #undef MCC_1MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_MCC_1MSCTRL_SEC_VAR_NOINIT_32BIT_STARTED
    #error "MCC_1MSCTRL_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_MCC_1MSCTRL_SEC_STARTED
  #undef CHK_MCC_1MSCTRL_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (MCC_1MSCTRL_START_SEC_CALIB_UNSPECIFIED)
  #undef MCC_1MSCTRL_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_MCC_1MSCTRL_SEC_STARTED
    #error "MCC_1MSCTRL section not closed"
  #endif
  #define CHK_MCC_1MSCTRL_SEC_STARTED
  #define CHK_MCC_1MSCTRL_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (MCC_1MSCTRL_STOP_SEC_CALIB_UNSPECIFIED)
  #undef MCC_1MSCTRL_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_MCC_1MSCTRL_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "MCC_1MSCTRL_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_MCC_1MSCTRL_SEC_STARTED
  #undef CHK_MCC_1MSCTRL_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR


#endif


/*------------------[MemMap error checking]----------------------------------*/

#if (defined MEMMAP_ERROR) /* to prevent double definition */
#error MEMMAP_ERROR defined, wrong MemMap.h usage
#endif /* if (!defined MEMMAP_ERROR) */
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MCC_MEMMAP_H_ */
/** @} */
