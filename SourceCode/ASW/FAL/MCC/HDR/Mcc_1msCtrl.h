/**
 * @defgroup Mcc_1msCtrl Mcc_1msCtrl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mcc_1msCtrl.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MCC_1MSCTRL_H_
#define MCC_1MSCTRL_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Mcc_Types.h"
#include "Mcc_Cfg.h"
#include "Mcc_Cal.h"
#include "SwcCommonFunction.h"
#include "L_CommonFunction.h"
#include "L_CommonFunction_1ms.h"
/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define MCC_1MSCTRL_MODULE_ID      (0)
 #define MCC_1MSCTRL_MAJOR_VERSION  (2)
 #define MCC_1MSCTRL_MINOR_VERSION  (0)
 #define MCC_1MSCTRL_PATCH_VERSION  (0)
 #define MCC_1MSCTRL_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Mcc_1msCtrl_HdrBusType Mcc_1msCtrlBus;

/* Internal Logic Bus */
extern MotorCurrentControl_SRCbusType	 MTRCurCTRLsrcbus;
/* Version Info */
extern const SwcVersionInfo_t Mcc_1msCtrlVersionInfo;

/* Input Data Element */
extern Spc_1msCtrlCircPFild1msInfo_t Mcc_1msCtrlCircPFild1msInfo;
extern Spc_1msCtrlPistPFild1msInfo_t Mcc_1msCtrlPistPFild1msInfo;
extern Msp_CtrlMotRotgAgSigInfo_t Mcc_1msCtrlMotRotgAgSigInfo;
extern Vat_CtrlIdbVlvActInfo_t Mcc_1msCtrlIdbVlvActInfo;
extern Pct_5msCtrlStkRecvryActnIfInfo_t Mcc_1msCtrlStkRecvryActnIfInfo;
extern Mom_HndlrEcuModeSts_t Mcc_1msCtrlEcuModeSts;
extern Eem_SuspcDetnFuncInhibitMccSts_t Mcc_1msCtrlFuncInhibitMccSts;
extern Pct_5msCtrlTgtDeltaStkType_t Mcc_1msCtrlTgtDeltaStkType;
extern Pct_5msCtrlPCtrlBoostMod_t Mcc_1msCtrlPCtrlBoostMod;
extern Pct_5msCtrlPCtrlFadeoutSt_t Mcc_1msCtrlPCtrlFadeoutSt;
extern Pct_5msCtrlPCtrlSt_t Mcc_1msCtrlPCtrlSt;
extern Pct_5msCtrlInVlvAllCloseReq_t Mcc_1msCtrlInVlvAllCloseReq;
extern Pct_5msCtrlPChamberVolume_t Mcc_1msCtrlPChamberVolume;
extern Pct_5msCtrlTarDeltaStk_t Mcc_1msCtrlTarDeltaStk;
extern Pct_5msCtrlFinalTarPFromPCtrl_t Mcc_1msCtrlFinalTarPFromPCtrl;

/* Output Data Element */
extern Mcc_1msCtrlMotDqIRefMccInfo_t Mcc_1msCtrlMotDqIRefMccInfo;
extern Mcc_1msCtrlIdbMotPosnInfo_t Mcc_1msCtrlIdbMotPosnInfo;
extern Mcc_1msCtrlFluxWeakengStInfo_t Mcc_1msCtrlFluxWeakengStInfo;
extern Mcc_1msCtrlMotCtrlMode_t Mcc_1msCtrlMotCtrlMode;
extern Mcc_1msCtrlMotCtrlState_t Mcc_1msCtrlMotCtrlState;
extern Mcc_1msCtrlMotICtrlFadeOutState_t Mcc_1msCtrlMotICtrlFadeOutState;
extern Mcc_1msCtrlStkRecvryStabnEndOK_t Mcc_1msCtrlStkRecvryStabnEndOK;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Mcc_1msCtrl_Init(void);
extern void Mcc_1msCtrl(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MCC_1MSCTRL_H_ */
/** @} */
