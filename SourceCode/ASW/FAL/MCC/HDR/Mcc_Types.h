/**
 * @defgroup Mcc_Types Mcc_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mcc_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MCC_TYPES_H_
#define MCC_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Asw_Types.h" /* HSH */
#include "Bsw_Types.h"
#include "Rte_Types.h"
#include "RteDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/



#define MCC_F16_1				(32768)
#define MCC_S16_MAX				(32767)
#define MCC_S16_MIN				(-32767)
#define MCC_FRAC16(x) 			((Frac16)((x) < 0.999969482421875 ? ((x) >= -1 ? (x)*0x8000 : 0x8000) : 0x7FFF))

#define MCC_S16_ABS_MUX_PATTERN			(1)

#define MCC_S16_CALC_CURR_REF			(1)
#define MCC_S16_CALC_DUTY_CMD			(0)

#define MCC_PID_CHG_CNT_MAX	(5)

#define MCC_S16_BASE_NA					(0)
#define MCC_S16_BASE_FW					(1)
#define MCC_S16_BASE_BW					(2)
#define MCC_S16_BASE_RS1				(3)
#define MCC_S16_BASE_RS2				(4)
#define MCC_S16_BASE_STB1				(5)
#define MCC_S16_BASE_STB2				(6)

#define MCC_S16_BASE_FW_LP				(11)
#define MCC_S16_BASE_BW_LP				(12)
#define MCC_S16_BASE_RS1_LP				(13)
#define MCC_S16_BASE_RS2_LP				(14)
#define MCC_S16_BASE_STB1_LP			(15)
#define MCC_S16_BASE_STB2_LP			(16)

#define MCC_S16_BASE_FW_HP				(21)
#define MCC_S16_BASE_BW_HP				(22)
#define MCC_S16_BASE_RS1_HP				(23)
#define MCC_S16_BASE_RS2_HP				(24)
#define MCC_S16_BASE_STB1_HP			(25)
#define MCC_S16_BASE_STB2_HP			(26)

#define MCC_S16_BASE_FW_BW				(31)
#define MCC_S16_BASE_BW_BW				(32)
#define MCC_S16_BASE_RS1_BW				(33)
#define MCC_S16_BASE_RS2_BW				(34)
#define MCC_S16_BASE_STB1_BW			(35)
#define MCC_S16_BASE_STB2_BW			(36)

#define MCC_S16_BASE_CHG_CNT_MAX	(4)

#define MCC_S16_PID_NA				(0)

#define MCC_S16_PID_SPEED_STAT_MIN	(100)
#define MCC_S16_PID_SPEED_FADEOUT	(100)
#define MCC_S16_PID_SPEED_FADEOUT1	(101)
#define MCC_S16_PID_SPEED_FADEOUT2	(102)
#define MCC_S16_PID_SPEED_FADEOUT3	(103)
#define MCC_S16_PID_SPEED_ACTIVE 	(110)
#define MCC_S16_PID_SPEED_SAFETY 	(120)
#define MCC_S16_PID_SPEED_NORMAL 	(130)
#define MCC_S16_PID_SPEED_CREEP 	(140)
#define MCC_S16_PID_SPEED_STOP 		(150)
#define MCC_S16_PID_SPEED_MUX		(160)
#define MCC_S16_PID_SPEED_STAT_MAX	(199)

#define MCC_S16_PID_POSI_STAT_MIN	(200)
#define MCC_S16_PID_POSI_NORMAL	 	(200)
#define MCC_S16_PID_POSI_ABS_MUX 	(210)
#define MCC_S16_PID_POSI_STAT_MAX 	(299)

#define MCC_S16_PID_PRES_STAT_MIN	(300)
#define MCC_S16_PID_PRES_NORMAL		(300)
#define MCC_S16_PID_PRES_MUX		(310)
#define MCC_S16_PID_PRES_STAT_MAX	(399)

#define MCC_S16_PID_CHG_CNT_MAX		(4)

#define MCC_S16_COMP_NA							(0)
#define MCC_S16_COMP_ABS_MUX_PATTERN			(1)


#define MCC_S8_BOOST_MODE_NOT_CONTROL  			(0)
#define MCC_S8_BOOST_MODE_STOP					(1)
#define MCC_S8_BOOST_MODE_CREEP					(2)
#define MCC_S8_BOOST_MODE_NORMAL				(3)
#define MCC_S8_BOOST_MODE_SAFETY				(4)
#define MCC_S8_BOOST_MODE_ACTIVE				(5)
#define MCC_S8_BOOST_MODE_IN_ABS				(6)	/* Torque Control */
#define MCC_S8_BOOST_MODE_ABS_TO_NORMAL			(7)
#define MCC_S8_BOOST_MODE_AEB					(8)
#define MCC_S8_BOOST_MODE_QUICK					(9)
#define MCC_S8_BOOST_MODE_IN_ABS_MUX			(10)

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Spc_1msCtrlCircPFild1msInfo_t Mcc_1msCtrlCircPFild1msInfo;
    Spc_1msCtrlPistPFild1msInfo_t Mcc_1msCtrlPistPFild1msInfo;
    Msp_CtrlMotRotgAgSigInfo_t Mcc_1msCtrlMotRotgAgSigInfo;
    Vat_CtrlIdbVlvActInfo_t Mcc_1msCtrlIdbVlvActInfo;
    Pct_5msCtrlStkRecvryActnIfInfo_t Mcc_1msCtrlStkRecvryActnIfInfo;
    Mom_HndlrEcuModeSts_t Mcc_1msCtrlEcuModeSts;
    Eem_SuspcDetnFuncInhibitMccSts_t Mcc_1msCtrlFuncInhibitMccSts;
    Pct_5msCtrlTgtDeltaStkType_t Mcc_1msCtrlTgtDeltaStkType;
    Pct_5msCtrlPCtrlBoostMod_t Mcc_1msCtrlPCtrlBoostMod;
    Pct_5msCtrlPCtrlFadeoutSt_t Mcc_1msCtrlPCtrlFadeoutSt;
    Pct_5msCtrlPCtrlSt_t Mcc_1msCtrlPCtrlSt;
    Pct_5msCtrlInVlvAllCloseReq_t Mcc_1msCtrlInVlvAllCloseReq;
    Pct_5msCtrlPChamberVolume_t Mcc_1msCtrlPChamberVolume;
    Pct_5msCtrlTarDeltaStk_t Mcc_1msCtrlTarDeltaStk;
    Pct_5msCtrlFinalTarPFromPCtrl_t Mcc_1msCtrlFinalTarPFromPCtrl;

/* Output Data Element */
    Mcc_1msCtrlMotDqIRefMccInfo_t Mcc_1msCtrlMotDqIRefMccInfo;
    Mcc_1msCtrlIdbMotPosnInfo_t Mcc_1msCtrlIdbMotPosnInfo;
    Mcc_1msCtrlFluxWeakengStInfo_t Mcc_1msCtrlFluxWeakengStInfo;
    Mcc_1msCtrlMotCtrlMode_t Mcc_1msCtrlMotCtrlMode;
    Mcc_1msCtrlMotCtrlState_t Mcc_1msCtrlMotCtrlState;
    Mcc_1msCtrlMotICtrlFadeOutState_t Mcc_1msCtrlMotICtrlFadeOutState;
    Mcc_1msCtrlStkRecvryStabnEndOK_t Mcc_1msCtrlStkRecvryStabnEndOK;
}Mcc_1msCtrl_HdrBusType;

typedef struct
{
	uint8_t	lmccu8PCtrlBoostMod;
	uint8_t lmccu8InVlvAllCloseReq;

	uint16_t lmccu16IdbSystemFailFlg;

	int16_t lmccs16VdcLink;
	int16_t lmccs16MtrCtrlMode;
	int16_t lmccs16CalcIqRefBaseState;
	int16_t lmccs16CalcIqRefPidState;
	int16_t lmccs16CalcIqRefPidStateOld;
	int16_t lmccs16BaseChgCnt;
	int16_t lmccs16BaseChgCntMax;
	int16_t lmccs16BaseChgCntMaxOld;
	int16_t lmccs16PidChgCnt;
	int16_t lmccs16CalcIqRefCompState;
	int16_t lmccs16CalcIqRefCompStateOld;
	int16_t lmccs16MtrCtrlModeOld;

}MotorCurrentControl_SRCbusType;

typedef	struct
{
	/* SETTING PARAMETERS */
	int16_t s16Kp;
	Frac16 f16Kp;
	int16_t s16KiTs;
	Frac16 f16KiTs;
	int16_t s16KdFs;
	Frac16 f16KdFs;

	int16_t	s16UpMin;
	int16_t	s16UpMax;
	int16_t	s16UiMin;
	int16_t	s16UiMax;
	int16_t	s16UdMin;
	int16_t	s16UdMax;
	int16_t s16UoutMin;
	int16_t s16UoutMax;

	int8_t	s8GainId;
	/* SETTING PARAMETERS */

	/* NON-SETTING PARAMETERS */
	int32_t s32Up;
	int32_t s32Ui;
	int32_t s32Ud;
	int16_t	s16Uout;

	int32_t	s32Err;
	int32_t	s32ErrOld;
	int32_t s32ErrUi;

	int32_t s32Ucalc;
	int32_t	s32Udiff;
	/* NON-SETTING PARAMETERS */
}mcc_pid_t;


/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MCC_TYPES_H_ */
/** @} */
