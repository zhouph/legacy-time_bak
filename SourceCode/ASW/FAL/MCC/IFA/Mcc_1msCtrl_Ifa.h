/**
 * @defgroup Mcc_1msCtrl_Ifa Mcc_1msCtrl_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mcc_1msCtrl_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MCC_1MSCTRL_IFA_H_
#define MCC_1MSCTRL_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Mcc_1msCtrl_Read_Mcc_1msCtrlCircPFild1msInfo(data) do \
{ \
    *data = Mcc_1msCtrlCircPFild1msInfo; \
}while(0);

#define Mcc_1msCtrl_Read_Mcc_1msCtrlPistPFild1msInfo(data) do \
{ \
    *data = Mcc_1msCtrlPistPFild1msInfo; \
}while(0);

#define Mcc_1msCtrl_Read_Mcc_1msCtrlMotRotgAgSigInfo(data) do \
{ \
    *data = Mcc_1msCtrlMotRotgAgSigInfo; \
}while(0);

#define Mcc_1msCtrl_Read_Mcc_1msCtrlIdbVlvActInfo(data) do \
{ \
    *data = Mcc_1msCtrlIdbVlvActInfo; \
}while(0);

#define Mcc_1msCtrl_Read_Mcc_1msCtrlStkRecvryActnIfInfo(data) do \
{ \
    *data = Mcc_1msCtrlStkRecvryActnIfInfo; \
}while(0);

#define Mcc_1msCtrl_Read_Mcc_1msCtrlCircPFild1msInfo_PrimCircPFild1ms(data) do \
{ \
    *data = Mcc_1msCtrlCircPFild1msInfo.PrimCircPFild1ms; \
}while(0);

#define Mcc_1msCtrl_Read_Mcc_1msCtrlCircPFild1msInfo_SecdCircPFild1ms(data) do \
{ \
    *data = Mcc_1msCtrlCircPFild1msInfo.SecdCircPFild1ms; \
}while(0);

#define Mcc_1msCtrl_Read_Mcc_1msCtrlPistPFild1msInfo_PistPFild1ms(data) do \
{ \
    *data = Mcc_1msCtrlPistPFild1msInfo.PistPFild1ms; \
}while(0);

#define Mcc_1msCtrl_Read_Mcc_1msCtrlMotRotgAgSigInfo_StkPosnMeasd(data) do \
{ \
    *data = Mcc_1msCtrlMotRotgAgSigInfo.StkPosnMeasd; \
}while(0);

#define Mcc_1msCtrl_Read_Mcc_1msCtrlMotRotgAgSigInfo_MotMechAngleSpdFild(data) do \
{ \
    *data = Mcc_1msCtrlMotRotgAgSigInfo.MotMechAngleSpdFild; \
}while(0);

#define Mcc_1msCtrl_Read_Mcc_1msCtrlIdbVlvActInfo_FadeoutSt2EndOk(data) do \
{ \
    *data = Mcc_1msCtrlIdbVlvActInfo.FadeoutSt2EndOk; \
}while(0);

#define Mcc_1msCtrl_Read_Mcc_1msCtrlStkRecvryActnIfInfo_StkRecvryCtrlState(data) do \
{ \
    *data = Mcc_1msCtrlStkRecvryActnIfInfo.StkRecvryCtrlState; \
}while(0);

#define Mcc_1msCtrl_Read_Mcc_1msCtrlStkRecvryActnIfInfo_StkRecvryRequestedTime(data) do \
{ \
    *data = Mcc_1msCtrlStkRecvryActnIfInfo.StkRecvryRequestedTime; \
}while(0);

#define Mcc_1msCtrl_Read_Mcc_1msCtrlEcuModeSts(data) do \
{ \
    *data = Mcc_1msCtrlEcuModeSts; \
}while(0);

#define Mcc_1msCtrl_Read_Mcc_1msCtrlFuncInhibitMccSts(data) do \
{ \
    *data = Mcc_1msCtrlFuncInhibitMccSts; \
}while(0);

#define Mcc_1msCtrl_Read_Mcc_1msCtrlTgtDeltaStkType(data) do \
{ \
    *data = Mcc_1msCtrlTgtDeltaStkType; \
}while(0);

#define Mcc_1msCtrl_Read_Mcc_1msCtrlPCtrlBoostMod(data) do \
{ \
    *data = Mcc_1msCtrlPCtrlBoostMod; \
}while(0);

#define Mcc_1msCtrl_Read_Mcc_1msCtrlPCtrlFadeoutSt(data) do \
{ \
    *data = Mcc_1msCtrlPCtrlFadeoutSt; \
}while(0);

#define Mcc_1msCtrl_Read_Mcc_1msCtrlPCtrlSt(data) do \
{ \
    *data = Mcc_1msCtrlPCtrlSt; \
}while(0);

#define Mcc_1msCtrl_Read_Mcc_1msCtrlInVlvAllCloseReq(data) do \
{ \
    *data = Mcc_1msCtrlInVlvAllCloseReq; \
}while(0);

#define Mcc_1msCtrl_Read_Mcc_1msCtrlPChamberVolume(data) do \
{ \
    *data = Mcc_1msCtrlPChamberVolume; \
}while(0);

#define Mcc_1msCtrl_Read_Mcc_1msCtrlTarDeltaStk(data) do \
{ \
    *data = Mcc_1msCtrlTarDeltaStk; \
}while(0);

#define Mcc_1msCtrl_Read_Mcc_1msCtrlFinalTarPFromPCtrl(data) do \
{ \
    *data = Mcc_1msCtrlFinalTarPFromPCtrl; \
}while(0);


/* Set Output DE MAcro Function */
#define Mcc_1msCtrl_Write_Mcc_1msCtrlMotDqIRefMccInfo(data) do \
{ \
    Mcc_1msCtrlMotDqIRefMccInfo = *data; \
}while(0);

#define Mcc_1msCtrl_Write_Mcc_1msCtrlIdbMotPosnInfo(data) do \
{ \
    Mcc_1msCtrlIdbMotPosnInfo = *data; \
}while(0);

#define Mcc_1msCtrl_Write_Mcc_1msCtrlFluxWeakengStInfo(data) do \
{ \
    Mcc_1msCtrlFluxWeakengStInfo = *data; \
}while(0);

#define Mcc_1msCtrl_Write_Mcc_1msCtrlMotDqIRefMccInfo_IdRef(data) do \
{ \
    Mcc_1msCtrlMotDqIRefMccInfo.IdRef = *data; \
}while(0);

#define Mcc_1msCtrl_Write_Mcc_1msCtrlMotDqIRefMccInfo_IqRef(data) do \
{ \
    Mcc_1msCtrlMotDqIRefMccInfo.IqRef = *data; \
}while(0);

#define Mcc_1msCtrl_Write_Mcc_1msCtrlIdbMotPosnInfo_MotTarPosn(data) do \
{ \
    Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn = *data; \
}while(0);

#define Mcc_1msCtrl_Write_Mcc_1msCtrlFluxWeakengStInfo_FluxWeakengFlg(data) do \
{ \
    Mcc_1msCtrlFluxWeakengStInfo.FluxWeakengFlg = *data; \
}while(0);

#define Mcc_1msCtrl_Write_Mcc_1msCtrlFluxWeakengStInfo_FluxWeakengFlgOld(data) do \
{ \
    Mcc_1msCtrlFluxWeakengStInfo.FluxWeakengFlgOld = *data; \
}while(0);

#define Mcc_1msCtrl_Write_Mcc_1msCtrlMotCtrlMode(data) do \
{ \
    Mcc_1msCtrlMotCtrlMode = *data; \
}while(0);

#define Mcc_1msCtrl_Write_Mcc_1msCtrlMotCtrlState(data) do \
{ \
    Mcc_1msCtrlMotCtrlState = *data; \
}while(0);

#define Mcc_1msCtrl_Write_Mcc_1msCtrlMotICtrlFadeOutState(data) do \
{ \
    Mcc_1msCtrlMotICtrlFadeOutState = *data; \
}while(0);

#define Mcc_1msCtrl_Write_Mcc_1msCtrlStkRecvryStabnEndOK(data) do \
{ \
    Mcc_1msCtrlStkRecvryStabnEndOK = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MCC_1MSCTRL_IFA_H_ */
/** @} */
