/**
 * @defgroup Mcc_1msCtrl_Ifa Mcc_1msCtrl_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mcc_1msCtrl_Ifa.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Mcc_1msCtrl_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define MCC_1MSCTRL_START_SEC_CONST_UNSPECIFIED
#include "Mcc_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define MCC_1MSCTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Mcc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MCC_1MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mcc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define MCC_1MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mcc_MemMap.h"
#define MCC_1MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Mcc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MCC_1MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Mcc_MemMap.h"
#define MCC_1MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Mcc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MCC_1MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Mcc_MemMap.h"
#define MCC_1MSCTRL_START_SEC_VAR_32BIT
#include "Mcc_MemMap.h"
/** Variable Section (32BIT)**/


#define MCC_1MSCTRL_STOP_SEC_VAR_32BIT
#include "Mcc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MCC_1MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mcc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define MCC_1MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mcc_MemMap.h"
#define MCC_1MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Mcc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MCC_1MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Mcc_MemMap.h"
#define MCC_1MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Mcc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MCC_1MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Mcc_MemMap.h"
#define MCC_1MSCTRL_START_SEC_VAR_32BIT
#include "Mcc_MemMap.h"
/** Variable Section (32BIT)**/


#define MCC_1MSCTRL_STOP_SEC_VAR_32BIT
#include "Mcc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define MCC_1MSCTRL_START_SEC_CODE
#include "Mcc_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define MCC_1MSCTRL_STOP_SEC_CODE
#include "Mcc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
