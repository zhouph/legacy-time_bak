
#include "../HDR/L_IdbTestPatternLib.h"

#if (M_IDB_TEST == ENABLE)
/* Function prototype ***************************************************/
int16_t LIDB_s16ReturnToOrigin(int32_t s32CurrentPosition);
int32_t LIDB_s32GenParabolaTargetPosi(int32_t StartPosi, int16_t EndCnt, int16_t *pCnt);
int16_t	L_s16SineWaveSlow(SWS *pParam);
int16_t	L_s16SineWaveFast(SWF *pParams);
int16_t L_s16Trapzoid(int16_t HLevel, int16_t cnt0, int16_t cnt1, int16_t cnt2, int16_t cnt3, int16_t cnt);
int16_t	L_s16GoUpStairs(uint16_t *pFlgOn, STR *pParam);
int16_t L_s16GoDownStairs(uint16_t *pFlgOn, STR *pParam);
Frac16 MCLIB_f16CosRez360Div1024Deg(Frac16 x);
Frac16 MCLIB_f16SinRez360Div1024Deg(Frac16 x);
int16_t MCLIB_s16TestPIDController(int32_t xRef, int32_t x, PID_Test *pParam);

/* Function Definition ***************************************************/

/*******************************************************************************
* FUNCTION NAME:        LIDB_s16ReturnToOrigin
* CALLED BY:            LIDB_vTestStrokeMap()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          IDB Return To Origin Function for HW Test
********************************************************************************/
int16_t LIDB_s16ReturnToOrigin(int32_t s32CurrentPosition)
{
	static int16_t las16ParabolaGenTdpCnt = 0;
	static uint8_t lau8ReturnToOriginTrigger = FALSE;
	static int32_t las32Rtn2OrgStartPosiTdp = 0;
	const int16_t s16ParabolaGenEndTdpCnt = 80;
	uint8_t u8ReturnToOriginComplete = FALSE;
	int32_t s32MtrPosition = 0;
	int16_t s16DeltaStrokeOutput = 0;

	if (lau8ReturnToOriginTrigger == FALSE)
	{
		lau8ReturnToOriginTrigger = TRUE;
		las16ParabolaGenTdpCnt = 0;
		las32Rtn2OrgStartPosiTdp = s32CurrentPosition;
	}
	else
	{
		las16ParabolaGenTdpCnt = las16ParabolaGenTdpCnt + 1;
	}

	s32MtrPosition = LIDB_s32GenParabolaTargetPosi(las32Rtn2OrgStartPosiTdp, s16ParabolaGenEndTdpCnt, &las16ParabolaGenTdpCnt);

	if (s32MtrPosition == ORIGINE_STROKE_0MM)
	{
	 	lau8ReturnToOriginTrigger = FALSE;
	 	las16ParabolaGenTdpCnt = 0;
	}
	else { /* Do Nothing */ }

	s16DeltaStrokeOutput = (int16_t)(s32MtrPosition - s32CurrentPosition);

	return s16DeltaStrokeOutput;

}

int32_t LIDB_s32GenParabolaTargetPosi(int32_t StartPosi, int16_t EndCnt, int16_t *pCnt)
{
	int16_t s16AInt = 0, s16Sqr = 0;
	int32_t	s32Temp0 = 0, s32EncCntSqr = 0, s32TargetPosi = 0;
	Frac16  f16AFrac = 0;


	s32Temp0 = ((int32_t)*pCnt - (int32_t)EndCnt) * ((int32_t)*pCnt - (int32_t)EndCnt);
	if(s32Temp0 > ASW_S16_MAX)
	{
		s16Sqr = ASW_S16_MAX;
	}
	else if(s32Temp0 < ASW_S16_MIN)
	{
		s16Sqr = ASW_S16_MIN;
	}
	else
	{
		s16Sqr = (int16_t)s32Temp0;
	}

	if(EndCnt > 0)
	{
		s32EncCntSqr = (int32_t)EndCnt * (int32_t)EndCnt;

		if((int32_t)StartPosi >= s32EncCntSqr)
		{
			s16AInt	= (int16_t)((int32_t)StartPosi / s32EncCntSqr);
			s32Temp0 = (int32_t)StartPosi - (int32_t)s16AInt * s32EncCntSqr;
			f16AFrac = (Frac16)(s32Temp0 * 32768 / s32EncCntSqr);
		}
		else
		{
			s16AInt = 0;
			f16AFrac = (Frac16)((int32_t)StartPosi * 32768 / s32EncCntSqr);
		}

		s32TargetPosi = (s16AInt * s16Sqr) + mult_r(f16AFrac, s16Sqr);

		if(s32TargetPosi > ASW_S16_MAX)
		{
			s32TargetPosi = ASW_S16_MAX;
		}
		else if(s32TargetPosi < ASW_S16_MIN)
		{
			s32TargetPosi = ASW_S16_MIN;
		}
		else { }
	}
	else
	{
		s32TargetPosi = 0;
	}

	if(*pCnt > EndCnt)
	{
		s32TargetPosi = 0;
		*pCnt = EndCnt;
	}
	else { }

	s32TargetPosi = (s32TargetPosi < 0)? 0: s32TargetPosi;

	return s32TargetPosi;
}


int16_t	L_s16SineWaveSlow(SWS *pParam)
{
    int16_t	y = 0, f16Temp0 = 0;
    int32_t	s32Temp0 = 0;
    Frac16	angle = 0;

	if (pParam->s16Tp != 0 && pParam->s16fs != 0)
	{
		s32Temp0 = (int32_t)65536 / ((int32_t)(pParam->s16Tp)) * (pParam->s32cnt) / ((int32_t)(pParam->s16fs)) + (int32_t)32768 / 180 * ((int32_t)(pParam->s16phase));
		if (s32Temp0 > ASW_S16_MAX)
		{
			angle = (Frac16)(s32Temp0 - 65535);
		}
		else if (s32Temp0 < (ASW_S16_MIN - 1))
		{
			angle = (Frac16)(s32Temp0 + 65535);
		}
		else
		{
			angle = (Frac16)s32Temp0;
		}

		f16Temp0 = MCLIB_f16SinRez360Div1024Deg(angle);
		s32Temp0 = (int32_t)mult_r(f16Temp0, pParam->s16Amp) + (int32_t)(pParam->s16offset);

		if (s32Temp0 > ASW_S16_MAX)
		{
			y = ASW_S16_MAX;
		}
		else if (s32Temp0 < (ASW_S16_MIN - 1))
		{
			y = ASW_S16_MIN - 1;
		}
		else
		{
			y = (int16_t)s32Temp0;
		}

		pParam->s32cnt++;
		s32Temp0 = (int32_t)pParam->s16fs * (int32_t)pParam->s16Tp;
		if (pParam->s32cnt > s32Temp0)
		{
			pParam->s32cnt = pParam->s32cnt - (2 * (int32_t)(pParam->s16fs) * (int32_t)(pParam->s16Tp));
		}
	}
	else
	{
		y = 0;
	}

    return y;
}

int16_t	L_s16SineWaveFast(SWF *pParams)
{
    int16_t	y = 0;
    int32_t	s32Temp0 = 0;
    Frac16	angle = 0, f16Temp0 = 0;

	if (pParams->s16fs != 0)
	{
		s32Temp0 = (int32_t)65536 * (int32_t)(pParams->s16fsw) * (pParams->s32cnt) / ((int32_t)(pParams->s16fs)) + (int32_t)32768 / 180 * ((int32_t)(pParams->s16phase));
		if (s32Temp0 > ASW_S16_MAX)
		{
			angle = (Frac16)(s32Temp0 - 65535);
		}
		else if (s32Temp0 < (ASW_S16_MIN - 1))
		{
			angle = (Frac16)(s32Temp0 + 65535);
		}
		else
		{
			angle = (Frac16)s32Temp0;
		}

		f16Temp0 = MCLIB_f16SinRez360Div1024Deg(angle);
		s32Temp0 = (int32_t)mult_r(f16Temp0, pParams->s16Amp) + (int32_t)(pParams->s16offset);
		if (s32Temp0 > ASW_S16_MAX)
		{
			y = ASW_S16_MAX;
		}
		else if (s32Temp0 < (ASW_S16_MIN - 1))
		{
			y = ASW_S16_MIN - 1;
		}
		else
		{
			y = (int16_t)s32Temp0;
		}

		pParams->s32cnt++;
		if (pParams->s32cnt > (int32_t)(pParams->s16fs))
		{
			pParams->s32cnt = pParams->s32cnt - (2 * (int32_t)(pParams->s16fs));
		}
	}
	else
	{
		y = 0;	/* fault */
	}

    return y;
}

int16_t L_s16Trapzoid(int16_t HLevel, int16_t cnt0, int16_t cnt1, int16_t cnt2, int16_t cnt3, int16_t cnt)
{
    int16_t	y = 0;

    if (cnt < cnt0)
    {
        y = 0;
    }
    else if (cnt >= cnt0 && cnt < cnt1)
    {
        if (cnt1 > cnt0)
        {
            y = HLevel * (cnt - cnt0) / (cnt1 - cnt0);
        }
        else
        {
            y = HLevel;
        }
    }
    else if (cnt >= cnt1 && cnt < cnt2)
    {
        y = HLevel;
    }
    else if (cnt >= cnt2 && cnt < cnt3)
    {
        if (cnt3 > cnt2)
        {
            y = HLevel * (cnt3 - cnt) / (cnt3 - cnt2);
        }
        else
        {
            y = 0;
        }
    }
    else
    {
        y = 0;
    }

    return y;
}

int16_t	L_s16GoUpStairs(uint16_t *pFlgOn, STR *pParam)
{
    int16_t	y = 0, t = 0, n = 0;

    if (pParam->s16dt > 0)
    {
        if ((pParam->s16cnt >= 0) && (pParam->s16cnt < ASW_S16_MAX))
        {
            t = pParam->s16Tp * pParam->s16cnt;

            if (t < pParam->s16ti)
            {
                *pFlgOn = 0;
                y = pParam->s16xi;

            }
            else if ((t >= pParam->s16ti) && (t < pParam->s16tf))
            {
                *pFlgOn = 1;
                n = (t - pParam->s16ti) / pParam->s16dt;
                y = pParam->s16xi + pParam->s16stepHgt * (n + 1);
            }
            else
            {
                y = 0;
                *pFlgOn = 0;
            }

			if (y > pParam->s16HgtMax)
			{
				y = pParam->s16HgtMax;
			}
			else if (y < pParam->s16HgtMin)
			{
				y = pParam->s16HgtMin;
			}
			else
			{
				;
			}

            pParam->s16cnt++;
        }
        else
        {
            *pFlgOn = 0;
            y = 0;
        }
    }
    else
    {
        *pFlgOn = 0;
        y = 0;
    }

    return y;
}

int16_t L_s16GoDownStairs(uint16_t *pFlgOn, STR *pParam)
{
    int16_t	y = 0, t = 0, n = 0;

    if (pParam->s16dt > 0)
    {
        if ((pParam->s16cnt >= 0) && (pParam->s16cnt < ASW_S16_MAX))
        {
            t = pParam->s16Tp * pParam->s16cnt;

            if (t < pParam->s16ti)
            {
                *pFlgOn = 0;
                y = pParam->s16xi;
            }
            else if ((t >= pParam->s16ti) && (t < pParam->s16tf))
            {
                *pFlgOn = 1;
                n = (t - pParam->s16ti) / pParam->s16dt;
                y = pParam->s16xi - pParam->s16stepHgt * (n + 1);
            }
            else
            {
                *pFlgOn = 0;
                y = 0;
            }

			if (y > pParam->s16HgtMax)
			{
				y = pParam->s16HgtMax;
			}
			else if (y < pParam->s16HgtMin)
			{
				y = pParam->s16HgtMin;
			}
			else
			{
				;
			}

            pParam->s16cnt++;
        }
        else
        {
            *pFlgOn = 0;
            y = 0;
        }
    }
    else
    {
        *pFlgOn = 0;
        y = 0;
    }

    return y;
}

Frac16 MCLIB_f16CosRez360Div1024Deg(Frac16 x)
{
	const static Frac16 cosArray[1025] = {
	-32768,		-32767,		-32765,		-32762,		-32758,
	-32752, 	-32746, 	-32738, 	-32728, 	-32718,
	-32706, 	-32693,     -32679, 	-32664, 	-32647,
	-32629,     -32610,     -32590, 	-32568, 	-32545,
	-32521, 	-32496,     -32470,     -32442,     -32413,
	-32383, 	-32352,     -32319,     -32285,     -32250,
	-32214, 	-32177, 	-32138, 	-32098,     -32057,
	-32015,     -31972, 	-31927, 	-31881,     -31834,
	-31786,     -31736, 	-31686, 	-31634, 	-31581,
	-31527,     -31471,     -31415,     -31357, 	-31298,
	-31238,     -31176,     -31114,     -31050, 	-30985,
	-30919, 	-30852, 	-30784,     -30714,     -30644,
	-30572, 	-30499, 	-30425,     -30350,     -30273,
	-30196, 	-30117, 	-30037, 	-29957, 	-29875,
	-29791, 	-29707,		-29622, 	-29535, 	-29448,
	-29359,     -29269,     -29178, 	-29086, 	-28993,
	-28899, 	-28803,     -28707,     -28609,     -28511,
	-28411, 	-28310,     -28209,     -28106,     -28002,
	-27897, 	-27791, 	-27684, 	-27576,     -27467,
	-27357,     -27245, 	-27133, 	-27020,     -26906,
	-26790,     -26674, 	-26557, 	-26439, 	-26319,
	-26199,     -26078, 	-25955,     -25832, 	-25708,
	-25583,     -25457,     -25330,     -25202, 	-25073,
	-24943, 	-24812, 	-24680,     -24547,     -24414,
	-24279, 	-24144, 	-24007,     -23870,     -23732,
	-23593, 	-23453, 	-23312, 	-23170, 	-23028,
	-22884,     -22740,     -22595, 	-22449,	    -22302,
	-22154,     -22005,     -21856, 	-21706, 	-21555,
	-21403, 	-21250,     -21097,     -20943,     -20788,
	-20632,     -20475,     -20318,     -20160,     -20001,
	-19841, 	-19681, 	-19520, 	-19358,     -19195,
	-19032, 	-18868, 	-18703, 	-18538,     -18372,
	-18205,     -18037, 	-17869, 	-17700, 	-17531,
	-17360,     -17190, 	-17018,     -16846, 	-16673,
	-16500,     -16326,     -16151,     -15976, 	-15800,
	-15624, 	-15446, 	-15269,     -15091,     -14912,
	-14733, 	-14553, 	-14372,     -14191,     -14010,
	-13828, 	-13645, 	-13462, 	-13279, 	-13095,
	-12910,     -12725,  	-12540, 	-12354, 	-12167,
	-11980,     -11793,     -11605, 	-11417, 	-11228,
	-11039, 	-10849,     -10660,     -10469,	    -10278,
	-10087, 	-9896,      -9704,      -9512,      -9319,
	-9126,  	-8933,  	-8739,  	-8545,      -8351,
	-8157,   	-7962,  	-7767,  	-7571,      -7375,
	-7179,      -6983,  	-6786,  	-6590,  	-6392,
	-6195,      -5998,      -5800,      -5602,  	-5404,
	-5205,      -5007,      -4808,      -4609,  	-4410,
	-4210,  	-4011,  	-3811,      -3612,      -3412,
	-3212,  	-3011,  	-2811,      -2611,      -2410,
	-2210,  	-2009,  	-1808,  	-1608,  	-1407,
	-1206,      -1005,      -804,   	-603,   	-402,
	-201,       0,


	201,		402,        603,        804,		1005,
	1206,       1407,       1608,       1808,       2009,
	2210,       2410,       2611,       2811,       3011,
	3212,   	3412,       3612,       3811,       4011,
	4210,       4410,       4609,       4808,       5007,
	5205,       5404,       5602,       5800,       5998,
	6195,       6392,       6590,       6786,       6983,
	7179,       7375,       7571,       7767,       7962,
   	8157,       8351,       8545,       8739,       8933,
   	9126,       9319,       9512,       9704,       9896,
   	10087,      10278,   	10469,      10660,      10849,
   	11039,      11228,      11417,      11605,      11793,
   	11980,      12167,      12354,      12540,   	12725,
   	12910,      13095,      13279,      13462,      13645,
   	13828,      14010,      14191,      14372,      14553,
   	14733,   	14912,      15091,      15269,      15446,
   	15624,      15800,      15976,      16151,      16326,
   	16500,      16673,      16846,   	17018,      17190,
   	17360,      17531,      17700,      17869,      18037,
   	18205,      18372,      18538,      18703,      18868,
   	19032,      19195,      19358,      19520,      19681,
   	19841,      20001,      20160,      20318,      20475,
   	20632,      20788,   	20943,      21097,      21250,
   	21403,      21555,      21706,      21856,      22005,
   	22154,      22302,      22449,      22595,   	22740,
   	22884,      23028,      23170,      23312,      23453,
   	23593,      23732,      23870,      24007,      24144,
   	24279,   	24414,      24547,      24680,      24812,
   	24943,      25073,      25202,      25330,      25457,
   	25583,      25708,      25832,   	25955,      26078,
   	26199,      26319,      26439,      26557,      26674,
   	26790,      26906,      27020,      27133,      27245,
   	27357,      27467,      27576,      27684,      27791,
   	27897,      28002,      28106,      28209,      28310,
   	28411,      28511,      28609,      28707,      28803,
   	28899,      28993,      29086,      29178,      29269,
   	29359,      29448,      29535,      29622,   	29707,
   	29791,      29875,      29957,      30037,      30117,
   	30196,      30273,      30350,      30425,      30499,
   	30572,   	30644,      30714,      30784,      30852,
   	30919,      30985,      31050,      31114,      31176,
   	31238,      31298,      31357,   	31415,      31471,
   	31527,      31581,      31634,      31686,      31736,
   	31786,      31834,      31881,      31927,      31972,
   	32015,      32057,      32098,      32138,      32177,
   	32214,      32250,      32285,      32319,      32352,
   	32383,      32413,   	32442,      32470,      32496,
   	32521,      32545,      32568,      32590,      32610,
   	32629,      32647,      32664,      32679,   	32693,
   	32706,      32718,      32728,      32738,      32746,
   	32752,      32758,      32762,      32765,      32767,


	32767, 		32767,		32765, 	  	32762,		32758,
	32752, 		32746, 		32738, 	  	32728, 	    32718,
	32706, 		32693, 		32679, 	  	32664, 	    32647,
	32629, 		32610, 		32590, 	  	32568, 	    32545,
	32521, 		32496, 		32470, 	  	32442, 	    32413,
	32383, 		32352, 		32319, 	  	32285, 	    32250,
	32214, 		32177, 		32138, 	  	32098, 	    32057,
	32015, 		31972, 		31927,		31881, 	    31834,
	31786, 		31736, 		31686, 	    31634, 	    31581,
	31527, 		31471, 		31415, 	    31357, 	    31298,
	31238, 		31176, 		31114, 	    31050, 	    30985,
	30919, 		30852, 		30784, 	    30714, 	    30644,
	30572, 		30499, 		30425, 	    30350, 	    30273,
	30196, 		30117, 		30037, 	    29957, 	    29875,
	29791, 		29707, 		29622, 	    29535, 	    29448,
	29359, 		29269, 		29178, 	    29086, 	    28993,
	28899, 		28803, 		28707, 	    28609, 	    28511,
	28411, 		28310, 		28209, 	    28106, 	    28002,
	27897, 		27791, 		27684, 	    27576, 	    27467,
	27357, 		27245, 		27133, 	    27020, 	    26906,
	26790, 		26674, 		26557, 	    26439, 	    26319,
	26199, 		26078, 		25955, 	    25832, 	    25708,
	25583, 		25457, 		25330, 	    25202, 	    25073,
	24943, 		24812, 		24680, 	    24547, 	    24414,
	24279, 		24144, 		24007, 	    23870, 	    23732,
	23593, 		23453, 		23312, 	    23170, 	    23028,
	22884, 		22740, 		22595, 	    22449, 	    22302,
	22154, 		22005, 		21856, 	    21706, 	    21555,
	21403, 		21250, 		21097, 	    20943, 	    20788,
	20632, 		20475, 		20318, 	    20160, 	    20001,
	19841, 		19681, 		19520, 	    19358, 	    19195,
	19032, 		18868, 		18703, 	    18538, 	    18372,
	18205, 		18037, 		17869, 	    17700, 	    17531,
	17360, 		17190, 		17018, 	    16846, 	    16673,
	16500, 		16326, 		16151, 	    15976, 	    15800,
	15624, 		15446, 		15269, 	    15091, 	    14912,
	14733, 		14553, 		14372, 	    14191, 	    14010,
	13828, 		13645, 		13462, 	    13279, 	    13095,
	12910, 		12725, 		12540, 	    12354, 	    12167,
	11980, 		11793, 		11605, 	    11417, 	    11228,
	11039, 		10849, 		10660, 	    10469, 	    10278,
	10087, 		9896,  		9704,  	    9512,  	    9319,
	9126,  		8933,  		8739,  	    8545,  	    8351,
	8157,  		7962,  		7767,  	    7571,  	    7375,
	7179,       6983,  		6786,  	    6590,  	    6392,
	6195,  		5998,  		5800,  	    5602,  	    5404,
	5205,  		5007,  		4808,  	    4609,  	    4410,
	4210,  		4011,  		3811,  	    3612,  	    3412,
	3212,  		3011,  		2811,  	    2611,  	    2410,
	2210,  		2009,  		1808,  	    1608,  	    1407,
	1206,  		1005,  		804,   	    603,   	    402,
	201,   		0,


    -201,       -402,       -603,       -804,		-1005,
    -1206,      -1407,      -1608,      -1808,      -2009,
    -2210,      -2410,      -2611,      -2811,      -3011,
    -3212,      -3412,      -3612,      -3811,      -4011,
    -4210,      -4410,      -4609,		-4808,      -5007,
    -5205,      -5404,      -5602,      -5800,      -5998,
    -6195,      -6392,      -6590,      -6786,      -6983,
    -7179,      -7375,      -7571,      -7767,      -7962,
    -8157,      -8351,      -8545,      -8739,      -8933,
    -9126,      -9319,      -9512,      -9704,      -9896,
    -10087,     -10278,     -10469,     -10660,     -10849,
    -11039,     -11228,     -11417,     -11605,     -11793,
    -11980,     -12167,     -12354,     -12540,     -12725,
    -12910,     -13095,     -13279,     -13462,     -13645,
    -13828,     -14010,     -14191,     -14372,     -14553,
    -14733,     -14912,     -15091,     -15269,     -15446,
    -15624,     -15800,     -15976,     -16151,     -16326,
    -16500,     -16673,     -16846,     -17018,     -17190,
    -17360,     -17531,     -17700,     -17869,     -18037,
    -18205,     -18372,     -18538,     -18703,     -18868,
    -19032,     -19195,     -19358,     -19520,     -19681,
    -19841,     -20001,     -20160,     -20318,     -20475,
    -20632,     -20788,     -20943,     -21097,     -21250,
    -21403,     -21555,     -21706,     -21856,     -22005,
    -22154,     -22302,     -22449,     -22595,     -22740,
    -22884,     -23028,     -23170,     -23312,     -23453,
    -23593,     -23732,     -23870,     -24007,     -24144,
    -24279,     -24414,     -24547,     -24680,     -24812,
    -24943,     -25073,     -25202,     -25330,     -25457,
    -25583,     -25708,     -25832,     -25955,     -26078,
    -26199,     -26319,     -26439,     -26557,     -26674,
    -26790,     -26906,     -27020,     -27133,     -27245,
    -27357,     -27467,     -27576,     -27684,     -27791,
    -27897,     -28002,     -28106,     -28209,     -28310,
    -28411,     -28511,     -28609,     -28707,     -28803,
    -28899,     -28993,     -29086,     -29178,     -29269,
    -29359,     -29448,     -29535,     -29622,     -29707,
    -29791,     -29875,     -29957,     -30037,     -30117,
    -30196,     -30273,     -30350,     -30425,     -30499,
    -30572,     -30644,     -30714,     -30784,     -30852,
    -30919,     -30985,     -31050,     -31114,     -31176,
    -31238,     -31298,     -31357,     -31415,     -31471,
    -31527,     -31581,     -31634,     -31686,     -31736,
    -31786,     -31834,     -31881,     -31927,     -31972,
    -32015,     -32057,     -32098,     -32138,     -32177,
    -32214,     -32250,     -32285,     -32319,     -32352,
    -32383,     -32413,     -32442,     -32470,     -32496,
    -32521,     -32545,     -32568,     -32590,     -32610,
    -32629,     -32647,     -32664,     -32679,     -32693,
    -32706,     -32718,     -32728,     -32738,     -32746,
    -32752,     -32758,     -32762,     -32765,     -32767,
    -32768
	};

	register Frac16 angle;

	angle = x/64;

	return cosArray[angle + 512];
}

Frac16 MCLIB_f16SinRez360Div1024Deg(Frac16 x)
{
	return MCLIB_f16CosRez360Div1024Deg(x - 16384);
}

/*int16_t MCLIB_s16PIDController(int32_t xRef, int32_t x, PID *pParam)*/
int16_t MCLIB_s16TestPIDController(int32_t xRef, int32_t x, PID_Test *pParam)
{
	register int16_t	s16Temp0 = 0, s16err = 0, s16uOut = 0, s16errDot = 0, s16errDotFilt = 0;
	register int32_t	s32err = 0, s32uDifference = 0, s32uCalc = 0, s32errDot = 0, s32Temp0 = 0, s32errDotFilt = 0;


	/* for D-Controller */

	if (pParam->s16Kd > 100)
	{
		pParam->s16Kd = 100;
	}
	else if (pParam->s16Kd < -100)
	{
		pParam->s16Kd = -100;
	}
	else
	{
		;
	}

	if (pParam->s16fs > 20000)
	{
		pParam->s16fs = 20000;
	}
	else if (pParam->s16fs < -20000)
	{
		pParam->s16fs = -20000;
	}
	else
	{
		;
	}

	if (pParam->s16wc > 1000)
	{
		pParam->s16wc = 1000;
	}
	else if (pParam->s16wc < -1000)
	{
		pParam->s16wc = -1000;
	}
	else
	{
		;
	}
	/* for D-Controller */

	/* calculate err */
    s32err = xRef - x;

	/* constrain err range */
	if (s32err > ASW_S16_MAX)
	{
		s16err = ASW_S16_MAX;
	}
	else if (s32err < ASW_S16_MIN)
	{
		s16err = ASW_S16_MIN;
	}
	else
	{
		s16err = (int16_t)s32err;
	}

	/* calculate uP */
	pParam->s32uP = (int32_t)pParam->s16Kp * (int32_t)s16err + (int32_t)mtr_mult_r(pParam->f16Kp, s16err);

	/* calculate uD */
	if ((pParam->s16Kd > 0 ) || (pParam->f16Kd > 0))
	{
		s32errDot = (int32_t)s16err - (int32_t)pParam->s16errOld;

	if (s32errDot > ASW_S16_MAX)
	{
		s16errDot = ASW_S16_MAX;
	}
	else if (s32errDot < ASW_S16_MIN)
	{
		s16errDot = ASW_S16_MIN;
	}
	else
	{
		s16errDot = (int16_t)s32errDot;
	}

		pParam->s32uD = (int32_t)pParam->s16Kd * (int32_t)s16errDot  + (int32_t)mtr_mult_r(pParam->f16Kd, s16errDot);
	}
	else
	{
		pParam->s32uD = 0;
	}
	pParam->s16errOld = s16err;

	/* calculate uI */
	s32err = s16err + (int32_t)pParam->s16Kb * (int32_t)pParam->s16uDifference + (int32_t)mtr_mult_r(pParam->f16Kb, pParam->s16uDifference);
	if (s32err > ASW_S16_MAX)
	{
		s16err = ASW_S16_MAX;
	}
	else if (s32err < ASW_S16_MIN)
	{
		s16err = ASW_S16_MIN;
	}
	else
	{
		s16err = (int16_t)s32err;
	}

	s16Temp0 = mtr_mult_r(pParam->f16KiTs, s16err);
	if ((s16Temp0 == 0) && (pParam->f16KiTs != 0) && (s16err != 0))
	{
		if ((pParam->f16KiTs > 0) && (s16err > 0))
		{
			pParam->s32uI = pParam->s32uI + (int32_t)pParam->s16KiTs * (int32_t)s16err + 1;
		}
		else if ((pParam->f16KiTs > 0) && (s16err < 0))
		{
			pParam->s32uI = pParam->s32uI + (int32_t)pParam->s16KiTs * (int32_t)s16err - 1;
		}
		else if ((pParam->f16KiTs < 0) && (s16err > 0))
		{
			pParam->s32uI = pParam->s32uI + (int32_t)pParam->s16KiTs * (int32_t)s16err - 1;
		}
		else if ((pParam->f16KiTs < 0) && (s16err < 0))
		{
			pParam->s32uI = pParam->s32uI + (int32_t)pParam->s16KiTs * (int32_t)s16err + 1;
		}
		else
		{
			;
		}
	}
	else
	{
		pParam->s32uI = pParam->s32uI + (int32_t)pParam->s16KiTs * (int32_t)s16err + (int32_t)mtr_mult_r(pParam->f16KiTs, s16err);
	}

	if (pParam->s32uI > pParam->s16uOutMax)
	{
		pParam->s32uI = pParam->s16uOutMax;
	}
	else if (pParam->s32uI < pParam->s16uOutMin)
	{
		pParam->s32uI = pParam->s16uOutMin;
	}
	else
	{
		;
	}
/*
	if ((pParam->s16Kd > 0 ) || (pParam->f16Kd > 0))
	{

		s32errDot = (int32_t)s16err - (int32_t)pParam->s16errOld;

	 if (s32errDot > S16_MAX)
	 {
	  s16errDot = S16_MAX;
	 }
	 else if (s32errDot < S16_MIN)
	 {
	  s16errDot = S16_MIN;
	 }
	 else
	 {
	  s16errDot = (int16_t)s32errDot;
	 }

		pParam->s32uD = (int32_t)pParam->s16Kd * (int32_t)s16errDot  + (int32_t)mtr_mult_r(pParam->f16Kd, s16errDot);
	 }
	 else
	 {
		pParam->s32uD = 0;
	 }
*/
	 s32uCalc = pParam->s32uP + pParam->s32uI + pParam->s32uD;


	if (s32uCalc > pParam->s16uOutMax)
    {
        s16uOut = pParam->s16uOutMax;
    }
    else if (s32uCalc < pParam->s16uOutMin)
    {
        s16uOut = pParam->s16uOutMin;
    }
    else
    {
        s16uOut = (int16_t)s32uCalc;
    }

    s32uDifference = (int32_t)s16uOut - s32uCalc;
	if (s32uDifference > ASW_S16_MAX)
	{
		pParam->s16uDifference = ASW_S16_MAX;
	}
	else if (s32uDifference < ASW_S16_MIN)
	{
		pParam->s16uDifference = ASW_S16_MIN;
	}
	else
	{
		pParam->s16uDifference = (int16_t)s32uDifference;
	}

/*	pParam->s16errOld = s16err; */
	pParam->s16errDotFiltOld = s16errDotFilt;


	if (s16uOut > pParam->s16uOutMax)
	{
		s16uOut = pParam->s16uOutMax;
	}
	else if (s16uOut <= pParam->s16uOutMin)
	{
		s16uOut = pParam->s16uOutMin;
	}
	else
	{
		;
	}

	pParam->s16uOut = s16uOut;

    return s16uOut;
}
#endif /*M_IDB_TEST*/
