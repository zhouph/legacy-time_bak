/*
 * L_IdbTestPressPerformance.h
 *
 *  Created on: 2015. 6. 18.
 *      Author: seokjong.kim@halla.com
 */

#ifndef L_IDBTESTCHAMBERVOLUME_H_
#define L_IDBTESTCHAMBERVOLUME_H_

extern stInternalInterface_t LIDB_vChamberVolumeTest(stInternalInterface_t* stInternalInterface_r);
extern stTestOverrideConfig_t LIDB_stChamberVolumeConfig(void);

#endif /* L_IDBTESTCHAMBERVOLUME_H_ */
