/*
 * L_IdbTestPressPerformance.c
 *
 *  Created on: 2015. 6. 18.
 *      Author: seokjong.kim@halla.com
 */
/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "../../HDR/L_IdbTestMain.h"

#if (M_IDB_TEST == ENABLE)

#include "L_IdbTestChamberVolume.h"
#include "L_IdbTestChamberVolumeConfig.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/


#define M_RAMP_TEST_MODE					0
#define M_STEP_TEST_MODE					1
#define M_CONTINUOUS_TEST_MODE				2

#define M_PRESSURE_PERFORMANCE_TEST_MODE	M_CONTINUOUS_TEST_MODE

#define M_STEP_DROP_TARGET					DISABLE

#define M_TEST_PATTERN_REPEAT_CNT			1

#define M_RAMP_TEST_INITIAL_CASE			0	/* Ramp Test Mode Decide 0~7 */

#define M_RAMP_TEST_CASES					8

#define S16_MAX_TP							S16_PCTRL_PRESS_150_BAR
#define M_S16_PRESSURE_FADE_OUT_TIME		U16_T_2_S

/* M_STEP_TEST_MODE Define */
#define M_S16_STEP_TEST_TP					S16_PCTRL_PRESS_70_BAR
#define M_S16_STEP_HIGH_HOLD_TIME			U16_T_5_S
#define M_S16_STEP_LOW_HOLD_TIME			U16_T_3_S

#define M_S16_INITIAL_DELTASTROKE			50

#define M_STATE_READY					(0)
/* For Ramp Test Mode */
#define M_STATE_INITIA_POSI				(1)
#define M_STATE_FW_LOW_PRESS_APPLY		(2)
#define M_STATE_BW_PRESS_APPLY			(3)
#define M_STATE_FW_HIGH_PRESS_APPLY		(4)
#define M_STATE_FW_HIGH_TO_LOW_RELEASE	(5)
#define M_STATE_INITIAL_RELEASE			(6)
#define M_STATE_FW_LOW_PRESS_APPLY2		(7)
#define M_STATE_FW_HIGH_PRESS_APPLY2	(8)
#define M_STATE_FW_HIGH_TO_LOW_RELEASE2	(9)
#define M_STATE_INITIAL_RELEASE2		(10)
#define M_STATE_RETURN_ORIGIN			(11)

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
	int32_t s32InitialPosi;
	int16_t s16FWLowPressTarget;
	int16_t s16BWPressTarget;
	int16_t s16FWHighPressTarget;
	int16_t s16FWHighToLowPressTarget;
	int16_t s16TestApplyRate;
	int16_t s16TestReleaseRate;
	int16_t s16TestHoldTime;
}stTEST_PATTERN_t;
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/

stTEST_PATTERN_t lidbstChamberVolumeTestPattern =
/*	INITIAL_POSI,        LOW_PRESS_TARGET,     BW_PRESS_TARGET,          FW_HIGH_PRESS_TARGET,   FW_HIGH_TO_LOW_TARGET,   APPLY_RATE,         RELEASE_RATE      TEST_HOLD_TIME*/
	{S32_MTRPOSI_20MM, S16_PCTRL_PRESS_60_BAR, S16_PCTRL_PRESS_100_BAR, S16_PCTRL_PRESS_140_BAR, S16_PCTRL_PRESS_110_BAR, S16_P_RATE_20_BPS, S16_P_RATE_20_BPS, U16_T_1_S};

/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
static int16_t LIDB_s16TestCalcStdTargetPressRate(int16_t s16TargetPressInput);
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/


/*******************************************************************************
* FUNCTION NAME:        LIDB_vChamberVolumeTest
* CALLED BY:            L_vCallTestIDB()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Generate target pressure for Idb pressure performance test
********************************************************************************/
stInternalInterface_t LIDB_vChamberVolumeTest(stInternalInterface_t* stInternalInterface_r)
{
	static uint8_t lidbu8PressCtrlTestActive 		= FALSE;
	static uint8_t lidbu8TestPatternRepeatCnt		= 0;
	static int16_t ldahbs16ProgressCnt = 0;
	static int16_t ldahbs16TestState = M_STATE_READY;
	static int16_t lidbs16TestTargetPressOld = S16_PCTRL_PRESS_0_BAR;
	int16_t s16TestTargetPress = S16_PCTRL_PRESS_0_BAR;
	int16_t s16TestDeltaStrokeTarget = 0;

	if (stInternalInterface_r->u8TestReady == 1)
	{
		lidbu8PressCtrlTestActive = TRUE;
	}
	else
	{
		lidbu8PressCtrlTestActive 											= FALSE;
		lidbu8TestPatternRepeatCnt 											= 0;
		lidbu8PressCtrlTestActive 											= FALSE;
		ldahbs16ProgressCnt 												= 0;
		ldahbs16TestState 													= M_STATE_READY;
		lidbs16TestTargetPressOld 											= S16_PCTRL_PRESS_0_BAR;
		s16TestTargetPress 													= S16_PCTRL_PRESS_0_BAR;
		s16TestDeltaStrokeTarget 											= 0;
		stInternalInterface_r->s8MTRPChamberVolume							= U8_BOOST_READY;
		stInternalInterface_r->s8VLVPChamberVolume							= U8_BOOST_READY;
		stInternalInterface_r->stTestOverrideConfig.u1OverrideDeltaStroke	= FALSE;
	}

	if (lidbu8PressCtrlTestActive == TRUE)
	{
		ldahbs16ProgressCnt = ldahbs16ProgressCnt + 1;

		if (lidbu8TestPatternRepeatCnt < M_TEST_PATTERN_REPEAT_CNT)
		{
			switch(ldahbs16TestState)
			{
			case M_STATE_READY:
				if (ldahbs16ProgressCnt >= lidbstChamberVolumeTestPattern.s16TestHoldTime)
				{
					s16TestTargetPress 													= S16_PCTRL_PRESS_0_1_BAR;
					ldahbs16ProgressCnt 												= 0;
					s16TestDeltaStrokeTarget 											= M_S16_INITIAL_DELTASTROKE;
					ldahbs16TestState 													= M_STATE_INITIA_POSI;
					stInternalInterface_r->s8MTRPChamberVolume							= U8_BOOST_LOW_PRESS;
					stInternalInterface_r->s8VLVPChamberVolume							= U8_BOOST_READY;
					stInternalInterface_r->stTestOverrideConfig.u1OverrideDeltaStroke	= TRUE;
				}
				else
				{
					s16TestDeltaStrokeTarget 											= 0;
					s16TestTargetPress 													= S16_PCTRL_PRESS_0_BAR;
					stInternalInterface_r->s8MTRPChamberVolume							= U8_BOOST_READY;
					stInternalInterface_r->s8VLVPChamberVolume							= U8_BOOST_READY;
					stInternalInterface_r->stTestOverrideConfig.u1OverrideDeltaStroke	= FALSE;

				}
				break;

			case M_STATE_INITIA_POSI:
				if (lidbstChamberVolumeTestPattern.s32InitialPosi <= stInternalInterface_r->s32TargetPosi)
				{
					s16TestTargetPress 													= S16_PCTRL_PRESS_0_1_BAR;
					s16TestDeltaStrokeTarget 											= 0;
					stInternalInterface_r->s8MTRPChamberVolume							= U8_BOOST_LOW_PRESS;
					stInternalInterface_r->s8VLVPChamberVolume							= U8_BOOST_READY;
					stInternalInterface_r->stTestOverrideConfig.u1OverrideDeltaStroke	= TRUE;

					if (ldahbs16ProgressCnt > lidbstChamberVolumeTestPattern.s16TestHoldTime)
					{
						ldahbs16TestState 													= M_STATE_FW_LOW_PRESS_APPLY;
						lidbs16TestTargetPressOld											= S16_PCTRL_PRESS_0_BAR;
						ldahbs16ProgressCnt 												= 0;
						stInternalInterface_r->s8MTRPChamberVolume							= U8_BOOST_LOW_PRESS;
						stInternalInterface_r->s8VLVPChamberVolume							= U8_BOOST_LOW_PRESS;
						stInternalInterface_r->stTestOverrideConfig.u1OverrideDeltaStroke	= FALSE;

						stInternalInterface_r->stTestOverrideConfig.u1OverrideCut1VlvCtrl	= FALSE;
						stInternalInterface_r->stTestOverrideConfig.u1OverrideCut2VlvCtrl	= FALSE;
					}
				}
				else
				{
					s16TestDeltaStrokeTarget											= M_S16_INITIAL_DELTASTROKE;
					s16TestTargetPress 													= S16_PCTRL_PRESS_0_1_BAR;
					ldahbs16ProgressCnt 												= 0;
					stInternalInterface_r->s8MTRPChamberVolume							= U8_BOOST_LOW_PRESS;
					stInternalInterface_r->s8VLVPChamberVolume							= U8_BOOST_READY;
					stInternalInterface_r->stTestOverrideConfig.u1OverrideDeltaStroke	= TRUE;

					stInternalInterface_r->u8TestActFlg_Cut								= FALSE;
					stInternalInterface_r->u8TestRespLv_Cut								= U8_VLV_RESPONSE_LV_NORMAL;
					stInternalInterface_r->stTestOverrideConfig.u1OverrideCut1VlvCtrl	= TRUE;
					stInternalInterface_r->stTestOverrideConfig.u1OverrideCut2VlvCtrl	= TRUE;
				}
				break;

			case M_STATE_FW_LOW_PRESS_APPLY:
				if (lidbs16TestTargetPressOld >= lidbstChamberVolumeTestPattern.s16FWLowPressTarget)
				{
					s16TestTargetPress 													= lidbstChamberVolumeTestPattern.s16FWLowPressTarget;
					ldahbs16TestState 													= M_STATE_BW_PRESS_APPLY;
					ldahbs16ProgressCnt 												= 0;
					stInternalInterface_r->s8MTRPChamberVolume							= U8_BOOST_LOW_PRESS;
					stInternalInterface_r->s8VLVPChamberVolume							= U8_BOOST_LOW_PRESS;
					stInternalInterface_r->stTestOverrideConfig.u1OverrideDeltaStroke	= FALSE;
				}
				else
				{
					s16TestTargetPress = S16_PCTRL_PRESS_0_1_BAR + ldahbs16ProgressCnt * (int16_t)((int32_t)(lidbstChamberVolumeTestPattern.s16TestApplyRate*100)/200);	/* 100 : 1bar == 100, 200 : bar/scan */

					if (s16TestTargetPress >= lidbstChamberVolumeTestPattern.s16FWLowPressTarget)
					{
						s16TestTargetPress = lidbstChamberVolumeTestPattern.s16FWLowPressTarget;
					}
					else { }
					stInternalInterface_r->s8MTRPChamberVolume							= U8_BOOST_LOW_PRESS;
					stInternalInterface_r->s8VLVPChamberVolume							= U8_BOOST_LOW_PRESS;
					stInternalInterface_r->stTestOverrideConfig.u1OverrideDeltaStroke	= FALSE;
				}

				break;
			case M_STATE_BW_PRESS_APPLY:
				if (lidbs16TestTargetPressOld >= lidbstChamberVolumeTestPattern.s16BWPressTarget)
				{
					s16TestTargetPress 													= lidbstChamberVolumeTestPattern.s16BWPressTarget;
					ldahbs16TestState 													= M_STATE_FW_HIGH_PRESS_APPLY;
					ldahbs16ProgressCnt 												= 0;
					stInternalInterface_r->s8MTRPChamberVolume							= U8_BOOST_BACKWARD;
					stInternalInterface_r->s8VLVPChamberVolume							= U8_BOOST_BACKWARD;
					stInternalInterface_r->stTestOverrideConfig.u1OverrideDeltaStroke	= FALSE;
				}
				else
				{
					s16TestTargetPress = lidbstChamberVolumeTestPattern.s16FWLowPressTarget + ldahbs16ProgressCnt * (int16_t)((int32_t)(lidbstChamberVolumeTestPattern.s16TestApplyRate*100)/200);	/* 100 : 1bar == 100, 200 : bar/scan */

					if (s16TestTargetPress > lidbstChamberVolumeTestPattern.s16BWPressTarget)
					{
						s16TestTargetPress = lidbstChamberVolumeTestPattern.s16BWPressTarget;
					}
					else{ }
					stInternalInterface_r->s8MTRPChamberVolume							= U8_BOOST_BACKWARD;
					stInternalInterface_r->s8VLVPChamberVolume							= U8_BOOST_BACKWARD;
					stInternalInterface_r->stTestOverrideConfig.u1OverrideDeltaStroke	= FALSE;
				}
				break;

			case M_STATE_FW_HIGH_PRESS_APPLY:
				if (lidbs16TestTargetPressOld >= lidbstChamberVolumeTestPattern.s16FWHighPressTarget)
				{
					s16TestTargetPress = lidbstChamberVolumeTestPattern.s16FWHighPressTarget;
					if (ldahbs16ProgressCnt > lidbstChamberVolumeTestPattern.s16TestHoldTime)
					{
						ldahbs16TestState = M_STATE_FW_HIGH_TO_LOW_RELEASE;
						ldahbs16ProgressCnt = 0;
					}
					else { }
					stInternalInterface_r->s8MTRPChamberVolume							= U8_BOOST_BACKWARD;
					stInternalInterface_r->s8VLVPChamberVolume							= U8_BOOST_BACKWARD;
					stInternalInterface_r->stTestOverrideConfig.u1OverrideDeltaStroke	= FALSE;
				}
				else
				{
					s16TestTargetPress = lidbstChamberVolumeTestPattern.s16BWPressTarget + ldahbs16ProgressCnt * (int16_t)((int32_t)(lidbstChamberVolumeTestPattern.s16TestApplyRate*100)/200);	/* 100 : 1bar == 100, 200 : bar/scan */

					if (s16TestTargetPress > lidbstChamberVolumeTestPattern.s16FWHighPressTarget)
					{
						s16TestTargetPress = lidbstChamberVolumeTestPattern.s16FWHighPressTarget;
					}
					else{ }
					stInternalInterface_r->s8MTRPChamberVolume							= U8_BOOST_HIGH_PRESS;
					stInternalInterface_r->s8VLVPChamberVolume							= U8_BOOST_HIGH_PRESS;
					stInternalInterface_r->stTestOverrideConfig.u1OverrideDeltaStroke	= FALSE;
				}
				break;

			case M_STATE_FW_HIGH_TO_LOW_RELEASE:
				if (lidbs16TestTargetPressOld <=  lidbstChamberVolumeTestPattern.s16FWHighToLowPressTarget)
				{
					s16TestTargetPress 													= lidbstChamberVolumeTestPattern.s16FWHighToLowPressTarget;
					ldahbs16TestState 													= M_STATE_INITIAL_RELEASE;
					ldahbs16ProgressCnt 												= 0;
					stInternalInterface_r->s8MTRPChamberVolume							= U8_BOOST_HIGH_PRESS;
					stInternalInterface_r->s8VLVPChamberVolume							= U8_BOOST_HIGH_PRESS;
					stInternalInterface_r->stTestOverrideConfig.u1OverrideDeltaStroke	= FALSE;
				}
				else
				{
					s16TestTargetPress = lidbstChamberVolumeTestPattern.s16FWHighPressTarget - ldahbs16ProgressCnt * (int16_t)((int32_t)(lidbstChamberVolumeTestPattern.s16TestReleaseRate*100)/200);	/* 100 : 1bar == 100, 200 : bar/scan */

					if (s16TestTargetPress < lidbstChamberVolumeTestPattern.s16FWHighToLowPressTarget)
					{
						s16TestTargetPress = lidbstChamberVolumeTestPattern.s16FWHighToLowPressTarget;
					}
					else{ }
					stInternalInterface_r->s8MTRPChamberVolume							= U8_BOOST_HIGH_PRESS;
					stInternalInterface_r->s8VLVPChamberVolume							= U8_BOOST_HIGH_PRESS;
					stInternalInterface_r->stTestOverrideConfig.u1OverrideDeltaStroke	= FALSE;
				}
				break;
			case M_STATE_INITIAL_RELEASE:
				s16TestTargetPress = lidbstChamberVolumeTestPattern.s16FWHighToLowPressTarget - ldahbs16ProgressCnt * (int16_t)((int32_t)(lidbstChamberVolumeTestPattern.s16TestReleaseRate*100)/200);	/* 100 : 1bar == 100, 200 : bar/scan */

				if (s16TestTargetPress < S16_PCTRL_PRESS_0_1_BAR)
				{
					s16TestTargetPress 													= S16_PCTRL_PRESS_0_1_BAR;
					ldahbs16TestState 													= M_STATE_FW_LOW_PRESS_APPLY2;
					ldahbs16ProgressCnt													= 0;
				}
				else { }

				stInternalInterface_r->s8MTRPChamberVolume							= U8_BOOST_LOW_PRESS;
				stInternalInterface_r->s8VLVPChamberVolume							= U8_BOOST_LOW_PRESS;
				stInternalInterface_r->stTestOverrideConfig.u1OverrideDeltaStroke	= FALSE;
				break;

			case M_STATE_FW_LOW_PRESS_APPLY2:
				if (lidbs16TestTargetPressOld >= lidbstChamberVolumeTestPattern.s16BWPressTarget)
				{
					s16TestTargetPress 													= lidbstChamberVolumeTestPattern.s16BWPressTarget;
					ldahbs16TestState 													= M_STATE_FW_HIGH_PRESS_APPLY2;
					ldahbs16ProgressCnt 												= 0;
				}
				else
				{
					s16TestTargetPress = S16_PCTRL_PRESS_0_1_BAR + ldahbs16ProgressCnt * (int16_t)((int32_t)(lidbstChamberVolumeTestPattern.s16TestApplyRate*100)/200);	/* 100 : 1bar == 100, 200 : bar/scan */

					if (s16TestTargetPress >= lidbstChamberVolumeTestPattern.s16BWPressTarget)
					{
						s16TestTargetPress = lidbstChamberVolumeTestPattern.s16BWPressTarget;
					}
					else{ }
				}
				stInternalInterface_r->s8MTRPChamberVolume							= U8_BOOST_LOW_PRESS;
				stInternalInterface_r->s8VLVPChamberVolume							= U8_BOOST_LOW_PRESS;
				stInternalInterface_r->stTestOverrideConfig.u1OverrideDeltaStroke	= FALSE;
				break;

			case M_STATE_FW_HIGH_PRESS_APPLY2:
				if (lidbs16TestTargetPressOld >= lidbstChamberVolumeTestPattern.s16FWHighPressTarget)
				{
					s16TestTargetPress = lidbstChamberVolumeTestPattern.s16FWHighPressTarget;
					if (ldahbs16ProgressCnt > lidbstChamberVolumeTestPattern.s16TestHoldTime)
					{
						ldahbs16TestState					= M_STATE_FW_HIGH_TO_LOW_RELEASE2;
						ldahbs16ProgressCnt					= 0;
					}
					else { }
				}
				else
				{
					s16TestTargetPress = lidbstChamberVolumeTestPattern.s16BWPressTarget + ldahbs16ProgressCnt * (int16_t)((int32_t)(lidbstChamberVolumeTestPattern.s16TestApplyRate*100)/200);	/* 100 : 1bar == 100, 200 : bar/scan */

					if (s16TestTargetPress > lidbstChamberVolumeTestPattern.s16FWHighPressTarget)
					{
						s16TestTargetPress = lidbstChamberVolumeTestPattern.s16FWHighPressTarget;
					}
					else{ }
				}
				stInternalInterface_r->s8MTRPChamberVolume							= U8_BOOST_HIGH_PRESS;
				stInternalInterface_r->s8VLVPChamberVolume							= U8_BOOST_HIGH_PRESS;
				stInternalInterface_r->stTestOverrideConfig.u1OverrideDeltaStroke	= FALSE;
				break;

			case M_STATE_FW_HIGH_TO_LOW_RELEASE2:
				if (lidbs16TestTargetPressOld <=  lidbstChamberVolumeTestPattern.s16FWHighToLowPressTarget)
				{
					s16TestTargetPress = S16_PCTRL_PRESS_0_BAR;
					ldahbs16TestState = M_STATE_INITIAL_RELEASE2;
					ldahbs16ProgressCnt = 0;
				}
				else
				{
					s16TestTargetPress = lidbstChamberVolumeTestPattern.s16FWHighPressTarget - ldahbs16ProgressCnt * (int16_t)((int32_t)(lidbstChamberVolumeTestPattern.s16TestReleaseRate*100)/200);	/* 100 : 1bar == 100, 200 : bar/scan */

					if (s16TestTargetPress < lidbstChamberVolumeTestPattern.s16FWHighToLowPressTarget)
					{
						s16TestTargetPress = lidbstChamberVolumeTestPattern.s16FWHighToLowPressTarget;
					}
					else{ }
				}
				stInternalInterface_r->s8MTRPChamberVolume							= U8_BOOST_HIGH_PRESS;
				stInternalInterface_r->s8VLVPChamberVolume							= U8_BOOST_HIGH_PRESS;
				stInternalInterface_r->stTestOverrideConfig.u1OverrideDeltaStroke	= FALSE;
				break;

			case M_STATE_INITIAL_RELEASE2:
				s16TestTargetPress = lidbstChamberVolumeTestPattern.s16FWHighToLowPressTarget - ldahbs16ProgressCnt * (int16_t)((int32_t)(lidbstChamberVolumeTestPattern.s16TestReleaseRate*100)/200);	/* 100 : 1bar == 100, 200 : bar/scan */

				if (s16TestTargetPress < S16_PCTRL_PRESS_0_1_BAR)
				{
					s16TestTargetPress 													= S16_PCTRL_PRESS_0_1_BAR;
					ldahbs16TestState 													= M_STATE_RETURN_ORIGIN;
					ldahbs16ProgressCnt 												= 0;
					stInternalInterface_r->s8MTRPChamberVolume							= U8_BOOST_LOW_PRESS;
					stInternalInterface_r->s8VLVPChamberVolume							= U8_BOOST_READY;
					stInternalInterface_r->stTestOverrideConfig.u1OverrideDeltaStroke	= FALSE;
					stInternalInterface_r->u8TestActFlg_Cut								= FALSE;
					stInternalInterface_r->u8TestRespLv_Cut								= U8_VLV_RESPONSE_LV_NORMAL;
					stInternalInterface_r->stTestOverrideConfig.u1OverrideCut1VlvCtrl	= TRUE;
					stInternalInterface_r->stTestOverrideConfig.u1OverrideCut2VlvCtrl	= TRUE;
				}
				else
				{
					stInternalInterface_r->s8MTRPChamberVolume							= U8_BOOST_LOW_PRESS;
					stInternalInterface_r->s8VLVPChamberVolume							= U8_BOOST_LOW_PRESS;
					stInternalInterface_r->stTestOverrideConfig.u1OverrideDeltaStroke	= FALSE;
				}
				break;

			case M_STATE_RETURN_ORIGIN:
				if (stInternalInterface_r->s32TargetPosi < -M_S16_INITIAL_DELTASTROKE)
				{
					s16TestDeltaStrokeTarget = M_S16_INITIAL_DELTASTROKE/2;
				}
				else if (stInternalInterface_r->s32TargetPosi > M_S16_INITIAL_DELTASTROKE)
				{
					s16TestDeltaStrokeTarget = L_s16LimitMinMax(-stInternalInterface_r->s32TargetPosi / 5, -100, -M_S16_INITIAL_DELTASTROKE/2);
				}
				else
				{
					s16TestDeltaStrokeTarget 									= 0;
					lidbu8TestPatternRepeatCnt 									= lidbu8TestPatternRepeatCnt + 1;
					ldahbs16TestState 											= M_STATE_READY;
				}

				s16TestTargetPress 													= S16_PCTRL_PRESS_0_1_BAR;
				ldahbs16ProgressCnt 												= 0;
				stInternalInterface_r->s8MTRPChamberVolume							= U8_BOOST_LOW_PRESS;
				stInternalInterface_r->s8VLVPChamberVolume							= U8_BOOST_READY;
				stInternalInterface_r->stTestOverrideConfig.u1OverrideDeltaStroke	= TRUE;
				stInternalInterface_r->u8TestActFlg_Cut								= FALSE;
				stInternalInterface_r->u8TestRespLv_Cut								= U8_VLV_RESPONSE_LV_NORMAL;
				stInternalInterface_r->stTestOverrideConfig.u1OverrideCut1VlvCtrl	= TRUE;
				stInternalInterface_r->stTestOverrideConfig.u1OverrideCut2VlvCtrl	= TRUE;
				break;

			default:
				s16TestTargetPress 							= S16_PCTRL_PRESS_0_BAR;
				lidbu8PressCtrlTestActive 					= FALSE;
				ldahbs16ProgressCnt 						= 0;
				break;
			}
		}
		else
		{

			lidbu8PressCtrlTestActive 											= FALSE;
			lidbu8TestPatternRepeatCnt 											= 0;
			ldahbs16ProgressCnt 												= 0;
			ldahbs16TestState 													= M_STATE_READY;
			lidbs16TestTargetPressOld 											= S16_PCTRL_PRESS_0_BAR;
			s16TestTargetPress 													= S16_PCTRL_PRESS_0_BAR;
			s16TestDeltaStrokeTarget 											= 0;
			stInternalInterface_r->u8TestReady 									= FALSE;
			stInternalInterface_r->s8MTRPChamberVolume							= U8_BOOST_READY;
			stInternalInterface_r->s8VLVPChamberVolume							= U8_BOOST_READY;
			stInternalInterface_r->stTestOverrideConfig.u1OverrideDeltaStroke	= FALSE;
			stInternalInterface_r->u8TestActFlg_Cut								= FALSE;
			stInternalInterface_r->u8TestRespLv_Cut								= U8_VLV_RESPONSE_LV_NORMAL;
			stInternalInterface_r->stTestOverrideConfig.u1OverrideCut1VlvCtrl	= TRUE;
			stInternalInterface_r->stTestOverrideConfig.u1OverrideCut2VlvCtrl	= TRUE;
		}
	}
	else { }

	stInternalInterface_r->s16TestStateMon 									= ldahbs16TestState;
	stInternalInterface_r->s16TestTgtDeltaStroke 							= s16TestDeltaStrokeTarget;
	stInternalInterface_r->stTestOverrideConfig.u1OverrideTargetPressure 	= TRUE;
	stInternalInterface_r->s16TestTargetPress 								= L_s16LimitMinMax(s16TestTargetPress, S16_PCTRL_PRESS_0_BAR, S16_MAX_TP);
	stInternalInterface_r->s16TestDeltaTargetPress 							= stInternalInterface_r->s16TestTargetPress - lidbs16TestTargetPressOld;
	stInternalInterface_r->u8PCtrlActFlg 									= (stInternalInterface_r->s16TestTargetPress > stInternalInterface_r->s16PCtrlActThreshPress)? TRUE: FALSE;
	stInternalInterface_r->s16TestTargetPressRate 							= LIDB_s16TestCalcStdTargetPressRate(stInternalInterface_r->s16TestTargetPress);
	lidbs16TestTargetPressOld = s16TestTargetPress;
	stInternalInterface_r->stTestOverrideConfig.u1OverridePCtrlState		= TRUE;
	stInternalInterface_r->s8PCtrlState										= LIDB_s8TestPressCtrlState(stInternalInterface_r->u8TestReady);
	stInternalInterface_r->stTestOverrideConfig.u1OverrideReqBoostMode		= TRUE;
	stInternalInterface_r->s8ReqBoostMode									= S8_BOOST_MODE_REQ_L5;
	stInternalInterface_r->stTestOverrideConfig.u1OverridePCtrlBoostMode		= TRUE;
	stInternalInterface_r->s8PCtrlBoostMode									= S8_BOOST_MODE_RESPONSE_L5;
	return (*stInternalInterface_r);
}

stTestOverrideConfig_t LIDB_stChamberVolumeConfig(void)
{
	static stTestOverrideConfig_t pstTestOverrideConfig = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

	pstTestOverrideConfig.u1OverridePCtrlState				= M_OVERRIDE_PCTRLSTATE;
	pstTestOverrideConfig.u1OverridePCtrlBoostMode			= M_OVERRIDE_PCTRLBOOSTMODE;
	pstTestOverrideConfig.u1OverrideReqBoostMode			= M_OVERRIDE_REQBOOSTMODE;
	pstTestOverrideConfig.u1OverridePedalSiganl				= M_OVERRIDE_PEDAL_SIGNAL;
	pstTestOverrideConfig.u1OverrideTargetPressure			= M_OVERRIDE_TARGET_PRESSURE;
	pstTestOverrideConfig.u1OverrideWheelTargetPressure		= M_OVERRIDE_WHEEL_TARGET_PRESSURE;
	pstTestOverrideConfig.u1OverrideChamberVolumeMtr		= M_OVERRIDE_CHAMBERVOLUME_MTR;
	pstTestOverrideConfig.u1OverrideChamberVolumeVlv		= M_OVERRIDE_CHAMBERVOLUME_VLV;
	pstTestOverrideConfig.u1OverrideDeltaStroke				= M_OVERRIDE_DELTA_STROKE;
	pstTestOverrideConfig.u1OverrideCut1VlvCtrl				= M_OVERRIDE_CUT1_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideCut2VlvCtrl				= M_OVERRIDE_CUT2_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideSimVlvCtrl				= M_OVERRIDE_SIM_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideCvVlvCtrl				= M_OVERRIDE_CV_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideRelVlvCtrl				= M_OVERRIDE_REL_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideBalVlvCtrl				= M_OVERRIDE_BAL_VLV_CONTROL;
	pstTestOverrideConfig.u1OverridePdVlvCtrl				= M_OVERRIDE_PD_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideWheelInVlvCtrl			= M_OVERRIDE_WHEEL_IN_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideWheelOutVlvCtrl			= M_OVERRIDE_WHEEL_OUT_VLV_CONTROL;

	return (pstTestOverrideConfig);
}

static int16_t LIDB_s16TestCalcStdTargetPressRate(int16_t s16TargetPressInput)
{
	int16_t s16TGPressBuffCntBf2scan = 0;
	int16_t s16FilterInput = 0;
	int32_t s32FilterInputTemp = 0;

	static int16_t lidbs16PressBuff[5] = {0,0,0,0,0};
	static int16_t lidbs16BuffCnt = 0;
	static int16_t lidbs16PressRate10ms = 0;

	lidbs16PressBuff[lidbs16BuffCnt] = s16TargetPressInput;

	lidbs16BuffCnt = lidbs16BuffCnt + 1;
    if(lidbs16BuffCnt>=5)
    {
    	lidbs16BuffCnt = 0;
    }
    else{ }

	/**********************************************************/
	/* Target Press rate (1 = 0.01bar/10ms = 1bar/s)          */
	/**********************************************************/
    if(lidbs16BuffCnt>=3)
    {
    	s16TGPressBuffCntBf2scan = lidbs16BuffCnt - 3;
    }
    else
    {
    	s16TGPressBuffCntBf2scan = lidbs16BuffCnt + 2;
    }

	s32FilterInputTemp = ((int32_t)s16TargetPressInput - (int32_t)lidbs16PressBuff[s16TGPressBuffCntBf2scan]); /* target press variation for 2scan. changed algorithm for simplicity in Feb. 2015 -> already checked both algorithms*/

	s16FilterInput = (int16_t)L_s32LimitMinMax(s32FilterInputTemp, ASW_S16_MIN, ASW_S16_MAX);

	lidbs16PressRate10ms =(L_s16Lpf1Int(s16FilterInput, lidbs16PressRate10ms, U8_LPF_7HZ_128G)); /* 1 bar/sec*/


	return lidbs16PressRate10ms;
}
#endif /* (M_IDB_TEST == ENABLE) */
