/*
 * L_IdbDeltaStrokeTest.h
 *
 *  Created on: 2015. 6. 10.
 *      Author: sohyun.ahn
 */

#ifndef L_IDBDELTASTROKETEST_H_
#define L_IDBDELTASTROKETEST_H_

#include "L_IdbTestMain.h"

extern stInternalInterface_t LIDB_vGenTestTargetStrokeIDB(stInternalInterface_t* pstInternalIf);
extern stTestOverrideConfig_t LIDB_stTestTargetStrokeConfig(void);

#endif /* L_IDBDELTASTROKETEST_H_ */
