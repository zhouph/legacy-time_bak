#include "../../HDR/L_IdbTestMain.h"

#if (M_IDB_TEST == ENABLE)
#include "L_IdbDeltaStrokeTest.h"
#include "L_IdbDeltaStrokeTestConfig.h"
#include "../../HDR/L_IdbTestPatternLib.h"

/* Local Definition ***********************************************************/

/* IDB TGTSTROKE TEST MODE Definition */
#define DELTA_STROKE_TEST_BY_CAL				(1)

#define U8_IDB_TGTSTROKE_TEST_MODE				DELTA_STROKE_TEST_BY_CAL

typedef struct
{
	uint8_t		u8IdbDelsTestTriggerFlg;
	uint8_t		u8IdbTestDataUpdateFlg;
	int16_t		s16IdbTestLimitMaxPress;
	int16_t		s16IdbTestDeltaStrok;
	int32_t		s32IdbTestDestPosi;
	int8_t		s8PChamberVolumeMtr;
	int8_t		s8PChamberVolumeVlv;
}stMakeDelStrokeCal_t;

/* IDB Local Definition *******************************************************/

stMakeDelStrokeCal_t stMakeDelStrokeCalInput        = {FALSE, FALSE, S16_PCTRL_PRESS_150_BAR, 50, S32_MTRPOSI_50MM, U8_BOOST_READY, U8_BOOST_READY};

uint8_t lu8IdbTgtStrokeTestMode 					= U8_IDB_TGTSTROKE_TEST_MODE;

/* LocalFunction prototype ***************************************************/
static int16_t LIDB_s16MakeTestDeltaStrokeByCal(stInternalInterface_t* pstInternalIf, int16_t s16LimitMaxPress, int16_t s16DeltaStroke, int32_t s32DestPosi);

stInternalInterface_t LIDB_vGenTestTargetStrokeIDB(stInternalInterface_t* pstInternalIf)
{
	static uint8_t lu8CompleteReturnToOrigin = FALSE;
	static uint8_t lu8IdbTestDataUpdateFlgOld = FALSE;
	static uint8_t lu8IdbDelsTestTriggerFlgOld = FALSE;

	static int16_t ls16LimitMaxPress = S16_PCTRL_PRESS_150_BAR;
	static int16_t ls16DeltaStrok = 50;
	static int32_t ls32DestPosi = S32_MTRPOSI_50MM;

	uint8_t		lu8IdbDelsTestTriggerFlg  	= stMakeDelStrokeCalInput.u8IdbDelsTestTriggerFlg;
	uint8_t		lu8IdbTestDataUpdateFlg  	= stMakeDelStrokeCalInput.u8IdbTestDataUpdateFlg ;

	/* lu8IdbTestDataUpdateFlg : Calibration Parameter */
	if ((lu8IdbTestDataUpdateFlg == TRUE) && (lu8IdbTestDataUpdateFlgOld == FALSE))
	{
		/* Data Update */
		ls16LimitMaxPress                  = stMakeDelStrokeCalInput.s16IdbTestLimitMaxPress;
		ls16DeltaStrok                     = stMakeDelStrokeCalInput.s16IdbTestDeltaStrok;
		ls32DestPosi                       = stMakeDelStrokeCalInput.s32IdbTestDestPosi;
		pstInternalIf->s8MTRPChamberVolume = stMakeDelStrokeCalInput.s8PChamberVolumeMtr;
		pstInternalIf->s8VLVPChamberVolume = stMakeDelStrokeCalInput.s8PChamberVolumeVlv;
	}
	else { }

	/* lu8IdbDelsTestTriggerFlg : Calibration Parameter */
	if ((lu8IdbDelsTestTriggerFlg == TRUE) && (lu8IdbDelsTestTriggerFlgOld == FALSE))
	{
		/* Test Trigger On */
		pstInternalIf->u8TestReady = TRUE;
	}
	else if ((lu8IdbDelsTestTriggerFlg == FALSE) && (lu8IdbDelsTestTriggerFlgOld == TRUE))
	{
		/* Test Trigger Off */
		pstInternalIf->u8TestReady = FALSE;
	}
	else { }

	pstInternalIf->s8PCtrlState 	= LIDB_s8TestPressCtrlState(pstInternalIf->u8TestReady);

	pstInternalIf->s8PCtrlBoostMode = S8_BOOST_MODE_RESPONSE_L5;

	switch(lu8IdbTgtStrokeTestMode)
	{
	case DELTA_STROKE_TEST_BY_CAL:
		pstInternalIf->s16TestTgtDeltaStroke = LIDB_s16MakeTestDeltaStrokeByCal(pstInternalIf, ls16LimitMaxPress, ls16DeltaStrok, ls32DestPosi);
		break;
	default:
		break;
	}

	lu8IdbTestDataUpdateFlgOld = lu8IdbTestDataUpdateFlg;
	lu8IdbDelsTestTriggerFlgOld = lu8IdbDelsTestTriggerFlg;

	return (*pstInternalIf);
}

static int16_t LIDB_s16MakeTestDeltaStrokeByCal(stInternalInterface_t* pstInternalIf, int16_t s16LimitMaxPress, int16_t s16DeltaStroke, int32_t s32DestPosi)
{
	static uint16_t lidbu16HoldStrokeCnt = 0;
	static uint8_t lidbu8RepeatPatternCnt = 0;

	int16_t s16TargetDeltaStroke = 0;

	if(pstInternalIf->u8TestReady == TRUE)
	{
		lidbu16HoldStrokeCnt = lidbu16HoldStrokeCnt + 1;

		if (lidbu16HoldStrokeCnt > U16_T_1_S)
		{
			lidbu16HoldStrokeCnt = U16_T_1_S;

			if (pstInternalIf->s32TargetPosi < s32DestPosi - s16DeltaStroke)
			{
				s16TargetDeltaStroke = s16DeltaStroke;
			}
			else if (pstInternalIf->s32TargetPosi > s32DestPosi + s16DeltaStroke)
			{
				s16TargetDeltaStroke = -s16DeltaStroke;
			}
			else
			{
				s16TargetDeltaStroke = s32DestPosi - pstInternalIf->s32TargetPosi;
			}

			if (pstInternalIf->s16TestCircuitP1_100bar > s16LimitMaxPress)
			{
				s16TargetDeltaStroke = 0;
			}
			else { }
		}
		else { }
	}
	else
	{
		lidbu16HoldStrokeCnt = 0;
	}

	return s16TargetDeltaStroke;
}

stTestOverrideConfig_t LIDB_stTestTargetStrokeConfig(void)
{
	static stTestOverrideConfig_t pstTestOverrideConfig = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

	pstTestOverrideConfig.u1OverridePCtrlState				= M_OVERRIDE_PCTRLSTATE;
	pstTestOverrideConfig.u1OverridePCtrlBoostMode			= M_OVERRIDE_PCTRLBOOSTMODE;
	pstTestOverrideConfig.u1OverrideReqBoostMode			= M_OVERRIDE_REQBOOSTMODE;
	pstTestOverrideConfig.u1OverridePedalSiganl				= M_OVERRIDE_PEDAL_SIGNAL;
	pstTestOverrideConfig.u1OverrideTargetPressure			= M_OVERRIDE_TARGET_PRESSURE;
	pstTestOverrideConfig.u1OverrideWheelTargetPressure		= M_OVERRIDE_WHEEL_TARGET_PRESSURE;
	pstTestOverrideConfig.u1OverrideChamberVolumeMtr		= M_OVERRIDE_CHAMBERVOLUME_MTR;
	pstTestOverrideConfig.u1OverrideChamberVolumeVlv		= M_OVERRIDE_CHAMBERVOLUME_VLV;
	pstTestOverrideConfig.u1OverrideDeltaStroke				= M_OVERRIDE_DELTA_STROKE;
	pstTestOverrideConfig.u1OverrideCut1VlvCtrl				= M_OVERRIDE_CUT1_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideCut2VlvCtrl				= M_OVERRIDE_CUT2_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideSimVlvCtrl				= M_OVERRIDE_SIM_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideCvVlvCtrl				= M_OVERRIDE_CV_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideRelVlvCtrl				= M_OVERRIDE_REL_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideBalVlvCtrl				= M_OVERRIDE_BAL_VLV_CONTROL;
	pstTestOverrideConfig.u1OverridePdVlvCtrl				= M_OVERRIDE_PD_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideWheelInVlvCtrl			= M_OVERRIDE_WHEEL_IN_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideWheelOutVlvCtrl			= M_OVERRIDE_WHEEL_OUT_VLV_CONTROL;

	return (pstTestOverrideConfig);
}

#endif/* (M_IDB_TEST == ENABLE) */
