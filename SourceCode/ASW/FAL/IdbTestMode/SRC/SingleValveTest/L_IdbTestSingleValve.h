/*
 * L_IdbTestSingleTargetTest.h
 *
 *  Created on: 2015. 6. 18.
 *      Author: seokjong.kim@halla.com
 */

#ifndef L_IDBTESTSINGLEVALVE_H_
#define L_IDBTESTSINGLEVALVE_H_

extern stInternalInterface_t LIDB_vTestSingleValve(stInternalInterface_t* stInternalInterface_r);
extern stTestOverrideConfig_t LIDB_stSingleValveConfig(void);

#endif /* L_IDBTESTSINGLEVALVE_H_ */
