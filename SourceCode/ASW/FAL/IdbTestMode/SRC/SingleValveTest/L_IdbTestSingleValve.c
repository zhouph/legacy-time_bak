/*
 * L_IdbTestPressPerformance.c
 *
 *  Created on: 2015. 6. 18.
 *      Author: seokjong.kim@halla.com
 */
/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "../../HDR/L_IdbTestMain.h"

#if (M_IDB_TEST == ENABLE)

#include "./L_IdbTestSingleValve.h"
#include "./L_IdbTestSingleValveConfig.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

#define M_VALVE_INDEX_CUT		0
#define M_VALVE_INDEX_SIM		1
#define M_VALVE_INDEX_CIR		2
#define M_VALVE_INDEX_BAL		3
#define M_VALVE_INDEX_PD		4
#define M_VALVE_INDEX_REL		5

#define M_VALVE_COUNT_MAX		6

#define M_STATE_READY			1
#define M_STATE_VALVE_ACT		2
#define M_STATE_VALVE_READY		3

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
	uint8_t		u8ValveActFlg;
	uint8_t		u8ValveOnRespLv;
	uint8_t		u8ValveOffRespLv;
}stValveInfo_t;

typedef struct
{
	stValveInfo_t	stVlvInfo[M_VALVE_COUNT_MAX];
}stValveTestInfo_t;

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
stValveTestInfo_t	stValveTestInfo =
{
	{
		{FALSE, U8_VLV_RESPONSE_LV_NORMAL, U8_VLV_RESPONSE_LV_NORMAL},
		{FALSE, U8_VLV_RESPONSE_LV_NORMAL, U8_VLV_RESPONSE_LV_NORMAL},
		{FALSE, U8_VLV_RESPONSE_LV_NORMAL, U8_VLV_RESPONSE_LV_NORMAL},
		{FALSE, U8_VLV_RESPONSE_LV_NORMAL, U8_VLV_RESPONSE_LV_NORMAL},
		{FALSE, U8_VLV_RESPONSE_LV_NORMAL, U8_VLV_RESPONSE_LV_NORMAL},
		{FALSE, U8_VLV_RESPONSE_LV_NORMAL, U8_VLV_RESPONSE_LV_NORMAL}
	}
};

uint8_t lidbu8ContinuousMode = FALSE;
uint8_t lidbu8ValveIndex = M_VALVE_INDEX_CUT;
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/


/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
/*******************************************************************************
* FUNCTION NAME:        LIDB_vTestSingleValve
* CALLED BY:            L_vCallTestIDB()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Generate target pressure for Idb pressure performance test
********************************************************************************/
stInternalInterface_t LIDB_vTestSingleValve(stInternalInterface_t* stInternalInterface_r)
{
	static int16_t lidbs16ProgressCnt 			= 0;
	static int16_t lidbs16TestState 			= M_STATE_READY;
	static uint8_t lidbu8PressCtrlTestActive 	= FALSE;

	int16_t s16TestTargetPress 					= S16_PCTRL_PRESS_0_BAR;

	if (stInternalInterface_r->u8TestReady == TRUE)
	{
		lidbu8PressCtrlTestActive = TRUE;
	}
	else
	{
		lidbu8PressCtrlTestActive 		= FALSE;
		lidbs16TestState 				= M_STATE_READY;
		if (lidbu8ContinuousMode == TRUE)
		{
			lidbu8ValveIndex = M_VALVE_INDEX_CUT;
		}
		else { }
	}

	if (lidbu8PressCtrlTestActive == TRUE)
	{

		lidbs16ProgressCnt = lidbs16ProgressCnt + 1;

		switch(lidbs16TestState)
		{
		case M_STATE_READY:

			if (lidbs16ProgressCnt > U8_T_500_MS)
			{
				lidbs16TestState = M_STATE_VALVE_ACT;
				lidbs16ProgressCnt = 0;
				stValveTestInfo.stVlvInfo[lidbu8ValveIndex].u8ValveActFlg = TRUE;
			}
			else { }
			break;

		case M_STATE_VALVE_ACT:
			stInternalInterface_r->u8TestActFlg_Cut = stValveTestInfo.stVlvInfo[M_VALVE_INDEX_CUT].u8ValveActFlg;
			stInternalInterface_r->u8TestRespLv_Cut = stValveTestInfo.stVlvInfo[M_VALVE_INDEX_CUT].u8ValveOnRespLv;

			stInternalInterface_r->u8TestActFlg_Sim = stValveTestInfo.stVlvInfo[M_VALVE_INDEX_SIM].u8ValveActFlg;
			stInternalInterface_r->u8TestRespLv_Sim = stValveTestInfo.stVlvInfo[M_VALVE_INDEX_SIM].u8ValveOnRespLv;

			stInternalInterface_r->u8TestActFlg_CV = stValveTestInfo.stVlvInfo[M_VALVE_INDEX_CIR].u8ValveActFlg;
			stInternalInterface_r->u8TestRespLv_CV = stValveTestInfo.stVlvInfo[M_VALVE_INDEX_CIR].u8ValveOnRespLv;

			stInternalInterface_r->u8TestActFlg_Bal = stValveTestInfo.stVlvInfo[M_VALVE_INDEX_BAL].u8ValveActFlg;
			stInternalInterface_r->u8TestRespLv_Bal = stValveTestInfo.stVlvInfo[M_VALVE_INDEX_BAL].u8ValveOnRespLv;

			stInternalInterface_r->u8TestActFlg_PD = stValveTestInfo.stVlvInfo[M_VALVE_INDEX_PD].u8ValveActFlg;
			stInternalInterface_r->u8TestRespLv_PD = stValveTestInfo.stVlvInfo[M_VALVE_INDEX_PD].u8ValveOnRespLv;

			stInternalInterface_r->u8TestActFlg_Rel = stValveTestInfo.stVlvInfo[M_VALVE_INDEX_REL].u8ValveActFlg;
			stInternalInterface_r->u8TestRespLv_Rel = stValveTestInfo.stVlvInfo[M_VALVE_INDEX_REL].u8ValveOnRespLv;

			if (lidbs16ProgressCnt > U16_T_2_S)
			{
				lidbs16TestState = M_STATE_VALVE_READY;
				lidbs16ProgressCnt = 0;
				stValveTestInfo.stVlvInfo[lidbu8ValveIndex].u8ValveActFlg = FALSE;
			}
			else { }
			break;

		case M_STATE_VALVE_READY:
			stInternalInterface_r->u8TestActFlg_Cut = stValveTestInfo.stVlvInfo[M_VALVE_INDEX_CUT].u8ValveActFlg;
			stInternalInterface_r->u8TestRespLv_Cut = stValveTestInfo.stVlvInfo[M_VALVE_INDEX_CUT].u8ValveOffRespLv;

			stInternalInterface_r->u8TestActFlg_Sim = stValveTestInfo.stVlvInfo[M_VALVE_INDEX_SIM].u8ValveActFlg;
			stInternalInterface_r->u8TestRespLv_Sim = stValveTestInfo.stVlvInfo[M_VALVE_INDEX_SIM].u8ValveOffRespLv;

			stInternalInterface_r->u8TestActFlg_CV = stValveTestInfo.stVlvInfo[M_VALVE_INDEX_CIR].u8ValveActFlg;
			stInternalInterface_r->u8TestRespLv_CV = stValveTestInfo.stVlvInfo[M_VALVE_INDEX_CIR].u8ValveOffRespLv;

			stInternalInterface_r->u8TestActFlg_Bal = stValveTestInfo.stVlvInfo[M_VALVE_INDEX_BAL].u8ValveActFlg;
			stInternalInterface_r->u8TestRespLv_Bal = stValveTestInfo.stVlvInfo[M_VALVE_INDEX_BAL].u8ValveOffRespLv;

			stInternalInterface_r->u8TestActFlg_PD = stValveTestInfo.stVlvInfo[M_VALVE_INDEX_PD].u8ValveActFlg;
			stInternalInterface_r->u8TestRespLv_PD = stValveTestInfo.stVlvInfo[M_VALVE_INDEX_PD].u8ValveOffRespLv;

			stInternalInterface_r->u8TestActFlg_Rel = stValveTestInfo.stVlvInfo[M_VALVE_INDEX_REL].u8ValveActFlg;
			stInternalInterface_r->u8TestRespLv_Rel = stValveTestInfo.stVlvInfo[M_VALVE_INDEX_REL].u8ValveOffRespLv;

			if (lidbs16ProgressCnt > U16_T_2_S)
			{
				lidbs16TestState = M_STATE_READY;
				lidbs16ProgressCnt = 0;
				if (lidbu8ContinuousMode == TRUE)
				{
					if (lidbu8ValveIndex < M_VALVE_COUNT_MAX - 1)
					{
						lidbu8ValveIndex = lidbu8ValveIndex + 1;
					}
					else
					{
						lidbu8ValveIndex = 0;
						stInternalInterface_r->u8TestReady = FALSE;
					}
				}
				else
				{
					stInternalInterface_r->u8TestReady = FALSE;
				}
			}
			else { }
			break;

		default:
			break;
		}
	}
	else
	{
		lidbs16TestState 					= M_STATE_READY;
		stInternalInterface_r->u8TestReady 	= FALSE;
	}

	stInternalInterface_r->s16TestStateMon										= lidbs16TestState;

	return (* stInternalInterface_r);
}

stTestOverrideConfig_t LIDB_stSingleValveConfig(void)
{
	static stTestOverrideConfig_t pstTestOverrideConfig = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

	pstTestOverrideConfig.u1OverridePCtrlState				= M_OVERRIDE_PCTRLSTATE;
	pstTestOverrideConfig.u1OverridePCtrlBoostMode			= M_OVERRIDE_PCTRLBOOSTMODE;
	pstTestOverrideConfig.u1OverrideReqBoostMode			= M_OVERRIDE_REQBOOSTMODE;
	pstTestOverrideConfig.u1OverridePedalSiganl				= M_OVERRIDE_PEDAL_SIGNAL;
	pstTestOverrideConfig.u1OverrideTargetPressure			= M_OVERRIDE_TARGET_PRESSURE;
	pstTestOverrideConfig.u1OverrideWheelTargetPressure		= M_OVERRIDE_WHEEL_TARGET_PRESSURE;
	pstTestOverrideConfig.u1OverrideChamberVolumeMtr		= M_OVERRIDE_CHAMBERVOLUME_MTR;
	pstTestOverrideConfig.u1OverrideChamberVolumeVlv		= M_OVERRIDE_CHAMBERVOLUME_VLV;
	pstTestOverrideConfig.u1OverrideDeltaStroke				= M_OVERRIDE_DELTA_STROKE;
	pstTestOverrideConfig.u1OverrideCut1VlvCtrl				= M_OVERRIDE_CUT1_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideCut2VlvCtrl				= M_OVERRIDE_CUT2_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideSimVlvCtrl				= M_OVERRIDE_SIM_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideCvVlvCtrl				= M_OVERRIDE_CV_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideRelVlvCtrl				= M_OVERRIDE_REL_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideBalVlvCtrl				= M_OVERRIDE_BAL_VLV_CONTROL;
	pstTestOverrideConfig.u1OverridePdVlvCtrl				= M_OVERRIDE_PD_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideWheelInVlvCtrl			= M_OVERRIDE_WHEEL_IN_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideWheelOutVlvCtrl			= M_OVERRIDE_WHEEL_OUT_VLV_CONTROL;

	return (pstTestOverrideConfig);
}

#endif /* (M_IDB_TEST == ENABLE) */
