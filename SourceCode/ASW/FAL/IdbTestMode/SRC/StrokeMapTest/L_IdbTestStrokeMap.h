/*
 * L_IdbTestStrokeMap.h
 *
 *  Created on: 2015. 6. 17.
 *      Author: seokjong.kim@halla.com
 */

#ifndef IDBTESTSTROKEMAP_H_
#define IDBTESTSTROKEMAP_H_

extern stInternalInterface_t LIDB_vTestStrokeMap(stInternalInterface_t* pstInternalIf);
extern stTestOverrideConfig_t LIDB_stTestStorkeMapConfig(void);

#endif /* L_IDBTESTSTROKEMAP_H_ */
