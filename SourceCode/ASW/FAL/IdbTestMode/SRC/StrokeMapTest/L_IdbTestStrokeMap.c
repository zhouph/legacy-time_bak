#include "../../HDR/L_IdbTestMain.h"

#if (M_IDB_TEST == ENABLE)

#include "L_IdbTestStrokeMap.h"
#include "L_IdbTestStrokeMapConfig.h"
#include "../../HDR/L_IdbTestPatternLib.h"

/* Local Definition ***********************************************************/

/* MAP Test Configuration */
#define S16_TEST_DELTA_STROKE_SIZE_DIVERSE		ENABLE /* ENABLE | DISABLE */
#define S16_TEST_DELTA_STROKE_SIZE				(30)
#define S16_TEST_DELTA_STROKE_SIZE_INIT			(10)
#define S16_TEST_DELTA_STROKE_DIVERSED_CNT		((S16_TEST_DELTA_STROKE_SIZE-S16_TEST_DELTA_STROKE_SIZE_INIT)*10)
#define S16_TEST_DELTA_STROKE_SIZE_BACKWARD		(-50)
#define S32_TEST_MAX_STROKE						S32_MTRPOSI_45MM

#define S16_PASS_CUT_HOLE_STROKE				(3000)
#define WHL_VALVE_PATTERN_INDEX_MAX				(10)
#define MAP_TEST_SEQUENCE_INDEX_MAX				(11)			/* 4Wheel, FL, FR, RL, RR, FL+FR, RL+RR, FL+RR, FR+RL, FL+RL, FR+RR */

#define S16_HOLD_STROKE_TIME					(U8_T_100_MS)
#define S16_HOLD_STROKE_TIME_2					(U16_T_10_S)
#define S16_HOLD_TIME_FOR_PROTECT_VALVE			(U16_T_10_S)
#define U8_CHANGE_TEST_CASE_INIT_INDEX			(U8_WHL_BBS_4WHL)

/* TEST PARAMETER SETTING */
#define	TEST_DELTA_STRKRCVR_SIZE				(400)
#define TEST_DELTA_STROKE_0_PULSE				(0)

#define ORIGINE_STROKE_0MM						(0)
/* STROKE RECOVERY TEST */
#define	U16_STRKRCVR_TEST_HOLD_STRKRCVR_TIME	U8_T_100_MS
#define S32_STRKRCVR_TEST_INIT_STROKE			S32_MTRPOSI_1MM
#define S16_INIT_DELTA_STROKE_SIZE				(1)
#define S16_TEST_DELTA_STROKE_0_PULSE			S32_MTRPOSI_0MM
#define S16_STRKRCVR_TEST_MPRESS_LEVEL			S16_MPRESS_70_BAR
#define U16_STRKRCVR_TEST_HOLD_STROKE_TIME		U16_T_5_S
#define S16_TEST_DELTA_STROKE_REAPPLY_SIZE		(100)
#define S32_TEST_ORIGINE_STROKE_0MM				(0)

/* TEST STATE DEFINATION*/
#define ORIGIN_STROKE							(0)
#define FORWARD_STROKE							(1)
#define BACKWARD_STROKE							(2)
#define HOLD_STROKE								(3)
#define HOLD_STROKE2							(4)
#define	RECOVERY_STROKE							(5)
#define	RECOVERY_BF_STROKE						(6)
#define INIT_STROKE_APPLY						(7)
#define INITIAL_PASS_CUT_HOLE					(8)
#define INITIAL_HOLD_STROKE						(9)
#define INITIAL_HOLD_STROKE2					(10)
#define HOLD_FOR_PROTECT_VALVE					(11)

/* Wheel volume test cases */
#define U8_WHL_BBS_4WHL                 		(0)
#define U8_WHL_FL                       		(1)
#define U8_WHL_FR                       		(2)
#define U8_WHL_RL                       		(3)
#define U8_WHL_RR                       		(4)
#define U8_WHL_FRONT                    		(5)
#define U8_WHL_REAR                     		(6)
#define U8_WHL_FL_RR                    		(7)
#define U8_WHL_FR_RL                    		(8)
#define U8_WHL_FL_RL                    		(9)
#define U8_WHL_FR_RR                    		(10)
#define U8_WHL_ALL_NO_CLOSE			    		(11)
#define U8_WHL_DEFAULT                  		(12)

typedef struct
{
	uint8_t u8IdbTestInletFL;
	uint8_t u8IdbTestInletFR;
	uint8_t u8IdbTestInletRL;
	uint8_t u8IdbTestInletRR;
} WhlIntetVV_t;

/* IDB Local Definition *******************************************************/

/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
static stInternalInterface_t LIDB_s16MakeFixedDeltaStrokeSQ(stInternalInterface_t* stInternalIf, int16_t s16TestTargetCircPress);
static WhlIntetVV_t LIDB_vSetWhlValveCmdSQ(uint8_t u8IdbTestReadyFlg, uint16_t u16TestCaseSelect, uint16_t u16PatternSelect, uint8_t u8StrokeDirection);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*******************************************************************************
* FUNCTION NAME:        LIDB_vTestStrokeMap
* CALLED BY:            L_vCallTestIDB()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Main Function for IDB Stroke Map Test
********************************************************************************/
stInternalInterface_t LIDB_vTestStrokeMap(stInternalInterface_t* pstInternalIf)
{
	static uint8_t lu8CompleteReturnToOrigin = FALSE;
	static uint16_t	lu16IdbTestCnt = 0;

	if (pstInternalIf->u8TestReady == 1)
	{
		lu16IdbTestCnt = lu16IdbTestCnt + 1;
		if (pstInternalIf->s32TargetPosi > (HW_MAX_STROKE_PULSE * 8 / 10))
		{
			pstInternalIf->u8TestReady = FALSE;
		}
	}
	else
	{
		lu16IdbTestCnt = 0;
	}

	/*  In which lu8IdbTestReadyFlg & lu8IdbTestChangeWhlVolCnt are reseted, when the test is finished*/
	*pstInternalIf = LIDB_s16MakeFixedDeltaStrokeSQ(pstInternalIf, S16_PCTRL_PRESS_100_BAR);

	return (*pstInternalIf);
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

/*******************************************************************************
* FUNCTION NAME:        LIDB_s16MakeFixedDeltaStrokeSQ
* CALLED BY:            LIDB_vTestStrokeMap()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Generate delta stroke pattern for stroke map test
********************************************************************************/
static stInternalInterface_t LIDB_s16MakeFixedDeltaStrokeSQ(stInternalInterface_t* pstInternalIf, int16_t s16TestTargetCircPress)
{
	static uint8_t lu8IdbTestChangeWhlVolCnt = U8_WHL_BBS_4WHL;
	static uint8_t lu8IdbTestChangeTestCase = U8_CHANGE_TEST_CASE_INIT_INDEX;

	static uint16_t lau16HoldStrokeCnt 		= 0;
	static int16_t lu16PassCutHoleStroke 	= S16_PASS_CUT_HOLE_STROKE;
  #if (S16_TEST_DELTA_STROKE_SIZE_DIVERSE == ENABLE)
	static uint16_t lau16DelStrkSizeDiverseCnt = 0;
  #endif
	WhlIntetVV_t stIdbTestWhlInletVV;
	int16_t s16TargetDeltaStroke 				= TEST_DELTA_STROKE_0_PULSE;

	static uint8_t lau8StrokeDirection 			= ORIGIN_STROKE;
	static uint8_t lu8IdbTestLoggingTriggerFlg = FALSE;
	static uint8_t lu8IdbTestAnalyzerFlg 		= FALSE;

	static uint8_t lu8IdbTestReadyFlg  			= FALSE;
	static uint8_t lu8IdbTestCVV      			= FALSE;
	static uint8_t lu8IdbTestCircuitVV 			= FALSE;
	static uint8_t lu8IdbTestReleaseVV			= FALSE;
	static int8_t  lss8IdbTestChamberVolume		= FALSE;


	lu8IdbTestReadyFlg = pstInternalIf->u8TestReady;

	if (lu8IdbTestReadyFlg == TRUE)
	{
		lu8IdbTestCVV = 1;
		lss8IdbTestChamberVolume = U8_BOOST_LOW_PRESS;
		lu8IdbTestCircuitVV = 1;
		lu8IdbTestReleaseVV = 1;
		switch (lau8StrokeDirection)
		{
			case ORIGIN_STROKE:
				lu8IdbTestCVV = 1;
				lu8IdbTestLoggingTriggerFlg = 1;

				if (lau16HoldStrokeCnt < S16_HOLD_STROKE_TIME)
				{
					lau16HoldStrokeCnt = lau16HoldStrokeCnt + 1;
					s16TargetDeltaStroke = TEST_DELTA_STROKE_0_PULSE;
					lu8IdbTestAnalyzerFlg = 0;
				}
				else
				{
					lau16HoldStrokeCnt = 0;
					lu8IdbTestAnalyzerFlg = 1;
					lau8StrokeDirection = INITIAL_PASS_CUT_HOLE;
				  #if (S16_TEST_DELTA_STROKE_SIZE_DIVERSE == ENABLE)
					s16TargetDeltaStroke = S16_TEST_DELTA_STROKE_SIZE_INIT + lau16DelStrkSizeDiverseCnt / 10;

					lau16DelStrkSizeDiverseCnt = lau16DelStrkSizeDiverseCnt + 1;
				  #else
					s16TargetDeltaStroke = S16_TEST_DELTA_STROKE_SIZE;
				  #endif
				}
				break;
			case INITIAL_PASS_CUT_HOLE:
				lau16HoldStrokeCnt = 0;
				lau8StrokeDirection = FORWARD_STROKE;
				  #if (S16_TEST_DELTA_STROKE_SIZE_DIVERSE == ENABLE)
				s16TargetDeltaStroke = S16_TEST_DELTA_STROKE_SIZE_INIT + lau16DelStrkSizeDiverseCnt / 10;
				lau16DelStrkSizeDiverseCnt = lau16DelStrkSizeDiverseCnt + 1;

				  #else
					s16TargetDeltaStroke = S16_TEST_DELTA_STROKE_SIZE;
				  #endif
				break;
			case FORWARD_STROKE:
				if (pstInternalIf->s16TestCircuitP1_100bar < s16TestTargetCircPress)
				{
				  #if (S16_TEST_DELTA_STROKE_SIZE_DIVERSE == ENABLE)
					s16TargetDeltaStroke = S16_TEST_DELTA_STROKE_SIZE_INIT + lau16DelStrkSizeDiverseCnt / 10;
					if(lau16DelStrkSizeDiverseCnt < S16_TEST_DELTA_STROKE_DIVERSED_CNT)
					{
						lau16DelStrkSizeDiverseCnt = lau16DelStrkSizeDiverseCnt + 1;
					}
					else
					{
						lau16DelStrkSizeDiverseCnt = S16_TEST_DELTA_STROKE_DIVERSED_CNT;
					}
				  #else
					s16TargetDeltaStroke = S16_TEST_DELTA_STROKE_SIZE;
				  #endif
					if(pstInternalIf->s32TargetPosi >= S32_TEST_MAX_STROKE)
					{
						s16TargetDeltaStroke = TEST_DELTA_STROKE_0_PULSE;
						if(lau16HoldStrokeCnt < U8_T_500_MS)
						{
							lau16HoldStrokeCnt = lau16HoldStrokeCnt + 1;
							s16TargetDeltaStroke = TEST_DELTA_STROKE_0_PULSE;
							if(pstInternalIf->s32MeasuredPosi >= S32_TEST_MAX_STROKE)
							{
								lau16HoldStrokeCnt = 0;
								lau8StrokeDirection = BACKWARD_STROKE;
								s16TargetDeltaStroke = S16_TEST_DELTA_STROKE_SIZE_BACKWARD;
							}
						}
						else
						{
							lau16HoldStrokeCnt = 0;
							lau8StrokeDirection = BACKWARD_STROKE;
							s16TargetDeltaStroke = S16_TEST_DELTA_STROKE_SIZE_BACKWARD;
						}
					}
				}
				else	/* Stroke Stop when MCP is over than s16TestTargetCircPress [bar] */
				{
					s16TargetDeltaStroke = TEST_DELTA_STROKE_0_PULSE;
					lau16HoldStrokeCnt = 0;
					lau8StrokeDirection = HOLD_STROKE;
				}

				break;
			case HOLD_STROKE:
				if (lau16HoldStrokeCnt < S16_HOLD_STROKE_TIME)
				{
					lau16HoldStrokeCnt = lau16HoldStrokeCnt + 1;
					s16TargetDeltaStroke = TEST_DELTA_STROKE_0_PULSE;
				}
				else
				{
					lau16HoldStrokeCnt = 0;
					lau8StrokeDirection = BACKWARD_STROKE;
					s16TargetDeltaStroke = S16_TEST_DELTA_STROKE_SIZE_BACKWARD;

				}

				break;
			case BACKWARD_STROKE:
				if (pstInternalIf->s32TargetPosi > S16_TEST_DELTA_STROKE_SIZE_BACKWARD)
				{
					s16TargetDeltaStroke = S16_TEST_DELTA_STROKE_SIZE_BACKWARD;
				}
				else if(pstInternalIf->s32TargetPosi > ORIGINE_STROKE_0MM)
				{
					s16TargetDeltaStroke = ORIGINE_STROKE_0MM  - pstInternalIf->s32TargetPosi;
				}
				else
				{
						lau16HoldStrokeCnt = 0;
						lu8IdbTestAnalyzerFlg = 0;
						lau8StrokeDirection = HOLD_STROKE2;
						s16TargetDeltaStroke = TEST_DELTA_STROKE_0_PULSE;
						lu8IdbTestChangeWhlVolCnt = lu8IdbTestChangeWhlVolCnt + 1;
						lu8IdbTestCVV = 0;
				}
				break;
			case HOLD_STROKE2:

			  #if (S16_TEST_DELTA_STROKE_SIZE_DIVERSE == ENABLE)
				lau16DelStrkSizeDiverseCnt = 0;
			  #endif
				if (lau16HoldStrokeCnt < S16_HOLD_STROKE_TIME_2)
				{
					lau16HoldStrokeCnt = lau16HoldStrokeCnt + 1;
					s16TargetDeltaStroke = TEST_DELTA_STROKE_0_PULSE;
					lu8IdbTestCVV = 0;
					lss8IdbTestChamberVolume = U8_BOOST_READY;
				}
				else
				{
					lau16HoldStrokeCnt = 0;

					if(lu8IdbTestChangeWhlVolCnt < (WHL_VALVE_PATTERN_INDEX_MAX - 1))
					{
						lu8IdbTestChangeWhlVolCnt = lu8IdbTestChangeWhlVolCnt + 1;
						lau8StrokeDirection = INITIAL_PASS_CUT_HOLE;
						lu8IdbTestAnalyzerFlg = 1;
					}
					else
					{
						lau8StrokeDirection = HOLD_FOR_PROTECT_VALVE;
						s16TargetDeltaStroke = TEST_DELTA_STROKE_0_PULSE;
						lu8IdbTestChangeWhlVolCnt = (WHL_VALVE_PATTERN_INDEX_MAX - 1);
						lu8IdbTestCVV = 0;
						lss8IdbTestChamberVolume = U8_BOOST_READY;
					}
				}

				break;
			case HOLD_FOR_PROTECT_VALVE:
				/* Hold Test for Protect Valve */
				lu8IdbTestCVV = 0;
				lss8IdbTestChamberVolume = U8_BOOST_READY;
				lu8IdbTestCircuitVV = 0;
				lu8IdbTestReleaseVV = 0;
				lu8IdbTestLoggingTriggerFlg = 0;
				if (lau16HoldStrokeCnt < S16_HOLD_TIME_FOR_PROTECT_VALVE)
				{
					lau16HoldStrokeCnt = lau16HoldStrokeCnt + 1;
					s16TargetDeltaStroke = TEST_DELTA_STROKE_0_PULSE;
				}
				else
				{
					lau16HoldStrokeCnt = 0;

					if(lu8IdbTestChangeTestCase < (MAP_TEST_SEQUENCE_INDEX_MAX - 1))
					{
						lu8IdbTestChangeWhlVolCnt = 0;
						lu8IdbTestChangeTestCase = lu8IdbTestChangeTestCase + 1;
						lau8StrokeDirection = INITIAL_PASS_CUT_HOLE;
						lu8IdbTestAnalyzerFlg = 1;
						lu8IdbTestLoggingTriggerFlg = 1;
					#if (S16_TEST_DELTA_STROKE_SIZE_DIVERSE == ENABLE)
						s16TargetDeltaStroke = S16_TEST_DELTA_STROKE_SIZE_INIT + lau16DelStrkSizeDiverseCnt / 10;
						lau16DelStrkSizeDiverseCnt = lau16DelStrkSizeDiverseCnt + 1;
					#else
						s16TargetDeltaStroke = (S16_TEST_DELTA_STROKE_SIZE);
					#endif
					}
					else
					{
						/* Exit Test Sequence */
						lau8StrokeDirection = ORIGIN_STROKE;
						s16TargetDeltaStroke = TEST_DELTA_STROKE_0_PULSE;
						lu8IdbTestReadyFlg = FALSE;
						lu8IdbTestChangeWhlVolCnt = 0;
						lu8IdbTestChangeTestCase = U8_CHANGE_TEST_CASE_INIT_INDEX;
					}
				}
				break;

			default :
				lau8StrokeDirection = ORIGIN_STROKE;
				lu8IdbTestReadyFlg = FALSE;
				lu8IdbTestChangeWhlVolCnt = 0;
				break;
		}
	}
	else
	{
		lss8IdbTestChamberVolume = U8_BOOST_READY;
		lau8StrokeDirection = ORIGIN_STROKE;
		lu8IdbTestChangeWhlVolCnt = 0;
		lu8IdbTestChangeTestCase = U8_CHANGE_TEST_CASE_INIT_INDEX;
		lau16HoldStrokeCnt = 0;
		lu16PassCutHoleStroke = S16_PASS_CUT_HOLE_STROKE;
		lu8IdbTestLoggingTriggerFlg = 0;
		lu8IdbTestAnalyzerFlg = 0;
	#if (S16_TEST_DELTA_STROKE_SIZE_DIVERSE == ENABLE)
		uint16_t lau16DelStrkSizeDiverseCnt = 0;
	#endif
		lu8IdbTestCVV = 0;
	}

	stIdbTestWhlInletVV = LIDB_vSetWhlValveCmdSQ(lu8IdbTestReadyFlg, lu8IdbTestChangeTestCase, lu8IdbTestChangeWhlVolCnt, lau8StrokeDirection);

	pstInternalIf->u8TestInletActFlg_FL 					= stIdbTestWhlInletVV.u8IdbTestInletFL;
	pstInternalIf->u8TestInletActFlg_FR 					= stIdbTestWhlInletVV.u8IdbTestInletFR;
	pstInternalIf->u8TestInletActFlg_RL 					= stIdbTestWhlInletVV.u8IdbTestInletRL;
	pstInternalIf->u8TestInletActFlg_RR 					= stIdbTestWhlInletVV.u8IdbTestInletRR;

	pstInternalIf->u8TestReady 								= lu8IdbTestReadyFlg;
	pstInternalIf->s8PCtrlBoostMode							= S8_BOOST_MODE_RESPONSE_L5;
	pstInternalIf->s8PCtrlState                             = LIDB_s8TestPressCtrlState(pstInternalIf->u8TestReady);
	pstInternalIf->s8MTRPChamberVolume 						= lss8IdbTestChamberVolume;
	pstInternalIf->s8VLVPChamberVolume 						= lss8IdbTestChamberVolume;
	pstInternalIf->u8TestActFlg_Cut 						= lu8IdbTestCVV     ;
	pstInternalIf->u8TestRespLv_Cut							= U8_VLV_RESPONSE_LV_NORMAL;
	pstInternalIf->u8TestActFlg_CV 							= lu8IdbTestCircuitVV;
	pstInternalIf->u8TestRespLv_CV							= U8_VLV_RESPONSE_LV_NORMAL;
	pstInternalIf->u8TestActFlg_Rel 						= lu8IdbTestReleaseVV;
	pstInternalIf->u8TestRespLv_Rel							= U8_VLV_RESPONSE_LV_NORMAL;
	pstInternalIf->s16TestTgtDeltaStroke 					= s16TargetDeltaStroke;

	pstInternalIf->stLogInterface.s16TestStateMon				= lau8StrokeDirection;
	pstInternalIf->stLogInterface.u8StrokeDirection				= lau8StrokeDirection;
	pstInternalIf->stLogInterface.u8IdbTestLoggingTriggerFlg	= lu8IdbTestLoggingTriggerFlg;
	pstInternalIf->stLogInterface.u8IdbTestAnalyzerFlg			= lu8IdbTestAnalyzerFlg;
	pstInternalIf->stLogInterface.u8IdbTestChangeWhlVolCnt		= lu8IdbTestChangeWhlVolCnt;
	pstInternalIf->stLogInterface.u8IdbTestChangeTestCase		= lu8IdbTestChangeTestCase;

	return (*pstInternalIf);
}

/*******************************************************************************
* FUNCTION NAME:        LIDB_vSetWhlValveCmdSQ
* CALLED BY:            LIDB_vTestStrokeMap()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Set Inlet valve command for Map test sequence
********************************************************************************/
static WhlIntetVV_t LIDB_vSetWhlValveCmdSQ(uint8_t u8IdbTestReadyFlg, uint16_t u16TestCaseSelect, uint16_t u16PatternSelect, uint8_t u8StrokeDirection)
{
	WhlIntetVV_t stIdbTestWhlInletVV;
	uint8_t lu8IdbTestInletFL = 0;
	uint8_t lu8IdbTestInletFR = 0;
	uint8_t lu8IdbTestInletRL = 0;
	uint8_t lu8IdbTestInletRR = 0;

    static uint16_t lau16WhlValveCtrlCnt = 0;
	static uint8_t ldahbu8ValveCmdState = 0;

	/* lau8PatternValveCmd[0][0] must be U8_WHL_DEFAULT */
	static const uint8_t lau8PatternValveCmd[MAP_TEST_SEQUENCE_INDEX_MAX][WHL_VALVE_PATTERN_INDEX_MAX] = {
		{U8_WHL_BBS_4WHL, U8_WHL_DEFAULT, U8_WHL_BBS_4WHL, U8_WHL_DEFAULT, U8_WHL_BBS_4WHL, U8_WHL_DEFAULT, U8_WHL_BBS_4WHL, U8_WHL_DEFAULT, U8_WHL_BBS_4WHL, U8_WHL_DEFAULT}, /* 4Wheel */
		{U8_WHL_FL,      U8_WHL_DEFAULT, U8_WHL_FL,      U8_WHL_DEFAULT, U8_WHL_FL,      U8_WHL_DEFAULT, U8_WHL_FL,      U8_WHL_DEFAULT, U8_WHL_FL,      U8_WHL_DEFAULT}, /* FL     */
		{U8_WHL_FR,      U8_WHL_DEFAULT, U8_WHL_FR,      U8_WHL_DEFAULT, U8_WHL_FR,      U8_WHL_DEFAULT, U8_WHL_FR,      U8_WHL_DEFAULT, U8_WHL_FR,      U8_WHL_DEFAULT}, /* FR     */
		{U8_WHL_RL,      U8_WHL_DEFAULT, U8_WHL_RL,      U8_WHL_DEFAULT, U8_WHL_RL,      U8_WHL_DEFAULT, U8_WHL_RL,      U8_WHL_DEFAULT, U8_WHL_RL,      U8_WHL_DEFAULT}, /* RL     */
		{U8_WHL_RR,      U8_WHL_DEFAULT, U8_WHL_RR,      U8_WHL_DEFAULT, U8_WHL_RR,      U8_WHL_DEFAULT, U8_WHL_RR,      U8_WHL_DEFAULT, U8_WHL_RR,      U8_WHL_DEFAULT}, /* RR     */
		{U8_WHL_FRONT,   U8_WHL_DEFAULT, U8_WHL_FRONT,   U8_WHL_DEFAULT, U8_WHL_FRONT,   U8_WHL_DEFAULT, U8_WHL_FRONT,   U8_WHL_DEFAULT, U8_WHL_FRONT,   U8_WHL_DEFAULT}, /* FL+FR  */
		{U8_WHL_REAR,    U8_WHL_DEFAULT, U8_WHL_REAR,    U8_WHL_DEFAULT, U8_WHL_REAR,    U8_WHL_DEFAULT, U8_WHL_REAR,    U8_WHL_DEFAULT, U8_WHL_REAR,    U8_WHL_DEFAULT}, /* RL+RR  */
		{U8_WHL_FL_RR,   U8_WHL_DEFAULT, U8_WHL_FL_RR,   U8_WHL_DEFAULT, U8_WHL_FL_RR,   U8_WHL_DEFAULT, U8_WHL_FL_RR,   U8_WHL_DEFAULT, U8_WHL_FL_RR,   U8_WHL_DEFAULT}, /* FL+RR  */
		{U8_WHL_FR_RL,   U8_WHL_DEFAULT, U8_WHL_FR_RL,   U8_WHL_DEFAULT, U8_WHL_FR_RL,   U8_WHL_DEFAULT, U8_WHL_FR_RL,   U8_WHL_DEFAULT, U8_WHL_FR_RL,   U8_WHL_DEFAULT}, /* FR+RL  */
		{U8_WHL_FL_RL,   U8_WHL_DEFAULT, U8_WHL_FL_RL,   U8_WHL_DEFAULT, U8_WHL_FL_RL,   U8_WHL_DEFAULT, U8_WHL_FL_RL,   U8_WHL_DEFAULT, U8_WHL_FL_RL,   U8_WHL_DEFAULT}, /* FL+RL  */
		{U8_WHL_FR_RR,   U8_WHL_DEFAULT, U8_WHL_FR_RR,   U8_WHL_DEFAULT, U8_WHL_FR_RR,   U8_WHL_DEFAULT, U8_WHL_FR_RR,   U8_WHL_DEFAULT, U8_WHL_FR_RR,   U8_WHL_DEFAULT}  /* FR+RR  */
	};

	ldahbu8ValveCmdState = lau8PatternValveCmd[u16TestCaseSelect][u16PatternSelect];

	if(u8IdbTestReadyFlg == TRUE)
	{
		if(u8StrokeDirection==HOLD_FOR_PROTECT_VALVE) {ldahbu8ValveCmdState=U8_WHL_BBS_4WHL;}/*{ldahbu8ValveCmdState=U8_WHL_ALL_NO_CLOSE;}*/
		switch(ldahbu8ValveCmdState)
		{
		case U8_WHL_FR_RL:
			lu8IdbTestInletFL = 1;
			lu8IdbTestInletRR = 1;
			break;
		case U8_WHL_FL_RR:
			lu8IdbTestInletFR = 1;
			lu8IdbTestInletRL = 1;
			break;
		case U8_WHL_FR_RR:
			lu8IdbTestInletFL = 1;
			lu8IdbTestInletRL = 1;
			break;
		case U8_WHL_FL_RL:
			lu8IdbTestInletFR = 1;
			lu8IdbTestInletRR = 1;
			break;
		case U8_WHL_FL:
			lu8IdbTestInletFR = 1;
			lu8IdbTestInletRL = 1;
			lu8IdbTestInletRR = 1;
			break;
		case U8_WHL_FR:
			lu8IdbTestInletFL = 1;
			lu8IdbTestInletRL = 1;
			lu8IdbTestInletRR = 1;
			break;
		case U8_WHL_RL:
			lu8IdbTestInletFL = 1;
			lu8IdbTestInletFR = 1;
			lu8IdbTestInletRR = 1;
			break;
		case U8_WHL_RR:
			lu8IdbTestInletFL = 1;
			lu8IdbTestInletFR = 1;
			lu8IdbTestInletRL = 1;
			break;
		case U8_WHL_FRONT:
			lu8IdbTestInletRL = 1;
			lu8IdbTestInletRR = 1;
			break;
		case U8_WHL_REAR:
			lu8IdbTestInletFL = 1;
			lu8IdbTestInletFR = 1;
			break;
		case U8_WHL_ALL_NO_CLOSE:
			lu8IdbTestInletFL = 1;
			lu8IdbTestInletFR = 1;
			lu8IdbTestInletRL = 1;
			lu8IdbTestInletRR = 1;
			break;
		case U8_WHL_BBS_4WHL:
			lu8IdbTestInletFL = 0;
			lu8IdbTestInletFR = 0;
			lu8IdbTestInletRL = 0;
			lu8IdbTestInletRR = 0;
			break;
		default:
			lu8IdbTestInletFL = 0;
			lu8IdbTestInletFR = 0;
			lu8IdbTestInletRL = 0;
			lu8IdbTestInletRR = 0;
			break;
		}
	}
	else
	{
		lau16WhlValveCtrlCnt = 0;
		ldahbu8ValveCmdState = 0;
		lu8IdbTestInletFL = 0;
		lu8IdbTestInletFR = 0;
		lu8IdbTestInletRL = 0;
		lu8IdbTestInletRR = 0;
	}

	stIdbTestWhlInletVV.u8IdbTestInletFL = lu8IdbTestInletFL;
	stIdbTestWhlInletVV.u8IdbTestInletFR = lu8IdbTestInletFR;
	stIdbTestWhlInletVV.u8IdbTestInletRL = lu8IdbTestInletRL;
	stIdbTestWhlInletVV.u8IdbTestInletRR = lu8IdbTestInletRR;

	return stIdbTestWhlInletVV;
}

stTestOverrideConfig_t LIDB_stTestStorkeMapConfig(void)
{
	static stTestOverrideConfig_t pstTestOverrideConfig = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

	pstTestOverrideConfig.u1OverridePCtrlState				= M_OVERRIDE_PCTRLSTATE;
	pstTestOverrideConfig.u1OverridePCtrlBoostMode			= M_OVERRIDE_PCTRLBOOSTMODE;
	pstTestOverrideConfig.u1OverrideReqBoostMode			= M_OVERRIDE_REQBOOSTMODE;
	pstTestOverrideConfig.u1OverridePedalSiganl				= M_OVERRIDE_PEDAL_SIGNAL;
	pstTestOverrideConfig.u1OverrideTargetPressure			= M_OVERRIDE_TARGET_PRESSURE;
	pstTestOverrideConfig.u1OverrideWheelTargetPressure		= M_OVERRIDE_WHEEL_TARGET_PRESSURE;
	pstTestOverrideConfig.u1OverrideChamberVolumeMtr		= M_OVERRIDE_CHAMBERVOLUME_MTR;
	pstTestOverrideConfig.u1OverrideChamberVolumeVlv		= M_OVERRIDE_CHAMBERVOLUME_VLV;
	pstTestOverrideConfig.u1OverrideDeltaStroke				= M_OVERRIDE_DELTA_STROKE;
	pstTestOverrideConfig.u1OverrideCut1VlvCtrl				= M_OVERRIDE_CUT1_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideCut2VlvCtrl				= M_OVERRIDE_CUT2_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideSimVlvCtrl				= M_OVERRIDE_SIM_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideCvVlvCtrl				= M_OVERRIDE_CV_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideRelVlvCtrl				= M_OVERRIDE_REL_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideBalVlvCtrl				= M_OVERRIDE_BAL_VLV_CONTROL;
	pstTestOverrideConfig.u1OverridePdVlvCtrl				= M_OVERRIDE_PD_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideWheelInVlvCtrl			= M_OVERRIDE_WHEEL_IN_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideWheelOutVlvCtrl			= M_OVERRIDE_WHEEL_OUT_VLV_CONTROL;

	return (pstTestOverrideConfig);
}

#endif/*M_IDB_TEST*/
