#include "../HDR/L_IdbTestMain.h"

#if (M_IDB_TEST == ENABLE)

#include "./ChamberVolume/L_IdbTestChamberVolume.h"
#include "./DeltaStrokeTest/L_IdbDeltaStrokeTest.h"
#include "./PedalSignal/L_IdbTestPedalSignal.h"
#include "./PressurePerformanceTest/L_IdbTestPressPerformance.h"
#include "./SingleTargetPattern/L_IdbTestSingleTargetTest.h"
#include "./StrokeMapTest/L_IdbTestStrokeMap.h"
#include "./SingleValveTest/L_IdbTestSingleValve.h"
#include "Acmctl_Ctrl.h"

/*Global Type Declaration ******************************************************/

/* Test Interface Variables */

IdbTest_Output_t stIdbTest_Output;

stTestOverrideConfig_t stTestOverrideConfigNull			= {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
stTestOverrideConfig_t stTestOverrideConfig 			= {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
stTestOverrideConfig_t stTestOverrideConfigUserSet 		= {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
stInternalInterface_t stInternalInterface;
/* Calibration Variable */
uint8_t lidbu8OverrideTestMode = M_IDB_TEST_MODE;
uint8_t lidbu8OverrideUpdateTrigger = FALSE;

/* Input from Other Components *****************************/
extern EscSwtStInfo_t Eem_MainEscSwtStInfo;
extern Det_5msCtrlPedlTrvlFinal_t Det_5msCtrlPedlTrvlFinal;
extern Spc_5msCtrlBlsFild_t Spc_5msCtrlBlsFild;
extern IdbMotPosnInfo_t Mcc_1msCtrlIdbMotPosnInfo;
extern Msp_CtrlMotRotgAgSigInfo_t Mcc_1msCtrlMotRotgAgSigInfo;
#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
extern PistPFildInfo_t Spc_5msCtrlPistPFildInfo;
#else
extern CircPFildInfo_t Spc_5msCtrlCircPFildInfo;
#endif
extern MotRotgAgSigInfo_t Msp_CtrlMotRotgAgSigInfo;
/***************************************************************/

/* LocalFunction prototype ***************************************************/

static IdbTest_Input_t LIDB_vGetIdbTestInputData(void);
static void LIDB_vSetIdbTestOutputData(stInternalInterface_t* stInternalInterface_r);


#if (__HW_TEST_TRIGERED_MODE == __HW_TEST_TRIGERED_BY_PEDAL)
static uint8_t LIDB_vTriggerIDBHWTestByPedal(int16_t s16PedalTravel, uint8_t u8TestReadyFlg);
#else /* (__HW_TEST_TRIGERED_MODE == __HW_TEST_TRIGERED_BY_TCS_SW) */
static uint8_t LIDB_vTriggerIDBHWTestByTCSSwitch(uint8_t u8TCSDisabledBySW, uint8_t u8TestReadyFlg);
#endif

void L_vCallTestIDB(void)
{
	static IdbTest_Input_t stIdbTest_Input;
	int16_t s16IdbTestPedlTrvlFinal = Det_5msCtrlPedlTrvlFinal;
	uint8_t u8IdbTestBlsFild = Spc_5msCtrlBlsFild;

	/* Override Configuration Update - Begin */
	if (stInternalInterface.u8TestMode != lidbu8OverrideTestMode)
	{
		stInternalInterface.u8TestMode 						= lidbu8OverrideTestMode;
		stInternalInterface.s16PCtrlActThreshPress			= M_PCTRL_ACT_THRESHOLD;
		switch(stInternalInterface.u8TestMode)
		{
		case M_IDB_STROKE_MAP_TEST:
			stTestOverrideConfig = LIDB_stTestStorkeMapConfig();
			break;
		case M_IDB_GEN_PEDALSIG_TEST:
			break;
		case M_IDB_GEN_TGTPRESS_TEST:
			break;
		case M_IDB_GEN_TGTSTROKE_TEST:
			stTestOverrideConfig = LIDB_stTestTargetStrokeConfig();
			break;
		case M_IDB_PRESSURE_PERFORMANCE_TEST:
			stTestOverrideConfig = LIDB_stPressurePerformanceConfig();
			break;
		case M_IDB_SINGLE_TARGET_PATTERN_TEST:
			stTestOverrideConfig = LIDB_stSingleTargetPatternConfig();
			break;
		case M_IDB_CHAMBER_VOLUME_CHANGE_TEST:
			stTestOverrideConfig = LIDB_stChamberVolumeConfig();
			break;
		case M_IDB_SINGLE_VALVE_TEST:
			stTestOverrideConfig = LIDB_stSingleValveConfig();
			break;

		default:
			break;
		}
		stInternalInterface.stTestOverrideConfig 				= stTestOverrideConfig;
		stTestOverrideConfigUserSet 							= stTestOverrideConfig;
	}
	else
	{
		/* Override Configuration Update by Trigger */
		if (lidbu8OverrideUpdateTrigger == TRUE)
		{
			lidbu8OverrideUpdateTrigger 				= FALSE;
			stTestOverrideConfig 						= stTestOverrideConfigUserSet;
		}
		else { }
		stInternalInterface.stTestOverrideConfig 	= stTestOverrideConfig;
	}
	/* Override Configuration Update - End */

	stIdbTest_Input = LIDB_vGetIdbTestInputData();

	stInternalInterface.s32TargetPosi              = stIdbTest_Input.m_ls32TestTgtPosi;
	stInternalInterface.s32MeasuredPosi            = stIdbTest_Input.m_ls32TestStkPosnmeasd;
	stInternalInterface.s16TestCircuitP1_100bar    = stIdbTest_Input.m_ls16TestCircuitP1_100bar;
	stInternalInterface.s16TestMotMechAngleSpdFild = stIdbTest_Input.m_ls16TestMotMechAngleSpdFild;

#if (__HW_TEST_TRIGERED_MODE == __HW_TEST_TRIGERED_BY_PEDAL)
	stInternalInterface.u8TestReady = LIDB_vTriggerIDBHWTestByPedal(s16IdbTestPedlTrvlFinal, stInternalInterface.u8TestReady);
#else
	stInternalInterface.u8TestReady = LIDB_vTriggerIDBHWTestByTCSSwitch(stIdbTest_Input.m_lu8TestTCSDisabledBySW, stInternalInterface.u8TestReady);
#endif

	switch(stInternalInterface.u8TestMode)
	{
	case M_IDB_STROKE_MAP_TEST:
		stInternalInterface = LIDB_vTestStrokeMap(&stInternalInterface);
		break;
	case M_IDB_GEN_PEDALSIG_TEST:
		break;
	case M_IDB_GEN_TGTPRESS_TEST:
		break;
	case M_IDB_GEN_TGTSTROKE_TEST:
		stInternalInterface = LIDB_vGenTestTargetStrokeIDB(&stInternalInterface);
		break;
	case M_IDB_PRESSURE_PERFORMANCE_TEST:
		stInternalInterface = LIDB_vTestPressurePerformance(&stInternalInterface);
		break;
	case M_IDB_SINGLE_TARGET_PATTERN_TEST:
		stInternalInterface = LIDB_vTestTargetGenSingleShape(&stInternalInterface);
		break;
	case M_IDB_CHAMBER_VOLUME_CHANGE_TEST:
		stInternalInterface = LIDB_vChamberVolumeTest(&stInternalInterface);
		break;
	case M_IDB_SINGLE_VALVE_TEST:
		stInternalInterface = LIDB_vTestSingleValve(&stInternalInterface);
		break;
	default:
		break;
	}

	LIDB_vSetIdbTestOutputData(&stInternalInterface);
}

static IdbTest_Input_t LIDB_vGetIdbTestInputData(void)
{
	IdbTest_Input_t stIdbTest_Input_w;

	stIdbTest_Input_w.m_ls16TestPedlTrvlFinal 		= Det_5msCtrlPedlTrvlFinal;
	stIdbTest_Input_w.m_lu8TestBlsFild 				= Eem_MainEscSwtStInfo.TcsDisabledBySwt;
	stIdbTest_Input_w.m_lu8TestTCSDisabledBySW 		= Eem_MainEscSwtStInfo.TcsDisabledBySwt;
	stIdbTest_Input_w.m_ls32TestTgtPosi 			= Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn;
	stIdbTest_Input_w.m_ls32TestStkPosnmeasd 		= Mcc_1msCtrlMotRotgAgSigInfo.StkPosnMeasd;
	stIdbTest_Input_w.m_ls16TestMotMechAngleSpdFild = Msp_CtrlMotRotgAgSigInfo.MotMechAngleSpdFild;
	stIdbTest_Input_w.m_ls16MotIqMeasd 				= Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIqMeasd;

#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
	stIdbTest_Input_w.m_ls16TestCircuitP1_100bar = Spc_5msCtrlPistPFildInfo.PistPFild_1_100Bar;
#else
	stIdbTest_Input_w.m_ls16TestCircuitP1_100bar = Spc_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar;
#endif

	return stIdbTest_Input_w;
}

static void LIDB_vSetIdbTestOutputData(stInternalInterface_t* stInternalInterface_r)
{

	stIdbTest_Output.m_lstTestOverrideConfig						= (stInternalInterface_r->u8TestReady == TRUE) ? stInternalInterface_r->stTestOverrideConfig : stTestOverrideConfigNull;

	stIdbTest_Output.m_lu8TestMode									= stInternalInterface_r->u8TestMode;

	stIdbTest_Output.m_lu8TestReady 								= stInternalInterface_r->u8TestReady;

	/* PCtrl State & BoostMode */
	stIdbTest_Output.m_ls8PCtrlState								= stInternalInterface_r->s8PCtrlState;
	stIdbTest_Output.m_ls8PCtrlBoostMode							= stInternalInterface_r->s8PCtrlBoostMode;
	stIdbTest_Output.m_ls8ReqBoostMode								= stInternalInterface_r->s8ReqBoostMode;

	stIdbTest_Output.m_lss16IdbTestPedal_1_10mm						= stInternalInterface_r->s16IdbTestPedal_1_10mm;

	/* Chamber Volume */
	stIdbTest_Output.m_lss8MTRPChamberVolume 						= stInternalInterface_r->s8MTRPChamberVolume;
	stIdbTest_Output.m_lss8VLVPChamberVolume 						= stInternalInterface_r->s8VLVPChamberVolume;

	/* Delta Stroke Target */
	stIdbTest_Output.m_ls16TestTgtDeltaStroke						= stInternalInterface_r->s16TestTgtDeltaStroke;

	/* Target Pressure */
	stIdbTest_Output.m_ls16TestTargetPress 							= stInternalInterface_r->s16TestTargetPress;
	stIdbTest_Output.m_ls16TestTargetPressDel 						= stInternalInterface_r->s16TestDeltaTargetPress;

	/* Wheel Target Pressure */
	stIdbTest_Output.m_ls16TestTP_FL 								= stInternalInterface_r->s16TestTargetP_FL;
	stIdbTest_Output.m_ls16TestTP_FR 								= stInternalInterface_r->s16TestTargetP_FR;
	stIdbTest_Output.m_ls16TestTP_RL 								= stInternalInterface_r->s16TestTargetP_RL;
	stIdbTest_Output.m_ls16TestTP_RR 								= stInternalInterface_r->s16TestTargetP_RR;

	/* Wheel Target Pressure Rate */
	stIdbTest_Output.m_ls16TestTPRate_FL 							= stInternalInterface_r->s16TestTargetPRate_FL;
	stIdbTest_Output.m_ls16TestTPRate_FR 							= stInternalInterface_r->s16TestTargetPRate_FR;
	stIdbTest_Output.m_ls16TestTPRate_RL 							= stInternalInterface_r->s16TestTargetPRate_RL;
	stIdbTest_Output.m_ls16TestTPRate_RR 							= stInternalInterface_r->s16TestTargetPRate_RR;

	stIdbTest_Output.m_lu8PCtrlActFlg								= stInternalInterface_r->u8PCtrlActFlg;

	stIdbTest_Output.m_lsTest_CUT1.lau8VlvCtrlState 				= (stInternalInterface_r->u8TestActFlg_Cut == TRUE) ? U8_VALVE_STATE_ACTIVE : U8_VALVE_STATE_READY;
	stIdbTest_Output.m_lsTest_CUT2.lau8VlvCtrlState 				= (stInternalInterface_r->u8TestActFlg_Cut == TRUE) ? U8_VALVE_STATE_ACTIVE : U8_VALVE_STATE_READY;
	stIdbTest_Output.m_lsTest_SIM.lau8VlvCtrlState  				= (stInternalInterface_r->u8TestActFlg_Sim == TRUE) ? U8_VALVE_STATE_ACTIVE : U8_VALVE_STATE_READY;
	stIdbTest_Output.m_lsTest_CV.lau8VlvCtrlState 					= (stInternalInterface_r->u8TestActFlg_CV  == TRUE) ? U8_VALVE_STATE_ACTIVE : U8_VALVE_STATE_READY;
	stIdbTest_Output.m_lsTest_RELV.lau8VlvCtrlState 				= (stInternalInterface_r->u8TestActFlg_Rel == TRUE) ? U8_VALVE_STATE_ACTIVE : U8_VALVE_STATE_READY;
	stIdbTest_Output.m_lsTest_PDV.lau8VlvCtrlState 					= (stInternalInterface_r->u8TestActFlg_PD  == TRUE) ? U8_VALVE_STATE_ACTIVE : U8_VALVE_STATE_READY;
	stIdbTest_Output.m_lsTest_BalV.lau8VlvCtrlState 				= (stInternalInterface_r->u8TestActFlg_Bal == TRUE) ? U8_VALVE_STATE_ACTIVE : U8_VALVE_STATE_READY;

	stIdbTest_Output.m_lsTest_CUT1.lau8VlvCtrlRespLv 				= stInternalInterface_r->u8TestRespLv_Cut;
	stIdbTest_Output.m_lsTest_CUT2.lau8VlvCtrlRespLv 				= stInternalInterface_r->u8TestRespLv_Cut;
	stIdbTest_Output.m_lsTest_SIM.lau8VlvCtrlRespLv  				= stInternalInterface_r->u8TestRespLv_Sim;
	stIdbTest_Output.m_lsTest_CV.lau8VlvCtrlRespLv   				= stInternalInterface_r->u8TestRespLv_CV;
	stIdbTest_Output.m_lsTest_RELV.lau8VlvCtrlRespLv 				= stInternalInterface_r->u8TestRespLv_Rel;
	stIdbTest_Output.m_lsTest_PDV.lau8VlvCtrlRespLv  				= stInternalInterface_r->u8TestRespLv_PD;
	stIdbTest_Output.m_lsTest_BalV.lau8VlvCtrlRespLv 				= stInternalInterface_r->u8TestRespLv_Bal;

	stIdbTest_Output.m_lu8TestInletActFlg_FL 						= stInternalInterface_r->u8TestInletActFlg_FL;
	stIdbTest_Output.m_lu8TestInletActFlg_FR 						= stInternalInterface_r->u8TestInletActFlg_FR;
	stIdbTest_Output.m_lu8TestInletActFlg_RL 						= stInternalInterface_r->u8TestInletActFlg_RL;
	stIdbTest_Output.m_lu8TestInletActFlg_RR 						= stInternalInterface_r->u8TestInletActFlg_RR;

	stInternalInterface_r->stLogInterface.s16TestStateMon			=  stInternalInterface_r->s16TestStateMon;

	stIdbTest_Output.stLogInterface									= stInternalInterface_r->stLogInterface;

}

int8_t LIDB_s8TestPressCtrlState(uint8_t u8TestReadyFlgInput)
{
	static int8_t lidbs8TestPCtrlStateOut = 0;
	static int16_t lidbs16TestPCtrlStatCnt = 0;
	switch(lidbs8TestPCtrlStateOut)
	{
	case S8_PRESS_READY:
		lidbs8TestPCtrlStateOut = (u8TestReadyFlgInput==TRUE) ? S8_PRESS_BOOST : S8_PRESS_READY;
		lidbs16TestPCtrlStatCnt = 0;
		break;
	case S8_PRESS_BOOST:
		lidbs8TestPCtrlStateOut = (u8TestReadyFlgInput==TRUE) ? S8_PRESS_BOOST : S8_PRESS_FADE_OUT;
		lidbs16TestPCtrlStatCnt = 0;
		break;
	case S8_PRESS_FADE_OUT:
		lidbs16TestPCtrlStatCnt = lidbs16TestPCtrlStatCnt + 1;
		if(lidbs16TestPCtrlStatCnt > U16_T_2_S)
		{
			lidbs8TestPCtrlStateOut = S8_PRESS_READY;
		}
		break;
	default:
		lidbs8TestPCtrlStateOut = S8_PRESS_READY;
		lidbs16TestPCtrlStatCnt = 0;
		break;
	}
	return lidbs8TestPCtrlStateOut;
}

#if (__HW_TEST_TRIGERED_MODE == __HW_TEST_TRIGERED_BY_PEDAL)
static uint8_t LIDB_vTriggerIDBHWTestByPedal(int16_t s16PedalTravel, uint8_t u8TestReadyFlg)
{
	static uint16_t lau16TriggerMaintainFlag = FALSE;
	static uint16_t lu16IdbTestPedalTravelCnt = 0;

	if (s16PedalTravel > S16_TRAVEL_20_MM)
	{
		lu16IdbTestPedalTravelCnt = lu16IdbTestPedalTravelCnt + 1;
	}
	else
	{
		if (lu16IdbTestPedalTravelCnt >= U8_T_200_MS)
		{
			u8TestReadyFlg ^= 1;
		}
		lu16IdbTestPedalTravelCnt = 0;
	}
	return u8TestReadyFlg;
}

#else /* (__HW_TEST_TRIGERED_MODE == __HW_TEST_TRIGERED_BY_TCS_SW) */
static uint8_t LIDB_vTriggerIDBHWTestByTCSSwitch(uint8_t u8TCSDisabledBySW, uint8_t u8TestReadyFlg)
{
	static uint16_t u16TestTriggerCnt = 0;

	if (u8TCSDisabledBySW==0)
	{
		if (u8TestReadyFlg == 0)
		{
			if (u16TestTriggerCnt >= U8_T_100_MS)
			{
				u8TestReadyFlg = TRUE;
			}
		}
		else
		{
			if (u16TestTriggerCnt >= U8_T_100_MS)
			{
				u8TestReadyFlg = FALSE;
			}
		}
		u16TestTriggerCnt = 0;
	}
	else
	{
		if(u16TestTriggerCnt <= U8_T_100_MS)
		{
			u16TestTriggerCnt++;
		}
		else
		{
			u16TestTriggerCnt = U8_T_100_MS;
		}
	}
	return u8TestReadyFlg;
}
#endif

#endif /*M_IDB_TEST*/
