/*
 * L_IdbTestPressPerformance.c
 *
 *  Created on: 2015. 6. 18.
 *      Author: seokjong.kim@halla.com
 */
/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "../../HDR/L_IdbTestMain.h"

#if (M_IDB_TEST == ENABLE)

#include "./L_IdbTestPressPerformance.h"
#include "./L_IdbTestPressPerformanceConfig.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

#define M_RAMP_TEST_MODE					0
#define M_STEP_TEST_MODE					1
#define M_CONTINUOUS_TEST_MODE				2

#define M_PRESSURE_PERFORMANCE_TEST_MODE	M_CONTINUOUS_TEST_MODE

#define M_TEST_PATTERN_REPEAT_CNT			1

#define M_RAMP_TEST_INITIAL_CASE			0	/* Ramp Test Mode Decide 0~7 */

#define M_RAMP_TEST_CASES					8

#define S16_MAX_TP							S16_PCTRL_PRESS_150_BAR
#define M_S16_PRESSURE_FADE_OUT_TIME		U16_T_2_S

/* M_STEP_TEST_MODE Define */
#define M_S16_STEP_TEST_TP					S16_PCTRL_PRESS_70_BAR
#define M_S16_STEP_HIGH_HOLD_TIME			U16_T_5_S
#define M_S16_STEP_LOW_HOLD_TIME			U16_T_3_S

#define M_STATE_READY					(0)
/* For Ramp Test Mode */
#define M_STATE_INITIALIZE				(1)
#define M_STATE_INITIAL_RISE			(2)
#define M_STATE_INITIAL_HOLD			(3)
#define M_STATE_TEST_RISE				(4)
#define M_STATE_TEST_HIGH_HOLD			(5)
#define M_STATE_TEST_FALL				(6)
#define M_STATE_TEST_FALL_HOLD			(7)
#define M_STATE_INITIAL_FALL			(8)

/* For Step Test Mode */
#define M_STATE_STEP_HIGH				(9)
#define M_STATE_STEP_HIGH_HOLD			(10)
#define M_STATE_STEP_LOW				(11)
#define M_STATE_STEP_LOW_HOLD			(12)




/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
	int16_t s16TestInitialTP;
	int16_t s16TestInitialTPRate;
	int16_t s16TestInitialTPHoldTime;
	int16_t s16TestHighTP;
	int16_t s16TestHighTPHoldTime;
	int16_t s16TestApplyRate;
	int16_t s16TestReleaseRate;
}stTEST_PATTERN_t;
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/

stTEST_PATTERN_t lidbstPerformanceTestPattern[M_RAMP_TEST_CASES] =
{
/*	INITIAL_TP,              INITIAL_TPRATE,   INITIAL_TP_HOLD_TIME, HIGH_TP,                HIGH_TP_HOLD_TIME, APPLY_RATE,         RELEASE_RATE */
	{S16_PCTRL_PRESS_0_BAR,  S16_P_RATE_5_BPS,  U16_T_2_S,           S16_PCTRL_PRESS_15_BAR,  U16_T_2_S,        S16_P_RATE_5_BPS,   S16_P_RATE_5_BPS  },	/* 0 to 15, rate 5 bar/s */
	{S16_PCTRL_PRESS_0_BAR,  S16_P_RATE_5_BPS,  U16_T_2_S,           S16_PCTRL_PRESS_15_BAR,  U16_T_2_S,        S16_P_RATE_50_BPS,  S16_P_RATE_50_BPS },	/* 0 to 15, rate 50 bar/s */
	{S16_PCTRL_PRESS_5_BAR,  S16_P_RATE_5_BPS,  U16_T_2_S,           S16_PCTRL_PRESS_40_BAR,  U16_T_2_S,        S16_P_RATE_5_BPS,   S16_P_RATE_5_BPS  },	/* 5 to 40, rate 5 bar/s */
	{S16_PCTRL_PRESS_5_BAR,  S16_P_RATE_5_BPS,  U16_T_2_S,           S16_PCTRL_PRESS_40_BAR,  U16_T_2_S,        S16_P_RATE_50_BPS,  S16_P_RATE_50_BPS },	/* 5 to 40, rate 50 bar/s */
	{S16_PCTRL_PRESS_5_BAR,  S16_P_RATE_5_BPS,  U16_T_2_S,           S16_PCTRL_PRESS_40_BAR,  U16_T_2_S,        S16_P_RATE_100_BPS, S16_P_RATE_100_BPS},	/* 5 to 40, rate 100 bar/s */
	{S16_PCTRL_PRESS_40_BAR, S16_P_RATE_20_BPS, U16_T_2_S,           S16_PCTRL_PRESS_120_BAR, U16_T_2_S,        S16_P_RATE_5_BPS,   S16_P_RATE_5_BPS  },	/* 40 to 120, rate 5 bar/s */
	{S16_PCTRL_PRESS_40_BAR, S16_P_RATE_20_BPS, U16_T_2_S,           S16_PCTRL_PRESS_120_BAR, U16_T_2_S,        S16_P_RATE_100_BPS, S16_P_RATE_100_BPS},	/* 40 to 120, rate 100 bar/s */
	{S16_PCTRL_PRESS_40_BAR, S16_P_RATE_20_BPS, U16_T_2_S,           S16_PCTRL_PRESS_120_BAR, U16_T_2_S,        S16_P_RATE_400_BPS, S16_P_RATE_400_BPS}		/* 40 to 120, rate 400 bar/s */
};

int16_t lidbs16PerformanceTestStepTP				= M_S16_STEP_TEST_TP;

/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
static int16_t LIDB_s16TestCalcStdTargetPressRate(int16_t s16TargetPressInput);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
/*******************************************************************************
* FUNCTION NAME:        LIDB_vTestPressurePerformance
* CALLED BY:            L_vCallTestIDB()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Generate target pressure for Idb pressure performance test
********************************************************************************/
stInternalInterface_t LIDB_vTestPressurePerformance(stInternalInterface_t* stInternalInterface_r)
{
	static int16_t ldahbs16ProgressCnt 				= 0;
	static int16_t ldahbs16TestState 				= M_STATE_READY;
	static int16_t lidbs16TestTargetPressOld 		= S16_PCTRL_PRESS_0_BAR;
	static int16_t lidbs16StepTestFlg 				= FALSE;
	static uint8_t ldahbu8PressCtrlTestActive 		= FALSE;
	static uint8_t lidbu8TestPatternRepeatCnt		= 0;
	static uint8_t lidbu8TestCaseIndex				= M_RAMP_TEST_INITIAL_CASE;
	int16_t s16TestTargetPress 						= S16_PCTRL_PRESS_0_BAR;

	if (stInternalInterface_r->u8TestReady == TRUE)
	{
		ldahbu8PressCtrlTestActive = TRUE;
	}
	else
	{
		ldahbu8PressCtrlTestActive 		= FALSE;
		lidbu8TestPatternRepeatCnt 		= 0;
		lidbu8TestCaseIndex 			= M_RAMP_TEST_INITIAL_CASE;
		ldahbs16TestState 				= M_STATE_READY;
		lidbs16TestTargetPressOld 		= S16_PCTRL_PRESS_0_BAR;
		s16TestTargetPress 				= S16_PCTRL_PRESS_0_BAR;
	}

	if (ldahbu8PressCtrlTestActive == TRUE)
	{
		ldahbs16ProgressCnt = ldahbs16ProgressCnt + 1;
		if (lidbu8TestPatternRepeatCnt < M_TEST_PATTERN_REPEAT_CNT)
		{
			switch(ldahbs16TestState)
			{
			case M_STATE_READY:
				if (ldahbs16ProgressCnt >= lidbstPerformanceTestPattern[lidbu8TestCaseIndex].s16TestInitialTPHoldTime)
				{
					s16TestTargetPress = S16_PCTRL_PRESS_0_BAR;
					ldahbs16ProgressCnt = 0;
					stInternalInterface_r->stLogInterface.u8IdbTestLoggingTriggerFlg = TRUE;
#if (M_PRESSURE_PERFORMANCE_TEST_MODE == M_STEP_TEST_MODE)
					ldahbs16TestState = M_STATE_STEP_HIGH;
#elif (M_PRESSURE_PERFORMANCE_TEST_MODE == M_CONTINUOUS_TEST_MODE)
					if (lidbs16StepTestFlg == TRUE)
					{
						ldahbs16TestState = M_STATE_STEP_HIGH;
					}
					else
					{
						ldahbs16TestState = M_STATE_INITIAL_RISE;
					}
#else	/* (M_PRESSURE_PERFORMANCE_TEST_MODE == M_RAMP_TEST_MODE) */
					ldahbs16TestState = M_STATE_INITIAL_RISE;
#endif
				}
				else
				{
					s16TestTargetPress = S16_PCTRL_PRESS_0_BAR;
				}
				break;

			case M_STATE_INITIAL_RISE:
				if (lidbs16TestTargetPressOld >= lidbstPerformanceTestPattern[lidbu8TestCaseIndex].s16TestInitialTP)
				{
					s16TestTargetPress = lidbstPerformanceTestPattern[lidbu8TestCaseIndex].s16TestInitialTP;
					ldahbs16TestState = M_STATE_INITIAL_HOLD;
					ldahbs16ProgressCnt = 0;
				}
				else
				{
					s16TestTargetPress = ldahbs16ProgressCnt * (int16_t)((int32_t)(lidbstPerformanceTestPattern[lidbu8TestCaseIndex].s16TestInitialTPRate*100)/200);	/* 100 : 1bar == 100, 200 : bar/scan */

					if (s16TestTargetPress > lidbstPerformanceTestPattern[lidbu8TestCaseIndex].s16TestInitialTP)
					{
						s16TestTargetPress = lidbstPerformanceTestPattern[lidbu8TestCaseIndex].s16TestInitialTP;
					}
					else{ }
				}
				break;

			case M_STATE_INITIAL_HOLD:
				if (ldahbs16ProgressCnt >= lidbstPerformanceTestPattern[lidbu8TestCaseIndex].s16TestInitialTPHoldTime)
				{
					s16TestTargetPress = lidbstPerformanceTestPattern[lidbu8TestCaseIndex].s16TestInitialTP;
					ldahbs16TestState = M_STATE_TEST_RISE;
					ldahbs16ProgressCnt = 0;
				}
				else
				{
					s16TestTargetPress = lidbstPerformanceTestPattern[lidbu8TestCaseIndex].s16TestInitialTP;
				}
				break;

			case M_STATE_TEST_RISE:
				if (lidbs16TestTargetPressOld >= lidbstPerformanceTestPattern[lidbu8TestCaseIndex].s16TestHighTP)
				{
					s16TestTargetPress = lidbstPerformanceTestPattern[lidbu8TestCaseIndex].s16TestHighTP;
					ldahbs16TestState = M_STATE_TEST_HIGH_HOLD;
					ldahbs16ProgressCnt = 0;
				}
				else
				{
					s16TestTargetPress = lidbstPerformanceTestPattern[lidbu8TestCaseIndex].s16TestInitialTP + ldahbs16ProgressCnt * (int16_t)((int32_t)(lidbstPerformanceTestPattern[lidbu8TestCaseIndex].s16TestApplyRate*100)/200);	/* 100 : 1bar == 100, 200 : bar/scan */
					s16TestTargetPress = L_s16LimitMinMax(s16TestTargetPress, S16_PCTRL_PRESS_0_BAR, lidbstPerformanceTestPattern[lidbu8TestCaseIndex].s16TestHighTP);
				}
				break;

			case M_STATE_TEST_HIGH_HOLD:
				if (ldahbs16ProgressCnt >= lidbstPerformanceTestPattern[lidbu8TestCaseIndex].s16TestHighTPHoldTime)
				{
					s16TestTargetPress = lidbstPerformanceTestPattern[lidbu8TestCaseIndex].s16TestHighTP;
					ldahbs16TestState = M_STATE_TEST_FALL;
					ldahbs16ProgressCnt = 0;
				}
				else
				{
					s16TestTargetPress = lidbstPerformanceTestPattern[lidbu8TestCaseIndex].s16TestHighTP;
				}
				break;

			case M_STATE_TEST_FALL:
				if (lidbs16TestTargetPressOld <= lidbstPerformanceTestPattern[lidbu8TestCaseIndex].s16TestInitialTP)
				{
					s16TestTargetPress = lidbstPerformanceTestPattern[lidbu8TestCaseIndex].s16TestInitialTP;
					ldahbs16TestState = M_STATE_TEST_FALL_HOLD;
					ldahbs16ProgressCnt = 0;
				}
				else
				{
					s16TestTargetPress = lidbstPerformanceTestPattern[lidbu8TestCaseIndex].s16TestHighTP - ldahbs16ProgressCnt * (int16_t)((int32_t)(lidbstPerformanceTestPattern[lidbu8TestCaseIndex].s16TestReleaseRate*100)/200);	/* 100 : 1bar == 100, 200 : bar/scan */

					if (s16TestTargetPress < lidbstPerformanceTestPattern[lidbu8TestCaseIndex].s16TestInitialTP)
					{
						s16TestTargetPress = lidbstPerformanceTestPattern[lidbu8TestCaseIndex].s16TestInitialTP;
					}
					else{ }
				}
				break;

			case M_STATE_TEST_FALL_HOLD:
				if (ldahbs16ProgressCnt >= lidbstPerformanceTestPattern[lidbu8TestCaseIndex].s16TestInitialTPHoldTime)
				{
					s16TestTargetPress = lidbstPerformanceTestPattern[lidbu8TestCaseIndex].s16TestInitialTP;
					ldahbs16TestState = M_STATE_INITIAL_FALL;
					ldahbs16ProgressCnt = 0;
				}
				else
				{
					s16TestTargetPress = lidbstPerformanceTestPattern[lidbu8TestCaseIndex].s16TestInitialTP;
				}
				break;

			case M_STATE_INITIAL_FALL:
				if (lidbs16TestTargetPressOld <= S16_PCTRL_PRESS_0_BAR)
				{
					s16TestTargetPress = S16_PCTRL_PRESS_0_BAR;
					if(ldahbs16ProgressCnt >= M_S16_PRESSURE_FADE_OUT_TIME)
					{
						ldahbs16TestState = M_STATE_READY;
						stInternalInterface_r->stLogInterface.u8IdbTestLoggingTriggerFlg = FALSE;
						lidbu8TestPatternRepeatCnt = lidbu8TestPatternRepeatCnt + 1;
						ldahbs16ProgressCnt = 0;
					}
					else { }
				}
				else
				{
					s16TestTargetPress = lidbstPerformanceTestPattern[lidbu8TestCaseIndex].s16TestInitialTP - ldahbs16ProgressCnt * (int16_t)((int32_t)(lidbstPerformanceTestPattern[lidbu8TestCaseIndex].s16TestInitialTPRate*100)/200);	/* 100 : 1bar == 100, 200 : bar/scan */

					if (s16TestTargetPress <= S16_PCTRL_PRESS_0_BAR)
					{
						s16TestTargetPress = S16_PCTRL_PRESS_0_BAR;
						ldahbs16ProgressCnt = 0;
					}
					else{ }
				}
				break;

			case M_STATE_STEP_HIGH:
				s16TestTargetPress = lidbs16PerformanceTestStepTP;
				ldahbs16TestState = M_STATE_STEP_HIGH_HOLD;
				ldahbs16ProgressCnt = 0;
				break;

			case M_STATE_STEP_HIGH_HOLD:
				if (ldahbs16ProgressCnt >= M_S16_STEP_HIGH_HOLD_TIME)
				{
					s16TestTargetPress = lidbs16PerformanceTestStepTP;
					ldahbs16TestState = M_STATE_STEP_LOW;
					ldahbs16ProgressCnt = 0;
				}
				else
				{
					s16TestTargetPress = lidbs16PerformanceTestStepTP;
				}
				break;

			case M_STATE_STEP_LOW:
				s16TestTargetPress = S16_PCTRL_PRESS_0_BAR;
				ldahbs16TestState = M_STATE_STEP_LOW_HOLD;
				ldahbs16ProgressCnt = 0;
				break;

			case M_STATE_STEP_LOW_HOLD:

				if (ldahbs16ProgressCnt >= M_S16_STEP_LOW_HOLD_TIME)
				{
					s16TestTargetPress = S16_PCTRL_PRESS_0_BAR;
					ldahbs16TestState = M_STATE_READY;
					ldahbs16ProgressCnt = 0;
					stInternalInterface_r->stLogInterface.u8IdbTestLoggingTriggerFlg = FALSE;
					lidbu8TestPatternRepeatCnt = lidbu8TestPatternRepeatCnt + 1;
				}
				else
				{
					s16TestTargetPress = S16_PCTRL_PRESS_0_BAR;
				}
				break;

			default:
				s16TestTargetPress = S16_PCTRL_PRESS_0_BAR;
				ldahbu8PressCtrlTestActive = FALSE;
				stInternalInterface_r->u8TestReady = FALSE;
				break;
			}
		}
		else
		{

#if (M_PRESSURE_PERFORMANCE_TEST_MODE == M_CONTINUOUS_TEST_MODE)
			lidbu8TestCaseIndex = lidbu8TestCaseIndex + 1;

			if (lidbu8TestCaseIndex >= M_RAMP_TEST_CASES)
			{
				if (lidbs16StepTestFlg == FALSE)
				{
					lidbs16StepTestFlg = TRUE;
				}
				else
				{
					lidbs16StepTestFlg = FALSE;
					lidbu8TestCaseIndex = M_RAMP_TEST_INITIAL_CASE;
					stInternalInterface_r->stLogInterface.u8IdbTestLoggingTriggerFlg = FALSE;
					ldahbu8PressCtrlTestActive = FALSE;
					stInternalInterface_r->u8TestReady = FALSE;
				}
			}
			else
			{
				lidbu8TestPatternRepeatCnt = 0;
				stInternalInterface_r->stLogInterface.u8IdbTestLoggingTriggerFlg = FALSE;
			}
#else
			stInternalInterface_r->stLogInterface.u8IdbTestLoggingTriggerFlg = FALSE;
			ldahbu8PressCtrlTestActive = FALSE;
			stInternalInterface_r->u8TestReady = FALSE;
			lidbu8TestPatternRepeatCnt = 0;
#endif
		}
	}
	else { }

	stInternalInterface_r->s16TestStateMon										= ldahbs16TestState;
	stInternalInterface_r->stTestOverrideConfig.u1OverrideTargetPressure 		= TRUE;
	stInternalInterface_r->s16TestTargetPress 									= L_s16LimitMinMax(s16TestTargetPress, S16_PCTRL_PRESS_0_BAR, S16_MAX_TP);
	stInternalInterface_r->s16TestDeltaTargetPress 								= stInternalInterface_r->s16TestTargetPress - lidbs16TestTargetPressOld;
	stInternalInterface_r->u8PCtrlActFlg 										= (stInternalInterface_r->s16TestTargetPress > stInternalInterface_r->s16PCtrlActThreshPress)? TRUE: FALSE;
	stInternalInterface_r->s16TestTargetPressRate 								= LIDB_s16TestCalcStdTargetPressRate(stInternalInterface_r->s16TestTargetPress);
	stInternalInterface_r->stTestOverrideConfig.u1OverrideWheelTargetPressure	= TRUE;
	stInternalInterface_r->s16TestTargetP_FL									= stInternalInterface_r->s16TestTargetPress;
	stInternalInterface_r->s16TestTargetP_FR									= stInternalInterface_r->s16TestTargetPress;
	stInternalInterface_r->s16TestTargetP_RL									= stInternalInterface_r->s16TestTargetPress;
	stInternalInterface_r->s16TestTargetP_RR									= stInternalInterface_r->s16TestTargetPress;
	stInternalInterface_r->s16TestTargetPRate_FL								= stInternalInterface_r->s16TestTargetPressRate;
	stInternalInterface_r->s16TestTargetPRate_FR								= stInternalInterface_r->s16TestTargetPressRate;
	stInternalInterface_r->s16TestTargetPRate_RL								= stInternalInterface_r->s16TestTargetPressRate;
	stInternalInterface_r->s16TestTargetPRate_RR								= stInternalInterface_r->s16TestTargetPressRate;

	stInternalInterface_r->stTestOverrideConfig.u1OverridePCtrlState				= TRUE;
	stInternalInterface_r->s8PCtrlState											= LIDB_s8TestPressCtrlState(stInternalInterface_r->u8TestReady);
	stInternalInterface_r->stTestOverrideConfig.u1OverrideReqBoostMode			= TRUE;
	stInternalInterface_r->s8ReqBoostMode										= S8_BOOST_MODE_REQ_L5;
	stInternalInterface_r->stTestOverrideConfig.u1OverridePCtrlBoostMode			= TRUE;
	stInternalInterface_r->s8PCtrlBoostMode										= S8_BOOST_MODE_RESPONSE_L5;
	lidbs16TestTargetPressOld = s16TestTargetPress;

	return (* stInternalInterface_r);
}

stTestOverrideConfig_t LIDB_stPressurePerformanceConfig(void)
{
	static stTestOverrideConfig_t pstTestOverrideConfig = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

	pstTestOverrideConfig.u1OverridePCtrlState				= M_OVERRIDE_PCTRLSTATE;
	pstTestOverrideConfig.u1OverridePCtrlBoostMode			= M_OVERRIDE_PCTRLBOOSTMODE;
	pstTestOverrideConfig.u1OverrideReqBoostMode			= M_OVERRIDE_REQBOOSTMODE;
	pstTestOverrideConfig.u1OverridePedalSiganl				= M_OVERRIDE_PEDAL_SIGNAL;
	pstTestOverrideConfig.u1OverrideTargetPressure			= M_OVERRIDE_TARGET_PRESSURE;
	pstTestOverrideConfig.u1OverrideWheelTargetPressure		= M_OVERRIDE_WHEEL_TARGET_PRESSURE;
	pstTestOverrideConfig.u1OverrideChamberVolumeMtr		= M_OVERRIDE_CHAMBERVOLUME_MTR;
	pstTestOverrideConfig.u1OverrideChamberVolumeVlv		= M_OVERRIDE_CHAMBERVOLUME_VLV;
	pstTestOverrideConfig.u1OverrideDeltaStroke				= M_OVERRIDE_DELTA_STROKE;
	pstTestOverrideConfig.u1OverrideCut1VlvCtrl				= M_OVERRIDE_CUT1_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideCut2VlvCtrl				= M_OVERRIDE_CUT2_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideSimVlvCtrl				= M_OVERRIDE_SIM_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideCvVlvCtrl				= M_OVERRIDE_CV_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideRelVlvCtrl				= M_OVERRIDE_REL_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideBalVlvCtrl				= M_OVERRIDE_BAL_VLV_CONTROL;
	pstTestOverrideConfig.u1OverridePdVlvCtrl				= M_OVERRIDE_PD_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideWheelInVlvCtrl			= M_OVERRIDE_WHEEL_IN_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideWheelOutVlvCtrl			= M_OVERRIDE_WHEEL_OUT_VLV_CONTROL;

	return (pstTestOverrideConfig);
}

static int16_t LIDB_s16TestCalcStdTargetPressRate(int16_t s16TargetPressInput)
{
	int16_t s16TGPressBuffCntBf2scan = 0;
	int16_t s16FilterInput = 0;
	int32_t s32FilterInputTemp = 0;

	static int16_t lidbs16PressBuff[5] = {0,0,0,0,0};
	static int16_t lidbs16BuffCnt = 0;
	static int16_t lidbs16PressRate10ms = 0;

	lidbs16PressBuff[lidbs16BuffCnt] = s16TargetPressInput;

	lidbs16BuffCnt = lidbs16BuffCnt + 1;
    if(lidbs16BuffCnt>=5)
    {
    	lidbs16BuffCnt = 0;
    }
    else{ }

	/**********************************************************/
	/* Target Press rate (1 = 0.01bar/10ms = 1bar/s)          */
	/**********************************************************/
    if(lidbs16BuffCnt>=3)
    {
    	s16TGPressBuffCntBf2scan = lidbs16BuffCnt - 3;
    }
    else
    {
    	s16TGPressBuffCntBf2scan = lidbs16BuffCnt + 2;
    }

	s32FilterInputTemp = ((int32_t)s16TargetPressInput - (int32_t)lidbs16PressBuff[s16TGPressBuffCntBf2scan]); /* target press variation for 2scan. changed algorithm for simplicity in Feb. 2015 -> already checked both algorithms*/

	s16FilterInput = (int16_t)L_s32LimitMinMax(s32FilterInputTemp, ASW_S16_MIN, ASW_S16_MAX);

	lidbs16PressRate10ms =(L_s16Lpf1Int(s16FilterInput, lidbs16PressRate10ms, U8_LPF_7HZ_128G)); /* 1 bar/sec*/


	return lidbs16PressRate10ms;
}
#endif /* (M_IDB_TEST == ENABLE) */
