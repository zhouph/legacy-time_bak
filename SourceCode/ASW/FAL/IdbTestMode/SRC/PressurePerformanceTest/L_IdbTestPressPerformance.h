/*
 * L_IdbTestPressPerformance.h
 *
 *  Created on: 2015. 6. 18.
 *      Author: seokjong.kim@halla.com
 */

#ifndef L_IDBTESTPRESSPERFORMANCE_H_
#define L_IDBTESTPRESSPERFORMANCE_H_

extern stInternalInterface_t LIDB_vTestPressurePerformance(stInternalInterface_t* stInternalInterface_r);
extern stTestOverrideConfig_t LIDB_stPressurePerformanceConfig(void);

#endif /* L_IDBTESTPRESSPERFORMANCE_H_ */
