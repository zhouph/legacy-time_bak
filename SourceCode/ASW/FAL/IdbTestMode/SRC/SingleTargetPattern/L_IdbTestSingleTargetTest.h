/*
 * L_IdbTestSingleTargetTest.h
 *
 *  Created on: 2015. 6. 18.
 *      Author: seokjong.kim@halla.com
 */

#ifndef L_IDBTESTSINGLETARGETTEST_H_
#define L_IDBTESTSINGLETARGETTEST_H_

extern stInternalInterface_t LIDB_vTestTargetGenSingleShape(stInternalInterface_t* stInternalInterface_r);
extern stTestOverrideConfig_t LIDB_stSingleTargetPatternConfig(void);

#endif /* L_IDBTESTSINGLETARGETTEST_H_ */
