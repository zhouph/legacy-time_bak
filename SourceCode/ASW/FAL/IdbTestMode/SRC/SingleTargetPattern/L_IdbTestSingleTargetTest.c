/*
 * L_IdbTestPressPerformance.c
 *
 *  Created on: 2015. 6. 18.
 *      Author: seokjong.kim@halla.com
 */
/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "../../HDR/L_IdbTestMain.h"

#if (M_IDB_TEST == ENABLE)

#include "./L_IdbTestSingleTargetTest.h"
#include "./L_IdbTestSingleTargetTestConfig.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

#define M_RAMP_TEST_MODE					1
#define M_STEP_TEST_MODE					2

#define M_PRESSURE_PERFORMANCE_TEST_MODE	M_RAMP_TEST_MODE

#define M_STEP_DROP_TARGET					DISABLE

#define S16_MAX_TP							S16_PCTRL_PRESS_150_BAR
#define M_S16_PRESSURE_FADE_OUT_TIME		U16_T_2_S

/* M_STEP_TEST_MODE Define */
#define M_S16_STEP_HIGH_HOLD_TIME			U16_T_5_S
#define M_S16_STEP_LOW_HOLD_TIME			U16_T_3_S

#define M_STATE_READY					(0)
/* For Ramp Test Mode */
#define M_STATE_INITIALIZE				(1)
#define M_STATE_INITIAL_RISE			(2)
#define M_STATE_INITIAL_HOLD			(3)
#define M_STATE_TEST_RISE				(4)
#define M_STATE_TEST_HIGH_HOLD			(5)
#define M_STATE_TEST_FALL				(6)
#define M_STATE_TEST_FALL_HOLD			(7)
#define M_STATE_INITIAL_FALL			(8)

/* For Step Test Mode */
#define M_STATE_STEP_HIGH				(9)
#define M_STATE_STEP_HIGH_HOLD			(10)
#define M_STATE_STEP_LOW				(11)
#define M_STATE_STEP_LOW_HOLD			(12)


/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
	int16_t s16TestInitialTP;
	int16_t s16TestInitialTPRate;
	int16_t s16TestInitialTPHoldTime;
	int16_t s16TestHighTP;
	int16_t s16TestHighTPHoldTime;
	int16_t s16TestApplyRate;
	int16_t s16TestReleaseRate;
	int16_t s16TestStepTargetP;
}stTEST_PATTERN_t;
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/

/*int16_t lidbs16TestTargetPress = S16_PCTRL_PRESS_0_BAR;
int16_t lidbs16TestTargetPressOld = S16_PCTRL_PRESS_0_BAR;
int16_t lidbs16TestTargetPressRate = S16_PCTRL_PRESS_0_BAR;
int16_t lidbs16TestTP_FL = S16_PCTRL_PRESS_0_BAR;
int16_t lidbs16TestTP_FR = S16_PCTRL_PRESS_0_BAR;
int16_t lidbs16TestTP_RL = S16_PCTRL_PRESS_0_BAR;
int16_t lidbs16TestTP_RR = S16_PCTRL_PRESS_0_BAR;
int16_t lidbs16TestTPRate_FL = S16_PCTRL_PRESS_0_BAR;
int16_t lidbs16TestTPRate_FR = S16_PCTRL_PRESS_0_BAR;
int16_t lidbs16TestTPRate_RL = S16_PCTRL_PRESS_0_BAR;
int16_t lidbs16TestTPRate_RR = S16_PCTRL_PRESS_0_BAR;
int16_t lidbs16TestTargetPressDel = S16_PCTRL_PRESS_0_BAR;
uint8_t lidbu8TestTPValidFlg = FALSE;*/
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
uint8_t lidbu8ReleaseVAlveResponseTestFlg = FALSE;

stTEST_PATTERN_t lidbstSingleTestPattern =
/*	INITIAL_TP,              INITIAL_TPRATE,   INITIAL_TP_HOLD_TIME, HIGH_TP,                HIGH_TP_HOLD_TIME, APPLY_RATE,         RELEASE_RATE      STEP_TARGET_P*/
	{S16_PCTRL_PRESS_0_BAR, S16_P_RATE_20_BPS, U16_T_2_S,           S16_PCTRL_PRESS_150_BAR, U16_T_2_S,        S16_P_RATE_20_BPS, S16_P_RATE_100_BPS, S16_PCTRL_PRESS_70_BAR};		/* 40 to 120, rate 400 bar/s */

/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
static int16_t LIDB_s16TestCalcStdTargetPressRate(int16_t s16TargetPressInput);
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
/*******************************************************************************
* FUNCTION NAME:        LIDB_vTestPressurePerformance
* CALLED BY:            L_vCallTestIDB()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Generate target pressure for Idb pressure performance test
********************************************************************************/
stInternalInterface_t LIDB_vTestTargetGenSingleShape(stInternalInterface_t* stInternalInterface_r)
{
	static int16_t lidbs16ProgressCnt 			= 0;
	static int16_t lidbs16TestState 			= M_STATE_READY;
	static int16_t lidbs16TestTargetPressOld 	= S16_PCTRL_PRESS_0_BAR;
	static int16_t lidbs16StepTestFlg 			= FALSE;
	static uint8_t lidbu8PressCtrlTestActive 	= FALSE;

	int16_t s16TestTargetPress 					= S16_PCTRL_PRESS_0_BAR;

	if (stInternalInterface_r->u8TestReady == TRUE)
	{
		lidbu8PressCtrlTestActive = TRUE;
	}
	else
	{
		lidbu8PressCtrlTestActive 		= FALSE;
		lidbs16TestState 				= M_STATE_READY;
	}

	if (lidbu8PressCtrlTestActive == TRUE)
	{

		lidbs16ProgressCnt = lidbs16ProgressCnt + 1;

		switch(lidbs16TestState)
		{
		case M_STATE_READY:
			if (lidbs16ProgressCnt >= lidbstSingleTestPattern.s16TestInitialTPHoldTime)
			{
				s16TestTargetPress 		= S16_PCTRL_PRESS_0_BAR;
				lidbs16ProgressCnt 	= 0;
#if (M_PRESSURE_PERFORMANCE_TEST_MODE == M_STEP_TEST_MODE)
				lidbs16TestState 		= M_STATE_STEP_HIGH;
#else	/* (M_PRESSURE_PERFORMANCE_TEST_MODE == M_RAMP_TEST_MODE) */
				lidbs16TestState 		= M_STATE_INITIAL_RISE;
#endif
			}
			else
			{
				s16TestTargetPress 		= S16_PCTRL_PRESS_0_BAR;
			}
			break;

		case M_STATE_INITIAL_RISE:
			if (lidbs16TestTargetPressOld >= lidbstSingleTestPattern.s16TestInitialTP)
			{
				s16TestTargetPress 		= lidbstSingleTestPattern.s16TestInitialTP;
				lidbs16TestState		= M_STATE_INITIAL_HOLD;
				lidbs16ProgressCnt 	= 0;
			}
			else
			{
				s16TestTargetPress = lidbs16ProgressCnt * (int16_t)((int32_t)(lidbstSingleTestPattern.s16TestInitialTPRate*100)/200);	/* 100 : 1bar == 100, 200 : bar/scan */

				if (s16TestTargetPress > lidbstSingleTestPattern.s16TestInitialTP)
				{
					s16TestTargetPress = lidbstSingleTestPattern.s16TestInitialTP;
				}
				else{ }
			}
			break;

		case M_STATE_INITIAL_HOLD:
			if (lidbs16ProgressCnt >= lidbstSingleTestPattern.s16TestInitialTPHoldTime)
			{
				s16TestTargetPress 		= lidbstSingleTestPattern.s16TestInitialTP;
				lidbs16TestState 		= M_STATE_TEST_RISE;
				lidbs16ProgressCnt 	= 0;
			}
			else
			{
				s16TestTargetPress = lidbstSingleTestPattern.s16TestInitialTP;
			}
			break;

		case M_STATE_TEST_RISE:
			if (lidbs16TestTargetPressOld >= lidbstSingleTestPattern.s16TestHighTP)
			{
				s16TestTargetPress 		= lidbstSingleTestPattern.s16TestHighTP;
				lidbs16TestState 		= M_STATE_TEST_HIGH_HOLD;
				lidbs16ProgressCnt 	= 0;
			}
			else
			{
				s16TestTargetPress = lidbstSingleTestPattern.s16TestInitialTP + lidbs16ProgressCnt * (int16_t)((int32_t)(lidbstSingleTestPattern.s16TestApplyRate*100)/200);	/* 100 : 1bar == 100, 200 : bar/scan */
				s16TestTargetPress = L_s16LimitMinMax(s16TestTargetPress, S16_PCTRL_PRESS_0_BAR, lidbstSingleTestPattern.s16TestHighTP);
			}
			break;

		case M_STATE_TEST_HIGH_HOLD:
			if (lidbs16ProgressCnt >= lidbstSingleTestPattern.s16TestHighTPHoldTime)
			{
				s16TestTargetPress 		= lidbstSingleTestPattern.s16TestHighTP;
				lidbs16TestState 		= M_STATE_TEST_FALL;
				lidbs16ProgressCnt 	= 0;

				if(lidbu8ReleaseVAlveResponseTestFlg == ENABLE)
				{
					stInternalInterface_r->u8TestActFlg_Rel = TRUE;
				}
			}
			else
			{
				s16TestTargetPress 		= lidbstSingleTestPattern.s16TestHighTP;
			}
			break;

		case M_STATE_TEST_FALL:
			if (lidbs16TestTargetPressOld <= lidbstSingleTestPattern.s16TestInitialTP)
			{
#if M_STEP_DROP_TARGET == ENABLE
				s16TestTargetPress 		= S16_PCTRL_PRESS_0_BAR;
#else
				s16TestTargetPress 		= lidbstSingleTestPattern.s16TestInitialTP;
#endif
				lidbs16TestState 		= M_STATE_TEST_FALL_HOLD;
				lidbs16ProgressCnt 	= 0;
			}
			else
			{
#if M_STEP_DROP_TARGET == ENABLE
				s16TestTargetPress = S16_PCTRL_PRESS_0_BAR;
#else
				s16TestTargetPress = lidbstSingleTestPattern.s16TestHighTP - lidbs16ProgressCnt * (int16_t)((int32_t)(lidbstSingleTestPattern.s16TestReleaseRate*100)/200);	/* 100 : 1bar == 100, 200 : bar/scan */

				if (s16TestTargetPress < lidbstSingleTestPattern.s16TestInitialTP)
				{
					s16TestTargetPress = lidbstSingleTestPattern.s16TestInitialTP;
				}
				else{ }
#endif
			}
			if(lidbu8ReleaseVAlveResponseTestFlg == ENABLE)
			{
				if((lidbs16ProgressCnt % 10)==1)
				{
					stInternalInterface_r->u8TestActFlg_Rel = (stInternalInterface_r->u8TestActFlg_Rel==TRUE) ? FALSE: TRUE;
				}
				stInternalInterface_r->u8TestRespLv_Rel = U8_VLV_RESPONSE_LV_QUICK;
				stInternalInterface_r->stTestOverrideConfig.u1OverrideRelVlvCtrl = TRUE;
			}
			break;

		case M_STATE_TEST_FALL_HOLD:
			if (lidbs16ProgressCnt >= lidbstSingleTestPattern.s16TestInitialTPHoldTime)
			{
#if M_STEP_DROP_TARGET == ENABLE
				s16TestTargetPress		= S16_PCTRL_PRESS_0_BAR;
#else
				s16TestTargetPress 		= lidbstSingleTestPattern.s16TestInitialTP;
#endif
				lidbs16TestState 		= M_STATE_INITIAL_FALL;
				lidbs16ProgressCnt 	= 0;
			}
			else
			{
#if M_STEP_DROP_TARGET == ENABLE
				s16TestTargetPress 		= S16_PCTRL_PRESS_0_BAR;
#else
				s16TestTargetPress 		= lidbstSingleTestPattern.s16TestInitialTP;
#endif
			}
			break;

		case M_STATE_INITIAL_FALL:
			if (lidbs16TestTargetPressOld <= S16_PCTRL_PRESS_0_BAR)
			{
				s16TestTargetPress = S16_PCTRL_PRESS_0_BAR;
				if(lidbs16ProgressCnt < M_S16_PRESSURE_FADE_OUT_TIME)
				{
				}
				else
				{
					lidbs16TestState 						= M_STATE_READY;
					lidbs16ProgressCnt 					= 0;
					lidbu8PressCtrlTestActive 				= FALSE;
					stInternalInterface_r->u8TestReady		= FALSE;
				}
			}
			else
			{
				s16TestTargetPress = lidbstSingleTestPattern.s16TestInitialTP - lidbs16ProgressCnt * (int16_t)((int32_t)(lidbstSingleTestPattern.s16TestInitialTPRate*100)/200);	/* 100 : 1bar == 100, 200 : bar/scan */

				if (s16TestTargetPress <= S16_PCTRL_PRESS_0_BAR)
				{
					s16TestTargetPress = S16_PCTRL_PRESS_0_BAR;
					lidbs16ProgressCnt = 0;
				}
				else{ }
			}
			break;

		case M_STATE_STEP_HIGH:
			s16TestTargetPress 		= lidbstSingleTestPattern.s16TestStepTargetP;
			lidbs16TestState 		= M_STATE_STEP_HIGH_HOLD;
			lidbs16ProgressCnt 	= 0;
			break;

		case M_STATE_STEP_HIGH_HOLD:
			if (lidbs16ProgressCnt >= M_S16_STEP_HIGH_HOLD_TIME)
			{
				s16TestTargetPress 		= lidbstSingleTestPattern.s16TestStepTargetP;
				lidbs16TestState 		= M_STATE_STEP_LOW;
				lidbs16ProgressCnt 	= 0;
			}
			else
			{
				s16TestTargetPress = lidbstSingleTestPattern.s16TestStepTargetP;
			}
			break;

		case M_STATE_STEP_LOW:
			s16TestTargetPress 		= S16_PCTRL_PRESS_0_BAR;
			lidbs16TestState 		= M_STATE_STEP_LOW_HOLD;
			lidbs16ProgressCnt 	= 0;
			break;

		case M_STATE_STEP_LOW_HOLD:
			if (lidbs16ProgressCnt >= M_S16_STEP_LOW_HOLD_TIME)
			{
				s16TestTargetPress 					= S16_PCTRL_PRESS_0_BAR;
				lidbs16TestState 					= M_STATE_READY;
				lidbs16ProgressCnt 				= 0;
				lidbu8PressCtrlTestActive 			= FALSE;
				stInternalInterface_r->u8TestReady 	= FALSE;
			}
			else
			{
				s16TestTargetPress = S16_PCTRL_PRESS_0_BAR;
			}
			break;

		default:
			s16TestTargetPress 					= S16_PCTRL_PRESS_0_BAR;
			lidbu8PressCtrlTestActive 			= FALSE;
			stInternalInterface_r->u8TestReady 	= FALSE;
			break;
		}
	}
	else
	{
		lidbs16TestState 					= M_STATE_READY;
		s16TestTargetPress 					= S16_PCTRL_PRESS_0_BAR;
		stInternalInterface_r->u8TestReady 	= FALSE;
	}

	stInternalInterface_r->s16TestStateMon										= lidbs16TestState;
	stInternalInterface_r->stTestOverrideConfig.u1OverrideTargetPressure 		= TRUE;
	stInternalInterface_r->s16TestTargetPress 									= L_s16LimitMinMax(s16TestTargetPress, S16_PCTRL_PRESS_0_BAR, S16_MAX_TP);
	stInternalInterface_r->s16TestDeltaTargetPress 								= stInternalInterface_r->s16TestTargetPress - lidbs16TestTargetPressOld;
	stInternalInterface_r->u8PCtrlActFlg 										= (stInternalInterface_r->s16TestTargetPress > stInternalInterface_r->s16PCtrlActThreshPress)? TRUE: FALSE;
	stInternalInterface_r->s16TestTargetPressRate 								= LIDB_s16TestCalcStdTargetPressRate(stInternalInterface_r->s16TestTargetPress);
	stInternalInterface_r->stTestOverrideConfig.u1OverrideWheelTargetPressure	= TRUE;
	stInternalInterface_r->s16TestTargetP_FL									= stInternalInterface_r->s16TestTargetPress;
	stInternalInterface_r->s16TestTargetP_FR									= stInternalInterface_r->s16TestTargetPress;
	stInternalInterface_r->s16TestTargetP_RL									= stInternalInterface_r->s16TestTargetPress;
	stInternalInterface_r->s16TestTargetP_RR									= stInternalInterface_r->s16TestTargetPress;
	stInternalInterface_r->s16TestTargetPRate_FL								= stInternalInterface_r->s16TestTargetPressRate;
	stInternalInterface_r->s16TestTargetPRate_FR								= stInternalInterface_r->s16TestTargetPressRate;
	stInternalInterface_r->s16TestTargetPRate_RL								= stInternalInterface_r->s16TestTargetPressRate;
	stInternalInterface_r->s16TestTargetPRate_RR								= stInternalInterface_r->s16TestTargetPressRate;

	stInternalInterface_r->stTestOverrideConfig.u1OverridePCtrlState			= TRUE;
	stInternalInterface_r->s8PCtrlState											= LIDB_s8TestPressCtrlState(stInternalInterface_r->u8TestReady);
	stInternalInterface_r->stTestOverrideConfig.u1OverrideReqBoostMode			= TRUE;
	stInternalInterface_r->s8ReqBoostMode										= S8_BOOST_MODE_REQ_L5;
	stInternalInterface_r->stTestOverrideConfig.u1OverridePCtrlBoostMode			= TRUE;
	stInternalInterface_r->s8PCtrlBoostMode										= S8_BOOST_MODE_RESPONSE_L5;

	lidbs16TestTargetPressOld = s16TestTargetPress;

	return (* stInternalInterface_r);
}

stTestOverrideConfig_t LIDB_stSingleTargetPatternConfig(void)
{
	static stTestOverrideConfig_t pstTestOverrideConfig = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

	pstTestOverrideConfig.u1OverridePCtrlState				= M_OVERRIDE_PCTRLSTATE;
	pstTestOverrideConfig.u1OverridePCtrlBoostMode			= M_OVERRIDE_PCTRLBOOSTMODE;
	pstTestOverrideConfig.u1OverrideReqBoostMode			= M_OVERRIDE_REQBOOSTMODE;
	pstTestOverrideConfig.u1OverridePedalSiganl				= M_OVERRIDE_PEDAL_SIGNAL;
	pstTestOverrideConfig.u1OverrideTargetPressure			= M_OVERRIDE_TARGET_PRESSURE;
	pstTestOverrideConfig.u1OverrideWheelTargetPressure		= M_OVERRIDE_WHEEL_TARGET_PRESSURE;
	pstTestOverrideConfig.u1OverrideChamberVolumeMtr		= M_OVERRIDE_CHAMBERVOLUME_MTR;
	pstTestOverrideConfig.u1OverrideChamberVolumeVlv		= M_OVERRIDE_CHAMBERVOLUME_VLV;
	pstTestOverrideConfig.u1OverrideDeltaStroke				= M_OVERRIDE_DELTA_STROKE;
	pstTestOverrideConfig.u1OverrideCut1VlvCtrl				= M_OVERRIDE_CUT1_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideCut2VlvCtrl				= M_OVERRIDE_CUT2_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideSimVlvCtrl				= M_OVERRIDE_SIM_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideCvVlvCtrl				= M_OVERRIDE_CV_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideRelVlvCtrl				= M_OVERRIDE_REL_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideBalVlvCtrl				= M_OVERRIDE_BAL_VLV_CONTROL;
	pstTestOverrideConfig.u1OverridePdVlvCtrl				= M_OVERRIDE_PD_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideWheelInVlvCtrl			= M_OVERRIDE_WHEEL_IN_VLV_CONTROL;
	pstTestOverrideConfig.u1OverrideWheelOutVlvCtrl			= M_OVERRIDE_WHEEL_OUT_VLV_CONTROL;

	return (pstTestOverrideConfig);
}

static int16_t LIDB_s16TestCalcStdTargetPressRate(int16_t s16TargetPressInput)
{
	int16_t s16TGPressBuffCntBf2scan = 0;
	int16_t s16FilterInput = 0;
	int32_t s32FilterInputTemp = 0;

	static int16_t lidbs16PressBuff[5] = {0,0,0,0,0};
	static int16_t lidbs16BuffCnt = 0;
	static int16_t lidbs16PressRate10ms = 0;

	lidbs16PressBuff[lidbs16BuffCnt] = s16TargetPressInput;

	lidbs16BuffCnt = lidbs16BuffCnt + 1;
    if(lidbs16BuffCnt>=5)
    {
    	lidbs16BuffCnt = 0;
    }
    else{ }

	/**********************************************************/
	/* Target Press rate (1 = 0.01bar/10ms = 1bar/s)          */
	/**********************************************************/
    if(lidbs16BuffCnt>=3)
    {
    	s16TGPressBuffCntBf2scan = lidbs16BuffCnt - 3;
    }
    else
    {
    	s16TGPressBuffCntBf2scan = lidbs16BuffCnt + 2;
    }

	s32FilterInputTemp = ((int32_t)s16TargetPressInput - (int32_t)lidbs16PressBuff[s16TGPressBuffCntBf2scan]); /* target press variation for 2scan. changed algorithm for simplicity in Feb. 2015 -> already checked both algorithms*/

	s16FilterInput = (int16_t)L_s32LimitMinMax(s32FilterInputTemp, ASW_S16_MIN, ASW_S16_MAX);

	lidbs16PressRate10ms =(L_s16Lpf1Int(s16FilterInput, lidbs16PressRate10ms, U8_LPF_7HZ_128G)); /* 1 bar/sec*/


	return lidbs16PressRate10ms;
}
#endif /* (M_IDB_TEST == ENABLE) */
