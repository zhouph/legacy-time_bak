/*
 * L_IdbTestPedalSignal.h
 *
 *  Created on: 2015. 6. 18.
 *      Author: seokjong.kim@halla.com
 */

#ifndef L_IDBTESTPEDALSIGNAL_H_
#define L_IDBTESTPEDALSIGNAL_H_

extern stInternalInterface_t LIDB_vGenTestPedalSignal(stInternalInterface_t* stInternalInterface_r);
extern stTestOverrideConfig_t LIDB_stPedalSignalConfig(stTestOverrideConfig_t * pstTestOverrideConfig);

#endif /* L_IDBTESTPEDALSIGNAL_H_ */
