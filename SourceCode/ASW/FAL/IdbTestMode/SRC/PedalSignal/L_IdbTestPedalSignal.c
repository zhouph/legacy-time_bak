/*
 * L_IdbTestPedalSignal.c
 *
 *  Created on: 2015. 6. 18.
 *      Author: seokjong.kim@halla.com
 */
/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "../../HDR/L_IdbTestMain.h"

#if (M_IDB_TEST == ENABLE)
#include "L_IdbTestPedalSignal.h"
#include "L_IdbTestPedalSignalConfig.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define M_ONE_SECOND_TO_SCAN			200
#define M_APPLY_TARGET_PEDAL_TRAVEL		(S16_TRAVEL_40_MM*10)
#define M_RELEASE_TARGET_PEDAL_TRAVEL	(S16_TRAVEL_0_MM)
#define M_PEDAL_TRAVEL_MAX				(S16_TRAVEL_70_MM*10)
#define M_APPLY_PEDAL_RATE				(S16_TRAVEL_10_MM*10)
#define M_RELEASE_PEDAL_RATE			-M_APPLY_PEDAL_RATE	/*(-S16_TRAVEL_20_MM*10)*/
#define M_INITIAL_TIME					U16_T_2_S
#define M_HOLD_TIME						U16_T_2_S
#define M_VALVE_RESET_TIME				U16_T_10_S
#define M_PCTRL_FADEOUT_TIME			U16_T_2_S
#define M_TEST_PATTERN_COUNT		3

#define M_NON_CONTROL_PHASE			0
#define M_INITIAL_PHASE				1
#define M_APPLY_PHASE				2
#define M_HOLD_PHASE				3
#define M_RELEASE_PHASE				4
#define M_VALVE_RESET_PHASE			5
#define M_CHECK_LOOP_PHASE			6
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
static int16_t L_s16GenTestPedal_1_100mm(int16_t s16PedalRate, int16_t s16CurrentPedal, int16_t s16TargetPedalTravel);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
/*******************************************************************************
* FUNCTION NAME:        LIDB_vTestStrokeMap
* CALLED BY:            L_vCallTestIDB()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          IDB Stroke Map Test Main Function
********************************************************************************/
/*******************************************************************************
* FUNCTION NAME:        L_vGenTestPedalIDB
* CALLED BY:            L_vCallTestIDB()
* Preconditions:        none
* PARAMETER:            none
* RETURN VALUE:         none
* Description:          Generate Pedal Signal for Idb Test
********************************************************************************/
stInternalInterface_t LIDB_vGenTestPedalSignal(stInternalInterface_t* stInternalInterface_r)
{
	static uint16_t lu16IdbTestCnt = 0;
	static uint16_t lu16IdbTestPedal_1_100mm = S16_TRAVEL_0_MM;
	static uint16_t lu16IdbTestLoopCnt = 0;
	static uint8_t lu8IdbTestTrigger = FALSE;
	static uint8_t lu8IdbTestPhase = M_NON_CONTROL_PHASE;

	stInternalInterface_r->stTestOverrideConfig.u1OverridePCtrlState = FALSE;

	switch(lu8IdbTestPhase)
	{
		case M_NON_CONTROL_PHASE:
			if (stInternalInterface_r->u8TestReady == TRUE)
			{
				lu8IdbTestPhase = M_INITIAL_PHASE;
			}
			else { /* Do Nothing */ }

			lu16IdbTestPedal_1_100mm = S16_TRAVEL_0_MM;
			lu16IdbTestLoopCnt = 0;
			lu8IdbTestTrigger = FALSE;
			lu16IdbTestCnt = 0;
			break;

		case M_INITIAL_PHASE:
			if ((lu16IdbTestPedal_1_100mm == S16_TRAVEL_0_MM) && (lu8IdbTestTrigger == FALSE))
			{	/* Initial Phase */
				lu16IdbTestCnt = lu16IdbTestCnt + 1;

				if (lu16IdbTestCnt >= M_INITIAL_TIME)
				{
					lu8IdbTestTrigger = TRUE;
					lu8IdbTestPhase = M_APPLY_PHASE;
					lu16IdbTestCnt = 0;
				}
				else{ /* Do Nothing */ }
			}
			else{ /* Do Nothing */ }
			break;

		case M_APPLY_PHASE:
			lu16IdbTestPedal_1_100mm = L_s16GenTestPedal_1_100mm(M_APPLY_PEDAL_RATE, lu16IdbTestPedal_1_100mm, M_APPLY_TARGET_PEDAL_TRAVEL);
			if (lu16IdbTestPedal_1_100mm >= M_APPLY_TARGET_PEDAL_TRAVEL)
			{
				lu16IdbTestPedal_1_100mm = M_APPLY_TARGET_PEDAL_TRAVEL;
				lu8IdbTestPhase = M_HOLD_PHASE;
				lu16IdbTestCnt = 0;
			}
			break;

		case M_HOLD_PHASE:
			lu16IdbTestCnt = lu16IdbTestCnt + 1;

			if (lu16IdbTestCnt >= M_HOLD_TIME)
			{
				lu8IdbTestPhase = M_RELEASE_PHASE;
				lu16IdbTestCnt = 0;
			}
			else{ /* Do Nothing */ }

			break;

		case M_RELEASE_PHASE:
			lu16IdbTestPedal_1_100mm = L_s16GenTestPedal_1_100mm(M_RELEASE_PEDAL_RATE, lu16IdbTestPedal_1_100mm, M_RELEASE_TARGET_PEDAL_TRAVEL);

			if (lu16IdbTestPedal_1_100mm <= M_RELEASE_TARGET_PEDAL_TRAVEL)
			{
				lu16IdbTestPedal_1_100mm = M_RELEASE_TARGET_PEDAL_TRAVEL;
				lu8IdbTestPhase = M_VALVE_RESET_PHASE;

				lu16IdbTestCnt = 0;
			}
			break;

		case M_VALVE_RESET_PHASE:
			lu16IdbTestCnt = lu16IdbTestCnt + 1;

			if (lu16IdbTestCnt >= M_VALVE_RESET_TIME)
			{
				lu8IdbTestPhase = M_CHECK_LOOP_PHASE;
				lu16IdbTestCnt = 0;
			}
			else if (lu16IdbTestCnt >= M_PCTRL_FADEOUT_TIME)
			{
				stInternalInterface_r->stTestOverrideConfig.u1OverridePCtrlState = TRUE;
				stInternalInterface_r->s8PCtrlState = S8_PRESS_READY;
			}
			else
			{
				/* Simulator Valve Reset Using PCtrlState */
				stInternalInterface_r->stTestOverrideConfig.u1OverridePCtrlState = TRUE;
				stInternalInterface_r->s8PCtrlState = S8_PRESS_FADE_OUT;
			}
			break;

		case M_CHECK_LOOP_PHASE:
			lu16IdbTestLoopCnt = lu16IdbTestLoopCnt + 1;

			if (lu16IdbTestLoopCnt == M_TEST_PATTERN_COUNT)
			{
				lu8IdbTestPhase = M_NON_CONTROL_PHASE;
				lu16IdbTestPedal_1_100mm = S16_TRAVEL_0_MM;
				stInternalInterface_r->u8TestReady = FALSE;
			}
			else
			{
				lu8IdbTestPhase = M_INITIAL_PHASE;
				lu8IdbTestTrigger = FALSE;
				lu16IdbTestCnt = 0;
			}
			break;

		default:
			break;
	}

	if ( stInternalInterface_r->u8TestReady == TRUE)
	{
		stInternalInterface_r->s16IdbTestPedal_1_10mm = L_u16LimitMinMax(lu16IdbTestPedal_1_100mm/10, 0, M_PEDAL_TRAVEL_MAX);	/* 0.1mm resulotion*/
	}
	else
	{
		lu8IdbTestPhase = M_NON_CONTROL_PHASE;
		stInternalInterface_r->s16IdbTestPedal_1_10mm = L_u16LimitDiff(S16_TRAVEL_0_MM, stInternalInterface_r->s16IdbTestPedal_1_10mm, S16_TRAVEL_0_2_MM, S16_TRAVEL_0_2_MM);
	}

	stInternalInterface_r->s16TestStateMon = (int16_t)lu8IdbTestPhase;

	return (* stInternalInterface_r);
}

stTestOverrideConfig_t LIDB_stPedalSignalConfig(stTestOverrideConfig_t * pstTestOverrideConfig)
{
	pstTestOverrideConfig->u1OverridePCtrlState				= M_OVERRIDE_PCTRLSTATE;;
	pstTestOverrideConfig->u1OverridePCtrlBoostMode			= M_OVERRIDE_PCTRLBOOSTMODE;
	pstTestOverrideConfig->u1OverrideReqBoostMode			= M_OVERRIDE_REQBOOSTMODE;
	pstTestOverrideConfig->u1OverridePedalSiganl			= M_OVERRIDE_PEDAL_SIGNAL;
	pstTestOverrideConfig->u1OverrideTargetPressure			= M_OVERRIDE_TARGET_PRESSURE;
	pstTestOverrideConfig->u1OverrideWheelTargetPressure	= M_OVERRIDE_WHEEL_TARGET_PRESSURE;
	pstTestOverrideConfig->u1OverrideChamberVolumeMtr		= M_OVERRIDE_CHAMBERVOLUME_MTR;
	pstTestOverrideConfig->u1OverrideChamberVolumeVlv		= M_OVERRIDE_CHAMBERVOLUME_VLV;
	pstTestOverrideConfig->u1OverrideDeltaStroke			= M_OVERRIDE_DELTA_STROKE;
	pstTestOverrideConfig->u1OverrideCut1VlvCtrl			= M_OVERRIDE_CUT1_VLV_CONTROL;
	pstTestOverrideConfig->u1OverrideCut2VlvCtrl			= M_OVERRIDE_CUT2_VLV_CONTROL;
	pstTestOverrideConfig->u1OverrideSimVlvCtrl				= M_OVERRIDE_SIM_VLV_CONTROL;
	pstTestOverrideConfig->u1OverrideCvVlvCtrl				= M_OVERRIDE_CV_VLV_CONTROL;
	pstTestOverrideConfig->u1OverrideRelVlvCtrl				= M_OVERRIDE_REL_VLV_CONTROL;
	pstTestOverrideConfig->u1OverrideBalVlvCtrl				= M_OVERRIDE_BAL_VLV_CONTROL;
	pstTestOverrideConfig->u1OverridePdVlvCtrl				= M_OVERRIDE_PD_VLV_CONTROL;
	pstTestOverrideConfig->u1OverrideWheelInVlvCtrl			= M_OVERRIDE_WHEEL_IN_VLV_CONTROL;
	pstTestOverrideConfig->u1OverrideWheelOutVlvCtrl		= M_OVERRIDE_WHEEL_OUT_VLV_CONTROL;

	return (*pstTestOverrideConfig);
}

static int16_t L_s16GenTestPedal_1_100mm(int16_t s16PedalRate, int16_t s16CurrentPedal, int16_t s16TargetPedalTravel)
{
	int16_t s16Pedal = 0;

	s16Pedal = L_s16LimitMinMax(s16CurrentPedal + s16PedalRate / M_ONE_SECOND_TO_SCAN, S16_TRAVEL_0_MM, M_PEDAL_TRAVEL_MAX);


	return s16Pedal;
}

#endif	/* (M_IDB_TEST == ENABLE) */
