# \file
#
# \brief IdbTestMode
#
# This file contains the implementation of the SWC
# module IdbTestMode.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

IdbTestMode_CORE_PATH     := $(MANDO_FAL_ROOT)\IdbTestMode
IdbTestMode_SRC_PATH      := $(IdbTestMode_CORE_PATH)\SRC
IdbTestMode_HDR_PATH      := $(IdbTestMode_CORE_PATH)\HDR

CC_INCLUDE_PATH    += $(IdbTestMode_SRC_PATH)
CC_INCLUDE_PATH    += $(IdbTestMode_HDR_PATH)