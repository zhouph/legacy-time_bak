# \file
#
# \brief IdbTestMode
#
# This file contains the implementation of the SWC
# module IdbTestMode.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += IdbTestMode_src

IdbTestMode_src_FILES  += $(IdbTestMode_SRC_PATH)\L_IdbTestMain.c
IdbTestMode_src_FILES  += $(IdbTestMode_SRC_PATH)\L_IdbTestPatternLib.c
IdbTestMode_src_FILES  += $(IdbTestMode_SRC_PATH)\DeltaStrokeTest\L_IdbDeltaStrokeTest.c
IdbTestMode_src_FILES  += $(IdbTestMode_SRC_PATH)\PedalSignal\L_IdbTestPedalSignal.c
IdbTestMode_src_FILES  += $(IdbTestMode_SRC_PATH)\ChamberVolume\L_IdbTestChamberVolume.c
IdbTestMode_src_FILES  += $(IdbTestMode_SRC_PATH)\SingleTargetPattern\L_IdbTestSingleTargetTest.c
IdbTestMode_src_FILES  += $(IdbTestMode_SRC_PATH)\StrokeMapTest\L_IdbTestStrokeMap.c
IdbTestMode_src_FILES  += $(IdbTestMode_SRC_PATH)\SingleValveTest\L_IdbTestSingleValve.c
IdbTestMode_src_FILES  += $(IdbTestMode_SRC_PATH)\PressurePerformanceTest\L_IdbTestPressPerformance.c

