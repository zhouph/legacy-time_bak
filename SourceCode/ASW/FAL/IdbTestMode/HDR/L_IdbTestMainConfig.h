/*
 * L_IdbTestConfig.h
 *
 *  Created on: 2015. 8. 27.
 *      Author: Seokjong Kim
 */

#ifndef L_IDBTESTCONFIG_H_
#define L_IDBTESTCONFIG_H_

#include "../../../../include/Asw_Types.h"
#include "Sal_Types.h"
#include "Rte_Types.h"
#include "RteDataHandle_Types.h"

#if (M_IDB_TEST == ENABLE)
/* Test Mode */
#define		M_IDB_STROKE_MAP_TEST				1
#define		M_IDB_GEN_PEDALSIG_TEST				2
#define 	M_IDB_GEN_TGTPRESS_TEST				3
#define 	M_IDB_GEN_TGTSTROKE_TEST			4
#define		M_IDB_PRESSURE_PERFORMANCE_TEST		5
#define		M_IDB_SINGLE_TARGET_PATTERN_TEST	6
#define		M_IDB_CHAMBER_VOLUME_CHANGE_TEST	7
#define		M_IDB_SINGLE_VALVE_TEST				8

/* Test Mode Selection */
#define		M_IDB_TEST_MODE						M_IDB_GEN_TGTSTROKE_TEST

/* Trigger Mode */
#define __HW_TEST_TRIGERED_BY_TCS_SW 			0
#define __HW_TEST_TRIGERED_BY_PEDAL 			1

/* Trigger Mode Selection */
#define __HW_TEST_TRIGERED_MODE 				__HW_TEST_TRIGERED_BY_TCS_SW

#define M_PCTRL_ACT_THRESHOLD					S16_PCTRL_PRESS_0_BAR

#endif /* (M_IDB_TEST == ENABLE) */
#endif /* L_IDBTESTCONFIG_H_ */
