/*
 * L_IdbTestPatternLib.h
 *
 *  Created on: 2015. 6. 10.
 *      Author: sohyun.ahn
 */

#ifndef L_IDBTESTPATTERNLIB_H_
#define L_IDBTESTPATTERNLIB_H_

#include "L_IdbTestMain.h"

#if (M_IDB_TEST == ENABLE)
#define TWO_PWR_15      32768
#define mult_r(a, b)    ((int16_t)(((int32_t)((Frac16)(a)) * (int32_t)((int16_t)(b))) / TWO_PWR_15))
#define mtr_mult_r(a, b)    ((int16_t)(((int32_t)((Frac16)(a)) * (int32_t)((int16_t)(b))) / TWO_PWR_15))
#define FRAC16(x) 		((Frac16)((x) < 0.999969482421875 ? ((x) >= -1 ? (x)*0x8000 : 0x8000) : 0x7FFF))
#define ORIGINE_STROKE_0MM				(0)

/* IDB SPECIALL PATTERN */
typedef struct
{
	int16_t		s16Amp;
	int16_t		s16Tp;
	int32_t		s32cnt;
	int16_t		s16phase;
	int16_t		s16offset;
	int16_t		s16fs;
}SWS;		/* parameter of Sine-Wave-Slow: the period of sine wave is slower than 1Hz */

typedef struct
{
	int16_t		s16Amp;
	int16_t		s16fsw;
	int32_t		s32cnt;
	int16_t		s16phase;
	int16_t		s16offset;
	int16_t		s16fs;
	/*
	int16_t		s16count0;
	*/
}SWF;		/* parameter of Sine-Wave-Fast: the period of sine wave is faster than 1Hz */

typedef struct
{
	int16_t		s16xi;		/* in pulse */
	int16_t		s16ti;		/* in msec	*/
	int16_t		s16tf;		/* in msec	*/
	int16_t		s16dt;		/* in msec	*/
	int16_t		s16Tp;		/* in msec	*/
	int16_t		s16cnt;
	int16_t		s16stepHgt;
	int16_t		s16HgtMax;
	int16_t		s16HgtMin;
}STR;

typedef struct
{
	int16_t		s16Kp;
	Frac16		f16Kp;

	int16_t		s16KiTs;
	Frac16		f16KiTs;
	int16_t		s16Kb;
	Frac16		f16Kb;

	int16_t		s16Kd;
	Frac16		f16Kd;
	int16_t		s16fs;
	int16_t		s16wc;

	int16_t		s16uOutMax;
	int16_t		s16uOutMin;

	int32_t		s32uP;
	int32_t		s32uI;
	int32_t		s32uD;
	int16_t		s16uDifference;
	int16_t		s16errOld;
	int16_t		s16errDotFiltOld;
	int16_t		s16uOut;
}PID_Test;

extern int16_t LIDB_s16ReturnToOrigin(int32_t s32CurrentPosition);
extern int32_t LIDB_s32GenParabolaTargetPosi(int32_t StartPosi, int16_t EndCnt, int16_t *pCnt);
extern int16_t	L_s16SineWaveSlow(SWS *pParams);
extern int16_t	L_s16SineWaveFast(SWF *pParams);
extern int16_t	L_s16GoUpStairs(uint16_t *pFlgOn, STR *pParam);
extern int16_t	L_s16GoDownStairs(uint16_t *pFlgOn, STR *pParam);
extern int16_t	L_s16Trapzoid(int16_t HLevel, int16_t cnt0, int16_t cnt1, int16_t cnt2, int16_t cnt3, int16_t cnt);
extern Frac16 MCLIB_f16CosRez360Div1024Deg(Frac16 x);
extern Frac16 MCLIB_f16SinRez360Div1024Deg(Frac16 x);
extern int16_t MCLIB_s16TestPIDController(int32_t xRef, int32_t x, PID_Test *pParam);

#endif /*M_IDB_TEST*/
#endif /* L_IDBTESTPATTERNLIB_H_ */
