/*
 * L_IdbTestMain.h
 *
 *  Created on: 2015. 6. 10.
 *      Author: sohyun.ahn
 */

#ifndef L_IDBTESTMAIN_H_
#define L_IDBTESTMAIN_H_

#include "../../../../include/Asw_Types.h"
#include "Sal_Types.h"
#include "Rte_Types.h"
#include "RteDataHandle_Types.h"
#include "L_IdbTestMainConfig.h"

#if (M_IDB_TEST == ENABLE)

typedef struct
{
    uint32_t lau1ValveAct:  1;

    /* New Interface */
    uint8_t lau8VlvCtrlState;
    uint8_t lau8VlvCtrlRespLv;
    uint8_t lau8VlvMaxholdTime;
}LT_VALVE_ACT_t;

typedef struct
{
	uint8_t	u1OverridePCtrlState			;
	uint8_t	u1OverridePCtrlBoostMode		;
	uint8_t	u1OverrideReqBoostMode			;
	uint8_t	u1OverridePedalSiganl			;
	uint8_t	u1OverrideTargetPressure		;
	uint8_t	u1OverrideWheelTargetPressure	;
	uint8_t	u1OverrideChamberVolumeMtr		;
	uint8_t	u1OverrideChamberVolumeVlv		;
	uint8_t	u1OverrideDeltaStroke			;
	uint8_t	u1OverrideCut1VlvCtrl			;
	uint8_t	u1OverrideCut2VlvCtrl			;
	uint8_t	u1OverrideSimVlvCtrl			;
	uint8_t	u1OverrideCvVlvCtrl				;
	uint8_t	u1OverrideRelVlvCtrl			;
	uint8_t	u1OverrideBalVlvCtrl			;
	uint8_t	u1OverridePdVlvCtrl				;
	uint8_t	u1OverrideWheelInVlvCtrl		;
	uint8_t	u1OverrideWheelOutVlvCtrl		;
}stTestOverrideConfig_t;

typedef struct
{
	uint8_t     u8IdbTestLoggingTriggerFlg;
	uint8_t     u8IdbTestAnalyzerFlg;
	uint8_t		u8StrokeDirection;
	uint8_t		u8IdbTestChangeWhlVolCnt;
	uint8_t		u8IdbTestChangeTestCase ;
	int16_t		s16TestStateMon;
}stLoggerInterface_t;

typedef struct
{
	stTestOverrideConfig_t stTestOverrideConfig;

	uint8_t		u8TestMode;

	uint8_t		u8TestReady;

	int8_t		s8PCtrlState;
	int8_t		s8ReqBoostMode;
	int8_t		s8PCtrlBoostMode;

	/* Chamber Volume */
	int8_t		s8MTRPChamberVolume;
	int8_t		s8VLVPChamberVolume;

	/* Test State Monitoring */
	int16_t		s16TestStateMon;
	
	/* Pedal Signal */
	int16_t		s16IdbTestPedal_1_10mm;

	/* Circuit Sensor Signal */
	int16_t     s16TestCircuitP1_100bar;
	
	/* MTR RPM */
	int16_t     s16TestMotMechAngleSpdFild;
	
	/* Motor Posi */
	int32_t		s32TargetPosi;
	int32_t		s32MeasuredPosi;

	/* Delta Stroke Target */
	int16_t		s16TestTgtDeltaStroke;

	/* Target Pressure */
	int16_t		s16TestTargetPress;
	int16_t		s16TestDeltaTargetPress;

	/* Target Pressure Rate */
	int16_t		s16TestTargetPressRate;

	/* Wheel Target Pressure */
	int16_t		s16TestTargetP_FL;
	int16_t		s16TestTargetP_FR;
	int16_t		s16TestTargetP_RL;
	int16_t		s16TestTargetP_RR;

	/* Wheel Target Pressure Rate */
	int16_t		s16TestTargetPRate_FL;
	int16_t		s16TestTargetPRate_FR;
	int16_t		s16TestTargetPRate_RL;
	int16_t		s16TestTargetPRate_RR;

	/* Pressure Control Active */
	uint8_t		u8PCtrlActFlg;
	int16_t		s16PCtrlActThreshPress;

	/* Valve Control */
	uint8_t  u8TestActFlg_Cut;
	uint8_t  u8TestActFlg_Sim;
	uint8_t  u8TestActFlg_CV ;
	uint8_t  u8TestActFlg_Rel;
	uint8_t  u8TestActFlg_Bal;
	uint8_t  u8TestActFlg_PD ;

	uint8_t  u8TestRespLv_Cut;
	uint8_t  u8TestRespLv_Sim;
	uint8_t  u8TestRespLv_CV;
	uint8_t  u8TestRespLv_Rel;
	uint8_t  u8TestRespLv_Bal;
	uint8_t  u8TestRespLv_PD;

	/* Whl valve control */
	uint8_t  u8TestInletActFlg_FL;
	uint8_t  u8TestInletActFlg_FR;
	uint8_t  u8TestInletActFlg_RL;
	uint8_t  u8TestInletActFlg_RR;

	/* Test logger */
	stLoggerInterface_t stLogInterface;

}stInternalInterface_t;

typedef struct
{
	int16_t m_ls16TestPedlTrvlFinal;
	uint8_t m_lu8TestTCSDisabledBySW;
	uint8_t m_lu8TestBlsFild;

	int32_t m_ls32TestTgtPosi;
	int16_t m_ls16TestCircuitP1_100bar;
	int32_t m_ls32TestStkPosnmeasd;
	int16_t m_ls16TestMotMechAngleSpdFild;
	int16_t m_ls16MotIqMeasd;

}IdbTest_Input_t;

typedef struct
{
	LT_VALVE_ACT_t m_lsTest_CUT1 ;
	LT_VALVE_ACT_t m_lsTest_CUT2 ;
	LT_VALVE_ACT_t m_lsTest_SIM ;
	LT_VALVE_ACT_t m_lsTest_RELV ;
	LT_VALVE_ACT_t m_lsTest_CV ;
	LT_VALVE_ACT_t m_lsTest_BalV ;
	LT_VALVE_ACT_t m_lsTest_PDV ;

	uint8_t m_lu8TestInletActFlg_FL;
	uint8_t m_lu8TestInletActFlg_FR;
	uint8_t m_lu8TestInletActFlg_RL;
	uint8_t m_lu8TestInletActFlg_RR;
}stIdbTestValveActSet;

typedef struct
{
	uint8_t m_lu8TestMode;

	uint8_t m_lu8TestReady;

	stTestOverrideConfig_t m_lstTestOverrideConfig;

	int8_t m_ls8PCtrlState;
	int8_t m_ls8PCtrlBoostMode;
	int8_t m_ls8ReqBoostMode;

	uint8_t m_lu8TestPosiOnlyFlg;

	int16_t m_lss16IdbTestPedal_1_10mm;


	int8_t m_lss8MTRPChamberVolume;
	int8_t m_lss8VLVPChamberVolume;

	int16_t m_ls16TestTgtDeltaStroke;

	int16_t m_ls16TestTargetPress;
	int16_t m_ls16TestTargetPressDel;

	int16_t m_ls16TestTargetPressRate;

	int16_t m_ls16TestTP_FL;
	int16_t m_ls16TestTP_FR;
	int16_t m_ls16TestTP_RL;
	int16_t m_ls16TestTP_RR;

	int16_t m_ls16TestTPRate_FL;
	int16_t m_ls16TestTPRate_FR;
	int16_t m_ls16TestTPRate_RL;
	int16_t m_ls16TestTPRate_RR;

	uint8_t m_lu8PCtrlActFlg;

	LT_VALVE_ACT_t m_lsTest_CUT1 ;
	LT_VALVE_ACT_t m_lsTest_CUT2 ;
	LT_VALVE_ACT_t m_lsTest_SIM ;
	LT_VALVE_ACT_t m_lsTest_RELV ;
	LT_VALVE_ACT_t m_lsTest_CV ;
	LT_VALVE_ACT_t m_lsTest_BalV ;
	LT_VALVE_ACT_t m_lsTest_PDV ;

	uint8_t m_lu8TestInletActFlg_FL;
	uint8_t m_lu8TestInletActFlg_FR;
	uint8_t m_lu8TestInletActFlg_RL;
	uint8_t m_lu8TestInletActFlg_RR;

	stLoggerInterface_t stLogInterface;

}IdbTest_Output_t;

/* external interface */
extern IdbTest_Output_t stIdbTest_Output;


/* external interface */

extern void L_vCallTestIDB(void);
extern int8_t LIDB_s8TestPressCtrlState(uint8_t u8TestReadyFlgInput);

#endif/*M_IDB_TEST*/

#endif /* L_IDBTESTMAIN_H_ */
