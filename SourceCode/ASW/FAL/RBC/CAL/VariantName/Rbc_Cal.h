/**
 * @defgroup Rbc_Cal Rbc_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Rbc_Cal.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef RBC_CAL_H_
#define RBC_CAL_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Rbc_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

typedef struct
{

	uint8_t      	U8_RB_MAIN_DISABLE;                	/* Comment [ RBC Prohibit Condtion ] */
	            	                                   	/* Group   [ /apCalRbc ] */
	            	                                   	/* Scale   [ None ]  Range [ 0 <= X <= 1 ] */
	uint8_t  	  	U8_R_PRESS_VS_TQ_RATIO_F;          	/* Comment [ Pressure to TQ Ratio for Front Wheel ] */
	            	                                   	/* Group   [ /apCalRbc/Sys. Parameter ] */
	            	                                   	/* Scale   [ None ]  Range [ 0 <= X <= 100 ] */
	uint8_t    	 	U8_R_PRESS_VS_TQ_RATIO_R;          	/* Comment [ Pressure to TQ Ratio for Rear Wheel ] */
	            	                                   	/* Group   [ /apCalRbc/Sys. Parameter ] */
	            	                                   	/* Scale   [ None ]  Range [ 0 <= X <= 100 ] */
	uint16_t    	U16_R_MBO_PUSH_SET;                	/* Comment [ RBC Enter Condition for Driver Demands of Braking ] */
	            	                                   	/* Group   [ /apCalRbc/Enter Condition/Enter by Target Press ] */
	            	                                   	/* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ 0 <= X <= 25 ] */
	uint8_t    		U8_INHIB_EXIT_BY_APS;              	/* Comment [ RBC Inhibit Exit Condition for APS ] */
	            	                                   	/* Group   [ /apCalRbc/Enter Condition/Enter by APS ] */
	            	                                   	/* Scale   [ None ]  Range [ 0 <= X <= 98 ] */
	uint8_t    		U8_INHIB_EXIT_PRESS_BY_MPRT;       	/* Comment [ RBC Inhibit Exit Condition for MPRT Pressure ] */
	            	                                   	/* Group   [ /apCalRbc/Enter Condition/Enter by MPRT Press ] */
	            	                                   	/* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ 0 <= X <= 25.5 ] */
	uint8_t    		U8_RB_ENTER_SPEED_LIMIT;           	/* Comment [ U8_RB_REDUCTION_START_SPEED보다 높은 속도로 설정 ] */
	            	                                   	/* Group   [ /apCalRbc/Enter Condition/Enter by Speed ] */
	            	                                   	/* Scale   [ f(x) = (X + 0) * 0.125 ]  Range [ 0 <= X <= 31.25 ] */
	uint16_t    	U16_R_MBO_RELEASE_SET;             	/* Comment [ RBC Exit Condition for Driver Demands of No Braking ] */
	            	                                   	/* Group   [ /apCalRbc/Exit Condition/Exit by Target Press ] */
	            	                                   	/* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ 0 <= X <= 25 ] */
	uint8_t    		U8_INHIB_SET_BY_APS;               	/* Comment [ RBC Inhibit Enter Condition for APS ] */
	            	                                   	/* Group   [ /apCalRbc/Exit Condition/Exit by APS ] */
	            	                                   	/* Scale   [ None ]  Range [ 0 <= X <= 99 ] */
	uint8_t    		U8_INHIB_SET_PRESS_BY_MPRT;        	/* Comment [ RBC Inhibit Enter Condition for MPRT Pressure ] */
	            	                                   	/* Group   [ /apCalRbc/Exit Condition/Exit by MPRT Press ] */
	            	                                   	/* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ 0 <= X <= 25.5 ] */
	uint8_t    		U8_RB_ENABLE_SPEED_LIMIT;          	/* Comment [ Minimum Speed to Enable Regenerative Braking Fuction ] */
														/* Group   [ /apCalRbc/Exit Condition/Exit by Speed ] */
														/* Scale   [ f(x) = (X + 0) * 0.125 ]  Range [ 0 <= X <= 10 ] */
	int16_t     	S16_CBC_RBC_INHIBIT_AY_ENTER_TH;   	/* Comment [ Corner Brake시 RBC 제어 금지 진입 조건 ] */
														/* Group   [ /apCalRbc/Inhibit Condition/Corner Brake Detect ] */
														/* Scale   [ f(x) = (X + 0) * 0.001 ]  Range [ 0 <= X <= 0.9 ] */
	int16_t     	S16_CBC_RBC_INHIBIT_AY_EXIT_TH;    	/* Comment [ Corner Brake시 RBC 제어 금지 해제 조건 ] */
														/* Group   [ /apCalRbc/Inhibit Condition/Corner Brake Detect ] */
														/* Scale   [ f(x) = (X + 0) * 0.001 ]  Range [ 0 <= X <= 0.9 ] */
	int8_t     		S8_FRONT_SLIP_DET_THRES_SLIP;      	/* Comment [ CBC Disable시 전륜과 후륜의 Slip 차이 ] */
														/* Group   [ /apCalRbc/Inhibit Condition/Front Wheel Slip Detect ] */
														/* Scale   [ None ]  Range [ 0 <= X <= 127 ] */
	int8_t     		S8_FRONT_SLIP_DET_THRES_VDIFF;     	/* Comment [ CBC Disable시 전륜과 후륜의 속도 차이 ] */
														/* Group   [ /apCalRbc/Inhibit Condition/Front Wheel Slip Detect ] */
														/* Scale   [ f(x) = (X + 0) * 0.125 ]  Range [ 0 <= X <= 15.875 ] */
	int16_t     	S16_RB_FAST_PEDAL_APPLY_THRES;     	/* Comment [ S16_FAST_PEDAL_APPLY_THRES 보다 작게 설정 ] */
														/* Group   [ /apCalRbc/Inhibit Condition/Quick Brake Detect ] */
														/* Scale   [ None ]  Range [ -32768 <= X <= 32767 ] */
	int16_t     	S16_ONS_BRK_CNT_POSSIBLE_SPD;      	/* Comment [ ONS 해제 브레이크 횟수를 count하기 위한 속도 조건 ] */
														/* Group   [ /apCalRbc/Inhibit Condition/Overnight soaking Detect ] */
														/* Scale   [ f(x) = (X + 0) * 0.125 ]  Range [ -4096 <= X <= 4095.875 ] */
	int16_t     	S16_ONS_BRK_CNT_TIME_TH;           	/* Comment [ ONS 해제 브레이크 횟수를 count하기 위한 최소 브레이크 유지시간 ] */
														/* Group   [ /apCalRbc/Inhibit Condition/Overnight soaking Detect ] */
														/* Scale   [ f(x) = (X + 0) * 0.005 ]  Range [ -163.84 <= X <= 163.835 ] */
	int16_t     	S16_ONS_TEMP_DIFF;                 	/* Comment [ ONS 판단에 필요한 외기온도와 냉각수 온도의 차이 ] */
														/* Group   [ /apCalRbc/Inhibit Condition/Overnight soaking Detect ] */
														/* Scale   [ None ]  Range [ 0 <= X <= 100 ] */
	uint16_t    	U16_ONS_EXIT_BRK_CNT;              	/* Comment [ ONS 해제 가능한 Bake 횟수 ] */
														/* Group   [ /apCalRbc/Inhibit Condition/Overnight soaking Detect ] */
														/* Scale   [ None ]  Range [ 0 <= X <= 65535 ] */
	uint16_t    	U16_ONS_TIME_CNT_AF_IGN_ON;        	/* Comment [ IGN ON 후 ONS 해제까지 필요한 시간 ] */
														/* Group   [ /apCalRbc/Inhibit Condition/Overnight soaking Detect ] */
														/* Scale   [ None ]  Range [ 0 <= X <= 65535 ] */
	uint8_t    		U8_RB_TQ_CMD_INC_RATE;             	/* Comment [ Inc Rate for RB TQ Command ] */
														/* Group   [ /apCalRbc/Rbc TQ Cmd/TQ Cmd Inc Rate ] */
														/* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ 0 <= X <= 0.5 ] */
	uint8_t    		U8_MAX_RB_TQ_PRESS_LIMIT;          	/* Comment [ Maximum RB Limit for Brake ] */
														/* Group   [ /apCalRbc/Rbc TQ Cmd/TQ Cmd Max Limit ] */
														/* Scale   [ None ]  Range [ 0 <= X <= 100 ] */
	uint16_t    	U16_MAX_POS_TQ_EQ_CONST;           	/* Comment [ EQ3 Constant for Max Possible RB TQ Equation ] */
														/* Group   [ /apCalRbc/Rbc TQ Cmd/TQ Cmd EQ3 ] */
														/* Scale   [ None ]  Range [ 0 <= X <= 65535 ] */
	uint8_t    		U8_MAX_POS_TQ_MAX_SPEED;           	/* Comment [ Max Speed to Enable RB by Motor Characteristic ] */
														/* Group   [ /apCalRbc/Rbc TQ Cmd/TQ Cmd Max Speed ] */
														/* Scale   [ None ]  Range [ 0 <= X <= 250 ] */
	uint8_t    		U8_TAR_WL_PRESS_MIN_1;             	/* Comment [ Minimum of Target Wheel Pressure for RB ] */
														/* Group   [ /apCalRbc/Target Wheel Press/Min. Target Wheel Press ] */
														/* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ 0 <= X <= 25.5 ] */
	int16_t     	S16_RBC_SLOW_FADE_OUT_RATE;        	/* Comment [ RBC제어 Slow fade out 조건 ] */
														/* Group   [ /apCalRbc/Fade Out Condtion/Dec Rate ] */
														/* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ 0 <= X <= 2 ] */
	int16_t   	  	S16_MAX_RBPRESS_DEC_RATE;           /* Comment [ RB press decrease limit ] */
														/* Group   [ /apCalRbc/New RBC Tuning ] */
														/* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ 0 <= X <= 10 ] */
	int16_t    		S16_MAX_RBPRESS_DEC_RATE_FAIL;      	/* Comment [ RB press increase limit in fail situation ] */
														/* Group   [ /apCalRbc/New RBC Tuning ] */
														/* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ 0 <= X <= 10 ] */
	int16_t    		S16_MAX_RBPRESS_INC_RATE;           /* Comment [ RB press increase limit ] */
														/* Group   [ /apCalRbc/New RBC Tuning ] */
														/* Scale   [ f(x) = (X + 0) * 0.1 ]  Range [ 0 <= X <= 10 ] */
	uint16_t    	U16_MAX_POS_TQ_EQ_RATIO_10NM;      	/* Comment [ EQ3 Ratio for Max Possible RB TQ Equation for 10NM resol ] */
														/* Group   [ /apCalRbc/New RBC Tuning ] */
														/* Scale   [ None ]  Range [ 0 <= X <= 65535 ] */
	uint8_t    		U8_RB_REDUCTION_START_SPEED;       	/* Comment [ RB->HB blending start speed ] */
														/* Group   [ /apCalRbc/New RBC Tuning ] */
														/* Scale   [ f(x) = (X + 0) * 0.125 ]  Range [ 0 <= X <= 31.875 ] */
	            	                                   	/* Scale   [ None ]  Range [ -32768 <= X <= 32767 ] */

} RegenBrakeCooperation_CalibType;


#define	U8_RB_MAIN_DISABLE                      	RBCcalib.U8_RB_MAIN_DISABLE
#define	U8_R_PRESS_VS_TQ_RATIO_F                	RBCcalib.U8_R_PRESS_VS_TQ_RATIO_F
#define	U8_R_PRESS_VS_TQ_RATIO_R                	RBCcalib.U8_R_PRESS_VS_TQ_RATIO_R
#define	U16_R_MBO_PUSH_SET                      	RBCcalib.U16_R_MBO_PUSH_SET
#define	U8_INHIB_EXIT_BY_APS                    	RBCcalib.U8_INHIB_EXIT_BY_APS
#define	U8_INHIB_EXIT_PRESS_BY_MPRT             	RBCcalib.U8_INHIB_EXIT_PRESS_BY_MPRT
#define	U8_RB_ENTER_SPEED_LIMIT                 	RBCcalib.U8_RB_ENTER_SPEED_LIMIT
#define	U16_R_MBO_RELEASE_SET                   	RBCcalib.U16_R_MBO_RELEASE_SET
#define	U8_INHIB_SET_BY_APS                     	RBCcalib.U8_INHIB_SET_BY_APS
#define	U8_INHIB_SET_PRESS_BY_MPRT              	RBCcalib.U8_INHIB_SET_PRESS_BY_MPRT
#define	U8_RB_ENABLE_SPEED_LIMIT                	RBCcalib.U8_RB_ENABLE_SPEED_LIMIT
#define	S16_CBC_RBC_INHIBIT_AY_ENTER_TH         	RBCcalib.S16_CBC_RBC_INHIBIT_AY_ENTER_TH
#define	S16_CBC_RBC_INHIBIT_AY_EXIT_TH          	RBCcalib.S16_CBC_RBC_INHIBIT_AY_EXIT_TH
#define	S8_FRONT_SLIP_DET_THRES_SLIP            	RBCcalib.S8_FRONT_SLIP_DET_THRES_SLIP
#define	S8_FRONT_SLIP_DET_THRES_VDIFF           	RBCcalib.S8_FRONT_SLIP_DET_THRES_VDIFF
#define	S16_RB_FAST_PEDAL_APPLY_THRES           	RBCcalib.S16_RB_FAST_PEDAL_APPLY_THRES
#define	S16_ONS_BRK_CNT_POSSIBLE_SPD            	RBCcalib.S16_ONS_BRK_CNT_POSSIBLE_SPD
#define	S16_ONS_BRK_CNT_TIME_TH                 	RBCcalib.S16_ONS_BRK_CNT_TIME_TH
#define	S16_ONS_TEMP_DIFF                       	RBCcalib.S16_ONS_TEMP_DIFF
#define	U16_ONS_EXIT_BRK_CNT                    	RBCcalib.U16_ONS_EXIT_BRK_CNT
#define	U16_ONS_TIME_CNT_AF_IGN_ON              	RBCcalib.U16_ONS_TIME_CNT_AF_IGN_ON
#define	U8_RB_TQ_CMD_INC_RATE                   	RBCcalib.U8_RB_TQ_CMD_INC_RATE
#define	U8_MAX_RB_TQ_PRESS_LIMIT                	RBCcalib.U8_MAX_RB_TQ_PRESS_LIMIT
#define	U16_MAX_POS_TQ_EQ_CONST                 	RBCcalib.U16_MAX_POS_TQ_EQ_CONST
#define	U8_MAX_POS_TQ_MAX_SPEED                 	RBCcalib.U8_MAX_POS_TQ_MAX_SPEED
#define	U8_TAR_WL_PRESS_MIN_1                   	RBCcalib.U8_TAR_WL_PRESS_MIN_1
#define	S16_RBC_SLOW_FADE_OUT_RATE              	RBCcalib.S16_RBC_SLOW_FADE_OUT_RATE
#define	S16_MAX_RBPRESS_DEC_RATE                 	RBCcalib.S16_MAX_RBPRESS_DEC_RATE
#define	S16_MAX_RBPRESS_DEC_RATE_FAIL            	RBCcalib.S16_MAX_RBPRESS_DEC_RATE_FAIL
#define	S16_MAX_RBPRESS_INC_RATE                 	RBCcalib.S16_MAX_RBPRESS_INC_RATE
#define	U16_MAX_POS_TQ_EQ_RATIO_10NM            	RBCcalib.U16_MAX_POS_TQ_EQ_RATIO_10NM
#define	U8_RB_REDUCTION_START_SPEED             	RBCcalib.U8_RB_REDUCTION_START_SPEED



/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
extern RegenBrakeCooperation_CalibType RBCcalib;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* RBC_CAL_H_ */
/** @} */
