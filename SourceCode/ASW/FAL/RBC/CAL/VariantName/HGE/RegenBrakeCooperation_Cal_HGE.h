	/* uchar8_t    	U8_RB_MAIN_DISABLE                    */		       0,	/* Comment [ RBC Prohibit Condtion ] */
	/* uchar8_t    	U8_R_PRESS_VS_TQ_RATIO_F              */		      70,	/* Comment [ Pressure to TQ Ratio for Front Wheel ] */
	/* uchar8_t    	U8_R_PRESS_VS_TQ_RATIO_R              */		      23,	/* Comment [ Pressure to TQ Ratio for Rear Wheel ] */
	/* uint16_t    	U16_R_MBO_PUSH_SET                    */		      100,	/* Comment [ RBC Enter Condition for Driver Demands of Braking ] */
																				/* ScaleVal[      1 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 25 ] */
	/* uchar8_t    	U8_INHIB_EXIT_BY_APS                  */		       2,	/* Comment [ RBC Inhibit Exit Condition for APS ] */
	/* uchar8_t    	U8_INHIB_EXIT_PRESS_BY_MPRT           */		      30,	/* Comment [ RBC Inhibit Exit Condition for MPRT Pressure ] */
																				/* ScaleVal[    0.3 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_RB_ENTER_SPEED_LIMIT               */		      136,	/* Comment [ U8_RB_REDUCTION_START_SPEED보다 높은 속도로 설정 ] */
																				/* ScaleVal[     17 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.25 ] */
	/* uint16_t    	U16_R_MBO_RELEASE_SET                 */		      90,	/* Comment [ RBC Exit Condition for Driver Demands of No Braking ] */
																				/* ScaleVal[    0.9 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 25 ] */
	/* uchar8_t    	U8_INHIB_SET_BY_APS                   */		       5,	/* Comment [ RBC Inhibit Enter Condition for APS ] */
	/* uchar8_t    	U8_INHIB_SET_PRESS_BY_MPRT            */		      10,	/* Comment [ RBC Inhibit Enter Condition for MPRT Pressure ] */
																				/* ScaleVal[    0.1 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 25.5 ] */
	/* uchar8_t    	U8_RB_ENABLE_SPEED_LIMIT              */		      48,	/* Comment [ Minimum Speed to Enable Regenerative Braking Fuction ] */
																				/* ScaleVal[      6 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 10 ] */
	/* int16_t     	S16_CBC_RBC_INHIBIT_AY_ENTER_TH       */		     700,	/* Comment [ CBC Disable시, Corner Brake시 RBC 제어 금지 진입 조건 ] */
																				/* ScaleVal[      0.7 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 0.9 ] */
	/* int16_t     	S16_CBC_RBC_INHIBIT_AY_EXIT_TH        */		     600,	/* Comment [ CBC Disable시, Corner Brake시 RBC 제어 금지 해제 조건 ] */
																				/* ScaleVal[      0.6 g ]	Scale [ f(x) = (X + 0) * 0.001 ]  Range   [ 0 <= X <= 0.9 ] */
	/* char8_t     	S8_FRONT_SLIP_DET_THRES_SLIP          */		       5,	/* Comment [전륜과 후륜의 Slip 차이 ] */
	/* char8_t     	S8_FRONT_SLIP_DET_THRES_VDIFF         */		      20,	/* Comment [전륜과 후륜의 속도 차이 ] */
																				/* ScaleVal[    2.5 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 15.875 ] */
	/* int16_t     	S16_RB_FAST_PEDAL_APPLY_THRES         */		     240,	/* Comment [ S16_FAST_PEDAL_APPLY_THRES 보다 작게 설정 ] */
	/* int16_t     	S16_ONS_BRK_CNT_POSSIBLE_SPD          */		     240,	/* Comment [ ONS 해제 브레이크 횟수를 count하기 위한 속도 조건 ] */
																				/* ScaleVal[     30 KPH ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ -4096 <= X <= 4095.875 ] */
	/* int16_t     	S16_ONS_BRK_CNT_TIME_TH               */		     200,	/* Comment [ ONS 해제 브레이크 횟수를 count하기 위한 최소 브레이크 유지시간 ] */
																				/* ScaleVal[      1 Sec ]	Scale [ f(x) = (X + 0) * 0.005 ]  Range   [ -163.84 <= X <= 163.835 ] */
	/* int16_t     	S16_ONS_TEMP_DIFF                     */		       5,	/* Comment [ ONS 판단에 필요한 외기온도와 냉각수 온도의 차이 ] */
	/* uint16_t    	U16_ONS_EXIT_BRK_CNT                  */		      10,	/* Comment [ ONS 해제 가능한 Bake 횟수 ] */
	/* uint16_t    	U16_ONS_TIME_CNT_AF_IGN_ON            */		      30,	/* Comment [ IGN ON 후 ONS 해제까지 필요한 시간 ] */
	/* uchar8_t    	U8_RB_TQ_CMD_INC_RATE                 */		      30,	/* Comment [ Inc Rate for RB TQ Command ] */
																				/* ScaleVal[ 0.3 bar/scan ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 0.5 ] */
	/* uchar8_t    	U8_MAX_RB_TQ_PRESS_LIMIT              */		      20,	/* Comment [ Maximum RB Limit for Brake ] */
	/* uint16_t    	U16_MAX_POS_TQ_EQ_CONST               */		       0,	/* Comment [ EQ3 Constant for Max Possible RB TQ Equation ] */
	/* uchar8_t    	U8_MAX_POS_TQ_MAX_SPEED               */		     175,	/* Comment [ Max Speed to Enable RB by Motor Characteristic ] */
	/* uchar8_t    	U8_TAR_WL_PRESS_MIN_1                 */		     100,	/* Comment [ Minimum of Target Wheel Pressure for RB ] */
																				/* ScaleVal[    0.5 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 25.5 ] */
	/* int16_t     	S16_RBC_SLOW_FADE_OUT_RATE            */		      10,	/* Comment [ RBC제어 Slow fade out 조건 ] */
																				/* ScaleVal[  0.1 Bar/s ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 2 ] */
	/* char8_t     	S16_MAX_RBPRESS_DEC_RATE               */		      70,	/* Comment [ RB press decrease limit ] */
																				/* ScaleVal[    0.7 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 10 ] */
	/* int16_t     	S16_MAX_RBPRESS_DEC_RATE_FAIL          */		      200,	/* Comment [ RB press increase limit in fail situation ] */
																				/* ScaleVal[      2 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 10 ] */
	/* int16_t     	S16_MAX_RBPRESS_INC_RATE               */		      70,	/* Comment [ RB press increase limit ] */
																				/* ScaleVal[    0.7 bar ]	Scale [ f(x) = (X + 0) * 0.01 ]  Range   [ 0 <= X <= 10 ] */
	/* uint16_t    	U16_MAX_POS_TQ_EQ_RATIO_10NM          */		    4900,	/* Comment [ EQ3 Ratio for Max Possible RB TQ Equation for 10NM resol ] */
	/* uchar8_t    	U8_RB_REDUCTION_START_SPEED           */		     136,	/* Comment [ RB->HB blending start speed ] */
																				/* ScaleVal[     17 kph ]	Scale [ f(x) = (X + 0) * 0.125 ]  Range   [ 0 <= X <= 31.875 ] */
