/**
 * @defgroup Rbc_Ctrl_Ifa Rbc_Ctrl_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Rbc_Ctrl_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef RBC_CTRL_IFA_H_
#define RBC_CTRL_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Rbc_Ctrl_Read_Rbc_CtrlCanRxRegenInfo(data) do \
{ \
    *data = Rbc_CtrlCanRxRegenInfo; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlCanRxAccelPedlInfo(data) do \
{ \
    *data = Rbc_CtrlCanRxAccelPedlInfo; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlCanRxIdbInfo(data) do \
{ \
    *data = Rbc_CtrlCanRxIdbInfo; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlCanRxEngTempInfo(data) do \
{ \
    *data = Rbc_CtrlCanRxEngTempInfo; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlAbsCtrlInfo(data) do \
{ \
    *data = Rbc_CtrlAbsCtrlInfo; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlAvhCtrlInfo(data) do \
{ \
    *data = Rbc_CtrlAvhCtrlInfo; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlEscCtrlInfo(data) do \
{ \
    *data = Rbc_CtrlEscCtrlInfo; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlSccCtrlInfo(data) do \
{ \
    *data = Rbc_CtrlSccCtrlInfo; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlTcsCtrlInfo(data) do \
{ \
    *data = Rbc_CtrlTcsCtrlInfo; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlCircPFildInfo(data) do \
{ \
    *data = Rbc_CtrlCircPFildInfo; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlPedlTrvlFildInfo(data) do \
{ \
    *data = Rbc_CtrlPedlTrvlFildInfo; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlPedlTrvlRateInfo(data) do \
{ \
    *data = Rbc_CtrlPedlTrvlRateInfo; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlPistPFildInfo(data) do \
{ \
    *data = Rbc_CtrlPistPFildInfo; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlEemFailData(data) do \
{ \
    *data = Rbc_CtrlEemFailData; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlEemSuspectData(data) do \
{ \
    *data = Rbc_CtrlEemSuspectData; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlCanRxRegenInfo_HcuRegenEna(data) do \
{ \
    *data = Rbc_CtrlCanRxRegenInfo.HcuRegenEna; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlCanRxRegenInfo_HcuRegenBrkTq(data) do \
{ \
    *data = Rbc_CtrlCanRxRegenInfo.HcuRegenBrkTq; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlCanRxAccelPedlInfo_AccelPedlVal(data) do \
{ \
    *data = Rbc_CtrlCanRxAccelPedlInfo.AccelPedlVal; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlCanRxAccelPedlInfo_AccelPedlValErr(data) do \
{ \
    *data = Rbc_CtrlCanRxAccelPedlInfo.AccelPedlValErr; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlCanRxIdbInfo_EngMsgFault(data) do \
{ \
    *data = Rbc_CtrlCanRxIdbInfo.EngMsgFault; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlCanRxIdbInfo_GearSelDisp(data) do \
{ \
    *data = Rbc_CtrlCanRxIdbInfo.GearSelDisp; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlCanRxIdbInfo_TcuSwiGs(data) do \
{ \
    *data = Rbc_CtrlCanRxIdbInfo.TcuSwiGs; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlCanRxIdbInfo_HcuServiceMod(data) do \
{ \
    *data = Rbc_CtrlCanRxIdbInfo.HcuServiceMod; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlCanRxIdbInfo_SubCanBusSigFault(data) do \
{ \
    *data = Rbc_CtrlCanRxIdbInfo.SubCanBusSigFault; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlCanRxIdbInfo_CoolantTemp(data) do \
{ \
    *data = Rbc_CtrlCanRxIdbInfo.CoolantTemp; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlCanRxIdbInfo_CoolantTempErr(data) do \
{ \
    *data = Rbc_CtrlCanRxIdbInfo.CoolantTempErr; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlCanRxEngTempInfo_EngTemp(data) do \
{ \
    *data = Rbc_CtrlCanRxEngTempInfo.EngTemp; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlCanRxEngTempInfo_EngTempErr(data) do \
{ \
    *data = Rbc_CtrlCanRxEngTempInfo.EngTempErr; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlAbsCtrlInfo_AbsActFlg(data) do \
{ \
    *data = Rbc_CtrlAbsCtrlInfo.AbsActFlg; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlAbsCtrlInfo_AbsDefectFlg(data) do \
{ \
    *data = Rbc_CtrlAbsCtrlInfo.AbsDefectFlg; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlAbsCtrlInfo_AbsTarPFrntLe(data) do \
{ \
    *data = Rbc_CtrlAbsCtrlInfo.AbsTarPFrntLe; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlAbsCtrlInfo_AbsTarPFrntRi(data) do \
{ \
    *data = Rbc_CtrlAbsCtrlInfo.AbsTarPFrntRi; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlAbsCtrlInfo_AbsTarPReLe(data) do \
{ \
    *data = Rbc_CtrlAbsCtrlInfo.AbsTarPReLe; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlAbsCtrlInfo_AbsTarPReRi(data) do \
{ \
    *data = Rbc_CtrlAbsCtrlInfo.AbsTarPReRi; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlAbsCtrlInfo_FrntWhlSlip(data) do \
{ \
    *data = Rbc_CtrlAbsCtrlInfo.FrntWhlSlip; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlAbsCtrlInfo_AbsDesTarP(data) do \
{ \
    *data = Rbc_CtrlAbsCtrlInfo.AbsDesTarP; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlAbsCtrlInfo_AbsDesTarPReqFlg(data) do \
{ \
    *data = Rbc_CtrlAbsCtrlInfo.AbsDesTarPReqFlg; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlAbsCtrlInfo_AbsCtrlFadeOutFlg(data) do \
{ \
    *data = Rbc_CtrlAbsCtrlInfo.AbsCtrlFadeOutFlg; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlAvhCtrlInfo_AvhActFlg(data) do \
{ \
    *data = Rbc_CtrlAvhCtrlInfo.AvhActFlg; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlAvhCtrlInfo_AvhDefectFlg(data) do \
{ \
    *data = Rbc_CtrlAvhCtrlInfo.AvhDefectFlg; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlAvhCtrlInfo_AvhTarP(data) do \
{ \
    *data = Rbc_CtrlAvhCtrlInfo.AvhTarP; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlEscCtrlInfo_EscActFlg(data) do \
{ \
    *data = Rbc_CtrlEscCtrlInfo.EscActFlg; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlEscCtrlInfo_EscDefectFlg(data) do \
{ \
    *data = Rbc_CtrlEscCtrlInfo.EscDefectFlg; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlEscCtrlInfo_EscTarPFrntLe(data) do \
{ \
    *data = Rbc_CtrlEscCtrlInfo.EscTarPFrntLe; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlEscCtrlInfo_EscTarPFrntRi(data) do \
{ \
    *data = Rbc_CtrlEscCtrlInfo.EscTarPFrntRi; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlEscCtrlInfo_EscTarPReLe(data) do \
{ \
    *data = Rbc_CtrlEscCtrlInfo.EscTarPReLe; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlEscCtrlInfo_EscTarPReRi(data) do \
{ \
    *data = Rbc_CtrlEscCtrlInfo.EscTarPReRi; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlSccCtrlInfo_SccActFlg(data) do \
{ \
    *data = Rbc_CtrlSccCtrlInfo.SccActFlg; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlSccCtrlInfo_SccDefectFlg(data) do \
{ \
    *data = Rbc_CtrlSccCtrlInfo.SccDefectFlg; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlSccCtrlInfo_SccTarP(data) do \
{ \
    *data = Rbc_CtrlSccCtrlInfo.SccTarP; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlTcsCtrlInfo_TcsActFlg(data) do \
{ \
    *data = Rbc_CtrlTcsCtrlInfo.TcsActFlg; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlTcsCtrlInfo_TcsDefectFlg(data) do \
{ \
    *data = Rbc_CtrlTcsCtrlInfo.TcsDefectFlg; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlTcsCtrlInfo_TcsTarPFrntLe(data) do \
{ \
    *data = Rbc_CtrlTcsCtrlInfo.TcsTarPFrntLe; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlTcsCtrlInfo_TcsTarPFrntRi(data) do \
{ \
    *data = Rbc_CtrlTcsCtrlInfo.TcsTarPFrntRi; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlTcsCtrlInfo_TcsTarPReLe(data) do \
{ \
    *data = Rbc_CtrlTcsCtrlInfo.TcsTarPReLe; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlTcsCtrlInfo_TcsTarPReRi(data) do \
{ \
    *data = Rbc_CtrlTcsCtrlInfo.TcsTarPReRi; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlTcsCtrlInfo_BothDrvgWhlBrkCtlReqFlg(data) do \
{ \
    *data = Rbc_CtrlTcsCtrlInfo.BothDrvgWhlBrkCtlReqFlg; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlCircPFildInfo_PrimCircPFild_1_100Bar(data) do \
{ \
    *data = Rbc_CtrlCircPFildInfo.PrimCircPFild_1_100Bar; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlCircPFildInfo_SecdCircPFild_1_100Bar(data) do \
{ \
    *data = Rbc_CtrlCircPFildInfo.SecdCircPFild_1_100Bar; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlPedlTrvlFildInfo_PdtFild(data) do \
{ \
    *data = Rbc_CtrlPedlTrvlFildInfo.PdtFild; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlPedlTrvlRateInfo_PedlTrvlRateMilliMtrPerSec(data) do \
{ \
    *data = Rbc_CtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlPistPFildInfo_PistPFild_1_100Bar(data) do \
{ \
    *data = Rbc_CtrlPistPFildInfo.PistPFild_1_100Bar; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlEemFailData_Eem_Fail_WssFL(data) do \
{ \
    *data = Rbc_CtrlEemFailData.Eem_Fail_WssFL; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlEemFailData_Eem_Fail_WssFR(data) do \
{ \
    *data = Rbc_CtrlEemFailData.Eem_Fail_WssFR; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlEemFailData_Eem_Fail_WssRL(data) do \
{ \
    *data = Rbc_CtrlEemFailData.Eem_Fail_WssRL; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlEemFailData_Eem_Fail_WssRR(data) do \
{ \
    *data = Rbc_CtrlEemFailData.Eem_Fail_WssRR; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlEemSuspectData_Eem_Suspect_WssFL(data) do \
{ \
    *data = Rbc_CtrlEemSuspectData.Eem_Suspect_WssFL; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlEemSuspectData_Eem_Suspect_WssFR(data) do \
{ \
    *data = Rbc_CtrlEemSuspectData.Eem_Suspect_WssFR; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlEemSuspectData_Eem_Suspect_WssRL(data) do \
{ \
    *data = Rbc_CtrlEemSuspectData.Eem_Suspect_WssRL; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlEemSuspectData_Eem_Suspect_WssRR(data) do \
{ \
    *data = Rbc_CtrlEemSuspectData.Eem_Suspect_WssRR; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlEcuModeSts(data) do \
{ \
    *data = Rbc_CtrlEcuModeSts; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlIgnOnOffSts(data) do \
{ \
    *data = Rbc_CtrlIgnOnOffSts; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlPCtrlAct(data) do \
{ \
    *data = Rbc_CtrlPCtrlAct; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlFuncInhibitRbcSts(data) do \
{ \
    *data = Rbc_CtrlFuncInhibitRbcSts; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlAy(data) do \
{ \
    *data = Rbc_CtrlAy; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlBlsFild(data) do \
{ \
    *data = Rbc_CtrlBlsFild; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlPedlTrvlFinal(data) do \
{ \
    *data = Rbc_CtrlPedlTrvlFinal; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlTarPFromBaseBrkCtrlr(data) do \
{ \
    *data = Rbc_CtrlTarPFromBaseBrkCtrlr; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlTarPRateFromBaseBrkCtrlr(data) do \
{ \
    *data = Rbc_CtrlTarPRateFromBaseBrkCtrlr; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlTarPFromBrkPedl(data) do \
{ \
    *data = Rbc_CtrlTarPFromBrkPedl; \
}while(0);

#define Rbc_Ctrl_Read_Rbc_CtrlVehSpdFild(data) do \
{ \
    *data = Rbc_CtrlVehSpdFild; \
}while(0);


/* Set Output DE MAcro Function */
#define Rbc_Ctrl_Write_Rbc_CtrlTarRgnBrkTqInfo(data) do \
{ \
    Rbc_CtrlTarRgnBrkTqInfo = *data; \
}while(0);

#define Rbc_Ctrl_Write_Rbc_CtrlRgnBrkCoopWithAbsInfo(data) do \
{ \
    Rbc_CtrlRgnBrkCoopWithAbsInfo = *data; \
}while(0);

#define Rbc_Ctrl_Write_Rbc_CtrlTarRgnBrkTqInfo_TarRgnBrkTq(data) do \
{ \
    Rbc_CtrlTarRgnBrkTqInfo.TarRgnBrkTq = *data; \
}while(0);

#define Rbc_Ctrl_Write_Rbc_CtrlTarRgnBrkTqInfo_VirtStkDep(data) do \
{ \
    Rbc_CtrlTarRgnBrkTqInfo.VirtStkDep = *data; \
}while(0);

#define Rbc_Ctrl_Write_Rbc_CtrlTarRgnBrkTqInfo_VirtStkValid(data) do \
{ \
    Rbc_CtrlTarRgnBrkTqInfo.VirtStkValid = *data; \
}while(0);

#define Rbc_Ctrl_Write_Rbc_CtrlTarRgnBrkTqInfo_EstTotBrkForce(data) do \
{ \
    Rbc_CtrlTarRgnBrkTqInfo.EstTotBrkForce = *data; \
}while(0);

#define Rbc_Ctrl_Write_Rbc_CtrlTarRgnBrkTqInfo_EstHydBrkForce(data) do \
{ \
    Rbc_CtrlTarRgnBrkTqInfo.EstHydBrkForce = *data; \
}while(0);

#define Rbc_Ctrl_Write_Rbc_CtrlTarRgnBrkTqInfo_EhbStat(data) do \
{ \
    Rbc_CtrlTarRgnBrkTqInfo.EhbStat = *data; \
}while(0);

#define Rbc_Ctrl_Write_Rbc_CtrlRgnBrkCoopWithAbsInfo_FrntSlipDetThdSlip(data) do \
{ \
    Rbc_CtrlRgnBrkCoopWithAbsInfo.FrntSlipDetThdSlip = *data; \
}while(0);

#define Rbc_Ctrl_Write_Rbc_CtrlRgnBrkCoopWithAbsInfo_FrntSlipDetThdUDiff(data) do \
{ \
    Rbc_CtrlRgnBrkCoopWithAbsInfo.FrntSlipDetThdUDiff = *data; \
}while(0);

#define Rbc_Ctrl_Write_Rbc_CtrlBrkPRednForBaseBrkCtrlr(data) do \
{ \
    Rbc_CtrlBrkPRednForBaseBrkCtrlr = *data; \
}while(0);

#define Rbc_Ctrl_Write_Rbc_CtrlRgnBrkCtlrBlendgFlg(data) do \
{ \
    Rbc_CtrlRgnBrkCtlrBlendgFlg = *data; \
}while(0);

#define Rbc_Ctrl_Write_Rbc_CtrlRgnBrkCtrlrActStFlg(data) do \
{ \
    Rbc_CtrlRgnBrkCtrlrActStFlg = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* RBC_CTRL_IFA_H_ */
/** @} */
