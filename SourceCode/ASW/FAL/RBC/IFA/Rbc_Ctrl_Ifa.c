/**
 * @defgroup Rbc_Ctrl_Ifa Rbc_Ctrl_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Rbc_Ctrl_Ifa.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Rbc_Ctrl_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define RBC_CTRL_START_SEC_CONST_UNSPECIFIED
#include "Rbc_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define RBC_CTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Rbc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define RBC_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Rbc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define RBC_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Rbc_MemMap.h"
#define RBC_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Rbc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define RBC_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Rbc_MemMap.h"
#define RBC_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Rbc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define RBC_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Rbc_MemMap.h"
#define RBC_CTRL_START_SEC_VAR_32BIT
#include "Rbc_MemMap.h"
/** Variable Section (32BIT)**/


#define RBC_CTRL_STOP_SEC_VAR_32BIT
#include "Rbc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define RBC_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Rbc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define RBC_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Rbc_MemMap.h"
#define RBC_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Rbc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define RBC_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Rbc_MemMap.h"
#define RBC_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Rbc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define RBC_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Rbc_MemMap.h"
#define RBC_CTRL_START_SEC_VAR_32BIT
#include "Rbc_MemMap.h"
/** Variable Section (32BIT)**/


#define RBC_CTRL_STOP_SEC_VAR_32BIT
#include "Rbc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define RBC_CTRL_START_SEC_CODE
#include "Rbc_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define RBC_CTRL_STOP_SEC_CODE
#include "Rbc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
