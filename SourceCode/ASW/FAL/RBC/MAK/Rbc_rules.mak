# \file
#
# \brief Rbc
#
# This file contains the implementation of the SWC
# module Rbc.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Rbc_src

Rbc_src_FILES        += $(Rbc_SRC_PATH)\Rbc_Ctrl.c
Rbc_src_FILES        += $(Rbc_SRC_PATH)\LSRBC_ConvActualRegenTqToPress.c
Rbc_src_FILES        += $(Rbc_SRC_PATH)\RBC_CalcPossibleRegenTq.c
Rbc_src_FILES        += $(Rbc_SRC_PATH)\RBC_DetControlState.c
Rbc_src_FILES        += $(Rbc_IFA_PATH)\Rbc_Ctrl_Ifa.c
Rbc_src_FILES        += $(Rbc_CFG_PATH)\Rbc_Cfg.c
Rbc_src_FILES        += $(Rbc_CAL_PATH)\Rbc_Cal.c

# /* HSH */
ifeq ($(ICE_COMPILE),true)
# Library
Rbc_src_FILES        += $(Rbc_CORE_PATH)\virtual_main.c
Rbc_src_FILES  += $(Rbc_CORE_PATH)\ICE\CMN\LIB\SwcCommonFunction.c
Rbc_src_FILES  += $(Rbc_CORE_PATH)\ICE\CMN\LIB\L_CommonFunction.c
Rbc_src_FILES  += $(Rbc_CORE_PATH)\ICE\CMN\FBL_HDR\FBL_HMC\Fbl_Header.c
Rbc_src_FILES  += $(Rbc_CORE_PATH)\ICE\CMN\FBL_HDR\FBL_HMC\Fbl_Mtab.c
endif

ifeq ($(UNIT_TEST),true)
	Rbc_src_FILES        += $(Rbc_UNITY_PATH)\unity.c
	Rbc_src_FILES        += $(Rbc_UNITY_PATH)\unity_fixture.c	
	Rbc_src_FILES        += $(Rbc_UT_PATH)\main.c
	Rbc_src_FILES        += $(Rbc_UT_PATH)\Rbc_Ctrl\Rbc_Ctrl_UtMain.c
endif