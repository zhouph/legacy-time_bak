/**
 * @defgroup Rbc_Cfg Rbc_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Rbc_Cfg.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Rbc_Cfg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define RBC_START_SEC_CONST_UNSPECIFIED
#include "Rbc_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define RBC_STOP_SEC_CONST_UNSPECIFIED
#include "Rbc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define RBC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Rbc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define RBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Rbc_MemMap.h"
#define RBC_START_SEC_VAR_NOINIT_32BIT
#include "Rbc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define RBC_STOP_SEC_VAR_NOINIT_32BIT
#include "Rbc_MemMap.h"
#define RBC_START_SEC_VAR_UNSPECIFIED
#include "Rbc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define RBC_STOP_SEC_VAR_UNSPECIFIED
#include "Rbc_MemMap.h"
#define RBC_START_SEC_VAR_32BIT
#include "Rbc_MemMap.h"
/** Variable Section (32BIT)**/


#define RBC_STOP_SEC_VAR_32BIT
#include "Rbc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define RBC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Rbc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define RBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Rbc_MemMap.h"
#define RBC_START_SEC_VAR_NOINIT_32BIT
#include "Rbc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define RBC_STOP_SEC_VAR_NOINIT_32BIT
#include "Rbc_MemMap.h"
#define RBC_START_SEC_VAR_UNSPECIFIED
#include "Rbc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define RBC_STOP_SEC_VAR_UNSPECIFIED
#include "Rbc_MemMap.h"
#define RBC_START_SEC_VAR_32BIT
#include "Rbc_MemMap.h"
/** Variable Section (32BIT)**/


#define RBC_STOP_SEC_VAR_32BIT
#include "Rbc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define RBC_START_SEC_CODE
#include "Rbc_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define RBC_STOP_SEC_CODE
#include "Rbc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
