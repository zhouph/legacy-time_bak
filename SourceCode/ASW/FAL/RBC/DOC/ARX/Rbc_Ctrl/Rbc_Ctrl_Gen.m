Rbc_CtrlCanRxRegenInfo = Simulink.Bus;
DeList={
    'HcuRegenEna'
    'HcuRegenBrkTq'
    };
Rbc_CtrlCanRxRegenInfo = CreateBus(Rbc_CtrlCanRxRegenInfo, DeList);
clear DeList;

Rbc_CtrlCanRxAccelPedlInfo = Simulink.Bus;
DeList={
    'AccelPedlVal'
    'AccelPedlValErr'
    };
Rbc_CtrlCanRxAccelPedlInfo = CreateBus(Rbc_CtrlCanRxAccelPedlInfo, DeList);
clear DeList;

Rbc_CtrlCanRxIdbInfo = Simulink.Bus;
DeList={
    'EngMsgFault'
    'GearSelDisp'
    'TcuSwiGs'
    'HcuServiceMod'
    'SubCanBusSigFault'
    'CoolantTemp'
    'CoolantTempErr'
    };
Rbc_CtrlCanRxIdbInfo = CreateBus(Rbc_CtrlCanRxIdbInfo, DeList);
clear DeList;

Rbc_CtrlCanRxEngTempInfo = Simulink.Bus;
DeList={
    'EngTemp'
    'EngTempErr'
    };
Rbc_CtrlCanRxEngTempInfo = CreateBus(Rbc_CtrlCanRxEngTempInfo, DeList);
clear DeList;

Rbc_CtrlAbsCtrlInfo = Simulink.Bus;
DeList={
    'AbsActFlg'
    'AbsDefectFlg'
    'AbsTarPFrntLe'
    'AbsTarPFrntRi'
    'AbsTarPReLe'
    'AbsTarPReRi'
    'FrntWhlSlip'
    'AbsDesTarP'
    'AbsDesTarPReqFlg'
    'AbsCtrlFadeOutFlg'
    };
Rbc_CtrlAbsCtrlInfo = CreateBus(Rbc_CtrlAbsCtrlInfo, DeList);
clear DeList;

Rbc_CtrlAvhCtrlInfo = Simulink.Bus;
DeList={
    'AvhActFlg'
    'AvhDefectFlg'
    'AvhTarP'
    };
Rbc_CtrlAvhCtrlInfo = CreateBus(Rbc_CtrlAvhCtrlInfo, DeList);
clear DeList;

Rbc_CtrlEscCtrlInfo = Simulink.Bus;
DeList={
    'EscActFlg'
    'EscDefectFlg'
    'EscTarPFrntLe'
    'EscTarPFrntRi'
    'EscTarPReLe'
    'EscTarPReRi'
    };
Rbc_CtrlEscCtrlInfo = CreateBus(Rbc_CtrlEscCtrlInfo, DeList);
clear DeList;

Rbc_CtrlSccCtrlInfo = Simulink.Bus;
DeList={
    'SccActFlg'
    'SccDefectFlg'
    'SccTarP'
    };
Rbc_CtrlSccCtrlInfo = CreateBus(Rbc_CtrlSccCtrlInfo, DeList);
clear DeList;

Rbc_CtrlTcsCtrlInfo = Simulink.Bus;
DeList={
    'TcsActFlg'
    'TcsDefectFlg'
    'TcsTarPFrntLe'
    'TcsTarPFrntRi'
    'TcsTarPReLe'
    'TcsTarPReRi'
    'BothDrvgWhlBrkCtlReqFlg'
    };
Rbc_CtrlTcsCtrlInfo = CreateBus(Rbc_CtrlTcsCtrlInfo, DeList);
clear DeList;

Rbc_CtrlCircPFildInfo = Simulink.Bus;
DeList={
    'PrimCircPFild_1_100Bar'
    'SecdCircPFild_1_100Bar'
    };
Rbc_CtrlCircPFildInfo = CreateBus(Rbc_CtrlCircPFildInfo, DeList);
clear DeList;

Rbc_CtrlPedlTrvlFildInfo = Simulink.Bus;
DeList={
    'PdtFild'
    };
Rbc_CtrlPedlTrvlFildInfo = CreateBus(Rbc_CtrlPedlTrvlFildInfo, DeList);
clear DeList;

Rbc_CtrlPedlTrvlRateInfo = Simulink.Bus;
DeList={
    'PedlTrvlRateMilliMtrPerSec'
    };
Rbc_CtrlPedlTrvlRateInfo = CreateBus(Rbc_CtrlPedlTrvlRateInfo, DeList);
clear DeList;

Rbc_CtrlPistPFildInfo = Simulink.Bus;
DeList={
    'PistPFild_1_100Bar'
    };
Rbc_CtrlPistPFildInfo = CreateBus(Rbc_CtrlPistPFildInfo, DeList);
clear DeList;

Rbc_CtrlEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_WssFL'
    'Eem_Fail_WssFR'
    'Eem_Fail_WssRL'
    'Eem_Fail_WssRR'
    };
Rbc_CtrlEemFailData = CreateBus(Rbc_CtrlEemFailData, DeList);
clear DeList;

Rbc_CtrlEemSuspectData = Simulink.Bus;
DeList={
    'Eem_Suspect_WssFL'
    'Eem_Suspect_WssFR'
    'Eem_Suspect_WssRL'
    'Eem_Suspect_WssRR'
    };
Rbc_CtrlEemSuspectData = CreateBus(Rbc_CtrlEemSuspectData, DeList);
clear DeList;

Rbc_CtrlEcuModeSts = Simulink.Bus;
DeList={'Rbc_CtrlEcuModeSts'};
Rbc_CtrlEcuModeSts = CreateBus(Rbc_CtrlEcuModeSts, DeList);
clear DeList;

Rbc_CtrlIgnOnOffSts = Simulink.Bus;
DeList={'Rbc_CtrlIgnOnOffSts'};
Rbc_CtrlIgnOnOffSts = CreateBus(Rbc_CtrlIgnOnOffSts, DeList);
clear DeList;

Rbc_CtrlPCtrlAct = Simulink.Bus;
DeList={'Rbc_CtrlPCtrlAct'};
Rbc_CtrlPCtrlAct = CreateBus(Rbc_CtrlPCtrlAct, DeList);
clear DeList;

Rbc_CtrlFuncInhibitRbcSts = Simulink.Bus;
DeList={'Rbc_CtrlFuncInhibitRbcSts'};
Rbc_CtrlFuncInhibitRbcSts = CreateBus(Rbc_CtrlFuncInhibitRbcSts, DeList);
clear DeList;

Rbc_CtrlAy = Simulink.Bus;
DeList={'Rbc_CtrlAy'};
Rbc_CtrlAy = CreateBus(Rbc_CtrlAy, DeList);
clear DeList;

Rbc_CtrlBlsFild = Simulink.Bus;
DeList={'Rbc_CtrlBlsFild'};
Rbc_CtrlBlsFild = CreateBus(Rbc_CtrlBlsFild, DeList);
clear DeList;

Rbc_CtrlPedlTrvlFinal = Simulink.Bus;
DeList={'Rbc_CtrlPedlTrvlFinal'};
Rbc_CtrlPedlTrvlFinal = CreateBus(Rbc_CtrlPedlTrvlFinal, DeList);
clear DeList;

Rbc_CtrlTarPFromBaseBrkCtrlr = Simulink.Bus;
DeList={'Rbc_CtrlTarPFromBaseBrkCtrlr'};
Rbc_CtrlTarPFromBaseBrkCtrlr = CreateBus(Rbc_CtrlTarPFromBaseBrkCtrlr, DeList);
clear DeList;

Rbc_CtrlTarPRateFromBaseBrkCtrlr = Simulink.Bus;
DeList={'Rbc_CtrlTarPRateFromBaseBrkCtrlr'};
Rbc_CtrlTarPRateFromBaseBrkCtrlr = CreateBus(Rbc_CtrlTarPRateFromBaseBrkCtrlr, DeList);
clear DeList;

Rbc_CtrlTarPFromBrkPedl = Simulink.Bus;
DeList={'Rbc_CtrlTarPFromBrkPedl'};
Rbc_CtrlTarPFromBrkPedl = CreateBus(Rbc_CtrlTarPFromBrkPedl, DeList);
clear DeList;

Rbc_CtrlVehSpdFild = Simulink.Bus;
DeList={'Rbc_CtrlVehSpdFild'};
Rbc_CtrlVehSpdFild = CreateBus(Rbc_CtrlVehSpdFild, DeList);
clear DeList;

Rbc_CtrlTarRgnBrkTqInfo = Simulink.Bus;
DeList={
    'TarRgnBrkTq'
    'VirtStkDep'
    'VirtStkValid'
    'EstTotBrkForce'
    'EstHydBrkForce'
    'EhbStat'
    };
Rbc_CtrlTarRgnBrkTqInfo = CreateBus(Rbc_CtrlTarRgnBrkTqInfo, DeList);
clear DeList;

Rbc_CtrlRgnBrkCoopWithAbsInfo = Simulink.Bus;
DeList={
    'FrntSlipDetThdSlip'
    'FrntSlipDetThdUDiff'
    };
Rbc_CtrlRgnBrkCoopWithAbsInfo = CreateBus(Rbc_CtrlRgnBrkCoopWithAbsInfo, DeList);
clear DeList;

Rbc_CtrlBrkPRednForBaseBrkCtrlr = Simulink.Bus;
DeList={'Rbc_CtrlBrkPRednForBaseBrkCtrlr'};
Rbc_CtrlBrkPRednForBaseBrkCtrlr = CreateBus(Rbc_CtrlBrkPRednForBaseBrkCtrlr, DeList);
clear DeList;

Rbc_CtrlRgnBrkCtlrBlendgFlg = Simulink.Bus;
DeList={'Rbc_CtrlRgnBrkCtlrBlendgFlg'};
Rbc_CtrlRgnBrkCtlrBlendgFlg = CreateBus(Rbc_CtrlRgnBrkCtlrBlendgFlg, DeList);
clear DeList;

Rbc_CtrlRgnBrkCtrlrActStFlg = Simulink.Bus;
DeList={'Rbc_CtrlRgnBrkCtrlrActStFlg'};
Rbc_CtrlRgnBrkCtrlrActStFlg = CreateBus(Rbc_CtrlRgnBrkCtrlrActStFlg, DeList);
clear DeList;

