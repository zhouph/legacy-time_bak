#define S_FUNCTION_NAME      Rbc_Ctrl_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          66
#define WidthOutputPort         11

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Rbc_Ctrl.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Rbc_CtrlCanRxRegenInfo.HcuRegenEna = input[0];
    Rbc_CtrlCanRxRegenInfo.HcuRegenBrkTq = input[1];
    Rbc_CtrlCanRxAccelPedlInfo.AccelPedlVal = input[2];
    Rbc_CtrlCanRxAccelPedlInfo.AccelPedlValErr = input[3];
    Rbc_CtrlCanRxIdbInfo.EngMsgFault = input[4];
    Rbc_CtrlCanRxIdbInfo.GearSelDisp = input[5];
    Rbc_CtrlCanRxIdbInfo.TcuSwiGs = input[6];
    Rbc_CtrlCanRxIdbInfo.HcuServiceMod = input[7];
    Rbc_CtrlCanRxIdbInfo.SubCanBusSigFault = input[8];
    Rbc_CtrlCanRxIdbInfo.CoolantTemp = input[9];
    Rbc_CtrlCanRxIdbInfo.CoolantTempErr = input[10];
    Rbc_CtrlCanRxEngTempInfo.EngTemp = input[11];
    Rbc_CtrlCanRxEngTempInfo.EngTempErr = input[12];
    Rbc_CtrlAbsCtrlInfo.AbsActFlg = input[13];
    Rbc_CtrlAbsCtrlInfo.AbsDefectFlg = input[14];
    Rbc_CtrlAbsCtrlInfo.AbsTarPFrntLe = input[15];
    Rbc_CtrlAbsCtrlInfo.AbsTarPFrntRi = input[16];
    Rbc_CtrlAbsCtrlInfo.AbsTarPReLe = input[17];
    Rbc_CtrlAbsCtrlInfo.AbsTarPReRi = input[18];
    Rbc_CtrlAbsCtrlInfo.FrntWhlSlip = input[19];
    Rbc_CtrlAbsCtrlInfo.AbsDesTarP = input[20];
    Rbc_CtrlAbsCtrlInfo.AbsDesTarPReqFlg = input[21];
    Rbc_CtrlAbsCtrlInfo.AbsCtrlFadeOutFlg = input[22];
    Rbc_CtrlAvhCtrlInfo.AvhActFlg = input[23];
    Rbc_CtrlAvhCtrlInfo.AvhDefectFlg = input[24];
    Rbc_CtrlAvhCtrlInfo.AvhTarP = input[25];
    Rbc_CtrlEscCtrlInfo.EscActFlg = input[26];
    Rbc_CtrlEscCtrlInfo.EscDefectFlg = input[27];
    Rbc_CtrlEscCtrlInfo.EscTarPFrntLe = input[28];
    Rbc_CtrlEscCtrlInfo.EscTarPFrntRi = input[29];
    Rbc_CtrlEscCtrlInfo.EscTarPReLe = input[30];
    Rbc_CtrlEscCtrlInfo.EscTarPReRi = input[31];
    Rbc_CtrlSccCtrlInfo.SccActFlg = input[32];
    Rbc_CtrlSccCtrlInfo.SccDefectFlg = input[33];
    Rbc_CtrlSccCtrlInfo.SccTarP = input[34];
    Rbc_CtrlTcsCtrlInfo.TcsActFlg = input[35];
    Rbc_CtrlTcsCtrlInfo.TcsDefectFlg = input[36];
    Rbc_CtrlTcsCtrlInfo.TcsTarPFrntLe = input[37];
    Rbc_CtrlTcsCtrlInfo.TcsTarPFrntRi = input[38];
    Rbc_CtrlTcsCtrlInfo.TcsTarPReLe = input[39];
    Rbc_CtrlTcsCtrlInfo.TcsTarPReRi = input[40];
    Rbc_CtrlTcsCtrlInfo.BothDrvgWhlBrkCtlReqFlg = input[41];
    Rbc_CtrlCircPFildInfo.PrimCircPFild_1_100Bar = input[42];
    Rbc_CtrlCircPFildInfo.SecdCircPFild_1_100Bar = input[43];
    Rbc_CtrlPedlTrvlFildInfo.PdtFild = input[44];
    Rbc_CtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec = input[45];
    Rbc_CtrlPistPFildInfo.PistPFild_1_100Bar = input[46];
    Rbc_CtrlEemFailData.Eem_Fail_WssFL = input[47];
    Rbc_CtrlEemFailData.Eem_Fail_WssFR = input[48];
    Rbc_CtrlEemFailData.Eem_Fail_WssRL = input[49];
    Rbc_CtrlEemFailData.Eem_Fail_WssRR = input[50];
    Rbc_CtrlEemSuspectData.Eem_Suspect_WssFL = input[51];
    Rbc_CtrlEemSuspectData.Eem_Suspect_WssFR = input[52];
    Rbc_CtrlEemSuspectData.Eem_Suspect_WssRL = input[53];
    Rbc_CtrlEemSuspectData.Eem_Suspect_WssRR = input[54];
    Rbc_CtrlEcuModeSts = input[55];
    Rbc_CtrlIgnOnOffSts = input[56];
    Rbc_CtrlPCtrlAct = input[57];
    Rbc_CtrlFuncInhibitRbcSts = input[58];
    Rbc_CtrlAy = input[59];
    Rbc_CtrlBlsFild = input[60];
    Rbc_CtrlPedlTrvlFinal = input[61];
    Rbc_CtrlTarPFromBaseBrkCtrlr = input[62];
    Rbc_CtrlTarPRateFromBaseBrkCtrlr = input[63];
    Rbc_CtrlTarPFromBrkPedl = input[64];
    Rbc_CtrlVehSpdFild = input[65];

    Rbc_Ctrl();


    output[0] = Rbc_CtrlTarRgnBrkTqInfo.TarRgnBrkTq;
    output[1] = Rbc_CtrlTarRgnBrkTqInfo.VirtStkDep;
    output[2] = Rbc_CtrlTarRgnBrkTqInfo.VirtStkValid;
    output[3] = Rbc_CtrlTarRgnBrkTqInfo.EstTotBrkForce;
    output[4] = Rbc_CtrlTarRgnBrkTqInfo.EstHydBrkForce;
    output[5] = Rbc_CtrlTarRgnBrkTqInfo.EhbStat;
    output[6] = Rbc_CtrlRgnBrkCoopWithAbsInfo.FrntSlipDetThdSlip;
    output[7] = Rbc_CtrlRgnBrkCoopWithAbsInfo.FrntSlipDetThdUDiff;
    output[8] = Rbc_CtrlBrkPRednForBaseBrkCtrlr;
    output[9] = Rbc_CtrlRgnBrkCtlrBlendgFlg;
    output[10] = Rbc_CtrlRgnBrkCtrlrActStFlg;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Rbc_Ctrl_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
