#include "unity.h"
#include "unity_fixture.h"
#include "Rbc_Ctrl.h"
#include "Rbc_Ctrl_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Saluint8 UtInput_Rbc_CtrlCanRxRegenInfo_HcuRegenEna[MAX_STEP] = RBC_CTRLCANRXREGENINFO_HCUREGENENA;
const Saluint16 UtInput_Rbc_CtrlCanRxRegenInfo_HcuRegenBrkTq[MAX_STEP] = RBC_CTRLCANRXREGENINFO_HCUREGENBRKTQ;
const Salsint16 UtInput_Rbc_CtrlCanRxAccelPedlInfo_AccelPedlVal[MAX_STEP] = RBC_CTRLCANRXACCELPEDLINFO_ACCELPEDLVAL;
const Saluint8 UtInput_Rbc_CtrlCanRxAccelPedlInfo_AccelPedlValErr[MAX_STEP] = RBC_CTRLCANRXACCELPEDLINFO_ACCELPEDLVALERR;
const Saluint8 UtInput_Rbc_CtrlCanRxIdbInfo_EngMsgFault[MAX_STEP] = RBC_CTRLCANRXIDBINFO_ENGMSGFAULT;
const Saluint8 UtInput_Rbc_CtrlCanRxIdbInfo_GearSelDisp[MAX_STEP] = RBC_CTRLCANRXIDBINFO_GEARSELDISP;
const Saluint8 UtInput_Rbc_CtrlCanRxIdbInfo_TcuSwiGs[MAX_STEP] = RBC_CTRLCANRXIDBINFO_TCUSWIGS;
const Saluint8 UtInput_Rbc_CtrlCanRxIdbInfo_HcuServiceMod[MAX_STEP] = RBC_CTRLCANRXIDBINFO_HCUSERVICEMOD;
const Saluint8 UtInput_Rbc_CtrlCanRxIdbInfo_SubCanBusSigFault[MAX_STEP] = RBC_CTRLCANRXIDBINFO_SUBCANBUSSIGFAULT;
const Salsint16 UtInput_Rbc_CtrlCanRxIdbInfo_CoolantTemp[MAX_STEP] = RBC_CTRLCANRXIDBINFO_COOLANTTEMP;
const Saluint8 UtInput_Rbc_CtrlCanRxIdbInfo_CoolantTempErr[MAX_STEP] = RBC_CTRLCANRXIDBINFO_COOLANTTEMPERR;
const Salsint16 UtInput_Rbc_CtrlCanRxEngTempInfo_EngTemp[MAX_STEP] = RBC_CTRLCANRXENGTEMPINFO_ENGTEMP;
const Saluint8 UtInput_Rbc_CtrlCanRxEngTempInfo_EngTempErr[MAX_STEP] = RBC_CTRLCANRXENGTEMPINFO_ENGTEMPERR;
const Rtesint32 UtInput_Rbc_CtrlAbsCtrlInfo_AbsActFlg[MAX_STEP] = RBC_CTRLABSCTRLINFO_ABSACTFLG;
const Rtesint32 UtInput_Rbc_CtrlAbsCtrlInfo_AbsDefectFlg[MAX_STEP] = RBC_CTRLABSCTRLINFO_ABSDEFECTFLG;
const Rtesint32 UtInput_Rbc_CtrlAbsCtrlInfo_AbsTarPFrntLe[MAX_STEP] = RBC_CTRLABSCTRLINFO_ABSTARPFRNTLE;
const Rtesint32 UtInput_Rbc_CtrlAbsCtrlInfo_AbsTarPFrntRi[MAX_STEP] = RBC_CTRLABSCTRLINFO_ABSTARPFRNTRI;
const Rtesint32 UtInput_Rbc_CtrlAbsCtrlInfo_AbsTarPReLe[MAX_STEP] = RBC_CTRLABSCTRLINFO_ABSTARPRELE;
const Rtesint32 UtInput_Rbc_CtrlAbsCtrlInfo_AbsTarPReRi[MAX_STEP] = RBC_CTRLABSCTRLINFO_ABSTARPRERI;
const Rtesint32 UtInput_Rbc_CtrlAbsCtrlInfo_FrntWhlSlip[MAX_STEP] = RBC_CTRLABSCTRLINFO_FRNTWHLSLIP;
const Rtesint32 UtInput_Rbc_CtrlAbsCtrlInfo_AbsDesTarP[MAX_STEP] = RBC_CTRLABSCTRLINFO_ABSDESTARP;
const Rtesint32 UtInput_Rbc_CtrlAbsCtrlInfo_AbsDesTarPReqFlg[MAX_STEP] = RBC_CTRLABSCTRLINFO_ABSDESTARPREQFLG;
const Rtesint32 UtInput_Rbc_CtrlAbsCtrlInfo_AbsCtrlFadeOutFlg[MAX_STEP] = RBC_CTRLABSCTRLINFO_ABSCTRLFADEOUTFLG;
const Rtesint32 UtInput_Rbc_CtrlAvhCtrlInfo_AvhActFlg[MAX_STEP] = RBC_CTRLAVHCTRLINFO_AVHACTFLG;
const Rtesint32 UtInput_Rbc_CtrlAvhCtrlInfo_AvhDefectFlg[MAX_STEP] = RBC_CTRLAVHCTRLINFO_AVHDEFECTFLG;
const Rtesint32 UtInput_Rbc_CtrlAvhCtrlInfo_AvhTarP[MAX_STEP] = RBC_CTRLAVHCTRLINFO_AVHTARP;
const Rtesint32 UtInput_Rbc_CtrlEscCtrlInfo_EscActFlg[MAX_STEP] = RBC_CTRLESCCTRLINFO_ESCACTFLG;
const Rtesint32 UtInput_Rbc_CtrlEscCtrlInfo_EscDefectFlg[MAX_STEP] = RBC_CTRLESCCTRLINFO_ESCDEFECTFLG;
const Rtesint32 UtInput_Rbc_CtrlEscCtrlInfo_EscTarPFrntLe[MAX_STEP] = RBC_CTRLESCCTRLINFO_ESCTARPFRNTLE;
const Rtesint32 UtInput_Rbc_CtrlEscCtrlInfo_EscTarPFrntRi[MAX_STEP] = RBC_CTRLESCCTRLINFO_ESCTARPFRNTRI;
const Rtesint32 UtInput_Rbc_CtrlEscCtrlInfo_EscTarPReLe[MAX_STEP] = RBC_CTRLESCCTRLINFO_ESCTARPRELE;
const Rtesint32 UtInput_Rbc_CtrlEscCtrlInfo_EscTarPReRi[MAX_STEP] = RBC_CTRLESCCTRLINFO_ESCTARPRERI;
const Rtesint32 UtInput_Rbc_CtrlSccCtrlInfo_SccActFlg[MAX_STEP] = RBC_CTRLSCCCTRLINFO_SCCACTFLG;
const Rtesint32 UtInput_Rbc_CtrlSccCtrlInfo_SccDefectFlg[MAX_STEP] = RBC_CTRLSCCCTRLINFO_SCCDEFECTFLG;
const Rtesint32 UtInput_Rbc_CtrlSccCtrlInfo_SccTarP[MAX_STEP] = RBC_CTRLSCCCTRLINFO_SCCTARP;
const Rtesint32 UtInput_Rbc_CtrlTcsCtrlInfo_TcsActFlg[MAX_STEP] = RBC_CTRLTCSCTRLINFO_TCSACTFLG;
const Rtesint32 UtInput_Rbc_CtrlTcsCtrlInfo_TcsDefectFlg[MAX_STEP] = RBC_CTRLTCSCTRLINFO_TCSDEFECTFLG;
const Rtesint32 UtInput_Rbc_CtrlTcsCtrlInfo_TcsTarPFrntLe[MAX_STEP] = RBC_CTRLTCSCTRLINFO_TCSTARPFRNTLE;
const Rtesint32 UtInput_Rbc_CtrlTcsCtrlInfo_TcsTarPFrntRi[MAX_STEP] = RBC_CTRLTCSCTRLINFO_TCSTARPFRNTRI;
const Rtesint32 UtInput_Rbc_CtrlTcsCtrlInfo_TcsTarPReLe[MAX_STEP] = RBC_CTRLTCSCTRLINFO_TCSTARPRELE;
const Rtesint32 UtInput_Rbc_CtrlTcsCtrlInfo_TcsTarPReRi[MAX_STEP] = RBC_CTRLTCSCTRLINFO_TCSTARPRERI;
const Rtesint32 UtInput_Rbc_CtrlTcsCtrlInfo_BothDrvgWhlBrkCtlReqFlg[MAX_STEP] = RBC_CTRLTCSCTRLINFO_BOTHDRVGWHLBRKCTLREQFLG;
const Rtesint32 UtInput_Rbc_CtrlCircPFildInfo_PrimCircPFild_1_100Bar[MAX_STEP] = RBC_CTRLCIRCPFILDINFO_PRIMCIRCPFILD_1_100BAR;
const Rtesint32 UtInput_Rbc_CtrlCircPFildInfo_SecdCircPFild_1_100Bar[MAX_STEP] = RBC_CTRLCIRCPFILDINFO_SECDCIRCPFILD_1_100BAR;
const Rtesint32 UtInput_Rbc_CtrlPedlTrvlFildInfo_PdtFild[MAX_STEP] = RBC_CTRLPEDLTRVLFILDINFO_PDTFILD;
const Rtesint32 UtInput_Rbc_CtrlPedlTrvlRateInfo_PedlTrvlRateMilliMtrPerSec[MAX_STEP] = RBC_CTRLPEDLTRVLRATEINFO_PEDLTRVLRATEMILLIMTRPERSEC;
const Rtesint32 UtInput_Rbc_CtrlPistPFildInfo_PistPFild_1_100Bar[MAX_STEP] = RBC_CTRLPISTPFILDINFO_PISTPFILD_1_100BAR;
const Saluint8 UtInput_Rbc_CtrlEemFailData_Eem_Fail_WssFL[MAX_STEP] = RBC_CTRLEEMFAILDATA_EEM_FAIL_WSSFL;
const Saluint8 UtInput_Rbc_CtrlEemFailData_Eem_Fail_WssFR[MAX_STEP] = RBC_CTRLEEMFAILDATA_EEM_FAIL_WSSFR;
const Saluint8 UtInput_Rbc_CtrlEemFailData_Eem_Fail_WssRL[MAX_STEP] = RBC_CTRLEEMFAILDATA_EEM_FAIL_WSSRL;
const Saluint8 UtInput_Rbc_CtrlEemFailData_Eem_Fail_WssRR[MAX_STEP] = RBC_CTRLEEMFAILDATA_EEM_FAIL_WSSRR;
const Saluint8 UtInput_Rbc_CtrlEemSuspectData_Eem_Suspect_WssFL[MAX_STEP] = RBC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_WSSFL;
const Saluint8 UtInput_Rbc_CtrlEemSuspectData_Eem_Suspect_WssFR[MAX_STEP] = RBC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_WSSFR;
const Saluint8 UtInput_Rbc_CtrlEemSuspectData_Eem_Suspect_WssRL[MAX_STEP] = RBC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_WSSRL;
const Saluint8 UtInput_Rbc_CtrlEemSuspectData_Eem_Suspect_WssRR[MAX_STEP] = RBC_CTRLEEMSUSPECTDATA_EEM_SUSPECT_WSSRR;
const Mom_HndlrEcuModeSts_t UtInput_Rbc_CtrlEcuModeSts[MAX_STEP] = RBC_CTRLECUMODESTS;
const Prly_HndlrIgnOnOffSts_t UtInput_Rbc_CtrlIgnOnOffSts[MAX_STEP] = RBC_CTRLIGNONOFFSTS;
const Pct_5msCtrlPCtrlAct_t UtInput_Rbc_CtrlPCtrlAct[MAX_STEP] = RBC_CTRLPCTRLACT;
const Eem_SuspcDetnFuncInhibitRbcSts_t UtInput_Rbc_CtrlFuncInhibitRbcSts[MAX_STEP] = RBC_CTRLFUNCINHIBITRBCSTS;
const Abc_CtrlAy_t UtInput_Rbc_CtrlAy[MAX_STEP] = RBC_CTRLAY;
const Spc_5msCtrlBlsFild_t UtInput_Rbc_CtrlBlsFild[MAX_STEP] = RBC_CTRLBLSFILD;
const Det_5msCtrlPedlTrvlFinal_t UtInput_Rbc_CtrlPedlTrvlFinal[MAX_STEP] = RBC_CTRLPEDLTRVLFINAL;
const Bbc_CtrlTarPFromBaseBrkCtrlr_t UtInput_Rbc_CtrlTarPFromBaseBrkCtrlr[MAX_STEP] = RBC_CTRLTARPFROMBASEBRKCTRLR;
const Bbc_CtrlTarPRateFromBaseBrkCtrlr_t UtInput_Rbc_CtrlTarPRateFromBaseBrkCtrlr[MAX_STEP] = RBC_CTRLTARPRATEFROMBASEBRKCTRLR;
const Bbc_CtrlTarPFromBrkPedl_t UtInput_Rbc_CtrlTarPFromBrkPedl[MAX_STEP] = RBC_CTRLTARPFROMBRKPEDL;
const Det_5msCtrlVehSpdFild_t UtInput_Rbc_CtrlVehSpdFild[MAX_STEP] = RBC_CTRLVEHSPDFILD;

const Rtesint32 UtExpected_Rbc_CtrlTarRgnBrkTqInfo_TarRgnBrkTq[MAX_STEP] = RBC_CTRLTARRGNBRKTQINFO_TARRGNBRKTQ;
const Rtesint32 UtExpected_Rbc_CtrlTarRgnBrkTqInfo_VirtStkDep[MAX_STEP] = RBC_CTRLTARRGNBRKTQINFO_VIRTSTKDEP;
const Rtesint32 UtExpected_Rbc_CtrlTarRgnBrkTqInfo_VirtStkValid[MAX_STEP] = RBC_CTRLTARRGNBRKTQINFO_VIRTSTKVALID;
const Rtesint32 UtExpected_Rbc_CtrlTarRgnBrkTqInfo_EstTotBrkForce[MAX_STEP] = RBC_CTRLTARRGNBRKTQINFO_ESTTOTBRKFORCE;
const Rtesint32 UtExpected_Rbc_CtrlTarRgnBrkTqInfo_EstHydBrkForce[MAX_STEP] = RBC_CTRLTARRGNBRKTQINFO_ESTHYDBRKFORCE;
const Rtesint32 UtExpected_Rbc_CtrlTarRgnBrkTqInfo_EhbStat[MAX_STEP] = RBC_CTRLTARRGNBRKTQINFO_EHBSTAT;
const Rtesint32 UtExpected_Rbc_CtrlRgnBrkCoopWithAbsInfo_FrntSlipDetThdSlip[MAX_STEP] = RBC_CTRLRGNBRKCOOPWITHABSINFO_FRNTSLIPDETTHDSLIP;
const Rtesint32 UtExpected_Rbc_CtrlRgnBrkCoopWithAbsInfo_FrntSlipDetThdUDiff[MAX_STEP] = RBC_CTRLRGNBRKCOOPWITHABSINFO_FRNTSLIPDETTHDUDIFF;
const Rbc_CtrlBrkPRednForBaseBrkCtrlr_t UtExpected_Rbc_CtrlBrkPRednForBaseBrkCtrlr[MAX_STEP] = RBC_CTRLBRKPREDNFORBASEBRKCTRLR;
const Rbc_CtrlRgnBrkCtlrBlendgFlg_t UtExpected_Rbc_CtrlRgnBrkCtlrBlendgFlg[MAX_STEP] = RBC_CTRLRGNBRKCTLRBLENDGFLG;
const Rbc_CtrlRgnBrkCtrlrActStFlg_t UtExpected_Rbc_CtrlRgnBrkCtrlrActStFlg[MAX_STEP] = RBC_CTRLRGNBRKCTRLRACTSTFLG;



TEST_GROUP(Rbc_Ctrl);
TEST_SETUP(Rbc_Ctrl)
{
    Rbc_Ctrl_Init();
}

TEST_TEAR_DOWN(Rbc_Ctrl)
{   /* Postcondition */

}

TEST(Rbc_Ctrl, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Rbc_CtrlCanRxRegenInfo.HcuRegenEna = UtInput_Rbc_CtrlCanRxRegenInfo_HcuRegenEna[i];
        Rbc_CtrlCanRxRegenInfo.HcuRegenBrkTq = UtInput_Rbc_CtrlCanRxRegenInfo_HcuRegenBrkTq[i];
        Rbc_CtrlCanRxAccelPedlInfo.AccelPedlVal = UtInput_Rbc_CtrlCanRxAccelPedlInfo_AccelPedlVal[i];
        Rbc_CtrlCanRxAccelPedlInfo.AccelPedlValErr = UtInput_Rbc_CtrlCanRxAccelPedlInfo_AccelPedlValErr[i];
        Rbc_CtrlCanRxIdbInfo.EngMsgFault = UtInput_Rbc_CtrlCanRxIdbInfo_EngMsgFault[i];
        Rbc_CtrlCanRxIdbInfo.GearSelDisp = UtInput_Rbc_CtrlCanRxIdbInfo_GearSelDisp[i];
        Rbc_CtrlCanRxIdbInfo.TcuSwiGs = UtInput_Rbc_CtrlCanRxIdbInfo_TcuSwiGs[i];
        Rbc_CtrlCanRxIdbInfo.HcuServiceMod = UtInput_Rbc_CtrlCanRxIdbInfo_HcuServiceMod[i];
        Rbc_CtrlCanRxIdbInfo.SubCanBusSigFault = UtInput_Rbc_CtrlCanRxIdbInfo_SubCanBusSigFault[i];
        Rbc_CtrlCanRxIdbInfo.CoolantTemp = UtInput_Rbc_CtrlCanRxIdbInfo_CoolantTemp[i];
        Rbc_CtrlCanRxIdbInfo.CoolantTempErr = UtInput_Rbc_CtrlCanRxIdbInfo_CoolantTempErr[i];
        Rbc_CtrlCanRxEngTempInfo.EngTemp = UtInput_Rbc_CtrlCanRxEngTempInfo_EngTemp[i];
        Rbc_CtrlCanRxEngTempInfo.EngTempErr = UtInput_Rbc_CtrlCanRxEngTempInfo_EngTempErr[i];
        Rbc_CtrlAbsCtrlInfo.AbsActFlg = UtInput_Rbc_CtrlAbsCtrlInfo_AbsActFlg[i];
        Rbc_CtrlAbsCtrlInfo.AbsDefectFlg = UtInput_Rbc_CtrlAbsCtrlInfo_AbsDefectFlg[i];
        Rbc_CtrlAbsCtrlInfo.AbsTarPFrntLe = UtInput_Rbc_CtrlAbsCtrlInfo_AbsTarPFrntLe[i];
        Rbc_CtrlAbsCtrlInfo.AbsTarPFrntRi = UtInput_Rbc_CtrlAbsCtrlInfo_AbsTarPFrntRi[i];
        Rbc_CtrlAbsCtrlInfo.AbsTarPReLe = UtInput_Rbc_CtrlAbsCtrlInfo_AbsTarPReLe[i];
        Rbc_CtrlAbsCtrlInfo.AbsTarPReRi = UtInput_Rbc_CtrlAbsCtrlInfo_AbsTarPReRi[i];
        Rbc_CtrlAbsCtrlInfo.FrntWhlSlip = UtInput_Rbc_CtrlAbsCtrlInfo_FrntWhlSlip[i];
        Rbc_CtrlAbsCtrlInfo.AbsDesTarP = UtInput_Rbc_CtrlAbsCtrlInfo_AbsDesTarP[i];
        Rbc_CtrlAbsCtrlInfo.AbsDesTarPReqFlg = UtInput_Rbc_CtrlAbsCtrlInfo_AbsDesTarPReqFlg[i];
        Rbc_CtrlAbsCtrlInfo.AbsCtrlFadeOutFlg = UtInput_Rbc_CtrlAbsCtrlInfo_AbsCtrlFadeOutFlg[i];
        Rbc_CtrlAvhCtrlInfo.AvhActFlg = UtInput_Rbc_CtrlAvhCtrlInfo_AvhActFlg[i];
        Rbc_CtrlAvhCtrlInfo.AvhDefectFlg = UtInput_Rbc_CtrlAvhCtrlInfo_AvhDefectFlg[i];
        Rbc_CtrlAvhCtrlInfo.AvhTarP = UtInput_Rbc_CtrlAvhCtrlInfo_AvhTarP[i];
        Rbc_CtrlEscCtrlInfo.EscActFlg = UtInput_Rbc_CtrlEscCtrlInfo_EscActFlg[i];
        Rbc_CtrlEscCtrlInfo.EscDefectFlg = UtInput_Rbc_CtrlEscCtrlInfo_EscDefectFlg[i];
        Rbc_CtrlEscCtrlInfo.EscTarPFrntLe = UtInput_Rbc_CtrlEscCtrlInfo_EscTarPFrntLe[i];
        Rbc_CtrlEscCtrlInfo.EscTarPFrntRi = UtInput_Rbc_CtrlEscCtrlInfo_EscTarPFrntRi[i];
        Rbc_CtrlEscCtrlInfo.EscTarPReLe = UtInput_Rbc_CtrlEscCtrlInfo_EscTarPReLe[i];
        Rbc_CtrlEscCtrlInfo.EscTarPReRi = UtInput_Rbc_CtrlEscCtrlInfo_EscTarPReRi[i];
        Rbc_CtrlSccCtrlInfo.SccActFlg = UtInput_Rbc_CtrlSccCtrlInfo_SccActFlg[i];
        Rbc_CtrlSccCtrlInfo.SccDefectFlg = UtInput_Rbc_CtrlSccCtrlInfo_SccDefectFlg[i];
        Rbc_CtrlSccCtrlInfo.SccTarP = UtInput_Rbc_CtrlSccCtrlInfo_SccTarP[i];
        Rbc_CtrlTcsCtrlInfo.TcsActFlg = UtInput_Rbc_CtrlTcsCtrlInfo_TcsActFlg[i];
        Rbc_CtrlTcsCtrlInfo.TcsDefectFlg = UtInput_Rbc_CtrlTcsCtrlInfo_TcsDefectFlg[i];
        Rbc_CtrlTcsCtrlInfo.TcsTarPFrntLe = UtInput_Rbc_CtrlTcsCtrlInfo_TcsTarPFrntLe[i];
        Rbc_CtrlTcsCtrlInfo.TcsTarPFrntRi = UtInput_Rbc_CtrlTcsCtrlInfo_TcsTarPFrntRi[i];
        Rbc_CtrlTcsCtrlInfo.TcsTarPReLe = UtInput_Rbc_CtrlTcsCtrlInfo_TcsTarPReLe[i];
        Rbc_CtrlTcsCtrlInfo.TcsTarPReRi = UtInput_Rbc_CtrlTcsCtrlInfo_TcsTarPReRi[i];
        Rbc_CtrlTcsCtrlInfo.BothDrvgWhlBrkCtlReqFlg = UtInput_Rbc_CtrlTcsCtrlInfo_BothDrvgWhlBrkCtlReqFlg[i];
        Rbc_CtrlCircPFildInfo.PrimCircPFild_1_100Bar = UtInput_Rbc_CtrlCircPFildInfo_PrimCircPFild_1_100Bar[i];
        Rbc_CtrlCircPFildInfo.SecdCircPFild_1_100Bar = UtInput_Rbc_CtrlCircPFildInfo_SecdCircPFild_1_100Bar[i];
        Rbc_CtrlPedlTrvlFildInfo.PdtFild = UtInput_Rbc_CtrlPedlTrvlFildInfo_PdtFild[i];
        Rbc_CtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec = UtInput_Rbc_CtrlPedlTrvlRateInfo_PedlTrvlRateMilliMtrPerSec[i];
        Rbc_CtrlPistPFildInfo.PistPFild_1_100Bar = UtInput_Rbc_CtrlPistPFildInfo_PistPFild_1_100Bar[i];
        Rbc_CtrlEemFailData.Eem_Fail_WssFL = UtInput_Rbc_CtrlEemFailData_Eem_Fail_WssFL[i];
        Rbc_CtrlEemFailData.Eem_Fail_WssFR = UtInput_Rbc_CtrlEemFailData_Eem_Fail_WssFR[i];
        Rbc_CtrlEemFailData.Eem_Fail_WssRL = UtInput_Rbc_CtrlEemFailData_Eem_Fail_WssRL[i];
        Rbc_CtrlEemFailData.Eem_Fail_WssRR = UtInput_Rbc_CtrlEemFailData_Eem_Fail_WssRR[i];
        Rbc_CtrlEemSuspectData.Eem_Suspect_WssFL = UtInput_Rbc_CtrlEemSuspectData_Eem_Suspect_WssFL[i];
        Rbc_CtrlEemSuspectData.Eem_Suspect_WssFR = UtInput_Rbc_CtrlEemSuspectData_Eem_Suspect_WssFR[i];
        Rbc_CtrlEemSuspectData.Eem_Suspect_WssRL = UtInput_Rbc_CtrlEemSuspectData_Eem_Suspect_WssRL[i];
        Rbc_CtrlEemSuspectData.Eem_Suspect_WssRR = UtInput_Rbc_CtrlEemSuspectData_Eem_Suspect_WssRR[i];
        Rbc_CtrlEcuModeSts = UtInput_Rbc_CtrlEcuModeSts[i];
        Rbc_CtrlIgnOnOffSts = UtInput_Rbc_CtrlIgnOnOffSts[i];
        Rbc_CtrlPCtrlAct = UtInput_Rbc_CtrlPCtrlAct[i];
        Rbc_CtrlFuncInhibitRbcSts = UtInput_Rbc_CtrlFuncInhibitRbcSts[i];
        Rbc_CtrlAy = UtInput_Rbc_CtrlAy[i];
        Rbc_CtrlBlsFild = UtInput_Rbc_CtrlBlsFild[i];
        Rbc_CtrlPedlTrvlFinal = UtInput_Rbc_CtrlPedlTrvlFinal[i];
        Rbc_CtrlTarPFromBaseBrkCtrlr = UtInput_Rbc_CtrlTarPFromBaseBrkCtrlr[i];
        Rbc_CtrlTarPRateFromBaseBrkCtrlr = UtInput_Rbc_CtrlTarPRateFromBaseBrkCtrlr[i];
        Rbc_CtrlTarPFromBrkPedl = UtInput_Rbc_CtrlTarPFromBrkPedl[i];
        Rbc_CtrlVehSpdFild = UtInput_Rbc_CtrlVehSpdFild[i];

        Rbc_Ctrl();

        TEST_ASSERT_EQUAL(Rbc_CtrlTarRgnBrkTqInfo.TarRgnBrkTq, UtExpected_Rbc_CtrlTarRgnBrkTqInfo_TarRgnBrkTq[i]);
        TEST_ASSERT_EQUAL(Rbc_CtrlTarRgnBrkTqInfo.VirtStkDep, UtExpected_Rbc_CtrlTarRgnBrkTqInfo_VirtStkDep[i]);
        TEST_ASSERT_EQUAL(Rbc_CtrlTarRgnBrkTqInfo.VirtStkValid, UtExpected_Rbc_CtrlTarRgnBrkTqInfo_VirtStkValid[i]);
        TEST_ASSERT_EQUAL(Rbc_CtrlTarRgnBrkTqInfo.EstTotBrkForce, UtExpected_Rbc_CtrlTarRgnBrkTqInfo_EstTotBrkForce[i]);
        TEST_ASSERT_EQUAL(Rbc_CtrlTarRgnBrkTqInfo.EstHydBrkForce, UtExpected_Rbc_CtrlTarRgnBrkTqInfo_EstHydBrkForce[i]);
        TEST_ASSERT_EQUAL(Rbc_CtrlTarRgnBrkTqInfo.EhbStat, UtExpected_Rbc_CtrlTarRgnBrkTqInfo_EhbStat[i]);
        TEST_ASSERT_EQUAL(Rbc_CtrlRgnBrkCoopWithAbsInfo.FrntSlipDetThdSlip, UtExpected_Rbc_CtrlRgnBrkCoopWithAbsInfo_FrntSlipDetThdSlip[i]);
        TEST_ASSERT_EQUAL(Rbc_CtrlRgnBrkCoopWithAbsInfo.FrntSlipDetThdUDiff, UtExpected_Rbc_CtrlRgnBrkCoopWithAbsInfo_FrntSlipDetThdUDiff[i]);
        TEST_ASSERT_EQUAL(Rbc_CtrlBrkPRednForBaseBrkCtrlr, UtExpected_Rbc_CtrlBrkPRednForBaseBrkCtrlr[i]);
        TEST_ASSERT_EQUAL(Rbc_CtrlRgnBrkCtlrBlendgFlg, UtExpected_Rbc_CtrlRgnBrkCtlrBlendgFlg[i]);
        TEST_ASSERT_EQUAL(Rbc_CtrlRgnBrkCtrlrActStFlg, UtExpected_Rbc_CtrlRgnBrkCtrlrActStFlg[i]);
    }
}

TEST_GROUP_RUNNER(Rbc_Ctrl)
{
    RUN_TEST_CASE(Rbc_Ctrl, All);
}
