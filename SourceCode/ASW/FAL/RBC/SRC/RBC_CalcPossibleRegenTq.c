/**
 * @defgroup RBC_CalcPossibleRegenTq RBC_CalcPossibleRegenTq
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        RBC_CalcPossibleRegenTq.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "RBC_CalcPossibleRegenTq.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/


//#define U8_IGN_OFF 		0
//#define U8_IGN_ON 		1
#define U8_RBC_STBL_TIME_AF_IGN_ON  U8_T_500_MS

#define S8_RBC_READY    0
#define S8_RBC_CONTROL  1
#define S8_RBC_FADE_OUT 5

#define __CORNER_BRAKE_RBC_INHIBIT                    ENABLE

//#define	S16_BLEND_SPD_CHANGE_PRESS_THRES		S16_MPRESS_30_BAR
#define	S16_BLEND_SPD_CHANGE_PRESS_THRES		S16_PCTRL_PRESS_30_BAR
#define U16_RB_REDUCTION_START_SPD_HIGH			S16_SPEED_20_KPH

//#define U8_SCC_TAR_WL_PRESS_MIN_1               S16_MPRESS_2_BAR
//#define U8_SCC_MAX_RB_TQ_PRESS_LIMIT            S16_MPRESS_15_BAR
#define U8_SCC_TAR_WL_PRESS_MIN_1               S16_PCTRL_PRESS_2_BAR
#define U8_SCC_MAX_RB_TQ_PRESS_LIMIT            S16_PCTRL_PRESS_15_BAR
#define U16_SCC_RB_ENABLE_SPEED_LIMIT           S16_SPEED_30_KPH
#define U16_SCC_RB_REDUCTION_START_SPEED        S16_SPEED_40_KPH
#define U16_SCC_RB_ENTER_SPEED_LIMIT            S16_SPEED_40_KPH

#define U16_RB_TORQUE_NM_MAX       3000

#define S16_RBC_FADE_OUT_RATE_DEFAULT   S16_MPRESS_1_BAR

/* 외부 변수를 define해서 적용하는 부분*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
    int32_t liu8wIgnStat;
    int32_t liu1fSubCanBusOffError;
    int32_t liu1fEngineMsgFaultDet;
    int32_t lcahbu1AHBOn;
    int16_t lsahbs16MCpresFilt;
    int32_t liu1fFLSpeedInvalid;
    int32_t liu1fFRSpeedInvalid;
    int32_t liu1fRLSpeedInvalid;
    int32_t liu1fRRSpeedInvalid;
    int32_t lru1TargetSystemOK;


    int16_t ldrbcs16VehicleSpeed;
    int32_t ldrbcu1DriverIntendToBrake;
    int32_t ldrbcu1DriverIntendToAccel;
    int32_t ldrbcu1RegenEnableGearSelected;
    int32_t ldrbcu1MotorRpmOKInDGearFlg;
    int32_t ldrbcu1InhibitRegenReEntryFlg;
    int32_t ldrbcu1SafetyFuctionActFlg;
    int32_t ldrbcu1FrontWheelSlipDec;
    int32_t ldrbcu1FastTurnFlg;
    int32_t ldrbcu1FastDecelApplyFlg;
    int32_t lcrbcu1RbcSccBlendFlag;
    int32_t ldrbcu1RbcSccReadyActFlgOld;
    int32_t ldrbcu1SccFuctionActFlg;
    int32_t ldrbcu1SccFuctionActOldFlg;
    int32_t ldrbcu1RbcSccReadyActFlg;
    uint16_t ldrbcu16PressToTorqRatioTotal;
    int16_t ldrbcs16DrvIntendedBrkP;
}GetPossibleRgnTq_t;
/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define RBC_CTRL_START_SEC_CONST_UNSPECIFIED
#include "Rbc_MemMap.h"
/** Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
#define RBC_CTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Rbc_MemMap.h"

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define RBC_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Rbc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
#define RBC_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Rbc_MemMap.h"



#define RBC_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Rbc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define RBC_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Rbc_MemMap.h"



#define RBC_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Rbc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define RBC_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Rbc_MemMap.h"



#define RBC_CTRL_START_SEC_VAR_32BIT
#include "Rbc_MemMap.h"
/** Variable Section (32BIT)**/
#define RBC_CTRL_STOP_SEC_VAR_32BIT
#include "Rbc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define RBC_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Rbc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
FAL_STATIC int16_t  lcrbcs16MaxPosRegenTqBar;
FAL_STATIC uint16_t lcrbcu16MaxPosRegenTqNm;

FAL_STATIC int8_t lcrbcs8ControlState;
FAL_STATIC int8_t lcrbcs8FadeOutRate;

FAL_STATIC int16_t  lcrbcs16TargetRBTqBar;

FAL_STATIC int16_t lcrbcs16RegenLowSpeedLimit;
FAL_STATIC int16_t lcrbcs16Regen2HydStartSpeed;
FAL_STATIC  int16_t lcrbcs16BlendSpdAtFastDecel;

FAL_STATIC int16_t lcrbcs16GeneratedMCPress;
FAL_STATIC uint16_t lcrbcu16GeneratedMCTqNm ;

FAL_STATIC uint16_t lcrbcu1ActiveFlg ;
FAL_STATIC uint16_t lcrbcu16TargetRBTqNm ;

FAL_STATIC GetPossibleRgnTq_t   GetPossibleRgnTq;


FAL_STATIC int32_t lcrbcu1CANSignalInvalid;
FAL_STATIC int32_t lcrbcu1InhibitFlg;
FAL_STATIC int32_t lcrbcu1SpeedInvalidFlg;
FAL_STATIC int32_t lcrbcu1InhibitByFM;
FAL_STATIC int32_t lcrbcu1InhibitBySetting;
FAL_STATIC int32_t lcrbcu1RbcBledSpeedUpdateFlag;
FAL_STATIC int32_t lcrbcu1BlendSpdChangeFlg;
FAL_STATIC int32_t lcrbcu1BlendSpdChangeOldFlg;
FAL_STATIC int32_t lcrbcu1RbcSccSpeedUpdateFlag;
FAL_STATIC int32_t lcrbcu1LowSpeedInhibitFlg;
FAL_STATIC int32_t lcrbcu1MaxSpeedInhibitFlg;
FAL_STATIC int32_t lcrbcu1ExitCmdRengenTorq;
FAL_STATIC int32_t lcrbcu1ActivatedBrkCycle;
FAL_STATIC int32_t lcrbcu1DelayRBCReEnter;
FAL_STATIC int32_t lcrbcu1CtrlStartOK;
FAL_STATIC int32_t lcrbcu1CtrlEndOK;
FAL_STATIC int32_t lcrbcu1CtrlSlowEndOK;
FAL_STATIC int32_t lcrbcu1CtrlSlowEndOKOld;
FAL_STATIC int32_t lcrbcu1FadeOutCtrlFlag;
FAL_STATIC int32_t lcrbcu1FadeOutCtrlEnd;
FAL_STATIC int32_t lcrbcu1FadeOutCtrlStart;
FAL_STATIC int32_t lcrbcu1SignalValidAftIgnOn;
#define RBC_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Rbc_MemMap.h"



#define RBC_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Rbc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define RBC_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Rbc_MemMap.h"



#define RBC_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Rbc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define RBC_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Rbc_MemMap.h"



#define RBC_CTRL_START_SEC_VAR_32BIT
#include "Rbc_MemMap.h"
/** Variable Section (32BIT)**/
#define RBC_CTRL_STOP_SEC_VAR_32BIT
#include "Rbc_MemMap.h"








/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define RBC_CTRL_START_SEC_CODE
#include "Rbc_MemMap.h"

static void LCRBC_vDecideCtrlInhibit(void);
static void LCRBC_vCalMainCtrlParameters(void);
static void LCRBC_vDecideSpeedLimit(void);
static void LCRBC_vCalMaxPossibleRegenTq(void);
static void LCRBC_vDctHydBrakePressStatus(void);

static void LCRBC_vDecideCtrlState(void);
static void LCRBC_vGenRegenBrkTorqCmd(void);

static void LCRBC_vDecideRBCEnter(void);
static void LCRBC_vDecideRBCExit(void);
static void LCRBC_vFadeOutCtrl(void);
static void LCRBC_vResetCtrlVariables(void);
static void LCRBC_vDctActivationHistory(void);

static void LCRBC_vDecideBlendingSpd(void);
static void LCRBC_vOutPutControl(void);
static void LCRBC_vInPutControl(void);
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*******************************************************************************
* FUNCTION NAME:		LCRBC_vCallControl
* CALLED BY:			LC_vCallMainControlAHB()
* Preconditions:		none
* PARAMETER:			none
* RETURN VALUE:			none
* Description:			Main Code of RBC Control Module
********************************************************************************/

void LCRBC_vCallControl(void)
{
    LCRBC_vInPutControl();
    
    LCRBC_vDecideCtrlInhibit();
    
    LCRBC_vCalMainCtrlParameters();
    
    LCRBC_vDecideCtrlState();
    
    LCRBC_vGenRegenBrkTorqCmd();

    LCRBC_vOutPutControl();

}



/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

static void LCRBC_vInPutControl(void)
{
    GetPossibleRgnTq.liu8wIgnStat                               = Rbc_CtrlBus.Rbc_CtrlIgnOnOffSts;
    GetPossibleRgnTq.liu1fSubCanBusOffError                     = Rbc_CtrlBus.Rbc_CtrlCanRxIdbInfo.SubCanBusSigFault;
    GetPossibleRgnTq.liu1fEngineMsgFaultDet                     = Rbc_CtrlBus.Rbc_CtrlCanRxIdbInfo.EngMsgFault;
    GetPossibleRgnTq.lcahbu1AHBOn                               = Rbc_CtrlBus.Rbc_CtrlPCtrlAct;
#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
    GetPossibleRgnTq.lsahbs16MCpresFilt                         = (int16_t)Rbc_CtrlBus.Rbc_CtrlPistPFildInfo.PistPFild_1_100Bar/10;
#elif __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
    GetPossibleRgnTq.lsahbs16MCpresFilt                         = (int16_t)Rbc_CtrlBus.Rbc_CtrlCircPFildInfo.PrimCircPFild_1_100Bar/10;
#else
#endif
    GetPossibleRgnTq.liu1fFLSpeedInvalid                        = Rbc_CtrlBus.Rbc_CtrlEemFailData.Eem_Fail_WssFL;
    GetPossibleRgnTq.liu1fFRSpeedInvalid                        = Rbc_CtrlBus.Rbc_CtrlEemFailData.Eem_Fail_WssFR;
    GetPossibleRgnTq.liu1fRLSpeedInvalid                        = Rbc_CtrlBus.Rbc_CtrlEemFailData.Eem_Fail_WssRL;
    GetPossibleRgnTq.liu1fRRSpeedInvalid                        = Rbc_CtrlBus.Rbc_CtrlEemFailData.Eem_Fail_WssRR;
    GetPossibleRgnTq.lru1TargetSystemOK                         = Rbc_CtrlBus.Rbc_CtrlCanRxRegenInfo.HcuRegenEna;



    GetPossibleRgnTq.ldrbcs16VehicleSpeed                       = RBCsrcbus.VehicleSpeed; // ok
    GetPossibleRgnTq.ldrbcu1DriverIntendToBrake                 = RBCsrcbus.DriverIntendToBrake;
    GetPossibleRgnTq.ldrbcu1DriverIntendToAccel                 = RBCsrcbus.DriverIntendToAccel;
    GetPossibleRgnTq.ldrbcu1RegenEnableGearSelected             = RBCsrcbus.RegenEnableGearSelected;
    GetPossibleRgnTq.ldrbcu1MotorRpmOKInDGearFlg                = RBCsrcbus.MotorRpmOKInDGearFlg;
    GetPossibleRgnTq.ldrbcu1InhibitRegenReEntryFlg              = RBCsrcbus.InhibitRegenReEntryFlg;
    GetPossibleRgnTq.ldrbcu1SafetyFuctionActFlg                 = RBCsrcbus.SafetyFuctionActFlg;
    GetPossibleRgnTq.ldrbcu1FrontWheelSlipDec                   = RBCsrcbus.FrontWheelSlipDec;
    GetPossibleRgnTq.ldrbcu1FastTurnFlg                         = RBCsrcbus.FastTurnFlg;
    GetPossibleRgnTq.ldrbcu1FastDecelApplyFlg                   = RBCsrcbus.FastDecelApplyFlg;

    GetPossibleRgnTq.lcrbcu1RbcSccBlendFlag                     = RBCsrcbus.RbcSccReadyActFlg; //check!!
    GetPossibleRgnTq.ldrbcu1RbcSccReadyActFlgOld                = RBCsrcbus.RbcSccReadyActFlgOld;
    GetPossibleRgnTq.ldrbcu1SccFuctionActFlg                    = RBCsrcbus.SccFuctionActFlg;
    GetPossibleRgnTq.ldrbcu1SccFuctionActOldFlg                 = RBCsrcbus.SccFuctionActOldFlg;
    GetPossibleRgnTq.ldrbcu1RbcSccReadyActFlg                   = RBCsrcbus.RbcSccReadyActFlg;
    GetPossibleRgnTq.ldrbcu16PressToTorqRatioTotal              = RBCsrcbus.PressToTorqRatioTotal;
    GetPossibleRgnTq.ldrbcs16DrvIntendedBrkP                    = RBCsrcbus.DrvIntendedBrkP;
}
/*******************************************************************************
* FUNCTION NAME:		LCRBC_vDecideCtrlInhibit
* CALLED BY:			LCRBC_vCallControl()
* Preconditions:		none
* PARAMETER:			none
* RETURN VALUE:			none
* Description:			Decide RBC Inhibition conditions
********************************************************************************/

static void LCRBC_vDecideCtrlInhibit(void)
{
    static int8_t lcrbcu8StabilizeTimeAftIgnOn = 0;
    /* Check CAN signal stabilized */
    
    if(GetPossibleRgnTq.liu8wIgnStat==U8_IGN_OFF) /* 1: IGN Off */
    {
        lcrbcu1SignalValidAftIgnOn = 0;
        lcrbcu8StabilizeTimeAftIgnOn = 0;
    }
    else
    {
        if(lcrbcu1SignalValidAftIgnOn==0)
        {
            lcrbcu8StabilizeTimeAftIgnOn = lcrbcu8StabilizeTimeAftIgnOn + 1;
            
            if(lcrbcu8StabilizeTimeAftIgnOn >= U8_RBC_STBL_TIME_AF_IGN_ON)
            {
                lcrbcu1SignalValidAftIgnOn = 1;
            }
        }
        else
        {
            ;
        }
    }


  /* Fail Management */
  
   	  #if (__HMC_CAN_VER==CAN_LF_HEV)  
    if((liu1fEngineMsgFaultDet==1) || (lcrbcu1SignalValidAftIgnOn==0) || (GetPossibleRgnTq.liu1fSubCanBusOffError==1))
	  #else	
    if((GetPossibleRgnTq.liu1fEngineMsgFaultDet==1) || (lcrbcu1SignalValidAftIgnOn==0))
	  #endif
	{
		lcrbcu1CANSignalInvalid = 1;
	}
	else
	{
		lcrbcu1CANSignalInvalid = 0;
	}
	
	/*lcahbu1SpeedInvalidFlg*/
	if((GetPossibleRgnTq.liu1fFLSpeedInvalid==1)||(GetPossibleRgnTq.liu1fFRSpeedInvalid==1)||(GetPossibleRgnTq.liu1fRLSpeedInvalid==1)||(GetPossibleRgnTq.liu1fRRSpeedInvalid==1))
	{
	    lcrbcu1SpeedInvalidFlg = 1;
	}
	else
	{
	    lcrbcu1SpeedInvalidFlg = 0;
	}

//	if ((lcrbcu1CANSignalInvalid==1) || (lcrbcu1SpeedInvalidFlg==1) || (lcahbu1InhibitFlg==1) || (lru1TargetSystemOK==0) ||
//	    (liu1fRBCSFaultDet==1))
//	{
//		lcrbcu1InhibitByFM = 1;
//	}
//	else
//	{
		lcrbcu1InhibitByFM = 0;
//	}



    
    if(lcrbcu1SignalValidAftIgnOn==0) /* Check every scan before Stabilizing Time After Ign On */
    {
        if((U8_RB_MAIN_DISABLE) > 0)
        {
            lcrbcu1InhibitBySetting = 1;
        }
        else
        {
            lcrbcu1InhibitBySetting = 0;
        }
    }
    
    

/* * Temporary for 6.10 release, 15 NZ pre*/
    if((lcrbcu1InhibitByFM==1) || (lcrbcu1InhibitBySetting==1))
    {
        lcrbcu1InhibitFlg = 1;
    }
    else
    {
        lcrbcu1InhibitFlg = 0;
    }

/*    lcrbcu1InhibitFlg = 1;*/
}

/*******************************************************************************
* FUNCTION NAME:		LCRBC_vCalMainCtrlParameters
* CALLED BY:			LCRBC_vCallControl()
* Preconditions:		none
* PARAMETER:			none
* RETURN VALUE:			none
* Description:			Cal Main RBC Control Parameters, such as MPRT
********************************************************************************/

static void LCRBC_vCalMainCtrlParameters(void)
{

    LCRBC_vDecideSpeedLimit();

    LCRBC_vCalMaxPossibleRegenTq();
    
    LCRBC_vDctHydBrakePressStatus();
    
    LCRBC_vDctActivationHistory();

}

/*******************************************************************************
* FUNCTION NAME:		LCRBC_vCalMainCtrlParameters
* CALLED BY:			LCRBC_vCallControl()
* Preconditions:		none
* PARAMETER:			none
* RETURN VALUE:			none
* Description:			Cal Main RBC Control Parameters, such as MPRT
********************************************************************************/

static void LCRBC_vDecideSpeedLimit(void)
{
    static int16_t lcrbcs16RegenEnterSpeedLimit = 0;

    LCRBC_vDecideBlendingSpd();

	if(GetPossibleRgnTq.lcahbu1AHBOn == 1)
    {
		lcrbcu1RbcBledSpeedUpdateFlag = ((lcrbcu1BlendSpdChangeOldFlg == 0) && (lcrbcu1BlendSpdChangeFlg == 1));
	}
	else
    {
		lcrbcu1RbcBledSpeedUpdateFlag = ((lcrbcu1BlendSpdChangeOldFlg == 1) && (lcrbcu1BlendSpdChangeFlg == 0));
	}

	if(GetPossibleRgnTq.lcrbcu1RbcSccBlendFlag == TRUE)
	{
		lcrbcu1RbcSccSpeedUpdateFlag = ((GetPossibleRgnTq.ldrbcu1SccFuctionActFlg == 0) && (GetPossibleRgnTq.ldrbcu1SccFuctionActOldFlg == 1));
	}
	else
	{
		lcrbcu1RbcSccSpeedUpdateFlag = ((GetPossibleRgnTq.ldrbcu1RbcSccReadyActFlg == 0) && (GetPossibleRgnTq.ldrbcu1RbcSccReadyActFlgOld == 1));
	}
	


    if( (lcrbcu1SignalValidAftIgnOn==0) ||
    	((GetPossibleRgnTq.ldrbcu1RbcSccReadyActFlg==1) && (GetPossibleRgnTq.ldrbcu1RbcSccReadyActFlgOld==0)) ||
    	(lcrbcu1RbcSccSpeedUpdateFlag == 1) || (lcrbcu1RbcBledSpeedUpdateFlag == 1) ) /* Check every scan before Stabilizing Time After Ign On */
    {
	    if(GetPossibleRgnTq.ldrbcu1RbcSccReadyActFlg==1)
	    {
	    	lcrbcs16RegenLowSpeedLimit = (int16_t)U16_SCC_RB_ENABLE_SPEED_LIMIT;
	    	lcrbcs16Regen2HydStartSpeed = (int16_t)U16_SCC_RB_REDUCTION_START_SPEED;
	    	lcrbcs16RegenEnterSpeedLimit = (int16_t)U16_SCC_RB_ENTER_SPEED_LIMIT;
        }
        else
        {
            lcrbcs16RegenLowSpeedLimit = (int16_t)U8_RB_ENABLE_SPEED_LIMIT;
	    	lcrbcs16RegenEnterSpeedLimit = (int16_t)U8_RB_ENTER_SPEED_LIMIT;
	    	
	    	if(lcrbcu1BlendSpdChangeFlg == 1)
	    	{
	    		lcrbcs16Regen2HydStartSpeed = lcrbcs16BlendSpdAtFastDecel;
	    	}
	    	else
	    	{
	    		lcrbcs16Regen2HydStartSpeed = (int16_t)U8_RB_REDUCTION_START_SPEED;
	    	}
        }
        
	    if(lcrbcs16RegenLowSpeedLimit < S16_SPEED_1_KPH)
        {
            lcrbcs16RegenLowSpeedLimit = S16_SPEED_1_KPH;
        }
        else
        {
            /* lcrbcs16RegenLowSpeedLimit = lcrbcs16RegenLowSpeedLimit;*/ ;
        }
        
        if(lcrbcs16Regen2HydStartSpeed <= lcrbcs16RegenLowSpeedLimit)
        {
            lcrbcs16Regen2HydStartSpeed = lcrbcs16RegenLowSpeedLimit;
        }
        else
        {
           /* lcrbcs16Regen2HydStartSpeed = lcrbcs16Regen2HydStartSpeed;*/;
        }
        
        if(lcrbcs16RegenEnterSpeedLimit <= lcrbcs16RegenLowSpeedLimit)
        {
            lcrbcs16RegenEnterSpeedLimit = lcrbcs16RegenLowSpeedLimit;
        }
        else
        {
            /*lcrbcs16RegenEnterSpeedLimit = lcrbcs16RegenEnterSpeedLimit;*/;
        }
    }
    else { }


	/* Speed Inhibition */
	
	if(lcrbcu1SpeedInvalidFlg==0)
	{
	    if(lcrbcu1ActiveFlg == 0)
	    {
    	    if(GetPossibleRgnTq.ldrbcs16VehicleSpeed < (lcrbcs16RegenEnterSpeedLimit))
    	    {
    	        lcrbcu1LowSpeedInhibitFlg = 1;
    	    }
    	    else
    	    {
    	        lcrbcu1LowSpeedInhibitFlg = 0;
    	    }
    	}
	    else
	    {
    	    if(GetPossibleRgnTq.ldrbcs16VehicleSpeed < (lcrbcs16RegenLowSpeedLimit))
    	    {
    	        lcrbcu1LowSpeedInhibitFlg = 1;
    	    }
    	    else
    	    {
    	        lcrbcu1LowSpeedInhibitFlg = 0;
    	    }
    	}
	    
	    if(GetPossibleRgnTq.ldrbcs16VehicleSpeed > ((int16_t)U8_MAX_POS_TQ_MAX_SPEED*S16_SPEED_1_KPH))
	    {
	        lcrbcu1MaxSpeedInhibitFlg = 1;
	    }
	    else
	    {
	        lcrbcu1MaxSpeedInhibitFlg = 0;
	    }
	}
	else
	{
	    lcrbcu1LowSpeedInhibitFlg = 1;
	    lcrbcu1MaxSpeedInhibitFlg = 1;
	}
    
}



/*******************************************************************************
* FUNCTION NAME:		LCRBC_vCalMaxPossibleRegenTq
* CALLED BY:			LCRBC_vCalMainCtrlParameters
* Preconditions:		none
* PARAMETER:			none
* RETURN VALUE:			none
* Description:			Decide RBC Inhibition conditions
********************************************************************************/

static void LCRBC_vCalMaxPossibleRegenTq(void)
{
    uint16_t u16MaxPosRegenTqByMap10Nm = 0;
   
    int16_t s16MaxTqLimitBar = 0;
    int16_t s16MaxPosRegenTqByMapBar = 0;
    int16_t s16MaxPosRegenTqRB2HydBar = 0;
    
    if((lcrbcu1LowSpeedInhibitFlg==0) && (lcrbcu1MaxSpeedInhibitFlg==0))
    {
        /* Eq2 , 0.1bar resol */
        if(GetPossibleRgnTq.ldrbcu1RbcSccReadyActFlg==1)
        {
        	s16MaxTqLimitBar = (int16_t)U8_SCC_MAX_RB_TQ_PRESS_LIMIT;
        }
        else
        {
//        	s16MaxTqLimitBar = ((int16_t)U8_MAX_RB_TQ_PRESS_LIMIT * S16_MPRESS_1_BAR);
        	s16MaxTqLimitBar = ((int16_t)U8_MAX_RB_TQ_PRESS_LIMIT * S16_PCTRL_PRESS_1_BAR);
        }
        /* Eq3 , 10Nm resol -> 0.1bar resol */

        /*u16MaxPosRegenTqByMap10Nm = ((U16_MAX_POS_TQ_EQ_RATIO_10NM/((uint16_t)GetPossibleRgnTq.ldrbcs16VehicleSpeed/S16_SPEED_1_KPH)) + U16_MAX_POS_TQ_EQ_CONST);  -> some truncation... */
        u16MaxPosRegenTqByMap10Nm = (uint16_t)(((int32_t)U16_MAX_POS_TQ_EQ_RATIO_10NM*S16_SPEED_1_KPH)/GetPossibleRgnTq.ldrbcs16VehicleSpeed) + U16_MAX_POS_TQ_EQ_CONST;  /* 10 Nm resol */
       
        if(u16MaxPosRegenTqByMap10Nm > (U16_RB_TORQUE_NM_MAX/10))
        {
            s16MaxPosRegenTqByMapBar = (int16_t)(((uint32_t)U16_RB_TORQUE_NM_MAX*100)/GetPossibleRgnTq.ldrbcu16PressToTorqRatioTotal);     /* Conv. 1Nm -> 0.1Nm -> 0.01bar resol */
        }
        else
        {
            s16MaxPosRegenTqByMapBar = (int16_t)(((uint32_t)u16MaxPosRegenTqByMap10Nm*1000)/GetPossibleRgnTq.ldrbcu16PressToTorqRatioTotal); /* Conv. 10Nm -> 0.1Nm -> 0.01bar resol */
        }

        /* Decide between Eq2 and Eq3 */
        if(s16MaxPosRegenTqByMapBar > s16MaxTqLimitBar)
        {
            lcrbcs16MaxPosRegenTqBar = s16MaxTqLimitBar;
        }
        else
        {
            lcrbcs16MaxPosRegenTqBar = s16MaxPosRegenTqByMapBar;
        }
        lcrbcu1ExitCmdRengenTorq = 0;
        /* Eq1 , 10Nm resol */
        if( (GetPossibleRgnTq.ldrbcs16VehicleSpeed <= lcrbcs16Regen2HydStartSpeed) && (lcrbcs16Regen2HydStartSpeed > lcrbcs16RegenLowSpeedLimit) )
        {
        	  s16MaxPosRegenTqRB2HydBar = L_s16IInter2Point(GetPossibleRgnTq.ldrbcs16VehicleSpeed,
		                                           lcrbcs16RegenLowSpeedLimit,  0,
		                                           lcrbcs16Regen2HydStartSpeed, s16MaxTqLimitBar);
			lcrbcu1ExitCmdRengenTorq = 1;
				
            
            /* Decide between Eq2 and Eq3 */
            if(lcrbcs16MaxPosRegenTqBar > s16MaxPosRegenTqRB2HydBar)
            {
                lcrbcs16MaxPosRegenTqBar = s16MaxPosRegenTqRB2HydBar;
            }
        }
        
        /* Conv. to Tq, 1Nm resol */
//        lcrbcu16MaxPosRegenTqNm = (uint16_t)(((uint32_t)lcrbcs16MaxPosRegenTqBar*GetPossibleRgnTq.ldrbcu16PressToTorqRatioTotal)/S16_MPRESS_1_BAR);
        lcrbcu16MaxPosRegenTqNm = (uint16_t)(((uint32_t)lcrbcs16MaxPosRegenTqBar*GetPossibleRgnTq.ldrbcu16PressToTorqRatioTotal)/S16_PCTRL_PRESS_1_BAR);
    }
    else
    {
		lcrbcu1ExitCmdRengenTorq = 0;
        lcrbcs16MaxPosRegenTqBar = 0;
        lcrbcu16MaxPosRegenTqNm  = 0;
    }
}


/*******************************************************************************
* FUNCTION NAME:		LCRBC_vDctHydBrakePressStatus
* CALLED BY:			LCRBC_vCalMainCtrlParameters()
* Preconditions:		none
* PARAMETER:			none
* RETURN VALUE:			none
* Description:			Detect Hydraulic Brake Pressure
********************************************************************************/
static void LCRBC_vDctHydBrakePressStatus(void)
{

	/*lcrbcs16GeneratedMCPress = lsahbs16MCpresFilt - ldahbs16CompForMinPressBC1;*/
	lcrbcs16GeneratedMCPress = GetPossibleRgnTq.lsahbs16MCpresFilt;

	
	if(lcrbcs16GeneratedMCPress > 0)
	{
//	    lcrbcu16GeneratedMCTqNm = (uint16_t)(((uint32_t)lcrbcs16GeneratedMCPress * GetPossibleRgnTq.ldrbcu16PressToTorqRatioTotal)/S16_MPRESS_1_BAR);
		lcrbcu16GeneratedMCTqNm = (uint16_t)(((uint32_t)lcrbcs16GeneratedMCPress * GetPossibleRgnTq.ldrbcu16PressToTorqRatioTotal)/S16_PCTRL_PRESS_1_BAR);
    }
	else
	{
	    lcrbcu16GeneratedMCTqNm = 0;
	}
}


/*******************************************************************************
* FUNCTION NAME:		LCRBC_vDctActivationHistory
* CALLED BY:			LCRBC_vCalMainCtrlParameters()
* Preconditions:		none
* PARAMETER:			none
* RETURN VALUE:			none
* Description:			Detect RBC Activation history and related parameters
********************************************************************************/
static void LCRBC_vDctActivationHistory(void)
{
    static int16_t lcrbcs16ReEnterDelayTimer = 0;
    
    /* check if RBC activated during braking cycle */
    if(GetPossibleRgnTq.ldrbcu1DriverIntendToBrake==1)
    {
        if(lcrbcu1ActiveFlg==1)
        {
            lcrbcu1ActivatedBrkCycle = 1;
        }
        else
        {
            ;
        }
    }
    else
    {
        lcrbcu1ActivatedBrkCycle = 0;
    }    
    

    /* Decide Delay Time Bf next RBC Entry */
    if(lcrbcu1ActivatedBrkCycle==1)
    {
        if(lcrbcu1ActiveFlg==0)
        {
            if(lcrbcs16ReEnterDelayTimer < U8_T_1000_MS) /*lcrbcs16ReEnterDelayTimeRef*/
            {
                lcrbcs16ReEnterDelayTimer = lcrbcs16ReEnterDelayTimer + 1;
                lcrbcu1DelayRBCReEnter = 1;
            }
            else
            {
                lcrbcu1DelayRBCReEnter = 0;
            }
        }
        else
        {
            lcrbcu1DelayRBCReEnter = 0;
            lcrbcs16ReEnterDelayTimer = 0;
        }
    }
    else
    {
        lcrbcu1DelayRBCReEnter = 0;
        lcrbcs16ReEnterDelayTimer = 0;
    }
    
}

/*******************************************************************************
* FUNCTION NAME:		LCRBC_vDecideCtrlState
* CALLED BY:			LCRBC_vCallControl()
* Preconditions:		none
* PARAMETER:			none
* RETURN VALUE:			none
* Description:			Decide RBC Inhibition conditions
********************************************************************************/

static void LCRBC_vDecideCtrlState(void)
{
	switch (lcrbcs8ControlState)
	{
		/*-------------------------------------------------------------------------- */		
		case S8_RBC_READY:
		    
		    LCRBC_vDecideRBCEnter();
		    
		    if(lcrbcu1CtrlStartOK==1)
		    {
		        lcrbcu1ActiveFlg = 1;
		        lcrbcs8ControlState = S8_RBC_CONTROL;
		        lcrbcu1CtrlStartOK = 0;
		    }
		    else
		    {
		        LCRBC_vResetCtrlVariables();
		    }
		    
		    break;
		/*-------------------------------------------------------------------------- */		
		case S8_RBC_CONTROL:
		    
		    LCRBC_vDecideRBCExit();
		    
		    if((lcrbcu1CtrlEndOK==1)||(lcrbcu1CtrlSlowEndOK==1))
		    {
		        lcrbcs8ControlState = S8_RBC_FADE_OUT;
		        
		        LCRBC_vFadeOutCtrl();
		        
		        lcrbcu1CtrlEndOK = 0;
		        lcrbcu1CtrlSlowEndOK = 0;
		    }
		    
		    break;
		/*-------------------------------------------------------------------------- */		
		case S8_RBC_FADE_OUT:
            
		    LCRBC_vDecideRBCExit();
		    LCRBC_vFadeOutCtrl();

		    if(lcrbcu1FadeOutCtrlEnd==1)
		    {
		        LCRBC_vResetCtrlVariables();
		        lcrbcu1FadeOutCtrlEnd = 0;
		        lcrbcs8ControlState = S8_RBC_READY;
		    }
            lcrbcu1CtrlEndOK = 0;
	        lcrbcu1CtrlSlowEndOK = 0;
		    
		    break;

		/*-------------------------------------------------------------------------- */		
      default:
          lcrbcs8ControlState = S8_RBC_READY;
      break;

    }
}

/*******************************************************************************
* FUNCTION NAME:		LCRBC_vDecideRBCEnter
* CALLED BY:			LCRBC_vDecideCtrlState()
* Preconditions:		none
* PARAMETER:			none
* RETURN VALUE:			none
* Description:			Decide RBC Enter conditions
********************************************************************************/

static void LCRBC_vDecideRBCEnter(void)
{
    lcrbcu1CtrlStartOK = 0;

    if( (GetPossibleRgnTq.lcahbu1AHBOn==1) &&(lcrbcu1InhibitFlg==0) && (lcrbcu1LowSpeedInhibitFlg==0) && (lcrbcu1MaxSpeedInhibitFlg==0) && (lcrbcu1DelayRBCReEnter==0) )
    {
        if((GetPossibleRgnTq.ldrbcu1SafetyFuctionActFlg==0) && (GetPossibleRgnTq.ldrbcu1InhibitRegenReEntryFlg == 0))
        {
            if((GetPossibleRgnTq.ldrbcu1DriverIntendToBrake==1) && (GetPossibleRgnTq.ldrbcu1DriverIntendToAccel==0) && (GetPossibleRgnTq.ldrbcu1RegenEnableGearSelected==1) && (lcrbcs16MaxPosRegenTqBar > (int16_t)U8_INHIB_EXIT_PRESS_BY_MPRT)
             && (GetPossibleRgnTq.ldrbcu1MotorRpmOKInDGearFlg==1))
            {
              #if __CORNER_BRAKE_RBC_INHIBIT==ENABLE
                if((GetPossibleRgnTq.ldrbcu1FastDecelApplyFlg==0)&&(GetPossibleRgnTq.ldrbcu1FastTurnFlg==0) && (GetPossibleRgnTq.ldrbcu1FrontWheelSlipDec==0))
              #else
                if(ldrbcu1FastDecelApplyFlg==0)
              #endif       
            	{
                	lcrbcu1CtrlStartOK = 1;
            	}
            }
            else { }
        }
        else { }
    }
    else { }    
}


/*******************************************************************************
* FUNCTION NAME:		LCRBC_vDecideRBCExit
* CALLED BY:			LCRBC_vDecideCtrlState()
* Preconditions:		none
* PARAMETER:			none
* RETURN VALUE:			none
* Description:			Decide RBC Enter conditions
********************************************************************************/

static void LCRBC_vDecideRBCExit(void)
{
    lcrbcu1CtrlSlowEndOK=0;
    if((GetPossibleRgnTq.lcahbu1AHBOn==0) || (lcrbcu1InhibitFlg==1) || (lcrbcu1LowSpeedInhibitFlg==1) || (lcrbcu1MaxSpeedInhibitFlg==1) )
    {
        lcrbcu1CtrlEndOK = 1;
    }
    else if( GetPossibleRgnTq.ldrbcu1SafetyFuctionActFlg==1 )
    {
        lcrbcu1CtrlEndOK = 1;
    }
    else if((GetPossibleRgnTq.ldrbcu1DriverIntendToBrake==0) || (GetPossibleRgnTq.ldrbcu1DriverIntendToAccel==1) || (GetPossibleRgnTq.ldrbcu1RegenEnableGearSelected==0) || (lcrbcs16MaxPosRegenTqBar <= (int16_t)U8_INHIB_SET_PRESS_BY_MPRT)
     || (GetPossibleRgnTq.ldrbcu1MotorRpmOKInDGearFlg==0))
    {
        lcrbcu1CtrlEndOK = 1;
    }
    else
    {
        lcrbcu1CtrlEndOK = 0;
    #if __CORNER_BRAKE_RBC_INHIBIT==ENABLE
        if((GetPossibleRgnTq.ldrbcu1FastDecelApplyFlg==1)||(GetPossibleRgnTq.ldrbcu1FastTurnFlg==1))
    #else
        if(GetPossibleRgnTq.ldrbcu1FastDecelApplyFlg==1)
    #endif
        {
            lcrbcu1CtrlSlowEndOK = 1; 
        }
        else if(GetPossibleRgnTq.ldrbcu1FrontWheelSlipDec==1)
        {
            lcrbcu1CtrlSlowEndOK = 1; 
        }
        else if((lcrbcu1CtrlSlowEndOKOld==1) && (lcrbcu1FadeOutCtrlFlag==1))
        {
            lcrbcu1CtrlSlowEndOK = 1; 
        }
        else 
        {
            lcrbcu1CtrlSlowEndOK = 0; 
        } 
    }
    lcrbcu1CtrlSlowEndOKOld = lcrbcu1CtrlSlowEndOK;
}

/*******************************************************************************
* FUNCTION NAME:		LCRBC_vFadeOutCtrl
* CALLED BY:			LCRBC_vDecideCtrlState()
* Preconditions:		none
* PARAMETER:			none
* RETURN VALUE:			none
* Description:			Decide RBC Fade Out parameters
********************************************************************************/
static void LCRBC_vFadeOutCtrl(void)
{
    static uint8_t lcrbcu8FadeOutTimeRef = 0;
    static uint8_t lcrbcu8FadeOutTimeCnt = 0;
    static int16_t lcrbcs16TargetRBPressAtCtrlExit = 0;
    
    if(lcrbcu1FadeOutCtrlFlag==0)
    {
        lcrbcu1FadeOutCtrlStart = 1;
        lcrbcu1FadeOutCtrlFlag = 1;
    }
    else
    {
        lcrbcu1FadeOutCtrlStart = 0;
    }

    if(lcrbcu1CtrlEndOK==1)
    {
//        lcrbcs8FadeOutRate = (int8_t)S16_MPRESS_1_BAR;
    	lcrbcs8FadeOutRate = (int8_t)S16_PCTRL_PRESS_1_BAR;
    }
    else if(lcrbcu1CtrlSlowEndOK==1)
    {
        lcrbcs8FadeOutRate = (lcrbcs8FadeOutRate < (int8_t)S16_RBC_SLOW_FADE_OUT_RATE) ? (int8_t)S16_RBC_SLOW_FADE_OUT_RATE : lcrbcs8FadeOutRate;  
    }
    else
    {
        lcrbcs8FadeOutRate = (int8_t)S16_RBC_FADE_OUT_RATE_DEFAULT;
    }
        
    if(lcrbcu1FadeOutCtrlStart==1)
    {
        lcrbcs16TargetRBPressAtCtrlExit = lcrbcs16TargetRBTqBar;
        lcrbcu8FadeOutTimeCnt = 1;
    }
    else 
    {
        if(lcrbcs8FadeOutRate<=0) 
        {
            lcrbcu8FadeOutTimeRef = U8_T_1000_MS;
        }
        else
        {
        	lcrbcu8FadeOutTimeRef = (uint8_t)(lcrbcs16TargetRBPressAtCtrlExit/(int16_t)lcrbcs8FadeOutRate);
        }
        
        if(lcrbcu8FadeOutTimeCnt < lcrbcu8FadeOutTimeRef)
        {
            lcrbcu8FadeOutTimeCnt = lcrbcu8FadeOutTimeCnt + 1;
        }
        else if(lcrbcs16TargetRBTqBar > lcrbcs8FadeOutRate)
        {
            lcrbcu8FadeOutTimeCnt = (uint8_t)L_u16LimitMinMax((lcrbcu8FadeOutTimeCnt + 1), 0, U8_T_1200_MS);
        }
        else
        {
            lcrbcu1FadeOutCtrlEnd = 1;
            lcrbcu8FadeOutTimeCnt = 0;
            lcrbcu1FadeOutCtrlFlag = 0;
        }
    }
}

static void LCRBC_vResetCtrlVariables(void)
{
    lcrbcu1ActiveFlg = 0;
    lcrbcs16TargetRBTqBar = 0;
    lcrbcu16TargetRBTqNm = 0;
    lcrbcs8FadeOutRate = 0;
    lcrbcu1CtrlSlowEndOKOld = 0;
}


/*******************************************************************************
* FUNCTION NAME:		LCRBC_vGenRegenBrkTorqCmd
* CALLED BY:			LCRBC_vCallControl()
* Preconditions:		none
* PARAMETER:			none
* RETURN VALUE:			none
* Description:			Decide Target Regen Brake Torque Cmd
********************************************************************************/

static void LCRBC_vGenRegenBrkTorqCmd(void)
{
    int16_t s16TargetRBPressTemp;      
    int8_t s8TargetRBPressIncLimit;
    int8_t s8TargetRBPressDecLimit;
    int16_t s16DriverIntendedBrkPress;
    int16_t  lcrbcs16TargetRBTqBarOld;

    lcrbcs16TargetRBTqBarOld = lcrbcs16TargetRBTqBar;
    
    if(lcrbcu1ActiveFlg == 1)
    {
		if(GetPossibleRgnTq.ldrbcu1RbcSccReadyActFlg==1)
		{
			s16DriverIntendedBrkPress = GetPossibleRgnTq.ldrbcs16DrvIntendedBrkP - (int16_t)U8_SCC_TAR_WL_PRESS_MIN_1;
		}
		else
        {
        s16DriverIntendedBrkPress = GetPossibleRgnTq.ldrbcs16DrvIntendedBrkP - (int16_t)U8_TAR_WL_PRESS_MIN_1;
		}
        s16DriverIntendedBrkPress = L_s16LimitMinMax(s16DriverIntendedBrkPress, S16_MPRESS_0_BAR, GetPossibleRgnTq.ldrbcs16DrvIntendedBrkP);
        
        if(s16DriverIntendedBrkPress <= lcrbcs16MaxPosRegenTqBar)
        {
            s16TargetRBPressTemp = s16DriverIntendedBrkPress;
        }
        else
        {
            s16TargetRBPressTemp = lcrbcs16MaxPosRegenTqBar;
        }

        s8TargetRBPressIncLimit = (int8_t)U8_RB_TQ_CMD_INC_RATE;/*S16_MPRESS_0_1_BAR;*/
//        s8TargetRBPressDecLimit = (int8_t)S16_MPRESS_0_7_BAR;
        s8TargetRBPressDecLimit = (int8_t)S16_PCTRL_PRESS_0_7_BAR;

        if(lcrbcu1FadeOutCtrlFlag == 1)  
        {
            s16TargetRBPressTemp = ((lcrbcs16TargetRBTqBarOld - lcrbcs8FadeOutRate) < s16TargetRBPressTemp) ? (lcrbcs16TargetRBTqBarOld - lcrbcs8FadeOutRate) : s16TargetRBPressTemp;

//            s8TargetRBPressIncLimit = S16_MPRESS_0_BAR;
            s8TargetRBPressIncLimit = S16_PCTRL_PRESS_0_BAR;
            s8TargetRBPressDecLimit = (lcrbcs8FadeOutRate > s8TargetRBPressDecLimit) ? lcrbcs8FadeOutRate : s8TargetRBPressDecLimit;             
        }
        

        /* Limit Inc / Dec rate */
        if((s16TargetRBPressTemp-lcrbcs16TargetRBTqBarOld) > ((int16_t)s8TargetRBPressIncLimit))
        {
            lcrbcs16TargetRBTqBar = lcrbcs16TargetRBTqBarOld + (int16_t)s8TargetRBPressIncLimit;
        }
        else if((s16TargetRBPressTemp-lcrbcs16TargetRBTqBarOld) < (-(int16_t)s8TargetRBPressDecLimit))
        {
            lcrbcs16TargetRBTqBar = lcrbcs16TargetRBTqBarOld - (int16_t)s8TargetRBPressDecLimit;
        }
        else
        { 
            lcrbcs16TargetRBTqBar = s16TargetRBPressTemp;
        }
//        lcrbcs16TargetRBTqBar = L_s16LimitMinMax(lcrbcs16TargetRBTqBar, S16_MPRESS_0_BAR, s16DriverIntendedBrkPress);
        lcrbcs16TargetRBTqBar = L_s16LimitMinMax(lcrbcs16TargetRBTqBar, S16_PCTRL_PRESS_0_BAR, s16DriverIntendedBrkPress);
        
        /* Conv. to Torque */
//        lcrbcu16TargetRBTqNm = ((uint16_t)lcrbcs16TargetRBTqBar*GetPossibleRgnTq.ldrbcu16PressToTorqRatioTotal)/S16_MPRESS_1_BAR ; /* Command to HCU, 1Nm resol*/
        lcrbcu16TargetRBTqNm = ((uint16_t)lcrbcs16TargetRBTqBar*GetPossibleRgnTq.ldrbcu16PressToTorqRatioTotal)/S16_PCTRL_PRESS_1_BAR ; /* Command to HCU, 1Nm resol*/
    }
    else
    {
        lcrbcs16TargetRBTqBar = 0;
        lcrbcu16TargetRBTqNm = 0;
    }
}

/*******************************************************************************
* FUNCTION NAME:		LCRBC_vDecideBlendingSpd
* CALLED BY:			LCRBC_vCalMainCtrlParameters()
* Preconditions:		none
* PARAMETER:			none
* RETURN VALUE:			none
* Description:			Decide Target Regen Brake Torque Cmd
********************************************************************************/

static void LCRBC_vDecideBlendingSpd(void)
{
	lcrbcu1BlendSpdChangeOldFlg = lcrbcu1BlendSpdChangeFlg;
	
	if(GetPossibleRgnTq.lcahbu1AHBOn == 1)
	{
		if((GetPossibleRgnTq.ldrbcs16VehicleSpeed <= (U16_RB_REDUCTION_START_SPD_HIGH + S16_SPEED_1_KPH)) && (lcrbcu1ExitCmdRengenTorq == 0))
		{
			if(GetPossibleRgnTq.ldrbcs16DrvIntendedBrkP >= S16_BLEND_SPD_CHANGE_PRESS_THRES)
			{
				lcrbcu1BlendSpdChangeFlg = 1;
				
				if(GetPossibleRgnTq.ldrbcs16VehicleSpeed >= (int16_t)U16_RB_REDUCTION_START_SPD_HIGH)
				{
					lcrbcs16BlendSpdAtFastDecel = (int16_t)U16_RB_REDUCTION_START_SPD_HIGH;
				}
				else if(GetPossibleRgnTq.ldrbcs16VehicleSpeed <= (int16_t)U8_RB_REDUCTION_START_SPEED)
				{
					lcrbcs16BlendSpdAtFastDecel = (int16_t)U8_RB_REDUCTION_START_SPEED;
				}
				else
				{
					lcrbcs16BlendSpdAtFastDecel = GetPossibleRgnTq.ldrbcs16VehicleSpeed;
				}
			}
			else { }
		}
		else { }
	}
	else
	{
		lcrbcu1BlendSpdChangeFlg = 0;
	}
}


static void LARBC_vSetRbcDataTxInterface(void)
{


/* Hydraulic Brake Force */
	Rbc_CtrlBus.Rbc_CtrlTarRgnBrkTqInfo.EstHydBrkForce = lcrbcu16GeneratedMCTqNm;	/* resolution : 1 Nm */

/* RBC Conversion Flag */
	/*lru1RbcRegenToFricConvReq = lru1RbcRegenToFricConvFlag; not sent in CAN*/

/* Regen Coop Enable Flag */
	if(lcrbcu1InhibitFlg==1)
	{
		Rbc_CtrlBus.Rbc_CtrlTarRgnBrkTqInfo.EhbStat = 0;
	}
	else
	{
		Rbc_CtrlBus.Rbc_CtrlTarRgnBrkTqInfo.EhbStat = 1;
	}


	Rbc_CtrlBus.Rbc_CtrlTarRgnBrkTqInfo.TarRgnBrkTq = lcrbcu16TargetRBTqNm;	/* resolution : 1 Nm */

}

static void LCRBC_vOutPutControl(void)
{
	LARBC_vSetRbcDataTxInterface();

	Rbc_CtrlBus.Rbc_CtrlRgnBrkCtrlrActStFlg = lcrbcu1ActiveFlg;
//	RBCbus.TarRgnBrkTqInfo.TarRgnBrkTq = lcrbcu16TargetRBTqNm;

    RBCsrcbus.CANSignalInvalid = lcrbcu1CANSignalInvalid;
    RBCsrcbus.InhibitFlg = lcrbcu1InhibitFlg;
    RBCsrcbus.SpeedInvalidFlg = lcrbcu1SpeedInvalidFlg;
    RBCsrcbus.InhibitByFM=lcrbcu1InhibitByFM;
    RBCsrcbus.InhibitBySetting=lcrbcu1InhibitBySetting;
    RBCsrcbus.RbcBledSpeedUpdateFlag=lcrbcu1RbcBledSpeedUpdateFlag;
    RBCsrcbus.BlendSpdChangeFlg=lcrbcu1BlendSpdChangeFlg;
    RBCsrcbus.BlendSpdChangeOldFlg=lcrbcu1BlendSpdChangeOldFlg;
    RBCsrcbus.RbcSccSpeedUpdateFlag=lcrbcu1RbcSccSpeedUpdateFlag;
    RBCsrcbus.LowSpeedInhibitFlg=lcrbcu1LowSpeedInhibitFlg;
    RBCsrcbus.MaxSpeedInhibitFlg=lcrbcu1MaxSpeedInhibitFlg;
    RBCsrcbus.ExitCmdRengenTorq=lcrbcu1ExitCmdRengenTorq;
    RBCsrcbus.ActivatedBrkCycle=lcrbcu1ActivatedBrkCycle;
    RBCsrcbus.DelayRBCReEnter=lcrbcu1DelayRBCReEnter;
    RBCsrcbus.CtrlStartOK=lcrbcu1CtrlStartOK;
    RBCsrcbus.CtrlEndOK=lcrbcu1CtrlEndOK;
    RBCsrcbus.CtrlSlowEndOK=lcrbcu1CtrlSlowEndOK;
    RBCsrcbus.CtrlSlowEndOKOld=lcrbcu1CtrlSlowEndOKOld;
    RBCsrcbus.FadeOutCtrlFlag=lcrbcu1FadeOutCtrlFlag;
    RBCsrcbus.FadeOutCtrlEnd=lcrbcu1FadeOutCtrlEnd;
    RBCsrcbus.FadeOutCtrlStart=lcrbcu1FadeOutCtrlStart;
    RBCsrcbus.SignalValidAftIgnOn=lcrbcu1SignalValidAftIgnOn; // ok
    RBCsrcbus.ControlState=lcrbcs8ControlState;
    RBCsrcbus.FadeOutRate=lcrbcs8FadeOutRate;
    RBCsrcbus.RegenLowSpeedLimit=lcrbcs16RegenLowSpeedLimit;
}


#define RBC_CTRL_STOP_SEC_CODE
#include "Rbc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
