/**
 * @defgroup Rbc_Ctrl Rbc_Ctrl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Rbc_Ctrl.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Rbc_Ctrl.h"
#include "Rbc_Ctrl_Ifa.h"
#include "IfxStm_reg.h"
#include "RBC_DetControlState.h"
#include "RBC_CalcPossibleRegenTq.h"
#include "LSRBC_ConvActualRegenTqToPress.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define RBC_CTRL_START_SEC_CONST_UNSPECIFIED
#include "Rbc_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define RBC_CTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Rbc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define RBC_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Rbc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Rbc_Ctrl_HdrBusType Rbc_CtrlBus;

/* Internal Logical Bus */
RegenBrakeCooperation_SRCbusType RBCsrcbus;

/* Version Info */
const SwcVersionInfo_t Rbc_CtrlVersionInfo = 
{   
    RBC_CTRL_MODULE_ID,           /* Rbc_CtrlVersionInfo.ModuleId */
    RBC_CTRL_MAJOR_VERSION,       /* Rbc_CtrlVersionInfo.MajorVer */
    RBC_CTRL_MINOR_VERSION,       /* Rbc_CtrlVersionInfo.MinorVer */
    RBC_CTRL_PATCH_VERSION,       /* Rbc_CtrlVersionInfo.PatchVer */
    RBC_CTRL_BRANCH_VERSION       /* Rbc_CtrlVersionInfo.BranchVer */
};
    
/* Input Data Element */
Proxy_RxCanRxRegenInfo_t Rbc_CtrlCanRxRegenInfo;
Proxy_RxCanRxAccelPedlInfo_t Rbc_CtrlCanRxAccelPedlInfo;
Proxy_RxCanRxIdbInfo_t Rbc_CtrlCanRxIdbInfo;
Proxy_RxCanRxEngTempInfo_t Rbc_CtrlCanRxEngTempInfo;
Abc_CtrlAbsCtrlInfo_t Rbc_CtrlAbsCtrlInfo;
Abc_CtrlAvhCtrlInfo_t Rbc_CtrlAvhCtrlInfo;
Abc_CtrlEscCtrlInfo_t Rbc_CtrlEscCtrlInfo;
Abc_CtrlSccCtrlInfo_t Rbc_CtrlSccCtrlInfo;
Abc_CtrlTcsCtrlInfo_t Rbc_CtrlTcsCtrlInfo;
Spc_5msCtrlCircPFildInfo_t Rbc_CtrlCircPFildInfo;
Spc_5msCtrlPedlTrvlFildInfo_t Rbc_CtrlPedlTrvlFildInfo;
Det_5msCtrlPedlTrvlRateInfo_t Rbc_CtrlPedlTrvlRateInfo;
Spc_5msCtrlPistPFildInfo_t Rbc_CtrlPistPFildInfo;
Eem_MainEemFailData_t Rbc_CtrlEemFailData;
Eem_MainEemSuspectData_t Rbc_CtrlEemSuspectData;
Mom_HndlrEcuModeSts_t Rbc_CtrlEcuModeSts;
Prly_HndlrIgnOnOffSts_t Rbc_CtrlIgnOnOffSts;
Pct_5msCtrlPCtrlAct_t Rbc_CtrlPCtrlAct;
Eem_SuspcDetnFuncInhibitRbcSts_t Rbc_CtrlFuncInhibitRbcSts;
Abc_CtrlAy_t Rbc_CtrlAy;
Spc_5msCtrlBlsFild_t Rbc_CtrlBlsFild;
Det_5msCtrlPedlTrvlFinal_t Rbc_CtrlPedlTrvlFinal;
Bbc_CtrlTarPFromBaseBrkCtrlr_t Rbc_CtrlTarPFromBaseBrkCtrlr;
Bbc_CtrlTarPRateFromBaseBrkCtrlr_t Rbc_CtrlTarPRateFromBaseBrkCtrlr;
Bbc_CtrlTarPFromBrkPedl_t Rbc_CtrlTarPFromBrkPedl;
Det_5msCtrlVehSpdFild_t Rbc_CtrlVehSpdFild;

/* Output Data Element */
Rbc_CtrlTarRgnBrkTqInfo_t Rbc_CtrlTarRgnBrkTqInfo;
Rbc_CtrlRgnBrkCoopWithAbsInfo_t Rbc_CtrlRgnBrkCoopWithAbsInfo;
Rbc_CtrlBrkPRednForBaseBrkCtrlr_t Rbc_CtrlBrkPRednForBaseBrkCtrlr;
Rbc_CtrlRgnBrkCtlrBlendgFlg_t Rbc_CtrlRgnBrkCtlrBlendgFlg;
Rbc_CtrlRgnBrkCtrlrActStFlg_t Rbc_CtrlRgnBrkCtrlrActStFlg;

uint32 Rbc_Ctrl_Timer_Start;
uint32 Rbc_Ctrl_Timer_Elapsed;

#define RBC_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Rbc_MemMap.h"
#define RBC_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Rbc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define RBC_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Rbc_MemMap.h"
#define RBC_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Rbc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define RBC_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Rbc_MemMap.h"
#define RBC_CTRL_START_SEC_VAR_32BIT
#include "Rbc_MemMap.h"
/** Variable Section (32BIT)**/


#define RBC_CTRL_STOP_SEC_VAR_32BIT
#include "Rbc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define RBC_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Rbc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define RBC_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Rbc_MemMap.h"
#define RBC_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Rbc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define RBC_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Rbc_MemMap.h"
#define RBC_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Rbc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define RBC_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Rbc_MemMap.h"
#define RBC_CTRL_START_SEC_VAR_32BIT
#include "Rbc_MemMap.h"
/** Variable Section (32BIT)**/


#define RBC_CTRL_STOP_SEC_VAR_32BIT
#include "Rbc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define RBC_CTRL_START_SEC_CODE
#include "Rbc_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Rbc_Ctrl_Init(void)
{
    /* Initialize internal bus */
    Rbc_CtrlBus.Rbc_CtrlCanRxRegenInfo.HcuRegenEna = 0;
    Rbc_CtrlBus.Rbc_CtrlCanRxRegenInfo.HcuRegenBrkTq = 0;
    Rbc_CtrlBus.Rbc_CtrlCanRxAccelPedlInfo.AccelPedlVal = 0;
    Rbc_CtrlBus.Rbc_CtrlCanRxAccelPedlInfo.AccelPedlValErr = 0;
    Rbc_CtrlBus.Rbc_CtrlCanRxIdbInfo.EngMsgFault = 0;
    Rbc_CtrlBus.Rbc_CtrlCanRxIdbInfo.GearSelDisp = 0;
    Rbc_CtrlBus.Rbc_CtrlCanRxIdbInfo.TcuSwiGs = 0;
    Rbc_CtrlBus.Rbc_CtrlCanRxIdbInfo.HcuServiceMod = 0;
    Rbc_CtrlBus.Rbc_CtrlCanRxIdbInfo.SubCanBusSigFault = 0;
    Rbc_CtrlBus.Rbc_CtrlCanRxIdbInfo.CoolantTemp = 0;
    Rbc_CtrlBus.Rbc_CtrlCanRxIdbInfo.CoolantTempErr = 0;
    Rbc_CtrlBus.Rbc_CtrlCanRxEngTempInfo.EngTemp = 0;
    Rbc_CtrlBus.Rbc_CtrlCanRxEngTempInfo.EngTempErr = 0;
    Rbc_CtrlBus.Rbc_CtrlAbsCtrlInfo.AbsActFlg = 0;
    Rbc_CtrlBus.Rbc_CtrlAbsCtrlInfo.AbsDefectFlg = 0;
    Rbc_CtrlBus.Rbc_CtrlAbsCtrlInfo.AbsTarPFrntLe = 0;
    Rbc_CtrlBus.Rbc_CtrlAbsCtrlInfo.AbsTarPFrntRi = 0;
    Rbc_CtrlBus.Rbc_CtrlAbsCtrlInfo.AbsTarPReLe = 0;
    Rbc_CtrlBus.Rbc_CtrlAbsCtrlInfo.AbsTarPReRi = 0;
    Rbc_CtrlBus.Rbc_CtrlAbsCtrlInfo.FrntWhlSlip = 0;
    Rbc_CtrlBus.Rbc_CtrlAbsCtrlInfo.AbsDesTarP = 0;
    Rbc_CtrlBus.Rbc_CtrlAbsCtrlInfo.AbsDesTarPReqFlg = 0;
    Rbc_CtrlBus.Rbc_CtrlAbsCtrlInfo.AbsCtrlFadeOutFlg = 0;
    Rbc_CtrlBus.Rbc_CtrlAvhCtrlInfo.AvhActFlg = 0;
    Rbc_CtrlBus.Rbc_CtrlAvhCtrlInfo.AvhDefectFlg = 0;
    Rbc_CtrlBus.Rbc_CtrlAvhCtrlInfo.AvhTarP = 0;
    Rbc_CtrlBus.Rbc_CtrlEscCtrlInfo.EscActFlg = 0;
    Rbc_CtrlBus.Rbc_CtrlEscCtrlInfo.EscDefectFlg = 0;
    Rbc_CtrlBus.Rbc_CtrlEscCtrlInfo.EscTarPFrntLe = 0;
    Rbc_CtrlBus.Rbc_CtrlEscCtrlInfo.EscTarPFrntRi = 0;
    Rbc_CtrlBus.Rbc_CtrlEscCtrlInfo.EscTarPReLe = 0;
    Rbc_CtrlBus.Rbc_CtrlEscCtrlInfo.EscTarPReRi = 0;
    Rbc_CtrlBus.Rbc_CtrlSccCtrlInfo.SccActFlg = 0;
    Rbc_CtrlBus.Rbc_CtrlSccCtrlInfo.SccDefectFlg = 0;
    Rbc_CtrlBus.Rbc_CtrlSccCtrlInfo.SccTarP = 0;
    Rbc_CtrlBus.Rbc_CtrlTcsCtrlInfo.TcsActFlg = 0;
    Rbc_CtrlBus.Rbc_CtrlTcsCtrlInfo.TcsDefectFlg = 0;
    Rbc_CtrlBus.Rbc_CtrlTcsCtrlInfo.TcsTarPFrntLe = 0;
    Rbc_CtrlBus.Rbc_CtrlTcsCtrlInfo.TcsTarPFrntRi = 0;
    Rbc_CtrlBus.Rbc_CtrlTcsCtrlInfo.TcsTarPReLe = 0;
    Rbc_CtrlBus.Rbc_CtrlTcsCtrlInfo.TcsTarPReRi = 0;
    Rbc_CtrlBus.Rbc_CtrlTcsCtrlInfo.BothDrvgWhlBrkCtlReqFlg = 0;
    Rbc_CtrlBus.Rbc_CtrlCircPFildInfo.PrimCircPFild_1_100Bar = 0;
    Rbc_CtrlBus.Rbc_CtrlCircPFildInfo.SecdCircPFild_1_100Bar = 0;
    Rbc_CtrlBus.Rbc_CtrlPedlTrvlFildInfo.PdtFild = 0;
    Rbc_CtrlBus.Rbc_CtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec = 0;
    Rbc_CtrlBus.Rbc_CtrlPistPFildInfo.PistPFild_1_100Bar = 0;
    Rbc_CtrlBus.Rbc_CtrlEemFailData.Eem_Fail_WssFL = 0;
    Rbc_CtrlBus.Rbc_CtrlEemFailData.Eem_Fail_WssFR = 0;
    Rbc_CtrlBus.Rbc_CtrlEemFailData.Eem_Fail_WssRL = 0;
    Rbc_CtrlBus.Rbc_CtrlEemFailData.Eem_Fail_WssRR = 0;
    Rbc_CtrlBus.Rbc_CtrlEemSuspectData.Eem_Suspect_WssFL = 0;
    Rbc_CtrlBus.Rbc_CtrlEemSuspectData.Eem_Suspect_WssFR = 0;
    Rbc_CtrlBus.Rbc_CtrlEemSuspectData.Eem_Suspect_WssRL = 0;
    Rbc_CtrlBus.Rbc_CtrlEemSuspectData.Eem_Suspect_WssRR = 0;
    Rbc_CtrlBus.Rbc_CtrlEcuModeSts = 0;
    Rbc_CtrlBus.Rbc_CtrlIgnOnOffSts = 0;
    Rbc_CtrlBus.Rbc_CtrlPCtrlAct = 0;
    Rbc_CtrlBus.Rbc_CtrlFuncInhibitRbcSts = 0;
    Rbc_CtrlBus.Rbc_CtrlAy = 0;
    Rbc_CtrlBus.Rbc_CtrlBlsFild = 0;
    Rbc_CtrlBus.Rbc_CtrlPedlTrvlFinal = 0;
    Rbc_CtrlBus.Rbc_CtrlTarPFromBaseBrkCtrlr = 0;
    Rbc_CtrlBus.Rbc_CtrlTarPRateFromBaseBrkCtrlr = 0;
    Rbc_CtrlBus.Rbc_CtrlTarPFromBrkPedl = 0;
    Rbc_CtrlBus.Rbc_CtrlVehSpdFild = 0;
    Rbc_CtrlBus.Rbc_CtrlTarRgnBrkTqInfo.TarRgnBrkTq = 0;
    Rbc_CtrlBus.Rbc_CtrlTarRgnBrkTqInfo.VirtStkDep = 0;
    Rbc_CtrlBus.Rbc_CtrlTarRgnBrkTqInfo.VirtStkValid = 0;
    Rbc_CtrlBus.Rbc_CtrlTarRgnBrkTqInfo.EstTotBrkForce = 0;
    Rbc_CtrlBus.Rbc_CtrlTarRgnBrkTqInfo.EstHydBrkForce = 0;
    Rbc_CtrlBus.Rbc_CtrlTarRgnBrkTqInfo.EhbStat = 0;
    Rbc_CtrlBus.Rbc_CtrlRgnBrkCoopWithAbsInfo.FrntSlipDetThdSlip = 0;
    Rbc_CtrlBus.Rbc_CtrlRgnBrkCoopWithAbsInfo.FrntSlipDetThdUDiff = 0;
    Rbc_CtrlBus.Rbc_CtrlBrkPRednForBaseBrkCtrlr = 0;
    Rbc_CtrlBus.Rbc_CtrlRgnBrkCtlrBlendgFlg = 0;
    Rbc_CtrlBus.Rbc_CtrlRgnBrkCtrlrActStFlg = 0;
}

void Rbc_Ctrl(void)
{
    Rbc_Ctrl_Timer_Start = STM0_TIM0.U;

    /* Input */
    Rbc_Ctrl_Read_Rbc_CtrlCanRxRegenInfo(&Rbc_CtrlBus.Rbc_CtrlCanRxRegenInfo);
    /*==============================================================================
    * Members of structure Rbc_CtrlCanRxRegenInfo 
     : Rbc_CtrlCanRxRegenInfo.HcuRegenEna;
     : Rbc_CtrlCanRxRegenInfo.HcuRegenBrkTq;
     =============================================================================*/
    
    Rbc_Ctrl_Read_Rbc_CtrlCanRxAccelPedlInfo(&Rbc_CtrlBus.Rbc_CtrlCanRxAccelPedlInfo);
    /*==============================================================================
    * Members of structure Rbc_CtrlCanRxAccelPedlInfo 
     : Rbc_CtrlCanRxAccelPedlInfo.AccelPedlVal;
     : Rbc_CtrlCanRxAccelPedlInfo.AccelPedlValErr;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Rbc_Ctrl_Read_Rbc_CtrlCanRxIdbInfo_EngMsgFault(&Rbc_CtrlBus.Rbc_CtrlCanRxIdbInfo.EngMsgFault);
    Rbc_Ctrl_Read_Rbc_CtrlCanRxIdbInfo_GearSelDisp(&Rbc_CtrlBus.Rbc_CtrlCanRxIdbInfo.GearSelDisp);
    Rbc_Ctrl_Read_Rbc_CtrlCanRxIdbInfo_TcuSwiGs(&Rbc_CtrlBus.Rbc_CtrlCanRxIdbInfo.TcuSwiGs);
    Rbc_Ctrl_Read_Rbc_CtrlCanRxIdbInfo_HcuServiceMod(&Rbc_CtrlBus.Rbc_CtrlCanRxIdbInfo.HcuServiceMod);
    Rbc_Ctrl_Read_Rbc_CtrlCanRxIdbInfo_SubCanBusSigFault(&Rbc_CtrlBus.Rbc_CtrlCanRxIdbInfo.SubCanBusSigFault);
    Rbc_Ctrl_Read_Rbc_CtrlCanRxIdbInfo_CoolantTemp(&Rbc_CtrlBus.Rbc_CtrlCanRxIdbInfo.CoolantTemp);
    Rbc_Ctrl_Read_Rbc_CtrlCanRxIdbInfo_CoolantTempErr(&Rbc_CtrlBus.Rbc_CtrlCanRxIdbInfo.CoolantTempErr);

    Rbc_Ctrl_Read_Rbc_CtrlCanRxEngTempInfo(&Rbc_CtrlBus.Rbc_CtrlCanRxEngTempInfo);
    /*==============================================================================
    * Members of structure Rbc_CtrlCanRxEngTempInfo 
     : Rbc_CtrlCanRxEngTempInfo.EngTemp;
     : Rbc_CtrlCanRxEngTempInfo.EngTempErr;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Rbc_Ctrl_Read_Rbc_CtrlAbsCtrlInfo_AbsActFlg(&Rbc_CtrlBus.Rbc_CtrlAbsCtrlInfo.AbsActFlg);
    Rbc_Ctrl_Read_Rbc_CtrlAbsCtrlInfo_AbsDefectFlg(&Rbc_CtrlBus.Rbc_CtrlAbsCtrlInfo.AbsDefectFlg);
    Rbc_Ctrl_Read_Rbc_CtrlAbsCtrlInfo_AbsTarPFrntLe(&Rbc_CtrlBus.Rbc_CtrlAbsCtrlInfo.AbsTarPFrntLe);
    Rbc_Ctrl_Read_Rbc_CtrlAbsCtrlInfo_AbsTarPFrntRi(&Rbc_CtrlBus.Rbc_CtrlAbsCtrlInfo.AbsTarPFrntRi);
    Rbc_Ctrl_Read_Rbc_CtrlAbsCtrlInfo_AbsTarPReLe(&Rbc_CtrlBus.Rbc_CtrlAbsCtrlInfo.AbsTarPReLe);
    Rbc_Ctrl_Read_Rbc_CtrlAbsCtrlInfo_AbsTarPReRi(&Rbc_CtrlBus.Rbc_CtrlAbsCtrlInfo.AbsTarPReRi);
    Rbc_Ctrl_Read_Rbc_CtrlAbsCtrlInfo_FrntWhlSlip(&Rbc_CtrlBus.Rbc_CtrlAbsCtrlInfo.FrntWhlSlip);
    Rbc_Ctrl_Read_Rbc_CtrlAbsCtrlInfo_AbsDesTarP(&Rbc_CtrlBus.Rbc_CtrlAbsCtrlInfo.AbsDesTarP);
    Rbc_Ctrl_Read_Rbc_CtrlAbsCtrlInfo_AbsDesTarPReqFlg(&Rbc_CtrlBus.Rbc_CtrlAbsCtrlInfo.AbsDesTarPReqFlg);
    Rbc_Ctrl_Read_Rbc_CtrlAbsCtrlInfo_AbsCtrlFadeOutFlg(&Rbc_CtrlBus.Rbc_CtrlAbsCtrlInfo.AbsCtrlFadeOutFlg);

    Rbc_Ctrl_Read_Rbc_CtrlAvhCtrlInfo(&Rbc_CtrlBus.Rbc_CtrlAvhCtrlInfo);
    /*==============================================================================
    * Members of structure Rbc_CtrlAvhCtrlInfo 
     : Rbc_CtrlAvhCtrlInfo.AvhActFlg;
     : Rbc_CtrlAvhCtrlInfo.AvhDefectFlg;
     : Rbc_CtrlAvhCtrlInfo.AvhTarP;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Rbc_Ctrl_Read_Rbc_CtrlEscCtrlInfo_EscActFlg(&Rbc_CtrlBus.Rbc_CtrlEscCtrlInfo.EscActFlg);
    Rbc_Ctrl_Read_Rbc_CtrlEscCtrlInfo_EscDefectFlg(&Rbc_CtrlBus.Rbc_CtrlEscCtrlInfo.EscDefectFlg);
    Rbc_Ctrl_Read_Rbc_CtrlEscCtrlInfo_EscTarPFrntLe(&Rbc_CtrlBus.Rbc_CtrlEscCtrlInfo.EscTarPFrntLe);
    Rbc_Ctrl_Read_Rbc_CtrlEscCtrlInfo_EscTarPFrntRi(&Rbc_CtrlBus.Rbc_CtrlEscCtrlInfo.EscTarPFrntRi);
    Rbc_Ctrl_Read_Rbc_CtrlEscCtrlInfo_EscTarPReLe(&Rbc_CtrlBus.Rbc_CtrlEscCtrlInfo.EscTarPReLe);
    Rbc_Ctrl_Read_Rbc_CtrlEscCtrlInfo_EscTarPReRi(&Rbc_CtrlBus.Rbc_CtrlEscCtrlInfo.EscTarPReRi);

    Rbc_Ctrl_Read_Rbc_CtrlSccCtrlInfo(&Rbc_CtrlBus.Rbc_CtrlSccCtrlInfo);
    /*==============================================================================
    * Members of structure Rbc_CtrlSccCtrlInfo 
     : Rbc_CtrlSccCtrlInfo.SccActFlg;
     : Rbc_CtrlSccCtrlInfo.SccDefectFlg;
     : Rbc_CtrlSccCtrlInfo.SccTarP;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Rbc_Ctrl_Read_Rbc_CtrlTcsCtrlInfo_TcsActFlg(&Rbc_CtrlBus.Rbc_CtrlTcsCtrlInfo.TcsActFlg);
    Rbc_Ctrl_Read_Rbc_CtrlTcsCtrlInfo_TcsDefectFlg(&Rbc_CtrlBus.Rbc_CtrlTcsCtrlInfo.TcsDefectFlg);
    Rbc_Ctrl_Read_Rbc_CtrlTcsCtrlInfo_TcsTarPFrntLe(&Rbc_CtrlBus.Rbc_CtrlTcsCtrlInfo.TcsTarPFrntLe);
    Rbc_Ctrl_Read_Rbc_CtrlTcsCtrlInfo_TcsTarPFrntRi(&Rbc_CtrlBus.Rbc_CtrlTcsCtrlInfo.TcsTarPFrntRi);
    Rbc_Ctrl_Read_Rbc_CtrlTcsCtrlInfo_TcsTarPReLe(&Rbc_CtrlBus.Rbc_CtrlTcsCtrlInfo.TcsTarPReLe);
    Rbc_Ctrl_Read_Rbc_CtrlTcsCtrlInfo_TcsTarPReRi(&Rbc_CtrlBus.Rbc_CtrlTcsCtrlInfo.TcsTarPReRi);
    Rbc_Ctrl_Read_Rbc_CtrlTcsCtrlInfo_BothDrvgWhlBrkCtlReqFlg(&Rbc_CtrlBus.Rbc_CtrlTcsCtrlInfo.BothDrvgWhlBrkCtlReqFlg);

    /* Decomposed structure interface */
    Rbc_Ctrl_Read_Rbc_CtrlCircPFildInfo_PrimCircPFild_1_100Bar(&Rbc_CtrlBus.Rbc_CtrlCircPFildInfo.PrimCircPFild_1_100Bar);
    Rbc_Ctrl_Read_Rbc_CtrlCircPFildInfo_SecdCircPFild_1_100Bar(&Rbc_CtrlBus.Rbc_CtrlCircPFildInfo.SecdCircPFild_1_100Bar);

    /* Decomposed structure interface */
    Rbc_Ctrl_Read_Rbc_CtrlPedlTrvlFildInfo_PdtFild(&Rbc_CtrlBus.Rbc_CtrlPedlTrvlFildInfo.PdtFild);

    /* Decomposed structure interface */
    Rbc_Ctrl_Read_Rbc_CtrlPedlTrvlRateInfo_PedlTrvlRateMilliMtrPerSec(&Rbc_CtrlBus.Rbc_CtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec);

    /* Decomposed structure interface */
    Rbc_Ctrl_Read_Rbc_CtrlPistPFildInfo_PistPFild_1_100Bar(&Rbc_CtrlBus.Rbc_CtrlPistPFildInfo.PistPFild_1_100Bar);

    /* Decomposed structure interface */
    Rbc_Ctrl_Read_Rbc_CtrlEemFailData_Eem_Fail_WssFL(&Rbc_CtrlBus.Rbc_CtrlEemFailData.Eem_Fail_WssFL);
    Rbc_Ctrl_Read_Rbc_CtrlEemFailData_Eem_Fail_WssFR(&Rbc_CtrlBus.Rbc_CtrlEemFailData.Eem_Fail_WssFR);
    Rbc_Ctrl_Read_Rbc_CtrlEemFailData_Eem_Fail_WssRL(&Rbc_CtrlBus.Rbc_CtrlEemFailData.Eem_Fail_WssRL);
    Rbc_Ctrl_Read_Rbc_CtrlEemFailData_Eem_Fail_WssRR(&Rbc_CtrlBus.Rbc_CtrlEemFailData.Eem_Fail_WssRR);

    /* Decomposed structure interface */
    Rbc_Ctrl_Read_Rbc_CtrlEemSuspectData_Eem_Suspect_WssFL(&Rbc_CtrlBus.Rbc_CtrlEemSuspectData.Eem_Suspect_WssFL);
    Rbc_Ctrl_Read_Rbc_CtrlEemSuspectData_Eem_Suspect_WssFR(&Rbc_CtrlBus.Rbc_CtrlEemSuspectData.Eem_Suspect_WssFR);
    Rbc_Ctrl_Read_Rbc_CtrlEemSuspectData_Eem_Suspect_WssRL(&Rbc_CtrlBus.Rbc_CtrlEemSuspectData.Eem_Suspect_WssRL);
    Rbc_Ctrl_Read_Rbc_CtrlEemSuspectData_Eem_Suspect_WssRR(&Rbc_CtrlBus.Rbc_CtrlEemSuspectData.Eem_Suspect_WssRR);

    Rbc_Ctrl_Read_Rbc_CtrlEcuModeSts(&Rbc_CtrlBus.Rbc_CtrlEcuModeSts);
    Rbc_Ctrl_Read_Rbc_CtrlIgnOnOffSts(&Rbc_CtrlBus.Rbc_CtrlIgnOnOffSts);
    Rbc_Ctrl_Read_Rbc_CtrlPCtrlAct(&Rbc_CtrlBus.Rbc_CtrlPCtrlAct);
    Rbc_Ctrl_Read_Rbc_CtrlFuncInhibitRbcSts(&Rbc_CtrlBus.Rbc_CtrlFuncInhibitRbcSts);
    Rbc_Ctrl_Read_Rbc_CtrlAy(&Rbc_CtrlBus.Rbc_CtrlAy);
    Rbc_Ctrl_Read_Rbc_CtrlBlsFild(&Rbc_CtrlBus.Rbc_CtrlBlsFild);
    Rbc_Ctrl_Read_Rbc_CtrlPedlTrvlFinal(&Rbc_CtrlBus.Rbc_CtrlPedlTrvlFinal);
    Rbc_Ctrl_Read_Rbc_CtrlTarPFromBaseBrkCtrlr(&Rbc_CtrlBus.Rbc_CtrlTarPFromBaseBrkCtrlr);
    Rbc_Ctrl_Read_Rbc_CtrlTarPRateFromBaseBrkCtrlr(&Rbc_CtrlBus.Rbc_CtrlTarPRateFromBaseBrkCtrlr);
    Rbc_Ctrl_Read_Rbc_CtrlTarPFromBrkPedl(&Rbc_CtrlBus.Rbc_CtrlTarPFromBrkPedl);
    Rbc_Ctrl_Read_Rbc_CtrlVehSpdFild(&Rbc_CtrlBus.Rbc_CtrlVehSpdFild);

    /* Process */

	LDRBC_vCallDetection();
	LCRBC_vCallControl();
	Rbc_CtrlBus.Rbc_CtrlRgnBrkCoopWithAbsInfo.FrntSlipDetThdSlip  = S8_FRONT_SLIP_DET_THRES_SLIP;
	Rbc_CtrlBus.Rbc_CtrlRgnBrkCoopWithAbsInfo.FrntSlipDetThdUDiff = S8_FRONT_SLIP_DET_THRES_VDIFF;

	LSRBC_vConvActRgnBrkTqToBrkP();

    /* Output */
    Rbc_Ctrl_Write_Rbc_CtrlTarRgnBrkTqInfo(&Rbc_CtrlBus.Rbc_CtrlTarRgnBrkTqInfo);
    /*==============================================================================
    * Members of structure Rbc_CtrlTarRgnBrkTqInfo 
     : Rbc_CtrlTarRgnBrkTqInfo.TarRgnBrkTq;
     : Rbc_CtrlTarRgnBrkTqInfo.VirtStkDep;
     : Rbc_CtrlTarRgnBrkTqInfo.VirtStkValid;
     : Rbc_CtrlTarRgnBrkTqInfo.EstTotBrkForce;
     : Rbc_CtrlTarRgnBrkTqInfo.EstHydBrkForce;
     : Rbc_CtrlTarRgnBrkTqInfo.EhbStat;
     =============================================================================*/
    
    Rbc_Ctrl_Write_Rbc_CtrlRgnBrkCoopWithAbsInfo(&Rbc_CtrlBus.Rbc_CtrlRgnBrkCoopWithAbsInfo);
    /*==============================================================================
    * Members of structure Rbc_CtrlRgnBrkCoopWithAbsInfo 
     : Rbc_CtrlRgnBrkCoopWithAbsInfo.FrntSlipDetThdSlip;
     : Rbc_CtrlRgnBrkCoopWithAbsInfo.FrntSlipDetThdUDiff;
     =============================================================================*/
    
    Rbc_Ctrl_Write_Rbc_CtrlBrkPRednForBaseBrkCtrlr(&Rbc_CtrlBus.Rbc_CtrlBrkPRednForBaseBrkCtrlr);
    Rbc_Ctrl_Write_Rbc_CtrlRgnBrkCtlrBlendgFlg(&Rbc_CtrlBus.Rbc_CtrlRgnBrkCtlrBlendgFlg);
    Rbc_Ctrl_Write_Rbc_CtrlRgnBrkCtrlrActStFlg(&Rbc_CtrlBus.Rbc_CtrlRgnBrkCtrlrActStFlg);

    Rbc_Ctrl_Timer_Elapsed = STM0_TIM0.U - Rbc_Ctrl_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define RBC_CTRL_STOP_SEC_CODE
#include "Rbc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
