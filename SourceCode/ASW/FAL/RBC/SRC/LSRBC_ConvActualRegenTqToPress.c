/**
 * @defgroup LSRBC_ConvActualRegenTqToPress LSRBC_ConvActualRegenTqToPress
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LSRBC_ConvActualRegenTqToPress.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "LSRBC_ConvActualRegenTqToPress.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/



#define U16_RB_TORQUE_NM_MAX            3000

#define S8_RBC_READY    0
#define S8_RBC_CONTROL  1
#define S8_RBC_FADE_OUT 5

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
    int32_t lrbcu16HCU_Regen_BrkTq_Nm;
    int32_t lcrbcu1ActiveFlg;
    int32_t ldahbs16DriverIntendedTP;
    int32_t lcrbcu1CANSignalInvalid;
    int32_t ldrbcu16PressToTorqRatioTotal;
    int32_t lcrbcs8ControlState;
    int32_t lcrbcs8FadeOutRate;
}GetConvRgnTq2P_t;
/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define RBC_CTRL_START_SEC_CONST_UNSPECIFIED
#include "Rbc_MemMap.h"
/** Constant Section (UNSPECIFIED)**/
/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
#define RBC_CTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Rbc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define RBC_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Rbc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
#define RBC_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Rbc_MemMap.h"



#define RBC_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Rbc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define RBC_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Rbc_MemMap.h"



#define RBC_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Rbc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define RBC_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Rbc_MemMap.h"



#define RBC_CTRL_START_SEC_VAR_32BIT
#include "Rbc_MemMap.h"
/** Variable Section (32BIT)**/
#define RBC_CTRL_STOP_SEC_VAR_32BIT
#include "Rbc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define RBC_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Rbc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
FAL_STATIC GetConvRgnTq2P_t GetConvRgnTq2P;
#define RBC_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Rbc_MemMap.h"



#define RBC_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Rbc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define RBC_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Rbc_MemMap.h"



#define RBC_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Rbc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
FAL_STATIC int16_t lsrbcs16HydPressReduceReq = 0;
FAL_STATIC int16_t lsrbcs16HydPressReduceReqOld = 0;

FAL_STATIC GetConvRgnTq2P_t GetConvRgnTq2P;

#define RBC_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Rbc_MemMap.h"



#define RBC_CTRL_START_SEC_VAR_32BIT
#include "Rbc_MemMap.h"
/** Variable Section (32BIT)**/
#define RBC_CTRL_STOP_SEC_VAR_32BIT
#include "Rbc_MemMap.h"

/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define RBC_CTRL_START_SEC_CODE
#include "Rbc_MemMap.h"

static void LSRBC_vConvActualRegenTqToPress(void);
static void LSRBC_vLimitRealTqAftRBCOff(int16_t s16RbTqPressDecMin);
static void LSRBC_vOutputRegenBrk(void);
static void LSRBC_vInputRegenBrk(void);
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void LSRBC_vConvActRgnBrkTqToBrkP(void)
{
    LSRBC_vInputRegenBrk();
    
	LSRBC_vConvActualRegenTqToPress();

	LSRBC_vOutputRegenBrk();
    
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static void LSRBC_vInputRegenBrk(void)
{
    GetConvRgnTq2P.lrbcu16HCU_Regen_BrkTq_Nm                     = Rbc_CtrlBus.Rbc_CtrlCanRxRegenInfo.HcuRegenBrkTq;
    GetConvRgnTq2P.lcrbcu1ActiveFlg                              = Rbc_CtrlBus.Rbc_CtrlRgnBrkCtrlrActStFlg;
    GetConvRgnTq2P.ldahbs16DriverIntendedTP                      = Rbc_CtrlBus.Rbc_CtrlTarPFromBrkPedl;
    GetConvRgnTq2P.lcrbcu1CANSignalInvalid                       = RBCsrcbus.CANSignalInvalid;
    GetConvRgnTq2P.ldrbcu16PressToTorqRatioTotal                 = RBCsrcbus.PressToTorqRatioTotal;
    GetConvRgnTq2P.lcrbcs8ControlState                           = RBCsrcbus.ControlState;
    GetConvRgnTq2P.lcrbcs8FadeOutRate                            = RBCsrcbus.FadeOutRate;
}

static void LSRBC_vConvActualRegenTqToPress(void)
{

    int16_t s16RbTqPressIncLimit;
    int16_t s16RbTqPressDecLimit;
    int16_t s16MaxTPReduction;

    /* Convert Actual Regen Torque -> Pressure */

    lsrbcs16HydPressReduceReqOld = lsrbcs16HydPressReduceReq;
    
    
    if(GetConvRgnTq2P.lcrbcu1CANSignalInvalid==0)
    {
        if(GetConvRgnTq2P.ldrbcu16PressToTorqRatioTotal==0)
        {
            lsrbcs16HydPressReduceReq =0;
        }

        else if(GetConvRgnTq2P.lrbcu16HCU_Regen_BrkTq_Nm <= U16_RB_TORQUE_NM_MAX)
        {
            lsrbcs16HydPressReduceReq = (int16_t)(((int32_t)GetConvRgnTq2P.lrbcu16HCU_Regen_BrkTq_Nm*S16_PCTRL_PRESS_1_BAR)/GetConvRgnTq2P.ldrbcu16PressToTorqRatioTotal);
        }
        else
        {
            lsrbcs16HydPressReduceReq = (int16_t)(((int32_t)U16_RB_TORQUE_NM_MAX*S16_PCTRL_PRESS_1_BAR)/GetConvRgnTq2P.ldrbcu16PressToTorqRatioTotal);
        }

        s16RbTqPressIncLimit=S16_MAX_RBPRESS_INC_RATE;
         
        if(GetConvRgnTq2P.lcrbcu1ActiveFlg == 0)
	    {
	        s16RbTqPressDecLimit=S16_MAX_RBPRESS_DEC_RATE_FAIL;
	        s16RbTqPressIncLimit= S16_PCTRL_PRESS_1_BAR;
	    }
	    else if((GetConvRgnTq2P.lcrbcu1ActiveFlg==1) && (GetConvRgnTq2P.lcrbcs8ControlState==S8_RBC_FADE_OUT))
	    {
	        if(GetConvRgnTq2P.lcrbcs8FadeOutRate >=S16_MAX_RBPRESS_DEC_RATE)
	        {
	            s16RbTqPressDecLimit =(int16_t)GetConvRgnTq2P.lcrbcs8FadeOutRate;
	        }
	        else
	        {
	            s16RbTqPressDecLimit=S16_MAX_RBPRESS_DEC_RATE;
	        }        
	    }
	    else
	    {
	        s16RbTqPressDecLimit=S16_MAX_RBPRESS_DEC_RATE;
	    }      

    }
    else
    {
        lsrbcs16HydPressReduceReq = S16_PCTRL_PRESS_0_BAR;

        s16RbTqPressIncLimit=0;
        s16RbTqPressDecLimit=(int16_t)S16_MAX_RBPRESS_DEC_RATE_FAIL;
    }


    /* Limit Inc / Dec rate */

    if((lsrbcs16HydPressReduceReq-lsrbcs16HydPressReduceReqOld) > (s16RbTqPressIncLimit))
    {
        lsrbcs16HydPressReduceReq = lsrbcs16HydPressReduceReqOld + s16RbTqPressIncLimit;
    }
    else if((lsrbcs16HydPressReduceReq-lsrbcs16HydPressReduceReqOld) < (-s16RbTqPressDecLimit))
    {
        lsrbcs16HydPressReduceReq = lsrbcs16HydPressReduceReqOld - s16RbTqPressDecLimit;
    }
    else { }
    
    LSRBC_vLimitRealTqAftRBCOff(S16_MAX_RBPRESS_DEC_RATE_FAIL);

//	if (lsrbcs16HydPressReduceReq > S16_MPRESS_0_BAR)
    if (lsrbcs16HydPressReduceReq > S16_PCTRL_PRESS_0_BAR)
	{
//		s16MaxTPReduction = L_s16LimitMinMax((GetConvRgnTq2P.ldahbs16DriverIntendedTP*10 - (int16_t)(S16_PRESS_RESOL_10_TO_100(U8_TAR_WL_PRESS_MIN_1))), S16_PCTRL_PRESS_0_BAR, ((int16_t)(S16_PRESS_RESOL_10_TO_100(U8_MAX_RB_TQ_PRESS_LIMIT)) * S16_PCTRL_PRESS_1_BAR));
		s16MaxTPReduction = L_s16LimitMinMax((GetConvRgnTq2P.ldahbs16DriverIntendedTP - ((int16_t) U8_TAR_WL_PRESS_MIN_1)), S16_PCTRL_PRESS_0_BAR, ((int16_t)((U8_MAX_RB_TQ_PRESS_LIMIT)) * S16_PCTRL_PRESS_1_BAR));

	}
	else
	{
		s16MaxTPReduction = S16_PCTRL_PRESS_0_BAR;
	}

	lsrbcs16HydPressReduceReq = L_s16LimitMinMax((int16_t)lsrbcs16HydPressReduceReq, S16_PCTRL_PRESS_0_BAR, s16MaxTPReduction);


}

static void LSRBC_vLimitRealTqAftRBCOff(int16_t s16RbTqPressDecMin)
{
  	static uint8_t lsrbcu8RBCRealTqEndCnt=0;



  	if((GetConvRgnTq2P.lcrbcu1ActiveFlg==0) && (lsrbcs16HydPressReduceReq>0))
	{
	    if(lsrbcu8RBCRealTqEndCnt < U8_T_500_MS)
	    {
	        lsrbcu8RBCRealTqEndCnt = lsrbcu8RBCRealTqEndCnt + 1;
	    }
	    else{ }
	}
    else 
    {
        lsrbcu8RBCRealTqEndCnt = U8_T_0_MS;
    }


    if(lsrbcu8RBCRealTqEndCnt >= U8_T_500_MS)
    {
		if((lsrbcs16HydPressReduceReqOld-lsrbcs16HydPressReduceReq) < s16RbTqPressDecMin)
		{
    		lsrbcs16HydPressReduceReq = lsrbcs16HydPressReduceReqOld - s16RbTqPressDecMin;
		}
		else { }
    
    	if(lsrbcs16HydPressReduceReq <= 0)
    	{
    		lsrbcs16HydPressReduceReq = 0;
    	}
    	else { }
    }
    else { }



}


static void LSRBC_vOutputRegenBrk(void)
{

	Rbc_CtrlBus.Rbc_CtrlBrkPRednForBaseBrkCtrlr = lsrbcs16HydPressReduceReq;

}


#define RBC_CTRL_STOP_SEC_CODE
#include "Rbc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
