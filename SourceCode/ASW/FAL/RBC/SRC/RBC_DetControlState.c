/**
 * @defgroup RBC_DetControlState RBC_DetControlState
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        RBC_DetControlState.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "RBC_DetControlState.h"
#include "L_CommonFunction.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
/* Global bus*/



/* output variable*/

#define U8_RB_ENABLE_GEAR_1 5   /*D*/
#define U8_RB_ENABLE_GEAR_2 8   /*Sport / Manual*/
#define U8_RB_ENABLE_GEAR_3 255 /* Any additional condition */


#define S16_RB_EXIT_PRESS_THRES           S16_PCTRL_PRESS_40_BAR//S16_MPRESS_40_BAR
#define U8_RBC_EXIT_WAIT_TIME             U8_T_50_MS

#define S16_PEDAL_TRAVEL_MAX    		  S16_TRAVEL_150_MM /* Cal parameter --> RTE input?? */
#define S16_FAST_PEDAL_APPLY_THRES        S16_TRAVEL_30_MM  /* Cal parameter --> RTE input?? */

#define U8_PRES_CTRL_L1                       1
#define U8_PRES_CTRL_L2                       2
#define U8_PRES_CTRL_L3                       3

#define     __RBC_ONSOAKING                   ENABLE

  #if __HMC_CAN_VER==CAN_EV
#define     __RBC_CHK_MOTOR_RPM               ENABLE
  #elif __HMC_CAN_VER==CAN_FCEV
#define     __RBC_CHK_MOTOR_RPM               DISABLE
  #else
#define     __RBC_CHK_MOTOR_RPM               DISABLE
  #endif


  #if (__SCC == ENABLE)
#define 	__SCC_RBC_ENABLE				ENABLE
#define		__SCC_VIRTUAL_PEDAL_CALC		ENABLE
  #else
#define 	__SCC_RBC_ENABLE				DISABLE
#define		__SCC_VIRTUAL_PEDAL_CALC		DISABLE
  #endif

#define     __CORNER_BRAKE_RBC_INHIBIT      ENABLE
#define     __INHIBIT_RBC_RE_ENTRY          ENABLE


/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
    int16_t s16VehicleSpeed;
    int16_t ldahbs16DriverIntendedTP;

    int16_t ldahbs16TargetPressRate10ms    ;
    int16_t lsahbs16PedalTravelFilt        ;
    int16_t ldahbs16PedalTravelRateMmPSec  ;
    int16_t lsahbs16PDTFilt                ;
    int32_t lcahbu1AHBOn                   ;
    int32_t liu1AbsAct                     ;
    int32_t liu1AbsDef                     ;
    int32_t liu1TcsCtl                     ;
    int32_t liu1TcsDef                     ;
    int32_t liu1EscCtl                     ;
    int32_t liu1EscDef                     ;
    int32_t liu1SccDef                     ;
    int32_t liu1SccCtl                     ;
    int32_t lis16SccTP                     ;
    int32_t liu1FrontWheelSlipDec          ;
    int16_t lis16LateralAcc                ;
    int32_t liu1cEMSAccPedDep_Pc_Err_Flg   ;
    int16_t lrs16AccelPosition             ;
    uint8_t lsrbcu8GsSel                   ;
    int32_t liu1fTcu1MsgTimeOutError       ;
    int16_t lsrbcs16CoolantTemp            ;
    int32_t lsrbcu1CoolantTempEMS3_ERR     ;
    int16_t lsrbcs16OutTempSnsrCIngOn      ;
    int32_t lsrbcu1OutTempSnsrErrFlg       ;
    int32_t lrbcu1HCU_CF_Hcu_ServiceMod    ;
    uint16_t lrbcu16HCU_Regen_BrkTq_Nm      ;
    int32_t ldrbcu1ActiveFlg               ;

    int32_t lcrbcu1CANSignalInvalid;
    uint16_t ldrbcu16TargetRBTqNm;

    int32_t lcrbcu1MaxSpeedInhibitFlg;
    int32_t lcrbcu1ExitCmdRengenTorq;
    int32_t lcrbcu1SignalValidAftIgnOn;
    int16_t lcrbcs16RegenLowSpeedLimit;
    int16_t ldahbs16AvhActiveBrkTP;
}GetDetRbcSt_t;

FAL_STATIC uint8_t ldrbcu8DriverBrakeIntendCnt = 0;

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define RBC_CTRL_START_SEC_CONST_UNSPECIFIED
#include "Rbc_MemMap.h"
/** Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/

#define RBC_CTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Rbc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define RBC_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Rbc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
#define RBC_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Rbc_MemMap.h"



#define RBC_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Rbc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define RBC_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Rbc_MemMap.h"



#define RBC_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Rbc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define RBC_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Rbc_MemMap.h"



#define RBC_CTRL_START_SEC_VAR_32BIT
#include "Rbc_MemMap.h"
/** Variable Section (32BIT)**/
#define RBC_CTRL_STOP_SEC_VAR_32BIT
#include "Rbc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/

#define RBC_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Rbc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


FAL_STATIC int16_t ldrbcs16DrvIntendedBrkP5msDiff ;
FAL_STATIC int16_t ldrbcs16DrvIntendedBrkPOld     ;

FAL_STATIC uint16_t ldrbcu16DrvIntendedBrkTqNm;
FAL_STATIC int16_t ldrbcs16PedalTravelFiltOld   ;
FAL_STATIC int16_t ldrbcs16PedalTravelFilt      ;


FAL_STATIC uint8_t  lru8MuSpeedCompFactor;
FAL_STATIC uint16_t  lru16MuCompFactor;
FAL_STATIC uint16_t lru16MuBuildFactor;

FAL_STATIC int16_t ldrbcs16RegenTorqRate;
FAL_STATIC int16_t ldrbcs16PedalTravelFilt5msDiff;

FAL_STATIC int16_t ldrbcs16TotalBrakeTP;
FAL_STATIC uint16_t ldrbcu16TotalBrakeTqNm;
FAL_STATIC int16_t ldrbcs16VirtualPDT;

FAL_STATIC int16_t ldrbcs16SccActiveBrkTP = 0;

FAL_STATIC int32_t ldrbcu1RBCBlendingFlag ;
FAL_STATIC int32_t ldrbcu1RegenEnableGearInput;
FAL_STATIC int32_t ldrbcu1RegenGearSelectedOld;
FAL_STATIC int32_t ldrbcu1EBS2_CF_Brk_VirtStkValid;
FAL_STATIC int32_t ldrbcu1ONSTimeCntAfIngOnFlg;
FAL_STATIC int32_t ldrbcu1ONSoakingTempFlg;
FAL_STATIC int32_t ldrbcu1RbcSccBlendFlag;
FAL_STATIC int32_t ldrbcu1ONSBrakeTimeCntFlg;
FAL_STATIC int32_t  ldrbcu1BrakeCntDuringONSFlg;
FAL_STATIC int32_t  ldrbcu1OverNightSoakingFlg;
FAL_STATIC int32_t  ldrbcu1RBCStartFlag;
/////


FAL_STATIC int16_t ldrbcs16VehicleSpeed;
FAL_STATIC int32_t ldrbcu1DriverIntendToBrake;
FAL_STATIC int32_t ldrbcu1DriverIntendToAccel;
FAL_STATIC int32_t ldrbcu1RegenEnableGearSelected;
FAL_STATIC int32_t ldrbcu1MotorRpmOKInDGearFlg;
FAL_STATIC int32_t ldrbcu1InhibitRegenReEntryFlg;
FAL_STATIC int32_t ldrbcu1SafetyFuctionActFlg;
FAL_STATIC int32_t ldrbcu1FrontWheelSlipDec;
FAL_STATIC int32_t ldrbcu1FastTurnFlg;
FAL_STATIC int32_t ldrbcu1FastDecelApplyFlg;

FAL_STATIC int32_t ldrbcu1RbcSccReadyActFlgOld;
FAL_STATIC int32_t ldrbcu1SccFuctionActFlg;
FAL_STATIC int32_t ldrbcu1SccFuctionActOldFlg;
FAL_STATIC int32_t ldrbcu1RbcSccReadyActFlg;
FAL_STATIC uint16_t ldrbcu16PressToTorqRatioTotal;
FAL_STATIC int16_t ldrbcs16DrvIntendedBrkP;
FAL_STATIC int32_t ldrbcu1RbcSccRegenActFlg;
FAL_STATIC int32_t ldrbcu1SccBrakeIntendFlg;


FAL_STATIC GetDetRbcSt_t GetDetRbcSt;
#define RBC_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Rbc_MemMap.h"



#define RBC_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Rbc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define RBC_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Rbc_MemMap.h"



#define RBC_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Rbc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define RBC_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Rbc_MemMap.h"



#define RBC_CTRL_START_SEC_VAR_32BIT
#include "Rbc_MemMap.h"
/** Variable Section (32BIT)**/
#define RBC_CTRL_STOP_SEC_VAR_32BIT
#include "Rbc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define RBC_CTRL_START_SEC_CODE
#include "Rbc_MemMap.h"

static void LDRBC_vCalTqPressConvRatio(void);

static void LDRBC_vDctDriverIntention(void);
static void LDRBC_vDctBrkDriverIntention(void);

#if (__SCC_RBC_ENABLE == ENABLE)
static void LDRBC_vDctSccBrkIntention(void);
#endif

static void LDRBC_vDctAccelDriverIntention(void);
static void LDRBC_vDctGearDriverIntention(void);
static void LDRBC_vDctFastDecelApply(void);

     #if __CORNER_BRAKE_RBC_INHIBIT==ENABLE
static void LDRBC_vDctLimitLeteralAcc(void);
    #endif
static void LDRBC_vDctVehicleStatus(void);
static void LDRBC_vChkSafetyFuctionAct(void);

static void LDRBC_vDctONSoaking(void);
static void LDRBC_vChkMotorRpm(void);

static void LDRBC_vDctStartRegenPress(void);
static void LDRBC_vDctExitRegenPress(void);
static void LDRBC_vCalcRegenTorqRate(void);

static void LDRBC_vDctSCCReadyAct(void);

static void LDRBC_vInhibitRegenReEntry(void);

#if (__ESTIMATE_TOTAL_BRAKE_TQ == ENABLE)
static int16_t LDRBC_s16GetTotalBrkPress(void);
#endif

static int16_t LDRBC_s16DctVirtualPDT(int16_t s16BrakeTP);
  #if (__SCC_VIRTUAL_PEDAL_CALC == ENABLE)
static int16_t LDRBC_s16GetPedalTravelFromTP(int16_t s16TargetPressure);
  #endif /* #if (__SCC_VIRTUAL_PEDAL_CALC == ENABLE) */

static void LDRBC_vChkActiveFuctionAct(void);

static int16_t LDRBC_s16DctActiveBrkTargetPres(uint8_t u8PresCtrlAllowLevel, uint8_t u8DefectFlg, uint8_t u8CtrlFlg, int16_t s16ActiveTPRequest, int16_t s16ActiveTPOld);

static void LDRBC_vOutPutDetection(void);
static void LDRBC_vInPutDetection(void);
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

void LDRBC_vCallDetection(void)
{
    LDRBC_vInPutDetection();

    LDRBC_vDctVehicleStatus();

    LDRBC_vDctSCCReadyAct();

    LDRBC_vDctDriverIntention();
   
    LDRBC_vDctFastDecelApply();
	
    #if __CORNER_BRAKE_RBC_INHIBIT==ENABLE 
    LDRBC_vDctLimitLeteralAcc();
    #endif
    
    LDRBC_vChkSafetyFuctionAct();
    
    LDRBC_vChkActiveFuctionAct();
    
	LDRBC_vInhibitRegenReEntry();

    LDRBC_vChkMotorRpm();

    LDRBC_vDctONSoaking(); 

    LDRBC_vCalTqPressConvRatio();
    
    LDRBC_vCalcRegenTorqRate();
    
    LDRBC_vDctStartRegenPress();
    
    LDRBC_vDctExitRegenPress();
  #if (__ESTIMATE_TOTAL_BRAKE_TQ == ENABLE)
	ldrbcs16VirtualPDT = LDRBC_s16DctVirtualPDT(ldrbcs16TotalBrakeTP);
  #else
	ldrbcs16VirtualPDT = LDRBC_s16DctVirtualPDT(ldrbcs16DrvIntendedBrkP);
  #endif

	LDRBC_vOutPutDetection();

}


/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static void LDRBC_vInPutDetection(void)
{
	(ldrbcu8DriverBrakeIntendCnt>2)? (ldrbcu8DriverBrakeIntendCnt - 2):0; 
	
        GetDetRbcSt.s16VehicleSpeed                          =(int16_t)Rbc_CtrlBus.Rbc_CtrlVehSpdFild;
        GetDetRbcSt.ldahbs16DriverIntendedTP                 =(int16_t)Rbc_CtrlBus.Rbc_CtrlTarPFromBrkPedl;
        GetDetRbcSt.ldahbs16AvhActiveBrkTP					 = Rbc_CtrlBus.Rbc_CtrlAvhCtrlInfo.AvhTarP;

     GetDetRbcSt.ldahbs16TargetPressRate10ms                 =(int16_t)Rbc_CtrlBus.Rbc_CtrlTarPRateFromBaseBrkCtrlr;
     GetDetRbcSt.lsahbs16PedalTravelFilt                     =(int16_t)Rbc_CtrlBus.Rbc_CtrlPedlTrvlFinal;
     GetDetRbcSt.ldahbs16PedalTravelRateMmPSec               =(int16_t)Rbc_CtrlBus.Rbc_CtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec;
     GetDetRbcSt.lsahbs16PDTFilt                             =(int16_t)Rbc_CtrlBus.Rbc_CtrlPedlTrvlFildInfo.PdtFild;
     GetDetRbcSt.lcahbu1AHBOn                                = Rbc_CtrlBus.Rbc_CtrlPCtrlAct;
     GetDetRbcSt.liu1AbsAct                                  = (Rbc_CtrlBus.Rbc_CtrlAbsCtrlInfo.AbsActFlg>0)?1:0;
     GetDetRbcSt.liu1AbsDef                                  = Rbc_CtrlBus.Rbc_CtrlAbsCtrlInfo.AbsDefectFlg;
     GetDetRbcSt.liu1TcsCtl                                  = (Rbc_CtrlBus.Rbc_CtrlTcsCtrlInfo.TcsActFlg>0)?1:0;
     GetDetRbcSt.liu1TcsDef                                  = Rbc_CtrlBus.Rbc_CtrlTcsCtrlInfo.TcsDefectFlg;
     GetDetRbcSt.liu1EscCtl                                  = (Rbc_CtrlBus.Rbc_CtrlEscCtrlInfo.EscActFlg>0)?1:0;
     GetDetRbcSt.liu1EscDef                                  = Rbc_CtrlBus.Rbc_CtrlEscCtrlInfo.EscDefectFlg;
     GetDetRbcSt.liu1SccDef                                  = Rbc_CtrlBus.Rbc_CtrlSccCtrlInfo.SccDefectFlg;
     GetDetRbcSt.liu1SccCtl                                  = Rbc_CtrlBus.Rbc_CtrlSccCtrlInfo.SccActFlg;
     GetDetRbcSt.lis16SccTP                                  = (int16_t)Rbc_CtrlBus.Rbc_CtrlSccCtrlInfo.SccTarP;
     GetDetRbcSt.liu1FrontWheelSlipDec                       = Rbc_CtrlBus.Rbc_CtrlAbsCtrlInfo.FrntWhlSlip;
     GetDetRbcSt.lis16LateralAcc                             = (int16_t)Rbc_CtrlBus.Rbc_CtrlAy;
     GetDetRbcSt.liu1cEMSAccPedDep_Pc_Err_Flg                = Rbc_CtrlBus.Rbc_CtrlCanRxAccelPedlInfo.AccelPedlValErr;
     GetDetRbcSt.lrs16AccelPosition                          = (int16_t)Rbc_CtrlBus.Rbc_CtrlCanRxAccelPedlInfo.AccelPedlVal;
     GetDetRbcSt.lsrbcu8GsSel                                = (uint8_t)Rbc_CtrlBus.Rbc_CtrlCanRxIdbInfo.GearSelDisp;
//     GetDetRbcSt.liu1fTcu1MsgTimeOutError                    = RBCbus.CANIfInfo.GearStInvldFlg; KYB
     GetDetRbcSt.lsrbcs16CoolantTemp                         = (int16_t)Rbc_CtrlBus.Rbc_CtrlCanRxIdbInfo.CoolantTemp;
     GetDetRbcSt.lsrbcu1CoolantTempEMS3_ERR                  = Rbc_CtrlBus.Rbc_CtrlCanRxIdbInfo.CoolantTempErr;
     GetDetRbcSt.lsrbcs16OutTempSnsrCIngOn                   = (int16_t)Rbc_CtrlBus.Rbc_CtrlCanRxEngTempInfo.EngTemp;
     GetDetRbcSt.lsrbcu1OutTempSnsrErrFlg                    = Rbc_CtrlBus.Rbc_CtrlCanRxEngTempInfo.EngTempErr;
     GetDetRbcSt.lrbcu1HCU_CF_Hcu_ServiceMod                 = Rbc_CtrlBus.Rbc_CtrlCanRxIdbInfo.HcuServiceMod;
     GetDetRbcSt.lrbcu16HCU_Regen_BrkTq_Nm                   =(uint16_t) Rbc_CtrlBus.Rbc_CtrlCanRxRegenInfo.HcuRegenBrkTq;
     GetDetRbcSt.ldrbcu1ActiveFlg                            = Rbc_CtrlBus.Rbc_CtrlRgnBrkCtrlrActStFlg;

      GetDetRbcSt.ldrbcu16TargetRBTqNm                          =(uint16_t)Rbc_CtrlBus.Rbc_CtrlTarRgnBrkTqInfo.TarRgnBrkTq;
      GetDetRbcSt.lcrbcu1CANSignalInvalid                       =RBCsrcbus.CANSignalInvalid;

     GetDetRbcSt.lcrbcu1ExitCmdRengenTorq                      =RBCsrcbus.ExitCmdRengenTorq;
     GetDetRbcSt.lcrbcu1SignalValidAftIgnOn                    =RBCsrcbus.SignalValidAftIgnOn;
     GetDetRbcSt.lcrbcs16RegenLowSpeedLimit                     =(int16_t)RBCsrcbus.RegenLowSpeedLimit;




}



static void LDRBC_vCalTqPressConvRatio(void)
{
	  #if __RBC_PRESS_TO_TQ_COMP
	LDRBC_vEstPressToTQCompensation();  
	ldrbcu16PressToTorqRatioTotal = ((uint16_t)U8_R_PRESS_VS_TQ_RATIO_F + U8_R_PRESS_VS_TQ_RATIO_R) + (((uint16_t)lru8MuSpeedCompFactor + lru16MuCompFactor)/10); /* resolution 1Nm */

	  #else /*__RBC_PRESS_TO_TQ_COMP*/
    ldrbcu16PressToTorqRatioTotal = (uint16_t)(U8_R_PRESS_VS_TQ_RATIO_F + U8_R_PRESS_VS_TQ_RATIO_R); /* resolution 1Nm */
      #endif /*__RBC_PRESS_TO_TQ_COMP*/     
      
    if(ldrbcu16PressToTorqRatioTotal<=0)
    {
        ldrbcu16PressToTorqRatioTotal = 1;
    }

}

    
static void LDRBC_vDctDriverIntention(void)
{
    ldrbcs16DrvIntendedBrkPOld = ldrbcs16DrvIntendedBrkP;
    ldrbcs16PedalTravelFiltOld = ldrbcs16PedalTravelFilt;
    
    ldrbcs16SccActiveBrkTP    = LDRBC_s16DctActiveBrkTargetPres(U8_PRES_CTRL_L2, (uint8_t)(GetDetRbcSt.liu1SccDef), (uint8_t)(GetDetRbcSt.liu1SccCtl), GetDetRbcSt.lis16SccTP,  ldrbcs16SccActiveBrkTP );

    if(ldrbcu1RbcSccReadyActFlg==1)
	{
		if(GetDetRbcSt.ldahbs16DriverIntendedTP <= ldrbcs16SccActiveBrkTP)
		{
			ldrbcs16DrvIntendedBrkP = ldrbcs16SccActiveBrkTP;
		}
		else 
		{
			ldrbcs16DrvIntendedBrkP = GetDetRbcSt.ldahbs16DriverIntendedTP;
		}
	}
	else 
	{
		if(GetDetRbcSt.ldahbs16DriverIntendedTP > 0)
		{
			ldrbcs16DrvIntendedBrkP = (int16_t)GetDetRbcSt.ldahbs16DriverIntendedTP;
		}
		else
		{
			ldrbcs16DrvIntendedBrkP = 0;
		}
	}

	ldrbcs16DrvIntendedBrkP5msDiff = ldrbcs16DrvIntendedBrkP - ldrbcs16DrvIntendedBrkPOld;

	if(GetDetRbcSt.lsahbs16PedalTravelFilt > 0)
	{
		ldrbcs16PedalTravelFilt = GetDetRbcSt.lsahbs16PedalTravelFilt;
	}
	else
	{
		ldrbcs16PedalTravelFilt = 0;
	}

	ldrbcs16PedalTravelFilt5msDiff = ldrbcs16PedalTravelFilt - ldrbcs16PedalTravelFiltOld;

  #if (__ESTIMATE_TOTAL_BRAKE_TQ == ENABLE)
	ldrbcs16TotalBrakeTP = LDRBC_s16GetTotalBrkPress();
//	ldrbcu16TotalBrakeTqNm = (uint16_t)(((uint32_t)ldrbcs16TotalBrakeTP * ldrbcu16PressToTorqRatioTotal)/S16_MPRESS_1_BAR); /* Sent to CAN */
	ldrbcu16TotalBrakeTqNm = (uint16_t)(((uint32_t)ldrbcs16TotalBrakeTP * ldrbcu16PressToTorqRatioTotal)/S16_PCTRL_PRESS_1_BAR); /* Sent to CAN */
  #else		/* (__ESTIMATE_TOTAL_BRAKE_TQ == DISABLE) */
//    ldrbcu16DrvIntendedBrkTqNm = (uint16_t)(((uint32_t)ldrbcs16DrvIntendedBrkP * ldrbcu16PressToTorqRatioTotal)/S16_MPRESS_1_BAR); /* Sent to CAN */
	ldrbcu16DrvIntendedBrkTqNm = (uint16_t)(((uint32_t)ldrbcs16DrvIntendedBrkP * ldrbcu16PressToTorqRatioTotal)/S16_PCTRL_PRESS_1_BAR); /* Sent to CAN */

  #endif	/* (__ESTIMATE_TOTAL_BRAKE_TQ == ENABLE) */
    

    /*
    static uint16_t ldrbcu8DrvIntBrkBuffCnt;
    
       // 0.1 bar/20ms -> 4bar/sec
    
    ldrbcs16DrvIntendedBrkPBuff[ldrbcu8DrvIntBrkBuffCnt] = ldrbcs16DrvIntendedBrkP;

    ldrbcu8DrvIntBrkBuffCnt = ldrbcu8DrvIntBrkBuffCnt + 1;
    if(ldrbcu8DrvIntBrkBuffCnt>=5) 
    {
        ldrbcu8DrvIntBrkBuffCnt = 0;
    }
    else
    {
        ;
    }

    ldrbcs16DrvIntendedBrkPRateBar = ((ldrbcs16DrvIntendedBrkP - ldrbcs16DrvIntendedBrkPBuff[ldrbcu8DrvIntBrkBuffCnt])*5); //resolution 1bar/sec
    */
    
        
    /* Dct Driver Intention to Brake */
	LDRBC_vDctBrkDriverIntention();

    /* Dct SCC Brake Intention  */	  
	  #if (__SCC_RBC_ENABLE == ENABLE)
	LDRBC_vDctSccBrkIntention();
	
	if(ldrbcu1SccBrakeIntendFlg == 1)
	{
	    ldrbcu1DriverIntendToBrake = 1;
	}
	else { }
	  #endif

    /* Dct Driver Intention to Accel */
	LDRBC_vDctAccelDriverIntention();
    /* Later to add Slope Condition, Fail Conditions */

    /* Dct Driver Gear selection */
	LDRBC_vDctGearDriverIntention();
}

/*******************************************************************************
* FUNCTION NAME:		LDRBC_vDctBrkDriverIntention
* CALLED BY:			LDRBC_vDctDriverIntention()
* Preconditions:		none
* PARAMETER:			none
* RETURN VALUE:			none
* Description:			Dct Driver Intention to Brake
********************************************************************************/

static void LDRBC_vDctBrkDriverIntention(void)
{

    if(ldrbcu1DriverIntendToBrake == 0)
    {
        if(ldrbcs16DrvIntendedBrkP >= (int16_t)U16_R_MBO_PUSH_SET)
        {
//            if((ldrbcs16DrvIntendedBrkP5msDiff > S16_MPRESS_0_BAR) || (ldrbcs16PedalTravelFilt5msDiff > S16_TRAVEL_0_MM))
        	if((ldrbcs16DrvIntendedBrkP5msDiff > S16_PCTRL_PRESS_0_BAR) || (ldrbcs16PedalTravelFilt5msDiff > S16_TRAVEL_0_MM))
            {
                ldrbcu8DriverBrakeIntendCnt = ldrbcu8DriverBrakeIntendCnt + 1; /* Apply Cnt */
               
//                if((ldrbcs16DrvIntendedBrkP5msDiff >= S16_MPRESS_0_2_BAR) || (ldrbcs16PedalTravelFilt5msDiff >= S16_TRAVEL_0_2_MM))
                if((ldrbcs16DrvIntendedBrkP5msDiff >= S16_PCTRL_PRESS_0_2_BAR) || (ldrbcs16PedalTravelFilt5msDiff >= S16_TRAVEL_0_2_MM))
				{
					ldrbcu8DriverBrakeIntendCnt = ldrbcu8DriverBrakeIntendCnt + 1;	/* Apply Cnt */
				}
				else { }
            }
//            else if((ldrbcs16DrvIntendedBrkP5msDiff < S16_MPRESS_0_BAR) || (ldrbcs16PedalTravelFilt5msDiff < S16_TRAVEL_0_MM))
        	else if((ldrbcs16DrvIntendedBrkP5msDiff < S16_PCTRL_PRESS_0_BAR) || (ldrbcs16PedalTravelFilt5msDiff < S16_TRAVEL_0_MM))
            {
               ldrbcu8DriverBrakeIntendCnt = (ldrbcu8DriverBrakeIntendCnt>2)? (ldrbcu8DriverBrakeIntendCnt - 2):0; /* Apply Cnt */
            }
            else
            {
                ;
            }
        }
        else
        {
            ldrbcu8DriverBrakeIntendCnt = 0;
        }
        
        if(ldrbcu8DriverBrakeIntendCnt >= U8_T_50_MS) /* 10 scan */
        {
            ldrbcu8DriverBrakeIntendCnt = 0;
            ldrbcu1DriverIntendToBrake = 1;
        }
		else { }
    }
    else
    {
        if(ldrbcs16DrvIntendedBrkP < U16_R_MBO_RELEASE_SET)
        {
//            if(ldrbcs16DrvIntendedBrkP5msDiff <= S16_MPRESS_0_BAR)
        	if(ldrbcs16DrvIntendedBrkP5msDiff <= S16_PCTRL_PRESS_0_BAR)
            {
            	  ldrbcu8DriverBrakeIntendCnt = ldrbcu8DriverBrakeIntendCnt + 1; /* Release Cnt */
            }
            else
            {
                ldrbcu8DriverBrakeIntendCnt = (ldrbcu8DriverBrakeIntendCnt>0)? (ldrbcu8DriverBrakeIntendCnt - 1):0; /* Release Cnt */
            }
        }
        else
        {
            ldrbcu8DriverBrakeIntendCnt = 0;
        }

        if(ldrbcu8DriverBrakeIntendCnt >= U8_T_25_MS)
        {
            ldrbcu8DriverBrakeIntendCnt = 0;
            ldrbcu1DriverIntendToBrake = 0;
        }
		else { }
    }
}
    
/*******************************************************************************
* FUNCTION NAME:		LDRBC_vDctSccBrkIntention
* CALLED BY:			LDRBC_vDctDriverIntention()
* Preconditions:		none
* PARAMETER:			none
* RETURN VALUE:			none
* Description:			Dct SCC Brake Intention
********************************************************************************/
#if (__SCC_RBC_ENABLE == ENABLE)
static void LDRBC_vDctSccBrkIntention(void)
{
    static uint8_t ldrbcu8DriverSccBrakeIntendCnt = 0;

    if (ldrbcu1SccBrakeIntendFlg ==0)
    {
        if (ldrbcu1RbcSccReadyActFlg==1)
        {     
            ldrbcu8DriverSccBrakeIntendCnt = ldrbcu8DriverSccBrakeIntendCnt + 3; /* Apply Cnt */
           
//            if(ldrbcs16DrvIntendedBrkP5msDiff >= S16_MPRESS_0_2_BAR)
            if(ldrbcs16DrvIntendedBrkP5msDiff >= S16_PCTRL_PRESS_0_2_BAR)
    		{
    			ldrbcu8DriverSccBrakeIntendCnt = ldrbcu8DriverSccBrakeIntendCnt + 1;	/* Apply Cnt */
    		}
    		else { }
        }
//        else if((ldrbcs16DrvIntendedBrkP5msDiff < S16_MPRESS_0_BAR) || (ldrbcu1RbcSccReadyActFlg==0))
        else if((ldrbcs16DrvIntendedBrkP5msDiff < S16_PCTRL_PRESS_0_BAR) || (ldrbcu1RbcSccReadyActFlg==0))
        {
           ldrbcu8DriverSccBrakeIntendCnt = (ldrbcu8DriverSccBrakeIntendCnt>2)? (ldrbcu8DriverSccBrakeIntendCnt - 2):0; /* Apply Cnt */
        }
        else
        {
            ;
        }
            
        if(ldrbcu8DriverSccBrakeIntendCnt >= U8_T_50_MS)  /* 10 scan */
        {
            ldrbcu8DriverSccBrakeIntendCnt = 0;
            ldrbcu1SccBrakeIntendFlg = 1;
        }
		else { }
    }
    else
    {
        if (ldrbcu1RbcSccReadyActFlg == 0)
        {
//            if((ldrbcs16DrvIntendedBrkP5msDiff <= S16_MPRESS_0_BAR) )
        	if((ldrbcs16DrvIntendedBrkP5msDiff <= S16_PCTRL_PRESS_0_BAR) )
            {
            	  ldrbcu8DriverSccBrakeIntendCnt = ldrbcu8DriverSccBrakeIntendCnt + 1; /* Release Cnt */
            }
            else
            {
                ldrbcu8DriverSccBrakeIntendCnt = (ldrbcu8DriverSccBrakeIntendCnt>0)? (ldrbcu8DriverSccBrakeIntendCnt - 1):0; /* Release Cnt */
            }
		}
        else
        {
            ldrbcu8DriverSccBrakeIntendCnt = 0;
        }
        

        if(ldrbcu8DriverSccBrakeIntendCnt >= U8_T_10_MS)
        {
            ldrbcu8DriverSccBrakeIntendCnt = 0;
            ldrbcu1SccBrakeIntendFlg = 0;
        }
		else { }
    }
}

#endif
/*******************************************************************************
* FUNCTION NAME:		LDRBC_vDctAccelDriverIntention
* CALLED BY:			LDRBC_vDctDriverIntention()
* Preconditions:		none
* PARAMETER:			none
* RETURN VALUE:			none
* Description:			Dct Driver Intention to Accel
********************************************************************************/

static void LDRBC_vDctAccelDriverIntention(void)
{
	static uint8_t lcrbcu8DriverAccelIntendCnt = 0;

	if((GetDetRbcSt.lcrbcu1CANSignalInvalid==0) && (GetDetRbcSt.liu1cEMSAccPedDep_Pc_Err_Flg==0))
	{
		if(ldrbcu1DriverIntendToAccel==0)
		{
			if(GetDetRbcSt.lrs16AccelPosition >= U8_INHIB_SET_BY_APS)
			{
				lcrbcu8DriverAccelIntendCnt = lcrbcu8DriverAccelIntendCnt + 1;
				if(lcrbcu8DriverAccelIntendCnt > U8_T_100_MS)
				{
				    lcrbcu8DriverAccelIntendCnt = U8_T_100_MS;
				}
				else { }
			}
			else { }
		}
		else
		{
			if(GetDetRbcSt.lrs16AccelPosition <= U8_INHIB_EXIT_BY_APS)
			{
				lcrbcu8DriverAccelIntendCnt = 0;
			}
			else { }
		}

		if(lcrbcu8DriverAccelIntendCnt > U8_T_50_MS)
    	{
    	    ldrbcu1DriverIntendToAccel = 1;
    	}
    	else
    	{
    	    ldrbcu1DriverIntendToAccel = 0;
    	}

	}
	else
	{
		lcrbcu8DriverAccelIntendCnt = 0;
		ldrbcu1DriverIntendToAccel = 0;
	}
}
	    
/*******************************************************************************
* FUNCTION NAME:		LDRBC_vDctGearDriverIntention
* CALLED BY:			LDRBC_vDctDriverIntention()
* Preconditions:		none
* PARAMETER:			none
* RETURN VALUE:			none
* Description:			Dct Driver Gear selection
********************************************************************************/

static void LDRBC_vDctGearDriverIntention(void)
{
    static uint8_t ldrbcu8GearChangeWaitCnt = 0;

    ldrbcu1RegenGearSelectedOld = ldrbcu1RegenEnableGearSelected;

    if((GetDetRbcSt.lcrbcu1CANSignalInvalid==0) && (GetDetRbcSt.liu1fTcu1MsgTimeOutError==0))
    {
    	if((GetDetRbcSt.lsrbcu8GsSel == U8_RB_ENABLE_GEAR_1) || (GetDetRbcSt.lsrbcu8GsSel == U8_RB_ENABLE_GEAR_2) || (GetDetRbcSt.lsrbcu8GsSel == U8_RB_ENABLE_GEAR_3))
    	{
    	    ldrbcu1RegenEnableGearInput = 1;
    	}
    	else
    	{
    	    ldrbcu1RegenEnableGearInput = 0;
    	}

        if(ldrbcu1RegenEnableGearSelected == 0)
        {
            if(ldrbcu1RegenEnableGearInput == 1)
            {
                ldrbcu8GearChangeWaitCnt = ldrbcu8GearChangeWaitCnt + 1;
            }
			else { }
            
            if(ldrbcu8GearChangeWaitCnt >= U8_T_50_MS)
            {
                ldrbcu8GearChangeWaitCnt = 0;
                ldrbcu1RegenEnableGearSelected = 1;
            }
			else { }
        }
        else
        {
            if(ldrbcu1RegenEnableGearInput == 0)
            {
                ldrbcu1RegenEnableGearSelected = 0;
            }
			else { }
        }
    }
    else
    {
        ldrbcu8GearChangeWaitCnt = 0;
        ldrbcu1RegenEnableGearSelected = 0;
    }
    
}


static void LDRBC_vDctVehicleStatus(void)
{
	ldrbcs16VehicleSpeed = GetDetRbcSt.s16VehicleSpeed;
	ldrbcs16VehicleSpeed = L_s16LimitMinMax(ldrbcs16VehicleSpeed, S16_SPEED_MIN, S16_SPEED_MAX);
}
    


static void LDRBC_vDctFastDecelApply(void)
{
	int16_t s16FastPedalApplyThres;
	static int8_t ldrbcs8FastDecelApplyCnt = 0;

/**********************************************************/
/* Est Init Fast Pedal Apply                              */
/**********************************************************/

	s16FastPedalApplyThres = S16_RB_FAST_PEDAL_APPLY_THRES;	
    if(s16FastPedalApplyThres >= S16_FAST_PEDAL_APPLY_THRES)
	{
		s16FastPedalApplyThres = S16_FAST_PEDAL_APPLY_THRES;
	}
	                                           
	                                           
    if(ldrbcu1FastDecelApplyFlg==0)
    {

        if((GetDetRbcSt.ldahbs16PedalTravelRateMmPSec>=s16FastPedalApplyThres)|| (ldrbcs16DrvIntendedBrkP>=S16_RB_EXIT_PRESS_THRES))
        {
        	ldrbcu1FastDecelApplyFlg =1;
        	ldrbcs8FastDecelApplyCnt = U8_T_150_MS; 
        }
        else
	    {
        	ldrbcs8FastDecelApplyCnt = 0;
        }
	
    }
    else
    {
//    	if((GetDetRbcSt.ldahbs16PedalTravelRateMmPSec <= ((s16FastPedalApplyThres*3)/5))&&(ldrbcs16DrvIntendedBrkP<=(S16_RB_EXIT_PRESS_THRES-S16_MPRESS_10_BAR)))
    	if((GetDetRbcSt.ldahbs16PedalTravelRateMmPSec <= ((s16FastPedalApplyThres*3)/5))&&(ldrbcs16DrvIntendedBrkP<=(S16_RB_EXIT_PRESS_THRES-S16_PCTRL_PRESS_10_BAR)))
    	{
       		ldrbcs8FastDecelApplyCnt = ldrbcs8FastDecelApplyCnt - U8_T_10_MS;
    	}
//    	else if((GetDetRbcSt.ldahbs16PedalTravelRateMmPSec <= ((s16FastPedalApplyThres*4)/5))&&(ldrbcs16DrvIntendedBrkP<=(S16_RB_EXIT_PRESS_THRES-S16_MPRESS_5_BAR)))
    	else if((GetDetRbcSt.ldahbs16PedalTravelRateMmPSec <= ((s16FastPedalApplyThres*4)/5))&&(ldrbcs16DrvIntendedBrkP<=(S16_RB_EXIT_PRESS_THRES-S16_PCTRL_PRESS_5_BAR)))
    	{
    		ldrbcs8FastDecelApplyCnt = ldrbcs8FastDecelApplyCnt - U8_T_5_MS;
    	}
    	else
    	{
    		;
    	}
  	
//    	if ((ldrbcs16DrvIntendedBrkP <= S16_MPRESS_0_BAR)||(ldrbcs8FastDecelApplyCnt<=0))
    	if ((ldrbcs16DrvIntendedBrkP <= S16_PCTRL_PRESS_0_BAR)||(ldrbcs8FastDecelApplyCnt<=0))
    	{
    	    ldrbcu1FastDecelApplyFlg = 0;
    	    ldrbcs8FastDecelApplyCnt = 0;
    	}
    	else 
        {
            ;
        }
    }
}

     #if __CORNER_BRAKE_RBC_INHIBIT==ENABLE 

static void LDRBC_vDctLimitLeteralAcc(void)
{
	int16_t s16RbcOffEnterAyThreshold;
	int16_t s16RbcOffExitAyThreshold;
	int16_t s16AbsAlat;
	int16_t s16RbcOffAyThresholdDiff; 
	static int8_t ldrbcs8FastTurnCnt = 0 ;
	
    s16AbsAlat = L_s16Abs(GetDetRbcSt.lis16LateralAcc);
    
   
    #if __CBC==ENABLE
 	s16RbcOffEnterAyThreshold = (lis16CbcAlatEnterTh*8)/10;
 	#else
 	s16RbcOffEnterAyThreshold = S16_CBC_RBC_INHIBIT_AY_ENTER_TH;
 	#endif
 	
    s16RbcOffEnterAyThreshold = (s16RbcOffEnterAyThreshold <= S16_LAT_0_30_G) ? (S16_LAT_0_30_G) : (s16RbcOffEnterAyThreshold);

 	if(ldrbcu1DriverIntendToBrake == 1)
 	{
    	
    	#if __CBC==ENABLE
 	    s16RbcOffExitAyThreshold = (lis16CbcAlatExitTh*8)/10;
 	    #else
 	    s16RbcOffExitAyThreshold = S16_CBC_RBC_INHIBIT_AY_EXIT_TH;
 	    #endif  
 	    
        s16RbcOffExitAyThreshold = (s16RbcOffExitAyThreshold <= S16_LAT_0_25_G) ? (S16_LAT_0_25_G) : (s16RbcOffExitAyThreshold);
        s16RbcOffAyThresholdDiff = s16RbcOffEnterAyThreshold - s16RbcOffExitAyThreshold;
        
        if(s16RbcOffAyThresholdDiff < S16_LAT_0_10_G)
        {
            s16RbcOffExitAyThreshold = s16RbcOffEnterAyThreshold - S16_LAT_0_10_G;
        }
        else 
        {
            ;
        }
    }
    else 
    {
        s16RbcOffExitAyThreshold = s16RbcOffEnterAyThreshold;
    }
                                          
    if(ldrbcu1FastTurnFlg == 0)
    {
    	if(s16AbsAlat >= s16RbcOffEnterAyThreshold)
    	{
    	    if(ldrbcs8FastTurnCnt < U8_T_50_MS)
    	    {
    	        ldrbcs8FastTurnCnt = ldrbcs8FastTurnCnt + U8_T_5_MS;
    	    }
    	    else 
            {
        		ldrbcu1FastTurnFlg = 1; 
        		ldrbcs8FastTurnCnt = U8_T_150_MS;             
            }

    	}
     	else
    	{
    	    if(ldrbcs8FastTurnCnt > 0)
    	    {
    		    ldrbcs8FastTurnCnt = ldrbcs8FastTurnCnt - U8_T_5_MS;
    	    }
    	}
    }
    else
    {
        if(s16AbsAlat <= s16RbcOffExitAyThreshold)
        {
            ldrbcs8FastTurnCnt = ldrbcs8FastTurnCnt - U8_T_5_MS;
            if(ldrbcs8FastTurnCnt <= 0)
    	    {
    	        ldrbcu1FastTurnFlg = 0;
    		    ldrbcs8FastTurnCnt = 0;
    	    }
        }
        else 
        {
            ;
        }
    }
} 

    #endif /* __CORNER_BRAKE_RBC_INHIBIT==ENABLE */

static void LDRBC_vChkSafetyFuctionAct(void)
{

	static uint8_t lcrbcu8FrontWheelSlipDetCnt = 0;

    if(GetDetRbcSt.lcahbu1AHBOn == 1 )
    {
        if( ldrbcu1SafetyFuctionActFlg == 0 )
        {
            if( ((GetDetRbcSt.liu1AbsAct==1)&&(GetDetRbcSt.liu1AbsDef==0)) || ((GetDetRbcSt.liu1TcsCtl==1)&&(GetDetRbcSt.liu1TcsDef==0)) || ((GetDetRbcSt.liu1EscCtl==1)&&(GetDetRbcSt.liu1EscDef==0)))
            {
                ldrbcu1SafetyFuctionActFlg = 1;
            }  
            else { }  
        }      
        else { }      
        
        if(ldrbcu1FrontWheelSlipDec == 0)
        {
        if(GetDetRbcSt.liu1FrontWheelSlipDec==1)
        {
				if(lcrbcu8FrontWheelSlipDetCnt >= U8_T_20_MS)
				{
					ldrbcu1FrontWheelSlipDec = 1;
				}
				else
				{
					lcrbcu8FrontWheelSlipDetCnt = lcrbcu8FrontWheelSlipDetCnt + 1;
				}
			}
			else
			{
				if(lcrbcu8FrontWheelSlipDetCnt > 0)
				{
					lcrbcu8FrontWheelSlipDetCnt = lcrbcu8FrontWheelSlipDetCnt - 1;
				}
				else { }
			}
		}
		else{ }
   
    }
    else 
    {
        ldrbcu1SafetyFuctionActFlg = 0;
        ldrbcu1FrontWheelSlipDec   = 0;
		lcrbcu8FrontWheelSlipDetCnt = 0;
    }
}

    

static void LDRBC_vInhibitRegenReEntry(void)
{
      #if (__INHIBIT_RBC_RE_ENTRY == ENABLE)
    if(GetDetRbcSt.lcahbu1AHBOn==1)
    {           
		  #if __CORNER_BRAKE_RBC_INHIBIT==ENABLE
        if((ldrbcu1FastDecelApplyFlg==1) || (ldrbcu1FastTurnFlg==1)
		  ||((ldrbcu1RegenGearSelectedOld==1)&&(ldrbcu1RegenEnableGearSelected==0)))
		  #else
		if((ldrbcu1FastDecelApplyFlg==1)
		  ||((ldrbcu1RegenGearSelectedOld==1)&&(ldrbcu1RegenEnableGearSelected==0)))
		  #endif
        {
            ldrbcu1InhibitRegenReEntryFlg = 1;
        }        
    }
    else 
    {
        ldrbcu1InhibitRegenReEntryFlg = 0;
    }
     #else
        ldrbcu1InhibitRegenReEntryFlg = 0;
     #endif /* (__INHIBIT_RBC_RE_ENTRY == ENABLE)*/
}



static void LDRBC_vDctONSoaking(void)
{
    static uint16_t ldrbcu16MinutesCntAfterIgnOn = 0;
    static uint16_t ldrbcu16SecondsCntAfterIgnOn = 0;
    static uint16_t ldrbcu16BrakeCntDuringONS = 0;
    static uint16_t ldrbcu16BrakeIntendTimeCnt = 0;
    static int16_t ldrbcs16TempGap = 0;
    static int16_t ldrbcs16TempGapAvg = 0;
    static int16_t ldrbcs16TempSum = 0;
    static int8_t ldrbcs8TempAvgCnt = 0;
	uint8_t u8TemperatureErrFlg = (uint8_t) (GetDetRbcSt.lsrbcu1CoolantTempEMS3_ERR | GetDetRbcSt.lsrbcu1OutTempSnsrErrFlg);

    #if __RBC_ONSOAKING == DISABLE
    
    ldrbcu1OverNightSoakingFlg =0;
    
    #else
    
    if(GetDetRbcSt.lcrbcu1SignalValidAftIgnOn == 1)
	{ 
	    /* Check Temperature */
        if((GetDetRbcSt.lcrbcu1CANSignalInvalid == 0) && (u8TemperatureErrFlg==0) && (GetDetRbcSt.lrbcu1HCU_CF_Hcu_ServiceMod==0))
        {
            if(ldrbcs8TempAvgCnt < U8_T_100_MS)
            {
                ldrbcs8TempAvgCnt = ldrbcs8TempAvgCnt + 1;
            }
            if(ldrbcs8TempAvgCnt <= U8_T_50_MS)
            {           
                ldrbcs16TempGap = L_s16Abs(GetDetRbcSt.lsrbcs16CoolantTemp - GetDetRbcSt.lsrbcs16OutTempSnsrCIngOn);
                ldrbcs16TempSum = ldrbcs16TempSum + ldrbcs16TempGap;
                
                if(ldrbcs8TempAvgCnt == U8_T_50_MS)
                {
                    ldrbcs16TempGapAvg = ldrbcs16TempSum/ldrbcs8TempAvgCnt ;
                    
                    if ( ldrbcs16TempGapAvg <= S16_ONS_TEMP_DIFF )  /* 1 = 1dgree */    
                    {
                        ldrbcu1ONSoakingTempFlg = 1;
                    }
                }
                
            }
        }
        else
        {
            ldrbcu1ONSoakingTempFlg = 0;
            ldrbcs8TempAvgCnt = U8_T_100_MS;
        }

        /* Time limit for ONS */
        ldrbcu16SecondsCntAfterIgnOn = ldrbcu16SecondsCntAfterIgnOn + 1;
        if(ldrbcu16SecondsCntAfterIgnOn >= U16_T_60_S)
        {
            ldrbcu16MinutesCntAfterIgnOn = ldrbcu16MinutesCntAfterIgnOn + 1;
            ldrbcu16SecondsCntAfterIgnOn = 0;
            if(ldrbcu16MinutesCntAfterIgnOn >= U16_ONS_TIME_CNT_AF_IGN_ON)  /* 1 =  1 minute */
            {                 
                ldrbcu1ONSTimeCntAfIngOnFlg = 1;
            }                 
        }

        /* Count # of Brake pedal apply */
        if((GetDetRbcSt.lcahbu1AHBOn==1) && (ldrbcu1DriverIntendToBrake==1))
        {
            if(ldrbcu1ONSBrakeTimeCntFlg == 0)
            {
                if(( ldrbcs16VehicleSpeed >= S16_ONS_BRK_CNT_POSSIBLE_SPD ) || (ldrbcu16BrakeIntendTimeCnt > 0)) /* 1 = 0.125kph */
                {
                    ldrbcu16BrakeIntendTimeCnt = ldrbcu16BrakeIntendTimeCnt + 1; 
                    
                    if(ldrbcu16BrakeIntendTimeCnt >= S16_ONS_BRK_CNT_TIME_TH)  /* 1 = 1scan */ 
                    {
                        ldrbcu1ONSBrakeTimeCntFlg = 1;           
                    }
                }
            }
        } 
        else
        {
            if(ldrbcu1ONSBrakeTimeCntFlg == 1)
            {
                ldrbcu1ONSBrakeTimeCntFlg = 0;
                ldrbcu16BrakeCntDuringONS = ldrbcu16BrakeCntDuringONS + 1;
                if(ldrbcu16BrakeCntDuringONS >= U16_ONS_EXIT_BRK_CNT)    /* 1 = 1 */ 
                {
                    ldrbcu1BrakeCntDuringONSFlg = 1;
                }   
            }
            else
            {
                ldrbcu16BrakeIntendTimeCnt = 0;
            }
            
        }
       
    }
    else
    {
        ldrbcu16MinutesCntAfterIgnOn = 0;
        ldrbcu16SecondsCntAfterIgnOn = 0;
        ldrbcu16BrakeCntDuringONS = 0;      
        ldrbcu16BrakeIntendTimeCnt = 0; 
        ldrbcu1ONSTimeCntAfIngOnFlg = 0; 
        ldrbcu1ONSBrakeTimeCntFlg = 0;
        ldrbcu1ONSoakingTempFlg = 0;
        ldrbcu1BrakeCntDuringONSFlg = 0;
        ldrbcs8TempAvgCnt = 0;
        ldrbcs16TempGapAvg = 0;
        ldrbcs16TempSum = 0;
    }
    
    if((ldrbcu1ONSoakingTempFlg==1) && (ldrbcu1ONSTimeCntAfIngOnFlg==0) && (ldrbcu1BrakeCntDuringONSFlg==0))
    {
        ldrbcu1OverNightSoakingFlg = 1;
    }
    else
    {
        ldrbcu1OverNightSoakingFlg = 0;
    }
    
    #endif
}


static void LDRBC_vChkMotorRpm(void) 
{

#if  __RBC_CHK_MOTOR_RPM == ENABLE
    static uint8_t ldrbcu8MotorLowRpmCnt = 0;
    int16_t s16RBCExitSpdLimtMotRpm;
    int16_t s16RBCExitSpdLimt;
    

    
    if (GetDetRbcSt.ldrbcu1ActiveFlg == 1)
    {	
        s16RBCExitSpdLimt = lcrbcs16RegenLowSpeedLimit - S16_SPEED_1_KPH;              
    }
    else
    {
    	s16RBCExitSpdLimt = lcrbcs16RegenLowSpeedLimit;
    }
    
    s16RBCExitSpdLimt = L_s16LimitMinMax(s16RBCExitSpdLimt, S16_SPEED_0_5_KPH, S16_SPEED_30_KPH);
		       
    s16RBCExitSpdLimtMotRpm = (int16_t)(((int32_t)s16RBCExitSpdLimt*S16_SPEED_KPH_TO_RPM_CONT)/8);

       
    if(ldrbcu1MotorRpmOKInDGearFlg == 1)
    {
	    if (GetDetRbcSt.ldrbcu1ActiveFlg == 1)
	    {	
            if (lesps16MCU_CR_Mot_ActRotSpd_rpm <= 0)
            {
                ldrbcu1MotorRpmOKInDGearFlg = 0;
                ldrbcu8MotorLowRpmCnt = U8_RBC_EXIT_WAIT_TIME; 
            }
            else if (lesps16MCU_CR_Mot_ActRotSpd_rpm <= s16RBCExitSpdLimtMotRpm)
            {
                ldrbcu8MotorLowRpmCnt = ldrbcu8MotorLowRpmCnt + 1;
                if(ldrbcu8MotorLowRpmCnt >= U8_RBC_EXIT_WAIT_TIME) 
                {
                    ldrbcu1MotorRpmOKInDGearFlg = 0;
                }
            }
            else 
            {
                ldrbcu8MotorLowRpmCnt = 0;
            }	        
	    }
	    else 
        {
            if (lesps16MCU_CR_Mot_ActRotSpd_rpm <= s16RBCExitSpdLimtMotRpm)
            {
                ldrbcu1MotorRpmOKInDGearFlg = 0;
            }
        }          
    }
    else 
    {
	    if (GetDetRbcSt.ldrbcu1ActiveFlg == 1)
	    {	
            if (lesps16MCU_CR_Mot_ActRotSpd_rpm > s16RBCExitSpdLimtMotRpm)
            {
                ldrbcu8MotorLowRpmCnt = ldrbcu8MotorLowRpmCnt - 1;
                if(ldrbcu8MotorLowRpmCnt <= 0) 
                {
                    ldrbcu1MotorRpmOKInDGearFlg = 1;
                }
            }
            else 
            {
                ldrbcu8MotorLowRpmCnt = U8_RBC_EXIT_WAIT_TIME;
            }	        
	    }
	    else 
        {
            if (lesps16MCU_CR_Mot_ActRotSpd_rpm > s16RBCExitSpdLimtMotRpm)
            {
                ldrbcu1MotorRpmOKInDGearFlg = 1;
				ldrbcu8MotorLowRpmCnt = 0;
            }
        }        
    }   
  #else 
    ldrbcu1MotorRpmOKInDGearFlg = 1;
  #endif    
  
}   


static void LDRBC_vDctStartRegenPress(void)
{
    int16_t s16RegenTorqRatio;

    if(GetDetRbcSt.ldrbcu16TargetRBTqNm == 0)
    {
        s16RegenTorqRatio = 0;
    }
    else
    {
        s16RegenTorqRatio = (int16_t)(((uint32_t)(GetDetRbcSt.lrbcu16HCU_Regen_BrkTq_Nm )*100)/GetDetRbcSt.ldrbcu16TargetRBTqNm);
    }
    
    
    if(GetDetRbcSt.ldrbcu1ActiveFlg == 1)
    {
        if(ldrbcu1RBCStartFlag == 0)
        {
//            if((s16RegenTorqRatio>S8_PERCENT_0_PRO) && (s16RegenTorqRatio<=S8_PERCENT_50_PRO)&&(ldrbcs16RegenTorqRate>S16_MPRESS_1_BAR))
        	if((s16RegenTorqRatio>S8_PERCENT_0_PRO) && (s16RegenTorqRatio<=S8_PERCENT_50_PRO)&&(ldrbcs16RegenTorqRate>S16_PCTRL_PRESS_1_BAR))
            {
                ldrbcu1RBCStartFlag = 1;
            }
            else { }
        }
        else
        {
            if((s16RegenTorqRatio<=S8_PERCENT_0_PRO) || (s16RegenTorqRatio>=S8_PERCENT_70_PRO))
            {
                ldrbcu1RBCStartFlag = 0;
            }
            else { }
        }
    }      
    else
    {
        ldrbcu1RBCStartFlag = 0;
    }
}

static void LDRBC_vDctExitRegenPress(void)
{
    int16_t s16RegenTorqRatio;
    static int16_t lsrbcu8RBCPressDecreaseCnt=0;
    
    if(GetDetRbcSt.ldrbcu16TargetRBTqNm == 0)
    {
        s16RegenTorqRatio = 0;
    }
    else
    {
        s16RegenTorqRatio = (int16_t)(((uint32_t)(GetDetRbcSt.lrbcu16HCU_Regen_BrkTq_Nm )*100)/GetDetRbcSt.ldrbcu16TargetRBTqNm);
    }
    

    if(GetDetRbcSt.ldrbcu1ActiveFlg == 1)
    {
        if(ldrbcu1RBCBlendingFlag == 0)
        {
//            if(GetDetRbcSt.ldahbs16TargetPressRate10ms >= S16_MPRESS_0_5_BAR)
        	if(GetDetRbcSt.ldahbs16TargetPressRate10ms >= S16_PCTRL_PRESS_0_5_BAR)
            {
//                if(((GetDetRbcSt.lcrbcu1ExitCmdRengenTorq == 1)&&(ldrbcs16RegenTorqRate < -S16_MPRESS_0_1_BAR)))
        		if(((GetDetRbcSt.lcrbcu1ExitCmdRengenTorq == 1)&&(ldrbcs16RegenTorqRate < -S16_PCTRL_PRESS_0_1_BAR)))
                {
                    ldrbcu1RBCBlendingFlag = 1;
                }                
//                else if((s16RegenTorqRatio > S8_PERCENT_30_PRO)&&(ldrbcs16RegenTorqRate < -S16_MPRESS_2_BAR))
        		else if((s16RegenTorqRatio > S8_PERCENT_30_PRO)&&(ldrbcs16RegenTorqRate < -S16_PCTRL_PRESS_2_BAR))
                {
                    ldrbcu1RBCBlendingFlag = 1;
                } 
//                else if((ldrbcs16RegenTorqRate < -S16_MPRESS_1_BAR) && (s16RegenTorqRatio > S8_PERCENT_20_PRO))
        		else if((ldrbcs16RegenTorqRate < -S16_PCTRL_PRESS_1_BAR) && (s16RegenTorqRatio > S8_PERCENT_20_PRO))
                {
                    lsrbcu8RBCPressDecreaseCnt = lsrbcu8RBCPressDecreaseCnt+1;
                    if(lsrbcu8RBCPressDecreaseCnt >= 2)
                    {
                        lsrbcu8RBCPressDecreaseCnt = 0; 
                        ldrbcu1RBCBlendingFlag = 1;
                    }
                }
				else { }
				
				if( (ldrbcu1RBCBlendingFlag == TRUE) && (ldrbcu1RbcSccReadyActFlg == TRUE) )
                {
					ldrbcu1RbcSccBlendFlag = TRUE;
                }
				else { }
				
            }

        }
        else
        {  
            if(GetDetRbcSt.lcrbcu1ExitCmdRengenTorq == 1)
            {
                ;	/* Do Nothing */
            }     
//            else if((ldrbcs16RegenTorqRate >= S16_MPRESS_0_BAR) || (s16RegenTorqRatio <= S8_PERCENT_0_PRO))
            else if((ldrbcs16RegenTorqRate >= S16_PCTRL_PRESS_0_BAR) || (s16RegenTorqRatio <= S8_PERCENT_0_PRO))
            {
                lsrbcu8RBCPressDecreaseCnt = lsrbcu8RBCPressDecreaseCnt+1;
                if(lsrbcu8RBCPressDecreaseCnt >= 10)
                {
                    lsrbcu8RBCPressDecreaseCnt = 0; 
                    ldrbcu1RBCBlendingFlag = 0;
                }
            }
            else { }
        }
    }      
    else
    {
        ldrbcu1RBCBlendingFlag = 0;
		ldrbcu1RbcSccBlendFlag = 0;
    }
}


static void LDRBC_vCalcRegenTorqRate(void)
{
	static int16_t ldrbcs16RegenTorqSum=0;
	static int16_t ldrbcs16RegenTorqBuff[3]={0,0,0};
	static int16_t ldrbcs16RegenTorqAvg=0;
	static int16_t ldrbcs16RegenTorqRateBuff[3]={0,0,0};
	static uint8_t ldrbcu8RegenTorqRateCnt=0;
	static uint8_t ldrbcu8RegenTorqAvgCnt=0;

	
/**********************************************************/
/* Update Regen Torque Buffer, Sum for 3scan              */
/**********************************************************/

	ldrbcs16RegenTorqSum = (ldrbcs16RegenTorqSum - (ldrbcs16RegenTorqBuff[ldrbcu8RegenTorqAvgCnt])) + (int16_t)GetDetRbcSt.lrbcu16HCU_Regen_BrkTq_Nm ;
    ldrbcs16RegenTorqAvg = ldrbcs16RegenTorqSum/3;           
    
    ldrbcs16RegenTorqBuff[ldrbcu8RegenTorqAvgCnt] = (int16_t)GetDetRbcSt.lrbcu16HCU_Regen_BrkTq_Nm ;
    ldrbcu8RegenTorqAvgCnt = ldrbcu8RegenTorqAvgCnt + 1;
    
    if(ldrbcu8RegenTorqAvgCnt>=3) 
    {
        ldrbcu8RegenTorqAvgCnt=0;
    }
    else 
    {
    	;
    }

/**********************************************************/
/* Regen torque change rate                               */
/**********************************************************/

    ldrbcs16RegenTorqRate = L_s16Lpf1Int((ldrbcs16RegenTorqAvg - ldrbcs16RegenTorqRateBuff[ldrbcu8RegenTorqRateCnt]), ldrbcs16RegenTorqRate, U8_LPF_7HZ_128G);
    ldrbcs16RegenTorqRateBuff[ldrbcu8RegenTorqRateCnt] = ldrbcs16RegenTorqAvg;
    
    ldrbcu8RegenTorqRateCnt = ldrbcu8RegenTorqRateCnt + 1;
    
    if(ldrbcu8RegenTorqRateCnt>=3) 
    {
        ldrbcu8RegenTorqRateCnt = 0;
    }
    else
    {
        ;
    }     
}   
    
static void LDRBC_vDctSCCReadyAct(void)
{
	ldrbcu1RbcSccReadyActFlgOld = ldrbcu1RbcSccReadyActFlg;
		
	  #if (__SCC_RBC_ENABLE == ENABLE)
	if(GetDetRbcSt.lcahbu1AHBOn==1)
	{
		if(ldrbcu1RbcSccReadyActFlg==0)
		{
//			if((liu1SccCtl==1) && (liu1SccDef==0) && (lis16SccTP>=S16_MPRESS_3_BAR))
			if((liu1SccCtl==1) && (liu1SccDef==0) && (lis16SccTP>=S16_PCTRL_PRESS_3_BAR))
			{
				ldrbcu1RbcSccReadyActFlg = 1;
			}
			else { }
		}
		else
		{
//			if((liu1SccCtl==1) && (liu1SccDef==0) && (lis16SccTP>=S16_MPRESS_0_BAR))
			if((liu1SccCtl==1) && (liu1SccDef==0) && (lis16SccTP>=S16_PCTRL_PRESS_0_BAR))
				{
					ldrbcu1RbcSccReadyActFlg = 1;
					if(GetDetRbcSt.lrbcu16HCU_Regen_BrkTq_Nm >0)
					{
						ldrbcu1RbcSccRegenActFlg = 1;
					}
				}
				else if(ldrbcu1RbcSccRegenActFlg==0)
				{
					ldrbcu1RbcSccReadyActFlg = 0;
				}
				else { }
		}		
	}
	else
	{
		ldrbcu1RbcSccReadyActFlg = 0;
		ldrbcu1RbcSccRegenActFlg = 0;
	}
	  #else
	ldrbcu1RbcSccReadyActFlg = 0;
	  #endif
}
    

/******************************************************************************
* FUNCTION NAME:      LDRBC_s16GetTotalBrkPress
* CALLED BY:          LDRBC_vDctDriverIntention()
* Preconditions:      none
* PARAMETER:
* RETURN VALUE:       s16TotalBrakeTP
* Description:        Estimate Total Brake Pressss
******************************************************************************/

#if (__ESTIMATE_TOTAL_BRAKE_TQ == ENABLE)
static int16_t LDRBC_s16GetTotalBrkPress(void)
{
	int16_t s16TotalBrakeTP = 0;

//	if(ldrbcs16SccActiveBrkTP > S16_MPRESS_0_BAR)
	if(ldrbcs16SccActiveBrkTP > S16_PCTRL_PRESS_0_BAR)
	{
		/* SCC Active state */
		if(GetDetRbcSt.ldahbs16DriverIntendedTP >= ldrbcs16SccActiveBrkTP)
		{
			/* DriverIntendedTP is larger than SCC ActiveBrkTP */
			s16TotalBrakeTP = GetDetRbcSt.ldahbs16DriverIntendedTP;
		}
		else
		{
			s16TotalBrakeTP = ldrbcs16SccActiveBrkTP;
		}
	}
	else
	{
		/* Normal state */
//		if(ldrbcs16PedalTravelFilt >= S16_AHBON_PD_MIN)
//		{
//			s16TotalBrakeTP = GetDetRbcSt.ldahbs16DriverIntendedTP;
//		}
//		else
//		{
			if(GetDetRbcSt.lcahbu1AHBOn==1)
			{
				s16TotalBrakeTP = GetDetRbcSt.ldahbs16DriverIntendedTP;
			}
			else
			{
				s16TotalBrakeTP = 0;
			}
//		}
	}

    if(s16TotalBrakeTP < GetDetRbcSt.ldahbs16AvhActiveBrkTP)
    {
    	s16TotalBrakeTP =GetDetRbcSt.ldahbs16AvhActiveBrkTP;
    }
    else { }

	return s16TotalBrakeTP;
}
#endif

#if (__SCC_VIRTUAL_PEDAL_CALC == ENABLE)
/*******************************************************************************
* FUNCTION NAME:		LDRBC_s16GetPedalTravelFromTP
* CALLED BY:			LDRBC_vDctVirtualPDT()
* Preconditions:		__SCC_VIRTUAL_PEDAL_CALC == ENABLE
* PARAMETER:			int16_t s16TargetPressure
* RETURN VALUE:			s16PedalTravel
* Description:			Convert Target Pressure to Pedal Travel
********************************************************************************/
static int16_t LDRBC_s16GetPedalTravelFromTP(int16_t s16TargetPressure)
{
    int16_t s16PedalTravel = 0;

    if(s16TargetPressure <= S16_PEDAL_TARGET_P_01)
    {
        s16PedalTravel = 0;
    }
    else if(s16TargetPressure >= S16_PEDAL_TARGET_P_MAX)
    {
        s16PedalTravel = S16_PEDAL_TRAVEL_MAX;
    }
    else
    {
        s16PedalTravel = L_s16IInter15Point(s16TargetPressure,
											S16_PEDAL_TARGET_P_01, S16_PEDAL_TRAVEL_01,
											S16_PEDAL_TARGET_P_02, S16_PEDAL_TRAVEL_02,
											S16_PEDAL_TARGET_P_03, S16_PEDAL_TRAVEL_03,
											S16_PEDAL_TARGET_P_04, S16_PEDAL_TRAVEL_04,
											S16_PEDAL_TARGET_P_05, S16_PEDAL_TRAVEL_05,
											S16_PEDAL_TARGET_P_06, S16_PEDAL_TRAVEL_06,
											S16_PEDAL_TARGET_P_07, S16_PEDAL_TRAVEL_07,
											S16_PEDAL_TARGET_P_08, S16_PEDAL_TRAVEL_08,
											S16_PEDAL_TARGET_P_09, S16_PEDAL_TRAVEL_09,
											S16_PEDAL_TARGET_P_10, S16_PEDAL_TRAVEL_10,
											S16_PEDAL_TARGET_P_11, S16_PEDAL_TRAVEL_11,
											S16_PEDAL_TARGET_P_12, S16_PEDAL_TRAVEL_12,
											S16_PEDAL_TARGET_P_13, S16_PEDAL_TRAVEL_13,
											S16_PEDAL_TARGET_P_14, S16_PEDAL_TRAVEL_14,
											S16_PEDAL_TARGET_P_MAX, S16_PEDAL_TRAVEL_MAX);
    }

    return s16PedalTravel;
}
#endif /* #if (__SCC_VIRTUAL_PEDAL_CALC == ENABLE) */

/******************************************************************************
* FUNCTION NAME:      LDRBC_s16DctVirtualPDT
* CALLED BY:          LDRBC_vCallDetection()
* Preconditions:      none
* PARAMETER:          s16BrakeTP
* RETURN VALUE:       s16VritualPDT
* Description:        Estimate Virtual PDT
******************************************************************************/
static int16_t LDRBC_s16DctVirtualPDT(int16_t s16BrakeTP)
{
	int16_t s16VritualPDT = 0;
	
  #if (__SCC_VIRTUAL_PEDAL_CALC == DISABLE)
  
	ldrbcu1EBS2_CF_Brk_VirtStkValid = 0;
	s16VritualPDT = GetDetRbcSt.lsahbs16PDTFilt;
	
  #else
  
	if((liu1fPdtSenFaultDet == TRUE) || (liu1fPdfSenFaultDet == TRUE))
	{
		ldrbcu1EBS2_CF_Brk_VirtStkValid = 1;
	}
	else
	{
		ldrbcu1EBS2_CF_Brk_VirtStkValid = 0;
	}


    if(ldrbcu1EBS2_CF_Brk_VirtStkValid == 1)
    {
        s16VritualPDT = 0;
    }
    else
    {
//		if((liu1SccCtl==1) && (liu1SccDef==0) && (lis16SccTP>=S16_MPRESS_0_BAR))
    	if((liu1SccCtl==1) && (liu1SccDef==0) && (lis16SccTP>=S16_PCTRL_PRESS_0_BAR))
		{
			/* SCC Active state */
			s16VritualPDT = LDRBC_s16GetPedalTravelFromTP(s16BrakeTP);	/* Get VirtualPDT From TP */
		}
		else
		{
			/* SCC is not active */
			s16VritualPDT = lsahbs16PDTFilt;
		}
    }

  #endif /* #if (__SCC_VIRTUAL_PEDAL_CALC == ENABLE) */		

	s16VritualPDT = L_s16LimitMinMax(s16VritualPDT, 0, S16_PEDAL_TRAVEL_MAX);
	return s16VritualPDT;
}

/*******************************************************************************
* FUNCTION NAME:		LDRBC_vChkActiveFuctionAct
* CALLED BY:			LDRBC_vCallDetection()
* Preconditions:		none
* PARAMETER:			none
* RETURN VALUE:			none
* Description:			Check Active Brake Function Activation
********************************************************************************/
static void LDRBC_vChkActiveFuctionAct(void)
{
	ldrbcu1SccFuctionActOldFlg = ldrbcu1SccFuctionActFlg;

	if(GetDetRbcSt.lcahbu1AHBOn == 1)
    {
        if(ldrbcu1SccFuctionActFlg == 0)
        {
//            if((GetDetRbcSt.liu1SccCtl==1) && (GetDetRbcSt.liu1SccDef==0) && (GetDetRbcSt.lis16SccTP>S16_MPRESS_0_BAR))
        	if((GetDetRbcSt.liu1SccCtl==1) && (GetDetRbcSt.liu1SccDef==0) && (GetDetRbcSt.lis16SccTP>S16_PCTRL_PRESS_0_BAR))
            {
                ldrbcu1SccFuctionActFlg = 1;
            }
            else { }
        }
        else { }
    }
    else
    {
        ldrbcu1SccFuctionActFlg = 0;
    }	
}



static int16_t LDRBC_s16DctActiveBrkTargetPres(uint8_t u8PresCtrlAllowLevel, uint8_t u8DefectFlg, uint8_t u8CtrlFlg, int16_t s16ActiveTPRequest, int16_t s16ActiveTPOld)
{
	int16_t s16ActiveTP=0;

	if(u8CtrlFlg==0)
	{
		;
	}
	else
	{
		if(GetDetRbcSt.lcrbcu1SignalValidAftIgnOn==1)
		{
			if((u8DefectFlg==0))       /* IGN On & !Defect : Need to check validity of ESC Ctrl inform via CAN for AHB Gen1 */
			{
				s16ActiveTP = s16ActiveTPRequest;
			}
//			else if(s16ActiveTPOld >= S16_MPRESS_1_BAR)                                        /* IGN On & Defect & On Ctrl */
			else if(s16ActiveTPOld >= S16_PCTRL_PRESS_1_BAR)                                        /* IGN On & Defect & On Ctrl */
			{
				if((u8PresCtrlAllowLevel==U8_PRES_CTRL_L3) || (u8PresCtrlAllowLevel==U8_PRES_CTRL_L2))
				{
//					s16ActiveTP = L_s16LimitDiff(s16ActiveTPRequest, s16ActiveTPOld, 0, S16_MPRESS_50_BAR);
					s16ActiveTP = L_s16LimitDiff(s16ActiveTPRequest, s16ActiveTPOld, 0, S16_PCTRL_PRESS_50_BAR);
				}
				else
				{
					;
				}
			}
			else
			{
				;
			}
		}
		else
		{
//			if(s16ActiveTPOld >= S16_MPRESS_1_BAR)
			if(s16ActiveTPOld >= S16_PCTRL_PRESS_1_BAR)
			{
				if(u8PresCtrlAllowLevel==U8_PRES_CTRL_L3)
				{
//					s16ActiveTP = L_s16LimitDiff(s16ActiveTPRequest, s16ActiveTPOld, 0, S16_MPRESS_50_BAR);
					s16ActiveTP = L_s16LimitDiff(s16ActiveTPRequest, s16ActiveTPOld, 0, S16_PCTRL_PRESS_50_BAR);
				}
				else
				{
					;
				}
			}
			else
			{
				;
			}
		}
	}

	return s16ActiveTP;
}


static void LDRBC_vOutPutDetection(void)
{

	Rbc_CtrlBus.Rbc_CtrlRgnBrkCtlrBlendgFlg = ldrbcu1RBCBlendingFlag ;
	Rbc_CtrlBus.Rbc_CtrlTarRgnBrkTqInfo.VirtStkDep = (uint16_t)ldrbcs16VirtualPDT;
	Rbc_CtrlBus.Rbc_CtrlTarRgnBrkTqInfo.VirtStkValid = ldrbcu1EBS2_CF_Brk_VirtStkValid;

/* Total Brake Force */
      #if (__ESTIMATE_TOTAL_BRAKE_TQ == ENABLE)
	Rbc_CtrlBus.Rbc_CtrlTarRgnBrkTqInfo.EstTotBrkForce = ldrbcu16TotalBrakeTqNm;	/* resolution : 1 Nm */
     #else
	RBCbus.TarRgnBrkTqInfo.lrbcu16EBS_CR_Brk_EstTot_Nm = ldrbcu16DrvIntendedBrkTqNm;	/* resolution : 1 Nm */
     #endif


	RBCsrcbus.VehicleSpeed=ldrbcs16VehicleSpeed;
	RBCsrcbus.SccFuctionActFlg=ldrbcu1SccFuctionActFlg;
	RBCsrcbus.RbcSccReadyActFlg=ldrbcu1RbcSccReadyActFlg;
	RBCsrcbus.RbcSccReadyActFlgOld=ldrbcu1RbcSccReadyActFlgOld;
	RBCsrcbus.DriverIntendToAccel=ldrbcu1DriverIntendToAccel;
	RBCsrcbus.RegenEnableGearSelected = ldrbcu1RegenEnableGearSelected;
	RBCsrcbus.MotorRpmOKInDGearFlg=ldrbcu1MotorRpmOKInDGearFlg;

	RBCsrcbus.FastDecelApplyFlg          =ldrbcu1FastDecelApplyFlg     ;
	RBCsrcbus.FastTurnFlg                =ldrbcu1FastTurnFlg           ;
	RBCsrcbus.SafetyFuctionActFlg        =ldrbcu1SafetyFuctionActFlg   ;
	RBCsrcbus.FrontWheelSlipDec          =ldrbcu1FrontWheelSlipDec     ;
	RBCsrcbus.RbcSccRegenActFlg          =ldrbcu1RbcSccRegenActFlg     ;
	RBCsrcbus.InhibitRegenReEntryFlg     =ldrbcu1InhibitRegenReEntryFlg;
	RBCsrcbus.SccBrakeIntendFlg          =ldrbcu1SccBrakeIntendFlg     ;
	RBCsrcbus.SccFuctionActFlg           =ldrbcu1SccFuctionActFlg      ;
	RBCsrcbus.SccFuctionActOldFlg        =ldrbcu1SccFuctionActOldFlg   ;
	RBCsrcbus.PressToTorqRatioTotal      =ldrbcu16PressToTorqRatioTotal;
	RBCsrcbus.DrvIntendedBrkP            =ldrbcs16DrvIntendedBrkP      ;
	RBCsrcbus.DriverIntendToBrake       =ldrbcu1DriverIntendToBrake;
	RBCsrcbus.RegenEnableGearInput=ldrbcu1RegenEnableGearInput;
	RBCsrcbus.RbcSccBlendFlag=ldrbcu1RbcSccBlendFlag;
	RBCsrcbus.DrvIntendedBrkTqNm=ldrbcu16DrvIntendedBrkTqNm;
	RBCsrcbus.TotalBrakeTqNm=ldrbcu16TotalBrakeTqNm;
	RBCsrcbus.VirtualPDT=ldrbcs16VirtualPDT;
}


#define RBC_CTRL_STOP_SEC_CODE
#include "Rbc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
