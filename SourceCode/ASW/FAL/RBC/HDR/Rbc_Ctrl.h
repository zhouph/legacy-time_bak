/**
 * @defgroup Rbc_Ctrl Rbc_Ctrl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Rbc_Ctrl.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef RBC_CTRL_H_
#define RBC_CTRL_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Rbc_Types.h"
#include "Rbc_Cfg.h"
#include "Rbc_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define RBC_CTRL_MODULE_ID      (0)
 #define RBC_CTRL_MAJOR_VERSION  (2)
 #define RBC_CTRL_MINOR_VERSION  (0)
 #define RBC_CTRL_PATCH_VERSION  (0)
 #define RBC_CTRL_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Rbc_Ctrl_HdrBusType Rbc_CtrlBus;

/* Internal Logical Bus */
extern RegenBrakeCooperation_SRCbusType    RBCsrcbus;


/* Version Info */
extern const SwcVersionInfo_t Rbc_CtrlVersionInfo;

/* Input Data Element */
extern Proxy_RxCanRxRegenInfo_t Rbc_CtrlCanRxRegenInfo;
extern Proxy_RxCanRxAccelPedlInfo_t Rbc_CtrlCanRxAccelPedlInfo;
extern Proxy_RxCanRxIdbInfo_t Rbc_CtrlCanRxIdbInfo;
extern Proxy_RxCanRxEngTempInfo_t Rbc_CtrlCanRxEngTempInfo;
extern Abc_CtrlAbsCtrlInfo_t Rbc_CtrlAbsCtrlInfo;
extern Abc_CtrlAvhCtrlInfo_t Rbc_CtrlAvhCtrlInfo;
extern Abc_CtrlEscCtrlInfo_t Rbc_CtrlEscCtrlInfo;
extern Abc_CtrlSccCtrlInfo_t Rbc_CtrlSccCtrlInfo;
extern Abc_CtrlTcsCtrlInfo_t Rbc_CtrlTcsCtrlInfo;
extern Spc_5msCtrlCircPFildInfo_t Rbc_CtrlCircPFildInfo;
extern Spc_5msCtrlPedlTrvlFildInfo_t Rbc_CtrlPedlTrvlFildInfo;
extern Det_5msCtrlPedlTrvlRateInfo_t Rbc_CtrlPedlTrvlRateInfo;
extern Spc_5msCtrlPistPFildInfo_t Rbc_CtrlPistPFildInfo;
extern Eem_MainEemFailData_t Rbc_CtrlEemFailData;
extern Eem_MainEemSuspectData_t Rbc_CtrlEemSuspectData;
extern Mom_HndlrEcuModeSts_t Rbc_CtrlEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t Rbc_CtrlIgnOnOffSts;
extern Pct_5msCtrlPCtrlAct_t Rbc_CtrlPCtrlAct;
extern Eem_SuspcDetnFuncInhibitRbcSts_t Rbc_CtrlFuncInhibitRbcSts;
extern Abc_CtrlAy_t Rbc_CtrlAy;
extern Spc_5msCtrlBlsFild_t Rbc_CtrlBlsFild;
extern Det_5msCtrlPedlTrvlFinal_t Rbc_CtrlPedlTrvlFinal;
extern Bbc_CtrlTarPFromBaseBrkCtrlr_t Rbc_CtrlTarPFromBaseBrkCtrlr;
extern Bbc_CtrlTarPRateFromBaseBrkCtrlr_t Rbc_CtrlTarPRateFromBaseBrkCtrlr;
extern Bbc_CtrlTarPFromBrkPedl_t Rbc_CtrlTarPFromBrkPedl;
extern Det_5msCtrlVehSpdFild_t Rbc_CtrlVehSpdFild;

/* Output Data Element */
extern Rbc_CtrlTarRgnBrkTqInfo_t Rbc_CtrlTarRgnBrkTqInfo;
extern Rbc_CtrlRgnBrkCoopWithAbsInfo_t Rbc_CtrlRgnBrkCoopWithAbsInfo;
extern Rbc_CtrlBrkPRednForBaseBrkCtrlr_t Rbc_CtrlBrkPRednForBaseBrkCtrlr;
extern Rbc_CtrlRgnBrkCtlrBlendgFlg_t Rbc_CtrlRgnBrkCtlrBlendgFlg;
extern Rbc_CtrlRgnBrkCtrlrActStFlg_t Rbc_CtrlRgnBrkCtrlrActStFlg;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Rbc_Ctrl_Init(void);
extern void Rbc_Ctrl(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* RBC_CTRL_H_ */
/** @} */
