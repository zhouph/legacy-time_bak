/**
 * @defgroup Rbc_Types Rbc_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Rbc_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef RBC_TYPES_H_
#define RBC_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Asw_Types.h" /* HSH */
#include "Bsw_Types.h"
#include "Rte_Types.h"
#include "RteDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Proxy_RxCanRxRegenInfo_t Rbc_CtrlCanRxRegenInfo;
    Proxy_RxCanRxAccelPedlInfo_t Rbc_CtrlCanRxAccelPedlInfo;
    Proxy_RxCanRxIdbInfo_t Rbc_CtrlCanRxIdbInfo;
    Proxy_RxCanRxEngTempInfo_t Rbc_CtrlCanRxEngTempInfo;
    Abc_CtrlAbsCtrlInfo_t Rbc_CtrlAbsCtrlInfo;
    Abc_CtrlAvhCtrlInfo_t Rbc_CtrlAvhCtrlInfo;
    Abc_CtrlEscCtrlInfo_t Rbc_CtrlEscCtrlInfo;
    Abc_CtrlSccCtrlInfo_t Rbc_CtrlSccCtrlInfo;
    Abc_CtrlTcsCtrlInfo_t Rbc_CtrlTcsCtrlInfo;
    Spc_5msCtrlCircPFildInfo_t Rbc_CtrlCircPFildInfo;
    Spc_5msCtrlPedlTrvlFildInfo_t Rbc_CtrlPedlTrvlFildInfo;
    Det_5msCtrlPedlTrvlRateInfo_t Rbc_CtrlPedlTrvlRateInfo;
    Spc_5msCtrlPistPFildInfo_t Rbc_CtrlPistPFildInfo;
    Eem_MainEemFailData_t Rbc_CtrlEemFailData;
    Eem_MainEemSuspectData_t Rbc_CtrlEemSuspectData;
    Mom_HndlrEcuModeSts_t Rbc_CtrlEcuModeSts;
    Prly_HndlrIgnOnOffSts_t Rbc_CtrlIgnOnOffSts;
    Pct_5msCtrlPCtrlAct_t Rbc_CtrlPCtrlAct;
    Eem_SuspcDetnFuncInhibitRbcSts_t Rbc_CtrlFuncInhibitRbcSts;
    Abc_CtrlAy_t Rbc_CtrlAy;
    Spc_5msCtrlBlsFild_t Rbc_CtrlBlsFild;
    Det_5msCtrlPedlTrvlFinal_t Rbc_CtrlPedlTrvlFinal;
    Bbc_CtrlTarPFromBaseBrkCtrlr_t Rbc_CtrlTarPFromBaseBrkCtrlr;
    Bbc_CtrlTarPRateFromBaseBrkCtrlr_t Rbc_CtrlTarPRateFromBaseBrkCtrlr;
    Bbc_CtrlTarPFromBrkPedl_t Rbc_CtrlTarPFromBrkPedl;
    Det_5msCtrlVehSpdFild_t Rbc_CtrlVehSpdFild;

/* Output Data Element */
    Rbc_CtrlTarRgnBrkTqInfo_t Rbc_CtrlTarRgnBrkTqInfo;
    Rbc_CtrlRgnBrkCoopWithAbsInfo_t Rbc_CtrlRgnBrkCoopWithAbsInfo;
    Rbc_CtrlBrkPRednForBaseBrkCtrlr_t Rbc_CtrlBrkPRednForBaseBrkCtrlr;
    Rbc_CtrlRgnBrkCtlrBlendgFlg_t Rbc_CtrlRgnBrkCtlrBlendgFlg;
    Rbc_CtrlRgnBrkCtrlrActStFlg_t Rbc_CtrlRgnBrkCtrlrActStFlg;
}Rbc_Ctrl_HdrBusType;


typedef struct
{

	   //uint16_t  CANSignalValid                :1;
	   uint16_t  DriverIntendToBrake           :1;
	   uint16_t  DriverIntendToAccel           :1;
	   uint16_t  RegenEnableGearInput          :1;
	   uint16_t  RegenEnableGearSelected       :1;
	   //uint16_t  DriverIntendToBrakeOld        :1;
	   uint16_t  MotorRpmOKInDGearFlg          :1;
	   //uint16_t  RegenGearSelectedOld          :1;

	   uint16_t  FastDecelApplyFlg             :1;
	   uint16_t  FastTurnFlg                   :1;
	   uint16_t  SafetyFuctionActFlg           :1;
	   uint16_t  FrontWheelSlipDec		       :1;
	   uint16_t  RbcSccReadyActFlg  	       :1;
	   uint16_t  RbcSccReadyActFlgOld		   :1;
	   uint16_t  RbcSccRegenActFlg             :1;
	   uint16_t  InhibitRegenReEntryFlg        :1;

	   //uint16_t  ONSTimeCntAfIngOnFlg          :1;
	   //uint16_t  OverNightSoakingFlg           :1;
	   //uint16_t  ONSoakingTempFlg              :1;
	   //uint16_t  ONSBrakeTimeCntFlg	           :1;
	   //uint16_t  BrakeCntDuringONSFlg		   :1;

	   //uint16_t  RBCStartFlag                  :1;
	   //uint16_t  EBS2_CF_Brk_VirtStkValid      :1;

	   uint16_t  SccBrakeIntendFlg             :1;
	   uint16_t  SccFuctionActFlg              :1;
	   uint16_t  SccFuctionActOldFlg           :1;
	   uint16_t  RbcSccBlendFlag               :1;


	   uint16_t CtrlStartOK                    :1;
	   uint16_t CtrlEndOK                      :1;
	   uint16_t FadeOutCtrlFlag                :1;
	   uint16_t FadeOutCtrlStart               :1;
	   uint16_t FadeOutCtrlEnd                 :1;
	   uint16_t ActivatedBrkCycle              :1;
	   uint16_t DelayRBCReEnter                :1;

	   uint16_t CtrlSlowEndOK                  :1;
	   uint16_t ExitCmdRengenTorq              :1;
	   uint16_t CtrlSlowEndOKOld               :1;
//	   uint16_t ExitRBCSpdFlag                  :1;
	   uint16_t RbcSccSpeedUpdateFlag          :1;
	   uint16_t BlendSpdChangeFlg              :1;
	   uint16_t BlendSpdChangeOldFlg           :1;
	   uint16_t RbcBledSpeedUpdateFlag         :1;

	   uint16_t InhibitFlg                     :1;
	   uint16_t InhibitByFM                    :1;
	   uint16_t InhibitBySetting               :1;
	   uint16_t SpeedInvalidFlg                :1;
	   uint16_t SignalValidAftIgnOn            :1;
	   uint16_t CANSignalInvalid               :1;
	   uint16_t LowSpeedInhibitFlg             :1;
	   uint16_t MaxSpeedInhibitFlg             :1;


	   uint16_t PressToTorqRatioTotal;
	   int16_t  VehicleSpeed;

	   int16_t  DrvIntendedBrkP;
	   uint16_t DrvIntendedBrkTqNm;
	   uint16_t TotalBrakeTqNm;
	   int16_t  VirtualPDT;

	   /* Temp for IDB-RBC CAN Interface */
	   //int16_t  TotalBrakeTP;
	   //int16_t  RegenTorqRate;

	   int8_t   ControlState;
	   int8_t   FadeOutRate;

	   int16_t  MaxPosRegenTqBar;
	   uint16_t MaxPosRegenTqNm;
	   int16_t  TargetRBTqBar;
	   //uint16_t TargetRBTqNm;
	   uint16_t GeneratedMCTqNm ;
	   int16_t  RegenLowSpeedLimit;
	   int16_t  Regen2HydStartSpeed;



}RegenBrakeCooperation_SRCbusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* RBC_TYPES_H_ */
/** @} */
