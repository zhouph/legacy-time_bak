/**
 * @defgroup Mando_Std_Types Mando_Std_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mando_Std_Types.h
 * @brief       Mando Standard Type File
 * @date        2014. 3. 11.
 ******************************************************************************/

#ifndef MANDO_STD_TYPES_H_
#define MANDO_STD_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
==============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
==============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
==============================================================================*/

typedef signed char				int8_t;
typedef unsigned char 			uint8_t;
typedef volatile signed char 	vint8_t;
typedef volatile unsigned char 	vuint8_t;

typedef signed short 			int16_t;
typedef unsigned short 			uint16_t;
typedef volatile signed short 	vint16_t;
typedef volatile unsigned short vuint16_t;

typedef signed int 				int32_t;
typedef unsigned int 			uint32_t;
typedef volatile signed int		vint32_t;
typedef volatile unsigned int 	vuint32_t;

typedef unsigned char       uchar8_t;
typedef signed char         char8_t;
typedef short int           char16_t;
typedef long int            char32_t;

typedef unsigned short int  uchar16_t;
typedef unsigned long int   uchar32_t;

typedef signed char         CHAR;
typedef short int           INT;
typedef long int            LONG;
typedef unsigned char       UCHAR;
typedef unsigned short int  UINT;
typedef unsigned long int   ULONG;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
==============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
==============================================================================*/

/*==============================================================================
 *                  END OF FILE
==============================================================================*/
#endif /* MANDO_STD_TYPES_H_ */
/** @} */
