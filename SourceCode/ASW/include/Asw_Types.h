/**
 * @defgroup Asw_Types Asw_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Asw_Types.h
 * @brief       Template file
 * @date        2014. 7. 7.
 ******************************************************************************/

#ifndef ASW_TYPES_H_
#define ASW_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Mando_Std_Types.h"
#include "Std_Types.h"
#include "Asw_Cfg.h"
#include "Asw_Parameter_Def.h"
#include "L_CommonFunction.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
#define FAL_STATIC STATIC

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef short int			Frac16;
typedef signed long    		Frac32;

typedef struct
{
    uint16 bit0 :1;
    uint16 bit1 :1;
    uint16 bit2 :1;
    uint16 bit3 :1;
    uint16 bit4 :1;
    uint16 bit5 :1;
    uint16 bit6 :1;
    uint16 bit7 :1;
} U8_BIT_STRUCT_t;

typedef unsigned long uBitType;

typedef struct
{
    uBitType bit31    :1;
    uBitType bit30    :1;
    uBitType bit29    :1;
    uBitType bit28    :1;
    uBitType bit27    :1;
    uBitType bit26    :1;
    uBitType bit25    :1;
    uBitType bit24    :1;
    uBitType bit23    :1;
    uBitType bit22    :1;
    uBitType bit21    :1;
    uBitType bit20    :1;
    uBitType bit19    :1;
    uBitType bit18    :1;
    uBitType bit17    :1;
    uBitType bit16    :1;
    uBitType bit15    :1;
    uBitType bit14    :1;
    uBitType bit13    :1;
    uBitType bit12    :1;
    uBitType bit11    :1;
    uBitType bit10    :1;
    uBitType bit9     :1;
    uBitType bit8     :1;
    uBitType bit7     :1;
    uBitType bit6     :1;
    uBitType bit5     :1;
    uBitType bit4     :1;
    uBitType bit3     :1;
    uBitType bit2     :1;
    uBitType bit1     :1;
    uBitType bit0     :1;
}U32_BIT_STRUCT_t;

/* EEPROM Logic Block typedef */
typedef struct
{
    sint32 PdtOffsEolReadVal;
    sint32 PdfOffsEolReadVal;
    sint32 PdtOffsDrvgReadVal;
    sint32 PdfOffsDrvgReadVal;
} Asw_PedlTrvlOffsEepInfo_t;

typedef struct
{
    sint32 PedlSimrPOffsEolReadVal;
    sint32 PedlSimrPOffsDrvgReadVal;
} Asw_PedlSimrPOffsEepInfo_t;

typedef struct
{
    sint32 PistPOffsEolReadVal;
    sint32 PistPOffsDrvgReadVal;
} Asw_PistPOffsEepInfo_t;

typedef struct
{
    sint32 PrimCircPOffsEolReadVal;
    sint32 PrimCircPOffsDrvgReadVal;
    sint32 SecdCircPOffsEolReadVal;
    sint32 SecdCircPOffsDrvgReadVal;
} Asw_CircPOffsEepInfo_t;

//typedef struct
//{
//    sint32 PedlTrvlOffsEolCalcReqFlg;
//    sint32 PSnsrOffsEolCalcReqFlg;
//} Asw_IdbSnsrOffsEolCalcReqByDiagInfo_t;

//typedef struct
//{
//    sint32 PdfOffsEolEepWrReqFlg;
//    sint32 PdtOffsEolEepWrReqFlg;
//    sint32 PedlSimrPOffsEolEepWrReqFlg;
//    sint32 PistPOffsEolEepWrReqFlg;
//    sint32 PrimCircPOffsEolEepWrReqFlg;
//    sint32 SecdCircPOffsEolEepWrReqFlg;
//    sint32 PdfOffsDrvgEepWrReqFlg;
//    sint32 PdtOffsDrvgEepWrReqFlg;
//    sint32 PedlSimrPOffsDrvgEepWrReqFlg;
//    sint32 PistPOffsDrvgEepWrReqFlg;
//    sint32 PrimCircPOffsDrvgEepWrReqFlg;
//    sint32 SecdCircPOffsDrvgEepWrReqFlg;
//} Asw_IdbSnsrOffsEepWrReqInfo_t;

typedef struct
{
	/*ESC Sensor Offset Eep Data*/
	sint32 KSTEER_offset_of_eeprom;
	sint32 KYAW_offset_of_eeprom;
	sint32 KLAT_offset_of_eeprom;
	sint32 fs_yaw_eeprom_offset;
	sint32 KYAW_eep_max;
	sint32 KYAW_eep_min;
	sint32 KSTEER_eep_max;
	sint32 KSTEER_eep_min;
	sint32 KLAT_eep_max;
	sint32 KLAT_eep_min;
	sint32 yaw_still_eep_max;
	sint32 yaw_still_eep_min;
	sint32 lsesps16EEPYawStandOfs;
	sint32 YawTmpEEPMap[31];

	/* ESC Adaptive model Eep Data */
	sint32 AdpvVehMdlVchEep;
	sint32 AdpvVehMdlVcrtRatEep;

	/* TCS DiscTemp Eep Data */
	sint32 btc_tmp_fl_Eep;
	sint32 btc_tmp_fr_Eep;
	sint32 btc_tmp_rl_Eep;
	sint32 btc_tmp_rr_Eep;
	sint32 ee_btcs_data_Eep;

	/* Ax Sensor Offset Eee Data*/

	sint32 LgtSnsrEolOffsEep;
	sint32 LgtSnsrDrvgOffsEep;

}Asw_ActvBrkCtrlEepInfo_t;

typedef struct
{
	/*ESC Sensor Offset Eep Complement Data*/
	/* The meaning of '_c' is a complement */
	sint32 KSTEER_offset_of_eeprom_c;
	sint32 KYAW_offset_of_eeprom_c;
	sint32 KLAT_offset_of_eeprom_c;
	sint32 fs_yaw_eeprom_offset_c;
	sint32 KYAW_eep_max_c;
	sint32 KYAW_eep_min_c;
	sint32 KSTEER_eep_max_c;
	sint32 KSTEER_eep_min_c;
	sint32 KLAT_eep_max_c;
	sint32 KLAT_eep_min_c;
	sint32 yaw_still_eep_max_c;
	sint32 yaw_still_eep_min_c;
	sint32 lsesps16EEPYawStandOfs_c;
	sint32 YawTmpEEPMap_c[31];

}Asw_ActvBrkCtrlEep2Info_t;

typedef struct
{
    Asw_PedlTrvlOffsEepInfo_t PedlTrvlOffsEepInfo;
    Asw_PedlTrvlOffsEepInfo_t PedlTrvlOffsEepInfo2;//complementary number for CS

    Asw_PedlSimrPOffsEepInfo_t PedlSimrPOffsEepInfo;
    Asw_PedlSimrPOffsEepInfo_t PedlSimrPOffsEepInfo2;//complementary number for CS

    Asw_PistPOffsEepInfo_t PistPOffsEepInfo;
    Asw_PistPOffsEepInfo_t PistPOffsEepInfo2;//complementary number for CS

    Asw_CircPOffsEepInfo_t CircPOffsEepInfo;
    Asw_CircPOffsEepInfo_t CircPOffsEepInfo2;//complementary number for CS

//    Asw_IdbSnsrOffsEolCalcReqByDiagInfo_t IdbSnsrOffsEolCalcReqByDiagInfo;
//    Asw_IdbSnsrOffsEepWrReqInfo_t IdbSnsrOffsEepWrReqInfo;
    Asw_ActvBrkCtrlEepInfo_t Asw_ActvBrkCtrlEepInfo;
    Asw_ActvBrkCtrlEep2Info_t Asw_ActvBrkCtrlEep2Info;
    sint32 ReadInvldFlg;       // CGH : It's Written by NvM
    sint32 WrReqCmpldFlg;      // CGH : It's Written by NvM
} Asw_LogicBlock_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ASW_TYPES_H_ */
/** @} */
