/*******************************************************************************
* Project Name:     Mando AHB
* File Name:        LogicParameter.par
* Description:      Common parameters using for Control Logic
* Logic version:    
********************************************************************************
*  Modification Log
*  Date         Author          Description
*  -----        -----           -----
*  9807         KGY           Initial Release
********************************************************************************/

//#define     ENABLE					1
//#define     DISABLE                 0

#define     S8_MTR_PWR_LEVEL_2        2

#define     ASW_U8_MAX                  255
#define     ASW_S16_MAX                 32767
#define     ASW_S16_MIN                -32767
#define     ASW_U16_MAX                 65535
#define     ASW_U16_MIN                 0
#define     U8_IGN_OFF      0// 1 C/S 넘어가면서 변경
#define     U8_IGN_ON      1// 0 C/S 넘어가면서 변경
#define     U8_FULL_CYCLETIME       5

#define S8_PRESS_READY						0
#define S8_PRESS_PREBOOST					1
#define S8_PRESS_BOOST						2
#define S8_PRESS_FADE_OUT					3
#define S8_PRESS_INHIBIT                    4
#define S8_PRESS_NONCONDITION				5

#define S8_BOOST_MODE_NOT_CONTROL			0
#define S8_BOOST_MODE_RESPONSE_L0			1		/* ABS Torque Control Mode 										*/
#define S8_BOOST_MODE_RESPONSE_L1			2		/* Quick Braking , AEB : 400bar/s 								*/
#define S8_BOOST_MODE_RESPONSE_L2			3		/* ESC oversteer (Include ROP), TCS_1_mode : 300bar/s 			*/
#define S8_BOOST_MODE_RESPONSE_L3			4		/* TCS_2_mode, TSP, ESC understeer : 200bar/s  					*/
#define S8_BOOST_MODE_RESPONSE_L4			5		/* Decel control : SCC : 200 bar/s dependent on speed			*/
#define S8_BOOST_MODE_RESPONSE_L5			6		/* Decel control : BBC, RBC : 200 bar/s Normal Speed			*/
#define S8_BOOST_MODE_RESPONSE_L6			7		/* Decel control : BBC, RBC : 200 bar/s Low Speed				*/
#define S8_BOOST_MODE_RESPONSE_L7			8		/* Decel control : BBC, RBC : 200 bar/s Stop Speed				*/
#define S8_BOOST_MODE_RESPONSE_L8			9       /* CBC SLS, EBD 200bar/s Reduce Pressure Control function		*/
#define S8_BOOST_MODE_RESPONSE_L9			10		/* TCS_4_mode, Hold function(HSA, AVH)AC Hold 100bar/s			*/
#define S8_BOOST_MODE_RESPONSE_L10			11		/* Release of Hold function										*/
#define S8_BOOST_MODE_RESPONSE_L11			12		/* ABS Torque to Normal											*/
#define S8_BOOST_MODE_RESPONSE_L12			13		/* PreBoost Mode												*/
#define S8_BOOST_MODE_RESPONSE_L13			14		/* ABS/EBD : Map Only											*/

/* From Arbitrator */
#define S8_BOOST_MODE_REQ_L0				0		/* ABS Torque Control Mode 										*/
#define S8_BOOST_MODE_REQ_L1				1		/* Quick Braking , AEB : 400bar/s 								*/
#define S8_BOOST_MODE_REQ_L2				2		/* ESC oversteer (Include ROP), ABS, TCS_1_mode : 300bar/s 		*/
#define S8_BOOST_MODE_REQ_L3				3		/* TCS_2_mode, TSP, ESC understeer : 200bar/s  					*/
#define S8_BOOST_MODE_REQ_L4				4		/* Decel control : SCC : 200 bar/s dependent on speed			*/
#define S8_BOOST_MODE_REQ_L5				5		/* Decel control : BBC, RBC : 200 bar/s Normal Speed			*/
#define S8_BOOST_MODE_REQ_L6				6		/* CBC SLS, EBD 200bar/s Reduce Pressure function				*/
#define S8_BOOST_MODE_REQ_L7				7		/* TCS_4_mode, Hold function(HSA, AVH)AC Hold 100bar/s			*/
#define S8_BOOST_MODE_REQ_L8				8		/* Release of Hold function										*/
#define S8_BOOST_MODE_REQ_L9				9		/* n-channel multiplexing mode */

#define S16_SPEED_STATE_STOP				0
#define S16_SPEED_STATE_CREEP				1
#define S16_SPEED_STATE_NORMAL				2


#define S8_PCTRL_NOT_FADEOUT_STATE			0
#define S8_PCTRL_FADEOUT_STATE_1			1
#define S8_PCTRL_FADEOUT_STATE_2			2
#define S8_PCTRL_FADEOUT_STATE_3			3


#define S8_STRK_RCVR_READY_STATE	    	0
#define S8_STRK_RCVR_RECOVERY1_STATE    	2
#define S8_STRK_RCVR_RECOVERY2_STATE    	3
#define S8_STRK_RCVR_STABILIZING1_STATE  	5
#define S8_STRK_RCVR_STABILIZING2_STATE  	6
#define S8_STRK_RCVR_INHIBIT_STATE	    	10

#define S16_RECOVERY_MIN_ACT_TIME_MS			(90)/*(L_TIME_90MS) */
#define S16_RECOVERY_END_REQ_TIME_MS			(-10)/*(-L_TIME_10MS)*/
#define S16_RECOVERY_DONT_CARE_TIME_MS			(0)/*(L_TIME_0MS)  */

#define S8_RCVR_RECOMMEND_LVL_1			1
#define S8_RCVR_RECOMMEND_LVL_2			2

#define U8_BOOST_READY	      		 0
#define U8_BOOST_LOW_PRESS           1
#define U8_BOOST_HIGH_PRESS			 2
#define U8_BOOST_BACKWARD 			 3
#define U8_BOOST_HOLD_PRESS   	     4

#define U8_VALVE_STATE_READY			0
#define U8_VALVE_STATE_INITIAL			1
#define U8_VALVE_STATE_ACTIVE			2
#define U8_VALVE_STATE_MAINTAIN			3
#define U8_VALVE_STATE_SUSPECT			4

#define U8_VLV_RESPONSE_LV_SPECIAL		5
#define U8_VLV_RESPONSE_LV_QUICK		4
#define U8_VLV_RESPONSE_LV_NORMAL		3
#define U8_VLV_RESPONSE_LV_MID			2
#define U8_VLV_RESPONSE_LV_SLOW			1

#define U8_VLV_MAX_CURRENT_LEVEL1		0		/* Normal */
#define U8_VLV_MAX_CURRENT_LEVEL2		1		/* Max */

/**********************************************************/
/* TIME CONSTANT (LOOP TIME 5MS)                       */
/**********************************************************/
#define		U8_T_0_MS              0    
#define		U8_T_5_MS              1    
#define     U8_T_10_MS             2  
#define     U8_T_15_MS             3  
#define     U8_T_20_MS             4  
#define     U8_T_25_MS             5  
#define     U8_T_30_MS             6  
#define     U8_T_35_MS             7  
#define     U8_T_40_MS             8  
#define     U8_T_45_MS             9  
#define     U8_T_50_MS             10 
#define     U8_T_55_MS             11 
#define     U8_T_60_MS             12 
#define     U8_T_65_MS             13 
#define     U8_T_70_MS             14 
#define     U8_T_75_MS             15 
#define     U8_T_80_MS             16 
#define     U8_T_85_MS             17 
#define     U8_T_90_MS             18 
#define     U8_T_95_MS             19 
#define     U8_T_100_MS            20 
#define     U8_T_110_MS            22 
#define     U8_T_120_MS            24 
#define     U8_T_130_MS            26 
#define     U8_T_140_MS            28 
#define     U8_T_150_MS            30 
#define     U8_T_160_MS            32 
#define     U8_T_170_MS            34 
#define     U8_T_180_MS            36 
#define     U8_T_190_MS            38 
#define     U8_T_200_MS            40  
#define     U8_T_250_MS            50 
#define     U8_T_300_MS            60  
#define     U8_T_350_MS            70   
#define     U8_T_400_MS            80   
#define     U8_T_450_MS            90 
#define     U8_T_500_MS            100
#define     U8_T_600_MS            120
#define     U8_T_700_MS            140
#define     U8_T_800_MS            160
#define     U8_T_900_MS            180
#define     U8_T_1000_MS           200
#define		U8_T_1200_MS		   240  

#define		U16_T_1_S				200  
#define     U16_T_2_S               400 
#define     U16_T_3_S               600 
#define     U16_T_4_S               800 
#define     U16_T_5_S               1000
#define     U16_T_6_S               1200
#define     U16_T_7_S               1400
#define     U16_T_8_S               1600
#define     U16_T_9_S               1800
#define     U16_T_10_S              2000
#define     U16_T_11_S              2200
#define     U16_T_12_S              2400
#define     U16_T_13_S              2600
#define     U16_T_14_S              2800
#define     U16_T_15_S              3000
#define     U16_T_16_S              3200
#define     U16_T_17_S              3400
#define     U16_T_18_S              3600
#define     U16_T_19_S              3800
#define     U16_T_20_S              4000
#define     U16_T_21_S              4200
#define     U16_T_22_S              4400
#define     U16_T_23_S              4600
#define     U16_T_24_S              4800
#define     U16_T_25_S              5000
#define     U16_T_26_S              5200
#define     U16_T_27_S              5400
#define     U16_T_28_S              5600
#define     U16_T_29_S              5800
#define     U16_T_30_S              6000
#define     U16_T_50_S              10000
#define     U16_T_60_S              12000
#define     U16_T_1_MIN             12000
#define     U16_T_1M_30S            18000 
#define     U16_T_2_MIN             24000
#define     U16_T_2M_30S            30000
#define     U16_T_3_MIN             36000
#define     U16_T_4_MIN             48000
#define     U16_T_5_MIN             60000

/**********************************************************/
/* MOTOR VOLTAGE (RESOLUTION 0.01V)                       */
/**********************************************************/
#define     U16_MSC_0_V             0
#define     U16_MSC_0_1_V           10
#define     U16_MSC_0_25_V          25
#define     U16_MSC_0_5_V           50
#define     U16_MSC_0_75_V          75
#define     U16_MSC_1_V             100
#define     U16_MSC_1_25_V          125
#define     U16_MSC_1_5_V           150
#define     U16_MSC_1_75_V          175
#define     U16_MSC_2_V             200
#define     U16_MSC_2_25_V          225
#define     U16_MSC_2_5_V           250
#define     U16_MSC_2_75_V          275
#define     U16_MSC_3_V             300
#define     U16_MSC_3_25_V          325
#define     U16_MSC_3_5_V           350
#define     U16_MSC_3_75_V          375
#define     U16_MSC_4_V             400
#define     U16_MSC_4_25_V          425
#define     U16_MSC_4_5_V           450
#define     U16_MSC_4_75_V          475
#define     U16_MSC_5_V             500
#define     U16_MSC_5_25_V          525
#define     U16_MSC_5_5_V           550
#define     U16_MSC_5_75_V          575
#define     U16_MSC_6_V             600
#define     U16_MSC_6_25_V          625
#define     U16_MSC_6_5_V           650
#define     U16_MSC_6_75_V          675
#define     U16_MSC_7_V             700
#define     U16_MSC_7_25_V          725
#define     U16_MSC_7_5_V           750
#define     U16_MSC_7_75_V          775
#define     U16_MSC_8_V             800
#define     U16_MSC_8_25_V          825
#define     U16_MSC_8_5_V           850
#define     U16_MSC_8_75_V          875
#define     U16_MSC_9_V             900
#define     U16_MSC_9_25_V          925
#define     U16_MSC_9_5_V           950
#define     U16_MSC_9_75_V          975
#define     U16_MSC_10_V            1000
#define     U16_MSC_10_25_V         1025
#define     U16_MSC_10_5_V          1050
#define     U16_MSC_10_75_V         1075
#define     U16_MSC_11_V            1100
#define     U16_MSC_11_25_V         1125
#define     U16_MSC_11_5_V          1150
#define     U16_MSC_11_75_V         1175
#define     U16_MSC_12_V            1200
#define     U16_MSC_12_25_V         1225
#define     U16_MSC_12_5_V          1250
#define     U16_MSC_12_75_V         1275
#define     U16_MSC_13_V            1300
#define     U16_MSC_13_25_V         1325
#define     U16_MSC_13_5_V          1350
#define     U16_MSC_13_75_V         1375
#define     U16_MSC_14_V            1400
#define     U16_MSC_14_25_V         1425
#define     U16_MSC_14_5_V          1450
#define     U16_MSC_14_75_V         1475
#define     U16_MSC_15_V            1500
#define     U16_MSC_15_25_V         1525
#define     U16_MSC_15_5_V          1550
#define     U16_MSC_15_75_V         1575
#define     U16_MSC_16_V            1600

/**********************************************************/
/* PRESSURE (RESOLUTION 0.1bar)                           */
/**********************************************************/

#define     S16_PRESS_RESOL                  10
#define     S16_MPRESS_0_BAR                 0
#define 	S16_MPRESS_0_1_BAR				 1	
#define     S16_MPRESS_0_2_BAR               2
#define     S16_MPRESS_0_3_BAR               3
#define     S16_MPRESS_0_5_BAR               5
#define     S16_MPRESS_0_7_BAR               7
#define     S16_MPRESS_0_8_BAR               8
#define     S16_MPRESS_1_BAR                 10
#define     S16_MPRESS_1_3_BAR               13
#define     S16_MPRESS_1_5_BAR               15
#define     S16_MPRESS_2_BAR                 20
#define     S16_MPRESS_3_BAR                 30
#define     S16_MPRESS_3_5_BAR               35
#define     S16_MPRESS_3_6_BAR               36
#define     S16_MPRESS_4_BAR                 40
#define     S16_MPRESS_5_BAR                 50
#define     S16_MPRESS_5_5_BAR               55
#define     S16_MPRESS_6_BAR                 60
#define     S16_MPRESS_7_BAR                 70
#define     S16_MPRESS_8_BAR                 80
#define     S16_MPRESS_8_5_BAR               85
#define     S16_MPRESS_9_BAR                 90
#define     S16_MPRESS_10_BAR                100
#define     S16_MPRESS_11_BAR                110
#define     S16_MPRESS_12_BAR                120
#define     S16_MPRESS_13_BAR                130
#define     S16_MPRESS_14_BAR                140
#define     S16_MPRESS_15_BAR                150
#define     S16_MPRESS_16_BAR                160
#define     S16_MPRESS_18_BAR                180
#define     S16_MPRESS_19_BAR                190
#define     S16_MPRESS_20_BAR                200
#define     S16_MPRESS_21_BAR                210
#define     S16_MPRESS_23_BAR                230
#define     S16_MPRESS_25_BAR                250
#define     S16_MPRESS_26_BAR                260
#define     S16_MPRESS_27_BAR                270
#define     S16_MPRESS_28_BAR                280
#define     S16_MPRESS_30_BAR                300
#define     S16_MPRESS_33_BAR                330
#define     S16_MPRESS_34_BAR                340
#define     S16_MPRESS_35_BAR                350
#define     S16_MPRESS_36_BAR                360
#define     S16_MPRESS_36_5_BAR                365
#define     S16_MPRESS_37_BAR                370
#define     S16_MPRESS_38_BAR                380
#define     S16_MPRESS_40_BAR                400
#define     S16_MPRESS_43_BAR                430
#define     S16_MPRESS_44_BAR                440
#define     S16_MPRESS_45_BAR                450
#define     S16_MPRESS_46_BAR                460
#define     S16_MPRESS_48_BAR                480
#define     S16_MPRESS_50_BAR                500
#define     S16_MPRESS_51_BAR                510
#define     S16_MPRESS_53_BAR                530
#define     S16_MPRESS_55_BAR                550
#define     S16_MPRESS_60_BAR                600
#define     S16_MPRESS_63_BAR                630
#define     S16_MPRESS_65_BAR                650
#define     S16_MPRESS_67_BAR                670
#define     S16_MPRESS_68_BAR                680
#define     S16_MPRESS_70_BAR                700
#define     S16_MPRESS_75_BAR                750
#define     S16_MPRESS_78_BAR                780
#define     S16_MPRESS_80_BAR                800
#define     S16_MPRESS_82_BAR                820
#define     S16_MPRESS_85_BAR                850
#define     S16_MPRESS_90_BAR                900
#define     S16_MPRESS_95_BAR                950
#define     S16_MPRESS_100_BAR               1000
#define     S16_MPRESS_105_BAR               1050
#define     S16_MPRESS_110_BAR               1100
#define     S16_MPRESS_115_BAR               1150
#define     S16_MPRESS_120_BAR               1200
#define     S16_MPRESS_130_BAR               1300
#define     S16_MPRESS_135_BAR               1350
#define     S16_MPRESS_140_BAR               1400
#define     S16_MPRESS_145_BAR               1450
#define     S16_MPRESS_150_BAR               1500
#define     S16_MPRESS_155_BAR               1550
#define     S16_MPRESS_160_BAR               1600
#define     S16_MPRESS_165_BAR               1650
#define     S16_MPRESS_170_BAR               1700
#define     S16_MPRESS_175_BAR               1750
#define     S16_MPRESS_180_BAR               1800
#define     S16_MPRESS_183_BAR               1830
#define     S16_MPRESS_190_BAR               1900
#define     S16_MPRESS_200_BAR               2000
#define     S16_MPRESS_400_BAR               4000
#define     S16_MPRESS_600_BAR               6000

/**********************************************************/
/* PRESSURE (RESOLUTION 0.01bar)                           */
/**********************************************************/

#define     S16_PCTRL_PRESS_0_BAR                (0)
#define     S16_PCTRL_PRESS_0_01_BAR             (1)
#define     S16_PCTRL_PRESS_0_02_BAR             (2)
#define     S16_PCTRL_PRESS_0_03_BAR             (3)
#define     S16_PCTRL_PRESS_0_04_BAR             (4)
#define     S16_PCTRL_PRESS_0_05_BAR             (5)
#define     S16_PCTRL_PRESS_0_06_BAR             (6)
#define     S16_PCTRL_PRESS_0_07_BAR             (7)
#define     S16_PCTRL_PRESS_0_08_BAR             (8)
#define     S16_PCTRL_PRESS_0_09_BAR             (9)
#define     S16_PCTRL_PRESS_0_1_BAR              (10)
#define     S16_PCTRL_PRESS_0_2_BAR              (20)
#define     S16_PCTRL_PRESS_0_3_BAR              (30)
#define     S16_PCTRL_PRESS_0_5_BAR              (50)
#define     S16_PCTRL_PRESS_0_7_BAR              (70)
#define     S16_PCTRL_PRESS_0_8_BAR              (80)
#define     S16_PCTRL_PRESS_1_BAR                (100)
#define     S16_PCTRL_PRESS_1_3_BAR              (130)
#define     S16_PCTRL_PRESS_1_5_BAR              (150)
#define     S16_PCTRL_PRESS_2_BAR                (200)
#define     S16_PCTRL_PRESS_3_BAR                (300)
#define     S16_PCTRL_PRESS_3_5_BAR              (350)
#define     S16_PCTRL_PRESS_3_6_BAR              (360)
#define     S16_PCTRL_PRESS_4_BAR                (400)
#define     S16_PCTRL_PRESS_5_BAR                (500)
#define     S16_PCTRL_PRESS_5_5_BAR              (550)
#define     S16_PCTRL_PRESS_6_BAR                (600)
#define     S16_PCTRL_PRESS_7_BAR                (700)
#define     S16_PCTRL_PRESS_8_BAR                (800)
#define     S16_PCTRL_PRESS_8_5_BAR              (850)
#define     S16_PCTRL_PRESS_9_BAR                (900)
#define     S16_PCTRL_PRESS_10_BAR               (1000)
#define     S16_PCTRL_PRESS_11_BAR               (1100)
#define     S16_PCTRL_PRESS_12_BAR               (1200)
#define     S16_PCTRL_PRESS_13_BAR               (1300)
#define     S16_PCTRL_PRESS_14_BAR               (1400)
#define     S16_PCTRL_PRESS_15_BAR               (1500)
#define     S16_PCTRL_PRESS_16_BAR               (1600)
#define     S16_PCTRL_PRESS_18_BAR               (1800)
#define     S16_PCTRL_PRESS_19_BAR               (1900)
#define     S16_PCTRL_PRESS_20_BAR               (2000)
#define     S16_PCTRL_PRESS_21_BAR               (2100)
#define     S16_PCTRL_PRESS_23_BAR               (2300)
#define     S16_PCTRL_PRESS_25_BAR               (2500)
#define     S16_PCTRL_PRESS_26_BAR               (2600)
#define     S16_PCTRL_PRESS_27_BAR               (2700)
#define     S16_PCTRL_PRESS_28_BAR               (2800)
#define     S16_PCTRL_PRESS_30_BAR               (3000)
#define     S16_PCTRL_PRESS_33_BAR               (3300)
#define     S16_PCTRL_PRESS_34_BAR               (3400)
#define     S16_PCTRL_PRESS_35_BAR               (3500)
#define     S16_PCTRL_PRESS_36_BAR               (3600)
#define     S16_PCTRL_PRESS_37_BAR               (3700)
#define     S16_PCTRL_PRESS_38_BAR               (3800)
#define     S16_PCTRL_PRESS_40_BAR               (4000)
#define     S16_PCTRL_PRESS_43_BAR               (4300)
#define     S16_PCTRL_PRESS_44_BAR               (4400)
#define     S16_PCTRL_PRESS_45_BAR               (4500)
#define     S16_PCTRL_PRESS_46_BAR               (4600)
#define     S16_PCTRL_PRESS_48_BAR               (4800)
#define     S16_PCTRL_PRESS_50_BAR               (5000)
#define     S16_PCTRL_PRESS_51_BAR               (5100)
#define     S16_PCTRL_PRESS_53_BAR               (5300)
#define     S16_PCTRL_PRESS_55_BAR               (5500)
#define     S16_PCTRL_PRESS_60_BAR               (6000)
#define     S16_PCTRL_PRESS_63_BAR               (6300)
#define     S16_PCTRL_PRESS_65_BAR               (6500)
#define     S16_PCTRL_PRESS_67_BAR               (6700)
#define     S16_PCTRL_PRESS_68_BAR               (6800)
#define     S16_PCTRL_PRESS_70_BAR               (7000)
#define     S16_PCTRL_PRESS_75_BAR               (7500)
#define     S16_PCTRL_PRESS_78_BAR               (7800)
#define     S16_PCTRL_PRESS_80_BAR               (8000)
#define     S16_PCTRL_PRESS_82_BAR               (8200)
#define     S16_PCTRL_PRESS_85_BAR               (8500)
#define     S16_PCTRL_PRESS_90_BAR               (9000)
#define     S16_PCTRL_PRESS_95_BAR               (9500)
#define     S16_PCTRL_PRESS_100_BAR              (10000)
#define     S16_PCTRL_PRESS_105_BAR              (10500)
#define     S16_PCTRL_PRESS_110_BAR              (11000)
#define     S16_PCTRL_PRESS_115_BAR              (11500)
#define     S16_PCTRL_PRESS_120_BAR              (12000)
#define     S16_PCTRL_PRESS_130_BAR              (13000)
#define     S16_PCTRL_PRESS_135_BAR              (13500)
#define     S16_PCTRL_PRESS_140_BAR              (14000)
#define     S16_PCTRL_PRESS_145_BAR              (14500)
#define     S16_PCTRL_PRESS_150_BAR              (15000)
#define     S16_PCTRL_PRESS_155_BAR              (15500)
#define     S16_PCTRL_PRESS_160_BAR              (16000)
#define     S16_PCTRL_PRESS_165_BAR              (16500)
#define     S16_PCTRL_PRESS_170_BAR              (17000)
#define     S16_PCTRL_PRESS_175_BAR              (17500)
#define     S16_PCTRL_PRESS_180_BAR              (18000)
#define     S16_PCTRL_PRESS_183_BAR              (18300)
#define     S16_PCTRL_PRESS_190_BAR              (19000)
#define     S16_PCTRL_PRESS_200_BAR              (20000)
#define     S16_PCTRL_PRESS_250_BAR              (25000)

#define     S16_PRESS_RESOL_10_TO_100(input)     ((input)*S16_MPRESS_1_BAR)
#define     S16_PRESS_RESOL_100_TO_10(input)     ((input)/S16_MPRESS_1_BAR)

/**********************************************************/
/* PRESSURE RATE (RESOLUTION 1bar/sec)                           */
/**********************************************************/

#define     S16_P_RATE_0_BPS                    (0)
#define     S16_P_RATE_1_BPS                    (1)
#define     S16_P_RATE_2_BPS                    (2)
#define     S16_P_RATE_3_BPS                    (3)
#define     S16_P_RATE_4_BPS                    (4)
#define     S16_P_RATE_5_BPS                    (5)
#define     S16_P_RATE_6_BPS                    (6)
#define     S16_P_RATE_7_BPS                    (7)
#define     S16_P_RATE_8_BPS                    (8)
#define     S16_P_RATE_9_BPS                    (9)
#define     S16_P_RATE_10_BPS                   (10)
#define     S16_P_RATE_11_BPS                   (11)
#define     S16_P_RATE_12_BPS                   (12)
#define     S16_P_RATE_13_BPS                   (13)
#define     S16_P_RATE_14_BPS                   (14)
#define     S16_P_RATE_15_BPS                   (15)
#define     S16_P_RATE_16_BPS                   (16)
#define     S16_P_RATE_18_BPS                   (18)
#define     S16_P_RATE_19_BPS                   (19)
#define     S16_P_RATE_20_BPS                   (20)
#define     S16_P_RATE_21_BPS                   (21)
#define     S16_P_RATE_23_BPS                   (23)
#define     S16_P_RATE_25_BPS                   (25)
#define     S16_P_RATE_26_BPS                   (26)
#define     S16_P_RATE_27_BPS                   (27)
#define     S16_P_RATE_28_BPS                   (28)
#define     S16_P_RATE_30_BPS                   (30)
#define     S16_P_RATE_33_BPS                   (33)
#define     S16_P_RATE_34_BPS                   (34)
#define     S16_P_RATE_35_BPS                   (35)
#define     S16_P_RATE_36_BPS                   (36)
#define     S16_P_RATE_37_BPS                   (37)
#define     S16_P_RATE_38_BPS                   (38)
#define     S16_P_RATE_40_BPS                   (40)
#define     S16_P_RATE_43_BPS                   (43)
#define     S16_P_RATE_44_BPS                   (44)
#define     S16_P_RATE_45_BPS                   (45)
#define     S16_P_RATE_46_BPS                   (46)
#define     S16_P_RATE_48_BPS                   (48)
#define     S16_P_RATE_50_BPS                   (50)
#define     S16_P_RATE_51_BPS                   (51)
#define     S16_P_RATE_53_BPS                   (53)
#define     S16_P_RATE_55_BPS                   (55)
#define     S16_P_RATE_60_BPS                   (60)
#define     S16_P_RATE_63_BPS                   (63)
#define     S16_P_RATE_65_BPS                   (65)
#define     S16_P_RATE_67_BPS                   (67)
#define     S16_P_RATE_68_BPS                   (68)
#define     S16_P_RATE_70_BPS                   (70)
#define     S16_P_RATE_75_BPS                   (75)
#define     S16_P_RATE_78_BPS                   (78)
#define     S16_P_RATE_80_BPS                   (80)
#define     S16_P_RATE_82_BPS                   (82)
#define     S16_P_RATE_85_BPS                   (85)
#define     S16_P_RATE_90_BPS                   (90)
#define     S16_P_RATE_95_BPS                   (95)
#define     S16_P_RATE_100_BPS                  (100)
#define     S16_P_RATE_105_BPS                  (105)
#define     S16_P_RATE_110_BPS                  (110)
#define     S16_P_RATE_115_BPS                  (115)
#define     S16_P_RATE_120_BPS                  (120)
#define     S16_P_RATE_130_BPS                  (130)
#define     S16_P_RATE_135_BPS                  (135)
#define     S16_P_RATE_140_BPS                  (140)
#define     S16_P_RATE_145_BPS                  (145)
#define     S16_P_RATE_150_BPS                  (150)
#define     S16_P_RATE_155_BPS                  (155)
#define     S16_P_RATE_160_BPS                  (160)
#define     S16_P_RATE_165_BPS                  (165)
#define     S16_P_RATE_170_BPS                  (170)
#define     S16_P_RATE_175_BPS                  (175)
#define     S16_P_RATE_180_BPS                  (180)
#define     S16_P_RATE_183_BPS                  (183)
#define     S16_P_RATE_190_BPS                  (190)
#define     S16_P_RATE_200_BPS                  (200)
#define     S16_P_RATE_250_BPS                  (250)
#define     S16_P_RATE_300_BPS                  (300)
#define     S16_P_RATE_350_BPS                  (350)
#define     S16_P_RATE_400_BPS                  (400)
#define     S16_P_RATE_500_BPS                  (500)
#define     S16_P_RATE_600_BPS                  (600)
#define     S16_P_RATE_700_BPS                  (700)
#define     S16_P_RATE_800_BPS                  (800)
#define     S16_P_RATE_900_BPS                  (900)
#define     S16_P_RATE_1000_BPS                 (1000)



// pressure resolution 0.01bar
#define S16_CIRC_PRESS_0_BAR	0
#define S16_CIRC_PRESS_0_5_BAR	50
#define S16_CIRC_PRESS_1_BAR	100
#define S16_CIRC_PRESS_2_BAR	200
#define S16_CIRC_PRESS_7_BAR	700
#define S16_CIRC_PRESS_15_BAR	1500
#define S16_CIRC_PRESS_200_BAR	20000


/**********************************************************/
/* PEDAL TRAVEL (RESOLUTION 0.1mm)                        */
/**********************************************************/

#define		S16_TRAVEL_0_MM                  0  
#define		S16_TRAVEL_0_1_MM                1
#define		S16_TRAVEL_0_2_MM                2
#define		S16_TRAVEL_0_3_MM                3
#define		S16_TRAVEL_0_5_MM                5  
#define		S16_TRAVEL_1_MM                  10  
#define		S16_TRAVEL_1_5_MM                15  
#define     S16_TRAVEL_2_MM                  20
#define     S16_TRAVEL_2_5_MM                25  
#define     S16_TRAVEL_3_MM                  30  
#define     S16_TRAVEL_4_MM                  40  
#define     S16_TRAVEL_5_MM                  50  
#define     S16_TRAVEL_6_MM                  60
#define     S16_TRAVEL_6_5_MM                65
#define     S16_TRAVEL_7_MM                  70  
#define     S16_TRAVEL_8_MM                  80  
#define     S16_TRAVEL_9_MM                  90  
#define     S16_TRAVEL_10_MM                 100 
#define     S16_TRAVEL_11_MM                 110 
#define     S16_TRAVEL_12_MM                 120
#define     S16_TRAVEL_12_5_MM               125
#define     S16_TRAVEL_13_MM                 130 
#define     S16_TRAVEL_14_MM                 140 
#define     S16_TRAVEL_15_MM                 150 
#define     S16_TRAVEL_16_MM                 160 
#define     S16_TRAVEL_17_MM                 170 
#define     S16_TRAVEL_18_MM                 180 
#define     S16_TRAVEL_19_MM                 190 
#define     S16_TRAVEL_20_MM                 200 
#define     S16_TRAVEL_20_5_MM               205 
#define     S16_TRAVEL_21_MM                 210 
#define     S16_TRAVEL_22_MM                 220 
#define     S16_TRAVEL_23_MM                 230 
#define     S16_TRAVEL_24_MM                 240 
#define     S16_TRAVEL_25_MM                 250 
#define     S16_TRAVEL_26_MM                 260 
#define     S16_TRAVEL_27_MM                 270 
#define     S16_TRAVEL_28_MM                 280 
#define     S16_TRAVEL_29_MM                 290 
#define     S16_TRAVEL_29_5_MM               295 
#define     S16_TRAVEL_30_MM                 300 
#define     S16_TRAVEL_31_MM                 310 
#define     S16_TRAVEL_32_MM                 320 
#define     S16_TRAVEL_33_MM                 330 
#define     S16_TRAVEL_34_MM                 340 
#define     S16_TRAVEL_35_MM                 350 
#define     S16_TRAVEL_36_MM                 360 
#define     S16_TRAVEL_37_MM                 370 
#define     S16_TRAVEL_38_MM                 380 
#define     S16_TRAVEL_38_5_MM               385 
#define     S16_TRAVEL_39_MM                 390 
#define     S16_TRAVEL_40_MM                 400 
#define     S16_TRAVEL_41_MM                 410 
#define     S16_TRAVEL_42_MM                 420 
#define     S16_TRAVEL_43_MM                 430 
#define     S16_TRAVEL_44_MM                 440 
#define     S16_TRAVEL_45_MM                 450 
#define     S16_TRAVEL_46_MM                 460 
#define     S16_TRAVEL_47_MM                 470 
#define     S16_TRAVEL_48_MM                 480 
#define     S16_TRAVEL_49_MM                 490 
#define     S16_TRAVEL_50_MM                 500 
#define     S16_TRAVEL_51_MM                 510 
#define     S16_TRAVEL_52_MM                 520 
#define     S16_TRAVEL_53_MM                 530 
#define     S16_TRAVEL_54_MM                 540 
#define     S16_TRAVEL_55_MM                 550 
#define     S16_TRAVEL_56_MM                 560 
#define     S16_TRAVEL_57_MM                 570 
#define     S16_TRAVEL_58_MM                 580 
#define     S16_TRAVEL_59_MM                 590 
#define     S16_TRAVEL_60_MM                 600 
#define     S16_TRAVEL_61_MM                 610 
#define     S16_TRAVEL_62_MM                 620 
#define     S16_TRAVEL_63_MM                 630 
#define     S16_TRAVEL_64_MM                 640 
#define     S16_TRAVEL_65_MM                 650 
#define     S16_TRAVEL_66_MM                 660 
#define     S16_TRAVEL_67_MM                 670 
#define     S16_TRAVEL_68_MM                 680 
#define     S16_TRAVEL_69_MM                 690 
#define     S16_TRAVEL_70_MM                 700 
#define     S16_TRAVEL_71_MM                 710 
#define     S16_TRAVEL_72_MM                 720 
#define     S16_TRAVEL_73_MM                 730 
#define     S16_TRAVEL_74_MM                 740 
#define     S16_TRAVEL_75_MM                 750 
#define     S16_TRAVEL_76_MM                 760 
#define     S16_TRAVEL_77_MM                 770 
#define     S16_TRAVEL_78_MM                 780 
#define     S16_TRAVEL_79_MM                 790 
#define     S16_TRAVEL_80_MM                 800 
#define     S16_TRAVEL_81_MM                 810 
#define     S16_TRAVEL_82_MM                 820 
#define     S16_TRAVEL_83_MM                 830 
#define     S16_TRAVEL_84_MM                 840 
#define     S16_TRAVEL_85_MM                 850 
#define     S16_TRAVEL_86_MM                 860 
#define     S16_TRAVEL_87_MM                 870 
#define     S16_TRAVEL_88_MM                 880 
#define     S16_TRAVEL_89_MM                 890 
#define     S16_TRAVEL_90_MM                 900 
#define     S16_TRAVEL_91_MM                 910 
#define     S16_TRAVEL_92_MM                 920 
#define     S16_TRAVEL_93_MM                 930 
#define     S16_TRAVEL_94_MM                 940 
#define     S16_TRAVEL_95_MM                 950 
#define     S16_TRAVEL_96_MM                 960 
#define     S16_TRAVEL_97_MM                 970 
#define     S16_TRAVEL_98_MM                 980 
#define     S16_TRAVEL_99_MM                 990 
#define     S16_TRAVEL_100_MM                1000
#define     S16_TRAVEL_101_MM                1010
#define     S16_TRAVEL_102_MM                1020
#define     S16_TRAVEL_103_MM                1030
#define     S16_TRAVEL_104_MM                1040
#define     S16_TRAVEL_105_MM                1050
#define     S16_TRAVEL_106_MM                1060
#define     S16_TRAVEL_107_MM                1070
#define     S16_TRAVEL_108_MM                1080
#define     S16_TRAVEL_109_MM                1090
#define     S16_TRAVEL_110_MM                1100
#define     S16_TRAVEL_111_MM                1110
#define     S16_TRAVEL_112_MM                1120
#define     S16_TRAVEL_113_MM                1130
#define     S16_TRAVEL_114_MM                1140
#define     S16_TRAVEL_115_MM                1150
#define     S16_TRAVEL_116_MM                1160
#define     S16_TRAVEL_117_MM                1170
#define     S16_TRAVEL_118_MM                1180
#define     S16_TRAVEL_119_MM                1190
#define     S16_TRAVEL_120_MM                1200
#define     S16_TRAVEL_121_MM                1210
#define     S16_TRAVEL_122_MM                1220
#define     S16_TRAVEL_123_MM                1230
#define     S16_TRAVEL_124_MM                1240
#define     S16_TRAVEL_125_MM                1250
#define     S16_TRAVEL_126_MM                1260
#define     S16_TRAVEL_127_MM                1270
#define     S16_TRAVEL_128_MM                1280
#define     S16_TRAVEL_129_MM                1290
#define     S16_TRAVEL_130_MM                1300
#define     S16_TRAVEL_131_MM                1310
#define     S16_TRAVEL_132_MM                1320
#define     S16_TRAVEL_133_MM                1330
#define     S16_TRAVEL_134_MM                1340
#define     S16_TRAVEL_135_MM                1350
#define     S16_TRAVEL_136_MM                1360
#define     S16_TRAVEL_137_MM                1370
#define     S16_TRAVEL_138_MM                1380
#define     S16_TRAVEL_139_MM                1390
#define     S16_TRAVEL_140_MM                1400
#define     S16_TRAVEL_141_MM                1410
#define     S16_TRAVEL_142_MM                1420
#define     S16_TRAVEL_143_MM                1430
#define     S16_TRAVEL_144_MM                1440
#define     S16_TRAVEL_145_MM                1450
#define     S16_TRAVEL_146_MM                1460
#define     S16_TRAVEL_147_MM                1470
#define     S16_TRAVEL_148_MM                1480
#define     S16_TRAVEL_149_MM                1490
#define     S16_TRAVEL_150_MM                1500
#define     S16_TRAVEL_151_MM                1510
#define     S16_TRAVEL_152_MM                1520
#define     S16_TRAVEL_153_MM                1530
#define     S16_TRAVEL_154_MM                1540
#define     S16_TRAVEL_155_MM                1550


/**********************************************************/
/* Speed (RESOLUTION 0.125kph)                            */
/**********************************************************/

#define     S16_SPEED_0_125_KPH              1
#define     S16_SPEED_0_25_KPH               2
#define     S16_SPEED_0_5_KPH                4
#define     S16_SPEED_1_KPH                  8
#define     S16_SPEED_2_KPH                  16
#define     S16_SPEED_5_KPH                  40
#define		S16_SPEED_6_KPH					 48
#define		S16_SPEED_7_KPH					 56	
#define     S16_SPEED_10_KPH                 80
#define     S16_SPEED_17_KPH                 136
#define     S16_SPEED_20_KPH                 160
#define     S16_SPEED_30_KPH                 240
#define     S16_SPEED_40_KPH                 320
#define     S16_SPEED_50_KPH                 400
#define     S16_SPEED_60_KPH                 480
#define     S16_SPEED_70_KPH                 560
#define     S16_SPEED_80_KPH                 640
#define     S16_SPEED_90_KPH                 720
#define     S16_SPEED_100_KPH                800
#define     S16_SPEED_110_KPH                880
#define     S16_SPEED_120_KPH                960
#define     S16_SPEED_130_KPH                1040
#define     S16_SPEED_140_KPH                1120
#define     S16_SPEED_150_KPH                1200
#define     S16_SPEED_160_KPH                1280
#define     S16_SPEED_170_KPH                1360
#define     S16_SPEED_180_KPH                1440
#define     S16_SPEED_190_KPH                1520
#define     S16_SPEED_200_KPH                1600
#define     S16_SPEED_210_KPH                1680
#define     S16_SPEED_220_KPH                1760
#define     S16_SPEED_230_KPH                1840
#define     S16_SPEED_240_KPH                1920
#define     S16_SPEED_250_KPH                2000

#define     S16_SPEED_MIN                    S16_SPEED_0_125_KPH
#define     S16_SPEED_MAX                    S16_SPEED_250_KPH

/**********************************************************/
/* Accel Position (RESOLUTION  1% )                       */
/**********************************************************/
#define    S16_ACCEL_01_PERCENT               1
#define    S16_ACCEL_02_PERCENT               2
#define    S16_ACCEL_03_PERCENT               3
#define    S16_ACCEL_04_PERCENT               4
#define    S16_ACCEL_05_PERCENT               5
#define    S16_ACCEL_06_PERCENT               6
#define    S16_ACCEL_07_PERCENT               7
#define    S16_ACCEL_08_PERCENT               8
#define    S16_ACCEL_09_PERCENT               9
#define    S16_ACCEL_10_PERCENT               10
#define    S16_ACCEL_15_PERCENT               15
#define    S16_ACCEL_20_PERCENT               20
#define    S16_ACCEL_25_PERCENT               25
#define    S16_ACCEL_30_PERCENT               30
#define    S16_ACCEL_35_PERCENT               35
#define    S16_ACCEL_40_PERCENT               40
#define    S16_ACCEL_45_PERCENT               45
#define    S16_ACCEL_50_PERCENT               50
#define    S16_ACCEL_55_PERCENT               55
#define    S16_ACCEL_60_PERCENT               60
#define    S16_ACCEL_65_PERCENT               65
#define    S16_ACCEL_70_PERCENT               70
#define    S16_ACCEL_75_PERCENT               75
#define    S16_ACCEL_80_PERCENT               80
#define    S16_ACCEL_85_PERCENT               85
#define    S16_ACCEL_90_PERCENT               90
#define    S16_ACCEL_95_PERCENT               95
#define    S16_ACCEL_100_PERCENT              100


/**********************************************************/
/* Valve Current (RESOLUTION 1mA)                         */
/**********************************************************/

#define		S16_CURRENT_0_MA                  0  
#define		S16_CURRENT_1_MA                  1
#define		S16_CURRENT_5_MA                  5
#define		S16_CURRENT_10_MA                 10
#define		S16_CURRENT_20_MA                 20
#define		S16_CURRENT_30_MA                 30
#define		S16_CURRENT_40_MA                 40
#define		S16_CURRENT_50_MA                 50
#define		S16_CURRENT_60_MA                 60
#define		S16_CURRENT_70_MA                 70
#define		S16_CURRENT_80_MA                 80
#define		S16_CURRENT_90_MA                 90
#define		S16_CURRENT_100_MA                100
#define		S16_CURRENT_110_MA                110
#define		S16_CURRENT_120_MA                120
#define		S16_CURRENT_130_MA                130
#define		S16_CURRENT_140_MA                140
#define		S16_CURRENT_150_MA                150
#define		S16_CURRENT_160_MA                160
#define		S16_CURRENT_170_MA                170
#define		S16_CURRENT_180_MA                180
#define		S16_CURRENT_190_MA                190
#define		S16_CURRENT_200_MA                200
#define		S16_CURRENT_250_MA  			  250
#define		S16_CURRENT_300_MA                300
#define		S16_CURRENT_400_MA                400
#define		S16_CURRENT_500_MA                500
#define		S16_CURRENT_600_MA                600
#define		S16_CURRENT_700_MA                700
#define		S16_CURRENT_800_MA                800
#define		S16_CURRENT_900_MA                900
#define		S16_CURRENT_1000_MA               1000
#define		S16_CURRENT_1100_MA               1100
#define		S16_CURRENT_1200_MA               1200
#define		S16_CURRENT_1300_MA               1300
#define		S16_CURRENT_1400_MA               1400
#define		S16_CURRENT_1500_MA               1500
#define     S16_CURRENT_1550_MA               1550
#define		S16_CURRENT_1600_MA               1600
#define		S16_CURRENT_1700_MA               1700
#define		S16_CURRENT_1800_MA               1800
#define		S16_CURRENT_1900_MA               1900
#define		S16_CURRENT_2000_MA               2000
#define		S16_CURRENT_2100_MA               2100
#define		S16_CURRENT_2250_MA               2250

/**********************************************************/
/* Lateral Acceleration (RESOLUTION 0.001g)               */
/**********************************************************/

#define     S16_LAT_0_002_G           2  
#define     S16_LAT_0_005_G           5  
#define     S16_LAT_0_01_G           10  
#define     S16_LAT_0_02_G           20  
#define     S16_LAT_0_05_G           50  
#define     S16_LAT_0_07_G           70  
#define     S16_LAT_0_08_G           80  
#define     S16_LAT_0_09_G           90  
#define     S16_LAT_0_10_G           100 
#define     S16_LAT_0_15_G           150 
#define     S16_LAT_0_17_G           170 
#define     S16_LAT_0_20_G           200 
#define     S16_LAT_0_25_G           250 
#define     S16_LAT_0_26_G           260 
#define     S16_LAT_0_30_G           300 
#define     S16_LAT_0_40_G           400 
#define     S16_LAT_0_45_G           450 
#define     S16_LAT_0_50_G           500 
#define     S16_LAT_0_55_G           550 
#define     S16_LAT_0_60_G           600 
#define     S16_LAT_0_65_G			 650 
#define     S16_LAT_0_70_G           700 
#define     S16_LAT_0_80_G           800 
#define     S16_LAT_0_85_G           850 
#define     S16_LAT_1_00_G           1000
#define     S16_LAT_1_30_G           1300
#define     S16_LAT_1_50_G           1500
#define     S16_LAT_4_00_G           4000



/**********************************************************/
/* Percent (RESOLUTION 1%)                         */
/**********************************************************/

#define		S8_PERCENT_0_PRO                  0  
#define		S8_PERCENT_10_PRO                 10
#define		S8_PERCENT_20_PRO                 20
#define		S8_PERCENT_30_PRO                 30  
#define		S8_PERCENT_40_PRO                 40
#define		S8_PERCENT_50_PRO                 50
#define		S8_PERCENT_60_PRO                 60  
#define		S8_PERCENT_70_PRO                 70
#define		S8_PERCENT_80_PRO                 80
#define		S8_PERCENT_90_PRO                 90  
#define		S8_PERCENT_100_PRO                100

/**********************************************************/
/* MOTOR CURRENT (RESOLUTION 175A)                        */
/**********************************************************/

#define     S16_MTRCURR_RESOL                (100  )
#define     S16_MTRCURR_0A                   (0    )
#define 	S16_MTRCURR_1A                   (100  )
#define     S16_MTRCURR_2A                   (200  )
#define     S16_MTRCURR_3A                   (300  )
#define     S16_MTRCURR_4A                   (400  )
#define     S16_MTRCURR_5A                   (500  )
#define     S16_MTRCURR_6A                   (600  )
#define     S16_MTRCURR_7A                   (700  )
#define     S16_MTRCURR_8A                   (800  )
#define     S16_MTRCURR_9A                   (900  )
#define     S16_MTRCURR_10A                  (1000 )
#define 	S16_MTRCURR_11A                  (1100 )
#define     S16_MTRCURR_12A                  (1200 )
#define     S16_MTRCURR_13A                  (1300 )
#define     S16_MTRCURR_14A                  (1400 )
#define     S16_MTRCURR_15A                  (1500 )
#define     S16_MTRCURR_16A                  (1600 )
#define     S16_MTRCURR_17A                  (1700 )
#define     S16_MTRCURR_18A                  (1800 )
#define     S16_MTRCURR_19A                  (1900 )
#define     S16_MTRCURR_20A                  (2000 )
#define 	S16_MTRCURR_21A                  (2100 )
#define     S16_MTRCURR_22A                  (2200 )
#define     S16_MTRCURR_23A                  (2300 )
#define     S16_MTRCURR_24A                  (2400 )
#define     S16_MTRCURR_25A                  (2500 )
#define     S16_MTRCURR_26A                  (2600 )
#define     S16_MTRCURR_27A                  (2700 )
#define     S16_MTRCURR_28A                  (2800 )
#define     S16_MTRCURR_29A                  (2900 )
#define     S16_MTRCURR_30A                  (3000 )
#define 	S16_MTRCURR_31A                  (3100 )
#define     S16_MTRCURR_32A                  (3200 )
#define     S16_MTRCURR_33A                  (3300 )
#define     S16_MTRCURR_34A                  (3400 )
#define     S16_MTRCURR_35A                  (3500 )
#define     S16_MTRCURR_36A                  (3600 )
#define     S16_MTRCURR_37A                  (3700 )
#define     S16_MTRCURR_38A                  (3800 )
#define     S16_MTRCURR_39A                  (3900 )
#define     S16_MTRCURR_40A                  (4000 )
#define 	S16_MTRCURR_41A                  (4100 )
#define     S16_MTRCURR_42A                  (4200 )
#define     S16_MTRCURR_43A                  (4300 )
#define     S16_MTRCURR_44A                  (4400 )
#define     S16_MTRCURR_45A                  (4500 )
#define     S16_MTRCURR_46A                  (4600 )
#define     S16_MTRCURR_47A                  (4700 )
#define     S16_MTRCURR_48A                  (4800 )
#define     S16_MTRCURR_49A                  (4900 )
#define     S16_MTRCURR_50A                  (5000 )
#define 	S16_MTRCURR_51A                  (5100 )
#define     S16_MTRCURR_52A                  (5200 )
#define     S16_MTRCURR_53A                  (5300 )
#define     S16_MTRCURR_54A                  (5400 )
#define     S16_MTRCURR_55A                  (5500 )
#define     S16_MTRCURR_56A                  (5600 )
#define     S16_MTRCURR_57A                  (5700 )
#define     S16_MTRCURR_58A                  (5800 )
#define     S16_MTRCURR_59A                  (5900 )
#define     S16_MTRCURR_60A                  (6000 )
#define 	S16_MTRCURR_61A                  (6100 )
#define     S16_MTRCURR_62A                  (6200 )
#define     S16_MTRCURR_63A                  (6300 )
#define     S16_MTRCURR_64A                  (6400 )
#define     S16_MTRCURR_65A                  (6500 )
#define     S16_MTRCURR_66A                  (6600 )
#define     S16_MTRCURR_67A                  (6700 )
#define     S16_MTRCURR_68A                  (6800 )
#define     S16_MTRCURR_69A                  (6900 )
#define     S16_MTRCURR_70A                  (7000 )
#define 	S16_MTRCURR_71A                  (7100 )
#define     S16_MTRCURR_72A                  (7200 )
#define     S16_MTRCURR_73A                  (7300 )
#define     S16_MTRCURR_74A                  (7400 )
#define     S16_MTRCURR_75A                  (7500 )
#define     S16_MTRCURR_76A                  (7600 )
#define     S16_MTRCURR_77A                  (7700 )
#define     S16_MTRCURR_78A                  (7800 )
#define     S16_MTRCURR_79A                  (7900 )
#define     S16_MTRCURR_80A                  (8000 )
#define 	S16_MTRCURR_81A                  (8100 )
#define     S16_MTRCURR_82A                  (8200 )
#define     S16_MTRCURR_83A                  (8300 )
#define     S16_MTRCURR_84A                  (8400 )
#define     S16_MTRCURR_85A                  (8500 )
#define     S16_MTRCURR_86A                  (8600 )
#define     S16_MTRCURR_87A                  (8700 )
#define     S16_MTRCURR_88A                  (8800 )
#define     S16_MTRCURR_89A                  (8900 )
#define     S16_MTRCURR_90A                  (9000 )
#define 	S16_MTRCURR_91A                  (9100 )
#define     S16_MTRCURR_92A                  (9200 )
#define     S16_MTRCURR_93A                  (9300 )
#define     S16_MTRCURR_94A                  (9400 )
#define     S16_MTRCURR_95A                  (9500 )
#define     S16_MTRCURR_96A                  (9600 )
#define     S16_MTRCURR_97A                  (9700 )
#define     S16_MTRCURR_98A                  (9800 )
#define     S16_MTRCURR_99A                  (9900 )
#define     S16_MTRCURR_100A                 (10000)
#define 	S16_MTRCURR_120A				 (12000)
#define     S16_MTRCURR_MAX                  (S16_MTRCURR_99A)
#define     S16_MTRCURR_MIN                  (-S16_MTRCURR_99A)

/**********************************************************/
/* MOTOR POSITION (RESOLUTION 975MM)                        */
/**********************************************************/
#define     S32_MTRPOSI_RESOL                (1126)     		/* 2048pulse / 1.819mm  = 1126*/
#define     S32_MTRPOSI_0MM                  (0  )
#define	    S32_MTRPOSI_0_2MM		    	 (225)
#define	    S32_MTRPOSI_0_25MM		   		 (281)
#define     S32_MTRPOSI_0_5MM                (563)
#define     S32_MTRPOSI_1MM                  (1126)
#define	    S32_MTRPOSI_1_5MM		    	 (1689)
#define     S32_MTRPOSI_2MM                  (2252)
#define     S32_MTRPOSI_3MM                  (3378)
#define     S32_MTRPOSI_4MM                  (4504)
#define     S32_MTRPOSI_5MM                  (5630)
#define     S32_MTRPOSI_6MM                  (6756)
#define     S32_MTRPOSI_7MM                  (7882)
#define     S32_MTRPOSI_8MM                  (9008)
#define     S32_MTRPOSI_9MM                  (10134)
#define     S32_MTRPOSI_10MM                 (11260)
#define     S32_MTRPOSI_11MM                 (12386)
#define     S32_MTRPOSI_12MM                 (13512)
#define     S32_MTRPOSI_13MM                 (14638)
#define     S32_MTRPOSI_14MM                 (15764)
#define     S32_MTRPOSI_15MM                 (16890)
#define     S32_MTRPOSI_16MM                 (18016)
#define     S32_MTRPOSI_17MM                 (19142)
#define     S32_MTRPOSI_18MM                 (20268)
#define     S32_MTRPOSI_19MM                 (21394)
#define     S32_MTRPOSI_20MM                 (22520)
#define     S32_MTRPOSI_21MM                 (23646)
#define     S32_MTRPOSI_22MM                 (24772)
#define     S32_MTRPOSI_23MM                 (25898)
#define     S32_MTRPOSI_24MM                 (27024)
#define     S32_MTRPOSI_25MM                 (28150)
#define     S32_MTRPOSI_26MM                 (29276)
#define     S32_MTRPOSI_27MM                 (30402)
#define     S32_MTRPOSI_28MM                 (31528)
#define     S32_MTRPOSI_29MM                 (32654)
#define     S32_MTRPOSI_30MM                 (33780)
#define     S32_MTRPOSI_31MM                 (34906)
#define     S32_MTRPOSI_32MM                 (36032)
#define     S32_MTRPOSI_33MM                 (37158)
#define     S32_MTRPOSI_34MM                 (38284)
#define     S32_MTRPOSI_35MM                 (39410)
#define     S32_MTRPOSI_36MM                 (40536)
#define     S32_MTRPOSI_37MM                 (41662)
#define     S32_MTRPOSI_38MM                 (42788)
#define     S32_MTRPOSI_39MM                 (43914)
#define     S32_MTRPOSI_40MM                 (45040)
#define     S32_MTRPOSI_41MM                 (46166)
#define     S32_MTRPOSI_42MM                 (47292)
#define     S32_MTRPOSI_43MM                 (48418)
#define     S32_MTRPOSI_44MM                 (49544)
#define     S32_MTRPOSI_45MM                 (50670)
#define     S32_MTRPOSI_46MM                 (51796)
#define     S32_MTRPOSI_47MM                 (52922)
#define     S32_MTRPOSI_48MM                 (54048)
#define     S32_MTRPOSI_49MM                 (55174)
#define     S32_MTRPOSI_50MM                 (56300)
#define     S32_MTRPOSI_51MM                 (57426)
#define     S32_MTRPOSI_52MM                 (58552)
#define     S32_MTRPOSI_53MM                 (59678)
#define     S32_MTRPOSI_54MM                 (60804)
#define     S32_MTRPOSI_55MM                 (61930)

/**********************************************************/
/* LPF GAIN Ts=1ms 128G                       */
/**********************************************************/
#define LPF_1_FIL_K_FC_25HZ_TS1MS   17
#define LPF_1_FIL_K_FC_20HZ_TS1MS   14
#define LPF_1_FIL_K_FC_15HZ_TS1MS   11
#define LPF_1_FIL_K_FC_10HZ_TS1MS   8

