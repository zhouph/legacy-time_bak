/**
 * @defgroup Vil_Cfg Vil_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Vil_Cfg.h
 * @brief       Vehicle Integrated Layer Configuration File
 * @date        2014. 4. 17.
 ******************************************************************************/

#ifndef VIL_CFG_H_
#define VIL_CFG_H_

/*==============================================================================
 *                  INCLUDE FILES
==============================================================================*/
#include "APPL/ASW_CAL_CFG.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
==============================================================================*/
#define __AUTOSAR_CORE_DIS      //@TODO : AUTOSAR 관련 이름을 정해야 함.
//@TODO : #define __AUTOSAR_CORE_ENA
#define __AUTOSAR_HEADER_ARCH_DIS
//@TODO : #define __AUTOSAR_HEADER_ARCH_ENA //if enabled, you can change from legacy to autosar mode


#define M_DEBUGGER

#ifdef M_DEBUGGER
  #ifndef STATIC
    #define STATIC volatile
  #endif
#else
  #ifndef STATIC
    #define STATIC static
  #endif
#endif /* M_DEBUGGER */

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
==============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
==============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
==============================================================================*/

/*==============================================================================
 *                  END OF FILE
==============================================================================*/
#endif /* VIL_CFG_H_ */
/** @} */
