/*
 * IDBLogDataSet.c
 *
 *  Created on: 2014. 12. 23.
 *      Author: yeonbin.ko
 */

/***************************************************************************************************/
/*                                           Include Header                                        */
/***************************************************************************************************/

#include "Asw_Types.h"
#include "SPC_Types.h"
#include "DET_Types.h"
#include "ABC_Types.h"
#include "RBC_Types.h"
#include "BBC_Types.h"
#include "PCT_Types.h"
#include "Vat_Types.h"
#include "MCC_Types.h"
#include "ARB_Types.h"
#include "Acmctl_Types.h"
#include "SES_Types.h"
#include "MSP_Types.h"
#include "PROXY_Types.h"
#include "VLV_Types.h"
#include "NvMIf_Types.h"
#include "Acmio_Sen.h"
#include "Pct_5msCtrl.h"

#include "Sal_Types.h"
#include "Task_50us_Types.h"

#if (M_IDB_TEST == ENABLE)
#include "L_IdbTestMain.h"
#endif

#define USE_LOGDATA (ENABLE)
#if (USE_LOGDATA == ENABLE)

/***************************************************************************************************/
/*                                       Local Type Definition                                     */
/***************************************************************************************************/
extern uint8_t	AHBLOG[80];

/***************************************************************************************************/
/*                                       Local Variable Definition                                 */
/***************************************************************************************************/

const static uint16_t lsu16LimValArray[16] = {0x00, 0x01, 0x03, 0x07, 0x0f, 0x1f, 0x3f, 0x7f, 0xff, 0x01ff, 0x03ff, 0x07ff, 0x0fff, 0x1fff, 0x3fff, 0x7fff};
const static uint8_t lsu8LimValArray[9] = {0x00, 0x01, 0x03, 0x07, 0x0f, 0x1f, 0x3f, 0x7f, 0xff};
const static uint8_t lsu8MaskingArray1[9] = {0x00, 0x01, 0x03, 0x07, 0x0f, 0x1f, 0x3f, 0x7f, 0xff};
const static uint8_t lsu8MaskingArray2[9] = {0x00, 0x80, 0xc0, 0xe0, 0xf0, 0xf8, 0xfc, 0xfe, 0xff};


/***************************************************************************************************/
/*                                 SWC Struct Type for Logging                                     */
/***************************************************************************************************/

/* Signal Proc */
typedef struct
{
    int16_t     lsahbs16AHBOfs_f;

    uint32_t    lsahbu1AHBFinalOfsOK    :1;
    uint32_t    lsahbu1AHBDrvEepOfsDrvUse:1;
    uint16_t    lsahbu1AHBFinalMonitor; /*F216 Debug TODO*/
}IdbSnsrOffsFinal_t;/* LogData Type 수정 필요 */

typedef struct
{
    int16_t     lsahbs16AHBEolEEPofsReadRaw;
    int16_t     lsahbs16AHBEEPofsRead;
    int16_t 	lsIdbs16EolOffsEepWr;
    uint32_t    lsahbu1AHBEepOfsWriteReq    :1;
    uint32_t    lsahbu1AHBEepOfsWriteOkFlg  :1;
    uint32_t    lsahbu1AHBEolOfsWriteReqOK  :1;
    uint32_t    lsahbu8AHBEEPofsReadInvalid :1;
    uint32_t    lsahbu1AHBEEPofsUseOK       :1;

}IdbSnsrOffsEolEep_t;/* LogData Type 수정 필요 */

typedef struct
{
    uint8_t lsahbu8AHBDrvOfsCnt;
    uint8_t lsahbu8AHBDrvOfsArrayCnt;
    int16_t lsahbs16AHBDrvOfsArrayMin;
    int16_t lsahbs16AHBDrvOfsArrayFilter;
    int16_t lsahbs16AHBDrvOfsArrayFConv;
    int16_t lsahbs16AHBDrvOfs_f;
    int16_t lsahbs16AHBDrvMin;
    uint8_t lsahbu8AHBDrvOfs10sArrayCnt;
    uint8_t ldahbu8AHBactAfterCNT;

    uint8_t lsahbu8AHBNormCondCnt;
    int16_t lsahbs16AHBNormCondMeanOld;
    int16_t lsahbs16AHBNormCondMean;
    int32_t lsahbs32AHBNormCondSum;

    uint8_t lsahbu8AHBDrvModeDeltTCnt;
    int16_t lsahbs16AHBNormCondMeanDiff;

    int16_t SnsrRawSig;
    int16_t s16SnsrRawSigLim;

    int32_t lsahbs32AHBDrvOfs1secSum;
    int32_t lsahbs32AHBDrvOfs1secSumArray[10];
    int32_t lsahbs32AHBDrvOfs1secArrayMax;
    int32_t lsahbs32AHBDrvOfs1secArrayMin;
    int32_t lsahbs32AHBDrvOfs1secArraySum;
    int32_t lsahbs32AHBDrvOfs1secArrayTrimSum;
    int32_t lsahbs32AHBDrvOfs1secArrayMean;
    int32_t lsahbs32AHBDrvofs10secSum;

    int16_t lsahbs16AHBDrvOfs60sSumOld;
    int16_t lsahbs16AHBDrvOfs60sSum;

    uint32_t    lsahbu1AHBDrv10secOfsOK     :1;
    uint32_t    lsahbu1AHBDrv1secOfsOK      :1;
    uint32_t    lsahbu1AHBDrv60secOfsReOK   :1;
    uint32_t    lsahbu1AHBDrv60secOfsOK     :1;
    uint32_t    lsahbu1AHBDrvOfsOK          :1;
    uint32_t    lsahbu1AHBact1secAfterOK    :1;
    uint32_t    lsahbu1NormCondFirstCheck   :1;
    uint32_t    lsahbu1SensorNormCond       :1;
    uint32_t    lsahbu1SensorDrvOfsCheckCond:1;
    uint32_t    lsahbu1SensorFaultDet		:1;
    uint32_t    lsahbu1SensorSusDet			:1;
}IdbSnsrOffsDrvgCalc_t;/* LogData Type 수정 필요 */

typedef struct
{
    int16_t lsahbs16DrvEEPofsReadRaw;
    int16_t lsahbs16AHBDrvEEPofsRead;

    uint32_t    lsahbu1AHBDrvEEPofsUseOK    :1;
    uint32_t    lsahbu8DrvEEPofsReadInvalid :1;
    uint32_t    lsahbu1AHBDrvOfsEepWriteReq :1;
    uint32_t    lsahbu1AHBeepWriteResetReq  :1;
    uint32_t    lsahbu1AHBDrveepWriteEnd    :1;
    int16_t 	lsIdbs16DrvgOffsEepWr;
}IdbSnsrOffsDrvgEep_t; /* LogData Type 수정 필요 */

typedef struct
{
	uint8_t ldidbu8InletValveOpenTi;
	uint8_t ldidbu8InletValveOpenTiOld;
	uint8_t ldidbu8InletValveOpenTiCorrd;
	uint8_t ldidbu8InletValveOpenTiForVol;
	uint8_t ldidbu8OutletValveOpenTi;
	uint8_t ldidbu8OutletValveOpenTiOld;
	uint8_t ldidbu8OutletValveOpenTiCorrd;
	uint8_t ldidbu8OutletValveOpenTiForVol;
	uint8_t ldidbu8InletValveOpenTiLfc;

	int32_t *ldidbs32InletVlvDrvReqOutpI;
	int32_t *ldidbs32OutletVlvDrvReqOutpI;

	/* Add for Volume based pressure */
	uint8_t ldidbu8WhlVolumeResetFlg;
	uint8_t ldidbu8WhlVolumeResetCnt;
	uint8_t ldidbu8InletVolumePercent;
	int32_t ldidbs32WhlVolumeByS;
	int32_t ldidbs32WhlVolumeBySOld;
	int16_t ldidbs16EstmdWhlPByS;
	uint8_t ldidbu8WhlPByVolBrakeOnFlg;
	uint8_t ldidbu8WhlPByVolBrakeOnCnt;
	uint8_t ldidbu8WhlPByVolBrakeOnInitFlg;
	uint8_t ldidbu8WhlPByVol1stRiseFlg;
	uint8_t ldidbu8WhlPByVol1stRiseCnt;
	int16_t ldidbs16WhlVChgViaInletVlvByS;
	int16_t ldidbs16WhlVChgViaOutletVlvByS;
	/* Add for Volume based pressure */
	/* Add for New Concept Pressure based pressure */
	int32_t ldidbs32WhlVolumeByP;
	int16_t ldidbs16EstmdWhlP;
	int16_t ldidbs16EstmdWhlPOld;
	int16_t ldidbs16EstDeltaP;
	int16_t ldidbs16EstDeltaPLPF;
	uint8_t ldidbu8EstPWhlState;
	uint8_t ldidbu8EstPWhlStateChgCnt;
	int16_t ldidbs16WhlVChgViaInletVlv;
	int16_t ldidbs16WhlVChgViaOutletVlv;
	int16_t ldidbs16WhlVChgViaCutVlv;
	int16_t ldidbs16WhlVChgViaBalVlv;
	/* Add for New Concept Pressure based pressure */

	uint8_t ldidbu8ContinuedFullRiseModeFlg;
	uint8_t ldidbu8ContinuedDumpModeFlg;
	uint8_t ldidbu8ContinuedLfcRiseModeFlg;

	uint8_t ldidbu8Wheel_Valve_Mode;
	uint8_t ldidbu8Wheel_Valve_Mode_old;

	uint8_t ldidbu8wpe_press_match_chk_cnt;
	uint8_t ldidbu8u1PressMatchSusFlg;

	int16_t ldidbu8InletValveDuty;

	/*Debuggers*/
	uint8_t Est_WhlP_Inlet_MON;
	uint8_t Est_WhlP_Outlet_MON;
	int32_t WhlPChgTotal_MON;
	int32_t Debugger;
}WPE_t;

typedef struct {
	unsigned u1Priority	  :1;
	unsigned u1PriorityCmdPerWheel  :1;
	unsigned u1CmdHalt  			:1;
	unsigned u1BuffFIFOAdded		:1;
	unsigned u1BuffRemoved  		:1;
	unsigned u1BuffInserted 			:1;

	uint8_t u8id		       ; /* type of Mux commands: currently there will be 6 types*/
	uint8_t u8State			   ; /* state of Mux command: executed, executing, not-executed, first-executed */
	/*uint8_t u8OldState		;*/ /* state of Mux command: executed, executing, not-executed, first-executed */
	uint16_t u16CmdExecTimeMs  ; 	/* Mux command executing time: max 20 seconds*/
	uint16_t u16CmdExecTime	   ; 	/* Mux command executing time: max 20 seconds*/
	/*uint16_t u16CmdExecTimer;*/ /* Mux command executing timer: max 20 seconds*/
	int16_t  s16WhlTargetP     ;
	int16_t  s16WhlTargetDeltaP;
	int16_t  s16WhlTargetPRate ;
	int16_t  s16EstWhlP		   ;
}LAMUXCTRLSTATE_t; /* multiplex output commands */

typedef struct {
	unsigned u1Priority	  :1;
	unsigned u1CtrlReq    :1;
	unsigned u1PriorityCmdPerWheel  :1;
	unsigned u1CmdHalt  			:1;
	uint8_t  u8id			   ;
	int16_t  s16WhlTargetP     ;
	int16_t  s16WhlTargetDeltaP;
	int16_t  s16WhlTargetPRate ;
	int16_t  s16EstWhlP		   ;
	uint8_t  u8ArbCtrlMod;
}LAMUXCTRLCMD_t; /* multiplex input commands */

typedef struct {
	unsigned u1InAbsCtrlActive          :1;
	unsigned u1InAbsCtrlFadeOut         :1;
	unsigned u1SafetyControlActive		:1;
	unsigned u1PCtrlActFlg				:1;
	unsigned u1EbdPulseUpActive			:1;
	unsigned u1EbdActive			    :1;
	unsigned u1AbsMtrTqCtrlActive		:1;
	unsigned u1EscTwoChannel			:1;		
}LAMUXSYSTCTRLFLGS_t;

typedef struct {
	unsigned u1MuxCtrlStrategy			:1; /* 1: FIFO  / 0: Highest delta-p */
	unsigned u1MuxCmdAllocated			:1;
	unsigned u1MuxStrkRecvrCtrlNeed		:1;
	unsigned u1MuxPCtrlStrategy			:1; /* 1: Delta-P / 0: Target P */
	unsigned u1MuxPCtrlNeed				:1;
	unsigned u1MuxCurCmdIdChanged		:1;
	/*
	unsigned u1HaltCurMuxCmd			:1;
	unsigned u1HaltCurMuxScan			:1;
	*/
	unsigned u1MuxNChannelCtrlActive	:1; /* 1: needed n-channel multiplex control / 0: 1-channel multiplex control */
}LAMUXWHLCTRLFLGS_t;

typedef struct
{
	uint8_t  u8MuxWhlCtrlMode;
	uint8_t  u8WhlCmdDelayTime;
	uint16_t u16WhlCmdExecTime;
	uint16_t u16MuxWhlVlvActRatio;
}LAMUX_WHL_VV_CMD_t;

#define U8_MUX_FIFO_BUFFER_SIZE	6

struct fifo_buffer_t {
         unsigned u32id;
         unsigned u32head;         /* first byte of data */
         unsigned u32tail;         /* last byte of data */
         unsigned char u8buffer[U8_MUX_FIFO_BUFFER_SIZE]; /* block of memory or array of data */
         unsigned u32BufferLen;  			/* length of the data */
};
typedef struct fifo_buffer_t MUX_FIFO_BUFFER;


typedef struct {
	uint32_t lau1ValveAct:  1;
	uint8_t  lau8ValveOnTime;
	/*uint16_t lau16Current; For IDB '15 NZ A,B sample */
	uint16_t lau16ValveActRatio;
	uint16_t lau16ValveActCnt;
}LA_MUXVLV_CMD_VAR_t;

typedef struct {
	uint16_t u16ValveActRatio[5];
}LA_MUXVLV_ACT_t;

/***************************************************************************************************/
/*                                     SWC Global Variable                                         */
/***************************************************************************************************/

/* SPC Extern ***************************************************/
extern IdbSnsrOffsDrvgCalc_t    PdtOffsDrvgCalc;
extern IdbSnsrOffsFinal_t       PdtOffsFinal;
extern IdbSnsrOffsDrvgEep_t     PdtOffsDrvgEep;
extern IdbSnsrOffsDrvgCalc_t    PedlSimrPOffsDrvgCalc;
extern IdbSnsrOffsFinal_t       PedlSimrPOffsFinal;
extern IdbSnsrOffsDrvgEep_t     PedlSimrPOffsDrvgEep;

extern IdbSnsrOffsEolEep_t      PdtOffsEolEep;
extern IdbSnsrOffsEolEep_t      PdfOffsEolEep;
extern IdbSnsrOffsEolEep_t      PedlSimrPOffsEolEep;
#if __P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE
extern IdbSnsrOffsDrvgEep_t     PistPOffsDrvgEep;
extern IdbSnsrOffsEolEep_t      PistPOffsEolEep;
#else __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
extern IdbSnsrOffsEolEep_t      PrimPOffsEolEep;
extern IdbSnsrOffsEolEep_t      SecdPOffsEolEep;
extern IdbSnsrOffsDrvgEep_t     PrimPOffsDrvgEep;
#endif

extern NvMIf_LogicBlockType NvMIf_LogicEepData;
/* Motor Extern ***************************************************/
extern int32_t lmccs16WrpmMechRef;
extern sint32 lsess32TgtPosiOrgSet;
extern uint16 lsesu16WallDetectedFlg;
extern Acmio_SenVdcLink_t Acmio_SenVdcLink;
extern uint16 MpsD1_Compen_SetFag_at1ms;
extern sint16 lacmctls16Idc;
extern uint16 lamtru16CtrlModForOrgSet;
extern int16_t lmccs16DeltaTgtPosi;
extern int16_t lmccs16CalcIqRefPidState;
extern int16_t lmccs16IqRefBase;
extern int16_t lmccs16IqRefPid;
extern int16_t lmccs16IqRefComp;

/* PCT Extern ***************************************************/
extern int16_t lapcts16DeltaStrokeMap;
extern int16_t lapcts16DeltaStrokeMapAvg;
extern int16_t lapcts16DeltaStrokePerror;
extern int16_t lapcts16DeltaStrokePrateError;
extern uint16_t lapctu16FadeoutState1EndOK;
extern int16_t lapcts16DeltaStrokePerrorFinal;
extern int16_t lapcts16TargetPressOldlog;
extern int16_t lapcts16MovAvgBuffResetFlg;
extern U8_BIT_STRUCT_t lapctDelStrkActFlg;

extern uint8_t lapctu8PdlVlvActHoldOnRequestFlg;

/* DET Extern ***************************************************/
extern int16_t lsahbs16PedalTravelByPS;
extern uint8_t ldahbu8PedalState;
extern uint8_t lsahbu8PDTSigTag;
extern uint8_t lsahbu8PDFSigTag;
extern uint8_t lsahbu8PSPSigTag;

extern int8_t  ldahbs8FastPedalApplyCnt;
extern uint8_t ldahbu8BrkReleaseIntendCnt;
extern int16_t ldahbs16PedalReleaseDctCnt;
extern int16_t ldahbs16FastApplyDetThresLow;
extern int16_t ldahbs16FastApplyDetThresHigh;
extern int16_t ldahbs16FastApply1msCnt;
extern uint8_t lsahbu8PedalInfoSigTag;

extern int16_t lsahbs16PedalRate1ms;

extern WPE_t WPE_FL, WPE_FR, WPE_RL, WPE_RR;

extern int32_t ldidbs32PrimCircP;
extern int32_t ldidbs32SecdCircP;
extern int32_t ldidbs32PedlSimrP;
extern uint8_t ldidbu8ActvBrkCtrlrActFlg;
extern int32_t ldidbs32StrokePosition;
extern uint8_t ldidbu8PrimCircBrakeOnInitFlg;
extern uint8_t ldidbu8SecdCircBrakeOnInitFlg;

/* ETC Extern ***************************************************/
extern SenPwrM_MainSenPwrMonitor_t SenPwrM_MainSenPwrMonitorData;
extern Eem_MainEemFailData_t Eem_MainEemFailData;
extern Eem_MainEemCtrlInhibitData_t Eem_MainEemCtrlInhibitData;
extern SasM_MainSASMOutInfo_t SasM_MainSASMOutInfo;

extern uint8_t u8IdbCtrlInhibitFlg;

/***************************************************************************************************/
/*                                          For Mux                                                */
/***************************************************************************************************/
extern MUX_FIFO_BUFFER	MuxFIFO;
extern LAMUX_WHL_VV_CMD_t	laMux_FLCMD, laMux_FRCMD, laMux_RLCMD, laMux_RRCMD; /* to LAIDB_CallMultiplexValveActuation.c*/
extern int16_t laidbs16MuxTarPress;
extern int16_t las16MuxCtrlState;
extern int16_t las16MuxCtrlMode;
extern LAMUXCTRLSTATE_t MuxStFrntLe;
extern LAMUXCTRLSTATE_t MuxStFrntRi;
extern LAMUXCTRLSTATE_t MuxStReLe;
extern LAMUXCTRLSTATE_t MuxStReRi;
extern LAMUXCTRLSTATE_t MuxCurCmd;

extern LAMUXCTRLCMD_t MuxCmdFrntLe;
extern LAMUXCTRLCMD_t MuxCmdFrntRi;
extern LAMUXCTRLCMD_t MuxCmdReLe;
extern LAMUXCTRLCMD_t MuxCmdReRi;
extern LAMUXSYSTCTRLFLGS_t lapctst_MuxCtrlFlg;    /* to LAPCT_DecideValveCtrlMode.c*/
extern LAMUXWHLCTRLFLGS_t lapctst_MuxWhlCtrlFlg; /* for logging - temporary */
extern uint8_t u8MuxSeqdebugger00;
extern uint8_t u8MuxExeTdebugger00, u8MuxExeTdebugger01;
extern uint8_t u8DebuggerInfo, u8DebuggerInfo02;

extern sint16 lamtrs16WrpmMech;
/*extern uint8_t lapctu1CutCloseDelayForQuickBrk;*/
extern int16_t laidbu16CmdExecTimer;
extern LA_VALVE_CMD_t lapctst_MuxInVCmdFL;
extern LA_VALVE_CMD_t lapctst_MuxInVCmdFR;
extern LA_VALVE_CMD_t lapctst_MuxInVCmdRL;
extern LA_VALVE_CMD_t lapctst_MuxInVCmdRR;
extern int16_t lapcts16MuxPistP;

extern LA_MUXVLV_CMD_VAR_t lapctst_MuxInVCtrlVarFL;
extern LA_MUXVLV_CMD_VAR_t lapctst_MuxInVCtrlVarFR;
extern LA_MUXVLV_CMD_VAR_t lapctst_MuxInVCtrlVarRL;
extern LA_MUXVLV_CMD_VAR_t lapctst_MuxInVCtrlVarRR;

extern LA_MUXVLV_ACT_t lapctst_MuxInVActFL;
extern LA_MUXVLV_ACT_t lapctst_MuxInVActFR;
extern LA_MUXVLV_ACT_t lapctst_MuxInVActRL;
extern LA_MUXVLV_ACT_t lapctst_MuxInVActRR;

/***************************************************************************************************/
/*                                          SWC CtrlBus                                            */
/***************************************************************************************************/
/*ASW*/
extern Spc_5msCtrl_HdrBusType Spc_5msCtrlBus;
extern Spc_1msCtrl_HdrBusType Spc_1msCtrlBus;
extern Det_5msCtrl_HdrBusType Det_5msCtrlBus;
extern Det_1msCtrl_HdrBusType Det_1msCtrlBus;
extern Bbc_Ctrl_HdrBusType Bbc_CtrlBus;
extern Pct_5msCtrl_HdrBusType Pct_5msCtrlBus;
extern PressureControl_LocalVar PressCTRLLocalBus;
extern Vat_Ctrl_HdrBusType Vat_CtrlBus;
extern Arb_Ctrl_HdrBusType Arb_CtrlBus;
extern Rbc_Ctrl_HdrBusType Rbc_CtrlBus;
extern Abc_Ctrl_HdrBusType Abc_CtrlBus;
//extern Mcc_5msCtrl_HdrBusType Mcc_5msCtrlBus;
extern Mcc_1msCtrl_HdrBusType Mcc_1msCtrlBus;
extern Acmctl_Ctrl_HdrBusType Acmctl_CtrlBus;

/*BSW*/
extern Msp_1msCtrl_HdrBusType Msp_1msCtrlBus;
extern Msp_Ctrl_HdrBusType Msp_CtrlBus;
extern Ses_Ctrl_HdrBusType Ses_CtrlBus;
extern Proxy_Rx_HdrBusType Proxy_RxBus;
extern Vlv_Actr_HdrBusType Vlv_ActrBus;

/***************************************************************************************************/
/*                                        Logcal Definiton                                         */
/***************************************************************************************************/

#define AHB_LOG_DATA			AHBLOG
#define M_IDB_LOG_DATA				1
#define M_DETECTION_LOG_DATA		2
#define M_SP_LOG_DATA				3
#define M_IDB_LOG_DATA_ESC_CHECK	4
#define M_IDB_LOG_DATA_MULTIPLEX	5
#define M_IDB_LOG_DATA_PCT_HW_TEST	6
#define M_IDB_WPEByVOL_DATA			7
#define M_IDB_LOG_DATA_VALVE_CHECK	8		
#define M_IDB_ARB_LOG_DATA			9
#define M_IDB_LOG_DATA_PCT			10

#define M_LOGIC_LOG_DATA		M_IDB_LOG_DATA

/***************************************************************************************************/
/*                                 Local Function Prototype                                        */
/***************************************************************************************************/
static int8_t LS_u8LimSignedIntToSignedChar(int16_t s16TempLogData, int16_t s16Scale);
static uint8_t LS_u8LimSignedIntToUnsignedChar(int16_t s16TempLogData, int16_t s16Scale);

#if (M_LOGIC_LOG_DATA==M_IDB_LOG_DATA)
	static void LSIDB_vCallIDBLogData(void);
#elif (M_LOGIC_LOG_DATA==M_IDB_LOG_DATA_PCT)
	static void LSIDB_vCallIDBLogDataPCT(void);
#elif (M_LOGIC_LOG_DATA==M_IDB_ARB_LOG_DATA)
	static void LSIDB_vCallIDBARBLogData(void);
#elif (M_LOGIC_LOG_DATA ==M_DETECTION_LOG_DATA)
	static void LSIDB_vCallDetectionLogData(void);
#elif(M_LOGIC_LOG_DATA == M_SP_LOG_DATA)
	static void LSIDB_vCallSignalProcess(void);
#elif(M_LOGIC_LOG_DATA ==M_IDB_LOG_DATA_ESC_CHECK)
	static void LSIDB_vCallIDBPCtrlLogData(void);
#elif(M_LOGIC_LOG_DATA ==M_IDB_LOG_DATA_MULTIPLEX)
	static void LSIDB_vCallIDBMultiplexLogData(void);
#elif (M_LOGIC_LOG_DATA ==M_IDB_LOG_DATA_PCT_HW_TEST)
	static void LSIDB_vCallPCTLogDataHWTest(void);
#elif (M_LOGIC_LOG_DATA ==M_IDB_WPEByVOL_DATA)
	static void LSIDB_vCallIDBWPEByVOLLogData(void);
#elif (M_LOGIC_LOG_DATA ==M_IDB_LOG_DATA_VALVE_CHECK)
	static void LSIDB_vCallIDBVLVLogData(void);
#else
#endif

void LS_vCallLogDataAHB(void)
{
#if (M_LOGIC_LOG_DATA==M_IDB_LOG_DATA)
	LSIDB_vCallIDBLogData();
#elif (M_LOGIC_LOG_DATA==M_IDB_LOG_DATA_PCT)
	LSIDB_vCallIDBLogDataPCT();
#elif (M_LOGIC_LOG_DATA==M_IDB_ARB_LOG_DATA)
	LSIDB_vCallIDBARBLogData();	
#elif (M_LOGIC_LOG_DATA ==M_DETECTION_LOG_DATA)
	LSIDB_vCallDetectionLogData();
#elif(M_LOGIC_LOG_DATA == M_SP_LOG_DATA)
	LSIDB_vCallSignalProcess();
#elif(M_LOGIC_LOG_DATA ==M_IDB_LOG_DATA_ESC_CHECK)
	LSIDB_vCallIDBPCtrlLogData();
#elif(M_LOGIC_LOG_DATA ==M_IDB_LOG_DATA_MULTIPLEX)
	LSIDB_vCallIDBMultiplexLogData();
#elif (M_LOGIC_LOG_DATA ==M_IDB_LOG_DATA_PCT_HW_TEST)
	LSIDB_vCallPCTLogDataHWTest();
#elif(M_LOGIC_LOG_DATA == M_IDB_WPEByVOL_DATA)
	LSIDB_vCallIDBWPEByVOLLogData();
#elif (M_LOGIC_LOG_DATA ==M_IDB_LOG_DATA_VALVE_CHECK)
	LSIDB_vCallIDBVLVLogData();
#else
#endif
}

static uint8_t LS_u8LimSignedIntToUnsignedChar(int16_t s16TempLogData, int16_t s16Scale)
{
	uint8_t u8LimitedValue;

	s16TempLogData=(s16TempLogData/s16Scale);

	if(s16TempLogData>250)
	{
	    u8LimitedValue = 250;
	}
	else if(s16TempLogData<0)
	{
	    u8LimitedValue = 255;
	}
	else
	{
	    u8LimitedValue = (uint8_t)s16TempLogData;
	}

	return u8LimitedValue;
}

static int8_t LS_u8LimSignedIntToSignedChar(int16_t s16TempLogData, int16_t s16Scale)
{
	int8_t s8LimitedValue;

	s16TempLogData=(s16TempLogData/s16Scale);

	if(s16TempLogData>126)
	{
	    s8LimitedValue = 126;
	}
	else if(s16TempLogData<(-126))
	{
	    s8LimitedValue = -126;
	}
	else
	{
	    s8LimitedValue = (int8_t)s16TempLogData;
	}

	return s8LimitedValue;
}


/******************************************************************************
* FUNCTION NAME:      LS_u8CombineTwoUnsignedInt
* Preconditions:      none 
* PARAMETER:          1st - Unsigned 16bit Int, 2nd - Unsigned  16bit Int, 
*                     3rd - bit length of 1st unsigned data, [1, 7]
* RETURN VALUE:       Combined unsinged 8bit char data
* Description:        Combine Unsigned 8bit char from two unsinged interger
******************************************************************************/

static uint8_t LS_u8CombineTwoUnsignedInt(uint16_t u16TempLogData1, uint16_t u16TempLogData2, uint16_t u16FstDataBits)
{
	uint8_t u8CombinedValue = 0;

	u16FstDataBits = (u16FstDataBits>7) ? 7 : ((u16FstDataBits<1) ? 1 : u16FstDataBits);

    u8CombinedValue|= (lsu8MaskingArray1[u16FstDataBits] & ((u16TempLogData1>lsu8LimValArray[u16FstDataBits]) ? lsu8LimValArray[u16FstDataBits] : u16TempLogData1));  
    u8CombinedValue|= (lsu8MaskingArray2[8-u16FstDataBits] & (((u16TempLogData2>lsu8LimValArray[8-u16FstDataBits]) ? lsu8LimValArray[8-u16FstDataBits] : u16TempLogData2)<<u16FstDataBits));     

	return u8CombinedValue;
}

/******************************************************************************
* FUNCTION NAME:      LS_u16CombineTwoUnsignedInt
* Preconditions:      none 
* PARAMETER:          1st - Unsigned 16bit Int, 2nd - Unsigned  16bit Int, 
*                     3rd - bit length of 1st unsigned data, [1, 15]
* RETURN VALUE:       Combined unsinged 16bit interger data
* Description:        Combine Unsigned integer from two unsinged interger
******************************************************************************/

static uint16_t LS_u16CombineTwoUnsignedInt(uint16_t u16TempLogData1, uint16_t u16TempLogData2, uint16_t u16FstDataBits)
{
	uint16_t u16CombinedValue = 0;
	uint8_t u8CombinedValue0 = 0;
	uint8_t u8CombinedValue1 = 0;

	u16FstDataBits = (u16FstDataBits>15) ? 15 : ((u16FstDataBits<1) ? 1 : u16FstDataBits);
	
	if(u16FstDataBits >= 8)
	{
		if(u16TempLogData1 >= lsu16LimValArray[u16FstDataBits])
		{
			u8CombinedValue0 = 0xff;
		    u8CombinedValue1|= (lsu8MaskingArray1[u16FstDataBits-8]);  
		    u8CombinedValue1|= (lsu8MaskingArray2[16-u16FstDataBits] & (((u16TempLogData2>lsu8LimValArray[16-u16FstDataBits]) ? lsu8LimValArray[16-u16FstDataBits] : u16TempLogData2)<<(u16FstDataBits-8)));     
		}
		else
		{
			u8CombinedValue0 = (uint8_t)u16TempLogData1;	
		
			u8CombinedValue1|= (lsu8MaskingArray1[u16FstDataBits-8] & (u16TempLogData1>>8));  
		    u8CombinedValue1|= (lsu8MaskingArray2[16-u16FstDataBits] & (((u16TempLogData2>lsu8LimValArray[16-u16FstDataBits]) ? lsu8LimValArray[16-u16FstDataBits] : u16TempLogData2)<<(u16FstDataBits-8)));     
	    }
	}
	else
	{
		if(u16TempLogData2 >= lsu16LimValArray[16-u16FstDataBits])
		{
			u8CombinedValue0|= (lsu8MaskingArray1[u16FstDataBits] & ((u16TempLogData1>lsu8LimValArray[u16FstDataBits]) ? lsu8LimValArray[u16FstDataBits] : u16TempLogData1));  
			u8CombinedValue0|= (lsu8MaskingArray2[8-u16FstDataBits]);
			u8CombinedValue1 = 0xff;
		}
		else
		{
			u8CombinedValue0|= (lsu8MaskingArray1[u16FstDataBits] & ((u16TempLogData1>lsu8LimValArray[u16FstDataBits]) ? lsu8LimValArray[u16FstDataBits] : u16TempLogData1));  
		    u8CombinedValue0|= (lsu8MaskingArray2[8-u16FstDataBits] & ((uint8_t)(u16TempLogData2<<u16FstDataBits)));     
			u8CombinedValue1 = (uint8_t)(u16TempLogData2>>(8-u16FstDataBits));
	    }
	}
		
	u16CombinedValue = ((uint16_t)u8CombinedValue1<<8) | (uint16_t)(u8CombinedValue0);
	
	return u16CombinedValue;
}

/******************************************************************************
* FUNCTION NAME:      LS_u16CombSignedIntNUnsignedInt
* Preconditions:      none 
* PARAMETER:          1st - Signed 16bit Int, 2nd - Unsigned  16bit Int, 
*                     3rd - bit length of signed data, [2, 15]
* RETURN VALUE:       Combined unsinged 16bit interger data
* Description:        Combine Unsigned integer from Signed integer and Unsinged interger
******************************************************************************/
 
static uint16_t LS_u16CombSignedIntNUnsignedInt(int16_t s16TempLogData1, uint16_t u16TempLogData2, uint16_t u16SignedDataBits)
{
	uint16_t u16CombinedValue = 0;
	uint16_t u16SignedDataAbs = 0;
	uint8_t u8SignBit = ((s16TempLogData1>=0) ? 0 : 1);
	
	u16SignedDataBits = (u16SignedDataBits>15) ? 15 : ((u16SignedDataBits<2) ? 2 : u16SignedDataBits);
	u16SignedDataAbs = ((s16TempLogData1>=0) ? s16TempLogData1 : (-s16TempLogData1));

	if(s16TempLogData1 >= lsu16LimValArray[(u16SignedDataBits-1)])
	{
		u16SignedDataAbs = (lsu16LimValArray[(u16SignedDataBits-1)]);
	}
	else if(s16TempLogData1 <= -(lsu16LimValArray[(u16SignedDataBits-1)] + 1))
	{
		u16SignedDataAbs = (lsu16LimValArray[(u16SignedDataBits-1)] + 1);
	}
	else
	{
		u16SignedDataAbs = (u8SignBit>0) ? (lsu16LimValArray[u16SignedDataBits]+1-u16SignedDataAbs) : u16SignedDataAbs;
	}

	u16CombinedValue = LS_u16CombineTwoUnsignedInt(u16SignedDataAbs, u16TempLogData2, u16SignedDataBits);

	return u16CombinedValue;
}


#if (M_LOGIC_LOG_DATA==M_IDB_LOG_DATA)
static void LSIDB_vCallIDBLogData(void)
{
	uint8_t	i;
	uint16_t u16CombinedLogData = 0;
	int16_t  s16CombinedLogData = 0;
	uint8_t  u8CombinedLogData = 0;

	/***************************************************************************************************/
	/*                                           LOG_BYTE0                                             */
	/***************************************************************************************************/
	s16CombinedLogData = LS_u16CombSignedIntNUnsignedInt((int16_t)(Spc_5msCtrlBus.Spc_5msCtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar/10),
	                                                 (uint16_t)(Abc_CtrlBus.Abc_CtrlStkRecvryActnIfInfo.StkRecvryCtrlState/10),
	                                                 (uint16_t)(12));
	AHB_LOG_DATA[0] = (uint8_t) (s16CombinedLogData);
	AHB_LOG_DATA[1] = (uint8_t) (s16CombinedLogData >> 8);

#if __P_SENSOR_MEASURE_TYPE== EACH_CIRCUIT_PRESSURE
	AHB_LOG_DATA[2] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar);
	AHB_LOG_DATA[3] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar>>8);
	AHB_LOG_DATA[4] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar);
	AHB_LOG_DATA[5] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar>>8);
#else
	AHB_LOG_DATA[2] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPistPFildInfo.PistPFild_1_100Bar);
	AHB_LOG_DATA[3] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPistPFildInfo.PistPFild_1_100Bar>>8);
	AHB_LOG_DATA[4] = (uint8_t) (lacmctls16Idc);
	AHB_LOG_DATA[5] = (uint8_t) (lacmctls16Idc>>8);
#endif

	i=0;
	if(MuxCmdFrntLe.u1CtrlReq              			                                    ==1) i|=0x01;
	if(MuxCmdFrntRi.u1CtrlReq              			                                    ==1) i|=0x02;
	if(MuxCmdReLe.u1CtrlReq                                                             ==1) i|=0x04;
	if(MuxCmdReRi.u1CtrlReq                			                                    ==1) i|=0x08;
	if(lapctst_MuxInVCtrlVarFL.lau1ValveAct			                                    ==1) i|=0x10;
	if(lapctst_MuxInVCtrlVarFR.lau1ValveAct			                                    ==1) i|=0x20;
	if(lapctst_MuxInVCtrlVarRL.lau1ValveAct			                                    ==1) i|=0x40;
	if(lapctst_MuxInVCtrlVarRR.lau1ValveAct			                                    ==1) i|=0x80;

	AHB_LOG_DATA[6] =(uint8_t) (i);

	i=0;
	if(Spc_5msCtrlBus.Spc_5msCtrlBlsFild                     	 						==1) i|=0x01;
	if(Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdtOffsCorrdStOkFlg				==1) i|=0x02;
	if(Spc_5msCtrlBus.Spc_5msCtrlPistPOffsCorrdInfo.PistPOffsCorrdStOkFlg	          	==1) i|=0x04;
	if(Bbc_CtrlBus.Bbc_CtrlIgnOnOffSts													==1) i|=0x08;
	if(u8IdbCtrlInhibitFlg																==1) i|=0x10;
	if(Eem_MainEemCtrlInhibitData.Eem_BBS_DefectiveModeFail								==1) i|=0x20;
	if(Mcc_1msCtrlBus.Mcc_1msCtrlMotICtrlFadeOutState									==1) i|=0x40;
	if(Pct_5msCtrlBus.Pct_5msCtrlIdbVlvActInfo.VlvFadeoutEndOK							==1) i|=0x80;

	AHB_LOG_DATA[7] =(uint8_t) (i);

	/***************************************************************************************************/
	/*                                           LOG_BYTE1                                             */
	/***************************************************************************************************/
	u16CombinedLogData = LS_u16CombineTwoUnsignedInt((uint16_t)(Det_5msCtrlBus.Det_5msCtrlPedlTrvlFinal),
	                                                 (uint16_t)(Pct_5msCtrlBus.Pct_5msCtrlPCtrlBoostMod),
	                                                 (uint16_t)(11));	
	AHB_LOG_DATA[8] = (uint8_t) (u16CombinedLogData);
	AHB_LOG_DATA[9] = (uint8_t) (u16CombinedLogData >> 8);
	
	s16CombinedLogData = LS_u16CombSignedIntNUnsignedInt((int16_t)(Det_5msCtrlBus.Det_5msCtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec),
	                                                 (uint16_t)(Pct_5msCtrlBus.Pct_5msCtrlPCtrlSt),
	                                                 (uint16_t)(12));		

	AHB_LOG_DATA[10] = (uint8_t) (s16CombinedLogData);
	AHB_LOG_DATA[11] = (uint8_t) (s16CombinedLogData >> 8);

#if __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
	s16CombinedLogData = LS_u16CombSignedIntNUnsignedInt((int16_t)(Det_5msCtrlBus.Det_5msCtrlCircPRateInfo.PrimCircPChgDurg10ms),
	                                                 (uint16_t)(PressCTRLLocalBus.u16MuxWheelVolumeState),
	                                                 (uint16_t)(12));		
#else
	s16CombinedLogData = LS_u16CombSignedIntNUnsignedInt((int16_t)(Det_5msCtrlBus.Det_5msCtrlPistPRateInfo.PistPChgDurg10ms),
	                                                 (uint16_t)(PressCTRLLocalBus.u16MuxWheelVolumeState),
	                                                 (uint16_t)(12));		
#endif
	AHB_LOG_DATA[12] = (uint8_t) (s16CombinedLogData);
	AHB_LOG_DATA[13] = (uint8_t) (s16CombinedLogData >> 8);

	u8CombinedLogData = LS_u8CombineTwoUnsignedInt((uint16_t)(Pct_5msCtrlBus.Pct_5msCtrlPCtrlFadeoutSt), 
	                                               (uint16_t)(PressCTRLLocalBus.u8WhlCtrlIndex), 2);
	AHB_LOG_DATA[14] = (uint8_t) (u8CombinedLogData);

	i=0;
	if(Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.DrvrIntendToRelsBrk	 				==1) i|=0x01;
	if(Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.PanicBrkStFlg         	 			==1) i|=0x02;
	if(Bbc_CtrlBus.Bbc_CtrlBaseBrkCtrlrActFlg               							==1) i|=0x04;
	if(Bbc_CtrlBus.Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo.PedlSimrPGendOnFlg             ==1) i|=0x08;
	if(Bbc_CtrlBus.Bbc_CtrlBaseBrkCtrlModInfo.VehStandStillStFlg		                ==1) i|=0x10;
	if(Det_1msCtrlBus.Det_1msCtrlBrkPedlStatus1msInfo.PanicBrkSt1msFlg                	==1) i|=0x20;
	if(lapcts16MovAvgBuffResetFlg														==1) i|=0x40;
	if(lapctDelStrkActFlg.bit2/*lapctu1NegativeCompDisableFlg*/							==1) i|=0x80;
	AHB_LOG_DATA[15] = (uint8_t) (i);

	/***************************************************************************************************/
	/*                                           LOG_BYTE2                                             */
	/***************************************************************************************************/
	u16CombinedLogData = LS_u16CombineTwoUnsignedInt((uint16_t)(Bbc_CtrlBus.Bbc_CtrlTarPFromBaseBrkCtrlr/10),
	                                                 (uint16_t)(PressCTRLLocalBus.s16ReqdPCtrlBoostMod),
	                                                 (uint16_t)(11));	
	AHB_LOG_DATA[16] = (uint8_t) (u16CombinedLogData);
	AHB_LOG_DATA[17] = (uint8_t) (u16CombinedLogData >> 8);

	AHB_LOG_DATA[18] = (uint8_t) LS_u8LimSignedIntToUnsignedChar(MuxCurCmd.s16WhlTargetP, 100);
	AHB_LOG_DATA[19] = (uint8_t) LS_u8LimSignedIntToUnsignedChar(MuxCurCmd.s16EstWhlP, 100);

	s16CombinedLogData = LS_u16CombSignedIntNUnsignedInt((int16_t)(PressCTRLLocalBus.s16MuxPwrPistonTarPRate),
	                                                 (uint16_t)(SasM_MainSASMOutInfo.SASM_Timeout_Err),
	                                                 (uint16_t)(12));		
	AHB_LOG_DATA[20] = (uint8_t) (s16CombinedLogData);
	AHB_LOG_DATA[21] = (uint8_t) (s16CombinedLogData >> 8);

	AHB_LOG_DATA[22] = (uint8_t) LS_u8LimSignedIntToUnsignedChar(Bbc_CtrlBus.Bbc_CtrlTarPFromBrkPedl,100);
	AHB_LOG_DATA[23] = (uint8_t) LS_u8LimSignedIntToSignedChar(lacmctls16Idc, 100);

	/***************************************************************************************************/
	/*                                           LOG_BYTE3                                             */
	/***************************************************************************************************/
	u16CombinedLogData = LS_u16CombineTwoUnsignedInt((uint16_t)(Pct_5msCtrlBus.Pct_5msCtrlFinalTarPFromPCtrl/10),
	                                                 (uint16_t)(MuxFIFO.u32head),
	                                                 (uint16_t)(11));	
	AHB_LOG_DATA[24] = (uint8_t) (u16CombinedLogData);
	AHB_LOG_DATA[25] = (uint8_t) (u16CombinedLogData >> 8);

	 s16CombinedLogData = LS_u16CombSignedIntNUnsignedInt((int16_t)(PressCTRLLocalBus.s16MuxPwrPistonTarDP/10),
	                                                  (uint16_t)(Pct_5msCtrlBus.Pct_5msCtrlPCtrlSt),
	                                                  (uint16_t)(12));
	AHB_LOG_DATA[26] = (uint8_t) (s16CombinedLogData);
	AHB_LOG_DATA[27] = (uint8_t) (s16CombinedLogData >> 8);

	u16CombinedLogData = LS_u16CombineTwoUnsignedInt((uint16_t)(lapcts16TargetPressOldlog/10),
	                                                 (uint16_t)(MuxFIFO.u32tail),
	                                                 (uint16_t)(11));	
	AHB_LOG_DATA[28] = (uint8_t) (u16CombinedLogData);
	AHB_LOG_DATA[29] = (uint8_t) (u16CombinedLogData >> 8);

	AHB_LOG_DATA[30] = (uint8_t) LS_u8LimSignedIntToUnsignedChar(Rbc_CtrlBus.Rbc_CtrlBrkPRednForBaseBrkCtrlr, 10);

	i=0;
	if(SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable										==1) i|=0x01;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PressDumpVlvReq								==1) i|=0x02;
	if(Eem_MainEemFailData.Eem_Fail_Str         											==1) i|=0x04;
	if(Pct_5msCtrlBus.Pct_5msCtrlPCtrlAct													==1) i|=0x08;
	if(MpsD1_Compen_SetFag_at1ms															==1) i|=0x10;
	if(Abc_CtrlBus.Abc_CtrlEscSwtStInfo.EscDisabledBySwt									==1) i|=0x20;
	if(0																					==1) i|=0x40;
	if(Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.ChmbBalVlvReq									==1) i|=0x80;
	AHB_LOG_DATA[31] =(uint8_t) (i);

	/***************************************************************************************************/
	/*                                           LOG_BYTE4                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[32] = (uint8_t) (lapcts16DeltaStrokeMap);
	AHB_LOG_DATA[33] = (uint8_t) (lapcts16DeltaStrokeMap >> 8);
	s16CombinedLogData = LS_u16CombSignedIntNUnsignedInt((int16_t)(Pct_5msCtrlBus.Pct_5msCtrlTarDeltaStk),
	                                                 (uint16_t)(0x000f & lapcts16DeltaStrokeMapAvg),
	                                                 (uint16_t)(12));	
	AHB_LOG_DATA[34] = (uint8_t) (s16CombinedLogData);
	AHB_LOG_DATA[35] = (uint8_t) (s16CombinedLogData >> 8);
	AHB_LOG_DATA[36] = (uint8_t) (lapcts16DeltaStrokeMapAvg >> 4);

	s16CombinedLogData = LS_u16CombSignedIntNUnsignedInt((int16_t)(lapcts16DeltaStrokePerror),
	                                                 (uint16_t)(0x000f & lapcts16DeltaStrokePerrorFinal),
	                                                 (uint16_t)(12));	
	AHB_LOG_DATA[37] = (uint8_t) (s16CombinedLogData);
	AHB_LOG_DATA[38] = (uint8_t) (s16CombinedLogData >> 8);
	AHB_LOG_DATA[39] = (uint8_t) (lapcts16DeltaStrokePerrorFinal >> 4);
	
	/***************************************************************************************************/
	/*                                           LOG_BYTE5                                             */
	/***************************************************************************************************/
//	AHB_LOG_DATA[40] = (uint8_t) (Vlv_ActrBus.Vlv_ActrVlvMonInfo.SimVlvMon/10);
//	AHB_LOG_DATA[41] = (uint8_t) (Vlv_ActrBus.Vlv_ActrVlvMonInfo.PrimCutVlvMon/10);
//	AHB_LOG_DATA[42] = (uint8_t) (Vlv_ActrBus.Vlv_ActrVlvMonInfo.SecdCutVlvMon/10);
//	AHB_LOG_DATA[43] = (uint8_t) (Vlv_ActrBus.Vlv_ActrVlvMonInfo.VlvRLVMon/10);
//	AHB_LOG_DATA[44] = (uint8_t) (Vlv_ActrBus.Vlv_ActrVlvMonInfo.VlvCVMon/10);
//	AHB_LOG_DATA[45] = (uint8_t) (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.RelsVlvReqData[0]/10);
//	AHB_LOG_DATA[46] = (uint8_t) (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.CircVlvReqData[0]/10);
//	AHB_LOG_DATA[47] = (uint8_t) (Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.ChmbBalVlvReqData[0]/10);

	// motor part debugging
	AHB_LOG_DATA[40] = (uint8_t) (lmccs16CalcIqRefPidState);
	AHB_LOG_DATA[41] = (uint8_t) (lmccs16CalcIqRefPidState >> 8);
	AHB_LOG_DATA[42] = (uint8_t) (lmccs16IqRefBase);
	AHB_LOG_DATA[43] = (uint8_t) (lmccs16IqRefBase >> 8);
	AHB_LOG_DATA[44] = (uint8_t) (lmccs16IqRefPid);
	AHB_LOG_DATA[45] = (uint8_t) (lmccs16IqRefPid >> 8);
	AHB_LOG_DATA[46] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotRotgAgSigInfo.MotMechAngleSpdFild);
	AHB_LOG_DATA[47] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotRotgAgSigInfo.MotMechAngleSpdFild >> 8);


	/***************************************************************************************************/
	/*                                           LOG_BYTE6                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[48] = (uint8_t) (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PressDumpVlvReqData[0]/10);
	AHB_LOG_DATA[49] = (uint8_t) (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SimVlvReqData[0]/10);
	AHB_LOG_DATA[50] = (uint8_t) (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReqData[0]/10);
	AHB_LOG_DATA[51] = (uint8_t) (Arb_CtrlBus.Arb_CtrlFinalTarPInfo.CtrlModOfWhlFrntLe);
	AHB_LOG_DATA[52] = (uint8_t) (Arb_CtrlBus.Arb_CtrlFinalTarPInfo.CtrlModOfOfWhlFrntRi);

	i=0;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SimVlvReq		  	 		==1) i|=0x01;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReq      	 		==1) i|=0x02;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SecdCutVlvReq       	 	==1) i|=0x04;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlIvReq			         	 	==1) i|=0x08;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrIvReq         			 	==1) i|=0x10;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RlIvReq       				   	==1) i|=0x20;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RrIvReq     			     	==1) i|=0x40;
	if(/*Proxy_lespu1TCU_SWI_GS*/0											==1) i|=0x80;
	AHB_LOG_DATA[53] =(uint8_t) (i);

	i=0;
	if(Abc_CtrlBus.Abc_CtrlFunctionLamp												==1) i|=0x01;
	if(Abc_CtrlBus.Abc_CtrlEbdCtrlInfo.EbdActFlg	 	 							==1) i|=0x02;
	if(Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsActFlg		 							==1) i|=0x04;
	if(Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.TcsActFlg	 	 							==1) i|=0x08;
	if(Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscActFlg		 							==1) i|=0x10;
	if(lapctu16FadeoutState1EndOK													==1) i|=0x20;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.CircVlvReq							==1) i|=0x40;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.RelsVlvReq							==1) i|=0x80;
	AHB_LOG_DATA[54] = (uint8_t) (i);

	i=0;
	if(Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSet 	 							==1) i|=0x01;
	if(Abc_CtrlBus.Abc_CtrlEscSwtStInfo.TcsDisabledBySwt 							==1) i|=0x02;
	if(lsesu16WallDetectedFlg														==1) i|=0x04;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlOvReq									==1) i|=0x08;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrOvReq									==1) i|=0x10;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RlOvReq									==1) i|=0x20;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RrOvReq									==1) i|=0x40;
	if(PressCTRLLocalBus.u8MultiplexCtrlActive										==1) i|=0x80;
	AHB_LOG_DATA[55] = (uint8_t) (i);

	/***************************************************************************************************/
	/*                                           LOG_BYTE7                                             */
	/***************************************************************************************************/
	if(Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSet == 0)
	{
		AHB_LOG_DATA[56] = (uint8_t) (lsess32TgtPosiOrgSet);
		AHB_LOG_DATA[57] = (uint8_t) (lsess32TgtPosiOrgSet >> 8);
		u8CombinedLogData = LS_u8CombineTwoUnsignedInt((uint16_t)(lsess32TgtPosiOrgSet >> 16),
	                                                   (uint16_t)(Arb_CtrlBus.Arb_CtrlFinalTarPInfo.CtrlModOfWhlReLe), 1);
		AHB_LOG_DATA[58] = (uint8_t) (u8CombinedLogData);
	}
	else
	{
		AHB_LOG_DATA[56] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn);
		AHB_LOG_DATA[57] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn >> 8);
		u8CombinedLogData = LS_u8CombineTwoUnsignedInt((uint16_t)(Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn >> 16), 
	                                                   (uint16_t)(Arb_CtrlBus.Arb_CtrlFinalTarPInfo.CtrlModOfWhlReLe), 1);
		AHB_LOG_DATA[58] = (uint8_t) (u8CombinedLogData);
	}
	
	AHB_LOG_DATA[59] = (uint8_t) (Msp_CtrlBus.Msp_CtrlMotRotgAgSigInfo.StkPosnMeasd);
	AHB_LOG_DATA[60] = (uint8_t) (Msp_CtrlBus.Msp_CtrlMotRotgAgSigInfo.StkPosnMeasd >> 8); /*Check*/
	u8CombinedLogData = LS_u8CombineTwoUnsignedInt((uint16_t)(Msp_CtrlBus.Msp_CtrlMotRotgAgSigInfo.StkPosnMeasd >> 16),
                                                   (uint16_t)(Arb_CtrlBus.Arb_CtrlFinalTarPInfo.CtrlModOfWhlReRi), 1);
	AHB_LOG_DATA[61] = (uint8_t) (u8CombinedLogData);
	
	AHB_LOG_DATA[62] = (uint8_t) (Msp_CtrlBus.Msp_CtrlMotRotgAgSigInfo.MotMechAngleSpdFild);
	AHB_LOG_DATA[63] = (uint8_t) (Msp_CtrlBus.Msp_CtrlMotRotgAgSigInfo.MotMechAngleSpdFild >> 8);

	/***************************************************************************************************/
	/*                                           LOG_BYTE8                                             */
	/***************************************************************************************************/
	if(Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSet == 0)
	{
		AHB_LOG_DATA[64] = (uint8_t) (Ses_CtrlBus.Ses_CtrlMotDqIRefSesInfo.IdRef);
		AHB_LOG_DATA[65] = (uint8_t) (Ses_CtrlBus.Ses_CtrlMotDqIRefSesInfo.IdRef >> 8);
		AHB_LOG_DATA[66] = (uint8_t) (Ses_CtrlBus.Ses_CtrlMotDqIRefSesInfo.IqRef);
		AHB_LOG_DATA[67] = (uint8_t) (Ses_CtrlBus.Ses_CtrlMotDqIRefSesInfo.IqRef >> 8);
	}
	else
	{
		AHB_LOG_DATA[64] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo.IdRef);
		AHB_LOG_DATA[65] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo.IdRef >> 8);
		AHB_LOG_DATA[66] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo.IqRef);
		AHB_LOG_DATA[67] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo.IqRef >> 8);
	}
	u8CombinedLogData = LS_u8CombineTwoUnsignedInt((uint16_t)(MuxCurCmd.u8id), 
                                                   (uint16_t)(MuxCurCmd.u8State), 4);
	AHB_LOG_DATA[68] = (uint8_t) (u8CombinedLogData);
	AHB_LOG_DATA[69] = (uint8_t) LS_u8LimSignedIntToUnsignedChar(Acmio_SenVdcLink, 100);

	s16CombinedLogData = LS_u16CombSignedIntNUnsignedInt((int16_t)(lmccs16WrpmMechRef),
	                                                 (uint16_t)(Mcc_1msCtrlBus.Mcc_1msCtrlMotCtrlMode),
	                                                 (uint16_t)(15));	
	AHB_LOG_DATA[70] = (uint8_t) (s16CombinedLogData);
	AHB_LOG_DATA[71] = (uint8_t) (s16CombinedLogData >> 8);

	/***************************************************************************************************/
	/*                                           LOG_BYTE9                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[72] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIdMeasd);
	AHB_LOG_DATA[73] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIdMeasd>>8);
	AHB_LOG_DATA[74] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIqMeasd);
	AHB_LOG_DATA[75] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIqMeasd>>8);
	AHB_LOG_DATA[76] = (uint8_t) LS_u8LimSignedIntToUnsignedChar(MuxCmdFrntLe.s16WhlTargetP, 100);
	AHB_LOG_DATA[77] = (uint8_t) LS_u8LimSignedIntToUnsignedChar(MuxCmdFrntRi.s16WhlTargetP, 100);
	AHB_LOG_DATA[78] = (uint8_t) LS_u8LimSignedIntToUnsignedChar(MuxCmdReLe.s16WhlTargetP, 100);
	AHB_LOG_DATA[79] = (uint8_t) LS_u8LimSignedIntToUnsignedChar(MuxCmdReRi.s16WhlTargetP, 100);
}
#endif /*IDB_LOG_DATA*/

#if (M_LOGIC_LOG_DATA==M_IDB_LOG_DATA_PCT)
static void LSIDB_vCallIDBLogDataPCT(void)
{
	uint8_t	i;
	uint16_t u16CombinedLogData = 0;
	int16_t  s16CombinedLogData = 0;
	uint8_t  u8CombinedLogData = 0;

	/***************************************************************************************************/
	/*                                           LOG_BYTE0                                             */
	/***************************************************************************************************/
	s16CombinedLogData = LS_u16CombSignedIntNUnsignedInt((int16_t)(Spc_5msCtrlBus.Spc_5msCtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar/10),
	                                                 (uint16_t)(Abc_CtrlBus.Abc_CtrlStkRecvryActnIfInfo.StkRecvryCtrlState/10),
	                                                 (uint16_t)(12));
	AHB_LOG_DATA[0] = (uint8_t) (s16CombinedLogData);
	AHB_LOG_DATA[1] = (uint8_t) (s16CombinedLogData >> 8);

#if __P_SENSOR_MEASURE_TYPE== EACH_CIRCUIT_PRESSURE
	AHB_LOG_DATA[2] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar);
	AHB_LOG_DATA[3] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar>>8);
	AHB_LOG_DATA[4] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar);
	AHB_LOG_DATA[5] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar>>8);
#else
	AHB_LOG_DATA[2] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPistPFildInfo.PistPFild_1_100Bar);
	AHB_LOG_DATA[3] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPistPFildInfo.PistPFild_1_100Bar>>8);
	AHB_LOG_DATA[4] = (uint8_t) (lamtrs16Idc);
	AHB_LOG_DATA[5] = (uint8_t) (lamtrs16Idc>>8);
#endif

	i=0;
	if(MuxCmdFrntLe.u1CtrlReq              			                                    ==1) i|=0x01;
	if(MuxCmdFrntRi.u1CtrlReq              			                                    ==1) i|=0x02;
	if(MuxCmdReLe.u1CtrlReq                                                             ==1) i|=0x04;
	if(MuxCmdReRi.u1CtrlReq                			                                    ==1) i|=0x08;
	if(lapctst_MuxInVCtrlVarFL.lau1ValveAct			                                    ==1) i|=0x10;
	if(lapctst_MuxInVCtrlVarFR.lau1ValveAct			                                    ==1) i|=0x20;
	if(lapctst_MuxInVCtrlVarRL.lau1ValveAct			                                    ==1) i|=0x40;
	if(lapctst_MuxInVCtrlVarRR.lau1ValveAct			                                    ==1) i|=0x80;

	AHB_LOG_DATA[6] =(uint8_t) (i);

	i=0;
	if(Spc_5msCtrlBus.Spc_5msCtrlBlsFild                     	 						==1) i|=0x01;
	if(Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdtOffsCorrdStOkFlg				==1) i|=0x02;
	if(Spc_5msCtrlBus.Spc_5msCtrlPistPOffsCorrdInfo.PistPOffsCorrdStOkFlg	          	==1) i|=0x04;
	if(Bbc_CtrlBus.Bbc_CtrlIgnOnOffSts													==1) i|=0x08;
	if(u8IdbCtrlInhibitFlg																==1) i|=0x10;
	if(Eem_MainEemCtrlInhibitData.Eem_BBS_DefectiveModeFail								==1) i|=0x20;
	if(Mcc_1msCtrlBus.Mcc_1msCtrlMotICtrlFadeOutState									==1) i|=0x40;
	if(Pct_5msCtrlBus.Pct_5msCtrlIdbVlvActInfo.VlvFadeoutEndOK							==1) i|=0x80;

	AHB_LOG_DATA[7] =(uint8_t) (i);

	/***************************************************************************************************/
	/*                                           LOG_BYTE1                                             */
	/***************************************************************************************************/
	u16CombinedLogData = LS_u16CombineTwoUnsignedInt((uint16_t)(Det_5msCtrlBus.Det_5msCtrlPedlTrvlFinal),
	                                                 (uint16_t)(Pct_5msCtrlBus.Pct_5msCtrlPCtrlBoostMod),
	                                                 (uint16_t)(11));
	AHB_LOG_DATA[8] = (uint8_t) (u16CombinedLogData);
	AHB_LOG_DATA[9] = (uint8_t) (u16CombinedLogData >> 8);

	s16CombinedLogData = LS_u16CombSignedIntNUnsignedInt((int16_t)(Det_5msCtrlBus.Det_5msCtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec),
	                                                 (uint16_t)(Pct_5msCtrlBus.Pct_5msCtrlPCtrlSt),
	                                                 (uint16_t)(12));

	AHB_LOG_DATA[10] = (uint8_t) (s16CombinedLogData);
	AHB_LOG_DATA[11] = (uint8_t) (s16CombinedLogData >> 8);

#if __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
	s16CombinedLogData = LS_u16CombSignedIntNUnsignedInt((int16_t)(Det_5msCtrlBus.Det_5msCtrlCircPRateInfo.PrimCircPChgDurg10ms),
	                                                 (uint16_t)(PressCTRLLocalBus.u16MuxWheelVolumeState),
	                                                 (uint16_t)(12));
#else
	s16CombinedLogData = LS_u16CombSignedIntNUnsignedInt((int16_t)(Det_5msCtrlBus.Det_5msCtrlPistPRateInfo.PistPChgDurg10ms),
	                                                 (uint16_t)(PressCTRLLocalBus.u16MuxWheelVolumeState),
	                                                 (uint16_t)(12));
#endif
	AHB_LOG_DATA[12] = (uint8_t) (s16CombinedLogData);
	AHB_LOG_DATA[13] = (uint8_t) (s16CombinedLogData >> 8);

	u8CombinedLogData = LS_u8CombineTwoUnsignedInt((uint16_t)(Pct_5msCtrlBus.Pct_5msCtrlPCtrlFadeoutSt),
	                                               (uint16_t)(PressCTRLLocalBus.u8WhlCtrlIndex), 2);
	AHB_LOG_DATA[14] = (uint8_t) (u8CombinedLogData);

	i=0;
	if(Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.DrvrIntendToRelsBrk	 				==1) i|=0x01;
	if(Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.PanicBrkStFlg         	 			==1) i|=0x02;
	if(Bbc_CtrlBus.Bbc_CtrlBaseBrkCtrlrActFlg               							==1) i|=0x04;
	if(Bbc_CtrlBus.Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo.PedlSimrPGendOnFlg             ==1) i|=0x08;
	if(Bbc_CtrlBus.Bbc_CtrlBaseBrkCtrlModInfo.VehStandStillStFlg		                ==1) i|=0x10;
	if(Det_1msCtrlBus.Det_1msCtrlBrkPedlStatus1msInfo.PanicBrkSt1msFlg                	==1) i|=0x20;
	if(lapcts16MovAvgBuffResetFlg														==1) i|=0x40;
	if(lapctDelStrkActFlg.bit2/*lapctu1NegativeCompDisableFlg*/							==1) i|=0x80;
	AHB_LOG_DATA[15] = (uint8_t) (i);

	/***************************************************************************************************/
	/*                                           LOG_BYTE2                                             */
	/***************************************************************************************************/
	u16CombinedLogData = LS_u16CombineTwoUnsignedInt((uint16_t)(Bbc_CtrlBus.Bbc_CtrlTarPFromBrkPedl/10),
	                                                 (uint16_t)(PressCTRLLocalBus.s16ReqdPCtrlBoostMod),
	                                                 (uint16_t)(11));	
	AHB_LOG_DATA[16] = (uint8_t) (u16CombinedLogData);
	AHB_LOG_DATA[17] = (uint8_t) (u16CombinedLogData >> 8);

	AHB_LOG_DATA[18] = (uint8_t) LS_u8LimSignedIntToUnsignedChar(MuxCurCmd.s16WhlTargetP, 100);
	AHB_LOG_DATA[19] = (uint8_t) LS_u8LimSignedIntToUnsignedChar(MuxCurCmd.s16EstWhlP, 100);

	s16CombinedLogData = LS_u16CombSignedIntNUnsignedInt((int16_t)(PressCTRLLocalBus.s16MuxPwrPistonTarPRate),
	                                                 (uint16_t)(SasM_MainSASMOutInfo.SASM_Timeout_Err),
	                                                 (uint16_t)(12));		
	AHB_LOG_DATA[20] = (uint8_t) (s16CombinedLogData);
	AHB_LOG_DATA[21] = (uint8_t) (s16CombinedLogData >> 8);

	s16CombinedLogData = LS_u16CombSignedIntNUnsignedInt((int16_t)(Det_5msCtrlBus.Det_5msCtrlCircPRateInfo.SecdCircPChgDurg10ms),
	                                                 (uint16_t)(0),
	                                                 (uint16_t)(12));
	AHB_LOG_DATA[22] = (uint8_t) (s16CombinedLogData);
	AHB_LOG_DATA[23] = (uint8_t) (s16CombinedLogData>>8);

	/***************************************************************************************************/
	/*                                           LOG_BYTE3                                             */
	/***************************************************************************************************/
	u16CombinedLogData = LS_u16CombineTwoUnsignedInt((uint16_t)(Pct_5msCtrlBus.Pct_5msCtrlFinalTarPFromPCtrl/10),
	                                                 (uint16_t)(MuxFIFO.u32head),
	                                                 (uint16_t)(11));
	AHB_LOG_DATA[24] = (uint8_t) (u16CombinedLogData);
	AHB_LOG_DATA[25] = (uint8_t) (u16CombinedLogData >> 8);

	 s16CombinedLogData = LS_u16CombSignedIntNUnsignedInt((int16_t)(PressCTRLLocalBus.s16MuxPwrPistonTarDP/10),
	                                                  (uint16_t)(0),
	                                                  (uint16_t)(12));
	AHB_LOG_DATA[26] = (uint8_t) (s16CombinedLogData);
	AHB_LOG_DATA[27] = (uint8_t) (s16CombinedLogData >> 8);

	u16CombinedLogData = LS_u16CombineTwoUnsignedInt((uint16_t)(lapcts16TargetPressOldlog/10),
	                                                 (uint16_t)(MuxFIFO.u32tail),
	                                                 (uint16_t)(11));
	AHB_LOG_DATA[28] = (uint8_t) (u16CombinedLogData);
	AHB_LOG_DATA[29] = (uint8_t) (u16CombinedLogData >> 8);

	AHB_LOG_DATA[30] = (uint8_t) LS_u8LimSignedIntToUnsignedChar(Rbc_CtrlBus.Rbc_CtrlBrkPRednForBaseBrkCtrlr, 10);

	i=0;
	if(SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable										==1) i|=0x01;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PressDumpVlvReq								==1) i|=0x02;
	if(Eem_MainEemFailData.Eem_Fail_Str         											==1) i|=0x04;
	if(Pct_5msCtrlBus.Pct_5msCtrlPCtrlAct													==1) i|=0x08;
	if(/*MpsD1_Compen_SetFag_at1ms*/0															==1) i|=0x10;
	if(Abc_CtrlBus.Abc_CtrlEscSwtStInfo.EscDisabledBySwt									==1) i|=0x20;
	if(/*SasM_MainSASMOutInfo.SASM_Timeout_Ihb*/0												==1) i|=0x40;
	if(Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.ChmbBalVlvReq									==1) i|=0x80;
	AHB_LOG_DATA[31] =(uint8_t) (i);

	/***************************************************************************************************/
	/*                                           LOG_BYTE4                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[32] = (uint8_t) (lapcts16DeltaStrokeMap);
	AHB_LOG_DATA[33] = (uint8_t) (lapcts16DeltaStrokeMap >> 8);
	s16CombinedLogData = LS_u16CombSignedIntNUnsignedInt((int16_t)(Pct_5msCtrlBus.Pct_5msCtrlTarDeltaStk),
	                                                 (uint16_t)(0x000f & lapcts16DeltaStrokeMapAvg),
	                                                 (uint16_t)(12));
	AHB_LOG_DATA[34] = (uint8_t) (s16CombinedLogData);
	AHB_LOG_DATA[35] = (uint8_t) (s16CombinedLogData >> 8);
	AHB_LOG_DATA[36] = (uint8_t) (lapcts16DeltaStrokeMapAvg >> 4);

	s16CombinedLogData = LS_u16CombSignedIntNUnsignedInt((int16_t)(lapcts16DeltaStrokePerror),
	                                                 (uint16_t)(0x000f & lapcts16DeltaStrokePerrorFinal),
	                                                 (uint16_t)(12));
	AHB_LOG_DATA[37] = (uint8_t) (s16CombinedLogData);
	AHB_LOG_DATA[38] = (uint8_t) (s16CombinedLogData >> 8);
	AHB_LOG_DATA[39] = (uint8_t) (lapcts16DeltaStrokePerrorFinal >> 4);

	/***************************************************************************************************/
	/*                                           LOG_BYTE5                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[40] = (uint8_t)(0x07 & Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo.CvVlvSt) + ((0x07 & Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo.CvVlvNoiseCtrlSt)<<3)
									+ ((0x03 & Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo.RlVlvSt)<<6);
	AHB_LOG_DATA[41] = (uint8_t)((0x04 & Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo.RlVlvSt)>>2) + ((0x07 & Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo.RlVlvNoiseCtrlSt)<<1)
									+ ((0x07 & Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo.PdVlvSt)<<4) + ((0x01 & Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo.PdVlvNoiseCtrlSt)<<7);
	AHB_LOG_DATA[42] = (uint8_t)((0x06 & Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo.PdVlvNoiseCtrlSt)>>1) + ((0x07 & Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo.BalVlvSt)<<2) + ((0x07 & Vat_CtrlBus.Vat_CtrlIdbPCtrllVlvCtrlStInfo.BalVlvNoiseCtrlSt)<<5);
	AHB_LOG_DATA[43] = (uint8_t)(((0x0F & Vat_CtrlBus.Vat_CtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvNoiseCtrlSt)<<4) + ((0x0F & Vat_CtrlBus.Vat_CtrlIdbPedlFeelVlvCtrlStInfo.PrimCutVlvSt)));
	AHB_LOG_DATA[44] = (uint8_t)(((0x0F & Vat_CtrlBus.Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SimVlvNoiseCtrlSt)<<4) + ((0x0F & Vat_CtrlBus.Vat_CtrlIdbPedlFeelVlvCtrlStInfo.SimVlvSt)));
	AHB_LOG_DATA[45] = (uint8_t) (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.RelsVlvReqData[0]/10);
	AHB_LOG_DATA[46] = (uint8_t) (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.CircVlvReqData[0]/10);
	AHB_LOG_DATA[47] = (uint8_t) (Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.ChmbBalVlvReqData[0]/10);

	/***************************************************************************************************/
	/*                                           LOG_BYTE6                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[48] = (uint8_t) (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PressDumpVlvReqData[0]/10);
	AHB_LOG_DATA[49] = (uint8_t) (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SimVlvReqData[0]/10);
	AHB_LOG_DATA[50] = (uint8_t) (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReqData[0]/10);
	s16CombinedLogData = LS_u16CombSignedIntNUnsignedInt((int16_t)(lapcts16DeltaStrokePrateError),
	                                                 (uint16_t)(0),
	                                                 (uint16_t)(12));
	AHB_LOG_DATA[51] = (uint8_t) (s16CombinedLogData);
	AHB_LOG_DATA[52] = (uint8_t) (s16CombinedLogData >> 8);

	i=0;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SimVlvReq		  	 		==1) i|=0x01;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReq      	 		==1) i|=0x02;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SecdCutVlvReq       	 	==1) i|=0x04;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlIvReq			         	 	==1) i|=0x08;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrIvReq         			 	==1) i|=0x10;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RlIvReq       				   	==1) i|=0x20;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RrIvReq     			     	==1) i|=0x40;
	if(/*Proxy_lespu1TCU_SWI_GS*/0											==1) i|=0x80;
	AHB_LOG_DATA[53] =(uint8_t) (i);

	i=0;
	if(Abc_CtrlBus.Abc_CtrlFunctionLamp												==1) i|=0x01;
	if(Abc_CtrlBus.Abc_CtrlEbdCtrlInfo.EbdActFlg	 	 							==1) i|=0x02;
	if(Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsActFlg		 							==1) i|=0x04;
	if(Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.TcsActFlg	 	 							==1) i|=0x08;
	if(Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscActFlg		 							==1) i|=0x10;
	if(lapctu16FadeoutState1EndOK													==1) i|=0x20;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.CircVlvReq							==1) i|=0x40;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.RelsVlvReq							==1) i|=0x80;
	AHB_LOG_DATA[54] = (uint8_t) (i);

	i=0;
	if(Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSet 	 							==1) i|=0x01;
	if(Abc_CtrlBus.Abc_CtrlEscSwtStInfo.TcsDisabledBySwt 							==1) i|=0x02;
	if(lamtru16WallDetectedFlg														==1) i|=0x04;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlOvReq									==1) i|=0x08;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrOvReq									==1) i|=0x10;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RlOvReq									==1) i|=0x20;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RrOvReq									==1) i|=0x40;
	if(PressCTRLLocalBus.u8MultiplexCtrlActive										==1) i|=0x80;
	AHB_LOG_DATA[55] = (uint8_t) (i);

	/***************************************************************************************************/
	/*                                           LOG_BYTE7                                             */
	/***************************************************************************************************/
	if(Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSet == 0)
	{
		AHB_LOG_DATA[56] = (uint8_t) (lamtrs32TgtPosiForMotOrgSet);
		AHB_LOG_DATA[57] = (uint8_t) (lamtrs32TgtPosiForMotOrgSet >> 8);
		u8CombinedLogData = LS_u8CombineTwoUnsignedInt((uint16_t)(lamtrs32TgtPosiForMotOrgSet >> 16),
	                                                   (uint16_t)(Arb_CtrlBus.Arb_CtrlFinalTarPInfo.CtrlModOfWhlReLe), 1);
		AHB_LOG_DATA[58] = (uint8_t) (u8CombinedLogData);
	}
	else
	{
		AHB_LOG_DATA[56] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn);
		AHB_LOG_DATA[57] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn >> 8);
		u8CombinedLogData = LS_u8CombineTwoUnsignedInt((uint16_t)(Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn >> 16),
	                                                   (uint16_t)(Arb_CtrlBus.Arb_CtrlFinalTarPInfo.CtrlModOfWhlReLe), 1);
		AHB_LOG_DATA[58] = (uint8_t) (u8CombinedLogData);
	}

	AHB_LOG_DATA[59] = (uint8_t) (Msp_CtrlBus.Msp_CtrlMotRotgAgSigInfo.StkPosnMeasd);
	AHB_LOG_DATA[60] = (uint8_t) (Msp_CtrlBus.Msp_CtrlMotRotgAgSigInfo.StkPosnMeasd >> 8); /*Check*/
	u8CombinedLogData = LS_u8CombineTwoUnsignedInt((uint16_t)(Msp_CtrlBus.Msp_CtrlMotRotgAgSigInfo.StkPosnMeasd >> 16),
                                                   (uint16_t)(Arb_CtrlBus.Arb_CtrlFinalTarPInfo.CtrlModOfWhlReRi), 1);
	AHB_LOG_DATA[61] = (uint8_t) (u8CombinedLogData);

	AHB_LOG_DATA[62] = (uint8_t) (Msp_CtrlBus.Msp_CtrlMotRotgAgSigInfo.MotMechAngleSpdFild);
	AHB_LOG_DATA[63] = (uint8_t) (Msp_CtrlBus.Msp_CtrlMotRotgAgSigInfo.MotMechAngleSpdFild >> 8);

	/***************************************************************************************************/
	/*                                           LOG_BYTE8                                             */
	/***************************************************************************************************/
	if(Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSet == 0)
	{
		AHB_LOG_DATA[64] = (uint8_t) (Ses_CtrlBus.Ses_CtrlMotDqIRefSesInfo.IdRef);
		AHB_LOG_DATA[65] = (uint8_t) (Ses_CtrlBus.Ses_CtrlMotDqIRefSesInfo.IdRef >> 8);
		AHB_LOG_DATA[66] = (uint8_t) (Ses_CtrlBus.Ses_CtrlMotDqIRefSesInfo.IqRef);
		AHB_LOG_DATA[67] = (uint8_t) (Ses_CtrlBus.Ses_CtrlMotDqIRefSesInfo.IqRef >> 8);
	}
	else
	{
		AHB_LOG_DATA[64] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo.IdRef);
		AHB_LOG_DATA[65] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo.IdRef >> 8);
		AHB_LOG_DATA[66] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo.IqRef);
		AHB_LOG_DATA[67] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo.IqRef >> 8);
	}
	u8CombinedLogData = LS_u8CombineTwoUnsignedInt((uint16_t)(MuxCurCmd.u8id),
                                                   (uint16_t)(MuxCurCmd.u8State), 4);
	AHB_LOG_DATA[68] = (uint8_t) (u8CombinedLogData);
	AHB_LOG_DATA[69] = (uint8_t) LS_u8LimSignedIntToUnsignedChar(Acmio_SenVdcLink, 100);

	s16CombinedLogData = LS_u16CombSignedIntNUnsignedInt((int16_t)(lamtrs16WrpmMechRef),
	                                                 (uint16_t)(Mcc_1msCtrlBus.Mcc_1msCtrlMotCtrlMode),
	                                                 (uint16_t)(15));
	AHB_LOG_DATA[70] = (uint8_t) (s16CombinedLogData);
	AHB_LOG_DATA[71] = (uint8_t) (s16CombinedLogData >> 8);

	/***************************************************************************************************/
	/*                                           LOG_BYTE9                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[72] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIdMeasd);
	AHB_LOG_DATA[73] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIdMeasd>>8);
	AHB_LOG_DATA[74] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIqMeasd);
	AHB_LOG_DATA[75] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIqMeasd>>8);
	AHB_LOG_DATA[76] = (uint8_t) LS_u8LimSignedIntToUnsignedChar(MuxCmdFrntLe.s16WhlTargetP, 100);
	AHB_LOG_DATA[77] = (uint8_t) LS_u8LimSignedIntToUnsignedChar(MuxCmdFrntRi.s16WhlTargetP, 100);
	AHB_LOG_DATA[78] = (uint8_t) LS_u8LimSignedIntToUnsignedChar(MuxCmdReLe.s16WhlTargetP, 100);
	AHB_LOG_DATA[79] = (uint8_t) LS_u8LimSignedIntToUnsignedChar(MuxCmdReRi.s16WhlTargetP, 100);
}
  
#endif /*M_IDB_LOG_DATA_PCT*/

#if (M_LOGIC_LOG_DATA==M_IDB_ARB_LOG_DATA)
static void LSIDB_vCallIDBARBLogData(void)
{
	uint8_t	i;

	/***************************************************************************************************/
	/*                                           LOG_BYTE0                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[0] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar);
	AHB_LOG_DATA[1] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar >> 8);

#if __P_SENSOR_MEASURE_TYPE== EACH_CIRCUIT_PRESSURE
	AHB_LOG_DATA[2] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar);
	AHB_LOG_DATA[3] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar>>8);
	AHB_LOG_DATA[4] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar);
	AHB_LOG_DATA[5] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar>>8);
#else
	AHB_LOG_DATA[2] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPistPFildInfo.PistPFild_1_100Bar);
	AHB_LOG_DATA[3] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPistPFildInfo.PistPFild_1_100Bar>>8);
	AHB_LOG_DATA[4] = (uint8_t) (lacmctls16Idc);
	AHB_LOG_DATA[5] = (uint8_t) (lacmctls16Idc>>8);
#endif
	AHB_LOG_DATA[6] = (uint8_t) LS_u8LimSignedIntToUnsignedChar(Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalTarPOfWhlReLe,100);//불필요(Abc_CtrlBus.Abc_CtrlStkRecvryActnIfInfo.StkRecvryCtrlState);

	i=0;
	if(Spc_5msCtrlBus.Spc_5msCtrlBlsFild                     	 						==1) i|=0x01;
	if(Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdtOffsCorrdStOkFlg				==1) i|=0x02;
	if(Spc_5msCtrlBus.Spc_5msCtrlPistPOffsCorrdInfo.PistPOffsCorrdStOkFlg	          	==1) i|=0x04;
	if(Bbc_CtrlBus.Bbc_CtrlIgnOnOffSts													==1) i|=0x08;
	if(u8IdbCtrlInhibitFlg																==1) i|=0x10;
	if(Eem_MainEemCtrlInhibitData.Eem_BBS_DefectiveModeFail								==1) i|=0x20;
	if(Mcc_1msCtrlBus.Mcc_1msCtrlMotICtrlFadeOutState									==1) i|=0x40;
	if(Pct_5msCtrlBus.Pct_5msCtrlIdbVlvActInfo.VlvFadeoutEndOK							==1) i|=0x80;

	AHB_LOG_DATA[7] =(uint8_t) (i);

	/***************************************************************************************************/
	/*                                           LOG_BYTE1                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[8]  = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPedlTrvlFinal);
	AHB_LOG_DATA[9]  = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPedlTrvlFinal >> 8);
	AHB_LOG_DATA[10]  = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec);
	AHB_LOG_DATA[11]  = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec >> 8);
	
//#if __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
	AHB_LOG_DATA[12]  = (uint8_t) LS_u8LimSignedIntToUnsignedChar(Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalTarPOfWhlFrntLe,100);//(Det_5msCtrlBus.Det_5msCtrlCircPRateInfo.PrimCircPChgDurg10ms);
	AHB_LOG_DATA[13]  = (uint8_t) LS_u8LimSignedIntToUnsignedChar(Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalTarPOfWhlFrntRi,100);//불필요(Det_5msCtrlBus.Det_5msCtrlCircPRateInfo.PrimCircPChgDurg10ms >>8);
//#else
//	AHB_LOG_DATA[12]  = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPistPRateInfo.PistPChgDurg10ms);
//	AHB_LOG_DATA[13]  = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPistPRateInfo.PistPChgDurg10ms>>8);
//#endif
	AHB_LOG_DATA[14] = (uint8_t) (PressCTRLLocalBus.u16MuxWheelVolumeState);

	i=0;
	if(Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.DrvrIntendToRelsBrk	 				==1) i|=0x01;
	if(Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.PanicBrkStFlg         	 			==1) i|=0x02;
	if(Bbc_CtrlBus.Bbc_CtrlBaseBrkCtrlrActFlg               							==1) i|=0x04;
	if(Bbc_CtrlBus.Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo.PedlSimrPGendOnFlg             ==1) i|=0x08;
	if(Bbc_CtrlBus.Bbc_CtrlBaseBrkCtrlModInfo.VehStandStillStFlg		                ==1) i|=0x10;
	if(Det_1msCtrlBus.Det_1msCtrlBrkPedlStatus1msInfo.PanicBrkSt1msFlg                	==1) i|=0x20;
	if(lapcts16MovAvgBuffResetFlg														==1) i|=0x40;
	if(lapctDelStrkActFlg.bit2/*lapctu1NegativeCompDisableFlg*/							==1) i|=0x80;
	AHB_LOG_DATA[15] = (uint8_t) (i);

	/***************************************************************************************************/
	/*                                           LOG_BYTE2                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[16]  = (uint8_t) LS_u8LimSignedIntToUnsignedChar(Bbc_CtrlBus.Bbc_CtrlTarPFromBaseBrkCtrlr,50);//(Bbc_CtrlBus.Bbc_CtrlTarPFromBaseBrkCtrlr); 줄여서
	AHB_LOG_DATA[17]  = (uint8_t) (Arb_CtrlBus.Arb_CtrlFinalTarPInfo.CtrlModOfWhlFrntLe);//(Bbc_CtrlBus.Bbc_CtrlTarPFromBaseBrkCtrlr >> 8);

	AHB_LOG_DATA[18]  = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlPCtrlFadeoutSt);
	AHB_LOG_DATA[19]  = (uint8_t) (PressCTRLLocalBus.u8WhlCtrlIndex);

	AHB_LOG_DATA[20]  = (uint8_t) LS_u8LimSignedIntToSignedChar(PressCTRLLocalBus.s16MuxPwrPistonTarPRate,5);//(PressCTRLLocalBus.s16MuxPwrPistonTarPRate); 줄여   /*Rbc_CtrlBus.Rbc_CtrlBrkPRednForBaseBrkCtrlr*/
	AHB_LOG_DATA[21]  = (uint8_t) (Arb_CtrlBus.Arb_CtrlFinalTarPInfo.CtrlModOfOfWhlFrntRi);//(PressCTRLLocalBus.s16MuxPwrPistonTarPRate >>8);

	AHB_LOG_DATA[22]  = (uint8_t) (Bbc_CtrlBus.Bbc_CtrlTarPFromBrkPedl/100);
	AHB_LOG_DATA[23]  = (uint8_t) (SasM_MainSASMOutInfo.SASM_Timeout_Err/*lespu8TCU_G_SEL_DISP*/);

	/***************************************************************************************************/
	/*                                           LOG_BYTE3                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[24] = (uint8_t) LS_u8LimSignedIntToUnsignedChar(Pct_5msCtrlBus.Pct_5msCtrlFinalTarPFromPCtrl,50);//(Pct_5msCtrlBus.Pct_5msCtrlFinalTarPFromPCtrl); 줄여
	AHB_LOG_DATA[25] = (uint8_t) (Arb_CtrlBus.Arb_CtrlFinalTarPInfo.CtrlModOfWhlReLe);//(Pct_5msCtrlBus.Pct_5msCtrlFinalTarPFromPCtrl >>8);
	AHB_LOG_DATA[26] = (uint8_t) LS_u8LimSignedIntToSignedChar(PressCTRLLocalBus.s16MuxPwrPistonTarDP,10); ;//(PressCTRLLocalBus.s16MuxPwrPistonTarDP ); 1/10
	AHB_LOG_DATA[27] = (uint8_t) (Arb_CtrlBus.Arb_CtrlFinalTarPInfo.CtrlModOfWhlReRi);//(PressCTRLLocalBus.s16MuxPwrPistonTarDP >> 8);
	AHB_LOG_DATA[28] = (uint8_t) (lapcts16DeltaStrokeMap);
	AHB_LOG_DATA[29] = (uint8_t) (lapcts16DeltaStrokeMap >> 8);
	AHB_LOG_DATA[30] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotCtrlMode);

	i=0;
	if(SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable										==1) i|=0x01;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PressDumpVlvReq								==1) i|=0x02;
	if(Eem_MainEemFailData.Eem_Fail_Str         											==1) i|=0x04;
	if(Pct_5msCtrlBus.Pct_5msCtrlPCtrlAct													==1) i|=0x08;
	if(MpsD1_Compen_SetFag_at1ms															==1) i|=0x10;
//	if(Abc_CtrlBus.Abc_CtrlEscSwtStInfo.EscDisabledBySwt									==1) i|=0x20;
	if(Arb_CtrlBus.Arb_CtrlFinalTarPInfo.PCtrlPrioOfWhlFrntLe								==1) i|=0x20;
	if(SasM_MainSASMOutInfo.SASM_Timeout_Ihb												==1) i|=0x40;
	if(Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.ChmbBalVlvReq									==1) i|=0x80;
	AHB_LOG_DATA[31] =(uint8_t) (i);

	/***************************************************************************************************/
	/*                                           LOG_BYTE4                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[32] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlPCtrlBoostMod);
	AHB_LOG_DATA[33] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlPCtrlSt);
	AHB_LOG_DATA[34] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlTarDeltaStk);
	AHB_LOG_DATA[35] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlTarDeltaStk >> 8);
	AHB_LOG_DATA[36] = (uint8_t) (lapcts16DeltaStrokePerror);
	AHB_LOG_DATA[37] = (uint8_t) (lapcts16DeltaStrokePerror>>8);
	AHB_LOG_DATA[38] = (uint8_t) (lapcts16DeltaStrokeMapAvg);
	AHB_LOG_DATA[39] = (uint8_t) (lapcts16DeltaStrokeMapAvg >> 8);

	/***************************************************************************************************/
	/*                                           LOG_BYTE5                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[40] = (uint8_t) (Vlv_ActrBus.Vlv_ActrVlvMonInfo.SimVlvMon/10);
	AHB_LOG_DATA[41] = (uint8_t) (Vlv_ActrBus.Vlv_ActrVlvMonInfo.PrimCutVlvMon/10);
	AHB_LOG_DATA[42] = (uint8_t) (Vlv_ActrBus.Vlv_ActrVlvMonInfo.SecdCutVlvMon/10);
	AHB_LOG_DATA[43] = (uint8_t) (Vlv_ActrBus.Vlv_ActrVlvMonInfo.VlvInFLMon/10);/*RLV Vlv Mon C-sample*/
	AHB_LOG_DATA[44] = (uint8_t) (Vlv_ActrBus.Vlv_ActrVlvMonInfo.VlvInFRMon/10);/*CV Vlv Mon C-sample*/
	AHB_LOG_DATA[45] = (uint8_t) (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.RelsVlvReqData[0]/10);
	AHB_LOG_DATA[46] = (uint8_t) (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.CircVlvReqData[0]/10);
	AHB_LOG_DATA[47] = (uint8_t) (Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.ChmbBalVlvReqData[0]/10);

	/***************************************************************************************************/
	/*                                           LOG_BYTE6                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[48] = (uint8_t) (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PressDumpVlvReqData[0]/10);
	AHB_LOG_DATA[49] = (uint8_t) (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SimVlvReqData[0]/10);
	AHB_LOG_DATA[50] = (uint8_t) (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReqData[0]/10);

	AHB_LOG_DATA[51] = (uint8_t)(Msp_CtrlBus.Msp_CtrlMotRotgAgSigInfo.MotMechAngleSpdFild);
	AHB_LOG_DATA[52] = (uint8_t)(Msp_CtrlBus.Msp_CtrlMotRotgAgSigInfo.MotMechAngleSpdFild >>8);

	i=0;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SimVlvReq		  	 		==1) i|=0x01;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReq      	 		==1) i|=0x02;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SecdCutVlvReq       	 	==1) i|=0x04;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlIvReq			         	 	==1) i|=0x08;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrIvReq         			 	==1) i|=0x10;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RlIvReq       				   	==1) i|=0x20;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RrIvReq     			     	==1) i|=0x40;
	if(/*Proxy_lespu1TCU_SWI_GS*/0											==1) i|=0x80;
	AHB_LOG_DATA[53] =(uint8_t) (i);

	i=0;
	if(Abc_CtrlBus.Abc_CtrlFunctionLamp												==1) i|=0x01;
	if(Abc_CtrlBus.Abc_CtrlEbdCtrlInfo.EbdActFlg	 	 							==1) i|=0x02;
	if(Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsActFlg		 							==1) i|=0x04;
	if(Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.TcsActFlg	 	 							==1) i|=0x08;
	if(Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscActFlg		 							==1) i|=0x10;
	if(lapctu16FadeoutState1EndOK													==1) i|=0x20;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.CircVlvReq							==1) i|=0x40;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.RelsVlvReq							==1) i|=0x80;
	AHB_LOG_DATA[54] = (uint8_t) (i);

	i=0;
	if(Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSet 	 							==1) i|=0x01;
//	if(Abc_CtrlBus.Abc_CtrlEscSwtStInfo.TcsDisabledBySwt 							==1) i|=0x02;
	if(Arb_CtrlBus.Arb_CtrlFinalTarPInfo.PCtrlPrioOfWhlFrntRi						==1) i|=0x02;
	if(lsesu16WallDetectedFlg														==1) i|=0x04;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlOvReq									==1) i|=0x08;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrOvReq									==1) i|=0x10;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RlOvReq									==1) i|=0x20;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RrOvReq									==1) i|=0x40;
	if(PressCTRLLocalBus.u8MultiplexCtrlActive										==1) i|=0x80;
	AHB_LOG_DATA[55] = (uint8_t) (i);

	/***************************************************************************************************/
	/*                                           LOG_BYTE7                                             */
	/***************************************************************************************************/
	if(Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSet == 0)
	{
		AHB_LOG_DATA[56] = (uint8_t) (lsess32TgtPosiOrgSet);
		AHB_LOG_DATA[57] = (uint8_t) (lsess32TgtPosiOrgSet >> 8);
		AHB_LOG_DATA[58] = (uint8_t) (lsess32TgtPosiOrgSet >> 16);
		AHB_LOG_DATA[59] = (uint8_t) (lsess32TgtPosiOrgSet >> 24);
	}
	else
	{
		AHB_LOG_DATA[56] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn);
		AHB_LOG_DATA[57] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn >> 8);
		AHB_LOG_DATA[58] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn >> 16);
		AHB_LOG_DATA[59] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn >> 24);
	}
	AHB_LOG_DATA[60] = (uint8_t) (Msp_1msCtrlBus.Msp_1msCtrlMotRotgAg1msSigInfo.StkPosnMeasd);
	AHB_LOG_DATA[61] = (uint8_t) (Msp_1msCtrlBus.Msp_1msCtrlMotRotgAg1msSigInfo.StkPosnMeasd >> 8);
	AHB_LOG_DATA[62] = (uint8_t) (Msp_1msCtrlBus.Msp_1msCtrlMotRotgAg1msSigInfo.StkPosnMeasd >> 16);
	AHB_LOG_DATA[63] = (uint8_t) (Msp_1msCtrlBus.Msp_1msCtrlMotRotgAg1msSigInfo.StkPosnMeasd >> 24);

	/***************************************************************************************************/
	/*                                           LOG_BYTE8                                             */
	/***************************************************************************************************/
	if(Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSet == 0)
	{
		AHB_LOG_DATA[64] = (uint8_t) (Ses_CtrlBus.Ses_CtrlMotDqIRefSesInfo.IdRef);
		AHB_LOG_DATA[65] = (uint8_t) (Ses_CtrlBus.Ses_CtrlMotDqIRefSesInfo.IdRef >> 8);
		AHB_LOG_DATA[66] = (uint8_t) (Ses_CtrlBus.Ses_CtrlMotDqIRefSesInfo.IqRef);
		AHB_LOG_DATA[67] = (uint8_t) (Ses_CtrlBus.Ses_CtrlMotDqIRefSesInfo.IqRef >> 8);
	}
	else
	{
		AHB_LOG_DATA[64] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo.IdRef);
		AHB_LOG_DATA[65] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo.IdRef >> 8);
		AHB_LOG_DATA[66] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo.IqRef);
		AHB_LOG_DATA[67] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo.IqRef >> 8);
	}
	AHB_LOG_DATA[68] = (uint8_t) (lapcts16TargetPressOldlog);
	AHB_LOG_DATA[69] = (uint8_t) (lapcts16TargetPressOldlog >>8);
	AHB_LOG_DATA[70] = (uint8_t) (lmccs16WrpmMechRef);
	AHB_LOG_DATA[71] = (uint8_t) (lmccs16WrpmMechRef >> 8);

	/***************************************************************************************************/
	/*                                           LOG_BYTE9                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[72] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIdMeasd);
	AHB_LOG_DATA[73] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIdMeasd>>8);
	AHB_LOG_DATA[74] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIqMeasd);
	AHB_LOG_DATA[75] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIqMeasd>>8);
	AHB_LOG_DATA[76] = (uint8_t) LS_u8LimSignedIntToUnsignedChar(Acmio_SenVdcLink,10);;//(Acmio_SenVdcLink); 
	AHB_LOG_DATA[77] = (uint8_t) LS_u8LimSignedIntToUnsignedChar(Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalTarPOfWhlReRi,100);//(Acmio_SenVdcLink >> 8);
	AHB_LOG_DATA[78] = (uint8_t) (lapcts16DeltaStrokePerrorFinal);
	AHB_LOG_DATA[79] = (uint8_t) (lapcts16DeltaStrokePerrorFinal >> 8);
}
#endif /*IDB_LOG_DATA*/
#if (M_LOGIC_LOG_DATA ==M_DETECTION_LOG_DATA)

extern Acmio_ActrMotPwmDataInfo_t Ioc_OutputCS50usMotPwmDataInfo;
extern Acmio_Actr_HdrBusType Acmio_ActrBus;
extern uint8_t  u8Mgd9180_NormalSpiStatus;
extern Mtr_Processing_SigMtrProcessOutInfo_t Mtr_Processing_SigMtrProcessOutInfo;
extern Acmio_SenMotCurrInfo_t Acmio_SenMotCurrInfo;
static void LSIDB_vCallDetectionLogData(void)
{
	uint8_t	i;

	/***************************************************************************************************/
	/*                                           LOG_BYTE0                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[0] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotReqDataAcmctlInfo.MotPwmPhUData);
	AHB_LOG_DATA[1] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotReqDataAcmctlInfo.MotPwmPhUData >> 8);

#if __P_SENSOR_MEASURE_TYPE== EACH_CIRCUIT_PRESSURE
	AHB_LOG_DATA[2] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotReqDataAcmctlInfo.MotPwmPhVData);
	AHB_LOG_DATA[3] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotReqDataAcmctlInfo.MotPwmPhVData>>8);
	AHB_LOG_DATA[4] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotReqDataAcmctlInfo.MotPwmPhWData);
	AHB_LOG_DATA[5] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotReqDataAcmctlInfo.MotPwmPhWData>>8);
#else
	AHB_LOG_DATA[2] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPistPFildInfo.PistPFild_1_100Bar);
	AHB_LOG_DATA[3] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPistPFildInfo.PistPFild_1_100Bar>>8);
	AHB_LOG_DATA[4] = (uint8_t) 0;//(.PistPFildInfo.);
	AHB_LOG_DATA[5] = (uint8_t) 0;//(.PistPFildInfo.SecdCircPFild>>8);
#endif

	AHB_LOG_DATA[6] = 0;
	i=0;
	if(Spc_5msCtrlBus.Spc_5msCtrlBlsFild                      	 						==1) i|=0x01;
	if(Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.PedlReldStUsingPedlSimrPFlg			==1) i|=0x02;
	//	    if(DET_BIT.PedalSigChg											==1) i|=0x04;

	AHB_LOG_DATA[7] = (uint8_t) (i);

	/***************************************************************************************************/
	/*                                           LOG_BYTE1                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[8]  = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPedlTrvlFinal);
	AHB_LOG_DATA[9]  = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPedlTrvlFinal >> 8);
	AHB_LOG_DATA[10]  = (uint8_t) (Acmio_SenVdcLink);
	AHB_LOG_DATA[11]  = (uint8_t) (Acmio_SenVdcLink>>8);

#if __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
	AHB_LOG_DATA[12]  = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlCircPRateInfo.PrimCircPRateBarPerSec);
	AHB_LOG_DATA[13]  = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlCircPRateInfo.PrimCircPRateBarPerSec>>8);
#else
	AHB_LOG_DATA[12]  = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPistPRateInfo.PistPRateBarPerSec);
	AHB_LOG_DATA[13]  = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPistPRateInfo.PistPRateBarPerSec>>8);
#endif
	AHB_LOG_DATA[14] = (uint8_t) (u8Mgd9180_NormalSpiStatus);

	i=0;
	if(Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.DrvrIntendToRelsBrk	 				==1) i|=0x01;
	if(Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.PanicBrkStFlg         	 			==1) i|=0x02;
	if(Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.PedlReldStFlg1               		==1) i|=0x04;
	if(Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.PedlReldStFlg2               		==1) i|=0x08;
	if(Det_5msCtrlBus.Det_5msCtrlVehStopStInfo.VehStopStFlg		                 		==1) i|=0x10;
	if(Det_1msCtrlBus.Det_1msCtrlBrkPedlStatus1msInfo.PanicBrkSt1msFlg                	==1) i|=0x20;
	AHB_LOG_DATA[15] = (uint8_t) (i);

	/***************************************************************************************************/
	/*                                           LOG_BYTE2                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[16]  = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotCurrInfo.MotCurrPhUMeasd);
	AHB_LOG_DATA[17]  = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotCurrInfo.MotCurrPhUMeasd>>8);

	AHB_LOG_DATA[18]  = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotCurrInfo.MotCurrPhVMeasd);
	AHB_LOG_DATA[19]  = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotCurrInfo.MotCurrPhVMeasd>>8);

	AHB_LOG_DATA[20]  = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotCurrInfo.MotCurrPhWMeasd);
	AHB_LOG_DATA[21]  = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotCurrInfo.MotCurrPhWMeasd>>8);

	AHB_LOG_DATA[22]  = (uint8_t)  (Bbc_CtrlBus.Bbc_CtrlTarPFromBrkPedl/100);

	i=0;
	if(Bbc_CtrlBus.Bbc_CtrlBaseBrkCtrlrActFlg                      	 					==1) i|=0x01;
	if(Bbc_CtrlBus.Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo.PedlSimrPGendOnFlg         	==1) i|=0x02;
	if(Rbc_CtrlBus.Rbc_CtrlRgnBrkCtrlrActStFlg	                	 					==1) i|=0x04;
	if(Rbc_CtrlBus.Rbc_CtrlRgnBrkCtlrBlendgFlg	                	 					==1) i|=0x08;
	AHB_LOG_DATA[23] =(uint8_t) (i);

	/***************************************************************************************************/
	/*                                           LOG_BYTE3                                             */
	/***************************************************************************************************/
#if __P_SENSOR_MEASURE_TYPE ==EACH_CIRCUIT_PRESSURE
	AHB_LOG_DATA[24] = (uint8_t) (Acmio_ActrBus.Acmio_ActrMotPwmDataInfo.MotPwmPhUhighData );
	AHB_LOG_DATA[25] = (uint8_t) (Acmio_ActrBus.Acmio_ActrMotPwmDataInfo.MotPwmPhUhighData >> 8);

	AHB_LOG_DATA[26] = (uint8_t) (Acmio_ActrBus.Acmio_ActrMotPwmDataInfo.MotPwmPhVhighData);
	AHB_LOG_DATA[27] = (uint8_t) (Acmio_ActrBus.Acmio_ActrMotPwmDataInfo.MotPwmPhVhighData >> 8);
	AHB_LOG_DATA[28] = (uint8_t) (Acmio_ActrBus.Acmio_ActrMotPwmDataInfo.MotPwmPhWhighData);
	AHB_LOG_DATA[29] = (uint8_t) (Acmio_ActrBus.Acmio_ActrMotPwmDataInfo.MotPwmPhWhighData >> 8);
#else
	AHB_LOG_DATA[24] = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPistPRateInfo.PistPChgDurg10ms );
	AHB_LOG_DATA[25] = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPistPRateInfo.PistPChgDurg10ms >> 8);

	AHB_LOG_DATA[26] = (uint8_t) (Det_1msCtrlBus.Det_1msCtrlPistPRate1msInfo.PistPRate1msAvg_1_100Bar);
	AHB_LOG_DATA[27] = (uint8_t) (Det_1msCtrlBus.Det_1msCtrlPistPRate1msInfo.PistPRate1msAvg_1_100Bar >> 8);
	AHB_LOG_DATA[28] = (uint8_t) (0);
	AHB_LOG_DATA[29] = (uint8_t) (0);
#endif
	AHB_LOG_DATA[30] = (uint8_t) ldahbu8PedalState;
	i=0;
//	if(.StkRecvryActnIfInfo.MotActnReqForStkRecvry		==1) i|=0x01;
//	if(.StkRecvryActnIfInfo.StkRecvryEmgyEntryReqFlg	==1) i|=0x02;
//	if(.StkRecvryActnIfInfo.StkRecvryReqCmpldFlg		==1) i|=0x04;
	AHB_LOG_DATA[31] =(uint8_t) (i);

	/***************************************************************************************************/
	/*                                           LOG_BYTE4                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[32] = (uint8_t) (Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhUhighData);
	AHB_LOG_DATA[33] = (uint8_t) (Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhUhighData>>8);
	AHB_LOG_DATA[34] = (uint8_t) (Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhVhighData);
	AHB_LOG_DATA[35] = (uint8_t) (Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhVhighData >> 8);
	AHB_LOG_DATA[36] = (uint8_t) (Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhWhighData);
	AHB_LOG_DATA[37] = (uint8_t) (Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhWhighData>>8);
#if __P_SENSOR_MEASURE_TYPE ==EACH_CIRCUIT_PRESSURE
	AHB_LOG_DATA[38] = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlCircPRateInfo.PrimCircPChgDurg5ms);
	AHB_LOG_DATA[39] = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlCircPRateInfo.PrimCircPChgDurg5ms >> 8);
#else
	AHB_LOG_DATA[38] = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPistPRateInfo.PistPChgDurg5ms);
	AHB_LOG_DATA[39] = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPistPRateInfo.PistPChgDurg5ms >> 8);
#endif

	/***************************************************************************************************/
	/*                                           LOG_BYTE5                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[40] = (uint8_t) (Vlv_ActrBus.Vlv_ActrVlvMonInfo.SimVlvMon/10);
	AHB_LOG_DATA[41] = (uint8_t) (Vlv_ActrBus.Vlv_ActrVlvMonInfo.PrimCutVlvMon/10);
	AHB_LOG_DATA[42] = (uint8_t) (Vlv_ActrBus.Vlv_ActrVlvMonInfo.VlvCVMon/10);
	AHB_LOG_DATA[43] = (uint8_t) (lsahbu8PDTSigTag);
	AHB_LOG_DATA[44] = (uint8_t) (lsahbu8PDFSigTag);
	AHB_LOG_DATA[45] = (uint8_t) (lsahbu8PSPSigTag);
	AHB_LOG_DATA[46] = (uint8_t) (ldahbu8BrkReleaseIntendCnt);
	AHB_LOG_DATA[47] = (uint8_t) (ldahbs8FastPedalApplyCnt);

	/***************************************************************************************************/
	/*                                           LOG_BYTE6                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[48] = (uint8_t) (Det_1msCtrlBus.Det_1msCtrlPedlTrvlFild1msInfo.PdtFild1ms/10);
	AHB_LOG_DATA[49] = (uint8_t) (ldahbs16PedalReleaseDctCnt);

	i=0;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SimVlvReq		  	 		==1) i|=0x01;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReq      	 		==1) i|=0x02;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SecdCutVlvReq       	 	==1) i|=0x04;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PrimCircVlvReq         	 	==1) i|=0x08;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SecdCircVlvReq          	==1) i|=0x10;
	if(0/*lau1ValveDriveReq*/												==1) i|=0x20;
	AHB_LOG_DATA[50] =(uint8_t) (i);

	AHB_LOG_DATA[51] = (uint8_t) (lsahbs16PedalTravelByPS/10);
	AHB_LOG_DATA[52] = (uint8_t) (Abc_CtrlBus.Abc_CtrlVehSpd/8);
	AHB_LOG_DATA[53] = (uint8_t) (Proxy_RxBus.Proxy_RxCanRxAccelPedlInfo.AccelPedlVal);

	i=0;
	if(0					==1) i|=0x01;
	if(Abc_CtrlBus.Abc_CtrlEbdCtrlInfo.EbdActFlg	 	 							==1) i|=0x02;
	if(Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsActFlg		 							==1) i|=0x04;
	if(Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.TcsActFlg	 	 							==1) i|=0x08;
	if(Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscActFlg		 							==1) i|=0x10;
	AHB_LOG_DATA[54] =(uint8_t) (i);

	i=0;
	if (Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSet 	 							==1) i|=0x01;
	if (Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSetErr	 						==1) i|=0x02;
	if (lsesu16WallDetectedFlg														==1) i|=0x04;
	if (0																			==1) i|=0x08;
	AHB_LOG_DATA[55] =(uint8_t) (i);

	/***************************************************************************************************/
	/*                                           LOG_BYTE7                                             */
	/***************************************************************************************************/
	if(Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSet == 0)
	{
		AHB_LOG_DATA[56] = (uint8_t) (lsess32TgtPosiOrgSet);
		AHB_LOG_DATA[57] = (uint8_t) (lsess32TgtPosiOrgSet >> 8);
		AHB_LOG_DATA[58] = (uint8_t) (lsess32TgtPosiOrgSet >> 16);
		AHB_LOG_DATA[59] = (uint8_t) (lsess32TgtPosiOrgSet >> 24);
	}
	else
	{
		AHB_LOG_DATA[56] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn);
		AHB_LOG_DATA[57] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn >> 8);
		AHB_LOG_DATA[58] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn >> 16);
		AHB_LOG_DATA[59] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn >> 24);
	}
	AHB_LOG_DATA[60] = (uint8_t) (Msp_CtrlBus.Msp_CtrlMotRotgAgSigInfo.StkPosnMeasd);
	AHB_LOG_DATA[61] = (uint8_t) (Msp_CtrlBus.Msp_CtrlMotRotgAgSigInfo.StkPosnMeasd >> 8);
	AHB_LOG_DATA[62] = (uint8_t) (Msp_CtrlBus.Msp_CtrlMotRotgAgSigInfo.StkPosnMeasd >> 16);
	AHB_LOG_DATA[63] = (uint8_t) (Msp_CtrlBus.Msp_CtrlMotRotgAgSigInfo.StkPosnMeasd >> 24);

	/***************************************************************************************************/
	/*                                           LOG_BYTE8                                             */
	/***************************************************************************************************/
	if(Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSet == 0)
	{
		AHB_LOG_DATA[64] = (uint8_t) (Ses_CtrlBus.Ses_CtrlMotDqIRefSesInfo.IdRef);
		AHB_LOG_DATA[65] = (uint8_t) (Ses_CtrlBus.Ses_CtrlMotDqIRefSesInfo.IdRef >> 8);
		AHB_LOG_DATA[66] = (uint8_t) (Ses_CtrlBus.Ses_CtrlMotDqIRefSesInfo.IqRef);
		AHB_LOG_DATA[67] = (uint8_t) (Ses_CtrlBus.Ses_CtrlMotDqIRefSesInfo.IqRef >> 8);
	}
	else
	{
		AHB_LOG_DATA[64] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo.IdRef);
		AHB_LOG_DATA[65] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo.IdRef >> 8);
		AHB_LOG_DATA[66] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo.IqRef);
		AHB_LOG_DATA[67] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo.IqRef >> 8);
	}
#if __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
	AHB_LOG_DATA[68] = (uint8_t) (Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhUMeasd);
	AHB_LOG_DATA[69] = (uint8_t) (Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhUMeasd>>8);
#else
	AHB_LOG_DATA[68] = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPistPRateInfo.PistPRate);
	AHB_LOG_DATA[69] = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPistPRateInfo.PistPRate>>8);
#endif
	AHB_LOG_DATA[70] = (uint8_t) (Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhVMeasd);
	AHB_LOG_DATA[71] = (uint8_t) (Mtr_Processing_SigMtrProcessOutInfo.MotCurrPhVMeasd >> 8);

	/***************************************************************************************************/
	/*                                           LOG_BYTE9                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[72] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIdMeasd);
	AHB_LOG_DATA[73] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIdMeasd>>8);
	AHB_LOG_DATA[74] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIqMeasd);
	AHB_LOG_DATA[75] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIqMeasd>>8);
	AHB_LOG_DATA[76] = (uint8_t) (Acmio_SenMotCurrInfo.MotCurrPhUMeasd);
	AHB_LOG_DATA[77] = (uint8_t) (Acmio_SenMotCurrInfo.MotCurrPhUMeasd>>8);
	AHB_LOG_DATA[78] = (uint8_t) (Acmio_SenMotCurrInfo.MotCurrPhVMeasd);
	AHB_LOG_DATA[79] = (uint8_t) (Acmio_SenMotCurrInfo.MotCurrPhVMeasd>>8);
}
#endif /*M_DETECTION_LOG_DATA*/

#if(M_LOGIC_LOG_DATA == M_SP_LOG_DATA)

extern uint8_t Idbu8PedalEolOfsCalcEnd;
extern uint8_t Idbu8PressEolOfsCalcEnd;
extern uint8_t Idbu8PedalEolOfsCalcOk;
extern uint8_t Idbu8PressEolOfsCalcOk;
extern DoorSwt_t Swt_SenDoorSwt;
extern BlsSwt_t Swt_SenBlsSwt;
static void LSIDB_vCallSignalProcess(void)
{
	uint8_t	i;

	/***************************************************************************************************/
	/*                                           LOG_BYTE0                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[0] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar);
	AHB_LOG_DATA[1] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar >> 8);
#if (__P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE)
	AHB_LOG_DATA[2] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPistPFildInfo.PistPFild_1_100Bar);
	AHB_LOG_DATA[3] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPistPFildInfo.PistPFild_1_100Bar>>8);
	AHB_LOG_DATA[4] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPistP5msRawInfo.PistPSig);
	AHB_LOG_DATA[5] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPistP5msRawInfo.PistPSig >>8);
#else
	AHB_LOG_DATA[2] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar);
	AHB_LOG_DATA[3] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar>>8);
	AHB_LOG_DATA[4] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlCircP5msRawInfo.PrimCircPSig);
	AHB_LOG_DATA[5] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlCircP5msRawInfo.PrimCircPSig >>8);
#endif
	AHB_LOG_DATA[6] = 0;

	i=0;
	if(Spc_5msCtrlBus.Spc_5msCtrlBlsFild                      	 						==1) i|=0x01;
	if(Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdtOffsCorrdStOkFlg				==1) i|=0x02;
	if(Spc_5msCtrlBus.Spc_5msCtrlPistPOffsCorrdInfo.PistPOffsCorrdStOkFlg           	==1) i|=0x04;
	if(NvMIf_LogicEepData.Eep.ReadInvldFlg          									==1) i|=0x08;
	if(PdtOffsDrvgCalc.lsahbu1AHBDrv60secOfsOK          								==1) i|=0x10;
	if(PdtOffsDrvgCalc.lsahbu1AHBDrvOfsOK   		       								==1) i|=0x20;
	if(PdtOffsDrvgCalc.lsahbu1SensorDrvOfsCheckCond        								==1) i|=0x40;
	if(PdtOffsDrvgCalc.lsahbu1SensorNormCond        	  								==1) i|=0x80;
	AHB_LOG_DATA[7] =(uint8_t) (i);

	/***************************************************************************************************/
	/*                                           LOG_BYTE1                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[8]  = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPdt5msRawInfo.PdtSig);
	AHB_LOG_DATA[9]  = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPdt5msRawInfo.PdtSig >> 8);
	AHB_LOG_DATA[10]  = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPdf5msRawInfo.PdfSig);
	AHB_LOG_DATA[11]  = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPdf5msRawInfo.PdfSig>>8);
	AHB_LOG_DATA[12]  = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPedlSimPRaw);
	AHB_LOG_DATA[13]  = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPedlSimPRaw>>8);
	AHB_LOG_DATA[14] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlWhlSpdInfo.FrWhlSpd/64);

	i=0;
	if(Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.DrvrIntendToRelsBrk	 				==1) i|=0x01;
	if(Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.PanicBrkStFlg         	 			==1) i|=0x02;
	if(Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.PedlReldStFlg1               		==1) i|=0x04;
	if(Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.PedlReldStFlg2               		==1) i|=0x08;
	if(Det_5msCtrlBus.Det_5msCtrlVehStopStInfo.VehStandStillStFlg                 		==1) i|=0x10;
	if(Det_1msCtrlBus.Det_1msCtrlBrkPedlStatus1msInfo.PanicBrkSt1msFlg                	==1) i|=0x20;
	if(PdtOffsDrvgCalc.lsahbu1AHBDrv10secOfsOK											==1) i|=0x40;
	if(PdtOffsEolEep.lsahbu8AHBEEPofsReadInvalid										==1) i|=0x80;
	AHB_LOG_DATA[15] = (uint8_t) (i);

	/***************************************************************************************************/
	/*                                           LOG_BYTE2                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[16]  = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlFildInfo.PdtFild);
	AHB_LOG_DATA[17]  = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlFildInfo.PdtFild>>8);

	AHB_LOG_DATA[18]  = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlFildInfo.PdfFild);
	AHB_LOG_DATA[19]  = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlFildInfo.PdfFild>>8);

	AHB_LOG_DATA[20]  = (uint8_t) (NvMIf_LogicEepData.Eep.PedlTrvlOffsEepInfo.PdtOffsEolReadVal);
	AHB_LOG_DATA[21]  = (uint8_t) (NvMIf_LogicEepData.Eep.PedlTrvlOffsEepInfo.PdtOffsEolReadVal>>8);

	AHB_LOG_DATA[22]  = (uint8_t)  (Bbc_CtrlBus.Bbc_CtrlTarPFromBrkPedl/100);
	i=0;
	if(Bbc_CtrlBus.Bbc_CtrlBaseBrkCtrlrActFlg                      	 					==1) i|=0x01;
	if(Bbc_CtrlBus.Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo.PedlSimrPGendOnFlg         	==1) i|=0x02;
	if(PedlSimrPOffsDrvgCalc.lsahbu1AHBDrv60secOfsOK									==1) i|=0x04;
	if(PedlSimrPOffsDrvgCalc.lsahbu1AHBDrvOfsOK											==1) i|=0x08;
	if(PedlSimrPOffsDrvgCalc.lsahbu1SensorDrvOfsCheckCond  								==1) i|=0x10;
	if(PedlSimrPOffsDrvgCalc.lsahbu1SensorNormCond										==1) i|=0x20;
	if(PedlSimrPOffsDrvgCalc.lsahbu1AHBDrv60secOfsReOK									==1) i|=0x40;
	if(PdtOffsDrvgCalc.lsahbu1AHBDrv60secOfsReOK										==1) i|=0x80;

	AHB_LOG_DATA[23] =(uint8_t) (i);

	/***************************************************************************************************/
	/*                                           LOG_BYTE3                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[24] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlFinalTarPFromPCtrl );
	AHB_LOG_DATA[25] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlFinalTarPFromPCtrl >> 8);

	AHB_LOG_DATA[26] = (uint8_t) (NvMIf_LogicEepData.Eep.PedlTrvlOffsEepInfo.PdfOffsEolReadVal );
	AHB_LOG_DATA[27] = (uint8_t) (NvMIf_LogicEepData.Eep.PedlTrvlOffsEepInfo.PdfOffsEolReadVal >> 8);

	AHB_LOG_DATA[28] = (uint8_t) (NvMIf_LogicEepData.Eep.PedlTrvlOffsEepInfo.PdtOffsDrvgReadVal);
	AHB_LOG_DATA[29] = (uint8_t) (NvMIf_LogicEepData.Eep.PedlTrvlOffsEepInfo.PdtOffsDrvgReadVal >> 8);
	AHB_LOG_DATA[30] = (uint8_t) PedlSimrPOffsFinal.lsahbs16AHBOfs_f;

	i=0;
	if(PdtOffsDrvgEep.lsahbu8DrvEEPofsReadInvalid							==1) i|=0x01;
	if(PedlSimrPOffsDrvgEep.lsahbu8DrvEEPofsReadInvalid						==1) i|=0x02;
#if (__P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE)
	if(PistPOffsDrvgEep.lsahbu8DrvEEPofsReadInvalid							==1) i|=0x04;
#else
	if(PrimPOffsDrvgEep.lsahbu8DrvEEPofsReadInvalid							==1) i|=0x04;
#endif
	if(Idbu8PedalEolOfsCalcEnd												==1) i|=0x08;
	if(Idbu8PressEolOfsCalcEnd												==1) i|=0x10;
	if(Idbu8PedalEolOfsCalcOk												==1) i|=0x20;
	if(Idbu8PressEolOfsCalcOk												==1) i|=0x40;
	if(PdfOffsEolEep.lsahbu8AHBEEPofsReadInvalid							==1) i|=0x80;
	AHB_LOG_DATA[31] =(uint8_t) (i);

	/***************************************************************************************************/
	/*                                           LOG_BYTE4                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[32] = (uint8_t) (NvMIf_LogicEepData.Eep.PedlTrvlOffsEepInfo.PdfOffsDrvgReadVal);
	AHB_LOG_DATA[33] = (uint8_t) (NvMIf_LogicEepData.Eep.PedlTrvlOffsEepInfo.PdfOffsDrvgReadVal>>8);
#if (__P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE)
	AHB_LOG_DATA[34] = (uint8_t) (NvMIf_LogicEepData.Eep.PistPOffsEepInfo.PistPOffsEolReadVal);
	AHB_LOG_DATA[35] = (uint8_t) (NvMIf_LogicEepData.Eep.PistPOffsEepInfo.PistPOffsEolReadVal >> 8);
	AHB_LOG_DATA[36] = (uint8_t) (NvMIf_LogicEepData.Eep.PistPOffsEepInfo.PistPOffsDrvgReadVal);
	AHB_LOG_DATA[37] = (uint8_t) (NvMIf_LogicEepData.Eep.PistPOffsEepInfo.PistPOffsDrvgReadVal>>8);
#else
	AHB_LOG_DATA[34] = (uint8_t) (NvMIf_LogicEepData.Eep.CircPOffsEepInfo.PrimCircPOffsEolReadVal);
	AHB_LOG_DATA[35] = (uint8_t) (NvMIf_LogicEepData.Eep.CircPOffsEepInfo.PrimCircPOffsEolReadVal >> 8);
	AHB_LOG_DATA[36] = (uint8_t) (NvMIf_LogicEepData.Eep.CircPOffsEepInfo.PrimCircPOffsDrvgReadVal);
	AHB_LOG_DATA[37] = (uint8_t) (NvMIf_LogicEepData.Eep.CircPOffsEepInfo.PrimCircPOffsDrvgReadVal>>8);
#endif
	AHB_LOG_DATA[38] = (uint8_t) (NvMIf_LogicEepData.Eep.CircPOffsEepInfo.SecdCircPOffsEolReadVal);
	AHB_LOG_DATA[39] = (uint8_t) (NvMIf_LogicEepData.Eep.CircPOffsEepInfo.SecdCircPOffsEolReadVal >> 8);

	/***************************************************************************************************/
	/*                                           LOG_BYTE5                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[40] = (uint8_t) (PdtOffsFinal.lsahbu1AHBFinalMonitor);
	AHB_LOG_DATA[41] = (uint8_t) (PdtOffsDrvgCalc.lsahbs16AHBDrvOfs_f);
	AHB_LOG_DATA[42] = (uint8_t) (PedlSimrPOffsFinal.lsahbu1AHBFinalMonitor);
	AHB_LOG_DATA[43] = (uint8_t) (PedlSimrPOffsDrvgCalc.lsahbs16AHBDrvOfs_f);
	AHB_LOG_DATA[44] = (uint8_t) (PdtOffsFinal.lsahbs16AHBOfs_f);
	AHB_LOG_DATA[45] = (uint8_t) (NvMIf_LogicEepData.Eep.PedlSimrPOffsEepInfo.PedlSimrPOffsEolReadVal);
	AHB_LOG_DATA[46] = (uint8_t) (NvMIf_LogicEepData.Eep.PedlSimrPOffsEepInfo.PedlSimrPOffsEolReadVal>>8);
	AHB_LOG_DATA[47] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlWhlSpdInfo.RrlWhlSpd/64);

	/***************************************************************************************************/
	/*                                           LOG_BYTE6                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[48] = (uint8_t) (NvMIf_LogicEepData.Eep.PedlSimrPOffsEepInfo.PedlSimrPOffsDrvgReadVal);
	AHB_LOG_DATA[49] = (uint8_t) (NvMIf_LogicEepData.Eep.PedlSimrPOffsEepInfo.PedlSimrPOffsDrvgReadVal>>8);

	i=0;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SimVlvReq		  	 		==1) i|=0x01;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReq      	 		==1) i|=0x02;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SecdCutVlvReq       	 	==1) i|=0x04;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlIvReq			         	 	==1) i|=0x08;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrIvReq         			 	==1) i|=0x10;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RlIvReq       				   	==1) i|=0x20;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RrIvReq     			     	==1) i|=0x40;
	if(Swt_SenDoorSwt														==1) i|=0x80;
	AHB_LOG_DATA[50] = (uint8_t) (i);

	AHB_LOG_DATA[51] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlWhlSpdInfo.RlWhlSpd/64);
	AHB_LOG_DATA[52] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlWhlSpdInfo.FlWhlSpd/64);
	AHB_LOG_DATA[53] = (uint8_t) (Proxy_RxBus.Proxy_RxCanRxAccelPedlInfo.AccelPedlVal);

	i=0;
	if(Spc_5msCtrlBus.Spc_5msCtrlBlsSwt															==1) i|=0x01;
	if(Spc_5msCtrlBus.Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo.PedlTrvlOffsEolCalcReqFlg	 	==1) i|=0x02;
	if(Spc_5msCtrlBus.Spc_5msCtrlIdbSnsrOffsEolCalcReqByDiagInfo.PSnsrOffsEolCalcReqFlg		 	==1) i|=0x04;
	if(Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.TcsActFlg	 	 										==1) i|=0x08;
	if(Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscActFlg		 										==1) i|=0x10;
	if(PdtOffsFinal.lsahbu1AHBDrvEepOfsDrvUse													==1) i|=0x20;
	if(PdtOffsDrvgEep.lsahbu1AHBeepWriteReq 													==1) i|=0x40;
	if(PdtOffsDrvgEep.lsahbu1AHBDrveepWriteEnd													==1) i|=0x80;
	AHB_LOG_DATA[54] =(uint8_t) (i);

	i=0;
	if (Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSet 	 							==1) i|=0x01;
	if (Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSetErr	 						==1) i|=0x02;
	if (lsesu16WallDetectedFlg														==1) i|=0x04;
	if (PedlSimrPOffsDrvgEep.lsahbu1AHBeepWriteReq 				 					==1) i|=0x08;
	if (PedlSimrPOffsDrvgEep.lsahbu1AHBDrveepWriteEnd			 					==1) i|=0x10;
	if (PedlSimrPOffsEolEep.lsahbu8AHBEEPofsReadInvalid			 					==1) i|=0x20;
#if (__P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE)
	if (PistPOffsEolEep.lsahbu8AHBEEPofsReadInvalid			 						==1) i|=0x40;
#else
	if (PrimPOffsEolEep.lsahbu8AHBEEPofsReadInvalid			 						==1) i|=0x40;
#endif
	if (Swt_SenBlsSwt												 				==1) i|=0x80;
	AHB_LOG_DATA[55] =(uint8_t) (i);

	/***************************************************************************************************/
	/*                                           LOG_BYTE7                                             */
	/***************************************************************************************************/
	if(Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSet == 0)
	{
		AHB_LOG_DATA[56] = (uint8_t) (lsess32TgtPosiOrgSet);
		AHB_LOG_DATA[57] = (uint8_t) (lsess32TgtPosiOrgSet >> 8);
		AHB_LOG_DATA[58] = (uint8_t) (lsess32TgtPosiOrgSet >> 16);
		AHB_LOG_DATA[59] = (uint8_t) (lsess32TgtPosiOrgSet >> 24);
	}
	else
	{
		AHB_LOG_DATA[56] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn);
		AHB_LOG_DATA[57] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn >> 8);
		AHB_LOG_DATA[58] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn >> 16);
		AHB_LOG_DATA[59] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn >> 24);
	}
	AHB_LOG_DATA[60] = (uint8_t) (Msp_1msCtrlBus.Msp_1msCtrlMotRotgAg1msSigInfo.StkPosnMeasd);
	AHB_LOG_DATA[61] = (uint8_t) (Msp_1msCtrlBus.Msp_1msCtrlMotRotgAg1msSigInfo.StkPosnMeasd >> 8);
	AHB_LOG_DATA[62] = (uint8_t) (Msp_1msCtrlBus.Msp_1msCtrlMotRotgAg1msSigInfo.StkPosnMeasd >> 16);
	AHB_LOG_DATA[63] = (uint8_t) (Msp_1msCtrlBus.Msp_1msCtrlMotRotgAg1msSigInfo.StkPosnMeasd >> 24);

	/***************************************************************************************************/
	/*                                           LOG_BYTE8                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[64] = (uint8_t) (Spc_1msCtrlBus.Spc_1msCtrlPedlTrvlFild1msInfo.PdtFild1ms);
	AHB_LOG_DATA[65] = (uint8_t) (Spc_1msCtrlBus.Spc_1msCtrlPedlTrvlFild1msInfo.PdtFild1ms >> 8);
#if (__P_SENSOR_MEASURE_TYPE == PISTON_PRESSURE)
	AHB_LOG_DATA[66] = (uint8_t) (Spc_1msCtrlBus.Spc_1msCtrlPistPFild1msInfo.PistPFild1ms);
	AHB_LOG_DATA[67] = (uint8_t) (Spc_1msCtrlBus.Spc_1msCtrlPistPFild1msInfo.PistPFild1ms >> 8);
#else
	AHB_LOG_DATA[66] = (uint8_t) (Spc_1msCtrlBus.Spc_1msCtrlCircPFild1msInfo.PrimCircPFild1ms);
	AHB_LOG_DATA[67] = (uint8_t) (Spc_1msCtrlBus.Spc_1msCtrlCircPFild1msInfo.PrimCircPFild1ms >> 8);
#endif
	AHB_LOG_DATA[68] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlWhlSpdFildInfo.WhlSpdFildFrntLe/8);
	AHB_LOG_DATA[69] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlWhlSpdFildInfo.WhlSpdFildFrntRi/8);
	AHB_LOG_DATA[70] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlWhlSpdFildInfo.WhlSpdFildReLe/8);
	AHB_LOG_DATA[71] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlWhlSpdFildInfo.WhlSpdFildReRi/8);

	/***************************************************************************************************/
	/*                                           LOG_BYTE9                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[72] = (uint8_t) (Spc_1msCtrlBus.Spc_1msCtrlCircPFild1msInfo.SecdCircPFild1ms);
	AHB_LOG_DATA[73] = (uint8_t) (Spc_1msCtrlBus.Spc_1msCtrlCircPFild1msInfo.SecdCircPFild1ms>>8);
	AHB_LOG_DATA[74] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIqMeasd);
	AHB_LOG_DATA[75] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIqMeasd>>8);
	AHB_LOG_DATA[76] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlCircP5msRawInfo.SecdCircPSig);
	AHB_LOG_DATA[77] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlCircP5msRawInfo.SecdCircPSig >> 8);
	AHB_LOG_DATA[78] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar);
	AHB_LOG_DATA[79] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar >> 8);
}
#endif /*M_SP_LOG_DATA*/

#if (M_LOGIC_LOG_DATA==M_IDB_LOG_DATA_ESC_CHECK)
extern sint16 lamtrs16WrpmMech;
extern MC_DqSyst lamtrDqImeas;
extern int16_t ldidbu8WheelVolumeCtrlMode;
extern U8_BIT_STRUCT_t laidbDelStrkActFlg;
extern U8_BIT_STRUCT_t laidbDelStrkActFlg2;
extern uint8_t laidbu1CutCloseDelayForQuickBrk;
extern uint16 lsesu16WallDetectedFlg;
extern uint16 lamtru16OrgSetSusFlg;
extern uint16_t lamtru16IdbSystemFailFlg;
extern uint8_t ldidbu8AllWheelControl;
extern uint8_t ldidbu8SelectWheelControl;

static void LSIDB_vCallIDBPCtrlLogData(void)
{
	uint8_t	i;
    int16_t s16TemporaryData;

		/* LOG_BYTE 0 */
        AHB_LOG_DATA[0]  = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn);             										/*ch 01*/
        AHB_LOG_DATA[1]  = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn >> 8);
        AHB_LOG_DATA[2]  = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn >> 16);           									/*ch 02*/
        AHB_LOG_DATA[3]  = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn >> 24);
        AHB_LOG_DATA[4]  = (uint8_t) (Msp_1msCtrlBus.Msp_CtrlMotRotgAgSigInfo.StkPosnMeasd);              											/*ch 03*/
        AHB_LOG_DATA[5]  = (uint8_t) (Msp_1msCtrlBus.Msp_CtrlMotRotgAgSigInfo.StkPosnMeasd >> 8);
        AHB_LOG_DATA[6]  = (uint8_t) (Msp_1msCtrlBus.Msp_CtrlMotRotgAgSigInfo.StkPosnMeasd >> 16);               									/*ch 04*/
        AHB_LOG_DATA[7]  = (uint8_t) (Msp_1msCtrlBus.Msp_CtrlMotRotgAgSigInfo.StkPosnMeasd >> 24);
        /* LOG_BYTE 1 */
        AHB_LOG_DATA[8]  = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPedlTrvlFinal);        										/*ch 05*/
        AHB_LOG_DATA[9]  = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPedlTrvlFinal >> 8);
        AHB_LOG_DATA[10] = (uint8_t) (Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalTarP);             									/*ch 06*/
        AHB_LOG_DATA[11] = (uint8_t) (Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalTarP >> 8);
        AHB_LOG_DATA[12] = (uint8_t) (Bbc_CtrlBus.Bbc_CtrlTarPFromBrkPedl);				    						/*ch 07*/
        AHB_LOG_DATA[13] = (uint8_t) (Bbc_CtrlBus.Bbc_CtrlTarPFromBrkPedl >> 8);
        AHB_LOG_DATA[14] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar);													/*ch 08*/
        AHB_LOG_DATA[15] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar>>8);
        /* LOG_BYTE 2 */
        AHB_LOG_DATA[16] = (uint8_t) (lmccs16WrpmMechRef);                								/*ch 09*/
        AHB_LOG_DATA[17] = (uint8_t) (lmccs16WrpmMechRef >> 8);   											/*ch 10*/
        AHB_LOG_DATA[18] = (uint8_t) (lamtrs16WrpmMech);          										/*ch 11*/
        AHB_LOG_DATA[19] = (uint8_t) (lamtrs16WrpmMech >> 8);      										 /*ch 12*/
        AHB_LOG_DATA[20] = (uint8_t) (laidbs16DeltaStrokeMapAvg);    										/*ch 13*/
        AHB_LOG_DATA[21] = (uint8_t) (laidbs16DeltaStrokeMapAvg>>8);											/*ch 14*/
        AHB_LOG_DATA[22] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPDelta/*laidbs16DeltaStrokeTargetWoLtd*/);      											/*ch 15*/
        AHB_LOG_DATA[23] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPDelta>>8);
        /* LOG_BYTE 3 */
        AHB_LOG_DATA[24] = (uint8_t) (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SimVlvReqData[0]/10);
        AHB_LOG_DATA[25] = (uint8_t) (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReqData[0]/10);
        AHB_LOG_DATA[26] = (uint8_t) (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PrimCircVlvReqData[0]/10);               /*ch 18*/
        AHB_LOG_DATA[27] = (uint8_t) (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SecdCircVlvReqData[0]/10);
        AHB_LOG_DATA[28] = (uint8_t) (/*Valve_GetCurrent(VALVE_PORT_SIM)/10 */0);			 /*ch 20*/
        AHB_LOG_DATA[29] = (uint8_t) (/*Valve_GetCurrent(VALVE_PORT_CUT1)/10*/0);        	 /*ch 21*/
        AHB_LOG_DATA[30] = (uint8_t) (/*Valve_GetCurrent(VALVE_PORT_CV1)/10 */0); 		 /*ch 22*/
        AHB_LOG_DATA[31] = (uint8_t) (/*Valve_GetCurrent(VALVE_PORT_CV2)/10 */0);/*LS_s8LimSignedIntToSignedChar(lamtrs16CalcIqRefPidState, 1);*/		    	 /*ch 23*/
        /* LOG_BYTE 4 */
        AHB_LOG_DATA[32] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIRefMccInfo.IdRef);      													 /*ch 24*/
        AHB_LOG_DATA[33] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIRefMccInfo.IdRef >> 8);
        AHB_LOG_DATA[34] = (uint8_t) (lamtrDqImeas.d);        												 /*ch 25*/
        AHB_LOG_DATA[35] = (uint8_t) (lamtrDqImeas.d>>8);
        AHB_LOG_DATA[36] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIRefMccInfo.IqRef);        												 /*ch 26*/
        AHB_LOG_DATA[37] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIRefMccInfo.IqRef >> 8);
        AHB_LOG_DATA[38] = (uint8_t) (lamtrDqImeas.q);          											 /*ch 27*/
        AHB_LOG_DATA[39] = (uint8_t) (lamtrDqImeas.q>>8);
		/* LOG_BYTE 5 */
        AHB_LOG_DATA[40] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlPCtrlSt);		                       							 /*ch 28*/
        AHB_LOG_DATA[41] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlPCtrlBoostMod);
        AHB_LOG_DATA[42] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlPCtrlFadeoutSt);                										 /*ch 29*/
		AHB_LOG_DATA[43] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotCtrlMode);
        AHB_LOG_DATA[44] = (uint8_t) (0);                    									 /*ch 30*/
        AHB_LOG_DATA[45] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlSeldMapOfBrkPedlToBrkP);													 /*ch 31*/
        AHB_LOG_DATA[46] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar);		  											     /*ch 32*/
        AHB_LOG_DATA[47] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar>> 8);	                                             /*ch 33*/
        /* LOG_BYTE 6 */
        AHB_LOG_DATA[48] = (uint8_t) (laidbs16DeltaStrokeMap);          												 /*ch 34*/
        AHB_LOG_DATA[49] = (uint8_t) (laidbs16DeltaStrokeMap>>8);
        AHB_LOG_DATA[50] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlTarDeltaStk);           											 /*ch 35*/
        AHB_LOG_DATA[51] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlTarDeltaStk>>8);
        AHB_LOG_DATA[52] = (uint8_t) (/*laidbs16DeltaStrokeMapAvg*/laidbs16DeltaStrokePerror);													 /*ch 36*/
        AHB_LOG_DATA[53] = (uint8_t) (/*laidbs16DeltaStrokeMapAvg*/laidbs16DeltaStrokePerror>> 8);
        AHB_LOG_DATA[54] = (uint8_t) (laidbs16DeltaStrokePerrorFinal);                                   					 /*ch 37*/
        AHB_LOG_DATA[55] = (uint8_t) (laidbs16DeltaStrokePerrorFinal>>8);
		/* LOG_BYTE 7 */
        AHB_LOG_DATA[56] = (uint8_t) LS_u8LimSignedIntToUnsignedChar(laidbs16InitInEffStrkComp,1);                                                   /*ch 38*/
        AHB_LOG_DATA[57] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPDelta);                                              /*ch 39*/
        AHB_LOG_DATA[58] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlFinalTarPInfo.FinalTarPDelta>>8);                                                /*ch 40*/
        AHB_LOG_DATA[59] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar/5);                                                /*ch 41*/
        AHB_LOG_DATA[60] = (uint8_t) (Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalTarPOfWhlFrntLe/5);
        AHB_LOG_DATA[61] = (uint8_t) (Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalTarPOfWhlFrntRi/5);  /*ch 43*/
        AHB_LOG_DATA[62] = (uint8_t) (Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalTarPOfWhlReLe/5);  /*ch 44*/
        AHB_LOG_DATA[63] = (uint8_t) (Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalTarPOfWhlReRi/5);  /*ch 45*/

		/* LOG_BYTE 8 */
        AHB_LOG_DATA[64] = (uint8_t) Vat_CtrlBus.Vat_CtrlIdbCircVlv1CtrlStInfo.VlvOnTi; //LS_u8LimSignedIntToUnsignedChar(ldahbs16VehicleSpeed,8);                /*ch 46*/
        AHB_LOG_DATA[65] = (uint8_t) Det_5msCtrlBus.Det_5msCtrlEstimdWhlPInfo.FrntLeEstimdWhlP;                /*ch 47*/
        AHB_LOG_DATA[66] = (uint8_t) Det_5msCtrlBus.Det_5msCtrlEstimdWhlPInfo.FrntRiEstimdWhlP;                /*ch 48*/
        AHB_LOG_DATA[67] = (uint8_t) Det_5msCtrlBus.Det_5msCtrlEstimdWhlPInfo.ReLeEstimdWhlP;                /*ch 49*/
        AHB_LOG_DATA[68] = (uint8_t) Det_5msCtrlBus.Det_5msCtrlEstimdWhlPInfo.ReRiEstimdWhlP;                /*ch 50*/
        AHB_LOG_DATA[69] = (uint8_t) Vat_CtrlBus.Vat_CtrlIdbCircVlv2CtrlStInfo.VlvOnTi; //lis8StrkRcvrCtrlState;            /*ch 51*/

        i=0;
        if (Spc_5msCtrlBus.Spc_5msCtrlBlsFild                      				 		==1) i|=0x01;
        if (Bbc_CtrlBus.Bbc_CtrlBaseBrkCtrlrActFlg		                    	 		==1) i|=0x02;
        if ((Arb_CtrlBus.Arb_CtrlFinalTarPInfo.PrimCircPBoostReqFlg)&0x01	 		 	==1) i|=0x04;
        if (u8IdbCtrlInhibitFlg								            	 			==1) i|=0x08;
        if ((Arb_CtrlBus.Arb_CtrlFinalTarPInfo.FinalTarPResetFlg) & 0x01   				==1) i|=0x10;
        if ((ldidbu8WheelVolumeCtrlMode) & 0x01  					 					==1) i|=0x20;
        if (0          	 																==1) i|=0x40;
        if ((Arb_CtrlBus.Arb_CtrlFinalTarPInfo.SecdyCircPBoostReqFlg)&0x01			 	==1) i|=0x80;
        AHB_LOG_DATA[70] =(uint8_t) (i);                                /*ch 52*/

        i=0;
        if (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SimVlvReq         			 ==1) i|=0x01;
        if (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReq          	 ==1) i|=0x02;
        if (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SecdCutVlvReq         		 ==1) i|=0x04;
        if (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PrimCircVlvReq         	 ==1) i|=0x08;
        if (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SecdCircVlvReq          	 ==1) i|=0x10;
	#if (__IDB_TEST == ENABLE)
		if (lu8IdbTestReadyFlg					 ==1) i|=0x20;
	#else
		if ((Arb_CtrlBus.Arb_CtrlFinalTarPInfo.WhlVolumeChgFlg&0x01)	 ==1) i|=0x20;
	#endif
        if (Rbc_CtrlBus.Rbc_CtrlRgnBrkCtrlrActStFlg			                	 ==1) i|=0x40;
        if (0        											 ==1) i|=0x80;
        AHB_LOG_DATA[71] =(uint8_t) (i);                                /*ch 53*/

		/* LOG_BYTE 9 */
        i=0;
        if ((0)&0x01			==1) i|=0x01;
        if (Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.DrvrIntendToRelsBrk	 ==1) i|=0x02;
        if (laidbDelStrkActFlg.bit2/*laidbu1NegativeCompDisableFlg*/		 ==1) i|=0x04;
        if (0																 ==1) i|=0x08;
        if (0																 ==1) i|=0x10;
        if (laidbu1CutCloseDelayForQuickBrk									 ==1) i|=0x20;
        if (Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.PanicBrkStFlg      	 ==1) i|=0x40;
        if (0    														 	 ==1) i|=0x80;
        AHB_LOG_DATA[72] =(uint8_t) (i);                                /*ch 54*/

        i=0;
        if ((Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSet)&0x01			 ==1) i|=0x01;
        if ((lsesu16WallDetectedFlg)&0x01									 ==1) i|=0x02;
        if ((lamtru16OrgSetSusFlg)&0x01										 ==1) i|=0x04;
        if ((lamtru16IdbSystemFailFlg)&0x01									 ==1) i|=0x08;
        if (laidbDelStrkActFlg.bit6/*laidbu1DelStrkPErrorCompFlg1*/ 		 ==1) i|=0x10;
        if (laidbDelStrkActFlg.bit4/*laidbu1DelStrkPErrorCompFlg2*/  		 ==1) i|=0x20;
        if (laidbDelStrkActFlg.bit3/*laidbu1DelStrkPErrorCompLimited*/		 ==1) i|=0x40;
        if (0																 ==1) i|=0x80;
        AHB_LOG_DATA[73] =(uint8_t) (i);                                /*ch 55*/

        i=0;
        if (0		 ==1) i|=0x01;
        if (Pct_5msCtrlBus.Pct_5msCtrlIdbVlvActInfo.VlvFadeoutEndOK			 ==1) i|=0x02;
        if (Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.TcsActFlg 						 	 ==1) i|=0x04;
        if (Abc_CtrlBus.Abc_CtrlEbdCtrlInfo.EbdActFlg              			 ==1) i|=0x08;
        if (Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsActFlg                  		 ==1) i|=0x10;
        if (Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscActFlg                  		 ==1) i|=0x20;
        if (0       																		 ==1) i|=0x40;
        if (0         																		 ==1) i|=0x80;
        AHB_LOG_DATA[74] =(uint8_t) (i);                                /*ch 56*/

        i=0;
        if(Bbc_CtrlBus.Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo.PedlSimrPGendOnFlg      ==1) i|=0x01;
        if(0        																 ==1) i|=0x02;
        if(0        ==1) i|=0x04;
        if(laidbDelStrkActFlg2.bit1/*laidbu1FadeoutState1EndOK*/		   			 ==1) i|=0x08;
        if(laidbDelStrkActFlg2.bit2/*laidbu1MotorFadeoutEndOK*/						 ==1) i|=0x10;
        if(/*temp*/0																 ==1) i|=0x20;
        if(/*temp*/0  																 ==1) i|=0x40;
        if(Det_5msCtrlBus.Det_5msCtrlVehStopStInfo.VehStandStillStFlg          		 ==1) i|=0x80;
        AHB_LOG_DATA[75] =(uint8_t) (i);                                /*ch 57*/

        i=0;
        if(0          ==1) i|=0x01;
        if(0              ==1) i|=0x02;
        if(0               ==1) i|=0x04;
        if(0               ==1) i|=0x08;
        if(0              ==1) i|=0x10;
        if(0              ==1) i|=0x20;
        if(ldidbu8AllWheelControl & 0x01         ==1) i|=0x40;
        if(ldidbu8SelectWheelControl & 0x01      ==1) i|=0x80;
        AHB_LOG_DATA[76] =(uint8_t) (i);                                /*ch 58*/

        i=0;
//        if(/*temp*/FL.HV_VL  			 ==1) i|=0x01;
//        if(/*temp*/FR.HV_VL  			 ==1) i|=0x02;
//        if(/*temp*/RL.HV_VL  			 ==1) i|=0x04;
//        if(/*temp*/RR.HV_VL  			 ==1) i|=0x08;
//        if(/*temp*/FL.AV_VL  			 ==1) i|=0x10;
//        if(/*temp*/FR.AV_VL  			 ==1) i|=0x20;
//        if(/*temp*/RL.AV_VL  			 ==1) i|=0x40;
//        if(/*temp*/RR.AV_VL  			 ==1) i|=0x80;
        AHB_LOG_DATA[77] =(uint8_t) (i);                                /*ch 59*/


        AHB_LOG_DATA[78] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlVdcLinkFild);            /*ch 60*/
        AHB_LOG_DATA[79] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlVdcLinkFild>>8);
}
#endif /* M_IDB_LOG_DATA_ESC_CHECK) */

#if (M_LOGIC_LOG_DATA==M_IDB_LOG_DATA_MULTIPLEX)

static void LSIDB_vCallIDBMultiplexLogData(void)
{
	uint8_t	i;

	/***************************************************************************************************/
	/*                                           LOG_BYTE0                                             */
	/***************************************************************************************************/
    if(Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSet == 0)
    {
       AHB_LOG_DATA[0] = (uint8_t) (lsess32TgtPosiOrgSet);
       AHB_LOG_DATA[1] = (uint8_t) (lsess32TgtPosiOrgSet >> 8);
       AHB_LOG_DATA[2] = (uint8_t) (lsess32TgtPosiOrgSet >> 16);
       AHB_LOG_DATA[3] = (uint8_t) (lsess32TgtPosiOrgSet >> 24);
    }
    else
    {
       AHB_LOG_DATA[0] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn);
       AHB_LOG_DATA[1] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn >> 8);
       AHB_LOG_DATA[2] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn >> 16);
       AHB_LOG_DATA[3] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn >> 24);
    }

	AHB_LOG_DATA[4] = (uint8_t) (Msp_1msCtrlBus.Msp_1msCtrlMotRotgAg1msSigInfo.StkPosnMeasd);
	AHB_LOG_DATA[5] = (uint8_t) (Msp_1msCtrlBus.Msp_1msCtrlMotRotgAg1msSigInfo.StkPosnMeasd >> 8);
	AHB_LOG_DATA[6] = (uint8_t) (Msp_1msCtrlBus.Msp_1msCtrlMotRotgAg1msSigInfo.StkPosnMeasd >> 16);
	AHB_LOG_DATA[7] = (uint8_t) (Msp_1msCtrlBus.Msp_1msCtrlMotRotgAg1msSigInfo.StkPosnMeasd >> 24);


	/***************************************************************************************************/
	/*                                           LOG_BYTE1                                             */
	/***************************************************************************************************/
    AHB_LOG_DATA[8]  = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPedlTrvlFinal);        										/*ch 03*/
    AHB_LOG_DATA[9]  = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPedlTrvlFinal >> 8);
    AHB_LOG_DATA[10] = (uint8_t) (laidbs16MuxTarPress);             									/*ch 04*/
    AHB_LOG_DATA[11] = (uint8_t) (laidbs16MuxTarPress >> 8);
    AHB_LOG_DATA[12] = (uint8_t) (lapcts16DeltaStrokeMap );	/* Spc_5msCtrlBus.Spc_5msCtrlCircPOffsCorrdInfo.PrimCircPOffsCorrd ch 05*/
    AHB_LOG_DATA[13] = (uint8_t) (lapcts16DeltaStrokeMap  >> 8); /*Spc_5msCtrlBus.Spc_5msCtrlCircPOffsCorrdInfo.PrimCircPOffsCorrd*/
#if __P_SENSOR_MEASURE_TYPE== EACH_CIRCUIT_PRESSURE
	AHB_LOG_DATA[14] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar);
	AHB_LOG_DATA[15] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar>>8);
#else
    AHB_LOG_DATA[14] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlPistPFildInfo.PistPFild_1_100Bar);													/*ch 06*/
    AHB_LOG_DATA[15] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlPistPFildInfo.PistPFild_1_100Bar >> 8);
#endif
	/***************************************************************************************************/
	/*                                           LOG_BYTE2                                             */
	/***************************************************************************************************/
    AHB_LOG_DATA[16] = (uint8_t) (lmccs16WrpmMechRef);
    AHB_LOG_DATA[17] = (uint8_t) (lmccs16WrpmMechRef >> 8);
    AHB_LOG_DATA[18] = (uint8_t) (lamtrs16WrpmMech);          											/*ch 08*/
    AHB_LOG_DATA[19] = (uint8_t) (lamtrs16WrpmMech >> 8);
    AHB_LOG_DATA[20] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlFinalTarPFromPCtrl/* Rbc_CtrlBus.Rbc_CtrlBrkPRednForBaseBrkCtrlr lamtrs16IqRefBase*/);           										/*ch 09*/
    AHB_LOG_DATA[21] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlFinalTarPFromPCtrl/* Rbc_CtrlBus.Rbc_CtrlBrkPRednForBaseBrkCtrlrlamtrs16IqRefBase*/ >> 8);
    AHB_LOG_DATA[22] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlTarDeltaStk);           								/*ch 10*/
    AHB_LOG_DATA[23] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlTarDeltaStk >> 8);
	/***************************************************************************************************/
	/*                                           LOG_BYTE3                                             */
	/***************************************************************************************************/

    /*
//        AHB_LOG_DATA[24] = (uint8_t) (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SimVlvReqData[0]/10);
//        AHB_LOG_DATA[25] = (uint8_t) (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReqData[0]/10);
        AHB_LOG_DATA[24] = (uint8_t) (Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RlIvReqData[0]/10);
        AHB_LOG_DATA[25] = (uint8_t) (Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RrIvReqData[0]/10);
        AHB_LOG_DATA[26] = (uint8_t) (Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrIvReqData[0]/10);
        AHB_LOG_DATA[27] = (uint8_t) (Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlIvReqData[0]/10);
    */

/*
    AHB_LOG_DATA[24] = (uint8_t) (lapctst_MuxInVActFL.u16ValveActRatio[0]/10);
    AHB_LOG_DATA[25] = (uint8_t) (lapctst_MuxInVActFR.u16ValveActRatio[0]/10);
    AHB_LOG_DATA[26] = (uint8_t) (lapctst_MuxInVActRL.u16ValveActRatio[0]/10);
    AHB_LOG_DATA[27] = (uint8_t) (lapctst_MuxInVActRR.u16ValveActRatio[0]/10);
*/
    AHB_LOG_DATA[24] = (uint8_t) (MuxCmdFrntLe.s16WhlTargetPRate/10);
    AHB_LOG_DATA[25] = (uint8_t) (MuxCmdFrntRi.s16WhlTargetPRate/10);
    AHB_LOG_DATA[26] = (uint8_t) (MuxCmdFrntLe.u8ArbCtrlMod);
    AHB_LOG_DATA[27] = (uint8_t) (MuxCmdReLe.u8ArbCtrlMod); /*MuxCmdFrntLe.u8ArbCtrlMod

    AHB_LOG_DATA[28] = (uint8_t) (Vlv_ActrBus.Vlv_ActrVlvMonInfo.SimVlvMon/10);
    AHB_LOG_DATA[29] = (uint8_t) (Vlv_ActrBus.Vlv_ActrVlvMonInfo.PrimCutVlvMon/10);
    AHB_LOG_DATA[30] = (uint8_t) (Vlv_ActrBus.Vlv_ActrVlvMonInfo.VlvInFRMon/10);
    AHB_LOG_DATA[31] = (uint8_t) (Vlv_ActrBus.Vlv_ActrVlvMonInfo.VlvInFLMon/10);
	/***************************************************************************************************/
	/*                                           LOG_BYTE4                                             */
	/***************************************************************************************************/
	if(Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSet == 0)
	{
		AHB_LOG_DATA[32] = (uint8_t) (Ses_CtrlBus.Ses_CtrlMotDqIRefSesInfo.IdRef);
		AHB_LOG_DATA[33] = (uint8_t) (Ses_CtrlBus.Ses_CtrlMotDqIRefSesInfo.IdRef >> 8);
	}
	else
	{
		AHB_LOG_DATA[32] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo.IdRef);
		AHB_LOG_DATA[33] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo.IdRef >> 8);
	}
    AHB_LOG_DATA[34] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIdMeasd);        												/*ch 20*/
    AHB_LOG_DATA[35] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIdMeasd>>8);

	if(Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSet == 0)
	{
		AHB_LOG_DATA[36] = (uint8_t) (Ses_CtrlBus.Ses_CtrlMotDqIRefSesInfo.IqRef);
		AHB_LOG_DATA[37] = (uint8_t) (Ses_CtrlBus.Ses_CtrlMotDqIRefSesInfo.IqRef >> 8);
	}
	else
	{
		AHB_LOG_DATA[36] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo.IqRef);
		AHB_LOG_DATA[37] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo.IqRef >> 8);
	}
    AHB_LOG_DATA[38] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIqMeasd);          			   								/*ch 22*/
    AHB_LOG_DATA[39] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIqMeasd>>8);

	/***************************************************************************************************/
	/*                                           LOG_BYTE5                                             */
	/***************************************************************************************************/
    AHB_LOG_DATA[40] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlPCtrlSt);		                       		/*ch 23*/
    AHB_LOG_DATA[41] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlPCtrlBoostMod);										/*ch 24*/
    AHB_LOG_DATA[42] = (uint8_t) ((las16MuxCtrlState*10)   +las16MuxCtrlMode);       /*ch 25*/
	AHB_LOG_DATA[43] = (uint8_t) (((Mcc_1msCtrlBus.Mcc_1msCtrlMotCtrlMode*100)+0/*lamtrs8MtrPosiDirecMode*/),1);/*ch 26*/
	AHB_LOG_DATA[44] = (uint8_t) (PressCTRLLocalBus.s16ReqdPCtrlBoostMod/*PressCTRLLocalBus.u16MuxWheelVolumeState*/);   						/*ch 27*/
	AHB_LOG_DATA[45] = (uint8_t) (PressCTRLLocalBus.u8WhlCtrlIndex); /*u8DebuggerInfo02*/ /*reserved01 */ /*ch 28*/
    AHB_LOG_DATA[46] = (uint8_t) (Bbc_CtrlBus.Bbc_CtrlTarPFromBrkPedl/100); 							/*ch 29*/
    AHB_LOG_DATA[47] = (uint8_t) (Acmio_SenVdcLink/100);									/*ch 30*/
	/***************************************************************************************************/
	/*                                           LOG_BYTE6                                             */
	/***************************************************************************************************/
    AHB_LOG_DATA[48] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPedlSimrPOffsCorrdInfo.PedlSimrPOffsCorrd);/*LS_u8LimSignedIntToUnsignedChar(MuxCmdFrntLe.EstWhlP   , 10)*/;	/*ch 31 -> ABS log data*/
    AHB_LOG_DATA[49] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPedlSimrPOffsCorrdInfo.PedlSimrPOffsCorrd>>8);/*LS_u8LimSignedIntToUnsignedChar(MuxCmdFrntRi.EstWhlP   , 10)*/;	/*ch 32 -> ABS log data*/
    AHB_LOG_DATA[50] = (uint8_t) (laidbu16CmdExecTimer)/*LS_u8LimSignedIntToUnsignedChar(MuxCmdReLe.EstWhlP     , 10)*/;	/*ch 33 -> ABS log data*/
    AHB_LOG_DATA[51] = (uint8_t) (MuxCurCmd.u16CmdExecTime)/*LS_u8LimSignedIntToUnsignedChar(MuxCmdReRi.EstWhlP     , 10)*/;	/*ch 34 -> ABS log data*/
    AHB_LOG_DATA[52] = (uint8_t) (MuxCmdFrntLe.s16WhlTargetP/100);			/*ch 35*/
    AHB_LOG_DATA[53] = (uint8_t) (MuxCmdFrntRi.s16WhlTargetP/100);			/*ch 36*/
    AHB_LOG_DATA[54] = (uint8_t) (MuxCmdReLe.s16WhlTargetP/100);			/*ch 37*/
    AHB_LOG_DATA[55] = (uint8_t) (MuxCmdReRi.s16WhlTargetP/100);			/*ch 38*/
	/***************************************************************************************************/
	/*                                           LOG_BYTE7                                             */
	/***************************************************************************************************/
    AHB_LOG_DATA[56] = (uint8_t) (laMux_FLCMD.u8MuxWhlCtrlMode*10+laMux_FRCMD.u8MuxWhlCtrlMode);	/*(lsahbu8PedalSigTag);*/										/*ch 39*/
    AHB_LOG_DATA[57] = (uint8_t) (laMux_RLCMD.u8MuxWhlCtrlMode*10+laMux_RRCMD.u8MuxWhlCtrlMode);	/*(liu16wSystemLoopCounter);*/									/*ch 40*/
    AHB_LOG_DATA[58] = (uint8_t) (PressCTRLLocalBus.s16MuxPwrPistonTarDP/*Det_5msCtrlBus.Det_5msCtrlCircPOffsCorrdInfo.SecdCircPOffsCorrd*//*Spc_5msCtrlBus.Spc_5msCtrlCircPOffsCorrdInfo.SecdCircPOffsCorrd*/);//LS_u8LimSignedIntToUnsignedChar(laidbu16CmdExecTimer,1);/*(lis16cEMSAccPedDep);*/													/*ch 41*/
    AHB_LOG_DATA[59] = (uint8_t) (PressCTRLLocalBus.s16MuxPwrPistonTarDP/*Det_5msCtrlBus.Det_5msCtrlCircPOffsCorrdInfo.SecdCircPOffsCorrd*//*Spc_5msCtrlBus.Spc_5msCtrlCircPOffsCorrdInfo.SecdCircPOffsCorrd*/>>8);//LS_u8LimSignedIntToUnsignedChar(MuxCurCmd.s16WhlTargetDeltaP, 10);	/*(LS_u8LimSignedIntToUnsignedChar(lsahbs16PSpresFilt,10));*/	/*ch 42*/


#if __P_SENSOR_MEASURE_TYPE== EACH_CIRCUIT_PRESSURE
	AHB_LOG_DATA[60] = (uint8_t) (PressCTRLLocalBus.s16MuxPwrPistonTarPRate/*Det_5msCtrlBus.Det_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar*/);
	AHB_LOG_DATA[61] = (uint8_t) (PressCTRLLocalBus.s16MuxPwrPistonTarPRate/*Det_5msCtrlBus.Det_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar*/>>8);
#else
	AHB_LOG_DATA[60] = (uint8_t) (lacmctls16Idc);
	AHB_LOG_DATA[61] = (uint8_t) (lacmctls16Idc>>8);
#endif

    i=0;
     if (MuxStReLe.u1Priority        ==1) i|=0x01;
     if (MuxCmdReLe.u1CmdHalt  			        ==1) i|=0x02;
     if (MuxStReLe.u1BuffFIFOAdded		        ==1) i|=0x04;
     if (MuxStReLe.u1BuffRemoved  		        ==1) i|=0x08;
     if (MuxStReLe.u1BuffInserted 		        ==1) i|=0x10;
     if (PressCTRLLocalBus.u8MuxCircCtrlReqPri	==1) i|=0x20;
     if (PressCTRLLocalBus.u8MuxCircCtrlReqSec	==1) i|=0x40;
     if (lapctst_MuxInVCtrlVarFR.lau1ValveAct			==1) i|=0x80;
     AHB_LOG_DATA[62] =(uint8_t) (i);

    i=0;
     if (MuxStReRi.u1Priority        ==1) i|=0x01;
     if (MuxCmdReRi.u1CmdHalt  			        ==1) i|=0x02;
     if (MuxStReRi.u1BuffFIFOAdded		        ==1) i|=0x04;
     if (MuxStReRi.u1BuffRemoved  		        ==1) i|=0x08;
     if (MuxStReRi.u1BuffInserted 		        ==1) i|=0x10;
     if (lapctst_MuxInVCtrlVarFL.lau1ValveAct	        ==1) i|=0x20;
     if (lapctst_MuxInVCtrlVarRL.lau1ValveAct	        ==1) i|=0x40;
     if (lapctst_MuxInVCtrlVarFR.lau1ValveAct	        ==1) i|=0x80;
     AHB_LOG_DATA[63] =(uint8_t) (i);

	/***************************************************************************************************/
	/*                                           LOG_BYTE8                                             */
	/***************************************************************************************************/
    AHB_LOG_DATA[64] = (uint8_t) (MuxFIFO.u32head);/*LS_u8LimSignedIntToUnsignedChar(ldahbs16VehicleSpeed,8)*/	/*ch 45*/
    AHB_LOG_DATA[65] = (uint8_t) (MuxCurCmd.u8id);	             /*ch 46*/
    AHB_LOG_DATA[66] = (uint8_t) (MuxCurCmd.u8State);	         /*ch 47*/
    AHB_LOG_DATA[67] = (uint8_t) (MuxCurCmd.u16CmdExecTimeMs);   /*ch 48*/
    AHB_LOG_DATA[68] = (uint8_t) (MuxFIFO.u32tail);	/*LS_u8LimSignedIntToUnsignedChar(lsahbs16WheelSpeedRR,8)*/	/*ch 49*/
    AHB_LOG_DATA[69] = (uint8_t) (Abc_CtrlBus.Abc_CtrlStkRecvryActnIfInfo.StkRecvryCtrlState);									/*ch 50*/

    i=0;
    if (Spc_5msCtrlBus.Spc_5msCtrlBlsFild								==1) i|=0x01;
    if (Bbc_CtrlBus.Bbc_CtrlBaseBrkCtrlrActFlg								==1) i|=0x02;
    if (Pct_5msCtrlBus.Pct_5msCtrlPCtrlAct) i|=0x04;
    if (u8IdbCtrlInhibitFlg 						==1) i|=0x08;
    if (0) i|=0x10; /*temp lcahbu1MotorDriveInhibitFlg*/
    if (Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.PrimBalVlvReq				==1) i|=0x20; /*temp lcahbu1InhibitDueToMotorFlg*/
    if (/*lcahbu1PedalInvalidFlg*/					0) i|=0x40;
    if (Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.SecdBalVlvReq					==1) i|=0x80;
    AHB_LOG_DATA[70] =(uint8_t) (i);                           /*ch 51*/

    i=0;
    if (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SimVlvReq				==1) i|=0x01;
    if (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReq			==1) i|=0x02;
    if (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SecdCutVlvReq  				==1) i|=0x04;
    if (Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlIvReq					==1) i|=0x08; /* laAHB_RLV1.lau1ValveAct: primary   */
    if (Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrIvReq				    ==1) i|=0x10; /* laAHB_RLV2.lau1ValveAct: secondary */
#if (__IDB_TEST == ENABLE)
	if (lu8IdbTestReadyFlg						==1) i|=0x20;
#else
	if (0					 					==1) i|=0x20;
#endif
    if (Rbc_CtrlBus.Rbc_CtrlRgnBrkCtrlrActStFlg         		==1) i|=0x40;
    if (/*liu1EscRearSmoothPresCtl*/        		0) i|=0x80;
    AHB_LOG_DATA[71] =(uint8_t) (i);                           /*ch 52*/

	/***************************************************************************************************/
	/*                                           LOG_BYTE9                                             */
	/***************************************************************************************************/
    i=0;
    if (MuxCmdFrntLe.u1CtrlReq					==1) i|=0x01;
    if (Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.DrvrIntendToRelsBrk		==1) i|=0x02;
    if (MuxCmdFrntRi.u1CtrlReq					==1) i|=0x04;
    if (MuxCmdReLe.u1CtrlReq					==1) i|=0x08;
    if (MuxCmdReRi.u1CtrlReq					==1) i|=0x10;
    /*if (lapctu1CutCloseDelayForQuickBrk			==1) i|=0x20;*/
    if (/*lcahbu1InitFastApplyFlg*/				0) i|=0x40;
    if (/*PressCTRLLocalBus.Pct_5msCtrlInitQuickBrkDctFlg*/	0) i|=0x80;
    AHB_LOG_DATA[72] =(uint8_t) (i);                           /*ch 53*/

    i=0; /*8~15*/
    if (Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSet						==1) i|=0x01;
    if (lsesu16WallDetectedFlg					==1) i|=0x02;
    if (/*liu1AbsDef*/							0) i|=0x04;
    if (Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSetErr					==1) i|=0x08;
    if (MuxStFrntRi.u1Priority/*laidbu1DelStrkPrateErrorCompFlg1*/		==1) i|=0x10;
    if (MuxCmdFrntRi.u1CmdHalt/*laidbu1DelStrkPrateErrorCompFlg2*/		==1) i|=0x20;
    if (MuxStFrntRi.u1BuffFIFOAdded/*laidbu1DelStrkPErrorCompLimited*/			==1) i|=0x40;
    if (MuxStFrntRi.u1BuffRemoved/*laidbu16PressureCtrlMode & 0x01	*/		==1) i|=0x80;
    AHB_LOG_DATA[73] =(uint8_t) (i);                          /*ch 54*/\

    i=0; /*16~23*/
    if (PressCTRLLocalBus.u8MultiplexCtrlActive					==1) i|=0x01;
    if (MuxStFrntRi.u1BuffInserted  						==1) i|=0x02;
    if (Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.TcsActFlg								==1) i|=0x04;
    if (Abc_CtrlBus.Abc_CtrlEbdCtrlInfo.EbdActFlg								==1) i|=0x08;
    if (lapctst_MuxCtrlFlg.u1InAbsCtrlActive  ==1) i|=0x10;
    if (Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscActFlg								==1) i|=0x20;
    if (/*lcahbu1SpeedInvalidFlg		*/			0) i|=0x40;
    if (/*lcahbu1EngineInfoInvalidFlg	*/			0) i|=0x80;
    AHB_LOG_DATA[74] =(uint8_t) (i);                          /*ch 55*/

    i=0;/*24~31*/
    if(MuxStFrntLe.u1Priority/*lcahbu1PedalSimPressGenOn*/				==1) i|=0x01;
    if(/*LAIDB_u8MuxFIFO_Empty(&MuxFIFO)*/		0) i|=0x02; /* lcahbu1PedalSimPForcedRelease */
    if(/*LAIDB_u8MuxFIFO_Full(&MuxFIFO)	*/		0) i|=0x04; /* lcahbu1PedalSimPressGenOnForFM*/
    if(MuxCmdFrntLe.u1CmdHalt/*lamtru16FadeOut2EndFlg*/					==1) i|=0x08;
    if(MuxStFrntLe.u1BuffFIFOAdded/*lamtru16FadeOut3EndFlg*/					==1) i|=0x10;
    if(MuxStFrntLe.u1BuffRemoved/*temp*//*lcahbu1AssistPressLimitFlg*/		==1) i|=0x20;
    if(MuxStFrntLe.u1BuffInserted/*temp*//*lcahbu1ReduceCurrCompFlg*/			==1) i|=0x40;
    if(Det_5msCtrlBus.Det_5msCtrlVehStopStInfo.VehStandStillStFlg					==1) i|=0x80;
    AHB_LOG_DATA[75] =(uint8_t) (i);

    i=0;/*32~39*/
     if(MuxCurCmd.u1Priority/*lcahbu1PedalOffsetInvalidFlg*/				==1) i|=0x01;
     if(MuxCurCmd.u1CmdHalt/*lcahbu1InhibitForDiagFlg*/					==1) i|=0x02;
     if(MuxCurCmd.u1BuffFIFOAdded/*ldahbu1PedalReleaseFlg1*/					==1) i|=0x04;
     if(MuxCurCmd.u1BuffRemoved/*ldahbu1PedalReleaseFlg2*/					==1) i|=0x08;
     if(lapctst_MuxCtrlFlg.u1SafetyControlActive/*liu1fVoltageLowLv1ErrDet*/==1) i|=0x10;
     if(lapctst_MuxWhlCtrlFlg.u1MuxNChannelCtrlActive/*liu1fVoltageLowestErrDet*/==1) i|=0x20;
     if(/*temp*/MuxCurCmd.u1BuffInserted/*lcahbu1IncAhbEnterExitThresFlg*/	==1) i|=0x40;
     if(/*temp*/lapctst_MuxCtrlFlg.u1InAbsCtrlFadeOut) i|=0x80;
     AHB_LOG_DATA[76] =(uint8_t) (i);

    i=0;/*40~47*/
    if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlIvReq  	==1) i|=0x01;
    if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrIvReq  	==1) i|=0x02;
    if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RlIvReq  	==1) i|=0x04;
    if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RrIvReq  	==1) i|=0x08;
    if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlOvReq  	==1) i|=0x10;
    if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrOvReq  	==1) i|=0x20;
    if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RlOvReq  	==1) i|=0x40;
    if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RrOvReq  	==1) i|=0x80;
    AHB_LOG_DATA[77] =(uint8_t) (i);                       /*ch 58*/

    AHB_LOG_DATA[78] = (uint8_t) (MuxCurCmd.s16WhlTargetP/100);		/*ch 59*/
    AHB_LOG_DATA[79] = (uint8_t) (MuxCurCmd.s16EstWhlP   /100);
}
#endif /*M_IDB_LOG_DATA_MULTIPLEX*/

#if (M_LOGIC_LOG_DATA ==M_IDB_LOG_DATA_PCT_HW_TEST)
static void LSIDB_vCallPCTLogDataHWTest(void)
{
	uint8_t	i;

	/***************************************************************************************************/
	/*                                           LOG_BYTE0                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[0] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar);
	AHB_LOG_DATA[1] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar >> 8);

#if __P_SENSOR_MEASURE_TYPE== EACH_CIRCUIT_PRESSURE
	AHB_LOG_DATA[2] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar);
	AHB_LOG_DATA[3] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar>>8);
	AHB_LOG_DATA[4] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar);
	AHB_LOG_DATA[5] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar>>8);
#else
	AHB_LOG_DATA[2] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPistPFildInfo.PistPFild_1_100Bar);
	AHB_LOG_DATA[3] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPistPFildInfo.PistPFild_1_100Bar>>8);
	AHB_LOG_DATA[4] = (uint8_t) (lacmctls16Idc);
	AHB_LOG_DATA[5] = (uint8_t) (lacmctls16Idc>>8);
#endif
	AHB_LOG_DATA[6] = (uint8_t) Abc_CtrlBus.Abc_CtrlStkRecvryActnIfInfo.StkRecvryCtrlState;

	i=0;
	if(Spc_5msCtrlBus.Spc_5msCtrlBlsFild                     	 						==1) i|=0x01;
	if(Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdtOffsCorrdStOkFlg				==1) i|=0x02;
	if(Spc_5msCtrlBus.Spc_5msCtrlPistPOffsCorrdInfo.PistPOffsCorrdStOkFlg	          	==1) i|=0x04;
	if(Bbc_CtrlBus.Bbc_CtrlIgnOnOffSts													==1) i|=0x08;
	if(u8IdbCtrlInhibitFlg																==1) i|=0x10;
	if(/*Eem_ResidualModReqBus.Eem_ResidualModReqResidualModeEntryFlg*/0				==1) i|=0x20;
	if(Mcc_1msCtrlBus.Mcc_1msCtrlMotICtrlFadeOutState									==1) i|=0x40;
	if(Pct_5msCtrlBus.Pct_5msCtrlIdbVlvActInfo.VlvFadeoutEndOK							==1) i|=0x80;

	AHB_LOG_DATA[7] =(uint8_t) (i);

	/***************************************************************************************************/
	/*                                           LOG_BYTE1                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[8]  = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPedlTrvlFinal);
	AHB_LOG_DATA[9]  = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPedlTrvlFinal >> 8);
	AHB_LOG_DATA[10]  = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec);
	AHB_LOG_DATA[11]  = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec >> 8);

#if __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
	AHB_LOG_DATA[12]  = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlCircPRateInfo.PrimCircPChgDurg10ms);
	AHB_LOG_DATA[13]  = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlCircPRateInfo.PrimCircPChgDurg10ms >>8);
#else
	AHB_LOG_DATA[12]  = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPistPRateInfo.PistPRateBarPerSec);
	AHB_LOG_DATA[13]  = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPistPRateInfo.PistPRateBarPerSec>>8);
#endif
#if (M_IDB_TEST == ENABLE)
	AHB_LOG_DATA[14] = (uint8_t) (lu8IdbTestChangeTestCase);
#else
	AHB_LOG_DATA[14] = (uint8_t) (PressCTRLLocalBus.u16MuxWheelVolumeState);
#endif
	i=0;
	if(Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.DrvrIntendToRelsBrk	 				==1) i|=0x01;
	if(Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.PanicBrkStFlg         	 			==1) i|=0x02;
	if(Bbc_CtrlBus.Bbc_CtrlBaseBrkCtrlrActFlg               							==1) i|=0x04;
	if(Bbc_CtrlBus.Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo.PedlSimrPGendOnFlg             ==1) i|=0x08;
	if(Bbc_CtrlBus.Bbc_CtrlBaseBrkCtrlModInfo.VehStandStillStFlg		                ==1) i|=0x10;
	if(Det_1msCtrlBus.Det_1msCtrlBrkPedlStatus1msInfo.PanicBrkSt1msFlg                	==1) i|=0x20;
	if(lapcts16MovAvgBuffResetFlg														==1) i|=0x40;
	if(lapctDelStrkActFlg.bit2/*lapctu1NegativeCompDisableFlg*/							==1) i|=0x80;
	AHB_LOG_DATA[15] = (uint8_t) (i);

	/***************************************************************************************************/
	/*                                           LOG_BYTE2                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[16]  = (uint8_t) (Bbc_CtrlBus.Bbc_CtrlTarPFromBaseBrkCtrlr);
	AHB_LOG_DATA[17]  = (uint8_t) (Bbc_CtrlBus.Bbc_CtrlTarPFromBaseBrkCtrlr >> 8);

	AHB_LOG_DATA[18]  = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlPCtrlFadeoutSt);
	AHB_LOG_DATA[19]  = (uint8_t) (PressCTRLLocalBus.u8WhlCtrlIndex);

	AHB_LOG_DATA[20]  = (uint8_t) (lu16IdbTestDebugger01);//(Rbc_CtrlBus.Rbc_CtrlBrkPRednForBaseBrkCtrlr);
	AHB_LOG_DATA[21]  = (uint8_t) (lu16IdbTestDebugger01>>8);//(Rbc_CtrlBus.Rbc_CtrlBrkPRednForBaseBrkCtrlr >>8);

	AHB_LOG_DATA[22]  = (uint8_t) (MTRCurCTRLsrcbus.CalcIqRefPidState);
	AHB_LOG_DATA[23]  = (uint8_t) (MTRCurCTRLsrcbus.CalcIqRefBaseState);

	/***************************************************************************************************/
	/*                                           LOG_BYTE3                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[24] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlFinalTarPFromPCtrl );
	AHB_LOG_DATA[25] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlFinalTarPFromPCtrl >> 8);
	AHB_LOG_DATA[26] = (uint8_t) (PressCTRLLocalBus.s16MuxPwrPistonTarDP );
	AHB_LOG_DATA[27] = (uint8_t) (PressCTRLLocalBus.s16MuxPwrPistonTarDP >> 8);
#if (M_IDB_TEST == ENABLE)
	AHB_LOG_DATA[28] = (uint8_t) (lu8IdbTestChangeWhlVolCnt);
	AHB_LOG_DATA[29] = (uint8_t) (lau8StrokeDirection);
#else
	AHB_LOG_DATA[28] = (uint8_t) (lapcts16DeltaStrokeMap);
	AHB_LOG_DATA[29] = (uint8_t) (lapcts16DeltaStrokeMap >> 8);
#endif
	AHB_LOG_DATA[30] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotCtrlMode);
	/*AHB_LOG_DATA[30] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlPCtrlFadeoutSt);*/

	i=0;
	if(/*Eem_EscSnrFaultDetnBus.Eem_EscSnrFaultDetnCanErrInfo.AyErr*/0							==1) i|=0x01;
	if(/*Eem_ErrFlagSetBus.Eem_ErrFlagSetSnsrErrInfo.EscSnsrErr*/0								==1) i|=0x02;
	if(/*Eem_EscSnrFaultDetnBus.Eem_EscSnrFaultDetnCanErrInfo.YawErr*/0							==1) i|=0x04;
#if (M_IDB_TEST == ENABLE)
	if(IdbTest_Output.m_lu8TestReady														==1) i|=0x08;
	if(lu8IdbTestAnalyzerFlg																==1) i|=0x10;
	if(lu8IdbTestLoggingTriggerFlg															==1) i|=0x20;
#else
	if(Pct_5msCtrlBus.Pct_5msCtrlPCtrlAct															==1) i|=0x08;
	if(MpsD1_Compen_SetFag_at1ms															==1) i|=0x10;
	if(Abc_CtrlBus.Abc_CtrlEscSwtStInfo.EscDisabledBySwt									==1) i|=0x20;
#endif
	if(/*Eem_EscSnrFaultDetnBus.Eem_EscSnrFaultDetnCanErrInfo.AxErr*/0							==1) i|=0x40;
	if(/*Eem_EscSnrFaultDetnBus.Eem_EscSnrFaultDetnCanErrInfo.StrErr*/0							==1) i|=0x80;
	AHB_LOG_DATA[31] =(uint8_t) (i);

	/***************************************************************************************************/
	/*                                           LOG_BYTE4                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[32] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlPCtrlBoostMod);
	AHB_LOG_DATA[33] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlPCtrlSt);
	AHB_LOG_DATA[34] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlTarDeltaStk);
	AHB_LOG_DATA[35] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlTarDeltaStk >> 8);
	AHB_LOG_DATA[36] = (uint8_t) (lapcts16DeltaStrokePerror);
	AHB_LOG_DATA[37] = (uint8_t) (lapcts16DeltaStrokePerror>>8);
	AHB_LOG_DATA[38] = (uint8_t) (lapcts16DeltaStrokeMapAvg);
	AHB_LOG_DATA[39] = (uint8_t) (lapcts16DeltaStrokeMapAvg >> 8);

	/***************************************************************************************************/
	/*                                           LOG_BYTE5                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[40] = (uint8_t) (Vlv_ActrBus.Vlv_ActrVlvMonInfo.SimVlvMon/10);
	AHB_LOG_DATA[41] = (uint8_t) (Vlv_ActrBus.Vlv_ActrVlvMonInfo.PrimCutVlvMon/10);
	AHB_LOG_DATA[42] = (uint8_t) (Vlv_ActrBus.Vlv_ActrVlvMonInfo.SecdCutVlvMon/10);
	AHB_LOG_DATA[43] = (uint8_t) (Vlv_ActrBus.Vlv_ActrVlvMonInfo.VlvInFLMon/10);
	AHB_LOG_DATA[44] = (uint8_t) (Vlv_ActrBus.Vlv_ActrVlvMonInfo.VlvInFRMon/10);

	AHB_LOG_DATA[45] = (uint8_t) (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.CircVlvReqData[0]/10);
	AHB_LOG_DATA[46] = (uint8_t) (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.RelsVlvReqData[0]/10);

	AHB_LOG_DATA[47] = (uint8_t) (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PressDumpVlvReqData[0]/10);
	/***************************************************************************************************/
	/*                                           LOG_BYTE6                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[48] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlPCtrlFadeoutSt);
	AHB_LOG_DATA[49] = (uint8_t) (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SimVlvReqData[0]/10);
	AHB_LOG_DATA[50] = (uint8_t) (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReqData[0]/10);

	AHB_LOG_DATA[51] = (uint8_t)(Msp_CtrlBus.Msp_CtrlMotRotgAgSigInfo.MotMechAngleSpdFild);
	AHB_LOG_DATA[52] = (uint8_t)(Msp_CtrlBus.Msp_CtrlMotRotgAgSigInfo.MotMechAngleSpdFild >>8);

	i=0;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SimVlvReq		  	 		==1) i|=0x01;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReq      	 		==1) i|=0x02;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SecdCutVlvReq       	 	==1) i|=0x04;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlIvReq			         	 	==1) i|=0x08;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrIvReq         			 	==1) i|=0x10;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RlIvReq       				   	==1) i|=0x20;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RrIvReq     			     	==1) i|=0x40;
	if(/*Proxy_lespu1TCU_SWI_GS*/0											==1) i|=0x80;

	AHB_LOG_DATA[53] =(uint8_t) (i);

	i=0;
	if(Abc_CtrlBus.Abc_CtrlFunctionLamp												==1) i|=0x01;
	if(Abc_CtrlBus.Abc_CtrlEbdCtrlInfo.EbdActFlg	 	 							!=0) i|=0x02;
	if(Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsActFlg		 							!=0) i|=0x04;
	if(Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.TcsActFlg	 	 							!=0) i|=0x08;
	if(Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscActFlg		 							!=0) i|=0x10;
	if(lapctu16FadeoutState1EndOK													==1) i|=0x20;
	if(Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.PrimBalVlvReq							==1) i|=0x40;
	if(Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.SecdBalVlvReq							==1) i|=0x80;
	AHB_LOG_DATA[54] = (uint8_t) (i);

	i=0;
	if(Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSet 	 							==1) i|=0x01;
	if(Abc_CtrlBus.Abc_CtrlEscSwtStInfo.TcsDisabledBySwt 							==1) i|=0x02;
	if(lsesu16WallDetectedFlg														==1) i|=0x04;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlOvReq									==1) i|=0x08;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrOvReq									==1) i|=0x10;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RlOvReq									==1) i|=0x20;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RrOvReq									==1) i|=0x40;
	if(PressCTRLLocalBus.u8MultiplexCtrlActive										==1) i|=0x80;
	AHB_LOG_DATA[55] = (uint8_t) (i);

	/***************************************************************************************************/
	/*                                           LOG_BYTE7                                             */
	/***************************************************************************************************/
	if(Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSet == 0)
	{
		AHB_LOG_DATA[56] = (uint8_t) (lsess32TgtPosiOrgSet);
		AHB_LOG_DATA[57] = (uint8_t) (lsess32TgtPosiOrgSet >> 8);
		AHB_LOG_DATA[58] = (uint8_t) (lsess32TgtPosiOrgSet >> 16);
		AHB_LOG_DATA[59] = (uint8_t) (lsess32TgtPosiOrgSet >> 24);
	}
	else
	{
		AHB_LOG_DATA[56] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn);
		AHB_LOG_DATA[57] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn >> 8);
		AHB_LOG_DATA[58] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn >> 16);
		AHB_LOG_DATA[59] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn >> 24);
	}
	AHB_LOG_DATA[60] = (uint8_t) (Msp_1msCtrlBus.Msp_1msCtrlMotRotgAg1msSigInfo.StkPosnMeasd);
	AHB_LOG_DATA[61] = (uint8_t) (Msp_1msCtrlBus.Msp_1msCtrlMotRotgAg1msSigInfo.StkPosnMeasd >> 8);
	AHB_LOG_DATA[62] = (uint8_t) (Msp_1msCtrlBus.Msp_1msCtrlMotRotgAg1msSigInfo.StkPosnMeasd >> 16);
	AHB_LOG_DATA[63] = (uint8_t) (Msp_1msCtrlBus.Msp_1msCtrlMotRotgAg1msSigInfo.StkPosnMeasd >> 24);

	/***************************************************************************************************/
	/*                                           LOG_BYTE8                                             */
	/***************************************************************************************************/
	if(Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSet == 0)
	{
		AHB_LOG_DATA[64] = (uint8_t) (Ses_CtrlBus.Ses_CtrlMotDqIRefSesInfo.IdRef);
		AHB_LOG_DATA[65] = (uint8_t) (Ses_CtrlBus.Ses_CtrlMotDqIRefSesInfo.IdRef >> 8);
		AHB_LOG_DATA[66] = (uint8_t) (Ses_CtrlBus.Ses_CtrlMotDqIRefSesInfo.IqRef);
		AHB_LOG_DATA[67] = (uint8_t) (Ses_CtrlBus.Ses_CtrlMotDqIRefSesInfo.IqRef >> 8);
	}
	else
	{
		AHB_LOG_DATA[64] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo.IdRef);
		AHB_LOG_DATA[65] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo.IdRef >> 8);
		AHB_LOG_DATA[66] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo.IqRef);
		AHB_LOG_DATA[67] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo.IqRef >> 8);
	}
	AHB_LOG_DATA[68] = (uint8_t) (lapcts16TargetPressOldlog);
	AHB_LOG_DATA[69] = (uint8_t) (lapcts16TargetPressOldlog >>8);
	AHB_LOG_DATA[70] = (uint8_t) (lmccs16WrpmMechRef);
	AHB_LOG_DATA[71] = (uint8_t) (lmccs16WrpmMechRef >> 8);

	/***************************************************************************************************/
	/*                                           LOG_BYTE9                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[72] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIdMeasd);
	AHB_LOG_DATA[73] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIdMeasd>>8);
	AHB_LOG_DATA[74] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIqMeasd);
	AHB_LOG_DATA[75] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIqMeasd>>8);
	AHB_LOG_DATA[76] = (uint8_t) (Acmio_SenVdcLink);
	AHB_LOG_DATA[77] = (uint8_t) (Acmio_SenVdcLink >> 8);
	AHB_LOG_DATA[78] = (uint8_t) (lapcts16DeltaStrokePerrorFinal);
	AHB_LOG_DATA[79] = (uint8_t) (lapcts16DeltaStrokePerrorFinal >> 8);
}
#endif /*M_IDB_LOG_DATA_PCT_HW_TEST*/

#if (M_LOGIC_LOG_DATA == M_IDB_WPEByVOL_DATA)
static void LSIDB_vCallIDBWPEByVOLLogData(void)
{
	uint8_t	i;

	/* LOG_BYTE 0  */
	/* Sig Input & Proc */
	AHB_LOG_DATA[0]  = (uint8_t) (ldidbs32PedlSimrP);
	AHB_LOG_DATA[1]  = (uint8_t) (ldidbs32PedlSimrP >> 8);
	AHB_LOG_DATA[2]  = (uint8_t) (ldidbs32PrimCircP);
	AHB_LOG_DATA[3]  = (uint8_t) (ldidbs32PrimCircP>>8);
	AHB_LOG_DATA[4]  = (uint8_t) (ldidbs32SecdCircP);
	AHB_LOG_DATA[5]  = (uint8_t) (ldidbs32SecdCircP>>8);
	AHB_LOG_DATA[6]  = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPedlTrvlFinal);
	AHB_LOG_DATA[7]  = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPedlTrvlFinal >> 8);

	/* LOG_BYTE 1 */
	/* Delete Orignal estP, Add combined EstP */
	AHB_LOG_DATA[8]  = (uint8_t) (0);
	AHB_LOG_DATA[9]  = (uint8_t) (0>>8);
	AHB_LOG_DATA[10] = (uint8_t) (0);
	AHB_LOG_DATA[11] = (uint8_t) (0>>8);
	AHB_LOG_DATA[12] = (uint8_t) (0);
	AHB_LOG_DATA[13] = (uint8_t) (0>>8);
	AHB_LOG_DATA[14] = (uint8_t) (0);
	AHB_LOG_DATA[15] = (uint8_t) (0>>8);

	/* LOG_BYTE 2 */
	/* WPE Detection By Volume By P */
	AHB_LOG_DATA[16] = (uint8_t) (WPE_FL.ldidbs16EstmdWhlP);
	AHB_LOG_DATA[17] = (uint8_t) (WPE_FL.ldidbs16EstmdWhlP>>8);
	AHB_LOG_DATA[18] = (uint8_t) (WPE_FR.ldidbs16EstmdWhlP);
	AHB_LOG_DATA[19] = (uint8_t) (WPE_FR.ldidbs16EstmdWhlP>>8);
	AHB_LOG_DATA[20] = (uint8_t) (WPE_RL.ldidbs16EstmdWhlP);
	AHB_LOG_DATA[21] = (uint8_t) (WPE_RL.ldidbs16EstmdWhlP>>8);
	AHB_LOG_DATA[22] = (uint8_t) (WPE_RR.ldidbs16EstmdWhlP);
	AHB_LOG_DATA[23] = (uint8_t) (WPE_RR.ldidbs16EstmdWhlP>>8);

	/* LOG_BYTE 3 */
	/* WPE Detection By Volume By S */
	AHB_LOG_DATA[24] = (uint8_t) (WPE_FL.ldidbs16EstmdWhlPByS);
	AHB_LOG_DATA[25] = (uint8_t) (WPE_FL.ldidbs16EstmdWhlPByS>>8);
	AHB_LOG_DATA[26] = (uint8_t) (WPE_FR.ldidbs16EstmdWhlPByS);
	AHB_LOG_DATA[27] = (uint8_t) (WPE_FR.ldidbs16EstmdWhlPByS>>8);
	AHB_LOG_DATA[28] = (uint8_t) (WPE_RL.ldidbs16EstmdWhlPByS);
	AHB_LOG_DATA[29] = (uint8_t) (WPE_RL.ldidbs16EstmdWhlPByS>>8);
	AHB_LOG_DATA[30] = (uint8_t) (WPE_RR.ldidbs16EstmdWhlPByS);
	AHB_LOG_DATA[31] = (uint8_t) (WPE_RR.ldidbs16EstmdWhlPByS>>8);

	/* LOG_BYTE 4 */
	/* WPE Detection Volume By P */
	AHB_LOG_DATA[32] = (uint8_t) (WPE_FL.ldidbs32WhlVolumeByP);
	AHB_LOG_DATA[33] = (uint8_t) (WPE_FL.ldidbs32WhlVolumeByP>>8);
	AHB_LOG_DATA[34] = (uint8_t) (WPE_FR.ldidbs32WhlVolumeByP);
	AHB_LOG_DATA[35] = (uint8_t) (WPE_FR.ldidbs32WhlVolumeByP>>8);
	AHB_LOG_DATA[36] = (uint8_t) (WPE_RL.ldidbs32WhlVolumeByP);
	AHB_LOG_DATA[37] = (uint8_t) (WPE_RL.ldidbs32WhlVolumeByP>>8);
	AHB_LOG_DATA[38] = (uint8_t) (WPE_RR.ldidbs32WhlVolumeByP);
	AHB_LOG_DATA[39] = (uint8_t) (WPE_RR.ldidbs32WhlVolumeByP>>8);

	/* LOG_BYTE 5 */
	/* WPE Detection Volume By S */
	AHB_LOG_DATA[40] = (uint8_t) (WPE_FL.ldidbs32WhlVolumeByS);
	AHB_LOG_DATA[41] = (uint8_t) (WPE_FL.ldidbs32WhlVolumeByS>>8);
	AHB_LOG_DATA[42] = (uint8_t) (WPE_FR.ldidbs32WhlVolumeByS);
	AHB_LOG_DATA[43] = (uint8_t) (WPE_FR.ldidbs32WhlVolumeByS>>8);
	AHB_LOG_DATA[44] = (uint8_t) (WPE_RL.ldidbs32WhlVolumeByS);
	AHB_LOG_DATA[45] = (uint8_t) (WPE_RL.ldidbs32WhlVolumeByS>>8);
	AHB_LOG_DATA[46] = (uint8_t) (WPE_RR.ldidbs32WhlVolumeByS);
	AHB_LOG_DATA[47] = (uint8_t) (WPE_RR.ldidbs32WhlVolumeByS>>8);

	/* LOG_BYTE 6 */
	/* WPE Valve On Time */
	AHB_LOG_DATA[48] = (uint8_t) (WPE_FL.ldidbu8InletValveOpenTiCorrd);
	AHB_LOG_DATA[49] = (uint8_t) (WPE_FR.ldidbu8InletValveOpenTiCorrd);
	AHB_LOG_DATA[50] = (uint8_t) (WPE_RL.ldidbu8InletValveOpenTiCorrd);
	AHB_LOG_DATA[51] = (uint8_t) (WPE_RR.ldidbu8InletValveOpenTiCorrd);
	AHB_LOG_DATA[52] = (uint8_t) (WPE_FL.ldidbu8OutletValveOpenTiCorrd);
	AHB_LOG_DATA[53] = (uint8_t) (WPE_FR.ldidbu8OutletValveOpenTiCorrd);
	AHB_LOG_DATA[54] = (uint8_t) (WPE_RL.ldidbu8OutletValveOpenTiCorrd);
	AHB_LOG_DATA[55] = (uint8_t) (WPE_RR.ldidbu8OutletValveOpenTiCorrd);

	/* LOG_BYTE 7 */
	/* Motor Stroke Position */
	if(Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSet == 0)
	{
		AHB_LOG_DATA[56] = (uint8_t) (lsess32TgtPosiOrgSet);
		AHB_LOG_DATA[57] = (uint8_t) (lsess32TgtPosiOrgSet >> 8);
		AHB_LOG_DATA[58] = (uint8_t) (lsess32TgtPosiOrgSet >> 16);
		AHB_LOG_DATA[59] = (uint8_t) (lsess32TgtPosiOrgSet >> 24);
	}
	else
	{
		AHB_LOG_DATA[56] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn);
		AHB_LOG_DATA[57] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn >> 8);
		AHB_LOG_DATA[58] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn >> 16);
		AHB_LOG_DATA[59] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn >> 24);
	}
	AHB_LOG_DATA[60] = (uint8_t) (ldidbs32StrokePosition);
	AHB_LOG_DATA[61] = (uint8_t) (ldidbs32StrokePosition >> 8);
	AHB_LOG_DATA[62] = (uint8_t) (ldidbs32StrokePosition >> 16);
	AHB_LOG_DATA[63] = (uint8_t) (ldidbs32StrokePosition >> 24);
	
	/* LOG_BYTE 8 */
	/* Motor I ctrl*/
	AHB_LOG_DATA[64] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo.IqRef);
	AHB_LOG_DATA[65] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo.IqRef >> 8);
	AHB_LOG_DATA[66] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIqMeasd);
	AHB_LOG_DATA[67] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIqMeasd>>8);
	/* Target S & P */
	AHB_LOG_DATA[68] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlTarDeltaStk);
	AHB_LOG_DATA[69] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlTarDeltaStk >> 8);
	AHB_LOG_DATA[70] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlFinalTarPFromPCtrl);
	AHB_LOG_DATA[71] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlFinalTarPFromPCtrl >> 8);

	/* LOG_BYTE 9 */
	/* Stroke Recvry */
	AHB_LOG_DATA[72] = (uint8_t) Abc_CtrlBus.Abc_CtrlStkRecvryActnIfInfo.StkRecvryCtrlState;
	/* Flag1 */
	i=0;
	if(Spc_5msCtrlBus.Spc_5msCtrlBlsFild										==1) i|=0x01;
	if(Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdtOffsCorrdStOkFlg		==1) i|=0x02;
	if(Spc_5msCtrlBus.Spc_5msCtrlCircPOffsCorrdInfo.PrimCircPOffsCorrdStOkFlg	==1) i|=0x04;
	if(Bbc_CtrlBus.Bbc_CtrlIgnOnOffSts											==1) i|=0x08;
	if(WPE_FL.ldidbu8ContinuedFullRiseModeFlg									==1) i|=0x10;
	if(WPE_FR.ldidbu8ContinuedFullRiseModeFlg									==1) i|=0x20;
	if(WPE_RL.ldidbu8ContinuedFullRiseModeFlg									==1) i|=0x40;
	if(WPE_RR.ldidbu8ContinuedFullRiseModeFlg									==1) i|=0x80;
	AHB_LOG_DATA[73] =(uint8_t) (i);

	/* Flag2 */
	i=0;
	if(Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.DrvrIntendToRelsBrk			==1) i|=0x01;
	if(Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.PanicBrkStFlg				==1) i|=0x02;
	if(Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.PedlReldStFlg1				==1) i|=0x04;
	if(Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.PedlReldStFlg2				==1) i|=0x08;
	if(Det_5msCtrlBus.Det_5msCtrlVehStopStInfo.VehStandStillStFlg				==1) i|=0x10;
	if(Det_1msCtrlBus.Det_1msCtrlBrkPedlStatus1msInfo.PanicBrkSt1msFlg			==1) i|=0x20;
	if(Bbc_CtrlBus.Bbc_CtrlBaseBrkCtrlrActFlg									==1) i|=0x40;
	if(Bbc_CtrlBus.Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo.PedlSimrPGendOnFlg		==1) i|=0x80;
	AHB_LOG_DATA[74] = (uint8_t) (i);

	/* Flag3 */
	i=0;
	if(Rbc_CtrlBus.Rbc_CtrlRgnBrkCtrlrActStFlg									==1) i|=0x01;
	if(Rbc_CtrlBus.Rbc_CtrlRgnBrkCtlrBlendgFlg									==1) i|=0x02;
	if(Bbc_CtrlBus.Bbc_CtrlBaseBrkCtrlModInfo.LowSpdCtrlInhibitFlg				==1) i|=0x04;
	if(Bbc_CtrlBus.Bbc_CtrlBaseBrkCtrlModInfo.StopdVehCtrlModFlg				==1) i|=0x08;
	if(Pct_5msCtrlBus.Pct_5msCtrlPCtrlAct										==1) i|=0x10;
	if(ldidbu8ActvBrkCtrlrActFlg												==1) i|=0x20;
	if(0																		==1) i|=0x40;
	if(0																		==1) i|=0x80;
	AHB_LOG_DATA[75] =(uint8_t) (i);

	/* Flag4 */
	i=0;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SimVlvReq						==1) i|=0x01;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReq					==1) i|=0x02;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SecdCutVlvReq					==1) i|=0x04;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PrimCircVlvReq					==1) i|=0x08;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SecdCircVlvReq					==1) i|=0x10;
	if(Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.PrimBalVlvReq						==1) i|=0x20;
	if(Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.SecdBalVlvReq						==1) i|=0x40;
	if(Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.ChmbBalVlvReq						==1) i|=0x80;
	AHB_LOG_DATA[76] =(uint8_t) (i);

	/* Flag5 */
	i=0;
	if(Abc_CtrlBus.Abc_CtrlEbdCtrlInfo.EbdActFlg								==1) i|=0x01;
	if(Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsActFlg								==1) i|=0x02;
	if(Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.TcsActFlg								==1) i|=0x04;
	if(Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscActFlg								==1) i|=0x08;
	if(Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSet							==1) i|=0x10;
	if(Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSetErr							==1) i|=0x20;
	if(lsesu16WallDetectedFlg													==1) i|=0x40;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.RelsVlvReq						==1) i|=0x80;   //Eem_LampReqBus.Eem_LampReqLampInfo.FnLampDrv
	AHB_LOG_DATA[77] =(uint8_t) (i);

	/* Flag6 */
	i=0;
	if (WPE_FL.ldidbu8WhlVolumeResetFlg											==1) i|=0x01;
	if (WPE_FR.ldidbu8WhlVolumeResetFlg											==1) i|=0x02;
	if (WPE_RL.ldidbu8WhlVolumeResetFlg											==1) i|=0x04;
	if (WPE_RR.ldidbu8WhlVolumeResetFlg											==1) i|=0x08;
	if (WPE_FL.ldidbu8ContinuedDumpModeFlg										==1) i|=0x10;
	if (WPE_FR.ldidbu8ContinuedDumpModeFlg										==1) i|=0x20;
	if (WPE_RL.ldidbu8ContinuedDumpModeFlg										==1) i|=0x40;
	if (WPE_RR.ldidbu8ContinuedDumpModeFlg										==1) i|=0x80;
	AHB_LOG_DATA[78] =(uint8_t) (i);

	/* Flag7 */
	i=0;
	if (WPE_FL.ldidbu8WhlPByVol1stRiseFlg										==1) i|=0x01;
	if (WPE_FR.ldidbu8WhlPByVol1stRiseFlg										==1) i|=0x02;
	if (WPE_RL.ldidbu8WhlPByVol1stRiseFlg										==1) i|=0x04;
	if (WPE_RR.ldidbu8WhlPByVol1stRiseFlg										==1) i|=0x08;
	if (ldidbu8PrimCircBrakeOnInitFlg											==1) i|=0x10;
	if (ldidbu8SecdCircBrakeOnInitFlg											==1) i|=0x20;
	if (0																		==1) i|=0x40;
	if (0																		==1) i|=0x80;
	AHB_LOG_DATA[79] =(uint8_t) (i);
}
#endif /*M_IDB_WPEByVOL_DATA*/

#if DISABLE
extern int8_t laptcs8CVlvCtrlStat;
extern int8_t laptcs8RLVlvCtrlStat;
extern int8_t laptcs8PDVlvCtrlStat;
extern int8_t laptcs8BALVlvCtrlStat;


extern int8_t laptcs8CVResponseLevel;
extern int8_t laptcs8RLVResponseLevel;
extern int8_t laptcs8PDVResponseLevel;
extern int8_t laptcs8BALResponseLevel;

extern uint8_t lapctu8Cut1VlvCtrlState  ;
extern uint8_t lapctu8Cut2VlvCtrlState  ;
extern uint8_t lapctu8SimVlvCtrlState   ;

extern uint8_t lapctu8Cut1VlvCtrlRespLv ;
extern uint8_t lapctu8Cut2VlvCtrlRespLv ;
extern uint8_t lapctu8SimVlvCtrlRespLv  ;

extern uint8_t lapctu8Cut1VlvMaxholdTime;
extern uint8_t lapctu8Cut2VlvMaxholdTime;
extern uint8_t lapctu8SimVlvMaxholdTime ;

extern LA_VAL_PETN laVAT__CutVlv1;
extern LA_VAL_PETN laVAT__CutVlv2;
extern LA_VAL_PETN laVAT__SimVlv;
extern LA_VAL_PETN laVAT__PDV ;
extern LA_VAL_PETN laVAT__BALV;
extern LA_VAL_PETN laVAT__RLV ;
extern LA_VAL_PETN laVAT__CIRV;
#endif
#if (M_LOGIC_LOG_DATA==M_IDB_LOG_DATA_VALVE_CHECK)
static void LSIDB_vCallIDBVLVLogData(void)
{
	uint8_t	i;

	/***************************************************************************************************/
	/*                                           LOG_BYTE0                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[0] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar);
	AHB_LOG_DATA[1] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPedlSimrPFildInfo.PedlSimrPFild_1_100Bar >> 8);

#if __P_SENSOR_MEASURE_TYPE== EACH_CIRCUIT_PRESSURE
	AHB_LOG_DATA[2] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar);
	AHB_LOG_DATA[3] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.PrimCircPFild_1_100Bar>>8);
	AHB_LOG_DATA[4] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar);
	AHB_LOG_DATA[5] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlCircPFildInfo.SecdCircPFild_1_100Bar>>8);
#else
	AHB_LOG_DATA[2] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPistPFildInfo.PistPFild_1_100Bar);
	AHB_LOG_DATA[3] = (uint8_t) (Spc_5msCtrlBus.Spc_5msCtrlPistPFildInfo.PistPFild_1_100Bar>>8);
	AHB_LOG_DATA[4] = (uint8_t) (lacmctls16Idc);
	AHB_LOG_DATA[5] = (uint8_t) (lacmctls16Idc>>8);
#endif
	AHB_LOG_DATA[6] = (uint8_t) (Abc_CtrlBus.Abc_CtrlStkRecvryActnIfInfo.StkRecvryCtrlState);

	i=0;
	if(Spc_5msCtrlBus.Spc_5msCtrlBlsFild                     	 						==1) i|=0x01;
	if(Spc_5msCtrlBus.Spc_5msCtrlPedlTrvlOffsCorrdInfo.PdtOffsCorrdStOkFlg				==1) i|=0x02;
	if(Spc_5msCtrlBus.Spc_5msCtrlPistPOffsCorrdInfo.PistPOffsCorrdStOkFlg	          	==1) i|=0x04;
	if(Bbc_CtrlBus.Bbc_CtrlIgnOnOffSts													==1) i|=0x08;
	if(u8IdbCtrlInhibitFlg																==1) i|=0x10;
	if(Eem_MainEemCtrlInhibitData.Eem_BBS_DefectiveModeFail								==1) i|=0x20;
	if(Mcc_1msCtrlBus.Mcc_1msCtrlMotICtrlFadeOutState									==1) i|=0x40;
	if(Pct_5msCtrlBus.Pct_5msCtrlIdbVlvActInfo.VlvFadeoutEndOK							==1) i|=0x80;

	AHB_LOG_DATA[7] =(uint8_t) (i);

	/***************************************************************************************************/
	/*                                           LOG_BYTE1                                             */
	/***************************************************************************************************/
#if DISABLE
	AHB_LOG_DATA[8]  = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPedlTrvlFinal);
	AHB_LOG_DATA[9]  = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPedlTrvlFinal >> 8);
	AHB_LOG_DATA[10]  = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec);
	AHB_LOG_DATA[11]  = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPedlTrvlRateInfo.PedlTrvlRateMilliMtrPerSec >> 8);

#if __P_SENSOR_MEASURE_TYPE == EACH_CIRCUIT_PRESSURE
	AHB_LOG_DATA[12]  = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlCircPRateInfo.PrimCircPChgDurg10ms);
	AHB_LOG_DATA[13]  = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlCircPRateInfo.PrimCircPChgDurg10ms >>8);
#else
	AHB_LOG_DATA[12]  = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPistPRateInfo.PistPRateBarPerSec);
	AHB_LOG_DATA[13]  = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPistPRateInfo.PistPRateBarPerSec>>8);
#endif
	AHB_LOG_DATA[14] = (uint8_t) (PressCTRLLocalBus.u16MuxWheelVolumeState);
#else
	AHB_LOG_DATA[8] = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPedlTrvlFinal);;
	AHB_LOG_DATA[9] = (uint8_t) (Det_5msCtrlBus.Det_5msCtrlPedlTrvlFinal >> 8);
	AHB_LOG_DATA[10] = (uint8_t) lapctu8Cut1VlvCtrlState/*lavatu8ValveState[0]*/;
	AHB_LOG_DATA[11] = (uint8_t) Pct_5msCtrlBus.Pct_5msCtrlPChamberVolume/*lavatu8ValveState[1]*/;
	AHB_LOG_DATA[12] = (uint8_t) lapctu8SimVlvCtrlState/*lavatu8ValveState[2]*/;
	AHB_LOG_DATA[13] = (uint8_t) lapctu8Cut1VlvCtrlRespLv;
	AHB_LOG_DATA[14] = (uint8_t) lapctu8SimVlvCtrlRespLv;
#endif

	i=0;
	if(/*Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.DrvrIntendToRelsBrk*/lapctu8PdlVlvActHoldOnRequestFlg	 				==1) i|=0x01;
	if(Det_5msCtrlBus.Det_5msCtrlBrkPedlStatusInfo.PanicBrkStFlg         	 			==1) i|=0x02;
	if(Bbc_CtrlBus.Bbc_CtrlBaseBrkCtrlrActFlg               							==1) i|=0x04;
	if(Bbc_CtrlBus.Bbc_CtrlVlvActtnReqByBaseBrkCtrlrInfo.PedlSimrPGendOnFlg             ==1) i|=0x08;
	if(Bbc_CtrlBus.Bbc_CtrlBaseBrkCtrlModInfo.VehStandStillStFlg		                ==1) i|=0x10;
	if(Det_1msCtrlBus.Det_1msCtrlBrkPedlStatus1msInfo.PanicBrkSt1msFlg                	==1) i|=0x20;
	if(lapcts16MovAvgBuffResetFlg														==1) i|=0x40;
	if(lapctDelStrkActFlg.bit2/*lapctu1NegativeCompDisableFlg*/							==1) i|=0x80;
	AHB_LOG_DATA[15] = (uint8_t) (i);

	/***************************************************************************************************/
	/*                                           LOG_BYTE2                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[16]  = (uint8_t) (Bbc_CtrlBus.Bbc_CtrlTarPFromBaseBrkCtrlr);
	AHB_LOG_DATA[17]  = (uint8_t) (Bbc_CtrlBus.Bbc_CtrlTarPFromBaseBrkCtrlr >> 8);

	AHB_LOG_DATA[18]  = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlPCtrlFadeoutSt);
	AHB_LOG_DATA[19]  = (uint8_t) (PressCTRLLocalBus.u8WhlCtrlIndex);

	AHB_LOG_DATA[20]  = (uint8_t) (Rbc_CtrlBus.Rbc_CtrlBrkPRednForBaseBrkCtrlr);
	AHB_LOG_DATA[21]  = (uint8_t) (Rbc_CtrlBus.Rbc_CtrlBrkPRednForBaseBrkCtrlr >>8);

	AHB_LOG_DATA[22]  = (uint8_t) (Bbc_CtrlBus.Bbc_CtrlTarPFromBrkPedl/100);
	AHB_LOG_DATA[23]  = (uint8_t) (SasM_MainSASMOutInfo.SASM_Timeout_Err/*lespu8TCU_G_SEL_DISP*/);

	/***************************************************************************************************/
	/*                                           LOG_BYTE3                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[24] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlFinalTarPFromPCtrl);
	AHB_LOG_DATA[25] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlFinalTarPFromPCtrl >>8);
	AHB_LOG_DATA[26] = (uint8_t) (PressCTRLLocalBus.s16MuxPwrPistonTarDP );
	AHB_LOG_DATA[27] = (uint8_t) (PressCTRLLocalBus.s16MuxPwrPistonTarDP >> 8);
	AHB_LOG_DATA[28] = (uint8_t) (lapcts16DeltaStrokeMap);
	AHB_LOG_DATA[29] = (uint8_t) (lapcts16DeltaStrokeMap >> 8);
	AHB_LOG_DATA[30] = (uint8_t) (/*Mcc_1msCtrlBus.Mcc_1msCtrlMotCtrlMode*/laptcs8BALResponseLevel);

	i=0;
	if(SenPwrM_MainSenPwrMonitorData.SenPwrM_12V_Stable										==1) i|=0x01;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PressDumpVlvReq								==1) i|=0x02;
	if(Eem_MainEemFailData.Eem_Fail_Str         											==1) i|=0x04;
	if(Pct_5msCtrlBus.Pct_5msCtrlPCtrlAct													==1) i|=0x08;
	if(MpsD1_Compen_SetFag_at1ms															==1) i|=0x10;
	if(Abc_CtrlBus.Abc_CtrlEscSwtStInfo.EscDisabledBySwt									==1) i|=0x20;
	if(SasM_MainSASMOutInfo.SASM_Timeout_Ihb												==1) i|=0x40;
	if(Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.ChmbBalVlvReq									==1) i|=0x80;
	AHB_LOG_DATA[31] =(uint8_t) (i);

	/***************************************************************************************************/
	/*                                           LOG_BYTE4                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[32] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlPCtrlBoostMod);
	AHB_LOG_DATA[33] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlPCtrlSt);
	AHB_LOG_DATA[34] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlTarDeltaStk);
	AHB_LOG_DATA[35] = (uint8_t) (Pct_5msCtrlBus.Pct_5msCtrlTarDeltaStk >> 8);
/*	AHB_LOG_DATA[36] = (uint8_t) (lapcts16DeltaStrokePerror);
	AHB_LOG_DATA[37] = (uint8_t) (lapcts16DeltaStrokePerror>>8);
	AHB_LOG_DATA[38] = (uint8_t) (lapcts16DeltaStrokeMapAvg);
	AHB_LOG_DATA[39] = (uint8_t) (lapcts16DeltaStrokeMapAvg >> 8);*/
	AHB_LOG_DATA[36] = (uint8_t)laptcs8CVlvCtrlStat;
	AHB_LOG_DATA[37] = (uint8_t)laptcs8RLVlvCtrlStat;
	AHB_LOG_DATA[38] = (uint8_t)laptcs8CVResponseLevel;
	AHB_LOG_DATA[39] = (uint8_t)laptcs8RLVResponseLevel;

	/***************************************************************************************************/
	/*                                           LOG_BYTE5                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[40] = (uint8_t) (Vlv_ActrBus.Vlv_ActrVlvMonInfo.SimVlvMon/10);
	AHB_LOG_DATA[41] = (uint8_t) (Vlv_ActrBus.Vlv_ActrVlvMonInfo.PrimCutVlvMon/10);
	AHB_LOG_DATA[42] = (uint8_t) (/*Vlv_ActrBus.Vlv_ActrVlvMonInfo.SecdCutVlvMon*/laptcs8PDVlvCtrlStat);
	AHB_LOG_DATA[43] = (uint8_t) (/*Vlv_ActrBus.Vlv_ActrVlvMonInfo.VlvInFLMon/10*/laptcs8BALVlvCtrlStat);/*RLV Vlv Mon C-sample*/
	AHB_LOG_DATA[44] = (uint8_t) (/*Vlv_ActrBus.Vlv_ActrVlvMonInfo.VlvInFRMon/10*/laptcs8PDVResponseLevel);/*CV Vlv Mon C-sample*/
	AHB_LOG_DATA[45] = (uint8_t) (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.RelsVlvReqData[0]/10);
	AHB_LOG_DATA[46] = (uint8_t) (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.CircVlvReqData[0]/10);
	AHB_LOG_DATA[47] = (uint8_t) (Vat_CtrlBus.Vat_CtrlIdbBalVlvReqInfo.ChmbBalVlvReqData[0]/10);

	/***************************************************************************************************/
	/*                                           LOG_BYTE6                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[48] = (uint8_t) (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PressDumpVlvReqData[0]/10);
	AHB_LOG_DATA[49] = (uint8_t) (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PressDumpVlvReqData[0]/*Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SimVlvReqData[0]*//10);
	AHB_LOG_DATA[50] = (uint8_t) (Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReqData[0]/10);

	AHB_LOG_DATA[51] = (uint8_t)(Msp_CtrlBus.Msp_CtrlMotRotgAgSigInfo.MotMechAngleSpdFild);
	AHB_LOG_DATA[52] = (uint8_t)(Msp_CtrlBus.Msp_CtrlMotRotgAgSigInfo.MotMechAngleSpdFild >>8);

	i=0;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SimVlvReq		  	 		==1) i|=0x01;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.PrimCutVlvReq      	 		==1) i|=0x02;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.SecdCutVlvReq       	 	==1) i|=0x04;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlIvReq			         	 	==1) i|=0x08;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrIvReq         			 	==1) i|=0x10;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RlIvReq       				   	==1) i|=0x20;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RrIvReq     			     	==1) i|=0x40;
	if(/*Proxy_lespu1TCU_SWI_GS*/0											==1) i|=0x80;
	AHB_LOG_DATA[53] =(uint8_t) (i);

	i=0;
	if(Abc_CtrlBus.Abc_CtrlFunctionLamp												==1) i|=0x01;
	if(Abc_CtrlBus.Abc_CtrlEbdCtrlInfo.EbdActFlg	 	 							==1) i|=0x02;
	if(Abc_CtrlBus.Abc_CtrlAbsCtrlInfo.AbsActFlg		 							==1) i|=0x04;
	if(Abc_CtrlBus.Abc_CtrlTcsCtrlInfo.TcsActFlg	 	 							==1) i|=0x08;
	if(Abc_CtrlBus.Abc_CtrlEscCtrlInfo.EscActFlg		 							==1) i|=0x10;
	if(lapctu16FadeoutState1EndOK													==1) i|=0x20;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.CircVlvReq							==1) i|=0x40;
	if(Vat_CtrlBus.Vat_CtrlNormVlvReqVlvActInfo.RelsVlvReq							==1) i|=0x80;
	AHB_LOG_DATA[54] = (uint8_t) (i);

	i=0;
	if(Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSet 	 							==1) i|=0x01;
	if(Abc_CtrlBus.Abc_CtrlEscSwtStInfo.TcsDisabledBySwt 							==1) i|=0x02;
	if(lsesu16WallDetectedFlg														==1) i|=0x04;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FlOvReq									==1) i|=0x08;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.FrOvReq									==1) i|=0x10;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RlOvReq									==1) i|=0x20;
	if(Vat_CtrlBus.Vat_CtrlWhlVlvReqIdbInfo.RrOvReq									==1) i|=0x40;
	if(PressCTRLLocalBus.u8MultiplexCtrlActive										==1) i|=0x80;
	AHB_LOG_DATA[55] = (uint8_t) (i);

	/***************************************************************************************************/
	/*                                           LOG_BYTE7                                             */
	/***************************************************************************************************/
	if(Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSet == 0)
	{
		AHB_LOG_DATA[56] = (uint8_t) (lsess32TgtPosiOrgSet);
		AHB_LOG_DATA[57] = (uint8_t) (lsess32TgtPosiOrgSet >> 8);
		AHB_LOG_DATA[58] = (uint8_t) (lsess32TgtPosiOrgSet >> 16);
		AHB_LOG_DATA[59] = (uint8_t) (lsess32TgtPosiOrgSet >> 24);
	}
	else
	{
		AHB_LOG_DATA[56] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn);
		AHB_LOG_DATA[57] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn >> 8);
		AHB_LOG_DATA[58] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn >> 16);
		AHB_LOG_DATA[59] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlIdbMotPosnInfo.MotTarPosn >> 24);
	}
	AHB_LOG_DATA[60] = (uint8_t) (Msp_1msCtrlBus.Msp_1msCtrlMotRotgAg1msSigInfo.StkPosnMeasd);
	AHB_LOG_DATA[61] = (uint8_t) (Msp_1msCtrlBus.Msp_1msCtrlMotRotgAg1msSigInfo.StkPosnMeasd >> 8);
	AHB_LOG_DATA[62] = (uint8_t) (Msp_1msCtrlBus.Msp_1msCtrlMotRotgAg1msSigInfo.StkPosnMeasd >> 16);
	AHB_LOG_DATA[63] = (uint8_t) (Msp_1msCtrlBus.Msp_1msCtrlMotRotgAg1msSigInfo.StkPosnMeasd >> 24);

	/***************************************************************************************************/
	/*                                           LOG_BYTE8                                             */
	/***************************************************************************************************/
	if(Ses_CtrlBus.Ses_CtrlMotOrgSetStInfo.MotOrgSet == 0)
	{
		AHB_LOG_DATA[64] = (uint8_t) (Ses_CtrlBus.Ses_CtrlMotDqIRefSesInfo.IdRef);
		AHB_LOG_DATA[65] = (uint8_t) (Ses_CtrlBus.Ses_CtrlMotDqIRefSesInfo.IdRef >> 8);
		AHB_LOG_DATA[66] = (uint8_t) (Ses_CtrlBus.Ses_CtrlMotDqIRefSesInfo.IqRef);
		AHB_LOG_DATA[67] = (uint8_t) (Ses_CtrlBus.Ses_CtrlMotDqIRefSesInfo.IqRef >> 8);
	}
	else
	{
		AHB_LOG_DATA[64] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo.IdRef);
		AHB_LOG_DATA[65] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo.IdRef >> 8);
		AHB_LOG_DATA[66] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo.IqRef);
		AHB_LOG_DATA[67] = (uint8_t) (Mcc_1msCtrlBus.Mcc_1msCtrlMotDqIRefMccInfo.IqRef >> 8);
	}
	AHB_LOG_DATA[68] = (uint8_t) (lapcts16TargetPressOldlog);
	AHB_LOG_DATA[69] = (uint8_t) (lapcts16TargetPressOldlog >>8);
	AHB_LOG_DATA[70] = (uint8_t) (lmccs16WrpmMechRef);
	AHB_LOG_DATA[71] = (uint8_t) (lmccs16WrpmMechRef >> 8);

	/***************************************************************************************************/
	/*                                           LOG_BYTE9                                             */
	/***************************************************************************************************/
	AHB_LOG_DATA[72] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIdMeasd);
	AHB_LOG_DATA[73] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIdMeasd>>8);
	AHB_LOG_DATA[74] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIqMeasd);
	AHB_LOG_DATA[75] = (uint8_t) (Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIqMeasd>>8);
	AHB_LOG_DATA[76] = (uint8_t) (Acmio_SenVdcLink);
	AHB_LOG_DATA[77] = (uint8_t) (Acmio_SenVdcLink >> 8);
	AHB_LOG_DATA[78] = (uint8_t) (lapcts16DeltaStrokePerrorFinal);
	AHB_LOG_DATA[79] = (uint8_t) (lapcts16DeltaStrokePerrorFinal >> 8);
}
#endif /*IDB_LOG_DATA*/
#endif
