#ifndef __LCFM_struct_H__
#define __LCFM_struct_H__

struct  FM_STRUCT
{
	/*FM FLAG0*/	
    unsigned SameSideWSSErrDet 	:   1;   /* bit 7  */
	unsigned DiagonalWSSErrDet 	:   1;   /* bit 6  */
	unsigned WheelFLErrDet     	:   1;   /* bit 5  */
	unsigned WheelFRErrDet     	:   1;   /* bit 4  */
	unsigned WheelRLErrDet     	:   1;   /* bit 3  */
	unsigned WheelRRErrDet     	:   1;   /* bit 2  */
	unsigned SolenoidErrDet    	:   1;   /* bit 1  */
	unsigned VRErrDet          	:   1;   /* bit 0  */
	/*FM FLAG1*/	           	
	unsigned ECUHwErrDet       	:	1;   /* bit 7  */
	unsigned VoltageOverErrDet 	:   1;   /* bit 6  */
	unsigned VoltageLowErrDet  	:   1;   /* bit 5  */
	unsigned VCErrDet          	:   1;   /* bit 4  */
	unsigned MotorErrDet       	:   1;   /* bit 3  */
	unsigned VoltageUnderErrDet	:   1;   /* bit 2  */
	unsigned AxErrDet		   	:   1;   /* bit 1  */
	unsigned YawErrDet         	:	1;   /* bit 0  */
	/*FM FLAG2*/	           	
	unsigned MCPErrDet         	:	1;   /* bit 7  */		
	unsigned PedalErrDet       	:	1;   /* bit 6  */
	unsigned BLSErrDet         	:	1;   /* bit 5  */		
	unsigned AyErrDet          	:	1;   /* bit 4  */
	unsigned StrErrDet         	:	1;   /* bit 3  */	
	unsigned DBCRelayErrDet    	:   1;   /* bit 2  */
	unsigned GearRErrDet       	:   1;   /* bit 1  */
	unsigned ClutchErrDet	   	:	1;   /* bit 0  */
	/*FM FLAG3*/	
	unsigned WheelFLSusDet      :   1;   /* bit 7  */   		
	unsigned WheelFRSusDet      :   1; 	/* bit 6  */    
    unsigned WheelRLSusDet      :   1;   /* bit 5  */	 
	unsigned WheelRRSusDet      :   1;   /* bit 4  */	
	unsigned MCPSusDet          :	1;   /* bit 3  */
	unsigned StrSusDet          :	1;   /* bit 2  */
	unsigned YawSusDet          :	1;   /* bit 1  */
	unsigned AySusDet 		    :	1;   /* bit 0  */
	/*FM FLAG4*/	
	unsigned BlsSusDet          		:	1; 	 /* bit 7  */  
	unsigned FrontWSSErrDet             :   1;    /* bit 6  */ 
	unsigned RearWSSErrDet              :   1;    /* bit 5  */ 		 
	unsigned ClutchSwitch_error_flg     :   1;    /* bit 4  */	
	unsigned GearRSwitch_error_flg      :   1;    /* bit 3  */   		
    unsigned ESCEcuHWErrDet				:	1;    /* bit 2  */	
	unsigned ESCSensorErrDet    		:   1;    /* bit 1  */ 
	unsigned MCPErrorDet        		:   1;    /* bit 0  */	
	/*FM FLAG5*/
	unsigned ABSEcuHWErrDet             :   1;    /* bit 7  */ 
	unsigned ESCSwitchFail			    :   1;    /* bit 6  */
	unsigned FWDTimeOutErr	            :   1;	  /* bit 5  */
	unsigned TCUTimeOutErr  			:   1;    /* bit 4  */
	unsigned MainCanLineErr             :   1;    /* bit 3  */
	unsigned ESCSolErrDet               :   1;    /* bit 2  */    
	unsigned DelayedBrakeFluidLevel     :   1;    /* bit 1  */    
	unsigned LatchedBrakeFluidLevel     :   1;    /* bit 0  */    
	/*FM FLAG6*/
	unsigned StrErrorDet                :   1;	  /* bit 7  */ 
	unsigned RearSolErr					:	1;    /* bit 6  */
	unsigned EMSTimeOutErrDet           :   1;    /* bit 5  */ 
	unsigned BrkAppSenInvalid			:	1;	  /* bit 4  */
	unsigned SAS_CHECK_OK/*init_end_flg*/               :   1;    /* bit 3  */   		
	unsigned u1HSA_error_flg			:	1;    /* bit 2  */
	unsigned u1BackDir                  :   1;    /* bit 1  */
	unsigned not_used_flag_0            :   1;    /* bit 0  */		
		
	/*FM FLAG7*/
	unsigned u1ebd_error_flg			:	1;    /* bit 7  */ 
	unsigned u1abs_error_flg            :	1;    /* bit 6  */ 
	unsigned u1hev_error_flg            :	1;    /* bit 5  */ 
	unsigned u1edc_error_flg            :	1;    /* bit 4  */ 
	unsigned u1btcs_error_flg           :	1;    /* bit 3  */ 
	unsigned u1vdc_error_flg			: 	1;    /* bit 2  */ 
	unsigned not_used_flag_71           : 	1;    /* bit 1  */  
	unsigned not_used_flag_70           : 	1;    /* bit 0  */	
		
	unsigned YawSenPwr1sOk 				:   1;    /* bit 7  */
	unsigned McpSenPwr1sOk      		:	1;    /* bit 6  */		            
	unsigned AySenPwr1sOk       		:	1;    /* bit 5  */
	unsigned SteerSenPwr1sOk    		:	1;    /* bit 4  */
	unsigned MpressOffsetOK				: 	1;	  /* bit 3  */		
	unsigned not_used_flag_81           :   1;    /* bit 1  */  
	unsigned not_used_flag_80           :   1;    /* bit 0  */	
};



#endif