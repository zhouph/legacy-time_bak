#ifndef __ARBIT_struct_H__
#define __ARBIT_struct_H__

#include "ASW_Base_Type.h"

struct  WL_ARBIT_STRUCT
{
    
    unsigned u1HV_VL:                   1;
    unsigned u1AV_VL:                   1;
    unsigned u1WL_CONTROL  :            1;
    unsigned u1REAR_WHEEL:              1;
    unsigned u1LEFT_WHEEL:              1;
    
    unsigned u1ABS:                     1;
    unsigned u1L_SLIP_CONTROL:          1;
    unsigned u1BUILT:                   1;
    unsigned u1LFC_built_reapply_flag:  1;
	    
    uint8_t pwm_duty_temp_dash;
    uint8_t LFC_Conven_Reapply_Time;
    uint8_t on_time_outlet_pwm;
    uint8_t lau8NCPwmDuty;
};

struct  CIRCUIT_ARBIT_STRUCT                    
{                                          
                                           
    unsigned u1TCL_PRIMARY:							1;					/*FR-split : FR*/
    unsigned u1TCL_SECONDARY:						1;					/*FR-split : FL*/  
    unsigned u1ESV_PRIMARY:							1;					/*FR-split : FR*/
    unsigned u1ESV_SECONDARY:						1;					/*FR-split : FL*/
    unsigned u1CONTROL:									1;           
                                           
    uint16_t TC_MFC_Current_Primary;            
    uint16_t TC_MFC_Current_Secondary;            
};


#endif
