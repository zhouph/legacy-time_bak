#ifndef ASW_BASE_TYPE_H_
#define ASW_BASE_TYPE_H_

//#include "../../SYSTEM/common.h"


typedef unsigned int		u1ClBitType;

/************************************
typedef unsigned short int16_t  uint16_t;
typedef unsigned long int16_t   uint32_t;
typedef unsigned long long  uint64_t;
 

typedef short int16_t   int16_t;
typedef long int16_t    int32_t;
typedef long long   int64_t;
*************************************/
struct asw_bit_position 
{
    u1ClBitType bit07    :1;
    u1ClBitType bit06    :1;
    u1ClBitType bit05    :1;
    u1ClBitType bit04    :1;
    u1ClBitType bit03    :1;
    u1ClBitType bit02    :1;
    u1ClBitType bit01    :1;
    u1ClBitType bit00    :1;

    u1ClBitType bit17    :1;
    u1ClBitType bit16    :1;
    u1ClBitType bit15    :1;
    u1ClBitType bit14    :1;
    u1ClBitType bit13    :1;
    u1ClBitType bit12    :1;
    u1ClBitType bit11    :1;
    u1ClBitType bit10    :1;

    u1ClBitType bit27    :1;
    u1ClBitType bit26    :1;
    u1ClBitType bit25    :1;
    u1ClBitType bit24    :1;
    u1ClBitType bit23    :1;
    u1ClBitType bit22    :1;
    u1ClBitType bit21    :1;
    u1ClBitType bit20    :1;

    u1ClBitType bit37    :1;
    u1ClBitType bit36    :1;
    u1ClBitType bit35    :1;
    u1ClBitType bit34    :1;
    u1ClBitType bit33    :1;
    u1ClBitType bit32    :1;
    u1ClBitType bit31    :1;
    u1ClBitType bit30    :1;
};

#if 0
#define     T_10_MS             1
#define     T_20_MS				2      
#define     T_30_MS             3  
#define     T_50_MS             5        
#define     T_100_MS            10
#define     T_130_MS            13
#define     T_150_MS            15
#define     T_200_MS            20
#define     T_250_MS            25
#define     T_300_MS            30
#define     T_350_MS            35
#define     T_400_MS            40
#define     T_450_MS            45
#define     T_500_MS            50
#define     T_550_MS            55
#define     T_600_MS            60
#define     T_700_MS            70
#define     T_800_MS            80
#define     T_900_MS            90
#define     T_1000_MS           100
#define     T_1500_MS           150
#define     T_1750_MS           175 
#define     T_2_S               200
#define     T_3_S               300
//#define     T_4S                400
#define     T_4S1               410
#define     T_4S2               420
#define     T_4S3               430
#define     T_4S4               440
#define     T_4S5               450
#define     T_5_S               500
#define     T_10_S              1000
#define     T_20_S              2000
#define     T_1MIN              6000
#define     T_3MIN              18000
#define     T_5MIN              30000
#define     T_7MIN              42000
#define     T_30MIN             180000
#define     T_4_S               400  
#define     T_8_S 	            800 
#define     T_12_S	            1200 
#define     T_16_S	            1600 
#define     T_20_S	            2000 
#define     T_24_S	            2400 
#define     T_28_S	            2800 
#define     T_32_S	            3200 
#define     T_36_S	            3600 
#define     T_40_S	            4000 
#define     T_44_S	            4400 
#define     T_48_S	            4800 
#define     T_52_S	            5200
#define     T_56_S	            5600
#define     T_60_S	            6000
#define     T_64_S	            6400
#define     T_68_S	            6800
#define     T_72_S	            7200
#define     T_76_S	            7600
#define     T_80_S	            8000
#define     T_84_S	            8400
#define     T_88_S	            8800
#define     T_92_S	            9200
#define     T_96_S	            9600
#define     T_100_S	            10000

/**************** VOLTAGE [ MV] ****************/
#define MCU_AD_RESOL            12      // 12bit
#define AD_REF_5V               5000

#define VOLT_TO_AD_TRANS(VOLT,R1,R2) (uint16_t)((uint32_t)((VOLT)*(2^MCU_AD_RESOL)*(R2))/(((R1)+(R2))*AD_REF_5V))  

#define MOTOR_AD_TRANS(VOLT)    VOLT_TO_AD_TRANS(VOLT,300,100)
#define MOTOR_12V               MOTOR_AD_TRANS(12000)
#define MOTOR_11V               MOTOR_AD_TRANS(11000)

#define FSR_AD_TRANS(VOLT)      VOLT_TO_AD_TRANS(VOLT,300,51)
#define FSR_16V                 FSR_AD_TRANS(16000)
#define FSR_9V0                 FSR_AD_TRANS(9000)

#define IGN_AD_TRANS(VOLT)      FSR_AD_TRANS(VOLT)
#define IGN_16V                 IGN_AD_TRANS(16000)
#define IGN_9V0                 IGN_AD_TRANS(9000)

#endif

/************************************************/
/*
typedef struct{
	short int S16_STOP_STATE_CONV_COEFFIC_F;
	short int S16_STOP_STATE_CONV_COEFFIC_R;
	short int S16_WHEEL_RADIUS;
} AswCalBody_t;
AswCalBody_t AswCalBody;
extern AswCalBody_t AswCalBody;

#define S16_STOP_STATE_CONV_COEFFIC_F                              AswCalBody.S16_STOP_STATE_CONV_COEFFIC_F                        
#define S16_STOP_STATE_CONV_COEFFIC_R                          AswCalBody.S16_STOP_STATE_CONV_COEFFIC_R                    
#define S16_WHEEL_RADIUS                         AswCalBody.S16_WHEEL_RADIUS   
*/

#endif /* ASW_BASE_TYPE_H_ */

