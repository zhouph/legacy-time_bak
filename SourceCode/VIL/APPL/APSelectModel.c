
#include "../_PAR/car/Car_Def.h"
#include "../_PAR/car/Car.par.h"

#if __CAR==KMC_TD
	#include "../APPL/TD/Model/APModelTdEsp2WD.h"
#elif __CAR==HMC_BH
	#include "../APPL/BH/Model/APModelBhEsp2WD.h"
#elif __CAR==HMC_HGE
    #include "../APPL/HGE/Model/APModelHgeEsp2WD.h"
#elif __CAR==HMC_YFE
    #include "../APPL/YFE/Model/APModelYfeEsp2WD.h"
#elif __CAR==KMC_TFE
    #include "../APPL/TFE/Model/APModelTfeEsp2WD.h"
#endif

