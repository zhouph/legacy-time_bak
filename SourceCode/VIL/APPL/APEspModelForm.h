#ifndef __APESPMODELFORM_H__
#define __APESPMODELFORM_H__

#include "../../ASW/include/Mando_Std_Types.h"
#include "../HDR/ASW_Base_Type.h"

typedef struct
{
    int16_t Yc_Vx[12];
    int16_t Yc_St[13];
    int16_t Yc_Map[15][12];
    int16_t S16_GAIN_BETA_D_BETA;
    uint16_t U16_GAIN_BETA_D_YAW;	
    int16_t S16_GAIN_BETA_D_DELTA;	
    int16_t S16_GAIN_YAW_D_BETA;	
    int16_t S16_GAIN_YAW_D_YAW;		
    int16_t S16_GAIN_YAW_D_DELTA;	
    int16_t S16_LAT_G_CAL_SCALE;	
    int16_t S16_YAWC_FILTER_20;		
    int16_t S16_YAWC_FILTER_40;		
    int16_t S16_YAWC_FILTER_60;		
    int16_t S16_LAT_G_FILTER_20;	
    int16_t S16_LAT_G_FILTER_40;	
    int16_t S16_LAT_G_FILTER_60;	
    int16_t S16_LAT_G_FILTER_80;	
    int16_t S16_LAT_G_FILTER_100;	
    int16_t S16_LAT_G_FILTER_120;	
    int16_t S16_LAT_G_FILTER_150;
    int16_t S16_STEERING_RATIO_10;	
    int16_t S16_CORNER_STIFF_FRONT;
    int16_t S16_CORNER_STIFF_REAR;	
    int16_t S16_LENGTH_CG_TO_FRONT;
    int16_t S16_LENGTH_CG_TO_REAR;	
    int16_t S16_VEHICLE_MASS_FL;
    int16_t S16_VEHICLE_MASS_FR;
    int16_t S16_VEHICLE_MASS_RL;
    int16_t S16_VEHICLE_MASS_RR;
    int16_t S16_MOMENT_OF_INERTIA;
    int16_t S16_WHEEL_TREAD_FRONT;
    int16_t S16_WHEEL_TREAD_REAR;   
    int16_t S16_UNDERSTEER_GRADIENT;
    int16_t S16_MAX_ROAD_ADHESION;
    int16_t S16_DELTA_STEER_FOR_BANK;
    int16_t S16_LAT_G_EXTRA_40;		
    int16_t S16_LAT_G_EXTRA_60;	
    int16_t S16_LAT_G_EXTRA_80;		
    int16_t S16_LAT_G_EXTRA_100;	
    int16_t S16_LAT_G_EXTRA_120;
    int16_t S16_LAT_G_EXTRA_150;
    int16_t S16_ADM_VCH_NOMINAL;
    int16_t S16_STR_1st_Point;	
    int16_t S16_STR_2nd_Point;	
    int16_t S16_STR_3rd_Point;			
    int16_t S16_STR_4th_Point;		
    int16_t S16_STR_5th_Point;		
    int16_t S16_STEERING_MAX_DEG;		
    int16_t S16_STR_RATIO_10_1st;
    int16_t S16_STR_RATIO_10_2nd;
    int16_t S16_STR_RATIO_10_3rd;
    int16_t S16_STR_RATIO_10_4th;
    int16_t S16_STR_RATIO_10_5th;
    int16_t S16_STR_RATIO_10_MAX;
    int16_t CW_S16_STR_RATIO_10_1st;
    int16_t CW_S16_STR_RATIO_10_2nd;	
    int16_t CW_S16_STR_RATIO_10_3rd;			
    int16_t CW_S16_STR_RATIO_10_4th;		
    int16_t CW_S16_STR_RATIO_10_5th;		
    int16_t CW_S16_STR_RATIO_10_MAX;
    int16_t S16_BANK_ALAT_Roll_Gain;
    int16_t S16_Yawm_Filter_Gain_60kph;
    int16_t S16_Yawm_Filter_Gain_90kph;
    int16_t S16_Yawm_Filter_Gain_120kph;
    int16_t S16_Yawm_Filter_Gain_150kph;
}ESP_MODEL_t;

extern ESP_MODEL_t *pEspModel;

#endif
