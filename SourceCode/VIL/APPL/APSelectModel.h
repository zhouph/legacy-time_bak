#ifndef __APSELECTMODEL_H__
#define __APSELECTMODEL_H__

#include "APEspModelForm.h"

/*Model parameters*/
/* #pragma CONST_SEG LOGIC_ESP_MODEL_0 */
extern /*const*/ ESP_MODEL_t  apEspModel2WD;
/* #pragma CONST_SEG LOGIC_ESP_MODEL_1 */
extern /*const*/ ESP_MODEL_t  apEspModel4WD_2H;
/* #pragma CONST_SEG LOGIC_ESP_MODEL_2 */
extern /*const*/ ESP_MODEL_t  apEspModel4WD_4H;
/* #pragma CONST_SEG LOGIC_ESP_MODEL_3 */
extern /*const*/ ESP_MODEL_t  apEspModel4WD_AUTO;
/* #pragma CONST_SEG DEFAULT */


#endif

