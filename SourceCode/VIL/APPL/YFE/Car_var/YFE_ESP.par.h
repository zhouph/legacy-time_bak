/*
*******************************************************************************
+------------------------------------------------------------------------------
| Directory HMC
+------------------------------------------------------------------------------
| Directory HMC
| Filename  DH_ESP.par
| Project   MGH-80 ESC
+------------------------------------------------------------------------------
*******************************************************************************
=> Revision Note
-------------------------------------------------------------------------------
*******************************************************************************
*/
/******************************************************************************
* Discription : Selectable Factors for Car variant MACROs
*******************************************************************************
*   __TCMF_ENABLE                   	__ESP_PLUSE only
*   __4WD_VEHICLE                   	ENABLE  or DISABLE
*   __DRIVE_TYPE                    	FRONT_WHEEL_DRV or REAR_WHEEL_DRV
*   __VALVE_SPLIT_TYPE            	    X_SPLIT_FR_SWAP / X_SPLIT / FR_SWAP
*   __SMART_WHEEL_SENSOR            	ENABLE  or DISABLE
*   __SMART_RL_INSTALLED
*   __SMART_RR_INSTALLED
*   __SMART_RL_FORWARD_DIRECTION    	DR_RIGHT or DR_LEFT
*   __SMART_RR_FORWARD_DIRECTION    	DR_RIGHT or DR_LEFT
*   __FL_PRESS                      	ENABLE  or DISABLE
*   __FR_PRESS                      	ENABLE  or DISABLE
*   __RL_PRESS                      	ENABLE  or DISABLE
*   __RR_PRESS                      	ENABLE  or DISABLE
*   __VSO_TYPE                      	SW0_GEN_TYPE or HARD_WIRE_TYPE
*   __VSO_WHEEL_OUT_SEL             	GM_VSO_WHEEL or HMC_VSO_WHEEL or ALL_WHEEL
*   __STEER_SENSOR_TYPE             	NONE 		or ANALOG_TYPE  or CAN_TYPE
*   __YAW_SENSOR_TYPE               	NONE    or ANALOG_TYPE  or CAN_TYPE
*   __YAW_SENSOR_DIRECTION          	FORWARD or BACKWARD
*   __AY_AX_SWAP                    	ENABLE  or DISABLE
*   __MP_SENSOR_TYPE                	NONE    or _200BAR  		or 170BAR
*   __G_SENSOR_TYPE                 	NONE    or ANALOG_TYPE  or CAN_TYPE
*   __G_SENSOR_DIRECTION            	FORWARD or BACKWARD
*   __G_SEN_EOL_OFFS_CORR           	ENABLE  or DISABLE
*   __G_SEN_DRV_OFFS_CORR							ENABLE  or DISABLE
*   __VACUUM_SENSOR_ENABLE          	ENABLE  or DISABLE
*   __VACUUM_PUMP_RELAY		          	ENABLE  or DISABLE
*   __BRAKE_LAMP_RELAY_ENABLE       	ENABLE  or DISABLE
*   __HDC_SW_TYPE                   	NONE    or ANALOG_TYPE  or CAN_TYPE
*   __AVH_SW_TYPE											NONE    or ANALOG_TYPE  or CAN_TYPE
*   __AVSM_SWITCH_TYPE 								NONE    or ANALOG_TYPE  or CAN_TYPE
*   __BRAKE_FLUID_LEVEL             	ENABLE  or DISABLE
*   __BLS_TYPE                      	NONE    or ANALOG_TYPE  or CAN_TYPE
*   __GEAR_SWITCH_TYPE              	NONE    or ANALOG_TYPE  or CAN_TYPE
*   __CLUTCH_SWITCH_TYPE            	NONE    or ANALOG_TYPE  or CAN_TYPE
*   __PARK_BRAKE_TYPE             	  NONE    or ANALOG_TYPE  or CAN_TYPE
*   __DIAG_TYPE                     	K_LINE  or CAN_LINE
*   __HMC_CAN_VER                   	CAN_1_0 or CAN_1_1  or CAN_1_2 or CAN_1_3 or CAN_1_4
*   HMC_DUAL_CAN											ENABLE  or DISABLE
*   GMLAN_ENABLE                   		ENABLE  or DISABLE
*   __LAMP_DRV_TYPE                	  NONE    or ANALOG_TYPE  or CAN_TYPE
*   __ABS_LAMP_TYPE                 	SEPERATE or COMMON
*   __FUNC_LAMP_ENABLE           	    ENABLE  or DISABLE
*   __HDC_LAMP_ENABLE            	    ENABLE  or DISABLE
*   __ESC_OFF_SWITCH_TYPE             ANALOG_TYPE or CAN_TYPE
*   __ESC_OFF_LAMP_ENABLE             ENABLE  or DISABLE
*   __ESC_WARNING_LAMP_DRV_TYPE       PASSIVE or ACTIVE
*   __ESC_OFF_LAMP_DRV_TYPE        		PASSIVE or ACTIVE
*   __AVH_PREWARNING_ENABLE						ENABLE  or DISABLE
*   __FMVSS126_ESC_ECE              	ENABLE  or DISABLE
*   __CUSTOMER_DTC                  	GM_DTC  or HMC_DTC
*   __EBD                          		ENABLE  or DISABLE
*   __ABS                          		ENABLE  or DISABLE
*   __BTC                          		ENABLE  or DISABLE
*   __TCS                          		ENABLE  or DISABLE
*   __VDC                          		ENABLE  or DISABLE
*   __HEV                          		ENABLE  or DISABLE
*   __HSA                          		ENABLE  or DISABLE
*   __HDC                          		ENABLE  or DISABLE
*   __ROP                          		ENABLE  or DISABLE
*   __VES                          		ENABLE  or DISABLE
*   __EDC                          		ENABLE  or DISABLE
*   __CBC                          		ENABLE  or DISABLE
*   __FBC                          		ENABLE  or DISABLE
*   __BDW                          		ENABLE  or DISABLE
*   __EBP                          		ENABLE  or DISABLE
*   __ACC                          		ENABLE  or DISABLE
*   __TSP                          		ENABLE  or DISABLE
*   __HBB                          		ENABLE  or DISABLE
*   __HRB                           	ENABLE  or DISABLE
*   __TOD            	              	ENABLE  or DISABLE
*   __SLS                          		ENABLE  or DISABLE
*   __SCC                          		ENABLE  or DISABLE
*   __RBC                          	  	ENABLE  or DISABLE
*   __VSM                          		ENABLE  or DISABLE
*   __SPAS								ENABLE  or DISABLE
*   __AVH                           	ENABLE  or DISABLE
*   __RWD_SPORTSMODE     				SPORTMODE_1 or SPORTMODE_2 or DISABLE
*   __TVBB              				ENABLE	or DISABLE
*   __SPAS_INTERFACE                	ENABLE	or DISABLE
*   __CDM                        			ENABLE  or DISABLE
*   __LVBA                          	ENABLE  or DISABLE
*   __IVSS                         		ENABLE  or DISABLE
*   __CRAM                      			ENABLE	or DISABLE
*   __RAR                      				ENABLE	or DISABLE
*   __DDS															ENABLE	or DISABLE
*   __DDS_PLUS												ENABLE	or DISABLE
*   __DDS_RESET_BY_ESC_SW 	   				ENABLE	or DISABLE
*   __DDS_FS_SIG											ENABLE	or DISABLE
*   __IGNOFF_WSS_OUT									ENABLE	or DISABLE
*   __IGNOFF_WSS_OUT									ENABLE	or DISABLE
*		__EPB_INTERFACE										ENABLE	or DISABLE
*   EPB_VARIANT_ENABLE								ENABLE	or DISABLE
*   HSA_VARIANT_ENABLE								ENABLE	or DISABLE
*   AX_VARIANT_ENABLE									ENABLE	or DISABLE
*   YAW_SNSR_ACU_TYPE									ENABLE	or DISABLE
*   __YAW_SNSR_ACU_CAN_VER                              CAN_MD  or CAN_HMC
*   AVH_INLINE_ENABLE									ENABLE	or DISABLE
*   ESS_INLINE_ENABLE									ENABLE	or DISABLE
*   SPAS_INLINE_ENABLE								ENABLE	or DISABLE
*   EPB_INLINE_ENABLE									ENABLE	or DISABLE
*   HSA_INLINE_ENABLE									ENABLE	or DISABLE
*   __ESS															ENABLE	or DISABLE
*   __ESS_EXTENDED_FN									ENABLE	or DISABLE
*   __HAZARD_SWITCH_TYPE							ENABLE	or DISABLE
*   __ESS_RELAY_ENABLE								ENABLE	or DISABLE
*   __ESS_HDC_RELAY_PARALLEL					ENABLE	or DISABLE
*   __ESP_DISABLE 										ENABLE	or DISABLE
*   __TCS_DISABLE											ENABLE	or DISABLE
*   __TC0_PORT                      	WFL or WFR or WRL or WRR
*   __TC1_PORT                      	WFL or WFR or WRL or WRR
*   __TC2_PORT                      	WFL or WFR or WRL or WRR
*   __TC3_PORT                      	WFL or WFR or WRL or WRR
*   __NM_SLEEP_ENABLE									ENABLE	or DISABLE
*   GM_APPL_SW_PN
*   __SPARE_WHEEL                  		ENABLE  or DISABLE
*   __PEDAL_SENSOR_TYPE								NONE    or ANALOG_TYPE  or CAN_TYPE
*   __PEDAL_SENSOR_SWAP								ENABLE  or DISABLE
*   __4WD_VARIANT_CODE								ENABLE  or DISABLE
*   __USE_PDT_SIGNAL									ENABLE  or DISABLE
*   __USE_PDF_SIGNAL									ENABLE  or DISABLE
*   SYS_PEDAL_SENSOR_MAX_STOKE
*   PedalStrokePDTMaxDepthPercent
*   PedalStrokePDTMinDepthPercent
*   PedalStrokePDFMaxDepthPercent
*   PedalStrokePDFMinDepthPercent
*   TRACK_WIDTH_F
*   TRACK_WIDTH_R

******************************************************************************/

#define __TCMF_ENABLE               __ESP_PLUSE

#define __4WD_VEHICLE               DISABLE

/* 4WD Variant Coding */
#define __4WD_VARIANT_CODE       	DISABLE  /* disable for Matlab compile */
#if __4WD_VARIANT_CODE == ENABLE
    #define __4WD                   DISABLE /* Fixed */
#else
    #define __4WD                   DISABLE /* Enable/Disable setting */
#endif

#define __DRIVE_TYPE                FRONT_WHEEL_DRV

/* HECU  specification */
#define __VALVE_SPLIT_TYPE          X_SPLIT

/* Smart Wheel Sensor selection */
#define __SMART_WHEEL_SENSOR        DISABLE
#if __SMART_WHEEL_SENSOR == ENABLE
#define __SMART_RL_INSTALLED
#define __SMART_RR_INSTALLED
#define __SMART_RL_FORWARD_DIRECTION    DR_RIGHT  /*전자팀 확인 요망*/
#define __SMART_RR_FORWARD_DIRECTION    DR_RIGHT
#define __4WD_SMART_RL_FORWARD_DIRECTION	DR_LEFT
#define __4WD_SMART_RR_FORWARD_DIRECTION	DR_RIGHT
#endif

/* Wheel Pressure */
#define __FL_PRESS                  DISABLE
#define __FR_PRESS                  DISABLE
#define __RL_PRESS                  DISABLE
#define __RR_PRESS                  DISABLE

/*VSO Specification*/
#define __VSO_TYPE                  HARD_WIRE_TYPE   /*4000pulse/mile, HARD_WIRE_TYPE*/
#define __VSO_WHEEL_OUT_SEL         WFR              /* VSO1(#17) OUTPUT Select */

/* Steer */
#define __STEER_SENSOR_TYPE         CAN_TYPE    /* NONE or ANALOG_TYPE or CAN_TYPE */

/* Yaw */
#define __YAW_SENSOR_TYPE           CAN_TYPE    /* NONE or ANALOG_TYPE or CAN_TYPE */
#define __YAW_SENSOR_DIRECTION      BACKWARD		/* 전자팀 사양 출처 확인요망*/
#define __AY_AX_SWAP                DISABLE


/* G-Sensor */
#define __G_SENSOR_ENABLE           ENABLE      // temp
#define __G_SENSOR_TYPE             CAN_TYPE    /* NONE or ANALOG_TYPE or CAN_TYPE */
#define __G_SENSOR_DIRECTION        BACKWARD
#define __G_SEN_EOL_OFFS_CORR 		DISABLE/*ENABLE*/		/*If G_Sensor Offset Correction is applied in EO */
#define __G_SEN_DRV_OFFS_CORR 		DISABLE		/*If G_Sensor Offset Correction is applied in driving condition*/

/* Vacuum */
#define __VACUUM_SENSOR_ENABLE      DISABLE     /* ENABLE or DISABLE */
#define __VACUUM_PUMP_RELAY         DISABLE     /* ENABLE or DISABLE */

/*Brake lamp relay*/
#define __BRAKE_LAMP_RELAY_ENABLE   DISABLE		/* ENABLE or DISABLE */	/* IDB 최초 버전 없음 */
#define __BRAKE_SWITCH_ENABLE       DISABLE//ENABLE      /* ENABLE or DISABLE */ /*KYB Temp*/

/* SWITCH specification */
#define __HDC_SW_TYPE		        NONE//ANALOG_TYPE      	/* NONE or ANALOG_TYPE or CAN_TYPE */
#define __AVH_SW_TYPE		        NONE /* NONE or ANALOG_TYPE or CAN_TYPE */
#define __AVSM_SWITCH_TYPE	        NONE        /* NONE or ANALOG_TYPE or CAN_TYPE */
#define __BRAKE_FLUID_LEVEL	        DISABLE   	/* ENABLE or DISABLE */
#define __BLS_TYPE		            ANALOG_TYPE//ANALOG_TYPE /* NONE or ANALOG_TYPE or CAN_TYPE */
#define __GEAR_SWITCH_TYPE	        CAN_TYPE        /* NONE or ANALOG_TYPE or CAN_TYPE */
#define __CLUTCH_SWITCH_TYPE        NONE            /* NONE or ANALOG_TYPE or CAN_TYPE */
#define __PARK_BRAKE_ENABLE         ENABLE /*FOR compile */
#define __PARK_BRAKE_TYPE	        ANALOG_TYPE	/* NONE or ANALOG_TYPE or CAN_TYPE */

/* Communication specification */
#define __DIAG_TYPE		            CAN_LINE
#define __HMC_CAN_VER		        CAN_YF_HEV//CAN_YF_HEV
#define HMC_DUAL_CAN		        DISABLE
#define GMLAN_ENABLE		        DISABLE

/* Lamp spcification */
#define __LAMP_DRV_TYPE			    CAN_TYPE    /* NONE or ANALOG_TYPE or CAN_TYPE */
#define __ABS_LAMP_TYPE			    COMMON
#define __FUNC_LAMP_ENABLE		    ENABLE
#define __HDC_LAMP_ENABLE		    DISABLE
#define __ESC_OFF_SWITCH_TYPE		ANALOG_TYPE    /* NONE or ANALOG_TYPE or CAN_TYPE */
#define __ESC_OFF_LAMP_ENABLE		ENABLE
#define __ESC_WARNING_LAMP_DRV_TYPE	ACTIVE
#define __ESC_OFF_LAMP_DRV_TYPE		ACTIVE
#define __AVH_PREWARNING_ENABLE	    DISABLE
#define __SSEM_RELAY_ENABLE         ENABLE

/* FMVSS126 spcification */
#define __FMVSS126_ESC_ECE          ENABLE

/* DTC specification */
#define __CUSTOMER_DTC              HMC_DTC

/* BF selection */
#define __EBD                       ENABLE
#define __ABS                       ENABLE
#define __BTC                       ENABLE
#define __TCS                       ENABLE
#define __VDC                       ENABLE

/* VAF specification */
#define __HEV                       DISABLE
#define __HSA                       DISABLE
#define __HDC                       DISABLE
#define __ROP                       DISABLE
#define __VES                       DISABLE
#define __EDC                       DISABLE
#define __CBC                       DISABLE
#define __FBC		               	DISABLE
#define __BDW		               	DISABLE
#define __EBP		              	DISABLE
#define __ACC		              	DISABLE
#define __TSP		              	DISABLE
#define __HBB		              	DISABLE
#define __HRB		              	DISABLE
#define __TOD		             	DISABLE
#define __SLS   		         	DISABLE
#define __SCC		             	DISABLE
#define __RBC		             	ENABLE
#define __VSM	              		DISABLE
#define __SPAS              		DISABLE
#define __AVH		            	DISABLE
#define __RWD_SPORTSMODE            DISABLE
#define __TVBB			    		DISABLE
#define __SPAS_INTERFACE			DISABLE
#define __CDM		           		DISABLE
#define __LVBA		         	    DISABLE
#define __IVSS                      DISABLE
#define __CRAM                      DISABLE
#define __RAR                       DISABLE
#define __DDS						                DISABLE
#define __DDS_PLUS					            	DISABLE
#define __DDS_RESET_BY_ESC_SW 	        			DISABLE
//#define __DDS_FS_SIG				DISABLE
#define __IGNOFF_WSS_OUT            DISABLE
#define __ADAPTIVE_MODEL_VCH_GEN    DISABLE
#define __ADAPTIVE_MODEL_VELO       DISABLE

/* EPB Interface*/
#define __EPB_INTERFACE               				DISABLE
#define EPB_VARIANT_ENABLE			  				DISABLE

/* HSA Variant Code */
#define HSA_VARIANT_ENABLE          					DISABLE
#if (HSA_VARIANT_ENABLE == ENABLE)
   #define AX_VARIANT_ENABLE        ENABLE
#endif

/*BLS Fail, No Warning Lamp*/
#define __BLS_NO_WLAMP_FM			ENABLE

/* ACU Yaw&G */
#define YAW_SNSR_ACU_TYPE           DISABLE
#define __YAW_SNSR_ACU_CAN_VER      CAN_MD      /* CAN_MD or CAN_HMC */
/* Inline Coding */
//  #define AVH_INLINE_ENABLE			DISABLE
//  #define ESS_INLINE_ENABLE			DISABLE
//  #define SPAS_INLINE_ENABLE			DISABLE
//  #define HSA_INLINE_ENABLE			DISABLE
//  #define EPB_INLINE_ENABLE			DISABLE

#define __ESS                       DISABLE
#define __ESS_EXTENDED_FN           DISABLE
#define __HAZARD_SWITCH_TYPE        CAN_TYPE
#define __ESS_RELAY_ENABLE          DISABLE
#define __ESS_HDC_RELAY_PARALLEL    DISABLE

/* Function Activation Setting */
#define  __ESP_DISABLE              DISABLE
#define  __TCS_DISABLE              DISABLE

/* Wheel Sensor Port Setting */
#define __TC0_PORT                  WFR
#define __TC1_PORT                  WFL
#define __TC2_PORT                  WRR
#define __TC3_PORT                  WRL

/* Communication specification */
#define __NM_SLEEP_ENABLE			DISABLE
#define GM_APPL_SW_PN				0

/*Pedal Travel Sensor*/
#define __PEDAL_SENSOR_TYPE         ANALOG_TYPE /* NONE or ANALOG_TYPE or CAN_TYPE */

/* Pedal Calc Parameter */
#if __PEDAL_SENSOR_TYPE==ANALOG_TYPE
    #define __USE_PDT_SIGNAL	    ENABLE
	#if __IDB_SYSTEM == DISABLE
    #define __USE_PDF_SIGNAL        ENABLE
	#else
	#define __USE_PDF_SIGNAL        DISABLE		/* IDB PDF 미사용 */
	#endif
    #define SYS_PEDAL_SENSOR_MAX_STOKE          130
    #define PedalStrokePDTMaxDepthPercent       820
    #define PedalStrokePDTMinDepthPercent       150
    #define PedalStrokePDFMaxDepthPercent       410
    #define PedalStrokePDFMinDepthPercent       75
#endif

