#include "../../APEspModelForm.h"

//#pragma CONST_SEG LOGIC_ESP_MODEL_0
const ESP_MODEL_t apEspModel2WD=
{
/*const 	INT Yc_Vx[12] =*/ {0,100,200,300,400,600,800,1000,1200,1400,2000,5000},
/*const 	INT Yc_St[13] =*/ {0,50,150,300,450,600,900,1200,1800,2700,3600,4500,5400},

/* const 	INT Yc_Map[15][12] =*/
{{10130,10130,10220,10170,9710,8890,8070,7460,7460,7460,7460,7460},
{10130,10130,10220,10170,9710,8890,8070,7460,7460,7460,7460,7460},
{10130,10130,10220,10170,9710,8890,8070,7460,7460,7460,7460,7460},
{10130,10130,10220,10170,9710,8890,8870,8500,8500,8500,8500,8500},
{10130,10130,10220,10170,9710,9390,8750,7910,7910,7910,7910,7910},
{10130,10130,10220,10170,9850,9250,8780,8330,7910,7910,7910,7910},
{10130,10130,10220,10170,10020,9070,8780,8330,7910,7910,7910,7910},
{10130,10130,10220,10170,9900,9070,8780,8330,7910,7910,7910,7910},
{10390,10390,10350,10230,9940,9940,9940,9940,9940,9940,9940,9940},
{10820,10820,10720,10160,10160,9940,9940,9940,9940,9940,9940,9940},
{11270,11270,11010,11010,11010,11010,11010,11010,11010,11010,11010,11010},
{12140,12140,11410,11410,11410,11010,11010,11010,11010,11010,11010,11010},
{12800,12800,11670,11670,11670,11670,11670,11670,11670,11670,11670,11670},
{12800,12800,11670,11670,11670,11670,11670,11670,11670,11670,11670,11670},
{12800,12800,11670,11670,11670,11670,11670,11670,11670,11670,11670,11670},},
/*S16_GAIN_BETA_D_BETA*/-2417,
/*U16_GAIN_BETA_D_YAW*/27846,
/*S16_GAIN_BETA_D_DELTA*/9249,
/*S16_GAIN_YAW_D_BETA*/23,
/*S16_GAIN_YAW_D_YAW*/-3827,
/*S16_GAIN_YAW_D_DELTA*/327,
/*S16_LAT_G_CAL_SCALE*/105,
/*S16_YAWC_FILTER_20*/70,
/*S16_YAWC_FILTER_40*/70,
/*S16_YAWC_FILTER_60*/70,
/*S16_LAT_G_FILTER_20*/45,
/*S16_LAT_G_FILTER_40*/45,
/*S16_LAT_G_FILTER_60*/55,
/*S16_LAT_G_FILTER_80*/71,
/*S16_LAT_G_FILTER_100*/90,
/*S16_LAT_G_FILTER_120*/128,
/*S16_LAT_G_FILTER_150*/128,
/*S16_STEERING_RATIO_10*/170,
/*S16_CORNER_STIFF_FRONT*/-1169,
/*S16_CORNER_STIFF_REAR*/-1887,
/*S16_LENGTH_CG_TO_FRONT*/1413,
/*S16_LENGTH_CG_TO_REAR*/1522,
/*S16_VEHICLE_MASS_FL*/455,
/*S16_VEHICLE_MASS_FR*/489,
/*S16_VEHICLE_MASS_RL*/452,
/*S16_VEHICLE_MASS_RR*/424,
/*S16_MOMENT_OF_INERTIA*/5046,
/*S16_WHEEL_TREAD_FRONT*/1575,
/*S16_WHEEL_TREAD_REAR*/1580,
/*S16_UNDERSTEER_GRADIENT*/337,
/*S16_MAX_ROAD_ADHESION*/83,
/*S16_DELTA_STEER_FOR_BANK*/33,
/*int S16_LAT_G_EXTRA_40*/105,
/*int S16_LAT_G_EXTRA_60*/105,
/*int S16_LAT_G_EXTRA_80*/105,
/*int S16_LAT_G_EXTRA_100*/105,
/*int S16_LAT_G_EXTRA_120*/105,
/*int S16_LAT_G_EXTRA_150*/105,
/*S16_ADM_VCH_NOMINAL*/841,
/*S16_STR_1st_Point*/900,
/*S16_STR_2nd_Point*/1800,
/*S16_STR_3rd_Point*/2700,
/*S16_STR_4th_Point*/3600,
/*S16_STR_5th_Point*/4500,
/*S16_STEERING_MAX_DEG*/6260,
/*S16_STR_RATIO_10_1st*/170,
/*S16_STR_RATIO_10_2nd*/170,
/*S16_STR_RATIO_10_3rd*/170,
/*S16_STR_RATIO_10_4th*/170,
/*S16_STR_RATIO_10_5th*/170,
/*S16_STR_RATIO_10_MAXDEG*/170,
/*S16_CW_STR_RATIO_10_1st*/170,
/*S16_CW_STR_RATIO_10_2nd*/170,
/*S16_CW_STR_RATIO_10_3rd*/170,
/*S16_CW_STR_RATIO_10_4th*/170,
/*S16_CW_STR_RATIO_10_5th*/170,
/*S16_CW_STR_RATIO_10_MAXDEG*/170,
/*S16_BANK_ALAT_Roll_Gain*/100,
/*S16_Yawm_Filter_Gain_60kph*/128,
/*S16_Yawm_Filter_Gain_90kph*/128,
/*S16_Yawm_Filter_Gain_120kph*/128,
/*S16_Yawm_Filter_Gain_150kph*/128,
};
//#pragma CONST_SEG DEFAULT