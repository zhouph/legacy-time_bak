#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	#define idx_START
	#define idx_FILE	idx_LEngTunePar
	#include "Mdyn_autosar.h"
#endif

//#if defined (__HMC_CCP) && (__HMC_CCP==ENABLE)

    #if defined __GM_CAL_SET
        #define __SET_ENG_VAR_PER_CALSET
    #else
        #define __ENG_VAR_SET_PER_ENG_VEH_TYPE
    #endif

    #define S16_AUTO_TM_ID  1
    #define S16_MANUAL_TM_ID  2
    #define S16_DCT_TM_ID   3
    #define S16_CVT_TM_ID   4    

    #if defined (__ENG_VAR_SET_PER_ENG_VEH_TYPE)

        #if (__CAR == KMC_HM)

            #if __SUSPENSION_TYPE == US
                #include "./Hm_US/Cal_data/HM_TUNapEngVarStruct.h"
            #else
                #include "./Hm/Cal_data/HM_TUNapEngVarStruct.h"
            #endif

        #elif (__CAR == HMC_LM)
	    	#if __SUSPENSION_TYPE == DOM
                #if __ENG_TYPE==ENG_GASOLINE
                    #include "./LM/Cal_Data/LM_TUNapEngVarStruct.h"
                #elif __ENG_TYPE==ENG_DIESEL
                    #include "./LM_DSL/Cal_Data/LM_TUNapEngVarStruct.h"
                #else
                #endif /*__ENG_TYPE*/
	    	#elif __SUSPENSION_TYPE == EU
			    #if __ENG_TYPE==ENG_GASOLINE
                  	#include "./LM_EU/Cal_Data/LM_TUNapEngVarStruct.h"
                #elif __ENG_TYPE==ENG_DIESEL
                    #include "./LM_EU_DSL/Cal_Data/LM_TUNapEngVarStruct.h"    
                #else
                #endif /*__ENG_TYPE*/
	    	#elif __SUSPENSION_TYPE == US
	    	    #if __ENG_TYPE==ENG_GASOLINE
	    	        #include "./LM_US/Cal_Data/LM_TUNapEngVarStruct.h"
                #endif
            #else
            #endif  /* __SUSPENSION_TYPE */
            
        #elif (__CAR==KMC_PU)
            #if __SUSPENSION_TYPE == DOM		    
                #if CAR_VAR_NUM == 1
                    #include "../Appl/PU_1T/Cal_data/PU_TUNapEngVarStruct.h"
			    #elif CAR_VAR_NUM == 2
				    #include "../Appl/PU_1T_ULTRA/Cal_data/PU_TUNapEngVarStruct.h"
			    #elif CAR_VAR_NUM == 3
				    #include "../Appl/PU_12T_ULTRA/Cal_data/PU_TUNapEngVarStruct.h"
			    #elif CAR_VAR_NUM == 4
				    #include "../Appl/PU_HIGH_2WD/Cal_data/PU_TUNapEngVarStruct.h"
			    #elif CAR_VAR_NUM == 5
				    #include "../Appl/PU_HIGH_4WD/Cal_data/PU_TUNapEngVarStruct.h"
			    #elif CAR_VAR_NUM == 6
				    #include "../Appl/PU_LPI_ULTRA/Cal_data/PU_TUNapEngVarStruct.h"
			    #endif  /* CAR_VAR_NUM */													 	
            #else	/*__SUSPENSION_TYPE*/
	        #endif	/*__SUSPENSION_TYPE*/ 

        #elif (__CAR==HMC_HR)
            #if __SUSPENSION_TYPE == DOM		    
                #if CAR_VAR_NUM == 1
                    #include "../Appl/HR/Cal_data/HR_TUNapEngVarStruct.h"
			    #elif CAR_VAR_NUM == 2
				    #include "../Appl/HR_HIGH_2WD/Cal_data/HR_TUNapEngVarStruct.h"
			    #endif  /* CAR_VAR_NUM */													 	
            #else	/*__SUSPENSION_TYPE*/
	        #endif	/*__SUSPENSION_TYPE*/ 
	        
        #elif __CAR==SYC_X100
            #if __SUSPENSION_TYPE == DOM
                #include "../Appl/X100/Cal_data/X100_TUNapEngVarStruct.h"												 	
            #else	/*__SUSPENSION_TYPE*/
                #error "Wrong Suspension Type"
	        #endif	/*__SUSPENSION_TYPE*/ 	        
	        
        #elif (__CAR==HMC_HD)
            #if __SUSPENSION_TYPE == DOM
                #if CAR_VAR_NUM == 1
                    #include "../APPL/HD_HEV/Cal_Data/HD_HEV_TUNapEngVarStruct.h"
                #endif
            #else
            #endif

        #elif (__CAR==HMC_GC)
            #if __SUSPENSION_TYPE == DOM
                #include "../APPL/GC/Cal_Data/GC_TUNapEngVarStruct.h"
            #else /*__SUSPENSION_TYPE*/
                #error "Wrong Suspension Type"
            #endif /*__SUSPENSION_TYPE*/ 

        #elif (__CAR==KMC_KM)
            #include "./KM/Cal_data/KM_TUNapEngVarStruct.h"

        #elif (__CAR==GM_M300)
            #include "./M300/Cal_data/M300_TUNapEngVarStruct.h"
            
        #elif (__CAR==HMC_TL)
            #if __SUSPENSION_TYPE == DOM
                #if CAR_VAR_NUM == 1
                    #include "../APPL/TL/Cal_Data/TL_TUNapEngVarStruct.h"
                #endif
            #else
            #endif

        #elif (__CAR==GM_J300)
            #include "./J300/Cal_data/J300_TUNapEngVarStruct.h"

        #elif (__CAR==KMC_TD)
            #include "./TD/Cal_data/TD_TUNapEngVarStruct.h"

        #elif (__CAR==GM_MALIBU)
            #include "./MALIBU/Cal_data/Malibu_TUNapEngVarStruct.h"
        
        #elif (__CAR==GM_XTS)
            #include "./XTS/Cal_data/XTS_TUNapEngVarStruct.h"    

        #elif (__CAR==GM_LAMBDA)
            #include "./LAMBDA/Cal_data/LAMBDA_TUNapEngVarStruct.h"
            
        #elif (__CAR==GM_TAHOE)
            #include "./TAHOE/Cal_data/TAHOE_TUNapEngVarStruct.h"

        #elif (__CAR==HMC_EN)
            #include "./EN/Cal_data/EN_TUNapEngVarStruct.h"

        #elif (__CAR==BMW740i)
            #include "./BMW740i/Cal_data/BMW740i_TUNapEngVarStruct.h"

        #elif (__CAR==HMC_BH)
            #include "./BH/Cal_data/BH_TUNapEngVarStruct.h"

        #elif (__CAR==HMC_HG)
            #if __SUSPENSION_TYPE == US
                #include "./HG_US/Cal_data/HG_TUNapEngVarStruct.h"
            #else
                #include "./HG/Cal_data/HG_TUNapEngVarStruct.h"
            #endif
            
        #elif (__CAR==KMC_TF)
            #include "./TF/Cal_data/TF_TUNapEngVarStruct.h"

        #elif (__CAR==KMC_PF)
            #include "./PF/Cal_data/PF_TUNapEngVarStruct.h"

        #elif (__CAR==SGM_328)
            #include "./SGM328/Cal_data/SGM328_TUNapEngVarStruct.h"

        #elif (__CAR==KMC_TFC)
            #include "./TFC/Cal_data/TFC_TUNapEngVarStruct.h"

        #elif (__CAR==HMC_YF)            
            #include "./YF/Cal_data/YF_TUNapEngVarStruct.h"
            
        #elif (__CAR==HMC_DH)
            #if __SUSPENSION_TYPE == DOM
                #include "./DH/Cal_Data/DH_TUNapEngVarStruct.h"
            #elif __SUSPENSION_TYPE == EU
                #include "./DH_EU/Cal_Data/DH_TUNapEngVarStruct.h"
            #elif __SUSPENSION_TYPE == US
                #include "./DH_US/Cal_Data/DH_TUNapEngVarStruct.h"
            #else
                #error "Wrong Suspension Type"
            #endif

        #elif (__CAR==HMC_VK)
            #if __SUSPENSION_TYPE == DOM
                #include "./VK/Cal_Data/VK_TUNapEngVarStruct.h"
            #elif __SUSPENSION_TYPE == US
                #include "./VK_US/Cal_Data/VK_TUNapEngVarStruct.h"
            #else
                #error "Wrong Suspension Type"
            #endif

        #elif (__CAR==HMC_VI)

            #if  __SUSPENSION_TYPE == US
                #include "./VI_US/Cal_data/VI_TUNapEngVarStruct.h"
            #else
                #include "./VI/Cal_data/VI_TUNapEngVarStruct.h"
            #endif
            
        #elif (__CAR==GM_S4500)

			#if __ESP_UPLEVEL==ENABLE
				#include "./GM_S4500_UPLEVEL/Cal_data/GM_S4500_UPLEVEL_TUNapEngVarStruct.h"
            #else
                #ifdef _GM_ESC_BASE_VARIANT
                	#include "./GM_S4500/Cal_data/GM_S4500_TUNapEngVarStruct.h"
                #else
                	#include "./GM_S4500_UPLEVEL/Cal_data/GM_S4500_UPLEVEL_TUNapEngVarStruct.h"
                #endif	
            #endif
            
        #elif (__CAR==DCX_COMPASS)
            #include "./COMPASS/Cal_data/COMPASS_TUNapEngVarStruct.h"

       #elif (__CAR==GM_SILVERADO)
            #include "./SILVERADO/Cal_data/SILVERADO_TUNapEngVarStruct.h"

        #elif (__CAR==PSA_C3)
            #include "./C3/Cal_data/C3_TUNapEngVarStruct.h"

        #elif (__CAR==PSA_C4)
            #include "./C4/Cal_data/C4_TUNapEngVarStruct.h"

        #elif (__CAR==BYD_5A)
            #include "./5A/Cal_data/5A_TUNapEngVarStruct.h"

        #elif (__CAR==GM_T300)
            #if __ESP_UPLEVEL==ENABLE
				#include "./GM_T300_UPLEVEL/Cal_data/GM_T300_UPLEVEL_TUNapEngVarStruct.h"
            #else
                #ifdef _GM_ESC_BASE_VARIANT
                	#include "./GM_T300/Cal_data/GM_T300_TUNapEngVarStruct.h"
                #else
                	#include "./GM_T300_UPLEVEL/Cal_data/GM_T300_UPLEVEL_TUNapEngVarStruct.h"
                #endif	
            #endif

        #else
            #error "Engine cal structure not defined"
        #endif

    #elif defined (__SET_ENG_VAR_PER_CALSET)
        
        /* Eng var struct not defined */
        
    #endif

//#endif
