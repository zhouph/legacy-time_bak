#ifndef ASW_CAL_CFG_H_
#define ASW_CAL_CFG_H_

#include "../_PAR/car/Car_Def.h"
#include "../_PAR/car/Car.par.h"


#define ASW_CAL_DEF __CAR

/*==================================================================================================
                                         INCLUDE FILES
==================================================================================================*/

#if ASW_CAL_DEF == HMC_BH
   #include "../APPL/BH/CAL/ASW_CAL_HMC_BH.h"
   #define ASW_CAL_DEFINED
#elif ASW_CAL_DEF == KMC_TD
   #include "../APPL/TD/CAL/ASW_CAL_KMC_TD.h"
   #define ASW_CAL_DEFINED   
#elif ASW_CAL_DEF == HMC_YFE
   #include "../APPL/YFE/CAL/ASW_CAL_HMC_YFE.h"
   #define ASW_CAL_DEFINED
#elif ASW_CAL_DEF == HMC_HGE
   #include "../APPL/HGE/CAL/ASW_CAL_HMC_HGE.h"
   #define ASW_CAL_DEFINED
#elif ASW_CAL_DEF == KMC_TFE
   #include "../APPL/TFE/CAL/ASW_CAL_KMC_TFE.h"
   #define ASW_CAL_DEFINED
#endif

#endif
