#if __CAR==KMC_HM
  #if __SUSPENSION_TYPE == US
    #if __ECU==ABS_ECU_1
        #include "../Appl/HM_US/car_var/HM_ABS.par.h"
    #elif __ECU==ESP_ECU_1
        #include "../Appl/HM_US/car_var/HM_ESP.par.h"
    #endif
  #else
    #if __ECU==ABS_ECU_1
        #include "../Appl/HM/car_var/HM_ABS.par.h"
    #elif __ECU==ESP_ECU_1
        #include "../Appl/HM/car_var/HM_ESP.par.h"
    #endif
  #endif  
#elif __CAR==HMC_JM
    #if __ECU==ABS_ECU_1
        #include "../Appl/JM/car_var/JM_ABS.par.h"
    #elif __ECU==ESP_ECU_1
        #include "../Appl/JM/car_var/JM_ESP.par.h"
    #endif
#elif __CAR==KMC_KM
    #if __ECU==ABS_ECU_1
        #include "../Appl/KM/car_var/KM_ABS.par.h"
    #elif __ECU==ESP_ECU_1
        #include "../Appl/KM/car_var/KM_ESP.par.h"
    #endif
        
#elif __CAR==KMC_PU
	#if __SUSPENSION_TYPE == DOM		    
	    #if __ECU==ABS_ECU_1
	    	/* #if CAR_VAR_NUM == 1 */
	        	#include "../Appl/PU_1T/car_var/PU_ABS.par.h"
	    #elif __ECU==ESP_ECU_1
	        #if CAR_VAR_NUM == 1
				 #include "../Appl/PU_1T/car_var/PU_ESP.par.h"
			#elif CAR_VAR_NUM == 2
				#include "../Appl/PU_1T_ULTRA/car_var/PU_ESP.par.h"
			#elif CAR_VAR_NUM == 3
				#include "../Appl/PU_12T_ULTRA/car_var/PU_ESP.par.h"
			#elif CAR_VAR_NUM == 4
				#include "../Appl/PU_HIGH_2WD/car_var/PU_ESP.par.h"
			#elif CAR_VAR_NUM == 5
				#include "../Appl/PU_HIGH_4WD/car_var/PU_ESP.par.h"
			#elif CAR_VAR_NUM == 6
				#include "../Appl/PU_LPI_ULTRA/car_var/PU_ESP.par.h"
			#endif  /* CAR_VAR_NUM */													 	
	    #endif  /* __ECU */
		
	#else	/*__SUSPENSION_TYPE*/
		#error "Wrong Suspension Type"
	#endif	/*__SUSPENSION_TYPE*/
	
#elif __CAR==HMC_GC
	#if __SUSPENSION_TYPE == DOM		    
	    #if __ECU==ABS_ECU_1
	        #if CAR_VAR_NUM == 1
	        	#include "../Appl/GC/Car_var/GC_ABS_1.par.h"
	        #elif CAR_VAR_NUM == 2
	        	#include "../Appl/GC/Car_var/GC_ABS_2.par.h"
	        #elif CAR_VAR_NUM == 3
	        	#include "../Appl/GC/Car_var/GC_ABS_3.par.h"	 
	        #elif CAR_VAR_NUM == 4
	        	#include "../Appl/GC/Car_var/GC_ABS_4.par.h"
	        #endif /* CAR_VAR_NUM */
	    #elif __ECU==ESP_ECU_1
	        #if CAR_VAR_NUM == 1
				#include "../Appl/GC/Car_var/GC_ESP_1.par.h"
	        #elif CAR_VAR_NUM == 2
				#include "../Appl/GC/Car_var/GC_ESP_2.par.h"
	        #elif CAR_VAR_NUM == 3
				#include "../Appl/GC/Car_var/GC_ESP_3.par.h"
            #endif
	    #endif  /* __ECU */
	#else	/*__SUSPENSION_TYPE*/
		#error "Wrong Suspension Type"
	#endif	/*__SUSPENSION_TYPE*/  	   
        
#elif __CAR==HMC_HR
	#if __SUSPENSION_TYPE == DOM		    
	    #if __ECU==ESP_ECU_1
	        #if CAR_VAR_NUM == 1
				 #include "../Appl/HR/Car_var/HR_ESP_1.par.h"
			#elif CAR_VAR_NUM == 2
				#include "../Appl/HR_HIGH_2WD/Car_var/HR_ESP_2.par.h"
			#endif  /* CAR_VAR_NUM */													 	
	    #endif  /* __ECU */		
	#else	/*__SUSPENSION_TYPE*/
	#endif	/*__SUSPENSION_TYPE*/  

#elif __CAR==SYC_X100
	#if __SUSPENSION_TYPE == DOM		    
	    #if __ECU==ABS_ECU_1
            #include "../Appl/X100/Car_var/X100_ABS.par.h"
	    #else
            #include "../Appl/X100/Car_var/X100_ESP.par.h"												 	
	    #endif  /* __ECU */
	#else	/*__SUSPENSION_TYPE*/
	    #error "Wrong Suspension Type"
	#endif	/*__SUSPENSION_TYPE*/
	        
#elif __CAR==HMC_PA
    #if __ECU==ABS_ECU_1
        #include "../Appl/PA/car_var/PA_ABS.par.h"
    #elif __ECU==ESP_ECU_1
      #if __ENG_TYPE==ENG_DIESEL
        #include "../Appl/PA_Diesel/car_var/PA_ESP.par.h"
      #else
        #include "../Appl/PA/car_var/PA_ESP.par.h"
      #endif
    #endif
#elif __CAR==BMW740i
    #if __ECU==ESP_ECU_1
		#include "../Appl/BMW740i/car_var/BMW740i_ESP.par.h.h"
	#else
		#error "Wrong Variant Type"	        
    #endif
#elif __CAR==KMC_SA
    #if __ECU==ABS_ECU_1
        #include "../Appl/SA/car_var/SA_ABS.par.h"
    #elif __ECU==ESP_ECU_1
        #include "../Appl/SA/car_var/SA_ESP.par.h"
    #endif
#elif __CAR==HMC_NF
    #if __ECU==ABS_ECU_1
        #include "../Appl/NF/car_var/NF_ABS.par.h"
    #elif __ECU==ESP_ECU_1
        #include "../Appl/NF/car_var/NF_ESP.par.h"
    #endif
#elif __CAR==DCX_COMPASS
    #if __ECU==ABS_ECU_1
        #include "../Appl/COMPASS/car_var/COMPASS_ABS.par.h"
    #elif __ECU==ESP_ECU_1
        #include "../Appl/COMPASS/car_var/COMPASS_ESP.par.h"
    #endif
#elif __CAR==HMC_GK
    #if __ECU==ABS_ECU_1
        #include "../Appl/GK/car_var/GK_ABS.par.h"
    #elif __ECU==ESP_ECU_1
        #include "../Appl/GK/car_var/GK_ESP.par.h"
    #endif
#elif __CAR==SYC_KYRON
    #if __ECU==ABS_ECU_1
        #include "../Appl/KY/car_var/KY_ABS.par.h"
    #elif __ECU==ESP_ECU_1
        #include "../Appl/KY/car_var/KY_ESP.par.h"
    #endif
#elif __CAR==SYC_RAXTON
    #if __ECU==ABS_ECU_1
        #include "../Appl/RX/car_var/RX_ABS.par.h"
    #elif __ECU==ESP_ECU_1
        #include "../Appl/RX/car_var/RX_ESP.par.h"
    #endif
#elif __CAR==HMC_HDC
    #if __ECU==ABS_ECU_1
        #include "../Appl/HDC/car_var/HDC_ABS.par.h"
    #elif __ECU==ESP_ECU_1
        #include "../Appl/HDC/car_var/HDC_ESP.par.h"
    #endif
#elif __CAR==GM_TAHOE
    #include "../Appl/TAHOE/car_var/TAHOE_ESP.par.h"
#elif __CAR==GM_SILVERADO 
    #include "../Appl/SILVERADO/car_var/SILVERADO_ESP.par.h"
#elif __CAR==GM_C100
    #if __4WD
        #include "../Appl/C100/car_var/C100_4WD_ESP.par.h"
    #else
        #include "../Appl/C100/car_var/C100_2WD_ESP.par.h"	
    #endif
#elif __CAR==CHINA_B12
    #if __ECU==ABS_ECU_1
        #include "../Appl/B12/car_var/B12_ABS.par.h"
    #elif __ECU==ESP_ECU_1
        #include "../Appl/B12/car_var/B12_ESP.par.h"	
    #endif
#elif __CAR==HMC_AM
    #if __ECU==ABS_ECU_1
        #include "../Appl/AM/car_var/AM_ABS.par.h"
    #elif __ECU==ESP_ECU_1
        #include "../Appl/AM/car_var/AM_ESP.par.h"	
    #endif
#elif __CAR==KMC_TD
  #if __ENG_TYPE==ENG_DIESEL
    #if __ECU==ABS_ECU_1
        #include "../Appl/TD_Diesel/car_var/TD_ABS.par.h"
    #elif __ECU==ESP_ECU_1
        #include "../Appl/TD_Diesel/car_var/TD_ESP.par.h"	
    #endif
  #else
	#if __ECU==ABS_ECU_1
        #include "../Appl/TD/car_var/TD_ABS.par.h"
    #elif __ECU==ESP_ECU_1
        #include "../Appl/TD/car_var/TD_ESP.par.h"	
    #endif  
  #endif
#elif __CAR==HMC_EN
    #if __ECU==ABS_ECU_1
        #include "../Appl/EN/car_var/EN_ABS.par.h"
    #elif __ECU==ESP_ECU_1
        #include "../Appl/EN/car_var/EN_ESP.par.h"	
    #endif
#elif __CAR==CHINA_A13
    #if __ECU==ABS_ECU_1
        #include "../Appl/A13/car_var/A13_ABS.par.h"	
    #endif
#elif __CAR==CHINA_A21
    #if __ECU==ABS_ECU_1
        #include "../Appl/A21/car_var/A21_ABS.par.h"
    #endif
#elif __CAR==CHINA_P11
    #if __ECU==ABS_ECU_1
        #include "../Appl/P11/car_var/P11_ABS.par.h"	
    #endif
#elif __CAR==HMC_BK
    #if __ECU==ABS_ECU_1
        #include "../Appl/BK/car_var/BK_ABS.par.h"
    #elif __ECU==ESP_ECU_1
        #include "../Appl/BK/car_var/BK_ESP.par.h"
    #endif
#elif __CAR==HMC_JB
    #if __ECU==ABS_ECU_1
        #include "../Appl/JB/car_var/JB_ABS.par.h"
    #elif __ECU==ESP_ECU_1
        #include "../Appl/JB/car_var/JB_ESP.par.h"
    #endif
    
#elif __CAR==HMC_TL
    #if __ECU == ESP_ECU_1
        #include "../APPL/TL/Car_var/TL_ESP_1.par.h"
    #endif
        
#elif __CAR==HMC_LM
	#if __SUSPENSION_TYPE == DOM		    
	    #if __ECU==ESP_ECU_1
	        #if CAR_VAR_NUM == 1
				 #include "../Appl/LM/car_var/LM_ESP_1.par.h"
			#elif CAR_VAR_NUM == 2
				#include "../Appl/LM/car_var/LM_ESP_2.par.h"
			#elif CAR_VAR_NUM == 3
				#include "../APPL/LM_DSL/Car_var/LM_ESP_3.par.h"
			#elif CAR_VAR_NUM == 4
			    #include "../APPL/LM_DSL/Car_var/LM_ESP_4.par.h"
	       	#endif	       	
	    #endif /* __ECU */	
	        
	#elif   __SUSPENSION_TYPE == EU 
	    #if __ECU==ABS_ECU_1 
	    	#if CAR_VAR_NUM == 1
	        	#include "../APPL/LM_EU/Car_var/LM_ABS_1.par.h"
	        #elif CAR_VAR_NUM == 2
	        	#include "../APPL/LM_EU/Car_var/LM_ABS_2.par.h"
			#elif CAR_VAR_NUM == 3
				#include "../APPL/LM_EU/Car_var/LM_ABS_3.par.h"
			#elif CAR_VAR_NUM == 4
				#include "../APPL/LM_EU/Car_var/LM_ABS_4.par.h"
			#endif	
	    #elif __ECU==ESP_ECU_1
	        #if CAR_VAR_NUM == 5
				 #include "../APPL/LM_EU/Car_var/LM_ESP_5.par.h"
			#elif CAR_VAR_NUM == 6
				#include "../APPL/LM_EU/Car_var/LM_ESP_6.par.h"
		    #elif CAR_VAR_NUM == 7
				 #include "../APPL/LM_EU_DSL/Car_var/LM_ESP_7.par.h"
			#elif CAR_VAR_NUM == 8
				#include "../APPL/LM_EU_DSL/Car_var/LM_ESP_8.par.h"
	       	#endif	       	
	    #endif /* __ECU */	  
		
	#elif   __SUSPENSION_TYPE == US
	    #if __ECU==ESP_ECU_1
	        #if CAR_VAR_NUM == 9
				 #include "../APPL/LM_US/Car_var/LM_ESP_9.par.h"
	       	#endif	       	
	    #endif /* __ECU */	  		
	#else	/*__SUSPENSION_TYPE*/
	#endif	/*__SUSPENSION_TYPE*/
			    
#elif __CAR==KMC_XM
    #if __ECU==ABS_ECU_1
        #include "../Appl/XM/car_var/XM_ABS.par.h"
    #elif __ECU==ESP_ECU_1
        #include "../Appl/XM/car_var/XM_ESP.par.h"
    #endif
#elif __CAR==KMC_MG			 /* test */
  #if __SUSPENSION_TYPE == EU 
	#if __ECU==ABS_ECU_1
		#include "../Appl/MG_EU/car_var/MG_ABS.par.h"
	#elif __ECU==ESP_ECU_1
		#include "../Appl/MG_EU/car_var/MG_ESP.par.h"
	#endif
  #else
	#if __ECU==ABS_ECU_1
		#include "../Appl/MG/car_var/MG_ABS.par.h"
	#elif __ECU==ESP_ECU_1
		#include "../Appl/MG/car_var/MG_ESP.par.h"
	#endif
  #endif
#elif __CAR==HMC_FC			 /* test */
	#if __ECU==ABS_ECU_1
		#include "../Appl/FC/car_var/FC_ABS.par.h"
	#elif __ECU==ESP_ECU_1
		#include "../Appl/FC/car_var/FC_ESP.par.h"
	#endif
#elif __CAR==KMC_UN 		 /* test */
  #if __SUSPENSION_TYPE == US
	#if __ECU==ABS_ECU_1
		#include "../Appl/Un_US/car_var/Un_ABS.par.h"
	#elif __ECU==ESP_ECU_1
		#include "../Appl/Un_US/car_var/Un_ESP.par.h"
	#endif	
  #elif __SUSPENSION_TYPE == EU
	#if __ECU==ABS_ECU_1
		#include "../Appl/Un_EU/car_var/Un_ABS.par.h"
	#elif __ECU==ESP_ECU_1
		#include "../Appl/Un_EU/car_var/Un_ESP.par.h"
	#endif	  
  #else
 	#if __ECU==ABS_ECU_1
		#include "../Appl/Un/car_var/Un_ABS.par.h"
	#elif __ECU==ESP_ECU_1
		#include "../Appl/Un/car_var/Un_ESP.par.h"
	#endif	 
  #endif  	
#elif __CAR==SYC_C200
    #if __ECU==ABS_ECU_1
        #include "../Appl/C200/car_var/C200_ABS.par.h"
    #elif __ECU==ESP_ECU_1
        #include "../Appl/C200/car_var/C200_ESP.par.h"
    #endif
#elif __CAR==GM_M300
	#if __ECU==ABS_ECU_1
		#include "../Appl/GM_M300/car_var/M300_ABS.par.h"
	#elif __ECU==ESP_ECU_1
		#include "../Appl/GM_M300/car_var/M300_ESP.par.h"
	#endif

#elif __CAR==GM_T300
	#if __ECU==ABS_ECU_1
		#include "../Appl/GM_T300/car_var/T300_ABS.par.h"
	#elif __ECU==ESP_ECU_1
		#ifdef _GM_ESC_BASE_VARIANT
			#include "../Appl/GM_T300/car_var/T300_ESP.par.h"
		#else
			#include "../Appl/GM_T300/car_var/T300_ESP.par.h"
		#endif	
	#endif

#elif __CAR==GM_J300
    #if __ECU==ABS_ECU_1
        #include "../Appl/J300/car_var/J300_ABS.par.h"
    #elif __ECU==ESP_ECU_1
        #include "../Appl/J300/car_var/J300_ESP.par.h"
    #endif

#elif __CAR==PSA_C3
    #if __ECU==ABS_ECU_1
        #include "../Appl/C3/car_var/C3_ABS.par.h"
    #elif __ECU==ESP_ECU_1
        #include "../Appl/C3/car_var/C3_ESP.par.h"
    #endif    

#elif __CAR==PSA_C4
    #if __ECU==ABS_ECU_1
        #include "../Appl/C4/car_var/C4_ABS.par.h"
    #elif __ECU==ESP_ECU_1
        #include "../Appl/C4/car_var/C4_ESP.par.h"
    #endif
	
#elif __CAR==GM_MALIBU
    #if __ECU==ESP_ECU_1
        #include "../Appl/MALIBU/car_var/MALIBU_ESP.par.h"
    #endif

#elif __CAR==GM_XTS
    #if __ECU==ESP_ECU_1
        #include "../Appl/XTS/car_var/XTS_ESP.par.h"
    #endif    
    
#elif __CAR==GM_LAMBDA
    #if __ECU==ABS_ECU_1
        #include "../Appl/LAMBDA/car_var/LAMBDA_ABS.par.h"
    #elif __ECU==ESP_ECU_1
    	#if CAR_VAR_NUM == 1
        	#include "../Appl/LAMBDA/car_var/LAMBDA_ESP_1.par.h"
		#elif CAR_VAR_NUM == 2
			#include "../Appl/LAMBDA/car_var/LAMBDA_ESP_2.par.h"
    	#endif        
    #endif
    
//#elif __CAR==GM_SIERRA
//    #if __ECU==ABS_ECU_1
//        #include "../Appl/SIERRA/car_var/SIERRA_ABS.par.h"
//    #elif __ECU==ESP_ECU_1
//        #include "../Appl/SIERRA/car_var/SIERRA_ESP.par.h"
//    #endif

#elif __CAR==GM_S4500
	#if __ECU==ABS_ECU_1
		#include "../Appl/GM_S4500/car_var/S4500_ABS.par.h"
	#elif __ECU==ESP_ECU_1
		#ifdef _GM_ESC_BASE_VARIANT
			#include "../Appl/GM_S4500/car_var/S4500_ESP.par.h"
		#else
			#include "../Appl/GM_S4500_UPLEVEL/car_var/S4500_ESP.par.h"
		#endif
	#endif

#elif __CAR==GM_GSUV
	#if __ECU==ABS_ECU_1
		#include "../Appl/GM_GSUV/car_var/GSUV_ABS.par.h"
	#elif __ECU==ESP_ECU_1
		#include "../Appl/GM_GSUV/car_var/GSUV_ESP.par.h"
	#endif

#elif __CAR==HMC_HD
    #if __ECU == ESP_ECU_1
        #include "../APPL/HD_HEV/Car_var/HD_HEV_ESP.par.h"
    #endif
 	
#elif __CAR==KMC_ED
 	#if __ECU==ESP_ECU_1
  		#include "../Appl/ED_HEV/car_var/ED_HEV_ESP.par.h"	
 	#endif
 	
#elif __CAR==SGM_308
	#if __ECU==ABS_ECU_1
		#include "../Appl/SGM308/Car_var/SGM308_ABS_1.par.h"
	#endif

#elif __CAR==SGM_328
    #if __ECU==ESP_ECU_1
		#include "../Appl/SGM328/Car_var/SGM_328_ESP_1.par.h"
	#endif

#elif __CAR==HMC_MC
	#if __ECU==ABS_ECU_1
		#include "../Appl/MC/car_var/MC_ABS.par.h"
	#elif __ECU==ESP_ECU_1
		#include "../Appl/MC/car_var/MC_ESP.par.h"
	#endif

#elif __CAR==SGMW_CN100
	#if __ECU==ABS_ECU_1
		#if __WHEEL_DRIVE_TYPE == FRONT_DRIVE
			#include "../Appl/CN100/car_var/CN100_FWD_ABS.par.h"
		#elif __WHEEL_DRIVE_TYPE == REAR_DRIVE
			#include "../Appl/CN100/car_var/CN100_RWD_ABS.par.h"
		#endif
	#endif

#elif __CAR==KMC_TA
	#if __ECU==ABS_ECU_1
		#include "../Appl/TA/car_var/TA_ABS.par.h"
	#elif __ECU==ESP_ECU_1
		#include "../Appl/TA/car_var/TA_ESP.par.h"
	#endif

#elif __CAR==CHINA_P12
	#if __ECU==ABS_ECU_1
		#include "../Appl/P12/car_var/P12_ABS.par.h"
	#endif

#elif __CAR==CHINA_E101
	#if __ECU==ABS_ECU_1
		#include "../Appl/E101/car_var/E101_ABS.par.h"
	#endif

#elif __CAR==HMC_YF
    #if __ECU==ABS_ECU_1
        #include "../Appl/YF/car_var/YF_ABS.par.h"
    #elif __ECU==ESP_ECU_1
        #include "../Appl/YF/car_var/YF_ESP.par.h"
    #endif
 
#elif __CAR==HMC_HG
    #if __ECU==ABS_ECU_1
        #include "../Appl/HG/car_var/HG_ABS.par.h"
    #elif __ECU==ESP_ECU_1
        #include "../Appl/HG/car_var/HG_ESP.par.h"
    #endif

#elif __CAR==HMC_BH
    #if __ECU==ABS_ECU_1
        #include "../Appl/BH/car_var/BH_ABS.par.h"
    #elif __ECU==ESP_ECU_1
        #include "../Appl/BH/car_var/BH_ESP.par.h"
    #endif

#elif __CAR==KMC_TF
    #if __ECU==ABS_ECU_1
        #include "../Appl/TF/car_var/TF_ABS.par.h"
    #elif __ECU==ESP_ECU_1
        #include "../Appl/TF/car_var/TF_ESP.par.h"
    #endif

#elif __CAR==KMC_TFC
        #include "../Appl/TFC/car_var/TFC_ESP.par.h"

#elif __CAR==KMC_PF
    #if __ECU==ABS_ECU_1
        #include "../Appl/PF/car_var/PF_ABS_1.par.h"
    #elif __ECU==ESP_ECU_1
        #include "../Appl/PF/car_var/PF_ESP_1.par.h"
    #endif

#elif __CAR==HMC_VF
  #if __SUSPENSION_TYPE == EU
    #if __ECU==ABS_ECU_1
        #include "../Appl/VF/car_var/VF_ABS.par.h"
    #elif __ECU==ESP_ECU_1
        #if CAR_VAR_NUM==EU_1
	    	#include "../Appl/VF/car_var/VF_ESP_1.par.h"
	    #elif CAR_VAR_NUM==EU_2
	    	#include "../Appl/VF/car_var/VF_ESP_2.par.h"
	    #elif CAR_VAR_NUM==EU_3
	    	#include "../Appl/VF/car_var/VF_ESP_3.par.h"
        #elif CAR_VAR_NUM==EU_4
	    	#include "../Appl/VF/car_var/VF_ESP_4.par.h"	        	
	    #else
	        #error "Wrong Variant Type"
	    #endif
    #endif
  #else /* __SUSPENSION_TYPE == EU */
    #if __ECU==ABS_ECU_1
        #include "../Appl/VF/car_var/VF_ABS.par.h"
    #elif __ECU==ESP_ECU_1
        #if CAR_VAR_NUM==EU_1
	    	#include "../Appl/VF/car_var/VF_ESP_1.par.h"
	    #elif CAR_VAR_NUM==EU_2
	    	#include "../Appl/VF/car_var/VF_ESP_2.par.h"
	    #elif CAR_VAR_NUM==EU_3
	    	#include "../Appl/VF/car_var/VF_ESP_3.par.h"
        #elif CAR_VAR_NUM==EU_4
	    	#include "../Appl/VF/car_var/VF_ESP_4.par.h"	        	
	    #else
	        #error "Wrong Variant Type"
	    #endif
    #endif  
  #endif /* __SUSPENSION_TYPE == EU */

#elif __CAR==KMC_KH
	#if  __SUSPENSION_TYPE == GEN
	    #if __ECU==ABS_ECU_1
	        #include "../Appl/KH_GEN/car_var/KH_ABS.par.h"
	    #elif __ECU==ESP_ECU_1
	        #include "../Appl/KH_GEN/car_var/KH_GEN_Premium_1.par.h"
	    #endif
	#elif __SUSPENSION_TYPE == DOM
	    #if __ECU==ABS_ECU_1
	        #include "../Appl/KH/car_var/KH_ABS.par.h"
	    #elif __ECU==ESP_ECU_1
	       	#include "../Appl/KH/car_var/KH_DOM_Premium_1.par.h"
	    #endif
    #else
        #error "Wrong Suspension Type"
    #endif   

#elif (__CAR==KMC_SL) 
    #if __SUSPENSION_TYPE == US
        #if __ECU==ABS_ECU_1
            #include "../Appl/SL_US/car_var/SL_ABS.par.h"
        #elif __ECU==ESP_ECU_1
            #include "../Appl/SL_US/car_var/SL_ESP.par.h"
        #endif

	#else	/* __SUSPENSION_TYPE == KOREA */
        /*#if __ENG_TYPE == ENG_DIESEL	
            #if __ECU==ABS_ECU_1
                #include "../Appl/SL_DSL/car_var/SL_ABS.par.h"
            #elif __ECU==ESP_ECU_1
                #include "../Appl/SL_DSL/car_var/SL_ESP.par.h"
            #endif
        #else*/
            #if __ECU==ABS_ECU_1
                #include "../Appl/SL/car_var/SL_ABS.par.h"
            #elif __ECU==ESP_ECU_1
                #include "../Appl/SL/car_var/SL_ESP.par.h"
            #endif   
        //#endif
    #endif
    
#elif __CAR==KMC_SO
	#if __ECU==ABS_ECU_1
		#include "../Appl/SO/car_var/SO_ABS.par.h"
	#elif __ECU==ESP_ECU_1
		#include "../Appl/SO/car_var/SO_ESP.par.h"
	#endif
	
#elif __CAR==HMC_DH
 	#if __SUSPENSION_TYPE == DOM
		#if __ECU==ESP_ECU_1
			#if (CAR_VAR_NUM == 1) || (CAR_VAR_NUM == 2) || (CAR_VAR_NUM == 3)
				#include "../Appl/DH/car_var/DH_ESP_1.par.h"
			#elif (CAR_VAR_NUM == 8) || (CAR_VAR_NUM == 9) || (CAR_VAR_NUM == 10)
				#include "../Appl/DH/car_var/DH_ESP_2.par.h"
			#else 
			#error "Wrong Car Variation Number"
			#endif
		#endif
	#elif __SUSPENSION_TYPE == EU
		#if __ECU==ESP_ECU_1
			#if (CAR_VAR_NUM == 4) || (CAR_VAR_NUM == 5) || (CAR_VAR_NUM == 6)
				#include "../Appl/DH_EU/car_var/DH_ESP_1.par.h"
			#elif (CAR_VAR_NUM == 11) || (CAR_VAR_NUM == 12) || (CAR_VAR_NUM == 13)
				#include "../Appl/DH_EU/car_var/DH_ESP_2.par.h"
			#else 
			#error "Wrong Car Variation Number"
			#endif
		#endif
	#elif __SUSPENSION_TYPE == US
		#if __ECU==ESP_ECU_1
			#if CAR_VAR_NUM == 7
				#include "../Appl/DH_US/car_var/DH_ESP_1.par.h"
			#elif CAR_VAR_NUM == 14
				#include "../Appl/DH_US/car_var/DH_ESP_2.par.h"
			#else 
			#error "Wrong Car Variation Number"
			#endif
		#endif
	#else 	/*__SUSPENSION_TYPE*/
        #error "Wrong Suspension Type"
	#endif	/*__SUSPENSION_TYPE*/
	
#elif __CAR==HMC_VK
 	#if __SUSPENSION_TYPE == DOM
		#if __ECU==ESP_ECU_1
			#include "../Appl/VK/Car_var/VK_ESP.par.h"
		#endif
	#elif __SUSPENSION_TYPE == US
		#if __ECU==ESP_ECU_1
			#include "../Appl/VK_US/car_var/VK_ESP.par.h"
		#endif
	#else 	/*__SUSPENSION_TYPE*/
        #error "Wrong Suspension Type"
	#endif	/*__SUSPENSION_TYPE*/
	
#elif __CAR==HMC_FS
	#if __SUSPENSION_TYPE == US
       #if __ECU==ABS_ECU_1
       #include "../Appl/FS_US/car_var/FS_ABS.par.h"
       #elif  __ECU==ESP_ECU_1
       #include "../Appl/FS_US/car_var/FS_ESP.par.h"
       #endif
       
	#elif __SUSPENSION_TYPE == EU
       #if __ECU==ABS_ECU_1
       #include "../Appl/FS_EU/car_var/FS_ABS.par.h"
       #elif  __ECU==ESP_ECU_1
       #include "../Appl/FS_EU/car_var/FS_ESP.par.h"
       #endif       
       
	#elif __SUSPENSION_TYPE == GEN     
       #if __ECU==ABS_ECU_1
       #include "../Appl/FS_GNR/car_var/FS_ABS.par.h"
       #elif  __ECU==ESP_ECU_1
       #include "../Appl/FS_GNR/car_var/FS_ESP.par.h"
       #endif   
     #else
        #if __ECU==ABS_ECU_1
       #include "../Appl/FS/car_var/FS_ABS.par.h"
       #elif  __ECU==ESP_ECU_1
       #include "../Appl/FS/car_var/FS_ESP.par.h"
       #endif       
     #endif
#elif __CAR==HMC_VI
	#if  __SUSPENSION_TYPE == US
	    #if __ECU==ABS_ECU_1
	        #include "../Appl/VI_US/car_var/VI_ABS.par.h"
	    #elif __ECU==ESP_ECU_1
	        #include "../Appl/VI_US/car_var/VI_ESP.par.h"
	    #endif
	#elif __SUSPENSION_TYPE == DOM
	    #if __ECU==ABS_ECU_1
	        #include "../Appl/VI/car_var/VI_ABS.par.h"
	    #elif __ECU==ESP_ECU_1
	       	#include "../Appl/VI/car_var/VI_ESP.par.h"
	    #endif
    #else
        #error "Wrong Suspension Type"
    #endif  
    
 #elif __CAR==BYD_5A
	    #if __ECU==ESP_ECU_1
	        #include "../Appl/5A/car_var/5A_ESP.par.h"
	    #else
	        #error "Wrong Suspension Type"
	    #endif

 #elif __CAR==HMC_YFE
        #if __ECU==ESP_ECU_1
            #include "../APPL/YFE/car_var/YFE_ESP.par.h"
        #else
            #error "Wrong Suspension Type"
        #endif
 #elif __CAR==KMC_TFE
        #if __ECU==ESP_ECU_1
            #include "../APPL/TFE/car_var/TFE_ESP.par.h"
        #else
            #error "Wrong Suspension Type"
        #endif
#elif __CAR==HMC_HGE
        #if __ECU==ESP_ECU_1
	        #if CAR_VAR_NUM == 1
				 #include "../AppL/HGE/car_var/HGE_ESP_1.par.h"
			#elif CAR_VAR_NUM == 2
				#include "../AppL/HGE/car_var/HGE_ESP_2.par.h"
	       	#endif
        #else
            #error "Wrong Suspension Type"
        #endif
#endif
