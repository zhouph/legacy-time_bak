
#include "../../../HDR/ASW_Base_Type.h"

#if (ASW_CFG_CAL_DEV_ENA == ENABLE)
    #define ASW_Calibration_t volatile
#else
    #define ASW_Calibration_t const
#endif

#define	U8_ESP_Output_Selection_Mode					7

#define	U16_TIRE_L										1988
#define U16_TIRE_L_R									1988

#define S16_RTA_DET_THRES								30
#define S16_RTA_DET_THRES_MAX							300

#define	S16_MU_LOW                            			180                        
#define	S16_MU_MED                            			200    
#define	S16_MU_MED_HIGH                       			500
#define	S16_MU_HIGH                           			700
#define	S16_MU_HIGH2                          			750    
#define	S16_MU_V_HIGH                         			850 

#define	S16_LM_EMS                            			250
#define	S16_MM_EMS                            			300
#define	S16_MHM_EMS                           			500
#define	S16_HM_EMS                            			600  

#define	S16TCSCpTCcrnt_1								0      
#define	S16TCSCpTCcrnt_2								380 
#define	S16TCSCpTCcrnt_3								530 
#define	S16TCSCpTCcrnt_4								800 
#define	S16TCSCpTCcrnt_5								1450
#define	S16TCSCpWhlPre_1								0   
#define	S16TCSCpWhlPre_2								10  
#define	S16TCSCpWhlPre_3								100 
#define	S16TCSCpWhlPre_4								250 
#define	S16TCSCpWhlPre_5								900 

#define	U8_EEC_FADE_OUT_CONTROL               			1
#define U8_EDC_TM_1_GEAR_RATIO							149
#define U8_EDC_TM_2_GEAR_RATIO							80
#define U8_EDC_TM_3_GEAR_RATIO							57
#define U8_EDC_TM_4_GEAR_RATIO							43
#define U8_EDC_TM_5_GEAR_RATIO							33
#define U8_EDC_TM_6_GEAR_RATIO							28
#define U8_EDC_TM_R_GEAR_RATIO							125
#define S16_TCS_ENG_TORQUE_MAX							2300

#define S16_Bank_Suspect_Thres2							700
#define S16_Bank_Lower_Suspect_Thres2					300
#define S16_Bank_Detect_Thres2							1000
#define TRACK_WIDTH_F									1540
#define TRACK_WIDTH_R									1540