/*
*******************************************************************************
+------------------------------------------------------------------------------
| Directory KMC
| Filename  TD_ABS.par
| Project   MGH-60 Domestic
+------------------------------------------------------------------------------
*******************************************************************************
=> Revision Note
-------------------------------------------------------------------------------
*******************************************************************************
*/
/******************************************************************************
* Discription : Selectable Factors for Car variant MACROs
*******************************************************************************
*   __TCMF_ENABLE                   __ESP_PLUSE only
*   __4WD_VEHICLE                   ENABLE  or DISABLE
*   __DRIVE_TYPE                    FRONT_WHEEL_DRV or REAR_WHEEL_DRV
*   __ESV_ENABLE                    ENABLE  or DISABLE
*   __WHEEL_SENSOR_TYPE             PASSIVE or ACTIVE   or SMART
*   __WHEEL_SWAP_TYPE               NORMAL  or FLR_SWAP or RLR_SWAP
*   __TC0_PORT                      WFL or WFR or WRL or WRR
*   __TC1_PORT                      WFL or WFR or WRL or WRR
*   __TC2_PORT                      WFL or WFR or WRL or WRR
*   __TC3_PORT                      WFL or WFR or WRL or WRR
*   __VSO_TYPE                      SW0_GEN_TYPE or HARD_WIRE_TYPE
*   __VSO_WHEEL_OUT_SEL             GM_VSO_WHEEL or HMC_VSO_WHEEL or ALL_WHEEL 
*   __STEER_SENSOR_TYPE             NONE    or ANALOG_TYPE  or CAN_TYPE
*   __YAW_SENSOR_TYPE               NONE    or ANALOG_TYPE  or CAN_TYPE
*   __YAW_SENSOR_DIRECTION          FORWARD or BACKWARD
*   __MP_SENSOR_TYPE                NONE    or _200BAR  or 170BAR
*   __G_SENSOR_ENABLE               ENABLE  or DISABLE
*   __G_SENSOR_DIRECTION            FORWARD or BACKWARD
*   __G_SENSOR_TYPE                 NONE    or ANALOG_TYPE  or CAN_TYPE
*   __VACUUM_SENSOR_ENABLE          ENABLE  or DISABLE
*   __ESS_RELAY_ENABLE              ENABLE  or DISABLE
*   __HDC_SW_TYPE                   NONE    or ANALOG_TYPE  or CAN_TYPE
*   __PARK_BRAKE_ENABLE             ENABLE  or DISABLE
*   __BRAKE_FLUID_LEVEL             ENABLE  or DISABLE
*   __BRAKE_SWITCH_ENABLE           ENABLE  or DISABLE
*   __BLS_TYPE                      NONE    or ANALOG_TYPE  or CAN_TYPE
*   __HAZARD_SWITCH_TYPE            NONE    or ANALOG_TYPE  or CAN_TYPE
*   __DIAG_TYPE                     K_LINE  or CAN_LINE
*   __HMC_CAN_VER                   CAN_1_0 or CAN_1_1  or CAN_1_2 or CAN_1_3 or CAN_1_4 or CAN_1_6
*   GMLAN_ENABLE                    ENABLE  or DISABLE
*   __LAMP_DRV_TYPE                 NONE    or ANALOG_TYPE  or CAN_TYPE
*   __ABS_LAMP_TYPE                 SEPERATE or COMMON
*   __FUNC_LAMP_ENABLE              ENABLE  or DISABLE
*   __HDC_LAMP_ENABLE               ENABLE  or DISABLE
*   __ESC_OFF_SWITCH_TYPE           NONE    or ANALOG_TYPE  or CAN_TYPE
*   __ESC_OFF_LAMP_ENABLE           ENABLE  or DISABLE
*   __ESC_WARNING_LAMP_DRV_TYPE     PASSIVE or ACTIVE
*   __ESC_OFF_LAMP_DRV_TYPE         PASSIVE or ACTIVE
*   __CUSTOMER_DTC                  GM_DTC  or HMC_DTC
*   __HSA                           ENABLE  or DISABLE
*   __HDC                           ENABLE  or DISABLE
*   __ROP                           ENABLE  or DISABLE
*   __VES                           ENABLE  or DISABLE
*   __ESS                           ENABLE  or DISABLE
*   __SPAS                          ENABLE  or DISABLE
*   __EDC                           ENABLE  or DISABLE
*   __SPARE_WHEEL                   ENABLE  or DISABLE
******************************************************************************/

#define __TCMF_ENABLE               DISABLE

#define __4WD_VEHICLE               DISABLE
#define __DRIVE_TYPE                FRONT_WHEEL_DRV

/* HECU  specification */
#define __ESV_ENABLE                DISABLE

/* SENSOR specification*/
#define __WHEEL_SENSOR_TYPE         ACTIVE
// #define __WHEEL_SWAP_TYPE           NORMAL

#define __TC0_PORT                  WFR
#define __TC1_PORT                  WFL
#define __TC2_PORT                  WRR
#define __TC3_PORT                  WRL

/*VSO Specification*/
#define __VSO_TYPE                  HARD_WIRE_TYPE   /*4000pulse/mile, HARD_WIRE_TYPE*/
#define __VSO_WHEEL_OUT_SEL         HMC_VSO_WHEEL     /*DRIVEN_WHEEL, NONDRIVEN_WHEEL,ALL_WHEEL*/


#define __STEER_SENSOR_TYPE         NONE
#define __YAW_SENSOR_TYPE           NONE
#define __YAW_SENSOR_DIRECTION      FORWARD
#define __MP_SENSOR_TYPE            NONE
#define __G_SENSOR_ENABLE           DISABLE
#define __G_SENSOR_DIRECTION        FORWARD
#define __G_SENSOR_TYPE             NONE
#define __VACUUM_SENSOR_ENABLE      DISABLE

/* SWITCH specification */
#define __HDC_SW_TYPE               NONE
#define __PARK_BRAKE_ENABLE         DISABLE
#define __BRAKE_FLUID_LEVEL         DISABLE
#define __BRAKE_SWITCH_ENABLE       DISABLE
#define __BLS_TYPE                  ANALOG_TYPE

/* Communication specification */
#define __DIAG_TYPE                 CAN_LINE
#define __HMC_CAN_VER               CAN_1_6
#define GMLAN_ENABLE                DISABLE

/* Lamp spcification */
#define __LAMP_DRV_TYPE             CAN_TYPE
#define __ABS_LAMP_TYPE             COMMON
#define __FUNC_LAMP_ENABLE          DISABLE
#define __HDC_LAMP_ENABLE           DISABLE
#define __ESC_OFF_SWITCH_TYPE       NONE
#define __ESC_OFF_LAMP_ENABLE       NONE
#define __ESC_WARNING_LAMP_DRV_TYPE DISABLE
#define __ESC_OFF_LAMP_DRV_TYPE     NONE

/* DTC specification */
#define __CUSTOMER_DTC              HMC_DTC

/* VAF specification */
#define __HEV                       DISABLE
#define __HSA                       DISABLE
#define __HDC                       DISABLE
#define __ROP                       DISABLE
#define __VES                       DISABLE
#define __EDC                       DISABLE
#define __SPAS                      DISABLE

#if __ESS_ENABLE == ENABLE
 #define __ESS                       ENABLE
 #define __HAZARD_SWITCH_TYPE        ANALOG_TYPE
 #define __ESS_RELAY_ENABLE          ENABLE
#else
 #define __ESS                       DISABLE
 #define __HAZARD_SWITCH_TYPE        NONE
 #define __ESS_RELAY_ENABLE          DISABLE
#endif

/* Tire specification */
#define __SPARE_WHEEL                ENABLE

#define __FLEX_BRAKING_MODE             DISABLE
