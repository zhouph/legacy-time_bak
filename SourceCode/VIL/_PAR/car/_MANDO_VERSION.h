#include "MANDO_VERSION.h"
#include "car.par"

#pragma CONST_SEG NON_BANKED

/*///////////////////////////////////////////////////////////////////////////*/
/*   HMC RELEASE VERSION */
/*///////////////////////////////////////////////////////////////////////////*/

#define vCarName	"HM   "
#define vSystem		"ESP"
#define vDriveMode	"4WD"
#define vPROTO		
#define vCAR_ID	    "23"  
#define vSYSTEM_CODE	"32"
#define vLogicVer   	"AA"
#define vFailsafeVer    "BB"
#define vDaigVer		"CC"
#define vReleasData		"6C12"   

/*///////////////////////////////////////////////////////////////////////////*/
/*   MANDO ENGINEER VERSION */
/*///////////////////////////////////////////////////////////////////////////*/

#define LOGIC_VERSION		{"01","A0","6524"}
#define SYSTEM_VERSION      {"02","A1","6523"}
#define FAILSAFE_VERSION	{"03","A2","6522"}
#define CAN_VERSION			{"04","A3","6521"}
#define DIAG_VERSION		{"05","A4","6520"}
#define TUNNING_VERSION		{"06","A5","6519"}
#define BOOT_VERSION		{"07","B0","6520"}


const MANDO_ENGINEER_VERSION_t MandoEngVer =
{
	__CAR,
	__VARIANT_CODE_ENABLE,
	#if !__VARIANT_CODE_ENABLE
	__4WD,
	__4WD_DRIVE_TYPE,
	#else
	0,0,
	#endif
	__DRIVE_TYPE,
	__SUSPENSION_TYPE,
	__BOOT_DRV_TYPE,
	__ECU,
	{__EBD,__ABS,__BTC,__TCS,__VDC,0},
	{__HDC,__HSA,__ROP,__EDC,__ETC,__ACC,__UCC,__TSP},
	{__STEER_SENSOR_TYPE,__WHEEL_SENSOR_TYPE,__YAW_SENSOR_TYPE,__MP_SENSOR_TYPE},
	{__HDC_SW_TYPE,__PARK_BRAKE_ENABLE,__HDC_LAMP_ENABLE,__ABS_LAMP_TYPE,0},
	{__HMC_CAN_VER,__DIAG_TYPE,__CLUSTER_TYPE,__CCP_ENABLE,0},
	{__LOGGER,__LOGGER_CHANNEL,__LOGGER_DATA,__MECU_MODE,__AX_SENSOR,0},
	LOGIC_VERSION,		
	SYSTEM_VERSION,     
	FAILSAFE_VERSION,   
	CAN_VERSION,        
	DIAG_VERSION,       
	TUNNING_VERSION,   
	BOOT_VERSION 	
};  

const HMC_VERSION_t HmcVerInfo=
{
	vCarName,
	vSystem,
	vDriveMode,
	" ",		/*Reserved */
	#if defined (vPROTO)
	"x",
	#endif
	vCAR_ID,
	vSYSTEM_CODE,
	vLogicVer,
	vFailsafeVer,
	vDaigVer,
	#if !defined(vPROTO)
	"-"
	#endif 
	" ",
	"-",
	vReleasData,
};	
	
#pragma CONST_SEG default
