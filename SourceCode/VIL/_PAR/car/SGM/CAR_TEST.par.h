/*
*******************************************************************************
+------------------------------------------------------------------------------
| Directory Par
| Filename  CAR_TEST.par
| Project   MGH80 INTERNATIONAL
+------------------------------------------------------------------------------
*******************************************************************************
=> Revision Note
-------------------------------------------------------------------------------
*******************************************************************************
*/

#ifndef __CAR_TEST_PAR
#define __CAR_TEST_PAR


/* 상기 MACRO ENABLE시 "//" 를 없애주시기 바랍니다.*/
//#define __UCC_1
//#define __IDC_System
//#define __H2DCL
//#define __CAN_SW
//#define __TC_PWM_ENABLE
//#define __EPB_INTERFACE
//#define __ACC_RealDevice_INTERFACE         //NF 차종에서 실제 ACC RealDevice interface
//#define __FAILSAFE_SIGNAL_TEST_SUBCAN
//#define __FAILSAFE_SIGNAL_TEST
//#define __FAILSAFE_SIGNAL_TEST_MAINCAN
//#define __MECU_MODE
//#define __CAN_DIAG_SUB_CHANNEL_ENABLE
//#define __MATLAB_COMPILE
//#define __STEER_REVERSE_DIRECTION
//#define __BRAKE_LAMP_CHECK_DISABLE
// #define GM_MODE_ENABLE

#ifdef __FAILSAFE_SIGNAL_TEST
    #define __Fault_Injection_Test
#endif

#define __Sim_Valve_TEST

//#define __DDS
//#define __DDS_PLUS
//#define __DDS_RESET_BY_ESC_SW      //No Control Box
//#define __DDS_FS_SIG

#if __STEER_SENSOR_TYPE == CAN_TYPE
    #if (__CAR==GM_C100&& __4WD==ENABLE) || ( __CAR == HMC_JM && __4WD==ENABLE) || (__CAR==DCX_COMPASS) || (__CAR == GM_SILVERADO)
        #define __STEER_REVERSE_DIRECTION
    #else
//        #define __STEER_REVERSE_DIRECTION
    #endif
#endif

#if __ECU==ESP_ECU_1
    #define __SAVE_FREEZE_FRAME
#elif __ECU==ABS_ECU_1
    //#define __SAVE_FREEZE_FRAME
#endif


#define __LOGGER
#if defined(__LOGGER)
    //#define __EXTEND_LOGGER
    //#define __OEM_LOGGER

    #if __ECU==ESP_ECU_1
        #if __CAR_MAKER==GM_KOREA
            #define __LOGGER_MAIN_CHANNEL_ENABLE
            //#define __LOGGER_SUB_CHANNEL_ENABLE      // default
        #else
            #define __LOGGER_MAIN_CHANNEL_ENABLE     // select
        //    #define __LOGGER_SUB_CHANNEL_ENABLE    // select
        #endif
    #else
        #define __LOGGER_MAIN_CHANNEL_ENABLE
    #endif
    
    #define SELECTED_CH_NUMBER_OF_TX_MESSAGE    10
#endif

/* VX1000 Logging enable */
//#define __VX1000_LOGGING_ENABLE
//#define __CCP_ENABLE
//#define __DURABILITY_TEST_ENABLE

//#define SYS_CLKOUT_TEST

//# define __DEBUG_FW

/* 임시 */
/* Integrated Yaw&G Sensor */
#define __YAW_G_LG_INTEGRATED       DISABLE         // Enable : integrated Yaw&G Sensor
#if __YAW_G_LG_INTEGRATED==ENABLE
    #define __INTEGRATED_X_Y_SWAP           DISABLE         // Enable : Ay&Ax Sensor Swap
    #define __YAW_SENSOR_REVERSE_ENABLE     DISABLE        // Enable : Yaw Sensor Direction 방향이 반대인 경우.
    #define __ESCi_AY_ANTI_DIRECTION        ENABLE
    #define __ESCi_AX_ANTI_DIRECTION        DISABLE

    #define __ESCi_SENSOR_USE_MAIN_SENSOR   DISABLE  // Enable , ESCi Sensor 를 ESC ECU 의 main Sensor 로 사용.

    /*ESCi 내부 센서 소자 선택*/
    //#define _SD767_IC_INSTALLED                      //SD787 사용 시
    #define _SCC1220_IC_INSTALLED                   //SCC1220 사용 시
    #define _YAW_G_LG_DMA_SPI1                       // RevB Smpl 에서 MCU PinMap 변경

    #define _ESCi_ENA    __YAW_G_LG_INTEGRATED  // ESCi

    #if _ESCi_ENA==ENABLE
        #define __ESCi_CONTROL  ENABLE
    #endif

#endif


/*****************************************************************************************
DEBUG_MODE 1 : Motor Test
*****************************************************************************************/
#endif  /* __CAR_TEST_PAR */
