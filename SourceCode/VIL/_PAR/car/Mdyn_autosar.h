#if defined(__AUTOSAR_HEADER_ARCH_ENA)
	/* Full DSC function only enabled */
	#define SEC_Fs_ErrorHandling							SECTION_II	//SECTION_I //hsh121204
	#define SEC_Fs_FAAStrHWCheck                            SECTION_III	//SECTION_I	//hsh121204
	#define SEC_Fs_FAAStrSensorCalc                         SECTION_III	//SECTION_I	//hsh121204
	#define SEC_Fs_FAAStrSignalCheck                        SECTION_III //SECTION_I	//hsh121204
	#define SEC_Fs_FADStrHWCheck                            SECTION_II	//SECTION_I	//hsh121204
	#define SEC_Fs_FADStrSensorCalc                         SECTION_II	//SECTION_I	//hsh121204
	#define SEC_Fs_FADStrSignalCheck                        SECTION_DEFAULT	//SECTION_I	//hsh121204
	#define SEC_Fs_FBIgnCheck                               SECTION_DEFAULT	//SECTION_I	//hsh121204
	#define SEC_Fs_FCVacumnSenCheck                         SECTION_I
	#define SEC_Fs_FEErrorProcessing                        SECTION_I
	#define SEC_Fs_FGAXSensorCheck                          SECTION_I
	#define SEC_Fs_FIAsicCheck                              SECTION_I
	#define SEC_Fs_FLLampRequest                            SECTION_II	//SECTION_I
	#define SEC_Fs_FOSenPowerCheck                          SECTION_I
	#define SEC_Fs_FPBlsSignalCheck                         SECTION_I
	#define SEC_Fs_FPMcpBlsHWCheck                          SECTION_I
	#define SEC_Fs_FPMcpBlsSensorCalc                       SECTION_I
	#define SEC_Fs_FPMcpBlsSignalCheck                      SECTION_I
	#define SEC_Fs_FRValveRelayCheck                        SECTION_I
	#define SEC_Fs_FSSwitchCheck                            SECTION_I
	#define SEC_Fs_FTMotorCheck                             SECTION_I
	#define SEC_Fs_FVSolAtvCheck                            SECTION_II
	#define SEC_Fs_FVSolPsvCheck                            SECTION_I
	#define SEC_Fs_FWSpeedCalc                              SECTION_I
	#define SEC_Fs_FWSpeedCheck                             SECTION_I
	#define SEC_Fs_FYLgYawHWCheck                           SECTION_I
	#define SEC_Fs_FYLgYawSensorCalc                        SECTION_I
	#define SEC_Fs_FYLgYawSignalCheck                       SECTION_I
	#define SEC_Fs_F_ActuatorLinkSet                        SECTION_III
	#define SEC_Fs_F_CommFunc                               SECTION_I
	#define SEC_Fs_F_MainFailsafe                           SECTION_I
	#define SEC_Fs_F_MECUFunction                           SECTION_I
	#define SEC_Fs_F_SensorHwCheck                          SECTION_III
	#define SEC_Fs_F_SensorSignalModel                      SECTION_I
	#define SEC_Fs_F_SensorSwCheck                          SECTION_I
	#define SEC_Fs_F_SignalTest                             SECTION_III
	#define SEC_Fs_F_VariableLinkSet                        SECTION_I
	
	//N0 New allocation	
	#define SEC_Fs_F_MOC_InterfaceLinkData                  SECTION_II
	#define SEC_Fs_FA_MOC_ActuatorCheck                     SECTION_III 
	#define SEC_Fs_F_LoggerBufferSet                        SECTION_II
	#define SEC_Fs_FCPedalSenCheck                          SECTION_III
	#define SEC_Fs_FI_MOC_HallCountSensorCheck              SECTION_II
	#define SEC_Fs_FI_MOC_SwitchCheck                       SECTION_II
	#define SEC_Fs_FS_MOC_CurrentSenCheck                   SECTION_III 
	#define SEC_Fs_FS_MOC_ForceSensorCheck                  SECTION_II
	#define SEC_Fs_8B20_FS_HW_VIEW_CHK                      SECTION_II 
	
	#define SEC_Diag_CDCL_LinkData                          SECTION_III
	#define SEC_Diag_CDCS_DiagService                       SECTION_III
	#define SEC_Diag_CDK_DiagService                        SECTION_III
	#define SEC_Diag_CDS_ReadRecordValue                    SECTION_II
	#define SEC_CL_btcs_Neo                                 SECTION_I
	#define SEC_CL_Hardware_Control                         SECTION_I
	#define SEC_CL_hm_logic_var                             SECTION_III
	#define SEC_CL_LAABSCallActCan                          SECTION_III
	#define SEC_CL_LAABSCallActHW                           SECTION_III
	#define SEC_CL_LAABSCallHWTest                          SECTION_III
	#define SEC_CL_LAABSCallHWTest_orig                     SECTION_III
	#define SEC_CL_LAABSCallMotorSpeedControl               SECTION_III
	#define SEC_CL_LAABSCallMotorSpeedControl_on_off        SECTION_III
	#define SEC_CL_LAABSGenerateLFCDuty                     SECTION_III
	#define SEC_CL_LAACCCallActCan                          SECTION_III
	#define SEC_CL_LAACCCallActHW                           SECTION_III
	#define SEC_CL_LAARBCallActHW                           SECTION_III
	#define SEC_CL_LAAVHCallActHW                           SECTION_DEFAULT
	#define SEC_CL_LABACallActHW                            SECTION_DEFAULT
	#define SEC_CL_LABDWCallActHW                           SECTION_DEFAULT
	#define SEC_CL_LACallMain                               SECTION_DEFAULT
	#define SEC_CL_LACBCCallActHW                           SECTION_DEFAULT
	#define SEC_CL_LADECCallActCan                          SECTION_DEFAULT
	#define SEC_CL_LADECCallActHW                           SECTION_DEFAULT
	#define SEC_CL_LAEBPCallActHW                           SECTION_DEFAULT
	#define SEC_CL_LAEDCCallActCan                          SECTION_DEFAULT
	#define SEC_CL_LAEPBCallActHW                           SECTION_DEFAULT
	#define SEC_CL_LAEPCCallActHW                           SECTION_DEFAULT
	#define SEC_CL_LAESPActuateNcNovalveF                   SECTION_DEFAULT
	#define SEC_CL_LAESPCallActCan                          SECTION_III
	#define SEC_CL_LAESPCallActHW                           SECTION_III
	#define SEC_CL_LAFBCCallActHW                           SECTION_DEFAULT
	#define SEC_CL_LAHBBCallActHW                           SECTION_DEFAULT
	#define SEC_CL_LAHDCCallActHW                           SECTION_DEFAULT
	#define SEC_CL_LAHRBCallActHW                           SECTION_DEFAULT
	#define SEC_CL_LAHSACallActHW                           SECTION_DEFAULT
	#define SEC_CL_LAPBACallActHW                           SECTION_DEFAULT
	#define SEC_CL_LASLSCallActHW                           SECTION_DEFAULT
	#define SEC_CL_LATCSCallActCan                          SECTION_DEFAULT
	#define SEC_CL_LATCSCallActHW                           SECTION_DEFAULT
	#define SEC_CL_LATSPCallActHW                           SECTION_DEFAULT
	#define SEC_CL_Lbtcs_Neo                                SECTION_DEFAULT
	#define SEC_CL_LCABSCallControl                         SECTION_DEFAULT
	#define SEC_CL_LCABSCallESPCombination                  SECTION_DEFAULT
	#define SEC_CL_LCABSDecideCtrlState                     SECTION_DEFAULT
	#define SEC_CL_LCABSDecideCtrlVariables                 SECTION_I
	#define SEC_CL_LCACCCallControl                         SECTION_I
	#define SEC_CL_LCAFSCallControl                         SECTION_I
	#define SEC_CL_LCallMain                                SECTION_I
	#define SEC_CL_LCARBCallControl                         SECTION_I
	#define SEC_CL_LCAVHCallControl                         SECTION_I
	#define SEC_CL_LCBDWCallControl                         SECTION_I
	#define SEC_CL_LCCallMain                               SECTION_I
	#define SEC_CL_LCCBCCallControl                         SECTION_DEFAULT
	#define SEC_CL_LCCDCCallControl                         SECTION_DEFAULT
	#define SEC_CL_LCDECCallControl                         SECTION_I
	#define SEC_CL_LCEBDCallControl                         SECTION_I
	#define SEC_CL_LCEBPCallControl                         SECTION_DEFAULT
	#define SEC_CL_LCEDCCallControl                         SECTION_I
	#define SEC_CL_LCEPBCallControl                         SECTION_III
	#define SEC_CL_LCEPCCallControl                         SECTION_DEFAULT
	#define SEC_CL_LCESPCalInterpolation                    SECTION_III
	#define SEC_CL_LCESPCallControl                         SECTION_DEFAULT
	#define SEC_CL_LCESPCallEngineControl                   SECTION_III
	#define SEC_CL_LCESPCallPartialControl                  SECTION_III
	#define SEC_CL_LCESPCalLpf                              SECTION_I
	#define SEC_CL_LCESPCallPressureControl                 SECTION_III
	#define SEC_CL_LCESPInterfaceSlipController             SECTION_I
	#define SEC_CL_LCETCCallControl                         SECTION_I
	#define SEC_CL_LCFBCCallControl                         SECTION_DEFAULT
	#define SEC_CL_LCHBBCallControl                         SECTION_I
	#define SEC_CL_LCHDCCallControl                         SECTION_I
	#define SEC_CL_LCHRBCallControl                         SECTION_III
	#define SEC_CL_LCHSACallControl                         SECTION_DEFAULT
	#define SEC_CL_LCPBACallControl                         SECTION_III
	#define SEC_CL_LCROPCallControl                         SECTION_DEFAULT
	#define SEC_CL_LCSLSCallControl                         SECTION_III
	#define SEC_CL_LCTCSCallControl                         SECTION_II
	#define SEC_CL_LCTSPCallControl                         SECTION_I
	#define SEC_CL_LDABSCallDctForVref                      SECTION_III
	#define SEC_CL_LDABSCallDetection                       SECTION_I
	#define SEC_CL_LDABSCallEstVehDecel                     SECTION_DEFAULT
	#define SEC_CL_LDABSCallSideVref                        SECTION_III
	#define SEC_CL_LDABSCallVrefCompForESP                  SECTION_I
	#define SEC_CL_LDABSCallVrefEst                         SECTION_III
	#define SEC_CL_LDABSCallVrefMainFilterProcess           SECTION_DEFAULT
	#define SEC_CL_LDABSDctRoadGradient                     SECTION_III
	#define SEC_CL_LDABSDctRoadSurface                      SECTION_DEFAULT
	#define SEC_CL_LDABSDctVehicleStatus                    SECTION_DEFAULT
	#define SEC_CL_LDABSDctWheelStatus                      SECTION_DEFAULT
	#define SEC_CL_LDAFSCallDetection                       SECTION_DEFAULT
	#define SEC_CL_LDBDWCallDetection                       SECTION_DEFAULT
	#define SEC_CL_LDCallMain                               SECTION_III
	#define SEC_CL_LDCDCCallDetection                       SECTION_III
	#define SEC_CL_LDEBPCallDetection                       SECTION_III
	#define SEC_CL_LDEDCCallDetection                       SECTION_DEFAULT
	#define SEC_CL_LDENGCallDetection                       SECTION_DEFAULT
	#define SEC_CL_LDENGEstPowertrain                       SECTION_III
	#define SEC_CL_LDEPCCallDetection                       SECTION_DEFAULT
	#define SEC_CL_LDESPCallDetection                       SECTION_DEFAULT
	#define SEC_CL_LDESPCallPartialDetection                SECTION_III
	#define SEC_CL_LDESPCalVehicleModel                     SECTION_DEFAULT
	#define SEC_CL_LDESPCompDeltaYaw                        SECTION_DEFAULT
	#define SEC_CL_LDESPCompDeltaYawDot                     SECTION_DEFAULT
	#define SEC_CL_LDESPCompDetDeltaYaw                     SECTION_DEFAULT
	#define SEC_CL_LDESPCompThreshold                       SECTION_III
	#define SEC_CL_LDESPDetectRoad                          SECTION_DEFAULT
	#define SEC_CL_LDESPDetectVehicleStatus                 SECTION_DEFAULT
	#define SEC_CL_LDESPDetectWheelState                    SECTION_I
	#define SEC_CL_LDESPEstBeta                             SECTION_DEFAULT
	#define SEC_CL_LDESSCallEmergencyBrkDet                 SECTION_DEFAULT
	#define SEC_CL_LDFBCCallDetection                       SECTION_II
	#define SEC_CL_LDPBAEstPressure                         SECTION_II
	#define SEC_CL_LDROPCallDetection                       SECTION_II
	#define SEC_CL_LDRTACallDetection                       SECTION_II
	#define SEC_CL_LDTCSCallDetection                       SECTION_II
	#define SEC_CL_LDTSPCallDetection                       SECTION_DEFAULT
	#define SEC_CL_LOABSCallCoordinator                     SECTION_I
	#define SEC_CL_LOCallMain                               SECTION_II
	#define SEC_CL_LOESPCallCoordinator                     SECTION_DEFAULT
	#define SEC_CL_LOTCSCallCoordinator                     SECTION_DEFAULT
	#define SEC_CL_LSABSCallSensorSignal                    SECTION_III
	#define SEC_CL_LSABSEstLongAccSensorOffsetMon           SECTION_I
	#define SEC_CL_LSAFSCallSensorSignal                    SECTION_I
	#define SEC_CL_LSAVHCallLogData                         SECTION_II
	#define SEC_CL_LSCallLogData                            SECTION_I
	#define SEC_CL_LSCallLogData_orig                       SECTION_I
	#define SEC_CL_LSCallMain                               SECTION_II
	#define SEC_CL_LSCDCCallSensorSignal                    SECTION_I
	#define SEC_CL_LSENGCallSensorSignal                    SECTION_I
	#define SEC_CL_LSESPCallBetaLogData                     SECTION_II
	#define SEC_CL_LSESPCallSensorSignal                    SECTION_I
	#define SEC_CL_LSESPCalSensorOffset                     SECTION_III
	#define SEC_CL_LSESPCalSlipRatio                        SECTION_II
	#define SEC_CL_LSESPCheckEspError                       SECTION_I
	#define SEC_CL_LSESPFilterEspSensor                     SECTION_II
	#define SEC_CL_LSFBCCallLogData                         SECTION_I
	#define SEC_CL_LSFBCCallSensorSignal                    SECTION_II
	#define SEC_CL_LSHDCCallLogData                         SECTION_II
	#define SEC_CL_LSMSCCallLogData                         SECTION_I
	#define SEC_CL_LSPBACallSensorSignal                    SECTION_II
	#define SEC_CL_LSTCSCallSensorSignal                    SECTION_II
	#define SEC_CL_Ventil                                   SECTION_I
	#define SEC_CL_LATVBBCallActHW                          SECTION_II //2011.08.23
	#define SEC_CL_LCEVPCallControl                         SECTION_II //2011.08.23
	#define SEC_CL_LCRDCMCallControl                        SECTION_II //2011.08.23
	#define SEC_CL_LCTVBBCallControl                        SECTION_II //2011.08.23
	#define SEC_CL_LDTVBBCallDetection                      SECTION_II //2011.08.23
	#define SEC_Fw_Brake_Main                               SECTION_II
	#define SEC_Fw_main                                     SECTION_II
	#define SEC_Fw_WChckRsrc                                SECTION_I
	#define SEC_Fw_WContlADC                                SECTION_III
	#define SEC_Fw_WContlEEP                                SECTION_III
	#define SEC_Fw_WContlLamp                               SECTION_III
	#define SEC_Fw_WContlLogger                             SECTION_II
	#define SEC_Fw_WContlSnr                                SECTION_II
	#define SEC_Fw_WContlSubFunc                            SECTION_I
	#define SEC_Fw_WContlSwtch                              SECTION_III
	#define SEC_Fw_WDecSenSig                               SECTION_I
	#define SEC_Fw_WDrv_Motor                               SECTION_III
	#define SEC_Fw_WDrv_Valve                               SECTION_III
	#define SEC_Fw_WInterfaceHallSensor                     SECTION_DEFAULT
	#define SEC_Fw_WItf_PwrSgnl                             SECTION_DEFAULT	//SECTION_III //hsh121204
	#define SEC_Fw_WDrv_ADC                                 SECTION_DEFAULT
	#define SEC_Fw_WDrv_AluCheck                            SECTION_DEFAULT
	#define SEC_Fw_WDrv_CAN                                 SECTION_DEFAULT
	#define SEC_Fw_WDrv_CommAsic                            SECTION_DEFAULT
	#define SEC_Fw_WDrv_EEP                                 SECTION_II
	#define SEC_Fw_WDrv_EscAsic                             SECTION_II
	#define SEC_Fw_WDrv_ETIMER                              SECTION_II
	#define SEC_Fw_WDrv_PIT                                 SECTION_III
	#define SEC_Fw_WDrv_PWM                                 SECTION_III
	#define SEC_Fw_WDrv_REGISTER                            SECTION_I
	#define SEC_Fw_WDrv_SPI                                 SECTION_I
	#define SEC_Fw_MPC55xx_init                             SECTION_I
	#define SEC_Fw_MPC55xx_init_debug                       SECTION_I
	#define SEC_Fw_MPC5643L_HWInit                          SECTION_I
	#define SEC_Fw_adc_init                                 SECTION_I
	#define SEC_Fw_dspi_init                                SECTION_I
	#define SEC_Fw_edma_init                                SECTION_I
	#define SEC_Fw_etimer_init                              SECTION_I
	#define SEC_Fw_flash_init                               SECTION_I
	#define SEC_Fw_flexcan_init                             SECTION_I
	#define SEC_Fw_flexpwm_init                             SECTION_I
	#define SEC_Fw_intc_init                                SECTION_I
	#define SEC_Fw_intc_sw_vecttable                        SECTION_I
	#define SEC_Fw_linflex_init                             SECTION_I
	#define SEC_Fw_msr_init                                 SECTION_I
	#define SEC_Fw_pit_init                                 SECTION_I
	#define SEC_Fw_rappid_utils                             SECTION_I
	#define SEC_Fw_romcopy                                  SECTION_I
	#define SEC_Fw_siu_init                                 SECTION_II
	#define SEC_Fw_stm_init                                 SECTION_II
	#define SEC_Fw_swt_init                                 SECTION_III
	#define SEC_Fw_sysclk_init                              SECTION_III
	#define SEC_Fw_sys_init                                 SECTION_III
	#define SEC_Fw_WDrv_ADXR800                             SECTION_III
	#define SEC_Fw_WDrv_SCA21XX                             SECTION_III
	#define SEC_Can_CCChkCan                                SECTION_I
	#define SEC_Can_CCDrvCan                                SECTION_I
	#define SEC_Can_CCDrvCanDiag                            SECTION_I
	#define SEC_Can_CCDrvLogger                             SECTION_I
	#define SEC_Can_CCDrvSubCan                             SECTION_I
	#define SEC_Can_CCTestFunc                              SECTION_I
	#define SEC_Can_CCDrvA13Can                             SECTION_I
	#define SEC_Can_CCDrvA21Can                             SECTION_I
	#define SEC_Can_CCDrvB12Can                             SECTION_I
	#define SEC_Can_CCDrvE101Can                            SECTION_I
	#define SEC_Can_CCDrvHMCCan                             SECTION_I
	#define SEC_Can_CCDrvHMC_HDCan                          SECTION_I
	#define SEC_Can_CCDrvSYCCan                             SECTION_I
	#define SEC_Can_CCDrvTestCarCan                         SECTION_I
	#define SEC_Can_CCHmcChkCan                             SECTION_I
	#define SEC_Can_CCDrvASTRACan                           SECTION_I
	#define SEC_Can_CCDrvC100Can                            SECTION_I
	#define SEC_Can_CCDrvFORDCan                            SECTION_I
	#define SEC_Can_CCDrvGMDWCan                            SECTION_I
	#define SEC_Can_CCDrvJ300Can                            SECTION_I
	#define SEC_Can_CCDrvKYRONCan                           SECTION_I
	#define SEC_Can_CCDrvRAXTONCan                          SECTION_I
	#define SEC_Can_CCDrvTAHOECan                           SECTION_I
	#define SEC_App_ETunePar                                SECTION_I
	#define SEC_App_LTunePar                                SECTION_DEFAULT
	#define SEC_Cal_BMW740i_TUNapCalAbs2WD   				SECTION_DEFAULT
	#define SEC_Cal_BMW740i_TUNapCalEsp2WD   				SECTION_DEFAULT
	#define SEC_Cal_BMW740i_TUNapCalAbsVafs2WD  			SECTION_DEFAULT
	#define SEC_Cal_BMW740i_TUNapCalEspEng01   				SECTION_DEFAULT

	#define SEC_Cal_BMW740i_TUNapCalEspVafs2WD              SECTION_DEFAULT           
	#define SEC_Cal_BMW740i_TUNapCalRbcOem2WD               SECTION_DEFAULT             
	
	#define SEC_vx1000						               	SECTION_I             

	/* New Logic Files are Added : 2012-09-10  */
	
	#define SEC_LAPACCallActHW								SECTION_I
	#define SEC_LASPASCallActHW								SECTION_I
	#define SEC_LCETSCCallControl							SECTION_II
	#define SEC_LCHRCCallControl							SECTION_II
	#define SEC_LCPACCallControl							SECTION_I
	#define SEC_LCSPASCallControl							SECTION_III
	#define SEC_LCTODCallControl							SECTION_III
	#define SEC_LDESPEstDiscTemp							SECTION_III
	#define SEC_LSESPInterfaceIVSS							SECTION_I

	
	
	
	
	#if idx_FILE==idx_Fs_ErrorHandling
		#if SEC_Fs_ErrorHandling==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_ErrorHandling==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_ErrorHandling==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_ErrorHandling==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Fs_FAAStrHWCheck                          
		#if SEC_Fs_FAAStrHWCheck==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_FAAStrHWCheck==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_FAAStrHWCheck==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_FAAStrHWCheck==SECTION_IV
			// default area 1
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif			
	#elif idx_FILE==idx_Fs_FAAStrSensorCalc                          
		#if SEC_Fs_FAAStrSensorCalc==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_FAAStrSensorCalc==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_FAAStrSensorCalc==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_FAAStrSensorCalc==SECTION_IV
			// default area 1
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Fs_FAAStrSignalCheck                          
		#if SEC_Fs_FAAStrSignalCheck==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_FAAStrSignalCheck==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_FAAStrSignalCheck==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_FAAStrSignalCheck==SECTION_IV
			// default area 1
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Fs_FADStrHWCheck                          
		#if SEC_Fs_FADStrHWCheck==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_FADStrHWCheck==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_FADStrHWCheck==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_FADStrHWCheck==SECTION_IV
			// default area 1
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Fs_FADStrSensorCalc                          
		#if SEC_Fs_FADStrSensorCalc==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_FADStrSensorCalc==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_FADStrSensorCalc==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_FADStrSensorCalc==SECTION_IV
			// default area 1
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Fs_FADStrSignalCheck                          
		#if SEC_Fs_FADStrSignalCheck==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_FADStrSignalCheck==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_FADStrSignalCheck==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_FADStrSignalCheck==SECTION_IV
			// default area 1
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	#elif idx_FILE==idx_Fs_FBIgnCheck                          
		#if SEC_Fs_FBIgnCheck ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_FBIgnCheck ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_FBIgnCheck ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_FBIgnCheck ==SECTION_IV
			// default area 1
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Fs_FCVacumnSenCheck                          
		#if SEC_Fs_FCVacumnSenCheck ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_FCVacumnSenCheck ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_FCVacumnSenCheck ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_FCVacumnSenCheck ==SECTION_IV
			// default area 1
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Fs_FEErrorProcessing                          
		#if SEC_Fs_FEErrorProcessing ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_FEErrorProcessing ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_FEErrorProcessing ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_FEErrorProcessing ==SECTION_IV
			// default area 1
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif						
	#elif idx_FILE==idx_Fs_FGAXSensorCheck                          
		#if SEC_Fs_FGAXSensorCheck ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_FGAXSensorCheck ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_FGAXSensorCheck ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_FGAXSensorCheck ==SECTION_IV
			// default area 1
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif	
	#elif idx_FILE==idx_Fs_FIAsicCheck                          
		#if SEC_Fs_FIAsicCheck ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_FIAsicCheck ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_FIAsicCheck ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_FIAsicCheck ==SECTION_IV
			// default area 1
		#else 
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Fs_FLLampRequest                          
		#if SEC_Fs_FLLampRequest ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_FLLampRequest ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_FLLampRequest ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_FLLampRequest ==SECTION_IV
			// default area 1
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Fs_FOSenPowerCheck                          
		#if SEC_Fs_FOSenPowerCheck ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_FOSenPowerCheck ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_FOSenPowerCheck ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_FOSenPowerCheck ==SECTION_IV
			// default area 1
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Fs_FPBlsSignalCheck                          
		#if SEC_Fs_FPBlsSignalCheck ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_FPBlsSignalCheck ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_FPBlsSignalCheck ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_FPBlsSignalCheck ==SECTION_IV
			// default area 1
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Fs_FPMcpBlsHWCheck                          
		#if SEC_Fs_FPMcpBlsHWCheck ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_FPMcpBlsHWCheck ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_FPMcpBlsHWCheck ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_FPMcpBlsHWCheck ==SECTION_IV
			// default area 1
		#else 
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Fs_FPMcpBlsSensorCalc                          
		#if SEC_Fs_FPMcpBlsSensorCalc ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_FPMcpBlsSensorCalc ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_FPMcpBlsSensorCalc ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_FPMcpBlsSensorCalc ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif													
	#elif idx_FILE==idx_Fs_FPMcpBlsSignalCheck                          
		#if SEC_Fs_FPMcpBlsSignalCheck ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_FPMcpBlsSignalCheck ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_FPMcpBlsSignalCheck ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_FPMcpBlsSignalCheck ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Fs_FRValveRelayCheck                          
		#if SEC_Fs_FRValveRelayCheck ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_FRValveRelayCheck ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_FRValveRelayCheck ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_FRValveRelayCheck ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Fs_FSSwitchCheck                          
		#if SEC_Fs_FSSwitchCheck ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_FSSwitchCheck ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_FSSwitchCheck ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_FSSwitchCheck ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Fs_FTMotorCheck                          
		#if SEC_Fs_FTMotorCheck ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_FTMotorCheck ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_FTMotorCheck ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_FTMotorCheck ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Fs_FVSolAtvCheck                          
		#if SEC_Fs_FVSolAtvCheck ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_FVSolAtvCheck ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_FVSolAtvCheck ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_FVSolAtvCheck ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Fs_FVSolPsvCheck                          
		#if SEC_Fs_FVSolPsvCheck ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_FVSolPsvCheck ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_FVSolPsvCheck ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_FVSolPsvCheck ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Fs_FWSpeedCalc                          
		#if SEC_Fs_FWSpeedCalc ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_FWSpeedCalc ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_FWSpeedCalc ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_FWSpeedCalc ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Fs_FWSpeedCheck                          
		#if SEC_Fs_FWSpeedCheck ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_FWSpeedCheck ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_FWSpeedCheck ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_FWSpeedCheck ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Fs_FYLgYawHWCheck                          
		#if SEC_Fs_FYLgYawHWCheck ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_FYLgYawHWCheck ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_FYLgYawHWCheck ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_FYLgYawHWCheck ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	#elif idx_FILE==idx_Fs_FYLgYawSensorCalc                          
		#if SEC_Fs_FYLgYawSensorCalc ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_FYLgYawSensorCalc ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_FYLgYawSensorCalc ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_FYLgYawSensorCalc ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	#elif idx_FILE==idx_Fs_FYLgYawSignalCheck                          
		#if SEC_Fs_FYLgYawSignalCheck ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_FYLgYawSignalCheck ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_FYLgYawSignalCheck ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_FYLgYawSignalCheck ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Fs_F_ActuatorLinkSet                          
		#if SEC_Fs_F_ActuatorLinkSet ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_F_ActuatorLinkSet ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_F_ActuatorLinkSet ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_F_ActuatorLinkSet ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif				
	#elif idx_FILE==idx_Fs_F_CommFunc                          
		#if SEC_Fs_F_CommFunc ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_F_CommFunc ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_F_CommFunc ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_F_CommFunc ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Fs_F_LoggerBufferSet                          
		#if SEC_Fs_F_LoggerBufferSet ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_F_LoggerBufferSet ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_F_LoggerBufferSet ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_F_LoggerBufferSet ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Fs_F_MainFailsafe                          
		#if SEC_Fs_F_MainFailsafe ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_F_MainFailsafe ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_F_MainFailsafe ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_F_MainFailsafe ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Fs_F_MECUFunction                          
		#if SEC_Fs_F_MECUFunction ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_F_MECUFunction ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_F_MECUFunction ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_F_MECUFunction ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Fs_F_SensorHwCheck                          
		#if SEC_Fs_F_SensorHwCheck ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_F_SensorHwCheck ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_F_SensorHwCheck ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_F_SensorHwCheck ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Fs_F_SensorSwCheck                          
		#if SEC_Fs_F_SensorSwCheck ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_F_SensorSwCheck ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_F_SensorSwCheck ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_F_SensorSwCheck ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Fs_F_SignalTest                          
		#if SEC_Fs_F_SignalTest ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_F_SignalTest ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_F_SignalTest ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_F_SignalTest ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Fs_F_VariableLinkSet                          
		#if SEC_Fs_F_VariableLinkSet ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_F_VariableLinkSet ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_F_VariableLinkSet ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_F_VariableLinkSet ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Diag_CDCL_LinkData                          
		#if SEC_Diag_CDCL_LinkData ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Diag_CDCL_LinkData ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Diag_CDCL_LinkData ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Diag_CDCL_LinkData ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Diag_CDCS_DiagService                          
		#if SEC_Diag_CDCS_DiagService ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Diag_CDCS_DiagService ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Diag_CDCS_DiagService ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Diag_CDCS_DiagService ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Diag_CDK_DiagService                          
		#if SEC_Diag_CDK_DiagService ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Diag_CDK_DiagService ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Diag_CDK_DiagService ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Diag_CDK_DiagService ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Diag_CDS_ReadRecordValue                          
		#if SEC_Diag_CDS_ReadRecordValue ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Diag_CDS_ReadRecordValue ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Diag_CDS_ReadRecordValue ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Diag_CDS_ReadRecordValue ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_btcs_Neo                          
		#if SEC_CL_btcs_Neo ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_btcs_Neo ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_btcs_Neo ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_btcs_Neo ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_Hardware_Control                          
		#if SEC_CL_Hardware_Control ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_Hardware_Control ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_Hardware_Control ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_Hardware_Control ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_hm_logic_var                          
		#if SEC_CL_hm_logic_var ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_hm_logic_var ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_hm_logic_var ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_hm_logic_var ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LAABSCallActCan                          
		#if SEC_CL_LAABSCallActCan ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LAABSCallActCan ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LAABSCallActCan ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LAABSCallActCan ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LAABSCallActHW                          
		#if SEC_CL_LAABSCallActHW ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LAABSCallActHW ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LAABSCallActHW ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LAABSCallActHW ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LAABSCallHWTest                          
		#if SEC_CL_LAABSCallHWTest ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LAABSCallHWTest ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LAABSCallHWTest ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LAABSCallHWTest ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LAABSCallMotorSpeedControl                          
		#if SEC_CL_LAABSCallMotorSpeedControl ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LAABSCallMotorSpeedControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LAABSCallMotorSpeedControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LAABSCallMotorSpeedControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LAABSCallMotorSpeedControl_on_off                          
		#if SEC_CL_LAABSCallMotorSpeedControl_on_off ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LAABSCallMotorSpeedControl_on_off ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LAABSCallMotorSpeedControl_on_off ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LAABSCallMotorSpeedControl_on_off ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LAABSGenerateLFCDuty                          
		#if SEC_CL_LAABSGenerateLFCDuty ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LAABSGenerateLFCDuty ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LAABSGenerateLFCDuty ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LAABSGenerateLFCDuty ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LAACCCallActCan                          
		#if SEC_CL_LAACCCallActCan ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LAACCCallActCan ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LAACCCallActCan ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LAACCCallActCan ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LAACCCallActHW                          
		#if SEC_CL_LAACCCallActHW ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LAACCCallActHW ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LAACCCallActHW ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LAACCCallActHW ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LAARBCallActHW                          
		#if SEC_CL_LAARBCallActHW ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LAARBCallActHW ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LAARBCallActHW ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LAARBCallActHW ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LAAVHCallActHW                          
		#if SEC_CL_LAAVHCallActHW ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LAAVHCallActHW ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LAAVHCallActHW ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LAAVHCallActHW ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LABACallActHW                          
		#if SEC_CL_LABACallActHW ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LABACallActHW ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LABACallActHW ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LABACallActHW ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LABDWCallActHW                          
		#if SEC_CL_LABDWCallActHW ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LABDWCallActHW ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LABDWCallActHW ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LABDWCallActHW ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LACallMain                          
		#if SEC_CL_LACallMain ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LACallMain ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LACallMain ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LACallMain ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LACBCCallActHW                          
		#if SEC_CL_LACBCCallActHW ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LACBCCallActHW ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LACBCCallActHW ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LACBCCallActHW ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LADECCallActCan                          
		#if SEC_CL_LADECCallActCan ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LADECCallActCan ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LADECCallActCan ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LADECCallActCan ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LADECCallActHW                          
		#if SEC_CL_LADECCallActHW ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LADECCallActHW ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LADECCallActHW ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LADECCallActHW ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LAEBPCallActHW                          
		#if SEC_CL_LAEBPCallActHW ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LAEBPCallActHW ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LAEBPCallActHW ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LAEBPCallActHW ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LAEDCCallActCan                          
		#if SEC_CL_LAEDCCallActCan ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LAEDCCallActCan ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LAEDCCallActCan ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LAEDCCallActCan ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LAEPBCallActHW                          
		#if SEC_CL_LAEPBCallActHW ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LAEPBCallActHW ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LAEPBCallActHW ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LAEPBCallActHW ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LAEPCCallActHW                          
		#if SEC_CL_LAEPCCallActHW ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LAEPCCallActHW ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LAEPCCallActHW ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LAEPCCallActHW ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LAESPActuateNcNovalveF                          
		#if SEC_CL_LAESPActuateNcNovalveF ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LAESPActuateNcNovalveF ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LAESPActuateNcNovalveF ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LAESPActuateNcNovalveFs ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LAESPCallActCan                          
		#if SEC_CL_LAESPCallActCan ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LAESPCallActCan ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LAESPCallActCan ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LAESPCallActCan ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LAESPCallActHW                          
		#if SEC_CL_LAESPCallActHW ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LAESPCallActHW ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LAESPCallActHW ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LAESPCallActHW ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LAFBCCallActHW                          
		#if SEC_CL_LAFBCCallActHW ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LAFBCCallActHW ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LAFBCCallActHW ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LAFBCCallActHW ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LAHBBCallActHW                          
		#if SEC_CL_LAHBBCallActHW ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LAHBBCallActHW ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LAHBBCallActHW ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LAHBBCallActHW ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LAHDCCallActHW                          
		#if SEC_CL_LAHDCCallActHW ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LAHDCCallActHW ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LAHDCCallActHW ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LAHDCCallActHW ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	#elif idx_FILE==idx_CL_LAHRBCallActHW                          
		#if SEC_CL_LAHRBCallActHW ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LAHRBCallActHW ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LAHRBCallActHW ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LAHRBCallActHW ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LAHSACallActHW                          
		#if SEC_CL_LAHSACallActHW ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LAHSACallActHW ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LAHSACallActHW ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LAHSACallActHW ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LAPBACallActHW                          
		#if SEC_CL_LAPBACallActHW ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LAPBACallActHW ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LAPBACallActHW ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LAPBACallActHW ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LASLSCallActHW                          
		#if SEC_CL_LASLSCallActHW ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LASLSCallActHW ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LASLSCallActHW ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LASLSCallActHW ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LATCSCallActCan                          
		#if SEC_CL_LATCSCallActCan ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LATCSCallActCan ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LATCSCallActCan ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LATCSCallActCan ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LATCSCallActHW                          
		#if SEC_CL_LATCSCallActHW ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LATCSCallActHW ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LATCSCallActHW ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LATCSCallActHW ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LATSPCallActHW                          
		#if SEC_CL_LATSPCallActHW ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LATSPCallActHW ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LATSPCallActHW ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LATSPCallActHW ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_Lbtcs_Neo                          
		#if SEC_CL_Lbtcs_Neo ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_Lbtcs_Neo ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_Lbtcs_Neo ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_Lbtcs_Neo ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCABSCallControl                          
		#if SEC_CL_LCABSCallControl ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCABSCallControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCABSCallControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCABSCallControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCABSCallESPCombination                          
		#if SEC_CL_LCABSCallESPCombination ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCABSCallESPCombination ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCABSCallESPCombination ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCABSCallESPCombination ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCABSDecideCtrlState                          
		#if SEC_CL_LCABSDecideCtrlState ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCABSDecideCtrlState ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCABSDecideCtrlState ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCABSDecideCtrlState ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCABSDecideCtrlVariables                          
		#if SEC_CL_LCABSDecideCtrlVariables ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCABSDecideCtrlVariables ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCABSDecideCtrlVariables ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCABSDecideCtrlVariables ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCACCCallControl                          
		#if SEC_CL_LCACCCallControl ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCACCCallControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCACCCallControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCACCCallControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCAFSCallControl                          
		#if SEC_CL_LCAFSCallControl ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCAFSCallControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCAFSCallControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCAFSCallControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCallMain                          
		#if SEC_CL_LCallMain ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCallMain ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCallMain ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCallMain ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCARBCallControl                          
		#if SEC_CL_LCARBCallControl ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCARBCallControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCARBCallControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCARBCallControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCAVHCallControl                          
		#if SEC_CL_LCAVHCallControl ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCAVHCallControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCAVHCallControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCAVHCallControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCBDWCallControl                          
		#if SEC_CL_LCBDWCallControl ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCBDWCallControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCBDWCallControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCBDWCallControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCCallMain                          
		#if SEC_CL_LCCallMain ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCCallMain ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCCallMain ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCCallMain ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCCBCCallControl                          
		#if SEC_CL_LCCBCCallControl ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCCBCCallControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCCBCCallControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCCBCCallControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCCDCCallControl                          
		#if SEC_CL_LCCDCCallControl ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCCDCCallControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCCDCCallControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCCDCCallControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCDECCallControl                          
		#if SEC_CL_LCDECCallControl ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCDECCallControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCDECCallControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCDECCallControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCEBDCallControl                          
		#if SEC_CL_LCEBDCallControl ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCEBDCallControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCEBDCallControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCEBDCallControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCEBPCallControl                          
		#if SEC_CL_LCEBPCallControl==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCEBPCallControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCEBPCallControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCEBPCallControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCEDCCallControl                          
		#if SEC_CL_LCEDCCallControl==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCEDCCallControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCEDCCallControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCEDCCallControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCEPBCallControl                          
		#if SEC_CL_LCEPBCallControl==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCEPBCallControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCEPBCallControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCEPBCallControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCEPCCallControl                          
		#if SEC_CL_LCEPCCallControl==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCEPCCallControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCEPCCallControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCEPCCallControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCESPCalInterpolation                          
		#if SEC_CL_LCESPCalInterpolation==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCESPCalInterpolation ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCESPCalInterpolation ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCESPCalInterpolation ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCESPCallControl                          
		#if SEC_CL_LCESPCallControl==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCESPCallControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCESPCallControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCESPCallControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCESPCallEngineControl                          
		#if SEC_CL_LCESPCallEngineControl==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCESPCallEngineControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCESPCallEngineControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCESPCallEngineControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCESPCallPartialControl                          
		#if SEC_CL_LCESPCallPartialControl==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCESPCallPartialControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCESPCallPartialControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCESPCallPartialControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCESPCalLpf                          
		#if SEC_CL_LCESPCalLpf==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCESPCalLpf ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCESPCalLpf ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCESPCalLpf ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCESPCallPressureControl                          
		#if SEC_CL_LCESPCallPressureControl==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCESPCallPressureControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCESPCallPressureControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCESPCallPressureControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCESPInterfaceSlipController                          
		#if SEC_CL_LCESPInterfaceSlipController==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCESPInterfaceSlipController ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCESPInterfaceSlipController ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCESPInterfaceSlipController ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCETCCallControl                          
		#if SEC_CL_LCETCCallControl==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCETCCallControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCETCCallControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCETCCallControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCFBCCallControl                          
		#if SEC_CL_LCFBCCallControl==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCFBCCallControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCFBCCallControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCFBCCallControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCHBBCallControl                          
		#if SEC_CL_LCHBBCallControl==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCHBBCallControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCHBBCallControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCHBBCallControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCHDCCallControl                          
		#if SEC_CL_LCHDCCallControl==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCHDCCallControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCHDCCallControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCHDCCallControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCHRBCallControl                          
		#if SEC_CL_LCHRBCallControl==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCHRBCallControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCHRBCallControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCHRBCallControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCHSACallControl                          
		#if SEC_CL_LCHSACallControl==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCHSACallControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCHSACallControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCHSACallControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCPBACallControl                          
		#if SEC_CL_LCPBACallControl==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCPBACallControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCPBACallControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCPBACallControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCROPCallControl                          
		#if SEC_CL_LCROPCallControl==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCROPCallControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCROPCallControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCROPCallControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCSLSCallControl                          
		#if SEC_CL_LCSLSCallControl==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCSLSCallControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCSLSCallControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCSLSCallControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCTCSCallControl                          
		#if SEC_CL_LCTCSCallControl==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCTCSCallControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCTCSCallControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCTCSCallControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LCTSPCallControl                          
		#if SEC_CL_LCTSPCallControl==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCTSPCallControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCTSPCallControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCTSPCallControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LDABSCallDctForVref                          
		#if SEC_CL_LDABSCallDctForVref ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDABSCallDctForVref  ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDABSCallDctForVref  ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDABSCallDctForVref  ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LDABSCallDetection                          
		#if SEC_CL_LDABSCallDetection ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDABSCallDetection  ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDABSCallDetection  ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDABSCallDetection  ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LDABSCallEstVehDecel                          
		#if SEC_CL_LDABSCallEstVehDecel==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDABSCallEstVehDecel==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDABSCallEstVehDecel==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDABSCallEstVehDecel==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LDABSCallVrefCompForESP                          
		#if SEC_CL_LDABSCallVrefCompForESP==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDABSCallVrefCompForESP==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDABSCallVrefCompForESP==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDABSCallVrefCompForESP==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LDABSCallVrefEst                           
		#if SEC_CL_LDABSCallVrefEst==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDABSCallVrefEst==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDABSCallVrefEst==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDABSCallVrefEst==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LDABSCallVrefMainFilterProcess                           
		#if SEC_CL_LDABSCallVrefMainFilterProcess==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDABSCallVrefMainFilterProcess==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDABSCallVrefMainFilterProcess==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDABSCallVrefMainFilterProcess==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LDABSDctRoadGradient                          /* 125 */
		#if SEC_CL_LDABSDctRoadGradient==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDABSDctRoadGradient==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDABSDctRoadGradient==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDABSDctRoadGradient==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LDABSDctRoadSurface                          /* 126 */
		#if SEC_CL_LDABSDctRoadSurface==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDABSDctRoadSurface==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDABSDctRoadSurface==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDABSDctRoadSurface==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LDABSDctVehicleStatus                          /* 127 */
		#if SEC_CL_LDABSDctVehicleStatus==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDABSDctVehicleStatus==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDABSDctVehicleStatus==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDABSDctVehicleStatus==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
		
	#elif idx_FILE==idx_CL_LDABSDctWheelStatus                          /* 128 */
		#if SEC_CL_LDABSDctWheelStatus==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDABSDctWheelStatus==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDABSDctWheelStatus==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDABSDctWheelStatus==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
		
	#elif idx_FILE==idx_CL_LDAFSCallDetection                          /* 129 */
		#if SEC_CL_LDAFSCallDetection==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDAFSCallDetection==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDAFSCallDetection==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDAFSCallDetection==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
		
	#elif idx_FILE==idx_CL_LDBDWCallDetection                          /* 130 */
		#if SEC_CL_LDBDWCallDetection==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDBDWCallDetection==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDBDWCallDetection==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDBDWCallDetection==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
		
	#elif idx_FILE==idx_CL_LDCallMain                            /* 131 */
		#if SEC_CL_LDCallMain==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDCallMain==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDCallMain==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDCallMain==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
		
	#elif idx_FILE==idx_CL_LDCDCCallDetection                            /* 132 */
		#if SEC_CL_LDCDCCallDetection==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDCDCCallDetection==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDCDCCallDetection==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDCDCCallDetection==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
		
	#elif idx_FILE==idx_CL_LDEBPCallDetection                            /* 133 */
		#if SEC_CL_LDEBPCallDetection ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDEBPCallDetection ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDEBPCallDetection ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDEBPCallDetection ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
		
	#elif idx_FILE==idx_CL_LDEDCCallDetection                            /* 134 */
		#if SEC_CL_LDEDCCallDetection ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDEDCCallDetection ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDEDCCallDetection ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDEDCCallDetection ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
		
	#elif idx_FILE==idx_CL_LDENGCallDetection                               /* 135 */
		#if SEC_CL_LDENGCallDetection ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDENGCallDetection ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDENGCallDetection ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDENGCallDetection ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
		
	#elif idx_FILE==idx_CL_LDENGEstPowertrain                               /* 136 */
		#if SEC_CL_LDENGEstPowertrain ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDENGEstPowertrain ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDENGEstPowertrain ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDENGEstPowertrain ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
		
	#elif idx_FILE==idx_CL_LDEPCCallDetection                               /* 137 */
		#if SEC_CL_LDEPCCallDetection ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDEPCCallDetection ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDEPCCallDetection ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDEPCCallDetection ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
		
	#elif idx_FILE==idx_CL_LDESPCallDetection                               /* 138 */
		#if SEC_CL_LDESPCallDetection ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDESPCallDetection ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDESPCallDetection ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDESPCallDetection ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LDESPCallPartialDetection                               /* 139 */
		#if SEC_CL_LDESPCallPartialDetection ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDESPCallPartialDetection ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDESPCallPartialDetection ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDESPCallPartialDetection ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LDESPCalVehicleModel                               /* 140 */
		#if SEC_CL_LDESPCalVehicleModel ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDESPCalVehicleModel ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDESPCalVehicleModel ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDESPCalVehicleModel ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
		
	#elif idx_FILE==idx_CL_LDESPCompDeltaYaw                               /* 141 */
		#if SEC_CL_LDESPCompDeltaYaw ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDESPCompDeltaYaw ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDESPCompDeltaYaw ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDESPCompDeltaYaw ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
		
	#elif idx_FILE==idx_CL_LDESPCompDeltaYawDot                               /* 142 */
		#if SEC_CL_LDESPCompDeltaYawDot ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDESPCompDeltaYawDot ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDESPCompDeltaYawDot ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDESPCompDeltaYawDot ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
		
	#elif idx_FILE==idx_CL_LDESPCompDetDeltaYaw                               /* 143 */
		#if SEC_CL_LDESPCompDetDeltaYaw ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDESPCompDetDeltaYaw ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDESPCompDetDeltaYaw ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDESPCompDetDeltaYaw ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LDESPCompThreshold                               /* 144 */
		#if SEC_CL_LDESPCompThreshold==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDESPCompThreshold==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDESPCompThreshold==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDESPCompThreshold==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LDESPDetectRoad                               /* 145 */
		#if SEC_CL_LDESPDetectRoad==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDESPDetectRoad==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDESPDetectRoad==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDESPDetectRoad==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
		
	#elif idx_FILE==idx_CL_LDESPDetectVehicleStatus                               /* 146 */
		#if SEC_CL_LDESPDetectVehicleStatus==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDESPDetectVehicleStatus==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDESPDetectVehicleStatus==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDESPDetectVehicleStatus==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
		
	#elif idx_FILE==idx_CL_LDESPDetectWheelState                               /* 147 */
		#if SEC_CL_LDESPDetectWheelState==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDESPDetectWheelState==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDESPDetectWheelState==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDESPDetectWheelState==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
		
	#elif idx_FILE==idx_CL_LDESPEstBeta                               /* 148 */
		#if SEC_CL_LDESPEstBeta==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDESPEstBeta==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDESPEstBeta==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDESPEstBeta==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		

	#elif idx_FILE==idx_CL_LDESSCallEmergencyBrkDet                               /* 149 */
		#if SEC_CL_LDESSCallEmergencyBrkDet ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDESSCallEmergencyBrkDet ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDESSCallEmergencyBrkDet ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDESSCallEmergencyBrkDet ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		

	#elif idx_FILE==idx_CL_LDFBCCallDetection                               /* 150 */
		#if SEC_CL_LDFBCCallDetection==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDFBCCallDetection ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDFBCCallDetection ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDFBCCallDetection ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LDPBAEstPressure                               /* 151 */
		#if SEC_CL_LDPBAEstPressure==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDPBAEstPressure ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDPBAEstPressure ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDPBAEstPressure ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LDROPCallDetection                               /* 152 */
		#if SEC_CL_LDROPCallDetection==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDROPCallDetection ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDROPCallDetection ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDROPCallDetection ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LDRTACallDetection                               /* 153 */
		#if SEC_CL_LDRTACallDetection==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDRTACallDetection ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDRTACallDetection ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDRTACallDetection ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LDTCSCallDetection                               /* 154 */
		#if SEC_CL_LDTCSCallDetection==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDTCSCallDetection ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDTCSCallDetection ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDTCSCallDetection ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LDTSPCallDetection                               /* 155 */
		#if SEC_CL_LDTSPCallDetection==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDTSPCallDetection ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDTSPCallDetection ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDTSPCallDetection ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LOABSCallCoordinator                               /* 156 */
		#if SEC_CL_LOABSCallCoordinator==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LOABSCallCoordinator ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LOABSCallCoordinator ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LOABSCallCoordinator ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LOCallMain                               /* 157 */
		#if SEC_CL_LOCallMain==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LOCallMain ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LOCallMain ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LOCallMain ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LOESPCallCoordinator                               /* 158 */
		#if SEC_CL_LOESPCallCoordinator==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LOESPCallCoordinator ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LOESPCallCoordinator ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LOESPCallCoordinator ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LOTCSCallCoordinator                               /* 159 */
		#if SEC_CL_LOTCSCallCoordinator==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LOTCSCallCoordinator ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LOTCSCallCoordinator ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LOTCSCallCoordinator ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LSABSCallSensorSignal                               /* 160 */
		#if SEC_CL_LSABSCallSensorSignal==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LSABSCallSensorSignal ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LSABSCallSensorSignal ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LSABSCallSensorSignal ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		

	#elif idx_FILE==idx_CL_LSABSEstLongAccSensorOffsetMon                               /* 161 */
		#if SEC_CL_LSABSEstLongAccSensorOffsetMon==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LSABSEstLongAccSensorOffsetMon ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LSABSEstLongAccSensorOffsetMon ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LSABSEstLongAccSensorOffsetMon ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LSAFSCallSensorSignal                               /* 162 */
		#if SEC_CL_LSAFSCallSensorSignal==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LSAFSCallSensorSignal==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LSAFSCallSensorSignal==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LSAFSCallSensorSignal==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LSAVHCallLogData                               /* 163 */
		#if SEC_CL_LSAVHCallLogData==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LSAVHCallLogData==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LSAVHCallLogData==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LSAVHCallLogData==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LSCallLogData                               /* 164 */
		#if SEC_CL_LSCallLogData==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LSCallLogData==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LSCallLogData==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LSCallLogData==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LSCallLogData_orig                               /* 165 */
		#if SEC_CL_LSCallLogData_orig==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LSCallLogData_orig==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LSCallLogData_orig==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LSCallLogData_orig==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LSCallMain                               /* 166 */
		#if SEC_CL_LSCallMain==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LSCallMain==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LSCallMain==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LSCallMain==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LSCDCCallSensorSignal                               /* 167 */
		#if SEC_CL_LSCDCCallSensorSignal==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LSCDCCallSensorSignal==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LSCDCCallSensorSignal==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LSCDCCallSensorSignal==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LSENGCallSensorSignal                               /* 168 */
		#if SEC_CL_LSENGCallSensorSignal==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LSENGCallSensorSignal==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LSENGCallSensorSignal==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LSENGCallSensorSignal==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LSESPCallBetaLogData                               /* 169 */
		#if SEC_CL_LSESPCallBetaLogData==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LSESPCallBetaLogData==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LSESPCallBetaLogData==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LSESPCallBetaLogData==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LSESPCallSensorSignal                               /* 170 */
		#if SEC_CL_LSESPCallSensorSignal==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LSESPCallSensorSignal==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LSESPCallSensorSignal==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LSESPCallSensorSignal==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LSESPCalSensorOffset                               /* 171 */
		#if SEC_CL_LSESPCalSensorOffset==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LSESPCalSensorOffset==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LSESPCalSensorOffset==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LSESPCalSensorOffset==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LSESPCalSlipRatio                               /* 172 */
		#if SEC_CL_LSESPCalSlipRatio==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LSESPCalSlipRatio==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LSESPCalSlipRatio==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LSESPCalSlipRatio==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LSESPCheckEspError                               /* 173 */
		#if SEC_CL_LSESPCheckEspError==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LSESPCheckEspError==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LSESPCheckEspError==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LSESPCheckEspError==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LSESPFilterEspSensor                               /* 174 */
		#if SEC_CL_LSESPFilterEspSensor==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LSESPFilterEspSensor==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LSESPFilterEspSensor==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LSESPFilterEspSensor==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LSFBCCallLogData                               /* 175 */
		#if SEC_CL_LSFBCCallLogData==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LSFBCCallLogData==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LSFBCCallLogData==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LSFBCCallLogData==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LSFBCCallSensorSignal                               /* 176 */
		#if SEC_CL_LSFBCCallSensorSignal==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LSFBCCallSensorSignal==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LSFBCCallSensorSignal==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LSFBCCallSensorSignal==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LSHDCCallLogData                               /* 177 */
		#if SEC_CL_LSHDCCallLogData==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LSHDCCallLogData==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LSHDCCallLogData==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LSHDCCallLogData==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LSMSCCallLogData                               /* 178 */
		#if SEC_CL_LSMSCCallLogData==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LSMSCCallLogData==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LSMSCCallLogData==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LSMSCCallLogData==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LSPBACallSensorSignal                               /* 179 */
		#if SEC_CL_LSPBACallSensorSignal==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LSPBACallSensorSignal==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LSPBACallSensorSignal==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LSPBACallSensorSignal==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_CL_LSTCSCallSensorSignal                               /* 180 */
		#if SEC_CL_LSTCSCallSensorSignal==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LSTCSCallSensorSignal==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LSTCSCallSensorSignal==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LSTCSCallSensorSignal==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	#elif idx_FILE==idx_CL_Ventil                               /* 182 */
		#if SEC_CL_Ventil==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_Ventil==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_Ventil==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_Ventil==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_Brake_Main                               /* 183 */
		#if SEC_Fw_Brake_Main==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_Brake_Main==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_Brake_Main==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_Brake_Main==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_main                               /* 184 */
		#if SEC_Fw_main==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_main==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_main==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_main==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_WChckRsrc                               /* 185 */
		#if SEC_Fw_WChckRsrc==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_WChckRsrc==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_WChckRsrc==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_WChckRsrc==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_WContlADC                               /* 186 */
		#if SEC_Fw_WContlADC==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_WContlADC==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_WContlADC==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_WContlADC==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_WContlEEP                               /* 187 */
		#if SEC_Fw_WContlEEP==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_WContlEEP==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_WContlEEP==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_WContlEEP==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_WContlLamp                               /* 188 */
		#if SEC_Fw_WContlLamp==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_WContlLamp==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_WContlLamp==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_WContlLamp==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_WContlLogger                               /* 189 */
		#if SEC_Fw_WContlLogger==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_WContlLogger==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_WContlLogger==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_WContlLogger==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_WContlSnr                               /* 190 */
		#if SEC_Fw_WContlSnr==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_WContlSnr==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_WContlSnr==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_WContlSnr==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_WContlSubFunc                               /* 191 */
		#if SEC_Fw_WContlSubFunc==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_WContlSubFunc==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_WContlSubFunc==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_WContlSubFunc==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_WContlSwtch                               /* 192 */
		#if SEC_Fw_WContlSwtch==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_WContlSwtch==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_WContlSwtch==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_WContlSwtch==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_WDecSenSig                               /* 193 */
		#if SEC_Fw_WDecSenSig==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_WDecSenSig==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_WDecSenSig==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_WDecSenSig==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_WDrv_Motor                               /* 194 */
		#if SEC_Fw_WDrv_Motor==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_WDrv_Motor==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_WDrv_Motor==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_WDrv_Motor==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_WDrv_Valve                               /* 195 */
		#if SEC_Fw_WDrv_Valve==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_WDrv_Valve==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_WDrv_Valve==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_WDrv_Valve==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_WInterfaceHallSensor                               /* 196 */
		#if SEC_Fw_WInterfaceHallSensor==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_WInterfaceHallSensor==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_WInterfaceHallSensor==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_WInterfaceHallSensor==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_WItf_PwrSgnl                               /* 197 */
		#if SEC_Fw_WItf_PwrSgnl==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_WItf_PwrSgnl==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_WItf_PwrSgnl==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_WItf_PwrSgnl==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_WDrv_ADC                               /* 198 */
		#if SEC_Fw_WDrv_ADC==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_WDrv_ADC==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_WDrv_ADC==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_WDrv_ADC==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_WDrv_AluCheck                               /* 199 */
		#if SEC_Fw_WDrv_AluCheck==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_WDrv_AluCheck==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_WDrv_AluCheck==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_WDrv_AluCheck==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_WDrv_CAN                               /* 200 */
		#if SEC_Fw_WDrv_CAN==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_WDrv_CAN==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_WDrv_CAN==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_WDrv_CAN==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_WDrv_CommAsic                               /* 201 */
		#if SEC_Fw_WDrv_CommAsic==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_WDrv_CommAsic==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_WDrv_CommAsic==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_WDrv_CommAsic==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_WDrv_EEP                               /* 202 */
		#if SEC_Fw_WDrv_EEP==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_WDrv_EEP==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_WDrv_EEP==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_WDrv_EEP==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_WDrv_EscAsic                               /* 203 */
		#if SEC_Fw_WDrv_EscAsic==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_WDrv_EscAsic==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_WDrv_EscAsic==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_WDrv_EscAsic==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_WDrv_ETIMER                               /* 204 */
		#if SEC_Fw_WDrv_ETIMER==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_WDrv_ETIMER==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_WDrv_ETIMER==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_WDrv_ETIMER==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_WDrv_PIT                               /* 205 */
		#if SEC_Fw_WDrv_PIT==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_WDrv_PIT==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_WDrv_PIT==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_WDrv_PIT==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		

	#elif idx_FILE==idx_Fw_WDrv_PWM                               /* 206 */
		#if SEC_Fw_WDrv_PWM==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_WDrv_PWM==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_WDrv_PWM==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_WDrv_PWM==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		

	#elif idx_FILE==idx_Fw_WDrv_REGISTER                               /* 207 */
		#if SEC_Fw_WDrv_REGISTER==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_WDrv_REGISTER==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_WDrv_REGISTER==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_WDrv_REGISTER==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_WDrv_SPI                               /* 208 */
		#if SEC_Fw_WDrv_SPI==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_WDrv_SPI==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_WDrv_SPI==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_WDrv_SPI==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_MPC55xx_init                               /* 209 */
		#if SEC_Fw_MPC55xx_init==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_MPC55xx_init==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_MPC55xx_init==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_MPC55xx_init==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_MPC55xx_init_debug                               /* 210 */
		#if SEC_Fw_MPC55xx_init_debug==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_MPC55xx_init_debug==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_MPC55xx_init_debug==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_MPC55xx_init_debug==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_MPC5643L_HWInit                               /* 211 */
		#if SEC_Fw_MPC5643L_HWInit==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_MPC5643L_HWInit==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_MPC5643L_HWInit==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_MPC5643L_HWInit==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_adc_init                               /* 212 */
		#if SEC_Fw_adc_init==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_adc_init==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_adc_init==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_adc_init==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_dspi_init                               /* 213 */
		#if SEC_Fw_dspi_init==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_dspi_init==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_dspi_init==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_dspi_init==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_edma_init                               /* 214 */
		#if SEC_Fw_edma_init==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_edma_init==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_edma_init==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_edma_init==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_etimer_init                               /* 215 */
		#if SEC_Fw_etimer_init==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_etimer_init==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_etimer_init==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_etimer_init==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_flash_init                               /* 216 */
		#if SEC_Fw_flash_init==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_flash_init==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_flash_init==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_flash_init==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_flexcan_init                               /* 217 */
		#if SEC_Fw_flexcan_init==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_flexcan_init==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_flexcan_init==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_flexcan_init==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_flexpwm_init                               /* 218 */
		#if SEC_Fw_flexpwm_init==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_flexpwm_init==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_flexpwm_init==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_flexpwm_init==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_intc_init                               /* 219 */
		#if SEC_Fw_intc_init==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_intc_init==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_intc_init==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_intc_init==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_intc_sw_vecttable                               /* 220 */
		#if SEC_Fw_intc_sw_vecttable==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_intc_sw_vecttable==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_intc_sw_vecttable==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_intc_sw_vecttable==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_linflex_init                               /* 221 */
		#if SEC_Fw_linflex_init==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_linflex_init==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_linflex_init==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_linflex_init==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_msr_init                               /* 222 */
		#if SEC_Fw_msr_init==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_msr_init==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_msr_init==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_msr_init==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		

	#elif idx_FILE==idx_Fw_pit_init                               /* 223 */
		#if SEC_Fw_pit_init==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_pit_init==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_pit_init==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_pit_init==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_rappid_utils                               /* 224 */
		#if SEC_Fw_rappid_utils==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_rappid_utils==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_rappid_utils==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_rappid_utils==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_romcopy                               /* 225 */
		#if SEC_Fw_romcopy==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_romcopy==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_romcopy==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_romcopy==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_siu_init                               /* 226 */
		#if SEC_Fw_siu_init==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_siu_init==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_siu_init==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_siu_init==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_stm_init                               /* 227 */
		#if SEC_Fw_stm_init==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_stm_init==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_stm_init==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_stm_init==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_swt_init                               /* 228 */
		#if SEC_Fw_swt_init==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_swt_init==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_swt_init==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_swt_init==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		

	#elif idx_FILE==idx_Fw_sysclk_init                               /* 229 */
		#if SEC_Fw_sysclk_init==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_sysclk_init==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_sysclk_init==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_sysclk_init==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Fw_sys_init                               /* 230 */
		#if SEC_Fw_sys_init==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_sys_init==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_sys_init==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_sys_init==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Can_CCChkCan                               /* 231 */
		#if SEC_Can_CCChkCan==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Can_CCChkCan==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Can_CCChkCan==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Can_CCChkCan==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Can_CCDrvCan                               /* 232 */
		#if SEC_Can_CCDrvCan==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Can_CCDrvCan==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Can_CCDrvCan==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Can_CCDrvCan==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		

	#elif idx_FILE==idx_Can_CCDrvCanDiag                               /* 233 */
		#if SEC_Can_CCDrvCanDiag==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Can_CCDrvCanDiag==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Can_CCDrvCanDiag==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Can_CCDrvCanDiag==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Can_CCDrvLogger                               /* 234 */
		#if SEC_Can_CCDrvLogger==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Can_CCDrvLogger==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Can_CCDrvLogger==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Can_CCDrvLogger==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Can_CCDrvSubCan                               /* 235 */
		#if SEC_Can_CCDrvSubCan==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Can_CCDrvSubCan==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Can_CCDrvSubCan==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Can_CCDrvSubCan==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Can_CCTestFunc                               /* 236 */
		#if SEC_Can_CCTestFunc==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Can_CCTestFunc==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Can_CCTestFunc==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Can_CCTestFunc==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Can_CCDrvA13Can                               /* 237 */
		#if SEC_Can_CCDrvA13Can==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Can_CCDrvA13Can==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Can_CCDrvA13Can==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Can_CCDrvA13Can==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Can_CCDrvA21Can                               /* 238 */
		#if SEC_Can_CCDrvA21Can==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Can_CCDrvA21Can==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Can_CCDrvA21Can==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Can_CCDrvA21Can==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Can_CCDrvB12Can                               /* 239 */
		#if SEC_Can_CCDrvB12Can==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Can_CCDrvB12Can==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Can_CCDrvB12Can==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Can_CCDrvB12Can==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Can_CCDrvE101Can                               /* 240 */
		#if SEC_Can_CCDrvE101Can==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Can_CCDrvE101Can==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Can_CCDrvE101Can==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Can_CCDrvE101Can==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Can_CCDrvHMCCan                               /* 241 */
		#if SEC_Can_CCDrvHMCCan==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Can_CCDrvHMCCan==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Can_CCDrvHMCCan==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Can_CCDrvHMCCan==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Can_CCDrvHMC_HDCan                               /* 242 */
		#if SEC_Can_CCDrvHMC_HDCan==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Can_CCDrvHMC_HDCan==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Can_CCDrvHMC_HDCan==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Can_CCDrvHMC_HDCan==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Can_CCDrvSYCCan                               /* 243 */
		#if SEC_Can_CCDrvSYCCan==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Can_CCDrvSYCCan==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Can_CCDrvSYCCan==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Can_CCDrvSYCCan==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Can_CCDrvTestCarCan                               /* 244 */
		#if SEC_Can_CCDrvTestCarCan==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Can_CCDrvTestCarCan==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Can_CCDrvTestCarCan==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Can_CCDrvTestCarCan==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Can_CCHmcChkCan                               /* 245 */
		#if SEC_Can_CCHmcChkCan==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Can_CCHmcChkCan==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Can_CCHmcChkCan==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Can_CCHmcChkCan==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Can_CCDrvASTRACan                               /* 246 */
		#if SEC_Can_CCDrvASTRACan==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Can_CCDrvASTRACan==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Can_CCDrvASTRACan==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Can_CCDrvASTRACan==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Can_CCDrvC100Can                               /* 247 */
		#if SEC_Can_CCDrvC100Can==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Can_CCDrvC100Can==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Can_CCDrvC100Can==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Can_CCDrvC100Can==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Can_CCDrvFORDCan                               /* 248 */
		#if SEC_Can_CCDrvFORDCan==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Can_CCDrvFORDCan==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Can_CCDrvFORDCan==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Can_CCDrvFORDCan==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Can_CCDrvGMDWCan                               /* 249 */
		#if SEC_Can_CCDrvGMDWCan==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Can_CCDrvGMDWCan==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Can_CCDrvGMDWCan==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Can_CCDrvGMDWCan==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Can_CCDrvJ300Can                               /* 250 */
		#if SEC_Can_CCDrvJ300Can==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Can_CCDrvJ300Can==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Can_CCDrvJ300Can==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Can_CCDrvJ300Can==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Can_CCDrvKYRONCan                               /* 251 */
		#if SEC_Can_CCDrvKYRONCan==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Can_CCDrvKYRONCan==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Can_CCDrvKYRONCan==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Can_CCDrvKYRONCan==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		

	#elif idx_FILE==idx_Can_CCDrvRAXTONCan                               /* 252 */
		#if SEC_Can_CCDrvRAXTONCan==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Can_CCDrvRAXTONCan==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Can_CCDrvRAXTONCan==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Can_CCDrvRAXTONCan==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_Can_CCDrvTAHOECan                               /* 253 */
		#if SEC_Can_CCDrvTAHOECan==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Can_CCDrvTAHOECan==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Can_CCDrvTAHOECan==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Can_CCDrvTAHOECan==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_App_ETunePar                               /* 254 */
		#if SEC_App_ETunePar==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_App_ETunePar==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_App_ETunePar==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_App_ETunePar==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	
	#elif idx_FILE==idx_App_LTunePar                               /* 255 */
		#if SEC_App_LTunePar==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_App_LTunePar==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_App_LTunePar==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_App_LTunePar==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
		
	#elif idx_FILE==idx_Fs_F_SensorSignalModel                             
		#if SEC_Fs_F_SensorSignalModel==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_F_SensorSignalModel==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_F_SensorSignalModel==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_F_SensorSignalModel==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Cal_BMW740i_TUNapCalAbs2WD                             
		#if SEC_Cal_BMW740i_TUNapCalAbs2WD==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Cal_BMW740i_TUNapCalAbs2WD==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalAbs2WD==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalAbs2WD==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Cal_BMW740i_TUNapCalAbsVafs2WD                             
		#if SEC_Cal_BMW740i_TUNapCalAbsVafs2WD==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Cal_BMW740i_TUNapCalAbsVafs2WD==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalAbsVafs2WD==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalAbsVafs2WD==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Cal_BMW740i_TUNapCalEsp2WD                             
		#if SEC_Cal_BMW740i_TUNapCalEsp2WD==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Cal_BMW740i_TUNapCalEsp2WD==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEsp2WD==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEsp2WD==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Cal_BMW740i_TUNapCalEspEng01                             
		#if SEC_Cal_BMW740i_TUNapCalEspEng01==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Cal_BMW740i_TUNapCalEspEng01==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEspEng01==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEspEng01==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
/*	#elif idx_FILE==idx_Cal_BMW740i_TUNapCalEspEng02                             
		#if SEC_Cal_BMW740i_TUNapCalEspEng02==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Cal_BMW740i_TUNapCalEspEng02==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEspEng02==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEspEng02==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Cal_BMW740i_TUNapCalEspEng03                             
		#if SEC_Cal_BMW740i_TUNapCalEspEng03==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Cal_BMW740i_TUNapCalEspEng03==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEspEng03==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEspEng03==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Cal_BMW740i_TUNapCalEspEng04                             
		#if SEC_Cal_BMW740i_TUNapCalEspEng03==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Cal_BMW740i_TUNapCalEspEng04==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEspEng04==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEspEng04==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Cal_BMW740i_TUNapCalEspEng05                             
		#if SEC_Cal_BMW740i_TUNapCalEspEng03==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Cal_BMW740i_TUNapCalEspEng05==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEspEng05==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEspEng05==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Cal_BMW740i_TUNapCalEspEng06                             
		#if SEC_Cal_BMW740i_TUNapCalEspEng06==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Cal_BMW740i_TUNapCalEspEng06==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEspEng06==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEspEng06==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Cal_BMW740i_TUNapCalEspEng07                             
		#if SEC_Cal_BMW740i_TUNapCalEspEng07==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Cal_BMW740i_TUNapCalEspEng07==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEspEng07==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEspEng07==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Cal_BMW740i_TUNapCalEspEng08                             
		#if SEC_Cal_BMW740i_TUNapCalEspEng08==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Cal_BMW740i_TUNapCalEspEng08==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEspEng08==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEspEng08==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Cal_BMW740i_TUNapCalEspEng09                             
		#if SEC_Cal_BMW740i_TUNapCalEspEng09==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Cal_BMW740i_TUNapCalEspEng09==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEspEng09==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEspEng09==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Cal_BMW740i_TUNapCalEspEng10                             
		#if SEC_Cal_BMW740i_TUNapCalEspEng10==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Cal_BMW740i_TUNapCalEspEng10==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEspEng10==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEspEng10==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Cal_BMW740i_TUNapCalEspEng11                             
		#if SEC_Cal_BMW740i_TUNapCalEspEng11==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Cal_BMW740i_TUNapCalEspEng11==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEspEng11==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEspEng11==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Cal_BMW740i_TUNapCalEspEng12                             
		#if SEC_Cal_BMW740i_TUNapCalEspEng12==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Cal_BMW740i_TUNapCalEspEng12==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEspEng12==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEspEng12==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Cal_BMW740i_TUNapCalEspEng13                             
		#if SEC_Cal_BMW740i_TUNapCalEspEng13==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Cal_BMW740i_TUNapCalEspEng13==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEspEng13==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEspEng13==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Cal_BMW740i_TUNapCalEspEng14                             
		#if SEC_Cal_BMW740i_TUNapCalEspEng14==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Cal_BMW740i_TUNapCalEspEng14==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEspEng14==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEspEng14==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Cal_BMW740i_TUNapCalEspEng15                             
		#if SEC_Cal_BMW740i_TUNapCalEspEng15==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Cal_BMW740i_TUNapCalEspEng15==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEspEng15==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEspEng15==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Cal_BMW740i_TUNapCalEspEng16                             
		#if SEC_Cal_BMW740i_TUNapCalEspEng16==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Cal_BMW740i_TUNapCalEspEng16==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEspEng16==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEspEng16==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif*/
	#elif idx_FILE==idx_Cal_BMW740i_TUNapCalEspVafs2WD                             
		#if SEC_Cal_BMW740i_TUNapCalEspVafs2WD==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Cal_BMW740i_TUNapCalEspVafs2WD==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEspVafs2WD==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalEspVafs2WD==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Cal_BMW740i_TUNapCalRbcOem2WD                             
		#if SEC_Cal_BMW740i_TUNapCalRbcOem2WD==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Cal_BMW740i_TUNapCalRbcOem2WD==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalRbcOem2WD==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Cal_BMW740i_TUNapCalRbcOem2WD==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_CL_LDABSCallSideVref                             
		#if SEC_CL_LDABSCallSideVref==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDABSCallSideVref==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDABSCallSideVref==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDABSCallSideVref==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_FlexRay                             
		#if SEC_FlexRay==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_FlexRay==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_FlexRay==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_FlexRay==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		
	#elif idx_FILE==idx_Fs_F_MOC_InterfaceLinkData                             
		#if SEC_Fs_F_MOC_InterfaceLinkData==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_F_MOC_InterfaceLinkData==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_F_MOC_InterfaceLinkData==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_F_MOC_InterfaceLinkData==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Fs_FA_MOC_ActuatorCheck                             ////////////////////////////
		#if SEC_Fs_FA_MOC_ActuatorCheck==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_FA_MOC_ActuatorCheck==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_FA_MOC_ActuatorCheck==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_FA_MOC_ActuatorCheck==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif

	#elif idx_FILE==idx_Fs_FCPedalSenCheck                              
		#if SEC_Fs_FCPedalSenCheck==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_FCPedalSenCheck==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_FCPedalSenCheck==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_FCPedalSenCheck==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Fs_FI_MOC_HallCountSensorCheck                             
		#if SEC_Fs_FI_MOC_HallCountSensorCheck==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_FI_MOC_HallCountSensorCheck==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_FI_MOC_HallCountSensorCheck==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_FI_MOC_HallCountSensorCheck==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Fs_FI_MOC_SwitchCheck                             
		#if SEC_Fs_FI_MOC_SwitchCheck==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_FI_MOC_SwitchCheck==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_FI_MOC_SwitchCheck==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_FI_MOC_SwitchCheck==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Fs_FS_MOC_CurrentSenCheck                             
		#if SEC_Fs_FS_MOC_CurrentSenCheck==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_FS_MOC_CurrentSenCheck==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_FS_MOC_CurrentSenCheck==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_FS_MOC_CurrentSenCheck==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Fs_FS_MOC_ForceSensorCheck                             
		#if SEC_Fs_FS_MOC_ForceSensorCheck==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_FS_MOC_ForceSensorCheck==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_FS_MOC_ForceSensorCheck==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_FS_MOC_ForceSensorCheck==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif
	#elif idx_FILE==idx_Fs_8B20_FS_HW_VIEW_CHK                             
		#if SEC_Fs_8B20_FS_HW_VIEW_CHK==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fs_8B20_FS_HW_VIEW_CHK==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fs_8B20_FS_HW_VIEW_CHK==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fs_8B20_FS_HW_VIEW_CHK==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif	
	#elif idx_FILE==idx_CL_LATVBBCallActHW                                   
		#if SEC_CL_LATVBBCallActHW ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LATVBBCallActHW ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LATVBBCallActHW ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LATVBBCallActHW ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		                                     //LATVBBCallActHW
	#elif idx_FILE==idx_CL_LCEVPCallControl                                   
		#if SEC_CL_LCEVPCallControl ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCEVPCallControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCEVPCallControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCEVPCallControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		                                     //LCEVPCallControl
	#elif idx_FILE==idx_CL_LCRDCMCallControl                                   
		#if SEC_CL_LCRDCMCallControl ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCRDCMCallControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCRDCMCallControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCRDCMCallControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		                                     //LCRDCMCallControl
	#elif idx_FILE==idx_CL_LCTVBBCallControl                                   
		#if SEC_CL_LCTVBBCallControl ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LCTVBBCallControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LCTVBBCallControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LCTVBBCallControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		                                     //LCTVBBCallControl
	#elif idx_FILE==idx_CL_LDTVBBCallDetection                                   
		#if SEC_CL_LDTVBBCallDetection ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_CL_LDTVBBCallDetection ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_CL_LDTVBBCallDetection ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_CL_LDTVBBCallDetection ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		                                     //LDTVBBCallDetection		
	#elif idx_FILE==idx_Fw_WDrv_ADXR800                                   
		#if SEC_Fw_WDrv_ADXR800 ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_WDrv_ADXR800 ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_WDrv_ADXR800 ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_WDrv_ADXR800 ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		                                     //LDTVBBCallDetection		
	
	#elif idx_FILE==idx_Fw_WDrv_SCA21XX                                   
		#if SEC_Fw_WDrv_SCA21X ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_Fw_WDrv_SCA21X ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_Fw_WDrv_SCA21X ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_Fw_WDrv_SCA21X ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		                                     //LDTVBBCallDetection			
	
	#elif idx_FILE==idx_vx1000                                   
		#if SEC_vx1000 ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_vx1000 ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_vx1000 ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_vx1000 ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		                                  
/* New Logic Files are Added : 2012-09-10  */
	#elif idx_FILE==idx_CL_LAPACCallActHW                                   
		#if SEC_LAPACCallActHW ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_LAPACCallActHW ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_LAPACCallActHW ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_LAPACCallActHW ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		                                  
	#elif idx_FILE==idx_CL_LASPASCallActHW                                   
		#if SEC_LASPASCallActHW ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_LASPASCallActHW ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_LASPASCallActHW ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_LASPASCallActHW ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		                                  
	#elif idx_FILE==idx_CL_LCETSCCallControl                                   
		#if SEC_LCETSCCallControl ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_LCETSCCallControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_LCETSCCallControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_LCETSCCallControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		                                  
	#elif idx_FILE==idx_CL_LCHRCCallControl                                   
		#if SEC_LCHRCCallControl ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_LCHRCCallControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_LCHRCCallControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_LCHRCCallControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		                                  
	#elif idx_FILE==idx_CL_LCPACCallControl                                   
		#if SEC_LCPACCallControl ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_LCPACCallControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_LCPACCallControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_LCPACCallControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		                                  
	#elif idx_FILE==idx_CL_LCSPASCallControl                                   
		#if SEC_LCSPASCallControl ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_LCSPASCallControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_LCSPASCallControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_LCSPASCallControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		                                  
	#elif idx_FILE==idx_CL_LCTODCallControl                                   
		#if SEC_LCTODCallControl ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_LCTODCallControl ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_LCTODCallControl ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_LCTODCallControl ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		                                  
	#elif idx_FILE==idx_CL_LDESPEstDiscTemp                                   
		#if SEC_LDESPEstDiscTemp ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_LDESPEstDiscTemp ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_LDESPEstDiscTemp ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_LDESPEstDiscTemp ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		                                  
	#elif idx_FILE==idx_CL_LSESPInterfaceIVSS                                   
		#if SEC_LSESPInterfaceIVSS ==SECTION_I
			#if defined(idx_START)
				#define MANDO_SECTION_I_START
			#else
				#define MANDO_SECTION_I_STOP
			#endif
		#elif SEC_LSESPInterfaceIVSS ==SECTION_II
			#if defined(idx_START)
				#define MANDO_SECTION_II_START
			#else
				#define MANDO_SECTION_II_STOP
			#endif				
		#elif SEC_LSESPInterfaceIVSS ==SECTION_III
			#if defined(idx_START)
				#define MANDO_SECTION_CALIBRATION_START
			#else
				#define MANDO_SECTION_CALIBRATION_STOP
			#endif				
		#elif SEC_LSESPInterfaceIVSS ==SECTION_IV
			// default area
		#else
			#define MANDO_SECTION_DEFAULT_AREA
		#endif		                                  
	#else
		#error "No Defined Pragma"
	#endif
	
	#undef SEC_Fs_ErrorHandling							
	#undef SEC_Fs_FAAStrHWCheck                         
	#undef SEC_Fs_FAAStrSensorCalc                      
	#undef SEC_Fs_FAAStrSignalCheck                     
	#undef SEC_Fs_FADStrHWCheck                         
	#undef SEC_Fs_FADStrSensorCalc                      
	#undef SEC_Fs_FADStrSignalCheck                     
	#undef SEC_Fs_FBIgnCheck                            
	#undef SEC_Fs_FCVacumnSenCheck                      
	#undef SEC_Fs_FEErrorProcessing                     
	#undef SEC_Fs_FGAXSensorCheck                       
	#undef SEC_Fs_FIAsicCheck                           
	#undef SEC_Fs_FLLampRequest                         
	#undef SEC_Fs_FOSenPowerCheck                       
	#undef SEC_Fs_FPBlsSignalCheck                      
	#undef SEC_Fs_FPMcpBlsHWCheck                       
	#undef SEC_Fs_FPMcpBlsSensorCalc                    
	#undef SEC_Fs_FPMcpBlsSignalCheck                   
	#undef SEC_Fs_FRValveRelayCheck                     
	#undef SEC_Fs_FSSwitchCheck                         
	#undef SEC_Fs_FTMotorCheck                          
	#undef SEC_Fs_FVSolAtvCheck                         
	#undef SEC_Fs_FVSolPsvCheck                         
	#undef SEC_Fs_FWSpeedCalc                           
	#undef SEC_Fs_FWSpeedCheck                          
	#undef SEC_Fs_FYLgYawHWCheck                        
	#undef SEC_Fs_FYLgYawSensorCalc                     
	#undef SEC_Fs_FYLgYawSignalCheck                    
	#undef SEC_Fs_F_ActuatorLinkSet                     
	#undef SEC_Fs_F_CommFunc                            
	#undef SEC_Fs_F_MainFailsafe                        
	#undef SEC_Fs_F_MECUFunction                        
	#undef SEC_Fs_F_SensorHwCheck                       
	#undef SEC_Fs_F_SensorSignalModel                   
	#undef SEC_Fs_F_SensorSwCheck                       
	#undef SEC_Fs_F_SignalTest                          
	#undef SEC_Fs_F_VariableLinkSet                     
	#undef SEC_Diag_CDCL_LinkData                       
	#undef SEC_Diag_CDCS_DiagService                    
	#undef SEC_Diag_CDK_DiagService                     
	#undef SEC_Diag_CDS_ReadRecordValue                 
	#undef SEC_CL_btcs_Neo                              
	#undef SEC_CL_Hardware_Control                      
	#undef SEC_CL_hm_logic_var                          
	#undef SEC_CL_LAABSCallActCan                       
	#undef SEC_CL_LAABSCallActHW                        
	#undef SEC_CL_LAABSCallHWTest                       
	#undef SEC_CL_LAABSCallHWTest_orig                  
	#undef SEC_CL_LAABSCallMotorSpeedControl            
	#undef SEC_CL_LAABSCallMotorSpeedControl_on_off     
	#undef SEC_CL_LAABSGenerateLFCDuty                  
	#undef SEC_CL_LAACCCallActCan                       
	#undef SEC_CL_LAACCCallActHW                        
	#undef SEC_CL_LAARBCallActHW                        
	#undef SEC_CL_LAAVHCallActHW                        
	#undef SEC_CL_LABACallActHW                         
	#undef SEC_CL_LABDWCallActHW                        
	#undef SEC_CL_LACallMain                            
	#undef SEC_CL_LACBCCallActHW                        
	#undef SEC_CL_LADECCallActCan                       
	#undef SEC_CL_LADECCallActHW                        
	#undef SEC_CL_LAEBPCallActHW                        
	#undef SEC_CL_LAEDCCallActCan                       
	#undef SEC_CL_LAEPBCallActHW                        
	#undef SEC_CL_LAEPCCallActHW                        
	#undef SEC_CL_LAESPActuateNcNovalveF                
	#undef SEC_CL_LAESPCallActCan                       
	#undef SEC_CL_LAESPCallActHW                        
	#undef SEC_CL_LAFBCCallActHW                        
	#undef SEC_CL_LAHBBCallActHW                        
	#undef SEC_CL_LAHDCCallActHW                        
	#undef SEC_CL_LAHRBCallActHW                        
	#undef SEC_CL_LAHSACallActHW                        
	#undef SEC_CL_LAPBACallActHW                        
	#undef SEC_CL_LASLSCallActHW                        
	#undef SEC_CL_LATCSCallActCan                       
	#undef SEC_CL_LATCSCallActHW                        
	#undef SEC_CL_LATSPCallActHW                        
	#undef SEC_CL_Lbtcs_Neo                             
	#undef SEC_CL_LCABSCallControl                      
	#undef SEC_CL_LCABSCallESPCombination               
	#undef SEC_CL_LCABSDecideCtrlState                  
	#undef SEC_CL_LCABSDecideCtrlVariables              
	#undef SEC_CL_LCACCCallControl                      
	#undef SEC_CL_LCAFSCallControl                      
	#undef SEC_CL_LCallMain                             
	#undef SEC_CL_LCARBCallControl                      
	#undef SEC_CL_LCAVHCallControl                      
	#undef SEC_CL_LCBDWCallControl                      
	#undef SEC_CL_LCCallMain                            
	#undef SEC_CL_LCCBCCallControl                      
	#undef SEC_CL_LCCDCCallControl                      
	#undef SEC_CL_LCDECCallControl                      
	#undef SEC_CL_LCEBDCallControl                      
	#undef SEC_CL_LCEBPCallControl                      
	#undef SEC_CL_LCEDCCallControl                      
	#undef SEC_CL_LCEPBCallControl                      
	#undef SEC_CL_LCEPCCallControl                      
	#undef SEC_CL_LCESPCalInterpolation                 
	#undef SEC_CL_LCESPCallControl                      
	#undef SEC_CL_LCESPCallEngineControl                
	#undef SEC_CL_LCESPCallPartialControl               
	#undef SEC_CL_LCESPCalLpf                           
	#undef SEC_CL_LCESPCallPressureControl              
	#undef SEC_CL_LCESPInterfaceSlipController          
	#undef SEC_CL_LCETCCallControl                      
	#undef SEC_CL_LCFBCCallControl                      
	#undef SEC_CL_LCHBBCallControl                      
	#undef SEC_CL_LCHDCCallControl                      
	#undef SEC_CL_LCHRBCallControl                      
	#undef SEC_CL_LCHSACallControl                      
	#undef SEC_CL_LCPBACallControl                      
	#undef SEC_CL_LCROPCallControl                      
	#undef SEC_CL_LCSLSCallControl                      
	#undef SEC_CL_LCTCSCallControl                      
	#undef SEC_CL_LCTSPCallControl                      
	#undef SEC_CL_LDABSCallDctForVref                   
	#undef SEC_CL_LDABSCallDetection                    
	#undef SEC_CL_LDABSCallEstVehDecel                  
	#undef SEC_CL_LDABSCallSideVref                     
	#undef SEC_CL_LDABSCallVrefCompForESP               
	#undef SEC_CL_LDABSCallVrefEst                      
	#undef SEC_CL_LDABSCallVrefMainFilterProcess        
	#undef SEC_CL_LDABSDctRoadGradient                  
	#undef SEC_CL_LDABSDctRoadSurface                   
	#undef SEC_CL_LDABSDctVehicleStatus                 
	#undef SEC_CL_LDABSDctWheelStatus                   
	#undef SEC_CL_LDAFSCallDetection                    
	#undef SEC_CL_LDBDWCallDetection                    
	#undef SEC_CL_LDCallMain                            
	#undef SEC_CL_LDCDCCallDetection                    
	#undef SEC_CL_LDEBPCallDetection                    
	#undef SEC_CL_LDEDCCallDetection                    
	#undef SEC_CL_LDENGCallDetection                    
	#undef SEC_CL_LDENGEstPowertrain                    
	#undef SEC_CL_LDEPCCallDetection                    
	#undef SEC_CL_LDESPCallDetection                    
	#undef SEC_CL_LDESPCallPartialDetection             
	#undef SEC_CL_LDESPCalVehicleModel                  
	#undef SEC_CL_LDESPCompDeltaYaw                     
	#undef SEC_CL_LDESPCompDeltaYawDot                  
	#undef SEC_CL_LDESPCompDetDeltaYaw                  
	#undef SEC_CL_LDESPCompThreshold                    
	#undef SEC_CL_LDESPDetectRoad                       
	#undef SEC_CL_LDESPDetectVehicleStatus              
	#undef SEC_CL_LDESPDetectWheelState                 
	#undef SEC_CL_LDESPEstBeta                          
	#undef SEC_CL_LDESSCallEmergencyBrkDet              
	#undef SEC_CL_LDFBCCallDetection                    
	#undef SEC_CL_LDPBAEstPressure                      
	#undef SEC_CL_LDROPCallDetection                    
	#undef SEC_CL_LDRTACallDetection                    
	#undef SEC_CL_LDTCSCallDetection                    
	#undef SEC_CL_LDTSPCallDetection                    
	#undef SEC_CL_LOABSCallCoordinator                  
	#undef SEC_CL_LOCallMain                            
	#undef SEC_CL_LOESPCallCoordinator                  
	#undef SEC_CL_LOTCSCallCoordinator                  
	#undef SEC_CL_LSABSCallSensorSignal                 
	#undef SEC_CL_LSABSEstLongAccSensorOffsetMon        
	#undef SEC_CL_LSAFSCallSensorSignal                 
	#undef SEC_CL_LSAVHCallLogData                      
	#undef SEC_CL_LSCallLogData                         
	#undef SEC_CL_LSCallLogData_orig                    
	#undef SEC_CL_LSCallMain                            
	#undef SEC_CL_LSCDCCallSensorSignal                 
	#undef SEC_CL_LSENGCallSensorSignal                 
	#undef SEC_CL_LSESPCallBetaLogData                  
	#undef SEC_CL_LSESPCallSensorSignal                 
	#undef SEC_CL_LSESPCalSensorOffset                  
	#undef SEC_CL_LSESPCalSlipRatio                     
	#undef SEC_CL_LSESPCheckEspError                    
	#undef SEC_CL_LSESPFilterEspSensor                  
	#undef SEC_CL_LSFBCCallLogData                      
	#undef SEC_CL_LSFBCCallSensorSignal                 
	#undef SEC_CL_LSHDCCallLogData                      
	#undef SEC_CL_LSMSCCallLogData                      
	#undef SEC_CL_LSPBACallSensorSignal                 
	#undef SEC_CL_LSTCSCallSensorSignal                 
	#undef SEC_CL_Ventil                                
	#undef SEC_Fw_Brake_Main                            
	#undef SEC_Fw_main                                  
	#undef SEC_Fw_WChckRsrc                             
	#undef SEC_Fw_WContlADC                             
	#undef SEC_Fw_WContlEEP                             
	#undef SEC_Fw_WContlLamp                            
	#undef SEC_Fw_WContlLogger                          
	#undef SEC_Fw_WContlSnr                             
	#undef SEC_Fw_WContlSubFunc                         
	#undef SEC_Fw_WContlSwtch                           
	#undef SEC_Fw_WDecSenSig                            
	#undef SEC_Fw_WDrv_Motor                            
	#undef SEC_Fw_WDrv_Valve                            
	#undef SEC_Fw_WInterfaceHallSensor                  
	#undef SEC_Fw_WItf_PwrSgnl                          
	#undef SEC_Fw_WDrv_ADC                              
	#undef SEC_Fw_WDrv_AluCheck                         
	#undef SEC_Fw_WDrv_CAN                              
	#undef SEC_Fw_WDrv_CommAsic                         
	#undef SEC_Fw_WDrv_EEP                              
	#undef SEC_Fw_WDrv_EscAsic                          
	#undef SEC_Fw_WDrv_ETIMER                           
	#undef SEC_Fw_WDrv_PIT                              
	#undef SEC_Fw_WDrv_PWM                              
	#undef SEC_Fw_WDrv_REGISTER                         
	#undef SEC_Fw_WDrv_SPI                              
	#undef SEC_Fw_MPC55xx_init                          
	#undef SEC_Fw_MPC55xx_init_debug                    
	#undef SEC_Fw_MPC5643L_HWInit                       
	#undef SEC_Fw_adc_init                              
	#undef SEC_Fw_dspi_init                             
	#undef SEC_Fw_edma_init                             
	#undef SEC_Fw_etimer_init                           
	#undef SEC_Fw_flash_init                            
	#undef SEC_Fw_flexcan_init                          
	#undef SEC_Fw_flexpwm_init                          
	#undef SEC_Fw_intc_init                             
	#undef SEC_Fw_intc_sw_vecttable                     
	#undef SEC_Fw_linflex_init                          
	#undef SEC_Fw_msr_init                              
	#undef SEC_Fw_pit_init                              
	#undef SEC_Fw_rappid_utils                          
	#undef SEC_Fw_romcopy                               
	#undef SEC_Fw_siu_init                              
	#undef SEC_Fw_stm_init                              
	#undef SEC_Fw_swt_init                              
	#undef SEC_Fw_sysclk_init                           
	#undef SEC_Fw_sys_init                              
	#undef SEC_Can_CCChkCan                             
	#undef SEC_Can_CCDrvCan                             
	#undef SEC_Can_CCDrvCanDiag                         
	#undef SEC_Can_CCDrvLogger                          
	#undef SEC_Can_CCDrvSubCan                          
	#undef SEC_Can_CCTestFunc                           
	#undef SEC_Can_CCDrvA13Can                          
	#undef SEC_Can_CCDrvA21Can                          
	#undef SEC_Can_CCDrvB12Can                          
	#undef SEC_Can_CCDrvE101Can                         
	#undef SEC_Can_CCDrvHMCCan                          
	#undef SEC_Can_CCDrvHMC_HDCan                       
	#undef SEC_Can_CCDrvSYCCan                          
	#undef SEC_Can_CCDrvTestCarCan                      
	#undef SEC_Can_CCHmcChkCan                          
	#undef SEC_Can_CCDrvASTRACan                        
	#undef SEC_Can_CCDrvC100Can                         
	#undef SEC_Can_CCDrvFORDCan                         
	#undef SEC_Can_CCDrvGMDWCan                         
	#undef SEC_Can_CCDrvJ300Can                         
	#undef SEC_Can_CCDrvKYRONCan                        
	#undef SEC_Can_CCDrvRAXTONCan                       
	#undef SEC_Can_CCDrvTAHOECan                        
	#undef SEC_App_ETunePar                             
	#undef SEC_App_LTunePar
	#undef SEC_Cal_BMW740i_TUNapCalAbs2WD
	#undef SEC_Cal_BMW740i_TUNapCalEsp2WD
	#undef SEC_Cal_BMW740i_TUNapCalAbsVafs2WD
	#undef SEC_Cal_BMW740i_TUNapCalEspEng01  
/*	#undef SEC_Cal_BMW740i_TUNapCalEspEng02  
	#undef SEC_Cal_BMW740i_TUNapCalEspEng03  
	#undef SEC_Cal_BMW740i_TUNapCalEspEng04  
	#undef SEC_Cal_BMW740i_TUNapCalEspEng05  
	#undef SEC_Cal_BMW740i_TUNapCalEspEng06  
	#undef SEC_Cal_BMW740i_TUNapCalEspEng07  
	#undef SEC_Cal_BMW740i_TUNapCalEspEng08  
	#undef SEC_Cal_BMW740i_TUNapCalEspEng09  
	#undef SEC_Cal_BMW740i_TUNapCalEspEng10  
	#undef SEC_Cal_BMW740i_TUNapCalEspEng11  
	#undef SEC_Cal_BMW740i_TUNapCalEspEng12  
	#undef SEC_Cal_BMW740i_TUNapCalEspEng13  
	#undef SEC_Cal_BMW740i_TUNapCalEspEng14  
	#undef SEC_Cal_BMW740i_TUNapCalEspEng15  
	#undef SEC_Cal_BMW740i_TUNapCalEspEng16  */
	#undef SEC_Cal_BMW740i_TUNapCalEspVafs2WD
	#undef SEC_Cal_BMW740i_TUNapCalRbcOem2WD 
	#undef SEC_CL_LDABSCallSideVref
	#undef SEC_FlexRay
	
	//N0 new allocation	
	#undef SEC_Fs_F_MOC_InterfaceLinkData    
	#undef SEC_Fs_FA_MOC_ActuatorCheck       
	#undef SEC_Fs_F_LoggerBufferSet          
	#undef SEC_Fs_FCPedalSenCheck            
	#undef SEC_Fs_FI_MOC_HallCountSensorCheck
	#undef SEC_Fs_FI_MOC_SwitchCheck         
	#undef SEC_Fs_FS_MOC_CurrentSenCheck     
	#undef SEC_Fs_FS_MOC_ForceSensorCheck    
	#undef SEC_Fs_8B20_FS_HW_VIEW_CHK        
	
	#undef SEC_CL_LATVBBCallActHW          //2011.08.23
	#undef SEC_CL_LCEVPCallControl         //2011.08.23
	#undef SEC_CL_LCRDCMCallControl        //2011.08.23 
	#undef SEC_CL_LCTVBBCallControl        //2011.08.23
	#undef SEC_CL_LDTVBBCallDetection      //2011.08.23

	#undef SEC_Fw_WDrv_ADXR800        //2011.08.23
	#undef SEC_Fw_WDrv_SCA21X      //2011.08.23
	#undef SEC_vx1000		      
	#undef SEC_LAPACCallActHW
	#undef SEC_LASPASCallActHW
	#undef SEC_LCETSCCallControl
	#undef SEC_LCHRCCallControl
	#undef SEC_LCPACCallControl
	#undef SEC_LCSPASCallControl
	#undef SEC_LCTODCallControl
	#undef SEC_LDESPEstDiscTemp
	#undef SEC_LSESPInterfaceIVSS
			      

	#if defined(idx_FILE)
		#undef idx_FILE
	#endif		
		
	#if defined(idx_START)
		#undef idx_START
	#endif
	
	#if !defined(MANDO_SECTION_DEFAULT_AREA)
		#include "MemMap.h"
	#endif		
#else
	#erorr "Header Include Error in autosar"
#endif /*#if defined(__AUTOSAR_HEADER_ARCH_ENA)*/























