#ifndef __CAR_DEFINE_H__
#define __CAR_DEFINE_H__

//
///* ON/OFF */
#define DISABLE         0
#define ENABLE          1

#if (!defined TRUE)
#define TRUE            1
#endif
#if (!defined FALSE)
#define FALSE           0
#endif
#define _VALID          1
#define _INVALID        0

//
///* CAR Maker */
#define HMC_KMC         0x01
#define SSANGYONG       0x02
#define GM_KOREA        0x03
#define GM_USA          0x04
#define GM_CHINA        0x05
#define CHERY           0x10
#define FORD            0x30
#define BMW             0x31
#define TEST_CAR        0xF0


//
///* CAR */
 /* HMC */
#define HMC_BH          0x0001
#define HMC_NF          0x0002
#define HMC_RP          0x0003
#define HMC_FS          0x0004
#define HMC_EN          0x0005
#define HMC_LM          0x0006
#define HMC_VI          0x0007
#define HMC_DH          0x0008
#define HMC_HG          0x0009
#define HMC_YF          0x000A
#define HMC_JB          0x000B
#define HMC_YFE         0x000C
#define HMC_HGE         0x000D
#define HMC_LFPE        0x000E
#define HMC_LFE         0x000F

 /* KMC */
#define KMC_KM          0x0101
#define KMC_HM          0x0102
#define KMC_SO          0x0103
#define KMC_SL          0x0104
#define KMC_KH          0x0105
#define KMC_XM          0x0106
#define KMC_TF          0x0107
#define KMC_MG          0x0108
#define KMC_TFE         0x0109
#define KMC_PSEV        0x010A
#define KMC_VGE         0x010B

 /* GM */
#define GM_TAHOE        0x0201
#define GM_LAMBDA       0x0202
#define GM_J300         0x0203
#define GM_S4500        0x0204
#define GM_MALIBU       0x0205
#define GM_SILVERADO    0x0206
#define GM_T300         0x0207
#define GM_XTS          0x0208
#define GM_M300         0x0209

 /* SYC */
#define SYC_C200        0x0301
#define SYC_KYRON       0x0302
#define SYC_RAXTON      0x0303
#define SYC_X100        0x0304

 /* SGM */
#define SGM_308         0x0401
#define SGM_328		    0x0402
 
 
 /* CHERY */


 /* TEST CAR */
#define DCX_COMPASS      0xF001
#define PSA_207          0xF002
#define PSA_C4           0xF003
#define BYD_5A           0xF004
#define PSA_C3           0xF005
   
/////////////////////////////////////////////////


///* 4WD DRIVE TYPE */
#define FWD_4H          0
#define FWD_AUTO        1
//

//* Engine Variation Set selection */
#define NONE            0    /* 엔진 variation 사양 이원화 안되는 경우 */
#define SET_1           1    /* 엔진 variation 1~16번 사양 */
#define SET_2           2    /* 엔진 variation 초과되는 사양들 */

//* suspension Variation Set selection */
/*Suspension type : KOREA */
#define DOM_1           1
#define DOM_2           2
#define DOM_3           3
#define DOM_4           4
#define DOM_5           5
/*Suspension type : US */
#define US_1            1 
#define US_2            2 
#define US_3            3 
#define US_4            4 
#define US_5            5 
/*Suspension type : EU */
#define EU_1            1 
#define EU_2            2 
#define EU_3            3 
#define EU_4            4 
#define EU_5            5 
/*Suspension type : GEN */
#define GEN_1           1 
#define GEN_2           2 
#define GEN_3           3 
#define GEN_4           4 
#define GEN_5           5

///* Suspension Type */
#define EU              0
#define DOM/*KOREA*/    1
#define US/*USA*/       2
#define GEN/*GENERAL*/  3
#define AUSTRALIA       4

///* Suspension Type2 */
#define SUS_NORMAL      0
#define SUS_SPORTS      1

///* CBS Type */
#define DRUM            0
#define CALIPER         1

///* Sport Mode */
#define SPORTMODE_1     1
#define SPORTMODE_2     2
#define SPORTMODE_3     3

///* WHEEL Drive Type */
#define FRONT_DRIVE     0
#define REAR_DRIVE      1

///* Engine Type */
#define ENG_GASOLINE    0
#define ENG_DIESEL      1
#define ENG_BETA_2_0    2   /* AM beta 2.0 engine */
#define ENG_THETA_2_0   3   /* TD theta 2.0 engine */
#define ENG_THETA_2_4   4   /* TD theta 2.4 engine */
//  
///* ECU */
#define ABS_ECU_1       1
#define ESP_ECU_1       2
//
///* BOOT*/
#define CCP_BOOT        0
#define HMC_BOOT        1
#define STANDALONE      2
#define GM_BOOT         3
#define GM3_BOOT        4
//
///* Drive Type */
#define FRONT_WHEEL_DRV 0
#define REAR_WHEEL_DRV  1
//
///* SENSOR */
#define NONE            0
#define ANALOG_TYPE     1
#define CAN_TYPE        2
#define INTEGRATED_TYPE     3
//
///* Communication */
#define K_LINE          0
#define CAN_LINE        1
#define UDS_CAN_LINE	2
//
///* SPEED SENSOR */
#define WSS_PASSIVE     0
#define WSS_ACTIVE      1
#define WSS_SMART       2
#define WSS_VDA         3

#define PASSIVE         0
#define ACTIVE          1
//
///* Smart WSS Direction  */
#define DR_RIGHT        1
#define DR_LEFT         2
//
///* SPEED SENSOR SWAP */
#define NORMAL          0
#define FLR_SWAP        1
#define RLR_SWAP        2
//
///* HMC CAN_VERSION */
#define CAN_1_1         1
#define CAN_1_2         2
#define CAN_1_3         3
#define CAN_1_4         4
#define CAN_1_6         5
#define CAN_HD_HEV      6
#define CAN_YF_HEV      7
#define CAN_EV          8
#define CAN_HE_KE       9
#define CAN_FCEV        10
#define CAN_2_0         0x20
//

///* SSYC CAN_VERSION */
#define SYMC_X100       0x30
#define SYMC_C200       0x31
#define SYMC_Q150       0x32
#define SYMC_W190       0x33

///* Lamp */
#define COMMON          0
#define SEPERATE        1
//
///*Yaw&LG */
#define FORWARD         1
#define BACKWARD        2
//
///*MP Sensor */
#define _170BAR         1
#define _200BAR         2

//
///*SPLIT Type*/
#define X_SPLIT         0
#define FR_SPLIT        1
#define X_SPLIT_FR_SWAP 2
//
///* CAN CHANNEL */
//#define MAIN_CAN      0
//#define SUB_CAN       1
//
///* LOGGER DATA */
#define LOGIC_DATA      1
#define FS_DATA         2
#define FW_DATA         3
#define CAN_DATA        4
//

//Drive Type Definiton
#define DM_2WD          0x01
//#define DM_4WD        0x01
#define DM_AWD          0x02
#define DM_PTWD         0x04

#define DM_2H           0x10
#define DM_4H           0x20
#define DM_AUTO         0x40
#define DM_LOCK         0x80

/*SSANGYONG*/
#define DM_2WD_EPS      0x02
#define DM_4WD          0x03
#define DM_4WD_EPS      0x04
#define DM_2H           0x10
#define DM_4H           0x20
#define DM_AUTO         0x40
#define DM_LOCK         0x80

//DTC Type
#define HMC_DTC         0
#define GM_DTC          1
#define SYMC_DTC        2

//DV LEVEL
#define DV0             0
#define DV1             1
#define DV2             2
#define DV3             4
#define PV              5
#define SOP             6
#define KR_DV1          7
#define KR_DV2          8

//Wheel Sensor Port Set
#define WFR             0
#define WFL             1
#define WRR             2
#define WRL             3

//VSO Generation Type
#define HARD_WIRE_TYPE  0
#define SW0_GEN_TYPE    1   /*GM - 4000pulses/mile*/

//GM Calibration Set
#define CAL_ABS_01      0x00
#define CAL_ABS_02      0x01
#define CAL_ABS_03      0x02
#define CAL_ABS_04      0x03
#define CAL_ABS_05      0x04
#define CAL_ABS_06      0x05
#define CAL_ABS_07      0x06
#define CAL_ABS_08      0x07
#define CAL_ABS_09      0x08
#define CAL_ABS_10      0x09
#define CAL_ABS_11      0x10
#define CAL_ABS_12      0x11
#define CAL_ABS_13      0x12

#define CAL_ESC_01      0x64
#define CAL_ESC_02      0x65
#define CAL_ESC_03      0x66
#define CAL_ESC_04      0x67
#define CAL_ESC_05      0x68
#define CAL_ESC_06      0x69
#define CAL_ESC_07      0x6A
#define CAL_ESC_08      0x6B
#define CAL_ESC_09      0x6C
#define CAL_ESC_10      0x6D
#define CAL_ESC_11      0x6E
#define CAL_ESC_12      0x6F
#define CAL_ESC_13      0x70
#define CAL_ESC_14      0x71
#define CAL_ESC_15      0x72
#define CAL_ESC_16      0x73
#define CAL_ESC_17      0x74
#define CAL_ESC_18      0x75
#define CAL_ESC_19      0x76
#define CAL_ESC_20      0x77
#define CAL_ESC_21      0x78
#define CAL_ESC_22      0x79
#define CAL_ESC_23      0x7A
#define CAL_ESC_24      0x7B
#define CAL_ESC_25      0x7C
#define CAL_ESC_26      0x7D
#define CAL_ESC_27      0x7E
#define CAL_ESC_28      0x7F
#define CAL_ESC_29      0x80
#define CAL_ESC_30      0x81
#define CAL_ESC_31      0x82
#define CAL_ESC_32      0x83
#define CAL_ESC_33      0x84
#define CAL_ESC_34      0x85
#define CAL_ESC_35      0x86
#define CAL_ESC_36      0x87
#define CAL_ESC_37      0x88
#define CAL_ESC_38      0x89
#define CAL_ESC_39      0x8A
#define CAL_ESC_40      0x8B
#define CAL_ESC_41      0x8C
#define CAL_ESC_42      0x8D

//OSC TYPE
#define OSC_20MHZ       0x00
#define OSC_40MHZ       0x01            

// SYSTEM CLOCK
#define SYSTEMCLK_60MHZ 0 
#define SYSTEMCLK_80MHZ 1 
#define SYSTEMCLK_100MHZ 2
#define SYSTEMCLK_120MHZ 3

//
// MGH80 __ECU_TYPE
#define  __EPB          1
#define  __MOC          2

// MGH80 __ECU_TYPE_2
#define __INTEGRATED    10
#define __NON_INTEGRATED 11

// MCU SELECT
#define MCU_PICTUS      0
#define MCU_LEOPARD     1

 /* IMU CAN_VERSION */
#define CAN_MD          1
#define CAN_HMC         2


#endif    //__CAR_DEFINE_H__
