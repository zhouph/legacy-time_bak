/*
*******************************************************************************
+------------------------------------------------------------------------------
| Directory Par
| Filename  car.par
| Project   MGH80 INTERNATIONAL
+------------------------------------------------------------------------------
*******************************************************************************
=> Revision Note
-------------------------------------------------------------------------------
*******************************************************************************
*/

/******************************************************************************
* Car.par Discription
* Selectable Factors for Car.par MACROs
* __CAR_MAKER                       HMC_KMC or SSANGYONG or GM_KOREA or GM_CHINA or GM_USA or FORD or TEST_CAR or CHERY or CHANA
* __CAR                             KMC_HM    or HMC_KMC   or HMC_KM or HMC_PA or KMC_SA
* CAR_VAR_NUM                       DOM or US or EU or GEN
* __AHB_SYSTEM                      ENABLE or DISABLE
* __SUSPENSION_TYPE                 USA or  KOREA   or  EU
* __CBS_TYPE                        CALIPER(Default) or DRUM
* __TEST_MODE_ENABLE                ENABLE or DISABLE
*********************************************************************************/
#ifndef __CAR_PAR
#define __CAR_PAR

#include "car_def.h"

/* CAR Definiton */
#define __CAR_MAKER                HMC_KMC          /* C100, TAHOE의 경우 TEST_CAR */
#define __CAR                      HMC_BH


/* Calibration Set Selection */
#if __CAR_MAKER==GM_KOREA
    #if __ECU==ABS_ECU_1
        #define __GM_CAL_SET                CAL_ABS_01
    #elif __ECU==ESP_ECU_1
        #define __GM_CAL_SET                CAL_ESC_01
    #endif
#else
    /*Car Varinat Number*/
    #define CAR_VAR_NUM                 1
#endif

/* Engine Variation Set selection */
#define __ENG_VAR_SET               NONE

/* Engine Selection */
#define __ENG_TYPE                  ENG_GASOLINE

/* AHB System selection */
#define __AHB_SYSTEM                DISABLE

/* Suspension selection */
#define __SUSPENSION_TYPE           DOM

#if (__MGH80_MOCi == ENABLE)
    /* EPB System selection */
    #define __ECU_TYPE                  __EPB   // __EPB    // J300, Astra Only __MOC   
    #define __ECU_TYPE_2                __INTEGRATED    //__INTEGRATED    // NONE 
#endif

/* car path */
#include "../../Appl/APSelectPar.h"

/* New fail management */
#define NEW_FM_ENABLE              DISABLE // ENABLE



//#define _G2XX_ECU            // RevC Smpl ( G2xx 대응용) ECU 사용 시.

/* Test Mode selection */
#define __TEST_MODE_ENABLE         ENABLE// DISABLE//ENABLE 
#if __TEST_MODE_ENABLE == ENABLE
    #include "CAR_TEST.par"
    #define __LOGGER_DATA_TYPE       LOGIC_DATA //FW_DATA
#endif

#endif  /*__CAR_PAR*/
