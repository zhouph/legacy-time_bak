/*
***********************************************************************************
+----------------------------------------------------------------------------------
| Directory SYSTEM
| Filename  MANDO_VERSION.h
| Project   MGH40 INTERNATIONAL
+----------------------------------------------------------------------------------

***********************************************************************************
=> Revision Note
-----------------------------------------------------------------------------------

***********************************************************************************
*/
#ifndef __MANDO_VERSION_H__
#define __MANDO_VERSION_H__

typedef unsigned int UINT;
typedef unsigned char UCHAR;
typedef unsigned long ULONG;

/*///////////////////////////////////////////////////////////////////////////*/
/*   HMC RELEASE VERSION */
/*///////////////////////////////////////////////////////////////////////////*/
/*
+------------------+
| SOFTWARE VERSION |
+------------------+
 X0000000000-0000
 ||||||||||| ||||
 ||||||||||| ||++-> release date(0~31)
 ||||||||||| |+---> release month(0~C)
 ||||||||||| +----> release year(0~9)
 |||||||||++------> diagnosis version(00~99)
 |||||||++--------> failsafe version(0A~9Z)
 |||||++----------> logic version(AA~ZZ)
 ||||+------------> system code2(A~Z)
 |||+-------------> system code1(1~4)
 |++--------------> vehicle(00~99)
 +----------------> proto

+----------------------+---+------------------+----------------------+
|    |  VEHICLE        |   |   SYSTEM code1   |   |  SYSTEM code2    |
|----+-----------------+---+------------------+---+------------------|
| 09 | HMC LC          | 1 | 2WD+no EBD       | A | ABS              |
| 10 | HMC FO          | 2 | 2WD+   EBD       | B | ABS+TDWS         |
| 11 | HMC SM          | 3 | 4WD+no EBD       | C | ABS+BTCS         |
| 12 | HMC SR          | 4 | 4WD+   EBD       | D | ABS+BTCS+TDWS    |
| 13 | HMC XD          +---+------------------+ E | ABS+FTCS         |
| 14 |                 |                      | F | ABS+FTCS+TDWS    |
| 15 | HPI DS-2        |                      | G | ABS+FTCS+VDC     |
| 20 | HMC FC          |                      | H | ABS+FTCS+VDC+TDWS|
| 21 | HMC GK          |                      | I | ABS+ETCS         |
| 43 | HMC SM          |                      |                      |
+----------------------+                      +----------------------+

*/


typedef struct
{
	UCHAR VehicleName[2];
	UCHAR SystemCode[2];
	UCHAR LogicVer[2];
	UCHAR FailsafeVer[2];
	UCHAR DiagVer[2];
}FIXED_VER_t;
	
typedef struct
{
	UCHAR Reserved_0
	UCHAR VehicleName[2];
	UCHAR SystemCode[2];
	UCHAR LogicVer[2];
	UCHAR FailsafeVer[2];
	UCHAR DiagVer[2];
	UCHAR SubVer;
	UCHAR Reserved_1;
	UCHAR ReleaseDate[4];
}VERSION_t;


typedef struct
{
	UCHAR CAR_NAME[5];
	UCHAR SYSTEM_NAME[5];
	UCHAR DRIVE_MODE[5];
	UCHAR Reserved;
	VERSION_t VerIDInfo;
}HMC_VERSION_t;

/*///////////////////////////////////////////////////////////////////////////*/
/*   MANDO ENGINEER VERSION */
/*///////////////////////////////////////////////////////////////////////////*/
typedef struct
{
	UINT ABS_ECU	:1;
	UINT ESP_ECU	:1;
	UINT ESP_PLUSE_ECU	:1;
	UINT ABS_PLUSE_ECU	:1;
	UINT reserved	:4;
}ECU_TYPE_t;

typedef struct
{
	UINT EBD_BF	:1;
	UINT ABS_BF	:1;
	UINT BTC_BF	:1;
	UINT TCS_BF	:1;
	UINT VDC_BF	:1;
	UINT reserved :3;
}BF_LOGIC_TYPE_t;

typedef struct
{
	UINT SAS_TYPE	:2;
	UINT WHEEL_SENSOR_TYPE	:2;
	UINT YAW_SENSOR_TYPE	:2;
	UINT MP_SENSOR_TYPE	:2;
}SENSOR_TYPE_t;

typedef struct
{
	UINT HMC_CAN_VER	:4;
	UINT DIAG_TYPE		:1;
	UINT CLUSTER_TYPE	:1;
	UINT CCP_TYPE		:1;
	UINT reserved		:1;
}COMM_TYPE_t;

typedef struct 
{
	UINT HDC_SW_TYPE	:2;
	UINT PARK_BRAKE_TYPE	:1;
	UINT HDC_LAMP_TYPE	:2;
	UINT ABS_LAMP_TYPE	:1;
	UINT reserved	:2;
}LAMP_SW_TYPE_t;

typedef struct 
{
	UINT LOGGER_ENABLE :1;
	UINT LOGGER_CHANNEL	:1;
	UINT LOGGER_DATA	:2;
	UINT MECU_MODE		:1;
	UINT AX_SENSOR		:1;
	UINT reserved		:2;
}TEST_TYPE_t;
			

typedef struct
{
	UINT HDC_VF	:1;
	UINT HSA_VF	:1;
	UINT ROP_VF	:1;
	UINT EDC_VF	:1;
	UINT ETC_VF :1;
	UINT DECEL_VF :1;
	UINT UCC_VF :1;
	UINT TSP	:1;
}VAF_LOGIC_TYPE_t;

typedef struct
{
	UCHAR PART_ID[2];
	UCHAR PART_VER[2] ;
	UCHAR RELESE_DATE[4];
}PART_VERSION_t;
		
	
typedef struct
{
	UCHAR CAR_ID;
	UINT VARIANT_CODE_ENABLE	:1;
	UINT FWD_TYPE	:1;
	UINT FWD_DRIVE_TYPE	:1;
	UINT DRIVE_TYPE	:1;
	UINT SUSPENSION_TYPE :3;
	UINT BOOT_DRV_TYPE	:1;
	UCHAR EcuType;
	BF_LOGIC_TYPE_t BaseFuncType;
	VAF_LOGIC_TYPE_t VAFuncType;		
	SENSOR_TYPE_t SensorType;
	LAMP_SW_TYPE_t LampSWType;
	COMM_TYPE_t CommType;
	TEST_TYPE_t TestType;	
	PART_VERSION_t LogicVer;
	PART_VERSION_t SystemVer;
	PART_VERSION_t FailsafeVer;
	PART_VERSION_t CanVer;
	PART_VERSION_t DiaVer;
	PART_VERSION_t TunVer;
	PART_VERSION_t BootVer;	
}MANDO_ENGINEER_VERSION_t;


extern const HMC_VERSION_t HmcVerInfo; 
extern const MANDO_ENGINEER_VERSION_t MandoVerInfo;

#endif

	
	
	
	
	
	
	
	
	
	



	
		
	