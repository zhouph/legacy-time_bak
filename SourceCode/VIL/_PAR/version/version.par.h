/***********************************************************************************
+------------------+
| SOFTWARE VERSION |
+------------------+
 X0000000000-0000
 ||||||||||| ||||
 ||||||||||| ||++-> release date(0~31)
 ||||||||||| |+---> release month(0~C)
 ||||||||||| +----> release year(0~9)
 |||||||||++------> diagnosis version(00~99)
 |||||||++--------> failsafe version(0A~9Z)
 |||||++----------> logic version(AA~ZZ)
 ||||+------------> system code2(A~Z)
 |||+-------------> system code1(1~4)
 |++--------------> vehicle(00~99)
 +----------------> proto

+----------------------+---+------------------+----------------------+
|    |  VEHICLE        |   |   SYSTEM code1   |   |  SYSTEM code2    |
|----+-----------------+---+------------------+---+------------------|
| 09 | HMC LC          | 1 | 2WD+no EBD       | A | ABS              |
| 10 | HMC FO          | 2 | 2WD+   EBD       | B | ABS+TDWS         |
| 11 | HMC SM          | 3 | 4WD+no EBD       | C | ABS+BTCS         |
| 12 | HMC SR          | 4 | 4WD+   EBD       | D | ABS+BTCS+TDWS    |
| 13 | HMC XD          +---+------------------+ E | ABS+FTCS         |
| 14 |                 |                      | F | ABS+FTCS+TDWS    |
| 15 | HPI DS-2        |                      | G | ABS+FTCS+VDC     |
| 20 | HMC FC          |                      | H | ABS+FTCS+VDC+TDWS|
| 21 | HMC GK          |                      | I | ABS+ETCS         |
| 43 | HMC SM          |                      |                      |
+----------------------+                      +----------------------+
***********************************************************************************/
#ifndef __VERSION_PAR
#define __VERSION_PAR

/*#pragma CONST_SEG SOFTWARE_VER*/
/***********************************************************************************/
#if (__CAR_MAKER==HMC_KMC) || (__CAR_MAKER==GM_CHINA)
/***********************************************************************************/
        //uint8_t MANDO_SW_Release_Counter=1;   /* S/W Release 시 마다 하나씩 증가 */
        uint8_t ELECTRIC_SW_DLS[1]={"A"};
        
        #if __ECU==ABS_ECU_1
            const uint8_t VERSION[2][16] = {"NF ABS          ",
                                            "040810 Version  "};
        #elif __ECU==ESP_ECU_1
            const uint8_t VERSION[2][16] = {"TD ESP          ",
                                            "040810 Version  "};
        #endif
    
/***********************************************************************************/
#elif __CAR_MAKER==GM_KOREA
/***********************************************************************************/
        uint8_t GM_SW_Release_Counter=1;   /* S/W Release 시 마다 하나씩 증가 */
        uint8_t GM_APPL_SW_DLS[2]={"AA"}; 

        #if __ECU==ABS_ECU_1
            const uint8_t VERSION[2][16] = {"V200 ABS        ",
                                            "040810 Version  "};
        #elif __ECU==ESP_ECU_1
            const uint8_t VERSION[2][16] = {"V200 ESP        ",
                                            "060425 Version  "};
        #endif
/***********************************************************************************/
#elif __CAR_MAKER==SSANGYONG
/***********************************************************************************/
        uint8_t SYMC_SW_Release_Counter=1;   /* S/W Release 시 마다 하나씩 증가 */
        
        #if __ECU==ABS_ECU_1
            const CHAR VERSION[2][16] = {"X100 ABS 4WD    ",
                                         "060425 Version  "};
        #elif __ECU==ESP_ECU_1
            const CHAR VERSION[2][16] = {"X100 ESC 4WD    ",
                                         "060425 Version  "};
        #endif
/***********************************************************************************/
#elif __CAR_MAKER==TEST_CAR
/***********************************************************************************/
        uint8_t MANDO_SW_Release_Counter=1; /* S/W ID 동일하게 배포시 Release 시 하나씩 증가 */

        #if __ECU==ABS_ECU_1
            const uint8_t VERSION[2][16] = {"COMPASS ABS     ",
                                            "040810 Version  "};
        #elif __ECU==ESP_ECU_1
            const uint8_t VERSION[2][16] = {"COMPASS ESP     ",
                                            "060425 Version  "};
        #endif

#endif
/*#pragma CONST_SEG DEFAULT*/

#endif /* __VERSION_PAR */
