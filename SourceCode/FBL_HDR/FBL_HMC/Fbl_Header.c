/****************************************************************************
| Project Name: GM-Header
|    File Name: GMHeader.C
|
|  Description: Flash Boot Loader configuration file
|
|-----------------------------------------------------------------
|               C O P Y R I G H T
|-----------------------------------------------------------------
| Copyright (c) 2003 by Vector Informatik GmbH, all rights reserved
|-----------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-----------------------------------------------------------------
| Initials      Name                   Company
| --------      --------------------   ---------------------------
|  Hp            Armin Happel          Vector Informatik GmbH
|-----------------------------------------------------------------
|               R E V I S I O N   H I S T O R Y
|-----------------------------------------------------------------
| Date         Ver   Author   Description
| ---------    ----  ------   ------------------------------------
| 2003-07-23   1.00   Hp      Creation
|*****************************************************************************/

#define GM_HEADER_DATA

/******************************************************************************/
/* Include files                                                              */
/******************************************************************************/
#include "Fbl_Header.h"
unsigned char OP_PN[4]={0x01,0x00,0x00,0x01};
unsigned char BOOT_PN[4]={0x00,0x00,0x00,0x01};
unsigned char CAL_PN[4]={0x02,0x00,0x00,0x01};


/* #pragma to locate data to the first segment of the application via linker command file!!!  */
#define	FBLHEADER_START_SEC_CONST
#include "Fbl_MemMap.h"

/* The following data is an example for a GM-Header with 2-byte HFI */
const  tSwmApplHeader GM_ApplHeader=
{
   /* 2-byte Checksum. 11AA is a placeholder for CANFlash to calculate and put in the checksum */
   {CSUM_PLACEHOLDER_HIBYTE,
    CSUM_PLACEHOLDER_LOBYTE },

   /* 2 bytes for the Module ID */
   {MODID_APPLICATION_HIBYTE,
    MODID_APPLICATION_LOBYTE },

   /* Define HFI Byte(s) */
   /* Use 4 bytes SWMI-data and 2 bytes Alpha-Code (DLS) */
#ifdef SWM_USE_2BYTE_HFI
   ((SWMI_DATALEN<<3) | ((DLS__DATALEN&1)<<2) | HFI_1STBYTE_SIZE_2BYTE | HFI_1STBYTE_PMA_PRESENT),
# if defined( SWM_USE_MPFH )
   (HFI_2NDBYTE_MPFH_PRESENT |
# else
   (HFI_2NDBYTE_MPFH_NOT_PRESENT |
# endif
# if defined( SWM_USE_DCID )
   HFI_2NDBYTE_DCID_PRESENT),
# else
   HFI_2NDBYTE_DCID_NOT_PRESENT),
#endif
#else
   ((SWMI_DATALEN<<3) | ((DLS__DATALEN&1)<<2) | HFI_1STBYTE_SIZE_1BYTE | HFI_1STBYTE_PMA_PRESENT),
#endif
   /* Followed by 4 byte SWMI data */
   { 0x2A,0x07,0x08,0x08 },

   /* Followed by 2 byte Alpha code (DLS) */
   { 'A','A' },

#if defined( SWM_USE_DCID )
   /* Followed by 2 byte Data Compatibility Identifier */
   { DCID_APPLICATION_HIBYTE, DCID_APPLICATION_LOBYTE },
#endif

   /* Now, reserve space for the PMAs */
   {
#ifdef SWM_USE_2BYTE_HFI
      0,
#endif
      SWM_DATA_MAX_NOAR
   },


   /* Provide as much PMAs as defined in MAX_NOAR (we use 10 in our example). */
   {
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 1st PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 2nd PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 3rd PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 4th PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 5th PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 6th PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 7th PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 8th PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 9th PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 10th PMA */

     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 11st PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 12nd PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 13rd PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 14th PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 15th PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 16th PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 17th PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 18th PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 19th PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 20th PMA */

     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 21st PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 22nd PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 23rd PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 24th PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 25th PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 26th PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 27th PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 28th PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 29th PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 30th PMA */

     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 31st PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 32nd PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 33rd PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 34th PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 35th PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 36th PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 37th PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 38th PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF,    /* 39th PMA */
     0xFF,0xFF,0xFF,0xFF,   0xFF,0xFF,0xFF,0xFF    /* 40th PMA */
   }



#if defined( SWM_USE_MPFH )
   /* Followed by Number-of-Additional-Modules (NOAM) */
   ,{ 0, SWM_DATA_MAX_NOAM },

   /* Provide as many additional-modules in NOAM (we use 2 in our example). */
   {
# if defined( SWM_USE_DCID )
      0x80, 0x00,  0x00,0x01,  0xFF,0xFF,0xFF,0xFF,  0xFF,0xFF,0xFF,0xFF,   /* 1st additional-module */
# else
      0x00,0x01, 0xFF,0xFF,0xFF,0xFF,  0xFF,0xFF,0xFF,0xFF,                /* 1st additional-module */
# endif
   }
#endif

};


#define	FBLHEADER_STOP_SEC_CONST
#include "Fbl_MemMap.h"





#define CALIBRATION_SET0_START
#include "Fbl_MemMap.h"

const  tSwmCalHeader GM_CalHeader_0=
{
   /* 2-byte Checksum. 11AA is a placeholder for CANFlash to calculate and put in the checksum */
   {CSUM_PLACEHOLDER_HIBYTE,
    CSUM_PLACEHOLDER_LOBYTE },

   /* 2 bytes for the Module ID */
   {MODID_CALIBRATION_HIBYTE,
    MODID_CALIBRATION_LOBYTE },

   /* Define HFI Byte(s) */
   /* Use 4 bytes SWMI-data and 2 bytes Alpha-Code (DLS) */
#ifdef SWM_USE_2BYTE_HFI
   ((SWMI_DATALEN<<3) | ((DLS__DATALEN&1)<<2) | HFI_1STBYTE_SIZE_2BYTE | HFI_1STBYTE_PMA_NOT_PRESENT),
    HFI_2NDBYTE_DCID_PRESENT,
#else
   ((SWMI_DATALEN<<3) | ((DLS__DATALEN&1)<<2) | HFI_1STBYTE_SIZE_1BYTE | HFI_1STBYTE_PMA_PRESENT),
#endif

   /* Followed by 4 byte SWMI data */
   { 0x00,0xBC,0x61,0x4E },

   /* Followed by 2 byte Alpha code (DLS) */
   { 'A','A' },
   { DCID_APPLICATION_HIBYTE, DCID_APPLICATION_LOBYTE }
};
#define CALIBRATION_SET0_STOP
#include "Fbl_MemMap.h"

/************   Organi, Version 1.47 Vector-Informatik GmbH  ************/
