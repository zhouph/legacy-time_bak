/****************************************************************************
|    File Name: FBL_DEF.H
|
|  Description: Main definitions for the Flash Boot Loader
|
|-----------------------------------------------------------------------------
|               C O P Y R I G H T
|-----------------------------------------------------------------------------
| Copyright (c) 2003-2007 by Vector Informatik GmbH, all rights reserved.
|
| This software is copyright protected and proprietary
| to Vector Informatik GmbH. Vector Informatik GmbH
| grants to you only those rights as set out in the
| license conditions. All other rights remain with
| Vector Informatik GmbH.
|
|-----------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-----------------------------------------------------------------------------
| Initials     Name                      Company
| --------     ---------------------     -------------------------------------
| CB           Christian Baeuerle        Vector Informatik GmbH
| Fz           Ralf Fritz                Vector Informatik GmbH
| Hp           Armin Happel              Vector Informatik GmbH
| Ls           Konrad Lazarus            Vector Informatik GmbH
| ThS          Thomas Sommer             Vector Informatik GmbH
| WM           Marco Wierer              Vector Informatik GmbH
| ACP          Alexandre C. Plombin      Vector CANtech, Inc.
| An           Ahmad Nasser              Vector CANtech, Inc.
| Rr           Robert Schaeffner         Vector Informatik GmbH
| Ap           Andreas Pick              Vector Informatik GmbH
| MFR          Michael Radwick           Vector CANtech, Inc.
| Et           Thomas Ebert              Vector Informatik GmbH
| BJE          Ben Erickson              Vector CANtech, Inc.
| QPs          Quetty Palacios           Vector Informatik GmbH
| FHe          Florian Hees              Vector Informatik GmbH
| swk          Slawomir Wilk             Vector Informatik GmbH
| Ci           Andre Caspari             Vector Informatik GmbH
| Awh          Andreas Wenckebach        Vector Informatik GmbH
| JHg          Joern Herwig              Vector Informatik GmbH
| Kak          Keith Kalski              Vector CANtech, Inc.
| Pal          Phil Lapczynski           Vector CANtech, Inc.

|-----------------------------------------------------------------------------
|               R E V I S I O N   H I S T O R Y
|-----------------------------------------------------------------------------
| Date        Version   Author  Description
| ----------  --------  ------  ----------------------------------------------
| 2002-01-16  00.09.00  CB      Creation
| 2002-02-21  00.91.00  Ls      Minor changes for the TMS470
|                               kCanTxOk/kCanTxFailed and kFblOk/kFblFailed added
|                               TMS470 added: start from appl over reset
| 2002-04-05  00.92.00  Ls      Define for SWM_DATA_ROW_SIZE updated
|                       ThS     Support for HC08 and VWTP1.6
| 2002-04-09  00.93.00  CB      Minor changes
|                               Definition of logical block table changed
|                               Definition of tLogicalBlockHeader removed (not used)
|                       ThS     tSWMI_PMA not necessary for VAG
| 2002-04-29  00.94.00  Ls      tSwmStoredDataTable: GM-section changed,
|                               tSwmDataStdTable added
| 2002-05-07  00.95.00  Ls      tSwmDataStdTable only for GM
|                               FBL_ADDR_TYPE/FBL_MEMSIZE_TYPE for
|                               C_COMP_TASKING_C16X changed
|                               MEMORY_HUGE added
| 2002-05-08            WM      Support for Hiware HC12 added
| 2002-05-16  00.96.00  Fz      Default value for FBL_REPEAT_CALL_CYCLE added
| 2002-05-16            Th      Added support of extended CAN identifier in
|                               CanInitTable
| 2002-05-15            CB      ESCAN00002873: Additional definition of the MTAB
|                               magic flag value
| 2002-05-23            Ls      changes in the GM-section for GMW3110V1.4
| 2002-06-12            Ls      #pragma for IAR_M16C added to FblLogicalBlockTable
| 2002-07-02  00.97.00  CB      FBL_ADDR_TYPE and FBL_MEMSIZE_TYPE changed for HC12
|                       Ls      const -> MEMORY_ROM
|                       WM      Adaptation for PPC
| 2002-07-17  00.98.00  CB      ENABLE_PROGRAMMING_MODE added
|                       WM      Added FBL_BOTTOM_BOOT_BLOCK define for PPC
|                       Ls      Added FBL_BOTTOM_BOOT_BLOCK define for TMS470
| 2002-07-18  00.99.00  CB      ESCAN00003274: Additional assign flags in the LBT
| 2002-07-29            Ls      kBlockTypeExt added
| 2002-08-06            WM      ESCAN00003438: Removed block disposability from
|                               tBlockDescriptor
| 2002-08-26  01.00.00  CB      ESCAN00003600: Added address member in tSegmentInfo
| 2002-09-04  01.01.00  Ls      Modeswitches of TMS470 removed
| 2002-09-12  01.02.00  CB      ESCAN00003773: tFblHeader structure definition
|                                expanded
|                       Ls      ESCAN00003764: FblLogicalBlockTable mapping
|                                changed for M16C
| 2002-09-18  01.03.00  Fz      Erase counter of V850 removed
| 2002-09-16  01.04.00  ACP     Added ST9 + Security Access Defines for FORD
| 2002-09-26            ACP     Modified MEMORY_HUGE setting so that it still
|                               works with FNOS
| 2002-10-07            WM      Minor changes for METROWERKS_PPC
| 2002-10-15  01.05.00  Fz      Support for Hitachi SH7047 added
| 2002-10-25            ACP     "_COMMENT" added to some C_COMP_GNU_ST9 ifdef's
| 2002-10-25  01.06.00  WM      Changes for DC version
| 2002-11-13  01.07.00  WM      Added paging support for HC12/STAR12
|                       WM      Added result types
| 2002-12-11  01.08.00  WM      Moved tSegmentInfo, tSegmentList to secmod.h
|                       WM      Added tFblAddress, tFblLength and tFblData for DC
| 2003-01-14  01.09.00  WM      ESCAN00004713: Fixed tFblAddress, tFblLength define
|                               Added tFblErrorType, tFblErrorCode and error
|                               defines for DC
| 2003-02-04  01.10.00  An      Added C_COMP_TASKING_XC16X
| 2003-02-18  01.11.00  CB      Changed C_COMP_HEW_SH7055 -> C_COMP_HEW_SH705X
| 2003-03-20  01.12.00  CB      Minor changes for XC16X
| 2003-04-01  01.13.00  Rr      Minor changes for NEC v850SCx
| 2003-04-07  01.14.00  Rr      Removed C_COMP_ANSI_TMS_470
| 2003-04-30  01.15.00  WM      Minor changes for PPC
|                       WM      Minor changes for VAG
| 2003-05-20  01.16.00  WM      ESCAN00005703: Adaptations for C_COMP_HIWARE_12
|                       CB      ESCAN00005681: Support for ST10-CCAN
| 2003-06-04  01.17.00  WM      ESCAN00005759: Added COMP_COSMIC_12 support
|                               for VAG
| 2003-06-11  01.18.00  WM      ESCAN00005848: Adaptations for MB90F394H
| 2003-06-11  01.19.00  Ls      ESCAN00005966: minor changes for M16C,
|                               tSwmStoredDataTable removed
| 2003-06-11  01.20.00  Rr      M32C added
|                               GM-Bootloader header added to the FBL-Header
| 2003-08-07  01.21.00  Ls      ESCAN00006198: M16C Mitsubishi added
|                       Th      ESCAN00006294: Changed C_ENABLE_EXTENDED_ID
|                               in FBL_ENABLE_EXTENDED_ID
|                       CB      Minor changes
| 2003-08-25  01.22.00  Ls      ESCAN00006477: FBL_ENABLE_START_OVER_RESET added
| 2003-09-05  01.23.00  WM      ESCAN00006539: Minor changes for PPC
| 2003-09-19  01.24.00  Rr      V850 added
| 2003-09-19  01.25.00  Rr      V850 NEC Compiler switch added
| 2003-11-05  01.26.00  Rr      ESCAN00006817: Add define
|                       Ls      ESCAN00006949: C_COMP_IAR_H8S added
| 2004-02-10  01.27.00  CB      ESCAN00007119: New OEM
|                       Ls      ESCAN00007291: New OEM
|                       WM      ESCAN00007465: Adaptations for C_COMP_IAR_12
| 2004-02-27  01.28.00  Rr      Added new OEM
| 2004-03-23  01.29.00  ACP     Added new OEM
|                       Ls      ESCAN00007291: Minor changes for one OEM
|                       Hp      ESCAN00007972: Internal defines not properly
|                                closed by #endif
|                       WM      ESCAN00008101: Minor changes for CLIENT_DC_UDS
|                       ACP     Added tSegmentInfo for Ford_GGDS
|                       AMN     Added GGDS to fblMode definitions, added
|                               FBL_START_WITH_RESP
| 2004-05-13  01.30.00  Hp      Removing section definition of FBLMTAB for FJ16LX
| 2004-05-19  01.31.00  CB      Additional seed-key constant added
| 2004-06-28  01.32.00  Ls      ESCAN00008376: New OEM
|                       WM      Changes for new Organi version
|                       Fz      ESCAN00008684: Add changeable CGMCS for NEC V850
| 2004-08-03  01.33.00  WM      ESCAN00009056: Added space modifier to tExportFct
|                               for C_COMP_COSMIC_12
|                               ESCAN00009075: Added maximum number of
|                                reprogramming cycles to tLogicalBlockTable
| 2004-08-18  01.34.00  WM      ESCAN00009216: Extended ECU PIN handling for
|                                CLIENT_CLAAS
|                               ESCAN00009267: Removed mtab related stuff from
|                                CLIENT_CLAAS section
| 2004-09-08  01.35.00  Ls      ESCAN00009561: Added C_COMP_HEW_H8S to 16Bit
|                               declaration of FBL_XXX_TYPE
| 2004-09-11  01.36.00  AP      ESCAN00009454: Removing section definition of
|                                FblLogicalBlockTable for NECV850/NEC
| 2004-09-11  01.37.00  Ls      ESCAN00009508: changed define dependencies for
|                                FORD and GM in the mtab section
| 2004-09-23  01.38.00  Rr      ESCAN00009454: Corrected section definition of
|                               FblLogicalBlockTable for NECV850/NEC
|                       AMN     Added bitrate parameters for C_COMP_TASKING_ST10_CCAN
| 2004-10-05  01.39.00  Rr      ESCAN00009768: Naming convention
| 2004-10-26  01.40.00  Rr      Add support for M32R
|                       Ap      ESCAN00010007: Support for Vector canbedded
|                                components
| 2004-11-05  01.41.00  MFR     ESCAN00009049: Add return codes for SecurityAccess
|                                response
|                               Remove flash-driver version record from
|                                SWMI-Root Table
|                               Add kFblBufferAlign to handle Flash source-buffer
|                                alignment
| 2004-11-08  01.42.00  Rr      Section definition changes for V850 GHS to support
|                                different compiler options
| 2004-11-12  01.43.00  Hp      Adding TLCS-900 to the CanInit structure
|                       Rr      Minor changes due to version defines changes
| 2004-11-29  01.44.00  Fz      Add error handling datatypes for VAG
| 2004-12-23  01.45.00  AMN     Converted FBL_CLIENT_GGDS to FBL_CLIENT_FORD_GGDS
|                       CB      Support for new OEM
| 2005-01-06  01.46.00  Fz      Add of NEC V850 PS2
|                       Et      ESCAN00010695: Support TX19 series (TMP1984)
|                       WM      Minor change for MC9S12X
|                       AMN     Changed FBL_VERSION to FBLOEM_FORD_RELEASE_VERSION
|                                for CLIENT_FORD
| 2005-01-25  01.47.00  CB      Changes for SLP8
|                       CB      ESCAN00010787: Names of FblHeader members changed
| 2005-01-31  01.48.00  AMN     Added support for TMS320
|                       MHz     Addded support for SA level 2 (upload)
|                               changed define kProgRequest and kNoProgRequest
| 2005-02-28  01.49.00  AMN     Added START_OVER_RESET support for FORD_SLP3
|                       CB      Added defines for FblHeader data and FblStart
|                               Minor changes for 78k0
| 2005-04-15  01.50.00  BJE     Changed FBL_MEMSIZE_TYPE to canuint32 for ST9
|                       BJE     Added ST9 specific CallFblStart macro and deleted
|                                extra #if defined(FBL_ENABLE_FBL_START)
| 2005-04-28  01.51.00  MHz     Removed double definition of access macros
|                                for FblHeader, added tDecParam for CLIENT_FORD_GGDS
| 2005-04-29  01.52.00  WM      ESCAN00012128: Unbalanced parenthesis in macro
|                                definition (FBL_CLIENT_FORD)
| 2005-05-11  01.53.00  Hp      Add support for V85x/IAR+NEC
|                       Rr      Add OEM
|                               Minor changes
| 2005-04-29  01.54.00  WM      ESCAN00012128: Unbalanced parenthesis in macro
|                                definition
| 2005-07-26  01.55.00  Ls      C_COMP_HEW_SH2_RCAN added
|                       QPs     C_COMP_FUJITSU_FR60_CCAN added with CanInitBT2
|                                definition for small baudrate
|                       FHe     Added support for C_COMP_METROWERKS_DSP5683X
| 2005-07-28  01.56.00  Rr      Added support for C_COMP_HEW_H8S_COMMENT
| 2005-08-10  01.57.00  AMN     Added defines of FblHeader and FblStart for FORD_GGDS
|                               Removed unnecessary FblSetStartWithResp macros for
|                                FORD_GGDS
| 2005-08-23  01.58.00  Rr      Added C_COMP_IAR_CR16 support
|                       QPs     Added C_COMP_FUJITSU_FR60_CCAN support
| 2005-09-13  01.59.00  Ls      ESCAN00013178: FBL_ENABLE_ROMMASK_FLAG added (PAG)
| 2005-11-09  01.60.00  MFR     Support for FBL_CLIENT_GM_SLP2
|                       BJE     Add support for 78k0 banked
|                       WM      ESCAN00013678: C_COMP_IAR_M16C/M32C added
|                       Rr      ESCAN00014218: removed possible compiler error if
|                                application and FBL are configured by GENy
| 2005-11-17  01.61.00  MFR     Match definition of tFblHeader to GENy for GM.
| 2005-12-08  01.62.00  swk     ESCAN00014459: Added C_COMP_KEIL_XC16X_COMMENT
|                       QPs     ESCAN00014563: Added C_COMP_QCC_MGT5X00_MSCAN12
| 2005-12-29  01.63.00  FHe     Added NONBANKED pragma for FblStackInit and
|                               C_COMP_HIWARE_12
| 2006-01-16  01.64.00  swk     ESCAN00014910: Encapsulated FblStackInit()
|                               with FBL_CLIENT_FORD_SLP3_COMMENT
| 2006-01-25  01.65.00  QPs     ESCAN00015074: Added support for C_COMP_MTRWRKS_MPC5X00
| 2006-03-03  01.66.00  QPs     ESCAN00015168: Added support for C_COMP_DIABDATA_MPC55XX
|                       Hp      Add PAG_SLP3 support
|                       Ci      Resolved Metrowerks compiler warnings C1020 and C4200
|                       AMN     Removed the redefinition of CallFblStart in case of Ford/SLP5
|                       FHe     Added CanInitTable support for MAC710X
| 2006-04-11  01.67.00  Ls      Minor changes for PAG_SLP3
|                       ACP     ESCAN0015874: updated memory qualifiers to V_MEMROMx
|                                in "extern const tFlashEraseTable FlashEraseTable"
|                       Ci      Added section definitions for MTAB and magic
|                                flags with C_COMP_NEC_V85X
|                       Rr      Added missing _COMMENT for C_COMP_xx_470
|                       ACP     Updated MEMORY_ROM to use V_MEMROMx for Ford GGDS section
|                                in "extern MEMORY_ROM tFblHeader FblHeader"
| 2006-05-03  01.68.00  BJE     ESCAN00016212: updated 78k0 memory type definition for mtab
| 2006-05-03  01.69.00  ACP     ESCAN00016400: updated FBL_ADDR/MEMSIZE_TYPE for PIC18
|                               ESCAN00016438:  Added kStartupPreInit/PostInit definition
|                                for new ApplFblStartup() parameter
| 2006-06-12  01.70.00  FHe     Added blockIndex for PAG SLP3 to logical block table
|                               Added flags for response after reset for PAG SLP3
|                               Added support for Vector SLP3 and VAG SLP2
| 2006-07-27  01.71.00  Rr      ESCAN00016815: Added support for C_PROCESSOR_FX3
|                       Qps     Added section definition for fblStartMagicFlag for
|                                C_COMP_GHS_V85X
|                       Hp      ESCAN00017109: Added support for Avccore
| 2006-08-22  01.72.00  Ci      Added support for C_COMP_NEC_V85X
|                       Hp      ESCAN00017238: Add support for GM SLP3
|                       FHe     Added GM specific section to tSegmentInfo structure
|                       QPs     Changed fblStartMagicFlag pragma for C_COMP_IAR_12
| 2006-08-25  01.73.00  Hp      Use common definition of tSegmentInfo for GM-SLP3
|                       WM      ESCAN00017455: Changed prototypes of FblStackInit
|                                and ApplFblStart
|                       QPs     ESCAN00017629: Added support for 16FX - OEM Renault
|                       swk     ESCAN00017711: Added support for KEIL and ST10F276x
|                       WM      ESCAN00017714: Additional entries in FBL header
|                               t able for FBL_ENABLE_META_DATA_ROM
|                       swk     ESCAN00017729: Added support for C_COMP_MTRWRKS_MCS08_MSCAN_COMMENT
| 2006-10-04  01.74.00  swk     ESCAN00017851: New tBlockDescriptor and tLogicalBlockTable
|                                definition for GM SLP3. Add several macros.
|                       Hp      ESCAN00017919: Provide optionally include-file for memory mapping
| 2006-12-18  01.75.00  FHe     Added support for V850 PH derivatives
|                       QPs     ESCAN00018021: Changed fblStartMagicFlag pragma definition
|                                 for C_COMP_GHS_V85X
|                       CB      ESCAN00018080: New OEM
|                       Hp      ESCAN00018246: Add support for new OEM HMC
|                       Hp      ESCAN00018248: Introduction of FBL assertions.
|                       Ci      ESCAN00018357: Corrected erroneous ifdef statement
|                       swk     ESCAN00018460: Warning supression fpr R8C and Metrowerks compiler
|                       Ci      ESCAN00018687: Add new assertion for missing forced RCRRP
|                       CB      ESCAN00018080: Minor changes
| 2007-02-07 01.76.00   Hp      ESCAN00018849: Remove CAN-specific defines
|                       Awh     Added Dx3 define for bustiming registers in tCanInitTable
|                       QPs     ESCAN00018849: Minor changes
|                       Hp      Improvements for MISRA compliance.
|                               ESCAN00019414:  GetFblXXXVersion() macro cannot be used for some OEMs
| 2007-02-21 01.77.00   FHe     Added support for C_COMP_VDSP_BF5XX
| 2007-06-29 01.78.00   Pal     ESCAN00019752: Support for C_COMP_GHS_V85X
|                       Ls      Added support for 16FX_CCAN (tCanInitTable)
|                       FHe     ESCAN00020931: VAG SLP2 renamed to SLP3
|                       Rr      ESCAN00019752: Harmonized C_COMP_GHS_V85X section names, where available
|                       JHg     Add support for FBL_ENABLE_ENCRYPTION_MODE to PAG SLP3 and VAG SLP3
|                       Kak     ESCAN00021246: Removed MEMORY_HUGE qualifier from FblStartFct for IAR/V850
| 2007-07-31 01.79.00   Rr      ESCAN00021415: Added support for Mcf Coldfire
|                       Kak     ESCAN00021678: Added GM_SLP2 to #if to enable memmap for fblStartMagicFlag
|                       Swk     ESCAN00021682: Added support for PIC18(tCanInitTable)
| 2007-08-03 01.80.00   Hp      ESCAN00021728: Separate Assertion defines in Fbl_Def into system and OEM-group
|                       Swk     ESCAN00021733: Added support for C_COMP_MICROCHIP_PIC18 new define of FblHeaderTable
| 2007-08-19 01.81.00   FHe     ESCAN00021732: Modernization Steps for Vector SLP1 (14230)
|                       JHg     ESCAN00021868: Use V850 FCAN derivatives to distinguish between
|                                FCAN and aFCAN specific options in tCanInitTable
|                       Fz      ESCAN00021812: Extern for CanInitTable added
| 2007-10-10 01.82.00   FHe     ESCAN00013178: FBL_ENABLE_ROMMASK_FLAG added (PAG UDS)
|                       Fz      ESCAN00022365: Macro GetFblSWMI causes compiler error
|                       Ci      ESCAN00022471: Added support for GNU compiler
|*****************************************************************************/
#ifndef FBL_DEF_H
#define FBL_DEF_H

/* Includes ******************************************************************/
/* Basic configurations */
#include "v_cfg.h"

#if defined( VGEN_GENY ) && !defined( VGEN_ENABLE_CANFBL )
/* this file was obviously not included in FBL, so it's used in user application
   check if MAGIC_NUMBER was generated. In this case we have to remove the check for the
   following include because FBL generation use different number than application */
# if defined( MAGIC_NUMBER )
#  undef MAGIC_NUMBER
# endif
#endif
/* Configuration file for flash boot loader */
#include "Fbl_Cfg.h"
#if defined( VGEN_GENY ) && !defined( VGEN_ENABLE_CANFBL )
/* the last include redefine MAGIC_NUMBER, which is not relevant for application so
   invalidate it */
# if defined( MAGIC_NUMBER )
#  undef MAGIC_NUMBER
# endif
#endif

/* Basic type definitions */
#include "v_def.h"

/* Defines *******************************************************************/
/* --- Version --- */
/* ##V_CFG_MANAGEMENT ##CQProject : FblDef CQComponent : Implementation */
#define FBLDEF_VERSION          0x0182u
#define FBLDEF_RELEASE_VERSION  0x00u

#ifndef NULL
# define NULL ((void *)0)
#endif

# define assertFbl(p, e)
# define assertFblUser(p, e)
# define assertFblGen(p, e)
# define assertFblInternal(p, e)

/*-- End of assertion definition --------------------------------------------*/


#ifndef SWM_DATA_ALIGN
# define SWM_DATA_ALIGN 0
#endif

/* CanTransmit return values */
#ifndef kCanTxOk
# define kCanTxOk             0     /* Msg transmitted                        */
#endif
#ifndef kCanTxFailed
# define kCanTxFailed         1     /* Tx path switched off                   */
#endif
#define kCanTxInProcess       2

/* Define return code of several functions   */
#define kFblOk                0
#define kFblFailed            1

/* Parameters for ApplFblStartup() */
#define kStartupPreInit       0
#define kStartupPostInit      1

/* Programming request flag */
#define kProgRequest         0x01u
#define kNoProgRequest       (vuint8)(( ~kProgRequest) & 0xffu)

/* Application validation  */
#define kApplValid            1     /* Application is fully programmed */
#define kApplInvalid          0     /* Operational software is missing */

#define kNrOfValidationBytes  2


/* Define to access the FBL header structure */
#define FblHeaderTable ((tFblHeader MEMORY_HUGE MEMORY_FAR *)(FBL_HEADER_ADDRESS))

/* Position of boot block: */
/* FBL_TOP_BOOT_BLOCK: The boot block is mapped to the high addresses */
/* FBL_BOTTOM_BOOT_BLOCK: The boot block is mapped to the low addresses */
#define FBL_TOP_BOOT_BLOCK

#if !defined(FBL_REPEAT_CALL_CYCLE)
/* Set default to 1ms for repeat time of main loop */
# define FBL_REPEAT_CALL_CYCLE 1
#endif

#ifdef FBL_ENABLE_BANKING
# define FBL_CALL_TYPE V_CALLBACK_FAR
#else
# define FBL_CALL_TYPE
#endif /* FBL_ENABLE_BANKING */

#ifndef V_CALLBACK_NEAR
# define V_CALLBACK_NEAR
#endif
#ifndef V_API_NEAR
# define V_API_NEAR
#endif


#if defined( FBL_ENABLE_FBL_START )
/* Define any strange pattern for magic flags used for reprogramming indication */
# define kFblStartMagicByte0   0xA5u
# define kFblStartMagicByte1   0xC3u
# define kFblStartMagicByte2   0x9Bu
# define kFblStartMagicByte3   0xE7u

/* Macros that can be used within the application to access the magic flags */
/* It must be ensured, that the data location of FBL-magic does not conflict with applicaton */
# define ApplSetFblStartMagicFlag()\
{\
   FblHeaderTable->pFblMagicFlags[0] = kFblStartMagicByte0;\
   FblHeaderTable->pFblMagicFlags[1] = kFblStartMagicByte1;\
   FblHeaderTable->pFblMagicFlags[2] = kFblStartMagicByte2;\
   FblHeaderTable->pFblMagicFlags[3] = kFblStartMagicByte3;\
}

# define FblSetFblStartMagicFlag()\
{\
   fblStartMagicFlag[0] = kFblStartMagicByte0;\
   fblStartMagicFlag[1] = kFblStartMagicByte1;\
   fblStartMagicFlag[2] = kFblStartMagicByte2;\
   fblStartMagicFlag[3] = kFblStartMagicByte3;\
}

# define FblChkFblStartMagicFlag()\
   ((    (fblStartMagicFlag[0] == kFblStartMagicByte0)\
      && (fblStartMagicFlag[1] == kFblStartMagicByte1)\
      && (fblStartMagicFlag[2] == kFblStartMagicByte2)\
      && (fblStartMagicFlag[3] == kFblStartMagicByte3))?1:0)

# define FblClrFblStartMagicFlag()\
{\
   fblStartMagicFlag[0] = 0x00;\
   fblStartMagicFlag[1] = 0x00;\
   fblStartMagicFlag[2] = 0x00;\
   fblStartMagicFlag[3] = 0x00;\
}

/** Defines to convert BigEndian bytes into short or long values ********************/
# ifdef C_CPUTYPE_BIGENDIAN
#  ifdef C_CPUTYPE_32BIT
#   define FblBytesToShort(hi,lo)            (((vuint16)(hi) << 8) | ((vuint16)(lo) ))
#  else
#   define FblBytesToShort(hi,lo)            (vuint16)*(V_MEMRAM0 V_MEMRAM1_FAR vuint16 V_MEMRAM2_FAR *)(&(hi))
#  endif
# endif
# ifdef C_CPUTYPE_LITTLEENDIAN
#  define FblBytesToShort(hi,lo)              (((vuint16)(hi) << 8) | ((vuint16)(lo) ))
# endif

# ifdef C_CPUTYPE_BIGENDIAN
#  ifdef C_CPUTYPE_32BIT
#   define FblBytesToLong(hiWrd_hiByt,hiWrd_loByt,loWrd_hiByt,loWrd_loByt)  \
                                         (((vuint32)(hiWrd_hiByt) << 24) |  \
                                          ((vuint32)(hiWrd_loByt) << 16) |  \
                                          ((vuint32)(loWrd_hiByt) <<  8) |  \
                                          ((vuint32)(loWrd_loByt)      )  )
#  else
#   define FblBytesToLong(hiWrd_hiByt,hiWrd_loByt,loWrd_hiByt, loWrd_loByt)  \
            (vuint32)*(V_MEMRAM0 V_MEMRAM1_FAR vuint32 V_MEMRAM2_FAR *)(&(hiWrd_hiByt))
#  endif
# endif
# ifdef C_CPUTYPE_LITTLEENDIAN
#  define FblBytesToLong(hiWrd_hiByt,hiWrd_loByt,loWrd_hiByt,loWrd_loByt)  \
                                        (((vuint32)(hiWrd_hiByt) << 24) |  \
                                         ((vuint32)(hiWrd_loByt) << 16) |  \
                                         ((vuint32)(loWrd_hiByt) <<  8) |  \
                                         ((vuint32)(loWrd_loByt)      )  )
# endif




#endif   /* #if defined( FBL_ENABLE_FBL_START ) */

#if 0
#define BIT0   0x01u
#define BIT1   0x02u
#define BIT2   0x04u
#define BIT3   0x08u
#define BIT4   0x10u
#define BIT5   0x20u
#define BIT6   0x40u
#define BIT7   0x80u
#else
#define BITs0   0x01u
#define BITs1   0x02u
#define BITs2   0x04u
#define BITs3   0x08u
#define BITs4   0x10u
#define BITs5   0x20u
#define BITs6   0x40u
#define BITs7   0x80u

#endif

/* Macros and values for fblMode */
#define START_FROM_APPL     BITs0
#define START_FROM_RESET    BITs1
#define APPL_CORRUPT        BITs2
#define STAY_IN_FLASHER     BITs3
#define FBL_START_WITH_RESP BITs6
#define FBL_START_WITH_PING BITs7

#define SetFblMode(state)   (fblMode = (fblMode & 0xF0u) | (state))
#define GetFblMode()        (fblMode & 0x0Fu)


/* Access macros for FblHeader elements for application */
#define GetFblMainVersion()    (FblHeaderTable->kFblMainVersion)
#define GetFblSubVersion()     (FblHeaderTable->kFblSubVersion)
#define GetFblReleaseVersion() (FblHeaderTable->kFblBugFixVersion)
#define  GetFblDCID0()         (FblHeaderTable->DCID[0])
#define  GetFblDCID1()         (FblHeaderTable->DCID[1])
#define  GetFblSWMI(i)         (FblHeaderTable->SWMI[(i)])
#define  GetFblSWMI0()         (FblHeaderTable->SWMI[0])
#define  GetFblSWMI1()         (FblHeaderTable->SWMI[1])
#define  GetFblSWMI2()         (FblHeaderTable->SWMI[2])
#define  GetFblSWMI3()         (FblHeaderTable->SWMI[3])
#define  GetFblDLS0()          (FblHeaderTable->DLS[0])
#define  GetFblDLS1()          (FblHeaderTable->DLS[1])
#define  GetFblStartMagic()    (FblHeaderTable->pFblMagicFlags)
#define  GetFblCanInitTable()  (FblHeaderTable->pCanInitTable)

#define FBL_START_PARAM       ((void *)0)
#define CallFblStart(pParam)  (FblHeaderTable->FblStartFct)((pParam))





/* Unless otherwise specified, STAY_IN_BOOT mode is disabled. */
#if !defined( FBL_ENABLE_STAY_IN_BOOT ) && !defined( FBL_DISABLE_STAY_IN_BOOT )
# define FBL_DISABLE_STAY_IN_BOOT
#endif

/* Field sizes in GM's File Header */
#define SWM_CS_MAX_LEN     ((vuint8)2)    /* Checksum field length */
#define SWM_MID_MAX_LEN    ((vuint8)2)    /* Module-ID field length */
#define SWM_HFI_MAX_LEN    ((vuint8)2)    /* Header-Format-Identifier field length */
#define SWM_SWMI_MAX_LEN   ((vuint8)16)   /* Software-Module-Identifier field length */
#define SWM_DLS_MAX_LEN    ((vuint8)3)    /* Design-Level-Suffex (Alpha Code) field length */
#define SWM_PN_MAX_LEN     ((vuint8)16)   /* Part-Number field length */
#define SWM_DCID_MAX_LEN   ((vuint8)2)    /* Data-Compatibility-Identifier field length */
#define SWM_NOAR_MAX_LEN   ((vuint8)2)    /* Number-Of-Address-Regions field length */
#define SWM_NOAM_MAX_LEN   ((vuint8)2)    /* Number-Of-Additional-Modules field length */
#define SWM_PMA_MAX_LEN    ((vuint8)4)    /* Product-Memory-Address field length */
#define SWM_NOB_MAX_LEN    ((vuint8)4)    /* Number-Of-Bytes field length */

#define SWM_SWMI_DFLT_LEN  ((vuint8)4)    /* Required SWMI field length for "Global Bootloader Specification" */


/* Types *********************************************************************/
typedef vuint8 tFblResult;                    /* FBL result codes */
typedef vuint8 tFblProgStatus;                /* Status of reprogramming flag */
typedef vuint8 tApplStatus;                   /* Application valid status */
typedef vuint8 tMagicFlag;                    /* Application valid flag */

#ifdef C_CPUTYPE_8BIT
typedef vuint16 FBL_ADDR_TYPE;
typedef vuint16 FBL_MEMSIZE_TYPE;
# define MEMSIZE_OK
#endif /* C_CPUTYPE_8BIT */
#ifdef C_CPUTYPE_16BIT
typedef vuint32 FBL_ADDR_TYPE;
typedef vuint32 FBL_MEMSIZE_TYPE;
# define MEMSIZE_OK
#endif /* C_CPUTYPE_16BIT */
#ifdef C_CPUTYPE_32BIT
typedef vuint32 FBL_ADDR_TYPE;
typedef vuint32 FBL_MEMSIZE_TYPE;
# define MEMSIZE_OK
#endif /* C_CPUTYPE_32BIT */

#ifdef MEMSIZE_OK
#else
# error "Error in FBL_DEF.H: C_CPUTYPE_ not defined."
#endif


/* Defines *******************************************************************/
#define kFblDiagTimeP2        FBL_DIAG_TIME_P2MAX
#define kFblDiagTimeP2Star    FBL_DIAG_TIME_P3MAX


/* Function pointer for realtime support */
typedef FBL_CALL_TYPE vuint8 (*tFblRealtimeFct)(void);

/* Function pointer for read memory */
typedef FBL_CALL_TYPE FBL_MEMSIZE_TYPE (*tReadMemoryFct)(FBL_ADDR_TYPE address, vuint8 *buffer, FBL_MEMSIZE_TYPE length);


#if defined (FBL_ENABLE_ENCRYPTION_MODE)
typedef struct tDecParam
{
   vuint8*        dataBuffer;
   vuint16        dataLength;
   vuint8         (* V_API_NEAR wdTriggerFct)(void);
}tDecParam;
#endif

/* Structure for address and length information of segments */
typedef struct tSegmentInfo
{
   FBL_ADDR_TYPE     targetAddress;
   FBL_MEMSIZE_TYPE  length;
} tSegmentInfo;

/* Segment data structure */
typedef struct tSegmentList
{
   vuint8         nrOfSegments;
   tSegmentInfo   segmentInfo[SWM_DATA_MAX_NOAR];
} tSegmentList;


typedef vuint8  *tCRCValue;

typedef struct tCRCParam
{
   tSegmentList        *segmentInfo;
   tCRCValue            crcVerify;
   tFblRealtimeFct      triggerFct;
   tReadMemoryFct       readMemory;
} tCRCParam;

typedef vuint16 tChecksum;


/*****************************************************************************/
/* Increment this version when changing the structure */
/* "tCanInitTable". This provides version control for */
/* the function FblStackInit() in FBL_HW.C.           */
#define FBL_CAN_INIT_TABLE_VERSION  0x0101

/* CAN initialization data sturcture */
/* Be sure to adjust the StackInit-Routine in the HW-File to changes! */
typedef struct  tCanInitTable
{
   vuint8 TpRxIdHigh;       /* CAN receive identifier */
   vuint8 TpRxIdLow;
   vuint8 TpTxIdHigh;       /* CAN transmit identifer */
   vuint8 TpTxIdLow;
   vuint8 CanInitCMCR0;   /* Module configuration register */
   vuint8 CanInitCMCR1;
   vuint8 CanInitCBTR0;   /* CAN bus timing register */
   vuint8 CanInitCBTR1;
   vuint8 ProgrammedState;             /* Mode byte for ReportProgrammedState service      */
   vuint8 ErrorCode;                   /* This byte determines SPS_TYPE_B or SPS_TYPE_C    */
   vuint8 RequestProgrammingMode;      /* Highspeed programming or normal mode             */
   vuint8 requestDownloadFormatID;     /* Subparameter of Request download                 */
   vuint32 requestDownloadMemorySize;  /* unCompressedMemorySize param of Request Download */
   MEMORY_HUGE MEMORY_FAR void (*PlaceHolder_func)(void);/* need not to be initialized. For internal use only */
}tCanInitTable;


/* SWM data structure. This is an export for the application            */
/* to get access to the software module information stored in Flash.    */
/* Since FBL uses the same data struct internally -> Do not change !!!  */

typedef struct tFblHeader
{
   vuint8    CS[2];      /* Module Checksum              */
   vuint8    MID[2];     /* Module ID                    */
   vuint8    HFI[2];     /* Header Format Identifier     */
   vuint8    SWMI[4];    /* SoftWare Module Identifier   */
   vuint8    DLS[2];     /* Design Level Suffix          */
   vuint8    DCID[2];    /* Data Compatiblity Identifier */
   vuint8    NOAR[2];    /* Number Of Address Regions    */
                         /* Product Memory Address + Number Of Bytes */
   vuint8    AddressRegions[FBL_GMHEADER_NOAR_FBL * 8];
   MEMORY_HUGE MEMORY_FAR void (*FblStartFct)( MEMORY_HUGE MEMORY_FAR tCanInitTable *CanInitTable );
   vuint8    kFblMainVersion;
   vuint8    kFblSubVersion;
   vuint8    kFblBugFixVersion;
   vuint8    kFblHeaderAlign1;      /* Alignment to even addresses      */
   V_MEMRAM0 V_MEMRAM1_FAR vuint8        V_MEMRAM2_FAR *pFblMagicFlags;
   V_MEMRAM0 V_MEMRAM1_FAR tCanInitTable V_MEMRAM2_FAR *pCanInitTable;

} tFblHeader;


#define SwmDataTable ((MEMORY_HUGE MEMORY_FAR tSwmDataTable *)(SWM_DATA_ADDR))



/* Global data ***************************************************************/
extern MEMORY_NEAR vuint8 fblMode;
#define FBLHEADER_START_SEC_CONST_EXPORT
#include "Fbl_MemMap.h"
V_MEMROM0 extern V_MEMROM1 tFblHeader V_MEMROM2 FblHeader;
#define FBLHEADER_STOP_SEC_CONST_EXPORT
#include "Fbl_MemMap.h"

#if defined( FBL_ENABLE_FBL_START )
void FblStart(tCanInitTable *pCanInitTable);
#endif /* FBL_ENABLE_FBL_START */

/* definition of array constants. Can be overwritten by user-defines *********/
#ifndef SWM_DATA_MAX_NOAR
# define SWM_DATA_MAX_NOAR    10
#endif
#ifndef SWM_DATA_MAX_NOAM
# define SWM_DATA_MAX_NOAM    19
#endif

/* definition used in FBL_MTAB for sector description  ***********************/
#define FBL_MTAB_MAGIC_FLAG      0xB5u

#define SWMI_TYPE           0x01u           /* This section includes SWMI Data   */
#define DATA_TYPE           0x02u           /* This section includes Data        */
#define MANDATORY_TYPE      0x80u           /* Section is mandatory              */
#define MANDATORY_MASK_OUT  0x0Fu           /* Mask to mask out mandatory flag   */

/* Length of the presence pattern. Two bytes are used. */
#define kPresPtnLen          2

/* define the 2-byte default presence pattern ********************************/
/* For application and calibration */
#define kPresPtnApp1         0xA5u
#define kPresPtnApp2         0x96u


/* General presence pattern, if CAL and Appl-PresPtn identical (use the ApplPresPtn)  */
#define kPresencePattern1    kPresPtnApp1
#define kPresencePattern2    kPresPtnApp2

#define kPresencePattern    (((vuint16)kPresPtnApp1<<8)|(vuint16)(kPresPtnApp2&0x00FFu))

#ifdef FBL_MTAB_NO_OF_SECTIONS
#else
/* Within the FBL, the size of the logical block table is determinde */
/* by the size value in the logical block table.                     */
# define FBL_MTAB_NO_OF_SECTIONS 1
#endif

typedef struct tBlockDescriptor
{
   vuint8            moduleId;            /* Module ID of the download file */
   vuint8            memDeviceIdx;        /* Index to the device driver when using multiple devices */
   vuint8            flashSegmentSize;
   vuint8            flashDeleted;
   FBL_ADDR_TYPE     blockStartAddress;   /* base address of the block to be erased  */
   FBL_MEMSIZE_TYPE  blockLength;         /* Length of the block to be erased        */
} tBlockDescriptor;

typedef struct tLogicalBlockTable
{
   vuint8    magicFlag;          /* value of FBL_FLASH_ERASE_TABLE_EXIST_CODE */
                                 /* indicates the existance of the table      */
   vuint8    noOfBlocks;         /* MUST correspond to the number of entries  */
                                 /* of the next table                         */

   tBlockDescriptor LogicalBlocks[FBL_MTAB_NO_OF_BLOCKS];
                                 /* This is table  erase sections, defining   */
                                 /* the base-address and the length of each   */
                                 /* module that could be erased with service  */
                                 /* B1 00 B2 xx (Sector erase)                */
   vuint8    magicFlagEnd;       /* value of inverted FBL_FLASH_ERASE_TABLE_EXIST_CODE */
                                 /* indicates the existance of the total length table  */
} tLogicalBlockTable;


/////////////////////////////////////// skj //////////////////////////////////////////////////////////////
//! A LogicalBlock_t struct
typedef struct
{
    vuint32   u32StartAddr;     //!< 32bit Start Address
    vuint32   u32Size;          //!< 32Bit block size
}LogicalBlock_t;


//! A  LogicalBlockTable_t struct for Description of Application Memory Map
typedef struct
{
    vuint8       u8MagicNumber;      //!< Magic Number for checking the Logical Block Table consistency
    vuint8       u8LbtMode;          //!< Calibration Module Number
    vuint8       u8ProductID;        //!< ECU type, refer to BootParameters.h file.
    vuint8       u8NegMagicNumber;       //!< Revision Information
    LogicalBlock_t Module[10]; //!< Application SW Block Area
}LogicalBlockTable_t;

typedef enum
{
    LBT_MODE_APP_ONLY = 0,
    LBT_MODE_APP_CAL1,
    LBT_MODE_APP_CAL2,
    LBT_MODE_APP_CAL3,
    LBT_MODE_APP_CAL4,
    LBT_MODE_APP_CAL5,
    LBT_MODE_APP_CAL6
}LbtModeType_t;

typedef enum
{
    LBT_MODULEID_APP = 0,
    LBT_MODULEID_CAL1,
    LBT_MODULEID_CAL2,
    LBT_MODULEID_CAL3,
    LBT_MODULEID_CAL4,
    LBT_MODULEID_CAL5,
    LBT_MODULEID_CAL6,
    LBT_MODULEID_BOOT = 0x80,
    LBT_MODULEID_KERNEL
}LbtModuleTypes_t;
/////////////////////////////////////// skj //////////////////////////////////////////////////////////////




#define LBT_START_SEC_CODE_EXPORT
#include "Fbl_MemMap.h"
/* Definition of Memory Regions */

V_MEMROM0 extern V_MEMROM1 tLogicalBlockTable V_MEMROM2 FblLogicalBlockTable;

#define LBT_STOP_SEC_CODE_EXPORT
#include "Fbl_MemMap.h"





 /* Prototypes ****************************************************************/

#if defined(FBL_ENABLE_FBL_START)
# define MAGICFLAG_START_SEC_DATA_EXPORT
# include "Fbl_MemMap.h"
 /* Example implementation on entering bootloader with 4 byte special ('FBLSTARTMAGIC') values */
 extern vuint8 fblStartMagicFlag[4];
# define MAGICFLAG_STOP_SEC_DATA_EXPORT
# include "Fbl_MemMap.h"


# define CANPARAM_START_SEC_DATA_EXPORT
# include "Fbl_MemMap.h"
 extern tCanInitTable CanInitTable;
# define CANPARAM_STOP_SEC_DATA_EXPORT
# include "Fbl_MemMap.h"
#endif /* FBL_ENABLE_FBL_START */





/* Value of parameter 'RequestProgrammingMode'  */
/* Defines the service request type used to     */
/* jump into bootloader.                        */
#define REQUEST_PROGRAMMING_MODE_LOWSPEED    0x01
#define REQUEST_PROGRAMMING_MODE_HIGHSPEED   0x02
#define REQUEST_DOWNLOAD_MODE_LOWSPEED       0x03
#define REQUEST_DOWNLOAD_MODE_HIGHSPEED      0x04

/* Subfunction of Programming Mode service (0xA5) */
#define ENABLE_PROGRAMMING_MODE 0x03





#endif /* #ifndef FBL_DEF_H */

/* End of file *************************************************************/
/************   Organi, Version 3.6.9 Vector-Informatik GmbH  ************/
