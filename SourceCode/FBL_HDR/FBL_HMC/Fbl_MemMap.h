/****************************************************************************
| Project Name: Flash Bootloader
|    File Name: Memmap_ext.h
|
|  Description: Section definition to locate data or code in memory
|               This file contains compiler specific section definition
|               for every segment that can be mapped within the bootloader
|               to a specific location.
|
|-----------------------------------------------------------------
|               C O P Y R I G H T
|-----------------------------------------------------------------
| Copyright (c) 2006 by Vector Informatik GmbH, all rights reserved
|
| Please note, that this file contains a collection of definitions
| to be used with the Flash Bootloader. These functions may
| influence the behaviour of the bootloader in principle. Therefore,
| great care must be taken to verify the correctness of the
| implementation.
|
| The contents of the originally delivered files are only examples resp.
| implementation proposals. With regard to the fact that these definitions
| are meant for demonstration purposes only, Vector Informatik�s
| liability shall be expressly excluded in cases of ordinary negligence,
| to the extent admissible by law or statute.
|-----------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-----------------------------------------------------------------
| Initials      Name                   Company
| --------      --------------------   ---------------------------
|  AmN           Ahmad Nasser          Vector CANtech, Inc.
|  CB            Christian Baeuerle    Vector Informatik GmbH
|  FHe           Florian Hees          Vector Informatik GmbH
|  HP            Armin Happel          Vector Informatik GmbH
|  Ls            Konrad Lazarus        Vector Informatik GmbH
|  MFR           Michael F. Radwick    Vector CANtech, Inc.
|  QPs           Quetty Palacios       Vector Informatik GmbH
|  Rr            Robert Schaeffner     Vector Informatik GmbH
|  SWk           Slawomir Wilk         Vector Informatik GmbH
|  AWh           Andreas Wenckebach    Vector Informatik GmbH
|-----------------------------------------------------------------
|               R E V I S I O N   H I S T O R Y
|-----------------------------------------------------------------
| Date          Ver    Author   Description
| ---------   -------  ------   ------------------------------------
| 2006-10-05  1.00.00    Hp     Creation.
| 2007-10-30  1.01.00    AWh    Adopted MagicFlag section name from fbl_def.h
|*****************************************************************************/

#ifndef  _MEM_INIT_H
#define  _MEM_INIT_H
//#include "../../VIL/_Par/car/Car.par.h"
#endif

/*-- Locate block descriptor (LBT) ----------*/
#if defined(LBT_START_SEC_CODE_EXPORT) || \
    defined(LBT_START_SEC_CODE)
#undef LBT_START_SEC_CODE
#undef LBT_START_SEC_CODE_EXPORT
/* Insert memory section definition here   */
    #pragma section farrom "app_user_lbt"   //E0923 Taeho: app_user_lbt 
/* if a re-programmable LBT shall be used. */
#endif

#if defined(LBT_STOP_SEC_CODE_EXPORT) || \
    defined(LBT_STOP_SEC_CODE)
#undef LBT_STOP_SEC_CODE
#undef LBT_STOP_SEC_CODE_EXPORT
/* Insert memory section definition here   */
    #pragma section farrom //E0923 Taeho: app_user_lbt  
/* if a re-programmable LBT shall be used. */
#endif


/*-- Locate NEAR data in FBL_DIAG ----------*/
#if defined(FBLDIAG_START_SEC_NEARDATA_EXPORT) || \
    defined(FBLDIAG_START_SEC_NEARDATA)
#undef FBLDIAG_START_SEC_NEARDATA
#undef FBLDIAG_START_SEC_NEARDATA_EXPORT
/* Insert memory section definition here   */
#endif

#if defined(FBLDIAG_STOP_SEC_NEARDATA_EXPORT) || \
    defined(FBLDIAG_STOP_SEC_NEARDATA)
#undef FBLDIAG_STOP_SEC_NEARDATA
#undef FBLDIAG_STOP_SEC_NEARDATA_EXPORT
/* Insert memory section definition here   */
#endif


/*-- Locate DiagBuffer to a specific RAM-location if necessary --*/
#if defined(DIAGBUFFER_START_SEC_DATA) || \
    defined(DIAGBUFFER_START_SEC_DATA_EXPORT)
#undef DIAGBUFFER_START_SEC_DATA
#undef DIAGBUFFER_START_SEC_DATA_EXPORT
/* Insert memory section definition here   */
#endif

#if defined(DIAGBUFFER_STOP_SEC_DATA) || \
    defined(DIAGBUFFER_STOP_SEC_DATA_EXPORT)
#undef DIAGBUFFER_STOP_SEC_DATA
#undef DIAGBUFFER_STOP_SEC_DATA_EXPORT
/* Insert memory section definition here   */
#endif


/*---- Locate CanInitTable ----*/

/* ------------------------------------------------------ */
/* ------------------------------------------------------ */
/* !!! NOTE: The CanInitTable can be mapped to the same   */
/*           location in application and Boot. Thus, data */
/*           don't need to be copied when transit from    */
/*           application to the boot.                     */
/*           Locate this section into uninitialized RAM.  */
/* ------------------------------------------------------ */
/* ------------------------------------------------------ */
#if defined(CANPARAM_START_SEC_DATA) || \
    defined(CANPARAM_START_SEC_DATA_EXPORT)
#undef CANPARAM_START_SEC_DATA
#undef CANPARAM_START_SEC_DATA_EXPORT
/* Insert memory section definition here   */

#endif

#if defined(CANPARAM_STOP_SEC_DATA) || \
    defined(CANPARAM_STOP_SEC_DATA_EXPORT)
#undef CANPARAM_STOP_SEC_DATA
#undef CANPARAM_STOP_SEC_DATA_EXPORT
/* Insert memory section definition here   */

#endif

/*---- Locate fblStartMagicFlag ----*/

/* ------------------------------------------------------ */
/* ------------------------------------------------------ */
/* !!! NOTE: This must be located in uninitialized RAM!!! */
/* ------------------------------------------------------ */
/* ------------------------------------------------------ */
#if defined(MAGICFLAG_START_SEC_DATA) || \
    defined(MAGICFLAG_START_SEC_DATA_EXPORT)
#undef MAGICFLAG_START_SEC_DATA
#undef MAGICFLAG_START_SEC_DATA_EXPORT
/* Insert memory section definition here   */

#endif

#if defined(MAGICFLAG_STOP_SEC_DATA) || \
    defined(MAGICFLAG_STOP_SEC_DATA_EXPORT)
#undef MAGICFLAG_STOP_SEC_DATA
#undef MAGICFLAG_STOP_SEC_DATA_EXPORT
/* Insert memory section definition here   */

#endif

/*---- Locate FblHeader ----*/
#if defined(FBLHEADER_START_SEC_CONST) || \
    defined(FBLHEADER_START_SEC_CONST_EXPORT)
#undef FBLHEADER_START_SEC_CONST
#undef FBLHEADER_START_SEC_CONST_EXPORT
/* Insert memory section definition here   */
    #pragma section farrom "app_header"     //E0923 Taeho: ���� ����
#endif

#if defined(FBLHEADER_STOP_SEC_CONST) || \
    defined(FBLHEADER_STOP_SEC_CONST_EXPORT)
#undef FBLHEADER_STOP_SEC_CONST
#undef FBLHEADER_STOP_SEC_CONST_EXPORT
/* Insert memory section definition here   */
    #pragma section farrom  //E0923 Taeho: appl_header section ����
#endif




/*-- Calibration Set 0  -----------------------------------------------------*/
#if defined(CALIBRATION_SET0_START)
    #undef CALIBRATION_SET0_START
    /* Insert memory section definition here   */
    #pragma section farrom "Cal0_header"
#endif
#if defined(CALIBRATION_SET0_STOP)
    #undef CALIBRATION_SET0_STOP
    /* Insert memory section definition here   */
    #pragma section farrom 
#endif

/************   Organi, Version 3.6.8 Vector-Informatik GmbH  ************/
