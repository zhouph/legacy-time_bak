/**************************************************************
*  File D:\NewGMHandler\Generation\GenData_V23\T300_ESC\v_cfg.h
*  generated at Wed Jun 11 12:19:05 2008
*             Toolversion:   432
*               Bussystem:   CAN
*
*  generated out of CANdb:   D:\NewGMHandler\DBC_V2.3\T300_ESC_HS_v2.3.DBC
*                            D:\NewGMHandler\DBC_V2.3\T300_ESC_CE_v2.3.DBC

*            Manufacturer:   General Motors
*                for node:   EBCM_HS
*   Generation parameters:   Target system = MCS12X_MSCAN
*                            Compiler      = Metrowerks
*
* License information:
*   -    Serialnumber:       CBD0700200
*   - Date of license:       26.11.2007
*
***************************************************************
Software is licensed for:
Mando Corp.
GM / GMLAN 3.0 / Freescale MC9S12X / Metrowerks 4.6 / MC9S12XEP100
**************************************************************/
#ifndef VECTOR_CFG_H
#define VECTOR_CFG_H



#define VGEN_CANGEN_VERSION 0x0432
#define VGEN_CANGEN_RELEASE_VERSION 0x22
#define CANGENEXE_VERSION 0x0432
#define CANGENEXE_RELEASE_VERSION 0x22

#define C_CLIENT_GM


//#define C_COMP_MTRWRKS_MCS12X_MSCAN12
//#define C_PROCESSOR_MCS12X_XX
#define C_ENABLE_CAN_CHANNELS
#define C_VERSION_REF_IMPLEMENTATION 0x140
#define V_ENABLE_USE_DUMMY_STATEMENT
#define VGEN_ENABLE_VSTDLIB
#define C_CPUTYPE_16BIT
#define C_CPUTYPE_BIGENDIAN
#define C_CPUTYPE_BITORDER_LSB2MSB

/**************************************
*    Version defines for Ini Files    *
***************************************/

#define VGEN_GM_INI_VERSION 0x0100
#define VGEN_GM_INI_RELEASE_VERSION 0x00

/**************************************
*           used modules              *
***************************************/

#define VGEN_DISABLE_BRS
#define VGEN_ENABLE_CAN_DRV
#define VGEN_ENABLE_IL_VECTOR

/* NM Modules */

#define VGEN_ENABLE_NM_GMLAN

/* TP Modules */

#define VGEN_ENABLE_TP_ISO_MC

/* Diagnose Modules */

#define VGEN_ENABLE_DIAG_CANDESC
#define VGEN_ENABLE_CCL

#define VGEN_DISABLE_DCI
#endif

