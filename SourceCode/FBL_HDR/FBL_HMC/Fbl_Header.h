/****************************************************************************
| Project Name: GM-Header
|    File Name: GMHeader.H
|
|  Description: Flash Boot Loader configuration file
|
|-----------------------------------------------------------------
|               C O P Y R I G H T
|-----------------------------------------------------------------
| Copyright (c) 2003-2005 by Vector Informatik GmbH, all rights reserved
|-----------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-----------------------------------------------------------------
| Initials      Name                   Company
| --------      --------------------   ---------------------------
|  Hp            Armin Happel          Vector Informatik GmbH
|  Et            Thomas Ebert          Vector Informatik GmbH
|  MFR           Michael F. Radwick    Vector CANtech, Inc.
|-----------------------------------------------------------------
|               R E V I S I O N   H I S T O R Y
|-----------------------------------------------------------------
| Date         Ver   Author   Description
| ---------    ----  ------   ------------------------------------
| 2003-07-23   1.00   Hp      Creation
| 2004-08-13   1.01   Et      support C_COMP_MITSUBISHI_M32C
| 2005-02-28   1.02   Fz      support for C_COMP_GHS_V85X changed
| 2005-10-27   1.03   MFR     Support for GM-FBL SLP2
|*****************************************************************************/

#ifndef GM_HEADER_MODULE_H
#define GM_HEADER_MODULE_H
//#include "../../LIB/common.h"

/* Configuration of SWMI-Information */
#define SWMI_DATALEN    4        /* Length of Software-module information (max. 16 char) */
#define DLS__DATALEN    2        /* Length of DLS (Alphacode; either 2 or 3 bytes        */

/* Use this number as the max. PMA you will reserve for CANFlash */
#define SWM_DATA_MAX_NOAR     40

/* Use this number as the max. additional-modules reserved for CANFlash */
#ifdef  SWM_DATA_MAX_NOAM
    #undef  SWM_DATA_MAX_NOAM
#endif
#define SWM_DATA_MAX_NOAM      1

#define SWM_USE_2BYTE_HFI
#define SWM_USE_MPFH                /* Only for GMW3110 v1.5 based FBL (SLP2) */
#define SWM_USE_DCID                /* Only for GMW3110 v1.5 based FBL (SLP2) */

/* Checksum magic code to instruct CANFLash to calculate the HEX-File checksum */
#define CSUM_PLACEHOLDER_HIBYTE     0x11u
#define CSUM_PLACEHOLDER_LOBYTE     0xAAu


/* Module ID for the application */
#define MODID_APPLICATION_HIBYTE    0x00u
#define MODID_APPLICATION_LOBYTE    0x01u

#define MODID_CALIBRATION_HIBYTE    0x00u
#define MODID_CALIBRATION_LOBYTE    0x02u


/* Data Compatibility ID for the application: Must match DCID in FBL */
#if (__MCUMEMORY_2M == 1)
#define DCID_APPLICATION_HIBYTE     0xB3u
#define DCID_APPLICATION_LOBYTE     0x00u
#else
#define DCID_APPLICATION_HIBYTE     0xB3u
#define DCID_APPLICATION_LOBYTE     0x10u
#endif
/* --------------------------------- */
/* Definition of the HFI bit fields */
/* --------------------------------- */

/* Define mask flags to extract the bit information */
#define HFI_1STBYTE_SWMI_LEN_MASK     0xF8u
#define HFI_1STBYTE_DLS_LEN_MASK      0x04u
#define HFI_1STBYTE_SIZE_BIT_MASK     0x02u
#define HFI_1STBYTE_PMA_BIT_MASK      0x01u
#define HFI_2NDBYTE_MPFH_BIT_MASK     0x80u
#define HFI_2NDBYTE_DCID_BIT_MASK     0x40u

/* Define bits within the HFI */
#define HFI_1STBYTE_SWMI_LEN_1BYTE    0x08u
#define HFI_1STBYTE_SWMI_LEN_2BYTE    0x10u
#define HFI_1STBYTE_SWMI_LEN_3BYTE    0x18u
#define HFI_1STBYTE_SWMI_LEN_4BYTE    0x20u
#define HFI_1STBYTE_SWMI_LEN_5BYTE    0x28u
#define HFI_1STBYTE_SWMI_LEN_6BYTE    0x30u
#define HFI_1STBYTE_SWMI_LEN_7BYTE    0x38u
#define HFI_1STBYTE_SWMI_LEN_8BYTE    0x40u
#define HFI_1STBYTE_SWMI_LEN_9BYTE    0x48u
#define HFI_1STBYTE_SWMI_LEN_10BYTE   0x50u
#define HFI_1STBYTE_SWMI_LEN_11BYTE   0x58u
#define HFI_1STBYTE_SWMI_LEN_12BYTE   0x60u
#define HFI_1STBYTE_SWMI_LEN_13BYTE   0x68u
#define HFI_1STBYTE_SWMI_LEN_14BYTE   0x70u
#define HFI_1STBYTE_SWMI_LEN_15BYTE   0x78u
#define HFI_1STBYTE_SWMI_LEN_16BYTE   0x80u

#define HFI_1STBYTE_DLS_LEN_2BYTE     0x00u
#define HFI_1STBYTE_DLS_LEN_3BYTE     0x04u
#define HFI_1STBYTE_SIZE_1BYTE        0x00u
#define HFI_1STBYTE_SIZE_2BYTE        0x02u
#define HFI_1STBYTE_PMA_PRESENT       0x01u
#define HFI_1STBYTE_PMA_NOT_PRESENT   0x00u

#define HFI_2NDBYTE_MPFH_PRESENT      0x80u
#define HFI_2NDBYTE_MPFH_NOT_PRESENT  0x00u

#define HFI_2NDBYTE_DCID_PRESENT      0x40u
#define HFI_2NDBYTE_DCID_NOT_PRESENT  0x00u

/* ----------------------------------------------------------- */
/* Define max. number of NOAR defined in the header            */
/* This is also a placeholder for PMA's. Allocate enough space */
/* in the PMA field to let CANFLash replace the values with    */
/* real PMA's from the HEX-File.                               */
/* ----------------------------------------------------------- */
/* NOTE: CANFlash REPLACES THE PMA ADDRESS AND LENGTH INFOR-   */
/*       MATION IN THE HEX-FILE IF THE CHECKSUM MAGIC CODE     */
/*       IS GIVEN!!!!!                                         */
/* ----------------------------------------------------------- */



typedef struct {
    unsigned char    CS[2];               /* Module Checksum            */
    unsigned char    MID[2];              /* Module ID                  */
    unsigned char    HFI;                 /* Header Format Identifier   */
#if defined( SWM_USE_2BYTE_HFI )
    unsigned char    MPFH;                /* Header Format Identifier   */
#endif
    unsigned char    SWMI[SWMI_DATALEN];  /* SoftWare Module Identifier */
    unsigned char    DLS[DLS__DATALEN];   /* Design Level Suffix        */
#if defined( SWM_USE_DCID )
    unsigned char    DCID[2];             /* Data Compatibility Identifier */
#endif
#if defined( SWM_USE_2BYTE_HFI )
    unsigned char    NOAR[2];             /* Number Of Address Regions  */
#else
    unsigned char    NOAR[1];             /* Number Of Address Regions  */
#endif
    struct {
      unsigned char    PMA[4];            /* Product Memory Address     */
      unsigned char    NOB[4];            /* Number Of Bytes            */
    } PMA_NOB[SWM_DATA_MAX_NOAR];
#if defined( SWM_USE_MPFH )
    unsigned char    NOAM[2];             /* Number of Additional Modules */
    struct {
# if defined( SWM_USE_DCID )
       unsigned char   DCID[2];              /* Data Compatibility ID */
# endif
       unsigned char   NOAR[2];              /* Number of Address Regions */
       unsigned char   PMA[4];               /* Product Memory Address */
       unsigned char   NOB[4];               /* Number Of Bytes */
    } PMA_NOAM[SWM_DATA_MAX_NOAM];
#endif
} tSwmApplHeader;

#if (DLS__DATALEN<2) || (DLS__DATALEN>3)
# error "Wrong configuration for DLS length."
#endif
#if (SWMI_DATALEN<4) || (SWMI_DATALEN>16)
# error "Wrong configuration for SWMI-length."
#endif
#if (( defined( SWM_USE_MPFH ) || defined( SWM_USE_DCID ) ) && \
     ( !defined( SWM_USE_2BYTE_HFI ) ))
# error "Wrong configuration of HFI."
#endif



/* ----------------------------------------------------------- */
/* Calibration Data Header Format                               */
/* This is also a placeholder for PMA's. Allocate enough space */
/* in the PMA field to let CANFLash replace the values with    */
/* real PMA's from the HEX-File.                               */
/* ----------------------------------------------------------- */



typedef struct {
    unsigned char    CS[2];               /* Module Checksum            */
    unsigned char    MID[2];              /* Module ID                  */
    unsigned char    HFI;                 /* Header Format Identifier   */
#ifdef SWM_USE_2BYTE_HFI
    unsigned char    MPFH;                /* Header Format Identifier   */
#endif
    unsigned char    SWMI[SWMI_DATALEN];  /* SoftWare Module Identifier */
    unsigned char    DLS[DLS__DATALEN];   /* Design Level Suffix        */
    unsigned char    CCID[2];
} tSwmCalHeader;


#define	FBLHEADER_START_SEC_CONST
#include "Fbl_MemMap.h"
extern const  tSwmApplHeader GM_ApplHeader;
#define	FBLHEADER_STOP_SEC_CONST
#include "Fbl_MemMap.h"


#define CALIBRATION_SET0_START
#include "Fbl_MemMap.h"
extern const  tSwmCalHeader GM_CalHeader_0;
#define CALIBRATION_SET0_STOP
#include "Fbl_MemMap.h"

/* Export the header definition */
extern unsigned char OP_PN[4];
extern unsigned char BOOT_PN[4];
extern unsigned char CAL_PN[4];




#endif

/************   Organi, Version 3.2.5 Vector-Informatik GmbH  ************/
