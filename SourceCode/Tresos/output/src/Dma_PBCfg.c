/******************************************************************************
**                                                                           **
** Copyright (C) Infineon Technologies (2013)                                **
**                                                                           **
** All rights reserved.                                                      **
**                                                                           **
** This document contains proprietary information belonging to Infineon      **
** Technologies. Passing on and copying of this document, and communication  **
** of its contents is not permitted without prior written authorization.     **
**                                                                           **
*******************************************************************************
**                                                                           **
**  FILENAME  : Dma_PBCfg.c                                                  **
**                                                                           **
**  $CC VERSION : \main\10 $                                                 **
**                                                                           **
**  DATE, TIME: 2015-07-13, 13:37:28                                         **
**                                                                           **
**  GENERATOR : Build b120625-0327                                           **
**                                                                           **
**  AUTHOR    : DL-AUTOSAR-Engineering                                       **
**                                                                           **
**  VENDOR    : Infineon Technologies                                        **
**                                                                           **
**  DESCRIPTION  : DMA configuration generated out of ECU configuration      **
**                 file (Dma.bmd/.xdm)                                       **
**                                                                           **
**  MAY BE CHANGED BY USER [yes/no]: No                                      **
**                                                                           **
******************************************************************************/
/*******************************************************************************
  TRACEBILITY : [cover parentID = DS_NAS_PR69_PR469_PR122_PR123,DS_NAS_PR647,
                DS_NAS_PR446,ASW:1499,ASW:1500,DS_NAS_PR78,ASW:1501,ASW:1504,
                ASW:1507,ASW:1508,ASW:1512,ASW:1513,ASW:1514,ASW:1517,ASW:1519,
                ASW:1523,ASW:1524,ASW:1528][/cover]
*******************************************************************************/

/*******************************************************************************
**                            Includes                                        **
*******************************************************************************/
/* Include module header file */
#include "Dma.h"
/*******************************************************************************
**                      Private Macro Definitions                             **
*******************************************************************************/

/*******************************************************************************
**                      Global Constant Definitions                           **
*******************************************************************************/
/*
                     Container: DmaChannelConfiguration
*/

/* Memory Mapping the configuration constant */
#define DMA_START_SEC_POSTBUILDCFG
#include "MemMap.h"

/************************** DMA Channel Config Root ***************************/

/******** Configuration of channels in DmaConfigSet(0) ********/
static const Dma_ChannelConfigType Dma_kChannelConfigRoot0[]=
{
 {
/* Configuration for DMA Channel(11) */
  0X00500000U, /* Configuration for DMA register CHCFGR */
  0X04307088U, /* Configuration for DMA register ADICR */
  0X01U,       /* Configuration for DMA register MODE */
  0X00U,       /* Configuration for DMA register HRR */
  11U           /*DMA channel ID*/
 },
 {
/* Configuration for DMA Channel(12) */
  0X00500000U, /* Configuration for DMA register CHCFGR */
  0X04307088U, /* Configuration for DMA register ADICR */
  0X01U,       /* Configuration for DMA register MODE */
  0X00U,       /* Configuration for DMA register HRR */
  12U           /*DMA channel ID*/
 },
 {
/* Configuration for DMA Channel(13) */
  0X00500000U, /* Configuration for DMA register CHCFGR */
  0X04307088U, /* Configuration for DMA register ADICR */
  0X01U,       /* Configuration for DMA register MODE */
  0X00U,       /* Configuration for DMA register HRR */
  13U           /*DMA channel ID*/
 },
 {
/* Configuration for DMA Channel(14) */
  0X00500000U, /* Configuration for DMA register CHCFGR */
  0X04307088U, /* Configuration for DMA register ADICR */
  0X01U,       /* Configuration for DMA register MODE */
  0X00U,       /* Configuration for DMA register HRR */
  14U           /*DMA channel ID*/
 },
 {
/* Configuration for DMA Channel(15) */
  0X00500000U, /* Configuration for DMA register CHCFGR */
  0X04307088U, /* Configuration for DMA register ADICR */
  0X01U,       /* Configuration for DMA register MODE */
  0X00U,       /* Configuration for DMA register HRR */
  15U           /*DMA channel ID*/
 },
 {
/* Configuration for DMA Channel(16) */
  0X00500000U, /* Configuration for DMA register CHCFGR */
  0X04307088U, /* Configuration for DMA register ADICR */
  0X01U,       /* Configuration for DMA register MODE */
  0X00U,       /* Configuration for DMA register HRR */
  16U           /*DMA channel ID*/
 },
 {
/* Configuration for DMA Channel(17) */
  0X00500000U, /* Configuration for DMA register CHCFGR */
  0X04307088U, /* Configuration for DMA register ADICR */
  0X01U,       /* Configuration for DMA register MODE */
  0X00U,       /* Configuration for DMA register HRR */
  17U           /*DMA channel ID*/
 },
 {
/* Configuration for DMA Channel(18) */
  0X00500000U, /* Configuration for DMA register CHCFGR */
  0X04307088U, /* Configuration for DMA register ADICR */
  0X01U,       /* Configuration for DMA register MODE */
  0X00U,       /* Configuration for DMA register HRR */
  18U           /*DMA channel ID*/
 }
};

/*************************** DMA Config Root **********************************/

const Dma_ConfigType Dma_ConfigRoot[1]=
{
/*************** Configuration for DmaConfigSet(0) ***************/
 {
  &Dma_kChannelConfigRoot0[0U], /* Address of channel config root
                                  for DMA configuration [0] */
  0X00000000U, /* Configuration for DMA register PRR0 */
  0X00000000U, /* Configuration for DMA register PRR1 */
  0X05030000U, /* Configuration for DMA register ERR0 */
  0X05030000U, /* Configuration for DMA register ERR1 */
  8U           /*Number of channels configured*/
 }
};

#define DMA_STOP_SEC_POSTBUILDCFG
#include "MemMap.h"
