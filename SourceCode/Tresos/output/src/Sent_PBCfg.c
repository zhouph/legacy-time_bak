/*******************************************************************************
**                                                                            **
** Copyright (C) Infineon Technologies (2013)                                 **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to Infineon       **
** Technologies. Passing on and copying of this document, and communication   **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME  : Sent_PBCfg.c                                                  **
**                                                                            **
**  $CC VERSION : \main\9 $                                                  **
**                                                                            **
**  DATE, TIME: 2015-07-13, 13:37:20                                          **
**                                                                            **
**  GENERATOR : Build b120625-0327                                            **
**                                                                            **
**  AUTHOR    : DL-AUTOSAR-Engineering                                        **
**                                                                            **
**  VENDOR    : Infineon Technologies                                         **
**                                                                            **
**  DESCRIPTION  : SENT configuration generated out of ECU configuration      **
**                 file  (Sent.bmd)                                           **
**                                                                            **
**  MAY BE CHANGED BY USER [yes/no]: no                                       **
**                                                                            **
*******************************************************************************/


/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/
#include "Sent_Cfg.h"

#define SENT_START_SEC_POSTBUILDCFG
#include "MemMap.h"

/*******************************************************************************
**                      Private Macro definition                              **
*******************************************************************************/

/*******************************************************************************
**                      Configuration Options                                 **
*******************************************************************************/

/*******************************************************************************
**                      Private Type Definitions                              **
*******************************************************************************/


/*******************************************************************************
**                      Private Function Declarations                         **
*******************************************************************************/


/*******************************************************************************
**                      Global Variable Definitions                           **
*******************************************************************************/

/*******************************************************************************
**                      Global Funtion Declarations                           **
*******************************************************************************/

/*******************************************************************************
**                      Global Constant Definitions                           **
*******************************************************************************/
/* SENT Channel Configuration for ConfigSet 0 */
static const Sent_ChannelCfgType Sent_ChanConfig0[] = 
{
  {
    /* SENT_RCRx register value */
    (0x00070670U),
    /* SENT_IOCRx register value */  
    (0x00000002U),
    /* SENT_VIEWx register value */  
    (0x73654012U),
    /* SENT_CPDRx register value */  
    (0x00000007U),
    /* SENT_CFDRx reg value (Baudrate tick time derived is 2.5971428571428574us) */  
    (0x0000071aU),
    /* Frame length */  
    (6U),
    /* Interrupt Node */
    (6U),        
    /* SENT Physical Channel Id */  
    (6U),
  },
  {
    /* SENT_RCRx register value */
    (0x00070670U),
    /* SENT_IOCRx register value */  
    (0x00000002U),
    /* SENT_VIEWx register value */  
    (0x73654012U),
    /* SENT_CPDRx register value */  
    (0x00000007U),
    /* SENT_CFDRx reg value (Baudrate tick time derived is 2.5971428571428574us) */  
    (0x0000071aU),
    /* Frame length */  
    (6U),
    /* Interrupt Node */
    (8U),        
    /* SENT Physical Channel Id */  
    (8U),
  },
  {
    /* SENT_RCRx register value */
    (0x00070670U),
    /* SENT_IOCRx register value */  
    (0x00000002U),
    /* SENT_VIEWx register value */  
    (0x73654012U),
    /* SENT_CPDRx register value */  
    (0x00000007U),
    /* SENT_CFDRx reg value (Baudrate tick time derived is 2.5971428571428574us) */  
    (0x0000071aU),
    /* Frame length */  
    (6U),
    /* Interrupt Node */
    (9U),        
    /* SENT Physical Channel Id */  
    (9U),
  },
};

/* SRN to SENT logical channel mapping for ConfigSet 0 */
static const uint8 Srn_ChanMap0[SENT_HW_MAX_CHANNELS] = 
{
  (SENT_CHANNEL_NOT_MAPPED),
  (SENT_CHANNEL_NOT_MAPPED),
  (SENT_CHANNEL_NOT_MAPPED),
  (SENT_CHANNEL_NOT_MAPPED),
  (SENT_CHANNEL_NOT_MAPPED),
  (SENT_CHANNEL_NOT_MAPPED),
  (0U),
  (SENT_CHANNEL_NOT_MAPPED),
  (1U),
  (2U),
};



/* SENT Module Configuration */
const Sent_ConfigType Sent_ConfigRoot[1] = 
{
  /* ConfigSet 0 */
  {
    /* Pointer to Channel configuration */
    &Sent_ChanConfig0[0],
    /* Pointer to the SRN mapping array */
    &Srn_ChanMap0[0],
    /* Module clock divider */
    (0x00000100U),
    /* ACCEN0 register value */     
    0xFFFFFFFFU, 
    /* Module Fractional step divider */      
    (1023U),
    /* SENT channels configured */
    (3U),
  },
};

#define SENT_STOP_SEC_POSTBUILDCFG
/*IFX_MISRA_RULE_19_01_STATUS= Memmap file included as per AUTOSAR*/
#include "MemMap.h"

