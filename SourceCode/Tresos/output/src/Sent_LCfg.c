/******************************************************************************
**                                                                           **
** Copyright (C) Infineon Technologies (2013)                                **
**                                                                           **
** All rights reserved.                                                      **
**                                                                           **
** This document contains proprietary information belonging to Infineon      **
** Technologies. Passing on and copying of this document, and communication  **
** of its contents is not permitted without prior written authorization.     **
**                                                                           **
*******************************************************************************
**                                                                           **
**  FILENAME  : Sent_LCfg.c                                                  **
**                                                                           **
**  $CC VERSION : \main\4 $                                                  **
**                                                                           **
**  DATE, TIME: 2015-07-13, 13:37:19                                         **
**                                                                           **
**  GENERATOR : Build b120625-0327                                           **
**                                                                           **
**  AUTHOR    : DL-AUTOSAR-Engineering                                       **
**                                                                           **
**  VENDOR    : Infineon Technologies                                        **
**                                                                           **
**  DESCRIPTION  : SENT configuration generated out of ECU configuration     **
**                 file (Sent.bmd/.xdm)                                      **
**                                                                           **
**  MAY BE CHANGED BY USER [yes/no]: No                                      **
**                                                                           **
******************************************************************************/
/*******************************************************************************
**                                                                            **
*******************************************************************************/

/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/

#include "Sent_Cfg.h"

/*******************************************************************************
**                      Global Macro Definitions                              **
*******************************************************************************/

/*******************************************************************************
**                      Private Macro Definitions                             **
*******************************************************************************/
/*******************************************************************************
**                      Private Type Definitions                              **
*******************************************************************************/


/*******************************************************************************
**                      Private Function Declarations                         **
*******************************************************************************/


/*******************************************************************************
**                      Global Funtion Declarations                           **
*******************************************************************************/

/*******************************************************************************
**                      Global Constant Definitions                           **
*******************************************************************************/
#define SENT_START_SEC_CONST_UNSPECIFIED
#include "MemMap.h"

/* SENT Application Callout function pointer array per logical channel 
 * Common for all configuration sets
 */
const Sent_NotifFnPtrType SentNotifFnPtrArray[3] = 
{
  &SentChannel0_CallOut,
  &SentChannel1_CallOut,
  &SentChannel2_CallOut,
};


#define SENT_STOP_SEC_CONST_UNSPECIFIED
/*IFX_MISRA_RULE_19_01_STATUS= Memmap file included as per AUTOSAR*/
#include "MemMap.h"
