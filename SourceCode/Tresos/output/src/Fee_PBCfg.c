/******************************************************************************
**                                                                           **
** Copyright (C) Infineon Technologies (2013)                                **
**                                                                           **
** All rights reserved.                                                      **
**                                                                           **
** This document contains proprietary information belonging to Infineon      **
** Technologies. Passing on and copying of this document, and communication  **
** of its contents is not permitted without prior written authorization.     **
**                                                                           **
*******************************************************************************
**                                                                           **
**  $FILENAME   : Fee_PBCfg.c $                                              **
**                                                                           **
**  $CC VERSION : \main\14 $                                                 **
**                                                                           **
**  DATE, TIME: 2015-07-13, 13:37:27                                         **
**                                                                           **
**  GENERATOR : Build b120625-0327                                           **
**                                                                           **
**  AUTHOR    : DL-AUTOSAR-Engineering                                       **
**                                                                           **
**  VENDOR    : Infineon Technologies                                        **
**                                                                           **
**  DESCRIPTION  : FEE configuration generated out of ECU configuration      **
**                   file (Fee.bmd)                                          **
**                                                                           **
**  MAY BE CHANGED BY USER [yes/no]: No                                      **
**                                                                           **
******************************************************************************/

/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/

/* Include Fee Module Header File */
#include "Fee.h"

/*******************************************************************************
**                      Imported Compiler Switch Checks                       **
*******************************************************************************/

/*******************************************************************************
**                      Private Macro Definitions                             **
*******************************************************************************/

/*******************************************************************************
**                      Private Type Definitions                              **
*******************************************************************************/

/*******************************************************************************
**                      Private Function Declarations                         **
*******************************************************************************/

/*******************************************************************************
**                      Global Function Declarations                          **
*******************************************************************************/

/*******************************************************************************
**                      Global Constant Definitions                           **
*******************************************************************************/

/* Allocate space for state data variables in RAM */
#define FEE_START_SEC_VAR_FAST_UNSPECIFIED
#include "MemMap.h"

/* Fee State Variable structure */
static Fee_StateDataType Fee_StateVar;

#define FEE_STOP_SEC_VAR_FAST_UNSPECIFIED
#include "MemMap.h"


/* User configured logical block initialization structure */
#define FEE_START_SEC_CONST_UNSPECIFIED
#include "MemMap.h"

static const Fee_BlockType Fee_BlockConfig[] =
{
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_NORMAL_DATA,     /* Block type is Normal */
    1U, /* Block number */
    258U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_NORMAL_DATA,     /* Block type is Normal */
    2U, /* Block number */
    514U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_NORMAL_DATA,     /* Block type is Normal */
    3U, /* Block number */
    258U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_NORMAL_DATA,     /* Block type is Normal */
    4U, /* Block number */
    770U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_NORMAL_DATA,     /* Block type is Normal */
    5U, /* Block number */
    130U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_NORMAL_DATA,     /* Block type is Normal */
    6U, /* Block number */
    130U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_NORMAL_DATA,     /* Block type is Normal */
    7U, /* Block number */
    258U /* Fee Block Size */
  }
};

/* Fee Global initialization structure. */
static const Fee_ConfigType Fee_ConfigRoot[] =
{
  {
    /* Fee State Data Structure */
    &Fee_StateVar,
    /* Pointer to logical block configurations */
    (Fee_BlockType *)(void *)&Fee_BlockConfig,
    /* Fee Job end notification API */
    &NvM_JobEndNotification,
    /* Fee Job error notification API */
    &NvM_JobErrorNotification,
    /* Fee threshold value */
    185560U,
    /* Number of blocks configured */
    7U,

    {
       /* Ignore the unconfigured blocks */
       FEE_UNCONFIG_BLOCK_IGNORE,
       /* Restart Garbage Collection during initialization */
       FEE_GC_RESTART_INIT,
       /* Erase Suspend feature is enabled */
       FEE_ERASE_SUSPEND_ENABLED,
       /* Reserved */
       0U
    },

    /* Fee Illegal State notification */
    (Fee_NotifFunctionPtrType)NULL_PTR,
    /* Erase All feature is enabled */
    (boolean)TRUE
  }
};

#define FEE_STOP_SEC_CONST_UNSPECIFIED
#include "MemMap.h"

#define FEE_START_SEC_SPL_VAR_32BIT
#include "MemMap.h"
Fee_ConfigPtrType Fee_CfgPtr = (Fee_ConfigPtrType)(void *)&Fee_ConfigRoot[0];
#define FEE_STOP_SEC_SPL_VAR_32BIT
#include "MemMap.h"

