/*******************************************************************************
**                                                                            **
** Copyright (C) Infineon Technologies (2013)                                 **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to Infineon       **
** Technologies. Passing on and copying of this document, and communication   **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  $FILENAME   : Can_17_MCanP_PBCfg.c $                                     **
**                                                                            **
**  $CC VERSION : \main\dev_tc23x_as4.0.3\3 $                                **
**                                                                            **
**  DATE, TIME: 2015-07-13, 13:37:28                                      **
**                                                                            **
**  GENERATOR : Build b120625-0327                                          **
**                                                                            **
**  AUTHOR    : DL-AUTOSAR-Engineering                                        **
**                                                                            **
**  VENDOR    : Infineon Technologies                                         **
**                                                                            **
**  DESCRIPTION  : CAN configuration generated out of ECU configuration       **
**                   file(Can_17_MCanP.bmd)                                   **
**                                                                            **
**  MAY BE CHANGED BY USER [yes/no]: No                                       **
**                                                                            **
*******************************************************************************/

/**  TRACEABILITY: [cover parentID=DS_NAS_PR69_PR469_PR122,DS_NAS_PR446]                      
                     [/cover]                                              **/

/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/

/* Include CAN Driver Header File */
#include "Can_17_MCanP.h"

/*******************************************************************************
**                      Private Macro Definitions                             **
*******************************************************************************/

/*******************************************************************************
**                      Imported Compiler Switch Check                        **
*******************************************************************************/
/*******************************************************************************
**                      Private Type Definitions                              **
*******************************************************************************/
/*******************************************************************************
**                      Private Function Declarations                         **
*******************************************************************************/
/*******************************************************************************
**                      Global Constant Definitions                           **
*******************************************************************************/

#define CAN_17_MCANP_START_SEC_POSTBUILDCFG
#include "MemMap.h"

/******************************************************************************/
                /* CAN Controller Baudrate Configurations */
/******************************************************************************/
                          /* Baudrate Setting */
   /* (uint16)((DIV8 << 15)|(TSEG2 << 12)|(TSEG1 << 8)|(SJW << 6)|(BRP)) */
   /* SJW   -> CanControllerSyncJumpWidth - 1                            */
   /* TSEG1 -> CanControllerPropSeg + CanControllerSeg1 - 1              */
   /* TSEG2 -> CanControllerSeg2 - 1                                     */
/******************************************************************************/


/* CanConfigSet_0 -> CanController_0 Baudrate Configuration */

static const Can_17_MCanP_ControllerBaudrateConfigType Can_kBaudrateConfig_0_0[] = 
{
  {
    /* Configured Baudrate -> 500 kbps */
    /* Actual Baudrate     -> 505.050505050505 kbps */
    /* BRP   -> 8 */
    /* SJW   -> 2  */
    /* TSEG1 -> 14  */
    /* TSEG2 -> 5  */
    /* DIV8  -> 0  */
    0x5e88U,
    500U
  }
};

/* CanConfigSet_0 -> CanController_1 Baudrate Configuration */

static const Can_17_MCanP_ControllerBaudrateConfigType Can_kBaudrateConfig_0_1[] = 
{
  {
    /* Configured Baudrate -> 500 kbps */
    /* Actual Baudrate     -> 505.050505050505 kbps */
    /* BRP   -> 8 */
    /* SJW   -> 2  */
    /* TSEG1 -> 14  */
    /* TSEG2 -> 5  */
    /* DIV8  -> 0  */
    0x5e88U,
    500U
  }
};

static const Can_BaudrateConfigPtrType Can_kBaudrateConfig_0[] = 
{
  { &Can_kBaudrateConfig_0_0[0] },
  { &Can_kBaudrateConfig_0_1[0] },
};


/* CAN L-PDU Receive Callout Function */
extern boolean Can_ReceiveCallOut (uint8 Hrh,
                                     Can_IdType CanId,
                                     uint8 CanDlc,
                                     const uint8 *CanSduPtr);

/******************************************************************************/
            /* Loopback and receive input pin selection setting */
/******************************************************************************/

/* Config 0 */
static const struct Can_NPCRValueType Can_kNPCR_0[] = 
{
  /* LoopBack Disabled, RXDCANxB */
  { 1U },
  /* LoopBack Disabled, RXDCANxA */
  { 0U },
};


/*******************************************************************************
               Transmit / Receive Hardware Object Configurations
********************************************************************************
 Tx Object -> { HW MO Id, [No. of Multiplexed MOs,] Hw Controller Id, Id Type }
        Rx Object -> { Mask, Msg Id, HW MO Id, Hw Controller Id, [Id Type] }
********************************************************************************
       Note: [1] If the associated CAN Controller is not activated then,
                   Hw Controller Id -> 255
             [2] If CanFilterMaskRef is not configured then, 
                   Mask -> 0x7ff - for STANDARD Msg Id Type
                           0x1fffffff - for EXTENDED/MIXED Msg Id Type
*******************************************************************************/

/* CanConfigSet_0 -> Transmit Hardware Object Configuration */
static const Can_TxHwObjectConfigType Can_kTxHwObjectConfig_0[] = 
{ 
  { 27U, 0U },
  { 28U, 0U },
  { 29U, 0U },
  { 30U, 0U },
  { 31U, 0U },
  { 32U, 0U },
  { 33U, 0U },
  { 34U, 0U },
  { 35U, 0U },
  { 36U, 0U },
  { 37U, 0U },
  { 38U, 0U },
  { 39U, 2U },
  { 40U, 2U },
  { 41U, 2U },
  { 42U, 2U },
  { 43U, 2U },
  { 44U, 2U },
  { 45U, 2U },
  { 46U, 2U },
  { 47U, 2U },
  { 48U, 2U },
  { 49U, 2U },
  { 50U, 2U },
  { 51U, 2U },
  { 52U, 2U },
  { 53U, 2U },
  { 54U, 2U },
  { 55U, 2U },
  { 56U, 2U },
  { 57U, 2U },
  { 58U, 2U },
  { 59U, 2U },
  { 60U, 2U },
  { 61U, 2U },
  { 62U, 2U },
  { 63U, 2U },
  { 64U, 2U }
};

/* CanConfigSet_0 -> Receive Hardware Object Configuration */
static const Can_RxHwObjectConfigType Can_kRxHwObjectConfig_0[] = 
{
  { 0x7ffU, 0x316U, 0U, 0U },
  { 0x7ffU, 0x329U, 1U, 0U },
  { 0x7ffU, 0x260U, 2U, 0U },
  { 0x7ffU, 0x43fU, 3U, 0U },
  { 0x7ffU, 0x440U, 4U, 0U },
  { 0x7ffU, 0x575U, 5U, 0U },
  { 0x7ffU, 0x100U, 6U, 0U },
  { 0x7ffU, 0x200U, 7U, 0U },
  { 0x7ffU, 0x201U, 8U, 0U },
  { 0x7ffU, 0x202U, 9U, 0U },
  { 0x7ffU, 0x592U, 10U, 0U },
  { 0x7ffU, 0x291U, 11U, 0U },
  { 0x7ffU, 0x523U, 12U, 0U },
  { 0x7ffU, 0x670U, 13U, 0U },
  { 0x7ffU, 0x165U, 14U, 0U },
  { 0x7ffU, 0x2b0U, 15U, 0U },
  { 0x7ffU, 0x651U, 16U, 0U },
  { 0x7ffU, 0x4f0U, 17U, 0U },
  { 0x7ffU, 0x541U, 18U, 0U },
  { 0x7ffU, 0x560U, 19U, 0U },
  { 0x7ffU, 0x7dfU, 20U, 0U },
  { 0x7ffU, 0x7d1U, 21U, 0U },
  { 0x7ffU, 0x690U, 22U, 0U },
  { 0x7ffU, 0x433U, 23U, 0U },
  { 0x7ffU, 0x188U, 24U, 2U },
  { 0x7ffU, 0x189U, 25U, 2U },
  { 0x7ffU, 0x528U, 26U, 2U }
};


/*******************************************************************************
              CAN Controller <-> CAN Hardware Object Mapping
********************************************************************************
        { First Rx Hardware Object, No. of Rx Hardware Objects, 
          First Tx Hardware Object, No. of Tx Hardware Objects }
********************************************************************************
           Note: If the CAN controller is not activated then,
                 { 0U, 0U, 0U, 0U } will be generated
*******************************************************************************/

/* CanConfigSet_0 -> CAN Controller - CAN Hardware Object Mapping */
static const Can_ControllerMOMapConfigType Can_kControllerMOMapConfig_0[] = 
{
  { { 0U, 24U, 27U, 12U } },
  { { 24U, 3U, 39U, 26U } }
};



/*******************************************************************************
              CAN Controller Handling of Events : Interrupt/Polling
********************************************************************************
        { CanBusoffProcessing, CanRxProcessing, 
          CanTxProcessing, CanWakeupProcessing }
********************************************************************************
           Note: If the CAN controller is not activated then,
                 { 0U, 0U, 0U, 0U } will be generated
*******************************************************************************/

/* CanConfigSet_0 -> CAN Controller - Handling of Events */
static const Can_EventHandlingType Can_kEventHandlingConfig_0[] = 
{
 { { (CAN_POLLING),(CAN_POLLING),(CAN_POLLING),(CAN_INTERRUPT)} },
 { { (CAN_POLLING),(CAN_POLLING),(CAN_POLLING),(CAN_INTERRUPT)} }
};



/*******************************************************************************
              CAN Controller <-> Default Baudrate Mapping
********************************************************************************
          { NBTR Register Settings, Default Baudrate in kbps }
*******************************************************************************/

/* CanConfigSet_0 -> CAN Controller - default baudrate mapping */
static const Can_17_MCanP_ControllerBaudrateConfigType Can_kDefaultBaudrateConfig_0[] = 
{
  /* CAN Controller 0 :
     Configured Baudrate -> 500 kbps
     Actual Baudrate     -> 505.050505050505 kbps */
  { 0x5e88U, 500U },
  /* CAN Controller 1 :
     Configured Baudrate -> 500 kbps
     Actual Baudrate     -> 505.050505050505 kbps */
  { 0x5e88U, 500U }
};




/******************************************************************************/
                      /* CAN Configuration Pointer */
/******************************************************************************/
      /* Over all CAN configurations in the array, pointer to one of the 
           configuration is passed as parameter to Can_Init API */   
/******************************************************************************/

const Can_17_MCanP_ConfigType Can_17_MCanP_ConfigRoot[] = 
{
  {
    /* Pointer to Loopback and receive input pin selection setting */
    &Can_kNPCR_0[0],

    /* Pointer to CAN Controller <-> Hardware Objects Mapping */
    &Can_kControllerMOMapConfig_0[0],

    /* Pointer to CAN Controller Handling of Events : Interrupt/Polling */
    &Can_kEventHandlingConfig_0[0],

    /* Pointer to Controller Baudrate Configuration */
    &Can_kBaudrateConfig_0[0],

    /* Pointer to default baudrate configuration */
    &Can_kDefaultBaudrateConfig_0[0],



    /* CAN L-PDU Receive Callout Function */
    &Can_ReceiveCallOut,

    /* Pointer to Configuration of transmit hardware objects */
    &Can_kTxHwObjectConfig_0[0],

    /* Pointer to Configuration of receive hardware objects */
    &Can_kRxHwObjectConfig_0[0],


    /* CAN Module clock configuration : Fractional Divider Register setting */
    /* NORMAL_DIVIDER - Bit 14 is set */
    /* CanClockStepValue 1023 (0x3ff) is assigned to bit 0-9. */
    0x43ffU,

    /* Number of configured CAN controllers */
    2U,
  
    /* CanControllerId --> CanHwControllerId (MultiCAN+ Controller Id) */
    { 0U, 2U },
 
    /* CAN Controller-wise number of baudrate configurations */
    { 1U, 1U },

    /* Total number of Transmit Hardware Objects */
    38U,
  
    /* Total number of Receive Hardware Objects */
    27U,
  
    /* Number of Receive Rx MOs + Rx FIFO MOs */
    27U,

    /* Transmit Hardware Objects Offset Kernel wise */
    { 27U },

    /* Receive Hardware Objects Offset Kernel wise */
    { 0U },

    /* Number of Rx FIFO MOs Kernel wise*/
    { 0U },

    /* Last MSPND register to be scanned for Rx MOs */
    0U,
  
    /* First MSPND register to be scanned for Tx MOs */
    0U,
  
    /* Last MSPND register to be scanned for Tx MOs */
    2U,

  }
};

#define CAN_17_MCANP_STOP_SEC_POSTBUILDCFG
#include "MemMap.h"

/*******************************************************************************
**                      Global Variable Definitions                           **
*******************************************************************************/
/*******************************************************************************
**                      Private Constant Definitions                          **
*******************************************************************************/
/*******************************************************************************
**                      Private Variable Definitions                          **
*******************************************************************************/
/*******************************************************************************
**                      Global Function Definitions                           **
*******************************************************************************/
/*******************************************************************************
**                      Private Function Definitions                          **
*******************************************************************************/

