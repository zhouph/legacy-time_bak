/*******************************************************************************
**                                                                            **
** Copyright (C) Infineon Technologies (2013)                                 **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to Infineon       **
** Technologies. Passing on and copying of this document, and communication   **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME  : Spi_PBCfg.c                                                   **
**                                                                            **
**  $CC VERSION : \main\65 $                                                 **
**                                                                            **
**  DATE, TIME: 2015-07-13, 13:37:28                                         **
**                                                                            **
**  GENERATOR : Build b120625-0327                                            **
**                                                                            **
**  AUTHOR    : DL-AUTOSAR-Engineering                                        **
**                                                                            **
**  VENDOR    : Infineon Technologies                                         **
**                                                                            **
**  DESCRIPTION  : SPI configuration generated out of ECU configuration       **
**                 file                                                       **
**                                                                            **
**  MAY BE CHANGED BY USER [yes/no]: no                                       **
**                                                                            **
*******************************************************************************/


/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/

/* Include SPI Module File */
#include "Spi.h"
/* Inclusion of Mcal Specific Global Header File */
#include "Mcal.h"
    
/*******************************************************************************
**                      Private Macro Definitions                             **
*******************************************************************************/
/*******************************************************************************
**                      Imported Compiler Switch Check                        **
*******************************************************************************/
/*******************************************************************************
**                      Private Type Definitions                              **
*******************************************************************************/
/*******************************************************************************
**                      Private Function Declarations                         **
*******************************************************************************/
/*******************************************************************************
**                      Global Constant Definitions                           **
*******************************************************************************/
/* MISRA RULE 87 VIOLATION: Inclusion of MemMap.h in between the code can't 
   be avoided as it is required for mapping global variables, constants 
   and code
*/ 
/* Violates MISRA Required Rule 16.9, 
            function identifier used without '&' or parenthisized parameter list
           when using function pointer in configurations   
*/

/*
                     Container: SpiChannelConfiguration
*/
#define SPI_START_SEC_POSTBUILDCFG
/*
 * To be used for global or static constants (unspecified size)
*/
#include "MemMap.h"
/* 
Configuration : Channel Configuration Constant Structure.
The IB Channels are configured first followed by EB.  
*/ 
static const Spi_ChannelConfigType Spi_kChannelConfig0[] =
{
/* EB Channel: SpiChannel_SPI_Ref Configuration */
  {   
    /* Default Data, SPI_DEFAULT_DATA */               
    (uint32)0x00000000,          
    /* Data Configuration */   
    Spi_DataConfig(16U, /* Data Width */
                   SPI_DATA_MSB_FIRST), /* Transfer Start */
        /* EB Channels : SPI112: Max EB Buffers  */
    (Spi_NumberOfDataType)511,
    /* Buffer Type, SPI_CHANNEL_TYPE */
    (uint8)SPI_EB_BUFFER,
  },
/* EB Channel: SpiChannel_SPI_Ref_1 Configuration */
  {   
    /* Default Data, SPI_DEFAULT_DATA */               
    (uint32)0x00000000,          
    /* Data Configuration */   
    Spi_DataConfig(16U, /* Data Width */
                   SPI_DATA_MSB_FIRST), /* Transfer Start */
        /* EB Channels : SPI112: Max EB Buffers  */
    (Spi_NumberOfDataType)511,
    /* Buffer Type, SPI_CHANNEL_TYPE */
    (uint8)SPI_EB_BUFFER,
  },
/* EB Channel: SpiChannel_SPI_Ref_2 Configuration */
  {   
    /* Default Data, SPI_DEFAULT_DATA */               
    (uint32)0x00000000,          
    /* Data Configuration */   
    Spi_DataConfig(16U, /* Data Width */
                   SPI_DATA_MSB_FIRST), /* Transfer Start */
        /* EB Channels : SPI112: Max EB Buffers  */
    (Spi_NumberOfDataType)511,
    /* Buffer Type, SPI_CHANNEL_TYPE */
    (uint8)SPI_EB_BUFFER,
  },
/* EB Channel: SpiChannel_ASIC_Ref Configuration */
  {   
    /* Default Data, SPI_DEFAULT_DATA */               
    (uint32)0x00000000,          
    /* Data Configuration */   
    Spi_DataConfig(16U, /* Data Width */
                   SPI_DATA_MSB_FIRST), /* Transfer Start */
        /* EB Channels : SPI112: Max EB Buffers  */
    (Spi_NumberOfDataType)511,
    /* Buffer Type, SPI_CHANNEL_TYPE */
    (uint8)SPI_EB_BUFFER,
  },
/* EB Channel: SpiChannel_MPS2_Init Configuration */
  {   
    /* Default Data, SPI_DEFAULT_DATA */               
    (uint32)0x00000000,          
    /* Data Configuration */   
    Spi_DataConfig(16U, /* Data Width */
                   SPI_DATA_MSB_FIRST), /* Transfer Start */
        /* EB Channels : SPI112: Max EB Buffers  */
    (Spi_NumberOfDataType)511,
    /* Buffer Type, SPI_CHANNEL_TYPE */
    (uint8)SPI_EB_BUFFER,
  },
/* EB Channel: SpiChannel_MPS2_ErrAng Configuration */
  {   
    /* Default Data, SPI_DEFAULT_DATA */               
    (uint32)0x00000000,          
    /* Data Configuration */   
    Spi_DataConfig(16U, /* Data Width */
                   SPI_DATA_MSB_FIRST), /* Transfer Start */
        /* EB Channels : SPI112: Max EB Buffers  */
    (Spi_NumberOfDataType)511,
    /* Buffer Type, SPI_CHANNEL_TYPE */
    (uint8)SPI_EB_BUFFER,
  },
/* EB Channel: SpiChannel_VLV_G_DRV Configuration */
  {   
    /* Default Data, SPI_DEFAULT_DATA */               
    (uint32)0x00000000,          
    /* Data Configuration */   
    Spi_DataConfig(16U, /* Data Width */
                   SPI_DATA_MSB_FIRST), /* Transfer Start */
        /* EB Channels : SPI112: Max EB Buffers  */
    (Spi_NumberOfDataType)511,
    /* Buffer Type, SPI_CHANNEL_TYPE */
    (uint8)SPI_EB_BUFFER,
  },
/* EB Channel: SpiChannel_REG Configuration */
  {   
    /* Default Data, SPI_DEFAULT_DATA */               
    (uint32)0x00000000,          
    /* Data Configuration */   
    Spi_DataConfig(16U, /* Data Width */
                   SPI_DATA_MSB_FIRST), /* Transfer Start */
        /* EB Channels : SPI112: Max EB Buffers  */
    (Spi_NumberOfDataType)511,
    /* Buffer Type, SPI_CHANNEL_TYPE */
    (uint8)SPI_EB_BUFFER,
  },
};

/*
                    Container: SpiJobConfiguration
*/
/* Notification Function of SpiJob_SPI_Ref is NULL_PTR */
/* Notification Function of SpiJob_SPI_Ref_1 is NULL_PTR */
/* Notification Function of SpiJob_SPI_Ref_2 is NULL_PTR */
/* Notification Function of SpiJob_ASIC_Ref is NULL_PTR */
/* Notification Function of SpiJob_MPS2_Init is NULL_PTR */
/* Notification Function of SpiJob_MPS2_ErrAng is NULL_PTR */
/* Notification Function of SpiJob_VLV_G_DRV is NULL_PTR */
/* Notification Function of SpiJob_REG is NULL_PTR */


/*
Configuration: Channel Assignment
*/
/*Channel Assignment of Job: SpiJob_SPI_Ref*/
static const Spi_ChannelType SpiJob_SPI_Ref_ChannelLinkPtr[] =
{
  SpiConf_SpiChannel_SpiChannel_SPI_Ref,
  SPI_CHANNEL_LINK_DELIMITER
};
/*
Configuration: Channel Assignment
*/
/*Channel Assignment of Job: SpiJob_SPI_Ref_1*/
static const Spi_ChannelType SpiJob_SPI_Ref_1_ChannelLinkPtr[] =
{
  SpiConf_SpiChannel_SpiChannel_SPI_Ref_1,
  SPI_CHANNEL_LINK_DELIMITER
};
/*
Configuration: Channel Assignment
*/
/*Channel Assignment of Job: SpiJob_SPI_Ref_2*/
static const Spi_ChannelType SpiJob_SPI_Ref_2_ChannelLinkPtr[] =
{
  SpiConf_SpiChannel_SpiChannel_SPI_Ref_2,
  SPI_CHANNEL_LINK_DELIMITER
};
/*
Configuration: Channel Assignment
*/
/*Channel Assignment of Job: SpiJob_ASIC_Ref*/
static const Spi_ChannelType SpiJob_ASIC_Ref_ChannelLinkPtr[] =
{
  SpiConf_SpiChannel_SpiChannel_ASIC_Ref,
  SPI_CHANNEL_LINK_DELIMITER
};
/*
Configuration: Channel Assignment
*/
/*Channel Assignment of Job: SpiJob_MPS2_Init*/
static const Spi_ChannelType SpiJob_MPS2_Init_ChannelLinkPtr[] =
{
  SpiConf_SpiChannel_SpiChannel_MPS2_Init,
  SPI_CHANNEL_LINK_DELIMITER
};
/*
Configuration: Channel Assignment
*/
/*Channel Assignment of Job: SpiJob_MPS2_ErrAng*/
static const Spi_ChannelType SpiJob_MPS2_ErrAng_ChannelLinkPtr[] =
{
  SpiConf_SpiChannel_SpiChannel_MPS2_ErrAng,
  SPI_CHANNEL_LINK_DELIMITER
};
/*
Configuration: Channel Assignment
*/
/*Channel Assignment of Job: SpiJob_VLV_G_DRV*/
static const Spi_ChannelType SpiJob_VLV_G_DRV_ChannelLinkPtr[] =
{
  SpiConf_SpiChannel_SpiChannel_VLV_G_DRV,
  SPI_CHANNEL_LINK_DELIMITER
};
/*
Configuration: Channel Assignment
*/
/*Channel Assignment of Job: SpiJob_REG*/
static const Spi_ChannelType SpiJob_REG_ChannelLinkPtr[] =
{
  SpiConf_SpiChannel_SpiChannel_REG,
  SPI_CHANNEL_LINK_DELIMITER
};

/*
Configuration: Job Configuration Constant Structure.
*/
static const Spi_JobConfigType Spi_kJobConfig0[] =
{

/* Job: SpiJob_SPI_Ref Configuration */
  {
    /* User given name outside the naming convention */
    /* Spi_ChannelLinkPtr */
    SpiJob_SPI_Ref_ChannelLinkPtr, 
    /* Baud Rate (10000000 Hz) Hw configuration Parameters */
    Spi_BaudRateParams(/*TQ*/(0x0U), /*Q*/(0x0U),
              /*A*/(0x3U), /*B*/(0x3U), /*C*/(0x3U)),

    /* Time Delay Configuration */
      (uint32)Spi_DelayParams(/*IPRE*/0x00,/*IDLE*/ 0x01,
        /*LPRE*/0x00, /*LEAD*/0x01, 
        /*TPRE*/0x00, /*TRAIL*/0x01),

    SPI_CS_HW, /* Hw CS Pin is Selected */

    /*CS Active level Polarity*/
    SPI_CS_POLARITY_LOW,

 /* Shift Clock Configuration : Clock Idle Polarity: SPI_SHIFT_CLOCK_IDLE_LEVEL,
                                  Clock Phase: SPI_DATA_SHIFT_EDGE */
    Spi_ShiftClkConfig(SPI_CLK_IDLE_HIGH,
                      SPI_DATA_SHIFT_LEAD),
   /* Job Priority escalated to Maximum as it is mapped 
      to one or more non-interruptible sequence */
    SPI_JOB_PRIORITY_3,

    /* Spi HW Unit. bit[7:4]: Channel no, bit[3:0]: hw module no */      
      (uint8)((uint8)SPI_QSPI_CHANNEL4 << 4U) | (SPI_QSPI1_INDEX),

    /* Channel Based Chip Select */
    (uint8)1U,

  },
/* Job: SpiJob_SPI_Ref_1 Configuration */
  {
    /* User given name outside the naming convention */
    /* Spi_ChannelLinkPtr */
    SpiJob_SPI_Ref_1_ChannelLinkPtr, 
    /* Baud Rate (10000000 Hz) Hw configuration Parameters */
    Spi_BaudRateParams(/*TQ*/(0x0U), /*Q*/(0x0U),
              /*A*/(0x3U), /*B*/(0x3U), /*C*/(0x3U)),

    /* Time Delay Configuration */
      (uint32)Spi_DelayParams(/*IPRE*/0x00,/*IDLE*/ 0x01,
        /*LPRE*/0x00, /*LEAD*/0x01, 
        /*TPRE*/0x00, /*TRAIL*/0x01),

    SPI_CS_HW, /* Hw CS Pin is Selected */

    /*CS Active level Polarity*/
    SPI_CS_POLARITY_LOW,

 /* Shift Clock Configuration : Clock Idle Polarity: SPI_SHIFT_CLOCK_IDLE_LEVEL,
                                  Clock Phase: SPI_DATA_SHIFT_EDGE */
    Spi_ShiftClkConfig(SPI_CLK_IDLE_HIGH,
                      SPI_DATA_SHIFT_LEAD),
   /* Job Priority escalated to Maximum as it is mapped 
      to one or more non-interruptible sequence */
    SPI_JOB_PRIORITY_3,

    /* Spi HW Unit. bit[7:4]: Channel no, bit[3:0]: hw module no */      
      (uint8)((uint8)SPI_QSPI_CHANNEL4 << 4U) | (SPI_QSPI1_INDEX),

    /* Channel Based Chip Select */
    (uint8)1U,

  },
/* Job: SpiJob_SPI_Ref_2 Configuration */
  {
    /* User given name outside the naming convention */
    /* Spi_ChannelLinkPtr */
    SpiJob_SPI_Ref_2_ChannelLinkPtr, 
    /* Baud Rate (10000000 Hz) Hw configuration Parameters */
    Spi_BaudRateParams(/*TQ*/(0x0U), /*Q*/(0x0U),
              /*A*/(0x3U), /*B*/(0x3U), /*C*/(0x3U)),

    /* Time Delay Configuration */
      (uint32)Spi_DelayParams(/*IPRE*/0x00,/*IDLE*/ 0x01,
        /*LPRE*/0x00, /*LEAD*/0x01, 
        /*TPRE*/0x00, /*TRAIL*/0x01),

    SPI_CS_HW, /* Hw CS Pin is Selected */

    /*CS Active level Polarity*/
    SPI_CS_POLARITY_LOW,

 /* Shift Clock Configuration : Clock Idle Polarity: SPI_SHIFT_CLOCK_IDLE_LEVEL,
                                  Clock Phase: SPI_DATA_SHIFT_EDGE */
    Spi_ShiftClkConfig(SPI_CLK_IDLE_HIGH,
                      SPI_DATA_SHIFT_LEAD),
   /* Job Priority escalated to Maximum as it is mapped 
      to one or more non-interruptible sequence */
    SPI_JOB_PRIORITY_3,

    /* Spi HW Unit. bit[7:4]: Channel no, bit[3:0]: hw module no */      
      (uint8)((uint8)SPI_QSPI_CHANNEL4 << 4U) | (SPI_QSPI1_INDEX),

    /* Channel Based Chip Select */
    (uint8)1U,

  },
/* Job: SpiJob_ASIC_Ref Configuration */
  {
    /* User given name outside the naming convention */
    /* Spi_ChannelLinkPtr */
    SpiJob_ASIC_Ref_ChannelLinkPtr, 
    /* Baud Rate (5000000 Hz) Hw configuration Parameters */
    Spi_BaudRateParams(/*TQ*/(0x0U), /*Q*/(0x1U),
              /*A*/(0x3U), /*B*/(0x3U), /*C*/(0x3U)),

    /* Time Delay Configuration */
      (uint32)Spi_DelayParams(/*IPRE*/0x00,/*IDLE*/ 0x01,
        /*LPRE*/0x00, /*LEAD*/0x02, 
        /*TPRE*/0x00, /*TRAIL*/0x04),

    SPI_CS_HW, /* Hw CS Pin is Selected */

    /*CS Active level Polarity*/
    SPI_CS_POLARITY_LOW,

 /* Shift Clock Configuration : Clock Idle Polarity: SPI_SHIFT_CLOCK_IDLE_LEVEL,
                                  Clock Phase: SPI_DATA_SHIFT_EDGE */
    Spi_ShiftClkConfig(SPI_CLK_IDLE_LOW,
                      SPI_DATA_SHIFT_TRAIL),
   /* Job Priority escalated to Maximum as it is mapped 
      to one or more non-interruptible sequence */
    SPI_JOB_PRIORITY_3,

    /* Spi HW Unit. bit[7:4]: Channel no, bit[3:0]: hw module no */      
      (uint8)((uint8)SPI_QSPI_CHANNEL2 << 4U) | (SPI_QSPI0_INDEX),

    /* Channel Based Chip Select */
    (uint8)1U,

  },
/* Job: SpiJob_MPS2_Init Configuration */
  {
    /* User given name outside the naming convention */
    /* Spi_ChannelLinkPtr */
    SpiJob_MPS2_Init_ChannelLinkPtr, 
    /* Baud Rate (10000000 Hz) Hw configuration Parameters */
    Spi_BaudRateParams(/*TQ*/(0x0U), /*Q*/(0x0U),
              /*A*/(0x3U), /*B*/(0x3U), /*C*/(0x3U)),

    /* Time Delay Configuration */
      (uint32)Spi_DelayParams(/*IPRE*/0x00,/*IDLE*/ 0x01,
        /*LPRE*/0x00, /*LEAD*/0x01, 
        /*TPRE*/0x00, /*TRAIL*/0x01),

    SPI_CS_HW, /* Hw CS Pin is Selected */

    /*CS Active level Polarity*/
    SPI_CS_POLARITY_LOW,

 /* Shift Clock Configuration : Clock Idle Polarity: SPI_SHIFT_CLOCK_IDLE_LEVEL,
                                  Clock Phase: SPI_DATA_SHIFT_EDGE */
    Spi_ShiftClkConfig(SPI_CLK_IDLE_HIGH,
                      SPI_DATA_SHIFT_LEAD),
   /* Job Priority escalated to Maximum as it is mapped 
      to one or more non-interruptible sequence */
    SPI_JOB_PRIORITY_3,

    /* Spi HW Unit. bit[7:4]: Channel no, bit[3:0]: hw module no */      
      (uint8)((uint8)SPI_QSPI_CHANNEL0 << 4U) | (SPI_QSPI2_INDEX),

    /* Channel Based Chip Select */
    (uint8)1U,

  },
/* Job: SpiJob_MPS2_ErrAng Configuration */
  {
    /* User given name outside the naming convention */
    /* Spi_ChannelLinkPtr */
    SpiJob_MPS2_ErrAng_ChannelLinkPtr, 
    /* Baud Rate (10000000 Hz) Hw configuration Parameters */
    Spi_BaudRateParams(/*TQ*/(0x0U), /*Q*/(0x0U),
              /*A*/(0x3U), /*B*/(0x3U), /*C*/(0x3U)),

    /* Time Delay Configuration */
      (uint32)Spi_DelayParams(/*IPRE*/0x00,/*IDLE*/ 0x01,
        /*LPRE*/0x00, /*LEAD*/0x01, 
        /*TPRE*/0x00, /*TRAIL*/0x01),

    SPI_CS_HW, /* Hw CS Pin is Selected */

    /*CS Active level Polarity*/
    SPI_CS_POLARITY_LOW,

 /* Shift Clock Configuration : Clock Idle Polarity: SPI_SHIFT_CLOCK_IDLE_LEVEL,
                                  Clock Phase: SPI_DATA_SHIFT_EDGE */
    Spi_ShiftClkConfig(SPI_CLK_IDLE_HIGH,
                      SPI_DATA_SHIFT_LEAD),
   /* Job Priority escalated to Maximum as it is mapped 
      to one or more non-interruptible sequence */
    SPI_JOB_PRIORITY_3,

    /* Spi HW Unit. bit[7:4]: Channel no, bit[3:0]: hw module no */      
      (uint8)((uint8)SPI_QSPI_CHANNEL0 << 4U) | (SPI_QSPI2_INDEX),

    /* Channel Based Chip Select */
    (uint8)1U,

  },
/* Job: SpiJob_VLV_G_DRV Configuration */
  {
    /* User given name outside the naming convention */
    /* Spi_ChannelLinkPtr */
    SpiJob_VLV_G_DRV_ChannelLinkPtr, 
    /* Baud Rate (5000000 Hz) Hw configuration Parameters */
    Spi_BaudRateParams(/*TQ*/(0x0U), /*Q*/(0x1U),
              /*A*/(0x3U), /*B*/(0x3U), /*C*/(0x3U)),

    /* Time Delay Configuration */
      (uint32)Spi_DelayParams(/*IPRE*/0x00,/*IDLE*/ 0x01,
        /*LPRE*/0x00, /*LEAD*/0x01, 
        /*TPRE*/0x00, /*TRAIL*/0x01),

    SPI_CS_HW, /* Hw CS Pin is Selected */

    /*CS Active level Polarity*/
    SPI_CS_POLARITY_LOW,

 /* Shift Clock Configuration : Clock Idle Polarity: SPI_SHIFT_CLOCK_IDLE_LEVEL,
                                  Clock Phase: SPI_DATA_SHIFT_EDGE */
    Spi_ShiftClkConfig(SPI_CLK_IDLE_HIGH,
                      SPI_DATA_SHIFT_LEAD),
   /* Job Priority escalated to Maximum as it is mapped 
      to one or more non-interruptible sequence */
    SPI_JOB_PRIORITY_3,

    /* Spi HW Unit. bit[7:4]: Channel no, bit[3:0]: hw module no */      
      (uint8)((uint8)SPI_QSPI_CHANNEL10 << 4U) | (SPI_QSPI3_INDEX),

    /* Channel Based Chip Select */
    (uint8)1U,

  },
/* Job: SpiJob_REG Configuration */
  {
    /* User given name outside the naming convention */
    /* Spi_ChannelLinkPtr */
    SpiJob_REG_ChannelLinkPtr, 
    /* Baud Rate (5000000 Hz) Hw configuration Parameters */
    Spi_BaudRateParams(/*TQ*/(0x0U), /*Q*/(0x1U),
              /*A*/(0x3U), /*B*/(0x3U), /*C*/(0x3U)),

    /* Time Delay Configuration */
      (uint32)Spi_DelayParams(/*IPRE*/0x01,/*IDLE*/ 0x01,
        /*LPRE*/0x00, /*LEAD*/0x01, 
        /*TPRE*/0x00, /*TRAIL*/0x01),

    SPI_CS_HW, /* Hw CS Pin is Selected */

    /*CS Active level Polarity*/
    SPI_CS_POLARITY_LOW,

 /* Shift Clock Configuration : Clock Idle Polarity: SPI_SHIFT_CLOCK_IDLE_LEVEL,
                                  Clock Phase: SPI_DATA_SHIFT_EDGE */
    Spi_ShiftClkConfig(SPI_CLK_IDLE_LOW,
                      SPI_DATA_SHIFT_LEAD),
   /* Job Priority escalated to Maximum as it is mapped 
      to one or more non-interruptible sequence */
    SPI_JOB_PRIORITY_3,

    /* Spi HW Unit. bit[7:4]: Channel no, bit[3:0]: hw module no */      
      (uint8)((uint8)SPI_QSPI_CHANNEL6 << 4U) | (SPI_QSPI3_INDEX),

    /* Channel Based Chip Select */
    (uint8)1U,

  }
};

/* 
                     Container: Spi_SequenceConfiguration
*/
/* Notification Function of Sequence: SpiSequence_0 is NULL_PTR */
/* Notification Function of Sequence: SpiSequence_1 is NULL_PTR */
/* Notification Function of Sequence: SpiSequence_2 is NULL_PTR */
/* Notification Function of Sequence: SpiSequence_ASIC is NULL_PTR */
/* Notification Function of Sequence: SpiSequence_MPS2_Init is NULL_PTR */
/* Notification Function of Sequence: SpiSequence_MPS2_ErrAng is NULL_PTR */
/* Notification Function of Sequence: SpiSequence_VLV_G_DRV is NULL_PTR */
/* Notification Function of Sequence: SpiSequence_REG is NULL_PTR */

/*
Configuration: Job Assignment
*/
/* Job Assignment of Sequence: SpiSequence_0 */
static const Spi_JobType SpiSequence_0_JobLinkPtr[] =
{
  SpiConf_SpiJob_SpiJob_SPI_Ref,
  SPI_JOB_LINK_DELIMITER
};
/*
Configuration: Job Assignment
*/
/* Job Assignment of Sequence: SpiSequence_1 */
static const Spi_JobType SpiSequence_1_JobLinkPtr[] =
{
  SpiConf_SpiJob_SpiJob_SPI_Ref_1,
  SPI_JOB_LINK_DELIMITER
};
/*
Configuration: Job Assignment
*/
/* Job Assignment of Sequence: SpiSequence_2 */
static const Spi_JobType SpiSequence_2_JobLinkPtr[] =
{
  SpiConf_SpiJob_SpiJob_SPI_Ref_2,
  SPI_JOB_LINK_DELIMITER
};
/*
Configuration: Job Assignment
*/
/* Job Assignment of Sequence: SpiSequence_ASIC */
static const Spi_JobType SpiSequence_ASIC_JobLinkPtr[] =
{
  SpiConf_SpiJob_SpiJob_ASIC_Ref,
  SPI_JOB_LINK_DELIMITER
};
/*
Configuration: Job Assignment
*/
/* Job Assignment of Sequence: SpiSequence_MPS2_Init */
static const Spi_JobType SpiSequence_MPS2_Init_JobLinkPtr[] =
{
  SpiConf_SpiJob_SpiJob_MPS2_Init,
  SPI_JOB_LINK_DELIMITER
};
/*
Configuration: Job Assignment
*/
/* Job Assignment of Sequence: SpiSequence_MPS2_ErrAng */
static const Spi_JobType SpiSequence_MPS2_ErrAng_JobLinkPtr[] =
{
  SpiConf_SpiJob_SpiJob_MPS2_ErrAng,
  SPI_JOB_LINK_DELIMITER
};
/*
Configuration: Job Assignment
*/
/* Job Assignment of Sequence: SpiSequence_VLV_G_DRV */
static const Spi_JobType SpiSequence_VLV_G_DRV_JobLinkPtr[] =
{
  SpiConf_SpiJob_SpiJob_VLV_G_DRV,
  SPI_JOB_LINK_DELIMITER
};
/*
Configuration: Job Assignment
*/
/* Job Assignment of Sequence: SpiSequence_REG */
static const Spi_JobType SpiSequence_REG_JobLinkPtr[] =
{
  SpiConf_SpiJob_SpiJob_REG,
  SPI_JOB_LINK_DELIMITER
};
/*
Configuration: Sequence Configuration Constant Structure.
*/
static const Spi_SequenceConfigType Spi_kSequenceConfig0[] =
{   /* Sequence: SpiSequence_0 Configuration */
  {
    /* User given name outside the naming convention */    /* Spi_JobLinkPtr */
    SpiSequence_0_JobLinkPtr,
     /* This holds the total number of jobs linked to this sequence */
    1U,
  },
  /* Sequence: SpiSequence_1 Configuration */
  {
    /* User given name outside the naming convention */    /* Spi_JobLinkPtr */
    SpiSequence_1_JobLinkPtr,
     /* This holds the total number of jobs linked to this sequence */
    1U,
  },
  /* Sequence: SpiSequence_2 Configuration */
  {
    /* User given name outside the naming convention */    /* Spi_JobLinkPtr */
    SpiSequence_2_JobLinkPtr,
     /* This holds the total number of jobs linked to this sequence */
    1U,
  },
  /* Sequence: SpiSequence_ASIC Configuration */
  {
    /* User given name outside the naming convention */    /* Spi_JobLinkPtr */
    SpiSequence_ASIC_JobLinkPtr,
     /* This holds the total number of jobs linked to this sequence */
    1U,
  },
  /* Sequence: SpiSequence_MPS2_Init Configuration */
  {
    /* User given name outside the naming convention */    /* Spi_JobLinkPtr */
    SpiSequence_MPS2_Init_JobLinkPtr,
     /* This holds the total number of jobs linked to this sequence */
    1U,
  },
  /* Sequence: SpiSequence_MPS2_ErrAng Configuration */
  {
    /* User given name outside the naming convention */    /* Spi_JobLinkPtr */
    SpiSequence_MPS2_ErrAng_JobLinkPtr,
     /* This holds the total number of jobs linked to this sequence */
    1U,
  },
  /* Sequence: SpiSequence_VLV_G_DRV Configuration */
  {
    /* User given name outside the naming convention */    /* Spi_JobLinkPtr */
    SpiSequence_VLV_G_DRV_JobLinkPtr,
     /* This holds the total number of jobs linked to this sequence */
    1U,
  },
  /* Sequence: SpiSequence_REG Configuration */
  {
    /* User given name outside the naming convention */    /* Spi_JobLinkPtr */
    SpiSequence_REG_JobLinkPtr,
     /* This holds the total number of jobs linked to this sequence */
    1U,
  }
};



static const Spi_HWModuleConfigType Spi_kModuleConfig0[]=
{
  /* QSPI0 Module */  
  {
      /*Clock Settings:Sleep Control Disabled*/
      SPI_CLK_SLEEP_DISABLE,
      SPI_QSPI0_MRIS_SEL,
      NULL_PTR,
  },
  /* QSPI1 Module */  
  {
      /*Clock Settings:Sleep Control Disabled*/
      SPI_CLK_SLEEP_DISABLE,
      SPI_QSPI1_MRIS_SEL,    
      NULL_PTR, 
  },
  /* QSPI2 Module */  
  {
      /*Clock Settings:Sleep Control Disabled*/
      SPI_CLK_SLEEP_DISABLE,
      SPI_QSPI2_MRIS_SEL,    
      NULL_PTR,      
  },
  /* QSPI3 Module */  
  {
      /*Clock Settings:Sleep Control Disabled*/
      SPI_CLK_SLEEP_DISABLE,
      SPI_QSPI3_MRIS_SEL,    
      NULL_PTR,      
  },
};


const Spi_ConfigType Spi_ConfigRoot[1] =
{
  {
    Spi_kChannelConfig0,
    Spi_kJobConfig0,
    Spi_kSequenceConfig0,
    {
      &Spi_kModuleConfig0[0],     
      &Spi_kModuleConfig0[1],
      &Spi_kModuleConfig0[2],
      &Spi_kModuleConfig0[3],
    },
    (Spi_ChannelType)(sizeof(Spi_kChannelConfig0) / \
                      sizeof(Spi_ChannelConfigType)),
    (Spi_JobType)(sizeof(Spi_kJobConfig0) / sizeof(Spi_JobConfigType)),
    (Spi_SequenceType)(sizeof(Spi_kSequenceConfig0) / \
                                        sizeof(Spi_SequenceConfigType)),
  }
};


#define SPI_STOP_SEC_POSTBUILDCFG
#include "MemMap.h"

/*******************************************************************************
**                      Global Variable Definitions                           **
*******************************************************************************/
/*******************************************************************************
**                      Private Constant Definitions                          **
*******************************************************************************/
/*******************************************************************************
**                      Private Variable Definitions                          **
*******************************************************************************/
/*******************************************************************************
**                      Global Function Definitions                           **
*******************************************************************************/
/*******************************************************************************
**                      Private Function Definitions                          **
*******************************************************************************/
/* General Notes */
/*
SPI095: The code file structure shall not be defined within this specification
completely. At this point it shall be pointed out that the code-file structure 
shall include the following file named:
- Spi_Lcfg.c ?for link time and for post-build configurable parameters and
- Spi_PBcfg.c ?for post build time configurable parameters.
These files shall contain all link time and post-build time configurable 
parameters.
This file shall contain all link time and post-build time configurable 
parameters.
For the implementation of VariantPC, the implementation stores the 
pre compile time parameters that have to be defined as constants in this file.

SPI123: In this configuration, all Sequences declared are considered as Non
Interruptible Sequences. That means, their dedicated parameter
SPI_INTERRUPTIBLE_SEQUENCE (see SPI064 & SPI106) could be omitted or the
FALSE value should be used as default.

*/


