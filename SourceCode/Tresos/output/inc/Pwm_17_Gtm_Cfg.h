/*******************************************************************************
**                                                                            **
** Copyright (C) Infineon Technologies (2013)                                 **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to Infineon       **
** Technologies. Passing on and copying of this document, and communication   **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  $FILENAME  : Pwm_17_Gtm_Cfg.h  $                                          **
**                                                                            **
**   $CC VERSION : \main\26 $                                                 **
**                                                                            **
**  DATE, TIME: 2015-07-13, 13:37:26                                          **
**                                                                            **
**  GENERATOR : Build b120625-0327                                            **
**                                                                            **
**   AUTHOR    : DL-AUTOSAR-Engineering                                       **
**                                                                            **
**   VENDOR    : Infineon Technologies                                        **
**                                                                            **
**  DESCRIPTION  : PWM configuration generated out of ECU configuration       **
**                   file                                                     **
**                                                                            **
**   MAY BE CHANGED BY USER [yes/no]: No                                     **
**                                                                            **
*******************************************************************************/

#ifndef PWMCFG_H
#define PWMCFG_H

/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/

/*******************************************************************************
**                      Global Macro Definitions                              **
*******************************************************************************/
#define PWM_17_GTM_AR_RELEASE_MAJOR_VERSION  (4U)
#define PWM_17_GTM_AR_RELEASE_MINOR_VERSION  (0U)
#define PWM_17_GTM_AR_RELEASE_REVISION_VERSION  (3U)

#define PWM_17_GTM_SW_MAJOR_VERSION  (4U)
#define PWM_17_GTM_SW_MINOR_VERSION  (0U)
#define PWM_17_GTM_SW_PATCH_VERSION  (2U)


/*
                    Container:PwmGeneral
*/
/* Instance ID for PWM_17_Gtm module */
#define PWM_17_GTM_INSTANCE_ID         ((uint8)0U)

/*
PWM075: Pre Compile time parameters are placed in Pwm_Cfg.h
*/
/*
  Configuration: PWM_DEV_ERROR_DETECT : PWM064 PWM078
  Configuration of Development Error Detection 
  PWM064: API Parameter Checking is enabled if this compiler
  switch is STD_ON.
  PWM078: All errors are reported to Det_ReportError if the
  switch is STD_ON.
  Adds/removes the Development error detection 
  from the code 
  - if STD_ON, Development error detection is enabled
  - if STD_OFF, Development error detection is disabled
*/
#define PWM_DEV_ERROR_DETECT                 (STD_OFF)


/*
  Configuration: PWM_DE_INIT_API
  Configuration of Pwm_DeInit API
  Adds/removes the service Pwm_DeInit() 
  from the code 
  - if STD_ON, Pwm_DeInit() can be used
  - if STD_OFF, Pwm_DeInit() cannot be used
*/
#define PWM_DE_INIT_API                      (STD_ON)


/*
  Configuration: PWM_GET_OUTPUT_STATE_API
  Configuration of Pwm_Get_Output_State_Api
  Adds/removes the service Pwm_GetOutputState() 
  from the code 
  - if STD_ON, Pwm_GetOutputState() can be used
  - if STD_OFF, Pwm_GetOutputState() cannot be used
*/
#define PWM_GET_OUTPUT_STATE_API                       (STD_OFF)


/*
  Configuration: PWM_SET_DUTY_CYCLE_API
  Configuration of Pwm_Set_Duty_Cycle_Api
  Adds/removes the service Pwm_GetOutputState() 
  from the code 
  - if STD_ON, Pwm_SetDutyCycle() can be used
  - if STD_OFF,Pwm_SetDutyCycle() cannot be used
*/
#define PWM_SET_DUTY_CYCLE_API                       (STD_ON)


/*
  Configuration: PWM_SET_OUTPUT_TO_IDLE_API
  Configuration of Pwm_Set_Output_To_Idle_Api
  Adds/removes the service Pwm_SetOutputToIdle() 
  from the code 
  - if STD_ON, Pwm_SetOutputToIdle() can be used
  - if STD_OFF,Pwm_SetOutputToIdle() cannot be used
*/
#define PWM_SET_OUTPUT_TO_IDLE_API                       (STD_OFF)


/*
  Configuration: PWM_SET_PERIOD_AND_DUTY_API
  Configuration of Pwm_Set_Period_And_Duty_Api
  Adds/removes the service Pwm_SetPeriodAndDuty() 
  from the code 
  - if STD_ON, Pwm_SetPeriodAndDuty() can be used
  - if STD_OFF,Pwm_SetPeriodAndDuty() cannot be used
*/
#define PWM_SET_PERIOD_AND_DUTY_API                       (STD_OFF)


/*
  Enables/Disables the Safety features 
  - if STD_ON, Safety features are enabled
  - if STD_OFF, Safety features are disabled
*/

#define PWM_SAFETY_ENABLE                 (STD_OFF)

/*
  Configuration: PWM_NOTIFICATION_SUPPORTED
  Adds/removes the service Pwm_EnableNotification()
  and Pwm_DisableNotification() from the code 
  - if STD_ON, Notification API's can be used
  - if STD_OFF, Notification API's cannot be used
*/
#define PWM_NOTIFICATION_SUPPORTED           (STD_ON)



#define PWM_DUTYCYCLE_UPDATED_ENDPERIOD      (STD_OFF)


#define PWM_DUTY_PERIOD_UPDATED_ENDPERIOD    (STD_OFF)

/*
  Configuration: PWM_VERSION_INFO_API
  Adds/removes the service Pwm_GetVersionInfo() 
  from the code 
  - if STD_ON, Pwm_GetVersionInfo() can be used
  - if STD_OFF, Pwm_GetVersionInfo() cannot be used
*/
#define PWM_VERSION_INFO_API                 (STD_OFF)


/* Configuration : PwmPBFixedAddress
   This parameter enables the user to switch STD_ON/STD_OFF the
   PostBuild Fixed Address Feature  
*/
#define PWM_PB_FIXEDADDR           (STD_OFF)


/* Configuration : PwmDutyShiftInTicks
   This parameter enables the user to enter the duty cycle and 
   shift value in absolute ticks, instead of percentage
*/
#define PWM_DUTY_SHIFT_IN_TICKS     (STD_ON)


/* Configured PWM Channels Symbolic Names */
#ifndef Pwm_17_GtmConf_PwmChannel_Simple_PWM
#define Pwm_17_GtmConf_PwmChannel_Simple_PWM          ((Pwm_17_Gtm_ChannelType)0)
#endif
#ifndef Pwm_17_GtmConf_PwmChannel_Simple_PWM_2
#define Pwm_17_GtmConf_PwmChannel_Simple_PWM_2          ((Pwm_17_Gtm_ChannelType)1)
#endif
#ifndef Pwm_17_GtmConf_PwmChannel_MTR_Center_REF_PWM
#define Pwm_17_GtmConf_PwmChannel_MTR_Center_REF_PWM          ((Pwm_17_Gtm_ChannelType)2)
#endif
#ifndef Pwm_17_GtmConf_PwmChannel_U_HIGH
#define Pwm_17_GtmConf_PwmChannel_U_HIGH          ((Pwm_17_Gtm_ChannelType)3)
#endif
#ifndef Pwm_17_GtmConf_PwmChannel_U_LOW
#define Pwm_17_GtmConf_PwmChannel_U_LOW          ((Pwm_17_Gtm_ChannelType)4)
#endif
#ifndef Pwm_17_GtmConf_PwmChannel_V_HIGH
#define Pwm_17_GtmConf_PwmChannel_V_HIGH          ((Pwm_17_Gtm_ChannelType)5)
#endif
#ifndef Pwm_17_GtmConf_PwmChannel_V_LOW
#define Pwm_17_GtmConf_PwmChannel_V_LOW          ((Pwm_17_Gtm_ChannelType)6)
#endif
#ifndef Pwm_17_GtmConf_PwmChannel_W_HIGH
#define Pwm_17_GtmConf_PwmChannel_W_HIGH          ((Pwm_17_Gtm_ChannelType)7)
#endif
#ifndef Pwm_17_GtmConf_PwmChannel_W_LOW
#define Pwm_17_GtmConf_PwmChannel_W_LOW          ((Pwm_17_Gtm_ChannelType)8)
#endif
#ifndef Pwm_17_GtmConf_PwmChannel_Valve_Center_REF_PWM_ADC_TrigRef
#define Pwm_17_GtmConf_PwmChannel_Valve_Center_REF_PWM_ADC_TrigRef          ((Pwm_17_Gtm_ChannelType)9)
#endif
#ifndef Pwm_17_GtmConf_PwmChannel_Valve_TOM1_Ch08
#define Pwm_17_GtmConf_PwmChannel_Valve_TOM1_Ch08          ((Pwm_17_Gtm_ChannelType)18)
#endif
#ifndef Pwm_17_GtmConf_PwmChannel_Valve_TOM1_Ch09
#define Pwm_17_GtmConf_PwmChannel_Valve_TOM1_Ch09          ((Pwm_17_Gtm_ChannelType)19)
#endif
#ifndef Pwm_17_GtmConf_PwmChannel_Valve_TOM1_Ch11
#define Pwm_17_GtmConf_PwmChannel_Valve_TOM1_Ch11          ((Pwm_17_Gtm_ChannelType)10)
#endif
#ifndef Pwm_17_GtmConf_PwmChannel_Valve_TOM1_Ch12
#define Pwm_17_GtmConf_PwmChannel_Valve_TOM1_Ch12          ((Pwm_17_Gtm_ChannelType)11)
#endif
#ifndef Pwm_17_GtmConf_PwmChannel_Valve_TOM1_Ch13
#define Pwm_17_GtmConf_PwmChannel_Valve_TOM1_Ch13          ((Pwm_17_Gtm_ChannelType)12)
#endif
#ifndef Pwm_17_GtmConf_PwmChannel_Valve_TOM1_Ch14
#define Pwm_17_GtmConf_PwmChannel_Valve_TOM1_Ch14          ((Pwm_17_Gtm_ChannelType)13)
#endif
#ifndef Pwm_17_GtmConf_PwmChannel_Valve_TOM1_Ch15
#define Pwm_17_GtmConf_PwmChannel_Valve_TOM1_Ch15          ((Pwm_17_Gtm_ChannelType)14)
#endif
#ifndef Pwm_17_GtmConf_PwmChannel_MOC_RL_PWM
#define Pwm_17_GtmConf_PwmChannel_MOC_RL_PWM          ((Pwm_17_Gtm_ChannelType)15)
#endif
#ifndef Pwm_17_GtmConf_PwmChannel_MOC_RR_PWM
#define Pwm_17_GtmConf_PwmChannel_MOC_RR_PWM          ((Pwm_17_Gtm_ChannelType)16)
#endif
#ifndef Pwm_17_GtmConf_PwmChannel_WD
#define Pwm_17_GtmConf_PwmChannel_WD          ((Pwm_17_Gtm_ChannelType)17)
#endif

/*
   Configuration:Max channels configured for Pwm
   Maximum Channels is calculated based on the number of channels configured for each configuration.
   For instance if configuration set 1 has 5 channels and configuration set 2 has
   7 channels then the Maxchannels is 7.
*/


#define PWM_MAX_CHANNELS           ((Pwm_17_Gtm_ChannelType)20)

#define PWM_MAX_QM_CHANNELS    (20)

/*
   Derived pre compile switches for optimization
   purpose
*/

#define PWM_VARIABLE_PERIOD_USED                     (STD_OFF)
#define PWM_FIXED_PERIOD_USED                        (STD_ON)
#define PWM_FIXED_PERIOD_SHIFTED_USED                (STD_OFF)
#define PWM_USED_FIXED_PERIOD_CENTER_ALIGNED         (STD_ON)

/*******************************************************************************
**                      Global Constant Declarations                          **
*******************************************************************************/

#endif
