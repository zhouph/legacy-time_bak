/*******************************************************************************
**                                                                            **
** Copyright (C) Infineon Technologies (2014)                                 **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to Infineon       **
** Technologies. Passing on and copying of this document, and communication   **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME  : Sent_Cfg.h                                                    **
**                                                                            **
**  $CC VERSION : \main\13 $                                                 **
**                                                                            **
**  DATE, TIME: 2015-07-13, 13:37:20                                          **
**                                                                            **
**  GENERATOR : Build b120625-0327                                            **
**                                                                            **
**  AUTHOR    : DL-AUTOSAR-Engineering                                        **
**                                                                            **
**  VENDOR    : Infineon Technologies                                         **
**                                                                            **
**  DESCRIPTION  : SENT configuration generated out of ECU configuration      **
**                 file (Sent.bmd)                                            **
**                                                                            **
**  MAY BE CHANGED BY USER [yes/no]: no                                       **
**                                                                            **
*******************************************************************************/
#ifndef SENT_CFG_H
#define SENT_CFG_H
/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/
/* SENT Type definitions */
#include "Sent_Types.h"
/*******************************************************************************
**                      Global Macro Definitions                              **
*******************************************************************************/
#define SENT_SW_MAJOR_VERSION              (0U)
#define SENT_SW_MINOR_VERSION              (0U)
#define SENT_SW_PATCH_VERSION              (3U)


/*
                     Container : SentDriverGeneralConfiguration
*/
/*
  The following macros will enable or disable a particular feature 
  in SENT module.
  Set the macro to ON to enable the feature and OFF to disable the same.
*/
/*
Configuration: SENT_DEV_ERROR_DETECT
Preprocessor switch for enabling the development error detection and 
reporting. 
- if STD_ON, DET is Enabled 
- if STD_OFF,DET is Disabled 
*/
#define SENT_DEV_ERROR_DETECT  (STD_ON)

/* Configuration: SENT_DEINIT_API
Sent_DeInit API configuration 
- if STD_ON, DeInit API is Enabled 
- if STD_OFF, DeInit API is Disabled 
*/
#define SENT_DEINIT_API        (STD_OFF)

/* Configuration: SENT_VERSION_INFO_API
Version Information API configuration 
- if STD_ON, VersionInfo API is Enabled 
- if STD_OFF, VersionInfo API is Disabled 
*/
#define SENT_VERSION_INFO_API  (STD_OFF)

/* Configuration: SENT_SPC_USED
SENT SPC Feature configuration 
- if STD_ON, SPC feature is Enabled 
- if STD_OFF, SPC feature is Disabled 
*/
#define SENT_SPC_USED         (STD_OFF)

/* Configuration: SENT_PB_FIXED_ADDRESS (Vendor specific)
Fixed address confiuration as per HIS recommendatons
- if STD_ON, ConfigRoot[0] is used by default as the fixed configuration
- if STD_OFF, Dynamic usage of the selected configurations according to AUTOSAR 
*/
#define SENT_PB_FIXED_ADDRESS (STD_OFF)

/* Configuration: SENT_ACCEN_MODE
SENT_ACCEN_DEFAULT - No ACCEN register access; default HW value
SENT_ACCEN_INIT - ACCEN register access only at initialization
SENT_ACCEN_API - ACCEN register access only through API  
*/
#define SENT_ACCEN_MODE       (SENT_ACCEN_DEFAULT)

/* Configuration: SENT_HW_MAX_CHANNELS
Maximum number of SENT physical channels supported
*/
#define SENT_HW_MAX_CHANNELS   (10U)

/* Configuration: SENT MODULE INSTANCE ID */
#define SENT_INSTANCE_ID       ((uint8)0)

/* Total no. of config sets */
#define SENT_CONFIG_COUNT    (1U)

/*******************************************************************************
**                      Global Macro Definitions                              **
*******************************************************************************/

/*******************************************************************************
**                      Global Type Definitions                               **
*******************************************************************************/

/*******************************************************************************
**                      Global Constant Declarations                          **
*******************************************************************************/

/*******************************************************************************
**                      Global Variable Declarations                          **
*******************************************************************************/

/*******************************************************************************
**                      Global Function Declarations                          **
*******************************************************************************/

/*IFX_MISRA_RULE_08_07_STATUS=SentNotifFnPtrArray variable 
is used across multiple files it is defined in generated Sent_LCfg.c file and   
accessed in Sent.c,hence it cannot be declared as a local variable*/
extern const Sent_NotifFnPtrType SentNotifFnPtrArray[3];

/* ConfigSet 0 */
/* Application callout function for SENT Channel6 */
extern void SentChannel0_CallOut (Sent_ChannelIdxType ChannelId, 
                                                Sent_NotifType Stat);
/* Application callout function for SENT Channel8 */
extern void SentChannel1_CallOut (Sent_ChannelIdxType ChannelId, 
                                                Sent_NotifType Stat);
/* Application callout function for SENT Channel9 */
extern void SentChannel2_CallOut (Sent_ChannelIdxType ChannelId, 
                                                Sent_NotifType Stat);
/*******************************************************************************
**                      Global Inline Function Definitions                    **
*******************************************************************************/
#endif  /* SENT_CFG_H */
