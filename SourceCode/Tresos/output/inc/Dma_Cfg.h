/******************************************************************************
**                                                                           **
** Copyright (C) Infineon Technologies (2013)                                **
**                                                                           **
** All rights reserved.                                                      **
**                                                                           **
** This document contains proprietary information belonging to Infineon      **
** Technologies. Passing on and copying of this document, and communication  **
** of its contents is not permitted without prior written authorization.     **
**                                                                           **
*******************************************************************************
**                                                                           **
**  FILENAME  : Dma_Cfg.h                                                    **
**                                                                           **
**  $CC VERSION : \main\8 $                                                  **
**                                                                           **
**  DATE, TIME: 2015-07-13, 13:37:28                                         **
**                                                                           **
**  GENERATOR : Build b120625-0327                                           **
**                                                                           **
**  AUTHOR    : DL-AUTOSAR-Engineering                                       **
**                                                                           **
**  VENDOR    : Infineon Technologies                                        **
**                                                                           **
**  DESCRIPTION  : DMA configuration generated out of ECU configuration      **
**                 file (Dma.bmd/.xdm)                                       **
**                                                                           **
**  MAY BE CHANGED BY USER [yes/no]: No                                      **
**                                                                           **
******************************************************************************/
/******************************************************************************
**                                                                           **
    TRACEBILITY :[cover parentID = DS_NAS_PR446,ASW:1500,ASW:1801,
                  ASW:1501,ASW:1504,ASW:1507,ASW:1508,ASW:1512,ASW:1513,
                  ASW:1514,ASW:1516,ASW:1517,ASW:1519,ASW:1523,ASW:1524,
                  ASW:1528][/cover]                                          **
******************************************************************************/

#ifndef DMA_CFG_H 
#define DMA_CFG_H 

/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/

/*******************************************************************************
**                      Global Macro Definitions                              **
*******************************************************************************/

#define DMA_VENDOR_ID                   ((uint16)17U)
#define DMA_MODULE_ID                   ((uint16)255U)
#define DMA_MODULE_INSTANCE             ((uint8)3U)

/*SW Version Information*/
#define DMA_SW_MAJOR_VERSION            (1U)
#define DMA_SW_MINOR_VERSION            (0U)
#define DMA_SW_PATCH_VERSION            (4U)

/*Number of DMA channels present in the controller*/
#define DMA_NUM_OF_CHANNELS             (64U)

/* Derived Configuration for DmaDevErrorDetect */
#define DMA_DEV_ERROR_DETECT            (STD_OFF)

/* Derived Configuration for DmaVersionInfoApi */
#define DMA_VERSION_INFO_API            (STD_OFF)

/* Derived Configuration for DmaDoubleBufferEnable */
#define DMA_DOUBLE_BUFFER_ENABLE      (STD_OFF)

/* Derived Configuration for DmaDeInitApi */
#define DMA_DEINIT_API                  (STD_OFF)

/* Derived Configuration for DmaLinkedListEnable*/
#define DMA_LINKED_LIST_ENABLE          (STD_OFF)

/* Derived Configuration DmaDebugSupport*/
#define DMA_DEBUG_SUPPORT               (STD_OFF)

/* Derived Configuration DmaPBFixedAddress*/
#define DMA_PB_FIXEDADDR                (STD_OFF)



/*******************************************************************************
**                      Global Type Definitions                               **
*******************************************************************************/

/*******************************************************************************
**                      Global Constant Declarations                          **
*******************************************************************************/

/*******************************************************************************
**                      Global Variable Declarations                          **
*******************************************************************************/

/*******************************************************************************
**                      Global Function Declarations                          **
*******************************************************************************/

/*******************************************************************************
**                      Global Inline Function Definitions                    **
*******************************************************************************/

#endif  /*End of DMA_CFG_H */


