/******************************************************************************
**                                                                           **
** Copyright (C) Infineon Technologies (2013)                                **
**                                                                           **
** All rights reserved.                                                      **
**                                                                           **
** This document contains proprietary information belonging to Infineon      **
** Technologies. Passing on and copying of this document, and communication  **
** of its contents is not permitted without prior written authorization.     **
**                                                                           **
*******************************************************************************
**                                                                           **
**  $FILENAME   : Fee_Cfg.h $                                                **
**                                                                           **
**  $CC VERSION : \main\16 $                                                 **
**                                                                           **
**  DATE, TIME: 2015-07-13, 13:37:27                                         **
**                                                                           **
**  GENERATOR : Build b120625-0327                                           **
**                                                                           **
**  AUTHOR    : DL-AUTOSAR-Engineering                                       **
**                                                                           **
**  VENDOR    : Infineon Technologies                                        **
**                                                                           **
**  DESCRIPTION  : FEE configuration generated out of ECU configuration      **
**                   file (Fee.bmd)                                          **
**                                                                           **
**  MAY BE CHANGED BY USER [yes/no]: No                                      **
**                                                                           **
******************************************************************************/

#ifndef  FEE_CFG_H
#define  FEE_CFG_H

/*******************************************************************************
**                      Include Section                                       **
*******************************************************************************/

/* Typedefs Imported from Memory Abstract Interface */ 
#include "MemIf_Types.h"

/* Callback functions imported from NvM Module */
#include "NvM_Cbk.h"

/* Functions imported from Fls Module */
#include "Fls_17_Pmu.h"

/*******************************************************************************
**                      Private Macro Definitions                             **
*******************************************************************************/

/* FEE AS version information */
#define FEE_AS_VERSION (403)
#define FEE_AR_RELEASE_MAJOR_VERSION  (4U)
#define FEE_AR_RELEASE_MINOR_VERSION  (0U)
#define FEE_AR_RELEASE_REVISION_VERSION  (3U)

/* Vendor specific implementation version information */
#define FEE_SW_MAJOR_VERSION  (2U)
#define FEE_SW_MINOR_VERSION  (2U)
#define FEE_SW_PATCH_VERSION  (4U)

/*******************************************************************************
**                      Global Function Declarations                          **
*******************************************************************************/
                        
/*******************************************************************************
**                    Static configuration parameters                         **
*******************************************************************************/

/* Development error detection enabled/disabled */
#define FEE_DEV_ERROR_DETECT       (STD_ON)

/* Fee_GetVersionInfo API enabled/disabled */
#define FEE_VERSION_INFO_API       (STD_ON)

/* Fee_GetCycleCount API enabled/disabled */
#define FEE_GET_CYCLE_COUNT_API    (STD_ON)

/* Fee_SetMode API enabled/disabled */
#define FEE_SET_MODE_SUPPORTED     (STD_ON)

/* Fee_17_GetPrevData API enabled/disabled */
#define FEE_GET_PREV_DATA_API      (STD_ON)

/* Enable/Disable Debug support  */
#define FEE_DEBUG_SUPPORT     (STD_OFF)

/* Erase suspend/resume feature supported in FLS */
#define FEE_FLS_SUPPORTS_ERASE_SUSPEND  (STD_ON)

/* DFlash WordLine size */
#define FEE_DFLASH_WORDLINE_SIZE     (512U)

#define FEE_CONTINUE          (0U)
#define FEE_STOP_AT_GC        (1U)

#define FEE_UNCFG_BLK_OVERFLOW_HANDLE    (FEE_CONTINUE)

/* Virtual page size, i.e., DF_EEPROM page size */
#define FEE_VIRTUAL_PAGE_SIZE      (8U)

/* Logical block's overhead in bytes */
#define FEE_BLOCK_OVERHEAD         (17U)

/* Logical block's data page overhead in bytes */
#define FEE_PAGE_OVERHEAD          (1U)

/* Maximum blocking (delay) time in ms */
#define FEE_MAXIMUM_BLOCKING_TIME  (10U)

/* Maximum number of configured blocks to be handled */
#define FEE_MAX_BLOCK_COUNT        (7U)

/* Symbolic names of logical blocks */
#ifdef FeeConf_FeeBlockConfiguration_EEP_FW_SEGMENT /* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_EEP_FW_SEGMENT already defined
#else 
#define FeeConf_FeeBlockConfiguration_EEP_FW_SEGMENT ((uint16)1)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_EEP_FW_SEGMENT */

#ifdef FeeConf_FeeBlockConfiguration_EEP_LOGIC_SEGMENT /* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_EEP_LOGIC_SEGMENT already defined
#else 
#define FeeConf_FeeBlockConfiguration_EEP_LOGIC_SEGMENT ((uint16)2)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_EEP_LOGIC_SEGMENT */

#ifdef FeeConf_FeeBlockConfiguration_EEP_CAN_SEGMENT /* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_EEP_CAN_SEGMENT already defined
#else 
#define FeeConf_FeeBlockConfiguration_EEP_CAN_SEGMENT ((uint16)3)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_EEP_CAN_SEGMENT */

#ifdef FeeConf_FeeBlockConfiguration_EEP_FS_SEGMENT /* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_EEP_FS_SEGMENT already defined
#else 
#define FeeConf_FeeBlockConfiguration_EEP_FS_SEGMENT ((uint16)4)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_EEP_FS_SEGMENT */

#ifdef FeeConf_FeeBlockConfiguration_EEP_MOC_SEGMENT /* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_EEP_MOC_SEGMENT already defined
#else 
#define FeeConf_FeeBlockConfiguration_EEP_MOC_SEGMENT ((uint16)5)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_EEP_MOC_SEGMENT */

#ifdef FeeConf_FeeBlockConfiguration_EEP_AHB_SEGMENT /* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_EEP_AHB_SEGMENT already defined
#else 
#define FeeConf_FeeBlockConfiguration_EEP_AHB_SEGMENT ((uint16)6)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_EEP_AHB_SEGMENT */

#ifdef FeeConf_FeeBlockConfiguration_EEP_DDS_SEGMENT /* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_EEP_DDS_SEGMENT already defined
#else 
#define FeeConf_FeeBlockConfiguration_EEP_DDS_SEGMENT ((uint16)7)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_EEP_DDS_SEGMENT */


#define FEE_DISABLE_DEM_REPORT   (0U)
#define FEE_ENABLE_DEM_REPORT    (1U)

/* DEM Configurations */
#define FEE_GC_INIT_DEM_REPORT       (FEE_DISABLE_DEM_REPORT)
#define FEE_WRITE_DEM_REPORT         (FEE_DISABLE_DEM_REPORT)
#define FEE_READ_DEM_REPORT          (FEE_DISABLE_DEM_REPORT)
#define FEE_GC_WRITE_DEM_REPORT      (FEE_DISABLE_DEM_REPORT)
#define FEE_GC_READ_DEM_REPORT       (FEE_DISABLE_DEM_REPORT)
#define FEE_GC_ERASE_DEM_REPORT      (FEE_DISABLE_DEM_REPORT)
#define FEE_INVALIDATE_DEM_REPORT    (FEE_DISABLE_DEM_REPORT)
#define FEE_WRITE_CYCLES_DEM_REPORT  (FEE_DISABLE_DEM_REPORT)
#define FEE_GC_TRIG_DEM_REPORT       (FEE_DISABLE_DEM_REPORT)
#define FEE_UNCFG_BLK_DEM_REPORT     (FEE_DISABLE_DEM_REPORT)
#define FEE_DEM_ENABLED              (STD_OFF)

/*******************************************************************************
**                      Global Symbols                                        **
*******************************************************************************/

#define FEE_CONFIG_PTR      (Fee_CfgPtr)

/*******************************************************************************
**                      Global Data Types                                     **
*******************************************************************************/

#endif /* #ifndef FEE_CFG_H */
