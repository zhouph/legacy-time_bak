/**
 * @defgroup Acmio_Cfg Acmio_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Acmio_Cfg.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Acmio_Cfg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
/* Dead Time Macro 설정 : Dead Time 아래 DEAD_TM_SET만 설정 하며, 설정 시 한쪽 펄스 기준으로 설정 한다. */
/* 아래 소프트웨어 결정 공식에서  "*2"는 Center Aligned PWM 의 양쪽을 적용 하기 위해서 필요하다.		*/
#define		DEAD_TM_SET				660											/*	660ns H/W Recommand (6/25)  */
#define		DEAD_TM_SW				( ( DEAD_TM_SET - DEAD_TM_HW ) *  2 / 10 )	
#define		DEAD_TM_HW				0										/*	Gate Driver 106ns */

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ACMIO_START_SEC_CONST_UNSPECIFIED
#include "Acmio_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/
const Acmio_ActrCfgType Acmio_ActrConfig = 
{
    /* PortU */
    {
        (Saluint16) 9729,      	/* Max input value */
        (Saluint16) 271,       	/* Min input value */
        (Saluint16) DEAD_TM_SW  /* Dead time */            
    },   
    /* PortV */
    {
        (Saluint16) 9729,      	/* Max input value */
        (Saluint16) 271,       	/* Min input value */
        (Saluint16) DEAD_TM_SW  /* Dead time */        
    },   
    /* PortW */
    {
        (Saluint16) 9729,      	/* Max input value */
        (Saluint16) 271,       	/* Min input value */
        (Saluint16) DEAD_TM_SW  /* Dead time */      
    },  
};

const Acmio_CurrSnsrCfgType Acmio_CurrSnsrConfig =
{
    (Saluint16) 100,         /* Voltage to Current Multiply factor */
    (Saluint16) 1,           /* Voltage to Current Divide factor */
    (Saluint16) 2500,        /* Offset */
};

const Acmio_MpsSnsrCfgType Acmio_MpsSnsrConfig =
{
    (Saluint16)    1,                            /* NumOfHole */
    (Saluint16) 2048                         /* FullElecAngle */
};
/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ACMIO_STOP_SEC_CONST_UNSPECIFIED
#include "Acmio_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACMIO_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmio_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ACMIO_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmio_MemMap.h"
#define ACMIO_START_SEC_VAR_NOINIT_32BIT
#include "Acmio_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACMIO_STOP_SEC_VAR_NOINIT_32BIT
#include "Acmio_MemMap.h"
#define ACMIO_START_SEC_VAR_UNSPECIFIED
#include "Acmio_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACMIO_STOP_SEC_VAR_UNSPECIFIED
#include "Acmio_MemMap.h"
#define ACMIO_START_SEC_VAR_32BIT
#include "Acmio_MemMap.h"
/** Variable Section (32BIT)**/


#define ACMIO_STOP_SEC_VAR_32BIT
#include "Acmio_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACMIO_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmio_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ACMIO_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmio_MemMap.h"
#define ACMIO_START_SEC_VAR_NOINIT_32BIT
#include "Acmio_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACMIO_STOP_SEC_VAR_NOINIT_32BIT
#include "Acmio_MemMap.h"
#define ACMIO_START_SEC_VAR_UNSPECIFIED
#include "Acmio_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACMIO_STOP_SEC_VAR_UNSPECIFIED
#include "Acmio_MemMap.h"
#define ACMIO_START_SEC_VAR_32BIT
#include "Acmio_MemMap.h"
/** Variable Section (32BIT)**/


#define ACMIO_STOP_SEC_VAR_32BIT
#include "Acmio_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ACMIO_START_SEC_CODE
#include "Acmio_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define ACMIO_STOP_SEC_CODE
#include "Acmio_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
