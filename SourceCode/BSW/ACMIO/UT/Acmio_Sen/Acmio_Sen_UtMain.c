#include "unity.h"
#include "unity_fixture.h"
#include "Acmio_Sen.h"
#include "Acmio_Sen_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Haluint16 UtInput_Acmio_SenMotMonInfo_MotPwrVoltMon[MAX_STEP] = ACMIO_SENMOTMONINFO_MOTPWRVOLTMON;
const Haluint16 UtInput_Acmio_SenMotVoltsMonInfo_MotVoltPhUMon[MAX_STEP] = ACMIO_SENMOTVOLTSMONINFO_MOTVOLTPHUMON;
const Haluint16 UtInput_Acmio_SenMotVoltsMonInfo_MotVoltPhVMon[MAX_STEP] = ACMIO_SENMOTVOLTSMONINFO_MOTVOLTPHVMON;
const Haluint16 UtInput_Acmio_SenMotVoltsMonInfo_MotVoltPhWMon[MAX_STEP] = ACMIO_SENMOTVOLTSMONINFO_MOTVOLTPHWMON;
const Haluint16 UtInput_Acmio_SenMotCurrMonInfo_MotCurrPhUMon0[MAX_STEP] = ACMIO_SENMOTCURRMONINFO_MOTCURRPHUMON0;
const Haluint16 UtInput_Acmio_SenMotCurrMonInfo_MotCurrPhUMon1[MAX_STEP] = ACMIO_SENMOTCURRMONINFO_MOTCURRPHUMON1;
const Haluint16 UtInput_Acmio_SenMotCurrMonInfo_MotCurrPhVMon0[MAX_STEP] = ACMIO_SENMOTCURRMONINFO_MOTCURRPHVMON0;
const Haluint16 UtInput_Acmio_SenMotCurrMonInfo_MotCurrPhVMon1[MAX_STEP] = ACMIO_SENMOTCURRMONINFO_MOTCURRPHVMON1;
const Haluint16 UtInput_Acmio_SenMotAngleMonInfo_MotPosiAngle1deg[MAX_STEP] = ACMIO_SENMOTANGLEMONINFO_MOTPOSIANGLE1DEG;
const Haluint16 UtInput_Acmio_SenMotAngleMonInfo_MotPosiAngle2deg[MAX_STEP] = ACMIO_SENMOTANGLEMONINFO_MOTPOSIANGLE2DEG;
const Haluint16 UtInput_Acmio_SenMotAngleMonInfo_MotPosiAngle1raw[MAX_STEP] = ACMIO_SENMOTANGLEMONINFO_MOTPOSIANGLE1RAW;
const Haluint16 UtInput_Acmio_SenMotAngleMonInfo_MotPosiAngle2raw[MAX_STEP] = ACMIO_SENMOTANGLEMONINFO_MOTPOSIANGLE2RAW;
const Mom_HndlrEcuModeSts_t UtInput_Acmio_SenEcuModeSts[MAX_STEP] = ACMIO_SENECUMODESTS;
const Eem_SuspcDetnFuncInhibitAcmioSts_t UtInput_Acmio_SenFuncInhibitAcmioSts[MAX_STEP] = ACMIO_SENFUNCINHIBITACMIOSTS;

const Salsint32 UtExpected_Acmio_SenMotCurrInfo_MotCurrPhUMeasd[MAX_STEP] = ACMIO_SENMOTCURRINFO_MOTCURRPHUMEASD;
const Salsint32 UtExpected_Acmio_SenMotCurrInfo_MotCurrPhVMeasd[MAX_STEP] = ACMIO_SENMOTCURRINFO_MOTCURRPHVMEASD;
const Salsint32 UtExpected_Acmio_SenMotCurrInfo_MotCurrPhWMeasd[MAX_STEP] = ACMIO_SENMOTCURRINFO_MOTCURRPHWMEASD;
const Saluint16 UtExpected_Acmio_SenMotAngle1Info_MotElecAngle1[MAX_STEP] = ACMIO_SENMOTANGLE1INFO_MOTELECANGLE1;
const Saluint16 UtExpected_Acmio_SenMotAngle1Info_MotMechAngle1[MAX_STEP] = ACMIO_SENMOTANGLE1INFO_MOTMECHANGLE1;
const Saluint16 UtExpected_Acmio_SenMotAngle2Info_MotElecAngle2[MAX_STEP] = ACMIO_SENMOTANGLE2INFO_MOTELECANGLE2;
const Saluint16 UtExpected_Acmio_SenMotAngle2Info_MotMechAngle2[MAX_STEP] = ACMIO_SENMOTANGLE2INFO_MOTMECHANGLE2;
const Acmio_SenVdcLink_t UtExpected_Acmio_SenVdcLink[MAX_STEP] = ACMIO_SENVDCLINK;



TEST_GROUP(Acmio_Sen);
TEST_SETUP(Acmio_Sen)
{
    Acmio_Sen_Init();
}

TEST_TEAR_DOWN(Acmio_Sen)
{   /* Postcondition */

}

TEST(Acmio_Sen, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Acmio_SenMotMonInfo.MotPwrVoltMon = UtInput_Acmio_SenMotMonInfo_MotPwrVoltMon[i];
        Acmio_SenMotVoltsMonInfo.MotVoltPhUMon = UtInput_Acmio_SenMotVoltsMonInfo_MotVoltPhUMon[i];
        Acmio_SenMotVoltsMonInfo.MotVoltPhVMon = UtInput_Acmio_SenMotVoltsMonInfo_MotVoltPhVMon[i];
        Acmio_SenMotVoltsMonInfo.MotVoltPhWMon = UtInput_Acmio_SenMotVoltsMonInfo_MotVoltPhWMon[i];
        Acmio_SenMotCurrMonInfo.MotCurrPhUMon0 = UtInput_Acmio_SenMotCurrMonInfo_MotCurrPhUMon0[i];
        Acmio_SenMotCurrMonInfo.MotCurrPhUMon1 = UtInput_Acmio_SenMotCurrMonInfo_MotCurrPhUMon1[i];
        Acmio_SenMotCurrMonInfo.MotCurrPhVMon0 = UtInput_Acmio_SenMotCurrMonInfo_MotCurrPhVMon0[i];
        Acmio_SenMotCurrMonInfo.MotCurrPhVMon1 = UtInput_Acmio_SenMotCurrMonInfo_MotCurrPhVMon1[i];
        Acmio_SenMotAngleMonInfo.MotPosiAngle1deg = UtInput_Acmio_SenMotAngleMonInfo_MotPosiAngle1deg[i];
        Acmio_SenMotAngleMonInfo.MotPosiAngle2deg = UtInput_Acmio_SenMotAngleMonInfo_MotPosiAngle2deg[i];
        Acmio_SenMotAngleMonInfo.MotPosiAngle1raw = UtInput_Acmio_SenMotAngleMonInfo_MotPosiAngle1raw[i];
        Acmio_SenMotAngleMonInfo.MotPosiAngle2raw = UtInput_Acmio_SenMotAngleMonInfo_MotPosiAngle2raw[i];
        Acmio_SenEcuModeSts = UtInput_Acmio_SenEcuModeSts[i];
        Acmio_SenFuncInhibitAcmioSts = UtInput_Acmio_SenFuncInhibitAcmioSts[i];

        Acmio_Sen();

        TEST_ASSERT_EQUAL(Acmio_SenMotCurrInfo.MotCurrPhUMeasd, UtExpected_Acmio_SenMotCurrInfo_MotCurrPhUMeasd[i]);
        TEST_ASSERT_EQUAL(Acmio_SenMotCurrInfo.MotCurrPhVMeasd, UtExpected_Acmio_SenMotCurrInfo_MotCurrPhVMeasd[i]);
        TEST_ASSERT_EQUAL(Acmio_SenMotCurrInfo.MotCurrPhWMeasd, UtExpected_Acmio_SenMotCurrInfo_MotCurrPhWMeasd[i]);
        TEST_ASSERT_EQUAL(Acmio_SenMotAngle1Info.MotElecAngle1, UtExpected_Acmio_SenMotAngle1Info_MotElecAngle1[i]);
        TEST_ASSERT_EQUAL(Acmio_SenMotAngle1Info.MotMechAngle1, UtExpected_Acmio_SenMotAngle1Info_MotMechAngle1[i]);
        TEST_ASSERT_EQUAL(Acmio_SenMotAngle2Info.MotElecAngle2, UtExpected_Acmio_SenMotAngle2Info_MotElecAngle2[i]);
        TEST_ASSERT_EQUAL(Acmio_SenMotAngle2Info.MotMechAngle2, UtExpected_Acmio_SenMotAngle2Info_MotMechAngle2[i]);
        TEST_ASSERT_EQUAL(Acmio_SenVdcLink, UtExpected_Acmio_SenVdcLink[i]);
    }
}

TEST_GROUP_RUNNER(Acmio_Sen)
{
    RUN_TEST_CASE(Acmio_Sen, All);
}
