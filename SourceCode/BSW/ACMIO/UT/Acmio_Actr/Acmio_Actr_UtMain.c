#include "unity.h"
#include "unity_fixture.h"
#include "Acmio_Actr.h"
#include "Acmio_Actr_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Salsint32 UtInput_Acmio_ActrMotReqDataDiagInfo_MotPwmPhUData[MAX_STEP] = ACMIO_ACTRMOTREQDATADIAGINFO_MOTPWMPHUDATA;
const Salsint32 UtInput_Acmio_ActrMotReqDataDiagInfo_MotPwmPhVData[MAX_STEP] = ACMIO_ACTRMOTREQDATADIAGINFO_MOTPWMPHVDATA;
const Salsint32 UtInput_Acmio_ActrMotReqDataDiagInfo_MotPwmPhWData[MAX_STEP] = ACMIO_ACTRMOTREQDATADIAGINFO_MOTPWMPHWDATA;
const Saluint8 UtInput_Acmio_ActrMotReqDataDiagInfo_MotReq[MAX_STEP] = ACMIO_ACTRMOTREQDATADIAGINFO_MOTREQ;
const Salsint32 UtInput_Acmio_ActrMotReqDataAcmctlInfo_MotPwmPhUData[MAX_STEP] = ACMIO_ACTRMOTREQDATAACMCTLINFO_MOTPWMPHUDATA;
const Salsint32 UtInput_Acmio_ActrMotReqDataAcmctlInfo_MotPwmPhVData[MAX_STEP] = ACMIO_ACTRMOTREQDATAACMCTLINFO_MOTPWMPHVDATA;
const Salsint32 UtInput_Acmio_ActrMotReqDataAcmctlInfo_MotPwmPhWData[MAX_STEP] = ACMIO_ACTRMOTREQDATAACMCTLINFO_MOTPWMPHWDATA;
const Saluint8 UtInput_Acmio_ActrMotReqDataAcmctlInfo_MotReq[MAX_STEP] = ACMIO_ACTRMOTREQDATAACMCTLINFO_MOTREQ;
const Mom_HndlrEcuModeSts_t UtInput_Acmio_ActrEcuModeSts[MAX_STEP] = ACMIO_ACTRECUMODESTS;
const Eem_SuspcDetnFuncInhibitAcmioSts_t UtInput_Acmio_ActrFuncInhibitAcmioSts[MAX_STEP] = ACMIO_ACTRFUNCINHIBITACMIOSTS;

const Saluint16 UtExpected_Acmio_ActrMotPwmDataInfo_MotPwmPhUhighData[MAX_STEP] = ACMIO_ACTRMOTPWMDATAINFO_MOTPWMPHUHIGHDATA;
const Saluint16 UtExpected_Acmio_ActrMotPwmDataInfo_MotPwmPhVhighData[MAX_STEP] = ACMIO_ACTRMOTPWMDATAINFO_MOTPWMPHVHIGHDATA;
const Saluint16 UtExpected_Acmio_ActrMotPwmDataInfo_MotPwmPhWhighData[MAX_STEP] = ACMIO_ACTRMOTPWMDATAINFO_MOTPWMPHWHIGHDATA;
const Saluint16 UtExpected_Acmio_ActrMotPwmDataInfo_MotPwmPhUlowData[MAX_STEP] = ACMIO_ACTRMOTPWMDATAINFO_MOTPWMPHULOWDATA;
const Saluint16 UtExpected_Acmio_ActrMotPwmDataInfo_MotPwmPhVlowData[MAX_STEP] = ACMIO_ACTRMOTPWMDATAINFO_MOTPWMPHVLOWDATA;
const Saluint16 UtExpected_Acmio_ActrMotPwmDataInfo_MotPwmPhWlowData[MAX_STEP] = ACMIO_ACTRMOTPWMDATAINFO_MOTPWMPHWLOWDATA;



TEST_GROUP(Acmio_Actr);
TEST_SETUP(Acmio_Actr)
{
    Acmio_Actr_Init();
}

TEST_TEAR_DOWN(Acmio_Actr)
{   /* Postcondition */

}

TEST(Acmio_Actr, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Acmio_ActrMotReqDataDiagInfo.MotPwmPhUData = UtInput_Acmio_ActrMotReqDataDiagInfo_MotPwmPhUData[i];
        Acmio_ActrMotReqDataDiagInfo.MotPwmPhVData = UtInput_Acmio_ActrMotReqDataDiagInfo_MotPwmPhVData[i];
        Acmio_ActrMotReqDataDiagInfo.MotPwmPhWData = UtInput_Acmio_ActrMotReqDataDiagInfo_MotPwmPhWData[i];
        Acmio_ActrMotReqDataDiagInfo.MotReq = UtInput_Acmio_ActrMotReqDataDiagInfo_MotReq[i];
        Acmio_ActrMotReqDataAcmctlInfo.MotPwmPhUData = UtInput_Acmio_ActrMotReqDataAcmctlInfo_MotPwmPhUData[i];
        Acmio_ActrMotReqDataAcmctlInfo.MotPwmPhVData = UtInput_Acmio_ActrMotReqDataAcmctlInfo_MotPwmPhVData[i];
        Acmio_ActrMotReqDataAcmctlInfo.MotPwmPhWData = UtInput_Acmio_ActrMotReqDataAcmctlInfo_MotPwmPhWData[i];
        Acmio_ActrMotReqDataAcmctlInfo.MotReq = UtInput_Acmio_ActrMotReqDataAcmctlInfo_MotReq[i];
        Acmio_ActrEcuModeSts = UtInput_Acmio_ActrEcuModeSts[i];
        Acmio_ActrFuncInhibitAcmioSts = UtInput_Acmio_ActrFuncInhibitAcmioSts[i];

        Acmio_Actr();

        TEST_ASSERT_EQUAL(Acmio_ActrMotPwmDataInfo.MotPwmPhUhighData, UtExpected_Acmio_ActrMotPwmDataInfo_MotPwmPhUhighData[i]);
        TEST_ASSERT_EQUAL(Acmio_ActrMotPwmDataInfo.MotPwmPhVhighData, UtExpected_Acmio_ActrMotPwmDataInfo_MotPwmPhVhighData[i]);
        TEST_ASSERT_EQUAL(Acmio_ActrMotPwmDataInfo.MotPwmPhWhighData, UtExpected_Acmio_ActrMotPwmDataInfo_MotPwmPhWhighData[i]);
        TEST_ASSERT_EQUAL(Acmio_ActrMotPwmDataInfo.MotPwmPhUlowData, UtExpected_Acmio_ActrMotPwmDataInfo_MotPwmPhUlowData[i]);
        TEST_ASSERT_EQUAL(Acmio_ActrMotPwmDataInfo.MotPwmPhVlowData, UtExpected_Acmio_ActrMotPwmDataInfo_MotPwmPhVlowData[i]);
        TEST_ASSERT_EQUAL(Acmio_ActrMotPwmDataInfo.MotPwmPhWlowData, UtExpected_Acmio_ActrMotPwmDataInfo_MotPwmPhWlowData[i]);
    }
}

TEST_GROUP_RUNNER(Acmio_Actr)
{
    RUN_TEST_CASE(Acmio_Actr, All);
}
