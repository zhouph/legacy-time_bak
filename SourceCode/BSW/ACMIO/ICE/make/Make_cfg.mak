# \file
#
# \brief AUTOSAR Make
#
# This file contains the implementation of the AUTOSAR
# module Make.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.


# Only include these variables once
ifndef PROJECT_MODULES

# project name in workspace
TRESOS2_PROJECT_NAME := basicTemplate

TRESOS2_ECU_ID       := ECU

ifeq ($(ENABLED_PLUGINS),)
# In case enabled modules are NOT specified explicitly
# use all enabled modules (including modules, that are NOT generated)
PROJECT_MODULES := Make
else
# otherwise only use generated modules
PROJECT_MODULES := Make
endif

# add tresos2 make plugin if not yet contained in SOFTWARE_MODULES
SOFTWARE_MODULES := $(filter-out tresos2,$(SOFTWARE_MODULES))
SOFTWARE_MODULES += tresos2

# There might already modules added to SOFTWARE_MODULES. So add only what's
# not yet contained SOFTWARE_MODULES.
# Duplicated entries are removed by the sort function.
SOFTWARE_MODULES += $(sort $(filter-out $(SOFTWARE_MODULES), $(PROJECT_MODULES)))

# variables defining module versions
Make_VARIANT      := TS_TxDxM4I0R0
Platforms_VARIANT := TS_T16D15M2I0R0

# target and derivate
TARGET   := TRICORE
DERIVATE := TC277

# output path for generated files
GEN_OUTPUT_PATH := $(PROJECT_ROOT)\ICE\output\generated

# output path for files created by the build environment
PROJECT_OUTPUT_PATH ?= $(GEN_OUTPUT_PATH)\..

# for compatibility reasons we need the AUTOSAR_BASE_OUTPUT_PATH
AUTOSAR_BASE_OUTPUT_PATH ?= $(GEN_OUTPUT_PATH)

endif




# Set Os Vendor to EB
OS_VENDOR := EB

