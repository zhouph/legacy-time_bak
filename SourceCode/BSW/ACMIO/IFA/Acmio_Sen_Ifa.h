/**
 * @defgroup Acmio_Sen_Ifa Acmio_Sen_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Acmio_Sen_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ACMIO_SEN_IFA_H_
#define ACMIO_SEN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Acmio_Sen_Read_Acmio_SenMotMonInfo(data) do \
{ \
    *data = Acmio_SenMotMonInfo; \
}while(0);

#define Acmio_Sen_Read_Acmio_SenMotVoltsMonInfo(data) do \
{ \
    *data = Acmio_SenMotVoltsMonInfo; \
}while(0);

#define Acmio_Sen_Read_Acmio_SenMotCurrMonInfo(data) do \
{ \
    *data = Acmio_SenMotCurrMonInfo; \
}while(0);

#define Acmio_Sen_Read_Acmio_SenMotAngleMonInfo(data) do \
{ \
    *data = Acmio_SenMotAngleMonInfo; \
}while(0);

#define Acmio_Sen_Read_Acmio_SenMotMonInfo_MotPwrVoltMon(data) do \
{ \
    *data = Acmio_SenMotMonInfo.MotPwrVoltMon; \
}while(0);

#define Acmio_Sen_Read_Acmio_SenMotVoltsMonInfo_MotVoltPhUMon(data) do \
{ \
    *data = Acmio_SenMotVoltsMonInfo.MotVoltPhUMon; \
}while(0);

#define Acmio_Sen_Read_Acmio_SenMotVoltsMonInfo_MotVoltPhVMon(data) do \
{ \
    *data = Acmio_SenMotVoltsMonInfo.MotVoltPhVMon; \
}while(0);

#define Acmio_Sen_Read_Acmio_SenMotVoltsMonInfo_MotVoltPhWMon(data) do \
{ \
    *data = Acmio_SenMotVoltsMonInfo.MotVoltPhWMon; \
}while(0);

#define Acmio_Sen_Read_Acmio_SenMotCurrMonInfo_MotCurrPhUMon0(data) do \
{ \
    *data = Acmio_SenMotCurrMonInfo.MotCurrPhUMon0; \
}while(0);

#define Acmio_Sen_Read_Acmio_SenMotCurrMonInfo_MotCurrPhUMon1(data) do \
{ \
    *data = Acmio_SenMotCurrMonInfo.MotCurrPhUMon1; \
}while(0);

#define Acmio_Sen_Read_Acmio_SenMotCurrMonInfo_MotCurrPhVMon0(data) do \
{ \
    *data = Acmio_SenMotCurrMonInfo.MotCurrPhVMon0; \
}while(0);

#define Acmio_Sen_Read_Acmio_SenMotCurrMonInfo_MotCurrPhVMon1(data) do \
{ \
    *data = Acmio_SenMotCurrMonInfo.MotCurrPhVMon1; \
}while(0);

#define Acmio_Sen_Read_Acmio_SenMotAngleMonInfo_MotPosiAngle1deg(data) do \
{ \
    *data = Acmio_SenMotAngleMonInfo.MotPosiAngle1deg; \
}while(0);

#define Acmio_Sen_Read_Acmio_SenMotAngleMonInfo_MotPosiAngle2deg(data) do \
{ \
    *data = Acmio_SenMotAngleMonInfo.MotPosiAngle2deg; \
}while(0);

#define Acmio_Sen_Read_Acmio_SenMotAngleMonInfo_MotPosiAngle1raw(data) do \
{ \
    *data = Acmio_SenMotAngleMonInfo.MotPosiAngle1raw; \
}while(0);

#define Acmio_Sen_Read_Acmio_SenMotAngleMonInfo_MotPosiAngle2raw(data) do \
{ \
    *data = Acmio_SenMotAngleMonInfo.MotPosiAngle2raw; \
}while(0);

#define Acmio_Sen_Read_Acmio_SenEcuModeSts(data) do \
{ \
    *data = Acmio_SenEcuModeSts; \
}while(0);

#define Acmio_Sen_Read_Acmio_SenFuncInhibitAcmioSts(data) do \
{ \
    *data = Acmio_SenFuncInhibitAcmioSts; \
}while(0);


/* Set Output DE MAcro Function */
#define Acmio_Sen_Write_Acmio_SenMotCurrInfo(data) do \
{ \
    Acmio_SenMotCurrInfo = *data; \
}while(0);

#define Acmio_Sen_Write_Acmio_SenMotAngle1Info(data) do \
{ \
    Acmio_SenMotAngle1Info = *data; \
}while(0);

#define Acmio_Sen_Write_Acmio_SenMotAngle2Info(data) do \
{ \
    Acmio_SenMotAngle2Info = *data; \
}while(0);

#define Acmio_Sen_Write_Acmio_SenMotCurrInfo_MotCurrPhUMeasd(data) do \
{ \
    Acmio_SenMotCurrInfo.MotCurrPhUMeasd = *data; \
}while(0);

#define Acmio_Sen_Write_Acmio_SenMotCurrInfo_MotCurrPhVMeasd(data) do \
{ \
    Acmio_SenMotCurrInfo.MotCurrPhVMeasd = *data; \
}while(0);

#define Acmio_Sen_Write_Acmio_SenMotCurrInfo_MotCurrPhWMeasd(data) do \
{ \
    Acmio_SenMotCurrInfo.MotCurrPhWMeasd = *data; \
}while(0);

#define Acmio_Sen_Write_Acmio_SenMotAngle1Info_MotElecAngle1(data) do \
{ \
    Acmio_SenMotAngle1Info.MotElecAngle1 = *data; \
}while(0);

#define Acmio_Sen_Write_Acmio_SenMotAngle1Info_MotMechAngle1(data) do \
{ \
    Acmio_SenMotAngle1Info.MotMechAngle1 = *data; \
}while(0);

#define Acmio_Sen_Write_Acmio_SenMotAngle2Info_MotElecAngle2(data) do \
{ \
    Acmio_SenMotAngle2Info.MotElecAngle2 = *data; \
}while(0);

#define Acmio_Sen_Write_Acmio_SenMotAngle2Info_MotMechAngle2(data) do \
{ \
    Acmio_SenMotAngle2Info.MotMechAngle2 = *data; \
}while(0);

#define Acmio_Sen_Write_Acmio_SenVdcLink(data) do \
{ \
    Acmio_SenVdcLink = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ACMIO_SEN_IFA_H_ */
/** @} */
