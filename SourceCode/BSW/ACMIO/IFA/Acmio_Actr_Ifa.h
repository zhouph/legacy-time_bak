/**
 * @defgroup Acmio_Actr_Ifa Acmio_Actr_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Acmio_Actr_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ACMIO_ACTR_IFA_H_
#define ACMIO_ACTR_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Acmio_Actr_Read_Acmio_ActrMotReqDataDiagInfo(data) do \
{ \
    *data = Acmio_ActrMotReqDataDiagInfo; \
}while(0);

#define Acmio_Actr_Read_Acmio_ActrMotReqDataAcmctlInfo(data) do \
{ \
    *data = Acmio_ActrMotReqDataAcmctlInfo; \
}while(0);

#define Acmio_Actr_Read_Acmio_ActrMotReqDataDiagInfo_MotPwmPhUData(data) do \
{ \
    *data = Acmio_ActrMotReqDataDiagInfo.MotPwmPhUData; \
}while(0);

#define Acmio_Actr_Read_Acmio_ActrMotReqDataDiagInfo_MotPwmPhVData(data) do \
{ \
    *data = Acmio_ActrMotReqDataDiagInfo.MotPwmPhVData; \
}while(0);

#define Acmio_Actr_Read_Acmio_ActrMotReqDataDiagInfo_MotPwmPhWData(data) do \
{ \
    *data = Acmio_ActrMotReqDataDiagInfo.MotPwmPhWData; \
}while(0);

#define Acmio_Actr_Read_Acmio_ActrMotReqDataDiagInfo_MotReq(data) do \
{ \
    *data = Acmio_ActrMotReqDataDiagInfo.MotReq; \
}while(0);

#define Acmio_Actr_Read_Acmio_ActrMotReqDataAcmctlInfo_MotPwmPhUData(data) do \
{ \
    *data = Acmio_ActrMotReqDataAcmctlInfo.MotPwmPhUData; \
}while(0);

#define Acmio_Actr_Read_Acmio_ActrMotReqDataAcmctlInfo_MotPwmPhVData(data) do \
{ \
    *data = Acmio_ActrMotReqDataAcmctlInfo.MotPwmPhVData; \
}while(0);

#define Acmio_Actr_Read_Acmio_ActrMotReqDataAcmctlInfo_MotPwmPhWData(data) do \
{ \
    *data = Acmio_ActrMotReqDataAcmctlInfo.MotPwmPhWData; \
}while(0);

#define Acmio_Actr_Read_Acmio_ActrMotReqDataAcmctlInfo_MotReq(data) do \
{ \
    *data = Acmio_ActrMotReqDataAcmctlInfo.MotReq; \
}while(0);

#define Acmio_Actr_Read_Acmio_ActrEcuModeSts(data) do \
{ \
    *data = Acmio_ActrEcuModeSts; \
}while(0);

#define Acmio_Actr_Read_Acmio_ActrFuncInhibitAcmioSts(data) do \
{ \
    *data = Acmio_ActrFuncInhibitAcmioSts; \
}while(0);


/* Set Output DE MAcro Function */
#define Acmio_Actr_Write_Acmio_ActrMotPwmDataInfo(data) do \
{ \
    Acmio_ActrMotPwmDataInfo = *data; \
}while(0);

#define Acmio_Actr_Write_Acmio_ActrMotPwmDataInfo_MotPwmPhUhighData(data) do \
{ \
    Acmio_ActrMotPwmDataInfo.MotPwmPhUhighData = *data; \
}while(0);

#define Acmio_Actr_Write_Acmio_ActrMotPwmDataInfo_MotPwmPhVhighData(data) do \
{ \
    Acmio_ActrMotPwmDataInfo.MotPwmPhVhighData = *data; \
}while(0);

#define Acmio_Actr_Write_Acmio_ActrMotPwmDataInfo_MotPwmPhWhighData(data) do \
{ \
    Acmio_ActrMotPwmDataInfo.MotPwmPhWhighData = *data; \
}while(0);

#define Acmio_Actr_Write_Acmio_ActrMotPwmDataInfo_MotPwmPhUlowData(data) do \
{ \
    Acmio_ActrMotPwmDataInfo.MotPwmPhUlowData = *data; \
}while(0);

#define Acmio_Actr_Write_Acmio_ActrMotPwmDataInfo_MotPwmPhVlowData(data) do \
{ \
    Acmio_ActrMotPwmDataInfo.MotPwmPhVlowData = *data; \
}while(0);

#define Acmio_Actr_Write_Acmio_ActrMotPwmDataInfo_MotPwmPhWlowData(data) do \
{ \
    Acmio_ActrMotPwmDataInfo.MotPwmPhWlowData = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ACMIO_ACTR_IFA_H_ */
/** @} */
