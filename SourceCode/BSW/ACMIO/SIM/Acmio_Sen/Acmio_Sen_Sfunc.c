#define S_FUNCTION_NAME      Acmio_Sen_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          14
#define WidthOutputPort         8

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Acmio_Sen.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Acmio_SenMotMonInfo.MotPwrVoltMon = input[0];
    Acmio_SenMotVoltsMonInfo.MotVoltPhUMon = input[1];
    Acmio_SenMotVoltsMonInfo.MotVoltPhVMon = input[2];
    Acmio_SenMotVoltsMonInfo.MotVoltPhWMon = input[3];
    Acmio_SenMotCurrMonInfo.MotCurrPhUMon0 = input[4];
    Acmio_SenMotCurrMonInfo.MotCurrPhUMon1 = input[5];
    Acmio_SenMotCurrMonInfo.MotCurrPhVMon0 = input[6];
    Acmio_SenMotCurrMonInfo.MotCurrPhVMon1 = input[7];
    Acmio_SenMotAngleMonInfo.MotPosiAngle1deg = input[8];
    Acmio_SenMotAngleMonInfo.MotPosiAngle2deg = input[9];
    Acmio_SenMotAngleMonInfo.MotPosiAngle1raw = input[10];
    Acmio_SenMotAngleMonInfo.MotPosiAngle2raw = input[11];
    Acmio_SenEcuModeSts = input[12];
    Acmio_SenFuncInhibitAcmioSts = input[13];

    Acmio_Sen();


    output[0] = Acmio_SenMotCurrInfo.MotCurrPhUMeasd;
    output[1] = Acmio_SenMotCurrInfo.MotCurrPhVMeasd;
    output[2] = Acmio_SenMotCurrInfo.MotCurrPhWMeasd;
    output[3] = Acmio_SenMotAngle1Info.MotElecAngle1;
    output[4] = Acmio_SenMotAngle1Info.MotMechAngle1;
    output[5] = Acmio_SenMotAngle2Info.MotElecAngle2;
    output[6] = Acmio_SenMotAngle2Info.MotMechAngle2;
    output[7] = Acmio_SenVdcLink;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Acmio_Sen_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
