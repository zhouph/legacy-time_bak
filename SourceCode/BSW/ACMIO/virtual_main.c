/* Virtual main for linking */
/* purpose : Generation of map file */
#include "Acmio_Sen.h"
#include "Acmio_Actr.h"

int main(void)
{
    Acmio_Sen_Init();
    Acmio_Actr_Init();

    while(1)
    {
        Acmio_Sen();
        Acmio_Actr();
    }
}