Acmio_ActrMotReqDataDiagInfo = Simulink.Bus;
DeList={
    'MotPwmPhUData'
    'MotPwmPhVData'
    'MotPwmPhWData'
    'MotReq'
    };
Acmio_ActrMotReqDataDiagInfo = CreateBus(Acmio_ActrMotReqDataDiagInfo, DeList);
clear DeList;

Acmio_ActrMotReqDataAcmctlInfo = Simulink.Bus;
DeList={
    'MotPwmPhUData'
    'MotPwmPhVData'
    'MotPwmPhWData'
    'MotReq'
    };
Acmio_ActrMotReqDataAcmctlInfo = CreateBus(Acmio_ActrMotReqDataAcmctlInfo, DeList);
clear DeList;

Acmio_ActrEcuModeSts = Simulink.Bus;
DeList={'Acmio_ActrEcuModeSts'};
Acmio_ActrEcuModeSts = CreateBus(Acmio_ActrEcuModeSts, DeList);
clear DeList;

Acmio_ActrFuncInhibitAcmioSts = Simulink.Bus;
DeList={'Acmio_ActrFuncInhibitAcmioSts'};
Acmio_ActrFuncInhibitAcmioSts = CreateBus(Acmio_ActrFuncInhibitAcmioSts, DeList);
clear DeList;

Acmio_ActrMotPwmDataInfo = Simulink.Bus;
DeList={
    'MotPwmPhUhighData'
    'MotPwmPhVhighData'
    'MotPwmPhWhighData'
    'MotPwmPhUlowData'
    'MotPwmPhVlowData'
    'MotPwmPhWlowData'
    };
Acmio_ActrMotPwmDataInfo = CreateBus(Acmio_ActrMotPwmDataInfo, DeList);
clear DeList;

