Acmio_SenMotMonInfo = Simulink.Bus;
DeList={
    'MotPwrVoltMon'
    };
Acmio_SenMotMonInfo = CreateBus(Acmio_SenMotMonInfo, DeList);
clear DeList;

Acmio_SenMotVoltsMonInfo = Simulink.Bus;
DeList={
    'MotVoltPhUMon'
    'MotVoltPhVMon'
    'MotVoltPhWMon'
    };
Acmio_SenMotVoltsMonInfo = CreateBus(Acmio_SenMotVoltsMonInfo, DeList);
clear DeList;

Acmio_SenMotCurrMonInfo = Simulink.Bus;
DeList={
    'MotCurrPhUMon0'
    'MotCurrPhUMon1'
    'MotCurrPhVMon0'
    'MotCurrPhVMon1'
    };
Acmio_SenMotCurrMonInfo = CreateBus(Acmio_SenMotCurrMonInfo, DeList);
clear DeList;

Acmio_SenMotAngleMonInfo = Simulink.Bus;
DeList={
    'MotPosiAngle1deg'
    'MotPosiAngle2deg'
    'MotPosiAngle1raw'
    'MotPosiAngle2raw'
    };
Acmio_SenMotAngleMonInfo = CreateBus(Acmio_SenMotAngleMonInfo, DeList);
clear DeList;

Acmio_SenEcuModeSts = Simulink.Bus;
DeList={'Acmio_SenEcuModeSts'};
Acmio_SenEcuModeSts = CreateBus(Acmio_SenEcuModeSts, DeList);
clear DeList;

Acmio_SenFuncInhibitAcmioSts = Simulink.Bus;
DeList={'Acmio_SenFuncInhibitAcmioSts'};
Acmio_SenFuncInhibitAcmioSts = CreateBus(Acmio_SenFuncInhibitAcmioSts, DeList);
clear DeList;

Acmio_SenMotCurrInfo = Simulink.Bus;
DeList={
    'MotCurrPhUMeasd'
    'MotCurrPhVMeasd'
    'MotCurrPhWMeasd'
    };
Acmio_SenMotCurrInfo = CreateBus(Acmio_SenMotCurrInfo, DeList);
clear DeList;

Acmio_SenMotAngle1Info = Simulink.Bus;
DeList={
    'MotElecAngle1'
    'MotMechAngle1'
    };
Acmio_SenMotAngle1Info = CreateBus(Acmio_SenMotAngle1Info, DeList);
clear DeList;

Acmio_SenMotAngle2Info = Simulink.Bus;
DeList={
    'MotElecAngle2'
    'MotMechAngle2'
    };
Acmio_SenMotAngle2Info = CreateBus(Acmio_SenMotAngle2Info, DeList);
clear DeList;

Acmio_SenVdcLink = Simulink.Bus;
DeList={'Acmio_SenVdcLink'};
Acmio_SenVdcLink = CreateBus(Acmio_SenVdcLink, DeList);
clear DeList;

