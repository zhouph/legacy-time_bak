# \file
#
# \brief Acmio
#
# This file contains the implementation of the SWC
# module Acmio.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Acmio_src

Acmio_src_FILES        += $(Acmio_SRC_PATH)\Acmio_Sen.c
Acmio_src_FILES        += $(Acmio_IFA_PATH)\Acmio_Sen_Ifa.c
Acmio_src_FILES        += $(Acmio_SRC_PATH)\Acmio_Actr.c
Acmio_src_FILES        += $(Acmio_IFA_PATH)\Acmio_Actr_Ifa.c
Acmio_src_FILES        += $(Acmio_SRC_PATH)\Acmio_ActrProcessing.c
Acmio_src_FILES        += $(Acmio_SRC_PATH)\Acmio_SenProcessing.c
Acmio_src_FILES        += $(Acmio_CFG_PATH)\Acmio_Cfg.c
Acmio_src_FILES        += $(Acmio_CAL_PATH)\Acmio_Cal.c

ifeq ($(ICE_COMPILE),true)
Acmio_src_FILES        += $(Acmio_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	Acmio_src_FILES        += $(Acmio_UNITY_PATH)\unity.c
	Acmio_src_FILES        += $(Acmio_UNITY_PATH)\unity_fixture.c	
	Acmio_src_FILES        += $(Acmio_UT_PATH)\main.c
	Acmio_src_FILES        += $(Acmio_UT_PATH)\Acmio_Sen\Acmio_Sen_UtMain.c
	Acmio_src_FILES        += $(Acmio_UT_PATH)\Acmio_Actr\Acmio_Actr_UtMain.c
endif