# \file
#
# \brief Acmio
#
# This file contains the implementation of the SWC
# module Acmio.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Acmio_CORE_PATH     := $(MANDO_BSW_ROOT)\Acmio
Acmio_CAL_PATH      := $(Acmio_CORE_PATH)\CAL\$(Acmio_VARIANT)
Acmio_SRC_PATH      := $(Acmio_CORE_PATH)\SRC
Acmio_CFG_PATH      := $(Acmio_CORE_PATH)\CFG\$(Acmio_VARIANT)
Acmio_HDR_PATH      := $(Acmio_CORE_PATH)\HDR
Acmio_IFA_PATH      := $(Acmio_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Acmio_CMN_PATH      := $(Acmio_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	Acmio_UT_PATH		:= $(Acmio_CORE_PATH)\UT
	Acmio_UNITY_PATH	:= $(Acmio_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(Acmio_UT_PATH)
	CC_INCLUDE_PATH		+= $(Acmio_UNITY_PATH)
	Acmio_Sen_PATH 	:= Acmio_UT_PATH\Acmio_Sen
	Acmio_Actr_PATH 	:= Acmio_UT_PATH\Acmio_Actr
endif
CC_INCLUDE_PATH    += $(Acmio_CAL_PATH)
CC_INCLUDE_PATH    += $(Acmio_SRC_PATH)
CC_INCLUDE_PATH    += $(Acmio_CFG_PATH)
CC_INCLUDE_PATH    += $(Acmio_HDR_PATH)
CC_INCLUDE_PATH    += $(Acmio_IFA_PATH)
CC_INCLUDE_PATH    += $(Acmio_CMN_PATH)

