/**
 * @defgroup Acmio_Actr Acmio_Actr
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Acmio_Actr.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ACMIO_ACTR_H_
#define ACMIO_ACTR_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Acmio_Types.h"
#include "Acmio_Cfg.h"
#include "Acmio_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define ACMIO_ACTR_MODULE_ID      (0)
 #define ACMIO_ACTR_MAJOR_VERSION  (2)
 #define ACMIO_ACTR_MINOR_VERSION  (0)
 #define ACMIO_ACTR_PATCH_VERSION  (0)
 #define ACMIO_ACTR_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Acmio_Actr_HdrBusType Acmio_ActrBus;

/* Version Info */
extern const SwcVersionInfo_t Acmio_ActrVersionInfo;

/* Input Data Element */
extern Diag_HndlrMotReqDataDiagInfo_t Acmio_ActrMotReqDataDiagInfo;
extern Acmctl_CtrlMotReqDataAcmctlInfo_t Acmio_ActrMotReqDataAcmctlInfo;
extern Mom_HndlrEcuModeSts_t Acmio_ActrEcuModeSts;
extern Eem_SuspcDetnFuncInhibitAcmioSts_t Acmio_ActrFuncInhibitAcmioSts;

/* Output Data Element */
extern Acmio_ActrMotPwmDataInfo_t Acmio_ActrMotPwmDataInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Acmio_Actr_Init(void);
extern void Acmio_Actr(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ACMIO_ACTR_H_ */
/** @} */
