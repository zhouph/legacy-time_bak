/**
 * @defgroup Acmio_Sen Acmio_Sen
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Acmio_Sen.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ACMIO_SEN_H_
#define ACMIO_SEN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Acmio_Types.h"
#include "Acmio_Cfg.h"
#include "Acmio_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define ACMIO_SEN_MODULE_ID      (0)
 #define ACMIO_SEN_MAJOR_VERSION  (2)
 #define ACMIO_SEN_MINOR_VERSION  (0)
 #define ACMIO_SEN_PATCH_VERSION  (0)
 #define ACMIO_SEN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Acmio_Sen_HdrBusType Acmio_SenBus;

/* Version Info */
extern const SwcVersionInfo_t Acmio_SenVersionInfo;

/* Input Data Element */
extern Ioc_InputSR1msMotMonInfo_t Acmio_SenMotMonInfo;
extern Ioc_InputSR1msMotVoltsMonInfo_t Acmio_SenMotVoltsMonInfo;
extern Ioc_InputSR50usMotCurrMonInfo_t Acmio_SenMotCurrMonInfo;
extern Ioc_InputSR50usMotAngleMonInfo_t Acmio_SenMotAngleMonInfo;
extern Mom_HndlrEcuModeSts_t Acmio_SenEcuModeSts;
extern Eem_SuspcDetnFuncInhibitAcmioSts_t Acmio_SenFuncInhibitAcmioSts;

/* Output Data Element */
extern Acmio_SenMotCurrInfo_t Acmio_SenMotCurrInfo;
extern Acmio_SenMotAngle1Info_t Acmio_SenMotAngle1Info;
extern Acmio_SenMotAngle2Info_t Acmio_SenMotAngle2Info;
extern Acmio_SenVdcLink_t Acmio_SenVdcLink;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Acmio_Sen_Init(void);
extern void Acmio_Sen(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ACMIO_SEN_H_ */
/** @} */
