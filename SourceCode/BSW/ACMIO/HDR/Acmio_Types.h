/**
 * @defgroup Acmio_Types Acmio_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Acmio_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ACMIO_TYPES_H_
#define ACMIO_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Ioc_InputSR1msMotMonInfo_t Acmio_SenMotMonInfo;
    Ioc_InputSR1msMotVoltsMonInfo_t Acmio_SenMotVoltsMonInfo;
    Ioc_InputSR50usMotCurrMonInfo_t Acmio_SenMotCurrMonInfo;
    Ioc_InputSR50usMotAngleMonInfo_t Acmio_SenMotAngleMonInfo;
    Mom_HndlrEcuModeSts_t Acmio_SenEcuModeSts;
    Eem_SuspcDetnFuncInhibitAcmioSts_t Acmio_SenFuncInhibitAcmioSts;

/* Output Data Element */
    Acmio_SenMotCurrInfo_t Acmio_SenMotCurrInfo;
    Acmio_SenMotAngle1Info_t Acmio_SenMotAngle1Info;
    Acmio_SenMotAngle2Info_t Acmio_SenMotAngle2Info;
    Acmio_SenVdcLink_t Acmio_SenVdcLink;
}Acmio_Sen_HdrBusType;

typedef struct
{
/* Input Data Element */
    Diag_HndlrMotReqDataDiagInfo_t Acmio_ActrMotReqDataDiagInfo;
   // Eem_ResidualModReqMotReqDataEemInfo_t Acmio_ActrMotReqDataEemInfo;
    Acmctl_CtrlMotReqDataAcmctlInfo_t Acmio_ActrMotReqDataAcmctlInfo;
    Mom_HndlrEcuModeSts_t Acmio_ActrEcuModeSts;
    Eem_SuspcDetnFuncInhibitAcmioSts_t Acmio_ActrFuncInhibitAcmioSts;

/* Output Data Element */
    Acmio_ActrMotPwmDataInfo_t Acmio_ActrMotPwmDataInfo;
}Acmio_Actr_HdrBusType;

 typedef struct
 {
    Saluint16        MaxValue;
    Saluint16        MinValue;
    Saluint16        DeadTime;
 }Acmio_DutyPortCfgType;

 typedef struct
 {
    Acmio_DutyPortCfgType    PortU;
    Acmio_DutyPortCfgType    PortV;
    Acmio_DutyPortCfgType    PortW;     
 }Acmio_ActrCfgType;

 typedef struct
 {
     Saluint16  MulFactor;
     Saluint16  DivFactor;
     Saluint16  Offset;
 }Acmio_CurrSnsrCfgType;

 typedef struct
 {
    Saluint16 NumOfHole;
    Saluint16 FullElecAngle;
 }Acmio_MpsSnsrCfgType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ACMIO_TYPES_H_ */
/** @} */
