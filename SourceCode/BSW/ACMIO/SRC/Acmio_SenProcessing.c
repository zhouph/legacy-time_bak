/**
 * @defgroup Acmio Acmio
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Acmio_InputProcessing.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Acmio_Sen.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ACMIO_SEN_START_SEC_CONST_UNSPECIFIED
#include "Acmio_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ACMIO_SEN_STOP_SEC_CONST_UNSPECIFIED
#include "Acmio_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACMIO_SEN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmio_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

#define ACMIO_SEN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmio_MemMap.h"
#define ACMIO_SEN_START_SEC_VAR_NOINIT_32BIT
#include "Acmio_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACMIO_SEN_STOP_SEC_VAR_NOINIT_32BIT
#include "Acmio_MemMap.h"
#define ACMIO_SEN_START_SEC_VAR_UNSPECIFIED
#include "Acmio_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACMIO_SEN_STOP_SEC_VAR_UNSPECIFIED
#include "Acmio_MemMap.h"
#define ACMIO_SEN_START_SEC_VAR_32BIT
#include "Acmio_MemMap.h"
/** Variable Section (32BIT)**/


#define ACMIO_SEN_STOP_SEC_VAR_32BIT
#include "Acmio_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACMIO_SEN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmio_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

 
#define ACMIO_SEN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmio_MemMap.h"
#define ACMIO_SEN_START_SEC_VAR_NOINIT_32BIT
#include "Acmio_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACMIO_SEN_STOP_SEC_VAR_NOINIT_32BIT
#include "Acmio_MemMap.h"
#define ACMIO_SEN_START_SEC_VAR_UNSPECIFIED
#include "Acmio_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACMIO_SEN_STOP_SEC_VAR_UNSPECIFIED
#include "Acmio_MemMap.h"
#define ACMIO_SEN_START_SEC_VAR_32BIT
#include "Acmio_MemMap.h"
/** Variable Section (32BIT)**/


#define ACMIO_SEN_STOP_SEC_VAR_32BIT
#include "Acmio_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ACMIO_SEN_START_SEC_CODE
#include "Acmio_MemMap.h"

void Acmio_InputProcessing(void);

inline static void ACMIO_SenseCurrent(void);
inline static void ACMIO_SenseVoltage(void);
inline static void ACMIO_SenseAngle(void);


/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

void Acmio_InputProcessing(void)
{
    ACMIO_SenseCurrent();
    ACMIO_SenseVoltage();
    ACMIO_SenseAngle();
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
   /****************************************************************************
| NAME:             ACMIO_SenseCurrent
| CALLED BY:        
| PRECONDITIONS:    none
|
| INPUT PARAMETERS: none
|
| RETURN VALUES:    none
|
| DESCRIPTION:     Motor Current Sensingg146
****************************************************************************/
inline static void ACMIO_SenseCurrent(void)
 {
    Haluint16 avgU, avgW;
    Salsint32 currU, currV,currW;

    avgU = (Acmio_SenBus.Acmio_SenMotCurrMonInfo.MotCurrPhUMon0 + Acmio_SenBus.Acmio_SenMotCurrMonInfo.MotCurrPhUMon1) / 2;
    avgW = (Acmio_SenBus.Acmio_SenMotCurrMonInfo.MotCurrPhVMon0 + Acmio_SenBus.Acmio_SenMotCurrMonInfo.MotCurrPhVMon1) / 2;

    currU = (((Salsint32)avgU) - Acmio_CurrSnsrConfig.Offset) * Acmio_CurrSnsrConfig.MulFactor / Acmio_CurrSnsrConfig.DivFactor;
    currW = (((Salsint32)avgW) - Acmio_CurrSnsrConfig.Offset) * Acmio_CurrSnsrConfig.MulFactor / Acmio_CurrSnsrConfig.DivFactor;
    currV = -(currU + currW);

    Acmio_SenBus.Acmio_SenMotCurrInfo.MotCurrPhUMeasd = currU;
    Acmio_SenBus.Acmio_SenMotCurrInfo.MotCurrPhVMeasd = currV;
    Acmio_SenBus.Acmio_SenMotCurrInfo.MotCurrPhWMeasd = currW;
 }
   /****************************************************************************
| NAME:             ACMIO_SenseVoltage
| CALLED BY:        
| PRECONDITIONS:    none
|
| INPUT PARAMETERS: none
|
| RETURN VALUES:    none
|
| DESCRIPTION:     Motor Voltage Sensing
****************************************************************************/

inline static void ACMIO_SenseVoltage(void)
 {
    //Acmio_SenBus.Acmio_SenVdcLink = (Saluint16)Acmio_SenBus.Acmio_SenMotMonInfo.MotVoltPhUMon;
    Acmio_SenBus.Acmio_SenVdcLink = (Saluint16)Acmio_SenBus.Acmio_SenMotMonInfo.MotPwrVoltMon;
 }
   /****************************************************************************
| NAME:             ACMIO_SenseAngle
| CALLED BY:        
| PRECONDITIONS:    none
|
| INPUT PARAMETERS: none
|
| RETURN VALUES:    none
|
| DESCRIPTION:     Motor Angle Sensing
****************************************************************************/

inline static void ACMIO_SenseAngle(void)
 {
    Saluint16 angle1, angle2;

    angle1 = (Saluint16)Acmio_SenBus.Acmio_SenMotAngleMonInfo.MotPosiAngle1raw;
    angle2 = (Saluint16)Acmio_SenBus.Acmio_SenMotAngleMonInfo.MotPosiAngle2raw;

    Acmio_SenBus.Acmio_SenMotAngle1Info.MotMechAngle1 = angle1;
    Acmio_SenBus.Acmio_SenMotAngle1Info.MotElecAngle1 = (angle1 % Acmio_MpsSnsrConfig.FullElecAngle)
                                                * Acmio_MpsSnsrConfig.NumOfHole;

    Acmio_SenBus.Acmio_SenMotAngle2Info.MotMechAngle2 = angle2;
    Acmio_SenBus.Acmio_SenMotAngle2Info.MotElecAngle2 = (angle2 % Acmio_MpsSnsrConfig.FullElecAngle)
                                                * Acmio_MpsSnsrConfig.NumOfHole;
 }
#define ACMIO_SEN_STOP_SEC_CODE
#include "Acmio_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
