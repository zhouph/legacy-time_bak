/**
 * @defgroup Acmio Acmio
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Acmio_OutputProcessing.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Acmio_Actr.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ACMIO_ACTR_START_SEC_CONST_UNSPECIFIED
#include "Acmio_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ACMIO_ACTR_STOP_SEC_CONST_UNSPECIFIED
#include "Acmio_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACMIO_ACTR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmio_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

#define ACMIO_ACTR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmio_MemMap.h"
#define ACMIO_ACTR_START_SEC_VAR_NOINIT_32BIT
#include "Acmio_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACMIO_ACTR_STOP_SEC_VAR_NOINIT_32BIT
#include "Acmio_MemMap.h"
#define ACMIO_ACTR_START_SEC_VAR_UNSPECIFIED
#include "Acmio_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACMIO_ACTR_STOP_SEC_VAR_UNSPECIFIED
#include "Acmio_MemMap.h"
#define ACMIO_ACTR_START_SEC_VAR_32BIT
#include "Acmio_MemMap.h"
/** Variable Section (32BIT)**/


#define ACMIO_ACTR_STOP_SEC_VAR_32BIT
#include "Acmio_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACMIO_ACTR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmio_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

#define ACMIO_ACTR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmio_MemMap.h"
#define ACMIO_ACTR_START_SEC_VAR_NOINIT_32BIT
#include "Acmio_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACMIO_ACTR_STOP_SEC_VAR_NOINIT_32BIT
#include "Acmio_MemMap.h"
#define ACMIO_ACTR_START_SEC_VAR_UNSPECIFIED
#include "Acmio_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACMIO_ACTR_STOP_SEC_VAR_UNSPECIFIED
#include "Acmio_MemMap.h"
#define ACMIO_ACTR_START_SEC_VAR_32BIT
#include "Acmio_MemMap.h"
/** Variable Section (32BIT)**/


#define ACMIO_ACTR_STOP_SEC_VAR_32BIT
#include "Acmio_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ACMIO_ACTR_START_SEC_CODE
#include "Acmio_MemMap.h"

void Acmio_OutputProcessing(void);

inline static void ACMIO_ActuateDiagInput(void);
inline static void ACMIO_ActuateEemInput(void);
inline static void ACMIO_ActuateAcmctlInput(void);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

void Acmio_OutputProcessing(void)
{ 
   ACMIO_ActuateAcmctlInput();
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

/****************************************************************************
| NAME:             ACMIO_ActuateAcmctlInput
| CALLED BY:        
| PRECONDITIONS:    none
|
| INPUT PARAMETERS: none
|
| RETURN VALUES:    none
|
| DESCRIPTION:     AcmCtl Motor Request Data Input
****************************************************************************/
inline static void ACMIO_ActuateAcmctlInput(void)
 {
    static Saluint16 temp_U_High_dutyLimit;
	static Saluint16 temp_U_Low_dutyLimit;
	static Saluint16 temp_V_High_dutyLimit;
	static Saluint16 temp_V_Low_dutyLimit;
	static Saluint16 temp_W_High_dutyLimit;
	static Saluint16 temp_W_Low_dutyLimit;

	temp_U_High_dutyLimit	= (Saluint16)Acmio_ActrBus.Acmio_ActrMotReqDataAcmctlInfo.MotPwmPhUData - Acmio_ActrConfig.PortU.DeadTime;
	temp_U_Low_dutyLimit	= (Saluint16)Acmio_ActrBus.Acmio_ActrMotReqDataAcmctlInfo.MotPwmPhUData + Acmio_ActrConfig.PortU.DeadTime;

	temp_V_High_dutyLimit	= (Saluint16)Acmio_ActrBus.Acmio_ActrMotReqDataAcmctlInfo.MotPwmPhVData - Acmio_ActrConfig.PortV.DeadTime;
	temp_V_Low_dutyLimit	= (Saluint16)Acmio_ActrBus.Acmio_ActrMotReqDataAcmctlInfo.MotPwmPhVData + Acmio_ActrConfig.PortV.DeadTime;

	temp_W_High_dutyLimit	= (Saluint16)Acmio_ActrBus.Acmio_ActrMotReqDataAcmctlInfo.MotPwmPhWData - Acmio_ActrConfig.PortW.DeadTime;
	temp_W_Low_dutyLimit	= (Saluint16)Acmio_ActrBus.Acmio_ActrMotReqDataAcmctlInfo.MotPwmPhWData + Acmio_ActrConfig.PortW.DeadTime;

	if(temp_U_High_dutyLimit < Acmio_ActrConfig.PortU.MinValue)		temp_U_High_dutyLimit = Acmio_ActrConfig.PortU.MinValue;
	if(temp_U_High_dutyLimit > Acmio_ActrConfig.PortU.MaxValue)		temp_U_High_dutyLimit = Acmio_ActrConfig.PortU.MaxValue;	
	if(temp_U_Low_dutyLimit  < Acmio_ActrConfig.PortU.MinValue)		temp_U_Low_dutyLimit  = Acmio_ActrConfig.PortU.MinValue;
	if(temp_U_Low_dutyLimit  > Acmio_ActrConfig.PortU.MaxValue)		temp_U_Low_dutyLimit  = Acmio_ActrConfig.PortU.MaxValue;	

	if(temp_V_High_dutyLimit < Acmio_ActrConfig.PortV.MinValue)		temp_V_High_dutyLimit = Acmio_ActrConfig.PortV.MinValue;
	if(temp_V_High_dutyLimit > Acmio_ActrConfig.PortV.MaxValue)		temp_V_High_dutyLimit = Acmio_ActrConfig.PortV.MaxValue;	
	if(temp_V_Low_dutyLimit  < Acmio_ActrConfig.PortV.MinValue)		temp_V_Low_dutyLimit  = Acmio_ActrConfig.PortV.MinValue;
	if(temp_V_Low_dutyLimit  > Acmio_ActrConfig.PortV.MaxValue)		temp_V_Low_dutyLimit  = Acmio_ActrConfig.PortV.MaxValue;	

	if(temp_W_High_dutyLimit < Acmio_ActrConfig.PortW.MinValue)		temp_W_High_dutyLimit = Acmio_ActrConfig.PortW.MinValue;
	if(temp_W_High_dutyLimit > Acmio_ActrConfig.PortW.MaxValue)		temp_W_High_dutyLimit = Acmio_ActrConfig.PortW.MaxValue;	
	if(temp_W_Low_dutyLimit  < Acmio_ActrConfig.PortW.MinValue)		temp_W_Low_dutyLimit  = Acmio_ActrConfig.PortW.MinValue;
	if(temp_W_Low_dutyLimit  > Acmio_ActrConfig.PortW.MaxValue)		temp_W_Low_dutyLimit  = Acmio_ActrConfig.PortW.MaxValue;

	Acmio_ActrBus.Acmio_ActrMotPwmDataInfo.MotPwmPhUhighData = temp_U_High_dutyLimit;
	Acmio_ActrBus.Acmio_ActrMotPwmDataInfo.MotPwmPhUlowData	 = temp_U_Low_dutyLimit;

	Acmio_ActrBus.Acmio_ActrMotPwmDataInfo.MotPwmPhVhighData = temp_V_High_dutyLimit;
    Acmio_ActrBus.Acmio_ActrMotPwmDataInfo.MotPwmPhVlowData  = temp_V_Low_dutyLimit;
	
    Acmio_ActrBus.Acmio_ActrMotPwmDataInfo.MotPwmPhWhighData = temp_W_High_dutyLimit;
    Acmio_ActrBus.Acmio_ActrMotPwmDataInfo.MotPwmPhWlowData  = temp_W_Low_dutyLimit;
 }

#define ACMIO_ACTR_STOP_SEC_CODE
#include "Acmio_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
