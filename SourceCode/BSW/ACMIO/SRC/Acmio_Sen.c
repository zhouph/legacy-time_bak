/**
 * @defgroup Acmio_Sen Acmio_Sen
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Acmio_Sen.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Acmio_Sen.h"
#include "Acmio_Sen_Ifa.h"
#include "Acmio_SenProcessing.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define SAL_MOT_I_SCALE             (0.1)       // resl. : 0.01 <-- 0.001
/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ACMIO_SEN_START_SEC_CONST_UNSPECIFIED
#include "Acmio_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ACMIO_SEN_STOP_SEC_CONST_UNSPECIFIED
#include "Acmio_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACMIO_SEN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmio_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Acmio_Sen_HdrBusType Acmio_SenBus;

/* Version Info */
const SwcVersionInfo_t Acmio_SenVersionInfo = 
{   
    ACMIO_SEN_MODULE_ID,           /* Acmio_SenVersionInfo.ModuleId */
    ACMIO_SEN_MAJOR_VERSION,       /* Acmio_SenVersionInfo.MajorVer */
    ACMIO_SEN_MINOR_VERSION,       /* Acmio_SenVersionInfo.MinorVer */
    ACMIO_SEN_PATCH_VERSION,       /* Acmio_SenVersionInfo.PatchVer */
    ACMIO_SEN_BRANCH_VERSION       /* Acmio_SenVersionInfo.BranchVer */
};
    
/* Input Data Element */
Ioc_InputSR1msMotMonInfo_t Acmio_SenMotMonInfo;
Ioc_InputSR1msMotVoltsMonInfo_t Acmio_SenMotVoltsMonInfo;
Ioc_InputSR50usMotCurrMonInfo_t Acmio_SenMotCurrMonInfo;
Ioc_InputSR50usMotAngleMonInfo_t Acmio_SenMotAngleMonInfo;
Mom_HndlrEcuModeSts_t Acmio_SenEcuModeSts;
Eem_SuspcDetnFuncInhibitAcmioSts_t Acmio_SenFuncInhibitAcmioSts;

/* Output Data Element */
Acmio_SenMotCurrInfo_t Acmio_SenMotCurrInfo;
Acmio_SenMotAngle1Info_t Acmio_SenMotAngle1Info;
Acmio_SenMotAngle2Info_t Acmio_SenMotAngle2Info;
Acmio_SenVdcLink_t Acmio_SenVdcLink;

uint32 Acmio_Sen_Timer_Start;
uint32 Acmio_Sen_Timer_Elapsed;

#define ACMIO_SEN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmio_MemMap.h"
#define ACMIO_SEN_START_SEC_VAR_NOINIT_32BIT
#include "Acmio_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACMIO_SEN_STOP_SEC_VAR_NOINIT_32BIT
#include "Acmio_MemMap.h"
#define ACMIO_SEN_START_SEC_VAR_UNSPECIFIED
#include "Acmio_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACMIO_SEN_STOP_SEC_VAR_UNSPECIFIED
#include "Acmio_MemMap.h"
#define ACMIO_SEN_START_SEC_VAR_32BIT
#include "Acmio_MemMap.h"
/** Variable Section (32BIT)**/


#define ACMIO_SEN_STOP_SEC_VAR_32BIT
#include "Acmio_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACMIO_SEN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmio_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ACMIO_SEN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmio_MemMap.h"
#define ACMIO_SEN_START_SEC_VAR_NOINIT_32BIT
#include "Acmio_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACMIO_SEN_STOP_SEC_VAR_NOINIT_32BIT
#include "Acmio_MemMap.h"
#define ACMIO_SEN_START_SEC_VAR_UNSPECIFIED
#include "Acmio_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACMIO_SEN_STOP_SEC_VAR_UNSPECIFIED
#include "Acmio_MemMap.h"
#define ACMIO_SEN_START_SEC_VAR_32BIT
#include "Acmio_MemMap.h"
/** Variable Section (32BIT)**/


#define ACMIO_SEN_STOP_SEC_VAR_32BIT
#include "Acmio_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ACMIO_SEN_START_SEC_CODE
#include "Acmio_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Acmio_Sen_Init(void)
{
    /* Initialize internal bus */
    Acmio_SenBus.Acmio_SenMotMonInfo.MotPwrVoltMon = 0;
    Acmio_SenBus.Acmio_SenMotVoltsMonInfo.MotVoltPhUMon = 0;
    Acmio_SenBus.Acmio_SenMotVoltsMonInfo.MotVoltPhVMon = 0;
    Acmio_SenBus.Acmio_SenMotVoltsMonInfo.MotVoltPhWMon = 0;
    Acmio_SenBus.Acmio_SenMotCurrMonInfo.MotCurrPhUMon0 = 0;
    Acmio_SenBus.Acmio_SenMotCurrMonInfo.MotCurrPhUMon1 = 0;
    Acmio_SenBus.Acmio_SenMotCurrMonInfo.MotCurrPhVMon0 = 0;
    Acmio_SenBus.Acmio_SenMotCurrMonInfo.MotCurrPhVMon1 = 0;
    Acmio_SenBus.Acmio_SenMotAngleMonInfo.MotPosiAngle1deg = 0;
    Acmio_SenBus.Acmio_SenMotAngleMonInfo.MotPosiAngle2deg = 0;
    Acmio_SenBus.Acmio_SenMotAngleMonInfo.MotPosiAngle1raw = 0;
    Acmio_SenBus.Acmio_SenMotAngleMonInfo.MotPosiAngle2raw = 0;
    Acmio_SenBus.Acmio_SenEcuModeSts = 0;
    Acmio_SenBus.Acmio_SenFuncInhibitAcmioSts = 0;
    Acmio_SenBus.Acmio_SenMotCurrInfo.MotCurrPhUMeasd = 0;
    Acmio_SenBus.Acmio_SenMotCurrInfo.MotCurrPhVMeasd = 0;
    Acmio_SenBus.Acmio_SenMotCurrInfo.MotCurrPhWMeasd = 0;
    Acmio_SenBus.Acmio_SenMotAngle1Info.MotElecAngle1 = 0;
    Acmio_SenBus.Acmio_SenMotAngle1Info.MotMechAngle1 = 0;
    Acmio_SenBus.Acmio_SenMotAngle2Info.MotElecAngle2 = 0;
    Acmio_SenBus.Acmio_SenMotAngle2Info.MotMechAngle2 = 0;
    Acmio_SenBus.Acmio_SenVdcLink = 0;
}

void Acmio_Sen(void)
{
    Acmio_Sen_Timer_Start = STM0_TIM0.U;

    /* Input */
    /* Decomposed structure interface */
    Acmio_Sen_Read_Acmio_SenMotMonInfo_MotPwrVoltMon(&Acmio_SenBus.Acmio_SenMotMonInfo.MotPwrVoltMon);

    Acmio_Sen_Read_Acmio_SenMotVoltsMonInfo(&Acmio_SenBus.Acmio_SenMotVoltsMonInfo);
    /*==============================================================================
    * Members of structure Acmio_SenMotVoltsMonInfo 
     : Acmio_SenMotVoltsMonInfo.MotVoltPhUMon;
     : Acmio_SenMotVoltsMonInfo.MotVoltPhVMon;
     : Acmio_SenMotVoltsMonInfo.MotVoltPhWMon;
     =============================================================================*/
    
    Acmio_Sen_Read_Acmio_SenMotCurrMonInfo(&Acmio_SenBus.Acmio_SenMotCurrMonInfo);
    /*==============================================================================
    * Members of structure Acmio_SenMotCurrMonInfo 
     : Acmio_SenMotCurrMonInfo.MotCurrPhUMon0;
     : Acmio_SenMotCurrMonInfo.MotCurrPhUMon1;
     : Acmio_SenMotCurrMonInfo.MotCurrPhVMon0;
     : Acmio_SenMotCurrMonInfo.MotCurrPhVMon1;
     =============================================================================*/
    
    Acmio_Sen_Read_Acmio_SenMotAngleMonInfo(&Acmio_SenBus.Acmio_SenMotAngleMonInfo);
    /*==============================================================================
    * Members of structure Acmio_SenMotAngleMonInfo 
     : Acmio_SenMotAngleMonInfo.MotPosiAngle1deg;
     : Acmio_SenMotAngleMonInfo.MotPosiAngle2deg;
     : Acmio_SenMotAngleMonInfo.MotPosiAngle1raw;
     : Acmio_SenMotAngleMonInfo.MotPosiAngle2raw;
     =============================================================================*/
    
    Acmio_Sen_Read_Acmio_SenEcuModeSts(&Acmio_SenBus.Acmio_SenEcuModeSts);
    Acmio_Sen_Read_Acmio_SenFuncInhibitAcmioSts(&Acmio_SenBus.Acmio_SenFuncInhibitAcmioSts);

    /* Process */
    Acmio_InputProcessing();

    /* Output */
    Acmio_Sen_Write_Acmio_SenMotCurrInfo(&Acmio_SenBus.Acmio_SenMotCurrInfo);
    /*==============================================================================
    * Members of structure Acmio_SenMotCurrInfo 
     : Acmio_SenMotCurrInfo.MotCurrPhUMeasd;
     : Acmio_SenMotCurrInfo.MotCurrPhVMeasd;
     : Acmio_SenMotCurrInfo.MotCurrPhWMeasd;
     =============================================================================*/
    
    Acmio_Sen_Write_Acmio_SenMotAngle1Info(&Acmio_SenBus.Acmio_SenMotAngle1Info);
    /*==============================================================================
    * Members of structure Acmio_SenMotAngle1Info 
     : Acmio_SenMotAngle1Info.MotElecAngle1;
     : Acmio_SenMotAngle1Info.MotMechAngle1;
     =============================================================================*/
    
    Acmio_Sen_Write_Acmio_SenMotAngle2Info(&Acmio_SenBus.Acmio_SenMotAngle2Info);
    /*==============================================================================
    * Members of structure Acmio_SenMotAngle2Info 
     : Acmio_SenMotAngle2Info.MotElecAngle2;
     : Acmio_SenMotAngle2Info.MotMechAngle2;
     =============================================================================*/
    
    Acmio_Sen_Write_Acmio_SenVdcLink(&Acmio_SenBus.Acmio_SenVdcLink);

    Acmio_Sen_Timer_Elapsed = STM0_TIM0.U - Acmio_Sen_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define ACMIO_SEN_STOP_SEC_CODE
#include "Acmio_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
