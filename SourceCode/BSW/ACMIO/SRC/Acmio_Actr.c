/**
 * @defgroup Acmio_Actr Acmio_Actr
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Acmio_Actr.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Acmio_Actr.h"
#include "Acmio_Actr_Ifa.h"
#include "Acmio_ActrProcessing.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ACMIO_ACTR_START_SEC_CONST_UNSPECIFIED
#include "Acmio_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ACMIO_ACTR_STOP_SEC_CONST_UNSPECIFIED
#include "Acmio_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACMIO_ACTR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmio_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Acmio_Actr_HdrBusType Acmio_ActrBus;

/* Version Info */
const SwcVersionInfo_t Acmio_ActrVersionInfo = 
{   
    ACMIO_ACTR_MODULE_ID,           /* Acmio_ActrVersionInfo.ModuleId */
    ACMIO_ACTR_MAJOR_VERSION,       /* Acmio_ActrVersionInfo.MajorVer */
    ACMIO_ACTR_MINOR_VERSION,       /* Acmio_ActrVersionInfo.MinorVer */
    ACMIO_ACTR_PATCH_VERSION,       /* Acmio_ActrVersionInfo.PatchVer */
    ACMIO_ACTR_BRANCH_VERSION       /* Acmio_ActrVersionInfo.BranchVer */
};
    
/* Input Data Element */
Diag_HndlrMotReqDataDiagInfo_t Acmio_ActrMotReqDataDiagInfo;
Acmctl_CtrlMotReqDataAcmctlInfo_t Acmio_ActrMotReqDataAcmctlInfo;
Mom_HndlrEcuModeSts_t Acmio_ActrEcuModeSts;
Eem_SuspcDetnFuncInhibitAcmioSts_t Acmio_ActrFuncInhibitAcmioSts;

/* Output Data Element */
Acmio_ActrMotPwmDataInfo_t Acmio_ActrMotPwmDataInfo;

uint32 Acmio_Actr_Timer_Start;
uint32 Acmio_Actr_Timer_Elapsed;

#define ACMIO_ACTR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmio_MemMap.h"
#define ACMIO_ACTR_START_SEC_VAR_NOINIT_32BIT
#include "Acmio_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACMIO_ACTR_STOP_SEC_VAR_NOINIT_32BIT
#include "Acmio_MemMap.h"
#define ACMIO_ACTR_START_SEC_VAR_UNSPECIFIED
#include "Acmio_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACMIO_ACTR_STOP_SEC_VAR_UNSPECIFIED
#include "Acmio_MemMap.h"
#define ACMIO_ACTR_START_SEC_VAR_32BIT
#include "Acmio_MemMap.h"
/** Variable Section (32BIT)**/


#define ACMIO_ACTR_STOP_SEC_VAR_32BIT
#include "Acmio_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACMIO_ACTR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmio_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ACMIO_ACTR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmio_MemMap.h"
#define ACMIO_ACTR_START_SEC_VAR_NOINIT_32BIT
#include "Acmio_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACMIO_ACTR_STOP_SEC_VAR_NOINIT_32BIT
#include "Acmio_MemMap.h"
#define ACMIO_ACTR_START_SEC_VAR_UNSPECIFIED
#include "Acmio_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACMIO_ACTR_STOP_SEC_VAR_UNSPECIFIED
#include "Acmio_MemMap.h"
#define ACMIO_ACTR_START_SEC_VAR_32BIT
#include "Acmio_MemMap.h"
/** Variable Section (32BIT)**/


#define ACMIO_ACTR_STOP_SEC_VAR_32BIT
#include "Acmio_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ACMIO_ACTR_START_SEC_CODE
#include "Acmio_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Acmio_Actr_Init(void)
{
    /* Initialize internal bus */
    Acmio_ActrBus.Acmio_ActrMotReqDataDiagInfo.MotPwmPhUData = 0;
    Acmio_ActrBus.Acmio_ActrMotReqDataDiagInfo.MotPwmPhVData = 0;
    Acmio_ActrBus.Acmio_ActrMotReqDataDiagInfo.MotPwmPhWData = 0;
    Acmio_ActrBus.Acmio_ActrMotReqDataDiagInfo.MotReq = 0;
    Acmio_ActrBus.Acmio_ActrMotReqDataAcmctlInfo.MotPwmPhUData = 0;
    Acmio_ActrBus.Acmio_ActrMotReqDataAcmctlInfo.MotPwmPhVData = 0;
    Acmio_ActrBus.Acmio_ActrMotReqDataAcmctlInfo.MotPwmPhWData = 0;
    Acmio_ActrBus.Acmio_ActrMotReqDataAcmctlInfo.MotReq = 0;
    Acmio_ActrBus.Acmio_ActrEcuModeSts = 0;
    Acmio_ActrBus.Acmio_ActrFuncInhibitAcmioSts = 0;
    Acmio_ActrBus.Acmio_ActrMotPwmDataInfo.MotPwmPhUhighData = 0;
    Acmio_ActrBus.Acmio_ActrMotPwmDataInfo.MotPwmPhVhighData = 0;
    Acmio_ActrBus.Acmio_ActrMotPwmDataInfo.MotPwmPhWhighData = 0;
    Acmio_ActrBus.Acmio_ActrMotPwmDataInfo.MotPwmPhUlowData = 0;
    Acmio_ActrBus.Acmio_ActrMotPwmDataInfo.MotPwmPhVlowData = 0;
    Acmio_ActrBus.Acmio_ActrMotPwmDataInfo.MotPwmPhWlowData = 0;
}

void Acmio_Actr(void)
{
    Acmio_Actr_Timer_Start = STM0_TIM0.U;

    /* Input */
    Acmio_Actr_Read_Acmio_ActrMotReqDataDiagInfo(&Acmio_ActrBus.Acmio_ActrMotReqDataDiagInfo);
    /*==============================================================================
    * Members of structure Acmio_ActrMotReqDataDiagInfo 
     : Acmio_ActrMotReqDataDiagInfo.MotPwmPhUData;
     : Acmio_ActrMotReqDataDiagInfo.MotPwmPhVData;
     : Acmio_ActrMotReqDataDiagInfo.MotPwmPhWData;
     : Acmio_ActrMotReqDataDiagInfo.MotReq;
     =============================================================================*/
    
    Acmio_Actr_Read_Acmio_ActrMotReqDataAcmctlInfo(&Acmio_ActrBus.Acmio_ActrMotReqDataAcmctlInfo);
    /*==============================================================================
    * Members of structure Acmio_ActrMotReqDataAcmctlInfo 
     : Acmio_ActrMotReqDataAcmctlInfo.MotPwmPhUData;
     : Acmio_ActrMotReqDataAcmctlInfo.MotPwmPhVData;
     : Acmio_ActrMotReqDataAcmctlInfo.MotPwmPhWData;
     : Acmio_ActrMotReqDataAcmctlInfo.MotReq;
     =============================================================================*/
    
    Acmio_Actr_Read_Acmio_ActrEcuModeSts(&Acmio_ActrBus.Acmio_ActrEcuModeSts);
    Acmio_Actr_Read_Acmio_ActrFuncInhibitAcmioSts(&Acmio_ActrBus.Acmio_ActrFuncInhibitAcmioSts);

    /* Process */
    Acmio_OutputProcessing();

    /* Output */
    Acmio_Actr_Write_Acmio_ActrMotPwmDataInfo(&Acmio_ActrBus.Acmio_ActrMotPwmDataInfo);
    /*==============================================================================
    * Members of structure Acmio_ActrMotPwmDataInfo 
     : Acmio_ActrMotPwmDataInfo.MotPwmPhUhighData;
     : Acmio_ActrMotPwmDataInfo.MotPwmPhVhighData;
     : Acmio_ActrMotPwmDataInfo.MotPwmPhWhighData;
     : Acmio_ActrMotPwmDataInfo.MotPwmPhUlowData;
     : Acmio_ActrMotPwmDataInfo.MotPwmPhVlowData;
     : Acmio_ActrMotPwmDataInfo.MotPwmPhWlowData;
     =============================================================================*/
    

    Acmio_Actr_Timer_Elapsed = STM0_TIM0.U - Acmio_Actr_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define ACMIO_ACTR_STOP_SEC_CODE
#include "Acmio_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
