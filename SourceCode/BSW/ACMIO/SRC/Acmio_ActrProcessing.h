/**
 * @defgroup Acmio Acmio
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Acmio_OutputProcessing.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ACMIO_OUTPUTPROCESSING_H_
#define ACMIO_OUTPUTPROCESSING_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Acmio_Types.h"
#include "Acmio_Cfg.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Acmio_OutputProcessing(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ACMIO_OUTPUTPROCESSING_H_ */
/** @} */
