#include "unity.h"
#include "unity_fixture.h"
#include "PressM_Main.h"
#include "PressM_Main_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Haluint16 UtInput_PressM_MainHalPressureInfo_McpPresData1[MAX_STEP] = PRESSM_MAINHALPRESSUREINFO_MCPPRESDATA1;
const Haluint16 UtInput_PressM_MainHalPressureInfo_McpPresData2[MAX_STEP] = PRESSM_MAINHALPRESSUREINFO_MCPPRESDATA2;
const Haluint8 UtInput_PressM_MainHalPressureInfo_McpSentCrc[MAX_STEP] = PRESSM_MAINHALPRESSUREINFO_MCPSENTCRC;
const Haluint16 UtInput_PressM_MainHalPressureInfo_McpSentData[MAX_STEP] = PRESSM_MAINHALPRESSUREINFO_MCPSENTDATA;
const Haluint8 UtInput_PressM_MainHalPressureInfo_McpSentMsgId[MAX_STEP] = PRESSM_MAINHALPRESSUREINFO_MCPSENTMSGID;
const Haluint8 UtInput_PressM_MainHalPressureInfo_McpSentSerialCrc[MAX_STEP] = PRESSM_MAINHALPRESSUREINFO_MCPSENTSERIALCRC;
const Haluint8 UtInput_PressM_MainHalPressureInfo_McpSentConfig[MAX_STEP] = PRESSM_MAINHALPRESSUREINFO_MCPSENTCONFIG;
const Haluint16 UtInput_PressM_MainHalPressureInfo_Wlp1PresData1[MAX_STEP] = PRESSM_MAINHALPRESSUREINFO_WLP1PRESDATA1;
const Haluint16 UtInput_PressM_MainHalPressureInfo_Wlp1PresData2[MAX_STEP] = PRESSM_MAINHALPRESSUREINFO_WLP1PRESDATA2;
const Haluint8 UtInput_PressM_MainHalPressureInfo_Wlp1SentCrc[MAX_STEP] = PRESSM_MAINHALPRESSUREINFO_WLP1SENTCRC;
const Haluint16 UtInput_PressM_MainHalPressureInfo_Wlp1SentData[MAX_STEP] = PRESSM_MAINHALPRESSUREINFO_WLP1SENTDATA;
const Haluint8 UtInput_PressM_MainHalPressureInfo_Wlp1SentMsgId[MAX_STEP] = PRESSM_MAINHALPRESSUREINFO_WLP1SENTMSGID;
const Haluint8 UtInput_PressM_MainHalPressureInfo_Wlp1SentSerialCrc[MAX_STEP] = PRESSM_MAINHALPRESSUREINFO_WLP1SENTSERIALCRC;
const Haluint8 UtInput_PressM_MainHalPressureInfo_Wlp1SentConfig[MAX_STEP] = PRESSM_MAINHALPRESSUREINFO_WLP1SENTCONFIG;
const Haluint16 UtInput_PressM_MainHalPressureInfo_Wlp2PresData1[MAX_STEP] = PRESSM_MAINHALPRESSUREINFO_WLP2PRESDATA1;
const Haluint16 UtInput_PressM_MainHalPressureInfo_Wlp2PresData2[MAX_STEP] = PRESSM_MAINHALPRESSUREINFO_WLP2PRESDATA2;
const Haluint8 UtInput_PressM_MainHalPressureInfo_Wlp2SentCrc[MAX_STEP] = PRESSM_MAINHALPRESSUREINFO_WLP2SENTCRC;
const Haluint16 UtInput_PressM_MainHalPressureInfo_Wlp2SentData[MAX_STEP] = PRESSM_MAINHALPRESSUREINFO_WLP2SENTDATA;
const Haluint8 UtInput_PressM_MainHalPressureInfo_Wlp2SentMsgId[MAX_STEP] = PRESSM_MAINHALPRESSUREINFO_WLP2SENTMSGID;
const Haluint8 UtInput_PressM_MainHalPressureInfo_Wlp2SentSerialCrc[MAX_STEP] = PRESSM_MAINHALPRESSUREINFO_WLP2SENTSERIALCRC;
const Haluint8 UtInput_PressM_MainHalPressureInfo_Wlp2SentConfig[MAX_STEP] = PRESSM_MAINHALPRESSUREINFO_WLP2SENTCONFIG;
const Haluint32 UtInput_PressM_MainSentHPressureInfo_McpSentInvalid[MAX_STEP] = PRESSM_MAINSENTHPRESSUREINFO_MCPSENTINVALID;
const Haluint32 UtInput_PressM_MainSentHPressureInfo_MCPSentSerialInvalid[MAX_STEP] = PRESSM_MAINSENTHPRESSUREINFO_MCPSENTSERIALINVALID;
const Haluint32 UtInput_PressM_MainSentHPressureInfo_Wlp1SentInvalid[MAX_STEP] = PRESSM_MAINSENTHPRESSUREINFO_WLP1SENTINVALID;
const Haluint32 UtInput_PressM_MainSentHPressureInfo_Wlp1SentSerialInvalid[MAX_STEP] = PRESSM_MAINSENTHPRESSUREINFO_WLP1SENTSERIALINVALID;
const Haluint32 UtInput_PressM_MainSentHPressureInfo_Wlp2SentInvalid[MAX_STEP] = PRESSM_MAINSENTHPRESSUREINFO_WLP2SENTINVALID;
const Haluint32 UtInput_PressM_MainSentHPressureInfo_Wlp2SentSerialInvalid[MAX_STEP] = PRESSM_MAINSENTHPRESSUREINFO_WLP2SENTSERIALINVALID;
const Mom_HndlrEcuModeSts_t UtInput_PressM_MainEcuModeSts[MAX_STEP] = PRESSM_MAINECUMODESTS;
const Prly_HndlrIgnOnOffSts_t UtInput_PressM_MainIgnOnOffSts[MAX_STEP] = PRESSM_MAINIGNONOFFSTS;
const Prly_HndlrIgnEdgeSts_t UtInput_PressM_MainIgnEdgeSts[MAX_STEP] = PRESSM_MAINIGNEDGESTS;
const Ioc_InputSR1msVBatt1Mon_t UtInput_PressM_MainVBatt1Mon[MAX_STEP] = PRESSM_MAINVBATT1MON;
const Diag_HndlrDiagClr_t UtInput_PressM_MainDiagClrSrs[MAX_STEP] = PRESSM_MAINDIAGCLRSRS;

const Saluint8 UtExpected_PressM_MainPresMonFaultInfo_CIRP1_FC_Fault_Err[MAX_STEP] = PRESSM_MAINPRESMONFAULTINFO_CIRP1_FC_FAULT_ERR;
const Saluint8 UtExpected_PressM_MainPresMonFaultInfo_CIRP1_FC_Mismatch_Err[MAX_STEP] = PRESSM_MAINPRESMONFAULTINFO_CIRP1_FC_MISMATCH_ERR;
const Saluint8 UtExpected_PressM_MainPresMonFaultInfo_CIRP1_FC_CRC_Err[MAX_STEP] = PRESSM_MAINPRESMONFAULTINFO_CIRP1_FC_CRC_ERR;
const Saluint8 UtExpected_PressM_MainPresMonFaultInfo_CIRP1_SC_Temp_Err[MAX_STEP] = PRESSM_MAINPRESMONFAULTINFO_CIRP1_SC_TEMP_ERR;
const Saluint8 UtExpected_PressM_MainPresMonFaultInfo_CIRP1_SC_Mismatch_Err[MAX_STEP] = PRESSM_MAINPRESMONFAULTINFO_CIRP1_SC_MISMATCH_ERR;
const Saluint8 UtExpected_PressM_MainPresMonFaultInfo_CIRP1_SC_CRC_Err[MAX_STEP] = PRESSM_MAINPRESMONFAULTINFO_CIRP1_SC_CRC_ERR;
const Saluint16 UtExpected_PressM_MainPresMonFaultInfo_CIRP1_SC_Diag_Info[MAX_STEP] = PRESSM_MAINPRESMONFAULTINFO_CIRP1_SC_DIAG_INFO;
const Saluint8 UtExpected_PressM_MainPresMonFaultInfo_CIRP2_FC_Fault_Err[MAX_STEP] = PRESSM_MAINPRESMONFAULTINFO_CIRP2_FC_FAULT_ERR;
const Saluint8 UtExpected_PressM_MainPresMonFaultInfo_CIRP2_FC_Mismatch_Err[MAX_STEP] = PRESSM_MAINPRESMONFAULTINFO_CIRP2_FC_MISMATCH_ERR;
const Saluint8 UtExpected_PressM_MainPresMonFaultInfo_CIRP2_FC_CRC_Err[MAX_STEP] = PRESSM_MAINPRESMONFAULTINFO_CIRP2_FC_CRC_ERR;
const Saluint8 UtExpected_PressM_MainPresMonFaultInfo_CIRP2_SC_Temp_Err[MAX_STEP] = PRESSM_MAINPRESMONFAULTINFO_CIRP2_SC_TEMP_ERR;
const Saluint8 UtExpected_PressM_MainPresMonFaultInfo_CIRP2_SC_Mismatch_Err[MAX_STEP] = PRESSM_MAINPRESMONFAULTINFO_CIRP2_SC_MISMATCH_ERR;
const Saluint8 UtExpected_PressM_MainPresMonFaultInfo_CIRP2_SC_CRC_Err[MAX_STEP] = PRESSM_MAINPRESMONFAULTINFO_CIRP2_SC_CRC_ERR;
const Saluint16 UtExpected_PressM_MainPresMonFaultInfo_CIRP2_SC_Diag_Info[MAX_STEP] = PRESSM_MAINPRESMONFAULTINFO_CIRP2_SC_DIAG_INFO;
const Saluint8 UtExpected_PressM_MainPresMonFaultInfo_SIMP_FC_Fault_Err[MAX_STEP] = PRESSM_MAINPRESMONFAULTINFO_SIMP_FC_FAULT_ERR;
const Saluint8 UtExpected_PressM_MainPresMonFaultInfo_SIMP_FC_Mismatch_Err[MAX_STEP] = PRESSM_MAINPRESMONFAULTINFO_SIMP_FC_MISMATCH_ERR;
const Saluint8 UtExpected_PressM_MainPresMonFaultInfo_SIMP_FC_CRC_Err[MAX_STEP] = PRESSM_MAINPRESMONFAULTINFO_SIMP_FC_CRC_ERR;
const Saluint8 UtExpected_PressM_MainPresMonFaultInfo_SIMP_SC_Temp_Err[MAX_STEP] = PRESSM_MAINPRESMONFAULTINFO_SIMP_SC_TEMP_ERR;
const Saluint8 UtExpected_PressM_MainPresMonFaultInfo_SIMP_SC_Mismatch_Err[MAX_STEP] = PRESSM_MAINPRESMONFAULTINFO_SIMP_SC_MISMATCH_ERR;
const Saluint8 UtExpected_PressM_MainPresMonFaultInfo_SIMP_SC_CRC_Err[MAX_STEP] = PRESSM_MAINPRESMONFAULTINFO_SIMP_SC_CRC_ERR;
const Saluint16 UtExpected_PressM_MainPresMonFaultInfo_SIMP_SC_Diag_Info[MAX_STEP] = PRESSM_MAINPRESMONFAULTINFO_SIMP_SC_DIAG_INFO;



TEST_GROUP(PressM_Main);
TEST_SETUP(PressM_Main)
{
    PressM_Main_Init();
}

TEST_TEAR_DOWN(PressM_Main)
{   /* Postcondition */

}

TEST(PressM_Main, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        PressM_MainHalPressureInfo.McpPresData1 = UtInput_PressM_MainHalPressureInfo_McpPresData1[i];
        PressM_MainHalPressureInfo.McpPresData2 = UtInput_PressM_MainHalPressureInfo_McpPresData2[i];
        PressM_MainHalPressureInfo.McpSentCrc = UtInput_PressM_MainHalPressureInfo_McpSentCrc[i];
        PressM_MainHalPressureInfo.McpSentData = UtInput_PressM_MainHalPressureInfo_McpSentData[i];
        PressM_MainHalPressureInfo.McpSentMsgId = UtInput_PressM_MainHalPressureInfo_McpSentMsgId[i];
        PressM_MainHalPressureInfo.McpSentSerialCrc = UtInput_PressM_MainHalPressureInfo_McpSentSerialCrc[i];
        PressM_MainHalPressureInfo.McpSentConfig = UtInput_PressM_MainHalPressureInfo_McpSentConfig[i];
        PressM_MainHalPressureInfo.Wlp1PresData1 = UtInput_PressM_MainHalPressureInfo_Wlp1PresData1[i];
        PressM_MainHalPressureInfo.Wlp1PresData2 = UtInput_PressM_MainHalPressureInfo_Wlp1PresData2[i];
        PressM_MainHalPressureInfo.Wlp1SentCrc = UtInput_PressM_MainHalPressureInfo_Wlp1SentCrc[i];
        PressM_MainHalPressureInfo.Wlp1SentData = UtInput_PressM_MainHalPressureInfo_Wlp1SentData[i];
        PressM_MainHalPressureInfo.Wlp1SentMsgId = UtInput_PressM_MainHalPressureInfo_Wlp1SentMsgId[i];
        PressM_MainHalPressureInfo.Wlp1SentSerialCrc = UtInput_PressM_MainHalPressureInfo_Wlp1SentSerialCrc[i];
        PressM_MainHalPressureInfo.Wlp1SentConfig = UtInput_PressM_MainHalPressureInfo_Wlp1SentConfig[i];
        PressM_MainHalPressureInfo.Wlp2PresData1 = UtInput_PressM_MainHalPressureInfo_Wlp2PresData1[i];
        PressM_MainHalPressureInfo.Wlp2PresData2 = UtInput_PressM_MainHalPressureInfo_Wlp2PresData2[i];
        PressM_MainHalPressureInfo.Wlp2SentCrc = UtInput_PressM_MainHalPressureInfo_Wlp2SentCrc[i];
        PressM_MainHalPressureInfo.Wlp2SentData = UtInput_PressM_MainHalPressureInfo_Wlp2SentData[i];
        PressM_MainHalPressureInfo.Wlp2SentMsgId = UtInput_PressM_MainHalPressureInfo_Wlp2SentMsgId[i];
        PressM_MainHalPressureInfo.Wlp2SentSerialCrc = UtInput_PressM_MainHalPressureInfo_Wlp2SentSerialCrc[i];
        PressM_MainHalPressureInfo.Wlp2SentConfig = UtInput_PressM_MainHalPressureInfo_Wlp2SentConfig[i];
        PressM_MainSentHPressureInfo.McpSentInvalid = UtInput_PressM_MainSentHPressureInfo_McpSentInvalid[i];
        PressM_MainSentHPressureInfo.MCPSentSerialInvalid = UtInput_PressM_MainSentHPressureInfo_MCPSentSerialInvalid[i];
        PressM_MainSentHPressureInfo.Wlp1SentInvalid = UtInput_PressM_MainSentHPressureInfo_Wlp1SentInvalid[i];
        PressM_MainSentHPressureInfo.Wlp1SentSerialInvalid = UtInput_PressM_MainSentHPressureInfo_Wlp1SentSerialInvalid[i];
        PressM_MainSentHPressureInfo.Wlp2SentInvalid = UtInput_PressM_MainSentHPressureInfo_Wlp2SentInvalid[i];
        PressM_MainSentHPressureInfo.Wlp2SentSerialInvalid = UtInput_PressM_MainSentHPressureInfo_Wlp2SentSerialInvalid[i];
        PressM_MainEcuModeSts = UtInput_PressM_MainEcuModeSts[i];
        PressM_MainIgnOnOffSts = UtInput_PressM_MainIgnOnOffSts[i];
        PressM_MainIgnEdgeSts = UtInput_PressM_MainIgnEdgeSts[i];
        PressM_MainVBatt1Mon = UtInput_PressM_MainVBatt1Mon[i];
        PressM_MainDiagClrSrs = UtInput_PressM_MainDiagClrSrs[i];

        PressM_Main();

        TEST_ASSERT_EQUAL(PressM_MainPresMonFaultInfo.CIRP1_FC_Fault_Err, UtExpected_PressM_MainPresMonFaultInfo_CIRP1_FC_Fault_Err[i]);
        TEST_ASSERT_EQUAL(PressM_MainPresMonFaultInfo.CIRP1_FC_Mismatch_Err, UtExpected_PressM_MainPresMonFaultInfo_CIRP1_FC_Mismatch_Err[i]);
        TEST_ASSERT_EQUAL(PressM_MainPresMonFaultInfo.CIRP1_FC_CRC_Err, UtExpected_PressM_MainPresMonFaultInfo_CIRP1_FC_CRC_Err[i]);
        TEST_ASSERT_EQUAL(PressM_MainPresMonFaultInfo.CIRP1_SC_Temp_Err, UtExpected_PressM_MainPresMonFaultInfo_CIRP1_SC_Temp_Err[i]);
        TEST_ASSERT_EQUAL(PressM_MainPresMonFaultInfo.CIRP1_SC_Mismatch_Err, UtExpected_PressM_MainPresMonFaultInfo_CIRP1_SC_Mismatch_Err[i]);
        TEST_ASSERT_EQUAL(PressM_MainPresMonFaultInfo.CIRP1_SC_CRC_Err, UtExpected_PressM_MainPresMonFaultInfo_CIRP1_SC_CRC_Err[i]);
        TEST_ASSERT_EQUAL(PressM_MainPresMonFaultInfo.CIRP1_SC_Diag_Info, UtExpected_PressM_MainPresMonFaultInfo_CIRP1_SC_Diag_Info[i]);
        TEST_ASSERT_EQUAL(PressM_MainPresMonFaultInfo.CIRP2_FC_Fault_Err, UtExpected_PressM_MainPresMonFaultInfo_CIRP2_FC_Fault_Err[i]);
        TEST_ASSERT_EQUAL(PressM_MainPresMonFaultInfo.CIRP2_FC_Mismatch_Err, UtExpected_PressM_MainPresMonFaultInfo_CIRP2_FC_Mismatch_Err[i]);
        TEST_ASSERT_EQUAL(PressM_MainPresMonFaultInfo.CIRP2_FC_CRC_Err, UtExpected_PressM_MainPresMonFaultInfo_CIRP2_FC_CRC_Err[i]);
        TEST_ASSERT_EQUAL(PressM_MainPresMonFaultInfo.CIRP2_SC_Temp_Err, UtExpected_PressM_MainPresMonFaultInfo_CIRP2_SC_Temp_Err[i]);
        TEST_ASSERT_EQUAL(PressM_MainPresMonFaultInfo.CIRP2_SC_Mismatch_Err, UtExpected_PressM_MainPresMonFaultInfo_CIRP2_SC_Mismatch_Err[i]);
        TEST_ASSERT_EQUAL(PressM_MainPresMonFaultInfo.CIRP2_SC_CRC_Err, UtExpected_PressM_MainPresMonFaultInfo_CIRP2_SC_CRC_Err[i]);
        TEST_ASSERT_EQUAL(PressM_MainPresMonFaultInfo.CIRP2_SC_Diag_Info, UtExpected_PressM_MainPresMonFaultInfo_CIRP2_SC_Diag_Info[i]);
        TEST_ASSERT_EQUAL(PressM_MainPresMonFaultInfo.SIMP_FC_Fault_Err, UtExpected_PressM_MainPresMonFaultInfo_SIMP_FC_Fault_Err[i]);
        TEST_ASSERT_EQUAL(PressM_MainPresMonFaultInfo.SIMP_FC_Mismatch_Err, UtExpected_PressM_MainPresMonFaultInfo_SIMP_FC_Mismatch_Err[i]);
        TEST_ASSERT_EQUAL(PressM_MainPresMonFaultInfo.SIMP_FC_CRC_Err, UtExpected_PressM_MainPresMonFaultInfo_SIMP_FC_CRC_Err[i]);
        TEST_ASSERT_EQUAL(PressM_MainPresMonFaultInfo.SIMP_SC_Temp_Err, UtExpected_PressM_MainPresMonFaultInfo_SIMP_SC_Temp_Err[i]);
        TEST_ASSERT_EQUAL(PressM_MainPresMonFaultInfo.SIMP_SC_Mismatch_Err, UtExpected_PressM_MainPresMonFaultInfo_SIMP_SC_Mismatch_Err[i]);
        TEST_ASSERT_EQUAL(PressM_MainPresMonFaultInfo.SIMP_SC_CRC_Err, UtExpected_PressM_MainPresMonFaultInfo_SIMP_SC_CRC_Err[i]);
        TEST_ASSERT_EQUAL(PressM_MainPresMonFaultInfo.SIMP_SC_Diag_Info, UtExpected_PressM_MainPresMonFaultInfo_SIMP_SC_Diag_Info[i]);
    }
}

TEST_GROUP_RUNNER(PressM_Main)
{
    RUN_TEST_CASE(PressM_Main, All);
}
