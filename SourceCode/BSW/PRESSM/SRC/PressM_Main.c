/**
 * @defgroup PressM_Main PressM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        PressM_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "PressM_Main.h"
#include "PressM_Process.h"

#include "PressM_Main_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PRESSM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "PressM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PRESSM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "PressM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PRESSM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "PressM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
PressM_Main_HdrBusType PressM_MainBus;

/* Version Info */
const SwcVersionInfo_t PressM_MainVersionInfo = 
{   
    PRESSM_MAIN_MODULE_ID,           /* PressM_MainVersionInfo.ModuleId */
    PRESSM_MAIN_MAJOR_VERSION,       /* PressM_MainVersionInfo.MajorVer */
    PRESSM_MAIN_MINOR_VERSION,       /* PressM_MainVersionInfo.MinorVer */
    PRESSM_MAIN_PATCH_VERSION,       /* PressM_MainVersionInfo.PatchVer */
    PRESSM_MAIN_BRANCH_VERSION       /* PressM_MainVersionInfo.BranchVer */
};
    
/* Input Data Element */
Ioc_InputSR1msHalPressureInfo_t PressM_MainHalPressureInfo;
SentH_MainSentHPressureInfo_t PressM_MainSentHPressureInfo;
Mom_HndlrEcuModeSts_t PressM_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t PressM_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t PressM_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t PressM_MainVBatt1Mon;
Diag_HndlrDiagClr_t PressM_MainDiagClrSrs;

/* Output Data Element */
PressM_MainPresMonFaultInfo_t PressM_MainPresMonFaultInfo;

uint32 PressM_Main_Timer_Start;
uint32 PressM_Main_Timer_Elapsed;

#define PRESSM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "PressM_MemMap.h"
#define PRESSM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "PressM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PRESSM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "PressM_MemMap.h"
#define PRESSM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "PressM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PRESSM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "PressM_MemMap.h"
#define PRESSM_MAIN_START_SEC_VAR_32BIT
#include "PressM_MemMap.h"
/** Variable Section (32BIT)**/


#define PRESSM_MAIN_STOP_SEC_VAR_32BIT
#include "PressM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PRESSM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "PressM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PRESSM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "PressM_MemMap.h"
#define PRESSM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "PressM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PRESSM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "PressM_MemMap.h"
#define PRESSM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "PressM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PRESSM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "PressM_MemMap.h"
#define PRESSM_MAIN_START_SEC_VAR_32BIT
#include "PressM_MemMap.h"
/** Variable Section (32BIT)**/


#define PRESSM_MAIN_STOP_SEC_VAR_32BIT
#include "PressM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PRESSM_MAIN_START_SEC_CODE
#include "PressM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void PressM_Main_Init(void)
{
    /* Initialize internal bus */
    PressM_MainBus.PressM_MainHalPressureInfo.McpPresData1 = 0;
    PressM_MainBus.PressM_MainHalPressureInfo.McpPresData2 = 0;
    PressM_MainBus.PressM_MainHalPressureInfo.McpSentCrc = 0;
    PressM_MainBus.PressM_MainHalPressureInfo.McpSentData = 0;
    PressM_MainBus.PressM_MainHalPressureInfo.McpSentMsgId = 0;
    PressM_MainBus.PressM_MainHalPressureInfo.McpSentSerialCrc = 0;
    PressM_MainBus.PressM_MainHalPressureInfo.McpSentConfig = 0;
    PressM_MainBus.PressM_MainHalPressureInfo.Wlp1PresData1 = 0;
    PressM_MainBus.PressM_MainHalPressureInfo.Wlp1PresData2 = 0;
    PressM_MainBus.PressM_MainHalPressureInfo.Wlp1SentCrc = 0;
    PressM_MainBus.PressM_MainHalPressureInfo.Wlp1SentData = 0;
    PressM_MainBus.PressM_MainHalPressureInfo.Wlp1SentMsgId = 0;
    PressM_MainBus.PressM_MainHalPressureInfo.Wlp1SentSerialCrc = 0;
    PressM_MainBus.PressM_MainHalPressureInfo.Wlp1SentConfig = 0;
    PressM_MainBus.PressM_MainHalPressureInfo.Wlp2PresData1 = 0;
    PressM_MainBus.PressM_MainHalPressureInfo.Wlp2PresData2 = 0;
    PressM_MainBus.PressM_MainHalPressureInfo.Wlp2SentCrc = 0;
    PressM_MainBus.PressM_MainHalPressureInfo.Wlp2SentData = 0;
    PressM_MainBus.PressM_MainHalPressureInfo.Wlp2SentMsgId = 0;
    PressM_MainBus.PressM_MainHalPressureInfo.Wlp2SentSerialCrc = 0;
    PressM_MainBus.PressM_MainHalPressureInfo.Wlp2SentConfig = 0;
    PressM_MainBus.PressM_MainSentHPressureInfo.McpSentInvalid = 0;
    PressM_MainBus.PressM_MainSentHPressureInfo.MCPSentSerialInvalid = 0;
    PressM_MainBus.PressM_MainSentHPressureInfo.Wlp1SentInvalid = 0;
    PressM_MainBus.PressM_MainSentHPressureInfo.Wlp1SentSerialInvalid = 0;
    PressM_MainBus.PressM_MainSentHPressureInfo.Wlp2SentInvalid = 0;
    PressM_MainBus.PressM_MainSentHPressureInfo.Wlp2SentSerialInvalid = 0;
    PressM_MainBus.PressM_MainEcuModeSts = 0;
    PressM_MainBus.PressM_MainIgnOnOffSts = 0;
    PressM_MainBus.PressM_MainIgnEdgeSts = 0;
    PressM_MainBus.PressM_MainVBatt1Mon = 0;
    PressM_MainBus.PressM_MainDiagClrSrs = 0;
    PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP1_FC_Fault_Err = 0;
    PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP1_FC_Mismatch_Err = 0;
    PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP1_FC_CRC_Err = 0;
    PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP1_SC_Temp_Err = 0;
    PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP1_SC_Mismatch_Err = 0;
    PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP1_SC_CRC_Err = 0;
    PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP1_SC_Diag_Info = 0;
    PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP2_FC_Fault_Err = 0;
    PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP2_FC_Mismatch_Err = 0;
    PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP2_FC_CRC_Err = 0;
    PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP2_SC_Temp_Err = 0;
    PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP2_SC_Mismatch_Err = 0;
    PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP2_SC_CRC_Err = 0;
    PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP2_SC_Diag_Info = 0;
    PressM_MainBus.PressM_MainPresMonFaultInfo.SIMP_FC_Fault_Err = 0;
    PressM_MainBus.PressM_MainPresMonFaultInfo.SIMP_FC_Mismatch_Err = 0;
    PressM_MainBus.PressM_MainPresMonFaultInfo.SIMP_FC_CRC_Err = 0;
    PressM_MainBus.PressM_MainPresMonFaultInfo.SIMP_SC_Temp_Err = 0;
    PressM_MainBus.PressM_MainPresMonFaultInfo.SIMP_SC_Mismatch_Err = 0;
    PressM_MainBus.PressM_MainPresMonFaultInfo.SIMP_SC_CRC_Err = 0;
    PressM_MainBus.PressM_MainPresMonFaultInfo.SIMP_SC_Diag_Info = 0;
}

void PressM_Main(void)
{
    PressM_Main_Timer_Start = STM0_TIM0.U;

    /* Input */
    PressM_Main_Read_PressM_MainHalPressureInfo(&PressM_MainBus.PressM_MainHalPressureInfo);
    /*==============================================================================
    * Members of structure PressM_MainHalPressureInfo 
     : PressM_MainHalPressureInfo.McpPresData1;
     : PressM_MainHalPressureInfo.McpPresData2;
     : PressM_MainHalPressureInfo.McpSentCrc;
     : PressM_MainHalPressureInfo.McpSentData;
     : PressM_MainHalPressureInfo.McpSentMsgId;
     : PressM_MainHalPressureInfo.McpSentSerialCrc;
     : PressM_MainHalPressureInfo.McpSentConfig;
     : PressM_MainHalPressureInfo.Wlp1PresData1;
     : PressM_MainHalPressureInfo.Wlp1PresData2;
     : PressM_MainHalPressureInfo.Wlp1SentCrc;
     : PressM_MainHalPressureInfo.Wlp1SentData;
     : PressM_MainHalPressureInfo.Wlp1SentMsgId;
     : PressM_MainHalPressureInfo.Wlp1SentSerialCrc;
     : PressM_MainHalPressureInfo.Wlp1SentConfig;
     : PressM_MainHalPressureInfo.Wlp2PresData1;
     : PressM_MainHalPressureInfo.Wlp2PresData2;
     : PressM_MainHalPressureInfo.Wlp2SentCrc;
     : PressM_MainHalPressureInfo.Wlp2SentData;
     : PressM_MainHalPressureInfo.Wlp2SentMsgId;
     : PressM_MainHalPressureInfo.Wlp2SentSerialCrc;
     : PressM_MainHalPressureInfo.Wlp2SentConfig;
     =============================================================================*/
    
    /* Decomposed structure interface */
    PressM_Main_Read_PressM_MainSentHPressureInfo_McpSentInvalid(&PressM_MainBus.PressM_MainSentHPressureInfo.McpSentInvalid);
    PressM_Main_Read_PressM_MainSentHPressureInfo_MCPSentSerialInvalid(&PressM_MainBus.PressM_MainSentHPressureInfo.MCPSentSerialInvalid);
    PressM_Main_Read_PressM_MainSentHPressureInfo_Wlp1SentInvalid(&PressM_MainBus.PressM_MainSentHPressureInfo.Wlp1SentInvalid);
    PressM_Main_Read_PressM_MainSentHPressureInfo_Wlp1SentSerialInvalid(&PressM_MainBus.PressM_MainSentHPressureInfo.Wlp1SentSerialInvalid);
    PressM_Main_Read_PressM_MainSentHPressureInfo_Wlp2SentInvalid(&PressM_MainBus.PressM_MainSentHPressureInfo.Wlp2SentInvalid);
    PressM_Main_Read_PressM_MainSentHPressureInfo_Wlp2SentSerialInvalid(&PressM_MainBus.PressM_MainSentHPressureInfo.Wlp2SentSerialInvalid);

    PressM_Main_Read_PressM_MainEcuModeSts(&PressM_MainBus.PressM_MainEcuModeSts);
    PressM_Main_Read_PressM_MainIgnOnOffSts(&PressM_MainBus.PressM_MainIgnOnOffSts);
    PressM_Main_Read_PressM_MainIgnEdgeSts(&PressM_MainBus.PressM_MainIgnEdgeSts);
    PressM_Main_Read_PressM_MainVBatt1Mon(&PressM_MainBus.PressM_MainVBatt1Mon);
    PressM_Main_Read_PressM_MainDiagClrSrs(&PressM_MainBus.PressM_MainDiagClrSrs);

    /* Process */
	if(PressM_MainBus.PressM_MainDiagClrSrs == 1)
	{
		PressM_Main_Init();
	}
	PressM_Process();
    /* Output */
    PressM_Main_Write_PressM_MainPresMonFaultInfo(&PressM_MainBus.PressM_MainPresMonFaultInfo);
    /*==============================================================================
    * Members of structure PressM_MainPresMonFaultInfo 
     : PressM_MainPresMonFaultInfo.CIRP1_FC_Fault_Err;
     : PressM_MainPresMonFaultInfo.CIRP1_FC_Mismatch_Err;
     : PressM_MainPresMonFaultInfo.CIRP1_FC_CRC_Err;
     : PressM_MainPresMonFaultInfo.CIRP1_SC_Temp_Err;
     : PressM_MainPresMonFaultInfo.CIRP1_SC_Mismatch_Err;
     : PressM_MainPresMonFaultInfo.CIRP1_SC_CRC_Err;
     : PressM_MainPresMonFaultInfo.CIRP1_SC_Diag_Info;
     : PressM_MainPresMonFaultInfo.CIRP2_FC_Fault_Err;
     : PressM_MainPresMonFaultInfo.CIRP2_FC_Mismatch_Err;
     : PressM_MainPresMonFaultInfo.CIRP2_FC_CRC_Err;
     : PressM_MainPresMonFaultInfo.CIRP2_SC_Temp_Err;
     : PressM_MainPresMonFaultInfo.CIRP2_SC_Mismatch_Err;
     : PressM_MainPresMonFaultInfo.CIRP2_SC_CRC_Err;
     : PressM_MainPresMonFaultInfo.CIRP2_SC_Diag_Info;
     : PressM_MainPresMonFaultInfo.SIMP_FC_Fault_Err;
     : PressM_MainPresMonFaultInfo.SIMP_FC_Mismatch_Err;
     : PressM_MainPresMonFaultInfo.SIMP_FC_CRC_Err;
     : PressM_MainPresMonFaultInfo.SIMP_SC_Temp_Err;
     : PressM_MainPresMonFaultInfo.SIMP_SC_Mismatch_Err;
     : PressM_MainPresMonFaultInfo.SIMP_SC_CRC_Err;
     : PressM_MainPresMonFaultInfo.SIMP_SC_Diag_Info;
     =============================================================================*/
    

    PressM_Main_Timer_Elapsed = STM0_TIM0.U - PressM_Main_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define PRESSM_MAIN_STOP_SEC_CODE
#include "PressM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
