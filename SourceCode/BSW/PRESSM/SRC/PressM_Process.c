/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Common.h"
#include "PressM_Main.h"
#include "PressM_Cfg.h"


/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
static PressMCfg *pPressM_Cfg;
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
static void PressM_FaultCheck(void);
static void PressM_MisMatchCheck(void);
static Std_ReturnType PressM_Compare2WordBitRev(uint16 origin, uint16 bitreverse);
static uint16 bit_reverse_word (uint16 value);
	

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void PressM_Process(void)
{
    PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP1_FC_Fault_Err 	 = ERR_NONE;
    PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP2_FC_Fault_Err 	 = ERR_NONE;
    PressM_MainBus.PressM_MainPresMonFaultInfo.SIMP_FC_Fault_Err 	 = ERR_NONE;

    PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP1_FC_Mismatch_Err = ERR_NONE;
    PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP2_FC_Mismatch_Err = ERR_NONE;
    PressM_MainBus.PressM_MainPresMonFaultInfo.SIMP_FC_Mismatch_Err  = ERR_NONE;

	pPressM_Cfg = PressM_GetCfg();
	if(PressM_MainBus.PressM_MainVBatt1Mon < PRESSM_7V0 || PressM_MainBus.PressM_MainVBatt1Mon > PRESSM_17V0)
	{
	    PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP1_FC_Fault_Err 	 = ERR_INHIBIT;
	    PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP2_FC_Fault_Err 	 = ERR_INHIBIT;
	    PressM_MainBus.PressM_MainPresMonFaultInfo.SIMP_FC_Fault_Err 	 = ERR_INHIBIT;

	    PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP1_FC_Mismatch_Err = ERR_INHIBIT;
	    PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP2_FC_Mismatch_Err = ERR_INHIBIT;
	    PressM_MainBus.PressM_MainPresMonFaultInfo.SIMP_FC_Mismatch_Err  = ERR_INHIBIT;
	}
	else
	{
		PressM_FaultCheck();
		PressM_MisMatchCheck();
	}
}

void PressM_Init(void)
{
	PressM_InitCfg();


    PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP1_FC_Fault_Err	 = ERR_NONE;
	PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP2_FC_Fault_Err	 = ERR_NONE;
    PressM_MainBus.PressM_MainPresMonFaultInfo.SIMP_FC_Fault_Err 	 = ERR_NONE;

	PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP1_FC_Mismatch_Err = ERR_NONE;
    PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP2_FC_Mismatch_Err = ERR_NONE;
	PressM_MainBus.PressM_MainPresMonFaultInfo.SIMP_FC_Mismatch_Err  = ERR_NONE;
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

static void PressM_FaultCheck(void)
{
    PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP1_FC_Fault_Err	 = ERR_NONE;
	PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP2_FC_Fault_Err	 = ERR_NONE;
    PressM_MainBus.PressM_MainPresMonFaultInfo.SIMP_FC_Fault_Err 	 = ERR_NONE;

	if(pPressM_Cfg->PressM_SIMP_type == SENT_TYPE)
	{
		if((PressM_MainBus.PressM_MainHalPressureInfo.McpPresData1	== PRES_SENT_FAULT_VALUE1) ||
			(PressM_MainBus.PressM_MainHalPressureInfo.McpPresData1 == PRES_SENT_FAULT_VALUE2) || 			
			(PressM_MainBus.PressM_MainHalPressureInfo.McpPresData2 == PRES_SENT_FAULT_VALUE1) ||
			(PressM_MainBus.PressM_MainHalPressureInfo.McpPresData2 == PRES_SENT_FAULT_VALUE2))
		{
			PressM_MainBus.PressM_MainPresMonFaultInfo.SIMP_FC_Fault_Err = ERR_PREFAILED;
		}
		else
		{
			PressM_MainBus.PressM_MainPresMonFaultInfo.SIMP_FC_Fault_Err = ERR_PREPASSED;
		}
	}
	else
	{
		PressM_MainBus.PressM_MainPresMonFaultInfo.SIMP_FC_Fault_Err = ERR_INHIBIT;
	}
	
	if(pPressM_Cfg->PressM_CIRP1_type == SENT_TYPE)
	{
		if((PressM_MainBus.PressM_MainHalPressureInfo.Wlp1PresData1  == PRES_SENT_FAULT_VALUE1) || 
			(PressM_MainBus.PressM_MainHalPressureInfo.Wlp1PresData1 == PRES_SENT_FAULT_VALUE2) || 
			(PressM_MainBus.PressM_MainHalPressureInfo.Wlp1PresData2 == PRES_SENT_FAULT_VALUE1) ||
			(PressM_MainBus.PressM_MainHalPressureInfo.Wlp1PresData2 == PRES_SENT_FAULT_VALUE2))
		{
			PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP1_FC_Fault_Err = ERR_PREFAILED;
		}
		else
		{
			PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP1_FC_Fault_Err = ERR_PREPASSED;
		}
	}
	else
	{
		PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP1_FC_Fault_Err	= ERR_INHIBIT;
	}

	if(pPressM_Cfg->PressM_CIRP2_type == SENT_TYPE)
	{
		if((PressM_MainBus.PressM_MainHalPressureInfo.Wlp2PresData1  == PRES_SENT_FAULT_VALUE1) || 
			(PressM_MainBus.PressM_MainHalPressureInfo.Wlp2PresData1 == PRES_SENT_FAULT_VALUE2) || 
			(PressM_MainBus.PressM_MainHalPressureInfo.Wlp2PresData2 == PRES_SENT_FAULT_VALUE1) ||
			(PressM_MainBus.PressM_MainHalPressureInfo.Wlp2PresData2 == PRES_SENT_FAULT_VALUE2))
		{
			PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP2_FC_Fault_Err = ERR_PREFAILED;
		}
		else
		{
			PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP2_FC_Fault_Err = ERR_PREPASSED;
		}
	}
	else
	{
		PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP2_FC_Fault_Err	= ERR_INHIBIT;
	}


}

static void PressM_MisMatchCheck(void)
{
	uint8 u8Result = 0;
	
	PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP1_FC_Mismatch_Err = ERR_NONE;
    PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP2_FC_Mismatch_Err = ERR_NONE;
	PressM_MainBus.PressM_MainPresMonFaultInfo.SIMP_FC_Mismatch_Err  = ERR_NONE;

	if(pPressM_Cfg->PressM_SIMP_type == SENT_TYPE)
	{
		u8Result = PressM_Compare2WordBitRev(PressM_MainBus.PressM_MainHalPressureInfo.McpPresData1, PressM_MainBus.PressM_MainHalPressureInfo.McpPresData2);
		if(u8Result != E_OK)
		{
			PressM_MainBus.PressM_MainPresMonFaultInfo.SIMP_FC_Mismatch_Err	= ERR_PREFAILED;
		}
		else
		{
			PressM_MainBus.PressM_MainPresMonFaultInfo.SIMP_FC_Mismatch_Err	= ERR_PREPASSED;
		}
	}
	else
	{
		PressM_MainBus.PressM_MainPresMonFaultInfo.SIMP_FC_Mismatch_Err	= ERR_INHIBIT;
	}

	if(pPressM_Cfg->PressM_CIRP1_type == SENT_TYPE)
	{
		u8Result = PressM_Compare2WordBitRev(PressM_MainBus.PressM_MainHalPressureInfo.Wlp1PresData1, PressM_MainBus.PressM_MainHalPressureInfo.Wlp1PresData2);
		if(u8Result != E_OK)
		{
			PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP1_FC_Mismatch_Err	= ERR_PREFAILED;
		}
		else
		{
			PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP1_FC_Mismatch_Err	= ERR_PREPASSED;
		}
	}
	else
	{
		PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP1_FC_Mismatch_Err	= ERR_INHIBIT;
	}

	if(pPressM_Cfg->PressM_CIRP2_type == SENT_TYPE)
	{
		u8Result = PressM_Compare2WordBitRev(PressM_MainBus.PressM_MainHalPressureInfo.Wlp2PresData1, PressM_MainBus.PressM_MainHalPressureInfo.Wlp2PresData2);
		if(u8Result != E_OK)
		{
			PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP2_FC_Mismatch_Err	= ERR_PREFAILED;
		}
		else
		{
			PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP2_FC_Mismatch_Err	= ERR_PREPASSED;
		}
	}
	else
	{
		PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP2_FC_Mismatch_Err	= ERR_INHIBIT;
	}

	/*To do*/
	PressM_MainBus.PressM_MainPresMonFaultInfo.SIMP_FC_Mismatch_Err		= ERR_PREPASSED;
	PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP1_FC_Mismatch_Err	= ERR_PREPASSED;
	PressM_MainBus.PressM_MainPresMonFaultInfo.CIRP2_FC_Mismatch_Err	= ERR_PREPASSED;
}


static Std_ReturnType PressM_Compare2WordBitRev(uint16 origin, uint16 bitreverse)
{
    Std_ReturnType ret = E_OK;
    uint16 origin_from_bitrev = 0;

    origin_from_bitrev = bit_reverse_word(bitreverse);
    if(origin != origin_from_bitrev)
    {
        ret = E_NOT_OK;
    }

    return ret;
}

static uint16 bit_reverse_word (uint16 value)
{
  const uint16 mask0 = 0x5555;
  const uint16 mask1 = 0x3333;
  const uint16 mask2 = 0x0F0F;
  const uint16 mask3 = 0x00FF;

  value = (((~mask0) & value) >> 1) | ((mask0 & value) << 1);
  value = (((~mask1) & value) >> 2) | ((mask1 & value) << 2);
  value = (((~mask2) & value) >> 4) | ((mask2 & value) << 4);
  value = (((~mask3) & value) >> 8) | ((mask3 & value) << 8);

  return value;
}



/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
