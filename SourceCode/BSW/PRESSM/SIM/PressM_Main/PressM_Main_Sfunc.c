#define S_FUNCTION_NAME      PressM_Main_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          32
#define WidthOutputPort         21

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "PressM_Main.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    PressM_MainHalPressureInfo.McpPresData1 = input[0];
    PressM_MainHalPressureInfo.McpPresData2 = input[1];
    PressM_MainHalPressureInfo.McpSentCrc = input[2];
    PressM_MainHalPressureInfo.McpSentData = input[3];
    PressM_MainHalPressureInfo.McpSentMsgId = input[4];
    PressM_MainHalPressureInfo.McpSentSerialCrc = input[5];
    PressM_MainHalPressureInfo.McpSentConfig = input[6];
    PressM_MainHalPressureInfo.Wlp1PresData1 = input[7];
    PressM_MainHalPressureInfo.Wlp1PresData2 = input[8];
    PressM_MainHalPressureInfo.Wlp1SentCrc = input[9];
    PressM_MainHalPressureInfo.Wlp1SentData = input[10];
    PressM_MainHalPressureInfo.Wlp1SentMsgId = input[11];
    PressM_MainHalPressureInfo.Wlp1SentSerialCrc = input[12];
    PressM_MainHalPressureInfo.Wlp1SentConfig = input[13];
    PressM_MainHalPressureInfo.Wlp2PresData1 = input[14];
    PressM_MainHalPressureInfo.Wlp2PresData2 = input[15];
    PressM_MainHalPressureInfo.Wlp2SentCrc = input[16];
    PressM_MainHalPressureInfo.Wlp2SentData = input[17];
    PressM_MainHalPressureInfo.Wlp2SentMsgId = input[18];
    PressM_MainHalPressureInfo.Wlp2SentSerialCrc = input[19];
    PressM_MainHalPressureInfo.Wlp2SentConfig = input[20];
    PressM_MainSentHPressureInfo.McpSentInvalid = input[21];
    PressM_MainSentHPressureInfo.MCPSentSerialInvalid = input[22];
    PressM_MainSentHPressureInfo.Wlp1SentInvalid = input[23];
    PressM_MainSentHPressureInfo.Wlp1SentSerialInvalid = input[24];
    PressM_MainSentHPressureInfo.Wlp2SentInvalid = input[25];
    PressM_MainSentHPressureInfo.Wlp2SentSerialInvalid = input[26];
    PressM_MainEcuModeSts = input[27];
    PressM_MainIgnOnOffSts = input[28];
    PressM_MainIgnEdgeSts = input[29];
    PressM_MainVBatt1Mon = input[30];
    PressM_MainDiagClrSrs = input[31];

    PressM_Main();


    output[0] = PressM_MainPresMonFaultInfo.CIRP1_FC_Fault_Err;
    output[1] = PressM_MainPresMonFaultInfo.CIRP1_FC_Mismatch_Err;
    output[2] = PressM_MainPresMonFaultInfo.CIRP1_FC_CRC_Err;
    output[3] = PressM_MainPresMonFaultInfo.CIRP1_SC_Temp_Err;
    output[4] = PressM_MainPresMonFaultInfo.CIRP1_SC_Mismatch_Err;
    output[5] = PressM_MainPresMonFaultInfo.CIRP1_SC_CRC_Err;
    output[6] = PressM_MainPresMonFaultInfo.CIRP1_SC_Diag_Info;
    output[7] = PressM_MainPresMonFaultInfo.CIRP2_FC_Fault_Err;
    output[8] = PressM_MainPresMonFaultInfo.CIRP2_FC_Mismatch_Err;
    output[9] = PressM_MainPresMonFaultInfo.CIRP2_FC_CRC_Err;
    output[10] = PressM_MainPresMonFaultInfo.CIRP2_SC_Temp_Err;
    output[11] = PressM_MainPresMonFaultInfo.CIRP2_SC_Mismatch_Err;
    output[12] = PressM_MainPresMonFaultInfo.CIRP2_SC_CRC_Err;
    output[13] = PressM_MainPresMonFaultInfo.CIRP2_SC_Diag_Info;
    output[14] = PressM_MainPresMonFaultInfo.SIMP_FC_Fault_Err;
    output[15] = PressM_MainPresMonFaultInfo.SIMP_FC_Mismatch_Err;
    output[16] = PressM_MainPresMonFaultInfo.SIMP_FC_CRC_Err;
    output[17] = PressM_MainPresMonFaultInfo.SIMP_SC_Temp_Err;
    output[18] = PressM_MainPresMonFaultInfo.SIMP_SC_Mismatch_Err;
    output[19] = PressM_MainPresMonFaultInfo.SIMP_SC_CRC_Err;
    output[20] = PressM_MainPresMonFaultInfo.SIMP_SC_Diag_Info;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    PressM_Main_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
