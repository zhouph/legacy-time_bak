PressM_MainHalPressureInfo = Simulink.Bus;
DeList={
    'McpPresData1'
    'McpPresData2'
    'McpSentCrc'
    'McpSentData'
    'McpSentMsgId'
    'McpSentSerialCrc'
    'McpSentConfig'
    'Wlp1PresData1'
    'Wlp1PresData2'
    'Wlp1SentCrc'
    'Wlp1SentData'
    'Wlp1SentMsgId'
    'Wlp1SentSerialCrc'
    'Wlp1SentConfig'
    'Wlp2PresData1'
    'Wlp2PresData2'
    'Wlp2SentCrc'
    'Wlp2SentData'
    'Wlp2SentMsgId'
    'Wlp2SentSerialCrc'
    'Wlp2SentConfig'
    };
PressM_MainHalPressureInfo = CreateBus(PressM_MainHalPressureInfo, DeList);
clear DeList;

PressM_MainSentHPressureInfo = Simulink.Bus;
DeList={
    'McpSentInvalid'
    'MCPSentSerialInvalid'
    'Wlp1SentInvalid'
    'Wlp1SentSerialInvalid'
    'Wlp2SentInvalid'
    'Wlp2SentSerialInvalid'
    };
PressM_MainSentHPressureInfo = CreateBus(PressM_MainSentHPressureInfo, DeList);
clear DeList;

PressM_MainEcuModeSts = Simulink.Bus;
DeList={'PressM_MainEcuModeSts'};
PressM_MainEcuModeSts = CreateBus(PressM_MainEcuModeSts, DeList);
clear DeList;

PressM_MainIgnOnOffSts = Simulink.Bus;
DeList={'PressM_MainIgnOnOffSts'};
PressM_MainIgnOnOffSts = CreateBus(PressM_MainIgnOnOffSts, DeList);
clear DeList;

PressM_MainIgnEdgeSts = Simulink.Bus;
DeList={'PressM_MainIgnEdgeSts'};
PressM_MainIgnEdgeSts = CreateBus(PressM_MainIgnEdgeSts, DeList);
clear DeList;

PressM_MainVBatt1Mon = Simulink.Bus;
DeList={'PressM_MainVBatt1Mon'};
PressM_MainVBatt1Mon = CreateBus(PressM_MainVBatt1Mon, DeList);
clear DeList;

PressM_MainDiagClrSrs = Simulink.Bus;
DeList={'PressM_MainDiagClrSrs'};
PressM_MainDiagClrSrs = CreateBus(PressM_MainDiagClrSrs, DeList);
clear DeList;

PressM_MainPresMonFaultInfo = Simulink.Bus;
DeList={
    'CIRP1_FC_Fault_Err'
    'CIRP1_FC_Mismatch_Err'
    'CIRP1_FC_CRC_Err'
    'CIRP1_SC_Temp_Err'
    'CIRP1_SC_Mismatch_Err'
    'CIRP1_SC_CRC_Err'
    'CIRP1_SC_Diag_Info'
    'CIRP2_FC_Fault_Err'
    'CIRP2_FC_Mismatch_Err'
    'CIRP2_FC_CRC_Err'
    'CIRP2_SC_Temp_Err'
    'CIRP2_SC_Mismatch_Err'
    'CIRP2_SC_CRC_Err'
    'CIRP2_SC_Diag_Info'
    'SIMP_FC_Fault_Err'
    'SIMP_FC_Mismatch_Err'
    'SIMP_FC_CRC_Err'
    'SIMP_SC_Temp_Err'
    'SIMP_SC_Mismatch_Err'
    'SIMP_SC_CRC_Err'
    'SIMP_SC_Diag_Info'
    };
PressM_MainPresMonFaultInfo = CreateBus(PressM_MainPresMonFaultInfo, DeList);
clear DeList;

