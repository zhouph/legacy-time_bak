/**
 * @defgroup PressM_Main PressM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        PressM_Main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PRESSM_MAIN_H_
#define PRESSM_MAIN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "PressM_Types.h"
#include "PressM_Cfg.h"
#include "PressM_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define PRESSM_MAIN_MODULE_ID      (0)
 #define PRESSM_MAIN_MAJOR_VERSION  (2)
 #define PRESSM_MAIN_MINOR_VERSION  (0)
 #define PRESSM_MAIN_PATCH_VERSION  (0)
 #define PRESSM_MAIN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern PressM_Main_HdrBusType PressM_MainBus;

/* Version Info */
extern const SwcVersionInfo_t PressM_MainVersionInfo;

/* Input Data Element */
extern Ioc_InputSR1msHalPressureInfo_t PressM_MainHalPressureInfo;
extern SentH_MainSentHPressureInfo_t PressM_MainSentHPressureInfo;
extern Mom_HndlrEcuModeSts_t PressM_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t PressM_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t PressM_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t PressM_MainVBatt1Mon;
extern Diag_HndlrDiagClr_t PressM_MainDiagClrSrs;

/* Output Data Element */
extern PressM_MainPresMonFaultInfo_t PressM_MainPresMonFaultInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void PressM_Main_Init(void);
extern void PressM_Main(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PRESSM_MAIN_H_ */
/** @} */
