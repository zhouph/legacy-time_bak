/**
 * @defgroup PressM_Types PressM_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        PressM_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PRESSM_TYPES_H_
#define PRESSM_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Ioc_InputSR1msHalPressureInfo_t PressM_MainHalPressureInfo;
    SentH_MainSentHPressureInfo_t PressM_MainSentHPressureInfo;
    Mom_HndlrEcuModeSts_t PressM_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t PressM_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t PressM_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t PressM_MainVBatt1Mon;
    Diag_HndlrDiagClr_t PressM_MainDiagClrSrs;

/* Output Data Element */
    PressM_MainPresMonFaultInfo_t PressM_MainPresMonFaultInfo;
}PressM_Main_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PRESSM_TYPES_H_ */
/** @} */
