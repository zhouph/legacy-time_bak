/**
 * @defgroup PressM_Main_Ifa PressM_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        PressM_Main_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PRESSM_MAIN_IFA_H_
#define PRESSM_MAIN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define PressM_Main_Read_PressM_MainHalPressureInfo(data) do \
{ \
    *data = PressM_MainHalPressureInfo; \
}while(0);

#define PressM_Main_Read_PressM_MainSentHPressureInfo(data) do \
{ \
    *data = PressM_MainSentHPressureInfo; \
}while(0);

#define PressM_Main_Read_PressM_MainHalPressureInfo_McpPresData1(data) do \
{ \
    *data = PressM_MainHalPressureInfo.McpPresData1; \
}while(0);

#define PressM_Main_Read_PressM_MainHalPressureInfo_McpPresData2(data) do \
{ \
    *data = PressM_MainHalPressureInfo.McpPresData2; \
}while(0);

#define PressM_Main_Read_PressM_MainHalPressureInfo_McpSentCrc(data) do \
{ \
    *data = PressM_MainHalPressureInfo.McpSentCrc; \
}while(0);

#define PressM_Main_Read_PressM_MainHalPressureInfo_McpSentData(data) do \
{ \
    *data = PressM_MainHalPressureInfo.McpSentData; \
}while(0);

#define PressM_Main_Read_PressM_MainHalPressureInfo_McpSentMsgId(data) do \
{ \
    *data = PressM_MainHalPressureInfo.McpSentMsgId; \
}while(0);

#define PressM_Main_Read_PressM_MainHalPressureInfo_McpSentSerialCrc(data) do \
{ \
    *data = PressM_MainHalPressureInfo.McpSentSerialCrc; \
}while(0);

#define PressM_Main_Read_PressM_MainHalPressureInfo_McpSentConfig(data) do \
{ \
    *data = PressM_MainHalPressureInfo.McpSentConfig; \
}while(0);

#define PressM_Main_Read_PressM_MainHalPressureInfo_Wlp1PresData1(data) do \
{ \
    *data = PressM_MainHalPressureInfo.Wlp1PresData1; \
}while(0);

#define PressM_Main_Read_PressM_MainHalPressureInfo_Wlp1PresData2(data) do \
{ \
    *data = PressM_MainHalPressureInfo.Wlp1PresData2; \
}while(0);

#define PressM_Main_Read_PressM_MainHalPressureInfo_Wlp1SentCrc(data) do \
{ \
    *data = PressM_MainHalPressureInfo.Wlp1SentCrc; \
}while(0);

#define PressM_Main_Read_PressM_MainHalPressureInfo_Wlp1SentData(data) do \
{ \
    *data = PressM_MainHalPressureInfo.Wlp1SentData; \
}while(0);

#define PressM_Main_Read_PressM_MainHalPressureInfo_Wlp1SentMsgId(data) do \
{ \
    *data = PressM_MainHalPressureInfo.Wlp1SentMsgId; \
}while(0);

#define PressM_Main_Read_PressM_MainHalPressureInfo_Wlp1SentSerialCrc(data) do \
{ \
    *data = PressM_MainHalPressureInfo.Wlp1SentSerialCrc; \
}while(0);

#define PressM_Main_Read_PressM_MainHalPressureInfo_Wlp1SentConfig(data) do \
{ \
    *data = PressM_MainHalPressureInfo.Wlp1SentConfig; \
}while(0);

#define PressM_Main_Read_PressM_MainHalPressureInfo_Wlp2PresData1(data) do \
{ \
    *data = PressM_MainHalPressureInfo.Wlp2PresData1; \
}while(0);

#define PressM_Main_Read_PressM_MainHalPressureInfo_Wlp2PresData2(data) do \
{ \
    *data = PressM_MainHalPressureInfo.Wlp2PresData2; \
}while(0);

#define PressM_Main_Read_PressM_MainHalPressureInfo_Wlp2SentCrc(data) do \
{ \
    *data = PressM_MainHalPressureInfo.Wlp2SentCrc; \
}while(0);

#define PressM_Main_Read_PressM_MainHalPressureInfo_Wlp2SentData(data) do \
{ \
    *data = PressM_MainHalPressureInfo.Wlp2SentData; \
}while(0);

#define PressM_Main_Read_PressM_MainHalPressureInfo_Wlp2SentMsgId(data) do \
{ \
    *data = PressM_MainHalPressureInfo.Wlp2SentMsgId; \
}while(0);

#define PressM_Main_Read_PressM_MainHalPressureInfo_Wlp2SentSerialCrc(data) do \
{ \
    *data = PressM_MainHalPressureInfo.Wlp2SentSerialCrc; \
}while(0);

#define PressM_Main_Read_PressM_MainHalPressureInfo_Wlp2SentConfig(data) do \
{ \
    *data = PressM_MainHalPressureInfo.Wlp2SentConfig; \
}while(0);

#define PressM_Main_Read_PressM_MainSentHPressureInfo_McpSentInvalid(data) do \
{ \
    *data = PressM_MainSentHPressureInfo.McpSentInvalid; \
}while(0);

#define PressM_Main_Read_PressM_MainSentHPressureInfo_MCPSentSerialInvalid(data) do \
{ \
    *data = PressM_MainSentHPressureInfo.MCPSentSerialInvalid; \
}while(0);

#define PressM_Main_Read_PressM_MainSentHPressureInfo_Wlp1SentInvalid(data) do \
{ \
    *data = PressM_MainSentHPressureInfo.Wlp1SentInvalid; \
}while(0);

#define PressM_Main_Read_PressM_MainSentHPressureInfo_Wlp1SentSerialInvalid(data) do \
{ \
    *data = PressM_MainSentHPressureInfo.Wlp1SentSerialInvalid; \
}while(0);

#define PressM_Main_Read_PressM_MainSentHPressureInfo_Wlp2SentInvalid(data) do \
{ \
    *data = PressM_MainSentHPressureInfo.Wlp2SentInvalid; \
}while(0);

#define PressM_Main_Read_PressM_MainSentHPressureInfo_Wlp2SentSerialInvalid(data) do \
{ \
    *data = PressM_MainSentHPressureInfo.Wlp2SentSerialInvalid; \
}while(0);

#define PressM_Main_Read_PressM_MainEcuModeSts(data) do \
{ \
    *data = PressM_MainEcuModeSts; \
}while(0);

#define PressM_Main_Read_PressM_MainIgnOnOffSts(data) do \
{ \
    *data = PressM_MainIgnOnOffSts; \
}while(0);

#define PressM_Main_Read_PressM_MainIgnEdgeSts(data) do \
{ \
    *data = PressM_MainIgnEdgeSts; \
}while(0);

#define PressM_Main_Read_PressM_MainVBatt1Mon(data) do \
{ \
    *data = PressM_MainVBatt1Mon; \
}while(0);

#define PressM_Main_Read_PressM_MainDiagClrSrs(data) do \
{ \
    *data = PressM_MainDiagClrSrs; \
}while(0);


/* Set Output DE MAcro Function */
#define PressM_Main_Write_PressM_MainPresMonFaultInfo(data) do \
{ \
    PressM_MainPresMonFaultInfo = *data; \
}while(0);

#define PressM_Main_Write_PressM_MainPresMonFaultInfo_CIRP1_FC_Fault_Err(data) do \
{ \
    PressM_MainPresMonFaultInfo.CIRP1_FC_Fault_Err = *data; \
}while(0);

#define PressM_Main_Write_PressM_MainPresMonFaultInfo_CIRP1_FC_Mismatch_Err(data) do \
{ \
    PressM_MainPresMonFaultInfo.CIRP1_FC_Mismatch_Err = *data; \
}while(0);

#define PressM_Main_Write_PressM_MainPresMonFaultInfo_CIRP1_FC_CRC_Err(data) do \
{ \
    PressM_MainPresMonFaultInfo.CIRP1_FC_CRC_Err = *data; \
}while(0);

#define PressM_Main_Write_PressM_MainPresMonFaultInfo_CIRP1_SC_Temp_Err(data) do \
{ \
    PressM_MainPresMonFaultInfo.CIRP1_SC_Temp_Err = *data; \
}while(0);

#define PressM_Main_Write_PressM_MainPresMonFaultInfo_CIRP1_SC_Mismatch_Err(data) do \
{ \
    PressM_MainPresMonFaultInfo.CIRP1_SC_Mismatch_Err = *data; \
}while(0);

#define PressM_Main_Write_PressM_MainPresMonFaultInfo_CIRP1_SC_CRC_Err(data) do \
{ \
    PressM_MainPresMonFaultInfo.CIRP1_SC_CRC_Err = *data; \
}while(0);

#define PressM_Main_Write_PressM_MainPresMonFaultInfo_CIRP1_SC_Diag_Info(data) do \
{ \
    PressM_MainPresMonFaultInfo.CIRP1_SC_Diag_Info = *data; \
}while(0);

#define PressM_Main_Write_PressM_MainPresMonFaultInfo_CIRP2_FC_Fault_Err(data) do \
{ \
    PressM_MainPresMonFaultInfo.CIRP2_FC_Fault_Err = *data; \
}while(0);

#define PressM_Main_Write_PressM_MainPresMonFaultInfo_CIRP2_FC_Mismatch_Err(data) do \
{ \
    PressM_MainPresMonFaultInfo.CIRP2_FC_Mismatch_Err = *data; \
}while(0);

#define PressM_Main_Write_PressM_MainPresMonFaultInfo_CIRP2_FC_CRC_Err(data) do \
{ \
    PressM_MainPresMonFaultInfo.CIRP2_FC_CRC_Err = *data; \
}while(0);

#define PressM_Main_Write_PressM_MainPresMonFaultInfo_CIRP2_SC_Temp_Err(data) do \
{ \
    PressM_MainPresMonFaultInfo.CIRP2_SC_Temp_Err = *data; \
}while(0);

#define PressM_Main_Write_PressM_MainPresMonFaultInfo_CIRP2_SC_Mismatch_Err(data) do \
{ \
    PressM_MainPresMonFaultInfo.CIRP2_SC_Mismatch_Err = *data; \
}while(0);

#define PressM_Main_Write_PressM_MainPresMonFaultInfo_CIRP2_SC_CRC_Err(data) do \
{ \
    PressM_MainPresMonFaultInfo.CIRP2_SC_CRC_Err = *data; \
}while(0);

#define PressM_Main_Write_PressM_MainPresMonFaultInfo_CIRP2_SC_Diag_Info(data) do \
{ \
    PressM_MainPresMonFaultInfo.CIRP2_SC_Diag_Info = *data; \
}while(0);

#define PressM_Main_Write_PressM_MainPresMonFaultInfo_SIMP_FC_Fault_Err(data) do \
{ \
    PressM_MainPresMonFaultInfo.SIMP_FC_Fault_Err = *data; \
}while(0);

#define PressM_Main_Write_PressM_MainPresMonFaultInfo_SIMP_FC_Mismatch_Err(data) do \
{ \
    PressM_MainPresMonFaultInfo.SIMP_FC_Mismatch_Err = *data; \
}while(0);

#define PressM_Main_Write_PressM_MainPresMonFaultInfo_SIMP_FC_CRC_Err(data) do \
{ \
    PressM_MainPresMonFaultInfo.SIMP_FC_CRC_Err = *data; \
}while(0);

#define PressM_Main_Write_PressM_MainPresMonFaultInfo_SIMP_SC_Temp_Err(data) do \
{ \
    PressM_MainPresMonFaultInfo.SIMP_SC_Temp_Err = *data; \
}while(0);

#define PressM_Main_Write_PressM_MainPresMonFaultInfo_SIMP_SC_Mismatch_Err(data) do \
{ \
    PressM_MainPresMonFaultInfo.SIMP_SC_Mismatch_Err = *data; \
}while(0);

#define PressM_Main_Write_PressM_MainPresMonFaultInfo_SIMP_SC_CRC_Err(data) do \
{ \
    PressM_MainPresMonFaultInfo.SIMP_SC_CRC_Err = *data; \
}while(0);

#define PressM_Main_Write_PressM_MainPresMonFaultInfo_SIMP_SC_Diag_Info(data) do \
{ \
    PressM_MainPresMonFaultInfo.SIMP_SC_Diag_Info = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PRESSM_MAIN_IFA_H_ */
/** @} */
