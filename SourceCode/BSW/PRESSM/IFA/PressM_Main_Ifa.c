/**
 * @defgroup PressM_Main_Ifa PressM_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        PressM_Main_Ifa.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "PressM_Main_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PRESSM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "PressM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PRESSM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "PressM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PRESSM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "PressM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PRESSM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "PressM_MemMap.h"
#define PRESSM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "PressM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PRESSM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "PressM_MemMap.h"
#define PRESSM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "PressM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PRESSM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "PressM_MemMap.h"
#define PRESSM_MAIN_START_SEC_VAR_32BIT
#include "PressM_MemMap.h"
/** Variable Section (32BIT)**/


#define PRESSM_MAIN_STOP_SEC_VAR_32BIT
#include "PressM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PRESSM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "PressM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PRESSM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "PressM_MemMap.h"
#define PRESSM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "PressM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PRESSM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "PressM_MemMap.h"
#define PRESSM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "PressM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PRESSM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "PressM_MemMap.h"
#define PRESSM_MAIN_START_SEC_VAR_32BIT
#include "PressM_MemMap.h"
/** Variable Section (32BIT)**/


#define PRESSM_MAIN_STOP_SEC_VAR_32BIT
#include "PressM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PRESSM_MAIN_START_SEC_CODE
#include "PressM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define PRESSM_MAIN_STOP_SEC_CODE
#include "PressM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
