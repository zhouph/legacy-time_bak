# \file
#
# \brief PressM
#
# This file contains the implementation of the SWC
# module PressM.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

PressM_CORE_PATH     := $(MANDO_BSW_ROOT)\PressM
PressM_CAL_PATH      := $(PressM_CORE_PATH)\CAL\$(PressM_VARIANT)
PressM_SRC_PATH      := $(PressM_CORE_PATH)\SRC
PressM_CFG_PATH      := $(PressM_CORE_PATH)\CFG\$(PressM_VARIANT)
PressM_HDR_PATH      := $(PressM_CORE_PATH)\HDR
PressM_IFA_PATH      := $(PressM_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    PressM_CMN_PATH      := $(PressM_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	PressM_UT_PATH		:= $(PressM_CORE_PATH)\UT
	PressM_UNITY_PATH	:= $(PressM_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(PressM_UT_PATH)
	CC_INCLUDE_PATH		+= $(PressM_UNITY_PATH)
	PressM_Main_PATH 	:= PressM_UT_PATH\PressM_Main
endif
CC_INCLUDE_PATH    += $(PressM_CAL_PATH)
CC_INCLUDE_PATH    += $(PressM_SRC_PATH)
CC_INCLUDE_PATH    += $(PressM_CFG_PATH)
CC_INCLUDE_PATH    += $(PressM_HDR_PATH)
CC_INCLUDE_PATH    += $(PressM_IFA_PATH)
CC_INCLUDE_PATH    += $(PressM_CMN_PATH)

