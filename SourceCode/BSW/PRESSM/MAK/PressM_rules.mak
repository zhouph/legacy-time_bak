# \file
#
# \brief PressM
#
# This file contains the implementation of the SWC
# module PressM.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += PressM_src

PressM_src_FILES        += $(PressM_SRC_PATH)\PressM_Main.c
PressM_src_FILES        += $(PressM_SRC_PATH)\PressM_Process.c
PressM_src_FILES        += $(PressM_IFA_PATH)\PressM_Main_Ifa.c
PressM_src_FILES        += $(PressM_CFG_PATH)\PressM_Cfg.c
PressM_src_FILES        += $(PressM_CAL_PATH)\PressM_Cal.c

ifeq ($(ICE_COMPILE),true)
PressM_src_FILES        += $(PressM_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	PressM_src_FILES        += $(PressM_UNITY_PATH)\unity.c
	PressM_src_FILES        += $(PressM_UNITY_PATH)\unity_fixture.c	
	PressM_src_FILES        += $(PressM_UT_PATH)\main.c
	PressM_src_FILES        += $(PressM_UT_PATH)\PressM_Main\PressM_Main_UtMain.c
endif