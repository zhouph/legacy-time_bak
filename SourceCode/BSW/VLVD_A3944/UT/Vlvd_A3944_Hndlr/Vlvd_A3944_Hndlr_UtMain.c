#include "unity.h"
#include "unity_fixture.h"
#include "Vlvd_A3944_Hndlr.h"
#include "Vlvd_A3944_Hndlr_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Mom_HndlrEcuModeSts_t UtInput_Vlvd_A3944_HndlrEcuModeSts[MAX_STEP] = VLVD_A3944_HNDLRECUMODESTS;
const Eem_SuspcDetnFuncInhibitVlvdSts_t UtInput_Vlvd_A3944_HndlrFuncInhibitVlvdSts[MAX_STEP] = VLVD_A3944_HNDLRFUNCINHIBITVLVDSTS;

const Haluint8 UtExpected_Vlvd_A3944_HndlrVlvdFF0DcdInfo_C0ol[MAX_STEP] = VLVD_A3944_HNDLRVLVDFF0DCDINFO_C0OL;
const Haluint8 UtExpected_Vlvd_A3944_HndlrVlvdFF0DcdInfo_C0sb[MAX_STEP] = VLVD_A3944_HNDLRVLVDFF0DCDINFO_C0SB;
const Haluint8 UtExpected_Vlvd_A3944_HndlrVlvdFF0DcdInfo_C0sg[MAX_STEP] = VLVD_A3944_HNDLRVLVDFF0DCDINFO_C0SG;
const Haluint8 UtExpected_Vlvd_A3944_HndlrVlvdFF0DcdInfo_C1ol[MAX_STEP] = VLVD_A3944_HNDLRVLVDFF0DCDINFO_C1OL;
const Haluint8 UtExpected_Vlvd_A3944_HndlrVlvdFF0DcdInfo_C1sb[MAX_STEP] = VLVD_A3944_HNDLRVLVDFF0DCDINFO_C1SB;
const Haluint8 UtExpected_Vlvd_A3944_HndlrVlvdFF0DcdInfo_C1sg[MAX_STEP] = VLVD_A3944_HNDLRVLVDFF0DCDINFO_C1SG;
const Haluint8 UtExpected_Vlvd_A3944_HndlrVlvdFF0DcdInfo_C2ol[MAX_STEP] = VLVD_A3944_HNDLRVLVDFF0DCDINFO_C2OL;
const Haluint8 UtExpected_Vlvd_A3944_HndlrVlvdFF0DcdInfo_C2sb[MAX_STEP] = VLVD_A3944_HNDLRVLVDFF0DCDINFO_C2SB;
const Haluint8 UtExpected_Vlvd_A3944_HndlrVlvdFF0DcdInfo_C2sg[MAX_STEP] = VLVD_A3944_HNDLRVLVDFF0DCDINFO_C2SG;
const Haluint8 UtExpected_Vlvd_A3944_HndlrVlvdFF0DcdInfo_Fr[MAX_STEP] = VLVD_A3944_HNDLRVLVDFF0DCDINFO_FR;
const Haluint8 UtExpected_Vlvd_A3944_HndlrVlvdFF0DcdInfo_Ot[MAX_STEP] = VLVD_A3944_HNDLRVLVDFF0DCDINFO_OT;
const Haluint8 UtExpected_Vlvd_A3944_HndlrVlvdFF0DcdInfo_Lr[MAX_STEP] = VLVD_A3944_HNDLRVLVDFF0DCDINFO_LR;
const Haluint8 UtExpected_Vlvd_A3944_HndlrVlvdFF0DcdInfo_Uv[MAX_STEP] = VLVD_A3944_HNDLRVLVDFF0DCDINFO_UV;
const Haluint8 UtExpected_Vlvd_A3944_HndlrVlvdFF0DcdInfo_Ff[MAX_STEP] = VLVD_A3944_HNDLRVLVDFF0DCDINFO_FF;
const Haluint8 UtExpected_Vlvd_A3944_HndlrVlvdFF1DcdInfo_C3ol[MAX_STEP] = VLVD_A3944_HNDLRVLVDFF1DCDINFO_C3OL;
const Haluint8 UtExpected_Vlvd_A3944_HndlrVlvdFF1DcdInfo_C3sb[MAX_STEP] = VLVD_A3944_HNDLRVLVDFF1DCDINFO_C3SB;
const Haluint8 UtExpected_Vlvd_A3944_HndlrVlvdFF1DcdInfo_C3sg[MAX_STEP] = VLVD_A3944_HNDLRVLVDFF1DCDINFO_C3SG;
const Haluint8 UtExpected_Vlvd_A3944_HndlrVlvdFF1DcdInfo_C4ol[MAX_STEP] = VLVD_A3944_HNDLRVLVDFF1DCDINFO_C4OL;
const Haluint8 UtExpected_Vlvd_A3944_HndlrVlvdFF1DcdInfo_C4sb[MAX_STEP] = VLVD_A3944_HNDLRVLVDFF1DCDINFO_C4SB;
const Haluint8 UtExpected_Vlvd_A3944_HndlrVlvdFF1DcdInfo_C4sg[MAX_STEP] = VLVD_A3944_HNDLRVLVDFF1DCDINFO_C4SG;
const Haluint8 UtExpected_Vlvd_A3944_HndlrVlvdFF1DcdInfo_C5ol[MAX_STEP] = VLVD_A3944_HNDLRVLVDFF1DCDINFO_C5OL;
const Haluint8 UtExpected_Vlvd_A3944_HndlrVlvdFF1DcdInfo_C5sb[MAX_STEP] = VLVD_A3944_HNDLRVLVDFF1DCDINFO_C5SB;
const Haluint8 UtExpected_Vlvd_A3944_HndlrVlvdFF1DcdInfo_C5sg[MAX_STEP] = VLVD_A3944_HNDLRVLVDFF1DCDINFO_C5SG;
const Haluint8 UtExpected_Vlvd_A3944_HndlrVlvdFF1DcdInfo_Fr[MAX_STEP] = VLVD_A3944_HNDLRVLVDFF1DCDINFO_FR;
const Haluint8 UtExpected_Vlvd_A3944_HndlrVlvdFF1DcdInfo_Ot[MAX_STEP] = VLVD_A3944_HNDLRVLVDFF1DCDINFO_OT;
const Haluint8 UtExpected_Vlvd_A3944_HndlrVlvdFF1DcdInfo_Lr[MAX_STEP] = VLVD_A3944_HNDLRVLVDFF1DCDINFO_LR;
const Haluint8 UtExpected_Vlvd_A3944_HndlrVlvdFF1DcdInfo_Uv[MAX_STEP] = VLVD_A3944_HNDLRVLVDFF1DCDINFO_UV;
const Haluint8 UtExpected_Vlvd_A3944_HndlrVlvdFF1DcdInfo_Ff[MAX_STEP] = VLVD_A3944_HNDLRVLVDFF1DCDINFO_FF;
const Vlvd_A3944_HndlrVlvdInvalid_t UtExpected_Vlvd_A3944_HndlrVlvdInvalid[MAX_STEP] = VLVD_A3944_HNDLRVLVDINVALID;
const Vlvd_A3944_HndlrVlvDrvEnRst_t UtExpected_Vlvd_A3944_HndlrVlvDrvEnRst[MAX_STEP] = VLVD_A3944_HNDLRVLVDRVENRST;



TEST_GROUP(Vlvd_A3944_Hndlr);
TEST_SETUP(Vlvd_A3944_Hndlr)
{
    Vlvd_A3944_Hndlr_Init();
}

TEST_TEAR_DOWN(Vlvd_A3944_Hndlr)
{   /* Postcondition */

}

TEST(Vlvd_A3944_Hndlr, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Vlvd_A3944_HndlrEcuModeSts = UtInput_Vlvd_A3944_HndlrEcuModeSts[i];
        Vlvd_A3944_HndlrFuncInhibitVlvdSts = UtInput_Vlvd_A3944_HndlrFuncInhibitVlvdSts[i];

        Vlvd_A3944_Hndlr();

        TEST_ASSERT_EQUAL(Vlvd_A3944_HndlrVlvdFF0DcdInfo.C0ol, UtExpected_Vlvd_A3944_HndlrVlvdFF0DcdInfo_C0ol[i]);
        TEST_ASSERT_EQUAL(Vlvd_A3944_HndlrVlvdFF0DcdInfo.C0sb, UtExpected_Vlvd_A3944_HndlrVlvdFF0DcdInfo_C0sb[i]);
        TEST_ASSERT_EQUAL(Vlvd_A3944_HndlrVlvdFF0DcdInfo.C0sg, UtExpected_Vlvd_A3944_HndlrVlvdFF0DcdInfo_C0sg[i]);
        TEST_ASSERT_EQUAL(Vlvd_A3944_HndlrVlvdFF0DcdInfo.C1ol, UtExpected_Vlvd_A3944_HndlrVlvdFF0DcdInfo_C1ol[i]);
        TEST_ASSERT_EQUAL(Vlvd_A3944_HndlrVlvdFF0DcdInfo.C1sb, UtExpected_Vlvd_A3944_HndlrVlvdFF0DcdInfo_C1sb[i]);
        TEST_ASSERT_EQUAL(Vlvd_A3944_HndlrVlvdFF0DcdInfo.C1sg, UtExpected_Vlvd_A3944_HndlrVlvdFF0DcdInfo_C1sg[i]);
        TEST_ASSERT_EQUAL(Vlvd_A3944_HndlrVlvdFF0DcdInfo.C2ol, UtExpected_Vlvd_A3944_HndlrVlvdFF0DcdInfo_C2ol[i]);
        TEST_ASSERT_EQUAL(Vlvd_A3944_HndlrVlvdFF0DcdInfo.C2sb, UtExpected_Vlvd_A3944_HndlrVlvdFF0DcdInfo_C2sb[i]);
        TEST_ASSERT_EQUAL(Vlvd_A3944_HndlrVlvdFF0DcdInfo.C2sg, UtExpected_Vlvd_A3944_HndlrVlvdFF0DcdInfo_C2sg[i]);
        TEST_ASSERT_EQUAL(Vlvd_A3944_HndlrVlvdFF0DcdInfo.Fr, UtExpected_Vlvd_A3944_HndlrVlvdFF0DcdInfo_Fr[i]);
        TEST_ASSERT_EQUAL(Vlvd_A3944_HndlrVlvdFF0DcdInfo.Ot, UtExpected_Vlvd_A3944_HndlrVlvdFF0DcdInfo_Ot[i]);
        TEST_ASSERT_EQUAL(Vlvd_A3944_HndlrVlvdFF0DcdInfo.Lr, UtExpected_Vlvd_A3944_HndlrVlvdFF0DcdInfo_Lr[i]);
        TEST_ASSERT_EQUAL(Vlvd_A3944_HndlrVlvdFF0DcdInfo.Uv, UtExpected_Vlvd_A3944_HndlrVlvdFF0DcdInfo_Uv[i]);
        TEST_ASSERT_EQUAL(Vlvd_A3944_HndlrVlvdFF0DcdInfo.Ff, UtExpected_Vlvd_A3944_HndlrVlvdFF0DcdInfo_Ff[i]);
        TEST_ASSERT_EQUAL(Vlvd_A3944_HndlrVlvdFF1DcdInfo.C3ol, UtExpected_Vlvd_A3944_HndlrVlvdFF1DcdInfo_C3ol[i]);
        TEST_ASSERT_EQUAL(Vlvd_A3944_HndlrVlvdFF1DcdInfo.C3sb, UtExpected_Vlvd_A3944_HndlrVlvdFF1DcdInfo_C3sb[i]);
        TEST_ASSERT_EQUAL(Vlvd_A3944_HndlrVlvdFF1DcdInfo.C3sg, UtExpected_Vlvd_A3944_HndlrVlvdFF1DcdInfo_C3sg[i]);
        TEST_ASSERT_EQUAL(Vlvd_A3944_HndlrVlvdFF1DcdInfo.C4ol, UtExpected_Vlvd_A3944_HndlrVlvdFF1DcdInfo_C4ol[i]);
        TEST_ASSERT_EQUAL(Vlvd_A3944_HndlrVlvdFF1DcdInfo.C4sb, UtExpected_Vlvd_A3944_HndlrVlvdFF1DcdInfo_C4sb[i]);
        TEST_ASSERT_EQUAL(Vlvd_A3944_HndlrVlvdFF1DcdInfo.C4sg, UtExpected_Vlvd_A3944_HndlrVlvdFF1DcdInfo_C4sg[i]);
        TEST_ASSERT_EQUAL(Vlvd_A3944_HndlrVlvdFF1DcdInfo.C5ol, UtExpected_Vlvd_A3944_HndlrVlvdFF1DcdInfo_C5ol[i]);
        TEST_ASSERT_EQUAL(Vlvd_A3944_HndlrVlvdFF1DcdInfo.C5sb, UtExpected_Vlvd_A3944_HndlrVlvdFF1DcdInfo_C5sb[i]);
        TEST_ASSERT_EQUAL(Vlvd_A3944_HndlrVlvdFF1DcdInfo.C5sg, UtExpected_Vlvd_A3944_HndlrVlvdFF1DcdInfo_C5sg[i]);
        TEST_ASSERT_EQUAL(Vlvd_A3944_HndlrVlvdFF1DcdInfo.Fr, UtExpected_Vlvd_A3944_HndlrVlvdFF1DcdInfo_Fr[i]);
        TEST_ASSERT_EQUAL(Vlvd_A3944_HndlrVlvdFF1DcdInfo.Ot, UtExpected_Vlvd_A3944_HndlrVlvdFF1DcdInfo_Ot[i]);
        TEST_ASSERT_EQUAL(Vlvd_A3944_HndlrVlvdFF1DcdInfo.Lr, UtExpected_Vlvd_A3944_HndlrVlvdFF1DcdInfo_Lr[i]);
        TEST_ASSERT_EQUAL(Vlvd_A3944_HndlrVlvdFF1DcdInfo.Uv, UtExpected_Vlvd_A3944_HndlrVlvdFF1DcdInfo_Uv[i]);
        TEST_ASSERT_EQUAL(Vlvd_A3944_HndlrVlvdFF1DcdInfo.Ff, UtExpected_Vlvd_A3944_HndlrVlvdFF1DcdInfo_Ff[i]);
        TEST_ASSERT_EQUAL(Vlvd_A3944_HndlrVlvdInvalid, UtExpected_Vlvd_A3944_HndlrVlvdInvalid[i]);
        TEST_ASSERT_EQUAL(Vlvd_A3944_HndlrVlvDrvEnRst, UtExpected_Vlvd_A3944_HndlrVlvDrvEnRst[i]);
    }
}

TEST_GROUP_RUNNER(Vlvd_A3944_Hndlr)
{
    RUN_TEST_CASE(Vlvd_A3944_Hndlr, All);
}
