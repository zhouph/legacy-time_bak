# \file
#
# \brief Vlvd_A3944
#
# This file contains the implementation of the SWC
# module Vlvd_A3944.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Vlvd_A3944_src

Vlvd_A3944_src_FILES        += $(Vlvd_A3944_SRC_PATH)\Vlvd_A3944_Hndlr.c
Vlvd_A3944_src_FILES        += $(Vlvd_A3944_SRC_PATH)\Vlvd_A3944_Process.c
Vlvd_A3944_src_FILES        += $(Vlvd_A3944_IFA_PATH)\Vlvd_A3944_Hndlr_Ifa.c
Vlvd_A3944_src_FILES        += $(Vlvd_A3944_CFG_PATH)\Vlvd_A3944_Cfg.c
Vlvd_A3944_src_FILES        += $(Vlvd_A3944_CAL_PATH)\Vlvd_A3944_Cal.c

ifeq ($(ICE_COMPILE),true)
Vlvd_A3944_src_FILES        += $(Vlvd_A3944_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	Vlvd_A3944_src_FILES        += $(Vlvd_A3944_UNITY_PATH)\unity.c
	Vlvd_A3944_src_FILES        += $(Vlvd_A3944_UNITY_PATH)\unity_fixture.c	
	Vlvd_A3944_src_FILES        += $(Vlvd_A3944_UT_PATH)\main.c
	Vlvd_A3944_src_FILES        += $(Vlvd_A3944_UT_PATH)\Vlvd_A3944_Hndlr\Vlvd_A3944_Hndlr_UtMain.c
endif