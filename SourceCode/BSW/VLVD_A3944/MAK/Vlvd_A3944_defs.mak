# \file
#
# \brief Vlvd_A3944
#
# This file contains the implementation of the SWC
# module Vlvd_A3944.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Vlvd_A3944_CORE_PATH     := $(MANDO_BSW_ROOT)\Vlvd_A3944
Vlvd_A3944_CAL_PATH      := $(Vlvd_A3944_CORE_PATH)\CAL\$(Vlvd_A3944_VARIANT)
Vlvd_A3944_SRC_PATH      := $(Vlvd_A3944_CORE_PATH)\SRC
Vlvd_A3944_CFG_PATH      := $(Vlvd_A3944_CORE_PATH)\CFG\$(Vlvd_A3944_VARIANT)
Vlvd_A3944_HDR_PATH      := $(Vlvd_A3944_CORE_PATH)\HDR
Vlvd_A3944_IFA_PATH      := $(Vlvd_A3944_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Vlvd_A3944_CMN_PATH      := $(Vlvd_A3944_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	Vlvd_A3944_UT_PATH		:= $(Vlvd_A3944_CORE_PATH)\UT
	Vlvd_A3944_UNITY_PATH	:= $(Vlvd_A3944_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(Vlvd_A3944_UT_PATH)
	CC_INCLUDE_PATH		+= $(Vlvd_A3944_UNITY_PATH)
	Vlvd_A3944_Hndlr_PATH 	:= Vlvd_A3944_UT_PATH\Vlvd_A3944_Hndlr
endif
CC_INCLUDE_PATH    += $(Vlvd_A3944_CAL_PATH)
CC_INCLUDE_PATH    += $(Vlvd_A3944_SRC_PATH)
CC_INCLUDE_PATH    += $(Vlvd_A3944_CFG_PATH)
CC_INCLUDE_PATH    += $(Vlvd_A3944_HDR_PATH)
CC_INCLUDE_PATH    += $(Vlvd_A3944_IFA_PATH)
CC_INCLUDE_PATH    += $(Vlvd_A3944_CMN_PATH)

