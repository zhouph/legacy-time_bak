/**
 * @defgroup Vlvd_A3944_Hndlr_Ifa Vlvd_A3944_Hndlr_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Vlvd_A3944_Hndlr_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef VLVD_A3944_HNDLR_IFA_H_
#define VLVD_A3944_HNDLR_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Vlvd_A3944_Hndlr_Read_Vlvd_A3944_HndlrEcuModeSts(data) do \
{ \
    *data = Vlvd_A3944_HndlrEcuModeSts; \
}while(0);

#define Vlvd_A3944_Hndlr_Read_Vlvd_A3944_HndlrFuncInhibitVlvdSts(data) do \
{ \
    *data = Vlvd_A3944_HndlrFuncInhibitVlvdSts; \
}while(0);


/* Set Output DE MAcro Function */
#define Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvdFF0DcdInfo(data) do \
{ \
    Vlvd_A3944_HndlrVlvdFF0DcdInfo = *data; \
}while(0);

#define Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvdFF1DcdInfo(data) do \
{ \
    Vlvd_A3944_HndlrVlvdFF1DcdInfo = *data; \
}while(0);

#define Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvdFF0DcdInfo_C0ol(data) do \
{ \
    Vlvd_A3944_HndlrVlvdFF0DcdInfo.C0ol = *data; \
}while(0);

#define Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvdFF0DcdInfo_C0sb(data) do \
{ \
    Vlvd_A3944_HndlrVlvdFF0DcdInfo.C0sb = *data; \
}while(0);

#define Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvdFF0DcdInfo_C0sg(data) do \
{ \
    Vlvd_A3944_HndlrVlvdFF0DcdInfo.C0sg = *data; \
}while(0);

#define Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvdFF0DcdInfo_C1ol(data) do \
{ \
    Vlvd_A3944_HndlrVlvdFF0DcdInfo.C1ol = *data; \
}while(0);

#define Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvdFF0DcdInfo_C1sb(data) do \
{ \
    Vlvd_A3944_HndlrVlvdFF0DcdInfo.C1sb = *data; \
}while(0);

#define Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvdFF0DcdInfo_C1sg(data) do \
{ \
    Vlvd_A3944_HndlrVlvdFF0DcdInfo.C1sg = *data; \
}while(0);

#define Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvdFF0DcdInfo_C2ol(data) do \
{ \
    Vlvd_A3944_HndlrVlvdFF0DcdInfo.C2ol = *data; \
}while(0);

#define Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvdFF0DcdInfo_C2sb(data) do \
{ \
    Vlvd_A3944_HndlrVlvdFF0DcdInfo.C2sb = *data; \
}while(0);

#define Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvdFF0DcdInfo_C2sg(data) do \
{ \
    Vlvd_A3944_HndlrVlvdFF0DcdInfo.C2sg = *data; \
}while(0);

#define Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvdFF0DcdInfo_Fr(data) do \
{ \
    Vlvd_A3944_HndlrVlvdFF0DcdInfo.Fr = *data; \
}while(0);

#define Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvdFF0DcdInfo_Ot(data) do \
{ \
    Vlvd_A3944_HndlrVlvdFF0DcdInfo.Ot = *data; \
}while(0);

#define Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvdFF0DcdInfo_Lr(data) do \
{ \
    Vlvd_A3944_HndlrVlvdFF0DcdInfo.Lr = *data; \
}while(0);

#define Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvdFF0DcdInfo_Uv(data) do \
{ \
    Vlvd_A3944_HndlrVlvdFF0DcdInfo.Uv = *data; \
}while(0);

#define Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvdFF0DcdInfo_Ff(data) do \
{ \
    Vlvd_A3944_HndlrVlvdFF0DcdInfo.Ff = *data; \
}while(0);

#define Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvdFF1DcdInfo_C3ol(data) do \
{ \
    Vlvd_A3944_HndlrVlvdFF1DcdInfo.C3ol = *data; \
}while(0);

#define Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvdFF1DcdInfo_C3sb(data) do \
{ \
    Vlvd_A3944_HndlrVlvdFF1DcdInfo.C3sb = *data; \
}while(0);

#define Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvdFF1DcdInfo_C3sg(data) do \
{ \
    Vlvd_A3944_HndlrVlvdFF1DcdInfo.C3sg = *data; \
}while(0);

#define Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvdFF1DcdInfo_C4ol(data) do \
{ \
    Vlvd_A3944_HndlrVlvdFF1DcdInfo.C4ol = *data; \
}while(0);

#define Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvdFF1DcdInfo_C4sb(data) do \
{ \
    Vlvd_A3944_HndlrVlvdFF1DcdInfo.C4sb = *data; \
}while(0);

#define Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvdFF1DcdInfo_C4sg(data) do \
{ \
    Vlvd_A3944_HndlrVlvdFF1DcdInfo.C4sg = *data; \
}while(0);

#define Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvdFF1DcdInfo_C5ol(data) do \
{ \
    Vlvd_A3944_HndlrVlvdFF1DcdInfo.C5ol = *data; \
}while(0);

#define Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvdFF1DcdInfo_C5sb(data) do \
{ \
    Vlvd_A3944_HndlrVlvdFF1DcdInfo.C5sb = *data; \
}while(0);

#define Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvdFF1DcdInfo_C5sg(data) do \
{ \
    Vlvd_A3944_HndlrVlvdFF1DcdInfo.C5sg = *data; \
}while(0);

#define Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvdFF1DcdInfo_Fr(data) do \
{ \
    Vlvd_A3944_HndlrVlvdFF1DcdInfo.Fr = *data; \
}while(0);

#define Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvdFF1DcdInfo_Ot(data) do \
{ \
    Vlvd_A3944_HndlrVlvdFF1DcdInfo.Ot = *data; \
}while(0);

#define Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvdFF1DcdInfo_Lr(data) do \
{ \
    Vlvd_A3944_HndlrVlvdFF1DcdInfo.Lr = *data; \
}while(0);

#define Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvdFF1DcdInfo_Uv(data) do \
{ \
    Vlvd_A3944_HndlrVlvdFF1DcdInfo.Uv = *data; \
}while(0);

#define Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvdFF1DcdInfo_Ff(data) do \
{ \
    Vlvd_A3944_HndlrVlvdFF1DcdInfo.Ff = *data; \
}while(0);

#define Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvdInvalid(data) do \
{ \
    Vlvd_A3944_HndlrVlvdInvalid = *data; \
}while(0);

#define Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvDrvEnRst(data) do \
{ \
    Vlvd_A3944_HndlrVlvDrvEnRst = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* VLVD_A3944_HNDLR_IFA_H_ */
/** @} */
