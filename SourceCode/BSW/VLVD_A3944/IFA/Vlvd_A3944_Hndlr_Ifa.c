/**
 * @defgroup Vlvd_A3944_Hndlr_Ifa Vlvd_A3944_Hndlr_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Vlvd_A3944_Hndlr_Ifa.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Vlvd_A3944_Hndlr_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define VLVD_A3944_HNDLR_START_SEC_CONST_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define VLVD_A3944_HNDLR_STOP_SEC_CONST_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define VLVD_A3944_HNDLR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define VLVD_A3944_HNDLR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
#define VLVD_A3944_HNDLR_START_SEC_VAR_NOINIT_32BIT
#include "Vlvd_A3944_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define VLVD_A3944_HNDLR_STOP_SEC_VAR_NOINIT_32BIT
#include "Vlvd_A3944_MemMap.h"
#define VLVD_A3944_HNDLR_START_SEC_VAR_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define VLVD_A3944_HNDLR_STOP_SEC_VAR_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
#define VLVD_A3944_HNDLR_START_SEC_VAR_32BIT
#include "Vlvd_A3944_MemMap.h"
/** Variable Section (32BIT)**/


#define VLVD_A3944_HNDLR_STOP_SEC_VAR_32BIT
#include "Vlvd_A3944_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define VLVD_A3944_HNDLR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define VLVD_A3944_HNDLR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
#define VLVD_A3944_HNDLR_START_SEC_VAR_NOINIT_32BIT
#include "Vlvd_A3944_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define VLVD_A3944_HNDLR_STOP_SEC_VAR_NOINIT_32BIT
#include "Vlvd_A3944_MemMap.h"
#define VLVD_A3944_HNDLR_START_SEC_VAR_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define VLVD_A3944_HNDLR_STOP_SEC_VAR_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
#define VLVD_A3944_HNDLR_START_SEC_VAR_32BIT
#include "Vlvd_A3944_MemMap.h"
/** Variable Section (32BIT)**/


#define VLVD_A3944_HNDLR_STOP_SEC_VAR_32BIT
#include "Vlvd_A3944_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define VLVD_A3944_HNDLR_START_SEC_CODE
#include "Vlvd_A3944_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define VLVD_A3944_HNDLR_STOP_SEC_CODE
#include "Vlvd_A3944_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
