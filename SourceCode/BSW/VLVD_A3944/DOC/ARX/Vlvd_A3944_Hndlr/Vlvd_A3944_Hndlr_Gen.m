Vlvd_A3944_HndlrEcuModeSts = Simulink.Bus;
DeList={'Vlvd_A3944_HndlrEcuModeSts'};
Vlvd_A3944_HndlrEcuModeSts = CreateBus(Vlvd_A3944_HndlrEcuModeSts, DeList);
clear DeList;

Vlvd_A3944_HndlrFuncInhibitVlvdSts = Simulink.Bus;
DeList={'Vlvd_A3944_HndlrFuncInhibitVlvdSts'};
Vlvd_A3944_HndlrFuncInhibitVlvdSts = CreateBus(Vlvd_A3944_HndlrFuncInhibitVlvdSts, DeList);
clear DeList;

Vlvd_A3944_HndlrVlvdFF0DcdInfo = Simulink.Bus;
DeList={
    'C0ol'
    'C0sb'
    'C0sg'
    'C1ol'
    'C1sb'
    'C1sg'
    'C2ol'
    'C2sb'
    'C2sg'
    'Fr'
    'Ot'
    'Lr'
    'Uv'
    'Ff'
    };
Vlvd_A3944_HndlrVlvdFF0DcdInfo = CreateBus(Vlvd_A3944_HndlrVlvdFF0DcdInfo, DeList);
clear DeList;

Vlvd_A3944_HndlrVlvdFF1DcdInfo = Simulink.Bus;
DeList={
    'C3ol'
    'C3sb'
    'C3sg'
    'C4ol'
    'C4sb'
    'C4sg'
    'C5ol'
    'C5sb'
    'C5sg'
    'Fr'
    'Ot'
    'Lr'
    'Uv'
    'Ff'
    };
Vlvd_A3944_HndlrVlvdFF1DcdInfo = CreateBus(Vlvd_A3944_HndlrVlvdFF1DcdInfo, DeList);
clear DeList;

Vlvd_A3944_HndlrVlvdInvalid = Simulink.Bus;
DeList={'Vlvd_A3944_HndlrVlvdInvalid'};
Vlvd_A3944_HndlrVlvdInvalid = CreateBus(Vlvd_A3944_HndlrVlvdInvalid, DeList);
clear DeList;

Vlvd_A3944_HndlrVlvDrvEnRst = Simulink.Bus;
DeList={'Vlvd_A3944_HndlrVlvDrvEnRst'};
Vlvd_A3944_HndlrVlvDrvEnRst = CreateBus(Vlvd_A3944_HndlrVlvDrvEnRst, DeList);
clear DeList;

