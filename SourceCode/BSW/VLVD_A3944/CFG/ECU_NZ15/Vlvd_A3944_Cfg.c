/**
 * @defgroup Vlvd_A3944_Cfg Vlvd_A3944_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Vlvd_A3944_Cfg.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Vlvd_A3944_Cfg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define VLVD_A3944_START_SEC_CONST_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/
 /* Valve driver IC manager module configuration table */

 const VdrM_ChannelConfigType VdrM_ChannelConfig[VdrM_VdrChannel_MaxNum] = 
 {
    /* VdrM_VdrChannel_CUTV_P */
    {
      ShortRetryTimer,       /* Retry select */
      EnableDiagPulldown,    /* diagnostic pull-down */
      TurnOffBlankTime280us, /* Turn-off blank time (nominal) 280us */
      TurnOnBlankTime28us,   /* Turn-on blank time select */
      ShortBattThreshold14p, /* Short to battery threshold select */
      ShortGndThreshold45p   /* Short to ground threshold select  */
    },
    /* VdrM_VdrChannel_CUTV_S */
    {
      ShortRetryTimer,       /* Retry select */
      EnableDiagPulldown,    /* diagnostic pull-down */
      TurnOffBlankTime280us, /* Turn-off blank time (nominal) 280us */
      TurnOnBlankTime28us,   /* Turn-on blank time select */
      ShortBattThreshold14p, /* Short to battery threshold select */
      ShortGndThreshold45p   /* Short to ground threshold select  */
    },
    /* VdrM_VdrChannel_CIRCUITV_P */
    {
      ShortRetryTimer,       /* Retry select */
      EnableDiagPulldown,    /* diagnostic pull-down */
      TurnOffBlankTime280us, /* Turn-off blank time (nominal) 280us */
      TurnOnBlankTime28us,   /* Turn-on blank time select */
      ShortBattThreshold14p, /* Short to battery threshold select */
      ShortGndThreshold45p   /* Short to ground threshold select  */
    },
    /* VdrM_VdrChannel_CIRCUITV_S */
    {
      ShortRetryTimer,       /* Retry select */
      EnableDiagPulldown,    /* diagnostic pull-down */
      TurnOffBlankTime280us, /* Turn-off blank time (nominal) 280us */
      TurnOnBlankTime28us,   /* Turn-on blank time select */
      ShortBattThreshold14p, /* Short to battery threshold select */
      ShortGndThreshold45p   /* Short to ground threshold select  */
    },
    /* VdrM_VdrChannel_SIMV */
    {
      ShortRetryTimer,       /* Retry select */
      EnableDiagPulldown,    /* diagnostic pull-down */
      TurnOffBlankTime280us, /* Turn-off blank time (nominal) 280us */
      TurnOnBlankTime28us,   /* Turn-on blank time select */
      ShortBattThreshold14p, /* Short to battery threshold select */
      ShortGndThreshold45p   /* Short to ground threshold select  */
    },
    /* VdrM_VdrChannel_Reserved */
    {
      ShortRetryTimer,       /* Retry select */
      EnableDiagPulldown,    /* diagnostic pull-down */
      TurnOffBlankTime280us, /* Turn-off blank time (nominal) 280us */
      TurnOnBlankTime28us,   /* Turn-on blank time select */
      ShortBattThreshold14p, /* Short to battery threshold select */
      ShortGndThreshold45p   /* Short to ground threshold select  */
    }
 };

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define VLVD_A3944_STOP_SEC_CONST_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define VLVD_A3944_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define VLVD_A3944_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
#define VLVD_A3944_START_SEC_VAR_NOINIT_32BIT
#include "Vlvd_A3944_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define VLVD_A3944_STOP_SEC_VAR_NOINIT_32BIT
#include "Vlvd_A3944_MemMap.h"
#define VLVD_A3944_START_SEC_VAR_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define VLVD_A3944_STOP_SEC_VAR_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
#define VLVD_A3944_START_SEC_VAR_32BIT
#include "Vlvd_A3944_MemMap.h"
/** Variable Section (32BIT)**/


#define VLVD_A3944_STOP_SEC_VAR_32BIT
#include "Vlvd_A3944_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define VLVD_A3944_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define VLVD_A3944_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
#define VLVD_A3944_START_SEC_VAR_NOINIT_32BIT
#include "Vlvd_A3944_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define VLVD_A3944_STOP_SEC_VAR_NOINIT_32BIT
#include "Vlvd_A3944_MemMap.h"
#define VLVD_A3944_START_SEC_VAR_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define VLVD_A3944_STOP_SEC_VAR_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
#define VLVD_A3944_START_SEC_VAR_32BIT
#include "Vlvd_A3944_MemMap.h"
/** Variable Section (32BIT)**/


#define VLVD_A3944_STOP_SEC_VAR_32BIT
#include "Vlvd_A3944_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define VLVD_A3944_START_SEC_CODE
#include "Vlvd_A3944_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define VLVD_A3944_STOP_SEC_CODE
#include "Vlvd_A3944_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
