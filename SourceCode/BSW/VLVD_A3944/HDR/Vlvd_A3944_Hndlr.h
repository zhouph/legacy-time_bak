/**
 * @defgroup Vlvd_A3944_Hndlr Vlvd_A3944_Hndlr
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Vlvd_A3944_Hndlr.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef VLVD_A3944_HNDLR_H_
#define VLVD_A3944_HNDLR_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Vlvd_A3944_Types.h"
#include "Vlvd_A3944_Cfg.h"
#include "Vlvd_A3944_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define VLVD_A3944_HNDLR_MODULE_ID      (0)
 #define VLVD_A3944_HNDLR_MAJOR_VERSION  (2)
 #define VLVD_A3944_HNDLR_MINOR_VERSION  (0)
 #define VLVD_A3944_HNDLR_PATCH_VERSION  (0)
 #define VLVD_A3944_HNDLR_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Vlvd_A3944_Hndlr_HdrBusType Vlvd_A3944_HndlrBus;

/* Version Info */
extern const SwcVersionInfo_t Vlvd_A3944_HndlrVersionInfo;

/* Input Data Element */
extern Mom_HndlrEcuModeSts_t Vlvd_A3944_HndlrEcuModeSts;
extern Eem_SuspcDetnFuncInhibitVlvdSts_t Vlvd_A3944_HndlrFuncInhibitVlvdSts;

/* Output Data Element */
extern Vlvd_A3944_HndlrVlvdFF0DcdInfo_t Vlvd_A3944_HndlrVlvdFF0DcdInfo;
extern Vlvd_A3944_HndlrVlvdFF1DcdInfo_t Vlvd_A3944_HndlrVlvdFF1DcdInfo;
extern Vlvd_A3944_HndlrVlvdInvalid_t Vlvd_A3944_HndlrVlvdInvalid;
extern Vlvd_A3944_HndlrVlvDrvEnRst_t Vlvd_A3944_HndlrVlvDrvEnRst;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Vlvd_A3944_Hndlr_Init(void);
extern void Vlvd_A3944_Hndlr(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* VLVD_A3944_HNDLR_H_ */
/** @} */
