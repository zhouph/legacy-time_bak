/**
 * @defgroup Vlvd_A3944_Types Vlvd_A3944_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Vlvd_A3944_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef VLVD_A3944_TYPES_H_
#define VLVD_A3944_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
#define GateSelectAdd               0x1
#define FaultMaskAdd                0x5
#define ChannelFaultConfigFixedAdd  0x1

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Mom_HndlrEcuModeSts_t Vlvd_A3944_HndlrEcuModeSts;
    Eem_SuspcDetnFuncInhibitVlvdSts_t Vlvd_A3944_HndlrFuncInhibitVlvdSts;

/* Output Data Element */
    Vlvd_A3944_HndlrVlvdFF0DcdInfo_t Vlvd_A3944_HndlrVlvdFF0DcdInfo;
    Vlvd_A3944_HndlrVlvdFF1DcdInfo_t Vlvd_A3944_HndlrVlvdFF1DcdInfo;
    Vlvd_A3944_HndlrVlvdInvalid_t Vlvd_A3944_HndlrVlvdInvalid;
    Vlvd_A3944_HndlrVlvDrvEnRst_t Vlvd_A3944_HndlrVlvDrvEnRst;
}Vlvd_A3944_Hndlr_HdrBusType;


/* VdrM Channel Type */
 typedef enum
 {
    VdrCh0   = 0,
    VdrCh1   = 1,
    VdrCh2   = 2,
    VdrCh3   = 3,
    VdrCh4   = 4,
    VdrCh5   = 5,
    VdrChmax = 6
 }VdrM_ChType;

 /* VdrM Data Type */
 typedef uint8 VdrM_DataType;

 /* Fault Register select Type */
 typedef enum
 {
   FaultReg0 = 0,
   FaultReg1
 }VdrM_FaultRegSelectType;

 /* Retry select */
 typedef enum
 {
    NotUsed = 0,      /* Lockout until reset but Not used */
    LockOut,          /* Lockout until reset */
    ShortRetryTimer,  /* Short retry timer. Nominally 10ms */
    LongRetryTimer    /* Long retry timer. Nominally 55ms */
 }VdrM_RetryType;

 /* diagnostic pull-down */
 typedef enum
 {
    EnableDiagPulldown = 0, /* Enable diagnostic pull-down */
    DisableDiagPulldown     /* Disable diagnostic pull-down */
 }VdrM_DiagPulldownType;

 /* Turn-off blank time select */
 typedef enum
 {
    TurnOffBlankTime80us  = 0,  /* Turn-off blank time (nominal) 80us */
    TurnOffBlankTime140us,      /* Turn-off blank time (nominal) 140us */
    TurnOffBlankTime280us,      /* Turn-off blank time (nominal) 280us */
    TurnOffBlankTime4ms         /* Turn-off blank time (nominal) 4ms */
 }VdrM_TurnOffBlankTimeType;

 /* Turn-on blank time select */
 typedef enum
 {
    TurnOnBlankTime5us = 0, /* Turn-on blank time (nominal) 5us */
    TurnOnBlankTime14us,     /* Turn-on blank time (nominal) 14us */
    TurnOnBlankTime28us,     /* Turn-on blank time (nominal) 28 us */
    TurnOnBlankTime56us     /* Turn-on blank time (nominal) 56 us */
 }VdrM_TurnOnBlankTimeType;

 /* Short to battery threshold select */
 typedef enum
 {
    ShortBattThreshold6p,   /* Short to battery threshold select 6% of VREG */
    ShortBattThreshold8p,   /* Short to battery threshold select 8% of VREG */
    ShortBattThreshold10p,   /* Short to battery threshold select 10% of VREG */
    ShortBattThreshold12p,   /* Short to battery threshold select 12% of VREG */
    ShortBattThreshold14p,   /* Short to battery threshold select 14% of VREG */
    ShortBattThreshold16p,   /* Short to battery threshold select 16% of VREG */
    ShortBattThreshold18p,   /* Short to battery threshold select 18% of VREG */
    ShortBattThreshold31p   /* Short to battery threshold select 31% of VREG */
 }VdrM_ShortToBattThresholdType;

 /* Short to ground threshold select  */
 typedef enum
 {
    ShortGndThreshold45p,  /* Short to ground threshold select 45% of VREG */
    ShortGndThreshold66p  /* Short to ground threshold select 66% of VREG */   
 }VdrM_ShortToGndThresholdType;
 
 /* Valve Driver Manager Channel Configuration Type */
 typedef struct
 {
    VdrM_RetryType  RT;               
    VdrM_DiagPulldownType NPD;        
    VdrM_TurnOffBlankTimeType TOF;    
    VdrM_TurnOnBlankTimeType  TON;   
    VdrM_ShortToBattThresholdType SB;
    VdrM_ShortToGndThresholdType  SG;
 }VdrM_ChannelConfigType;

 /* Fault Registers Type */
 typedef struct
 {
    VdrM_DataType FF;     /* Fault detected */
    VdrM_DataType UV;     /* Under-voltage */
    VdrM_DataType LR;     /* Logical Reset */
    VdrM_DataType OT;     /* Over-temperature */
    VdrM_DataType FR;     /* Fault Register */
    VdrM_DataType C2SG;   /* Channel2 Short to Ground */
    VdrM_DataType C2SB;   /* Channel2 Short to Battery */
    VdrM_DataType C2OL;   /* Channel2 Open load */
    VdrM_DataType C1SG;   /* Channel1 Short to Ground */
    VdrM_DataType C1SB;   /* Channel1 Short to Battery */
    VdrM_DataType C1OL;   /* Channel1 Open Load */
    VdrM_DataType C0SG;   /* Channel0 Short to Ground */
    VdrM_DataType C0SB;   /* Channel0 Short to Battery */
    VdrM_DataType C0OL;   /* Channel0 Open Load */        
 }VdrM_FaultRegType;

 /* Channel Fault Status Type*/
 typedef struct
 {
    VdrM_DataType UnderVoltage;   /* Under voltage fault */
    VdrM_DataType LogicalReset;   /* Logical Reset info*/
    VdrM_DataType OverTemp;       /* Over temperature fault */
    VdrM_DataType ShortToGnd;     /* Short to ground fault */
    VdrM_DataType ShortToBatt;    /* Short to battery fault */
    VdrM_DataType OpenLoad;       /* Open load fault */
 }VdrM_FaultStatusType;

typedef union
{
    uint16    R;
    struct
    {
        uint32    mu1_G0        :1;
        uint32    mu1_G1        :1;
        uint32    mu1_G2        :1;
        uint32    mu1_G3        :1;
        uint32    mu1_G4        :1;
        uint32    mu1_G5        :1;
        uint32    Reserved0     :6;
        uint32    Address       :4;
        uint32    Reserved1     :16;
    }B;
}Vlvd_A3944_GateSelType;

typedef union
{
    uint16    R;
    struct
    {
        uint32    mu1_K0        :1;
        uint32    mu1_K1        :1;
        uint32    mu1_K2        :1;
        uint32    mu1_K3        :1;
        uint32    mu1_K4        :1;
        uint32    mu1_K5        :1;
        uint32    mu1_OLM       :1;
        uint32    Reserved0     :5;
        uint32    Address       :4;
        uint32    Reserved1     :16;
    }B;
}Vlvd_A3944_FaultMaskType;

typedef union
{
    uint16    R;
    struct
    {
        uint32    mu1_SG        :1;
        uint32    mu3_SB        :3;
        uint32    mu2_TON       :2;
        uint32    mu2_TOF       :2;
        uint32    mu1_NPD       :1;
        uint32    Reserved0     :1;
        uint32    mu2_RT        :2;
        uint32    Address       :3;
        uint32    FixedAdd      :1;
        uint32    Reserved1     :16;
    }B;
}Vlvd_A3944_ChannelFaultCfgType;

typedef union
{
    uint16    R;
    struct
    {
        uint32    mu1_C0OL      :1;
        uint32    mu1_C0SB      :1;
        uint32    mu1_C0SG      :1;
        uint32    mu1_C1OL      :1;
        uint32    mu1_C1SB      :1;
        uint32    mu1_C1SG      :1;
        uint32    mu1_C2OL      :1;
        uint32    mu1_C2SB      :1;
        uint32    mu1_C2SG      :1;
        uint32    Reserved0     :2;
        uint32    mu1_FR        :1;
        uint32    mu1_OT        :1;
        uint32    mu1_LR        :1;
        uint32    mu1_UV        :1;
        uint32    mu1_FF        :1;
        uint32    Reserved1     :16;
    }B;
    struct
    {
        uint32    mu1_C3OL      :1;
        uint32    mu1_C3SB      :1;
        uint32    mu1_C3SG      :1;
        uint32    mu1_C4OL      :1;
        uint32    mu1_C4SB      :1;
        uint32    mu1_C4SG      :1;
        uint32    mu1_C5OL      :1;
        uint32    mu1_C5SB      :1;
        uint32    mu1_C5SG      :1;
        uint32    Reserved0     :2;
        uint32    mu1_FR        :1;
        uint32    mu1_OT        :1;
        uint32    mu1_LR        :1;
        uint32    mu1_UV        :1;
        uint32    mu1_FF        :1;
        uint32    Reserved1     :16;
    }B1;
}Vlvd_A3944_FaultType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* VLVD_A3944_TYPES_H_ */
/** @} */
