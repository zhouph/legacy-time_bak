/**
 * @defgroup Vlvd_A3944_Hndlr Vlvd_A3944_Hndlr
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Vlvd_A3944_Hndlr.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Vlvd_A3944_Hndlr.h"
#include "Vlvd_A3944_Hndlr_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define VLVD_A3944_HNDLR_START_SEC_CONST_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
#include "Spim_Cdd.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define VLVD_A3944_HNDLR_STOP_SEC_CONST_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define VLVD_A3944_HNDLR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

uint32 Vlvd_A3944_TxFrame[10];
uint32 Vlvd_A3944_RxFrame[10];
uint8 Vlvd_MsgNum;


#define VLVD_A3944_HNDLR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
#define VLVD_A3944_HNDLR_START_SEC_VAR_NOINIT_32BIT
#include "Vlvd_A3944_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define VLVD_A3944_HNDLR_STOP_SEC_VAR_NOINIT_32BIT
#include "Vlvd_A3944_MemMap.h"
#define VLVD_A3944_HNDLR_START_SEC_VAR_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define VLVD_A3944_HNDLR_STOP_SEC_VAR_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
#define VLVD_A3944_HNDLR_START_SEC_VAR_32BIT
#include "Vlvd_A3944_MemMap.h"
/** Variable Section (32BIT)**/


#define VLVD_A3944_HNDLR_STOP_SEC_VAR_32BIT
#include "Vlvd_A3944_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define VLVD_A3944_HNDLR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

Vlvd_A3944_GateSelType Vlvd_A3944_GateSel;
Vlvd_A3944_FaultMaskType Vlvd_A3944_FaultMask;
Vlvd_A3944_ChannelFaultCfgType Vlvd_A3944_ChannelFaultCfgCh0;
Vlvd_A3944_ChannelFaultCfgType Vlvd_A3944_ChannelFaultCfgCh1;
Vlvd_A3944_ChannelFaultCfgType Vlvd_A3944_ChannelFaultCfgCh2;
Vlvd_A3944_ChannelFaultCfgType Vlvd_A3944_ChannelFaultCfgCh3;
Vlvd_A3944_ChannelFaultCfgType Vlvd_A3944_ChannelFaultCfgCh4;
Vlvd_A3944_ChannelFaultCfgType Vlvd_A3944_ChannelFaultCfgCh5;
Vlvd_A3944_FaultType Vlvd_A3944_Fault;

#define VLVD_A3944_HNDLR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
#define VLVD_A3944_HNDLR_START_SEC_VAR_NOINIT_32BIT
#include "Vlvd_A3944_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define VLVD_A3944_HNDLR_STOP_SEC_VAR_NOINIT_32BIT
#include "Vlvd_A3944_MemMap.h"
#define VLVD_A3944_HNDLR_START_SEC_VAR_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define VLVD_A3944_HNDLR_STOP_SEC_VAR_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
#define VLVD_A3944_HNDLR_START_SEC_VAR_32BIT
#include "Vlvd_A3944_MemMap.h"
/** Variable Section (32BIT)**/


#define VLVD_A3944_HNDLR_STOP_SEC_VAR_32BIT
#include "Vlvd_A3944_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
void Vlvd_A3944_Init(void);
void Vlvd_A3944_Process(void);
static void Vlvd_A3944_FaultStatusCheck(void);
static void Vlvd_A3944_PortControl(void);

#define VLVD_A3944_HNDLR_START_SEC_CODE
#include "Vlvd_A3944_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Vlvd_A3944_Init(void)
{
    Vlvd_A3944_GateSel.B.Address                = GateSelectAdd;
    
    Vlvd_A3944_FaultMask.B.Address              = FaultMaskAdd;
    
	/* Channel 0 Fault Config Spi Tx Signal Set */
    Vlvd_A3944_ChannelFaultCfgCh0.B.FixedAdd    = ChannelFaultConfigFixedAdd;
    Vlvd_A3944_ChannelFaultCfgCh0.B.Address     = VdrCh0;
    Vlvd_A3944_ChannelFaultCfgCh0.B.mu2_RT      = VdrM_ChannelConfig[VdrCh0].RT;
    Vlvd_A3944_ChannelFaultCfgCh0.B.mu1_NPD     = VdrM_ChannelConfig[VdrCh0].NPD;
    Vlvd_A3944_ChannelFaultCfgCh0.B.mu2_TOF     = VdrM_ChannelConfig[VdrCh0].TOF;
    Vlvd_A3944_ChannelFaultCfgCh0.B.mu2_TON     = VdrM_ChannelConfig[VdrCh0].TON;
    Vlvd_A3944_ChannelFaultCfgCh0.B.mu3_SB      = VdrM_ChannelConfig[VdrCh0].SB;
    Vlvd_A3944_ChannelFaultCfgCh0.B.mu1_SG      = VdrM_ChannelConfig[VdrCh0].SG;
    
	/* Channel 1 Fault Config Spi Tx Signal Set */
    Vlvd_A3944_ChannelFaultCfgCh1.B.FixedAdd    = ChannelFaultConfigFixedAdd;
    Vlvd_A3944_ChannelFaultCfgCh1.B.Address     = VdrCh1;
    Vlvd_A3944_ChannelFaultCfgCh1.B.mu2_RT      = VdrM_ChannelConfig[VdrCh1].RT;
    Vlvd_A3944_ChannelFaultCfgCh1.B.mu1_NPD     = VdrM_ChannelConfig[VdrCh1].NPD;
    Vlvd_A3944_ChannelFaultCfgCh1.B.mu2_TOF     = VdrM_ChannelConfig[VdrCh1].TOF;
    Vlvd_A3944_ChannelFaultCfgCh1.B.mu2_TON     = VdrM_ChannelConfig[VdrCh1].TON;
    Vlvd_A3944_ChannelFaultCfgCh1.B.mu3_SB      = VdrM_ChannelConfig[VdrCh1].SB;
    Vlvd_A3944_ChannelFaultCfgCh1.B.mu1_SG      = VdrM_ChannelConfig[VdrCh1].SG;
    
	/* Channel 2 Fault Config Spi Tx Signal Set */
    Vlvd_A3944_ChannelFaultCfgCh2.B.FixedAdd    = ChannelFaultConfigFixedAdd;
    Vlvd_A3944_ChannelFaultCfgCh2.B.Address     = VdrCh2;
    Vlvd_A3944_ChannelFaultCfgCh2.B.mu2_RT      = VdrM_ChannelConfig[VdrCh2].RT;
    Vlvd_A3944_ChannelFaultCfgCh2.B.mu1_NPD     = VdrM_ChannelConfig[VdrCh2].NPD;
    Vlvd_A3944_ChannelFaultCfgCh2.B.mu2_TOF     = VdrM_ChannelConfig[VdrCh2].TOF;
    Vlvd_A3944_ChannelFaultCfgCh2.B.mu2_TON     = VdrM_ChannelConfig[VdrCh2].TON;
    Vlvd_A3944_ChannelFaultCfgCh2.B.mu3_SB      = VdrM_ChannelConfig[VdrCh2].SB;
    Vlvd_A3944_ChannelFaultCfgCh2.B.mu1_SG      = VdrM_ChannelConfig[VdrCh2].SG;
    
	/* Channel 3 Fault Config Spi Tx Signal Set */
    Vlvd_A3944_ChannelFaultCfgCh3.B.FixedAdd    = ChannelFaultConfigFixedAdd;
    Vlvd_A3944_ChannelFaultCfgCh3.B.Address     = VdrCh3;
    Vlvd_A3944_ChannelFaultCfgCh3.B.mu2_RT      = VdrM_ChannelConfig[VdrCh3].RT;
    Vlvd_A3944_ChannelFaultCfgCh3.B.mu1_NPD     = VdrM_ChannelConfig[VdrCh3].NPD;
    Vlvd_A3944_ChannelFaultCfgCh3.B.mu2_TOF     = VdrM_ChannelConfig[VdrCh3].TOF;
    Vlvd_A3944_ChannelFaultCfgCh3.B.mu2_TON     = VdrM_ChannelConfig[VdrCh3].TON;
    Vlvd_A3944_ChannelFaultCfgCh3.B.mu3_SB      = VdrM_ChannelConfig[VdrCh3].SB;
    Vlvd_A3944_ChannelFaultCfgCh3.B.mu1_SG      = VdrM_ChannelConfig[VdrCh3].SG;
    
	/* Channel 4 Fault Config Spi Tx Signal Set */
    Vlvd_A3944_ChannelFaultCfgCh4.B.FixedAdd    = ChannelFaultConfigFixedAdd;
    Vlvd_A3944_ChannelFaultCfgCh4.B.Address     = VdrCh4;
    Vlvd_A3944_ChannelFaultCfgCh4.B.mu2_RT      = VdrM_ChannelConfig[VdrCh4].RT;
    Vlvd_A3944_ChannelFaultCfgCh4.B.mu1_NPD     = VdrM_ChannelConfig[VdrCh4].NPD;
    Vlvd_A3944_ChannelFaultCfgCh4.B.mu2_TOF     = VdrM_ChannelConfig[VdrCh4].TOF;
    Vlvd_A3944_ChannelFaultCfgCh4.B.mu2_TON     = VdrM_ChannelConfig[VdrCh4].TON;
    Vlvd_A3944_ChannelFaultCfgCh4.B.mu3_SB      = VdrM_ChannelConfig[VdrCh4].SB;
    Vlvd_A3944_ChannelFaultCfgCh4.B.mu1_SG      = VdrM_ChannelConfig[VdrCh4].SG;
    
	/* Channel 5 Fault Config Spi Tx Signal Set */
    Vlvd_A3944_ChannelFaultCfgCh5.B.FixedAdd    = ChannelFaultConfigFixedAdd;
    Vlvd_A3944_ChannelFaultCfgCh5.B.Address     = VdrCh5;
    Vlvd_A3944_ChannelFaultCfgCh5.B.mu2_RT      = VdrM_ChannelConfig[VdrCh5].RT;
    Vlvd_A3944_ChannelFaultCfgCh5.B.mu1_NPD     = VdrM_ChannelConfig[VdrCh5].NPD;
    Vlvd_A3944_ChannelFaultCfgCh5.B.mu2_TOF     = VdrM_ChannelConfig[VdrCh5].TOF;
    Vlvd_A3944_ChannelFaultCfgCh5.B.mu2_TON     = VdrM_ChannelConfig[VdrCh5].TON;
    Vlvd_A3944_ChannelFaultCfgCh5.B.mu3_SB      = VdrM_ChannelConfig[VdrCh5].SB;
    Vlvd_A3944_ChannelFaultCfgCh5.B.mu1_SG      = VdrM_ChannelConfig[VdrCh5].SG;
}
void Vlvd_A3944_Process(void)
{
    Vlvd_A3944_FaultStatusCheck();
    
    Vlvd_A3944_TxFrame[0] = (uint32)Vlvd_A3944_GateSel.R;
    Vlvd_A3944_TxFrame[1] = (uint32)Vlvd_A3944_FaultMask.R;
    Vlvd_A3944_TxFrame[2] = (uint32)Vlvd_A3944_ChannelFaultCfgCh0.R;
    Vlvd_A3944_TxFrame[3] = (uint32)Vlvd_A3944_ChannelFaultCfgCh1.R;
    Vlvd_A3944_TxFrame[4] = (uint32)Vlvd_A3944_ChannelFaultCfgCh2.R;
    Vlvd_A3944_TxFrame[5] = (uint32)Vlvd_A3944_ChannelFaultCfgCh3.R;
    Vlvd_A3944_TxFrame[6] = (uint32)Vlvd_A3944_ChannelFaultCfgCh4.R;
    Vlvd_A3944_TxFrame[7] = (uint32)Vlvd_A3944_ChannelFaultCfgCh5.R;

    Vlvd_MsgNum = 8;

    Spim_Cdd_SetupEB(Spim_Cdd_SpiChannel_Vdr_A3944, (Spim_Cdd_DataType*)Vlvd_A3944_TxFrame, (Spim_Cdd_DataType*)Vlvd_A3944_RxFrame, Vlvd_MsgNum);
    Vlvd_A3944_PortControl();
}



/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
/****************************************************************************
| NAME:             VdrM_FaultStatusCheck
| CALLED BY:        
| PRECONDITIONS:    none
|
| INPUT PARAMETERS: none
|
| RETURN VALUES:    none
|
| DESCRIPTION:      VdrM module Fault Check
|
|  <Fault Registers Descriptions>
|  FF: Logic 1 if any faults have been detected since the last fault reset.
|  UV: Logic 1 if any VDD or VREG undervoltage faults have been detected since the last fault reset.
|  LR: Logic 1 if a logic reset has occurred since the last register read. 
|      A logic reset is caused by a power on-reset or by taking the RESETN input low.
|  OT: Logic 1 if an overtemperature fault has been detected since the last fault reset.
|  FR: Fault register identifier.
|     - Logic 0 for Fault0 register,
|     - logic 1 for Fault1 register.
|  CxSG: Logic 1 if channel short to ground detected.
|  CxSB: Logic 1 if channel short to battery detected.
|  CxOL: Logic 1 if channel open load detected.
|      Where x is channel number.
****************************************************************************/
static void Vlvd_A3944_FaultStatusCheck(void)
{
    uint8 i = 0;
    
    for(i=0;i<Vlvd_MsgNum;i++)
    {
        Vlvd_A3944_Fault.R = (uint16)Vlvd_A3944_RxFrame[i];
        if(Vlvd_A3944_Fault.B.mu1_FF == 0x1)
        {
            if(Vlvd_A3944_Fault.B.mu1_FR == FaultReg0)
            {
                Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF0DcdInfo.Ff      = (Haluint8)Vlvd_A3944_Fault.B.mu1_FF;
                Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF0DcdInfo.Uv      = (Haluint8)Vlvd_A3944_Fault.B.mu1_UV;
                Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF0DcdInfo.Lr      = (Haluint8)Vlvd_A3944_Fault.B.mu1_LR;
                Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF0DcdInfo.Ot      = (Haluint8)Vlvd_A3944_Fault.B.mu1_OT;
                Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF0DcdInfo.Fr      = (Haluint8)Vlvd_A3944_Fault.B.mu1_FR;
                Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF0DcdInfo.C2sg    = (Haluint8)Vlvd_A3944_Fault.B.mu1_C2SG;
                Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF0DcdInfo.C2sb    = (Haluint8)Vlvd_A3944_Fault.B.mu1_C2SB;
                Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF0DcdInfo.C2ol    = (Haluint8)Vlvd_A3944_Fault.B.mu1_C2OL;
                Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF0DcdInfo.C1sg    = (Haluint8)Vlvd_A3944_Fault.B.mu1_C1SG;
                Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF0DcdInfo.C1sb    = (Haluint8)Vlvd_A3944_Fault.B.mu1_C1SB;
                Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF0DcdInfo.C1ol    = (Haluint8)Vlvd_A3944_Fault.B.mu1_C1OL;
                Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF0DcdInfo.C0sg    = (Haluint8)Vlvd_A3944_Fault.B.mu1_C0SG;
                Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF0DcdInfo.C0sb    = (Haluint8)Vlvd_A3944_Fault.B.mu1_C0SB;
                Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF0DcdInfo.C0ol    = (Haluint8)Vlvd_A3944_Fault.B.mu1_C0OL;
            }
            else if(Vlvd_A3944_Fault.B.mu1_FR == FaultReg1)
            {
                Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF1DcdInfo.Ff      = (Haluint8)Vlvd_A3944_Fault.B1.mu1_FF;
                Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF1DcdInfo.Uv      = (Haluint8)Vlvd_A3944_Fault.B1.mu1_UV;
                Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF1DcdInfo.Lr      = (Haluint8)Vlvd_A3944_Fault.B1.mu1_LR;
                Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF1DcdInfo.Ot      = (Haluint8)Vlvd_A3944_Fault.B1.mu1_OT;
                Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF1DcdInfo.Fr      = (Haluint8)Vlvd_A3944_Fault.B1.mu1_FR;
                Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF1DcdInfo.C5sg    = (Haluint8)Vlvd_A3944_Fault.B1.mu1_C5SG;
                Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF1DcdInfo.C5sb    = (Haluint8)Vlvd_A3944_Fault.B1.mu1_C5SB;
                Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF1DcdInfo.C5ol    = (Haluint8)Vlvd_A3944_Fault.B1.mu1_C5OL;
                Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF1DcdInfo.C4sg    = (Haluint8)Vlvd_A3944_Fault.B1.mu1_C4SG;
                Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF1DcdInfo.C4sb    = (Haluint8)Vlvd_A3944_Fault.B1.mu1_C4SB;
                Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF1DcdInfo.C4ol    = (Haluint8)Vlvd_A3944_Fault.B1.mu1_C4OL;
                Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF1DcdInfo.C3sg    = (Haluint8)Vlvd_A3944_Fault.B1.mu1_C3SG;
                Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF1DcdInfo.C3sb    = (Haluint8)Vlvd_A3944_Fault.B1.mu1_C3SB;
                Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF1DcdInfo.C3ol    = (Haluint8)Vlvd_A3944_Fault.B1.mu1_C3OL;
            }
            else
            {
                ;
            }
        }

    Vlvd_A3944_TxFrame[i] = 0;
    Vlvd_A3944_RxFrame[i] = 0;
    }

    
 }

 static void Vlvd_A3944_PortControl(void)
 {
    static uint8 delay; /* TEMP for waiting until FSP is ON to prevent wrong STG, OL detection */

    if(delay > 5)
    {
    	Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvDrvEnRst = STD_HIGH;
		/*
        if(Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvDrvEemInfo.VlvDrvReq == STD_ON)
        {
            Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvDrvEnRst = Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvDrvEemInfo.VlvDrvEn;
        }
        else
        {
            Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvDrvEnRst = STD_HIGH;
        }
		*/
    }
    else
    {
        delay++;
    }
 }
 
#define VLVD_A3944_HNDLR_STOP_SEC_CODE
#include "Vlvd_A3944_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
