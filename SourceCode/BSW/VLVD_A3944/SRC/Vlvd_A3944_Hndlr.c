/**
 * @defgroup Vlvd_A3944_Hndlr Vlvd_A3944_Hndlr
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Vlvd_A3944_Hndlr.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Vlvd_A3944_Hndlr.h"
#include "Vlvd_A3944_Hndlr_Ifa.h"
#include "IfxStm_reg.h"
#include "Vlvd_A3944_Process.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define VLVD_A3944_HNDLR_START_SEC_CONST_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
#include "Spim_Cdd.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define VLVD_A3944_HNDLR_STOP_SEC_CONST_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define VLVD_A3944_HNDLR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Vlvd_A3944_Hndlr_HdrBusType Vlvd_A3944_HndlrBus;

/* Version Info */
const SwcVersionInfo_t Vlvd_A3944_HndlrVersionInfo = 
{   
    VLVD_A3944_HNDLR_MODULE_ID,           /* Vlvd_A3944_HndlrVersionInfo.ModuleId */
    VLVD_A3944_HNDLR_MAJOR_VERSION,       /* Vlvd_A3944_HndlrVersionInfo.MajorVer */
    VLVD_A3944_HNDLR_MINOR_VERSION,       /* Vlvd_A3944_HndlrVersionInfo.MinorVer */
    VLVD_A3944_HNDLR_PATCH_VERSION,       /* Vlvd_A3944_HndlrVersionInfo.PatchVer */
    VLVD_A3944_HNDLR_BRANCH_VERSION       /* Vlvd_A3944_HndlrVersionInfo.BranchVer */
};
    
/* Input Data Element */
Mom_HndlrEcuModeSts_t Vlvd_A3944_HndlrEcuModeSts;
Eem_SuspcDetnFuncInhibitVlvdSts_t Vlvd_A3944_HndlrFuncInhibitVlvdSts;

/* Output Data Element */
Vlvd_A3944_HndlrVlvdFF0DcdInfo_t Vlvd_A3944_HndlrVlvdFF0DcdInfo;
Vlvd_A3944_HndlrVlvdFF1DcdInfo_t Vlvd_A3944_HndlrVlvdFF1DcdInfo;
Vlvd_A3944_HndlrVlvdInvalid_t Vlvd_A3944_HndlrVlvdInvalid;
Vlvd_A3944_HndlrVlvDrvEnRst_t Vlvd_A3944_HndlrVlvDrvEnRst;

uint32 Vlvd_A3944_Hndlr_Timer_Start;
uint32 Vlvd_A3944_Hndlr_Timer_Elapsed;

#define VLVD_A3944_HNDLR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
#define VLVD_A3944_HNDLR_START_SEC_VAR_NOINIT_32BIT
#include "Vlvd_A3944_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define VLVD_A3944_HNDLR_STOP_SEC_VAR_NOINIT_32BIT
#include "Vlvd_A3944_MemMap.h"
#define VLVD_A3944_HNDLR_START_SEC_VAR_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define VLVD_A3944_HNDLR_STOP_SEC_VAR_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
#define VLVD_A3944_HNDLR_START_SEC_VAR_32BIT
#include "Vlvd_A3944_MemMap.h"
/** Variable Section (32BIT)**/


#define VLVD_A3944_HNDLR_STOP_SEC_VAR_32BIT
#include "Vlvd_A3944_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define VLVD_A3944_HNDLR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define VLVD_A3944_HNDLR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
#define VLVD_A3944_HNDLR_START_SEC_VAR_NOINIT_32BIT
#include "Vlvd_A3944_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define VLVD_A3944_HNDLR_STOP_SEC_VAR_NOINIT_32BIT
#include "Vlvd_A3944_MemMap.h"
#define VLVD_A3944_HNDLR_START_SEC_VAR_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define VLVD_A3944_HNDLR_STOP_SEC_VAR_UNSPECIFIED
#include "Vlvd_A3944_MemMap.h"
#define VLVD_A3944_HNDLR_START_SEC_VAR_32BIT
#include "Vlvd_A3944_MemMap.h"
/** Variable Section (32BIT)**/


#define VLVD_A3944_HNDLR_STOP_SEC_VAR_32BIT
#include "Vlvd_A3944_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define VLVD_A3944_HNDLR_START_SEC_CODE
#include "Vlvd_A3944_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Vlvd_A3944_Hndlr_Init(void)
{
    /* Initialize internal bus */
    Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrEcuModeSts = 0;
    Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrFuncInhibitVlvdSts = 0;
    Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF0DcdInfo.C0ol = 0;
    Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF0DcdInfo.C0sb = 0;
    Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF0DcdInfo.C0sg = 0;
    Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF0DcdInfo.C1ol = 0;
    Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF0DcdInfo.C1sb = 0;
    Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF0DcdInfo.C1sg = 0;
    Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF0DcdInfo.C2ol = 0;
    Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF0DcdInfo.C2sb = 0;
    Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF0DcdInfo.C2sg = 0;
    Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF0DcdInfo.Fr = 0;
    Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF0DcdInfo.Ot = 0;
    Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF0DcdInfo.Lr = 0;
    Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF0DcdInfo.Uv = 0;
    Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF0DcdInfo.Ff = 0;
    Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF1DcdInfo.C3ol = 0;
    Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF1DcdInfo.C3sb = 0;
    Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF1DcdInfo.C3sg = 0;
    Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF1DcdInfo.C4ol = 0;
    Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF1DcdInfo.C4sb = 0;
    Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF1DcdInfo.C4sg = 0;
    Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF1DcdInfo.C5ol = 0;
    Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF1DcdInfo.C5sb = 0;
    Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF1DcdInfo.C5sg = 0;
    Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF1DcdInfo.Fr = 0;
    Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF1DcdInfo.Ot = 0;
    Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF1DcdInfo.Lr = 0;
    Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF1DcdInfo.Uv = 0;
    Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF1DcdInfo.Ff = 0;
    Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdInvalid = 0;
    Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvDrvEnRst = 0;
    Vlvd_A3944_Init();
}

void Vlvd_A3944_Hndlr(void)
{
    Vlvd_A3944_Hndlr_Timer_Start = STM0_TIM0.U;

    /* Input */
    Vlvd_A3944_Hndlr_Read_Vlvd_A3944_HndlrEcuModeSts(&Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrEcuModeSts);
    Vlvd_A3944_Hndlr_Read_Vlvd_A3944_HndlrFuncInhibitVlvdSts(&Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrFuncInhibitVlvdSts);

    /* Process */
    Vlvd_A3944_Process();
    
    /* Output */
    Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvdFF0DcdInfo(&Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF0DcdInfo);
    /*==============================================================================
    * Members of structure Vlvd_A3944_HndlrVlvdFF0DcdInfo 
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.C0ol;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.C0sb;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.C0sg;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.C1ol;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.C1sb;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.C1sg;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.C2ol;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.C2sb;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.C2sg;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.Fr;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.Ot;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.Lr;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.Uv;
     : Vlvd_A3944_HndlrVlvdFF0DcdInfo.Ff;
     =============================================================================*/
    
    Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvdFF1DcdInfo(&Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdFF1DcdInfo);
    /*==============================================================================
    * Members of structure Vlvd_A3944_HndlrVlvdFF1DcdInfo 
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.C3ol;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.C3sb;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.C3sg;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.C4ol;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.C4sb;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.C4sg;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.C5ol;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.C5sb;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.C5sg;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.Fr;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.Ot;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.Lr;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.Uv;
     : Vlvd_A3944_HndlrVlvdFF1DcdInfo.Ff;
     =============================================================================*/
    
    Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvdInvalid(&Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvdInvalid);
    Vlvd_A3944_Hndlr_Write_Vlvd_A3944_HndlrVlvDrvEnRst(&Vlvd_A3944_HndlrBus.Vlvd_A3944_HndlrVlvDrvEnRst);

    Vlvd_A3944_Hndlr_Timer_Elapsed = STM0_TIM0.U - Vlvd_A3944_Hndlr_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define VLVD_A3944_HNDLR_STOP_SEC_CODE
#include "Vlvd_A3944_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
