/**
 * @defgroup Msp_Types Msp_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Msp_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MSP_TYPES_H_
#define MSP_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Rte_Types.h"
#include "RteDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Acmio_SenMotAngle1Info_t Msp_CtrlMotAngle1Info;
    Ses_CtrlMotOrgSetStInfo_t Msp_CtrlMotOrgSetStInfo;
    Ses_CtrlPwrPistStBfMotOrgSetInfo_t Msp_CtrlPwrPistStBfMotOrgSetInfo;
    Mom_HndlrEcuModeSts_t Msp_CtrlEcuModeSts;
    Eem_SuspcDetnFuncInhibitMspSts_t Msp_CtrlFuncInhibitMspSts;

/* Output Data Element */
    Msp_CtrlMotRotgAgSigInfo_t Msp_CtrlMotRotgAgSigInfo;
    Msp_CtrlMotRotgAgSigBfMotOrgSetInfo_t Msp_CtrlMotRotgAgSigBfMotOrgSetInfo;
}Msp_Ctrl_HdrBusType;

typedef struct
{
/* Input Data Element */
    Acmio_SenVdcLink_t Msp_1msCtrlVdcLink;

/* Output Data Element */
    Msp_1msCtrlVdcLinkFild_t Msp_1msCtrlVdcLinkFild;
}Msp_1msCtrl_HdrBusType;


/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MSP_TYPES_H_ */
/** @} */
