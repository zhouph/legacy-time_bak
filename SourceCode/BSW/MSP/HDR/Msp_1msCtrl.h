/**
 * @defgroup Msp_1msCtrl Msp_1msCtrl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Msp_1msCtrl.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MSP_1MSCTRL_H_
#define MSP_1MSCTRL_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Msp_Types.h"
#include "Msp_Cfg.h"
#include "Msp_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define MSP_1MSCTRL_MODULE_ID      (0)
 #define MSP_1MSCTRL_MAJOR_VERSION  (2)
 #define MSP_1MSCTRL_MINOR_VERSION  (0)
 #define MSP_1MSCTRL_PATCH_VERSION  (0)
 #define MSP_1MSCTRL_BRANCH_VERSION (0)

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Msp_1msCtrl_HdrBusType Msp_1msCtrlBus;

/* Version Info */
extern const SwcVersionInfo_t Msp_1msCtrlVersionInfo;

/* Input Data Element */
extern Acmio_SenVdcLink_t Msp_1msCtrlVdcLink;

/* Output Data Element */
extern Msp_1msCtrlVdcLinkFild_t Msp_1msCtrlVdcLinkFild;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Msp_1msCtrl_Init(void);
extern void Msp_1msCtrl(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MSP_1MSCTRL_H_ */
/** @} */
