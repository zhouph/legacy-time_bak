/**
 * @defgroup Msp_Ctrl Msp_Ctrl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Msp_Ctrl.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MSP_CTRL_H_
#define MSP_CTRL_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Msp_Types.h"
#include "Msp_Cfg.h"
#include "Msp_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define MSP_CTRL_MODULE_ID      (0)
 #define MSP_CTRL_MAJOR_VERSION  (2)
 #define MSP_CTRL_MINOR_VERSION  (0)
 #define MSP_CTRL_PATCH_VERSION  (0)
 #define MSP_CTRL_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Msp_Ctrl_HdrBusType Msp_CtrlBus;

/* Version Info */
extern const SwcVersionInfo_t Msp_CtrlVersionInfo;

/* Input Data Element */
extern Acmio_SenMotAngle1Info_t Msp_CtrlMotAngle1Info;
extern Ses_CtrlMotOrgSetStInfo_t Msp_CtrlMotOrgSetStInfo;
extern Ses_CtrlPwrPistStBfMotOrgSetInfo_t Msp_CtrlPwrPistStBfMotOrgSetInfo;
extern Mom_HndlrEcuModeSts_t Msp_CtrlEcuModeSts;
extern Eem_SuspcDetnFuncInhibitMspSts_t Msp_CtrlFuncInhibitMspSts;

/* Output Data Element */
extern Msp_CtrlMotRotgAgSigInfo_t Msp_CtrlMotRotgAgSigInfo;
extern Msp_CtrlMotRotgAgSigBfMotOrgSetInfo_t Msp_CtrlMotRotgAgSigBfMotOrgSetInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Msp_Ctrl_Init(void);
extern void Msp_Ctrl(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MSP_CTRL_H_ */
/** @} */
