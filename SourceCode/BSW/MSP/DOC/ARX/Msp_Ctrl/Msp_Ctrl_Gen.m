Msp_CtrlMotAngle1Info = Simulink.Bus;
DeList={
    'MotElecAngle1'
    'MotMechAngle1'
    };
Msp_CtrlMotAngle1Info = CreateBus(Msp_CtrlMotAngle1Info, DeList);
clear DeList;

Msp_CtrlMotOrgSetStInfo = Simulink.Bus;
DeList={
    'MotOrgSet'
    };
Msp_CtrlMotOrgSetStInfo = CreateBus(Msp_CtrlMotOrgSetStInfo, DeList);
clear DeList;

Msp_CtrlPwrPistStBfMotOrgSetInfo = Simulink.Bus;
DeList={
    'PosiWallTemp'
    'WallDetectedFlg'
    };
Msp_CtrlPwrPistStBfMotOrgSetInfo = CreateBus(Msp_CtrlPwrPistStBfMotOrgSetInfo, DeList);
clear DeList;

Msp_CtrlEcuModeSts = Simulink.Bus;
DeList={'Msp_CtrlEcuModeSts'};
Msp_CtrlEcuModeSts = CreateBus(Msp_CtrlEcuModeSts, DeList);
clear DeList;

Msp_CtrlFuncInhibitMspSts = Simulink.Bus;
DeList={'Msp_CtrlFuncInhibitMspSts'};
Msp_CtrlFuncInhibitMspSts = CreateBus(Msp_CtrlFuncInhibitMspSts, DeList);
clear DeList;

Msp_CtrlMotRotgAgSigInfo = Simulink.Bus;
DeList={
    'StkPosnMeasd'
    'MotElecAngleFild'
    'MotMechAngleSpdFild'
    };
Msp_CtrlMotRotgAgSigInfo = CreateBus(Msp_CtrlMotRotgAgSigInfo, DeList);
clear DeList;

Msp_CtrlMotRotgAgSigBfMotOrgSetInfo = Simulink.Bus;
DeList={
    'StkPosnMeasdBfMotOrgSet'
    'MotMechAngleSpdBfMotOrgSet'
    };
Msp_CtrlMotRotgAgSigBfMotOrgSetInfo = CreateBus(Msp_CtrlMotRotgAgSigBfMotOrgSetInfo, DeList);
clear DeList;

