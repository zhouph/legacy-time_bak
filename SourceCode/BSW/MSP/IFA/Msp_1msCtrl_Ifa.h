/**
 * @defgroup Msp_1msCtrl_Ifa Msp_1msCtrl_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Msp_1msCtrl_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MSP_1MSCTRL_IFA_H_
#define MSP_1MSCTRL_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Msp_1msCtrl_Read_Msp_1msCtrlVdcLink(data) do \
{ \
    *data = Msp_1msCtrlVdcLink; \
}while(0);


/* Set Output DE MAcro Function */
#define Msp_1msCtrl_Write_Msp_1msCtrlVdcLinkFild(data) do \
{ \
    Msp_1msCtrlVdcLinkFild = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MSP_1MSCTRL_IFA_H_ */
/** @} */
