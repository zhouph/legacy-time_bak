/**
 * @defgroup Msp_1msCtrl_Ifa Msp_1msCtrl_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Msp_1msCtrl_Ifa.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Msp_1msCtrl_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define MSP_1MSCTRL_START_SEC_CONST_UNSPECIFIED
#include "Msp_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define MSP_1MSCTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Msp_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MSP_1MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Msp_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define MSP_1MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Msp_MemMap.h"
#define MSP_1MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Msp_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MSP_1MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Msp_MemMap.h"
#define MSP_1MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Msp_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MSP_1MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Msp_MemMap.h"
#define MSP_1MSCTRL_START_SEC_VAR_32BIT
#include "Msp_MemMap.h"
/** Variable Section (32BIT)**/


#define MSP_1MSCTRL_STOP_SEC_VAR_32BIT
#include "Msp_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MSP_1MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Msp_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define MSP_1MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Msp_MemMap.h"
#define MSP_1MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Msp_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MSP_1MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Msp_MemMap.h"
#define MSP_1MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Msp_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MSP_1MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Msp_MemMap.h"
#define MSP_1MSCTRL_START_SEC_VAR_32BIT
#include "Msp_MemMap.h"
/** Variable Section (32BIT)**/


#define MSP_1MSCTRL_STOP_SEC_VAR_32BIT
#include "Msp_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define MSP_1MSCTRL_START_SEC_CODE
#include "Msp_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define MSP_1MSCTRL_STOP_SEC_CODE
#include "Msp_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
