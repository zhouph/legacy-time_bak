/**
 * @defgroup Msp_Ctrl_Ifa Msp_Ctrl_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Msp_Ctrl_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MSP_CTRL_IFA_H_
#define MSP_CTRL_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Msp_Ctrl_Read_Msp_CtrlMotAngle1Info(data) do \
{ \
    *data = Msp_CtrlMotAngle1Info; \
}while(0);

#define Msp_Ctrl_Read_Msp_CtrlMotOrgSetStInfo(data) do \
{ \
    *data = Msp_CtrlMotOrgSetStInfo; \
}while(0);

#define Msp_Ctrl_Read_Msp_CtrlPwrPistStBfMotOrgSetInfo(data) do \
{ \
    *data = Msp_CtrlPwrPistStBfMotOrgSetInfo; \
}while(0);

#define Msp_Ctrl_Read_Msp_CtrlMotAngle1Info_MotElecAngle1(data) do \
{ \
    *data = Msp_CtrlMotAngle1Info.MotElecAngle1; \
}while(0);

#define Msp_Ctrl_Read_Msp_CtrlMotAngle1Info_MotMechAngle1(data) do \
{ \
    *data = Msp_CtrlMotAngle1Info.MotMechAngle1; \
}while(0);

#define Msp_Ctrl_Read_Msp_CtrlMotOrgSetStInfo_MotOrgSet(data) do \
{ \
    *data = Msp_CtrlMotOrgSetStInfo.MotOrgSet; \
}while(0);

#define Msp_Ctrl_Read_Msp_CtrlPwrPistStBfMotOrgSetInfo_PosiWallTemp(data) do \
{ \
    *data = Msp_CtrlPwrPistStBfMotOrgSetInfo.PosiWallTemp; \
}while(0);

#define Msp_Ctrl_Read_Msp_CtrlPwrPistStBfMotOrgSetInfo_WallDetectedFlg(data) do \
{ \
    *data = Msp_CtrlPwrPistStBfMotOrgSetInfo.WallDetectedFlg; \
}while(0);

#define Msp_Ctrl_Read_Msp_CtrlEcuModeSts(data) do \
{ \
    *data = Msp_CtrlEcuModeSts; \
}while(0);

#define Msp_Ctrl_Read_Msp_CtrlFuncInhibitMspSts(data) do \
{ \
    *data = Msp_CtrlFuncInhibitMspSts; \
}while(0);


/* Set Output DE MAcro Function */
#define Msp_Ctrl_Write_Msp_CtrlMotRotgAgSigInfo(data) do \
{ \
    Msp_CtrlMotRotgAgSigInfo = *data; \
}while(0);

#define Msp_Ctrl_Write_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo(data) do \
{ \
    Msp_CtrlMotRotgAgSigBfMotOrgSetInfo = *data; \
}while(0);

#define Msp_Ctrl_Write_Msp_CtrlMotRotgAgSigInfo_StkPosnMeasd(data) do \
{ \
    Msp_CtrlMotRotgAgSigInfo.StkPosnMeasd = *data; \
}while(0);

#define Msp_Ctrl_Write_Msp_CtrlMotRotgAgSigInfo_MotElecAngleFild(data) do \
{ \
    Msp_CtrlMotRotgAgSigInfo.MotElecAngleFild = *data; \
}while(0);

#define Msp_Ctrl_Write_Msp_CtrlMotRotgAgSigInfo_MotMechAngleSpdFild(data) do \
{ \
    Msp_CtrlMotRotgAgSigInfo.MotMechAngleSpdFild = *data; \
}while(0);

#define Msp_Ctrl_Write_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo_StkPosnMeasdBfMotOrgSet(data) do \
{ \
    Msp_CtrlMotRotgAgSigBfMotOrgSetInfo.StkPosnMeasdBfMotOrgSet = *data; \
}while(0);

#define Msp_Ctrl_Write_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo_MotMechAngleSpdBfMotOrgSet(data) do \
{ \
    Msp_CtrlMotRotgAgSigBfMotOrgSetInfo.MotMechAngleSpdBfMotOrgSet = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MSP_CTRL_IFA_H_ */
/** @} */
