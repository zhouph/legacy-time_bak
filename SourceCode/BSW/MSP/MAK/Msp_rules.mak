# \file
#
# \brief Msp
#
# This file contains the implementation of the SWC
# module Msp.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Msp_src

Msp_src_FILES        += $(Msp_SRC_PATH)\Msp_Ctrl.c
Msp_src_FILES        += $(Msp_IFA_PATH)\Msp_Ctrl_Ifa.c
Msp_src_FILES        += $(Msp_SRC_PATH)\Msp_1msCtrl.c
Msp_src_FILES        += $(Msp_IFA_PATH)\Msp_1msCtrl_Ifa.c
Msp_src_FILES        += $(Msp_CFG_PATH)\Msp_Cfg.c
Msp_src_FILES        += $(Msp_CAL_PATH)\Msp_Cal.c
Msp_src_FILES        += $(Msp_SRC_PATH)\Msp_CalcMotSigProc.c
Msp_src_FILES        += $(Msp_SRC_PATH)\Msp_FilterVdcLink.c
ifeq ($(ICE_COMPILE),true)
Msp_src_FILES        += $(Msp_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	Msp_src_FILES        += $(Msp_UNITY_PATH)\unity.c
	Msp_src_FILES        += $(Msp_UNITY_PATH)\unity_fixture.c	
	Msp_src_FILES        += $(Msp_UT_PATH)\main.c
	Msp_src_FILES        += $(Msp_UT_PATH)\Msp_Ctrl\Msp_Ctrl_UtMain.c
	Msp_src_FILES        += $(Msp_UT_PATH)\Msp_1msCtrl\Msp_1msCtrl_UtMain.c
endif