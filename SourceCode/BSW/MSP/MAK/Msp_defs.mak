# \file
#
# \brief Msp
#
# This file contains the implementation of the SWC
# module Msp.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Msp_CORE_PATH     := $(MANDO_BSW_ROOT)\Msp
Msp_CAL_PATH      := $(Msp_CORE_PATH)\CAL\$(Msp_VARIANT)
Msp_SRC_PATH      := $(Msp_CORE_PATH)\SRC
Msp_CFG_PATH      := $(Msp_CORE_PATH)\CFG\$(Msp_VARIANT)
Msp_HDR_PATH      := $(Msp_CORE_PATH)\HDR
Msp_IFA_PATH      := $(Msp_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Msp_CMN_PATH      := $(Msp_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	Msp_UT_PATH		:= $(Msp_CORE_PATH)\UT
	Msp_UNITY_PATH	:= $(Msp_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(Msp_UT_PATH)
	CC_INCLUDE_PATH		+= $(Msp_UNITY_PATH)
	Msp_Ctrl_PATH 	:= Msp_UT_PATH\Msp_Ctrl
	Msp_1msCtrl_PATH 	:= Msp_UT_PATH\Msp_1msCtrl
endif
CC_INCLUDE_PATH    += $(Msp_CAL_PATH)
CC_INCLUDE_PATH    += $(Msp_SRC_PATH)
CC_INCLUDE_PATH    += $(Msp_CFG_PATH)
CC_INCLUDE_PATH    += $(Msp_HDR_PATH)
CC_INCLUDE_PATH    += $(Msp_IFA_PATH)
CC_INCLUDE_PATH    += $(Msp_CMN_PATH)

