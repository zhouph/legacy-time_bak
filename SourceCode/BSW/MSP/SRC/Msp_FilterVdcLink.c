/**
 * @defgroup LAMTR_SigProc LAMTR_SigProc
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        LAMTR_SigProc.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Msp_FilterVdcLink.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define MSP_S16_VDCLINK_MAX		(20000)
#define MSP_S16_VDCLINK_MIN		(0)

#define MSP_LPF_1_FIL_K_FC_25HZ_TS1MS   (17)
#define MSP_LPF_1_FIL_K_FC_20HZ_TS1MS   (14)
#define MSP_LPF_1_FIL_K_FC_15HZ_TS1MS   (11)
#define MSP_LPF_1_FIL_K_FC_10HZ_TS1MS   (8)

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
	sint16 lmsps16VdcLinkRaw;
} GetMotCtrlSigProc_t;
/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define MSP_1MSCTRL_START_SEC_CONST_UNSPECIFIED
#include "Msp_MemMap.h"
/** Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
#define MSP_1MSCTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Msp_MemMap.h"

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MSP_1MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Msp_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
#define MSP_1MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Msp_MemMap.h"



#define MSP_1MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Msp_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define MSP_1MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Msp_MemMap.h"



#define MSP_1MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Msp_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define MSP_1MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Msp_MemMap.h"



#define MSP_1MSCTRL_START_SEC_VAR_32BIT
#include "Msp_MemMap.h"
/** Variable Section (32BIT)**/
#define MSP_1MSCTRL_STOP_SEC_VAR_32BIT
#include "Msp_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MSP_1MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Msp_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

sint16 lmsps16VdcLink;
sint16 lmsps16VdcLinkOld;

GetMotCtrlSigProc_t GetMotCtrlSigProc;

#define MSP_1MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Msp_MemMap.h"



#define MSP_1MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Msp_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define MSP_1MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Msp_MemMap.h"



#define MSP_1MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Msp_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define MSP_1MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Msp_MemMap.h"



#define MSP_1MSCTRL_START_SEC_VAR_32BIT
#include "Msp_MemMap.h"
/** Variable Section (32BIT)**/
#define MSP_1MSCTRL_STOP_SEC_VAR_32BIT
#include "Msp_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/

#define MSP_1MSCTRL_START_SEC_CODE
#include "Msp_MemMap.h"

static void Msp_vCalcVdcLink(void);
static void Msp_vInpIfSigProcForMotCtrl(void);
static void Msp_vOutpIfSigProcForMotCtrl(void);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void LAMTR_vCallSigProcForMotCtrl(void)
{
	Msp_vInpIfSigProcForMotCtrl();
	Msp_vCalcVdcLink();
	Msp_vOutpIfSigProcForMotCtrl();
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static void Msp_vInpIfSigProcForMotCtrl(void)
{
	GetMotCtrlSigProc.lmsps16VdcLinkRaw = (sint16)Msp_1msCtrlBus.Msp_1msCtrlVdcLink;

}
static void Msp_vOutpIfSigProcForMotCtrl(void)
{
	Msp_1msCtrlBus.Msp_1msCtrlVdcLinkFild = lmsps16VdcLink;
}


static void Msp_vCalcVdcLink(void)
{
	lmsps16VdcLinkOld = lmsps16VdcLink;
	if (GetMotCtrlSigProc.lmsps16VdcLinkRaw > MSP_S16_VDCLINK_MAX)
	{
		GetMotCtrlSigProc.lmsps16VdcLinkRaw = MSP_S16_VDCLINK_MAX;
	}
	else if (GetMotCtrlSigProc.lmsps16VdcLinkRaw < MSP_S16_VDCLINK_MIN)
	{
		GetMotCtrlSigProc.lmsps16VdcLinkRaw = MSP_S16_VDCLINK_MIN;
	}
	else
	{
		;
	}

	lmsps16VdcLink = L_s16Lpf1Int1ms(GetMotCtrlSigProc.lmsps16VdcLinkRaw, lmsps16VdcLinkOld, MSP_LPF_1_FIL_K_FC_10HZ_TS1MS);
}

#define MSP_1MSCTRL_STOP_SEC_CODE
#include "Msp_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */


