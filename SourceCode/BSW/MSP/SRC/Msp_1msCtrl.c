/**
 * @defgroup Msp_1msCtrl Msp_1msCtrl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Msp_1msCtrl.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Msp_1msCtrl.h"
#include "Msp_1msCtrl_Ifa.h"
#include "IfxStm_reg.h"
#include "Msp_FilterVdcLink.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define MSP_1MSCTRL_START_SEC_CONST_UNSPECIFIED
#include "Msp_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define MSP_1MSCTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Msp_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MSP_1MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Msp_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Msp_1msCtrl_HdrBusType Msp_1msCtrlBus;

/* Version Info */
const SwcVersionInfo_t Msp_1msCtrlVersionInfo = 
{   
    MSP_1MSCTRL_MODULE_ID,           /* Msp_1msCtrlVersionInfo.ModuleId */
    MSP_1MSCTRL_MAJOR_VERSION,       /* Msp_1msCtrlVersionInfo.MajorVer */
    MSP_1MSCTRL_MINOR_VERSION,       /* Msp_1msCtrlVersionInfo.MinorVer */
    MSP_1MSCTRL_PATCH_VERSION,       /* Msp_1msCtrlVersionInfo.PatchVer */
    MSP_1MSCTRL_BRANCH_VERSION       /* Msp_1msCtrlVersionInfo.BranchVer */
};
    
/* Input Data Element */
Acmio_SenVdcLink_t Msp_1msCtrlVdcLink;

/* Output Data Element */
Msp_1msCtrlVdcLinkFild_t Msp_1msCtrlVdcLinkFild;

uint32 Msp_1msCtrl_Timer_Start;
uint32 Msp_1msCtrl_Timer_Elapsed;

#define MSP_1MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Msp_MemMap.h"
#define MSP_1MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Msp_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MSP_1MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Msp_MemMap.h"
#define MSP_1MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Msp_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MSP_1MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Msp_MemMap.h"
#define MSP_1MSCTRL_START_SEC_VAR_32BIT
#include "Msp_MemMap.h"
/** Variable Section (32BIT)**/


#define MSP_1MSCTRL_STOP_SEC_VAR_32BIT
#include "Msp_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MSP_1MSCTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Msp_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define MSP_1MSCTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Msp_MemMap.h"
#define MSP_1MSCTRL_START_SEC_VAR_NOINIT_32BIT
#include "Msp_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MSP_1MSCTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Msp_MemMap.h"
#define MSP_1MSCTRL_START_SEC_VAR_UNSPECIFIED
#include "Msp_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MSP_1MSCTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Msp_MemMap.h"
#define MSP_1MSCTRL_START_SEC_VAR_32BIT
#include "Msp_MemMap.h"
/** Variable Section (32BIT)**/


#define MSP_1MSCTRL_STOP_SEC_VAR_32BIT
#include "Msp_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define MSP_1MSCTRL_START_SEC_CODE
#include "Msp_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Msp_1msCtrl_Init(void)
{
    /* Initialize internal bus */
    Msp_1msCtrlBus.Msp_1msCtrlVdcLink = 0;
    Msp_1msCtrlBus.Msp_1msCtrlVdcLinkFild = 0;
}

void Msp_1msCtrl(void)
{
    Msp_1msCtrl_Timer_Start = STM0_TIM0.U;

    /* Input */
    Msp_1msCtrl_Read_Msp_1msCtrlVdcLink(&Msp_1msCtrlBus.Msp_1msCtrlVdcLink);

    /* Process */
    LAMTR_vCallSigProcForMotCtrl();

    /* Output */
    Msp_1msCtrl_Write_Msp_1msCtrlVdcLinkFild(&Msp_1msCtrlBus.Msp_1msCtrlVdcLinkFild);

    Msp_1msCtrl_Timer_Elapsed = STM0_TIM0.U - Msp_1msCtrl_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define MSP_1MSCTRL_STOP_SEC_CODE
#include "Msp_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
