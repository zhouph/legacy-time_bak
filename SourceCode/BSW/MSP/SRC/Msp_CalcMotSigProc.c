/**
 * @defgroup Msp Msp
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Msp.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Msp_CalcMotSigProc.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define	MSP_S16_MECH_ANGLE_360_PULSE			(8192)
#define	MSP_S16_MECH_ANGLE_270_PULSE			(6144)
#define	MSP_S16_MECH_ANGLE_90_PULSE				(2048)

#define MSP_S16_ELEC_ANGLE_360_PULSE			(2048)
#define	MSP_S16_ELEC_ANGLE_360_DEG				(3600)
#define	MSP_S16_ELEC_ANGLE_0					(0)
#define	MSP_S16_ELEC_ANGLE_OFFSET_PULSE 		(285)

#define MSP_S16_SMPL_FREQ						(10000)
#define MSP_POSI_2_POSIRAW_SF					(4)

#define MSP_U8_DPOSIRAW_ARRAY_LENGTH			(10)

#define MSP_S32_POSI_MAX						(247720)		/* 1 mechnical rev --> 2048 pulse */
#define MSP_S32_POSI_MIN						(-247720)

/*============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define MSP_START_SEC_CONST_UNSPECIFIED
#include "Msp_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/


/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define MSP_STOP_SEC_CONST_UNSPECIFIED
#include "Msp_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLES DEFINITIONS
 =============================================================================*/
#define MSP_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Msp_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

#define MSP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Msp_MemMap.h"
#define MSP_START_SEC_VAR_NOINIT_32BIT
#include "Msp_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MSP_STOP_SEC_VAR_NOINIT_32BIT
#include "Msp_MemMap.h"
#define MSP_START_SEC_VAR_UNSPECIFIED
#include "Msp_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MSP_STOP_SEC_VAR_UNSPECIFIED
#include "Msp_MemMap.h"
#define MSP_START_SEC_VAR_32BIT
#include "Msp_MemMap.h"
/** Variable Section (32BIT)**/


#define MSP_STOP_SEC_VAR_32BIT
#include "Msp_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLES DEFINITIONS
 =============================================================================*/
#define MSP_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Msp_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define MSP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Msp_MemMap.h"
#define MSP_START_SEC_VAR_NOINIT_32BIT
#include "Msp_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MSP_STOP_SEC_VAR_NOINIT_32BIT
#include "Msp_MemMap.h"
#define MSP_START_SEC_VAR_UNSPECIFIED
#include "Msp_MemMap.h"

/** Variable Section (UNSPECIFIED)**/
uint8 lmspu8WallDetectedFlg;
uint8 lmspu8WallDetectedFlgOld;
uint8 lmspu8MotOrgSetFlgOld;
uint8 lmspu8MotOrgSetFlg;
uint8 lmspu8DPosiRawArrayFullFlg;
uint8 lmspu8MotStkPosnCalcResetFlg;

uint16 lmspu8DPosiRawArrayIdx;

sint16 lmsps16WrpmMech;
sint16 lmsps16WrpsElec;		/* should be added to I/F */
sint16 lmsps16ThetaElec;
sint16 lmsps16Theta;
sint16 lmpss16DeltaPosiMin;
sint16 lmpss16DeltaPosiMax;

sint32 lmsps32DeltaThetaMech;
sint32 lmsps32ThetaMechOld;
sint32 lmsps32ThetaMech;
sint32 lmsps32DPosiRaw;
sint32 lmsps32DPosiRawArray[MSP_U8_DPOSIRAW_ARRAY_LENGTH];
sint32 lmsps32DPosiRawArraySum;
sint32 lmsps32DPosiRawMovAvg;
sint32 lmsps32PosiRaw;
//sint32 lmsps32PosiBfOrgSet;
sint32 lmsps32PosiWall;
sint32 lmsps32Posi;

#define MSP_STOP_SEC_VAR_UNSPECIFIED
#include "Msp_MemMap.h"
#define MSP_START_SEC_VAR_32BIT
#include "Msp_MemMap.h"
/** Variable Section (32BIT)**/


#define MSP_STOP_SEC_VAR_32BIT
#include "Msp_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define MSP_START_SEC_CODE
#include "Msp_MemMap.h"

inline static void Msp_vCalcMtrSpeed(void);
inline static void Msp_vCalcMtrStkPosn(void);
inline static void Msp_vCalcElecAngle(void);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

void Msp_CallMotAngleSigProc(void)
{
	lmsps32ThetaMechOld = lmsps32ThetaMech;
	lmsps32ThetaMech = (sint16)Msp_CtrlBus.Msp_CtrlMotAngle1Info.MotMechAngle1;
	lmsps16Theta = (sint16)Msp_CtrlBus.Msp_CtrlMotAngle1Info.MotElecAngle1;
	lmspu8MotOrgSetFlgOld = lmspu8MotOrgSetFlg;
	lmspu8MotOrgSetFlg = Msp_CtrlBus.Msp_CtrlMotOrgSetStInfo.MotOrgSet;
//	lmsps32PosiBfOrgSet = Msp_CtrlBus.Msp_CtrlMotRotgAgSigBfMotOrgSetInfo.StkPosnMeasdBfMotOrgSet;
	lmsps32PosiWall = Msp_CtrlBus.Msp_CtrlPwrPistStBfMotOrgSetInfo.PosiWallTemp;
	lmspu8WallDetectedFlgOld = lmspu8WallDetectedFlg;
	lmspu8WallDetectedFlg = (uint8)Msp_CtrlBus.Msp_CtrlPwrPistStBfMotOrgSetInfo.WallDetectedFlg;

/*	if ((lmspu8MotOrgSetFlgOld == 0) && (lmspu8MotOrgSetFlg == 1))
	{
		lmspu8MotStkPosnCalcResetFlg = 1;
	}
	else
	{
		lmspu8MotStkPosnCalcResetFlg = 0;
	}*/

	Msp_vCalcMtrStkPosn();
	Msp_vCalcMtrSpeed();
	Msp_vCalcElecAngle();

	Msp_CtrlBus.Msp_CtrlMotRotgAgSigInfo.StkPosnMeasd = lmsps32Posi;
	Msp_CtrlBus.Msp_CtrlMotRotgAgSigInfo.MotElecAngleFild = lmsps16ThetaElec;
	Msp_CtrlBus.Msp_CtrlMotRotgAgSigInfo.MotMechAngleSpdFild = lmsps16WrpmMech;
}


/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

inline static void Msp_vCalcMtrStkPosn(void)
{
	static uint16 u8ArrayIdx;
	static sint16 s16Temp0;

	/* calc delta position */
	lmsps32DeltaThetaMech = lmsps32ThetaMech - lmsps32ThetaMechOld;
	if ((lmsps32ThetaMech < MSP_S16_MECH_ANGLE_90_PULSE) && (lmsps32ThetaMechOld > MSP_S16_MECH_ANGLE_270_PULSE))
	{
		lmsps32DeltaThetaMech = lmsps32DeltaThetaMech + MSP_S16_MECH_ANGLE_360_PULSE;
	}
	else if ((lmsps32ThetaMech > MSP_S16_MECH_ANGLE_270_PULSE) && (lmsps32ThetaMechOld < MSP_S16_MECH_ANGLE_90_PULSE))
	{
		lmsps32DeltaThetaMech = lmsps32DeltaThetaMech - MSP_S16_MECH_ANGLE_360_PULSE;
	}
	else
	{
		;
	}

	lmsps32DPosiRaw = -lmsps32DeltaThetaMech;
	/* calc delta position */

	/* calc sum of delta position array */
	if(lmspu8DPosiRawArrayIdx >= MSP_U8_DPOSIRAW_ARRAY_LENGTH)
	{
		lmspu8DPosiRawArrayIdx = 0;
	}

	if (lmspu8DPosiRawArrayFullFlg == 0)
	{
		lmsps32DPosiRawArray[lmspu8DPosiRawArrayIdx] = lmsps32DPosiRaw;
		lmsps32DPosiRawArraySum = lmsps32DPosiRawArraySum + lmsps32DPosiRaw;
		lmspu8DPosiRawArrayIdx++;
		if (lmspu8DPosiRawArrayIdx > 0)
		{
			lmsps32DPosiRawMovAvg = lmsps32DPosiRawArraySum/lmspu8DPosiRawArrayIdx;
		}
		else
		{
			lmsps32DPosiRawMovAvg = 0;
		}

		if (lmspu8DPosiRawArrayIdx >= MSP_U8_DPOSIRAW_ARRAY_LENGTH)
		{
			lmspu8DPosiRawArrayFullFlg = 1;
			lmspu8DPosiRawArrayIdx = 0;
		}
	}
	else
	{
		lmsps32DPosiRawArray[lmspu8DPosiRawArrayIdx] = lmsps32DPosiRaw;

		lmpss16DeltaPosiMin = lmsps32DPosiRawArray[0];
		lmpss16DeltaPosiMax = lmsps32DPosiRawArray[0];

		/* calc sum of delta position array */
		lmsps32DPosiRawArraySum = 0;
		for (u8ArrayIdx = 0; u8ArrayIdx < MSP_U8_DPOSIRAW_ARRAY_LENGTH; u8ArrayIdx++)
		{
			if (lmsps32DPosiRawArray[u8ArrayIdx] < lmpss16DeltaPosiMin)
			{
				lmpss16DeltaPosiMin = lmsps32DPosiRawArray[u8ArrayIdx];
			}
			else if (lmsps32DPosiRawArray[u8ArrayIdx] > lmpss16DeltaPosiMax)
			{
				lmpss16DeltaPosiMax = lmsps32DPosiRawArray[u8ArrayIdx];
			}
			else
			{
				;
			}

			lmsps32DPosiRawArraySum = lmsps32DPosiRawArraySum + lmsps32DPosiRawArray[u8ArrayIdx];
		}
		lmsps32DPosiRawArraySum = lmsps32DPosiRawArraySum - lmpss16DeltaPosiMin - lmpss16DeltaPosiMax;
		/* calc sum of delta position array */

		if (MSP_U8_DPOSIRAW_ARRAY_LENGTH > 2)
		{
			lmsps32DPosiRawMovAvg = lmsps32DPosiRawArraySum/(MSP_U8_DPOSIRAW_ARRAY_LENGTH - 2);
		}
		else
		{
			lmsps32DPosiRawMovAvg = 0;
		}

		lmspu8DPosiRawArrayIdx++;
	}
	/* calc sum of delta position array */

	/* calc position */
	if ((lmspu8WallDetectedFlgOld == 0) && (lmspu8WallDetectedFlg == 1))
	{
		lmsps32PosiRaw = lmsps32PosiWall * MSP_POSI_2_POSIRAW_SF;
		lmsps32DPosiRawArray[0] = lmsps32PosiRaw;
		for(u8ArrayIdx = 1; u8ArrayIdx < MSP_U8_DPOSIRAW_ARRAY_LENGTH; u8ArrayIdx++)
		{
			lmsps32DPosiRawArray[u8ArrayIdx] = 0;
		}

		lmspu8DPosiRawArrayFullFlg = 0;
		lmspu8DPosiRawArrayIdx = 1;
		lmsps32DPosiRawArraySum = lmsps32DPosiRawArray[0];
		lmsps32DPosiRawMovAvg = 0;

		lmsps32DPosiRaw = 0;
	}

	if ((lmspu8MotOrgSetFlgOld == 0) && (lmspu8MotOrgSetFlg == 1))
	{
		lmsps32PosiRaw = 0;
		lmsps32DPosiRawArray[0] = lmsps32PosiRaw;
		for(u8ArrayIdx = 1; u8ArrayIdx < MSP_U8_DPOSIRAW_ARRAY_LENGTH; u8ArrayIdx++)
		{
			lmsps32DPosiRawArray[u8ArrayIdx] = 0;
		}

		lmspu8DPosiRawArrayFullFlg = 0;
		lmspu8DPosiRawArrayIdx = 1;
		lmsps32DPosiRawArraySum = lmsps32DPosiRawArray[0];
		lmsps32DPosiRawMovAvg = 0;

		lmsps32DPosiRaw = 0;
	}

	lmsps32PosiRaw = lmsps32PosiRaw + lmsps32DPosiRaw;
	if (lmsps32PosiRaw > MSP_S32_POSI_MAX)
	{
		lmsps32PosiRaw = MSP_S32_POSI_MAX;
	}
	else if (lmsps32PosiRaw < MSP_S32_POSI_MIN)
	{
		lmsps32PosiRaw = MSP_S32_POSI_MIN;
	}
	else
	{
		;
	}

	lmsps32Posi = lmsps32PosiRaw/MSP_POSI_2_POSIRAW_SF; /* for scaling 8196 --> 2048 per 1rev */
	/* calc position */
}


inline static void Msp_vCalcMtrSpeed(void)
{
	lmsps16WrpmMech = lmsps32DPosiRawMovAvg * 60 * MSP_S16_SMPL_FREQ / MSP_S16_MECH_ANGLE_360_PULSE;
}

inline static void Msp_vCalcElecAngle(void)
{
	lmsps16ThetaElec =(sint16)((sint32)(lmsps16Theta - MSP_S16_ELEC_ANGLE_OFFSET_PULSE) *MSP_S16_ELEC_ANGLE_360_DEG/MSP_S16_ELEC_ANGLE_360_PULSE);

	if (lmsps16ThetaElec < MSP_S16_ELEC_ANGLE_0)
	{
		lmsps16ThetaElec = lmsps16ThetaElec + MSP_S16_ELEC_ANGLE_360_DEG;
	}
	else if (lmsps16ThetaElec > MSP_S16_ELEC_ANGLE_360_DEG)
	{
		lmsps16ThetaElec = lmsps16ThetaElec - MSP_S16_ELEC_ANGLE_360_DEG;
	}
	else
	{
		;
	}
}

#define MSP_STOP_SEC_CODE
#include "Msp_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
