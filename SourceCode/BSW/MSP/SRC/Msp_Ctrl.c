/**
 * @defgroup Msp_Ctrl Msp_Ctrl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Msp_Ctrl.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Msp_Ctrl.h"
#include "Msp_Ctrl_Ifa.h"
#include "IfxStm_reg.h"
#include "Msp_CalcMotSigProc.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define MSP_CTRL_START_SEC_CONST_UNSPECIFIED
#include "Msp_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define MSP_CTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Msp_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MSP_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Msp_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Msp_Ctrl_HdrBusType Msp_CtrlBus;

/* Version Info */
const SwcVersionInfo_t Msp_CtrlVersionInfo = 
{   
    MSP_CTRL_MODULE_ID,           /* Msp_CtrlVersionInfo.ModuleId */
    MSP_CTRL_MAJOR_VERSION,       /* Msp_CtrlVersionInfo.MajorVer */
    MSP_CTRL_MINOR_VERSION,       /* Msp_CtrlVersionInfo.MinorVer */
    MSP_CTRL_PATCH_VERSION,       /* Msp_CtrlVersionInfo.PatchVer */
    MSP_CTRL_BRANCH_VERSION       /* Msp_CtrlVersionInfo.BranchVer */
};
    
/* Input Data Element */
Acmio_SenMotAngle1Info_t Msp_CtrlMotAngle1Info;
Ses_CtrlMotOrgSetStInfo_t Msp_CtrlMotOrgSetStInfo;
Ses_CtrlPwrPistStBfMotOrgSetInfo_t Msp_CtrlPwrPistStBfMotOrgSetInfo;
Mom_HndlrEcuModeSts_t Msp_CtrlEcuModeSts;
Eem_SuspcDetnFuncInhibitMspSts_t Msp_CtrlFuncInhibitMspSts;

/* Output Data Element */
Msp_CtrlMotRotgAgSigInfo_t Msp_CtrlMotRotgAgSigInfo;
Msp_CtrlMotRotgAgSigBfMotOrgSetInfo_t Msp_CtrlMotRotgAgSigBfMotOrgSetInfo;

uint32 Msp_Ctrl_Timer_Start;
uint32 Msp_Ctrl_Timer_Elapsed;

#define MSP_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Msp_MemMap.h"
#define MSP_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Msp_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MSP_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Msp_MemMap.h"
#define MSP_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Msp_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MSP_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Msp_MemMap.h"
#define MSP_CTRL_START_SEC_VAR_32BIT
#include "Msp_MemMap.h"
/** Variable Section (32BIT)**/


#define MSP_CTRL_STOP_SEC_VAR_32BIT
#include "Msp_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MSP_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Msp_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define MSP_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Msp_MemMap.h"
#define MSP_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Msp_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MSP_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Msp_MemMap.h"
#define MSP_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Msp_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MSP_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Msp_MemMap.h"
#define MSP_CTRL_START_SEC_VAR_32BIT
#include "Msp_MemMap.h"
/** Variable Section (32BIT)**/


#define MSP_CTRL_STOP_SEC_VAR_32BIT
#include "Msp_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define MSP_CTRL_START_SEC_CODE
#include "Msp_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Msp_Ctrl_Init(void)
{
    /* Initialize internal bus */
    Msp_CtrlBus.Msp_CtrlMotAngle1Info.MotElecAngle1 = 0;
    Msp_CtrlBus.Msp_CtrlMotAngle1Info.MotMechAngle1 = 0;
    Msp_CtrlBus.Msp_CtrlMotOrgSetStInfo.MotOrgSet = 0;
    Msp_CtrlBus.Msp_CtrlPwrPistStBfMotOrgSetInfo.PosiWallTemp = 0;
    Msp_CtrlBus.Msp_CtrlPwrPistStBfMotOrgSetInfo.WallDetectedFlg = 0;
    Msp_CtrlBus.Msp_CtrlEcuModeSts = 0;
    Msp_CtrlBus.Msp_CtrlFuncInhibitMspSts = 0;
    Msp_CtrlBus.Msp_CtrlMotRotgAgSigInfo.StkPosnMeasd = 0;
    Msp_CtrlBus.Msp_CtrlMotRotgAgSigInfo.MotElecAngleFild = 0;
    Msp_CtrlBus.Msp_CtrlMotRotgAgSigInfo.MotMechAngleSpdFild = 0;
    Msp_CtrlBus.Msp_CtrlMotRotgAgSigBfMotOrgSetInfo.StkPosnMeasdBfMotOrgSet = 0;
    Msp_CtrlBus.Msp_CtrlMotRotgAgSigBfMotOrgSetInfo.MotMechAngleSpdBfMotOrgSet = 0;
}

void Msp_Ctrl(void)
{
    Msp_Ctrl_Timer_Start = STM0_TIM0.U;

    /* Input */
    Msp_Ctrl_Read_Msp_CtrlMotAngle1Info(&Msp_CtrlBus.Msp_CtrlMotAngle1Info);
    /*==============================================================================
    * Members of structure Msp_CtrlMotAngle1Info 
     : Msp_CtrlMotAngle1Info.MotElecAngle1;
     : Msp_CtrlMotAngle1Info.MotMechAngle1;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Msp_Ctrl_Read_Msp_CtrlMotOrgSetStInfo_MotOrgSet(&Msp_CtrlBus.Msp_CtrlMotOrgSetStInfo.MotOrgSet);

    Msp_Ctrl_Read_Msp_CtrlPwrPistStBfMotOrgSetInfo(&Msp_CtrlBus.Msp_CtrlPwrPistStBfMotOrgSetInfo);
    /*==============================================================================
    * Members of structure Msp_CtrlPwrPistStBfMotOrgSetInfo 
     : Msp_CtrlPwrPistStBfMotOrgSetInfo.PosiWallTemp;
     : Msp_CtrlPwrPistStBfMotOrgSetInfo.WallDetectedFlg;
     =============================================================================*/
    
    Msp_Ctrl_Read_Msp_CtrlEcuModeSts(&Msp_CtrlBus.Msp_CtrlEcuModeSts);
    Msp_Ctrl_Read_Msp_CtrlFuncInhibitMspSts(&Msp_CtrlBus.Msp_CtrlFuncInhibitMspSts);

    /* Process */
    Msp_CallMotAngleSigProc();

    /* Output */
    Msp_Ctrl_Write_Msp_CtrlMotRotgAgSigInfo(&Msp_CtrlBus.Msp_CtrlMotRotgAgSigInfo);
    /*==============================================================================
    * Members of structure Msp_CtrlMotRotgAgSigInfo 
     : Msp_CtrlMotRotgAgSigInfo.StkPosnMeasd;
     : Msp_CtrlMotRotgAgSigInfo.MotElecAngleFild;
     : Msp_CtrlMotRotgAgSigInfo.MotMechAngleSpdFild;
     =============================================================================*/
    
    Msp_Ctrl_Write_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo(&Msp_CtrlBus.Msp_CtrlMotRotgAgSigBfMotOrgSetInfo);
    /*==============================================================================
    * Members of structure Msp_CtrlMotRotgAgSigBfMotOrgSetInfo 
     : Msp_CtrlMotRotgAgSigBfMotOrgSetInfo.StkPosnMeasdBfMotOrgSet;
     : Msp_CtrlMotRotgAgSigBfMotOrgSetInfo.MotMechAngleSpdBfMotOrgSet;
     =============================================================================*/
    

    Msp_Ctrl_Timer_Elapsed = STM0_TIM0.U - Msp_Ctrl_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define MSP_CTRL_STOP_SEC_CODE
#include "Msp_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
