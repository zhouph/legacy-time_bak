/* Virtual main for linking */
/* purpose : Generation of map file */
#include "Msp_Ctrl.h"
#include "Msp_1msCtrl.h"

int main(void)
{
    Msp_Ctrl_Init();
    Msp_1msCtrl_Init();

    while(1)
    {
        Msp_Ctrl();
        Msp_1msCtrl();
    }
}