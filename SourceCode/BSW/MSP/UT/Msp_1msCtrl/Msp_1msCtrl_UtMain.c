#include "unity.h"
#include "unity_fixture.h"
#include "Msp_1msCtrl.h"
#include "Msp_1msCtrl_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Acmio_SenVdcLink_t UtInput_Msp_1msCtrlVdcLink[MAX_STEP] = MSP_1MSCTRLVDCLINK;

const Msp_1msCtrlVdcLinkFild_t UtExpected_Msp_1msCtrlVdcLinkFild[MAX_STEP] = MSP_1MSCTRLVDCLINKFILD;



TEST_GROUP(Msp_1msCtrl);
TEST_SETUP(Msp_1msCtrl)
{
    Msp_1msCtrl_Init();
}

TEST_TEAR_DOWN(Msp_1msCtrl)
{   /* Postcondition */

}

TEST(Msp_1msCtrl, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Msp_1msCtrlVdcLink = UtInput_Msp_1msCtrlVdcLink[i];

        Msp_1msCtrl();

        TEST_ASSERT_EQUAL(Msp_1msCtrlVdcLinkFild, UtExpected_Msp_1msCtrlVdcLinkFild[i]);
    }
}

TEST_GROUP_RUNNER(Msp_1msCtrl)
{
    RUN_TEST_CASE(Msp_1msCtrl, All);
}
