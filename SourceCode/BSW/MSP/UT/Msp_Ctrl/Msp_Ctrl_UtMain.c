#include "unity.h"
#include "unity_fixture.h"
#include "Msp_Ctrl.h"
#include "Msp_Ctrl_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Saluint16 UtInput_Msp_CtrlMotAngle1Info_MotElecAngle1[MAX_STEP] = MSP_CTRLMOTANGLE1INFO_MOTELECANGLE1;
const Saluint16 UtInput_Msp_CtrlMotAngle1Info_MotMechAngle1[MAX_STEP] = MSP_CTRLMOTANGLE1INFO_MOTMECHANGLE1;
const Saluint8 UtInput_Msp_CtrlMotOrgSetStInfo_MotOrgSet[MAX_STEP] = MSP_CTRLMOTORGSETSTINFO_MOTORGSET;
const Salsint32 UtInput_Msp_CtrlPwrPistStBfMotOrgSetInfo_PosiWallTemp[MAX_STEP] = MSP_CTRLPWRPISTSTBFMOTORGSETINFO_POSIWALLTEMP;
const Salsint32 UtInput_Msp_CtrlPwrPistStBfMotOrgSetInfo_WallDetectedFlg[MAX_STEP] = MSP_CTRLPWRPISTSTBFMOTORGSETINFO_WALLDETECTEDFLG;
const Mom_HndlrEcuModeSts_t UtInput_Msp_CtrlEcuModeSts[MAX_STEP] = MSP_CTRLECUMODESTS;
const Eem_SuspcDetnFuncInhibitMspSts_t UtInput_Msp_CtrlFuncInhibitMspSts[MAX_STEP] = MSP_CTRLFUNCINHIBITMSPSTS;

const Salsint32 UtExpected_Msp_CtrlMotRotgAgSigInfo_StkPosnMeasd[MAX_STEP] = MSP_CTRLMOTROTGAGSIGINFO_STKPOSNMEASD;
const Salsint32 UtExpected_Msp_CtrlMotRotgAgSigInfo_MotElecAngleFild[MAX_STEP] = MSP_CTRLMOTROTGAGSIGINFO_MOTELECANGLEFILD;
const Salsint32 UtExpected_Msp_CtrlMotRotgAgSigInfo_MotMechAngleSpdFild[MAX_STEP] = MSP_CTRLMOTROTGAGSIGINFO_MOTMECHANGLESPDFILD;
const Salsint32 UtExpected_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo_StkPosnMeasdBfMotOrgSet[MAX_STEP] = MSP_CTRLMOTROTGAGSIGBFMOTORGSETINFO_STKPOSNMEASDBFMOTORGSET;
const Salsint32 UtExpected_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo_MotMechAngleSpdBfMotOrgSet[MAX_STEP] = MSP_CTRLMOTROTGAGSIGBFMOTORGSETINFO_MOTMECHANGLESPDBFMOTORGSET;



TEST_GROUP(Msp_Ctrl);
TEST_SETUP(Msp_Ctrl)
{
    Msp_Ctrl_Init();
}

TEST_TEAR_DOWN(Msp_Ctrl)
{   /* Postcondition */

}

TEST(Msp_Ctrl, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Msp_CtrlMotAngle1Info.MotElecAngle1 = UtInput_Msp_CtrlMotAngle1Info_MotElecAngle1[i];
        Msp_CtrlMotAngle1Info.MotMechAngle1 = UtInput_Msp_CtrlMotAngle1Info_MotMechAngle1[i];
        Msp_CtrlMotOrgSetStInfo.MotOrgSet = UtInput_Msp_CtrlMotOrgSetStInfo_MotOrgSet[i];
        Msp_CtrlPwrPistStBfMotOrgSetInfo.PosiWallTemp = UtInput_Msp_CtrlPwrPistStBfMotOrgSetInfo_PosiWallTemp[i];
        Msp_CtrlPwrPistStBfMotOrgSetInfo.WallDetectedFlg = UtInput_Msp_CtrlPwrPistStBfMotOrgSetInfo_WallDetectedFlg[i];
        Msp_CtrlEcuModeSts = UtInput_Msp_CtrlEcuModeSts[i];
        Msp_CtrlFuncInhibitMspSts = UtInput_Msp_CtrlFuncInhibitMspSts[i];

        Msp_Ctrl();

        TEST_ASSERT_EQUAL(Msp_CtrlMotRotgAgSigInfo.StkPosnMeasd, UtExpected_Msp_CtrlMotRotgAgSigInfo_StkPosnMeasd[i]);
        TEST_ASSERT_EQUAL(Msp_CtrlMotRotgAgSigInfo.MotElecAngleFild, UtExpected_Msp_CtrlMotRotgAgSigInfo_MotElecAngleFild[i]);
        TEST_ASSERT_EQUAL(Msp_CtrlMotRotgAgSigInfo.MotMechAngleSpdFild, UtExpected_Msp_CtrlMotRotgAgSigInfo_MotMechAngleSpdFild[i]);
        TEST_ASSERT_EQUAL(Msp_CtrlMotRotgAgSigBfMotOrgSetInfo.StkPosnMeasdBfMotOrgSet, UtExpected_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo_StkPosnMeasdBfMotOrgSet[i]);
        TEST_ASSERT_EQUAL(Msp_CtrlMotRotgAgSigBfMotOrgSetInfo.MotMechAngleSpdBfMotOrgSet, UtExpected_Msp_CtrlMotRotgAgSigBfMotOrgSetInfo_MotMechAngleSpdBfMotOrgSet[i]);
    }
}

TEST_GROUP_RUNNER(Msp_Ctrl)
{
    RUN_TEST_CASE(Msp_Ctrl, All);
}
