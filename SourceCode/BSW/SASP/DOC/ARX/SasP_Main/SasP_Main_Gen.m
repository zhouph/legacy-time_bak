SasP_MainEemFailData = Simulink.Bus;
DeList={
    'Eem_Fail_SenPwr_12V'
    'Eem_Fail_Yaw'
    'Eem_Fail_Ay'
    'Eem_Fail_Str'
    'Eem_Fail_WssFL'
    'Eem_Fail_WssFR'
    'Eem_Fail_WssRL'
    'Eem_Fail_WssRR'
    };
SasP_MainEemFailData = CreateBus(SasP_MainEemFailData, DeList);
clear DeList;

SasP_MainCanRxEscInfo = Simulink.Bus;
DeList={
    'SteeringAngle'
    };
SasP_MainCanRxEscInfo = CreateBus(SasP_MainCanRxEscInfo, DeList);
clear DeList;

SasP_MainEscSwtStInfo = Simulink.Bus;
DeList={
    'EscDisabledBySwt'
    };
SasP_MainEscSwtStInfo = CreateBus(SasP_MainEscSwtStInfo, DeList);
clear DeList;

SasP_MainWhlSpdInfo = Simulink.Bus;
DeList={
    'FlWhlSpd'
    'FrWhlSpd'
    'RlWhlSpd'
    'RrlWhlSpd'
    };
SasP_MainWhlSpdInfo = CreateBus(SasP_MainWhlSpdInfo, DeList);
clear DeList;

SasP_MainAbsCtrlInfo = Simulink.Bus;
DeList={
    'AbsActFlg'
    };
SasP_MainAbsCtrlInfo = CreateBus(SasP_MainAbsCtrlInfo, DeList);
clear DeList;

SasP_MainEscCtrlInfo = Simulink.Bus;
DeList={
    'EscActFlg'
    };
SasP_MainEscCtrlInfo = CreateBus(SasP_MainEscCtrlInfo, DeList);
clear DeList;

SasP_MainTcsCtrlInfo = Simulink.Bus;
DeList={
    'TcsActFlg'
    };
SasP_MainTcsCtrlInfo = CreateBus(SasP_MainTcsCtrlInfo, DeList);
clear DeList;

SasP_MainSenPwrMonitorData = Simulink.Bus;
DeList={
    'SenPwrM_5V_Stable'
    'SenPwrM_12V_Stable'
    };
SasP_MainSenPwrMonitorData = CreateBus(SasP_MainSenPwrMonitorData, DeList);
clear DeList;

SasP_MainEemSuspectData = Simulink.Bus;
DeList={
    'Eem_Suspect_WssFL'
    'Eem_Suspect_WssFR'
    'Eem_Suspect_WssRL'
    'Eem_Suspect_WssRR'
    'Eem_Suspect_FrontWss'
    'Eem_Suspect_RearWss'
    };
SasP_MainEemSuspectData = CreateBus(SasP_MainEemSuspectData, DeList);
clear DeList;

SasP_MainCanMonData = Simulink.Bus;
DeList={
    'CanM_MainBusOff_Err'
    };
SasP_MainCanMonData = CreateBus(SasP_MainCanMonData, DeList);
clear DeList;

SasP_MainWssSpeedOut = Simulink.Bus;
DeList={
    'WssMax'
    'WssMin'
    };
SasP_MainWssSpeedOut = CreateBus(SasP_MainWssSpeedOut, DeList);
clear DeList;

SasP_MainEcuModeSts = Simulink.Bus;
DeList={'SasP_MainEcuModeSts'};
SasP_MainEcuModeSts = CreateBus(SasP_MainEcuModeSts, DeList);
clear DeList;

SasP_MainIgnOnOffSts = Simulink.Bus;
DeList={'SasP_MainIgnOnOffSts'};
SasP_MainIgnOnOffSts = CreateBus(SasP_MainIgnOnOffSts, DeList);
clear DeList;

SasP_MainIgnEdgeSts = Simulink.Bus;
DeList={'SasP_MainIgnEdgeSts'};
SasP_MainIgnEdgeSts = CreateBus(SasP_MainIgnEdgeSts, DeList);
clear DeList;

SasP_MainVBatt1Mon = Simulink.Bus;
DeList={'SasP_MainVBatt1Mon'};
SasP_MainVBatt1Mon = CreateBus(SasP_MainVBatt1Mon, DeList);
clear DeList;

SasP_MainDiagClrSrs = Simulink.Bus;
DeList={'SasP_MainDiagClrSrs'};
SasP_MainDiagClrSrs = CreateBus(SasP_MainDiagClrSrs, DeList);
clear DeList;

SasP_MainVehSpd = Simulink.Bus;
DeList={'SasP_MainVehSpd'};
SasP_MainVehSpd = CreateBus(SasP_MainVehSpd, DeList);
clear DeList;

SasP_MainSasPlauOutput = Simulink.Bus;
DeList={
    'SasPlauModelErr'
    'SasPlauOffsetErr'
    'SasPlauStickErr'
    };
SasP_MainSasPlauOutput = CreateBus(SasP_MainSasPlauOutput, DeList);
clear DeList;

