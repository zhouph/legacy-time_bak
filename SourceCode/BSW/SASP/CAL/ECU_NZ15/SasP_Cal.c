/**
 * @defgroup SasP_Cal SasP_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SasP_Cal.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "SasP_Cal.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SASP_START_SEC_CALIB_UNSPECIFIED
#include "SasP_MemMap.h"
/* Global Calibration Section */


#define SASP_STOP_SEC_CALIB_UNSPECIFIED
#include "SasP_MemMap.h"

#define SASP_START_SEC_CONST_UNSPECIFIED
#include "SasP_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define SASP_STOP_SEC_CONST_UNSPECIFIED
#include "SasP_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SASP_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SasP_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SASP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SasP_MemMap.h"
#define SASP_START_SEC_VAR_NOINIT_32BIT
#include "SasP_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SASP_STOP_SEC_VAR_NOINIT_32BIT
#include "SasP_MemMap.h"
#define SASP_START_SEC_VAR_UNSPECIFIED
#include "SasP_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SASP_STOP_SEC_VAR_UNSPECIFIED
#include "SasP_MemMap.h"
#define SASP_START_SEC_VAR_32BIT
#include "SasP_MemMap.h"
/** Variable Section (32BIT)**/


#define SASP_STOP_SEC_VAR_32BIT
#include "SasP_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SASP_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SasP_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SASP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SasP_MemMap.h"
#define SASP_START_SEC_VAR_NOINIT_32BIT
#include "SasP_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SASP_STOP_SEC_VAR_NOINIT_32BIT
#include "SasP_MemMap.h"
#define SASP_START_SEC_VAR_UNSPECIFIED
#include "SasP_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SASP_STOP_SEC_VAR_UNSPECIFIED
#include "SasP_MemMap.h"
#define SASP_START_SEC_VAR_32BIT
#include "SasP_MemMap.h"
/** Variable Section (32BIT)**/


#define SASP_STOP_SEC_VAR_32BIT
#include "SasP_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SASP_START_SEC_CODE
#include "SasP_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define SASP_STOP_SEC_CODE
#include "SasP_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
