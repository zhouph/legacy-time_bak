# \file
#
# \brief SasP
#
# This file contains the implementation of the SWC
# module SasP.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

SasP_CORE_PATH     := $(MANDO_BSW_ROOT)\SasP
SasP_CAL_PATH      := $(SasP_CORE_PATH)\CAL\$(SasP_VARIANT)
SasP_SRC_PATH      := $(SasP_CORE_PATH)\SRC
SasP_CFG_PATH      := $(SasP_CORE_PATH)\CFG\$(SasP_VARIANT)
SasP_HDR_PATH      := $(SasP_CORE_PATH)\HDR
SasP_IFA_PATH      := $(SasP_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    SasP_CMN_PATH      := $(SasP_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	SasP_UT_PATH		:= $(SasP_CORE_PATH)\UT
	SasP_UNITY_PATH	:= $(SasP_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(SasP_UT_PATH)
	CC_INCLUDE_PATH		+= $(SasP_UNITY_PATH)
	SasP_Main_PATH 	:= SasP_UT_PATH\SasP_Main
endif
CC_INCLUDE_PATH    += $(SasP_CAL_PATH)
CC_INCLUDE_PATH    += $(SasP_SRC_PATH)
CC_INCLUDE_PATH    += $(SasP_CFG_PATH)
CC_INCLUDE_PATH    += $(SasP_HDR_PATH)
CC_INCLUDE_PATH    += $(SasP_IFA_PATH)
CC_INCLUDE_PATH    += $(SasP_CMN_PATH)

