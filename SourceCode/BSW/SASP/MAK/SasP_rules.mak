# \file
#
# \brief SasP
#
# This file contains the implementation of the SWC
# module SasP.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += SasP_src

SasP_src_FILES        += $(SasP_SRC_PATH)\SasP_Main.c
SasP_src_FILES        += $(SasP_SRC_PATH)\SasP_Main_Proc.c
SasP_src_FILES        += $(SasP_IFA_PATH)\SasP_Main_Ifa.c
SasP_src_FILES        += $(SasP_CFG_PATH)\SasP_Cfg.c
SasP_src_FILES        += $(SasP_CAL_PATH)\SasP_Cal.c

ifeq ($(ICE_COMPILE),true)
SasP_src_FILES        += $(SasP_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	SasP_src_FILES        += $(SasP_UNITY_PATH)\unity.c
	SasP_src_FILES        += $(SasP_UNITY_PATH)\unity_fixture.c	
	SasP_src_FILES        += $(SasP_UT_PATH)\main.c
	SasP_src_FILES        += $(SasP_UT_PATH)\SasP_Main\SasP_Main_UtMain.c
endif