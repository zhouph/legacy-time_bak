#define S_FUNCTION_NAME      SasP_Main_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          34
#define WidthOutputPort         3

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "SasP_Main.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    SasP_MainEemFailData.Eem_Fail_SenPwr_12V = input[0];
    SasP_MainEemFailData.Eem_Fail_Yaw = input[1];
    SasP_MainEemFailData.Eem_Fail_Ay = input[2];
    SasP_MainEemFailData.Eem_Fail_Str = input[3];
    SasP_MainEemFailData.Eem_Fail_WssFL = input[4];
    SasP_MainEemFailData.Eem_Fail_WssFR = input[5];
    SasP_MainEemFailData.Eem_Fail_WssRL = input[6];
    SasP_MainEemFailData.Eem_Fail_WssRR = input[7];
    SasP_MainCanRxEscInfo.SteeringAngle = input[8];
    SasP_MainEscSwtStInfo.EscDisabledBySwt = input[9];
    SasP_MainWhlSpdInfo.FlWhlSpd = input[10];
    SasP_MainWhlSpdInfo.FrWhlSpd = input[11];
    SasP_MainWhlSpdInfo.RlWhlSpd = input[12];
    SasP_MainWhlSpdInfo.RrlWhlSpd = input[13];
    SasP_MainAbsCtrlInfo.AbsActFlg = input[14];
    SasP_MainEscCtrlInfo.EscActFlg = input[15];
    SasP_MainTcsCtrlInfo.TcsActFlg = input[16];
    SasP_MainSenPwrMonitorData.SenPwrM_5V_Stable = input[17];
    SasP_MainSenPwrMonitorData.SenPwrM_12V_Stable = input[18];
    SasP_MainEemSuspectData.Eem_Suspect_WssFL = input[19];
    SasP_MainEemSuspectData.Eem_Suspect_WssFR = input[20];
    SasP_MainEemSuspectData.Eem_Suspect_WssRL = input[21];
    SasP_MainEemSuspectData.Eem_Suspect_WssRR = input[22];
    SasP_MainEemSuspectData.Eem_Suspect_FrontWss = input[23];
    SasP_MainEemSuspectData.Eem_Suspect_RearWss = input[24];
    SasP_MainCanMonData.CanM_MainBusOff_Err = input[25];
    SasP_MainWssSpeedOut.WssMax = input[26];
    SasP_MainWssSpeedOut.WssMin = input[27];
    SasP_MainEcuModeSts = input[28];
    SasP_MainIgnOnOffSts = input[29];
    SasP_MainIgnEdgeSts = input[30];
    SasP_MainVBatt1Mon = input[31];
    SasP_MainDiagClrSrs = input[32];
    SasP_MainVehSpd = input[33];

    SasP_Main();


    output[0] = SasP_MainSasPlauOutput.SasPlauModelErr;
    output[1] = SasP_MainSasPlauOutput.SasPlauOffsetErr;
    output[2] = SasP_MainSasPlauOutput.SasPlauStickErr;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    SasP_Main_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
