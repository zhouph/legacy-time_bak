#define UT_MAX_STEP 5

#define SASP_MAINEEMFAILDATA_EEM_FAIL_SENPWR_12V {0,0,0,0,0}
#define SASP_MAINEEMFAILDATA_EEM_FAIL_YAW {0,0,0,0,0}
#define SASP_MAINEEMFAILDATA_EEM_FAIL_AY {0,0,0,0,0}
#define SASP_MAINEEMFAILDATA_EEM_FAIL_STR {0,0,0,0,0}
#define SASP_MAINEEMFAILDATA_EEM_FAIL_WSSFL {0,0,0,0,0}
#define SASP_MAINEEMFAILDATA_EEM_FAIL_WSSFR {0,0,0,0,0}
#define SASP_MAINEEMFAILDATA_EEM_FAIL_WSSRL {0,0,0,0,0}
#define SASP_MAINEEMFAILDATA_EEM_FAIL_WSSRR {0,0,0,0,0}
#define SASP_MAINCANRXESCINFO_STEERINGANGLE {0,0,0,0,0}
#define SASP_MAINESCSWTSTINFO_ESCDISABLEDBYSWT {0,0,0,0,0}
#define SASP_MAINWHLSPDINFO_FLWHLSPD {0,0,0,0,0}
#define SASP_MAINWHLSPDINFO_FRWHLSPD {0,0,0,0,0}
#define SASP_MAINWHLSPDINFO_RLWHLSPD {0,0,0,0,0}
#define SASP_MAINWHLSPDINFO_RRLWHLSPD {0,0,0,0,0}
#define SASP_MAINABSCTRLINFO_ABSACTFLG {0,0,0,0,0}
#define SASP_MAINESCCTRLINFO_ESCACTFLG {0,0,0,0,0}
#define SASP_MAINTCSCTRLINFO_TCSACTFLG {0,0,0,0,0}
#define SASP_MAINSENPWRMONITORDATA_SENPWRM_5V_STABLE {0,0,0,0,0}
#define SASP_MAINSENPWRMONITORDATA_SENPWRM_12V_STABLE {0,0,0,0,0}
#define SASP_MAINEEMSUSPECTDATA_EEM_SUSPECT_WSSFL {0,0,0,0,0}
#define SASP_MAINEEMSUSPECTDATA_EEM_SUSPECT_WSSFR {0,0,0,0,0}
#define SASP_MAINEEMSUSPECTDATA_EEM_SUSPECT_WSSRL {0,0,0,0,0}
#define SASP_MAINEEMSUSPECTDATA_EEM_SUSPECT_WSSRR {0,0,0,0,0}
#define SASP_MAINEEMSUSPECTDATA_EEM_SUSPECT_FRONTWSS {0,0,0,0,0}
#define SASP_MAINEEMSUSPECTDATA_EEM_SUSPECT_REARWSS {0,0,0,0,0}
#define SASP_MAINCANMONDATA_CANM_MAINBUSOFF_ERR {0,0,0,0,0}
#define SASP_MAINWSSSPEEDOUT_WSSMAX {0,0,0,0,0}
#define SASP_MAINWSSSPEEDOUT_WSSMIN {0,0,0,0,0}
#define SASP_MAINECUMODESTS  {0,0,0,0,0}
#define SASP_MAINIGNONOFFSTS  {0,0,0,0,0}
#define SASP_MAINIGNEDGESTS  {0,0,0,0,0}
#define SASP_MAINVBATT1MON  {0,0,0,0,0}
#define SASP_MAINDIAGCLRSRS  {0,0,0,0,0}
#define SASP_MAINVEHSPD  {0,0,0,0,0}

#define SASP_MAINSASPLAUOUTPUT_SASPLAUMODELERR   {0,0,0,0,0}
#define SASP_MAINSASPLAUOUTPUT_SASPLAUOFFSETERR   {0,0,0,0,0}
#define SASP_MAINSASPLAUOUTPUT_SASPLAUSTICKERR   {0,0,0,0,0}
