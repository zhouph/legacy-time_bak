#include "unity.h"
#include "unity_fixture.h"
#include "SasP_Main.h"
#include "SasP_Main_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Saluint8 UtInput_SasP_MainEemFailData_Eem_Fail_SenPwr_12V[MAX_STEP] = SASP_MAINEEMFAILDATA_EEM_FAIL_SENPWR_12V;
const Saluint8 UtInput_SasP_MainEemFailData_Eem_Fail_Yaw[MAX_STEP] = SASP_MAINEEMFAILDATA_EEM_FAIL_YAW;
const Saluint8 UtInput_SasP_MainEemFailData_Eem_Fail_Ay[MAX_STEP] = SASP_MAINEEMFAILDATA_EEM_FAIL_AY;
const Saluint8 UtInput_SasP_MainEemFailData_Eem_Fail_Str[MAX_STEP] = SASP_MAINEEMFAILDATA_EEM_FAIL_STR;
const Saluint8 UtInput_SasP_MainEemFailData_Eem_Fail_WssFL[MAX_STEP] = SASP_MAINEEMFAILDATA_EEM_FAIL_WSSFL;
const Saluint8 UtInput_SasP_MainEemFailData_Eem_Fail_WssFR[MAX_STEP] = SASP_MAINEEMFAILDATA_EEM_FAIL_WSSFR;
const Saluint8 UtInput_SasP_MainEemFailData_Eem_Fail_WssRL[MAX_STEP] = SASP_MAINEEMFAILDATA_EEM_FAIL_WSSRL;
const Saluint8 UtInput_SasP_MainEemFailData_Eem_Fail_WssRR[MAX_STEP] = SASP_MAINEEMFAILDATA_EEM_FAIL_WSSRR;
const Salsint16 UtInput_SasP_MainCanRxEscInfo_SteeringAngle[MAX_STEP] = SASP_MAINCANRXESCINFO_STEERINGANGLE;
const Saluint8 UtInput_SasP_MainEscSwtStInfo_EscDisabledBySwt[MAX_STEP] = SASP_MAINESCSWTSTINFO_ESCDISABLEDBYSWT;
const Saluint16 UtInput_SasP_MainWhlSpdInfo_FlWhlSpd[MAX_STEP] = SASP_MAINWHLSPDINFO_FLWHLSPD;
const Saluint16 UtInput_SasP_MainWhlSpdInfo_FrWhlSpd[MAX_STEP] = SASP_MAINWHLSPDINFO_FRWHLSPD;
const Saluint16 UtInput_SasP_MainWhlSpdInfo_RlWhlSpd[MAX_STEP] = SASP_MAINWHLSPDINFO_RLWHLSPD;
const Saluint16 UtInput_SasP_MainWhlSpdInfo_RrlWhlSpd[MAX_STEP] = SASP_MAINWHLSPDINFO_RRLWHLSPD;
const Rtesint32 UtInput_SasP_MainAbsCtrlInfo_AbsActFlg[MAX_STEP] = SASP_MAINABSCTRLINFO_ABSACTFLG;
const Rtesint32 UtInput_SasP_MainEscCtrlInfo_EscActFlg[MAX_STEP] = SASP_MAINESCCTRLINFO_ESCACTFLG;
const Rtesint32 UtInput_SasP_MainTcsCtrlInfo_TcsActFlg[MAX_STEP] = SASP_MAINTCSCTRLINFO_TCSACTFLG;
const Saluint8 UtInput_SasP_MainSenPwrMonitorData_SenPwrM_5V_Stable[MAX_STEP] = SASP_MAINSENPWRMONITORDATA_SENPWRM_5V_STABLE;
const Saluint8 UtInput_SasP_MainSenPwrMonitorData_SenPwrM_12V_Stable[MAX_STEP] = SASP_MAINSENPWRMONITORDATA_SENPWRM_12V_STABLE;
const Saluint8 UtInput_SasP_MainEemSuspectData_Eem_Suspect_WssFL[MAX_STEP] = SASP_MAINEEMSUSPECTDATA_EEM_SUSPECT_WSSFL;
const Saluint8 UtInput_SasP_MainEemSuspectData_Eem_Suspect_WssFR[MAX_STEP] = SASP_MAINEEMSUSPECTDATA_EEM_SUSPECT_WSSFR;
const Saluint8 UtInput_SasP_MainEemSuspectData_Eem_Suspect_WssRL[MAX_STEP] = SASP_MAINEEMSUSPECTDATA_EEM_SUSPECT_WSSRL;
const Saluint8 UtInput_SasP_MainEemSuspectData_Eem_Suspect_WssRR[MAX_STEP] = SASP_MAINEEMSUSPECTDATA_EEM_SUSPECT_WSSRR;
const Saluint8 UtInput_SasP_MainEemSuspectData_Eem_Suspect_FrontWss[MAX_STEP] = SASP_MAINEEMSUSPECTDATA_EEM_SUSPECT_FRONTWSS;
const Saluint8 UtInput_SasP_MainEemSuspectData_Eem_Suspect_RearWss[MAX_STEP] = SASP_MAINEEMSUSPECTDATA_EEM_SUSPECT_REARWSS;
const Saluint8 UtInput_SasP_MainCanMonData_CanM_MainBusOff_Err[MAX_STEP] = SASP_MAINCANMONDATA_CANM_MAINBUSOFF_ERR;
const Saluint32 UtInput_SasP_MainWssSpeedOut_WssMax[MAX_STEP] = SASP_MAINWSSSPEEDOUT_WSSMAX;
const Saluint32 UtInput_SasP_MainWssSpeedOut_WssMin[MAX_STEP] = SASP_MAINWSSSPEEDOUT_WSSMIN;
const Mom_HndlrEcuModeSts_t UtInput_SasP_MainEcuModeSts[MAX_STEP] = SASP_MAINECUMODESTS;
const Prly_HndlrIgnOnOffSts_t UtInput_SasP_MainIgnOnOffSts[MAX_STEP] = SASP_MAINIGNONOFFSTS;
const Prly_HndlrIgnEdgeSts_t UtInput_SasP_MainIgnEdgeSts[MAX_STEP] = SASP_MAINIGNEDGESTS;
const Ioc_InputSR1msVBatt1Mon_t UtInput_SasP_MainVBatt1Mon[MAX_STEP] = SASP_MAINVBATT1MON;
const Diag_HndlrDiagClr_t UtInput_SasP_MainDiagClrSrs[MAX_STEP] = SASP_MAINDIAGCLRSRS;
const Abc_CtrlVehSpd_t UtInput_SasP_MainVehSpd[MAX_STEP] = SASP_MAINVEHSPD;

const Saluint8 UtExpected_SasP_MainSasPlauOutput_SasPlauModelErr[MAX_STEP] = SASP_MAINSASPLAUOUTPUT_SASPLAUMODELERR;
const Saluint8 UtExpected_SasP_MainSasPlauOutput_SasPlauOffsetErr[MAX_STEP] = SASP_MAINSASPLAUOUTPUT_SASPLAUOFFSETERR;
const Saluint8 UtExpected_SasP_MainSasPlauOutput_SasPlauStickErr[MAX_STEP] = SASP_MAINSASPLAUOUTPUT_SASPLAUSTICKERR;



TEST_GROUP(SasP_Main);
TEST_SETUP(SasP_Main)
{
    SasP_Main_Init();
}

TEST_TEAR_DOWN(SasP_Main)
{   /* Postcondition */

}

TEST(SasP_Main, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        SasP_MainEemFailData.Eem_Fail_SenPwr_12V = UtInput_SasP_MainEemFailData_Eem_Fail_SenPwr_12V[i];
        SasP_MainEemFailData.Eem_Fail_Yaw = UtInput_SasP_MainEemFailData_Eem_Fail_Yaw[i];
        SasP_MainEemFailData.Eem_Fail_Ay = UtInput_SasP_MainEemFailData_Eem_Fail_Ay[i];
        SasP_MainEemFailData.Eem_Fail_Str = UtInput_SasP_MainEemFailData_Eem_Fail_Str[i];
        SasP_MainEemFailData.Eem_Fail_WssFL = UtInput_SasP_MainEemFailData_Eem_Fail_WssFL[i];
        SasP_MainEemFailData.Eem_Fail_WssFR = UtInput_SasP_MainEemFailData_Eem_Fail_WssFR[i];
        SasP_MainEemFailData.Eem_Fail_WssRL = UtInput_SasP_MainEemFailData_Eem_Fail_WssRL[i];
        SasP_MainEemFailData.Eem_Fail_WssRR = UtInput_SasP_MainEemFailData_Eem_Fail_WssRR[i];
        SasP_MainCanRxEscInfo.SteeringAngle = UtInput_SasP_MainCanRxEscInfo_SteeringAngle[i];
        SasP_MainEscSwtStInfo.EscDisabledBySwt = UtInput_SasP_MainEscSwtStInfo_EscDisabledBySwt[i];
        SasP_MainWhlSpdInfo.FlWhlSpd = UtInput_SasP_MainWhlSpdInfo_FlWhlSpd[i];
        SasP_MainWhlSpdInfo.FrWhlSpd = UtInput_SasP_MainWhlSpdInfo_FrWhlSpd[i];
        SasP_MainWhlSpdInfo.RlWhlSpd = UtInput_SasP_MainWhlSpdInfo_RlWhlSpd[i];
        SasP_MainWhlSpdInfo.RrlWhlSpd = UtInput_SasP_MainWhlSpdInfo_RrlWhlSpd[i];
        SasP_MainAbsCtrlInfo.AbsActFlg = UtInput_SasP_MainAbsCtrlInfo_AbsActFlg[i];
        SasP_MainEscCtrlInfo.EscActFlg = UtInput_SasP_MainEscCtrlInfo_EscActFlg[i];
        SasP_MainTcsCtrlInfo.TcsActFlg = UtInput_SasP_MainTcsCtrlInfo_TcsActFlg[i];
        SasP_MainSenPwrMonitorData.SenPwrM_5V_Stable = UtInput_SasP_MainSenPwrMonitorData_SenPwrM_5V_Stable[i];
        SasP_MainSenPwrMonitorData.SenPwrM_12V_Stable = UtInput_SasP_MainSenPwrMonitorData_SenPwrM_12V_Stable[i];
        SasP_MainEemSuspectData.Eem_Suspect_WssFL = UtInput_SasP_MainEemSuspectData_Eem_Suspect_WssFL[i];
        SasP_MainEemSuspectData.Eem_Suspect_WssFR = UtInput_SasP_MainEemSuspectData_Eem_Suspect_WssFR[i];
        SasP_MainEemSuspectData.Eem_Suspect_WssRL = UtInput_SasP_MainEemSuspectData_Eem_Suspect_WssRL[i];
        SasP_MainEemSuspectData.Eem_Suspect_WssRR = UtInput_SasP_MainEemSuspectData_Eem_Suspect_WssRR[i];
        SasP_MainEemSuspectData.Eem_Suspect_FrontWss = UtInput_SasP_MainEemSuspectData_Eem_Suspect_FrontWss[i];
        SasP_MainEemSuspectData.Eem_Suspect_RearWss = UtInput_SasP_MainEemSuspectData_Eem_Suspect_RearWss[i];
        SasP_MainCanMonData.CanM_MainBusOff_Err = UtInput_SasP_MainCanMonData_CanM_MainBusOff_Err[i];
        SasP_MainWssSpeedOut.WssMax = UtInput_SasP_MainWssSpeedOut_WssMax[i];
        SasP_MainWssSpeedOut.WssMin = UtInput_SasP_MainWssSpeedOut_WssMin[i];
        SasP_MainEcuModeSts = UtInput_SasP_MainEcuModeSts[i];
        SasP_MainIgnOnOffSts = UtInput_SasP_MainIgnOnOffSts[i];
        SasP_MainIgnEdgeSts = UtInput_SasP_MainIgnEdgeSts[i];
        SasP_MainVBatt1Mon = UtInput_SasP_MainVBatt1Mon[i];
        SasP_MainDiagClrSrs = UtInput_SasP_MainDiagClrSrs[i];
        SasP_MainVehSpd = UtInput_SasP_MainVehSpd[i];

        SasP_Main();

        TEST_ASSERT_EQUAL(SasP_MainSasPlauOutput.SasPlauModelErr, UtExpected_SasP_MainSasPlauOutput_SasPlauModelErr[i]);
        TEST_ASSERT_EQUAL(SasP_MainSasPlauOutput.SasPlauOffsetErr, UtExpected_SasP_MainSasPlauOutput_SasPlauOffsetErr[i]);
        TEST_ASSERT_EQUAL(SasP_MainSasPlauOutput.SasPlauStickErr, UtExpected_SasP_MainSasPlauOutput_SasPlauStickErr[i]);
    }
}

TEST_GROUP_RUNNER(SasP_Main)
{
    RUN_TEST_CASE(SasP_Main, All);
}
