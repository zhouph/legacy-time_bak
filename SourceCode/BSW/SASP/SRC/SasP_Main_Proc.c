/**
 * @defgroup SasP_Main SasP_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SasP_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "SasP_Main.h"
#include "SasP_Main_Ifa.h"
#include "SasP_Main_Proc.h"
#include "common.h"
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void SasP_Main_Proc(SasP_Main_HdrBusType *pSasPInfo);
void FAD_vChkDrvMode(SasP_Main_HdrBusType *pSasPInfo);
void FAD_vSteerOffsetCheck(SasP_Main_HdrBusType *pSasPInfo);

Saluint16 fadu16DiffMaxMinSpeed;
Saluint8 fadu1StrDrvOk;

void SasP_Main_Proc(SasP_Main_HdrBusType *pSasPInfo)
{
    /* Process */
    FAD_vChkDrvMode(&SasP_MainBus);
    FAD_vSteerOffsetCheck(&SasP_MainBus);
    /* Output */
    if(pSasPInfo->SasP_MainDiagClrSrs==1)
    {
         pSasPInfo->SasP_MainSasPlauOutput.SasPlauModelErr=ERR_NONE;
         pSasPInfo->SasP_MainSasPlauOutput.SasPlauOffsetErr=ERR_NONE;
         pSasPInfo->SasP_MainSasPlauOutput.SasPlauStickErr=ERR_NONE;
    }

}

void FAD_vChkDrvMode(SasP_Main_HdrBusType *pSasPInfo)
{
    /*To-do : Yaw, Ay, Steer Model 3,4 Should be added*/
    #if 0
    Saluint16 Temp_abs_Yaw,Temp_abs_ALAT,Temp_Diff_Steer,Temp_Diff_Max_Min,Temp_Diff_Mean_Min,Temp_Diff_Mean_Max;
    Saluint16 Temp_abs_model_max,Temp_abs_model_min;
    Temp_abs_Yaw=Temp_abs_ALAT=Temp_Diff_Steer=Temp_Diff_Max_Min=Temp_Diff_Mean_Min=Temp_Diff_Mean_Max=0;
    Temp_abs_model_max=Temp_abs_model_min=0;
    /* Calculate between max speed and min speed */
    fadu16DiffMaxMinSpeed=(pSasPInfo->SasP_MainWssSpeedOut.WssMax-pSasPInfo->SasP_MainWssSpeedOut.WssMin);
    
    Temp_abs_Yaw=(uchar16_t)(abs(YAW.model[0]));
    Temp_abs_ALAT=(uchar16_t)(abs(ALAT.model[0]));
    /* str check */
    if((fadu16DiffMaxMinSpeed<__STR_SW_STRAIGHT_DIFFMAXMIN_SPEED)&&(fadu8ValidModelCount>__STR_SW_STRAIGHT_VALIDMODEL_NUMBER)
    &&(Temp_abs_Yaw <__STR_SW_STRAIGHT_YAWM_0_DET_THRES)&&(Temp_abs_ALAT<__STR_SW_STRAIGHT_AYM_0_DET_THRES)
    &&(fadu1InhibitSlip==0)
    )
    {
        if(fadu8StrDrvModeCnt<__STR_SW_STRAIGHT_CRANK_TIME) 
        {
            fadu8StrDrvModeCnt++;
        }
        else 
        {
            fadu1StrDrvOk=1;
        }
    }
    else
    {
        fadu8StrDrvModeCnt=0;
        fadu1StrDrvOk=0;
    }
    Temp_abs_Yaw=(uchar16_t)(abs(YAW.model[0]));
    Temp_abs_ALAT=(uchar16_t)(abs(ALAT.model[0]));
    Temp_Diff_Steer=(uchar16_t)(abs(STEER.model[3]-STEER.model[4]));
    Temp_abs_model_max=(uchar16_t)(abs(fads16Model_MAX));
    Temp_abs_model_min=(uchar16_t)(abs(fads16Model_MIN));
    /* steady check */
    if((Temp_abs_model_max<__STR_SW_STEADY_MODEL_MAX_VALUE)&&(Temp_abs_model_min<__STR_SW_STEADY_MODEL_MIN_VALUE)
    &&(fadu16DiffMaxMinSpeed<__STR_SW_STEADY_DIFFMAXMIN_SPEED)&&(fadu8ValidModelCount>__STR_SW_STEADY_VALIDMODEL_NUMBER)
    &&(fadu1InhibitIce==0)&&(fadu1InhibitDriverWill==0)&&(fadu1InhibitSlip==0)&&(fadu1InhbitSlalom==0)
    &&(Temp_abs_Yaw<__STR_SW_STEADY_YAWM_0_DET_THRES)&&(Temp_abs_ALAT<__STR_SW_STEADY_AYM_0_DET_THRES)
    &(Temp_Diff_Steer<__STR_SW_STEADY_DIFF_MODEL3_MODEL4)
    )
    {
        f(fadu8SteadyDrvModeCnt<__STR_SW_STEADY_CRANK_TIME)
        {
            fadu8SteadyDrvModeCnt++;
        }
        else
        {
            fadu1SteadyDrvOk=1;
        }
    }
    else 
    {
        fadu8SteadyDrvModeCnt=0;
        fadu1SteadyDrvOk=0;
    }
    Temp_abs_Yaw=(uchar16_t)(abs(YAW.model[0]));
    Temp_abs_ALAT=(uchar16_t)(abs(ALAT.model[0]));
    Temp_Diff_Max_Min=(uchar16_t)(abs(fads16Model_MAX-fads16Model_MIN));
    Temp_Diff_Mean_Min=(uchar16_t)(abs(fads16Model_MEAN-fads16Model_MIN));
    Temp_Diff_Mean_Max=(uchar16_t)(abs(fads16Model_MEAN-fads16Model_MAX));
    Temp_abs_model_max=(uchar16_t)(abs(fads16Model_MAX));
    Temp_abs_model_min=(uchar16_t)(abs(fads16Model_MIN));
    /* unsteady check */
    if((Temp_abs_model_max<__STR_SW_UNSTEADY_MODEL_MAX_VALUE)&&(Temp_abs_model_min<__STR_SW_UNSTEADY_MODEL_MIN_VALUE)
    &&(fadu16DiffMaxMinSpeed<__STR_SW_UNSTEADY_DIFFMAXMIN_SPEED)&&(fadu8ValidModelCount>__STR_SW_UNSTEADY_VALIDMODEL_NUMBER)
    &&(fadu1InhibitIce==0)&&(fadu1InhibitDriverWill==0)&&(fadu1InhibitSlip==0)&&(fadu1InhbitSlalom==0)
    &&(Temp_abs_Yaw<__STR_SW_UNSTEADY_YAWM_0_DET_THRES)&&(Temp_abs_ALAT<__STR_SW_UNSTEADY_AYM_0_DET_THRES)
    &(Temp_Diff_Max_Min<__STR_SW_UNSTEADY_DIFF_MODELMAX_MODELMIN)
    &(Temp_Diff_Mean_Max<__STR_SW_UNSTEADY_DIFF_MODELMEAN_MODELMAX)
    &(Temp_Diff_Mean_Min<__STR_SW_UNSTEADY_DIFF_MODELMEAN_MODELMIN)
    )
    {
        if(fadu8UnsteadyDrvModeCnt<__STR_SW_UNSTEADY_CRANK_TIME)
        {
            fadu8UnsteadyDrvModeCnt++;
        }
        else
        {
            fadu1UnsteadyDrvOk=1;
        }
    }
    else
    {
        adu8UnsteadyDrvModeCnt=0;
        adu1UnsteadyDrvOk=0;
    }
    #endif
}
void FAD_vSteerOffsetCheck(SasP_Main_HdrBusType *pSasPInfo)
{
    Saluint8 fadu1ShortTermOffsetInvalid;
    Saluint8 fadu1ShortTermOffsetSuspect;
    Saluint8 fadu8LWSOffsetErrCnt;
    Saluint8 fadu8LWSOffsetOkCnt;
    Saluint8 fadu8OffsetErrFlg;
    
    fadu1ShortTermOffsetInvalid=0;
    fadu1ShortTermOffsetSuspect=0;
    fadu8OffsetErrFlg=0;
    if(fadu1StrDrvOk==1)
    {
        if(abs(pSasPInfo->SasP_MainCanRxEscInfo.SteeringAngle)>U16_STEER_55DEG)
        {
            fadu1ShortTermOffsetInvalid=1;
            if(fadu8LWSOffsetErrCnt<__STR_OFFSET_DET_TIME)
            {
                fadu8LWSOffsetErrCnt++;
                if(fadu8LWSOffsetErrCnt>(__STR_OFFSET_DET_TIME/2))
                {
                    fadu1ShortTermOffsetSuspect=1;                                        
                }
                else
                {
                    if(fadu8LWSOffsetErrCnt>(__STR_OFFSET_DET_TIME/4))
                    {
                        if( (pSasPInfo->SasP_MainEscCtrlInfo.EscActFlg==1)||(pSasPInfo->SasP_MainTcsCtrlInfo.TcsActFlg==1) )
                        {
                            fadu1ShortTermOffsetSuspect=1;                                            
                        }
                    }
                }
            }
            else
            {
                fadu8LWSOffsetErrCnt=0;
                fadu8OffsetErrFlg=1;
            }
        }
        else
        {
            fadu8LWSOffsetErrCnt=0;

            /* To-Do*/
            /*if(feu1StgShortOffsetTestFlg==0)
            {
                fadu8LWSOffsetOkCnt++;
                if(fadu8LWSOffsetOkCnt>__STR_OFFSET_DET_TIME)
                {
                    fadu8LWSOffsetOkCnt=0;
                }
            }*/
        }
    }
    else 
    {
        fadu8LWSOffsetErrCnt=0;
        fadu8LWSOffsetOkCnt=0;
    }
    pSasPInfo->SasP_MainSasPlauOutput.SasPlauOffsetErr = fadu8OffsetErrFlg;
}


