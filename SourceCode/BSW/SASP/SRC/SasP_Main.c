/**
 * @defgroup SasP_Main SasP_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SasP_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "SasP_Main.h"
#include "SasP_Main_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SASP_MAIN_START_SEC_CONST_UNSPECIFIED
#include "SasP_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define SASP_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "SasP_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SASP_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SasP_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
SasP_Main_HdrBusType SasP_MainBus;

/* Version Info */
const SwcVersionInfo_t SasP_MainVersionInfo = 
{   
    SASP_MAIN_MODULE_ID,           /* SasP_MainVersionInfo.ModuleId */
    SASP_MAIN_MAJOR_VERSION,       /* SasP_MainVersionInfo.MajorVer */
    SASP_MAIN_MINOR_VERSION,       /* SasP_MainVersionInfo.MinorVer */
    SASP_MAIN_PATCH_VERSION,       /* SasP_MainVersionInfo.PatchVer */
    SASP_MAIN_BRANCH_VERSION       /* SasP_MainVersionInfo.BranchVer */
};
    
/* Input Data Element */
Eem_MainEemFailData_t SasP_MainEemFailData;
Proxy_RxCanRxEscInfo_t SasP_MainCanRxEscInfo;
Swt_SenEscSwtStInfo_t SasP_MainEscSwtStInfo;
Wss_SenWhlSpdInfo_t SasP_MainWhlSpdInfo;
Abc_CtrlAbsCtrlInfo_t SasP_MainAbsCtrlInfo;
Abc_CtrlEscCtrlInfo_t SasP_MainEscCtrlInfo;
Abc_CtrlTcsCtrlInfo_t SasP_MainTcsCtrlInfo;
SenPwrM_MainSenPwrMonitor_t SasP_MainSenPwrMonitorData;
Eem_MainEemSuspectData_t SasP_MainEemSuspectData;
CanM_MainCanMonData_t SasP_MainCanMonData;
Wss_SenWssSpeedOut_t SasP_MainWssSpeedOut;
Mom_HndlrEcuModeSts_t SasP_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t SasP_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t SasP_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t SasP_MainVBatt1Mon;
Diag_HndlrDiagClr_t SasP_MainDiagClrSrs;
Abc_CtrlVehSpd_t SasP_MainVehSpd;

/* Output Data Element */
SasP_MainSasPlauOutput_t SasP_MainSasPlauOutput;

uint32 SasP_Main_Timer_Start;
uint32 SasP_Main_Timer_Elapsed;

#define SASP_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SasP_MemMap.h"
#define SASP_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "SasP_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SASP_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "SasP_MemMap.h"
#define SASP_MAIN_START_SEC_VAR_UNSPECIFIED
#include "SasP_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SASP_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "SasP_MemMap.h"
#define SASP_MAIN_START_SEC_VAR_32BIT
#include "SasP_MemMap.h"
/** Variable Section (32BIT)**/


#define SASP_MAIN_STOP_SEC_VAR_32BIT
#include "SasP_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SASP_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SasP_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SASP_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SasP_MemMap.h"
#define SASP_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "SasP_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SASP_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "SasP_MemMap.h"
#define SASP_MAIN_START_SEC_VAR_UNSPECIFIED
#include "SasP_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SASP_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "SasP_MemMap.h"
#define SASP_MAIN_START_SEC_VAR_32BIT
#include "SasP_MemMap.h"
/** Variable Section (32BIT)**/


#define SASP_MAIN_STOP_SEC_VAR_32BIT
#include "SasP_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SASP_MAIN_START_SEC_CODE
#include "SasP_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void SasP_Main_Init(void)
{
    /* Initialize internal bus */
    SasP_MainBus.SasP_MainEemFailData.Eem_Fail_SenPwr_12V = 0;
    SasP_MainBus.SasP_MainEemFailData.Eem_Fail_Yaw = 0;
    SasP_MainBus.SasP_MainEemFailData.Eem_Fail_Ay = 0;
    SasP_MainBus.SasP_MainEemFailData.Eem_Fail_Str = 0;
    SasP_MainBus.SasP_MainEemFailData.Eem_Fail_WssFL = 0;
    SasP_MainBus.SasP_MainEemFailData.Eem_Fail_WssFR = 0;
    SasP_MainBus.SasP_MainEemFailData.Eem_Fail_WssRL = 0;
    SasP_MainBus.SasP_MainEemFailData.Eem_Fail_WssRR = 0;
    SasP_MainBus.SasP_MainCanRxEscInfo.SteeringAngle = 0;
    SasP_MainBus.SasP_MainEscSwtStInfo.EscDisabledBySwt = 0;
    SasP_MainBus.SasP_MainWhlSpdInfo.FlWhlSpd = 0;
    SasP_MainBus.SasP_MainWhlSpdInfo.FrWhlSpd = 0;
    SasP_MainBus.SasP_MainWhlSpdInfo.RlWhlSpd = 0;
    SasP_MainBus.SasP_MainWhlSpdInfo.RrlWhlSpd = 0;
    SasP_MainBus.SasP_MainAbsCtrlInfo.AbsActFlg = 0;
    SasP_MainBus.SasP_MainEscCtrlInfo.EscActFlg = 0;
    SasP_MainBus.SasP_MainTcsCtrlInfo.TcsActFlg = 0;
    SasP_MainBus.SasP_MainSenPwrMonitorData.SenPwrM_5V_Stable = 0;
    SasP_MainBus.SasP_MainSenPwrMonitorData.SenPwrM_12V_Stable = 0;
    SasP_MainBus.SasP_MainEemSuspectData.Eem_Suspect_WssFL = 0;
    SasP_MainBus.SasP_MainEemSuspectData.Eem_Suspect_WssFR = 0;
    SasP_MainBus.SasP_MainEemSuspectData.Eem_Suspect_WssRL = 0;
    SasP_MainBus.SasP_MainEemSuspectData.Eem_Suspect_WssRR = 0;
    SasP_MainBus.SasP_MainEemSuspectData.Eem_Suspect_FrontWss = 0;
    SasP_MainBus.SasP_MainEemSuspectData.Eem_Suspect_RearWss = 0;
    SasP_MainBus.SasP_MainCanMonData.CanM_MainBusOff_Err = 0;
    SasP_MainBus.SasP_MainWssSpeedOut.WssMax = 0;
    SasP_MainBus.SasP_MainWssSpeedOut.WssMin = 0;
    SasP_MainBus.SasP_MainEcuModeSts = 0;
    SasP_MainBus.SasP_MainIgnOnOffSts = 0;
    SasP_MainBus.SasP_MainIgnEdgeSts = 0;
    SasP_MainBus.SasP_MainVBatt1Mon = 0;
    SasP_MainBus.SasP_MainDiagClrSrs = 0;
    SasP_MainBus.SasP_MainVehSpd = 0;
    SasP_MainBus.SasP_MainSasPlauOutput.SasPlauModelErr = 0;
    SasP_MainBus.SasP_MainSasPlauOutput.SasPlauOffsetErr = 0;
    SasP_MainBus.SasP_MainSasPlauOutput.SasPlauStickErr = 0;
}

void SasP_Main(void)
{
    SasP_Main_Timer_Start = STM0_TIM0.U;

    /* Input */
    /* Decomposed structure interface */
    SasP_Main_Read_SasP_MainEemFailData_Eem_Fail_SenPwr_12V(&SasP_MainBus.SasP_MainEemFailData.Eem_Fail_SenPwr_12V);
    SasP_Main_Read_SasP_MainEemFailData_Eem_Fail_Yaw(&SasP_MainBus.SasP_MainEemFailData.Eem_Fail_Yaw);
    SasP_Main_Read_SasP_MainEemFailData_Eem_Fail_Ay(&SasP_MainBus.SasP_MainEemFailData.Eem_Fail_Ay);
    SasP_Main_Read_SasP_MainEemFailData_Eem_Fail_Str(&SasP_MainBus.SasP_MainEemFailData.Eem_Fail_Str);
    SasP_Main_Read_SasP_MainEemFailData_Eem_Fail_WssFL(&SasP_MainBus.SasP_MainEemFailData.Eem_Fail_WssFL);
    SasP_Main_Read_SasP_MainEemFailData_Eem_Fail_WssFR(&SasP_MainBus.SasP_MainEemFailData.Eem_Fail_WssFR);
    SasP_Main_Read_SasP_MainEemFailData_Eem_Fail_WssRL(&SasP_MainBus.SasP_MainEemFailData.Eem_Fail_WssRL);
    SasP_Main_Read_SasP_MainEemFailData_Eem_Fail_WssRR(&SasP_MainBus.SasP_MainEemFailData.Eem_Fail_WssRR);

    /* Decomposed structure interface */
    SasP_Main_Read_SasP_MainCanRxEscInfo_SteeringAngle(&SasP_MainBus.SasP_MainCanRxEscInfo.SteeringAngle);

    /* Decomposed structure interface */
    SasP_Main_Read_SasP_MainEscSwtStInfo_EscDisabledBySwt(&SasP_MainBus.SasP_MainEscSwtStInfo.EscDisabledBySwt);

    SasP_Main_Read_SasP_MainWhlSpdInfo(&SasP_MainBus.SasP_MainWhlSpdInfo);
    /*==============================================================================
    * Members of structure SasP_MainWhlSpdInfo 
     : SasP_MainWhlSpdInfo.FlWhlSpd;
     : SasP_MainWhlSpdInfo.FrWhlSpd;
     : SasP_MainWhlSpdInfo.RlWhlSpd;
     : SasP_MainWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/
    
    /* Decomposed structure interface */
    SasP_Main_Read_SasP_MainAbsCtrlInfo_AbsActFlg(&SasP_MainBus.SasP_MainAbsCtrlInfo.AbsActFlg);

    /* Decomposed structure interface */
    SasP_Main_Read_SasP_MainEscCtrlInfo_EscActFlg(&SasP_MainBus.SasP_MainEscCtrlInfo.EscActFlg);

    /* Decomposed structure interface */
    SasP_Main_Read_SasP_MainTcsCtrlInfo_TcsActFlg(&SasP_MainBus.SasP_MainTcsCtrlInfo.TcsActFlg);

    /* Decomposed structure interface */
    SasP_Main_Read_SasP_MainSenPwrMonitorData_SenPwrM_5V_Stable(&SasP_MainBus.SasP_MainSenPwrMonitorData.SenPwrM_5V_Stable);
    SasP_Main_Read_SasP_MainSenPwrMonitorData_SenPwrM_12V_Stable(&SasP_MainBus.SasP_MainSenPwrMonitorData.SenPwrM_12V_Stable);

    /* Decomposed structure interface */
    SasP_Main_Read_SasP_MainEemSuspectData_Eem_Suspect_WssFL(&SasP_MainBus.SasP_MainEemSuspectData.Eem_Suspect_WssFL);
    SasP_Main_Read_SasP_MainEemSuspectData_Eem_Suspect_WssFR(&SasP_MainBus.SasP_MainEemSuspectData.Eem_Suspect_WssFR);
    SasP_Main_Read_SasP_MainEemSuspectData_Eem_Suspect_WssRL(&SasP_MainBus.SasP_MainEemSuspectData.Eem_Suspect_WssRL);
    SasP_Main_Read_SasP_MainEemSuspectData_Eem_Suspect_WssRR(&SasP_MainBus.SasP_MainEemSuspectData.Eem_Suspect_WssRR);
    SasP_Main_Read_SasP_MainEemSuspectData_Eem_Suspect_FrontWss(&SasP_MainBus.SasP_MainEemSuspectData.Eem_Suspect_FrontWss);
    SasP_Main_Read_SasP_MainEemSuspectData_Eem_Suspect_RearWss(&SasP_MainBus.SasP_MainEemSuspectData.Eem_Suspect_RearWss);

    /* Decomposed structure interface */
    SasP_Main_Read_SasP_MainCanMonData_CanM_MainBusOff_Err(&SasP_MainBus.SasP_MainCanMonData.CanM_MainBusOff_Err);

    SasP_Main_Read_SasP_MainWssSpeedOut(&SasP_MainBus.SasP_MainWssSpeedOut);
    /*==============================================================================
    * Members of structure SasP_MainWssSpeedOut 
     : SasP_MainWssSpeedOut.WssMax;
     : SasP_MainWssSpeedOut.WssMin;
     =============================================================================*/
    
    SasP_Main_Read_SasP_MainEcuModeSts(&SasP_MainBus.SasP_MainEcuModeSts);
    SasP_Main_Read_SasP_MainIgnOnOffSts(&SasP_MainBus.SasP_MainIgnOnOffSts);
    SasP_Main_Read_SasP_MainIgnEdgeSts(&SasP_MainBus.SasP_MainIgnEdgeSts);
    SasP_Main_Read_SasP_MainVBatt1Mon(&SasP_MainBus.SasP_MainVBatt1Mon);
    SasP_Main_Read_SasP_MainDiagClrSrs(&SasP_MainBus.SasP_MainDiagClrSrs);
    SasP_Main_Read_SasP_MainVehSpd(&SasP_MainBus.SasP_MainVehSpd);

    /* Process */
    SasP_Main_Proc(&SasP_MainBus);
    /* Output */
    SasP_Main_Write_SasP_MainSasPlauOutput(&SasP_MainBus.SasP_MainSasPlauOutput);
    /*==============================================================================
    * Members of structure SasP_MainSasPlauOutput 
     : SasP_MainSasPlauOutput.SasPlauModelErr;
     : SasP_MainSasPlauOutput.SasPlauOffsetErr;
     : SasP_MainSasPlauOutput.SasPlauStickErr;
     =============================================================================*/
    

    SasP_Main_Timer_Elapsed = STM0_TIM0.U - SasP_Main_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define SASP_MAIN_STOP_SEC_CODE
#include "SasP_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
