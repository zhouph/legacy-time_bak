/**
 * @defgroup SasP_Main SasP_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SasP_Main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SASP_MAIN_H_
#define SASP_MAIN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "SasP_Types.h"
#include "SasP_Cfg.h"
#include "SasP_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define SASP_MAIN_MODULE_ID      (0)
 #define SASP_MAIN_MAJOR_VERSION  (2)
 #define SASP_MAIN_MINOR_VERSION  (0)
 #define SASP_MAIN_PATCH_VERSION  (0)
 #define SASP_MAIN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern SasP_Main_HdrBusType SasP_MainBus;

/* Version Info */
extern const SwcVersionInfo_t SasP_MainVersionInfo;

/* Input Data Element */
extern Eem_MainEemFailData_t SasP_MainEemFailData;
extern Proxy_RxCanRxEscInfo_t SasP_MainCanRxEscInfo;
extern Swt_SenEscSwtStInfo_t SasP_MainEscSwtStInfo;
extern Wss_SenWhlSpdInfo_t SasP_MainWhlSpdInfo;
extern Abc_CtrlAbsCtrlInfo_t SasP_MainAbsCtrlInfo;
extern Abc_CtrlEscCtrlInfo_t SasP_MainEscCtrlInfo;
extern Abc_CtrlTcsCtrlInfo_t SasP_MainTcsCtrlInfo;
extern SenPwrM_MainSenPwrMonitor_t SasP_MainSenPwrMonitorData;
extern Eem_MainEemSuspectData_t SasP_MainEemSuspectData;
extern CanM_MainCanMonData_t SasP_MainCanMonData;
extern Wss_SenWssSpeedOut_t SasP_MainWssSpeedOut;
extern Mom_HndlrEcuModeSts_t SasP_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t SasP_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t SasP_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t SasP_MainVBatt1Mon;
extern Diag_HndlrDiagClr_t SasP_MainDiagClrSrs;
extern Abc_CtrlVehSpd_t SasP_MainVehSpd;

/* Output Data Element */
extern SasP_MainSasPlauOutput_t SasP_MainSasPlauOutput;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void SasP_Main_Init(void);
extern void SasP_Main(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SASP_MAIN_H_ */
/** @} */
