/**
 * @defgroup SasP_Types SasP_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SasP_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SASP_TYPES_H_
#define SASP_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Rte_Types.h"
#include "RteDataHandle_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Eem_MainEemFailData_t SasP_MainEemFailData;
    Proxy_RxCanRxEscInfo_t SasP_MainCanRxEscInfo;
    Swt_SenEscSwtStInfo_t SasP_MainEscSwtStInfo;
    Wss_SenWhlSpdInfo_t SasP_MainWhlSpdInfo;
    Abc_CtrlAbsCtrlInfo_t SasP_MainAbsCtrlInfo;
    Abc_CtrlEscCtrlInfo_t SasP_MainEscCtrlInfo;
    Abc_CtrlTcsCtrlInfo_t SasP_MainTcsCtrlInfo;
    SenPwrM_MainSenPwrMonitor_t SasP_MainSenPwrMonitorData;
    Eem_MainEemSuspectData_t SasP_MainEemSuspectData;
    CanM_MainCanMonData_t SasP_MainCanMonData;
    Wss_SenWssSpeedOut_t SasP_MainWssSpeedOut;
    Mom_HndlrEcuModeSts_t SasP_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t SasP_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t SasP_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t SasP_MainVBatt1Mon;
    Diag_HndlrDiagClr_t SasP_MainDiagClrSrs;
    Abc_CtrlVehSpd_t SasP_MainVehSpd;

/* Output Data Element */
    SasP_MainSasPlauOutput_t SasP_MainSasPlauOutput;
}SasP_Main_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SASP_TYPES_H_ */
/** @} */
