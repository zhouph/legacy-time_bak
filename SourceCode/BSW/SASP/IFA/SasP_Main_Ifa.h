/**
 * @defgroup SasP_Main_Ifa SasP_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SasP_Main_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SASP_MAIN_IFA_H_
#define SASP_MAIN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define SasP_Main_Read_SasP_MainEemFailData(data) do \
{ \
    *data = SasP_MainEemFailData; \
}while(0);

#define SasP_Main_Read_SasP_MainCanRxEscInfo(data) do \
{ \
    *data = SasP_MainCanRxEscInfo; \
}while(0);

#define SasP_Main_Read_SasP_MainEscSwtStInfo(data) do \
{ \
    *data = SasP_MainEscSwtStInfo; \
}while(0);

#define SasP_Main_Read_SasP_MainWhlSpdInfo(data) do \
{ \
    *data = SasP_MainWhlSpdInfo; \
}while(0);

#define SasP_Main_Read_SasP_MainAbsCtrlInfo(data) do \
{ \
    *data = SasP_MainAbsCtrlInfo; \
}while(0);

#define SasP_Main_Read_SasP_MainEscCtrlInfo(data) do \
{ \
    *data = SasP_MainEscCtrlInfo; \
}while(0);

#define SasP_Main_Read_SasP_MainTcsCtrlInfo(data) do \
{ \
    *data = SasP_MainTcsCtrlInfo; \
}while(0);

#define SasP_Main_Read_SasP_MainSenPwrMonitorData(data) do \
{ \
    *data = SasP_MainSenPwrMonitorData; \
}while(0);

#define SasP_Main_Read_SasP_MainEemSuspectData(data) do \
{ \
    *data = SasP_MainEemSuspectData; \
}while(0);

#define SasP_Main_Read_SasP_MainCanMonData(data) do \
{ \
    *data = SasP_MainCanMonData; \
}while(0);

#define SasP_Main_Read_SasP_MainWssSpeedOut(data) do \
{ \
    *data = SasP_MainWssSpeedOut; \
}while(0);

#define SasP_Main_Read_SasP_MainEemFailData_Eem_Fail_SenPwr_12V(data) do \
{ \
    *data = SasP_MainEemFailData.Eem_Fail_SenPwr_12V; \
}while(0);

#define SasP_Main_Read_SasP_MainEemFailData_Eem_Fail_Yaw(data) do \
{ \
    *data = SasP_MainEemFailData.Eem_Fail_Yaw; \
}while(0);

#define SasP_Main_Read_SasP_MainEemFailData_Eem_Fail_Ay(data) do \
{ \
    *data = SasP_MainEemFailData.Eem_Fail_Ay; \
}while(0);

#define SasP_Main_Read_SasP_MainEemFailData_Eem_Fail_Str(data) do \
{ \
    *data = SasP_MainEemFailData.Eem_Fail_Str; \
}while(0);

#define SasP_Main_Read_SasP_MainEemFailData_Eem_Fail_WssFL(data) do \
{ \
    *data = SasP_MainEemFailData.Eem_Fail_WssFL; \
}while(0);

#define SasP_Main_Read_SasP_MainEemFailData_Eem_Fail_WssFR(data) do \
{ \
    *data = SasP_MainEemFailData.Eem_Fail_WssFR; \
}while(0);

#define SasP_Main_Read_SasP_MainEemFailData_Eem_Fail_WssRL(data) do \
{ \
    *data = SasP_MainEemFailData.Eem_Fail_WssRL; \
}while(0);

#define SasP_Main_Read_SasP_MainEemFailData_Eem_Fail_WssRR(data) do \
{ \
    *data = SasP_MainEemFailData.Eem_Fail_WssRR; \
}while(0);

#define SasP_Main_Read_SasP_MainCanRxEscInfo_SteeringAngle(data) do \
{ \
    *data = SasP_MainCanRxEscInfo.SteeringAngle; \
}while(0);

#define SasP_Main_Read_SasP_MainEscSwtStInfo_EscDisabledBySwt(data) do \
{ \
    *data = SasP_MainEscSwtStInfo.EscDisabledBySwt; \
}while(0);

#define SasP_Main_Read_SasP_MainWhlSpdInfo_FlWhlSpd(data) do \
{ \
    *data = SasP_MainWhlSpdInfo.FlWhlSpd; \
}while(0);

#define SasP_Main_Read_SasP_MainWhlSpdInfo_FrWhlSpd(data) do \
{ \
    *data = SasP_MainWhlSpdInfo.FrWhlSpd; \
}while(0);

#define SasP_Main_Read_SasP_MainWhlSpdInfo_RlWhlSpd(data) do \
{ \
    *data = SasP_MainWhlSpdInfo.RlWhlSpd; \
}while(0);

#define SasP_Main_Read_SasP_MainWhlSpdInfo_RrlWhlSpd(data) do \
{ \
    *data = SasP_MainWhlSpdInfo.RrlWhlSpd; \
}while(0);

#define SasP_Main_Read_SasP_MainAbsCtrlInfo_AbsActFlg(data) do \
{ \
    *data = SasP_MainAbsCtrlInfo.AbsActFlg; \
}while(0);

#define SasP_Main_Read_SasP_MainEscCtrlInfo_EscActFlg(data) do \
{ \
    *data = SasP_MainEscCtrlInfo.EscActFlg; \
}while(0);

#define SasP_Main_Read_SasP_MainTcsCtrlInfo_TcsActFlg(data) do \
{ \
    *data = SasP_MainTcsCtrlInfo.TcsActFlg; \
}while(0);

#define SasP_Main_Read_SasP_MainSenPwrMonitorData_SenPwrM_5V_Stable(data) do \
{ \
    *data = SasP_MainSenPwrMonitorData.SenPwrM_5V_Stable; \
}while(0);

#define SasP_Main_Read_SasP_MainSenPwrMonitorData_SenPwrM_12V_Stable(data) do \
{ \
    *data = SasP_MainSenPwrMonitorData.SenPwrM_12V_Stable; \
}while(0);

#define SasP_Main_Read_SasP_MainEemSuspectData_Eem_Suspect_WssFL(data) do \
{ \
    *data = SasP_MainEemSuspectData.Eem_Suspect_WssFL; \
}while(0);

#define SasP_Main_Read_SasP_MainEemSuspectData_Eem_Suspect_WssFR(data) do \
{ \
    *data = SasP_MainEemSuspectData.Eem_Suspect_WssFR; \
}while(0);

#define SasP_Main_Read_SasP_MainEemSuspectData_Eem_Suspect_WssRL(data) do \
{ \
    *data = SasP_MainEemSuspectData.Eem_Suspect_WssRL; \
}while(0);

#define SasP_Main_Read_SasP_MainEemSuspectData_Eem_Suspect_WssRR(data) do \
{ \
    *data = SasP_MainEemSuspectData.Eem_Suspect_WssRR; \
}while(0);

#define SasP_Main_Read_SasP_MainEemSuspectData_Eem_Suspect_FrontWss(data) do \
{ \
    *data = SasP_MainEemSuspectData.Eem_Suspect_FrontWss; \
}while(0);

#define SasP_Main_Read_SasP_MainEemSuspectData_Eem_Suspect_RearWss(data) do \
{ \
    *data = SasP_MainEemSuspectData.Eem_Suspect_RearWss; \
}while(0);

#define SasP_Main_Read_SasP_MainCanMonData_CanM_MainBusOff_Err(data) do \
{ \
    *data = SasP_MainCanMonData.CanM_MainBusOff_Err; \
}while(0);

#define SasP_Main_Read_SasP_MainWssSpeedOut_WssMax(data) do \
{ \
    *data = SasP_MainWssSpeedOut.WssMax; \
}while(0);

#define SasP_Main_Read_SasP_MainWssSpeedOut_WssMin(data) do \
{ \
    *data = SasP_MainWssSpeedOut.WssMin; \
}while(0);

#define SasP_Main_Read_SasP_MainEcuModeSts(data) do \
{ \
    *data = SasP_MainEcuModeSts; \
}while(0);

#define SasP_Main_Read_SasP_MainIgnOnOffSts(data) do \
{ \
    *data = SasP_MainIgnOnOffSts; \
}while(0);

#define SasP_Main_Read_SasP_MainIgnEdgeSts(data) do \
{ \
    *data = SasP_MainIgnEdgeSts; \
}while(0);

#define SasP_Main_Read_SasP_MainVBatt1Mon(data) do \
{ \
    *data = SasP_MainVBatt1Mon; \
}while(0);

#define SasP_Main_Read_SasP_MainDiagClrSrs(data) do \
{ \
    *data = SasP_MainDiagClrSrs; \
}while(0);

#define SasP_Main_Read_SasP_MainVehSpd(data) do \
{ \
    *data = SasP_MainVehSpd; \
}while(0);


/* Set Output DE MAcro Function */
#define SasP_Main_Write_SasP_MainSasPlauOutput(data) do \
{ \
    SasP_MainSasPlauOutput = *data; \
}while(0);

#define SasP_Main_Write_SasP_MainSasPlauOutput_SasPlauModelErr(data) do \
{ \
    SasP_MainSasPlauOutput.SasPlauModelErr = *data; \
}while(0);

#define SasP_Main_Write_SasP_MainSasPlauOutput_SasPlauOffsetErr(data) do \
{ \
    SasP_MainSasPlauOutput.SasPlauOffsetErr = *data; \
}while(0);

#define SasP_Main_Write_SasP_MainSasPlauOutput_SasPlauStickErr(data) do \
{ \
    SasP_MainSasPlauOutput.SasPlauStickErr = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SASP_MAIN_IFA_H_ */
/** @} */
