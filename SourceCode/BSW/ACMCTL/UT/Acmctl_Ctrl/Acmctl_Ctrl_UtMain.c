#include "unity.h"
#include "unity_fixture.h"
#include "Acmctl_Ctrl.h"
#include "Acmctl_Ctrl_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Rtesint32 UtInput_Acmctl_CtrlMotDqIRefMccInfo_IdRef[MAX_STEP] = ACMCTL_CTRLMOTDQIREFMCCINFO_IDREF;
const Rtesint32 UtInput_Acmctl_CtrlMotDqIRefMccInfo_IqRef[MAX_STEP] = ACMCTL_CTRLMOTDQIREFMCCINFO_IQREF;
const Salsint32 UtInput_Acmctl_CtrlMotCurrInfo_MotCurrPhUMeasd[MAX_STEP] = ACMCTL_CTRLMOTCURRINFO_MOTCURRPHUMEASD;
const Salsint32 UtInput_Acmctl_CtrlMotCurrInfo_MotCurrPhVMeasd[MAX_STEP] = ACMCTL_CTRLMOTCURRINFO_MOTCURRPHVMEASD;
const Salsint32 UtInput_Acmctl_CtrlMotCurrInfo_MotCurrPhWMeasd[MAX_STEP] = ACMCTL_CTRLMOTCURRINFO_MOTCURRPHWMEASD;
const Salsint32 UtInput_Acmctl_CtrlMotRotgAgSigInfo_MotElecAngleFild[MAX_STEP] = ACMCTL_CTRLMOTROTGAGSIGINFO_MOTELECANGLEFILD;
const Saluint8 UtInput_Acmctl_CtrlMotOrgSetStInfo_MotOrgSet[MAX_STEP] = ACMCTL_CTRLMOTORGSETSTINFO_MOTORGSET;
const Salsint32 UtInput_Acmctl_CtrlMotDqIRefSesInfo_IdRef[MAX_STEP] = ACMCTL_CTRLMOTDQIREFSESINFO_IDREF;
const Salsint32 UtInput_Acmctl_CtrlMotDqIRefSesInfo_IqRef[MAX_STEP] = ACMCTL_CTRLMOTDQIREFSESINFO_IQREF;
const Rtesint32 UtInput_Acmctl_CtrlFluxWeakengStInfo_FluxWeakengFlg[MAX_STEP] = ACMCTL_CTRLFLUXWEAKENGSTINFO_FLUXWEAKENGFLG;
const Rtesint32 UtInput_Acmctl_CtrlFluxWeakengStInfo_FluxWeakengFlgOld[MAX_STEP] = ACMCTL_CTRLFLUXWEAKENGSTINFO_FLUXWEAKENGFLGOLD;
const Salsint32 UtInput_Acmctl_CtrlMtrArbtratorData_IqSelected[MAX_STEP] = ACMCTL_CTRLMTRARBTRATORDATA_IQSELECTED;
const Salsint32 UtInput_Acmctl_CtrlMtrArbtratorData_IdSelected[MAX_STEP] = ACMCTL_CTRLMTRARBTRATORDATA_IDSELECTED;
const Salsint32 UtInput_Acmctl_CtrlMtrArbtratorData_UPhasePWMSelected[MAX_STEP] = ACMCTL_CTRLMTRARBTRATORDATA_UPHASEPWMSELECTED;
const Salsint32 UtInput_Acmctl_CtrlMtrArbtratorData_VPhasePWMSelected[MAX_STEP] = ACMCTL_CTRLMTRARBTRATORDATA_VPHASEPWMSELECTED;
const Salsint32 UtInput_Acmctl_CtrlMtrArbtratorData_WPhasePWMSelected[MAX_STEP] = ACMCTL_CTRLMTRARBTRATORDATA_WPHASEPWMSELECTED;
const Salsint32 UtInput_Acmctl_CtrlMtrArbtratorData_MtrCtrlMode[MAX_STEP] = ACMCTL_CTRLMTRARBTRATORDATA_MTRCTRLMODE;
const Saluint8 UtInput_Acmctl_CtrlMtrArbtratorInfo_MtrArbCalMode[MAX_STEP] = ACMCTL_CTRLMTRARBTRATORINFO_MTRARBCALMODE;
const Saluint32 UtInput_Acmctl_CtrlMtrArbtratorInfo_MtrArbState[MAX_STEP] = ACMCTL_CTRLMTRARBTRATORINFO_MTRARBSTATE;
const Salsint32 UtInput_Acmctl_CtrlMtrProcessOutInfo_MotCurrPhUMeasd[MAX_STEP] = ACMCTL_CTRLMTRPROCESSOUTINFO_MOTCURRPHUMEASD;
const Salsint32 UtInput_Acmctl_CtrlMtrProcessOutInfo_MotCurrPhVMeasd[MAX_STEP] = ACMCTL_CTRLMTRPROCESSOUTINFO_MOTCURRPHVMEASD;
const Salsint32 UtInput_Acmctl_CtrlMtrProcessOutInfo_MotCurrPhWMeasd[MAX_STEP] = ACMCTL_CTRLMTRPROCESSOUTINFO_MOTCURRPHWMEASD;
const Saluint16 UtInput_Acmctl_CtrlMtrProcessOutInfo_MotPosiAngle1raw[MAX_STEP] = ACMCTL_CTRLMTRPROCESSOUTINFO_MOTPOSIANGLE1RAW;
const Saluint16 UtInput_Acmctl_CtrlMtrProcessOutInfo_MotPosiAngle2raw[MAX_STEP] = ACMCTL_CTRLMTRPROCESSOUTINFO_MOTPOSIANGLE2RAW;
const Salsint32 UtInput_Acmctl_CtrlMtrProcessOutInfo_MotPosiAngle1_1_100Deg[MAX_STEP] = ACMCTL_CTRLMTRPROCESSOUTINFO_MOTPOSIANGLE1_1_100DEG;
const Salsint32 UtInput_Acmctl_CtrlMtrProcessOutInfo_MotPosiAngle2_1_100Deg[MAX_STEP] = ACMCTL_CTRLMTRPROCESSOUTINFO_MOTPOSIANGLE2_1_100DEG;
const Mom_HndlrEcuModeSts_t UtInput_Acmctl_CtrlEcuModeSts[MAX_STEP] = ACMCTL_CTRLECUMODESTS;
const Msp_1msCtrlVdcLinkFild_t UtInput_Acmctl_CtrlVdcLinkFild[MAX_STEP] = ACMCTL_CTRLVDCLINKFILD;
const Mcc_1msCtrlMotCtrlMode_t UtInput_Acmctl_CtrlMotCtrlMode[MAX_STEP] = ACMCTL_CTRLMOTCTRLMODE;
const Mcc_1msCtrlMotCtrlState_t UtInput_Acmctl_CtrlMotCtrlState[MAX_STEP] = ACMCTL_CTRLMOTCTRLSTATE;
const Eem_SuspcDetnFuncInhibitAcmctlSts_t UtInput_Acmctl_CtrlFuncInhibitAcmctlSts[MAX_STEP] = ACMCTL_CTRLFUNCINHIBITACMCTLSTS;
const Arbitrator_MtrMtrArbDriveState_t UtInput_Acmctl_CtrlMtrArbDriveState[MAX_STEP] = ACMCTL_CTRLMTRARBDRIVESTATE;

const Salsint32 UtExpected_Acmctl_CtrlMotReqDataAcmctlInfo_MotPwmPhUData[MAX_STEP] = ACMCTL_CTRLMOTREQDATAACMCTLINFO_MOTPWMPHUDATA;
const Salsint32 UtExpected_Acmctl_CtrlMotReqDataAcmctlInfo_MotPwmPhVData[MAX_STEP] = ACMCTL_CTRLMOTREQDATAACMCTLINFO_MOTPWMPHVDATA;
const Salsint32 UtExpected_Acmctl_CtrlMotReqDataAcmctlInfo_MotPwmPhWData[MAX_STEP] = ACMCTL_CTRLMOTREQDATAACMCTLINFO_MOTPWMPHWDATA;
const Saluint8 UtExpected_Acmctl_CtrlMotReqDataAcmctlInfo_MotReq[MAX_STEP] = ACMCTL_CTRLMOTREQDATAACMCTLINFO_MOTREQ;
const Salsint32 UtExpected_Acmctl_CtrlMotDqIMeasdInfo_MotIqMeasd[MAX_STEP] = ACMCTL_CTRLMOTDQIMEASDINFO_MOTIQMEASD;
const Salsint32 UtExpected_Acmctl_CtrlMotDqIMeasdInfo_MotIdMeasd[MAX_STEP] = ACMCTL_CTRLMOTDQIMEASDINFO_MOTIDMEASD;



TEST_GROUP(Acmctl_Ctrl);
TEST_SETUP(Acmctl_Ctrl)
{
    Acmctl_Ctrl_Init();
}

TEST_TEAR_DOWN(Acmctl_Ctrl)
{   /* Postcondition */

}

TEST(Acmctl_Ctrl, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Acmctl_CtrlMotDqIRefMccInfo.IdRef = UtInput_Acmctl_CtrlMotDqIRefMccInfo_IdRef[i];
        Acmctl_CtrlMotDqIRefMccInfo.IqRef = UtInput_Acmctl_CtrlMotDqIRefMccInfo_IqRef[i];
        Acmctl_CtrlMotCurrInfo.MotCurrPhUMeasd = UtInput_Acmctl_CtrlMotCurrInfo_MotCurrPhUMeasd[i];
        Acmctl_CtrlMotCurrInfo.MotCurrPhVMeasd = UtInput_Acmctl_CtrlMotCurrInfo_MotCurrPhVMeasd[i];
        Acmctl_CtrlMotCurrInfo.MotCurrPhWMeasd = UtInput_Acmctl_CtrlMotCurrInfo_MotCurrPhWMeasd[i];
        Acmctl_CtrlMotRotgAgSigInfo.MotElecAngleFild = UtInput_Acmctl_CtrlMotRotgAgSigInfo_MotElecAngleFild[i];
        Acmctl_CtrlMotOrgSetStInfo.MotOrgSet = UtInput_Acmctl_CtrlMotOrgSetStInfo_MotOrgSet[i];
        Acmctl_CtrlMotDqIRefSesInfo.IdRef = UtInput_Acmctl_CtrlMotDqIRefSesInfo_IdRef[i];
        Acmctl_CtrlMotDqIRefSesInfo.IqRef = UtInput_Acmctl_CtrlMotDqIRefSesInfo_IqRef[i];
        Acmctl_CtrlFluxWeakengStInfo.FluxWeakengFlg = UtInput_Acmctl_CtrlFluxWeakengStInfo_FluxWeakengFlg[i];
        Acmctl_CtrlFluxWeakengStInfo.FluxWeakengFlgOld = UtInput_Acmctl_CtrlFluxWeakengStInfo_FluxWeakengFlgOld[i];
        Acmctl_CtrlMtrArbtratorData.IqSelected = UtInput_Acmctl_CtrlMtrArbtratorData_IqSelected[i];
        Acmctl_CtrlMtrArbtratorData.IdSelected = UtInput_Acmctl_CtrlMtrArbtratorData_IdSelected[i];
        Acmctl_CtrlMtrArbtratorData.UPhasePWMSelected = UtInput_Acmctl_CtrlMtrArbtratorData_UPhasePWMSelected[i];
        Acmctl_CtrlMtrArbtratorData.VPhasePWMSelected = UtInput_Acmctl_CtrlMtrArbtratorData_VPhasePWMSelected[i];
        Acmctl_CtrlMtrArbtratorData.WPhasePWMSelected = UtInput_Acmctl_CtrlMtrArbtratorData_WPhasePWMSelected[i];
        Acmctl_CtrlMtrArbtratorData.MtrCtrlMode = UtInput_Acmctl_CtrlMtrArbtratorData_MtrCtrlMode[i];
        Acmctl_CtrlMtrArbtratorInfo.MtrArbCalMode = UtInput_Acmctl_CtrlMtrArbtratorInfo_MtrArbCalMode[i];
        Acmctl_CtrlMtrArbtratorInfo.MtrArbState = UtInput_Acmctl_CtrlMtrArbtratorInfo_MtrArbState[i];
        Acmctl_CtrlMtrProcessOutInfo.MotCurrPhUMeasd = UtInput_Acmctl_CtrlMtrProcessOutInfo_MotCurrPhUMeasd[i];
        Acmctl_CtrlMtrProcessOutInfo.MotCurrPhVMeasd = UtInput_Acmctl_CtrlMtrProcessOutInfo_MotCurrPhVMeasd[i];
        Acmctl_CtrlMtrProcessOutInfo.MotCurrPhWMeasd = UtInput_Acmctl_CtrlMtrProcessOutInfo_MotCurrPhWMeasd[i];
        Acmctl_CtrlMtrProcessOutInfo.MotPosiAngle1raw = UtInput_Acmctl_CtrlMtrProcessOutInfo_MotPosiAngle1raw[i];
        Acmctl_CtrlMtrProcessOutInfo.MotPosiAngle2raw = UtInput_Acmctl_CtrlMtrProcessOutInfo_MotPosiAngle2raw[i];
        Acmctl_CtrlMtrProcessOutInfo.MotPosiAngle1_1_100Deg = UtInput_Acmctl_CtrlMtrProcessOutInfo_MotPosiAngle1_1_100Deg[i];
        Acmctl_CtrlMtrProcessOutInfo.MotPosiAngle2_1_100Deg = UtInput_Acmctl_CtrlMtrProcessOutInfo_MotPosiAngle2_1_100Deg[i];
        Acmctl_CtrlEcuModeSts = UtInput_Acmctl_CtrlEcuModeSts[i];
        Acmctl_CtrlVdcLinkFild = UtInput_Acmctl_CtrlVdcLinkFild[i];
        Acmctl_CtrlMotCtrlMode = UtInput_Acmctl_CtrlMotCtrlMode[i];
        Acmctl_CtrlMotCtrlState = UtInput_Acmctl_CtrlMotCtrlState[i];
        Acmctl_CtrlFuncInhibitAcmctlSts = UtInput_Acmctl_CtrlFuncInhibitAcmctlSts[i];
        Acmctl_CtrlMtrArbDriveState = UtInput_Acmctl_CtrlMtrArbDriveState[i];

        Acmctl_Ctrl();

        TEST_ASSERT_EQUAL(Acmctl_CtrlMotReqDataAcmctlInfo.MotPwmPhUData, UtExpected_Acmctl_CtrlMotReqDataAcmctlInfo_MotPwmPhUData[i]);
        TEST_ASSERT_EQUAL(Acmctl_CtrlMotReqDataAcmctlInfo.MotPwmPhVData, UtExpected_Acmctl_CtrlMotReqDataAcmctlInfo_MotPwmPhVData[i]);
        TEST_ASSERT_EQUAL(Acmctl_CtrlMotReqDataAcmctlInfo.MotPwmPhWData, UtExpected_Acmctl_CtrlMotReqDataAcmctlInfo_MotPwmPhWData[i]);
        TEST_ASSERT_EQUAL(Acmctl_CtrlMotReqDataAcmctlInfo.MotReq, UtExpected_Acmctl_CtrlMotReqDataAcmctlInfo_MotReq[i]);
        TEST_ASSERT_EQUAL(Acmctl_CtrlMotDqIMeasdInfo.MotIqMeasd, UtExpected_Acmctl_CtrlMotDqIMeasdInfo_MotIqMeasd[i]);
        TEST_ASSERT_EQUAL(Acmctl_CtrlMotDqIMeasdInfo.MotIdMeasd, UtExpected_Acmctl_CtrlMotDqIMeasdInfo_MotIdMeasd[i]);
    }
}

TEST_GROUP_RUNNER(Acmctl_Ctrl)
{
    RUN_TEST_CASE(Acmctl_Ctrl, All);
}
