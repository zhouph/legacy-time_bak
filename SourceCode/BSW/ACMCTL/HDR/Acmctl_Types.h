/**
 * @defgroup Acmctl_Types Acmctl_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Acmctl_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ACMCTL_TYPES_H_
#define ACMCTL_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Rte_Types.h"
#include "RteDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
typedef short int           	Mivt_Frac16;

#define S16_MTR_DUTY_MAX        (4999)
#define S16_MTR_DUTY_MAX_HALF   (2499)
#define F_MTR_DUTY_MAX        	(4999.0f)
#define F_MTR_DUTY_MAX_HALF    	(2499.0f)

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Mcc_1msCtrlMotDqIRefMccInfo_t Acmctl_CtrlMotDqIRefMccInfo;
    Acmio_SenMotCurrInfo_t Acmctl_CtrlMotCurrInfo;
    Msp_CtrlMotRotgAgSigInfo_t Acmctl_CtrlMotRotgAgSigInfo;
    Ses_CtrlMotOrgSetStInfo_t Acmctl_CtrlMotOrgSetStInfo;
    Ses_CtrlMotDqIRefSesInfo_t Acmctl_CtrlMotDqIRefSesInfo;
    Mcc_1msCtrlFluxWeakengStInfo_t Acmctl_CtrlFluxWeakengStInfo;
    Arbitrator_MtrMtrArbitratorData_t Acmctl_CtrlMtrArbtratorData;
    Arbitrator_MtrMtrArbitratorInfo_t Acmctl_CtrlMtrArbtratorInfo;
    Mtr_Processing_SigMtrProcessOutInfo_t Acmctl_CtrlMtrProcessOutInfo;
    Mom_HndlrEcuModeSts_t Acmctl_CtrlEcuModeSts;
    Msp_1msCtrlVdcLinkFild_t Acmctl_CtrlVdcLinkFild;
    Mcc_1msCtrlMotCtrlMode_t Acmctl_CtrlMotCtrlMode;
    Mcc_1msCtrlMotCtrlState_t Acmctl_CtrlMotCtrlState;
    Eem_SuspcDetnFuncInhibitAcmctlSts_t Acmctl_CtrlFuncInhibitAcmctlSts;
    Arbitrator_MtrMtrArbDriveState_t Acmctl_CtrlMtrArbDriveState;

/* Output Data Element */
    Acmctl_CtrlMotReqDataAcmctlInfo_t Acmctl_CtrlMotReqDataAcmctlInfo;
    Acmctl_CtrlMotDqIMeasdInfo_t Acmctl_CtrlMotDqIMeasdInfo;
}Acmctl_Ctrl_HdrBusType;

typedef struct
{
	float fsin;
	float fcos;
} fsc_t;

typedef struct
{
	float fa;
	float fb;
	float fc;
} fabc_t;

typedef struct
{
	float fd;
	float fq;
} fdq_t;

typedef struct
{
	/* SETTING PARAMETERS */
	float fKp;
	float fKiTs;
	float fId;

	/* NON-SETTING PARAMETERS */
	float fUp;
	float fUi;

	float fErr;
	float fErrUi;

	float fUdiff;
	float fUout;
	float fUcalc;
} fpi_t;


//fixed
/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
typedef signed long         Mivt_Frac32;

#define Mivt_F16_MAX			(32767)
#define Mivt_F16_1              (32768)
#define Mivt_mult_r(a, b)       ((sint16)(((sint32)((Mivt_Frac16)(a)) * (sint32)((sint16)(b))) / Mivt_F16_1))
#define Mivt_Frac16(x)          ((Mivt_Frac16)((x) < 0.999969482421875 ? ((x) >= -1 ? (x)*0x8000 : 0x8000) : 0x7FFF))


#define SQRT3               (1.7320508)
#define ONE_DIV_SQRT3       (Mivt_Frac16( 1 / SQRT3 ))
#define HALF_SQRT3          (Mivt_Frac16( SQRT3 / 2 ))
#define ONE                 (0x007fff00)

#define S16_MTR_DUTY_MAX        (4999)
#define S16_MTR_DUTY_MAX_HALF   (2499)
#define MTR_PWM_MAX             (4999)

#define S16_MTR_MAX                 32767
#define S16_MTR_MIN                -32767

typedef struct
{
    Mivt_Frac16 a;
    Mivt_Frac16 b;
    Mivt_Frac16 c;
} MC_3PhSyst;

typedef struct
{
    Mivt_Frac16 alpha;
    Mivt_Frac16 beta;
} MC_2PhSyst;

typedef struct
{
    Mivt_Frac16 sin;
    Mivt_Frac16 cos;
} MC_Angle;

typedef struct
{
    Mivt_Frac16 d;
    Mivt_Frac16 q;
} MC_DqSyst;

typedef struct
{
    /* SETTING PARAMETERS */
	sint16 s16Kp;
	Mivt_Frac16 fr16Kp;
	sint16 s16KiTs;
	Mivt_Frac16 fr16KiTs;
    /* SETTING PARAMETERS */

    /* NON-SETTING PARAMETERS */
	sint32 s32Up;
	sint32 s32Ui;
	sint32 s32Uff;

	sint32 s32Err;
	sint32 s32ErrUi;

    sint16  s16Uout;
	sint32 s32Ucalc;
	sint32 s32Udiff;
    /* NON-SETTING PARAMETERS */
} pi3_t;


/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ACMCTL_TYPES_H_ */
/** @} */
