/**
 * @defgroup Acmctl_Ctrl Acmctl_Ctrl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Acmctl_Ctrl.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ACMCTL_CTRL_H_
#define ACMCTL_CTRL_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Acmctl_Types.h"
#include "Acmctl_Cfg.h"
#include "Acmctl_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define ACMCTL_CTRL_MODULE_ID      (0)
 #define ACMCTL_CTRL_MAJOR_VERSION  (2)
 #define ACMCTL_CTRL_MINOR_VERSION  (0)
 #define ACMCTL_CTRL_PATCH_VERSION  (0)
 #define ACMCTL_CTRL_BRANCH_VERSION (0)

 #define ACMCTL_FLOATING			(1)

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Acmctl_Ctrl_HdrBusType Acmctl_CtrlBus;

/* Version Info */
extern const SwcVersionInfo_t Acmctl_CtrlVersionInfo;

/* Input Data Element */
extern Mcc_1msCtrlMotDqIRefMccInfo_t Acmctl_CtrlMotDqIRefMccInfo;
extern Acmio_SenMotCurrInfo_t Acmctl_CtrlMotCurrInfo;
extern Msp_CtrlMotRotgAgSigInfo_t Acmctl_CtrlMotRotgAgSigInfo;
extern Ses_CtrlMotOrgSetStInfo_t Acmctl_CtrlMotOrgSetStInfo;
extern Ses_CtrlMotDqIRefSesInfo_t Acmctl_CtrlMotDqIRefSesInfo;
extern Mcc_1msCtrlFluxWeakengStInfo_t Acmctl_CtrlFluxWeakengStInfo;
extern Arbitrator_MtrMtrArbitratorData_t Acmctl_CtrlMtrArbtratorData;
extern Arbitrator_MtrMtrArbitratorInfo_t Acmctl_CtrlMtrArbtratorInfo;
extern Mtr_Processing_SigMtrProcessOutInfo_t Acmctl_CtrlMtrProcessOutInfo;
extern Mom_HndlrEcuModeSts_t Acmctl_CtrlEcuModeSts;
extern Msp_1msCtrlVdcLinkFild_t Acmctl_CtrlVdcLinkFild;
extern Mcc_1msCtrlMotCtrlMode_t Acmctl_CtrlMotCtrlMode;
extern Mcc_1msCtrlMotCtrlState_t Acmctl_CtrlMotCtrlState;
extern Eem_SuspcDetnFuncInhibitAcmctlSts_t Acmctl_CtrlFuncInhibitAcmctlSts;
extern Arbitrator_MtrMtrArbDriveState_t Acmctl_CtrlMtrArbDriveState;

/* Output Data Element */
extern Acmctl_CtrlMotReqDataAcmctlInfo_t Acmctl_CtrlMotReqDataAcmctlInfo;
extern Acmctl_CtrlMotDqIMeasdInfo_t Acmctl_CtrlMotDqIMeasdInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Acmctl_Ctrl_Init(void);
extern void Acmctl_Ctrl(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ACMCTL_CTRL_H_ */
/** @} */
