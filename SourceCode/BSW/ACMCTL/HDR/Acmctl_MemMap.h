/**
 * @defgroup Acmctl_MemMap Acmctl_MemMap
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Acmctl_MemMap.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ACMCTL_MEMMAP_H_
#define ACMCTL_MEMMAP_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/*------------------[MemMap error checking]----------------------------------*/

#if (defined MEMMAP_ERROR) /* to prevent double definition */
#error MEMMAP_ERROR defined, wrong MemMap.h usage
#endif /* if (!defined MEMMAP_ERROR) */

/** \brief Checks if this file has been correctly used
 **
 ** This definition checks if this file has been correctly included
 ** the last time.
 ** We do it here, before the big if-elif. */
#define MEMMAP_ERROR


#if 0
/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (ACMCTL_START_SEC_CODE)
  #undef ACMCTL_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_ACMCTL_SEC_STARTED
    #error "ACMCTL section not closed"
  #endif
  #define CHK_ACMCTL_SEC_STARTED
  #define CHK_ACMCTL_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACMCTL_STOP_SEC_CODE)
  #undef ACMCTL_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ACMCTL_SEC_CODE_STARTED
    #error "ACMCTL_SEC_CODE not opened"
  #endif
  #undef CHK_ACMCTL_SEC_STARTED
  #undef CHK_ACMCTL_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (ACMCTL_START_SEC_CONST_UNSPECIFIED)
  #undef ACMCTL_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_ACMCTL_SEC_STARTED
    #error "ACMCTL section not closed"
  #endif
  #define CHK_ACMCTL_SEC_STARTED
  #define CHK_ACMCTL_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACMCTL_STOP_SEC_CONST_UNSPECIFIED)
  #undef ACMCTL_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ACMCTL_SEC_CONST_UNSPECIFIED_STARTED
    #error "ACMCTL_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_ACMCTL_SEC_STARTED
  #undef CHK_ACMCTL_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (ACMCTL_START_SEC_VAR_UNSPECIFIED)
  #undef ACMCTL_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_ACMCTL_SEC_STARTED
    #error "ACMCTL section not closed"
  #endif
  #define CHK_ACMCTL_SEC_STARTED
  #define CHK_ACMCTL_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACMCTL_STOP_SEC_VAR_UNSPECIFIED)
  #undef ACMCTL_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ACMCTL_SEC_VAR_UNSPECIFIED_STARTED
    #error "ACMCTL_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_ACMCTL_SEC_STARTED
  #undef CHK_ACMCTL_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACMCTL_START_SEC_VAR_32BIT)
  #undef ACMCTL_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_ACMCTL_SEC_STARTED
    #error "ACMCTL section not closed"
  #endif
  #define CHK_ACMCTL_SEC_STARTED
  #define CHK_ACMCTL_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACMCTL_STOP_SEC_VAR_32BIT)
  #undef ACMCTL_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ACMCTL_SEC_VAR_32BIT_STARTED
    #error "ACMCTL_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_ACMCTL_SEC_STARTED
  #undef CHK_ACMCTL_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACMCTL_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef ACMCTL_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_ACMCTL_SEC_STARTED
    #error "ACMCTL section not closed"
  #endif
  #define CHK_ACMCTL_SEC_STARTED
  #define CHK_ACMCTL_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACMCTL_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef ACMCTL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ACMCTL_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "ACMCTL_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_ACMCTL_SEC_STARTED
  #undef CHK_ACMCTL_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACMCTL_START_SEC_VAR_NOINIT_32BIT)
  #undef ACMCTL_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_ACMCTL_SEC_STARTED
    #error "ACMCTL section not closed"
  #endif
  #define CHK_ACMCTL_SEC_STARTED
  #define CHK_ACMCTL_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACMCTL_STOP_SEC_VAR_NOINIT_32BIT)
  #undef ACMCTL_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ACMCTL_SEC_VAR_NOINIT_32BIT_STARTED
    #error "ACMCTL_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_ACMCTL_SEC_STARTED
  #undef CHK_ACMCTL_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (ACMCTL_START_SEC_CALIB_UNSPECIFIED)
  #undef ACMCTL_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_ACMCTL_SEC_STARTED
    #error "ACMCTL section not closed"
  #endif
  #define CHK_ACMCTL_SEC_STARTED
  #define CHK_ACMCTL_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACMCTL_STOP_SEC_CALIB_UNSPECIFIED)
  #undef ACMCTL_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ACMCTL_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "ACMCTL_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_ACMCTL_SEC_STARTED
  #undef CHK_ACMCTL_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR



/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (ACMCTL_CTRL_START_SEC_CODE)
  #undef ACMCTL_CTRL_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_ACMCTL_CTRL_SEC_STARTED
    #error "ACMCTL_CTRL section not closed"
  #endif
  #define CHK_ACMCTL_CTRL_SEC_STARTED
  #define CHK_ACMCTL_CTRL_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACMCTL_CTRL_STOP_SEC_CODE)
  #undef ACMCTL_CTRL_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ACMCTL_CTRL_SEC_CODE_STARTED
    #error "ACMCTL_CTRL_SEC_CODE not opened"
  #endif
  #undef CHK_ACMCTL_CTRL_SEC_STARTED
  #undef CHK_ACMCTL_CTRL_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (ACMCTL_CTRL_START_SEC_CONST_UNSPECIFIED)
  #undef ACMCTL_CTRL_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_ACMCTL_CTRL_SEC_STARTED
    #error "ACMCTL_CTRL section not closed"
  #endif
  #define CHK_ACMCTL_CTRL_SEC_STARTED
  #define CHK_ACMCTL_CTRL_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACMCTL_CTRL_STOP_SEC_CONST_UNSPECIFIED)
  #undef ACMCTL_CTRL_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ACMCTL_CTRL_SEC_CONST_UNSPECIFIED_STARTED
    #error "ACMCTL_CTRL_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_ACMCTL_CTRL_SEC_STARTED
  #undef CHK_ACMCTL_CTRL_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (ACMCTL_CTRL_START_SEC_VAR_UNSPECIFIED)
  #undef ACMCTL_CTRL_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_ACMCTL_CTRL_SEC_STARTED
    #error "ACMCTL_CTRL section not closed"
  #endif
  #define CHK_ACMCTL_CTRL_SEC_STARTED
  #define CHK_ACMCTL_CTRL_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACMCTL_CTRL_STOP_SEC_VAR_UNSPECIFIED)
  #undef ACMCTL_CTRL_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ACMCTL_CTRL_SEC_VAR_UNSPECIFIED_STARTED
    #error "ACMCTL_CTRL_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_ACMCTL_CTRL_SEC_STARTED
  #undef CHK_ACMCTL_CTRL_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACMCTL_CTRL_START_SEC_VAR_32BIT)
  #undef ACMCTL_CTRL_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_ACMCTL_CTRL_SEC_STARTED
    #error "ACMCTL_CTRL section not closed"
  #endif
  #define CHK_ACMCTL_CTRL_SEC_STARTED
  #define CHK_ACMCTL_CTRL_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACMCTL_CTRL_STOP_SEC_VAR_32BIT)
  #undef ACMCTL_CTRL_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ACMCTL_CTRL_SEC_VAR_32BIT_STARTED
    #error "ACMCTL_CTRL_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_ACMCTL_CTRL_SEC_STARTED
  #undef CHK_ACMCTL_CTRL_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACMCTL_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef ACMCTL_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_ACMCTL_CTRL_SEC_STARTED
    #error "ACMCTL_CTRL section not closed"
  #endif
  #define CHK_ACMCTL_CTRL_SEC_STARTED
  #define CHK_ACMCTL_CTRL_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACMCTL_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef ACMCTL_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ACMCTL_CTRL_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "ACMCTL_CTRL_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_ACMCTL_CTRL_SEC_STARTED
  #undef CHK_ACMCTL_CTRL_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACMCTL_CTRL_START_SEC_VAR_NOINIT_32BIT)
  #undef ACMCTL_CTRL_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_ACMCTL_CTRL_SEC_STARTED
    #error "ACMCTL_CTRL section not closed"
  #endif
  #define CHK_ACMCTL_CTRL_SEC_STARTED
  #define CHK_ACMCTL_CTRL_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACMCTL_CTRL_STOP_SEC_VAR_NOINIT_32BIT)
  #undef ACMCTL_CTRL_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ACMCTL_CTRL_SEC_VAR_NOINIT_32BIT_STARTED
    #error "ACMCTL_CTRL_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_ACMCTL_CTRL_SEC_STARTED
  #undef CHK_ACMCTL_CTRL_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (ACMCTL_CTRL_START_SEC_CALIB_UNSPECIFIED)
  #undef ACMCTL_CTRL_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_ACMCTL_CTRL_SEC_STARTED
    #error "ACMCTL_CTRL section not closed"
  #endif
  #define CHK_ACMCTL_CTRL_SEC_STARTED
  #define CHK_ACMCTL_CTRL_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (ACMCTL_CTRL_STOP_SEC_CALIB_UNSPECIFIED)
  #undef ACMCTL_CTRL_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_ACMCTL_CTRL_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "ACMCTL_CTRL_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_ACMCTL_CTRL_SEC_STARTED
  #undef CHK_ACMCTL_CTRL_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR


#endif


/*------------------[MemMap error checking]----------------------------------*/

#if (defined MEMMAP_ERROR) /* to prevent double definition */
#error MEMMAP_ERROR defined, wrong MemMap.h usage
#endif /* if (!defined MEMMAP_ERROR) */
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ACMCTL_MEMMAP_H_ */
/** @} */
