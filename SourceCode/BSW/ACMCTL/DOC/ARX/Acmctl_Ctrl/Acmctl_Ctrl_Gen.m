Acmctl_CtrlMotDqIRefMccInfo = Simulink.Bus;
DeList={
    'IdRef'
    'IqRef'
    };
Acmctl_CtrlMotDqIRefMccInfo = CreateBus(Acmctl_CtrlMotDqIRefMccInfo, DeList);
clear DeList;

Acmctl_CtrlMotCurrInfo = Simulink.Bus;
DeList={
    'MotCurrPhUMeasd'
    'MotCurrPhVMeasd'
    'MotCurrPhWMeasd'
    };
Acmctl_CtrlMotCurrInfo = CreateBus(Acmctl_CtrlMotCurrInfo, DeList);
clear DeList;

Acmctl_CtrlMotRotgAgSigInfo = Simulink.Bus;
DeList={
    'MotElecAngleFild'
    };
Acmctl_CtrlMotRotgAgSigInfo = CreateBus(Acmctl_CtrlMotRotgAgSigInfo, DeList);
clear DeList;

Acmctl_CtrlMotOrgSetStInfo = Simulink.Bus;
DeList={
    'MotOrgSet'
    };
Acmctl_CtrlMotOrgSetStInfo = CreateBus(Acmctl_CtrlMotOrgSetStInfo, DeList);
clear DeList;

Acmctl_CtrlMotDqIRefSesInfo = Simulink.Bus;
DeList={
    'IdRef'
    'IqRef'
    };
Acmctl_CtrlMotDqIRefSesInfo = CreateBus(Acmctl_CtrlMotDqIRefSesInfo, DeList);
clear DeList;

Acmctl_CtrlFluxWeakengStInfo = Simulink.Bus;
DeList={
    'FluxWeakengFlg'
    'FluxWeakengFlgOld'
    };
Acmctl_CtrlFluxWeakengStInfo = CreateBus(Acmctl_CtrlFluxWeakengStInfo, DeList);
clear DeList;

Acmctl_CtrlMtrArbtratorData = Simulink.Bus;
DeList={
    'IqSelected'
    'IdSelected'
    'UPhasePWMSelected'
    'VPhasePWMSelected'
    'WPhasePWMSelected'
    'MtrCtrlMode'
    };
Acmctl_CtrlMtrArbtratorData = CreateBus(Acmctl_CtrlMtrArbtratorData, DeList);
clear DeList;

Acmctl_CtrlMtrArbtratorInfo = Simulink.Bus;
DeList={
    'MtrArbCalMode'
    'MtrArbState'
    };
Acmctl_CtrlMtrArbtratorInfo = CreateBus(Acmctl_CtrlMtrArbtratorInfo, DeList);
clear DeList;

Acmctl_CtrlMtrProcessOutInfo = Simulink.Bus;
DeList={
    'MotCurrPhUMeasd'
    'MotCurrPhVMeasd'
    'MotCurrPhWMeasd'
    'MotPosiAngle1raw'
    'MotPosiAngle2raw'
    'MotPosiAngle1_1_100Deg'
    'MotPosiAngle2_1_100Deg'
    };
Acmctl_CtrlMtrProcessOutInfo = CreateBus(Acmctl_CtrlMtrProcessOutInfo, DeList);
clear DeList;

Acmctl_CtrlEcuModeSts = Simulink.Bus;
DeList={'Acmctl_CtrlEcuModeSts'};
Acmctl_CtrlEcuModeSts = CreateBus(Acmctl_CtrlEcuModeSts, DeList);
clear DeList;

Acmctl_CtrlVdcLinkFild = Simulink.Bus;
DeList={'Acmctl_CtrlVdcLinkFild'};
Acmctl_CtrlVdcLinkFild = CreateBus(Acmctl_CtrlVdcLinkFild, DeList);
clear DeList;

Acmctl_CtrlMotCtrlMode = Simulink.Bus;
DeList={'Acmctl_CtrlMotCtrlMode'};
Acmctl_CtrlMotCtrlMode = CreateBus(Acmctl_CtrlMotCtrlMode, DeList);
clear DeList;

Acmctl_CtrlMotCtrlState = Simulink.Bus;
DeList={'Acmctl_CtrlMotCtrlState'};
Acmctl_CtrlMotCtrlState = CreateBus(Acmctl_CtrlMotCtrlState, DeList);
clear DeList;

Acmctl_CtrlFuncInhibitAcmctlSts = Simulink.Bus;
DeList={'Acmctl_CtrlFuncInhibitAcmctlSts'};
Acmctl_CtrlFuncInhibitAcmctlSts = CreateBus(Acmctl_CtrlFuncInhibitAcmctlSts, DeList);
clear DeList;

Acmctl_CtrlMtrArbDriveState = Simulink.Bus;
DeList={'Acmctl_CtrlMtrArbDriveState'};
Acmctl_CtrlMtrArbDriveState = CreateBus(Acmctl_CtrlMtrArbDriveState, DeList);
clear DeList;

Acmctl_CtrlMotReqDataAcmctlInfo = Simulink.Bus;
DeList={
    'MotPwmPhUData'
    'MotPwmPhVData'
    'MotPwmPhWData'
    'MotReq'
    };
Acmctl_CtrlMotReqDataAcmctlInfo = CreateBus(Acmctl_CtrlMotReqDataAcmctlInfo, DeList);
clear DeList;

Acmctl_CtrlMotDqIMeasdInfo = Simulink.Bus;
DeList={
    'MotIqMeasd'
    'MotIdMeasd'
    };
Acmctl_CtrlMotDqIMeasdInfo = CreateBus(Acmctl_CtrlMotDqIMeasdInfo, DeList);
clear DeList;

