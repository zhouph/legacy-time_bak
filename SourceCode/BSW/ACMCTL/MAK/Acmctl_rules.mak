# \file
#
# \brief Acmctl
#
# This file contains the implementation of the SWC
# module Acmctl.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Acmctl_src

Acmctl_src_FILES        += $(Acmctl_SRC_PATH)\Acmctl_Ctrl.c
Acmctl_src_FILES        += $(Acmctl_IFA_PATH)\Acmctl_Ctrl_Ifa.c
Acmctl_src_FILES        += $(Acmctl_CFG_PATH)\Acmctl_Cfg.c
Acmctl_src_FILES        += $(Acmctl_CAL_PATH)\Acmctl_Cal.c
Acmctl_src_FILES        += $(Acmctl_SRC_PATH)\Acmctl_ControlCurrent.c
Acmctl_src_FILES        += $(Acmctl_SRC_PATH)\Acmctl_MotorControllerLibrary.c
ifeq ($(ICE_COMPILE),true)
Acmctl_src_FILES        += $(Acmctl_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	Acmctl_src_FILES        += $(Acmctl_UNITY_PATH)\unity.c
	Acmctl_src_FILES        += $(Acmctl_UNITY_PATH)\unity_fixture.c	
	Acmctl_src_FILES        += $(Acmctl_UT_PATH)\main.c
	Acmctl_src_FILES        += $(Acmctl_UT_PATH)\Acmctl_Ctrl\Acmctl_Ctrl_UtMain.c
endif