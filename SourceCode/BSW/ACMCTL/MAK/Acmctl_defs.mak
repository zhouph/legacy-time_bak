# \file
#
# \brief Acmctl
#
# This file contains the implementation of the SWC
# module Acmctl.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Acmctl_CORE_PATH     := $(MANDO_BSW_ROOT)\Acmctl
Acmctl_CAL_PATH      := $(Acmctl_CORE_PATH)\CAL\$(Acmctl_VARIANT)
Acmctl_SRC_PATH      := $(Acmctl_CORE_PATH)\SRC
Acmctl_CFG_PATH      := $(Acmctl_CORE_PATH)\CFG\$(Acmctl_VARIANT)
Acmctl_HDR_PATH      := $(Acmctl_CORE_PATH)\HDR
Acmctl_IFA_PATH      := $(Acmctl_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Acmctl_CMN_PATH      := $(Acmctl_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	Acmctl_UT_PATH		:= $(Acmctl_CORE_PATH)\UT
	Acmctl_UNITY_PATH	:= $(Acmctl_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(Acmctl_UT_PATH)
	CC_INCLUDE_PATH		+= $(Acmctl_UNITY_PATH)
	Acmctl_Ctrl_PATH 	:= Acmctl_UT_PATH\Acmctl_Ctrl
endif
CC_INCLUDE_PATH    += $(Acmctl_CAL_PATH)
CC_INCLUDE_PATH    += $(Acmctl_SRC_PATH)
CC_INCLUDE_PATH    += $(Acmctl_CFG_PATH)
CC_INCLUDE_PATH    += $(Acmctl_HDR_PATH)
CC_INCLUDE_PATH    += $(Acmctl_IFA_PATH)
CC_INCLUDE_PATH    += $(Acmctl_CMN_PATH)

