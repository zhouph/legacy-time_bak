#define S_FUNCTION_NAME      Acmctl_Ctrl_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          32
#define WidthOutputPort         6

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Acmctl_Ctrl.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Acmctl_CtrlMotDqIRefMccInfo.IdRef = input[0];
    Acmctl_CtrlMotDqIRefMccInfo.IqRef = input[1];
    Acmctl_CtrlMotCurrInfo.MotCurrPhUMeasd = input[2];
    Acmctl_CtrlMotCurrInfo.MotCurrPhVMeasd = input[3];
    Acmctl_CtrlMotCurrInfo.MotCurrPhWMeasd = input[4];
    Acmctl_CtrlMotRotgAgSigInfo.MotElecAngleFild = input[5];
    Acmctl_CtrlMotOrgSetStInfo.MotOrgSet = input[6];
    Acmctl_CtrlMotDqIRefSesInfo.IdRef = input[7];
    Acmctl_CtrlMotDqIRefSesInfo.IqRef = input[8];
    Acmctl_CtrlFluxWeakengStInfo.FluxWeakengFlg = input[9];
    Acmctl_CtrlFluxWeakengStInfo.FluxWeakengFlgOld = input[10];
    Acmctl_CtrlMtrArbtratorData.IqSelected = input[11];
    Acmctl_CtrlMtrArbtratorData.IdSelected = input[12];
    Acmctl_CtrlMtrArbtratorData.UPhasePWMSelected = input[13];
    Acmctl_CtrlMtrArbtratorData.VPhasePWMSelected = input[14];
    Acmctl_CtrlMtrArbtratorData.WPhasePWMSelected = input[15];
    Acmctl_CtrlMtrArbtratorData.MtrCtrlMode = input[16];
    Acmctl_CtrlMtrArbtratorInfo.MtrArbCalMode = input[17];
    Acmctl_CtrlMtrArbtratorInfo.MtrArbState = input[18];
    Acmctl_CtrlMtrProcessOutInfo.MotCurrPhUMeasd = input[19];
    Acmctl_CtrlMtrProcessOutInfo.MotCurrPhVMeasd = input[20];
    Acmctl_CtrlMtrProcessOutInfo.MotCurrPhWMeasd = input[21];
    Acmctl_CtrlMtrProcessOutInfo.MotPosiAngle1raw = input[22];
    Acmctl_CtrlMtrProcessOutInfo.MotPosiAngle2raw = input[23];
    Acmctl_CtrlMtrProcessOutInfo.MotPosiAngle1_1_100Deg = input[24];
    Acmctl_CtrlMtrProcessOutInfo.MotPosiAngle2_1_100Deg = input[25];
    Acmctl_CtrlEcuModeSts = input[26];
    Acmctl_CtrlVdcLinkFild = input[27];
    Acmctl_CtrlMotCtrlMode = input[28];
    Acmctl_CtrlMotCtrlState = input[29];
    Acmctl_CtrlFuncInhibitAcmctlSts = input[30];
    Acmctl_CtrlMtrArbDriveState = input[31];

    Acmctl_Ctrl();


    output[0] = Acmctl_CtrlMotReqDataAcmctlInfo.MotPwmPhUData;
    output[1] = Acmctl_CtrlMotReqDataAcmctlInfo.MotPwmPhVData;
    output[2] = Acmctl_CtrlMotReqDataAcmctlInfo.MotPwmPhWData;
    output[3] = Acmctl_CtrlMotReqDataAcmctlInfo.MotReq;
    output[4] = Acmctl_CtrlMotDqIMeasdInfo.MotIqMeasd;
    output[5] = Acmctl_CtrlMotDqIMeasdInfo.MotIdMeasd;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Acmctl_Ctrl_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
