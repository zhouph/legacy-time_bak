/**
 * @defgroup MotorControllerLibrary_Sub MotorControllerLibrary_Sub
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        MotorControllerLibrary_Sub.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Acmctl_MotorControllerLibrary.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define F_1_DIV_SQRT3			(0.57735f)
#define F_2_DIV_3				(0.66667f)
#define F_1_DIV_3				(0.33333f)
#define F_SQRT3_2				(0.86603f)

#define F_MTR_VDCLINK_MIN 		(9.0f)

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ACMCTL_START_SEC_CONST_UNSPECIFIED
#include "Acmctl_MemMap.h"
/** Constant Section (UNSPECIFIED)**/



/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/


#define ACMCTL_STOP_SEC_CONST_UNSPECIFIED
#include "Acmctl_MemMap.h"

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACMCTL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmctl_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
#define ACMCTL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmctl_MemMap.h"

#define ACMCTL_START_SEC_VAR_NOINIT_32BIT
#include "Acmctl_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define ACMCTL_STOP_SEC_VAR_NOINIT_32BIT
#include "Acmctl_MemMap.h"

#define ACMCTL_START_SEC_VAR_UNSPECIFIED
#include "Acmctl_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define ACMCTL_STOP_SEC_VAR_UNSPECIFIED
#include "Acmctl_MemMap.h"

#define ACMCTL_START_SEC_VAR_32BIT
#include "Acmctl_MemMap.h"
/** Variable Section (32BIT)**/
#define ACMCTL_STOP_SEC_VAR_32BIT
#include "Acmctl_MemMap.h"

/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACMCTL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmctl_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
#define ACMCTL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmctl_MemMap.h"

#define ACMCTL_START_SEC_VAR_NOINIT_32BIT
#include "Acmctl_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define ACMCTL_STOP_SEC_VAR_NOINIT_32BIT
#include "Acmctl_MemMap.h"

#define ACMCTL_START_SEC_VAR_UNSPECIFIED
#include "Acmctl_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define ACMCTL_STOP_SEC_VAR_UNSPECIFIED
#include "Acmctl_MemMap.h"

#define ACMCTL_START_SEC_VAR_32BIT
#include "Acmctl_MemMap.h"
/** Variable Section (32BIT)**/
#define ACMCTL_STOP_SEC_VAR_32BIT
#include "Acmctl_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ACMCTL_START_SEC_CODE
#include "Acmctl_MemMap.h"
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
const static Mivt_Frac16 sin0To90DegArray[901] = {
		0,57,114,171,228,285,343,400,457,514,
		571,629,686,743,800,857,914,972,1029,1086,
		1143,1200,1257,1315,1372,1429,1486,1543,1600,1657,
		1714,1772,1829,1886,1943,2000,2057,2114,2171,2228,
		2285,2342,2399,2456,2513,2570,2627,2684,2741,2798,
		2855,2912,2969,3026,3083,3140,3197,3254,3311,3368,
		3425,3482,3538,3595,3652,3709,3766,3823,3879,3936,
		3993,4050,4106,4163,4220,4277,4333,4390,4447,4503,
		4560,4617,4673,4730,4786,4843,4899,4956,5013,5069,
		5126,5182,5238,5295,5351,5408,5464,5521,5577,5633,
		5690,5746,5802,5858,5915,5971,6027,6083,6140,6196,
		6252,6308,6364,6420,6476,6532,6588,6644,6700,6756,
		6812,6868,6924,6980,7036,7092,7148,7203,7259,7315,
		7371,7426,7482,7538,7593,7649,7705,7760,7816,7871,
		7927,7982,8038,8093,8149,8204,8259,8315,8370,8425,
		8480,8536,8591,8646,8701,8756,8811,8867,8922,8977,
		9032,9087,9141,9196,9251,9306,9361,9416,9470,9525,
		9580,9635,9689,9744,9798,9853,9908,9962,10017,10071,
		10125,10180,10234,10288,10343,10397,10451,10505,10560,10614,
		10668,10722,10776,10830,10884,10938,10992,11045,11099,11153,
		11207,11261,11314,11368,11422,11475,11529,11582,11636,11689,
		11743,11796,11849,11903,11956,12009,12062,12115,12168,12222,
		12275,12328,12381,12434,12486,12539,12592,12645,12698,12750,
		12803,12856,12908,12961,13013,13066,13118,13171,13223,13275,
		13327,13380,13432,13484,13536,13588,13640,13692,13744,13796,
		13848,13900,13951,14003,14055,14106,14158,14210,14261,14313,
		14364,14415,14467,14518,14569,14621,14672,14723,14774,14825,
		14876,14927,14978,15029,15079,15130,15181,15231,15282,15333,
		15383,15434,15484,15534,15585,15635,15685,15735,15786,15836,
		15886,15936,15986,16036,16085,16135,16185,16235,16284,16334,
		16383,16433,16482,16532,16581,16631,16680,16729,16778,16827,
		16876,16925,16974,17023,17072,17121,17169,17218,17267,17315,
		17364,17412,17461,17509,17557,17606,17654,17702,17750,17798,
		17846,17894,17942,17990,18038,18085,18133,18181,18228,18276,
		18323,18371,18418,18465,18512,18559,18607,18654,18701,18748,
		18794,18841,18888,18935,18981,19028,19075,19121,19167,19214,
		19260,19306,19352,19399,19445,19491,19537,19582,19628,19674,
		19720,19765,19811,19857,19902,19947,19993,20038,20083,20128,
		20173,20219,20264,20308,20353,20398,20443,20487,20532,20577,
		20621,20665,20710,20754,20798,20843,20887,20931,20975,21019,
		21062,21106,21150,21194,21237,21281,21324,21367,21411,21454,
		21497,21540,21583,21626,21669,21712,21755,21798,21840,21883,
		21926,21968,22010,22053,22095,22137,22179,22221,22263,22305,
		22347,22389,22431,22472,22514,22556,22597,22638,22680,22721,
		22762,22803,22844,22885,22926,22967,23008,23048,23089,23129,
		23170,23210,23251,23291,23331,23371,23411,23451,23491,23531,
		23571,23611,23650,23690,23729,23769,23808,23847,23886,23925,
		23964,24003,24042,24081,24120,24159,24197,24236,24274,24313,
		24351,24389,24427,24465,24503,24541,24579,24617,24655,24692,
		24730,24767,24805,24842,24879,24916,24954,24991,25028,25064,
		25101,25138,25175,25211,25248,25284,25320,25357,25393,25429,
		25465,25501,25537,25573,25608,25644,25680,25715,25750,25786,
		25821,25856,25891,25926,25961,25996,26031,26066,26100,26135,
		26169,26204,26238,26272,26306,26340,26374,26408,26442,26476,
		26509,26543,26576,26610,26643,26676,26710,26743,26776,26809,
		26841,26874,26907,26940,26972,27004,27037,27069,27101,27133,
		27165,27197,27229,27261,27293,27324,27356,27387,27419,27450,
		27481,27512,27543,27574,27605,27636,27666,27697,27728,27758,
		27788,27819,27849,27879,27909,27939,27969,27998,28028,28058,
		28087,28117,28146,28175,28204,28233,28262,28291,28320,28349,
		28377,28406,28434,28463,28491,28519,28547,28575,28603,28631,
		28659,28687,28714,28742,28769,28797,28824,28851,28878,28905,
		28932,28959,28985,29012,29039,29065,29091,29118,29144,29170,
		29196,29222,29248,29273,29299,29325,29350,29376,29401,29426,
		29451,29476,29501,29526,29551,29575,29600,29624,29649,29673,
		29697,29722,29746,29769,29793,29817,29841,29864,29888,29911,
		29935,29958,29981,30004,30027,30050,30072,30095,30118,30140,
		30163,30185,30207,30229,30251,30273,30295,30317,30338,30360,
		30381,30403,30424,30445,30466,30487,30508,30529,30550,30571,
		30591,30612,30632,30652,30672,30692,30712,30732,30752,30772,
		30791,30811,30830,30850,30869,30888,30907,30926,30945,30964,
		30982,31001,31019,31038,31056,31074,31092,31110,31128,31146,
		31164,31181,31199,31216,31234,31251,31268,31285,31302,31319,
		31336,31352,31369,31385,31402,31418,31434,31450,31466,31482,
		31498,31514,31529,31545,31560,31576,31591,31606,31621,31636,
		31651,31666,31680,31695,31709,31724,31738,31752,31766,31780,
		31794,31808,31822,31835,31849,31862,31875,31889,31902,31915,
		31928,31940,31953,31966,31978,31991,32003,32015,32027,32040,
		32051,32063,32075,32087,32098,32110,32121,32132,32143,32154,
		32165,32176,32187,32198,32208,32219,32229,32239,32250,32260,
		32270,32280,32289,32299,32309,32318,32327,32337,32346,32355,
		32364,32373,32382,32390,32399,32408,32416,32424,32432,32441,
		32449,32457,32464,32472,32480,32487,32495,32502,32509,32516,
		32523,32530,32537,32544,32550,32557,32563,32570,32576,32582,
		32588,32594,32600,32605,32611,32617,32622,32627,32633,32638,
		32643,32648,32653,32657,32662,32666,32671,32675,32680,32684,
		32688,32692,32695,32699,32703,32706,32710,32713,32716,32720,
		32723,32726,32728,32731,32734,32736,32739,32741,32743,32745,
		32748,32749,32751,32753,32755,32756,32758,32759,32760,32761,
		32763,32763,32764,32765,32766,32766,32767,32767,32767,32767,
		32767
};

void Acmctl_vSinCos(fsc_t *pSinCos, sint16 s16x)
{
	static sint16 theta;

	if ((s16x >= 0) && (s16x <= 900))
	{
		theta = s16x;
		pSinCos->fsin = (float)sin0To90DegArray[theta];
	}
	else if ((s16x > 900) && (s16x <= 1800))
	{
		theta = 1800 - s16x;
		pSinCos->fsin = (float)sin0To90DegArray[theta];
	}
	else if ((s16x > 1800) && (s16x <= 2700))
	{
		theta = s16x - 1800;
		pSinCos->fsin = -(float)sin0To90DegArray[theta];
	}
	else if ((s16x > 2700) && (s16x <= 3600))
	{
		theta = 3600 - s16x;
		pSinCos->fsin = -(float)sin0To90DegArray[theta];
	}
	else
	{
		theta = 0;
		pSinCos->fsin = (float)sin0To90DegArray[theta];
	}

	if ((s16x >= 0) && (s16x <= 900))
	{
		theta = 900 - s16x;
		pSinCos->fcos = (float)sin0To90DegArray[theta];
	}
	else if ((s16x > 900) && (s16x <= 1800))
	{
		theta = s16x - 900;
		pSinCos->fcos = -(float)sin0To90DegArray[theta];
	}
	else if ((s16x > 1800) && (s16x <= 2700))
	{
		theta = 2700 - s16x;
		pSinCos->fcos = -(float)sin0To90DegArray[theta];
	}
	else if ((s16x > 2700) && (s16x <= 3600))
	{
		theta = s16x - 2700;
		pSinCos->fcos = (float)sin0To90DegArray[theta];
	}
	else
	{
		theta = 0;
		pSinCos->fcos = (float)sin0To90DegArray[theta];
	}

	pSinCos->fsin = pSinCos->fsin/32768.0f;
	pSinCos->fcos = pSinCos->fcos/32768.0f;
}

void Acmctl_vAbc2DqTrfm(fdq_t *pDq, fabc_t *pAbc, fsc_t *pSinCos)
{
	static float Xds, Xqs;
	Xds = F_2_DIV_3*pAbc->fa - F_1_DIV_3*(pAbc->fb + pAbc->fc);
	Xqs = F_1_DIV_SQRT3*(pAbc->fb - pAbc->fc);

	pDq->fd =  pSinCos->fcos*Xds + pSinCos->fsin*Xqs;
	pDq->fq = -pSinCos->fsin*Xds + pSinCos->fcos*Xqs;
}

void Acmctl_vInitfPi(fpi_t *pParam)
{
	pParam->fUp = 0.0f;
	pParam->fUi = 0.0f;

	pParam->fErr = 0.0f;
	pParam->fErrUi = 0.0f;

	pParam->fUdiff = 0.0f;
	pParam->fUout = 0.0f;
	pParam->fUcalc = 0.0f;
}

float Acmctl_fPIController(float fx_ref, float fx_mea, float fUff, fpi_t *pParam)
{
	/* calculate err */
	pParam->fErr = fx_ref - fx_mea;

	/* back-calculation */
	pParam->fUdiff = pParam->fUout - pParam->fUcalc;
	if (pParam->fKp > 0.0f)
	{
		pParam->fErrUi = pParam->fErr + pParam->fUdiff/pParam->fKp;
	}
	else
	{
		pParam->fErrUi = pParam->fErr;
	}

	/* calculate Up */
	pParam->fUp = pParam->fKp * pParam->fErr;

	/* calculate Ui */
	pParam->fUi = pParam->fUi + pParam->fKiTs*pParam->fErrUi;

	/* calculate Ucalc */
	pParam->fUcalc = pParam->fUp + pParam->fUi + fUff;

	return pParam->fUcalc;
}

void Acmctl_vVdq2VabcnTrfm(fabc_t *pVabcn, fdq_t *pVdq, fsc_t *pSinCos, float fVdc)
{
	static float fVmax, fVmin, fVsn, fVds, fVqs, fVdcHalf;
	static fabc_t Vabcs;

	fVds = pSinCos->fcos*pVdq->fd - pSinCos->fsin*pVdq->fq;
	fVqs = pSinCos->fsin*pVdq->fd + pSinCos->fcos*pVdq->fq;

	Vabcs.fa = fVds;
	Vabcs.fb = -0.5f*fVds + F_SQRT3_2*fVqs;
	Vabcs.fc = -0.5f*fVds - F_SQRT3_2*fVqs;

	fVdcHalf = 0.5f*fVdc;

	if (Vabcs.fa > Vabcs.fb)
	{
		fVmax = Vabcs.fa;
		fVmin = Vabcs.fb;
	}
	else
	{
		fVmax = Vabcs.fb;
		fVmin = Vabcs.fa;
	}

	if (Vabcs.fc > fVmax)
	{
		fVmax = Vabcs.fc;
	}

	if (Vabcs.fc < fVmin)
	{
		fVmin = Vabcs.fc;
	}

	fVsn = -0.5f*(fVmax + fVmin);
	pVabcn->fa = Vabcs.fa + fVsn;
	pVabcn->fb = Vabcs.fb + fVsn;
	pVabcn->fc = Vabcs.fc + fVsn;

	if (pVabcn->fa > fVdcHalf)
	{
		pVabcn->fa = fVdcHalf;
	}
	else if (pVabcn->fa < -fVdcHalf)
	{
		pVabcn->fa = -fVdcHalf;
	}
	else
	{
		;
	}

	if (pVabcn->fb > fVdcHalf)
	{
		pVabcn->fb = fVdcHalf;
	}
	else if (pVabcn->fb < -fVdcHalf)
	{
		pVabcn->fb = -fVdcHalf;
	}
	else
	{
		;
	}

	if (pVabcn->fc > fVdcHalf)
	{
		pVabcn->fc = fVdcHalf;
	}
	else if (pVabcn->fc < -fVdcHalf)
	{
		pVabcn->fc = -fVdcHalf;
	}
	else
	{
		;
	}
}

void Acmctl_vCalcDuty3(fabc_t *pDuty, fabc_t *pPoleVolt, float fVdcLink, float fDutyMax)
{
	static float fVdc;

	if (fVdcLink < F_MTR_VDCLINK_MIN)
	{
		fVdc = F_MTR_VDCLINK_MIN;
	}
	else
	{
		fVdc = fVdcLink;
	}

	pDuty->fa = (0.5f + pPoleVolt->fa/fVdc)*fDutyMax;
	if(pDuty->fa > fDutyMax)
	{
		pDuty->fa = fDutyMax;
	}
	else if(pDuty->fa < 0.0f)
	{
		pDuty->fa = 0.0f;
	}
	else
	{
		;
	}

	pDuty->fb = (0.5f + pPoleVolt->fb/fVdc)*fDutyMax;
	if(pDuty->fb > fDutyMax)
	{
		pDuty->fb = fDutyMax;
	}
	else if(pDuty->fb < 0.0f)
	{
		pDuty->fb = 0.0f;
	}
	else
	{
		;
	}

	pDuty->fc = (0.5f + pPoleVolt->fc/fVdc)*fDutyMax;
	if(pDuty->fc > fDutyMax)
	{
		pDuty->fc = fDutyMax;
	}
	else if(pDuty->fc < 0.0f)
	{
		pDuty->fc = 0.0f;
	}
	else
	{
		;
	}
}

//fixed
#define S16_MTR_VDCLINK_MAX		(15000)
#define S16_MTR_VDCLINK_MIN		(9000)		/* should be reviewed */

void MCLIB_ClarkTrfmInv(MC_3PhSyst *p_abc, MC_2PhSyst *pAlphaBeta)
{
	Mivt_Frac32 fr32Temp0 = (((Mivt_Frac32)pAlphaBeta->beta) * HALF_SQRT3) >> 15;
	Mivt_Frac32 fr32Temp1 = 0;

	p_abc->a = pAlphaBeta->alpha;

	fr32Temp1 = -(((Mivt_Frac32)pAlphaBeta->alpha) >>1) + fr32Temp0;
	if (fr32Temp1 > S16_MTR_MAX)
	{
		p_abc->b = S16_MTR_MAX;
	}
	else if (fr32Temp1 < S16_MTR_MIN)
	{
		p_abc->b = S16_MTR_MIN;
	}
	else
	{
		p_abc->b = (Mivt_Frac16)fr32Temp1;
	}

	fr32Temp1 = -(((Mivt_Frac32)pAlphaBeta->alpha) >>1) - fr32Temp0;
	if (fr32Temp1 > S16_MTR_MAX)
	{
		p_abc->c = S16_MTR_MAX;
	}
	else if (fr32Temp1 < S16_MTR_MIN)
	{
		p_abc->c = S16_MTR_MIN;
	}
	else
	{
		p_abc->c = (Mivt_Frac16)fr32Temp1;
	}
}


Mivt_Frac16 Acmctl_f16Sin2(sint16 x)
{
	Mivt_Frac16 y = 0;
	sint16 theta = 0;

	if ((x >= 0) && (x <= 900))
	{
		theta = x;
		y = sin0To90DegArray[theta];
	}
	else if ((x > 900) && (x <= 1800))
	{
		theta = 1800 - x;
		y = sin0To90DegArray[theta];
	}
	else if ((x > 1800) && (x <= 2700))
	{
		theta = x - 1800;
		y = -sin0To90DegArray[theta];
	}
	else if ((x > 2700) && (x <= 3600))
	{
		theta = 3600 - x;
		y = -sin0To90DegArray[theta];
	}
	else
	{
		theta = 0;
		y = sin0To90DegArray[theta];
	}

	return y;
}

Mivt_Frac16 Acmctl_f16Cos2(sint16 x)
{
	Mivt_Frac16 y = 0;
	sint16 theta = 0;

	if ((x >= 0) && (x <= 900))
	{
		theta = 900 - x;
		y = sin0To90DegArray[theta];
	}
	else if ((x > 900) && (x <= 1800))
	{
		theta = x - 900;
		y = -sin0To90DegArray[theta];
	}
	else if ((x > 1800) && (x <= 2700))
	{
		theta = 2700 - x;
		y = -sin0To90DegArray[theta];
	}
	else if ((x > 2700) && (x <= 3600))
	{
		theta = x - 2700;
		y = sin0To90DegArray[theta];
	}
	else
	{
		theta = 0;
		y = sin0To90DegArray[theta];
	}

	return y;
}

void MCLIB_ClarkTrfm( MC_2PhSyst *pAlphaBeta, MC_3PhSyst *p_abc )
{
	register Mivt_Frac32 fr32Temp;
	pAlphaBeta->alpha = p_abc->a;
	fr32Temp = p_abc-> b * ONE_DIV_SQRT3;
	fr32Temp = ( (Mivt_Frac32)p_abc->a * ONE_DIV_SQRT3 + fr32Temp*2 )>>15;


	if     (fr32Temp> 32767)	{	pAlphaBeta->beta =  32767;		}
	else if(fr32Temp<-32767) 	{	pAlphaBeta->beta = -32767;		}
	else                        {   pAlphaBeta->beta = (Mivt_Frac16)fr32Temp;}


}

void MCLIB_ClarkTrfm2( MC_2PhSyst *pAlphaBeta, MC_3PhSyst *p_abc )
{
	Mivt_Frac32 fr32Temp = 0;
	fr32Temp = (2*p_abc->a - p_abc->b - p_abc->c)/3;
	if     (fr32Temp> 32767)	{	pAlphaBeta->alpha =  32767;		}
	else if(fr32Temp<-32767) 	{	pAlphaBeta->alpha = -32767;		}
	else                        {   pAlphaBeta->alpha = (Mivt_Frac16)fr32Temp;}

	fr32Temp = ONE_DIV_SQRT3*(p_abc->b - p_abc->c) >> 15;
	if     (fr32Temp> 32767)	{	pAlphaBeta->beta =  32767;		}
	else if(fr32Temp<-32767) 	{	pAlphaBeta->beta = -32767;		}
	else                        {   pAlphaBeta->beta = (Mivt_Frac16)fr32Temp;}
}


void MCLIB_ParkTrfm( MC_DqSyst *pDQ, MC_2PhSyst *pAlphaBeta, MC_Angle *pSinCos )
{
	register Mivt_Frac32 fr32Temp;
	fr32Temp = ( pAlphaBeta->alpha * pSinCos->cos  +  pAlphaBeta->beta * pSinCos->sin ) >> 15 ;


	if     (fr32Temp> 32767)	{	pDQ -> d =  32767;		}
	else if(fr32Temp<-32767) 	{	pDQ -> d = -32767;		}
	else                        {   pDQ -> d = (Mivt_Frac16)fr32Temp;}

	fr32Temp = ( pAlphaBeta->beta  * pSinCos->cos  -  pAlphaBeta->alpha* pSinCos->sin ) >> 15 ;
	if     (fr32Temp> 32767)	{	pDQ -> q =  32767;		}
	else if(fr32Temp<-32767) 	{	pDQ -> q = -32767;		}
	else                        {   pDQ -> q = (Mivt_Frac16)fr32Temp;}
}


void MCLIB_ParkTrfmInv( MC_2PhSyst *pAlphaBeta, MC_DqSyst *pDQ, MC_Angle *pSinCos )
{
	register Mivt_Frac32 fr32Temp;
	fr32Temp = ( pDQ->d * pSinCos->cos -  pDQ -> q * pSinCos -> sin ) >> 15 ;
	if     (fr32Temp> 32767)	{	pAlphaBeta->alpha =  32767;		}
	else if(fr32Temp<-32767) 	{	pAlphaBeta->alpha = -32767;		}
	else                        {   pAlphaBeta->alpha = (Mivt_Frac16)fr32Temp;}

	fr32Temp = ( pDQ->d * pSinCos->sin +  pDQ -> q * pSinCos -> cos ) >> 15 ;
	if     (fr32Temp> 32767)	{	pAlphaBeta->beta =  32767;		}
	else if(fr32Temp<-32767) 	{	pAlphaBeta->beta = -32767;		}
	else                        {   pAlphaBeta->beta = (Mivt_Frac16)fr32Temp;}
}

void Acmctl_vInitPI3(pi3_t *pParam)
{
	/* NON-SETTING PARAMETERS */
	pParam->s32Up = 0 ;
	pParam->s32Ui = 0;
	pParam->s32Uff = 0;

	pParam->s32Err = 0;
	pParam->s32ErrUi = 0;

	pParam->s16Uout = 0;
	pParam->s32Ucalc = 0;
	pParam->s32Udiff = 0;
	/* NON-SETTING PARAMETERS */
}

void MCLIB_CalcDuty(MC_3PhSyst *p_abc, MC_3PhSyst *pPoleVolt, sint16 VdcLink, sint16 DutyMax)
{
	static sint32 s32Temp0;
	static Mivt_Frac16 DutyMaxHalf;

	DutyMaxHalf = DutyMax/2;

	if (VdcLink > S16_MTR_VDCLINK_MIN)
	{
		s32Temp0 = DutyMaxHalf + (sint32)(pPoleVolt->a) * DutyMax / VdcLink;
		if(s32Temp0 > DutyMax)
		{
			p_abc->a = DutyMax;
		}
		else if(s32Temp0 < 0)
		{
			p_abc->a = 0;
		}
		else
		{
			p_abc->a = (sint16)s32Temp0;
		}

		s32Temp0 = DutyMaxHalf + (sint32)(pPoleVolt->b) * DutyMax / VdcLink;
		if(s32Temp0 > DutyMax)
		{
			p_abc->b = DutyMax;
		}
		else if(s32Temp0 < 0)
		{
			p_abc->b = 0;
		}
		else
		{
			p_abc->b = (sint16)s32Temp0;
		}

		s32Temp0 = DutyMaxHalf + (sint32)(pPoleVolt->c) * DutyMax / VdcLink;
		if(s32Temp0 > DutyMax)
		{
			p_abc->c = DutyMax;
		}
		else if(s32Temp0 < 0)
		{
			p_abc->c = 0;
		}
		else
		{
			p_abc->c = (sint16)s32Temp0;
		}
	}
	else
	{
		s32Temp0 = DutyMaxHalf + (sint32)(pPoleVolt->a) * DutyMax / S16_MTR_VDCLINK_MIN;
		if(s32Temp0 > DutyMax)
		{
			p_abc->a = DutyMax;
		}
		else if(s32Temp0 < 0)
		{
			p_abc->a = 0;
		}
		else
		{
			p_abc->a = (sint16)s32Temp0;
		}

		s32Temp0 = DutyMaxHalf + (sint32)(pPoleVolt->b) * DutyMax / S16_MTR_VDCLINK_MIN;
		if(s32Temp0 > DutyMax)
		{
			p_abc->b = DutyMax;
		}
		else if(s32Temp0 < 0)
		{
			p_abc->b = 0;
		}
		else
		{
			p_abc->b = (sint16)s32Temp0;
		}

		s32Temp0 = DutyMaxHalf + (sint32)(pPoleVolt->c) * DutyMax / S16_MTR_VDCLINK_MIN;
		if(s32Temp0 > DutyMax)
		{
			p_abc->c = DutyMax;
		}
		else if(s32Temp0 < 0)
		{
			p_abc->c = 0;
		}
		else
		{
			p_abc->c = (sint16)s32Temp0;
		}
	}
}

void MCLIB_CalcVout(MC_3PhSyst *pPoleVolt, MC_3PhSyst *pPhaseVolt, sint16 Vdclink)
{
	static sint16 VdcHalf;
	static sint16 Vmax;
	static sint16 Vmin;
	static sint16 Vsn;
	static sint32 Van;
	static sint32 Vbn;
	static sint32 Vcn;

	VdcHalf = Vdclink/2;

	if ((pPhaseVolt->a) > (pPhaseVolt->b))
	{
		Vmax = pPhaseVolt->a;
		Vmin = pPhaseVolt->b;
	}
	else
	{
		Vmax = pPhaseVolt->b;
		Vmin = pPhaseVolt->a;
	}

	if ((pPhaseVolt->c) > Vmax)
	{
		Vmax = pPhaseVolt->c;
	}

	if ((pPhaseVolt->c) < Vmin)
	{
		Vmin = pPhaseVolt->c;
	}

	Vsn = -(Vmax + Vmin)/2;

	Van = (sint32)(pPhaseVolt->a) + (sint32)Vsn;
	if (Van > VdcHalf)
	{
		pPoleVolt->a = VdcHalf;
	}
	else if (Van < -VdcHalf)
	{
		pPoleVolt->a = -VdcHalf;
	}
	else
	{
		pPoleVolt->a = (sint16)Van;
	}

	Vbn = (sint32)(pPhaseVolt->b) + (sint32)Vsn;
	if (Vbn > VdcHalf)
	{
		pPoleVolt->b = VdcHalf;
	}
	else if (Vbn < -VdcHalf)
	{
		pPoleVolt->b = -VdcHalf;
	}
	else
	{
		pPoleVolt->b = (sint16)Vbn;
	}

	Vcn = (sint32)(pPhaseVolt->c) + (sint32)Vsn;
	if (Vcn > VdcHalf)
	{
		pPoleVolt->c = VdcHalf;
	}
	else if (Vcn < -VdcHalf)
	{
		pPoleVolt->c = -VdcHalf;
	}
	else
	{
		pPoleVolt->c = (sint16)Vcn;
	}
}

sint16 Acmctl_s16PIController3(sint16 xRef, sint16 x, sint16 Uff, pi3_t *pParam)
{
	static sint32 s32Tmp0;
	static sint32 s32Tmp1;


	/* calculate err */
	pParam->s32Err = xRef - x;
	if (pParam->s32Err > S16_MTR_MAX)
	{
		pParam->s32Err = S16_MTR_MAX;
	}
	else if (pParam->s32Err < S16_MTR_MIN)
	{
		pParam->s32Err = S16_MTR_MIN;
	}
	else
	{
		;
	}
	/* calculate err */


	/* back-calculation */
	pParam->s32Udiff = (sint32)pParam->s16Uout - pParam->s32Ucalc;
	if (pParam->s32Udiff > S16_MTR_MAX)
	{
		pParam->s32Udiff = S16_MTR_MAX;
	}
	else if (pParam->s32Udiff < S16_MTR_MIN)
	{
		pParam->s32Udiff = S16_MTR_MIN;
	}
	else
	{
		;
	}

	s32Tmp0 = pParam->s16Kp*Mivt_F16_1 + pParam->fr16Kp;
	if ((s32Tmp0 != 0) && (pParam->s32Udiff != 0))
	{
		s32Tmp1 = (pParam->s32Udiff*Mivt_F16_1)/s32Tmp0;
		pParam->s32ErrUi = pParam->s32Err + s32Tmp1;
		if (pParam->s32ErrUi > S16_MTR_MAX)
		{
			pParam->s32ErrUi = S16_MTR_MAX;
		}
		else if (pParam->s32ErrUi < S16_MTR_MIN)
		{
			pParam->s32ErrUi = S16_MTR_MIN;
		}
		else
		{
			;
		}
	}
	else
	{
		pParam->s32ErrUi = pParam->s32Err;
	}
	/* back-calculation */


	/* calculate Up */
	pParam->s32Up = (sint32)pParam->s16Kp * pParam->s32Err + pParam->fr16Kp*pParam->s32Err/Mivt_F16_1;
	if (pParam->s32Up > S16_MTR_MAX)
	{
		pParam->s32Up = S16_MTR_MAX;
	}
	else if (pParam->s32Up < S16_MTR_MIN)
	{
		pParam->s32Up = S16_MTR_MIN;
	}
	else
	{
		;
	}
	/* calculate Up */


	/* calculate Ui */
	s32Tmp0 = pParam->fr16KiTs*pParam->s32ErrUi;
	s32Tmp1 = s32Tmp0/Mivt_F16_1;
	if ((s32Tmp0 != 0) && (s32Tmp1 == 0))
	{
		if (s32Tmp0 > 0)
		{
			pParam->s32Ui = pParam->s32Ui + (sint32)pParam->s16KiTs*(sint32)pParam->s32ErrUi + 1;
		}
		else if (s32Tmp0 < 0)
		{
			pParam->s32Ui = pParam->s32Ui + (sint32)pParam->s16KiTs*(sint32)pParam->s32ErrUi - 1;
		}
		else
		{
			pParam->s32Ui = pParam->s32Ui + (sint32)pParam->s16KiTs*(sint32)pParam->s32ErrUi;
		}
	}
	else
	{
		pParam->s32Ui = pParam->s32Ui + (sint32)pParam->s16KiTs*(sint32)pParam->s32ErrUi + s32Tmp1;
	}
	if (pParam->s32Ui > S16_MTR_MAX)
	{
		pParam->s32Ui = S16_MTR_MAX;
	}
	else if (pParam->s32Ui < S16_MTR_MIN)
	{
		pParam->s32Ui = S16_MTR_MIN;
	}
	else
	{
		;
	}
	/* calculate Ui */


	/* Uff */
	pParam->s32Uff = (sint32)Uff;
	if (pParam->s32Uff > S16_MTR_MAX)
	{
		pParam->s32Uff = S16_MTR_MAX;
	}
	else if (pParam->s32Uff < S16_MTR_MIN)
	{
		pParam->s32Uff = S16_MTR_MIN;
	}
	else
	{
		;
	}


	/* constrain DeltaUout */
	pParam->s32Ucalc = pParam->s32Up + pParam->s32Ui + pParam->s32Uff;
	if (pParam->s32Ucalc > S16_MTR_MAX)
	{
		pParam->s32Ucalc = S16_MTR_MAX;
	}
	else if (pParam->s32Ucalc < S16_MTR_MIN)
	{
		pParam->s32Ucalc = S16_MTR_MIN;
	}
	else
	{
		;
	}

	return (sint16)(pParam->s32Ucalc);
}

void Acmctl_vPiGainChanger(pi3_t *pPi, pi3_t *pPi_fin,pi3_t *pPi_ini , sint16 s16Step)
{
	static sint32 s32dKp;
	static sint32 s32dKiTs;

	static sint32 s32Kp;
	static sint32 s32Kp_fin;
	static sint32 s32Kp_ini;

	static sint32 s32KiTs;
	static sint32 s32KiTs_fin;
	static sint32 s32KiTs_ini;

	if (s16Step > 0)
	{
		s32Kp = (sint32)pPi->s16Kp*Mivt_F16_1 + (sint32)pPi->fr16Kp;
		s32Kp_fin = (sint32)pPi_fin->s16Kp*Mivt_F16_1 + (sint32)pPi_fin->fr16Kp;
		s32Kp_ini = (sint32)pPi_ini->s16Kp*Mivt_F16_1 + (sint32)pPi_ini->fr16Kp;

		s32KiTs = (sint32)pPi->s16KiTs*Mivt_F16_1 + (sint32)pPi->fr16KiTs;
		s32KiTs_fin = (sint32)pPi_fin->s16KiTs*Mivt_F16_1 + (sint32)pPi_fin->fr16KiTs;
		s32KiTs_ini = (sint32)pPi_ini->s16KiTs*Mivt_F16_1 + (sint32)pPi_ini->fr16KiTs;

		s32dKp = (s32Kp_fin - s32Kp_ini)/s16Step;
		s32dKiTs = (s32KiTs_fin - s32KiTs_ini)/s16Step;

		s32Kp = s32Kp + s32dKp;
		if (s32dKp >= 0)
		{
			if (s32Kp > s32Kp_fin)
			{
				s32Kp = s32Kp_fin;
			}
		}
		else
		{
			if (s32Kp < s32Kp_fin)
			{
				s32Kp = s32Kp_fin;
			}
		}

		s32KiTs = s32KiTs + s32dKiTs;
		if (s32dKiTs >= 0)
		{
			if (s32KiTs > s32KiTs_fin)
			{
				s32KiTs = s32KiTs_fin;
			}
		}
		else
		{
			if (s32KiTs < s32KiTs_fin)
			{
				s32KiTs = s32KiTs_fin;
			}
		}

		pPi->s16Kp = (sint16)(s32Kp/Mivt_F16_1);
		pPi->fr16Kp = (Mivt_Frac16)(s32Kp - (sint32)pPi->s16Kp*Mivt_F16_1);

		pPi->s16KiTs = (sint16)(s32KiTs/Mivt_F16_1);
		pPi->fr16KiTs = (Mivt_Frac16)(s32KiTs - (sint32)pPi->s16KiTs*Mivt_F16_1);
	}
}


/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/


#define Acmctl_STOP_SEC_CODE
#include "Acmctl_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
