/**
 * @defgroup Acmctl_Ctrl Acmctl_Ctrl
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Acmctl_Ctrl.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Acmctl_Ctrl.h"
#include "Acmctl_Ctrl_Ifa.h"
#include "Acmctl_ControlCurrent.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define SAL_MOT_IVTR_DUTY_SCALE     (2)         // resl. : 0.02 --> 0.01
/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ACMCTL_CTRL_START_SEC_CONST_UNSPECIFIED
#include "Acmctl_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ACMCTL_CTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Acmctl_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACMCTL_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmctl_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Acmctl_Ctrl_HdrBusType Acmctl_CtrlBus;

/* Version Info */
const SwcVersionInfo_t Acmctl_CtrlVersionInfo = 
{   
    ACMCTL_CTRL_MODULE_ID,           /* Acmctl_CtrlVersionInfo.ModuleId */
    ACMCTL_CTRL_MAJOR_VERSION,       /* Acmctl_CtrlVersionInfo.MajorVer */
    ACMCTL_CTRL_MINOR_VERSION,       /* Acmctl_CtrlVersionInfo.MinorVer */
    ACMCTL_CTRL_PATCH_VERSION,       /* Acmctl_CtrlVersionInfo.PatchVer */
    ACMCTL_CTRL_BRANCH_VERSION       /* Acmctl_CtrlVersionInfo.BranchVer */
};
    
/* Input Data Element */
Mcc_1msCtrlMotDqIRefMccInfo_t Acmctl_CtrlMotDqIRefMccInfo;
Acmio_SenMotCurrInfo_t Acmctl_CtrlMotCurrInfo;
Msp_CtrlMotRotgAgSigInfo_t Acmctl_CtrlMotRotgAgSigInfo;
Ses_CtrlMotOrgSetStInfo_t Acmctl_CtrlMotOrgSetStInfo;
Ses_CtrlMotDqIRefSesInfo_t Acmctl_CtrlMotDqIRefSesInfo;
Mcc_1msCtrlFluxWeakengStInfo_t Acmctl_CtrlFluxWeakengStInfo;
Arbitrator_MtrMtrArbitratorData_t Acmctl_CtrlMtrArbtratorData;
Arbitrator_MtrMtrArbitratorInfo_t Acmctl_CtrlMtrArbtratorInfo;
Mtr_Processing_SigMtrProcessOutInfo_t Acmctl_CtrlMtrProcessOutInfo;
Mom_HndlrEcuModeSts_t Acmctl_CtrlEcuModeSts;
Msp_1msCtrlVdcLinkFild_t Acmctl_CtrlVdcLinkFild;
Mcc_1msCtrlMotCtrlMode_t Acmctl_CtrlMotCtrlMode;
Mcc_1msCtrlMotCtrlState_t Acmctl_CtrlMotCtrlState;
Eem_SuspcDetnFuncInhibitAcmctlSts_t Acmctl_CtrlFuncInhibitAcmctlSts;
Arbitrator_MtrMtrArbDriveState_t Acmctl_CtrlMtrArbDriveState;

/* Output Data Element */
Acmctl_CtrlMotReqDataAcmctlInfo_t Acmctl_CtrlMotReqDataAcmctlInfo;
Acmctl_CtrlMotDqIMeasdInfo_t Acmctl_CtrlMotDqIMeasdInfo;

uint32 Acmctl_Ctrl_Timer_Start;
uint32 Acmctl_Ctrl_Timer_Elapsed;

#define ACMCTL_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmctl_MemMap.h"
#define ACMCTL_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Acmctl_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACMCTL_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Acmctl_MemMap.h"
#define ACMCTL_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Acmctl_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACMCTL_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Acmctl_MemMap.h"
#define ACMCTL_CTRL_START_SEC_VAR_32BIT
#include "Acmctl_MemMap.h"
/** Variable Section (32BIT)**/


#define ACMCTL_CTRL_STOP_SEC_VAR_32BIT
#include "Acmctl_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACMCTL_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmctl_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ACMCTL_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmctl_MemMap.h"
#define ACMCTL_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Acmctl_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACMCTL_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Acmctl_MemMap.h"
#define ACMCTL_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Acmctl_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACMCTL_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Acmctl_MemMap.h"
#define ACMCTL_CTRL_START_SEC_VAR_32BIT
#include "Acmctl_MemMap.h"
/** Variable Section (32BIT)**/


#define ACMCTL_CTRL_STOP_SEC_VAR_32BIT
#include "Acmctl_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ACMCTL_CTRL_START_SEC_CODE
#include "Acmctl_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Acmctl_Ctrl_Init(void)
{
    /* Initialize internal bus */
    Acmctl_CtrlBus.Acmctl_CtrlMotDqIRefMccInfo.IdRef = 0;
    Acmctl_CtrlBus.Acmctl_CtrlMotDqIRefMccInfo.IqRef = 0;
    Acmctl_CtrlBus.Acmctl_CtrlMotCurrInfo.MotCurrPhUMeasd = 0;
    Acmctl_CtrlBus.Acmctl_CtrlMotCurrInfo.MotCurrPhVMeasd = 0;
    Acmctl_CtrlBus.Acmctl_CtrlMotCurrInfo.MotCurrPhWMeasd = 0;
    Acmctl_CtrlBus.Acmctl_CtrlMotRotgAgSigInfo.MotElecAngleFild = 0;
    Acmctl_CtrlBus.Acmctl_CtrlMotOrgSetStInfo.MotOrgSet = 0;
    Acmctl_CtrlBus.Acmctl_CtrlMotDqIRefSesInfo.IdRef = 0;
    Acmctl_CtrlBus.Acmctl_CtrlMotDqIRefSesInfo.IqRef = 0;
    Acmctl_CtrlBus.Acmctl_CtrlFluxWeakengStInfo.FluxWeakengFlg = 0;
    Acmctl_CtrlBus.Acmctl_CtrlFluxWeakengStInfo.FluxWeakengFlgOld = 0;
    Acmctl_CtrlBus.Acmctl_CtrlMtrArbtratorData.IqSelected = 0;
    Acmctl_CtrlBus.Acmctl_CtrlMtrArbtratorData.IdSelected = 0;
    Acmctl_CtrlBus.Acmctl_CtrlMtrArbtratorData.UPhasePWMSelected = 0;
    Acmctl_CtrlBus.Acmctl_CtrlMtrArbtratorData.VPhasePWMSelected = 0;
    Acmctl_CtrlBus.Acmctl_CtrlMtrArbtratorData.WPhasePWMSelected = 0;
    Acmctl_CtrlBus.Acmctl_CtrlMtrArbtratorData.MtrCtrlMode = 0;
    Acmctl_CtrlBus.Acmctl_CtrlMtrArbtratorInfo.MtrArbCalMode = 0;
    Acmctl_CtrlBus.Acmctl_CtrlMtrArbtratorInfo.MtrArbState = 0;
    Acmctl_CtrlBus.Acmctl_CtrlMtrProcessOutInfo.MotCurrPhUMeasd = 0;
    Acmctl_CtrlBus.Acmctl_CtrlMtrProcessOutInfo.MotCurrPhVMeasd = 0;
    Acmctl_CtrlBus.Acmctl_CtrlMtrProcessOutInfo.MotCurrPhWMeasd = 0;
    Acmctl_CtrlBus.Acmctl_CtrlMtrProcessOutInfo.MotPosiAngle1raw = 0;
    Acmctl_CtrlBus.Acmctl_CtrlMtrProcessOutInfo.MotPosiAngle2raw = 0;
    Acmctl_CtrlBus.Acmctl_CtrlMtrProcessOutInfo.MotPosiAngle1_1_100Deg = 0;
    Acmctl_CtrlBus.Acmctl_CtrlMtrProcessOutInfo.MotPosiAngle2_1_100Deg = 0;
    Acmctl_CtrlBus.Acmctl_CtrlEcuModeSts = 0;
    Acmctl_CtrlBus.Acmctl_CtrlVdcLinkFild = 0;
    Acmctl_CtrlBus.Acmctl_CtrlMotCtrlMode = 0;
    Acmctl_CtrlBus.Acmctl_CtrlMotCtrlState = 0;
    Acmctl_CtrlBus.Acmctl_CtrlFuncInhibitAcmctlSts = 0;
    Acmctl_CtrlBus.Acmctl_CtrlMtrArbDriveState = 0;
    Acmctl_CtrlBus.Acmctl_CtrlMotReqDataAcmctlInfo.MotPwmPhUData = 0;
    Acmctl_CtrlBus.Acmctl_CtrlMotReqDataAcmctlInfo.MotPwmPhVData = 0;
    Acmctl_CtrlBus.Acmctl_CtrlMotReqDataAcmctlInfo.MotPwmPhWData = 0;
    Acmctl_CtrlBus.Acmctl_CtrlMotReqDataAcmctlInfo.MotReq = 1;
    Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIqMeasd = 0;
    Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIdMeasd = 0;
	Acmctl_vInitMotIvtrDutyGenn();
}

void Acmctl_Ctrl(void)
{
    Acmctl_Ctrl_Timer_Start = STM0_TIM0.U;

    /* Input */
    Acmctl_Ctrl_Read_Acmctl_CtrlMotDqIRefMccInfo(&Acmctl_CtrlBus.Acmctl_CtrlMotDqIRefMccInfo);
    /*==============================================================================
    * Members of structure Acmctl_CtrlMotDqIRefMccInfo 
     : Acmctl_CtrlMotDqIRefMccInfo.IdRef;
     : Acmctl_CtrlMotDqIRefMccInfo.IqRef;
     =============================================================================*/
    
    Acmctl_Ctrl_Read_Acmctl_CtrlMotCurrInfo(&Acmctl_CtrlBus.Acmctl_CtrlMotCurrInfo);
    /*==============================================================================
    * Members of structure Acmctl_CtrlMotCurrInfo 
     : Acmctl_CtrlMotCurrInfo.MotCurrPhUMeasd;
     : Acmctl_CtrlMotCurrInfo.MotCurrPhVMeasd;
     : Acmctl_CtrlMotCurrInfo.MotCurrPhWMeasd;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Acmctl_Ctrl_Read_Acmctl_CtrlMotRotgAgSigInfo_MotElecAngleFild(&Acmctl_CtrlBus.Acmctl_CtrlMotRotgAgSigInfo.MotElecAngleFild);

    /* Decomposed structure interface */
    Acmctl_Ctrl_Read_Acmctl_CtrlMotOrgSetStInfo_MotOrgSet(&Acmctl_CtrlBus.Acmctl_CtrlMotOrgSetStInfo.MotOrgSet);

    Acmctl_Ctrl_Read_Acmctl_CtrlMotDqIRefSesInfo(&Acmctl_CtrlBus.Acmctl_CtrlMotDqIRefSesInfo);
    /*==============================================================================
    * Members of structure Acmctl_CtrlMotDqIRefSesInfo 
     : Acmctl_CtrlMotDqIRefSesInfo.IdRef;
     : Acmctl_CtrlMotDqIRefSesInfo.IqRef;
     =============================================================================*/
    
    Acmctl_Ctrl_Read_Acmctl_CtrlFluxWeakengStInfo(&Acmctl_CtrlBus.Acmctl_CtrlFluxWeakengStInfo);
    /*==============================================================================
    * Members of structure Acmctl_CtrlFluxWeakengStInfo 
     : Acmctl_CtrlFluxWeakengStInfo.FluxWeakengFlg;
     : Acmctl_CtrlFluxWeakengStInfo.FluxWeakengFlgOld;
     =============================================================================*/
    
    Acmctl_Ctrl_Read_Acmctl_CtrlMtrArbtratorData(&Acmctl_CtrlBus.Acmctl_CtrlMtrArbtratorData);
    /*==============================================================================
    * Members of structure Acmctl_CtrlMtrArbtratorData 
     : Acmctl_CtrlMtrArbtratorData.IqSelected;
     : Acmctl_CtrlMtrArbtratorData.IdSelected;
     : Acmctl_CtrlMtrArbtratorData.UPhasePWMSelected;
     : Acmctl_CtrlMtrArbtratorData.VPhasePWMSelected;
     : Acmctl_CtrlMtrArbtratorData.WPhasePWMSelected;
     : Acmctl_CtrlMtrArbtratorData.MtrCtrlMode;
     =============================================================================*/
    
    Acmctl_Ctrl_Read_Acmctl_CtrlMtrArbtratorInfo(&Acmctl_CtrlBus.Acmctl_CtrlMtrArbtratorInfo);
    /*==============================================================================
    * Members of structure Acmctl_CtrlMtrArbtratorInfo 
     : Acmctl_CtrlMtrArbtratorInfo.MtrArbCalMode;
     : Acmctl_CtrlMtrArbtratorInfo.MtrArbState;
     =============================================================================*/
    
    Acmctl_Ctrl_Read_Acmctl_CtrlMtrProcessOutInfo(&Acmctl_CtrlBus.Acmctl_CtrlMtrProcessOutInfo);
    /*==============================================================================
    * Members of structure Acmctl_CtrlMtrProcessOutInfo 
     : Acmctl_CtrlMtrProcessOutInfo.MotCurrPhUMeasd;
     : Acmctl_CtrlMtrProcessOutInfo.MotCurrPhVMeasd;
     : Acmctl_CtrlMtrProcessOutInfo.MotCurrPhWMeasd;
     : Acmctl_CtrlMtrProcessOutInfo.MotPosiAngle1raw;
     : Acmctl_CtrlMtrProcessOutInfo.MotPosiAngle2raw;
     : Acmctl_CtrlMtrProcessOutInfo.MotPosiAngle1_1_100Deg;
     : Acmctl_CtrlMtrProcessOutInfo.MotPosiAngle2_1_100Deg;
     =============================================================================*/
    
    Acmctl_Ctrl_Read_Acmctl_CtrlEcuModeSts(&Acmctl_CtrlBus.Acmctl_CtrlEcuModeSts);
    Acmctl_Ctrl_Read_Acmctl_CtrlVdcLinkFild(&Acmctl_CtrlBus.Acmctl_CtrlVdcLinkFild);
    Acmctl_Ctrl_Read_Acmctl_CtrlMotCtrlMode(&Acmctl_CtrlBus.Acmctl_CtrlMotCtrlMode);
    Acmctl_Ctrl_Read_Acmctl_CtrlMotCtrlState(&Acmctl_CtrlBus.Acmctl_CtrlMotCtrlState);
    Acmctl_Ctrl_Read_Acmctl_CtrlFuncInhibitAcmctlSts(&Acmctl_CtrlBus.Acmctl_CtrlFuncInhibitAcmctlSts);
    Acmctl_Ctrl_Read_Acmctl_CtrlMtrArbDriveState(&Acmctl_CtrlBus.Acmctl_CtrlMtrArbDriveState);

    /* Process */
    Acmctl_vCallMotIvtrDutyGenn();

    Acmctl_CtrlBus.Acmctl_CtrlMotReqDataAcmctlInfo.MotPwmPhUData = (float32)Acmctl_CtrlBus.Acmctl_CtrlMotReqDataAcmctlInfo.MotPwmPhUData * SAL_MOT_IVTR_DUTY_SCALE;
	Acmctl_CtrlBus.Acmctl_CtrlMotReqDataAcmctlInfo.MotPwmPhVData = (float32)Acmctl_CtrlBus.Acmctl_CtrlMotReqDataAcmctlInfo.MotPwmPhVData * SAL_MOT_IVTR_DUTY_SCALE;
	Acmctl_CtrlBus.Acmctl_CtrlMotReqDataAcmctlInfo.MotPwmPhWData = (float32)Acmctl_CtrlBus.Acmctl_CtrlMotReqDataAcmctlInfo.MotPwmPhWData * SAL_MOT_IVTR_DUTY_SCALE;
    /* Output */
    Acmctl_Ctrl_Write_Acmctl_CtrlMotReqDataAcmctlInfo(&Acmctl_CtrlBus.Acmctl_CtrlMotReqDataAcmctlInfo);
    /*==============================================================================
    * Members of structure Acmctl_CtrlMotReqDataAcmctlInfo 
     : Acmctl_CtrlMotReqDataAcmctlInfo.MotPwmPhUData;
     : Acmctl_CtrlMotReqDataAcmctlInfo.MotPwmPhVData;
     : Acmctl_CtrlMotReqDataAcmctlInfo.MotPwmPhWData;
     : Acmctl_CtrlMotReqDataAcmctlInfo.MotReq;
     =============================================================================*/
    
    Acmctl_Ctrl_Write_Acmctl_CtrlMotDqIMeasdInfo(&Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo);
    /*==============================================================================
    * Members of structure Acmctl_CtrlMotDqIMeasdInfo 
     : Acmctl_CtrlMotDqIMeasdInfo.MotIqMeasd;
     : Acmctl_CtrlMotDqIMeasdInfo.MotIdMeasd;
     =============================================================================*/
    

    Acmctl_Ctrl_Timer_Elapsed = STM0_TIM0.U - Acmctl_Ctrl_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define ACMCTL_CTRL_STOP_SEC_CODE
#include "Acmctl_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
