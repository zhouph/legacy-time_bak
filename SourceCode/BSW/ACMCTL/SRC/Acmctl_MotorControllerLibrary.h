/**
 * @defgroup MotorControllerLibrary_Sub MotorControllerLibrary_Sub
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        MotorControllerLibrary_Sub.h
 * @brief       Local Header File
 * @date        20YY. MM. DD.
 ******************************************************************************/
 
#ifndef ACMCTL_MOTORCONTROLLERLIBRARY_H_
#define ACMCTL_MOTORCONTROLLERLIBRARY_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Acmctl_Ctrl.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Acmctl_vSinCos(fsc_t *pSinCos, sint16 s16x);
extern void Acmctl_vAbc2DqTrfm(fdq_t *pDq, fabc_t *pAbc, fsc_t *pSinCos);
extern void Acmctl_vInitfPi(fpi_t *pParam);
extern float Acmctl_fPIController(float fx_ref, float fx_mea, float fUff, fpi_t *pParam);
extern void Acmctl_vVdq2VabcnTrfm(fabc_t *pVabcn, fdq_t *pVdq, fsc_t *pSinCos, float fVdc);
extern void Acmctl_vCalcDuty3(fabc_t *p_abc, fabc_t *pPoleVolt, float fVdcLink, float fDutyMax);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ACMCTL_MOTORCONTROLLERLIBRARY_H_ */
/** @} */
