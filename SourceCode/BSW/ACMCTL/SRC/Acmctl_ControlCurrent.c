/**
 * @defgroup MotorControllerLibrary_Sub MotorControllerLibrary_Sub
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        MotorControllerLibrary_Sub.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Acmctl_ControlCurrent.h"
#include "Acmctl_MotorControllerLibrary.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define S16_MTR_CALC_CURR_REF		(1)
#define S16_MTR_CALC_DUTY_CMD		(0)

#define F_MTR_12V					(12)

#define F_MTR_L						(0.000033f)
#define F_MTR_R						(0.009f)
#define F_ACMCTL_TS					(0.0001f)

#define S8_ACMCTL_PI_CHG_CNT_MAX	(10)
/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
	sint8 lacmctls8FWeakFlg;
    sint16 lacmctls16IdRef;
    sint16 lacmctls16IqRef;
	uint16 lacmctlu16OrgSetFlg;
	sint16 lacmctls16VdcLink;
	sint16 lacmctls16MtrCtrlMode;
	sint16 lacmctls16ThetaElec;
	sint16 lacmctls16Ias;
	sint16 lacmctls16Ibs;
	sint16 lacmctls16Ics;

}GetMotIvtrCtrl_t;
/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ACMCTL_START_SEC_CONST_UNSPECIFIED
#include "Acmctl_MemMap.h"
/** Constant Section (UNSPECIFIED)**/


/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/


#define ACMCTL_STOP_SEC_CONST_UNSPECIFIED
#include "Acmctl_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACMCTL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmctl_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

#define ACMCTL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmctl_MemMap.h"

#define ACMCTL_START_SEC_VAR_NOINIT_32BIT
#include "Acmctl_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define ACMCTL_STOP_SEC_VAR_NOINIT_32BIT
#include "Acmctl_MemMap.h"

#define ACMCTL_START_SEC_VAR_UNSPECIFIED
#include "Acmctl_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACMCTL_STOP_SEC_VAR_UNSPECIFIED
#include "Acmctl_MemMap.h"
#define ACMCTL_START_SEC_VAR_32BIT
#include "Acmctl_MemMap.h"
/** Variable Section (32BIT)**/


#define ACMCTL_STOP_SEC_VAR_32BIT
#include "Acmctl_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACMCTL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmctl_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
sint8 lacmctls8FWeakFlgOld;
sint16 lacmctls16DutyA;
sint16 lacmctls16DutyB;
sint16 lacmctls16DutyC;
sint16 lacmctls16Id;
sint16 lacmctls16Iq;
sint16 lacmctls16Idc;
sint16 lacmctls16PiChgCnt;
sint16 lacmctls16MtrCtrlModeOld;

GetMotIvtrCtrl_t GetMotIvtrCtrl;

fpi_t lacmctlstfPiAct_d;
fpi_t lacmctlstfPiAct_q;
fpi_t lacmctlstfPiSmall_d;
fpi_t lacmctlstfPiSmall_q;
fpi_t lacmctlstfPiBig_d;
fpi_t lacmctlstfPiBig_q;
fpi_t lacmctlstfPiFin_d;
fpi_t lacmctlstfPiFin_q;

fdq_t lacmctlstfVcmd;
fdq_t lacmctlstfVout;
fdq_t lacmctlstfIdq;

fabc_t lacmctlstfVabcn;
fabc_t lacmctlstfIabc;

fabc_t	lacmctlstfDuty;
fsc_t 	lacmctlstfSinCos;

float lacmctlfIa;
float lacmctlfIb;
float lacmctlfIc;
float lacmctlfIdc;
float lacmctlfIdRef;
float lacmctlfIqRef;
float lacmctlfVdcLink;
float lacmctlfDeltaKp_d;
float lacmctlfDeltaKp_q;
float lacmctlfDeltaKiTs_d;
float lacmctlfDeltaKiTs_q;
float lacmctlfVff_d;
float lacmctlfVff_q;

#define ACMCTL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmctl_MemMap.h"

#define ACMCTL_START_SEC_VAR_NOINIT_32BIT
#include "Acmctl_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/
#define ACMCTL_STOP_SEC_VAR_NOINIT_32BIT
#include "Acmctl_MemMap.h"
#define ACMCTL_START_SEC_VAR_UNSPECIFIED
#include "Acmctl_MemMap.h"
/** Variable Section (UNSPECIFIED)**/
#define ACMCTL_STOP_SEC_VAR_UNSPECIFIED
#include "Acmctl_MemMap.h"
#define ACMCTL_START_SEC_VAR_32BIT
#include "Acmctl_MemMap.h"
/** Variable Section (32BIT)**/


#define ACMCTL_STOP_SEC_VAR_32BIT
#include "Acmctl_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ACMCTL_START_SEC_CODE
#include "Acmctl_MemMap.h"

static void Acmctl_vCallInterfaceIsr(void);
static void Acmctl_vOutpIfForMotIvtrCtlr(void);
static void Acmctl_vInitControlCurrent(void);
static void Acmctl_vControlCurrent(void);
static void Acmctl_vControlCurrent4OrgSet(void);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Acmctl_vCallMotIvtrDutyGenn(void)
{
	Acmctl_vCallInterfaceIsr();

	if(GetMotIvtrCtrl.lacmctlu16OrgSetFlg == 1)
	{
		Acmctl_vControlCurrent();
	}
	else
	{
		Acmctl_vControlCurrent4OrgSet();
	}

	Acmctl_vOutpIfForMotIvtrCtlr();
}
void Acmctl_vInitMotIvtrDutyGenn(void)
{
	Acmctl_vInitControlCurrent();
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
inline static void Acmctl_vCallInterfaceIsr(void)
{
	/* current sensor measurement update */
/*
	GetMotIvtrCtrl.lacmctls16Ias = (sint16)Acmctl_CtrlBus.Acmctl_CtrlMotCurrInfo.MotCurrPhUMeasd;
	GetMotIvtrCtrl.lacmctls16Ibs = (sint16)Acmctl_CtrlBus.Acmctl_CtrlMotCurrInfo.MotCurrPhVMeasd;
	GetMotIvtrCtrl.lacmctls16Ics = -(GetMotIvtrCtrl.lacmctls16Ias + GetMotIvtrCtrl.lacmctls16Ibs);
*/

	GetMotIvtrCtrl.lacmctls16Ias = (sint16)Acmctl_CtrlBus.Acmctl_CtrlMtrProcessOutInfo.MotCurrPhUMeasd;
	GetMotIvtrCtrl.lacmctls16Ibs = (sint16)Acmctl_CtrlBus.Acmctl_CtrlMtrProcessOutInfo.MotCurrPhVMeasd;
	GetMotIvtrCtrl.lacmctls16Ics = (sint16)Acmctl_CtrlBus.Acmctl_CtrlMtrProcessOutInfo.MotCurrPhWMeasd;


	lacmctls16MtrCtrlModeOld 			= GetMotIvtrCtrl.lacmctls16MtrCtrlMode;
	GetMotIvtrCtrl.lacmctls16MtrCtrlMode	= (sint16)Acmctl_CtrlBus.Acmctl_CtrlMotCtrlMode;
	GetMotIvtrCtrl.lacmctls16VdcLink 		= (sint16)Acmctl_CtrlBus.Acmctl_CtrlVdcLinkFild;

	GetMotIvtrCtrl.lacmctlu16OrgSetFlg 	= (uint16)Acmctl_CtrlBus.Acmctl_CtrlMotOrgSetStInfo.MotOrgSet;

	GetMotIvtrCtrl.lacmctls16ThetaElec 	= Acmctl_CtrlBus.Acmctl_CtrlMotRotgAgSigInfo.MotElecAngleFild;

	if(GetMotIvtrCtrl.lacmctlu16OrgSetFlg == 1)
	{
		GetMotIvtrCtrl.lacmctls16IdRef = (sint16) Acmctl_CtrlBus.Acmctl_CtrlMotDqIRefMccInfo.IdRef;
		GetMotIvtrCtrl.lacmctls16IqRef = (sint16)(-Acmctl_CtrlBus.Acmctl_CtrlMotDqIRefMccInfo.IqRef);
	}
	else
	{
		GetMotIvtrCtrl.lacmctls16IdRef = (sint16)Acmctl_CtrlBus.Acmctl_CtrlMotDqIRefSesInfo.IdRef;
		GetMotIvtrCtrl.lacmctls16IqRef = (sint16)(-Acmctl_CtrlBus.Acmctl_CtrlMotDqIRefSesInfo.IqRef);
	}

	lacmctls8FWeakFlgOld = GetMotIvtrCtrl.lacmctls8FWeakFlg;
	GetMotIvtrCtrl.lacmctls8FWeakFlg = (sint8)Acmctl_CtrlBus.Acmctl_CtrlFluxWeakengStInfo.FluxWeakengFlg;


	/* convert to floating variables */
	lacmctlstfIabc.fa = (float)GetMotIvtrCtrl.lacmctls16Ias*0.01f;
	lacmctlstfIabc.fb = (float)GetMotIvtrCtrl.lacmctls16Ibs*0.01f;
	lacmctlstfIabc.fc = (float)GetMotIvtrCtrl.lacmctls16Ics*0.01f;
	lacmctlfVdcLink = (float)GetMotIvtrCtrl.lacmctls16VdcLink*0.001f;

	lacmctlfIdRef = (float)GetMotIvtrCtrl.lacmctls16IdRef*0.01f;
	lacmctlfIqRef = (float)GetMotIvtrCtrl.lacmctls16IqRef*0.01f;
}


inline static void Acmctl_vOutpIfForMotIvtrCtlr(void)
{
	Acmctl_CtrlBus.Acmctl_CtrlMotReqDataAcmctlInfo.MotPwmPhUData = lacmctls16DutyA;
	Acmctl_CtrlBus.Acmctl_CtrlMotReqDataAcmctlInfo.MotPwmPhVData = lacmctls16DutyB;
	Acmctl_CtrlBus.Acmctl_CtrlMotReqDataAcmctlInfo.MotPwmPhWData = lacmctls16DutyC;

	Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIdMeasd = lacmctls16Id;
	Acmctl_CtrlBus.Acmctl_CtrlMotDqIMeasdInfo.MotIqMeasd = lacmctls16Iq;
}


#if S16_MTR_ACT_MODE == S16_MTR_NORMAL
#if ACMCTL_FLOATING == 1
static void Acmctl_vInitControlCurrent(void)
{
	lacmctlstfPiSmall_d.fId = 1.0f;
	lacmctlstfPiSmall_d.fKp = 0.075f;
	lacmctlstfPiSmall_d.fKiTs = 0.0025f;

	lacmctlstfPiBig_d.fId = 11.0f;
	lacmctlstfPiBig_d.fKp = 0.15f;
	lacmctlstfPiBig_d.fKiTs = 0.005f;

	lacmctlstfPiSmall_q.fId = 2.0f;
	lacmctlstfPiSmall_q.fKp = 0.075f;
	lacmctlstfPiSmall_q.fKiTs = 0.0025f;

	lacmctlstfPiBig_q.fId = 22.0f;
	lacmctlstfPiBig_q.fKp = 0.15f;
	lacmctlstfPiBig_q.fKiTs = 0.005f;

	lacmctlstfPiAct_d = lacmctlstfPiSmall_d;
	lacmctlstfPiAct_q = lacmctlstfPiSmall_q;
}

inline static void Acmctl_vControlCurrent(void)
{
	lacmctlfIdc = 1.5f*(lacmctlstfVout.fd*lacmctlstfIdq.fd + lacmctlstfVout.fq*lacmctlstfIdq.fq)/lacmctlfVdcLink;

	Acmctl_vSinCos(&lacmctlstfSinCos, GetMotIvtrCtrl.lacmctls16ThetaElec);
	Acmctl_vAbc2DqTrfm(&lacmctlstfIdq, &lacmctlstfIabc, &lacmctlstfSinCos);

	if ( (lacmctls16MtrCtrlModeOld != GetMotIvtrCtrl.lacmctls16MtrCtrlMode)
			&& (GetMotIvtrCtrl.lacmctls16MtrCtrlMode == S16_MTR_CALC_CURR_REF) )
	{
		Acmctl_vInitfPi((fpi_t*)&lacmctlstfPiAct_d);
		Acmctl_vInitfPi((fpi_t*)&lacmctlstfPiAct_q);
	}

	if (GetMotIvtrCtrl.lacmctls16MtrCtrlMode == S16_MTR_CALC_CURR_REF)
	{
		/* PI gain changing */
		if ((lacmctls8FWeakFlgOld == 0) && (GetMotIvtrCtrl.lacmctls8FWeakFlg == 1))
		{
			lacmctls16PiChgCnt = 1;

			lacmctlstfPiFin_d = lacmctlstfPiBig_d;
			lacmctlfDeltaKp_d = (lacmctlstfPiFin_d.fKp - lacmctlstfPiAct_d.fKp)/(float)S8_ACMCTL_PI_CHG_CNT_MAX;
			lacmctlfDeltaKiTs_d = (lacmctlstfPiFin_d.fKiTs - lacmctlstfPiAct_d.fKiTs)/(float)S8_ACMCTL_PI_CHG_CNT_MAX;
			lacmctlstfPiAct_d.fKp = lacmctlstfPiAct_d.fKp + lacmctlfDeltaKp_d;
			lacmctlstfPiAct_d.fKiTs = lacmctlstfPiAct_d.fKiTs + lacmctlfDeltaKiTs_d;

			lacmctlstfPiFin_q = lacmctlstfPiBig_q;
			lacmctlfDeltaKp_q = (lacmctlstfPiFin_q.fKp - lacmctlstfPiAct_q.fKp)/(float)S8_ACMCTL_PI_CHG_CNT_MAX;
			lacmctlfDeltaKiTs_q = (lacmctlstfPiFin_q.fKiTs - lacmctlstfPiAct_q.fKiTs)/(float)S8_ACMCTL_PI_CHG_CNT_MAX;
			lacmctlstfPiAct_q.fKp = lacmctlstfPiAct_q.fKp + lacmctlfDeltaKp_q;
			lacmctlstfPiAct_q.fKiTs = lacmctlstfPiAct_q.fKiTs + lacmctlfDeltaKiTs_q;
		}
		else if ((lacmctls8FWeakFlgOld == 1) && (GetMotIvtrCtrl.lacmctls8FWeakFlg == 0))
		{
			lacmctls16PiChgCnt = 1;

			lacmctlstfPiFin_d = lacmctlstfPiSmall_d;
			lacmctlfDeltaKp_d = (lacmctlstfPiFin_d.fKp - lacmctlstfPiAct_d.fKp)/(float)S8_ACMCTL_PI_CHG_CNT_MAX;
			lacmctlfDeltaKiTs_d = (lacmctlstfPiFin_d.fKiTs - lacmctlstfPiAct_d.fKiTs)/(float)S8_ACMCTL_PI_CHG_CNT_MAX;
			lacmctlstfPiAct_d.fKp = lacmctlstfPiAct_d.fKp + lacmctlfDeltaKp_d;
			lacmctlstfPiAct_d.fKiTs = lacmctlstfPiAct_d.fKiTs + lacmctlfDeltaKiTs_d;

			lacmctlstfPiFin_q = lacmctlstfPiSmall_q;
			lacmctlfDeltaKp_q = (lacmctlstfPiFin_q.fKp - lacmctlstfPiAct_q.fKp)/(float)S8_ACMCTL_PI_CHG_CNT_MAX;
			lacmctlfDeltaKiTs_q = (lacmctlstfPiFin_q.fKiTs - lacmctlstfPiAct_q.fKiTs)/(float)S8_ACMCTL_PI_CHG_CNT_MAX;
			lacmctlstfPiAct_q.fKp = lacmctlstfPiAct_q.fKp + lacmctlfDeltaKp_q;
			lacmctlstfPiAct_q.fKiTs = lacmctlstfPiAct_q.fKiTs + lacmctlfDeltaKiTs_q;
		}
		else
		{
			if ((lacmctls16PiChgCnt >= 1) && (lacmctls16PiChgCnt < S8_ACMCTL_PI_CHG_CNT_MAX))
			{
				lacmctls16PiChgCnt++;

				lacmctlstfPiAct_d.fKp = lacmctlstfPiAct_d.fKp + lacmctlfDeltaKp_d;
				lacmctlstfPiAct_d.fKiTs = lacmctlstfPiAct_d.fKiTs + lacmctlfDeltaKiTs_d;

				lacmctlstfPiAct_q.fKp = lacmctlstfPiAct_q.fKp + lacmctlfDeltaKp_q;
				lacmctlstfPiAct_q.fKiTs = lacmctlstfPiAct_q.fKiTs + lacmctlfDeltaKiTs_q;
			}
			else if (lacmctls16PiChgCnt >= S8_ACMCTL_PI_CHG_CNT_MAX)
			{
				lacmctls16PiChgCnt = 0;

				lacmctlstfPiAct_d = lacmctlstfPiFin_d;
				lacmctlfDeltaKp_d = 0.0f;
				lacmctlfDeltaKiTs_d = 0.0f;

				lacmctlstfPiAct_q = lacmctlstfPiFin_q;
				lacmctlfDeltaKp_q = 0.0f;
				lacmctlfDeltaKiTs_q = 0.0f;
			}
			else
			{
				;
			}
			/* PI gain changing */
		}

		lacmctlfVff_d = 0.0f;
		lacmctlfVff_q = 0.0f;

		lacmctlstfVcmd.fd = Acmctl_fPIController(lacmctlfIdRef, lacmctlstfIdq.fd, lacmctlfVff_d, (fpi_t*)&lacmctlstfPiAct_d);
		lacmctlstfVcmd.fq = Acmctl_fPIController(lacmctlfIqRef, lacmctlstfIdq.fq, lacmctlfVff_q, (fpi_t*)&lacmctlstfPiAct_q);

		Acmctl_vVdq2VabcnTrfm(&lacmctlstfVabcn, &lacmctlstfVcmd, &lacmctlstfSinCos, lacmctlfVdcLink);
		Acmctl_vCalcDuty3(&lacmctlstfDuty, &lacmctlstfVabcn, lacmctlfVdcLink, F_MTR_DUTY_MAX);
		Acmctl_vAbc2DqTrfm(&lacmctlstfVout, &lacmctlstfVabcn, &lacmctlstfSinCos);

		lacmctlstfPiAct_d.fUout = lacmctlstfVout.fd;
		lacmctlstfPiAct_q.fUout = lacmctlstfVout.fq;
	}
	else
	{
		lacmctlstfDuty.fa = F_MTR_DUTY_MAX_HALF;
		lacmctlstfDuty.fb = F_MTR_DUTY_MAX_HALF;
		lacmctlstfDuty.fc = F_MTR_DUTY_MAX_HALF;
	}

	/* convert to integer variables */
	lacmctls16DutyA = (sint16)(lacmctlstfDuty.fa);
	if (lacmctls16DutyA > S16_MTR_DUTY_MAX)
	{
		lacmctls16DutyA = S16_MTR_DUTY_MAX;
	}
	else if (lacmctls16DutyA < 0)
	{
		lacmctls16DutyA = 0;
	}
	else
	{
		;
	}

	lacmctls16DutyB = (sint16)(lacmctlstfDuty.fb);
	if (lacmctls16DutyB > S16_MTR_DUTY_MAX)
	{
		lacmctls16DutyB = S16_MTR_DUTY_MAX;
	}
	else if (lacmctls16DutyB < 0)
	{
		lacmctls16DutyB = 0;
	}
	else
	{
		;
	}

	lacmctls16DutyC = (sint16)(lacmctlstfDuty.fc);
	if (lacmctls16DutyC > S16_MTR_DUTY_MAX)
	{
		lacmctls16DutyC = S16_MTR_DUTY_MAX;
	}
	else if (lacmctls16DutyC < 0)
	{
		lacmctls16DutyC = 0;
	}
	else
	{
		;
	}

	lacmctls16Idc = (sint16)lacmctlfIdc*100.0f;
	lacmctls16Id = (sint16)(lacmctlstfIdq.fd*100.0f);
	lacmctls16Iq = (sint16)(lacmctlstfIdq.fq*100.0f);
	/* convert to integer variables */
}

static void Acmctl_vControlCurrent4OrgSet(void)
{
	lacmctlfIdc = 1.5f*(lacmctlstfVout.fd*lacmctlstfIdq.fd + lacmctlstfVout.fq*lacmctlstfIdq.fq)/lacmctlfVdcLink;
	lacmctls16Idc = (sint16)lacmctlfIdc*100.0f;

	Acmctl_vSinCos(&lacmctlstfSinCos, GetMotIvtrCtrl.lacmctls16ThetaElec);
	Acmctl_vAbc2DqTrfm(&lacmctlstfIdq, &lacmctlstfIabc, &lacmctlstfSinCos);
	lacmctls16Id = (sint16)(lacmctlstfIdq.fd*100.0f);
	lacmctls16Iq = (sint16)(lacmctlstfIdq.fq*100.0f);

	if ( (lacmctls16MtrCtrlModeOld != GetMotIvtrCtrl.lacmctls16MtrCtrlMode)
			&& (GetMotIvtrCtrl.lacmctls16MtrCtrlMode == S16_MTR_CALC_CURR_REF) )
	{
		Acmctl_vInitfPi((fpi_t*)&lacmctlstfPiAct_d);
		Acmctl_vInitfPi((fpi_t*)&lacmctlstfPiAct_q);
	}

	lacmctlfVff_d = 0.0f;
	lacmctlfVff_q = 0.0f;

	lacmctlstfVcmd.fd = Acmctl_fPIController(lacmctlfIdRef, lacmctlstfIdq.fd, lacmctlfVff_d, (fpi_t*)&lacmctlstfPiAct_d);
	lacmctlstfVcmd.fq = Acmctl_fPIController(lacmctlfIqRef, lacmctlstfIdq.fq, lacmctlfVff_q, (fpi_t*)&lacmctlstfPiAct_q);

	Acmctl_vVdq2VabcnTrfm(&lacmctlstfVabcn, &lacmctlstfVcmd, &lacmctlstfSinCos, lacmctlfVdcLink);
	Acmctl_vCalcDuty3(&lacmctlstfDuty, &lacmctlstfVabcn, F_MTR_12V, F_MTR_DUTY_MAX);
	Acmctl_vAbc2DqTrfm(&lacmctlstfVout, &lacmctlstfVabcn, &lacmctlstfSinCos);

	lacmctlstfPiAct_d.fUout = lacmctlstfVout.fd;
	lacmctlstfPiAct_q.fUout = lacmctlstfVout.fq;

	/* convert to integer variables */
	lacmctls16DutyA = (sint16)(lacmctlstfDuty.fa);
	if (lacmctls16DutyA > S16_MTR_DUTY_MAX)
	{
		lacmctls16DutyA = S16_MTR_DUTY_MAX;
	}
	else if (lacmctls16DutyA < 0)
	{
		lacmctls16DutyA = 0;
	}
	else
	{
		;
	}

	lacmctls16DutyB = (sint16)(lacmctlstfDuty.fb);
	if (lacmctls16DutyB > S16_MTR_DUTY_MAX)
	{
		lacmctls16DutyB = S16_MTR_DUTY_MAX;
	}
	else if (lacmctls16DutyB < 0)
	{
		lacmctls16DutyB = 0;
	}
	else
	{
		;
	}

	lacmctls16DutyC = (sint16)(lacmctlstfDuty.fc);
	if (lacmctls16DutyC > S16_MTR_DUTY_MAX)
	{
		lacmctls16DutyC = S16_MTR_DUTY_MAX;
	}
	else if (lacmctls16DutyC < 0)
	{
		lacmctls16DutyC = 0;
	}
	else
	{
		;
	}
	lacmctls16Idc = (sint16)lacmctlfIdc*100.0f;
	lacmctls16Id = (sint16)(lacmctlstfIdq.fd*100.0f);
	lacmctls16Iq = (sint16)(lacmctlstfIdq.fq*100.0f);
	/* convert to integer variables */
}
#else

#define S16_MTR_12V		(12000)

MC_Angle   lamtrAngSinCos;
MC_2PhSyst lamtr2PhImeas;
MC_2PhSyst lamtr2PhVcmd;
MC_2PhSyst lamtr2PhVout;
MC_3PhSyst lamtr3PhVcmd;
MC_3PhSyst lamtr3PhVout;
MC_3PhSyst lamtr3PhDuty;

MC_3PhSyst lamtr3PhDutyRatio;

MC_DqSyst lamtrDqImeas;
MC_DqSyst lamtrDqVff;
MC_DqSyst lamtrDqVcmd;
MC_DqSyst lamtrDqVout;


pi3_t lamtrPi3_d;
pi3_t lamtrPi3_q;
pi3_t lamtrPi3Ini_d;
pi3_t lamtrPi3Ini_q;
pi3_t lamtrPi3Fin_d;
pi3_t lamtrPi3Fin_q;

sint16 lamtrs16KpS_d;
Mivt_Frac16 lamtrfr16KpS_d;
sint16 lamtrs16KiTsS_d;
Mivt_Frac16 lamtrfr16KiTsS_d;

sint16 lamtrs16KpS_q;
Mivt_Frac16 lamtrfr16KpS_q;
sint16 lamtrs16KiTsS_q;
Mivt_Frac16 lamtrfr16KiTsS_q;

sint16 lamtrs16KpB_d;
Mivt_Frac16 lamtrfr16KpB_d;
sint16 lamtrs16KiTsB_d;
Mivt_Frac16 lamtrfr16KiTsB_d;

sint16 lamtrs16KpB_q;
Mivt_Frac16 lamtrfr16KpB_q;
sint16 lamtrs16KiTsB_q;
Mivt_Frac16 lamtrfr16KiTsB_q;

MC_3PhSyst lamtr3PhImeas;
sint16 lamtrs16Idc;

GetMotIvtrCtrl_t GetMotIvtrCtrl;

sint16	lamtr16VdcLinkFildMon;
uint8 lamtru8PiGainChangedFlg;


static void Acmctl_vInitControlCurrent(void)
{
	lamtrfr16KpS_d = Mivt_Frac16(0.75);
	lamtrfr16KiTsS_d = Mivt_Frac16(0.025);

	lamtrfr16KpS_q = Mivt_Frac16(0.75);
	lamtrfr16KiTsS_q = Mivt_Frac16(0.025);

	lamtrs16KpB_d = 1;
	lamtrfr16KpB_d = Mivt_Frac16(0.5);
	lamtrfr16KiTsB_d = Mivt_Frac16(0.05);

	lamtrs16KpB_q = 1;
	lamtrfr16KpB_q = Mivt_Frac16(0.5);
	lamtrfr16KiTsB_q = Mivt_Frac16(0.05);

	lamtrPi3_d.fr16Kp = lamtrfr16KpS_d;
	lamtrPi3_d.fr16KiTs = lamtrfr16KiTsS_d;

	lamtrPi3_q.fr16Kp = lamtrfr16KpS_q;
	lamtrPi3_q.fr16KiTs = lamtrfr16KiTsS_q;
}

inline static void Acmctl_vControlCurrent(void)
{
	//fixed
	lamtr3PhImeas.a = GetMotIvtrCtrl.lacmctls16Ias;
	lamtr3PhImeas.b = GetMotIvtrCtrl.lacmctls16Ibs;
	lamtr3PhImeas.c = GetMotIvtrCtrl.lacmctls16Ics;

	lamtrs16Idc = (lamtrDqVout.d *  lamtrDqImeas.d + lamtrDqVout.q *  lamtrDqImeas.q)/2*3/GetMotIvtrCtrl.lacmctls16VdcLink;
	lamtrAngSinCos.sin = Acmctl_f16Sin2(GetMotIvtrCtrl.lacmctls16ThetaElec);
	lamtrAngSinCos.cos = Acmctl_f16Cos2(GetMotIvtrCtrl.lacmctls16ThetaElec);
	MCLIB_ClarkTrfm((MC_2PhSyst*)&lamtr2PhImeas, (MC_3PhSyst*)&lamtr3PhImeas);
	MCLIB_ParkTrfm((MC_DqSyst*)&lamtrDqImeas, (MC_2PhSyst*)&lamtr2PhImeas, (MC_Angle*)&lamtrAngSinCos);

	if ( (lacmctls16MtrCtrlModeOld != GetMotIvtrCtrl.lacmctls16MtrCtrlMode)
			&& (GetMotIvtrCtrl.lacmctls16MtrCtrlMode == S16_MTR_CALC_CURR_REF) )
	{
		Acmctl_vInitPI3((pi3_t*)&lamtrPi3_d);
		Acmctl_vInitPI3((pi3_t*)&lamtrPi3_q);
	}

	if (GetMotIvtrCtrl.lacmctls16MtrCtrlMode == S16_MTR_CALC_CURR_REF)
	{
		if ((lacmctls8FWeakFlgOld == 0) && (GetMotIvtrCtrl.lacmctls8FWeakFlg == 1))
		{
			lamtrPi3Ini_d.s16Kp 	= lamtrs16KpS_d;
			lamtrPi3Ini_d.fr16Kp 	= lamtrfr16KpS_d;
			lamtrPi3Ini_d.s16KiTs 	= lamtrs16KiTsS_d;
			lamtrPi3Ini_d.fr16KiTs 	= lamtrfr16KiTsS_d;

			lamtrPi3Fin_d.s16Kp 	= lamtrs16KpB_d;
			lamtrPi3Fin_d.fr16Kp 	= lamtrfr16KpB_d;
			lamtrPi3Fin_d.s16KiTs 	= lamtrs16KiTsB_d;
			lamtrPi3Fin_d.fr16KiTs 	= lamtrfr16KiTsB_d;

			Acmctl_vPiGainChanger(&lamtrPi3_d, &lamtrPi3Fin_d, &lamtrPi3Ini_d, 5);

			lamtrPi3_q.s16Kp 		= lamtrPi3_d.s16Kp;
			lamtrPi3_q.fr16Kp 		= lamtrPi3_d.fr16Kp;
			lamtrPi3_q.s16KiTs 		= lamtrPi3_d.s16KiTs;
			lamtrPi3_q.fr16KiTs 	= lamtrPi3_d.fr16KiTs;
		}
		else if ((lacmctls8FWeakFlgOld == 1) && (GetMotIvtrCtrl.lacmctls8FWeakFlg == 0))
		{
			lamtrPi3Ini_d.s16Kp 	= lamtrs16KpB_d;
			lamtrPi3Ini_d.fr16Kp 	= lamtrfr16KpB_d;
			lamtrPi3Ini_d.s16KiTs 	= lamtrs16KiTsB_d;
			lamtrPi3Ini_d.fr16KiTs 	= lamtrfr16KiTsB_d;

			lamtrPi3Fin_d.s16Kp 	= lamtrs16KpS_d;
			lamtrPi3Fin_d.fr16Kp 	= lamtrfr16KpS_d;
			lamtrPi3Fin_d.s16KiTs 	= lamtrs16KiTsS_d;
			lamtrPi3Fin_d.fr16KiTs 	= lamtrfr16KiTsS_d;

			Acmctl_vPiGainChanger(&lamtrPi3_d, &lamtrPi3Fin_d, &lamtrPi3Ini_d, 5);

			lamtrPi3_q.s16Kp 		= lamtrPi3_d.s16Kp;
			lamtrPi3_q.fr16Kp 		= lamtrPi3_d.fr16Kp;
			lamtrPi3_q.s16KiTs 		= lamtrPi3_d.s16KiTs;
			lamtrPi3_q.fr16KiTs 	= lamtrPi3_d.fr16KiTs;
		}
		else
		{
			;
		}



		lamtrDqVcmd.d = Acmctl_s16PIController3(GetMotIvtrCtrl.lacmctls16IdRef, lamtrDqImeas.d, 0, (pi3_t*)&lamtrPi3_d);
		lamtrDqVcmd.q = Acmctl_s16PIController3(GetMotIvtrCtrl.lacmctls16IqRef, lamtrDqImeas.q, 0, (pi3_t*)&lamtrPi3_q);

		/* don't touch */
		MCLIB_ParkTrfmInv((MC_2PhSyst*)&lamtr2PhVcmd, (MC_DqSyst*)&lamtrDqVcmd, (MC_Angle*)&lamtrAngSinCos);
		MCLIB_ClarkTrfmInv((MC_3PhSyst*)&lamtr3PhVcmd, (MC_2PhSyst*)&lamtr2PhVcmd);
		MCLIB_CalcVout(&lamtr3PhVout, &lamtr3PhVcmd, GetMotIvtrCtrl.lacmctls16VdcLink);
		MCLIB_ClarkTrfm2((MC_2PhSyst*)&lamtr2PhVout,(MC_3PhSyst*)&lamtr3PhVout);
		MCLIB_ParkTrfm((MC_DqSyst*)&lamtrDqVout, (MC_2PhSyst*)&lamtr2PhVout, (MC_Angle*)&lamtrAngSinCos);
		MCLIB_CalcDuty((MC_3PhSyst*)&lamtr3PhDuty, (MC_3PhSyst*)&lamtr3PhVout, GetMotIvtrCtrl.lacmctls16VdcLink, S16_MTR_DUTY_MAX);
		/* don't touch */

		lamtrPi3_d.s16Uout = lamtrDqVout.d;
		lamtrPi3_q.s16Uout = lamtrDqVout.q;
    }
    else
    {
    	lamtr3PhDuty.a = S16_MTR_DUTY_MAX_HALF;
    	lamtr3PhDuty.b = S16_MTR_DUTY_MAX_HALF;
    	lamtr3PhDuty.c = S16_MTR_DUTY_MAX_HALF;
    }

	lacmctls16DutyA = lamtr3PhDuty.a;
	lacmctls16DutyB = lamtr3PhDuty.b;
	lacmctls16DutyC = lamtr3PhDuty.c;

	lacmctls16Id = lamtrDqImeas.d;
	lacmctls16Iq = lamtrDqImeas.q;

/*
	if (lsahbs16PedalTravelFilt > 100)
	{
    	lamtr3PhDuty.a = 2000;
    	lamtr3PhDuty.b = 1000;
    	lamtr3PhDuty.c = 1000;
	}
	else
	{
    	lamtr3PhDuty.a = S16_MTR_DUTY_MAX_HALF;
    	lamtr3PhDuty.b = S16_MTR_DUTY_MAX_HALF;
    	lamtr3PhDuty.c = S16_MTR_DUTY_MAX_HALF;
	}
*/
}

static void Acmctl_vControlCurrent4OrgSet(void)
{
	lamtrs16Idc = (lamtrDqVout.d *  lamtrDqImeas.d + lamtrDqVout.q *  lamtrDqImeas.q)/2*3/GetMotIvtrCtrl.lacmctls16VdcLink;
	lamtrAngSinCos.sin = Acmctl_f16Sin2(GetMotIvtrCtrl.lacmctls16ThetaElec);
	lamtrAngSinCos.cos = Acmctl_f16Cos2(GetMotIvtrCtrl.lacmctls16ThetaElec);
	MCLIB_ClarkTrfm((MC_2PhSyst*)&lamtr2PhImeas, (MC_3PhSyst*)&lamtr3PhImeas);
	MCLIB_ParkTrfm((MC_DqSyst*)&lamtrDqImeas, (MC_2PhSyst*)&lamtr2PhImeas, (MC_Angle*)&lamtrAngSinCos);


	if ( (lacmctls16MtrCtrlModeOld != GetMotIvtrCtrl.lacmctls16MtrCtrlMode)
			&& (GetMotIvtrCtrl.lacmctls16MtrCtrlMode == S16_MTR_CALC_CURR_REF) )
	{
		Acmctl_vInitPI3((pi3_t*)&lamtrPi3_d);
		Acmctl_vInitPI3((pi3_t*)&lamtrPi3_q);
	}

	lamtrDqVcmd.d = Acmctl_s16PIController3(GetMotIvtrCtrl.lacmctls16IdRef, lamtrDqImeas.d, 0, (pi3_t*)&lamtrPi3_d);
	lamtrDqVcmd.q = Acmctl_s16PIController3(GetMotIvtrCtrl.lacmctls16IqRef, lamtrDqImeas.q, 0, (pi3_t*)&lamtrPi3_q);

	MCLIB_ParkTrfmInv((MC_2PhSyst*)&lamtr2PhVcmd, (MC_DqSyst*)&lamtrDqVcmd, (MC_Angle*)&lamtrAngSinCos);
	MCLIB_ClarkTrfmInv((MC_3PhSyst*)&lamtr3PhVcmd, (MC_2PhSyst*)&lamtr2PhVcmd);
	MCLIB_CalcVout(&lamtr3PhVout, &lamtr3PhVcmd, S16_MTR_12V);
	MCLIB_ClarkTrfm2((MC_2PhSyst*)&lamtr2PhVout,(MC_3PhSyst*)&lamtr3PhVout);
	MCLIB_ParkTrfm((MC_DqSyst*)&lamtrDqVout, (MC_2PhSyst*)&lamtr2PhVout, (MC_Angle*)&lamtrAngSinCos);
	MCLIB_CalcDuty((MC_3PhSyst*)&lamtr3PhDuty, (MC_3PhSyst*)&lamtr3PhVout, S16_MTR_12V, S16_MTR_DUTY_MAX);

	lamtrPi3_d.s16Uout = lamtrDqVout.d;
	lamtrPi3_q.s16Uout = lamtrDqVout.q;

	lacmctls16DutyA = lamtr3PhDuty.a;
	lacmctls16DutyB = lamtr3PhDuty.b;
	lacmctls16DutyC = lamtr3PhDuty.c;

	lacmctls16Id = lamtrDqImeas.d;
	lacmctls16Iq = lamtrDqImeas.q;

}
#endif	//fixed
#endif
#define Acmctl_STOP_SEC_CODE
#include "Acmctl_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
