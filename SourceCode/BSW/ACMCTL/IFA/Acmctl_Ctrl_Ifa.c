/**
 * @defgroup Acmctl_Ctrl_Ifa Acmctl_Ctrl_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Acmctl_Ctrl_Ifa.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Acmctl_Ctrl_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define ACMCTL_CTRL_START_SEC_CONST_UNSPECIFIED
#include "Acmctl_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define ACMCTL_CTRL_STOP_SEC_CONST_UNSPECIFIED
#include "Acmctl_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACMCTL_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmctl_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ACMCTL_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmctl_MemMap.h"
#define ACMCTL_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Acmctl_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACMCTL_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Acmctl_MemMap.h"
#define ACMCTL_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Acmctl_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACMCTL_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Acmctl_MemMap.h"
#define ACMCTL_CTRL_START_SEC_VAR_32BIT
#include "Acmctl_MemMap.h"
/** Variable Section (32BIT)**/


#define ACMCTL_CTRL_STOP_SEC_VAR_32BIT
#include "Acmctl_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define ACMCTL_CTRL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmctl_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define ACMCTL_CTRL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Acmctl_MemMap.h"
#define ACMCTL_CTRL_START_SEC_VAR_NOINIT_32BIT
#include "Acmctl_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define ACMCTL_CTRL_STOP_SEC_VAR_NOINIT_32BIT
#include "Acmctl_MemMap.h"
#define ACMCTL_CTRL_START_SEC_VAR_UNSPECIFIED
#include "Acmctl_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define ACMCTL_CTRL_STOP_SEC_VAR_UNSPECIFIED
#include "Acmctl_MemMap.h"
#define ACMCTL_CTRL_START_SEC_VAR_32BIT
#include "Acmctl_MemMap.h"
/** Variable Section (32BIT)**/


#define ACMCTL_CTRL_STOP_SEC_VAR_32BIT
#include "Acmctl_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define ACMCTL_CTRL_START_SEC_CODE
#include "Acmctl_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define ACMCTL_CTRL_STOP_SEC_CODE
#include "Acmctl_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
