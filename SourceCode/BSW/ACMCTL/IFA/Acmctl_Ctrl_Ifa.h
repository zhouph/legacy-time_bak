/**
 * @defgroup Acmctl_Ctrl_Ifa Acmctl_Ctrl_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Acmctl_Ctrl_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef ACMCTL_CTRL_IFA_H_
#define ACMCTL_CTRL_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Acmctl_Ctrl_Read_Acmctl_CtrlMotDqIRefMccInfo(data) do \
{ \
    *data = Acmctl_CtrlMotDqIRefMccInfo; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlMotCurrInfo(data) do \
{ \
    *data = Acmctl_CtrlMotCurrInfo; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlMotRotgAgSigInfo(data) do \
{ \
    *data = Acmctl_CtrlMotRotgAgSigInfo; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlMotOrgSetStInfo(data) do \
{ \
    *data = Acmctl_CtrlMotOrgSetStInfo; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlMotDqIRefSesInfo(data) do \
{ \
    *data = Acmctl_CtrlMotDqIRefSesInfo; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlFluxWeakengStInfo(data) do \
{ \
    *data = Acmctl_CtrlFluxWeakengStInfo; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlMtrArbtratorData(data) do \
{ \
    *data = Acmctl_CtrlMtrArbtratorData; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlMtrArbtratorInfo(data) do \
{ \
    *data = Acmctl_CtrlMtrArbtratorInfo; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlMtrProcessOutInfo(data) do \
{ \
    *data = Acmctl_CtrlMtrProcessOutInfo; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlMotDqIRefMccInfo_IdRef(data) do \
{ \
    *data = Acmctl_CtrlMotDqIRefMccInfo.IdRef; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlMotDqIRefMccInfo_IqRef(data) do \
{ \
    *data = Acmctl_CtrlMotDqIRefMccInfo.IqRef; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlMotCurrInfo_MotCurrPhUMeasd(data) do \
{ \
    *data = Acmctl_CtrlMotCurrInfo.MotCurrPhUMeasd; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlMotCurrInfo_MotCurrPhVMeasd(data) do \
{ \
    *data = Acmctl_CtrlMotCurrInfo.MotCurrPhVMeasd; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlMotCurrInfo_MotCurrPhWMeasd(data) do \
{ \
    *data = Acmctl_CtrlMotCurrInfo.MotCurrPhWMeasd; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlMotRotgAgSigInfo_MotElecAngleFild(data) do \
{ \
    *data = Acmctl_CtrlMotRotgAgSigInfo.MotElecAngleFild; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlMotOrgSetStInfo_MotOrgSet(data) do \
{ \
    *data = Acmctl_CtrlMotOrgSetStInfo.MotOrgSet; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlMotDqIRefSesInfo_IdRef(data) do \
{ \
    *data = Acmctl_CtrlMotDqIRefSesInfo.IdRef; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlMotDqIRefSesInfo_IqRef(data) do \
{ \
    *data = Acmctl_CtrlMotDqIRefSesInfo.IqRef; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlFluxWeakengStInfo_FluxWeakengFlg(data) do \
{ \
    *data = Acmctl_CtrlFluxWeakengStInfo.FluxWeakengFlg; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlFluxWeakengStInfo_FluxWeakengFlgOld(data) do \
{ \
    *data = Acmctl_CtrlFluxWeakengStInfo.FluxWeakengFlgOld; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlMtrArbtratorData_IqSelected(data) do \
{ \
    *data = Acmctl_CtrlMtrArbtratorData.IqSelected; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlMtrArbtratorData_IdSelected(data) do \
{ \
    *data = Acmctl_CtrlMtrArbtratorData.IdSelected; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlMtrArbtratorData_UPhasePWMSelected(data) do \
{ \
    *data = Acmctl_CtrlMtrArbtratorData.UPhasePWMSelected; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlMtrArbtratorData_VPhasePWMSelected(data) do \
{ \
    *data = Acmctl_CtrlMtrArbtratorData.VPhasePWMSelected; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlMtrArbtratorData_WPhasePWMSelected(data) do \
{ \
    *data = Acmctl_CtrlMtrArbtratorData.WPhasePWMSelected; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlMtrArbtratorData_MtrCtrlMode(data) do \
{ \
    *data = Acmctl_CtrlMtrArbtratorData.MtrCtrlMode; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlMtrArbtratorInfo_MtrArbCalMode(data) do \
{ \
    *data = Acmctl_CtrlMtrArbtratorInfo.MtrArbCalMode; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlMtrArbtratorInfo_MtrArbState(data) do \
{ \
    *data = Acmctl_CtrlMtrArbtratorInfo.MtrArbState; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlMtrProcessOutInfo_MotCurrPhUMeasd(data) do \
{ \
    *data = Acmctl_CtrlMtrProcessOutInfo.MotCurrPhUMeasd; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlMtrProcessOutInfo_MotCurrPhVMeasd(data) do \
{ \
    *data = Acmctl_CtrlMtrProcessOutInfo.MotCurrPhVMeasd; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlMtrProcessOutInfo_MotCurrPhWMeasd(data) do \
{ \
    *data = Acmctl_CtrlMtrProcessOutInfo.MotCurrPhWMeasd; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlMtrProcessOutInfo_MotPosiAngle1raw(data) do \
{ \
    *data = Acmctl_CtrlMtrProcessOutInfo.MotPosiAngle1raw; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlMtrProcessOutInfo_MotPosiAngle2raw(data) do \
{ \
    *data = Acmctl_CtrlMtrProcessOutInfo.MotPosiAngle2raw; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlMtrProcessOutInfo_MotPosiAngle1_1_100Deg(data) do \
{ \
    *data = Acmctl_CtrlMtrProcessOutInfo.MotPosiAngle1_1_100Deg; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlMtrProcessOutInfo_MotPosiAngle2_1_100Deg(data) do \
{ \
    *data = Acmctl_CtrlMtrProcessOutInfo.MotPosiAngle2_1_100Deg; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlEcuModeSts(data) do \
{ \
    *data = Acmctl_CtrlEcuModeSts; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlVdcLinkFild(data) do \
{ \
    *data = Acmctl_CtrlVdcLinkFild; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlMotCtrlMode(data) do \
{ \
    *data = Acmctl_CtrlMotCtrlMode; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlMotCtrlState(data) do \
{ \
    *data = Acmctl_CtrlMotCtrlState; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlFuncInhibitAcmctlSts(data) do \
{ \
    *data = Acmctl_CtrlFuncInhibitAcmctlSts; \
}while(0);

#define Acmctl_Ctrl_Read_Acmctl_CtrlMtrArbDriveState(data) do \
{ \
    *data = Acmctl_CtrlMtrArbDriveState; \
}while(0);


/* Set Output DE MAcro Function */
#define Acmctl_Ctrl_Write_Acmctl_CtrlMotReqDataAcmctlInfo(data) do \
{ \
    Acmctl_CtrlMotReqDataAcmctlInfo = *data; \
}while(0);

#define Acmctl_Ctrl_Write_Acmctl_CtrlMotDqIMeasdInfo(data) do \
{ \
    Acmctl_CtrlMotDqIMeasdInfo = *data; \
}while(0);

#define Acmctl_Ctrl_Write_Acmctl_CtrlMotReqDataAcmctlInfo_MotPwmPhUData(data) do \
{ \
    Acmctl_CtrlMotReqDataAcmctlInfo.MotPwmPhUData = *data; \
}while(0);

#define Acmctl_Ctrl_Write_Acmctl_CtrlMotReqDataAcmctlInfo_MotPwmPhVData(data) do \
{ \
    Acmctl_CtrlMotReqDataAcmctlInfo.MotPwmPhVData = *data; \
}while(0);

#define Acmctl_Ctrl_Write_Acmctl_CtrlMotReqDataAcmctlInfo_MotPwmPhWData(data) do \
{ \
    Acmctl_CtrlMotReqDataAcmctlInfo.MotPwmPhWData = *data; \
}while(0);

#define Acmctl_Ctrl_Write_Acmctl_CtrlMotReqDataAcmctlInfo_MotReq(data) do \
{ \
    Acmctl_CtrlMotReqDataAcmctlInfo.MotReq = *data; \
}while(0);

#define Acmctl_Ctrl_Write_Acmctl_CtrlMotDqIMeasdInfo_MotIqMeasd(data) do \
{ \
    Acmctl_CtrlMotDqIMeasdInfo.MotIqMeasd = *data; \
}while(0);

#define Acmctl_Ctrl_Write_Acmctl_CtrlMotDqIMeasdInfo_MotIdMeasd(data) do \
{ \
    Acmctl_CtrlMotDqIMeasdInfo.MotIdMeasd = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* ACMCTL_CTRL_IFA_H_ */
/** @} */
