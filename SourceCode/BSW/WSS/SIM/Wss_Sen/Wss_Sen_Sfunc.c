#define S_FUNCTION_NAME      Wss_Sen_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          310
#define WidthOutputPort         19

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Wss_Sen.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NO_FAULT = input[0];
    Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WHEEL_SPEED_DECODER_DATA = input[1];
    Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STANDSTILL = input[2];
    Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_LATCH_D0 = input[3];
    Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NODATA = input[4];
    Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_INVALID = input[5];
    Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_OPEN = input[6];
    Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STB = input[7];
    Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STG = input[8];
    Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WSI_OT = input[9];
    Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_CURRENT_HI = input[10];
    Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NO_FAULT = input[11];
    Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WHEEL_SPEED_DECODER_DATA = input[12];
    Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STANDSTILL = input[13];
    Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_LATCH_D0 = input[14];
    Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NODATA = input[15];
    Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_INVALID = input[16];
    Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_OPEN = input[17];
    Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STB = input[18];
    Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STG = input[19];
    Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WSI_OT = input[20];
    Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_CURRENT_HI = input[21];
    Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NO_FAULT = input[22];
    Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WHEEL_SPEED_DECODER_DATA = input[23];
    Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STANDSTILL = input[24];
    Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_LATCH_D0 = input[25];
    Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NODATA = input[26];
    Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_INVALID = input[27];
    Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_OPEN = input[28];
    Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STB = input[29];
    Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STG = input[30];
    Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WSI_OT = input[31];
    Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_CURRENT_HI = input[32];
    Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NO_FAULT = input[33];
    Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WHEEL_SPEED_DECODER_DATA = input[34];
    Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STANDSTILL = input[35];
    Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_LATCH_D0 = input[36];
    Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NODATA = input[37];
    Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_INVALID = input[38];
    Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_OPEN = input[39];
    Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STB = input[40];
    Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STG = input[41];
    Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WSI_OT = input[42];
    Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_CURRENT_HI = input[43];
    Wss_SenWssMonInfo.FlRisngIdx = input[44];
    Wss_SenWssMonInfo.FlFallIdx = input[45];
    Wss_SenWssMonInfo.FlRisngTiStamp[0] = input[46];
    Wss_SenWssMonInfo.FlRisngTiStamp[1] = input[47];
    Wss_SenWssMonInfo.FlRisngTiStamp[2] = input[48];
    Wss_SenWssMonInfo.FlRisngTiStamp[3] = input[49];
    Wss_SenWssMonInfo.FlRisngTiStamp[4] = input[50];
    Wss_SenWssMonInfo.FlRisngTiStamp[5] = input[51];
    Wss_SenWssMonInfo.FlRisngTiStamp[6] = input[52];
    Wss_SenWssMonInfo.FlRisngTiStamp[7] = input[53];
    Wss_SenWssMonInfo.FlRisngTiStamp[8] = input[54];
    Wss_SenWssMonInfo.FlRisngTiStamp[9] = input[55];
    Wss_SenWssMonInfo.FlRisngTiStamp[10] = input[56];
    Wss_SenWssMonInfo.FlRisngTiStamp[11] = input[57];
    Wss_SenWssMonInfo.FlRisngTiStamp[12] = input[58];
    Wss_SenWssMonInfo.FlRisngTiStamp[13] = input[59];
    Wss_SenWssMonInfo.FlRisngTiStamp[14] = input[60];
    Wss_SenWssMonInfo.FlRisngTiStamp[15] = input[61];
    Wss_SenWssMonInfo.FlRisngTiStamp[16] = input[62];
    Wss_SenWssMonInfo.FlRisngTiStamp[17] = input[63];
    Wss_SenWssMonInfo.FlRisngTiStamp[18] = input[64];
    Wss_SenWssMonInfo.FlRisngTiStamp[19] = input[65];
    Wss_SenWssMonInfo.FlRisngTiStamp[20] = input[66];
    Wss_SenWssMonInfo.FlRisngTiStamp[21] = input[67];
    Wss_SenWssMonInfo.FlRisngTiStamp[22] = input[68];
    Wss_SenWssMonInfo.FlRisngTiStamp[23] = input[69];
    Wss_SenWssMonInfo.FlRisngTiStamp[24] = input[70];
    Wss_SenWssMonInfo.FlRisngTiStamp[25] = input[71];
    Wss_SenWssMonInfo.FlRisngTiStamp[26] = input[72];
    Wss_SenWssMonInfo.FlRisngTiStamp[27] = input[73];
    Wss_SenWssMonInfo.FlRisngTiStamp[28] = input[74];
    Wss_SenWssMonInfo.FlRisngTiStamp[29] = input[75];
    Wss_SenWssMonInfo.FlRisngTiStamp[30] = input[76];
    Wss_SenWssMonInfo.FlRisngTiStamp[31] = input[77];
    Wss_SenWssMonInfo.FlFallTiStamp[0] = input[78];
    Wss_SenWssMonInfo.FlFallTiStamp[1] = input[79];
    Wss_SenWssMonInfo.FlFallTiStamp[2] = input[80];
    Wss_SenWssMonInfo.FlFallTiStamp[3] = input[81];
    Wss_SenWssMonInfo.FlFallTiStamp[4] = input[82];
    Wss_SenWssMonInfo.FlFallTiStamp[5] = input[83];
    Wss_SenWssMonInfo.FlFallTiStamp[6] = input[84];
    Wss_SenWssMonInfo.FlFallTiStamp[7] = input[85];
    Wss_SenWssMonInfo.FlFallTiStamp[8] = input[86];
    Wss_SenWssMonInfo.FlFallTiStamp[9] = input[87];
    Wss_SenWssMonInfo.FlFallTiStamp[10] = input[88];
    Wss_SenWssMonInfo.FlFallTiStamp[11] = input[89];
    Wss_SenWssMonInfo.FlFallTiStamp[12] = input[90];
    Wss_SenWssMonInfo.FlFallTiStamp[13] = input[91];
    Wss_SenWssMonInfo.FlFallTiStamp[14] = input[92];
    Wss_SenWssMonInfo.FlFallTiStamp[15] = input[93];
    Wss_SenWssMonInfo.FlFallTiStamp[16] = input[94];
    Wss_SenWssMonInfo.FlFallTiStamp[17] = input[95];
    Wss_SenWssMonInfo.FlFallTiStamp[18] = input[96];
    Wss_SenWssMonInfo.FlFallTiStamp[19] = input[97];
    Wss_SenWssMonInfo.FlFallTiStamp[20] = input[98];
    Wss_SenWssMonInfo.FlFallTiStamp[21] = input[99];
    Wss_SenWssMonInfo.FlFallTiStamp[22] = input[100];
    Wss_SenWssMonInfo.FlFallTiStamp[23] = input[101];
    Wss_SenWssMonInfo.FlFallTiStamp[24] = input[102];
    Wss_SenWssMonInfo.FlFallTiStamp[25] = input[103];
    Wss_SenWssMonInfo.FlFallTiStamp[26] = input[104];
    Wss_SenWssMonInfo.FlFallTiStamp[27] = input[105];
    Wss_SenWssMonInfo.FlFallTiStamp[28] = input[106];
    Wss_SenWssMonInfo.FlFallTiStamp[29] = input[107];
    Wss_SenWssMonInfo.FlFallTiStamp[30] = input[108];
    Wss_SenWssMonInfo.FlFallTiStamp[31] = input[109];
    Wss_SenWssMonInfo.FrRisngIdx = input[110];
    Wss_SenWssMonInfo.FrFallIdx = input[111];
    Wss_SenWssMonInfo.FrRisngTiStamp[0] = input[112];
    Wss_SenWssMonInfo.FrRisngTiStamp[1] = input[113];
    Wss_SenWssMonInfo.FrRisngTiStamp[2] = input[114];
    Wss_SenWssMonInfo.FrRisngTiStamp[3] = input[115];
    Wss_SenWssMonInfo.FrRisngTiStamp[4] = input[116];
    Wss_SenWssMonInfo.FrRisngTiStamp[5] = input[117];
    Wss_SenWssMonInfo.FrRisngTiStamp[6] = input[118];
    Wss_SenWssMonInfo.FrRisngTiStamp[7] = input[119];
    Wss_SenWssMonInfo.FrRisngTiStamp[8] = input[120];
    Wss_SenWssMonInfo.FrRisngTiStamp[9] = input[121];
    Wss_SenWssMonInfo.FrRisngTiStamp[10] = input[122];
    Wss_SenWssMonInfo.FrRisngTiStamp[11] = input[123];
    Wss_SenWssMonInfo.FrRisngTiStamp[12] = input[124];
    Wss_SenWssMonInfo.FrRisngTiStamp[13] = input[125];
    Wss_SenWssMonInfo.FrRisngTiStamp[14] = input[126];
    Wss_SenWssMonInfo.FrRisngTiStamp[15] = input[127];
    Wss_SenWssMonInfo.FrRisngTiStamp[16] = input[128];
    Wss_SenWssMonInfo.FrRisngTiStamp[17] = input[129];
    Wss_SenWssMonInfo.FrRisngTiStamp[18] = input[130];
    Wss_SenWssMonInfo.FrRisngTiStamp[19] = input[131];
    Wss_SenWssMonInfo.FrRisngTiStamp[20] = input[132];
    Wss_SenWssMonInfo.FrRisngTiStamp[21] = input[133];
    Wss_SenWssMonInfo.FrRisngTiStamp[22] = input[134];
    Wss_SenWssMonInfo.FrRisngTiStamp[23] = input[135];
    Wss_SenWssMonInfo.FrRisngTiStamp[24] = input[136];
    Wss_SenWssMonInfo.FrRisngTiStamp[25] = input[137];
    Wss_SenWssMonInfo.FrRisngTiStamp[26] = input[138];
    Wss_SenWssMonInfo.FrRisngTiStamp[27] = input[139];
    Wss_SenWssMonInfo.FrRisngTiStamp[28] = input[140];
    Wss_SenWssMonInfo.FrRisngTiStamp[29] = input[141];
    Wss_SenWssMonInfo.FrRisngTiStamp[30] = input[142];
    Wss_SenWssMonInfo.FrRisngTiStamp[31] = input[143];
    Wss_SenWssMonInfo.FrFallTiStamp[0] = input[144];
    Wss_SenWssMonInfo.FrFallTiStamp[1] = input[145];
    Wss_SenWssMonInfo.FrFallTiStamp[2] = input[146];
    Wss_SenWssMonInfo.FrFallTiStamp[3] = input[147];
    Wss_SenWssMonInfo.FrFallTiStamp[4] = input[148];
    Wss_SenWssMonInfo.FrFallTiStamp[5] = input[149];
    Wss_SenWssMonInfo.FrFallTiStamp[6] = input[150];
    Wss_SenWssMonInfo.FrFallTiStamp[7] = input[151];
    Wss_SenWssMonInfo.FrFallTiStamp[8] = input[152];
    Wss_SenWssMonInfo.FrFallTiStamp[9] = input[153];
    Wss_SenWssMonInfo.FrFallTiStamp[10] = input[154];
    Wss_SenWssMonInfo.FrFallTiStamp[11] = input[155];
    Wss_SenWssMonInfo.FrFallTiStamp[12] = input[156];
    Wss_SenWssMonInfo.FrFallTiStamp[13] = input[157];
    Wss_SenWssMonInfo.FrFallTiStamp[14] = input[158];
    Wss_SenWssMonInfo.FrFallTiStamp[15] = input[159];
    Wss_SenWssMonInfo.FrFallTiStamp[16] = input[160];
    Wss_SenWssMonInfo.FrFallTiStamp[17] = input[161];
    Wss_SenWssMonInfo.FrFallTiStamp[18] = input[162];
    Wss_SenWssMonInfo.FrFallTiStamp[19] = input[163];
    Wss_SenWssMonInfo.FrFallTiStamp[20] = input[164];
    Wss_SenWssMonInfo.FrFallTiStamp[21] = input[165];
    Wss_SenWssMonInfo.FrFallTiStamp[22] = input[166];
    Wss_SenWssMonInfo.FrFallTiStamp[23] = input[167];
    Wss_SenWssMonInfo.FrFallTiStamp[24] = input[168];
    Wss_SenWssMonInfo.FrFallTiStamp[25] = input[169];
    Wss_SenWssMonInfo.FrFallTiStamp[26] = input[170];
    Wss_SenWssMonInfo.FrFallTiStamp[27] = input[171];
    Wss_SenWssMonInfo.FrFallTiStamp[28] = input[172];
    Wss_SenWssMonInfo.FrFallTiStamp[29] = input[173];
    Wss_SenWssMonInfo.FrFallTiStamp[30] = input[174];
    Wss_SenWssMonInfo.FrFallTiStamp[31] = input[175];
    Wss_SenWssMonInfo.RlRisngIdx = input[176];
    Wss_SenWssMonInfo.RlFallIdx = input[177];
    Wss_SenWssMonInfo.RlRisngTiStamp[0] = input[178];
    Wss_SenWssMonInfo.RlRisngTiStamp[1] = input[179];
    Wss_SenWssMonInfo.RlRisngTiStamp[2] = input[180];
    Wss_SenWssMonInfo.RlRisngTiStamp[3] = input[181];
    Wss_SenWssMonInfo.RlRisngTiStamp[4] = input[182];
    Wss_SenWssMonInfo.RlRisngTiStamp[5] = input[183];
    Wss_SenWssMonInfo.RlRisngTiStamp[6] = input[184];
    Wss_SenWssMonInfo.RlRisngTiStamp[7] = input[185];
    Wss_SenWssMonInfo.RlRisngTiStamp[8] = input[186];
    Wss_SenWssMonInfo.RlRisngTiStamp[9] = input[187];
    Wss_SenWssMonInfo.RlRisngTiStamp[10] = input[188];
    Wss_SenWssMonInfo.RlRisngTiStamp[11] = input[189];
    Wss_SenWssMonInfo.RlRisngTiStamp[12] = input[190];
    Wss_SenWssMonInfo.RlRisngTiStamp[13] = input[191];
    Wss_SenWssMonInfo.RlRisngTiStamp[14] = input[192];
    Wss_SenWssMonInfo.RlRisngTiStamp[15] = input[193];
    Wss_SenWssMonInfo.RlRisngTiStamp[16] = input[194];
    Wss_SenWssMonInfo.RlRisngTiStamp[17] = input[195];
    Wss_SenWssMonInfo.RlRisngTiStamp[18] = input[196];
    Wss_SenWssMonInfo.RlRisngTiStamp[19] = input[197];
    Wss_SenWssMonInfo.RlRisngTiStamp[20] = input[198];
    Wss_SenWssMonInfo.RlRisngTiStamp[21] = input[199];
    Wss_SenWssMonInfo.RlRisngTiStamp[22] = input[200];
    Wss_SenWssMonInfo.RlRisngTiStamp[23] = input[201];
    Wss_SenWssMonInfo.RlRisngTiStamp[24] = input[202];
    Wss_SenWssMonInfo.RlRisngTiStamp[25] = input[203];
    Wss_SenWssMonInfo.RlRisngTiStamp[26] = input[204];
    Wss_SenWssMonInfo.RlRisngTiStamp[27] = input[205];
    Wss_SenWssMonInfo.RlRisngTiStamp[28] = input[206];
    Wss_SenWssMonInfo.RlRisngTiStamp[29] = input[207];
    Wss_SenWssMonInfo.RlRisngTiStamp[30] = input[208];
    Wss_SenWssMonInfo.RlRisngTiStamp[31] = input[209];
    Wss_SenWssMonInfo.RlFallTiStamp[0] = input[210];
    Wss_SenWssMonInfo.RlFallTiStamp[1] = input[211];
    Wss_SenWssMonInfo.RlFallTiStamp[2] = input[212];
    Wss_SenWssMonInfo.RlFallTiStamp[3] = input[213];
    Wss_SenWssMonInfo.RlFallTiStamp[4] = input[214];
    Wss_SenWssMonInfo.RlFallTiStamp[5] = input[215];
    Wss_SenWssMonInfo.RlFallTiStamp[6] = input[216];
    Wss_SenWssMonInfo.RlFallTiStamp[7] = input[217];
    Wss_SenWssMonInfo.RlFallTiStamp[8] = input[218];
    Wss_SenWssMonInfo.RlFallTiStamp[9] = input[219];
    Wss_SenWssMonInfo.RlFallTiStamp[10] = input[220];
    Wss_SenWssMonInfo.RlFallTiStamp[11] = input[221];
    Wss_SenWssMonInfo.RlFallTiStamp[12] = input[222];
    Wss_SenWssMonInfo.RlFallTiStamp[13] = input[223];
    Wss_SenWssMonInfo.RlFallTiStamp[14] = input[224];
    Wss_SenWssMonInfo.RlFallTiStamp[15] = input[225];
    Wss_SenWssMonInfo.RlFallTiStamp[16] = input[226];
    Wss_SenWssMonInfo.RlFallTiStamp[17] = input[227];
    Wss_SenWssMonInfo.RlFallTiStamp[18] = input[228];
    Wss_SenWssMonInfo.RlFallTiStamp[19] = input[229];
    Wss_SenWssMonInfo.RlFallTiStamp[20] = input[230];
    Wss_SenWssMonInfo.RlFallTiStamp[21] = input[231];
    Wss_SenWssMonInfo.RlFallTiStamp[22] = input[232];
    Wss_SenWssMonInfo.RlFallTiStamp[23] = input[233];
    Wss_SenWssMonInfo.RlFallTiStamp[24] = input[234];
    Wss_SenWssMonInfo.RlFallTiStamp[25] = input[235];
    Wss_SenWssMonInfo.RlFallTiStamp[26] = input[236];
    Wss_SenWssMonInfo.RlFallTiStamp[27] = input[237];
    Wss_SenWssMonInfo.RlFallTiStamp[28] = input[238];
    Wss_SenWssMonInfo.RlFallTiStamp[29] = input[239];
    Wss_SenWssMonInfo.RlFallTiStamp[30] = input[240];
    Wss_SenWssMonInfo.RlFallTiStamp[31] = input[241];
    Wss_SenWssMonInfo.RrRisngIdx = input[242];
    Wss_SenWssMonInfo.RrFallIdx = input[243];
    Wss_SenWssMonInfo.RrRisngTiStamp[0] = input[244];
    Wss_SenWssMonInfo.RrRisngTiStamp[1] = input[245];
    Wss_SenWssMonInfo.RrRisngTiStamp[2] = input[246];
    Wss_SenWssMonInfo.RrRisngTiStamp[3] = input[247];
    Wss_SenWssMonInfo.RrRisngTiStamp[4] = input[248];
    Wss_SenWssMonInfo.RrRisngTiStamp[5] = input[249];
    Wss_SenWssMonInfo.RrRisngTiStamp[6] = input[250];
    Wss_SenWssMonInfo.RrRisngTiStamp[7] = input[251];
    Wss_SenWssMonInfo.RrRisngTiStamp[8] = input[252];
    Wss_SenWssMonInfo.RrRisngTiStamp[9] = input[253];
    Wss_SenWssMonInfo.RrRisngTiStamp[10] = input[254];
    Wss_SenWssMonInfo.RrRisngTiStamp[11] = input[255];
    Wss_SenWssMonInfo.RrRisngTiStamp[12] = input[256];
    Wss_SenWssMonInfo.RrRisngTiStamp[13] = input[257];
    Wss_SenWssMonInfo.RrRisngTiStamp[14] = input[258];
    Wss_SenWssMonInfo.RrRisngTiStamp[15] = input[259];
    Wss_SenWssMonInfo.RrRisngTiStamp[16] = input[260];
    Wss_SenWssMonInfo.RrRisngTiStamp[17] = input[261];
    Wss_SenWssMonInfo.RrRisngTiStamp[18] = input[262];
    Wss_SenWssMonInfo.RrRisngTiStamp[19] = input[263];
    Wss_SenWssMonInfo.RrRisngTiStamp[20] = input[264];
    Wss_SenWssMonInfo.RrRisngTiStamp[21] = input[265];
    Wss_SenWssMonInfo.RrRisngTiStamp[22] = input[266];
    Wss_SenWssMonInfo.RrRisngTiStamp[23] = input[267];
    Wss_SenWssMonInfo.RrRisngTiStamp[24] = input[268];
    Wss_SenWssMonInfo.RrRisngTiStamp[25] = input[269];
    Wss_SenWssMonInfo.RrRisngTiStamp[26] = input[270];
    Wss_SenWssMonInfo.RrRisngTiStamp[27] = input[271];
    Wss_SenWssMonInfo.RrRisngTiStamp[28] = input[272];
    Wss_SenWssMonInfo.RrRisngTiStamp[29] = input[273];
    Wss_SenWssMonInfo.RrRisngTiStamp[30] = input[274];
    Wss_SenWssMonInfo.RrRisngTiStamp[31] = input[275];
    Wss_SenWssMonInfo.RrFallTiStamp[0] = input[276];
    Wss_SenWssMonInfo.RrFallTiStamp[1] = input[277];
    Wss_SenWssMonInfo.RrFallTiStamp[2] = input[278];
    Wss_SenWssMonInfo.RrFallTiStamp[3] = input[279];
    Wss_SenWssMonInfo.RrFallTiStamp[4] = input[280];
    Wss_SenWssMonInfo.RrFallTiStamp[5] = input[281];
    Wss_SenWssMonInfo.RrFallTiStamp[6] = input[282];
    Wss_SenWssMonInfo.RrFallTiStamp[7] = input[283];
    Wss_SenWssMonInfo.RrFallTiStamp[8] = input[284];
    Wss_SenWssMonInfo.RrFallTiStamp[9] = input[285];
    Wss_SenWssMonInfo.RrFallTiStamp[10] = input[286];
    Wss_SenWssMonInfo.RrFallTiStamp[11] = input[287];
    Wss_SenWssMonInfo.RrFallTiStamp[12] = input[288];
    Wss_SenWssMonInfo.RrFallTiStamp[13] = input[289];
    Wss_SenWssMonInfo.RrFallTiStamp[14] = input[290];
    Wss_SenWssMonInfo.RrFallTiStamp[15] = input[291];
    Wss_SenWssMonInfo.RrFallTiStamp[16] = input[292];
    Wss_SenWssMonInfo.RrFallTiStamp[17] = input[293];
    Wss_SenWssMonInfo.RrFallTiStamp[18] = input[294];
    Wss_SenWssMonInfo.RrFallTiStamp[19] = input[295];
    Wss_SenWssMonInfo.RrFallTiStamp[20] = input[296];
    Wss_SenWssMonInfo.RrFallTiStamp[21] = input[297];
    Wss_SenWssMonInfo.RrFallTiStamp[22] = input[298];
    Wss_SenWssMonInfo.RrFallTiStamp[23] = input[299];
    Wss_SenWssMonInfo.RrFallTiStamp[24] = input[300];
    Wss_SenWssMonInfo.RrFallTiStamp[25] = input[301];
    Wss_SenWssMonInfo.RrFallTiStamp[26] = input[302];
    Wss_SenWssMonInfo.RrFallTiStamp[27] = input[303];
    Wss_SenWssMonInfo.RrFallTiStamp[28] = input[304];
    Wss_SenWssMonInfo.RrFallTiStamp[29] = input[305];
    Wss_SenWssMonInfo.RrFallTiStamp[30] = input[306];
    Wss_SenWssMonInfo.RrFallTiStamp[31] = input[307];
    Wss_SenEcuModeSts = input[308];
    Wss_SenFuncInhibitWssSts = input[309];

    Wss_Sen();


    output[0] = Wss_SenWhlPulseCntInfo.FlWhlPulseCnt;
    output[1] = Wss_SenWhlPulseCntInfo.FrWhlPulseCnt;
    output[2] = Wss_SenWhlPulseCntInfo.RlWhlPulseCnt;
    output[3] = Wss_SenWhlPulseCntInfo.RrWhlPulseCnt;
    output[4] = Wss_SenWhlSpdInfo.FlWhlSpd;
    output[5] = Wss_SenWhlSpdInfo.FrWhlSpd;
    output[6] = Wss_SenWhlSpdInfo.RlWhlSpd;
    output[7] = Wss_SenWhlSpdInfo.RrlWhlSpd;
    output[8] = Wss_SenWhlEdgeCntInfo.FlWhlEdgeCnt;
    output[9] = Wss_SenWhlEdgeCntInfo.FrWhlEdgeCnt;
    output[10] = Wss_SenWhlEdgeCntInfo.RlWhlEdgeCnt;
    output[11] = Wss_SenWhlEdgeCntInfo.RrWhlEdgeCnt;
    output[12] = Wss_SenWhlSnsrTypeInfo.FlWhlSnsrType;
    output[13] = Wss_SenWhlSnsrTypeInfo.FrWhlSnsrType;
    output[14] = Wss_SenWhlSnsrTypeInfo.RlWhlSnsrType;
    output[15] = Wss_SenWhlSnsrTypeInfo.RrWhlSnsrType;
    output[16] = Wss_SenWssSpeedOut.WssMax;
    output[17] = Wss_SenWssSpeedOut.WssMin;
    output[18] = Wss_SenWssCalcInfo.Rough_Sus_Flg;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Wss_Sen_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
