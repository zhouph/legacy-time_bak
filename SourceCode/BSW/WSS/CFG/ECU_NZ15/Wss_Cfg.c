/**
 * @defgroup Wss_Cfg Wss_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Wss_Cfg.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Wss_Cfg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define WSS_START_SEC_CONST_UNSPECIFIED
#include "Wss_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define WSS_STOP_SEC_CONST_UNSPECIFIED
#include "Wss_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define WSS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Wss_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
const Wss_ChannelConfigType Wss_ChannelConfig[Wss_WssChannel_MaxNum] =
{
    /* Wss_WssChannel_Ch_FL */
    {
        WSS_TYPE_ACTIVE                 /* Wheel Speed Sensor Type */
    },
    /* Wss_WssChannel_Ch_FR */
    {
        WSS_TYPE_ACTIVE                 /* Wheel Speed Sensor Type */
    },
    /* Wss_WssChannel_Ch_RL */
    {
        WSS_TYPE_ACTIVE                 /* Wheel Speed Sensor Type */
    },
    /* Wss_WssChannel_Ch_RR */
    {
        WSS_TYPE_ACTIVE                 /* Wheel Speed Sensor Type */
    }
};


#define WSS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Wss_MemMap.h"
#define WSS_START_SEC_VAR_NOINIT_32BIT
#include "Wss_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define WSS_STOP_SEC_VAR_NOINIT_32BIT
#include "Wss_MemMap.h"
#define WSS_START_SEC_VAR_UNSPECIFIED
#include "Wss_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define WSS_STOP_SEC_VAR_UNSPECIFIED
#include "Wss_MemMap.h"
#define WSS_START_SEC_VAR_32BIT
#include "Wss_MemMap.h"
/** Variable Section (32BIT)**/


#define WSS_STOP_SEC_VAR_32BIT
#include "Wss_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define WSS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Wss_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define WSS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Wss_MemMap.h"
#define WSS_START_SEC_VAR_NOINIT_32BIT
#include "Wss_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define WSS_STOP_SEC_VAR_NOINIT_32BIT
#include "Wss_MemMap.h"
#define WSS_START_SEC_VAR_UNSPECIFIED
#include "Wss_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define WSS_STOP_SEC_VAR_UNSPECIFIED
#include "Wss_MemMap.h"
#define WSS_START_SEC_VAR_32BIT
#include "Wss_MemMap.h"
/** Variable Section (32BIT)**/


#define WSS_STOP_SEC_VAR_32BIT
#include "Wss_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define WSS_START_SEC_CODE
#include "Wss_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define WSS_STOP_SEC_CODE
#include "Wss_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
