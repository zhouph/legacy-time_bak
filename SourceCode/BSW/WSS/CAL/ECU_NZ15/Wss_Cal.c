/**
 * @defgroup Wss_Cal Wss_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Wss_Cal.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Wss_Cal.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define WSS_START_SEC_CALIB_UNSPECIFIED
#include "Wss_MemMap.h"
/* Global Calibration Section */


#define WSS_STOP_SEC_CALIB_UNSPECIFIED
#include "Wss_MemMap.h"

#define WSS_START_SEC_CONST_UNSPECIFIED
#include "Wss_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define WSS_STOP_SEC_CONST_UNSPECIFIED
#include "Wss_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define WSS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Wss_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
Wss_CalConfigType Wss_CalConfig =
{
    /*  uint16_t        WSS_U16_TIRE_FL         */      2010,   /* Comment [ Tire Size of FL] */

    /*  uint16_t        WSS_U16_TIRE_FR         */      2010,   /* Comment [ Tire Size of FR] */

    /*  uint16_t        WSS_U16_TIRE_RL         */      2010,   /* Comment [ Tire Size of RL] */

    /*  uint16_t        WSS_U16_TIRE_RR         */      2010,   /* Comment [ Tire Size of RR] */

    /*  uint8_t         WSS_U8_TEETH_FL         */      48  ,   /* Comment [ Teeth Number of FL] */

    /*  uint8_t         WSS_U8_TEETH_FR         */      48  ,   /* Comment [ Teeth Number of FR] */

    /*  uint8_t         WSS_U8_TEETH_RL         */      47  ,   /* Comment [ Teeth Number of RL] */

    /*  uint8_t         WSS_U8_TEETH_RR         */      47  ,   /* Comment [ Teeth Number of RR] */
};



#define WSS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Wss_MemMap.h"
#define WSS_START_SEC_VAR_NOINIT_32BIT
#include "Wss_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define WSS_STOP_SEC_VAR_NOINIT_32BIT
#include "Wss_MemMap.h"
#define WSS_START_SEC_VAR_UNSPECIFIED
#include "Wss_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define WSS_STOP_SEC_VAR_UNSPECIFIED
#include "Wss_MemMap.h"
#define WSS_START_SEC_VAR_32BIT
#include "Wss_MemMap.h"
/** Variable Section (32BIT)**/


#define WSS_STOP_SEC_VAR_32BIT
#include "Wss_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define WSS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Wss_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define WSS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Wss_MemMap.h"
#define WSS_START_SEC_VAR_NOINIT_32BIT
#include "Wss_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define WSS_STOP_SEC_VAR_NOINIT_32BIT
#include "Wss_MemMap.h"
#define WSS_START_SEC_VAR_UNSPECIFIED
#include "Wss_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define WSS_STOP_SEC_VAR_UNSPECIFIED
#include "Wss_MemMap.h"
#define WSS_START_SEC_VAR_32BIT
#include "Wss_MemMap.h"
/** Variable Section (32BIT)**/


#define WSS_STOP_SEC_VAR_32BIT
#include "Wss_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define WSS_START_SEC_CODE
#include "Wss_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define WSS_STOP_SEC_CODE
#include "Wss_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
