# \file
#
# \brief Wss
#
# This file contains the implementation of the SWC
# module Wss.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Wss_src

Wss_src_FILES        += $(Wss_SRC_PATH)\Wss_Sen.c
Wss_src_FILES        += $(Wss_IFA_PATH)\Wss_Sen_Ifa.c
Wss_src_FILES        += $(Wss_SRC_PATH)\Wss_DecodePulse.c
Wss_src_FILES        += $(Wss_SRC_PATH)\Wss_CalcSpeed.c
Wss_src_FILES        += $(Wss_CFG_PATH)\Wss_Cfg.c
Wss_src_FILES        += $(Wss_CAL_PATH)\Wss_Cal.c

ifeq ($(ICE_COMPILE),true)
Wss_src_FILES        += $(Wss_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	Wss_src_FILES        += $(Wss_UNITY_PATH)\unity.c
	Wss_src_FILES        += $(Wss_UNITY_PATH)\unity_fixture.c	
	Wss_src_FILES        += $(Wss_UT_PATH)\main.c
	Wss_src_FILES        += $(Wss_UT_PATH)\Wss_Sen\Wss_Sen_UtMain.c
endif