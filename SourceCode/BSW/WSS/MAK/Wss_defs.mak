# \file
#
# \brief Wss
#
# This file contains the implementation of the SWC
# module Wss.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Wss_CORE_PATH     := $(MANDO_BSW_ROOT)\Wss
Wss_CAL_PATH      := $(Wss_CORE_PATH)\CAL\$(Wss_VARIANT)
Wss_SRC_PATH      := $(Wss_CORE_PATH)\SRC
Wss_CFG_PATH      := $(Wss_CORE_PATH)\CFG\$(Wss_VARIANT)
Wss_HDR_PATH      := $(Wss_CORE_PATH)\HDR
Wss_IFA_PATH      := $(Wss_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Wss_CMN_PATH      := $(Wss_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	Wss_UT_PATH		:= $(Wss_CORE_PATH)\UT
	Wss_UNITY_PATH	:= $(Wss_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(Wss_UT_PATH)
	CC_INCLUDE_PATH		+= $(Wss_UNITY_PATH)
	Wss_Sen_PATH 	:= Wss_UT_PATH\Wss_Sen
endif
CC_INCLUDE_PATH    += $(Wss_CAL_PATH)
CC_INCLUDE_PATH    += $(Wss_SRC_PATH)
CC_INCLUDE_PATH    += $(Wss_CFG_PATH)
CC_INCLUDE_PATH    += $(Wss_HDR_PATH)
CC_INCLUDE_PATH    += $(Wss_IFA_PATH)
CC_INCLUDE_PATH    += $(Wss_CMN_PATH)

