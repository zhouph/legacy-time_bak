/**
 * @defgroup Wss_Sen Wss_Sen
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Wss_Sen.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef WSS_SEN_H_
#define WSS_SEN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Wss_Types.h"
#include "Wss_Cfg.h"
#include "Wss_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define WSS_SEN_MODULE_ID      (0)
 #define WSS_SEN_MAJOR_VERSION  (2)
 #define WSS_SEN_MINOR_VERSION  (0)
 #define WSS_SEN_PATCH_VERSION  (0)
 #define WSS_SEN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Wss_Sen_HdrBusType Wss_SenBus;

/* Version Info */
extern const SwcVersionInfo_t Wss_SenVersionInfo;

/* Input Data Element */
extern Ach_InputAchWssPort0AsicInfo_t Wss_SenAchWssPort0AsicInfo;
extern Ach_InputAchWssPort1AsicInfo_t Wss_SenAchWssPort1AsicInfo;
extern Ach_InputAchWssPort2AsicInfo_t Wss_SenAchWssPort2AsicInfo;
extern Ach_InputAchWssPort3AsicInfo_t Wss_SenAchWssPort3AsicInfo;
extern Ioc_InputSR5msWssMonInfo_t Wss_SenWssMonInfo;
extern Mom_HndlrEcuModeSts_t Wss_SenEcuModeSts;
extern Eem_SuspcDetnFuncInhibitWssSts_t Wss_SenFuncInhibitWssSts;

/* Output Data Element */
extern Wss_SenWhlPulseCntInfo_t Wss_SenWhlPulseCntInfo;
extern Wss_SenWhlSpdInfo_t Wss_SenWhlSpdInfo;
extern Wss_SenWhlEdgeCntInfo_t Wss_SenWhlEdgeCntInfo;
extern Wss_SenWhlSnsrTypeInfo_t Wss_SenWhlSnsrTypeInfo;
extern Wss_SenWssSpeedOut_t Wss_SenWssSpeedOut;
extern Wss_SenWssCalc_t Wss_SenWssCalcInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Wss_Sen_Init(void);
extern void Wss_Sen(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* WSS_SEN_H_ */
/** @} */
