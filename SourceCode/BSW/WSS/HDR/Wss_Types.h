/**
 * @defgroup Wss_Types Wss_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Wss_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef WSS_TYPES_H_
#define WSS_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
#define WSS_TIMESTAMP_BUF_SIZE  32
#define WSS_PULSE_BUF_NUM       10

#define WSS_TYPE_ACTIVE         0
#define WSS_TYPE_SMART          1
#define WSS_TYPE_VDA            2
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
 /** \brief Type for Wheel Speed Sensor Channel */
typedef Saluint8 Wss_ChannelType;

/** \brief Type for Ring Buffer Index information of DMA for Wheel */
typedef Saluint16 Wss_IdxType;

/** \brief Type for Ring Buffer information of DMA for Wheel */
typedef Saluint32 Wss_ArrayType;

/** \brief Type for Pulse Count of Wheel */
typedef Saluint8 Wss_CntType;

/** \brief Type for Puse Edge Count of Wheel */
typedef Saluint16 Wss_EdgeType;

/** \brief Type for Number of Array Buffer */
typedef Saluint8 Wss_BufNumType;

typedef Saluint8 Wss_SnsrType;

typedef struct
{
    Saluint32 FlPulseWitdh;
    Saluint32 FrPulseWitdh;
    Saluint32 RlPulseWitdh;
    Saluint32 RrPulseWitdh;
}WssWhlPulseWidthSum_t;

typedef struct
{
    Saluint32 FlPulseWitdh[WSS_PULSE_BUF_NUM];
    Saluint32 FrPulseWitdh[WSS_PULSE_BUF_NUM];
    Saluint32 RlPulseWitdh[WSS_PULSE_BUF_NUM];
    Saluint32 RrPulseWitdh[WSS_PULSE_BUF_NUM];
}WssWhlPulseWidthBuf_t;


/** \brief Type for Wss Channel Config */
typedef struct 
{
    Wss_SnsrType                WssSensorType;
}Wss_ChannelConfigType;

/** \brief Type for Legacy Wheel Struct */
typedef struct
{
    Saluint8  u8PulseCnt;               /* Pulse Count for one loop */
    Saluint32 u32PulseWidthSum;
    Saluint32 u32PulseWidthBuf[WSS_PULSE_BUF_NUM];    /* last 5 Pulse Width for one loop */
    Saluint8  u8PulseStartStep;
    Saluint32 u32PeriodTempStred;
    Saluint32 u32Period;
    Saluint16 u16ToneWheelDeviation;
    Saluint16 fsu16SpeedOfResol64;
}Wheel_Calc_t;

/** \brief Type for Wss Calibration parameter Struct */
typedef struct
{
    Saluint16 WSS_U16_TIRE_FL ;   /* [ Tire Size of FL] */
    Saluint16 WSS_U16_TIRE_FR ;   /* [ Tire Size of FR] */
    Saluint16 WSS_U16_TIRE_RL ;   /* [ Tire Size of RL] */
    Saluint16 WSS_U16_TIRE_RR ;   /* [ Tire Size of RR] */
    Saluint8  WSS_U8_TEETH_FL ;   /* [ Teeth Number of FL] */
    Saluint8  WSS_U8_TEETH_FR ;   /* [ Teeth Number of FR] */
    Saluint8  WSS_U8_TEETH_RL ;   /* [ Teeth Number of RL] */
    Saluint8  WSS_U8_TEETH_RR ;   /* [ Teeth Number of RR] */
} Wss_CalConfigType;

typedef struct
{
/* Input Data Element */
    Ach_InputAchWssPort0AsicInfo_t Wss_SenAchWssPort0AsicInfo;
    Ach_InputAchWssPort1AsicInfo_t Wss_SenAchWssPort1AsicInfo;
    Ach_InputAchWssPort2AsicInfo_t Wss_SenAchWssPort2AsicInfo;
    Ach_InputAchWssPort3AsicInfo_t Wss_SenAchWssPort3AsicInfo;
    Ioc_InputSR5msWssMonInfo_t Wss_SenWssMonInfo;
    Mom_HndlrEcuModeSts_t Wss_SenEcuModeSts;
    Eem_SuspcDetnFuncInhibitWssSts_t Wss_SenFuncInhibitWssSts;

/* Output Data Element */
    Wss_SenWhlPulseCntInfo_t Wss_SenWhlPulseCntInfo;
    Wss_SenWhlSpdInfo_t Wss_SenWhlSpdInfo;
    Wss_SenWhlEdgeCntInfo_t Wss_SenWhlEdgeCntInfo;
    Wss_SenWhlSnsrTypeInfo_t Wss_SenWhlSnsrTypeInfo;
    Wss_SenWssSpeedOut_t Wss_SenWssSpeedOut;
    Wss_SenWssCalc_t Wss_SenWssCalcInfo;
	/* Internal Data Element */
    WssWhlPulseWidthSum_t Wss_MainPulseWidthSum;
    WssWhlPulseWidthBuf_t Wss_MainPulseWidthBuf;
}Wss_Sen_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* WSS_TYPES_H_ */
/** @} */
