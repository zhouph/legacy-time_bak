/**
 * @defgroup Wss_MemMap Wss_MemMap
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Wss_MemMap.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef WSS_MEMMAP_H_
#define WSS_MEMMAP_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/*------------------[MemMap error checking]----------------------------------*/

#if (defined MEMMAP_ERROR) /* to prevent double definition */
#error MEMMAP_ERROR defined, wrong MemMap.h usage
#endif /* if (!defined MEMMAP_ERROR) */

/** \brief Checks if this file has been correctly used
 **
 ** This definition checks if this file has been correctly included
 ** the last time.
 ** We do it here, before the big if-elif. */
#define MEMMAP_ERROR


#if 0
/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (WSS_START_SEC_CODE)
  #undef WSS_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_WSS_SEC_STARTED
    #error "WSS section not closed"
  #endif
  #define CHK_WSS_SEC_STARTED
  #define CHK_WSS_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (WSS_STOP_SEC_CODE)
  #undef WSS_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_WSS_SEC_CODE_STARTED
    #error "WSS_SEC_CODE not opened"
  #endif
  #undef CHK_WSS_SEC_STARTED
  #undef CHK_WSS_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (WSS_START_SEC_CONST_UNSPECIFIED)
  #undef WSS_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_WSS_SEC_STARTED
    #error "WSS section not closed"
  #endif
  #define CHK_WSS_SEC_STARTED
  #define CHK_WSS_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (WSS_STOP_SEC_CONST_UNSPECIFIED)
  #undef WSS_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_WSS_SEC_CONST_UNSPECIFIED_STARTED
    #error "WSS_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_WSS_SEC_STARTED
  #undef CHK_WSS_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (WSS_START_SEC_VAR_UNSPECIFIED)
  #undef WSS_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_WSS_SEC_STARTED
    #error "WSS section not closed"
  #endif
  #define CHK_WSS_SEC_STARTED
  #define CHK_WSS_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (WSS_STOP_SEC_VAR_UNSPECIFIED)
  #undef WSS_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_WSS_SEC_VAR_UNSPECIFIED_STARTED
    #error "WSS_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_WSS_SEC_STARTED
  #undef CHK_WSS_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (WSS_START_SEC_VAR_32BIT)
  #undef WSS_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_WSS_SEC_STARTED
    #error "WSS section not closed"
  #endif
  #define CHK_WSS_SEC_STARTED
  #define CHK_WSS_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (WSS_STOP_SEC_VAR_32BIT)
  #undef WSS_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_WSS_SEC_VAR_32BIT_STARTED
    #error "WSS_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_WSS_SEC_STARTED
  #undef CHK_WSS_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (WSS_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef WSS_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_WSS_SEC_STARTED
    #error "WSS section not closed"
  #endif
  #define CHK_WSS_SEC_STARTED
  #define CHK_WSS_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (WSS_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef WSS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_WSS_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "WSS_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_WSS_SEC_STARTED
  #undef CHK_WSS_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (WSS_START_SEC_VAR_NOINIT_32BIT)
  #undef WSS_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_WSS_SEC_STARTED
    #error "WSS section not closed"
  #endif
  #define CHK_WSS_SEC_STARTED
  #define CHK_WSS_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (WSS_STOP_SEC_VAR_NOINIT_32BIT)
  #undef WSS_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_WSS_SEC_VAR_NOINIT_32BIT_STARTED
    #error "WSS_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_WSS_SEC_STARTED
  #undef CHK_WSS_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (WSS_START_SEC_CALIB_UNSPECIFIED)
  #undef WSS_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_WSS_SEC_STARTED
    #error "WSS section not closed"
  #endif
  #define CHK_WSS_SEC_STARTED
  #define CHK_WSS_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (WSS_STOP_SEC_CALIB_UNSPECIFIED)
  #undef WSS_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_WSS_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "WSS_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_WSS_SEC_STARTED
  #undef CHK_WSS_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR



/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (WSS_SEN_START_SEC_CODE)
  #undef WSS_SEN_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_WSS_SEN_SEC_STARTED
    #error "WSS_SEN section not closed"
  #endif
  #define CHK_WSS_SEN_SEC_STARTED
  #define CHK_WSS_SEN_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (WSS_SEN_STOP_SEC_CODE)
  #undef WSS_SEN_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_WSS_SEN_SEC_CODE_STARTED
    #error "WSS_SEN_SEC_CODE not opened"
  #endif
  #undef CHK_WSS_SEN_SEC_STARTED
  #undef CHK_WSS_SEN_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (WSS_SEN_START_SEC_CONST_UNSPECIFIED)
  #undef WSS_SEN_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_WSS_SEN_SEC_STARTED
    #error "WSS_SEN section not closed"
  #endif
  #define CHK_WSS_SEN_SEC_STARTED
  #define CHK_WSS_SEN_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (WSS_SEN_STOP_SEC_CONST_UNSPECIFIED)
  #undef WSS_SEN_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_WSS_SEN_SEC_CONST_UNSPECIFIED_STARTED
    #error "WSS_SEN_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_WSS_SEN_SEC_STARTED
  #undef CHK_WSS_SEN_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (WSS_SEN_START_SEC_VAR_UNSPECIFIED)
  #undef WSS_SEN_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_WSS_SEN_SEC_STARTED
    #error "WSS_SEN section not closed"
  #endif
  #define CHK_WSS_SEN_SEC_STARTED
  #define CHK_WSS_SEN_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (WSS_SEN_STOP_SEC_VAR_UNSPECIFIED)
  #undef WSS_SEN_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_WSS_SEN_SEC_VAR_UNSPECIFIED_STARTED
    #error "WSS_SEN_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_WSS_SEN_SEC_STARTED
  #undef CHK_WSS_SEN_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (WSS_SEN_START_SEC_VAR_32BIT)
  #undef WSS_SEN_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_WSS_SEN_SEC_STARTED
    #error "WSS_SEN section not closed"
  #endif
  #define CHK_WSS_SEN_SEC_STARTED
  #define CHK_WSS_SEN_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (WSS_SEN_STOP_SEC_VAR_32BIT)
  #undef WSS_SEN_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_WSS_SEN_SEC_VAR_32BIT_STARTED
    #error "WSS_SEN_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_WSS_SEN_SEC_STARTED
  #undef CHK_WSS_SEN_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (WSS_SEN_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef WSS_SEN_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_WSS_SEN_SEC_STARTED
    #error "WSS_SEN section not closed"
  #endif
  #define CHK_WSS_SEN_SEC_STARTED
  #define CHK_WSS_SEN_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (WSS_SEN_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef WSS_SEN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_WSS_SEN_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "WSS_SEN_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_WSS_SEN_SEC_STARTED
  #undef CHK_WSS_SEN_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (WSS_SEN_START_SEC_VAR_NOINIT_32BIT)
  #undef WSS_SEN_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_WSS_SEN_SEC_STARTED
    #error "WSS_SEN section not closed"
  #endif
  #define CHK_WSS_SEN_SEC_STARTED
  #define CHK_WSS_SEN_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (WSS_SEN_STOP_SEC_VAR_NOINIT_32BIT)
  #undef WSS_SEN_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_WSS_SEN_SEC_VAR_NOINIT_32BIT_STARTED
    #error "WSS_SEN_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_WSS_SEN_SEC_STARTED
  #undef CHK_WSS_SEN_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (WSS_SEN_START_SEC_CALIB_UNSPECIFIED)
  #undef WSS_SEN_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_WSS_SEN_SEC_STARTED
    #error "WSS_SEN section not closed"
  #endif
  #define CHK_WSS_SEN_SEC_STARTED
  #define CHK_WSS_SEN_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (WSS_SEN_STOP_SEC_CALIB_UNSPECIFIED)
  #undef WSS_SEN_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_WSS_SEN_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "WSS_SEN_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_WSS_SEN_SEC_STARTED
  #undef CHK_WSS_SEN_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR


#endif


/*------------------[MemMap error checking]----------------------------------*/

#if (defined MEMMAP_ERROR) /* to prevent double definition */
#error MEMMAP_ERROR defined, wrong MemMap.h usage
#endif /* if (!defined MEMMAP_ERROR) */
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* WSS_MEMMAP_H_ */
/** @} */
