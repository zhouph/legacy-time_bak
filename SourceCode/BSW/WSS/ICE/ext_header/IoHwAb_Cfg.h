/**
 * @defgroup IoHwAb_Cfg IoHwAb_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        IoHwAb_Cfg.h
 * @brief       Template file
 * @date        2014. 11. 13.
 ******************************************************************************/

#ifndef IOHWAB_CFG_H_
#define IOHWAB_CFG_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "IoHwAb_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 /* Input port symbolic names */
 #define IOHWAB_IN_PORT_VLV_FLNC               (IoHwAb_PortType) 0
 #define IOHWAB_IN_PORT_VLV_FRNC               (IoHwAb_PortType) 1
 #define IOHWAB_IN_PORT_VLV_RLNC               (IoHwAb_PortType) 2
 #define IOHWAB_IN_PORT_VLV_RRNC               (IoHwAb_PortType) 3
 #define IOHWAB_IN_PORT_VLV_FLNO               (IoHwAb_PortType) 4
 #define IOHWAB_IN_PORT_VLV_FRNO               (IoHwAb_PortType) 5
 #define IOHWAB_IN_PORT_VLV_RLNO               (IoHwAb_PortType) 6
 #define IOHWAB_IN_PORT_VLV_RRNO               (IoHwAb_PortType) 7
/* Adc voltage */
 #define IOHWAB_IN_PORT_NORM_PSIMP_POS_MON     (IoHwAb_PortType) 8
 #define IOHWAB_IN_PORT_NORM_PSIMP_NEG_MON     (IoHwAb_PortType) 9
 #define IOHWAB_IN_PORT_NORM_MCP_NEG_MON       (IoHwAb_PortType) 10
 #define IOHWAB_IN_PORT_NORM_MCP_POS_MON       (IoHwAb_PortType) 11
 #define IOHWAB_IN_PORT_NORM_FSP_CBS_MON       (IoHwAb_PortType) 12
 #define IOHWAB_IN_PORT_NORM_CSP_MON           (IoHwAb_PortType) 13
 #define IOHWAB_IN_PORT_NORM_VBATT02_MON       (IoHwAb_PortType) 14
 #define IOHWAB_IN_PORT_NORM_VBATT01_MON       (IoHwAb_PortType) 15
 #define IOHWAB_IN_PORT_NORM_5V_P_POW_MON      (IoHwAb_PortType) 16
 #define IOHWAB_IN_PORT_NORM_VDD_MON           (IoHwAb_PortType) 17
 #define IOHWAB_IN_PORT_NORM_3p3V_VCC_MON      (IoHwAb_PortType) 18
 #define IOHWAB_IN_PORT_NORM_5V_VCC_MON        (IoHwAb_PortType) 19
 #define IOHWAB_IN_PORT_NORM_TEMP_V            (IoHwAb_PortType) 20
 #define IOHWAB_IN_PORT_NORM_PDT_5V_MON        (IoHwAb_PortType) 21
 #define IOHWAB_IN_PORT_NORM_PDF_5V_MON        (IoHwAb_PortType) 22
 #define IOHWAB_IN_PORT_NORM_PDT_SIG_MON       (IoHwAb_PortType) 23
 #define IOHWAB_IN_PORT_NORM_PDF_SIG_MON       (IoHwAb_PortType) 24
 #define IOHWAB_IN_PORT_NORM_CE_MON            (IoHwAb_PortType) 25
 #define IOHWAB_IN_PORT_NORM_B_REL             (IoHwAb_PortType) 26
 #define IOHWAB_IN_PORT_NORM_ADC_U_OUT         (IoHwAb_PortType) 27
 #define IOHWAB_IN_PORT_NORM_ADC_V_OUT         (IoHwAb_PortType) 28
 #define IOHWAB_IN_PORT_NORM_ADC_W_OUT         (IoHwAb_PortType) 29
 #define IOHWAB_IN_PORT_NORM_ADC_STAR          (IoHwAb_PortType) 30
 #define IOHWAB_IN_PORT_VLV_CUTV1_MON          (IoHwAb_PortType) 31
 #define IOHWAB_IN_PORT_VLV_CUTV2_MON          (IoHwAb_PortType) 32
 #define IOHWAB_IN_PORT_VLV_CIRCUITV_P_MON     (IoHwAb_PortType) 33
 #define IOHWAB_IN_PORT_VLV_CIRCUITV_S_MON     (IoHwAb_PortType) 34
 #define IOHWAB_IN_PORT_VLV_SIMV_MON           (IoHwAb_PortType) 35
 #define IOHWAB_IN_PORT_MOT_ADC_IV             (IoHwAb_PortType) 36
 #define IOHWAB_IN_PORT_MOT_ADC_IU             (IoHwAb_PortType) 37
/* Digital Input */
 #define IOHWAB_IN_PORT_BLS_MON                (IoHwAb_PortType) 38
 #define IOHWAB_IN_PORT_FLEX_BRAKE_A_MON       (IoHwAb_PortType) 39
 #define IOHWAB_IN_PORT_FLEX_BRAKE_B_MON       (IoHwAb_PortType) 40
 #define IOHWAB_IN_PORT_RELAY_ESS_MON          (IoHwAb_PortType) 41
 #define IOHWAB_IN_PORT_RSM_DBC_MON            (IoHwAb_PortType) 42
 #define IOHWAB_IN_PORT_AVH_SW_MON             (IoHwAb_PortType) 43
 #define IOHWAB_IN_PORT_BS_SW_MON              (IoHwAb_PortType) 44
 #define IOHWAB_IN_PORT_DOOR_SW_MON            (IoHwAb_PortType) 45
 #define IOHWAB_IN_PORT_ESC_SW_MON             (IoHwAb_PortType) 46
 #define IOHWAB_IN_PORT_HAZARD_SW_MON          (IoHwAb_PortType) 47
 #define IOHWAB_IN_PORT_HDC_SW_MON             (IoHwAb_PortType) 48
 #define IOHWAB_IN_PORT_PB_SW_MON              (IoHwAb_PortType) 49
 #define IOHWAB_IN_PORT_VDA_AI                 (IoHwAb_PortType) 50
 #define IOHWAB_IN_PORT_RELAY_FAULT            (IoHwAb_PortType) 51
 #define IOHWAB_IN_PORT_FF1                    (IoHwAb_PortType) 52
 #define IOHWAB_IN_PORT_MPS1_ANGLE             (IoHwAb_PortType) 53
 #define IOHWAB_IN_PORT_MPS2_ANGLE             (IoHwAb_PortType) 54

 #define IOHWAB_IN_PORT_WSS_FL				   (IoHwAb_PortType) 55
 #define IOHWAB_IN_PORT_WSS_FR				   (IoHwAb_PortType) 56
 #define IOHWAB_IN_PORT_WSS_RL				   (IoHwAb_PortType) 57
 #define IOHWAB_IN_PORT_WSS_RR				   (IoHwAb_PortType) 58
 
 /* Output port symbolic names */
 #define IOHWAB_OUT_PORT_VLV_FLNC       (IoHwAb_PortType) 59
 #define IOHWAB_OUT_PORT_VLV_FRNC       (IoHwAb_PortType) 60
 #define IOHWAB_OUT_PORT_VLV_RLNC       (IoHwAb_PortType) 61
 #define IOHWAB_OUT_PORT_VLV_RRNC       (IoHwAb_PortType) 62
 #define IOHWAB_OUT_PORT_VLV_FLNO       (IoHwAb_PortType) 63
 #define IOHWAB_OUT_PORT_VLV_FRNO       (IoHwAb_PortType) 64
 #define IOHWAB_OUT_PORT_VLV_RLNO       (IoHwAb_PortType) 65
 #define IOHWAB_OUT_PORT_VLV_RRNO       (IoHwAb_PortType) 66
 #define IOHWAB_OUT_PORT_VLV_PRI_CUT    (IoHwAb_PortType) 67
 #define IOHWAB_OUT_PORT_VLV_SEC_CUT    (IoHwAb_PortType) 68
 #define IOHWAB_OUT_PORT_VLV_PRI_CV     (IoHwAb_PortType) 69
 #define IOHWAB_OUT_PORT_VLV_SEC_CV     (IoHwAb_PortType) 70
 #define IOHWAB_OUT_PORT_VLV_SIM        (IoHwAb_PortType) 71
 #define IOHWAB_OUT_PORT_RLY_VALVE      (IoHwAb_PortType) 72
 #define IOHWAB_OUT_PORT_RLY_BRK_LAMP   (IoHwAb_PortType) 73
 #define IOHWAB_OUT_PORT_RLY_ESS_LAMP   (IoHwAb_PortType) 74
 #define IOHWAB_OUT_PORT_MOT_U_HIGH     (IoHwAb_PortType) 75
 #define IOHWAB_OUT_PORT_MOT_U_LOW      (IoHwAb_PortType) 76
 #define IOHWAB_OUT_PORT_MOT_V_HIGH     (IoHwAb_PortType) 77
 #define IOHWAB_OUT_PORT_MOT_V_LOW      (IoHwAb_PortType) 78
 #define IOHWAB_OUT_PORT_MOT_W_HIGH     (IoHwAb_PortType) 79
 #define IOHWAB_OUT_PORT_MOT_W_LOW      (IoHwAb_PortType) 80
 /* Dio Set high Low */
 #define IOHWAB_OUT_PORT_GD_ENB         (IoHwAb_PortType) 81
 #define IOHWAB_OUT_PORT_RELAY_DBC_DRV  (IoHwAb_PortType) 82
 #define IOHWAB_OUT_PORT_ECU_INHIBIT    (IoHwAb_PortType) 83
 #define IOHWAB_OUT_PORT_RELAY_ESS_DRV  (IoHwAb_PortType) 84
 #define IOHWAB_OUT_PORT_PDF5V_INH      (IoHwAb_PortType) 85
 #define IOHWAB_OUT_PORT_PDT5V_INH      (IoHwAb_PortType) 86
 #define IOHWAB_OUT_PORT_GD_PWR_DIS     (IoHwAb_PortType) 87
 #define IOHWAB_OUT_PORT_FSR2_OFF_MAIN  (IoHwAb_PortType) 88
 #define IOHWAB_OUT_PORT_MCU_LED_1      (IoHwAb_PortType) 89
 #define IOHWAB_OUT_PORT_MCU_LED_2      (IoHwAb_PortType) 90
 #define IOHWAB_OUT_PORT_VSO_IN_DRV     (IoHwAb_PortType) 91
 #define IOHWAB_OUT_PORT_V_RESET        (IoHwAb_PortType) 92
 #define IOHWAB_OUT_PORT_CSP_DRV        (IoHwAb_PortType) 93
 #define IOHWAB_OUT_PORT_TEST_PIN_0     (IoHwAb_PortType) 94
 #define IOHWAB_OUT_PORT_TEST_PIN_1     (IoHwAb_PortType) 95
 #define IOHWAB_OUT_PORT_TEST_PIN_2     (IoHwAb_PortType) 96
 #define IOHWAB_OUT_PORT_TEST_PIN_3     (IoHwAb_PortType) 97
 #define IOHWAB_OUT_PORT_TEST_PIN_4     (IoHwAb_PortType) 98
 #define IOHWAB_OUT_PORT_TEST_PIN_5     (IoHwAb_PortType) 99
 #define IOHWAB_OUT_PORT_TEST_PIN_6     (IoHwAb_PortType) 100
 #define IOHWAB_OUT_PORT_TEST_PIN_7     (IoHwAb_PortType) 101
 #define IOHWAB_OUT_PORT_TEST_PIN_8     (IoHwAb_PortType) 102
 #define IOHWAB_OUT_PORT_TEST_PIN_9     (IoHwAb_PortType) 103
 #define IOHWAB_OUT_PORT_TEST_PIN_10    (IoHwAb_PortType) 104
 #define IOHWAB_OUT_PORT_TEST_PIN_11    (IoHwAb_PortType) 105
 #define IOHWAB_OUT_PORT_TEST_PIN_12    (IoHwAb_PortType) 106
 #define IOHWAB_OUT_PORT_FSR_ON         (IoHwAb_PortType) 107
 #define IOHWAB_OUT_PORT_ESF            (IoHwAb_PortType) 108

/* Digital In/output bi-direction */
 #define IOHWAB_INOUT_PORT_FF2          (IoHwAb_PortType) 109
/* Adc voltage Added for 20141024 PCB */
 #define IOHWAB_IN_PORT_NORM_MCP2_NEG_MON       (IoHwAb_PortType) 110
 #define IOHWAB_IN_PORT_NORM_MCP2_POS_MON       (IoHwAb_PortType) 111
 #define IOHWAB_IN_PORT_MOT_ADC_IV_2            (IoHwAb_PortType) 112
 #define IOHWAB_IN_PORT_MOT_ADC_IU_2            (IoHwAb_PortType) 113
 #define IOHWAB_OUT_PORT_RELAY_ENABLE           (IoHwAb_PortType) 114

 #define IOHWAB_PORT_MAX_NUM            (IoHwAb_PortType) 115

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/
 /* Routing Configuration */
 extern const IoHwAb_RoutingType IoHwAb_RoutingConfig[IOHWAB_PORT_MAX_NUM];

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* IOHWAB_CFG_H_ */
/** @} */
