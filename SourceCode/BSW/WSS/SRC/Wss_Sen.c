/**
 * @defgroup Wss_Sen Wss_Sen
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Wss_Sen.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Wss_Sen.h"
#include "Wss_Sen_Ifa.h"
#include "IfxStm_reg.h"
#include "Wss_CalcSpeed.h"
#include "Wss_DecodePulse.h"


/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define WSS_SEN_START_SEC_CONST_UNSPECIFIED
#include "Wss_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define WSS_SEN_STOP_SEC_CONST_UNSPECIFIED
#include "Wss_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define WSS_SEN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Wss_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Wss_Sen_HdrBusType Wss_SenBus;

/* Version Info */
const SwcVersionInfo_t Wss_SenVersionInfo = 
{   
    WSS_SEN_MODULE_ID,           /* Wss_SenVersionInfo.ModuleId */
    WSS_SEN_MAJOR_VERSION,       /* Wss_SenVersionInfo.MajorVer */
    WSS_SEN_MINOR_VERSION,       /* Wss_SenVersionInfo.MinorVer */
    WSS_SEN_PATCH_VERSION,       /* Wss_SenVersionInfo.PatchVer */
    WSS_SEN_BRANCH_VERSION       /* Wss_SenVersionInfo.BranchVer */
};
    
/* Input Data Element */
Ach_InputAchWssPort0AsicInfo_t Wss_SenAchWssPort0AsicInfo;
Ach_InputAchWssPort1AsicInfo_t Wss_SenAchWssPort1AsicInfo;
Ach_InputAchWssPort2AsicInfo_t Wss_SenAchWssPort2AsicInfo;
Ach_InputAchWssPort3AsicInfo_t Wss_SenAchWssPort3AsicInfo;
Ioc_InputSR5msWssMonInfo_t Wss_SenWssMonInfo;
Mom_HndlrEcuModeSts_t Wss_SenEcuModeSts;
Eem_SuspcDetnFuncInhibitWssSts_t Wss_SenFuncInhibitWssSts;

/* Output Data Element */
Wss_SenWhlPulseCntInfo_t Wss_SenWhlPulseCntInfo;
Wss_SenWhlSpdInfo_t Wss_SenWhlSpdInfo;
Wss_SenWhlEdgeCntInfo_t Wss_SenWhlEdgeCntInfo;
Wss_SenWhlSnsrTypeInfo_t Wss_SenWhlSnsrTypeInfo;
Wss_SenWssSpeedOut_t Wss_SenWssSpeedOut;
Wss_SenWssCalc_t Wss_SenWssCalcInfo;

uint32 Wss_Sen_Timer_Start;
uint32 Wss_Sen_Timer_Elapsed;

#define WSS_SEN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Wss_MemMap.h"
#define WSS_SEN_START_SEC_VAR_NOINIT_32BIT
#include "Wss_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define WSS_SEN_STOP_SEC_VAR_NOINIT_32BIT
#include "Wss_MemMap.h"
#define WSS_SEN_START_SEC_VAR_UNSPECIFIED
#include "Wss_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define WSS_SEN_STOP_SEC_VAR_UNSPECIFIED
#include "Wss_MemMap.h"
#define WSS_SEN_START_SEC_VAR_32BIT
#include "Wss_MemMap.h"
/** Variable Section (32BIT)**/


#define WSS_SEN_STOP_SEC_VAR_32BIT
#include "Wss_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define WSS_SEN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Wss_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define WSS_SEN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Wss_MemMap.h"
#define WSS_SEN_START_SEC_VAR_NOINIT_32BIT
#include "Wss_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define WSS_SEN_STOP_SEC_VAR_NOINIT_32BIT
#include "Wss_MemMap.h"
#define WSS_SEN_START_SEC_VAR_UNSPECIFIED
#include "Wss_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define WSS_SEN_STOP_SEC_VAR_UNSPECIFIED
#include "Wss_MemMap.h"
#define WSS_SEN_START_SEC_VAR_32BIT
#include "Wss_MemMap.h"
/** Variable Section (32BIT)**/


#define WSS_SEN_STOP_SEC_VAR_32BIT
#include "Wss_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define WSS_SEN_START_SEC_CODE
#include "Wss_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Wss_Sen_Init(void)
{
    uint16 i;

    /* Initialize internal bus */
    Wss_SenBus.Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NO_FAULT = 0;
    Wss_SenBus.Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WHEEL_SPEED_DECODER_DATA = 0;
    Wss_SenBus.Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STANDSTILL = 0;
    Wss_SenBus.Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_LATCH_D0 = 0;
    Wss_SenBus.Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NODATA = 0;
    Wss_SenBus.Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_INVALID = 0;
    Wss_SenBus.Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_OPEN = 0;
    Wss_SenBus.Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STB = 0;
    Wss_SenBus.Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STG = 0;
    Wss_SenBus.Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WSI_OT = 0;
    Wss_SenBus.Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_CURRENT_HI = 0;
    Wss_SenBus.Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NO_FAULT = 0;
    Wss_SenBus.Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WHEEL_SPEED_DECODER_DATA = 0;
    Wss_SenBus.Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STANDSTILL = 0;
    Wss_SenBus.Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_LATCH_D0 = 0;
    Wss_SenBus.Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NODATA = 0;
    Wss_SenBus.Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_INVALID = 0;
    Wss_SenBus.Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_OPEN = 0;
    Wss_SenBus.Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STB = 0;
    Wss_SenBus.Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STG = 0;
    Wss_SenBus.Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WSI_OT = 0;
    Wss_SenBus.Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_CURRENT_HI = 0;
    Wss_SenBus.Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NO_FAULT = 0;
    Wss_SenBus.Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WHEEL_SPEED_DECODER_DATA = 0;
    Wss_SenBus.Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STANDSTILL = 0;
    Wss_SenBus.Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_LATCH_D0 = 0;
    Wss_SenBus.Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NODATA = 0;
    Wss_SenBus.Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_INVALID = 0;
    Wss_SenBus.Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_OPEN = 0;
    Wss_SenBus.Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STB = 0;
    Wss_SenBus.Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STG = 0;
    Wss_SenBus.Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WSI_OT = 0;
    Wss_SenBus.Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_CURRENT_HI = 0;
    Wss_SenBus.Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NO_FAULT = 0;
    Wss_SenBus.Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WHEEL_SPEED_DECODER_DATA = 0;
    Wss_SenBus.Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STANDSTILL = 0;
    Wss_SenBus.Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_LATCH_D0 = 0;
    Wss_SenBus.Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NODATA = 0;
    Wss_SenBus.Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_INVALID = 0;
    Wss_SenBus.Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_OPEN = 0;
    Wss_SenBus.Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STB = 0;
    Wss_SenBus.Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STG = 0;
    Wss_SenBus.Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WSI_OT = 0;
    Wss_SenBus.Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_CURRENT_HI = 0;
    Wss_SenBus.Wss_SenWssMonInfo.FlRisngIdx = 0;
    Wss_SenBus.Wss_SenWssMonInfo.FlFallIdx = 0;
    for(i=0;i<32;i++) Wss_SenBus.Wss_SenWssMonInfo.FlRisngTiStamp[i] = 0;   
    for(i=0;i<32;i++) Wss_SenBus.Wss_SenWssMonInfo.FlFallTiStamp[i] = 0;   
    Wss_SenBus.Wss_SenWssMonInfo.FrRisngIdx = 0;
    Wss_SenBus.Wss_SenWssMonInfo.FrFallIdx = 0;
    for(i=0;i<32;i++) Wss_SenBus.Wss_SenWssMonInfo.FrRisngTiStamp[i] = 0;   
    for(i=0;i<32;i++) Wss_SenBus.Wss_SenWssMonInfo.FrFallTiStamp[i] = 0;   
    Wss_SenBus.Wss_SenWssMonInfo.RlRisngIdx = 0;
    Wss_SenBus.Wss_SenWssMonInfo.RlFallIdx = 0;
    for(i=0;i<32;i++) Wss_SenBus.Wss_SenWssMonInfo.RlRisngTiStamp[i] = 0;   
    for(i=0;i<32;i++) Wss_SenBus.Wss_SenWssMonInfo.RlFallTiStamp[i] = 0;   
    Wss_SenBus.Wss_SenWssMonInfo.RrRisngIdx = 0;
    Wss_SenBus.Wss_SenWssMonInfo.RrFallIdx = 0;
    for(i=0;i<32;i++) Wss_SenBus.Wss_SenWssMonInfo.RrRisngTiStamp[i] = 0;   
    for(i=0;i<32;i++) Wss_SenBus.Wss_SenWssMonInfo.RrFallTiStamp[i] = 0;   
    Wss_SenBus.Wss_SenEcuModeSts = 0;
    Wss_SenBus.Wss_SenFuncInhibitWssSts = 0;
    Wss_SenBus.Wss_SenWhlPulseCntInfo.FlWhlPulseCnt = 0;
    Wss_SenBus.Wss_SenWhlPulseCntInfo.FrWhlPulseCnt = 0;
    Wss_SenBus.Wss_SenWhlPulseCntInfo.RlWhlPulseCnt = 0;
    Wss_SenBus.Wss_SenWhlPulseCntInfo.RrWhlPulseCnt = 0;
    Wss_SenBus.Wss_SenWhlSpdInfo.FlWhlSpd = 0;
    Wss_SenBus.Wss_SenWhlSpdInfo.FrWhlSpd = 0;
    Wss_SenBus.Wss_SenWhlSpdInfo.RlWhlSpd = 0;
    Wss_SenBus.Wss_SenWhlSpdInfo.RrlWhlSpd = 0;
    Wss_SenBus.Wss_SenWhlEdgeCntInfo.FlWhlEdgeCnt = 0;
    Wss_SenBus.Wss_SenWhlEdgeCntInfo.FrWhlEdgeCnt = 0;
    Wss_SenBus.Wss_SenWhlEdgeCntInfo.RlWhlEdgeCnt = 0;
    Wss_SenBus.Wss_SenWhlEdgeCntInfo.RrWhlEdgeCnt = 0;
    Wss_SenBus.Wss_SenWhlSnsrTypeInfo.FlWhlSnsrType = 0;
    Wss_SenBus.Wss_SenWhlSnsrTypeInfo.FrWhlSnsrType = 0;
    Wss_SenBus.Wss_SenWhlSnsrTypeInfo.RlWhlSnsrType = 0;
    Wss_SenBus.Wss_SenWhlSnsrTypeInfo.RrWhlSnsrType = 0;
    Wss_SenBus.Wss_SenWssSpeedOut.WssMax = 0;
    Wss_SenBus.Wss_SenWssSpeedOut.WssMin = 0;
    Wss_SenBus.Wss_SenWssCalcInfo.Rough_Sus_Flg = 0;
	Wss_Init_DecodePulse();
}

void Wss_Sen(void)
{
    uint16 i;
    
    Wss_Sen_Timer_Start = STM0_TIM0.U;

    /* Input */
    Wss_Sen_Read_Wss_SenAchWssPort0AsicInfo(&Wss_SenBus.Wss_SenAchWssPort0AsicInfo);
    /*==============================================================================
    * Members of structure Wss_SenAchWssPort0AsicInfo 
     : Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NO_FAULT;
     : Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STANDSTILL;
     : Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_LATCH_D0;
     : Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NODATA;
     : Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_INVALID;
     : Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_OPEN;
     : Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STB;
     : Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STG;
     : Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WSI_OT;
     : Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_CURRENT_HI;
     =============================================================================*/
    
    Wss_Sen_Read_Wss_SenAchWssPort1AsicInfo(&Wss_SenBus.Wss_SenAchWssPort1AsicInfo);
    /*==============================================================================
    * Members of structure Wss_SenAchWssPort1AsicInfo 
     : Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NO_FAULT;
     : Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STANDSTILL;
     : Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_LATCH_D0;
     : Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NODATA;
     : Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_INVALID;
     : Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_OPEN;
     : Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STB;
     : Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STG;
     : Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WSI_OT;
     : Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_CURRENT_HI;
     =============================================================================*/
    
    Wss_Sen_Read_Wss_SenAchWssPort2AsicInfo(&Wss_SenBus.Wss_SenAchWssPort2AsicInfo);
    /*==============================================================================
    * Members of structure Wss_SenAchWssPort2AsicInfo 
     : Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NO_FAULT;
     : Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STANDSTILL;
     : Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_LATCH_D0;
     : Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NODATA;
     : Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_INVALID;
     : Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_OPEN;
     : Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STB;
     : Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STG;
     : Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WSI_OT;
     : Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_CURRENT_HI;
     =============================================================================*/
    
    Wss_Sen_Read_Wss_SenAchWssPort3AsicInfo(&Wss_SenBus.Wss_SenAchWssPort3AsicInfo);
    /*==============================================================================
    * Members of structure Wss_SenAchWssPort3AsicInfo 
     : Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NO_FAULT;
     : Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STANDSTILL;
     : Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_LATCH_D0;
     : Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NODATA;
     : Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_INVALID;
     : Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_OPEN;
     : Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STB;
     : Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STG;
     : Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WSI_OT;
     : Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_CURRENT_HI;
     =============================================================================*/
    
    Wss_Sen_Read_Wss_SenWssMonInfo(&Wss_SenBus.Wss_SenWssMonInfo);
    /*==============================================================================
    * Members of structure Wss_SenWssMonInfo 
     : Wss_SenWssMonInfo.FlRisngIdx;
     : Wss_SenWssMonInfo.FlFallIdx;
     : Wss_SenWssMonInfo.FlRisngTiStamp;
     : Wss_SenWssMonInfo.FlFallTiStamp;
     : Wss_SenWssMonInfo.FrRisngIdx;
     : Wss_SenWssMonInfo.FrFallIdx;
     : Wss_SenWssMonInfo.FrRisngTiStamp;
     : Wss_SenWssMonInfo.FrFallTiStamp;
     : Wss_SenWssMonInfo.RlRisngIdx;
     : Wss_SenWssMonInfo.RlFallIdx;
     : Wss_SenWssMonInfo.RlRisngTiStamp;
     : Wss_SenWssMonInfo.RlFallTiStamp;
     : Wss_SenWssMonInfo.RrRisngIdx;
     : Wss_SenWssMonInfo.RrFallIdx;
     : Wss_SenWssMonInfo.RrRisngTiStamp;
     : Wss_SenWssMonInfo.RrFallTiStamp;
     =============================================================================*/
    
    Wss_Sen_Read_Wss_SenEcuModeSts(&Wss_SenBus.Wss_SenEcuModeSts);
    Wss_Sen_Read_Wss_SenFuncInhibitWssSts(&Wss_SenBus.Wss_SenFuncInhibitWssSts);

    /* Process */
    Wss_DecodePulse();
    Wss_CalcSpeed();

    /* Output */
    Wss_Sen_Write_Wss_SenWhlPulseCntInfo(&Wss_SenBus.Wss_SenWhlPulseCntInfo);
    /*==============================================================================
    * Members of structure Wss_SenWhlPulseCntInfo 
     : Wss_SenWhlPulseCntInfo.FlWhlPulseCnt;
     : Wss_SenWhlPulseCntInfo.FrWhlPulseCnt;
     : Wss_SenWhlPulseCntInfo.RlWhlPulseCnt;
     : Wss_SenWhlPulseCntInfo.RrWhlPulseCnt;
     =============================================================================*/
    
    Wss_Sen_Write_Wss_SenWhlSpdInfo(&Wss_SenBus.Wss_SenWhlSpdInfo);
    /*==============================================================================
    * Members of structure Wss_SenWhlSpdInfo 
     : Wss_SenWhlSpdInfo.FlWhlSpd;
     : Wss_SenWhlSpdInfo.FrWhlSpd;
     : Wss_SenWhlSpdInfo.RlWhlSpd;
     : Wss_SenWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/
    
    Wss_Sen_Write_Wss_SenWhlEdgeCntInfo(&Wss_SenBus.Wss_SenWhlEdgeCntInfo);
    /*==============================================================================
    * Members of structure Wss_SenWhlEdgeCntInfo 
     : Wss_SenWhlEdgeCntInfo.FlWhlEdgeCnt;
     : Wss_SenWhlEdgeCntInfo.FrWhlEdgeCnt;
     : Wss_SenWhlEdgeCntInfo.RlWhlEdgeCnt;
     : Wss_SenWhlEdgeCntInfo.RrWhlEdgeCnt;
     =============================================================================*/
    
    Wss_Sen_Write_Wss_SenWhlSnsrTypeInfo(&Wss_SenBus.Wss_SenWhlSnsrTypeInfo);
    /*==============================================================================
    * Members of structure Wss_SenWhlSnsrTypeInfo 
     : Wss_SenWhlSnsrTypeInfo.FlWhlSnsrType;
     : Wss_SenWhlSnsrTypeInfo.FrWhlSnsrType;
     : Wss_SenWhlSnsrTypeInfo.RlWhlSnsrType;
     : Wss_SenWhlSnsrTypeInfo.RrWhlSnsrType;
     =============================================================================*/
    
    Wss_Sen_Write_Wss_SenWssSpeedOut(&Wss_SenBus.Wss_SenWssSpeedOut);
    /*==============================================================================
    * Members of structure Wss_SenWssSpeedOut 
     : Wss_SenWssSpeedOut.WssMax;
     : Wss_SenWssSpeedOut.WssMin;
     =============================================================================*/
    
    Wss_Sen_Write_Wss_SenWssCalcInfo(&Wss_SenBus.Wss_SenWssCalcInfo);
    /*==============================================================================
    * Members of structure Wss_SenWssCalcInfo 
     : Wss_SenWssCalcInfo.Rough_Sus_Flg;
     =============================================================================*/
    

    Wss_Sen_Timer_Elapsed = STM0_TIM0.U - Wss_Sen_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define WSS_SEN_STOP_SEC_CODE
#include "Wss_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
