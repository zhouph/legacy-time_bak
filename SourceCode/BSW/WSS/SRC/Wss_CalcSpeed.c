/**
 * @defgroup Wss Wss
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Wss_CalcSpeed.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Wss_Sen.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define WSS_CALC_PERIOD         5
#define NUMBER_OF_WHEEL         4

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define WSS_START_SEC_CONST_UNSPECIFIED
#include "Wss_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define WSS_STOP_SEC_CONST_UNSPECIFIED
#include "Wss_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define WSS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Wss_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
static Wheel_Calc_t *WHEEL,WheelFL,WheelFR,WheelRL,WheelRR;


#define WSS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Wss_MemMap.h"
#define WSS_START_SEC_VAR_NOINIT_32BIT
#include "Wss_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define WSS_STOP_SEC_VAR_NOINIT_32BIT
#include "Wss_MemMap.h"
#define WSS_START_SEC_VAR_UNSPECIFIED
#include "Wss_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define WSS_STOP_SEC_VAR_UNSPECIFIED
#include "Wss_MemMap.h"
#define WSS_START_SEC_VAR_32BIT
#include "Wss_MemMap.h"
/** Variable Section (32BIT)**/


#define WSS_STOP_SEC_VAR_32BIT
#include "Wss_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define WSS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Wss_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
static Saluint32 Wss_MinDistanceFl, Wss_MinDistanceFr, Wss_MinDistanceRl, Wss_MinDistanceRr;
static Saluint32 Wss_SpeedConstFl, Wss_SpeedConstFr, Wss_SpeedConstRl, Wss_SpeedConstRr;
static Wss_ArrayType WhlSpeedArrInfo[NUMBER_OF_WHEEL];

#define WSS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Wss_MemMap.h"
#define WSS_START_SEC_VAR_NOINIT_32BIT
#include "Wss_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define WSS_STOP_SEC_VAR_NOINIT_32BIT
#include "Wss_MemMap.h"
#define WSS_START_SEC_VAR_UNSPECIFIED
#include "Wss_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define WSS_STOP_SEC_VAR_UNSPECIFIED
#include "Wss_MemMap.h"
#define WSS_START_SEC_VAR_32BIT
#include "Wss_MemMap.h"
/** Variable Section (32BIT)**/


#define WSS_STOP_SEC_VAR_32BIT
#include "Wss_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
Wss_ArrayType MinMaxSelectAlgorithm(uint8 ArraySize, Wss_ArrayType* WheelArray, Wss_ArrayType* iMin, Wss_ArrayType* iMax);

#define WSS_START_SEC_CODE
#include "Wss_MemMap.h"

void Wss_CalcSpeed(void);
Saluint32 Wss_CalcPeriod(Saluint8 CH, Saluint8 pulseCnt, Saluint32 pulseWidthSum, Saluint32* pulseWidthBuf,  Saluint32 SpeedConst);
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Wss_CalcSpeed(void)
{
    Wss_MinDistanceFl = \
        ((Saluint32)Wss_CalConfig.WSS_U16_TIRE_FL*3600*64)/Wss_CalConfig.WSS_U8_TEETH_FL ;
    Wss_MinDistanceFr = \
        ((Saluint32)Wss_CalConfig.WSS_U16_TIRE_FR*3600*64)/Wss_CalConfig.WSS_U8_TEETH_FR ;
    Wss_MinDistanceRl = \
        ((Saluint32)Wss_CalConfig.WSS_U16_TIRE_RL*3600*64)/Wss_CalConfig.WSS_U8_TEETH_RL ;
    Wss_MinDistanceRr = \
        ((Saluint32)Wss_CalConfig.WSS_U16_TIRE_RR*3600*64)/Wss_CalConfig.WSS_U8_TEETH_RR ;

    Wss_SpeedConstFl = Wss_MinDistanceFl;
    Wss_SpeedConstFr = Wss_MinDistanceFr;
    Wss_SpeedConstRl = Wss_MinDistanceRl;
    Wss_SpeedConstRr = Wss_MinDistanceRr;

    WheelFL.u32Period = Wss_CalcPeriod(Wss_WssChannel_Ch_FL, Wss_SenBus.Wss_SenWhlPulseCntInfo.FlWhlPulseCnt, Wss_SenBus.Wss_MainPulseWidthSum.FlPulseWitdh, &Wss_SenBus.Wss_MainPulseWidthBuf.FlPulseWitdh[0], Wss_SpeedConstFl);
    if(WheelFL.u32Period>0)
    {
        WheelFL.fsu16SpeedOfResol64 = (Saluint16)(Wss_SpeedConstFl/WheelFL.u32Period);
    }
    else
    {
        WheelFL.fsu16SpeedOfResol64 = 1;
    }

    WheelFR.u32Period = Wss_CalcPeriod(Wss_WssChannel_Ch_FR, Wss_SenBus.Wss_SenWhlPulseCntInfo.FrWhlPulseCnt, Wss_SenBus.Wss_MainPulseWidthSum.FrPulseWitdh, &Wss_SenBus.Wss_MainPulseWidthBuf.FrPulseWitdh[0], Wss_SpeedConstFr);
    if(WheelFR.u32Period>0)
    {       
        WheelFR.fsu16SpeedOfResol64 = (Saluint16)(Wss_SpeedConstFr/WheelFR.u32Period);
    }
    else
    {
        WheelFR.fsu16SpeedOfResol64 = 1;
    }
        
    WheelRL.u32Period = Wss_CalcPeriod(Wss_WssChannel_Ch_RL, Wss_SenBus.Wss_SenWhlPulseCntInfo.RlWhlPulseCnt, Wss_SenBus.Wss_MainPulseWidthSum.RlPulseWitdh, &Wss_SenBus.Wss_MainPulseWidthBuf.RlPulseWitdh[0], Wss_SpeedConstRl);
    if(WheelRL.u32Period>0)
    {       
        WheelRL.fsu16SpeedOfResol64 = (Saluint16)(Wss_SpeedConstRl/WheelRL.u32Period);
    }
    else
    {
        WheelRL.fsu16SpeedOfResol64 = 1;
    }
        
    WheelRR.u32Period = Wss_CalcPeriod(Wss_WssChannel_Ch_RR, Wss_SenBus.Wss_SenWhlPulseCntInfo.RrWhlPulseCnt, Wss_SenBus.Wss_MainPulseWidthSum.RrPulseWitdh, &Wss_SenBus.Wss_MainPulseWidthBuf.RrPulseWitdh[0], Wss_SpeedConstRr);
    if(WheelRR.u32Period>0)
    {       
        WheelRR.fsu16SpeedOfResol64 = (Saluint16)(Wss_SpeedConstRr/WheelRR.u32Period);
    }
    else
    {
        WheelRR.fsu16SpeedOfResol64 = 1;
    }

    WhlSpeedArrInfo[0] = Wss_SenBus.Wss_SenWhlSpdInfo.FlWhlSpd = WheelFL.fsu16SpeedOfResol64;
    WhlSpeedArrInfo[1] = Wss_SenBus.Wss_SenWhlSpdInfo.FrWhlSpd = WheelFR.fsu16SpeedOfResol64;
    WhlSpeedArrInfo[2] = Wss_SenBus.Wss_SenWhlSpdInfo.RlWhlSpd = WheelRL.fsu16SpeedOfResol64;
    WhlSpeedArrInfo[3] = Wss_SenBus.Wss_SenWhlSpdInfo.RrlWhlSpd = WheelRR.fsu16SpeedOfResol64;

    MinMaxSelectAlgorithm(NUMBER_OF_WHEEL, &WhlSpeedArrInfo[0], &Wss_SenBus.Wss_SenWssSpeedOut.WssMin, &Wss_SenBus.Wss_SenWssSpeedOut.WssMax);
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
Saluint32 Wss_CalcPeriod(Saluint8 CH, Saluint8 pulseCnt, Saluint32 pulseWidthSum, Saluint32* pulseWidthBuf,  Saluint32 SpeedConst)
{
    Saluint16 i=0;
    Saluint32 fu32Period=0;
    Wheel_Calc_t *pWheel = NULL_PTR;

    if(CH == Wss_WssChannel_Ch_FL)
    {
        pWheel   = &WheelFL;
    }
    if(CH == Wss_WssChannel_Ch_FR)
    {
        pWheel   = &WheelFR;
    }
    if(CH == Wss_WssChannel_Ch_RL)
    {
        pWheel   = &WheelRL;
    }
    if(CH == Wss_WssChannel_Ch_RR)
    {
        pWheel   = &WheelRR;
    }
    else
    {
        ;
    }
 
    if(pWheel != NULL_PTR)
    {
        pWheel->u8PulseCnt = pulseCnt;

        pWheel->u32PulseWidthSum = pulseWidthSum;

        for(i=0;i<WSS_PULSE_BUF_NUM;i++)
        {
            pWheel->u32PulseWidthBuf[i] = pulseWidthBuf[i];
        }
        
        if(pWheel->u8PulseCnt > 0)
        {
            pWheel->u32PeriodTempStred=0;
        }
        if(pWheel->u8PulseStartStep<2)
        {
            pWheel->u8PulseStartStep+=pWheel->u8PulseCnt;
        }

        if(pWheel->u8PulseStartStep>=2)
        {
            if(pWheel->u8PulseCnt >= 2)
            {
                if((pWheel->u8PulseCnt%2)==0)
                {
                    /*Pulse Count Even*/
                    fu32Period = (pWheel->u32PulseWidthSum/pWheel->u8PulseCnt)*2;
                }
                else
                {
                    /*Pulse Count Odd*/
                    fu32Period = ((pWheel->u32PulseWidthSum-pWheel->u32PulseWidthBuf[0])/(pWheel->u8PulseCnt-1))*2;
                }            
            }
            else if(pWheel->u8PulseCnt>0)
            {

                fu32Period = pWheel->u32PulseWidthBuf[0]+pWheel->u32PulseWidthBuf[1];
            }
            else
            {
                 pWheel->u32PeriodTempStred+=(WSS_CALC_PERIOD*1000);

                if(pWheel->u32PeriodTempStred>pWheel->u32Period)
                {
                        fu32Period = pWheel->u32Period + (WSS_CALC_PERIOD*1000);
                }
                else
                {
                    fu32Period = pWheel->u32Period;
                }
            }     
        }
        else if(pWheel->u8PulseStartStep==1)
        {
            fu32Period=SpeedConst;
            /* TBD 141106 seonho.hong
            if(SPAS_INLINE_STATE==0)
            {
                fu32Period=SpeedConst;
            }
            else
            {
                if(pWheel->u8PulseCnt == 0)
                {
                    pWheel->u32PeriodTempStred+=(WSS_CALC_PERIOD*1000);

                    if(pWheel->u32PeriodTempStred>pWheel->u32Period)
                    {
                            fu32Period = pWheel->u32Period + (WSS_CALC_PERIOD*1000);
                    }
                    else
                    {
                        fu32Period = pWheel->u32Period;
                    }    
                }
                else if(pWheel->u8PulseCnt == 1)
                {
                    fu32Period = pWheel->u32PulseWidthBuf[0]*2;
                }
                else
                {
                    ;
                }
            }
            */
        }
        else
        {
            fu32Period=SpeedConst;
        }
            
        if(fu32Period>SpeedConst)
        {
            pWheel->u8PulseStartStep=0;
            fu32Period=SpeedConst;
        }
        else
        {
            ;
        }
        
        if(pWheel->u32PulseWidthBuf[1]>0)
        {
            pWheel->u16ToneWheelDeviation = (Saluint16)((pWheel->u32PulseWidthBuf[0]*100ul)/pWheel->u32PulseWidthBuf[1]);
        }

     }

    return fu32Period;
}

Wss_ArrayType MinMaxSelectAlgorithm(uint8 ArraySize, Wss_ArrayType* WheelArray, Wss_ArrayType* iMin, Wss_ArrayType* iMax)
{
    *iMax = WheelArray[0];
    *iMin = WheelArray[0];

    uint8 index;
    for (index=1; index<ArraySize; index++)
    {
        if (WheelArray[index] < *iMin)
        {
            *iMin = WheelArray[index];
        }    
        if (WheelArray[index] > *iMax)
        {
            *iMax = WheelArray[index];
        }
    }

    return 0;
}

#define WSS_STOP_SEC_CODE
#include "Wss_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
