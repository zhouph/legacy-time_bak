/**
 * @defgroup Wss Wss
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Wss_DecodePulse.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Wss_Sen.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define WSS_TIMESTAMP_BITMASK  0x00FFFFFF
#define WSS_PULSE_CLEAR_CNT         0xFF    /* 0~254 , 0xFF: Invalid Signal on the CAN Tx Signal of Wheel Pulse Count */

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define WSS_START_SEC_CONST_UNSPECIFIED
#include "Wss_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define WSS_STOP_SEC_CONST_UNSPECIFIED
#include "Wss_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define WSS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Wss_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

static Wss_IdxType Wss_RisingIdxOld[Wss_WssChannel_MaxNum] = {31,31,31,31};
static Wss_IdxType Wss_FallingIdxOld[Wss_WssChannel_MaxNum] = {31,31,31,31};

static Wss_CntType Wss_PulseCnt[Wss_WssChannel_MaxNum];
static Wss_EdgeType Wss_WheelEdgeCnt[Wss_WssChannel_MaxNum] = {0,0,0,0};


#define WSS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Wss_MemMap.h"
#define WSS_START_SEC_VAR_NOINIT_32BIT
#include "Wss_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define WSS_STOP_SEC_VAR_NOINIT_32BIT
#include "Wss_MemMap.h"
#define WSS_START_SEC_VAR_UNSPECIFIED
#include "Wss_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define WSS_STOP_SEC_VAR_UNSPECIFIED
#include "Wss_MemMap.h"
#define WSS_START_SEC_VAR_32BIT
#include "Wss_MemMap.h"
/** Variable Section (32BIT)**/


#define WSS_STOP_SEC_VAR_32BIT
#include "Wss_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define WSS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Wss_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define WSS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Wss_MemMap.h"
#define WSS_START_SEC_VAR_NOINIT_32BIT
#include "Wss_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define WSS_STOP_SEC_VAR_NOINIT_32BIT
#include "Wss_MemMap.h"
#define WSS_START_SEC_VAR_UNSPECIFIED
#include "Wss_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define WSS_STOP_SEC_VAR_UNSPECIFIED
#include "Wss_MemMap.h"
#define WSS_START_SEC_VAR_32BIT
#include "Wss_MemMap.h"
/** Variable Section (32BIT)**/


#define WSS_STOP_SEC_VAR_32BIT
#include "Wss_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define WSS_START_SEC_CODE
#include "Wss_MemMap.h"
void Wss_Init_DecodePulse(void);
void Wss_DecodePulse(void);

static void Wss_ChDecodePulse(Wss_ChannelType channel, Wss_IdxType rIdx, Wss_IdxType fIdx, Wss_ArrayType* rTiStamp, Wss_ArrayType* fTiStamp, Saluint32* pulseWidthSum, Saluint32* pulseWidthBuf);
static Wss_ArrayType Wss_SubtractTimeStamp(Wss_ArrayType operand1, Wss_ArrayType operand2);
static Wss_IdxType Wss_MinusRingBufIdx(Wss_IdxType operand1, Wss_IdxType operand2);
static Wss_IdxType Wss_PlusRingBufIdx(Wss_IdxType operand1, Wss_IdxType operand2);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
/************************************************************************
 * FUNCTION NAME:      WB_InitWheelSnr
 * CALLED BY:          WM_vInitVariables
 * Preconditions:      none
 * PARAMETER:          Handle of initialization of variables related to wheel sor
 * RETURN VALUE:       none
 * Description:        Initialize Pointer, valiables ...
 *************************************************************************/
void Wss_Init_DecodePulse(void)
{
    Wss_ChannelType channel;

#if 0
    for(channel = 0 ; channel < Wss_WssChannel_MaxNum ; channel++)
    {
        /* Wss Rising, Falling Index Initialization */
        Wss_RisingIdxOld[channel]   = (WSS_TIMESTAMP_BUF_SIZE - 1);
        Wss_FallingIdxOld[channel]  = (WSS_TIMESTAMP_BUF_SIZE - 1);
        /* Wss Rising, Falling Timestamp Buffer Initialization */
    }
    Wss_SenBus.Wss_SenWssMonInfo.FlRisngIdx = (WSS_TIMESTAMP_BUF_SIZE - 1);
    Wss_SenBus.Wss_SenWssMonInfo.FrRisngIdx = (WSS_TIMESTAMP_BUF_SIZE - 1);
    Wss_SenBus.Wss_SenWssMonInfo.RlRisngIdx = (WSS_TIMESTAMP_BUF_SIZE - 1);
    Wss_SenBus.Wss_SenWssMonInfo.RrRisngIdx = (WSS_TIMESTAMP_BUF_SIZE - 1);

    Wss_SenBus.Wss_SenWssMonInfo.FlFallIdx = (WSS_TIMESTAMP_BUF_SIZE - 1);
    Wss_SenBus.Wss_SenWssMonInfo.FrFallIdx = (WSS_TIMESTAMP_BUF_SIZE - 1);
    Wss_SenBus.Wss_SenWssMonInfo.RlFallIdx = (WSS_TIMESTAMP_BUF_SIZE - 1);
    Wss_SenBus.Wss_SenWssMonInfo.RrFallIdx = (WSS_TIMESTAMP_BUF_SIZE - 1);
#endif

    /* WsSpeed Sensor Type Copy from Wss Configuration Data*/
    Wss_SenBus.Wss_SenWhlSnsrTypeInfo.FlWhlSnsrType = (Saluint8)Wss_ChannelConfig[Wss_WssChannel_Ch_FL].WssSensorType;
    Wss_SenBus.Wss_SenWhlSnsrTypeInfo.FrWhlSnsrType = (Saluint8)Wss_ChannelConfig[Wss_WssChannel_Ch_FR].WssSensorType;
    Wss_SenBus.Wss_SenWhlSnsrTypeInfo.RlWhlSnsrType = (Saluint8)Wss_ChannelConfig[Wss_WssChannel_Ch_RL].WssSensorType;
    Wss_SenBus.Wss_SenWhlSnsrTypeInfo.RrWhlSnsrType = (Saluint8)Wss_ChannelConfig[Wss_WssChannel_Ch_RR].WssSensorType;

}
void Wss_DecodePulse(void)
{
    Wss_ChDecodePulse(Wss_WssChannel_Ch_FL, Wss_SenBus.Wss_SenWssMonInfo.FlRisngIdx, Wss_SenBus.Wss_SenWssMonInfo.FlFallIdx, &Wss_SenBus.Wss_SenWssMonInfo.FlRisngTiStamp[0], &Wss_SenBus.Wss_SenWssMonInfo.FlFallTiStamp[0], &Wss_SenBus.Wss_MainPulseWidthSum.FlPulseWitdh, &Wss_SenBus.Wss_MainPulseWidthBuf.FlPulseWitdh[0]);
    Wss_ChDecodePulse(Wss_WssChannel_Ch_FR, Wss_SenBus.Wss_SenWssMonInfo.FrRisngIdx, Wss_SenBus.Wss_SenWssMonInfo.FrFallIdx, &Wss_SenBus.Wss_SenWssMonInfo.FrRisngTiStamp[0], &Wss_SenBus.Wss_SenWssMonInfo.FrFallTiStamp[0], &Wss_SenBus.Wss_MainPulseWidthSum.FrPulseWitdh, &Wss_SenBus.Wss_MainPulseWidthBuf.FrPulseWitdh[0]);
    Wss_ChDecodePulse(Wss_WssChannel_Ch_RL, Wss_SenBus.Wss_SenWssMonInfo.RlRisngIdx, Wss_SenBus.Wss_SenWssMonInfo.RlFallIdx, &Wss_SenBus.Wss_SenWssMonInfo.RlRisngTiStamp[0], &Wss_SenBus.Wss_SenWssMonInfo.RlFallTiStamp[0], &Wss_SenBus.Wss_MainPulseWidthSum.RlPulseWitdh, &Wss_SenBus.Wss_MainPulseWidthBuf.RlPulseWitdh[0]);
    Wss_ChDecodePulse(Wss_WssChannel_Ch_RR, Wss_SenBus.Wss_SenWssMonInfo.RrRisngIdx, Wss_SenBus.Wss_SenWssMonInfo.RrFallIdx, &Wss_SenBus.Wss_SenWssMonInfo.RrRisngTiStamp[0], &Wss_SenBus.Wss_SenWssMonInfo.RrFallTiStamp[0], &Wss_SenBus.Wss_MainPulseWidthSum.RrPulseWitdh, &Wss_SenBus.Wss_MainPulseWidthBuf.RrPulseWitdh[0]);

    Wss_SenBus.Wss_SenWhlPulseCntInfo.FlWhlPulseCnt = Wss_PulseCnt[Wss_WssChannel_Ch_FL];
    Wss_SenBus.Wss_SenWhlPulseCntInfo.FrWhlPulseCnt = Wss_PulseCnt[Wss_WssChannel_Ch_FR];
    Wss_SenBus.Wss_SenWhlPulseCntInfo.RlWhlPulseCnt = Wss_PulseCnt[Wss_WssChannel_Ch_RL];
    Wss_SenBus.Wss_SenWhlPulseCntInfo.RrWhlPulseCnt = Wss_PulseCnt[Wss_WssChannel_Ch_RR];
    Wss_SenBus.Wss_SenWhlEdgeCntInfo.FlWhlEdgeCnt = Wss_WheelEdgeCnt[Wss_WssChannel_Ch_FL];
    Wss_SenBus.Wss_SenWhlEdgeCntInfo.FrWhlEdgeCnt = Wss_WheelEdgeCnt[Wss_WssChannel_Ch_FR];
    Wss_SenBus.Wss_SenWhlEdgeCntInfo.RlWhlEdgeCnt = Wss_WheelEdgeCnt[Wss_WssChannel_Ch_RL];
    Wss_SenBus.Wss_SenWhlEdgeCntInfo.RrWhlEdgeCnt = Wss_WheelEdgeCnt[Wss_WssChannel_Ch_RR];
    

}

void Wss_ChDecodePulse(Wss_ChannelType channel, Wss_IdxType rIdx, Wss_IdxType fIdx, Wss_ArrayType* rTiStamp, Wss_ArrayType* fTiStamp, Saluint32* pulseWidthSum, Saluint32* pulseWidthBuf)
{

    Wss_ArrayType TimeStamp, TimeStampOld;

    /* Calculation the Pulse Count */
    Wss_PulseCnt[channel] = Wss_MinusRingBufIdx(rIdx, Wss_RisingIdxOld[channel]) + Wss_MinusRingBufIdx(fIdx, Wss_FallingIdxOld[channel]) ; 
    /* Calculation the Edge Count Sum for CAN Tx Wheel Pulse */
    Wss_WheelEdgeCnt[channel] = (Wss_WheelEdgeCnt[channel] + Wss_PulseCnt[channel]) % WSS_PULSE_CLEAR_CNT;

     /* Select the Previous and Current Time Stamp Value among the Rising and Falling Time Stamp Buffer */
    if(Wss_RisingIdxOld[channel] != Wss_FallingIdxOld[channel])
    {
        TimeStampOld = rTiStamp[Wss_RisingIdxOld[channel]];
    }
    else
    {
        TimeStampOld = fTiStamp[Wss_FallingIdxOld[channel]];
    }
    if(rIdx != fIdx)
    {
        TimeStamp = rTiStamp[rIdx];
    }
    else
    {
        TimeStamp = fTiStamp[fIdx];
    }
    /* Calculation the Pulse Width Sum with Current and Previous Time Stamp Value */
    *pulseWidthSum = Wss_SubtractTimeStamp(TimeStamp, TimeStampOld);        

    /* Calculate and Save the PulseWidth in last 4 times to Pulse Width Buffer*/
    if(rIdx != fIdx)
    {
        pulseWidthBuf[3] = Wss_SubtractTimeStamp(fTiStamp[Wss_MinusRingBufIdx(fIdx,1)] , rTiStamp[Wss_MinusRingBufIdx(rIdx,2)]);
        pulseWidthBuf[2] = Wss_SubtractTimeStamp(rTiStamp[Wss_MinusRingBufIdx(rIdx,1)] , fTiStamp[Wss_MinusRingBufIdx(fIdx,1)]);
        pulseWidthBuf[1] = Wss_SubtractTimeStamp(fTiStamp[fIdx] , rTiStamp[Wss_MinusRingBufIdx(rIdx,1)]);
        pulseWidthBuf[0] = Wss_SubtractTimeStamp(rTiStamp[rIdx] , fTiStamp[fIdx]);
    }
    else
    {
        pulseWidthBuf[3] = Wss_SubtractTimeStamp(rTiStamp[Wss_MinusRingBufIdx(rIdx,1)] , fTiStamp[Wss_MinusRingBufIdx(fIdx,2)]);
        pulseWidthBuf[2] = Wss_SubtractTimeStamp(fTiStamp[Wss_MinusRingBufIdx(fIdx,1)] , rTiStamp[Wss_MinusRingBufIdx(rIdx,1)]);
        pulseWidthBuf[1] = Wss_SubtractTimeStamp(rTiStamp[rIdx] , fTiStamp[Wss_MinusRingBufIdx(fIdx,1)]);
        pulseWidthBuf[0] = Wss_SubtractTimeStamp(fTiStamp[fIdx] , rTiStamp[rIdx]);
    }
    /* Old Buffer Index Setting */
    Wss_RisingIdxOld[channel]  = rIdx;
    Wss_FallingIdxOld[channel] = fIdx;       
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static Wss_IdxType Wss_MinusRingBufIdx(Wss_IdxType operand1, Wss_IdxType operand2)
{
    Wss_IdxType result_val;

    if (operand1 >= operand2) 
    {
        result_val = operand1 - operand2;
    }
    else 
    {
        result_val = WSS_TIMESTAMP_BUF_SIZE - (operand2 - operand1);
    }

    return result_val;
}

static Wss_IdxType Wss_PlusRingBufIdx(Wss_IdxType operand1, Wss_IdxType operand2)
{
    Wss_IdxType result_val, u16temp;

    /* Key : operand1+operand2 should be less than "WSS_TIMESTAMP_BUF_SIZE"*/
    u16temp = operand1 + operand2;

    if (u16temp >= WSS_TIMESTAMP_BUF_SIZE) 
    {
        result_val = u16temp - WSS_TIMESTAMP_BUF_SIZE;
    } 
    else 
    {
        result_val = u16temp;
    }

    return result_val;
}

static Wss_ArrayType Wss_SubtractTimeStamp(Wss_ArrayType operand1, Wss_ArrayType operand2)
{
    Wss_ArrayType result_val;
    
    result_val = (operand1 - operand2) & WSS_TIMESTAMP_BITMASK;
    
    return result_val;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define WSS_STOP_SEC_CODE
#include "Wss_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
