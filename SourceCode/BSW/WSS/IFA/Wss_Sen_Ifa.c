/**
 * @defgroup Wss_Sen_Ifa Wss_Sen_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Wss_Sen_Ifa.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Wss_Sen_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define WSS_SEN_START_SEC_CONST_UNSPECIFIED
#include "Wss_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define WSS_SEN_STOP_SEC_CONST_UNSPECIFIED
#include "Wss_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define WSS_SEN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Wss_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define WSS_SEN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Wss_MemMap.h"
#define WSS_SEN_START_SEC_VAR_NOINIT_32BIT
#include "Wss_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define WSS_SEN_STOP_SEC_VAR_NOINIT_32BIT
#include "Wss_MemMap.h"
#define WSS_SEN_START_SEC_VAR_UNSPECIFIED
#include "Wss_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define WSS_SEN_STOP_SEC_VAR_UNSPECIFIED
#include "Wss_MemMap.h"
#define WSS_SEN_START_SEC_VAR_32BIT
#include "Wss_MemMap.h"
/** Variable Section (32BIT)**/


#define WSS_SEN_STOP_SEC_VAR_32BIT
#include "Wss_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define WSS_SEN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Wss_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define WSS_SEN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Wss_MemMap.h"
#define WSS_SEN_START_SEC_VAR_NOINIT_32BIT
#include "Wss_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define WSS_SEN_STOP_SEC_VAR_NOINIT_32BIT
#include "Wss_MemMap.h"
#define WSS_SEN_START_SEC_VAR_UNSPECIFIED
#include "Wss_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define WSS_SEN_STOP_SEC_VAR_UNSPECIFIED
#include "Wss_MemMap.h"
#define WSS_SEN_START_SEC_VAR_32BIT
#include "Wss_MemMap.h"
/** Variable Section (32BIT)**/


#define WSS_SEN_STOP_SEC_VAR_32BIT
#include "Wss_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define WSS_SEN_START_SEC_CODE
#include "Wss_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define WSS_SEN_STOP_SEC_CODE
#include "Wss_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
