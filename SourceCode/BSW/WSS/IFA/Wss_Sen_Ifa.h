/**
 * @defgroup Wss_Sen_Ifa Wss_Sen_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Wss_Sen_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef WSS_SEN_IFA_H_
#define WSS_SEN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Wss_Sen_Read_Wss_SenAchWssPort0AsicInfo(data) do \
{ \
    *data = Wss_SenAchWssPort0AsicInfo; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort1AsicInfo(data) do \
{ \
    *data = Wss_SenAchWssPort1AsicInfo; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort2AsicInfo(data) do \
{ \
    *data = Wss_SenAchWssPort2AsicInfo; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort3AsicInfo(data) do \
{ \
    *data = Wss_SenAchWssPort3AsicInfo; \
}while(0);

#define Wss_Sen_Read_Wss_SenWssMonInfo(data) do \
{ \
    *data = Wss_SenWssMonInfo; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_NO_FAULT(data) do \
{ \
    *data = Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NO_FAULT; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_WHEEL_SPEED_DECODER_DATA(data) do \
{ \
    *data = Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WHEEL_SPEED_DECODER_DATA; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_STANDSTILL(data) do \
{ \
    *data = Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STANDSTILL; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_LATCH_D0(data) do \
{ \
    *data = Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_LATCH_D0; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_NODATA(data) do \
{ \
    *data = Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NODATA; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_INVALID(data) do \
{ \
    *data = Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_INVALID; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_OPEN(data) do \
{ \
    *data = Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_OPEN; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_STB(data) do \
{ \
    *data = Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STB; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_STG(data) do \
{ \
    *data = Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STG; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_WSI_OT(data) do \
{ \
    *data = Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WSI_OT; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_CURRENT_HI(data) do \
{ \
    *data = Wss_SenAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_CURRENT_HI; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_NO_FAULT(data) do \
{ \
    *data = Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NO_FAULT; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_WHEEL_SPEED_DECODER_DATA(data) do \
{ \
    *data = Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WHEEL_SPEED_DECODER_DATA; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_STANDSTILL(data) do \
{ \
    *data = Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STANDSTILL; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_LATCH_D0(data) do \
{ \
    *data = Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_LATCH_D0; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_NODATA(data) do \
{ \
    *data = Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NODATA; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_INVALID(data) do \
{ \
    *data = Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_INVALID; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_OPEN(data) do \
{ \
    *data = Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_OPEN; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_STB(data) do \
{ \
    *data = Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STB; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_STG(data) do \
{ \
    *data = Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STG; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_WSI_OT(data) do \
{ \
    *data = Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WSI_OT; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_CURRENT_HI(data) do \
{ \
    *data = Wss_SenAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_CURRENT_HI; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_NO_FAULT(data) do \
{ \
    *data = Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NO_FAULT; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_WHEEL_SPEED_DECODER_DATA(data) do \
{ \
    *data = Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WHEEL_SPEED_DECODER_DATA; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_STANDSTILL(data) do \
{ \
    *data = Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STANDSTILL; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_LATCH_D0(data) do \
{ \
    *data = Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_LATCH_D0; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_NODATA(data) do \
{ \
    *data = Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NODATA; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_INVALID(data) do \
{ \
    *data = Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_INVALID; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_OPEN(data) do \
{ \
    *data = Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_OPEN; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_STB(data) do \
{ \
    *data = Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STB; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_STG(data) do \
{ \
    *data = Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STG; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_WSI_OT(data) do \
{ \
    *data = Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WSI_OT; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_CURRENT_HI(data) do \
{ \
    *data = Wss_SenAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_CURRENT_HI; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_NO_FAULT(data) do \
{ \
    *data = Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NO_FAULT; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_WHEEL_SPEED_DECODER_DATA(data) do \
{ \
    *data = Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WHEEL_SPEED_DECODER_DATA; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_STANDSTILL(data) do \
{ \
    *data = Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STANDSTILL; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_LATCH_D0(data) do \
{ \
    *data = Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_LATCH_D0; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_NODATA(data) do \
{ \
    *data = Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NODATA; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_INVALID(data) do \
{ \
    *data = Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_INVALID; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_OPEN(data) do \
{ \
    *data = Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_OPEN; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_STB(data) do \
{ \
    *data = Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STB; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_STG(data) do \
{ \
    *data = Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STG; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_WSI_OT(data) do \
{ \
    *data = Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WSI_OT; \
}while(0);

#define Wss_Sen_Read_Wss_SenAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_CURRENT_HI(data) do \
{ \
    *data = Wss_SenAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_CURRENT_HI; \
}while(0);

#define Wss_Sen_Read_Wss_SenWssMonInfo_FlRisngIdx(data) do \
{ \
    *data = Wss_SenWssMonInfo.FlRisngIdx; \
}while(0);

#define Wss_Sen_Read_Wss_SenWssMonInfo_FlFallIdx(data) do \
{ \
    *data = Wss_SenWssMonInfo.FlFallIdx; \
}while(0);

#define Wss_Sen_Read_Wss_SenWssMonInfo_FlRisngTiStamp(data) do \
{ \
    for(i=0;i<32;i++) *data[i] = Wss_SenWssMonInfo.FlRisngTiStamp[i]; \
}while(0);

#define Wss_Sen_Read_Wss_SenWssMonInfo_FlFallTiStamp(data) do \
{ \
    for(i=0;i<32;i++) *data[i] = Wss_SenWssMonInfo.FlFallTiStamp[i]; \
}while(0);

#define Wss_Sen_Read_Wss_SenWssMonInfo_FrRisngIdx(data) do \
{ \
    *data = Wss_SenWssMonInfo.FrRisngIdx; \
}while(0);

#define Wss_Sen_Read_Wss_SenWssMonInfo_FrFallIdx(data) do \
{ \
    *data = Wss_SenWssMonInfo.FrFallIdx; \
}while(0);

#define Wss_Sen_Read_Wss_SenWssMonInfo_FrRisngTiStamp(data) do \
{ \
    for(i=0;i<32;i++) *data[i] = Wss_SenWssMonInfo.FrRisngTiStamp[i]; \
}while(0);

#define Wss_Sen_Read_Wss_SenWssMonInfo_FrFallTiStamp(data) do \
{ \
    for(i=0;i<32;i++) *data[i] = Wss_SenWssMonInfo.FrFallTiStamp[i]; \
}while(0);

#define Wss_Sen_Read_Wss_SenWssMonInfo_RlRisngIdx(data) do \
{ \
    *data = Wss_SenWssMonInfo.RlRisngIdx; \
}while(0);

#define Wss_Sen_Read_Wss_SenWssMonInfo_RlFallIdx(data) do \
{ \
    *data = Wss_SenWssMonInfo.RlFallIdx; \
}while(0);

#define Wss_Sen_Read_Wss_SenWssMonInfo_RlRisngTiStamp(data) do \
{ \
    for(i=0;i<32;i++) *data[i] = Wss_SenWssMonInfo.RlRisngTiStamp[i]; \
}while(0);

#define Wss_Sen_Read_Wss_SenWssMonInfo_RlFallTiStamp(data) do \
{ \
    for(i=0;i<32;i++) *data[i] = Wss_SenWssMonInfo.RlFallTiStamp[i]; \
}while(0);

#define Wss_Sen_Read_Wss_SenWssMonInfo_RrRisngIdx(data) do \
{ \
    *data = Wss_SenWssMonInfo.RrRisngIdx; \
}while(0);

#define Wss_Sen_Read_Wss_SenWssMonInfo_RrFallIdx(data) do \
{ \
    *data = Wss_SenWssMonInfo.RrFallIdx; \
}while(0);

#define Wss_Sen_Read_Wss_SenWssMonInfo_RrRisngTiStamp(data) do \
{ \
    for(i=0;i<32;i++) *data[i] = Wss_SenWssMonInfo.RrRisngTiStamp[i]; \
}while(0);

#define Wss_Sen_Read_Wss_SenWssMonInfo_RrFallTiStamp(data) do \
{ \
    for(i=0;i<32;i++) *data[i] = Wss_SenWssMonInfo.RrFallTiStamp[i]; \
}while(0);

#define Wss_Sen_Read_Wss_SenEcuModeSts(data) do \
{ \
    *data = Wss_SenEcuModeSts; \
}while(0);

#define Wss_Sen_Read_Wss_SenFuncInhibitWssSts(data) do \
{ \
    *data = Wss_SenFuncInhibitWssSts; \
}while(0);


/* Set Output DE MAcro Function */
#define Wss_Sen_Write_Wss_SenWhlPulseCntInfo(data) do \
{ \
    Wss_SenWhlPulseCntInfo = *data; \
}while(0);

#define Wss_Sen_Write_Wss_SenWhlSpdInfo(data) do \
{ \
    Wss_SenWhlSpdInfo = *data; \
}while(0);

#define Wss_Sen_Write_Wss_SenWhlEdgeCntInfo(data) do \
{ \
    Wss_SenWhlEdgeCntInfo = *data; \
}while(0);

#define Wss_Sen_Write_Wss_SenWhlSnsrTypeInfo(data) do \
{ \
    Wss_SenWhlSnsrTypeInfo = *data; \
}while(0);

#define Wss_Sen_Write_Wss_SenWssSpeedOut(data) do \
{ \
    Wss_SenWssSpeedOut = *data; \
}while(0);

#define Wss_Sen_Write_Wss_SenWssCalcInfo(data) do \
{ \
    Wss_SenWssCalcInfo = *data; \
}while(0);

#define Wss_Sen_Write_Wss_SenWhlPulseCntInfo_FlWhlPulseCnt(data) do \
{ \
    Wss_SenWhlPulseCntInfo.FlWhlPulseCnt = *data; \
}while(0);

#define Wss_Sen_Write_Wss_SenWhlPulseCntInfo_FrWhlPulseCnt(data) do \
{ \
    Wss_SenWhlPulseCntInfo.FrWhlPulseCnt = *data; \
}while(0);

#define Wss_Sen_Write_Wss_SenWhlPulseCntInfo_RlWhlPulseCnt(data) do \
{ \
    Wss_SenWhlPulseCntInfo.RlWhlPulseCnt = *data; \
}while(0);

#define Wss_Sen_Write_Wss_SenWhlPulseCntInfo_RrWhlPulseCnt(data) do \
{ \
    Wss_SenWhlPulseCntInfo.RrWhlPulseCnt = *data; \
}while(0);

#define Wss_Sen_Write_Wss_SenWhlSpdInfo_FlWhlSpd(data) do \
{ \
    Wss_SenWhlSpdInfo.FlWhlSpd = *data; \
}while(0);

#define Wss_Sen_Write_Wss_SenWhlSpdInfo_FrWhlSpd(data) do \
{ \
    Wss_SenWhlSpdInfo.FrWhlSpd = *data; \
}while(0);

#define Wss_Sen_Write_Wss_SenWhlSpdInfo_RlWhlSpd(data) do \
{ \
    Wss_SenWhlSpdInfo.RlWhlSpd = *data; \
}while(0);

#define Wss_Sen_Write_Wss_SenWhlSpdInfo_RrlWhlSpd(data) do \
{ \
    Wss_SenWhlSpdInfo.RrlWhlSpd = *data; \
}while(0);

#define Wss_Sen_Write_Wss_SenWhlEdgeCntInfo_FlWhlEdgeCnt(data) do \
{ \
    Wss_SenWhlEdgeCntInfo.FlWhlEdgeCnt = *data; \
}while(0);

#define Wss_Sen_Write_Wss_SenWhlEdgeCntInfo_FrWhlEdgeCnt(data) do \
{ \
    Wss_SenWhlEdgeCntInfo.FrWhlEdgeCnt = *data; \
}while(0);

#define Wss_Sen_Write_Wss_SenWhlEdgeCntInfo_RlWhlEdgeCnt(data) do \
{ \
    Wss_SenWhlEdgeCntInfo.RlWhlEdgeCnt = *data; \
}while(0);

#define Wss_Sen_Write_Wss_SenWhlEdgeCntInfo_RrWhlEdgeCnt(data) do \
{ \
    Wss_SenWhlEdgeCntInfo.RrWhlEdgeCnt = *data; \
}while(0);

#define Wss_Sen_Write_Wss_SenWhlSnsrTypeInfo_FlWhlSnsrType(data) do \
{ \
    Wss_SenWhlSnsrTypeInfo.FlWhlSnsrType = *data; \
}while(0);

#define Wss_Sen_Write_Wss_SenWhlSnsrTypeInfo_FrWhlSnsrType(data) do \
{ \
    Wss_SenWhlSnsrTypeInfo.FrWhlSnsrType = *data; \
}while(0);

#define Wss_Sen_Write_Wss_SenWhlSnsrTypeInfo_RlWhlSnsrType(data) do \
{ \
    Wss_SenWhlSnsrTypeInfo.RlWhlSnsrType = *data; \
}while(0);

#define Wss_Sen_Write_Wss_SenWhlSnsrTypeInfo_RrWhlSnsrType(data) do \
{ \
    Wss_SenWhlSnsrTypeInfo.RrWhlSnsrType = *data; \
}while(0);

#define Wss_Sen_Write_Wss_SenWssSpeedOut_WssMax(data) do \
{ \
    Wss_SenWssSpeedOut.WssMax = *data; \
}while(0);

#define Wss_Sen_Write_Wss_SenWssSpeedOut_WssMin(data) do \
{ \
    Wss_SenWssSpeedOut.WssMin = *data; \
}while(0);

#define Wss_Sen_Write_Wss_SenWssCalcInfo_Rough_Sus_Flg(data) do \
{ \
    Wss_SenWssCalcInfo.Rough_Sus_Flg = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* WSS_SEN_IFA_H_ */
/** @} */
