#include "unity.h"
#include "unity_fixture.h"
#include "SysPwrM_Main.h"
#include "SysPwrM_Main_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Haluint8 UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_ovc[MAX_STEP] = SYSPWRM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD1_OVC;
const Haluint8 UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_out_of_reg[MAX_STEP] = SYSPWRM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD2_OUT_OF_REG;
const Haluint8 UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ovc[MAX_STEP] = SYSPWRM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD3_OVC;
const Haluint8 UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_ovc[MAX_STEP] = SYSPWRM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD4_OVC;
const Haluint8 UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_diode_loss[MAX_STEP] = SYSPWRM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD1_DIODE_LOSS;
const Haluint8 UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_rev_curr[MAX_STEP] = SYSPWRM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD2_REV_CURR;
const Haluint8 UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_t_sd[MAX_STEP] = SYSPWRM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD1_T_SD;
const Haluint8 UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_t_sd[MAX_STEP] = SYSPWRM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD2_T_SD;
const Haluint8 UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_uv[MAX_STEP] = SYSPWRM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD1_UV;
const Haluint8 UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_uv[MAX_STEP] = SYSPWRM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD2_UV;
const Haluint8 UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_uv[MAX_STEP] = SYSPWRM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD3_UV;
const Haluint8 UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_uv[MAX_STEP] = SYSPWRM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD4_UV;
const Haluint8 UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_ov[MAX_STEP] = SYSPWRM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD1_OV;
const Haluint8 UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_ov[MAX_STEP] = SYSPWRM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD2_OV;
const Haluint8 UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ov[MAX_STEP] = SYSPWRM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD3_OV;
const Haluint8 UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_ov[MAX_STEP] = SYSPWRM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD4_OV;
const Haluint8 UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vint_ov[MAX_STEP] = SYSPWRM_MAINACHSYSPWRASICINFO_ACH_ASIC_VINT_OV;
const Haluint8 UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vint_uv[MAX_STEP] = SYSPWRM_MAINACHSYSPWRASICINFO_ACH_ASIC_VINT_UV;
const Haluint8 UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_dgndloss[MAX_STEP] = SYSPWRM_MAINACHSYSPWRASICINFO_ACH_ASIC_DGNDLOSS;
const Haluint8 UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vpwr_uv[MAX_STEP] = SYSPWRM_MAINACHSYSPWRASICINFO_ACH_ASIC_VPWR_UV;
const Haluint8 UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vpwr_ov[MAX_STEP] = SYSPWRM_MAINACHSYSPWRASICINFO_ACH_ASIC_VPWR_OV;
const Haluint8 UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_long[MAX_STEP] = SYSPWRM_MAINACHSYSPWRASICINFO_ACH_ASIC_COMM_TOO_LONG;
const Haluint8 UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_short[MAX_STEP] = SYSPWRM_MAINACHSYSPWRASICINFO_ACH_ASIC_COMM_TOO_SHORT;
const Haluint8 UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_add[MAX_STEP] = SYSPWRM_MAINACHSYSPWRASICINFO_ACH_ASIC_COMM_WRONG_ADD;
const Haluint8 UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_fcnt[MAX_STEP] = SYSPWRM_MAINACHSYSPWRASICINFO_ACH_ASIC_COMM_WRONG_FCNT;
const Haluint8 UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_crc[MAX_STEP] = SYSPWRM_MAINACHSYSPWRASICINFO_ACH_ASIC_COMM_WRONG_CRC;
const Haluint8 UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd5_ov[MAX_STEP] = SYSPWRM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD5_OV;
const Haluint8 UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd5_uv[MAX_STEP] = SYSPWRM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD5_UV;
const Haluint8 UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd5_t_sd[MAX_STEP] = SYSPWRM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD5_T_SD;
const Haluint8 UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd5_out_of_reg[MAX_STEP] = SYSPWRM_MAINACHSYSPWRASICINFO_ACH_ASIC_VDD5_OUT_OF_REG;
const Saluint16 UtInput_SysPwrM_MainWhlSpdInfo_FlWhlSpd[MAX_STEP] = SYSPWRM_MAINWHLSPDINFO_FLWHLSPD;
const Saluint16 UtInput_SysPwrM_MainWhlSpdInfo_FrWhlSpd[MAX_STEP] = SYSPWRM_MAINWHLSPDINFO_FRWHLSPD;
const Saluint16 UtInput_SysPwrM_MainWhlSpdInfo_RlWhlSpd[MAX_STEP] = SYSPWRM_MAINWHLSPDINFO_RLWHLSPD;
const Saluint16 UtInput_SysPwrM_MainWhlSpdInfo_RrlWhlSpd[MAX_STEP] = SYSPWRM_MAINWHLSPDINFO_RRLWHLSPD;
const Saluint8 UtInput_SysPwrM_MainRxClu2Info_IGN_SW[MAX_STEP] = SYSPWRM_MAINRXCLU2INFO_IGN_SW;
const Mom_HndlrEcuModeSts_t UtInput_SysPwrM_MainEcuModeSts[MAX_STEP] = SYSPWRM_MAINECUMODESTS;
const Prly_HndlrIgnOnOffSts_t UtInput_SysPwrM_MainIgnOnOffSts[MAX_STEP] = SYSPWRM_MAINIGNONOFFSTS;
const Prly_HndlrIgnEdgeSts_t UtInput_SysPwrM_MainIgnEdgeSts[MAX_STEP] = SYSPWRM_MAINIGNEDGESTS;
const Ioc_InputSR1msVBatt1Mon_t UtInput_SysPwrM_MainVBatt1Mon[MAX_STEP] = SYSPWRM_MAINVBATT1MON;
const Ioc_InputSR1msVBatt2Mon_t UtInput_SysPwrM_MainVBatt2Mon[MAX_STEP] = SYSPWRM_MAINVBATT2MON;
const Diag_HndlrDiagClr_t UtInput_SysPwrM_MainDiagClrSrs[MAX_STEP] = SYSPWRM_MAINDIAGCLRSRS;
const Arbitrator_VlvArbVlvDriveState_t UtInput_SysPwrM_MainArbVlvDriveState[MAX_STEP] = SYSPWRM_MAINARBVLVDRIVESTATE;
const Ioc_InputSR1msVdd1Mon_t UtInput_SysPwrM_MainVdd1Mon[MAX_STEP] = SYSPWRM_MAINVDD1MON;
const Ioc_InputSR1msVdd2Mon_t UtInput_SysPwrM_MainVdd2Mon[MAX_STEP] = SYSPWRM_MAINVDD2MON;
const Ioc_InputSR1msVdd3Mon_t UtInput_SysPwrM_MainVdd3Mon[MAX_STEP] = SYSPWRM_MAINVDD3MON;
const Ioc_InputSR1msVdd4Mon_t UtInput_SysPwrM_MainVdd4Mon[MAX_STEP] = SYSPWRM_MAINVDD4MON;
const Ioc_InputSR1msVdd5Mon_t UtInput_SysPwrM_MainVdd5Mon[MAX_STEP] = SYSPWRM_MAINVDD5MON;
const Ioc_InputSR1msVddMon_t UtInput_SysPwrM_MainVddMon[MAX_STEP] = SYSPWRM_MAINVDDMON;
const Arbitrator_MtrMtrArbDriveState_t UtInput_SysPwrM_MainMtrArbDriveState[MAX_STEP] = SYSPWRM_MAINMTRARBDRIVESTATE;

const Saluint8 UtExpected_SysPwrM_MainSysPwrMonData_SysPwrM_OverVolt_1st_Err[MAX_STEP] = SYSPWRM_MAINSYSPWRMONDATA_SYSPWRM_OVERVOLT_1ST_ERR;
const Saluint8 UtExpected_SysPwrM_MainSysPwrMonData_SysPwrM_OverVolt_2st_Err[MAX_STEP] = SYSPWRM_MAINSYSPWRMONDATA_SYSPWRM_OVERVOLT_2ST_ERR;
const Saluint8 UtExpected_SysPwrM_MainSysPwrMonData_SysPwrM_UnderVolt_1st_Err[MAX_STEP] = SYSPWRM_MAINSYSPWRMONDATA_SYSPWRM_UNDERVOLT_1ST_ERR;
const Saluint8 UtExpected_SysPwrM_MainSysPwrMonData_SysPwrM_UnderVolt_2st_Err[MAX_STEP] = SYSPWRM_MAINSYSPWRMONDATA_SYSPWRM_UNDERVOLT_2ST_ERR;
const Saluint8 UtExpected_SysPwrM_MainSysPwrMonData_SysPwrM_UnderVolt_3st_Err[MAX_STEP] = SYSPWRM_MAINSYSPWRMONDATA_SYSPWRM_UNDERVOLT_3ST_ERR;
const Saluint8 UtExpected_SysPwrM_MainSysPwrMonData_SysPwrM_Asic_Vdd_Err[MAX_STEP] = SYSPWRM_MAINSYSPWRMONDATA_SYSPWRM_ASIC_VDD_ERR;
const Saluint8 UtExpected_SysPwrM_MainSysPwrMonData_SysPwrM_Asic_OverVolt_Err[MAX_STEP] = SYSPWRM_MAINSYSPWRMONDATA_SYSPWRM_ASIC_OVERVOLT_ERR;
const Saluint8 UtExpected_SysPwrM_MainSysPwrMonData_SysPwrM_Asic_UnderVolt_Err[MAX_STEP] = SYSPWRM_MAINSYSPWRMONDATA_SYSPWRM_ASIC_UNDERVOLT_ERR;
const Saluint8 UtExpected_SysPwrM_MainSysPwrMonData_SysPwrM_Asic_OverTemp_Err[MAX_STEP] = SYSPWRM_MAINSYSPWRMONDATA_SYSPWRM_ASIC_OVERTEMP_ERR;
const Saluint8 UtExpected_SysPwrM_MainSysPwrMonData_SysPwrM_Asic_Gndloss_Err[MAX_STEP] = SYSPWRM_MAINSYSPWRMONDATA_SYSPWRM_ASIC_GNDLOSS_ERR;
const Saluint8 UtExpected_SysPwrM_MainSysPwrMonData_SysPwrM_Asic_Comm_Err[MAX_STEP] = SYSPWRM_MAINSYSPWRMONDATA_SYSPWRM_ASIC_COMM_ERR;
const Saluint8 UtExpected_SysPwrM_MainSysPwrMonData_SysPwrM_IgnLine_Open_Err[MAX_STEP] = SYSPWRM_MAINSYSPWRMONDATA_SYSPWRM_IGNLINE_OPEN_ERR;



TEST_GROUP(SysPwrM_Main);
TEST_SETUP(SysPwrM_Main)
{
    SysPwrM_Main_Init();
}

TEST_TEAR_DOWN(SysPwrM_Main)
{   /* Postcondition */

}

TEST(SysPwrM_Main, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc = UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_ovc[i];
        SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg = UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_out_of_reg[i];
        SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc = UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ovc[i];
        SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc = UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_ovc[i];
        SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss = UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_diode_loss[i];
        SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr = UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_rev_curr[i];
        SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd = UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_t_sd[i];
        SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd = UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_t_sd[i];
        SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_uv = UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_uv[i];
        SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_uv = UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_uv[i];
        SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv = UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_uv[i];
        SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_uv = UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_uv[i];
        SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ov = UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_ov[i];
        SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_ov = UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_ov[i];
        SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov = UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ov[i];
        SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ov = UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_ov[i];
        SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vint_ov = UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vint_ov[i];
        SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vint_uv = UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vint_uv[i];
        SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_dgndloss = UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_dgndloss[i];
        SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv = UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vpwr_uv[i];
        SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov = UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vpwr_ov[i];
        SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long = UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_long[i];
        SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short = UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_short[i];
        SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add = UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_add[i];
        SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt = UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_fcnt[i];
        SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc = UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_crc[i];
        SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_ov = UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd5_ov[i];
        SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_uv = UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd5_uv[i];
        SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_t_sd = UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd5_t_sd[i];
        SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_out_of_reg = UtInput_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd5_out_of_reg[i];
        SysPwrM_MainWhlSpdInfo.FlWhlSpd = UtInput_SysPwrM_MainWhlSpdInfo_FlWhlSpd[i];
        SysPwrM_MainWhlSpdInfo.FrWhlSpd = UtInput_SysPwrM_MainWhlSpdInfo_FrWhlSpd[i];
        SysPwrM_MainWhlSpdInfo.RlWhlSpd = UtInput_SysPwrM_MainWhlSpdInfo_RlWhlSpd[i];
        SysPwrM_MainWhlSpdInfo.RrlWhlSpd = UtInput_SysPwrM_MainWhlSpdInfo_RrlWhlSpd[i];
        SysPwrM_MainRxClu2Info.IGN_SW = UtInput_SysPwrM_MainRxClu2Info_IGN_SW[i];
        SysPwrM_MainEcuModeSts = UtInput_SysPwrM_MainEcuModeSts[i];
        SysPwrM_MainIgnOnOffSts = UtInput_SysPwrM_MainIgnOnOffSts[i];
        SysPwrM_MainIgnEdgeSts = UtInput_SysPwrM_MainIgnEdgeSts[i];
        SysPwrM_MainVBatt1Mon = UtInput_SysPwrM_MainVBatt1Mon[i];
        SysPwrM_MainVBatt2Mon = UtInput_SysPwrM_MainVBatt2Mon[i];
        SysPwrM_MainDiagClrSrs = UtInput_SysPwrM_MainDiagClrSrs[i];
        SysPwrM_MainArbVlvDriveState = UtInput_SysPwrM_MainArbVlvDriveState[i];
        SysPwrM_MainVdd1Mon = UtInput_SysPwrM_MainVdd1Mon[i];
        SysPwrM_MainVdd2Mon = UtInput_SysPwrM_MainVdd2Mon[i];
        SysPwrM_MainVdd3Mon = UtInput_SysPwrM_MainVdd3Mon[i];
        SysPwrM_MainVdd4Mon = UtInput_SysPwrM_MainVdd4Mon[i];
        SysPwrM_MainVdd5Mon = UtInput_SysPwrM_MainVdd5Mon[i];
        SysPwrM_MainVddMon = UtInput_SysPwrM_MainVddMon[i];
        SysPwrM_MainMtrArbDriveState = UtInput_SysPwrM_MainMtrArbDriveState[i];

        SysPwrM_Main();

        TEST_ASSERT_EQUAL(SysPwrM_MainSysPwrMonData.SysPwrM_OverVolt_1st_Err, UtExpected_SysPwrM_MainSysPwrMonData_SysPwrM_OverVolt_1st_Err[i]);
        TEST_ASSERT_EQUAL(SysPwrM_MainSysPwrMonData.SysPwrM_OverVolt_2st_Err, UtExpected_SysPwrM_MainSysPwrMonData_SysPwrM_OverVolt_2st_Err[i]);
        TEST_ASSERT_EQUAL(SysPwrM_MainSysPwrMonData.SysPwrM_UnderVolt_1st_Err, UtExpected_SysPwrM_MainSysPwrMonData_SysPwrM_UnderVolt_1st_Err[i]);
        TEST_ASSERT_EQUAL(SysPwrM_MainSysPwrMonData.SysPwrM_UnderVolt_2st_Err, UtExpected_SysPwrM_MainSysPwrMonData_SysPwrM_UnderVolt_2st_Err[i]);
        TEST_ASSERT_EQUAL(SysPwrM_MainSysPwrMonData.SysPwrM_UnderVolt_3st_Err, UtExpected_SysPwrM_MainSysPwrMonData_SysPwrM_UnderVolt_3st_Err[i]);
        TEST_ASSERT_EQUAL(SysPwrM_MainSysPwrMonData.SysPwrM_Asic_Vdd_Err, UtExpected_SysPwrM_MainSysPwrMonData_SysPwrM_Asic_Vdd_Err[i]);
        TEST_ASSERT_EQUAL(SysPwrM_MainSysPwrMonData.SysPwrM_Asic_OverVolt_Err, UtExpected_SysPwrM_MainSysPwrMonData_SysPwrM_Asic_OverVolt_Err[i]);
        TEST_ASSERT_EQUAL(SysPwrM_MainSysPwrMonData.SysPwrM_Asic_UnderVolt_Err, UtExpected_SysPwrM_MainSysPwrMonData_SysPwrM_Asic_UnderVolt_Err[i]);
        TEST_ASSERT_EQUAL(SysPwrM_MainSysPwrMonData.SysPwrM_Asic_OverTemp_Err, UtExpected_SysPwrM_MainSysPwrMonData_SysPwrM_Asic_OverTemp_Err[i]);
        TEST_ASSERT_EQUAL(SysPwrM_MainSysPwrMonData.SysPwrM_Asic_Gndloss_Err, UtExpected_SysPwrM_MainSysPwrMonData_SysPwrM_Asic_Gndloss_Err[i]);
        TEST_ASSERT_EQUAL(SysPwrM_MainSysPwrMonData.SysPwrM_Asic_Comm_Err, UtExpected_SysPwrM_MainSysPwrMonData_SysPwrM_Asic_Comm_Err[i]);
        TEST_ASSERT_EQUAL(SysPwrM_MainSysPwrMonData.SysPwrM_IgnLine_Open_Err, UtExpected_SysPwrM_MainSysPwrMonData_SysPwrM_IgnLine_Open_Err[i]);
    }
}

TEST_GROUP_RUNNER(SysPwrM_Main)
{
    RUN_TEST_CASE(SysPwrM_Main, All);
}
