/**
 * @defgroup SysPwrM_Main_Ifa SysPwrM_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SysPwrM_Main_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SYSPWRM_MAIN_IFA_H_
#define SYSPWRM_MAIN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo(data) do \
{ \
    *data = SysPwrM_MainAchSysPwrAsicInfo; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainWhlSpdInfo(data) do \
{ \
    *data = SysPwrM_MainWhlSpdInfo; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainRxClu2Info(data) do \
{ \
    *data = SysPwrM_MainRxClu2Info; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_ovc(data) do \
{ \
    *data = SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_out_of_reg(data) do \
{ \
    *data = SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ovc(data) do \
{ \
    *data = SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_ovc(data) do \
{ \
    *data = SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_diode_loss(data) do \
{ \
    *data = SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_rev_curr(data) do \
{ \
    *data = SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_t_sd(data) do \
{ \
    *data = SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_t_sd(data) do \
{ \
    *data = SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_uv(data) do \
{ \
    *data = SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_uv; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_uv(data) do \
{ \
    *data = SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_uv; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_uv(data) do \
{ \
    *data = SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_uv(data) do \
{ \
    *data = SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_uv; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_ov(data) do \
{ \
    *data = SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ov; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_ov(data) do \
{ \
    *data = SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_ov; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ov(data) do \
{ \
    *data = SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_ov(data) do \
{ \
    *data = SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ov; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vint_ov(data) do \
{ \
    *data = SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vint_ov; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vint_uv(data) do \
{ \
    *data = SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vint_uv; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_dgndloss(data) do \
{ \
    *data = SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_dgndloss; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vpwr_uv(data) do \
{ \
    *data = SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vpwr_ov(data) do \
{ \
    *data = SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_long(data) do \
{ \
    *data = SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_short(data) do \
{ \
    *data = SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_add(data) do \
{ \
    *data = SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_fcnt(data) do \
{ \
    *data = SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_crc(data) do \
{ \
    *data = SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd5_ov(data) do \
{ \
    *data = SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_ov; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd5_uv(data) do \
{ \
    *data = SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_uv; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd5_t_sd(data) do \
{ \
    *data = SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_t_sd; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd5_out_of_reg(data) do \
{ \
    *data = SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_out_of_reg; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainWhlSpdInfo_FlWhlSpd(data) do \
{ \
    *data = SysPwrM_MainWhlSpdInfo.FlWhlSpd; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainWhlSpdInfo_FrWhlSpd(data) do \
{ \
    *data = SysPwrM_MainWhlSpdInfo.FrWhlSpd; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainWhlSpdInfo_RlWhlSpd(data) do \
{ \
    *data = SysPwrM_MainWhlSpdInfo.RlWhlSpd; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainWhlSpdInfo_RrlWhlSpd(data) do \
{ \
    *data = SysPwrM_MainWhlSpdInfo.RrlWhlSpd; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainRxClu2Info_IGN_SW(data) do \
{ \
    *data = SysPwrM_MainRxClu2Info.IGN_SW; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainEcuModeSts(data) do \
{ \
    *data = SysPwrM_MainEcuModeSts; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainIgnOnOffSts(data) do \
{ \
    *data = SysPwrM_MainIgnOnOffSts; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainIgnEdgeSts(data) do \
{ \
    *data = SysPwrM_MainIgnEdgeSts; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainVBatt1Mon(data) do \
{ \
    *data = SysPwrM_MainVBatt1Mon; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainVBatt2Mon(data) do \
{ \
    *data = SysPwrM_MainVBatt2Mon; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainDiagClrSrs(data) do \
{ \
    *data = SysPwrM_MainDiagClrSrs; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainArbVlvDriveState(data) do \
{ \
    *data = SysPwrM_MainArbVlvDriveState; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainVdd1Mon(data) do \
{ \
    *data = SysPwrM_MainVdd1Mon; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainVdd2Mon(data) do \
{ \
    *data = SysPwrM_MainVdd2Mon; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainVdd3Mon(data) do \
{ \
    *data = SysPwrM_MainVdd3Mon; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainVdd4Mon(data) do \
{ \
    *data = SysPwrM_MainVdd4Mon; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainVdd5Mon(data) do \
{ \
    *data = SysPwrM_MainVdd5Mon; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainVddMon(data) do \
{ \
    *data = SysPwrM_MainVddMon; \
}while(0);

#define SysPwrM_Main_Read_SysPwrM_MainMtrArbDriveState(data) do \
{ \
    *data = SysPwrM_MainMtrArbDriveState; \
}while(0);


/* Set Output DE MAcro Function */
#define SysPwrM_Main_Write_SysPwrM_MainSysPwrMonData(data) do \
{ \
    SysPwrM_MainSysPwrMonData = *data; \
}while(0);

#define SysPwrM_Main_Write_SysPwrM_MainSysPwrMonData_SysPwrM_OverVolt_1st_Err(data) do \
{ \
    SysPwrM_MainSysPwrMonData.SysPwrM_OverVolt_1st_Err = *data; \
}while(0);

#define SysPwrM_Main_Write_SysPwrM_MainSysPwrMonData_SysPwrM_OverVolt_2st_Err(data) do \
{ \
    SysPwrM_MainSysPwrMonData.SysPwrM_OverVolt_2st_Err = *data; \
}while(0);

#define SysPwrM_Main_Write_SysPwrM_MainSysPwrMonData_SysPwrM_UnderVolt_1st_Err(data) do \
{ \
    SysPwrM_MainSysPwrMonData.SysPwrM_UnderVolt_1st_Err = *data; \
}while(0);

#define SysPwrM_Main_Write_SysPwrM_MainSysPwrMonData_SysPwrM_UnderVolt_2st_Err(data) do \
{ \
    SysPwrM_MainSysPwrMonData.SysPwrM_UnderVolt_2st_Err = *data; \
}while(0);

#define SysPwrM_Main_Write_SysPwrM_MainSysPwrMonData_SysPwrM_UnderVolt_3st_Err(data) do \
{ \
    SysPwrM_MainSysPwrMonData.SysPwrM_UnderVolt_3st_Err = *data; \
}while(0);

#define SysPwrM_Main_Write_SysPwrM_MainSysPwrMonData_SysPwrM_Asic_Vdd_Err(data) do \
{ \
    SysPwrM_MainSysPwrMonData.SysPwrM_Asic_Vdd_Err = *data; \
}while(0);

#define SysPwrM_Main_Write_SysPwrM_MainSysPwrMonData_SysPwrM_Asic_OverVolt_Err(data) do \
{ \
    SysPwrM_MainSysPwrMonData.SysPwrM_Asic_OverVolt_Err = *data; \
}while(0);

#define SysPwrM_Main_Write_SysPwrM_MainSysPwrMonData_SysPwrM_Asic_UnderVolt_Err(data) do \
{ \
    SysPwrM_MainSysPwrMonData.SysPwrM_Asic_UnderVolt_Err = *data; \
}while(0);

#define SysPwrM_Main_Write_SysPwrM_MainSysPwrMonData_SysPwrM_Asic_OverTemp_Err(data) do \
{ \
    SysPwrM_MainSysPwrMonData.SysPwrM_Asic_OverTemp_Err = *data; \
}while(0);

#define SysPwrM_Main_Write_SysPwrM_MainSysPwrMonData_SysPwrM_Asic_Gndloss_Err(data) do \
{ \
    SysPwrM_MainSysPwrMonData.SysPwrM_Asic_Gndloss_Err = *data; \
}while(0);

#define SysPwrM_Main_Write_SysPwrM_MainSysPwrMonData_SysPwrM_Asic_Comm_Err(data) do \
{ \
    SysPwrM_MainSysPwrMonData.SysPwrM_Asic_Comm_Err = *data; \
}while(0);

#define SysPwrM_Main_Write_SysPwrM_MainSysPwrMonData_SysPwrM_IgnLine_Open_Err(data) do \
{ \
    SysPwrM_MainSysPwrMonData.SysPwrM_IgnLine_Open_Err = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SYSPWRM_MAIN_IFA_H_ */
/** @} */
