/**
 * @defgroup SysPwrM_Main_Ifa SysPwrM_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SysPwrM_Main_Ifa.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "SysPwrM_Main_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SYSPWRM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "SysPwrM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define SYSPWRM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "SysPwrM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SYSPWRM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SysPwrM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SYSPWRM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SysPwrM_MemMap.h"
#define SYSPWRM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "SysPwrM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SYSPWRM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "SysPwrM_MemMap.h"
#define SYSPWRM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "SysPwrM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SYSPWRM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "SysPwrM_MemMap.h"
#define SYSPWRM_MAIN_START_SEC_VAR_32BIT
#include "SysPwrM_MemMap.h"
/** Variable Section (32BIT)**/


#define SYSPWRM_MAIN_STOP_SEC_VAR_32BIT
#include "SysPwrM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SYSPWRM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SysPwrM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SYSPWRM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SysPwrM_MemMap.h"
#define SYSPWRM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "SysPwrM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SYSPWRM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "SysPwrM_MemMap.h"
#define SYSPWRM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "SysPwrM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SYSPWRM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "SysPwrM_MemMap.h"
#define SYSPWRM_MAIN_START_SEC_VAR_32BIT
#include "SysPwrM_MemMap.h"
/** Variable Section (32BIT)**/


#define SYSPWRM_MAIN_STOP_SEC_VAR_32BIT
#include "SysPwrM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SYSPWRM_MAIN_START_SEC_CODE
#include "SysPwrM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define SYSPWRM_MAIN_STOP_SEC_CODE
#include "SysPwrM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
