# \file
#
# \brief SysPwrM
#
# This file contains the implementation of the SWC
# module SysPwrM.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

SysPwrM_CORE_PATH     := $(MANDO_BSW_ROOT)\SysPwrM
SysPwrM_CAL_PATH      := $(SysPwrM_CORE_PATH)\CAL\$(SysPwrM_VARIANT)
SysPwrM_SRC_PATH      := $(SysPwrM_CORE_PATH)\SRC
SysPwrM_CFG_PATH      := $(SysPwrM_CORE_PATH)\CFG\$(SysPwrM_VARIANT)
SysPwrM_HDR_PATH      := $(SysPwrM_CORE_PATH)\HDR
SysPwrM_IFA_PATH      := $(SysPwrM_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    SysPwrM_CMN_PATH      := $(SysPwrM_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	SysPwrM_UT_PATH		:= $(SysPwrM_CORE_PATH)\UT
	SysPwrM_UNITY_PATH	:= $(SysPwrM_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(SysPwrM_UT_PATH)
	CC_INCLUDE_PATH		+= $(SysPwrM_UNITY_PATH)
	SysPwrM_Main_PATH 	:= SysPwrM_UT_PATH\SysPwrM_Main
endif
CC_INCLUDE_PATH    += $(SysPwrM_CAL_PATH)
CC_INCLUDE_PATH    += $(SysPwrM_SRC_PATH)
CC_INCLUDE_PATH    += $(SysPwrM_CFG_PATH)
CC_INCLUDE_PATH    += $(SysPwrM_HDR_PATH)
CC_INCLUDE_PATH    += $(SysPwrM_IFA_PATH)
CC_INCLUDE_PATH    += $(SysPwrM_CMN_PATH)

