# \file
#
# \brief SysPwrM
#
# This file contains the implementation of the SWC
# module SysPwrM.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += SysPwrM_src

SysPwrM_src_FILES        += $(SysPwrM_SRC_PATH)\SysPwrM_Main.c
SysPwrM_src_FILES        += $(SysPwrM_IFA_PATH)\SysPwrM_Main_Ifa.c
SysPwrM_src_FILES        += $(SysPwrM_CFG_PATH)\SysPwrM_Cfg.c
SysPwrM_src_FILES        += $(SysPwrM_CAL_PATH)\SysPwrM_Cal.c
SysPwrM_src_FILES        += $(SysPwrM_SRC_PATH)\SysPwrM_Process.c

ifeq ($(ICE_COMPILE),true)
SysPwrM_src_FILES        += $(SysPwrM_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	SysPwrM_src_FILES        += $(SysPwrM_UNITY_PATH)\unity.c
	SysPwrM_src_FILES        += $(SysPwrM_UNITY_PATH)\unity_fixture.c	
	SysPwrM_src_FILES        += $(SysPwrM_UT_PATH)\main.c
	SysPwrM_src_FILES        += $(SysPwrM_UT_PATH)\SysPwrM_Main\SysPwrM_Main_UtMain.c
endif