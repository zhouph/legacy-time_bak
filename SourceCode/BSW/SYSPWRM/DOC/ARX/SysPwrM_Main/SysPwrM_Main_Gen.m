SysPwrM_MainAchSysPwrAsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_vdd1_ovc'
    'Ach_Asic_vdd2_out_of_reg'
    'Ach_Asic_vdd3_ovc'
    'Ach_Asic_vdd4_ovc'
    'Ach_Asic_vdd1_diode_loss'
    'Ach_Asic_vdd2_rev_curr'
    'Ach_Asic_vdd1_t_sd'
    'Ach_Asic_vdd2_t_sd'
    'Ach_Asic_vdd1_uv'
    'Ach_Asic_vdd2_uv'
    'Ach_Asic_vdd3_uv'
    'Ach_Asic_vdd4_uv'
    'Ach_Asic_vdd1_ov'
    'Ach_Asic_vdd2_ov'
    'Ach_Asic_vdd3_ov'
    'Ach_Asic_vdd4_ov'
    'Ach_Asic_vint_ov'
    'Ach_Asic_vint_uv'
    'Ach_Asic_dgndloss'
    'Ach_Asic_vpwr_uv'
    'Ach_Asic_vpwr_ov'
    'Ach_Asic_comm_too_long'
    'Ach_Asic_comm_too_short'
    'Ach_Asic_comm_wrong_add'
    'Ach_Asic_comm_wrong_fcnt'
    'Ach_Asic_comm_wrong_crc'
    'Ach_Asic_vdd5_ov'
    'Ach_Asic_vdd5_uv'
    'Ach_Asic_vdd5_t_sd'
    'Ach_Asic_vdd5_out_of_reg'
    };
SysPwrM_MainAchSysPwrAsicInfo = CreateBus(SysPwrM_MainAchSysPwrAsicInfo, DeList);
clear DeList;

SysPwrM_MainWhlSpdInfo = Simulink.Bus;
DeList={
    'FlWhlSpd'
    'FrWhlSpd'
    'RlWhlSpd'
    'RrlWhlSpd'
    };
SysPwrM_MainWhlSpdInfo = CreateBus(SysPwrM_MainWhlSpdInfo, DeList);
clear DeList;

SysPwrM_MainRxClu2Info = Simulink.Bus;
DeList={
    'IGN_SW'
    };
SysPwrM_MainRxClu2Info = CreateBus(SysPwrM_MainRxClu2Info, DeList);
clear DeList;

SysPwrM_MainEcuModeSts = Simulink.Bus;
DeList={'SysPwrM_MainEcuModeSts'};
SysPwrM_MainEcuModeSts = CreateBus(SysPwrM_MainEcuModeSts, DeList);
clear DeList;

SysPwrM_MainIgnOnOffSts = Simulink.Bus;
DeList={'SysPwrM_MainIgnOnOffSts'};
SysPwrM_MainIgnOnOffSts = CreateBus(SysPwrM_MainIgnOnOffSts, DeList);
clear DeList;

SysPwrM_MainIgnEdgeSts = Simulink.Bus;
DeList={'SysPwrM_MainIgnEdgeSts'};
SysPwrM_MainIgnEdgeSts = CreateBus(SysPwrM_MainIgnEdgeSts, DeList);
clear DeList;

SysPwrM_MainVBatt1Mon = Simulink.Bus;
DeList={'SysPwrM_MainVBatt1Mon'};
SysPwrM_MainVBatt1Mon = CreateBus(SysPwrM_MainVBatt1Mon, DeList);
clear DeList;

SysPwrM_MainVBatt2Mon = Simulink.Bus;
DeList={'SysPwrM_MainVBatt2Mon'};
SysPwrM_MainVBatt2Mon = CreateBus(SysPwrM_MainVBatt2Mon, DeList);
clear DeList;

SysPwrM_MainDiagClrSrs = Simulink.Bus;
DeList={'SysPwrM_MainDiagClrSrs'};
SysPwrM_MainDiagClrSrs = CreateBus(SysPwrM_MainDiagClrSrs, DeList);
clear DeList;

SysPwrM_MainArbVlvDriveState = Simulink.Bus;
DeList={'SysPwrM_MainArbVlvDriveState'};
SysPwrM_MainArbVlvDriveState = CreateBus(SysPwrM_MainArbVlvDriveState, DeList);
clear DeList;

SysPwrM_MainVdd1Mon = Simulink.Bus;
DeList={'SysPwrM_MainVdd1Mon'};
SysPwrM_MainVdd1Mon = CreateBus(SysPwrM_MainVdd1Mon, DeList);
clear DeList;

SysPwrM_MainVdd2Mon = Simulink.Bus;
DeList={'SysPwrM_MainVdd2Mon'};
SysPwrM_MainVdd2Mon = CreateBus(SysPwrM_MainVdd2Mon, DeList);
clear DeList;

SysPwrM_MainVdd3Mon = Simulink.Bus;
DeList={'SysPwrM_MainVdd3Mon'};
SysPwrM_MainVdd3Mon = CreateBus(SysPwrM_MainVdd3Mon, DeList);
clear DeList;

SysPwrM_MainVdd4Mon = Simulink.Bus;
DeList={'SysPwrM_MainVdd4Mon'};
SysPwrM_MainVdd4Mon = CreateBus(SysPwrM_MainVdd4Mon, DeList);
clear DeList;

SysPwrM_MainVdd5Mon = Simulink.Bus;
DeList={'SysPwrM_MainVdd5Mon'};
SysPwrM_MainVdd5Mon = CreateBus(SysPwrM_MainVdd5Mon, DeList);
clear DeList;

SysPwrM_MainVddMon = Simulink.Bus;
DeList={'SysPwrM_MainVddMon'};
SysPwrM_MainVddMon = CreateBus(SysPwrM_MainVddMon, DeList);
clear DeList;

SysPwrM_MainMtrArbDriveState = Simulink.Bus;
DeList={'SysPwrM_MainMtrArbDriveState'};
SysPwrM_MainMtrArbDriveState = CreateBus(SysPwrM_MainMtrArbDriveState, DeList);
clear DeList;

SysPwrM_MainSysPwrMonData = Simulink.Bus;
DeList={
    'SysPwrM_OverVolt_1st_Err'
    'SysPwrM_OverVolt_2st_Err'
    'SysPwrM_UnderVolt_1st_Err'
    'SysPwrM_UnderVolt_2st_Err'
    'SysPwrM_UnderVolt_3st_Err'
    'SysPwrM_Asic_Vdd_Err'
    'SysPwrM_Asic_OverVolt_Err'
    'SysPwrM_Asic_UnderVolt_Err'
    'SysPwrM_Asic_OverTemp_Err'
    'SysPwrM_Asic_Gndloss_Err'
    'SysPwrM_Asic_Comm_Err'
    'SysPwrM_IgnLine_Open_Err'
    };
SysPwrM_MainSysPwrMonData = CreateBus(SysPwrM_MainSysPwrMonData, DeList);
clear DeList;

