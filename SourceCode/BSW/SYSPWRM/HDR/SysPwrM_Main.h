/**
 * @defgroup SysPwrM_Main SysPwrM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SysPwrM_Main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SYSPWRM_MAIN_H_
#define SYSPWRM_MAIN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "SysPwrM_Types.h"
#include "SysPwrM_Cfg.h"
#include "SysPwrM_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define SYSPWRM_MAIN_MODULE_ID      (0)
 #define SYSPWRM_MAIN_MAJOR_VERSION  (2)
 #define SYSPWRM_MAIN_MINOR_VERSION  (0)
 #define SYSPWRM_MAIN_PATCH_VERSION  (0)
 #define SYSPWRM_MAIN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern SysPwrM_Main_HdrBusType SysPwrM_MainBus;

/* Version Info */
extern const SwcVersionInfo_t SysPwrM_MainVersionInfo;

/* Input Data Element */
extern Ach_InputAchSysPwrAsicInfo_t SysPwrM_MainAchSysPwrAsicInfo;
extern Wss_SenWhlSpdInfo_t SysPwrM_MainWhlSpdInfo;
extern Proxy_RxByComRxClu2Info_t SysPwrM_MainRxClu2Info;
extern Mom_HndlrEcuModeSts_t SysPwrM_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t SysPwrM_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t SysPwrM_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t SysPwrM_MainVBatt1Mon;
extern Ioc_InputSR1msVBatt2Mon_t SysPwrM_MainVBatt2Mon;
extern Diag_HndlrDiagClr_t SysPwrM_MainDiagClrSrs;
extern Arbitrator_VlvArbVlvDriveState_t SysPwrM_MainArbVlvDriveState;
extern Ioc_InputSR1msVdd1Mon_t SysPwrM_MainVdd1Mon;
extern Ioc_InputSR1msVdd2Mon_t SysPwrM_MainVdd2Mon;
extern Ioc_InputSR1msVdd3Mon_t SysPwrM_MainVdd3Mon;
extern Ioc_InputSR1msVdd4Mon_t SysPwrM_MainVdd4Mon;
extern Ioc_InputSR1msVdd5Mon_t SysPwrM_MainVdd5Mon;
extern Ioc_InputSR1msVddMon_t SysPwrM_MainVddMon;
extern Arbitrator_MtrMtrArbDriveState_t SysPwrM_MainMtrArbDriveState;

/* Output Data Element */
extern SysPwrM_MainSysPwrMonData_t SysPwrM_MainSysPwrMonData;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void SysPwrM_Main_Init(void);
extern void SysPwrM_Main(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SYSPWRM_MAIN_H_ */
/** @} */
