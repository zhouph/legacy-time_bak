/**
 * @defgroup SysPwrM_Types SysPwrM_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SysPwrM_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef SYSPWRM_TYPES_H_
#define SYSPWRM_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Ach_InputAchSysPwrAsicInfo_t SysPwrM_MainAchSysPwrAsicInfo;
    Wss_SenWhlSpdInfo_t SysPwrM_MainWhlSpdInfo;
    Proxy_RxByComRxClu2Info_t SysPwrM_MainRxClu2Info;
    Mom_HndlrEcuModeSts_t SysPwrM_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t SysPwrM_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t SysPwrM_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t SysPwrM_MainVBatt1Mon;
    Ioc_InputSR1msVBatt2Mon_t SysPwrM_MainVBatt2Mon;
    Diag_HndlrDiagClr_t SysPwrM_MainDiagClrSrs;
    Arbitrator_VlvArbVlvDriveState_t SysPwrM_MainArbVlvDriveState;
    Ioc_InputSR1msVdd1Mon_t SysPwrM_MainVdd1Mon;
    Ioc_InputSR1msVdd2Mon_t SysPwrM_MainVdd2Mon;
    Ioc_InputSR1msVdd3Mon_t SysPwrM_MainVdd3Mon;
    Ioc_InputSR1msVdd4Mon_t SysPwrM_MainVdd4Mon;
    Ioc_InputSR1msVdd5Mon_t SysPwrM_MainVdd5Mon;
    Ioc_InputSR1msVddMon_t SysPwrM_MainVddMon;
    Arbitrator_MtrMtrArbDriveState_t SysPwrM_MainMtrArbDriveState;

/* Output Data Element */
    SysPwrM_MainSysPwrMonData_t SysPwrM_MainSysPwrMonData;
}SysPwrM_Main_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* SYSPWRM_TYPES_H_ */
/** @} */
