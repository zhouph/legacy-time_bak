#define S_FUNCTION_NAME      SysPwrM_Main_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          49
#define WidthOutputPort         12

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "SysPwrM_Main.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc = input[0];
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg = input[1];
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc = input[2];
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc = input[3];
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss = input[4];
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr = input[5];
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd = input[6];
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd = input[7];
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_uv = input[8];
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_uv = input[9];
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv = input[10];
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_uv = input[11];
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ov = input[12];
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_ov = input[13];
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov = input[14];
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ov = input[15];
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vint_ov = input[16];
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vint_uv = input[17];
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_dgndloss = input[18];
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv = input[19];
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov = input[20];
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long = input[21];
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short = input[22];
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add = input[23];
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt = input[24];
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc = input[25];
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_ov = input[26];
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_uv = input[27];
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_t_sd = input[28];
    SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_out_of_reg = input[29];
    SysPwrM_MainWhlSpdInfo.FlWhlSpd = input[30];
    SysPwrM_MainWhlSpdInfo.FrWhlSpd = input[31];
    SysPwrM_MainWhlSpdInfo.RlWhlSpd = input[32];
    SysPwrM_MainWhlSpdInfo.RrlWhlSpd = input[33];
    SysPwrM_MainRxClu2Info.IGN_SW = input[34];
    SysPwrM_MainEcuModeSts = input[35];
    SysPwrM_MainIgnOnOffSts = input[36];
    SysPwrM_MainIgnEdgeSts = input[37];
    SysPwrM_MainVBatt1Mon = input[38];
    SysPwrM_MainVBatt2Mon = input[39];
    SysPwrM_MainDiagClrSrs = input[40];
    SysPwrM_MainArbVlvDriveState = input[41];
    SysPwrM_MainVdd1Mon = input[42];
    SysPwrM_MainVdd2Mon = input[43];
    SysPwrM_MainVdd3Mon = input[44];
    SysPwrM_MainVdd4Mon = input[45];
    SysPwrM_MainVdd5Mon = input[46];
    SysPwrM_MainVddMon = input[47];
    SysPwrM_MainMtrArbDriveState = input[48];

    SysPwrM_Main();


    output[0] = SysPwrM_MainSysPwrMonData.SysPwrM_OverVolt_1st_Err;
    output[1] = SysPwrM_MainSysPwrMonData.SysPwrM_OverVolt_2st_Err;
    output[2] = SysPwrM_MainSysPwrMonData.SysPwrM_UnderVolt_1st_Err;
    output[3] = SysPwrM_MainSysPwrMonData.SysPwrM_UnderVolt_2st_Err;
    output[4] = SysPwrM_MainSysPwrMonData.SysPwrM_UnderVolt_3st_Err;
    output[5] = SysPwrM_MainSysPwrMonData.SysPwrM_Asic_Vdd_Err;
    output[6] = SysPwrM_MainSysPwrMonData.SysPwrM_Asic_OverVolt_Err;
    output[7] = SysPwrM_MainSysPwrMonData.SysPwrM_Asic_UnderVolt_Err;
    output[8] = SysPwrM_MainSysPwrMonData.SysPwrM_Asic_OverTemp_Err;
    output[9] = SysPwrM_MainSysPwrMonData.SysPwrM_Asic_Gndloss_Err;
    output[10] = SysPwrM_MainSysPwrMonData.SysPwrM_Asic_Comm_Err;
    output[11] = SysPwrM_MainSysPwrMonData.SysPwrM_IgnLine_Open_Err;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    SysPwrM_Main_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
