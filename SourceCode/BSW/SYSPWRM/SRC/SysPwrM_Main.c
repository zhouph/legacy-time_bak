/**
 * @defgroup SysPwrM_Main SysPwrM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SysPwrM_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "SysPwrM_Main.h"
#include "SysPwrM_Main_Ifa.h"
#include "IfxStm_reg.h"
#include "SysPwrM_Process.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define SYSPWRM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "SysPwrM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define SYSPWRM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "SysPwrM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SYSPWRM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SysPwrM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
SysPwrM_Main_HdrBusType SysPwrM_MainBus;

/* Version Info */
const SwcVersionInfo_t SysPwrM_MainVersionInfo = 
{   
    SYSPWRM_MAIN_MODULE_ID,           /* SysPwrM_MainVersionInfo.ModuleId */
    SYSPWRM_MAIN_MAJOR_VERSION,       /* SysPwrM_MainVersionInfo.MajorVer */
    SYSPWRM_MAIN_MINOR_VERSION,       /* SysPwrM_MainVersionInfo.MinorVer */
    SYSPWRM_MAIN_PATCH_VERSION,       /* SysPwrM_MainVersionInfo.PatchVer */
    SYSPWRM_MAIN_BRANCH_VERSION       /* SysPwrM_MainVersionInfo.BranchVer */
};
    
/* Input Data Element */
Ach_InputAchSysPwrAsicInfo_t SysPwrM_MainAchSysPwrAsicInfo;
Wss_SenWhlSpdInfo_t SysPwrM_MainWhlSpdInfo;
Proxy_RxByComRxClu2Info_t SysPwrM_MainRxClu2Info;
Mom_HndlrEcuModeSts_t SysPwrM_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t SysPwrM_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t SysPwrM_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t SysPwrM_MainVBatt1Mon;
Ioc_InputSR1msVBatt2Mon_t SysPwrM_MainVBatt2Mon;
Diag_HndlrDiagClr_t SysPwrM_MainDiagClrSrs;
Arbitrator_VlvArbVlvDriveState_t SysPwrM_MainArbVlvDriveState;
Ioc_InputSR1msVdd1Mon_t SysPwrM_MainVdd1Mon;
Ioc_InputSR1msVdd2Mon_t SysPwrM_MainVdd2Mon;
Ioc_InputSR1msVdd3Mon_t SysPwrM_MainVdd3Mon;
Ioc_InputSR1msVdd4Mon_t SysPwrM_MainVdd4Mon;
Ioc_InputSR1msVdd5Mon_t SysPwrM_MainVdd5Mon;
Ioc_InputSR1msVddMon_t SysPwrM_MainVddMon;
Arbitrator_MtrMtrArbDriveState_t SysPwrM_MainMtrArbDriveState;

/* Output Data Element */
SysPwrM_MainSysPwrMonData_t SysPwrM_MainSysPwrMonData;

uint32 SysPwrM_Main_Timer_Start;
uint32 SysPwrM_Main_Timer_Elapsed;

#define SYSPWRM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SysPwrM_MemMap.h"
#define SYSPWRM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "SysPwrM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SYSPWRM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "SysPwrM_MemMap.h"
#define SYSPWRM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "SysPwrM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SYSPWRM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "SysPwrM_MemMap.h"
#define SYSPWRM_MAIN_START_SEC_VAR_32BIT
#include "SysPwrM_MemMap.h"
/** Variable Section (32BIT)**/


#define SYSPWRM_MAIN_STOP_SEC_VAR_32BIT
#include "SysPwrM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define SYSPWRM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "SysPwrM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define SYSPWRM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "SysPwrM_MemMap.h"
#define SYSPWRM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "SysPwrM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define SYSPWRM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "SysPwrM_MemMap.h"
#define SYSPWRM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "SysPwrM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define SYSPWRM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "SysPwrM_MemMap.h"
#define SYSPWRM_MAIN_START_SEC_VAR_32BIT
#include "SysPwrM_MemMap.h"
/** Variable Section (32BIT)**/


#define SYSPWRM_MAIN_STOP_SEC_VAR_32BIT
#include "SysPwrM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define SYSPWRM_MAIN_START_SEC_CODE
#include "SysPwrM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void SysPwrM_Main_Init(void)
{
    /* Initialize internal bus */
    SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc = 0;
    SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg = 0;
    SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc = 0;
    SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc = 0;
    SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss = 0;
    SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr = 0;
    SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd = 0;
    SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd = 0;
    SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_uv = 0;
    SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_uv = 0;
    SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv = 0;
    SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_uv = 0;
    SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ov = 0;
    SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_ov = 0;
    SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov = 0;
    SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ov = 0;
    SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vint_ov = 0;
    SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vint_uv = 0;
    SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_dgndloss = 0;
    SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv = 0;
    SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov = 0;
    SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long = 0;
    SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short = 0;
    SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add = 0;
    SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt = 0;
    SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc = 0;
    SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_ov = 0;
    SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_uv = 0;
    SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_t_sd = 0;
    SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_out_of_reg = 0;
    SysPwrM_MainBus.SysPwrM_MainWhlSpdInfo.FlWhlSpd = 0;
    SysPwrM_MainBus.SysPwrM_MainWhlSpdInfo.FrWhlSpd = 0;
    SysPwrM_MainBus.SysPwrM_MainWhlSpdInfo.RlWhlSpd = 0;
    SysPwrM_MainBus.SysPwrM_MainWhlSpdInfo.RrlWhlSpd = 0;
    SysPwrM_MainBus.SysPwrM_MainRxClu2Info.IGN_SW = 0;
    SysPwrM_MainBus.SysPwrM_MainEcuModeSts = 0;
    SysPwrM_MainBus.SysPwrM_MainIgnOnOffSts = 0;
    SysPwrM_MainBus.SysPwrM_MainIgnEdgeSts = 0;
    SysPwrM_MainBus.SysPwrM_MainVBatt1Mon = 0;
    SysPwrM_MainBus.SysPwrM_MainVBatt2Mon = 0;
    SysPwrM_MainBus.SysPwrM_MainDiagClrSrs = 0;
    SysPwrM_MainBus.SysPwrM_MainArbVlvDriveState = 0;
    SysPwrM_MainBus.SysPwrM_MainVdd1Mon = 0;
    SysPwrM_MainBus.SysPwrM_MainVdd2Mon = 0;
    SysPwrM_MainBus.SysPwrM_MainVdd3Mon = 0;
    SysPwrM_MainBus.SysPwrM_MainVdd4Mon = 0;
    SysPwrM_MainBus.SysPwrM_MainVdd5Mon = 0;
    SysPwrM_MainBus.SysPwrM_MainVddMon = 0;
    SysPwrM_MainBus.SysPwrM_MainMtrArbDriveState = 0;
    SysPwrM_MainBus.SysPwrM_MainSysPwrMonData.SysPwrM_OverVolt_1st_Err = 0;
    SysPwrM_MainBus.SysPwrM_MainSysPwrMonData.SysPwrM_OverVolt_2st_Err = 0;
    SysPwrM_MainBus.SysPwrM_MainSysPwrMonData.SysPwrM_UnderVolt_1st_Err = 0;
    SysPwrM_MainBus.SysPwrM_MainSysPwrMonData.SysPwrM_UnderVolt_2st_Err = 0;
    SysPwrM_MainBus.SysPwrM_MainSysPwrMonData.SysPwrM_UnderVolt_3st_Err = 0;
    SysPwrM_MainBus.SysPwrM_MainSysPwrMonData.SysPwrM_Asic_Vdd_Err = 0;
    SysPwrM_MainBus.SysPwrM_MainSysPwrMonData.SysPwrM_Asic_OverVolt_Err = 0;
    SysPwrM_MainBus.SysPwrM_MainSysPwrMonData.SysPwrM_Asic_UnderVolt_Err = 0;
    SysPwrM_MainBus.SysPwrM_MainSysPwrMonData.SysPwrM_Asic_OverTemp_Err = 0;
    SysPwrM_MainBus.SysPwrM_MainSysPwrMonData.SysPwrM_Asic_Gndloss_Err = 0;
    SysPwrM_MainBus.SysPwrM_MainSysPwrMonData.SysPwrM_Asic_Comm_Err = 0;
    SysPwrM_MainBus.SysPwrM_MainSysPwrMonData.SysPwrM_IgnLine_Open_Err = 0;
	SysPwrM_Process_Init();
}

void SysPwrM_Main(void)
{
    SysPwrM_Main_Timer_Start = STM0_TIM0.U;

    /* Input */
    /* Decomposed structure interface */
    SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_ovc(&SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ovc);
    SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_out_of_reg(&SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_out_of_reg);
    SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ovc(&SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ovc);
    SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_ovc(&SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ovc);
    SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_diode_loss(&SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_diode_loss);
    SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_rev_curr(&SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_rev_curr);
    SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_t_sd(&SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_t_sd);
    SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_t_sd(&SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_t_sd);
    SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_uv(&SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_uv);
    SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_uv(&SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_uv);
    SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_uv(&SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_uv);
    SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_uv(&SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_uv);
    SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd1_ov(&SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd1_ov);
    SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd2_ov(&SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd2_ov);
    SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd3_ov(&SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd3_ov);
    SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd4_ov(&SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd4_ov);
    SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vint_ov(&SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vint_ov);
    SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vint_uv(&SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vint_uv);
    SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_dgndloss(&SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_dgndloss);
    SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vpwr_uv(&SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_uv);
    SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vpwr_ov(&SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vpwr_ov);
    SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_long(&SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long);
    SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_short(&SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short);
    SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_add(&SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add);
    SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_fcnt(&SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt);
    SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_crc(&SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc);
    SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd5_ov(&SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_ov);
    SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd5_uv(&SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_uv);
    SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd5_t_sd(&SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_t_sd);
    SysPwrM_Main_Read_SysPwrM_MainAchSysPwrAsicInfo_Ach_Asic_vdd5_out_of_reg(&SysPwrM_MainBus.SysPwrM_MainAchSysPwrAsicInfo.Ach_Asic_vdd5_out_of_reg);

    SysPwrM_Main_Read_SysPwrM_MainWhlSpdInfo(&SysPwrM_MainBus.SysPwrM_MainWhlSpdInfo);
    /*==============================================================================
    * Members of structure SysPwrM_MainWhlSpdInfo 
     : SysPwrM_MainWhlSpdInfo.FlWhlSpd;
     : SysPwrM_MainWhlSpdInfo.FrWhlSpd;
     : SysPwrM_MainWhlSpdInfo.RlWhlSpd;
     : SysPwrM_MainWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/
    
    SysPwrM_Main_Read_SysPwrM_MainRxClu2Info(&SysPwrM_MainBus.SysPwrM_MainRxClu2Info);
    /*==============================================================================
    * Members of structure SysPwrM_MainRxClu2Info 
     : SysPwrM_MainRxClu2Info.IGN_SW;
     =============================================================================*/
    
    SysPwrM_Main_Read_SysPwrM_MainEcuModeSts(&SysPwrM_MainBus.SysPwrM_MainEcuModeSts);
    SysPwrM_Main_Read_SysPwrM_MainIgnOnOffSts(&SysPwrM_MainBus.SysPwrM_MainIgnOnOffSts);
    SysPwrM_Main_Read_SysPwrM_MainIgnEdgeSts(&SysPwrM_MainBus.SysPwrM_MainIgnEdgeSts);
    SysPwrM_Main_Read_SysPwrM_MainVBatt1Mon(&SysPwrM_MainBus.SysPwrM_MainVBatt1Mon);
    SysPwrM_Main_Read_SysPwrM_MainVBatt2Mon(&SysPwrM_MainBus.SysPwrM_MainVBatt2Mon);
    SysPwrM_Main_Read_SysPwrM_MainDiagClrSrs(&SysPwrM_MainBus.SysPwrM_MainDiagClrSrs);
    SysPwrM_Main_Read_SysPwrM_MainArbVlvDriveState(&SysPwrM_MainBus.SysPwrM_MainArbVlvDriveState);
    SysPwrM_Main_Read_SysPwrM_MainVdd1Mon(&SysPwrM_MainBus.SysPwrM_MainVdd1Mon);
    SysPwrM_Main_Read_SysPwrM_MainVdd2Mon(&SysPwrM_MainBus.SysPwrM_MainVdd2Mon);
    SysPwrM_Main_Read_SysPwrM_MainVdd3Mon(&SysPwrM_MainBus.SysPwrM_MainVdd3Mon);
    SysPwrM_Main_Read_SysPwrM_MainVdd4Mon(&SysPwrM_MainBus.SysPwrM_MainVdd4Mon);
    SysPwrM_Main_Read_SysPwrM_MainVdd5Mon(&SysPwrM_MainBus.SysPwrM_MainVdd5Mon);
    SysPwrM_Main_Read_SysPwrM_MainVddMon(&SysPwrM_MainBus.SysPwrM_MainVddMon);
    SysPwrM_Main_Read_SysPwrM_MainMtrArbDriveState(&SysPwrM_MainBus.SysPwrM_MainMtrArbDriveState);

    /* Process */
	SysPwrM_Process(&SysPwrM_MainBus);
    /* Output */
    SysPwrM_Main_Write_SysPwrM_MainSysPwrMonData(&SysPwrM_MainBus.SysPwrM_MainSysPwrMonData);
    /*==============================================================================
    * Members of structure SysPwrM_MainSysPwrMonData 
     : SysPwrM_MainSysPwrMonData.SysPwrM_OverVolt_1st_Err;
     : SysPwrM_MainSysPwrMonData.SysPwrM_OverVolt_2st_Err;
     : SysPwrM_MainSysPwrMonData.SysPwrM_UnderVolt_1st_Err;
     : SysPwrM_MainSysPwrMonData.SysPwrM_UnderVolt_2st_Err;
     : SysPwrM_MainSysPwrMonData.SysPwrM_UnderVolt_3st_Err;
     : SysPwrM_MainSysPwrMonData.SysPwrM_Asic_Vdd_Err;
     : SysPwrM_MainSysPwrMonData.SysPwrM_Asic_OverVolt_Err;
     : SysPwrM_MainSysPwrMonData.SysPwrM_Asic_UnderVolt_Err;
     : SysPwrM_MainSysPwrMonData.SysPwrM_Asic_OverTemp_Err;
     : SysPwrM_MainSysPwrMonData.SysPwrM_Asic_Gndloss_Err;
     : SysPwrM_MainSysPwrMonData.SysPwrM_Asic_Comm_Err;
     : SysPwrM_MainSysPwrMonData.SysPwrM_IgnLine_Open_Err;
     =============================================================================*/
    

    SysPwrM_Main_Timer_Elapsed = STM0_TIM0.U - SysPwrM_Main_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define SYSPWRM_MAIN_STOP_SEC_CODE
#include "SysPwrM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
