/**
 * @defgroup SysPwrM_Process SysPwrM_Process
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        SenPwrM_Process.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "SysPwrM_Process.h"
#include "SysPwrM_Main.h"
#include "common.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define BATT_1ST_LOW_VOLT_DRIVE_OFF              U16_CALCVOLT_8V8
#define HMC_BATT_1ST_LOW_VOLT_DRIVE_OFF          U16_CALCVOLT_9V4
#define BATT_1ST_LOW_VOLT_EXC_DRIVE_OFF          U16_CALCVOLT_8V4
#define BATT_2ND_LOW_VOLT                        U16_CALCVOLT_8V0
#define HMC_BATT_2ND_LOW_VOLT                    U16_CALCVOLT_7V2
#define LOW_VOLTAGE_ERR_OFF_DIFFERENCE           U16_CALCVOLT_0V3

#define BATT_1ST_OVER_VOLT                       U16_CALCVOLT_17V0
#define BATT_2ND_OVER_VOLT                       U16_CALCVOLT_19V0
#define OVER_VOLTAGE_ERR_OFF_DIFFERENCE          U16_CALCVOLT_1V0

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
uint8_t underContolState = 0; /* 확인 필요 !-> fu1UnderControl */
uint8_t HwIgnState = 0; /* 확인 필요 !-> fu1UnderControl */
uint16_t u16WhlCheck = 0;
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
 
void SysPwrM_Process_Init(void)
{
	;
}

uint16 SysPwrM_WheelCheck(SysPwrM_Main_HdrBusType *pSysPwrMjWhlCheck)
{
	uint16 u16WheelStatus = 0;
	uint16 u16TempFlWhlSpd = 0;
	uint16 u16TempFrWhlSpd = 0;
	uint16 u16TempRlWhlSpd = 0;
	uint16 u16TempRrWhlSpd = 0;

	u16TempFlWhlSpd = pSysPwrMjWhlCheck->SysPwrM_MainWhlSpdInfo.FlWhlSpd;
	u16TempFrWhlSpd = pSysPwrMjWhlCheck->SysPwrM_MainWhlSpdInfo.FrWhlSpd;
	u16TempRlWhlSpd = pSysPwrMjWhlCheck->SysPwrM_MainWhlSpdInfo.RlWhlSpd;
	u16TempRrWhlSpd = pSysPwrMjWhlCheck->SysPwrM_MainWhlSpdInfo.RrlWhlSpd;

	if(u16TempFlWhlSpd>u16TempFrWhlSpd)
	{
		if(u16TempFlWhlSpd>u16TempRlWhlSpd)
		{
			if(u16TempFlWhlSpd>u16TempRrWhlSpd)
			{
				u16WheelStatus = u16TempFlWhlSpd;
			}
			else
			{
				u16WheelStatus = u16TempRrWhlSpd;
			}
		}
		else if(u16TempRlWhlSpd>u16TempRrWhlSpd)
		{
			u16WheelStatus = u16TempRlWhlSpd;
		}
		else
		{
			u16WheelStatus = u16TempRrWhlSpd;
		}
	}
	else if(u16TempFrWhlSpd>u16TempRlWhlSpd)
	{
		if(u16TempFrWhlSpd>u16TempRrWhlSpd)
		{
			u16WheelStatus = u16TempFrWhlSpd;
		}
		else
		{
			u16WheelStatus = u16TempRrWhlSpd;
		}
	}
	else if(u16TempRlWhlSpd>u16TempRrWhlSpd)
	{
		u16WheelStatus = u16TempRlWhlSpd;		
	}
	else
	{
		if(u16TempRrWhlSpd>u16TempFlWhlSpd)
		{
			u16WheelStatus = u16TempRrWhlSpd;		
		}
		else
		{
			u16WheelStatus = u16TempFlWhlSpd;		
		}
	}
	u16WheelStatus = (uint16)(u16WheelStatus/64);
	return u16WheelStatus;
}

void SysPwrM_Check1st_Volt(SysPwrM_Main_HdrBusType *pSysPwrMCheck1)
{
	uint16_t u16ChekcUnderVolOn = 0;
	uint16_t u16ChekcUnderVolOff = 0;	
	/* Over Voltage Check */
	if(pSysPwrMCheck1->SysPwrM_MainVBatt1Mon>BATT_1ST_OVER_VOLT)
	{
		pSysPwrMCheck1->SysPwrM_MainSysPwrMonData.SysPwrM_OverVolt_1st_Err = ERR_PREFAILED;
	}
	else
	{
		if(pSysPwrMCheck1->SysPwrM_MainSysPwrMonData.SysPwrM_OverVolt_1st_Err == ERR_PREFAILED)
		{
			if(pSysPwrMCheck1->SysPwrM_MainVBatt1Mon <= (BATT_1ST_OVER_VOLT-OVER_VOLTAGE_ERR_OFF_DIFFERENCE))
			{
				pSysPwrMCheck1->SysPwrM_MainSysPwrMonData.SysPwrM_OverVolt_1st_Err = ERR_PREPASSED;
			}
		}
		else
		{
			pSysPwrMCheck1->SysPwrM_MainSysPwrMonData.SysPwrM_OverVolt_1st_Err = ERR_PREPASSED;
		}
	}

	/* Under Voltage Check */
	if(pSysPwrMCheck1->SysPwrM_MainVBatt1Mon<U16_CALCVOLT_6V0)
	{
		if((underContolState==1)||(u16WhlCheck <U16_F64SPEED_7KPH))
		{
			/* Control or Standstill */
			u16ChekcUnderVolOn = BATT_1ST_LOW_VOLT_EXC_DRIVE_OFF;	 /* 8.4V */  
			u16ChekcUnderVolOff = u16ChekcUnderVolOn+LOW_VOLTAGE_ERR_OFF_DIFFERENCE;
		}
		else
		{
			u16ChekcUnderVolOn = HMC_BATT_1ST_LOW_VOLT_DRIVE_OFF;
			u16ChekcUnderVolOff = u16ChekcUnderVolOn+LOW_VOLTAGE_ERR_OFF_DIFFERENCE;
		}
	}
	
	if(pSysPwrMCheck1->SysPwrM_MainVBatt1Mon<u16ChekcUnderVolOn)
	{
		pSysPwrMCheck1->SysPwrM_MainSysPwrMonData.SysPwrM_UnderVolt_1st_Err = ERR_PREFAILED;
	}
	else
	{
		if(pSysPwrMCheck1->SysPwrM_MainVBatt1Mon>u16ChekcUnderVolOff)
		{
			pSysPwrMCheck1->SysPwrM_MainSysPwrMonData.SysPwrM_UnderVolt_1st_Err = ERR_PREPASSED;
		}
	}
}

void SysPwrM_Check2nd_Volt(SysPwrM_Main_HdrBusType *pSysPwrMCheck2)
{
	/* Over Voltage Check */
	if(pSysPwrMCheck2->SysPwrM_MainVBatt1Mon>BATT_2ND_OVER_VOLT)
	{
		pSysPwrMCheck2->SysPwrM_MainSysPwrMonData.SysPwrM_OverVolt_2st_Err = ERR_PREFAILED;
	}
	else
	{
		if(pSysPwrMCheck2->SysPwrM_MainSysPwrMonData.SysPwrM_OverVolt_2st_Err == ERR_PREFAILED)
		{
			if(pSysPwrMCheck2->SysPwrM_MainVBatt1Mon <= (BATT_2ND_OVER_VOLT-OVER_VOLTAGE_ERR_OFF_DIFFERENCE))
			{
				pSysPwrMCheck2->SysPwrM_MainSysPwrMonData.SysPwrM_OverVolt_2st_Err = ERR_PREPASSED;
			}
		}
		else
		{
			pSysPwrMCheck2->SysPwrM_MainSysPwrMonData.SysPwrM_OverVolt_2st_Err = ERR_PREPASSED;
		}
	}

	/* Under Voltage Check */
	if(pSysPwrMCheck2->SysPwrM_MainVBatt1Mon<BATT_2ND_LOW_VOLT)
	{
		pSysPwrMCheck2->SysPwrM_MainSysPwrMonData.SysPwrM_UnderVolt_2st_Err = ERR_PREFAILED;
	}
	else
	{
		if(pSysPwrMCheck2->SysPwrM_MainSysPwrMonData.SysPwrM_UnderVolt_2st_Err == ERR_PREFAILED)
		{
			if(pSysPwrMCheck2->SysPwrM_MainVBatt1Mon > (BATT_2ND_LOW_VOLT+LOW_VOLTAGE_ERR_OFF_DIFFERENCE))
			{
				pSysPwrMCheck2->SysPwrM_MainSysPwrMonData.SysPwrM_UnderVolt_2st_Err = ERR_PREPASSED;
			}
		}
		else
		{
			pSysPwrMCheck2->SysPwrM_MainSysPwrMonData.SysPwrM_UnderVolt_2st_Err = ERR_PREPASSED;
		}
	}
}

void SysPwrM_Check3rd_Volt(SysPwrM_Main_HdrBusType *pSysPwrMCheck3)
{
	/* Under Voltage Check */
	if(pSysPwrMCheck3->SysPwrM_MainVBatt1Mon<U16_CALCVOLT_7V0)
	{
		pSysPwrMCheck3->SysPwrM_MainSysPwrMonData.SysPwrM_UnderVolt_3st_Err = ERR_PREFAILED;
	}
	else
	{
		if(pSysPwrMCheck3->SysPwrM_MainSysPwrMonData.SysPwrM_UnderVolt_3st_Err == ERR_PREFAILED)
		{
			if(pSysPwrMCheck3->SysPwrM_MainVBatt1Mon > U16_CALCVOLT_7V5)
			{
				pSysPwrMCheck3->SysPwrM_MainSysPwrMonData.SysPwrM_UnderVolt_3st_Err = ERR_PREPASSED;
			}
		}
		else
		{
			pSysPwrMCheck3->SysPwrM_MainSysPwrMonData.SysPwrM_UnderVolt_3st_Err = ERR_PREPASSED;
		}
	}
}

void SysPwrM_CheckHwIGN(SysPwrM_Main_HdrBusType *pSysPwrMCheckIgn)
{
	if((pSysPwrMCheckIgn->SysPwrM_MainIgnOnOffSts == 0/*CE_OFF_STATE*/)&&(pSysPwrMCheckIgn->SysPwrM_MainRxClu2Info.IGN_SW == 0/*CE_ON_STATE*/)&&(u16WhlCheck>U16_F64SPEED_15KPH))
	{
		pSysPwrMCheckIgn->SysPwrM_MainSysPwrMonData.SysPwrM_IgnLine_Open_Err = ERR_PREFAILED;
	}
	if(pSysPwrMCheckIgn->SysPwrM_MainIgnOnOffSts == 1/*CE_ON_STATE*/)
	{
		pSysPwrMCheckIgn->SysPwrM_MainSysPwrMonData.SysPwrM_IgnLine_Open_Err = ERR_PREPASSED;
	}
	if(pSysPwrMCheckIgn->SysPwrM_MainRxClu2Info.IGN_SW == 1/*CE_OFF_STATE*/)/* CAN IGN State 확인필요 !! */
	{
		pSysPwrMCheckIgn->SysPwrM_MainSysPwrMonData.SysPwrM_IgnLine_Open_Err = ERR_INHIBIT;
	}		
}

void SysPwrM_Process(SysPwrM_Main_HdrBusType *pSysPwrMCheck)
{
	u16WhlCheck = SysPwrM_WheelCheck(pSysPwrMCheck);

	/* 1st Range Over Under Voltage Check : Batt1 */
	SysPwrM_Check1st_Volt(pSysPwrMCheck);

	/* 2nd Range Over Under Voltage Check : Batt1 */
	SysPwrM_Check2nd_Volt(pSysPwrMCheck);

	/* 3rd Range Over Under Voltage Check : Batt1 */
	SysPwrM_Check3rd_Volt(pSysPwrMCheck);

	/* IGN Line Open Check */
	SysPwrM_CheckHwIGN(pSysPwrMCheck);
} 
