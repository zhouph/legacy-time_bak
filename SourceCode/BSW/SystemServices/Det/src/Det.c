/******************************************************************************
**                                                                           **
** Copyright (C) Infineon Technologies (2013)                                **
**                                                                           **
** All rights reserved.                                                      **
**                                                                           **
** This document contains proprietary information belonging to Infineon      **
** Technologies. Passing on and copying of this document, and communication  **
** of its contents is not permitted without prior written authorization.     **
**                                                                           **
*******************************************************************************
**                                                                           **
**  $FILENAME   : Det.c $                                                    **
**                                                                           **
**  $CC VERSION : \main\3 $                                                  **
**                                                                           **
**  $DATE       : 2013-06-20 $                                               **
**                                                                           **
**  AUTHOR      : DL-AUTOSAR-Engineering                                     **
**                                                                           **
**  VENDOR      : Infineon Technologies                                      **
**                                                                           **
**  DESCRIPTION : This file contains stub for Det_ReportError                **
**                                                                           **
**  MAY BE CHANGED BY USER [yes/no]: Yes                                     **
**                                                                           **
******************************************************************************/
/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/

/* Inclusion of Platform_Types.h and Compiler.h */
#include "Std_Types.h"
#include "Mcal.h"
//#include "Test_Print.h"
#include "Det.h"

/*******************************************************************************
**                      Imported Compiler Switch Check                        **
*******************************************************************************/
/*******************************************************************************
**                      Private Macro Definitions                             **
*******************************************************************************/
/** \brief Det has not yet been inited */
#define DET_STATE_UNINIT      1U
/** \brief Det is initialized but was not yet been started */
#define DET_STATE_NOT_STARTED 2U
/** \brief Det is idle and ready to send an error if neccessary */
#define DET_STATE_IDLE        3U
/** \brief Det is sending an error and is waiting for the confirmation*/
#define DET_STATE_SENDING     4U

/*******************************************************************************
**                      Private Type Definitions                              **
*******************************************************************************/
typedef uint8 Det_StateType;
/*******************************************************************************
**                      Private Function Declarations                         **
*******************************************************************************/
/*******************************************************************************
**                      Global Constant Definitions                           **
*******************************************************************************/
#define DET_BUFFERSIZE 50
/*******************************************************************************
**                      Global Variable Definitions                           **
*******************************************************************************/
/*******************************************************************************
**                      Private Constant Definitions                          **
*******************************************************************************/
/*******************************************************************************
**                      Private Variable Definitions                          **
*******************************************************************************/
uint16 Det_WriteIndex;
uint16 Det_UsedSlots;
uint16 Det_ErrorLost;
Det_StateType   Det_State = DET_STATE_UNINIT;
Det_ErrorBufferType Det_ErrorBuffer[DET_BUFFERSIZE];
/*******************************************************************************
**                      Global Function Definitions                           **
*******************************************************************************/
void Det_Init(void)
{
   /* state change: UNINIT -> NOT_STARTED */
   Det_State = DET_STATE_NOT_STARTED;

   /* initialize all global variables */

   for (Det_WriteIndex = 0U;
        Det_WriteIndex < DET_BUFFERSIZE;
        ++Det_WriteIndex)
   {
      Det_ErrorBuffer[Det_WriteIndex].ModuleId   = 0U;
      Det_ErrorBuffer[Det_WriteIndex].InstanceId = 0U;
      Det_ErrorBuffer[Det_WriteIndex].ApiId      = 0U;
      Det_ErrorBuffer[Det_WriteIndex].ErrorId    = 0U;
   }

   Det_WriteIndex = 0U;
   Det_ErrorLost  = 0U;
   Det_UsedSlots  = 0U;
}

/* Test Stub for Det_ReportError */ 
void Det_ReportError(uint16 ModuleId,uint8 InstanceId,uint8 ApiId,uint8 ErrorId) 
{
//  print_f("\n DET OCCURED \n ");
//  print_f("ModuleId:%d, InstanceId: %d, ApiId:%d, ErrorId:%d"
//           ,ModuleId, InstanceId, ApiId, ErrorId);
	   if (Det_State != DET_STATE_UNINIT)
	   {
	      /* call the notification functions which are configured */
	      //DET_NOTIFICATIONS(ModuleId, InstanceId, ApiId, ErrorId);



	      /* lock access to DET global variables */
	      //SchM_Enter_Det_SCHM_DET_EXCLUSIVE_AREA_0();

	      /* queue the reported error */


	      /* Keep first errors reported to the Det */
	      if (Det_UsedSlots < DET_BUFFERSIZE)
	      {
	         Det_ErrorBuffer[Det_WriteIndex].ModuleId   = ModuleId;
	         Det_ErrorBuffer[Det_WriteIndex].InstanceId = InstanceId;
	         Det_ErrorBuffer[Det_WriteIndex].ApiId      = ApiId;
	         Det_ErrorBuffer[Det_WriteIndex].ErrorId    = ErrorId;

	         ++Det_WriteIndex; /* increase write index, overflow does not matter */
	         ++Det_UsedSlots;  /* increment used slot counter */
	      }
	      else /* Det_UsedSlots >= DET_BUFFERSIZE */
	      {
	         if (Det_ErrorLost < ((uint16)0xFFFFU))
	         {
	            ++Det_ErrorLost;       /* count lost error reports */
	         }
	      }



	      /* unlock access to DET global variables */
	      //SchM_Exit_Det_SCHM_DET_EXCLUSIVE_AREA_0();


	   }
	   /* else Det_State == DET_STATE_UNINIT: ignore the function call in the
	    * uninitialized state. Other modules would report such an development
	    * error to the Det but the Det cannot report this to itself. */


}
/*******************************************************************************
**                      Private Function Definitions                          **
*******************************************************************************/


