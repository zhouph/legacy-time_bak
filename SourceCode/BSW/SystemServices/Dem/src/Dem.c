/********************************************************************************
**                                                                            **
** Copyright (C) Infineon Technologies (2012)                                 **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to Infineon       **
** Technologies. Passing on and copying of this document, and communication   **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME  : Dem.c                                                         **
**                                                                            **
**  VERSION   : 1.0.3                                                         **
**                                                                            **
**  DATE      : 2012.11.29                                                    **
**                                                                            **
**  VARIANT   : NA                                                            **
**                                                                            **
**  PLATFORM  : Infineon Aurix                                                **
**                                                                            **
**  COMPILER  : Tasking / Diab / GNU                                          **
**                                                                            **
**  AUTHOR    : DL-AUTOSAR-Engineering                                        **
**                                                                            **
**  VENDOR    : Infineon Technologies                                         **
**                                                                            **
**                                                                            **
**  DESCRIPTION  : Test Stub for Dem Module                                   **
**                                                                            **
**  SPECIFICATION(S) :  NA                                                    **
**                                                                            **
**  MAY BE CHANGED BY USER [yes/no]: yes                                      **
**                                                                            **
*******************************************************************************/

/******************************************************************************
**                      Author(s) Identity                                   **
*******************************************************************************
**                                                                           **
** Initials     Name                                                         **
** --------------------------------------------------------------------------**
** AT          Angeswaran Thangaswamy                                        **
** Chaitra     Chaitra Shanthpur                                             **
** JV          Jaidev Venkataraman                                           **
** RS          Roopa Sirur                                                   **
******************************************************************************/

/*******************************************************************************
**                      Revision Control History                              **
*******************************************************************************/
/* 
 * V1.0.3  2012.11.29  AT       : Updated to remove unused param warning in GNU.
 * V1.0.2  2007.02.26  RS       : File Header Changed.
 * V1.0.1: 2006.07.18, JV       : Changed Dem sub function to 
                                  Dem_ReportErrorStatus from Dem_SetEventStatus
 * V1.0.0: 2005.10.11, Chaitra  :  DEM error report functionality
 *
 */
/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/

/* Inclusion of Platform_Types.h and Compiler.h */
#include "Std_Types.h"
#include "Mcal.h"
//#include "Test_Print.h"
#include "Dem.h"


/*******************************************************************************
**                      Imported Compiler Switch Check                        **
*******************************************************************************/
/*******************************************************************************
**                      Private Macro Definitions                             **
*******************************************************************************/
/*******************************************************************************
**                      Private Type Definitions                              **
*******************************************************************************/
/*******************************************************************************
**                      Private Function Declarations                         **
*******************************************************************************/
/*******************************************************************************
**                      Global Constant Definitions                           **
*******************************************************************************/
/*******************************************************************************
**                      Global Variable Definitions                           **
*******************************************************************************/
/*******************************************************************************
**                      Private Constant Definitions                          **
*******************************************************************************/
/*******************************************************************************
**                      Private Variable Definitions                          **
*******************************************************************************/
/*******************************************************************************
**                      Global Function Definitions                           **
*******************************************************************************/
/* Test Stub For Dem_ReportErrorStatus */
void Dem_ReportErrorStatus ( Dem_EventIdType EventId,
                          Dem_EventStatusType EventStatus)
{
  //print_f("\n DEM OCCURED \n");
  //print_f("EventId:%d", EventId);
  /* this line is provided to remove unused param warning in GNU */
  UNUSED_PARAMETER(EventStatus)
}
/*******************************************************************************
**                      Private Function Definitions                          **
*******************************************************************************/







