/*******************************************************************************
**                                                                            **
** Copyright (C) Infineon Technologies (2009)                                 **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to Infineon       **
** Technologies. Passing on and copying of this document, and communication   **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME  : Dem_Types.h                                                   **
**                                                                            **
**  VERSION   : 1.0.1                                                         **
**                                                                            **
**  DATE      : 2009.06.25                                                    **
**                                                                            **
**  VARIANT   : NA                                                            **
**                                                                            **
**  PLATFORM  : Independent                                                   **
**                                                                            **
**  COMPILER  : Independent                                                   **
**                                                                            **
**  AUTHOR    : DL-AUTOSAR-Engineering                                        **
**                                                                            **
**  VENDOR    : Infineon Technologies                                         **
**                                                                            **
**  DESCRIPTION  : Test Stub file for Dem Module                              **
**                                                                            **
**  SPECIFICATION(S) :  NA                                                    **
**                                                                            **
**  MAY BE CHANGED BY USER [yes/no]: yes                                      **
**                                                                            **
*******************************************************************************/

/******************************************************************************
**                      Author(s) Identity                                   **
*******************************************************************************
**                                                                           **
** Initials     Name                                                         **
** --------------------------------------------------------------------------**
** HM           Hamzath Mohammed                                             **
**                                                                           **
******************************************************************************/

/*******************************************************************************
**                      Revision Control History                              **
*******************************************************************************/
/*
 * V1.0.1: 2009.06.25, HM  : typecast to Dem_EventStatusType
 * V1.0.0: 2007.04.16, RS  : Created the Initial Version *
*/

#ifndef  _DEM_TYPES_H
#define  _DEM_TYPES_H                

/*******************************************************************************
**                      Includes
*******************************************************************************/

/*******************************************************************************
**                      Global Macro Definitions                              **
*******************************************************************************/
#define DEM_EVENT_STATUS_PASSED ((Dem_EventStatusType)0x00)
#define DEM_EVENT_STATUS_FAILED ((Dem_EventStatusType)0x01)
/*******************************************************************************
**                      Global Type Definitions                               **
*******************************************************************************/
typedef uint16 Dem_EventIdType; 
typedef uint8 Dem_EventStatusType;
/*******************************************************************************
**                      Global Constant Declarations                          **
*******************************************************************************/

/*******************************************************************************
**                      Global Variable Declarations                          **
*******************************************************************************/

/*******************************************************************************
**                      Global Function Declarations                          **
*******************************************************************************/

/*******************************************************************************
**                      Global Inline Function Definitions                    **
*******************************************************************************/
#endif
