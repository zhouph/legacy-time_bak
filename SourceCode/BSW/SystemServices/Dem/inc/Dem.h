/*******************************************************************************
**                                                                            **
** Copyright (C) Infineon Technologies (2012)                                 **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to Infineon       **
** Technologies. Passing on and copying of this document, and communication   **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME  : Dem.h                                                         **
**                                                                            **
**  VERSION   : 1.0.1                                                         **
**                                                                            **
**  DATE      : 2012.06.01                                                    **
**                                                                            **
**  VARIANT   : NA                                                            **
**                                                                            **
**  PLATFORM  : Independent                                                   **
**                                                                            **
**  COMPILER  : Independent                                                   **
**                                                                            ** 
**  AUTHOR    : DL-AUTOSAR-Engineering                                        **
**                                                                            **
**  VENDOR    : Infineon Technologies                                         **              `
**                                                                            **
**  DESCRIPTION  : Test Stub file for Dem Module                              **
**                                                                            **
**  SPECIFICATION(S) :  NA                                                    **
**                                                                            **
**  MAY BE CHANGED BY USER [yes/no]: yes                                      **
**                                                                            **
*******************************************************************************/

/******************************************************************************
**                      Author(s) Identity                                    **
********************************************************************************
**                                                                            **
** Initials     Name                                                          **
** ---------------------------------------------------------------------------**
** RS           Roopa Sirur                                                   **
** MK           Mundhra Karan                                                  **
**                                                                            **
*******************************************************************************/

/*******************************************************************************
**                      Revision Control History                              **
*******************************************************************************/
/*
 * V1.0.0: 2007.04.16, RS  : Created the Initial Version *
 * V1.0.1: 2012.06.01, MK  : Removed inclusion of Dem_IntErrId.h, 
                             not requied for AS403 *
*/

#ifndef     _DEM_H
#define     _DEM_H                

/*******************************************************************************
**                      Includes
*******************************************************************************/
#include "Std_Types.h" 
#include "Dem_Types.h"
#include "Dem_PBcfg.h"

/*******************************************************************************
**                      Global Macro Definitions                              **
*******************************************************************************/
#define DEM_AR_RELEASE_MAJOR_VERSION      (4U)
#define DEM_AR_RELEASE_MINOR_VERSION      (0U)
#define DEM_AR_RELEASE_REVISION_VERSION   (2U)

/*******************************************************************************
**                      Global Type Definitions                               **
*******************************************************************************/

/*******************************************************************************
**                      Global Constant Declarations                          **
*******************************************************************************/

/*******************************************************************************
**                      Global Variable Declarations                          **
*******************************************************************************/

/*******************************************************************************
**                      Global Function Declarations                          **
*******************************************************************************/
extern void Dem_ReportErrorStatus ( Dem_EventIdType EventId,
                          Dem_EventStatusType EventStatus);

/*******************************************************************************
**                      Global Inline Function Definitions                    **
*******************************************************************************/

#endif
