# \file
#
# \brief SystemServices
#
# This file contains the implementation of the BSW
# module SystemServices.
#
# \author Mando, Advanced R&D, Korea

#################################################################
# DEFINITIONS

SystemServices_CORE_PATH     := $(MANDO_BSW_ROOT)\SystemServices
SystemServices_Dem_PATH      := $(SystemServices_CORE_PATH)\Dem
SystemServices_Det_PATH      := $(SystemServices_CORE_PATH)\Det
SystemServices_EcuM_PATH     := $(SystemServices_CORE_PATH)\EcuM
SystemServices_OS_PATH       := $(SystemServices_CORE_PATH)\OS
SystemServices_SchM_PATH     := $(SystemServices_CORE_PATH)\SchM



CC_INCLUDE_PATH    += $(SystemServices_Dem_PATH)\inc
CC_INCLUDE_PATH    += $(SystemServices_Det_PATH)\inc
CC_INCLUDE_PATH    += $(SystemServices_EcuM_PATH)\inc
CC_INCLUDE_PATH    += $(SystemServices_OS_PATH)\inc
CC_INCLUDE_PATH    += $(SystemServices_SchM_PATH)\inc