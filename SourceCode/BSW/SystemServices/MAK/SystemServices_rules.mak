# \file
#
# \brief SystemServices
#
# This file contains the implementation of the BSW
# module SystemServices.
#
# \author Mando, Advanced R&D, Korea

#################################################################
# REGISTRY

LIBRARIES_TO_BUILD     += SystemServices_src

SystemServices_src_FILES       += $(SystemServices_Dem_PATH)\src\Dem.c
SystemServices_src_FILES       += $(SystemServices_Det_PATH)\src\Det.c
SystemServices_src_FILES       += $(SystemServices_EcuM_PATH)\src\EcuM.c
SystemServices_src_FILES       += $(SystemServices_EcuM_PATH)\src\EcuM_Callout_Stubs.c
SystemServices_src_FILES       += $(SystemServices_OS_PATH)\src\Os.c
SystemServices_src_FILES       += $(SystemServices_SchM_PATH)\src\SchM.c

#################################################################
