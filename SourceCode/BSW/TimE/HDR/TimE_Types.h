/**
 * @defgroup Proxy_Types Proxy_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        TimE_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/
#ifndef TIME_TYPES_H_
#define TIME_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Platform_Types.h"
/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
/*stuct for alive supervision checkpoint configuation*/
typedef struct 			
{
	uint8 	Tolerance_AS;
	uint32 	Expected_period;
	float 	Margin_level;   
}TimEAScheckpointCfg_t;	

/*stuct for deadline supervision checkpoint configuation*/
typedef struct			
{
	boolean StartCp;
	boolean EndCp;
	uint32 	DeadlineMin_NS;
	uint32	DeadlineMax_NS;
	uint8	Tolerance_DS;
}TimEDScheckpointCfg_t;	

/*stuct for logical supervision graph configuation*/
typedef struct 
{
	uint8 PredCp;
	uint8 CpId;
}TimELSGraphCfg_t;

/*stuct for logical supervision graphlist configuation*/
typedef struct 
{
	TimELSGraphCfg_t 	TimELSGraphCfg[20];
	uint8				Tolerance_LS;
	uint8				Graphsize;
}TimELSGraphlistCfg_t;

/*stuct for return value of TimE*/
typedef struct			
{
	uint8 	CP_State; 	//0: STATE_OK, 1: STATE_FAIL, 2: STATE_ERROR, 3: STATE_CFG_FAULT
	uint8	Graph_State;	//0: INITIAL, 1:STATE_OK, 2:STATE_FAIL
	uint8 	Error_CP;
	uint8 	FaultCounter;
	boolean	TimEErrorFlg;
}TimE_Return_t;	

//indication
typedef struct 			
{
	uint8	ExpectedIndication;	
	uint8 	MaxMargin;
	uint8 	MinMargin;
	uint8 	SupervisionRefCycle;
	uint8 	Tolerance_AS;   
}TimEAScheckpointCfg0_t;

typedef struct
{
	uint8	AS_Status;
	uint8	AS_ErrorCP;
}TimE_EachASStatus_t;

typedef struct{
	uint8 TimE_asresult;
	uint8 TimE_dsresult;
	uint8 TimE_lsresult;
}TimE_LocalStatus_t;

typedef struct 
{
	uint8	GlobalStatus;
	uint8	ExpiredSupervisionCycleCnt;
}TimE_GlobalStatus_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PROXY_TYPES_H_ */
/** @} */
