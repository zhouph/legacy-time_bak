/**
 * @defgroup 
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        TimE.h
 * @brief       Template file
 * @date        2015. 
 ******************************************************************************/

#ifndef TIME_H_
#define TIME_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "TimE_Types.h"
#include "TimE_Cfg.h"
/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern TimE_Return_t SetAlivesupervisionCheckpoint(uint8 ASCPId);
extern TimE_Return_t SetDeadlinesupervisionCheckpoint(uint8 DSCPId);
extern TimE_Return_t SetLogicalsupervisionCheckpoint(uint8 GraphId, uint8 LSCPId, uint8 CpType);
extern void TimELogicalSupervisionTestCase_4(void);
extern void TimELogicalSupervisionTestCase_5(void);
extern void TimELogicalSupervisionTestCase_6(void);
extern void TimELogicalSupervisionTestCase_7(void);

//indication
extern TimE_Return_t SetAlivesupervisionCheckpoint0(uint8 ASCPId);
extern void TimE_GlobalSupervision(void);
extern void TimE_init(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* TIME_H_ */
/** @} */
