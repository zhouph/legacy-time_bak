/**
 * @defgroup 
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        TimE.c
 * @brief       Template file
 * @date        2015. . .
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "TimE.h" 

 //test 
 #include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
/** Variable Section (NOINIT_UNSPECIFIED)**/
/*for alive supervision*/
TimE_Return_t AS_return[TIME_AS_CHECKPOINT_NUM];  
boolean FirstExecution[TIME_AS_CHECKPOINT_NUM];
uint32	ASCheckpointOld[TIME_AS_CHECKPOINT_NUM];
uint32	ASCheckpoint[TIME_AS_CHECKPOINT_NUM];
#ifdef TIME_DEBUG_MODE
uint32 Debug_ASM_period_ns[TIME_AS_CHECKPOINT_NUM];
#endif 


/*for deadline supervision*/
uint32 DeadlineStartCp[TIME_DS_CHECKPOINT_NUM];
uint32 DeadlineEndCp[TIME_DS_CHECKPOINT_NUM];
TimE_Return_t DS_return[TIME_DS_CHECKPOINT_NUM];
#ifdef TIME_DEBUG_MODE
uint32	Debug_DSM_execution_ns[TIME_DS_CHECKPOINT_NUM];
#endif 

/*for logical supervision*/
uint8	LSLastVisitedCp[TIME_LS_GRAPH_NUM];
TimE_Return_t LS_return[TIME_LS_GRAPH_NUM];
boolean TimELSFaultFlg[TIME_LS_GRAPH_NUM]; 

//indication
uint8 TimE_ASMinbound[TIME_AS_CHECKPOINT_NUM];
uint8 TimE_ASMaxbound[TIME_AS_CHECKPOINT_NUM];
boolean TimE_initialized=FALSE; 
boolean TimE_AL_Active = FALSE; 
uint8	TimE_EachSupervisionCycleCnt;
uint32 TimE_GlobalSupervisionCycleCnt;
uint8	CPRefCycleCnt[TIME_AS_CHECKPOINT_NUM];

TimE_EachASStatus_t	TimE_EachASStatus[TIME_AS_CHECKPOINT_NUM];
uint8 Alivecounter[TIME_AS_CHECKPOINT_NUM]={0};
uint8 AlivecounterOld[TIME_AS_CHECKPOINT_NUM]={0};
TimE_LocalStatus_t TimE_LocalStatus;
TimE_GlobalStatus_t TimE_GlobalStatus;


/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
#if (ALIVESUPERVISION_PERIOD_MEASURE == STD_ON)
TimE_Return_t SetAlivesupervisionCheckpoint(uint8 ASCPId)
{
	uint32 min_bound;
	uint32 max_bound;
	uint32 M_period, M_period_ns;  
	
	if(FirstExecution[ASCPId] == 0)
	{	
		ASCheckpoint[ASCPId] = BASICTIMETICK;
		FirstExecution[ASCPId]=1;
	}
	else 
	{
		ASCheckpointOld[ASCPId]	= ASCheckpoint[ASCPId];
		ASCheckpoint[ASCPId] = BASICTIMETICK;
		
		M_period = ASCheckpoint[ASCPId] - ASCheckpointOld[ASCPId]; 
		M_period_ns = M_period * TICKTONS;		
		min_bound = TimEAScheckpointCfg[ASCPId].Expected_period - (TimEAScheckpointCfg[ASCPId].Expected_period*TimEAScheckpointCfg[ASCPId].Margin_level);
		max_bound = TimEAScheckpointCfg[ASCPId].Expected_period + (TimEAScheckpointCfg[ASCPId].Expected_period*TimEAScheckpointCfg[ASCPId].Margin_level);  
	
#ifdef TIME_DEBUG_MODE
		Debug_ASM_period_ns[ASCPId] = M_period_ns;
#endif 		
		
		if((M_period_ns >= min_bound) && (M_period_ns <= max_bound))
		{
			// success 
			AS_return[ASCPId].CP_State =STATE_OK; 
		}
		else 
		{
			//fail
			AS_return[ASCPId].CP_State = STATE_FAULT;  
			AS_return[ASCPId].Error_CP = ASCPId;
			AS_return[ASCPId].FaultCounter ++;
			
			if(AS_return[ASCPId].FaultCounter >= TimEAScheckpointCfg[ASCPId].Tolerance_AS)
			{
				//error, fail flag set 
				AS_return[ASCPId].TimEErrorFlg =1;
				AS_return[ASCPId].CP_State = STATE_ERROR; 
				AS_return[ASCPId].FaultCounter =0; 
			}
		}
	}

	return AS_return[ASCPId]; 
	
}
#endif //(ALIVESUPERVISION_PERIOD_MEASURE == STD_ON)

TimE_Return_t SetDeadlinesupervisionCheckpoint(uint8 DSCPId)
{
	uint32 M_execution, M_execution_ns;
	
	if((TimEDScheckpointCfg[DSCPId].StartCp == TRUE)&&(TimEDScheckpointCfg[DSCPId].EndCp == FALSE))	//START CP
	{
		DeadlineStartCp[DSCPId] = BASICTIMETICK;
		DS_return[DSCPId].CP_State = STATE_OK;
	}
	else if((TimEDScheckpointCfg[DSCPId].StartCp == FALSE)&&(TimEDScheckpointCfg[DSCPId].EndCp == TRUE))	//END CP 
	{
		DeadlineEndCp[DSCPId] = BASICTIMETICK;
		M_execution = DeadlineEndCp[DSCPId] - DeadlineStartCp[DSCPId-1];
		M_execution_ns = M_execution * TICKTONS;
		
#ifdef TIME_DEBUG_MODE
		Debug_DSM_execution_ns[DSCPId] = M_execution_ns;
#endif 
		
		if((M_execution_ns >= TimEDScheckpointCfg[DSCPId].DeadlineMin_NS) && (M_execution_ns <=  TimEDScheckpointCfg[DSCPId].DeadlineMax_NS))
		{
			//success
			DS_return[DSCPId].CP_State = STATE_OK;
		}
		else 
		{
			//fail
			DS_return[DSCPId].FaultCounter ++;
			DS_return[DSCPId].CP_State = STATE_FAULT;  
			DS_return[DSCPId].Error_CP = DSCPId;
			if(DS_return[DSCPId].FaultCounter >= TimEDScheckpointCfg[DSCPId].Tolerance_DS)
			{
				//error, fail flag set 
				DS_return[DSCPId].TimEErrorFlg =1;
				DS_return[DSCPId].CP_State = STATE_ERROR;  
				DS_return[DSCPId].FaultCounter =0; 
			}
			
		}
	}
	else 
	{
		DS_return[DSCPId].TimEErrorFlg=1; 
		DS_return[DSCPId].CP_State = STATE_CFG_FAULT; 
		DS_return[DSCPId].Error_CP = DSCPId;	
	}
	
	return DS_return[DSCPId];
}

TimE_Return_t SetLogicalsupervisionCheckpoint(uint8 GraphId, uint8 LSCPId, uint8 CpType) 
{
	uint8 i;	
	if(LS_return[GraphId].Graph_State != STATE_ERROR)
	{	
		if((CpType == LS_FIRST_CHECKPOINT)&&(LSLastVisitedCp[GraphId] ==0))	//lastcpÂ¿Â¡Â¼Â­ clear ÂµÃ‡Â¾ÃºÂ½ 
		{
			LSLastVisitedCp[GraphId] = LSCPId;
			TimELSFaultFlg[TIME_LS_GRAPH_NUM] = FALSE;
		} 
		else if((CpType == LS_FIRST_CHECKPOINT)&&(LSLastVisitedCp[GraphId] !=0))	//lastcpÂ¿Â¡Â¼Â­ clear ÂµÃ‡ÃÃ¶ Â¾ÃŠÂ¾Ã’Â½ 
		{
			TimELSFaultFlg[TIME_LS_GRAPH_NUM] = TRUE;
		}
		else
		{			
			for(i=0; i<TimELSGraphlistCfg[GraphId].Graphsize; i++)
			{	
					if((LSCPId == TimELSGraphlistCfg[GraphId].TimELSGraphCfg[i].CpId)&&(TimELSGraphlistCfg[GraphId].TimELSGraphCfg[i].PredCp == LSLastVisitedCp[GraphId]))
					{
						if(CpType != LS_LAST_CHECKPOINT)
						{	//cp ok
							LSLastVisitedCp[GraphId] = LSCPId;
						}
						else 
						{	//graph ok
							LSLastVisitedCp[GraphId] =0;	//clear
						}
						LS_return[GraphId].CP_State = STATE_OK;
						TimELSFaultFlg[TIME_LS_GRAPH_NUM] = FALSE;
						
						return LS_return[GraphId];	
					}										
					else 
					{
						if(i==(TimELSGraphlistCfg[GraphId].Graphsize-1))
						{					
							TimELSFaultFlg[TIME_LS_GRAPH_NUM] = TRUE; //graph size Â´Ã™ ÂµÂ¹Â¶Â§Â±Ã®ÃÃ¶ Â¸Ã¸ÃƒÂ£Â¸Â¸Ã© 		
						}
					}
			}				
		}		
		if(TimELSFaultFlg[TIME_LS_GRAPH_NUM] == TRUE)
		{	//fail
			if(CpType == LS_LAST_CHECKPOINT)				
			{
				LSLastVisitedCp[GraphId] =0;	//clear 
			}
			else
			{
				LSLastVisitedCp[GraphId] = LSCPId;
			}				
			LS_return[GraphId].FaultCounter ++;
			LS_return[GraphId].CP_State = STATE_FAULT;  
			LS_return[GraphId].Error_CP = LSCPId;
			LS_return[GraphId].Graph_State = STATE_FAULT;
			if(LS_return[GraphId].FaultCounter >= TimELSGraphlistCfg[GraphId].Tolerance_LS)
			{
				//error, fail flag set 
				LS_return[GraphId].TimEErrorFlg =1;
				LS_return[GraphId].CP_State = STATE_ERROR;  
				LS_return[GraphId].FaultCounter =0; 
				LS_return[GraphId].Graph_State = STATE_ERROR;				
			}
		}
	}
	return LS_return[GraphId];	
}

/*******************************************************************************************************************************************
*************************************************LS Test code ******************************************************************************
********************************************************************************************************************************************/  
uint8 a;
void TimELogicalSupervisionTestCase_4(void)
{
	LSresult[TIME_LS_GRAPH1]= SetLogicalsupervisionCheckpoint(TIME_LS_GRAPH1, TIME_LS_CHECKPOINT_FOR_GRAPH1_CP1, LS_FIRST_CHECKPOINT);	  //1
	
	LSresult[TIME_LS_GRAPH1]= SetLogicalsupervisionCheckpoint(TIME_LS_GRAPH1, TIME_LS_CHECKPOINT_FOR_GRAPH1_CP2, LS_RELATION_CHECKPOINT); //2
	
	LSresult[TIME_LS_GRAPH1]= SetLogicalsupervisionCheckpoint(TIME_LS_GRAPH1, TIME_LS_CHECKPOINT_FOR_GRAPH1_CP3, LS_RELATION_CHECKPOINT); //3
	
	if(a==0)
	{
		LSresult[TIME_LS_GRAPH1]= SetLogicalsupervisionCheckpoint(TIME_LS_GRAPH1, TIME_LS_CHECKPOINT_FOR_GRAPH1_CP4, LS_LAST_CHECKPOINT);   //4
	}
	else
	{
		LSresult[TIME_LS_GRAPH1]= SetLogicalsupervisionCheckpoint(TIME_LS_GRAPH1, TIME_LS_CHECKPOINT_FOR_GRAPH1_CP5, LS_LAST_CHECKPOINT);   //5
	}
	
	a=~a;
}		
	
boolean b;
void TimELogicalSupervisionTestCase_5(void)
{
	LSresult[TIME_LS_GRAPH2]= SetLogicalsupervisionCheckpoint(TIME_LS_GRAPH2, TIME_LS_CHECKPOINT_FOR_GRAPH2_CP1, LS_FIRST_CHECKPOINT);	  //1
	
	LSresult[TIME_LS_GRAPH2]= SetLogicalsupervisionCheckpoint(TIME_LS_GRAPH2, TIME_LS_CHECKPOINT_FOR_GRAPH2_CP2, LS_RELATION_CHECKPOINT); //2
		
	//LSresult[TIME_LS_GRAPH2]= SetLogicalsupervisionCheckpoint(TIME_LS_GRAPH2, TIME_LS_CHECKPOINT_FOR_GRAPH2_CP4, LS_RELATION_CHECKPOINT);   //4
	if(b==0)
	{
		LSresult[TIME_LS_GRAPH2]= SetLogicalsupervisionCheckpoint(TIME_LS_GRAPH2, TIME_LS_CHECKPOINT_FOR_GRAPH2_CP3, LS_RELATION_CHECKPOINT);   //3
	}
	else
	{
		LSresult[TIME_LS_GRAPH2]= SetLogicalsupervisionCheckpoint(TIME_LS_GRAPH2, TIME_LS_CHECKPOINT_FOR_GRAPH2_CP6, LS_RELATION_CHECKPOINT);   //6
	}
		
	LSresult[TIME_LS_GRAPH2]= SetLogicalsupervisionCheckpoint(TIME_LS_GRAPH2, TIME_LS_CHECKPOINT_FOR_GRAPH2_CP4, LS_RELATION_CHECKPOINT);   //4
	
	LSresult[TIME_LS_GRAPH2]= SetLogicalsupervisionCheckpoint(TIME_LS_GRAPH2, TIME_LS_CHECKPOINT_FOR_GRAPH2_CP5, LS_LAST_CHECKPOINT);   //5
	
	b=~b;
}	

boolean c;
void TimELogicalSupervisionTestCase_6(void)
{
	LSresult[TIME_LS_GRAPH3]= SetLogicalsupervisionCheckpoint(TIME_LS_GRAPH3, TIME_LS_CHECKPOINT_FOR_GRAPH3_CP1, LS_FIRST_CHECKPOINT);	  //1
	
	LSresult[TIME_LS_GRAPH3]= SetLogicalsupervisionCheckpoint(TIME_LS_GRAPH3, TIME_LS_CHECKPOINT_FOR_GRAPH3_CP2, LS_RELATION_CHECKPOINT); //2
	
	if(c==0)
	{
		LSresult[TIME_LS_GRAPH3]= SetLogicalsupervisionCheckpoint(TIME_LS_GRAPH3, TIME_LS_CHECKPOINT_FOR_GRAPH3_CP3, LS_RELATION_CHECKPOINT);   //3
	}
	
    LSresult[TIME_LS_GRAPH3]= SetLogicalsupervisionCheckpoint(TIME_LS_GRAPH3, TIME_LS_CHECKPOINT_FOR_GRAPH3_CP4, LS_RELATION_CHECKPOINT);   //4  
    
    LSresult[TIME_LS_GRAPH3]= SetLogicalsupervisionCheckpoint(TIME_LS_GRAPH3, TIME_LS_CHECKPOINT_FOR_GRAPH3_CP5, LS_LAST_CHECKPOINT);   //5
	
	c=~c;
}
	
boolean d;
void TimELogicalSupervisionTestCase_7(void)
{
	LSresult[TIME_LS_GRAPH4]= SetLogicalsupervisionCheckpoint(TIME_LS_GRAPH4, TIME_LS_CHECKPOINT_FOR_GRAPH4_CP1, LS_FIRST_CHECKPOINT);	  //1
	
	LSresult[TIME_LS_GRAPH4]= SetLogicalsupervisionCheckpoint(TIME_LS_GRAPH4, TIME_LS_CHECKPOINT_FOR_GRAPH4_CP2, LS_RELATION_CHECKPOINT); //2
	
	//LSresult[TIME_LS_GRAPH4]= SetLogicalsupervisionCheckpoint(TIME_LS_GRAPH4, TIME_LS_CHECKPOINT_FOR_GRAPH4_CP3, LS_RELATION_CHECKPOINT);   //3
	
	if(d==0)
	{
		LSresult[TIME_LS_GRAPH4]= SetLogicalsupervisionCheckpoint(TIME_LS_GRAPH4, TIME_LS_CHECKPOINT_FOR_GRAPH4_CP3, LS_RELATION_CHECKPOINT);   //3
	}
	
	LSresult[TIME_LS_GRAPH4]= SetLogicalsupervisionCheckpoint(TIME_LS_GRAPH4, TIME_LS_CHECKPOINT_FOR_GRAPH4_CP4, LS_RELATION_CHECKPOINT);   //4
    
    LSresult[TIME_LS_GRAPH4]= SetLogicalsupervisionCheckpoint(TIME_LS_GRAPH4, TIME_LS_CHECKPOINT_FOR_GRAPH4_CP5, LS_LAST_CHECKPOINT);   //5

	d=~d;
} 


/***************************************************************************************************************
Indication
****************************************************************************************************************/
#if (ALIVESUPERVISION_INDICATION == STD_ON)
void TimE_init(void)
{
	uint8 i;

	for(i=0; i<TIME_AS_CHECKPOINT_NUM;i++)
	{
			TimE_ASMinbound[i] = TimEAScheckpointCfg0[i].ExpectedIndication + TimEAScheckpointCfg0[i].MinMargin;
			TimE_ASMaxbound[i] = TimEAScheckpointCfg0[i].ExpectedIndication + TimEAScheckpointCfg0[i].MaxMargin;

			AS_return[i].CP_State = STATE_DEACTIVATED;
	}

	TimE_initialized = TRUE; 
}

void TimE_SetEachASStatus(void)
{
	uint8 i;

	if(TimE_AL_Active == TRUE)
	{

		TimE_EachSupervisionCycleCnt = TimE_GlobalSupervisionCycleCnt % MAX_SUPERVISION_CYCLE_COUNTER ; 

		for(i=0; i<TIME_AS_CHECKPOINT_NUM;i++)
		{
			CPRefCycleCnt[i] = TimE_EachSupervisionCycleCnt % TimEAScheckpointCfg0[i].SupervisionRefCycle;	

			if(CPRefCycleCnt[i]==0)	
			{	
				if((Alivecounter[i]==0)&&(AlivecounterOld[i]==0))
				{
					TimE_EachASStatus[i].AS_Status = TimE_AS_STATE_DEACTIVATED;
					TimE_EachASStatus[i].AS_ErrorCP = i;
				}
				else if((TimE_ASMinbound[i]<=Alivecounter[i]) && (TimE_ASMaxbound[i]>=Alivecounter[i]))	
				{
					TimE_EachASStatus[i].AS_Status = TimE_AS_STATE_OK; 	

					Alivecounter[i] =0; 
					AlivecounterOld[i] =0;
				}
				else if((TimE_ASMinbound[i] > Alivecounter[i] ) || (TimE_ASMaxbound[i] < Alivecounter[i] ))
				{
					TimE_EachASStatus[i].AS_Status = TimE_AS_STATE_FAILED; 				  
					TimE_EachASStatus[i].AS_ErrorCP = i;
				}
				else;
			}
		}
	}
}

void TimE_SetLocalStatus(void)
{
	uint8 i;

	if(TimE_AL_Active ==TRUE)
	{
		uint8 as_failcnt=0;
		uint8 ds_failcnt=0;
		uint8 ls_failcnt=0;
		uint8 as_deactivatecnt =0; 

		for(i=0; i<TIME_AS_CHECKPOINT_NUM;i++)
		{
			if(TimE_EachASStatus[i].AS_Status ==TimE_AS_STATE_FAILED )
				as_failcnt++;
			else if(TimE_EachASStatus[i].AS_Status ==TimE_AS_STATE_DEACTIVATED )
				as_deactivatecnt++;
			else ; 
		}

		for(i=0; i<TIME_DS_CHECKPOINT_NUM; i++)
		{
			if(DS_return[i].CP_State == STATE_ERROR )
				ds_failcnt++; 
			else if(DS_return[i].CP_State == STATE_FAULT)
				ds_failcnt++;
			else;
		}

		for(i=0; i<TIME_LS_GRAPH_NUM ; i++)
		{
			if(LS_return[i].Graph_State==STATE_ERROR)	
				ls_failcnt++; 
			else if(LS_return[i].Graph_State==STATE_FAULT)
				ls_failcnt++;
			else;
		}

		if((as_failcnt ==0) && (as_deactivatecnt ==0))
			TimE_LocalStatus.TimE_asresult = TimE_LOCAL_STATUS_OK;	
		else if(as_deactivatecnt !=0 )
			TimE_LocalStatus.TimE_asresult = TimE_LOCAL_STATUS_DEACTIVATED;
		else 
			TimE_LocalStatus.TimE_asresult = TimE_LOCAL_STATUS_FAILED;

		if(ds_failcnt ==0)
			TimE_LocalStatus.TimE_dsresult = TimE_LOCAL_STATUS_OK;	
		else
			TimE_LocalStatus.TimE_dsresult = TimE_LOCAL_STATUS_FAILED;

		if(ls_failcnt == 0)
			TimE_LocalStatus.TimE_lsresult = TimE_LOCAL_STATUS_OK;
		else
			TimE_LocalStatus.TimE_lsresult = TimE_LOCAL_STATUS_FAILED;
	}

}

void TimE_GlobalSupervision(void)
{
	uint8 i;

	if(TimE_initialized == TRUE)
	{
		TimE_SetEachASStatus();

		TimE_SetLocalStatus();
		
		TimE_AL_Active = TRUE;

		TimE_GlobalSupervisionCycleCnt ++;

		if(TimE_AL_Active == TRUE)
		{

			if((TimE_LocalStatus.TimE_asresult == TimE_LOCAL_STATUS_OK)&&(TimE_LocalStatus.TimE_dsresult == TimE_LOCAL_STATUS_OK)\
				&&(TimE_LocalStatus.TimE_lsresult == TimE_LOCAL_STATUS_OK))
				TimE_GlobalStatus.GlobalStatus = TimE_GLOBAL_STATUS_OK; 
			else if(TimE_LocalStatus.TimE_asresult ==TimE_LOCAL_STATUS_DEACTIVATED)
				TimE_GlobalStatus.GlobalStatus = TimE_GLOBAL_STATUS_DEACTIVATED;
			else 
			{
				TimE_GlobalStatus.GlobalStatus = TimE_GLOBAL_STATUS_FAILED;
				TimE_GlobalStatus.ExpiredSupervisionCycleCnt ++;

				if(TimE_GlobalStatus.ExpiredSupervisionCycleCnt > TimE_EXPIREDSUPERVISIONCYCLETOL)
				{
					TimE_GlobalStatus.GlobalStatus = TimE_GLOBAL_STATUS_EXPIRED;
					TimE_initialized =FALSE;
					TimE_GlobalStatus.ExpiredSupervisionCycleCnt = 0;
				}
			}
		}
	}//if(TimE_initialized == TRUE)
}

TimE_Return_t SetAlivesupervisionCheckpoint0(uint8 ASCPId)
{
	if(TimE_AL_Active==TRUE) 
	{
		Alivecounter[ASCPId]++;

		if((Alivecounter[ASCPId] - AlivecounterOld[ASCPId])==1)
		{
			AS_return[ASCPId].CP_State =STATE_OK;
			AlivecounterOld[ASCPId] = Alivecounter[ASCPId];
		}
	}	
	else
		AS_return[ASCPId].CP_State = STATE_DEACTIVATED; 

	return AS_return[ASCPId];	
}
#endif //(ALIVESUPERVISION_INDICATION == STD_ON)
