/**
 * @defgroup 
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        TimE_Cfg.c
 * @brief       Template file
 * @date        2015. .
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "TimE_Cfg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/


/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
/** Variable Section (UNSPECIFIED)**/

/* checkpoint configuration for alive supervision*/
#if (ALIVESUPERVISION_PERIOD_MEASURE == STD_ON)
TimEAScheckpointCfg_t TimEAScheckpointCfg[TIME_AS_CHECKPOINT_NUM] =	
{	
	/* AS Checkpoint*/	
	{/*[0]  TIME_AS_CHECKPOINT_FOR_50MICROS*/
		10, 		/*Toler_alivesupervision*/
		50000, 		/*Expected_period[ns]*/
		0.1f,			/*Margin_level*/
	},
	{/*[1]  TIME_AS_CHECKPOINT_FOR_1MS*/
		10,
		1000000,
		0.1f,
	},
	{/*[2]	TIME_AS_CHECKPOINT_FOR_5MS*/
		10,
		5000000,
		0.1f
	},
	{/*[3]	TIME_AS_CHECKPOINT_FOR_10MS*/
		10,
		10000000,
		0.1f
	}	
};
#endif 
/* checkpoint configuration for deadline supervision*/
TimEDScheckpointCfg_t TimEDScheckpointCfg[TIME_DS_CHECKPOINT_NUM] =
{
	/*DS Checkpoint -2��*/ 
	{/*[0]TIME_DS_CHECKPOINT_FOR_PROXY_TXBYCOM_START*/
		TRUE,		/*StartCp*/
		FALSE,		/*EndCp*/
		0,			/*DeadlineMin_NS*/
		0,			/*DeadlineMax_NS*/
		0
	},
	{/*[1]TIME_DS_CHECKPOINT_FOR_PROXY_TXBYCOM_END*/
		FALSE,		/*StartCp*/
		TRUE,		/*EndCp*/
		30000,		/*DeadlineMin_NS*/		
		//60000,
		80000,		/*DeadlineMax_NS*/
		10
	}
};

/* graph configuration for logical supervision*/
TimELSGraphlistCfg_t TimELSGraphlistCfg[TIME_LS_GRAPH_NUM] = 
{
	{//[0]Testcase#3
		{
			{1,	2},
			{2,	3},
			{3,	4},
			{0,	0},
			{0,	0},
			{0,	0},
			{0,	0},
			{0,	0},
			{0,	0},
			{0,	0},
			{0,	0},
			{0,	0},
			{0,	0},
			{0,	0},
			{0,	0},
			{0,	0},
			{0,	0},
			{0,	0},
			{0,	0},
			{0,	0},
		},
		10,
		3,
	},
	{//[1]Testcase#4
		{
			{1, 2},
			{2, 3},
			{3, 4},
			{3, 5},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
		},
		10,
		4,	
	},
	{//[2]Testcase#5
		{
			{1, 2},
			{2, 3},
			{2, 6},
			{3, 4},
			{6, 4},
			{4, 5},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
		},
		10,
		6,
	},
	{//[3]Testcase#6
		{
			{1, 2},
			{2, 3},
			{2, 4},
			{3, 4},
			{4, 5},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
		},
		10,
		5,
	},
	{//[4]Testcase#7
		{
			{1, 2},
			{2, 3},
			{3, 3},
			{3, 4},
			{4, 5},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
			{0, 0},
		},
		10,
		5,
	}
};

#if (ALIVESUPERVISION_INDICATION == STD_ON)
TimEAScheckpointCfg0_t TimEAScheckpointCfg0[TIME_AS_CHECKPOINT_NUM] =	
{
	/* AS Checkpoint*/	
	{/*[0]  TIME_AS_CHECKPOINT_FOR_50MICROS*/
		100, 		/*ExpectedIndication*/
		//50,
		2, 			/*MaxMargin*/
		0,			/*MinMargin*/
		1,			/*SupervisionRefCycle*/
		10,			/*Tolerance_AS*/
	},	
	{/*[1]  TIME_AS_CHECKPOINT_FOR_1MS*/
		5, 		/*ExpectedIndication*/
		2, 			/*MaxMargin*/
		0,			/*MinMargin*/
		1,			/*SupervisionRefCycle*/
		//2,
		10,			/*Tolerance_AS*/
	},
	{/*[2]	TIME_AS_CHECKPOINT_FOR_5MS*/
		1, 			/*ExpectedIndication*/
	//	5,
		2, 			/*MaxMargin*/
		0,			/*MinMargin*/
		1,			/*SupervisionRefCycle*/
		10,			/*Tolerance_AS*/
	},	
	{/*[3]	TIME_AS_CHECKPOINT_FOR_10MS*/
		1, 			/*ExpectedIndication*/
		2, 			/*MaxMargin*/
		0,			/*MinMargin*/
	//	1,			/*SupervisionRefCycle*/
		2,
		10,			/*Tolerance_AS*/
	}
};
#endif 

/*return value*/
TimE_Return_t ASresult[TIME_AS_CHECKPOINT_NUM];
TimE_Return_t DSresult[TIME_DS_CHECKPOINT_NUM];
TimE_Return_t LSresult[TIME_LS_GRAPH_NUM];
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/


/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/


/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
