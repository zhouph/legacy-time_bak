/**
 * @defgroup 
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        TimE_Cfg.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/
#ifndef TIME_CFG_H_
#define TIME_CFG_H_
/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "TimE_Types.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
#define TIME_DEBUG_MODE					STD_ON
 
#define ALIVESUPERVISION_PERIOD_MEASURE STD_OFF
#define ALIVESUPERVISION_INDICATION		STD_ON


/* number of checkpoint */
#define TIME_AS_CHECKPOINT_NUM 	4
#define TIME_DS_CHECKPOINT_NUM	2
#define TIME_LS_GRAPH_NUM	5

/* Alive Supervision Check point id */

#define TIME_AS_CHECKPOINT_FOR_50MICROS	0
#define TIME_AS_CHECKPOINT_FOR_1MS		1
#define TIME_AS_CHECKPOINT_FOR_5MS		2
#define TIME_AS_CHECKPOINT_FOR_10MS		3

#if 0
//indication
#define TIME_AS_CHECKPOINT_FOR_5MS		0
#define TIME_AS_CHECKPOINT_FOR_10MS		1
#endif 

/*Deadline Supervision Check point id*/
#define TIME_DS_CHECKPOINT_FOR_PROXY_TXBYCOM_START	0
#define TIME_DS_CHECKPOINT_FOR_PROXY_TXBYCOM_END	1

/*Logical Supervision Graph id*/
#define TIME_LS_GRAPH0	0
#define TIME_LS_GRAPH1	1
#define TIME_LS_GRAPH2	2
#define TIME_LS_GRAPH3	3
#define TIME_LS_GRAPH4	4

/*Logical Supervision Checkpoint Type*/
#define LS_FIRST_CHECKPOINT		0x00
#define LS_LAST_CHECKPOINT		0xFF
#define LS_RELATION_CHECKPOINT	0x11

/*Logical Supervision Check point id*/
#define TIME_LS_CHECKPOINT_FOR_GRAPH0_CP1		1
#define TIME_LS_CHECKPOINT_FOR_GRAPH0_CP2		2
#define TIME_LS_CHECKPOINT_FOR_GRAPH0_CP3		3
#define TIME_LS_CHECKPOINT_FOR_GRAPH0_CP4		4

#define TIME_LS_CHECKPOINT_FOR_GRAPH1_CP1		1
#define TIME_LS_CHECKPOINT_FOR_GRAPH1_CP2		2
#define TIME_LS_CHECKPOINT_FOR_GRAPH1_CP3		3
#define TIME_LS_CHECKPOINT_FOR_GRAPH1_CP4		4
#define TIME_LS_CHECKPOINT_FOR_GRAPH1_CP5		5

#define TIME_LS_CHECKPOINT_FOR_GRAPH2_CP1		1
#define TIME_LS_CHECKPOINT_FOR_GRAPH2_CP2		2
#define TIME_LS_CHECKPOINT_FOR_GRAPH2_CP3		3
#define TIME_LS_CHECKPOINT_FOR_GRAPH2_CP4		4
#define TIME_LS_CHECKPOINT_FOR_GRAPH2_CP5		5
#define TIME_LS_CHECKPOINT_FOR_GRAPH2_CP6		6

#define TIME_LS_CHECKPOINT_FOR_GRAPH3_CP1		1
#define TIME_LS_CHECKPOINT_FOR_GRAPH3_CP2		2
#define TIME_LS_CHECKPOINT_FOR_GRAPH3_CP3		3
#define TIME_LS_CHECKPOINT_FOR_GRAPH3_CP4		4
#define TIME_LS_CHECKPOINT_FOR_GRAPH3_CP5		5

#define TIME_LS_CHECKPOINT_FOR_GRAPH4_CP1		1
#define TIME_LS_CHECKPOINT_FOR_GRAPH4_CP2		2
#define TIME_LS_CHECKPOINT_FOR_GRAPH4_CP3		3
#define TIME_LS_CHECKPOINT_FOR_GRAPH4_CP4		4
#define TIME_LS_CHECKPOINT_FOR_GRAPH4_CP5		5

/*timer using supervision*/
#define	BASICTIMETICK 		(STM0_TIM0.U)
/*conversion value tick to ns*/
#define TICKTONS			10

/*Return Value*/
#define STATE_OK			0
#define STATE_FAULT			1
#define STATE_ERROR 		2
#define STATE_CFG_FAULT		3
#define STATE_DEACTIVATED	4

#define STATE_LSGRAPH_OK	5

 //indication
#define TimE_AS_STATE_OK				0
#define TimE_AS_STATE_FAILED			1 
#define TimE_AS_STATE_DEACTIVATED		4 

#define TimE_LOCAL_STATUS_OK			0
#define TimE_LOCAL_STATUS_FAILED 		1
#define TimE_LOCAL_STATUS_EXPIRED		2
#define TimE_LOCAL_STATUS_DEACTIVATED	4

#define TimE_GLOBAL_STATUS_OK			0
#define TimE_GLOBAL_STATUS_FAILED 		1
#define TimE_GLOBAL_STATUS_EXPIRED		2
#define TimE_GLOBAL_STATUS_DEACTIVATED	4

#define TimE_EXPIREDSUPERVISIONCYCLETOL 1
#define MAX_SUPERVISION_CYCLE_COUNTER	2
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
extern TimEAScheckpointCfg_t 	TimEAScheckpointCfg[TIME_AS_CHECKPOINT_NUM];
extern TimEDScheckpointCfg_t 	TimEDScheckpointCfg[TIME_DS_CHECKPOINT_NUM];
extern TimELSGraphlistCfg_t		TimELSGraphlistCfg[TIME_LS_GRAPH_NUM];

//indication 
extern TimEAScheckpointCfg0_t	TimEAScheckpointCfg0[TIME_AS_CHECKPOINT_NUM];
//extern uint8 TimE_GlobalSupervisionCycleCnt;
extern uint8 CPRefCycleCnt[TIME_AS_CHECKPOINT_NUM]; 
extern uint8 TimE_ASMaxbound[TIME_AS_CHECKPOINT_NUM];
extern uint8 TimE_ASMinbound[TIME_AS_CHECKPOINT_NUM];
//extern uint8 Alivecounter[TIME_AS_CHECKPOINT_NUM], AlivecounterOld[TIME_AS_CHECKPOINT_NUM];

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/
extern TimE_Return_t LSresult[TIME_LS_GRAPH_NUM];
extern TimE_Return_t ASresult[TIME_AS_CHECKPOINT_NUM];
extern TimE_Return_t DSresult[TIME_DS_CHECKPOINT_NUM];
/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
	
/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* TIME_CFG_H_ */
/** @} */
