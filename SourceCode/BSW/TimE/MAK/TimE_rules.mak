# \file
#
# \brief TimE
#
# This file contains the implementation of the SWC
# module TimE.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += TimE_src

TimE_src_FILES        += $(TimE_SRC_PATH)\TimE.c
TimE_src_FILES        += $(TimE_CFG_PATH)\TimE_Cfg.c

