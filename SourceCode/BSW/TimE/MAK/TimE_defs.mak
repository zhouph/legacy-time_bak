# \file
#
# \brief TimE
#
# This file contains the implementation of the SWC
# module TimE.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

TimE_CORE_PATH     := $(MANDO_BSW_ROOT)\TimE
TimE_SRC_PATH      := $(TimE_CORE_PATH)\SRC
TimE_HDR_PATH      := $(TimE_CORE_PATH)\HDR
TimE_CFG_PATH      := $(TimE_CORE_PATH)\CFG\$(TimE_VARIANT)
TimE_CAL_PATH      := $(TimE_CORE_PATH)\CAL\$(TimE_VARIANT)
TimE_IFA_PATH      := $(TimE_CORE_PATH)\IFA



CC_INCLUDE_PATH    += $(TimE_SRC_PATH)
CC_INCLUDE_PATH    += $(TimE_CFG_PATH)
CC_INCLUDE_PATH    += $(TimE_HDR_PATH)
CC_INCLUDE_PATH    += $(TimE_IFA_PATH)
CC_INCLUDE_PATH    += $(TimE_CMN_PATH)
CC_INCLUDE_PATH    += $(TimE_CAL_PATH)

