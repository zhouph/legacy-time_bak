/**
 * @defgroup IoHwAb_Types IoHwAb_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        IoHwAb_Types.h
 * @brief       Template file
 * @date        2014. 11. 17.
 ******************************************************************************/

#ifndef IOHWAB_TYPES_H_
#define IOHWAB_TYPES_H_

#ifndef STRINGIFY
#define STRINGIFY(x) #x
#endif /* STRINGIFY */
#define IOHWAB_PATH_BSWINCLUDE( _FILENAME_ ) STRINGIFY(../../include/_FILENAME_)
/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Io Hw Voltage Array Length, Must be same with ADCIF_MAX_BUF_LEN */
#define IOHWAB_ARRAYLEN                30
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
 /* IO HW Port ID type */
 typedef uint16 IoHwAb_PortType;

 /* Requesting buffer type */
 typedef uint16 IoHwAb_DataType;

 /* Requesting buffer length type */
 typedef uint8 IoHwAb_BufLenType;

 /* IO channel type */
 typedef uint16 IoHwAb_IoChannelType;

 /* Conversion multiply factor type */
 typedef uint32 IoHwAb_MulFactorType;
 
 /* Conversion divide factor type */
 typedef uint32 IoHwAb_DivFactorType;

 /* Digital Input Level type */
 typedef uint8  IoHwAb_LevelType;

 /* Edge Index Type */
 typedef uint16  IoHwAb_EdgeIdxType;

  /* Pulse Index Type */
 typedef uint32  IoHwAb_TimeStampArrayType;

 /* IO HW Module ID type */
 typedef enum
 {
    ModuleID_Dio = 0,
    ModuleID_Spi,
    ModuleID_Pwm,
    ModuleID_Adc,
    ModuleID_IcuCdd
 }IoHwAb_ModuleIDType;

 typedef enum
 {
    DataIfType_Single = 0,
    DataIfType_Array
 }IoHwAb_DataIfType;
 
 /* Routing configuration table type */
 typedef struct
 {
    IoHwAb_ModuleIDType   ModuleID;
    IoHwAb_IoChannelType  IoChannel;
    IoHwAb_DataIfType     DataIfType;
    IoHwAb_MulFactorType  MulFactor;
    IoHwAb_DivFactorType  DivFactor;
 }IoHwAb_RoutingType;

 
 /* IO HW Status data structure */
 typedef uint32 IoHwAb_StatusType;

/* Data Array type */
 typedef struct{
  uint16   Buf[IOHWAB_ARRAYLEN];
  uint8    Len;
}IoHwAb_VoltArrayType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* IOHWAB_TYPES_H_ */
/** @} */
