# \file
#
# \brief Aka
#
# This file contains the implementation of the SWC
# module Aka.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Aka_src

Aka_src_FILES        += $(Aka_SRC_PATH)\Aka_Main.c
Aka_src_FILES        += $(Aka_SRC_PATH)\Aka_Process.c
Aka_src_FILES        += $(Aka_IFA_PATH)\Aka_Main_Ifa.c
Aka_src_FILES        += $(Aka_CFG_PATH)\Aka_Cfg.c
Aka_src_FILES        += $(Aka_CAL_PATH)\Aka_Cal.c

ifeq ($(ICE_COMPILE),true)
Aka_src_FILES        += $(Aka_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	Aka_src_FILES        += $(Aka_UNITY_PATH)\unity.c
	Aka_src_FILES        += $(Aka_UNITY_PATH)\unity_fixture.c	
	Aka_src_FILES        += $(Aka_UT_PATH)\main.c
	Aka_src_FILES        += $(Aka_UT_PATH)\Aka_Main\Aka_Main_UtMain.c
endif