# \file
#
# \brief Aka
#
# This file contains the implementation of the SWC
# module Aka.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Aka_CORE_PATH     := $(MANDO_BSW_ROOT)\Aka
Aka_CAL_PATH      := $(Aka_CORE_PATH)\CAL\$(Aka_VARIANT)
Aka_SRC_PATH      := $(Aka_CORE_PATH)\SRC
Aka_CFG_PATH      := $(Aka_CORE_PATH)\CFG\$(Aka_VARIANT)
Aka_HDR_PATH      := $(Aka_CORE_PATH)\HDR
Aka_IFA_PATH      := $(Aka_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Aka_CMN_PATH      := $(Aka_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	Aka_UT_PATH		:= $(Aka_CORE_PATH)\UT
	Aka_UNITY_PATH	:= $(Aka_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(Aka_UT_PATH)
	CC_INCLUDE_PATH		+= $(Aka_UNITY_PATH)
	Aka_Main_PATH 	:= Aka_UT_PATH\Aka_Main
endif
CC_INCLUDE_PATH    += $(Aka_CAL_PATH)
CC_INCLUDE_PATH    += $(Aka_SRC_PATH)
CC_INCLUDE_PATH    += $(Aka_CFG_PATH)
CC_INCLUDE_PATH    += $(Aka_HDR_PATH)
CC_INCLUDE_PATH    += $(Aka_IFA_PATH)
CC_INCLUDE_PATH    += $(Aka_CMN_PATH)

