#include "unity.h"
#include "unity_fixture.h"
#include "Aka_Main.h"
#include "Aka_Main_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Acm_MainAcmAsicInitCompleteFlag_t UtInput_Aka_MainAcmAsicInitCompleteFlag[MAX_STEP] = AKA_MAINACMASICINITCOMPLETEFLAG;
const Prly_HndlrPrlyEcuInhibit_t UtInput_Aka_MainPrlyEcuInhibit[MAX_STEP] = AKA_MAINPRLYECUINHIBIT;

const Haluint8 UtExpected_Aka_MainAkaKeepAliveWriteData_KeepAliveDataFlg[MAX_STEP] = AKA_MAINAKAKEEPALIVEWRITEDATA_KEEPALIVEDATAFLG;
const Haluint8 UtExpected_Aka_MainAkaKeepAliveWriteData_ACH_Tx1_PHOLD[MAX_STEP] = AKA_MAINAKAKEEPALIVEWRITEDATA_ACH_TX1_PHOLD;
const Haluint8 UtExpected_Aka_MainAkaKeepAliveWriteData_ACH_Tx1_KA[MAX_STEP] = AKA_MAINAKAKEEPALIVEWRITEDATA_ACH_TX1_KA;



TEST_GROUP(Aka_Main);
TEST_SETUP(Aka_Main)
{
    Aka_Main_Init();
}

TEST_TEAR_DOWN(Aka_Main)
{   /* Postcondition */

}

TEST(Aka_Main, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Aka_MainAcmAsicInitCompleteFlag = UtInput_Aka_MainAcmAsicInitCompleteFlag[i];
        Aka_MainPrlyEcuInhibit = UtInput_Aka_MainPrlyEcuInhibit[i];

        Aka_Main();

        TEST_ASSERT_EQUAL(Aka_MainAkaKeepAliveWriteData.KeepAliveDataFlg, UtExpected_Aka_MainAkaKeepAliveWriteData_KeepAliveDataFlg[i]);
        TEST_ASSERT_EQUAL(Aka_MainAkaKeepAliveWriteData.ACH_Tx1_PHOLD, UtExpected_Aka_MainAkaKeepAliveWriteData_ACH_Tx1_PHOLD[i]);
        TEST_ASSERT_EQUAL(Aka_MainAkaKeepAliveWriteData.ACH_Tx1_KA, UtExpected_Aka_MainAkaKeepAliveWriteData_ACH_Tx1_KA[i]);
    }
}

TEST_GROUP_RUNNER(Aka_Main)
{
    RUN_TEST_CASE(Aka_Main, All);
}
