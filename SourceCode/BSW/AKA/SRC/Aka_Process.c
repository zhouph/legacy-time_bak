/**
 * @defgroup Aka_Main Aka_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Aka_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Aka_Main.h"
#include "Aka_Main_Ifa.h"
#include "Aka_Process.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define AKA_MAIN_START_SEC_CONST_UNSPECIFIED
#include "Aka_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define AKA_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "Aka_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define AKA_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Aka_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define AKA_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Aka_MemMap.h"
#define AKA_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "Aka_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define AKA_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "Aka_MemMap.h"
#define AKA_MAIN_START_SEC_VAR_UNSPECIFIED
#include "Aka_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define AKA_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "Aka_MemMap.h"
#define AKA_MAIN_START_SEC_VAR_32BIT
#include "Aka_MemMap.h"
/** Variable Section (32BIT)**/


#define AKA_MAIN_STOP_SEC_VAR_32BIT
#include "Aka_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define AKA_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Aka_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define AKA_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Aka_MemMap.h"
#define AKA_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "Aka_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define AKA_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "Aka_MemMap.h"
#define AKA_MAIN_START_SEC_VAR_UNSPECIFIED
#include "Aka_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define AKA_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "Aka_MemMap.h"
#define AKA_MAIN_START_SEC_VAR_32BIT
#include "Aka_MemMap.h"
/** Variable Section (32BIT)**/


#define AKA_MAIN_STOP_SEC_VAR_32BIT
#include "Aka_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define AKA_MAIN_START_SEC_CODE
#include "Aka_MemMap.h"

static void Aka_SetPowerHold(uint8 Level);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

void Aka_Process(void)
{
    Aka_MainBus.Aka_MainAkaKeepAliveWriteData.KeepAliveDataFlg = 0;      /* Clear Set Flag */
    
    if(Aka_MainBus.Aka_MainAcmAsicInitCompleteFlag == 1)
    {
        /* Get ECU Shutdown command from RTE or EcuM */
        /*Aka_KeepAlive.ACH_Tx1_PHOLD = 0;*/
        Aka_MainBus.Aka_MainAkaKeepAliveWriteData.KeepAliveDataFlg = 1;
    }
}



/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

static void Aka_SetPowerHold(uint8 Level)
{
    if(Level == 0)
    {
        Aka_MainBus.Aka_MainAkaKeepAliveWriteData.ACH_Tx1_PHOLD = 0;
    }
    else
    {
        Aka_MainBus.Aka_MainAkaKeepAliveWriteData.ACH_Tx1_PHOLD = 1;
    }
}

#define AKA_MAIN_STOP_SEC_CODE
#include "Aka_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
