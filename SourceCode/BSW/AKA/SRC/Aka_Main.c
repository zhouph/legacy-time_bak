/**
 * @defgroup Aka_Main Aka_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Aka_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Aka_Main.h"
#include "Aka_Main_Ifa.h"
#include "IfxStm_reg.h"
#include "Aka_Process.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define AKA_MAIN_START_SEC_CONST_UNSPECIFIED
#include "Aka_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define AKA_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "Aka_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define AKA_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Aka_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Aka_Main_HdrBusType Aka_MainBus;

/* Version Info */
const SwcVersionInfo_t Aka_MainVersionInfo = 
{   
    AKA_MAIN_MODULE_ID,           /* Aka_MainVersionInfo.ModuleId */
    AKA_MAIN_MAJOR_VERSION,       /* Aka_MainVersionInfo.MajorVer */
    AKA_MAIN_MINOR_VERSION,       /* Aka_MainVersionInfo.MinorVer */
    AKA_MAIN_PATCH_VERSION,       /* Aka_MainVersionInfo.PatchVer */
    AKA_MAIN_BRANCH_VERSION       /* Aka_MainVersionInfo.BranchVer */
};
    
/* Input Data Element */
Acm_MainAcmAsicInitCompleteFlag_t Aka_MainAcmAsicInitCompleteFlag;
Prly_HndlrPrlyEcuInhibit_t Aka_MainPrlyEcuInhibit;

/* Output Data Element */
Aka_MainAkaKeepAliveWriteData_t Aka_MainAkaKeepAliveWriteData;

uint32 Aka_Main_Timer_Start;
uint32 Aka_Main_Timer_Elapsed;

#define AKA_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Aka_MemMap.h"
#define AKA_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "Aka_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define AKA_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "Aka_MemMap.h"
#define AKA_MAIN_START_SEC_VAR_UNSPECIFIED
#include "Aka_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define AKA_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "Aka_MemMap.h"
#define AKA_MAIN_START_SEC_VAR_32BIT
#include "Aka_MemMap.h"
/** Variable Section (32BIT)**/


#define AKA_MAIN_STOP_SEC_VAR_32BIT
#include "Aka_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define AKA_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Aka_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define AKA_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Aka_MemMap.h"
#define AKA_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "Aka_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define AKA_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "Aka_MemMap.h"
#define AKA_MAIN_START_SEC_VAR_UNSPECIFIED
#include "Aka_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define AKA_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "Aka_MemMap.h"
#define AKA_MAIN_START_SEC_VAR_32BIT
#include "Aka_MemMap.h"
/** Variable Section (32BIT)**/


#define AKA_MAIN_STOP_SEC_VAR_32BIT
#include "Aka_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define AKA_MAIN_START_SEC_CODE
#include "Aka_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Aka_Main_Init(void)
{
    /* Initialize internal bus */
    Aka_MainBus.Aka_MainAcmAsicInitCompleteFlag = 0;
    Aka_MainBus.Aka_MainPrlyEcuInhibit = 0;
    Aka_MainBus.Aka_MainAkaKeepAliveWriteData.KeepAliveDataFlg = 0;
    Aka_MainBus.Aka_MainAkaKeepAliveWriteData.ACH_Tx1_PHOLD = 0;
    Aka_MainBus.Aka_MainAkaKeepAliveWriteData.ACH_Tx1_KA = 0;
}

void Aka_Main(void)
{
    Aka_Main_Timer_Start = STM0_TIM0.U;

    /* Input */
    Aka_Main_Read_Aka_MainAcmAsicInitCompleteFlag(&Aka_MainBus.Aka_MainAcmAsicInitCompleteFlag);
    Aka_Main_Read_Aka_MainPrlyEcuInhibit(&Aka_MainBus.Aka_MainPrlyEcuInhibit);

    /* Process */
    Aka_Process();

    /* Output */
    Aka_Main_Write_Aka_MainAkaKeepAliveWriteData(&Aka_MainBus.Aka_MainAkaKeepAliveWriteData);
    /*==============================================================================
    * Members of structure Aka_MainAkaKeepAliveWriteData 
     : Aka_MainAkaKeepAliveWriteData.KeepAliveDataFlg;
     : Aka_MainAkaKeepAliveWriteData.ACH_Tx1_PHOLD;
     : Aka_MainAkaKeepAliveWriteData.ACH_Tx1_KA;
     =============================================================================*/
    

    Aka_Main_Timer_Elapsed = STM0_TIM0.U - Aka_Main_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define AKA_MAIN_STOP_SEC_CODE
#include "Aka_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
