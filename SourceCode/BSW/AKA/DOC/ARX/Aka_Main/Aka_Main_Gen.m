Aka_MainAcmAsicInitCompleteFlag = Simulink.Bus;
DeList={'Aka_MainAcmAsicInitCompleteFlag'};
Aka_MainAcmAsicInitCompleteFlag = CreateBus(Aka_MainAcmAsicInitCompleteFlag, DeList);
clear DeList;

Aka_MainPrlyEcuInhibit = Simulink.Bus;
DeList={'Aka_MainPrlyEcuInhibit'};
Aka_MainPrlyEcuInhibit = CreateBus(Aka_MainPrlyEcuInhibit, DeList);
clear DeList;

Aka_MainAkaKeepAliveWriteData = Simulink.Bus;
DeList={
    'KeepAliveDataFlg'
    'ACH_Tx1_PHOLD'
    'ACH_Tx1_KA'
    };
Aka_MainAkaKeepAliveWriteData = CreateBus(Aka_MainAkaKeepAliveWriteData, DeList);
clear DeList;

