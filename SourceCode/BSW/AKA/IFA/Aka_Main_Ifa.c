/**
 * @defgroup Aka_Main_Ifa Aka_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Aka_Main_Ifa.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Aka_Main_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define AKA_MAIN_START_SEC_CONST_UNSPECIFIED
#include "Aka_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define AKA_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "Aka_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define AKA_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Aka_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define AKA_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Aka_MemMap.h"
#define AKA_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "Aka_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define AKA_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "Aka_MemMap.h"
#define AKA_MAIN_START_SEC_VAR_UNSPECIFIED
#include "Aka_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define AKA_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "Aka_MemMap.h"
#define AKA_MAIN_START_SEC_VAR_32BIT
#include "Aka_MemMap.h"
/** Variable Section (32BIT)**/


#define AKA_MAIN_STOP_SEC_VAR_32BIT
#include "Aka_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define AKA_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Aka_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define AKA_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Aka_MemMap.h"
#define AKA_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "Aka_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define AKA_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "Aka_MemMap.h"
#define AKA_MAIN_START_SEC_VAR_UNSPECIFIED
#include "Aka_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define AKA_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "Aka_MemMap.h"
#define AKA_MAIN_START_SEC_VAR_32BIT
#include "Aka_MemMap.h"
/** Variable Section (32BIT)**/


#define AKA_MAIN_STOP_SEC_VAR_32BIT
#include "Aka_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define AKA_MAIN_START_SEC_CODE
#include "Aka_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define AKA_MAIN_STOP_SEC_CODE
#include "Aka_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
