/**
 * @defgroup Aka_Main_Ifa Aka_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Aka_Main_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef AKA_MAIN_IFA_H_
#define AKA_MAIN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Aka_Main_Read_Aka_MainAcmAsicInitCompleteFlag(data) do \
{ \
    *data = Aka_MainAcmAsicInitCompleteFlag; \
}while(0);

#define Aka_Main_Read_Aka_MainPrlyEcuInhibit(data) do \
{ \
    *data = Aka_MainPrlyEcuInhibit; \
}while(0);


/* Set Output DE MAcro Function */
#define Aka_Main_Write_Aka_MainAkaKeepAliveWriteData(data) do \
{ \
    Aka_MainAkaKeepAliveWriteData = *data; \
}while(0);

#define Aka_Main_Write_Aka_MainAkaKeepAliveWriteData_KeepAliveDataFlg(data) do \
{ \
    Aka_MainAkaKeepAliveWriteData.KeepAliveDataFlg = *data; \
}while(0);

#define Aka_Main_Write_Aka_MainAkaKeepAliveWriteData_ACH_Tx1_PHOLD(data) do \
{ \
    Aka_MainAkaKeepAliveWriteData.ACH_Tx1_PHOLD = *data; \
}while(0);

#define Aka_Main_Write_Aka_MainAkaKeepAliveWriteData_ACH_Tx1_KA(data) do \
{ \
    Aka_MainAkaKeepAliveWriteData.ACH_Tx1_KA = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* AKA_MAIN_IFA_H_ */
/** @} */
