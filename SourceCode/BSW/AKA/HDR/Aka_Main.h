/**
 * @defgroup Aka_Main Aka_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Aka_Main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef AKA_MAIN_H_
#define AKA_MAIN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Aka_Types.h"
#include "Aka_Cfg.h"
#include "Aka_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define AKA_MAIN_MODULE_ID      (0)
 #define AKA_MAIN_MAJOR_VERSION  (2)
 #define AKA_MAIN_MINOR_VERSION  (0)
 #define AKA_MAIN_PATCH_VERSION  (0)
 #define AKA_MAIN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Aka_Main_HdrBusType Aka_MainBus;

/* Version Info */
extern const SwcVersionInfo_t Aka_MainVersionInfo;

/* Input Data Element */
extern Acm_MainAcmAsicInitCompleteFlag_t Aka_MainAcmAsicInitCompleteFlag;
extern Prly_HndlrPrlyEcuInhibit_t Aka_MainPrlyEcuInhibit;

/* Output Data Element */
extern Aka_MainAkaKeepAliveWriteData_t Aka_MainAkaKeepAliveWriteData;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Aka_Main_Init(void);
extern void Aka_Main(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* AKA_MAIN_H_ */
/** @} */
