#define S_FUNCTION_NAME      WssM_Main_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          56
#define WidthOutputPort         21

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "WssM_Main.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long = input[0];
    WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short = input[1];
    WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add = input[2];
    WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt = input[3];
    WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc = input[4];
    WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NO_FAULT = input[5];
    WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WHEEL_SPEED_DECODER_DATA = input[6];
    WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STANDSTILL = input[7];
    WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_LATCH_D0 = input[8];
    WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NODATA = input[9];
    WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_INVALID = input[10];
    WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_OPEN = input[11];
    WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STB = input[12];
    WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STG = input[13];
    WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WSI_OT = input[14];
    WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_CURRENT_HI = input[15];
    WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NO_FAULT = input[16];
    WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WHEEL_SPEED_DECODER_DATA = input[17];
    WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STANDSTILL = input[18];
    WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_LATCH_D0 = input[19];
    WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NODATA = input[20];
    WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_INVALID = input[21];
    WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_OPEN = input[22];
    WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STB = input[23];
    WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STG = input[24];
    WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WSI_OT = input[25];
    WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_CURRENT_HI = input[26];
    WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NO_FAULT = input[27];
    WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WHEEL_SPEED_DECODER_DATA = input[28];
    WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STANDSTILL = input[29];
    WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_LATCH_D0 = input[30];
    WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NODATA = input[31];
    WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_INVALID = input[32];
    WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_OPEN = input[33];
    WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STB = input[34];
    WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STG = input[35];
    WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WSI_OT = input[36];
    WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_CURRENT_HI = input[37];
    WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NO_FAULT = input[38];
    WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WHEEL_SPEED_DECODER_DATA = input[39];
    WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STANDSTILL = input[40];
    WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_LATCH_D0 = input[41];
    WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NODATA = input[42];
    WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_INVALID = input[43];
    WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_OPEN = input[44];
    WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STB = input[45];
    WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STG = input[46];
    WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WSI_OT = input[47];
    WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_CURRENT_HI = input[48];
    WssM_MainWssSpeedOut.WssMin = input[49];
    WssM_MainAchAsicInvalid = input[50];
    WssM_MainEcuModeSts = input[51];
    WssM_MainIgnOnOffSts = input[52];
    WssM_MainIgnEdgeSts = input[53];
    WssM_MainVBatt1Mon = input[54];
    WssM_MainDiagClrSrs = input[55];

    WssM_Main();


    output[0] = WssM_MainWssMonData.WssM_FlOpen_Err;
    output[1] = WssM_MainWssMonData.WssM_FlShort_Err;
    output[2] = WssM_MainWssMonData.WssM_FlOverTemp_Err;
    output[3] = WssM_MainWssMonData.WssM_FlLeakage_Err;
    output[4] = WssM_MainWssMonData.WssM_FrOpen_Err;
    output[5] = WssM_MainWssMonData.WssM_FrShort_Err;
    output[6] = WssM_MainWssMonData.WssM_FrOverTemp_Err;
    output[7] = WssM_MainWssMonData.WssM_FrLeakage_Err;
    output[8] = WssM_MainWssMonData.WssM_RlOpen_Err;
    output[9] = WssM_MainWssMonData.WssM_RlShort_Err;
    output[10] = WssM_MainWssMonData.WssM_RlOverTemp_Err;
    output[11] = WssM_MainWssMonData.WssM_RlLeakage_Err;
    output[12] = WssM_MainWssMonData.WssM_RrOpen_Err;
    output[13] = WssM_MainWssMonData.WssM_RrShort_Err;
    output[14] = WssM_MainWssMonData.WssM_RrOverTemp_Err;
    output[15] = WssM_MainWssMonData.WssM_RrLeakage_Err;
    output[16] = WssM_MainWssMonData.WssM_Asic_Comm_Err;
    output[17] = WssM_MainWssMonData.WssM_Inhibit_FL;
    output[18] = WssM_MainWssMonData.WssM_Inhibit_FR;
    output[19] = WssM_MainWssMonData.WssM_Inhibit_RL;
    output[20] = WssM_MainWssMonData.WssM_Inhibit_RR;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    WssM_Main_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
