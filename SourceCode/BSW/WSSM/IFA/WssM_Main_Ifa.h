/**
 * @defgroup WssM_Main_Ifa WssM_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        WssM_Main_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef WSSM_MAIN_IFA_H_
#define WSSM_MAIN_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define WssM_Main_Read_WssM_MainAchSysPwrAsicInfo(data) do \
{ \
    *data = WssM_MainAchSysPwrAsicInfo; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort0AsicInfo(data) do \
{ \
    *data = WssM_MainAchWssPort0AsicInfo; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort1AsicInfo(data) do \
{ \
    *data = WssM_MainAchWssPort1AsicInfo; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort2AsicInfo(data) do \
{ \
    *data = WssM_MainAchWssPort2AsicInfo; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort3AsicInfo(data) do \
{ \
    *data = WssM_MainAchWssPort3AsicInfo; \
}while(0);

#define WssM_Main_Read_WssM_MainWssSpeedOut(data) do \
{ \
    *data = WssM_MainWssSpeedOut; \
}while(0);

#define WssM_Main_Read_WssM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_long(data) do \
{ \
    *data = WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long; \
}while(0);

#define WssM_Main_Read_WssM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_short(data) do \
{ \
    *data = WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short; \
}while(0);

#define WssM_Main_Read_WssM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_add(data) do \
{ \
    *data = WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add; \
}while(0);

#define WssM_Main_Read_WssM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_fcnt(data) do \
{ \
    *data = WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt; \
}while(0);

#define WssM_Main_Read_WssM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_crc(data) do \
{ \
    *data = WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_NO_FAULT(data) do \
{ \
    *data = WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NO_FAULT; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_WHEEL_SPEED_DECODER_DATA(data) do \
{ \
    *data = WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WHEEL_SPEED_DECODER_DATA; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_STANDSTILL(data) do \
{ \
    *data = WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STANDSTILL; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_LATCH_D0(data) do \
{ \
    *data = WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_LATCH_D0; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_NODATA(data) do \
{ \
    *data = WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NODATA; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_INVALID(data) do \
{ \
    *data = WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_INVALID; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_OPEN(data) do \
{ \
    *data = WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_OPEN; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_STB(data) do \
{ \
    *data = WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STB; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_STG(data) do \
{ \
    *data = WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STG; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_WSI_OT(data) do \
{ \
    *data = WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WSI_OT; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_CURRENT_HI(data) do \
{ \
    *data = WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_CURRENT_HI; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_NO_FAULT(data) do \
{ \
    *data = WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NO_FAULT; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_WHEEL_SPEED_DECODER_DATA(data) do \
{ \
    *data = WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WHEEL_SPEED_DECODER_DATA; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_STANDSTILL(data) do \
{ \
    *data = WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STANDSTILL; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_LATCH_D0(data) do \
{ \
    *data = WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_LATCH_D0; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_NODATA(data) do \
{ \
    *data = WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NODATA; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_INVALID(data) do \
{ \
    *data = WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_INVALID; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_OPEN(data) do \
{ \
    *data = WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_OPEN; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_STB(data) do \
{ \
    *data = WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STB; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_STG(data) do \
{ \
    *data = WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STG; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_WSI_OT(data) do \
{ \
    *data = WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WSI_OT; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_CURRENT_HI(data) do \
{ \
    *data = WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_CURRENT_HI; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_NO_FAULT(data) do \
{ \
    *data = WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NO_FAULT; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_WHEEL_SPEED_DECODER_DATA(data) do \
{ \
    *data = WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WHEEL_SPEED_DECODER_DATA; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_STANDSTILL(data) do \
{ \
    *data = WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STANDSTILL; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_LATCH_D0(data) do \
{ \
    *data = WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_LATCH_D0; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_NODATA(data) do \
{ \
    *data = WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NODATA; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_INVALID(data) do \
{ \
    *data = WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_INVALID; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_OPEN(data) do \
{ \
    *data = WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_OPEN; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_STB(data) do \
{ \
    *data = WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STB; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_STG(data) do \
{ \
    *data = WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STG; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_WSI_OT(data) do \
{ \
    *data = WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WSI_OT; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_CURRENT_HI(data) do \
{ \
    *data = WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_CURRENT_HI; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_NO_FAULT(data) do \
{ \
    *data = WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NO_FAULT; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_WHEEL_SPEED_DECODER_DATA(data) do \
{ \
    *data = WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WHEEL_SPEED_DECODER_DATA; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_STANDSTILL(data) do \
{ \
    *data = WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STANDSTILL; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_LATCH_D0(data) do \
{ \
    *data = WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_LATCH_D0; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_NODATA(data) do \
{ \
    *data = WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NODATA; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_INVALID(data) do \
{ \
    *data = WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_INVALID; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_OPEN(data) do \
{ \
    *data = WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_OPEN; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_STB(data) do \
{ \
    *data = WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STB; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_STG(data) do \
{ \
    *data = WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STG; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_WSI_OT(data) do \
{ \
    *data = WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WSI_OT; \
}while(0);

#define WssM_Main_Read_WssM_MainAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_CURRENT_HI(data) do \
{ \
    *data = WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_CURRENT_HI; \
}while(0);

#define WssM_Main_Read_WssM_MainWssSpeedOut_WssMin(data) do \
{ \
    *data = WssM_MainWssSpeedOut.WssMin; \
}while(0);

#define WssM_Main_Read_WssM_MainAchAsicInvalid(data) do \
{ \
    *data = WssM_MainAchAsicInvalid; \
}while(0);

#define WssM_Main_Read_WssM_MainEcuModeSts(data) do \
{ \
    *data = WssM_MainEcuModeSts; \
}while(0);

#define WssM_Main_Read_WssM_MainIgnOnOffSts(data) do \
{ \
    *data = WssM_MainIgnOnOffSts; \
}while(0);

#define WssM_Main_Read_WssM_MainIgnEdgeSts(data) do \
{ \
    *data = WssM_MainIgnEdgeSts; \
}while(0);

#define WssM_Main_Read_WssM_MainVBatt1Mon(data) do \
{ \
    *data = WssM_MainVBatt1Mon; \
}while(0);

#define WssM_Main_Read_WssM_MainDiagClrSrs(data) do \
{ \
    *data = WssM_MainDiagClrSrs; \
}while(0);


/* Set Output DE MAcro Function */
#define WssM_Main_Write_WssM_MainWssMonData(data) do \
{ \
    WssM_MainWssMonData = *data; \
}while(0);

#define WssM_Main_Write_WssM_MainWssMonData_WssM_FlOpen_Err(data) do \
{ \
    WssM_MainWssMonData.WssM_FlOpen_Err = *data; \
}while(0);

#define WssM_Main_Write_WssM_MainWssMonData_WssM_FlShort_Err(data) do \
{ \
    WssM_MainWssMonData.WssM_FlShort_Err = *data; \
}while(0);

#define WssM_Main_Write_WssM_MainWssMonData_WssM_FlOverTemp_Err(data) do \
{ \
    WssM_MainWssMonData.WssM_FlOverTemp_Err = *data; \
}while(0);

#define WssM_Main_Write_WssM_MainWssMonData_WssM_FlLeakage_Err(data) do \
{ \
    WssM_MainWssMonData.WssM_FlLeakage_Err = *data; \
}while(0);

#define WssM_Main_Write_WssM_MainWssMonData_WssM_FrOpen_Err(data) do \
{ \
    WssM_MainWssMonData.WssM_FrOpen_Err = *data; \
}while(0);

#define WssM_Main_Write_WssM_MainWssMonData_WssM_FrShort_Err(data) do \
{ \
    WssM_MainWssMonData.WssM_FrShort_Err = *data; \
}while(0);

#define WssM_Main_Write_WssM_MainWssMonData_WssM_FrOverTemp_Err(data) do \
{ \
    WssM_MainWssMonData.WssM_FrOverTemp_Err = *data; \
}while(0);

#define WssM_Main_Write_WssM_MainWssMonData_WssM_FrLeakage_Err(data) do \
{ \
    WssM_MainWssMonData.WssM_FrLeakage_Err = *data; \
}while(0);

#define WssM_Main_Write_WssM_MainWssMonData_WssM_RlOpen_Err(data) do \
{ \
    WssM_MainWssMonData.WssM_RlOpen_Err = *data; \
}while(0);

#define WssM_Main_Write_WssM_MainWssMonData_WssM_RlShort_Err(data) do \
{ \
    WssM_MainWssMonData.WssM_RlShort_Err = *data; \
}while(0);

#define WssM_Main_Write_WssM_MainWssMonData_WssM_RlOverTemp_Err(data) do \
{ \
    WssM_MainWssMonData.WssM_RlOverTemp_Err = *data; \
}while(0);

#define WssM_Main_Write_WssM_MainWssMonData_WssM_RlLeakage_Err(data) do \
{ \
    WssM_MainWssMonData.WssM_RlLeakage_Err = *data; \
}while(0);

#define WssM_Main_Write_WssM_MainWssMonData_WssM_RrOpen_Err(data) do \
{ \
    WssM_MainWssMonData.WssM_RrOpen_Err = *data; \
}while(0);

#define WssM_Main_Write_WssM_MainWssMonData_WssM_RrShort_Err(data) do \
{ \
    WssM_MainWssMonData.WssM_RrShort_Err = *data; \
}while(0);

#define WssM_Main_Write_WssM_MainWssMonData_WssM_RrOverTemp_Err(data) do \
{ \
    WssM_MainWssMonData.WssM_RrOverTemp_Err = *data; \
}while(0);

#define WssM_Main_Write_WssM_MainWssMonData_WssM_RrLeakage_Err(data) do \
{ \
    WssM_MainWssMonData.WssM_RrLeakage_Err = *data; \
}while(0);

#define WssM_Main_Write_WssM_MainWssMonData_WssM_Asic_Comm_Err(data) do \
{ \
    WssM_MainWssMonData.WssM_Asic_Comm_Err = *data; \
}while(0);

#define WssM_Main_Write_WssM_MainWssMonData_WssM_Inhibit_FL(data) do \
{ \
    WssM_MainWssMonData.WssM_Inhibit_FL = *data; \
}while(0);

#define WssM_Main_Write_WssM_MainWssMonData_WssM_Inhibit_FR(data) do \
{ \
    WssM_MainWssMonData.WssM_Inhibit_FR = *data; \
}while(0);

#define WssM_Main_Write_WssM_MainWssMonData_WssM_Inhibit_RL(data) do \
{ \
    WssM_MainWssMonData.WssM_Inhibit_RL = *data; \
}while(0);

#define WssM_Main_Write_WssM_MainWssMonData_WssM_Inhibit_RR(data) do \
{ \
    WssM_MainWssMonData.WssM_Inhibit_RR = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* WSSM_MAIN_IFA_H_ */
/** @} */
