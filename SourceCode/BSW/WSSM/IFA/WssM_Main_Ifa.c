/**
 * @defgroup WssM_Main_Ifa WssM_Main_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        WssM_Main_Ifa.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "WssM_Main_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define WSSM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "WssM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define WSSM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "WssM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define WSSM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "WssM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define WSSM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "WssM_MemMap.h"
#define WSSM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "WssM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define WSSM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "WssM_MemMap.h"
#define WSSM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "WssM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define WSSM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "WssM_MemMap.h"
#define WSSM_MAIN_START_SEC_VAR_32BIT
#include "WssM_MemMap.h"
/** Variable Section (32BIT)**/


#define WSSM_MAIN_STOP_SEC_VAR_32BIT
#include "WssM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define WSSM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "WssM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define WSSM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "WssM_MemMap.h"
#define WSSM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "WssM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define WSSM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "WssM_MemMap.h"
#define WSSM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "WssM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define WSSM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "WssM_MemMap.h"
#define WSSM_MAIN_START_SEC_VAR_32BIT
#include "WssM_MemMap.h"
/** Variable Section (32BIT)**/


#define WSSM_MAIN_STOP_SEC_VAR_32BIT
#include "WssM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define WSSM_MAIN_START_SEC_CODE
#include "WssM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define WSSM_MAIN_STOP_SEC_CODE
#include "WssM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
