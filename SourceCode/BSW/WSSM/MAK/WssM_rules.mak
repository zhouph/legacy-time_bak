# \file
#
# \brief WssM
#
# This file contains the implementation of the SWC
# module WssM.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += WssM_src

WssM_src_FILES        += $(WssM_SRC_PATH)\WssM_Main.c
WssM_src_FILES        += $(WssM_IFA_PATH)\WssM_Main_Ifa.c
WssM_src_FILES        += $(WssM_CFG_PATH)\WssM_Cfg.c
WssM_src_FILES        += $(WssM_CAL_PATH)\WssM_Cal.c
WssM_src_FILES        += $(WssM_SRC_PATH)\WssM_Main_Proc.c

ifeq ($(ICE_COMPILE),true)
WssM_src_FILES        += $(WssM_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	WssM_src_FILES        += $(WssM_UNITY_PATH)\unity.c
	WssM_src_FILES        += $(WssM_UNITY_PATH)\unity_fixture.c	
	WssM_src_FILES        += $(WssM_UT_PATH)\main.c
	WssM_src_FILES        += $(WssM_UT_PATH)\WssM_Main\WssM_Main_UtMain.c
endif