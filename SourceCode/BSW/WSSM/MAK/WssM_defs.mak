# \file
#
# \brief WssM
#
# This file contains the implementation of the SWC
# module WssM.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

WssM_CORE_PATH     := $(MANDO_BSW_ROOT)\WssM
WssM_CAL_PATH      := $(WssM_CORE_PATH)\CAL\$(WssM_VARIANT)
WssM_SRC_PATH      := $(WssM_CORE_PATH)\SRC
WssM_CFG_PATH      := $(WssM_CORE_PATH)\CFG\$(WssM_VARIANT)
WssM_HDR_PATH      := $(WssM_CORE_PATH)\HDR
WssM_IFA_PATH      := $(WssM_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    WssM_CMN_PATH      := $(WssM_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	WssM_UT_PATH		:= $(WssM_CORE_PATH)\UT
	WssM_UNITY_PATH	:= $(WssM_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(WssM_UT_PATH)
	CC_INCLUDE_PATH		+= $(WssM_UNITY_PATH)
	WssM_Main_PATH 	:= WssM_UT_PATH\WssM_Main
endif
CC_INCLUDE_PATH    += $(WssM_CAL_PATH)
CC_INCLUDE_PATH    += $(WssM_SRC_PATH)
CC_INCLUDE_PATH    += $(WssM_CFG_PATH)
CC_INCLUDE_PATH    += $(WssM_HDR_PATH)
CC_INCLUDE_PATH    += $(WssM_IFA_PATH)
CC_INCLUDE_PATH    += $(WssM_CMN_PATH)

