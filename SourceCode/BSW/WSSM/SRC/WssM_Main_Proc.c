/**
 * @defgroup WssM_Main WssM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        WssM_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "WssM_Main.h"
#include "WssM_Main_Ifa.h"

#include "WssM_Main_Proc.h"
#include "common.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define WSSM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "WssM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define WSSM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "WssM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define WSSM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "WssM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

#define WSSM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "WssM_MemMap.h"
#define WSSM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "WssM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define WSSM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "WssM_MemMap.h"
#define WSSM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "WssM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define WSSM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "WssM_MemMap.h"
#define WSSM_MAIN_START_SEC_VAR_32BIT
#include "WssM_MemMap.h"
/** Variable Section (32BIT)**/


#define WSSM_MAIN_STOP_SEC_VAR_32BIT
#include "WssM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define WSSM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "WssM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define WSSM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "WssM_MemMap.h"
#define WSSM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "WssM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define WSSM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "WssM_MemMap.h"
#define WSSM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "WssM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define WSSM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "WssM_MemMap.h"
#define WSSM_MAIN_START_SEC_VAR_32BIT
#include "WssM_MemMap.h"
/** Variable Section (32BIT)**/


#define WSSM_MAIN_STOP_SEC_VAR_32BIT
#include "WssM_MemMap.h"

/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define WSSM_MAIN_START_SEC_CODE
#include "WssM_MemMap.h"

void WssM_Main_Proc(WssM_Main_HdrBusType *pWssMInfo)
{
    uint8 u8FrOpenErr = ERR_NONE;
    uint8 u8FrShortErr = ERR_NONE;
    uint8 u8FrOverTempErr = ERR_NONE;
    uint8 u8FrCurrentHIErr = ERR_NONE;
    
    uint8 u8FlOpenErr = ERR_NONE;
    uint8 u8FlShortErr = ERR_NONE;
    uint8 u8FlOverTempErr = ERR_NONE;
    uint8 u8FlCurrentHIErr = ERR_NONE;

    uint8 u8RrOpenErr = ERR_NONE;
    uint8 u8RrShortErr = ERR_NONE;
    uint8 u8RrOverTempErr = ERR_NONE;
    uint8 u8RrCurrentHIErr = ERR_NONE;

    uint8 u8RlOpenErr = ERR_NONE;
    uint8 u8RlShortErr = ERR_NONE;
    uint8 u8RlOverTempErr = ERR_NONE;
    uint8 u8RlCurrentHIErr = ERR_NONE;
    
    uint8 u8Inhibit = 0;

    /*Inhibit condition : To be added*/
    
    if( (pWssMInfo->WssM_MainVBatt1Mon < U16_BATT1VOLT_7V0) || (pWssMInfo->WssM_MainVBatt1Mon > U16_BATT1VOLT_17V0) )
    {
        u8Inhibit=ERR_INHIBIT;
    }
    pWssMInfo->WssM_MainWssMonData.WssM_FlOpen_Err=u8Inhibit;
    pWssMInfo->WssM_MainWssMonData.WssM_FlShort_Err=u8Inhibit;
    pWssMInfo->WssM_MainWssMonData.WssM_FlOverTemp_Err=u8Inhibit;
    pWssMInfo->WssM_MainWssMonData.WssM_FlLeakage_Err=u8Inhibit;

    pWssMInfo->WssM_MainWssMonData.WssM_FrOpen_Err=u8Inhibit;
    pWssMInfo->WssM_MainWssMonData.WssM_FrShort_Err=u8Inhibit;
    pWssMInfo->WssM_MainWssMonData.WssM_FrOverTemp_Err=u8Inhibit;
    pWssMInfo->WssM_MainWssMonData.WssM_FrLeakage_Err=u8Inhibit;

    pWssMInfo->WssM_MainWssMonData.WssM_RlOpen_Err=u8Inhibit;
    pWssMInfo->WssM_MainWssMonData.WssM_RlShort_Err=u8Inhibit;
    pWssMInfo->WssM_MainWssMonData.WssM_RlOverTemp_Err=u8Inhibit;
    pWssMInfo->WssM_MainWssMonData.WssM_RlLeakage_Err=u8Inhibit;

    pWssMInfo->WssM_MainWssMonData.WssM_RrOpen_Err=u8Inhibit;
    pWssMInfo->WssM_MainWssMonData.WssM_RrShort_Err=u8Inhibit;
    pWssMInfo->WssM_MainWssMonData.WssM_RrOverTemp_Err=u8Inhibit;
    pWssMInfo->WssM_MainWssMonData.WssM_RrLeakage_Err=u8Inhibit;
    
     /*Signal Check Inhibit Flag*/   
    pWssMInfo->WssM_MainWssMonData.WssM_Inhibit_FL=0;
    pWssMInfo->WssM_MainWssMonData.WssM_Inhibit_FR=0;
    pWssMInfo->WssM_MainWssMonData.WssM_Inhibit_RL=0;
    pWssMInfo->WssM_MainWssMonData.WssM_Inhibit_RR=0;
   
     
    /*FL*/
    if(pWssMInfo->WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NO_FAULT == 1)
    {
        if(pWssMInfo->WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_OPEN == 1)
        {
            u8FlOpenErr = ERR_PREFAILED;
        }
        else
        {
            u8FlOpenErr = ERR_PREPASSED;
        }
        if( (pWssMInfo->WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STB == 1)
            ||(pWssMInfo->WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STG == 1) )
        {
            u8FlShortErr = ERR_PREFAILED;
        }
        else
        {
            u8FlShortErr = ERR_PREPASSED;
        }
        if(pWssMInfo->WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WSI_OT == 1)
        {
            u8FlOverTempErr = ERR_PREFAILED;
        }
        else
        {
            u8FlOverTempErr = ERR_PREPASSED;
        }
        if(pWssMInfo->WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_CURRENT_HI == 1)
        {
            u8FlCurrentHIErr = ERR_PREFAILED;
        }
        else
        {
           u8FlCurrentHIErr = ERR_PREPASSED;
        }
        if( (u8FlOpenErr==ERR_PREFAILED)||(u8FlShortErr==ERR_PREFAILED)||(u8FlOverTempErr==ERR_PREFAILED)||(u8FlCurrentHIErr==ERR_PREFAILED) )
        {
            pWssMInfo->WssM_MainWssMonData.WssM_Inhibit_FL=1;
        }
    }
    else
    {
        u8FlOpenErr = ERR_PREPASSED;
        u8FlShortErr = ERR_PREPASSED;
        u8FlOverTempErr = ERR_PREPASSED;
        u8FlCurrentHIErr = ERR_PREPASSED;
    }

    /*FR*/
    if(pWssMInfo->WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NO_FAULT == 1)
    {
        if(pWssMInfo->WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_OPEN == 1)
        {
            u8FrOpenErr = ERR_PREFAILED;
        }
        else
        {
            u8FrOpenErr = ERR_PREPASSED;
        }
        if( (pWssMInfo->WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STB == 1)
            ||(pWssMInfo->WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STG == 1) )
        {
            u8FrShortErr = ERR_PREFAILED;
        }
        else
        {
            u8FrShortErr = ERR_PREPASSED;
        }
        if(pWssMInfo->WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WSI_OT == 1)
        {
            u8FrOverTempErr = ERR_PREFAILED;
        }
        else
        {
            u8FrOverTempErr = ERR_PREPASSED;
        }
        if(pWssMInfo->WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_CURRENT_HI == 1)
        {
            u8FrCurrentHIErr = ERR_PREFAILED;
        }
        else
        {
            u8FrCurrentHIErr = ERR_PREPASSED;
        }
        if( (u8FrOpenErr==ERR_PREFAILED)||(u8FrShortErr==ERR_PREFAILED)||(u8FrOverTempErr==ERR_PREFAILED)||(u8FrCurrentHIErr==ERR_PREFAILED) )
        {
            pWssMInfo->WssM_MainWssMonData.WssM_Inhibit_FR=1;
        }
    }
    else
    {
        u8FrOpenErr = ERR_PREPASSED;
        u8FrShortErr = ERR_PREPASSED;
        u8FrOverTempErr = ERR_PREPASSED;
        u8FrCurrentHIErr = ERR_PREPASSED;
    }

     /*RL*/
    if(pWssMInfo->WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NO_FAULT == 1)
    {
        if(pWssMInfo->WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_OPEN == 1)
        {
            u8RlOpenErr = ERR_PREFAILED;
        }
        else
        {
            u8RlOpenErr = ERR_PREPASSED;
        }
        if( (pWssMInfo->WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STB == 1)
            ||(pWssMInfo->WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STG == 1) )
        {
            u8RlShortErr = ERR_PREFAILED;
        }
        else
        {
            u8RlShortErr = ERR_PREPASSED;
        }
        if(pWssMInfo->WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WSI_OT == 1)
        {
            u8RlOverTempErr = ERR_PREFAILED;
        }
        else
        {
            u8RlOverTempErr = ERR_PREPASSED;
        }
        if(pWssMInfo->WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_CURRENT_HI == 1)
        {
            u8RlCurrentHIErr = ERR_PREFAILED;
        }
        else
        {
            u8RlCurrentHIErr = ERR_PREPASSED;
        }
        if( (u8RlOpenErr==ERR_PREFAILED)||(u8RlShortErr==ERR_PREFAILED)||(u8RlOverTempErr==ERR_PREFAILED)||(u8RlCurrentHIErr==ERR_PREFAILED) )
        {
            pWssMInfo->WssM_MainWssMonData.WssM_Inhibit_RL=1;
        }
    }
    else
    {
        u8RlOpenErr = ERR_PREPASSED;
        u8RlShortErr = ERR_PREPASSED;
        u8RlOverTempErr = ERR_PREPASSED;
        u8RlCurrentHIErr = ERR_PREPASSED;
    }

    /*RR*/
    if(pWssMInfo->WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NO_FAULT == 1)
    {
        if(pWssMInfo->WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_OPEN == 1)
        {
            u8RrOpenErr = ERR_PREFAILED;
        }
        else
        {
            u8RrOpenErr = ERR_PREPASSED;
        }
        if( (pWssMInfo->WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STB == 1)
            ||(pWssMInfo->WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STG == 1) )
        {
            u8RrShortErr = ERR_PREFAILED;
        }
        else
        {
            u8RrShortErr = ERR_PREPASSED;
        }
        if(pWssMInfo->WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WSI_OT == 1)
        {
            u8RrOverTempErr = ERR_PREFAILED;
        }
        else
        {
            u8RrOverTempErr = ERR_PREPASSED;
        }
        if(pWssMInfo->WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_CURRENT_HI == 1)
        {
            u8RrCurrentHIErr = ERR_PREFAILED;
        }
        else
        {
            u8RrCurrentHIErr = ERR_PREPASSED;
        }
        if( (u8RrOpenErr==ERR_PREFAILED)||(u8RrShortErr==ERR_PREFAILED)||(u8RrOverTempErr==ERR_PREFAILED)||(u8RrCurrentHIErr==ERR_PREFAILED) )
        {
            pWssMInfo->WssM_MainWssMonData.WssM_Inhibit_RR=1;
        }
    }
    else
    {
        u8RrOpenErr = ERR_PREPASSED;
        u8RrShortErr = ERR_PREPASSED;
        u8RrOverTempErr = ERR_PREPASSED;
        u8RrCurrentHIErr = ERR_PREPASSED;
    }
    /*FL Copy internal faultflag to output signal*/
    pWssMInfo->WssM_MainWssMonData.WssM_FlOpen_Err = u8FlOpenErr;
    pWssMInfo->WssM_MainWssMonData.WssM_FlShort_Err = u8FlShortErr;
    pWssMInfo->WssM_MainWssMonData.WssM_FlOverTemp_Err = u8FlOverTempErr;
    pWssMInfo->WssM_MainWssMonData.WssM_FlLeakage_Err = u8FlCurrentHIErr;
    /*FR Copy internal faultflag to output signal*/
    pWssMInfo->WssM_MainWssMonData.WssM_FrOpen_Err = u8FrOpenErr;
    pWssMInfo->WssM_MainWssMonData.WssM_FrShort_Err = u8FrShortErr;
    pWssMInfo->WssM_MainWssMonData.WssM_FrOverTemp_Err = u8FrOverTempErr;
    pWssMInfo->WssM_MainWssMonData.WssM_FrLeakage_Err = u8FrCurrentHIErr;
    /*RL Copy internal faultflag to output signal*/
    pWssMInfo->WssM_MainWssMonData.WssM_RlOpen_Err = u8RlOpenErr;
    pWssMInfo->WssM_MainWssMonData.WssM_RlShort_Err = u8RlShortErr;
    pWssMInfo->WssM_MainWssMonData.WssM_RlOverTemp_Err = u8RlOverTempErr;
    pWssMInfo->WssM_MainWssMonData.WssM_RlLeakage_Err = u8RlCurrentHIErr;
    /*RR Copy internal faultflag to output signal*/    
    pWssMInfo->WssM_MainWssMonData.WssM_RrOpen_Err = u8RrOpenErr;
    pWssMInfo->WssM_MainWssMonData.WssM_RrShort_Err = u8RrShortErr;
    pWssMInfo->WssM_MainWssMonData.WssM_RrOverTemp_Err = u8RrOverTempErr;
    pWssMInfo->WssM_MainWssMonData.WssM_RrLeakage_Err = u8RrCurrentHIErr;

    if(pWssMInfo->WssM_MainDiagClrSrs==1)
    {
        pWssMInfo->WssM_MainWssMonData.WssM_FlShort_Err=ERR_NONE;
        pWssMInfo->WssM_MainWssMonData.WssM_FlOpen_Err=ERR_NONE;
        pWssMInfo->WssM_MainWssMonData.WssM_FlOverTemp_Err=ERR_NONE;
        pWssMInfo->WssM_MainWssMonData.WssM_FlLeakage_Err=ERR_NONE;
        
        pWssMInfo->WssM_MainWssMonData.WssM_FrShort_Err=ERR_NONE;
        pWssMInfo->WssM_MainWssMonData.WssM_FrOpen_Err=ERR_NONE;
        pWssMInfo->WssM_MainWssMonData.WssM_FrOverTemp_Err=ERR_NONE;
        pWssMInfo->WssM_MainWssMonData.WssM_FrLeakage_Err=ERR_NONE;

        pWssMInfo->WssM_MainWssMonData.WssM_RlShort_Err=ERR_NONE;
        pWssMInfo->WssM_MainWssMonData.WssM_RlOpen_Err=ERR_NONE;
        pWssMInfo->WssM_MainWssMonData.WssM_RlOverTemp_Err=ERR_NONE;
        pWssMInfo->WssM_MainWssMonData.WssM_RlLeakage_Err=ERR_NONE;

        pWssMInfo->WssM_MainWssMonData.WssM_RrShort_Err=ERR_NONE;
        pWssMInfo->WssM_MainWssMonData.WssM_RrOpen_Err=ERR_NONE;
        pWssMInfo->WssM_MainWssMonData.WssM_RrOverTemp_Err=ERR_NONE;
        pWssMInfo->WssM_MainWssMonData.WssM_RrLeakage_Err=ERR_NONE;
    }
}
