/**
 * @defgroup WssM_Main WssM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        WssM_Main.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "WssM_Main.h"
#include "WssM_Main_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define WSSM_MAIN_START_SEC_CONST_UNSPECIFIED
#include "WssM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define WSSM_MAIN_STOP_SEC_CONST_UNSPECIFIED
#include "WssM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define WSSM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "WssM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
WssM_Main_HdrBusType WssM_MainBus;

/* Version Info */
const SwcVersionInfo_t WssM_MainVersionInfo = 
{   
    WSSM_MAIN_MODULE_ID,           /* WssM_MainVersionInfo.ModuleId */
    WSSM_MAIN_MAJOR_VERSION,       /* WssM_MainVersionInfo.MajorVer */
    WSSM_MAIN_MINOR_VERSION,       /* WssM_MainVersionInfo.MinorVer */
    WSSM_MAIN_PATCH_VERSION,       /* WssM_MainVersionInfo.PatchVer */
    WSSM_MAIN_BRANCH_VERSION       /* WssM_MainVersionInfo.BranchVer */
};
    
/* Input Data Element */
Ach_InputAchSysPwrAsicInfo_t WssM_MainAchSysPwrAsicInfo;
Ach_InputAchWssPort0AsicInfo_t WssM_MainAchWssPort0AsicInfo;
Ach_InputAchWssPort1AsicInfo_t WssM_MainAchWssPort1AsicInfo;
Ach_InputAchWssPort2AsicInfo_t WssM_MainAchWssPort2AsicInfo;
Ach_InputAchWssPort3AsicInfo_t WssM_MainAchWssPort3AsicInfo;
Wss_SenWssSpeedOut_t WssM_MainWssSpeedOut;
Ach_InputAchAsicInvalidInfo_t WssM_MainAchAsicInvalid;
Mom_HndlrEcuModeSts_t WssM_MainEcuModeSts;
Prly_HndlrIgnOnOffSts_t WssM_MainIgnOnOffSts;
Prly_HndlrIgnEdgeSts_t WssM_MainIgnEdgeSts;
Ioc_InputSR1msVBatt1Mon_t WssM_MainVBatt1Mon;
Diag_HndlrDiagClr_t WssM_MainDiagClrSrs;

/* Output Data Element */
WssM_MainWssMonData_t WssM_MainWssMonData;

uint32 WssM_Main_Timer_Start;
uint32 WssM_Main_Timer_Elapsed;

#define WSSM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "WssM_MemMap.h"
#define WSSM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "WssM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define WSSM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "WssM_MemMap.h"
#define WSSM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "WssM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define WSSM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "WssM_MemMap.h"
#define WSSM_MAIN_START_SEC_VAR_32BIT
#include "WssM_MemMap.h"
/** Variable Section (32BIT)**/


#define WSSM_MAIN_STOP_SEC_VAR_32BIT
#include "WssM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define WSSM_MAIN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "WssM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define WSSM_MAIN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "WssM_MemMap.h"
#define WSSM_MAIN_START_SEC_VAR_NOINIT_32BIT
#include "WssM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define WSSM_MAIN_STOP_SEC_VAR_NOINIT_32BIT
#include "WssM_MemMap.h"
#define WSSM_MAIN_START_SEC_VAR_UNSPECIFIED
#include "WssM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define WSSM_MAIN_STOP_SEC_VAR_UNSPECIFIED
#include "WssM_MemMap.h"
#define WSSM_MAIN_START_SEC_VAR_32BIT
#include "WssM_MemMap.h"
/** Variable Section (32BIT)**/


#define WSSM_MAIN_STOP_SEC_VAR_32BIT
#include "WssM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define WSSM_MAIN_START_SEC_CODE
#include "WssM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void WssM_Main_Init(void)
{
    /* Initialize internal bus */
    WssM_MainBus.WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long = 0;
    WssM_MainBus.WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short = 0;
    WssM_MainBus.WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add = 0;
    WssM_MainBus.WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt = 0;
    WssM_MainBus.WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc = 0;
    WssM_MainBus.WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NO_FAULT = 0;
    WssM_MainBus.WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WHEEL_SPEED_DECODER_DATA = 0;
    WssM_MainBus.WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STANDSTILL = 0;
    WssM_MainBus.WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_LATCH_D0 = 0;
    WssM_MainBus.WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NODATA = 0;
    WssM_MainBus.WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_INVALID = 0;
    WssM_MainBus.WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_OPEN = 0;
    WssM_MainBus.WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STB = 0;
    WssM_MainBus.WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STG = 0;
    WssM_MainBus.WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WSI_OT = 0;
    WssM_MainBus.WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_CURRENT_HI = 0;
    WssM_MainBus.WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NO_FAULT = 0;
    WssM_MainBus.WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WHEEL_SPEED_DECODER_DATA = 0;
    WssM_MainBus.WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STANDSTILL = 0;
    WssM_MainBus.WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_LATCH_D0 = 0;
    WssM_MainBus.WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NODATA = 0;
    WssM_MainBus.WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_INVALID = 0;
    WssM_MainBus.WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_OPEN = 0;
    WssM_MainBus.WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STB = 0;
    WssM_MainBus.WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STG = 0;
    WssM_MainBus.WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WSI_OT = 0;
    WssM_MainBus.WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_CURRENT_HI = 0;
    WssM_MainBus.WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NO_FAULT = 0;
    WssM_MainBus.WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WHEEL_SPEED_DECODER_DATA = 0;
    WssM_MainBus.WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STANDSTILL = 0;
    WssM_MainBus.WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_LATCH_D0 = 0;
    WssM_MainBus.WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NODATA = 0;
    WssM_MainBus.WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_INVALID = 0;
    WssM_MainBus.WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_OPEN = 0;
    WssM_MainBus.WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STB = 0;
    WssM_MainBus.WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STG = 0;
    WssM_MainBus.WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WSI_OT = 0;
    WssM_MainBus.WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_CURRENT_HI = 0;
    WssM_MainBus.WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NO_FAULT = 0;
    WssM_MainBus.WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WHEEL_SPEED_DECODER_DATA = 0;
    WssM_MainBus.WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STANDSTILL = 0;
    WssM_MainBus.WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_LATCH_D0 = 0;
    WssM_MainBus.WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NODATA = 0;
    WssM_MainBus.WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_INVALID = 0;
    WssM_MainBus.WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_OPEN = 0;
    WssM_MainBus.WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STB = 0;
    WssM_MainBus.WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STG = 0;
    WssM_MainBus.WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WSI_OT = 0;
    WssM_MainBus.WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_CURRENT_HI = 0;
    WssM_MainBus.WssM_MainWssSpeedOut.WssMin = 0;
    WssM_MainBus.WssM_MainAchAsicInvalid = 0;
    WssM_MainBus.WssM_MainEcuModeSts = 0;
    WssM_MainBus.WssM_MainIgnOnOffSts = 0;
    WssM_MainBus.WssM_MainIgnEdgeSts = 0;
    WssM_MainBus.WssM_MainVBatt1Mon = 0;
    WssM_MainBus.WssM_MainDiagClrSrs = 0;
    WssM_MainBus.WssM_MainWssMonData.WssM_FlOpen_Err = 0;
    WssM_MainBus.WssM_MainWssMonData.WssM_FlShort_Err = 0;
    WssM_MainBus.WssM_MainWssMonData.WssM_FlOverTemp_Err = 0;
    WssM_MainBus.WssM_MainWssMonData.WssM_FlLeakage_Err = 0;
    WssM_MainBus.WssM_MainWssMonData.WssM_FrOpen_Err = 0;
    WssM_MainBus.WssM_MainWssMonData.WssM_FrShort_Err = 0;
    WssM_MainBus.WssM_MainWssMonData.WssM_FrOverTemp_Err = 0;
    WssM_MainBus.WssM_MainWssMonData.WssM_FrLeakage_Err = 0;
    WssM_MainBus.WssM_MainWssMonData.WssM_RlOpen_Err = 0;
    WssM_MainBus.WssM_MainWssMonData.WssM_RlShort_Err = 0;
    WssM_MainBus.WssM_MainWssMonData.WssM_RlOverTemp_Err = 0;
    WssM_MainBus.WssM_MainWssMonData.WssM_RlLeakage_Err = 0;
    WssM_MainBus.WssM_MainWssMonData.WssM_RrOpen_Err = 0;
    WssM_MainBus.WssM_MainWssMonData.WssM_RrShort_Err = 0;
    WssM_MainBus.WssM_MainWssMonData.WssM_RrOverTemp_Err = 0;
    WssM_MainBus.WssM_MainWssMonData.WssM_RrLeakage_Err = 0;
    WssM_MainBus.WssM_MainWssMonData.WssM_Asic_Comm_Err = 0;
    WssM_MainBus.WssM_MainWssMonData.WssM_Inhibit_FL = 0;
    WssM_MainBus.WssM_MainWssMonData.WssM_Inhibit_FR = 0;
    WssM_MainBus.WssM_MainWssMonData.WssM_Inhibit_RL = 0;
    WssM_MainBus.WssM_MainWssMonData.WssM_Inhibit_RR = 0;
}

void WssM_Main(void)
{
    WssM_Main_Timer_Start = STM0_TIM0.U;

    /* Input */
    /* Decomposed structure interface */
    WssM_Main_Read_WssM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_long(&WssM_MainBus.WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long);
    WssM_Main_Read_WssM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_short(&WssM_MainBus.WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short);
    WssM_Main_Read_WssM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_add(&WssM_MainBus.WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add);
    WssM_Main_Read_WssM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_fcnt(&WssM_MainBus.WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt);
    WssM_Main_Read_WssM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_crc(&WssM_MainBus.WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc);

    WssM_Main_Read_WssM_MainAchWssPort0AsicInfo(&WssM_MainBus.WssM_MainAchWssPort0AsicInfo);
    /*==============================================================================
    * Members of structure WssM_MainAchWssPort0AsicInfo 
     : WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NO_FAULT;
     : WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STANDSTILL;
     : WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_LATCH_D0;
     : WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NODATA;
     : WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_INVALID;
     : WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_OPEN;
     : WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STB;
     : WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STG;
     : WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WSI_OT;
     : WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_CURRENT_HI;
     =============================================================================*/
    
    WssM_Main_Read_WssM_MainAchWssPort1AsicInfo(&WssM_MainBus.WssM_MainAchWssPort1AsicInfo);
    /*==============================================================================
    * Members of structure WssM_MainAchWssPort1AsicInfo 
     : WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NO_FAULT;
     : WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STANDSTILL;
     : WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_LATCH_D0;
     : WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NODATA;
     : WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_INVALID;
     : WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_OPEN;
     : WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STB;
     : WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STG;
     : WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WSI_OT;
     : WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_CURRENT_HI;
     =============================================================================*/
    
    WssM_Main_Read_WssM_MainAchWssPort2AsicInfo(&WssM_MainBus.WssM_MainAchWssPort2AsicInfo);
    /*==============================================================================
    * Members of structure WssM_MainAchWssPort2AsicInfo 
     : WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NO_FAULT;
     : WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STANDSTILL;
     : WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_LATCH_D0;
     : WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NODATA;
     : WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_INVALID;
     : WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_OPEN;
     : WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STB;
     : WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STG;
     : WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WSI_OT;
     : WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_CURRENT_HI;
     =============================================================================*/
    
    WssM_Main_Read_WssM_MainAchWssPort3AsicInfo(&WssM_MainBus.WssM_MainAchWssPort3AsicInfo);
    /*==============================================================================
    * Members of structure WssM_MainAchWssPort3AsicInfo 
     : WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NO_FAULT;
     : WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
     : WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STANDSTILL;
     : WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_LATCH_D0;
     : WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NODATA;
     : WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_INVALID;
     : WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_OPEN;
     : WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STB;
     : WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STG;
     : WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WSI_OT;
     : WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_CURRENT_HI;
     =============================================================================*/
    
    /* Decomposed structure interface */
    WssM_Main_Read_WssM_MainWssSpeedOut_WssMin(&WssM_MainBus.WssM_MainWssSpeedOut.WssMin);

    WssM_Main_Read_WssM_MainAchAsicInvalid(&WssM_MainBus.WssM_MainAchAsicInvalid);
    WssM_Main_Read_WssM_MainEcuModeSts(&WssM_MainBus.WssM_MainEcuModeSts);
    WssM_Main_Read_WssM_MainIgnOnOffSts(&WssM_MainBus.WssM_MainIgnOnOffSts);
    WssM_Main_Read_WssM_MainIgnEdgeSts(&WssM_MainBus.WssM_MainIgnEdgeSts);
    WssM_Main_Read_WssM_MainVBatt1Mon(&WssM_MainBus.WssM_MainVBatt1Mon);
    WssM_Main_Read_WssM_MainDiagClrSrs(&WssM_MainBus.WssM_MainDiagClrSrs);

    /* Process */
    WssM_Main_Proc(&WssM_MainBus);
    
    /* Output */
    WssM_Main_Write_WssM_MainWssMonData(&WssM_MainBus.WssM_MainWssMonData);
    /*==============================================================================
    * Members of structure WssM_MainWssMonData 
     : WssM_MainWssMonData.WssM_FlOpen_Err;
     : WssM_MainWssMonData.WssM_FlShort_Err;
     : WssM_MainWssMonData.WssM_FlOverTemp_Err;
     : WssM_MainWssMonData.WssM_FlLeakage_Err;
     : WssM_MainWssMonData.WssM_FrOpen_Err;
     : WssM_MainWssMonData.WssM_FrShort_Err;
     : WssM_MainWssMonData.WssM_FrOverTemp_Err;
     : WssM_MainWssMonData.WssM_FrLeakage_Err;
     : WssM_MainWssMonData.WssM_RlOpen_Err;
     : WssM_MainWssMonData.WssM_RlShort_Err;
     : WssM_MainWssMonData.WssM_RlOverTemp_Err;
     : WssM_MainWssMonData.WssM_RlLeakage_Err;
     : WssM_MainWssMonData.WssM_RrOpen_Err;
     : WssM_MainWssMonData.WssM_RrShort_Err;
     : WssM_MainWssMonData.WssM_RrOverTemp_Err;
     : WssM_MainWssMonData.WssM_RrLeakage_Err;
     : WssM_MainWssMonData.WssM_Asic_Comm_Err;
     : WssM_MainWssMonData.WssM_Inhibit_FL;
     : WssM_MainWssMonData.WssM_Inhibit_FR;
     : WssM_MainWssMonData.WssM_Inhibit_RL;
     : WssM_MainWssMonData.WssM_Inhibit_RR;
     =============================================================================*/
    

    WssM_Main_Timer_Elapsed = STM0_TIM0.U - WssM_Main_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define WSSM_MAIN_STOP_SEC_CODE
#include "WssM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
