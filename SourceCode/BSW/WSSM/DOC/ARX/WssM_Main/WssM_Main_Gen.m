WssM_MainAchSysPwrAsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_comm_too_long'
    'Ach_Asic_comm_too_short'
    'Ach_Asic_comm_wrong_add'
    'Ach_Asic_comm_wrong_fcnt'
    'Ach_Asic_comm_wrong_crc'
    };
WssM_MainAchSysPwrAsicInfo = CreateBus(WssM_MainAchSysPwrAsicInfo, DeList);
clear DeList;

WssM_MainAchWssPort0AsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_CH0_WSIRSDR3_NO_FAULT'
    'Ach_Asic_CH0_WSIRSDR3_WHEEL_SPEED_DECODER_DATA'
    'Ach_Asic_CH0_WSIRSDR3_STANDSTILL'
    'Ach_Asic_CH0_WSIRSDR3_LATCH_D0'
    'Ach_Asic_CH0_WSIRSDR3_NODATA'
    'Ach_Asic_CH0_WSIRSDR3_INVALID'
    'Ach_Asic_CH0_WSIRSDR3_OPEN'
    'Ach_Asic_CH0_WSIRSDR3_STB'
    'Ach_Asic_CH0_WSIRSDR3_STG'
    'Ach_Asic_CH0_WSIRSDR3_WSI_OT'
    'Ach_Asic_CH0_WSIRSDR3_CURRENT_HI'
    };
WssM_MainAchWssPort0AsicInfo = CreateBus(WssM_MainAchWssPort0AsicInfo, DeList);
clear DeList;

WssM_MainAchWssPort1AsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_CH1_WSIRSDR3_NO_FAULT'
    'Ach_Asic_CH1_WSIRSDR3_WHEEL_SPEED_DECODER_DATA'
    'Ach_Asic_CH1_WSIRSDR3_STANDSTILL'
    'Ach_Asic_CH1_WSIRSDR3_LATCH_D0'
    'Ach_Asic_CH1_WSIRSDR3_NODATA'
    'Ach_Asic_CH1_WSIRSDR3_INVALID'
    'Ach_Asic_CH1_WSIRSDR3_OPEN'
    'Ach_Asic_CH1_WSIRSDR3_STB'
    'Ach_Asic_CH1_WSIRSDR3_STG'
    'Ach_Asic_CH1_WSIRSDR3_WSI_OT'
    'Ach_Asic_CH1_WSIRSDR3_CURRENT_HI'
    };
WssM_MainAchWssPort1AsicInfo = CreateBus(WssM_MainAchWssPort1AsicInfo, DeList);
clear DeList;

WssM_MainAchWssPort2AsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_CH2_WSIRSDR3_NO_FAULT'
    'Ach_Asic_CH2_WSIRSDR3_WHEEL_SPEED_DECODER_DATA'
    'Ach_Asic_CH2_WSIRSDR3_STANDSTILL'
    'Ach_Asic_CH2_WSIRSDR3_LATCH_D0'
    'Ach_Asic_CH2_WSIRSDR3_NODATA'
    'Ach_Asic_CH2_WSIRSDR3_INVALID'
    'Ach_Asic_CH2_WSIRSDR3_OPEN'
    'Ach_Asic_CH2_WSIRSDR3_STB'
    'Ach_Asic_CH2_WSIRSDR3_STG'
    'Ach_Asic_CH2_WSIRSDR3_WSI_OT'
    'Ach_Asic_CH2_WSIRSDR3_CURRENT_HI'
    };
WssM_MainAchWssPort2AsicInfo = CreateBus(WssM_MainAchWssPort2AsicInfo, DeList);
clear DeList;

WssM_MainAchWssPort3AsicInfo = Simulink.Bus;
DeList={
    'Ach_Asic_CH3_WSIRSDR3_NO_FAULT'
    'Ach_Asic_CH3_WSIRSDR3_WHEEL_SPEED_DECODER_DATA'
    'Ach_Asic_CH3_WSIRSDR3_STANDSTILL'
    'Ach_Asic_CH3_WSIRSDR3_LATCH_D0'
    'Ach_Asic_CH3_WSIRSDR3_NODATA'
    'Ach_Asic_CH3_WSIRSDR3_INVALID'
    'Ach_Asic_CH3_WSIRSDR3_OPEN'
    'Ach_Asic_CH3_WSIRSDR3_STB'
    'Ach_Asic_CH3_WSIRSDR3_STG'
    'Ach_Asic_CH3_WSIRSDR3_WSI_OT'
    'Ach_Asic_CH3_WSIRSDR3_CURRENT_HI'
    };
WssM_MainAchWssPort3AsicInfo = CreateBus(WssM_MainAchWssPort3AsicInfo, DeList);
clear DeList;

WssM_MainWssSpeedOut = Simulink.Bus;
DeList={
    'WssMin'
    };
WssM_MainWssSpeedOut = CreateBus(WssM_MainWssSpeedOut, DeList);
clear DeList;

WssM_MainAchAsicInvalid = Simulink.Bus;
DeList={'WssM_MainAchAsicInvalid'};
WssM_MainAchAsicInvalid = CreateBus(WssM_MainAchAsicInvalid, DeList);
clear DeList;

WssM_MainEcuModeSts = Simulink.Bus;
DeList={'WssM_MainEcuModeSts'};
WssM_MainEcuModeSts = CreateBus(WssM_MainEcuModeSts, DeList);
clear DeList;

WssM_MainIgnOnOffSts = Simulink.Bus;
DeList={'WssM_MainIgnOnOffSts'};
WssM_MainIgnOnOffSts = CreateBus(WssM_MainIgnOnOffSts, DeList);
clear DeList;

WssM_MainIgnEdgeSts = Simulink.Bus;
DeList={'WssM_MainIgnEdgeSts'};
WssM_MainIgnEdgeSts = CreateBus(WssM_MainIgnEdgeSts, DeList);
clear DeList;

WssM_MainVBatt1Mon = Simulink.Bus;
DeList={'WssM_MainVBatt1Mon'};
WssM_MainVBatt1Mon = CreateBus(WssM_MainVBatt1Mon, DeList);
clear DeList;

WssM_MainDiagClrSrs = Simulink.Bus;
DeList={'WssM_MainDiagClrSrs'};
WssM_MainDiagClrSrs = CreateBus(WssM_MainDiagClrSrs, DeList);
clear DeList;

WssM_MainWssMonData = Simulink.Bus;
DeList={
    'WssM_FlOpen_Err'
    'WssM_FlShort_Err'
    'WssM_FlOverTemp_Err'
    'WssM_FlLeakage_Err'
    'WssM_FrOpen_Err'
    'WssM_FrShort_Err'
    'WssM_FrOverTemp_Err'
    'WssM_FrLeakage_Err'
    'WssM_RlOpen_Err'
    'WssM_RlShort_Err'
    'WssM_RlOverTemp_Err'
    'WssM_RlLeakage_Err'
    'WssM_RrOpen_Err'
    'WssM_RrShort_Err'
    'WssM_RrOverTemp_Err'
    'WssM_RrLeakage_Err'
    'WssM_Asic_Comm_Err'
    'WssM_Inhibit_FL'
    'WssM_Inhibit_FR'
    'WssM_Inhibit_RL'
    'WssM_Inhibit_RR'
    };
WssM_MainWssMonData = CreateBus(WssM_MainWssMonData, DeList);
clear DeList;

