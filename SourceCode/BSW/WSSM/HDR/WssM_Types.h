/**
 * @defgroup WssM_Types WssM_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        WssM_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef WSSM_TYPES_H_
#define WSSM_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Ach_InputAchSysPwrAsicInfo_t WssM_MainAchSysPwrAsicInfo;
    Ach_InputAchWssPort0AsicInfo_t WssM_MainAchWssPort0AsicInfo;
    Ach_InputAchWssPort1AsicInfo_t WssM_MainAchWssPort1AsicInfo;
    Ach_InputAchWssPort2AsicInfo_t WssM_MainAchWssPort2AsicInfo;
    Ach_InputAchWssPort3AsicInfo_t WssM_MainAchWssPort3AsicInfo;
    Wss_SenWssSpeedOut_t WssM_MainWssSpeedOut;
    Ach_InputAchAsicInvalidInfo_t WssM_MainAchAsicInvalid;
    Mom_HndlrEcuModeSts_t WssM_MainEcuModeSts;
    Prly_HndlrIgnOnOffSts_t WssM_MainIgnOnOffSts;
    Prly_HndlrIgnEdgeSts_t WssM_MainIgnEdgeSts;
    Ioc_InputSR1msVBatt1Mon_t WssM_MainVBatt1Mon;
    Diag_HndlrDiagClr_t WssM_MainDiagClrSrs;

/* Output Data Element */
    WssM_MainWssMonData_t WssM_MainWssMonData;
}WssM_Main_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* WSSM_TYPES_H_ */
/** @} */
