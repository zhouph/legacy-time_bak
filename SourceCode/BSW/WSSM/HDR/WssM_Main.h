/**
 * @defgroup WssM_Main WssM_Main
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        WssM_Main.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef WSSM_MAIN_H_
#define WSSM_MAIN_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "WssM_Types.h"
#include "WssM_Cfg.h"
#include "WssM_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define WSSM_MAIN_MODULE_ID      (0)
 #define WSSM_MAIN_MAJOR_VERSION  (2)
 #define WSSM_MAIN_MINOR_VERSION  (0)
 #define WSSM_MAIN_PATCH_VERSION  (0)
 #define WSSM_MAIN_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern WssM_Main_HdrBusType WssM_MainBus;

/* Version Info */
extern const SwcVersionInfo_t WssM_MainVersionInfo;

/* Input Data Element */
extern Ach_InputAchSysPwrAsicInfo_t WssM_MainAchSysPwrAsicInfo;
extern Ach_InputAchWssPort0AsicInfo_t WssM_MainAchWssPort0AsicInfo;
extern Ach_InputAchWssPort1AsicInfo_t WssM_MainAchWssPort1AsicInfo;
extern Ach_InputAchWssPort2AsicInfo_t WssM_MainAchWssPort2AsicInfo;
extern Ach_InputAchWssPort3AsicInfo_t WssM_MainAchWssPort3AsicInfo;
extern Wss_SenWssSpeedOut_t WssM_MainWssSpeedOut;
extern Ach_InputAchAsicInvalidInfo_t WssM_MainAchAsicInvalid;
extern Mom_HndlrEcuModeSts_t WssM_MainEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t WssM_MainIgnOnOffSts;
extern Prly_HndlrIgnEdgeSts_t WssM_MainIgnEdgeSts;
extern Ioc_InputSR1msVBatt1Mon_t WssM_MainVBatt1Mon;
extern Diag_HndlrDiagClr_t WssM_MainDiagClrSrs;

/* Output Data Element */
extern WssM_MainWssMonData_t WssM_MainWssMonData;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void WssM_Main_Init(void);
extern void WssM_Main(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* WSSM_MAIN_H_ */
/** @} */
