#include "unity.h"
#include "unity_fixture.h"
#include "WssM_Main.h"
#include "WssM_Main_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Haluint8 UtInput_WssM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_long[MAX_STEP] = WSSM_MAINACHSYSPWRASICINFO_ACH_ASIC_COMM_TOO_LONG;
const Haluint8 UtInput_WssM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_short[MAX_STEP] = WSSM_MAINACHSYSPWRASICINFO_ACH_ASIC_COMM_TOO_SHORT;
const Haluint8 UtInput_WssM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_add[MAX_STEP] = WSSM_MAINACHSYSPWRASICINFO_ACH_ASIC_COMM_WRONG_ADD;
const Haluint8 UtInput_WssM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_fcnt[MAX_STEP] = WSSM_MAINACHSYSPWRASICINFO_ACH_ASIC_COMM_WRONG_FCNT;
const Haluint8 UtInput_WssM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_crc[MAX_STEP] = WSSM_MAINACHSYSPWRASICINFO_ACH_ASIC_COMM_WRONG_CRC;
const Haluint8 UtInput_WssM_MainAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_NO_FAULT[MAX_STEP] = WSSM_MAINACHWSSPORT0ASICINFO_ACH_ASIC_CH0_WSIRSDR3_NO_FAULT;
const Haluint16 UtInput_WssM_MainAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_WHEEL_SPEED_DECODER_DATA[MAX_STEP] = WSSM_MAINACHWSSPORT0ASICINFO_ACH_ASIC_CH0_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
const Haluint8 UtInput_WssM_MainAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_STANDSTILL[MAX_STEP] = WSSM_MAINACHWSSPORT0ASICINFO_ACH_ASIC_CH0_WSIRSDR3_STANDSTILL;
const Haluint8 UtInput_WssM_MainAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_LATCH_D0[MAX_STEP] = WSSM_MAINACHWSSPORT0ASICINFO_ACH_ASIC_CH0_WSIRSDR3_LATCH_D0;
const Haluint8 UtInput_WssM_MainAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_NODATA[MAX_STEP] = WSSM_MAINACHWSSPORT0ASICINFO_ACH_ASIC_CH0_WSIRSDR3_NODATA;
const Haluint8 UtInput_WssM_MainAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_INVALID[MAX_STEP] = WSSM_MAINACHWSSPORT0ASICINFO_ACH_ASIC_CH0_WSIRSDR3_INVALID;
const Haluint8 UtInput_WssM_MainAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_OPEN[MAX_STEP] = WSSM_MAINACHWSSPORT0ASICINFO_ACH_ASIC_CH0_WSIRSDR3_OPEN;
const Haluint8 UtInput_WssM_MainAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_STB[MAX_STEP] = WSSM_MAINACHWSSPORT0ASICINFO_ACH_ASIC_CH0_WSIRSDR3_STB;
const Haluint8 UtInput_WssM_MainAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_STG[MAX_STEP] = WSSM_MAINACHWSSPORT0ASICINFO_ACH_ASIC_CH0_WSIRSDR3_STG;
const Haluint8 UtInput_WssM_MainAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_WSI_OT[MAX_STEP] = WSSM_MAINACHWSSPORT0ASICINFO_ACH_ASIC_CH0_WSIRSDR3_WSI_OT;
const Haluint8 UtInput_WssM_MainAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_CURRENT_HI[MAX_STEP] = WSSM_MAINACHWSSPORT0ASICINFO_ACH_ASIC_CH0_WSIRSDR3_CURRENT_HI;
const Haluint8 UtInput_WssM_MainAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_NO_FAULT[MAX_STEP] = WSSM_MAINACHWSSPORT1ASICINFO_ACH_ASIC_CH1_WSIRSDR3_NO_FAULT;
const Haluint16 UtInput_WssM_MainAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_WHEEL_SPEED_DECODER_DATA[MAX_STEP] = WSSM_MAINACHWSSPORT1ASICINFO_ACH_ASIC_CH1_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
const Haluint8 UtInput_WssM_MainAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_STANDSTILL[MAX_STEP] = WSSM_MAINACHWSSPORT1ASICINFO_ACH_ASIC_CH1_WSIRSDR3_STANDSTILL;
const Haluint8 UtInput_WssM_MainAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_LATCH_D0[MAX_STEP] = WSSM_MAINACHWSSPORT1ASICINFO_ACH_ASIC_CH1_WSIRSDR3_LATCH_D0;
const Haluint8 UtInput_WssM_MainAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_NODATA[MAX_STEP] = WSSM_MAINACHWSSPORT1ASICINFO_ACH_ASIC_CH1_WSIRSDR3_NODATA;
const Haluint8 UtInput_WssM_MainAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_INVALID[MAX_STEP] = WSSM_MAINACHWSSPORT1ASICINFO_ACH_ASIC_CH1_WSIRSDR3_INVALID;
const Haluint8 UtInput_WssM_MainAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_OPEN[MAX_STEP] = WSSM_MAINACHWSSPORT1ASICINFO_ACH_ASIC_CH1_WSIRSDR3_OPEN;
const Haluint8 UtInput_WssM_MainAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_STB[MAX_STEP] = WSSM_MAINACHWSSPORT1ASICINFO_ACH_ASIC_CH1_WSIRSDR3_STB;
const Haluint8 UtInput_WssM_MainAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_STG[MAX_STEP] = WSSM_MAINACHWSSPORT1ASICINFO_ACH_ASIC_CH1_WSIRSDR3_STG;
const Haluint8 UtInput_WssM_MainAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_WSI_OT[MAX_STEP] = WSSM_MAINACHWSSPORT1ASICINFO_ACH_ASIC_CH1_WSIRSDR3_WSI_OT;
const Haluint8 UtInput_WssM_MainAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_CURRENT_HI[MAX_STEP] = WSSM_MAINACHWSSPORT1ASICINFO_ACH_ASIC_CH1_WSIRSDR3_CURRENT_HI;
const Haluint8 UtInput_WssM_MainAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_NO_FAULT[MAX_STEP] = WSSM_MAINACHWSSPORT2ASICINFO_ACH_ASIC_CH2_WSIRSDR3_NO_FAULT;
const Haluint16 UtInput_WssM_MainAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_WHEEL_SPEED_DECODER_DATA[MAX_STEP] = WSSM_MAINACHWSSPORT2ASICINFO_ACH_ASIC_CH2_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
const Haluint8 UtInput_WssM_MainAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_STANDSTILL[MAX_STEP] = WSSM_MAINACHWSSPORT2ASICINFO_ACH_ASIC_CH2_WSIRSDR3_STANDSTILL;
const Haluint8 UtInput_WssM_MainAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_LATCH_D0[MAX_STEP] = WSSM_MAINACHWSSPORT2ASICINFO_ACH_ASIC_CH2_WSIRSDR3_LATCH_D0;
const Haluint8 UtInput_WssM_MainAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_NODATA[MAX_STEP] = WSSM_MAINACHWSSPORT2ASICINFO_ACH_ASIC_CH2_WSIRSDR3_NODATA;
const Haluint8 UtInput_WssM_MainAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_INVALID[MAX_STEP] = WSSM_MAINACHWSSPORT2ASICINFO_ACH_ASIC_CH2_WSIRSDR3_INVALID;
const Haluint8 UtInput_WssM_MainAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_OPEN[MAX_STEP] = WSSM_MAINACHWSSPORT2ASICINFO_ACH_ASIC_CH2_WSIRSDR3_OPEN;
const Haluint8 UtInput_WssM_MainAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_STB[MAX_STEP] = WSSM_MAINACHWSSPORT2ASICINFO_ACH_ASIC_CH2_WSIRSDR3_STB;
const Haluint8 UtInput_WssM_MainAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_STG[MAX_STEP] = WSSM_MAINACHWSSPORT2ASICINFO_ACH_ASIC_CH2_WSIRSDR3_STG;
const Haluint8 UtInput_WssM_MainAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_WSI_OT[MAX_STEP] = WSSM_MAINACHWSSPORT2ASICINFO_ACH_ASIC_CH2_WSIRSDR3_WSI_OT;
const Haluint8 UtInput_WssM_MainAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_CURRENT_HI[MAX_STEP] = WSSM_MAINACHWSSPORT2ASICINFO_ACH_ASIC_CH2_WSIRSDR3_CURRENT_HI;
const Haluint8 UtInput_WssM_MainAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_NO_FAULT[MAX_STEP] = WSSM_MAINACHWSSPORT3ASICINFO_ACH_ASIC_CH3_WSIRSDR3_NO_FAULT;
const Haluint16 UtInput_WssM_MainAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_WHEEL_SPEED_DECODER_DATA[MAX_STEP] = WSSM_MAINACHWSSPORT3ASICINFO_ACH_ASIC_CH3_WSIRSDR3_WHEEL_SPEED_DECODER_DATA;
const Haluint8 UtInput_WssM_MainAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_STANDSTILL[MAX_STEP] = WSSM_MAINACHWSSPORT3ASICINFO_ACH_ASIC_CH3_WSIRSDR3_STANDSTILL;
const Haluint8 UtInput_WssM_MainAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_LATCH_D0[MAX_STEP] = WSSM_MAINACHWSSPORT3ASICINFO_ACH_ASIC_CH3_WSIRSDR3_LATCH_D0;
const Haluint8 UtInput_WssM_MainAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_NODATA[MAX_STEP] = WSSM_MAINACHWSSPORT3ASICINFO_ACH_ASIC_CH3_WSIRSDR3_NODATA;
const Haluint8 UtInput_WssM_MainAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_INVALID[MAX_STEP] = WSSM_MAINACHWSSPORT3ASICINFO_ACH_ASIC_CH3_WSIRSDR3_INVALID;
const Haluint8 UtInput_WssM_MainAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_OPEN[MAX_STEP] = WSSM_MAINACHWSSPORT3ASICINFO_ACH_ASIC_CH3_WSIRSDR3_OPEN;
const Haluint8 UtInput_WssM_MainAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_STB[MAX_STEP] = WSSM_MAINACHWSSPORT3ASICINFO_ACH_ASIC_CH3_WSIRSDR3_STB;
const Haluint8 UtInput_WssM_MainAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_STG[MAX_STEP] = WSSM_MAINACHWSSPORT3ASICINFO_ACH_ASIC_CH3_WSIRSDR3_STG;
const Haluint8 UtInput_WssM_MainAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_WSI_OT[MAX_STEP] = WSSM_MAINACHWSSPORT3ASICINFO_ACH_ASIC_CH3_WSIRSDR3_WSI_OT;
const Haluint8 UtInput_WssM_MainAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_CURRENT_HI[MAX_STEP] = WSSM_MAINACHWSSPORT3ASICINFO_ACH_ASIC_CH3_WSIRSDR3_CURRENT_HI;
const Saluint32 UtInput_WssM_MainWssSpeedOut_WssMin[MAX_STEP] = WSSM_MAINWSSSPEEDOUT_WSSMIN;
const Ach_InputAchAsicInvalidInfo_t UtInput_WssM_MainAchAsicInvalid[MAX_STEP] = WSSM_MAINACHASICINVALID;
const Mom_HndlrEcuModeSts_t UtInput_WssM_MainEcuModeSts[MAX_STEP] = WSSM_MAINECUMODESTS;
const Prly_HndlrIgnOnOffSts_t UtInput_WssM_MainIgnOnOffSts[MAX_STEP] = WSSM_MAINIGNONOFFSTS;
const Prly_HndlrIgnEdgeSts_t UtInput_WssM_MainIgnEdgeSts[MAX_STEP] = WSSM_MAINIGNEDGESTS;
const Ioc_InputSR1msVBatt1Mon_t UtInput_WssM_MainVBatt1Mon[MAX_STEP] = WSSM_MAINVBATT1MON;
const Diag_HndlrDiagClr_t UtInput_WssM_MainDiagClrSrs[MAX_STEP] = WSSM_MAINDIAGCLRSRS;

const Saluint8 UtExpected_WssM_MainWssMonData_WssM_FlOpen_Err[MAX_STEP] = WSSM_MAINWSSMONDATA_WSSM_FLOPEN_ERR;
const Saluint8 UtExpected_WssM_MainWssMonData_WssM_FlShort_Err[MAX_STEP] = WSSM_MAINWSSMONDATA_WSSM_FLSHORT_ERR;
const Saluint8 UtExpected_WssM_MainWssMonData_WssM_FlOverTemp_Err[MAX_STEP] = WSSM_MAINWSSMONDATA_WSSM_FLOVERTEMP_ERR;
const Saluint8 UtExpected_WssM_MainWssMonData_WssM_FlLeakage_Err[MAX_STEP] = WSSM_MAINWSSMONDATA_WSSM_FLLEAKAGE_ERR;
const Saluint8 UtExpected_WssM_MainWssMonData_WssM_FrOpen_Err[MAX_STEP] = WSSM_MAINWSSMONDATA_WSSM_FROPEN_ERR;
const Saluint8 UtExpected_WssM_MainWssMonData_WssM_FrShort_Err[MAX_STEP] = WSSM_MAINWSSMONDATA_WSSM_FRSHORT_ERR;
const Saluint8 UtExpected_WssM_MainWssMonData_WssM_FrOverTemp_Err[MAX_STEP] = WSSM_MAINWSSMONDATA_WSSM_FROVERTEMP_ERR;
const Saluint8 UtExpected_WssM_MainWssMonData_WssM_FrLeakage_Err[MAX_STEP] = WSSM_MAINWSSMONDATA_WSSM_FRLEAKAGE_ERR;
const Saluint8 UtExpected_WssM_MainWssMonData_WssM_RlOpen_Err[MAX_STEP] = WSSM_MAINWSSMONDATA_WSSM_RLOPEN_ERR;
const Saluint8 UtExpected_WssM_MainWssMonData_WssM_RlShort_Err[MAX_STEP] = WSSM_MAINWSSMONDATA_WSSM_RLSHORT_ERR;
const Saluint8 UtExpected_WssM_MainWssMonData_WssM_RlOverTemp_Err[MAX_STEP] = WSSM_MAINWSSMONDATA_WSSM_RLOVERTEMP_ERR;
const Saluint8 UtExpected_WssM_MainWssMonData_WssM_RlLeakage_Err[MAX_STEP] = WSSM_MAINWSSMONDATA_WSSM_RLLEAKAGE_ERR;
const Saluint8 UtExpected_WssM_MainWssMonData_WssM_RrOpen_Err[MAX_STEP] = WSSM_MAINWSSMONDATA_WSSM_RROPEN_ERR;
const Saluint8 UtExpected_WssM_MainWssMonData_WssM_RrShort_Err[MAX_STEP] = WSSM_MAINWSSMONDATA_WSSM_RRSHORT_ERR;
const Saluint8 UtExpected_WssM_MainWssMonData_WssM_RrOverTemp_Err[MAX_STEP] = WSSM_MAINWSSMONDATA_WSSM_RROVERTEMP_ERR;
const Saluint8 UtExpected_WssM_MainWssMonData_WssM_RrLeakage_Err[MAX_STEP] = WSSM_MAINWSSMONDATA_WSSM_RRLEAKAGE_ERR;
const Saluint8 UtExpected_WssM_MainWssMonData_WssM_Asic_Comm_Err[MAX_STEP] = WSSM_MAINWSSMONDATA_WSSM_ASIC_COMM_ERR;
const Saluint8 UtExpected_WssM_MainWssMonData_WssM_Inhibit_FL[MAX_STEP] = WSSM_MAINWSSMONDATA_WSSM_INHIBIT_FL;
const Saluint8 UtExpected_WssM_MainWssMonData_WssM_Inhibit_FR[MAX_STEP] = WSSM_MAINWSSMONDATA_WSSM_INHIBIT_FR;
const Saluint8 UtExpected_WssM_MainWssMonData_WssM_Inhibit_RL[MAX_STEP] = WSSM_MAINWSSMONDATA_WSSM_INHIBIT_RL;
const Saluint8 UtExpected_WssM_MainWssMonData_WssM_Inhibit_RR[MAX_STEP] = WSSM_MAINWSSMONDATA_WSSM_INHIBIT_RR;



TEST_GROUP(WssM_Main);
TEST_SETUP(WssM_Main)
{
    WssM_Main_Init();
}

TEST_TEAR_DOWN(WssM_Main)
{   /* Postcondition */

}

TEST(WssM_Main, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_long = UtInput_WssM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_long[i];
        WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_too_short = UtInput_WssM_MainAchSysPwrAsicInfo_Ach_Asic_comm_too_short[i];
        WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_add = UtInput_WssM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_add[i];
        WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_fcnt = UtInput_WssM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_fcnt[i];
        WssM_MainAchSysPwrAsicInfo.Ach_Asic_comm_wrong_crc = UtInput_WssM_MainAchSysPwrAsicInfo_Ach_Asic_comm_wrong_crc[i];
        WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NO_FAULT = UtInput_WssM_MainAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_NO_FAULT[i];
        WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WHEEL_SPEED_DECODER_DATA = UtInput_WssM_MainAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_WHEEL_SPEED_DECODER_DATA[i];
        WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STANDSTILL = UtInput_WssM_MainAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_STANDSTILL[i];
        WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_LATCH_D0 = UtInput_WssM_MainAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_LATCH_D0[i];
        WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_NODATA = UtInput_WssM_MainAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_NODATA[i];
        WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_INVALID = UtInput_WssM_MainAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_INVALID[i];
        WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_OPEN = UtInput_WssM_MainAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_OPEN[i];
        WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STB = UtInput_WssM_MainAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_STB[i];
        WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_STG = UtInput_WssM_MainAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_STG[i];
        WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_WSI_OT = UtInput_WssM_MainAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_WSI_OT[i];
        WssM_MainAchWssPort0AsicInfo.Ach_Asic_CH0_WSIRSDR3_CURRENT_HI = UtInput_WssM_MainAchWssPort0AsicInfo_Ach_Asic_CH0_WSIRSDR3_CURRENT_HI[i];
        WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NO_FAULT = UtInput_WssM_MainAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_NO_FAULT[i];
        WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WHEEL_SPEED_DECODER_DATA = UtInput_WssM_MainAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_WHEEL_SPEED_DECODER_DATA[i];
        WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STANDSTILL = UtInput_WssM_MainAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_STANDSTILL[i];
        WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_LATCH_D0 = UtInput_WssM_MainAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_LATCH_D0[i];
        WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_NODATA = UtInput_WssM_MainAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_NODATA[i];
        WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_INVALID = UtInput_WssM_MainAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_INVALID[i];
        WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_OPEN = UtInput_WssM_MainAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_OPEN[i];
        WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STB = UtInput_WssM_MainAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_STB[i];
        WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_STG = UtInput_WssM_MainAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_STG[i];
        WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_WSI_OT = UtInput_WssM_MainAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_WSI_OT[i];
        WssM_MainAchWssPort1AsicInfo.Ach_Asic_CH1_WSIRSDR3_CURRENT_HI = UtInput_WssM_MainAchWssPort1AsicInfo_Ach_Asic_CH1_WSIRSDR3_CURRENT_HI[i];
        WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NO_FAULT = UtInput_WssM_MainAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_NO_FAULT[i];
        WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WHEEL_SPEED_DECODER_DATA = UtInput_WssM_MainAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_WHEEL_SPEED_DECODER_DATA[i];
        WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STANDSTILL = UtInput_WssM_MainAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_STANDSTILL[i];
        WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_LATCH_D0 = UtInput_WssM_MainAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_LATCH_D0[i];
        WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_NODATA = UtInput_WssM_MainAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_NODATA[i];
        WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_INVALID = UtInput_WssM_MainAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_INVALID[i];
        WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_OPEN = UtInput_WssM_MainAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_OPEN[i];
        WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STB = UtInput_WssM_MainAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_STB[i];
        WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_STG = UtInput_WssM_MainAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_STG[i];
        WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_WSI_OT = UtInput_WssM_MainAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_WSI_OT[i];
        WssM_MainAchWssPort2AsicInfo.Ach_Asic_CH2_WSIRSDR3_CURRENT_HI = UtInput_WssM_MainAchWssPort2AsicInfo_Ach_Asic_CH2_WSIRSDR3_CURRENT_HI[i];
        WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NO_FAULT = UtInput_WssM_MainAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_NO_FAULT[i];
        WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WHEEL_SPEED_DECODER_DATA = UtInput_WssM_MainAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_WHEEL_SPEED_DECODER_DATA[i];
        WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STANDSTILL = UtInput_WssM_MainAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_STANDSTILL[i];
        WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_LATCH_D0 = UtInput_WssM_MainAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_LATCH_D0[i];
        WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_NODATA = UtInput_WssM_MainAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_NODATA[i];
        WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_INVALID = UtInput_WssM_MainAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_INVALID[i];
        WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_OPEN = UtInput_WssM_MainAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_OPEN[i];
        WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STB = UtInput_WssM_MainAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_STB[i];
        WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_STG = UtInput_WssM_MainAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_STG[i];
        WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_WSI_OT = UtInput_WssM_MainAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_WSI_OT[i];
        WssM_MainAchWssPort3AsicInfo.Ach_Asic_CH3_WSIRSDR3_CURRENT_HI = UtInput_WssM_MainAchWssPort3AsicInfo_Ach_Asic_CH3_WSIRSDR3_CURRENT_HI[i];
        WssM_MainWssSpeedOut.WssMin = UtInput_WssM_MainWssSpeedOut_WssMin[i];
        WssM_MainAchAsicInvalid = UtInput_WssM_MainAchAsicInvalid[i];
        WssM_MainEcuModeSts = UtInput_WssM_MainEcuModeSts[i];
        WssM_MainIgnOnOffSts = UtInput_WssM_MainIgnOnOffSts[i];
        WssM_MainIgnEdgeSts = UtInput_WssM_MainIgnEdgeSts[i];
        WssM_MainVBatt1Mon = UtInput_WssM_MainVBatt1Mon[i];
        WssM_MainDiagClrSrs = UtInput_WssM_MainDiagClrSrs[i];

        WssM_Main();

        TEST_ASSERT_EQUAL(WssM_MainWssMonData.WssM_FlOpen_Err, UtExpected_WssM_MainWssMonData_WssM_FlOpen_Err[i]);
        TEST_ASSERT_EQUAL(WssM_MainWssMonData.WssM_FlShort_Err, UtExpected_WssM_MainWssMonData_WssM_FlShort_Err[i]);
        TEST_ASSERT_EQUAL(WssM_MainWssMonData.WssM_FlOverTemp_Err, UtExpected_WssM_MainWssMonData_WssM_FlOverTemp_Err[i]);
        TEST_ASSERT_EQUAL(WssM_MainWssMonData.WssM_FlLeakage_Err, UtExpected_WssM_MainWssMonData_WssM_FlLeakage_Err[i]);
        TEST_ASSERT_EQUAL(WssM_MainWssMonData.WssM_FrOpen_Err, UtExpected_WssM_MainWssMonData_WssM_FrOpen_Err[i]);
        TEST_ASSERT_EQUAL(WssM_MainWssMonData.WssM_FrShort_Err, UtExpected_WssM_MainWssMonData_WssM_FrShort_Err[i]);
        TEST_ASSERT_EQUAL(WssM_MainWssMonData.WssM_FrOverTemp_Err, UtExpected_WssM_MainWssMonData_WssM_FrOverTemp_Err[i]);
        TEST_ASSERT_EQUAL(WssM_MainWssMonData.WssM_FrLeakage_Err, UtExpected_WssM_MainWssMonData_WssM_FrLeakage_Err[i]);
        TEST_ASSERT_EQUAL(WssM_MainWssMonData.WssM_RlOpen_Err, UtExpected_WssM_MainWssMonData_WssM_RlOpen_Err[i]);
        TEST_ASSERT_EQUAL(WssM_MainWssMonData.WssM_RlShort_Err, UtExpected_WssM_MainWssMonData_WssM_RlShort_Err[i]);
        TEST_ASSERT_EQUAL(WssM_MainWssMonData.WssM_RlOverTemp_Err, UtExpected_WssM_MainWssMonData_WssM_RlOverTemp_Err[i]);
        TEST_ASSERT_EQUAL(WssM_MainWssMonData.WssM_RlLeakage_Err, UtExpected_WssM_MainWssMonData_WssM_RlLeakage_Err[i]);
        TEST_ASSERT_EQUAL(WssM_MainWssMonData.WssM_RrOpen_Err, UtExpected_WssM_MainWssMonData_WssM_RrOpen_Err[i]);
        TEST_ASSERT_EQUAL(WssM_MainWssMonData.WssM_RrShort_Err, UtExpected_WssM_MainWssMonData_WssM_RrShort_Err[i]);
        TEST_ASSERT_EQUAL(WssM_MainWssMonData.WssM_RrOverTemp_Err, UtExpected_WssM_MainWssMonData_WssM_RrOverTemp_Err[i]);
        TEST_ASSERT_EQUAL(WssM_MainWssMonData.WssM_RrLeakage_Err, UtExpected_WssM_MainWssMonData_WssM_RrLeakage_Err[i]);
        TEST_ASSERT_EQUAL(WssM_MainWssMonData.WssM_Asic_Comm_Err, UtExpected_WssM_MainWssMonData_WssM_Asic_Comm_Err[i]);
        TEST_ASSERT_EQUAL(WssM_MainWssMonData.WssM_Inhibit_FL, UtExpected_WssM_MainWssMonData_WssM_Inhibit_FL[i]);
        TEST_ASSERT_EQUAL(WssM_MainWssMonData.WssM_Inhibit_FR, UtExpected_WssM_MainWssMonData_WssM_Inhibit_FR[i]);
        TEST_ASSERT_EQUAL(WssM_MainWssMonData.WssM_Inhibit_RL, UtExpected_WssM_MainWssMonData_WssM_Inhibit_RL[i]);
        TEST_ASSERT_EQUAL(WssM_MainWssMonData.WssM_Inhibit_RR, UtExpected_WssM_MainWssMonData_WssM_Inhibit_RR[i]);
    }
}

TEST_GROUP_RUNNER(WssM_Main)
{
    RUN_TEST_CASE(WssM_Main, All);
}
