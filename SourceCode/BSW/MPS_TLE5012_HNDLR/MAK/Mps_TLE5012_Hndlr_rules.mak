# \file
#
# \brief Mps_TLE5012_Hndlr
#
# This file contains the implementation of the SWC
# module Mps_TLE5012_Hndlr.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Mps_TLE5012_Hndlr_src

Mps_TLE5012_Hndlr_src_FILES        += $(Mps_TLE5012_Hndlr_SRC_PATH)\Mps_TLE5012_Hndlr_1ms.c
Mps_TLE5012_Hndlr_src_FILES        += $(Mps_TLE5012_Hndlr_IFA_PATH)\Mps_TLE5012_Hndlr_1ms_Ifa.c
Mps_TLE5012_Hndlr_src_FILES        += $(Mps_TLE5012_Hndlr_SRC_PATH)\Mps_TLE5012_Hndlr_50us.c
Mps_TLE5012_Hndlr_src_FILES        += $(Mps_TLE5012_Hndlr_IFA_PATH)\Mps_TLE5012_Hndlr_50us_Ifa.c
Mps_TLE5012_Hndlr_src_FILES        += $(Mps_TLE5012_Hndlr_CFG_PATH)\Mps_TLE5012_Hndlr_Cfg.c
Mps_TLE5012_Hndlr_src_FILES        += $(Mps_TLE5012_Hndlr_CAL_PATH)\Mps_TLE5012_Hndlr_Cal.c
Mps_TLE5012_Hndlr_src_FILES        += $(Mps_TLE5012_Hndlr_SRC_PATH)\Mps_TLE5012_Spi.c
Mps_TLE5012_Hndlr_src_FILES        += $(Mps_TLE5012_Hndlr_SRC_PATH)\Mps_TLE5012_Stat_Decode.c

ifeq ($(ICE_COMPILE),true)
Mps_TLE5012_Hndlr_src_FILES        += $(Mps_TLE5012_Hndlr_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	Mps_TLE5012_Hndlr_src_FILES        += $(Mps_TLE5012_Hndlr_UNITY_PATH)\unity.c
	Mps_TLE5012_Hndlr_src_FILES        += $(Mps_TLE5012_Hndlr_UNITY_PATH)\unity_fixture.c	
	Mps_TLE5012_Hndlr_src_FILES        += $(Mps_TLE5012_Hndlr_UT_PATH)\main.c
	Mps_TLE5012_Hndlr_src_FILES        += $(Mps_TLE5012_Hndlr_UT_PATH)\Mps_TLE5012_Hndlr_1ms\Mps_TLE5012_Hndlr_1ms_UtMain.c
	Mps_TLE5012_Hndlr_src_FILES        += $(Mps_TLE5012_Hndlr_UT_PATH)\Mps_TLE5012_Hndlr_50us\Mps_TLE5012_Hndlr_50us_UtMain.c
endif