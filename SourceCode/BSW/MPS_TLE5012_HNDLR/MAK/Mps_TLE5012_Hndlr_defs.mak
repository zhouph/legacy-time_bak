# \file
#
# \brief Mps_TLE5012_Hndlr
#
# This file contains the implementation of the SWC
# module Mps_TLE5012_Hndlr.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Mps_TLE5012_Hndlr_CORE_PATH     := $(MANDO_BSW_ROOT)\Mps_TLE5012_Hndlr
Mps_TLE5012_Hndlr_CAL_PATH      := $(Mps_TLE5012_Hndlr_CORE_PATH)\CAL\$(Mps_TLE5012_Hndlr_VARIANT)
Mps_TLE5012_Hndlr_SRC_PATH      := $(Mps_TLE5012_Hndlr_CORE_PATH)\SRC
Mps_TLE5012_Hndlr_CFG_PATH      := $(Mps_TLE5012_Hndlr_CORE_PATH)\CFG\$(Mps_TLE5012_Hndlr_VARIANT)
Mps_TLE5012_Hndlr_HDR_PATH      := $(Mps_TLE5012_Hndlr_CORE_PATH)\HDR
Mps_TLE5012_Hndlr_IFA_PATH      := $(Mps_TLE5012_Hndlr_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Mps_TLE5012_Hndlr_CMN_PATH      := $(Mps_TLE5012_Hndlr_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	Mps_TLE5012_Hndlr_UT_PATH		:= $(Mps_TLE5012_Hndlr_CORE_PATH)\UT
	Mps_TLE5012_Hndlr_UNITY_PATH	:= $(Mps_TLE5012_Hndlr_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(Mps_TLE5012_Hndlr_UT_PATH)
	CC_INCLUDE_PATH		+= $(Mps_TLE5012_Hndlr_UNITY_PATH)
	Mps_TLE5012_Hndlr_1ms_PATH 	:= Mps_TLE5012_Hndlr_UT_PATH\Mps_TLE5012_Hndlr_1ms
	Mps_TLE5012_Hndlr_50us_PATH 	:= Mps_TLE5012_Hndlr_UT_PATH\Mps_TLE5012_Hndlr_50us
endif
CC_INCLUDE_PATH    += $(Mps_TLE5012_Hndlr_CAL_PATH)
CC_INCLUDE_PATH    += $(Mps_TLE5012_Hndlr_SRC_PATH)
CC_INCLUDE_PATH    += $(Mps_TLE5012_Hndlr_CFG_PATH)
CC_INCLUDE_PATH    += $(Mps_TLE5012_Hndlr_HDR_PATH)
CC_INCLUDE_PATH    += $(Mps_TLE5012_Hndlr_IFA_PATH)
CC_INCLUDE_PATH    += $(Mps_TLE5012_Hndlr_CMN_PATH)

