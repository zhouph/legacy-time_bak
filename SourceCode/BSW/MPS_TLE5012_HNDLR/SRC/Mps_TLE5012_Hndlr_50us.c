/**
 * @defgroup Mps_TLE5012_Hndlr_50us Mps_TLE5012_Hndlr_50us
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mps_TLE5012_Hndlr_50us.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Mps_TLE5012_Hndlr_50us.h"
#include "Mps_TLE5012_Hndlr_50us_Ifa.h"
#include "IfxStm_reg.h"
#include "Gptm_Cdd.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define MPS_TLE5012_HNDLR_50US_START_SEC_CONST_UNSPECIFIED
#include "Mps_TLE5012_Hndlr_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define MPS_TLE5012_HNDLR_50US_STOP_SEC_CONST_UNSPECIFIED
#include "Mps_TLE5012_Hndlr_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MPS_TLE5012_HNDLR_50US_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mps_TLE5012_Hndlr_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Mps_TLE5012_Hndlr_50us_HdrBusType Mps_TLE5012_Hndlr_50usBus;

/* Version Info */
const SwcVersionInfo_t Mps_TLE5012_Hndlr_50usVersionInfo = 
{   
    MPS_TLE5012_HNDLR_50US_MODULE_ID,           /* Mps_TLE5012_Hndlr_50usVersionInfo.ModuleId */
    MPS_TLE5012_HNDLR_50US_MAJOR_VERSION,       /* Mps_TLE5012_Hndlr_50usVersionInfo.MajorVer */
    MPS_TLE5012_HNDLR_50US_MINOR_VERSION,       /* Mps_TLE5012_Hndlr_50usVersionInfo.MinorVer */
    MPS_TLE5012_HNDLR_50US_PATCH_VERSION,       /* Mps_TLE5012_Hndlr_50usVersionInfo.PatchVer */
    MPS_TLE5012_HNDLR_50US_BRANCH_VERSION       /* Mps_TLE5012_Hndlr_50usVersionInfo.BranchVer */
};
    
/* Input Data Element */
Mom_HndlrEcuModeSts_t Mps_TLE5012_Hndlr_50usEcuModeSts;

/* Output Data Element */
Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo_t Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo;
Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo_t Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo;

uint32 Mps_TLE5012_Hndlr_50us_Timer_Start;
uint32 Mps_TLE5012_Hndlr_50us_Timer_Elapsed;

#define MPS_TLE5012_HNDLR_50US_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mps_TLE5012_Hndlr_MemMap.h"
#define MPS_TLE5012_HNDLR_50US_START_SEC_VAR_NOINIT_32BIT
#include "Mps_TLE5012_Hndlr_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MPS_TLE5012_HNDLR_50US_STOP_SEC_VAR_NOINIT_32BIT
#include "Mps_TLE5012_Hndlr_MemMap.h"
#define MPS_TLE5012_HNDLR_50US_START_SEC_VAR_UNSPECIFIED
#include "Mps_TLE5012_Hndlr_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MPS_TLE5012_HNDLR_50US_STOP_SEC_VAR_UNSPECIFIED
#include "Mps_TLE5012_Hndlr_MemMap.h"
#define MPS_TLE5012_HNDLR_50US_START_SEC_VAR_32BIT
#include "Mps_TLE5012_Hndlr_MemMap.h"
/** Variable Section (32BIT)**/


#define MPS_TLE5012_HNDLR_50US_STOP_SEC_VAR_32BIT
#include "Mps_TLE5012_Hndlr_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MPS_TLE5012_HNDLR_50US_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mps_TLE5012_Hndlr_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define MPS_TLE5012_HNDLR_50US_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mps_TLE5012_Hndlr_MemMap.h"
#define MPS_TLE5012_HNDLR_50US_START_SEC_VAR_NOINIT_32BIT
#include "Mps_TLE5012_Hndlr_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MPS_TLE5012_HNDLR_50US_STOP_SEC_VAR_NOINIT_32BIT
#include "Mps_TLE5012_Hndlr_MemMap.h"
#define MPS_TLE5012_HNDLR_50US_START_SEC_VAR_UNSPECIFIED
#include "Mps_TLE5012_Hndlr_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MPS_TLE5012_HNDLR_50US_STOP_SEC_VAR_UNSPECIFIED
#include "Mps_TLE5012_Hndlr_MemMap.h"
#define MPS_TLE5012_HNDLR_50US_START_SEC_VAR_32BIT
#include "Mps_TLE5012_Hndlr_MemMap.h"
/** Variable Section (32BIT)**/


#define MPS_TLE5012_HNDLR_50US_STOP_SEC_VAR_32BIT
#include "Mps_TLE5012_Hndlr_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define MPS_TLE5012_HNDLR_50US_START_SEC_CODE
#include "Mps_TLE5012_Hndlr_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Mps_TLE5012_Hndlr_50us_Init(void)
{
    /* Initialize internal bus */
    Mps_TLE5012_Hndlr_50usBus.Mps_TLE5012_Hndlr_50usEcuModeSts = 0;
    Mps_TLE5012_Hndlr_50usBus.Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo.D1IIFAngRawData = 0;
    Mps_TLE5012_Hndlr_50usBus.Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo.D1IIFAngDegree = 0;
    Mps_TLE5012_Hndlr_50usBus.Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo.D2IIFAngRawData = 0;
    Mps_TLE5012_Hndlr_50usBus.Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo.D2IIFAngDegree = 0;
}

void Mps_TLE5012_Hndlr_50us(void)
{
    Mps_TLE5012_Hndlr_50us_Timer_Start = STM0_TIM0.U;

    /* Input */
    Mps_TLE5012_Hndlr_50us_Read_Mps_TLE5012_Hndlr_50usEcuModeSts(&Mps_TLE5012_Hndlr_50usBus.Mps_TLE5012_Hndlr_50usEcuModeSts);

    /* Process */
	uint16 temp_mps_tle5012_Gpt12_T3_TimerVal;
	temp_mps_tle5012_Gpt12_T3_TimerVal = MpsD1_Gpt12Iif_GetAngle();

	Mps_TLE5012_Hndlr_50usBus.Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo.D1IIFAngRawData = (temp_mps_tle5012_Gpt12_T3_TimerVal >>1);

    /* Output */
    Mps_TLE5012_Hndlr_50us_Write_Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo(&Mps_TLE5012_Hndlr_50usBus.Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo);
    /*==============================================================================
    * Members of structure Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo 
     : Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo.D1IIFAngRawData;
     : Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo.D1IIFAngDegree;
     =============================================================================*/
    
    Mps_TLE5012_Hndlr_50us_Write_Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo(&Mps_TLE5012_Hndlr_50usBus.Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo);
    /*==============================================================================
    * Members of structure Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo 
     : Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo.D2IIFAngRawData;
     : Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo.D2IIFAngDegree;
     =============================================================================*/
    

    Mps_TLE5012_Hndlr_50us_Timer_Elapsed = STM0_TIM0.U - Mps_TLE5012_Hndlr_50us_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define MPS_TLE5012_HNDLR_50US_STOP_SEC_CODE
#include "Mps_TLE5012_Hndlr_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
