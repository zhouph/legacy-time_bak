/**
 * @defgroup Mps_TLE5012_Spi Mps_TLE5012_Spi
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mps_TLE5012_Spi.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Spim_Cdd.h"
#include "Mps_TLE5012_Spi_Ifa.h"
#include "Mps_TLE5012_Spi.h"
#include "Gptm_Cdd.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define TLE5012B_ANGLE_DECODE_MASK			0x00007FFF
#define TLE5012B_STAT_DECODE_MASK			0x0000FFFF
#define ASYNCSPI_ONE_FLAME_WAIT_CNT			550 /* 1 Flame 9us == 550Tick */
#define NUMBER_OF_FLAME_FOR_10_FLAME		10	/* CMD, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, Safety Wrd */
#define NUMBER_OF_FLAME_FOR_5_FLAME			5	/* CMD, Ang Read, Ang Speed Read, STAT Read, Safety Wrd */
#define NUMBER_OF_FLAME_FOR_3_FLAME			3	/* CMD, Data, Safety Wrd */
#define OPENDRAIN_TXFLOATMSG				0x0000FFFF

#define TLE5012B_CMD_RW_W						0x00000000
#define TLE5012B_CMD_RW_R						0x00008000
#define TLE5012B_CMD_LOCK_DEFAULTACESS			0x00000000
#define TLE5012B_CMD_LOCK_CONFIGACESS			0x00005000
#define TLE5012B_CMD_UPD_ACCESS_CURRENT_VAL		0x00000000
#define TLE5012B_CMD_UPD_ACCESS_UPDATEBUFF_VAL	0x00000400
#define TLE5012B_CMD_ADDR_START_MOD2			0x00000080
#define TLE5012B_CMD_ADDR_START_AVAL			0x00000020
#define TLE5012B_CMD_ADDR_START_STAT			0x00000000
#define TLE5012B_CMD_ND_8DATA					0x00000008
#define TLE5012B_CMD_ND_3DATA					0x00000003
#define TLE5012B_CMD_ND_1DATA					0x00000001

#define TLE5012B_MOD2_RES					0x00000000
#define TLE5012B_MOD2_ANGRANGE_FAC1			0x00000800
#define TLE5012B_MOD2_ANGRANGE_FAC4			0x00002000
#define TLE5012B_MOD2_ANGRANGE_FACHLLF		0x00000400
#define TLE5012B_MOD2_ANGDIR_CCW			0x00000000
#define TLE5012B_MOD2_ANGDIR_CW				0x00000008	
#define TLE5012B_MOD2_PREDICT_DIS			0x00000000	
#define TLE5012B_MOD2_PREDICT_EN			0x00000004	
#define TLE5012B_MOD2_AUTOCAL_NO			0x00000000
#define TLE5012B_MOD2_AUTOCAL_M1			0x00000001
#define TLE5012B_MOD2_AUTOCAL_M2			0x00000002
#define TLE5012B_MOD2_AUTOCAL_M3			0x00000003

#define TLE5012B_MOD3_ANGBASE_DECODE_MASK	0x0000FFF0
#define TLE5012B_MOD3_SPIKEF_DIS			0x00000000
#define TLE5012B_MOD3_SPIKEF_EN				0x00000008
#define TLE5012B_MOD3_SSCOD_PUSHPULL		0x00000000
#define TLE5012B_MOD3_SSCOD_OPENDRAIN		0x00000004
#define TLE5012B_MOD3_PADDRV_SDSDFE			0x00000000
#define TLE5012B_MOD3_PADDRV_SDSDSE			0x00000001
#define TLE5012B_MOD3_PADDRV_WDMDFE			0x00000002
#define TLE5012B_MOD3_PADDRV_WDWDSE			0x00000003		

#define TLE5012B_OFFX_XOFFSET_DECODE_MASK	0x0000FFF0
#define TLE5012B_OFFX_RES					0x00000000

#define TLE5012B_OFFY_YOFFSET_DECODE_MASK	0x0000FFF0
#define TLE5012B_OFFY_RES					0x00000000

#define TLE5012B_SYNCH_SYNCH_DECODE_MASK	0x0000FFF0
#define TLE5012B_SYNCH_RES					0x00000000

#define TLE5012B_IFAB_ORTHO_DECODE_MASK		0x0000FFF0
#define TLE5012B_IFAB_FIRUDR_85P3			0x00000000
#define TLE5012B_IFAB_FIRUDR_42P7			0x00000008
#define TLE5012B_IFAB_IFABOD_PUSHPUL		0x00000000
#define TLE5012B_IFAB_IFABOD_OPENDRAIN		0x00000004
#define TLE5012B_IFAB_IFABHYST_0P			0x00000000
#define TLE5012B_IFAB_IFABHYST_0P175		0x00000001
#define TLE5012B_IFAB_IFABHYST_0P35			0x00000002
#define TLE5012B_IFAB_IFABHYST_0P70			0x00000003

#define TLE5012B_MOD4_TCOXT_DECODE_MASK		0x0000FE00
#define TLE5012B_MOD4_HSMPLP_ABCNTEN		0x00000020
#define TLE5012B_MOD4_HSMPLP_ABCNTDIS		0x000000A0
#define TLE5012B_MOD4_IFABRES_IIFRES12BIT	0x00000000
#define TLE5012B_MOD4_IFABRES_IIFRES11BIT	0x00000008
#define TLE5012B_MOD4_IFABRES_IIFRES10BIT	0x00000010
#define TLE5012B_MOD4_IFABRES_IIFRES09BIT	0x00000018
#define TLE5012B_MOD4_RES					0x00000000
#define TLE5012B_MOD4_IFMD_IIF				0x00000000
#define TLE5012B_MOD4_IFMD_PWM				0x00000001
#define TLE5012B_MOD4_IFMD_HSM				0x00000002
#define TLE5012B_MOD4_IFMD_SPC				0x00000003

#define TLE5012B_TCOY_TCOYT_DECODE_MASK		0x0000FE00
#define TLE5012B_TCOY_SBIST_DIS				0x00000000
#define TLE5012B_TCOY_SBIST_EN				0x00000100
#define TLE5012B_TCOY_CARPAR_MASK			0x000000FF

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
uint16 	MpsD1_Spi_Async_Wait_Cnt;
uint16	MpsD1_SpiAngDeg36000buff[10];

uint32 MpsD1_SpiTx_AllConfigReadBuff[10] = {0x00000000, 0x00000000, 0x00000000, 
											0x00000000, 0x00000000, 0x00000000, 
											0x00000000, 0x00000000, 0x00000000, 
											0x00000000};
uint32 MpsD1_SpiRx_AllConfigReadBuff[10] = {0x00000000, 0x00000000, 0x00000000, 
											0x00000000, 0x00000000, 0x00000000, 
											0x00000000, 0x00000000, 0x00000000, 
											0x00000000};

uint32 MpsD1_SpiTx_AllConfigWrtBuff[10]  = {0x00000000, 0x00000000, 0x00000000, 
										    0x00000000, 0x00000000, 0x00000000, 
										    0x00000000, 0x00000000, 0x00000000, 
										    0x00000000};
uint32 MpsD1_SpiRx_AllConfigWrtBuff[10]  = {0x00000000, 0x00000000, 0x00000000, 
										    0x00000000, 0x00000000, 0x00000000, 
										    0x00000000, 0x00000000, 0x00000000, 
										    0x00000000};

uint32	MpsD1_SpiTx_AngReadBuff[3] 		= {0x00000000, 0x00000000, 0x00000000};
uint32	MpsD1_SpiRx_AngReadBuff[3] 		= {0x00000000, 0x00000000, 0x00000000};

uint32	MpsD1_SpiTx_STATAngReadBuff[5] 	= {0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000};
uint32	MpsD1_SpiRx_STATAngReadBuff[5] 	= {0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000};

/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
void MpsD1_AsyncSpi_Wait(uint16 one_flame_tick, uint16 needed_flame);
void MpsD1_Spi_ReadAllConfig(void);
void MpsD1_Spi_DecodeAndSetAllConfig(void);

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
uint16 MpsD1_Spi_ReadAngle(void);
uint32 MpsD1_Spi_ReadStatAng(void);
void MpsD1_Spi_Config(void);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
/* MpsTle5012 Angle Register Read Sequence at MCU 
   Tx Angle Regiseter Read Cmd - Rx CmdResqonse: Angle Register Value - Rx SafetyWrd */
uint16 MpsD1_Spi_ReadAngle(void)
{
	uint16 temp_MpsD1_SpiAngRawData = 0;
	uint16 temp_MpsD1_Cmdwrd = 0;

	/* Setting TX_Buffer[0]: Command Word = 0x00008021 */
	temp_MpsD1_Cmdwrd = TLE5012B_CMD_RW_R | TLE5012B_CMD_LOCK_DEFAULTACESS | TLE5012B_CMD_UPD_ACCESS_CURRENT_VAL | TLE5012B_CMD_ADDR_START_AVAL | TLE5012B_CMD_ND_1DATA;
	MpsD1_SpiTx_AngReadBuff[0] = temp_MpsD1_Cmdwrd;

	/* Setting TX_Buffer[1 to 2]: OPENDRAIN_TXFLOATMSG(0x0000FFFF) - Generation Clk for Receving Opend Drain Rx Data */
	MpsD1_SpiTx_AngReadBuff[1] = OPENDRAIN_TXFLOATMSG;
	MpsD1_SpiTx_AngReadBuff[2] = OPENDRAIN_TXFLOATMSG;
	
	Spim_Cdd_SetupEB(Spim_Cdd_SpiChannel_Mps_Tle5012_1, (Spim_Cdd_DataType*)MpsD1_SpiTx_AngReadBuff, (Spim_Cdd_DataType*)MpsD1_SpiRx_AngReadBuff, 3);
	Spim_Cdd_AsyncTransmit(Spim_Cdd_SpiSequence_Mps_Tle5012);

	temp_MpsD1_SpiAngRawData =(uint16)(MpsD1_SpiRx_AngReadBuff[1] & TLE5012B_ANGLE_DECODE_MASK);

	return temp_MpsD1_SpiAngRawData;
}

uint32 MpsD1_Spi_ReadStatAng(void)
{	
	uint32 temp_MpsD1_StatAngRawData = 0;
	uint32 temp_MpsD1_StatshiftData = 0;
	uint32 temp_MpsD1_AngData = 0;
	uint16 temp_MpsD1_Cmdwrd = 0;

	/* Setting TX_Buffer[0]: Command Word = 0x00008003 */
	temp_MpsD1_Cmdwrd = TLE5012B_CMD_RW_R | TLE5012B_CMD_LOCK_DEFAULTACESS | TLE5012B_CMD_UPD_ACCESS_CURRENT_VAL | TLE5012B_CMD_ADDR_START_STAT | TLE5012B_CMD_ND_3DATA;
	MpsD1_SpiTx_STATAngReadBuff[0] = temp_MpsD1_Cmdwrd;

	/* Setting TX_Buffer[1 to 4]: OPENDRAIN_TXFLOATMSG(0x0000FFFF) - Generation Clk for Receving Opend Drain Rx Data */
	MpsD1_SpiTx_STATAngReadBuff[1] = OPENDRAIN_TXFLOATMSG;
	MpsD1_SpiTx_STATAngReadBuff[2] = OPENDRAIN_TXFLOATMSG;
	MpsD1_SpiTx_STATAngReadBuff[3] = OPENDRAIN_TXFLOATMSG;
	MpsD1_SpiTx_STATAngReadBuff[4] = OPENDRAIN_TXFLOATMSG;

	Spim_Cdd_SetupEB(Spim_Cdd_SpiChannel_Mps_Tle5012_1, (Spim_Cdd_DataType*)MpsD1_SpiTx_STATAngReadBuff, (Spim_Cdd_DataType*)MpsD1_SpiRx_STATAngReadBuff, 5);
	Spim_Cdd_AsyncTransmit(Spim_Cdd_SpiSequence_Mps_Tle5012);

	temp_MpsD1_StatshiftData = (MpsD1_SpiRx_STATAngReadBuff[1] & TLE5012B_STAT_DECODE_MASK) << 16;
	temp_MpsD1_AngData = MpsD1_SpiRx_STATAngReadBuff[3] & TLE5012B_ANGLE_DECODE_MASK;

	temp_MpsD1_StatAngRawData = temp_MpsD1_StatshiftData | temp_MpsD1_AngData;

	return temp_MpsD1_StatAngRawData;
}

void MpsD1_Spi_Config(void)
{		
	uint16 temp_MpsD1_SpiAngRawData = 0;

	MpsD1_Spi_ReadAllConfig();
	MpsD1_AsyncSpi_Wait(ASYNCSPI_ONE_FLAME_WAIT_CNT, NUMBER_OF_FLAME_FOR_10_FLAME);
	MpsD1_Spi_DecodeAndSetAllConfig();
	MpsD1_AsyncSpi_Wait(ASYNCSPI_ONE_FLAME_WAIT_CNT, NUMBER_OF_FLAME_FOR_10_FLAME);
	
	temp_MpsD1_SpiAngRawData = MpsD1_Spi_ReadAngle();
	MpsD1_AsyncSpi_Wait(ASYNCSPI_ONE_FLAME_WAIT_CNT, NUMBER_OF_FLAME_FOR_3_FLAME);

	temp_MpsD1_SpiAngRawData = MpsD1_Spi_ReadAngle();
	MpsD1_AsyncSpi_Wait(ASYNCSPI_ONE_FLAME_WAIT_CNT, NUMBER_OF_FLAME_FOR_3_FLAME);

	temp_MpsD1_SpiAngRawData = MpsD1_Spi_ReadAngle();
	MpsD1_AsyncSpi_Wait(ASYNCSPI_ONE_FLAME_WAIT_CNT, NUMBER_OF_FLAME_FOR_3_FLAME);

	temp_MpsD1_SpiAngRawData = MpsD1_Spi_ReadAngle();

	//MpsD1_Gpt12Iif_CompensateAngle(temp_MpsD1_SpiAngRawData);
}

void MpsD1_AsyncSpi_Wait(uint16 one_flame_tick, uint16 needed_flame)
{
	uint16 i = 0;
	uint16 temp_One_Flame_Tick = 0;
	uint16 temp_Needed_Number_Of_Flame = 0;
	uint16 temp_Needed_Total_Wait_Tick = 0;

	temp_One_Flame_Tick = one_flame_tick;
	temp_Needed_Number_Of_Flame = needed_flame;
	
	temp_Needed_Total_Wait_Tick = temp_One_Flame_Tick * temp_Needed_Number_Of_Flame;
	
	for(i=0;i<temp_Needed_Total_Wait_Tick;i++)
	{
		MpsD1_Spi_Async_Wait_Cnt++;
	}
}

void MpsD1_Spi_ReadAllConfig(void)
{
	uint32 temp_MpsD1_Cmdwrd = 0;

	/* Setting TX_Buffer[0]: Command Word = 0x0000D088 */
	temp_MpsD1_Cmdwrd = TLE5012B_CMD_RW_R | TLE5012B_CMD_LOCK_CONFIGACESS | TLE5012B_CMD_ADDR_START_MOD2 | TLE5012B_CMD_ND_8DATA;
	MpsD1_SpiTx_AllConfigReadBuff[0] = temp_MpsD1_Cmdwrd;

	/* Setting TX_Buffer[1 to 9]: OPENDRAIN_TXFLOATMSG(0x0000FFFF) - Generation Clk for Receving Opend Drain Rx Data */
	MpsD1_SpiTx_AllConfigReadBuff[1] =  OPENDRAIN_TXFLOATMSG; 	
	MpsD1_SpiTx_AllConfigReadBuff[2] =  OPENDRAIN_TXFLOATMSG;
	MpsD1_SpiTx_AllConfigReadBuff[3] =  OPENDRAIN_TXFLOATMSG;
	MpsD1_SpiTx_AllConfigReadBuff[4] =  OPENDRAIN_TXFLOATMSG;
	MpsD1_SpiTx_AllConfigReadBuff[5] =  OPENDRAIN_TXFLOATMSG;
	MpsD1_SpiTx_AllConfigReadBuff[6] =  OPENDRAIN_TXFLOATMSG;
	MpsD1_SpiTx_AllConfigReadBuff[7] =  OPENDRAIN_TXFLOATMSG;
	MpsD1_SpiTx_AllConfigReadBuff[8] =  OPENDRAIN_TXFLOATMSG;
	MpsD1_SpiTx_AllConfigReadBuff[9] =  OPENDRAIN_TXFLOATMSG;
		
	Spim_Cdd_SetupEB(Spim_Cdd_SpiChannel_Mps_Tle5012_1, (Spim_Cdd_DataType*)MpsD1_SpiTx_AllConfigReadBuff, (Spim_Cdd_DataType*)MpsD1_SpiRx_AllConfigReadBuff, 10);
	Spim_Cdd_AsyncTransmit(Spim_Cdd_SpiSequence_Mps_Tle5012);
}	

void MpsD1_Spi_DecodeAndSetAllConfig(void)
{	
	uint32 temp_MpsD1_Cmdwrd = 0;

	uint32 temp_MpsD1_Mod2_WrtData = 0;
	
	uint32 temp_MpsD1_Mod3_RawdData = 0;
	uint32 temp_MpsD1_Mod3_RawdData_AngBase = 0;
	uint32 temp_MpsD1_Mod3_WrtData = 0;
	
	uint32 temp_MpsD1_Offx_RawdData = 0;
	uint32 temp_MpsD1_Offx_RawdData_Xoffset = 0;
	uint32 temp_MpsD1_Offx_WrtData = 0;
	
	uint32 temp_MpsD1_Offy_RawdData = 0;
	uint32 temp_MpsD1_Offy_RawdData_Yoffset = 0;
	uint32 temp_MpsD1_Offy_WrtData = 0;

	uint32 temp_MpsD1_Synch_RawdData = 0;
	uint32 temp_MpsD1_Synch_RawdData_Synch = 0;
	uint32 temp_MpsD1_Synch_WrtData = 0;

	uint32 temp_MpsD1_Ifab_RawdData = 0;
	uint32 temp_MpsD1_Ifab_RawdData_Ortho = 0;
	uint32 temp_MpsD1_Ifab_WrtData = 0;

	uint32 temp_MpsD1_Mod4_RawdData = 0;
	uint32 temp_MpsD1_Mod4_RawdData_Tcoxt = 0;
	uint32 temp_MpsD1_Mod4_WrtData = 0;

	uint32 temp_MpsD1_Tcoy_RawdData = 0;
	uint32 temp_MpsD1_Tcoy_RawdData_Tcoyt = 0;
	uint32 temp_MpsD1_Calculated_CrcPar = 0;
	uint32 temp_MpsD1_Tcoy_WrtData = 0;

	/* Setting TX_Buffer[0]: Command Word */
	temp_MpsD1_Cmdwrd = TLE5012B_CMD_RW_W | TLE5012B_CMD_LOCK_CONFIGACESS | TLE5012B_CMD_UPD_ACCESS_CURRENT_VAL | TLE5012B_CMD_ADDR_START_MOD2 | TLE5012B_CMD_ND_8DATA;
	MpsD1_SpiTx_AllConfigWrtBuff[0] = temp_MpsD1_Cmdwrd;

	/* Setting TX_Buffer[1]: MOD2 (Addr 0x08) */
	temp_MpsD1_Mod2_WrtData = TLE5012B_MOD2_RES | TLE5012B_MOD2_ANGRANGE_FAC1 | TLE5012B_MOD2_ANGDIR_CW | TLE5012B_MOD2_PREDICT_DIS | TLE5012B_MOD2_AUTOCAL_M1;
	MpsD1_SpiTx_AllConfigWrtBuff[1] = temp_MpsD1_Mod2_WrtData;

	/* Setting TX_Buffer[2]: MOD3 (Addr 0x09) */
	temp_MpsD1_Mod3_RawdData = MpsD1_SpiRx_AllConfigReadBuff[2];
	temp_MpsD1_Mod3_RawdData_AngBase = temp_MpsD1_Mod3_RawdData & TLE5012B_MOD3_ANGBASE_DECODE_MASK;
	temp_MpsD1_Mod3_WrtData = temp_MpsD1_Mod3_RawdData_AngBase | TLE5012B_MOD3_SPIKEF_DIS | TLE5012B_MOD3_SSCOD_OPENDRAIN | TLE5012B_MOD3_PADDRV_SDSDFE;
	MpsD1_SpiTx_AllConfigWrtBuff[2] = temp_MpsD1_Mod3_WrtData;

	/* Setting TX_Buffer[3]: OFFX (Addr 0x0A) */
	temp_MpsD1_Offx_RawdData = MpsD1_SpiRx_AllConfigReadBuff[3];
	temp_MpsD1_Offx_RawdData_Xoffset = temp_MpsD1_Offx_RawdData & TLE5012B_OFFX_XOFFSET_DECODE_MASK;
	temp_MpsD1_Offx_WrtData = temp_MpsD1_Offx_RawdData_Xoffset | TLE5012B_OFFX_RES;
	MpsD1_SpiTx_AllConfigWrtBuff[3] = temp_MpsD1_Offx_WrtData;

	/* Setting TX_Buffer[4]: OFFY (Addr 0x0B) */
	temp_MpsD1_Offy_RawdData = MpsD1_SpiRx_AllConfigReadBuff[4];
	temp_MpsD1_Offy_RawdData_Yoffset = temp_MpsD1_Offy_RawdData & TLE5012B_OFFY_YOFFSET_DECODE_MASK;
	temp_MpsD1_Offy_WrtData = temp_MpsD1_Offy_RawdData_Yoffset | TLE5012B_OFFY_RES;
	MpsD1_SpiTx_AllConfigWrtBuff[4] = temp_MpsD1_Offy_WrtData;

	/* Setting TX_Buffer[5]: SYNCH (Addr 0x0C) */
	temp_MpsD1_Synch_RawdData = MpsD1_SpiRx_AllConfigReadBuff[5]; 
	temp_MpsD1_Synch_RawdData_Synch = temp_MpsD1_Synch_RawdData & TLE5012B_SYNCH_SYNCH_DECODE_MASK;
	temp_MpsD1_Synch_WrtData = temp_MpsD1_Synch_RawdData_Synch | TLE5012B_SYNCH_RES;
	MpsD1_SpiTx_AllConfigWrtBuff[5] = temp_MpsD1_Synch_WrtData;

	/* Setting TX_Buffer[6]: IFAB (Addr 0x0D) */
	temp_MpsD1_Ifab_RawdData = MpsD1_SpiRx_AllConfigReadBuff[6];  
	temp_MpsD1_Ifab_RawdData_Ortho = temp_MpsD1_Ifab_RawdData & TLE5012B_IFAB_ORTHO_DECODE_MASK;
	temp_MpsD1_Ifab_WrtData = temp_MpsD1_Ifab_RawdData_Ortho | TLE5012B_IFAB_FIRUDR_42P7 | TLE5012B_IFAB_IFABOD_PUSHPUL | TLE5012B_IFAB_IFABHYST_0P70;
	MpsD1_SpiTx_AllConfigWrtBuff[6] =  temp_MpsD1_Ifab_WrtData;

	/* Setting TX_Buffer[7]: MOD4 (Addr 0x0E) */
	temp_MpsD1_Mod4_RawdData = MpsD1_SpiRx_AllConfigReadBuff[7];  
	temp_MpsD1_Mod4_RawdData_Tcoxt = temp_MpsD1_Mod4_RawdData &TLE5012B_MOD4_TCOXT_DECODE_MASK;
	temp_MpsD1_Mod4_WrtData = temp_MpsD1_Mod4_RawdData_Tcoxt | TLE5012B_MOD4_HSMPLP_ABCNTEN | TLE5012B_MOD4_IFABRES_IIFRES12BIT | TLE5012B_MOD4_RES | TLE5012B_MOD4_IFMD_IIF;
	MpsD1_SpiTx_AllConfigWrtBuff[7] =  temp_MpsD1_Mod4_WrtData;

	/* Setting TX_Buffer[8]: TCOY (Addr 0x0F) */
	temp_MpsD1_Tcoy_RawdData = MpsD1_SpiRx_AllConfigReadBuff[8];
	temp_MpsD1_Tcoy_RawdData_Tcoyt = temp_MpsD1_Tcoy_RawdData & TLE5012B_TCOY_TCOYT_DECODE_MASK;
	/* CR_PAR Calculation*/
	temp_MpsD1_Calculated_CrcPar = temp_MpsD1_Calculated_CrcPar & TLE5012B_TCOY_CARPAR_MASK;
	temp_MpsD1_Tcoy_WrtData = temp_MpsD1_Tcoy_RawdData_Tcoyt | TLE5012B_TCOY_SBIST_EN | temp_MpsD1_Calculated_CrcPar;
	MpsD1_SpiTx_AllConfigWrtBuff[8] =  temp_MpsD1_Tcoy_WrtData;

	/* Setting TX_Buffer[9]: OPENDRAIN_TXFLOATMSG(0x0000FFFF) - Generation Clk for Receving Opend Drain Rx Data */
	MpsD1_SpiTx_AllConfigWrtBuff[9] =  OPENDRAIN_TXFLOATMSG;

	Spim_Cdd_SetupEB(Spim_Cdd_SpiChannel_Mps_Tle5012_1, (Spim_Cdd_DataType*)MpsD1_SpiTx_AllConfigWrtBuff, (Spim_Cdd_DataType*)MpsD1_SpiRx_AllConfigWrtBuff, 10);
	Spim_Cdd_AsyncTransmit(Spim_Cdd_SpiSequence_Mps_Tle5012);
}