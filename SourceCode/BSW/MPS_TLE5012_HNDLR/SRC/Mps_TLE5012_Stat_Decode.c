/**
 * @defgroup Mps_TLE5012_Stat_Decode Mps_TLE5012_Stat_Decode
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mps_TLE5012_Stat_Decode.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Mps_TLE5012_Stat_Decode.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define TLE5012B_STAT_RDST_DECODE_MASK		0x00008000
#define TLE5012B_STAT_SNR_DECODE_MASK		0x00006000
#define TLE5012B_STAT_NOGMRA_DECODE_MASK	0x00001000
#define TLE5012B_STAT_NOGMRXY_DECODE_MASK	0x00000800
#define TLE5012B_STAT_SROM_DECODE_MASK		0x00000400
#define TLE5012B_STAT_SADCT_DECODE_MASK		0x00000200
#define TLE5012B_STAT_RES_DECODE_MASK		0x00000100
#define TLE5012B_STAT_SMAGOL_DECODE_MASK	0x00000080
#define TLE5012B_STAT_SXYOL_DECODE_MASK		0x00000040
#define TLE5012B_STAT_SOV_DECODE_MASK		0x00000020
#define TLE5012B_STAT_SDSPU_DECODE_MASK		0x00000010
#define TLE5012B_STAT_SFUSE_DECODE_MASK		0x00000008
#define TLE5012B_STAT_SVR_DECODE_MASK		0x00000004
#define TLE5012B_STAT_SWD_DECODE_MASK		0x00000002
#define TLE5012B_STAT_SRST_DECODE_MASK		0x00000001
/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
uint8 MpsD1_Stat_RD_ST_Decode(uint16 MpsD1_StatRegister_RawData);
uint8 MpsD1_Stat_S_NR_Decode(uint16 MpsD1_StatRegister_RawData);
uint8 MpsD1_Stat_NO_GMR_A_Decode(uint16 MpsD1_StatRegister_RawData);
uint8 MpsD1_Stat_NO_GMR_XY_Decode(uint16 MpsD1_StatRegister_RawData);
uint8 MpsD1_Stat_S_ROM_Decode(uint16 MpsD1_StatRegister_RawData);
uint8 MpsD1_Stat_S_ADCT_Decode(uint16 MpsD1_StatRegister_RawData);
uint8 MpsD1_Stat_S_MAGOL_Decode(uint16 MpsD1_StatRegister_RawData);
uint8 MpsD1_Stat_S_XYOL_Decode(uint16 MpsD1_StatRegister_RawData);
uint8 MpsD1_Stat_S_OV_Decode(uint16 MpsD1_StatRegister_RawData);
uint8 MpsD1_Stat_S_DSPU_Decode(uint16 MpsD1_StatRegister_RawData);
uint8 MpsD1_Stat_S_FUSE_Decode(uint16 MpsD1_StatRegister_RawData);
uint8 MpsD1_Stat_S_VR_Decode(uint16 MpsD1_StatRegister_RawData);
uint8 MpsD1_Stat_S_WD_Decode(uint16 MpsD1_StatRegister_RawData);
uint8 MpsD1_Stat_S_RST_Decode(uint16 MpsD1_StatRegister_RawData);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
uint8 MpsD1_Stat_RD_ST_Decode(uint16 MpsD1_StatRegister_RawData)
{
	uint8 temp_RD_ST;

	temp_RD_ST = 0;
	temp_RD_ST = (uint8)((MpsD1_StatRegister_RawData & TLE5012B_STAT_RDST_DECODE_MASK) >> 15);

	return temp_RD_ST;
}	

uint8 MpsD1_Stat_S_NR_Decode(uint16 MpsD1_StatRegister_RawData)
{
	uint8 temp_S_NR;

	temp_S_NR = 0;
	temp_S_NR = (uint8)((MpsD1_StatRegister_RawData & TLE5012B_STAT_SNR_DECODE_MASK) >> 13);

	return temp_S_NR;
}	

uint8 MpsD1_Stat_NO_GMR_A_Decode(uint16 MpsD1_StatRegister_RawData)
{
	uint8 temp_NO_GMR_A;	

	temp_NO_GMR_A = 0;
	temp_NO_GMR_A = (uint8)((MpsD1_StatRegister_RawData & TLE5012B_STAT_NOGMRA_DECODE_MASK) >> 12);

	return temp_NO_GMR_A;
}	

uint8 MpsD1_Stat_NO_GMR_XY_Decode(uint16 MpsD1_StatRegister_RawData)
{	
	uint8 temp_NO_GMR_XY;	

	temp_NO_GMR_XY = 0;
	temp_NO_GMR_XY = (uint8)((MpsD1_StatRegister_RawData & TLE5012B_STAT_NOGMRXY_DECODE_MASK) >> 11);

	return temp_NO_GMR_XY;
}	

uint8 MpsD1_Stat_S_ROM_Decode(uint16 MpsD1_StatRegister_RawData)
{	
	uint8 temp_S_ROM;

	temp_S_ROM = 0;
	temp_S_ROM = (uint8)((MpsD1_StatRegister_RawData & TLE5012B_STAT_SROM_DECODE_MASK) >> 10);

	return temp_S_ROM;
}	

uint8 MpsD1_Stat_S_ADCT_Decode(uint16 MpsD1_StatRegister_RawData)
{	
	uint8 temp_S_ADCT;

	temp_S_ADCT = 0;
	temp_S_ADCT = (uint8)((MpsD1_StatRegister_RawData & TLE5012B_STAT_SADCT_DECODE_MASK) >> 9);

	return temp_S_ADCT;
}	

uint8 MpsD1_Stat_S_MAGOL_Decode(uint16 MpsD1_StatRegister_RawData)
{	
	uint8 temp_S_MAGOL;

	temp_S_MAGOL = 0;
	temp_S_MAGOL = (uint8)((MpsD1_StatRegister_RawData & TLE5012B_STAT_SMAGOL_DECODE_MASK) >> 7);

	return temp_S_MAGOL;
}
		
uint8 MpsD1_Stat_S_XYOL_Decode(uint16 MpsD1_StatRegister_RawData)
{	
	uint8 temp_S_XYOL;

	temp_S_XYOL = 0;
	temp_S_XYOL = (uint8)((MpsD1_StatRegister_RawData & TLE5012B_STAT_SXYOL_DECODE_MASK) >> 6);

	return temp_S_XYOL;
}

uint8 MpsD1_Stat_S_OV_Decode(uint16 MpsD1_StatRegister_RawData)
{	
	uint8 temp_S_OV;

	temp_S_OV = 0;
	temp_S_OV = (uint8)((MpsD1_StatRegister_RawData & TLE5012B_STAT_SOV_DECODE_MASK) >> 5);

	return temp_S_OV;
}
uint8 MpsD1_Stat_S_DSPU_Decode(uint16 MpsD1_StatRegister_RawData)
{	
	uint8 temp_S_DSPU;

	temp_S_DSPU = 0;
	temp_S_DSPU = (uint8)((MpsD1_StatRegister_RawData & TLE5012B_STAT_SDSPU_DECODE_MASK) >> 4);

	return temp_S_DSPU;
}
uint8 MpsD1_Stat_S_FUSE_Decode(uint16 MpsD1_StatRegister_RawData)
{	
	uint8 temp_S_FUSE;

	temp_S_FUSE = 0;
	temp_S_FUSE = (uint8)((MpsD1_StatRegister_RawData & TLE5012B_STAT_SFUSE_DECODE_MASK) >> 3);

	return temp_S_FUSE;
}	
uint8 MpsD1_Stat_S_VR_Decode(uint16 MpsD1_StatRegister_RawData)
{	
	uint8 temp_S_VR;

	temp_S_VR = 0;
	temp_S_VR = (uint8)((MpsD1_StatRegister_RawData & TLE5012B_STAT_SVR_DECODE_MASK) >> 2);

	return temp_S_VR;
}	
uint8 MpsD1_Stat_S_WD_Decode(uint16 MpsD1_StatRegister_RawData)
{	
	uint8 temp_S_WD;

	temp_S_WD = 0;
	temp_S_WD = (uint8)((MpsD1_StatRegister_RawData & TLE5012B_STAT_SWD_DECODE_MASK) >> 1);

	return temp_S_WD;
}
uint8 MpsD1_Stat_S_RST_Decode(uint16 MpsD1_StatRegister_RawData)
{	
	uint8 temp_S_RST;

	temp_S_RST = 0;
	temp_S_RST = (uint8)((MpsD1_StatRegister_RawData & TLE5012B_STAT_SRST_DECODE_MASK) >> 0);

	return temp_S_RST;
}

