/**
 * @defgroup Mps_TLE5012_Stat_Decode Mps_TLE5012_Stat_Decode
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mps_TLE5012_Stat_Decode.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/
#ifndef MPS_TLE5012_STAT_DECODE_H_
#define MPS_TLE5012_STAT_DECODE_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Mps_TLE5012_Hndlr_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern uint8 MpsD1_Stat_RD_ST_Decode(uint16 MpsD1_StatRegister_Raw_Data);
extern uint8 MpsD1_Stat_RD_ST_Decode(uint16 MpsD1_StatRegister_RawData);
extern uint8 MpsD1_Stat_S_NR_Decode(uint16 MpsD1_StatRegister_RawData);
extern uint8 MpsD1_Stat_NO_GMR_A_Decode(uint16 MpsD1_StatRegister_RawData);
extern uint8 MpsD1_Stat_NO_GMR_XY_Decode(uint16 MpsD1_StatRegister_RawData);
extern uint8 MpsD1_Stat_S_ROM_Decode(uint16 MpsD1_StatRegister_RawData);
extern uint8 MpsD1_Stat_S_ADCT_Decode(uint16 MpsD1_StatRegister_RawData);
extern uint8 MpsD1_Stat_S_MAGOL_Decode(uint16 MpsD1_StatRegister_RawData);
extern uint8 MpsD1_Stat_S_XYOL_Decode(uint16 MpsD1_StatRegister_RawData);
extern uint8 MpsD1_Stat_S_OV_Decode(uint16 MpsD1_StatRegister_RawData);
extern uint8 MpsD1_Stat_S_DSPU_Decode(uint16 MpsD1_StatRegister_RawData);
extern uint8 MpsD1_Stat_S_FUSE_Decode(uint16 MpsD1_StatRegister_RawData);
extern uint8 MpsD1_Stat_S_VR_Decode(uint16 MpsD1_StatRegister_RawData);
extern uint8 MpsD1_Stat_S_WD_Decode(uint16 MpsD1_StatRegister_RawData);
extern uint8 MpsD1_Stat_S_RST_Decode(uint16 MpsD1_StatRegister_RawData);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MPS_TLE5012_STAT_DECODE_H_ */
/** @} */

