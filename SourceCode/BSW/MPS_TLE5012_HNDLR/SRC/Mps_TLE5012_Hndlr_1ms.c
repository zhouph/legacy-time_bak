/**
 * @defgroup Mps_TLE5012_Hndlr_1ms Mps_TLE5012_Hndlr_1ms
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mps_TLE5012_Hndlr_1ms.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Mps_TLE5012_Hndlr_1ms.h"
#include "Mps_TLE5012_Hndlr_1ms_Ifa.h"
#include "Mps_TLE5012_Spi.h"
#include "Mps_TLE5012_Stat_Decode.h"
#include "Gptm_Cdd.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define STATANG2STAT_MASK 0xFFFF0000
#define STATANG2ANG_MASK  0x0000FFFF

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define MPS_TLE5012_HNDLR_1MS_START_SEC_CONST_UNSPECIFIED
#include "Mps_TLE5012_Hndlr_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define MPS_TLE5012_HNDLR_1MS_STOP_SEC_CONST_UNSPECIFIED
#include "Mps_TLE5012_Hndlr_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MPS_TLE5012_HNDLR_1MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mps_TLE5012_Hndlr_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Mps_TLE5012_Hndlr_1ms_HdrBusType Mps_TLE5012_Hndlr_1msBus;

/* Version Info */
const SwcVersionInfo_t Mps_TLE5012_Hndlr_1msVersionInfo = 
{   
    MPS_TLE5012_HNDLR_1MS_MODULE_ID,           /* Mps_TLE5012_Hndlr_1msVersionInfo.ModuleId */
    MPS_TLE5012_HNDLR_1MS_MAJOR_VERSION,       /* Mps_TLE5012_Hndlr_1msVersionInfo.MajorVer */
    MPS_TLE5012_HNDLR_1MS_MINOR_VERSION,       /* Mps_TLE5012_Hndlr_1msVersionInfo.MinorVer */
    MPS_TLE5012_HNDLR_1MS_PATCH_VERSION,       /* Mps_TLE5012_Hndlr_1msVersionInfo.PatchVer */
    MPS_TLE5012_HNDLR_1MS_BRANCH_VERSION       /* Mps_TLE5012_Hndlr_1msVersionInfo.BranchVer */
};
    
/* Input Data Element */
Mom_HndlrEcuModeSts_t Mps_TLE5012_Hndlr_1msEcuModeSts;

/* Output Data Element */
Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo_t Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo;
Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_t Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo;
Mps_TLE5012_Hndlr_1msMpsD2SpiAngInfo_t Mps_TLE5012_Hndlr_1msMpsD2SpiAngInfo;
Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_t Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo;
Mps_TLE5012_Hndlr_1msMpsInvalid_t Mps_TLE5012_Hndlr_1msMpsInvalid;

uint32 Mps_TLE5012_Hndlr_1ms_Timer_Start;
uint32 Mps_TLE5012_Hndlr_1ms_Timer_Elapsed;

#define MPS_TLE5012_HNDLR_1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mps_TLE5012_Hndlr_MemMap.h"
#define MPS_TLE5012_HNDLR_1MS_START_SEC_VAR_NOINIT_32BIT
#include "Mps_TLE5012_Hndlr_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MPS_TLE5012_HNDLR_1MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Mps_TLE5012_Hndlr_MemMap.h"
#define MPS_TLE5012_HNDLR_1MS_START_SEC_VAR_UNSPECIFIED
#include "Mps_TLE5012_Hndlr_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MPS_TLE5012_HNDLR_1MS_STOP_SEC_VAR_UNSPECIFIED
#include "Mps_TLE5012_Hndlr_MemMap.h"
#define MPS_TLE5012_HNDLR_1MS_START_SEC_VAR_32BIT
#include "Mps_TLE5012_Hndlr_MemMap.h"
/** Variable Section (32BIT)**/


#define MPS_TLE5012_HNDLR_1MS_STOP_SEC_VAR_32BIT
#include "Mps_TLE5012_Hndlr_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define MPS_TLE5012_HNDLR_1MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mps_TLE5012_Hndlr_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define MPS_TLE5012_HNDLR_1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Mps_TLE5012_Hndlr_MemMap.h"
#define MPS_TLE5012_HNDLR_1MS_START_SEC_VAR_NOINIT_32BIT
#include "Mps_TLE5012_Hndlr_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define MPS_TLE5012_HNDLR_1MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Mps_TLE5012_Hndlr_MemMap.h"
#define MPS_TLE5012_HNDLR_1MS_START_SEC_VAR_UNSPECIFIED
#include "Mps_TLE5012_Hndlr_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define MPS_TLE5012_HNDLR_1MS_STOP_SEC_VAR_UNSPECIFIED
#include "Mps_TLE5012_Hndlr_MemMap.h"
#define MPS_TLE5012_HNDLR_1MS_START_SEC_VAR_32BIT
#include "Mps_TLE5012_Hndlr_MemMap.h"
/** Variable Section (32BIT)**/


#define MPS_TLE5012_HNDLR_1MS_STOP_SEC_VAR_32BIT
#include "Mps_TLE5012_Hndlr_MemMap.h"

typedef struct
{
	uint8 Stat_RD_ST;
	uint8 Stat_S_NR;
	uint8 Stat_NO_GMR_A;
	uint8 Stat_NO_GMR_XY;
	uint8 Stat_S_ROM;
	uint8 Stat_S_ADCT;
	uint8 Stat_Reserved;
	uint8 Stat_S_MAGOL;
	uint8 Stat_S_XYOL;
	uint8 Stat_S_OV;
	uint8 Stat_S_DSPU;
	uint8 Stat_S_FUSE;
	uint8 Stat_S_VR;
	uint8 Stat_S_WD;
	uint8 Stat_S_RST;
}Mps_TLE5012_STAT;

uint32 MpsD1_Spi_StatAngRaw_Data;
uint16 MpsD1_Spi_StatRawData;
uint16 MpsD1_Spi_AngRawData;
Mps_TLE5012_STAT Mps_TLE5012D1_STAT;

uint16 mps_1ms_cnt;
uint16 MpsD1_Compen_SetFag_at1ms;

/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define MPS_TLE5012_HNDLR_1MS_START_SEC_CODE
#include "Mps_TLE5012_Hndlr_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Mps_TLE5012_Hndlr_1ms_Init(void)
{
    /* Initialize internal bus */
    Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msEcuModeSts = 0;
    Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo.D1SpiAngRawData = 0;
    Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo.D2SpiAngRawData = 0;
    Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo.D1SpiAng36000 = 0;
    Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_VR = 0;
    Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_WD = 0;
    Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_ROM = 0;
    Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_ADCT = 0;
    Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_DSPU = 0;
    Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_FUSE = 0;
    Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_MAGOL = 0;
    Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_OV = 0;
    Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsD2SpiAngInfo.D2SpiAng36000 = 0;
    Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_VR = 0;
    Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_WD = 0;
    Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_ROM = 0;
    Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_ADCT = 0;
    Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_DSPU = 0;
    Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_FUSE = 0;
    Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_MAGOL = 0;
    Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_OV = 0;
    Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsInvalid = 0;

    /* Process */
	MpsD1_Spi_StatAngRaw_Data = 0;
	MpsD1_Spi_StatRawData = 0;
	MpsD1_Spi_AngRawData = 0;
	Mps_TLE5012D1_STAT.Stat_RD_ST 		= 0;
	Mps_TLE5012D1_STAT.Stat_S_NR 		= 0;
	Mps_TLE5012D1_STAT.Stat_NO_GMR_A 	= 0;
	Mps_TLE5012D1_STAT.Stat_NO_GMR_XY 	= 0;
	Mps_TLE5012D1_STAT.Stat_S_ROM 		= 0;
	Mps_TLE5012D1_STAT.Stat_S_ADCT 		= 0;
	Mps_TLE5012D1_STAT.Stat_Reserved 	= 0;
	Mps_TLE5012D1_STAT.Stat_S_MAGOL 	= 0;
	Mps_TLE5012D1_STAT.Stat_S_XYOL 		= 0;
	Mps_TLE5012D1_STAT.Stat_S_OV 		= 0;
	Mps_TLE5012D1_STAT.Stat_S_DSPU 		= 0;
	Mps_TLE5012D1_STAT.Stat_S_FUSE 		= 0;
	Mps_TLE5012D1_STAT.Stat_S_VR 		= 0;
	Mps_TLE5012D1_STAT.Stat_S_WD 		= 0;
	Mps_TLE5012D1_STAT.Stat_S_RST 		= 0;

	MpsD1_Spi_Config();
}

void Mps_TLE5012_Hndlr_1ms(void)
{
    Mps_TLE5012_Hndlr_1ms_Timer_Start = STM0_TIM0.U;

    /* Input */
    Mps_TLE5012_Hndlr_1ms_Read_Mps_TLE5012_Hndlr_1msEcuModeSts(&Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msEcuModeSts);

    /* Process */
	MpsD1_Spi_StatAngRaw_Data = 0;
	MpsD1_Spi_StatRawData = 0;
	MpsD1_Spi_AngRawData = 0;
	Mps_TLE5012D1_STAT.Stat_RD_ST 		= 0;
	Mps_TLE5012D1_STAT.Stat_S_NR 		= 0;
	Mps_TLE5012D1_STAT.Stat_NO_GMR_A 	= 0;
	Mps_TLE5012D1_STAT.Stat_NO_GMR_XY 	= 0;
	Mps_TLE5012D1_STAT.Stat_S_ROM 		= 0;
	Mps_TLE5012D1_STAT.Stat_S_ADCT 		= 0;
	Mps_TLE5012D1_STAT.Stat_Reserved 	= 0;
	Mps_TLE5012D1_STAT.Stat_S_MAGOL 	= 0;
	Mps_TLE5012D1_STAT.Stat_S_XYOL 		= 0;
	Mps_TLE5012D1_STAT.Stat_S_OV 		= 0;
	Mps_TLE5012D1_STAT.Stat_S_DSPU 		= 0;
	Mps_TLE5012D1_STAT.Stat_S_FUSE 		= 0;
	Mps_TLE5012D1_STAT.Stat_S_VR 		= 0;
	Mps_TLE5012D1_STAT.Stat_S_WD 		= 0;
	Mps_TLE5012D1_STAT.Stat_S_RST 		= 0;

	MpsD1_Spi_StatAngRaw_Data 	= MpsD1_Spi_ReadStatAng();
	MpsD1_Spi_StatRawData 		= (uint16)( (MpsD1_Spi_StatAngRaw_Data & STATANG2STAT_MASK) >> 16 );
	MpsD1_Spi_AngRawData 		= (uint16)(MpsD1_Spi_StatAngRaw_Data & STATANG2ANG_MASK);

	mps_1ms_cnt++;
	if(mps_1ms_cnt == 35)
	{
		MpsD1_Gpt12Iif_CompensateAngle(MpsD1_Spi_AngRawData); 
		MpsD1_Compen_SetFag_at1ms = 1;
	}
	else if(mps_1ms_cnt > 35)
	{
		mps_1ms_cnt = 36;
	}	
	
	Mps_TLE5012D1_STAT.Stat_S_VR 	= MpsD1_Stat_S_VR_Decode(MpsD1_Spi_StatRawData);
	Mps_TLE5012D1_STAT.Stat_S_WD 	= MpsD1_Stat_S_WD_Decode(MpsD1_Spi_StatRawData);
	Mps_TLE5012D1_STAT.Stat_S_ROM 	= MpsD1_Stat_NO_GMR_XY_Decode(MpsD1_Spi_StatRawData);
	Mps_TLE5012D1_STAT.Stat_S_ADCT 	= MpsD1_Stat_S_ROM_Decode(MpsD1_Spi_StatRawData);
	Mps_TLE5012D1_STAT.Stat_S_DSPU 	= MpsD1_Stat_S_DSPU_Decode(MpsD1_Spi_StatRawData);
	Mps_TLE5012D1_STAT.Stat_S_FUSE 	= MpsD1_Stat_S_FUSE_Decode(MpsD1_Spi_StatRawData);
	Mps_TLE5012D1_STAT.Stat_S_MAGOL = MpsD1_Stat_S_MAGOL_Decode(MpsD1_Spi_StatRawData);
	Mps_TLE5012D1_STAT.Stat_S_OV 	= MpsD1_Stat_S_OV_Decode(MpsD1_Spi_StatRawData);

	Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo.D1SpiAngRawData = MpsD1_Spi_AngRawData;

	Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_VR		= Mps_TLE5012D1_STAT.Stat_S_VR ;
	Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_WD		= Mps_TLE5012D1_STAT.Stat_S_WD;
	Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_ROM		= Mps_TLE5012D1_STAT.Stat_S_ROM;
	Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_ADCT	= Mps_TLE5012D1_STAT.Stat_S_ADCT;
	Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_DSPU	= Mps_TLE5012D1_STAT.Stat_S_DSPU;
	Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_FUSE	= Mps_TLE5012D1_STAT.Stat_S_FUSE;	
	Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_MAGOL	= Mps_TLE5012D1_STAT.Stat_S_MAGOL;	
	Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_OV		= Mps_TLE5012D1_STAT.Stat_S_OV;

    /* Output */
    Mps_TLE5012_Hndlr_1ms_Write_Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo(&Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo);
    /*==============================================================================
    * Members of structure Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo 
     : Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo.D1SpiAngRawData;
     : Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo.D2SpiAngRawData;
     : Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo.D1SpiAng36000;
     =============================================================================*/
    
    Mps_TLE5012_Hndlr_1ms_Write_Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo(&Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo);
    /*==============================================================================
    * Members of structure Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo 
     : Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_VR;
     : Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_WD;
     : Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_ROM;
     : Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_ADCT;
     : Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_DSPU;
     : Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_FUSE;
     : Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_MAGOL;
     : Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_OV;
     =============================================================================*/
    
    Mps_TLE5012_Hndlr_1ms_Write_Mps_TLE5012_Hndlr_1msMpsD2SpiAngInfo(&Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsD2SpiAngInfo);
    /*==============================================================================
    * Members of structure Mps_TLE5012_Hndlr_1msMpsD2SpiAngInfo 
     : Mps_TLE5012_Hndlr_1msMpsD2SpiAngInfo.D2SpiAng36000;
     =============================================================================*/
    
    Mps_TLE5012_Hndlr_1ms_Write_Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo(&Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo);
    /*==============================================================================
    * Members of structure Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo 
     : Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_VR;
     : Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_WD;
     : Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_ROM;
     : Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_ADCT;
     : Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_DSPU;
     : Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_FUSE;
     : Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_MAGOL;
     : Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_OV;
     =============================================================================*/
    
    Mps_TLE5012_Hndlr_1ms_Write_Mps_TLE5012_Hndlr_1msMpsInvalid(&Mps_TLE5012_Hndlr_1msBus.Mps_TLE5012_Hndlr_1msMpsInvalid);

    Mps_TLE5012_Hndlr_1ms_Timer_Elapsed = STM0_TIM0.U - Mps_TLE5012_Hndlr_1ms_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define MPS_TLE5012_HNDLR_1MS_STOP_SEC_CODE
#include "Mps_TLE5012_Hndlr_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
