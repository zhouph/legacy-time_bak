/* Virtual main for linking */
/* purpose : Generation of map file */
#include "Mps_TLE5012_Hndlr_1ms.h"
#include "Mps_TLE5012_Hndlr_50us.h"

int main(void)
{
    Mps_TLE5012_Hndlr_1ms_Init();
    Mps_TLE5012_Hndlr_50us_Init();

    while(1)
    {
        Mps_TLE5012_Hndlr_1ms();
        Mps_TLE5012_Hndlr_50us();
    }
}