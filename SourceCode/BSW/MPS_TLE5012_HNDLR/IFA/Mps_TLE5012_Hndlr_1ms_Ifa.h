/**
 * @defgroup Mps_TLE5012_Hndlr_1ms_Ifa Mps_TLE5012_Hndlr_1ms_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mps_TLE5012_Hndlr_1ms_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MPS_TLE5012_HNDLR_1MS_IFA_H_
#define MPS_TLE5012_HNDLR_1MS_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Mps_TLE5012_Hndlr_1ms_Read_Mps_TLE5012_Hndlr_1msEcuModeSts(data) do \
{ \
    *data = Mps_TLE5012_Hndlr_1msEcuModeSts; \
}while(0);


/* Set Output DE MAcro Function */
#define Mps_TLE5012_Hndlr_1ms_Write_Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo(data) do \
{ \
    Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo = *data; \
}while(0);

#define Mps_TLE5012_Hndlr_1ms_Write_Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo(data) do \
{ \
    Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo = *data; \
}while(0);

#define Mps_TLE5012_Hndlr_1ms_Write_Mps_TLE5012_Hndlr_1msMpsD2SpiAngInfo(data) do \
{ \
    Mps_TLE5012_Hndlr_1msMpsD2SpiAngInfo = *data; \
}while(0);

#define Mps_TLE5012_Hndlr_1ms_Write_Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo(data) do \
{ \
    Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo = *data; \
}while(0);

#define Mps_TLE5012_Hndlr_1ms_Write_Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo_D1SpiAngRawData(data) do \
{ \
    Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo.D1SpiAngRawData = *data; \
}while(0);

#define Mps_TLE5012_Hndlr_1ms_Write_Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo_D2SpiAngRawData(data) do \
{ \
    Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo.D2SpiAngRawData = *data; \
}while(0);

#define Mps_TLE5012_Hndlr_1ms_Write_Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo_D1SpiAng36000(data) do \
{ \
    Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo.D1SpiAng36000 = *data; \
}while(0);

#define Mps_TLE5012_Hndlr_1ms_Write_Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_S_VR(data) do \
{ \
    Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_VR = *data; \
}while(0);

#define Mps_TLE5012_Hndlr_1ms_Write_Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_S_WD(data) do \
{ \
    Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_WD = *data; \
}while(0);

#define Mps_TLE5012_Hndlr_1ms_Write_Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_S_ROM(data) do \
{ \
    Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_ROM = *data; \
}while(0);

#define Mps_TLE5012_Hndlr_1ms_Write_Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_S_ADCT(data) do \
{ \
    Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_ADCT = *data; \
}while(0);

#define Mps_TLE5012_Hndlr_1ms_Write_Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_S_DSPU(data) do \
{ \
    Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_DSPU = *data; \
}while(0);

#define Mps_TLE5012_Hndlr_1ms_Write_Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_S_FUSE(data) do \
{ \
    Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_FUSE = *data; \
}while(0);

#define Mps_TLE5012_Hndlr_1ms_Write_Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_S_MAGOL(data) do \
{ \
    Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_MAGOL = *data; \
}while(0);

#define Mps_TLE5012_Hndlr_1ms_Write_Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_S_OV(data) do \
{ \
    Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_OV = *data; \
}while(0);

#define Mps_TLE5012_Hndlr_1ms_Write_Mps_TLE5012_Hndlr_1msMpsD2SpiAngInfo_D2SpiAng36000(data) do \
{ \
    Mps_TLE5012_Hndlr_1msMpsD2SpiAngInfo.D2SpiAng36000 = *data; \
}while(0);

#define Mps_TLE5012_Hndlr_1ms_Write_Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_S_VR(data) do \
{ \
    Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_VR = *data; \
}while(0);

#define Mps_TLE5012_Hndlr_1ms_Write_Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_S_WD(data) do \
{ \
    Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_WD = *data; \
}while(0);

#define Mps_TLE5012_Hndlr_1ms_Write_Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_S_ROM(data) do \
{ \
    Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_ROM = *data; \
}while(0);

#define Mps_TLE5012_Hndlr_1ms_Write_Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_S_ADCT(data) do \
{ \
    Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_ADCT = *data; \
}while(0);

#define Mps_TLE5012_Hndlr_1ms_Write_Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_S_DSPU(data) do \
{ \
    Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_DSPU = *data; \
}while(0);

#define Mps_TLE5012_Hndlr_1ms_Write_Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_S_FUSE(data) do \
{ \
    Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_FUSE = *data; \
}while(0);

#define Mps_TLE5012_Hndlr_1ms_Write_Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_S_MAGOL(data) do \
{ \
    Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_MAGOL = *data; \
}while(0);

#define Mps_TLE5012_Hndlr_1ms_Write_Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_S_OV(data) do \
{ \
    Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_OV = *data; \
}while(0);

#define Mps_TLE5012_Hndlr_1ms_Write_Mps_TLE5012_Hndlr_1msMpsInvalid(data) do \
{ \
    Mps_TLE5012_Hndlr_1msMpsInvalid = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MPS_TLE5012_HNDLR_1MS_IFA_H_ */
/** @} */
