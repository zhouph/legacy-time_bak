/**
 * @defgroup Mps_TLE5012_Hndlr_50us_Ifa Mps_TLE5012_Hndlr_50us_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mps_TLE5012_Hndlr_50us_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MPS_TLE5012_HNDLR_50US_IFA_H_
#define MPS_TLE5012_HNDLR_50US_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Mps_TLE5012_Hndlr_50us_Read_Mps_TLE5012_Hndlr_50usEcuModeSts(data) do \
{ \
    *data = Mps_TLE5012_Hndlr_50usEcuModeSts; \
}while(0);


/* Set Output DE MAcro Function */
#define Mps_TLE5012_Hndlr_50us_Write_Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo(data) do \
{ \
    Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo = *data; \
}while(0);

#define Mps_TLE5012_Hndlr_50us_Write_Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo(data) do \
{ \
    Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo = *data; \
}while(0);

#define Mps_TLE5012_Hndlr_50us_Write_Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo_D1IIFAngRawData(data) do \
{ \
    Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo.D1IIFAngRawData = *data; \
}while(0);

#define Mps_TLE5012_Hndlr_50us_Write_Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo_D1IIFAngDegree(data) do \
{ \
    Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo.D1IIFAngDegree = *data; \
}while(0);

#define Mps_TLE5012_Hndlr_50us_Write_Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo_D2IIFAngRawData(data) do \
{ \
    Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo.D2IIFAngRawData = *data; \
}while(0);

#define Mps_TLE5012_Hndlr_50us_Write_Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo_D2IIFAngDegree(data) do \
{ \
    Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo.D2IIFAngDegree = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MPS_TLE5012_HNDLR_50US_IFA_H_ */
/** @} */
