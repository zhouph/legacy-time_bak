#include "unity.h"
#include "unity_fixture.h"
#include "Mps_TLE5012_Hndlr_1ms.h"
#include "Mps_TLE5012_Hndlr_1ms_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Mom_HndlrEcuModeSts_t UtInput_Mps_TLE5012_Hndlr_1msEcuModeSts[MAX_STEP] = MPS_TLE5012_HNDLR_1MSECUMODESTS;

const Haluint16 UtExpected_Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo_D1SpiAngRawData[MAX_STEP] = MPS_TLE5012_HNDLR_1MSMPSD1SPIANGINFO_D1SPIANGRAWDATA;
const Haluint16 UtExpected_Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo_D2SpiAngRawData[MAX_STEP] = MPS_TLE5012_HNDLR_1MSMPSD1SPIANGINFO_D2SPIANGRAWDATA;
const Haluint16 UtExpected_Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo_D1SpiAng36000[MAX_STEP] = MPS_TLE5012_HNDLR_1MSMPSD1SPIANGINFO_D1SPIANG36000;
const Haluint8 UtExpected_Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_S_VR[MAX_STEP] = MPS_TLE5012_HNDLR_1MSMPSD1SPIDCDINFO_S_VR;
const Haluint8 UtExpected_Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_S_WD[MAX_STEP] = MPS_TLE5012_HNDLR_1MSMPSD1SPIDCDINFO_S_WD;
const Haluint8 UtExpected_Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_S_ROM[MAX_STEP] = MPS_TLE5012_HNDLR_1MSMPSD1SPIDCDINFO_S_ROM;
const Haluint8 UtExpected_Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_S_ADCT[MAX_STEP] = MPS_TLE5012_HNDLR_1MSMPSD1SPIDCDINFO_S_ADCT;
const Haluint8 UtExpected_Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_S_DSPU[MAX_STEP] = MPS_TLE5012_HNDLR_1MSMPSD1SPIDCDINFO_S_DSPU;
const Haluint8 UtExpected_Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_S_FUSE[MAX_STEP] = MPS_TLE5012_HNDLR_1MSMPSD1SPIDCDINFO_S_FUSE;
const Haluint8 UtExpected_Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_S_MAGOL[MAX_STEP] = MPS_TLE5012_HNDLR_1MSMPSD1SPIDCDINFO_S_MAGOL;
const Haluint8 UtExpected_Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_S_OV[MAX_STEP] = MPS_TLE5012_HNDLR_1MSMPSD1SPIDCDINFO_S_OV;
const Haluint16 UtExpected_Mps_TLE5012_Hndlr_1msMpsD2SpiAngInfo_D2SpiAng36000[MAX_STEP] = MPS_TLE5012_HNDLR_1MSMPSD2SPIANGINFO_D2SPIANG36000;
const Haluint8 UtExpected_Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_S_VR[MAX_STEP] = MPS_TLE5012_HNDLR_1MSMPSD2SPIDCDINFO_S_VR;
const Haluint8 UtExpected_Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_S_WD[MAX_STEP] = MPS_TLE5012_HNDLR_1MSMPSD2SPIDCDINFO_S_WD;
const Haluint8 UtExpected_Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_S_ROM[MAX_STEP] = MPS_TLE5012_HNDLR_1MSMPSD2SPIDCDINFO_S_ROM;
const Haluint8 UtExpected_Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_S_ADCT[MAX_STEP] = MPS_TLE5012_HNDLR_1MSMPSD2SPIDCDINFO_S_ADCT;
const Haluint8 UtExpected_Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_S_DSPU[MAX_STEP] = MPS_TLE5012_HNDLR_1MSMPSD2SPIDCDINFO_S_DSPU;
const Haluint8 UtExpected_Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_S_FUSE[MAX_STEP] = MPS_TLE5012_HNDLR_1MSMPSD2SPIDCDINFO_S_FUSE;
const Haluint8 UtExpected_Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_S_MAGOL[MAX_STEP] = MPS_TLE5012_HNDLR_1MSMPSD2SPIDCDINFO_S_MAGOL;
const Haluint8 UtExpected_Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_S_OV[MAX_STEP] = MPS_TLE5012_HNDLR_1MSMPSD2SPIDCDINFO_S_OV;
const Mps_TLE5012_Hndlr_1msMpsInvalid_t UtExpected_Mps_TLE5012_Hndlr_1msMpsInvalid[MAX_STEP] = MPS_TLE5012_HNDLR_1MSMPSINVALID;



TEST_GROUP(Mps_TLE5012_Hndlr_1ms);
TEST_SETUP(Mps_TLE5012_Hndlr_1ms)
{
    Mps_TLE5012_Hndlr_1ms_Init();
}

TEST_TEAR_DOWN(Mps_TLE5012_Hndlr_1ms)
{   /* Postcondition */

}

TEST(Mps_TLE5012_Hndlr_1ms, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Mps_TLE5012_Hndlr_1msEcuModeSts = UtInput_Mps_TLE5012_Hndlr_1msEcuModeSts[i];

        Mps_TLE5012_Hndlr_1ms();

        TEST_ASSERT_EQUAL(Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo.D1SpiAngRawData, UtExpected_Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo_D1SpiAngRawData[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo.D2SpiAngRawData, UtExpected_Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo_D2SpiAngRawData[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo.D1SpiAng36000, UtExpected_Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo_D1SpiAng36000[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_VR, UtExpected_Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_S_VR[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_WD, UtExpected_Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_S_WD[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_ROM, UtExpected_Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_S_ROM[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_ADCT, UtExpected_Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_S_ADCT[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_DSPU, UtExpected_Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_S_DSPU[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_FUSE, UtExpected_Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_S_FUSE[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_MAGOL, UtExpected_Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_S_MAGOL[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo.S_OV, UtExpected_Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_S_OV[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012_Hndlr_1msMpsD2SpiAngInfo.D2SpiAng36000, UtExpected_Mps_TLE5012_Hndlr_1msMpsD2SpiAngInfo_D2SpiAng36000[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_VR, UtExpected_Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_S_VR[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_WD, UtExpected_Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_S_WD[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_ROM, UtExpected_Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_S_ROM[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_ADCT, UtExpected_Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_S_ADCT[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_DSPU, UtExpected_Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_S_DSPU[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_FUSE, UtExpected_Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_S_FUSE[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_MAGOL, UtExpected_Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_S_MAGOL[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo.S_OV, UtExpected_Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_S_OV[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012_Hndlr_1msMpsInvalid, UtExpected_Mps_TLE5012_Hndlr_1msMpsInvalid[i]);
    }
}

TEST_GROUP_RUNNER(Mps_TLE5012_Hndlr_1ms)
{
    RUN_TEST_CASE(Mps_TLE5012_Hndlr_1ms, All);
}
