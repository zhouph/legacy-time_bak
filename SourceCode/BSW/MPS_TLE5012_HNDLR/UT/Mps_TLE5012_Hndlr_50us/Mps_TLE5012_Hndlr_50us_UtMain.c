#include "unity.h"
#include "unity_fixture.h"
#include "Mps_TLE5012_Hndlr_50us.h"
#include "Mps_TLE5012_Hndlr_50us_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Mom_HndlrEcuModeSts_t UtInput_Mps_TLE5012_Hndlr_50usEcuModeSts[MAX_STEP] = MPS_TLE5012_HNDLR_50USECUMODESTS;

const Haluint16 UtExpected_Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo_D1IIFAngRawData[MAX_STEP] = MPS_TLE5012_HNDLR_50USMPSD1IIFANGINFO_D1IIFANGRAWDATA;
const Haluint16 UtExpected_Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo_D1IIFAngDegree[MAX_STEP] = MPS_TLE5012_HNDLR_50USMPSD1IIFANGINFO_D1IIFANGDEGREE;
const Haluint16 UtExpected_Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo_D2IIFAngRawData[MAX_STEP] = MPS_TLE5012_HNDLR_50USMPSD2IIFANGINFO_D2IIFANGRAWDATA;
const Haluint16 UtExpected_Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo_D2IIFAngDegree[MAX_STEP] = MPS_TLE5012_HNDLR_50USMPSD2IIFANGINFO_D2IIFANGDEGREE;



TEST_GROUP(Mps_TLE5012_Hndlr_50us);
TEST_SETUP(Mps_TLE5012_Hndlr_50us)
{
    Mps_TLE5012_Hndlr_50us_Init();
}

TEST_TEAR_DOWN(Mps_TLE5012_Hndlr_50us)
{   /* Postcondition */

}

TEST(Mps_TLE5012_Hndlr_50us, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Mps_TLE5012_Hndlr_50usEcuModeSts = UtInput_Mps_TLE5012_Hndlr_50usEcuModeSts[i];

        Mps_TLE5012_Hndlr_50us();

        TEST_ASSERT_EQUAL(Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo.D1IIFAngRawData, UtExpected_Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo_D1IIFAngRawData[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo.D1IIFAngDegree, UtExpected_Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo_D1IIFAngDegree[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo.D2IIFAngRawData, UtExpected_Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo_D2IIFAngRawData[i]);
        TEST_ASSERT_EQUAL(Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo.D2IIFAngDegree, UtExpected_Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo_D2IIFAngDegree[i]);
    }
}

TEST_GROUP_RUNNER(Mps_TLE5012_Hndlr_50us)
{
    RUN_TEST_CASE(Mps_TLE5012_Hndlr_50us, All);
}
