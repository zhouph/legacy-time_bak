/**
 * @defgroup Mps_TLE5012_Hndlr_50us Mps_TLE5012_Hndlr_50us
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mps_TLE5012_Hndlr_50us.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MPS_TLE5012_HNDLR_50US_H_
#define MPS_TLE5012_HNDLR_50US_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Mps_TLE5012_Hndlr_Types.h"
#include "Mps_TLE5012_Hndlr_Cfg.h"
#include "Mps_TLE5012_Hndlr_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define MPS_TLE5012_HNDLR_50US_MODULE_ID      (0)
 #define MPS_TLE5012_HNDLR_50US_MAJOR_VERSION  (2)
 #define MPS_TLE5012_HNDLR_50US_MINOR_VERSION  (0)
 #define MPS_TLE5012_HNDLR_50US_PATCH_VERSION  (0)
 #define MPS_TLE5012_HNDLR_50US_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Mps_TLE5012_Hndlr_50us_HdrBusType Mps_TLE5012_Hndlr_50usBus;

/* Version Info */
extern const SwcVersionInfo_t Mps_TLE5012_Hndlr_50usVersionInfo;

/* Input Data Element */
extern Mom_HndlrEcuModeSts_t Mps_TLE5012_Hndlr_50usEcuModeSts;

/* Output Data Element */
extern Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo_t Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo;
extern Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo_t Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Mps_TLE5012_Hndlr_50us_Init(void);
extern void Mps_TLE5012_Hndlr_50us(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MPS_TLE5012_HNDLR_50US_H_ */
/** @} */
