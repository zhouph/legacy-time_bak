/**
 * @defgroup Mps_TLE5012_Hndlr_Types Mps_TLE5012_Hndlr_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mps_TLE5012_Hndlr_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MPS_TLE5012_HNDLR_TYPES_H_
#define MPS_TLE5012_HNDLR_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Mom_HndlrEcuModeSts_t Mps_TLE5012_Hndlr_1msEcuModeSts;

/* Output Data Element */
    Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo_t Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo;
    Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_t Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo;
    Mps_TLE5012_Hndlr_1msMpsD2SpiAngInfo_t Mps_TLE5012_Hndlr_1msMpsD2SpiAngInfo;
    Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_t Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo;
    Mps_TLE5012_Hndlr_1msMpsInvalid_t Mps_TLE5012_Hndlr_1msMpsInvalid;
}Mps_TLE5012_Hndlr_1ms_HdrBusType;

typedef struct
{
/* Input Data Element */
    Mom_HndlrEcuModeSts_t Mps_TLE5012_Hndlr_50usEcuModeSts;

/* Output Data Element */
    Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo_t Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo;
    Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo_t Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo;
}Mps_TLE5012_Hndlr_50us_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MPS_TLE5012_HNDLR_TYPES_H_ */
/** @} */
