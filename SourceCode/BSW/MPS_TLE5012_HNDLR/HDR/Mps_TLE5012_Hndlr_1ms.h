/**
 * @defgroup Mps_TLE5012_Hndlr_1ms Mps_TLE5012_Hndlr_1ms
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Mps_TLE5012_Hndlr_1ms.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef MPS_TLE5012_HNDLR_1MS_H_
#define MPS_TLE5012_HNDLR_1MS_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Mps_TLE5012_Hndlr_Types.h"
#include "Mps_TLE5012_Hndlr_Cfg.h"
#include "Mps_TLE5012_Hndlr_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define MPS_TLE5012_HNDLR_1MS_MODULE_ID      (0)
 #define MPS_TLE5012_HNDLR_1MS_MAJOR_VERSION  (2)
 #define MPS_TLE5012_HNDLR_1MS_MINOR_VERSION  (0)
 #define MPS_TLE5012_HNDLR_1MS_PATCH_VERSION  (0)
 #define MPS_TLE5012_HNDLR_1MS_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Mps_TLE5012_Hndlr_1ms_HdrBusType Mps_TLE5012_Hndlr_1msBus;

/* Version Info */
extern const SwcVersionInfo_t Mps_TLE5012_Hndlr_1msVersionInfo;

/* Input Data Element */
extern Mom_HndlrEcuModeSts_t Mps_TLE5012_Hndlr_1msEcuModeSts;

/* Output Data Element */
extern Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo_t Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo;
extern Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo_t Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo;
extern Mps_TLE5012_Hndlr_1msMpsD2SpiAngInfo_t Mps_TLE5012_Hndlr_1msMpsD2SpiAngInfo;
extern Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo_t Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo;
extern Mps_TLE5012_Hndlr_1msMpsInvalid_t Mps_TLE5012_Hndlr_1msMpsInvalid;

extern uint16 MpsD1_Compen_SetFag_at1ms;
/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Mps_TLE5012_Hndlr_1ms_Init(void);
extern void Mps_TLE5012_Hndlr_1ms(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* MPS_TLE5012_HNDLR_1MS_H_ */
/** @} */
