Mps_TLE5012_Hndlr_50usEcuModeSts = Simulink.Bus;
DeList={'Mps_TLE5012_Hndlr_50usEcuModeSts'};
Mps_TLE5012_Hndlr_50usEcuModeSts = CreateBus(Mps_TLE5012_Hndlr_50usEcuModeSts, DeList);
clear DeList;

Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo = Simulink.Bus;
DeList={
    'D1IIFAngRawData'
    'D1IIFAngDegree'
    };
Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo = CreateBus(Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo, DeList);
clear DeList;

Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo = Simulink.Bus;
DeList={
    'D2IIFAngRawData'
    'D2IIFAngDegree'
    };
Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo = CreateBus(Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo, DeList);
clear DeList;

