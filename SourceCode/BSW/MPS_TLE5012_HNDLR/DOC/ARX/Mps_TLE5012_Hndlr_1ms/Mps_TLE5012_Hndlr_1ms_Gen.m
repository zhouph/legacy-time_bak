Mps_TLE5012_Hndlr_1msEcuModeSts = Simulink.Bus;
DeList={'Mps_TLE5012_Hndlr_1msEcuModeSts'};
Mps_TLE5012_Hndlr_1msEcuModeSts = CreateBus(Mps_TLE5012_Hndlr_1msEcuModeSts, DeList);
clear DeList;

Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo = Simulink.Bus;
DeList={
    'D1SpiAngRawData'
    'D2SpiAngRawData'
    'D1SpiAng36000'
    };
Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo = CreateBus(Mps_TLE5012_Hndlr_1msMpsD1SpiAngInfo, DeList);
clear DeList;

Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo = Simulink.Bus;
DeList={
    'S_VR'
    'S_WD'
    'S_ROM'
    'S_ADCT'
    'S_DSPU'
    'S_FUSE'
    'S_MAGOL'
    'S_OV'
    };
Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo = CreateBus(Mps_TLE5012_Hndlr_1msMpsD1SpiDcdInfo, DeList);
clear DeList;

Mps_TLE5012_Hndlr_1msMpsD2SpiAngInfo = Simulink.Bus;
DeList={
    'D2SpiAng36000'
    };
Mps_TLE5012_Hndlr_1msMpsD2SpiAngInfo = CreateBus(Mps_TLE5012_Hndlr_1msMpsD2SpiAngInfo, DeList);
clear DeList;

Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo = Simulink.Bus;
DeList={
    'S_VR'
    'S_WD'
    'S_ROM'
    'S_ADCT'
    'S_DSPU'
    'S_FUSE'
    'S_MAGOL'
    'S_OV'
    };
Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo = CreateBus(Mps_TLE5012_Hndlr_1msMpsD2SpiDcdInfo, DeList);
clear DeList;

Mps_TLE5012_Hndlr_1msMpsInvalid = Simulink.Bus;
DeList={'Mps_TLE5012_Hndlr_1msMpsInvalid'};
Mps_TLE5012_Hndlr_1msMpsInvalid = CreateBus(Mps_TLE5012_Hndlr_1msMpsInvalid, DeList);
clear DeList;

