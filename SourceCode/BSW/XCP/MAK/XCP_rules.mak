# \file
#
# \brief XCP
#
# This file contains the implementation of the BSW
# module XCP.
#
# \author Mando, Advanced R&D, Korea

#################################################################
# REGISTRY

LIBRARIES_TO_BUILD     += XCP_src

XCP_src_FILES       += $(XCP_CORE_PATH)\DAP\vx1000_tc2xx.c

#################################################################
