# \file
#
# \brief XCP
#
# This file contains the implementation of the BSW
# module XCP.
#
# \author Mando, Advanced R&D, Korea

#################################################################
# DEFINITIONS

XCP_CORE_PATH     := $(MANDO_BSW_ROOT)\XCP

CC_INCLUDE_PATH   += $(XCP_CORE_PATH)
CC_INCLUDE_PATH   += $(XCP_CORE_PATH)\DAP

