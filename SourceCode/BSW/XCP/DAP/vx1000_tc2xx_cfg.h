/*------------------------------------------------------------------------------- */
/* vx1000_tc2xx_cfg.h                                                             */
/* Program instrumentation for Vector VX with Tricore DAP                         */
/* Version 2.24  05.09.2013                                                       */
/* Vector Informatik GmbH                                                         */
/*                                                                                */
/* User specific configuration parameters                                         */
/*------------------------------------------------------------------------------- */



/* Note: vx1000_tc2xx_cfg.h serves multiple purposes and is included several times  /
/  with different predefines being set to select the wanted purpose. IT IS INTENTED   /
/  that some lines may be repeatedly processed (namely the "memmap" part) while the   /
/  rest (the configuration part) actually IS PROTECTED from double inclusion even     /
/  though your static code analyzer tool might not see this and state a warning or    /
/  error about lacking protection. Those wrong messages can be safely ignored.       */


/* first the alternative memmap parts */
#if defined(VX1000_BEGSECT_VXSTRUCT_H) && !defined(VX1000_BEGSECT_VXSTRUCT_H_UNDO) /* explicite multiple inclusion */

/* Define the memory section were the global VX1000 data is placed */
/* The address of the variable gVX1000 in this memory section must be transferred to the VxConfig or VxPlugin settings ! */
#define VX1000_STRUCT_ADDR 0xB0005000UL

/* optional section pragmas, defines and/or includes for forcing special linkage in headers. This is compiler dependent: */

#elif defined(VX1000_BEGSECT_VXSTRUCT_C) && !defined(VX1000_BEGSECT_VXSTRUCT_C_UNDO)

/* optional section pragmas, defines and/or includes for forcing special linkage in modules. This is compiler dependent: */

#elif defined(VX1000_ENDSECT_VXSTRUCT_H) && !defined(VX1000_ENDSECT_VXSTRUCT_H_UNDO)

/* optional section pragmas, defines and/or includes for restoring previous linkage in headers. This is compiler dependent: */

#elif defined(VX1000_ENDSECT_VXSTRUCT_C) && !defined(VX1000_ENDSECT_VXSTRUCT_C_UNDO)

/* optional section pragmas, defines and/or includes for restoring previous linkage in modules. This is compiler dependent: */

#elif defined(VX1000_BEGSECT_VXMODULE_H) && !defined(VX1000_BEGSECT_VXMODULE_H_UNDO)

/* optional section pragmas, defines and/or includes for forcing special linkage in headers -- empty in this demo */

#elif defined(VX1000_BEGSECT_VXMODULE_C) && !defined(VX1000_BEGSECT_VXMODULE_C_UNDO)

/* optional section pragmas, defines and/or includes for forcing special linkage in modules -- empty in this demo */

#elif defined(VX1000_ENDSECT_VXMODULE_H) && !defined(VX1000_ENDSECT_VXMODULE_H_UNDO)

/* optional section pragmas, defines and/or includes for restoring previous linkage in headers -- empty in this demo */

#elif defined(VX1000_ENDSECT_VXMODULE_C) && !defined(VX1000_ENDSECT_VXMODULE_C_UNDO)

/* optional section pragmas, defines and/or includes for restoring previous linkage in modules -- empty in this demo */

#elif defined(VX1000_BEGSECT_EMEM_HDR_H) && !defined(VX1000_BEGSECT_EMEM_HDR_H_UNDO)

/* optional section pragmas, defines and/or includes for forcing special linkage in headers -- empty in this demo */

#elif defined(VX1000_BEGSECT_EMEM_HDR_C) && !defined(VX1000_BEGSECT_EMEM_HDR_C_UNDO)

/* optional section pragmas, defines and/or includes for forcing special linkage in modules -- empty in this demo */
#pragma section all "vxEmemIdHeader"

#elif defined(VX1000_ENDSECT_EMEM_HDR_H) && !defined(VX1000_ENDSECT_EMEM_HDR_H_UNDO)

/* optional section pragmas, defines and/or includes for restoring previous linkage in headers -- empty in this demo */

#elif defined(VX1000_ENDSECT_EMEM_HDR_C) && !defined(VX1000_ENDSECT_EMEM_HDR_C_UNDO)

/* optional section pragmas, defines and/or includes for restoring previous linkage in modules -- empty in this demo */
#pragma section all


/* now follows the normal configuration header that protects itself from repeated inclusion */
#elif !defined(VX1000_TC2XX_CFG_H)
#define VX1000_TC2XX_CFG_H



//#define VX1000_DISABLE_INSTRUMENTATION /* for production code the entire VX1000 functionality may be disabled transparently */

#if !defined(VX1000_DISABLE_INSTRUMENTATION)
/*----------------------------------------------------------------------------- */
/* DAP mode */

#define VX1000_OCDS_OIFM 0x00000000 // 2pin DAP (legacy)


/*----------------------------------------------------------------------------- */
/* Define the target ECU identification string (for XCP GET_ID) */
/* This is optional and may be used by CANape to autodetect the A2L file name */

#define VX1000_ECUID         "TC275TU-64F200W"
#define VX1000_ECUID_LEN     15           /* number of actual characters in VX1000_ECUID (_no_ trailing '\0'! */


/*----------------------------------------------------------------------------- */
/* Timestamp clock */

/* A timestamp clock is needed, ... */
/*   1. if DAQ over DATA TRACE is used and timestamps generated by the ECU are desired */
/*   2. if DAQ over JTAG is used (for Nexus class 2+) */
/*   3. to check bypass timeouts */
/*   4. to check coldstart delay timeouts */
/* Note that there is no method to create a free running timer without impact */
/* to the application, so it is up to the user to implement that */

/* Define any free running 32 Bit timer here and specifiy the resolution */

#if !defined(VX1000_ECUREG_STM0_TIM0)
#define VX1000_ECUREG_STM0_TIM0 *(volatile unsigned int*)0xF0000010 /* Use System Timer 0 of CPU0*/
#endif /* !STM_TIM0 */
#define VX1000_CLOCK() (VX1000_UINT32)(VX1000_ECUREG_STM0_TIM0)
#define VX1000_CLOCK_TICKS_PER_MS 100000UL




/*----------------------------------------------------------------------------- */
/* Define parameters for measurement with daq display tables (OLDA) */

/* Turn on OLDA daq  */
//#define VX1000_OLDA
#if defined(VX1000_OLDA)

/* Define a memory location and size where the OLDA (daq display) tables are stored: */
/* (Size needs to be at least the amount of measurement data + 4 Byte for each memory gap) */

/* Option 1: Use 8KByte OVRAM  */
//#define VX1000_OLDA_MEMORY_ADDR 0xAFE80000 /* Must always be non cached ! */
//#define VX1000_OLDA_MEMORY_SIZE 0x2000

/* Option 2: Use 16KByte from EMEM */
//#define VX1000_OLDA_MEMORY_ADDR 0
//#define VX1000_OLDA_MEMORY_SIZE 0

/* Option 3: Use 8KByte memory defined in within VX1000_DATA */
#define VX1000_OLDA_MEMORY_SIZE 0x2000 /* Size in Bytes */

/* Option 4: Share the memory with an existing XCP on CAN driver */
//#include "XCPBasic.h"
//#define VX1000_OLDA_MEMORY_ADDR ((unsigned int)&xcp.Daq) /* Addr */
//#define VX1000_OLDA_MEMORY_SIZE sizeof(xcp.Daq) /* Size in Bytes */

/* Turn on benchmarking of the event copy durations, undef this to save memory */
#define VX1000_OLDA_BENCHMARK

/* Enable Olda overload detection. */
#define VX1000_OLDA_OVERLOAD_DETECTION

/* Define length and offset of size information in Olda V6 transfer table entries */
/*#define VX1000_OLDA_SIZE_LENGTH  6U*/
/*#define VX1000_OLDA_SIZE_OFFSET 21U*/

/* Olda V7 enables access to any ECU memory location, however it requires latest FW version */
#define VX1000_OLDA_FORCE_V7

#endif /* VX1000_OLDA */


/*----------------------------------------------------------------------------- */
/* Define parameters for stimulation and bypassing */

/* Turn on stimulation and bypassing  */
//#define VX1000_STIM

/* Use indirect stimulation to optimize performance against memory usage */
/* Needs additional memory (amount of bypass data + 4 byte per memory gap) */
//#define VX1000_STIM_BY_OLDA

/* Define the event number range for Bypass events */
/* (Each event needs 4 memory bytes) */
/* (Max number range is 0..31) */
#define VX1000_STIM_EVENT_OFFSET    10    /* Index of the first STIM event */
//#define VX1000_STIM_EVENT_COUNT    2    /* Number of STIM events used, starting at VX1000_STIM_EVENT_OFFSET */

/* Turn on benchmarking of the Bypass round trip time */
#define VX1000_STIM_BENCHMARK
#define VX1000_STIM_HISTOGRAM ((VX1000_STIM_EVENT_OFFSET) + 0) /* measure the first/only stim event's timings */


/*----------------------------------------------------------------------------- */
/* Coldstart */

/* Define the maximum coldstart delay during VX1000_INIT_ASYNC_END */
#define VX1000_COLDSTART_TIMEOUT_MS 50

/* Turn on benchmarking of the cold start behavior (undef this to save memory) */

//#define VX1000_COLDSTART_BENCHMARK

//#define VX1000_COLDSTART_CALIBRATION
//#define VX1000_COLDSTART_FLASH_ADDR     0x1234
//#define VX1000_COLDSTART_FLASH_SIZE     64
//#define VX1000_COLDSTART_CHS_BLOCK_SIZE 64


/*----------------------------------------------------------------------------- */
/* Software Reset */

/* Define the maximum delay before doing a ECU softreset (required for handshake with VX) */
#define VX1000_SOFTRESET_TIMEOUT_MS   20


/*----------------------------------------------------------------------------- */
/* Software Reset Detection */
/* Activate this define, if a RSTOUT line to the VX hardware is available which signals SW resets */
#define VX1000_SW_RSTOUT_AVAILABLE
/* If no RSTOUT line is available, the VX1000_INIT() checks for SW resets. */
/* Therefore the whole gVX1000 as well as the OLDA memory have to be located in a no-clear area of the RAM */


/*----------------------------------------------------------------------------- */
/* VX1000 Detection */

#define VX1000_DETECTION
#define VX1000_DETECTION_TIMEOUT_US 50

/*----------------------------------------------------------------------------- */
/* Calibration */

/* Define a FLASH overlay for calibration */
/* Uses Tricore's CalRam feature to map the specified RAM over the specified FLASH location */
/* VX1000 uses the mailbox to control page switching, this can also be done via XCP on CAN */

/* Turn on the FLASH overlay */
//#define VX1000_OVERLAY
#if defined(VX1000_OVERLAY)
#define VX1000_MAILBOX_OVERLAY_CONTROL

#define VX1000_OVERLAY_VX_CONFIGURABLE

#if defined(VX1000_OVERLAY_VX_CONFIGURABLE)
/* Enable the desired overlay features: */
/* Can the application handle keep awakes correctly? */
#define VX1000_OVLENBL_KEEP_AWAKE
/* Has the application synchronous page switching support? */
#define VX1000_OVLENBL_SYNC_PAGESWITCH
/* Does the application want to do core-synchronous page switching? */
#define VX1000_OVLENBL_CORE_SYNC_PAGESW
/* Does the application want to verify the working page before page switch? */
#define VX1000_OVLENBL_VALIDATE_PAGESW
/* Has the Emem a dedicated power supply*/
#define VX1000_OVLENBL_PERSISTENT_EMEM
/* Does the application want to perform a soft-reset after it detected a cal-Wakeup? */
#define VX1000_OVLENBL_RST_ON_CALWAKEUP
/* The epk addr of the WP should be calculated by the VX1000 */
#define VX1000_OVLENBL_USE_VX_EPK_TRANS

/* Configure the paramters for the active features; provide prototype where due: */
#define VX1000_OVL_EPK_LENGTH                                4
#define VX1000_OVL_EPK_REFPAGE_ADDR                          0x0
#define VX1000_OVL_CAL_BUS_MASTER                            0x7UL /* derivative dependent mask of PFLASH busmasters */
#define VX1000_SYNCAL_VALIDATE_WP_CB(addr)                   overlayValidateAddr(addr)
#define VX1000_SYNCAL_USRVALIDATE_WP_CB(value, mask, master) overlayValidateUser(value,mask,master)
#define VX1000_OVL_RST_ON_CAL_WAKEUP_CB()                    calWakeupReset();

#else /* !VX1000_OVERLAY_VX_CONFIGURABLE */

/* Either reuse the XCP on CAN calibration page handlers for overlay control or use your very own routines. */
/*#define VX1000_INIT_CAL_PAGE()*/                       /* empty; must be terminated with a semicolon if nonempty  */
/*#define VX1000_GET_CAL_PAGE(seg, mod)                ApplXcpGetCalPage((seg), (mod))*/
/*#define VX1000_SET_CAL_PAGE(seg, pag, mod)           ApplXcpSetCalPage((seg), (pag), (mod))*/
/*#define VX1000_COPY_CAL_PAGE(sseg, spag, dseg, dpag) ApplXcpCopyCalPage((sseg), (spag), (dseg), (dpag))*/


/*#define VX1000_MAILBOX_OVERLAY_CONTROL*/

#endif /* !VX1000_OVERLAY_VX_CONFIGURABLE */
#endif /* VX1000_OVERLAY */


/*-----------------------------------------------------------------------------*/
/* Resource Management */
//#define VX1000_RES_MGMT

#if defined VX1000_RES_MGMT

extern char _lc_ge_vxEmemIdHeader;

#define VX1000_RES_MGMT_ENABLE_OVL_RAM

#define VX1000_RES_MGMT_OVL_RAM_START (&_lc_ge_vxEmemIdHeader)
#define VX1000_RES_MGMT_OVL_RAM_SIZE (0xBF100000UL - ((unsigned int)&_lc_ge_vxEmemIdHeader))


#define VX1000_RES_MGMT_ENABLE_CFG_ITEM

#define VX1000_RES_MGMT_CFG_ITEM_START    0
#define VX1000_RES_MGMT_CFG_ITEM_LEN      32

#endif /* VX1000_RES_MGMT */

/*----------------------------------------------------------------------------- */
/* Mailbox */

/* Use the mailbox to communicate with the XCP on CAN driver */
/* This is used for printf, page switching, copy page and page relocation */
//#define VX1000_MAILBOX

#define VX1000_MAILBOX_SLOTS (5)
#define VX1000_MAILBOX_SLOT_DWORDS (4UL)
//#define VX1000_MAILBOX_PROVIDE_SPLITREAD
//#define VX1000_MAILBOX_PROVIDE_SPLITWRITE

/*
#define VX1000_MAILBOX_PRINTF
*/
#define VX1000_PRINTF_MINIMAL /* use a reduced printf function instead of the full one */


/*----------------------------------------------------------------------------- */
/* Generic Hooking */

//#define VX1000_HOOK_ENABLE
//#define VX1000_HOOK_COUNT              5


/* ----------------------------------------------------------------------------- */
/* XCP FKL loading support _without_ entering debug mode */
/* these defines are only needed for flash programming if the device cannot be driven externally into debug halt state:      */
//#define VX1000_FKL_SUPPORT_ADDR                TODO       /* 32 aligned bytes in RAM that are not overlapped by FKL space. */
                                                            /* Hint: these bytes may be used during normal operation for     */
                                                            /* purposes. End of RAM is always a good place for them.         */
#if defined(VX1000_FKL_SUPPORT_ADDR)
#define VX1000_DISABLE_ALL_INTERRUPTS()        /* TODO */
#define VX1000_STOP_OS_TIMING_PROTECTION()     /* empty: no Autosar used at all                                              */
#define VX1000_STOP_OTHER_CORES()              /* empty: other cores do not use main core's memory                           */
#define VX1000_ENABLE_STANDARD_RAM_MAPPING()   /* empty: default setting still active                                        */
#define VX1000_SERVE_WATCHDOG(x)               /* empty: watchdog is not active in this application.                         */
                                               /* Hint: must be a macro or an asm function because it must not use stack OR  */
                                               /* it may use stack but then VX1000_ENABLE_STANDARD_RAM_MAPPING() must ensure */
                                               /* that the used stack does not overlap with the FKL space by updating GPR1.  */
#endif /* VX1000_FKL_SUPPORT_ADDR */


/*----------------------------------------------------------------------------- */
/* Multicore system defines */

#define VX1000_MCREG_CORE_ID 0xfe1c  /* CPUn Core Identification Register */
//#define VX1000_MAINCORE 0x0 
#define VX1000_MAINCORE 0x1
#define VX1000_RUNNING_ON_MAINCORE() ( __mfcr(VX1000_MCREG_CORE_ID) == VX1000_MAINCORE)

//#define VX1000_COMPILED_FOR_SLAVECORES


#else /* VX1000_DISABLE_INSTRUMENTATION */

/* As some compilers fail creating completly empty object files or linking those, this is a workaround by making the module non-empty: */
#define VX1000_EMPTYFILE_DUMMYDECL /* empty */
/* the tasking compiler does not know the warning directive, and the message definitely can't be raised to an error */
/*#warning "VX1000 has been disabled: The API of the code instrumentation is redefined to empty statements/empty data structures."*/ /* PRQA S 3115 */ /* Not violating MISRA Rule 19.16 because "warning" IS meaningful */

#endif /* VX1000_DISABLE_INSTRUMENTATION */


#if 0 /* --- template code start --- */
/*===================================================================== */
/* */
/*  Essential VX1000 macros. */
/* */

/* Initialize the global data needed for VX1000 */
/* Must be called after reset during initialisation before any other VX1000_xxxx() routine is called !! */
VX1000_INIT()

/*-------------------------------------- */
/* Measurement data acquisition */

/* Trigger an universal event (OLDA) (0..30) with timestamp */
VX1000_EVENT(DaqEvent)

/*-------------------------------------- */

/* Stimulation and Bypassing */

/* Stimulation control function */
/* Call this periodically in a low priority background task */
VX1000_STIM_CONTROL()

/* Check if stimulation for a stimulation event is turned on (return !=0 true) and bypass */
/* TimeOut is in ms */
/* if (0==VX1000_BYPASS(1, 2, 1000) { function_to_bypass(); } */
r = VX1000_BYPASS(DaqEvent, StimEvent, TimeOut);



/*===================================================================== */
/* */
/* Code instrumentation example: */
/* */

#include "vx1000_tc2xx.h"

void main( void )
{
  /* Initialize the VX1000 system */
  /* Must be called before any other VX1000_xxx macro is called */
  VX1000_INIT()
  ...
  for (;;)
  {
    ...
    if (task0)
    {
      ...
      /* Measurement     */
      VX1000_EVENT(0) /* Trigger XCP measurement event number 0 */

      /* Bypassing */
      /* Event number 2 is the bypass stimulation event, it indicates that */
      /* the ECU is now waiting for completion of the stimulation */
      /* Event number 1 is the bypass trigger event, it is used to trigger */
      /* calculation of the bypass in the tool, as long as the bypass is */
      /* not activated in the tool, this event is not executed */
      /* 2000 is the timeout in ms, in case of timeout, the original code will not be executed */
      if (0==VX1000_BYPASS(1, 2, 2000))
      {
        /* Original code to be bypassed */
        out = function(in);
      }
    }
    /* Call this macro cyclically outside of any bypassed code. */
    VX1000_STIM_CONTROL()
  }
}

void task1(void)
{
   ...
   /* Measurement */
   VX1000_EVENT(7) /* Trigger XCP measurement event number 7 */
}


/*===================================================================== */
#endif /* --- template code stop --- */


#endif /* !VX1000_TC2XX_CFG_H (or any MEMMAP-part) */

