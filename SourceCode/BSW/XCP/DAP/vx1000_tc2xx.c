/*------------------------------------------------------------------------------- */
/* vx1000_tc2xx.c                                                                 */
/* Program instrumentation for Vector VX with Tricore DAP                         */
/* Version 2.24  05.09.2013                                                       */
/* Vector Informatik GmbH                                                         */
/*                                                                                */
/* Don't modify this file, parameters are defined in vx1000_tc2xx_cfg.h           */
/*------------------------------------------------------------------------------- */


/* ----------------------------------------------------------------------------------------------------------------------- /
/ Status of MISRA conformance:                                                                                             /
/ ---------------------------                                                                                              /
/  * advisory and required rules 11.x and 17.x "usage of pointers, pointer arithmetics                                     /
/    and casts between differently typed or differently qualified pointers"                                                /
/     - violated because copying data from and to arbitrary locations is the core feature of this module                   /
/     - see individual justifications of the particular violations in the code                                             /
/                                                                                                                          /
/  * advisory rule 19.1 "location of file inclusions"                                                                      /
/     - violated because there exists no MISRA-conform workaround without loosing essential functionality                  /
/     - see individual justifications of the particular violations in the code                                             /
/                                                                                                                          /
/  * required rule 19.4 "complete statement inside a macro"                                                                /
/     - todo (not worked around yet because it's not clear whether this is a user's requirement or not - see comment)      /
/                                                                                                                          /
/  * advisory rule 19.7 "functions vs. macros"                                                                             /
/     - violated because possible workarounds induce a significant performance loss                                        /
/     - see individual justifications of the particular violations in the code                                             /
/                                                                                                                          /
/  * required rule 19.15 "repeated file inclusion"                                                                         /
/     - just a phantom detection by the checker tool, no real violation                                                    /
/                                                                                                                          /
/  * advisory and required rules 5.2, 8.8, 12.13, 17.4, 19.4                                                               /
/     - only violated in dead code (function calcAdd32() - todo: solve before using this code in new features!)            /
/                                                                                                                          /
/------------------------------------------------------------------------------------------------------------------------ */


/*
TODO: add all function headers according to this template/example:
*/

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_StimTransfer                                                                                         */
/* API name:      none                                                                                                        */
/* Return value:  none                                                                                                        */
/* Parameter1:    eventNumber E [0,gVX1000.Olda.EventCount)                                                                   */
/*                Validity ensured by internal silent abort. // or: {internal assertion, caller }.                            */
/* Preemption:    This function must not be interrupted by any vx1000_* function.                                             */
/*                This function should not interrupt and should not be interrupted by code that reads the stimulation data.   */
/* Termination:   May leave the destination data in an inconsistent state.                                                    */
/*                Internal data stays valid, no problems on reactivation.                                                     */
/* Precondition1: vx1000_OldaInit() must have been called successfully.                                                       */
/* Precondition2: The MMU must be programmed such that the source memory and the destination memory are visible.              */
/* Precondition3: The MPU must be programmed such that the source memory is readable and the destination memory is writable.  */
/* Description:   Processes all transfer descriptors assigned to parameter1, copies data from olda buffer to the destinations.*/
/*----------------------------------------------------------------------------------------------------------------------------*/

/*
TODO: use VX1000_SUFFUN() for all function calls and declarations
*/

#include "vx1000_tc2xx.h" /* PRQA S 0883*/ /* Actually not violating MISRA Rule 19.15: this file is included only once into this module */

#if defined(VX1000_MAILBOX_OVERLAY_CONTROL) || defined(VX1000_MAILBOX)
#if defined(VX1000_MEMCPY)
#define memcopy(D, S, L)         VX1000_MEMCPY((void *)(D), (void *)(S), (L)) /* PRQA S 3453 */ /* Willingly violating MISRA Rule 19.7 to allow the user to replace the library function by a version conforming to MISRA */
#else  /* !VX1000_MEMCPY */
#include <string.h>  /* Needed for memcpy */
#define memcopy(D, S, L)         (void)memcpy((void *)(D), (const void *)(S), (L)) /* PRQA S 3453 */ /* Willingly violating MISRA Rule 19.7 to allow the user to replace the library function by a version conforming to MISRA */
#endif /* !VX1000_MEMCPY */
#endif /* VX1000_MAILBOX_OVERLAY_CONTROL, VX1000_MAILBOX */

#if defined(VX1000_MAILBOX_PRINTF)
#include <stdarg.h>
#if !defined(VX1000_PRINTF_MINIMAL)
#include <stdio.h> /* this violates MISRA Rule 20.9, but the user is responsible to turn off printf in production code while the features still are very useful in debug code */
#endif /* !VX1000_PRINTF_MINIMAL */
#endif /* VX1000_MAILBOX_PRINTF */

#if ((VX1000_FILE_VERSION) != (((/*major*/ 2 /*major*/)<<8)|(/*minor*/ 23 /*minor*/)))
#error "version of vx1000_tc2xx.h does not match version of vx1000_tc2xx.c!"
#endif

#if defined(VX1000_DISABLE_INSTRUMENTATION)

/* Some compilers (e.g. cosmic) fail creating completly empty object files. Allow the user to provide a workaround: */
VX1000_EMPTYFILE_DUMMYDECL

#else /* !VX1000_DISABLE_INSTRUMENTATION */


/* include userdefined lines with optional section pragmas to force individual linkage of VX1000 code and/or data: */
#define VX1000_BEGSECT_VXMODULE_C
#include "vx1000_tc2xx_cfg.h"
#define VX1000_BEGSECT_VXMODULE_C_UNDO


/* --- local prototypes --- */
#if defined(VX1000_STIM_BENCHMARK)
static void vx1000_StimBenchmarkStimCheck( void );
#endif /* VX1000_STIM_BENCHMARK */
#if defined(VX1000_MAILBOX)
#if defined(VX1000_OVERLAY) && !defined(VX1000_COMPILED_FOR_SLAVECORES)
#if !defined(VX1000_INIT_CAL_PAGE)
static void vx1000_InitCalPage( void );
#define VX1000_INIT_CAL_PAGE() vx1000_InitCalPage(); /* PRQA S 3453 */ /* Willingly violating MISRA Rule 19.7 to be able to use reconfigurable API wrappers */
#define VX1000_INIT_CAL_PAGE_INTERNAL
#endif /* !VX1000_INIT_CAL_PAGE */
#if defined(VX1000_OVERLAY_TLB)
static VX1000_UINT8 vx1000_MMUWriteTLBEntry(VX1000_UINT8 n, VX1000_UINT32 ea, VX1000_UINT32 ra, VX1000_UINT32 pagesize,
                                            VX1000_UINT8 cache_inhibit, VX1000_UINT8 cache_writethrough, VX1000_UINT8 permissions);
#elif defined(VX1000_OVERLAY_DESCR_IDX) || defined(VX1000_MPC56xCRAM_BASE_ADDR)
static VX1000_UINT8 vx1000_MapCalRam(VX1000_UINT32 size, VX1000_UINT32 target, VX1000_UINT32 source);
#endif /* VX1000_OVERLAY_DESCR_IDX | VX1000_MPC56xCRAM_BASE_ADDR */
#if !defined(VX1000_GET_CAL_PAGE)
static VX1000_UINT8 vx1000_GetCalPage( VX1000_UINT8 segment, VX1000_UINT8 mode );
#define VX1000_GET_CAL_PAGE(seg, mod) vx1000_GetCalPage((VX1000_UINT8)(seg), (VX1000_UINT8)(mod)) /* PRQA S 3453 */ /* Willingly violating MISRA Rule 19.7 to be able to use reconfigurable API wrappers */
#define VX1000_GET_CAL_PAGE_INTERNAL
#endif /* !VX1000_GET_CAL_PAGE */
#if !defined(VX1000_SET_CAL_PAGE)
static VX1000_UINT8 vx1000_SetCalPage( VX1000_UINT8 segment, VX1000_UINT8 page, VX1000_UINT8 mode, VX1000_UINT8 onStartup);
#define VX1000_SET_CAL_PAGE(seg, pag, mod, stup) vx1000_SetCalPage((VX1000_UINT8)(seg), (VX1000_UINT8)(pag), (VX1000_UINT8)(mod), (VX1000_UINT8)(stup)) /* PRQA S 3453 */ /* Willingly violating MISRA Rule 19.7 to be able to use reconfigurable API wrappers */
#define VX1000_SET_CAL_PAGE_INTERNAL
#endif /* !VX1000_SET_CAL_PAGE */
#if !defined(VX1000_COPY_CAL_PAGE)
static VX1000_UINT8 vx1000_CopyCalPage( VX1000_UINT8 srcSeg, VX1000_UINT8 srcPage, VX1000_UINT8 dstSeg, VX1000_UINT8 dstPage );
#define VX1000_COPY_CAL_PAGE(sseg, spag, dseg, dpag) vx1000_CopyCalPage((VX1000_UINT8)(sseg), (VX1000_UINT8)(spag), (VX1000_UINT8)(dseg), (VX1000_UINT8)(dpag)) /* PRQA S 3453 */ /* Willingly violating MISRA Rule 19.7 to be able to use reconfigurable API wrappers */
#define VX1000_COPY_CAL_PAGE_INTERNAL
#endif /* !VX1000_COPY_CAL_PAGE */
#else /* !VX1000_OVERLAY */
#define VX1000_INIT_CAL_PAGE() /* empty */
#endif /* !VX1000_OVERLAY */
#if defined(VX1000_MAILBOX_PRINTF)
static void vx1000_MailboxPutchar( VX1000_CHAR character );
#endif /* VX1000_MAILBOX_PRINTF */
#endif /* VX1000_MAILBOX */

#if defined(VX1000_STIM)
#if !defined(VX1000_RESET_STIM_TIMEOUT) || !defined(VX1000_CHECK_STIM_TIMEOUT)
static VX1000_UINT32 SetTimeoutUs(VX1000_UINT32 t);
static VX1000_UINT8 CheckTimeout(VX1000_UINT32 timeout);
#endif /* !VX1000_RESET_STIM_TIMEOUT && !VX1000_CHECK_STIM_TIMEOUT */
#endif /* VX1000_STIM */


/*------------------------------------------------------------------------------- */
/* VX1000 initialization */

/* Initialize the global data */
/* Must be called before any other VX1000_XXXX routine is called */
void vx1000_InitAsyncStart(void)
{
#if !defined(VX1000_COMPILED_FOR_SLAVECORES)
  // optional todo: check HW reset state: if no hard reset, assume VX1000-data already/still being valid and skip initialisation ...
#if defined(VX1000_RUNNING_ON_MAINCORE)
  if (VX1000_RUNNING_ON_MAINCORE()) /* always true on singlecore MCU, otherwise needs compiler specific assembly */
#endif /* VX1000_RUNNING_ON_MAINCORE */
  {
	/* Tricore specific trace hardware init: */

#ifndef VX1000_SW_RSTOUT_AVAILABLE
     /* Check if last Reset was a SW Reset */
    if ((VX1000_MCREG_SCU_RSTSTAT & 0x00000010UL) == 0x00000010UL)
    {
      /* Check if the gVX1000 data structure is still valid */
      if ((gVX1000.MagicId == (VX1000_STRUCT_MAGIC)) && (gVX1000.Version == (VX1000_STRUCT_VERSION)))
      {
        VX1000_UINT32 t,n;

        /* Reenable OLDA trace */
        /* Disable OLDA write trap generation */
          VX1000_MCREG_LMU_MEMCON = 0x00000003UL;

        VX1000_SPECIAL_EVENT((VX1000_ECU_EVT_SW_RESET))
        gVX1000.ToolDetectState |= VX1000_TDS_APPRST;

        /* Wait 10ms */
        /* First create enough trace traffic to empty the tile buffer */
        /* Give VX a chance to set the timestamp offset */
        t = VX1000_CLOCK();
        n = 0;
        while ((VX1000_CLOCK()) - t < (VX1000_CLOCK_TICKS_PER_MS) * 10UL)
        {
          for (n++; n < 0x1000UL; n++)  /* Generate max. 64K Trace data */
          {
            VX1000_TIMESTAMP()
            VX1000_TIMESTAMP()
            VX1000_TIMESTAMP()
            VX1000_TIMESTAMP()
            VX1000_TIMESTAMP()
          }
        }
        return; /* Success */
      }
      else
      {
        VX1000_SPECIAL_EVENT((VX1000_ECU_EVT_SW_RESET_FAIL))
      }
    }
#endif /* VX1000_SW_RSTOUT_AVAILABLE */



    /* Initialize the gVX1000 data structure   */
    gVX1000.MagicId = 0;
    VX1000_STORE_TIMESTAMP(VX1000_CLOCK())
    /* VX1000_STORE_EVENT(0) -- Eventnumber is not touched, to be sure no event is triggered */
    gVX1000.Version = VX1000_STRUCT_VERSION; /* note: bits 8..31 are checked by old FW versions to be 0x000000 */
    gVX1000.GetIdPtr = VX1000_ECUID_PTR; /* PRQA S 0311 */ /* GetIdPtr is never used for writing, so MISRA Rule 11.5 is not relevant here */ /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because this integer variable either contains a pointer to an actual object or an integral magic number for "invalid" */  /* PRQA S 0309 */ /* not violating MISRA Rule 11.3 with respect to the type sizes: 32 bit are sufficient to hold either a 0:32 flat or a 10:16 paged pointer */
    gVX1000.GetIdLen = VX1000_ECUID_LEN;
    gVX1000.XcpMailboxPtr = VX1000_MAILBOX_PTR; /* PRQA S 0303 */ /* cannot avoid violating MISRA Rule 11.3 because pointers are transferred compressed and decompression uses integer operations */ /* PRQA S 0309 */ /* not violating MISRA Rule 11.3 with respect to the type sizes: 32 bit are sufficient to hold either a 0:32 flat or a 10:16 paged pointer */
    gVX1000.StimCtrlPtr = VX1000_STIM_PTR; /* PRQA S 0303 */ /* cannot avoid violating MISRA Rule 11.3 because pointers are transferred compressed and decompression uses integer operations */ /* PRQA S 0309 */ /* not violating MISRA Rule 11.3 with respect to the type sizes: 32 bit are sufficient to hold either a 0:32 flat or a 10:16 paged pointer */
    gVX1000.RamSynchField = 0;
    gVX1000.ToolDetectState = 0;
    gVX1000.OldaPtr = VX1000_OLDA_PTR; /* PRQA S 0303 */ /* cannot avoid violating MISRA Rule 11.3 because pointers are transferred compressed and decompression uses integer operations */ /* PRQA S 0309 */ /* not violating MISRA Rule 11.3 with respect to the type sizes: 32 bit are sufficient to hold either a 0:32 flat or a 10:16 paged pointer */
    gVX1000.OvlPtr = VX1000_OVL_PTR; /* PRQA S 0303 */ /* cannot avoid violating MISRA Rule 11.3 because pointers are transferred compressed and decompression uses integer operations */ /* PRQA S 0309 */ /* not violating MISRA Rule 11.3 with respect to the type sizes: 32 bit are sufficient to hold either a 0:32 flat or a 10:16 paged pointer */
    gVX1000.ResMgmtPtr = VX1000_RES_MGMT_PTR; /* PRQA S 0303 */ /* cannot avoid violating MISRA Rule 11.3 because pointers are transferred compressed and decompression uses integer operations */ /* PRQA S 0309 */ /* not violating MISRA Rule 11.3 with respect to the type sizes: 32 bit are sufficient to hold either a 0:32 flat or a 10:16 paged pointer */
    gVX1000.OldaEventNumber = 0;
    gVX1000.CalPtr = 0;
    gVX1000.ToolCtrlState = 0;
    gVX1000.res6 = 0; /* -- will be replaced in v8 by TimestampInfo */
    gVX1000.res7 = 0; /* -- will be replaced in v8 by VersionInfo */

#if 0  /* start of dead code - TODO: we'll use another, yet to be defined method to store the info ... */
    /* ASAM/XCP timestamp info: bits 0..15: TIMESTAMP_TICKS (per UNIT), 16..18: TIMESTAMP_MODE.SIZE (always 4 with VX), /
    /  19: TIMESTAMP_MODE.FIXED (always on with VX), 20..23: TIMESTAMP_MODE.UNIT , 24..31: currently reserved (0x00)   */
//#if   ((VX1000_CLOCK_TICKS_PER_MS) <= 65UL )
//    gVX1000.TimestampInfo = (VX1000_UINT32)(((VX1000_CLOCK_TICKS_PER_S)          ) | (0x009c0000UL)); /* [s] */
//#elif ((VX1000_CLOCK_TICKS_PER_MS) <= 0xFFFFUL )
    gVX1000.TimestampInfo = (VX1000_UINT32)(((VX1000_CLOCK_TICKS_PER_MS)         ) | (0x006c0000UL)); /* [ms] */
//#elif ((VX1000_CLOCK_TICKS_PER_US) <= 0xFFFFUL )
//    gVX1000.TimestampInfo = (VX1000_UINT32)(((VX1000_CLOCK_TICKS_PER_US)         ) | (0x003c0000UL)); /* [us] */
//#else
//    gVX1000.TimestampInfo = (VX1000_UINT32)(((VX1000_CLOCK_TICKS_PER_US) / 1000UL) | (0x000c0000UL)); /* [ns] */
//#endif

    gVX1000.VersionInfo = (VX1000_UINT32)((((VX1000_FILE_VERSION) >> 8) & 0xffUL) | (((VX1000_FILE_VERSION) & 0xffUL) << 8));  /* bits 0..7: instrumentation files' major version, 8..15: minor version, 16..31: reserved (for status, patch version -> currently 0x0000) */
#endif /* end of dead code */

    VX1000_OLDA_INIT()
    VX1000_MAILBOX_INIT()
    VX1000_OVERLAY_INIT()
    VX1000_RES_MGMT_INIT()

#if defined(VX1000_STIM)
    VX1000_STIM_INIT()
    gVX1000.Stim.TimeoutCtr = 0;
    gVX1000.Stim.TimeoutCtr2 = 0;
    VX1000_STIM_BENCHMARK_INIT()
#endif /* VX1000_STIM */
    VX1000_HOOK_INIT()

#if defined(VX1000_MAILBOX) && defined(VX1000_MAILBOX_OVERLAY_CONTROL)
    VX1000_INIT_CAL_PAGE()
#endif /* VX1000_MAILBOX && VX1000_MAILBOX_OVERLAY_CONTROL */


    /* optional: structuresizetest feature */


    /* Lock DAP in 2 PIN mode to improve communication during tool detection and coldstart by the VX1000 */
    /* Register is ENDINIT protected! */
    //VX1000_MCREG_OCDS_OIFM =  0x00000000UL;  /* Two-pin DAP (legacy) */


    gVX1000.ToolDetectState |= (VX1000_UINT32)VX1000_TDS_INIT;
    gVX1000.MagicId = (VX1000_UINT32)VX1000_STRUCT_MAGIC;

    /* tricore specific VX1000 detection */
    VX1000_DETECT_VX_ASYNC_START()

    VX1000_SPECIAL_EVENT(VX1000_EVENT_STRUCT_INIT) /* PRQA S 0303 */ /* cannot avoid violating MISRA Rule 11.3 because the user/compiler typically provides the HW address as an integer (it does not know the VX pointer types) */
  }
#endif /* !VX1000_COMPILED_FOR_SLAVECORES */

  /* optional: VX1000_BreakpointInit(); /-* has to be performed per core */

}

void vx1000_InitAsyncEnd(void)
{
#if defined(VX1000_RUNNING_ON_MAINCORE)
  if (0==VX1000_RUNNING_ON_MAINCORE())
  {
    while (gVX1000.MagicId != (VX1000_UINT32)VX1000_STRUCT_MAGIC)
    {
      ; /* busy wait until another core initialized VX1000_DATA */
    }
  }
#endif /* VX1000_RUNNING_ON_MAINCORE */

  /* VX1000 detection */
  if (VX1000_DETECT_VX_ASYNC_END() != 0)
  {
    /* If tool detection was successfull, DAP is always in 2 pin mode */
  }
  else
  {
    /* Lock DAP in the user selected pin mode */
    /* Register is ENDINIT protected! */
    //VX1000_MCREG_OCDS_OIFM = VX1000_OCDS_OIFM;
  }
}


/* Should not be used in new designs */
/* Only for compatibility in existing projects */
void vx1000_Init(void)
{
  VX1000_INIT_ASYNC_START()
  VX1000_INIT_ASYNC_END()
}


/*------------------------------------------------------------------------------- */
/* Pre-restart handshake */

/* Handshake with VX1000 base module before ECU is doing a software reset */
/* Must be called before software reset is executed */
VX1000_UINT8 vx1000_PrepareSoftreset(void)
{
  VX1000_UINT32 t0;
  VX1000_UINT8 retVal = 0; /* 0="accepted", 1="initiated", 2="acknowledged", 3="handled", 4="confirmed", {5,6}="timeout" */

  if ((gVX1000.ToolCtrlState & (VX1000_TCS_PRESENT)) != 0)  /* Handshake is only needed when a VX is connected */
  {
    VX1000_STORE_TIMESTAMP(VX1000_CLOCK())
    t0 = gVX1000.EventTimestamp;

    /* trigger a special event */
    VX1000_SPECIAL_EVENT(VX1000_ECU_EVT_SW_RESET_PREP) /* PRQA S 0303 */ /* cannot avoid violating MISRA Rule 11.3 because the user/compiler typically provides the HW address as an integer (it does not know the VX pointer types) */
    retVal = 1;

    /* wait with timeout until the VX1000 acknowledges the special event by setting VX1000_TCS_SOFTRESET_PREP */
    while (retVal == 1)
    {
      if ((gVX1000.ToolCtrlState & (VX1000_UINT32)(VX1000_TCS_SOFTRESET_PREP)) != 0)
      {
        /* retVal = 2; */
        gVX1000.MagicId = 0;   /* destroy the MagicId */
        retVal = 3;

        /* wait with timeout until the VX1000 also confirmed that it saw the destroyed MagicId by resetting VX1000_TCS_SOFTRESET_PREP */
        while (retVal == 3)
        {
          if (0==(gVX1000.ToolCtrlState & (VX1000_UINT32)(VX1000_TCS_SOFTRESET_PREP)))
          {
            retVal = 4;
          }
          else if ((gVX1000.EventTimestamp - t0) >= ((VX1000_SOFTRESET_TIMEOUT_MS) * (VX1000_CLOCK_TICKS_PER_MS)))
          {
            retVal = 5; /* means "timeout" */
          }
          else
          {
            ; /* empty else is only here for MISRA */
          }
          VX1000_STORE_TIMESTAMP(VX1000_CLOCK())
        } /* while() */
      }
      else if ((gVX1000.EventTimestamp - t0) >= ((VX1000_SOFTRESET_TIMEOUT_MS) * (VX1000_CLOCK_TICKS_PER_MS)))
      {
        retVal = 6; /* means "timeout" or "rejected" */
      }
      else
      {
        ; /* empty else is only here for MISRA */
      }
      VX1000_STORE_TIMESTAMP(VX1000_CLOCK())
    } /* while() */
    if (retVal == 4) /* reduce the return values to a simplified, boolean meaning */
    {
      retVal = 0;
    }
    else
    {
      retVal = 1;
    }
  }
  return retVal;
}


/*------------------------------------------------------------------------------- */
/* Tool detection */

#if defined(VX1000_DETECTION) || 1 /* on tricore, this function is always present because it is needed in DAP init decission */

#if defined(VX1000_COLDSTART_CALIBRATION)

static VX1000_UINT32 calcAdd32(VX1000_UINT32 addr, VX1000_UINT32 bytes); /* todo: move up */
static VX1000_UINT32 calcAdd32(VX1000_UINT32 addr, VX1000_UINT32 bytes)  /* todo: shall this be used anywhere else? Otherwise why not inlining the few lines into vx1000_CalcFlashChecksum()? */
{
  VX1000_UINT32 sum32 = 0;
#if 0 /* todo: this is just a MISRA compliant but functional inequivalent idea for profiling vs. the #else-code below. Must not be used! */
  VX1000_UINT32 k, *data = (VX1000_UINT32 *)addr; /* PRQA S 0306 */ /* willingly violating MISRA Rule 11.3 here to be able to replace pointer arithmetic by integer arithmetic elsewhere */
  for (k = 0; k <= bytes; k += 4UL * 8UL)
  {
    sum32 += data[0] + data[1] + data[2] + data[3] + data[4] + data[5] + data[6] + data[7];
    data = &data[8];
  }
#else
  VX1000_UINT32 *p, *pl;

  /* Don't change this \/ \/ \/ code, before you are sure that runtime will not be affected !! (note: 18 MISRA violations in this block!) */
  p  = (VX1000_UINT32 *)addr;
  pl = (VX1000_UINT32 *)(addr+bytes);
  do
  {
    sum32 += *p++;
    sum32 += *p++;
    sum32 += *p++;
    sum32 += *p++;
    sum32 += *p++;
    sum32 += *p++;
    sum32 += *p++;
    sum32 += *p++;
  } while (p < pl);
  /* Don't change this /\ /\ /\ code, before you are sure that runtime will not be affected !! */
#endif
  return sum32;
}

/*todo: add check for existence && NZ of the defines and then move everything into the consistency check section in the header*/
#if (VX1000_OLDA_MEMORY_SIZE) < (((VX1000_COLDSTART_FLASH_SIZE) / (VX1000_COLDSTART_CHS_BLOCK_SIZE)) + 4) * 4
#error "VX1000_MEMORY_SIZE too small for FLASH checksum array"
#endif /* VX1000_OLDA_MEMORY_SIZE < ... */
#if ((VX1000_COLDSTART_FLASH_SIZE) % (VX1000_COLDSTART_CHS_BLOCK_SIZE)) != 0
#error "VX1000_COLDSTART_FLASH_SIZE % VX1000_COLDSTART_CHS_BLOCK_SIZE != 0"
#endif /* VX1000_COLDSTART_FLASH_SIZE % VX1000_COLDSTART_CHS_BLOCK_SIZE */

/* todo: add the used global variables to the end of this module plus to the header plus add VX1000_COLDSTART_CHECKSUM_MAGIC */
extern void vx1000_CalcFlashChecksum( void ); /* todo: move to header */
void vx1000_CalcFlashChecksum( void )
{
  VX1000_UINT32 a;
  VX1000_UINT32 i, *p;

   p = (VX1000_UINT32*)gVX1000.Olda.MemoryAddr; /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because pointers are transferred compressed and decompression uses integer operations */
   p[0] = (VX1000_UINT32)(VX1000_COLDSTART_CHS_MAGIC);
   p[1] = (VX1000_UINT32)(VX1000_COLDSTART_FLASH_ADDR);
   p[2] = (VX1000_UINT32)(VX1000_COLDSTART_FLASH_SIZE);
   p[3] = (VX1000_UINT32)(VX1000_COLDSTART_CHS_BLOCK_SIZE);
  p = (VX1000_UINT32*)(gVX1000.Olda.MemoryAddr + 4*4); /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because this integer variable either contains a pointer to an actual object or an integral macig number for "invalid" */
   a = (VX1000_UINT32)(VX1000_COLDSTART_FLASH_ADDR);
   for (i = 0; i < ((VX1000_COLDSTART_FLASH_SIZE) / (VX1000_COLDSTART_CHS_BLOCK_SIZE)); i++)
   {
      p[i] = calcAdd32(a, (VX1000_COLDSTART_CHS_BLOCK_SIZE));
      a += VX1000_COLDSTART_CHS_BLOCK_SIZE;
   }
}

#endif /* VX1000_COLDSTART_CALIBRATION */

/* Detect, if a VX1000 is connected */
/* Delay for coldstart measurement setup, if required by tool */
/* Returns VX1000_TDS_DETECTED if detected */
/* Must be called after Tricore's ENDINIT is set */
void vx1000_DetectVxAsyncStart( void )
{
  VX1000_UINT32 toolHandshakeData; /* only on tricore: */
#if defined(VX1000_DETECT_XCP)
  /* Check whether there is a XCP on CAN connected and disable VX OLDA in that case */
  /* (Otherwise the probably-shared memory for the display tables would be overwritten by VX coldstart setup!)*/
  if ((xcp.SessionStatus & (SS_CONNECTED)) != 0)
  {
    gVX1000.Olda.MagicId = 0; /* XCP connected: Disable OLDA */
  }
  else
  {
    gVX1000.Olda.MagicId = VX1000_OLDA_MAGIC; /* VX is alone: Reenable OLDA */
    /* todo: the VX will not start using olda now by itself -> we have to request this somehow */
  }
#endif /* VX1000_DETECT_XCP */

  /* Check if 1st call after reset (&& on multicore systems, whether we run on the main core) */
  if ((0==(gVX1000.ToolDetectState & ((VX1000_UINT32)VX1000_TDS_DETECT))) /* PRQA S 3415*/ /* Actually not violating MISRA Rule 12.4 because VX1000_RUNNING_ON_MAINCORE() by definition cannot have any desired side effect */
#if defined(VX1000_RUNNING_ON_MAINCORE)
      &&  (VX1000_RUNNING_ON_MAINCORE()) /* TODO: when not running on the main core, return gVX1000.ToolDetectState only if VX1000_DATA looks "already initialized"! */
#endif /* VX1000_RUNNING_ON_MAINCORE */
      )
  {
    VX1000_STORE_TIMESTAMP(VX1000_CLOCK())
    gVX1000.ToolDetectState |= (VX1000_UINT32)VX1000_TDS_DETECT;
    gVX1000_DETECT_StartTime = gVX1000.EventTimestamp;

#if defined(VX1000_COLDSTART_BENCHMARK)
    gVX1000_DETECT_StartTimeAsyncEnd = 0;
    gVX1000_DETECT_ToolDetectTime = 0;
    gVX1000_DETECT_ChecksumDoneTime = 0;
    gVX1000_DETECT_EndTime = 0;
    gVX1000_DETECT_EndTimeAsyncStart = 0;
#endif /* VX1000_COLDSTART_BENCHMARK */

    /* Wait some time for tool presence indication, when ECU was reset, the tool may need some time to react */
    /* Check timeout VX1000_DETECTION_TIMEOUT_US */
    /* Thanks to Tricore's DAP interface, VX1000_DETECTION_TIMEOUT_US may be as small as 1 us */
    /* (VX can access the TRIG register even if it can't access memory (gVX1000.ToolCtrlState/DetectState), yet) */
    do
    {
      /* Check tool presence bits */
      toolHandshakeData = VX1000_HANDSHAKE_TRIG_REG;
      if (toolHandshakeData & VX1000_HANDSHAKE_VX_PRESENT)
      {
#if defined(VX1000_OVLENBL_KEEP_AWAKE)
        if(toolHandshakeData & VX1000_HANDSHAKE_CALWAK_REQ)
        {
          // Set the CAL_WAKEUP bit in ToolCtrlState so it can be checked by VX1000_CAL_WAKEUP_REQUESTED().
          gVX1000.ToolCtrlState |= ((VX1000_UINT32)(VX1000_TCS_CAL_WAKEUP));
#ifdef VX1000_OVLENBL_RST_ON_CALWAKEUP
          VX1000_OVL_RST_ON_CAL_WAKEUP_CB()
#endif /* VX1000_OVLENBL_RST_ON_CALWAKEUP */

          /* Decrement the presenceCounter so that is cal_wakeup_active returns 1 the next time */
          gVX1000.Ovl.ecuLastPresenceCounter--;
        }
        if(toolHandshakeData & VX1000_HANDSHAKE_SWITCH_TO_WP_REQ)
        {
          gVX1000.ToolCtrlState |= (VX1000_UINT32)(VX1000_TCS_SWITCH_TO_WP);
        }
#endif /* VX1000_OVLENBL_KEEP_AWAKE */

        /* Set tool detected bit in gVX1000.ToolDetectState */
        gVX1000.ToolDetectState |= (VX1000_UINT32)(VX1000_TDS_DETECTED);

        /* Acknowledge tool detect in TRIG register */
        VX1000_MCREG_OCDS_TRIGS = (VX1000_HANDSHAKE_TRIG_REG_OFFSET + VX1000_HANDSHAKE_VX_DETECTED_ACK);

        /* CBS_OSTATE.OJC[3..1]  0x001 for No_Powerfail, 0x011 for Powerfail */

        /* Check, if tool requested a (coldstart) init delay */
        if(toolHandshakeData & VX1000_HANDSHAKE_VX_INIT_DELAY)
        {
          gVX1000.ToolDetectState |= (VX1000_UINT32)(VX1000_TDS_COLDSTART_DELAY_REQ);
        }
#if defined(VX1000_COLDSTART_BENCHMARK)
        gVX1000_DETECT_ToolDetectTime = gVX1000.EventTimestamp;
#endif /* VX1000_COLDSTART_BENCHMARK */

        break; /* note: this "early-out"-break can be removed safely if constant runtime is preferred (or for MISRA) */
      } /* Tool present */
      VX1000_STORE_TIMESTAMP(VX1000_CLOCK())
    } while ((gVX1000.EventTimestamp - gVX1000_DETECT_StartTime) < ((VX1000_DETECTION_TIMEOUT_US) * (VX1000_CLOCK_TICKS_PER_US)));
    gVX1000.ToolDetectState |= (VX1000_UINT32)VX1000_TDS_DETECT_DONE;
#if defined(VX1000_COLDSTART_BENCHMARK)
    gVX1000_DETECT_EndTimeAsyncStart = VX1000_CLOCK();  /* here, gVX1000.EventTimestamp may be outdated (break's path) */
#endif /* VX1000_COLDSTART_BENCHMARK */
  } /* 1st call */
  else
  {
    /* Check if the VX is connected and update the state */
    // TODO: set or clear bits in gVX1000.ToolDetectState depending on toolHandshakeData ...
  }
/* optional: VX1000_DETECT_XCP (disable VX OLDA in case of XCP on CAN) */
}


/* Stall for remaining delay if required by tool (for downloading coldstart measurement setup) */
/* Update connection status */
/* Return VX1000_TDS_DETECTED if detected */
/* Note: vx1000_DetectVxAsyncStart() must have been called beforehand */
VX1000_UINT8 vx1000_DetectVxAsyncEnd( void )
{
#if defined(VX1000_RUNNING_ON_MAINCORE)
  if (0==(VX1000_RUNNING_ON_MAINCORE()))
  {
    if ((gVX1000.ToolDetectState & (VX1000_UINT32)((VX1000_TDS_COLDSTART_DELAY_REQ) | (VX1000_TDS_COLDSTART_DELAY))) != 0)
    {
      while (0 == (gVX1000.ToolDetectState & (VX1000_UINT32) ((VX1000_TDS_COLDSTART_DONE) | (VX1000_TDS_COLDSTART_TIMEOUT))))
      {
        ; /* busy wait for the main core to complete the handshake */
      }
    }
  }
  else
#endif /* VX1000_RUNNING_ON_MAINCORE */
  if ( (0 != (gVX1000.ToolDetectState & (VX1000_UINT32)VX1000_TDS_COLDSTART_DELAY_REQ)) && (0 == (gVX1000.ToolDetectState & (VX1000_UINT32)VX1000_TDS_COLDSTART_DELAY)) )
  {
    VX1000_STORE_TIMESTAMP(VX1000_CLOCK())

#if defined(VX1000_COLDSTART_BENCHMARK)
    gVX1000_DETECT_StartTimeAsyncEnd = gVX1000.EventTimestamp;
#endif /* VX1000_COLDSTART_BENCHMARK */


    gVX1000.ToolDetectState |= (VX1000_UINT32)VX1000_TDS_COLDSTART_DELAY;

    /* Calculate checksums over the calibration areas and store in Olda memory */
    /* VX1000 may use this to optimize calibration download */
#if defined(VX1000_COLDSTART_CALIBRATION)
    gVX1000.ToolDetectState |= (VX1000_UINT32)VX1000_TDS_COLDSTART_CHS_BUSY;
    vx1000_CalcFlashChecksum();
    gVX1000.ToolDetectState |= (VX1000_UINT32)VX1000_TDS_COLDSTART_CHS_DONE;
#if defined(VX1000_COLDSTART_BENCHMARK)
    gVX1000_DETECT_ChecksumDoneTime = VX1000_CLOCK();  /* here, gVX1000.EventTimestamp is outdated */
#endif /* VX1000_COLDSTART_BENCHMARK */
#endif /* VX1000_COLDSTART_CALIBRATION */

    gVX1000.ToolDetectState &= (VX1000_UINT32)~((VX1000_TDS_COLDSTART_DONE) | (VX1000_TDS_COLDSTART_TIMEOUT));
    do
    {
      if ((gVX1000.ToolCtrlState & ((VX1000_UINT32)(VX1000_TCS_COLDSTART_DONE))) != 0)
      {
        /* Init acknowledge */
#if defined(VX1000_MAILBOX_OVERLAY_CONTROL)
        if ((gVX1000.ToolCtrlState & ((VX1000_UINT32)(VX1000_TCS_SWITCH_TO_WP))) != 0)
        {
          if (VX1000_SET_CAL_PAGE(0, 1, (VX1000_CAL_ECU) | (VX1000_CAL_XCP), 1) != 0)
          {
            gVX1000.ToolDetectState |= (VX1000_UINT32)(VX1000_TDS_ERROR);
          }
        }
#endif /* VX1000_MAILBOX_OVERLAY_CONTROL */
        gVX1000.ToolDetectState |= (VX1000_UINT32)(VX1000_TDS_COLDSTART_DONE);
      }
      else if ((gVX1000.EventTimestamp - gVX1000_DETECT_StartTime) >= ((VX1000_COLDSTART_TIMEOUT_MS) * (VX1000_CLOCK_TICKS_PER_MS)))
      {
        /* Timeout acknowledge */
        gVX1000.ToolDetectState |= (VX1000_UINT32)(VX1000_TDS_COLDSTART_TIMEOUT);
      }
      else
      {
        ; /* empty else only here for MISRA */
      }
      VX1000_STORE_TIMESTAMP(VX1000_CLOCK())
     } while (0 == (gVX1000.ToolDetectState & (VX1000_UINT32) ((VX1000_TDS_COLDSTART_DONE) | (VX1000_TDS_COLDSTART_TIMEOUT))));/* do */

#if defined(VX1000_COLDSTART_BENCHMARK)
    gVX1000_DETECT_EndTime = VX1000_CLOCK();  /* here, gVX1000.EventTimestamp may be outdated (break's path) */
#endif /* VX1000_COLDSTART_BENCHMARK */
  }
#if defined(VX1000_RUNNING_ON_MAINCORE)
  else
  {
    ; /* this dummy else case is here only for MISRA */
  }
#endif /* VX1000_RUNNING_ON_MAINCORE */

  return (VX1000_UINT8)(gVX1000.ToolDetectState & (VX1000_UINT32)(VX1000_TDS_DETECTED));
}
#endif /* VX1000_DETECTION */



/*------------------------------------------------------------------------------- */
/* Breakpoint triggered actions */

/* optional: breakpoint externals and implementation */



/*------------------------------------------------------------------------------- */
/* Measurement data acquisition */

/* Copy routine */
/* Event number is limited to 0..30, event number 31 is reserved, >= 32 is not possible  */
/* Address must be dword aligned */

#if defined(VX1000_OLDA)

/* Initialize the olda data structure  */
void vx1000_OldaInit( void )
{
   /* This is done by VX1000, only if OLDA is really used  */
   /* OVRCON = 3; Disable OLDA write trap generation */

  gVX1000.Olda.Version = VX1000_OLDA_VERSION;
  gVX1000.Olda.Running = 0;
#if !defined(VX1000_OLDA_MEMORY_ADDR)
  gVX1000.Olda.MemoryAddr = (VX1000_UINT32)&gVX1000.Olda.Data[0]; /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because pointers are transferred compressed and decompression uses integer operations */ /* PRQA S 0309 */ /* not violating MISRA Rule 11.3 with respect to the type sizes: 32 bit are sufficient to hold either a 0:32 flat or a 10:16 paged pointer */
#else /* VX1000_OLDA_MEMORY_ADDR */
  gVX1000.Olda.MemoryAddr = VX1000_OLDA_MEMORY_ADDR;
#endif /* VX1000_OLDA_MEMORY_ADDR */
  gVX1000.Olda.MemorySize = VX1000_OLDA_MEMORY_SIZE;
  gVX1000.Olda.EventCount = 0;
  gVX1000.Olda.EventList = 0;
  gVX1000.Olda.TransferList = 0;
  gVX1000.Olda.SizeLengthNOffset = (VX1000_OLDA_SIZE_OFFSET) | ((VX1000_OLDA_SIZE_LENGTH) << 5);
  gVX1000.Olda.SizeSwapValue = VX1000_OLDA_SIZE_SWAP_VALUE;
  gVX1000.Olda.OldaFeatures = (VX1000_FEAT_OLDA_STIM)
                            | (VX1000_FEAT_OLDA_OVERLOADDETECT);
  gVX1000.Olda.Res1 = 0;
  gVX1000.Olda.Res2 = 0;

#if defined(VX1000_OLDA_BENCHMARK)
  {
    VX1000_UINT32 i;
    for (i = 0; i < VX1000_OLDA_EVENT_COUNT; i++) { gVX1000_OLDA_Duration[i] = 0; }
  }
#endif /* VX1000_OLDA_BENCHMARK */

  gVX1000.Olda.MagicId = VX1000_OLDA_MAGIC;
}


/* Trigger OLDA event (Copy routine) */
void vx1000_OldaEvent( VX1000_UINT8 eventNumber )
{
  VX1000_UINT8 size;
  VX1000_UINT32 *src = 0; /* note: few compilers warn about unused initialisation but more would warn if it was missing */ /* PRQA S 3197 */
  VX1000_UINT32 *dst;
  VX1000_UINT16 i, n;
  VX1000_OLDA_EVENT_T *event;
#if defined(VX1000_OLDA_FORCE_V7)
  VX1000_UINT32 j, sizeList;
#endif /* VX1000_OLDA_FORCE_V7 */

  /* Take the timestamp first to have maximum precision */
  /* This timestamp will be needed for data trace as well */
  VX1000_STORE_TIMESTAMP((VX1000_CLOCK()))

  if ((gVX1000.Olda.Running == 1) && (gVX1000.Olda.MagicId == VX1000_OLDA_MAGIC))
  {
    /* OLDA is running */
    if (! (eventNumber >= gVX1000.Olda.EventCount) )
    {
      /* the eventNumer is valid */
      event = &gVX1000.Olda.EventList[eventNumber];
      n = event->TransferCount;
#if defined(VX1000_OLDA_BENCHMARK)
      gVX1000_OLDA_TransferSize[eventNumber]  = 0; /* Size of all transfers */
      gVX1000_OLDA_TransferCount[eventNumber] = n; /* Count of the tranfers */
#endif /* VX1000_OLDA_BENCHMARK */

      if (! (0==n) )
      {
        /* eventNumber is active, there's data to be copied */

        /* Take an individual copy of the timestamp for each event, gVX1000.EventTimestamp mustn't have changed meanwhile */
        event->EventTimestamp = gVX1000.EventTimestamp;
#if defined(VX1000_OLDA_OVERLOAD_DETECTION)
        /* Increase the Event-Counter to detect Overruns */
        event->EventCounter++;
#endif /* VX1000_OLDA_OVERLOAD_DETECTION */

        /* Copy data to an intermediate location */
        i = event->TransferIndex;
        dst = (VX1000_UINT32*)event->TransferDest; /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because pointers are transferred compressed and decompression uses integer operations */
        while (n > 0)
        {
          n--;
          src = (VX1000_UINT32*)gVX1000.Olda.TransferList[i]; /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because pointers are transferred compressed and decompression uses integer operations */
          i++;
#if defined(VX1000_OLDA_FORCE_V7)
          size = (VX1000_UINT8)((VX1000_UINT32)src & 0x00000003UL); /* PRQA S 0306 */ /* willingly violating MISRA Rule 11.3 to avoid recomputation of integer bits already being present in the pointer variable */ /* PRQA S 0309 */ /* not violating MISRA Rule 11.3 with respect to the type sizes: 32 bit are sufficient to hold either a 0:32 flat or a 10:16 paged pointer */
          if ( 0==size )
          {
            /* TransferList entry consists of up to 5 transfer sizes [31..2] and size list identifier [1..0]==0 */
            sizeList = (VX1000_UINT32)src >> 2; /* PRQA S 0306 */ /* willingly violating MISRA Rule 11.3 to avoid recomputation of integer bits already being present in the pointer variable */ /* PRQA S 0309 */ /* not violating MISRA Rule 11.3 with respect to the type sizes: 32 bit are sufficient to hold either a 0:32 flat or a 10:16 paged pointer */
            n++; /* undo decrement of remaining transfers because size list entry isn't a transfer */
            for (j = 0; (j < 5) && (n > 0); j++)
            {
              size = (VX1000_UINT8)(sizeList & 0x3FUL);
              sizeList >>= 6U;
              n--;
              src = (VX1000_UINT32*)gVX1000.Olda.TransferList[i]; /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because pointers are transferred compressed and decompression uses integer operations */
              i++;
#if defined(VX1000_OLDA_BENCHMARK)
              gVX1000_OLDA_TransferSize[eventNumber] += size; /* Size of all transfers */
#endif /* VX1000_OLDA_BENCHMARK */
#if defined(VX1000_MEMCPY)
              VX1000_MEMCPY(dst, src, size << 2);  /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because this callback function may be declared either with or without memory qualifiers by the user */
              dst += size; /* PRQA S 0488 */ /* cannot avoid violating MISRA Rule 17.4 because memcopy based on computed pointers is a core feature of this application */
#else /* !VX1000_MEMCPY */
              while (size != 0)
              {
                *dst = *src;
                dst++; /* PRQA S 0489 */ /* cannot avoid violating MISRA Rule 17.4 because memcopy and thus pointer incrementation is a core feature of this application */
                src++; /* PRQA S 0489 */ /* cannot avoid violating MISRA Rule 17.4 because memcopy and thus pointer incrementation is a core feature of this application */
                size--;
              } /* while size */
#endif /* !VX1000_MEMCPY */
            }
          }
          else
          {
            /* TransferList entry consists of source address [31..2] and transfer size [1..0] in 32-bit-words */
            src = (VX1000_UINT32*)((VX1000_UINT32)src ^ size); /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because pointers are transferred compressed and decompression uses integer operations */ /* PRQA S 0309 */ /* not violating MISRA Rule 11.3 with respect to the type sizes: 32 bit are sufficient to hold either a 0:32 flat or a 10:16 paged pointer */
#if defined(VX1000_OLDA_BENCHMARK)
            gVX1000_OLDA_TransferSize[eventNumber] += size; /* Size of all transfers */
#endif /* VX1000_OLDA_BENCHMARK */
#if defined(VX1000_MEMCPY)
            VX1000_MEMCPY(dst, src, size << 2); /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because this callback function may be declared either with or without memory qualifiers by the user */
            dst += size; /* PRQA S 0488 */ /* cannot avoid violating MISRA Rule 17.4 because memcopy based on computed pointers is a core feature of this application */
#else /* !VX1000_MEMCPY */
            while (size != 0)
            {
              *dst = *src;
              dst++; /* PRQA S 0489 */ /* cannot avoid violating MISRA Rule 17.4 because memcopy and thus pointer incrementation is a core feature of this application */
              src++; /* PRQA S 0489 */ /* cannot avoid violating MISRA Rule 17.4 because memcopy and thus pointer incrementation is a core feature of this application */
              size--;
            } /* while size */
#endif /* !VX1000_MEMCPY */
          }
#else /* !VX1000_OLDA_FORCE_V7 */
          size = (VX1000_UINT8)( ((VX1000_UINT32)src & (VX1000_OLDA_SIZE_MASK)) >> (VX1000_OLDA_SIZE_OFFSET) );
          src = (VX1000_UINT32*)( ( (VX1000_UINT32)src & (~(VX1000_OLDA_SIZE_MASK)) ) | (VX1000_OLDA_SIZE_REPLACEMENT) );
#if defined(VX1000_OLDA_BENCHMARK)
          gVX1000_OLDA_TransferSize[eventNumber] += size; /* Size of all transfers */
#endif /* VX1000_OLDA_BENCHMARK */
#if defined(VX1000_MEMCPY)
          VX1000_MEMCPY(dst, src, size << 2); /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because this callback function may be declared either with or without memory qualifiers by the user */
          dst += size; /* PRQA S 0488 */ /* cannot avoid violating MISRA Rule 17.4 because memcopy based on computed pointers is a core feature of this application */
#else /* !VX1000_MEMCPY */
          while (size != 0)
          {
            *dst = *src;
            dst++; /* PRQA S 0489 */ /* cannot avoid violating MISRA Rule 17.4 because memcopy and thus pointer incrementation is a core feature of this application */
            src++; /* PRQA S 0489 */ /* cannot avoid violating MISRA Rule 17.4 because memcopy and thus pointer incrementation is a core feature of this application */
            size--;
          } /* while size */
#endif /* !VX1000_MEMCPY */
#endif /* !VX1000_OLDA_FORCE_V7 */
        } /* while n*/
#if defined(VX1000_OLDA_OVERLOAD_DETECTION)
        /* Increase the Event-Counter to detect Overruns */
        event->EventCounter++;
#endif /* VX1000_OLDA_OVERLOAD_DETECTION */

        /* Trigger the VX1000 to copy the data */
        VX1000_MCREG_OCDS_TRIGS = eventNumber; /* Tricore specific replacement for gVX1000.OldaEventNumber ^= (1UL << eventNumber); */
      } /* if n */
    } /* if eventNumber */
#if defined(VX1000_OLDA_BENCHMARK)
    /* Timing measurement */
    gVX1000_OLDA_Duration[eventNumber] = (VX1000_CLOCK()) - gVX1000.EventTimestamp;
#endif /* VX1000_OLDA_BENCHMARK */
  } /* if OLDA */

  /* Trigger a trace event */
  VX1000_STORE_EVENT(eventNumber) /* Had been removed due to EM00034754 but was readded due to EM00035042 */
  return;
}

#if defined(VX1000_STIM_BY_OLDA)

/* Trigger an STIM request event */
/* A STIM request is not associated to DAQ data, it is associated to STIM data */
void vx1000_OldaStimRequestEvent( VX1000_UINT8 eventNumber )
{
  VX1000_OLDA_EVENT_T *event;

  VX1000_STORE_TIMESTAMP((VX1000_CLOCK()))

  if ((gVX1000.Olda.Running == 1) && (gVX1000.Olda.MagicId == VX1000_OLDA_MAGIC))
  {
    /* OLDA is running */
    if (eventNumber < gVX1000.Olda.EventCount)
    {
      /* eventNumer is valid */
      event = &gVX1000.Olda.EventList[eventNumber];

      /* Take an individual copy of the timestamp for each event */
      event->EventTimestamp = gVX1000.EventTimestamp;
#if defined(VX1000_OLDA_OVERLOAD_DETECTION)
      /* Increase the Event-Counter by two for Overrun detection */
      event->EventCounter += 2;
#endif /* VX1000_OLDA_OVERLOAD_DETECTION */

      /* Trigger the VX1000 to copy the data */
      VX1000_MCREG_OCDS_TRIGS = eventNumber;
    }
  }
  VX1000_STORE_EVENT(eventNumber)
}


#endif /* VX1000_STIM_BY_OLDA */

#endif /* VX1000_OLDA */



/*------------------------------------------------------------------------------- */
/* Bypassing */

#if defined(VX1000_STIM)

#if defined(VX1000_STIM_BY_OLDA)
/* copy OLDA STIM data */
static void vx1000_StimTransfer( VX1000_UINT8 eventNumber );
static void vx1000_StimTransfer( VX1000_UINT8 eventNumber )
{
  VX1000_UINT8 size;
  VX1000_UINT8 *src;
  VX1000_UINT8 *dst;
  VX1000_UINT16 i, n;
  VX1000_OLDA_EVENT_T *event;
#if defined(VX1000_OLDA_FORCE_V7)
  VX1000_UINT32 j = 0, sizeList = 0; /* sizeList dummy initialisation to avoid compiler warnings */
#endif /* VX1000_OLDA_FORCE_V7 */
#if defined(VX1000_OLDA_BENCHMARK) /* Timing measurement */
  VX1000_UINT32 t0;

  t0 = VX1000_CLOCK();
#endif /* VX1000_OLDA_BENCHMARK */
  if ((gVX1000.Olda.Running == 1) && (gVX1000.Olda.MagicId == VX1000_OLDA_MAGIC))
  {
    /* OLDA is running */
    if (eventNumber < gVX1000.Olda.EventCount)
    {
      /* eventNumer is valid */
      event = &gVX1000.Olda.EventList[eventNumber];
      src = (VX1000_UINT8*)event->TransferDest; /* yes: src is loaded from dst entry for stim */ /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because pointers are transferred compressed and decompression uses integer operations */
      n = event->TransferCount;
#if defined(VX1000_OLDA_BENCHMARK)
      gVX1000_OLDA_TransferSize[eventNumber]  = 0; /* Size of all transfers */
      gVX1000_OLDA_TransferCount[eventNumber] = n; /* Count of the tranfers */
#endif /* VX1000_OLDA_BENCHMARK */
      for (i = event->TransferIndex; (i == i) && (n > 0); i++) /* yes: check n, not i */
      {
        /* eventNumber is active: data is to be copied */
#if defined(VX1000_OLDA_FORCE_V7)
        /* TransferList entry consists of up to 4 transfer sizes, followed by up to 4 pure transfer address entries */
        if (j==0)
        {
          j = 4;
          sizeList = gVX1000.Olda.TransferList[i];
        }
        else
        {
          j--;
          dst = (VX1000_UINT8*)gVX1000.Olda.TransferList[i]; /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because pointers are transferred compressed and decompression uses integer operations */
          size = (VX1000_UINT8)(sizeList & 0x000000FFUL); /* copy 0..255 bytes (this differs from DAQ which uses words) */
          sizeList >>= 8U;
#else /* !VX1000_OLDA_FORCE_V7 */
        /* TransferList entry contains merged size and address information for exactly one transfer */
        {
          dst = (VX1000_UINT8*)gVX1000.Olda.TransferList[i]; /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because pointers are transferred compressed and decompression uses integer operations */
          size = ((VX1000_UINT32)dst & (VX1000_OLDA_SIZE_MASK)) >> (VX1000_OLDA_SIZE_OFFSET);
          dst = (VX1000_UINT8*)( ( (VX1000_UINT32)dst & (~(VX1000_OLDA_SIZE_MASK)) ) | (VX1000_OLDA_SIZE_REPLACEMENT) );
#endif /* !VX1000_OLDA_FORCE_V7 */
#if defined(VX1000_OLDA_BENCHMARK)
          gVX1000_OLDA_TransferSize[eventNumber] += size; /* Size of all transfers */
#endif /* VX1000_OLDA_BENCHMARK */
#if defined(VX1000_MEMCPY)
          VX1000_MEMCPY(dst, src, size); /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because this callback function may be declared either with or without memory qualifiers by the user */
          src += size; /* PRQA S 0488 */ /* cannot avoid violating MISRA Rule 17.4 because memcopy based on computed pointers is a core feature of this application */
#else /* !VX1000_MEMCPY */
          while (size != 0)
          {
            *dst = *src;
            dst++; /* PRQA S 0489 */ /* cannot avoid violating MISRA Rule 17.4 because memcopy and thus pointer incrementation is a core feature of this application */
            src++; /* PRQA S 0489 */ /* cannot avoid violating MISRA Rule 17.4 because memcopy and thus pointer incrementation is a core feature of this application */
            size--;
          } /* while size */
#endif /* !VX1000_MEMCPY */
          n--;
        }
      } /* for */
    }
#if defined(VX1000_OLDA_BENCHMARK)
    gVX1000_OLDA_Duration[eventNumber] = (VX1000_CLOCK()) - t0; /* Timing measurement */
#endif /* VX1000_OLDA_BENCHMARK */
  }
  return;
}
#endif /* VX1000_STIM_BY_OLDA */

#if (0==(VX1000_STIM_EVENT_OFFSET))
#define VX1000_IS_STIM_EVENT(e)  ((e) < ((VX1000_STIM_EVENT_OFFSET) + (VX1000_STIM_EVENT_COUNT))) /* PRQA S 3453 */ /* willingsly violating MISRA Rule 19.7 because calling a function instead of executing a single machine instruction is far too inefficient for realtime applications */
#else /* VX1000_STIM_EVENT_OFFSET */
#define VX1000_IS_STIM_EVENT(e)  (((e) >= VX1000_STIM_EVENT_OFFSET) && \
                                  ((e) < ((VX1000_STIM_EVENT_OFFSET) + (VX1000_STIM_EVENT_COUNT)))) /* PRQA S 3453 */ /* willingsly violating MISRA Rule 19.7 because introducing a new function function only for this specific preprocessor case increases code complexity too much */
#endif /* VX1000_STIM_EVENT_OFFSET */


#if !defined(VX1000_RESET_STIM_TIMEOUT) || !defined(VX1000_CHECK_STIM_TIMEOUT)

static VX1000_UINT32 SetTimeoutUs(VX1000_UINT32 t)
{
  return (VX1000_CLOCK()) + (t * (VX1000_CLOCK_TICKS_PER_US));
}

static VX1000_UINT8 CheckTimeout(VX1000_UINT32 timeout)
{
  VX1000_UINT8 retVal = 0;  /* "no timeout, yet" */
  if (((timeout - (VX1000_CLOCK())) & 0x80000000UL) != 0) { retVal = 1; }
  return retVal;
}

/* todo MISRA: the next line violates(?) the "C macros shall only expand to a braced initialised, a constant, a   /
/  parenthesised expression, a type qualifier, a storage class specifier, or a do-while-zero construct."-rule!    /
/  Embracing it or wrapping into do-while-zero however did not make the qac message vanish. So check why we need  /
/  this user-redefinable macro at all or whether we can manually expand it in the code                           */
#define VX1000_RESET_STIM_TIMEOUT(x) gVX1000_timeout = SetTimeoutUs((VX1000_UINT32)(x));
#define VX1000_CHECK_STIM_TIMEOUT() CheckTimeout(gVX1000_timeout) /* Return true if timeout */

#endif /* !VX1000_RESET_STIM_TIMEOUT && !VX1000_CHECK_STIM_TIMEOUT */



/* Initialize the STIM data structure */
/* Called once in VX1000_INIT_ASYNC_START() and everytime STIM is turned off */
void vx1000_StimInit(void)
{
  VX1000_UINT32 i;

  gVX1000.Stim.Version = 0;
  gVX1000.Stim.Control = 0;
  gVX1000.Stim.EvtOffset = VX1000_STIM_EVENT_OFFSET;
  gVX1000.Stim.EvtNumber = VX1000_STIM_EVENT_COUNT;
  for (i = 0; i < VX1000_STIM_EVENT_COUNT; i++)
  {
    gVX1000.Stim.Event[i].Ctr = 0;
    gVX1000.Stim.Event[i].RqCtr = 0;
    gVX1000.Stim.Event[i].Enable = 0;
#if !defined(VX1000_STIM_BY_OLDA)
    gVX1000.Stim.Event[i].Copying = 0;
#endif /* !VX1000_STIM_BY_OLDA */
  }
  gVX1000.Stim.Enable = 0;
  gVX1000.Stim.MagicId = (VX1000_UINT32)(VX1000_STIM_MAGIC);
}


#if defined(VX1000_STIM_BENCHMARK)

/* Init the STIM benchmark data */
void vx1000_StimBenchmarkInit( void )
{
  VX1000_UINT32 i;

  for (i = 0; i < VX1000_STIM_EVENT_COUNT; i++)
  {
    gVX1000_STIM_Begin[i] = 0;
    gVX1000_STIM_Duration[i] = 0;
  }

#if defined(VX1000_STIM_HISTOGRAM)
  for (i = 0; i < 256; i++) { gVX1000_STIM_Histogram[i] = 0; }
  for (i = 0; i < 16; i++) { gVX1000_STIM_Histogram2[i] = 0; }
#endif /* VX1000_STIM_HISTOGRAM */
}


/* Start benchmark timer */
void vx1000_StimBenchmarkStimBegin( VX1000_UINT8 stim_event )
{
  gVX1000_STIM_Begin[stim_event - (VX1000_STIM_EVENT_OFFSET)] = VX1000_CLOCK(); /* Timing measurement */
}

/* Stop benchmark timer */
void vx1000_StimBenchmarkStimEnd( VX1000_UINT8 stim_event, VX1000_UINT8 timeout_flag )
{
  VX1000_UINT32 t0,t1,dt;

  t0 = gVX1000_STIM_Begin[stim_event - (VX1000_STIM_EVENT_OFFSET)];
  if (t0 != 0)
  {
    gVX1000_STIM_Begin[stim_event - (VX1000_STIM_EVENT_OFFSET)] = 0;
    t1 = VX1000_CLOCK();
    dt =  t1 - t0;
    gVX1000_STIM_Duration[stim_event - (VX1000_STIM_EVENT_OFFSET)] = dt; /* Last delay for each individual event */

    /* Build the histograms for event VX1000_STIM_HISTOGRAM */
#if defined(VX1000_STIM_HISTOGRAM)
    if (stim_event == (VX1000_STIM_HISTOGRAM))
    {
      gVX1000_STIM_Histogram2[0]++; /* Index 0 is the cycles with 0 timeouts counter ! */

      if (!timeout_flag)
      {
        /* Build the delay histogram */
        /* 20us resolution, 0..5100us:     */
        t0 = (VX1000_UINT32)(dt / ((VX1000_CLOCK_TICKS_PER_MS) / 50UL));
        if (t0 >= 256) { t0 = 255; }
        gVX1000_STIM_Histogram[t0]++;
      }
      else
      {
        /* Build the timeout burst count histogram */
        /* 0..15 */
        t0 = gVX1000.Stim.TimeoutCtr2;
        if (t0 <= 15)
        {
          gVX1000_STIM_Histogram2[t0]++;
          gVX1000_STIM_Histogram2[t0-1]--; /* t0 is always >0 when timeout_flag is set */
        }
      }
    }
#else /* !VX1000_STIM_HISTOGRAM */
    timeout_flag = timeout_flag; /* dummy access to prevent compiler warning */
#endif /* !VX1000_STIM_HISTOGRAM */
  }
}

/* Check benchmark timer */
static void vx1000_StimBenchmarkStimCheck( void )
{
#if defined(VX1000_STIM_HISTOGRAM)
  if (VX1000_STIM_ACTIVE(VX1000_STIM_HISTOGRAM) != 0)
  {
    if ((gVX1000.Stim.Event[(VX1000_STIM_HISTOGRAM) - (VX1000_STIM_EVENT_OFFSET)].Ctr)
    ==  (gVX1000.Stim.Event[(VX1000_STIM_HISTOGRAM) - (VX1000_STIM_EVENT_OFFSET)].RqCtr))
    {
      VX1000_STIM_BENCHMARK_STIM_END(VX1000_STIM_HISTOGRAM, 0)
    }
  }
#endif /* VX1000_STIM_HISTOGRAM */
}


#endif /* VX1000_STIM_BENCHMARK */

/* Keep-Alive-Handler (to be called cyclic by the application)*/
void vx1000_StimControl(void)
{
  if ((gVX1000.Stim.Control != 0) && (0==gVX1000.Stim.Enable))
  {
    /* Reset state of generic hooking -- the code is a little bit misplaced, isn't it ... */
#if defined(VX1000_HOOK_ENABLE)
    gVX1000_HookControl.active_index = VX1000_HOOK_COUNT;
    gVX1000_HookControl.active_id = 0xFFFFU;
#endif /* VX1000_HOOK_ENABLE */

    /* Clear benchmark data */
    VX1000_STIM_BENCHMARK_INIT()

    gVX1000.Stim.TimeoutCtr = 0;
    gVX1000.Stim.TimeoutCtr2 = 0;

#if defined(VX1000_STIM_TYPE_0)
    VX1000_SPECIAL_EVENT(VX1000_EVENT_STIM_ACK) /* Send an acknowledge STIM event when the ECU is ready for STIM  */ /* PRQA S 0303 */ /* cannot avoid violating MISRA Rule 11.3 because the user/compiler typically provides the HW address as an integer (it does not know the VX pointer types) */
#endif /* VX1000_STIM_TYPE_0 */

    gVX1000.Stim.Enable = 1;
  }
  else
  {
    /* Reinitialize STIM when VX1000 turns STIM off */
    if ((0==gVX1000.Stim.Control) && (gVX1000.Stim.Enable == 1)) { VX1000_STIM_INIT() }
  }

#if defined(VX1000_STIM_BENCHMARK)
  /* STIM Benchmark: Specific measurements for a single event from request to acknowledge */
  vx1000_StimBenchmarkStimCheck();
#endif /* VX1000_STIM_BENCHMARK */
}


/* Stim Active Check */
VX1000_UINT8 vx1000_StimActive( VX1000_UINT8 stim_event )
{
  VX1000_UINT8 retVal = 0;  /* "inactive" */
  if ((gVX1000.Stim.Control != 0) && (gVX1000.Stim.Enable  != 0) && (VX1000_IS_STIM_EVENT(stim_event))
  && (gVX1000.Stim.Event[stim_event - (VX1000_STIM_EVENT_OFFSET)].Enable != 0))
  {
    retVal = 1;  /* "active" */
  }
  return retVal;
}


/* Request a specific STIM data set assciated to event stim_event */
void vx1000_StimRequest( VX1000_UINT8 stim_event )
{
  VX1000_STIM_BENCHMARK_STIM_BEGIN(stim_event)

  gVX1000.Stim.Event[stim_event - (VX1000_STIM_EVENT_OFFSET)].RqCtr++;
  VX1000_STIM_REQUEST_EVENT(stim_event)
}


/* Wait until a specific STIM request is fullfilled */
/* Timeout specified in [us], the time relates to a previous call of VX1000_STIM_REQUEST() */
/* Return 1 in case of timeout */
VX1000_UINT8 vx1000_StimWait( VX1000_UINT8 stim_event, VX1000_UINT8 copy_enable, VX1000_UINT32 timeout_us )
{
  VX1000_UINT8 errorcode = 0U;
  static volatile VX1000_UINT32 gVX1000_timeout; /* can be per core and static */

  VX1000_RESET_STIM_TIMEOUT(timeout_us)
  while ( (0==errorcode) /* Busy wait with timeout until direct stimulation is done (or olda stimulation data arrived) */
  && (gVX1000.Stim.Event[stim_event - (VX1000_STIM_EVENT_OFFSET)].Ctr
  !=  gVX1000.Stim.Event[stim_event - (VX1000_STIM_EVENT_OFFSET)].RqCtr) )
  {
    if ((VX1000_CHECK_STIM_TIMEOUT()) != 0)
    {
#if !defined(VX1000_STIM_BY_OLDA)
      if (gVX1000.Stim.Event[stim_event - (VX1000_STIM_EVENT_OFFSET)].Copying != 0)
      {
        /* If timeout happened, but copying data has already started,   */
        /* give a second chance to complete within the next milisecond: */
        VX1000_RESET_STIM_TIMEOUT(1000U)
        while (0==errorcode)
        {
          if (gVX1000.Stim.Event[stim_event - (VX1000_STIM_EVENT_OFFSET)].Ctr
              ==  gVX1000.Stim.Event[stim_event - (VX1000_STIM_EVENT_OFFSET)].RqCtr)
          {
            errorcode = 1; /* timeout, but new data already valid */
          }
          else if (VX1000_CHECK_STIM_TIMEOUT())
          {
            VX1000_SPECIAL_EVENT(VX1000_EVENT_STIM_ERR(stim_event)) /* PRQA S 0303 */ /* cannot avoid violating MISRA Rule 11.3 because the user/compiler typically provides the HW address as an integer (it does not know the VX pointer types) */
            errorcode = 2; /* timeout, data incomplete */
          }
        }
      }
      else
#endif /* !VX1000_STIM_BY_OLDA */
      {
        if ((VX1000_STIM_ACTIVE(stim_event)) != 0) { VX1000_SPECIAL_EVENT(VX1000_EVENT_STIM_TIMEOUT(stim_event)) } /* PRQA S 0303 */ /* cannot avoid violating MISRA Rule 11.3 because the user/compiler typically provides the HW address as an integer (it does not know the VX pointer types) */
        errorcode = 2; /* timeout, no new data */
      }
    } /* Timeout */
  } /* while */

  if (errorcode == 2) /* Timeout (no new valid data) --> increment timeout counter and timeout burst counter */
  {
    gVX1000.Stim.TimeoutCtr++;
    gVX1000.Stim.TimeoutCtr2++;
  }
  else /* either no timeout at all or timeout, but still all data arrived successfully */
  {
    gVX1000.Stim.TimeoutCtr2 = 0; /* Clear the timeout burst counter */
#if defined(VX1000_STIM_BY_OLDA)
    /* Copy the STIM data to final memory locations */
    if (copy_enable != 0) { vx1000_StimTransfer(stim_event); }
#else /* !VX1000_STIM_BY_OLDA */
    copy_enable = copy_enable; /* dummy access to prevent compiler warning */
#endif /* !VX1000_STIM_BY_OLDA */
  }
  errorcode >>= 1;
  VX1000_STIM_BENCHMARK_STIM_END(stim_event, errorcode)
  return errorcode;
}


/* Event number daq_event is the bypass stimulation event, it indicates */
/* that the ECU is now waiting for completion of the stimulation */
/* Event number stim_event is the bypass trigger event, it should be used */
/* to trigger calculation of the bypass in the tool; as long */
/* as the bypass is not activated in the tool, this event is not executed. */
/* Time timeout specified in [us], the time starts now. */
/* Return 1, if the bypass is active and the bypassed code has to be disabled */
/* Return 2, in case of timeout */
VX1000_UINT8 vx1000_Bypass( VX1000_UINT8 daq_event, VX1000_UINT8 stim_event, VX1000_UINT32 timeout_us)
{
  VX1000_UINT8 retVal = 0;    /* 0 means "Bypass inactive, enable the bypassed code" */

  if ((VX1000_STIM_ACTIVE(stim_event)) != 0)
  {
    VX1000_EVENT(daq_event)
    VX1000_STIM_REQUEST(stim_event)
    if ((VX1000_STIM_WAIT(stim_event, timeout_us)) != 0)
    {
      retVal = 2;           /* "Bypass active, timeout" */
    }
    else
    {
      retVal = 1;           /* "Bypass active, ok, disable the bypassed code" */
    }
  }
  return retVal;
}

VX1000_UINT8 vx1000_BypassTrigger( VX1000_UINT8 daq_event, VX1000_UINT8 stim_event )
{
  VX1000_UINT8 retVal = 0;    /* 0 means "Bypass inactive" */

  if (VX1000_STIM_ACTIVE(stim_event) != 0)
  {
    VX1000_EVENT(daq_event)
    retVal = 1;              /* "Bypass active" */
  }
  return retVal;
}

/* Time timeout specified in [us], the time starts now. */
VX1000_UINT8 vx1000_BypassWait( VX1000_UINT8 stim_event, VX1000_UINT32 timeout_us )
{
  VX1000_UINT8 retVal = 0;    /* 0 means "Bypass inactive, enable the bypassed code" */

  if (VX1000_STIM_ACTIVE(stim_event) != 0)
  {
    VX1000_STIM_REQUEST(stim_event)
    if (VX1000_STIM_WAIT(stim_event, timeout_us) != 0)
    {
      retVal = 2;           /* "Bypass active, timeout. It may be too late to enable the bypassed code" */
    }
    else
    {
      retVal = 1;           /* "Bypass active, ok, disable the bypassed code" */
    }
  }
  return retVal;
}


/* Event number stim_trigger_event is the stimulation daq event to request data from CANape */
/* Event number stim_event is the stimulation event */
/* Return 1, if stimulation is active  */
/* Return 2, if stimulation is active and there is a timeout */
/* cycle_delay specifies the number of cycles between the triggering and the associated stimulation, the */
/* first (cycle_delay) cycles are not stimulated ! */
/* timeout may be 0, if cycle_delay is >0 */
#if defined(VX1000_STIM_BY_OLDA)
/* Reuse the events copying flag as cycle delay counter: */
#define CycleDelayCtr(event) gVX1000.Stim.Event[(event) - (VX1000_STIM_EVENT_OFFSET)].Copying /* PRQA S 3453 */ /* willingly violating MISRA Rule 19.7 because define is used as an lvalue and thus cannot be replaced by a function call */

VX1000_UINT8 vx1000_Stimulate( VX1000_UINT8 stim_trigger_event, VX1000_UINT8 stim_event,
VX1000_UINT8 cycle_delay, VX1000_UINT32 timeout_us)
{
  VX1000_UINT8 retVal = 0; /* means "Stim not active" */

  if (0==cycle_delay)
  {
    /* If cycle_delay is 0, stimulation has normal bypass behaviour */
    retVal = vx1000_Bypass(stim_trigger_event, stim_event, timeout_us);
  }
  else
  {
    if (VX1000_STIM_ACTIVE(stim_event) != 0)
    {
      if (0==(CycleDelayCtr(stim_event)))
      {
        /* Delay period has expired */
        /* Wait for stimulation data available in the OLDA buffer and stimulate */
        if ((VX1000_STIM_WAIT(stim_event, timeout_us)) != 0)
        {
          /* We cannot distinguish the cases "EOF_stimfile reached, no more data" and   */
          /* "timeout requirement just too hard, stim data still waiting in the queue". */
          /* In either case we do NOT try to prefetch further data and we always signal */
          /* "Stim (still) active, Timeout" to the caller: */
          retVal = 2;
        }
        else
        {
          VX1000_STIM_REQUEST(stim_event)     /* Request stimulation data */
          VX1000_EVENT(stim_trigger_event)    /* Trigger a stimulation */
          retVal = 1;                         /* Stim active, Ok */
        }
      }
      else
      {
        CycleDelayCtr(stim_event)--;
        VX1000_EVENT(stim_trigger_event)       /* Trigger a stimulation */
        if (0==(CycleDelayCtr(stim_event)))
        {
          VX1000_STIM_REQUEST(stim_event)     /* Request stimulation data */
        }
        /* Stim not active, startup delay still in progress */
      }
    } /* if Stim active */
    else
    {
      if (cycle_delay > 100) { cycle_delay = 100; } /* Silently ensures to not exceed the VX's max queue size */
      CycleDelayCtr(stim_event) = cycle_delay;
    } /* if Stim not active */
  } /* if cycle_delay > 0 */

  return retVal;
}
#endif /* VX1000_STIM_BY_OLDA */

#endif /* VX1000_STIM */


/*------------------------------------------------------------------------------- */
/* Generic Hooking */

#if defined(VX1000_HOOK_ENABLE)

void vx1000_HookInit( void )
{
  VX1000_UINT8 i;

  gVX1000_HookControl.magic = VX1000_HOOK_MAGIC;
  gVX1000_HookControl.count = 0;
  gVX1000_HookControl.active_index = VX1000_HOOK_COUNT;
  gVX1000_HookControl.active_id = 0xFFFFU;
  for (i = 0; i < VX1000_HOOK_COUNT; i++)
  {
    /* Define standard behaviour copy=on, execute original code=off, daq=off */
    gVX1000_HookControl.table[i].id = 0xFFFFU;
#if defined(VX1000_STIM)
    gVX1000_HookControl.table[i].stim_event = 0xFFU;
#endif
    gVX1000_HookControl.table[i].trigger_event = 0xFFU;
    gVX1000_HookControl.table[i].daq_event = 0xFFU;
    gVX1000_HookControl.table[i].control = VX1000_HOOK_COPY_ENABLE;
  }
}


/* Generic event */
void vx1000_GenericEvent( VX1000_UINT16 hook_id )
{
  VX1000_UINT8 i;

  /* Lookup the hook id */
  if ((gVX1000_HookControl.magic == (VX1000_HOOK_MAGIC)) && (gVX1000_HookControl.count != 0))
  {
    for (i = 0; (i < gVX1000_HookControl.count) && (gVX1000_HookControl.table[i].id != hook_id); i++)
    {
      /* just loop until we found a valid index i pointing to hook_id or there are no indices left */
    }
    if (i < gVX1000_HookControl.count)
    {
      VX1000_EVENT(gVX1000_HookControl.table[i].trigger_event)
    }
  }
}


/* Trigger a generic bypass
/   return 0: Run the original code
/             (inactive bypass or active bypass and original code enabled)
/   return 1: Don't run the original code
/             (bypass active and original code disabled) */
VX1000_UINT8 vx1000_EnterHook( VX1000_UINT16 hook_id )
{
  VX1000_UINT8 i, retVal = 0; /* means "enable the bypassed code" */

  /* Initialization check */
  if ((gVX1000_HookControl.magic == (VX1000_HOOK_MAGIC)) && (gVX1000_HookControl.count != 0))
  {
    for (i = 0; (i < gVX1000_HookControl.count) && (gVX1000_HookControl.table[i].id != hook_id); i++)
    {
      /* just loop until we found a valid index i pointing to hook_id or there are no indices left */
    }
    if (i < gVX1000_HookControl.count)
    {
#if defined(VX1000_STIM)
      /* i is a valid index -> found a bypass entry */
      if (vx1000_StimActive(gVX1000_HookControl.table[i].stim_event) != 0)
      {
        /* This bypass is enabled by the tool, but check for nested bypasses first */
        if (gVX1000_HookControl.active_index >= gVX1000_HookControl.count)
        {
          /* Trigger this bypass */
          gVX1000_HookControl.active_index = i;
          gVX1000_HookControl.active_id = hook_id;
          VX1000_EVENT(gVX1000_HookControl.table[i].trigger_event)
          vx1000_StimRequest(gVX1000_HookControl.table[i].stim_event);
          if (0==(gVX1000_HookControl.table[i].control & (VX1000_HOOK_CODE_ALWAYS)))
          {
            retVal = 1; /* Bypass active normally --> don't execute the bypassed code */
          }
          /* else: Bypass active but original code enabled --> enable the bypassed code */
        }
        /* else: Nesting detected, don't execute this bypass --> enable the bypassed code */
      }
      else
#endif /* VX1000_STIM */
      {
        gVX1000_HookControl.active_index = i;
        gVX1000_HookControl.active_id = hook_id;
        if ((gVX1000_HookControl.table[i].control & (VX1000_HOOK_TRIGGER_EVENT_ENABLE)) != 0)
        {
          VX1000_EVENT(gVX1000_HookControl.table[i].trigger_event)
        }
      }
      /* Bypass inactive --> enable the bypassed code */
    }
    /* else: Id not found, Bypass inactive --> enable the bypassed code */
  }
  /* else: Generic bypassing not initialized yet --> enable the bypassed code */
  return retVal;
}


/* Stimulate for a generic bypass
/   return 3: stimulation not done, timeout, execute original code
/   return 2: stimulation not done, timeout
/   return 1: stimulation done, no timeout, ok
/   return 0: bypass inactive */
VX1000_UINT8 vx1000_LeaveHook( VX1000_UINT16 hook_id, VX1000_UINT32 timeout)
{
  VX1000_UINT16 idx, ident;
  VX1000_UINT8 retVal = 0; /* this means "bypass inactive" */

  if ((gVX1000_HookControl.magic == (VX1000_HOOK_MAGIC)) && (gVX1000_HookControl.count != 0))
  {
    /* Generic bypassing is initialised */
    idx = gVX1000_HookControl.active_index;
    ident = gVX1000_HookControl.active_id;
    if ((idx < gVX1000_HookControl.count) && (ident == hook_id))
    {
      /* The specified generic bypass is being executed right now */
      gVX1000_HookControl.active_index = VX1000_HOOK_COUNT;
      gVX1000_HookControl.active_id = 0xFFFFU;
#if defined(VX1000_STIM)
      if (vx1000_StimActive(gVX1000_HookControl.table[idx].stim_event) != 0)
      {
        if ((gVX1000_HookControl.table[idx].control & (VX1000_HOOK_DAQ_EVENT_ENABLE)) != 0)
        {
          VX1000_EVENT(gVX1000_HookControl.table[idx].daq_event)    /* Trigger an additional DAQ event */
        }
        /* Wait */
        if (vx1000_StimWait(gVX1000_HookControl.table[idx].stim_event
              ,(VX1000_UINT8)(gVX1000_HookControl.table[idx].control & (VX1000_UINT8)(VX1000_HOOK_COPY_ENABLE)), timeout) != 0)
        {
          if ((gVX1000_HookControl.table[idx].control & (VX1000_HOOK_CODE_TIMEOUT)) != 0)
          {
            retVal = 3;  /* Timeout, execute original code */
          }
          else
          {
            retVal = 2; /* Timeout, no action */
          }
        }
        else
        {
          retVal = 1;   /* Bypass active, done, ok */
        }
      }
      else
#endif /* VX1000_STIM */
      {
        if ((gVX1000_HookControl.table[idx].control & (VX1000_HOOK_DAQ_EVENT_ENABLE)) != 0)
        {
          VX1000_EVENT(gVX1000_HookControl.table[idx].daq_event)    /* Trigger an additional DAQ event */
        }
      } /* do not remove this pair of braces - it's required for MISRA */
      /* Bypass not startet by tool, Bypass inactive */
    }
    /* else: Id not found, Bypass inactive */
  }
  /* else: Generic bypassing not active at all */
  return retVal;
}

#endif /* VX1000_HOOK_ENABLE */


/*------------------------------------------------------------------------------- */
/* Calibration (new version w/ dynamic setting by VX based on resource management */

#if defined(VX1000_OVERLAY) && !defined(VX1000_COMPILED_FOR_SLAVECORES)

void vx1000_EmemHdrInit(void)
{
  VX1000_EMEM_HDR_T *hdr = (VX1000_EMEM_HDR_T*)gVX1000.Ovl.persistentECUEmemHeaderPtr;
  hdr->version = VX1000_EMEM_HDR_VERSION;
  hdr->reserved = 0; /* initialize the reserved field for crc-reasons */
  hdr->magicId = VX1000_EMEM_HDR_MAGIC;
}

void vx1000_OverlayInit(void)
{
  gVX1000.Ovl.presenceCounter = 0;
  gVX1000.Ovl.ovlConfigValue  = 0;
  gVX1000.Ovl.ovlConfigMask   = 0;
  gVX1000.Ovl.calFeaturesEnable = 0
#if defined(VX1000_OVLENBL_KEEP_AWAKE)
                                | (VX1000_OVLFEAT_KEEP_AWAKE)
#endif /* VX1000_OVLENBL_KEEP_AWAKE */
#if defined(VX1000_OVLENBL_SYNC_PAGESWITCH)
                                | (VX1000_OVLFEAT_SYNC_PAGESWITCH)
#endif /* VX1000_OVLENBL_SYNC_PAGESWITCH */
#if defined(VX1000_OVLENBL_PERSISTENT_EMEM)
                                | (VX1000_OVLFEAT_PERSISTENT_EMEM)
#endif /* VX1000_OVLENBL_PERSISTENT_EMEM */
#if defined(VX1000_OVLENBL_RST_ON_CALWAKEUP)
                                | (VX1000_OVLFEAT_RST_ON_CALWAKEUP)
#endif /* VX1000_OVLENBL_RST_ON_CALWAKEUP */
#if defined(VX1000_OVLENBL_USE_VX_EPK_TRANS)
                                | (VX1000_OVLFEAT_USE_VX_EPK_TRANS)
#endif /* VX1000_OVLENBL_USE_VX_EPK_TRANS */
#if defined(VX1000_OVLENBL_VALIDATE_PAGESW)
                                | (VX1000_OVLFEAT_VALIDATE_PAGESW)
#endif /* VX1000_OVLENBL_VALIDATE_PAGESW */
#if defined(VX1000_OVLENBL_CORE_SYNC_PAGESW)
                                | (VX1000_OVLFEAT_CORE_SYNC_PAGESW)
#endif /* VX1000_OVLENBL_CORE_SYNC_PAGESW */
                                ;

  gVX1000.Ovl.persistentECUEmemHeaderPtr = VX1000_EMEM_HDR_PTR;
  gVX1000.Ovl.ovlBusMasterMask = VX1000_OVL_CAL_BUS_MASTER;

  gVX1000.Ovl.ecuLastPresenceCounter = 0;
  gVX1000.Ovl.ovlEPKLength = (VX1000_UINT16)(VX1000_OVL_EPK_LENGTH);
  gVX1000.Ovl.ovlReferencePageDataEPKAddress = (VX1000_UINT32)(VX1000_OVL_EPK_REFPAGE_ADDR);
  gVX1000.Ovl.ovlWorkingPageDataEPKAddress = 0xFFFFFFFFUL;
  gVX1000.Ovl.syncCalSwitchDataPtr = ((VX1000_UINT32)&gVX1000.Ovl.syncCalData);

  gVX1000.Ovl.version = VX1000_OVL_VERSION;

  if (gVX1000.Ovl.persistentECUEmemHeaderPtr != 0)
  {
    vx1000_EmemHdrInit();
  }

  gVX1000.Ovl.magicId = VX1000_OVL_MAGIC;
}

#endif /* VX1000_OVERLAY & !VX1000_COMPILED_FOR_SLAVECORES */


#if defined(VX1000_RES_MGMT) && !defined(VX1000_COMPILED_FOR_SLAVECORES)

void vx1000_ResMgmtInit(void)
{
  gVX1000.ResMgmt.resMgmtEnable = 0UL; /* PRQA S 3198 */ /* this global volatile variable is accessed by debug tool in the background */

  gVX1000.ResMgmt.version = VX1000_RES_MGMT_VERSION;
  gVX1000.ResMgmt.ovlConfigItemStart  = (VX1000_UINT8)(VX1000_RES_MGMT_CFG_ITEM_START);
  gVX1000.ResMgmt.ovlConfigItemLength = (VX1000_UINT8)(VX1000_RES_MGMT_CFG_ITEM_LEN);

  gVX1000.ResMgmt.ovlRamStart   = (VX1000_UINT32)(VX1000_RES_MGMT_OVL_RAM_START);
  gVX1000.ResMgmt.ovlRamSize    = (VX1000_UINT32)(VX1000_RES_MGMT_OVL_RAM_SIZE);

  gVX1000.ResMgmt.resMgmtEnable = (VX1000_RES_MGMT_ENBLVAL_CFG_ITEM)
                                | (VX1000_RES_MGMT_ENBLVAL_OVL_RAM);

  gVX1000.ResMgmt.magicId = VX1000_RES_MGMT_MAGIC;
}


/* Disable the ... TODO
/   return 0: ... 
/             
/   return 1: ...
/             */
VX1000_UINT8 vx1000_DisableAccess( void )
{
  VX1000_UINT8 retVal = 0;
  if (VX1000_DETECTED())
  {
    retVal = 1;
  }
  else
  {
    VX1000_INVALIDATE_EMEM()
    gVX1000.ToolDetectState |= VX1000_TDS_VX_ACCESS_DISABLED;
  }
  return retVal;
}

VX1000_UINT8 vx1000_EmemInit( void )
{
  unsigned int res;
  unsigned char err = 0;

  if (!(VX1000_MCREG_CBS_OSTATE & 0x1)) {
    VX1000_MCREG_CBS_OEC = 0xA1;
    VX1000_MCREG_CBS_OEC = 0x5E;
    VX1000_MCREG_CBS_OEC = 0xA1;
    VX1000_MCREG_CBS_OEC = 0x5E;
  }

  if ((VX1000_MCREG_CBS_OSTATE & 0x40)) {
    //VX1000_MCREG_CBS_OSTATE
    VX1000_MCREG_CBS_OCNTRL = 0x00000400;
  }

  if ((VX1000_MCREG_EMEM_CLC & 0x2)) {
    VX1000_MCREG_EMEM_CLC = 0x0;
  }

  VX1000_EMEM_UNLOCK(res);

  // Define all tiles as "calibration memory"
  VX1000_MCREG_EMEM_TILECONFIG = 0x00000000;
  // Set all tiles to non backbone bus mode
  VX1000_MCREG_EMEM_TILECT = 0x0;
  VX1000_MCREG_EMEM_TILECC = 0x0;

  return err;

}

#endif /* VX1000_RES_MGMT & !VX1000_COMPILED_FOR_SLAVECORES */


#if defined(VX1000_OVERLAY) && defined(VX1000_MAILBOX) && !defined(VX1000_COMPILED_FOR_SLAVECORES) /* mailbox no slavecore support and slavecore no MMU */
#if defined(VX1000_OVLENBL_KEEP_AWAKE)
/* Check whether ... TODO
/   return 0: ... 
/             
/   return 1: ...
/             */
VX1000_UINT8 vx1000_IsCalWakeupActive(void)
{
  VX1000_UINT8 retVal = 0;
  if (gVX1000.Ovl.ecuLastPresenceCounter != gVX1000.Ovl.presenceCounter)
  {
    retVal = 1;
  }
  gVX1000.Ovl.ecuLastPresenceCounter = gVX1000.Ovl.presenceCounter;
  return retVal;
}
#endif /* VX1000_OVLENBL_KEEP_AWAKE */

#if defined(VX1000_OVLENBL_PERSISTENT_EMEM)
void vx1000_InvalidateEmem(void)
{
  if (gVX1000.MagicId == (VX1000_UINT32)(VX1000_STRUCT_MAGIC))
  {
    if (gVX1000.OvlPtr != 0UL)
    {
      if (gVX1000.Ovl.persistentECUEmemHeaderPtr != 0UL)
      {
        VX1000_EMEM_HDR_T *hdr = (VX1000_EMEM_HDR_T*)gVX1000.Ovl.persistentECUEmemHeaderPtr;
        hdr->ememInitEnd = 0UL;
        hdr->ememInitEndInvert = 0UL;
        hdr->ememInitStart= 0UL;
        hdr->ememInitStartInvert = 0UL;
      }
    }
  }
}
#endif /* VX1000_OVLENBL_PERSISTENT_EMEM */

#if defined(VX1000_MAILBOX_OVERLAY_CONTROL)
#define VX1000_CALPAGE_RAM 1
#define VX1000_CALPAGE_FLASH 0
static VX1000_UINT8 gVX1000_XCP_CalPage; /* RAM */
static VX1000_UINT8 gVX1000_ECU_CalPage; /* RAM */
#endif /* VX1000_MAILBOX_OVERLAY_CONTROL */


#if defined(VX1000_INIT_CAL_PAGE_INTERNAL)
static void vx1000_InitCalPage( void )
{
  /* Initialize CALRAM */
#if defined(VX1000_OVERLAY_TLB)
  memcopy((void*)VX1000_CALRAM_ADDR,(void*)VX1000_OVERLAY_ADDR,VX1000_OVERLAY_SIZE); /* FLASH->RAM */ /* PRQA S 0306 */ /* willingly violating MISRA Rule 11.3 here to be able to replace pointer arithmetic by integer arithmetic elsewhere */
  /* Map RAM over FLASH, cache inhibit, RWE */
  (void)vx1000_MMUWriteTLBEntry(VX1000_OVERLAY_TLB, VX1000_OVERLAY_ADDR, VX1000_CALRAM_ADDR,
                                VX1000_OVERLAY_SIZE, 1, 1, 0x3F); /* RAM */
#elif defined(VX1000_OVERLAY_DESCR_IDX) || defined(VX1000_MPC56xCRAM_BASE_ADDR)
  memcopy((void*)VX1000_CALRAM_ADDR,(void*)((VX1000_OVERLAY_ADDR)^(VX1000_CALROM_ADDR_XOR)),VX1000_OVERLAY_SIZE); /* FLASH->RAM */ /* PRQA S 0306 */ /* willingly violating MISRA Rule 11.3 here to be able to replace pointer arithmetic by integer arithmetic elsewhere */
  /* Map RAM over FLASH, cache inhibit, RWE */
  (void)vx1000_MapCalRam(VX1000_OVERLAY_SIZE, VX1000_OVERLAY_ADDR, VX1000_CALRAM_ADDR);
#endif /* VX1000_OVERLAY_DESCR_IDX | VX1000_MPC56xCRAM_BASE_ADDR*/

#if defined(VX1000_MAILBOX_OVERLAY_CONTROL)
  gVX1000_XCP_CalPage = VX1000_CALPAGE_RAM; /* RAM */
  gVX1000_ECU_CalPage = VX1000_CALPAGE_RAM; /* RAM */
#endif /* VX1000_MAILBOX_OVERLAY_CONTROL */
}
#endif /* VX1000_INIT_CAL_PAGE_INTERNAL */

#if defined(VX1000_MAILBOX_OVERLAY_CONTROL)

#if defined(VX1000_GET_CAL_PAGE_INTERNAL)
static VX1000_UINT8 vx1000_GetCalPage( VX1000_UINT8 segment, VX1000_UINT8 mode )
{
  VX1000_UINT8 retVal = gVX1000_ECU_CalPage;

  if ((mode & ((VX1000_CAL_ECU) | (VX1000_CAL_XCP))) == (VX1000_CAL_ECU))
  {
    retVal = gVX1000_ECU_CalPage;
  }
  else if ((mode & ((VX1000_CAL_ECU) | (VX1000_CAL_XCP))) == (VX1000_CAL_XCP))
  {
    retVal = gVX1000_XCP_CalPage;
  }
  else /* just here for MISRA */
  {
    segment++; /* just to avoid compiler warnings */ /* PRQA S 3199 */ /* just a dummy access anyway */
  }
  return retVal;
}
#endif /* VX1000_GET_CAL_PAGE_INTERNAL */


#if defined(VX1000_SET_CAL_PAGE_INTERNAL)
#if defined(VX1000_OVLENBL_SYNC_PAGESWITCH)
void vx1000_OverlaySetConfigDone( VX1000_UINT8 cfgResult, VX1000_UINT8 page, VX1000_UINT8 onStartup )
{
  VX1000_UINT32 txLen;
  VX1000_CHAR txBuf[8];

  if (0 == cfgResult)
  {
    txLen = 1;
    txBuf[0] = (VX1000_CHAR)-1;
    gVX1000_ECU_CalPage = page;
    if (page != 0)
    {
      gVX1000.ToolDetectState |= VX1000_TDS_WORKING_PAGE;
    }
    else
    {
      gVX1000.ToolDetectState &= ~(VX1000_TDS_WORKING_PAGE);
    }
  }
  else
  {
    txLen = 2;
    txBuf[0] = (VX1000_CHAR)-2;
    txBuf[1] = cfgResult;
  }
  if (onStartup == 0)
  {
    (void)vx1000_MailboxWrite(txLen, txBuf);
  }
}


/*
 * Target specific functions for enabling/disabling overlays
 * \Return 0: Nothing done                                                                     
 *         1: Page switch done
 *         2: Value not written correctly
 *         3: No single-master page-switch possible
 *         4: Generic error
 *        
 */
#if defined(VX1000_OVL_SET_CONFIG_INTERNAL)
VX1000_UINT8 vx1000_OverlaySetConfigTC2xx( VX1000_UINT32 value,         /* Overlay windows to be activated/deactivated */
                                             VX1000_UINT32 mask,        /* Resource Mask */
                                             VX1000_UINT8 page,         /* Overlay Page */
                                             VX1000_UINT32 master,      /* Masters to be activated */
                                             VX1000_UINT32 calMaster )  /* Masters resource Mask */
{



  /* Note: Implementation valid only for TC27x! */
  VX1000_UINT8 retVal = 0; /* assume OK */
  volatile VX1000_UINT32 checkValue;

  /* Check, if master is valid */
  if ( (master & calMaster) != master )
  {
    retVal = 3;
  }

  /* Check, if all overlay windows are VX resources. If not, abort. */
  else if (mask != 0xFFFFFFFF) {
    retVal = 4;
  }

  /* If overlay page == Flash, deactivate all overlays */
  else if (page == 0)
  {
    VX1000_MCREG_CBS_OCNTRL = 0x000000C0;
    VX1000_MCREG_SCU_OVCCON = ( 0x00060000 | master ); /* DCINVAL|OVSTP|master */
    VX1000_MCREG_CBS_OCNTRL = 0x00000040;
  }

  /* Activate given overlay windows */
  else
  {
    if (master & 0x1) {
      VX1000_MCREG_OVC0_OSEL = value;
    }
    if (master & 0x2) {
      VX1000_MCREG_OVC1_OSEL = value;
    }
    if (master & 0x4) {
      VX1000_MCREG_OVC2_OSEL = value;
    }
    VX1000_MCREG_CBS_OCNTRL = 0x000000C0;
    VX1000_MCREG_SCU_OVCCON = ( 0x00050000 | master ); /* DCINVAL|OVSTRT|master */
    VX1000_MCREG_CBS_OCNTRL = 0x00000040;
  }
  return retVal;
}
#endif /* VX1000_OVL_SET_CONFIG_INTERNAL */

VX1000_UINT8 vx1000_overlayIsPageSwitchRequested(VX1000_UINT32 master)
{
  unsigned int i;
  if (gVX1000.Ovl.syncCalData.pageSwitchRequested)
  {
    for (i = 0; i < 32; i++)
    {
      if ((master & (1UL<<i)) != 0)
      {
        if (gVX1000.Ovl.syncCalData.coreDone[i] == 0)
        {
          return 1;
        }
      }
    }
  }
  return 0;
}


VX1000_UINT8 vx1000_OverlayChkPageSwitchCore( VX1000_UINT32 master )
{
  VX1000_UINT32 i;
  VX1000_UINT8 someNotReady = 0;
  VX1000_UINT8 result = 0;
  if (gVX1000.Ovl.syncCalData.pageSwitchRequested)
  {
    for (i = 0; i < 32; i++)
    {
      if ((master & (1UL<<i)) != 0)
      {
        if (gVX1000.Ovl.syncCalData.coreDone[i] == 0)
        {
          someNotReady = 1;
          result = VX1000_OVL_SET_CONFIG(gVX1000.Ovl.syncCalData.overlayValue, gVX1000.Ovl.syncCalData.overlayMask, gVX1000.Ovl.syncCalData.targetPage, master,  gVX1000.Ovl.ovlBusMasterMask);   
          if (result == 0)
          {
            /* 0 means no error. returning 1 to signal done*/
            gVX1000.Ovl.syncCalData.coreDone[i] = 1;
          }
          else if (result == 1)
          {
            /* 1 is not a valid return value. Use Generic error instead */
            gVX1000.Ovl.syncCalData.coreDone[i] = 2;
          }
          else
          {
            gVX1000.Ovl.syncCalData.coreDone[i] = result;
          }
        }
      }
    }
  }
  return someNotReady;
}

VX1000_UINT8 vx1000_OverlayChkPageSwitchDone( void )
{
  VX1000_UINT8 i;
  VX1000_UINT8 retVal = 0;
  VX1000_UINT8 someNotReady = 0;
  VX1000_UINT8 someHasErrors = 0;
  VX1000_UINT8 firstError = 0;
  if (gVX1000.Ovl.syncCalData.pageSwitchRequested)
  {
    for (i = 0; i < 32; i++)
    {
      if (gVX1000.Ovl.syncCalData.busMasterRequested & (1UL<<i))
      {
        if (gVX1000.Ovl.syncCalData.coreDone[i] == 0)
        {
          someNotReady = 1;
        }
        else if (gVX1000.Ovl.syncCalData.coreDone[i] == 1)
        {
          /* Switch was executed correctly */
        }
        else
        {
          firstError = gVX1000.Ovl.syncCalData.coreDone[i];
          someHasErrors = 1;
        }
        
      }
    }
    if (someNotReady == 0)
    {
      retVal = 1;
      gVX1000.Ovl.syncCalData.pageSwitchRequested = 0;
      VX1000_OVL_SET_CONFIG_DONE_VOID(firstError, gVX1000.Ovl.syncCalData.targetPage)
    }
    if (someHasErrors)
    {
      retVal = 2;
    }
  }
  return retVal;
}

#endif /* VX1000_OVLENBL_SYNC_PAGESWITCH */

static VX1000_UINT8 vx1000_SetCalPage( VX1000_UINT8 segment, VX1000_UINT8 page, VX1000_UINT8 mode, VX1000_UINT8 onStartup )
{
  VX1000_UINT8 retVal = 0;
#if defined(VX1000_OVLENBL_CORE_SYNC_PAGESW)
  VX1000_UINT8 i = 0;
#else /* !VX1000_OVLENBL_CORE_SYNC_PAGESW */
  onStartup = onStartup; /* just prevent "unused parameter" compiler warnings. Note that other compilers in turn may warn "code has no effect" */
#endif /* !VX1000_OVLENBL_CORE_SYNC_PAGESW */
  if (segment != 0)
  {
    retVal = VX1000_CRC_OUT_OF_RANGE; /* Only one segment supported */
  }
  else if (page > 1)
  {
    retVal = VX1000_CRC_PAGE_NOT_VALID;
  }
#if defined(VX1000_OVERLAY_VX_CONFIGURABLE)
  else if ((mode & (VX1000_CAL_ECU)) != 0)
  {
    /* Hint: switching the VX1000_CAL_XCP is not relevant in this mode (it is handled by the VX). */

#if defined(VX1000_OVLENBL_VALIDATE_PAGESW)
    if (page != 0)
    {
#if defined(VX1000_OVLENBL_USE_VX_EPK_TRANS)
        retVal = (VX1000_UINT8)(VX1000_SYNCAL_VALIDATE_WP_CB(gVX1000.Ovl.ovlWorkingPageDataEPKAddress));
#else /* !VX1000_OVLENBL_USE_VX_EPK_TRANS */
      retVal = (VX1000_UINT8)(VX1000_SYNCAL_USRVALIDATE_WP_CB(gVX1000.Ovl.ovlConfigValue, gVX1000.Ovl.ovlConfigMask, gVX1000.Ovl.ovlBusMasterMask));
#endif /* !VX1000_OVLENBL_USE_VX_EPK_TRANS */
    }
    if (retVal != 0)
    {
        return retVal;
      ; /* do nothing anymore */
    }
    else 
#endif /* VX1000_OVLENBL_VALIDATE_PAGESW */

#if defined(VX1000_OVLENBL_CORE_SYNC_PAGESW)
    if (0==onStartup)
    {
      gVX1000.Ovl.syncCalData.pageSwitchRequested = 0;
      gVX1000.Ovl.syncCalData.overlayValue = gVX1000.Ovl.ovlConfigValue;
      gVX1000.Ovl.syncCalData.overlayMask = gVX1000.Ovl.ovlConfigMask;
      gVX1000.Ovl.syncCalData.targetPage = page;
      gVX1000.Ovl.syncCalData.busMasterRequested = gVX1000.Ovl.ovlBusMasterMask;
      for (i = 0; i < 32; i++)
      {
        gVX1000.Ovl.syncCalData.coreDone[i] = 0;
      }
      gVX1000.Ovl.syncCalData.pageSwitchRequested = 1;
      /* The actual switch is done asynchronously */
      return VX1000_CRC_CMD_BUSY;
    }
    else
    {
      retVal = VX1000_OVL_SET_CONFIG(gVX1000.Ovl.ovlConfigValue, gVX1000.Ovl.ovlConfigMask, 1, gVX1000.Ovl.ovlBusMasterMask,  gVX1000.Ovl.ovlBusMasterMask);
      VX1000_OVL_SET_CONFIG_DONE_STUP(retVal, 1);
    }
#else
    {
#if defined(VX1000_OVLENBL_SYNC_PAGESWITCH)
      retVal = VX1000_OVERLAY_SET_CONFIG(gVX1000.Ovl.ovlConfigValue, gVX1000.Ovl.ovlConfigMask, 1, gVX1000.Ovl.ovlBusMasterMask,  gVX1000.Ovl.ovlBusMasterMask);
      VX1000_OVL_SET_CONFIG_DONE_STUP(retVal, 0);
#else /* !VX1000_OVLENBL_SYNC_PAGESWITCH */
      retVal = 1;
#endif /* !VX1000_OVLENBL_SYNC_PAGESWITCH */
    }
#endif /* VX1000_OVLENBL_CORE_SYNC_PAGESW */
  }
#endif /* VX1000_OVERLAY_VX_CONFIGURABLE */
#if defined(VX1000_OVERLAY_TLB) || defined(VX1000_OVERLAY_DESCR_IDX)
  else if ((mode & ((VX1000_CAL_ECU) | (VX1000_CAL_XCP))) == ((VX1000_CAL_ECU) | (VX1000_CAL_XCP)))
  {
    if (page != gVX1000_ECU_CalPage)
    {
      if (page == VX1000_CALPAGE_RAM)
      { /* RAM */
        /* Map RAM over FLASH, cache inhibit, RWE */
#if defined(VX1000_OVERLAY_TLB)
        (void)vx1000_MMUWriteTLBEntry(VX1000_OVERLAY_TLB, VX1000_OVERLAY_ADDR, VX1000_CALRAM_ADDR,
        VX1000_OVERLAY_SIZE, 1, 1, 0x3F); /* RAM */
#elif defined(VX1000_OVERLAY_DESCR_IDX)
        (void)vx1000_MapCalRam(VX1000_OVERLAY_SIZE, VX1000_OVERLAY_ADDR, VX1000_CALRAM_ADDR);
#endif /* VX1000_OVERLAY_DESCR_IDX */
      }
      else
      {
#if defined(VX1000_OVERLAY_TLB)
        /* Map FLASH, cache inhibit, RWE */
        (void)vx1000_MMUWriteTLBEntry(VX1000_OVERLAY_TLB, VX1000_OVERLAY_ADDR, VX1000_OVERLAY_ADDR,
        VX1000_OVERLAY_SIZE, 1, 1, 0x3F); /* FLASH */
#elif defined(VX1000_OVERLAY_DESCR_IDX)
        (void)vx1000_MapCalRam(0, 0, 0);
#endif /* VX1000_OVERLAY_DESCR_IDX */
      }
      gVX1000_XCP_CalPage = page;
      gVX1000_ECU_CalPage = page;
    }
  }
  else if ((mode & (VX1000_CAL_ECU)) != 0)
  {
    if (page != gVX1000_ECU_CalPage)
    {
      if (page == VX1000_CALPAGE_RAM)
      { /* RAM */
#if defined(VX1000_OVERLAY_TLB)
        /* Map RAM over FLASH, cache inhibit, RWE */
        (void)vx1000_MMUWriteTLBEntry(VX1000_OVERLAY_TLB, VX1000_OVERLAY_ADDR, VX1000_CALRAM_ADDR,
        VX1000_OVERLAY_SIZE, 1, 1, 0x3F); /* RAM */
#elif defined(VX1000_OVERLAY_DESCR_IDX)
        (void)vx1000_MapCalRam(VX1000_OVERLAY_SIZE, VX1000_OVERLAY_ADDR, VX1000_CALRAM_ADDR);
#endif /* VX1000_OVERLAY_DESCR_IDX */
      }
      else
      {
#if defined(VX1000_OVERLAY_TLB)
        /* Map FLASH, cache inhibit, RWE */
        (void)vx1000_MMUWriteTLBEntry(VX1000_OVERLAY_TLB, VX1000_OVERLAY_ADDR, VX1000_OVERLAY_ADDR,
        VX1000_OVERLAY_SIZE, 1, 1, 0x3F); /* FLASH */
#elif defined(VX1000_OVERLAY_DESCR_IDX)
        (void)vx1000_MapCalRam(0, 0, 0);
#endif /* VX1000_OVERLAY_DESCR_IDX */
      }
      gVX1000_ECU_CalPage = page;
    }
  }
  else if ((mode & (VX1000_CAL_XCP)) != 0)
  {
    gVX1000_XCP_CalPage = page;
  }
#endif /* VX1000_OVERLAY_TLB | VX1000_OVERLAY_DESCR_IDX */
  else
  {
    ; /* the empty else case is here only for MISRA */
  }
  return retVal;
}
#endif /* VX1000_SET_CAL_PAGE_INTERNAL */


#if defined(VX1000_COPY_CAL_PAGE_INTERNAL)
static VX1000_UINT8 vx1000_CopyCalPage( VX1000_UINT8 srcSeg, VX1000_UINT8 srcPage, VX1000_UINT8 dstSeg, VX1000_UINT8 dstPage )
{
#if defined(VX1000_OVERLAY_VX_CONFIGURABLE)
  /* CopyCalPage has to be done by the VX in this mode */
  VX1000_UINT8 retVal = 1 /* TODO: use an appropriate error code define! */;
  srcSeg = srcSeg;   /* PRQA S 3199 */ /* Actually this dummy asignment is only here to prevent compiler/MISRA warnings about unused variables/parameters */
  dstSeg = dstSeg;   /* PRQA S 3199 */ /* Actually this dummy asignment is only here to prevent compiler/MISRA warnings about unused variables/parameters */
  srcPage = srcPage; /* PRQA S 3199 */ /* Actually this dummy asignment is only here to prevent compiler/MISRA warnings about unused variables/parameters */
  dstPage = dstPage; /* PRQA S 3199 */ /* Actually this dummy asignment is only here to prevent compiler/MISRA warnings about unused variables/parameters */
#elif defined(VX1000_OVERLAY_TLB) || defined(VX1000_OVERLAY_DESCR_IDX) || defined(VX1000_MPC56xCRAM_BASE_ADDR)
  VX1000_UINT8 retVal = 0;
  /* Check parameters */
  /* Only copy from RAM to FLASH makes sense */
  if ((srcSeg | dstSeg) != 0) { retVal = VX1000_CRC_SEGMENT_NOT_VALID; /* Segments are not supported */ }
  else if (dstPage == srcPage) { retVal = VX1000_CRC_PAGE_NOT_VALID; /* Can not copy on itself */ }
  else if (0==dstPage) { retVal = VX1000_CRC_ACCESS_DENIED; /* Can not copy to FLASH page  */ }
  /* Initialize CALRAM */
  /* Initialize CALRAM, copy from FLASH to RAM */
#if defined(VX1000_OVERLAY_TLB)
  /* Turn mapping of, if needed */
  else if (gVX1000_ECU_CalPage == (VX1000_CALPAGE_RAM))
  {
    (void)vx1000_SetCalPage(0, VX1000_CALPAGE_FLASH, VX1000_CAL_ECU);
    memcopy((void*)VX1000_CALRAM_ADDR, (void*)(VX1000_OVERLAY_ADDR), VX1000_OVERLAY_SIZE); /* FLASH->RAM */ /* PRQA S 0306 */ /* willingly violating MISRA Rule 11.3 here to be able to replace pointer arithmetic by integer arithmetic elsewhere */
    (void)vx1000_SetCalPage(0, VX1000_CALPAGE_RAM, VX1000_CAL_ECU);
  }
#endif /* VX1000_OVERLAY_TLB */
  else
  {
#if defined(VX1000_OVERLAY_TLB)
    memcopy((void*)VX1000_CALRAM_ADDR, (void*)(VX1000_OVERLAY_ADDR), VX1000_OVERLAY_SIZE); /* FLASH->RAM */ /* PRQA S 0306 */ /* willingly violating MISRA Rule 11.3 here to be able to replace pointer arithmetic by integer arithmetic elsewhere */
#elif defined(VX1000_OVERLAY_DESCR_IDX) || defined(VX1000_MPC56xCRAM_BASE_ADDR)
    memcopy((void*)VX1000_CALRAM_ADDR, (void*)((VX1000_OVERLAY_ADDR)^(VX1000_CALROM_ADDR_XOR)), VX1000_OVERLAY_SIZE); /* FLASH->RAM */ /* PRQA S 0306 */ /* willingly violating MISRA Rule 11.3 here to be able to replace pointer arithmetic by integer arithmetic elsewhere */
#endif /* VX1000_OVERLAY_DESCR_IDX | VX1000_MPC56xCRAM_BASE_ADDR */
  }
#endif /* VX1000_OVERLAY_TLB | VX1000_OVERLAY_DESCR_IDX & !VX1000_OVERLAY_VX_CONFIGURABLE */
  return retVal;
}
#endif /* VX1000_COPY_CAL_PAGE_INTERNAL */


#endif /* VX1000_MAILBOX_OVERLAY_CONTROL */


#endif /* VX1000_OVERLAY && !VX1000_COMPILED_FOR_SLAVECORES */


/*---------------------------------------------------------------------------- */
/* Mailbox */

#if defined(VX1000_MAILBOX) && !defined(VX1000_COMPILED_FOR_SLAVECORES)

#define VX1000_MAILBOX_FLG_CLR                0
#define VX1000_MAILBOX_FLG_SET                1

#if defined(VX1000_MAILBOX_PROVIDE_SPLITREAD)
static VX1000_UINT32 sFlgRdSplitPend;
#endif /* VX1000_MAILBOX_PROVIDE_SPLITREAD */
#if defined(VX1000_MAILBOX_PROVIDE_SPLITWRITE)
static VX1000_UINT32 sFlgWrSplitPend;
#endif /* VX1000_MAILBOX_PROVIDE_SPLITWRITE */


/* Initialize the Master->Slave and Slave->Master mailboxes. */
void vx1000_MailboxInit(void)
{
#if defined(VX1000_MAILBOX_PROVIDE_SPLITREAD)
  sFlgRdSplitPend = VX1000_MAILBOX_FLG_CLR;
#endif /* VX1000_MAILBOX_PROVIDE_SPLITREAD */
#if defined(VX1000_MAILBOX_PROVIDE_SPLITWRITE)
  sFlgWrSplitPend = VX1000_MAILBOX_FLG_CLR;
#endif /* VX1000_MAILBOX_PROVIDE_SPLITWRITE */

  gVX1000.Mailbox.Version = 0;
  gVX1000.Mailbox.SlotSize = (VX1000_MAILBOX_SLOT_DWORDS) << 2;
  gVX1000.Mailbox.MS_ReadIdx = 0;
  gVX1000.Mailbox.MS_WriteIdx = 0;
  gVX1000.Mailbox.SM_ReadIdx = 0;
  gVX1000.Mailbox.SM_WriteIdx = 0;
  gVX1000.Mailbox.RstReq = 0;
  gVX1000.Mailbox.RstAck = 0;
  gVX1000.Mailbox.MS_Slots = VX1000_MAILBOX_SLOTS;
  gVX1000.Mailbox.SM_Slots = VX1000_MAILBOX_SLOTS;
  gVX1000.Mailbox.MS_Ptr = (VX1000_UINT32)gVX1000.Mailbox.MSData; /* PRQA S 0306 */ /* willingly violating MISRA Rule 11.3 here to be able to replace pointer arithmetic by integer arithmetic elsewhere */ /* PRQA S 0309 */ /* not violating MISRA Rule 11.3 with respect to the type sizes: 32 bit are sufficient to hold either a 0:32 flat or a 10:16 paged pointer */
  gVX1000.Mailbox.SM_Ptr = (VX1000_UINT32)gVX1000.Mailbox.SMData; /* PRQA S 0306 */ /* willingly violating MISRA Rule 11.3 here to be able to replace pointer arithmetic by integer arithmetic elsewhere */ /* PRQA S 0309 */ /* not violating MISRA Rule 11.3 with respect to the type sizes: 32 bit are sufficient to hold either a 0:32 flat or a 10:16 paged pointer */
  gVX1000.Mailbox.MagicId = (VX1000_UINT32)VX1000_MAILBOX_MAGIC;
}


/* Write len bytes from pBuf to the Slave->Master mailbox */
/* and notify the master. */
VX1000_UINT32 vx1000_MailboxWrite(VX1000_UINT32 len, /**/const/**/ VX1000_CHAR* pBuf)
{
  VX1000_UINT32 retVal = VX1000_MAILBOX_OK;

  if (0U==(VX1000_MAILBOX_FREE_WR_SLOTS))
  {
    retVal = VX1000_MAILBOX_ERR_FULL;
  }
  else if (0==pBuf)
  {
    retVal = VX1000_MAILBOX_ERR_NULL;
  }
  else if (len > ((VX1000_MAILBOX_SLOT_DWORDS) << 2))
  {
    retVal = VX1000_MAILBOX_ERR_SIZE;
  }
  else
  {
    memcopy((void*)((VX1000_UINT32)((volatile VX1000_UINT32*)(&gVX1000.Mailbox.SMData[gVX1000.Mailbox.SM_WriteIdx][1]))),(void*)(VX1000_UINT32)pBuf, len); /* PRQA S 0311 */ /*PRQA S 0303 */ /*PRQA S 0306 */ /* compiler warnings about differing prototype of this library function and potential mismatch of type qualifiers force violation of MISRA Rules 11.3 and 11.5 */
    gVX1000.Mailbox.SMData[gVX1000.Mailbox.SM_WriteIdx][0] = len;
    gVX1000.Mailbox.SM_WriteIdx = (VX1000_UINT8)VX1000_MAILBOX_NEXT(gVX1000.Mailbox.SM_WriteIdx);
    VX1000_SPECIAL_EVENT(VX1000_EVENT_MAILBOX_UPDATE) /* note: EM00034754 did not mention to remove THIS one */ /* PRQA S 0303 */ /* cannot avoid violating MISRA Rule 11.3 because the user/compiler typically provides the HW address as an integer (it does not know the VX pointer types) */
  }
  return retVal;
}


#if defined(VX1000_MAILBOX_PROVIDE_SPLITWRITE)
/* Return a pointer to the data field of the next free Slave->Master mailbox slot in ppBuf. */
/* Once data is filled into the mailbox by the calling function, */
/* vx1000_MailboxWriteDone() has to be called to notify the master. */
VX1000_UINT32 vx1000_MailboxWriteSplit(VX1000_UINT32 **ppBuf)
{
  VX1000_UINT32 retVal = VX1000_MAILBOX_OK;

  if (0U==(VX1000_MAILBOX_FREE_WR_SLOTS))
  {
    retVal = VX1000_MAILBOX_ERR_FULL;
  }
  else if (0==ppBuf)
  {
    retVal = VX1000_MAILBOX_ERR_NULL;
  }
  else if (sFlgWrSplitPend != 0)
  {
    retVal = VX1000_MAILBOX_ERR_SPLIT_PEND;
  }
  else
  {
    sFlgWrSplitPend = VX1000_MAILBOX_FLG_SET;
    *ppBuf = (VX1000_UINT32 *)(VX1000_UINT32)&gVX1000.Mailbox.SMData[gVX1000.Mailbox.SM_WriteIdx][1]; /* PRQA S 0306 */ /* possible compiler warnings about loosing qualifiers force violating MISRA Rule 11.3 here by explicit dummy cast chain */
  }
  return retVal;
}


/* Finish a Slave->Master mailbox transfer that has been started with vx1000_MailboxWriteSplit(). */
VX1000_UINT32 vx1000_MailboxWriteDone(VX1000_UINT32 len)
{
  VX1000_UINT32 retVal = VX1000_MAILBOX_OK;

  if (len > ((VX1000_MAILBOX_SLOT_DWORDS) << 2))
  {
    retVal = VX1000_MAILBOX_ERR_SIZE;
  }
  else if (0==sFlgWrSplitPend)
  {
    retVal = VX1000_MAILBOX_ERR_SPLIT_PEND;
  }
  else
  {
    sFlgWrSplitPend = VX1000_MAILBOX_FLG_CLR;
    gVX1000.Mailbox.SMData[gVX1000.Mailbox.SM_WriteIdx][0] = len;
    gVX1000.Mailbox.SM_WriteIdx = (VX1000_UINT8)VX1000_MAILBOX_NEXT(gVX1000.Mailbox.SM_WriteIdx);
    VX1000_SPECIAL_EVENT(VX1000_EVENT_MAILBOX_UPDATE) /* note: EM00034754 did not mention to remove THIS one */ /* PRQA S 0303 */ /* cannot avoid violating MISRA Rule 11.3 because the user/compiler typically provides the HW address as an integer (it does not know the VX pointer types) */
  }
  return retVal;
}
#endif /* VX1000_MAILBOX_PROVIDE_SPLITWRITE */

/* Read the data from the next filled Master->Slave mailbox slot */
/* into pBuf and return the number of bytes in pLen. */
/* Upon calling pLen contains the maximum number of bytes that */
/* can be read into pBuf. If the number of bytes exceeds pLen, */
/* an error is returned and no data is copied. */
VX1000_UINT32 vx1000_MailboxRead(VX1000_UINT32* pLen, VX1000_CHAR* pBuf)
{
  VX1000_UINT32 len;
  VX1000_UINT32 retVal = VX1000_MAILBOX_OK;

  if (0==(VX1000_MAILBOX_USED_RD_SLOTS))
  {
    retVal = VX1000_MAILBOX_ERR_EMPTY;
  }
  else if ((0==pLen) || (0==pBuf))
  {
    retVal = VX1000_MAILBOX_ERR_NULL;
  }
  else
  {
    len = gVX1000.Mailbox.MSData[gVX1000.Mailbox.MS_ReadIdx][0];
    if (len > *pLen)
    {
      retVal = VX1000_MAILBOX_ERR_SIZE;
    }
#if defined(VX1000_MAILBOX_PROVIDE_SPLITREAD)
    else if (sFlgRdSplitPend != 0)
    {
      retVal = VX1000_MAILBOX_ERR_SPLIT_PEND;
    }
#endif /* VX1000_MAILBOX_PROVIDE_SPLITREAD */
    else
    {
      *pLen = len;
      memcopy(pBuf, (void*)((VX1000_UINT32)((volatile VX1000_UINT32*)(&gVX1000.Mailbox.MSData[gVX1000.Mailbox.MS_ReadIdx][1]))), len);  /*PRQA S 0303 */ /*PRQA S 0306 */ /* compiler warnings about differing prototype of this library function and potential mismatch of type qualifiers force violation of MISRA Rules 11.3 */
      gVX1000.Mailbox.MS_ReadIdx = (VX1000_UINT8)VX1000_MAILBOX_NEXT(gVX1000.Mailbox.MS_ReadIdx);
      /* VX1000_SPECIAL_EVENT(VX1000_EVENT_MAILBOX_UPDATE) -- removed due to EM00034754 */
    }
  }
  return retVal;
}


#if defined(VX1000_MAILBOX_PROVIDE_SPLITREAD)
/* Return the data length in pLen and a pointer on the data of the next */
/* filled Master->Slave mailbox slot in ppBuf. */
/* Once the mailbox slot is read by the calling function, mailboxReadDone() */
/* must be called to mark the mailbox slot as empty and notify the master. */
VX1000_UINT32 vx1000_MailboxReadSplit(VX1000_UINT32* pLen, VX1000_UINT32** ppBuf)
{
  VX1000_UINT32 retVal = VX1000_MAILBOX_OK;

  if (0==(VX1000_MAILBOX_USED_RD_SLOTS))
  {
    retVal = VX1000_MAILBOX_ERR_EMPTY;
  }
  else if ((0==pLen) || (0==ppBuf))
  {
    retVal = VX1000_MAILBOX_ERR_NULL;
  }
  else if (sFlgRdSplitPend != 0)
  {
    retVal = VX1000_MAILBOX_ERR_SPLIT_PEND;
  }
  else
  {
    sFlgRdSplitPend = VX1000_MAILBOX_FLG_SET;
    *pLen = gVX1000.Mailbox.MSData[gVX1000.Mailbox.MS_ReadIdx][0];
    *ppBuf = (VX1000_UINT32*)(VX1000_UINT32)&gVX1000.Mailbox.MSData[gVX1000.Mailbox.MS_ReadIdx][1]; /* PRQA S 0306 */ /* possible compiler warnings about loosing qualifiers force violating MISRA Rule 11.3 here by explicit dummy cast chain */
  }
  return retVal;
}

/* Mark the filled Master->Slave mailbox slot as empty, that has been returned */
/* by previously calling mailboxReadSplit(). Notify the master afterwards. */
VX1000_UINT32 vx1000_MailboxReadDone(void)                                        /* used by flash kernels */
{
  VX1000_UINT32 retVal = VX1000_MAILBOX_OK;

  if (0==sFlgRdSplitPend)
  {
    retVal = VX1000_MAILBOX_ERR_SPLIT_PEND;
  }
  else
  {
    sFlgRdSplitPend = VX1000_MAILBOX_FLG_CLR;
    gVX1000.Mailbox.MS_ReadIdx = (VX1000_UINT8)VX1000_MAILBOX_NEXT(gVX1000.Mailbox.MS_ReadIdx);
    /* VX1000_SPECIAL_EVENT(VX1000_EVENT_MAILBOX_UPDATE) -- removed due to EM00034754 */
  }
  return retVal;
}
#endif /* VX1000_MAILBOX_PROVIDE_SPLITREAD */


/* XCP compliant protocol handler - though these defines are only visible inside this module, they still may conflict whith stuff coming in via vx1000_tc2xx_cfg.h subincludes, so better add VX1000_-pefices */
#define VX1000_CRM_CMD txBuf[0]
#define VX1000_CRM_ERR txBuf[1]


/* XCP calibration page handling */
#if defined(VX1000_MAILBOX_OVERLAY_CONTROL)

#define CC_SET_CAL_PAGE                    0xEB
#define CC_GET_CAL_PAGE                    0xEA
#define CC_COPY_CAL_PAGE                   0xE4

/* SET_CAL_PAGE */
#define CRO_SET_CAL_PAGE_LEN               4
#define CRO_SET_CAL_PAGE_MODE              rxBuf[(1)]
#define CRO_SET_CAL_PAGE_SEGMENT           rxBuf[(2)]
#define CRO_SET_CAL_PAGE_PAGE              rxBuf[(3)]
#define CRM_SET_CAL_PAGE_LEN               1

/* GET_CAL_PAGE */
#define CRO_GET_CAL_PAGE_LEN               3
#define CRO_GET_CAL_PAGE_MODE              rxBuf[(1)]
#define CRO_GET_CAL_PAGE_SEGMENT           rxBuf[(2)]
#define CRM_GET_CAL_PAGE_LEN               4
#define CRM_GET_CAL_PAGE_PAGE              txBuf[(3)]

/* COPY_CAL_PAGE */
#define CRO_COPY_CAL_PAGE_LEN              5
#define CRO_COPY_CAL_PAGE_SRC_SEGMENT      rxBuf[(1)]
#define CRO_COPY_CAL_PAGE_SRC_PAGE         rxBuf[(2)]
#define CRO_COPY_CAL_PAGE_DEST_SEGMENT     rxBuf[(3)]
#define CRO_COPY_CAL_PAGE_DEST_PAGE        rxBuf[(4)]
#define CRM_COPY_CAL_PAGE_LEN              1
#endif /* VX1000_MAILBOX_OVERLAY_CONTROL */

/* flash programming via mailbox */
#if defined(VX1000_MAILBOX_FLASH)

/* Commands: */
#define CC_PROGRAM_START                   0xD2
#define CC_PROGRAM_CLEAR                   0xD1
#define CC_PROGRAM                         0xD0
#define CC_PROGRAM_RESET                   0xCF
#define CC_PROGRAM_PREPARE                 0xCC
#define CC_PROGRAM_NEXT                    0xCA
#define CC_PROGRAM_MAX                     0xC9
#define CC_PROGRAM_VERIFY                  0xC8

/* Data for: */
/* - PROGRAM_PREPARE */
#define CRO_PROGRAM_PREPARE_LEN            4
#define CRO_PROGRAM_PREPARE_SIZE           *((VX1000_UINT16*)&rxBuf[(2)])
#define CRM_PROGRAM_PREPARE_LEN            1

/* - PROGRAM_START */
#define CRO_PROGRAM_START_LEN              1
#define CRM_PROGRAM_START_LEN              7
#define CRM_PROGRAM_COMM_MODE_PGM          txBuf[(2)]
#define CRM_PROGRAM_MAX_CTO_PGM            txBuf[(3)]
#define CRM_PROGRAM_MAX_BS_PGM             txBuf[(4)]
#define CRM_PROGRAM_MIN_ST_PGM             txBuf[(5)]
#define CRM_PROGRAM_QUEUE_SIZE_PGM         txBuf[(6)]

/* - PROGRAM_CLEAR */
#define CRO_PROGRAM_CLEAR_LEN              8
#define CRO_PROGRAM_CLEAR_MODE             rxBuf[(1)]
#define CRO_PROGRAM_CLEAR_SIZE             *((VX1000_UINT32*)&rxBuf[(4)])
#define CRM_PROGRAM_CLEAR_LEN              1

/* - PROGRAM */
#define CRO_PROGRAM_MAX_SIZE               ((VX1000_UINT8)((kXcpMaxCTO - 2) / kXcpAG))
#define CRO_PROGRAM_LEN                    2 /* + CRO_PROGRAM_SIZE */
#define CRO_PROGRAM_SIZE                   rxBuf[(1)]
#define CRO_PROGRAM_DATA                   (&rxBuf[(2))]
#define CRM_PROGRAM_LEN                    1

#endif /* VX1000_MAILBOX_FLASH */

#if defined(VX1000_MAILBOX_OVERLAY_CONTROL) || defined(VX1000_MAILBOX_FLASH)
void vx1000_MailboxHandler(void)
{
  VX1000_UINT32 rxLen, txLen;
  VX1000_CHAR rxBuf[8], txBuf[8];
  VX1000_UINT8 postponeAnswer = 0;

  rxLen = 8;
  if ((VX1000_MAILBOX_OK) == vx1000_MailboxRead(&rxLen, &rxBuf[0]))
  {
    txLen = 2;
    VX1000_CRM_CMD = (VX1000_CHAR)-1;
    VX1000_CRM_ERR = 0x00;
    switch ((VX1000_UINT8)rxBuf[0]) /* check CRO_CMD */
    {
#if defined(VX1000_MAILBOX_OVERLAY_CONTROL)
    case (VX1000_UINT8)(CC_GET_CAL_PAGE):
      CRM_GET_CAL_PAGE_PAGE = (VX1000_CHAR)(VX1000_GET_CAL_PAGE((CRO_GET_CAL_PAGE_SEGMENT), (CRO_GET_CAL_PAGE_MODE)));
      txLen = CRM_GET_CAL_PAGE_LEN;
      break;
    case (VX1000_UINT8)(CC_SET_CAL_PAGE):
      VX1000_CRM_ERR = (VX1000_CHAR)(VX1000_SET_CAL_PAGE((CRO_SET_CAL_PAGE_SEGMENT), (CRO_SET_CAL_PAGE_PAGE), (CRO_SET_CAL_PAGE_MODE),0));
      if (VX1000_CRM_ERR == VX1000_CRC_CMD_BUSY)
      {
        postponeAnswer = 1; /* Async call. Mailbox answer will be sent in the callback done macro.*/
      }
      txLen = CRM_SET_CAL_PAGE_LEN;
      break;
    case (VX1000_UINT8)(CC_COPY_CAL_PAGE):
      VX1000_CRM_ERR = (VX1000_CHAR)(VX1000_COPY_CAL_PAGE((CRO_COPY_CAL_PAGE_SRC_SEGMENT),  (CRO_COPY_CAL_PAGE_SRC_PAGE),
      (CRO_COPY_CAL_PAGE_DEST_SEGMENT), (CRO_COPY_CAL_PAGE_DEST_PAGE)));
      txLen = CRM_COPY_CAL_PAGE_LEN;
      break;
#endif /* VX1000_MAILBOX_OVERLAY_CONTROL */
#if defined(VX1000_MAILBOX_FLASH)
    case (VX1000_UINT8)(CC_PROGRAM_PREPARE):
      /* CRO_PROGRAM_PREPARE_SIZE */
      /* @@@@ Problem: get start addr from a previous SET_MTA, then check that FKL memory does not interfer */
      /*      (HOW to get FKL end address?!?) with current RAM usage and in case provide error handling !   */
      txLen = CRM_PROGRAM_PREPARE_LEN;
      break;
    case (VX1000_UINT8)(CC_PROGRAM_START):
      CRM_PROGRAM_COMM_MODE_PGM = 0;
      CRM_PROGRAM_MAX_CTO_PGM = 8;
      CRM_PROGRAM_MAX_BS_PGM = 1;
      CRM_PROGRAM_MIN_ST_PGM = 0;
      CRM_PROGRAM_QUEUE_SIZE_PGM = 1;
      txLen = CRM_PROGRAM_START_LEN;
      /* @@@@ Problem: how to actually program flash? */
      break;
#endif /* VX1000_MAILBOX_FLASH */
    default:
      VX1000_CRM_ERR = VX1000_CRC_CMD_UNKNOWN;
      break;
    }
#if defined(VX1000_OVLENBL_SYNC_PAGESWITCH)
    if (0==postponeAnswer)
#endif /* VX1000_OVLENBL_SYNC_PAGESWITCH */
    {
      if ((VX1000_CRM_ERR) != 0) 
      { 
        VX1000_CRM_CMD = (VX1000_CHAR)-2; 
        txLen = 2;
      }
      if ((VX1000_CRM_ERR) != 0) { VX1000_CRM_CMD = (VX1000_CHAR)-2; }
      (void)vx1000_MailboxWrite(txLen, txBuf);
    }
  }
}
#endif /* VX1000_MAILBOX_OVERLAY_CONTROL, VX1000_MAILBOX_FLASH */

#if defined(VX1000_MAILBOX_PRINTF)
static void vx1000_MailboxPutchar( VX1000_CHAR character )
{
  static VX1000_CHAR vx1000EvBuf[8];
  static VX1000_UINT32 vx1000EvLen;

  if ((vx1000EvBuf[0] != (VX1000_CHAR)-4) || (vx1000EvBuf[1] != (VX1000_CHAR)1) || (vx1000EvLen >= 8UL)) /* PRQA S 3355 */ /* though it's true that this algorithm speculates on probabilities, MISRA Rule 13.7 is NOT violated (the checker ignored the static nature of the variables) */
  {
    /* initialize the protocol header once */
    vx1000EvBuf[0] = (VX1000_CHAR)-4; /* SERV */
    vx1000EvBuf[1] = (VX1000_CHAR)1;  /* SERV_TEXT */
    vx1000EvLen = 2;
  }
  vx1000EvBuf[vx1000EvLen] = character;
  vx1000EvLen++;
  if ( (vx1000EvLen >= 8) || (0==character) ) /* PRQA S 3356 */ /* though it's true that this algorithm speculates on probabilities, MISRA Rule 13.7 is NOT violated (the checker ignored the static nature of the variables) */
  {
    (void)vx1000_MailboxWrite(vx1000EvLen, vx1000EvBuf);
    vx1000EvLen = 2;
  }
}

#if !defined(VX1000_PRINTF_MINIMAL)
/* The full printf library function not only occupies two dozens of Kbytes TEXT but also allocates */
/* a rather huge amount of stack upon calls so it is not well suited for multitasking environments */
void vx1000_MailboxPrintf( const VX1000_CHAR *format, ... ) /* PRQA S 5069 */ /* Cannot avoid violating MISRA Rule 19.4 because a C++-based workaround would only violate other rules */
{
  va_list argptr;
  VX1000_CHAR buf[256];

  va_start(argptr,format);
  (void)vsprintf((VX1000_CHAR*)buf, (const VX1000_CHAR*)format, argptr);
  va_end(argptr); /* PRQA S 3199 */ /* it depends on the used compiler whether this statement has useful side effects or not */

  /* Transmit the text message */
  {
    VX1000_CHAR *p = buf;
    while (*p != 0)
    {
      vx1000_MailboxPutchar((VX1000_CHAR)*p);
      p++; /* PRQA S 0489 */ /* cannot avoid violating MISRA Rule 17.4 because memcopy and thus pointer incrementation is a core feature of this application */
    }
  }

  /* Transmit the terminating 0x00. */
  vx1000_MailboxPutchar( 0x00 );
}
#else /* VX1000_PRINTF_MINIMAL */
/* This reduced printf implementation does not support features like float support or argument */
/* repetition, but it usually uses less TEXT and RAM usage is only a few bytes at runtime.     */

#define PF_NONE          0U
#define PF_IN_TOKEN      1U
#define PF_IN_WIDTH      2U
#define PF_AFTER_WIDTH   4U
#define PF_IN_PREC       8U
#define PF_AFTER_PREC   16U
#define PF_IN_SIZE      32U
#define PF_AFTER_SIZE   64U
#define PF_INVALID     128U
#define GENBIN2DECAUXSTAGE(B)                                                                                                   \
if (number >=(2*(B))){ if (number >=(6*(B))){ if (number >=(8*(B))) { if (number >=(9*(B))){ number -= 9*(B); *p = '9'; p++;}   \
                                                                      else                 { number -= 8*(B); *p = '8'; p++;}}  \
                                              else                  { if (number >=(7*(B))){ number -= 7*(B); *p = '7'; p++;}   \
                                                                      else                 { number -= 6*(B); *p = '6'; p++;}}} \
                       else                 { if (number >=(4*(B))) { if (number >=(5*(B))){ number -= 5*(B); *p = '5'; p++;}   \
                                                                      else                 { number -= 4*(B); *p = '4'; p++;}}  \
                                              else                  { if (number >=(3*(B))){ number -= 3*(B); *p = '3'; p++;}   \
                                                                      else                 { number -= 2*(B); *p = '2'; p++;}}}}\
else                 { if (number >=(1*(B)))                                               { number -= 1*(B); *p = '1'; p++;}   \
                       else                                                                { number -= 0*(B); *p = '0'; p++;}}

void vx1000_MailboxPrintf( const VX1000_CHAR *format, ... ) /* PRQA S 5069 */ /* Cannot avoid violating MISRA Rule 19.4 because a C++-based workaround would only violate other rules */
{
  va_list arglist;
  VX1000_LONG number;
  VX1000_LDOUBL fdummy = 0.0;
  VX1000_CHAR *p2, numbuf[24];
  VX1000_INT   i, width=0, prec=0, size=0, basesize=0;
  VX1000_CHAR  c, digif=0, filler=0, alignleft=0, showpos=0, showbase=0, *p, signedtype=0, floattype=0;
  VX1000_UINT8 state;

  va_start(arglist,format);
  state = (PF_NONE);
  for (; *format != 0; format++) /* PRQA S 0489 */ /* cannot avoid violating MISRA Rule 17.4 because memcopy and thus pointer incrementation is a core feature of this application */
  {
    c = *format;
    if (state == (PF_NONE))
    {
      if (c == '%')
      {
        state = (PF_IN_TOKEN);
        width = prec = size = basesize = 0;
        floattype = signedtype = 0;
        filler = showpos = ' ';
      }
      else
      {
        vx1000_MailboxPutchar(c);
      }
      continue;
    }
    if (c == '%')
    {
      if (state == (PF_IN_TOKEN)) /* no real token, just an escaped "%" */
      {
        state = (PF_NONE);
        vx1000_MailboxPutchar(c);
      }
      else
      {
        state |= (PF_INVALID);
      }
      continue;
    }
    if ((c >= '0') && (c <= '9'))
    {
      c -= '0';
      switch (state & ((PF_IN_WIDTH) | (PF_IN_PREC) | (PF_IN_SIZE))) /* continue in a number */
      {
        case (PF_IN_WIDTH): width = (10 * width) + (VX1000_INT)c; continue;
        case (PF_IN_PREC):  prec  = (10 * prec ) + (VX1000_INT)c; continue;
        case (PF_IN_SIZE):  size  = (10 * size ) + (VX1000_INT)c; continue;
        default:  /* only here for MISRA */
          break; /* only here for MISRA */
      }
      if (0==(state & (PF_AFTER_WIDTH))) /* start reading width number */
      {
        state |= (PF_IN_WIDTH);
        width = (VX1000_INT)c;
        if (c==0) { filler = '0'; }
        continue;
      }
      if (0==(state & (PF_AFTER_PREC))) /* start reading precission number */
      {
        state |= (PF_IN_PREC);
        prec = (VX1000_INT)c;
        continue;
      }
      state |= (PF_INVALID);
      continue;
    }
    switch (state & ((PF_IN_WIDTH) | (PF_IN_PREC) | (PF_IN_SIZE))) /* stop any number */
    {
      case (PF_IN_WIDTH): state ^= (PF_IN_WIDTH) | (PF_AFTER_WIDTH); break;
      case (PF_IN_PREC):  state ^= (PF_IN_PREC)  | (PF_AFTER_PREC);  break;
      case (PF_IN_SIZE):  state ^= (PF_IN_SIZE)  | (PF_AFTER_SIZE);  break;
      default:  /* only here for MISRA */
        break; /* only here for MISRA */
    }
    if (c == 'l')
    {
      state |= (PF_IN_SIZE);
      size += (VX1000_INT)(8 * (sizeof(VX1000_LONG) - sizeof(VX1000_INT)));
      if (size > 64) { state |= (PF_INVALID); }
      continue;
    }
    switch (c) /* get the format specifier */
    {
      case '#':
        showbase = 1;
        continue;
      case '+':
        showpos = '+';
        continue;
      case '-':
        alignleft = 1;
        continue;
      case 'L':
        state |= (PF_AFTER_SIZE);
        size = 256;
        continue;
      case 'I': /* microsoft specific size: start reading the number */
        state |= (PF_IN_SIZE);
        continue;
      case 'c':
        vx1000_MailboxPutchar((VX1000_CHAR)va_arg(arglist, VX1000_INT));
        state = (PF_NONE);
        continue;
      case 's':
        /* todo: care for alignment */
        p = (VX1000_CHAR*)va_arg(arglist, VX1000_CHAR*);
        while (*p != 0) { vx1000_MailboxPutchar(*p); p++; } /* PRQA S 0489 */ /* cannot avoid violating MISRA Rule 17.4 because memcopy and thus pointer incrementation is a core feature of this application */
        state = (PF_NONE);
        continue;
      case 'o':
        digif = '7';
        basesize = 8 * sizeof(VX1000_INT);
        break;
      case 'd':
        /*fallthrough*/
      case 'i':
        signedtype = 1;
        /*fallthrough*/
      case 'u':
        digif = '9';
        basesize = 8 * sizeof(VX1000_INT);
        break;
      case 'X':
        digif = 'F';
        basesize = 8 * sizeof(VX1000_INT);
        break;
      case 'x':
        digif = 'f';
        basesize = 8 * sizeof(VX1000_INT);
        break;
      case 'e':
      case 'E':
        /* scientific floats are not supported */
        basesize = 128;
        floattype= 1;
        break;
      case 'f':
      case 'F':
      case 'g':
      case 'G':
        /* fixed floats are not supported */
        basesize = 64;
        floattype= 1;
        break;
      default:
        state |= (PF_INVALID);
    }
    if (0==(state & (PF_INVALID)))
    {
      state = (PF_NONE);
      if (size < basesize) { size = basesize; }
      if (floattype != 0)
      {
        switch (size)
        {
          /* floats currently are not supported - just advance the pointer and print a hash sign as placeholder */
          case 64:  vx1000_MailboxPutchar('#'); fdummy = (VX1000_LDOUBL)va_arg(arglist, /*float*/VX1000_DOUBLE); continue;
          case 128: vx1000_MailboxPutchar('#'); fdummy = (VX1000_LDOUBL)va_arg(arglist, VX1000_DOUBLE);          continue;
          case 256: vx1000_MailboxPutchar('#'); fdummy = (VX1000_LDOUBL)va_arg(arglist, VX1000_LDOUBL);          continue;
          default:    /* only here for MISRA */
            fdummy++; /* just to avoid compiler warnings */ /* PRQA S 3199 */ /* dont't care - it's just a dummy! */
            break;    /* only here for MISRA */
        }
      }
      else
      {
        switch (size)
        {
          case 8 * sizeof(VX1000_INT): number = (VX1000_LONG)va_arg(arglist, VX1000_INT);   break;
          default:                     number = (VX1000_LONG)va_arg(arglist, VX1000_LONG);  break;
          }
          if (signedtype != 0)
          {
            if (number < 0)
            {
              number = -number;
              showpos = '-';
            }
        }
        if (showpos != ' ') { vx1000_MailboxPutchar(showpos); }
        switch (digif)
        {
          case '7':
            i = 11;
            numbuf[i] = 0;
            while (i != 0)
            {
              i--; numbuf[i] = (VX1000_CHAR)(0x7UL & (VX1000_UINT32)number);
              numbuf[i] += '0';
              number >>= 3; /* PRQA S 0502 */ /* Actually not violating MISRA Rule 12.7 because any bits shifted in are masked before usage of the result */
            }
            break;
          case '9':
            /*numbuf[10] = 0;*/
            p = numbuf;
            if ((VX1000_UINT32)number >= (2 * 1000000000UL))  { if ((VX1000_UINT32)number >= (4UL * 1000000000UL))
                                                                     { number -= 2 * 1000000000L;
                                                                       number -= 2 * 1000000000L; *p = '4'; p++;} /* PRQA S 0489 */ /* cannot avoid violating MISRA Rule 17.4 because memcopy and thus pointer incrementation is a core feature of this application */
                                               else             { if ((VX1000_UINT32)number >= (3UL * 1000000000UL))
                                                                     { number -= 2 * 1000000000L;
                                                                       number -= 1 * 1000000000L; *p = '3'; p++;} /* PRQA S 0489 */ /* cannot avoid violating MISRA Rule 17.4 because memcopy and thus pointer incrementation is a core feature of this application */
                                                                  else
                                                                     { number -= 2 * 1000000000L; *p = '2'; p++;}}} /* PRQA S 0489 */ /* cannot avoid violating MISRA Rule 17.4 because memcopy and thus pointer incrementation is a core feature of this application */
            else                             { if ((VX1000_UINT32)number >= (1 * 1000000000UL))
                                                                     { number -= 1 * 1000000000L; *p = '1'; p++;} /* PRQA S 0489 */ /* cannot avoid violating MISRA Rule 17.4 because memcopy and thus pointer incrementation is a core feature of this application */
                                               else
                                                                     { number -= 0 * 1000000000L; *p = '0'; p++;}} /* PRQA S 0489 */ /* cannot avoid violating MISRA Rule 17.4 because memcopy and thus pointer incrementation is a core feature of this application */
            GENBIN2DECAUXSTAGE(100000000L) /* PRQA S 0715 */ /* if the compiler warns that it does not support more than 15 nested IFs, disable the VX1000_PRINTF_MINIMAL define in the header */ /* PRQA S 0489 */ /* cannot avoid violating MISRA Rule 17.4 because memcopy and thus pointer incrementation is a core feature of this application */
            GENBIN2DECAUXSTAGE(10000000L)  /* PRQA S 0715 */ /* if the compiler warns that it does not support more than 15 nested IFs, disable the VX1000_PRINTF_MINIMAL define in the header */ /* PRQA S 0489 */ /* cannot avoid violating MISRA Rule 17.4 because memcopy and thus pointer incrementation is a core feature of this application */
            GENBIN2DECAUXSTAGE(1000000L)   /* PRQA S 0715 */ /* if the compiler warns that it does not support more than 15 nested IFs, disable the VX1000_PRINTF_MINIMAL define in the header */ /* PRQA S 0489 */ /* cannot avoid violating MISRA Rule 17.4 because memcopy and thus pointer incrementation is a core feature of this application */
            GENBIN2DECAUXSTAGE(100000L)    /* PRQA S 0715 */ /* if the compiler warns that it does not support more than 15 nested IFs, disable the VX1000_PRINTF_MINIMAL define in the header */ /* PRQA S 0489 */ /* cannot avoid violating MISRA Rule 17.4 because memcopy and thus pointer incrementation is a core feature of this application */
            GENBIN2DECAUXSTAGE(10000L)     /* PRQA S 0715 */ /* if the compiler warns that it does not support more than 15 nested IFs, disable the VX1000_PRINTF_MINIMAL define in the header */ /* PRQA S 0489 */ /* cannot avoid violating MISRA Rule 17.4 because memcopy and thus pointer incrementation is a core feature of this application */
            GENBIN2DECAUXSTAGE(1000L)      /* PRQA S 0715 */ /* if the compiler warns that it does not support more than 15 nested IFs, disable the VX1000_PRINTF_MINIMAL define in the header */ /* PRQA S 0489 */ /* cannot avoid violating MISRA Rule 17.4 because memcopy and thus pointer incrementation is a core feature of this application */
            GENBIN2DECAUXSTAGE(100L)       /* PRQA S 0715 */ /* if the compiler warns that it does not support more than 15 nested IFs, disable the VX1000_PRINTF_MINIMAL define in the header */ /* PRQA S 0489 */ /* cannot avoid violating MISRA Rule 17.4 because memcopy and thus pointer incrementation is a core feature of this application */
            GENBIN2DECAUXSTAGE(10L)        /* PRQA S 0715 */ /* if the compiler warns that it does not support more than 15 nested IFs, disable the VX1000_PRINTF_MINIMAL define in the header */ /* PRQA S 0489 */ /* cannot avoid violating MISRA Rule 17.4 because memcopy and thus pointer incrementation is a core feature of this application */
            GENBIN2DECAUXSTAGE(1L)         /* PRQA S 0715 */ /* if the compiler warns that it does not support more than 15 nested IFs, disable the VX1000_PRINTF_MINIMAL define in the header */ /* PRQA S 0489 */ /* cannot avoid violating MISRA Rule 17.4 because memcopy and thus pointer incrementation is a core feature of this application */
            *p = 0;
            number = number; p = p; /* PRQA S 3198 */ /* dummy assignments to avoid compiler warnings. Well, some other compilers complain now about "possible redundant expression" */
            break;
          case 'F': /* fallthrough to 'f' */
          case 'f':
            digif -= 15;
            i = 8;
            numbuf[i] = 0;
            while (i != 0)
            {
              c = (VX1000_CHAR)(0xfUL & (VX1000_UINT32)number);
              number >>= 4; /* PRQA S 0502 */ /* Actually not violating MISRA Rule 12.7 because any bits shifted in are masked before usage of the result */
              c += (VX1000_CHAR)((c > 9) ? digif : (VX1000_CHAR)'0');
              i--; numbuf[i] = c;
            }
            break;
          default:  /* only here for MISRA */
            break; /* only here for MISRA */
        }
        for (p = numbuf; *p == '0'; p++) { } /* PRQA S 0489 */ /* cannot avoid violating MISRA Rule 17.4 because memcopy and thus pointer incrementation is a core feature of this application */
        if (0==*p) { p--; } /* PRQA S 0489 */ /* cannot avoid violating MISRA Rule 17.4 because strcat and thus pointer deccrementation is a core feature of this application */
        if (alignleft == 1)
        {
          if ((showbase == 1) && (digif != '9'))
          {
            vx1000_MailboxPutchar((VX1000_CHAR)'0');
            if (digif != '7') { vx1000_MailboxPutchar((VX1000_CHAR)'x'); }
          }
          for (; *p != 0; p++) { vx1000_MailboxPutchar(*p); width--; } /* PRQA S 0489 */ /* cannot avoid violating MISRA Rule 17.4 because memcopy and thus pointer incrementation is a core feature of this application */
          for (--width; width >= 0; width--) { vx1000_MailboxPutchar(' ');  }
        }
        else
        {
          for (p2 = p; *p2 != 0; p2++) { width--; } /* PRQA S 0489 */ /* cannot avoid violating MISRA Rule 17.4 because memcopy and thus pointer incrementation is a core feature of this application */
          for (--width; width >= 0; width--) { vx1000_MailboxPutchar(filler); }
          if ((showbase == 1) && (digif != '9'))
          {
            vx1000_MailboxPutchar((VX1000_CHAR)'0');
            if (digif != '7') { vx1000_MailboxPutchar((VX1000_CHAR)'x'); }
          }
          for (; *p != 0; p++) { vx1000_MailboxPutchar(*p); } /* PRQA S 0489 */ /* cannot avoid violating MISRA Rule 17.4 because memcopy and thus pointer incrementation is a core feature of this application */
        }
        continue;
      }
    }
    /* reaching here means we misunderstood the format string and assumed stupid argument. /
    /  type. To avoid invalid pointer accesses to any following arguments, we leave now:  */
    vx1000_MailboxPutchar('!');vx1000_MailboxPutchar(64);vx1000_MailboxPutchar('!');
    break; /* the for loop */
  } /* for */
  va_end(arglist); /* PRQA S 3199 */ /* it depends on the used compiler whether this statement has useful side effects or not */
  /* Transmit the terminating 0x00. */
  vx1000_MailboxPutchar( (VX1000_UINT8)0x00U );
}
#endif /* VX1000_PRINTF_MINIMAL */



#endif /* VX1000_MAILBOX_PRINTF */
#endif /* VX1000_MAILBOX  && !VX1000_COMPILED_FOR_SLAVECORES */


#if defined(VX1000_FKL_SUPPORT_ADDR) && !defined(VX1000_COMPILED_FOR_SLAVECORES)
void vx1000_FlashPrepareLoop(void)
{
  gVX1000.ToolDetectState |= (VX1000_UINT32)(VX1000_TDS_FKL_REQ_DETECTED);
  VX1000_DISABLE_ALL_INTERRUPTS()                /* prevent other tasks / ISRs from overwriting RAM / reading flash */
  VX1000_STOP_OS_TIMING_PROTECTION()             /* prevent Autosar OS in SC2 / SC4 from killing the loop/kernel */
  VX1000_STOP_OTHER_CORES()                      /* prevent other cores (or DMA/PEC) from overwriting RAM / reading flash */
  VX1000_ENABLE_STANDARD_RAM_MAPPING()           /* repair nondefault RAM mapping (the FKL is linked to standard location) */

  (VX1000_FKL_WORKSPACE)->DeprotectState    = (VX1000_UINT16)(VX1000_FKL_STATE1CODE); /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because access to unallocated address space requires usage of pointers */
  (VX1000_FKL_WORKSPACE)->DeprotectTrigger  = (VX1000_UINT16)(VX1000_FKL_TOSTATE1CODE); /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because access to unallocated address space requires usage of pointers */
  (VX1000_FKL_WORKSPACE)->TransitionTimeout = (VX1000_UINT32)(VX1000_FKL_TRANSITION_TIMEOUT); /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because access to unallocated address space requires usage of pointers */
  (VX1000_FKL_WORKSPACE)->WdgData1 = (VX1000_UINT32)0xFFFFffffUL; /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because access to unallocated address space requires usage of pointers */
  (VX1000_FKL_WORKSPACE)->WdgData2 = (VX1000_UINT32)0xFFFFffffUL; /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because access to unallocated address space requires usage of pointers */
  (VX1000_FKL_WORKSPACE)->WdgData3 = (VX1000_UINT32)0xFFFFffffUL; /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because access to unallocated address space requires usage of pointers */
  *(VX1000_UINT32*)&((VX1000_FKL_WORKSPACE)->EntryPoint) = (VX1000_UINT32)0xFFFFFFFFUL; /* PRQA S 0310 */ /* accept violating MISRA Rule 11.4 to allow invalidation of the function pointer */ /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because access to unallocated address space requires usage of pointers */

  /* Now normal operation of the ECU (Appl + XCP instrumentation) is stopped: tell the tool how to further communicate: */
  gVX1000.MagicId = (VX1000_UINT32)(VX1000_FKL_SUPPORT_ADDR);
  gVX1000.ToolDetectState |= (VX1000_UINT32)(VX1000_TDS_FKL_FORCED_IDLE);
  /* Once the tool accepted the new communication area, no longer access gVX1000, because that location is likely to be */
  /* reused for storing the flash kernel code, so reads would return invalid data and writes could destroy the kernel.  */

  while (1) /* intentional infinite loop (can be left by pseudo call to FKL or by watchdog reset) */
  {
    /* The code inside this loop must not use stack (access non-register-ed local variables or call non-FKL functions) */
    /* (if that can't be ensured, we'd have to reload the SP with (VX1000_FKL_SUPPORT_ADDR-16): HW-SPECIFIC ASSEMBLY!) */
    if ((VX1000_FKL_WORKSPACE)->TransitionTimeout != 0) /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because access to unallocated address space requires usage of pointers */
    {
      (VX1000_FKL_WORKSPACE)->TransitionTimeout--; /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because access to unallocated address space requires usage of pointers */
      VX1000_SERVE_WATCHDOG(&((VX1000_FKL_WORKSPACE)->WdgData1))
    }
    else
    {
      switch ((((VX1000_UINT32)((VX1000_FKL_WORKSPACE)->DeprotectState) << 8U) << 8U) | (VX1000_FKL_WORKSPACE)->DeprotectTrigger) /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because access to unallocated address space requires usage of pointers */
      {
      case ((VX1000_UINT32)(VX1000_FKL_STATE1CODE) << 16) | (VX1000_FKL_TOSTATE1CODE):
        /* still in the initialisation phase */
        break;
      case ((VX1000_UINT32)(VX1000_FKL_STATE1CODE) << 16) | (VX1000_FKL_TOSTATE2CODE):
        (VX1000_FKL_WORKSPACE)->TransitionTimeout = (VX1000_UINT32)(VX1000_FKL_TRANSITION_TIMEOUT); /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because access to unallocated address space requires usage of pointers */
        (VX1000_FKL_WORKSPACE)->DeprotectState = VX1000_FKL_STATE2CODE; /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because access to unallocated address space requires usage of pointers */
        break;
      case ((VX1000_UINT32)(VX1000_FKL_STATE2CODE) << 16) | (VX1000_FKL_TOSTATE3CODE):
        (VX1000_FKL_WORKSPACE)->TransitionTimeout = (VX1000_UINT32)(VX1000_FKL_TRANSITION_TIMEOUT); /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because access to unallocated address space requires usage of pointers */
        (VX1000_FKL_WORKSPACE)->DeprotectState = VX1000_FKL_STATE3CODE; /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because access to unallocated address space requires usage of pointers */
        break;
      case ((VX1000_UINT32)(VX1000_FKL_STATE3CODE) << 16) | (VX1000_FKL_TOSTATE4CODE):
        (VX1000_FKL_WORKSPACE)->TransitionTimeout = (VX1000_UINT32)(VX1000_FKL_TRANSITION_TIMEOUT); /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because access to unallocated address space requires usage of pointers */
        (VX1000_FKL_WORKSPACE)->DeprotectState = VX1000_FKL_STATE4CODE; /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because access to unallocated address space requires usage of pointers */
        break;
      case ((VX1000_UINT32)(VX1000_FKL_STATE4CODE) << 16) | (VX1000_FKL_TOSTATE5CODE):
        (VX1000_FKL_WORKSPACE)->TransitionTimeout = (VX1000_UINT32)(VX1000_FKL_TRANSITION_TIMEOUT); /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because access to unallocated address space requires usage of pointers */
        (VX1000_FKL_WORKSPACE)->DeprotectState = VX1000_FKL_STATE5CODE; /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because access to unallocated address space requires usage of pointers */
        break;
      case ((VX1000_UINT32)(VX1000_FKL_STATE5CODE) << 16) | (VX1000_FKL_LAUNCHCODE):
        (VX1000_FKL_WORKSPACE)->TransitionTimeout = 1UL; /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because access to unallocated address space requires usage of pointers */
        if (*(VX1000_UINT32*)&((VX1000_FKL_WORKSPACE)->EntryPoint) != 0xFFFFFFFFUL)  /* PRQA S 0310 */ /* accept violating MISRA Rule 11.4 to allow checking for invalidated function pointer */ /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because access to unallocated address space requires usage of pointers */
        {
          ((VX1000_FKL_WORKSPACE)->EntryPoint)((VX1000_FKL_WORKSPACE)->FklParam);  /* call the FKL: will not return here */ /* PRQA S 0306 */ /* cannot avoid violating MISRA Rule 11.3 because access to unallocated address space requires usage of pointers */
        }
        break;
      default:
        /* bad trigger sequence detected */
        while (1) /* intentional infinite loop without watchdog serving, will lead to watchdog reset */
        {
          /* intentionally left empty */
        }
        /*break; /-* unreachable code, intentionally here to please MISRA checkers */
      }
    }
  }
}
#endif /* VX1000_FKL_SUPPORT_ADDR  && !VX1000_COMPILED_FOR_SLAVECORES */


#if !defined(VX1000_COMPILED_FOR_SLAVECORES) /* global data have to exist only once (in shared memory) */

/* The following is VX1000_DATA, but split to allow separate linkage of gVX1000 at a userdefined address */
VX1000_STIM_BENCHMARK_DATA
VX1000_ECUID_DATA
VX1000_OLDA_BENCHMARK_DATA
VX1000_COLDSTART_BENCHMARK_DATA
VX1000_HOOK_CONTROL_DATA

/* include userdefined lines with optional section pragmas to force individual linkage of VX1000 structure data. */
#define VX1000_BEGSECT_VXSTRUCT_C
#include "vx1000_tc2xx_cfg.h" /* PRQA S 5087 */ /* willingly violating MISRA Rule 19.1 because the section pragmas are needed exactly here (and not at the start of the file) */
#define VX1000_BEGSECT_VXSTRUCT_C_UNDO

VX1000_STRUCT_DATA

/* include userdefined lines with optional section pragmas to restore previous linkage of data: */
#define VX1000_ENDSECT_VXSTRUCT_C
#include "vx1000_tc2xx_cfg.h" /* PRQA S 5087 */ /* willingly violating MISRA Rule 19.1 because the section pragmas are needed exactly here (and not at the start of the file) */
#define VX1000_ENDSECT_VXSTRUCT_C_UNDO

#endif /* !VX1000_COMPILED_FOR_SLAVECORES */

/* Note that we were using nested section switches here (which might be unsupported by some compilers but has the    */
/* advantage of not requiring #undefs (those would violate the MISRA coding guidelines) nor VX1000_xxxSECT_2         */
/* defines (those would spoil the yyy_cfg.h file). This means that we SHOULDN'T PLACE ANY FURTHER CODE OR DATA       */
/* below (with the exception of external user code/data that explicitely shall NOT go into VX sections).             */

/* include userdefined lines with optional section pragmas to restore standard linkage of code and/or data: */
#define VX1000_ENDSECT_VXMODULE_C
#include "vx1000_tc2xx_cfg.h" /* PRQA S 5087 */ /* willingly violating MISRA Rule 19.1 because the section pragmas are needed exactly here (and not at the start of the file) */
#define VX1000_ENDSECT_VXMODULE_C_UNDO

#if !defined(VX1000_COMPILED_FOR_SLAVECORES) /* global data have to exist only once (in shared memory) */


/* include userdefined lines with optional section pragmas to force individual linkage of EMEM header data. */
#define VX1000_BEGSECT_EMEM_HDR_C
#include "vx1000_tc2xx_cfg.h" /* PRQA S 5087 */ /* willingly violating MISRA Rule 19.1 because the section pragmas are needed exactly here (and not at the start of the file) */
#define VX1000_BEGSECT_EMEM_HDR_C_UNDO

VX1000_EMEM_HDR_DATA

/* include userdefined lines with optional section pragmas to force individual linkage of EMEM header data. */
#define VX1000_ENDSECT_EMEM_HDR_C
#include "vx1000_tc2xx_cfg.h" /* PRQA S 5087 */ /* willingly violating MISRA Rule 19.1 because the section pragmas are needed exactly here (and not at the start of the file) */
#define VX1000_ENDSECT_EMEM_HDR_C_UNDO

#endif /* !VX1000_COMPILED_FOR_SLAVECORES */

#endif /* !VX1000_DISABLE_INSTRUMENTATION */

