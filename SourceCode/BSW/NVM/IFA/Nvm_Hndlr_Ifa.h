/**
 * @defgroup Nvm_Hndlr_Ifa Nvm_Hndlr_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Nvm_Hndlr_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef NVM_HNDLR_IFA_H_
#define NVM_HNDLR_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Nvm_Hndlr_Read_Nvm_HndlrEcuModeSts(data) do \
{ \
    *data = Nvm_HndlrEcuModeSts; \
}while(0);

#define Nvm_Hndlr_Read_Nvm_HndlrFuncInhibitNvmSts(data) do \
{ \
    *data = Nvm_HndlrFuncInhibitNvmSts; \
}while(0);


/* Set Output DE MAcro Function */
#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KPdtOffsEolReadVal(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.KPdtOffsEolReadVal = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KPdfOffsEolReadVal(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.KPdfOffsEolReadVal = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KPdtOffsDrvgReadVal(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.KPdtOffsDrvgReadVal = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KPdfOffsDrvgReadVal(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.KPdfOffsDrvgReadVal = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KPedlSimPOffsEolReadVal(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.KPedlSimPOffsEolReadVal = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KPedlSimPOffsDrvgReadVal(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.KPedlSimPOffsDrvgReadVal = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KPistPOffsEolReadVal(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.KPistPOffsEolReadVal = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KPistPOffsDrvgReadVal(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.KPistPOffsDrvgReadVal = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KPrimCircPOffsEolReadVal(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.KPrimCircPOffsEolReadVal = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KPrimCircPOffsDrvgReadVal(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.KPrimCircPOffsDrvgReadVal = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KSecdCircPOffsEolReadVal(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.KSecdCircPOffsEolReadVal = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KSecdCircPOffsDrvgReadVal(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.KSecdCircPOffsDrvgReadVal = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KSteerEepOffs(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.KSteerEepOffs = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KYawEepOffs(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.KYawEepOffs = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KLatEepOffs(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.KLatEepOffs = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KFsYawEepOffs(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.KFsYawEepOffs = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KYawEepMax(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.KYawEepMax = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KYawEepMin(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.KYawEepMin = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KSteerEepMax(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.KSteerEepMax = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KSteerEepMin(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.KSteerEepMin = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KLatEepMax(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.KLatEepMax = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KLatEepMin(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.KLatEepMin = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KYawStillEepMax(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.KYawStillEepMax = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KYawStillEepMin(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.KYawStillEepMin = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KYawStandEepOffs(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.KYawStandEepOffs = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap(data) \
{ \
    for(i=0;i<31;i++) Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[i] = *data[i]; \
}
#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KAdpvVehMdlVchEep(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.KAdpvVehMdlVchEep = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KAdpvVehMdlVcrtRatEep(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.KAdpvVehMdlVcrtRatEep = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KFlBtcTmpEep(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.KFlBtcTmpEep = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KFrBtcTmpEep(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.KFrBtcTmpEep = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KRlBtcTmpEep(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.KRlBtcTmpEep = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KRrBtcTmpEep(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.KRrBtcTmpEep = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KEeBtcsDataEep(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.KEeBtcsDataEep = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KLgtSnsrEolEepOffs(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.KLgtSnsrEolEepOffs = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_KLgtSnsrDrvgEepOffs(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.KLgtSnsrDrvgEepOffs = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_ReadInvld(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.ReadInvld = *data; \
}while(0);

#define Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo_WrReqCmpld(data) do \
{ \
    Nvm_HndlrLogicEepDataInfo.WrReqCmpld = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* NVM_HNDLR_IFA_H_ */
/** @} */
