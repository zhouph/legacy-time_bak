#include "unity.h"
#include "unity_fixture.h"
#include "Nvm_Hndlr.h"
#include "Nvm_Hndlr_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Mom_HndlrEcuModeSts_t UtInput_Nvm_HndlrEcuModeSts[MAX_STEP] = NVM_HNDLRECUMODESTS;
const Eem_SuspcDetnFuncInhibitNvmSts_t UtInput_Nvm_HndlrFuncInhibitNvmSts[MAX_STEP] = NVM_HNDLRFUNCINHIBITNVMSTS;

const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KPdtOffsEolReadVal[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KPDTOFFSEOLREADVAL;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KPdfOffsEolReadVal[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KPDFOFFSEOLREADVAL;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KPdtOffsDrvgReadVal[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KPDTOFFSDRVGREADVAL;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KPdfOffsDrvgReadVal[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KPDFOFFSDRVGREADVAL;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KPedlSimPOffsEolReadVal[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KPEDLSIMPOFFSEOLREADVAL;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KPedlSimPOffsDrvgReadVal[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KPEDLSIMPOFFSDRVGREADVAL;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KPistPOffsEolReadVal[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KPISTPOFFSEOLREADVAL;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KPistPOffsDrvgReadVal[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KPISTPOFFSDRVGREADVAL;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KPrimCircPOffsEolReadVal[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KPRIMCIRCPOFFSEOLREADVAL;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KPrimCircPOffsDrvgReadVal[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KPRIMCIRCPOFFSDRVGREADVAL;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KSecdCircPOffsEolReadVal[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KSECDCIRCPOFFSEOLREADVAL;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KSecdCircPOffsDrvgReadVal[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KSECDCIRCPOFFSDRVGREADVAL;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KSteerEepOffs[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KSTEEREEPOFFS;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawEepOffs[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWEEPOFFS;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KLatEepOffs[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KLATEEPOFFS;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KFsYawEepOffs[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KFSYAWEEPOFFS;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawEepMax[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWEEPMAX;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawEepMin[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWEEPMIN;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KSteerEepMax[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KSTEEREEPMAX;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KSteerEepMin[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KSTEEREEPMIN;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KLatEepMax[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KLATEEPMAX;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KLatEepMin[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KLATEEPMIN;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawStillEepMax[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWSTILLEEPMAX;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawStillEepMin[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWSTILLEEPMIN;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawStandEepOffs[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWSTANDEEPOFFS;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_0[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWTMPEEPMAP_0;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_1[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWTMPEEPMAP_1;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_2[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWTMPEEPMAP_2;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_3[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWTMPEEPMAP_3;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_4[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWTMPEEPMAP_4;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_5[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWTMPEEPMAP_5;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_6[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWTMPEEPMAP_6;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_7[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWTMPEEPMAP_7;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_8[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWTMPEEPMAP_8;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_9[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWTMPEEPMAP_9;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_10[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWTMPEEPMAP_10;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_11[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWTMPEEPMAP_11;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_12[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWTMPEEPMAP_12;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_13[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWTMPEEPMAP_13;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_14[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWTMPEEPMAP_14;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_15[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWTMPEEPMAP_15;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_16[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWTMPEEPMAP_16;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_17[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWTMPEEPMAP_17;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_18[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWTMPEEPMAP_18;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_19[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWTMPEEPMAP_19;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_20[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWTMPEEPMAP_20;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_21[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWTMPEEPMAP_21;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_22[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWTMPEEPMAP_22;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_23[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWTMPEEPMAP_23;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_24[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWTMPEEPMAP_24;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_25[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWTMPEEPMAP_25;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_26[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWTMPEEPMAP_26;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_27[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWTMPEEPMAP_27;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_28[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWTMPEEPMAP_28;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_29[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWTMPEEPMAP_29;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_30[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KYAWTMPEEPMAP_30;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KAdpvVehMdlVchEep[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KADPVVEHMDLVCHEEP;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KAdpvVehMdlVcrtRatEep[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KADPVVEHMDLVCRTRATEEP;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KFlBtcTmpEep[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KFLBTCTMPEEP;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KFrBtcTmpEep[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KFRBTCTMPEEP;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KRlBtcTmpEep[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KRLBTCTMPEEP;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KRrBtcTmpEep[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KRRBTCTMPEEP;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KEeBtcsDataEep[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KEEBTCSDATAEEP;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KLgtSnsrEolEepOffs[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KLGTSNSREOLEEPOFFS;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_KLgtSnsrDrvgEepOffs[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_KLGTSNSRDRVGEEPOFFS;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_ReadInvld[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_READINVLD;
const Salsint32 UtExpected_Nvm_HndlrLogicEepDataInfo_WrReqCmpld[MAX_STEP] = NVM_HNDLRLOGICEEPDATAINFO_WRREQCMPLD;



TEST_GROUP(Nvm_Hndlr);
TEST_SETUP(Nvm_Hndlr)
{
    Nvm_Hndlr_Init();
}

TEST_TEAR_DOWN(Nvm_Hndlr)
{   /* Postcondition */

}

TEST(Nvm_Hndlr, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Nvm_HndlrEcuModeSts = UtInput_Nvm_HndlrEcuModeSts[i];
        Nvm_HndlrFuncInhibitNvmSts = UtInput_Nvm_HndlrFuncInhibitNvmSts[i];

        Nvm_Hndlr();

        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KPdtOffsEolReadVal, UtExpected_Nvm_HndlrLogicEepDataInfo_KPdtOffsEolReadVal[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KPdfOffsEolReadVal, UtExpected_Nvm_HndlrLogicEepDataInfo_KPdfOffsEolReadVal[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KPdtOffsDrvgReadVal, UtExpected_Nvm_HndlrLogicEepDataInfo_KPdtOffsDrvgReadVal[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KPdfOffsDrvgReadVal, UtExpected_Nvm_HndlrLogicEepDataInfo_KPdfOffsDrvgReadVal[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KPedlSimPOffsEolReadVal, UtExpected_Nvm_HndlrLogicEepDataInfo_KPedlSimPOffsEolReadVal[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KPedlSimPOffsDrvgReadVal, UtExpected_Nvm_HndlrLogicEepDataInfo_KPedlSimPOffsDrvgReadVal[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KPistPOffsEolReadVal, UtExpected_Nvm_HndlrLogicEepDataInfo_KPistPOffsEolReadVal[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KPistPOffsDrvgReadVal, UtExpected_Nvm_HndlrLogicEepDataInfo_KPistPOffsDrvgReadVal[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KPrimCircPOffsEolReadVal, UtExpected_Nvm_HndlrLogicEepDataInfo_KPrimCircPOffsEolReadVal[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KPrimCircPOffsDrvgReadVal, UtExpected_Nvm_HndlrLogicEepDataInfo_KPrimCircPOffsDrvgReadVal[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KSecdCircPOffsEolReadVal, UtExpected_Nvm_HndlrLogicEepDataInfo_KSecdCircPOffsEolReadVal[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KSecdCircPOffsDrvgReadVal, UtExpected_Nvm_HndlrLogicEepDataInfo_KSecdCircPOffsDrvgReadVal[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KSteerEepOffs, UtExpected_Nvm_HndlrLogicEepDataInfo_KSteerEepOffs[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawEepOffs, UtExpected_Nvm_HndlrLogicEepDataInfo_KYawEepOffs[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KLatEepOffs, UtExpected_Nvm_HndlrLogicEepDataInfo_KLatEepOffs[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KFsYawEepOffs, UtExpected_Nvm_HndlrLogicEepDataInfo_KFsYawEepOffs[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawEepMax, UtExpected_Nvm_HndlrLogicEepDataInfo_KYawEepMax[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawEepMin, UtExpected_Nvm_HndlrLogicEepDataInfo_KYawEepMin[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KSteerEepMax, UtExpected_Nvm_HndlrLogicEepDataInfo_KSteerEepMax[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KSteerEepMin, UtExpected_Nvm_HndlrLogicEepDataInfo_KSteerEepMin[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KLatEepMax, UtExpected_Nvm_HndlrLogicEepDataInfo_KLatEepMax[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KLatEepMin, UtExpected_Nvm_HndlrLogicEepDataInfo_KLatEepMin[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawStillEepMax, UtExpected_Nvm_HndlrLogicEepDataInfo_KYawStillEepMax[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawStillEepMin, UtExpected_Nvm_HndlrLogicEepDataInfo_KYawStillEepMin[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawStandEepOffs, UtExpected_Nvm_HndlrLogicEepDataInfo_KYawStandEepOffs[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[0], UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_0[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[1], UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_1[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[2], UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_2[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[3], UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_3[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[4], UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_4[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[5], UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_5[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[6], UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_6[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[7], UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_7[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[8], UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_8[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[9], UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_9[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[10], UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_10[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[11], UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_11[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[12], UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_12[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[13], UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_13[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[14], UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_14[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[15], UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_15[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[16], UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_16[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[17], UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_17[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[18], UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_18[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[19], UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_19[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[20], UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_20[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[21], UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_21[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[22], UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_22[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[23], UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_23[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[24], UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_24[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[25], UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_25[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[26], UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_26[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[27], UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_27[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[28], UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_28[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[29], UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_29[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[30], UtExpected_Nvm_HndlrLogicEepDataInfo_KYawTmpEepMap_30[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KAdpvVehMdlVchEep, UtExpected_Nvm_HndlrLogicEepDataInfo_KAdpvVehMdlVchEep[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KAdpvVehMdlVcrtRatEep, UtExpected_Nvm_HndlrLogicEepDataInfo_KAdpvVehMdlVcrtRatEep[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KFlBtcTmpEep, UtExpected_Nvm_HndlrLogicEepDataInfo_KFlBtcTmpEep[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KFrBtcTmpEep, UtExpected_Nvm_HndlrLogicEepDataInfo_KFrBtcTmpEep[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KRlBtcTmpEep, UtExpected_Nvm_HndlrLogicEepDataInfo_KRlBtcTmpEep[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KRrBtcTmpEep, UtExpected_Nvm_HndlrLogicEepDataInfo_KRrBtcTmpEep[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KEeBtcsDataEep, UtExpected_Nvm_HndlrLogicEepDataInfo_KEeBtcsDataEep[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KLgtSnsrEolEepOffs, UtExpected_Nvm_HndlrLogicEepDataInfo_KLgtSnsrEolEepOffs[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.KLgtSnsrDrvgEepOffs, UtExpected_Nvm_HndlrLogicEepDataInfo_KLgtSnsrDrvgEepOffs[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.ReadInvld, UtExpected_Nvm_HndlrLogicEepDataInfo_ReadInvld[i]);
        TEST_ASSERT_EQUAL(Nvm_HndlrLogicEepDataInfo.WrReqCmpld, UtExpected_Nvm_HndlrLogicEepDataInfo_WrReqCmpld[i]);
    }
}

TEST_GROUP_RUNNER(Nvm_Hndlr)
{
    RUN_TEST_CASE(Nvm_Hndlr, All);
}
