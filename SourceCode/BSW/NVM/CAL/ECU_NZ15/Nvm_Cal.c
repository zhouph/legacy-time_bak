/**
 * @defgroup Nvm_Cal Nvm_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Nvm_Cal.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Nvm_Cal.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define NVM_START_SEC_CALIB_UNSPECIFIED
#include "Nvm_MemMap.h"
/* Global Calibration Section */


#define NVM_STOP_SEC_CALIB_UNSPECIFIED
#include "Nvm_MemMap.h"

#define NVM_START_SEC_CONST_UNSPECIFIED
#include "Nvm_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define NVM_STOP_SEC_CONST_UNSPECIFIED
#include "Nvm_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define NVM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Nvm_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define NVM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Nvm_MemMap.h"
#define NVM_START_SEC_VAR_NOINIT_32BIT
#include "Nvm_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define NVM_STOP_SEC_VAR_NOINIT_32BIT
#include "Nvm_MemMap.h"
#define NVM_START_SEC_VAR_UNSPECIFIED
#include "Nvm_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define NVM_STOP_SEC_VAR_UNSPECIFIED
#include "Nvm_MemMap.h"
#define NVM_START_SEC_VAR_32BIT
#include "Nvm_MemMap.h"
/** Variable Section (32BIT)**/


#define NVM_STOP_SEC_VAR_32BIT
#include "Nvm_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define NVM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Nvm_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define NVM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Nvm_MemMap.h"
#define NVM_START_SEC_VAR_NOINIT_32BIT
#include "Nvm_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define NVM_STOP_SEC_VAR_NOINIT_32BIT
#include "Nvm_MemMap.h"
#define NVM_START_SEC_VAR_UNSPECIFIED
#include "Nvm_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define NVM_STOP_SEC_VAR_UNSPECIFIED
#include "Nvm_MemMap.h"
#define NVM_START_SEC_VAR_32BIT
#include "Nvm_MemMap.h"
/** Variable Section (32BIT)**/


#define NVM_STOP_SEC_VAR_32BIT
#include "Nvm_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define NVM_START_SEC_CODE
#include "Nvm_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define NVM_STOP_SEC_CODE
#include "Nvm_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
