/**
 * @defgroup Nvm_Hndlr Nvm_Hndlr
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Nvm_Hndlr.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Nvm_Hndlr.h"
#include "NvmIf.h"
#include "Nvm_Hndlr_Ifa.h"
#include "IfxStm_reg.h"
#include "MemIf_Types.h"
#include "Fee.h"
#include "Crc.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define MEM_NO_JOB_U8               0x00U    /* no job pending */
#define MEM_STATUS_WRITE_U8         0xF0U    /* write operation */
#define MEM_STATUS_READ_U8          0x0FU    /* read operation */
//#define MEM_ALL_BLOCKS_U8           0xFFU    /* ask for all blocks */

#define MEM_DATA_READ_U8            0x01U    /* read data */
#define MEM_READ_BUSY_U8            0x02U    /* read operation being carried out */
#define MEM_READ_READY_U8           0x03U    /* read operation done */
#define MEM_READ_CS_ERROR_U8        0x04U    /* read error */
#define MEM_READ_FEE_ERROR_U8       0x05U    /* FEE read error */
#define MEM_DATA_WRITE_U8           0x10U    /* write data */
#define MEM_WRITE_BUSY_U8           0x20U    /* write operation being carried out */
#define MEM_WRITE_READY_U8          0x30U    /* write operation done */
#define MEM_WRITE_CS_ERROR_U8       0x40U    /* write error */
#define MEM_WRITE_FEE_ERROR_U8      0x50U    /* FEE write error */

#define MEM_FEE_TIMEOUT_U8          0x0AU   /* number of failed fee accesses */
#define MEM_MAX_TRY_WRITE_U8        0x03U   /* maximum number of write attempts */

#define MEM_DATA_MATCH_U2           0x00U
#define MEM_DATA_MISMATCH_U2        0x01U

#define MEM_DATA_COPY_U2            0x00U
#define MEM_DATA_COMPARE_U2         0x01U

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef enum
{
    MEM_JOB_NO,              /* no jobs to process */
    MEM_UNINIT,              /* not initialized */
    MEM_VOLTAGE_OOR,         /* voltage out of range, wait for voltage OK */
    MEM_JOB_PENDING,         /* a job is being processed */
    MEM_JOB_BUSY,            /* job not (yet) accepted */
    MEM_JOB_ERROR,           /* error while processing a job */
    MEM_JOB_START_VERIFY,    /* start verification */
    MEM_JOB_PENDING_VERIFY,  /* read back pending */
    MEM_JOB_BUSY_VERIFY      /* actual verification */
    //MEM_PAUSED_BY_DIAG     /* diag module access fee - memory is paused */
} MemStates_t;

typedef enum
{
    MEM_NO_NOTIFY,
    MEM_END_NOTIFY,
    MEM_ERROR_NOTIFY
}MemNotifyStat_t;

typedef enum
{
    MEM_OFFSET_OFF,
    MEM_OFFSET_ON
}MemOffsetFlag_t;

typedef enum
{
    MEM_ASYNC_ACCESS_OFF,
    MEM_ASYNC_ACCESS_ON
}MemAccessFlag_t;

typedef enum
{
    MEM_BLOCK_OK,
    MEM_BLOCK_ERROR,
    MEM_BLOCK_FAILED,
    MEM_BLOCK_CANCELED,
    MEM_BLOCK_PENDING,
    MEM_BLOCK_INCONSISTENT,
    MEM_BLOCK_INVALID
}MemJobResult_t;

typedef struct
{
    uint8 InternalFlag_u2:2;                        /* finite-state machine status */
    uint8 CopyRequest_u2:2;                         /* copy data for reading to RAM */
    uint8 CompareResult_u2:2;                       /* compare data from RAM and data from ROM */
    MemJobResult_t LastResult_u8;                           /* data read & write result */
    MemAccessFlag_t SyncAccessFlag_u8;
    MemNotifyStat_t NotifyFlag_u8;
    uint16 MemHiPrioJob_u16;
}MemCurrentBlockInfo_t;

typedef struct
{
    MemOffsetFlag_t BlockOffset_en;
    uint16 BlockOffset_u16;
    uint16 BlockSize_u16;
}MemReadDataOffset_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define NVM_HNDLR_START_SEC_CONST_UNSPECIFIED
#include "Nvm_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define NVM_HNDLR_STOP_SEC_CONST_UNSPECIFIED
#include "Nvm_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define NVM_HNDLR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Nvm_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Nvm_Hndlr_HdrBusType Nvm_HndlrBus;

/* Version Info */
const SwcVersionInfo_t Nvm_HndlrVersionInfo = 
{   
    NVM_HNDLR_MODULE_ID,           /* Nvm_HndlrVersionInfo.ModuleId */
    NVM_HNDLR_MAJOR_VERSION,       /* Nvm_HndlrVersionInfo.MajorVer */
    NVM_HNDLR_MINOR_VERSION,       /* Nvm_HndlrVersionInfo.MinorVer */
    NVM_HNDLR_PATCH_VERSION,       /* Nvm_HndlrVersionInfo.PatchVer */
    NVM_HNDLR_BRANCH_VERSION       /* Nvm_HndlrVersionInfo.BranchVer */
};
    
/* Input Data Element */
Mom_HndlrEcuModeSts_t Nvm_HndlrEcuModeSts;
Eem_SuspcDetnFuncInhibitNvmSts_t Nvm_HndlrFuncInhibitNvmSts;

/* Output Data Element */
Nvm_HndlrLogicEepDataInfo_t Nvm_HndlrLogicEepDataInfo;

uint32 Nvm_Hndlr_Timer_Start;
uint32 Nvm_Hndlr_Timer_Elapsed;

#define NVM_HNDLR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Nvm_MemMap.h"
#define NVM_HNDLR_START_SEC_VAR_NOINIT_32BIT
#include "Nvm_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define NVM_HNDLR_STOP_SEC_VAR_NOINIT_32BIT
#include "Nvm_MemMap.h"
#define NVM_HNDLR_START_SEC_VAR_UNSPECIFIED
#include "Nvm_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define NVM_HNDLR_STOP_SEC_VAR_UNSPECIFIED
#include "Nvm_MemMap.h"
#define NVM_HNDLR_START_SEC_VAR_32BIT
#include "Nvm_MemMap.h"
/** Variable Section (32BIT)**/


#define NVM_HNDLR_STOP_SEC_VAR_32BIT
#include "Nvm_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define NVM_HNDLR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Nvm_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define NVM_HNDLR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Nvm_MemMap.h"
#define NVM_HNDLR_START_SEC_VAR_NOINIT_32BIT
#include "Nvm_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define NVM_HNDLR_STOP_SEC_VAR_NOINIT_32BIT
#include "Nvm_MemMap.h"
#define NVM_HNDLR_START_SEC_VAR_UNSPECIFIED
#include "Nvm_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define NVM_HNDLR_STOP_SEC_VAR_UNSPECIFIED
#include "Nvm_MemMap.h"
#define NVM_HNDLR_START_SEC_VAR_32BIT
#include "Nvm_MemMap.h"
/** Variable Section (32BIT)**/

static Std_ReturnType resultflag;
static MemHWError_t MemHWError_en = MEM_NO_HW_ERROR;
static MemStates_t MemState_en = MEM_UNINIT;  /* module states */

static uint8 MemJobList_u8[MEM_NUM_OF_BLOCKS];          /* jobs list */
static uint8 WriteDataBuffer[FEE_BUF_SIZE];
static uint8 ReadDataBuffer[FEE_BUF_SIZE];
static uint8 MemJobCount_u8 = 0U;                       /* current job number */
static uint16 VerifyCounter_u16 = 0U;      /* counter for verified bytes */

static MemCurrentBlockInfo_t NvM_CurrentBlockInfo;
static MemReadDataOffset_t NvM_ReadDataOffset[MEM_NUM_OF_BLOCKS];
static uint8 NvM_DataMisMatchSet[MEM_NUM_OF_BLOCKS];
static uint8 MemErrorCounter_u8 = 0U;      /* memory error counter to detect defective DFLASH */
static uint8 WriteError_u8 = 0U;           /* memory write error counter */
#define NVM_HNDLR_STOP_SEC_VAR_32BIT
#include "Nvm_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define NVM_HNDLR_START_SEC_CODE
#include "Nvm_MemMap.h"

//static void NvMCdd_Init(void);
static void NvM_InitDefaults(void);
static uint8 Mem_ReadAllData(void);
static uint8 Mem_WriteAllData(void);
static void NvMCdd_MainFunction(void);
static uint8 Mem_SetHiPrioJob(uint16 MemBlockId_u16);
static void Mem_WriteDataBlock(uint16 MemBlockId_u16, uint8 * SrcBufPtr);
static void Mem_ReadDataBlock(uint16 MemBlockId_u16, uint8 * DestBufPtr);
static void NvM_MainFSM(void);
static void NvM_SubFSM(void);
static Std_ReturnType NvMCdd_WriteBlock( uint16 MemBlockId_u16, uint8* DestPtr);
static Std_ReturnType NvMCdd_ReadBlock(uint16 MemBlockId_u16, uint16 BlockOffset, uint8 *SrcPtr, uint16 Length);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Nvm_Hndlr_Init(void)
{
    uint16 i;

    /* Initialize internal bus */
    Nvm_HndlrBus.Nvm_HndlrEcuModeSts = 0;
    Nvm_HndlrBus.Nvm_HndlrFuncInhibitNvmSts = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KPdtOffsEolReadVal = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KPdfOffsEolReadVal = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KPdtOffsDrvgReadVal = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KPdfOffsDrvgReadVal = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KPedlSimPOffsEolReadVal = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KPedlSimPOffsDrvgReadVal = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KPistPOffsEolReadVal = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KPistPOffsDrvgReadVal = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KPrimCircPOffsEolReadVal = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KPrimCircPOffsDrvgReadVal = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KSecdCircPOffsEolReadVal = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KSecdCircPOffsDrvgReadVal = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KSteerEepOffs = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KYawEepOffs = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KLatEepOffs = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KFsYawEepOffs = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KYawEepMax = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KYawEepMin = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KSteerEepMax = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KSteerEepMin = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KLatEepMax = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KLatEepMin = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KYawStillEepMax = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KYawStillEepMin = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KYawStandEepOffs = 0;
    for(i=0;i<31;i++) Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[i] = 0;   
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KAdpvVehMdlVchEep = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KAdpvVehMdlVcrtRatEep = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KFlBtcTmpEep = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KFrBtcTmpEep = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KRlBtcTmpEep = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KRrBtcTmpEep = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KEeBtcsDataEep = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KLgtSnsrEolEepOffs = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KLgtSnsrDrvgEepOffs = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.ReadInvld = 0;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.WrReqCmpld = 0;
}

void Nvm_Hndlr(void)
{
    uint16 i;
    
    Nvm_Hndlr_Timer_Start = STM0_TIM0.U;

    /* Input */
    Nvm_Hndlr_Read_Nvm_HndlrEcuModeSts(&Nvm_HndlrBus.Nvm_HndlrEcuModeSts);
    Nvm_Hndlr_Read_Nvm_HndlrFuncInhibitNvmSts(&Nvm_HndlrBus.Nvm_HndlrFuncInhibitNvmSts);

    /* Process */
    NvMCdd_MainFunction();
    NvMIf_UpdateInternalBusData();

    /* Output */
    Nvm_Hndlr_Write_Nvm_HndlrLogicEepDataInfo(&Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo);
    /*==============================================================================
    * Members of structure Nvm_HndlrLogicEepDataInfo 
     : Nvm_HndlrLogicEepDataInfo.KPdtOffsEolReadVal;
     : Nvm_HndlrLogicEepDataInfo.KPdfOffsEolReadVal;
     : Nvm_HndlrLogicEepDataInfo.KPdtOffsDrvgReadVal;
     : Nvm_HndlrLogicEepDataInfo.KPdfOffsDrvgReadVal;
     : Nvm_HndlrLogicEepDataInfo.KPedlSimPOffsEolReadVal;
     : Nvm_HndlrLogicEepDataInfo.KPedlSimPOffsDrvgReadVal;
     : Nvm_HndlrLogicEepDataInfo.KPistPOffsEolReadVal;
     : Nvm_HndlrLogicEepDataInfo.KPistPOffsDrvgReadVal;
     : Nvm_HndlrLogicEepDataInfo.KPrimCircPOffsEolReadVal;
     : Nvm_HndlrLogicEepDataInfo.KPrimCircPOffsDrvgReadVal;
     : Nvm_HndlrLogicEepDataInfo.KSecdCircPOffsEolReadVal;
     : Nvm_HndlrLogicEepDataInfo.KSecdCircPOffsDrvgReadVal;
     : Nvm_HndlrLogicEepDataInfo.KSteerEepOffs;
     : Nvm_HndlrLogicEepDataInfo.KYawEepOffs;
     : Nvm_HndlrLogicEepDataInfo.KLatEepOffs;
     : Nvm_HndlrLogicEepDataInfo.KFsYawEepOffs;
     : Nvm_HndlrLogicEepDataInfo.KYawEepMax;
     : Nvm_HndlrLogicEepDataInfo.KYawEepMin;
     : Nvm_HndlrLogicEepDataInfo.KSteerEepMax;
     : Nvm_HndlrLogicEepDataInfo.KSteerEepMin;
     : Nvm_HndlrLogicEepDataInfo.KLatEepMax;
     : Nvm_HndlrLogicEepDataInfo.KLatEepMin;
     : Nvm_HndlrLogicEepDataInfo.KYawStillEepMax;
     : Nvm_HndlrLogicEepDataInfo.KYawStillEepMin;
     : Nvm_HndlrLogicEepDataInfo.KYawStandEepOffs;
     : Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap;
     : Nvm_HndlrLogicEepDataInfo.KAdpvVehMdlVchEep;
     : Nvm_HndlrLogicEepDataInfo.KAdpvVehMdlVcrtRatEep;
     : Nvm_HndlrLogicEepDataInfo.KFlBtcTmpEep;
     : Nvm_HndlrLogicEepDataInfo.KFrBtcTmpEep;
     : Nvm_HndlrLogicEepDataInfo.KRlBtcTmpEep;
     : Nvm_HndlrLogicEepDataInfo.KRrBtcTmpEep;
     : Nvm_HndlrLogicEepDataInfo.KEeBtcsDataEep;
     : Nvm_HndlrLogicEepDataInfo.KLgtSnsrEolEepOffs;
     : Nvm_HndlrLogicEepDataInfo.KLgtSnsrDrvgEepOffs;
     : Nvm_HndlrLogicEepDataInfo.ReadInvld;
     : Nvm_HndlrLogicEepDataInfo.WrReqCmpld;
     =============================================================================*/
    

    Nvm_Hndlr_Timer_Elapsed = STM0_TIM0.U - Nvm_Hndlr_Timer_Start;
}
void NvM_ReadAll(void)
{
   (void)Mem_ReadAllData();

}

void NvM_WriteAll(void)
{
    (void)Mem_WriteAllData();
}

uint8 NvM_WriteBlock( NvM_BlockIdType BlockId, const void* NvM_SrcPtr)
{
    Mem_WriteDataBlock(BlockId, NvM_SrcPtr);
    return 0;
}

uint8 NvM_ReadBlock(NvM_BlockIdType BlockId, void *SrcPtr)
{
     Mem_ReadDataBlock(BlockId, SrcPtr);
     return 0;
}

void NvM_CopyForWriteNoCS(void)
{
    /* initialize variables */
    uint16 Counter_u16 = 0;

    for (Counter_u16 = 0; Counter_u16 < MemConfigList[MemJobCount_u8].Length_u16; Counter_u16++)
    {
       /* copy data to write buffer */
       WriteDataBuffer[Counter_u16] = *(MemConfigList[MemJobCount_u8].RamAddr_u8 + Counter_u16);
       ReadDataBuffer[Counter_u16] = WriteDataBuffer[Counter_u16];
    }
}

uint8 NvM_CopyForReadNoCS(void)
{
    /* initialize variables */
    uint16 Counter_u16 = 0;

    if(MEM_DATA_COMPARE_U2 == NvM_CurrentBlockInfo.CopyRequest_u2)
    {
        /* initialization to the default values */
        NvM_CurrentBlockInfo.CopyRequest_u2 = MEM_DATA_COPY_U2;
        NvM_CurrentBlockInfo.CompareResult_u2 = MEM_DATA_MATCH_U2;

        for (Counter_u16 = 0; Counter_u16 < MemConfigList[MemJobCount_u8].Length_u16; Counter_u16++)
        {
             if(*(MemConfigList[MemJobCount_u8].RamAddr_u8 + Counter_u16) != ReadDataBuffer[Counter_u16])
             {
                 //NvM_CurrentBlockInfo.CompareResult_u2 = MEM_DATA_MISMATCH_U2;
                 NvM_DataMisMatchSet[MemJobCount_u8] = MEM_DATA_MISMATCH_U2;
                 return TRUE;
             }
        }
    }
    else
    {
        if(MEM_OFFSET_ON == NvM_ReadDataOffset[MemJobCount_u8].BlockOffset_en)
        {
            for (Counter_u16 = 0; Counter_u16 < NvM_ReadDataOffset[MemJobCount_u8].BlockSize_u16; Counter_u16++)
            {
                 *((MemConfigList[MemJobCount_u8].RamAddr_u8 + NvM_ReadDataOffset[MemJobCount_u8].BlockOffset_u16) +
                         Counter_u16) = ReadDataBuffer[Counter_u16];
            }

            NvM_ReadDataOffset[MemJobCount_u8].BlockOffset_en = MEM_OFFSET_OFF;
        }
        else
        {
            /* (MEM_DATA_COPY == NvM_CurrentBlockInfo.CopyRequest_u8) */
            for (Counter_u16 = 0; Counter_u16 < MemConfigList[MemJobCount_u8].Length_u16; Counter_u16++)
            {
                 *(MemConfigList[MemJobCount_u8].RamAddr_u8 + Counter_u16) = ReadDataBuffer[Counter_u16];
            }
        }
    }

    return TRUE;

}

void NvM_CopyForWrite(void)
{
    /* initialize variables */
    uint16 Counter_u16 = 0;
    uint16 CSBuffer_u16 = 0;    /* temporary variable for checksum calculation */

    for (Counter_u16 = 0; Counter_u16 < MemConfigList[MemJobCount_u8].Length_u16; Counter_u16++)
    {
       /* copy data to write buffer */
       WriteDataBuffer[Counter_u16] = *(MemConfigList[MemJobCount_u8].RamAddr_u8 + Counter_u16);
    }
    /* calculate the checksum */
    CSBuffer_u16 = Crc_CalculateCRC16(WriteDataBuffer, (uint32)MemConfigList[MemJobCount_u8].Length_u16, 0, TRUE);

    /* write checksum to the buffer */
    WriteDataBuffer[MemConfigList[MemJobCount_u8].Length_u16 + CRC16_H] = (uint8) (CSBuffer_u16 >> 8);
    WriteDataBuffer[MemConfigList[MemJobCount_u8].Length_u16 + CRC16_L] = (uint8) CSBuffer_u16;

    for (Counter_u16 = 0; Counter_u16 < (MemConfigList[MemJobCount_u8].Length_u16 + CRC16); Counter_u16++)
    {
       /* copy data to verify buffer */
        ReadDataBuffer[Counter_u16] = WriteDataBuffer[Counter_u16];
    }
}

uint8 NvM_CopyForRead (void)
{

  uint16 Counter_u16 = 0;            /* local counter */
  uint16 CSBuffer_u16 = 0;           /* temporary variable for checksum calculation */
  uint8 CheckSumValid_u8 = FALSE;    /* state (TRUE: CS ok)*/

  if(MEM_OFFSET_ON == NvM_ReadDataOffset[MemJobCount_u8].BlockOffset_en)
  {
        for (Counter_u16 = 0; Counter_u16 < NvM_ReadDataOffset[MemJobCount_u8].BlockSize_u16; Counter_u16++)
        {
             *((MemConfigList[MemJobCount_u8].RamAddr_u8 + NvM_ReadDataOffset[MemJobCount_u8].BlockOffset_u16) +
                     Counter_u16) = ReadDataBuffer[Counter_u16];
        }
        NvM_ReadDataOffset[MemJobCount_u8].BlockOffset_en = MEM_OFFSET_OFF;

        return TRUE;
  }

  /* calculate checksum */
  CSBuffer_u16 = Crc_CalculateCRC16(ReadDataBuffer, (uint32)(MemConfigList[MemJobCount_u8].Length_u16), 0, TRUE);

  /* compare checksums */
  if (CSBuffer_u16 == (((uint16)(ReadDataBuffer[MemConfigList[MemJobCount_u8].Length_u16 + CRC16_H] << 8)) +
        (ReadDataBuffer[MemConfigList[MemJobCount_u8].Length_u16 + CRC16_L])))
  {
       CheckSumValid_u8 = TRUE; /* checksum is ok */
  }

  if (TRUE == CheckSumValid_u8)
  {
       /* copy data to RAM */
       if(MEM_DATA_COMPARE_U2 == NvM_CurrentBlockInfo.CopyRequest_u2)
       {
           NvM_CurrentBlockInfo.CopyRequest_u2 = MEM_DATA_COPY_U2;
           NvM_CurrentBlockInfo.CompareResult_u2 = MEM_DATA_MATCH_U2;

           for (Counter_u16 = 0; Counter_u16 < MemConfigList[MemJobCount_u8].Length_u16; Counter_u16++)
           {
               if(*(MemConfigList[MemJobCount_u8].RamAddr_u8 + Counter_u16) != ReadDataBuffer[Counter_u16])
               {
                   NvM_DataMisMatchSet[MemJobCount_u8] = MEM_DATA_MISMATCH_U2;
                   //NvM_CurrentBlockInfo.CompareResult_u2 = MEM_DATA_MISMATCH_U2;
                   break;
               }
           }
       }
       else
       {
           /* (MEM_DATA_COPY == NvM_CurrentBlockInfo.CopyRequest_u8) */
           for (Counter_u16 = 0; Counter_u16 < MemConfigList[MemJobCount_u8].Length_u16; Counter_u16++)
           {
               *(MemConfigList[MemJobCount_u8].RamAddr_u8 + Counter_u16) = ReadDataBuffer[Counter_u16];
           }
       }
  }
  else
  {
        NvM_CurrentBlockInfo.CopyRequest_u2 = MEM_DATA_COPY_U2;
        //NvM_CurrentBlockInfo.CompareResult_u2 = MEM_DATA_MISMATCH_U2;
        NvM_DataMisMatchSet[MemJobCount_u8] = MEM_DATA_MISMATCH_U2;

        if (NULL_PTR != MemConfigList[MemJobCount_u8].MirrorRamAddr_u8)
        {
            // copy data to ram
            for (Counter_u16 = 0; Counter_u16 < MemConfigList[MemJobCount_u8].Length_u16; Counter_u16++)
            {
                /* checksum not ok, copy default values from ROM to user structure */
                *(MemConfigList[MemJobCount_u8].RamAddr_u8 + Counter_u16) =
                    *(MemConfigList[MemJobCount_u8].MirrorRamAddr_u8 + Counter_u16);
            }
        }

  }

  return CheckSumValid_u8;
}

void NvM_JobEndNotification (void)
{
    NvM_CurrentBlockInfo.NotifyFlag_u8 = MEM_END_NOTIFY;
}

void NvM_JobErrorNotification (void)
{
    NvM_CurrentBlockInfo.NotifyFlag_u8 = MEM_ERROR_NOTIFY;
    switch(Fee_GetJobResult())
    {
        case MEMIF_BLOCK_INCONSISTENT:
            NvM_CurrentBlockInfo.LastResult_u8 = MEM_BLOCK_INCONSISTENT;
            break;

        case MEMIF_BLOCK_INVALID:
            NvM_CurrentBlockInfo.LastResult_u8 = MEM_BLOCK_INVALID;
            break;

        case MEMIF_JOB_FAILED:
            NvM_CurrentBlockInfo.LastResult_u8 = MEM_BLOCK_FAILED;
            break;

        default:
            break;
    }
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
void NvMCdd_Init(void)
{
    MemStates_t State_en = MemState_en;   /* this is needed to guarantee consistency */
    MemIf_StatusType RetVal_en;

    if (((MEM_UNINIT == State_en) || (MEM_JOB_NO == State_en))
            && (MEM_NO_HW_ERROR == MemHWError_en))
    {
        Fee_Init(); //    initialize FEE

        while(MEMIF_IDLE != Fee_GetStatus())
        {
            Fee_MainFunction();
            Fls_17_Pmu_MainFunction();
        }

        RetVal_en = Fee_GetStatus();
        if(MEMIF_UNINIT == RetVal_en)
        {
            MemState_en = MEM_UNINIT;
        }
        else
        {
            NvM_InitDefaults();
            MemHWError_en = MEM_NO_HW_ERROR;
            MemState_en = MEM_JOB_NO;
        }
        
    }
    else
    {
        MemState_en = MEM_UNINIT;
    }
}

static void NvM_InitDefaults(void)
{
    uint16 Counter_u16 = 0U;

    NvM_CurrentBlockInfo.MemHiPrioJob_u16 = MEM_NUM_OF_BLOCKS;

    for (Counter_u16 = 0; Counter_u16 < MEM_NUM_OF_BLOCKS; Counter_u16++)
    {
        MemJobList_u8[Counter_u16] = MEM_NO_JOB_U8;
    }
}

static uint8 Mem_ReadAllData(void)
{

    uint16 Counter_u16 = 0;  /* counter for loop over all jobs */
    MemJobState_t retVal = MEM_OK_JOB;

    if(TRUE == NvM_CurrentBlockInfo.InternalFlag_u2)
    {
        retVal = MEM_BUSY_JOB;
    }
    else
    {
        /* initialize for reading all data blocks */
        NvM_CurrentBlockInfo.SyncAccessFlag_u8 = MEM_ASYNC_ACCESS_OFF;
        for (Counter_u16 = 0; Counter_u16 < MEM_NUM_OF_BLOCKS; Counter_u16++)
        {
            MemJobList_u8[Counter_u16] = (MemJobList_u8[Counter_u16] & MEM_STATUS_WRITE_U8) +
                    MEM_DATA_READ_U8;

            NvMCdd_MainFunction();

            if((uint8)MEM_READ_FEE_ERROR_U8 == (MemJobList_u8[Counter_u16] &
                    (uint8)MEM_STATUS_READ_U8))
            {
                retVal = MEM_ERROR_JOB;
            }
        }
        NvM_CurrentBlockInfo.SyncAccessFlag_u8 = MEM_ASYNC_ACCESS_ON;
    }

    return retVal;

}

static uint8 Mem_WriteAllData(void)
{

    MemJobState_t retVal = MEN_NO_JOB;
    uint16 Counter_u16 = 0;

    if(TRUE == NvM_CurrentBlockInfo.InternalFlag_u2)
    {
        retVal = MEM_BUSY_JOB;
    }
    else
    {
        NvM_CurrentBlockInfo.SyncAccessFlag_u8 = MEM_ASYNC_ACCESS_OFF;
        for (Counter_u16 = 0; Counter_u16 < MEM_NUM_OF_BLOCKS; Counter_u16++)
        {
            if( (((uint8)MEM_READ_CS_ERROR_U8 == (MemJobList_u8[Counter_u16] & (uint8)MEM_STATUS_READ_U8)) ||
              ((uint8)MEM_READ_FEE_ERROR_U8 == (MemJobList_u8[Counter_u16] & (uint8)MEM_STATUS_READ_U8))) ||
            //( ((uint8)MEM_WRITE_CS_ERROR_U8 == (MemJobList_u8[Counter_u16] & (uint8)MEM_STATUS_WRITE_U8)) ||
              ((uint8)MEM_WRITE_FEE_ERROR_U8 == (MemJobList_u8[Counter_u16] & (uint8)MEM_STATUS_WRITE_U8)) )
            {
                MemJobList_u8[Counter_u16] = (MemJobList_u8[Counter_u16] & MEM_STATUS_READ_U8) +
                        MEM_DATA_WRITE_U8;

                NvMCdd_MainFunction();
            }
            else
            {
                NvM_CurrentBlockInfo.CopyRequest_u2 = MEM_DATA_COMPARE_U2;
                MemJobList_u8[Counter_u16] = (MemJobList_u8[Counter_u16] & MEM_STATUS_WRITE_U8) +
                                    MEM_DATA_READ_U8;
                NvMCdd_MainFunction();
                NvM_CurrentBlockInfo.CopyRequest_u2 = MEM_DATA_COPY_U2;

            }//end of condition

        }//end of loop

        for (Counter_u16 = 0; Counter_u16 < MEM_NUM_OF_BLOCKS; Counter_u16++)
        {
            if(MEM_DATA_MISMATCH_U2 == NvM_DataMisMatchSet[Counter_u16])
            {
                MemJobList_u8[Counter_u16] = (MemJobList_u8[Counter_u16] & MEM_STATUS_READ_U8) +
                                            MEM_DATA_WRITE_U8;
            }
        }

        for (Counter_u16 = 0; Counter_u16 < MEM_NUM_OF_BLOCKS; Counter_u16++)
        {
            if((uint8)MEM_DATA_WRITE_U8 == (MemJobList_u8[Counter_u16] & (uint8)MEM_STATUS_WRITE_U8))
            {
                Mem_SetHiPrioJob(Counter_u16);
                NvMCdd_MainFunction();
            }
        }

        NvM_CurrentBlockInfo.SyncAccessFlag_u8 = MEM_ASYNC_ACCESS_ON;
        retVal = MEM_OK_JOB;
    }



}

static void NvMCdd_MainFunction(void)
{
    do{
        NvM_MainFSM();
        NvM_SubFSM();
    }while(NvM_CurrentBlockInfo.InternalFlag_u2 == TRUE);
}

static void NvM_MainFSM(void)
{
    uint16 Counter_u16 = 0U;                   /* general purpose counter (for loops etc.) */

    if (MEM_UNINIT == MemState_en)
    {
        /* call Error Handler function */
        //ErrHandler(Parameters);
        return;
    }

    switch(MemState_en)
    {
        case MEM_JOB_NO:
        {

            for(Counter_u16 = 0; Counter_u16 < MEM_NUM_OF_BLOCKS; Counter_u16++)
            {
                if(MEM_NUM_OF_BLOCKS != NvM_CurrentBlockInfo.MemHiPrioJob_u16)
                {
                    /* Do priority request*/
                    MemJobCount_u8 = (uint8)NvM_CurrentBlockInfo.MemHiPrioJob_u16;
                    NvM_CurrentBlockInfo.MemHiPrioJob_u16 = MEM_NUM_OF_BLOCKS;
                }

                if(MEM_DATA_WRITE_U8 == (MemJobList_u8[MemJobCount_u8] & MEM_STATUS_WRITE_U8))
                {
                    /* copy user data to write buffer */
                    WriteError_u8 = 0U;
                    NvM_CurrentBlockInfo.LastResult_u8 = MEM_BLOCK_OK;
                    MemConfigList[MemJobCount_u8].WriteFunction();

                    resultflag = NvMCdd_WriteBlock(MemConfigList[MemJobCount_u8].FeeMemBlockId_u16,
                          WriteDataBuffer);

                    if(E_NOT_OK == resultflag)
                    {
                        MemErrorCounter_u8++;
                        MemState_en = MEM_JOB_PENDING;
                    }
                    else
                    {
                        if(MEM_END_NOTIFY == NvM_CurrentBlockInfo.NotifyFlag_u8)
                        {
                            NvM_CurrentBlockInfo.NotifyFlag_u8 = MEM_NO_NOTIFY;
                            MemState_en = MEM_JOB_BUSY;

                        }
                        else if(MEM_ERROR_NOTIFY == NvM_CurrentBlockInfo.NotifyFlag_u8)
                        {
                            //MemErrorCounter_u8++;
                            NvM_CurrentBlockInfo.NotifyFlag_u8 = MEM_NO_NOTIFY;
                            MemState_en = MEM_JOB_ERROR;
                        }
                        else
                        {
                            MemState_en = MEM_JOB_PENDING;
                        }
                    }

                    /* set job state to busy */
                    MemJobList_u8[MemJobCount_u8] = (MemJobList_u8[MemJobCount_u8] & MEM_STATUS_READ_U8) +
                      MEM_WRITE_BUSY_U8;

                    break;

                }
                else if(MEM_DATA_READ_U8 == (MemJobList_u8[MemJobCount_u8] & MEM_STATUS_READ_U8))
                {
                    /* read request job from fee */
                    NvM_CurrentBlockInfo.LastResult_u8 = MEM_BLOCK_OK;
                    resultflag = NvMCdd_ReadBlock( MemConfigList[MemJobCount_u8].FeeMemBlockId_u16, 0,
                            ReadDataBuffer, MemConfigList[MemJobCount_u8].Length_u16 );

                    if(E_NOT_OK == resultflag)
                    {
                        MemErrorCounter_u8++;
                        MemState_en = MEM_JOB_PENDING;
                    }
                    else
                    {
                        if(MEM_END_NOTIFY == NvM_CurrentBlockInfo.NotifyFlag_u8)
                        {
                            NvM_CurrentBlockInfo.NotifyFlag_u8 = MEM_NO_NOTIFY;
                            MemState_en = MEM_JOB_BUSY;

                        }
                        else if(MEM_ERROR_NOTIFY == NvM_CurrentBlockInfo.NotifyFlag_u8)
                        {
                            //MemErrorCounter_u8++;
                            NvM_CurrentBlockInfo.NotifyFlag_u8 = MEM_NO_NOTIFY;
                            MemState_en = MEM_JOB_ERROR;
                        }
                        else
                        {
                            MemState_en = MEM_JOB_PENDING;
                        }
                    }

                    MemJobList_u8[MemJobCount_u8] = (MemJobList_u8[MemJobCount_u8] & MEM_STATUS_WRITE_U8) +
                          MEM_READ_BUSY_U8;

                    break;
                }
                else
                {
                    MemJobCount_u8++;
                    if (MEM_NUM_OF_BLOCKS <= MemJobCount_u8)
                    {
                        MemJobCount_u8 = 0;
                    }
                }//end of if-else

            }//end of for-loop

            break;
        }

        case MEM_JOB_BUSY:
        {
            MemErrorCounter_u8 = 0U;
            if (MEM_READ_BUSY_U8 ==(MemJobList_u8[MemJobCount_u8] & MEM_STATUS_READ_U8))
            {
                /* copy read data to user structure and perform checksum calculation */
                if (TRUE == MemConfigList[MemJobCount_u8].ReadFunction())
                {
                   /* advance to ready state */
                   MemJobList_u8[MemJobCount_u8] = (MemJobList_u8[MemJobCount_u8] & MEM_STATUS_WRITE_U8) +
                       MEM_READ_READY_U8;
                   MemState_en = MEM_JOB_NO;

                }
                else
                {
                  /* advance to error state */
                  MemJobList_u8[MemJobCount_u8] = (MemJobList_u8[MemJobCount_u8] & MEM_STATUS_WRITE_U8) +
                      MEM_READ_CS_ERROR_U8;
                  MemState_en = MEM_JOB_NO;

                }

            }
            else if (MEM_WRITE_BUSY_U8 == (MemJobList_u8[MemJobCount_u8] & MEM_STATUS_WRITE_U8))
            {
                MemState_en = MEM_JOB_START_VERIFY;  /* advance to verification state */
            }
            else
            {
                MemState_en = MEM_JOB_NO;
            }

            break;
        }

        case MEM_JOB_PENDING:
        {
            if(MEM_BLOCK_ERROR == NvM_CurrentBlockInfo.LastResult_u8)
            {
                if (MEM_HW_ERROR_OCCURRED == MemHWError_en)
                {
                    MemState_en = MEM_UNINIT;
                    /* Error Handler function call and initialization */
                    //ErrHandler(Parameters);
                }
                else
                {
                    MemJobList_u8[MemJobCount_u8] = MEM_WRITE_FEE_ERROR_U8 | MEM_READ_FEE_ERROR_U8;
                    MemState_en = MEM_UNINIT;
                    NvMCdd_Init();
                    MemHWError_en = MEM_HW_ERROR_OCCURRED;
                }

                break;
            }
            else
            {
                if (MEM_READ_BUSY_U8 == (MemJobList_u8[MemJobCount_u8] & MEM_STATUS_READ_U8))
                {

                    /* read request job from fee */
                    resultflag = NvMCdd_ReadBlock( MemConfigList[MemJobCount_u8].FeeMemBlockId_u16, 0,
                            ReadDataBuffer, MemConfigList[MemJobCount_u8].Length_u16 );

                    if(E_NOT_OK == resultflag)
                    {
                        MemErrorCounter_u8++;
                        MemState_en = MEM_JOB_PENDING;
                    }
                    else
                    {
                        if(MEM_END_NOTIFY == NvM_CurrentBlockInfo.NotifyFlag_u8)
                        {
                            NvM_CurrentBlockInfo.NotifyFlag_u8 = MEM_NO_NOTIFY;
                            MemState_en = MEM_JOB_BUSY;

                        }
                        else if(MEM_ERROR_NOTIFY == NvM_CurrentBlockInfo.NotifyFlag_u8)
                        {
                            //MemErrorCounter_u8++;
                            NvM_CurrentBlockInfo.NotifyFlag_u8 = MEM_NO_NOTIFY;
                            MemState_en = MEM_JOB_ERROR;
                        }
                        else
                        {
                            MemState_en = MEM_JOB_PENDING;
                        }
                    }
                }
                else
                {
                    resultflag = NvMCdd_WriteBlock(MemConfigList[MemJobCount_u8].FeeMemBlockId_u16,
                          WriteDataBuffer);

                    if(E_NOT_OK == resultflag)
                    {
                        MemErrorCounter_u8++;
                        MemState_en = MEM_JOB_PENDING;
                    }
                    else
                    {
                        if(MEM_END_NOTIFY == NvM_CurrentBlockInfo.NotifyFlag_u8)
                        {
                            NvM_CurrentBlockInfo.NotifyFlag_u8 = MEM_NO_NOTIFY;
                            MemState_en = MEM_JOB_BUSY;

                        }
                        else if(MEM_ERROR_NOTIFY == NvM_CurrentBlockInfo.NotifyFlag_u8)
                        {
                            //MemErrorCounter_u8++;
                            NvM_CurrentBlockInfo.NotifyFlag_u8 = MEM_NO_NOTIFY;
                            MemState_en = MEM_JOB_ERROR;
                        }
                        else
                        {
                            MemState_en = MEM_JOB_PENDING;
                        }
                    }
                }

             }//end if (MEM_HW_ERROR_OCCURRED == MemHWError_en)

             /* check whether FEE did not react for too long */
             if (MEM_FEE_TIMEOUT_U8 <= MemErrorCounter_u8)
             {
                 MemState_en = MEM_JOB_ERROR;
             }

             break;
        }

        default:
        {
            break;
        }

    }//end of switch
}

static void NvM_SubFSM(void)
{
    uint16 Counter_u16 = 0U;

    if (MEM_UNINIT == MemState_en)
    {
        return;
    }

    switch(MemState_en)
    {

        case MEM_JOB_START_VERIFY:
        {
/*
            if(TRUE == NvM_CurrentBlockInfo.InternalFlag_u2)
            {
                NvM_CurrentBlockInfo.LastResult_u8 = MEM_BUSY_JOB;
                NvM_CurrentBlockInfo.InternalFlag_u2 = FALSE;
                return;
            }
*/
            /* read request job from fee */
            NvM_CurrentBlockInfo.LastResult_u8 = MEM_BLOCK_OK;
            resultflag = NvMCdd_ReadBlock( MemConfigList[MemJobCount_u8].FeeMemBlockId_u16, 0,
                    ReadDataBuffer, MemConfigList[MemJobCount_u8].Length_u16 );

            if(E_NOT_OK == resultflag)
            {
                MemErrorCounter_u8++;
                MemState_en = MEM_JOB_PENDING_VERIFY;
            }
            else
            {
                if(MEM_END_NOTIFY == NvM_CurrentBlockInfo.NotifyFlag_u8)
                {
                    NvM_CurrentBlockInfo.NotifyFlag_u8 = MEM_NO_NOTIFY;
                    MemState_en = MEM_JOB_BUSY_VERIFY;

                }
                else if(MEM_ERROR_NOTIFY == NvM_CurrentBlockInfo.NotifyFlag_u8)
                {
                    //MemErrorCounter_u8++;
                    NvM_CurrentBlockInfo.NotifyFlag_u8 = MEM_NO_NOTIFY;
                    MemState_en = MEM_JOB_ERROR;
                }
                else
                {
                    MemState_en = MEM_JOB_PENDING_VERIFY;
                }
            }

            break;
        }

        case MEM_JOB_PENDING_VERIFY:
        {
            if(MEM_BLOCK_ERROR == NvM_CurrentBlockInfo.LastResult_u8)
            {
                if (MEM_HW_ERROR_OCCURRED == MemHWError_en)
                {
                   MemState_en = MEM_UNINIT;
                   /* Error Handler function call and initialization */
                   //ErrHandler(Parameters);
                }
                else
                {
                    MemJobList_u8[MemJobCount_u8] = MEM_WRITE_FEE_ERROR_U8 | MEM_READ_FEE_ERROR_U8;
                    MemState_en = MEM_UNINIT;
                    NvMCdd_Init();
                    MemHWError_en = MEM_HW_ERROR_OCCURRED;
                }

                break;
            }
            else
            {
                /* read request job from fee */
                resultflag = NvMCdd_ReadBlock( MemConfigList[MemJobCount_u8].FeeMemBlockId_u16, 0,
                        ReadDataBuffer, MemConfigList[MemJobCount_u8].Length_u16 );

                if(E_NOT_OK == resultflag)
                {
                    MemErrorCounter_u8++;
                    MemState_en = MEM_JOB_PENDING_VERIFY;
                }
                else
                {
                    if(MEM_END_NOTIFY == NvM_CurrentBlockInfo.NotifyFlag_u8)
                    {
                        NvM_CurrentBlockInfo.NotifyFlag_u8 = MEM_NO_NOTIFY;
                        MemState_en = MEM_JOB_BUSY_VERIFY;

                    }
                    else if(MEM_ERROR_NOTIFY == NvM_CurrentBlockInfo.NotifyFlag_u8)
                    {
                        //MemErrorCounter_u8++;
                        NvM_CurrentBlockInfo.NotifyFlag_u8 = MEM_NO_NOTIFY;
                        MemState_en = MEM_JOB_ERROR;
                    }
                    else
                    {
                        MemState_en = MEM_JOB_PENDING_VERIFY;
                    }
                }
             }

             /* check whether FEE did not react for too long */
             if (MEM_FEE_TIMEOUT_U8 <= MemErrorCounter_u8)
             {
                 MemState_en = MEM_JOB_ERROR;
             }
             break;
        }

        case MEM_JOB_BUSY_VERIFY:
        {

            MemErrorCounter_u8 = 0U;
            if(MEM_BLOCK_ERROR == NvM_CurrentBlockInfo.LastResult_u8)
            {
                if (MEM_HW_ERROR_OCCURRED == MemHWError_en)
                {
                    MemState_en = MEM_UNINIT;
                    /* Error Handler function call and initialization */
                    //ErrHandler(Parameters);
                }
                else
                {
                    MemJobList_u8[MemJobCount_u8] = MEM_WRITE_FEE_ERROR_U8 | MEM_READ_FEE_ERROR_U8;
                    MemState_en = MEM_UNINIT;
                    NvMCdd_Init();
                    MemHWError_en = MEM_HW_ERROR_OCCURRED;
                }
            }
            else
            {
                VerifyCounter_u16 = 0;
                for (Counter_u16 = 0; Counter_u16 < MemConfigList[MemJobCount_u8].Length_u16; Counter_u16++)
                {
                     VerifyCounter_u16++;

                     /* compare written with read-back data */
                     if (ReadDataBuffer[Counter_u16] != WriteDataBuffer[Counter_u16])
                     {
                             break;          /* data differ */
                     }
                }

                if (VerifyCounter_u16 == MemConfigList[MemJobCount_u8].Length_u16)
                {
                    MemJobList_u8[MemJobCount_u8] = (MemJobList_u8[MemJobCount_u8] & MEM_STATUS_READ_U8) +
                            MEM_WRITE_READY_U8;

                    MemJobList_u8[MemJobCount_u8] = (MemJobList_u8[MemJobCount_u8] & MEM_STATUS_WRITE_U8) +
                            MEM_READ_READY_U8;

                    MemState_en = MEM_JOB_NO;
                }
                else
                {
                     // Verify failed
                     if (MEM_MAX_TRY_WRITE_U8 > WriteError_u8)
                     {
                        WriteError_u8++;
                        /* start job again */
                        MemJobList_u8[MemJobCount_u8] = (MemJobList_u8[MemJobCount_u8] & MEM_STATUS_READ_U8) +
                            MEM_WRITE_BUSY_U8;

                        NvM_CurrentBlockInfo.LastResult_u8 = MEM_BLOCK_OK;
                        MemConfigList[MemJobCount_u8].WriteFunction();
                        MemState_en = MEM_JOB_PENDING;

                     }
                     else
                     {
                        MemJobList_u8[MemJobCount_u8] = (MemJobList_u8[MemJobCount_u8] & MEM_STATUS_READ_U8) +
                            MEM_WRITE_FEE_ERROR_U8;
                        MemState_en = MEM_JOB_NO;

                     }
                }
            }

            break;
        }

        case MEM_JOB_ERROR:
        {
             /* was it a read job? */
             if (MEM_READ_BUSY_U8 == (MemJobList_u8[MemJobCount_u8] & MEM_STATUS_READ_U8))
             {
                   if (NULL_PTR != MemConfigList[MemJobCount_u8].MirrorRamAddr_u8)
                   {
                      /* copy defaults from rom */
                      for (Counter_u16 = 0; Counter_u16 < MemConfigList[MemJobCount_u8].Length_u16; Counter_u16++)
                      {
                         *(MemConfigList[MemJobCount_u8].RamAddr_u8 + Counter_u16) =
                             *(MemConfigList[MemJobCount_u8].MirrorRamAddr_u8 + Counter_u16);
                      }
                   }

                   /* advance to error state */
                   MemJobList_u8[MemJobCount_u8] = (MemJobList_u8[MemJobCount_u8] & MEM_STATUS_WRITE_U8) +
                           MEM_READ_FEE_ERROR_U8;

             }
             else if (MEM_WRITE_BUSY_U8 == (MemJobList_u8[MemJobCount_u8] & MEM_STATUS_WRITE_U8))
             {
                   /* advance to error state */
                   MemJobList_u8[MemJobCount_u8] = (MemJobList_u8[MemJobCount_u8] & MEM_STATUS_READ_U8) +
                       MEM_WRITE_FEE_ERROR_U8;
             }

             /* make an error entry */
             //ErrHandler(Parameters);
             //MemState_en = MEM_UNINIT;
             MemState_en = MEM_JOB_NO;
             break;

        }

        default:
        {
            break;
        }
      }//end of switch


    if((MemState_en == MEM_JOB_NO) || (MemState_en == MEM_UNINIT) || (MemState_en == MEM_JOB_ERROR) ||
            (MemState_en == MEM_VOLTAGE_OOR))
    {
        //NvM_ReadDataOffset[MemJobCount_u8].BlockOffset_en = MEM_OFFSET_OFF;
        NvM_CurrentBlockInfo.InternalFlag_u2 = FALSE;
    }
    else if(MEM_ASYNC_ACCESS_ON == NvM_CurrentBlockInfo.SyncAccessFlag_u8)
    {
        NvM_CurrentBlockInfo.InternalFlag_u2 = FALSE;
    }
    else
    {
        NvM_CurrentBlockInfo.InternalFlag_u2 = TRUE;
    }

}

static Std_ReturnType NvMCdd_WriteBlock( uint16 MemBlockId_u16, uint8* DestPtr)
{

  if(MEM_BLOCK_OK == NvM_CurrentBlockInfo.LastResult_u8)
  {
      /* copy user data to write buffer */
      MemConfigList[MemJobCount_u8].WriteFunction();

      //MemBlockId_u16 += 1U;
      resultflag = Fee_Write((MemBlockId_u16 + 1U), DestPtr);   /* one time call */

      if( E_OK == resultflag )
      {
          if(MEM_ASYNC_ACCESS_OFF == NvM_CurrentBlockInfo.SyncAccessFlag_u8)
          {
              do{
                  Fee_MainFunction();
                  Fls_17_Pmu_MainFunction();
              }while(MEMIF_IDLE != Fee_GetStatus());
          }

          NvM_CurrentBlockInfo.LastResult_u8 = MEM_BLOCK_PENDING;
      }
      else
      {
            switch(Fee_GetStatus())
            switch(MEMIF_UNINIT)
            {
                case MEMIF_UNINIT:
                    NvM_CurrentBlockInfo.LastResult_u8 = MEM_BLOCK_ERROR;
                    break;

                case MEMIF_BUSY_INTERNAL:
                    break;

                default:
                    break;
            }
      }
  }
  else
  {
      resultflag = E_OK;
  }

  return resultflag;

}

static Std_ReturnType NvMCdd_ReadBlock(uint16 MemBlockId_u16, uint16 BlockOffset, uint8 *SrcPtr, uint16 Length)
{
    if(MEM_BLOCK_OK == NvM_CurrentBlockInfo.LastResult_u8)
    {
        if(MEM_OFFSET_ON == NvM_ReadDataOffset[MemJobCount_u8].BlockOffset_en)
        {
            BlockOffset = NvM_ReadDataOffset[MemJobCount_u8].BlockOffset_u16;
            Length = NvM_ReadDataOffset[MemJobCount_u8].BlockSize_u16;
        }
        else
        {
            if(MEM_CRC == MemConfigList[MemJobCount_u8].Crc_en)
            {
                Length = (Length + CRC16);
            }
            else
            {
                /* do nothing */
            }
        }

        //MemBlockId_u16 += 1U;
        resultflag = Fee_Read((MemBlockId_u16 + 1U), BlockOffset, SrcPtr, Length);  /* one time call */
        if( E_OK == resultflag )
        {
            if(MEM_ASYNC_ACCESS_OFF == NvM_CurrentBlockInfo.SyncAccessFlag_u8)
            {
                do{
                    Fee_MainFunction();
                    Fls_17_Pmu_MainFunction();
                }while(MEMIF_IDLE != Fee_GetStatus());
            }
            NvM_CurrentBlockInfo.LastResult_u8 = MEM_BLOCK_PENDING;
        }
        else
        {
            switch(Fee_GetStatus())
            {
                case MEMIF_UNINIT:
                    NvM_CurrentBlockInfo.LastResult_u8 = MEM_BLOCK_ERROR;
                    break;

                case MEMIF_BUSY_INTERNAL:
                    break;

                default:
                    break;
            }
        }
    }
    else
    {
        resultflag = E_OK;
    } //NvM_CurrentBlockInfo.LastResult_u8 = MEM_PENDING_JOB;

    return resultflag;
}

static uint8 Mem_SetHiPrioJob(uint16 MemBlockId_u16)
{
    MemJobState_t retVal = MEM_OK_JOB;

   if(MEM_NUM_OF_BLOCKS != NvM_CurrentBlockInfo.MemHiPrioJob_u16)
   {
       retVal = MEM_BUSY_JOB;
   }
   else
   {
      if((MEM_NUM_OF_BLOCKS <= MemBlockId_u16) || (MEM_NO_BLK > MemBlockId_u16))
      {
          retVal = MEN_NO_JOB;
      }
      else
      {
          NvM_CurrentBlockInfo.MemHiPrioJob_u16 = MemBlockId_u16;
      }
   }
   return retVal;

}


uint8 Mem_SetWriteJob(uint16 MemBlockId_u16)
{
    MemStates_t State_en = MemState_en;
    MemJobState_t retVal = MEM_OK_JOB;

    if (MEM_UNINIT == State_en)
    {
        return MEM_ERROR_JOB;
    }

    if ((MEM_NUM_OF_BLOCKS <= MemBlockId_u16) || (MEM_NO_BLK > MemBlockId_u16))
    {
        retVal = MEN_NO_JOB;
    }
    else
    {
        if (MEM_WRITE_BUSY_U8 != (MemJobList_u8[MemBlockId_u16] & MEM_STATUS_WRITE_U8))
        {
            // insert write job into jobs list
            MemJobList_u8[MemBlockId_u16] =
                    (MemJobList_u8[MemBlockId_u16] & MEM_STATUS_READ_U8) + MEM_DATA_WRITE_U8;
        }
        else
        {
            retVal = MEM_BUSY_JOB;
        }
    }

    return retVal;

}

uint8 Mem_SetReadJob(uint16 MemBlockId_u16)
{
    MemStates_t State_en = MemState_en;
    MemJobState_t retVal = MEM_OK_JOB;


    if (MEM_UNINIT == State_en)
    {
        return MEM_ERROR_JOB;
    }

    if ((MEM_NUM_OF_BLOCKS <= MemBlockId_u16) || (MEM_NO_BLK > MemBlockId_u16))
    {
        retVal = MEN_NO_JOB;
    }
    else
    {
        if (MEM_READ_BUSY_U8 != (MemJobList_u8[MemBlockId_u16] & MEM_STATUS_READ_U8))
        {
           /* insert read job into jobs list */
            MemJobList_u8[MemBlockId_u16] =
                    (MemJobList_u8[MemBlockId_u16] & MEM_STATUS_WRITE_U8) + MEM_DATA_READ_U8;
        }
        else
        {
            retVal = MEM_BUSY_JOB;
        }
    }

    return retVal;
}

uint8 Mem_SetReadOffsetJob(uint16 MemBlockId_u16, uint16 BlockOffset, uint16 Lenght)
{
    MemStates_t State_en = MemState_en;
    MemJobState_t retVal = MEM_OK_JOB;


    if (MEM_UNINIT == State_en)
    {
        retVal = MEM_ERROR_JOB;
    }

    if ((MEM_NUM_OF_BLOCKS <= MemBlockId_u16) || (MEM_NO_BLK > MemBlockId_u16))
    {
        retVal = MEN_NO_JOB;
    }

    if((MEM_NUM_OF_BLOCKS < (BlockOffset + Lenght)) || (MEM_NO_BLK >= BlockOffset) || (MEM_NO_BLK > Lenght))
    {
        retVal = MEN_NO_JOB;
    }
    else
    {
        if (MEM_READ_BUSY_U8 != (MemJobList_u8[MemBlockId_u16] & MEM_STATUS_READ_U8))
        {
           /* insert read job into jobs list */
            NvM_ReadDataOffset[MemBlockId_u16].BlockOffset_en = MEM_OFFSET_ON;
            NvM_ReadDataOffset[MemBlockId_u16].BlockOffset_u16 = BlockOffset;
            NvM_ReadDataOffset[MemBlockId_u16].BlockSize_u16 = Lenght;

            MemJobList_u8[MemBlockId_u16] =
                    (MemJobList_u8[MemBlockId_u16] & MEM_STATUS_WRITE_U8) + MEM_DATA_READ_U8;
        }
        else
        {
            retVal = MEM_BUSY_JOB;
        }
    }

    return retVal;

}

uint8 Mem_GetJobStatus(uint16 MemBlockId_u16, uint8 MemJob_u8)
{
    uint16 Counter_u16 = 0;  /* counter for loop over all jobs */
    MemJobState_t retVal = MEM_OK_JOB;
    MemStates_t State_en = MemState_en;

    if (MEM_UNINIT == State_en)
    {
        return MEM_ERROR_JOB;
    }

    if(MEM_ALL_READ_BLOCKS == MemJob_u8)
    {
        for(Counter_u16 = 0; Counter_u16 < MEM_NUM_OF_BLOCKS; Counter_u16++)
        {
            if (((uint8)MEM_DATA_READ_U8 == (MemJobList_u8[Counter_u16] & (uint8)MEM_STATUS_READ_U8)) ||
                ((uint8)MEM_READ_BUSY_U8 == (MemJobList_u8[Counter_u16] & (uint8)MEM_STATUS_READ_U8)))
            {
                return MEM_BUSY_JOB;
            }
        }
    }
    else if(MEM_ALL_WRITE_BLOCKS == MemJob_u8)
    {
        for(Counter_u16 = 0; Counter_u16 < MEM_NUM_OF_BLOCKS; Counter_u16++)
        {
            if (((uint8)MEM_DATA_WRITE_U8 == (MemJobList_u8[Counter_u16] & (uint8)MEM_STATUS_WRITE_U8)) ||
                ((uint8)MEM_WRITE_BUSY_U8 == (MemJobList_u8[Counter_u16] & (uint8)MEM_STATUS_WRITE_U8)))
            {
                return MEM_BUSY_JOB;
            }
        }
    }
    else
    {
        if((MemBlockId_u16 < MEM_FEE_BLK_1) || (MemBlockId_u16 >= MEM_NUM_OF_BLOCKS))
        {
            retVal = MEN_NO_JOB;
        }
        else
        {
            /* is a read job pending or being processed? */
            if( (((uint8)MEM_READ_CS_ERROR_U8 == (MemJobList_u8[MemBlockId_u16] & (uint8)MEM_STATUS_READ_U8)) ||
                  ((uint8)MEM_READ_FEE_ERROR_U8 == (MemJobList_u8[MemBlockId_u16] & (uint8)MEM_STATUS_READ_U8))) ||
                //( ((uint8)MEM_WRITE_CS_ERROR_U8 == (MemJobList_u8[MemBlockId_u16] & (uint8)MEM_STATUS_WRITE_U8)) ||
                 ( ((uint8)MEM_WRITE_FEE_ERROR_U8 == (MemJobList_u8[MemBlockId_u16] & (uint8)MEM_STATUS_WRITE_U8))
                ))
            {
                retVal = MEM_ERROR_JOB;
            }
            else if ( (((uint8)MEM_DATA_READ_U8 == (MemJobList_u8[MemBlockId_u16] & MEM_STATUS_READ_U8)) ||
                  ((uint8)MEM_READ_BUSY_U8 == (MemJobList_u8[MemBlockId_u16] & MEM_STATUS_READ_U8))) ||
                ( ((uint8)MEM_DATA_WRITE_U8 == (MemJobList_u8[MemBlockId_u16] & MEM_STATUS_WRITE_U8)) ||
                  ((uint8)MEM_WRITE_BUSY_U8 == (MemJobList_u8[MemBlockId_u16] & MEM_STATUS_WRITE_U8))
                ))
            {
                retVal = MEM_BUSY_JOB;
            }
        }
    }

    return retVal;
}

void Mem_ReadDataBlock(uint16 MemBlockId_u16, uint8 *DestBufPtr)
{
    uint16 Counter_u16 = 0;

    for(Counter_u16 = 0; Counter_u16 < MemConfigList[MemBlockId_u16].Length_u16; Counter_u16++)
    {
        *(DestBufPtr + Counter_u16) = *(MemConfigList[MemBlockId_u16].RamAddr_u8 + Counter_u16);
    }
}

void Mem_ReadDataOffset(uint16 MemBlockId_u16, uint16 BlockOffset, uint8 *DestBufPtr, uint16 Lenght)
{
    uint16 Counter_u16 = 0;
    for(Counter_u16 = BlockOffset; Counter_u16 < (BlockOffset + Lenght); Counter_u16++)
    {
        *(DestBufPtr + Counter_u16) = *(MemConfigList[MemBlockId_u16].RamAddr_u8 + Counter_u16);
    }
}

void Mem_WriteDataBlock(uint16 MemBlockId_u16, uint8 *SrcBufPtr)
{
    uint16 Counter_u16 = 0;

    for(Counter_u16 = 0; Counter_u16 < MemConfigList[MemBlockId_u16].Length_u16; Counter_u16++)
    {
        *(MemConfigList[MemBlockId_u16].RamAddr_u8 + Counter_u16) = *(SrcBufPtr + Counter_u16);
    }

}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define NVM_HNDLR_STOP_SEC_CODE
#include "Nvm_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
