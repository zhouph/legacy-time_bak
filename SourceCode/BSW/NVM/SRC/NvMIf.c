/**
 * @defgroup NvMIf NvMIf
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        NvMIf.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "NvMIf.h"
#if (TC27X_FEE == ENABLE) /* KCLim_Fee : For TC27x Fee*/
#include "Nvm_Hndlr.h"
#endif

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/


/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLES DEFINITIONS
 =============================================================================*/
extern Nvm_Hndlr_HdrBusType Nvm_HndlrBus;
NvMIf_LogicBlockType NvMIf_LogicEepData;

uint16_t weu16EepBuff[2];
uint8_t  weu8EepFwData[16];
uint8_t  wmu8EcuHwVersion; 
static uint8_t  weu8E2pState;
uint8_t  weu8CopyEepBuff[EEP_ERROR_BYTE_NO];
uint8_t  weu8CopySuppEepBuff[EEP_ERROR_BYTE_NO];
static uint8_t  we_u8EEPErr;
uint16_t weu16Eep_addr;;
t_u8Bit weEepFlg;

#if (TC27X_FEE == ENABLE) /* KCLim_Fee : For TC27x Fee*/
uint16_t Fee_WriteAllFailBlock = 0x0000; /* Use Bit upto 16ea */
MemJobState_t  weReadAllRet = MEM_OK_JOB;
MemJobState_t  weWriteAllRet = MEM_OK_JOB;
#endif /* #if (TC27X_FEE == ENABLE) */

/* KCLim NvMIf : Variables defined in FS */
ErrBuffer_t         ErrBuffer[U8NUM_MAX_ERROR_NUM];
__align(4) uint8    feu8ErrorPointer[U8NUM_SIZE_ERROR_PTR];
/*==============================================================================
 *                  LOCAL VARIABLES DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
static void WE_vInitBlock(void);
static void WE_vHandleEEEError(void);
static void WE_vInitE2pBuff(void);
static void WE_vCopyEepDataToRam(void);
static void Test_DtcArray();

#if (TC27X_FEE == ENABLE) /* KCLim_Fee : For TC27x Fee*/
uint16_t WE_u8GetBlockId(uint16_t u16Address);
uint16_t WE_u8GetBlockBasePosition(MemBlockId_t u16BlockId);
void     WE_vGetBlockIdBlockPosition(uint16_t u16Address, uint16_t *u16BlockId, uint16_t *u16Position);
#endif /* #if (TC27X_FEE == ENABLE) */

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void WE_vInitEEP(void)
{
    uint8_t rev_temp[2]={0,0};
    uint16_t cmp_temp=0;
    
    (void)NvM_ReadBlock(MEM_FEE_BLK_2, &NvMIf_LogicEepData.LogicBlock[0]);


#if (TC27X_FEE == ENABLE) /* KCLim_Fee : For TC27x Fee*/
    WE_u8ReadEEP_Byte(E2P_START_ADDR,&rev_temp[0],2);
    cmp_temp= (uint16_t)(((uint16_t)rev_temp[0]<<8) | rev_temp[1]);

    if(E2P_MAGIC_NUM != cmp_temp)
    {
        WE_vInitE2pBuff();
        WE_vCopyEepDataToRam();
    }
    else
    {
        WE_vCopyEepDataToRam();
    }

#endif /* #if (TC27X_FEE == ENABLE) */
}

void WE_vApplProcessWER(void)
{
    static uint8_t wr_cnt;

    uint8_t u8tEepReadData[4];
	uint32_t u32tEepReadData;
    uint32_t u32EepWriteData;
    static uint32_t *p32Eep_Data;

    static uint16_t eep_addr;
    uint8_t rd_temp;
    
#if (TC27X_FEE == ENABLE) /* KCLim_Fee : For TC27x Fee*/
    MemBlockId_t u16FeeBlockId             = 0x0000;
    static uint16_t Write_u16FeeBlockBasePosition   = 0x0000;
    static uint16_t Write_u16FeeBlockTargetPosition = 0x0000;
    static uint16_t Testweu16EepBuff[2];

    if((weu1WriteSector==1)|| (weu1EraseSector == 1))
    {

		if(weu1EraseSector==1)
		{
			weu16EepBuff[0] = 0xFFFF;
			weu16EepBuff[1] = 0xFFFF;
		}

		p32Eep_Data = (uint32_t*)weu16EepBuff;

		eep_addr  = weu16Eep_addr;

	    WE_vGetBlockIdBlockPosition(eep_addr, &u16FeeBlockId, &Write_u16FeeBlockTargetPosition);
        
#if (0)
		/* 0916 KCLim: Fee*/
		MemConfigList[u16FeeBlockId].RamAddr_u8[u16FeeBlockTargetPosition] = (uint8_t)(weu16EepBuff[0]>>8);
		u16FeeBlockTargetPosition++;
		MemConfigList[u16FeeBlockId].RamAddr_u8[u16FeeBlockTargetPosition] = (uint8_t)(weu16EepBuff[0]);
		u16FeeBlockTargetPosition++;
		MemConfigList[u16FeeBlockId].RamAddr_u8[u16FeeBlockTargetPosition] = (uint8_t)(weu16EepBuff[1]>>8);
		u16FeeBlockTargetPosition++;
		MemConfigList[u16FeeBlockId].RamAddr_u8[u16FeeBlockTargetPosition] = (uint8_t)(weu16EepBuff[1]);
#else
		/* 0916 KCLim: Fee*/
		MemConfigList[u16FeeBlockId].RamAddr_u8[Write_u16FeeBlockTargetPosition] = (uint8_t)(weu16EepBuff[0]);
		Write_u16FeeBlockTargetPosition++;
		MemConfigList[u16FeeBlockId].RamAddr_u8[Write_u16FeeBlockTargetPosition] = (uint8_t)(weu16EepBuff[0]>>8);
		Write_u16FeeBlockTargetPosition++;
		MemConfigList[u16FeeBlockId].RamAddr_u8[Write_u16FeeBlockTargetPosition] = (uint8_t)(weu16EepBuff[1]);
		Write_u16FeeBlockTargetPosition++;
		MemConfigList[u16FeeBlockId].RamAddr_u8[Write_u16FeeBlockTargetPosition] = (uint8_t)(weu16EepBuff[1]>>8);
		Testweu16EepBuff[0] = weu16EepBuff[0];
		Testweu16EepBuff[1] = weu16EepBuff[1];
#endif
		weu1WriteSector=0;
		weu1EraseSector=0;
    } /* if((weu1WriteSector==1)|| (weu1EraseSector == 1)) */
    
#endif /* #if (TC27X_FEE == ENABLE) */
}    

uint8_t WE_u8ReadEEP_Byte(uint16_t e2p_id, uint8_t *ch_ptr, uint16_t num)
{

#if (TC27X_FEE == ENABLE) /* KCLim_Fee : For TC27x Fee*/
    uint16_t u16i                      = 0x0000;
    uint16_t u16FeeBlockId             = 0x0000;

    static uint16_t Read_u16FeeBlockTargetPosition = 0x0000;
    static uint16_t Read_u16FeeBlockNextPosition = 0x0000;


//    u16FeeBlockId           = WE_u8GetBlockId(e2p_id);
//    Read_u16FeeBlockBasePosition = WE_u8GetBlockBasePosition(u16FeeBlockId);
//    Read_u16FeeBlockTargetPosition = e2p_id - Read_u16FeeBlockBasePosition;

	/* Get Block Id and target position(Index) in Block */
    WE_vGetBlockIdBlockPosition(e2p_id, &u16FeeBlockId, &Read_u16FeeBlockTargetPosition);


    for(u16i = 0x00; u16i < num; u16i++)
    {
    	Read_u16FeeBlockNextPosition = Read_u16FeeBlockTargetPosition + u16i;
    	ch_ptr[u16i] = MemConfigList[u16FeeBlockId].RamAddr_u8[Read_u16FeeBlockNextPosition];
    }

    return 1;
#endif /* #if (TC27X_FEE == ENABLE) */
}

static void WE_vInitE2pBuff(void)
{
    uint8_t rev_temp[2]={0,0};
    uint16_t cmp_temp = 0, i=0;
    static uint8_t  weu8RevData[16];
    static uint16_t weu16RcvData=0;

#if (TC27X_FEE == ENABLE) /* KCLim_Fee : For TC27x Fee*/
    /*
     * 	1. 紐⑤�?EEPROM 0�쑝濡� ?�덇린�??
     * 	2. EEPROM 泥ル쾲吏�?2byte�뿉 留ㅼ쭅�꽆踰�?Write
     *  3. Call WE_vInitBlock();
     * */
    uint8_t weu8MagicNum[2] = {0};
    uint16_t weu16FeeBlockId         = 0x0000;
    uint16_t weu16FeeBlockTargetPosition = 0x0000;
    uint16_t weu16FeeBlockNextPosition   = 0x0000;

	/* Get Block Id and target position(Index) in Block */
    WE_vGetBlockIdBlockPosition(E2P_START_ADDR, &weu16FeeBlockId, &weu16FeeBlockTargetPosition);


    weu8MagicNum[0] = (uint8_t)(E2P_MAGIC_NUM >> 8);
    weu8MagicNum[1] = (uint8_t)(E2P_MAGIC_NUM);

    MemConfigList[weu16FeeBlockId].RamAddr_u8[weu16FeeBlockTargetPosition] = weu8MagicNum[0];
    weu16FeeBlockTargetPosition++;
    MemConfigList[weu16FeeBlockId].RamAddr_u8[weu16FeeBlockTargetPosition] = weu8MagicNum[1];

    WE_vInitBlock();
#endif /* #if (TC27X_FEE == ENABLE) */

}

void NvMIf_UpdateInternalBusData(void)
{
    uint8 i;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KPdtOffsEolReadVal = NvMIf_LogicEepData.Eep.PedlTrvlOffsEepInfo.PdtOffsEolReadVal;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KPdfOffsEolReadVal = NvMIf_LogicEepData.Eep.PedlTrvlOffsEepInfo.PdfOffsEolReadVal;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KPdtOffsDrvgReadVal = NvMIf_LogicEepData.Eep.PedlTrvlOffsEepInfo.PdtOffsDrvgReadVal;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KPdfOffsDrvgReadVal = NvMIf_LogicEepData.Eep.PedlTrvlOffsEepInfo.PdfOffsDrvgReadVal;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KPedlSimPOffsEolReadVal = NvMIf_LogicEepData.Eep.PedlSimrPOffsEepInfo.PedlSimrPOffsEolReadVal;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KPedlSimPOffsDrvgReadVal = NvMIf_LogicEepData.Eep.PedlSimrPOffsEepInfo.PedlSimrPOffsDrvgReadVal;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KPistPOffsEolReadVal = NvMIf_LogicEepData.Eep.PistPOffsEepInfo.PistPOffsEolReadVal;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KPistPOffsDrvgReadVal = NvMIf_LogicEepData.Eep.PistPOffsEepInfo.PistPOffsDrvgReadVal;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KPrimCircPOffsEolReadVal = NvMIf_LogicEepData.Eep.CircPOffsEepInfo.PrimCircPOffsEolReadVal;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KPrimCircPOffsDrvgReadVal = NvMIf_LogicEepData.Eep.CircPOffsEepInfo.PrimCircPOffsDrvgReadVal;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KSecdCircPOffsEolReadVal = NvMIf_LogicEepData.Eep.CircPOffsEepInfo.SecdCircPOffsEolReadVal;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KSecdCircPOffsDrvgReadVal = NvMIf_LogicEepData.Eep.CircPOffsEepInfo.SecdCircPOffsDrvgReadVal;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KSteerEepOffs = NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.KSTEER_offset_of_eeprom;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KYawEepOffs = NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.KYAW_offset_of_eeprom;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KLatEepOffs = NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.KLAT_offset_of_eeprom;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KFsYawEepOffs = NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.fs_yaw_eeprom_offset;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KYawEepMax = NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.KYAW_eep_max;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KYawEepMin = NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.KYAW_eep_min;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KSteerEepMax = NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.KSTEER_eep_max;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KSteerEepMin = NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.KSTEER_eep_min;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KLatEepMax = NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.KLAT_eep_max;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KLatEepMin = NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.KLAT_eep_min;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KYawStillEepMax = NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.yaw_still_eep_max;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KYawStillEepMin = NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.yaw_still_eep_min;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KYawStandEepOffs = NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.lsesps16EEPYawStandOfs;
    for(i=0;i<31;i++) Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[i] = NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.YawTmpEEPMap[i];
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KAdpvVehMdlVchEep = NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.AdpvVehMdlVchEep;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KAdpvVehMdlVcrtRatEep = NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.AdpvVehMdlVcrtRatEep;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KFlBtcTmpEep = NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.btc_tmp_fl_Eep;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KFrBtcTmpEep = NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.btc_tmp_fr_Eep;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KRlBtcTmpEep = NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.btc_tmp_rl_Eep;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KRrBtcTmpEep = NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.btc_tmp_rr_Eep;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KEeBtcsDataEep = NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.ee_btcs_data_Eep;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KLgtSnsrEolEepOffs = NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.LgtSnsrEolOffsEep;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.KLgtSnsrDrvgEepOffs = NvMIf_LogicEepData.Eep.Asw_ActvBrkCtrlEepInfo.LgtSnsrDrvgOffsEep;
    Nvm_HndlrBus.Nvm_HndlrLogicEepDataInfo.ReadInvld = NvMIf_LogicEepData.Eep.ReadInvldFlg;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static void WE_vInitBlock(void)
{
#if (TC27X_FEE == ENABLE) /* KCLim_Fee : For TC27x Fee*/
    uint16_t u16i = 0x0000;
	uint16_t addr;
    MemBlockId_t u16FeeBlockId             = 0x0000;
    uint16_t u16FeeBlockTargetPosition = 0x0000;

    addr=U16_EEP_ERROR_CODE_ADDR;
     /* Clear Error Code */
    WE_vGetBlockIdBlockPosition(addr, &u16FeeBlockId, &u16FeeBlockTargetPosition);

    for (u16i=0x0000;u16i<U16_EEP_ERROR_CODE_SIZE;u16i++)
    {
        MemConfigList[u16FeeBlockId].RamAddr_u8[u16FeeBlockTargetPosition] = 0xFF;
        u16FeeBlockTargetPosition++;
    }
#endif
}

static void WE_vCopyEepDataToRam(void)
{
    uint16_t i=0, j=0;
    uint8_t eepTemp[8]={0,0,0,0,0,0,0,0};
    uint8_t temp=0, rtn_check=0;
    uint8_t data_corrupted = 0;


#if (TC27X_FEE == ENABLE) /* KCLim_Fee : For TC27x Fee*/

    rtn_check=WE_u8ReadEEP_Byte(U16_EEP_ERROR_CODE_ADDR,&weu8CopyEepBuff[0],EEP_ERROR_BYTE_NO);
    rtn_check=WE_u8ReadEEP_Byte(U16_EEP_ERROR2_CODE_ADDR, &weu8CopySuppEepBuff[0], EEP_ERROR_BYTE_NO);


    /* Load Error Pointer Data */
    rtn_check=WE_u8ReadEEP_Byte(U16_EEP_DTC_PTR_ADDR, &feu8ErrorPointer[0], U8NUM_SIZE_ERROR_PTR);

    /* Load DTC Data(DTC_Index, SOD, FTB, FreezeFrame) */
    for(i=0;i<U8NUM_MAX_ERROR_NUM;i++)
    {
        rtn_check=WE_u8ReadEEP_Byte(U16_EEP_DTC_DATA_ADDR+(i*U8NUM_DTC_INFORM), &(ErrBuffer[i].ErrMember[U8_DTC_INFORM_ERRCODE]), U8NUM_DTC_INFORM);

        for (j=0;j<U8NUM_MAX_ERROR_NUM;j++)
        {
            if(feu8ErrorPointer[j] == i)
            {
                break;
            }
        }

        /* Error Buffer : Exist, Error Pointer Nothing */
        if(j>=U8NUM_MAX_ERROR_NUM)
        {
            for(j=0;j<U8NUM_DTC_INFORM;j++)
            {
                data_corrupted = 1 ;
            }
        }

    }


    /* Error Buffer : Nothing, Error Pointer Exist */
    if(data_corrupted == 0)
    {
        for(i=0;i<U8NUM_MAX_ERROR_NUM;i++)
        {
            if(feu8ErrorPointer[i] != 0xFF)
            {
                if (ErrBuffer[feu8ErrorPointer[i]].ErrMember[U8_DTC_INFORM_ERRCODE] == 0)
                {
                    data_corrupted = 1;
                    break;
                }
            }
        }
    }

    /* Same Error Index Exists */
    if(data_corrupted == 0)
    {
        for (i=0;i<U8NUM_MAX_ERROR_NUM;i++)
        {
            if(feu8ErrorPointer[i] != 0xFF)
            {
                for(j=0;j<U8NUM_MAX_ERROR_NUM;j++)
                {
                    if(i != j)
                    {
                        if(feu8ErrorPointer[i] == feu8ErrorPointer[j])
                        {
                            data_corrupted = 1;
                            break;
                        }
                    }
                }

                if(data_corrupted == 1)
                {
                    break;
                }
            }
        }
    }

    if (data_corrupted == 1)
    {
        j=0;
        for (i=0;i<U8NUM_MAX_ERROR_NUM;i++)
        {
            feu8ErrorPointer[i] = 0xFF;
        }

        for (i=0;i<U8NUM_MAX_ERROR_NUM;i++)
        {
            if(ErrBuffer[i].ErrMember[U8_DTC_INFORM_ERRCODE] != 0)
            {
                feu8ErrorPointer[j] = (uchar8_t)i;
                j++;
            }
        }
    }

    /* Read ASIC Version */
    /*===============================================================
       ASIC Version Read �븿�닔 ?�붽�?
    ================================================================*/
    rtn_check=WE_u8ReadEEP_Byte(U16_EEP_ECU_HW_VERSION_DATA,&eepTemp[0],4);
    temp = (eepTemp[2]&0x0C)>>2;
    if(temp==3)
    {
    	Diag_asic_version=0x30;
    }
    else if(temp==0)
    {
    	Diag_asic_version=0x31;
    }
    else if(temp==1)
    {
    	Diag_asic_version=0x32;
    }
    else
    {
        ;
    }

    rtn_check=WE_u8ReadEEP_Byte(U16_EEP_ECU_HW_VERSION_DATA_1,&eepTemp[0],4);
    wmu8EcuHwVersion=eepTemp[0];

#endif /* #if (TC27X_FEE == ENABLE) */
}


#if (TC27X_FEE == ENABLE) /* KCLim_Fee : For TC27x Fee*/
void WE_vGetBlockIdBlockPosition(uint16_t u16Address, uint16_t *u16BlockId, uint16_t *u16Position)
{
    MemBlockId_t u16FeeBlockId             = 0;
    uint16_t     u16FeeBlockBasePosition   = 0x0000;
    uint16_t     u16FeeBlockTargetPosition = 0x0000;

    u16FeeBlockId           = WE_u8GetBlockId(u16Address);
    u16FeeBlockBasePosition = WE_u8GetBlockBasePosition(u16FeeBlockId);
    u16FeeBlockTargetPosition =u16Address - u16FeeBlockBasePosition;

    *u16BlockId = u16FeeBlockId;
    *u16Position =u16FeeBlockTargetPosition;

}

uint16_t WE_u8GetBlockId(uint16_t u16Address)
{

	MemBlockId_t tRetBlockId = 0x0000;

	if ((EEP_FW_SEGMENT <= u16Address) && (EEP_LOGIC_SEGMENT > u16Address))
	{
		/* In FW segment range */
		tRetBlockId = MEM_FEE_BLK_1;
	}
	else if ((EEP_LOGIC_SEGMENT <= u16Address) && (EEP_CAN_SEGMENT > u16Address))
	{
		/* In LOGIC segment range */
		tRetBlockId = MEM_FEE_BLK_2;
	}
	else if ((EEP_CAN_SEGMENT <= u16Address) && (EEP_FS_SEGMENT > u16Address))
	{
		/* In CAN segment range */
		tRetBlockId = MEM_FEE_BLK_3;
	}
	else if ((EEP_FS_SEGMENT <= u16Address) && (EEP_MOC_SEGMENT > u16Address))
	{
		/* In FS segment range */
		tRetBlockId = MEM_FEE_BLK_4;
	}
	else if ((EEP_MOC_SEGMENT <= u16Address) && (EEP_AHB_SEGMENT > u16Address))
	{
		/* In MOC segment range */
		tRetBlockId = MEM_FEE_BLK_5;
	}
	else if ((EEP_AHB_SEGMENT <= u16Address) && (EEP_DDS_SEGMENT > u16Address))
	{
		/* In AHB segment range */
		tRetBlockId = MEM_FEE_BLK_6;
	}
	else if ((EEP_DDS_SEGMENT <= u16Address) && (EEP_END_SEGMENT > u16Address))
	{
		/* In DDS segment range */
		tRetBlockId = MEM_FEE_BLK_7;
	}
	else
	{

		tRetBlockId = 0xFFFF;
	}

	return (uint16_t)tRetBlockId;
}

uint16_t WE_u8GetBlockBasePosition(MemBlockId_t u16BlockId)
{
	uint16_t u16RegBasePosition = 0x0000;

	switch (u16BlockId)
	{
		case (MEM_FEE_BLK_1):
			u16RegBasePosition = EEP_FW_SEGMENT;
			break;


		case (MEM_FEE_BLK_2):
			u16RegBasePosition = EEP_LOGIC_SEGMENT;
			break;

		case (MEM_FEE_BLK_3):
			u16RegBasePosition = EEP_CAN_SEGMENT;
			break;

		case (MEM_FEE_BLK_4):
			u16RegBasePosition = EEP_FS_SEGMENT;
			break;

		case (MEM_FEE_BLK_5):
			u16RegBasePosition = EEP_MOC_SEGMENT;
			break;

		case (MEM_FEE_BLK_6):
			u16RegBasePosition = EEP_AHB_SEGMENT;
			break;

		case (MEM_FEE_BLK_7):
			u16RegBasePosition = EEP_DDS_SEGMENT;
			break;

		default:
			u16RegBasePosition = 0xFFFF;
			break;
	}
	return u16RegBasePosition;
}

void WE_vFeeInit()
{
	Nvm_Hndlr_Init();
}

void WE_vFeeMain()
{
    //NvM_MainFunction();// 140916 KCLim: Fee
	Fee_MainFunction();
	Fls_17_Pmu_MainFunction();
#if (__FEE_DTC_TEST == ENABLE)/* KCLim_Fee : For TC27x Fee*/
    Test_DtcArray();
#endif /* #if (__FEE_DTC_TEST == ENABLE) */

}

void WE_vFeeReadAll()
{

#if (__FEE_DTC_TEST == ENABLE)/* KCLim_Fee : For TC27x Fee*/
    Test_DtcArray();
#endif /* #if (__FEE_DTC_TEST == ENABLE) */
}

void WE_vFeeWriteAll()
{
    (void)NvM_WriteBlock(MEM_FEE_BLK_2, &NvMIf_LogicEepData.LogicBlock[0]);
#if (0)
		if (MEM_OK_JOB == weFeeSetJobRet)
		{
			/* Write Job request accepted */
			do{
				NvM_MainFunction();
				weFeeGetJobRet = Mem_GetJobStatus(u16BlockId, /*MEM_WRITE_READY_U8*/0x30U);
				u8WriteDoneInteration++;
			}while((MEM_OK_JOB != weFeeGetJobRet) && (FEE_WRITEALL_COUNT > u8WriteDoneInteration));

			if ((MEM_OK_JOB != weFeeGetJobRet) || (FEE_WRITEALL_COUNT < u8WriteDoneInteration))
			{
				Fee_WriteAllFailBlock = (Fee_WriteAllFailBlock | (0x01 << u16BlockId));
			}
			else
			{
				/* OK Write Block Completed */
			}
		}
		else
		{
			/* Write Job request not accepted */
			/*
			 * To Do : Error Handling
			 * */
		}
#endif
}

#if (__FEE_DTC_TEST == ENABLE)/* KCLim_Fee : For TC27x Fee*/

uint8_t DTCTest_u8DTCData[10][32];
uint8_t DTCTest_u8DTCPtrData[12];
uint8_t DTCTest_u8DTCSystemCycle[4];
uint8_t DTCTest_u8ErrorCode[96];
uint8_t DTCTest_u8Error2Code[96];

void Test_DtcArray()
{

	uint16_t u16i = 0x0000;
	uint16_t u16j = 0x0000;
	uint16_t u16k = 0x0000;
	for (u16i = 0x0000; u16i<10; u16i++)
	{

		for (u16j = 0x0000; u16j<32; u16j++)
		{
			//DTCTest_u8DTCData[u16j][u16i] = MemParameter4[u16k];
			DTCTest_u8DTCData[u16i][u16j] = MemConfigList[3].RamAddr_u8[u16k];
			u16k++;
		}

	}
	for (u16i = 0x0000; u16i<12; u16i++)
	{
		DTCTest_u8DTCPtrData[u16i] = MemConfigList[3].RamAddr_u8[u16k];
		u16k++;
	}
	for (u16i = 0x0000; u16i<4; u16i++)
	{
		DTCTest_u8DTCSystemCycle[u16i] = MemConfigList[3].RamAddr_u8[u16k];
		u16k++;
	}
	for (u16i = 0x0000; u16i<96; u16i++)
	{
		DTCTest_u8ErrorCode[u16i] = MemConfigList[3].RamAddr_u8[u16k];
		u16k++;
	}
	for (u16i = 0x0000; u16i<96; u16i++)
	{
		DTCTest_u8Error2Code[u16i] = MemConfigList[3].RamAddr_u8[u16k];
		u16k++;
	}

}
#endif /* #if (__FEE_DTC_TEST == ENABLE) */

#endif /* #if (TC27X_FEE == ENABLE) */

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
