/**
 * @defgroup Nvm_Types Nvm_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Nvm_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef NVM_TYPES_H_
#define NVM_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
#define NVM_BLOCK_SIZE_FW       MEM_BLK1_SIZE  /* EEP_FW_SEGMENT    : 256 byte */
#define NVM_BLOCK_SIZE_LOGIC    MEM_BLK2_SIZE  /* EEP_LOGIC_SEGMENT : 512 byte */
#define NVM_BLOCK_SIZE_CAN      MEM_BLK3_SIZE  /* EEP_CAN_SEGMENT   : 256 byte */
#define NVM_BLOCK_SIZE_FS       MEM_BLK4_SIZE  /* EEP_FS_SEGMENT    : 768 byte */
#define NVM_BLOCK_SIZE_MOC      MEM_BLK5_SIZE  /* EEP_MOC_SEGMENT   : 128 byte */
#define NVM_BLOCK_SIZE_AHB      MEM_BLK6_SIZE  /* EEP_AHB_SEGMENT   : 128 byte */
#define NVM_BLOCK_SIZE_DDS      MEM_BLK7_SIZE  /* EEP_DDS_SEGMENT   : 256 byte */

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Mom_HndlrEcuModeSts_t Nvm_HndlrEcuModeSts;
    Eem_SuspcDetnFuncInhibitNvmSts_t Nvm_HndlrFuncInhibitNvmSts;

/* Output Data Element */
    Nvm_HndlrLogicEepDataInfo_t Nvm_HndlrLogicEepDataInfo;
}Nvm_Hndlr_HdrBusType;
typedef enum
{
    MEN_NO_JOB,
    MEM_OK_JOB,
    MEM_BUSY_JOB,
    MEM_PENDING_JOB,
    MEM_ERROR_JOB
}MemJobState_t;

typedef enum
{
    NVM_BLOCK_FW    ,
    NVM_BLOCK_LOGIC ,
    NVM_BLOCK_CAN   ,
    NVM_BLOCK_FS    ,
    NVM_BLOCK_MOC   ,
    NVM_BLOCK_AHB    ,
    NVM_BLOCK_DDS   ,
    NNM_NUM_OF_BLOCKS
}NvM_BlockIdType;

typedef enum
{
    MEM_NO_HW_ERROR,         /* no hardware error occurred */
    MEM_HW_ERROR_OCCURRED    /* hardware error occurred */
}MemHWError_t;
/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* NVM_TYPES_H_ */
/** @} */
