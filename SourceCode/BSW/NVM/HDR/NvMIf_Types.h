/**
 * @defgroup NvMIf_Types NvMIf_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        NvMIf_Types.h
 * @brief       Template file
 * @date        2014. 11. 25.
 ******************************************************************************/

#ifndef NVMIF_TYPES_H_
#define NVMIF_TYPES_H_

#ifndef STRINGIFY
#define STRINGIFY(x) #x
#endif /* STRINGIFY */
/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "NvM_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
//@@SR ASW_Types.h Need to delete
#if 0
typedef signed char             int8_t;
typedef unsigned char           uint8_t;
typedef volatile signed char    vint8_t;
typedef volatile unsigned char  vuint8_t;
typedef unsigned long uBitType;

typedef signed short            int16_t;
typedef unsigned short          uint16_t;
typedef volatile signed short   vint16_t;
typedef volatile unsigned short vuint16_t;

typedef signed int              int32_t;
typedef unsigned int            uint32_t;
typedef volatile signed int     vint32_t;
typedef volatile unsigned int   vuint32_t;

typedef unsigned char       uchar8_t;
typedef signed char         char8_t;
typedef short int           char16_t;
typedef long int            char32_t;

typedef unsigned short int  uchar16_t;
typedef unsigned long int   uchar32_t;

typedef signed char         CHAR;
typedef short int           INT;
typedef long int            LONG;
typedef unsigned char       UCHAR;
typedef unsigned short int  UINT;
typedef unsigned long int   ULONG;

typedef short int           Frac16;
typedef signed long         Frac32;

struct bit_position 
{
    uBitType bit7    :1;
    uBitType bit6    :1;
    uBitType bit5    :1;
    uBitType bit4    :1;
    uBitType bit3    :1;
    uBitType bit2    :1;
    uBitType bit1    :1;
    uBitType bit0    :1;
};


typedef union
{
    uint8_t R;
    struct
    {
        uBitType bit7    :1;
        uBitType bit6    :1;
        uBitType bit5    :1;
        uBitType bit4    :1;
        uBitType bit3    :1;
        uBitType bit2    :1;
        uBitType bit1    :1;
        uBitType bit0    :1;
    }B;
}t_u8Bit;

typedef union
{
    uint16_t R;
    struct
    {
        uBitType bit15   :1;
        uBitType bit14   :1;
        uBitType bit13   :1;
        uBitType bit12   :1;
        uBitType bit11   :1;
        uBitType bit10   :1;
        uBitType bit9    :1;
        uBitType bit8    :1;
        uBitType bit7    :1;
        uBitType bit6    :1;
        uBitType bit5    :1;
        uBitType bit4    :1;
        uBitType bit3    :1;
        uBitType bit2    :1;
        uBitType bit1    :1;
        uBitType bit0    :1;
    }B;
}t_u16Bit;


typedef struct
{
    uBitType bit15   :1;
    uBitType bit14   :1;
    uBitType bit13   :1;
    uBitType bit12   :1;
    uBitType bit11   :1;
    uBitType bit10   :1;
    uBitType bit9    :1;
    uBitType bit8    :1;
    uBitType bit7    :1;
    uBitType bit6    :1;
    uBitType bit5    :1;
    uBitType bit4    :1;
    uBitType bit3    :1;
    uBitType bit2    :1;
    uBitType bit1    :1;
    uBitType bit0    :1;
}t_u16Bit_Flg;



//MGH80 EPB

struct Epb_8BIT_FLG
{
    uBitType BIT_0      :1;
    uBitType BIT_1      :1;
    uBitType BIT_2      :1;
    uBitType BIT_3      :1;
    uBitType BIT_4      :1;
    uBitType BIT_5      :1;
    uBitType BIT_6      :1;
    uBitType BIT_7      :1;
};

typedef struct
{
    uBitType BIT_0      :1;
    uBitType BIT_1      :1;
    uBitType BIT_2      :1;
    uBitType BIT_3      :1;
    uBitType BIT_4      :1;
    uBitType BIT_5      :1;
    uBitType BIT_6      :1;
    uBitType BIT_7      :1;
}Epb_8BIT_FLG_t;


struct Epb_16BIT_FLG
{
    uBitType BIT_8      :1;
    uBitType BIT_9      :1;
    uBitType BIT_10     :1;
    uBitType BIT_11     :1;
    uBitType BIT_12     :1;
    uBitType BIT_13     :1;
    uBitType BIT_14     :1;
    uBitType BIT_15     :1;
    
    uBitType BIT_0      :1;
    uBitType BIT_1      :1;
    uBitType BIT_2      :1;
    uBitType BIT_3      :1;
    uBitType BIT_4      :1;
    uBitType BIT_5      :1;
    uBitType BIT_6      :1;
    uBitType BIT_7      :1;
};


typedef struct 
{
    uBitType u1Bit0      :1;
    uBitType u1Bit1      :1;
    uBitType u1Bit2      :1;
    uBitType u1Bit3      :1;
    uBitType u1Bit4      :1;
    uBitType u1Bit5      :1;
    uBitType u1Bit6      :1;
    uBitType u1Bit7      :1;
}BIT_STRUCT_t;

typedef struct
{
    uint16_t bit0 :1;
    uint16_t bit1 :1;
    uint16_t bit2 :1;
    uint16_t bit3 :1;
    uint16_t bit4 :1;
    uint16_t bit5 :1;
    uint16_t bit6 :1;
    uint16_t bit7 :1;
} U8_BIT_STRUCT_t;

typedef struct
{
    uBitType bit31    :1;
    uBitType bit30    :1;
    uBitType bit29    :1;
    uBitType bit28    :1;
    uBitType bit27    :1;
    uBitType bit26    :1;
    uBitType bit25    :1;
    uBitType bit24    :1;
    uBitType bit23    :1;
    uBitType bit22    :1;
    uBitType bit21    :1;
    uBitType bit20    :1;
    uBitType bit19    :1;
    uBitType bit18    :1;
    uBitType bit17    :1;
    uBitType bit16    :1;
    uBitType bit15    :1;
    uBitType bit14    :1;
    uBitType bit13    :1;
    uBitType bit12    :1;
    uBitType bit11    :1;
    uBitType bit10    :1;
    uBitType bit9     :1;
    uBitType bit8     :1;
    uBitType bit7     :1;
    uBitType bit6     :1;
    uBitType bit5     :1;
    uBitType bit4     :1;
    uBitType bit3     :1;
    uBitType bit2     :1;
    uBitType bit1     :1;
    uBitType bit0     :1;
}U32_BIT_STRUCT_t;
#endif
#if 0
typedef union
{
    uint8_t R;
    struct
    {
        uBitType bit7    :1;
        uBitType bit6    :1;
        uBitType bit5    :1;
        uBitType bit4    :1;
        uBitType bit3    :1;
        uBitType bit2    :1;
        uBitType bit1    :1;
        uBitType bit0    :1;
    }B;
}t_u8Bit;

/* EEPROM Logic Block typedef */
typedef struct
{
    int32_t PdtOffsEolReadVal;
    int32_t PdfOffsEolReadVal;
    int32_t PdtOffsDrvgReadVal;
    int32_t PdfOffsDrvgReadVal;
} Asw_PedlTrvlOffsEepInfo_t;

typedef struct
{
    int32_t PedlSimrPOffsEolReadVal;
    int32_t PedlSimrPOffsDrvgReadVal;
} Asw_PedlSimrPOffsEepInfo_t;

typedef struct
{
    int32_t PistPOffsEolReadVal;
    int32_t PistPOffsDrvgReadVal;
} Asw_PistPOffsEepInfo_t;

typedef struct
{
    int32_t PrimCircPOffsEolReadVal;
    int32_t PrimCircPOffsDrvgReadVal;
    int32_t SecdCircPOffsEolReadVal;
    int32_t SecdCircPOffsDrvgReadVal;
} Asw_CircPOffsEepInfo_t;

//typedef struct
//{
//    int32_t PedlTrvlOffsEolCalcReqFlg;
//    int32_t PSnsrOffsEolCalcReqFlg;
//} Asw_IdbSnsrOffsEolCalcReqByDiagInfo_t;

//typedef struct
//{
//    int32_t PdfOffsEolEepWrReqFlg;
//    int32_t PdtOffsEolEepWrReqFlg;
//    int32_t PedlSimrPOffsEolEepWrReqFlg;
//    int32_t PistPOffsEolEepWrReqFlg;
//    int32_t PrimCircPOffsEolEepWrReqFlg;
//    int32_t SecdCircPOffsEolEepWrReqFlg;
//    int32_t PdfOffsDrvgEepWrReqFlg;
//    int32_t PdtOffsDrvgEepWrReqFlg;
//    int32_t PedlSimrPOffsDrvgEepWrReqFlg;
//    int32_t PistPOffsDrvgEepWrReqFlg;
//    int32_t PrimCircPOffsDrvgEepWrReqFlg;
//    int32_t SecdCircPOffsDrvgEepWrReqFlg;
//} Asw_IdbSnsrOffsEepWrReqInfo_t;

typedef struct
{
    /*ESC Sensor Offset Eep Data*/
    int32_t KSTEER_offset_of_eeprom;
    int32_t KYAW_offset_of_eeprom;
    int32_t KLAT_offset_of_eeprom;
    int32_t fs_yaw_eeprom_offset;
    int32_t KYAW_eep_max;
    int32_t KYAW_eep_min;
    int32_t KSTEER_eep_max;
    int32_t KSTEER_eep_min;
    int32_t KLAT_eep_max;
    int32_t KLAT_eep_min;
    int32_t yaw_still_eep_max;
    int32_t yaw_still_eep_min;
    int32_t lsesps16EEPYawStandOfs;
    int32_t YawTmpEEPMap[31];

    int32_t AdpvVehMdlVchEep;
    int32_t AdpvVehMdlVcrtRatEep;

    /* TCS DiscTemp Eep Data */
    int32_t btc_tmp_fl_Eep;
    int32_t btc_tmp_fr_Eep;
    int32_t btc_tmp_rl_Eep;
    int32_t btc_tmp_rr_Eep;
    int32_t ee_btcs_data_Eep;

    /* Ax Sensor Offset Eee Data*/

    int32_t LgtSnsrEolOffsEep;
    int32_t LgtSnsrDrvgOffsEep;

}Asw_ActvBrkCtrlEepInfo_t;

typedef struct
{
    Asw_PedlTrvlOffsEepInfo_t PedlTrvlOffsEepInfo;
    Asw_PedlSimrPOffsEepInfo_t PedlSimrPOffsEepInfo;
    Asw_PistPOffsEepInfo_t PistPOffsEepInfo;
    Asw_CircPOffsEepInfo_t CircPOffsEepInfo;
//    Asw_IdbSnsrOffsEolCalcReqByDiagInfo_t IdbSnsrOffsEolCalcReqByDiagInfo;
//    Asw_IdbSnsrOffsEepWrReqInfo_t IdbSnsrOffsEepWrReqInfo;
    Asw_ActvBrkCtrlEepInfo_t Asw_ActvBrkCtrlEepInfo;
    int32_t ReadInvldFlg;       // CGH : It's Written by NvM
    int32_t WrReqCmpldFlg;      // CGH : It's Written by NvM
} Asw_LogicBlock_t;
#endif
//@@SR ASW_Types.h Need to delete

typedef union
{
    uint8 LogicBlock[512];
    Asw_LogicBlock_t Eep;
} NvMIf_LogicBlockType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* NVMIF_TYPES_H_ */
/** @} */
