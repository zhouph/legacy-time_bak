/**
 * @defgroup Nvm_Hndlr Nvm_Hndlr
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Nvm_Hndlr.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef NVM_HNDLR_H_
#define NVM_HNDLR_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Nvm_Types.h"
#include "Nvm_Cfg.h"
#include "Nvm_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define NVM_HNDLR_MODULE_ID      (0)
 #define NVM_HNDLR_MAJOR_VERSION  (2)
 #define NVM_HNDLR_MINOR_VERSION  (0)
 #define NVM_HNDLR_PATCH_VERSION  (0)
 #define NVM_HNDLR_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Nvm_Hndlr_HdrBusType Nvm_HndlrBus;

/* Version Info */
extern const SwcVersionInfo_t Nvm_HndlrVersionInfo;

/* Input Data Element */
extern Mom_HndlrEcuModeSts_t Nvm_HndlrEcuModeSts;
extern Eem_SuspcDetnFuncInhibitNvmSts_t Nvm_HndlrFuncInhibitNvmSts;

/* Output Data Element */
extern Nvm_HndlrLogicEepDataInfo_t Nvm_HndlrLogicEepDataInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Nvm_Hndlr_Init(void);
extern void Nvm_Hndlr(void);
extern void NvM_ReadAll(void);
extern void NvM_WriteAll(void);
extern uint8 NvM_WriteBlock(NvM_BlockIdType BlockId, const void* NvM_SrcPtr);
extern uint8 NvM_ReadBlock(NvM_BlockIdType BlockId, void* NvM_DstPtr);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* NVM_HNDLR_H_ */
/** @} */
