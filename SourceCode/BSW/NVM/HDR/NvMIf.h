/**
 * @defgroup NvMIf NvMIf
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        NvMIf.h
 * @brief       SWC Header File(with Scheduled functions)
 * @date        20YY. MM. DD.
 ******************************************************************************/
 
#ifndef NVMIF_H_
#define NVMIF_H_

/*==============================================================================
 *                  INCLUDE FILES
 ==============================================================================*/
#include "Asw_Types.h"
#include "NvMIf_Types.h"
#include "WTypedefs.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/*EEP address paramenters*/
#define E2P_START_ADDR  0x0000
#define E2P_SIZE        0x1000
#define E2P_MAGIC_NUM   0xAA55
#define FREEZE_FRAME_ADD
//#define TC27X_FEE ENABLE /* KCLim Temp*/

#if (TC27X_FEE == ENABLE) /* KCLim_Fee : For TC27x Fee*/
#define FEE_WRITEALL_COUNT 10U
#endif /* #if (TC27X_FEE == ENABLE) */

#define EEP_FW_SEGMENT                          0x0000  /* 256 byte */
#define EEP_LOGIC_SEGMENT                       0x0100  /* 512 byte */
#define EEP_CAN_SEGMENT                         0x0300  /* 256 byte */
#define EEP_FS_SEGMENT                          0x0400  /* 768 byte */
#define EEP_MOC_SEGMENT                         0x0700  /* 128 byte */
#define EEP_AHB_SEGMENT                         0x0780  /* 128 byte */
#define EEP_DDS_SEGMENT                         0x0800  /* 256 byte */
#define EEP_END_SEGMENT                         0x0900 /* KCLim_Fee */

/* FW EEPROM DATA */
#define U16_EEP_ECU_PRODUCT_DATA_ADDR           (0x40 + EEP_FW_SEGMENT)    /* 0x40 */
#define U16_EEP_ECU_PRODUCT_DATA_SIZE           (48)

#define U16_EEP_SEED_KEY_DATA                   (0x50 + EEP_FW_SEGMENT)

#define U16_EEP_ECU_HW_VERSION_DATA             (0x60 + EEP_FW_SEGMENT)

#define U16_EEP_ECU_HW_VERSION_DATA_1           (0x64 + EEP_FW_SEGMENT)  

#define U16_EEP_BBOX_CNT_ADDR                   (0x68 + EEP_FW_SEGMENT)   
#define U16_EEP_BBOX_CNT_SIZE                   4

#define U16_EEP_VARIANT_CODE_ADDR               (0x70 + EEP_FW_SEGMENT)
#define U16_EEP_VARIANT_CODE_SIZE               8 

#define U16_EEP_INLINE_CODE_ADDR                (0x78 + EEP_FW_SEGMENT)
#define U16_EEP_INLINE_CODE_SIZE                8


/* LOGIC EEPROM DATA */
#define U16_EEP_AX_DRIVING_OFS_EEP_ADDR         (0x00 + EEP_LOGIC_SEGMENT)
#define U16_EEP_AX_DRIVING_OFS_EEP_SIZE         4

#define U16_EEP_ADAPT_MODEL_VCH_EEP_ADDR        (0x04 + EEP_LOGIC_SEGMENT)
#define U16_EEP_ADAPT_MODEL_VCH_EEP_SIZE        4

#define U16_EEP_ADAPT_MODEL_VELO_EEP_ADDR       (0x08 + EEP_LOGIC_SEGMENT)
#define U16_EEP_ADAPT_MODEL_VELO_EEP_SIZE       4     

#define U16_EEP_YAW_SNSR_SN_ADDR                (0x0C + EEP_LOGIC_SEGMENT)
#define U16_EEP_YAW_SNSR_SN_SIZE                4

#define U16_EEP_YAW_SNSR_SN_ADDR_2              (0x10 + EEP_LOGIC_SEGMENT)
#define U16_EEP_YAW_SNSR_SN_SIZE_2              4


#define U16_EEP_BCP_OFS_EEP_ADDR                (0x20 + EEP_LOGIC_SEGMENT) /* 0x120 */
#define U16_EEP_BCP_OFS_EEP_SIZE                4  
    
#define U16_EEP_BC2P_OFS_EEP_ADDR                (0x24 + EEP_LOGIC_SEGMENT)
#define U16_EEP_BC2P_OFS_EEP_SIZE                4

#define U16_EEP_PSP_OFS_EEP_ADDR                (0x28 + EEP_LOGIC_SEGMENT)
#define U16_EEP_PSP_OFS_EEP_SIZE                4

#define U16_EEP_BCP_DRIVING_OFS_EEP_ADDR        (0x2c + EEP_LOGIC_SEGMENT)
#define U16_EEP_BCP_DRIVING_OFS_EEP_SIZE        4  
    
#define U16_EEP_BC2P_DRIVING_OFS_EEP_ADDR        (0x30 + EEP_LOGIC_SEGMENT) /* 0x130 */
#define U16_EEP_BC2P_DRIVING_OFS_EEP_SIZE        4

#define U16_EEP_PSP_DRIVING_OFS_EEP_ADDR        (0x34 + EEP_LOGIC_SEGMENT)
#define U16_EEP_PSP_DRIVING_OFS_EEP_SIZE        4



#define U16_EEP_BTC_DATA_ADDR                   (0x70 + EEP_LOGIC_SEGMENT)  /* 170 */
#define U16_EEP_BTC_DATA_SIZE                   16


////////////////////////////////////////////////////////////////////////////////////
                    /* Model sensor offset eeprom */
                    /* ((0x80 ~ 0xe0) + EEP_LOGIC_SEGMENT)  ===> 0x180 ~ 0x1e0*/
////////////////////////////////////////////////////////////////////////////////////


	
#define U16_EEP_AX_OFS_EEP_ADDR                 (0xE4 + EEP_LOGIC_SEGMENT)
#define U16_EEP_AX_OFS_EEP_SIZE                 4 

#define U16_EEP_PDT_OFS_EEP_ADDR                (0xE8 + EEP_LOGIC_SEGMENT)
#define U16_EEP_PDT_OFS_EEP_SIZE                4  
    
#define U16_EEP_PDF_OFS_EEP_ADDR                (0xEC + EEP_LOGIC_SEGMENT)
#define U16_EEP_PDF_OFS_EEP_SIZE                4

#define U16_EEP_MP_OFS_EEP_ADDR                 (0xF0 + EEP_LOGIC_SEGMENT)
#define U16_EEP_MP_OFS_EEP_SIZE                 4
#define U16_EEP_PEDAL_OFS_EEP_SIZE              12     


#define U16_EEP_PDT_DRIVING_OFS_EEP_ADDR        (0xF4 + EEP_LOGIC_SEGMENT)
#define U16_EEP_PDT_DRIVING_OFS_EEP_SIZE        4  
    
#define U16_EEP_PDF_DRIVING_OFS_EEP_ADDR        (0xF8 + EEP_LOGIC_SEGMENT)
#define U16_EEP_PDF_DRIVING_OFS_EEP_SIZE        4

#define U16_EEP_MP_DRIVING_OFS_EEP_ADDR         (0xFC + EEP_LOGIC_SEGMENT)
#define U16_EEP_MP_DRIVING_OFS_EEP_SIZE         4
#define U16_EEP_PEDAL_DRIVING_OFS_EEP_SIZE      12


////////////////////////////////////////////////////////////////////////////////////
                /* lsespu16YawTmpAddress EEPROM */
                /* ((0x100 ~ 0x180) + EEP_LOGIC_SEGMENT) === 0x200 ~ 0x2FF*/
////////////////////////////////////////////////////////////////////////////////////

	


/* GMLAN DATA */
#define U16_RSCODE_EEP_ADDR                     (0x10 + EEP_CAN_SEGMENT) /* 0x310 */
#define U16_RSCODE_EEP_SIZE                     12

#define U16_PSCODE_1_EEP_ADDR                   (0x1C + EEP_CAN_SEGMENT)  
#define U16_PSCODE_1_EEP_SIZE                   12 

#define U16_PSCODE_2_EEP_ADDR                   (0x28 + EEP_CAN_SEGMENT)  
#define U16_PSCODE_2_EEP_SIZE                   12

#define U16_VIN_EEP_ADDR                        (0x34 + EEP_CAN_SEGMENT)  
#define U16_VIN_EEP_SIZE                        20

#define U16_HSSUBNET_EEP_ADDR                   (0x48 + EEP_CAN_SEGMENT)  
#define U16_HSSUBNET_EEP_SIZE                   8

#define U16_CESUBNET_EEP_ADDR                   (0x50 + EEP_CAN_SEGMENT)  
#define U16_CESUBNET_EEP_SIZE                   8
    
#define U16_PROGDATE_EEP_ADDR                   (0x58 + EEP_CAN_SEGMENT)  
#define U16_PROGDATE_EEP_SIZE                   4

#define U16_SECURE_CODE_EEP_ADDR                (0x5C + EEP_CAN_SEGMENT) 
#define U16_SECURE_CODE_EEP_SIZE                4
 
#define U16_VTD_EEP_ADDR                        (0x60 + EEP_CAN_SEGMENT)  
#define U16_VTD_EEP_SIZE                        4

#define U16_ECU_CONFIG_EEP_ADDR                 (0x64 + EEP_CAN_SEGMENT)  
#define U16_ECU_CONFIG_EEP_SIZE                 4

#define U16_MEC_EEP_ADDR                        (0x68 + EEP_CAN_SEGMENT)  
#define U16_MEC_EEP_ADDR_SIZE                   4

#define U16_XML_PN_EEP_ADDR                     (0x6C + EEP_CAN_SEGMENT)  /*4byte  */
#define U16_XML_PN_EEP_SIZE                     4

#define U16_XML_DLS_EEP_ADDR                    (0x70 + EEP_CAN_SEGMENT)  /*4byte  */
#define U16_XML_DLS_EEP_SIZE                    4 

#define U16_DELIVERY_CONDI_EEP_ADDR             (0x74 + EEP_CAN_SEGMENT)  /*4byte  */
#define U16_DELIVERY_CONDI_EEP_SIZE             4 

#define U16_EMPN_EEP_ADDR                       (0x78 + EEP_CAN_SEGMENT)  /*4byte  */
#define U16_EMPN_EEP_SIZE                       4  

#define U16_BMPN_EEP_ADDR                       (0x7C + EEP_CAN_SEGMENT)  /*4byte  */
#define U16_BMPN_EEP_SIZE                       4  

#define U16_TRCACE_CHAR_EEP_ADDR                (0x80 + EEP_CAN_SEGMENT)  /*16byte  */
#define U16_TRCACE_CHAR_EEP_SIZE                16

#define U16_EMPN_DLS_EEP_ADDR                   (0x84 + EEP_CAN_SEGMENT)  /*4byte  */
#define U16_EMPN_DLS_EEP_SIZE                   4  

#define U16_BMPN_DLS_EEP_ADDR                   (0x88 + EEP_CAN_SEGMENT)  /*4byte  */
#define U16_BMPN_DLS_EEP_SIZE                   4   

#define U16_BLS_VARIANT_EEP_ADDR                (0x8C + EEP_CAN_SEGMENT)  /*4byte  */
#define U16_BLS_VARIANT_EEP_SIZE                4    

#define U16_EEP_EVP_DRV_TIME_DATA_ADDR          (0x90 + EEP_CAN_SEGMENT)  /*4byte  */
#define U16_EEP_EVP_DRV_TIME_DATA_SIZE          4

#define U16_EEP_EVP_DRV_CNT_DATA_ADDR           (0x94 + EEP_CAN_SEGMENT)  /*4byte  */
#define U16_EEP_EVP_DRV_CNT_DATA_SIZE           4

#define U16_CODE_FINGER_EEP_ADDR                (0x98 + EEP_CAN_SEGMENT)
#define U16_CODE_FINGER_EEP_SIZE                12

#define U16_DATA_FINGER_EEP_ADDR                (0xA4 + EEP_CAN_SEGMENT)
#define U16_DATA_FINGER_EEP_SIZE                12

#define U16_BOOT_FINGER_EEP_ADDR                (0xB0 + EEP_CAN_SEGMENT)
#define U16_BOOT_FINGER_EEP_SIZE                12


/* FS EEPROM DATA */
#ifdef FREEZE_FRAME_ADD
#define U16_EEP_DTC_DATA_ADDR                   (0x00 + EEP_FS_SEGMENT) /* EEP_FS_SEGMENT, 1280 */
#define U16_EEP_DTC_DATA_SIZE                   (320)

#define U16_EEP_DTC_PTR_ADDR                    (0xF0+0x50 + EEP_FS_SEGMENT) /* EEP_FS_SEGMENT + F0, 1520 */
#define U16_EEP_DTC_PTR_SIZE                    (12)

#define U16_EEP_SYSTEM_CYCLE_ADDR               (0xFC +0x50+ EEP_FS_SEGMENT) /* EEP_FS_SEGMENT + FC, 1532 */ 
#define U16_EEP_SYSTEM_CYCLE_SIZE               (4)   

#define U16_EEP_ERROR_CODE_ADDR                 (0x100 +0x50+ EEP_FS_SEGMENT) /* EEP_FS_SEGMENT + 0x100, 1536 */
#define U16_EEP_ERROR_CODE_SIZE                 (96)

#define U16_EEP_ERROR2_CODE_ADDR                (0x160 +0x50+ EEP_FS_SEGMENT) /* EEP_FS_SEGMENT + 0x160, 1632 */
#define U16_EEP_ERROR2_CODE_SIZE                (96)
#else
#define U16_EEP_DTC_DATA_ADDR                   (0x00 + EEP_FS_SEGMENT) /* EEP_FS_SEGMENT, 1280 */
#define U16_EEP_DTC_DATA_SIZE                   (240)

#define U16_EEP_DTC_PTR_ADDR                    (0xF0 + EEP_FS_SEGMENT) /* EEP_FS_SEGMENT + F0, 1520 */
#define U16_EEP_DTC_PTR_SIZE                    (12)

#define U16_EEP_SYSTEM_CYCLE_ADDR               (0xFC + EEP_FS_SEGMENT) /* EEP_FS_SEGMENT + FC, 1532 */ 
#define U16_EEP_SYSTEM_CYCLE_SIZE               (4)   

#define U16_EEP_ERROR_CODE_ADDR                 (0x100 + EEP_FS_SEGMENT) /* EEP_FS_SEGMENT + 0x100, 1536 */
#define U16_EEP_ERROR_CODE_SIZE                 (96)

#define U16_EEP_ERROR2_CODE_ADDR                (0x160 + EEP_FS_SEGMENT) /* EEP_FS_SEGMENT + 0x160, 1632 */
#define U16_EEP_ERROR2_CODE_SIZE                (96)
#endif

/* MOC EEPROM DATA */
#define U16_EEP_MOC_DATA1_ADDR                  (0x00 + EEP_MOC_SEGMENT)
#define U16_EEP_MOC_DATA2_ADDR                  (0x10 + EEP_MOC_SEGMENT)
#define U16_EEP_MOC_DATA3_ADDR                  (0x20 + EEP_MOC_SEGMENT)
#define U16_EEP_MOC_DATA4_ADDR                  (0x30 + EEP_MOC_SEGMENT)
#define U16_EEP_MOC_DATA_SIZE                   16

extern t_u8Bit weEepFlg;
#define weu1EraseSector         weEepFlg.B.bit0
#define weu1WriteSector         weEepFlg.B.bit1
#define weu1ErrEEP              weEepFlg.B.bit2
#define AsicVerInvalidFlg       weEepFlg.B.bit3
#define weu1EEPRamUpdate        weEepFlg.B.bit4

#define EEP_ERROR_BYTE_NO               96
#define ERROR_BYTE_NO                  EEP_ERROR_BYTE_NO
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
    uint8_t EepBuff[4]; 
    int16_t ReadBuff;
}Eep4byteWrite_t;

typedef struct
{
    UINT    u1MocEEPROMWriteReq1        :1;
    UINT    u1MocEEPROMReadFail1         :1;
    UINT    u1MocEEPROMWriteReq2        :1;
    UINT    u1MocEEPROMReadFail2         :1;
    UINT    u1MocEEPROMWriteReq3        :1;
    UINT    u1MocEEPROMReadFail3         :1;
    UINT    u1MocEEPROMWriteReq4        :1;
    UINT    u1MocEEPROMReadFail4         :1;
    UINT    u1LastEEPWrite_flg1         :1;
    UINT    u1LastEEPWrite_flg2         :1;     
    UINT    u1LastEEPWrite_flg3         :1;
    
    UINT    u1EepLogicDataReadFail      :1;
}MOC_EEPROM_FLG_t;
extern MOC_EEPROM_FLG_t                 wbu8MocEepromFlg;
#define wbu1MocEEPROMWriteReq1          wbu8MocEepromFlg.u1MocEEPROMWriteReq1
#define wbu1MocEEPROMReadFail1          wbu8MocEepromFlg.u1MocEEPROMReadFail1
#define wbu1MocEEPROMWriteReq2          wbu8MocEepromFlg.u1MocEEPROMWriteReq2
#define wbu1MocEEPROMReadFail2          wbu8MocEepromFlg.u1MocEEPROMReadFail2
#define wbu1MocEEPROMWriteReq3          wbu8MocEepromFlg.u1MocEEPROMWriteReq3
#define wbu1MocEEPROMReadFail3          wbu8MocEepromFlg.u1MocEEPROMReadFail3 
#define wbu1MocEEPROMWriteReq4          wbu8MocEepromFlg.u1MocEEPROMWriteReq4
#define wbu1MocEEPROMReadFail4          wbu8MocEepromFlg.u1MocEEPROMReadFail4
#define wu1LastEEPWrite_flg1            wbu8MocEepromFlg.u1LastEEPWrite_flg1   
#define wu1LastEEPWrite_flg2            wbu8MocEepromFlg.u1LastEEPWrite_flg2
#define wu1LastEEPWrite_flg3            wbu8MocEepromFlg.u1LastEEPWrite_flg3
#define wu1EepLogicDataReadFail         wbu8MocEepromFlg.u1EepLogicDataReadFail


/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/
 
/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
extern NvMIf_LogicBlockType NvMIf_LogicEepData;



extern uint8_t weu8CopyEepBuff[EEP_ERROR_BYTE_NO];
extern uint8_t weu8CopySuppEepBuff[EEP_ERROR_BYTE_NO];
extern uint16_t weu16EepBuff[2];
extern uint8_t  weu8EepFwData[16];

extern uint16_t weu16Eep_addr;
extern uint8_t  Diag_asic_version;
extern uint8_t  wmu8EcuHwVersion;

/* KCLim NvMIf : Variables defined in FS */
#define U8NUM_SIZE_ERROR_PTR       16
#define U8NUM_DTC_INFORM      32
#define U8NUM_X10_DTC_INFORM  320

#define U8NUM_MAX_ERROR_NUM        10
#define U8NUM_SIZE_ERROR_PTR       16

#define U8_DTC_INFORM_ERRCODE               0
#define U8_DTC_INFORM_FTB                   1
#define U8_DTC_INFORM_SOD                   2
#define U8_DTC_INFORM_IGN_CNT               3
#define U8_DTC_INFORM_OCCNUM                4
#define U8_DTC_INFORM_FLAG                  5
#define U8_DTC_INFORM_WSS_FL                6
#define U8_DTC_INFORM_WSS_FR                7
#define U8_DTC_INFORM_WSS_RL                8
#define U8_DTC_INFORM_WSS_RR                9
#define U8_DTC_INFORM_AX                    10
#define U8_DTC_INFORM_AY                    11
#define U8_DTC_INFORM_YAW                   12
#define U8_DTC_INFORM_SAS                   13
#define U8_DTC_INFORM_HPA                   14
#define U8_DTC_INFORM_SUBCODE               15
#define U8_DTC_INFORM_FLP_RLP               16
#define U8_DTC_INFORM_FRP_RRP               17
#define U8_DTC_INFORM_VDD                   18
#define U8_DTC_INFORM_SENPWR_12V            19
#define U8_DTC_INFORM_SENPWR_5V             20
#define U8_DTC_INFORM_ASIC                  21
#define U8_DTC_INFORM_ACCEL                 22
#define U8_DTC_INFORM_SYS_CYCLE             23
#define U8_DTC_INFORM_BCPP                  24
#define U8_DTC_INFORM_BCPS                  25
#define U8_DTC_INFORM_PSP                   26
#define U8_DTC_INFORM_PDT                   27
#define U8_DTC_INFORM_PDF                   28
#define U8_DTC_INFORM_MTP                   29
#define U8_DTC_INFORM2_FLAG                 30
#define U8_DTC_RESERVED                     31


typedef union
{
    UCHAR ErrMember[U8NUM_DTC_INFORM];
}ErrBuffer_t;


extern uint8            feu8ErrorPointer[U8NUM_SIZE_ERROR_PTR];
extern ErrBuffer_t ErrBuffer[U8NUM_MAX_ERROR_NUM];
/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void WE_vInitEEP(void);
extern uint8_t  WE_u8ReadEEP_Byte(uint16_t e2p_id, uint8_t *ch_ptr, uint16_t num);
extern void     WE_vApplProcessWER(void);
extern uint8_t  WE_vContlEepLoop(void);
extern void NvMCdd_Init(void);


#if (TC27X_FEE == ENABLE) /* KCLim_Fee : For TC27x Fee*/
void WE_vFeeInit();
void WE_vFeeMain();
void WE_vFeeReadAll();
void WE_vFeeWriteAll();
void NvMIf_UpdateInternalBusData();
#endif /* #if (TC27X_FEE == ENABLE) */
/*==============================================================================
 *                  END OF FILE
 ==============================================================================*/
#endif /* NVMIF_H_ */
/** @} */
