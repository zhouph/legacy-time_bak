Nvm_HndlrEcuModeSts = Simulink.Bus;
DeList={'Nvm_HndlrEcuModeSts'};
Nvm_HndlrEcuModeSts = CreateBus(Nvm_HndlrEcuModeSts, DeList);
clear DeList;

Nvm_HndlrFuncInhibitNvmSts = Simulink.Bus;
DeList={'Nvm_HndlrFuncInhibitNvmSts'};
Nvm_HndlrFuncInhibitNvmSts = CreateBus(Nvm_HndlrFuncInhibitNvmSts, DeList);
clear DeList;

Nvm_HndlrLogicEepDataInfo = Simulink.Bus;
DeList={
    'KPdtOffsEolReadVal'
    'KPdfOffsEolReadVal'
    'KPdtOffsDrvgReadVal'
    'KPdfOffsDrvgReadVal'
    'KPedlSimPOffsEolReadVal'
    'KPedlSimPOffsDrvgReadVal'
    'KPistPOffsEolReadVal'
    'KPistPOffsDrvgReadVal'
    'KPrimCircPOffsEolReadVal'
    'KPrimCircPOffsDrvgReadVal'
    'KSecdCircPOffsEolReadVal'
    'KSecdCircPOffsDrvgReadVal'
    'KSteerEepOffs'
    'KYawEepOffs'
    'KLatEepOffs'
    'KFsYawEepOffs'
    'KYawEepMax'
    'KYawEepMin'
    'KSteerEepMax'
    'KSteerEepMin'
    'KLatEepMax'
    'KLatEepMin'
    'KYawStillEepMax'
    'KYawStillEepMin'
    'KYawStandEepOffs'
    'KYawTmpEepMap_array_0'
    'KYawTmpEepMap_array_1'
    'KYawTmpEepMap_array_2'
    'KYawTmpEepMap_array_3'
    'KYawTmpEepMap_array_4'
    'KYawTmpEepMap_array_5'
    'KYawTmpEepMap_array_6'
    'KYawTmpEepMap_array_7'
    'KYawTmpEepMap_array_8'
    'KYawTmpEepMap_array_9'
    'KYawTmpEepMap_array_10'
    'KYawTmpEepMap_array_11'
    'KYawTmpEepMap_array_12'
    'KYawTmpEepMap_array_13'
    'KYawTmpEepMap_array_14'
    'KYawTmpEepMap_array_15'
    'KYawTmpEepMap_array_16'
    'KYawTmpEepMap_array_17'
    'KYawTmpEepMap_array_18'
    'KYawTmpEepMap_array_19'
    'KYawTmpEepMap_array_20'
    'KYawTmpEepMap_array_21'
    'KYawTmpEepMap_array_22'
    'KYawTmpEepMap_array_23'
    'KYawTmpEepMap_array_24'
    'KYawTmpEepMap_array_25'
    'KYawTmpEepMap_array_26'
    'KYawTmpEepMap_array_27'
    'KYawTmpEepMap_array_28'
    'KYawTmpEepMap_array_29'
    'KYawTmpEepMap_array_30'
    'KAdpvVehMdlVchEep'
    'KAdpvVehMdlVcrtRatEep'
    'KFlBtcTmpEep'
    'KFrBtcTmpEep'
    'KRlBtcTmpEep'
    'KRrBtcTmpEep'
    'KEeBtcsDataEep'
    'KLgtSnsrEolEepOffs'
    'KLgtSnsrDrvgEepOffs'
    'ReadInvld'
    'WrReqCmpld'
    };
Nvm_HndlrLogicEepDataInfo = CreateBus(Nvm_HndlrLogicEepDataInfo, DeList);
clear DeList;

