/**
 * @defgroup Nvm_Cfg Nvm_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Nvm_Cfg.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef NVM_CFG_H_
#define NVM_CFG_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Nvm_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
#define CRC16               (2U)
#define CRC16_H             (0U)
#define CRC16_L             (1U)
#define REDUNDANT           (2U)
#define MEM_NO_BLK          (0U)
#define MEM_ONE_BLOCK       (0U)
#define MEM_ALL_READ_BLOCKS (1U)
#define MEM_ALL_WRITE_BLOCKS (2U)

#define TEST_MEMORY (0U)
#define MEM_BLK1_SIZE   (256U)  /* EEP_FW_SEGMENT    : 256 byte */
#define MEM_BLK2_SIZE   (512U)  /* EEP_LOGIC_SEGMENT : 512 byte */
#define MEM_BLK3_SIZE   (256U)  /* EEP_CAN_SEGMENT   : 256 byte */
#define MEM_BLK4_SIZE   (768U)  /* EEP_FS_SEGMENT    : 768 byte */
#define MEM_BLK5_SIZE   (128U)  /* EEP_MOC_SEGMENT   : 128 byte */
#define MEM_BLK6_SIZE   (128U)  /* EEP_AHB_SEGMENT   : 128 byte */
#define MEM_BLK7_SIZE   (256U)  /* EEP_DDS_SEGMENT   : 256 byte */

/* temporary buffer size bigger than the biggest block size */
#define FEE_BUF_SIZE   ((MEM_BLK4_SIZE + CRC16) * (2U)) /* temporary buffer size bigger than the biggest block size */

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

typedef enum
{
    MEM_FEE_BLK_1, /* EEP_FW_SEGMENT  */
    MEM_FEE_BLK_2, /* EEP_LOGIC_SEGMENT  */
    MEM_FEE_BLK_3, /* EEP_CAN_SEGMENT  */
    MEM_FEE_BLK_4, /* EEP_FS_SEGMENT  */
    MEM_FEE_BLK_5, /* EEP_MOC_SEGMENT  */
    MEM_FEE_BLK_6, /* EEP_AHB_SEGMENT  */
    MEM_FEE_BLK_7, /* EEP_DDS_SEGMENT  */
    MEM_NUM_OF_BLOCKS

}MemBlockId_t;

typedef enum
{
    MEM_NATIVE = 0,             /* no redundant */
    MEM_REDUNDANCY = 1          /* single redundant */
} MemRedundancyDepth_t;

typedef enum
{
    MEM_NO_CRC = 0,
    MEM_CRC = 1
}MemCrc_t;

typedef struct
{
    uint16 FeeMemBlockId_u16;                    /* block ID in FEE(DFLASH)*/
    uint16 Length_u16;                         /* number of bytes in block */
    uint8* RamAddr_u8;                           /* target address in app-layer RAM*/
    uint8* MirrorRamAddr_u8;                     /* target address in permanent mirroring RAM*/
    MemRedundancyDepth_t RedundancyDepth_en;     /* depth of redundancy */
    MemCrc_t Crc_en;
    uint8 (*ReadFunction)(void);                 /* copy function for writing to RAM */
    void (*WriteFunction)(void);                 /* copy function for reading from RAM */
} MemConfigEntry_t;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/
extern const MemConfigEntry_t MemConfigList[];

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
uint8 NvM_CopyForRead(void);
uint8 NvM_CopyForReadNoCS(void);
void NvM_CopyForWrite(void);
void NvM_CopyForWriteNoCS(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* NVM_CFG_H_ */
/** @} */
