/**
 * @defgroup Nvm_Cfg Nvm_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Nvm_Cfg.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Nvm_Cfg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
const uint8 MemParameter1[MEM_BLK1_SIZE];
const uint8 MemParameter2[MEM_BLK2_SIZE];
const uint8 MemParameter3[MEM_BLK3_SIZE];
const uint8 MemParameter4[MEM_BLK4_SIZE];
const uint8 MemParameter5[MEM_BLK5_SIZE];
const uint8 MemParameter6[MEM_BLK6_SIZE];
const uint8 MemParameter7[MEM_BLK7_SIZE];

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define NVM_START_SEC_CONST_UNSPECIFIED
#include "Nvm_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/
const MemConfigEntry_t MemConfigList[] =
{
    {MEM_FEE_BLK_1,     MEM_BLK1_SIZE,  (uint8*)&MemParameter1,     NULL_PTR,       MEM_NATIVE,     MEM_CRC,    NvM_CopyForRead,      NvM_CopyForWrite},
    {MEM_FEE_BLK_2,     MEM_BLK2_SIZE,  (uint8*)&MemParameter2,     NULL_PTR,       MEM_NATIVE,     MEM_CRC,    NvM_CopyForRead,      NvM_CopyForWrite},
    {MEM_FEE_BLK_3,     MEM_BLK3_SIZE,  (uint8*)&MemParameter3,     NULL_PTR,       MEM_NATIVE,     MEM_CRC,    NvM_CopyForRead,      NvM_CopyForWrite},
    {MEM_FEE_BLK_4,     MEM_BLK4_SIZE,  (uint8*)&MemParameter4,     NULL_PTR,       MEM_NATIVE,     MEM_CRC,    NvM_CopyForRead,      NvM_CopyForWrite},
    {MEM_FEE_BLK_5,     MEM_BLK5_SIZE,  (uint8*)&MemParameter5,     NULL_PTR,       MEM_NATIVE,     MEM_CRC,    NvM_CopyForRead,      NvM_CopyForWrite},
    {MEM_FEE_BLK_6,     MEM_BLK6_SIZE,  (uint8*)&MemParameter6,     NULL_PTR,       MEM_NATIVE,     MEM_CRC,    NvM_CopyForRead,      NvM_CopyForWrite},
    {MEM_FEE_BLK_7,     MEM_BLK7_SIZE,  (uint8*)&MemParameter7,     NULL_PTR,       MEM_NATIVE,     MEM_CRC,    NvM_CopyForRead,      NvM_CopyForWrite}

};

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define NVM_STOP_SEC_CONST_UNSPECIFIED
#include "Nvm_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define NVM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Nvm_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define NVM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Nvm_MemMap.h"
#define NVM_START_SEC_VAR_NOINIT_32BIT
#include "Nvm_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define NVM_STOP_SEC_VAR_NOINIT_32BIT
#include "Nvm_MemMap.h"
#define NVM_START_SEC_VAR_UNSPECIFIED
#include "Nvm_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define NVM_STOP_SEC_VAR_UNSPECIFIED
#include "Nvm_MemMap.h"
#define NVM_START_SEC_VAR_32BIT
#include "Nvm_MemMap.h"
/** Variable Section (32BIT)**/


#define NVM_STOP_SEC_VAR_32BIT
#include "Nvm_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define NVM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Nvm_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define NVM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Nvm_MemMap.h"
#define NVM_START_SEC_VAR_NOINIT_32BIT
#include "Nvm_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define NVM_STOP_SEC_VAR_NOINIT_32BIT
#include "Nvm_MemMap.h"
#define NVM_START_SEC_VAR_UNSPECIFIED
#include "Nvm_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define NVM_STOP_SEC_VAR_UNSPECIFIED
#include "Nvm_MemMap.h"
#define NVM_START_SEC_VAR_32BIT
#include "Nvm_MemMap.h"
/** Variable Section (32BIT)**/


#define NVM_STOP_SEC_VAR_32BIT
#include "Nvm_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define NVM_START_SEC_CODE
#include "Nvm_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define NVM_STOP_SEC_CODE
#include "Nvm_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
