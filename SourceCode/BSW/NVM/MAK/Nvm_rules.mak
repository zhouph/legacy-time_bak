# \file
#
# \brief Nvm
#
# This file contains the implementation of the SWC
# module Nvm.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Nvm_src

Nvm_src_FILES        += $(Nvm_SRC_PATH)\Nvm_Hndlr.c
Nvm_src_FILES        += $(Nvm_SRC_PATH)\NvmIf.c
Nvm_src_FILES        += $(Nvm_IFA_PATH)\Nvm_Hndlr_Ifa.c
Nvm_src_FILES        += $(Nvm_CFG_PATH)\Nvm_Cfg.c
Nvm_src_FILES        += $(Nvm_CAL_PATH)\Nvm_Cal.c

ifeq ($(ICE_COMPILE),true)
Nvm_src_FILES        += $(Nvm_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	Nvm_src_FILES        += $(Nvm_UNITY_PATH)\unity.c
	Nvm_src_FILES        += $(Nvm_UNITY_PATH)\unity_fixture.c	
	Nvm_src_FILES        += $(Nvm_UT_PATH)\main.c
	Nvm_src_FILES        += $(Nvm_UT_PATH)\Nvm_Hndlr\Nvm_Hndlr_UtMain.c
endif