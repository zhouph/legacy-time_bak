# \file
#
# \brief Nvm
#
# This file contains the implementation of the SWC
# module Nvm.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Nvm_CORE_PATH     := $(MANDO_BSW_ROOT)\Nvm
Nvm_CAL_PATH      := $(Nvm_CORE_PATH)\CAL\$(Nvm_VARIANT)
Nvm_SRC_PATH      := $(Nvm_CORE_PATH)\SRC
Nvm_CFG_PATH      := $(Nvm_CORE_PATH)\CFG\$(Nvm_VARIANT)
Nvm_HDR_PATH      := $(Nvm_CORE_PATH)\HDR
Nvm_IFA_PATH      := $(Nvm_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Nvm_CMN_PATH      := $(Nvm_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	Nvm_UT_PATH		:= $(Nvm_CORE_PATH)\UT
	Nvm_UNITY_PATH	:= $(Nvm_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(Nvm_UT_PATH)
	CC_INCLUDE_PATH		+= $(Nvm_UNITY_PATH)
	Nvm_Hndlr_PATH 	:= Nvm_UT_PATH\Nvm_Hndlr
endif
CC_INCLUDE_PATH    += $(Nvm_CAL_PATH)
CC_INCLUDE_PATH    += $(Nvm_SRC_PATH)
CC_INCLUDE_PATH    += $(Nvm_CFG_PATH)
CC_INCLUDE_PATH    += $(Nvm_HDR_PATH)
CC_INCLUDE_PATH    += $(Nvm_IFA_PATH)
CC_INCLUDE_PATH    += $(Nvm_CMN_PATH)

