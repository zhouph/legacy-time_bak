#define S_FUNCTION_NAME      Nvm_Hndlr_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          2
#define WidthOutputPort         67

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Nvm_Hndlr.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Nvm_HndlrEcuModeSts = input[0];
    Nvm_HndlrFuncInhibitNvmSts = input[1];

    Nvm_Hndlr();


    output[0] = Nvm_HndlrLogicEepDataInfo.KPdtOffsEolReadVal;
    output[1] = Nvm_HndlrLogicEepDataInfo.KPdfOffsEolReadVal;
    output[2] = Nvm_HndlrLogicEepDataInfo.KPdtOffsDrvgReadVal;
    output[3] = Nvm_HndlrLogicEepDataInfo.KPdfOffsDrvgReadVal;
    output[4] = Nvm_HndlrLogicEepDataInfo.KPedlSimPOffsEolReadVal;
    output[5] = Nvm_HndlrLogicEepDataInfo.KPedlSimPOffsDrvgReadVal;
    output[6] = Nvm_HndlrLogicEepDataInfo.KPistPOffsEolReadVal;
    output[7] = Nvm_HndlrLogicEepDataInfo.KPistPOffsDrvgReadVal;
    output[8] = Nvm_HndlrLogicEepDataInfo.KPrimCircPOffsEolReadVal;
    output[9] = Nvm_HndlrLogicEepDataInfo.KPrimCircPOffsDrvgReadVal;
    output[10] = Nvm_HndlrLogicEepDataInfo.KSecdCircPOffsEolReadVal;
    output[11] = Nvm_HndlrLogicEepDataInfo.KSecdCircPOffsDrvgReadVal;
    output[12] = Nvm_HndlrLogicEepDataInfo.KSteerEepOffs;
    output[13] = Nvm_HndlrLogicEepDataInfo.KYawEepOffs;
    output[14] = Nvm_HndlrLogicEepDataInfo.KLatEepOffs;
    output[15] = Nvm_HndlrLogicEepDataInfo.KFsYawEepOffs;
    output[16] = Nvm_HndlrLogicEepDataInfo.KYawEepMax;
    output[17] = Nvm_HndlrLogicEepDataInfo.KYawEepMin;
    output[18] = Nvm_HndlrLogicEepDataInfo.KSteerEepMax;
    output[19] = Nvm_HndlrLogicEepDataInfo.KSteerEepMin;
    output[20] = Nvm_HndlrLogicEepDataInfo.KLatEepMax;
    output[21] = Nvm_HndlrLogicEepDataInfo.KLatEepMin;
    output[22] = Nvm_HndlrLogicEepDataInfo.KYawStillEepMax;
    output[23] = Nvm_HndlrLogicEepDataInfo.KYawStillEepMin;
    output[24] = Nvm_HndlrLogicEepDataInfo.KYawStandEepOffs;
    output[25] = Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[0];
    output[26] = Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[1];
    output[27] = Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[2];
    output[28] = Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[3];
    output[29] = Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[4];
    output[30] = Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[5];
    output[31] = Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[6];
    output[32] = Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[7];
    output[33] = Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[8];
    output[34] = Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[9];
    output[35] = Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[10];
    output[36] = Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[11];
    output[37] = Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[12];
    output[38] = Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[13];
    output[39] = Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[14];
    output[40] = Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[15];
    output[41] = Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[16];
    output[42] = Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[17];
    output[43] = Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[18];
    output[44] = Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[19];
    output[45] = Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[20];
    output[46] = Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[21];
    output[47] = Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[22];
    output[48] = Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[23];
    output[49] = Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[24];
    output[50] = Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[25];
    output[51] = Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[26];
    output[52] = Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[27];
    output[53] = Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[28];
    output[54] = Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[29];
    output[55] = Nvm_HndlrLogicEepDataInfo.KYawTmpEepMap[30];
    output[56] = Nvm_HndlrLogicEepDataInfo.KAdpvVehMdlVchEep;
    output[57] = Nvm_HndlrLogicEepDataInfo.KAdpvVehMdlVcrtRatEep;
    output[58] = Nvm_HndlrLogicEepDataInfo.KFlBtcTmpEep;
    output[59] = Nvm_HndlrLogicEepDataInfo.KFrBtcTmpEep;
    output[60] = Nvm_HndlrLogicEepDataInfo.KRlBtcTmpEep;
    output[61] = Nvm_HndlrLogicEepDataInfo.KRrBtcTmpEep;
    output[62] = Nvm_HndlrLogicEepDataInfo.KEeBtcsDataEep;
    output[63] = Nvm_HndlrLogicEepDataInfo.KLgtSnsrEolEepOffs;
    output[64] = Nvm_HndlrLogicEepDataInfo.KLgtSnsrDrvgEepOffs;
    output[65] = Nvm_HndlrLogicEepDataInfo.ReadInvld;
    output[66] = Nvm_HndlrLogicEepDataInfo.WrReqCmpld;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Nvm_Hndlr_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
