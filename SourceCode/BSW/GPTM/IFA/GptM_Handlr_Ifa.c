/**
 * @defgroup GptM_Handlr_Ifa GptM_Handlr_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        GptM_Handlr_Ifa.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "GptM_Handlr_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define GPTM_HANDLR_START_SEC_CONST_UNSPECIFIED
#include "GptM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define GPTM_HANDLR_STOP_SEC_CONST_UNSPECIFIED
#include "GptM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define GPTM_HANDLR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "GptM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define GPTM_HANDLR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "GptM_MemMap.h"
#define GPTM_HANDLR_START_SEC_VAR_NOINIT_32BIT
#include "GptM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define GPTM_HANDLR_STOP_SEC_VAR_NOINIT_32BIT
#include "GptM_MemMap.h"
#define GPTM_HANDLR_START_SEC_VAR_UNSPECIFIED
#include "GptM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define GPTM_HANDLR_STOP_SEC_VAR_UNSPECIFIED
#include "GptM_MemMap.h"
#define GPTM_HANDLR_START_SEC_VAR_32BIT
#include "GptM_MemMap.h"
/** Variable Section (32BIT)**/


#define GPTM_HANDLR_STOP_SEC_VAR_32BIT
#include "GptM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define GPTM_HANDLR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "GptM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define GPTM_HANDLR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "GptM_MemMap.h"
#define GPTM_HANDLR_START_SEC_VAR_NOINIT_32BIT
#include "GptM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define GPTM_HANDLR_STOP_SEC_VAR_NOINIT_32BIT
#include "GptM_MemMap.h"
#define GPTM_HANDLR_START_SEC_VAR_UNSPECIFIED
#include "GptM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define GPTM_HANDLR_STOP_SEC_VAR_UNSPECIFIED
#include "GptM_MemMap.h"
#define GPTM_HANDLR_START_SEC_VAR_32BIT
#include "GptM_MemMap.h"
/** Variable Section (32BIT)**/


#define GPTM_HANDLR_STOP_SEC_VAR_32BIT
#include "GptM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define GPTM_HANDLR_START_SEC_CODE
#include "GptM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define GPTM_HANDLR_STOP_SEC_CODE
#include "GptM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
