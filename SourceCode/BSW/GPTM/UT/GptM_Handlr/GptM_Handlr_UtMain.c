#include "unity.h"
#include "unity_fixture.h"
#include "GptM_Handlr.h"
#include "GptM_Handlr_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Mom_HndlrEcuModeSts_t UtInput_GptM_HandlrEcuModeSts[MAX_STEP] = GPTM_HANDLRECUMODESTS;




TEST_GROUP(GptM_Handlr);
TEST_SETUP(GptM_Handlr)
{
    GptM_Handlr_Init();
}

TEST_TEAR_DOWN(GptM_Handlr)
{   /* Postcondition */

}

TEST(GptM_Handlr, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        GptM_HandlrEcuModeSts = UtInput_GptM_HandlrEcuModeSts[i];

        GptM_Handlr();

    }
}

TEST_GROUP_RUNNER(GptM_Handlr)
{
    RUN_TEST_CASE(GptM_Handlr, All);
}
