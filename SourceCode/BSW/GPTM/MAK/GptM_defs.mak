# \file
#
# \brief GptM
#
# This file contains the implementation of the SWC
# module GptM.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

GptM_CORE_PATH     := $(MANDO_BSW_ROOT)\GptM
GptM_CAL_PATH      := $(GptM_CORE_PATH)\CAL\$(GptM_VARIANT)
GptM_SRC_PATH      := $(GptM_CORE_PATH)\SRC
GptM_CFG_PATH      := $(GptM_CORE_PATH)\CFG\$(GptM_VARIANT)
GptM_HDR_PATH      := $(GptM_CORE_PATH)\HDR
GptM_IFA_PATH      := $(GptM_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    GptM_CMN_PATH      := $(GptM_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	GptM_UT_PATH		:= $(GptM_CORE_PATH)\UT
	GptM_UNITY_PATH	:= $(GptM_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(GptM_UT_PATH)
	CC_INCLUDE_PATH		+= $(GptM_UNITY_PATH)
	GptM_Handlr_PATH 	:= GptM_UT_PATH\GptM_Handlr
endif
CC_INCLUDE_PATH    += $(GptM_CAL_PATH)
CC_INCLUDE_PATH    += $(GptM_SRC_PATH)
CC_INCLUDE_PATH    += $(GptM_CFG_PATH)
CC_INCLUDE_PATH    += $(GptM_HDR_PATH)
CC_INCLUDE_PATH    += $(GptM_IFA_PATH)
CC_INCLUDE_PATH    += $(GptM_CMN_PATH)

