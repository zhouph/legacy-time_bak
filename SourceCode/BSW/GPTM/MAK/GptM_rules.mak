# \file
#
# \brief GptM
#
# This file contains the implementation of the SWC
# module GptM.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += GptM_src

GptM_src_FILES        += $(GptM_SRC_PATH)\GptM_Handlr.c
GptM_src_FILES        += $(GptM_IFA_PATH)\GptM_Handlr_Ifa.c
GptM_src_FILES        += $(GptM_CFG_PATH)\GptM_Cfg.c
GptM_src_FILES        += $(GptM_CAL_PATH)\GptM_Cal.c
GptM_src_FILES        += $(GptM_SRC_PATH)\GptM_Cdd.c

ifeq ($(ICE_COMPILE),true)
GptM_src_FILES        += $(GptM_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	GptM_src_FILES        += $(GptM_UNITY_PATH)\unity.c
	GptM_src_FILES        += $(GptM_UNITY_PATH)\unity_fixture.c	
	GptM_src_FILES        += $(GptM_UT_PATH)\main.c
	GptM_src_FILES        += $(GptM_UT_PATH)\GptM_Handlr\GptM_Handlr_UtMain.c
endif