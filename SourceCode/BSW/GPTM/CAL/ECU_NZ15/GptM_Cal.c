/**
 * @defgroup GptM_Cal GptM_Cal
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        GptM_Cal.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "GptM_Cal.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define GPTM_START_SEC_CALIB_UNSPECIFIED
#include "GptM_MemMap.h"
/* Global Calibration Section */


#define GPTM_STOP_SEC_CALIB_UNSPECIFIED
#include "GptM_MemMap.h"

#define GPTM_START_SEC_CONST_UNSPECIFIED
#include "GptM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define GPTM_STOP_SEC_CONST_UNSPECIFIED
#include "GptM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define GPTM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "GptM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define GPTM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "GptM_MemMap.h"
#define GPTM_START_SEC_VAR_NOINIT_32BIT
#include "GptM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define GPTM_STOP_SEC_VAR_NOINIT_32BIT
#include "GptM_MemMap.h"
#define GPTM_START_SEC_VAR_UNSPECIFIED
#include "GptM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define GPTM_STOP_SEC_VAR_UNSPECIFIED
#include "GptM_MemMap.h"
#define GPTM_START_SEC_VAR_32BIT
#include "GptM_MemMap.h"
/** Variable Section (32BIT)**/


#define GPTM_STOP_SEC_VAR_32BIT
#include "GptM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define GPTM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "GptM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define GPTM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "GptM_MemMap.h"
#define GPTM_START_SEC_VAR_NOINIT_32BIT
#include "GptM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define GPTM_STOP_SEC_VAR_NOINIT_32BIT
#include "GptM_MemMap.h"
#define GPTM_START_SEC_VAR_UNSPECIFIED
#include "GptM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define GPTM_STOP_SEC_VAR_UNSPECIFIED
#include "GptM_MemMap.h"
#define GPTM_START_SEC_VAR_32BIT
#include "GptM_MemMap.h"
/** Variable Section (32BIT)**/


#define GPTM_STOP_SEC_VAR_32BIT
#include "GptM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define GPTM_START_SEC_CODE
#include "GptM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define GPTM_STOP_SEC_CODE
#include "GptM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
