/**
 * @defgroup GptM_Types GptM_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        GptM_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef GPTM_TYPES_H_
#define GPTM_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Mom_HndlrEcuModeSts_t GptM_HandlrEcuModeSts;

/* Output Data Element */
}GptM_Handlr_HdrBusType;

typedef enum
{
    IfxGpt12_Gpt1BlockPrescaler_8  = 0,  /**< \brief fGPT/8 */
    IfxGpt12_Gpt1BlockPrescaler_4  = 1,  /**< \brief fGPT/4 */
    IfxGpt12_Gpt1BlockPrescaler_32 = 2,  /**< \brief fGPT/32 */
    IfxGpt12_Gpt1BlockPrescaler_16 = 3   /**< \brief fGPT/16 */
} IfxGpt12_Gpt1BlockPrescaler;

typedef enum
{
    IfxGpt12_Mode_timer                                 = 0,  /**< \brief Timer Mode selected */
    IfxGpt12_Mode_counter                               = 1,  /**< \brief Counter Mode selected */
    IfxGpt12_Mode_lowGatedTimer                         = 2,  /**< \brief Low Gated Timer Mode selected */
    IfxGpt12_Mode_highGatedTimer                        = 3,  /**< \brief High Gated Timer Mode selected */
    IfxGpt12_Mode_reload                                = 4,  /**< \brief Reload Mode selected */
    IfxGpt12_Mode_capture                               = 5,  /**< \brief Capture Mode selected */
    IfxGpt12_Mode_incrementalInterfaceRotationDetection = 6,  /**< \brief Incremental Interface Mode selected */
    IfxGpt12_Mode_incrementalInterfaceEdgeDetection     = 7   /**< \brief Incremental Interface Mode selected */
} IfxGpt12_Mode;

typedef enum
{
    IfxGpt12_IncrementalInterfaceInputMode_stopCounterTx        = 0, /**< \brief Counter Tx Stop */
    IfxGpt12_IncrementalInterfaceInputMode_bothEdgesTxIN        = 1, /**< \brief (rising or falling edge) on TxIN */
    IfxGpt12_IncrementalInterfaceInputMode_bothEdgesTxEUD       = 2, /**< \brief (rising or falling edge) on TxEUD */
    IfxGpt12_IncrementalInterfaceInputMode_bothEdgesTxINOrTxEUD = 3  /**< \brief (rising or falling edge) on any Tx input (TxIN or TxEUD) */
} IfxGpt12_IncrementalInterfaceInputMode;

typedef enum
{
    IfxGpt12_TimerDirectionSource_internal = 0,  /**< \brief Timer Dir Control = TxUD (x=2,3,4) */
    IfxGpt12_TimerDirectionSource_external = 1   /**< \brief Timer Dir Control = TxUD (x=2,3,4) */
} IfxGpt12_TimerDirectionSource;

typedef enum
{
    IfxGpt12_TimerDirection_up   = 0, /**< \brief Timer Up Direction selected */
    IfxGpt12_TimerDirection_down = 1  /**< \brief Timer Down Direction selected */
} IfxGpt12_TimerDirection;

typedef enum
{
    IfxGpt12_TimerOutput_Disable   = 0, /**< \brief Timer Up Direction selected */
    IfxGpt12_TimerOutput_Enalbe = 1  /**< \brief Timer Down Direction selected */
} IfxGpt12_TimerOntpurEnDis;

typedef enum
{
    IfxGpt12_TimerRun_stop  = 0, /**< \brief Timer x Stops */
    IfxGpt12_TimerRun_start = 1  /**< \brief Timer X Run */
} IfxGpt12_TimerRun;

typedef enum
{
    IfxGpt12_Input_A = 0,  /**< \brief signal TXINA selected */
    IfxGpt12_Input_B = 1,  /**< \brief signal TXINB selected */
    IfxGpt12_Input_C = 2,  /**< \brief signal TXINC selected */
    IfxGpt12_Input_D = 3   /**< \brief signal TXIND selected */
} IfxGpt12_Input;

typedef enum
{
    IfxGpt12_EudInput_A = 0,  /**< \brief signal TXEUDA selected */
    IfxGpt12_EudInput_B = 1,  /**< \brief signal TXEUDB selected */
    IfxGpt12_EudInput_C = 2,  /**< \brief signal TXEUDC selected */
    IfxGpt12_EudInput_D = 3   /**< \brief signal TXEUDD selected */
} IfxGpt12_EudInput;

typedef enum
{
    IfxGpt12_CaptureInputMode_none            = 0,  /**< \brief None, Counter is disabled */
    IfxGpt12_CaptureInputMode_risingEdgeTxIN  = 1,  /**< \brief rising edge on TxIN */
    IfxGpt12_CaptureInputMode_fallingEdgeTxIN = 2,  /**< \brief falling edge on TxIN */
    IfxGpt12_CaptureInputMode_bothEdgesTxIN   = 3   /**< \brief (rising or falling edge) on TxIN */
} IfxGpt12_CaptureInputMode;

typedef enum
{
	IfxGpt12_TimerT3ClearEnDis_NoEffectofT4EUDontimerT2  	  = 0, /**< \brief Timer Up Direction selected */
	IfxGpt12_TimerT3ClearEnDis_FallingEdgeonT4EUDclearstimerT2 = 1  /**< \brief Timer Down Direction selected */
} IfxGpt12_TimerT2ClearEnDis;

typedef enum
{
	IfxGpt12_TimerT3ClearEnDis_NoEffectofT4INontimerT3  	  = 0, /**< \brief Timer Up Direction selected */
	IfxGpt12_TimerT3ClearEnDis_FallingEdgeonT4INclearstimerT3 = 1  /**< \brief Timer Down Direction selected */
} IfxGpt12_TimerT3ClearEnDis;

typedef enum
{
	IfxGpt12_TimerT4InterruptEnDis_Enable  = 0, /**< \brief Timer Up Direction selected */
	IfxGpt12_TimerT4InterruptEnDis_Disable = 1  /**< \brief Timer Down Direction selected */
} IfxGpt12_TimerT4InterruptEnDis;

typedef enum
{
    IfxGpt12_TimerRemoteControl_off = 0,  /**< \brief T2 RemoteControl Off */
    IfxGpt12_TimerRemoteControl_on  = 1   /**< \brief T2 RemoteControl On */
} IfxGpt12_TimerRemoteControl;
/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* GPTM_TYPES_H_ */
/** @} */
