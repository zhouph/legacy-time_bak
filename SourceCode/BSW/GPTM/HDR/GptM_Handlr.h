/**
 * @defgroup GptM_Handlr GptM_Handlr
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        GptM_Handlr.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef GPTM_HANDLR_H_
#define GPTM_HANDLR_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "GptM_Types.h"
#include "GptM_Cfg.h"
#include "GptM_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define GPTM_HANDLR_MODULE_ID      (0)
 #define GPTM_HANDLR_MAJOR_VERSION  (2)
 #define GPTM_HANDLR_MINOR_VERSION  (0)
 #define GPTM_HANDLR_PATCH_VERSION  (0)
 #define GPTM_HANDLR_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern GptM_Handlr_HdrBusType GptM_HandlrBus;

/* Version Info */
extern const SwcVersionInfo_t GptM_HandlrVersionInfo;

/* Input Data Element */
extern Mom_HndlrEcuModeSts_t GptM_HandlrEcuModeSts;

/* Output Data Element */

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void GptM_Handlr_Init(void);
extern void GptM_Handlr(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* GPTM_HANDLR_H_ */
/** @} */
