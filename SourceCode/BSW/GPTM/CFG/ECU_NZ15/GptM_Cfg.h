/**
 * @defgroup GptM_Cfg GptM_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        GptM_Cfg.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef GPTM_CFG_H_
#define GPTM_CFG_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "GptM_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* GPTM_CFG_H_ */
/** @} */
