/**
 * @defgroup GptM_Handlr GptM_Handlr
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        GptM_Handlr.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "GptM_Handlr.h"
#include "GptM_Handlr_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define GPTM_HANDLR_START_SEC_CONST_UNSPECIFIED
#include "GptM_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define GPTM_HANDLR_STOP_SEC_CONST_UNSPECIFIED
#include "GptM_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define GPTM_HANDLR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "GptM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
GptM_Handlr_HdrBusType GptM_HandlrBus;

/* Version Info */
const SwcVersionInfo_t GptM_HandlrVersionInfo = 
{   
    GPTM_HANDLR_MODULE_ID,           /* GptM_HandlrVersionInfo.ModuleId */
    GPTM_HANDLR_MAJOR_VERSION,       /* GptM_HandlrVersionInfo.MajorVer */
    GPTM_HANDLR_MINOR_VERSION,       /* GptM_HandlrVersionInfo.MinorVer */
    GPTM_HANDLR_PATCH_VERSION,       /* GptM_HandlrVersionInfo.PatchVer */
    GPTM_HANDLR_BRANCH_VERSION       /* GptM_HandlrVersionInfo.BranchVer */
};
    
/* Input Data Element */
Mom_HndlrEcuModeSts_t GptM_HandlrEcuModeSts;

/* Output Data Element */

uint32 GptM_Handlr_Timer_Start;
uint32 GptM_Handlr_Timer_Elapsed;

#define GPTM_HANDLR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "GptM_MemMap.h"
#define GPTM_HANDLR_START_SEC_VAR_NOINIT_32BIT
#include "GptM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define GPTM_HANDLR_STOP_SEC_VAR_NOINIT_32BIT
#include "GptM_MemMap.h"
#define GPTM_HANDLR_START_SEC_VAR_UNSPECIFIED
#include "GptM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define GPTM_HANDLR_STOP_SEC_VAR_UNSPECIFIED
#include "GptM_MemMap.h"
#define GPTM_HANDLR_START_SEC_VAR_32BIT
#include "GptM_MemMap.h"
/** Variable Section (32BIT)**/


#define GPTM_HANDLR_STOP_SEC_VAR_32BIT
#include "GptM_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define GPTM_HANDLR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "GptM_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define GPTM_HANDLR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "GptM_MemMap.h"
#define GPTM_HANDLR_START_SEC_VAR_NOINIT_32BIT
#include "GptM_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define GPTM_HANDLR_STOP_SEC_VAR_NOINIT_32BIT
#include "GptM_MemMap.h"
#define GPTM_HANDLR_START_SEC_VAR_UNSPECIFIED
#include "GptM_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define GPTM_HANDLR_STOP_SEC_VAR_UNSPECIFIED
#include "GptM_MemMap.h"
#define GPTM_HANDLR_START_SEC_VAR_32BIT
#include "GptM_MemMap.h"
/** Variable Section (32BIT)**/


#define GPTM_HANDLR_STOP_SEC_VAR_32BIT
#include "GptM_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define GPTM_HANDLR_START_SEC_CODE
#include "GptM_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void GptM_Handlr_Init(void)
{
    /* Initialize internal bus */
    GptM_HandlrBus.GptM_HandlrEcuModeSts = 0;
}

void GptM_Handlr(void)
{
    GptM_Handlr_Timer_Start = STM0_TIM0.U;

    /* Input */
    GptM_Handlr_Read_GptM_HandlrEcuModeSts(&GptM_HandlrBus.GptM_HandlrEcuModeSts);

    /* Process */

    /* Output */

    GptM_Handlr_Timer_Elapsed = STM0_TIM0.U - GptM_Handlr_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define GPTM_HANDLR_STOP_SEC_CODE
#include "GptM_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
