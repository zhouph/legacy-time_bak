/**
 * @defgroup Gptm_Cdd Gptm_Cdd
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Gptm_Cdd.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
//#include "Gptm_Cdd_Ifa.h"
#include "Mps_TLE5012_Spi.h"
#include "IfxGpt12_reg.h"
#include "Mcal.h"
#include "IfxSrc_regdef.h"
#include "GptM_Types.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
uint16 T3_Timer_Value_Compensated_Flg;

/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
   
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
void IfxGpt12_Config(void);
void IfxGpt12_enableModule(Ifx_GPT12 *gpt12);
void IfxGpt12_setGpt1BlockPrescaler(Ifx_GPT12 *gpt12, IfxGpt12_Gpt1BlockPrescaler bps1);

void IfxGpt12_T2_Config(void);
void IfxGpt12_T2_setMode(Ifx_GPT12 *gpt12, IfxGpt12_Mode mode);
void IfxGpt12_T2_setIncrementalInterfaceInputMode(Ifx_GPT12 *gpt12, IfxGpt12_IncrementalInterfaceInputMode inputMode);
void IfxGpt12_T2_setDirectionSource(Ifx_GPT12 *gpt12, IfxGpt12_TimerDirectionSource source);
void IfxGpt12_T2_setTimerDirection(Ifx_GPT12 *gpt12, IfxGpt12_TimerDirection direction);
void IfxGpt12_T2_run(Ifx_GPT12 *gpt12, IfxGpt12_TimerRun runTimer);
void IfxGpt12_T2_setInput(Ifx_GPT12 *gpt12, IfxGpt12_Input input);
void IfxGpt12_T2_setEudInput(Ifx_GPT12 *gpt12, IfxGpt12_EudInput input);

void IfxGpt12_T3_Config(void);
void IfxGpt12_T3_setMode(Ifx_GPT12 *gpt12, IfxGpt12_Mode mode);
void IfxGpt12_T3_setIncrementalInterfaceInputMode(Ifx_GPT12 *gpt12, IfxGpt12_IncrementalInterfaceInputMode inputMode);
void IfxGpt12_T3_setDirectionSource(Ifx_GPT12 *gpt12, IfxGpt12_TimerDirectionSource source);
void IfxGpt12_T3_setTimerDirection(Ifx_GPT12 *gpt12, IfxGpt12_TimerDirection direction);
void IfxGpt12_T3_run(Ifx_GPT12 *gpt12, IfxGpt12_TimerRun runTimer);
void IfxGpt12_T3_setInput(Ifx_GPT12 *gpt12, IfxGpt12_Input input);
void IfxGpt12_T3_setEudInput(Ifx_GPT12 *gpt12, IfxGpt12_EudInput input);

void IfxGpt12_T4_Config(void);
void IfxGpt12_T4_setMode(Ifx_GPT12 *gpt12, IfxGpt12_Mode mode);
void IfxGpt12_T4_setCaptureInputMode(Ifx_GPT12 *gpt12, IfxGpt12_CaptureInputMode inputMode);
void IfxGpt12_T4_enableClearTimerT2(Ifx_GPT12 *gpt12, IfxGpt12_TimerT2ClearEnDis enabled);
void IfxGpt12_T4_enableClearTimerT3(Ifx_GPT12 *gpt12, IfxGpt12_TimerT3ClearEnDis enabled);
void IfxGpt12_T4_setInterruptEnable(Ifx_GPT12 *gpt12, IfxGpt12_TimerT4InterruptEnDis enabled);
void IfxGpt12_T4_setRemoteControl(Ifx_GPT12 *gpt12, IfxGpt12_TimerRemoteControl control);
void IfxGpt12_T4_run(Ifx_GPT12 *gpt12, IfxGpt12_TimerRun runTimer);
void IfxGpt12_T4_setInput(Ifx_GPT12 *gpt12, IfxGpt12_Input input);
void IfxGpt12_T4_setEudInput(Ifx_GPT12 *gpt12, IfxGpt12_EudInput input);

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
void Gpt12IIF_Init(void);
void MpsD1_Gpt12Iif_CompensateAngle(uint16 MpsD1_SpiAngRawData);
uint16 MpsD1_Gpt12Iif_GetAngle(void);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Gpt12IIF_Init(void)
{
	IfxGpt12_Config();
	IfxGpt12_T2_Config();
	IfxGpt12_T3_Config();
	IfxGpt12_T4_Config();
}

void IfxGpt12_Config(void)
{
	IfxGpt12_enableModule(&MODULE_GPT120);
	IfxGpt12_setGpt1BlockPrescaler(&MODULE_GPT120, IfxGpt12_Gpt1BlockPrescaler_4);
}

void IfxGpt12_enableModule(Ifx_GPT12 *gpt12)
{
	Mcal_ResetENDINIT();
	gpt12->CLC.B.DISR = 0;
	Mcal_SetENDINIT();
}

void IfxGpt12_setGpt1BlockPrescaler(Ifx_GPT12 *gpt12, IfxGpt12_Gpt1BlockPrescaler bps1)
{
    gpt12->T3CON.B.BPS1 = bps1;
}

void IfxGpt12_T2_run(Ifx_GPT12 *gpt12, IfxGpt12_TimerRun runTimer)
{
    gpt12->T2CON.B.T2R = runTimer;
}

void IfxGpt12_T2_Config(void)
{
	IfxGpt12_T2_setMode(&MODULE_GPT120, IfxGpt12_Mode_incrementalInterfaceEdgeDetection);
	IfxGpt12_T2_setIncrementalInterfaceInputMode(&MODULE_GPT120, IfxGpt12_IncrementalInterfaceInputMode_bothEdgesTxINOrTxEUD);
	IfxGpt12_T2_setDirectionSource(&MODULE_GPT120, IfxGpt12_TimerDirectionSource_external);
	IfxGpt12_T2_setTimerDirection(&MODULE_GPT120, IfxGpt12_TimerDirection_up);
	IfxGpt12_T2_run(&MODULE_GPT120, IfxGpt12_TimerRun_start);
	IfxGpt12_T2_setInput(&MODULE_GPT120, IfxGpt12_Input_A);
	IfxGpt12_T2_setEudInput(&MODULE_GPT120, IfxGpt12_EudInput_A);

}

void IfxGpt12_T2_setMode(Ifx_GPT12 *gpt12, IfxGpt12_Mode mode)
{
    gpt12->T2CON.B.T2M = mode;
}

void IfxGpt12_T2_setIncrementalInterfaceInputMode(Ifx_GPT12 *gpt12, IfxGpt12_IncrementalInterfaceInputMode inputMode)
{
    gpt12->T2CON.B.T2I = inputMode;
}

void IfxGpt12_T2_setDirectionSource(Ifx_GPT12 *gpt12, IfxGpt12_TimerDirectionSource source)
{
    gpt12->T2CON.B.T2UDE = source;
}

void IfxGpt12_T2_setTimerDirection(Ifx_GPT12 *gpt12, IfxGpt12_TimerDirection direction)
{
    gpt12->T2CON.B.T2UD = direction;
}

void IfxGpt12_T2_setInput(Ifx_GPT12 *gpt12, IfxGpt12_Input input)
{
    gpt12->PISEL.B.IST2IN = input;
}

void IfxGpt12_T2_setEudInput(Ifx_GPT12 *gpt12, IfxGpt12_EudInput input)
{
    gpt12->PISEL.B.IST2EUD = input;
}

void IfxGpt12_T3_Config(void)
{
	IfxGpt12_T3_setMode(&MODULE_GPT120, IfxGpt12_Mode_incrementalInterfaceEdgeDetection);
	IfxGpt12_T3_setIncrementalInterfaceInputMode(&MODULE_GPT120, IfxGpt12_IncrementalInterfaceInputMode_bothEdgesTxINOrTxEUD);
	IfxGpt12_T3_setDirectionSource(&MODULE_GPT120, IfxGpt12_TimerDirectionSource_external);
	IfxGpt12_T3_setTimerDirection(&MODULE_GPT120, IfxGpt12_TimerDirection_up);
	IfxGpt12_T3_run(&MODULE_GPT120, IfxGpt12_TimerRun_stop);
	IfxGpt12_T3_setInput(&MODULE_GPT120, IfxGpt12_Input_B);
	IfxGpt12_T3_setEudInput(&MODULE_GPT120, IfxGpt12_EudInput_B);
}

void IfxGpt12_T3_setMode(Ifx_GPT12 *gpt12, IfxGpt12_Mode mode)
{
	gpt12->T3CON.B.T3M = mode;
}

void IfxGpt12_T3_setIncrementalInterfaceInputMode(Ifx_GPT12 *gpt12, IfxGpt12_IncrementalInterfaceInputMode inputMode)
{
    gpt12->T3CON.B.T3I = inputMode;
}

void IfxGpt12_T3_setDirectionSource(Ifx_GPT12 *gpt12, IfxGpt12_TimerDirectionSource source)
{
    gpt12->T3CON.B.T3UDE = source;
}

void IfxGpt12_T3_setTimerDirection(Ifx_GPT12 *gpt12, IfxGpt12_TimerDirection direction)
{
    gpt12->T3CON.B.T3UD = direction;
}


void IfxGpt12_T3_run(Ifx_GPT12 *gpt12, IfxGpt12_TimerRun runTimer)
{
    gpt12->T3CON.B.T3R = runTimer;
}

void IfxGpt12_T3_setInput(Ifx_GPT12 *gpt12, IfxGpt12_Input input)
{
    gpt12->PISEL.B.IST3IN = input;
}

void IfxGpt12_T3_setEudInput(Ifx_GPT12 *gpt12, IfxGpt12_EudInput input)
{
    gpt12->PISEL.B.IST3EUD = input;
}

void IfxGpt12_T4_Config(void)
{
	IfxGpt12_T4_setMode(&MODULE_GPT120, IfxGpt12_Mode_capture);
	IfxGpt12_T4_setCaptureInputMode(&MODULE_GPT120, IfxGpt12_CaptureInputMode_fallingEdgeTxIN);
	IfxGpt12_T4_enableClearTimerT2(&MODULE_GPT120, IfxGpt12_TimerT3ClearEnDis_FallingEdgeonT4EUDclearstimerT2);
	IfxGpt12_T4_enableClearTimerT3(&MODULE_GPT120, IfxGpt12_TimerT3ClearEnDis_FallingEdgeonT4INclearstimerT3);
	IfxGpt12_T4_setInterruptEnable(&MODULE_GPT120, IfxGpt12_TimerT4InterruptEnDis_Disable);
	IfxGpt12_T4_setRemoteControl(&MODULE_GPT120, IfxGpt12_TimerRemoteControl_off);
	IfxGpt12_T4_run(&MODULE_GPT120, IfxGpt12_TimerRun_stop);
	IfxGpt12_T4_setInput(&MODULE_GPT120, IfxGpt12_Input_B);
	IfxGpt12_T4_setEudInput(&MODULE_GPT120, IfxGpt12_Input_A);
}

void IfxGpt12_T4_setMode(Ifx_GPT12 *gpt12, IfxGpt12_Mode mode)
{
    gpt12->T4CON.B.T4M = mode;
}

void IfxGpt12_T4_setCaptureInputMode(Ifx_GPT12 *gpt12, IfxGpt12_CaptureInputMode inputMode)
{
    gpt12->T4CON.B.T4I = inputMode;
}

void IfxGpt12_T4_enableClearTimerT2(Ifx_GPT12 *gpt12, IfxGpt12_TimerT2ClearEnDis enabled)
{
    gpt12->T4CON.B.CLRT2EN = enabled ? 1 : 0;
}

void IfxGpt12_T4_enableClearTimerT3(Ifx_GPT12 *gpt12, IfxGpt12_TimerT3ClearEnDis enabled)
{
    gpt12->T4CON.B.CLRT3EN = enabled ? 1 : 0;
}

void IfxGpt12_T4_setInterruptEnable(Ifx_GPT12 *gpt12, IfxGpt12_TimerT4InterruptEnDis enabled)
{
    gpt12->T4CON.B.T4IRDIS = enabled ? 0 : 1;
}

void IfxGpt12_T4_setRemoteControl(Ifx_GPT12 *gpt12, IfxGpt12_TimerRemoteControl control)
{
    gpt12->T4CON.B.T4RC = control;
}

void IfxGpt12_T4_run(Ifx_GPT12 *gpt12, IfxGpt12_TimerRun runTimer)
{
    gpt12->T4CON.B.T4R = runTimer;
}

void IfxGpt12_T4_setInput(Ifx_GPT12 *gpt12, IfxGpt12_Input input)
{
    gpt12->PISEL.B.IST4IN = input;
}

void IfxGpt12_T4_setEudInput(Ifx_GPT12 *gpt12, IfxGpt12_EudInput input)
{
    gpt12->PISEL.B.IST4EUD = input;
}

void MpsD1_Gpt12Iif_CompensateAngle(uint16 MpsD1_SpiAngRawData)
{
	uint16 temp_MpsD1_SpiAngRawData;

	temp_MpsD1_SpiAngRawData = 0;
	temp_MpsD1_SpiAngRawData = MpsD1_SpiAngRawData;
	
	if(T3_Timer_Value_Compensated_Flg == 0)
	{	
		MODULE_GPT120.T3.B.T3 = temp_MpsD1_SpiAngRawData / 2;
		T3_Timer_Value_Compensated_Flg = 1;
	}
	IfxGpt12_T3_run(&MODULE_GPT120, IfxGpt12_TimerRun_start);
}

uint16 MpsD1_Gpt12Iif_GetAngle(void)
{
        static uint16 temp_Gtp12_T3_TimerValue =0;;
        uint16 temp_Gtp12_T3_TimerValue_old;

        temp_Gtp12_T3_TimerValue_old=temp_Gtp12_T3_TimerValue;
        temp_Gtp12_T3_TimerValue = MODULE_GPT120.T3.B.T3;

        if((temp_Gtp12_T3_TimerValue >= 0xC001) && (temp_Gtp12_T3_TimerValue <= 0xFFFF))
        {
            temp_Gtp12_T3_TimerValue = temp_Gtp12_T3_TimerValue - 0xC000;
        }
        else if(
        (temp_Gtp12_T3_TimerValue>0x3FFF)&&(temp_Gtp12_T3_TimerValue<0xC000))
        {
              temp_Gtp12_T3_TimerValue=temp_Gtp12_T3_TimerValue_old;
        }
        else
        {
             ;
        }


	/*
	 * MODULE_GPT120.T3.B.T3
	 * 0x0001(1) -->   0.02 Degree / 0x3FFF(16383) --> 359.97 Degree / 0x3FFF - 0x0001 = 0x3FFE = 16382
	 * 0x0000    -->   0.00 Degree
	 * 0x4000    --> 360.00 Degree = 0.00 Degree, T3 Cleared --> 0x000
	 * 0xC001    -->   0.02 Degree / 0xFFFF(65535) --> 359.97 Degree / 0xFFFF - 0xC001 = 0x3FFE = 16382
	 * 0x0001 == 0xC001 / 0x3FFF == 0xFFFF
	 * 0xC001 - 0xC000 = 0x0001
	 * 0xFFFF - 0xC000 = 0x3FFF
	 */
	 return temp_Gtp12_T3_TimerValue;
}

