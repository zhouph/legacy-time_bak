clc;
clear all;

WORK_DIR = cd;
SRC_DIR = strcat(WORK_DIR,'\..\..\SRC');
IFA_DIR = strcat(WORK_DIR,'\..\..\IFA');
HDR_DIR = strcat(WORK_DIR,'\..\..\HDR');
VAR_DIR = strcat(WORK_DIR,'\..\..\CFG\VariantName');
CAL_DIR = strcat(WORK_DIR,'\..\..\CAL');
CMN_DIR = strcat(WORK_DIR,'\..\..\ICE\CMN');

TAR_FILE={
{SRC_DIR,'GptM_Handlr.c'}
{IFA_DIR,'GptM_Handlr_Ifa.c'}
{IFA_DIR,'GptM_Handlr_Ifa.h'}
{HDR_DIR,'GptM_Handlr.h'}
{HDR_DIR,'GptM_MemMap.h'}
{HDR_DIR,'GptM_Types.h'}
{VAR_DIR,'GptM_Cfg.c'}
{VAR_DIR,'GptM_Cfg.h'}
{CAL_DIR,'GptM_Cal.c'}
{CAL_DIR,'GptM_Cal.h'}
{CMN_DIR,'Bsw_Cfg.h'}
{CMN_DIR,'Bsw_Types.h'}
{CMN_DIR,'Compiler.h'}
{CMN_DIR,'Compiler_Cfg.h'}
{CMN_DIR,'Hal_Types.h'}
{CMN_DIR,'HalDataHandle_Types.h'}
{CMN_DIR,'Platform_Types.h'}
{CMN_DIR,'Sal_Types.h'}
{CMN_DIR,'SalDataHandle_Types.h'}
{CMN_DIR,'Rte_Types.h'}
{CMN_DIR,'RteDataHandle_Types.h'}
{CMN_DIR,'Std_Types.h'}
};

N = length(TAR_FILE);
for i=1:N
    copyfile(fullfile(TAR_FILE{i}{1},TAR_FILE{i}{2}), fullfile(cd));
end

mex -g -v GptM_Handlr_Sfunc.c GptM_Handlr.c GptM_Handlr_Ifa.c GptM_Cfg.c GptM_Cal.c;

for i=1:N
    delete(TAR_FILE{i}{2});
end
