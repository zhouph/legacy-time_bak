#define S_FUNCTION_NAME      Proxy_TxByCom_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          207
#define WidthOutputPort         0

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Proxy_TxByCom.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Proxy_TxByComTxYawCbitInfo.TestYawRateSensor = input[0];
    Proxy_TxByComTxYawCbitInfo.TestAcclSensor = input[1];
    Proxy_TxByComTxYawCbitInfo.YawSerialNumberReq = input[2];
    Proxy_TxByComTxWhlSpdInfo.WhlSpdFL = input[3];
    Proxy_TxByComTxWhlSpdInfo.WhlSpdFR = input[4];
    Proxy_TxByComTxWhlSpdInfo.WhlSpdRL = input[5];
    Proxy_TxByComTxWhlSpdInfo.WhlSpdRR = input[6];
    Proxy_TxByComTxWhlPulInfo.WhlPulFL = input[7];
    Proxy_TxByComTxWhlPulInfo.WhlPulFR = input[8];
    Proxy_TxByComTxWhlPulInfo.WhlPulRL = input[9];
    Proxy_TxByComTxWhlPulInfo.WhlPulRR = input[10];
    Proxy_TxByComTxTcs5Info.CfBrkAbsWLMP = input[11];
    Proxy_TxByComTxTcs5Info.CfBrkEbdWLMP = input[12];
    Proxy_TxByComTxTcs5Info.CfBrkTcsWLMP = input[13];
    Proxy_TxByComTxTcs5Info.CfBrkTcsFLMP = input[14];
    Proxy_TxByComTxTcs5Info.CfBrkAbsDIAG = input[15];
    Proxy_TxByComTxTcs5Info.CrBrkWheelFL_KMH = input[16];
    Proxy_TxByComTxTcs5Info.CrBrkWheelFR_KMH = input[17];
    Proxy_TxByComTxTcs5Info.CrBrkWheelRL_KMH = input[18];
    Proxy_TxByComTxTcs5Info.CrBrkWheelRR_KMH = input[19];
    Proxy_TxByComTxTcs1Info.CfBrkTcsREQ = input[20];
    Proxy_TxByComTxTcs1Info.CfBrkTcsPAS = input[21];
    Proxy_TxByComTxTcs1Info.CfBrkTcsDEF = input[22];
    Proxy_TxByComTxTcs1Info.CfBrkTcsCTL = input[23];
    Proxy_TxByComTxTcs1Info.CfBrkAbsACT = input[24];
    Proxy_TxByComTxTcs1Info.CfBrkAbsDEF = input[25];
    Proxy_TxByComTxTcs1Info.CfBrkEbdDEF = input[26];
    Proxy_TxByComTxTcs1Info.CfBrkTcsGSC = input[27];
    Proxy_TxByComTxTcs1Info.CfBrkEspPAS = input[28];
    Proxy_TxByComTxTcs1Info.CfBrkEspDEF = input[29];
    Proxy_TxByComTxTcs1Info.CfBrkEspCTL = input[30];
    Proxy_TxByComTxTcs1Info.CfBrkMsrREQ = input[31];
    Proxy_TxByComTxTcs1Info.CfBrkTcsMFRN = input[32];
    Proxy_TxByComTxTcs1Info.CfBrkMinGEAR = input[33];
    Proxy_TxByComTxTcs1Info.CfBrkMaxGEAR = input[34];
    Proxy_TxByComTxTcs1Info.BrakeLight = input[35];
    Proxy_TxByComTxTcs1Info.HacCtl = input[36];
    Proxy_TxByComTxTcs1Info.HacPas = input[37];
    Proxy_TxByComTxTcs1Info.HacDef = input[38];
    Proxy_TxByComTxTcs1Info.CrBrkTQI_PC = input[39];
    Proxy_TxByComTxTcs1Info.CrBrkTQIMSR_PC = input[40];
    Proxy_TxByComTxTcs1Info.CrBrkTQISLW_PC = input[41];
    Proxy_TxByComTxTcs1Info.CrEbsMSGCHKSUM = input[42];
    Proxy_TxByComTxSasCalInfo.CalSas_CCW = input[43];
    Proxy_TxByComTxSasCalInfo.CalSas_Lws_CID = input[44];
    Proxy_TxByComTxEsp2Info.LatAccel = input[45];
    Proxy_TxByComTxEsp2Info.LatAccelStat = input[46];
    Proxy_TxByComTxEsp2Info.LatAccelDiag = input[47];
    Proxy_TxByComTxEsp2Info.LongAccel = input[48];
    Proxy_TxByComTxEsp2Info.LongAccelStat = input[49];
    Proxy_TxByComTxEsp2Info.LongAccelDiag = input[50];
    Proxy_TxByComTxEsp2Info.YawRate = input[51];
    Proxy_TxByComTxEsp2Info.YawRateStat = input[52];
    Proxy_TxByComTxEsp2Info.YawRateDiag = input[53];
    Proxy_TxByComTxEsp2Info.CylPres = input[54];
    Proxy_TxByComTxEsp2Info.CylPresStat = input[55];
    Proxy_TxByComTxEsp2Info.CylPresDiag = input[56];
    Proxy_TxByComTxEbs1Info.CfBrkEbsStat = input[57];
    Proxy_TxByComTxEbs1Info.CrBrkRegenTQLimit_NM = input[58];
    Proxy_TxByComTxEbs1Info.CrBrkEstTot_NM = input[59];
    Proxy_TxByComTxEbs1Info.CfBrkSnsFail = input[60];
    Proxy_TxByComTxEbs1Info.CfBrkRbsWLamp = input[61];
    Proxy_TxByComTxEbs1Info.CfBrkVacuumSysDef = input[62];
    Proxy_TxByComTxEbs1Info.CrBrkEstHyd_NM = input[63];
    Proxy_TxByComTxEbs1Info.CrBrkStkDep_PC = input[64];
    Proxy_TxByComTxEbs1Info.CfBrkPgmRun1 = input[65];
    Proxy_TxByComTxAhb1Info.CfAhbWLMP = input[66];
    Proxy_TxByComTxAhb1Info.CfAhbDiag = input[67];
    Proxy_TxByComTxAhb1Info.CfAhbAct = input[68];
    Proxy_TxByComTxAhb1Info.CfAhbDef = input[69];
    Proxy_TxByComTxAhb1Info.CfAhbSLMP = input[70];
    Proxy_TxByComTxAhb1Info.CfBrkAhbSTDep_PC = input[71];
    Proxy_TxByComTxAhb1Info.CfBrkBuzzR = input[72];
    Proxy_TxByComTxAhb1Info.CfBrkPedalCalStatus = input[73];
    Proxy_TxByComTxAhb1Info.CfBrkAhbSnsFail = input[74];
    Proxy_TxByComTxAhb1Info.WhlSpdFLAhb = input[75];
    Proxy_TxByComTxAhb1Info.WhlSpdFRAhb = input[76];
    Proxy_TxByComTxAhb1Info.CrAhbMsgChkSum = input[77];
    Proxy_TxByComSenPwrMonitorData.SenPwrM_5V_Stable = input[78];
    Proxy_TxByComSenPwrMonitorData.SenPwrM_12V_Stable = input[79];
    Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.QU_TAR_REPAT_XTRQ_FTAX_BAX_ACT = input[80];
    Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.TAR_REPAT_XTRQ_FTAX_BAX_ACT = input[81];
    Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.TAR_WMOM_PT_SUM_STAB_FAST = input[82];
    Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.ST_ECU_RQ_WMOM_PT_SUM_STAB = input[83];
    Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.QU_RQ_ILK_PT = input[84];
    Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.QU_TAR_WMOM_PT_SUM_STAB = input[85];
    Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.TAR_WMOM_PT_SUM_STAB = input[86];
    Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.ST_ECU_AVL_BRTORQ_SUM = input[87];
    Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_SER_PRMSN_DBC = input[88];
    Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_SER_PRF_BRK = input[89];
    Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.AVL_BRTORQ_SUM_DVCH = input[90];
    Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.AVL_BRTORQ_SUM = input[91];
    Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_AVL_BRTORQ_SUM = input[92];
    Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_AVL_BRTORQ_SUM_DVCH = input[93];
    Proxy_TxByComAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_FLH = input[94];
    Proxy_TxByComAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_RRH = input[95];
    Proxy_TxByComAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_FRH = input[96];
    Proxy_TxByComAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_RRH = input[97];
    Proxy_TxByComAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_RLH = input[98];
    Proxy_TxByComAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_FLH = input[99];
    Proxy_TxByComAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_RLH = input[100];
    Proxy_TxByComAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_FRH = input[101];
    Proxy_TxByComAVL_RPM_WHLInfo.AVL_RPM_WHL_RLH = input[102];
    Proxy_TxByComAVL_RPM_WHLInfo.AVL_RPM_WHL_RRH = input[103];
    Proxy_TxByComAVL_RPM_WHLInfo.AVL_RPM_WHL_FLH = input[104];
    Proxy_TxByComAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_FLH = input[105];
    Proxy_TxByComAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_FRH = input[106];
    Proxy_TxByComAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_RLH = input[107];
    Proxy_TxByComAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_RRH = input[108];
    Proxy_TxByComAVL_RPM_WHLInfo.AVL_RPM_WHL_FRH = input[109];
    Proxy_TxByComRQ_WMOM_PT_SUM_RECUPInfo.QU_TAR_WMOM_PT_SUM_RECUP = input[110];
    Proxy_TxByComRQ_WMOM_PT_SUM_RECUPInfo.TAR_WMOM_PT_SUM_RECUP = input[111];
    Proxy_TxByComRQ_WMOM_PT_SUM_RECUPInfo.ST_ECU_RQ_WMOM_PT_SUM_RECUP = input[112];
    Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.ST_ECU_ST_STAB_DSC = input[113];
    Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.ST_BRG_MSA = input[114];
    Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.ST_BRG_DV = input[115];
    Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.ST_UNRU_DSC = input[116];
    Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.QU_FN_FDR = input[117];
    Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.QU_FN_ABS = input[118];
    Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.QU_FN_ASC = input[119];
    Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.PRD_TIM_VEHSS = input[120];
    Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.QU_SER_ECBA = input[121];
    Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.TAR_BRTORQ_SUM_COOTD = input[122];
    Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.AVL_WAY_BRKFA = input[123];
    Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.AVL_FORC_BRKFA = input[124];
    Proxy_TxByComAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_RLH = input[125];
    Proxy_TxByComAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_RRH = input[126];
    Proxy_TxByComAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_FLH = input[127];
    Proxy_TxByComAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_FRH = input[128];
    Proxy_TxByComAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_FLH = input[129];
    Proxy_TxByComAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_RRH = input[130];
    Proxy_TxByComAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_FRH = input[131];
    Proxy_TxByComAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_RLH = input[132];
    Proxy_TxByComTAR_LTRQD_BAXInfo.TAR_LTRQD_BAX = input[133];
    Proxy_TxByComTAR_LTRQD_BAXInfo.LIM_MAX_LTRQD_BAX = input[134];
    Proxy_TxByComST_YMRInfo.QU_SER_CLCTR_YMR = input[135];
    Proxy_TxByComWEGSTRECKEInfo.MILE_FLH = input[136];
    Proxy_TxByComWEGSTRECKEInfo.MILE_FRH = input[137];
    Proxy_TxByComWEGSTRECKEInfo.MILE_INTM_TOMSRM = input[138];
    Proxy_TxByComWEGSTRECKEInfo.MILE = input[139];
    Proxy_TxByComDISP_CC_DRDY_00Info.ST_IDC_CC_DRDY_00 = input[140];
    Proxy_TxByComDISP_CC_DRDY_00Info.ST_CC_DRDY_00 = input[141];
    Proxy_TxByComDISP_CC_DRDY_00Info.TRANF_CC_DRDY_00 = input[142];
    Proxy_TxByComDISP_CC_DRDY_00Info.NO_CC_DRDY_00 = input[143];
    Proxy_TxByComDISP_CC_BYPA_00Info.TRANF_CC_BYPA_00 = input[144];
    Proxy_TxByComDISP_CC_BYPA_00Info.ST_IDC_CC_BYPA_00 = input[145];
    Proxy_TxByComDISP_CC_BYPA_00Info.ST_CC_BYPA_00 = input[146];
    Proxy_TxByComDISP_CC_BYPA_00Info.NO_CC_BYPA_00 = input[147];
    Proxy_TxByComST_VHSSInfo.ST_VEHSS = input[148];
    Proxy_TxByComDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_1_ERR = input[149];
    Proxy_TxByComDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_3_ERR = input[150];
    Proxy_TxByComDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_4_ERR = input[151];
    Proxy_TxByComDIAG_OBD_HYB_1Info.DIAG_OBD_HYB_1_MUX_MAX = input[152];
    Proxy_TxByComDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_2_ERR = input[153];
    Proxy_TxByComDIAG_OBD_HYB_1Info.DIAG_OBD_HYB_1_MUX_IMME = input[154];
    Proxy_TxByComSU_DSCInfo.SU_DSC_WSS = input[155];
    Proxy_TxByComST_TYR_RDCInfo.QU_TPL_RDC = input[156];
    Proxy_TxByComST_TYR_RDCInfo.QU_FN_TYR_INFO_RDC = input[157];
    Proxy_TxByComST_TYR_RDCInfo.QU_TFAI_RDC = input[158];
    Proxy_TxByComST_TYR_RPAInfo.QU_TFAI_RPA = input[159];
    Proxy_TxByComST_TYR_RPAInfo.QU_FN_TYR_INFO_RPA = input[160];
    Proxy_TxByComST_TYR_RPAInfo.QU_TPL_RPA = input[161];
    Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_2 = input[162];
    Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_1 = input[163];
    Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_3 = input[164];
    Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_4 = input[165];
    Proxy_TxByComST_TYR_2Info.QU_RDC_INIT_DISP = input[166];
    Proxy_TxByComAVL_T_TYRInfo.AVL_TEMP_TYR_RRH = input[167];
    Proxy_TxByComAVL_T_TYRInfo.AVL_TEMP_TYR_FLH = input[168];
    Proxy_TxByComAVL_T_TYRInfo.AVL_TEMP_TYR_RLH = input[169];
    Proxy_TxByComAVL_T_TYRInfo.AVL_TEMP_TYR_FRH = input[170];
    Proxy_TxByComTEMP_BRKInfo.TEMP_BRDSK_RLH_VRFD = input[171];
    Proxy_TxByComTEMP_BRKInfo.QU_TEMP_BRDSK_RLH_VRFD = input[172];
    Proxy_TxByComTEMP_BRKInfo.TEMP_BRDSK_RRH_VRFD = input[173];
    Proxy_TxByComTEMP_BRKInfo.QU_TEMP_BRDSK_RRH_VRFD = input[174];
    Proxy_TxByComAVL_P_TYRInfo.AVL_P_TYR_FRH = input[175];
    Proxy_TxByComAVL_P_TYRInfo.AVL_P_TYR_FLH = input[176];
    Proxy_TxByComAVL_P_TYRInfo.AVL_P_TYR_RRH = input[177];
    Proxy_TxByComAVL_P_TYRInfo.AVL_P_TYR_RLH = input[178];
    Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_13 = input[179];
    Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_04 = input[180];
    Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_05 = input[181];
    Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_06 = input[182];
    Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_01 = input[183];
    Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_02 = input[184];
    Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_03 = input[185];
    Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_07 = input[186];
    Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_11 = input[187];
    Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_10 = input[188];
    Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_09 = input[189];
    Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_15 = input[190];
    Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_14 = input[191];
    Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_12 = input[192];
    Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_00 = input[193];
    Proxy_TxByComFR_DBG_DSCInfo.BDTM_TA_FR = input[194];
    Proxy_TxByComFR_DBG_DSCInfo.BDTM_TA_RL = input[195];
    Proxy_TxByComFR_DBG_DSCInfo.BDTM_TA_RR = input[196];
    Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_08 = input[197];
    Proxy_TxByComFR_DBG_DSCInfo.ST_STATE = input[198];
    Proxy_TxByComFR_DBG_DSCInfo.BDTM_TA_FL = input[199];
    Proxy_TxByComFR_DBG_DSCInfo.ABSRef = input[200];
    Proxy_TxByComFR_DBG_DSCInfo.LOW_VOLTAGE = input[201];
    Proxy_TxByComFR_DBG_DSCInfo.DEBUG_PCIB = input[202];
    Proxy_TxByComFR_DBG_DSCInfo.DBG_DSC = input[203];
    Proxy_TxByComFR_DBG_DSCInfo.LATACC = input[204];
    Proxy_TxByComFR_DBG_DSCInfo.TRIGGER_FS = input[205];
    Proxy_TxByComFR_DBG_DSCInfo.TRIGGER_IS = input[206];

    Proxy_TxByCom();


    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Proxy_TxByCom_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
