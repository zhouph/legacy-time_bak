#define S_FUNCTION_NAME      Proxy_RxByCom_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          0
#define WidthOutputPort         454

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Proxy_RxByCom.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];


    Proxy_RxByCom();


    output[0] = Proxy_RxByComRxBms1Info.Bms1SocPc;
    output[1] = Proxy_RxByComRxYawSerialInfo.YawSerialNum_0;
    output[2] = Proxy_RxByComRxYawSerialInfo.YawSerialNum_1;
    output[3] = Proxy_RxByComRxYawSerialInfo.YawSerialNum_2;
    output[4] = Proxy_RxByComRxYawAccInfo.YawRateValidData;
    output[5] = Proxy_RxByComRxYawAccInfo.YawRateSelfTestStatus;
    output[6] = Proxy_RxByComRxYawAccInfo.YawRateSignal_0;
    output[7] = Proxy_RxByComRxYawAccInfo.YawRateSignal_1;
    output[8] = Proxy_RxByComRxYawAccInfo.SensorOscFreqDev;
    output[9] = Proxy_RxByComRxYawAccInfo.Gyro_Fail;
    output[10] = Proxy_RxByComRxYawAccInfo.Raster_Fail;
    output[11] = Proxy_RxByComRxYawAccInfo.Eep_Fail;
    output[12] = Proxy_RxByComRxYawAccInfo.Batt_Range_Err;
    output[13] = Proxy_RxByComRxYawAccInfo.Asic_Fail;
    output[14] = Proxy_RxByComRxYawAccInfo.Accel_Fail;
    output[15] = Proxy_RxByComRxYawAccInfo.Ram_Fail;
    output[16] = Proxy_RxByComRxYawAccInfo.Rom_Fail;
    output[17] = Proxy_RxByComRxYawAccInfo.Ad_Fail;
    output[18] = Proxy_RxByComRxYawAccInfo.Osc_Fail;
    output[19] = Proxy_RxByComRxYawAccInfo.Watchdog_Rst;
    output[20] = Proxy_RxByComRxYawAccInfo.Plaus_Err_Pst;
    output[21] = Proxy_RxByComRxYawAccInfo.RollingCounter;
    output[22] = Proxy_RxByComRxYawAccInfo.Can_Func_Err;
    output[23] = Proxy_RxByComRxYawAccInfo.AccelEratorRateSig_0;
    output[24] = Proxy_RxByComRxYawAccInfo.AccelEratorRateSig_1;
    output[25] = Proxy_RxByComRxYawAccInfo.LatAccValidData;
    output[26] = Proxy_RxByComRxYawAccInfo.LatAccSelfTestStatus;
    output[27] = Proxy_RxByComRxTcu6Info.ShiftClass_Ccan;
    output[28] = Proxy_RxByComRxTcu5Info.Typ;
    output[29] = Proxy_RxByComRxTcu5Info.GearTyp;
    output[30] = Proxy_RxByComRxTcu1Info.Targe;
    output[31] = Proxy_RxByComRxTcu1Info.GarChange;
    output[32] = Proxy_RxByComRxTcu1Info.Flt;
    output[33] = Proxy_RxByComRxTcu1Info.GarSelDisp;
    output[34] = Proxy_RxByComRxTcu1Info.TQRedReq_PC;
    output[35] = Proxy_RxByComRxTcu1Info.TQRedReqSlw_PC;
    output[36] = Proxy_RxByComRxTcu1Info.TQIncReq_PC;
    output[37] = Proxy_RxByComRxSasInfo.Angle;
    output[38] = Proxy_RxByComRxSasInfo.Speed;
    output[39] = Proxy_RxByComRxSasInfo.Ok;
    output[40] = Proxy_RxByComRxSasInfo.Cal;
    output[41] = Proxy_RxByComRxSasInfo.Trim;
    output[42] = Proxy_RxByComRxSasInfo.CheckSum;
    output[43] = Proxy_RxByComRxSasInfo.MsgCount;
    output[44] = Proxy_RxByComRxMcu2Info.Flt;
    output[45] = Proxy_RxByComRxMcu1Info.MoTestTQ_PC;
    output[46] = Proxy_RxByComRxMcu1Info.MotActRotSpd_RPM;
    output[47] = Proxy_RxByComRxLongAccInfo.IntSenFltSymtmActive;
    output[48] = Proxy_RxByComRxLongAccInfo.IntSenFaultPresent;
    output[49] = Proxy_RxByComRxLongAccInfo.LongAccSenCirErrPre;
    output[50] = Proxy_RxByComRxLongAccInfo.LonACSenRanChkErrPre;
    output[51] = Proxy_RxByComRxLongAccInfo.LongRollingCounter;
    output[52] = Proxy_RxByComRxLongAccInfo.IntTempSensorFault;
    output[53] = Proxy_RxByComRxLongAccInfo.LongAccInvalidData;
    output[54] = Proxy_RxByComRxLongAccInfo.LongAccSelfTstStatus;
    output[55] = Proxy_RxByComRxLongAccInfo.LongAccRateSignal_0;
    output[56] = Proxy_RxByComRxLongAccInfo.LongAccRateSignal_1;
    output[57] = Proxy_RxByComRxHcu5Info.HevMod;
    output[58] = Proxy_RxByComRxHcu3Info.TmIntQcMDBINV_PC;
    output[59] = Proxy_RxByComRxHcu3Info.MotTQCMC_PC;
    output[60] = Proxy_RxByComRxHcu3Info.MotTQCMDBINV_PC;
    output[61] = Proxy_RxByComRxHcu2Info.ServiceMod;
    output[62] = Proxy_RxByComRxHcu2Info.RegenENA;
    output[63] = Proxy_RxByComRxHcu2Info.RegenBRKTQ_NM;
    output[64] = Proxy_RxByComRxHcu2Info.CrpTQ_NM;
    output[65] = Proxy_RxByComRxHcu2Info.WhlDEMTQ_NM;
    output[66] = Proxy_RxByComRxHcu1Info.EngCltStat;
    output[67] = Proxy_RxByComRxHcu1Info.HEVRDY;
    output[68] = Proxy_RxByComRxHcu1Info.EngTQCmdBinV_PC;
    output[69] = Proxy_RxByComRxHcu1Info.EngTQCmd_PC;
    output[70] = Proxy_RxByComRxFact1Info.OutTemp_SNR_C;
    output[71] = Proxy_RxByComRxEms3Info.EngColTemp_C;
    output[72] = Proxy_RxByComRxEms2Info.EngSpdErr;
    output[73] = Proxy_RxByComRxEms2Info.AccPedDep_PC;
    output[74] = Proxy_RxByComRxEms2Info.Tps_PC;
    output[75] = Proxy_RxByComRxEms1Info.TqStd_NM;
    output[76] = Proxy_RxByComRxEms1Info.ActINDTQ_PC;
    output[77] = Proxy_RxByComRxEms1Info.EngSpd_RPM;
    output[78] = Proxy_RxByComRxEms1Info.IndTQ_PC;
    output[79] = Proxy_RxByComRxEms1Info.FrictTQ_PC;
    output[80] = Proxy_RxByComRxClu2Info.IGN_SW;
    output[81] = Proxy_RxByComRxClu1Info.P_Brake_Act;
    output[82] = Proxy_RxByComRxClu1Info.Cf_Clu_BrakeFluIDSW;
    output[83] = Proxy_RxByComRxMsgOkFlgInfo.Bms1MsgOkFlg;
    output[84] = Proxy_RxByComRxMsgOkFlgInfo.YawSerialMsgOkFlg;
    output[85] = Proxy_RxByComRxMsgOkFlgInfo.YawAccMsgOkFlg;
    output[86] = Proxy_RxByComRxMsgOkFlgInfo.Tcu6MsgOkFlg;
    output[87] = Proxy_RxByComRxMsgOkFlgInfo.Tcu5MsgOkFlg;
    output[88] = Proxy_RxByComRxMsgOkFlgInfo.Tcu1MsgOkFlg;
    output[89] = Proxy_RxByComRxMsgOkFlgInfo.SasMsgOkFlg;
    output[90] = Proxy_RxByComRxMsgOkFlgInfo.Mcu2MsgOkFlg;
    output[91] = Proxy_RxByComRxMsgOkFlgInfo.Mcu1MsgOkFlg;
    output[92] = Proxy_RxByComRxMsgOkFlgInfo.LongAccMsgOkFlg;
    output[93] = Proxy_RxByComRxMsgOkFlgInfo.Hcu5MsgOkFlg;
    output[94] = Proxy_RxByComRxMsgOkFlgInfo.Hcu3MsgOkFlg;
    output[95] = Proxy_RxByComRxMsgOkFlgInfo.Hcu2MsgOkFlg;
    output[96] = Proxy_RxByComRxMsgOkFlgInfo.Hcu1MsgOkFlg;
    output[97] = Proxy_RxByComRxMsgOkFlgInfo.Fatc1MsgOkFlg;
    output[98] = Proxy_RxByComRxMsgOkFlgInfo.Ems3MsgOkFlg;
    output[99] = Proxy_RxByComRxMsgOkFlgInfo.Ems2MsgOkFlg;
    output[100] = Proxy_RxByComRxMsgOkFlgInfo.Ems1MsgOkFlg;
    output[101] = Proxy_RxByComRxMsgOkFlgInfo.Clu2MsgOkFlg;
    output[102] = Proxy_RxByComRxMsgOkFlgInfo.Clu1MsgOkFlg;
    output[103] = Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.ST_ECU_ST_REPAT_XTRQ_FTAX_BAX;
    output[104] = Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.ALIV_ST_REPAT_XTRQ_FTAX_BAX;
    output[105] = Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.CRC_ST_REPAT_XTRQ_FTAX_BAX;
    output[106] = Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.QU_SER_REPAT_XTRQ_FTAX_BAX_ACT;
    output[107] = Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.AVL_REPAT_XTRQ_FTAX_BAX;
    output[108] = Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.QU_AVL_REPAT_XTRQ_FTAX_BAX;
    output[109] = Proxy_RxByComAVL_LTRQD_BAXInfo.QU_SER_LTRQD_BAX;
    output[110] = Proxy_RxByComAVL_LTRQD_BAXInfo.CRC_AVL_LTRQD_BAX;
    output[111] = Proxy_RxByComAVL_LTRQD_BAXInfo.QU_AVL_LTRQD_BAX;
    output[112] = Proxy_RxByComAVL_LTRQD_BAXInfo.AVL_LTRQD_BAX;
    output[113] = Proxy_RxByComAVL_LTRQD_BAXInfo.ALIV_AVL_LTRQD_BAX;
    output[114] = Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.AVL_TORQ_CRSH_DMEE;
    output[115] = Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.AVL_TORQ_CRSH;
    output[116] = Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.ST_SAIL_DRV_2;
    output[117] = Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.CRC_ANG_ACPD;
    output[118] = Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.QU_AVL_RPM_ENG_CRSH;
    output[119] = Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.AVL_RPM_ENG_CRSH;
    output[120] = Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.ALIV_TORQ_CRSH_1;
    output[121] = Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.CRC_TORQ_CRSH_1;
    output[122] = Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.AVL_ANG_ACPD;
    output[123] = Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.QU_AVL_ANG_ACPD;
    output[124] = Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.ALIV_ANG_ACPD;
    output[125] = Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.ST_ECU_ANG_ACPD;
    output[126] = Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.AVL_ANG_ACPD_VIRT;
    output[127] = Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.ECO_ANG_ACPD;
    output[128] = Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.GRAD_AVL_ANG_ACPD;
    output[129] = Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.ST_INTF_DRASY;
    output[130] = Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.AVL_RPM_BAX_RED;
    output[131] = Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.ST_PENG_PT;
    output[132] = Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.ST_AVAI_INTV_PT_DRS;
    output[133] = Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.QU_AVL_RPM_BAX_RED;
    output[134] = Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.QU_SER_WMOM_PT_SUM_DRS;
    output[135] = Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.QU_SER_WMOM_PT_SUM_STAB;
    output[136] = Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.ST_ECU_WMOM_DRV_5;
    output[137] = Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.TAR_WMOM_PT_SUM_COOTD;
    output[138] = Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.QU_SER_COOR_TORQ_BDRV;
    output[139] = Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.ST_ECU_WMOM_DRV_4;
    output[140] = Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.ST_DRVDIR_DVCH;
    output[141] = Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.CRC_WMOM_DRV_4;
    output[142] = Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.ALIV_WMOM_DRV_4;
    output[143] = Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.ALIV_WMOM_DRV_5;
    output[144] = Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.CRC_WMOM_DRV_5;
    output[145] = Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.QU_AVL_WMOM_PT_SUM;
    output[146] = Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_ERR_AMP;
    output[147] = Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.REIN_PT;
    output[148] = Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM;
    output[149] = Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.CRC_WMOM_DRV_1;
    output[150] = Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.ALIV_WMOM_DRV_1;
    output[151] = Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.ST_ECU_WMOM_DRV_1;
    output[152] = Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_MAX;
    output[153] = Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_FAST_TOP;
    output[154] = Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_FAST_BOT;
    output[155] = Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.ST_ECU_WMOM_DRV_2;
    output[156] = Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.QU_REIN_PT;
    output[157] = Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.CRC_WMOM_DRV_2;
    output[158] = Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.ALIV_WMOM_DRV_2;
    output[159] = Proxy_RxByComHGLV_VEH_FILTInfo.HGLV_VEH_FILT_FRH;
    output[160] = Proxy_RxByComHGLV_VEH_FILTInfo.HGLV_VEH_FILT_FLH;
    output[161] = Proxy_RxByComHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_FRH;
    output[162] = Proxy_RxByComHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_FLH;
    output[163] = Proxy_RxByComHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_RRH;
    output[164] = Proxy_RxByComHGLV_VEH_FILTInfo.HGLV_VEH_FILT_RLH;
    output[165] = Proxy_RxByComHGLV_VEH_FILTInfo.ALIV_HGLV_VEH_FILT;
    output[166] = Proxy_RxByComHGLV_VEH_FILTInfo.HGLV_VEH_FILT_RRH;
    output[167] = Proxy_RxByComHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_RLH;
    output[168] = Proxy_RxByComHGLV_VEH_FILTInfo.CRC_HGLV_VEH_FILT;
    output[169] = Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.ATTAV_ESTI;
    output[170] = Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.ATTA_ESTI_ERR_AMP;
    output[171] = Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.VY_ESTI_ERR_AMP;
    output[172] = Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.VY_ESTI;
    output[173] = Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.ATTAV_ESTI_ERR_AMP;
    output[174] = Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.CRC_VEH_DYNMC_DT_ESTI_VRFD;
    output[175] = Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.ALIV_VEH_DYNMC_DT_ESTI_VRFD;
    output[176] = Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.QU_VEH_DYNMC_DT_ESTI;
    output[177] = Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.ATTA_ESTI;
    output[178] = Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.QU_ACLNY_COG;
    output[179] = Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNY_COG_ERR_AMP;
    output[180] = Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ALIV_ACLNX_COG;
    output[181] = Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNX_COG;
    output[182] = Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ALIV_ACLNY_COG;
    output[183] = Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.CRC_ACLNX_COG;
    output[184] = Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.CRC_ACLNY_COG;
    output[185] = Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNY_COG;
    output[186] = Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNX_COG_ERR_AMP;
    output[187] = Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.QU_ACLNX_COG;
    output[188] = Proxy_RxByComV_VEH_V_VEH_2Info.CRC_V_VEH;
    output[189] = Proxy_RxByComV_VEH_V_VEH_2Info.ALIV_V_VEH;
    output[190] = Proxy_RxByComV_VEH_V_VEH_2Info.QU_V_VEH_COG;
    output[191] = Proxy_RxByComV_VEH_V_VEH_2Info.DVCO_VEH;
    output[192] = Proxy_RxByComV_VEH_V_VEH_2Info.ST_V_VEH_NSS;
    output[193] = Proxy_RxByComV_VEH_V_VEH_2Info.V_VEH_COG;
    output[194] = Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo.ALIV_VYAW_VEH;
    output[195] = Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo.VYAW_VEH_ERR_AMP;
    output[196] = Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo.CRC_VYAW_VEH;
    output[197] = Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo.QU_VYAW_VEH;
    output[198] = Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo.VYAW_VEH;
    output[199] = Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo.QU_AVL_TRGR_RW;
    output[200] = Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo.AVL_TRGR_RW;
    output[201] = Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo.AVL_LOGR_RW_FAST;
    output[202] = Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo.ALIV_TLT_RW;
    output[203] = Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo.CRC_TLT_RW;
    output[204] = Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo.QU_AVL_LOGR_RW;
    output[205] = Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo.AVL_LOGR_RW;
    output[206] = Proxy_RxByComAVL_STEA_FTAXInfo.QU_AVL_STEA_FTAX_PNI;
    output[207] = Proxy_RxByComAVL_STEA_FTAXInfo.AVL_STEA_FTAX_PNI;
    output[208] = Proxy_RxByComAVL_STEA_FTAXInfo.CRC_AVL_STEA_FTAX;
    output[209] = Proxy_RxByComAVL_STEA_FTAXInfo.QU_AVL_STEA_FTAX_WHL;
    output[210] = Proxy_RxByComAVL_STEA_FTAXInfo.ALIV_AVL_STEA_FTAX;
    output[211] = Proxy_RxByComAVL_STEA_FTAXInfo.AVL_STEA_FTAX_WHL;
    output[212] = Proxy_RxByComAVL_STEA_FTAXInfo.AVL_STEA_FTAX_WHL_ERR_AMP;
    output[213] = Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_PRMSN_DBC_DCRN_MAX;
    output[214] = Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_PRF_BRK;
    output[215] = Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_V_HDC;
    output[216] = Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_PRMSN_DBC_DCRN_GRAD_MAX;
    output[217] = Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.ALIV_SPEC_PRMSN_IBRK_HDC;
    output[218] = Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.CRC_SPEC_PRMSN_IBRK_HDC;
    output[219] = Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_PRMSN_DBC_TR_THRV;
    output[220] = Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_BRTORQ_SUM;
    output[221] = Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_BRTORQ_SUM;
    output[222] = Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_PRMSN_DBC;
    output[223] = Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.RQ_TAO_SSM;
    output[224] = Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.CRC_RQ_BRTORQ_SUM;
    output[225] = Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.ALIV_RQ_BRTORQ_SUM;
    output[226] = Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_DTORQ_BOT;
    output[227] = Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_RECUP_MAX;
    output[228] = Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_ILS;
    output[229] = Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.CRC_WMOM_DRV_3;
    output[230] = Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.ALIV_WMOM_DRV_3;
    output[231] = Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_DTORQ_TOP;
    output[232] = Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.ST_ECU_WMOM_DRV_6;
    output[233] = Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.ALIV_WMOM_DRV_6;
    output[234] = Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.CRC_WMOM_DRV_6;
    output[235] = Proxy_RxByComWMOM_DRV_7Info.ST_EL_DRVG;
    output[236] = Proxy_RxByComWMOM_DRV_7Info.PRD_AVL_WMOM_PT_SUM_RECUP_MAX;
    output[237] = Proxy_RxByComWMOM_DRV_7Info.ALIV_WMOM_DRV_7;
    output[238] = Proxy_RxByComWMOM_DRV_7Info.CRC_WMOM_DRV_7;
    output[239] = Proxy_RxByComWMOM_DRV_7Info.ST_ECU_WMOM_DRV_7;
    output[240] = Proxy_RxByComWMOM_DRV_7Info.QU_SER_WMOM_PT_SUM_RECUP;
    output[241] = Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.TAR_DIFF_BRTORQ_BAX_YMR;
    output[242] = Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.FACT_TAR_COMPT_DRV_YMR;
    output[243] = Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.QU_TAR_DIFF_BRTORQ_YMR;
    output[244] = Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.CRC_RQ_DIFF_BRTORQ_YMR;
    output[245] = Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.ALIV_RQ_DIFF_BRTORQ_YMR;
    output[246] = Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.TAR_DIFF_BRTORQ_FTAX_YMR;
    output[247] = Proxy_RxByComDT_BRKSYS_ENGMGInfo.QU_AVL_LOWP_BRKFA;
    output[248] = Proxy_RxByComDT_BRKSYS_ENGMGInfo.AVL_LOWP_BRKFA;
    output[249] = Proxy_RxByComDT_BRKSYS_ENGMGInfo.CRC_DT_BRKSYS_ENGMG;
    output[250] = Proxy_RxByComDT_BRKSYS_ENGMGInfo.ALIV_DT_BRKSYS_ENGMG;
    output[251] = Proxy_RxByComERRM_BN_UInfo.CTR_ERRM_BN_U;
    output[252] = Proxy_RxByComCTR_CRInfo.ALIV_CTR_CR;
    output[253] = Proxy_RxByComCTR_CRInfo.ST_EXCE_ACLN_THRV;
    output[254] = Proxy_RxByComCTR_CRInfo.CTR_PHTR_CR;
    output[255] = Proxy_RxByComCTR_CRInfo.CTR_ITLI_CR;
    output[256] = Proxy_RxByComCTR_CRInfo.CTR_CLSY_CR;
    output[257] = Proxy_RxByComCTR_CRInfo.CTR_AUTOM_ECAL_CR;
    output[258] = Proxy_RxByComCTR_CRInfo.CTR_SWO_EKP_CR;
    output[259] = Proxy_RxByComCTR_CRInfo.CRC_CTR_CR;
    output[260] = Proxy_RxByComCTR_CRInfo.CTR_PCSH_MST;
    output[261] = Proxy_RxByComCTR_CRInfo.CTR_HAZW_CR;
    output[262] = Proxy_RxByComKLEMMENInfo.ST_OP_MSA;
    output[263] = Proxy_RxByComKLEMMENInfo.RQ_DRVG_RDI;
    output[264] = Proxy_RxByComKLEMMENInfo.ST_KL_DBG;
    output[265] = Proxy_RxByComKLEMMENInfo.ST_KL_50_MSA;
    output[266] = Proxy_RxByComKLEMMENInfo.ST_SSP;
    output[267] = Proxy_RxByComKLEMMENInfo.RWDT_BLS;
    output[268] = Proxy_RxByComKLEMMENInfo.ST_STCD_PENG;
    output[269] = Proxy_RxByComKLEMMENInfo.ST_KL;
    output[270] = Proxy_RxByComKLEMMENInfo.ST_KL_DIV;
    output[271] = Proxy_RxByComKLEMMENInfo.ST_VEH_CON;
    output[272] = Proxy_RxByComKLEMMENInfo.CRC_KL;
    output[273] = Proxy_RxByComKLEMMENInfo.ALIV_COU_KL;
    output[274] = Proxy_RxByComKLEMMENInfo.ST_KL_30B;
    output[275] = Proxy_RxByComKLEMMENInfo.CON_CLT_SW;
    output[276] = Proxy_RxByComKLEMMENInfo.CTR_ENG_STOP;
    output[277] = Proxy_RxByComKLEMMENInfo.ST_PLK;
    output[278] = Proxy_RxByComKLEMMENInfo.ST_KL_15N;
    output[279] = Proxy_RxByComKLEMMENInfo.ST_KL_KEY_VLD;
    output[280] = Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_MES_TSTMP;
    output[281] = Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.PCKG_ID;
    output[282] = Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.SUPP_ID;
    output[283] = Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_5;
    output[284] = Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_8;
    output[285] = Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_7;
    output[286] = Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_6;
    output[287] = Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.ALIV_RDC_DT_PCKG_2;
    output[288] = Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_1;
    output[289] = Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.TYR_ID;
    output[290] = Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.ALIV_RDC_DT_PCKG_1;
    output[291] = Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_4;
    output[292] = Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_3;
    output[293] = Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_2;
    output[294] = Proxy_RxByComBEDIENUNG_WISCHERInfo.ALIV_WISW;
    output[295] = Proxy_RxByComBEDIENUNG_WISCHERInfo.OP_WISW;
    output[296] = Proxy_RxByComBEDIENUNG_WISCHERInfo.OP_WIPO;
    output[297] = Proxy_RxByComDT_PT_2Info.ST_GRSEL_DRV;
    output[298] = Proxy_RxByComDT_PT_2Info.TEMP_EOI_DRV;
    output[299] = Proxy_RxByComDT_PT_2Info.RLS_ENGSTA;
    output[300] = Proxy_RxByComDT_PT_2Info.RPM_ENG_MAX_ALW;
    output[301] = Proxy_RxByComDT_PT_2Info.RDUC_DOCTR_RPM_DRV_2;
    output[302] = Proxy_RxByComDT_PT_2Info.TEMP_ENG_DRV;
    output[303] = Proxy_RxByComDT_PT_2Info.ST_DRV_VEH;
    output[304] = Proxy_RxByComDT_PT_2Info.ST_ECU_DT_PT_2;
    output[305] = Proxy_RxByComDT_PT_2Info.ST_IDLG_ENG_DRV;
    output[306] = Proxy_RxByComDT_PT_2Info.ST_ILK_STRT_DRV;
    output[307] = Proxy_RxByComDT_PT_2Info.ST_SW_CLT_DRV;
    output[308] = Proxy_RxByComDT_PT_2Info.CRC_DT_PT_2;
    output[309] = Proxy_RxByComDT_PT_2Info.ALIV_DT_PT_2;
    output[310] = Proxy_RxByComDT_PT_2Info.ST_ENG_RUN_DRV;
    output[311] = Proxy_RxByComSTAT_ENG_STA_AUTOInfo.RQ_MSA_ENG_STA_1;
    output[312] = Proxy_RxByComSTAT_ENG_STA_AUTOInfo.RQ_MSA_ENG_STA_2;
    output[313] = Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ALIV_ST_ENG_STA_AUTO;
    output[314] = Proxy_RxByComSTAT_ENG_STA_AUTOInfo.PSBTY_MSA_ENG_STOP_STA;
    output[315] = Proxy_RxByComSTAT_ENG_STA_AUTOInfo.CRC_ST_ENG_STA_AUTO;
    output[316] = Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_SHFT_MSA_ENGSTP;
    output[317] = Proxy_RxByComSTAT_ENG_STA_AUTOInfo.RQ_SLIP_K0;
    output[318] = Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_TAR_PENG_CENG;
    output[319] = Proxy_RxByComSTAT_ENG_STA_AUTOInfo.SPEC_TYP_ENGSTA;
    output[320] = Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_RPM_CLCTR_MOT;
    output[321] = Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_FN_MSA;
    output[322] = Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_AVL_PENG_CENG_ENGMG;
    output[323] = Proxy_RxByComSTAT_ENG_STA_AUTOInfo.DISP_REAS_PREV_SWO_CENG;
    output[324] = Proxy_RxByComSTAT_ENG_STA_AUTOInfo.VARI_TYP_ENGSTA;
    output[325] = Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_TAR_CENG;
    output[326] = Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_AVAI_SAIL_DME;
    output[327] = Proxy_RxByComST_ENERG_GENInfo.ST_LDST_GEN_DRV;
    output[328] = Proxy_RxByComST_ENERG_GENInfo.DT_PCU_SCP;
    output[329] = Proxy_RxByComST_ENERG_GENInfo.ST_GEN_DRV;
    output[330] = Proxy_RxByComST_ENERG_GENInfo.AVL_I_GEN_DRV;
    output[331] = Proxy_RxByComST_ENERG_GENInfo.LDST_GEN_DRV;
    output[332] = Proxy_RxByComST_ENERG_GENInfo.ST_LDREL_GEN;
    output[333] = Proxy_RxByComST_ENERG_GENInfo.ST_CHG_STOR;
    output[334] = Proxy_RxByComST_ENERG_GENInfo.TEMP_BT_14V;
    output[335] = Proxy_RxByComST_ENERG_GENInfo.ST_I_IBS;
    output[336] = Proxy_RxByComST_ENERG_GENInfo.ST_SEP_STOR;
    output[337] = Proxy_RxByComST_ENERG_GENInfo.ST_BN2_SCP;
    output[338] = Proxy_RxByComDT_PT_1Info.RQ_SHPA_GRB_REGE_PAFI;
    output[339] = Proxy_RxByComDT_PT_1Info.ST_RTIR_DRV;
    output[340] = Proxy_RxByComDT_PT_1Info.CTR_SLCK_DRV;
    output[341] = Proxy_RxByComDT_PT_1Info.RQ_STASS_ENGMG;
    output[342] = Proxy_RxByComDT_PT_1Info.ST_CAT_HT;
    output[343] = Proxy_RxByComDT_PT_1Info.RQ_SHPA_GRB_CHGBLC;
    output[344] = Proxy_RxByComDT_PT_1Info.TAR_RPM_IDLG_DRV_EXT;
    output[345] = Proxy_RxByComDT_PT_1Info.SLCTN_BUS_COMM_ENG_GRB;
    output[346] = Proxy_RxByComDT_PT_1Info.TAR_RPM_IDLG_DRV;
    output[347] = Proxy_RxByComDT_PT_1Info.ST_INFS_DRV;
    output[348] = Proxy_RxByComDT_PT_1Info.ST_SW_WAUP_DRV;
    output[349] = Proxy_RxByComDT_PT_1Info.AIP_ENG_DRV;
    output[350] = Proxy_RxByComDT_PT_1Info.RDUC_DOCTR_RPM_DRV;
    output[351] = Proxy_RxByComDT_GRDT_DRVInfo.ST_OPMO_GRDT_DRV;
    output[352] = Proxy_RxByComDT_GRDT_DRVInfo.ST_RSTA_GRDT;
    output[353] = Proxy_RxByComDT_GRDT_DRVInfo.CRC_GRDT_DRV;
    output[354] = Proxy_RxByComDT_GRDT_DRVInfo.ALIV_GRDT_DRV;
    output[355] = Proxy_RxByComDIAG_OBD_ENGMG_ELInfo.RQ_MIL_DIAG_OBD_ENGMG_EL;
    output[356] = Proxy_RxByComDIAG_OBD_ENGMG_ELInfo.RQ_RST_OBD_DIAG;
    output[357] = Proxy_RxByComSTAT_CT_HABRInfo.ST_CT_HABR;
    output[358] = Proxy_RxByComDT_PT_3Info.TRNRAO_BAX;
    output[359] = Proxy_RxByComDT_PT_3Info.QU_TRNRAO_BAX;
    output[360] = Proxy_RxByComDT_PT_3Info.ALIV_DT_PT_3;
    output[361] = Proxy_RxByComEINHEITEN_BN2020Info.UN_TORQ_S_MOD;
    output[362] = Proxy_RxByComEINHEITEN_BN2020Info.UN_PWR_S_MOD;
    output[363] = Proxy_RxByComEINHEITEN_BN2020Info.UN_DATE_EXT;
    output[364] = Proxy_RxByComEINHEITEN_BN2020Info.UN_COSP_EL;
    output[365] = Proxy_RxByComEINHEITEN_BN2020Info.UN_TEMP;
    output[366] = Proxy_RxByComEINHEITEN_BN2020Info.UN_AIP;
    output[367] = Proxy_RxByComEINHEITEN_BN2020Info.LANG;
    output[368] = Proxy_RxByComEINHEITEN_BN2020Info.UN_DATE;
    output[369] = Proxy_RxByComEINHEITEN_BN2020Info.UN_T;
    output[370] = Proxy_RxByComEINHEITEN_BN2020Info.UN_SPDM_DGTL;
    output[371] = Proxy_RxByComEINHEITEN_BN2020Info.UN_MILE;
    output[372] = Proxy_RxByComEINHEITEN_BN2020Info.UN_FU;
    output[373] = Proxy_RxByComEINHEITEN_BN2020Info.UN_COSP;
    output[374] = Proxy_RxByComA_TEMPInfo.TEMP_EX_UNFILT;
    output[375] = Proxy_RxByComA_TEMPInfo.TEMP_EX;
    output[376] = Proxy_RxByComWISCHERGESCHWINDIGKEITInfo.ST_RNSE;
    output[377] = Proxy_RxByComWISCHERGESCHWINDIGKEITInfo.INT_RN;
    output[378] = Proxy_RxByComWISCHERGESCHWINDIGKEITInfo.V_WI;
    output[379] = Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_DRD_SFY_CTRL;
    output[380] = Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_DVDR_VRFD;
    output[381] = Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_DVDR_SFY_CTRL;
    output[382] = Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_PSDR_SFY_CTRL;
    output[383] = Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_PSD_SFY_CTRL;
    output[384] = Proxy_RxByComSTAT_DS_VRFDInfo.ALIV_ST_DSW_VRFD;
    output[385] = Proxy_RxByComSTAT_DS_VRFDInfo.CRC_ST_DSW_VRFD;
    output[386] = Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_DRD_VRFD;
    output[387] = Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_PSDR_VRFD;
    output[388] = Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_PSD_VRFD;
    output[389] = Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY;
    output[390] = Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.RQ_SW_DRDY_MMID;
    output[391] = Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.RQ_SU_SW_DRDY;
    output[392] = Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.CRC_SU_SW_DRDY;
    output[393] = Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.ALIV_SU_SW_DRDY;
    output[394] = Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.RQ_SW_DRDY_KDIS;
    output[395] = Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY_CHAS;
    output[396] = Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY_DRV;
    output[397] = Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY_STAB;
    output[398] = Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.DISP_ST_DSC;
    output[399] = Proxy_RxByComSTAT_ANHAENGERInfo.ST_SYNCN_HAZWCL_TRAI;
    output[400] = Proxy_RxByComSTAT_ANHAENGERInfo.ST_RFLI_DF_TRAI;
    output[401] = Proxy_RxByComSTAT_ANHAENGERInfo.ST_TRAI;
    output[402] = Proxy_RxByComSTAT_ANHAENGERInfo.ST_DI_DF_TRAI;
    output[403] = Proxy_RxByComSTAT_ANHAENGERInfo.ST_PO_AHV;
    output[404] = Proxy_RxByComFZZSTDInfo.ST_ILK_ERRM_FZM;
    output[405] = Proxy_RxByComFZZSTDInfo.ST_ENERG_FZM;
    output[406] = Proxy_RxByComFZZSTDInfo.ST_BT_PROTE_WUP;
    output[407] = Proxy_RxByComBEDIENUNG_FAHRWERKInfo.OP_MOD_TRCT_DSC;
    output[408] = Proxy_RxByComBEDIENUNG_FAHRWERKInfo.OP_TPCT;
    output[409] = Proxy_RxByComFAHRZEUGTYPInfo.QUAN_CYL;
    output[410] = Proxy_RxByComFAHRZEUGTYPInfo.QUAN_GR;
    output[411] = Proxy_RxByComFAHRZEUGTYPInfo.TYP_VEH;
    output[412] = Proxy_RxByComFAHRZEUGTYPInfo.TYP_BODY;
    output[413] = Proxy_RxByComFAHRZEUGTYPInfo.TYP_ENG;
    output[414] = Proxy_RxByComFAHRZEUGTYPInfo.CLAS_PWR;
    output[415] = Proxy_RxByComFAHRZEUGTYPInfo.TYP_CNT;
    output[416] = Proxy_RxByComFAHRZEUGTYPInfo.TYP_STE;
    output[417] = Proxy_RxByComFAHRZEUGTYPInfo.TYP_GRB;
    output[418] = Proxy_RxByComFAHRZEUGTYPInfo.TYP_CAPA;
    output[419] = Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_RRH;
    output[420] = Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_BLTB_SW_RM;
    output[421] = Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_BLTB_SW_RRH;
    output[422] = Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_ERR_SEAT_MT_DR;
    output[423] = Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_BLTB_SW_DR;
    output[424] = Proxy_RxByComST_BLT_CT_SOCCUInfo.CRC_ST_BLT_CT_SOCCU;
    output[425] = Proxy_RxByComST_BLT_CT_SOCCUInfo.ALIV_COU_ST_BLT_CT_SOCCU;
    output[426] = Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_DR;
    output[427] = Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_BLTB_SW_RLH;
    output[428] = Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_RLH;
    output[429] = Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_BLTB_SW_PS;
    output[430] = Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_PS;
    output[431] = Proxy_RxByComFAHRGESTELLNUMMERInfo.NO_VECH_1;
    output[432] = Proxy_RxByComFAHRGESTELLNUMMERInfo.NO_VECH_2;
    output[433] = Proxy_RxByComFAHRGESTELLNUMMERInfo.NO_VECH_3;
    output[434] = Proxy_RxByComFAHRGESTELLNUMMERInfo.NO_VECH_6;
    output[435] = Proxy_RxByComFAHRGESTELLNUMMERInfo.NO_VECH_7;
    output[436] = Proxy_RxByComFAHRGESTELLNUMMERInfo.NO_VECH_4;
    output[437] = Proxy_RxByComFAHRGESTELLNUMMERInfo.NO_VECH_5;
    output[438] = Proxy_RxByComRELATIVZEITInfo.T_SEC_COU_REL;
    output[439] = Proxy_RxByComRELATIVZEITInfo.T_DAY_COU_ABSL;
    output[440] = Proxy_RxByComKILOMETERSTANDInfo.RNG;
    output[441] = Proxy_RxByComKILOMETERSTANDInfo.ST_FLLV_FUTA_SPAR;
    output[442] = Proxy_RxByComKILOMETERSTANDInfo.FLLV_FUTA_RH;
    output[443] = Proxy_RxByComKILOMETERSTANDInfo.FLLV_FUTA_LH;
    output[444] = Proxy_RxByComKILOMETERSTANDInfo.FLLV_FUTA;
    output[445] = Proxy_RxByComKILOMETERSTANDInfo.MILE_KM;
    output[446] = Proxy_RxByComUHRZEIT_DATUMInfo.DISP_DATE_WDAY;
    output[447] = Proxy_RxByComUHRZEIT_DATUMInfo.DISP_DATE_DAY;
    output[448] = Proxy_RxByComUHRZEIT_DATUMInfo.DISP_DATE_MON;
    output[449] = Proxy_RxByComUHRZEIT_DATUMInfo.ST_DISP_CTI_DATE;
    output[450] = Proxy_RxByComUHRZEIT_DATUMInfo.DISP_DATE_YR;
    output[451] = Proxy_RxByComUHRZEIT_DATUMInfo.DISP_HR;
    output[452] = Proxy_RxByComUHRZEIT_DATUMInfo.DISP_SEC;
    output[453] = Proxy_RxByComUHRZEIT_DATUMInfo.DISP_MN;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Proxy_RxByCom_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
