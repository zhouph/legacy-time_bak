#define S_FUNCTION_NAME      Proxy_Tx_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          56
#define WidthOutputPort         208

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Proxy_Tx.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Proxy_TxEemCtrlInhibitData.Eem_BBS_DegradeModeFail = input[0];
    Proxy_TxEemCtrlInhibitData.Eem_BBS_DefectiveModeFail = input[1];
    Proxy_TxCanTxInfo.TqIntvTCS = input[2];
    Proxy_TxCanTxInfo.TqIntvMsr = input[3];
    Proxy_TxCanTxInfo.TqIntvSlowTCS = input[4];
    Proxy_TxCanTxInfo.MinGear = input[5];
    Proxy_TxCanTxInfo.MaxGear = input[6];
    Proxy_TxCanTxInfo.TcsReq = input[7];
    Proxy_TxCanTxInfo.TcsCtrl = input[8];
    Proxy_TxCanTxInfo.AbsAct = input[9];
    Proxy_TxCanTxInfo.TcsGearShiftChr = input[10];
    Proxy_TxCanTxInfo.EspCtrl = input[11];
    Proxy_TxCanTxInfo.MsrReq = input[12];
    Proxy_TxCanTxInfo.TcsProductInfo = input[13];
    Proxy_TxPdt5msRawInfo.PdtSig = input[14];
    Proxy_TxPdf5msRawInfo.PdfSig = input[15];
    Proxy_TxTarRgnBrkTqInfo.TarRgnBrkTq = input[16];
    Proxy_TxTarRgnBrkTqInfo.EstTotBrkForce = input[17];
    Proxy_TxTarRgnBrkTqInfo.EstHydBrkForce = input[18];
    Proxy_TxTarRgnBrkTqInfo.EhbStat = input[19];
    Proxy_TxEscSwtStInfo.EscDisabledBySwt = input[20];
    Proxy_TxEscSwtStInfo.TcsDisabledBySwt = input[21];
    Proxy_TxWhlSpdInfo.FlWhlSpd = input[22];
    Proxy_TxWhlSpdInfo.FrWhlSpd = input[23];
    Proxy_TxWhlSpdInfo.RlWhlSpd = input[24];
    Proxy_TxWhlSpdInfo.RrlWhlSpd = input[25];
    Proxy_TxWhlEdgeCntInfo.FlWhlEdgeCnt = input[26];
    Proxy_TxWhlEdgeCntInfo.FrWhlEdgeCnt = input[27];
    Proxy_TxWhlEdgeCntInfo.RlWhlEdgeCnt = input[28];
    Proxy_TxWhlEdgeCntInfo.RrWhlEdgeCnt = input[29];
    Proxy_TxPistP5msRawInfo.PistPSig = input[30];
    Proxy_TxLogicEepDataInfo.KPdtOffsEolReadVal = input[31];
    Proxy_TxLogicEepDataInfo.ReadInvld = input[32];
    Proxy_TxEemLampData.Eem_Lamp_EBDLampRequest = input[33];
    Proxy_TxEemLampData.Eem_Lamp_ABSLampRequest = input[34];
    Proxy_TxEemLampData.Eem_Lamp_TCSLampRequest = input[35];
    Proxy_TxEemLampData.Eem_Lamp_TCSOffLampRequest = input[36];
    Proxy_TxEemLampData.Eem_Lamp_VDCLampRequest = input[37];
    Proxy_TxEemLampData.Eem_Lamp_VDCOffLampRequest = input[38];
    Proxy_TxEemLampData.Eem_Lamp_HDCLampRequest = input[39];
    Proxy_TxEemLampData.Eem_Lamp_BBSBuzzorRequest = input[40];
    Proxy_TxEemLampData.Eem_Lamp_RBCSLampRequest = input[41];
    Proxy_TxEemLampData.Eem_Lamp_AHBLampRequest = input[42];
    Proxy_TxEemLampData.Eem_Lamp_ServiceLampRequest = input[43];
    Proxy_TxEemLampData.Eem_Lamp_TCSFuncLampRequest = input[44];
    Proxy_TxEemLampData.Eem_Lamp_VDCFuncLampRequest = input[45];
    Proxy_TxEemLampData.Eem_Lamp_AVHLampRequest = input[46];
    Proxy_TxEemLampData.Eem_Lamp_AVHILampRequest = input[47];
    Proxy_TxEemLampData.Eem_Lamp_ACCEnable = input[48];
    Proxy_TxEemLampData.Eem_Lamp_CDMEnable = input[49];
    Proxy_TxEemLampData.Eem_Buzzor_On = input[50];
    Proxy_TxEcuModeSts = input[51];
    Proxy_TxIgnOnOffSts = input[52];
    Proxy_TxDiagSci = input[53];
    Proxy_TxDiagAhbSci = input[54];
    Proxy_TxFuncInhibitProxySts = input[55];

    Proxy_Tx();


    output[0] = Proxy_TxTxESCSensorInfo.EstimatedYaw;
    output[1] = Proxy_TxTxESCSensorInfo.EstimatedAY;
    output[2] = Proxy_TxTxESCSensorInfo.A_long_1_1000g;
    output[3] = Proxy_TxTxYawCbitInfo.TestYawRateSensor;
    output[4] = Proxy_TxTxYawCbitInfo.TestAcclSensor;
    output[5] = Proxy_TxTxYawCbitInfo.YawSerialNumberReq;
    output[6] = Proxy_TxTxWhlSpdInfo.WhlSpdFL;
    output[7] = Proxy_TxTxWhlSpdInfo.WhlSpdFR;
    output[8] = Proxy_TxTxWhlSpdInfo.WhlSpdRL;
    output[9] = Proxy_TxTxWhlSpdInfo.WhlSpdRR;
    output[10] = Proxy_TxTxWhlPulInfo.WhlPulFL;
    output[11] = Proxy_TxTxWhlPulInfo.WhlPulFR;
    output[12] = Proxy_TxTxWhlPulInfo.WhlPulRL;
    output[13] = Proxy_TxTxWhlPulInfo.WhlPulRR;
    output[14] = Proxy_TxTxTcs5Info.CfBrkAbsWLMP;
    output[15] = Proxy_TxTxTcs5Info.CfBrkEbdWLMP;
    output[16] = Proxy_TxTxTcs5Info.CfBrkTcsWLMP;
    output[17] = Proxy_TxTxTcs5Info.CfBrkTcsFLMP;
    output[18] = Proxy_TxTxTcs5Info.CfBrkAbsDIAG;
    output[19] = Proxy_TxTxTcs5Info.CrBrkWheelFL_KMH;
    output[20] = Proxy_TxTxTcs5Info.CrBrkWheelFR_KMH;
    output[21] = Proxy_TxTxTcs5Info.CrBrkWheelRL_KMH;
    output[22] = Proxy_TxTxTcs5Info.CrBrkWheelRR_KMH;
    output[23] = Proxy_TxTxTcs1Info.CfBrkTcsREQ;
    output[24] = Proxy_TxTxTcs1Info.CfBrkTcsPAS;
    output[25] = Proxy_TxTxTcs1Info.CfBrkTcsDEF;
    output[26] = Proxy_TxTxTcs1Info.CfBrkTcsCTL;
    output[27] = Proxy_TxTxTcs1Info.CfBrkAbsACT;
    output[28] = Proxy_TxTxTcs1Info.CfBrkAbsDEF;
    output[29] = Proxy_TxTxTcs1Info.CfBrkEbdDEF;
    output[30] = Proxy_TxTxTcs1Info.CfBrkTcsGSC;
    output[31] = Proxy_TxTxTcs1Info.CfBrkEspPAS;
    output[32] = Proxy_TxTxTcs1Info.CfBrkEspDEF;
    output[33] = Proxy_TxTxTcs1Info.CfBrkEspCTL;
    output[34] = Proxy_TxTxTcs1Info.CfBrkMsrREQ;
    output[35] = Proxy_TxTxTcs1Info.CfBrkTcsMFRN;
    output[36] = Proxy_TxTxTcs1Info.CfBrkMinGEAR;
    output[37] = Proxy_TxTxTcs1Info.CfBrkMaxGEAR;
    output[38] = Proxy_TxTxTcs1Info.BrakeLight;
    output[39] = Proxy_TxTxTcs1Info.HacCtl;
    output[40] = Proxy_TxTxTcs1Info.HacPas;
    output[41] = Proxy_TxTxTcs1Info.HacDef;
    output[42] = Proxy_TxTxTcs1Info.CrBrkTQI_PC;
    output[43] = Proxy_TxTxTcs1Info.CrBrkTQIMSR_PC;
    output[44] = Proxy_TxTxTcs1Info.CrBrkTQISLW_PC;
    output[45] = Proxy_TxTxTcs1Info.CrEbsMSGCHKSUM;
    output[46] = Proxy_TxTxSasCalInfo.CalSas_CCW;
    output[47] = Proxy_TxTxSasCalInfo.CalSas_Lws_CID;
    output[48] = Proxy_TxTxEsp2Info.LatAccel;
    output[49] = Proxy_TxTxEsp2Info.LatAccelStat;
    output[50] = Proxy_TxTxEsp2Info.LatAccelDiag;
    output[51] = Proxy_TxTxEsp2Info.LongAccel;
    output[52] = Proxy_TxTxEsp2Info.LongAccelStat;
    output[53] = Proxy_TxTxEsp2Info.LongAccelDiag;
    output[54] = Proxy_TxTxEsp2Info.YawRate;
    output[55] = Proxy_TxTxEsp2Info.YawRateStat;
    output[56] = Proxy_TxTxEsp2Info.YawRateDiag;
    output[57] = Proxy_TxTxEsp2Info.CylPres;
    output[58] = Proxy_TxTxEsp2Info.CylPresStat;
    output[59] = Proxy_TxTxEsp2Info.CylPresDiag;
    output[60] = Proxy_TxTxEbs1Info.CfBrkEbsStat;
    output[61] = Proxy_TxTxEbs1Info.CrBrkRegenTQLimit_NM;
    output[62] = Proxy_TxTxEbs1Info.CrBrkEstTot_NM;
    output[63] = Proxy_TxTxEbs1Info.CfBrkSnsFail;
    output[64] = Proxy_TxTxEbs1Info.CfBrkRbsWLamp;
    output[65] = Proxy_TxTxEbs1Info.CfBrkVacuumSysDef;
    output[66] = Proxy_TxTxEbs1Info.CrBrkEstHyd_NM;
    output[67] = Proxy_TxTxEbs1Info.CrBrkStkDep_PC;
    output[68] = Proxy_TxTxEbs1Info.CfBrkPgmRun1;
    output[69] = Proxy_TxTxAhb1Info.CfAhbWLMP;
    output[70] = Proxy_TxTxAhb1Info.CfAhbDiag;
    output[71] = Proxy_TxTxAhb1Info.CfAhbAct;
    output[72] = Proxy_TxTxAhb1Info.CfAhbDef;
    output[73] = Proxy_TxTxAhb1Info.CfAhbSLMP;
    output[74] = Proxy_TxTxAhb1Info.CfBrkAhbSTDep_PC;
    output[75] = Proxy_TxTxAhb1Info.CfBrkBuzzR;
    output[76] = Proxy_TxTxAhb1Info.CfBrkPedalCalStatus;
    output[77] = Proxy_TxTxAhb1Info.CfBrkAhbSnsFail;
    output[78] = Proxy_TxTxAhb1Info.WhlSpdFLAhb;
    output[79] = Proxy_TxTxAhb1Info.WhlSpdFRAhb;
    output[80] = Proxy_TxTxAhb1Info.CrAhbMsgChkSum;
    output[81] = Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.QU_TAR_REPAT_XTRQ_FTAX_BAX_ACT;
    output[82] = Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.TAR_REPAT_XTRQ_FTAX_BAX_ACT;
    output[83] = Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.TAR_WMOM_PT_SUM_STAB_FAST;
    output[84] = Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.ST_ECU_RQ_WMOM_PT_SUM_STAB;
    output[85] = Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.QU_RQ_ILK_PT;
    output[86] = Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.QU_TAR_WMOM_PT_SUM_STAB;
    output[87] = Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.TAR_WMOM_PT_SUM_STAB;
    output[88] = Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.ST_ECU_AVL_BRTORQ_SUM;
    output[89] = Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_SER_PRMSN_DBC;
    output[90] = Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_SER_PRF_BRK;
    output[91] = Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.AVL_BRTORQ_SUM_DVCH;
    output[92] = Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.AVL_BRTORQ_SUM;
    output[93] = Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_AVL_BRTORQ_SUM;
    output[94] = Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_AVL_BRTORQ_SUM_DVCH;
    output[95] = Proxy_TxAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_FLH;
    output[96] = Proxy_TxAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_RRH;
    output[97] = Proxy_TxAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_FRH;
    output[98] = Proxy_TxAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_RRH;
    output[99] = Proxy_TxAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_RLH;
    output[100] = Proxy_TxAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_FLH;
    output[101] = Proxy_TxAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_RLH;
    output[102] = Proxy_TxAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_FRH;
    output[103] = Proxy_TxAVL_RPM_WHLInfo.AVL_RPM_WHL_RLH;
    output[104] = Proxy_TxAVL_RPM_WHLInfo.AVL_RPM_WHL_RRH;
    output[105] = Proxy_TxAVL_RPM_WHLInfo.AVL_RPM_WHL_FLH;
    output[106] = Proxy_TxAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_FLH;
    output[107] = Proxy_TxAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_FRH;
    output[108] = Proxy_TxAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_RLH;
    output[109] = Proxy_TxAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_RRH;
    output[110] = Proxy_TxAVL_RPM_WHLInfo.AVL_RPM_WHL_FRH;
    output[111] = Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo.QU_TAR_WMOM_PT_SUM_RECUP;
    output[112] = Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo.TAR_WMOM_PT_SUM_RECUP;
    output[113] = Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo.ST_ECU_RQ_WMOM_PT_SUM_RECUP;
    output[114] = Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info.ST_ECU_ST_STAB_DSC;
    output[115] = Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info.ST_BRG_MSA;
    output[116] = Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info.ST_BRG_DV;
    output[117] = Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info.ST_UNRU_DSC;
    output[118] = Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info.QU_FN_FDR;
    output[119] = Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info.QU_FN_ABS;
    output[120] = Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info.QU_FN_ASC;
    output[121] = Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info.PRD_TIM_VEHSS;
    output[122] = Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.QU_SER_ECBA;
    output[123] = Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.TAR_BRTORQ_SUM_COOTD;
    output[124] = Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.AVL_WAY_BRKFA;
    output[125] = Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.AVL_FORC_BRKFA;
    output[126] = Proxy_TxAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_RLH;
    output[127] = Proxy_TxAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_RRH;
    output[128] = Proxy_TxAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_FLH;
    output[129] = Proxy_TxAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_FRH;
    output[130] = Proxy_TxAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_FLH;
    output[131] = Proxy_TxAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_RRH;
    output[132] = Proxy_TxAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_FRH;
    output[133] = Proxy_TxAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_RLH;
    output[134] = Proxy_TxTAR_LTRQD_BAXInfo.TAR_LTRQD_BAX;
    output[135] = Proxy_TxTAR_LTRQD_BAXInfo.LIM_MAX_LTRQD_BAX;
    output[136] = Proxy_TxST_YMRInfo.QU_SER_CLCTR_YMR;
    output[137] = Proxy_TxWEGSTRECKEInfo.MILE_FLH;
    output[138] = Proxy_TxWEGSTRECKEInfo.MILE_FRH;
    output[139] = Proxy_TxWEGSTRECKEInfo.MILE_INTM_TOMSRM;
    output[140] = Proxy_TxWEGSTRECKEInfo.MILE;
    output[141] = Proxy_TxDISP_CC_DRDY_00Info.ST_IDC_CC_DRDY_00;
    output[142] = Proxy_TxDISP_CC_DRDY_00Info.ST_CC_DRDY_00;
    output[143] = Proxy_TxDISP_CC_DRDY_00Info.TRANF_CC_DRDY_00;
    output[144] = Proxy_TxDISP_CC_DRDY_00Info.NO_CC_DRDY_00;
    output[145] = Proxy_TxDISP_CC_BYPA_00Info.TRANF_CC_BYPA_00;
    output[146] = Proxy_TxDISP_CC_BYPA_00Info.ST_IDC_CC_BYPA_00;
    output[147] = Proxy_TxDISP_CC_BYPA_00Info.ST_CC_BYPA_00;
    output[148] = Proxy_TxDISP_CC_BYPA_00Info.NO_CC_BYPA_00;
    output[149] = Proxy_TxST_VHSSInfo.ST_VEHSS;
    output[150] = Proxy_TxDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_1_ERR;
    output[151] = Proxy_TxDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_3_ERR;
    output[152] = Proxy_TxDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_4_ERR;
    output[153] = Proxy_TxDIAG_OBD_HYB_1Info.DIAG_OBD_HYB_1_MUX_MAX;
    output[154] = Proxy_TxDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_2_ERR;
    output[155] = Proxy_TxDIAG_OBD_HYB_1Info.DIAG_OBD_HYB_1_MUX_IMME;
    output[156] = Proxy_TxSU_DSCInfo.SU_DSC_WSS;
    output[157] = Proxy_TxST_TYR_RDCInfo.QU_TPL_RDC;
    output[158] = Proxy_TxST_TYR_RDCInfo.QU_FN_TYR_INFO_RDC;
    output[159] = Proxy_TxST_TYR_RDCInfo.QU_TFAI_RDC;
    output[160] = Proxy_TxST_TYR_RPAInfo.QU_TFAI_RPA;
    output[161] = Proxy_TxST_TYR_RPAInfo.QU_FN_TYR_INFO_RPA;
    output[162] = Proxy_TxST_TYR_RPAInfo.QU_TPL_RPA;
    output[163] = Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_2;
    output[164] = Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_1;
    output[165] = Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_3;
    output[166] = Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_4;
    output[167] = Proxy_TxST_TYR_2Info.QU_RDC_INIT_DISP;
    output[168] = Proxy_TxAVL_T_TYRInfo.AVL_TEMP_TYR_RRH;
    output[169] = Proxy_TxAVL_T_TYRInfo.AVL_TEMP_TYR_FLH;
    output[170] = Proxy_TxAVL_T_TYRInfo.AVL_TEMP_TYR_RLH;
    output[171] = Proxy_TxAVL_T_TYRInfo.AVL_TEMP_TYR_FRH;
    output[172] = Proxy_TxTEMP_BRKInfo.TEMP_BRDSK_RLH_VRFD;
    output[173] = Proxy_TxTEMP_BRKInfo.QU_TEMP_BRDSK_RLH_VRFD;
    output[174] = Proxy_TxTEMP_BRKInfo.TEMP_BRDSK_RRH_VRFD;
    output[175] = Proxy_TxTEMP_BRKInfo.QU_TEMP_BRDSK_RRH_VRFD;
    output[176] = Proxy_TxAVL_P_TYRInfo.AVL_P_TYR_FRH;
    output[177] = Proxy_TxAVL_P_TYRInfo.AVL_P_TYR_FLH;
    output[178] = Proxy_TxAVL_P_TYRInfo.AVL_P_TYR_RRH;
    output[179] = Proxy_TxAVL_P_TYRInfo.AVL_P_TYR_RLH;
    output[180] = Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_13;
    output[181] = Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_04;
    output[182] = Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_05;
    output[183] = Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_06;
    output[184] = Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_01;
    output[185] = Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_02;
    output[186] = Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_03;
    output[187] = Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_07;
    output[188] = Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_11;
    output[189] = Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_10;
    output[190] = Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_09;
    output[191] = Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_15;
    output[192] = Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_14;
    output[193] = Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_12;
    output[194] = Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_00;
    output[195] = Proxy_TxFR_DBG_DSCInfo.BDTM_TA_FR;
    output[196] = Proxy_TxFR_DBG_DSCInfo.BDTM_TA_RL;
    output[197] = Proxy_TxFR_DBG_DSCInfo.BDTM_TA_RR;
    output[198] = Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_08;
    output[199] = Proxy_TxFR_DBG_DSCInfo.ST_STATE;
    output[200] = Proxy_TxFR_DBG_DSCInfo.BDTM_TA_FL;
    output[201] = Proxy_TxFR_DBG_DSCInfo.ABSRef;
    output[202] = Proxy_TxFR_DBG_DSCInfo.LOW_VOLTAGE;
    output[203] = Proxy_TxFR_DBG_DSCInfo.DEBUG_PCIB;
    output[204] = Proxy_TxFR_DBG_DSCInfo.DBG_DSC;
    output[205] = Proxy_TxFR_DBG_DSCInfo.LATACC;
    output[206] = Proxy_TxFR_DBG_DSCInfo.TRIGGER_FS;
    output[207] = Proxy_TxFR_DBG_DSCInfo.TRIGGER_IS;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Proxy_Tx_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
