#define S_FUNCTION_NAME      Proxy_Rx_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          391
#define WidthOutputPort         77

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Proxy_Rx.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Proxy_RxRxBms1Info.Bms1SocPc = input[0];
    Proxy_RxRxYawSerialInfo.YawSerialNum_0 = input[1];
    Proxy_RxRxYawSerialInfo.YawSerialNum_1 = input[2];
    Proxy_RxRxYawSerialInfo.YawSerialNum_2 = input[3];
    Proxy_RxRxYawAccInfo.YawRateValidData = input[4];
    Proxy_RxRxYawAccInfo.YawRateSelfTestStatus = input[5];
    Proxy_RxRxYawAccInfo.YawRateSignal_0 = input[6];
    Proxy_RxRxYawAccInfo.YawRateSignal_1 = input[7];
    Proxy_RxRxYawAccInfo.SensorOscFreqDev = input[8];
    Proxy_RxRxYawAccInfo.Gyro_Fail = input[9];
    Proxy_RxRxYawAccInfo.Raster_Fail = input[10];
    Proxy_RxRxYawAccInfo.Eep_Fail = input[11];
    Proxy_RxRxYawAccInfo.Batt_Range_Err = input[12];
    Proxy_RxRxYawAccInfo.Asic_Fail = input[13];
    Proxy_RxRxYawAccInfo.Accel_Fail = input[14];
    Proxy_RxRxYawAccInfo.Ram_Fail = input[15];
    Proxy_RxRxYawAccInfo.Rom_Fail = input[16];
    Proxy_RxRxYawAccInfo.Ad_Fail = input[17];
    Proxy_RxRxYawAccInfo.Osc_Fail = input[18];
    Proxy_RxRxYawAccInfo.Watchdog_Rst = input[19];
    Proxy_RxRxYawAccInfo.Plaus_Err_Pst = input[20];
    Proxy_RxRxYawAccInfo.RollingCounter = input[21];
    Proxy_RxRxYawAccInfo.Can_Func_Err = input[22];
    Proxy_RxRxYawAccInfo.AccelEratorRateSig_0 = input[23];
    Proxy_RxRxYawAccInfo.AccelEratorRateSig_1 = input[24];
    Proxy_RxRxYawAccInfo.LatAccValidData = input[25];
    Proxy_RxRxYawAccInfo.LatAccSelfTestStatus = input[26];
    Proxy_RxRxTcu6Info.ShiftClass_Ccan = input[27];
    Proxy_RxRxTcu5Info.Typ = input[28];
    Proxy_RxRxTcu5Info.GearTyp = input[29];
    Proxy_RxRxTcu1Info.Targe = input[30];
    Proxy_RxRxTcu1Info.GarChange = input[31];
    Proxy_RxRxTcu1Info.Flt = input[32];
    Proxy_RxRxTcu1Info.GarSelDisp = input[33];
    Proxy_RxRxTcu1Info.TQRedReq_PC = input[34];
    Proxy_RxRxTcu1Info.TQRedReqSlw_PC = input[35];
    Proxy_RxRxTcu1Info.TQIncReq_PC = input[36];
    Proxy_RxRxSasInfo.Angle = input[37];
    Proxy_RxRxSasInfo.Speed = input[38];
    Proxy_RxRxSasInfo.Ok = input[39];
    Proxy_RxRxSasInfo.Cal = input[40];
    Proxy_RxRxSasInfo.Trim = input[41];
    Proxy_RxRxSasInfo.CheckSum = input[42];
    Proxy_RxRxSasInfo.MsgCount = input[43];
    Proxy_RxRxMcu2Info.Flt = input[44];
    Proxy_RxRxMcu1Info.MoTestTQ_PC = input[45];
    Proxy_RxRxMcu1Info.MotActRotSpd_RPM = input[46];
    Proxy_RxRxLongAccInfo.IntSenFltSymtmActive = input[47];
    Proxy_RxRxLongAccInfo.IntSenFaultPresent = input[48];
    Proxy_RxRxLongAccInfo.LongAccSenCirErrPre = input[49];
    Proxy_RxRxLongAccInfo.LonACSenRanChkErrPre = input[50];
    Proxy_RxRxLongAccInfo.LongRollingCounter = input[51];
    Proxy_RxRxLongAccInfo.IntTempSensorFault = input[52];
    Proxy_RxRxLongAccInfo.LongAccInvalidData = input[53];
    Proxy_RxRxLongAccInfo.LongAccSelfTstStatus = input[54];
    Proxy_RxRxLongAccInfo.LongAccRateSignal_0 = input[55];
    Proxy_RxRxLongAccInfo.LongAccRateSignal_1 = input[56];
    Proxy_RxRxHcu5Info.HevMod = input[57];
    Proxy_RxRxHcu3Info.TmIntQcMDBINV_PC = input[58];
    Proxy_RxRxHcu3Info.MotTQCMC_PC = input[59];
    Proxy_RxRxHcu3Info.MotTQCMDBINV_PC = input[60];
    Proxy_RxRxHcu2Info.ServiceMod = input[61];
    Proxy_RxRxHcu2Info.RegenENA = input[62];
    Proxy_RxRxHcu2Info.RegenBRKTQ_NM = input[63];
    Proxy_RxRxHcu2Info.CrpTQ_NM = input[64];
    Proxy_RxRxHcu2Info.WhlDEMTQ_NM = input[65];
    Proxy_RxRxHcu1Info.EngCltStat = input[66];
    Proxy_RxRxHcu1Info.HEVRDY = input[67];
    Proxy_RxRxHcu1Info.EngTQCmdBinV_PC = input[68];
    Proxy_RxRxHcu1Info.EngTQCmd_PC = input[69];
    Proxy_RxRxFact1Info.OutTemp_SNR_C = input[70];
    Proxy_RxRxEms3Info.EngColTemp_C = input[71];
    Proxy_RxRxEms2Info.EngSpdErr = input[72];
    Proxy_RxRxEms2Info.AccPedDep_PC = input[73];
    Proxy_RxRxEms2Info.Tps_PC = input[74];
    Proxy_RxRxEms1Info.TqStd_NM = input[75];
    Proxy_RxRxEms1Info.ActINDTQ_PC = input[76];
    Proxy_RxRxEms1Info.EngSpd_RPM = input[77];
    Proxy_RxRxEms1Info.IndTQ_PC = input[78];
    Proxy_RxRxEms1Info.FrictTQ_PC = input[79];
    Proxy_RxRxClu2Info.IGN_SW = input[80];
    Proxy_RxRxClu1Info.P_Brake_Act = input[81];
    Proxy_RxRxClu1Info.Cf_Clu_BrakeFluIDSW = input[82];
    Proxy_RxRxMsgOkFlgInfo.Bms1MsgOkFlg = input[83];
    Proxy_RxRxMsgOkFlgInfo.YawSerialMsgOkFlg = input[84];
    Proxy_RxRxMsgOkFlgInfo.YawAccMsgOkFlg = input[85];
    Proxy_RxRxMsgOkFlgInfo.Tcu6MsgOkFlg = input[86];
    Proxy_RxRxMsgOkFlgInfo.Tcu5MsgOkFlg = input[87];
    Proxy_RxRxMsgOkFlgInfo.Tcu1MsgOkFlg = input[88];
    Proxy_RxRxMsgOkFlgInfo.SasMsgOkFlg = input[89];
    Proxy_RxRxMsgOkFlgInfo.Mcu2MsgOkFlg = input[90];
    Proxy_RxRxMsgOkFlgInfo.Mcu1MsgOkFlg = input[91];
    Proxy_RxRxMsgOkFlgInfo.LongAccMsgOkFlg = input[92];
    Proxy_RxRxMsgOkFlgInfo.Hcu5MsgOkFlg = input[93];
    Proxy_RxRxMsgOkFlgInfo.Hcu3MsgOkFlg = input[94];
    Proxy_RxRxMsgOkFlgInfo.Hcu2MsgOkFlg = input[95];
    Proxy_RxRxMsgOkFlgInfo.Hcu1MsgOkFlg = input[96];
    Proxy_RxRxMsgOkFlgInfo.Fatc1MsgOkFlg = input[97];
    Proxy_RxRxMsgOkFlgInfo.Ems3MsgOkFlg = input[98];
    Proxy_RxRxMsgOkFlgInfo.Ems2MsgOkFlg = input[99];
    Proxy_RxRxMsgOkFlgInfo.Ems1MsgOkFlg = input[100];
    Proxy_RxRxMsgOkFlgInfo.Clu2MsgOkFlg = input[101];
    Proxy_RxRxMsgOkFlgInfo.Clu1MsgOkFlg = input[102];
    Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.ST_ECU_ST_REPAT_XTRQ_FTAX_BAX = input[103];
    Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.QU_SER_REPAT_XTRQ_FTAX_BAX_ACT = input[104];
    Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.AVL_REPAT_XTRQ_FTAX_BAX = input[105];
    Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.QU_AVL_REPAT_XTRQ_FTAX_BAX = input[106];
    Proxy_RxAVL_LTRQD_BAXInfo.QU_SER_LTRQD_BAX = input[107];
    Proxy_RxAVL_LTRQD_BAXInfo.QU_AVL_LTRQD_BAX = input[108];
    Proxy_RxAVL_LTRQD_BAXInfo.AVL_LTRQD_BAX = input[109];
    Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.AVL_TORQ_CRSH_DMEE = input[110];
    Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.AVL_TORQ_CRSH = input[111];
    Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.ST_SAIL_DRV_2 = input[112];
    Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.QU_AVL_RPM_ENG_CRSH = input[113];
    Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.AVL_RPM_ENG_CRSH = input[114];
    Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.AVL_ANG_ACPD = input[115];
    Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.QU_AVL_ANG_ACPD = input[116];
    Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.ST_ECU_ANG_ACPD = input[117];
    Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.AVL_ANG_ACPD_VIRT = input[118];
    Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.ECO_ANG_ACPD = input[119];
    Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.GRAD_AVL_ANG_ACPD = input[120];
    Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.ST_INTF_DRASY = input[121];
    Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.AVL_RPM_BAX_RED = input[122];
    Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.ST_PENG_PT = input[123];
    Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.ST_AVAI_INTV_PT_DRS = input[124];
    Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.QU_AVL_RPM_BAX_RED = input[125];
    Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.QU_SER_WMOM_PT_SUM_DRS = input[126];
    Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.QU_SER_WMOM_PT_SUM_STAB = input[127];
    Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.ST_ECU_WMOM_DRV_5 = input[128];
    Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.TAR_WMOM_PT_SUM_COOTD = input[129];
    Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.QU_SER_COOR_TORQ_BDRV = input[130];
    Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.ST_ECU_WMOM_DRV_4 = input[131];
    Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.ST_DRVDIR_DVCH = input[132];
    Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.QU_AVL_WMOM_PT_SUM = input[133];
    Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_ERR_AMP = input[134];
    Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.REIN_PT = input[135];
    Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM = input[136];
    Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.ST_ECU_WMOM_DRV_1 = input[137];
    Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_MAX = input[138];
    Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_FAST_TOP = input[139];
    Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_FAST_BOT = input[140];
    Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.ST_ECU_WMOM_DRV_2 = input[141];
    Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.QU_REIN_PT = input[142];
    Proxy_RxHGLV_VEH_FILTInfo.HGLV_VEH_FILT_FRH = input[143];
    Proxy_RxHGLV_VEH_FILTInfo.HGLV_VEH_FILT_FLH = input[144];
    Proxy_RxHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_FRH = input[145];
    Proxy_RxHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_FLH = input[146];
    Proxy_RxHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_RRH = input[147];
    Proxy_RxHGLV_VEH_FILTInfo.HGLV_VEH_FILT_RLH = input[148];
    Proxy_RxHGLV_VEH_FILTInfo.HGLV_VEH_FILT_RRH = input[149];
    Proxy_RxHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_RLH = input[150];
    Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.ATTAV_ESTI = input[151];
    Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.ATTA_ESTI_ERR_AMP = input[152];
    Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.VY_ESTI_ERR_AMP = input[153];
    Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.VY_ESTI = input[154];
    Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.ATTAV_ESTI_ERR_AMP = input[155];
    Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.QU_VEH_DYNMC_DT_ESTI = input[156];
    Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.ATTA_ESTI = input[157];
    Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.QU_ACLNY_COG = input[158];
    Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNY_COG_ERR_AMP = input[159];
    Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNX_COG = input[160];
    Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNY_COG = input[161];
    Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNX_COG_ERR_AMP = input[162];
    Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.QU_ACLNX_COG = input[163];
    Proxy_RxV_VEH_V_VEH_2Info.QU_V_VEH_COG = input[164];
    Proxy_RxV_VEH_V_VEH_2Info.DVCO_VEH = input[165];
    Proxy_RxV_VEH_V_VEH_2Info.ST_V_VEH_NSS = input[166];
    Proxy_RxV_VEH_V_VEH_2Info.V_VEH_COG = input[167];
    Proxy_RxDT_DRDYSEN_EXT_VYAW_VEHInfo.VYAW_VEH_ERR_AMP = input[168];
    Proxy_RxDT_DRDYSEN_EXT_VYAW_VEHInfo.QU_VYAW_VEH = input[169];
    Proxy_RxDT_DRDYSEN_EXT_VYAW_VEHInfo.VYAW_VEH = input[170];
    Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo.QU_AVL_TRGR_RW = input[171];
    Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo.AVL_TRGR_RW = input[172];
    Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo.AVL_LOGR_RW_FAST = input[173];
    Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo.QU_AVL_LOGR_RW = input[174];
    Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo.AVL_LOGR_RW = input[175];
    Proxy_RxAVL_STEA_FTAXInfo.QU_AVL_STEA_FTAX_PNI = input[176];
    Proxy_RxAVL_STEA_FTAXInfo.AVL_STEA_FTAX_PNI = input[177];
    Proxy_RxAVL_STEA_FTAXInfo.QU_AVL_STEA_FTAX_WHL = input[178];
    Proxy_RxAVL_STEA_FTAXInfo.AVL_STEA_FTAX_WHL = input[179];
    Proxy_RxAVL_STEA_FTAXInfo.AVL_STEA_FTAX_WHL_ERR_AMP = input[180];
    Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_PRMSN_DBC_DCRN_MAX = input[181];
    Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_PRF_BRK = input[182];
    Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_V_HDC = input[183];
    Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_PRMSN_DBC_DCRN_GRAD_MAX = input[184];
    Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_PRMSN_DBC_TR_THRV = input[185];
    Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_BRTORQ_SUM = input[186];
    Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_BRTORQ_SUM = input[187];
    Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_PRMSN_DBC = input[188];
    Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.RQ_TAO_SSM = input[189];
    Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_DTORQ_BOT = input[190];
    Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_RECUP_MAX = input[191];
    Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_ILS = input[192];
    Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_DTORQ_TOP = input[193];
    Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info.ST_ECU_WMOM_DRV_6 = input[194];
    Proxy_RxWMOM_DRV_7Info.ST_EL_DRVG = input[195];
    Proxy_RxWMOM_DRV_7Info.PRD_AVL_WMOM_PT_SUM_RECUP_MAX = input[196];
    Proxy_RxWMOM_DRV_7Info.ST_ECU_WMOM_DRV_7 = input[197];
    Proxy_RxWMOM_DRV_7Info.QU_SER_WMOM_PT_SUM_RECUP = input[198];
    Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.TAR_DIFF_BRTORQ_BAX_YMR = input[199];
    Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.FACT_TAR_COMPT_DRV_YMR = input[200];
    Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.QU_TAR_DIFF_BRTORQ_YMR = input[201];
    Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.TAR_DIFF_BRTORQ_FTAX_YMR = input[202];
    Proxy_RxDT_BRKSYS_ENGMGInfo.QU_AVL_LOWP_BRKFA = input[203];
    Proxy_RxDT_BRKSYS_ENGMGInfo.AVL_LOWP_BRKFA = input[204];
    Proxy_RxERRM_BN_UInfo.CTR_ERRM_BN_U = input[205];
    Proxy_RxCTR_CRInfo.ST_EXCE_ACLN_THRV = input[206];
    Proxy_RxCTR_CRInfo.CTR_PHTR_CR = input[207];
    Proxy_RxCTR_CRInfo.CTR_ITLI_CR = input[208];
    Proxy_RxCTR_CRInfo.CTR_CLSY_CR = input[209];
    Proxy_RxCTR_CRInfo.CTR_AUTOM_ECAL_CR = input[210];
    Proxy_RxCTR_CRInfo.CTR_SWO_EKP_CR = input[211];
    Proxy_RxCTR_CRInfo.CTR_PCSH_MST = input[212];
    Proxy_RxCTR_CRInfo.CTR_HAZW_CR = input[213];
    Proxy_RxKLEMMENInfo.ST_OP_MSA = input[214];
    Proxy_RxKLEMMENInfo.RQ_DRVG_RDI = input[215];
    Proxy_RxKLEMMENInfo.ST_KL_DBG = input[216];
    Proxy_RxKLEMMENInfo.ST_KL_50_MSA = input[217];
    Proxy_RxKLEMMENInfo.ST_SSP = input[218];
    Proxy_RxKLEMMENInfo.RWDT_BLS = input[219];
    Proxy_RxKLEMMENInfo.ST_STCD_PENG = input[220];
    Proxy_RxKLEMMENInfo.ST_KL = input[221];
    Proxy_RxKLEMMENInfo.ST_KL_DIV = input[222];
    Proxy_RxKLEMMENInfo.ST_VEH_CON = input[223];
    Proxy_RxKLEMMENInfo.ST_KL_30B = input[224];
    Proxy_RxKLEMMENInfo.CON_CLT_SW = input[225];
    Proxy_RxKLEMMENInfo.CTR_ENG_STOP = input[226];
    Proxy_RxKLEMMENInfo.ST_PLK = input[227];
    Proxy_RxKLEMMENInfo.ST_KL_15N = input[228];
    Proxy_RxKLEMMENInfo.ST_KL_KEY_VLD = input[229];
    Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_MES_TSTMP = input[230];
    Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.PCKG_ID = input[231];
    Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.SUPP_ID = input[232];
    Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_5 = input[233];
    Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_8 = input[234];
    Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_7 = input[235];
    Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_6 = input[236];
    Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_1 = input[237];
    Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.TYR_ID = input[238];
    Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_4 = input[239];
    Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_3 = input[240];
    Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_2 = input[241];
    Proxy_RxBEDIENUNG_WISCHERInfo.OP_WISW = input[242];
    Proxy_RxBEDIENUNG_WISCHERInfo.OP_WIPO = input[243];
    Proxy_RxDT_PT_2Info.ST_GRSEL_DRV = input[244];
    Proxy_RxDT_PT_2Info.TEMP_EOI_DRV = input[245];
    Proxy_RxDT_PT_2Info.RLS_ENGSTA = input[246];
    Proxy_RxDT_PT_2Info.RPM_ENG_MAX_ALW = input[247];
    Proxy_RxDT_PT_2Info.RDUC_DOCTR_RPM_DRV_2 = input[248];
    Proxy_RxDT_PT_2Info.TEMP_ENG_DRV = input[249];
    Proxy_RxDT_PT_2Info.ST_DRV_VEH = input[250];
    Proxy_RxDT_PT_2Info.ST_ECU_DT_PT_2 = input[251];
    Proxy_RxDT_PT_2Info.ST_IDLG_ENG_DRV = input[252];
    Proxy_RxDT_PT_2Info.ST_ILK_STRT_DRV = input[253];
    Proxy_RxDT_PT_2Info.ST_SW_CLT_DRV = input[254];
    Proxy_RxDT_PT_2Info.ST_ENG_RUN_DRV = input[255];
    Proxy_RxSTAT_ENG_STA_AUTOInfo.RQ_MSA_ENG_STA_1 = input[256];
    Proxy_RxSTAT_ENG_STA_AUTOInfo.RQ_MSA_ENG_STA_2 = input[257];
    Proxy_RxSTAT_ENG_STA_AUTOInfo.PSBTY_MSA_ENG_STOP_STA = input[258];
    Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_SHFT_MSA_ENGSTP = input[259];
    Proxy_RxSTAT_ENG_STA_AUTOInfo.RQ_SLIP_K0 = input[260];
    Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_TAR_PENG_CENG = input[261];
    Proxy_RxSTAT_ENG_STA_AUTOInfo.SPEC_TYP_ENGSTA = input[262];
    Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_RPM_CLCTR_MOT = input[263];
    Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_FN_MSA = input[264];
    Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_AVL_PENG_CENG_ENGMG = input[265];
    Proxy_RxSTAT_ENG_STA_AUTOInfo.DISP_REAS_PREV_SWO_CENG = input[266];
    Proxy_RxSTAT_ENG_STA_AUTOInfo.VARI_TYP_ENGSTA = input[267];
    Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_TAR_CENG = input[268];
    Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_AVAI_SAIL_DME = input[269];
    Proxy_RxST_ENERG_GENInfo.ST_LDST_GEN_DRV = input[270];
    Proxy_RxST_ENERG_GENInfo.DT_PCU_SCP = input[271];
    Proxy_RxST_ENERG_GENInfo.ST_GEN_DRV = input[272];
    Proxy_RxST_ENERG_GENInfo.AVL_I_GEN_DRV = input[273];
    Proxy_RxST_ENERG_GENInfo.LDST_GEN_DRV = input[274];
    Proxy_RxST_ENERG_GENInfo.ST_LDREL_GEN = input[275];
    Proxy_RxST_ENERG_GENInfo.ST_CHG_STOR = input[276];
    Proxy_RxST_ENERG_GENInfo.TEMP_BT_14V = input[277];
    Proxy_RxST_ENERG_GENInfo.ST_I_IBS = input[278];
    Proxy_RxST_ENERG_GENInfo.ST_SEP_STOR = input[279];
    Proxy_RxST_ENERG_GENInfo.ST_BN2_SCP = input[280];
    Proxy_RxDT_PT_1Info.RQ_SHPA_GRB_REGE_PAFI = input[281];
    Proxy_RxDT_PT_1Info.ST_RTIR_DRV = input[282];
    Proxy_RxDT_PT_1Info.CTR_SLCK_DRV = input[283];
    Proxy_RxDT_PT_1Info.RQ_STASS_ENGMG = input[284];
    Proxy_RxDT_PT_1Info.ST_CAT_HT = input[285];
    Proxy_RxDT_PT_1Info.RQ_SHPA_GRB_CHGBLC = input[286];
    Proxy_RxDT_PT_1Info.TAR_RPM_IDLG_DRV_EXT = input[287];
    Proxy_RxDT_PT_1Info.SLCTN_BUS_COMM_ENG_GRB = input[288];
    Proxy_RxDT_PT_1Info.TAR_RPM_IDLG_DRV = input[289];
    Proxy_RxDT_PT_1Info.ST_INFS_DRV = input[290];
    Proxy_RxDT_PT_1Info.ST_SW_WAUP_DRV = input[291];
    Proxy_RxDT_PT_1Info.AIP_ENG_DRV = input[292];
    Proxy_RxDT_PT_1Info.RDUC_DOCTR_RPM_DRV = input[293];
    Proxy_RxDT_GRDT_DRVInfo.ST_OPMO_GRDT_DRV = input[294];
    Proxy_RxDT_GRDT_DRVInfo.ST_RSTA_GRDT = input[295];
    Proxy_RxDIAG_OBD_ENGMG_ELInfo.RQ_MIL_DIAG_OBD_ENGMG_EL = input[296];
    Proxy_RxDIAG_OBD_ENGMG_ELInfo.RQ_RST_OBD_DIAG = input[297];
    Proxy_RxSTAT_CT_HABRInfo.ST_CT_HABR = input[298];
    Proxy_RxDT_PT_3Info.TRNRAO_BAX = input[299];
    Proxy_RxDT_PT_3Info.QU_TRNRAO_BAX = input[300];
    Proxy_RxEINHEITEN_BN2020Info.UN_TORQ_S_MOD = input[301];
    Proxy_RxEINHEITEN_BN2020Info.UN_PWR_S_MOD = input[302];
    Proxy_RxEINHEITEN_BN2020Info.UN_DATE_EXT = input[303];
    Proxy_RxEINHEITEN_BN2020Info.UN_COSP_EL = input[304];
    Proxy_RxEINHEITEN_BN2020Info.UN_TEMP = input[305];
    Proxy_RxEINHEITEN_BN2020Info.UN_AIP = input[306];
    Proxy_RxEINHEITEN_BN2020Info.LANG = input[307];
    Proxy_RxEINHEITEN_BN2020Info.UN_DATE = input[308];
    Proxy_RxEINHEITEN_BN2020Info.UN_T = input[309];
    Proxy_RxEINHEITEN_BN2020Info.UN_SPDM_DGTL = input[310];
    Proxy_RxEINHEITEN_BN2020Info.UN_MILE = input[311];
    Proxy_RxEINHEITEN_BN2020Info.UN_FU = input[312];
    Proxy_RxEINHEITEN_BN2020Info.UN_COSP = input[313];
    Proxy_RxA_TEMPInfo.TEMP_EX_UNFILT = input[314];
    Proxy_RxA_TEMPInfo.TEMP_EX = input[315];
    Proxy_RxWISCHERGESCHWINDIGKEITInfo.ST_RNSE = input[316];
    Proxy_RxWISCHERGESCHWINDIGKEITInfo.INT_RN = input[317];
    Proxy_RxWISCHERGESCHWINDIGKEITInfo.V_WI = input[318];
    Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_DRD_SFY_CTRL = input[319];
    Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_DVDR_VRFD = input[320];
    Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_DVDR_SFY_CTRL = input[321];
    Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_PSDR_SFY_CTRL = input[322];
    Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_PSD_SFY_CTRL = input[323];
    Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_DRD_VRFD = input[324];
    Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_PSDR_VRFD = input[325];
    Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_PSD_VRFD = input[326];
    Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY = input[327];
    Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.RQ_SW_DRDY_MMID = input[328];
    Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.RQ_SU_SW_DRDY = input[329];
    Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.RQ_SW_DRDY_KDIS = input[330];
    Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY_CHAS = input[331];
    Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY_DRV = input[332];
    Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY_STAB = input[333];
    Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.DISP_ST_DSC = input[334];
    Proxy_RxSTAT_ANHAENGERInfo.ST_SYNCN_HAZWCL_TRAI = input[335];
    Proxy_RxSTAT_ANHAENGERInfo.ST_RFLI_DF_TRAI = input[336];
    Proxy_RxSTAT_ANHAENGERInfo.ST_TRAI = input[337];
    Proxy_RxSTAT_ANHAENGERInfo.ST_DI_DF_TRAI = input[338];
    Proxy_RxSTAT_ANHAENGERInfo.ST_PO_AHV = input[339];
    Proxy_RxFZZSTDInfo.ST_ILK_ERRM_FZM = input[340];
    Proxy_RxFZZSTDInfo.ST_ENERG_FZM = input[341];
    Proxy_RxFZZSTDInfo.ST_BT_PROTE_WUP = input[342];
    Proxy_RxBEDIENUNG_FAHRWERKInfo.OP_MOD_TRCT_DSC = input[343];
    Proxy_RxBEDIENUNG_FAHRWERKInfo.OP_TPCT = input[344];
    Proxy_RxFAHRZEUGTYPInfo.QUAN_CYL = input[345];
    Proxy_RxFAHRZEUGTYPInfo.QUAN_GR = input[346];
    Proxy_RxFAHRZEUGTYPInfo.TYP_VEH = input[347];
    Proxy_RxFAHRZEUGTYPInfo.TYP_BODY = input[348];
    Proxy_RxFAHRZEUGTYPInfo.TYP_ENG = input[349];
    Proxy_RxFAHRZEUGTYPInfo.CLAS_PWR = input[350];
    Proxy_RxFAHRZEUGTYPInfo.TYP_CNT = input[351];
    Proxy_RxFAHRZEUGTYPInfo.TYP_STE = input[352];
    Proxy_RxFAHRZEUGTYPInfo.TYP_GRB = input[353];
    Proxy_RxFAHRZEUGTYPInfo.TYP_CAPA = input[354];
    Proxy_RxST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_RRH = input[355];
    Proxy_RxST_BLT_CT_SOCCUInfo.ST_BLTB_SW_RM = input[356];
    Proxy_RxST_BLT_CT_SOCCUInfo.ST_BLTB_SW_RRH = input[357];
    Proxy_RxST_BLT_CT_SOCCUInfo.ST_ERR_SEAT_MT_DR = input[358];
    Proxy_RxST_BLT_CT_SOCCUInfo.ST_BLTB_SW_DR = input[359];
    Proxy_RxST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_DR = input[360];
    Proxy_RxST_BLT_CT_SOCCUInfo.ST_BLTB_SW_RLH = input[361];
    Proxy_RxST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_RLH = input[362];
    Proxy_RxST_BLT_CT_SOCCUInfo.ST_BLTB_SW_PS = input[363];
    Proxy_RxST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_PS = input[364];
    Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_1 = input[365];
    Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_2 = input[366];
    Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_3 = input[367];
    Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_6 = input[368];
    Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_7 = input[369];
    Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_4 = input[370];
    Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_5 = input[371];
    Proxy_RxRELATIVZEITInfo.T_SEC_COU_REL = input[372];
    Proxy_RxRELATIVZEITInfo.T_DAY_COU_ABSL = input[373];
    Proxy_RxKILOMETERSTANDInfo.RNG = input[374];
    Proxy_RxKILOMETERSTANDInfo.ST_FLLV_FUTA_SPAR = input[375];
    Proxy_RxKILOMETERSTANDInfo.FLLV_FUTA_RH = input[376];
    Proxy_RxKILOMETERSTANDInfo.FLLV_FUTA_LH = input[377];
    Proxy_RxKILOMETERSTANDInfo.FLLV_FUTA = input[378];
    Proxy_RxKILOMETERSTANDInfo.MILE_KM = input[379];
    Proxy_RxUHRZEIT_DATUMInfo.DISP_DATE_WDAY = input[380];
    Proxy_RxUHRZEIT_DATUMInfo.DISP_DATE_DAY = input[381];
    Proxy_RxUHRZEIT_DATUMInfo.DISP_DATE_MON = input[382];
    Proxy_RxUHRZEIT_DATUMInfo.ST_DISP_CTI_DATE = input[383];
    Proxy_RxUHRZEIT_DATUMInfo.DISP_DATE_YR = input[384];
    Proxy_RxUHRZEIT_DATUMInfo.DISP_HR = input[385];
    Proxy_RxUHRZEIT_DATUMInfo.DISP_SEC = input[386];
    Proxy_RxUHRZEIT_DATUMInfo.DISP_MN = input[387];
    Proxy_RxEcuModeSts = input[388];
    Proxy_RxPCtrlAct = input[389];
    Proxy_RxFuncInhibitProxySts = input[390];

    Proxy_Rx();


    output[0] = Proxy_RxCanRxRegenInfo.HcuRegenEna;
    output[1] = Proxy_RxCanRxRegenInfo.HcuRegenBrkTq;
    output[2] = Proxy_RxCanRxAccelPedlInfo.AccelPedlVal;
    output[3] = Proxy_RxCanRxAccelPedlInfo.AccelPedlValErr;
    output[4] = Proxy_RxCanRxIdbInfo.EngMsgFault;
    output[5] = Proxy_RxCanRxIdbInfo.TarGearPosi;
    output[6] = Proxy_RxCanRxIdbInfo.GearSelDisp;
    output[7] = Proxy_RxCanRxIdbInfo.TcuSwiGs;
    output[8] = Proxy_RxCanRxIdbInfo.HcuServiceMod;
    output[9] = Proxy_RxCanRxIdbInfo.SubCanBusSigFault;
    output[10] = Proxy_RxCanRxIdbInfo.CoolantTemp;
    output[11] = Proxy_RxCanRxIdbInfo.CoolantTempErr;
    output[12] = Proxy_RxCanRxEngTempInfo.EngTemp;
    output[13] = Proxy_RxCanRxEngTempInfo.EngTempErr;
    output[14] = Proxy_RxCanRxEscInfo.Ax;
    output[15] = Proxy_RxCanRxEscInfo.YawRate;
    output[16] = Proxy_RxCanRxEscInfo.SteeringAngle;
    output[17] = Proxy_RxCanRxEscInfo.Ay;
    output[18] = Proxy_RxCanRxEscInfo.PbSwt;
    output[19] = Proxy_RxCanRxEscInfo.ClutchSwt;
    output[20] = Proxy_RxCanRxEscInfo.GearRSwt;
    output[21] = Proxy_RxCanRxEscInfo.EngActIndTq;
    output[22] = Proxy_RxCanRxEscInfo.EngRpm;
    output[23] = Proxy_RxCanRxEscInfo.EngIndTq;
    output[24] = Proxy_RxCanRxEscInfo.EngFrictionLossTq;
    output[25] = Proxy_RxCanRxEscInfo.EngStdTq;
    output[26] = Proxy_RxCanRxEscInfo.TurbineRpm;
    output[27] = Proxy_RxCanRxEscInfo.ThrottleAngle;
    output[28] = Proxy_RxCanRxEscInfo.TpsResol1000;
    output[29] = Proxy_RxCanRxEscInfo.PvAvCanResol1000;
    output[30] = Proxy_RxCanRxEscInfo.EngChr;
    output[31] = Proxy_RxCanRxEscInfo.EngVol;
    output[32] = Proxy_RxCanRxEscInfo.GearType;
    output[33] = Proxy_RxCanRxEscInfo.EngClutchState;
    output[34] = Proxy_RxCanRxEscInfo.EngTqCmdBeforeIntv;
    output[35] = Proxy_RxCanRxEscInfo.MotEstTq;
    output[36] = Proxy_RxCanRxEscInfo.MotTqCmdBeforeIntv;
    output[37] = Proxy_RxCanRxEscInfo.TqIntvTCU;
    output[38] = Proxy_RxCanRxEscInfo.TqIntvSlowTCU;
    output[39] = Proxy_RxCanRxEscInfo.TqIncReq;
    output[40] = Proxy_RxCanRxEscInfo.DecelReq;
    output[41] = Proxy_RxCanRxEscInfo.RainSnsStat;
    output[42] = Proxy_RxCanRxEscInfo.EngSpdErr;
    output[43] = Proxy_RxCanRxEscInfo.AtType;
    output[44] = Proxy_RxCanRxEscInfo.MtType;
    output[45] = Proxy_RxCanRxEscInfo.CvtType;
    output[46] = Proxy_RxCanRxEscInfo.DctType;
    output[47] = Proxy_RxCanRxEscInfo.HevAtTcu;
    output[48] = Proxy_RxCanRxEscInfo.TurbineRpmErr;
    output[49] = Proxy_RxCanRxEscInfo.ThrottleAngleErr;
    output[50] = Proxy_RxCanRxEscInfo.TopTrvlCltchSwtAct;
    output[51] = Proxy_RxCanRxEscInfo.TopTrvlCltchSwtActV;
    output[52] = Proxy_RxCanRxEscInfo.HillDesCtrlMdSwtAct;
    output[53] = Proxy_RxCanRxEscInfo.HillDesCtrlMdSwtActV;
    output[54] = Proxy_RxCanRxEscInfo.WiperIntSW;
    output[55] = Proxy_RxCanRxEscInfo.WiperLow;
    output[56] = Proxy_RxCanRxEscInfo.WiperHigh;
    output[57] = Proxy_RxCanRxEscInfo.WiperValid;
    output[58] = Proxy_RxCanRxEscInfo.WiperAuto;
    output[59] = Proxy_RxCanRxEscInfo.TcuFaultSts;
    output[60] = Proxy_RxCanRxClu2Info.IgnRun;
    output[61] = Proxy_RxCanRxEemInfo.YawRateInvld;
    output[62] = Proxy_RxCanRxEemInfo.AyInvld;
    output[63] = Proxy_RxCanRxEemInfo.SteeringAngleRxOk;
    output[64] = Proxy_RxCanRxEemInfo.AxInvldData;
    output[65] = Proxy_RxCanRxEemInfo.Ems1RxErr;
    output[66] = Proxy_RxCanRxEemInfo.Ems2RxErr;
    output[67] = Proxy_RxCanRxEemInfo.Tcu1RxErr;
    output[68] = Proxy_RxCanRxEemInfo.Tcu5RxErr;
    output[69] = Proxy_RxCanRxEemInfo.Hcu1RxErr;
    output[70] = Proxy_RxCanRxEemInfo.Hcu2RxErr;
    output[71] = Proxy_RxCanRxEemInfo.Hcu3RxErr;
    output[72] = Proxy_RxCanRxInfo.MainBusOffFlag;
    output[73] = Proxy_RxCanRxInfo.SenBusOffFlag;
    output[74] = Proxy_RxIMUCalcInfo.Reverse_Gear_flg;
    output[75] = Proxy_RxIMUCalcInfo.Reverse_Judge_Time;
    output[76] = Proxy_RxCanRxGearSelDispErrInfo;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Proxy_Rx_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
