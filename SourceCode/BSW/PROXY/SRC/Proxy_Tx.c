/**
 * @defgroup Proxy_Tx Proxy_Tx
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Proxy_Tx.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Proxy_Tx.h"
#include "Proxy_Tx_Ifa.h"
#include "IfxStm_reg.h"
#include "Proxy_Encode.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PROXY_TX_START_SEC_CONST_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PROXY_TX_STOP_SEC_CONST_UNSPECIFIED
#include "Proxy_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PROXY_TX_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Proxy_Tx_HdrBusType Proxy_TxBus;

/* Version Info */
const SwcVersionInfo_t Proxy_TxVersionInfo = 
{   
    PROXY_TX_MODULE_ID,           /* Proxy_TxVersionInfo.ModuleId */
    PROXY_TX_MAJOR_VERSION,       /* Proxy_TxVersionInfo.MajorVer */
    PROXY_TX_MINOR_VERSION,       /* Proxy_TxVersionInfo.MinorVer */
    PROXY_TX_PATCH_VERSION,       /* Proxy_TxVersionInfo.PatchVer */
    PROXY_TX_BRANCH_VERSION       /* Proxy_TxVersionInfo.BranchVer */
};
    
/* Input Data Element */
Eem_MainEemCtrlInhibitData_t Proxy_TxEemCtrlInhibitData;
Abc_CtrlCanTxInfo_t Proxy_TxCanTxInfo;
Pedal_SenSyncPdt5msRawInfo_t Proxy_TxPdt5msRawInfo;
Pedal_SenSyncPdf5msRawInfo_t Proxy_TxPdf5msRawInfo;
Rbc_CtrlTarRgnBrkTqInfo_t Proxy_TxTarRgnBrkTqInfo;
Swt_SenEscSwtStInfo_t Proxy_TxEscSwtStInfo;
Wss_SenWhlSpdInfo_t Proxy_TxWhlSpdInfo;
Wss_SenWhlEdgeCntInfo_t Proxy_TxWhlEdgeCntInfo;
Press_SenSyncPistP5msRawInfo_t Proxy_TxPistP5msRawInfo;
Nvm_HndlrLogicEepDataInfo_t Proxy_TxLogicEepDataInfo;
Eem_MainEemLampData_t Proxy_TxEemLampData;
Mom_HndlrEcuModeSts_t Proxy_TxEcuModeSts;
Prly_HndlrIgnOnOffSts_t Proxy_TxIgnOnOffSts;
Diag_HndlrDiagSci_t Proxy_TxDiagSci;
Diag_HndlrDiagAhbSci_t Proxy_TxDiagAhbSci;
Eem_SuspcDetnFuncInhibitProxySts_t Proxy_TxFuncInhibitProxySts;

/* Output Data Element */
Proxy_TxTxESCSensorInfo_t Proxy_TxTxESCSensorInfo;
Proxy_TxTxYawCbitInfo_t Proxy_TxTxYawCbitInfo;
Proxy_TxTxWhlSpdInfo_t Proxy_TxTxWhlSpdInfo;
Proxy_TxTxWhlPulInfo_t Proxy_TxTxWhlPulInfo;
Proxy_TxTxTcs5Info_t Proxy_TxTxTcs5Info;
Proxy_TxTxTcs1Info_t Proxy_TxTxTcs1Info;
Proxy_TxTxSasCalInfo_t Proxy_TxTxSasCalInfo;
Proxy_TxTxEsp2Info_t Proxy_TxTxEsp2Info;
Proxy_TxTxEbs1Info_t Proxy_TxTxEbs1Info;
Proxy_TxTxAhb1Info_t Proxy_TxTxAhb1Info;
Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo_t Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo;
Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo_t Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo;
Proxy_TxAVL_BRTORQ_WHLInfo_t Proxy_TxAVL_BRTORQ_WHLInfo;
Proxy_TxAVL_RPM_WHLInfo_t Proxy_TxAVL_RPM_WHLInfo;
Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo_t Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo;
Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info_t Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info;
Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo_t Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo;
Proxy_TxAVL_QUAN_EES_WHLInfo_t Proxy_TxAVL_QUAN_EES_WHLInfo;
Proxy_TxTAR_LTRQD_BAXInfo_t Proxy_TxTAR_LTRQD_BAXInfo;
Proxy_TxST_YMRInfo_t Proxy_TxST_YMRInfo;
Proxy_TxWEGSTRECKEInfo_t Proxy_TxWEGSTRECKEInfo;
Proxy_TxDISP_CC_DRDY_00Info_t Proxy_TxDISP_CC_DRDY_00Info;
Proxy_TxDISP_CC_BYPA_00Info_t Proxy_TxDISP_CC_BYPA_00Info;
Proxy_TxST_VHSSInfo_t Proxy_TxST_VHSSInfo;
Proxy_TxDIAG_OBD_HYB_1Info_t Proxy_TxDIAG_OBD_HYB_1Info;
Proxy_TxSU_DSCInfo_t Proxy_TxSU_DSCInfo;
Proxy_TxST_TYR_RDCInfo_t Proxy_TxST_TYR_RDCInfo;
Proxy_TxST_TYR_RPAInfo_t Proxy_TxST_TYR_RPAInfo;
Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info_t Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info;
Proxy_TxST_TYR_2Info_t Proxy_TxST_TYR_2Info;
Proxy_TxAVL_T_TYRInfo_t Proxy_TxAVL_T_TYRInfo;
Proxy_TxTEMP_BRKInfo_t Proxy_TxTEMP_BRKInfo;
Proxy_TxAVL_P_TYRInfo_t Proxy_TxAVL_P_TYRInfo;
Proxy_TxFR_DBG_DSCInfo_t Proxy_TxFR_DBG_DSCInfo;

uint32 Proxy_Tx_Timer_Start;
uint32 Proxy_Tx_Timer_Elapsed;

#define PROXY_TX_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Proxy_MemMap.h"
#define PROXY_TX_START_SEC_VAR_NOINIT_32BIT
#include "Proxy_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PROXY_TX_STOP_SEC_VAR_NOINIT_32BIT
#include "Proxy_MemMap.h"
#define PROXY_TX_START_SEC_VAR_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PROXY_TX_STOP_SEC_VAR_UNSPECIFIED
#include "Proxy_MemMap.h"
#define PROXY_TX_START_SEC_VAR_32BIT
#include "Proxy_MemMap.h"
/** Variable Section (32BIT)**/


#define PROXY_TX_STOP_SEC_VAR_32BIT
#include "Proxy_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PROXY_TX_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PROXY_TX_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Proxy_MemMap.h"
#define PROXY_TX_START_SEC_VAR_NOINIT_32BIT
#include "Proxy_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PROXY_TX_STOP_SEC_VAR_NOINIT_32BIT
#include "Proxy_MemMap.h"
#define PROXY_TX_START_SEC_VAR_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PROXY_TX_STOP_SEC_VAR_UNSPECIFIED
#include "Proxy_MemMap.h"
#define PROXY_TX_START_SEC_VAR_32BIT
#include "Proxy_MemMap.h"
/** Variable Section (32BIT)**/


#define PROXY_TX_STOP_SEC_VAR_32BIT
#include "Proxy_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PROXY_TX_START_SEC_CODE
#include "Proxy_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Proxy_Tx_Init(void)
{
    /* Initialize internal bus */
    Proxy_TxBus.Proxy_TxEemCtrlInhibitData.Eem_BBS_DegradeModeFail = 0;
    Proxy_TxBus.Proxy_TxEemCtrlInhibitData.Eem_BBS_DefectiveModeFail = 0;
    Proxy_TxBus.Proxy_TxCanTxInfo.TqIntvTCS = 0;
    Proxy_TxBus.Proxy_TxCanTxInfo.TqIntvMsr = 0;
    Proxy_TxBus.Proxy_TxCanTxInfo.TqIntvSlowTCS = 0;
    Proxy_TxBus.Proxy_TxCanTxInfo.MinGear = 0;
    Proxy_TxBus.Proxy_TxCanTxInfo.MaxGear = 0;
    Proxy_TxBus.Proxy_TxCanTxInfo.TcsReq = 0;
    Proxy_TxBus.Proxy_TxCanTxInfo.TcsCtrl = 0;
    Proxy_TxBus.Proxy_TxCanTxInfo.AbsAct = 0;
    Proxy_TxBus.Proxy_TxCanTxInfo.TcsGearShiftChr = 0;
    Proxy_TxBus.Proxy_TxCanTxInfo.EspCtrl = 0;
    Proxy_TxBus.Proxy_TxCanTxInfo.MsrReq = 0;
    Proxy_TxBus.Proxy_TxCanTxInfo.TcsProductInfo = 0;
    Proxy_TxBus.Proxy_TxPdt5msRawInfo.PdtSig = 0;
    Proxy_TxBus.Proxy_TxPdf5msRawInfo.PdfSig = 0;
    Proxy_TxBus.Proxy_TxTarRgnBrkTqInfo.TarRgnBrkTq = 0;
    Proxy_TxBus.Proxy_TxTarRgnBrkTqInfo.EstTotBrkForce = 0;
    Proxy_TxBus.Proxy_TxTarRgnBrkTqInfo.EstHydBrkForce = 0;
    Proxy_TxBus.Proxy_TxTarRgnBrkTqInfo.EhbStat = 0;
    Proxy_TxBus.Proxy_TxEscSwtStInfo.EscDisabledBySwt = 0;
    Proxy_TxBus.Proxy_TxEscSwtStInfo.TcsDisabledBySwt = 0;
    Proxy_TxBus.Proxy_TxWhlSpdInfo.FlWhlSpd = 0;
    Proxy_TxBus.Proxy_TxWhlSpdInfo.FrWhlSpd = 0;
    Proxy_TxBus.Proxy_TxWhlSpdInfo.RlWhlSpd = 0;
    Proxy_TxBus.Proxy_TxWhlSpdInfo.RrlWhlSpd = 0;
    Proxy_TxBus.Proxy_TxWhlEdgeCntInfo.FlWhlEdgeCnt = 0;
    Proxy_TxBus.Proxy_TxWhlEdgeCntInfo.FrWhlEdgeCnt = 0;
    Proxy_TxBus.Proxy_TxWhlEdgeCntInfo.RlWhlEdgeCnt = 0;
    Proxy_TxBus.Proxy_TxWhlEdgeCntInfo.RrWhlEdgeCnt = 0;
    Proxy_TxBus.Proxy_TxPistP5msRawInfo.PistPSig = 0;
    Proxy_TxBus.Proxy_TxLogicEepDataInfo.KPdtOffsEolReadVal = 0;
    Proxy_TxBus.Proxy_TxLogicEepDataInfo.ReadInvld = 0;
    Proxy_TxBus.Proxy_TxEemLampData.Eem_Lamp_EBDLampRequest = 0;
    Proxy_TxBus.Proxy_TxEemLampData.Eem_Lamp_ABSLampRequest = 0;
    Proxy_TxBus.Proxy_TxEemLampData.Eem_Lamp_TCSLampRequest = 0;
    Proxy_TxBus.Proxy_TxEemLampData.Eem_Lamp_TCSOffLampRequest = 0;
    Proxy_TxBus.Proxy_TxEemLampData.Eem_Lamp_VDCLampRequest = 0;
    Proxy_TxBus.Proxy_TxEemLampData.Eem_Lamp_VDCOffLampRequest = 0;
    Proxy_TxBus.Proxy_TxEemLampData.Eem_Lamp_HDCLampRequest = 0;
    Proxy_TxBus.Proxy_TxEemLampData.Eem_Lamp_BBSBuzzorRequest = 0;
    Proxy_TxBus.Proxy_TxEemLampData.Eem_Lamp_RBCSLampRequest = 0;
    Proxy_TxBus.Proxy_TxEemLampData.Eem_Lamp_AHBLampRequest = 0;
    Proxy_TxBus.Proxy_TxEemLampData.Eem_Lamp_ServiceLampRequest = 0;
    Proxy_TxBus.Proxy_TxEemLampData.Eem_Lamp_TCSFuncLampRequest = 0;
    Proxy_TxBus.Proxy_TxEemLampData.Eem_Lamp_VDCFuncLampRequest = 0;
    Proxy_TxBus.Proxy_TxEemLampData.Eem_Lamp_AVHLampRequest = 0;
    Proxy_TxBus.Proxy_TxEemLampData.Eem_Lamp_AVHILampRequest = 0;
    Proxy_TxBus.Proxy_TxEemLampData.Eem_Lamp_ACCEnable = 0;
    Proxy_TxBus.Proxy_TxEemLampData.Eem_Lamp_CDMEnable = 0;
    Proxy_TxBus.Proxy_TxEemLampData.Eem_Buzzor_On = 0;
    Proxy_TxBus.Proxy_TxEcuModeSts = 0;
    Proxy_TxBus.Proxy_TxIgnOnOffSts = 0;
    Proxy_TxBus.Proxy_TxDiagSci = 0;
    Proxy_TxBus.Proxy_TxDiagAhbSci = 0;
    Proxy_TxBus.Proxy_TxFuncInhibitProxySts = 0;
    Proxy_TxBus.Proxy_TxTxESCSensorInfo.EstimatedYaw = 0;
    Proxy_TxBus.Proxy_TxTxESCSensorInfo.EstimatedAY = 0;
    Proxy_TxBus.Proxy_TxTxESCSensorInfo.A_long_1_1000g = 0;
    Proxy_TxBus.Proxy_TxTxYawCbitInfo.TestYawRateSensor = 0;
    Proxy_TxBus.Proxy_TxTxYawCbitInfo.TestAcclSensor = 0;
    Proxy_TxBus.Proxy_TxTxYawCbitInfo.YawSerialNumberReq = 0;
    Proxy_TxBus.Proxy_TxTxWhlSpdInfo.WhlSpdFL = 0;
    Proxy_TxBus.Proxy_TxTxWhlSpdInfo.WhlSpdFR = 0;
    Proxy_TxBus.Proxy_TxTxWhlSpdInfo.WhlSpdRL = 0;
    Proxy_TxBus.Proxy_TxTxWhlSpdInfo.WhlSpdRR = 0;
    Proxy_TxBus.Proxy_TxTxWhlPulInfo.WhlPulFL = 0;
    Proxy_TxBus.Proxy_TxTxWhlPulInfo.WhlPulFR = 0;
    Proxy_TxBus.Proxy_TxTxWhlPulInfo.WhlPulRL = 0;
    Proxy_TxBus.Proxy_TxTxWhlPulInfo.WhlPulRR = 0;
    Proxy_TxBus.Proxy_TxTxTcs5Info.CfBrkAbsWLMP = 0;
    Proxy_TxBus.Proxy_TxTxTcs5Info.CfBrkEbdWLMP = 0;
    Proxy_TxBus.Proxy_TxTxTcs5Info.CfBrkTcsWLMP = 0;
    Proxy_TxBus.Proxy_TxTxTcs5Info.CfBrkTcsFLMP = 0;
    Proxy_TxBus.Proxy_TxTxTcs5Info.CfBrkAbsDIAG = 0;
    Proxy_TxBus.Proxy_TxTxTcs5Info.CrBrkWheelFL_KMH = 0;
    Proxy_TxBus.Proxy_TxTxTcs5Info.CrBrkWheelFR_KMH = 0;
    Proxy_TxBus.Proxy_TxTxTcs5Info.CrBrkWheelRL_KMH = 0;
    Proxy_TxBus.Proxy_TxTxTcs5Info.CrBrkWheelRR_KMH = 0;
    Proxy_TxBus.Proxy_TxTxTcs1Info.CfBrkTcsREQ = 0;
    Proxy_TxBus.Proxy_TxTxTcs1Info.CfBrkTcsPAS = 0;
    Proxy_TxBus.Proxy_TxTxTcs1Info.CfBrkTcsDEF = 0;
    Proxy_TxBus.Proxy_TxTxTcs1Info.CfBrkTcsCTL = 0;
    Proxy_TxBus.Proxy_TxTxTcs1Info.CfBrkAbsACT = 0;
    Proxy_TxBus.Proxy_TxTxTcs1Info.CfBrkAbsDEF = 0;
    Proxy_TxBus.Proxy_TxTxTcs1Info.CfBrkEbdDEF = 0;
    Proxy_TxBus.Proxy_TxTxTcs1Info.CfBrkTcsGSC = 0;
    Proxy_TxBus.Proxy_TxTxTcs1Info.CfBrkEspPAS = 0;
    Proxy_TxBus.Proxy_TxTxTcs1Info.CfBrkEspDEF = 0;
    Proxy_TxBus.Proxy_TxTxTcs1Info.CfBrkEspCTL = 0;
    Proxy_TxBus.Proxy_TxTxTcs1Info.CfBrkMsrREQ = 0;
    Proxy_TxBus.Proxy_TxTxTcs1Info.CfBrkTcsMFRN = 0;
    Proxy_TxBus.Proxy_TxTxTcs1Info.CfBrkMinGEAR = 0;
    Proxy_TxBus.Proxy_TxTxTcs1Info.CfBrkMaxGEAR = 0;
    Proxy_TxBus.Proxy_TxTxTcs1Info.BrakeLight = 0;
    Proxy_TxBus.Proxy_TxTxTcs1Info.HacCtl = 0;
    Proxy_TxBus.Proxy_TxTxTcs1Info.HacPas = 0;
    Proxy_TxBus.Proxy_TxTxTcs1Info.HacDef = 0;
    Proxy_TxBus.Proxy_TxTxTcs1Info.CrBrkTQI_PC = 0;
    Proxy_TxBus.Proxy_TxTxTcs1Info.CrBrkTQIMSR_PC = 0;
    Proxy_TxBus.Proxy_TxTxTcs1Info.CrBrkTQISLW_PC = 0;
    Proxy_TxBus.Proxy_TxTxTcs1Info.CrEbsMSGCHKSUM = 0;
    Proxy_TxBus.Proxy_TxTxSasCalInfo.CalSas_CCW = 0;
    Proxy_TxBus.Proxy_TxTxSasCalInfo.CalSas_Lws_CID = 0;
    Proxy_TxBus.Proxy_TxTxEsp2Info.LatAccel = 0;
    Proxy_TxBus.Proxy_TxTxEsp2Info.LatAccelStat = 0;
    Proxy_TxBus.Proxy_TxTxEsp2Info.LatAccelDiag = 0;
    Proxy_TxBus.Proxy_TxTxEsp2Info.LongAccel = 0;
    Proxy_TxBus.Proxy_TxTxEsp2Info.LongAccelStat = 0;
    Proxy_TxBus.Proxy_TxTxEsp2Info.LongAccelDiag = 0;
    Proxy_TxBus.Proxy_TxTxEsp2Info.YawRate = 0;
    Proxy_TxBus.Proxy_TxTxEsp2Info.YawRateStat = 0;
    Proxy_TxBus.Proxy_TxTxEsp2Info.YawRateDiag = 0;
    Proxy_TxBus.Proxy_TxTxEsp2Info.CylPres = 0;
    Proxy_TxBus.Proxy_TxTxEsp2Info.CylPresStat = 0;
    Proxy_TxBus.Proxy_TxTxEsp2Info.CylPresDiag = 0;
    Proxy_TxBus.Proxy_TxTxEbs1Info.CfBrkEbsStat = 0;
    Proxy_TxBus.Proxy_TxTxEbs1Info.CrBrkRegenTQLimit_NM = 0;
    Proxy_TxBus.Proxy_TxTxEbs1Info.CrBrkEstTot_NM = 0;
    Proxy_TxBus.Proxy_TxTxEbs1Info.CfBrkSnsFail = 0;
    Proxy_TxBus.Proxy_TxTxEbs1Info.CfBrkRbsWLamp = 0;
    Proxy_TxBus.Proxy_TxTxEbs1Info.CfBrkVacuumSysDef = 0;
    Proxy_TxBus.Proxy_TxTxEbs1Info.CrBrkEstHyd_NM = 0;
    Proxy_TxBus.Proxy_TxTxEbs1Info.CrBrkStkDep_PC = 0;
    Proxy_TxBus.Proxy_TxTxEbs1Info.CfBrkPgmRun1 = 0;
    Proxy_TxBus.Proxy_TxTxAhb1Info.CfAhbWLMP = 0;
    Proxy_TxBus.Proxy_TxTxAhb1Info.CfAhbDiag = 0;
    Proxy_TxBus.Proxy_TxTxAhb1Info.CfAhbAct = 0;
    Proxy_TxBus.Proxy_TxTxAhb1Info.CfAhbDef = 0;
    Proxy_TxBus.Proxy_TxTxAhb1Info.CfAhbSLMP = 0;
    Proxy_TxBus.Proxy_TxTxAhb1Info.CfBrkAhbSTDep_PC = 0;
    Proxy_TxBus.Proxy_TxTxAhb1Info.CfBrkBuzzR = 0;
    Proxy_TxBus.Proxy_TxTxAhb1Info.CfBrkPedalCalStatus = 0;
    Proxy_TxBus.Proxy_TxTxAhb1Info.CfBrkAhbSnsFail = 0;
    Proxy_TxBus.Proxy_TxTxAhb1Info.WhlSpdFLAhb = 0;
    Proxy_TxBus.Proxy_TxTxAhb1Info.WhlSpdFRAhb = 0;
    Proxy_TxBus.Proxy_TxTxAhb1Info.CrAhbMsgChkSum = 0;
    Proxy_TxBus.Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.QU_TAR_REPAT_XTRQ_FTAX_BAX_ACT = 0;
    Proxy_TxBus.Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.TAR_REPAT_XTRQ_FTAX_BAX_ACT = 0;
    Proxy_TxBus.Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.TAR_WMOM_PT_SUM_STAB_FAST = 0;
    Proxy_TxBus.Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.ST_ECU_RQ_WMOM_PT_SUM_STAB = 0;
    Proxy_TxBus.Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.QU_RQ_ILK_PT = 0;
    Proxy_TxBus.Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.QU_TAR_WMOM_PT_SUM_STAB = 0;
    Proxy_TxBus.Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.TAR_WMOM_PT_SUM_STAB = 0;
    Proxy_TxBus.Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.ST_ECU_AVL_BRTORQ_SUM = 0;
    Proxy_TxBus.Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_SER_PRMSN_DBC = 0;
    Proxy_TxBus.Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_SER_PRF_BRK = 0;
    Proxy_TxBus.Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.AVL_BRTORQ_SUM_DVCH = 0;
    Proxy_TxBus.Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.AVL_BRTORQ_SUM = 0;
    Proxy_TxBus.Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_AVL_BRTORQ_SUM = 0;
    Proxy_TxBus.Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_AVL_BRTORQ_SUM_DVCH = 0;
    Proxy_TxBus.Proxy_TxAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_FLH = 0;
    Proxy_TxBus.Proxy_TxAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_RRH = 0;
    Proxy_TxBus.Proxy_TxAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_FRH = 0;
    Proxy_TxBus.Proxy_TxAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_RRH = 0;
    Proxy_TxBus.Proxy_TxAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_RLH = 0;
    Proxy_TxBus.Proxy_TxAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_FLH = 0;
    Proxy_TxBus.Proxy_TxAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_RLH = 0;
    Proxy_TxBus.Proxy_TxAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_FRH = 0;
    Proxy_TxBus.Proxy_TxAVL_RPM_WHLInfo.AVL_RPM_WHL_RLH = 0;
    Proxy_TxBus.Proxy_TxAVL_RPM_WHLInfo.AVL_RPM_WHL_RRH = 0;
    Proxy_TxBus.Proxy_TxAVL_RPM_WHLInfo.AVL_RPM_WHL_FLH = 0;
    Proxy_TxBus.Proxy_TxAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_FLH = 0;
    Proxy_TxBus.Proxy_TxAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_FRH = 0;
    Proxy_TxBus.Proxy_TxAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_RLH = 0;
    Proxy_TxBus.Proxy_TxAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_RRH = 0;
    Proxy_TxBus.Proxy_TxAVL_RPM_WHLInfo.AVL_RPM_WHL_FRH = 0;
    Proxy_TxBus.Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo.QU_TAR_WMOM_PT_SUM_RECUP = 0;
    Proxy_TxBus.Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo.TAR_WMOM_PT_SUM_RECUP = 0;
    Proxy_TxBus.Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo.ST_ECU_RQ_WMOM_PT_SUM_RECUP = 0;
    Proxy_TxBus.Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info.ST_ECU_ST_STAB_DSC = 0;
    Proxy_TxBus.Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info.ST_BRG_MSA = 0;
    Proxy_TxBus.Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info.ST_BRG_DV = 0;
    Proxy_TxBus.Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info.ST_UNRU_DSC = 0;
    Proxy_TxBus.Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info.QU_FN_FDR = 0;
    Proxy_TxBus.Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info.QU_FN_ABS = 0;
    Proxy_TxBus.Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info.QU_FN_ASC = 0;
    Proxy_TxBus.Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info.PRD_TIM_VEHSS = 0;
    Proxy_TxBus.Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.QU_SER_ECBA = 0;
    Proxy_TxBus.Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.TAR_BRTORQ_SUM_COOTD = 0;
    Proxy_TxBus.Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.AVL_WAY_BRKFA = 0;
    Proxy_TxBus.Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.AVL_FORC_BRKFA = 0;
    Proxy_TxBus.Proxy_TxAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_RLH = 0;
    Proxy_TxBus.Proxy_TxAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_RRH = 0;
    Proxy_TxBus.Proxy_TxAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_FLH = 0;
    Proxy_TxBus.Proxy_TxAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_FRH = 0;
    Proxy_TxBus.Proxy_TxAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_FLH = 0;
    Proxy_TxBus.Proxy_TxAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_RRH = 0;
    Proxy_TxBus.Proxy_TxAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_FRH = 0;
    Proxy_TxBus.Proxy_TxAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_RLH = 0;
    Proxy_TxBus.Proxy_TxTAR_LTRQD_BAXInfo.TAR_LTRQD_BAX = 0;
    Proxy_TxBus.Proxy_TxTAR_LTRQD_BAXInfo.LIM_MAX_LTRQD_BAX = 0;
    Proxy_TxBus.Proxy_TxST_YMRInfo.QU_SER_CLCTR_YMR = 0;
    Proxy_TxBus.Proxy_TxWEGSTRECKEInfo.MILE_FLH = 0;
    Proxy_TxBus.Proxy_TxWEGSTRECKEInfo.MILE_FRH = 0;
    Proxy_TxBus.Proxy_TxWEGSTRECKEInfo.MILE_INTM_TOMSRM = 0;
    Proxy_TxBus.Proxy_TxWEGSTRECKEInfo.MILE = 0;
    Proxy_TxBus.Proxy_TxDISP_CC_DRDY_00Info.ST_IDC_CC_DRDY_00 = 0;
    Proxy_TxBus.Proxy_TxDISP_CC_DRDY_00Info.ST_CC_DRDY_00 = 0;
    Proxy_TxBus.Proxy_TxDISP_CC_DRDY_00Info.TRANF_CC_DRDY_00 = 0;
    Proxy_TxBus.Proxy_TxDISP_CC_DRDY_00Info.NO_CC_DRDY_00 = 0;
    Proxy_TxBus.Proxy_TxDISP_CC_BYPA_00Info.TRANF_CC_BYPA_00 = 0;
    Proxy_TxBus.Proxy_TxDISP_CC_BYPA_00Info.ST_IDC_CC_BYPA_00 = 0;
    Proxy_TxBus.Proxy_TxDISP_CC_BYPA_00Info.ST_CC_BYPA_00 = 0;
    Proxy_TxBus.Proxy_TxDISP_CC_BYPA_00Info.NO_CC_BYPA_00 = 0;
    Proxy_TxBus.Proxy_TxST_VHSSInfo.ST_VEHSS = 0;
    Proxy_TxBus.Proxy_TxDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_1_ERR = 0;
    Proxy_TxBus.Proxy_TxDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_3_ERR = 0;
    Proxy_TxBus.Proxy_TxDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_4_ERR = 0;
    Proxy_TxBus.Proxy_TxDIAG_OBD_HYB_1Info.DIAG_OBD_HYB_1_MUX_MAX = 0;
    Proxy_TxBus.Proxy_TxDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_2_ERR = 0;
    Proxy_TxBus.Proxy_TxDIAG_OBD_HYB_1Info.DIAG_OBD_HYB_1_MUX_IMME = 0;
    Proxy_TxBus.Proxy_TxSU_DSCInfo.SU_DSC_WSS = 0;
    Proxy_TxBus.Proxy_TxST_TYR_RDCInfo.QU_TPL_RDC = 0;
    Proxy_TxBus.Proxy_TxST_TYR_RDCInfo.QU_FN_TYR_INFO_RDC = 0;
    Proxy_TxBus.Proxy_TxST_TYR_RDCInfo.QU_TFAI_RDC = 0;
    Proxy_TxBus.Proxy_TxST_TYR_RPAInfo.QU_TFAI_RPA = 0;
    Proxy_TxBus.Proxy_TxST_TYR_RPAInfo.QU_FN_TYR_INFO_RPA = 0;
    Proxy_TxBus.Proxy_TxST_TYR_RPAInfo.QU_TPL_RPA = 0;
    Proxy_TxBus.Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_2 = 0;
    Proxy_TxBus.Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_1 = 0;
    Proxy_TxBus.Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_3 = 0;
    Proxy_TxBus.Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_4 = 0;
    Proxy_TxBus.Proxy_TxST_TYR_2Info.QU_RDC_INIT_DISP = 0;
    Proxy_TxBus.Proxy_TxAVL_T_TYRInfo.AVL_TEMP_TYR_RRH = 0;
    Proxy_TxBus.Proxy_TxAVL_T_TYRInfo.AVL_TEMP_TYR_FLH = 0;
    Proxy_TxBus.Proxy_TxAVL_T_TYRInfo.AVL_TEMP_TYR_RLH = 0;
    Proxy_TxBus.Proxy_TxAVL_T_TYRInfo.AVL_TEMP_TYR_FRH = 0;
    Proxy_TxBus.Proxy_TxTEMP_BRKInfo.TEMP_BRDSK_RLH_VRFD = 0;
    Proxy_TxBus.Proxy_TxTEMP_BRKInfo.QU_TEMP_BRDSK_RLH_VRFD = 0;
    Proxy_TxBus.Proxy_TxTEMP_BRKInfo.TEMP_BRDSK_RRH_VRFD = 0;
    Proxy_TxBus.Proxy_TxTEMP_BRKInfo.QU_TEMP_BRDSK_RRH_VRFD = 0;
    Proxy_TxBus.Proxy_TxAVL_P_TYRInfo.AVL_P_TYR_FRH = 0;
    Proxy_TxBus.Proxy_TxAVL_P_TYRInfo.AVL_P_TYR_FLH = 0;
    Proxy_TxBus.Proxy_TxAVL_P_TYRInfo.AVL_P_TYR_RRH = 0;
    Proxy_TxBus.Proxy_TxAVL_P_TYRInfo.AVL_P_TYR_RLH = 0;
    Proxy_TxBus.Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_13 = 0;
    Proxy_TxBus.Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_04 = 0;
    Proxy_TxBus.Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_05 = 0;
    Proxy_TxBus.Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_06 = 0;
    Proxy_TxBus.Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_01 = 0;
    Proxy_TxBus.Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_02 = 0;
    Proxy_TxBus.Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_03 = 0;
    Proxy_TxBus.Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_07 = 0;
    Proxy_TxBus.Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_11 = 0;
    Proxy_TxBus.Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_10 = 0;
    Proxy_TxBus.Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_09 = 0;
    Proxy_TxBus.Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_15 = 0;
    Proxy_TxBus.Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_14 = 0;
    Proxy_TxBus.Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_12 = 0;
    Proxy_TxBus.Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_00 = 0;
    Proxy_TxBus.Proxy_TxFR_DBG_DSCInfo.BDTM_TA_FR = 0;
    Proxy_TxBus.Proxy_TxFR_DBG_DSCInfo.BDTM_TA_RL = 0;
    Proxy_TxBus.Proxy_TxFR_DBG_DSCInfo.BDTM_TA_RR = 0;
    Proxy_TxBus.Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_08 = 0;
    Proxy_TxBus.Proxy_TxFR_DBG_DSCInfo.ST_STATE = 0;
    Proxy_TxBus.Proxy_TxFR_DBG_DSCInfo.BDTM_TA_FL = 0;
    Proxy_TxBus.Proxy_TxFR_DBG_DSCInfo.ABSRef = 0;
    Proxy_TxBus.Proxy_TxFR_DBG_DSCInfo.LOW_VOLTAGE = 0;
    Proxy_TxBus.Proxy_TxFR_DBG_DSCInfo.DEBUG_PCIB = 0;
    Proxy_TxBus.Proxy_TxFR_DBG_DSCInfo.DBG_DSC = 0;
    Proxy_TxBus.Proxy_TxFR_DBG_DSCInfo.LATACC = 0;
    Proxy_TxBus.Proxy_TxFR_DBG_DSCInfo.TRIGGER_FS = 0;
    Proxy_TxBus.Proxy_TxFR_DBG_DSCInfo.TRIGGER_IS = 0;
}

void Proxy_Tx(void)
{
    Proxy_Tx_Timer_Start = STM0_TIM0.U;

    /* Input */
    /* Decomposed structure interface */
    Proxy_Tx_Read_Proxy_TxEemCtrlInhibitData_Eem_BBS_DegradeModeFail(&Proxy_TxBus.Proxy_TxEemCtrlInhibitData.Eem_BBS_DegradeModeFail);
    Proxy_Tx_Read_Proxy_TxEemCtrlInhibitData_Eem_BBS_DefectiveModeFail(&Proxy_TxBus.Proxy_TxEemCtrlInhibitData.Eem_BBS_DefectiveModeFail);

    Proxy_Tx_Read_Proxy_TxCanTxInfo(&Proxy_TxBus.Proxy_TxCanTxInfo);
    /*==============================================================================
    * Members of structure Proxy_TxCanTxInfo 
     : Proxy_TxCanTxInfo.TqIntvTCS;
     : Proxy_TxCanTxInfo.TqIntvMsr;
     : Proxy_TxCanTxInfo.TqIntvSlowTCS;
     : Proxy_TxCanTxInfo.MinGear;
     : Proxy_TxCanTxInfo.MaxGear;
     : Proxy_TxCanTxInfo.TcsReq;
     : Proxy_TxCanTxInfo.TcsCtrl;
     : Proxy_TxCanTxInfo.AbsAct;
     : Proxy_TxCanTxInfo.TcsGearShiftChr;
     : Proxy_TxCanTxInfo.EspCtrl;
     : Proxy_TxCanTxInfo.MsrReq;
     : Proxy_TxCanTxInfo.TcsProductInfo;
     =============================================================================*/
    
    Proxy_Tx_Read_Proxy_TxPdt5msRawInfo(&Proxy_TxBus.Proxy_TxPdt5msRawInfo);
    /*==============================================================================
    * Members of structure Proxy_TxPdt5msRawInfo 
     : Proxy_TxPdt5msRawInfo.PdtSig;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Proxy_Tx_Read_Proxy_TxPdf5msRawInfo_PdfSig(&Proxy_TxBus.Proxy_TxPdf5msRawInfo.PdfSig);

    /* Decomposed structure interface */
    Proxy_Tx_Read_Proxy_TxTarRgnBrkTqInfo_TarRgnBrkTq(&Proxy_TxBus.Proxy_TxTarRgnBrkTqInfo.TarRgnBrkTq);
    Proxy_Tx_Read_Proxy_TxTarRgnBrkTqInfo_EstTotBrkForce(&Proxy_TxBus.Proxy_TxTarRgnBrkTqInfo.EstTotBrkForce);
    Proxy_Tx_Read_Proxy_TxTarRgnBrkTqInfo_EstHydBrkForce(&Proxy_TxBus.Proxy_TxTarRgnBrkTqInfo.EstHydBrkForce);
    Proxy_Tx_Read_Proxy_TxTarRgnBrkTqInfo_EhbStat(&Proxy_TxBus.Proxy_TxTarRgnBrkTqInfo.EhbStat);

    /* Decomposed structure interface */
    Proxy_Tx_Read_Proxy_TxEscSwtStInfo_EscDisabledBySwt(&Proxy_TxBus.Proxy_TxEscSwtStInfo.EscDisabledBySwt);
    Proxy_Tx_Read_Proxy_TxEscSwtStInfo_TcsDisabledBySwt(&Proxy_TxBus.Proxy_TxEscSwtStInfo.TcsDisabledBySwt);

    Proxy_Tx_Read_Proxy_TxWhlSpdInfo(&Proxy_TxBus.Proxy_TxWhlSpdInfo);
    /*==============================================================================
    * Members of structure Proxy_TxWhlSpdInfo 
     : Proxy_TxWhlSpdInfo.FlWhlSpd;
     : Proxy_TxWhlSpdInfo.FrWhlSpd;
     : Proxy_TxWhlSpdInfo.RlWhlSpd;
     : Proxy_TxWhlSpdInfo.RrlWhlSpd;
     =============================================================================*/
    
    Proxy_Tx_Read_Proxy_TxWhlEdgeCntInfo(&Proxy_TxBus.Proxy_TxWhlEdgeCntInfo);
    /*==============================================================================
    * Members of structure Proxy_TxWhlEdgeCntInfo 
     : Proxy_TxWhlEdgeCntInfo.FlWhlEdgeCnt;
     : Proxy_TxWhlEdgeCntInfo.FrWhlEdgeCnt;
     : Proxy_TxWhlEdgeCntInfo.RlWhlEdgeCnt;
     : Proxy_TxWhlEdgeCntInfo.RrWhlEdgeCnt;
     =============================================================================*/
    
    Proxy_Tx_Read_Proxy_TxPistP5msRawInfo(&Proxy_TxBus.Proxy_TxPistP5msRawInfo);
    /*==============================================================================
    * Members of structure Proxy_TxPistP5msRawInfo 
     : Proxy_TxPistP5msRawInfo.PistPSig;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Proxy_Tx_Read_Proxy_TxLogicEepDataInfo_KPdtOffsEolReadVal(&Proxy_TxBus.Proxy_TxLogicEepDataInfo.KPdtOffsEolReadVal);
    Proxy_Tx_Read_Proxy_TxLogicEepDataInfo_ReadInvld(&Proxy_TxBus.Proxy_TxLogicEepDataInfo.ReadInvld);

    Proxy_Tx_Read_Proxy_TxEemLampData(&Proxy_TxBus.Proxy_TxEemLampData);
    /*==============================================================================
    * Members of structure Proxy_TxEemLampData 
     : Proxy_TxEemLampData.Eem_Lamp_EBDLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_ABSLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_TCSLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_TCSOffLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_VDCLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_VDCOffLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_HDCLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_BBSBuzzorRequest;
     : Proxy_TxEemLampData.Eem_Lamp_RBCSLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_AHBLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_ServiceLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_TCSFuncLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_VDCFuncLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_AVHLampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_AVHILampRequest;
     : Proxy_TxEemLampData.Eem_Lamp_ACCEnable;
     : Proxy_TxEemLampData.Eem_Lamp_CDMEnable;
     : Proxy_TxEemLampData.Eem_Buzzor_On;
     =============================================================================*/
    
    Proxy_Tx_Read_Proxy_TxEcuModeSts(&Proxy_TxBus.Proxy_TxEcuModeSts);
    Proxy_Tx_Read_Proxy_TxIgnOnOffSts(&Proxy_TxBus.Proxy_TxIgnOnOffSts);
    Proxy_Tx_Read_Proxy_TxDiagSci(&Proxy_TxBus.Proxy_TxDiagSci);
    Proxy_Tx_Read_Proxy_TxDiagAhbSci(&Proxy_TxBus.Proxy_TxDiagAhbSci);
    Proxy_Tx_Read_Proxy_TxFuncInhibitProxySts(&Proxy_TxBus.Proxy_TxFuncInhibitProxySts);

    /* Process */
    Proxy_TxProc();
    Proxy_vChkCan();
    
    /* Output */
    Proxy_Tx_Write_Proxy_TxTxESCSensorInfo(&Proxy_TxBus.Proxy_TxTxESCSensorInfo);
    /*==============================================================================
    * Members of structure Proxy_TxTxESCSensorInfo 
     : Proxy_TxTxESCSensorInfo.EstimatedYaw;
     : Proxy_TxTxESCSensorInfo.EstimatedAY;
     : Proxy_TxTxESCSensorInfo.A_long_1_1000g;
     =============================================================================*/
    
    Proxy_Tx_Write_Proxy_TxTxYawCbitInfo(&Proxy_TxBus.Proxy_TxTxYawCbitInfo);
    /*==============================================================================
    * Members of structure Proxy_TxTxYawCbitInfo 
     : Proxy_TxTxYawCbitInfo.TestYawRateSensor;
     : Proxy_TxTxYawCbitInfo.TestAcclSensor;
     : Proxy_TxTxYawCbitInfo.YawSerialNumberReq;
     =============================================================================*/
    
    Proxy_Tx_Write_Proxy_TxTxWhlSpdInfo(&Proxy_TxBus.Proxy_TxTxWhlSpdInfo);
    /*==============================================================================
    * Members of structure Proxy_TxTxWhlSpdInfo 
     : Proxy_TxTxWhlSpdInfo.WhlSpdFL;
     : Proxy_TxTxWhlSpdInfo.WhlSpdFR;
     : Proxy_TxTxWhlSpdInfo.WhlSpdRL;
     : Proxy_TxTxWhlSpdInfo.WhlSpdRR;
     =============================================================================*/
    
    Proxy_Tx_Write_Proxy_TxTxWhlPulInfo(&Proxy_TxBus.Proxy_TxTxWhlPulInfo);
    /*==============================================================================
    * Members of structure Proxy_TxTxWhlPulInfo 
     : Proxy_TxTxWhlPulInfo.WhlPulFL;
     : Proxy_TxTxWhlPulInfo.WhlPulFR;
     : Proxy_TxTxWhlPulInfo.WhlPulRL;
     : Proxy_TxTxWhlPulInfo.WhlPulRR;
     =============================================================================*/
    
    Proxy_Tx_Write_Proxy_TxTxTcs5Info(&Proxy_TxBus.Proxy_TxTxTcs5Info);
    /*==============================================================================
    * Members of structure Proxy_TxTxTcs5Info 
     : Proxy_TxTxTcs5Info.CfBrkAbsWLMP;
     : Proxy_TxTxTcs5Info.CfBrkEbdWLMP;
     : Proxy_TxTxTcs5Info.CfBrkTcsWLMP;
     : Proxy_TxTxTcs5Info.CfBrkTcsFLMP;
     : Proxy_TxTxTcs5Info.CfBrkAbsDIAG;
     : Proxy_TxTxTcs5Info.CrBrkWheelFL_KMH;
     : Proxy_TxTxTcs5Info.CrBrkWheelFR_KMH;
     : Proxy_TxTxTcs5Info.CrBrkWheelRL_KMH;
     : Proxy_TxTxTcs5Info.CrBrkWheelRR_KMH;
     =============================================================================*/
    
    Proxy_Tx_Write_Proxy_TxTxTcs1Info(&Proxy_TxBus.Proxy_TxTxTcs1Info);
    /*==============================================================================
    * Members of structure Proxy_TxTxTcs1Info 
     : Proxy_TxTxTcs1Info.CfBrkTcsREQ;
     : Proxy_TxTxTcs1Info.CfBrkTcsPAS;
     : Proxy_TxTxTcs1Info.CfBrkTcsDEF;
     : Proxy_TxTxTcs1Info.CfBrkTcsCTL;
     : Proxy_TxTxTcs1Info.CfBrkAbsACT;
     : Proxy_TxTxTcs1Info.CfBrkAbsDEF;
     : Proxy_TxTxTcs1Info.CfBrkEbdDEF;
     : Proxy_TxTxTcs1Info.CfBrkTcsGSC;
     : Proxy_TxTxTcs1Info.CfBrkEspPAS;
     : Proxy_TxTxTcs1Info.CfBrkEspDEF;
     : Proxy_TxTxTcs1Info.CfBrkEspCTL;
     : Proxy_TxTxTcs1Info.CfBrkMsrREQ;
     : Proxy_TxTxTcs1Info.CfBrkTcsMFRN;
     : Proxy_TxTxTcs1Info.CfBrkMinGEAR;
     : Proxy_TxTxTcs1Info.CfBrkMaxGEAR;
     : Proxy_TxTxTcs1Info.BrakeLight;
     : Proxy_TxTxTcs1Info.HacCtl;
     : Proxy_TxTxTcs1Info.HacPas;
     : Proxy_TxTxTcs1Info.HacDef;
     : Proxy_TxTxTcs1Info.CrBrkTQI_PC;
     : Proxy_TxTxTcs1Info.CrBrkTQIMSR_PC;
     : Proxy_TxTxTcs1Info.CrBrkTQISLW_PC;
     : Proxy_TxTxTcs1Info.CrEbsMSGCHKSUM;
     =============================================================================*/
    
    Proxy_Tx_Write_Proxy_TxTxSasCalInfo(&Proxy_TxBus.Proxy_TxTxSasCalInfo);
    /*==============================================================================
    * Members of structure Proxy_TxTxSasCalInfo 
     : Proxy_TxTxSasCalInfo.CalSas_CCW;
     : Proxy_TxTxSasCalInfo.CalSas_Lws_CID;
     =============================================================================*/
    
    Proxy_Tx_Write_Proxy_TxTxEsp2Info(&Proxy_TxBus.Proxy_TxTxEsp2Info);
    /*==============================================================================
    * Members of structure Proxy_TxTxEsp2Info 
     : Proxy_TxTxEsp2Info.LatAccel;
     : Proxy_TxTxEsp2Info.LatAccelStat;
     : Proxy_TxTxEsp2Info.LatAccelDiag;
     : Proxy_TxTxEsp2Info.LongAccel;
     : Proxy_TxTxEsp2Info.LongAccelStat;
     : Proxy_TxTxEsp2Info.LongAccelDiag;
     : Proxy_TxTxEsp2Info.YawRate;
     : Proxy_TxTxEsp2Info.YawRateStat;
     : Proxy_TxTxEsp2Info.YawRateDiag;
     : Proxy_TxTxEsp2Info.CylPres;
     : Proxy_TxTxEsp2Info.CylPresStat;
     : Proxy_TxTxEsp2Info.CylPresDiag;
     =============================================================================*/
    
    Proxy_Tx_Write_Proxy_TxTxEbs1Info(&Proxy_TxBus.Proxy_TxTxEbs1Info);
    /*==============================================================================
    * Members of structure Proxy_TxTxEbs1Info 
     : Proxy_TxTxEbs1Info.CfBrkEbsStat;
     : Proxy_TxTxEbs1Info.CrBrkRegenTQLimit_NM;
     : Proxy_TxTxEbs1Info.CrBrkEstTot_NM;
     : Proxy_TxTxEbs1Info.CfBrkSnsFail;
     : Proxy_TxTxEbs1Info.CfBrkRbsWLamp;
     : Proxy_TxTxEbs1Info.CfBrkVacuumSysDef;
     : Proxy_TxTxEbs1Info.CrBrkEstHyd_NM;
     : Proxy_TxTxEbs1Info.CrBrkStkDep_PC;
     : Proxy_TxTxEbs1Info.CfBrkPgmRun1;
     =============================================================================*/
    
    Proxy_Tx_Write_Proxy_TxTxAhb1Info(&Proxy_TxBus.Proxy_TxTxAhb1Info);
    /*==============================================================================
    * Members of structure Proxy_TxTxAhb1Info 
     : Proxy_TxTxAhb1Info.CfAhbWLMP;
     : Proxy_TxTxAhb1Info.CfAhbDiag;
     : Proxy_TxTxAhb1Info.CfAhbAct;
     : Proxy_TxTxAhb1Info.CfAhbDef;
     : Proxy_TxTxAhb1Info.CfAhbSLMP;
     : Proxy_TxTxAhb1Info.CfBrkAhbSTDep_PC;
     : Proxy_TxTxAhb1Info.CfBrkBuzzR;
     : Proxy_TxTxAhb1Info.CfBrkPedalCalStatus;
     : Proxy_TxTxAhb1Info.CfBrkAhbSnsFail;
     : Proxy_TxTxAhb1Info.WhlSpdFLAhb;
     : Proxy_TxTxAhb1Info.WhlSpdFRAhb;
     : Proxy_TxTxAhb1Info.CrAhbMsgChkSum;
     =============================================================================*/
    
    Proxy_Tx_Write_Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo(&Proxy_TxBus.Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo);
    /*==============================================================================
    * Members of structure Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo 
     : Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.QU_TAR_REPAT_XTRQ_FTAX_BAX_ACT;
     : Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.TAR_REPAT_XTRQ_FTAX_BAX_ACT;
     : Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.TAR_WMOM_PT_SUM_STAB_FAST;
     : Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.ST_ECU_RQ_WMOM_PT_SUM_STAB;
     : Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.QU_RQ_ILK_PT;
     : Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.QU_TAR_WMOM_PT_SUM_STAB;
     : Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.TAR_WMOM_PT_SUM_STAB;
     =============================================================================*/
    
    Proxy_Tx_Write_Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo(&Proxy_TxBus.Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo);
    /*==============================================================================
    * Members of structure Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo 
     : Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.ST_ECU_AVL_BRTORQ_SUM;
     : Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_SER_PRMSN_DBC;
     : Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_SER_PRF_BRK;
     : Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.AVL_BRTORQ_SUM_DVCH;
     : Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.AVL_BRTORQ_SUM;
     : Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_AVL_BRTORQ_SUM;
     : Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_AVL_BRTORQ_SUM_DVCH;
     =============================================================================*/
    
    Proxy_Tx_Write_Proxy_TxAVL_BRTORQ_WHLInfo(&Proxy_TxBus.Proxy_TxAVL_BRTORQ_WHLInfo);
    /*==============================================================================
    * Members of structure Proxy_TxAVL_BRTORQ_WHLInfo 
     : Proxy_TxAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_FLH;
     : Proxy_TxAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_RRH;
     : Proxy_TxAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_FRH;
     : Proxy_TxAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_RRH;
     : Proxy_TxAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_RLH;
     : Proxy_TxAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_FLH;
     : Proxy_TxAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_RLH;
     : Proxy_TxAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_FRH;
     =============================================================================*/
    
    Proxy_Tx_Write_Proxy_TxAVL_RPM_WHLInfo(&Proxy_TxBus.Proxy_TxAVL_RPM_WHLInfo);
    /*==============================================================================
    * Members of structure Proxy_TxAVL_RPM_WHLInfo 
     : Proxy_TxAVL_RPM_WHLInfo.AVL_RPM_WHL_RLH;
     : Proxy_TxAVL_RPM_WHLInfo.AVL_RPM_WHL_RRH;
     : Proxy_TxAVL_RPM_WHLInfo.AVL_RPM_WHL_FLH;
     : Proxy_TxAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_FLH;
     : Proxy_TxAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_FRH;
     : Proxy_TxAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_RLH;
     : Proxy_TxAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_RRH;
     : Proxy_TxAVL_RPM_WHLInfo.AVL_RPM_WHL_FRH;
     =============================================================================*/
    
    Proxy_Tx_Write_Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo(&Proxy_TxBus.Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo);
    /*==============================================================================
    * Members of structure Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo 
     : Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo.QU_TAR_WMOM_PT_SUM_RECUP;
     : Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo.TAR_WMOM_PT_SUM_RECUP;
     : Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo.ST_ECU_RQ_WMOM_PT_SUM_RECUP;
     =============================================================================*/
    
    Proxy_Tx_Write_Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info(&Proxy_TxBus.Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info);
    /*==============================================================================
    * Members of structure Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info 
     : Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info.ST_ECU_ST_STAB_DSC;
     : Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info.ST_BRG_MSA;
     : Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info.ST_BRG_DV;
     : Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info.ST_UNRU_DSC;
     : Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info.QU_FN_FDR;
     : Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info.QU_FN_ABS;
     : Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info.QU_FN_ASC;
     : Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info.PRD_TIM_VEHSS;
     =============================================================================*/
    
    Proxy_Tx_Write_Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo(&Proxy_TxBus.Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo);
    /*==============================================================================
    * Members of structure Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo 
     : Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.QU_SER_ECBA;
     : Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.TAR_BRTORQ_SUM_COOTD;
     : Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.AVL_WAY_BRKFA;
     : Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.AVL_FORC_BRKFA;
     =============================================================================*/
    
    Proxy_Tx_Write_Proxy_TxAVL_QUAN_EES_WHLInfo(&Proxy_TxBus.Proxy_TxAVL_QUAN_EES_WHLInfo);
    /*==============================================================================
    * Members of structure Proxy_TxAVL_QUAN_EES_WHLInfo 
     : Proxy_TxAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_RLH;
     : Proxy_TxAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_RRH;
     : Proxy_TxAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_FLH;
     : Proxy_TxAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_FRH;
     : Proxy_TxAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_FLH;
     : Proxy_TxAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_RRH;
     : Proxy_TxAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_FRH;
     : Proxy_TxAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_RLH;
     =============================================================================*/
    
    Proxy_Tx_Write_Proxy_TxTAR_LTRQD_BAXInfo(&Proxy_TxBus.Proxy_TxTAR_LTRQD_BAXInfo);
    /*==============================================================================
    * Members of structure Proxy_TxTAR_LTRQD_BAXInfo 
     : Proxy_TxTAR_LTRQD_BAXInfo.TAR_LTRQD_BAX;
     : Proxy_TxTAR_LTRQD_BAXInfo.LIM_MAX_LTRQD_BAX;
     =============================================================================*/
    
    Proxy_Tx_Write_Proxy_TxST_YMRInfo(&Proxy_TxBus.Proxy_TxST_YMRInfo);
    /*==============================================================================
    * Members of structure Proxy_TxST_YMRInfo 
     : Proxy_TxST_YMRInfo.QU_SER_CLCTR_YMR;
     =============================================================================*/
    
    Proxy_Tx_Write_Proxy_TxWEGSTRECKEInfo(&Proxy_TxBus.Proxy_TxWEGSTRECKEInfo);
    /*==============================================================================
    * Members of structure Proxy_TxWEGSTRECKEInfo 
     : Proxy_TxWEGSTRECKEInfo.MILE_FLH;
     : Proxy_TxWEGSTRECKEInfo.MILE_FRH;
     : Proxy_TxWEGSTRECKEInfo.MILE_INTM_TOMSRM;
     : Proxy_TxWEGSTRECKEInfo.MILE;
     =============================================================================*/
    
    Proxy_Tx_Write_Proxy_TxDISP_CC_DRDY_00Info(&Proxy_TxBus.Proxy_TxDISP_CC_DRDY_00Info);
    /*==============================================================================
    * Members of structure Proxy_TxDISP_CC_DRDY_00Info 
     : Proxy_TxDISP_CC_DRDY_00Info.ST_IDC_CC_DRDY_00;
     : Proxy_TxDISP_CC_DRDY_00Info.ST_CC_DRDY_00;
     : Proxy_TxDISP_CC_DRDY_00Info.TRANF_CC_DRDY_00;
     : Proxy_TxDISP_CC_DRDY_00Info.NO_CC_DRDY_00;
     =============================================================================*/
    
    Proxy_Tx_Write_Proxy_TxDISP_CC_BYPA_00Info(&Proxy_TxBus.Proxy_TxDISP_CC_BYPA_00Info);
    /*==============================================================================
    * Members of structure Proxy_TxDISP_CC_BYPA_00Info 
     : Proxy_TxDISP_CC_BYPA_00Info.TRANF_CC_BYPA_00;
     : Proxy_TxDISP_CC_BYPA_00Info.ST_IDC_CC_BYPA_00;
     : Proxy_TxDISP_CC_BYPA_00Info.ST_CC_BYPA_00;
     : Proxy_TxDISP_CC_BYPA_00Info.NO_CC_BYPA_00;
     =============================================================================*/
    
    Proxy_Tx_Write_Proxy_TxST_VHSSInfo(&Proxy_TxBus.Proxy_TxST_VHSSInfo);
    /*==============================================================================
    * Members of structure Proxy_TxST_VHSSInfo 
     : Proxy_TxST_VHSSInfo.ST_VEHSS;
     =============================================================================*/
    
    Proxy_Tx_Write_Proxy_TxDIAG_OBD_HYB_1Info(&Proxy_TxBus.Proxy_TxDIAG_OBD_HYB_1Info);
    /*==============================================================================
    * Members of structure Proxy_TxDIAG_OBD_HYB_1Info 
     : Proxy_TxDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_1_ERR;
     : Proxy_TxDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_3_ERR;
     : Proxy_TxDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_4_ERR;
     : Proxy_TxDIAG_OBD_HYB_1Info.DIAG_OBD_HYB_1_MUX_MAX;
     : Proxy_TxDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_2_ERR;
     : Proxy_TxDIAG_OBD_HYB_1Info.DIAG_OBD_HYB_1_MUX_IMME;
     =============================================================================*/
    
    Proxy_Tx_Write_Proxy_TxSU_DSCInfo(&Proxy_TxBus.Proxy_TxSU_DSCInfo);
    /*==============================================================================
    * Members of structure Proxy_TxSU_DSCInfo 
     : Proxy_TxSU_DSCInfo.SU_DSC_WSS;
     =============================================================================*/
    
    Proxy_Tx_Write_Proxy_TxST_TYR_RDCInfo(&Proxy_TxBus.Proxy_TxST_TYR_RDCInfo);
    /*==============================================================================
    * Members of structure Proxy_TxST_TYR_RDCInfo 
     : Proxy_TxST_TYR_RDCInfo.QU_TPL_RDC;
     : Proxy_TxST_TYR_RDCInfo.QU_FN_TYR_INFO_RDC;
     : Proxy_TxST_TYR_RDCInfo.QU_TFAI_RDC;
     =============================================================================*/
    
    Proxy_Tx_Write_Proxy_TxST_TYR_RPAInfo(&Proxy_TxBus.Proxy_TxST_TYR_RPAInfo);
    /*==============================================================================
    * Members of structure Proxy_TxST_TYR_RPAInfo 
     : Proxy_TxST_TYR_RPAInfo.QU_TFAI_RPA;
     : Proxy_TxST_TYR_RPAInfo.QU_FN_TYR_INFO_RPA;
     : Proxy_TxST_TYR_RPAInfo.QU_TPL_RPA;
     =============================================================================*/
    
    Proxy_Tx_Write_Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info(&Proxy_TxBus.Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info);
    /*==============================================================================
    * Members of structure Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info 
     : Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_2;
     : Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_1;
     : Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_3;
     : Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_4;
     =============================================================================*/
    
    Proxy_Tx_Write_Proxy_TxST_TYR_2Info(&Proxy_TxBus.Proxy_TxST_TYR_2Info);
    /*==============================================================================
    * Members of structure Proxy_TxST_TYR_2Info 
     : Proxy_TxST_TYR_2Info.QU_RDC_INIT_DISP;
     =============================================================================*/
    
    Proxy_Tx_Write_Proxy_TxAVL_T_TYRInfo(&Proxy_TxBus.Proxy_TxAVL_T_TYRInfo);
    /*==============================================================================
    * Members of structure Proxy_TxAVL_T_TYRInfo 
     : Proxy_TxAVL_T_TYRInfo.AVL_TEMP_TYR_RRH;
     : Proxy_TxAVL_T_TYRInfo.AVL_TEMP_TYR_FLH;
     : Proxy_TxAVL_T_TYRInfo.AVL_TEMP_TYR_RLH;
     : Proxy_TxAVL_T_TYRInfo.AVL_TEMP_TYR_FRH;
     =============================================================================*/
    
    Proxy_Tx_Write_Proxy_TxTEMP_BRKInfo(&Proxy_TxBus.Proxy_TxTEMP_BRKInfo);
    /*==============================================================================
    * Members of structure Proxy_TxTEMP_BRKInfo 
     : Proxy_TxTEMP_BRKInfo.TEMP_BRDSK_RLH_VRFD;
     : Proxy_TxTEMP_BRKInfo.QU_TEMP_BRDSK_RLH_VRFD;
     : Proxy_TxTEMP_BRKInfo.TEMP_BRDSK_RRH_VRFD;
     : Proxy_TxTEMP_BRKInfo.QU_TEMP_BRDSK_RRH_VRFD;
     =============================================================================*/
    
    Proxy_Tx_Write_Proxy_TxAVL_P_TYRInfo(&Proxy_TxBus.Proxy_TxAVL_P_TYRInfo);
    /*==============================================================================
    * Members of structure Proxy_TxAVL_P_TYRInfo 
     : Proxy_TxAVL_P_TYRInfo.AVL_P_TYR_FRH;
     : Proxy_TxAVL_P_TYRInfo.AVL_P_TYR_FLH;
     : Proxy_TxAVL_P_TYRInfo.AVL_P_TYR_RRH;
     : Proxy_TxAVL_P_TYRInfo.AVL_P_TYR_RLH;
     =============================================================================*/
    
    Proxy_Tx_Write_Proxy_TxFR_DBG_DSCInfo(&Proxy_TxBus.Proxy_TxFR_DBG_DSCInfo);
    /*==============================================================================
    * Members of structure Proxy_TxFR_DBG_DSCInfo 
     : Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_13;
     : Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_04;
     : Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_05;
     : Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_06;
     : Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_01;
     : Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_02;
     : Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_03;
     : Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_07;
     : Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_11;
     : Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_10;
     : Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_09;
     : Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_15;
     : Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_14;
     : Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_12;
     : Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_00;
     : Proxy_TxFR_DBG_DSCInfo.BDTM_TA_FR;
     : Proxy_TxFR_DBG_DSCInfo.BDTM_TA_RL;
     : Proxy_TxFR_DBG_DSCInfo.BDTM_TA_RR;
     : Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_08;
     : Proxy_TxFR_DBG_DSCInfo.ST_STATE;
     : Proxy_TxFR_DBG_DSCInfo.BDTM_TA_FL;
     : Proxy_TxFR_DBG_DSCInfo.ABSRef;
     : Proxy_TxFR_DBG_DSCInfo.LOW_VOLTAGE;
     : Proxy_TxFR_DBG_DSCInfo.DEBUG_PCIB;
     : Proxy_TxFR_DBG_DSCInfo.DBG_DSC;
     : Proxy_TxFR_DBG_DSCInfo.LATACC;
     : Proxy_TxFR_DBG_DSCInfo.TRIGGER_FS;
     : Proxy_TxFR_DBG_DSCInfo.TRIGGER_IS;
     =============================================================================*/
    

    Proxy_Tx_Timer_Elapsed = STM0_TIM0.U - Proxy_Tx_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define PROXY_TX_STOP_SEC_CODE
#include "Proxy_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
