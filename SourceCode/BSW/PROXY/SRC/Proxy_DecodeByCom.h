/**
 * @defgroup Proxy_Decode ApplCom_Decode
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Proxy_Decode.h
 * @brief       Local Header File
 * @date        20YY. MM. DD.
 ******************************************************************************/
 
#ifndef PROXY_DECODE_H_
#define PROXY_DECODE_H_

/*==============================================================================
 *                  INCLUDE FILES
 ==============================================================================*/
#include "Proxy_Rx.h"
#include "Proxy_Types.h"
#include "Proxy_Cfg.h"
#include "Com.h"
 
/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/
 
/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
extern CC_RxChkVar_t CLU2_CANIGN_RxChk;
extern CC_RxChkVar_t FATC1_RxChk;
extern CC_RxChkVar_t TCU5_RxChk;
extern CC_RxChkVar_t TCU6_RxChk;
extern CC_RxChkVar_t TCU2_RxChk;
extern CC_RxChkVar_t CGW1_RxChk;
extern CC_RxChkVar_t CGW1_CANIGN_RxChk;
extern CC_SignalChk_t SCC_Ems1EngSpd_InvChk;
extern CC_SignalChk_t SCC_FTcu_InvChk;
extern CC_SignalChk_t PVAV_CAN_InvChk;
extern CC_SignalChk_t TPS_InvChk;
extern CC_SignalChk_t G_SEL_DISP_InvChk;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Proxy_RxProc(void);
extern void Proxy_CanIfRxIndication(Can_IdType RxPduId, PduInfoType* PduInfoPtr);
extern void Proxy_vChkCan(void);
extern void CC_vClearCanError(uint8_t Variant,uint8_t ccu8_CH);

/*==============================================================================
 *                  END OF FILE
 ==============================================================================*/
#endif /* APPLCOM_DECODE_H_ */
/** @} */
