/**
 * @defgroup Proxy_Decode Proxy_Decode
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Proxy_Decode.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Proxy_Decode.h"
#include "Eem_Main.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define SWAP_FUNCTION(A,B) A^=B^=A^=B;

/*============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PROXY_START_SEC_CONST_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PROXY_STOP_SEC_CONST_UNSPECIFIED
#include "Proxy_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLES DEFINITIONS
 =============================================================================*/
extern Proxy_Rx_HdrBusType Proxy_RxBus;
extern Proxy_Tx_HdrBusType Proxy_TxBus;

AC_CHK_FLG_t            CanChk1;
AC_CHK2_FLG_t           CanChk2;
uint8                   acu8PwrCeStat;
uint16                  acu16TimeoutChkPeriod;
uint8                   acu8ChkSlot20ms;
uint8                   acu8ChkSlot40ms;
uint8                   acu8ChkSlot100ms;
uint8                   acu8ChkSlot200ms;
uint8                   acu8ChkSlot400ms;
uint8                   CAN_overrun_cnt;
uint8                   ccu8BusOffCnt;
uint16                  ccu16CANAddChkTime;
uint8                   Proxy_fu1AllControl;
uint8                   Proxy_lcrbcu1ActiveFlg;
uint8                   ccu1AxSnsrSetFlg;
Proxy_RX_MISSING_FLG_t  cc_RX_MISSING_t;
CC_TimeoutVar_t         RxMsg_ToChk[CHK_CAN_MAX_NUM];

#define PROXY_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PROXY_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Proxy_MemMap.h"
#define PROXY_START_SEC_VAR_NOINIT_32BIT
#include "Proxy_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PROXY_STOP_SEC_VAR_NOINIT_32BIT
#include "Proxy_MemMap.h"
#define PROXY_START_SEC_VAR_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PROXY_STOP_SEC_VAR_UNSPECIFIED
#include "Proxy_MemMap.h"
#define PROXY_START_SEC_VAR_32BIT
#include "Proxy_MemMap.h"
/** Variable Section (32BIT)**/


#define PROXY_STOP_SEC_VAR_32BIT
#include "Proxy_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLES DEFINITIONS
 =============================================================================*/
 static uint8 cu8ClrFltVarblCh;

#define PROXY_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
uint8   Proxy_fcu6TMMessage;
uint8   Proxy_fcu8LWSSpeed;
uint8   Proxy_fcu1LWSCal;
uint8   Proxy_fcu1LWSTrim;
uint8   Proxy_fcu4LWSCheckSum;
uint8   Proxy_fcu4LWSMsgCount;
uint8   Proxy_fu1SteerMsgCopyOk;
uint8   Proxy_stg_sensor_ok_flg;
uint8   Proxy_fcu1TPSError;
uint8   Proxy_fcu8PVAV;
uint8   Proxy_fcu1PVAVError;
uint8   Proxy_fcu8TPS;

uint16 Proxy_fcu16EngRpm;
uint16 Proxy_Proxy_fu1BFLSignalInv;
uint16 Proxy_fu1BFLSignal;
uint32 Proxy_fcu24SenSerialNumber;
uint8 Proxy_fcu4SenRollingCount;
uint8 Proxy_fcu1SenAYCbitState;
uint8 Proxy_fcu1SenYawCbitState;
sint8 Proxy_fcs8SenRasterFreqency;
uint8 Proxy_fcu1SenFailAD;
uint8 Proxy_fcu1SenFailROM;
uint8 Proxy_fcu1SenFailRAM;
uint8 Proxy_fcu1SenFailAccel;
uint8 Proxy_fcu1SenFailGyro;
uint8 Proxy_fcu1SenFailPowerRange;
uint8 Proxy_fcu1SenFailEEPROM;
uint8 Proxy_fcu1SenFailASIC;
uint8 Proxy_fcu1SenFailCanFunctionality;
uint8 Proxy_fcu1SenFailPlausibility;
uint8 Proxy_fcu1SenFailRaster;
uint8 Proxy_fcu1SenFailWatchDog;
uint8 Proxy_fcu1SenFailOscilator;
uint8 Proxy_fu1YawMsgCopyOk;
uint8 Proxy_fu1YawLongMsgCopyOk;
uint8 Proxy_fcu1AxFaultActive;
uint8 Proxy_fcu1AxFaultPresent;
uint8 Proxy_fcu1AxCircuitErrPresent;
uint8 Proxy_fcu1AxRangeErrorPresent;
uint8 Proxy_fcu4AxRollingCounter;
uint8 Proxy_fcu1AxInternalTempFault;
uint8 Proxy_fcu1AxCBITStatus;
uint8 Proxy_lespu8TCU_G_SEL_DISP;

uint8 Proxy_lespu1TCU_SWI_GS;
uint8 Proxy_cu8CF_Tcu_ShiftClass;
uint8 Proxy_cu1CF_Hcu_HevRdy;
uint32 Proxy_cu32CR_Hcu_EngTqCmd_Pc_ConvX10K;

sint32 Proxy_cs32CR_Hcu_CrpTq_Nm_Conv;
sint32 Proxy_cs32CR_Hcu_WhlDemTq_Nm_Conv;
sint32 Proxy_cs32CR_Hcu_TMInTqCmdBInv_ConvXK;
sint32 Proxy_cs32CR_Hcu_MotTqCmd_pc_ConvXK;
uint8 Proxy_cu8CR_Hcu_HevMod;

sint32 Proxy_cs32CR_Mot_ActRotSpd_rpm_Conv;
uint8 Proxy_cu1CF_Mcu_Flt;
uint16 Proxy_cu16CR_Bms_Soc_Pc_ConvX10;
uint8 Proxy_fcu8GearPosition;

uint8 Proxy_lespu1EMS_HEV_AT_TCU;
uint8 Proxy_ccu1ParkingSwitch;
uint8 Proxy_ccu1GetImuSerialNum;

uint8   Proxy_ccu1IgnRun;
uint8   ccu8Clu1ParkBrkAct;
uint8   ccu1ParkingSwitch_CLU;
uint8 ccu1ImuYawRXFault;

uint8 Proxy_cu1_YAW_RxMissing_Flg;
uint8   Proxy_cu1_AY_RxMissing_Flg;
uint8   Proxy_cu1Tcu6RxOk_Flg;
uint8   Proxy_cu1_AX_RxMissing_Flg;
uint8   Proxy_ccu1ParkingSwitchInv;
uint8 Proxy_ccu1IgnRunInv;

CC_RxChkVar_t TCU6_RxChk;
CC_RxChkVar_t TCU5_RxChk;
CC_RxChkVar_t TCU1_RxChk;
CC_SignalChk_t SCC_FTcu_InvChk;
CC_TimeoutVar_t TCU1_ToChk;
CC_SignalChk_t G_SEL_DISP_InvChk;
CC_RxChkVar_t FATC1_RxChk;
CC_RxChkVar_t EMS3_RxChk;
CC_RxChkVar_t EMS2_RxChk;
CC_SignalChk_t SCC_Ems1EngSpd_InvChk;
CC_TimeoutVar_t EMS2_ToChk;
CC_SignalChk_t PVAV_CAN_InvChk;
CC_SignalChk_t TPS_InvChk;
CC_RxChkVar_t EMS1_RxChk;
CC_RxChkVar_t CLU2_RxChk;
CC_RxChkVar_t CLU2_CANIGN_RxChk;
CC_RxChkVar_t CLU1_RxChk;
uint8 ccu1ImuYawRXFault;
Proxy_TimeoutSusp_t ccIMU_YAW;
uint8   ccu1ParkingSwitch_CLU_tout;
uint8 ccu1ParkingSwitchInv_CLU;
uint8 ccu1AxSnsrSetFlg;

#define PROXY_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Proxy_MemMap.h"
#define PROXY_START_SEC_VAR_NOINIT_32BIT
#include "Proxy_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PROXY_STOP_SEC_VAR_NOINIT_32BIT
#include "Proxy_MemMap.h"
#define PROXY_START_SEC_VAR_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PROXY_STOP_SEC_VAR_UNSPECIFIED
#include "Proxy_MemMap.h"
#define PROXY_START_SEC_VAR_32BIT
#include "Proxy_MemMap.h"
/** Variable Section (32BIT)**/


#define PROXY_STOP_SEC_VAR_32BIT
#include "Proxy_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PROXY_START_SEC_CODE
#include "Proxy_MemMap.h"
static void CC_vClearErrOem(uint8 Variant);
static void CC_vClearTimeout(CC_TimeoutVar_t *CanTOVarPtr, uint8 u8TempErrNum);
static void CC_vClearInvalidSignal(CC_SignalChk_t *ToChkSignal, uint8 u8TempErrNum);

static void Proxy_RxBms1(void);
static void Proxy_RxYawSerial(void);
static void Proxy_RxYawAcc(void);
static void Proxy_RxTcu6(void);
static void Proxy_RxTcu5(void);
static void Proxy_RxTcu1(void);
static void Proxy_RxSas(void);
static void Proxy_RxMcu2(void);
static void Proxy_RxMcu1(void);
static void Proxy_RxLongAcc(void);
static void Proxy_RxHcu5(void);
static void Proxy_RxHcu3(void);
static void Proxy_RxHcu2(void);
static void Proxy_RxHcu1(void);
static void Proxy_RxFatc1(void);
static void Proxy_RxEms3(void);
static void Proxy_RxEms2(void);
static void Proxy_RxEms1(void);
static void Proxy_RxClu2(void);
static void Proxy_RxClu1(void);
static void Proxy_RxApplyCfgInfo(void);
static void CC_vLoadRxInterface(void);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

void Proxy_RxProc(void)
{
    Proxy_RxBms1();
    Proxy_RxYawSerial();
    Proxy_RxYawAcc();
    Proxy_RxTcu6();
    Proxy_RxTcu5();
    Proxy_RxTcu1();
    Proxy_RxSas();
    Proxy_RxMcu2();
    Proxy_RxMcu1();
    Proxy_RxLongAcc();
    Proxy_RxHcu5();
    Proxy_RxHcu3();
    Proxy_RxHcu2();
    Proxy_RxHcu1();
    Proxy_RxFatc1();
    Proxy_RxEms3();
    Proxy_RxEms2();
    Proxy_RxEms1();
    Proxy_RxClu2();
    Proxy_RxClu1();
    CC_vLoadRxInterface();
	if(Eem_MainDiagClrSrs == 1)
	{
		CC_vClearCanError(1,3);
	}
    /*
    Proxy_RxBus.Proxy_RxCanRxEemInfo.Ems1RxErr = RxMsg_ToChk[CHK_CAN_EMS1_MSG_RX].u1RxErrFlg;
    Proxy_RxBus.Proxy_RxCanRxEemInfo.Ems2RxErr = RxMsg_ToChk[CHK_CAN_EMS2_MSG_RX].u1RxErrFlg;
    Proxy_RxBus.Proxy_RxCanRxEemInfo.Tcu1RxErr = RxMsg_ToChk[CHK_CAN_TCU1_MSG_RX].u1RxErrFlg;
    Proxy_RxBus.Proxy_RxCanRxEemInfo.Tcu5RxErr = RxMsg_ToChk[CHK_CAN_TCU5_MSG_RX].u1RxErrFlg;
    Proxy_RxBus.Proxy_RxCanRxEemInfo.Hcu1RxErr = RxMsg_ToChk[CHK_CAN_HCU1_MSG_RX].u1RxErrFlg;
    Proxy_RxBus.Proxy_RxCanRxEemInfo.Hcu2RxErr = RxMsg_ToChk[CHK_CAN_HCU2_MSG_RX].u1RxErrFlg;
    Proxy_RxBus.Proxy_RxCanRxEemInfo.Hcu3RxErr = RxMsg_ToChk[CHK_CAN_HCU3_MSG_RX].u1RxErrFlg;
    */
    
    Proxy_RxBus.Proxy_RxCanRxEemInfo.Ems1RxErr = EMS1_RxChk.u1RxMissingFlg;
    Proxy_RxBus.Proxy_RxCanRxEemInfo.Ems2RxErr = EMS2_RxChk.u1RxMissingFlg;
    Proxy_RxBus.Proxy_RxCanRxEemInfo.Tcu1RxErr = TCU1_RxChk.u1RxMissingFlg;
    Proxy_RxBus.Proxy_RxCanRxEemInfo.Tcu5RxErr = TCU5_RxChk.u1RxMissingFlg;
    Proxy_RxBus.Proxy_RxCanRxEemInfo.Hcu1RxErr = 0;
    Proxy_RxBus.Proxy_RxCanRxEemInfo.Hcu2RxErr = 0;
    Proxy_RxBus.Proxy_RxCanRxEemInfo.Hcu3RxErr = 0;

    Proxy_RxApplyCfgInfo();
}

void Proxy_RxApplyCfgInfo(void)
{
    if (Proxy_Config.AxAySwapEnable == ENABLE)
    {
        SWAP_FUNCTION(Proxy_RxBus.Proxy_RxCanRxEscInfo.Ax, Proxy_RxBus.Proxy_RxCanRxEscInfo.Ay)
        SWAP_FUNCTION(Proxy_RxBus.Proxy_RxCanRxEemInfo.AxInvldData, Proxy_RxBus.Proxy_RxCanRxEemInfo.AyInvld)
        SWAP_FUNCTION(Proxy_fcu1AxCBITStatus, Proxy_fcu1SenAYCbitState)
    }
}

void Proxy_SetRxOKByAppl(Proxy_MsgIdType MsgId)
{
  RxMsg_ToChk[MsgId].u1RxOkFlg=1;
}

void Proxy_SetRxCopyOKByAppl(Proxy_MsgIdType MsgId)
{
  RxMsg_ToChk[MsgId].u1RxCopyOkFlg=1;
}

uint8 Proxy_GetMsgValidFlag(Proxy_MsgIdType MsgId)
{
  return (uint8)RxMsg_ToChk[MsgId].u1RxValidFlg;
}

static void Proxy_vChkSysVariant(void)
{
    ccu1AxSnsrSetFlg = 1;
}

static uint8 AC_u8SetCanChkInhibit(void)
{
    uint8  cc_u8ChkInhibit;
    cc_u8ChkInhibit=0;
    return cc_u8ChkInhibit;
}

static void Proxy_CanTimeoutChk(CC_TimeoutVar_t *CanTOVarPtr, uint8 ccu8_IncFactor, uint8 ccu8_DecFactor, uint16 ccu16_ErrorOnTime)
{
    uint16 ccu16_ErrCnt;
    uint8 ccu8_ChkCanMsgFlg;

    if(CanTOVarPtr->u2NetChannel==FLEXCAN_MODULE_A)
    {
        ccu8_ChkCanMsgFlg=ccu1MainCheckCanFMsgFlg;
    }
    else
    {
        ccu8_ChkCanMsgFlg=ccu1SubCheckCanFMsgFlg;
    }
    if(ccu8_ChkCanMsgFlg==1)     /*Each Channel Check Enable - if Check is Disable, failure variable is cleared before function call.  So this functioin excute nothing*/
    {
        ccu16_ErrCnt=ccu16_ErrorOnTime;

        if(ccu1InhibitToChk==0)  /*in case of call each Message, concludes Inhibit*/
        {
            if(CanTOVarPtr->u1RxOkFlg==1)
            {
                CanTOVarPtr->u16RxOkCnt=CanTOVarPtr->u16RxOkCnt+ccu8_IncFactor;
                CanTOVarPtr->u1RxOkFlg=0;
                CanTOVarPtr->ui1TimeoutSusp=0;
                if(CanTOVarPtr->u16RxErrCnt<=ccu8_DecFactor)
                {
                    CanTOVarPtr->u16RxErrCnt=0;
                }
                else
                {
                    CanTOVarPtr->u16RxErrCnt=CanTOVarPtr->u16RxErrCnt-ccu8_DecFactor;
                }
            }
            else
            {
                CanTOVarPtr->u16RxErrCnt=CanTOVarPtr->u16RxErrCnt+ccu8_IncFactor;
                CanTOVarPtr->u16RxOkCnt=0;
                CanTOVarPtr->u1RxValidFlg=0;
                CanTOVarPtr->ui1TimeoutSusp=1;
            }

            if(CanTOVarPtr->u16RxOkCnt>=ccu16_ErrorOnTime)
            {
                CanTOVarPtr->u16RxOkCnt=ccu16_ErrorOnTime;
                CanTOVarPtr->u16RxErrCnt=0;
                CanTOVarPtr->u1RxValidFlg=1;
            }
            else
            {
                ;
            }

            if(CanTOVarPtr->u16RxErrCnt>=ccu16_ErrCnt+ccu16CANAddChkTime)
            {
                CanTOVarPtr->u1RxErrFlg=1;
                CanTOVarPtr->u16RxErrCnt=ccu16_ErrCnt+ccu16CANAddChkTime;
            }
            else
            {
                ;
            }
        }
        else
        {
            ccu1InhibitToChk=0;
            CanTOVarPtr->u16RxOkCnt  =0;
            CanTOVarPtr->u1RxValidFlg=0;
            CanTOVarPtr->u1RxOkFlg   =0;
            CanTOVarPtr->u16RxErrCnt =0;
            CanTOVarPtr->ui1TimeoutSusp=0;
        }
    }
}

static void Proxy_ChkTimeoutOem(void)
{
    ccu1InhibitToChk=0;
    switch(acu8ChkSlot20ms) /* Task number : 0~1 */
    {
        case 0:
            Proxy_CanTimeoutChk(&RxMsg_ToChk[CHK_CAN_EMS1_MSG_RX], U8_CC_TIME_20MS, U8_CC_TIME_10MS, U8_CC_TIME_500MS);
            Proxy_CanTimeoutChk(&RxMsg_ToChk[CHK_CAN_HCU1_MSG_RX], U8_CC_TIME_20MS, U8_CC_TIME_10MS, U8_CC_TIME_500MS);
            Proxy_CanTimeoutChk(&RxMsg_ToChk[CHK_CAN_HCU3_MSG_RX], U8_CC_TIME_20MS, U8_CC_TIME_10MS, U8_CC_TIME_500MS);
            break;
        case 1:
            Proxy_CanTimeoutChk(&RxMsg_ToChk[CHK_CAN_EMS2_MSG_RX], U8_CC_TIME_20MS, U8_CC_TIME_10MS, U8_CC_TIME_500MS);
            Proxy_CanTimeoutChk(&RxMsg_ToChk[CHK_CAN_HCU2_MSG_RX], U8_CC_TIME_20MS, U8_CC_TIME_10MS, U8_CC_TIME_500MS);
            Proxy_CanTimeoutChk(&RxMsg_ToChk[CHK_CAN_TCU1_MSG_RX], U8_CC_TIME_20MS, U8_CC_TIME_10MS, U8_CC_TIME_500MS);
            break;
    }
    ccu1InhibitToChk=0;
    switch(acu8ChkSlot40ms) /* Task number : 0~3 */
    {
        case 0:
            Proxy_CanTimeoutChk(&RxMsg_ToChk[CHK_CAN_CLU1_MSG_RX], U8_CC_TIME_40MS, U8_CC_TIME_20MS, U8_CC_TIME_500MS);
            break;
    }
    ccu1InhibitToChk=0;
    switch(acu8ChkSlot200ms) /* Task number : 0~19 */
    {
        case 0:
            Proxy_CanTimeoutChk(&RxMsg_ToChk[CHK_CAN_TCU5_MSG_RX], U8_CC_TIME_200MS, U8_CC_TIME_100MS, U8_CC_TIME_500MS);
            break;
    }
}

static void Proxy_ChkMsgCan(void)
{
    acu16TimeoutChkPeriod=acu16TimeoutChkPeriod+1;
    acu8ChkSlot20ms =(uint8)(acu16TimeoutChkPeriod%((uint16)U8_CC_TIME_20MS));
    acu8ChkSlot40ms =(uint8)(acu16TimeoutChkPeriod%((uint16)U8_CC_TIME_40MS));
    acu8ChkSlot100ms=(uint8)(acu16TimeoutChkPeriod%((uint16)U8_CC_TIME_100MS));
    acu8ChkSlot200ms=(uint8)(acu16TimeoutChkPeriod%((uint16)U8_CC_TIME_200MS));
    acu8ChkSlot400ms=(uint8)(acu16TimeoutChkPeriod%((uint16)U8_CC_TIME_400MS));

    if(acu16TimeoutChkPeriod==U8_CC_TIME_400MS)
    {
        acu16TimeoutChkPeriod=0;
    }
    else
    {
        ;
    }
    Proxy_ChkTimeoutOem();
}

static void Proxy_DetCanError(void)
{
    if(ccu1TxDisableFlg==0)
    {
        if(CAN_bus_off_flg==0)
        {
            ccu1MainCheckCanFMsgFlg=1;
        }
        else
        {
           ccu1MainCheckCanFMsgFlg=0;
        }
    }
    else
    {
        ccu1MainCheckCanFMsgFlg=0;

        ccu8BusOffCnt=0;
        CAN_overrun_cnt=0;
        CAN_overrun_flg=0;
    }
    Proxy_ChkMsgCan();
}

uint8 CC_vCanMsgRxChk(Proxy_TimeoutSusp_t *RxChkPtr, uint16 u16MsgPeriod)
{
  RxChkPtr->u16ChkPeriod=RxChkPtr->u16ChkPeriod+1;

  if(RxChkPtr->u16ChkPeriod>=(u16MsgPeriod*2))
  {
    if(RxChkPtr->u1RxCopyFlg==0)
    {
      RxChkPtr->u1RxFaultFlg=1;
    }
    else
    {
      RxChkPtr->u1RxFaultFlg=0;
    }
    RxChkPtr->u16ChkPeriod=0;
    RxChkPtr->u1RxCopyFlg =0;
  }
  else
  {
    ;
  }

  return RxChkPtr->u1RxFaultFlg;
}

void CC_vCanReceiveChk(CC_RxChkVar_t *CanRxChkPtr, uint8 ccu8_RxFlg, uint16 ccu16_RxMissTime)
{
    if(ccu8_RxFlg==1)
    {
        CanRxChkPtr->u16RxMissingCnt=0;
        CanRxChkPtr->u1RxMissingFlg=0;
    }
    else
    {
        if(CanRxChkPtr->u16RxMissingCnt>=ccu16_RxMissTime)
        {
            CanRxChkPtr->u1RxMissingFlg=1;
        }
        else
        {
            CanRxChkPtr->u16RxMissingCnt=CanRxChkPtr->u16RxMissingCnt+1;
        }
    }
}

void CC_vChkCanMsgSignal(CC_SignalChk_t *ToChkSignal, uint8 cc_u8ChkInhint, uint8 cc_u8RxData)
{
    uint16 ccu16_ErrCnt;
    uint8 ccu8_ChkCanMsgFlg;

    if(ToChkSignal->u2NetChannel==FLEXCAN_MODULE_A)   /*Dual CAN 고려 Check Enable Flag Main/Sub 분리*/
    {
        ccu8_ChkCanMsgFlg=ccu1MainCheckCanFMsgFlg;

    }
    else
    {
        ccu8_ChkCanMsgFlg=ccu1SubCheckCanFMsgFlg;
    }

    if((Proxy_fu1AllControl==1)||(Proxy_lcrbcu1ActiveFlg==1))
    {
        ccu16_ErrCnt=(uint16)cc_u8RxData;
    }
    else
    {
        ccu16_ErrCnt=(uint16)(((uint16)cc_u8RxData)*3);
    }

    if((cc_u8ChkInhint==0)&&(ccu8_ChkCanMsgFlg==1))
    {
        if(ToChkSignal->u1RefSigErrDet==1)
        {
            ToChkSignal->u8OkCnt = 0;
            ToChkSignal->u1OkFlg = 0;
            ToChkSignal->u16ErrorCnt = ToChkSignal->u16ErrorCnt + 1;
        }
        else
        {
            ToChkSignal->u8OkCnt = ToChkSignal->u8OkCnt + 1;
        }

        if(ToChkSignal->u16ErrorCnt >= ccu16_ErrCnt)
        {
            ToChkSignal->u1ErrorFlg  = 1;
            ToChkSignal->u16ErrorCnt = ccu16_ErrCnt;
        }
        else if(ToChkSignal->u16ErrorCnt >= 5)
        {
            ToChkSignal->u1SigErrSuspFlg=1;
        }
        else
        {
            ;
        }

        if(ToChkSignal->u8OkCnt >= cc_u8RxData)
        {
            ToChkSignal->u1OkFlg = 1;
            ToChkSignal->u8OkCnt = cc_u8RxData;
        }
        else
        {
            ;
        }

        if(ToChkSignal->u8OkCnt >= 3)
        {
            ToChkSignal->u16ErrorCnt    =0;
            ToChkSignal->u1SigErrSuspFlg=0;
        }
        else
        {
            ;
        }
    }
    else
    {
        ToChkSignal->u8OkCnt = 0;
        ToChkSignal->u1OkFlg = 0;
        ToChkSignal->u16ErrorCnt    = 0;
        ToChkSignal->u1RefSigErrDet = 0;
        ToChkSignal->u1SigErrSuspFlg= 0;
    }
}

void CC_vClearCanError(uint8 Variant,uint8 ccu8_CH)
{
    if((Variant==1) || (Variant==3))
    {
        /* ccu16TimeoutChkPeriod=0;*/
    }
    else
    {
        ;
    }
    cu8ClrFltVarblCh=ccu8_CH;
    CC_vClearErrOem(Variant);
}

static void CC_vClearErrOem(uint8 Variant)
{
    /*** Clear Timeout Variables ***/

        CC_vClearTimeout(&RxMsg_ToChk[CHK_CAN_EMS1_MSG_RX], Variant);

        CC_vClearTimeout(&RxMsg_ToChk[CHK_CAN_EMS2_MSG_RX], Variant);

    #ifdef __EMS5ToChk_ENA_ENABLE
        CC_vClearTimeout(&EMS5_ToChk, Variant);
    #endif

        CC_vClearTimeout(&RxMsg_ToChk[CHK_CAN_TCU1_MSG_RX], Variant);

    #ifdef __TCU2ToChk_ENA_ENABLE
        CC_vClearTimeout(&TCU2_ToChk, Variant);
    #endif

        CC_vClearTimeout(&RxMsg_ToChk[CHK_CAN_TCU5_MSG_RX], Variant);

        CC_vClearTimeout(&RxMsg_ToChk[CHK_CAN_HCU1_MSG_RX], Variant);

        CC_vClearTimeout(&RxMsg_ToChk[CHK_CAN_HCU2_MSG_RX], Variant);

        CC_vClearTimeout(&RxMsg_ToChk[CHK_CAN_HCU3_MSG_RX], Variant);

    #ifdef __ITM1ToChk_ENA_ENABLE
        CC_vClearTimeout(&ITM1_ToChk, Variant);
    #endif

    #ifdef __ECS1ToChk_ENA_ENABLE
        CC_vClearTimeout(&ECS1_ToChk, Variant);
    #endif

    #ifdef __ECS2ToChk_ENA_ENABLE
        CC_vClearTimeout(&ECS2_ToChk, Variant);
    #endif

    #ifdef __EPB1ToChk_ENA_ENABLE
        CC_vClearTimeout(&EPB1_ToChk, Variant);
    #endif

    #ifdef __VSM2ToChk_ENA_ENABLE
        CC_vClearTimeout(&VSM2_ToChk, Variant);
    #endif

    #ifdef __SCC1ToChk_ENA_ENABLE
        CC_vClearTimeout(&SCC1_ToChk, Variant);
    #endif

    #ifdef __SCC2ToChk_ENA_ENABLE
        CC_vClearTimeout(&SCC2_ToChk, Variant);
    #endif

    #ifdef __SCC3ToChk_ENA_ENABLE
        CC_vClearTimeout(&SCC3_ToChk, Variant);
    #endif

        CC_vClearTimeout(&RxMsg_ToChk[CHK_CAN_CLU1_MSG_RX], Variant);


    #ifdef __CLU2ToChk_ENA_ENABLE
        CC_vClearTimeout(&CLU2_ToChk, Variant);
    #endif

    #ifdef __CLU_SW_ToChk_ENA_ENABLE
        CC_vClearTimeout(&CLU_SW_ToChk, Variant);
    #endif

     /*** Clear Invalid Signal Variables ***/

        CC_vClearInvalidSignal(&SCC_Ems1EngSpd_InvChk, Variant);

    #ifdef __SCC__EMS5MSGINVALID_CHK_ENABLE
        CC_vClearInvalidSignal(&SCC_Ems5Fail_InvChk, Variant);
    #endif

        CC_vClearInvalidSignal(&SCC_FTcu_InvChk, Variant);

        CC_vClearInvalidSignal(&PVAV_CAN_InvChk, Variant);

        CC_vClearInvalidSignal(&TPS_InvChk, Variant);

        CC_vClearInvalidSignal(&G_SEL_DISP_InvChk, Variant);


    #ifdef __N_TC_ERR_CAN_INVALIDCHK_ENABLE
        CC_vClearInvalidSignal(&N_TC_InvChk, Variant);
    #endif
    #ifdef __SCC__SCCMSGINVALID_CHK_ENABLE
        CC_vClearInvalidSignal(&SCC_TorqReq_InvChk, Variant);
    #endif

    #ifdef __CLU2_GEARR_INVALIDCHK_ENABLE
        CC_vClearInvalidSignal(&AVH_GearR_InvChk, Variant);
    #endif

    #ifdef __CLU_SW_INVALIDCHK_ENABLE
        CC_vClearInvalidSignal(&ESC_SWITCH_InvChk, Variant);
    #endif

    #ifdef __EPB1_FAIL_INVALIDCHK_ENABLE
        CC_vClearInvalidSignal(&EPB_Fail_InvChk, Variant);
    #endif

    #ifdef __VSM_TORQ_INVALIDCHK_ENABLE
        CC_vClearInvalidSignal(&VSM_Torq_InvChk, Variant);
    #endif

    #ifdef __VSM_CS_INVALIDCHK_ENABLE
        CC_vClearInvalidSignal(&VSM2_CS_InvChk, Variant);
    #endif


    /*** Clear ARC Check Variables ***/
    #ifdef __VSM__ROLL_CHK_ENABLE
        CC_vClearArcChk(&VSM2_ARC, Variant);
    #endif

    #ifdef __SCC__ROLL_CHK_ENABLE
        CC_vClearArcChk(&SCC1_ARC, Variant);
    #endif


    /*** Clear Variant Coding Variables ***/
    #ifdef CC_VAR_CODING_4WD
        CC_vClearVcSingleError(&FWD_VarChk);
    #endif

    #ifdef CC_VAR_CODING_AX
        CC_vClearVcSingleError(&AX_VarChk);
    #endif

    #ifdef CC_VAR_CODING_DCT
        CC_vClearVcSingleError(&DCT_VarChk);
    #endif

    #ifdef CC_VAR_CODING_EPB
        CC_vClearVcSingleError(&EPB_VarChk);
    #endif
}

static void CC_vClearTimeout(CC_TimeoutVar_t *CanTOVarPtr, uint8 u8TempErrNum)
{
    if((cu8ClrFltVarblCh==FLEXCAN_MODULE_ALL)||(cu8ClrFltVarblCh==CanTOVarPtr->u2NetChannel))
    {
        if((u8TempErrNum==1) || (u8TempErrNum==3))
        {
            CanTOVarPtr->u16RxOkCnt=0;
            CanTOVarPtr->u1RxValidFlg=0;
            CanTOVarPtr->u1RxOkFlg=0;
            CanTOVarPtr->ui1TimeoutSusp=0;
            if(u8TempErrNum==1)
            {
                CanTOVarPtr->u1RxErrFlg=0;
                CanTOVarPtr->u16RxErrCnt=0;
            }
            else
            {
                ;
            }
        }
        else if(u8TempErrNum==2)
        {
            CanTOVarPtr->u16RxErrCnt=0;
        }
        else
        {
            ;
        }
    }
}

static void CC_vClearInvalidSignal(CC_SignalChk_t *ToChkSignal, uint8 u8TempErrNum)
{
    if((cu8ClrFltVarblCh==FLEXCAN_MODULE_ALL)||(cu8ClrFltVarblCh==ToChkSignal->u2NetChannel))
    {
       ToChkSignal->u8OkCnt       = 0;
       ToChkSignal->u16ErrorCnt   = 0;
       ToChkSignal->u1RefSigErrDet= 0;

       if(u8TempErrNum==1)
       {
           ToChkSignal->u1ErrorFlg    = 0;
           ToChkSignal->u1OkFlg       = 0;
       }
       else
       {
           ;
       }
   }
}

void Proxy_vChkCan(void)
{
  static uint16 acu16RarCnt;
  uint8 ac_u8ChkInhbt = 1;

  if(Proxy_TxBus.Proxy_TxIgnOnOffSts==0)
  {
    ccu1IgnOnTimeOkFlg = 0;
    acu16RarCnt = 0;
  }
  else
  {
    if(acu16RarCnt>=U8_CC_TIME_1SEC)
    {
      ccu1IgnOnTimeOkFlg=1;
      ac_u8ChkInhbt = 0;
    }
    else
    {
      acu16RarCnt++;
      ac_u8ChkInhbt = 1;
    }
  }

    if(ac_u8ChkInhbt>0)
    {
        ccu1CanChkInhbtFlg=1;
    }
    else
    {
        ccu1CanChkInhbtFlg=0;
    }

    if(ccu1CanChkInhbtFlg==0)
    {
      Proxy_DetCanError();
    }
    else
    {
      ;
    }

    Proxy_vChkSysVariant();
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

static void Proxy_RxBms1(void)
{
    uint8 ComSignalData;
        
    if(RxMsg_ToChk[CHK_CAN_BMS1_MSG_RX].u1RxCopyOkFlg==1 && Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Bms1MsgOkFlg==1)
    {
        Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Bms1MsgOkFlg=0;
        RxMsg_ToChk[CHK_CAN_BMS1_MSG_RX].u1RxCopyOkFlg=0;

        /* Initial value: 00H                       */
        /* Error identifier: -                      */
        /* Physical range: 0 ~ 100 [%] = 00H ~ C8H  */
        /* Conversion: (PH) = 0.5 * (HEX) [%]       */

        //Com_ReceiveSignal(COM_SIG_RX_BMS1_SOC_PC,&ComSignalData);
        Proxy_cu16CR_Bms_Soc_Pc_ConvX10   = (uint16)Proxy_RxBus.Proxy_RxRxBms1Info.Bms1SocPc*5;
    }
}

static void Proxy_RxYawSerial(void)
{
  uint8 ComSignalData1,ComSignalData2,ComSignalData3;
  if(RxMsg_ToChk[CHK_CAN_YAWSERIAL_MSG_RX].u1RxCopyOkFlg==1 && Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.YawSerialMsgOkFlg==1)
  {
    //Com_ReceiveSignal(COM_SIG_RX_YAWSERIAL_YAWSERIALNUM_0,&ComSignalData1);
    //Com_ReceiveSignal(COM_SIG_RX_YAWSERIAL_YAWSERIALNUM_1,&ComSignalData2);
    //Com_ReceiveSignal(COM_SIG_RX_YAWSERIAL_YAWSERIALNUM_2,&ComSignalData3);
    Proxy_fcu24SenSerialNumber = (uint32)((((uint32)Proxy_RxBus.Proxy_RxRxYawSerialInfo.YawSerialNum_0<<16)|\
                                             ((uint32)Proxy_RxBus.Proxy_RxRxYawSerialInfo.YawSerialNum_1<<8))|\
                                             (uint32)Proxy_RxBus.Proxy_RxRxYawSerialInfo.YawSerialNum_2);
    Proxy_ccu1GetImuSerialNum=1;

    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.YawSerialMsgOkFlg=0;
    RxMsg_ToChk[CHK_CAN_YAWSERIAL_MSG_RX].u1RxCopyOkFlg=0;
  }
}

static void Proxy_RxYawAcc(void)
{
  uint8 ComSignalData;
  uint8 ComSignalData1,ComSignalData2;
  static uint8 ccu8_RxTimeout1=0;
  static uint8 ccu8_RxTimeout2=0;
  ccu1ImuYawRXFault=CC_vCanMsgRxChk(&ccIMU_YAW, U8_CC_TIME_10MS);
  
  if(RxMsg_ToChk[CHK_CAN_YAWACC_MSG_RX].u1RxCopyOkFlg==1 && Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.YawAccMsgOkFlg==1)
  {
    ccu8_RxTimeout1      =0;
    Proxy_cu1_YAW_RxMissing_Flg=0;
    Proxy_cu1_AY_RxMissing_Flg =0;

    /* Yaw & Lg Sensor*/
    //Com_ReceiveSignal(COM_SIG_RX_YAWACC_YAWRATEVAILDDATA,&ComSignalData);
    Proxy_RxBus.Proxy_RxCanRxEemInfo.YawRateInvld     = Proxy_RxBus.Proxy_RxRxYawAccInfo.YawRateValidData;

    //Com_ReceiveSignal(COM_SIG_RX_YAWACC_YAWRATESELFTESTSTATUS,&ComSignalData);
    Proxy_fcu1SenYawCbitState    = Proxy_RxBus.Proxy_RxRxYawAccInfo.YawRateSelfTestStatus;

    //Com_ReceiveSignal(COM_SIG_RX_YAWACC_YAWRATESIGNAL_0,&ComSignalData1);
    //Com_ReceiveSignal(COM_SIG_RX_YAWACC_YAWRATESIGNAL_1,&ComSignalData2);
    Proxy_RxBus.Proxy_RxCanRxEscInfo.YawRate      = (sint16)(((uint16)Proxy_RxBus.Proxy_RxRxYawAccInfo.YawRateSignal_0<<4) | \
                                                                Proxy_RxBus.Proxy_RxRxYawAccInfo.YawRateSignal_1);

    //Com_ReceiveSignal(COM_SIG_RX_YAWACC_SENSOROSCFREQDEV,&ComSignalData);
    Proxy_fcs8SenRasterFreqency  = Proxy_RxBus.Proxy_RxRxYawAccInfo.SensorOscFreqDev;

    //Com_ReceiveSignal(COM_SIG_RX_YAWACC_GYRO_FAIL,&ComSignalData);
    Proxy_fcu1SenFailGyro        = Proxy_RxBus.Proxy_RxRxYawAccInfo.Gyro_Fail;

    //Com_ReceiveSignal(COM_SIG_RX_YAWACC_RASTER_FAIL,&ComSignalData);
    Proxy_fcu1SenFailRaster      = Proxy_RxBus.Proxy_RxRxYawAccInfo.Raster_Fail;

    //Com_ReceiveSignal(COM_SIG_RX_YAWACC_EEP_FAIL,&ComSignalData);
    Proxy_fcu1SenFailEEPROM      =Proxy_RxBus.Proxy_RxRxYawAccInfo.Eep_Fail;

    //Com_ReceiveSignal(COM_SIG_RX_YAWACC_BATT_RANGE_ERR,&ComSignalData);
    Proxy_fcu1SenFailPowerRange  =Proxy_RxBus.Proxy_RxRxYawAccInfo.Batt_Range_Err;

    //Com_ReceiveSignal(COM_SIG_RX_YAWACC_ASIC_FAIL,&ComSignalData);
    Proxy_fcu1SenFailASIC        =Proxy_RxBus.Proxy_RxRxYawAccInfo.Asic_Fail;

    //Com_ReceiveSignal(COM_SIG_RX_YAWACC_ACCEL_FAIL,&ComSignalData);
    Proxy_fcu1SenFailAccel       =Proxy_RxBus.Proxy_RxRxYawAccInfo.Accel_Fail;

    //Com_ReceiveSignal(COM_SIG_RX_YAWACC_RAM_FAIL,&ComSignalData);
    Proxy_fcu1SenFailRAM         =Proxy_RxBus.Proxy_RxRxYawAccInfo.Ram_Fail;

    //Com_ReceiveSignal(COM_SIG_RX_YAWACC_ROM_FAIL,&ComSignalData);
    Proxy_fcu1SenFailROM         =Proxy_RxBus.Proxy_RxRxYawAccInfo.Rom_Fail;

    //Com_ReceiveSignal(COM_SIG_RX_YAWACC_AD_FAIL,&ComSignalData);
    Proxy_fcu1SenFailAD          =Proxy_RxBus.Proxy_RxRxYawAccInfo.Ad_Fail;

    //Com_ReceiveSignal(COM_SIG_RX_YAWACC_OSC_FAIL,&ComSignalData);
    Proxy_fcu1SenFailOscilator   =Proxy_RxBus.Proxy_RxRxYawAccInfo.Osc_Fail;

    //Com_ReceiveSignal(COM_SIG_RX_YAWACC_WATCHDOG_RST,&ComSignalData);
    Proxy_fcu1SenFailWatchDog    =Proxy_RxBus.Proxy_RxRxYawAccInfo.Watchdog_Rst;

    //Com_ReceiveSignal(COM_SIG_RX_YAWACC_PLAUS_ERR_PST,&ComSignalData);
    Proxy_fcu1SenFailPlausibility=Proxy_RxBus.Proxy_RxRxYawAccInfo.Plaus_Err_Pst;

    //Com_ReceiveSignal(COM_SIG_RX_YAWACC_ROLLINGCOUNTER,&ComSignalData);
    Proxy_fcu4SenRollingCount    =Proxy_RxBus.Proxy_RxRxYawAccInfo.RollingCounter;

    //Com_ReceiveSignal(COM_SIG_RX_YAWACC_CAN_FUNC_ERR,&ComSignalData);
    Proxy_fcu1SenFailCanFunctionality=Proxy_RxBus.Proxy_RxRxYawAccInfo.Can_Func_Err;

    //Com_ReceiveSignal(COM_SIG_RX_YAWACC_ACCELERATORRATESIG_0,&ComSignalData1);
    //Com_ReceiveSignal(COM_SIG_RX_YAWACC_ACCELERATORRATESIG_1,&ComSignalData2);
    Proxy_RxBus.Proxy_RxCanRxEscInfo.Ay     =(sint16)(((uint16)Proxy_RxBus.Proxy_RxRxYawAccInfo.AccelEratorRateSig_0<<8) |\
                                                        Proxy_RxBus.Proxy_RxRxYawAccInfo.AccelEratorRateSig_1);

    //Com_ReceiveSignal(COM_SIG_RX_YAWACC_LATACCVALIDDATA,&ComSignalData);
    Proxy_RxBus.Proxy_RxCanRxEemInfo.AyInvld    =Proxy_RxBus.Proxy_RxRxYawAccInfo.LatAccValidData;

    //Com_ReceiveSignal(COM_SIG_RX_YAWACC_LATACCSELFTESTSTATUS,&ComSignalData);
    Proxy_fcu1SenAYCbitState   =Proxy_RxBus.Proxy_RxRxYawAccInfo.LatAccSelfTestStatus;

    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.YawAccMsgOkFlg=0;
    RxMsg_ToChk[CHK_CAN_YAWACC_MSG_RX].u1RxCopyOkFlg=0;
    Proxy_fu1YawMsgCopyOk=1;
  }
  else
  {
    if(ccu8_RxTimeout1==U8_CC_TIME_500MS_5msLoop)
    {
      Proxy_cu1_YAW_RxMissing_Flg=1;
      Proxy_cu1_AY_RxMissing_Flg =1;
    }
    else
    {
      ccu8_RxTimeout1=ccu8_RxTimeout1+1;
    }
  }
}

static void Proxy_RxTcu6(void)
{
  uint8 ComSignalData;

  CC_vCanReceiveChk(&TCU6_RxChk, (uint8)RxMsg_ToChk[CHK_CAN_TCU6_MSG_RX].u1RxCopyOkFlg, U8_CC_TIME_500MS_5msLoop);

  if(TCU6_RxChk.u1RxMissingFlg==1)
  {
    Proxy_cu1Tcu6RxOk_Flg=0;
  }
  else
  {
    Proxy_cu1Tcu6RxOk_Flg=1;
  }

  if(RxMsg_ToChk[CHK_CAN_TCU6_MSG_RX].u1RxCopyOkFlg==1 && Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Tcu6MsgOkFlg==1)
  {
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Tcu6MsgOkFlg = 0;
    RxMsg_ToChk[CHK_CAN_TCU6_MSG_RX].u1RxCopyOkFlg=0;
    //Com_ReceiveSignal(COM_SIG_RX_TCU6_SHIFTCLASS_CCAN,&ComSignalData);
    Proxy_cu8CF_Tcu_ShiftClass = Proxy_RxBus.Proxy_RxRxTcu6Info.ShiftClass_Ccan;
  }    
}

static void Proxy_RxTcu5(void)
{
  uint8 ComSignalData;

  CC_vCanReceiveChk(&TCU5_RxChk, (uint8)RxMsg_ToChk[CHK_CAN_TCU5_MSG_RX].u1RxCopyOkFlg, U8_CC_TIME_500MS_5msLoop);

  if(RxMsg_ToChk[CHK_CAN_TCU5_MSG_RX].u1RxCopyOkFlg==1 && Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Tcu5MsgOkFlg==1)
  {
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Tcu5MsgOkFlg = 0;
    RxMsg_ToChk[CHK_CAN_TCU5_MSG_RX].u1RxCopyOkFlg=0;

    Proxy_RxBus.Proxy_RxCanRxEscInfo.AtType    = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.CvtType   = 0;
    Proxy_lespu1EMS_HEV_AT_TCU= 0;

    //Com_ReceiveSignal(COM_SIG_RX_TCU5_TYP,&ComSignalData);
    if (Proxy_RxBus.Proxy_RxRxTcu5Info.Typ==0x01)
    {
      Proxy_RxBus.Proxy_RxCanRxEscInfo.AtType = 1;
      Proxy_fcu6TMMessage    = 0x00;
    }
    else if(Proxy_RxBus.Proxy_RxRxTcu5Info.Typ==0x02)
    {
      Proxy_RxBus.Proxy_RxCanRxEscInfo.CvtType = 1;
      Proxy_fcu6TMMessage     = 0x0A;
    }
    else if(Proxy_RxBus.Proxy_RxRxTcu5Info.Typ==0x03)
    {
      Proxy_lespu1EMS_HEV_AT_TCU = 1;
      Proxy_fcu6TMMessage     = 0x00;
    }
    else
    {
      ;
    }

    //Com_ReceiveSignal(COM_SIG_RX_TCU5_GEARTYP,&ComSignalData);
    Proxy_RxBus.Proxy_RxCanRxEscInfo.GearType = Proxy_RxBus.Proxy_RxRxTcu5Info.GearTyp;
  }    
}

static void Proxy_RxTcu1(void)
{
  uint8 ComSignalData;
  uint16 ComSignalData1;
  
  CC_vCanReceiveChk(&TCU1_RxChk, (uint8)RxMsg_ToChk[CHK_CAN_TCU1_MSG_RX].u1RxCopyOkFlg, U8_CC_TIME_500MS_5msLoop);
        
  if(TCU1_RxChk.u1RxMissingFlg==1)
  {
     Proxy_RxBus.Proxy_RxCanRxGearSelDispErrInfo=1;
  }

  if(RxMsg_ToChk[CHK_CAN_TCU1_MSG_RX].u1RxCopyOkFlg==1 && Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Tcu1MsgOkFlg==1)
  {
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Tcu1MsgOkFlg=0;
    RxMsg_ToChk[CHK_CAN_TCU1_MSG_RX].u1RxCopyOkFlg=0;

    //Com_ReceiveSignal(COM_SIG_RX_TCU1_TARGE,&ComSignalData);
    Proxy_RxBus.Proxy_RxCanRxIdbInfo.TarGearPosi    = (uint8)Proxy_RxBus.Proxy_RxRxTcu1Info.Targe;

    //Com_ReceiveSignal(COM_SIG_RX_TCU1_GARCHANGE,&ComSignalData);
    Proxy_lespu1TCU_SWI_GS = Proxy_RxBus.Proxy_RxRxTcu1Info.GarChange;

    //Com_ReceiveSignal(COM_SIG_RX_TCU1_FLT,&ComSignalData);
    /* TCU1 mes TCU Fault Signal Error Check */
    if(((Proxy_RxBus.Proxy_RxRxTcu1Info.Flt&0x01)==0x01)||((Proxy_RxBus.Proxy_RxRxTcu1Info.Flt&0x02)==0x02))
    {
      Proxy_RxBus.Proxy_RxCanRxEscInfo.TcuFaultSts=1;
    }
    else
    {
      Proxy_RxBus.Proxy_RxCanRxEscInfo.TcuFaultSts=0;
    }

    if(((Proxy_RxBus.Proxy_RxRxTcu1Info.Flt&0x01)==0x01)||((Proxy_RxBus.Proxy_RxRxTcu1Info.Flt&0x02)==0x02))
    {
      SCC_FTcu_InvChk.u1RefSigErrDet=1;
    }
    else
    {
      SCC_FTcu_InvChk.u1RefSigErrDet=0;
    }
    CC_vChkCanMsgSignal(&SCC_FTcu_InvChk, RxMsg_ToChk[CHK_CAN_TCU1_MSG_RX].u1RxErrFlg, __U8_SCC_TCU1MSGINVALID_THRES);

    //Com_ReceiveSignal(COM_SIG_RX_TCU1_GARSELDISP,&ComSignalData);
    Proxy_RxBus.Proxy_RxCanRxIdbInfo.GearSelDisp = Proxy_lespu8TCU_G_SEL_DISP= (uint8)Proxy_RxBus.Proxy_RxRxTcu1Info.GarSelDisp;
    Proxy_fcu8GearPosition    = (uint8)Proxy_RxBus.Proxy_RxRxTcu1Info.GarSelDisp;

    Proxy_RxBus.Proxy_RxCanRxGearSelDispErrInfo = 0;

    if(Proxy_lespu8TCU_G_SEL_DISP == 0x0F)
    {
      Proxy_RxBus.Proxy_RxCanRxGearSelDispErrInfo = 1;
    }

    if(Proxy_lespu8TCU_G_SEL_DISP == 0x0F)
    {
      G_SEL_DISP_InvChk.u1RefSigErrDet=1;
    }
    else
    {
      G_SEL_DISP_InvChk.u1RefSigErrDet=0;
    }
    CC_vChkCanMsgSignal(&G_SEL_DISP_InvChk, RxMsg_ToChk[CHK_CAN_TCU1_MSG_RX].u1RxErrFlg, __U8_SCC_TCU1MSGINVALID_THRES);

    //Com_ReceiveSignal(COM_SIG_RX_TCU1_TQREDREQ_PC,&ComSignalData1);
    Proxy_RxBus.Proxy_RxCanRxEscInfo.TqIntvTCU   = (sint16)((sint16)Proxy_RxBus.Proxy_RxRxTcu1Info.TQRedReq_PC*39/10);

    //Com_ReceiveSignal(COM_SIG_RX_TCU1_TQREDREQSLW_PC,&ComSignalData1);
    Proxy_RxBus.Proxy_RxCanRxEscInfo.TqIntvSlowTCU= (sint16)((sint16)Proxy_RxBus.Proxy_RxRxTcu1Info.TQRedReqSlw_PC*39/10);

    //Com_ReceiveSignal(COM_SIG_RX_TCU1_TQINCREQ_PC,&ComSignalData);
    Proxy_RxBus.Proxy_RxCanRxEscInfo.TqIncReq   = (sint16)((sint16)Proxy_RxBus.Proxy_RxRxTcu1Info.TQIncReq_PC*39/10);
  }    
}

static void Proxy_RxSas(void)
{
  uint8 ComSignalData;
  uint16 ComSignalData1;

  if(RxMsg_ToChk[CHK_CAN_SAS1_MSG_RX].u1RxCopyOkFlg==1 && Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.SasMsgOkFlg==1)
  {
    /* SAS Sensor */
    //Com_ReceiveSignal(COM_SIG_RX_SAS1_ANGLE,&ComSignalData1);
    Proxy_RxBus.Proxy_RxCanRxEscInfo.SteeringAngle   = (sint16)Proxy_RxBus.Proxy_RxRxSasInfo.Angle;

    //Com_ReceiveSignal(COM_SIG_RX_SAS1_SPEED,&ComSignalData);
    Proxy_fcu8LWSSpeed    = Proxy_RxBus.Proxy_RxRxSasInfo.Speed;

    //Com_ReceiveSignal(COM_SIG_RX_SAS1_OK,&ComSignalData);
    Proxy_RxBus.Proxy_RxCanRxEemInfo.SteeringAngleRxOk       = Proxy_RxBus.Proxy_RxRxSasInfo.Ok;

    //Com_ReceiveSignal(COM_SIG_RX_SAS1_CAL,&ComSignalData);
    Proxy_fcu1LWSCal      = Proxy_RxBus.Proxy_RxRxSasInfo.Cal;

    //Com_ReceiveSignal(COM_SIG_RX_SAS1_TRIM,&ComSignalData);
    Proxy_fcu1LWSTrim     = Proxy_RxBus.Proxy_RxRxSasInfo.Trim;

    //Com_ReceiveSignal(COM_SIG_RX_SAS1_CHECKSUM,&ComSignalData);
    Proxy_fcu4LWSCheckSum = Proxy_RxBus.Proxy_RxRxSasInfo.CheckSum;

    //Com_ReceiveSignal(COM_SIG_RX_SAS1_MSGCOUNT,&ComSignalData);
    Proxy_fcu4LWSMsgCount = Proxy_RxBus.Proxy_RxRxSasInfo.MsgCount;

    Proxy_fu1SteerMsgCopyOk=1;
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.SasMsgOkFlg=0;
    RxMsg_ToChk[CHK_CAN_SAS1_MSG_RX].u1RxCopyOkFlg=0;
    Proxy_stg_sensor_ok_flg=1;
  }    
}

static void Proxy_RxMcu2(void)
{
  uint8 ComSignalData;
        
  if(RxMsg_ToChk[CHK_CAN_MCU2_MSG_RX].u1RxCopyOkFlg==1 && Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Mcu2MsgOkFlg==1)
  {
     Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Mcu2MsgOkFlg=0;
     RxMsg_ToChk[CHK_CAN_MCU2_MSG_RX].u1RxCopyOkFlg=0;
     //Com_ReceiveSignal(COM_SIG_RX_MCU2_FLT,&ComSignalData);
     Proxy_cu1CF_Mcu_Flt = Proxy_RxBus.Proxy_RxRxMcu2Info.Flt; /* Not used */
  }
}

static void Proxy_RxMcu1(void)
{
  uint16 ComSignalData;

  if(RxMsg_ToChk[CHK_CAN_MCU1_MSG_RX].u1RxCopyOkFlg==1 && Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Mcu1MsgOkFlg==1)
  {
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Mcu1MsgOkFlg=0;
    RxMsg_ToChk[CHK_CAN_MCU1_MSG_RX].u1RxCopyOkFlg=0;
    
    /* Error identifier: -                   */
    /* Physical range: -100~ 99.80 [%] = 200H ~ 1ffH    */
    /* Conversion: If MSB of high byte is 1,            */
    /* (PH) = 0.1953125 * (HEX) -200 [%]                */
    /* --> 변경 (0.1953125 -> 0.195)  연산시 값이 넘어감*/
    /* Else (PH) = 0.1953125 * (HEX) [%]                */
    //Com_ReceiveSignal(COM_SIG_RX_MCU1_MOTESTTQ_PC,&ComSignalData);
    if( ( Proxy_RxBus.Proxy_RxRxMcu1Info.MoTestTQ_PC & u16g_MOTTQCMD_SIGNBIT_MASK ) == u16g_MOTTQCMD_SIGNBIT_MASK )
    {
        Proxy_RxBus.Proxy_RxCanRxEscInfo.MotEstTq =\
        (sint16)((((uint32)Proxy_RxBus.Proxy_RxRxMcu1Info.MoTestTQ_PC)*19531)/10000)-2000;
    }
    else
    {
        Proxy_RxBus.Proxy_RxCanRxEscInfo.MotEstTq =\
        (sint16)((((uint32)Proxy_RxBus.Proxy_RxRxMcu1Info.MoTestTQ_PC)*19531)/10000);
    }
    
    /* Initial value: 0000H                                     */
    /* Range: -32,767 ~ 32,767Nm = 8000H ~ 7FFFH                */
    /* If MSB of high byte is 1, PH = HEX - 65535               */
    /* else PH = HEX                                                                    */
    //Com_ReceiveSignal(COM_SIG_RX_MCU1_MOTACTROTSPD_RPM,&ComSignalData);
    if( ( Proxy_RxBus.Proxy_RxRxMcu1Info.MotActRotSpd_RPM & u16g_SIGNBIT_MASK ) == u16g_SIGNBIT_MASK )
    {
      Proxy_cs32CR_Mot_ActRotSpd_rpm_Conv =\
       (signed int)Proxy_RxBus.Proxy_RxRxMcu1Info.MotActRotSpd_RPM - 65535;
    }
    else
    {
      Proxy_cs32CR_Mot_ActRotSpd_rpm_Conv = \
      (signed int)Proxy_RxBus.Proxy_RxRxMcu1Info.MotActRotSpd_RPM;
    }
  }    
}

static void Proxy_RxLongAcc(void)
{
  uint8 ComSignalData;
  uint8 ComSignalData1,ComSignalData2;

  static uint8 ccu8_RxTimeout2=0;

  if(RxMsg_ToChk[CHK_CAN_LONGACC_MSG_RX].u1RxCopyOkFlg==1 && Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.LongAccMsgOkFlg==1)
  {
    ccu8_RxTimeout2     =0; /* 500ms Check */
    Proxy_cu1_AX_RxMissing_Flg=0;
    //Com_ReceiveSignal(COM_SIG_RX_LONGACC_INTSENFLTSYMTMACTIVE,&ComSignalData);
    Proxy_fcu1AxFaultActive       = Proxy_RxBus.Proxy_RxRxLongAccInfo.IntSenFltSymtmActive;

    //Com_ReceiveSignal(COM_SIG_RX_LONGACC_INTSENFAULTPRESENT,&ComSignalData);
    Proxy_fcu1AxFaultPresent      = Proxy_RxBus.Proxy_RxRxLongAccInfo.IntSenFaultPresent;

    //Com_ReceiveSignal(COM_SIG_RX_LONGACC_LONGACCSENCIRERRPRE,&ComSignalData);
    Proxy_fcu1AxCircuitErrPresent = Proxy_RxBus.Proxy_RxRxLongAccInfo.LongAccSenCirErrPre;

    //Com_ReceiveSignal(COM_SIG_RX_LONGACC_LONACSENRANCHKERRPRE,&ComSignalData);
    Proxy_fcu1AxRangeErrorPresent = Proxy_RxBus.Proxy_RxRxLongAccInfo.LonACSenRanChkErrPre;

    //Com_ReceiveSignal(COM_SIG_RX_LONGACC_LONGROLLINGCOUNTER,&ComSignalData);
    Proxy_fcu4AxRollingCounter    = Proxy_RxBus.Proxy_RxRxLongAccInfo.LongRollingCounter;

    //Com_ReceiveSignal(COM_SIG_RX_LONGACC_INTTEMPSENSORFAULT,&ComSignalData);
    Proxy_fcu1AxInternalTempFault = Proxy_RxBus.Proxy_RxRxLongAccInfo.IntTempSensorFault;

    //Com_ReceiveSignal(COM_SIG_RX_LONGACC_LONGACCINVALIDDATA,&ComSignalData);
    Proxy_RxBus.Proxy_RxCanRxEemInfo.AxInvldData         =     Proxy_RxBus.Proxy_RxRxLongAccInfo.LongAccInvalidData;

    //Com_ReceiveSignal(COM_SIG_RX_LONGACC_LONGACCSELFTSTSTATUS,&ComSignalData);
    Proxy_fcu1AxCBITStatus          =     Proxy_RxBus.Proxy_RxRxLongAccInfo.LongAccSelfTstStatus;

    //Com_ReceiveSignal(COM_SIG_RX_LONGACC_LONGACCRATESIGNAL_0,&ComSignalData1);
    //Com_ReceiveSignal(COM_SIG_RX_LONGACC_LONGACCRATESIGNAL_1,&ComSignalData2);
    Proxy_RxBus.Proxy_RxCanRxEscInfo.Ax         =     (sint16)(((uint16)Proxy_RxBus.Proxy_RxRxLongAccInfo.LongAccRateSignal_0<<8) |\
                                                                           Proxy_RxBus.Proxy_RxRxLongAccInfo.LongAccRateSignal_1);

    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.LongAccMsgOkFlg=0;
    RxMsg_ToChk[CHK_CAN_LONGACC_MSG_RX].u1RxCopyOkFlg=0;
    Proxy_fu1YawLongMsgCopyOk=1;
  }
  else
  {
    if(ccu8_RxTimeout2==U8_CC_TIME_500MS_5msLoop)
    {
      Proxy_cu1_AX_RxMissing_Flg=1;
    }
    else
    {
      ccu8_RxTimeout2=ccu8_RxTimeout2+1;
    }
  }
}

static void Proxy_RxHcu5(void)
{
  uint8 ComSignalData;
  if(RxMsg_ToChk[CHK_CAN_HCU5_MSG_RX].u1RxCopyOkFlg==1 && Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Hcu5MsgOkFlg==1)
  {
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Hcu5MsgOkFlg=0;
    RxMsg_ToChk[CHK_CAN_HCU5_MSG_RX].u1RxCopyOkFlg=0;
    /* 0 None, 1 Vehicle Stop 2 EV Propulsion       */
    /* 3 Power Assist 4 Engine Only Propulsion      */
    /* 5 Engine Generation 6 Regeneration           */
    /* 7 Engine Brake 8 Power Researve              */
    /* 9 Engine Generation/Motor Drive              */
    //Com_ReceiveSignal(COM_SIG_RX_HCU5_HEVMOD,&ComSignalData);
    Proxy_cu8CR_Hcu_HevMod = Proxy_RxBus.Proxy_RxRxHcu5Info.HevMod;
  }    
}

static void Proxy_RxHcu3(void)
{
  uint16 ComSignalData;
        
  if(RxMsg_ToChk[CHK_CAN_HCU3_MSG_RX].u1RxCopyOkFlg==1 && Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Hcu3MsgOkFlg==1)
  {
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Hcu3MsgOkFlg=0;
    RxMsg_ToChk[CHK_CAN_HCU3_MSG_RX].u1RxCopyOkFlg=0;
      
    /* Initial value: 00H                               */
    /* Physical range: -200~ 199.80 [%] = 400H ~ 3ffH   */
    /* Conversion: If MSB of high byte is 1,            */
    /* (PH) = 0.1953125 * (HEX) -200 [%]                */
    /* --> 변경 (0.1953125 -> 0.195)  연산시 값이 넘어감*/
    /* Else (PH) = 0.1953125 * (HEX) [%]                */
    //Com_ReceiveSignal(COM_SIG_RX_HCU3_TMINTQCMDBINV_PC,&ComSignalData);
    if( ( Proxy_RxBus.Proxy_RxRxHcu3Info.TmIntQcMDBINV_PC & u16g_MOTESTTQ_SIGNBIT_MASK ) == u16g_MOTESTTQ_SIGNBIT_MASK )
    {
        Proxy_cs32CR_Hcu_TMInTqCmdBInv_ConvXK = \
        (signed int)(((signed int)Proxy_RxBus.Proxy_RxRxHcu3Info.TmIntQcMDBINV_PC * 195 )  - 400000);
    }
    else
    {
        Proxy_cs32CR_Hcu_TMInTqCmdBInv_ConvXK = \
        (signed int)Proxy_RxBus.Proxy_RxRxHcu3Info.TmIntQcMDBINV_PC * 195;
    }
    /* Initial value: 00H                               */
    /* Physical range: -100~ 99.80 [%] = 200H ~ 1ffH    */
    /* Conversion: If MSB of high byte is 1,            */
    /* (PH) = 0.1953125 * (HEX) -200 [%]                */
    /* --> 변경 (0.1953125 -> 0.195)  연산시 값이 넘어감*/
    /* Else (PH) = 0.1953125 * (HEX) [%]                */
    //Com_ReceiveSignal(COM_SIG_RX_HCU3_MOTTQCMC_PC,&ComSignalData);
    if( ( Proxy_RxBus.Proxy_RxRxHcu3Info.MotTQCMC_PC & u16g_MOTTQCMD_SIGNBIT_MASK ) == u16g_MOTTQCMD_SIGNBIT_MASK )
    {
        Proxy_cs32CR_Hcu_MotTqCmd_pc_ConvXK = \
        (signed int)(((signed int)Proxy_RxBus.Proxy_RxRxHcu3Info.MotTQCMC_PC * 195)   - 200000);
    }
    else
    {
        Proxy_cs32CR_Hcu_MotTqCmd_pc_ConvXK = \
        (signed int)Proxy_RxBus.Proxy_RxRxHcu3Info.MotTQCMC_PC * 195;
    }

    /* Initial value: 00H                               */
    /* Physical range: -100~ 99.80 [%] = 200H ~ 1ffH    */
    /* Conversion: If MSB of high byte is 1,            */
    /* (PH) = 0.1953125 * (HEX) -200 [%]                */
    /* --> 변경 (0.1953125 -> 0.195)  연산시 값이 넘어감*/
    /* Else (PH) = 0.1953125 * (HEX) [%]                */
    //Com_ReceiveSignal(COM_SIG_RX_HCU3_MOTTQCMDBINV_PC,&ComSignalData);
    if( ( Proxy_RxBus.Proxy_RxRxHcu3Info.MotTQCMDBINV_PC & u16g_MOTTQCMD_SIGNBIT_MASK ) == u16g_MOTTQCMD_SIGNBIT_MASK )
    {
        Proxy_RxBus.Proxy_RxCanRxEscInfo.MotTqCmdBeforeIntv = \
        (sint16)((((uint32)Proxy_RxBus.Proxy_RxRxHcu3Info.MotTQCMDBINV_PC)*19531)/10000)-2000;
    }
    else
    {
        Proxy_RxBus.Proxy_RxCanRxEscInfo.MotTqCmdBeforeIntv = \
        (sint16)((((uint32)Proxy_RxBus.Proxy_RxRxHcu3Info.MotTQCMDBINV_PC)*19531)/10000);
    }
  }    
}

static void Proxy_RxHcu2(void)
{
  uint8 ComSignalData;
  uint16 ComSignalData1;
  if(RxMsg_ToChk[CHK_CAN_HCU2_MSG_RX].u1RxCopyOkFlg==1 && Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Hcu2MsgOkFlg==1)
  { 
    RxMsg_ToChk[CHK_CAN_HCU2_MSG_RX].u1RxCopyOkFlg=0;
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Hcu2MsgOkFlg=0;
    /* Service Test Mode                                                                */
    /* Normal Mode                              0                                               */
    /* 2WD Test Mode                    1                                               */
    //Com_ReceiveSignal(COM_SIG_RX_HCU2_SERIVCEMOD,&ComSignalData);
    Proxy_RxBus.Proxy_RxCanRxIdbInfo.HcuServiceMod = (uint8)Proxy_RxBus.Proxy_RxRxHcu2Info.ServiceMod;

    /* CF_Hcu_RegenEna indicates regeneration mode              */
    /* Regen Brake Impossible   0                                               */
    /* Regen Brake Possible             1                                               */
    //Com_ReceiveSignal(COM_SIG_RX_HCU2_REGENENA,&ComSignalData);
    Proxy_RxBus.Proxy_RxCanRxRegenInfo.HcuRegenEna = (uint8)Proxy_RxBus.Proxy_RxRxHcu2Info.RegenENA;

    /* Regen Brake Torque                                                               */
    /* Init Value : 0x0000                                                              */
    /* Physical range : 0...65535Nm = 0x0000...0xFFFF   */
    //Com_ReceiveSignal(COM_SIG_RX_HCU2_REGENBRKTQ_NM,&ComSignalData1);
    Proxy_RxBus.Proxy_RxCanRxRegenInfo.HcuRegenBrkTq = Proxy_RxBus.Proxy_RxRxHcu2Info.RegenBRKTQ_NM;

    /* Initial value: 0000H                                     */
    /* Range: -32,767 ~ 32,767Nm = 8000H ~ 7FFFH                */
    /* If MSB of high byte is 1, PH = HEX - 65535               */
    /* else PH = HEX */
    //Com_ReceiveSignal(COM_SIG_RX_HCU2_CRPTQ_NM,&ComSignalData1);
    if( ( Proxy_RxBus.Proxy_RxRxHcu2Info.CrpTQ_NM & u16g_SIGNBIT_MASK ) == u16g_SIGNBIT_MASK )
    {
        Proxy_cs32CR_Hcu_CrpTq_Nm_Conv = (signed int)Proxy_RxBus.Proxy_RxRxHcu2Info.CrpTQ_NM - 65535;
    }
    else
    {
        Proxy_cs32CR_Hcu_CrpTq_Nm_Conv = (signed int)Proxy_RxBus.Proxy_RxRxHcu2Info.CrpTQ_NM;
    }

    /* Initial value: 0000H                                     */
    /* Range: -32,767 ~ 32,767Nm = 8000H ~ 7FFFH                */
    /* If MSB of high byte is 1, PH = HEX - 65535               */
    /* else PH = HEX                                                                    */
    //Com_ReceiveSignal(COM_SIG_RX_HCU2_WHLDEMTQ_NM,&ComSignalData1);
    if( ( Proxy_RxBus.Proxy_RxRxHcu2Info.WhlDEMTQ_NM & u16g_SIGNBIT_MASK ) == u16g_SIGNBIT_MASK )
    {
        Proxy_cs32CR_Hcu_WhlDemTq_Nm_Conv = (signed int)Proxy_RxBus.Proxy_RxRxHcu2Info.WhlDEMTQ_NM - 65535;
    }
    else
    {
        Proxy_cs32CR_Hcu_WhlDemTq_Nm_Conv = (signed int)Proxy_RxBus.Proxy_RxRxHcu2Info.WhlDEMTQ_NM;
    }
  }    
}

static void Proxy_RxHcu1(void)
{
  uint8 ComSignalData;
  if(RxMsg_ToChk[CHK_CAN_HCU1_MSG_RX].u1RxCopyOkFlg==1 && Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Hcu1MsgOkFlg==1)
  {
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Hcu1MsgOkFlg=0;
    RxMsg_ToChk[CHK_CAN_HCU1_MSG_RX].u1RxCopyOkFlg=0;

    //Com_ReceiveSignal(COM_SIG_RX_HCU1_ENGCLTSTAT,&ComSignalData);
    Proxy_RxBus.Proxy_RxCanRxEscInfo.EngClutchState = Proxy_RxBus.Proxy_RxRxHcu1Info.EngCltStat;   /* 0:OPEN, 1:SLIP, 2:LOCK, 3-7: - */

    /* CF_Hcu_HevRdy Signal Rx Add - 2013.08.08 */
    //Com_ReceiveSignal(COM_SIG_RX_HCU1_HEVRDY,&ComSignalData);
    Proxy_cu1CF_Hcu_HevRdy = Proxy_RxBus.Proxy_RxRxHcu1Info.HEVRDY;   /* 0 : HEV not Ready, 1 : HEV Drivable */

    //Com_ReceiveSignal(COM_SIG_RX_HCU1_ENGTQCMDBINV_PC,&ComSignalData);
    Proxy_RxBus.Proxy_RxCanRxEscInfo.EngTqCmdBeforeIntv = \
    (((sint16)Proxy_RxBus.Proxy_RxRxHcu1Info.EngTQCmdBinV_PC)*39/10);

    /* Initial value: 00H                                       */
    /* Range: 0~99.61% = 00H~FEH                                */
    /* Conversion: = 0.3906 * (HEX) [%]                         */
    /* Calculated Maximum Value: 254*3906=996030=0xF32BE*/
    //Com_ReceiveSignal(COM_SIG_RX_HCU1_ENGTQCMD_PC,&ComSignalData);
    Proxy_cu32CR_Hcu_EngTqCmd_Pc_ConvX10K = \
    (uint32)Proxy_RxBus.Proxy_RxRxHcu1Info.EngTQCmd_PC * 3906;
  }    
}

static void Proxy_RxFatc1(void)
{
  uint8 ComSignalData;

  CC_vCanReceiveChk(&FATC1_RxChk, (uint8)RxMsg_ToChk[CHK_CAN_FATC1_MSG_RX].u1RxCopyOkFlg, U8_CC_TIME_1SEC_5msLoop);

  if(RxMsg_ToChk[CHK_CAN_FATC1_MSG_RX].u1RxCopyOkFlg==1 && Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Fatc1MsgOkFlg==1)
  {
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Fatc1MsgOkFlg = 0;
    RxMsg_ToChk[CHK_CAN_FATC1_MSG_RX].u1RxCopyOkFlg=0;

    //Com_ReceiveSignal(COM_SIG_RX_FATC1_OUTTEMP_SNR_C,&ComSignalData);
    if(Proxy_RxBus.Proxy_RxRxFact1Info.OutTemp_SNR_C==0xFF)
    {
      Proxy_RxBus.Proxy_RxCanRxEngTempInfo.EngTempErr=1;
    }
    else
    {
      Proxy_RxBus.Proxy_RxCanRxEngTempInfo.EngTempErr=0;
      Proxy_RxBus.Proxy_RxCanRxEngTempInfo.EngTemp = \
      (sint16)(((sint16)Proxy_RxBus.Proxy_RxRxFact1Info.OutTemp_SNR_C)*50 - 4000) ;
    }
  }    
}

static void Proxy_RxEms3(void)
{
  uint8 ComSignalData;

  CC_vCanReceiveChk(&EMS3_RxChk, (uint8)RxMsg_ToChk[CHK_CAN_EMS3_MSG_RX].u1RxCopyOkFlg, U8_CC_TIME_500MS_5msLoop);
    
  if(RxMsg_ToChk[CHK_CAN_EMS3_MSG_RX].u1RxCopyOkFlg==1 && Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Ems3MsgOkFlg==1)
  {
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Ems3MsgOkFlg=0;
    RxMsg_ToChk[CHK_CAN_EMS3_MSG_RX].u1RxCopyOkFlg=0;

    //Com_ReceiveSignal(COM_SIG_RX_EMS3_ENGCOLTEMP_C,&ComSignalData);
    if(((uint8)Proxy_RxBus.Proxy_RxRxEms3Info.EngColTemp_C) == 0xFF)
    {
      Proxy_RxBus.Proxy_RxCanRxIdbInfo.CoolantTempErr = 1;
    }
    else
    {
      Proxy_RxBus.Proxy_RxCanRxIdbInfo.CoolantTempErr = 0;
      Proxy_RxBus.Proxy_RxCanRxIdbInfo.CoolantTemp = \
      (sint16)(((sint16)Proxy_RxBus.Proxy_RxRxEms3Info.EngColTemp_C*3/4)-48);
    }
  }    
}

static void Proxy_RxEms2(void)
{
  uint8 ComSignalData;

  CC_vCanReceiveChk(&EMS2_RxChk, (uint8)RxMsg_ToChk[CHK_CAN_EMS2_MSG_RX].u1RxCopyOkFlg, U8_CC_TIME_500MS_5msLoop);

  if(EMS2_RxChk.u1RxMissingFlg==1)
  {
    Proxy_RxBus.Proxy_RxCanRxAccelPedlInfo.AccelPedlValErr=Proxy_fcu1PVAVError= 1;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.ThrottleAngleErr=Proxy_fcu1TPSError= 1;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.EngSpdErr=1;
  }

  if(RxMsg_ToChk[CHK_CAN_EMS2_MSG_RX].u1RxCopyOkFlg==1 && Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Ems2MsgOkFlg==1)
  {
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Ems2MsgOkFlg=0;
    RxMsg_ToChk[CHK_CAN_EMS2_MSG_RX].u1RxCopyOkFlg=0;

    /* EMS2 CF_Ems_EngSpedErr Signal Error Check */
    //Com_ReceiveSignal(COM_SIG_RX_EMS2_ENGSPDERR,&ComSignalData);
    if(Proxy_RxBus.Proxy_RxRxEms2Info.EngSpdErr == 1)
    {
      Proxy_RxBus.Proxy_RxCanRxEscInfo.EngSpdErr = 1;
    }
    else
    {
      Proxy_RxBus.Proxy_RxCanRxEscInfo.EngSpdErr = 0;
    }

    if(Proxy_RxBus.Proxy_RxRxEms2Info.EngSpdErr == 1)
    {
      SCC_Ems1EngSpd_InvChk.u1RefSigErrDet=1;
    }
    else
    {
      SCC_Ems1EngSpd_InvChk.u1RefSigErrDet=0;
    }
    CC_vChkCanMsgSignal(&SCC_Ems1EngSpd_InvChk, RxMsg_ToChk[CHK_CAN_EMS2_MSG_RX].u1RxErrFlg, __U8_SCC_EMS1EMS2MSGINVALID_THRES);

    //Com_ReceiveSignal(COM_SIG_RX_EMS2_ACCPEDDEP_PC,&ComSignalData);
    if(((uint8)Proxy_RxBus.Proxy_RxRxEms2Info.AccPedDep_PC) == 0xFF)
    {
      Proxy_RxBus.Proxy_RxCanRxAccelPedlInfo.AccelPedlValErr = 1;
      Proxy_fcu1PVAVError = 1;
    }
    else
    {
      /* AVH 관련 Resol 변수 추가 - 제어팀 요청 */
      Proxy_RxBus.Proxy_RxCanRxAccelPedlInfo.AccelPedlVal= (sint16)((sint16)Proxy_RxBus.Proxy_RxRxEms2Info.AccPedDep_PC*25/64);
      Proxy_RxBus.Proxy_RxCanRxEscInfo.PvAvCanResol1000 =(sint16)((sint16)Proxy_RxBus.Proxy_RxRxEms2Info.AccPedDep_PC*125/32);
      Proxy_fcu8PVAV = (uint8)((uint16)Proxy_RxBus.Proxy_RxRxEms2Info.AccPedDep_PC*25/64);

      Proxy_RxBus.Proxy_RxCanRxAccelPedlInfo.AccelPedlValErr = 0;
      Proxy_fcu1PVAVError = 0;
    }

    if(((uint8)Proxy_RxBus.Proxy_RxRxEms2Info.AccPedDep_PC) == 0xFF)
    {
      PVAV_CAN_InvChk.u1RefSigErrDet=1;
    }
    else
    {
      PVAV_CAN_InvChk.u1RefSigErrDet=0;
    }

    CC_vChkCanMsgSignal(&PVAV_CAN_InvChk, RxMsg_ToChk[CHK_CAN_EMS2_MSG_RX].u1RxErrFlg, __U8_SCC_EMS1EMS2MSGINVALID_THRES);

    /*AVH EMS TPS 관련 계산식 in AHB Gen1
    Proxy_lesps16EMS_TPS      = (sint16)(((sint16)b_CR_Ems_Tps_Pc_c - 0x20)*100/213);
    AVH EMS TPS 관련 계산식 in AHB Gen3
    */
    //Com_ReceiveSignal(COM_SIG_RX_EMS2_TPS_PC,&ComSignalData);
    if(Proxy_RxBus.Proxy_RxRxEms2Info.Tps_PC == 0xFF)
    {
      Proxy_RxBus.Proxy_RxCanRxEscInfo.ThrottleAngleErr=Proxy_fcu1TPSError= 1;
    }
    else
    {
      Proxy_RxBus.Proxy_RxCanRxEscInfo.ThrottleAngleErr=Proxy_fcu1TPSError= 0;
      Proxy_RxBus.Proxy_RxCanRxEscInfo.ThrottleAngle      = (sint16)(((sint16)Proxy_RxBus.Proxy_RxRxEms2Info.Tps_PC - 0x20)*7/15);
      Proxy_RxBus.Proxy_RxCanRxEscInfo.TpsResol1000 =  (sint16)(((sint16)Proxy_RxBus.Proxy_RxRxEms2Info.Tps_PC - 0x20)*14/3);

      /* AVH 관련 Resol 변수 추가 */
      if(Proxy_RxBus.Proxy_RxCanRxEscInfo.ThrottleAngle>0)
      {
        Proxy_fcu8TPS = (unsigned char)Proxy_RxBus.Proxy_RxCanRxEscInfo.ThrottleAngle;
      }
      else
      {
        Proxy_fcu8TPS = 0;
      }
    }

    if(ComSignalData == 0xFF)
    {
      TPS_InvChk.u1RefSigErrDet=1;
    }
    else
    {
      TPS_InvChk.u1RefSigErrDet=0;
    }

    CC_vChkCanMsgSignal(&TPS_InvChk, RxMsg_ToChk[CHK_CAN_EMS2_MSG_RX].u1RxErrFlg, __U8_SCC_EMS1EMS2MSGINVALID_THRES);
  }    
}

static void Proxy_RxEms1(void)
{
  uint8 ComSignalData;
  uint16 ComSignalData1;
  
  CC_vCanReceiveChk(&EMS1_RxChk, (uint8)RxMsg_ToChk[CHK_CAN_EMS1_MSG_RX].u1RxCopyOkFlg, U8_CC_TIME_500MS_5msLoop);

  if(RxMsg_ToChk[CHK_CAN_EMS1_MSG_RX].u1RxCopyOkFlg==1 && Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Ems1MsgOkFlg==1)
  {
    RxMsg_ToChk[CHK_CAN_EMS1_MSG_RX].u1RxCopyOkFlg=0;
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Ems1MsgOkFlg=0;

    //Com_ReceiveSignal(COM_SIG_RX_EMS1_TQSTD_NM,&ComSignalData);
    Proxy_RxBus.Proxy_RxCanRxEscInfo.EngStdTq    = (sint16)Proxy_RxBus.Proxy_RxRxEms1Info.TqStd_NM;

    //Com_ReceiveSignal(COM_SIG_RX_EMS1_ACTINDTQ_PC,&ComSignalData);
    Proxy_RxBus.Proxy_RxCanRxEscInfo.EngActIndTq= (sint16)((sint16)Proxy_RxBus.Proxy_RxRxEms1Info.ActINDTQ_PC*39/10);

    //Com_ReceiveSignal(COM_SIG_RX_EMS1_ENGSPD_RPM,&ComSignalData1);
    Proxy_RxBus.Proxy_RxCanRxEscInfo.EngRpm          = (sint16)Proxy_RxBus.Proxy_RxRxEms1Info.EngSpd_RPM/4;
    Proxy_fcu16EngRpm           = (uint16)Proxy_RxBus.Proxy_RxRxEms1Info.EngSpd_RPM/4;

    //Com_ReceiveSignal(COM_SIG_RX_EMS1_INDTQ_PC,&ComSignalData);
    Proxy_RxBus.Proxy_RxCanRxEscInfo.EngIndTq   = (sint16)((sint16)Proxy_RxBus.Proxy_RxRxEms1Info.IndTQ_PC*39/10);

    //Com_ReceiveSignal(COM_SIG_RX_EMS1_FRICTTQ_PC,&ComSignalData);
    Proxy_RxBus.Proxy_RxCanRxEscInfo.EngFrictionLossTq       = (sint16)((sint16)Proxy_RxBus.Proxy_RxRxEms1Info.FrictTQ_PC*39/10);
  }    
}

static void Proxy_RxClu2(void)
{
  uint8 ComSignalData;

  CC_vCanReceiveChk(&CLU2_RxChk, (uint8)RxMsg_ToChk[CHK_CAN_CLU2_MSG_RX].u1RxCopyOkFlg, U16_CC_TIME_2SEC_5msLoop);
  CC_vCanReceiveChk(&CLU2_CANIGN_RxChk, (uint8)RxMsg_ToChk[CHK_CAN_CLU2_MSG_RX].u1RxCopyOkFlg, U8_CC_TIME_500MS_5msLoop);
  
  Proxy_ccu1IgnRunInv=CLU2_CANIGN_RxChk.u1RxMissingFlg;
  if(CLU2_RxChk.u1RxMissingFlg==1)
  {
  }
  else
  {
    ;
  }

  if(RxMsg_ToChk[CHK_CAN_CLU2_MSG_RX].u1RxCopyOkFlg==1 && Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Clu2MsgOkFlg==1)
  {
    RxMsg_ToChk[CHK_CAN_CLU2_MSG_RX].u1RxCopyOkFlg=0;
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Clu2MsgOkFlg = 0;

    //Com_ReceiveSignal(COM_SIG_RX_CLU2_IGN_SW,&ComSignalData);
    if(Proxy_RxBus.Proxy_RxRxClu2Info.IGN_SW==3)
    {
      Proxy_ccu1IgnRun=1;
    }
    else
    {
      Proxy_ccu1IgnRun=0;
    }
  }
}

static void Proxy_RxClu1(void)
{
  uint8 ComSignalData;

  CC_vCanReceiveChk(&CLU1_RxChk, (uint8)RxMsg_ToChk[CHK_CAN_CLU1_MSG_RX].u1RxCopyOkFlg, U8_CC_TIME_500MS_5msLoop);

  if(CLU1_RxChk.u1RxMissingFlg==1)
  {
    Proxy_Proxy_fu1BFLSignalInv=1;
    ccu1ParkingSwitch_CLU_tout=1;
  }
  else
  {
    Proxy_Proxy_fu1BFLSignalInv=0;
    ccu1ParkingSwitch_CLU_tout=0;
  }

  if(RxMsg_ToChk[CHK_CAN_CLU1_MSG_RX].u1RxCopyOkFlg==1 && Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Clu1MsgOkFlg==1)
  {
    RxMsg_ToChk[CHK_CAN_CLU1_MSG_RX].u1RxCopyOkFlg=0;
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Clu1MsgOkFlg = 0;

    //Com_ReceiveSignal(COM_SIG_RX_CLU1_P_BRAKE_ACT,&ComSignalData);
    ccu8Clu1ParkBrkAct=Proxy_RxBus.Proxy_RxRxClu1Info.P_Brake_Act;
    if(ComSignalData==1)
    {
      ccu1ParkingSwitch_CLU=1;
    }
    else
    {
      ccu1ParkingSwitch_CLU=0;
    }

    /* BFL Switch SW 추가 */
    //Com_ReceiveSignal(COM_SIG_RX_CLU1_CF_CLU_BRAKEFLUIDSW,&ComSignalData);
    Proxy_fu1BFLSignal = Proxy_RxBus.Proxy_RxRxClu1Info.Cf_Clu_BrakeFluIDSW;

    /* HG,VG HEV SCC 사양 추가에 따른 Signal 추가 */
  }
}
        
static void CC_vLoadRxInterface(void)
{
  Proxy_ccu1ParkingSwitchInv=1;

  /*CLU1메시지 보다 EPB1 메시지가 우선순위가 높도록 설정*/

  if(ccu1ParkingSwitch_CLU_tout==0)
  {
    Proxy_RxBus.Proxy_RxCanRxEscInfo.PbSwt   =ccu1ParkingSwitch_CLU;
    Proxy_ccu1ParkingSwitchInv=ccu1ParkingSwitchInv_CLU;
  }
  else
  {
    ;
  }


  #ifdef __EPB1_ENA_ENABLE
  if(ccu1ParkingSwitch_EPB_tout==0)
  {
    Proxy_5msBus.Proxy_5msCanRxInfo.PbSwt   =ccu1ParkingSwitch_EPB;
    Proxy_ccu1ParkingSwitchInv=ccu1ParkingSwitchInv_EPB;
  }
  else
  {
    ;
  }
  #endif
}

#define PROXY_STOP_SEC_CODE
#include "Proxy_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */

