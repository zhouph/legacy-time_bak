/**
 * @defgroup Proxy_EncodeByCom Proxy_EncodeByCom
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Proxy_EncodeByCom.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Proxy_EncodeByCom.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PROXY_START_SEC_CONST_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PROXY_STOP_SEC_CONST_UNSPECIFIED
#include "Proxy_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLES DEFINITIONS
 =============================================================================*/
#define PROXY_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
extern Proxy_RxByCom_HdrBusType Proxy_RxByComBus;
extern Proxy_TxByCom_HdrBusType Proxy_TxByComBus;

extern Com_FrameType  Com_FrameBuf[CAN_FRAME_MAX_NUM];

/* For Compile */
#define PROXY_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Proxy_MemMap.h"
#define PROXY_START_SEC_VAR_NOINIT_32BIT
#include "Proxy_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PROXY_STOP_SEC_VAR_NOINIT_32BIT
#include "Proxy_MemMap.h"
#define PROXY_START_SEC_VAR_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PROXY_STOP_SEC_VAR_UNSPECIFIED
#include "Proxy_MemMap.h"
#define PROXY_START_SEC_VAR_32BIT
#include "Proxy_MemMap.h"
/** Variable Section (32BIT)**/


#define PROXY_STOP_SEC_VAR_32BIT
#include "Proxy_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLES DEFINITIONS
 =============================================================================*/
#define PROXY_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PROXY_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Proxy_MemMap.h"
#define PROXY_START_SEC_VAR_NOINIT_32BIT
#include "Proxy_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PROXY_STOP_SEC_VAR_NOINIT_32BIT
#include "Proxy_MemMap.h"
#define PROXY_START_SEC_VAR_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PROXY_STOP_SEC_VAR_UNSPECIFIED
#include "Proxy_MemMap.h"
#define PROXY_START_SEC_VAR_32BIT
#include "Proxy_MemMap.h"
/** Variable Section (32BIT)**/


#define PROXY_STOP_SEC_VAR_32BIT
#include "Proxy_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
static uint8 Proxy_CalWheelPulChkSum(void);
static uint8 Proxy_CalAHB1ChkSum(void);
static uint8 Proxy_vEncTxIsrTCS1(void);

#define PROXY_START_SEC_CODE
#include "Proxy_MemMap.h"

static void Proxy_TxYawCbit(void);
static void Proxy_TxWhlSpd(void);
static void Proxy_TxWhlPul(void);
static void Proxy_TxTcs5(void);
static void Proxy_TxTcs1(void);
static void Proxy_TxSasCal(void);
static void Proxy_TxEsp2(void);
static void Proxy_TxEbs1(void);
static void Proxy_TxAhb1(void);
static void Proxy_Getfu1BrakeLampRequest(void);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

void Proxy_TxProcByCom(void)
{
   Proxy_TxTcs1();
   Proxy_TxTcs5();
   Proxy_TxEsp2();
   Proxy_TxWhlPul();
   Proxy_TxAhb1();
   Proxy_TxEbs1();
   Proxy_TxWhlSpd();
   Proxy_TxYawCbit();
   Proxy_TxSasCal();
}
  
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static void Proxy_TxYawCbit(void)
{
    Com_SignalDataType ComSignalData;

    ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxYawCbitInfo.TestYawRateSensor;
    Com_SendSignal(COM_SIG_TX_YAWCBIT_TESTYAWRATESENSOR, &ComSignalData);

    ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxYawCbitInfo.TestAcclSensor;
    Com_SendSignal(COM_SIG_TX_YAWCBIT_TESTACCLSENSOR, &ComSignalData);

    ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxYawCbitInfo.YawSerialNumberReq;
    Com_SendSignal(COM_SIG_TX_YAWCBIT_YAWSERIALNUMBERREQ, &ComSignalData);
}

static void Proxy_TxWhlSpd(void)
{
  Com_SignalDataType ComSignalData;
  
  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxWhlSpdInfo.WhlSpdFL;
  Com_SendSignal(COM_SIG_TX_WHLSPD_WHL_SPD_FL, &ComSignalData);
  
  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxWhlSpdInfo.WhlSpdFR;
  Com_SendSignal(COM_SIG_TX_WHLSPD_WHL_SPD_FR, &ComSignalData);
  
  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxWhlSpdInfo.WhlSpdRL;
  Com_SendSignal(COM_SIG_TX_WHLSPD_WHL_SPD_RL, &ComSignalData);
  
  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxWhlSpdInfo.WhlSpdRR;
  Com_SendSignal(COM_SIG_TX_WHLSPD_WHL_SPD_RR, &ComSignalData);
}

static void Proxy_TxWhlPul(void)
{
  Com_SignalDataType ComSignalData;

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxWhlPulInfo.WhlPulFL;
  Com_SendSignal(COM_SIG_TX_WHL_PUL_FL, &ComSignalData);
  
  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxWhlPulInfo.WhlPulFR;
  Com_SendSignal(COM_SIG_TX_WHL_PUL_FR, &ComSignalData);
  
  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxWhlPulInfo.WhlPulRL;
  Com_SendSignal(COM_SIG_TX_WHL_PUL_RL, &ComSignalData);
  
  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxWhlPulInfo.WhlPulRR;
  Com_SendSignal(COM_SIG_TX_WHL_PUL_RR, &ComSignalData);
  
  ComSignalData = Proxy_CalWheelPulChkSum();
  Com_SendSignal(COM_SIG_TX_WHL_PUL_CHKSUM, &ComSignalData);
}

static void Proxy_TxTcs5(void)
{
  Com_SignalDataType ComSignalData;

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxTcs5Info.CfBrkAbsWLMP;
  Com_SendSignal(COM_SIG_TX_TCS5_CF_BRK_ABSWLMP, &ComSignalData);

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxTcs5Info.CfBrkEbdWLMP;
  Com_SendSignal(COM_SIG_TX_TCS5_CF_BRK_EBDWLMP, &ComSignalData);

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxTcs5Info.CfBrkTcsWLMP;
  Com_SendSignal(COM_SIG_TX_TCS5_CF_BRK_TCSWLMP, &ComSignalData);

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxTcs5Info.CfBrkTcsFLMP;
  Com_SendSignal(COM_SIG_TX_TCS5_CF_BRK_TCSFLMP, &ComSignalData);

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxTcs5Info.CfBrkAbsDIAG;
  Com_SendSignal(COM_SIG_TX_TCS5_CF_BRK_ABSDIAG, &ComSignalData);

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxTcs5Info.CrBrkWheelFL_KMH;
  Com_SendSignal(COM_SIG_TX_TCS5_CR_BRK_WHEELFL_KMH, &ComSignalData);
  
  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxTcs5Info.CrBrkWheelFR_KMH;
  Com_SendSignal(COM_SIG_TX_TCS5_CR_BRK_WHEELFR_KMH, &ComSignalData);
  
  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxTcs5Info.CrBrkWheelRL_KMH;
  Com_SendSignal(COM_SIG_TX_TCS5_CR_BRK_WHEELRL_KMH, &ComSignalData);

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxTcs5Info.CrBrkWheelRR_KMH;
  Com_SendSignal(COM_SIG_TX_TCS5_CR_BRK_WHEELRR_KMH, &ComSignalData);
}

static void Proxy_TxTcs1(void)
{
  Com_SignalDataType ComSignalData;
  
  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CfBrkTcsREQ;
  Com_SendSignal(COM_SIG_TX_TCS1_CF_BRK_TCSREQ, &ComSignalData);
  
  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CfBrkTcsPAS;
  Com_SendSignal(COM_SIG_TX_TCS1_CF_BRK_TCSPAS, &ComSignalData);

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CfBrkTcsDEF;  
  Com_SendSignal(COM_SIG_TX_TCS1_CF_BRK_TCSDEF, &ComSignalData);
  
  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CfBrkTcsCTL;
  Com_SendSignal(COM_SIG_TX_TCS1_CF_BRK_TCSCTL, &ComSignalData);
  
  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CfBrkAbsACT;
  Com_SendSignal(COM_SIG_TX_TCS1_CF_BRK_ABSACT, &ComSignalData);
  
  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CfBrkAbsDEF;
  Com_SendSignal(COM_SIG_TX_TCS1_CF_BRK_ABSDEF, &ComSignalData);
  
  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CfBrkEbdDEF;
  Com_SendSignal(COM_SIG_TX_TCS1_CF_BRK_EBDDEF, &ComSignalData);
  
  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CfBrkTcsGSC;
  Com_SendSignal(COM_SIG_TX_TCS1_CF_BRK_TCSGSC, &ComSignalData);
  
  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CfBrkEspPAS;
  Com_SendSignal(COM_SIG_TX_TCS1_CF_BRK_ESPPAS, &ComSignalData);

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CfBrkEspDEF;
  Com_SendSignal(COM_SIG_TX_TCS1_CF_BRK_ESPDEF, &ComSignalData);
  
  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CfBrkEspCTL;
  Com_SendSignal(COM_SIG_TX_TCS1_CF_BRK_ESPCTL, &ComSignalData);
  
  ComSignalData     = Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CfBrkMsrREQ;
  Com_SendSignal(COM_SIG_TX_TCS1_CF_BRK_MSRREQ, &ComSignalData);
  
  ComSignalData    = Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CfBrkTcsMFRN;
  Com_SendSignal(COM_SIG_TX_TCS1_CF_BRK_TCSMFRN, &ComSignalData);
  
  ComSignalData    = Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CfBrkMinGEAR;
  Com_SendSignal(COM_SIG_TX_TCS1_CF_BRK_MINGEAR, &ComSignalData);
  
  ComSignalData    = Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CfBrkMaxGEAR;
  Com_SendSignal(COM_SIG_TX_TCS1_CF_BRK_MAXGEAR, &ComSignalData);

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.BrakeLight;
  Com_SendSignal(COM_SIG_TX_TCS1_BRAKELIGHT, &ComSignalData);

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.HacCtl;
  Com_SendSignal(COM_SIG_TX_TCS1_HAC_CTL, &ComSignalData);

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.HacPas;
  Com_SendSignal(COM_SIG_TX_TCS1_HAC_PAS, &ComSignalData);

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.HacDef;
  Com_SendSignal(COM_SIG_TX_TCS1_HAC_DEF, &ComSignalData);

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CrBrkTQI_PC;
  Com_SendSignal(COM_SIG_TX_TCS1_CR_BRK_TQI_PC, &ComSignalData);
  
  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CrBrkTQIMSR_PC;
  Com_SendSignal(COM_SIG_TX_TCS1_CR_BRK_TQIMSR_PC, &ComSignalData);
  
  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CrBrkTQISLW_PC;
  Com_SendSignal(COM_SIG_TX_TCS1_CR_BRK_TQISLW_PC, &ComSignalData);
  
  Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CrEbsMSGCHKSUM = Proxy_vEncTxIsrTCS1();
  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CrEbsMSGCHKSUM;
  Com_SendSignal(COM_SIG_TX_TCS1_CR_EBS_MSGCHKSUM, &ComSignalData);
}

static void Proxy_TxSasCal(void)
{
  Com_SignalDataType ComSignalData;

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxSasCalInfo.CalSas_CCW;
  Com_SendSignal(COM_SIG_TX_CALSAS_CCW, &ComSignalData);

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxSasCalInfo.CalSas_Lws_CID;
  Com_SendSignal(COM_SIG_TX_CALSAS_LWS_CID, &ComSignalData);
}

static void Proxy_TxEsp2(void)
{
  Com_SignalDataType ComSignalData;

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxEsp2Info.LatAccel;
  Com_SendSignal(COM_SIG_TX_ESP2_LAT_ACCEL, &ComSignalData);
  
  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxEsp2Info.LatAccelStat;
  Com_SendSignal(COM_SIG_TX_ESP2_LAT_ACCEL_STAT, &ComSignalData);
  
  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxEsp2Info.LatAccelDiag;
  Com_SendSignal(COM_SIG_TX_ESP2_LAT_ACCEL_DIAG, &ComSignalData);
  
  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxEsp2Info.LongAccel;
  Com_SendSignal(COM_SIG_TX_ESP2_LONG_ACCEL, &ComSignalData);
  
  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxEsp2Info.LongAccelStat;
  Com_SendSignal(COM_SIG_TX_ESP2_LONG_ACCEL_STAT, &ComSignalData);
  
  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxEsp2Info.LongAccelDiag;
  Com_SendSignal(COM_SIG_TX_ESP2_LONG_ACCEL_DIAG, &ComSignalData);

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxEsp2Info.YawRate;
  Com_SendSignal(COM_SIG_TX_ESP2_YAW_RATE, &ComSignalData);
  
  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxEsp2Info.YawRateStat;
  Com_SendSignal(COM_SIG_TX_ESP2_YAW_RATE_STAT, &ComSignalData);
  
  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxEsp2Info.YawRateDiag;
  Com_SendSignal(COM_SIG_TX_ESP2_YAW_RATE_DIAG, &ComSignalData);
  
  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxEsp2Info.CylPres;
  Com_SendSignal(COM_SIG_TX_ESP2_CYL_PRES, &ComSignalData);
  
  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxEsp2Info.CylPresStat;
  Com_SendSignal(COM_SIG_TX_ESP2_CYL_PRES_STAT, &ComSignalData);
  
  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxEsp2Info.CylPresDiag;
  Com_SendSignal(COM_SIG_TX_ESP2_CYL_PRES_DIAG, &ComSignalData);
}

static void Proxy_TxEbs1(void)
{
  Com_SignalDataType ComSignalData;

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxEbs1Info.CfBrkEbsStat;
  Com_SendSignal(COM_SIG_TX_EBS1_CF_BRK_EBSSTAT, &ComSignalData);

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxEbs1Info.CrBrkRegenTQLimit_NM;
  Com_SendSignal(COM_SIG_TX_EBS1_CR_BRK_REGENTQLIMIT_NM, &ComSignalData);

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxEbs1Info.CrBrkEstTot_NM;
  Com_SendSignal(COM_SIG_TX_EBS1_CR_BRK_ESTTOT_NM, &ComSignalData);

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxEbs1Info.CfBrkSnsFail;
  Com_SendSignal(COM_SIG_TX_EBS1_CF_BRK_SNSFAIL, &ComSignalData);

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxEbs1Info.CfBrkRbsWLamp;
  Com_SendSignal(COM_SIG_TX_EBS1_CF_BRK_RBSWLAMP, &ComSignalData);

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxEbs1Info.CfBrkVacuumSysDef;
  Com_SendSignal(COM_SIG_TX_EBS1_CF_BRK_VACUUMSYSDEF, &ComSignalData);

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxEbs1Info.CrBrkEstHyd_NM;
  Com_SendSignal(COM_SIG_TX_EBS1_CR_BRK_ESTHYD_NM, &ComSignalData);

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxEbs1Info.CrBrkStkDep_PC;
  Com_SendSignal(COM_SIG_TX_EBS1_CR_BRK_STKDEP_PC, &ComSignalData);

  Com_SendSignal(COM_SIG_TX_EBS1_CF_BRK_PGMRUN1, &Proxy_TxByComBus.Proxy_TxByComTxEbs1Info.CfBrkPgmRun1);
}

static void Proxy_TxAhb1(void)
{
  Com_SignalDataType ComSignalData;
  sint16 ComSignalData1;

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxAhb1Info.CfAhbWLMP;
  Com_SendSignal(COM_SIG_TX_AHB1_CF_AHB_WLMP, &ComSignalData);

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxAhb1Info.CfAhbDiag;
  Com_SendSignal(COM_SIG_TX_AHB1_CF_AHB_DIAG, &ComSignalData);

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxAhb1Info.CfAhbAct;
  Com_SendSignal(COM_SIG_TX_AHB1_CF_AHB_ACT, &ComSignalData);

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxAhb1Info.CfAhbDef;
  Com_SendSignal(COM_SIG_TX_AHB1_CF_AHB_DEF, &ComSignalData);

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxAhb1Info.CfAhbSLMP;
  Com_SendSignal(COM_SIG_TX_AHB1_CF_AHB_SLMP, &ComSignalData);

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxAhb1Info.CfBrkAhbSTDep_PC;
  Com_SendSignal(COM_SIG_TX_AHB1_CF_BRK_AHBSTDEP_PC, &ComSignalData);

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxAhb1Info.CfBrkBuzzR;
  Com_SendSignal(COM_SIG_TX_AHB1_CF_BRK_BUZZR, &ComSignalData);

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxAhb1Info.CfBrkPedalCalStatus;
  Com_SendSignal(COM_SIG_TX_AHB1_CF_BRK_PEDALCALSTATUS, &ComSignalData);

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxAhb1Info.CfBrkAhbSnsFail;
  Com_SendSignal(COM_SIG_TX_AHB1_CF_BRK_AHBSNSFAIL, &ComSignalData);

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxAhb1Info.WhlSpdFLAhb;
  Com_SendSignal(COM_SIG_TX_AHB1_WHL_SPD_FL_AHB, &ComSignalData);

  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxAhb1Info.WhlSpdFRAhb;
  Com_SendSignal(COM_SIG_TX_AHB1_WHL_SPD_FR_AHB, &ComSignalData);

  Proxy_TxByComBus.Proxy_TxByComTxAhb1Info.CrAhbMsgChkSum = Proxy_CalAHB1ChkSum();
  ComSignalData = Proxy_TxByComBus.Proxy_TxByComTxAhb1Info.CrAhbMsgChkSum;
  Com_SendSignal(COM_SIG_TX_AHB1_CR_AHB_MSGCHKSUM, &ComSignalData);
}

uint8 Proxy_CalAHB1ChkSum(void)
{
  uint8 chksum;
  return chksum = (Com_FrameBuf[CAN_FRAME_AHB1_MSG_TX].Data.B[0]
                   ^Com_FrameBuf[CAN_FRAME_AHB1_MSG_TX].Data.B[1]
                   ^Com_FrameBuf[CAN_FRAME_AHB1_MSG_TX].Data.B[2]
                   ^Com_FrameBuf[CAN_FRAME_AHB1_MSG_TX].Data.B[3]
                   ^Com_FrameBuf[CAN_FRAME_AHB1_MSG_TX].Data.B[4]
                   ^Com_FrameBuf[CAN_FRAME_AHB1_MSG_TX].Data.B[5]
                   ^Com_FrameBuf[CAN_FRAME_AHB1_MSG_TX].Data.B[6]);
}

uint8 Proxy_CalWheelPulChkSum(void)
{
  uint8 chksum;
  return chksum = (Com_FrameBuf[CAN_FRAME_WHLPUL_MSG_TX].Data.B[0]
                     +Com_FrameBuf[CAN_FRAME_WHLPUL_MSG_TX].Data.B[1]
                     +Com_FrameBuf[CAN_FRAME_WHLPUL_MSG_TX].Data.B[2]
                     +Com_FrameBuf[CAN_FRAME_WHLPUL_MSG_TX].Data.B[3]
                     +Com_FrameBuf[CAN_FRAME_WHLPUL_MSG_TX].Data.B[4]
                     +Com_FrameBuf[CAN_FRAME_WHLPUL_MSG_TX].Data.B[5]
                     +Com_FrameBuf[CAN_FRAME_WHLPUL_MSG_TX].Data.B[6]);
}

uint8 Proxy_vEncTxIsrTCS1(void)
{
  uint8 chksum;
  return chksum = (Com_FrameBuf[CAN_FRAME_TCS1_MSG_TX].Data.B[0]
                   ^Com_FrameBuf[CAN_FRAME_TCS1_MSG_TX].Data.B[1]
                   ^Com_FrameBuf[CAN_FRAME_TCS1_MSG_TX].Data.B[2]
                   ^Com_FrameBuf[CAN_FRAME_TCS1_MSG_TX].Data.B[3]
                   ^Com_FrameBuf[CAN_FRAME_TCS1_MSG_TX].Data.B[4]
                   ^Com_FrameBuf[CAN_FRAME_TCS1_MSG_TX].Data.B[5]
                   ^Com_FrameBuf[CAN_FRAME_TCS1_MSG_TX].Data.B[6]);
}

#define PROXY_STOP_SEC_CODE
#include "Proxy_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */

