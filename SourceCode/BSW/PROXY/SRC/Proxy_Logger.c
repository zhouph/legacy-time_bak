/*
 * PROXY_Logger.c
 *
 *  Created on: 2015. 1. 13.
 *      Author: jinsu.park
 */


/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Proxy_Logger.h"
#include "Proxy_Decode.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PROXY_START_SEC_CONST_UNSPECIFIED
#include "PROXY_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PROXY_STOP_SEC_CONST_UNSPECIFIED
#include "PROXY_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLES DEFINITIONS
 =============================================================================*/
#define PROXY_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "PROXY_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
extern uint8		CAN_LOG_DATA[80];
extern uint8		AHBLOG[80];
#define PROXY_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "PROXY_MemMap.h"
#define PROXY_START_SEC_VAR_NOINIT_32BIT
#include "PROXY_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PROXY_STOP_SEC_VAR_NOINIT_32BIT
#include "PROXY_MemMap.h"
#define PROXY_START_SEC_VAR_UNSPECIFIED
#include "PROXY_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PROXY_STOP_SEC_VAR_UNSPECIFIED
#include "PROXY_MemMap.h"
#define PROXY_START_SEC_VAR_32BIT
#include "PROXY_MemMap.h"
/** Variable Section (32BIT)**/


#define PROXY_STOP_SEC_VAR_32BIT
#include "PROXY_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLES DEFINITIONS
 =============================================================================*/
#define PROXY_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "PROXY_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PROXY_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "PROXY_MemMap.h"
#define PROXY_START_SEC_VAR_NOINIT_32BIT
#include "PROXY_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PROXY_STOP_SEC_VAR_NOINIT_32BIT
#include "PROXY_MemMap.h"
#define PROXY_START_SEC_VAR_UNSPECIFIED
#include "PROXY_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PROXY_STOP_SEC_VAR_UNSPECIFIED
#include "PROXY_MemMap.h"
#define PROXY_START_SEC_VAR_32BIT
#include "PROXY_MemMap.h"
/** Variable Section (32BIT)**/


#define PROXY_STOP_SEC_VAR_32BIT
#include "PROXY_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PROXY_START_SEC_CODE
#include "PROXY_MemMap.h"
 
static void PROXY_LoadLoggerData(void)
{
	Com_SendSignal(COM_SIG_TX_AHBLOG0_0, &AHBLOG[0]);
	Com_SendSignal(COM_SIG_TX_AHBLOG0_1, &AHBLOG[1]);
	Com_SendSignal(COM_SIG_TX_AHBLOG0_2, &AHBLOG[2]);
	Com_SendSignal(COM_SIG_TX_AHBLOG0_3, &AHBLOG[3]);
	Com_SendSignal(COM_SIG_TX_AHBLOG0_4, &AHBLOG[4]);
	Com_SendSignal(COM_SIG_TX_AHBLOG0_5, &AHBLOG[5]);
	Com_SendSignal(COM_SIG_TX_AHBLOG0_6, &AHBLOG[6]);
	Com_SendSignal(COM_SIG_TX_AHBLOG0_7, &AHBLOG[7]);
	Com_SendSignal(COM_SIG_TX_AHBLOG1_0, &AHBLOG[8]);
	Com_SendSignal(COM_SIG_TX_AHBLOG1_1, &AHBLOG[9]);
	Com_SendSignal(COM_SIG_TX_AHBLOG1_2, &AHBLOG[10]);
	Com_SendSignal(COM_SIG_TX_AHBLOG1_3, &AHBLOG[11]);
	Com_SendSignal(COM_SIG_TX_AHBLOG1_4, &AHBLOG[12]);
	Com_SendSignal(COM_SIG_TX_AHBLOG1_5, &AHBLOG[13]);
	Com_SendSignal(COM_SIG_TX_AHBLOG1_6, &AHBLOG[14]);
	Com_SendSignal(COM_SIG_TX_AHBLOG1_7, &AHBLOG[15]);
	Com_SendSignal(COM_SIG_TX_AHBLOG2_0, &AHBLOG[16]);
	Com_SendSignal(COM_SIG_TX_AHBLOG2_1, &AHBLOG[17]);
	Com_SendSignal(COM_SIG_TX_AHBLOG2_2, &AHBLOG[18]);
	Com_SendSignal(COM_SIG_TX_AHBLOG2_3, &AHBLOG[19]);
	Com_SendSignal(COM_SIG_TX_AHBLOG2_4, &AHBLOG[20]);
	Com_SendSignal(COM_SIG_TX_AHBLOG2_5, &AHBLOG[21]);
	Com_SendSignal(COM_SIG_TX_AHBLOG2_6, &AHBLOG[22]);
	Com_SendSignal(COM_SIG_TX_AHBLOG2_7, &AHBLOG[23]);
	Com_SendSignal(COM_SIG_TX_AHBLOG3_0, &AHBLOG[24]);
	Com_SendSignal(COM_SIG_TX_AHBLOG3_1, &AHBLOG[25]);
	Com_SendSignal(COM_SIG_TX_AHBLOG3_2, &AHBLOG[26]);
	Com_SendSignal(COM_SIG_TX_AHBLOG3_3, &AHBLOG[27]);
	Com_SendSignal(COM_SIG_TX_AHBLOG3_4, &AHBLOG[28]);
	Com_SendSignal(COM_SIG_TX_AHBLOG3_5, &AHBLOG[29]);
	Com_SendSignal(COM_SIG_TX_AHBLOG3_6, &AHBLOG[30]);
	Com_SendSignal(COM_SIG_TX_AHBLOG3_7, &AHBLOG[31]);
	Com_SendSignal(COM_SIG_TX_AHBLOG4_0, &AHBLOG[32]);
	Com_SendSignal(COM_SIG_TX_AHBLOG4_1, &AHBLOG[33]);
	Com_SendSignal(COM_SIG_TX_AHBLOG4_2, &AHBLOG[34]);
	Com_SendSignal(COM_SIG_TX_AHBLOG4_3, &AHBLOG[35]);
	Com_SendSignal(COM_SIG_TX_AHBLOG4_4, &AHBLOG[36]);
	Com_SendSignal(COM_SIG_TX_AHBLOG4_5, &AHBLOG[37]);
	Com_SendSignal(COM_SIG_TX_AHBLOG4_6, &AHBLOG[38]);
	Com_SendSignal(COM_SIG_TX_AHBLOG4_7, &AHBLOG[39]);
	Com_SendSignal(COM_SIG_TX_AHBLOG5_0, &AHBLOG[40]);
	Com_SendSignal(COM_SIG_TX_AHBLOG5_1, &AHBLOG[41]);
	Com_SendSignal(COM_SIG_TX_AHBLOG5_2, &AHBLOG[42]);
	Com_SendSignal(COM_SIG_TX_AHBLOG5_3, &AHBLOG[43]);
	Com_SendSignal(COM_SIG_TX_AHBLOG5_4, &AHBLOG[44]);
	Com_SendSignal(COM_SIG_TX_AHBLOG5_5, &AHBLOG[45]);
	Com_SendSignal(COM_SIG_TX_AHBLOG5_6, &AHBLOG[46]);
	Com_SendSignal(COM_SIG_TX_AHBLOG5_7, &AHBLOG[47]);
	Com_SendSignal(COM_SIG_TX_AHBLOG6_0, &AHBLOG[48]);
	Com_SendSignal(COM_SIG_TX_AHBLOG6_1, &AHBLOG[49]);
	Com_SendSignal(COM_SIG_TX_AHBLOG6_2, &AHBLOG[50]);
	Com_SendSignal(COM_SIG_TX_AHBLOG6_3, &AHBLOG[51]);
	Com_SendSignal(COM_SIG_TX_AHBLOG6_4, &AHBLOG[52]);
	Com_SendSignal(COM_SIG_TX_AHBLOG6_5, &AHBLOG[53]);
	Com_SendSignal(COM_SIG_TX_AHBLOG6_6, &AHBLOG[54]);
	Com_SendSignal(COM_SIG_TX_AHBLOG6_7, &AHBLOG[55]);
	Com_SendSignal(COM_SIG_TX_AHBLOG7_0, &AHBLOG[56]);
	Com_SendSignal(COM_SIG_TX_AHBLOG7_1, &AHBLOG[57]);
	Com_SendSignal(COM_SIG_TX_AHBLOG7_2, &AHBLOG[58]);
	Com_SendSignal(COM_SIG_TX_AHBLOG7_3, &AHBLOG[59]);
	Com_SendSignal(COM_SIG_TX_AHBLOG7_4, &AHBLOG[60]);
	Com_SendSignal(COM_SIG_TX_AHBLOG7_5, &AHBLOG[61]);
	Com_SendSignal(COM_SIG_TX_AHBLOG7_6, &AHBLOG[62]);
	Com_SendSignal(COM_SIG_TX_AHBLOG7_7, &AHBLOG[63]);
	Com_SendSignal(COM_SIG_TX_AHBLOG8_0, &AHBLOG[64]);
	Com_SendSignal(COM_SIG_TX_AHBLOG8_1, &AHBLOG[65]);
	Com_SendSignal(COM_SIG_TX_AHBLOG8_2, &AHBLOG[66]);
	Com_SendSignal(COM_SIG_TX_AHBLOG8_3, &AHBLOG[67]);
	Com_SendSignal(COM_SIG_TX_AHBLOG8_4, &AHBLOG[68]);
	Com_SendSignal(COM_SIG_TX_AHBLOG8_5, &AHBLOG[69]);
	Com_SendSignal(COM_SIG_TX_AHBLOG8_6, &AHBLOG[70]);
	Com_SendSignal(COM_SIG_TX_AHBLOG8_7, &AHBLOG[71]);
	Com_SendSignal(COM_SIG_TX_AHBLOG9_0, &AHBLOG[72]);
	Com_SendSignal(COM_SIG_TX_AHBLOG9_1, &AHBLOG[73]);
	Com_SendSignal(COM_SIG_TX_AHBLOG9_2, &AHBLOG[74]);
	Com_SendSignal(COM_SIG_TX_AHBLOG9_3, &AHBLOG[75]);
	Com_SendSignal(COM_SIG_TX_AHBLOG9_4, &AHBLOG[76]);
	Com_SendSignal(COM_SIG_TX_AHBLOG9_5, &AHBLOG[77]);
	Com_SendSignal(COM_SIG_TX_AHBLOG9_6, &AHBLOG[78]);
	Com_SendSignal(COM_SIG_TX_AHBLOG9_7, &AHBLOG[79]);

	Com_SendSignal(COM_SIG_TX_ESCLOG0_0, &CAN_LOG_DATA[0]);
	Com_SendSignal(COM_SIG_TX_ESCLOG0_1, &CAN_LOG_DATA[1]);
	Com_SendSignal(COM_SIG_TX_ESCLOG0_2, &CAN_LOG_DATA[2]);
	Com_SendSignal(COM_SIG_TX_ESCLOG0_3, &CAN_LOG_DATA[3]);
	Com_SendSignal(COM_SIG_TX_ESCLOG0_4, &CAN_LOG_DATA[4]);
	Com_SendSignal(COM_SIG_TX_ESCLOG0_5, &CAN_LOG_DATA[5]);
	Com_SendSignal(COM_SIG_TX_ESCLOG0_6, &CAN_LOG_DATA[6]);
	Com_SendSignal(COM_SIG_TX_ESCLOG0_7, &CAN_LOG_DATA[7]);
	Com_SendSignal(COM_SIG_TX_ESCLOG1_0, &CAN_LOG_DATA[8]);
	Com_SendSignal(COM_SIG_TX_ESCLOG1_1, &CAN_LOG_DATA[9]);
	Com_SendSignal(COM_SIG_TX_ESCLOG1_2, &CAN_LOG_DATA[10]);
	Com_SendSignal(COM_SIG_TX_ESCLOG1_3, &CAN_LOG_DATA[11]);
	Com_SendSignal(COM_SIG_TX_ESCLOG1_4, &CAN_LOG_DATA[12]);
	Com_SendSignal(COM_SIG_TX_ESCLOG1_5, &CAN_LOG_DATA[13]);
	Com_SendSignal(COM_SIG_TX_ESCLOG1_6, &CAN_LOG_DATA[14]);
	Com_SendSignal(COM_SIG_TX_ESCLOG1_7, &CAN_LOG_DATA[15]);
	Com_SendSignal(COM_SIG_TX_ESCLOG2_0, &CAN_LOG_DATA[16]);
	Com_SendSignal(COM_SIG_TX_ESCLOG2_1, &CAN_LOG_DATA[17]);
	Com_SendSignal(COM_SIG_TX_ESCLOG2_2, &CAN_LOG_DATA[18]);
	Com_SendSignal(COM_SIG_TX_ESCLOG2_3, &CAN_LOG_DATA[19]);
	Com_SendSignal(COM_SIG_TX_ESCLOG2_4, &CAN_LOG_DATA[20]);
	Com_SendSignal(COM_SIG_TX_ESCLOG2_5, &CAN_LOG_DATA[21]);
	Com_SendSignal(COM_SIG_TX_ESCLOG2_6, &CAN_LOG_DATA[22]);
	Com_SendSignal(COM_SIG_TX_ESCLOG2_7, &CAN_LOG_DATA[23]);
	Com_SendSignal(COM_SIG_TX_ESCLOG3_0, &CAN_LOG_DATA[24]);
	Com_SendSignal(COM_SIG_TX_ESCLOG3_1, &CAN_LOG_DATA[25]);
	Com_SendSignal(COM_SIG_TX_ESCLOG3_2, &CAN_LOG_DATA[26]);
	Com_SendSignal(COM_SIG_TX_ESCLOG3_3, &CAN_LOG_DATA[27]);
	Com_SendSignal(COM_SIG_TX_ESCLOG3_4, &CAN_LOG_DATA[28]);
	Com_SendSignal(COM_SIG_TX_ESCLOG3_5, &CAN_LOG_DATA[29]);
	Com_SendSignal(COM_SIG_TX_ESCLOG3_6, &CAN_LOG_DATA[30]);
	Com_SendSignal(COM_SIG_TX_ESCLOG3_7, &CAN_LOG_DATA[31]);
	Com_SendSignal(COM_SIG_TX_ESCLOG4_0, &CAN_LOG_DATA[32]);
	Com_SendSignal(COM_SIG_TX_ESCLOG4_1, &CAN_LOG_DATA[33]);
	Com_SendSignal(COM_SIG_TX_ESCLOG4_2, &CAN_LOG_DATA[34]);
	Com_SendSignal(COM_SIG_TX_ESCLOG4_3, &CAN_LOG_DATA[35]);
	Com_SendSignal(COM_SIG_TX_ESCLOG4_4, &CAN_LOG_DATA[36]);
	Com_SendSignal(COM_SIG_TX_ESCLOG4_5, &CAN_LOG_DATA[37]);
	Com_SendSignal(COM_SIG_TX_ESCLOG4_6, &CAN_LOG_DATA[38]);
	Com_SendSignal(COM_SIG_TX_ESCLOG4_7, &CAN_LOG_DATA[39]);
	Com_SendSignal(COM_SIG_TX_ESCLOG5_0, &CAN_LOG_DATA[40]);
	Com_SendSignal(COM_SIG_TX_ESCLOG5_1, &CAN_LOG_DATA[41]);
	Com_SendSignal(COM_SIG_TX_ESCLOG5_2, &CAN_LOG_DATA[42]);
	Com_SendSignal(COM_SIG_TX_ESCLOG5_3, &CAN_LOG_DATA[43]);
	Com_SendSignal(COM_SIG_TX_ESCLOG5_4, &CAN_LOG_DATA[44]);
	Com_SendSignal(COM_SIG_TX_ESCLOG5_5, &CAN_LOG_DATA[45]);
	Com_SendSignal(COM_SIG_TX_ESCLOG5_6, &CAN_LOG_DATA[46]);
	Com_SendSignal(COM_SIG_TX_ESCLOG5_7, &CAN_LOG_DATA[47]);
	Com_SendSignal(COM_SIG_TX_ESCLOG6_0, &CAN_LOG_DATA[48]);
	Com_SendSignal(COM_SIG_TX_ESCLOG6_1, &CAN_LOG_DATA[49]);
	Com_SendSignal(COM_SIG_TX_ESCLOG6_2, &CAN_LOG_DATA[50]);
	Com_SendSignal(COM_SIG_TX_ESCLOG6_3, &CAN_LOG_DATA[51]);
	Com_SendSignal(COM_SIG_TX_ESCLOG6_4, &CAN_LOG_DATA[52]);
	Com_SendSignal(COM_SIG_TX_ESCLOG6_5, &CAN_LOG_DATA[53]);
	Com_SendSignal(COM_SIG_TX_ESCLOG6_6, &CAN_LOG_DATA[54]);
	Com_SendSignal(COM_SIG_TX_ESCLOG6_7, &CAN_LOG_DATA[55]);
	Com_SendSignal(COM_SIG_TX_ESCLOG7_0, &CAN_LOG_DATA[56]);
	Com_SendSignal(COM_SIG_TX_ESCLOG7_1, &CAN_LOG_DATA[57]);
	Com_SendSignal(COM_SIG_TX_ESCLOG7_2, &CAN_LOG_DATA[58]);
	Com_SendSignal(COM_SIG_TX_ESCLOG7_3, &CAN_LOG_DATA[59]);
	Com_SendSignal(COM_SIG_TX_ESCLOG7_4, &CAN_LOG_DATA[60]);
	Com_SendSignal(COM_SIG_TX_ESCLOG7_5, &CAN_LOG_DATA[61]);
	Com_SendSignal(COM_SIG_TX_ESCLOG7_6, &CAN_LOG_DATA[62]);
	Com_SendSignal(COM_SIG_TX_ESCLOG7_7, &CAN_LOG_DATA[63]);
	Com_SendSignal(COM_SIG_TX_ESCLOG8_0, &CAN_LOG_DATA[64]);
	Com_SendSignal(COM_SIG_TX_ESCLOG8_1, &CAN_LOG_DATA[65]);
	Com_SendSignal(COM_SIG_TX_ESCLOG8_2, &CAN_LOG_DATA[66]);
	Com_SendSignal(COM_SIG_TX_ESCLOG8_3, &CAN_LOG_DATA[67]);
	Com_SendSignal(COM_SIG_TX_ESCLOG8_4, &CAN_LOG_DATA[68]);
	Com_SendSignal(COM_SIG_TX_ESCLOG8_5, &CAN_LOG_DATA[69]);
	Com_SendSignal(COM_SIG_TX_ESCLOG8_6, &CAN_LOG_DATA[70]);
	Com_SendSignal(COM_SIG_TX_ESCLOG8_7, &CAN_LOG_DATA[71]);
	Com_SendSignal(COM_SIG_TX_ESCLOG9_0, &CAN_LOG_DATA[72]);
	Com_SendSignal(COM_SIG_TX_ESCLOG9_1, &CAN_LOG_DATA[73]);
	Com_SendSignal(COM_SIG_TX_ESCLOG9_2, &CAN_LOG_DATA[74]);
	Com_SendSignal(COM_SIG_TX_ESCLOG9_3, &CAN_LOG_DATA[75]);
	Com_SendSignal(COM_SIG_TX_ESCLOG9_4, &CAN_LOG_DATA[76]);
	Com_SendSignal(COM_SIG_TX_ESCLOG9_5, &CAN_LOG_DATA[77]);
	Com_SendSignal(COM_SIG_TX_ESCLOG9_6, &CAN_LOG_DATA[78]);
	Com_SendSignal(COM_SIG_TX_ESCLOG9_7, &CAN_LOG_DATA[79]);

}

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void PROXY_StartLoggerTrans(void)
{
		PROXY_LoadLoggerData();
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define PROXY_STOP_SEC_CODE
#include "PROXY_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
