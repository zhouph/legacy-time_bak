/**
 * @defgroup Proxy_DecodeByCom Proxy_DecodeByCom
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Proxy_DecodeByCom.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Proxy_Decode.h"
#include "Proxy_DecodeByCom.h"
#include "Com.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PROXY_START_SEC_CONST_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PROXY_STOP_SEC_CONST_UNSPECIFIED
#include "Proxy_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLES DEFINITIONS
 =============================================================================*/
extern Proxy_RxByCom_HdrBusType Proxy_RxByComBus;
extern Proxy_TxByCom_HdrBusType Proxy_TxByComBus;

uint8 ccu1SasCSFault;
uint8 Proxy_fcu8LWSCS;

CC_TimeoutVar_t         RxMsg_ToChkCopyOk[CHK_CAN_MAX_NUM];

#define PROXY_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PROXY_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Proxy_MemMap.h"
#define PROXY_START_SEC_VAR_NOINIT_32BIT
#include "Proxy_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PROXY_STOP_SEC_VAR_NOINIT_32BIT
#include "Proxy_MemMap.h"
#define PROXY_START_SEC_VAR_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PROXY_STOP_SEC_VAR_UNSPECIFIED
#include "Proxy_MemMap.h"
#define PROXY_START_SEC_VAR_32BIT
#include "Proxy_MemMap.h"
/** Variable Section (32BIT)**/


#define PROXY_STOP_SEC_VAR_32BIT
#include "Proxy_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLES DEFINITIONS
 =============================================================================*/
 static uint8_t cu8ClrFltVarblCh;

#define PROXY_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

#define PROXY_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Proxy_MemMap.h"
#define PROXY_START_SEC_VAR_NOINIT_32BIT
#include "Proxy_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PROXY_STOP_SEC_VAR_NOINIT_32BIT
#include "Proxy_MemMap.h"
#define PROXY_START_SEC_VAR_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PROXY_STOP_SEC_VAR_UNSPECIFIED
#include "Proxy_MemMap.h"
#define PROXY_START_SEC_VAR_32BIT
#include "Proxy_MemMap.h"
/** Variable Section (32BIT)**/


#define PROXY_STOP_SEC_VAR_32BIT
#include "Proxy_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PROXY_START_SEC_CODE
#include "Proxy_MemMap.h"

static void Proxy_RxBms1(void);
static void Proxy_RxYawSerial(void);
static void Proxy_RxYawAcc(void);
static void Proxy_RxTcu6(void);
static void Proxy_RxTcu5(void);
static void Proxy_RxTcu1(void);
static void Proxy_RxSas(void);
static void Proxy_RxMcu2(void);
static void Proxy_RxMcu1(void);
static void Proxy_RxLongAcc(void);
static void Proxy_RxHcu5(void);
static void Proxy_RxHcu3(void);
static void Proxy_RxHcu2(void);
static void Proxy_RxHcu1(void);
static void Proxy_RxFatc1(void);
static void Proxy_RxEms3(void);
static void Proxy_RxEms2(void);
static void Proxy_RxEms1(void);
static void Proxy_RxClu2(void);
static void Proxy_RxClu1(void);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

void Proxy_RxProcByCom(void)
{
    Proxy_RxBms1();
    Proxy_RxYawSerial();
    Proxy_RxYawAcc();
    Proxy_RxTcu6();
    Proxy_RxTcu5();
    Proxy_RxTcu1();
    Proxy_RxSas();
    Proxy_RxMcu2();
    Proxy_RxMcu1();
    Proxy_RxLongAcc();
    Proxy_RxHcu5();
    Proxy_RxHcu3();
    Proxy_RxHcu2();
    Proxy_RxHcu1();
    Proxy_RxFatc1();
    Proxy_RxEms3();
    Proxy_RxEms2();
    Proxy_RxEms1();
    Proxy_RxClu2();
    Proxy_RxClu1();
}

void Proxy_SetRxOK(Proxy_MsgIdType MsgId)
{
  RxMsg_ToChkCopyOk[MsgId].u1RxOkFlg=1;

  switch (MsgId)
  {
  case CAN_FRAME_EMS1_MSG_RX:
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.Ems1MsgOkFlg = 1;
    break;
  case CAN_FRAME_EMS2_MSG_RX:
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.Ems2MsgOkFlg = 1;
    break;
  case CAN_FRAME_TCU1_MSG_RX:
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.Tcu1MsgOkFlg = 1;
    break;
  case CAN_FRAME_TCU5_MSG_RX:
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.Tcu5MsgOkFlg = 1;
    break;
  case CAN_FRAME_TCU6_MSG_RX:
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.Tcu6MsgOkFlg = 1;
    break;
  case CAN_FRAME_HCU1_MSG_RX:
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.Hcu1MsgOkFlg = 1;
    break;
  case CAN_FRAME_HCU2_MSG_RX:
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.Hcu2MsgOkFlg = 1;
    break;
  case CAN_FRAME_HCU3_MSG_RX:
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.Hcu3MsgOkFlg = 1;
    break;
  case CAN_FRAME_HCU5_MSG_RX:
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.Hcu5MsgOkFlg = 1;
    break;
  case CAN_FRAME_MCU1_MSG_RX:
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.Mcu1MsgOkFlg = 1;
    break;
  case CAN_FRAME_MCU2_MSG_RX:
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.Mcu2MsgOkFlg = 1;
    break;
  case CAN_FRAME_BMS1_MSG_RX:
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.Bms1MsgOkFlg = 1;
    break;
  case CAN_FRAME_SAS1_MSG_RX:
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.SasMsgOkFlg = 1;
    break;
  case CAN_FRAME_FATC1_MSG_RX:
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.Fatc1MsgOkFlg = 1;
    break;
  case CAN_FRAME_CLU1_MSG_RX:
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.Clu1MsgOkFlg = 1;
    break;
  case CAN_FRAME_EMS3_MSG_RX:
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.Ems3MsgOkFlg = 1;
    break;
  case CAN_FRAME_CLU2_MSG_RX:
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.Clu2MsgOkFlg = 1;
    break;
  case CAN_FRAME_YAWACC_MSG_RX:
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.YawAccMsgOkFlg = 1;
    break;
  case CAN_FRAME_LONGACC_MSG_RX:
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.LongAccMsgOkFlg = 1; 
    break;
  case CAN_FRAME_YAWSERIAL_MSG_RX:
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.YawSerialMsgOkFlg = 1;
    break;
  case CAN_FRAME_EPB1_MSG_RX:
  case CAN_FRAME_CGW1_MSG_RX:
  case CAN_FRAME_TCU2_MSG_RX:
  case CAN_FRAME_EMS6_MSG_RX:
  case CAN_FRAME_VSM2_MSG_RX:
  case CAN_FRAME_DIAG_FUNC_MSG_RX:
  case CAN_FRAME_DIAG_PHY_MSG_RX:
    break;
  }
}

void Proxy_SetRxCopyOK(Proxy_MsgIdType MsgId)
{
  RxMsg_ToChkCopyOk[MsgId].u1RxCopyOkFlg=1;
}

void Proxy_CanIfRxIndication(Can_IdType RxPduId, PduInfoType* PduInfoPtr)
{
  uint8 i;
  if ((RxPduId == 0x7D1) || (RxPduId == 0x7DF))
  {
    Diag_CanIfRxIndication(RxPduId,PduInfoPtr->SduLength,PduInfoPtr->SduDataPtr);
  }
  else
  {
    ;
  }

  for (i=0;i<COM_NUMBER_OF_RX_MSG;i++)
  {
    if (Com_FrameConfig[i].MsgId==RxPduId)
    {
      Proxy_SetRxOK(i);
      Proxy_SetRxCopyOK(i);
      Proxy_SetRxOKByAppl(i);
      Proxy_SetRxCopyOKByAppl(i);
      break;
    }
  }
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

static void Proxy_RxBms1(void)
{
    uint8 ComSignalData;
	
    if(RxMsg_ToChkCopyOk[CHK_CAN_BMS1_MSG_RX].u1RxCopyOkFlg==1)
    {
    	RxMsg_ToChkCopyOk[CHK_CAN_BMS1_MSG_RX].u1RxCopyOkFlg=0;

	Com_ReceiveSignal(COM_SIG_RX_BMS1_SOC_PC,&ComSignalData);
	Proxy_RxByComBus.Proxy_RxByComRxBms1Info.Bms1SocPc = ComSignalData;
    }
}

static void Proxy_RxYawSerial(void)
{
  uint8 ComSignalData1,ComSignalData2,ComSignalData3;
  if(RxMsg_ToChkCopyOk[CHK_CAN_YAWSERIAL_MSG_RX].u1RxCopyOkFlg==1)
  {
    Com_ReceiveSignal(COM_SIG_RX_YAWSERIAL_YAWSERIALNUM_0,&ComSignalData1);
    Com_ReceiveSignal(COM_SIG_RX_YAWSERIAL_YAWSERIALNUM_1,&ComSignalData2);
    Com_ReceiveSignal(COM_SIG_RX_YAWSERIAL_YAWSERIALNUM_2,&ComSignalData3);
    
    Proxy_RxByComBus.Proxy_RxByComRxYawSerialInfo.YawSerialNum_0 = ComSignalData1;
    Proxy_RxByComBus.Proxy_RxByComRxYawSerialInfo.YawSerialNum_1 = ComSignalData2;
    Proxy_RxByComBus.Proxy_RxByComRxYawSerialInfo.YawSerialNum_2 = ComSignalData3;

    RxMsg_ToChkCopyOk[CHK_CAN_YAWSERIAL_MSG_RX].u1RxCopyOkFlg=0;
  }
}

static void Proxy_RxYawAcc(void)
{
  uint8 ComSignalData;
  uint8 ComSignalData1,ComSignalData2;

  if(RxMsg_ToChkCopyOk[CHK_CAN_YAWACC_MSG_RX].u1RxCopyOkFlg==1)
  {
    /* Yaw & Lg Sensor*/
    Com_ReceiveSignal(COM_SIG_RX_YAWACC_YAWRATEVAILDDATA,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.YawRateValidData = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_YAWACC_YAWRATESELFTESTSTATUS,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.YawRateSelfTestStatus = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_YAWACC_YAWRATESIGNAL_0,&ComSignalData1);
    Com_ReceiveSignal(COM_SIG_RX_YAWACC_YAWRATESIGNAL_1,&ComSignalData2);
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.YawRateSignal_0 = ComSignalData1;
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.YawRateSignal_1 = ComSignalData2;

    Com_ReceiveSignal(COM_SIG_RX_YAWACC_SENSOROSCFREQDEV,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.SensorOscFreqDev = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_YAWACC_GYRO_FAIL,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.Gyro_Fail = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_YAWACC_RASTER_FAIL,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.Raster_Fail = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_YAWACC_EEP_FAIL,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.Eep_Fail = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_YAWACC_BATT_RANGE_ERR,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.Batt_Range_Err = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_YAWACC_ASIC_FAIL,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.Asic_Fail = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_YAWACC_ACCEL_FAIL,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.Accel_Fail = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_YAWACC_RAM_FAIL,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.Ram_Fail = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_YAWACC_ROM_FAIL,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.Rom_Fail = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_YAWACC_AD_FAIL,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.Ad_Fail = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_YAWACC_OSC_FAIL,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.Osc_Fail = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_YAWACC_WATCHDOG_RST,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.Watchdog_Rst = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_YAWACC_PLAUS_ERR_PST,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.Plaus_Err_Pst = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_YAWACC_ROLLINGCOUNTER,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.RollingCounter = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_YAWACC_CAN_FUNC_ERR,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.Can_Func_Err = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_YAWACC_ACCELERATORRATESIG_0,&ComSignalData1);
    Com_ReceiveSignal(COM_SIG_RX_YAWACC_ACCELERATORRATESIG_1,&ComSignalData2);
    
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.AccelEratorRateSig_0 = ComSignalData1;
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.AccelEratorRateSig_1 = ComSignalData2;

    Com_ReceiveSignal(COM_SIG_RX_YAWACC_LATACCVALIDDATA,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.LatAccValidData = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_YAWACC_LATACCSELFTESTSTATUS,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.LatAccSelfTestStatus = ComSignalData;
    
    RxMsg_ToChkCopyOk[CHK_CAN_YAWACC_MSG_RX].u1RxCopyOkFlg=0;
  }
}
    
static void Proxy_RxTcu6(void)
{
  uint8 ComSignalData;

  if(RxMsg_ToChkCopyOk[CHK_CAN_TCU6_MSG_RX].u1RxCopyOkFlg==1)
  {
    RxMsg_ToChkCopyOk[CHK_CAN_TCU6_MSG_RX].u1RxCopyOkFlg = 0;
    
    Com_ReceiveSignal(COM_SIG_RX_TCU6_SHIFTCLASS_CCAN,&ComSignalData);
    
    Proxy_RxByComBus.Proxy_RxByComRxTcu6Info.ShiftClass_Ccan = ComSignalData;
  }    
}

static void Proxy_RxTcu5(void)
{
  uint8 ComSignalData;
  
  if(RxMsg_ToChkCopyOk[CHK_CAN_TCU5_MSG_RX].u1RxCopyOkFlg==1)
  {
    RxMsg_ToChkCopyOk[CHK_CAN_TCU5_MSG_RX].u1RxCopyOkFlg = 0;
    
    Com_ReceiveSignal(COM_SIG_RX_TCU5_TYP,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxTcu5Info.Typ = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_TCU5_GEARTYP,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxTcu5Info.GearTyp = ComSignalData;
  }    
}

static void Proxy_RxTcu1(void)
{
  uint8 ComSignalData;
  uint16 ComSignalData1;

  if(RxMsg_ToChkCopyOk[CHK_CAN_TCU1_MSG_RX].u1RxCopyOkFlg==1)
  {
    RxMsg_ToChkCopyOk[CHK_CAN_TCU1_MSG_RX].u1RxCopyOkFlg=0;

    Com_ReceiveSignal(COM_SIG_RX_TCU1_TARGE,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxTcu1Info.Targe = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_TCU1_GARCHANGE,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxTcu1Info.GarChange = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_TCU1_FLT,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxTcu1Info.Flt = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_TCU1_GARSELDISP,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxTcu1Info.GarSelDisp = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_TCU1_TQREDREQ_PC,&ComSignalData1);
    Proxy_RxByComBus.Proxy_RxByComRxTcu1Info.TQRedReq_PC = ComSignalData1;

    Com_ReceiveSignal(COM_SIG_RX_TCU1_TQREDREQSLW_PC,&ComSignalData1);
    Proxy_RxByComBus.Proxy_RxByComRxTcu1Info.TQRedReqSlw_PC = ComSignalData1;
    
    Com_ReceiveSignal(COM_SIG_RX_TCU1_TQINCREQ_PC,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxTcu1Info.TQIncReq_PC = ComSignalData;
  }    
}

static void Proxy_RxSas(void)
{
  uint8 ComSignalData;
  uint16 ComSignalData1;
   
  uint8_t ccu8_RxSASCs, ccu8_CalcedSASCs;
  if(RxMsg_ToChkCopyOk[CHK_CAN_SAS1_MSG_RX].u1RxCopyOkFlg==1)
  {
    /* SAS Sensor */
    Com_ReceiveSignal(COM_SIG_RX_SAS1_ANGLE,&ComSignalData1);
    Proxy_RxByComBus.Proxy_RxByComRxSasInfo.Angle = ComSignalData1;
  	
    Com_ReceiveSignal(COM_SIG_RX_SAS1_SPEED,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxSasInfo.Speed = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_SAS1_OK,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxSasInfo.Ok = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_SAS1_CAL,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxSasInfo.Cal = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_SAS1_TRIM,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxSasInfo.Trim = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_SAS1_CHECKSUM,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxSasInfo.CheckSum = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_SAS1_MSGCOUNT,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxSasInfo.MsgCount = ComSignalData;

    Proxy_fcu8LWSCS=((uint8)(Com_FrameBuf[CAN_FRAME_SAS1_MSG_RX].Data.B[0])^\
  	  	     (uint8_t)(Com_FrameBuf[CAN_FRAME_SAS1_MSG_RX].Data.B[1])^\
		     (uint8_t)(Com_FrameBuf[CAN_FRAME_SAS1_MSG_RX].Data.B[2])^\
		     (uint8_t)(Com_FrameBuf[CAN_FRAME_SAS1_MSG_RX].Data.B[3])^\
		     ((uint8_t)Com_FrameBuf[CAN_FRAME_SAS1_MSG_RX].Data.B[4]&0x0F));

    RxMsg_ToChkCopyOk[CHK_CAN_SAS1_MSG_RX].u1RxCopyOkFlg=0;

    ccu8_RxSASCs =(uint8_t)Proxy_RxByComBus.Proxy_RxByComRxSasInfo.CheckSum;
    ccu8_CalcedSASCs=(uint8_t)(((Proxy_fcu8LWSCS&0x0f)^((Proxy_fcu8LWSCS&(uint8_t)0xf0)>>4))&0x0f);

    if(ccu8_RxSASCs!=ccu8_CalcedSASCs)
    {
       ccu1SasCSFault=1;
    }
    else
    {
       ccu1SasCSFault=0;
    }
  }
}

static void Proxy_RxMcu2(void)
{
  uint8 ComSignalData;
	
  if(RxMsg_ToChkCopyOk[CHK_CAN_MCU2_MSG_RX].u1RxCopyOkFlg==1)
  {
      RxMsg_ToChkCopyOk[CHK_CAN_MCU2_MSG_RX].u1RxCopyOkFlg=0;
      Com_ReceiveSignal(COM_SIG_RX_MCU2_FLT,&ComSignalData);
      Proxy_RxByComBus.Proxy_RxByComRxMcu2Info.Flt = ComSignalData;
  }
}

static void Proxy_RxMcu1(void)
{
  uint16 ComSignalData;
  if(RxMsg_ToChkCopyOk[CHK_CAN_MCU1_MSG_RX].u1RxCopyOkFlg==1)
  {
    RxMsg_ToChkCopyOk[CHK_CAN_MCU1_MSG_RX].u1RxCopyOkFlg=0;
    
    Com_ReceiveSignal(COM_SIG_RX_MCU1_MOTESTTQ_PC,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxMcu1Info.MoTestTQ_PC = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_MCU1_MOTACTROTSPD_RPM,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxMcu1Info.MotActRotSpd_RPM = ComSignalData;
  }    
}

static void Proxy_RxLongAcc(void)
{
    uint8 ComSignalData;
    uint8 ComSignalData1,ComSignalData2;

    if(RxMsg_ToChkCopyOk[CHK_CAN_LONGACC_MSG_RX].u1RxCopyOkFlg==1)
    {
      Com_ReceiveSignal(COM_SIG_RX_LONGACC_INTSENFLTSYMTMACTIVE,&ComSignalData);
      Proxy_RxByComBus.Proxy_RxByComRxLongAccInfo.IntSenFltSymtmActive = ComSignalData;

      Com_ReceiveSignal(COM_SIG_RX_LONGACC_INTSENFAULTPRESENT,&ComSignalData);
      Proxy_RxByComBus.Proxy_RxByComRxLongAccInfo.IntSenFaultPresent = ComSignalData;

      Com_ReceiveSignal(COM_SIG_RX_LONGACC_LONGACCSENCIRERRPRE,&ComSignalData);
      Proxy_RxByComBus.Proxy_RxByComRxLongAccInfo.LongAccSenCirErrPre = ComSignalData;

      Com_ReceiveSignal(COM_SIG_RX_LONGACC_LONACSENRANCHKERRPRE,&ComSignalData);
      Proxy_RxByComBus.Proxy_RxByComRxLongAccInfo.LonACSenRanChkErrPre = ComSignalData;

      Com_ReceiveSignal(COM_SIG_RX_LONGACC_LONGROLLINGCOUNTER,&ComSignalData);
      Proxy_RxByComBus.Proxy_RxByComRxLongAccInfo.LongRollingCounter = ComSignalData;

      Com_ReceiveSignal(COM_SIG_RX_LONGACC_INTTEMPSENSORFAULT,&ComSignalData);
      Proxy_RxByComBus.Proxy_RxByComRxLongAccInfo.IntTempSensorFault = ComSignalData;

      Com_ReceiveSignal(COM_SIG_RX_LONGACC_LONGACCINVALIDDATA,&ComSignalData);
      Proxy_RxByComBus.Proxy_RxByComRxLongAccInfo.LongAccInvalidData = ComSignalData;

      Com_ReceiveSignal(COM_SIG_RX_LONGACC_LONGACCSELFTSTSTATUS,&ComSignalData);
      Proxy_RxByComBus.Proxy_RxByComRxLongAccInfo.LongAccSelfTstStatus = ComSignalData;

      Com_ReceiveSignal(COM_SIG_RX_LONGACC_LONGACCRATESIGNAL_0,&ComSignalData1);
      Com_ReceiveSignal(COM_SIG_RX_LONGACC_LONGACCRATESIGNAL_1,&ComSignalData2);
      Proxy_RxByComBus.Proxy_RxByComRxLongAccInfo.LongAccRateSignal_0 = ComSignalData1;
      Proxy_RxByComBus.Proxy_RxByComRxLongAccInfo.LongAccRateSignal_1 = ComSignalData2;

      RxMsg_ToChkCopyOk[CHK_CAN_LONGACC_MSG_RX].u1RxCopyOkFlg=0;
    }
}

static void Proxy_RxHcu5(void)
{
  uint8 ComSignalData;
  if(RxMsg_ToChkCopyOk[CHK_CAN_HCU5_MSG_RX].u1RxCopyOkFlg==1)
  {
     RxMsg_ToChkCopyOk[CHK_CAN_HCU5_MSG_RX].u1RxCopyOkFlg=0;
     Com_ReceiveSignal(COM_SIG_RX_HCU5_HEVMOD,&ComSignalData);
     Proxy_RxByComBus.Proxy_RxByComRxHcu5Info.HevMod = ComSignalData;
  }
}

static void Proxy_RxHcu3(void)
{
  uint16 ComSignalData;
	
  if(RxMsg_ToChkCopyOk[CHK_CAN_HCU3_MSG_RX].u1RxCopyOkFlg==1)
  {
    RxMsg_ToChkCopyOk[CHK_CAN_HCU3_MSG_RX].u1RxCopyOkFlg=0;

    Com_ReceiveSignal(COM_SIG_RX_HCU3_TMINTQCMDBINV_PC,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxHcu3Info.TmIntQcMDBINV_PC = ComSignalData;
    
    Com_ReceiveSignal(COM_SIG_RX_HCU3_MOTTQCMC_PC,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxHcu3Info.MotTQCMC_PC = ComSignalData;
    
    Com_ReceiveSignal(COM_SIG_RX_HCU3_MOTTQCMDBINV_PC,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxHcu3Info.MotTQCMDBINV_PC = ComSignalData;
  }    
}

static void Proxy_RxHcu2(void)
{
  uint8 ComSignalData;
  uint16 ComSignalData1;
  if(RxMsg_ToChkCopyOk[CHK_CAN_HCU2_MSG_RX].u1RxCopyOkFlg==1)
  {
    RxMsg_ToChkCopyOk[CHK_CAN_HCU2_MSG_RX].u1RxCopyOkFlg=0;

    Com_ReceiveSignal(COM_SIG_RX_HCU2_SERIVCEMOD,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxHcu2Info.ServiceMod = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_HCU2_REGENENA,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxHcu2Info.RegenENA = ComSignalData;
    
    Com_ReceiveSignal(COM_SIG_RX_HCU2_REGENBRKTQ_NM,&ComSignalData1);
    Proxy_RxByComBus.Proxy_RxByComRxHcu2Info.RegenBRKTQ_NM = ComSignalData1;

    Com_ReceiveSignal(COM_SIG_RX_HCU2_CRPTQ_NM,&ComSignalData1);
    Proxy_RxByComBus.Proxy_RxByComRxHcu2Info.CrpTQ_NM = ComSignalData1;

    Com_ReceiveSignal(COM_SIG_RX_HCU2_WHLDEMTQ_NM,&ComSignalData1);
    Proxy_RxByComBus.Proxy_RxByComRxHcu2Info.WhlDEMTQ_NM = ComSignalData1;
  }    
}

static void Proxy_RxHcu1(void)
{
  uint8 ComSignalData;
  if(RxMsg_ToChkCopyOk[CHK_CAN_HCU1_MSG_RX].u1RxCopyOkFlg==1)
  {
    RxMsg_ToChkCopyOk[CHK_CAN_HCU1_MSG_RX].u1RxCopyOkFlg=0;
    Com_ReceiveSignal(COM_SIG_RX_HCU1_ENGCLTSTAT,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxHcu1Info.EngCltStat = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_HCU1_HEVRDY,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxHcu1Info.HEVRDY = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_HCU1_ENGTQCMDBINV_PC,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxHcu1Info.EngTQCmdBinV_PC = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_HCU1_ENGTQCMD_PC,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxHcu1Info.EngTQCmd_PC = ComSignalData;
  }    
}

static void Proxy_RxFatc1(void)
{
  uint8 ComSignalData;

  if(RxMsg_ToChkCopyOk[CHK_CAN_FATC1_MSG_RX].u1RxCopyOkFlg==1)
  {
    RxMsg_ToChkCopyOk[CHK_CAN_FATC1_MSG_RX].u1RxCopyOkFlg = 0;
  	
    Com_ReceiveSignal(COM_SIG_RX_FATC1_OUTTEMP_SNR_C,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxFact1Info.OutTemp_SNR_C = ComSignalData;
  }    
}

static void Proxy_RxEms3(void)
{
  uint8 ComSignalData;
 
  if(RxMsg_ToChkCopyOk[CHK_CAN_EMS3_MSG_RX].u1RxCopyOkFlg==1)
  {
    RxMsg_ToChkCopyOk[CHK_CAN_EMS3_MSG_RX].u1RxCopyOkFlg=0;

    Com_ReceiveSignal(COM_SIG_RX_EMS3_ENGCOLTEMP_C,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxEms3Info.EngColTemp_C = ComSignalData;    
  }    
}

static void Proxy_RxEms2(void)
{
  uint8 ComSignalData;

  if(RxMsg_ToChkCopyOk[CHK_CAN_EMS2_MSG_RX].u1RxCopyOkFlg==1)
  {
    RxMsg_ToChkCopyOk[CHK_CAN_EMS2_MSG_RX].u1RxCopyOkFlg=0;

    Com_ReceiveSignal(COM_SIG_RX_EMS2_ENGSPDERR,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxEms2Info.EngSpdErr = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_EMS2_ACCPEDDEP_PC,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxEms2Info.AccPedDep_PC = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_EMS2_TPS_PC,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxEms2Info.Tps_PC = ComSignalData;
  }
}

static void Proxy_RxEms1(void)
{
  uint8 ComSignalData;
  uint16 ComSignalData1;

  if(RxMsg_ToChkCopyOk[CHK_CAN_EMS1_MSG_RX].u1RxCopyOkFlg==1)
  {
    RxMsg_ToChkCopyOk[CHK_CAN_EMS1_MSG_RX].u1RxCopyOkFlg=0;

    Com_ReceiveSignal(COM_SIG_RX_EMS1_TQSTD_NM,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxEms1Info.TqStd_NM = ComSignalData;
    
    Com_ReceiveSignal(COM_SIG_RX_EMS1_ACTINDTQ_PC,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxEms1Info.ActINDTQ_PC = ComSignalData;
    
    Com_ReceiveSignal(COM_SIG_RX_EMS1_ENGSPD_RPM,&ComSignalData1);
    Proxy_RxByComBus.Proxy_RxByComRxEms1Info.EngSpd_RPM = ComSignalData1;
    
    Com_ReceiveSignal(COM_SIG_RX_EMS1_INDTQ_PC,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxEms1Info.IndTQ_PC = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_EMS1_FRICTTQ_PC,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxEms1Info.FrictTQ_PC = ComSignalData;
  }    
}

static void Proxy_RxClu2(void)
{
  uint8 ComSignalData;

  if(RxMsg_ToChkCopyOk[CHK_CAN_CLU2_MSG_RX].u1RxCopyOkFlg==1)
  {
    RxMsg_ToChkCopyOk[CHK_CAN_CLU2_MSG_RX].u1RxCopyOkFlg = 0;

    Com_ReceiveSignal(COM_SIG_RX_CLU2_IGN_SW,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxClu2Info.IGN_SW = ComSignalData;
  }
}

static void Proxy_RxClu1(void)
{
  uint8 ComSignalData;

  if(RxMsg_ToChkCopyOk[CHK_CAN_CLU1_MSG_RX].u1RxCopyOkFlg==1)
  {
    RxMsg_ToChkCopyOk[CHK_CAN_CLU1_MSG_RX].u1RxCopyOkFlg = 0;

    Com_ReceiveSignal(COM_SIG_RX_CLU1_P_BRAKE_ACT,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxClu1Info.P_Brake_Act = ComSignalData;

    Com_ReceiveSignal(COM_SIG_RX_CLU1_CF_CLU_BRAKEFLUIDSW,&ComSignalData);
    Proxy_RxByComBus.Proxy_RxByComRxClu1Info.Cf_Clu_BrakeFluIDSW = ComSignalData;
  }
}

#define PROXY_STOP_SEC_CODE
#include "Proxy_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */

