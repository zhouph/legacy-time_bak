/**
 * @defgroup Proxy_TxByCom Proxy_TxByCom
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Proxy_TxByCom.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Proxy_TxByCom.h"
#include "Proxy_TxByCom_Ifa.h"
#include "IfxStm_reg.h"
//LEJ
#include "TimE.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PROXY_TXBYCOM_START_SEC_CONST_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PROXY_TXBYCOM_STOP_SEC_CONST_UNSPECIFIED
#include "Proxy_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PROXY_TXBYCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Proxy_TxByCom_HdrBusType Proxy_TxByComBus;

/* Version Info */
const SwcVersionInfo_t Proxy_TxByComVersionInfo = 
{   
    PROXY_TXBYCOM_MODULE_ID,           /* Proxy_TxByComVersionInfo.ModuleId */
    PROXY_TXBYCOM_MAJOR_VERSION,       /* Proxy_TxByComVersionInfo.MajorVer */
    PROXY_TXBYCOM_MINOR_VERSION,       /* Proxy_TxByComVersionInfo.MinorVer */
    PROXY_TXBYCOM_PATCH_VERSION,       /* Proxy_TxByComVersionInfo.PatchVer */
    PROXY_TXBYCOM_BRANCH_VERSION       /* Proxy_TxByComVersionInfo.BranchVer */
};
    
/* Input Data Element */
Proxy_TxTxYawCbitInfo_t Proxy_TxByComTxYawCbitInfo;
Proxy_TxTxWhlSpdInfo_t Proxy_TxByComTxWhlSpdInfo;
Proxy_TxTxWhlPulInfo_t Proxy_TxByComTxWhlPulInfo;
Proxy_TxTxTcs5Info_t Proxy_TxByComTxTcs5Info;
Proxy_TxTxTcs1Info_t Proxy_TxByComTxTcs1Info;
Proxy_TxTxSasCalInfo_t Proxy_TxByComTxSasCalInfo;
Proxy_TxTxEsp2Info_t Proxy_TxByComTxEsp2Info;
Proxy_TxTxEbs1Info_t Proxy_TxByComTxEbs1Info;
Proxy_TxTxAhb1Info_t Proxy_TxByComTxAhb1Info;
SenPwrM_MainSenPwrMonitor_t Proxy_TxByComSenPwrMonitorData;
Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo_t Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo;
Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo_t Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo;
Proxy_TxAVL_BRTORQ_WHLInfo_t Proxy_TxByComAVL_BRTORQ_WHLInfo;
Proxy_TxAVL_RPM_WHLInfo_t Proxy_TxByComAVL_RPM_WHLInfo;
Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo_t Proxy_TxByComRQ_WMOM_PT_SUM_RECUPInfo;
Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info_t Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info;
Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo_t Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo;
Proxy_TxAVL_QUAN_EES_WHLInfo_t Proxy_TxByComAVL_QUAN_EES_WHLInfo;
Proxy_TxTAR_LTRQD_BAXInfo_t Proxy_TxByComTAR_LTRQD_BAXInfo;
Proxy_TxST_YMRInfo_t Proxy_TxByComST_YMRInfo;
Proxy_TxWEGSTRECKEInfo_t Proxy_TxByComWEGSTRECKEInfo;
Proxy_TxDISP_CC_DRDY_00Info_t Proxy_TxByComDISP_CC_DRDY_00Info;
Proxy_TxDISP_CC_BYPA_00Info_t Proxy_TxByComDISP_CC_BYPA_00Info;
Proxy_TxST_VHSSInfo_t Proxy_TxByComST_VHSSInfo;
Proxy_TxDIAG_OBD_HYB_1Info_t Proxy_TxByComDIAG_OBD_HYB_1Info;
Proxy_TxSU_DSCInfo_t Proxy_TxByComSU_DSCInfo;
Proxy_TxST_TYR_RDCInfo_t Proxy_TxByComST_TYR_RDCInfo;
Proxy_TxST_TYR_RPAInfo_t Proxy_TxByComST_TYR_RPAInfo;
Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info_t Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info;
Proxy_TxST_TYR_2Info_t Proxy_TxByComST_TYR_2Info;
Proxy_TxAVL_T_TYRInfo_t Proxy_TxByComAVL_T_TYRInfo;
Proxy_TxTEMP_BRKInfo_t Proxy_TxByComTEMP_BRKInfo;
Proxy_TxAVL_P_TYRInfo_t Proxy_TxByComAVL_P_TYRInfo;
Proxy_TxFR_DBG_DSCInfo_t Proxy_TxByComFR_DBG_DSCInfo;

/* Output Data Element */

uint32 Proxy_TxByCom_Timer_Start;
uint32 Proxy_TxByCom_Timer_Elapsed;

#define PROXY_TXBYCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Proxy_MemMap.h"
#define PROXY_TXBYCOM_START_SEC_VAR_NOINIT_32BIT
#include "Proxy_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PROXY_TXBYCOM_STOP_SEC_VAR_NOINIT_32BIT
#include "Proxy_MemMap.h"
#define PROXY_TXBYCOM_START_SEC_VAR_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PROXY_TXBYCOM_STOP_SEC_VAR_UNSPECIFIED
#include "Proxy_MemMap.h"
#define PROXY_TXBYCOM_START_SEC_VAR_32BIT
#include "Proxy_MemMap.h"
/** Variable Section (32BIT)**/


#define PROXY_TXBYCOM_STOP_SEC_VAR_32BIT
#include "Proxy_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PROXY_TXBYCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PROXY_TXBYCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Proxy_MemMap.h"
#define PROXY_TXBYCOM_START_SEC_VAR_NOINIT_32BIT
#include "Proxy_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PROXY_TXBYCOM_STOP_SEC_VAR_NOINIT_32BIT
#include "Proxy_MemMap.h"
#define PROXY_TXBYCOM_START_SEC_VAR_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PROXY_TXBYCOM_STOP_SEC_VAR_UNSPECIFIED
#include "Proxy_MemMap.h"
#define PROXY_TXBYCOM_START_SEC_VAR_32BIT
#include "Proxy_MemMap.h"
/** Variable Section (32BIT)**/


#define PROXY_TXBYCOM_STOP_SEC_VAR_32BIT
#include "Proxy_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PROXY_TXBYCOM_START_SEC_CODE
#include "Proxy_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Proxy_TxByCom_Init(void)
{
    /* Initialize internal bus */
    Proxy_TxByComBus.Proxy_TxByComTxYawCbitInfo.TestYawRateSensor = 0;
    Proxy_TxByComBus.Proxy_TxByComTxYawCbitInfo.TestAcclSensor = 0;
    Proxy_TxByComBus.Proxy_TxByComTxYawCbitInfo.YawSerialNumberReq = 0;
    Proxy_TxByComBus.Proxy_TxByComTxWhlSpdInfo.WhlSpdFL = 0;
    Proxy_TxByComBus.Proxy_TxByComTxWhlSpdInfo.WhlSpdFR = 0;
    Proxy_TxByComBus.Proxy_TxByComTxWhlSpdInfo.WhlSpdRL = 0;
    Proxy_TxByComBus.Proxy_TxByComTxWhlSpdInfo.WhlSpdRR = 0;
    Proxy_TxByComBus.Proxy_TxByComTxWhlPulInfo.WhlPulFL = 0;
    Proxy_TxByComBus.Proxy_TxByComTxWhlPulInfo.WhlPulFR = 0;
    Proxy_TxByComBus.Proxy_TxByComTxWhlPulInfo.WhlPulRL = 0;
    Proxy_TxByComBus.Proxy_TxByComTxWhlPulInfo.WhlPulRR = 0;
    Proxy_TxByComBus.Proxy_TxByComTxTcs5Info.CfBrkAbsWLMP = 0;
    Proxy_TxByComBus.Proxy_TxByComTxTcs5Info.CfBrkEbdWLMP = 0;
    Proxy_TxByComBus.Proxy_TxByComTxTcs5Info.CfBrkTcsWLMP = 0;
    Proxy_TxByComBus.Proxy_TxByComTxTcs5Info.CfBrkTcsFLMP = 0;
    Proxy_TxByComBus.Proxy_TxByComTxTcs5Info.CfBrkAbsDIAG = 0;
    Proxy_TxByComBus.Proxy_TxByComTxTcs5Info.CrBrkWheelFL_KMH = 0;
    Proxy_TxByComBus.Proxy_TxByComTxTcs5Info.CrBrkWheelFR_KMH = 0;
    Proxy_TxByComBus.Proxy_TxByComTxTcs5Info.CrBrkWheelRL_KMH = 0;
    Proxy_TxByComBus.Proxy_TxByComTxTcs5Info.CrBrkWheelRR_KMH = 0;
    Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CfBrkTcsREQ = 0;
    Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CfBrkTcsPAS = 0;
    Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CfBrkTcsDEF = 0;
    Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CfBrkTcsCTL = 0;
    Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CfBrkAbsACT = 0;
    Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CfBrkAbsDEF = 0;
    Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CfBrkEbdDEF = 0;
    Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CfBrkTcsGSC = 0;
    Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CfBrkEspPAS = 0;
    Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CfBrkEspDEF = 0;
    Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CfBrkEspCTL = 0;
    Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CfBrkMsrREQ = 0;
    Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CfBrkTcsMFRN = 0;
    Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CfBrkMinGEAR = 0;
    Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CfBrkMaxGEAR = 0;
    Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.BrakeLight = 0;
    Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.HacCtl = 0;
    Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.HacPas = 0;
    Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.HacDef = 0;
    Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CrBrkTQI_PC = 0;
    Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CrBrkTQIMSR_PC = 0;
    Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CrBrkTQISLW_PC = 0;
    Proxy_TxByComBus.Proxy_TxByComTxTcs1Info.CrEbsMSGCHKSUM = 0;
    Proxy_TxByComBus.Proxy_TxByComTxSasCalInfo.CalSas_CCW = 0;
    Proxy_TxByComBus.Proxy_TxByComTxSasCalInfo.CalSas_Lws_CID = 0;
    Proxy_TxByComBus.Proxy_TxByComTxEsp2Info.LatAccel = 0;
    Proxy_TxByComBus.Proxy_TxByComTxEsp2Info.LatAccelStat = 0;
    Proxy_TxByComBus.Proxy_TxByComTxEsp2Info.LatAccelDiag = 0;
    Proxy_TxByComBus.Proxy_TxByComTxEsp2Info.LongAccel = 0;
    Proxy_TxByComBus.Proxy_TxByComTxEsp2Info.LongAccelStat = 0;
    Proxy_TxByComBus.Proxy_TxByComTxEsp2Info.LongAccelDiag = 0;
    Proxy_TxByComBus.Proxy_TxByComTxEsp2Info.YawRate = 0;
    Proxy_TxByComBus.Proxy_TxByComTxEsp2Info.YawRateStat = 0;
    Proxy_TxByComBus.Proxy_TxByComTxEsp2Info.YawRateDiag = 0;
    Proxy_TxByComBus.Proxy_TxByComTxEsp2Info.CylPres = 0;
    Proxy_TxByComBus.Proxy_TxByComTxEsp2Info.CylPresStat = 0;
    Proxy_TxByComBus.Proxy_TxByComTxEsp2Info.CylPresDiag = 0;
    Proxy_TxByComBus.Proxy_TxByComTxEbs1Info.CfBrkEbsStat = 0;
    Proxy_TxByComBus.Proxy_TxByComTxEbs1Info.CrBrkRegenTQLimit_NM = 0;
    Proxy_TxByComBus.Proxy_TxByComTxEbs1Info.CrBrkEstTot_NM = 0;
    Proxy_TxByComBus.Proxy_TxByComTxEbs1Info.CfBrkSnsFail = 0;
    Proxy_TxByComBus.Proxy_TxByComTxEbs1Info.CfBrkRbsWLamp = 0;
    Proxy_TxByComBus.Proxy_TxByComTxEbs1Info.CfBrkVacuumSysDef = 0;
    Proxy_TxByComBus.Proxy_TxByComTxEbs1Info.CrBrkEstHyd_NM = 0;
    Proxy_TxByComBus.Proxy_TxByComTxEbs1Info.CrBrkStkDep_PC = 0;
    Proxy_TxByComBus.Proxy_TxByComTxEbs1Info.CfBrkPgmRun1 = 0;
    Proxy_TxByComBus.Proxy_TxByComTxAhb1Info.CfAhbWLMP = 0;
    Proxy_TxByComBus.Proxy_TxByComTxAhb1Info.CfAhbDiag = 0;
    Proxy_TxByComBus.Proxy_TxByComTxAhb1Info.CfAhbAct = 0;
    Proxy_TxByComBus.Proxy_TxByComTxAhb1Info.CfAhbDef = 0;
    Proxy_TxByComBus.Proxy_TxByComTxAhb1Info.CfAhbSLMP = 0;
    Proxy_TxByComBus.Proxy_TxByComTxAhb1Info.CfBrkAhbSTDep_PC = 0;
    Proxy_TxByComBus.Proxy_TxByComTxAhb1Info.CfBrkBuzzR = 0;
    Proxy_TxByComBus.Proxy_TxByComTxAhb1Info.CfBrkPedalCalStatus = 0;
    Proxy_TxByComBus.Proxy_TxByComTxAhb1Info.CfBrkAhbSnsFail = 0;
    Proxy_TxByComBus.Proxy_TxByComTxAhb1Info.WhlSpdFLAhb = 0;
    Proxy_TxByComBus.Proxy_TxByComTxAhb1Info.WhlSpdFRAhb = 0;
    Proxy_TxByComBus.Proxy_TxByComTxAhb1Info.CrAhbMsgChkSum = 0;
    Proxy_TxByComBus.Proxy_TxByComSenPwrMonitorData.SenPwrM_5V_Stable = 0;
    Proxy_TxByComBus.Proxy_TxByComSenPwrMonitorData.SenPwrM_12V_Stable = 0;
    Proxy_TxByComBus.Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.QU_TAR_REPAT_XTRQ_FTAX_BAX_ACT = 0;
    Proxy_TxByComBus.Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.TAR_REPAT_XTRQ_FTAX_BAX_ACT = 0;
    Proxy_TxByComBus.Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.TAR_WMOM_PT_SUM_STAB_FAST = 0;
    Proxy_TxByComBus.Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.ST_ECU_RQ_WMOM_PT_SUM_STAB = 0;
    Proxy_TxByComBus.Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.QU_RQ_ILK_PT = 0;
    Proxy_TxByComBus.Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.QU_TAR_WMOM_PT_SUM_STAB = 0;
    Proxy_TxByComBus.Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.TAR_WMOM_PT_SUM_STAB = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.ST_ECU_AVL_BRTORQ_SUM = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_SER_PRMSN_DBC = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_SER_PRF_BRK = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.AVL_BRTORQ_SUM_DVCH = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.AVL_BRTORQ_SUM = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_AVL_BRTORQ_SUM = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_AVL_BRTORQ_SUM_DVCH = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_FLH = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_RRH = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_FRH = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_RRH = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_RLH = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_FLH = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_RLH = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_FRH = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_RPM_WHLInfo.AVL_RPM_WHL_RLH = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_RPM_WHLInfo.AVL_RPM_WHL_RRH = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_RPM_WHLInfo.AVL_RPM_WHL_FLH = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_FLH = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_FRH = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_RLH = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_RRH = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_RPM_WHLInfo.AVL_RPM_WHL_FRH = 0;
    Proxy_TxByComBus.Proxy_TxByComRQ_WMOM_PT_SUM_RECUPInfo.QU_TAR_WMOM_PT_SUM_RECUP = 0;
    Proxy_TxByComBus.Proxy_TxByComRQ_WMOM_PT_SUM_RECUPInfo.TAR_WMOM_PT_SUM_RECUP = 0;
    Proxy_TxByComBus.Proxy_TxByComRQ_WMOM_PT_SUM_RECUPInfo.ST_ECU_RQ_WMOM_PT_SUM_RECUP = 0;
    Proxy_TxByComBus.Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.ST_ECU_ST_STAB_DSC = 0;
    Proxy_TxByComBus.Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.ST_BRG_MSA = 0;
    Proxy_TxByComBus.Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.ST_BRG_DV = 0;
    Proxy_TxByComBus.Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.ST_UNRU_DSC = 0;
    Proxy_TxByComBus.Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.QU_FN_FDR = 0;
    Proxy_TxByComBus.Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.QU_FN_ABS = 0;
    Proxy_TxByComBus.Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.QU_FN_ASC = 0;
    Proxy_TxByComBus.Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.PRD_TIM_VEHSS = 0;
    Proxy_TxByComBus.Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.QU_SER_ECBA = 0;
    Proxy_TxByComBus.Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.TAR_BRTORQ_SUM_COOTD = 0;
    Proxy_TxByComBus.Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.AVL_WAY_BRKFA = 0;
    Proxy_TxByComBus.Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.AVL_FORC_BRKFA = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_RLH = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_RRH = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_FLH = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_FRH = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_FLH = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_RRH = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_FRH = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_RLH = 0;
    Proxy_TxByComBus.Proxy_TxByComTAR_LTRQD_BAXInfo.TAR_LTRQD_BAX = 0;
    Proxy_TxByComBus.Proxy_TxByComTAR_LTRQD_BAXInfo.LIM_MAX_LTRQD_BAX = 0;
    Proxy_TxByComBus.Proxy_TxByComST_YMRInfo.QU_SER_CLCTR_YMR = 0;
    Proxy_TxByComBus.Proxy_TxByComWEGSTRECKEInfo.MILE_FLH = 0;
    Proxy_TxByComBus.Proxy_TxByComWEGSTRECKEInfo.MILE_FRH = 0;
    Proxy_TxByComBus.Proxy_TxByComWEGSTRECKEInfo.MILE_INTM_TOMSRM = 0;
    Proxy_TxByComBus.Proxy_TxByComWEGSTRECKEInfo.MILE = 0;
    Proxy_TxByComBus.Proxy_TxByComDISP_CC_DRDY_00Info.ST_IDC_CC_DRDY_00 = 0;
    Proxy_TxByComBus.Proxy_TxByComDISP_CC_DRDY_00Info.ST_CC_DRDY_00 = 0;
    Proxy_TxByComBus.Proxy_TxByComDISP_CC_DRDY_00Info.TRANF_CC_DRDY_00 = 0;
    Proxy_TxByComBus.Proxy_TxByComDISP_CC_DRDY_00Info.NO_CC_DRDY_00 = 0;
    Proxy_TxByComBus.Proxy_TxByComDISP_CC_BYPA_00Info.TRANF_CC_BYPA_00 = 0;
    Proxy_TxByComBus.Proxy_TxByComDISP_CC_BYPA_00Info.ST_IDC_CC_BYPA_00 = 0;
    Proxy_TxByComBus.Proxy_TxByComDISP_CC_BYPA_00Info.ST_CC_BYPA_00 = 0;
    Proxy_TxByComBus.Proxy_TxByComDISP_CC_BYPA_00Info.NO_CC_BYPA_00 = 0;
    Proxy_TxByComBus.Proxy_TxByComST_VHSSInfo.ST_VEHSS = 0;
    Proxy_TxByComBus.Proxy_TxByComDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_1_ERR = 0;
    Proxy_TxByComBus.Proxy_TxByComDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_3_ERR = 0;
    Proxy_TxByComBus.Proxy_TxByComDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_4_ERR = 0;
    Proxy_TxByComBus.Proxy_TxByComDIAG_OBD_HYB_1Info.DIAG_OBD_HYB_1_MUX_MAX = 0;
    Proxy_TxByComBus.Proxy_TxByComDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_2_ERR = 0;
    Proxy_TxByComBus.Proxy_TxByComDIAG_OBD_HYB_1Info.DIAG_OBD_HYB_1_MUX_IMME = 0;
    Proxy_TxByComBus.Proxy_TxByComSU_DSCInfo.SU_DSC_WSS = 0;
    Proxy_TxByComBus.Proxy_TxByComST_TYR_RDCInfo.QU_TPL_RDC = 0;
    Proxy_TxByComBus.Proxy_TxByComST_TYR_RDCInfo.QU_FN_TYR_INFO_RDC = 0;
    Proxy_TxByComBus.Proxy_TxByComST_TYR_RDCInfo.QU_TFAI_RDC = 0;
    Proxy_TxByComBus.Proxy_TxByComST_TYR_RPAInfo.QU_TFAI_RPA = 0;
    Proxy_TxByComBus.Proxy_TxByComST_TYR_RPAInfo.QU_FN_TYR_INFO_RPA = 0;
    Proxy_TxByComBus.Proxy_TxByComST_TYR_RPAInfo.QU_TPL_RPA = 0;
    Proxy_TxByComBus.Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_2 = 0;
    Proxy_TxByComBus.Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_1 = 0;
    Proxy_TxByComBus.Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_3 = 0;
    Proxy_TxByComBus.Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_4 = 0;
    Proxy_TxByComBus.Proxy_TxByComST_TYR_2Info.QU_RDC_INIT_DISP = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_T_TYRInfo.AVL_TEMP_TYR_RRH = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_T_TYRInfo.AVL_TEMP_TYR_FLH = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_T_TYRInfo.AVL_TEMP_TYR_RLH = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_T_TYRInfo.AVL_TEMP_TYR_FRH = 0;
    Proxy_TxByComBus.Proxy_TxByComTEMP_BRKInfo.TEMP_BRDSK_RLH_VRFD = 0;
    Proxy_TxByComBus.Proxy_TxByComTEMP_BRKInfo.QU_TEMP_BRDSK_RLH_VRFD = 0;
    Proxy_TxByComBus.Proxy_TxByComTEMP_BRKInfo.TEMP_BRDSK_RRH_VRFD = 0;
    Proxy_TxByComBus.Proxy_TxByComTEMP_BRKInfo.QU_TEMP_BRDSK_RRH_VRFD = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_P_TYRInfo.AVL_P_TYR_FRH = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_P_TYRInfo.AVL_P_TYR_FLH = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_P_TYRInfo.AVL_P_TYR_RRH = 0;
    Proxy_TxByComBus.Proxy_TxByComAVL_P_TYRInfo.AVL_P_TYR_RLH = 0;
    Proxy_TxByComBus.Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_13 = 0;
    Proxy_TxByComBus.Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_04 = 0;
    Proxy_TxByComBus.Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_05 = 0;
    Proxy_TxByComBus.Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_06 = 0;
    Proxy_TxByComBus.Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_01 = 0;
    Proxy_TxByComBus.Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_02 = 0;
    Proxy_TxByComBus.Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_03 = 0;
    Proxy_TxByComBus.Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_07 = 0;
    Proxy_TxByComBus.Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_11 = 0;
    Proxy_TxByComBus.Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_10 = 0;
    Proxy_TxByComBus.Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_09 = 0;
    Proxy_TxByComBus.Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_15 = 0;
    Proxy_TxByComBus.Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_14 = 0;
    Proxy_TxByComBus.Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_12 = 0;
    Proxy_TxByComBus.Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_00 = 0;
    Proxy_TxByComBus.Proxy_TxByComFR_DBG_DSCInfo.BDTM_TA_FR = 0;
    Proxy_TxByComBus.Proxy_TxByComFR_DBG_DSCInfo.BDTM_TA_RL = 0;
    Proxy_TxByComBus.Proxy_TxByComFR_DBG_DSCInfo.BDTM_TA_RR = 0;
    Proxy_TxByComBus.Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_08 = 0;
    Proxy_TxByComBus.Proxy_TxByComFR_DBG_DSCInfo.ST_STATE = 0;
    Proxy_TxByComBus.Proxy_TxByComFR_DBG_DSCInfo.BDTM_TA_FL = 0;
    Proxy_TxByComBus.Proxy_TxByComFR_DBG_DSCInfo.ABSRef = 0;
    Proxy_TxByComBus.Proxy_TxByComFR_DBG_DSCInfo.LOW_VOLTAGE = 0;
    Proxy_TxByComBus.Proxy_TxByComFR_DBG_DSCInfo.DEBUG_PCIB = 0;
    Proxy_TxByComBus.Proxy_TxByComFR_DBG_DSCInfo.DBG_DSC = 0;
    Proxy_TxByComBus.Proxy_TxByComFR_DBG_DSCInfo.LATACC = 0;
    Proxy_TxByComBus.Proxy_TxByComFR_DBG_DSCInfo.TRIGGER_FS = 0;
    Proxy_TxByComBus.Proxy_TxByComFR_DBG_DSCInfo.TRIGGER_IS = 0;
}

void Proxy_TxByCom(void)
{
	DSresult[TIME_DS_CHECKPOINT_FOR_PROXY_TXBYCOM_START] = SetDeadlinesupervisionCheckpoint(TIME_DS_CHECKPOINT_FOR_PROXY_TXBYCOM_START);	//start 
    Proxy_TxByCom_Timer_Start = STM0_TIM0.U;
	LSresult[TIME_LS_GRAPH0]=SetLogicalsupervisionCheckpoint(TIME_LS_GRAPH0, TIME_LS_CHECKPOINT_FOR_GRAPH0_CP1, LS_FIRST_CHECKPOINT);	//1
	
	TimELogicalSupervisionTestCase_4();
    /* Input */
    Proxy_TxByCom_Read_Proxy_TxByComTxYawCbitInfo(&Proxy_TxByComBus.Proxy_TxByComTxYawCbitInfo);
	//LSresult[TIME_LS_GRAPH0]= SetLogicalsupervisionCheckpoint(TIME_LS_GRAPH0, TIME_LS_CHECKPOINT_FOR_GRAPH0_CP3, LS_RELATION_CHECKPOINT);	//3
    /*==============================================================================
    * Members of structure Proxy_TxByComTxYawCbitInfo 
     : Proxy_TxByComTxYawCbitInfo.TestYawRateSensor;
     : Proxy_TxByComTxYawCbitInfo.TestAcclSensor;
     : Proxy_TxByComTxYawCbitInfo.YawSerialNumberReq;
     =============================================================================*/
	LSresult[TIME_LS_GRAPH0]= SetLogicalsupervisionCheckpoint(TIME_LS_GRAPH0, TIME_LS_CHECKPOINT_FOR_GRAPH0_CP2, LS_RELATION_CHECKPOINT);//2
	TimELogicalSupervisionTestCase_5();
	
    Proxy_TxByCom_Read_Proxy_TxByComTxWhlSpdInfo(&Proxy_TxByComBus.Proxy_TxByComTxWhlSpdInfo);
    /*==============================================================================
    * Members of structure Proxy_TxByComTxWhlSpdInfo 
     : Proxy_TxByComTxWhlSpdInfo.WhlSpdFL;
     : Proxy_TxByComTxWhlSpdInfo.WhlSpdFR;
     : Proxy_TxByComTxWhlSpdInfo.WhlSpdRL;
     : Proxy_TxByComTxWhlSpdInfo.WhlSpdRR;
     =============================================================================*/
	LSresult[TIME_LS_GRAPH0]= SetLogicalsupervisionCheckpoint(TIME_LS_GRAPH0, TIME_LS_CHECKPOINT_FOR_GRAPH0_CP3, LS_RELATION_CHECKPOINT);//3
	TimELogicalSupervisionTestCase_6();
	
    Proxy_TxByCom_Read_Proxy_TxByComTxWhlPulInfo(&Proxy_TxByComBus.Proxy_TxByComTxWhlPulInfo);	
    /*==============================================================================
    * Members of structure Proxy_TxByComTxWhlPulInfo 
     : Proxy_TxByComTxWhlPulInfo.WhlPulFL;
     : Proxy_TxByComTxWhlPulInfo.WhlPulFR;
     : Proxy_TxByComTxWhlPulInfo.WhlPulRL;
     : Proxy_TxByComTxWhlPulInfo.WhlPulRR;
     =============================================================================*/
	LSresult[TIME_LS_GRAPH0]= SetLogicalsupervisionCheckpoint(TIME_LS_GRAPH0, TIME_LS_CHECKPOINT_FOR_GRAPH0_CP4, LS_LAST_CHECKPOINT);//4
	TimELogicalSupervisionTestCase_7();
    Proxy_TxByCom_Read_Proxy_TxByComTxTcs5Info(&Proxy_TxByComBus.Proxy_TxByComTxTcs5Info);	
    /*==============================================================================
    * Members of structure Proxy_TxByComTxTcs5Info 
     : Proxy_TxByComTxTcs5Info.CfBrkAbsWLMP;
     : Proxy_TxByComTxTcs5Info.CfBrkEbdWLMP;
     : Proxy_TxByComTxTcs5Info.CfBrkTcsWLMP;
     : Proxy_TxByComTxTcs5Info.CfBrkTcsFLMP;
     : Proxy_TxByComTxTcs5Info.CfBrkAbsDIAG;
     : Proxy_TxByComTxTcs5Info.CrBrkWheelFL_KMH;
     : Proxy_TxByComTxTcs5Info.CrBrkWheelFR_KMH;
     : Proxy_TxByComTxTcs5Info.CrBrkWheelRL_KMH;
     : Proxy_TxByComTxTcs5Info.CrBrkWheelRR_KMH;
     =============================================================================*/
    
    Proxy_TxByCom_Read_Proxy_TxByComTxTcs1Info(&Proxy_TxByComBus.Proxy_TxByComTxTcs1Info);
    /*==============================================================================
    * Members of structure Proxy_TxByComTxTcs1Info 
     : Proxy_TxByComTxTcs1Info.CfBrkTcsREQ;
     : Proxy_TxByComTxTcs1Info.CfBrkTcsPAS;
     : Proxy_TxByComTxTcs1Info.CfBrkTcsDEF;
     : Proxy_TxByComTxTcs1Info.CfBrkTcsCTL;
     : Proxy_TxByComTxTcs1Info.CfBrkAbsACT;
     : Proxy_TxByComTxTcs1Info.CfBrkAbsDEF;
     : Proxy_TxByComTxTcs1Info.CfBrkEbdDEF;
     : Proxy_TxByComTxTcs1Info.CfBrkTcsGSC;
     : Proxy_TxByComTxTcs1Info.CfBrkEspPAS;
     : Proxy_TxByComTxTcs1Info.CfBrkEspDEF;
     : Proxy_TxByComTxTcs1Info.CfBrkEspCTL;
     : Proxy_TxByComTxTcs1Info.CfBrkMsrREQ;
     : Proxy_TxByComTxTcs1Info.CfBrkTcsMFRN;
     : Proxy_TxByComTxTcs1Info.CfBrkMinGEAR;
     : Proxy_TxByComTxTcs1Info.CfBrkMaxGEAR;
     : Proxy_TxByComTxTcs1Info.BrakeLight;
     : Proxy_TxByComTxTcs1Info.HacCtl;
     : Proxy_TxByComTxTcs1Info.HacPas;
     : Proxy_TxByComTxTcs1Info.HacDef;
     : Proxy_TxByComTxTcs1Info.CrBrkTQI_PC;
     : Proxy_TxByComTxTcs1Info.CrBrkTQIMSR_PC;
     : Proxy_TxByComTxTcs1Info.CrBrkTQISLW_PC;
     : Proxy_TxByComTxTcs1Info.CrEbsMSGCHKSUM;
     =============================================================================*/
    
    Proxy_TxByCom_Read_Proxy_TxByComTxSasCalInfo(&Proxy_TxByComBus.Proxy_TxByComTxSasCalInfo);
    /*==============================================================================
    * Members of structure Proxy_TxByComTxSasCalInfo 
     : Proxy_TxByComTxSasCalInfo.CalSas_CCW;
     : Proxy_TxByComTxSasCalInfo.CalSas_Lws_CID;
     =============================================================================*/
    
    Proxy_TxByCom_Read_Proxy_TxByComTxEsp2Info(&Proxy_TxByComBus.Proxy_TxByComTxEsp2Info);
    /*==============================================================================
    * Members of structure Proxy_TxByComTxEsp2Info 
     : Proxy_TxByComTxEsp2Info.LatAccel;
     : Proxy_TxByComTxEsp2Info.LatAccelStat;
     : Proxy_TxByComTxEsp2Info.LatAccelDiag;
     : Proxy_TxByComTxEsp2Info.LongAccel;
     : Proxy_TxByComTxEsp2Info.LongAccelStat;
     : Proxy_TxByComTxEsp2Info.LongAccelDiag;
     : Proxy_TxByComTxEsp2Info.YawRate;
     : Proxy_TxByComTxEsp2Info.YawRateStat;
     : Proxy_TxByComTxEsp2Info.YawRateDiag;
     : Proxy_TxByComTxEsp2Info.CylPres;
     : Proxy_TxByComTxEsp2Info.CylPresStat;
     : Proxy_TxByComTxEsp2Info.CylPresDiag;
     =============================================================================*/
    
    Proxy_TxByCom_Read_Proxy_TxByComTxEbs1Info(&Proxy_TxByComBus.Proxy_TxByComTxEbs1Info);
    /*==============================================================================
    * Members of structure Proxy_TxByComTxEbs1Info 
     : Proxy_TxByComTxEbs1Info.CfBrkEbsStat;
     : Proxy_TxByComTxEbs1Info.CrBrkRegenTQLimit_NM;
     : Proxy_TxByComTxEbs1Info.CrBrkEstTot_NM;
     : Proxy_TxByComTxEbs1Info.CfBrkSnsFail;
     : Proxy_TxByComTxEbs1Info.CfBrkRbsWLamp;
     : Proxy_TxByComTxEbs1Info.CfBrkVacuumSysDef;
     : Proxy_TxByComTxEbs1Info.CrBrkEstHyd_NM;
     : Proxy_TxByComTxEbs1Info.CrBrkStkDep_PC;
     : Proxy_TxByComTxEbs1Info.CfBrkPgmRun1;
     =============================================================================*/
    
    Proxy_TxByCom_Read_Proxy_TxByComTxAhb1Info(&Proxy_TxByComBus.Proxy_TxByComTxAhb1Info);
    /*==============================================================================
    * Members of structure Proxy_TxByComTxAhb1Info 
     : Proxy_TxByComTxAhb1Info.CfAhbWLMP;
     : Proxy_TxByComTxAhb1Info.CfAhbDiag;
     : Proxy_TxByComTxAhb1Info.CfAhbAct;
     : Proxy_TxByComTxAhb1Info.CfAhbDef;
     : Proxy_TxByComTxAhb1Info.CfAhbSLMP;
     : Proxy_TxByComTxAhb1Info.CfBrkAhbSTDep_PC;
     : Proxy_TxByComTxAhb1Info.CfBrkBuzzR;
     : Proxy_TxByComTxAhb1Info.CfBrkPedalCalStatus;
     : Proxy_TxByComTxAhb1Info.CfBrkAhbSnsFail;
     : Proxy_TxByComTxAhb1Info.WhlSpdFLAhb;
     : Proxy_TxByComTxAhb1Info.WhlSpdFRAhb;
     : Proxy_TxByComTxAhb1Info.CrAhbMsgChkSum;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Proxy_TxByCom_Read_Proxy_TxByComSenPwrMonitorData_SenPwrM_5V_Stable(&Proxy_TxByComBus.Proxy_TxByComSenPwrMonitorData.SenPwrM_5V_Stable);
    Proxy_TxByCom_Read_Proxy_TxByComSenPwrMonitorData_SenPwrM_12V_Stable(&Proxy_TxByComBus.Proxy_TxByComSenPwrMonitorData.SenPwrM_12V_Stable);

    Proxy_TxByCom_Read_Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo(&Proxy_TxByComBus.Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo);
    /*==============================================================================
    * Members of structure Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo 
     : Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.QU_TAR_REPAT_XTRQ_FTAX_BAX_ACT;
     : Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.TAR_REPAT_XTRQ_FTAX_BAX_ACT;
     : Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.TAR_WMOM_PT_SUM_STAB_FAST;
     : Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.ST_ECU_RQ_WMOM_PT_SUM_STAB;
     : Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.QU_RQ_ILK_PT;
     : Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.QU_TAR_WMOM_PT_SUM_STAB;
     : Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.TAR_WMOM_PT_SUM_STAB;
     =============================================================================*/
    
    Proxy_TxByCom_Read_Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo(&Proxy_TxByComBus.Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo);
    /*==============================================================================
    * Members of structure Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo 
     : Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.ST_ECU_AVL_BRTORQ_SUM;
     : Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_SER_PRMSN_DBC;
     : Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_SER_PRF_BRK;
     : Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.AVL_BRTORQ_SUM_DVCH;
     : Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.AVL_BRTORQ_SUM;
     : Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_AVL_BRTORQ_SUM;
     : Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_AVL_BRTORQ_SUM_DVCH;
     =============================================================================*/
    
    Proxy_TxByCom_Read_Proxy_TxByComAVL_BRTORQ_WHLInfo(&Proxy_TxByComBus.Proxy_TxByComAVL_BRTORQ_WHLInfo);
    /*==============================================================================
    * Members of structure Proxy_TxByComAVL_BRTORQ_WHLInfo 
     : Proxy_TxByComAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_FLH;
     : Proxy_TxByComAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_RRH;
     : Proxy_TxByComAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_FRH;
     : Proxy_TxByComAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_RRH;
     : Proxy_TxByComAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_RLH;
     : Proxy_TxByComAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_FLH;
     : Proxy_TxByComAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_RLH;
     : Proxy_TxByComAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_FRH;
     =============================================================================*/
    
    Proxy_TxByCom_Read_Proxy_TxByComAVL_RPM_WHLInfo(&Proxy_TxByComBus.Proxy_TxByComAVL_RPM_WHLInfo);
    /*==============================================================================
    * Members of structure Proxy_TxByComAVL_RPM_WHLInfo 
     : Proxy_TxByComAVL_RPM_WHLInfo.AVL_RPM_WHL_RLH;
     : Proxy_TxByComAVL_RPM_WHLInfo.AVL_RPM_WHL_RRH;
     : Proxy_TxByComAVL_RPM_WHLInfo.AVL_RPM_WHL_FLH;
     : Proxy_TxByComAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_FLH;
     : Proxy_TxByComAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_FRH;
     : Proxy_TxByComAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_RLH;
     : Proxy_TxByComAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_RRH;
     : Proxy_TxByComAVL_RPM_WHLInfo.AVL_RPM_WHL_FRH;
     =============================================================================*/
    
    Proxy_TxByCom_Read_Proxy_TxByComRQ_WMOM_PT_SUM_RECUPInfo(&Proxy_TxByComBus.Proxy_TxByComRQ_WMOM_PT_SUM_RECUPInfo);
    /*==============================================================================
    * Members of structure Proxy_TxByComRQ_WMOM_PT_SUM_RECUPInfo 
     : Proxy_TxByComRQ_WMOM_PT_SUM_RECUPInfo.QU_TAR_WMOM_PT_SUM_RECUP;
     : Proxy_TxByComRQ_WMOM_PT_SUM_RECUPInfo.TAR_WMOM_PT_SUM_RECUP;
     : Proxy_TxByComRQ_WMOM_PT_SUM_RECUPInfo.ST_ECU_RQ_WMOM_PT_SUM_RECUP;
     =============================================================================*/
    
    Proxy_TxByCom_Read_Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info(&Proxy_TxByComBus.Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info);
    /*==============================================================================
    * Members of structure Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info 
     : Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.ST_ECU_ST_STAB_DSC;
     : Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.ST_BRG_MSA;
     : Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.ST_BRG_DV;
     : Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.ST_UNRU_DSC;
     : Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.QU_FN_FDR;
     : Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.QU_FN_ABS;
     : Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.QU_FN_ASC;
     : Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.PRD_TIM_VEHSS;
     =============================================================================*/
    
    Proxy_TxByCom_Read_Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo(&Proxy_TxByComBus.Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo);
    /*==============================================================================
    * Members of structure Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo 
     : Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.QU_SER_ECBA;
     : Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.TAR_BRTORQ_SUM_COOTD;
     : Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.AVL_WAY_BRKFA;
     : Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.AVL_FORC_BRKFA;
     =============================================================================*/
    
    Proxy_TxByCom_Read_Proxy_TxByComAVL_QUAN_EES_WHLInfo(&Proxy_TxByComBus.Proxy_TxByComAVL_QUAN_EES_WHLInfo);
    /*==============================================================================
    * Members of structure Proxy_TxByComAVL_QUAN_EES_WHLInfo 
     : Proxy_TxByComAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_RLH;
     : Proxy_TxByComAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_RRH;
     : Proxy_TxByComAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_FLH;
     : Proxy_TxByComAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_FRH;
     : Proxy_TxByComAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_FLH;
     : Proxy_TxByComAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_RRH;
     : Proxy_TxByComAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_FRH;
     : Proxy_TxByComAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_RLH;
     =============================================================================*/
    
    Proxy_TxByCom_Read_Proxy_TxByComTAR_LTRQD_BAXInfo(&Proxy_TxByComBus.Proxy_TxByComTAR_LTRQD_BAXInfo);
    /*==============================================================================
    * Members of structure Proxy_TxByComTAR_LTRQD_BAXInfo 
     : Proxy_TxByComTAR_LTRQD_BAXInfo.TAR_LTRQD_BAX;
     : Proxy_TxByComTAR_LTRQD_BAXInfo.LIM_MAX_LTRQD_BAX;
     =============================================================================*/
    
    Proxy_TxByCom_Read_Proxy_TxByComST_YMRInfo(&Proxy_TxByComBus.Proxy_TxByComST_YMRInfo);
    /*==============================================================================
    * Members of structure Proxy_TxByComST_YMRInfo 
     : Proxy_TxByComST_YMRInfo.QU_SER_CLCTR_YMR;
     =============================================================================*/
    
    Proxy_TxByCom_Read_Proxy_TxByComWEGSTRECKEInfo(&Proxy_TxByComBus.Proxy_TxByComWEGSTRECKEInfo);
    /*==============================================================================
    * Members of structure Proxy_TxByComWEGSTRECKEInfo 
     : Proxy_TxByComWEGSTRECKEInfo.MILE_FLH;
     : Proxy_TxByComWEGSTRECKEInfo.MILE_FRH;
     : Proxy_TxByComWEGSTRECKEInfo.MILE_INTM_TOMSRM;
     : Proxy_TxByComWEGSTRECKEInfo.MILE;
     =============================================================================*/
    
    Proxy_TxByCom_Read_Proxy_TxByComDISP_CC_DRDY_00Info(&Proxy_TxByComBus.Proxy_TxByComDISP_CC_DRDY_00Info);
    /*==============================================================================
    * Members of structure Proxy_TxByComDISP_CC_DRDY_00Info 
     : Proxy_TxByComDISP_CC_DRDY_00Info.ST_IDC_CC_DRDY_00;
     : Proxy_TxByComDISP_CC_DRDY_00Info.ST_CC_DRDY_00;
     : Proxy_TxByComDISP_CC_DRDY_00Info.TRANF_CC_DRDY_00;
     : Proxy_TxByComDISP_CC_DRDY_00Info.NO_CC_DRDY_00;
     =============================================================================*/
    
    Proxy_TxByCom_Read_Proxy_TxByComDISP_CC_BYPA_00Info(&Proxy_TxByComBus.Proxy_TxByComDISP_CC_BYPA_00Info);
    /*==============================================================================
    * Members of structure Proxy_TxByComDISP_CC_BYPA_00Info 
     : Proxy_TxByComDISP_CC_BYPA_00Info.TRANF_CC_BYPA_00;
     : Proxy_TxByComDISP_CC_BYPA_00Info.ST_IDC_CC_BYPA_00;
     : Proxy_TxByComDISP_CC_BYPA_00Info.ST_CC_BYPA_00;
     : Proxy_TxByComDISP_CC_BYPA_00Info.NO_CC_BYPA_00;
     =============================================================================*/
    
    Proxy_TxByCom_Read_Proxy_TxByComST_VHSSInfo(&Proxy_TxByComBus.Proxy_TxByComST_VHSSInfo);
    /*==============================================================================
    * Members of structure Proxy_TxByComST_VHSSInfo 
     : Proxy_TxByComST_VHSSInfo.ST_VEHSS;
     =============================================================================*/
    
    Proxy_TxByCom_Read_Proxy_TxByComDIAG_OBD_HYB_1Info(&Proxy_TxByComBus.Proxy_TxByComDIAG_OBD_HYB_1Info);
    /*==============================================================================
    * Members of structure Proxy_TxByComDIAG_OBD_HYB_1Info 
     : Proxy_TxByComDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_1_ERR;
     : Proxy_TxByComDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_3_ERR;
     : Proxy_TxByComDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_4_ERR;
     : Proxy_TxByComDIAG_OBD_HYB_1Info.DIAG_OBD_HYB_1_MUX_MAX;
     : Proxy_TxByComDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_2_ERR;
     : Proxy_TxByComDIAG_OBD_HYB_1Info.DIAG_OBD_HYB_1_MUX_IMME;
     =============================================================================*/
    
    Proxy_TxByCom_Read_Proxy_TxByComSU_DSCInfo(&Proxy_TxByComBus.Proxy_TxByComSU_DSCInfo);
    /*==============================================================================
    * Members of structure Proxy_TxByComSU_DSCInfo 
     : Proxy_TxByComSU_DSCInfo.SU_DSC_WSS;
     =============================================================================*/
    
    Proxy_TxByCom_Read_Proxy_TxByComST_TYR_RDCInfo(&Proxy_TxByComBus.Proxy_TxByComST_TYR_RDCInfo);
    /*==============================================================================
    * Members of structure Proxy_TxByComST_TYR_RDCInfo 
     : Proxy_TxByComST_TYR_RDCInfo.QU_TPL_RDC;
     : Proxy_TxByComST_TYR_RDCInfo.QU_FN_TYR_INFO_RDC;
     : Proxy_TxByComST_TYR_RDCInfo.QU_TFAI_RDC;
     =============================================================================*/
    
    Proxy_TxByCom_Read_Proxy_TxByComST_TYR_RPAInfo(&Proxy_TxByComBus.Proxy_TxByComST_TYR_RPAInfo);
    /*==============================================================================
    * Members of structure Proxy_TxByComST_TYR_RPAInfo 
     : Proxy_TxByComST_TYR_RPAInfo.QU_TFAI_RPA;
     : Proxy_TxByComST_TYR_RPAInfo.QU_FN_TYR_INFO_RPA;
     : Proxy_TxByComST_TYR_RPAInfo.QU_TPL_RPA;
     =============================================================================*/
    
    Proxy_TxByCom_Read_Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info(&Proxy_TxByComBus.Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info);
    /*==============================================================================
    * Members of structure Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info 
     : Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_2;
     : Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_1;
     : Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_3;
     : Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_4;
     =============================================================================*/
    
    Proxy_TxByCom_Read_Proxy_TxByComST_TYR_2Info(&Proxy_TxByComBus.Proxy_TxByComST_TYR_2Info);
    /*==============================================================================
    * Members of structure Proxy_TxByComST_TYR_2Info 
     : Proxy_TxByComST_TYR_2Info.QU_RDC_INIT_DISP;
     =============================================================================*/
    
    Proxy_TxByCom_Read_Proxy_TxByComAVL_T_TYRInfo(&Proxy_TxByComBus.Proxy_TxByComAVL_T_TYRInfo);
    /*==============================================================================
    * Members of structure Proxy_TxByComAVL_T_TYRInfo 
     : Proxy_TxByComAVL_T_TYRInfo.AVL_TEMP_TYR_RRH;
     : Proxy_TxByComAVL_T_TYRInfo.AVL_TEMP_TYR_FLH;
     : Proxy_TxByComAVL_T_TYRInfo.AVL_TEMP_TYR_RLH;
     : Proxy_TxByComAVL_T_TYRInfo.AVL_TEMP_TYR_FRH;
     =============================================================================*/
    
    Proxy_TxByCom_Read_Proxy_TxByComTEMP_BRKInfo(&Proxy_TxByComBus.Proxy_TxByComTEMP_BRKInfo);
    /*==============================================================================
    * Members of structure Proxy_TxByComTEMP_BRKInfo 
     : Proxy_TxByComTEMP_BRKInfo.TEMP_BRDSK_RLH_VRFD;
     : Proxy_TxByComTEMP_BRKInfo.QU_TEMP_BRDSK_RLH_VRFD;
     : Proxy_TxByComTEMP_BRKInfo.TEMP_BRDSK_RRH_VRFD;
     : Proxy_TxByComTEMP_BRKInfo.QU_TEMP_BRDSK_RRH_VRFD;
     =============================================================================*/
    
    Proxy_TxByCom_Read_Proxy_TxByComAVL_P_TYRInfo(&Proxy_TxByComBus.Proxy_TxByComAVL_P_TYRInfo);
    /*==============================================================================
    * Members of structure Proxy_TxByComAVL_P_TYRInfo 
     : Proxy_TxByComAVL_P_TYRInfo.AVL_P_TYR_FRH;
     : Proxy_TxByComAVL_P_TYRInfo.AVL_P_TYR_FLH;
     : Proxy_TxByComAVL_P_TYRInfo.AVL_P_TYR_RRH;
     : Proxy_TxByComAVL_P_TYRInfo.AVL_P_TYR_RLH;
     =============================================================================*/
    
    Proxy_TxByCom_Read_Proxy_TxByComFR_DBG_DSCInfo(&Proxy_TxByComBus.Proxy_TxByComFR_DBG_DSCInfo);
    /*==============================================================================
    * Members of structure Proxy_TxByComFR_DBG_DSCInfo 
     : Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_13;
     : Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_04;
     : Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_05;
     : Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_06;
     : Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_01;
     : Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_02;
     : Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_03;
     : Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_07;
     : Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_11;
     : Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_10;
     : Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_09;
     : Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_15;
     : Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_14;
     : Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_12;
     : Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_00;
     : Proxy_TxByComFR_DBG_DSCInfo.BDTM_TA_FR;
     : Proxy_TxByComFR_DBG_DSCInfo.BDTM_TA_RL;
     : Proxy_TxByComFR_DBG_DSCInfo.BDTM_TA_RR;
     : Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_08;
     : Proxy_TxByComFR_DBG_DSCInfo.ST_STATE;
     : Proxy_TxByComFR_DBG_DSCInfo.BDTM_TA_FL;
     : Proxy_TxByComFR_DBG_DSCInfo.ABSRef;
     : Proxy_TxByComFR_DBG_DSCInfo.LOW_VOLTAGE;
     : Proxy_TxByComFR_DBG_DSCInfo.DEBUG_PCIB;
     : Proxy_TxByComFR_DBG_DSCInfo.DBG_DSC;
     : Proxy_TxByComFR_DBG_DSCInfo.LATACC;
     : Proxy_TxByComFR_DBG_DSCInfo.TRIGGER_FS;
     : Proxy_TxByComFR_DBG_DSCInfo.TRIGGER_IS;
     =============================================================================*/
    

    /* Process */
    Proxy_TxProcByCom();

    /* Output */

    Proxy_TxByCom_Timer_Elapsed = STM0_TIM0.U - Proxy_TxByCom_Timer_Start;
	DSresult[TIME_DS_CHECKPOINT_FOR_PROXY_TXBYCOM_END] = SetDeadlinesupervisionCheckpoint(TIME_DS_CHECKPOINT_FOR_PROXY_TXBYCOM_END);	//end 
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define PROXY_TXBYCOM_STOP_SEC_CODE
#include "Proxy_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
