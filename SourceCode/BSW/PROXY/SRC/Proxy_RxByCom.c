/**
 * @defgroup Proxy_RxByCom Proxy_RxByCom
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Proxy_RxByCom.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Proxy_RxByCom.h"
#include "Proxy_RxByCom_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PROXY_RXBYCOM_START_SEC_CONST_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PROXY_RXBYCOM_STOP_SEC_CONST_UNSPECIFIED
#include "Proxy_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PROXY_RXBYCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Proxy_RxByCom_HdrBusType Proxy_RxByComBus;

/* Version Info */
const SwcVersionInfo_t Proxy_RxByComVersionInfo = 
{   
    PROXY_RXBYCOM_MODULE_ID,           /* Proxy_RxByComVersionInfo.ModuleId */
    PROXY_RXBYCOM_MAJOR_VERSION,       /* Proxy_RxByComVersionInfo.MajorVer */
    PROXY_RXBYCOM_MINOR_VERSION,       /* Proxy_RxByComVersionInfo.MinorVer */
    PROXY_RXBYCOM_PATCH_VERSION,       /* Proxy_RxByComVersionInfo.PatchVer */
    PROXY_RXBYCOM_BRANCH_VERSION       /* Proxy_RxByComVersionInfo.BranchVer */
};
    
/* Input Data Element */

/* Output Data Element */
Proxy_RxByComRxBms1Info_t Proxy_RxByComRxBms1Info;
Proxy_RxByComRxYawSerialInfo_t Proxy_RxByComRxYawSerialInfo;
Proxy_RxByComRxYawAccInfo_t Proxy_RxByComRxYawAccInfo;
Proxy_RxByComRxTcu6Info_t Proxy_RxByComRxTcu6Info;
Proxy_RxByComRxTcu5Info_t Proxy_RxByComRxTcu5Info;
Proxy_RxByComRxTcu1Info_t Proxy_RxByComRxTcu1Info;
Proxy_RxByComRxSasInfo_t Proxy_RxByComRxSasInfo;
Proxy_RxByComRxMcu2Info_t Proxy_RxByComRxMcu2Info;
Proxy_RxByComRxMcu1Info_t Proxy_RxByComRxMcu1Info;
Proxy_RxByComRxLongAccInfo_t Proxy_RxByComRxLongAccInfo;
Proxy_RxByComRxHcu5Info_t Proxy_RxByComRxHcu5Info;
Proxy_RxByComRxHcu3Info_t Proxy_RxByComRxHcu3Info;
Proxy_RxByComRxHcu2Info_t Proxy_RxByComRxHcu2Info;
Proxy_RxByComRxHcu1Info_t Proxy_RxByComRxHcu1Info;
Proxy_RxByComRxFact1Info_t Proxy_RxByComRxFact1Info;
Proxy_RxByComRxEms3Info_t Proxy_RxByComRxEms3Info;
Proxy_RxByComRxEms2Info_t Proxy_RxByComRxEms2Info;
Proxy_RxByComRxEms1Info_t Proxy_RxByComRxEms1Info;
Proxy_RxByComRxClu2Info_t Proxy_RxByComRxClu2Info;
Proxy_RxByComRxClu1Info_t Proxy_RxByComRxClu1Info;
Proxy_RxByComRxMsgOkFlgInfo_t Proxy_RxByComRxMsgOkFlgInfo;
Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo_t Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo;
Proxy_RxByComAVL_LTRQD_BAXInfo_t Proxy_RxByComAVL_LTRQD_BAXInfo;
Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo_t Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo;
Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info_t Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info;
Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info_t Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info;
Proxy_RxByComHGLV_VEH_FILTInfo_t Proxy_RxByComHGLV_VEH_FILTInfo;
Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo_t Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo;
Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo_t Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo;
Proxy_RxByComV_VEH_V_VEH_2Info_t Proxy_RxByComV_VEH_V_VEH_2Info;
Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo_t Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo;
Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo_t Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo;
Proxy_RxByComAVL_STEA_FTAXInfo_t Proxy_RxByComAVL_STEA_FTAXInfo;
Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_t Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo;
Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info_t Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info;
Proxy_RxByComWMOM_DRV_7Info_t Proxy_RxByComWMOM_DRV_7Info;
Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo_t Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo;
Proxy_RxByComDT_BRKSYS_ENGMGInfo_t Proxy_RxByComDT_BRKSYS_ENGMGInfo;
Proxy_RxByComERRM_BN_UInfo_t Proxy_RxByComERRM_BN_UInfo;
Proxy_RxByComCTR_CRInfo_t Proxy_RxByComCTR_CRInfo;
Proxy_RxByComKLEMMENInfo_t Proxy_RxByComKLEMMENInfo;
Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_t Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info;
Proxy_RxByComBEDIENUNG_WISCHERInfo_t Proxy_RxByComBEDIENUNG_WISCHERInfo;
Proxy_RxByComDT_PT_2Info_t Proxy_RxByComDT_PT_2Info;
Proxy_RxByComSTAT_ENG_STA_AUTOInfo_t Proxy_RxByComSTAT_ENG_STA_AUTOInfo;
Proxy_RxByComST_ENERG_GENInfo_t Proxy_RxByComST_ENERG_GENInfo;
Proxy_RxByComDT_PT_1Info_t Proxy_RxByComDT_PT_1Info;
Proxy_RxByComDT_GRDT_DRVInfo_t Proxy_RxByComDT_GRDT_DRVInfo;
Proxy_RxByComDIAG_OBD_ENGMG_ELInfo_t Proxy_RxByComDIAG_OBD_ENGMG_ELInfo;
Proxy_RxByComSTAT_CT_HABRInfo_t Proxy_RxByComSTAT_CT_HABRInfo;
Proxy_RxByComDT_PT_3Info_t Proxy_RxByComDT_PT_3Info;
Proxy_RxByComEINHEITEN_BN2020Info_t Proxy_RxByComEINHEITEN_BN2020Info;
Proxy_RxByComA_TEMPInfo_t Proxy_RxByComA_TEMPInfo;
Proxy_RxByComWISCHERGESCHWINDIGKEITInfo_t Proxy_RxByComWISCHERGESCHWINDIGKEITInfo;
Proxy_RxByComSTAT_DS_VRFDInfo_t Proxy_RxByComSTAT_DS_VRFDInfo;
Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info_t Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info;
Proxy_RxByComSTAT_ANHAENGERInfo_t Proxy_RxByComSTAT_ANHAENGERInfo;
Proxy_RxByComFZZSTDInfo_t Proxy_RxByComFZZSTDInfo;
Proxy_RxByComBEDIENUNG_FAHRWERKInfo_t Proxy_RxByComBEDIENUNG_FAHRWERKInfo;
Proxy_RxByComFAHRZEUGTYPInfo_t Proxy_RxByComFAHRZEUGTYPInfo;
Proxy_RxByComST_BLT_CT_SOCCUInfo_t Proxy_RxByComST_BLT_CT_SOCCUInfo;
Proxy_RxByComFAHRGESTELLNUMMERInfo_t Proxy_RxByComFAHRGESTELLNUMMERInfo;
Proxy_RxByComRELATIVZEITInfo_t Proxy_RxByComRELATIVZEITInfo;
Proxy_RxByComKILOMETERSTANDInfo_t Proxy_RxByComKILOMETERSTANDInfo;
Proxy_RxByComUHRZEIT_DATUMInfo_t Proxy_RxByComUHRZEIT_DATUMInfo;

uint32 Proxy_RxByCom_Timer_Start;
uint32 Proxy_RxByCom_Timer_Elapsed;

#define PROXY_RXBYCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Proxy_MemMap.h"
#define PROXY_RXBYCOM_START_SEC_VAR_NOINIT_32BIT
#include "Proxy_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PROXY_RXBYCOM_STOP_SEC_VAR_NOINIT_32BIT
#include "Proxy_MemMap.h"
#define PROXY_RXBYCOM_START_SEC_VAR_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PROXY_RXBYCOM_STOP_SEC_VAR_UNSPECIFIED
#include "Proxy_MemMap.h"
#define PROXY_RXBYCOM_START_SEC_VAR_32BIT
#include "Proxy_MemMap.h"
/** Variable Section (32BIT)**/


#define PROXY_RXBYCOM_STOP_SEC_VAR_32BIT
#include "Proxy_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PROXY_RXBYCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PROXY_RXBYCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Proxy_MemMap.h"
#define PROXY_RXBYCOM_START_SEC_VAR_NOINIT_32BIT
#include "Proxy_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PROXY_RXBYCOM_STOP_SEC_VAR_NOINIT_32BIT
#include "Proxy_MemMap.h"
#define PROXY_RXBYCOM_START_SEC_VAR_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PROXY_RXBYCOM_STOP_SEC_VAR_UNSPECIFIED
#include "Proxy_MemMap.h"
#define PROXY_RXBYCOM_START_SEC_VAR_32BIT
#include "Proxy_MemMap.h"
/** Variable Section (32BIT)**/


#define PROXY_RXBYCOM_STOP_SEC_VAR_32BIT
#include "Proxy_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PROXY_RXBYCOM_START_SEC_CODE
#include "Proxy_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Proxy_RxByCom_Init(void)
{
    /* Initialize internal bus */
    Proxy_RxByComBus.Proxy_RxByComRxBms1Info.Bms1SocPc = 0;
    Proxy_RxByComBus.Proxy_RxByComRxYawSerialInfo.YawSerialNum_0 = 0;
    Proxy_RxByComBus.Proxy_RxByComRxYawSerialInfo.YawSerialNum_1 = 0;
    Proxy_RxByComBus.Proxy_RxByComRxYawSerialInfo.YawSerialNum_2 = 0;
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.YawRateValidData = 0;
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.YawRateSelfTestStatus = 0;
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.YawRateSignal_0 = 0;
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.YawRateSignal_1 = 0;
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.SensorOscFreqDev = 0;
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.Gyro_Fail = 0;
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.Raster_Fail = 0;
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.Eep_Fail = 0;
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.Batt_Range_Err = 0;
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.Asic_Fail = 0;
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.Accel_Fail = 0;
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.Ram_Fail = 0;
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.Rom_Fail = 0;
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.Ad_Fail = 0;
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.Osc_Fail = 0;
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.Watchdog_Rst = 0;
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.Plaus_Err_Pst = 0;
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.RollingCounter = 0;
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.Can_Func_Err = 0;
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.AccelEratorRateSig_0 = 0;
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.AccelEratorRateSig_1 = 0;
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.LatAccValidData = 0;
    Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo.LatAccSelfTestStatus = 0;
    Proxy_RxByComBus.Proxy_RxByComRxTcu6Info.ShiftClass_Ccan = 0;
    Proxy_RxByComBus.Proxy_RxByComRxTcu5Info.Typ = 0;
    Proxy_RxByComBus.Proxy_RxByComRxTcu5Info.GearTyp = 0;
    Proxy_RxByComBus.Proxy_RxByComRxTcu1Info.Targe = 0;
    Proxy_RxByComBus.Proxy_RxByComRxTcu1Info.GarChange = 0;
    Proxy_RxByComBus.Proxy_RxByComRxTcu1Info.Flt = 0;
    Proxy_RxByComBus.Proxy_RxByComRxTcu1Info.GarSelDisp = 0;
    Proxy_RxByComBus.Proxy_RxByComRxTcu1Info.TQRedReq_PC = 0;
    Proxy_RxByComBus.Proxy_RxByComRxTcu1Info.TQRedReqSlw_PC = 0;
    Proxy_RxByComBus.Proxy_RxByComRxTcu1Info.TQIncReq_PC = 0;
    Proxy_RxByComBus.Proxy_RxByComRxSasInfo.Angle = 0;
    Proxy_RxByComBus.Proxy_RxByComRxSasInfo.Speed = 0;
    Proxy_RxByComBus.Proxy_RxByComRxSasInfo.Ok = 0;
    Proxy_RxByComBus.Proxy_RxByComRxSasInfo.Cal = 0;
    Proxy_RxByComBus.Proxy_RxByComRxSasInfo.Trim = 0;
    Proxy_RxByComBus.Proxy_RxByComRxSasInfo.CheckSum = 0;
    Proxy_RxByComBus.Proxy_RxByComRxSasInfo.MsgCount = 0;
    Proxy_RxByComBus.Proxy_RxByComRxMcu2Info.Flt = 0;
    Proxy_RxByComBus.Proxy_RxByComRxMcu1Info.MoTestTQ_PC = 0;
    Proxy_RxByComBus.Proxy_RxByComRxMcu1Info.MotActRotSpd_RPM = 0;
    Proxy_RxByComBus.Proxy_RxByComRxLongAccInfo.IntSenFltSymtmActive = 0;
    Proxy_RxByComBus.Proxy_RxByComRxLongAccInfo.IntSenFaultPresent = 0;
    Proxy_RxByComBus.Proxy_RxByComRxLongAccInfo.LongAccSenCirErrPre = 0;
    Proxy_RxByComBus.Proxy_RxByComRxLongAccInfo.LonACSenRanChkErrPre = 0;
    Proxy_RxByComBus.Proxy_RxByComRxLongAccInfo.LongRollingCounter = 0;
    Proxy_RxByComBus.Proxy_RxByComRxLongAccInfo.IntTempSensorFault = 0;
    Proxy_RxByComBus.Proxy_RxByComRxLongAccInfo.LongAccInvalidData = 0;
    Proxy_RxByComBus.Proxy_RxByComRxLongAccInfo.LongAccSelfTstStatus = 0;
    Proxy_RxByComBus.Proxy_RxByComRxLongAccInfo.LongAccRateSignal_0 = 0;
    Proxy_RxByComBus.Proxy_RxByComRxLongAccInfo.LongAccRateSignal_1 = 0;
    Proxy_RxByComBus.Proxy_RxByComRxHcu5Info.HevMod = 0;
    Proxy_RxByComBus.Proxy_RxByComRxHcu3Info.TmIntQcMDBINV_PC = 0;
    Proxy_RxByComBus.Proxy_RxByComRxHcu3Info.MotTQCMC_PC = 0;
    Proxy_RxByComBus.Proxy_RxByComRxHcu3Info.MotTQCMDBINV_PC = 0;
    Proxy_RxByComBus.Proxy_RxByComRxHcu2Info.ServiceMod = 0;
    Proxy_RxByComBus.Proxy_RxByComRxHcu2Info.RegenENA = 0;
    Proxy_RxByComBus.Proxy_RxByComRxHcu2Info.RegenBRKTQ_NM = 0;
    Proxy_RxByComBus.Proxy_RxByComRxHcu2Info.CrpTQ_NM = 0;
    Proxy_RxByComBus.Proxy_RxByComRxHcu2Info.WhlDEMTQ_NM = 0;
    Proxy_RxByComBus.Proxy_RxByComRxHcu1Info.EngCltStat = 0;
    Proxy_RxByComBus.Proxy_RxByComRxHcu1Info.HEVRDY = 0;
    Proxy_RxByComBus.Proxy_RxByComRxHcu1Info.EngTQCmdBinV_PC = 0;
    Proxy_RxByComBus.Proxy_RxByComRxHcu1Info.EngTQCmd_PC = 0;
    Proxy_RxByComBus.Proxy_RxByComRxFact1Info.OutTemp_SNR_C = 0;
    Proxy_RxByComBus.Proxy_RxByComRxEms3Info.EngColTemp_C = 0;
    Proxy_RxByComBus.Proxy_RxByComRxEms2Info.EngSpdErr = 0;
    Proxy_RxByComBus.Proxy_RxByComRxEms2Info.AccPedDep_PC = 0;
    Proxy_RxByComBus.Proxy_RxByComRxEms2Info.Tps_PC = 0;
    Proxy_RxByComBus.Proxy_RxByComRxEms1Info.TqStd_NM = 0;
    Proxy_RxByComBus.Proxy_RxByComRxEms1Info.ActINDTQ_PC = 0;
    Proxy_RxByComBus.Proxy_RxByComRxEms1Info.EngSpd_RPM = 0;
    Proxy_RxByComBus.Proxy_RxByComRxEms1Info.IndTQ_PC = 0;
    Proxy_RxByComBus.Proxy_RxByComRxEms1Info.FrictTQ_PC = 0;
    Proxy_RxByComBus.Proxy_RxByComRxClu2Info.IGN_SW = 0;
    Proxy_RxByComBus.Proxy_RxByComRxClu1Info.P_Brake_Act = 0;
    Proxy_RxByComBus.Proxy_RxByComRxClu1Info.Cf_Clu_BrakeFluIDSW = 0;
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.Bms1MsgOkFlg = 0;
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.YawSerialMsgOkFlg = 0;
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.YawAccMsgOkFlg = 0;
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.Tcu6MsgOkFlg = 0;
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.Tcu5MsgOkFlg = 0;
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.Tcu1MsgOkFlg = 0;
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.SasMsgOkFlg = 0;
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.Mcu2MsgOkFlg = 0;
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.Mcu1MsgOkFlg = 0;
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.LongAccMsgOkFlg = 0;
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.Hcu5MsgOkFlg = 0;
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.Hcu3MsgOkFlg = 0;
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.Hcu2MsgOkFlg = 0;
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.Hcu1MsgOkFlg = 0;
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.Fatc1MsgOkFlg = 0;
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.Ems3MsgOkFlg = 0;
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.Ems2MsgOkFlg = 0;
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.Ems1MsgOkFlg = 0;
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.Clu2MsgOkFlg = 0;
    Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo.Clu1MsgOkFlg = 0;
    Proxy_RxByComBus.Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.ST_ECU_ST_REPAT_XTRQ_FTAX_BAX = 0;
    Proxy_RxByComBus.Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.ALIV_ST_REPAT_XTRQ_FTAX_BAX = 0;
    Proxy_RxByComBus.Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.CRC_ST_REPAT_XTRQ_FTAX_BAX = 0;
    Proxy_RxByComBus.Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.QU_SER_REPAT_XTRQ_FTAX_BAX_ACT = 0;
    Proxy_RxByComBus.Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.AVL_REPAT_XTRQ_FTAX_BAX = 0;
    Proxy_RxByComBus.Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.QU_AVL_REPAT_XTRQ_FTAX_BAX = 0;
    Proxy_RxByComBus.Proxy_RxByComAVL_LTRQD_BAXInfo.QU_SER_LTRQD_BAX = 0;
    Proxy_RxByComBus.Proxy_RxByComAVL_LTRQD_BAXInfo.CRC_AVL_LTRQD_BAX = 0;
    Proxy_RxByComBus.Proxy_RxByComAVL_LTRQD_BAXInfo.QU_AVL_LTRQD_BAX = 0;
    Proxy_RxByComBus.Proxy_RxByComAVL_LTRQD_BAXInfo.AVL_LTRQD_BAX = 0;
    Proxy_RxByComBus.Proxy_RxByComAVL_LTRQD_BAXInfo.ALIV_AVL_LTRQD_BAX = 0;
    Proxy_RxByComBus.Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.AVL_TORQ_CRSH_DMEE = 0;
    Proxy_RxByComBus.Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.AVL_TORQ_CRSH = 0;
    Proxy_RxByComBus.Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.ST_SAIL_DRV_2 = 0;
    Proxy_RxByComBus.Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.CRC_ANG_ACPD = 0;
    Proxy_RxByComBus.Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.QU_AVL_RPM_ENG_CRSH = 0;
    Proxy_RxByComBus.Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.AVL_RPM_ENG_CRSH = 0;
    Proxy_RxByComBus.Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.ALIV_TORQ_CRSH_1 = 0;
    Proxy_RxByComBus.Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.CRC_TORQ_CRSH_1 = 0;
    Proxy_RxByComBus.Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.AVL_ANG_ACPD = 0;
    Proxy_RxByComBus.Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.QU_AVL_ANG_ACPD = 0;
    Proxy_RxByComBus.Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.ALIV_ANG_ACPD = 0;
    Proxy_RxByComBus.Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.ST_ECU_ANG_ACPD = 0;
    Proxy_RxByComBus.Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.AVL_ANG_ACPD_VIRT = 0;
    Proxy_RxByComBus.Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.ECO_ANG_ACPD = 0;
    Proxy_RxByComBus.Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.GRAD_AVL_ANG_ACPD = 0;
    Proxy_RxByComBus.Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.ST_INTF_DRASY = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.AVL_RPM_BAX_RED = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.ST_PENG_PT = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.ST_AVAI_INTV_PT_DRS = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.QU_AVL_RPM_BAX_RED = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.QU_SER_WMOM_PT_SUM_DRS = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.QU_SER_WMOM_PT_SUM_STAB = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.ST_ECU_WMOM_DRV_5 = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.TAR_WMOM_PT_SUM_COOTD = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.QU_SER_COOR_TORQ_BDRV = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.ST_ECU_WMOM_DRV_4 = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.ST_DRVDIR_DVCH = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.CRC_WMOM_DRV_4 = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.ALIV_WMOM_DRV_4 = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.ALIV_WMOM_DRV_5 = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.CRC_WMOM_DRV_5 = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.QU_AVL_WMOM_PT_SUM = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_ERR_AMP = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.REIN_PT = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.CRC_WMOM_DRV_1 = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.ALIV_WMOM_DRV_1 = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.ST_ECU_WMOM_DRV_1 = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_MAX = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_FAST_TOP = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_FAST_BOT = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.ST_ECU_WMOM_DRV_2 = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.QU_REIN_PT = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.CRC_WMOM_DRV_2 = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.ALIV_WMOM_DRV_2 = 0;
    Proxy_RxByComBus.Proxy_RxByComHGLV_VEH_FILTInfo.HGLV_VEH_FILT_FRH = 0;
    Proxy_RxByComBus.Proxy_RxByComHGLV_VEH_FILTInfo.HGLV_VEH_FILT_FLH = 0;
    Proxy_RxByComBus.Proxy_RxByComHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_FRH = 0;
    Proxy_RxByComBus.Proxy_RxByComHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_FLH = 0;
    Proxy_RxByComBus.Proxy_RxByComHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_RRH = 0;
    Proxy_RxByComBus.Proxy_RxByComHGLV_VEH_FILTInfo.HGLV_VEH_FILT_RLH = 0;
    Proxy_RxByComBus.Proxy_RxByComHGLV_VEH_FILTInfo.ALIV_HGLV_VEH_FILT = 0;
    Proxy_RxByComBus.Proxy_RxByComHGLV_VEH_FILTInfo.HGLV_VEH_FILT_RRH = 0;
    Proxy_RxByComBus.Proxy_RxByComHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_RLH = 0;
    Proxy_RxByComBus.Proxy_RxByComHGLV_VEH_FILTInfo.CRC_HGLV_VEH_FILT = 0;
    Proxy_RxByComBus.Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.ATTAV_ESTI = 0;
    Proxy_RxByComBus.Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.ATTA_ESTI_ERR_AMP = 0;
    Proxy_RxByComBus.Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.VY_ESTI_ERR_AMP = 0;
    Proxy_RxByComBus.Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.VY_ESTI = 0;
    Proxy_RxByComBus.Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.ATTAV_ESTI_ERR_AMP = 0;
    Proxy_RxByComBus.Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.CRC_VEH_DYNMC_DT_ESTI_VRFD = 0;
    Proxy_RxByComBus.Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.ALIV_VEH_DYNMC_DT_ESTI_VRFD = 0;
    Proxy_RxByComBus.Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.QU_VEH_DYNMC_DT_ESTI = 0;
    Proxy_RxByComBus.Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.ATTA_ESTI = 0;
    Proxy_RxByComBus.Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.QU_ACLNY_COG = 0;
    Proxy_RxByComBus.Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNY_COG_ERR_AMP = 0;
    Proxy_RxByComBus.Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ALIV_ACLNX_COG = 0;
    Proxy_RxByComBus.Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNX_COG = 0;
    Proxy_RxByComBus.Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ALIV_ACLNY_COG = 0;
    Proxy_RxByComBus.Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.CRC_ACLNX_COG = 0;
    Proxy_RxByComBus.Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.CRC_ACLNY_COG = 0;
    Proxy_RxByComBus.Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNY_COG = 0;
    Proxy_RxByComBus.Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNX_COG_ERR_AMP = 0;
    Proxy_RxByComBus.Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.QU_ACLNX_COG = 0;
    Proxy_RxByComBus.Proxy_RxByComV_VEH_V_VEH_2Info.CRC_V_VEH = 0;
    Proxy_RxByComBus.Proxy_RxByComV_VEH_V_VEH_2Info.ALIV_V_VEH = 0;
    Proxy_RxByComBus.Proxy_RxByComV_VEH_V_VEH_2Info.QU_V_VEH_COG = 0;
    Proxy_RxByComBus.Proxy_RxByComV_VEH_V_VEH_2Info.DVCO_VEH = 0;
    Proxy_RxByComBus.Proxy_RxByComV_VEH_V_VEH_2Info.ST_V_VEH_NSS = 0;
    Proxy_RxByComBus.Proxy_RxByComV_VEH_V_VEH_2Info.V_VEH_COG = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo.ALIV_VYAW_VEH = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo.VYAW_VEH_ERR_AMP = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo.CRC_VYAW_VEH = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo.QU_VYAW_VEH = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo.VYAW_VEH = 0;
    Proxy_RxByComBus.Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo.QU_AVL_TRGR_RW = 0;
    Proxy_RxByComBus.Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo.AVL_TRGR_RW = 0;
    Proxy_RxByComBus.Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo.AVL_LOGR_RW_FAST = 0;
    Proxy_RxByComBus.Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo.ALIV_TLT_RW = 0;
    Proxy_RxByComBus.Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo.CRC_TLT_RW = 0;
    Proxy_RxByComBus.Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo.QU_AVL_LOGR_RW = 0;
    Proxy_RxByComBus.Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo.AVL_LOGR_RW = 0;
    Proxy_RxByComBus.Proxy_RxByComAVL_STEA_FTAXInfo.QU_AVL_STEA_FTAX_PNI = 0;
    Proxy_RxByComBus.Proxy_RxByComAVL_STEA_FTAXInfo.AVL_STEA_FTAX_PNI = 0;
    Proxy_RxByComBus.Proxy_RxByComAVL_STEA_FTAXInfo.CRC_AVL_STEA_FTAX = 0;
    Proxy_RxByComBus.Proxy_RxByComAVL_STEA_FTAXInfo.QU_AVL_STEA_FTAX_WHL = 0;
    Proxy_RxByComBus.Proxy_RxByComAVL_STEA_FTAXInfo.ALIV_AVL_STEA_FTAX = 0;
    Proxy_RxByComBus.Proxy_RxByComAVL_STEA_FTAXInfo.AVL_STEA_FTAX_WHL = 0;
    Proxy_RxByComBus.Proxy_RxByComAVL_STEA_FTAXInfo.AVL_STEA_FTAX_WHL_ERR_AMP = 0;
    Proxy_RxByComBus.Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_PRMSN_DBC_DCRN_MAX = 0;
    Proxy_RxByComBus.Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_PRF_BRK = 0;
    Proxy_RxByComBus.Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_V_HDC = 0;
    Proxy_RxByComBus.Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_PRMSN_DBC_DCRN_GRAD_MAX = 0;
    Proxy_RxByComBus.Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.ALIV_SPEC_PRMSN_IBRK_HDC = 0;
    Proxy_RxByComBus.Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.CRC_SPEC_PRMSN_IBRK_HDC = 0;
    Proxy_RxByComBus.Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_PRMSN_DBC_TR_THRV = 0;
    Proxy_RxByComBus.Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_BRTORQ_SUM = 0;
    Proxy_RxByComBus.Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_BRTORQ_SUM = 0;
    Proxy_RxByComBus.Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_PRMSN_DBC = 0;
    Proxy_RxByComBus.Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.RQ_TAO_SSM = 0;
    Proxy_RxByComBus.Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.CRC_RQ_BRTORQ_SUM = 0;
    Proxy_RxByComBus.Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.ALIV_RQ_BRTORQ_SUM = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_DTORQ_BOT = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_RECUP_MAX = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_ILS = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.CRC_WMOM_DRV_3 = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.ALIV_WMOM_DRV_3 = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_DTORQ_TOP = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.ST_ECU_WMOM_DRV_6 = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.ALIV_WMOM_DRV_6 = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.CRC_WMOM_DRV_6 = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_7Info.ST_EL_DRVG = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_7Info.PRD_AVL_WMOM_PT_SUM_RECUP_MAX = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_7Info.ALIV_WMOM_DRV_7 = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_7Info.CRC_WMOM_DRV_7 = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_7Info.ST_ECU_WMOM_DRV_7 = 0;
    Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_7Info.QU_SER_WMOM_PT_SUM_RECUP = 0;
    Proxy_RxByComBus.Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.TAR_DIFF_BRTORQ_BAX_YMR = 0;
    Proxy_RxByComBus.Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.FACT_TAR_COMPT_DRV_YMR = 0;
    Proxy_RxByComBus.Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.QU_TAR_DIFF_BRTORQ_YMR = 0;
    Proxy_RxByComBus.Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.CRC_RQ_DIFF_BRTORQ_YMR = 0;
    Proxy_RxByComBus.Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.ALIV_RQ_DIFF_BRTORQ_YMR = 0;
    Proxy_RxByComBus.Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.TAR_DIFF_BRTORQ_FTAX_YMR = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_BRKSYS_ENGMGInfo.QU_AVL_LOWP_BRKFA = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_BRKSYS_ENGMGInfo.AVL_LOWP_BRKFA = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_BRKSYS_ENGMGInfo.CRC_DT_BRKSYS_ENGMG = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_BRKSYS_ENGMGInfo.ALIV_DT_BRKSYS_ENGMG = 0;
    Proxy_RxByComBus.Proxy_RxByComERRM_BN_UInfo.CTR_ERRM_BN_U = 0;
    Proxy_RxByComBus.Proxy_RxByComCTR_CRInfo.ALIV_CTR_CR = 0;
    Proxy_RxByComBus.Proxy_RxByComCTR_CRInfo.ST_EXCE_ACLN_THRV = 0;
    Proxy_RxByComBus.Proxy_RxByComCTR_CRInfo.CTR_PHTR_CR = 0;
    Proxy_RxByComBus.Proxy_RxByComCTR_CRInfo.CTR_ITLI_CR = 0;
    Proxy_RxByComBus.Proxy_RxByComCTR_CRInfo.CTR_CLSY_CR = 0;
    Proxy_RxByComBus.Proxy_RxByComCTR_CRInfo.CTR_AUTOM_ECAL_CR = 0;
    Proxy_RxByComBus.Proxy_RxByComCTR_CRInfo.CTR_SWO_EKP_CR = 0;
    Proxy_RxByComBus.Proxy_RxByComCTR_CRInfo.CRC_CTR_CR = 0;
    Proxy_RxByComBus.Proxy_RxByComCTR_CRInfo.CTR_PCSH_MST = 0;
    Proxy_RxByComBus.Proxy_RxByComCTR_CRInfo.CTR_HAZW_CR = 0;
    Proxy_RxByComBus.Proxy_RxByComKLEMMENInfo.ST_OP_MSA = 0;
    Proxy_RxByComBus.Proxy_RxByComKLEMMENInfo.RQ_DRVG_RDI = 0;
    Proxy_RxByComBus.Proxy_RxByComKLEMMENInfo.ST_KL_DBG = 0;
    Proxy_RxByComBus.Proxy_RxByComKLEMMENInfo.ST_KL_50_MSA = 0;
    Proxy_RxByComBus.Proxy_RxByComKLEMMENInfo.ST_SSP = 0;
    Proxy_RxByComBus.Proxy_RxByComKLEMMENInfo.RWDT_BLS = 0;
    Proxy_RxByComBus.Proxy_RxByComKLEMMENInfo.ST_STCD_PENG = 0;
    Proxy_RxByComBus.Proxy_RxByComKLEMMENInfo.ST_KL = 0;
    Proxy_RxByComBus.Proxy_RxByComKLEMMENInfo.ST_KL_DIV = 0;
    Proxy_RxByComBus.Proxy_RxByComKLEMMENInfo.ST_VEH_CON = 0;
    Proxy_RxByComBus.Proxy_RxByComKLEMMENInfo.CRC_KL = 0;
    Proxy_RxByComBus.Proxy_RxByComKLEMMENInfo.ALIV_COU_KL = 0;
    Proxy_RxByComBus.Proxy_RxByComKLEMMENInfo.ST_KL_30B = 0;
    Proxy_RxByComBus.Proxy_RxByComKLEMMENInfo.CON_CLT_SW = 0;
    Proxy_RxByComBus.Proxy_RxByComKLEMMENInfo.CTR_ENG_STOP = 0;
    Proxy_RxByComBus.Proxy_RxByComKLEMMENInfo.ST_PLK = 0;
    Proxy_RxByComBus.Proxy_RxByComKLEMMENInfo.ST_KL_15N = 0;
    Proxy_RxByComBus.Proxy_RxByComKLEMMENInfo.ST_KL_KEY_VLD = 0;
    Proxy_RxByComBus.Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_MES_TSTMP = 0;
    Proxy_RxByComBus.Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.PCKG_ID = 0;
    Proxy_RxByComBus.Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.SUPP_ID = 0;
    Proxy_RxByComBus.Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_5 = 0;
    Proxy_RxByComBus.Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_8 = 0;
    Proxy_RxByComBus.Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_7 = 0;
    Proxy_RxByComBus.Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_6 = 0;
    Proxy_RxByComBus.Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.ALIV_RDC_DT_PCKG_2 = 0;
    Proxy_RxByComBus.Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_1 = 0;
    Proxy_RxByComBus.Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.TYR_ID = 0;
    Proxy_RxByComBus.Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.ALIV_RDC_DT_PCKG_1 = 0;
    Proxy_RxByComBus.Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_4 = 0;
    Proxy_RxByComBus.Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_3 = 0;
    Proxy_RxByComBus.Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_2 = 0;
    Proxy_RxByComBus.Proxy_RxByComBEDIENUNG_WISCHERInfo.ALIV_WISW = 0;
    Proxy_RxByComBus.Proxy_RxByComBEDIENUNG_WISCHERInfo.OP_WISW = 0;
    Proxy_RxByComBus.Proxy_RxByComBEDIENUNG_WISCHERInfo.OP_WIPO = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_PT_2Info.ST_GRSEL_DRV = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_PT_2Info.TEMP_EOI_DRV = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_PT_2Info.RLS_ENGSTA = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_PT_2Info.RPM_ENG_MAX_ALW = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_PT_2Info.RDUC_DOCTR_RPM_DRV_2 = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_PT_2Info.TEMP_ENG_DRV = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_PT_2Info.ST_DRV_VEH = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_PT_2Info.ST_ECU_DT_PT_2 = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_PT_2Info.ST_IDLG_ENG_DRV = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_PT_2Info.ST_ILK_STRT_DRV = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_PT_2Info.ST_SW_CLT_DRV = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_PT_2Info.CRC_DT_PT_2 = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_PT_2Info.ALIV_DT_PT_2 = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_PT_2Info.ST_ENG_RUN_DRV = 0;
    Proxy_RxByComBus.Proxy_RxByComSTAT_ENG_STA_AUTOInfo.RQ_MSA_ENG_STA_1 = 0;
    Proxy_RxByComBus.Proxy_RxByComSTAT_ENG_STA_AUTOInfo.RQ_MSA_ENG_STA_2 = 0;
    Proxy_RxByComBus.Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ALIV_ST_ENG_STA_AUTO = 0;
    Proxy_RxByComBus.Proxy_RxByComSTAT_ENG_STA_AUTOInfo.PSBTY_MSA_ENG_STOP_STA = 0;
    Proxy_RxByComBus.Proxy_RxByComSTAT_ENG_STA_AUTOInfo.CRC_ST_ENG_STA_AUTO = 0;
    Proxy_RxByComBus.Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_SHFT_MSA_ENGSTP = 0;
    Proxy_RxByComBus.Proxy_RxByComSTAT_ENG_STA_AUTOInfo.RQ_SLIP_K0 = 0;
    Proxy_RxByComBus.Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_TAR_PENG_CENG = 0;
    Proxy_RxByComBus.Proxy_RxByComSTAT_ENG_STA_AUTOInfo.SPEC_TYP_ENGSTA = 0;
    Proxy_RxByComBus.Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_RPM_CLCTR_MOT = 0;
    Proxy_RxByComBus.Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_FN_MSA = 0;
    Proxy_RxByComBus.Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_AVL_PENG_CENG_ENGMG = 0;
    Proxy_RxByComBus.Proxy_RxByComSTAT_ENG_STA_AUTOInfo.DISP_REAS_PREV_SWO_CENG = 0;
    Proxy_RxByComBus.Proxy_RxByComSTAT_ENG_STA_AUTOInfo.VARI_TYP_ENGSTA = 0;
    Proxy_RxByComBus.Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_TAR_CENG = 0;
    Proxy_RxByComBus.Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_AVAI_SAIL_DME = 0;
    Proxy_RxByComBus.Proxy_RxByComST_ENERG_GENInfo.ST_LDST_GEN_DRV = 0;
    Proxy_RxByComBus.Proxy_RxByComST_ENERG_GENInfo.DT_PCU_SCP = 0;
    Proxy_RxByComBus.Proxy_RxByComST_ENERG_GENInfo.ST_GEN_DRV = 0;
    Proxy_RxByComBus.Proxy_RxByComST_ENERG_GENInfo.AVL_I_GEN_DRV = 0;
    Proxy_RxByComBus.Proxy_RxByComST_ENERG_GENInfo.LDST_GEN_DRV = 0;
    Proxy_RxByComBus.Proxy_RxByComST_ENERG_GENInfo.ST_LDREL_GEN = 0;
    Proxy_RxByComBus.Proxy_RxByComST_ENERG_GENInfo.ST_CHG_STOR = 0;
    Proxy_RxByComBus.Proxy_RxByComST_ENERG_GENInfo.TEMP_BT_14V = 0;
    Proxy_RxByComBus.Proxy_RxByComST_ENERG_GENInfo.ST_I_IBS = 0;
    Proxy_RxByComBus.Proxy_RxByComST_ENERG_GENInfo.ST_SEP_STOR = 0;
    Proxy_RxByComBus.Proxy_RxByComST_ENERG_GENInfo.ST_BN2_SCP = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_PT_1Info.RQ_SHPA_GRB_REGE_PAFI = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_PT_1Info.ST_RTIR_DRV = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_PT_1Info.CTR_SLCK_DRV = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_PT_1Info.RQ_STASS_ENGMG = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_PT_1Info.ST_CAT_HT = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_PT_1Info.RQ_SHPA_GRB_CHGBLC = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_PT_1Info.TAR_RPM_IDLG_DRV_EXT = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_PT_1Info.SLCTN_BUS_COMM_ENG_GRB = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_PT_1Info.TAR_RPM_IDLG_DRV = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_PT_1Info.ST_INFS_DRV = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_PT_1Info.ST_SW_WAUP_DRV = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_PT_1Info.AIP_ENG_DRV = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_PT_1Info.RDUC_DOCTR_RPM_DRV = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_GRDT_DRVInfo.ST_OPMO_GRDT_DRV = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_GRDT_DRVInfo.ST_RSTA_GRDT = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_GRDT_DRVInfo.CRC_GRDT_DRV = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_GRDT_DRVInfo.ALIV_GRDT_DRV = 0;
    Proxy_RxByComBus.Proxy_RxByComDIAG_OBD_ENGMG_ELInfo.RQ_MIL_DIAG_OBD_ENGMG_EL = 0;
    Proxy_RxByComBus.Proxy_RxByComDIAG_OBD_ENGMG_ELInfo.RQ_RST_OBD_DIAG = 0;
    Proxy_RxByComBus.Proxy_RxByComSTAT_CT_HABRInfo.ST_CT_HABR = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_PT_3Info.TRNRAO_BAX = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_PT_3Info.QU_TRNRAO_BAX = 0;
    Proxy_RxByComBus.Proxy_RxByComDT_PT_3Info.ALIV_DT_PT_3 = 0;
    Proxy_RxByComBus.Proxy_RxByComEINHEITEN_BN2020Info.UN_TORQ_S_MOD = 0;
    Proxy_RxByComBus.Proxy_RxByComEINHEITEN_BN2020Info.UN_PWR_S_MOD = 0;
    Proxy_RxByComBus.Proxy_RxByComEINHEITEN_BN2020Info.UN_DATE_EXT = 0;
    Proxy_RxByComBus.Proxy_RxByComEINHEITEN_BN2020Info.UN_COSP_EL = 0;
    Proxy_RxByComBus.Proxy_RxByComEINHEITEN_BN2020Info.UN_TEMP = 0;
    Proxy_RxByComBus.Proxy_RxByComEINHEITEN_BN2020Info.UN_AIP = 0;
    Proxy_RxByComBus.Proxy_RxByComEINHEITEN_BN2020Info.LANG = 0;
    Proxy_RxByComBus.Proxy_RxByComEINHEITEN_BN2020Info.UN_DATE = 0;
    Proxy_RxByComBus.Proxy_RxByComEINHEITEN_BN2020Info.UN_T = 0;
    Proxy_RxByComBus.Proxy_RxByComEINHEITEN_BN2020Info.UN_SPDM_DGTL = 0;
    Proxy_RxByComBus.Proxy_RxByComEINHEITEN_BN2020Info.UN_MILE = 0;
    Proxy_RxByComBus.Proxy_RxByComEINHEITEN_BN2020Info.UN_FU = 0;
    Proxy_RxByComBus.Proxy_RxByComEINHEITEN_BN2020Info.UN_COSP = 0;
    Proxy_RxByComBus.Proxy_RxByComA_TEMPInfo.TEMP_EX_UNFILT = 0;
    Proxy_RxByComBus.Proxy_RxByComA_TEMPInfo.TEMP_EX = 0;
    Proxy_RxByComBus.Proxy_RxByComWISCHERGESCHWINDIGKEITInfo.ST_RNSE = 0;
    Proxy_RxByComBus.Proxy_RxByComWISCHERGESCHWINDIGKEITInfo.INT_RN = 0;
    Proxy_RxByComBus.Proxy_RxByComWISCHERGESCHWINDIGKEITInfo.V_WI = 0;
    Proxy_RxByComBus.Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_DRD_SFY_CTRL = 0;
    Proxy_RxByComBus.Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_DVDR_VRFD = 0;
    Proxy_RxByComBus.Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_DVDR_SFY_CTRL = 0;
    Proxy_RxByComBus.Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_PSDR_SFY_CTRL = 0;
    Proxy_RxByComBus.Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_PSD_SFY_CTRL = 0;
    Proxy_RxByComBus.Proxy_RxByComSTAT_DS_VRFDInfo.ALIV_ST_DSW_VRFD = 0;
    Proxy_RxByComBus.Proxy_RxByComSTAT_DS_VRFDInfo.CRC_ST_DSW_VRFD = 0;
    Proxy_RxByComBus.Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_DRD_VRFD = 0;
    Proxy_RxByComBus.Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_PSDR_VRFD = 0;
    Proxy_RxByComBus.Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_PSD_VRFD = 0;
    Proxy_RxByComBus.Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY = 0;
    Proxy_RxByComBus.Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.RQ_SW_DRDY_MMID = 0;
    Proxy_RxByComBus.Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.RQ_SU_SW_DRDY = 0;
    Proxy_RxByComBus.Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.CRC_SU_SW_DRDY = 0;
    Proxy_RxByComBus.Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.ALIV_SU_SW_DRDY = 0;
    Proxy_RxByComBus.Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.RQ_SW_DRDY_KDIS = 0;
    Proxy_RxByComBus.Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY_CHAS = 0;
    Proxy_RxByComBus.Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY_DRV = 0;
    Proxy_RxByComBus.Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY_STAB = 0;
    Proxy_RxByComBus.Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.DISP_ST_DSC = 0;
    Proxy_RxByComBus.Proxy_RxByComSTAT_ANHAENGERInfo.ST_SYNCN_HAZWCL_TRAI = 0;
    Proxy_RxByComBus.Proxy_RxByComSTAT_ANHAENGERInfo.ST_RFLI_DF_TRAI = 0;
    Proxy_RxByComBus.Proxy_RxByComSTAT_ANHAENGERInfo.ST_TRAI = 0;
    Proxy_RxByComBus.Proxy_RxByComSTAT_ANHAENGERInfo.ST_DI_DF_TRAI = 0;
    Proxy_RxByComBus.Proxy_RxByComSTAT_ANHAENGERInfo.ST_PO_AHV = 0;
    Proxy_RxByComBus.Proxy_RxByComFZZSTDInfo.ST_ILK_ERRM_FZM = 0;
    Proxy_RxByComBus.Proxy_RxByComFZZSTDInfo.ST_ENERG_FZM = 0;
    Proxy_RxByComBus.Proxy_RxByComFZZSTDInfo.ST_BT_PROTE_WUP = 0;
    Proxy_RxByComBus.Proxy_RxByComBEDIENUNG_FAHRWERKInfo.OP_MOD_TRCT_DSC = 0;
    Proxy_RxByComBus.Proxy_RxByComBEDIENUNG_FAHRWERKInfo.OP_TPCT = 0;
    Proxy_RxByComBus.Proxy_RxByComFAHRZEUGTYPInfo.QUAN_CYL = 0;
    Proxy_RxByComBus.Proxy_RxByComFAHRZEUGTYPInfo.QUAN_GR = 0;
    Proxy_RxByComBus.Proxy_RxByComFAHRZEUGTYPInfo.TYP_VEH = 0;
    Proxy_RxByComBus.Proxy_RxByComFAHRZEUGTYPInfo.TYP_BODY = 0;
    Proxy_RxByComBus.Proxy_RxByComFAHRZEUGTYPInfo.TYP_ENG = 0;
    Proxy_RxByComBus.Proxy_RxByComFAHRZEUGTYPInfo.CLAS_PWR = 0;
    Proxy_RxByComBus.Proxy_RxByComFAHRZEUGTYPInfo.TYP_CNT = 0;
    Proxy_RxByComBus.Proxy_RxByComFAHRZEUGTYPInfo.TYP_STE = 0;
    Proxy_RxByComBus.Proxy_RxByComFAHRZEUGTYPInfo.TYP_GRB = 0;
    Proxy_RxByComBus.Proxy_RxByComFAHRZEUGTYPInfo.TYP_CAPA = 0;
    Proxy_RxByComBus.Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_RRH = 0;
    Proxy_RxByComBus.Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_BLTB_SW_RM = 0;
    Proxy_RxByComBus.Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_BLTB_SW_RRH = 0;
    Proxy_RxByComBus.Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_ERR_SEAT_MT_DR = 0;
    Proxy_RxByComBus.Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_BLTB_SW_DR = 0;
    Proxy_RxByComBus.Proxy_RxByComST_BLT_CT_SOCCUInfo.CRC_ST_BLT_CT_SOCCU = 0;
    Proxy_RxByComBus.Proxy_RxByComST_BLT_CT_SOCCUInfo.ALIV_COU_ST_BLT_CT_SOCCU = 0;
    Proxy_RxByComBus.Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_DR = 0;
    Proxy_RxByComBus.Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_BLTB_SW_RLH = 0;
    Proxy_RxByComBus.Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_RLH = 0;
    Proxy_RxByComBus.Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_BLTB_SW_PS = 0;
    Proxy_RxByComBus.Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_PS = 0;
    Proxy_RxByComBus.Proxy_RxByComFAHRGESTELLNUMMERInfo.NO_VECH_1 = 0;
    Proxy_RxByComBus.Proxy_RxByComFAHRGESTELLNUMMERInfo.NO_VECH_2 = 0;
    Proxy_RxByComBus.Proxy_RxByComFAHRGESTELLNUMMERInfo.NO_VECH_3 = 0;
    Proxy_RxByComBus.Proxy_RxByComFAHRGESTELLNUMMERInfo.NO_VECH_6 = 0;
    Proxy_RxByComBus.Proxy_RxByComFAHRGESTELLNUMMERInfo.NO_VECH_7 = 0;
    Proxy_RxByComBus.Proxy_RxByComFAHRGESTELLNUMMERInfo.NO_VECH_4 = 0;
    Proxy_RxByComBus.Proxy_RxByComFAHRGESTELLNUMMERInfo.NO_VECH_5 = 0;
    Proxy_RxByComBus.Proxy_RxByComRELATIVZEITInfo.T_SEC_COU_REL = 0;
    Proxy_RxByComBus.Proxy_RxByComRELATIVZEITInfo.T_DAY_COU_ABSL = 0;
    Proxy_RxByComBus.Proxy_RxByComKILOMETERSTANDInfo.RNG = 0;
    Proxy_RxByComBus.Proxy_RxByComKILOMETERSTANDInfo.ST_FLLV_FUTA_SPAR = 0;
    Proxy_RxByComBus.Proxy_RxByComKILOMETERSTANDInfo.FLLV_FUTA_RH = 0;
    Proxy_RxByComBus.Proxy_RxByComKILOMETERSTANDInfo.FLLV_FUTA_LH = 0;
    Proxy_RxByComBus.Proxy_RxByComKILOMETERSTANDInfo.FLLV_FUTA = 0;
    Proxy_RxByComBus.Proxy_RxByComKILOMETERSTANDInfo.MILE_KM = 0;
    Proxy_RxByComBus.Proxy_RxByComUHRZEIT_DATUMInfo.DISP_DATE_WDAY = 0;
    Proxy_RxByComBus.Proxy_RxByComUHRZEIT_DATUMInfo.DISP_DATE_DAY = 0;
    Proxy_RxByComBus.Proxy_RxByComUHRZEIT_DATUMInfo.DISP_DATE_MON = 0;
    Proxy_RxByComBus.Proxy_RxByComUHRZEIT_DATUMInfo.ST_DISP_CTI_DATE = 0;
    Proxy_RxByComBus.Proxy_RxByComUHRZEIT_DATUMInfo.DISP_DATE_YR = 0;
    Proxy_RxByComBus.Proxy_RxByComUHRZEIT_DATUMInfo.DISP_HR = 0;
    Proxy_RxByComBus.Proxy_RxByComUHRZEIT_DATUMInfo.DISP_SEC = 0;
    Proxy_RxByComBus.Proxy_RxByComUHRZEIT_DATUMInfo.DISP_MN = 0;
}

void Proxy_RxByCom(void)
{
    Proxy_RxByCom_Timer_Start = STM0_TIM0.U;

    /* Input */

    /* Process */
    Proxy_RxProcByCom();
    
    /* Output */
    Proxy_RxByCom_Write_Proxy_RxByComRxBms1Info(&Proxy_RxByComBus.Proxy_RxByComRxBms1Info);
    /*==============================================================================
    * Members of structure Proxy_RxByComRxBms1Info 
     : Proxy_RxByComRxBms1Info.Bms1SocPc;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComRxYawSerialInfo(&Proxy_RxByComBus.Proxy_RxByComRxYawSerialInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComRxYawSerialInfo 
     : Proxy_RxByComRxYawSerialInfo.YawSerialNum_0;
     : Proxy_RxByComRxYawSerialInfo.YawSerialNum_1;
     : Proxy_RxByComRxYawSerialInfo.YawSerialNum_2;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComRxYawAccInfo(&Proxy_RxByComBus.Proxy_RxByComRxYawAccInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComRxYawAccInfo 
     : Proxy_RxByComRxYawAccInfo.YawRateValidData;
     : Proxy_RxByComRxYawAccInfo.YawRateSelfTestStatus;
     : Proxy_RxByComRxYawAccInfo.YawRateSignal_0;
     : Proxy_RxByComRxYawAccInfo.YawRateSignal_1;
     : Proxy_RxByComRxYawAccInfo.SensorOscFreqDev;
     : Proxy_RxByComRxYawAccInfo.Gyro_Fail;
     : Proxy_RxByComRxYawAccInfo.Raster_Fail;
     : Proxy_RxByComRxYawAccInfo.Eep_Fail;
     : Proxy_RxByComRxYawAccInfo.Batt_Range_Err;
     : Proxy_RxByComRxYawAccInfo.Asic_Fail;
     : Proxy_RxByComRxYawAccInfo.Accel_Fail;
     : Proxy_RxByComRxYawAccInfo.Ram_Fail;
     : Proxy_RxByComRxYawAccInfo.Rom_Fail;
     : Proxy_RxByComRxYawAccInfo.Ad_Fail;
     : Proxy_RxByComRxYawAccInfo.Osc_Fail;
     : Proxy_RxByComRxYawAccInfo.Watchdog_Rst;
     : Proxy_RxByComRxYawAccInfo.Plaus_Err_Pst;
     : Proxy_RxByComRxYawAccInfo.RollingCounter;
     : Proxy_RxByComRxYawAccInfo.Can_Func_Err;
     : Proxy_RxByComRxYawAccInfo.AccelEratorRateSig_0;
     : Proxy_RxByComRxYawAccInfo.AccelEratorRateSig_1;
     : Proxy_RxByComRxYawAccInfo.LatAccValidData;
     : Proxy_RxByComRxYawAccInfo.LatAccSelfTestStatus;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComRxTcu6Info(&Proxy_RxByComBus.Proxy_RxByComRxTcu6Info);
    /*==============================================================================
    * Members of structure Proxy_RxByComRxTcu6Info 
     : Proxy_RxByComRxTcu6Info.ShiftClass_Ccan;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComRxTcu5Info(&Proxy_RxByComBus.Proxy_RxByComRxTcu5Info);
    /*==============================================================================
    * Members of structure Proxy_RxByComRxTcu5Info 
     : Proxy_RxByComRxTcu5Info.Typ;
     : Proxy_RxByComRxTcu5Info.GearTyp;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComRxTcu1Info(&Proxy_RxByComBus.Proxy_RxByComRxTcu1Info);
    /*==============================================================================
    * Members of structure Proxy_RxByComRxTcu1Info 
     : Proxy_RxByComRxTcu1Info.Targe;
     : Proxy_RxByComRxTcu1Info.GarChange;
     : Proxy_RxByComRxTcu1Info.Flt;
     : Proxy_RxByComRxTcu1Info.GarSelDisp;
     : Proxy_RxByComRxTcu1Info.TQRedReq_PC;
     : Proxy_RxByComRxTcu1Info.TQRedReqSlw_PC;
     : Proxy_RxByComRxTcu1Info.TQIncReq_PC;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComRxSasInfo(&Proxy_RxByComBus.Proxy_RxByComRxSasInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComRxSasInfo 
     : Proxy_RxByComRxSasInfo.Angle;
     : Proxy_RxByComRxSasInfo.Speed;
     : Proxy_RxByComRxSasInfo.Ok;
     : Proxy_RxByComRxSasInfo.Cal;
     : Proxy_RxByComRxSasInfo.Trim;
     : Proxy_RxByComRxSasInfo.CheckSum;
     : Proxy_RxByComRxSasInfo.MsgCount;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComRxMcu2Info(&Proxy_RxByComBus.Proxy_RxByComRxMcu2Info);
    /*==============================================================================
    * Members of structure Proxy_RxByComRxMcu2Info 
     : Proxy_RxByComRxMcu2Info.Flt;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComRxMcu1Info(&Proxy_RxByComBus.Proxy_RxByComRxMcu1Info);
    /*==============================================================================
    * Members of structure Proxy_RxByComRxMcu1Info 
     : Proxy_RxByComRxMcu1Info.MoTestTQ_PC;
     : Proxy_RxByComRxMcu1Info.MotActRotSpd_RPM;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComRxLongAccInfo(&Proxy_RxByComBus.Proxy_RxByComRxLongAccInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComRxLongAccInfo 
     : Proxy_RxByComRxLongAccInfo.IntSenFltSymtmActive;
     : Proxy_RxByComRxLongAccInfo.IntSenFaultPresent;
     : Proxy_RxByComRxLongAccInfo.LongAccSenCirErrPre;
     : Proxy_RxByComRxLongAccInfo.LonACSenRanChkErrPre;
     : Proxy_RxByComRxLongAccInfo.LongRollingCounter;
     : Proxy_RxByComRxLongAccInfo.IntTempSensorFault;
     : Proxy_RxByComRxLongAccInfo.LongAccInvalidData;
     : Proxy_RxByComRxLongAccInfo.LongAccSelfTstStatus;
     : Proxy_RxByComRxLongAccInfo.LongAccRateSignal_0;
     : Proxy_RxByComRxLongAccInfo.LongAccRateSignal_1;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComRxHcu5Info(&Proxy_RxByComBus.Proxy_RxByComRxHcu5Info);
    /*==============================================================================
    * Members of structure Proxy_RxByComRxHcu5Info 
     : Proxy_RxByComRxHcu5Info.HevMod;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComRxHcu3Info(&Proxy_RxByComBus.Proxy_RxByComRxHcu3Info);
    /*==============================================================================
    * Members of structure Proxy_RxByComRxHcu3Info 
     : Proxy_RxByComRxHcu3Info.TmIntQcMDBINV_PC;
     : Proxy_RxByComRxHcu3Info.MotTQCMC_PC;
     : Proxy_RxByComRxHcu3Info.MotTQCMDBINV_PC;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComRxHcu2Info(&Proxy_RxByComBus.Proxy_RxByComRxHcu2Info);
    /*==============================================================================
    * Members of structure Proxy_RxByComRxHcu2Info 
     : Proxy_RxByComRxHcu2Info.ServiceMod;
     : Proxy_RxByComRxHcu2Info.RegenENA;
     : Proxy_RxByComRxHcu2Info.RegenBRKTQ_NM;
     : Proxy_RxByComRxHcu2Info.CrpTQ_NM;
     : Proxy_RxByComRxHcu2Info.WhlDEMTQ_NM;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComRxHcu1Info(&Proxy_RxByComBus.Proxy_RxByComRxHcu1Info);
    /*==============================================================================
    * Members of structure Proxy_RxByComRxHcu1Info 
     : Proxy_RxByComRxHcu1Info.EngCltStat;
     : Proxy_RxByComRxHcu1Info.HEVRDY;
     : Proxy_RxByComRxHcu1Info.EngTQCmdBinV_PC;
     : Proxy_RxByComRxHcu1Info.EngTQCmd_PC;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComRxFact1Info(&Proxy_RxByComBus.Proxy_RxByComRxFact1Info);
    /*==============================================================================
    * Members of structure Proxy_RxByComRxFact1Info 
     : Proxy_RxByComRxFact1Info.OutTemp_SNR_C;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComRxEms3Info(&Proxy_RxByComBus.Proxy_RxByComRxEms3Info);
    /*==============================================================================
    * Members of structure Proxy_RxByComRxEms3Info 
     : Proxy_RxByComRxEms3Info.EngColTemp_C;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComRxEms2Info(&Proxy_RxByComBus.Proxy_RxByComRxEms2Info);
    /*==============================================================================
    * Members of structure Proxy_RxByComRxEms2Info 
     : Proxy_RxByComRxEms2Info.EngSpdErr;
     : Proxy_RxByComRxEms2Info.AccPedDep_PC;
     : Proxy_RxByComRxEms2Info.Tps_PC;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComRxEms1Info(&Proxy_RxByComBus.Proxy_RxByComRxEms1Info);
    /*==============================================================================
    * Members of structure Proxy_RxByComRxEms1Info 
     : Proxy_RxByComRxEms1Info.TqStd_NM;
     : Proxy_RxByComRxEms1Info.ActINDTQ_PC;
     : Proxy_RxByComRxEms1Info.EngSpd_RPM;
     : Proxy_RxByComRxEms1Info.IndTQ_PC;
     : Proxy_RxByComRxEms1Info.FrictTQ_PC;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComRxClu2Info(&Proxy_RxByComBus.Proxy_RxByComRxClu2Info);
    /*==============================================================================
    * Members of structure Proxy_RxByComRxClu2Info 
     : Proxy_RxByComRxClu2Info.IGN_SW;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComRxClu1Info(&Proxy_RxByComBus.Proxy_RxByComRxClu1Info);
    /*==============================================================================
    * Members of structure Proxy_RxByComRxClu1Info 
     : Proxy_RxByComRxClu1Info.P_Brake_Act;
     : Proxy_RxByComRxClu1Info.Cf_Clu_BrakeFluIDSW;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComRxMsgOkFlgInfo(&Proxy_RxByComBus.Proxy_RxByComRxMsgOkFlgInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComRxMsgOkFlgInfo 
     : Proxy_RxByComRxMsgOkFlgInfo.Bms1MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.YawSerialMsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.YawAccMsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Tcu6MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Tcu5MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Tcu1MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.SasMsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Mcu2MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Mcu1MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.LongAccMsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Hcu5MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Hcu3MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Hcu2MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Hcu1MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Fatc1MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Ems3MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Ems2MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Ems1MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Clu2MsgOkFlg;
     : Proxy_RxByComRxMsgOkFlgInfo.Clu1MsgOkFlg;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo(&Proxy_RxByComBus.Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo 
     : Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.ST_ECU_ST_REPAT_XTRQ_FTAX_BAX;
     : Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.ALIV_ST_REPAT_XTRQ_FTAX_BAX;
     : Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.CRC_ST_REPAT_XTRQ_FTAX_BAX;
     : Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.QU_SER_REPAT_XTRQ_FTAX_BAX_ACT;
     : Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.AVL_REPAT_XTRQ_FTAX_BAX;
     : Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.QU_AVL_REPAT_XTRQ_FTAX_BAX;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComAVL_LTRQD_BAXInfo(&Proxy_RxByComBus.Proxy_RxByComAVL_LTRQD_BAXInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComAVL_LTRQD_BAXInfo 
     : Proxy_RxByComAVL_LTRQD_BAXInfo.QU_SER_LTRQD_BAX;
     : Proxy_RxByComAVL_LTRQD_BAXInfo.CRC_AVL_LTRQD_BAX;
     : Proxy_RxByComAVL_LTRQD_BAXInfo.QU_AVL_LTRQD_BAX;
     : Proxy_RxByComAVL_LTRQD_BAXInfo.AVL_LTRQD_BAX;
     : Proxy_RxByComAVL_LTRQD_BAXInfo.ALIV_AVL_LTRQD_BAX;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo(&Proxy_RxByComBus.Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo 
     : Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.AVL_TORQ_CRSH_DMEE;
     : Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.AVL_TORQ_CRSH;
     : Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.ST_SAIL_DRV_2;
     : Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.CRC_ANG_ACPD;
     : Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.QU_AVL_RPM_ENG_CRSH;
     : Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.AVL_RPM_ENG_CRSH;
     : Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.ALIV_TORQ_CRSH_1;
     : Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.CRC_TORQ_CRSH_1;
     : Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.AVL_ANG_ACPD;
     : Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.QU_AVL_ANG_ACPD;
     : Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.ALIV_ANG_ACPD;
     : Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.ST_ECU_ANG_ACPD;
     : Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.AVL_ANG_ACPD_VIRT;
     : Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.ECO_ANG_ACPD;
     : Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.GRAD_AVL_ANG_ACPD;
     : Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.ST_INTF_DRASY;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info(&Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info);
    /*==============================================================================
    * Members of structure Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info 
     : Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.AVL_RPM_BAX_RED;
     : Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.ST_PENG_PT;
     : Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.ST_AVAI_INTV_PT_DRS;
     : Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.QU_AVL_RPM_BAX_RED;
     : Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.QU_SER_WMOM_PT_SUM_DRS;
     : Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.QU_SER_WMOM_PT_SUM_STAB;
     : Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.ST_ECU_WMOM_DRV_5;
     : Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.TAR_WMOM_PT_SUM_COOTD;
     : Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.QU_SER_COOR_TORQ_BDRV;
     : Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.ST_ECU_WMOM_DRV_4;
     : Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.ST_DRVDIR_DVCH;
     : Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.CRC_WMOM_DRV_4;
     : Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.ALIV_WMOM_DRV_4;
     : Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.ALIV_WMOM_DRV_5;
     : Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.CRC_WMOM_DRV_5;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info(&Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info);
    /*==============================================================================
    * Members of structure Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info 
     : Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.QU_AVL_WMOM_PT_SUM;
     : Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_ERR_AMP;
     : Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.REIN_PT;
     : Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM;
     : Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.CRC_WMOM_DRV_1;
     : Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.ALIV_WMOM_DRV_1;
     : Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.ST_ECU_WMOM_DRV_1;
     : Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_MAX;
     : Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_FAST_TOP;
     : Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_FAST_BOT;
     : Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.ST_ECU_WMOM_DRV_2;
     : Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.QU_REIN_PT;
     : Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.CRC_WMOM_DRV_2;
     : Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.ALIV_WMOM_DRV_2;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComHGLV_VEH_FILTInfo(&Proxy_RxByComBus.Proxy_RxByComHGLV_VEH_FILTInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComHGLV_VEH_FILTInfo 
     : Proxy_RxByComHGLV_VEH_FILTInfo.HGLV_VEH_FILT_FRH;
     : Proxy_RxByComHGLV_VEH_FILTInfo.HGLV_VEH_FILT_FLH;
     : Proxy_RxByComHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_FRH;
     : Proxy_RxByComHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_FLH;
     : Proxy_RxByComHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_RRH;
     : Proxy_RxByComHGLV_VEH_FILTInfo.HGLV_VEH_FILT_RLH;
     : Proxy_RxByComHGLV_VEH_FILTInfo.ALIV_HGLV_VEH_FILT;
     : Proxy_RxByComHGLV_VEH_FILTInfo.HGLV_VEH_FILT_RRH;
     : Proxy_RxByComHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_RLH;
     : Proxy_RxByComHGLV_VEH_FILTInfo.CRC_HGLV_VEH_FILT;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo(&Proxy_RxByComBus.Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo 
     : Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.ATTAV_ESTI;
     : Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.ATTA_ESTI_ERR_AMP;
     : Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.VY_ESTI_ERR_AMP;
     : Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.VY_ESTI;
     : Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.ATTAV_ESTI_ERR_AMP;
     : Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.CRC_VEH_DYNMC_DT_ESTI_VRFD;
     : Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.ALIV_VEH_DYNMC_DT_ESTI_VRFD;
     : Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.QU_VEH_DYNMC_DT_ESTI;
     : Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.ATTA_ESTI;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo(&Proxy_RxByComBus.Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo 
     : Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.QU_ACLNY_COG;
     : Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNY_COG_ERR_AMP;
     : Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ALIV_ACLNX_COG;
     : Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNX_COG;
     : Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ALIV_ACLNY_COG;
     : Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.CRC_ACLNX_COG;
     : Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.CRC_ACLNY_COG;
     : Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNY_COG;
     : Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNX_COG_ERR_AMP;
     : Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.QU_ACLNX_COG;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComV_VEH_V_VEH_2Info(&Proxy_RxByComBus.Proxy_RxByComV_VEH_V_VEH_2Info);
    /*==============================================================================
    * Members of structure Proxy_RxByComV_VEH_V_VEH_2Info 
     : Proxy_RxByComV_VEH_V_VEH_2Info.CRC_V_VEH;
     : Proxy_RxByComV_VEH_V_VEH_2Info.ALIV_V_VEH;
     : Proxy_RxByComV_VEH_V_VEH_2Info.QU_V_VEH_COG;
     : Proxy_RxByComV_VEH_V_VEH_2Info.DVCO_VEH;
     : Proxy_RxByComV_VEH_V_VEH_2Info.ST_V_VEH_NSS;
     : Proxy_RxByComV_VEH_V_VEH_2Info.V_VEH_COG;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo(&Proxy_RxByComBus.Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo 
     : Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo.ALIV_VYAW_VEH;
     : Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo.VYAW_VEH_ERR_AMP;
     : Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo.CRC_VYAW_VEH;
     : Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo.QU_VYAW_VEH;
     : Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo.VYAW_VEH;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo(&Proxy_RxByComBus.Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo 
     : Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo.QU_AVL_TRGR_RW;
     : Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo.AVL_TRGR_RW;
     : Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo.AVL_LOGR_RW_FAST;
     : Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo.ALIV_TLT_RW;
     : Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo.CRC_TLT_RW;
     : Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo.QU_AVL_LOGR_RW;
     : Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo.AVL_LOGR_RW;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComAVL_STEA_FTAXInfo(&Proxy_RxByComBus.Proxy_RxByComAVL_STEA_FTAXInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComAVL_STEA_FTAXInfo 
     : Proxy_RxByComAVL_STEA_FTAXInfo.QU_AVL_STEA_FTAX_PNI;
     : Proxy_RxByComAVL_STEA_FTAXInfo.AVL_STEA_FTAX_PNI;
     : Proxy_RxByComAVL_STEA_FTAXInfo.CRC_AVL_STEA_FTAX;
     : Proxy_RxByComAVL_STEA_FTAXInfo.QU_AVL_STEA_FTAX_WHL;
     : Proxy_RxByComAVL_STEA_FTAXInfo.ALIV_AVL_STEA_FTAX;
     : Proxy_RxByComAVL_STEA_FTAXInfo.AVL_STEA_FTAX_WHL;
     : Proxy_RxByComAVL_STEA_FTAXInfo.AVL_STEA_FTAX_WHL_ERR_AMP;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo(&Proxy_RxByComBus.Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo 
     : Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_PRMSN_DBC_DCRN_MAX;
     : Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_PRF_BRK;
     : Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_V_HDC;
     : Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_PRMSN_DBC_DCRN_GRAD_MAX;
     : Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.ALIV_SPEC_PRMSN_IBRK_HDC;
     : Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.CRC_SPEC_PRMSN_IBRK_HDC;
     : Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_PRMSN_DBC_TR_THRV;
     : Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_BRTORQ_SUM;
     : Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_BRTORQ_SUM;
     : Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_PRMSN_DBC;
     : Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.RQ_TAO_SSM;
     : Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.CRC_RQ_BRTORQ_SUM;
     : Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.ALIV_RQ_BRTORQ_SUM;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info(&Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info);
    /*==============================================================================
    * Members of structure Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info 
     : Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_DTORQ_BOT;
     : Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_RECUP_MAX;
     : Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_ILS;
     : Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.CRC_WMOM_DRV_3;
     : Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.ALIV_WMOM_DRV_3;
     : Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_DTORQ_TOP;
     : Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.ST_ECU_WMOM_DRV_6;
     : Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.ALIV_WMOM_DRV_6;
     : Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.CRC_WMOM_DRV_6;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_7Info(&Proxy_RxByComBus.Proxy_RxByComWMOM_DRV_7Info);
    /*==============================================================================
    * Members of structure Proxy_RxByComWMOM_DRV_7Info 
     : Proxy_RxByComWMOM_DRV_7Info.ST_EL_DRVG;
     : Proxy_RxByComWMOM_DRV_7Info.PRD_AVL_WMOM_PT_SUM_RECUP_MAX;
     : Proxy_RxByComWMOM_DRV_7Info.ALIV_WMOM_DRV_7;
     : Proxy_RxByComWMOM_DRV_7Info.CRC_WMOM_DRV_7;
     : Proxy_RxByComWMOM_DRV_7Info.ST_ECU_WMOM_DRV_7;
     : Proxy_RxByComWMOM_DRV_7Info.QU_SER_WMOM_PT_SUM_RECUP;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo(&Proxy_RxByComBus.Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo 
     : Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.TAR_DIFF_BRTORQ_BAX_YMR;
     : Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.FACT_TAR_COMPT_DRV_YMR;
     : Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.QU_TAR_DIFF_BRTORQ_YMR;
     : Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.CRC_RQ_DIFF_BRTORQ_YMR;
     : Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.ALIV_RQ_DIFF_BRTORQ_YMR;
     : Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.TAR_DIFF_BRTORQ_FTAX_YMR;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComDT_BRKSYS_ENGMGInfo(&Proxy_RxByComBus.Proxy_RxByComDT_BRKSYS_ENGMGInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComDT_BRKSYS_ENGMGInfo 
     : Proxy_RxByComDT_BRKSYS_ENGMGInfo.QU_AVL_LOWP_BRKFA;
     : Proxy_RxByComDT_BRKSYS_ENGMGInfo.AVL_LOWP_BRKFA;
     : Proxy_RxByComDT_BRKSYS_ENGMGInfo.CRC_DT_BRKSYS_ENGMG;
     : Proxy_RxByComDT_BRKSYS_ENGMGInfo.ALIV_DT_BRKSYS_ENGMG;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComERRM_BN_UInfo(&Proxy_RxByComBus.Proxy_RxByComERRM_BN_UInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComERRM_BN_UInfo 
     : Proxy_RxByComERRM_BN_UInfo.CTR_ERRM_BN_U;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComCTR_CRInfo(&Proxy_RxByComBus.Proxy_RxByComCTR_CRInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComCTR_CRInfo 
     : Proxy_RxByComCTR_CRInfo.ALIV_CTR_CR;
     : Proxy_RxByComCTR_CRInfo.ST_EXCE_ACLN_THRV;
     : Proxy_RxByComCTR_CRInfo.CTR_PHTR_CR;
     : Proxy_RxByComCTR_CRInfo.CTR_ITLI_CR;
     : Proxy_RxByComCTR_CRInfo.CTR_CLSY_CR;
     : Proxy_RxByComCTR_CRInfo.CTR_AUTOM_ECAL_CR;
     : Proxy_RxByComCTR_CRInfo.CTR_SWO_EKP_CR;
     : Proxy_RxByComCTR_CRInfo.CRC_CTR_CR;
     : Proxy_RxByComCTR_CRInfo.CTR_PCSH_MST;
     : Proxy_RxByComCTR_CRInfo.CTR_HAZW_CR;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComKLEMMENInfo(&Proxy_RxByComBus.Proxy_RxByComKLEMMENInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComKLEMMENInfo 
     : Proxy_RxByComKLEMMENInfo.ST_OP_MSA;
     : Proxy_RxByComKLEMMENInfo.RQ_DRVG_RDI;
     : Proxy_RxByComKLEMMENInfo.ST_KL_DBG;
     : Proxy_RxByComKLEMMENInfo.ST_KL_50_MSA;
     : Proxy_RxByComKLEMMENInfo.ST_SSP;
     : Proxy_RxByComKLEMMENInfo.RWDT_BLS;
     : Proxy_RxByComKLEMMENInfo.ST_STCD_PENG;
     : Proxy_RxByComKLEMMENInfo.ST_KL;
     : Proxy_RxByComKLEMMENInfo.ST_KL_DIV;
     : Proxy_RxByComKLEMMENInfo.ST_VEH_CON;
     : Proxy_RxByComKLEMMENInfo.CRC_KL;
     : Proxy_RxByComKLEMMENInfo.ALIV_COU_KL;
     : Proxy_RxByComKLEMMENInfo.ST_KL_30B;
     : Proxy_RxByComKLEMMENInfo.CON_CLT_SW;
     : Proxy_RxByComKLEMMENInfo.CTR_ENG_STOP;
     : Proxy_RxByComKLEMMENInfo.ST_PLK;
     : Proxy_RxByComKLEMMENInfo.ST_KL_15N;
     : Proxy_RxByComKLEMMENInfo.ST_KL_KEY_VLD;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info(&Proxy_RxByComBus.Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info);
    /*==============================================================================
    * Members of structure Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info 
     : Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_MES_TSTMP;
     : Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.PCKG_ID;
     : Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.SUPP_ID;
     : Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_5;
     : Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_8;
     : Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_7;
     : Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_6;
     : Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.ALIV_RDC_DT_PCKG_2;
     : Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_1;
     : Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.TYR_ID;
     : Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.ALIV_RDC_DT_PCKG_1;
     : Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_4;
     : Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_3;
     : Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_2;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComBEDIENUNG_WISCHERInfo(&Proxy_RxByComBus.Proxy_RxByComBEDIENUNG_WISCHERInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComBEDIENUNG_WISCHERInfo 
     : Proxy_RxByComBEDIENUNG_WISCHERInfo.ALIV_WISW;
     : Proxy_RxByComBEDIENUNG_WISCHERInfo.OP_WISW;
     : Proxy_RxByComBEDIENUNG_WISCHERInfo.OP_WIPO;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComDT_PT_2Info(&Proxy_RxByComBus.Proxy_RxByComDT_PT_2Info);
    /*==============================================================================
    * Members of structure Proxy_RxByComDT_PT_2Info 
     : Proxy_RxByComDT_PT_2Info.ST_GRSEL_DRV;
     : Proxy_RxByComDT_PT_2Info.TEMP_EOI_DRV;
     : Proxy_RxByComDT_PT_2Info.RLS_ENGSTA;
     : Proxy_RxByComDT_PT_2Info.RPM_ENG_MAX_ALW;
     : Proxy_RxByComDT_PT_2Info.RDUC_DOCTR_RPM_DRV_2;
     : Proxy_RxByComDT_PT_2Info.TEMP_ENG_DRV;
     : Proxy_RxByComDT_PT_2Info.ST_DRV_VEH;
     : Proxy_RxByComDT_PT_2Info.ST_ECU_DT_PT_2;
     : Proxy_RxByComDT_PT_2Info.ST_IDLG_ENG_DRV;
     : Proxy_RxByComDT_PT_2Info.ST_ILK_STRT_DRV;
     : Proxy_RxByComDT_PT_2Info.ST_SW_CLT_DRV;
     : Proxy_RxByComDT_PT_2Info.CRC_DT_PT_2;
     : Proxy_RxByComDT_PT_2Info.ALIV_DT_PT_2;
     : Proxy_RxByComDT_PT_2Info.ST_ENG_RUN_DRV;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComSTAT_ENG_STA_AUTOInfo(&Proxy_RxByComBus.Proxy_RxByComSTAT_ENG_STA_AUTOInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComSTAT_ENG_STA_AUTOInfo 
     : Proxy_RxByComSTAT_ENG_STA_AUTOInfo.RQ_MSA_ENG_STA_1;
     : Proxy_RxByComSTAT_ENG_STA_AUTOInfo.RQ_MSA_ENG_STA_2;
     : Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ALIV_ST_ENG_STA_AUTO;
     : Proxy_RxByComSTAT_ENG_STA_AUTOInfo.PSBTY_MSA_ENG_STOP_STA;
     : Proxy_RxByComSTAT_ENG_STA_AUTOInfo.CRC_ST_ENG_STA_AUTO;
     : Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_SHFT_MSA_ENGSTP;
     : Proxy_RxByComSTAT_ENG_STA_AUTOInfo.RQ_SLIP_K0;
     : Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_TAR_PENG_CENG;
     : Proxy_RxByComSTAT_ENG_STA_AUTOInfo.SPEC_TYP_ENGSTA;
     : Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_RPM_CLCTR_MOT;
     : Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_FN_MSA;
     : Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_AVL_PENG_CENG_ENGMG;
     : Proxy_RxByComSTAT_ENG_STA_AUTOInfo.DISP_REAS_PREV_SWO_CENG;
     : Proxy_RxByComSTAT_ENG_STA_AUTOInfo.VARI_TYP_ENGSTA;
     : Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_TAR_CENG;
     : Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_AVAI_SAIL_DME;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComST_ENERG_GENInfo(&Proxy_RxByComBus.Proxy_RxByComST_ENERG_GENInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComST_ENERG_GENInfo 
     : Proxy_RxByComST_ENERG_GENInfo.ST_LDST_GEN_DRV;
     : Proxy_RxByComST_ENERG_GENInfo.DT_PCU_SCP;
     : Proxy_RxByComST_ENERG_GENInfo.ST_GEN_DRV;
     : Proxy_RxByComST_ENERG_GENInfo.AVL_I_GEN_DRV;
     : Proxy_RxByComST_ENERG_GENInfo.LDST_GEN_DRV;
     : Proxy_RxByComST_ENERG_GENInfo.ST_LDREL_GEN;
     : Proxy_RxByComST_ENERG_GENInfo.ST_CHG_STOR;
     : Proxy_RxByComST_ENERG_GENInfo.TEMP_BT_14V;
     : Proxy_RxByComST_ENERG_GENInfo.ST_I_IBS;
     : Proxy_RxByComST_ENERG_GENInfo.ST_SEP_STOR;
     : Proxy_RxByComST_ENERG_GENInfo.ST_BN2_SCP;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComDT_PT_1Info(&Proxy_RxByComBus.Proxy_RxByComDT_PT_1Info);
    /*==============================================================================
    * Members of structure Proxy_RxByComDT_PT_1Info 
     : Proxy_RxByComDT_PT_1Info.RQ_SHPA_GRB_REGE_PAFI;
     : Proxy_RxByComDT_PT_1Info.ST_RTIR_DRV;
     : Proxy_RxByComDT_PT_1Info.CTR_SLCK_DRV;
     : Proxy_RxByComDT_PT_1Info.RQ_STASS_ENGMG;
     : Proxy_RxByComDT_PT_1Info.ST_CAT_HT;
     : Proxy_RxByComDT_PT_1Info.RQ_SHPA_GRB_CHGBLC;
     : Proxy_RxByComDT_PT_1Info.TAR_RPM_IDLG_DRV_EXT;
     : Proxy_RxByComDT_PT_1Info.SLCTN_BUS_COMM_ENG_GRB;
     : Proxy_RxByComDT_PT_1Info.TAR_RPM_IDLG_DRV;
     : Proxy_RxByComDT_PT_1Info.ST_INFS_DRV;
     : Proxy_RxByComDT_PT_1Info.ST_SW_WAUP_DRV;
     : Proxy_RxByComDT_PT_1Info.AIP_ENG_DRV;
     : Proxy_RxByComDT_PT_1Info.RDUC_DOCTR_RPM_DRV;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComDT_GRDT_DRVInfo(&Proxy_RxByComBus.Proxy_RxByComDT_GRDT_DRVInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComDT_GRDT_DRVInfo 
     : Proxy_RxByComDT_GRDT_DRVInfo.ST_OPMO_GRDT_DRV;
     : Proxy_RxByComDT_GRDT_DRVInfo.ST_RSTA_GRDT;
     : Proxy_RxByComDT_GRDT_DRVInfo.CRC_GRDT_DRV;
     : Proxy_RxByComDT_GRDT_DRVInfo.ALIV_GRDT_DRV;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComDIAG_OBD_ENGMG_ELInfo(&Proxy_RxByComBus.Proxy_RxByComDIAG_OBD_ENGMG_ELInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComDIAG_OBD_ENGMG_ELInfo 
     : Proxy_RxByComDIAG_OBD_ENGMG_ELInfo.RQ_MIL_DIAG_OBD_ENGMG_EL;
     : Proxy_RxByComDIAG_OBD_ENGMG_ELInfo.RQ_RST_OBD_DIAG;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComSTAT_CT_HABRInfo(&Proxy_RxByComBus.Proxy_RxByComSTAT_CT_HABRInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComSTAT_CT_HABRInfo 
     : Proxy_RxByComSTAT_CT_HABRInfo.ST_CT_HABR;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComDT_PT_3Info(&Proxy_RxByComBus.Proxy_RxByComDT_PT_3Info);
    /*==============================================================================
    * Members of structure Proxy_RxByComDT_PT_3Info 
     : Proxy_RxByComDT_PT_3Info.TRNRAO_BAX;
     : Proxy_RxByComDT_PT_3Info.QU_TRNRAO_BAX;
     : Proxy_RxByComDT_PT_3Info.ALIV_DT_PT_3;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComEINHEITEN_BN2020Info(&Proxy_RxByComBus.Proxy_RxByComEINHEITEN_BN2020Info);
    /*==============================================================================
    * Members of structure Proxy_RxByComEINHEITEN_BN2020Info 
     : Proxy_RxByComEINHEITEN_BN2020Info.UN_TORQ_S_MOD;
     : Proxy_RxByComEINHEITEN_BN2020Info.UN_PWR_S_MOD;
     : Proxy_RxByComEINHEITEN_BN2020Info.UN_DATE_EXT;
     : Proxy_RxByComEINHEITEN_BN2020Info.UN_COSP_EL;
     : Proxy_RxByComEINHEITEN_BN2020Info.UN_TEMP;
     : Proxy_RxByComEINHEITEN_BN2020Info.UN_AIP;
     : Proxy_RxByComEINHEITEN_BN2020Info.LANG;
     : Proxy_RxByComEINHEITEN_BN2020Info.UN_DATE;
     : Proxy_RxByComEINHEITEN_BN2020Info.UN_T;
     : Proxy_RxByComEINHEITEN_BN2020Info.UN_SPDM_DGTL;
     : Proxy_RxByComEINHEITEN_BN2020Info.UN_MILE;
     : Proxy_RxByComEINHEITEN_BN2020Info.UN_FU;
     : Proxy_RxByComEINHEITEN_BN2020Info.UN_COSP;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComA_TEMPInfo(&Proxy_RxByComBus.Proxy_RxByComA_TEMPInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComA_TEMPInfo 
     : Proxy_RxByComA_TEMPInfo.TEMP_EX_UNFILT;
     : Proxy_RxByComA_TEMPInfo.TEMP_EX;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComWISCHERGESCHWINDIGKEITInfo(&Proxy_RxByComBus.Proxy_RxByComWISCHERGESCHWINDIGKEITInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComWISCHERGESCHWINDIGKEITInfo 
     : Proxy_RxByComWISCHERGESCHWINDIGKEITInfo.ST_RNSE;
     : Proxy_RxByComWISCHERGESCHWINDIGKEITInfo.INT_RN;
     : Proxy_RxByComWISCHERGESCHWINDIGKEITInfo.V_WI;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComSTAT_DS_VRFDInfo(&Proxy_RxByComBus.Proxy_RxByComSTAT_DS_VRFDInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComSTAT_DS_VRFDInfo 
     : Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_DRD_SFY_CTRL;
     : Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_DVDR_VRFD;
     : Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_DVDR_SFY_CTRL;
     : Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_PSDR_SFY_CTRL;
     : Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_PSD_SFY_CTRL;
     : Proxy_RxByComSTAT_DS_VRFDInfo.ALIV_ST_DSW_VRFD;
     : Proxy_RxByComSTAT_DS_VRFDInfo.CRC_ST_DSW_VRFD;
     : Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_DRD_VRFD;
     : Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_PSDR_VRFD;
     : Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_PSD_VRFD;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info(&Proxy_RxByComBus.Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info);
    /*==============================================================================
    * Members of structure Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info 
     : Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY;
     : Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.RQ_SW_DRDY_MMID;
     : Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.RQ_SU_SW_DRDY;
     : Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.CRC_SU_SW_DRDY;
     : Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.ALIV_SU_SW_DRDY;
     : Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.RQ_SW_DRDY_KDIS;
     : Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY_CHAS;
     : Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY_DRV;
     : Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY_STAB;
     : Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.DISP_ST_DSC;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComSTAT_ANHAENGERInfo(&Proxy_RxByComBus.Proxy_RxByComSTAT_ANHAENGERInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComSTAT_ANHAENGERInfo 
     : Proxy_RxByComSTAT_ANHAENGERInfo.ST_SYNCN_HAZWCL_TRAI;
     : Proxy_RxByComSTAT_ANHAENGERInfo.ST_RFLI_DF_TRAI;
     : Proxy_RxByComSTAT_ANHAENGERInfo.ST_TRAI;
     : Proxy_RxByComSTAT_ANHAENGERInfo.ST_DI_DF_TRAI;
     : Proxy_RxByComSTAT_ANHAENGERInfo.ST_PO_AHV;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComFZZSTDInfo(&Proxy_RxByComBus.Proxy_RxByComFZZSTDInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComFZZSTDInfo 
     : Proxy_RxByComFZZSTDInfo.ST_ILK_ERRM_FZM;
     : Proxy_RxByComFZZSTDInfo.ST_ENERG_FZM;
     : Proxy_RxByComFZZSTDInfo.ST_BT_PROTE_WUP;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComBEDIENUNG_FAHRWERKInfo(&Proxy_RxByComBus.Proxy_RxByComBEDIENUNG_FAHRWERKInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComBEDIENUNG_FAHRWERKInfo 
     : Proxy_RxByComBEDIENUNG_FAHRWERKInfo.OP_MOD_TRCT_DSC;
     : Proxy_RxByComBEDIENUNG_FAHRWERKInfo.OP_TPCT;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComFAHRZEUGTYPInfo(&Proxy_RxByComBus.Proxy_RxByComFAHRZEUGTYPInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComFAHRZEUGTYPInfo 
     : Proxy_RxByComFAHRZEUGTYPInfo.QUAN_CYL;
     : Proxy_RxByComFAHRZEUGTYPInfo.QUAN_GR;
     : Proxy_RxByComFAHRZEUGTYPInfo.TYP_VEH;
     : Proxy_RxByComFAHRZEUGTYPInfo.TYP_BODY;
     : Proxy_RxByComFAHRZEUGTYPInfo.TYP_ENG;
     : Proxy_RxByComFAHRZEUGTYPInfo.CLAS_PWR;
     : Proxy_RxByComFAHRZEUGTYPInfo.TYP_CNT;
     : Proxy_RxByComFAHRZEUGTYPInfo.TYP_STE;
     : Proxy_RxByComFAHRZEUGTYPInfo.TYP_GRB;
     : Proxy_RxByComFAHRZEUGTYPInfo.TYP_CAPA;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComST_BLT_CT_SOCCUInfo(&Proxy_RxByComBus.Proxy_RxByComST_BLT_CT_SOCCUInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComST_BLT_CT_SOCCUInfo 
     : Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_RRH;
     : Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_BLTB_SW_RM;
     : Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_BLTB_SW_RRH;
     : Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_ERR_SEAT_MT_DR;
     : Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_BLTB_SW_DR;
     : Proxy_RxByComST_BLT_CT_SOCCUInfo.CRC_ST_BLT_CT_SOCCU;
     : Proxy_RxByComST_BLT_CT_SOCCUInfo.ALIV_COU_ST_BLT_CT_SOCCU;
     : Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_DR;
     : Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_BLTB_SW_RLH;
     : Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_RLH;
     : Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_BLTB_SW_PS;
     : Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_PS;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComFAHRGESTELLNUMMERInfo(&Proxy_RxByComBus.Proxy_RxByComFAHRGESTELLNUMMERInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComFAHRGESTELLNUMMERInfo 
     : Proxy_RxByComFAHRGESTELLNUMMERInfo.NO_VECH_1;
     : Proxy_RxByComFAHRGESTELLNUMMERInfo.NO_VECH_2;
     : Proxy_RxByComFAHRGESTELLNUMMERInfo.NO_VECH_3;
     : Proxy_RxByComFAHRGESTELLNUMMERInfo.NO_VECH_6;
     : Proxy_RxByComFAHRGESTELLNUMMERInfo.NO_VECH_7;
     : Proxy_RxByComFAHRGESTELLNUMMERInfo.NO_VECH_4;
     : Proxy_RxByComFAHRGESTELLNUMMERInfo.NO_VECH_5;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComRELATIVZEITInfo(&Proxy_RxByComBus.Proxy_RxByComRELATIVZEITInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComRELATIVZEITInfo 
     : Proxy_RxByComRELATIVZEITInfo.T_SEC_COU_REL;
     : Proxy_RxByComRELATIVZEITInfo.T_DAY_COU_ABSL;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComKILOMETERSTANDInfo(&Proxy_RxByComBus.Proxy_RxByComKILOMETERSTANDInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComKILOMETERSTANDInfo 
     : Proxy_RxByComKILOMETERSTANDInfo.RNG;
     : Proxy_RxByComKILOMETERSTANDInfo.ST_FLLV_FUTA_SPAR;
     : Proxy_RxByComKILOMETERSTANDInfo.FLLV_FUTA_RH;
     : Proxy_RxByComKILOMETERSTANDInfo.FLLV_FUTA_LH;
     : Proxy_RxByComKILOMETERSTANDInfo.FLLV_FUTA;
     : Proxy_RxByComKILOMETERSTANDInfo.MILE_KM;
     =============================================================================*/
    
    Proxy_RxByCom_Write_Proxy_RxByComUHRZEIT_DATUMInfo(&Proxy_RxByComBus.Proxy_RxByComUHRZEIT_DATUMInfo);
    /*==============================================================================
    * Members of structure Proxy_RxByComUHRZEIT_DATUMInfo 
     : Proxy_RxByComUHRZEIT_DATUMInfo.DISP_DATE_WDAY;
     : Proxy_RxByComUHRZEIT_DATUMInfo.DISP_DATE_DAY;
     : Proxy_RxByComUHRZEIT_DATUMInfo.DISP_DATE_MON;
     : Proxy_RxByComUHRZEIT_DATUMInfo.ST_DISP_CTI_DATE;
     : Proxy_RxByComUHRZEIT_DATUMInfo.DISP_DATE_YR;
     : Proxy_RxByComUHRZEIT_DATUMInfo.DISP_HR;
     : Proxy_RxByComUHRZEIT_DATUMInfo.DISP_SEC;
     : Proxy_RxByComUHRZEIT_DATUMInfo.DISP_MN;
     =============================================================================*/
    

    Proxy_RxByCom_Timer_Elapsed = STM0_TIM0.U - Proxy_RxByCom_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define PROXY_RXBYCOM_STOP_SEC_CODE
#include "Proxy_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
