/**
 * @defgroup Proxy_Rx Proxy_Rx
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Proxy_Rx.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Proxy_Rx.h"
#include "Proxy_Rx_Ifa.h"
#include "IfxStm_reg.h"
#include "Proxy_Decode.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PROXY_RX_START_SEC_CONST_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PROXY_RX_STOP_SEC_CONST_UNSPECIFIED
#include "Proxy_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PROXY_RX_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Proxy_Rx_HdrBusType Proxy_RxBus;

/* Version Info */
const SwcVersionInfo_t Proxy_RxVersionInfo = 
{   
    PROXY_RX_MODULE_ID,           /* Proxy_RxVersionInfo.ModuleId */
    PROXY_RX_MAJOR_VERSION,       /* Proxy_RxVersionInfo.MajorVer */
    PROXY_RX_MINOR_VERSION,       /* Proxy_RxVersionInfo.MinorVer */
    PROXY_RX_PATCH_VERSION,       /* Proxy_RxVersionInfo.PatchVer */
    PROXY_RX_BRANCH_VERSION       /* Proxy_RxVersionInfo.BranchVer */
};
    
/* Input Data Element */
Proxy_RxByComRxBms1Info_t Proxy_RxRxBms1Info;
Proxy_RxByComRxYawSerialInfo_t Proxy_RxRxYawSerialInfo;
Proxy_RxByComRxYawAccInfo_t Proxy_RxRxYawAccInfo;
Proxy_RxByComRxTcu6Info_t Proxy_RxRxTcu6Info;
Proxy_RxByComRxTcu5Info_t Proxy_RxRxTcu5Info;
Proxy_RxByComRxTcu1Info_t Proxy_RxRxTcu1Info;
Proxy_RxByComRxSasInfo_t Proxy_RxRxSasInfo;
Proxy_RxByComRxMcu2Info_t Proxy_RxRxMcu2Info;
Proxy_RxByComRxMcu1Info_t Proxy_RxRxMcu1Info;
Proxy_RxByComRxLongAccInfo_t Proxy_RxRxLongAccInfo;
Proxy_RxByComRxHcu5Info_t Proxy_RxRxHcu5Info;
Proxy_RxByComRxHcu3Info_t Proxy_RxRxHcu3Info;
Proxy_RxByComRxHcu2Info_t Proxy_RxRxHcu2Info;
Proxy_RxByComRxHcu1Info_t Proxy_RxRxHcu1Info;
Proxy_RxByComRxFact1Info_t Proxy_RxRxFact1Info;
Proxy_RxByComRxEms3Info_t Proxy_RxRxEms3Info;
Proxy_RxByComRxEms2Info_t Proxy_RxRxEms2Info;
Proxy_RxByComRxEms1Info_t Proxy_RxRxEms1Info;
Proxy_RxByComRxClu2Info_t Proxy_RxRxClu2Info;
Proxy_RxByComRxClu1Info_t Proxy_RxRxClu1Info;
Proxy_RxByComRxMsgOkFlgInfo_t Proxy_RxRxMsgOkFlgInfo;
Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo_t Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo;
Proxy_RxByComAVL_LTRQD_BAXInfo_t Proxy_RxAVL_LTRQD_BAXInfo;
Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo_t Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo;
Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info_t Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info;
Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info_t Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info;
Proxy_RxByComHGLV_VEH_FILTInfo_t Proxy_RxHGLV_VEH_FILTInfo;
Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo_t Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo;
Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo_t Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo;
Proxy_RxByComV_VEH_V_VEH_2Info_t Proxy_RxV_VEH_V_VEH_2Info;
Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo_t Proxy_RxDT_DRDYSEN_EXT_VYAW_VEHInfo;
Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo_t Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo;
Proxy_RxByComAVL_STEA_FTAXInfo_t Proxy_RxAVL_STEA_FTAXInfo;
Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_t Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo;
Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info_t Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info;
Proxy_RxByComWMOM_DRV_7Info_t Proxy_RxWMOM_DRV_7Info;
Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo_t Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo;
Proxy_RxByComDT_BRKSYS_ENGMGInfo_t Proxy_RxDT_BRKSYS_ENGMGInfo;
Proxy_RxByComERRM_BN_UInfo_t Proxy_RxERRM_BN_UInfo;
Proxy_RxByComCTR_CRInfo_t Proxy_RxCTR_CRInfo;
Proxy_RxByComKLEMMENInfo_t Proxy_RxKLEMMENInfo;
Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_t Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info;
Proxy_RxByComBEDIENUNG_WISCHERInfo_t Proxy_RxBEDIENUNG_WISCHERInfo;
Proxy_RxByComDT_PT_2Info_t Proxy_RxDT_PT_2Info;
Proxy_RxByComSTAT_ENG_STA_AUTOInfo_t Proxy_RxSTAT_ENG_STA_AUTOInfo;
Proxy_RxByComST_ENERG_GENInfo_t Proxy_RxST_ENERG_GENInfo;
Proxy_RxByComDT_PT_1Info_t Proxy_RxDT_PT_1Info;
Proxy_RxByComDT_GRDT_DRVInfo_t Proxy_RxDT_GRDT_DRVInfo;
Proxy_RxByComDIAG_OBD_ENGMG_ELInfo_t Proxy_RxDIAG_OBD_ENGMG_ELInfo;
Proxy_RxByComSTAT_CT_HABRInfo_t Proxy_RxSTAT_CT_HABRInfo;
Proxy_RxByComDT_PT_3Info_t Proxy_RxDT_PT_3Info;
Proxy_RxByComEINHEITEN_BN2020Info_t Proxy_RxEINHEITEN_BN2020Info;
Proxy_RxByComA_TEMPInfo_t Proxy_RxA_TEMPInfo;
Proxy_RxByComWISCHERGESCHWINDIGKEITInfo_t Proxy_RxWISCHERGESCHWINDIGKEITInfo;
Proxy_RxByComSTAT_DS_VRFDInfo_t Proxy_RxSTAT_DS_VRFDInfo;
Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info_t Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info;
Proxy_RxByComSTAT_ANHAENGERInfo_t Proxy_RxSTAT_ANHAENGERInfo;
Proxy_RxByComFZZSTDInfo_t Proxy_RxFZZSTDInfo;
Proxy_RxByComBEDIENUNG_FAHRWERKInfo_t Proxy_RxBEDIENUNG_FAHRWERKInfo;
Proxy_RxByComFAHRZEUGTYPInfo_t Proxy_RxFAHRZEUGTYPInfo;
Proxy_RxByComST_BLT_CT_SOCCUInfo_t Proxy_RxST_BLT_CT_SOCCUInfo;
Proxy_RxByComFAHRGESTELLNUMMERInfo_t Proxy_RxFAHRGESTELLNUMMERInfo;
Proxy_RxByComRELATIVZEITInfo_t Proxy_RxRELATIVZEITInfo;
Proxy_RxByComKILOMETERSTANDInfo_t Proxy_RxKILOMETERSTANDInfo;
Proxy_RxByComUHRZEIT_DATUMInfo_t Proxy_RxUHRZEIT_DATUMInfo;
Mom_HndlrEcuModeSts_t Proxy_RxEcuModeSts;
Pct_5msCtrlPCtrlAct_t Proxy_RxPCtrlAct;
Eem_SuspcDetnFuncInhibitProxySts_t Proxy_RxFuncInhibitProxySts;

/* Output Data Element */
Proxy_RxCanRxRegenInfo_t Proxy_RxCanRxRegenInfo;
Proxy_RxCanRxAccelPedlInfo_t Proxy_RxCanRxAccelPedlInfo;
Proxy_RxCanRxIdbInfo_t Proxy_RxCanRxIdbInfo;
Proxy_RxCanRxEngTempInfo_t Proxy_RxCanRxEngTempInfo;
Proxy_RxCanRxEscInfo_t Proxy_RxCanRxEscInfo;
Proxy_RxCanRxClu2Info_t Proxy_RxCanRxClu2Info;
Proxy_RxCanRxEemInfo_t Proxy_RxCanRxEemInfo;
Proxy_RxCanRxInfo_t Proxy_RxCanRxInfo;
Proxy_RxIMUCalc_t Proxy_RxIMUCalcInfo;
Proxy_RxCanRxGearSelDispErrInfo_t Proxy_RxCanRxGearSelDispErrInfo;

uint32 Proxy_Rx_Timer_Start;
uint32 Proxy_Rx_Timer_Elapsed;

#define PROXY_RX_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Proxy_MemMap.h"
#define PROXY_RX_START_SEC_VAR_NOINIT_32BIT
#include "Proxy_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PROXY_RX_STOP_SEC_VAR_NOINIT_32BIT
#include "Proxy_MemMap.h"
#define PROXY_RX_START_SEC_VAR_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PROXY_RX_STOP_SEC_VAR_UNSPECIFIED
#include "Proxy_MemMap.h"
#define PROXY_RX_START_SEC_VAR_32BIT
#include "Proxy_MemMap.h"
/** Variable Section (32BIT)**/


#define PROXY_RX_STOP_SEC_VAR_32BIT
#include "Proxy_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define PROXY_RX_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PROXY_RX_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Proxy_MemMap.h"
#define PROXY_RX_START_SEC_VAR_NOINIT_32BIT
#include "Proxy_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PROXY_RX_STOP_SEC_VAR_NOINIT_32BIT
#include "Proxy_MemMap.h"
#define PROXY_RX_START_SEC_VAR_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PROXY_RX_STOP_SEC_VAR_UNSPECIFIED
#include "Proxy_MemMap.h"
#define PROXY_RX_START_SEC_VAR_32BIT
#include "Proxy_MemMap.h"
/** Variable Section (32BIT)**/


#define PROXY_RX_STOP_SEC_VAR_32BIT
#include "Proxy_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PROXY_RX_START_SEC_CODE
#include "Proxy_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Proxy_Rx_Init(void)
{
    /* Initialize internal bus */
    Proxy_RxBus.Proxy_RxRxBms1Info.Bms1SocPc = 0;
    Proxy_RxBus.Proxy_RxRxYawSerialInfo.YawSerialNum_0 = 0;
    Proxy_RxBus.Proxy_RxRxYawSerialInfo.YawSerialNum_1 = 0;
    Proxy_RxBus.Proxy_RxRxYawSerialInfo.YawSerialNum_2 = 0;
    Proxy_RxBus.Proxy_RxRxYawAccInfo.YawRateValidData = 0;
    Proxy_RxBus.Proxy_RxRxYawAccInfo.YawRateSelfTestStatus = 0;
    Proxy_RxBus.Proxy_RxRxYawAccInfo.YawRateSignal_0 = 0;
    Proxy_RxBus.Proxy_RxRxYawAccInfo.YawRateSignal_1 = 0;
    Proxy_RxBus.Proxy_RxRxYawAccInfo.SensorOscFreqDev = 0;
    Proxy_RxBus.Proxy_RxRxYawAccInfo.Gyro_Fail = 0;
    Proxy_RxBus.Proxy_RxRxYawAccInfo.Raster_Fail = 0;
    Proxy_RxBus.Proxy_RxRxYawAccInfo.Eep_Fail = 0;
    Proxy_RxBus.Proxy_RxRxYawAccInfo.Batt_Range_Err = 0;
    Proxy_RxBus.Proxy_RxRxYawAccInfo.Asic_Fail = 0;
    Proxy_RxBus.Proxy_RxRxYawAccInfo.Accel_Fail = 0;
    Proxy_RxBus.Proxy_RxRxYawAccInfo.Ram_Fail = 0;
    Proxy_RxBus.Proxy_RxRxYawAccInfo.Rom_Fail = 0;
    Proxy_RxBus.Proxy_RxRxYawAccInfo.Ad_Fail = 0;
    Proxy_RxBus.Proxy_RxRxYawAccInfo.Osc_Fail = 0;
    Proxy_RxBus.Proxy_RxRxYawAccInfo.Watchdog_Rst = 0;
    Proxy_RxBus.Proxy_RxRxYawAccInfo.Plaus_Err_Pst = 0;
    Proxy_RxBus.Proxy_RxRxYawAccInfo.RollingCounter = 0;
    Proxy_RxBus.Proxy_RxRxYawAccInfo.Can_Func_Err = 0;
    Proxy_RxBus.Proxy_RxRxYawAccInfo.AccelEratorRateSig_0 = 0;
    Proxy_RxBus.Proxy_RxRxYawAccInfo.AccelEratorRateSig_1 = 0;
    Proxy_RxBus.Proxy_RxRxYawAccInfo.LatAccValidData = 0;
    Proxy_RxBus.Proxy_RxRxYawAccInfo.LatAccSelfTestStatus = 0;
    Proxy_RxBus.Proxy_RxRxTcu6Info.ShiftClass_Ccan = 0;
    Proxy_RxBus.Proxy_RxRxTcu5Info.Typ = 0;
    Proxy_RxBus.Proxy_RxRxTcu5Info.GearTyp = 0;
    Proxy_RxBus.Proxy_RxRxTcu1Info.Targe = 0;
    Proxy_RxBus.Proxy_RxRxTcu1Info.GarChange = 0;
    Proxy_RxBus.Proxy_RxRxTcu1Info.Flt = 0;
    Proxy_RxBus.Proxy_RxRxTcu1Info.GarSelDisp = 0;
    Proxy_RxBus.Proxy_RxRxTcu1Info.TQRedReq_PC = 0;
    Proxy_RxBus.Proxy_RxRxTcu1Info.TQRedReqSlw_PC = 0;
    Proxy_RxBus.Proxy_RxRxTcu1Info.TQIncReq_PC = 0;
    Proxy_RxBus.Proxy_RxRxSasInfo.Angle = 0;
    Proxy_RxBus.Proxy_RxRxSasInfo.Speed = 0;
    Proxy_RxBus.Proxy_RxRxSasInfo.Ok = 0;
    Proxy_RxBus.Proxy_RxRxSasInfo.Cal = 0;
    Proxy_RxBus.Proxy_RxRxSasInfo.Trim = 0;
    Proxy_RxBus.Proxy_RxRxSasInfo.CheckSum = 0;
    Proxy_RxBus.Proxy_RxRxSasInfo.MsgCount = 0;
    Proxy_RxBus.Proxy_RxRxMcu2Info.Flt = 0;
    Proxy_RxBus.Proxy_RxRxMcu1Info.MoTestTQ_PC = 0;
    Proxy_RxBus.Proxy_RxRxMcu1Info.MotActRotSpd_RPM = 0;
    Proxy_RxBus.Proxy_RxRxLongAccInfo.IntSenFltSymtmActive = 0;
    Proxy_RxBus.Proxy_RxRxLongAccInfo.IntSenFaultPresent = 0;
    Proxy_RxBus.Proxy_RxRxLongAccInfo.LongAccSenCirErrPre = 0;
    Proxy_RxBus.Proxy_RxRxLongAccInfo.LonACSenRanChkErrPre = 0;
    Proxy_RxBus.Proxy_RxRxLongAccInfo.LongRollingCounter = 0;
    Proxy_RxBus.Proxy_RxRxLongAccInfo.IntTempSensorFault = 0;
    Proxy_RxBus.Proxy_RxRxLongAccInfo.LongAccInvalidData = 0;
    Proxy_RxBus.Proxy_RxRxLongAccInfo.LongAccSelfTstStatus = 0;
    Proxy_RxBus.Proxy_RxRxLongAccInfo.LongAccRateSignal_0 = 0;
    Proxy_RxBus.Proxy_RxRxLongAccInfo.LongAccRateSignal_1 = 0;
    Proxy_RxBus.Proxy_RxRxHcu5Info.HevMod = 0;
    Proxy_RxBus.Proxy_RxRxHcu3Info.TmIntQcMDBINV_PC = 0;
    Proxy_RxBus.Proxy_RxRxHcu3Info.MotTQCMC_PC = 0;
    Proxy_RxBus.Proxy_RxRxHcu3Info.MotTQCMDBINV_PC = 0;
    Proxy_RxBus.Proxy_RxRxHcu2Info.ServiceMod = 0;
    Proxy_RxBus.Proxy_RxRxHcu2Info.RegenENA = 0;
    Proxy_RxBus.Proxy_RxRxHcu2Info.RegenBRKTQ_NM = 0;
    Proxy_RxBus.Proxy_RxRxHcu2Info.CrpTQ_NM = 0;
    Proxy_RxBus.Proxy_RxRxHcu2Info.WhlDEMTQ_NM = 0;
    Proxy_RxBus.Proxy_RxRxHcu1Info.EngCltStat = 0;
    Proxy_RxBus.Proxy_RxRxHcu1Info.HEVRDY = 0;
    Proxy_RxBus.Proxy_RxRxHcu1Info.EngTQCmdBinV_PC = 0;
    Proxy_RxBus.Proxy_RxRxHcu1Info.EngTQCmd_PC = 0;
    Proxy_RxBus.Proxy_RxRxFact1Info.OutTemp_SNR_C = 0;
    Proxy_RxBus.Proxy_RxRxEms3Info.EngColTemp_C = 0;
    Proxy_RxBus.Proxy_RxRxEms2Info.EngSpdErr = 0;
    Proxy_RxBus.Proxy_RxRxEms2Info.AccPedDep_PC = 0;
    Proxy_RxBus.Proxy_RxRxEms2Info.Tps_PC = 0;
    Proxy_RxBus.Proxy_RxRxEms1Info.TqStd_NM = 0;
    Proxy_RxBus.Proxy_RxRxEms1Info.ActINDTQ_PC = 0;
    Proxy_RxBus.Proxy_RxRxEms1Info.EngSpd_RPM = 0;
    Proxy_RxBus.Proxy_RxRxEms1Info.IndTQ_PC = 0;
    Proxy_RxBus.Proxy_RxRxEms1Info.FrictTQ_PC = 0;
    Proxy_RxBus.Proxy_RxRxClu2Info.IGN_SW = 0;
    Proxy_RxBus.Proxy_RxRxClu1Info.P_Brake_Act = 0;
    Proxy_RxBus.Proxy_RxRxClu1Info.Cf_Clu_BrakeFluIDSW = 0;
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Bms1MsgOkFlg = 0;
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.YawSerialMsgOkFlg = 0;
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.YawAccMsgOkFlg = 0;
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Tcu6MsgOkFlg = 0;
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Tcu5MsgOkFlg = 0;
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Tcu1MsgOkFlg = 0;
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.SasMsgOkFlg = 0;
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Mcu2MsgOkFlg = 0;
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Mcu1MsgOkFlg = 0;
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.LongAccMsgOkFlg = 0;
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Hcu5MsgOkFlg = 0;
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Hcu3MsgOkFlg = 0;
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Hcu2MsgOkFlg = 0;
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Hcu1MsgOkFlg = 0;
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Fatc1MsgOkFlg = 0;
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Ems3MsgOkFlg = 0;
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Ems2MsgOkFlg = 0;
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Ems1MsgOkFlg = 0;
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Clu2MsgOkFlg = 0;
    Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo.Clu1MsgOkFlg = 0;
    Proxy_RxBus.Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.ST_ECU_ST_REPAT_XTRQ_FTAX_BAX = 0;
    Proxy_RxBus.Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.QU_SER_REPAT_XTRQ_FTAX_BAX_ACT = 0;
    Proxy_RxBus.Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.AVL_REPAT_XTRQ_FTAX_BAX = 0;
    Proxy_RxBus.Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.QU_AVL_REPAT_XTRQ_FTAX_BAX = 0;
    Proxy_RxBus.Proxy_RxAVL_LTRQD_BAXInfo.QU_SER_LTRQD_BAX = 0;
    Proxy_RxBus.Proxy_RxAVL_LTRQD_BAXInfo.QU_AVL_LTRQD_BAX = 0;
    Proxy_RxBus.Proxy_RxAVL_LTRQD_BAXInfo.AVL_LTRQD_BAX = 0;
    Proxy_RxBus.Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.AVL_TORQ_CRSH_DMEE = 0;
    Proxy_RxBus.Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.AVL_TORQ_CRSH = 0;
    Proxy_RxBus.Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.ST_SAIL_DRV_2 = 0;
    Proxy_RxBus.Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.QU_AVL_RPM_ENG_CRSH = 0;
    Proxy_RxBus.Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.AVL_RPM_ENG_CRSH = 0;
    Proxy_RxBus.Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.AVL_ANG_ACPD = 0;
    Proxy_RxBus.Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.QU_AVL_ANG_ACPD = 0;
    Proxy_RxBus.Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.ST_ECU_ANG_ACPD = 0;
    Proxy_RxBus.Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.AVL_ANG_ACPD_VIRT = 0;
    Proxy_RxBus.Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.ECO_ANG_ACPD = 0;
    Proxy_RxBus.Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.GRAD_AVL_ANG_ACPD = 0;
    Proxy_RxBus.Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.ST_INTF_DRASY = 0;
    Proxy_RxBus.Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.AVL_RPM_BAX_RED = 0;
    Proxy_RxBus.Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.ST_PENG_PT = 0;
    Proxy_RxBus.Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.ST_AVAI_INTV_PT_DRS = 0;
    Proxy_RxBus.Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.QU_AVL_RPM_BAX_RED = 0;
    Proxy_RxBus.Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.QU_SER_WMOM_PT_SUM_DRS = 0;
    Proxy_RxBus.Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.QU_SER_WMOM_PT_SUM_STAB = 0;
    Proxy_RxBus.Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.ST_ECU_WMOM_DRV_5 = 0;
    Proxy_RxBus.Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.TAR_WMOM_PT_SUM_COOTD = 0;
    Proxy_RxBus.Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.QU_SER_COOR_TORQ_BDRV = 0;
    Proxy_RxBus.Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.ST_ECU_WMOM_DRV_4 = 0;
    Proxy_RxBus.Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.ST_DRVDIR_DVCH = 0;
    Proxy_RxBus.Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.QU_AVL_WMOM_PT_SUM = 0;
    Proxy_RxBus.Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_ERR_AMP = 0;
    Proxy_RxBus.Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.REIN_PT = 0;
    Proxy_RxBus.Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM = 0;
    Proxy_RxBus.Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.ST_ECU_WMOM_DRV_1 = 0;
    Proxy_RxBus.Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_MAX = 0;
    Proxy_RxBus.Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_FAST_TOP = 0;
    Proxy_RxBus.Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_FAST_BOT = 0;
    Proxy_RxBus.Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.ST_ECU_WMOM_DRV_2 = 0;
    Proxy_RxBus.Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.QU_REIN_PT = 0;
    Proxy_RxBus.Proxy_RxHGLV_VEH_FILTInfo.HGLV_VEH_FILT_FRH = 0;
    Proxy_RxBus.Proxy_RxHGLV_VEH_FILTInfo.HGLV_VEH_FILT_FLH = 0;
    Proxy_RxBus.Proxy_RxHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_FRH = 0;
    Proxy_RxBus.Proxy_RxHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_FLH = 0;
    Proxy_RxBus.Proxy_RxHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_RRH = 0;
    Proxy_RxBus.Proxy_RxHGLV_VEH_FILTInfo.HGLV_VEH_FILT_RLH = 0;
    Proxy_RxBus.Proxy_RxHGLV_VEH_FILTInfo.HGLV_VEH_FILT_RRH = 0;
    Proxy_RxBus.Proxy_RxHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_RLH = 0;
    Proxy_RxBus.Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.ATTAV_ESTI = 0;
    Proxy_RxBus.Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.ATTA_ESTI_ERR_AMP = 0;
    Proxy_RxBus.Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.VY_ESTI_ERR_AMP = 0;
    Proxy_RxBus.Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.VY_ESTI = 0;
    Proxy_RxBus.Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.ATTAV_ESTI_ERR_AMP = 0;
    Proxy_RxBus.Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.QU_VEH_DYNMC_DT_ESTI = 0;
    Proxy_RxBus.Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.ATTA_ESTI = 0;
    Proxy_RxBus.Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.QU_ACLNY_COG = 0;
    Proxy_RxBus.Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNY_COG_ERR_AMP = 0;
    Proxy_RxBus.Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNX_COG = 0;
    Proxy_RxBus.Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNY_COG = 0;
    Proxy_RxBus.Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNX_COG_ERR_AMP = 0;
    Proxy_RxBus.Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.QU_ACLNX_COG = 0;
    Proxy_RxBus.Proxy_RxV_VEH_V_VEH_2Info.QU_V_VEH_COG = 0;
    Proxy_RxBus.Proxy_RxV_VEH_V_VEH_2Info.DVCO_VEH = 0;
    Proxy_RxBus.Proxy_RxV_VEH_V_VEH_2Info.ST_V_VEH_NSS = 0;
    Proxy_RxBus.Proxy_RxV_VEH_V_VEH_2Info.V_VEH_COG = 0;
    Proxy_RxBus.Proxy_RxDT_DRDYSEN_EXT_VYAW_VEHInfo.VYAW_VEH_ERR_AMP = 0;
    Proxy_RxBus.Proxy_RxDT_DRDYSEN_EXT_VYAW_VEHInfo.QU_VYAW_VEH = 0;
    Proxy_RxBus.Proxy_RxDT_DRDYSEN_EXT_VYAW_VEHInfo.VYAW_VEH = 0;
    Proxy_RxBus.Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo.QU_AVL_TRGR_RW = 0;
    Proxy_RxBus.Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo.AVL_TRGR_RW = 0;
    Proxy_RxBus.Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo.AVL_LOGR_RW_FAST = 0;
    Proxy_RxBus.Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo.QU_AVL_LOGR_RW = 0;
    Proxy_RxBus.Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo.AVL_LOGR_RW = 0;
    Proxy_RxBus.Proxy_RxAVL_STEA_FTAXInfo.QU_AVL_STEA_FTAX_PNI = 0;
    Proxy_RxBus.Proxy_RxAVL_STEA_FTAXInfo.AVL_STEA_FTAX_PNI = 0;
    Proxy_RxBus.Proxy_RxAVL_STEA_FTAXInfo.QU_AVL_STEA_FTAX_WHL = 0;
    Proxy_RxBus.Proxy_RxAVL_STEA_FTAXInfo.AVL_STEA_FTAX_WHL = 0;
    Proxy_RxBus.Proxy_RxAVL_STEA_FTAXInfo.AVL_STEA_FTAX_WHL_ERR_AMP = 0;
    Proxy_RxBus.Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_PRMSN_DBC_DCRN_MAX = 0;
    Proxy_RxBus.Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_PRF_BRK = 0;
    Proxy_RxBus.Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_V_HDC = 0;
    Proxy_RxBus.Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_PRMSN_DBC_DCRN_GRAD_MAX = 0;
    Proxy_RxBus.Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_PRMSN_DBC_TR_THRV = 0;
    Proxy_RxBus.Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_BRTORQ_SUM = 0;
    Proxy_RxBus.Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_BRTORQ_SUM = 0;
    Proxy_RxBus.Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_PRMSN_DBC = 0;
    Proxy_RxBus.Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.RQ_TAO_SSM = 0;
    Proxy_RxBus.Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_DTORQ_BOT = 0;
    Proxy_RxBus.Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_RECUP_MAX = 0;
    Proxy_RxBus.Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_ILS = 0;
    Proxy_RxBus.Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_DTORQ_TOP = 0;
    Proxy_RxBus.Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info.ST_ECU_WMOM_DRV_6 = 0;
    Proxy_RxBus.Proxy_RxWMOM_DRV_7Info.ST_EL_DRVG = 0;
    Proxy_RxBus.Proxy_RxWMOM_DRV_7Info.PRD_AVL_WMOM_PT_SUM_RECUP_MAX = 0;
    Proxy_RxBus.Proxy_RxWMOM_DRV_7Info.ST_ECU_WMOM_DRV_7 = 0;
    Proxy_RxBus.Proxy_RxWMOM_DRV_7Info.QU_SER_WMOM_PT_SUM_RECUP = 0;
    Proxy_RxBus.Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.TAR_DIFF_BRTORQ_BAX_YMR = 0;
    Proxy_RxBus.Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.FACT_TAR_COMPT_DRV_YMR = 0;
    Proxy_RxBus.Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.QU_TAR_DIFF_BRTORQ_YMR = 0;
    Proxy_RxBus.Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.TAR_DIFF_BRTORQ_FTAX_YMR = 0;
    Proxy_RxBus.Proxy_RxDT_BRKSYS_ENGMGInfo.QU_AVL_LOWP_BRKFA = 0;
    Proxy_RxBus.Proxy_RxDT_BRKSYS_ENGMGInfo.AVL_LOWP_BRKFA = 0;
    Proxy_RxBus.Proxy_RxERRM_BN_UInfo.CTR_ERRM_BN_U = 0;
    Proxy_RxBus.Proxy_RxCTR_CRInfo.ST_EXCE_ACLN_THRV = 0;
    Proxy_RxBus.Proxy_RxCTR_CRInfo.CTR_PHTR_CR = 0;
    Proxy_RxBus.Proxy_RxCTR_CRInfo.CTR_ITLI_CR = 0;
    Proxy_RxBus.Proxy_RxCTR_CRInfo.CTR_CLSY_CR = 0;
    Proxy_RxBus.Proxy_RxCTR_CRInfo.CTR_AUTOM_ECAL_CR = 0;
    Proxy_RxBus.Proxy_RxCTR_CRInfo.CTR_SWO_EKP_CR = 0;
    Proxy_RxBus.Proxy_RxCTR_CRInfo.CTR_PCSH_MST = 0;
    Proxy_RxBus.Proxy_RxCTR_CRInfo.CTR_HAZW_CR = 0;
    Proxy_RxBus.Proxy_RxKLEMMENInfo.ST_OP_MSA = 0;
    Proxy_RxBus.Proxy_RxKLEMMENInfo.RQ_DRVG_RDI = 0;
    Proxy_RxBus.Proxy_RxKLEMMENInfo.ST_KL_DBG = 0;
    Proxy_RxBus.Proxy_RxKLEMMENInfo.ST_KL_50_MSA = 0;
    Proxy_RxBus.Proxy_RxKLEMMENInfo.ST_SSP = 0;
    Proxy_RxBus.Proxy_RxKLEMMENInfo.RWDT_BLS = 0;
    Proxy_RxBus.Proxy_RxKLEMMENInfo.ST_STCD_PENG = 0;
    Proxy_RxBus.Proxy_RxKLEMMENInfo.ST_KL = 0;
    Proxy_RxBus.Proxy_RxKLEMMENInfo.ST_KL_DIV = 0;
    Proxy_RxBus.Proxy_RxKLEMMENInfo.ST_VEH_CON = 0;
    Proxy_RxBus.Proxy_RxKLEMMENInfo.ST_KL_30B = 0;
    Proxy_RxBus.Proxy_RxKLEMMENInfo.CON_CLT_SW = 0;
    Proxy_RxBus.Proxy_RxKLEMMENInfo.CTR_ENG_STOP = 0;
    Proxy_RxBus.Proxy_RxKLEMMENInfo.ST_PLK = 0;
    Proxy_RxBus.Proxy_RxKLEMMENInfo.ST_KL_15N = 0;
    Proxy_RxBus.Proxy_RxKLEMMENInfo.ST_KL_KEY_VLD = 0;
    Proxy_RxBus.Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_MES_TSTMP = 0;
    Proxy_RxBus.Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.PCKG_ID = 0;
    Proxy_RxBus.Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.SUPP_ID = 0;
    Proxy_RxBus.Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_5 = 0;
    Proxy_RxBus.Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_8 = 0;
    Proxy_RxBus.Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_7 = 0;
    Proxy_RxBus.Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_6 = 0;
    Proxy_RxBus.Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_1 = 0;
    Proxy_RxBus.Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.TYR_ID = 0;
    Proxy_RxBus.Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_4 = 0;
    Proxy_RxBus.Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_3 = 0;
    Proxy_RxBus.Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_2 = 0;
    Proxy_RxBus.Proxy_RxBEDIENUNG_WISCHERInfo.OP_WISW = 0;
    Proxy_RxBus.Proxy_RxBEDIENUNG_WISCHERInfo.OP_WIPO = 0;
    Proxy_RxBus.Proxy_RxDT_PT_2Info.ST_GRSEL_DRV = 0;
    Proxy_RxBus.Proxy_RxDT_PT_2Info.TEMP_EOI_DRV = 0;
    Proxy_RxBus.Proxy_RxDT_PT_2Info.RLS_ENGSTA = 0;
    Proxy_RxBus.Proxy_RxDT_PT_2Info.RPM_ENG_MAX_ALW = 0;
    Proxy_RxBus.Proxy_RxDT_PT_2Info.RDUC_DOCTR_RPM_DRV_2 = 0;
    Proxy_RxBus.Proxy_RxDT_PT_2Info.TEMP_ENG_DRV = 0;
    Proxy_RxBus.Proxy_RxDT_PT_2Info.ST_DRV_VEH = 0;
    Proxy_RxBus.Proxy_RxDT_PT_2Info.ST_ECU_DT_PT_2 = 0;
    Proxy_RxBus.Proxy_RxDT_PT_2Info.ST_IDLG_ENG_DRV = 0;
    Proxy_RxBus.Proxy_RxDT_PT_2Info.ST_ILK_STRT_DRV = 0;
    Proxy_RxBus.Proxy_RxDT_PT_2Info.ST_SW_CLT_DRV = 0;
    Proxy_RxBus.Proxy_RxDT_PT_2Info.ST_ENG_RUN_DRV = 0;
    Proxy_RxBus.Proxy_RxSTAT_ENG_STA_AUTOInfo.RQ_MSA_ENG_STA_1 = 0;
    Proxy_RxBus.Proxy_RxSTAT_ENG_STA_AUTOInfo.RQ_MSA_ENG_STA_2 = 0;
    Proxy_RxBus.Proxy_RxSTAT_ENG_STA_AUTOInfo.PSBTY_MSA_ENG_STOP_STA = 0;
    Proxy_RxBus.Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_SHFT_MSA_ENGSTP = 0;
    Proxy_RxBus.Proxy_RxSTAT_ENG_STA_AUTOInfo.RQ_SLIP_K0 = 0;
    Proxy_RxBus.Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_TAR_PENG_CENG = 0;
    Proxy_RxBus.Proxy_RxSTAT_ENG_STA_AUTOInfo.SPEC_TYP_ENGSTA = 0;
    Proxy_RxBus.Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_RPM_CLCTR_MOT = 0;
    Proxy_RxBus.Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_FN_MSA = 0;
    Proxy_RxBus.Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_AVL_PENG_CENG_ENGMG = 0;
    Proxy_RxBus.Proxy_RxSTAT_ENG_STA_AUTOInfo.DISP_REAS_PREV_SWO_CENG = 0;
    Proxy_RxBus.Proxy_RxSTAT_ENG_STA_AUTOInfo.VARI_TYP_ENGSTA = 0;
    Proxy_RxBus.Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_TAR_CENG = 0;
    Proxy_RxBus.Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_AVAI_SAIL_DME = 0;
    Proxy_RxBus.Proxy_RxST_ENERG_GENInfo.ST_LDST_GEN_DRV = 0;
    Proxy_RxBus.Proxy_RxST_ENERG_GENInfo.DT_PCU_SCP = 0;
    Proxy_RxBus.Proxy_RxST_ENERG_GENInfo.ST_GEN_DRV = 0;
    Proxy_RxBus.Proxy_RxST_ENERG_GENInfo.AVL_I_GEN_DRV = 0;
    Proxy_RxBus.Proxy_RxST_ENERG_GENInfo.LDST_GEN_DRV = 0;
    Proxy_RxBus.Proxy_RxST_ENERG_GENInfo.ST_LDREL_GEN = 0;
    Proxy_RxBus.Proxy_RxST_ENERG_GENInfo.ST_CHG_STOR = 0;
    Proxy_RxBus.Proxy_RxST_ENERG_GENInfo.TEMP_BT_14V = 0;
    Proxy_RxBus.Proxy_RxST_ENERG_GENInfo.ST_I_IBS = 0;
    Proxy_RxBus.Proxy_RxST_ENERG_GENInfo.ST_SEP_STOR = 0;
    Proxy_RxBus.Proxy_RxST_ENERG_GENInfo.ST_BN2_SCP = 0;
    Proxy_RxBus.Proxy_RxDT_PT_1Info.RQ_SHPA_GRB_REGE_PAFI = 0;
    Proxy_RxBus.Proxy_RxDT_PT_1Info.ST_RTIR_DRV = 0;
    Proxy_RxBus.Proxy_RxDT_PT_1Info.CTR_SLCK_DRV = 0;
    Proxy_RxBus.Proxy_RxDT_PT_1Info.RQ_STASS_ENGMG = 0;
    Proxy_RxBus.Proxy_RxDT_PT_1Info.ST_CAT_HT = 0;
    Proxy_RxBus.Proxy_RxDT_PT_1Info.RQ_SHPA_GRB_CHGBLC = 0;
    Proxy_RxBus.Proxy_RxDT_PT_1Info.TAR_RPM_IDLG_DRV_EXT = 0;
    Proxy_RxBus.Proxy_RxDT_PT_1Info.SLCTN_BUS_COMM_ENG_GRB = 0;
    Proxy_RxBus.Proxy_RxDT_PT_1Info.TAR_RPM_IDLG_DRV = 0;
    Proxy_RxBus.Proxy_RxDT_PT_1Info.ST_INFS_DRV = 0;
    Proxy_RxBus.Proxy_RxDT_PT_1Info.ST_SW_WAUP_DRV = 0;
    Proxy_RxBus.Proxy_RxDT_PT_1Info.AIP_ENG_DRV = 0;
    Proxy_RxBus.Proxy_RxDT_PT_1Info.RDUC_DOCTR_RPM_DRV = 0;
    Proxy_RxBus.Proxy_RxDT_GRDT_DRVInfo.ST_OPMO_GRDT_DRV = 0;
    Proxy_RxBus.Proxy_RxDT_GRDT_DRVInfo.ST_RSTA_GRDT = 0;
    Proxy_RxBus.Proxy_RxDIAG_OBD_ENGMG_ELInfo.RQ_MIL_DIAG_OBD_ENGMG_EL = 0;
    Proxy_RxBus.Proxy_RxDIAG_OBD_ENGMG_ELInfo.RQ_RST_OBD_DIAG = 0;
    Proxy_RxBus.Proxy_RxSTAT_CT_HABRInfo.ST_CT_HABR = 0;
    Proxy_RxBus.Proxy_RxDT_PT_3Info.TRNRAO_BAX = 0;
    Proxy_RxBus.Proxy_RxDT_PT_3Info.QU_TRNRAO_BAX = 0;
    Proxy_RxBus.Proxy_RxEINHEITEN_BN2020Info.UN_TORQ_S_MOD = 0;
    Proxy_RxBus.Proxy_RxEINHEITEN_BN2020Info.UN_PWR_S_MOD = 0;
    Proxy_RxBus.Proxy_RxEINHEITEN_BN2020Info.UN_DATE_EXT = 0;
    Proxy_RxBus.Proxy_RxEINHEITEN_BN2020Info.UN_COSP_EL = 0;
    Proxy_RxBus.Proxy_RxEINHEITEN_BN2020Info.UN_TEMP = 0;
    Proxy_RxBus.Proxy_RxEINHEITEN_BN2020Info.UN_AIP = 0;
    Proxy_RxBus.Proxy_RxEINHEITEN_BN2020Info.LANG = 0;
    Proxy_RxBus.Proxy_RxEINHEITEN_BN2020Info.UN_DATE = 0;
    Proxy_RxBus.Proxy_RxEINHEITEN_BN2020Info.UN_T = 0;
    Proxy_RxBus.Proxy_RxEINHEITEN_BN2020Info.UN_SPDM_DGTL = 0;
    Proxy_RxBus.Proxy_RxEINHEITEN_BN2020Info.UN_MILE = 0;
    Proxy_RxBus.Proxy_RxEINHEITEN_BN2020Info.UN_FU = 0;
    Proxy_RxBus.Proxy_RxEINHEITEN_BN2020Info.UN_COSP = 0;
    Proxy_RxBus.Proxy_RxA_TEMPInfo.TEMP_EX_UNFILT = 0;
    Proxy_RxBus.Proxy_RxA_TEMPInfo.TEMP_EX = 0;
    Proxy_RxBus.Proxy_RxWISCHERGESCHWINDIGKEITInfo.ST_RNSE = 0;
    Proxy_RxBus.Proxy_RxWISCHERGESCHWINDIGKEITInfo.INT_RN = 0;
    Proxy_RxBus.Proxy_RxWISCHERGESCHWINDIGKEITInfo.V_WI = 0;
    Proxy_RxBus.Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_DRD_SFY_CTRL = 0;
    Proxy_RxBus.Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_DVDR_VRFD = 0;
    Proxy_RxBus.Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_DVDR_SFY_CTRL = 0;
    Proxy_RxBus.Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_PSDR_SFY_CTRL = 0;
    Proxy_RxBus.Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_PSD_SFY_CTRL = 0;
    Proxy_RxBus.Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_DRD_VRFD = 0;
    Proxy_RxBus.Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_PSDR_VRFD = 0;
    Proxy_RxBus.Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_PSD_VRFD = 0;
    Proxy_RxBus.Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY = 0;
    Proxy_RxBus.Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.RQ_SW_DRDY_MMID = 0;
    Proxy_RxBus.Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.RQ_SU_SW_DRDY = 0;
    Proxy_RxBus.Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.RQ_SW_DRDY_KDIS = 0;
    Proxy_RxBus.Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY_CHAS = 0;
    Proxy_RxBus.Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY_DRV = 0;
    Proxy_RxBus.Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY_STAB = 0;
    Proxy_RxBus.Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.DISP_ST_DSC = 0;
    Proxy_RxBus.Proxy_RxSTAT_ANHAENGERInfo.ST_SYNCN_HAZWCL_TRAI = 0;
    Proxy_RxBus.Proxy_RxSTAT_ANHAENGERInfo.ST_RFLI_DF_TRAI = 0;
    Proxy_RxBus.Proxy_RxSTAT_ANHAENGERInfo.ST_TRAI = 0;
    Proxy_RxBus.Proxy_RxSTAT_ANHAENGERInfo.ST_DI_DF_TRAI = 0;
    Proxy_RxBus.Proxy_RxSTAT_ANHAENGERInfo.ST_PO_AHV = 0;
    Proxy_RxBus.Proxy_RxFZZSTDInfo.ST_ILK_ERRM_FZM = 0;
    Proxy_RxBus.Proxy_RxFZZSTDInfo.ST_ENERG_FZM = 0;
    Proxy_RxBus.Proxy_RxFZZSTDInfo.ST_BT_PROTE_WUP = 0;
    Proxy_RxBus.Proxy_RxBEDIENUNG_FAHRWERKInfo.OP_MOD_TRCT_DSC = 0;
    Proxy_RxBus.Proxy_RxBEDIENUNG_FAHRWERKInfo.OP_TPCT = 0;
    Proxy_RxBus.Proxy_RxFAHRZEUGTYPInfo.QUAN_CYL = 0;
    Proxy_RxBus.Proxy_RxFAHRZEUGTYPInfo.QUAN_GR = 0;
    Proxy_RxBus.Proxy_RxFAHRZEUGTYPInfo.TYP_VEH = 0;
    Proxy_RxBus.Proxy_RxFAHRZEUGTYPInfo.TYP_BODY = 0;
    Proxy_RxBus.Proxy_RxFAHRZEUGTYPInfo.TYP_ENG = 0;
    Proxy_RxBus.Proxy_RxFAHRZEUGTYPInfo.CLAS_PWR = 0;
    Proxy_RxBus.Proxy_RxFAHRZEUGTYPInfo.TYP_CNT = 0;
    Proxy_RxBus.Proxy_RxFAHRZEUGTYPInfo.TYP_STE = 0;
    Proxy_RxBus.Proxy_RxFAHRZEUGTYPInfo.TYP_GRB = 0;
    Proxy_RxBus.Proxy_RxFAHRZEUGTYPInfo.TYP_CAPA = 0;
    Proxy_RxBus.Proxy_RxST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_RRH = 0;
    Proxy_RxBus.Proxy_RxST_BLT_CT_SOCCUInfo.ST_BLTB_SW_RM = 0;
    Proxy_RxBus.Proxy_RxST_BLT_CT_SOCCUInfo.ST_BLTB_SW_RRH = 0;
    Proxy_RxBus.Proxy_RxST_BLT_CT_SOCCUInfo.ST_ERR_SEAT_MT_DR = 0;
    Proxy_RxBus.Proxy_RxST_BLT_CT_SOCCUInfo.ST_BLTB_SW_DR = 0;
    Proxy_RxBus.Proxy_RxST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_DR = 0;
    Proxy_RxBus.Proxy_RxST_BLT_CT_SOCCUInfo.ST_BLTB_SW_RLH = 0;
    Proxy_RxBus.Proxy_RxST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_RLH = 0;
    Proxy_RxBus.Proxy_RxST_BLT_CT_SOCCUInfo.ST_BLTB_SW_PS = 0;
    Proxy_RxBus.Proxy_RxST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_PS = 0;
    Proxy_RxBus.Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_1 = 0;
    Proxy_RxBus.Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_2 = 0;
    Proxy_RxBus.Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_3 = 0;
    Proxy_RxBus.Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_6 = 0;
    Proxy_RxBus.Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_7 = 0;
    Proxy_RxBus.Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_4 = 0;
    Proxy_RxBus.Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_5 = 0;
    Proxy_RxBus.Proxy_RxRELATIVZEITInfo.T_SEC_COU_REL = 0;
    Proxy_RxBus.Proxy_RxRELATIVZEITInfo.T_DAY_COU_ABSL = 0;
    Proxy_RxBus.Proxy_RxKILOMETERSTANDInfo.RNG = 0;
    Proxy_RxBus.Proxy_RxKILOMETERSTANDInfo.ST_FLLV_FUTA_SPAR = 0;
    Proxy_RxBus.Proxy_RxKILOMETERSTANDInfo.FLLV_FUTA_RH = 0;
    Proxy_RxBus.Proxy_RxKILOMETERSTANDInfo.FLLV_FUTA_LH = 0;
    Proxy_RxBus.Proxy_RxKILOMETERSTANDInfo.FLLV_FUTA = 0;
    Proxy_RxBus.Proxy_RxKILOMETERSTANDInfo.MILE_KM = 0;
    Proxy_RxBus.Proxy_RxUHRZEIT_DATUMInfo.DISP_DATE_WDAY = 0;
    Proxy_RxBus.Proxy_RxUHRZEIT_DATUMInfo.DISP_DATE_DAY = 0;
    Proxy_RxBus.Proxy_RxUHRZEIT_DATUMInfo.DISP_DATE_MON = 0;
    Proxy_RxBus.Proxy_RxUHRZEIT_DATUMInfo.ST_DISP_CTI_DATE = 0;
    Proxy_RxBus.Proxy_RxUHRZEIT_DATUMInfo.DISP_DATE_YR = 0;
    Proxy_RxBus.Proxy_RxUHRZEIT_DATUMInfo.DISP_HR = 0;
    Proxy_RxBus.Proxy_RxUHRZEIT_DATUMInfo.DISP_SEC = 0;
    Proxy_RxBus.Proxy_RxUHRZEIT_DATUMInfo.DISP_MN = 0;
    Proxy_RxBus.Proxy_RxEcuModeSts = 0;
    Proxy_RxBus.Proxy_RxPCtrlAct = 0;
    Proxy_RxBus.Proxy_RxFuncInhibitProxySts = 0;
    Proxy_RxBus.Proxy_RxCanRxRegenInfo.HcuRegenEna = 0;
    Proxy_RxBus.Proxy_RxCanRxRegenInfo.HcuRegenBrkTq = 0;
    Proxy_RxBus.Proxy_RxCanRxAccelPedlInfo.AccelPedlVal = 0;
    Proxy_RxBus.Proxy_RxCanRxAccelPedlInfo.AccelPedlValErr = 0;
    Proxy_RxBus.Proxy_RxCanRxIdbInfo.EngMsgFault = 0;
    Proxy_RxBus.Proxy_RxCanRxIdbInfo.TarGearPosi = 0;
    Proxy_RxBus.Proxy_RxCanRxIdbInfo.GearSelDisp = 0;
    Proxy_RxBus.Proxy_RxCanRxIdbInfo.TcuSwiGs = 0;
    Proxy_RxBus.Proxy_RxCanRxIdbInfo.HcuServiceMod = 0;
    Proxy_RxBus.Proxy_RxCanRxIdbInfo.SubCanBusSigFault = 0;
    Proxy_RxBus.Proxy_RxCanRxIdbInfo.CoolantTemp = 0;
    Proxy_RxBus.Proxy_RxCanRxIdbInfo.CoolantTempErr = 0;
    Proxy_RxBus.Proxy_RxCanRxEngTempInfo.EngTemp = 0;
    Proxy_RxBus.Proxy_RxCanRxEngTempInfo.EngTempErr = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.Ax = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.YawRate = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.SteeringAngle = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.Ay = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.PbSwt = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.ClutchSwt = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.GearRSwt = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.EngActIndTq = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.EngRpm = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.EngIndTq = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.EngFrictionLossTq = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.EngStdTq = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.TurbineRpm = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.ThrottleAngle = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.TpsResol1000 = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.PvAvCanResol1000 = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.EngChr = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.EngVol = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.GearType = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.EngClutchState = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.EngTqCmdBeforeIntv = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.MotEstTq = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.MotTqCmdBeforeIntv = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.TqIntvTCU = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.TqIntvSlowTCU = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.TqIncReq = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.DecelReq = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.RainSnsStat = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.EngSpdErr = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.AtType = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.MtType = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.CvtType = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.DctType = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.HevAtTcu = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.TurbineRpmErr = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.ThrottleAngleErr = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.TopTrvlCltchSwtAct = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.TopTrvlCltchSwtActV = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.HillDesCtrlMdSwtAct = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.HillDesCtrlMdSwtActV = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.WiperIntSW = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.WiperLow = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.WiperHigh = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.WiperValid = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.WiperAuto = 0;
    Proxy_RxBus.Proxy_RxCanRxEscInfo.TcuFaultSts = 0;
    Proxy_RxBus.Proxy_RxCanRxClu2Info.IgnRun = 0;
    Proxy_RxBus.Proxy_RxCanRxEemInfo.YawRateInvld = 0;
    Proxy_RxBus.Proxy_RxCanRxEemInfo.AyInvld = 0;
    Proxy_RxBus.Proxy_RxCanRxEemInfo.SteeringAngleRxOk = 0;
    Proxy_RxBus.Proxy_RxCanRxEemInfo.AxInvldData = 0;
    Proxy_RxBus.Proxy_RxCanRxEemInfo.Ems1RxErr = 0;
    Proxy_RxBus.Proxy_RxCanRxEemInfo.Ems2RxErr = 0;
    Proxy_RxBus.Proxy_RxCanRxEemInfo.Tcu1RxErr = 0;
    Proxy_RxBus.Proxy_RxCanRxEemInfo.Tcu5RxErr = 0;
    Proxy_RxBus.Proxy_RxCanRxEemInfo.Hcu1RxErr = 0;
    Proxy_RxBus.Proxy_RxCanRxEemInfo.Hcu2RxErr = 0;
    Proxy_RxBus.Proxy_RxCanRxEemInfo.Hcu3RxErr = 0;
    Proxy_RxBus.Proxy_RxCanRxInfo.MainBusOffFlag = 0;
    Proxy_RxBus.Proxy_RxCanRxInfo.SenBusOffFlag = 0;
    Proxy_RxBus.Proxy_RxIMUCalcInfo.Reverse_Gear_flg = 0;
    Proxy_RxBus.Proxy_RxIMUCalcInfo.Reverse_Judge_Time = 0;
    Proxy_RxBus.Proxy_RxCanRxGearSelDispErrInfo = 0;
}

void Proxy_Rx(void)
{
    Proxy_Rx_Timer_Start = STM0_TIM0.U;

    /* Input */
    Proxy_Rx_Read_Proxy_RxRxBms1Info(&Proxy_RxBus.Proxy_RxRxBms1Info);
    /*==============================================================================
    * Members of structure Proxy_RxRxBms1Info 
     : Proxy_RxRxBms1Info.Bms1SocPc;
     =============================================================================*/
    
    Proxy_Rx_Read_Proxy_RxRxYawSerialInfo(&Proxy_RxBus.Proxy_RxRxYawSerialInfo);
    /*==============================================================================
    * Members of structure Proxy_RxRxYawSerialInfo 
     : Proxy_RxRxYawSerialInfo.YawSerialNum_0;
     : Proxy_RxRxYawSerialInfo.YawSerialNum_1;
     : Proxy_RxRxYawSerialInfo.YawSerialNum_2;
     =============================================================================*/
    
    Proxy_Rx_Read_Proxy_RxRxYawAccInfo(&Proxy_RxBus.Proxy_RxRxYawAccInfo);
    /*==============================================================================
    * Members of structure Proxy_RxRxYawAccInfo 
     : Proxy_RxRxYawAccInfo.YawRateValidData;
     : Proxy_RxRxYawAccInfo.YawRateSelfTestStatus;
     : Proxy_RxRxYawAccInfo.YawRateSignal_0;
     : Proxy_RxRxYawAccInfo.YawRateSignal_1;
     : Proxy_RxRxYawAccInfo.SensorOscFreqDev;
     : Proxy_RxRxYawAccInfo.Gyro_Fail;
     : Proxy_RxRxYawAccInfo.Raster_Fail;
     : Proxy_RxRxYawAccInfo.Eep_Fail;
     : Proxy_RxRxYawAccInfo.Batt_Range_Err;
     : Proxy_RxRxYawAccInfo.Asic_Fail;
     : Proxy_RxRxYawAccInfo.Accel_Fail;
     : Proxy_RxRxYawAccInfo.Ram_Fail;
     : Proxy_RxRxYawAccInfo.Rom_Fail;
     : Proxy_RxRxYawAccInfo.Ad_Fail;
     : Proxy_RxRxYawAccInfo.Osc_Fail;
     : Proxy_RxRxYawAccInfo.Watchdog_Rst;
     : Proxy_RxRxYawAccInfo.Plaus_Err_Pst;
     : Proxy_RxRxYawAccInfo.RollingCounter;
     : Proxy_RxRxYawAccInfo.Can_Func_Err;
     : Proxy_RxRxYawAccInfo.AccelEratorRateSig_0;
     : Proxy_RxRxYawAccInfo.AccelEratorRateSig_1;
     : Proxy_RxRxYawAccInfo.LatAccValidData;
     : Proxy_RxRxYawAccInfo.LatAccSelfTestStatus;
     =============================================================================*/
    
    Proxy_Rx_Read_Proxy_RxRxTcu6Info(&Proxy_RxBus.Proxy_RxRxTcu6Info);
    /*==============================================================================
    * Members of structure Proxy_RxRxTcu6Info 
     : Proxy_RxRxTcu6Info.ShiftClass_Ccan;
     =============================================================================*/
    
    Proxy_Rx_Read_Proxy_RxRxTcu5Info(&Proxy_RxBus.Proxy_RxRxTcu5Info);
    /*==============================================================================
    * Members of structure Proxy_RxRxTcu5Info 
     : Proxy_RxRxTcu5Info.Typ;
     : Proxy_RxRxTcu5Info.GearTyp;
     =============================================================================*/
    
    Proxy_Rx_Read_Proxy_RxRxTcu1Info(&Proxy_RxBus.Proxy_RxRxTcu1Info);
    /*==============================================================================
    * Members of structure Proxy_RxRxTcu1Info 
     : Proxy_RxRxTcu1Info.Targe;
     : Proxy_RxRxTcu1Info.GarChange;
     : Proxy_RxRxTcu1Info.Flt;
     : Proxy_RxRxTcu1Info.GarSelDisp;
     : Proxy_RxRxTcu1Info.TQRedReq_PC;
     : Proxy_RxRxTcu1Info.TQRedReqSlw_PC;
     : Proxy_RxRxTcu1Info.TQIncReq_PC;
     =============================================================================*/
    
    Proxy_Rx_Read_Proxy_RxRxSasInfo(&Proxy_RxBus.Proxy_RxRxSasInfo);
    /*==============================================================================
    * Members of structure Proxy_RxRxSasInfo 
     : Proxy_RxRxSasInfo.Angle;
     : Proxy_RxRxSasInfo.Speed;
     : Proxy_RxRxSasInfo.Ok;
     : Proxy_RxRxSasInfo.Cal;
     : Proxy_RxRxSasInfo.Trim;
     : Proxy_RxRxSasInfo.CheckSum;
     : Proxy_RxRxSasInfo.MsgCount;
     =============================================================================*/
    
    Proxy_Rx_Read_Proxy_RxRxMcu2Info(&Proxy_RxBus.Proxy_RxRxMcu2Info);
    /*==============================================================================
    * Members of structure Proxy_RxRxMcu2Info 
     : Proxy_RxRxMcu2Info.Flt;
     =============================================================================*/
    
    Proxy_Rx_Read_Proxy_RxRxMcu1Info(&Proxy_RxBus.Proxy_RxRxMcu1Info);
    /*==============================================================================
    * Members of structure Proxy_RxRxMcu1Info 
     : Proxy_RxRxMcu1Info.MoTestTQ_PC;
     : Proxy_RxRxMcu1Info.MotActRotSpd_RPM;
     =============================================================================*/
    
    Proxy_Rx_Read_Proxy_RxRxLongAccInfo(&Proxy_RxBus.Proxy_RxRxLongAccInfo);
    /*==============================================================================
    * Members of structure Proxy_RxRxLongAccInfo 
     : Proxy_RxRxLongAccInfo.IntSenFltSymtmActive;
     : Proxy_RxRxLongAccInfo.IntSenFaultPresent;
     : Proxy_RxRxLongAccInfo.LongAccSenCirErrPre;
     : Proxy_RxRxLongAccInfo.LonACSenRanChkErrPre;
     : Proxy_RxRxLongAccInfo.LongRollingCounter;
     : Proxy_RxRxLongAccInfo.IntTempSensorFault;
     : Proxy_RxRxLongAccInfo.LongAccInvalidData;
     : Proxy_RxRxLongAccInfo.LongAccSelfTstStatus;
     : Proxy_RxRxLongAccInfo.LongAccRateSignal_0;
     : Proxy_RxRxLongAccInfo.LongAccRateSignal_1;
     =============================================================================*/
    
    Proxy_Rx_Read_Proxy_RxRxHcu5Info(&Proxy_RxBus.Proxy_RxRxHcu5Info);
    /*==============================================================================
    * Members of structure Proxy_RxRxHcu5Info 
     : Proxy_RxRxHcu5Info.HevMod;
     =============================================================================*/
    
    Proxy_Rx_Read_Proxy_RxRxHcu3Info(&Proxy_RxBus.Proxy_RxRxHcu3Info);
    /*==============================================================================
    * Members of structure Proxy_RxRxHcu3Info 
     : Proxy_RxRxHcu3Info.TmIntQcMDBINV_PC;
     : Proxy_RxRxHcu3Info.MotTQCMC_PC;
     : Proxy_RxRxHcu3Info.MotTQCMDBINV_PC;
     =============================================================================*/
    
    Proxy_Rx_Read_Proxy_RxRxHcu2Info(&Proxy_RxBus.Proxy_RxRxHcu2Info);
    /*==============================================================================
    * Members of structure Proxy_RxRxHcu2Info 
     : Proxy_RxRxHcu2Info.ServiceMod;
     : Proxy_RxRxHcu2Info.RegenENA;
     : Proxy_RxRxHcu2Info.RegenBRKTQ_NM;
     : Proxy_RxRxHcu2Info.CrpTQ_NM;
     : Proxy_RxRxHcu2Info.WhlDEMTQ_NM;
     =============================================================================*/
    
    Proxy_Rx_Read_Proxy_RxRxHcu1Info(&Proxy_RxBus.Proxy_RxRxHcu1Info);
    /*==============================================================================
    * Members of structure Proxy_RxRxHcu1Info 
     : Proxy_RxRxHcu1Info.EngCltStat;
     : Proxy_RxRxHcu1Info.HEVRDY;
     : Proxy_RxRxHcu1Info.EngTQCmdBinV_PC;
     : Proxy_RxRxHcu1Info.EngTQCmd_PC;
     =============================================================================*/
    
    Proxy_Rx_Read_Proxy_RxRxFact1Info(&Proxy_RxBus.Proxy_RxRxFact1Info);
    /*==============================================================================
    * Members of structure Proxy_RxRxFact1Info 
     : Proxy_RxRxFact1Info.OutTemp_SNR_C;
     =============================================================================*/
    
    Proxy_Rx_Read_Proxy_RxRxEms3Info(&Proxy_RxBus.Proxy_RxRxEms3Info);
    /*==============================================================================
    * Members of structure Proxy_RxRxEms3Info 
     : Proxy_RxRxEms3Info.EngColTemp_C;
     =============================================================================*/
    
    Proxy_Rx_Read_Proxy_RxRxEms2Info(&Proxy_RxBus.Proxy_RxRxEms2Info);
    /*==============================================================================
    * Members of structure Proxy_RxRxEms2Info 
     : Proxy_RxRxEms2Info.EngSpdErr;
     : Proxy_RxRxEms2Info.AccPedDep_PC;
     : Proxy_RxRxEms2Info.Tps_PC;
     =============================================================================*/
    
    Proxy_Rx_Read_Proxy_RxRxEms1Info(&Proxy_RxBus.Proxy_RxRxEms1Info);
    /*==============================================================================
    * Members of structure Proxy_RxRxEms1Info 
     : Proxy_RxRxEms1Info.TqStd_NM;
     : Proxy_RxRxEms1Info.ActINDTQ_PC;
     : Proxy_RxRxEms1Info.EngSpd_RPM;
     : Proxy_RxRxEms1Info.IndTQ_PC;
     : Proxy_RxRxEms1Info.FrictTQ_PC;
     =============================================================================*/
    
    Proxy_Rx_Read_Proxy_RxRxClu2Info(&Proxy_RxBus.Proxy_RxRxClu2Info);
    /*==============================================================================
    * Members of structure Proxy_RxRxClu2Info 
     : Proxy_RxRxClu2Info.IGN_SW;
     =============================================================================*/
    
    Proxy_Rx_Read_Proxy_RxRxClu1Info(&Proxy_RxBus.Proxy_RxRxClu1Info);
    /*==============================================================================
    * Members of structure Proxy_RxRxClu1Info 
     : Proxy_RxRxClu1Info.P_Brake_Act;
     : Proxy_RxRxClu1Info.Cf_Clu_BrakeFluIDSW;
     =============================================================================*/
    
    Proxy_Rx_Read_Proxy_RxRxMsgOkFlgInfo(&Proxy_RxBus.Proxy_RxRxMsgOkFlgInfo);
    /*==============================================================================
    * Members of structure Proxy_RxRxMsgOkFlgInfo 
     : Proxy_RxRxMsgOkFlgInfo.Bms1MsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.YawSerialMsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.YawAccMsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.Tcu6MsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.Tcu5MsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.Tcu1MsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.SasMsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.Mcu2MsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.Mcu1MsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.LongAccMsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.Hcu5MsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.Hcu3MsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.Hcu2MsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.Hcu1MsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.Fatc1MsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.Ems3MsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.Ems2MsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.Ems1MsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.Clu2MsgOkFlg;
     : Proxy_RxRxMsgOkFlgInfo.Clu1MsgOkFlg;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Proxy_Rx_Read_Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo_ST_ECU_ST_REPAT_XTRQ_FTAX_BAX(&Proxy_RxBus.Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.ST_ECU_ST_REPAT_XTRQ_FTAX_BAX);
    Proxy_Rx_Read_Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo_QU_SER_REPAT_XTRQ_FTAX_BAX_ACT(&Proxy_RxBus.Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.QU_SER_REPAT_XTRQ_FTAX_BAX_ACT);
    Proxy_Rx_Read_Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo_AVL_REPAT_XTRQ_FTAX_BAX(&Proxy_RxBus.Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.AVL_REPAT_XTRQ_FTAX_BAX);
    Proxy_Rx_Read_Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo_QU_AVL_REPAT_XTRQ_FTAX_BAX(&Proxy_RxBus.Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.QU_AVL_REPAT_XTRQ_FTAX_BAX);

    /* Decomposed structure interface */
    Proxy_Rx_Read_Proxy_RxAVL_LTRQD_BAXInfo_QU_SER_LTRQD_BAX(&Proxy_RxBus.Proxy_RxAVL_LTRQD_BAXInfo.QU_SER_LTRQD_BAX);
    Proxy_Rx_Read_Proxy_RxAVL_LTRQD_BAXInfo_QU_AVL_LTRQD_BAX(&Proxy_RxBus.Proxy_RxAVL_LTRQD_BAXInfo.QU_AVL_LTRQD_BAX);
    Proxy_Rx_Read_Proxy_RxAVL_LTRQD_BAXInfo_AVL_LTRQD_BAX(&Proxy_RxBus.Proxy_RxAVL_LTRQD_BAXInfo.AVL_LTRQD_BAX);

    /* Decomposed structure interface */
    Proxy_Rx_Read_Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo_AVL_TORQ_CRSH_DMEE(&Proxy_RxBus.Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.AVL_TORQ_CRSH_DMEE);
    Proxy_Rx_Read_Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo_AVL_TORQ_CRSH(&Proxy_RxBus.Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.AVL_TORQ_CRSH);
    Proxy_Rx_Read_Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo_ST_SAIL_DRV_2(&Proxy_RxBus.Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.ST_SAIL_DRV_2);
    Proxy_Rx_Read_Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo_QU_AVL_RPM_ENG_CRSH(&Proxy_RxBus.Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.QU_AVL_RPM_ENG_CRSH);
    Proxy_Rx_Read_Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo_AVL_RPM_ENG_CRSH(&Proxy_RxBus.Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.AVL_RPM_ENG_CRSH);
    Proxy_Rx_Read_Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo_AVL_ANG_ACPD(&Proxy_RxBus.Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.AVL_ANG_ACPD);
    Proxy_Rx_Read_Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo_QU_AVL_ANG_ACPD(&Proxy_RxBus.Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.QU_AVL_ANG_ACPD);
    Proxy_Rx_Read_Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo_ST_ECU_ANG_ACPD(&Proxy_RxBus.Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.ST_ECU_ANG_ACPD);
    Proxy_Rx_Read_Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo_AVL_ANG_ACPD_VIRT(&Proxy_RxBus.Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.AVL_ANG_ACPD_VIRT);
    Proxy_Rx_Read_Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo_ECO_ANG_ACPD(&Proxy_RxBus.Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.ECO_ANG_ACPD);
    Proxy_Rx_Read_Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo_GRAD_AVL_ANG_ACPD(&Proxy_RxBus.Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.GRAD_AVL_ANG_ACPD);
    Proxy_Rx_Read_Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo_ST_INTF_DRASY(&Proxy_RxBus.Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.ST_INTF_DRASY);

    /* Decomposed structure interface */
    Proxy_Rx_Read_Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info_AVL_RPM_BAX_RED(&Proxy_RxBus.Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.AVL_RPM_BAX_RED);
    Proxy_Rx_Read_Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info_ST_PENG_PT(&Proxy_RxBus.Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.ST_PENG_PT);
    Proxy_Rx_Read_Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info_ST_AVAI_INTV_PT_DRS(&Proxy_RxBus.Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.ST_AVAI_INTV_PT_DRS);
    Proxy_Rx_Read_Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info_QU_AVL_RPM_BAX_RED(&Proxy_RxBus.Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.QU_AVL_RPM_BAX_RED);
    Proxy_Rx_Read_Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info_QU_SER_WMOM_PT_SUM_DRS(&Proxy_RxBus.Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.QU_SER_WMOM_PT_SUM_DRS);
    Proxy_Rx_Read_Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info_QU_SER_WMOM_PT_SUM_STAB(&Proxy_RxBus.Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.QU_SER_WMOM_PT_SUM_STAB);
    Proxy_Rx_Read_Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info_ST_ECU_WMOM_DRV_5(&Proxy_RxBus.Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.ST_ECU_WMOM_DRV_5);
    Proxy_Rx_Read_Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info_TAR_WMOM_PT_SUM_COOTD(&Proxy_RxBus.Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.TAR_WMOM_PT_SUM_COOTD);
    Proxy_Rx_Read_Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info_QU_SER_COOR_TORQ_BDRV(&Proxy_RxBus.Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.QU_SER_COOR_TORQ_BDRV);
    Proxy_Rx_Read_Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info_ST_ECU_WMOM_DRV_4(&Proxy_RxBus.Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.ST_ECU_WMOM_DRV_4);
    Proxy_Rx_Read_Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info_ST_DRVDIR_DVCH(&Proxy_RxBus.Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.ST_DRVDIR_DVCH);

    /* Decomposed structure interface */
    Proxy_Rx_Read_Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info_QU_AVL_WMOM_PT_SUM(&Proxy_RxBus.Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.QU_AVL_WMOM_PT_SUM);
    Proxy_Rx_Read_Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info_AVL_WMOM_PT_SUM_ERR_AMP(&Proxy_RxBus.Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_ERR_AMP);
    Proxy_Rx_Read_Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info_REIN_PT(&Proxy_RxBus.Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.REIN_PT);
    Proxy_Rx_Read_Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info_AVL_WMOM_PT_SUM(&Proxy_RxBus.Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM);
    Proxy_Rx_Read_Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info_ST_ECU_WMOM_DRV_1(&Proxy_RxBus.Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.ST_ECU_WMOM_DRV_1);
    Proxy_Rx_Read_Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info_AVL_WMOM_PT_SUM_MAX(&Proxy_RxBus.Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_MAX);
    Proxy_Rx_Read_Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info_AVL_WMOM_PT_SUM_FAST_TOP(&Proxy_RxBus.Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_FAST_TOP);
    Proxy_Rx_Read_Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info_AVL_WMOM_PT_SUM_FAST_BOT(&Proxy_RxBus.Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_FAST_BOT);
    Proxy_Rx_Read_Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info_ST_ECU_WMOM_DRV_2(&Proxy_RxBus.Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.ST_ECU_WMOM_DRV_2);
    Proxy_Rx_Read_Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info_QU_REIN_PT(&Proxy_RxBus.Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.QU_REIN_PT);

    /* Decomposed structure interface */
    Proxy_Rx_Read_Proxy_RxHGLV_VEH_FILTInfo_HGLV_VEH_FILT_FRH(&Proxy_RxBus.Proxy_RxHGLV_VEH_FILTInfo.HGLV_VEH_FILT_FRH);
    Proxy_Rx_Read_Proxy_RxHGLV_VEH_FILTInfo_HGLV_VEH_FILT_FLH(&Proxy_RxBus.Proxy_RxHGLV_VEH_FILTInfo.HGLV_VEH_FILT_FLH);
    Proxy_Rx_Read_Proxy_RxHGLV_VEH_FILTInfo_QU_HGLV_VEH_FILT_FRH(&Proxy_RxBus.Proxy_RxHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_FRH);
    Proxy_Rx_Read_Proxy_RxHGLV_VEH_FILTInfo_QU_HGLV_VEH_FILT_FLH(&Proxy_RxBus.Proxy_RxHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_FLH);
    Proxy_Rx_Read_Proxy_RxHGLV_VEH_FILTInfo_QU_HGLV_VEH_FILT_RRH(&Proxy_RxBus.Proxy_RxHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_RRH);
    Proxy_Rx_Read_Proxy_RxHGLV_VEH_FILTInfo_HGLV_VEH_FILT_RLH(&Proxy_RxBus.Proxy_RxHGLV_VEH_FILTInfo.HGLV_VEH_FILT_RLH);
    Proxy_Rx_Read_Proxy_RxHGLV_VEH_FILTInfo_HGLV_VEH_FILT_RRH(&Proxy_RxBus.Proxy_RxHGLV_VEH_FILTInfo.HGLV_VEH_FILT_RRH);
    Proxy_Rx_Read_Proxy_RxHGLV_VEH_FILTInfo_QU_HGLV_VEH_FILT_RLH(&Proxy_RxBus.Proxy_RxHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_RLH);

    /* Decomposed structure interface */
    Proxy_Rx_Read_Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo_ATTAV_ESTI(&Proxy_RxBus.Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.ATTAV_ESTI);
    Proxy_Rx_Read_Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo_ATTA_ESTI_ERR_AMP(&Proxy_RxBus.Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.ATTA_ESTI_ERR_AMP);
    Proxy_Rx_Read_Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo_VY_ESTI_ERR_AMP(&Proxy_RxBus.Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.VY_ESTI_ERR_AMP);
    Proxy_Rx_Read_Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo_VY_ESTI(&Proxy_RxBus.Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.VY_ESTI);
    Proxy_Rx_Read_Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo_ATTAV_ESTI_ERR_AMP(&Proxy_RxBus.Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.ATTAV_ESTI_ERR_AMP);
    Proxy_Rx_Read_Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo_QU_VEH_DYNMC_DT_ESTI(&Proxy_RxBus.Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.QU_VEH_DYNMC_DT_ESTI);
    Proxy_Rx_Read_Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo_ATTA_ESTI(&Proxy_RxBus.Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.ATTA_ESTI);

    /* Decomposed structure interface */
    Proxy_Rx_Read_Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo_QU_ACLNY_COG(&Proxy_RxBus.Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.QU_ACLNY_COG);
    Proxy_Rx_Read_Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo_ACLNY_COG_ERR_AMP(&Proxy_RxBus.Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNY_COG_ERR_AMP);
    Proxy_Rx_Read_Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo_ACLNX_COG(&Proxy_RxBus.Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNX_COG);
    Proxy_Rx_Read_Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo_ACLNY_COG(&Proxy_RxBus.Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNY_COG);
    Proxy_Rx_Read_Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo_ACLNX_COG_ERR_AMP(&Proxy_RxBus.Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNX_COG_ERR_AMP);
    Proxy_Rx_Read_Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo_QU_ACLNX_COG(&Proxy_RxBus.Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.QU_ACLNX_COG);

    /* Decomposed structure interface */
    Proxy_Rx_Read_Proxy_RxV_VEH_V_VEH_2Info_QU_V_VEH_COG(&Proxy_RxBus.Proxy_RxV_VEH_V_VEH_2Info.QU_V_VEH_COG);
    Proxy_Rx_Read_Proxy_RxV_VEH_V_VEH_2Info_DVCO_VEH(&Proxy_RxBus.Proxy_RxV_VEH_V_VEH_2Info.DVCO_VEH);
    Proxy_Rx_Read_Proxy_RxV_VEH_V_VEH_2Info_ST_V_VEH_NSS(&Proxy_RxBus.Proxy_RxV_VEH_V_VEH_2Info.ST_V_VEH_NSS);
    Proxy_Rx_Read_Proxy_RxV_VEH_V_VEH_2Info_V_VEH_COG(&Proxy_RxBus.Proxy_RxV_VEH_V_VEH_2Info.V_VEH_COG);

    /* Decomposed structure interface */
    Proxy_Rx_Read_Proxy_RxDT_DRDYSEN_EXT_VYAW_VEHInfo_VYAW_VEH_ERR_AMP(&Proxy_RxBus.Proxy_RxDT_DRDYSEN_EXT_VYAW_VEHInfo.VYAW_VEH_ERR_AMP);
    Proxy_Rx_Read_Proxy_RxDT_DRDYSEN_EXT_VYAW_VEHInfo_QU_VYAW_VEH(&Proxy_RxBus.Proxy_RxDT_DRDYSEN_EXT_VYAW_VEHInfo.QU_VYAW_VEH);
    Proxy_Rx_Read_Proxy_RxDT_DRDYSEN_EXT_VYAW_VEHInfo_VYAW_VEH(&Proxy_RxBus.Proxy_RxDT_DRDYSEN_EXT_VYAW_VEHInfo.VYAW_VEH);

    /* Decomposed structure interface */
    Proxy_Rx_Read_Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo_QU_AVL_TRGR_RW(&Proxy_RxBus.Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo.QU_AVL_TRGR_RW);
    Proxy_Rx_Read_Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo_AVL_TRGR_RW(&Proxy_RxBus.Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo.AVL_TRGR_RW);
    Proxy_Rx_Read_Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo_AVL_LOGR_RW_FAST(&Proxy_RxBus.Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo.AVL_LOGR_RW_FAST);
    Proxy_Rx_Read_Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo_QU_AVL_LOGR_RW(&Proxy_RxBus.Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo.QU_AVL_LOGR_RW);
    Proxy_Rx_Read_Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo_AVL_LOGR_RW(&Proxy_RxBus.Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo.AVL_LOGR_RW);

    /* Decomposed structure interface */
    Proxy_Rx_Read_Proxy_RxAVL_STEA_FTAXInfo_QU_AVL_STEA_FTAX_PNI(&Proxy_RxBus.Proxy_RxAVL_STEA_FTAXInfo.QU_AVL_STEA_FTAX_PNI);
    Proxy_Rx_Read_Proxy_RxAVL_STEA_FTAXInfo_AVL_STEA_FTAX_PNI(&Proxy_RxBus.Proxy_RxAVL_STEA_FTAXInfo.AVL_STEA_FTAX_PNI);
    Proxy_Rx_Read_Proxy_RxAVL_STEA_FTAXInfo_QU_AVL_STEA_FTAX_WHL(&Proxy_RxBus.Proxy_RxAVL_STEA_FTAXInfo.QU_AVL_STEA_FTAX_WHL);
    Proxy_Rx_Read_Proxy_RxAVL_STEA_FTAXInfo_AVL_STEA_FTAX_WHL(&Proxy_RxBus.Proxy_RxAVL_STEA_FTAXInfo.AVL_STEA_FTAX_WHL);
    Proxy_Rx_Read_Proxy_RxAVL_STEA_FTAXInfo_AVL_STEA_FTAX_WHL_ERR_AMP(&Proxy_RxBus.Proxy_RxAVL_STEA_FTAXInfo.AVL_STEA_FTAX_WHL_ERR_AMP);

    /* Decomposed structure interface */
    Proxy_Rx_Read_Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_TAR_PRMSN_DBC_DCRN_MAX(&Proxy_RxBus.Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_PRMSN_DBC_DCRN_MAX);
    Proxy_Rx_Read_Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_QU_TAR_PRF_BRK(&Proxy_RxBus.Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_PRF_BRK);
    Proxy_Rx_Read_Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_QU_TAR_V_HDC(&Proxy_RxBus.Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_V_HDC);
    Proxy_Rx_Read_Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_TAR_PRMSN_DBC_DCRN_GRAD_MAX(&Proxy_RxBus.Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_PRMSN_DBC_DCRN_GRAD_MAX);
    Proxy_Rx_Read_Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_TAR_PRMSN_DBC_TR_THRV(&Proxy_RxBus.Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_PRMSN_DBC_TR_THRV);
    Proxy_Rx_Read_Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_TAR_BRTORQ_SUM(&Proxy_RxBus.Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_BRTORQ_SUM);
    Proxy_Rx_Read_Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_QU_TAR_BRTORQ_SUM(&Proxy_RxBus.Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_BRTORQ_SUM);
    Proxy_Rx_Read_Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_QU_TAR_PRMSN_DBC(&Proxy_RxBus.Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_PRMSN_DBC);
    Proxy_Rx_Read_Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_RQ_TAO_SSM(&Proxy_RxBus.Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.RQ_TAO_SSM);

    /* Decomposed structure interface */
    Proxy_Rx_Read_Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info_AVL_WMOM_PT_SUM_DTORQ_BOT(&Proxy_RxBus.Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_DTORQ_BOT);
    Proxy_Rx_Read_Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info_AVL_WMOM_PT_SUM_RECUP_MAX(&Proxy_RxBus.Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_RECUP_MAX);
    Proxy_Rx_Read_Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info_AVL_WMOM_PT_SUM_ILS(&Proxy_RxBus.Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_ILS);
    Proxy_Rx_Read_Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info_AVL_WMOM_PT_SUM_DTORQ_TOP(&Proxy_RxBus.Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_DTORQ_TOP);
    Proxy_Rx_Read_Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info_ST_ECU_WMOM_DRV_6(&Proxy_RxBus.Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info.ST_ECU_WMOM_DRV_6);

    /* Decomposed structure interface */
    Proxy_Rx_Read_Proxy_RxWMOM_DRV_7Info_ST_EL_DRVG(&Proxy_RxBus.Proxy_RxWMOM_DRV_7Info.ST_EL_DRVG);
    Proxy_Rx_Read_Proxy_RxWMOM_DRV_7Info_PRD_AVL_WMOM_PT_SUM_RECUP_MAX(&Proxy_RxBus.Proxy_RxWMOM_DRV_7Info.PRD_AVL_WMOM_PT_SUM_RECUP_MAX);
    Proxy_Rx_Read_Proxy_RxWMOM_DRV_7Info_ST_ECU_WMOM_DRV_7(&Proxy_RxBus.Proxy_RxWMOM_DRV_7Info.ST_ECU_WMOM_DRV_7);
    Proxy_Rx_Read_Proxy_RxWMOM_DRV_7Info_QU_SER_WMOM_PT_SUM_RECUP(&Proxy_RxBus.Proxy_RxWMOM_DRV_7Info.QU_SER_WMOM_PT_SUM_RECUP);

    /* Decomposed structure interface */
    Proxy_Rx_Read_Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo_TAR_DIFF_BRTORQ_BAX_YMR(&Proxy_RxBus.Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.TAR_DIFF_BRTORQ_BAX_YMR);
    Proxy_Rx_Read_Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo_FACT_TAR_COMPT_DRV_YMR(&Proxy_RxBus.Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.FACT_TAR_COMPT_DRV_YMR);
    Proxy_Rx_Read_Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo_QU_TAR_DIFF_BRTORQ_YMR(&Proxy_RxBus.Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.QU_TAR_DIFF_BRTORQ_YMR);
    Proxy_Rx_Read_Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo_TAR_DIFF_BRTORQ_FTAX_YMR(&Proxy_RxBus.Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.TAR_DIFF_BRTORQ_FTAX_YMR);

    /* Decomposed structure interface */
    Proxy_Rx_Read_Proxy_RxDT_BRKSYS_ENGMGInfo_QU_AVL_LOWP_BRKFA(&Proxy_RxBus.Proxy_RxDT_BRKSYS_ENGMGInfo.QU_AVL_LOWP_BRKFA);
    Proxy_Rx_Read_Proxy_RxDT_BRKSYS_ENGMGInfo_AVL_LOWP_BRKFA(&Proxy_RxBus.Proxy_RxDT_BRKSYS_ENGMGInfo.AVL_LOWP_BRKFA);

    Proxy_Rx_Read_Proxy_RxERRM_BN_UInfo(&Proxy_RxBus.Proxy_RxERRM_BN_UInfo);
    /*==============================================================================
    * Members of structure Proxy_RxERRM_BN_UInfo 
     : Proxy_RxERRM_BN_UInfo.CTR_ERRM_BN_U;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Proxy_Rx_Read_Proxy_RxCTR_CRInfo_ST_EXCE_ACLN_THRV(&Proxy_RxBus.Proxy_RxCTR_CRInfo.ST_EXCE_ACLN_THRV);
    Proxy_Rx_Read_Proxy_RxCTR_CRInfo_CTR_PHTR_CR(&Proxy_RxBus.Proxy_RxCTR_CRInfo.CTR_PHTR_CR);
    Proxy_Rx_Read_Proxy_RxCTR_CRInfo_CTR_ITLI_CR(&Proxy_RxBus.Proxy_RxCTR_CRInfo.CTR_ITLI_CR);
    Proxy_Rx_Read_Proxy_RxCTR_CRInfo_CTR_CLSY_CR(&Proxy_RxBus.Proxy_RxCTR_CRInfo.CTR_CLSY_CR);
    Proxy_Rx_Read_Proxy_RxCTR_CRInfo_CTR_AUTOM_ECAL_CR(&Proxy_RxBus.Proxy_RxCTR_CRInfo.CTR_AUTOM_ECAL_CR);
    Proxy_Rx_Read_Proxy_RxCTR_CRInfo_CTR_SWO_EKP_CR(&Proxy_RxBus.Proxy_RxCTR_CRInfo.CTR_SWO_EKP_CR);
    Proxy_Rx_Read_Proxy_RxCTR_CRInfo_CTR_PCSH_MST(&Proxy_RxBus.Proxy_RxCTR_CRInfo.CTR_PCSH_MST);
    Proxy_Rx_Read_Proxy_RxCTR_CRInfo_CTR_HAZW_CR(&Proxy_RxBus.Proxy_RxCTR_CRInfo.CTR_HAZW_CR);

    /* Decomposed structure interface */
    Proxy_Rx_Read_Proxy_RxKLEMMENInfo_ST_OP_MSA(&Proxy_RxBus.Proxy_RxKLEMMENInfo.ST_OP_MSA);
    Proxy_Rx_Read_Proxy_RxKLEMMENInfo_RQ_DRVG_RDI(&Proxy_RxBus.Proxy_RxKLEMMENInfo.RQ_DRVG_RDI);
    Proxy_Rx_Read_Proxy_RxKLEMMENInfo_ST_KL_DBG(&Proxy_RxBus.Proxy_RxKLEMMENInfo.ST_KL_DBG);
    Proxy_Rx_Read_Proxy_RxKLEMMENInfo_ST_KL_50_MSA(&Proxy_RxBus.Proxy_RxKLEMMENInfo.ST_KL_50_MSA);
    Proxy_Rx_Read_Proxy_RxKLEMMENInfo_ST_SSP(&Proxy_RxBus.Proxy_RxKLEMMENInfo.ST_SSP);
    Proxy_Rx_Read_Proxy_RxKLEMMENInfo_RWDT_BLS(&Proxy_RxBus.Proxy_RxKLEMMENInfo.RWDT_BLS);
    Proxy_Rx_Read_Proxy_RxKLEMMENInfo_ST_STCD_PENG(&Proxy_RxBus.Proxy_RxKLEMMENInfo.ST_STCD_PENG);
    Proxy_Rx_Read_Proxy_RxKLEMMENInfo_ST_KL(&Proxy_RxBus.Proxy_RxKLEMMENInfo.ST_KL);
    Proxy_Rx_Read_Proxy_RxKLEMMENInfo_ST_KL_DIV(&Proxy_RxBus.Proxy_RxKLEMMENInfo.ST_KL_DIV);
    Proxy_Rx_Read_Proxy_RxKLEMMENInfo_ST_VEH_CON(&Proxy_RxBus.Proxy_RxKLEMMENInfo.ST_VEH_CON);
    Proxy_Rx_Read_Proxy_RxKLEMMENInfo_ST_KL_30B(&Proxy_RxBus.Proxy_RxKLEMMENInfo.ST_KL_30B);
    Proxy_Rx_Read_Proxy_RxKLEMMENInfo_CON_CLT_SW(&Proxy_RxBus.Proxy_RxKLEMMENInfo.CON_CLT_SW);
    Proxy_Rx_Read_Proxy_RxKLEMMENInfo_CTR_ENG_STOP(&Proxy_RxBus.Proxy_RxKLEMMENInfo.CTR_ENG_STOP);
    Proxy_Rx_Read_Proxy_RxKLEMMENInfo_ST_PLK(&Proxy_RxBus.Proxy_RxKLEMMENInfo.ST_PLK);
    Proxy_Rx_Read_Proxy_RxKLEMMENInfo_ST_KL_15N(&Proxy_RxBus.Proxy_RxKLEMMENInfo.ST_KL_15N);
    Proxy_Rx_Read_Proxy_RxKLEMMENInfo_ST_KL_KEY_VLD(&Proxy_RxBus.Proxy_RxKLEMMENInfo.ST_KL_KEY_VLD);

    /* Decomposed structure interface */
    Proxy_Rx_Read_Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_RDC_MES_TSTMP(&Proxy_RxBus.Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_MES_TSTMP);
    Proxy_Rx_Read_Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_PCKG_ID(&Proxy_RxBus.Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.PCKG_ID);
    Proxy_Rx_Read_Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_SUPP_ID(&Proxy_RxBus.Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.SUPP_ID);
    Proxy_Rx_Read_Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_RDC_DT_5(&Proxy_RxBus.Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_5);
    Proxy_Rx_Read_Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_RDC_DT_8(&Proxy_RxBus.Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_8);
    Proxy_Rx_Read_Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_RDC_DT_7(&Proxy_RxBus.Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_7);
    Proxy_Rx_Read_Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_RDC_DT_6(&Proxy_RxBus.Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_6);
    Proxy_Rx_Read_Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_RDC_DT_1(&Proxy_RxBus.Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_1);
    Proxy_Rx_Read_Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_TYR_ID(&Proxy_RxBus.Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.TYR_ID);
    Proxy_Rx_Read_Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_RDC_DT_4(&Proxy_RxBus.Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_4);
    Proxy_Rx_Read_Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_RDC_DT_3(&Proxy_RxBus.Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_3);
    Proxy_Rx_Read_Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_RDC_DT_2(&Proxy_RxBus.Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_2);

    /* Decomposed structure interface */
    Proxy_Rx_Read_Proxy_RxBEDIENUNG_WISCHERInfo_OP_WISW(&Proxy_RxBus.Proxy_RxBEDIENUNG_WISCHERInfo.OP_WISW);
    Proxy_Rx_Read_Proxy_RxBEDIENUNG_WISCHERInfo_OP_WIPO(&Proxy_RxBus.Proxy_RxBEDIENUNG_WISCHERInfo.OP_WIPO);

    /* Decomposed structure interface */
    Proxy_Rx_Read_Proxy_RxDT_PT_2Info_ST_GRSEL_DRV(&Proxy_RxBus.Proxy_RxDT_PT_2Info.ST_GRSEL_DRV);
    Proxy_Rx_Read_Proxy_RxDT_PT_2Info_TEMP_EOI_DRV(&Proxy_RxBus.Proxy_RxDT_PT_2Info.TEMP_EOI_DRV);
    Proxy_Rx_Read_Proxy_RxDT_PT_2Info_RLS_ENGSTA(&Proxy_RxBus.Proxy_RxDT_PT_2Info.RLS_ENGSTA);
    Proxy_Rx_Read_Proxy_RxDT_PT_2Info_RPM_ENG_MAX_ALW(&Proxy_RxBus.Proxy_RxDT_PT_2Info.RPM_ENG_MAX_ALW);
    Proxy_Rx_Read_Proxy_RxDT_PT_2Info_RDUC_DOCTR_RPM_DRV_2(&Proxy_RxBus.Proxy_RxDT_PT_2Info.RDUC_DOCTR_RPM_DRV_2);
    Proxy_Rx_Read_Proxy_RxDT_PT_2Info_TEMP_ENG_DRV(&Proxy_RxBus.Proxy_RxDT_PT_2Info.TEMP_ENG_DRV);
    Proxy_Rx_Read_Proxy_RxDT_PT_2Info_ST_DRV_VEH(&Proxy_RxBus.Proxy_RxDT_PT_2Info.ST_DRV_VEH);
    Proxy_Rx_Read_Proxy_RxDT_PT_2Info_ST_ECU_DT_PT_2(&Proxy_RxBus.Proxy_RxDT_PT_2Info.ST_ECU_DT_PT_2);
    Proxy_Rx_Read_Proxy_RxDT_PT_2Info_ST_IDLG_ENG_DRV(&Proxy_RxBus.Proxy_RxDT_PT_2Info.ST_IDLG_ENG_DRV);
    Proxy_Rx_Read_Proxy_RxDT_PT_2Info_ST_ILK_STRT_DRV(&Proxy_RxBus.Proxy_RxDT_PT_2Info.ST_ILK_STRT_DRV);
    Proxy_Rx_Read_Proxy_RxDT_PT_2Info_ST_SW_CLT_DRV(&Proxy_RxBus.Proxy_RxDT_PT_2Info.ST_SW_CLT_DRV);
    Proxy_Rx_Read_Proxy_RxDT_PT_2Info_ST_ENG_RUN_DRV(&Proxy_RxBus.Proxy_RxDT_PT_2Info.ST_ENG_RUN_DRV);

    /* Decomposed structure interface */
    Proxy_Rx_Read_Proxy_RxSTAT_ENG_STA_AUTOInfo_RQ_MSA_ENG_STA_1(&Proxy_RxBus.Proxy_RxSTAT_ENG_STA_AUTOInfo.RQ_MSA_ENG_STA_1);
    Proxy_Rx_Read_Proxy_RxSTAT_ENG_STA_AUTOInfo_RQ_MSA_ENG_STA_2(&Proxy_RxBus.Proxy_RxSTAT_ENG_STA_AUTOInfo.RQ_MSA_ENG_STA_2);
    Proxy_Rx_Read_Proxy_RxSTAT_ENG_STA_AUTOInfo_PSBTY_MSA_ENG_STOP_STA(&Proxy_RxBus.Proxy_RxSTAT_ENG_STA_AUTOInfo.PSBTY_MSA_ENG_STOP_STA);
    Proxy_Rx_Read_Proxy_RxSTAT_ENG_STA_AUTOInfo_ST_SHFT_MSA_ENGSTP(&Proxy_RxBus.Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_SHFT_MSA_ENGSTP);
    Proxy_Rx_Read_Proxy_RxSTAT_ENG_STA_AUTOInfo_RQ_SLIP_K0(&Proxy_RxBus.Proxy_RxSTAT_ENG_STA_AUTOInfo.RQ_SLIP_K0);
    Proxy_Rx_Read_Proxy_RxSTAT_ENG_STA_AUTOInfo_ST_TAR_PENG_CENG(&Proxy_RxBus.Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_TAR_PENG_CENG);
    Proxy_Rx_Read_Proxy_RxSTAT_ENG_STA_AUTOInfo_SPEC_TYP_ENGSTA(&Proxy_RxBus.Proxy_RxSTAT_ENG_STA_AUTOInfo.SPEC_TYP_ENGSTA);
    Proxy_Rx_Read_Proxy_RxSTAT_ENG_STA_AUTOInfo_ST_RPM_CLCTR_MOT(&Proxy_RxBus.Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_RPM_CLCTR_MOT);
    Proxy_Rx_Read_Proxy_RxSTAT_ENG_STA_AUTOInfo_ST_FN_MSA(&Proxy_RxBus.Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_FN_MSA);
    Proxy_Rx_Read_Proxy_RxSTAT_ENG_STA_AUTOInfo_ST_AVL_PENG_CENG_ENGMG(&Proxy_RxBus.Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_AVL_PENG_CENG_ENGMG);
    Proxy_Rx_Read_Proxy_RxSTAT_ENG_STA_AUTOInfo_DISP_REAS_PREV_SWO_CENG(&Proxy_RxBus.Proxy_RxSTAT_ENG_STA_AUTOInfo.DISP_REAS_PREV_SWO_CENG);
    Proxy_Rx_Read_Proxy_RxSTAT_ENG_STA_AUTOInfo_VARI_TYP_ENGSTA(&Proxy_RxBus.Proxy_RxSTAT_ENG_STA_AUTOInfo.VARI_TYP_ENGSTA);
    Proxy_Rx_Read_Proxy_RxSTAT_ENG_STA_AUTOInfo_ST_TAR_CENG(&Proxy_RxBus.Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_TAR_CENG);
    Proxy_Rx_Read_Proxy_RxSTAT_ENG_STA_AUTOInfo_ST_AVAI_SAIL_DME(&Proxy_RxBus.Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_AVAI_SAIL_DME);

    Proxy_Rx_Read_Proxy_RxST_ENERG_GENInfo(&Proxy_RxBus.Proxy_RxST_ENERG_GENInfo);
    /*==============================================================================
    * Members of structure Proxy_RxST_ENERG_GENInfo 
     : Proxy_RxST_ENERG_GENInfo.ST_LDST_GEN_DRV;
     : Proxy_RxST_ENERG_GENInfo.DT_PCU_SCP;
     : Proxy_RxST_ENERG_GENInfo.ST_GEN_DRV;
     : Proxy_RxST_ENERG_GENInfo.AVL_I_GEN_DRV;
     : Proxy_RxST_ENERG_GENInfo.LDST_GEN_DRV;
     : Proxy_RxST_ENERG_GENInfo.ST_LDREL_GEN;
     : Proxy_RxST_ENERG_GENInfo.ST_CHG_STOR;
     : Proxy_RxST_ENERG_GENInfo.TEMP_BT_14V;
     : Proxy_RxST_ENERG_GENInfo.ST_I_IBS;
     : Proxy_RxST_ENERG_GENInfo.ST_SEP_STOR;
     : Proxy_RxST_ENERG_GENInfo.ST_BN2_SCP;
     =============================================================================*/
    
    Proxy_Rx_Read_Proxy_RxDT_PT_1Info(&Proxy_RxBus.Proxy_RxDT_PT_1Info);
    /*==============================================================================
    * Members of structure Proxy_RxDT_PT_1Info 
     : Proxy_RxDT_PT_1Info.RQ_SHPA_GRB_REGE_PAFI;
     : Proxy_RxDT_PT_1Info.ST_RTIR_DRV;
     : Proxy_RxDT_PT_1Info.CTR_SLCK_DRV;
     : Proxy_RxDT_PT_1Info.RQ_STASS_ENGMG;
     : Proxy_RxDT_PT_1Info.ST_CAT_HT;
     : Proxy_RxDT_PT_1Info.RQ_SHPA_GRB_CHGBLC;
     : Proxy_RxDT_PT_1Info.TAR_RPM_IDLG_DRV_EXT;
     : Proxy_RxDT_PT_1Info.SLCTN_BUS_COMM_ENG_GRB;
     : Proxy_RxDT_PT_1Info.TAR_RPM_IDLG_DRV;
     : Proxy_RxDT_PT_1Info.ST_INFS_DRV;
     : Proxy_RxDT_PT_1Info.ST_SW_WAUP_DRV;
     : Proxy_RxDT_PT_1Info.AIP_ENG_DRV;
     : Proxy_RxDT_PT_1Info.RDUC_DOCTR_RPM_DRV;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Proxy_Rx_Read_Proxy_RxDT_GRDT_DRVInfo_ST_OPMO_GRDT_DRV(&Proxy_RxBus.Proxy_RxDT_GRDT_DRVInfo.ST_OPMO_GRDT_DRV);
    Proxy_Rx_Read_Proxy_RxDT_GRDT_DRVInfo_ST_RSTA_GRDT(&Proxy_RxBus.Proxy_RxDT_GRDT_DRVInfo.ST_RSTA_GRDT);

    Proxy_Rx_Read_Proxy_RxDIAG_OBD_ENGMG_ELInfo(&Proxy_RxBus.Proxy_RxDIAG_OBD_ENGMG_ELInfo);
    /*==============================================================================
    * Members of structure Proxy_RxDIAG_OBD_ENGMG_ELInfo 
     : Proxy_RxDIAG_OBD_ENGMG_ELInfo.RQ_MIL_DIAG_OBD_ENGMG_EL;
     : Proxy_RxDIAG_OBD_ENGMG_ELInfo.RQ_RST_OBD_DIAG;
     =============================================================================*/
    
    Proxy_Rx_Read_Proxy_RxSTAT_CT_HABRInfo(&Proxy_RxBus.Proxy_RxSTAT_CT_HABRInfo);
    /*==============================================================================
    * Members of structure Proxy_RxSTAT_CT_HABRInfo 
     : Proxy_RxSTAT_CT_HABRInfo.ST_CT_HABR;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Proxy_Rx_Read_Proxy_RxDT_PT_3Info_TRNRAO_BAX(&Proxy_RxBus.Proxy_RxDT_PT_3Info.TRNRAO_BAX);
    Proxy_Rx_Read_Proxy_RxDT_PT_3Info_QU_TRNRAO_BAX(&Proxy_RxBus.Proxy_RxDT_PT_3Info.QU_TRNRAO_BAX);

    Proxy_Rx_Read_Proxy_RxEINHEITEN_BN2020Info(&Proxy_RxBus.Proxy_RxEINHEITEN_BN2020Info);
    /*==============================================================================
    * Members of structure Proxy_RxEINHEITEN_BN2020Info 
     : Proxy_RxEINHEITEN_BN2020Info.UN_TORQ_S_MOD;
     : Proxy_RxEINHEITEN_BN2020Info.UN_PWR_S_MOD;
     : Proxy_RxEINHEITEN_BN2020Info.UN_DATE_EXT;
     : Proxy_RxEINHEITEN_BN2020Info.UN_COSP_EL;
     : Proxy_RxEINHEITEN_BN2020Info.UN_TEMP;
     : Proxy_RxEINHEITEN_BN2020Info.UN_AIP;
     : Proxy_RxEINHEITEN_BN2020Info.LANG;
     : Proxy_RxEINHEITEN_BN2020Info.UN_DATE;
     : Proxy_RxEINHEITEN_BN2020Info.UN_T;
     : Proxy_RxEINHEITEN_BN2020Info.UN_SPDM_DGTL;
     : Proxy_RxEINHEITEN_BN2020Info.UN_MILE;
     : Proxy_RxEINHEITEN_BN2020Info.UN_FU;
     : Proxy_RxEINHEITEN_BN2020Info.UN_COSP;
     =============================================================================*/
    
    Proxy_Rx_Read_Proxy_RxA_TEMPInfo(&Proxy_RxBus.Proxy_RxA_TEMPInfo);
    /*==============================================================================
    * Members of structure Proxy_RxA_TEMPInfo 
     : Proxy_RxA_TEMPInfo.TEMP_EX_UNFILT;
     : Proxy_RxA_TEMPInfo.TEMP_EX;
     =============================================================================*/
    
    Proxy_Rx_Read_Proxy_RxWISCHERGESCHWINDIGKEITInfo(&Proxy_RxBus.Proxy_RxWISCHERGESCHWINDIGKEITInfo);
    /*==============================================================================
    * Members of structure Proxy_RxWISCHERGESCHWINDIGKEITInfo 
     : Proxy_RxWISCHERGESCHWINDIGKEITInfo.ST_RNSE;
     : Proxy_RxWISCHERGESCHWINDIGKEITInfo.INT_RN;
     : Proxy_RxWISCHERGESCHWINDIGKEITInfo.V_WI;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Proxy_Rx_Read_Proxy_RxSTAT_DS_VRFDInfo_ST_DSW_DRD_SFY_CTRL(&Proxy_RxBus.Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_DRD_SFY_CTRL);
    Proxy_Rx_Read_Proxy_RxSTAT_DS_VRFDInfo_ST_DSW_DVDR_VRFD(&Proxy_RxBus.Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_DVDR_VRFD);
    Proxy_Rx_Read_Proxy_RxSTAT_DS_VRFDInfo_ST_DSW_DVDR_SFY_CTRL(&Proxy_RxBus.Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_DVDR_SFY_CTRL);
    Proxy_Rx_Read_Proxy_RxSTAT_DS_VRFDInfo_ST_DSW_PSDR_SFY_CTRL(&Proxy_RxBus.Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_PSDR_SFY_CTRL);
    Proxy_Rx_Read_Proxy_RxSTAT_DS_VRFDInfo_ST_DSW_PSD_SFY_CTRL(&Proxy_RxBus.Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_PSD_SFY_CTRL);
    Proxy_Rx_Read_Proxy_RxSTAT_DS_VRFDInfo_ST_DSW_DRD_VRFD(&Proxy_RxBus.Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_DRD_VRFD);
    Proxy_Rx_Read_Proxy_RxSTAT_DS_VRFDInfo_ST_DSW_PSDR_VRFD(&Proxy_RxBus.Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_PSDR_VRFD);
    Proxy_Rx_Read_Proxy_RxSTAT_DS_VRFDInfo_ST_DSW_PSD_VRFD(&Proxy_RxBus.Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_PSD_VRFD);

    /* Decomposed structure interface */
    Proxy_Rx_Read_Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info_AVL_MOD_SW_DRDY(&Proxy_RxBus.Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY);
    Proxy_Rx_Read_Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info_RQ_SW_DRDY_MMID(&Proxy_RxBus.Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.RQ_SW_DRDY_MMID);
    Proxy_Rx_Read_Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info_RQ_SU_SW_DRDY(&Proxy_RxBus.Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.RQ_SU_SW_DRDY);
    Proxy_Rx_Read_Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info_RQ_SW_DRDY_KDIS(&Proxy_RxBus.Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.RQ_SW_DRDY_KDIS);
    Proxy_Rx_Read_Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info_AVL_MOD_SW_DRDY_CHAS(&Proxy_RxBus.Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY_CHAS);
    Proxy_Rx_Read_Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info_AVL_MOD_SW_DRDY_DRV(&Proxy_RxBus.Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY_DRV);
    Proxy_Rx_Read_Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info_AVL_MOD_SW_DRDY_STAB(&Proxy_RxBus.Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY_STAB);
    Proxy_Rx_Read_Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info_DISP_ST_DSC(&Proxy_RxBus.Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.DISP_ST_DSC);

    Proxy_Rx_Read_Proxy_RxSTAT_ANHAENGERInfo(&Proxy_RxBus.Proxy_RxSTAT_ANHAENGERInfo);
    /*==============================================================================
    * Members of structure Proxy_RxSTAT_ANHAENGERInfo 
     : Proxy_RxSTAT_ANHAENGERInfo.ST_SYNCN_HAZWCL_TRAI;
     : Proxy_RxSTAT_ANHAENGERInfo.ST_RFLI_DF_TRAI;
     : Proxy_RxSTAT_ANHAENGERInfo.ST_TRAI;
     : Proxy_RxSTAT_ANHAENGERInfo.ST_DI_DF_TRAI;
     : Proxy_RxSTAT_ANHAENGERInfo.ST_PO_AHV;
     =============================================================================*/
    
    Proxy_Rx_Read_Proxy_RxFZZSTDInfo(&Proxy_RxBus.Proxy_RxFZZSTDInfo);
    /*==============================================================================
    * Members of structure Proxy_RxFZZSTDInfo 
     : Proxy_RxFZZSTDInfo.ST_ILK_ERRM_FZM;
     : Proxy_RxFZZSTDInfo.ST_ENERG_FZM;
     : Proxy_RxFZZSTDInfo.ST_BT_PROTE_WUP;
     =============================================================================*/
    
    Proxy_Rx_Read_Proxy_RxBEDIENUNG_FAHRWERKInfo(&Proxy_RxBus.Proxy_RxBEDIENUNG_FAHRWERKInfo);
    /*==============================================================================
    * Members of structure Proxy_RxBEDIENUNG_FAHRWERKInfo 
     : Proxy_RxBEDIENUNG_FAHRWERKInfo.OP_MOD_TRCT_DSC;
     : Proxy_RxBEDIENUNG_FAHRWERKInfo.OP_TPCT;
     =============================================================================*/
    
    Proxy_Rx_Read_Proxy_RxFAHRZEUGTYPInfo(&Proxy_RxBus.Proxy_RxFAHRZEUGTYPInfo);
    /*==============================================================================
    * Members of structure Proxy_RxFAHRZEUGTYPInfo 
     : Proxy_RxFAHRZEUGTYPInfo.QUAN_CYL;
     : Proxy_RxFAHRZEUGTYPInfo.QUAN_GR;
     : Proxy_RxFAHRZEUGTYPInfo.TYP_VEH;
     : Proxy_RxFAHRZEUGTYPInfo.TYP_BODY;
     : Proxy_RxFAHRZEUGTYPInfo.TYP_ENG;
     : Proxy_RxFAHRZEUGTYPInfo.CLAS_PWR;
     : Proxy_RxFAHRZEUGTYPInfo.TYP_CNT;
     : Proxy_RxFAHRZEUGTYPInfo.TYP_STE;
     : Proxy_RxFAHRZEUGTYPInfo.TYP_GRB;
     : Proxy_RxFAHRZEUGTYPInfo.TYP_CAPA;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Proxy_Rx_Read_Proxy_RxST_BLT_CT_SOCCUInfo_ST_SEAT_OCCU_RRH(&Proxy_RxBus.Proxy_RxST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_RRH);
    Proxy_Rx_Read_Proxy_RxST_BLT_CT_SOCCUInfo_ST_BLTB_SW_RM(&Proxy_RxBus.Proxy_RxST_BLT_CT_SOCCUInfo.ST_BLTB_SW_RM);
    Proxy_Rx_Read_Proxy_RxST_BLT_CT_SOCCUInfo_ST_BLTB_SW_RRH(&Proxy_RxBus.Proxy_RxST_BLT_CT_SOCCUInfo.ST_BLTB_SW_RRH);
    Proxy_Rx_Read_Proxy_RxST_BLT_CT_SOCCUInfo_ST_ERR_SEAT_MT_DR(&Proxy_RxBus.Proxy_RxST_BLT_CT_SOCCUInfo.ST_ERR_SEAT_MT_DR);
    Proxy_Rx_Read_Proxy_RxST_BLT_CT_SOCCUInfo_ST_BLTB_SW_DR(&Proxy_RxBus.Proxy_RxST_BLT_CT_SOCCUInfo.ST_BLTB_SW_DR);
    Proxy_Rx_Read_Proxy_RxST_BLT_CT_SOCCUInfo_ST_SEAT_OCCU_DR(&Proxy_RxBus.Proxy_RxST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_DR);
    Proxy_Rx_Read_Proxy_RxST_BLT_CT_SOCCUInfo_ST_BLTB_SW_RLH(&Proxy_RxBus.Proxy_RxST_BLT_CT_SOCCUInfo.ST_BLTB_SW_RLH);
    Proxy_Rx_Read_Proxy_RxST_BLT_CT_SOCCUInfo_ST_SEAT_OCCU_RLH(&Proxy_RxBus.Proxy_RxST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_RLH);
    Proxy_Rx_Read_Proxy_RxST_BLT_CT_SOCCUInfo_ST_BLTB_SW_PS(&Proxy_RxBus.Proxy_RxST_BLT_CT_SOCCUInfo.ST_BLTB_SW_PS);
    Proxy_Rx_Read_Proxy_RxST_BLT_CT_SOCCUInfo_ST_SEAT_OCCU_PS(&Proxy_RxBus.Proxy_RxST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_PS);

    Proxy_Rx_Read_Proxy_RxFAHRGESTELLNUMMERInfo(&Proxy_RxBus.Proxy_RxFAHRGESTELLNUMMERInfo);
    /*==============================================================================
    * Members of structure Proxy_RxFAHRGESTELLNUMMERInfo 
     : Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_1;
     : Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_2;
     : Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_3;
     : Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_6;
     : Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_7;
     : Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_4;
     : Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_5;
     =============================================================================*/
    
    Proxy_Rx_Read_Proxy_RxRELATIVZEITInfo(&Proxy_RxBus.Proxy_RxRELATIVZEITInfo);
    /*==============================================================================
    * Members of structure Proxy_RxRELATIVZEITInfo 
     : Proxy_RxRELATIVZEITInfo.T_SEC_COU_REL;
     : Proxy_RxRELATIVZEITInfo.T_DAY_COU_ABSL;
     =============================================================================*/
    
    Proxy_Rx_Read_Proxy_RxKILOMETERSTANDInfo(&Proxy_RxBus.Proxy_RxKILOMETERSTANDInfo);
    /*==============================================================================
    * Members of structure Proxy_RxKILOMETERSTANDInfo 
     : Proxy_RxKILOMETERSTANDInfo.RNG;
     : Proxy_RxKILOMETERSTANDInfo.ST_FLLV_FUTA_SPAR;
     : Proxy_RxKILOMETERSTANDInfo.FLLV_FUTA_RH;
     : Proxy_RxKILOMETERSTANDInfo.FLLV_FUTA_LH;
     : Proxy_RxKILOMETERSTANDInfo.FLLV_FUTA;
     : Proxy_RxKILOMETERSTANDInfo.MILE_KM;
     =============================================================================*/
    
    Proxy_Rx_Read_Proxy_RxUHRZEIT_DATUMInfo(&Proxy_RxBus.Proxy_RxUHRZEIT_DATUMInfo);
    /*==============================================================================
    * Members of structure Proxy_RxUHRZEIT_DATUMInfo 
     : Proxy_RxUHRZEIT_DATUMInfo.DISP_DATE_WDAY;
     : Proxy_RxUHRZEIT_DATUMInfo.DISP_DATE_DAY;
     : Proxy_RxUHRZEIT_DATUMInfo.DISP_DATE_MON;
     : Proxy_RxUHRZEIT_DATUMInfo.ST_DISP_CTI_DATE;
     : Proxy_RxUHRZEIT_DATUMInfo.DISP_DATE_YR;
     : Proxy_RxUHRZEIT_DATUMInfo.DISP_HR;
     : Proxy_RxUHRZEIT_DATUMInfo.DISP_SEC;
     : Proxy_RxUHRZEIT_DATUMInfo.DISP_MN;
     =============================================================================*/
    
    Proxy_Rx_Read_Proxy_RxEcuModeSts(&Proxy_RxBus.Proxy_RxEcuModeSts);
    Proxy_Rx_Read_Proxy_RxPCtrlAct(&Proxy_RxBus.Proxy_RxPCtrlAct);
    Proxy_Rx_Read_Proxy_RxFuncInhibitProxySts(&Proxy_RxBus.Proxy_RxFuncInhibitProxySts);

    /* Process */
    Proxy_RxProc();

    /* Output */
    Proxy_Rx_Write_Proxy_RxCanRxRegenInfo(&Proxy_RxBus.Proxy_RxCanRxRegenInfo);
    /*==============================================================================
    * Members of structure Proxy_RxCanRxRegenInfo 
     : Proxy_RxCanRxRegenInfo.HcuRegenEna;
     : Proxy_RxCanRxRegenInfo.HcuRegenBrkTq;
     =============================================================================*/
    
    Proxy_Rx_Write_Proxy_RxCanRxAccelPedlInfo(&Proxy_RxBus.Proxy_RxCanRxAccelPedlInfo);
    /*==============================================================================
    * Members of structure Proxy_RxCanRxAccelPedlInfo 
     : Proxy_RxCanRxAccelPedlInfo.AccelPedlVal;
     : Proxy_RxCanRxAccelPedlInfo.AccelPedlValErr;
     =============================================================================*/
    
    Proxy_Rx_Write_Proxy_RxCanRxIdbInfo(&Proxy_RxBus.Proxy_RxCanRxIdbInfo);
    /*==============================================================================
    * Members of structure Proxy_RxCanRxIdbInfo 
     : Proxy_RxCanRxIdbInfo.EngMsgFault;
     : Proxy_RxCanRxIdbInfo.TarGearPosi;
     : Proxy_RxCanRxIdbInfo.GearSelDisp;
     : Proxy_RxCanRxIdbInfo.TcuSwiGs;
     : Proxy_RxCanRxIdbInfo.HcuServiceMod;
     : Proxy_RxCanRxIdbInfo.SubCanBusSigFault;
     : Proxy_RxCanRxIdbInfo.CoolantTemp;
     : Proxy_RxCanRxIdbInfo.CoolantTempErr;
     =============================================================================*/
    
    Proxy_Rx_Write_Proxy_RxCanRxEngTempInfo(&Proxy_RxBus.Proxy_RxCanRxEngTempInfo);
    /*==============================================================================
    * Members of structure Proxy_RxCanRxEngTempInfo 
     : Proxy_RxCanRxEngTempInfo.EngTemp;
     : Proxy_RxCanRxEngTempInfo.EngTempErr;
     =============================================================================*/
    
    Proxy_Rx_Write_Proxy_RxCanRxEscInfo(&Proxy_RxBus.Proxy_RxCanRxEscInfo);
    /*==============================================================================
    * Members of structure Proxy_RxCanRxEscInfo 
     : Proxy_RxCanRxEscInfo.Ax;
     : Proxy_RxCanRxEscInfo.YawRate;
     : Proxy_RxCanRxEscInfo.SteeringAngle;
     : Proxy_RxCanRxEscInfo.Ay;
     : Proxy_RxCanRxEscInfo.PbSwt;
     : Proxy_RxCanRxEscInfo.ClutchSwt;
     : Proxy_RxCanRxEscInfo.GearRSwt;
     : Proxy_RxCanRxEscInfo.EngActIndTq;
     : Proxy_RxCanRxEscInfo.EngRpm;
     : Proxy_RxCanRxEscInfo.EngIndTq;
     : Proxy_RxCanRxEscInfo.EngFrictionLossTq;
     : Proxy_RxCanRxEscInfo.EngStdTq;
     : Proxy_RxCanRxEscInfo.TurbineRpm;
     : Proxy_RxCanRxEscInfo.ThrottleAngle;
     : Proxy_RxCanRxEscInfo.TpsResol1000;
     : Proxy_RxCanRxEscInfo.PvAvCanResol1000;
     : Proxy_RxCanRxEscInfo.EngChr;
     : Proxy_RxCanRxEscInfo.EngVol;
     : Proxy_RxCanRxEscInfo.GearType;
     : Proxy_RxCanRxEscInfo.EngClutchState;
     : Proxy_RxCanRxEscInfo.EngTqCmdBeforeIntv;
     : Proxy_RxCanRxEscInfo.MotEstTq;
     : Proxy_RxCanRxEscInfo.MotTqCmdBeforeIntv;
     : Proxy_RxCanRxEscInfo.TqIntvTCU;
     : Proxy_RxCanRxEscInfo.TqIntvSlowTCU;
     : Proxy_RxCanRxEscInfo.TqIncReq;
     : Proxy_RxCanRxEscInfo.DecelReq;
     : Proxy_RxCanRxEscInfo.RainSnsStat;
     : Proxy_RxCanRxEscInfo.EngSpdErr;
     : Proxy_RxCanRxEscInfo.AtType;
     : Proxy_RxCanRxEscInfo.MtType;
     : Proxy_RxCanRxEscInfo.CvtType;
     : Proxy_RxCanRxEscInfo.DctType;
     : Proxy_RxCanRxEscInfo.HevAtTcu;
     : Proxy_RxCanRxEscInfo.TurbineRpmErr;
     : Proxy_RxCanRxEscInfo.ThrottleAngleErr;
     : Proxy_RxCanRxEscInfo.TopTrvlCltchSwtAct;
     : Proxy_RxCanRxEscInfo.TopTrvlCltchSwtActV;
     : Proxy_RxCanRxEscInfo.HillDesCtrlMdSwtAct;
     : Proxy_RxCanRxEscInfo.HillDesCtrlMdSwtActV;
     : Proxy_RxCanRxEscInfo.WiperIntSW;
     : Proxy_RxCanRxEscInfo.WiperLow;
     : Proxy_RxCanRxEscInfo.WiperHigh;
     : Proxy_RxCanRxEscInfo.WiperValid;
     : Proxy_RxCanRxEscInfo.WiperAuto;
     : Proxy_RxCanRxEscInfo.TcuFaultSts;
     =============================================================================*/
    
    Proxy_Rx_Write_Proxy_RxCanRxClu2Info(&Proxy_RxBus.Proxy_RxCanRxClu2Info);
    /*==============================================================================
    * Members of structure Proxy_RxCanRxClu2Info 
     : Proxy_RxCanRxClu2Info.IgnRun;
     =============================================================================*/
    
    Proxy_Rx_Write_Proxy_RxCanRxEemInfo(&Proxy_RxBus.Proxy_RxCanRxEemInfo);
    /*==============================================================================
    * Members of structure Proxy_RxCanRxEemInfo 
     : Proxy_RxCanRxEemInfo.YawRateInvld;
     : Proxy_RxCanRxEemInfo.AyInvld;
     : Proxy_RxCanRxEemInfo.SteeringAngleRxOk;
     : Proxy_RxCanRxEemInfo.AxInvldData;
     : Proxy_RxCanRxEemInfo.Ems1RxErr;
     : Proxy_RxCanRxEemInfo.Ems2RxErr;
     : Proxy_RxCanRxEemInfo.Tcu1RxErr;
     : Proxy_RxCanRxEemInfo.Tcu5RxErr;
     : Proxy_RxCanRxEemInfo.Hcu1RxErr;
     : Proxy_RxCanRxEemInfo.Hcu2RxErr;
     : Proxy_RxCanRxEemInfo.Hcu3RxErr;
     =============================================================================*/
    
    Proxy_Rx_Write_Proxy_RxCanRxInfo(&Proxy_RxBus.Proxy_RxCanRxInfo);
    /*==============================================================================
    * Members of structure Proxy_RxCanRxInfo 
     : Proxy_RxCanRxInfo.MainBusOffFlag;
     : Proxy_RxCanRxInfo.SenBusOffFlag;
     =============================================================================*/
    
    Proxy_Rx_Write_Proxy_RxIMUCalcInfo(&Proxy_RxBus.Proxy_RxIMUCalcInfo);
    /*==============================================================================
    * Members of structure Proxy_RxIMUCalcInfo 
     : Proxy_RxIMUCalcInfo.Reverse_Gear_flg;
     : Proxy_RxIMUCalcInfo.Reverse_Judge_Time;
     =============================================================================*/
    
    Proxy_Rx_Write_Proxy_RxCanRxGearSelDispErrInfo(&Proxy_RxBus.Proxy_RxCanRxGearSelDispErrInfo);

    Proxy_Rx_Timer_Elapsed = STM0_TIM0.U - Proxy_Rx_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define PROXY_RX_STOP_SEC_CODE
#include "Proxy_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
