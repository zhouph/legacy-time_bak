/**
 * @defgroup Proxy_Encode Proxy_Encode
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Proxy_Encode.c
 * @brief       Local Source File
 * @data        20YY. MM. DD.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Proxy_Encode.h"
#include "Com.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define SYS_PEDAL_SENSOR_MAX_STOKE 130

/*============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define PROXY_START_SEC_CONST_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define PROXY_STOP_SEC_CONST_UNSPECIFIED
#include "Proxy_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLES DEFINITIONS
 =============================================================================*/
#define PROXY_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
extern Proxy_Rx_HdrBusType Proxy_RxBus;
extern Proxy_Tx_HdrBusType Proxy_TxBus;

extern uint8 ccu1AxSnsrSetFlg;
extern uint8 Proxy_cu1_AY_RxMissing_Flg;
extern uint8 Proxy_cu1_AX_RxMissing_Flg;
extern uint8 Proxy_cu1_YAW_RxMissing_Flg;

sint16 Proxy_ws16PdtSnsrOfset;
uint8	Proxy_fsu8ServiceLampOnReq;
uint8 Proxy_fu1WheelFLErrDet;
uint8 Proxy_fu1WheelFRErrDet;
uint8 Proxy_fu1WheelRLErrDet;
uint8 Proxy_fu1WheelRRErrDet;
uint8 Proxy_fu1TCSDisabledBySW;
uint8 Proxy_fu1ESCDisabledBySW;
uint8 Proxy_feu1FlWssErrFlg;
uint8 Proxy_feu1FrWssErrFlg;
uint8 Proxy_feu1RlWssErrFlg;
uint8 Proxy_feu1RrWssErrFlg;
uint8 Proxy_fcu1ABSLampDrive;
uint8 Proxy_fcu1EBDLampDrive;
uint8 Proxy_fcu1VDCOffLampDrive;
uint8 Proxy_fcu1VDCLampDrive;
uint8 Proxy_fcu2FunctionLampDrive;
uint8 Proxy_fcu1DiagOn;
uint8 Proxy_fcu1TCSError;
uint8 Proxy_fcu1ABSError;
uint8 Proxy_fcu1EBDError;
uint8 Proxy_fcu1VDCError;
uint8 Proxy_fcu1HSAError;
uint16 Proxy_fcu16AxSnsr;
uint8 Proxy_fcu1AxSnsrDiag;
uint8 Proxy_fcu1AxSnsrInvalid;
uint16 Proxy_fcu16AySnsr;
uint8 Proxy_fcu1AySnsrDiag;
uint8 Proxy_fcu1AySnsrInvalid;
uint16 Proxy_fcu16YawSnsr;
uint8 Proxy_fcu1YawSnsrDiag;
uint8 Proxy_fcu1YawSnsrInvalid;
uint16 Proxy_fcu16McpSnsr;
uint8 Proxy_fcu1McpSnsrDiag;
uint8 Proxy_fcu1McpSnsrInvalid;
uint8 Proxy_fu1BrakeLampRequest;
uint8 Proxy_fu1HSAOn;
uint8 Proxy_fu1AhbOnDiag;
uint8 Proxy_fsu1AhbDefectiveModeFail;
uint8 Proxy_fsu1AhbDegradedModeFail;
uint8 Proxy_fsu1AhbWLampOnReq;
uint8 Proxy_fsu1PdtSenFaultDet;
uint8 Proxy_fsu1BuzzerOn;
uint8 Proxy_wu1PdtSnsrOfSetInvalid;
short int Proxy_fs16PDTSignal_1_10mm;
uint8 Proxy_lcahbu1AHBOn;
uint8 Proxy_lcahbu1HPACharge;

uint8 Proxy_lsahbu8AHBEEPofsReadInvalid;

uint8 Proxy_fyu1YawCbitReq;
uint8 Proxy_fyu1AYCbitReq;
uint8 Proxy_wbu1YawSNReq;

extern uint8 Proxy_EngMsgFaultFlg;
extern uint8 Proxy_SubCanBusSigFaultFlg;

uint8 wbu1AxOfsReadEep;
uint8 wbs16AxOfsFirstEep;

//@@ Added for SAL interface
signed int Proxy_fys16EstimatedYaw;
signed int Proxy_fys16EstimatedAY;
signed int Proxy_a_long_1_1000g;
extern Press_SenCircPBufInfo_t Press_SenCircPBufInfo;
extern Swt_SenBlsSwt_t Swt_SenBlsSwt;

extern Eem_MainEemFailData_t Eem_MainEemFailData;

#define WHL_PUL_ERR 0xFF

/* For Compile */
#define PROXY_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Proxy_MemMap.h"
#define PROXY_START_SEC_VAR_NOINIT_32BIT
#include "Proxy_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PROXY_STOP_SEC_VAR_NOINIT_32BIT
#include "Proxy_MemMap.h"
#define PROXY_START_SEC_VAR_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PROXY_STOP_SEC_VAR_UNSPECIFIED
#include "Proxy_MemMap.h"
#define PROXY_START_SEC_VAR_32BIT
#include "Proxy_MemMap.h"
/** Variable Section (32BIT)**/


#define PROXY_STOP_SEC_VAR_32BIT
#include "Proxy_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLES DEFINITIONS
 =============================================================================*/
#define PROXY_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define PROXY_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Proxy_MemMap.h"
#define PROXY_START_SEC_VAR_NOINIT_32BIT
#include "Proxy_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define PROXY_STOP_SEC_VAR_NOINIT_32BIT
#include "Proxy_MemMap.h"
#define PROXY_START_SEC_VAR_UNSPECIFIED
#include "Proxy_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define PROXY_STOP_SEC_VAR_UNSPECIFIED
#include "Proxy_MemMap.h"
#define PROXY_START_SEC_VAR_32BIT
#include "Proxy_MemMap.h"
/** Variable Section (32BIT)**/


#define PROXY_STOP_SEC_VAR_32BIT
#include "Proxy_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define PROXY_START_SEC_CODE
#include "Proxy_MemMap.h"

static void Proxy_TxYawCbit(void);
static void Proxy_TxWhlSpd(void);
static void Proxy_TxWhlPul(void);
static void Proxy_TxTcs5(void);
static void Proxy_TxTcs1(void);
static void Proxy_TxSasCal(void);
static void Proxy_TxEsp2(void);
static void Proxy_TxEbs1(void);
static void Proxy_TxAhb1(void);
static void Proxy_Getfcu16AySnr(void);
static void Proxy_Getfcu16AxSnsr(void);
static void Proxy_Getfcu16YawSnsr(void);
static void Proxy_Getfcu16McpSnsr(void);
static void Proxy_Getfu1BrakeLampRequest(void);
static void Proxy_GetPDTValue_Ofset(void);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Proxy_TxProc(void)
{
    Proxy_GetPDTValue_Ofset();
    Proxy_TxTcs1();
    Proxy_TxTcs5();
    Proxy_TxEsp2();
    Proxy_TxWhlPul();
    Proxy_TxAhb1();
    Proxy_TxEbs1();
    Proxy_TxWhlSpd();
    Proxy_TxYawCbit();
    Proxy_TxSasCal();
}
  
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static void Proxy_TxYawCbit(void)
{
    Com_SignalDataType ComSignalData;

    Proxy_TxBus.Proxy_TxTxYawCbitInfo.TestYawRateSensor=Proxy_fyu1YawCbitReq;
    //Com_SendSignal(COM_SIG_TX_YAWCBIT_TESTYAWRATESENSOR, &ComSignalData);

    Proxy_TxBus.Proxy_TxTxYawCbitInfo.TestAcclSensor=Proxy_fyu1AYCbitReq;
    //Com_SendSignal(COM_SIG_TX_YAWCBIT_TESTACCLSENSOR, &ComSignalData);

    Proxy_TxBus.Proxy_TxTxYawCbitInfo.YawSerialNumberReq=Proxy_wbu1YawSNReq;
    //Com_SendSignal(COM_SIG_TX_YAWCBIT_YAWSERIALNUMBERREQ, &ComSignalData);
}

static void Proxy_TxWhlSpd(void)
{
	Com_SignalDataType ComSignalData;

	Proxy_TxBus.Proxy_TxTxWhlSpdInfo.WhlSpdFL = Proxy_TxBus.Proxy_TxWhlSpdInfo.FlWhlSpd/2;
	Proxy_TxBus.Proxy_TxTxWhlSpdInfo.WhlSpdFR = Proxy_TxBus.Proxy_TxWhlSpdInfo.FrWhlSpd/2;
	Proxy_TxBus.Proxy_TxTxWhlSpdInfo.WhlSpdRL = Proxy_TxBus.Proxy_TxWhlSpdInfo.RlWhlSpd/2;
	Proxy_TxBus.Proxy_TxTxWhlSpdInfo.WhlSpdRR = Proxy_TxBus.Proxy_TxWhlSpdInfo.RrlWhlSpd/2;

#if 0 // comfile for PJS
  Proxy_fu1WheelFLErrDet = Proxy_TxBus.Proxy_TxErrWhlSpdInfo.FlWhlSpdErr;
  if(Proxy_fu1WheelFLErrDet==1)
  {
    Proxy_TxBus.Proxy_TxTxWhlSpdInfo.WhlSpdFL = 0x3FFF;
    //Com_SendSignal(COM_SIG_TX_WHLSPD_WHL_SPD_FL, &ComSignalData);
  }
  else
  {
    //ComSignalData = Wss_GetSpeedFL()/2;
    Proxy_TxBus.Proxy_TxTxWhlSpdInfo.WhlSpdFL = Proxy_TxBus.Proxy_TxWhlSpdInfo.FlWhlSpd/2;
    //Com_SendSignal(COM_SIG_TX_WHLSPD_WHL_SPD_FL, &ComSignalData);
  }

  Proxy_fu1WheelFRErrDet = Proxy_TxBus.Proxy_TxErrWhlSpdInfo.FrWhlSpdErr;
  if(Proxy_fu1WheelFRErrDet==1)
  {
    Proxy_TxBus.Proxy_TxTxWhlSpdInfo.WhlSpdFR = 0x3FFF;
    //Com_SendSignal(COM_SIG_TX_WHLSPD_WHL_SPD_FR, &ComSignalData);
  }
  else
  {
    //ComSignalData = Wss_GetSpeedFR()/2;
    Proxy_TxBus.Proxy_TxTxWhlSpdInfo.WhlSpdFR = Proxy_TxBus.Proxy_TxWhlSpdInfo.FrWhlSpd/2;
    //Com_SendSignal(COM_SIG_TX_WHLSPD_WHL_SPD_FR, &ComSignalData);
  }

  Proxy_fu1WheelRLErrDet = Proxy_TxBus.Proxy_TxErrWhlSpdInfo.RlWhlSpdErr;
  if(Proxy_fu1WheelRLErrDet==1)
  {
    Proxy_TxBus.Proxy_TxTxWhlSpdInfo.WhlSpdRL = 0x3FFF;
    //Com_SendSignal(COM_SIG_TX_WHLSPD_WHL_SPD_RL, &ComSignalData);
  }
  else
  {
    //ComSignalData = Wss_GetSpeedRL()/2;
    Proxy_TxBus.Proxy_TxTxWhlSpdInfo.WhlSpdRL = Proxy_TxBus.Proxy_TxWhlSpdInfo.RlWhlSpd/2;
    //Com_SendSignal(COM_SIG_TX_WHLSPD_WHL_SPD_RL, &ComSignalData);
  }

  Proxy_fu1WheelRRErrDet = Proxy_TxBus.Proxy_TxErrWhlSpdInfo.RrWhlSpdErr;
  if(Proxy_fu1WheelRRErrDet==1)
  {
    Proxy_TxBus.Proxy_TxTxWhlSpdInfo.WhlSpdRR = 0x3FFF;
    //Com_SendSignal(COM_SIG_TX_WHLSPD_WHL_SPD_RR, &ComSignalData);
  }
  else
  {

    //ComSignalData = Wss_GetSpeedRR()/2;
    Proxy_TxBus.Proxy_TxTxWhlSpdInfo.WhlSpdRR = Proxy_TxBus.Proxy_TxWhlSpdInfo.RrlWhlSpd/2;
    //Com_SendSignal(COM_SIG_TX_WHLSPD_WHL_SPD_RR, &ComSignalData);
  }
#endif // comfile for PJS  
}

static void Proxy_TxWhlPul(void)
{
	Com_SignalDataType ComSignalData;

    Proxy_TxBus.Proxy_TxTxWhlPulInfo.WhlPulFL=Proxy_TxBus.Proxy_TxWhlEdgeCntInfo.FlWhlEdgeCnt;
    Proxy_TxBus.Proxy_TxTxWhlPulInfo.WhlPulFR=Proxy_TxBus.Proxy_TxWhlEdgeCntInfo.FrWhlEdgeCnt;
    Proxy_TxBus.Proxy_TxTxWhlPulInfo.WhlPulRL=Proxy_TxBus.Proxy_TxWhlEdgeCntInfo.RlWhlEdgeCnt;
    Proxy_TxBus.Proxy_TxTxWhlPulInfo.WhlPulRR=Proxy_TxBus.Proxy_TxWhlEdgeCntInfo.RrWhlEdgeCnt;

#if 0 //comfile for PJS
  Proxy_feu1FlWssErrFlg = Proxy_TxBus.Proxy_TxErrWhlSpdInfo.FlWhlSpdErr;
  if (Proxy_feu1FlWssErrFlg==1)
  {
    Proxy_TxBus.Proxy_TxTxWhlPulInfo.WhlPulFL=WHL_PUL_ERR;
    //Com_SendSignal(COM_SIG_TX_WHL_PUL_FL, &ComSignalData);
  }
  else
  {
    //ComSignalData=Wss_GetWheelEdgeCnt(Wss_WssChannel_Ch_FL);
    Proxy_TxBus.Proxy_TxTxWhlPulInfo.WhlPulFL=Proxy_TxBus.Proxy_TxWhlEdgeCntInfo.FlWhlEdgeCnt;
    //Com_SendSignal(COM_SIG_TX_WHL_PUL_FL, &ComSignalData);
  }

  Proxy_feu1FrWssErrFlg = Proxy_TxBus.Proxy_TxErrWhlSpdInfo.FrWhlSpdErr;
  if (Proxy_feu1FrWssErrFlg==1)
  {
    Proxy_TxBus.Proxy_TxTxWhlPulInfo.WhlPulFR=WHL_PUL_ERR;
    //Com_SendSignal(COM_SIG_TX_WHL_PUL_FR, &ComSignalData);
  }
  else
  {
    //ComSignalData=Wss_GetWheelEdgeCnt(Wss_WssChannel_Ch_FR);
    Proxy_TxBus.Proxy_TxTxWhlPulInfo.WhlPulFR=Proxy_TxBus.Proxy_TxWhlEdgeCntInfo.FrWhlEdgeCnt;
    //Com_SendSignal(COM_SIG_TX_WHL_PUL_FR, &ComSignalData);
  }

  Proxy_feu1RlWssErrFlg = Proxy_TxBus.Proxy_TxErrWhlSpdInfo.RlWhlSpdErr;
  if (Proxy_feu1RlWssErrFlg==1)
  {
    Proxy_TxBus.Proxy_TxTxWhlPulInfo.WhlPulRL=WHL_PUL_ERR;
    //Com_SendSignal(COM_SIG_TX_WHL_PUL_RL, &ComSignalData);
  }
  else
  {
    //ComSignalData=Wss_GetWheelEdgeCnt(Wss_WssChannel_Ch_RL);
    Proxy_TxBus.Proxy_TxTxWhlPulInfo.WhlPulRL=Proxy_TxBus.Proxy_TxWhlEdgeCntInfo.RlWhlEdgeCnt;
   //Com_SendSignal(COM_SIG_TX_WHL_PUL_RL, &ComSignalData);
  }

  Proxy_feu1RrWssErrFlg = Proxy_TxBus.Proxy_TxErrWhlSpdInfo.RrWhlSpdErr;
  if (Proxy_feu1RrWssErrFlg==1)
  {
    Proxy_TxBus.Proxy_TxTxWhlPulInfo.WhlPulRR=WHL_PUL_ERR;
    //Com_SendSignal(COM_SIG_TX_WHL_PUL_RR, &ComSignalData);
  }
  else
  {
    //ComSignalData=Wss_GetWheelEdgeCnt(Wss_WssChannel_Ch_RR);
    Proxy_TxBus.Proxy_TxTxWhlPulInfo.WhlPulRR=Proxy_TxBus.Proxy_TxWhlEdgeCntInfo.RrWhlEdgeCnt;
    //Com_SendSignal(COM_SIG_TX_WHL_PUL_RR, &ComSignalData);
  }

  //ComSignalData = Proxy_CalWheelPulChkSum();
  //Com_SendSignal(COM_SIG_TX_WHL_PUL_CHKSUM, &ComSignalData);
#endif //comfile for PJS  
}

static void Proxy_TxTcs5(void)
{
  Com_SignalDataType ComSignalData;

  Proxy_fcu1ABSLampDrive = Proxy_TxBus.Proxy_TxEemLampData.Eem_Lamp_ABSLampRequest; /* Proxy_TxBus.Proxy_TxLampInfo.AbsLampDrv; */
  Proxy_TxBus.Proxy_TxTxTcs5Info.CfBrkAbsWLMP=Proxy_fcu1ABSLampDrive;
  //Com_SendSignal(COM_SIG_TX_TCS5_CF_BRK_ABSWLMP, &ComSignalData);

  Proxy_fcu1EBDLampDrive = Proxy_TxBus.Proxy_TxEemLampData.Eem_Lamp_EBDLampRequest; /* Proxy_TxBus.Proxy_TxLampInfo.EbdLampDrv; */
  Proxy_TxBus.Proxy_TxTxTcs5Info.CfBrkEbdWLMP=Proxy_fcu1EBDLampDrive;
  //Com_SendSignal(COM_SIG_TX_TCS5_CF_BRK_EBDWLMP, &ComSignalData);

  Proxy_fcu1VDCOffLampDrive = Proxy_TxBus.Proxy_TxEemLampData.Eem_Lamp_VDCOffLampRequest; /* Proxy_TxBus.Proxy_TxLampInfo.VdcOffLampDrv; */
  Proxy_TxBus.Proxy_TxTxTcs5Info.CfBrkTcsWLMP=Proxy_fcu1VDCOffLampDrive;
  //Com_SendSignal(COM_SIG_TX_TCS5_CF_BRK_TCSWLMP, &ComSignalData);

  Proxy_fcu1VDCLampDrive = Proxy_TxBus.Proxy_TxEemLampData.Eem_Lamp_VDCLampRequest; /* Proxy_TxBus.Proxy_TxLampInfo.VdcLampDrv; */
  Proxy_fcu2FunctionLampDrive = Proxy_TxBus.Proxy_TxEemLampData.Eem_Lamp_VDCFuncLampRequest; /* Proxy_TxBus.Proxy_TxLampInfo.FnLampDrv; */
  if(Proxy_fcu1VDCLampDrive==1)
  {
      Proxy_TxBus.Proxy_TxTxTcs5Info.CfBrkTcsFLMP=Proxy_fcu1VDCLampDrive;
      //Com_SendSignal(COM_SIG_TX_TCS5_CF_BRK_TCSFLMP, &ComSignalData);

  }
  else
  {
      Proxy_TxBus.Proxy_TxTxTcs5Info.CfBrkTcsFLMP=Proxy_fcu2FunctionLampDrive;
      //Com_SendSignal(COM_SIG_TX_TCS5_CF_BRK_TCSFLMP, &ComSignalData);
  }

  //Proxy_fcu1DiagOn = diag_sci_flg;
  Proxy_fcu1DiagOn = Proxy_TxBus.Proxy_TxDiagSci;
  Proxy_TxBus.Proxy_TxTxTcs5Info.CfBrkAbsDIAG=Proxy_fcu1DiagOn;
  //Com_SendSignal(COM_SIG_TX_TCS5_CF_BRK_ABSDIAG, &ComSignalData);

  Proxy_feu1FlWssErrFlg = Eem_MainEemFailData.Eem_Fail_WssFL; /* Proxy_TxBus.Proxy_TxErrWhlSpdInfo.FlWhlSpdErr; */
  if (Proxy_feu1FlWssErrFlg==1)
  {
    Proxy_TxBus.Proxy_TxTxTcs5Info.CrBrkWheelFL_KMH=0xFFF;
    //Com_SendSignal(COM_SIG_TX_TCS5_CR_BRK_WHEELFL_KMH, &ComSignalData);
  }
  else
  {
    //ComSignalData=Wss_GetSpeedFL()/8;
    Proxy_TxBus.Proxy_TxTxTcs5Info.CrBrkWheelFL_KMH=Proxy_TxBus.Proxy_TxWhlSpdInfo.FlWhlSpd/8;
    //Com_SendSignal(COM_SIG_TX_TCS5_CR_BRK_WHEELFL_KMH, &ComSignalData);
  }

  Proxy_feu1FrWssErrFlg = Eem_MainEemFailData.Eem_Fail_WssFR; /* Proxy_TxBus.Proxy_TxErrWhlSpdInfo.FrWhlSpdErr; */
  if (Proxy_feu1FrWssErrFlg==1)
  {
    Proxy_TxBus.Proxy_TxTxTcs5Info.CrBrkWheelFR_KMH=0xFFF;
    //Com_SendSignal(COM_SIG_TX_TCS5_CR_BRK_WHEELFR_KMH, &ComSignalData);
  }
  else
  {
    //ComSignalData=Wss_GetSpeedFR()/8;
    Proxy_TxBus.Proxy_TxTxTcs5Info.CrBrkWheelFR_KMH=Proxy_TxBus.Proxy_TxWhlSpdInfo.FrWhlSpd/8;
    //Com_SendSignal(COM_SIG_TX_TCS5_CR_BRK_WHEELFR_KMH, &ComSignalData);
  }

  Proxy_feu1RlWssErrFlg = Eem_MainEemFailData.Eem_Fail_WssRL; /* Proxy_TxBus.Proxy_TxErrWhlSpdInfo.RlWhlSpdErr; */
  if (Proxy_feu1RlWssErrFlg==1)
  {
    Proxy_TxBus.Proxy_TxTxTcs5Info.CrBrkWheelRL_KMH=0xFFF;
    //Com_SendSignal(COM_SIG_TX_TCS5_CR_BRK_WHEELRL_KMH, &ComSignalData);
  }
  else
  {
    //ComSignalData=Wss_GetSpeedRL()/8;
    Proxy_TxBus.Proxy_TxTxTcs5Info.CrBrkWheelRL_KMH = Proxy_TxBus.Proxy_TxWhlSpdInfo.RlWhlSpd/8;
    //Com_SendSignal(COM_SIG_TX_TCS5_CR_BRK_WHEELRL_KMH, &ComSignalData);
  }

  Proxy_feu1RrWssErrFlg = Eem_MainEemFailData.Eem_Fail_WssRR; /* Proxy_TxBus.Proxy_TxErrWhlSpdInfo.RrWhlSpdErr; */
  if (Proxy_feu1RrWssErrFlg==1)
  {
	  Proxy_TxBus.Proxy_TxTxTcs5Info.CrBrkWheelRR_KMH=0xFFF;
	  //Com_SendSignal(COM_SIG_TX_TCS5_CR_BRK_WHEELRR_KMH, &ComSignalData);
  }
  else
  {
	  //ComSignalData=Wss_GetSpeedRR()/8;
    Proxy_TxBus.Proxy_TxTxTcs5Info.CrBrkWheelRR_KMH=Proxy_TxBus.Proxy_TxWhlSpdInfo.RrlWhlSpd/8;
	  //Com_SendSignal(COM_SIG_TX_TCS5_CR_BRK_WHEELRR_KMH, &ComSignalData);
  }

}

static void Proxy_TxTcs1(void)
{
  Com_SignalDataType ComSignalData;
  
  //ComSignalData     = Proxy_lespu1TCS_TCS_REQ;
  Proxy_TxBus.Proxy_TxTxTcs1Info.CfBrkTcsREQ     = Proxy_TxBus.Proxy_TxCanTxInfo.TcsReq;
  //Com_SendSignal(COM_SIG_TX_TCS1_CF_BRK_TCSREQ, &ComSignalData);
  
  Proxy_fu1TCSDisabledBySW = Proxy_TxBus.Proxy_TxEscSwtStInfo.TcsDisabledBySwt;
  Proxy_TxBus.Proxy_TxTxTcs1Info.CfBrkTcsPAS     = Proxy_fu1TCSDisabledBySW;
  //Com_SendSignal(COM_SIG_TX_TCS1_CF_BRK_TCSPAS, &ComSignalData);
  
  Proxy_fcu1TCSError = Proxy_TxBus.Proxy_TxEemCtrlInhibitData.Eem_CtrlIhb_Tcs; /* Proxy_TxBus.Proxy_TxCtrlErrInfo.TcsFailure; */
  Proxy_TxBus.Proxy_TxTxTcs1Info.CfBrkTcsDEF     = Proxy_fcu1TCSError;
  //Com_SendSignal(COM_SIG_TX_TCS1_CF_BRK_TCSDEF, &ComSignalData);
  
  //ComSignalData     = Proxy_lespu1TCS_TCS_CTL;
  Proxy_TxBus.Proxy_TxTxTcs1Info.CfBrkTcsCTL     = Proxy_TxBus.Proxy_TxCanTxInfo.TcsCtrl;
  //Com_SendSignal(COM_SIG_TX_TCS1_CF_BRK_TCSCTL, &ComSignalData);
  
  //ComSignalData     = Proxy_lespu1TCS_ABS_ACT;
  Proxy_TxBus.Proxy_TxTxTcs1Info.CfBrkAbsACT     = Proxy_TxBus.Proxy_TxCanTxInfo.AbsAct;
  //Com_SendSignal(COM_SIG_TX_TCS1_CF_BRK_ABSACT, &ComSignalData);
  
  Proxy_fcu1ABSError = Proxy_TxBus.Proxy_TxEemCtrlInhibitData.Eem_CtrlIhb_Abs; /* Proxy_TxBus.Proxy_TxCtrlErrInfo.AbsFailure; */
  Proxy_TxBus.Proxy_TxTxTcs1Info.CfBrkAbsDEF     = Proxy_fcu1ABSError;
  //Com_SendSignal(COM_SIG_TX_TCS1_CF_BRK_ABSDEF, &ComSignalData);
  
  Proxy_fcu1EBDError = Proxy_TxBus.Proxy_TxEemCtrlInhibitData.Eem_CtrlIhb_Ebd; /* Proxy_TxBus.Proxy_TxCtrlErrInfo.EbdFailure; */
  Proxy_TxBus.Proxy_TxTxTcs1Info.CfBrkEbdDEF     = Proxy_fcu1EBDError;
  //Com_SendSignal(COM_SIG_TX_TCS1_CF_BRK_EBDDEF, &ComSignalData);
  
  //ComSignalData     = Proxy_lespu1TCS_TCS_GSC;
  Proxy_TxBus.Proxy_TxTxTcs1Info.CfBrkTcsGSC     = Proxy_TxBus.Proxy_TxCanTxInfo.TcsGearShiftChr;
  //Com_SendSignal(COM_SIG_TX_TCS1_CF_BRK_TCSGSC, &ComSignalData);
  
  Proxy_fu1ESCDisabledBySW = Proxy_TxBus.Proxy_TxEscSwtStInfo.EscDisabledBySwt;
  Proxy_TxBus.Proxy_TxTxTcs1Info.CfBrkEspPAS     = Proxy_fu1ESCDisabledBySW;
  //Com_SendSignal(COM_SIG_TX_TCS1_CF_BRK_ESPPAS, &ComSignalData);
  
  Proxy_fcu1VDCError = Proxy_TxBus.Proxy_TxEemCtrlInhibitData.Eem_CtrlIhb_Vdc; /* Proxy_TxBus.Proxy_TxCtrlErrInfo.VdcFailure; */
  Proxy_TxBus.Proxy_TxTxTcs1Info.CfBrkEspDEF     = Proxy_fcu1VDCError;
  //Com_SendSignal(COM_SIG_TX_TCS1_CF_BRK_ESPDEF, &ComSignalData);
  
  //ComSignalData     = Proxy_lespu1TCS_ESP_CTL;
  Proxy_TxBus.Proxy_TxTxTcs1Info.CfBrkEspCTL     = Proxy_TxBus.Proxy_TxCanTxInfo.EspCtrl;
  //Com_SendSignal(COM_SIG_TX_TCS1_CF_BRK_ESPCTL, &ComSignalData);
  
  //ComSignalData     = Proxy_lespu1TCS_MSR_C_REQ;
  Proxy_TxBus.Proxy_TxTxTcs1Info.CfBrkMsrREQ     = Proxy_TxBus.Proxy_TxCanTxInfo.MsrReq;
  //Com_SendSignal(COM_SIG_TX_TCS1_CF_BRK_MSRREQ, &ComSignalData);
  
  //ComSignalData    = Proxy_lespu1TCS_TCS_MFRN;
  Proxy_TxBus.Proxy_TxTxTcs1Info.CfBrkTcsMFRN    = Proxy_TxBus.Proxy_TxCanTxInfo.TcsProductInfo;
  //Com_SendSignal(COM_SIG_TX_TCS1_CF_BRK_TCSMFRN, &ComSignalData);
  
  //ComSignalData    = Proxy_lespu8ESP_GMIN_ESP;
  Proxy_TxBus.Proxy_TxTxTcs1Info.CfBrkMinGEAR    = Proxy_TxBus.Proxy_TxCanTxInfo.MinGear;
  //Com_SendSignal(COM_SIG_TX_TCS1_CF_BRK_MINGEAR, &ComSignalData);
  
  //ComSignalData    = Proxy_lespu8ESP_GMAX_ESP;
  Proxy_TxBus.Proxy_TxTxTcs1Info.CfBrkMaxGEAR    = Proxy_TxBus.Proxy_TxCanTxInfo.MaxGear;
  //Com_SendSignal(COM_SIG_TX_TCS1_CF_BRK_MAXGEAR, &ComSignalData);
  
  Proxy_Getfu1BrakeLampRequest();
  Proxy_TxBus.Proxy_TxTxTcs1Info.BrakeLight = Proxy_fu1BrakeLampRequest;
  //Com_SendSignal(COM_SIG_TX_TCS1_BRAKELIGHT, &ComSignalData);

  Proxy_TxBus.Proxy_TxTxTcs1Info.HacCtl = Proxy_fu1HSAOn;
  //Com_SendSignal(COM_SIG_TX_TCS1_HAC_CTL, &ComSignalData);

  Proxy_TxBus.Proxy_TxTxTcs1Info.HacPas = 1;
  //Com_SendSignal(COM_SIG_TX_TCS1_HAC_PAS, &ComSignalData);

  Proxy_TxBus.Proxy_TxTxTcs1Info.HacDef = Proxy_TxBus.Proxy_TxEemCtrlInhibitData.Eem_CtrlIhb_Hsa; /* Proxy_fcu1HSAError; */
  //Com_SendSignal(COM_SIG_TX_TCS1_HAC_DEF, &ComSignalData);

  Proxy_TxBus.Proxy_TxTxTcs1Info.CrBrkTQI_PC=(unsigned short)((((signed int)Proxy_TxBus.Proxy_TxCanTxInfo.TqIntvTCS)*10)/39);
  //Com_SendSignal(COM_SIG_TX_TCS1_CR_BRK_TQI_PC, &ComSignalData);
  
  Proxy_TxBus.Proxy_TxTxTcs1Info.CrBrkTQIMSR_PC=(unsigned short)((((signed int)Proxy_TxBus.Proxy_TxCanTxInfo.TqIntvMsr)*10)/39);
  //Com_SendSignal(COM_SIG_TX_TCS1_CR_BRK_TQIMSR_PC, &ComSignalData);
  
  Proxy_TxBus.Proxy_TxTxTcs1Info.CrBrkTQISLW_PC=(unsigned short)((((signed int)Proxy_TxBus.Proxy_TxCanTxInfo.TqIntvSlowTCS)*10)/39);
  //Com_SendSignal(COM_SIG_TX_TCS1_CR_BRK_TQISLW_PC, &ComSignalData);
  
  //Com_SendSignal(COM_SIG_TX_TCS1_CR_EBS_MSGCHKSUM, &ComSignalData);
}

static void Proxy_Getfu1BrakeLampRequest(void)
{
	signed int Proxy_bls_Raw;
	//SAL_Read_Sw_BlsRaw(&Proxy_bls_Raw);
	//Proxy_fu1BrakeLampRequest = Proxy_10msBus.Proxy_10msSwtStsInfo.BlsSwt;
	Proxy_fu1BrakeLampRequest = Swt_SenBlsSwt;
}

static void Proxy_TxSasCal(void)
{
	Com_SignalDataType ComSignalData;

	Proxy_TxBus.Proxy_TxTxSasCalInfo.CalSas_CCW = 0xa;
	//Com_SendSignal(COM_SIG_TX_CALSAS_CCW, &ComSignalData);

	Proxy_TxBus.Proxy_TxTxSasCalInfo.CalSas_Lws_CID = 0x7ff ;
	//Com_SendSignal(COM_SIG_TX_CALSAS_LWS_CID, &ComSignalData);
}

static void Proxy_TxEsp2(void)
{
  Com_SignalDataType ComSignalData;

  Proxy_Getfcu16AySnr();
  Proxy_TxBus.Proxy_TxTxEsp2Info.LatAccel = Proxy_fcu16AySnsr;
  //Com_SendSignal(COM_SIG_TX_ESP2_LAT_ACCEL, &ComSignalData);
  
  Proxy_TxBus.Proxy_TxTxEsp2Info.LatAccelStat = Proxy_fcu1AySnsrInvalid;
  //Com_SendSignal(COM_SIG_TX_ESP2_LAT_ACCEL_STAT, &ComSignalData);
  
  Proxy_TxBus.Proxy_TxTxEsp2Info.LatAccelDiag = Proxy_fcu1AySnsrDiag;
  //Com_SendSignal(COM_SIG_TX_ESP2_LAT_ACCEL_DIAG, &ComSignalData);
  
  Proxy_Getfcu16AxSnsr();
  if(ccu1AxSnsrSetFlg==0)
  {
  	Proxy_TxBus.Proxy_TxTxEsp2Info.LongAccel = 0;
    //Com_SendSignal(COM_SIG_TX_ESP2_LONG_ACCEL, &ComSignalData);

  	Proxy_TxBus.Proxy_TxTxEsp2Info.LongAccelStat = 0;
    //Com_SendSignal(COM_SIG_TX_ESP2_LONG_ACCEL_STAT, &ComSignalData);

  	Proxy_TxBus.Proxy_TxTxEsp2Info.LongAccelDiag = 0;
    //Com_SendSignal(COM_SIG_TX_ESP2_LONG_ACCEL_DIAG, &ComSignalData);
  }
  else
  {
  	Proxy_TxBus.Proxy_TxTxEsp2Info.LongAccel = Proxy_fcu16AxSnsr;
    //Com_SendSignal(COM_SIG_TX_ESP2_LONG_ACCEL, &ComSignalData);

  	Proxy_TxBus.Proxy_TxTxEsp2Info.LongAccelStat = Proxy_fcu1AxSnsrInvalid;
    //Com_SendSignal(COM_SIG_TX_ESP2_LONG_ACCEL_STAT, &ComSignalData);

  	Proxy_TxBus.Proxy_TxTxEsp2Info.LongAccelDiag = Proxy_fcu1AxSnsrDiag;
    //Com_SendSignal(COM_SIG_TX_ESP2_LONG_ACCEL_DIAG, &ComSignalData);
  }
  
  Proxy_Getfcu16YawSnsr();
  Proxy_TxBus.Proxy_TxTxEsp2Info.YawRate = Proxy_fcu16YawSnsr;
  //Com_SendSignal(COM_SIG_TX_ESP2_YAW_RATE, &ComSignalData);
  
  Proxy_TxBus.Proxy_TxTxEsp2Info.YawRateStat = Proxy_fcu1YawSnsrInvalid;
  //Com_SendSignal(COM_SIG_TX_ESP2_YAW_RATE_STAT, &ComSignalData);
  
  Proxy_TxBus.Proxy_TxTxEsp2Info.YawRateDiag = Proxy_fcu1YawSnsrDiag;
  //Com_SendSignal(COM_SIG_TX_ESP2_YAW_RATE_DIAG, &ComSignalData);
  
  Proxy_Getfcu16McpSnsr();
  Proxy_TxBus.Proxy_TxTxEsp2Info.CylPres = Proxy_fcu16McpSnsr;
  //Com_SendSignal(COM_SIG_TX_ESP2_CYL_PRES, &ComSignalData);
  
  Proxy_TxBus.Proxy_TxTxEsp2Info.CylPresStat = Proxy_fcu1McpSnsrInvalid;
  //Com_SendSignal(COM_SIG_TX_ESP2_CYL_PRES_STAT, &ComSignalData);
  
  Proxy_TxBus.Proxy_TxTxEsp2Info.CylPresDiag = Proxy_fcu1McpSnsrDiag;
  //Com_SendSignal(COM_SIG_TX_ESP2_CYL_PRES_DIAG, &ComSignalData);
}

void Proxy_Read_LatASnsrSigRaw(signed int *LatASnsrSigRaw)
{
    sint32 temp_LatASnsrSigRaw = 0;

    if(Proxy_RxBus.Proxy_RxCanRxEscInfo.Ay>=2048)
    {
        temp_LatASnsrSigRaw=-(sint16)(((sint32)(4095-Proxy_RxBus.Proxy_RxCanRxEscInfo.Ay)*625)/392);
    }
    else
    {
        temp_LatASnsrSigRaw=(sint16)(((sint32)Proxy_RxBus.Proxy_RxCanRxEscInfo.Ay*625)/392);
    }

    if (Proxy_Config.AyDirection == FORWARD)
    {
        temp_LatASnsrSigRaw = temp_LatASnsrSigRaw;
    }
    else 
    {
        temp_LatASnsrSigRaw = -temp_LatASnsrSigRaw;
    }

    *LatASnsrSigRaw = temp_LatASnsrSigRaw;
}

static void Proxy_Getfcu16AySnr(void)
{
	Proxy_Read_LatASnsrSigRaw(&Proxy_fys16EstimatedAY);
	Proxy_TxBus.Proxy_TxTxESCSensorInfo.EstimatedAY = Proxy_fys16EstimatedAY;
	Proxy_fcu1AySnsrInvalid = Proxy_cu1_AY_RxMissing_Flg;

	if(Proxy_fcu1AySnsrInvalid==1)
	{
		Proxy_fcu16AySnsr=0;
		Proxy_fcu1AySnsrDiag=0;
	}
	else
	{
	  if(0/*cbit_process_step==1*/)
	  {
	  	//fcu16AySnsr=fcu16AySnsrOld;
	  	Proxy_fcu1AySnsrDiag=1;
	  }
	  else
	  {
	      if(Proxy_fys16EstimatedAY >= 1043)
	      {
	      	Proxy_fcu16AySnsr=0x7FE;
	      }
	      else if(Proxy_fys16EstimatedAY <= -1043)
	      {
	      	Proxy_fcu16AySnsr=0;
	      }
	      else
	      {
	      	Proxy_fcu16AySnsr = (unsigned short)(((((signed int)Proxy_fys16EstimatedAY)*49)/50)+1023);
	        //fcu16AySnsrOld = fcu16AySnsr;
	      }

	      Proxy_fcu1AySnsrDiag = 0;
	  }
	}

#if 0 //comfile for PJS
  if(Proxy_TxBus.Proxy_TxCanErrInfo.AyErr/*fu1AyErrorDet==1*/)
  {
  	Proxy_fcu16AySnsr=0x7FF;
  	Proxy_fcu1AySnsrInvalid=1;
  	Proxy_fcu1AySnsrDiag=0;
  }
  else
  {
      if(Proxy_fcu1AySnsrInvalid==1)
      {
      	Proxy_fcu16AySnsr=0;
      	Proxy_fcu1AySnsrDiag=0;
      }
      else
      {
          if(0/*cbit_process_step==1*/)
          {
          	//fcu16AySnsr=fcu16AySnsrOld;
          	Proxy_fcu1AySnsrDiag=1;
          }
          else
          {
              if(Proxy_fys16EstimatedAY >= 1043)
              {
              	Proxy_fcu16AySnsr=0x7FE;
              }
              else if(Proxy_fys16EstimatedAY <= -1043)
              {
              	Proxy_fcu16AySnsr=0;
              }
              else
              {
              	Proxy_fcu16AySnsr = (unsigned short)(((((signed int)Proxy_fys16EstimatedAY)*49)/50)+1023);
                //fcu16AySnsrOld = fcu16AySnsr;
              }

              Proxy_fcu1AySnsrDiag = 0;
          }
      }
  }
#endif //comfile for PJS  
}

void Proxy_Read_LgtASnsrSigRaw(signed int *LgtASnsrSigRaw)
{
    sint32 temp_LgtASnsrSigRaw = 0;

    if(Proxy_RxBus.Proxy_RxCanRxEscInfo.Ax>=2048)
    {
        temp_LgtASnsrSigRaw=(sint16)((sint32)((Proxy_RxBus.Proxy_RxCanRxEscInfo.Ax-4095)*625)/392);
    }
    else
    {
        temp_LgtASnsrSigRaw=(sint16)((sint32)(Proxy_RxBus.Proxy_RxCanRxEscInfo.Ax*625)/392);
    }

    if (Proxy_Config.AxDirection == FORWARD)
    {
        temp_LgtASnsrSigRaw = temp_LgtASnsrSigRaw;
    }
    else
    {
        temp_LgtASnsrSigRaw = - temp_LgtASnsrSigRaw;
    }

    *LgtASnsrSigRaw = temp_LgtASnsrSigRaw;
}

static void Proxy_Getfcu16AxSnsr(void)
{
	short int Proxy_fs16_CalAx;
	/***** Ax Snsr For HMC CAN spec *****/
	Proxy_Read_LgtASnsrSigRaw(&Proxy_a_long_1_1000g);
	Proxy_TxBus.Proxy_TxTxESCSensorInfo.A_long_1_1000g = Proxy_a_long_1_1000g;
	Proxy_fcu1AxSnsrInvalid = Proxy_cu1_AX_RxMissing_Flg;

	if(Proxy_fcu1AxSnsrInvalid==1)
	{
		Proxy_fcu16AxSnsr=0;
		Proxy_fcu1AxSnsrDiag=0;
	}
	else
	{
	    if(0/*cbit_process_step==1*/)
	    {
	        //fcu16AxSnsr=fcu16AxSnsrOld;
	    	Proxy_fcu1AxSnsrDiag=1;
	    }
	    else
	    {
	        if(wbu1AxOfsReadEep==1)
	        {
	        	Proxy_fs16_CalAx = Proxy_a_long_1_1000g-wbs16AxOfsFirstEep;
	        }
	        else
	        {
	        	Proxy_fs16_CalAx = Proxy_a_long_1_1000g;
	        }

	  			if(Proxy_fs16_CalAx >= 1043)
	        {
			          Proxy_fcu16AxSnsr=0x7FE;
	        }
	        else if(Proxy_fs16_CalAx <= -1043)
	        {
	        	Proxy_fcu16AxSnsr=0;
	        }
	        else
	        {
	        	Proxy_fcu16AxSnsr = (unsigned short)(((((signed int)Proxy_fs16_CalAx)*49)/50)+1023);
	          //fcu16AxSnsrOld = fcu16AxSnsr;
	        }
		          Proxy_fcu1AxSnsrDiag = 0;
	    }
	}

#if 0 //comfile for PJS
    if(Proxy_TxBus.Proxy_TxCanErrInfo.AxErr/*fu1AxErrorDet==1*/)
    {
    	Proxy_fcu16AxSnsr=0x7FF;
    	Proxy_fcu1AxSnsrInvalid=1;
    	Proxy_fcu1AxSnsrDiag=0;
    }
    else
    {
        if(Proxy_fcu1AxSnsrInvalid==1)
        {
        	Proxy_fcu16AxSnsr=0;
        	Proxy_fcu1AxSnsrDiag=0;
        }
        else
        {
            if(0/*cbit_process_step==1*/)
            {
                //fcu16AxSnsr=fcu16AxSnsrOld;
            	Proxy_fcu1AxSnsrDiag=1;
            }
            else
            {
                if(wbu1AxOfsReadEep==1)
                {
                	Proxy_fs16_CalAx = Proxy_a_long_1_1000g-wbs16AxOfsFirstEep;
                }
                else
                {
                	Proxy_fs16_CalAx = Proxy_a_long_1_1000g;
                }

          			if(Proxy_fs16_CalAx >= 1043)
                {
				          Proxy_fcu16AxSnsr=0x7FE;
                }
                else if(Proxy_fs16_CalAx <= -1043)
                {
                	Proxy_fcu16AxSnsr=0;
                }
                else
                {
                	Proxy_fcu16AxSnsr = (unsigned short)(((((signed int)Proxy_fs16_CalAx)*49)/50)+1023);
                  //fcu16AxSnsrOld = fcu16AxSnsr;
                }
			          Proxy_fcu1AxSnsrDiag = 0;
            }
        }
    }
#endif //comfile for PJS    
}

void Proxy_Read_YawRateRaw(signed int *YawRateRaw)
{
    sint32 temp_YawRateRaw = 0;

    if(Proxy_RxBus.Proxy_RxCanRxEscInfo.YawRate>=2048)
    {
        temp_YawRateRaw=(sint16)(((sint32)(Proxy_RxBus.Proxy_RxCanRxEscInfo.YawRate-4095)*625)/100);               }
    else
    {
        temp_YawRateRaw=(sint16)(((sint32)Proxy_RxBus.Proxy_RxCanRxEscInfo.YawRate*625)/100);
    }
    
    *YawRateRaw = temp_YawRateRaw;
}

static void Proxy_Getfcu16YawSnsr(void)
{
	Proxy_Read_YawRateRaw(&Proxy_fys16EstimatedYaw);
	Proxy_TxBus.Proxy_TxTxESCSensorInfo.EstimatedYaw = Proxy_fys16EstimatedYaw;
	Proxy_fcu1YawSnsrInvalid = Proxy_cu1_YAW_RxMissing_Flg;

	if(Proxy_fcu1YawSnsrInvalid==1)
	{
		Proxy_fcu16YawSnsr=0;
		Proxy_fcu1YawSnsrDiag=0;
	}
	else
	{
		if(0/*cbit_process_step==1*/)
		{
		  //fcu16YawSnsr=fcu16YawSnsrOld;
		Proxy_fcu1YawSnsrDiag=1;
		}
		else
		{
		  if(Proxy_fys16EstimatedYaw >= 4095)
		  {
		  	Proxy_fcu16YawSnsr=0x1FFE;
		  }
		  else if(Proxy_fys16EstimatedYaw <= -4095)
		  {
		  	Proxy_fcu16YawSnsr=0;
		  }
		  else
		  {
		  	Proxy_fcu16YawSnsr = (unsigned short)(Proxy_fys16EstimatedYaw+4095);
		      //fcu16YawSnsrOld = fcu16YawSnsr;
		  }

		  Proxy_fcu1YawSnsrDiag = 0;
		}
    }

#if 0 //comfile for PJS
	if(Proxy_TxBus.Proxy_TxCanErrInfo.YawErr/*fu1YawErrorDet==1*/)
		{
			Proxy_fcu16YawSnsr=0x1FFF;
			Proxy_fcu1YawSnsrInvalid=1;
			Proxy_fcu1YawSnsrDiag=0;
		}
		else
	    {
			if(Proxy_fcu1YawSnsrInvalid==1)
			{
				Proxy_fcu16YawSnsr=0;
				Proxy_fcu1YawSnsrDiag=0;
			}
			else
			{
          if(0/*cbit_process_step==1*/)
          {
              //fcu16YawSnsr=fcu16YawSnsrOld;
          	Proxy_fcu1YawSnsrDiag=1;
          }
          else
          {
              if(Proxy_fys16EstimatedYaw >= 4095)
              {
              	Proxy_fcu16YawSnsr=0x1FFE;
              }
              else if(Proxy_fys16EstimatedYaw <= -4095)
              {
              	Proxy_fcu16YawSnsr=0;
              }
              else
              {
              	Proxy_fcu16YawSnsr = (unsigned short)(Proxy_fys16EstimatedYaw+4095);
                  //fcu16YawSnsrOld = fcu16YawSnsr;
              }

              Proxy_fcu1YawSnsrDiag = 0;
          }
      }
    }
#endif //comfile for PJS    
}

static void Proxy_Getfcu16McpSnsr(void)
{
	signed int Proxy_BCP1_POS_100_BAR;
	sint16 Proxy_fs16PedalTravelFilt;
	Proxy_fs16PedalTravelFilt = Proxy_fs16PDTSignal_1_10mm-Proxy_ws16PdtSnsrOfset;
	//Proxy_BCP1_POS_100_BAR = Proxy_10msBus.Proxy_10msCircPRawInfo.SecdCircPRaw;
	Proxy_BCP1_POS_100_BAR = Press_SenCircPBufInfo.SecdCircPRaw[4];	

	if(Proxy_fs16PedalTravelFilt >= 30)
	{
		Proxy_fcu16McpSnsr=(unsigned short)(Proxy_BCP1_POS_100_BAR/10);
	}
	else
	{
		Proxy_fcu16McpSnsr=0;
	}

	if(Proxy_BCP1_POS_100_BAR<= 0)
	{
		Proxy_fcu16McpSnsr=0;
	}
	Proxy_fcu1McpSnsrInvalid = 0;
	Proxy_fcu1McpSnsrDiag = 0;

#if 0 //comfile for PJS
  if(Proxy_TxBus.Proxy_TxPedErrInfo.PedlErr/*(fsu1BCP1SenFaultDet==1)||(fsu1BCP2SenFaultDet==1)||((fsu1PdtSenFaultDet==1) && (fsu1PdfSenFaultDet==1)) */)
  {
  	Proxy_fcu16McpSnsr=0xFFF;
  	Proxy_fcu1McpSnsrInvalid=1;
  	Proxy_fcu1McpSnsrDiag=0;
  }
  else
  {
      if(Proxy_fs16PedalTravelFilt >= 30)
      {
      	Proxy_fcu16McpSnsr=(unsigned short)(Proxy_BCP1_POS_100_BAR/10);
      }
      else
      {
      	Proxy_fcu16McpSnsr=0;
      }

      if(Proxy_BCP1_POS_100_BAR<= 0)
      {
      	Proxy_fcu16McpSnsr=0;
      }
      Proxy_fcu1McpSnsrInvalid = 0;
      Proxy_fcu1McpSnsrDiag = 0;
  }
#endif //comfile for PJS  
}

static void Proxy_TxEbs1(void)
{
	Com_SignalDataType ComSignalData;
	uint8_t u8t_StkDep_Pc;
	signed short tempStkDep_Offset=0;
	static uint8_t Proxy_Brk_PgmRun1=0;
	
	//ComSignalData = Proxy_lrbcu1EBS_CF_Brk_EHBStat;
  Proxy_TxBus.Proxy_TxTxEbs1Info.CfBrkEbsStat = Proxy_TxBus.Proxy_TxTarRgnBrkTqInfo.EhbStat;
	//Com_SendSignal(COM_SIG_TX_EBS1_CF_BRK_EBSSTAT, &ComSignalData);

	//ComSignalData = Proxy_lrbcu16EBS_CR_Brk_HHTCmd_Nm;
  Proxy_TxBus.Proxy_TxTxEbs1Info.CrBrkRegenTQLimit_NM = Proxy_TxBus.Proxy_TxTarRgnBrkTqInfo.TarRgnBrkTq;
	//Com_SendSignal(COM_SIG_TX_EBS1_CR_BRK_REGENTQLIMIT_NM, &ComSignalData);

	//ComSignalData = Proxy_lrbcu16EBS_CR_Brk_EstTot_Nm;
  Proxy_TxBus.Proxy_TxTxEbs1Info.CrBrkEstTot_NM = Proxy_TxBus.Proxy_TxTarRgnBrkTqInfo.EstTotBrkForce;
	//Com_SendSignal(COM_SIG_TX_EBS1_CR_BRK_ESTTOT_NM, &ComSignalData);

    Proxy_fsu1PdtSenFaultDet = Eem_MainEemFailData.Eem_Fail_PedalPDT;
	Proxy_TxBus.Proxy_TxTxEbs1Info.CfBrkSnsFail  = Proxy_fsu1PdtSenFaultDet;
	//Com_SendSignal(COM_SIG_TX_EBS1_CF_BRK_SNSFAIL, &ComSignalData);

	Proxy_TxBus.Proxy_TxTxEbs1Info.CfBrkRbsWLamp = Proxy_TxBus.Proxy_TxEemLampData.Eem_Lamp_RBCSLampRequest; /* 0 */;
	//Com_SendSignal(COM_SIG_TX_EBS1_CF_BRK_RBSWLAMP, &ComSignalData);

	Proxy_TxBus.Proxy_TxTxEbs1Info.CfBrkVacuumSysDef = 0;
	//Com_SendSignal(COM_SIG_TX_EBS1_CF_BRK_VACUUMSYSDEF, &ComSignalData);

	Proxy_TxBus.Proxy_TxTxEbs1Info.CrBrkEstHyd_NM = 0;
	//Com_SendSignal(COM_SIG_TX_EBS1_CR_BRK_ESTHYD_NM, &ComSignalData);

	Proxy_wu1PdtSnsrOfSetInvalid = Proxy_lsahbu8AHBEEPofsReadInvalid;
  if(Proxy_wu1PdtSnsrOfSetInvalid == 0)
  {
      tempStkDep_Offset=Proxy_fs16PDTSignal_1_10mm-Proxy_ws16PdtSnsrOfset;
  }
  else
  {
      tempStkDep_Offset=Proxy_fs16PDTSignal_1_10mm;
  }

  if(tempStkDep_Offset<0) u8t_StkDep_Pc=0;
  else if(tempStkDep_Offset>=(SYS_PEDAL_SENSOR_MAX_STOKE*10)) u8t_StkDep_Pc=0xFF;
  else u8t_StkDep_Pc=(uint8_t)(tempStkDep_Offset*12800ul/SYS_PEDAL_SENSOR_MAX_STOKE/500);

  Proxy_TxBus.Proxy_TxTxEbs1Info.CrBrkStkDep_PC = u8t_StkDep_Pc;
  //Com_SendSignal(COM_SIG_TX_EBS1_CR_BRK_STKDEP_PC, &ComSignalData);
  
  Proxy_Brk_PgmRun1^=1;
  Proxy_TxBus.Proxy_TxTxEbs1Info.CfBrkPgmRun1 = Proxy_Brk_PgmRun1;
  //Com_SendSignal(COM_SIG_TX_EBS1_CF_BRK_PGMRUN1, &Proxy_Brk_PgmRun1);
}

static void Proxy_TxAhb1(void)
{
	Com_SignalDataType ComSignalData;
	sint16 ComSignalData1;
#if 0 //comfile for PJS		
	Proxy_fsu1AhbWLampOnReq = Proxy_TxBus.Proxy_TxEemLampData.Eem_Lamp_AHBLampRequest; /* Proxy_TxBus.Proxy_TxLampInfo.AhbWLampOnReq; */
	Proxy_TxBus.Proxy_TxTxAhb1Info.CfAhbWLMP = Proxy_fsu1AhbWLampOnReq;
	//Com_SendSignal(COM_SIG_TX_AHB1_CF_AHB_WLMP, &ComSignalData);
#endif //comfile for PJS  

	//Proxy_fu1AhbOnDiag = diag_ahb_sci_flg;
  Proxy_fu1AhbOnDiag = Proxy_TxBus.Proxy_TxDiagAhbSci;
	Proxy_TxBus.Proxy_TxTxAhb1Info.CfAhbDiag = Proxy_fu1AhbOnDiag;
	//Com_SendSignal(COM_SIG_TX_AHB1_CF_AHB_DIAG, &ComSignalData);

	if((Proxy_lcahbu1HPACharge ==1)&&(Proxy_lcahbu1AHBOn==1))
	{
		Proxy_TxBus.Proxy_TxTxAhb1Info.CfAhbAct = 3; /*Charging+AHB Control*/
	}
	else if(Proxy_lcahbu1AHBOn ==1)
	{
		Proxy_TxBus.Proxy_TxTxAhb1Info.CfAhbAct = 2; /*AHB Control*/
	}
	else if(Proxy_lcahbu1HPACharge==1)
	{
		Proxy_TxBus.Proxy_TxTxAhb1Info.CfAhbAct = 1; /*Charging*/
	}
	else
	{
		Proxy_TxBus.Proxy_TxTxAhb1Info.CfAhbAct = 0;
	}

	//Com_SendSignal(COM_SIG_TX_AHB1_CF_AHB_ACT, &ComSignalData);


	Proxy_fsu1AhbDefectiveModeFail = Proxy_TxBus.Proxy_TxEemCtrlInhibitData.Eem_BBS_DefectiveModeFail; /* Proxy_TxBus.Proxy_TxAhbErrInfo.AhbDfctvModFailure; */
	Proxy_fsu1AhbDegradedModeFail = Proxy_TxBus.Proxy_TxEemCtrlInhibitData.Eem_BBS_DegradeModeFail; /* Proxy_TxBus.Proxy_TxAhbErrInfo.AhbDegradedModFailure; */
	if(Proxy_fsu1AhbDefectiveModeFail==1)
	{
		Proxy_TxBus.Proxy_TxTxAhb1Info.CfAhbDef=2; /*AHB is defective mode*/
	}
	else if(Proxy_fsu1AhbDegradedModeFail ==1)
	{
		Proxy_TxBus.Proxy_TxTxAhb1Info.CfAhbDef=1; /*AHB is Degraded mode*/
	}
	else
	{
		Proxy_TxBus.Proxy_TxTxAhb1Info.CfAhbDef=0;
	}

	//Com_SendSignal(COM_SIG_TX_AHB1_CF_AHB_DEF, &ComSignalData);

	Proxy_fsu8ServiceLampOnReq = Proxy_TxBus.Proxy_TxEemLampData.Eem_Lamp_ServiceLampRequest; /* Proxy_TxBus.Proxy_TxLampInfo.SrvLampOnReq; */
	Proxy_TxBus.Proxy_TxTxAhb1Info.CfAhbSLMP = Proxy_fsu8ServiceLampOnReq;
	//Com_SendSignal(COM_SIG_TX_AHB1_CF_AHB_SLMP, &ComSignalData);

	Proxy_wu1PdtSnsrOfSetInvalid = Proxy_lsahbu8AHBEEPofsReadInvalid;
  if(Proxy_wu1PdtSnsrOfSetInvalid == 0)
  {
  	ComSignalData1=Proxy_fs16PDTSignal_1_10mm-Proxy_ws16PdtSnsrOfset;
  }
  else
  {
  	ComSignalData1=Proxy_fs16PDTSignal_1_10mm;
  }
  Proxy_TxBus.Proxy_TxTxAhb1Info.CfBrkAhbSTDep_PC = (unsigned short)ComSignalData1;
  //Com_SendSignal(COM_SIG_TX_AHB1_CF_BRK_AHBSTDEP_PC, &ComSignalData);

#if 0 //comfile for PJS		
  Proxy_fsu1BuzzerOn = Proxy_TxBus.Proxy_TxBuzzerInfo;
  Proxy_TxBus.Proxy_TxTxAhb1Info.CfBrkBuzzR = Proxy_fsu1BuzzerOn;
#endif //comfile for PJS  
  //Com_SendSignal(COM_SIG_TX_AHB1_CF_BRK_BUZZR, &ComSignalData);


  if(Proxy_lsahbu8AHBEEPofsReadInvalid==1)
  {
  	Proxy_TxBus.Proxy_TxTxAhb1Info.CfBrkPedalCalStatus=0;
  }
  else
  {
  	Proxy_TxBus.Proxy_TxTxAhb1Info.CfBrkPedalCalStatus=1;
  }

  //Com_SendSignal(COM_SIG_TX_AHB1_CF_BRK_PEDALCALSTATUS, &ComSignalData);

  Proxy_fsu1PdtSenFaultDet = Eem_MainEemFailData.Eem_Fail_PedalPDT;; /* Proxy_TxBus.Proxy_TxPedErrInfo.PedlErr; */
  Proxy_TxBus.Proxy_TxTxAhb1Info.CfBrkAhbSnsFail = Proxy_fsu1PdtSenFaultDet;
  //Com_SendSignal(COM_SIG_TX_AHB1_CF_BRK_AHBSNSFAIL, &ComSignalData);

  /* WHL_SPD FL, FR Signal �߰� */
  Proxy_fu1WheelFLErrDet = Eem_MainEemFailData.Eem_Fail_WssFL; /* Proxy_TxBus.Proxy_TxErrWhlSpdInfo.FlWhlSpdErr; */
  if(Proxy_fu1WheelFLErrDet==1)
  {
  	Proxy_TxBus.Proxy_TxTxAhb1Info.WhlSpdFLAhb = 0x3FFF;
  }
  else
  {
  	//ComSignalData = (Wss_GetSpeedFL()/2);
    Proxy_TxBus.Proxy_TxTxAhb1Info.WhlSpdFLAhb = Proxy_TxBus.Proxy_TxWhlSpdInfo.FlWhlSpd/2;
  }

  //Com_SendSignal(COM_SIG_TX_AHB1_WHL_SPD_FL_AHB, &ComSignalData);

  Proxy_fu1WheelFRErrDet = Eem_MainEemFailData.Eem_Fail_WssFL; /* Proxy_TxBus.Proxy_TxErrWhlSpdInfo.FrWhlSpdErr; */
  if(Proxy_fu1WheelFRErrDet==1)
  {
  	Proxy_TxBus.Proxy_TxTxAhb1Info.WhlSpdFRAhb = 0x3FFF;
  }
  else
  {
  	//ComSignalData = (Wss_GetSpeedFR()/2);
    Proxy_TxBus.Proxy_TxTxAhb1Info.WhlSpdFRAhb = Proxy_TxBus.Proxy_TxWhlSpdInfo.FrWhlSpd;
  }

  //Com_SendSignal(COM_SIG_TX_AHB1_WHL_SPD_FR_AHB, &ComSignalData);
  //Com_SendSignal(COM_SIG_TX_AHB1_CR_AHB_MSGCHKSUM, &ComSignalData);
}

static void Proxy_GetPDTValue_Ofset(void)
{
  Proxy_fs16PDTSignal_1_10mm = Proxy_TxBus.Proxy_TxPdt5msRawInfo.PdtSig;
  Proxy_ws16PdtSnsrOfset = Proxy_TxBus.Proxy_TxLogicEepDataInfo.KPdtOffsEolReadVal;
  Proxy_lsahbu8AHBEEPofsReadInvalid = Proxy_TxBus.Proxy_TxLogicEepDataInfo.ReadInvld;
}

#define PROXY_STOP_SEC_CODE
#include "Proxy_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */

