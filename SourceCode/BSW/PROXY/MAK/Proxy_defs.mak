# \file
#
# \brief Proxy
#
# This file contains the implementation of the SWC
# module Proxy.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Proxy_CORE_PATH     := $(MANDO_BSW_ROOT)\Proxy
Proxy_CAL_PATH      := $(Proxy_CORE_PATH)\CAL\$(Proxy_VARIANT)
Proxy_SRC_PATH      := $(Proxy_CORE_PATH)\SRC
Proxy_CFG_PATH      := $(Proxy_CORE_PATH)\CFG\$(Proxy_VARIANT)
Proxy_HDR_PATH      := $(Proxy_CORE_PATH)\HDR
Proxy_IFA_PATH      := $(Proxy_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Proxy_CMN_PATH      := $(Proxy_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	Proxy_UT_PATH		:= $(Proxy_CORE_PATH)\UT
	Proxy_UNITY_PATH	:= $(Proxy_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(Proxy_UT_PATH)
	CC_INCLUDE_PATH		+= $(Proxy_UNITY_PATH)
	Proxy_Rx_PATH 	:= Proxy_UT_PATH\Proxy_Rx
	Proxy_RxByCom_PATH 	:= Proxy_UT_PATH\Proxy_RxByCom
	Proxy_Tx_PATH 	:= Proxy_UT_PATH\Proxy_Tx
	Proxy_TxByCom_PATH 	:= Proxy_UT_PATH\Proxy_TxByCom
endif
CC_INCLUDE_PATH    += $(Proxy_CAL_PATH)
CC_INCLUDE_PATH    += $(Proxy_SRC_PATH)
CC_INCLUDE_PATH    += $(Proxy_CFG_PATH)
CC_INCLUDE_PATH    += $(Proxy_HDR_PATH)
CC_INCLUDE_PATH    += $(Proxy_IFA_PATH)
CC_INCLUDE_PATH    += $(Proxy_CMN_PATH)

