# \file
#
# \brief Proxy
#
# This file contains the implementation of the SWC
# module Proxy.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Proxy_src

Proxy_src_FILES        += $(Proxy_SRC_PATH)\Proxy_Rx.c
Proxy_src_FILES        += $(Proxy_SRC_PATH)\Proxy_Decode.c
Proxy_src_FILES        += $(Proxy_IFA_PATH)\Proxy_Rx_Ifa.c
Proxy_src_FILES        += $(Proxy_SRC_PATH)\Proxy_RxByCom.c
Proxy_src_FILES        += $(Proxy_SRC_PATH)\Proxy_DecodeByCom.c
Proxy_src_FILES        += $(Proxy_IFA_PATH)\Proxy_RxByCom_Ifa.c
Proxy_src_FILES        += $(Proxy_SRC_PATH)\Proxy_Tx.c
Proxy_src_FILES        += $(Proxy_SRC_PATH)\Proxy_Encode.c
Proxy_src_FILES        += $(Proxy_IFA_PATH)\Proxy_Tx_Ifa.c
Proxy_src_FILES        += $(Proxy_SRC_PATH)\Proxy_TxByCom.c
Proxy_src_FILES        += $(Proxy_SRC_PATH)\Proxy_EncodeByCom.c
Proxy_src_FILES        += $(Proxy_IFA_PATH)\Proxy_TxByCom_Ifa.c
Proxy_src_FILES        += $(Proxy_CFG_PATH)\Proxy_Cfg.c
Proxy_src_FILES        += $(Proxy_CAL_PATH)\Proxy_Cal.c
Proxy_src_FILES        += $(Proxy_SRC_PATH)\Proxy_Logger.c
ifeq ($(ICE_COMPILE),true)
Proxy_src_FILES        += $(Proxy_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	Proxy_src_FILES        += $(Proxy_UNITY_PATH)\unity.c
	Proxy_src_FILES        += $(Proxy_UNITY_PATH)\unity_fixture.c	
	Proxy_src_FILES        += $(Proxy_UT_PATH)\main.c
	Proxy_src_FILES        += $(Proxy_UT_PATH)\Proxy_Rx\Proxy_Rx_UtMain.c
	Proxy_src_FILES        += $(Proxy_UT_PATH)\Proxy_RxByCom\Proxy_RxByCom_UtMain.c
	Proxy_src_FILES        += $(Proxy_UT_PATH)\Proxy_Tx\Proxy_Tx_UtMain.c
	Proxy_src_FILES        += $(Proxy_UT_PATH)\Proxy_TxByCom\Proxy_TxByCom_UtMain.c
endif