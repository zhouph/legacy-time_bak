/**
 * @defgroup Proxy_Cfg Proxy_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Proxy_Cfg.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PROXY_CFG_H_
#define PROXY_CFG_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Proxy_Types.h"
#include "Vil_Cfg.h" /*KYB*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
#define FLEXCAN_MODULE_A        0
#define FLEXCAN_MODULE_C        1
#define FLEXCAN_MODULE_ALL      3

#define U8_CC_TIME_10MS   1
#define U8_CC_TIME_20MS   2
#define U8_CC_TIME_30MS   3
#define U8_CC_TIME_40MS   4
#define U8_CC_TIME_50MS   5
#define U8_CC_TIME_70MS   7
#define U8_CC_TIME_100MS  10
#define U8_CC_TIME_150MS  15
#define U8_CC_TIME_200MS  20
#define U8_CC_TIME_300MS  30
#define U8_CC_TIME_400MS  40
#define U8_CC_TIME_500MS  50
#define U8_CC_TIME_1SEC   100
#define U16_CC_TIME_2SEC  200
#define U16_CC_TIME_5SEC  500

/* Check CAN Message ID symbolic name */
#define CHK_CAN_EMS1_MSG_RX                 (Proxy_MsgIdType) 0
#define CHK_CAN_EMS2_MSG_RX                 (Proxy_MsgIdType) 1
#define CHK_CAN_EMS6_MSG_RX                 (Proxy_MsgIdType) 2
#define CHK_CAN_TCU1_MSG_RX                 (Proxy_MsgIdType) 3
#define CHK_CAN_TCU2_MSG_RX                 (Proxy_MsgIdType) 4
#define CHK_CAN_TCU5_MSG_RX                 (Proxy_MsgIdType) 5
#define CHK_CAN_TCU6_MSG_RX                 (Proxy_MsgIdType) 6
#define CHK_CAN_HCU1_MSG_RX                 (Proxy_MsgIdType) 7
#define CHK_CAN_HCU2_MSG_RX                 (Proxy_MsgIdType) 8
#define CHK_CAN_HCU3_MSG_RX                 (Proxy_MsgIdType) 9
#define CHK_CAN_HCU5_MSG_RX                 (Proxy_MsgIdType) 10
#define CHK_CAN_MCU1_MSG_RX                 (Proxy_MsgIdType) 11
#define CHK_CAN_MCU2_MSG_RX                 (Proxy_MsgIdType) 12
#define CHK_CAN_BMS1_MSG_RX                 (Proxy_MsgIdType) 13
#define CHK_CAN_VSM2_MSG_RX                 (Proxy_MsgIdType) 14
#define CHK_CAN_SAS1_MSG_RX                 (Proxy_MsgIdType) 15
#define CHK_CAN_FATC1_MSG_RX                (Proxy_MsgIdType) 16
#define CHK_CAN_CLU1_MSG_RX                 (Proxy_MsgIdType) 17
#define CHK_CAN_CGW1_MSG_RX                 (Proxy_MsgIdType) 18
#define CHK_CAN_EMS3_MSG_RX                 (Proxy_MsgIdType) 19
#define CHK_CAN_DIAG_FUNC_MSG_RX            (Proxy_MsgIdType) 20
#define CHK_CAN_DIAG_PHY_MSG_RX             (Proxy_MsgIdType) 21
#define CHK_CAN_CLU2_MSG_RX                 (Proxy_MsgIdType) 22
#define CHK_CAN_EPB1_MSG_RX                 (Proxy_MsgIdType) 23
#define CHK_CAN_YAWACC_MSG_RX               (Proxy_MsgIdType) 24
#define CHK_CAN_LONGACC_MSG_RX              (Proxy_MsgIdType) 25
#define CHK_CAN_YAWSERIAL_MSG_RX            (Proxy_MsgIdType) 26

#define CHK_CAN_MAX_NUM                     27

#define u16g_MOTTQCMD_SIGNBIT_MASK          ( (unsigned short)( 0x0200) )
#define u16g_SIGNBIT_MASK                   ( (unsigned short)( 0x8000) )
#define u16g_MOTESTTQ_SIGNBIT_MASK          ( (unsigned short)( 0x0400) )
#define U8_CC_TIME_500MS_5msLoop  100
#define U8_CC_TIME_1SEC_5msLoop   200
#define U16_CC_TIME_2SEC_5msLoop  400

#define     __U8_SCC_EMS1EMS2MSGINVALID_THRES   50  /*500ms*/
#define     __U8_SCC_TCU1MSGINVALID_THRES       50  /*500ms*/

#define DIRECTION_FORWARD_TYPE              (Proxy_DirectionType)FORWARD
#define DIRECTION_BACKWARD_TYPE             (Proxy_DirectionType)BACKWARD


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
 /* Proxy config type */
 extern Proxy_ConfigType Proxy_Config;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PROXY_CFG_H_ */
/** @} */
