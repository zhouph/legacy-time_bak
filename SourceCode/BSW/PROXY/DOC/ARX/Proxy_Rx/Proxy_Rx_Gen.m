Proxy_RxRxBms1Info = Simulink.Bus;
DeList={
    'Bms1SocPc'
    };
Proxy_RxRxBms1Info = CreateBus(Proxy_RxRxBms1Info, DeList);
clear DeList;

Proxy_RxRxYawSerialInfo = Simulink.Bus;
DeList={
    'YawSerialNum_0'
    'YawSerialNum_1'
    'YawSerialNum_2'
    };
Proxy_RxRxYawSerialInfo = CreateBus(Proxy_RxRxYawSerialInfo, DeList);
clear DeList;

Proxy_RxRxYawAccInfo = Simulink.Bus;
DeList={
    'YawRateValidData'
    'YawRateSelfTestStatus'
    'YawRateSignal_0'
    'YawRateSignal_1'
    'SensorOscFreqDev'
    'Gyro_Fail'
    'Raster_Fail'
    'Eep_Fail'
    'Batt_Range_Err'
    'Asic_Fail'
    'Accel_Fail'
    'Ram_Fail'
    'Rom_Fail'
    'Ad_Fail'
    'Osc_Fail'
    'Watchdog_Rst'
    'Plaus_Err_Pst'
    'RollingCounter'
    'Can_Func_Err'
    'AccelEratorRateSig_0'
    'AccelEratorRateSig_1'
    'LatAccValidData'
    'LatAccSelfTestStatus'
    };
Proxy_RxRxYawAccInfo = CreateBus(Proxy_RxRxYawAccInfo, DeList);
clear DeList;

Proxy_RxRxTcu6Info = Simulink.Bus;
DeList={
    'ShiftClass_Ccan'
    };
Proxy_RxRxTcu6Info = CreateBus(Proxy_RxRxTcu6Info, DeList);
clear DeList;

Proxy_RxRxTcu5Info = Simulink.Bus;
DeList={
    'Typ'
    'GearTyp'
    };
Proxy_RxRxTcu5Info = CreateBus(Proxy_RxRxTcu5Info, DeList);
clear DeList;

Proxy_RxRxTcu1Info = Simulink.Bus;
DeList={
    'Targe'
    'GarChange'
    'Flt'
    'GarSelDisp'
    'TQRedReq_PC'
    'TQRedReqSlw_PC'
    'TQIncReq_PC'
    };
Proxy_RxRxTcu1Info = CreateBus(Proxy_RxRxTcu1Info, DeList);
clear DeList;

Proxy_RxRxSasInfo = Simulink.Bus;
DeList={
    'Angle'
    'Speed'
    'Ok'
    'Cal'
    'Trim'
    'CheckSum'
    'MsgCount'
    };
Proxy_RxRxSasInfo = CreateBus(Proxy_RxRxSasInfo, DeList);
clear DeList;

Proxy_RxRxMcu2Info = Simulink.Bus;
DeList={
    'Flt'
    };
Proxy_RxRxMcu2Info = CreateBus(Proxy_RxRxMcu2Info, DeList);
clear DeList;

Proxy_RxRxMcu1Info = Simulink.Bus;
DeList={
    'MoTestTQ_PC'
    'MotActRotSpd_RPM'
    };
Proxy_RxRxMcu1Info = CreateBus(Proxy_RxRxMcu1Info, DeList);
clear DeList;

Proxy_RxRxLongAccInfo = Simulink.Bus;
DeList={
    'IntSenFltSymtmActive'
    'IntSenFaultPresent'
    'LongAccSenCirErrPre'
    'LonACSenRanChkErrPre'
    'LongRollingCounter'
    'IntTempSensorFault'
    'LongAccInvalidData'
    'LongAccSelfTstStatus'
    'LongAccRateSignal_0'
    'LongAccRateSignal_1'
    };
Proxy_RxRxLongAccInfo = CreateBus(Proxy_RxRxLongAccInfo, DeList);
clear DeList;

Proxy_RxRxHcu5Info = Simulink.Bus;
DeList={
    'HevMod'
    };
Proxy_RxRxHcu5Info = CreateBus(Proxy_RxRxHcu5Info, DeList);
clear DeList;

Proxy_RxRxHcu3Info = Simulink.Bus;
DeList={
    'TmIntQcMDBINV_PC'
    'MotTQCMC_PC'
    'MotTQCMDBINV_PC'
    };
Proxy_RxRxHcu3Info = CreateBus(Proxy_RxRxHcu3Info, DeList);
clear DeList;

Proxy_RxRxHcu2Info = Simulink.Bus;
DeList={
    'ServiceMod'
    'RegenENA'
    'RegenBRKTQ_NM'
    'CrpTQ_NM'
    'WhlDEMTQ_NM'
    };
Proxy_RxRxHcu2Info = CreateBus(Proxy_RxRxHcu2Info, DeList);
clear DeList;

Proxy_RxRxHcu1Info = Simulink.Bus;
DeList={
    'EngCltStat'
    'HEVRDY'
    'EngTQCmdBinV_PC'
    'EngTQCmd_PC'
    };
Proxy_RxRxHcu1Info = CreateBus(Proxy_RxRxHcu1Info, DeList);
clear DeList;

Proxy_RxRxFact1Info = Simulink.Bus;
DeList={
    'OutTemp_SNR_C'
    };
Proxy_RxRxFact1Info = CreateBus(Proxy_RxRxFact1Info, DeList);
clear DeList;

Proxy_RxRxEms3Info = Simulink.Bus;
DeList={
    'EngColTemp_C'
    };
Proxy_RxRxEms3Info = CreateBus(Proxy_RxRxEms3Info, DeList);
clear DeList;

Proxy_RxRxEms2Info = Simulink.Bus;
DeList={
    'EngSpdErr'
    'AccPedDep_PC'
    'Tps_PC'
    };
Proxy_RxRxEms2Info = CreateBus(Proxy_RxRxEms2Info, DeList);
clear DeList;

Proxy_RxRxEms1Info = Simulink.Bus;
DeList={
    'TqStd_NM'
    'ActINDTQ_PC'
    'EngSpd_RPM'
    'IndTQ_PC'
    'FrictTQ_PC'
    };
Proxy_RxRxEms1Info = CreateBus(Proxy_RxRxEms1Info, DeList);
clear DeList;

Proxy_RxRxClu2Info = Simulink.Bus;
DeList={
    'IGN_SW'
    };
Proxy_RxRxClu2Info = CreateBus(Proxy_RxRxClu2Info, DeList);
clear DeList;

Proxy_RxRxClu1Info = Simulink.Bus;
DeList={
    'P_Brake_Act'
    'Cf_Clu_BrakeFluIDSW'
    };
Proxy_RxRxClu1Info = CreateBus(Proxy_RxRxClu1Info, DeList);
clear DeList;

Proxy_RxRxMsgOkFlgInfo = Simulink.Bus;
DeList={
    'Bms1MsgOkFlg'
    'YawSerialMsgOkFlg'
    'YawAccMsgOkFlg'
    'Tcu6MsgOkFlg'
    'Tcu5MsgOkFlg'
    'Tcu1MsgOkFlg'
    'SasMsgOkFlg'
    'Mcu2MsgOkFlg'
    'Mcu1MsgOkFlg'
    'LongAccMsgOkFlg'
    'Hcu5MsgOkFlg'
    'Hcu3MsgOkFlg'
    'Hcu2MsgOkFlg'
    'Hcu1MsgOkFlg'
    'Fatc1MsgOkFlg'
    'Ems3MsgOkFlg'
    'Ems2MsgOkFlg'
    'Ems1MsgOkFlg'
    'Clu2MsgOkFlg'
    'Clu1MsgOkFlg'
    };
Proxy_RxRxMsgOkFlgInfo = CreateBus(Proxy_RxRxMsgOkFlgInfo, DeList);
clear DeList;

Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo = Simulink.Bus;
DeList={
    'ST_ECU_ST_REPAT_XTRQ_FTAX_BAX'
    'QU_SER_REPAT_XTRQ_FTAX_BAX_ACT'
    'AVL_REPAT_XTRQ_FTAX_BAX'
    'QU_AVL_REPAT_XTRQ_FTAX_BAX'
    };
Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo = CreateBus(Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo, DeList);
clear DeList;

Proxy_RxAVL_LTRQD_BAXInfo = Simulink.Bus;
DeList={
    'QU_SER_LTRQD_BAX'
    'QU_AVL_LTRQD_BAX'
    'AVL_LTRQD_BAX'
    };
Proxy_RxAVL_LTRQD_BAXInfo = CreateBus(Proxy_RxAVL_LTRQD_BAXInfo, DeList);
clear DeList;

Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo = Simulink.Bus;
DeList={
    'AVL_TORQ_CRSH_DMEE'
    'AVL_TORQ_CRSH'
    'ST_SAIL_DRV_2'
    'QU_AVL_RPM_ENG_CRSH'
    'AVL_RPM_ENG_CRSH'
    'AVL_ANG_ACPD'
    'QU_AVL_ANG_ACPD'
    'ST_ECU_ANG_ACPD'
    'AVL_ANG_ACPD_VIRT'
    'ECO_ANG_ACPD'
    'GRAD_AVL_ANG_ACPD'
    'ST_INTF_DRASY'
    };
Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo = CreateBus(Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo, DeList);
clear DeList;

Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info = Simulink.Bus;
DeList={
    'AVL_RPM_BAX_RED'
    'ST_PENG_PT'
    'ST_AVAI_INTV_PT_DRS'
    'QU_AVL_RPM_BAX_RED'
    'QU_SER_WMOM_PT_SUM_DRS'
    'QU_SER_WMOM_PT_SUM_STAB'
    'ST_ECU_WMOM_DRV_5'
    'TAR_WMOM_PT_SUM_COOTD'
    'QU_SER_COOR_TORQ_BDRV'
    'ST_ECU_WMOM_DRV_4'
    'ST_DRVDIR_DVCH'
    };
Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info = CreateBus(Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info, DeList);
clear DeList;

Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info = Simulink.Bus;
DeList={
    'QU_AVL_WMOM_PT_SUM'
    'AVL_WMOM_PT_SUM_ERR_AMP'
    'REIN_PT'
    'AVL_WMOM_PT_SUM'
    'ST_ECU_WMOM_DRV_1'
    'AVL_WMOM_PT_SUM_MAX'
    'AVL_WMOM_PT_SUM_FAST_TOP'
    'AVL_WMOM_PT_SUM_FAST_BOT'
    'ST_ECU_WMOM_DRV_2'
    'QU_REIN_PT'
    };
Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info = CreateBus(Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info, DeList);
clear DeList;

Proxy_RxHGLV_VEH_FILTInfo = Simulink.Bus;
DeList={
    'HGLV_VEH_FILT_FRH'
    'HGLV_VEH_FILT_FLH'
    'QU_HGLV_VEH_FILT_FRH'
    'QU_HGLV_VEH_FILT_FLH'
    'QU_HGLV_VEH_FILT_RRH'
    'HGLV_VEH_FILT_RLH'
    'HGLV_VEH_FILT_RRH'
    'QU_HGLV_VEH_FILT_RLH'
    };
Proxy_RxHGLV_VEH_FILTInfo = CreateBus(Proxy_RxHGLV_VEH_FILTInfo, DeList);
clear DeList;

Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo = Simulink.Bus;
DeList={
    'ATTAV_ESTI'
    'ATTA_ESTI_ERR_AMP'
    'VY_ESTI_ERR_AMP'
    'VY_ESTI'
    'ATTAV_ESTI_ERR_AMP'
    'QU_VEH_DYNMC_DT_ESTI'
    'ATTA_ESTI'
    };
Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo = CreateBus(Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo, DeList);
clear DeList;

Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo = Simulink.Bus;
DeList={
    'QU_ACLNY_COG'
    'ACLNY_COG_ERR_AMP'
    'ACLNX_COG'
    'ACLNY_COG'
    'ACLNX_COG_ERR_AMP'
    'QU_ACLNX_COG'
    };
Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo = CreateBus(Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo, DeList);
clear DeList;

Proxy_RxV_VEH_V_VEH_2Info = Simulink.Bus;
DeList={
    'QU_V_VEH_COG'
    'DVCO_VEH'
    'ST_V_VEH_NSS'
    'V_VEH_COG'
    };
Proxy_RxV_VEH_V_VEH_2Info = CreateBus(Proxy_RxV_VEH_V_VEH_2Info, DeList);
clear DeList;

Proxy_RxDT_DRDYSEN_EXT_VYAW_VEHInfo = Simulink.Bus;
DeList={
    'VYAW_VEH_ERR_AMP'
    'QU_VYAW_VEH'
    'VYAW_VEH'
    };
Proxy_RxDT_DRDYSEN_EXT_VYAW_VEHInfo = CreateBus(Proxy_RxDT_DRDYSEN_EXT_VYAW_VEHInfo, DeList);
clear DeList;

Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo = Simulink.Bus;
DeList={
    'QU_AVL_TRGR_RW'
    'AVL_TRGR_RW'
    'AVL_LOGR_RW_FAST'
    'QU_AVL_LOGR_RW'
    'AVL_LOGR_RW'
    };
Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo = CreateBus(Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo, DeList);
clear DeList;

Proxy_RxAVL_STEA_FTAXInfo = Simulink.Bus;
DeList={
    'QU_AVL_STEA_FTAX_PNI'
    'AVL_STEA_FTAX_PNI'
    'QU_AVL_STEA_FTAX_WHL'
    'AVL_STEA_FTAX_WHL'
    'AVL_STEA_FTAX_WHL_ERR_AMP'
    };
Proxy_RxAVL_STEA_FTAXInfo = CreateBus(Proxy_RxAVL_STEA_FTAXInfo, DeList);
clear DeList;

Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo = Simulink.Bus;
DeList={
    'TAR_PRMSN_DBC_DCRN_MAX'
    'QU_TAR_PRF_BRK'
    'QU_TAR_V_HDC'
    'TAR_PRMSN_DBC_DCRN_GRAD_MAX'
    'TAR_PRMSN_DBC_TR_THRV'
    'TAR_BRTORQ_SUM'
    'QU_TAR_BRTORQ_SUM'
    'QU_TAR_PRMSN_DBC'
    'RQ_TAO_SSM'
    };
Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo = CreateBus(Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo, DeList);
clear DeList;

Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info = Simulink.Bus;
DeList={
    'AVL_WMOM_PT_SUM_DTORQ_BOT'
    'AVL_WMOM_PT_SUM_RECUP_MAX'
    'AVL_WMOM_PT_SUM_ILS'
    'AVL_WMOM_PT_SUM_DTORQ_TOP'
    'ST_ECU_WMOM_DRV_6'
    };
Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info = CreateBus(Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info, DeList);
clear DeList;

Proxy_RxWMOM_DRV_7Info = Simulink.Bus;
DeList={
    'ST_EL_DRVG'
    'PRD_AVL_WMOM_PT_SUM_RECUP_MAX'
    'ST_ECU_WMOM_DRV_7'
    'QU_SER_WMOM_PT_SUM_RECUP'
    };
Proxy_RxWMOM_DRV_7Info = CreateBus(Proxy_RxWMOM_DRV_7Info, DeList);
clear DeList;

Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo = Simulink.Bus;
DeList={
    'TAR_DIFF_BRTORQ_BAX_YMR'
    'FACT_TAR_COMPT_DRV_YMR'
    'QU_TAR_DIFF_BRTORQ_YMR'
    'TAR_DIFF_BRTORQ_FTAX_YMR'
    };
Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo = CreateBus(Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo, DeList);
clear DeList;

Proxy_RxDT_BRKSYS_ENGMGInfo = Simulink.Bus;
DeList={
    'QU_AVL_LOWP_BRKFA'
    'AVL_LOWP_BRKFA'
    };
Proxy_RxDT_BRKSYS_ENGMGInfo = CreateBus(Proxy_RxDT_BRKSYS_ENGMGInfo, DeList);
clear DeList;

Proxy_RxERRM_BN_UInfo = Simulink.Bus;
DeList={
    'CTR_ERRM_BN_U'
    };
Proxy_RxERRM_BN_UInfo = CreateBus(Proxy_RxERRM_BN_UInfo, DeList);
clear DeList;

Proxy_RxCTR_CRInfo = Simulink.Bus;
DeList={
    'ST_EXCE_ACLN_THRV'
    'CTR_PHTR_CR'
    'CTR_ITLI_CR'
    'CTR_CLSY_CR'
    'CTR_AUTOM_ECAL_CR'
    'CTR_SWO_EKP_CR'
    'CTR_PCSH_MST'
    'CTR_HAZW_CR'
    };
Proxy_RxCTR_CRInfo = CreateBus(Proxy_RxCTR_CRInfo, DeList);
clear DeList;

Proxy_RxKLEMMENInfo = Simulink.Bus;
DeList={
    'ST_OP_MSA'
    'RQ_DRVG_RDI'
    'ST_KL_DBG'
    'ST_KL_50_MSA'
    'ST_SSP'
    'RWDT_BLS'
    'ST_STCD_PENG'
    'ST_KL'
    'ST_KL_DIV'
    'ST_VEH_CON'
    'ST_KL_30B'
    'CON_CLT_SW'
    'CTR_ENG_STOP'
    'ST_PLK'
    'ST_KL_15N'
    'ST_KL_KEY_VLD'
    };
Proxy_RxKLEMMENInfo = CreateBus(Proxy_RxKLEMMENInfo, DeList);
clear DeList;

Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info = Simulink.Bus;
DeList={
    'RDC_MES_TSTMP'
    'PCKG_ID'
    'SUPP_ID'
    'RDC_DT_5'
    'RDC_DT_8'
    'RDC_DT_7'
    'RDC_DT_6'
    'RDC_DT_1'
    'TYR_ID'
    'RDC_DT_4'
    'RDC_DT_3'
    'RDC_DT_2'
    };
Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info = CreateBus(Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info, DeList);
clear DeList;

Proxy_RxBEDIENUNG_WISCHERInfo = Simulink.Bus;
DeList={
    'OP_WISW'
    'OP_WIPO'
    };
Proxy_RxBEDIENUNG_WISCHERInfo = CreateBus(Proxy_RxBEDIENUNG_WISCHERInfo, DeList);
clear DeList;

Proxy_RxDT_PT_2Info = Simulink.Bus;
DeList={
    'ST_GRSEL_DRV'
    'TEMP_EOI_DRV'
    'RLS_ENGSTA'
    'RPM_ENG_MAX_ALW'
    'RDUC_DOCTR_RPM_DRV_2'
    'TEMP_ENG_DRV'
    'ST_DRV_VEH'
    'ST_ECU_DT_PT_2'
    'ST_IDLG_ENG_DRV'
    'ST_ILK_STRT_DRV'
    'ST_SW_CLT_DRV'
    'ST_ENG_RUN_DRV'
    };
Proxy_RxDT_PT_2Info = CreateBus(Proxy_RxDT_PT_2Info, DeList);
clear DeList;

Proxy_RxSTAT_ENG_STA_AUTOInfo = Simulink.Bus;
DeList={
    'RQ_MSA_ENG_STA_1'
    'RQ_MSA_ENG_STA_2'
    'PSBTY_MSA_ENG_STOP_STA'
    'ST_SHFT_MSA_ENGSTP'
    'RQ_SLIP_K0'
    'ST_TAR_PENG_CENG'
    'SPEC_TYP_ENGSTA'
    'ST_RPM_CLCTR_MOT'
    'ST_FN_MSA'
    'ST_AVL_PENG_CENG_ENGMG'
    'DISP_REAS_PREV_SWO_CENG'
    'VARI_TYP_ENGSTA'
    'ST_TAR_CENG'
    'ST_AVAI_SAIL_DME'
    };
Proxy_RxSTAT_ENG_STA_AUTOInfo = CreateBus(Proxy_RxSTAT_ENG_STA_AUTOInfo, DeList);
clear DeList;

Proxy_RxST_ENERG_GENInfo = Simulink.Bus;
DeList={
    'ST_LDST_GEN_DRV'
    'DT_PCU_SCP'
    'ST_GEN_DRV'
    'AVL_I_GEN_DRV'
    'LDST_GEN_DRV'
    'ST_LDREL_GEN'
    'ST_CHG_STOR'
    'TEMP_BT_14V'
    'ST_I_IBS'
    'ST_SEP_STOR'
    'ST_BN2_SCP'
    };
Proxy_RxST_ENERG_GENInfo = CreateBus(Proxy_RxST_ENERG_GENInfo, DeList);
clear DeList;

Proxy_RxDT_PT_1Info = Simulink.Bus;
DeList={
    'RQ_SHPA_GRB_REGE_PAFI'
    'ST_RTIR_DRV'
    'CTR_SLCK_DRV'
    'RQ_STASS_ENGMG'
    'ST_CAT_HT'
    'RQ_SHPA_GRB_CHGBLC'
    'TAR_RPM_IDLG_DRV_EXT'
    'SLCTN_BUS_COMM_ENG_GRB'
    'TAR_RPM_IDLG_DRV'
    'ST_INFS_DRV'
    'ST_SW_WAUP_DRV'
    'AIP_ENG_DRV'
    'RDUC_DOCTR_RPM_DRV'
    };
Proxy_RxDT_PT_1Info = CreateBus(Proxy_RxDT_PT_1Info, DeList);
clear DeList;

Proxy_RxDT_GRDT_DRVInfo = Simulink.Bus;
DeList={
    'ST_OPMO_GRDT_DRV'
    'ST_RSTA_GRDT'
    };
Proxy_RxDT_GRDT_DRVInfo = CreateBus(Proxy_RxDT_GRDT_DRVInfo, DeList);
clear DeList;

Proxy_RxDIAG_OBD_ENGMG_ELInfo = Simulink.Bus;
DeList={
    'RQ_MIL_DIAG_OBD_ENGMG_EL'
    'RQ_RST_OBD_DIAG'
    };
Proxy_RxDIAG_OBD_ENGMG_ELInfo = CreateBus(Proxy_RxDIAG_OBD_ENGMG_ELInfo, DeList);
clear DeList;

Proxy_RxSTAT_CT_HABRInfo = Simulink.Bus;
DeList={
    'ST_CT_HABR'
    };
Proxy_RxSTAT_CT_HABRInfo = CreateBus(Proxy_RxSTAT_CT_HABRInfo, DeList);
clear DeList;

Proxy_RxDT_PT_3Info = Simulink.Bus;
DeList={
    'TRNRAO_BAX'
    'QU_TRNRAO_BAX'
    };
Proxy_RxDT_PT_3Info = CreateBus(Proxy_RxDT_PT_3Info, DeList);
clear DeList;

Proxy_RxEINHEITEN_BN2020Info = Simulink.Bus;
DeList={
    'UN_TORQ_S_MOD'
    'UN_PWR_S_MOD'
    'UN_DATE_EXT'
    'UN_COSP_EL'
    'UN_TEMP'
    'UN_AIP'
    'LANG'
    'UN_DATE'
    'UN_T'
    'UN_SPDM_DGTL'
    'UN_MILE'
    'UN_FU'
    'UN_COSP'
    };
Proxy_RxEINHEITEN_BN2020Info = CreateBus(Proxy_RxEINHEITEN_BN2020Info, DeList);
clear DeList;

Proxy_RxA_TEMPInfo = Simulink.Bus;
DeList={
    'TEMP_EX_UNFILT'
    'TEMP_EX'
    };
Proxy_RxA_TEMPInfo = CreateBus(Proxy_RxA_TEMPInfo, DeList);
clear DeList;

Proxy_RxWISCHERGESCHWINDIGKEITInfo = Simulink.Bus;
DeList={
    'ST_RNSE'
    'INT_RN'
    'V_WI'
    };
Proxy_RxWISCHERGESCHWINDIGKEITInfo = CreateBus(Proxy_RxWISCHERGESCHWINDIGKEITInfo, DeList);
clear DeList;

Proxy_RxSTAT_DS_VRFDInfo = Simulink.Bus;
DeList={
    'ST_DSW_DRD_SFY_CTRL'
    'ST_DSW_DVDR_VRFD'
    'ST_DSW_DVDR_SFY_CTRL'
    'ST_DSW_PSDR_SFY_CTRL'
    'ST_DSW_PSD_SFY_CTRL'
    'ST_DSW_DRD_VRFD'
    'ST_DSW_PSDR_VRFD'
    'ST_DSW_PSD_VRFD'
    };
Proxy_RxSTAT_DS_VRFDInfo = CreateBus(Proxy_RxSTAT_DS_VRFDInfo, DeList);
clear DeList;

Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info = Simulink.Bus;
DeList={
    'AVL_MOD_SW_DRDY'
    'RQ_SW_DRDY_MMID'
    'RQ_SU_SW_DRDY'
    'RQ_SW_DRDY_KDIS'
    'AVL_MOD_SW_DRDY_CHAS'
    'AVL_MOD_SW_DRDY_DRV'
    'AVL_MOD_SW_DRDY_STAB'
    'DISP_ST_DSC'
    };
Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info = CreateBus(Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info, DeList);
clear DeList;

Proxy_RxSTAT_ANHAENGERInfo = Simulink.Bus;
DeList={
    'ST_SYNCN_HAZWCL_TRAI'
    'ST_RFLI_DF_TRAI'
    'ST_TRAI'
    'ST_DI_DF_TRAI'
    'ST_PO_AHV'
    };
Proxy_RxSTAT_ANHAENGERInfo = CreateBus(Proxy_RxSTAT_ANHAENGERInfo, DeList);
clear DeList;

Proxy_RxFZZSTDInfo = Simulink.Bus;
DeList={
    'ST_ILK_ERRM_FZM'
    'ST_ENERG_FZM'
    'ST_BT_PROTE_WUP'
    };
Proxy_RxFZZSTDInfo = CreateBus(Proxy_RxFZZSTDInfo, DeList);
clear DeList;

Proxy_RxBEDIENUNG_FAHRWERKInfo = Simulink.Bus;
DeList={
    'OP_MOD_TRCT_DSC'
    'OP_TPCT'
    };
Proxy_RxBEDIENUNG_FAHRWERKInfo = CreateBus(Proxy_RxBEDIENUNG_FAHRWERKInfo, DeList);
clear DeList;

Proxy_RxFAHRZEUGTYPInfo = Simulink.Bus;
DeList={
    'QUAN_CYL'
    'QUAN_GR'
    'TYP_VEH'
    'TYP_BODY'
    'TYP_ENG'
    'CLAS_PWR'
    'TYP_CNT'
    'TYP_STE'
    'TYP_GRB'
    'TYP_CAPA'
    };
Proxy_RxFAHRZEUGTYPInfo = CreateBus(Proxy_RxFAHRZEUGTYPInfo, DeList);
clear DeList;

Proxy_RxST_BLT_CT_SOCCUInfo = Simulink.Bus;
DeList={
    'ST_SEAT_OCCU_RRH'
    'ST_BLTB_SW_RM'
    'ST_BLTB_SW_RRH'
    'ST_ERR_SEAT_MT_DR'
    'ST_BLTB_SW_DR'
    'ST_SEAT_OCCU_DR'
    'ST_BLTB_SW_RLH'
    'ST_SEAT_OCCU_RLH'
    'ST_BLTB_SW_PS'
    'ST_SEAT_OCCU_PS'
    };
Proxy_RxST_BLT_CT_SOCCUInfo = CreateBus(Proxy_RxST_BLT_CT_SOCCUInfo, DeList);
clear DeList;

Proxy_RxFAHRGESTELLNUMMERInfo = Simulink.Bus;
DeList={
    'NO_VECH_1'
    'NO_VECH_2'
    'NO_VECH_3'
    'NO_VECH_6'
    'NO_VECH_7'
    'NO_VECH_4'
    'NO_VECH_5'
    };
Proxy_RxFAHRGESTELLNUMMERInfo = CreateBus(Proxy_RxFAHRGESTELLNUMMERInfo, DeList);
clear DeList;

Proxy_RxRELATIVZEITInfo = Simulink.Bus;
DeList={
    'T_SEC_COU_REL'
    'T_DAY_COU_ABSL'
    };
Proxy_RxRELATIVZEITInfo = CreateBus(Proxy_RxRELATIVZEITInfo, DeList);
clear DeList;

Proxy_RxKILOMETERSTANDInfo = Simulink.Bus;
DeList={
    'RNG'
    'ST_FLLV_FUTA_SPAR'
    'FLLV_FUTA_RH'
    'FLLV_FUTA_LH'
    'FLLV_FUTA'
    'MILE_KM'
    };
Proxy_RxKILOMETERSTANDInfo = CreateBus(Proxy_RxKILOMETERSTANDInfo, DeList);
clear DeList;

Proxy_RxUHRZEIT_DATUMInfo = Simulink.Bus;
DeList={
    'DISP_DATE_WDAY'
    'DISP_DATE_DAY'
    'DISP_DATE_MON'
    'ST_DISP_CTI_DATE'
    'DISP_DATE_YR'
    'DISP_HR'
    'DISP_SEC'
    'DISP_MN'
    };
Proxy_RxUHRZEIT_DATUMInfo = CreateBus(Proxy_RxUHRZEIT_DATUMInfo, DeList);
clear DeList;

Proxy_RxEcuModeSts = Simulink.Bus;
DeList={'Proxy_RxEcuModeSts'};
Proxy_RxEcuModeSts = CreateBus(Proxy_RxEcuModeSts, DeList);
clear DeList;

Proxy_RxPCtrlAct = Simulink.Bus;
DeList={'Proxy_RxPCtrlAct'};
Proxy_RxPCtrlAct = CreateBus(Proxy_RxPCtrlAct, DeList);
clear DeList;

Proxy_RxFuncInhibitProxySts = Simulink.Bus;
DeList={'Proxy_RxFuncInhibitProxySts'};
Proxy_RxFuncInhibitProxySts = CreateBus(Proxy_RxFuncInhibitProxySts, DeList);
clear DeList;

Proxy_RxCanRxRegenInfo = Simulink.Bus;
DeList={
    'HcuRegenEna'
    'HcuRegenBrkTq'
    };
Proxy_RxCanRxRegenInfo = CreateBus(Proxy_RxCanRxRegenInfo, DeList);
clear DeList;

Proxy_RxCanRxAccelPedlInfo = Simulink.Bus;
DeList={
    'AccelPedlVal'
    'AccelPedlValErr'
    };
Proxy_RxCanRxAccelPedlInfo = CreateBus(Proxy_RxCanRxAccelPedlInfo, DeList);
clear DeList;

Proxy_RxCanRxIdbInfo = Simulink.Bus;
DeList={
    'EngMsgFault'
    'TarGearPosi'
    'GearSelDisp'
    'TcuSwiGs'
    'HcuServiceMod'
    'SubCanBusSigFault'
    'CoolantTemp'
    'CoolantTempErr'
    };
Proxy_RxCanRxIdbInfo = CreateBus(Proxy_RxCanRxIdbInfo, DeList);
clear DeList;

Proxy_RxCanRxEngTempInfo = Simulink.Bus;
DeList={
    'EngTemp'
    'EngTempErr'
    };
Proxy_RxCanRxEngTempInfo = CreateBus(Proxy_RxCanRxEngTempInfo, DeList);
clear DeList;

Proxy_RxCanRxEscInfo = Simulink.Bus;
DeList={
    'Ax'
    'YawRate'
    'SteeringAngle'
    'Ay'
    'PbSwt'
    'ClutchSwt'
    'GearRSwt'
    'EngActIndTq'
    'EngRpm'
    'EngIndTq'
    'EngFrictionLossTq'
    'EngStdTq'
    'TurbineRpm'
    'ThrottleAngle'
    'TpsResol1000'
    'PvAvCanResol1000'
    'EngChr'
    'EngVol'
    'GearType'
    'EngClutchState'
    'EngTqCmdBeforeIntv'
    'MotEstTq'
    'MotTqCmdBeforeIntv'
    'TqIntvTCU'
    'TqIntvSlowTCU'
    'TqIncReq'
    'DecelReq'
    'RainSnsStat'
    'EngSpdErr'
    'AtType'
    'MtType'
    'CvtType'
    'DctType'
    'HevAtTcu'
    'TurbineRpmErr'
    'ThrottleAngleErr'
    'TopTrvlCltchSwtAct'
    'TopTrvlCltchSwtActV'
    'HillDesCtrlMdSwtAct'
    'HillDesCtrlMdSwtActV'
    'WiperIntSW'
    'WiperLow'
    'WiperHigh'
    'WiperValid'
    'WiperAuto'
    'TcuFaultSts'
    };
Proxy_RxCanRxEscInfo = CreateBus(Proxy_RxCanRxEscInfo, DeList);
clear DeList;

Proxy_RxCanRxClu2Info = Simulink.Bus;
DeList={
    'IgnRun'
    };
Proxy_RxCanRxClu2Info = CreateBus(Proxy_RxCanRxClu2Info, DeList);
clear DeList;

Proxy_RxCanRxEemInfo = Simulink.Bus;
DeList={
    'YawRateInvld'
    'AyInvld'
    'SteeringAngleRxOk'
    'AxInvldData'
    'Ems1RxErr'
    'Ems2RxErr'
    'Tcu1RxErr'
    'Tcu5RxErr'
    'Hcu1RxErr'
    'Hcu2RxErr'
    'Hcu3RxErr'
    };
Proxy_RxCanRxEemInfo = CreateBus(Proxy_RxCanRxEemInfo, DeList);
clear DeList;

Proxy_RxCanRxInfo = Simulink.Bus;
DeList={
    'MainBusOffFlag'
    'SenBusOffFlag'
    };
Proxy_RxCanRxInfo = CreateBus(Proxy_RxCanRxInfo, DeList);
clear DeList;

Proxy_RxIMUCalcInfo = Simulink.Bus;
DeList={
    'Reverse_Gear_flg'
    'Reverse_Judge_Time'
    };
Proxy_RxIMUCalcInfo = CreateBus(Proxy_RxIMUCalcInfo, DeList);
clear DeList;

Proxy_RxCanRxGearSelDispErrInfo = Simulink.Bus;
DeList={'Proxy_RxCanRxGearSelDispErrInfo'};
Proxy_RxCanRxGearSelDispErrInfo = CreateBus(Proxy_RxCanRxGearSelDispErrInfo, DeList);
clear DeList;

