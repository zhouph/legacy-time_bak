Proxy_TxByComTxYawCbitInfo = Simulink.Bus;
DeList={
    'TestYawRateSensor'
    'TestAcclSensor'
    'YawSerialNumberReq'
    };
Proxy_TxByComTxYawCbitInfo = CreateBus(Proxy_TxByComTxYawCbitInfo, DeList);
clear DeList;

Proxy_TxByComTxWhlSpdInfo = Simulink.Bus;
DeList={
    'WhlSpdFL'
    'WhlSpdFR'
    'WhlSpdRL'
    'WhlSpdRR'
    };
Proxy_TxByComTxWhlSpdInfo = CreateBus(Proxy_TxByComTxWhlSpdInfo, DeList);
clear DeList;

Proxy_TxByComTxWhlPulInfo = Simulink.Bus;
DeList={
    'WhlPulFL'
    'WhlPulFR'
    'WhlPulRL'
    'WhlPulRR'
    };
Proxy_TxByComTxWhlPulInfo = CreateBus(Proxy_TxByComTxWhlPulInfo, DeList);
clear DeList;

Proxy_TxByComTxTcs5Info = Simulink.Bus;
DeList={
    'CfBrkAbsWLMP'
    'CfBrkEbdWLMP'
    'CfBrkTcsWLMP'
    'CfBrkTcsFLMP'
    'CfBrkAbsDIAG'
    'CrBrkWheelFL_KMH'
    'CrBrkWheelFR_KMH'
    'CrBrkWheelRL_KMH'
    'CrBrkWheelRR_KMH'
    };
Proxy_TxByComTxTcs5Info = CreateBus(Proxy_TxByComTxTcs5Info, DeList);
clear DeList;

Proxy_TxByComTxTcs1Info = Simulink.Bus;
DeList={
    'CfBrkTcsREQ'
    'CfBrkTcsPAS'
    'CfBrkTcsDEF'
    'CfBrkTcsCTL'
    'CfBrkAbsACT'
    'CfBrkAbsDEF'
    'CfBrkEbdDEF'
    'CfBrkTcsGSC'
    'CfBrkEspPAS'
    'CfBrkEspDEF'
    'CfBrkEspCTL'
    'CfBrkMsrREQ'
    'CfBrkTcsMFRN'
    'CfBrkMinGEAR'
    'CfBrkMaxGEAR'
    'BrakeLight'
    'HacCtl'
    'HacPas'
    'HacDef'
    'CrBrkTQI_PC'
    'CrBrkTQIMSR_PC'
    'CrBrkTQISLW_PC'
    'CrEbsMSGCHKSUM'
    };
Proxy_TxByComTxTcs1Info = CreateBus(Proxy_TxByComTxTcs1Info, DeList);
clear DeList;

Proxy_TxByComTxSasCalInfo = Simulink.Bus;
DeList={
    'CalSas_CCW'
    'CalSas_Lws_CID'
    };
Proxy_TxByComTxSasCalInfo = CreateBus(Proxy_TxByComTxSasCalInfo, DeList);
clear DeList;

Proxy_TxByComTxEsp2Info = Simulink.Bus;
DeList={
    'LatAccel'
    'LatAccelStat'
    'LatAccelDiag'
    'LongAccel'
    'LongAccelStat'
    'LongAccelDiag'
    'YawRate'
    'YawRateStat'
    'YawRateDiag'
    'CylPres'
    'CylPresStat'
    'CylPresDiag'
    };
Proxy_TxByComTxEsp2Info = CreateBus(Proxy_TxByComTxEsp2Info, DeList);
clear DeList;

Proxy_TxByComTxEbs1Info = Simulink.Bus;
DeList={
    'CfBrkEbsStat'
    'CrBrkRegenTQLimit_NM'
    'CrBrkEstTot_NM'
    'CfBrkSnsFail'
    'CfBrkRbsWLamp'
    'CfBrkVacuumSysDef'
    'CrBrkEstHyd_NM'
    'CrBrkStkDep_PC'
    'CfBrkPgmRun1'
    };
Proxy_TxByComTxEbs1Info = CreateBus(Proxy_TxByComTxEbs1Info, DeList);
clear DeList;

Proxy_TxByComTxAhb1Info = Simulink.Bus;
DeList={
    'CfAhbWLMP'
    'CfAhbDiag'
    'CfAhbAct'
    'CfAhbDef'
    'CfAhbSLMP'
    'CfBrkAhbSTDep_PC'
    'CfBrkBuzzR'
    'CfBrkPedalCalStatus'
    'CfBrkAhbSnsFail'
    'WhlSpdFLAhb'
    'WhlSpdFRAhb'
    'CrAhbMsgChkSum'
    };
Proxy_TxByComTxAhb1Info = CreateBus(Proxy_TxByComTxAhb1Info, DeList);
clear DeList;

Proxy_TxByComSenPwrMonitorData = Simulink.Bus;
DeList={
    'SenPwrM_5V_Stable'
    'SenPwrM_12V_Stable'
    };
Proxy_TxByComSenPwrMonitorData = CreateBus(Proxy_TxByComSenPwrMonitorData, DeList);
clear DeList;

Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo = Simulink.Bus;
DeList={
    'QU_TAR_REPAT_XTRQ_FTAX_BAX_ACT'
    'TAR_REPAT_XTRQ_FTAX_BAX_ACT'
    'TAR_WMOM_PT_SUM_STAB_FAST'
    'ST_ECU_RQ_WMOM_PT_SUM_STAB'
    'QU_RQ_ILK_PT'
    'QU_TAR_WMOM_PT_SUM_STAB'
    'TAR_WMOM_PT_SUM_STAB'
    };
Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo = CreateBus(Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo, DeList);
clear DeList;

Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo = Simulink.Bus;
DeList={
    'ST_ECU_AVL_BRTORQ_SUM'
    'QU_SER_PRMSN_DBC'
    'QU_SER_PRF_BRK'
    'AVL_BRTORQ_SUM_DVCH'
    'AVL_BRTORQ_SUM'
    'QU_AVL_BRTORQ_SUM'
    'QU_AVL_BRTORQ_SUM_DVCH'
    };
Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo = CreateBus(Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo, DeList);
clear DeList;

Proxy_TxByComAVL_BRTORQ_WHLInfo = Simulink.Bus;
DeList={
    'QU_AVL_BRTORQ_WHL_FLH'
    'QU_AVL_BRTORQ_WHL_RRH'
    'QU_AVL_BRTORQ_WHL_FRH'
    'AVL_BRTORQ_WHL_RRH'
    'AVL_BRTORQ_WHL_RLH'
    'AVL_BRTORQ_WHL_FLH'
    'QU_AVL_BRTORQ_WHL_RLH'
    'AVL_BRTORQ_WHL_FRH'
    };
Proxy_TxByComAVL_BRTORQ_WHLInfo = CreateBus(Proxy_TxByComAVL_BRTORQ_WHLInfo, DeList);
clear DeList;

Proxy_TxByComAVL_RPM_WHLInfo = Simulink.Bus;
DeList={
    'AVL_RPM_WHL_RLH'
    'AVL_RPM_WHL_RRH'
    'AVL_RPM_WHL_FLH'
    'QU_AVL_RPM_WHL_FLH'
    'QU_AVL_RPM_WHL_FRH'
    'QU_AVL_RPM_WHL_RLH'
    'QU_AVL_RPM_WHL_RRH'
    'AVL_RPM_WHL_FRH'
    };
Proxy_TxByComAVL_RPM_WHLInfo = CreateBus(Proxy_TxByComAVL_RPM_WHLInfo, DeList);
clear DeList;

Proxy_TxByComRQ_WMOM_PT_SUM_RECUPInfo = Simulink.Bus;
DeList={
    'QU_TAR_WMOM_PT_SUM_RECUP'
    'TAR_WMOM_PT_SUM_RECUP'
    'ST_ECU_RQ_WMOM_PT_SUM_RECUP'
    };
Proxy_TxByComRQ_WMOM_PT_SUM_RECUPInfo = CreateBus(Proxy_TxByComRQ_WMOM_PT_SUM_RECUPInfo, DeList);
clear DeList;

Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info = Simulink.Bus;
DeList={
    'ST_ECU_ST_STAB_DSC'
    'ST_BRG_MSA'
    'ST_BRG_DV'
    'ST_UNRU_DSC'
    'QU_FN_FDR'
    'QU_FN_ABS'
    'QU_FN_ASC'
    'PRD_TIM_VEHSS'
    };
Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info = CreateBus(Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info, DeList);
clear DeList;

Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo = Simulink.Bus;
DeList={
    'QU_SER_ECBA'
    'TAR_BRTORQ_SUM_COOTD'
    'AVL_WAY_BRKFA'
    'AVL_FORC_BRKFA'
    };
Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo = CreateBus(Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo, DeList);
clear DeList;

Proxy_TxByComAVL_QUAN_EES_WHLInfo = Simulink.Bus;
DeList={
    'AVL_QUAN_EES_WHL_RLH'
    'AVL_QUAN_EES_WHL_RRH'
    'AVL_QUAN_EES_WHL_FLH'
    'AVL_QUAN_EES_WHL_FRH'
    'QU_AVL_QUAN_EES_WHL_FLH'
    'QU_AVL_QUAN_EES_WHL_RRH'
    'QU_AVL_QUAN_EES_WHL_FRH'
    'QU_AVL_QUAN_EES_WHL_RLH'
    };
Proxy_TxByComAVL_QUAN_EES_WHLInfo = CreateBus(Proxy_TxByComAVL_QUAN_EES_WHLInfo, DeList);
clear DeList;

Proxy_TxByComTAR_LTRQD_BAXInfo = Simulink.Bus;
DeList={
    'TAR_LTRQD_BAX'
    'LIM_MAX_LTRQD_BAX'
    };
Proxy_TxByComTAR_LTRQD_BAXInfo = CreateBus(Proxy_TxByComTAR_LTRQD_BAXInfo, DeList);
clear DeList;

Proxy_TxByComST_YMRInfo = Simulink.Bus;
DeList={
    'QU_SER_CLCTR_YMR'
    };
Proxy_TxByComST_YMRInfo = CreateBus(Proxy_TxByComST_YMRInfo, DeList);
clear DeList;

Proxy_TxByComWEGSTRECKEInfo = Simulink.Bus;
DeList={
    'MILE_FLH'
    'MILE_FRH'
    'MILE_INTM_TOMSRM'
    'MILE'
    };
Proxy_TxByComWEGSTRECKEInfo = CreateBus(Proxy_TxByComWEGSTRECKEInfo, DeList);
clear DeList;

Proxy_TxByComDISP_CC_DRDY_00Info = Simulink.Bus;
DeList={
    'ST_IDC_CC_DRDY_00'
    'ST_CC_DRDY_00'
    'TRANF_CC_DRDY_00'
    'NO_CC_DRDY_00'
    };
Proxy_TxByComDISP_CC_DRDY_00Info = CreateBus(Proxy_TxByComDISP_CC_DRDY_00Info, DeList);
clear DeList;

Proxy_TxByComDISP_CC_BYPA_00Info = Simulink.Bus;
DeList={
    'TRANF_CC_BYPA_00'
    'ST_IDC_CC_BYPA_00'
    'ST_CC_BYPA_00'
    'NO_CC_BYPA_00'
    };
Proxy_TxByComDISP_CC_BYPA_00Info = CreateBus(Proxy_TxByComDISP_CC_BYPA_00Info, DeList);
clear DeList;

Proxy_TxByComST_VHSSInfo = Simulink.Bus;
DeList={
    'ST_VEHSS'
    };
Proxy_TxByComST_VHSSInfo = CreateBus(Proxy_TxByComST_VHSSInfo, DeList);
clear DeList;

Proxy_TxByComDIAG_OBD_HYB_1Info = Simulink.Bus;
DeList={
    'QU_FN_OBD_1_SEN_1_ERR'
    'QU_FN_OBD_1_SEN_3_ERR'
    'QU_FN_OBD_1_SEN_4_ERR'
    'DIAG_OBD_HYB_1_MUX_MAX'
    'QU_FN_OBD_1_SEN_2_ERR'
    'DIAG_OBD_HYB_1_MUX_IMME'
    };
Proxy_TxByComDIAG_OBD_HYB_1Info = CreateBus(Proxy_TxByComDIAG_OBD_HYB_1Info, DeList);
clear DeList;

Proxy_TxByComSU_DSCInfo = Simulink.Bus;
DeList={
    'SU_DSC_WSS'
    };
Proxy_TxByComSU_DSCInfo = CreateBus(Proxy_TxByComSU_DSCInfo, DeList);
clear DeList;

Proxy_TxByComST_TYR_RDCInfo = Simulink.Bus;
DeList={
    'QU_TPL_RDC'
    'QU_FN_TYR_INFO_RDC'
    'QU_TFAI_RDC'
    };
Proxy_TxByComST_TYR_RDCInfo = CreateBus(Proxy_TxByComST_TYR_RDCInfo, DeList);
clear DeList;

Proxy_TxByComST_TYR_RPAInfo = Simulink.Bus;
DeList={
    'QU_TFAI_RPA'
    'QU_FN_TYR_INFO_RPA'
    'QU_TPL_RPA'
    };
Proxy_TxByComST_TYR_RPAInfo = CreateBus(Proxy_TxByComST_TYR_RPAInfo, DeList);
clear DeList;

Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info = Simulink.Bus;
DeList={
    'TYR_ID_2'
    'TYR_ID_1'
    'TYR_ID_3'
    'TYR_ID_4'
    };
Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info = CreateBus(Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info, DeList);
clear DeList;

Proxy_TxByComST_TYR_2Info = Simulink.Bus;
DeList={
    'QU_RDC_INIT_DISP'
    };
Proxy_TxByComST_TYR_2Info = CreateBus(Proxy_TxByComST_TYR_2Info, DeList);
clear DeList;

Proxy_TxByComAVL_T_TYRInfo = Simulink.Bus;
DeList={
    'AVL_TEMP_TYR_RRH'
    'AVL_TEMP_TYR_FLH'
    'AVL_TEMP_TYR_RLH'
    'AVL_TEMP_TYR_FRH'
    };
Proxy_TxByComAVL_T_TYRInfo = CreateBus(Proxy_TxByComAVL_T_TYRInfo, DeList);
clear DeList;

Proxy_TxByComTEMP_BRKInfo = Simulink.Bus;
DeList={
    'TEMP_BRDSK_RLH_VRFD'
    'QU_TEMP_BRDSK_RLH_VRFD'
    'TEMP_BRDSK_RRH_VRFD'
    'QU_TEMP_BRDSK_RRH_VRFD'
    };
Proxy_TxByComTEMP_BRKInfo = CreateBus(Proxy_TxByComTEMP_BRKInfo, DeList);
clear DeList;

Proxy_TxByComAVL_P_TYRInfo = Simulink.Bus;
DeList={
    'AVL_P_TYR_FRH'
    'AVL_P_TYR_FLH'
    'AVL_P_TYR_RRH'
    'AVL_P_TYR_RLH'
    };
Proxy_TxByComAVL_P_TYRInfo = CreateBus(Proxy_TxByComAVL_P_TYRInfo, DeList);
clear DeList;

Proxy_TxByComFR_DBG_DSCInfo = Simulink.Bus;
DeList={
    'DBG_RDCi_13'
    'DBG_RDCi_04'
    'DBG_RDCi_05'
    'DBG_RDCi_06'
    'DBG_RDCi_01'
    'DBG_RDCi_02'
    'DBG_RDCi_03'
    'DBG_RDCi_07'
    'DBG_RDCi_11'
    'DBG_RDCi_10'
    'DBG_RDCi_09'
    'DBG_RDCi_15'
    'DBG_RDCi_14'
    'DBG_RDCi_12'
    'DBG_RDCi_00'
    'BDTM_TA_FR'
    'BDTM_TA_RL'
    'BDTM_TA_RR'
    'DBG_RDCi_08'
    'ST_STATE'
    'BDTM_TA_FL'
    'ABSRef'
    'LOW_VOLTAGE'
    'DEBUG_PCIB'
    'DBG_DSC'
    'LATACC'
    'TRIGGER_FS'
    'TRIGGER_IS'
    };
Proxy_TxByComFR_DBG_DSCInfo = CreateBus(Proxy_TxByComFR_DBG_DSCInfo, DeList);
clear DeList;

