Proxy_TxEemCtrlInhibitData = Simulink.Bus;
DeList={
    'Eem_BBS_DegradeModeFail'
    'Eem_BBS_DefectiveModeFail'
    };
Proxy_TxEemCtrlInhibitData = CreateBus(Proxy_TxEemCtrlInhibitData, DeList);
clear DeList;

Proxy_TxCanTxInfo = Simulink.Bus;
DeList={
    'TqIntvTCS'
    'TqIntvMsr'
    'TqIntvSlowTCS'
    'MinGear'
    'MaxGear'
    'TcsReq'
    'TcsCtrl'
    'AbsAct'
    'TcsGearShiftChr'
    'EspCtrl'
    'MsrReq'
    'TcsProductInfo'
    };
Proxy_TxCanTxInfo = CreateBus(Proxy_TxCanTxInfo, DeList);
clear DeList;

Proxy_TxPdt5msRawInfo = Simulink.Bus;
DeList={
    'PdtSig'
    };
Proxy_TxPdt5msRawInfo = CreateBus(Proxy_TxPdt5msRawInfo, DeList);
clear DeList;

Proxy_TxPdf5msRawInfo = Simulink.Bus;
DeList={
    'PdfSig'
    };
Proxy_TxPdf5msRawInfo = CreateBus(Proxy_TxPdf5msRawInfo, DeList);
clear DeList;

Proxy_TxTarRgnBrkTqInfo = Simulink.Bus;
DeList={
    'TarRgnBrkTq'
    'EstTotBrkForce'
    'EstHydBrkForce'
    'EhbStat'
    };
Proxy_TxTarRgnBrkTqInfo = CreateBus(Proxy_TxTarRgnBrkTqInfo, DeList);
clear DeList;

Proxy_TxEscSwtStInfo = Simulink.Bus;
DeList={
    'EscDisabledBySwt'
    'TcsDisabledBySwt'
    };
Proxy_TxEscSwtStInfo = CreateBus(Proxy_TxEscSwtStInfo, DeList);
clear DeList;

Proxy_TxWhlSpdInfo = Simulink.Bus;
DeList={
    'FlWhlSpd'
    'FrWhlSpd'
    'RlWhlSpd'
    'RrlWhlSpd'
    };
Proxy_TxWhlSpdInfo = CreateBus(Proxy_TxWhlSpdInfo, DeList);
clear DeList;

Proxy_TxWhlEdgeCntInfo = Simulink.Bus;
DeList={
    'FlWhlEdgeCnt'
    'FrWhlEdgeCnt'
    'RlWhlEdgeCnt'
    'RrWhlEdgeCnt'
    };
Proxy_TxWhlEdgeCntInfo = CreateBus(Proxy_TxWhlEdgeCntInfo, DeList);
clear DeList;

Proxy_TxPistP5msRawInfo = Simulink.Bus;
DeList={
    'PistPSig'
    };
Proxy_TxPistP5msRawInfo = CreateBus(Proxy_TxPistP5msRawInfo, DeList);
clear DeList;

Proxy_TxLogicEepDataInfo = Simulink.Bus;
DeList={
    'KPdtOffsEolReadVal'
    'ReadInvld'
    };
Proxy_TxLogicEepDataInfo = CreateBus(Proxy_TxLogicEepDataInfo, DeList);
clear DeList;

Proxy_TxEemLampData = Simulink.Bus;
DeList={
    'Eem_Lamp_EBDLampRequest'
    'Eem_Lamp_ABSLampRequest'
    'Eem_Lamp_TCSLampRequest'
    'Eem_Lamp_TCSOffLampRequest'
    'Eem_Lamp_VDCLampRequest'
    'Eem_Lamp_VDCOffLampRequest'
    'Eem_Lamp_HDCLampRequest'
    'Eem_Lamp_BBSBuzzorRequest'
    'Eem_Lamp_RBCSLampRequest'
    'Eem_Lamp_AHBLampRequest'
    'Eem_Lamp_ServiceLampRequest'
    'Eem_Lamp_TCSFuncLampRequest'
    'Eem_Lamp_VDCFuncLampRequest'
    'Eem_Lamp_AVHLampRequest'
    'Eem_Lamp_AVHILampRequest'
    'Eem_Lamp_ACCEnable'
    'Eem_Lamp_CDMEnable'
    'Eem_Buzzor_On'
    };
Proxy_TxEemLampData = CreateBus(Proxy_TxEemLampData, DeList);
clear DeList;

Proxy_TxEcuModeSts = Simulink.Bus;
DeList={'Proxy_TxEcuModeSts'};
Proxy_TxEcuModeSts = CreateBus(Proxy_TxEcuModeSts, DeList);
clear DeList;

Proxy_TxIgnOnOffSts = Simulink.Bus;
DeList={'Proxy_TxIgnOnOffSts'};
Proxy_TxIgnOnOffSts = CreateBus(Proxy_TxIgnOnOffSts, DeList);
clear DeList;

Proxy_TxDiagSci = Simulink.Bus;
DeList={'Proxy_TxDiagSci'};
Proxy_TxDiagSci = CreateBus(Proxy_TxDiagSci, DeList);
clear DeList;

Proxy_TxDiagAhbSci = Simulink.Bus;
DeList={'Proxy_TxDiagAhbSci'};
Proxy_TxDiagAhbSci = CreateBus(Proxy_TxDiagAhbSci, DeList);
clear DeList;

Proxy_TxFuncInhibitProxySts = Simulink.Bus;
DeList={'Proxy_TxFuncInhibitProxySts'};
Proxy_TxFuncInhibitProxySts = CreateBus(Proxy_TxFuncInhibitProxySts, DeList);
clear DeList;

Proxy_TxTxESCSensorInfo = Simulink.Bus;
DeList={
    'EstimatedYaw'
    'EstimatedAY'
    'A_long_1_1000g'
    };
Proxy_TxTxESCSensorInfo = CreateBus(Proxy_TxTxESCSensorInfo, DeList);
clear DeList;

Proxy_TxTxYawCbitInfo = Simulink.Bus;
DeList={
    'TestYawRateSensor'
    'TestAcclSensor'
    'YawSerialNumberReq'
    };
Proxy_TxTxYawCbitInfo = CreateBus(Proxy_TxTxYawCbitInfo, DeList);
clear DeList;

Proxy_TxTxWhlSpdInfo = Simulink.Bus;
DeList={
    'WhlSpdFL'
    'WhlSpdFR'
    'WhlSpdRL'
    'WhlSpdRR'
    };
Proxy_TxTxWhlSpdInfo = CreateBus(Proxy_TxTxWhlSpdInfo, DeList);
clear DeList;

Proxy_TxTxWhlPulInfo = Simulink.Bus;
DeList={
    'WhlPulFL'
    'WhlPulFR'
    'WhlPulRL'
    'WhlPulRR'
    };
Proxy_TxTxWhlPulInfo = CreateBus(Proxy_TxTxWhlPulInfo, DeList);
clear DeList;

Proxy_TxTxTcs5Info = Simulink.Bus;
DeList={
    'CfBrkAbsWLMP'
    'CfBrkEbdWLMP'
    'CfBrkTcsWLMP'
    'CfBrkTcsFLMP'
    'CfBrkAbsDIAG'
    'CrBrkWheelFL_KMH'
    'CrBrkWheelFR_KMH'
    'CrBrkWheelRL_KMH'
    'CrBrkWheelRR_KMH'
    };
Proxy_TxTxTcs5Info = CreateBus(Proxy_TxTxTcs5Info, DeList);
clear DeList;

Proxy_TxTxTcs1Info = Simulink.Bus;
DeList={
    'CfBrkTcsREQ'
    'CfBrkTcsPAS'
    'CfBrkTcsDEF'
    'CfBrkTcsCTL'
    'CfBrkAbsACT'
    'CfBrkAbsDEF'
    'CfBrkEbdDEF'
    'CfBrkTcsGSC'
    'CfBrkEspPAS'
    'CfBrkEspDEF'
    'CfBrkEspCTL'
    'CfBrkMsrREQ'
    'CfBrkTcsMFRN'
    'CfBrkMinGEAR'
    'CfBrkMaxGEAR'
    'BrakeLight'
    'HacCtl'
    'HacPas'
    'HacDef'
    'CrBrkTQI_PC'
    'CrBrkTQIMSR_PC'
    'CrBrkTQISLW_PC'
    'CrEbsMSGCHKSUM'
    };
Proxy_TxTxTcs1Info = CreateBus(Proxy_TxTxTcs1Info, DeList);
clear DeList;

Proxy_TxTxSasCalInfo = Simulink.Bus;
DeList={
    'CalSas_CCW'
    'CalSas_Lws_CID'
    };
Proxy_TxTxSasCalInfo = CreateBus(Proxy_TxTxSasCalInfo, DeList);
clear DeList;

Proxy_TxTxEsp2Info = Simulink.Bus;
DeList={
    'LatAccel'
    'LatAccelStat'
    'LatAccelDiag'
    'LongAccel'
    'LongAccelStat'
    'LongAccelDiag'
    'YawRate'
    'YawRateStat'
    'YawRateDiag'
    'CylPres'
    'CylPresStat'
    'CylPresDiag'
    };
Proxy_TxTxEsp2Info = CreateBus(Proxy_TxTxEsp2Info, DeList);
clear DeList;

Proxy_TxTxEbs1Info = Simulink.Bus;
DeList={
    'CfBrkEbsStat'
    'CrBrkRegenTQLimit_NM'
    'CrBrkEstTot_NM'
    'CfBrkSnsFail'
    'CfBrkRbsWLamp'
    'CfBrkVacuumSysDef'
    'CrBrkEstHyd_NM'
    'CrBrkStkDep_PC'
    'CfBrkPgmRun1'
    };
Proxy_TxTxEbs1Info = CreateBus(Proxy_TxTxEbs1Info, DeList);
clear DeList;

Proxy_TxTxAhb1Info = Simulink.Bus;
DeList={
    'CfAhbWLMP'
    'CfAhbDiag'
    'CfAhbAct'
    'CfAhbDef'
    'CfAhbSLMP'
    'CfBrkAhbSTDep_PC'
    'CfBrkBuzzR'
    'CfBrkPedalCalStatus'
    'CfBrkAhbSnsFail'
    'WhlSpdFLAhb'
    'WhlSpdFRAhb'
    'CrAhbMsgChkSum'
    };
Proxy_TxTxAhb1Info = CreateBus(Proxy_TxTxAhb1Info, DeList);
clear DeList;

Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo = Simulink.Bus;
DeList={
    'QU_TAR_REPAT_XTRQ_FTAX_BAX_ACT'
    'TAR_REPAT_XTRQ_FTAX_BAX_ACT'
    'TAR_WMOM_PT_SUM_STAB_FAST'
    'ST_ECU_RQ_WMOM_PT_SUM_STAB'
    'QU_RQ_ILK_PT'
    'QU_TAR_WMOM_PT_SUM_STAB'
    'TAR_WMOM_PT_SUM_STAB'
    };
Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo = CreateBus(Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo, DeList);
clear DeList;

Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo = Simulink.Bus;
DeList={
    'ST_ECU_AVL_BRTORQ_SUM'
    'QU_SER_PRMSN_DBC'
    'QU_SER_PRF_BRK'
    'AVL_BRTORQ_SUM_DVCH'
    'AVL_BRTORQ_SUM'
    'QU_AVL_BRTORQ_SUM'
    'QU_AVL_BRTORQ_SUM_DVCH'
    };
Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo = CreateBus(Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo, DeList);
clear DeList;

Proxy_TxAVL_BRTORQ_WHLInfo = Simulink.Bus;
DeList={
    'QU_AVL_BRTORQ_WHL_FLH'
    'QU_AVL_BRTORQ_WHL_RRH'
    'QU_AVL_BRTORQ_WHL_FRH'
    'AVL_BRTORQ_WHL_RRH'
    'AVL_BRTORQ_WHL_RLH'
    'AVL_BRTORQ_WHL_FLH'
    'QU_AVL_BRTORQ_WHL_RLH'
    'AVL_BRTORQ_WHL_FRH'
    };
Proxy_TxAVL_BRTORQ_WHLInfo = CreateBus(Proxy_TxAVL_BRTORQ_WHLInfo, DeList);
clear DeList;

Proxy_TxAVL_RPM_WHLInfo = Simulink.Bus;
DeList={
    'AVL_RPM_WHL_RLH'
    'AVL_RPM_WHL_RRH'
    'AVL_RPM_WHL_FLH'
    'QU_AVL_RPM_WHL_FLH'
    'QU_AVL_RPM_WHL_FRH'
    'QU_AVL_RPM_WHL_RLH'
    'QU_AVL_RPM_WHL_RRH'
    'AVL_RPM_WHL_FRH'
    };
Proxy_TxAVL_RPM_WHLInfo = CreateBus(Proxy_TxAVL_RPM_WHLInfo, DeList);
clear DeList;

Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo = Simulink.Bus;
DeList={
    'QU_TAR_WMOM_PT_SUM_RECUP'
    'TAR_WMOM_PT_SUM_RECUP'
    'ST_ECU_RQ_WMOM_PT_SUM_RECUP'
    };
Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo = CreateBus(Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo, DeList);
clear DeList;

Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info = Simulink.Bus;
DeList={
    'ST_ECU_ST_STAB_DSC'
    'ST_BRG_MSA'
    'ST_BRG_DV'
    'ST_UNRU_DSC'
    'QU_FN_FDR'
    'QU_FN_ABS'
    'QU_FN_ASC'
    'PRD_TIM_VEHSS'
    };
Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info = CreateBus(Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info, DeList);
clear DeList;

Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo = Simulink.Bus;
DeList={
    'QU_SER_ECBA'
    'TAR_BRTORQ_SUM_COOTD'
    'AVL_WAY_BRKFA'
    'AVL_FORC_BRKFA'
    };
Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo = CreateBus(Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo, DeList);
clear DeList;

Proxy_TxAVL_QUAN_EES_WHLInfo = Simulink.Bus;
DeList={
    'AVL_QUAN_EES_WHL_RLH'
    'AVL_QUAN_EES_WHL_RRH'
    'AVL_QUAN_EES_WHL_FLH'
    'AVL_QUAN_EES_WHL_FRH'
    'QU_AVL_QUAN_EES_WHL_FLH'
    'QU_AVL_QUAN_EES_WHL_RRH'
    'QU_AVL_QUAN_EES_WHL_FRH'
    'QU_AVL_QUAN_EES_WHL_RLH'
    };
Proxy_TxAVL_QUAN_EES_WHLInfo = CreateBus(Proxy_TxAVL_QUAN_EES_WHLInfo, DeList);
clear DeList;

Proxy_TxTAR_LTRQD_BAXInfo = Simulink.Bus;
DeList={
    'TAR_LTRQD_BAX'
    'LIM_MAX_LTRQD_BAX'
    };
Proxy_TxTAR_LTRQD_BAXInfo = CreateBus(Proxy_TxTAR_LTRQD_BAXInfo, DeList);
clear DeList;

Proxy_TxST_YMRInfo = Simulink.Bus;
DeList={
    'QU_SER_CLCTR_YMR'
    };
Proxy_TxST_YMRInfo = CreateBus(Proxy_TxST_YMRInfo, DeList);
clear DeList;

Proxy_TxWEGSTRECKEInfo = Simulink.Bus;
DeList={
    'MILE_FLH'
    'MILE_FRH'
    'MILE_INTM_TOMSRM'
    'MILE'
    };
Proxy_TxWEGSTRECKEInfo = CreateBus(Proxy_TxWEGSTRECKEInfo, DeList);
clear DeList;

Proxy_TxDISP_CC_DRDY_00Info = Simulink.Bus;
DeList={
    'ST_IDC_CC_DRDY_00'
    'ST_CC_DRDY_00'
    'TRANF_CC_DRDY_00'
    'NO_CC_DRDY_00'
    };
Proxy_TxDISP_CC_DRDY_00Info = CreateBus(Proxy_TxDISP_CC_DRDY_00Info, DeList);
clear DeList;

Proxy_TxDISP_CC_BYPA_00Info = Simulink.Bus;
DeList={
    'TRANF_CC_BYPA_00'
    'ST_IDC_CC_BYPA_00'
    'ST_CC_BYPA_00'
    'NO_CC_BYPA_00'
    };
Proxy_TxDISP_CC_BYPA_00Info = CreateBus(Proxy_TxDISP_CC_BYPA_00Info, DeList);
clear DeList;

Proxy_TxST_VHSSInfo = Simulink.Bus;
DeList={
    'ST_VEHSS'
    };
Proxy_TxST_VHSSInfo = CreateBus(Proxy_TxST_VHSSInfo, DeList);
clear DeList;

Proxy_TxDIAG_OBD_HYB_1Info = Simulink.Bus;
DeList={
    'QU_FN_OBD_1_SEN_1_ERR'
    'QU_FN_OBD_1_SEN_3_ERR'
    'QU_FN_OBD_1_SEN_4_ERR'
    'DIAG_OBD_HYB_1_MUX_MAX'
    'QU_FN_OBD_1_SEN_2_ERR'
    'DIAG_OBD_HYB_1_MUX_IMME'
    };
Proxy_TxDIAG_OBD_HYB_1Info = CreateBus(Proxy_TxDIAG_OBD_HYB_1Info, DeList);
clear DeList;

Proxy_TxSU_DSCInfo = Simulink.Bus;
DeList={
    'SU_DSC_WSS'
    };
Proxy_TxSU_DSCInfo = CreateBus(Proxy_TxSU_DSCInfo, DeList);
clear DeList;

Proxy_TxST_TYR_RDCInfo = Simulink.Bus;
DeList={
    'QU_TPL_RDC'
    'QU_FN_TYR_INFO_RDC'
    'QU_TFAI_RDC'
    };
Proxy_TxST_TYR_RDCInfo = CreateBus(Proxy_TxST_TYR_RDCInfo, DeList);
clear DeList;

Proxy_TxST_TYR_RPAInfo = Simulink.Bus;
DeList={
    'QU_TFAI_RPA'
    'QU_FN_TYR_INFO_RPA'
    'QU_TPL_RPA'
    };
Proxy_TxST_TYR_RPAInfo = CreateBus(Proxy_TxST_TYR_RPAInfo, DeList);
clear DeList;

Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info = Simulink.Bus;
DeList={
    'TYR_ID_2'
    'TYR_ID_1'
    'TYR_ID_3'
    'TYR_ID_4'
    };
Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info = CreateBus(Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info, DeList);
clear DeList;

Proxy_TxST_TYR_2Info = Simulink.Bus;
DeList={
    'QU_RDC_INIT_DISP'
    };
Proxy_TxST_TYR_2Info = CreateBus(Proxy_TxST_TYR_2Info, DeList);
clear DeList;

Proxy_TxAVL_T_TYRInfo = Simulink.Bus;
DeList={
    'AVL_TEMP_TYR_RRH'
    'AVL_TEMP_TYR_FLH'
    'AVL_TEMP_TYR_RLH'
    'AVL_TEMP_TYR_FRH'
    };
Proxy_TxAVL_T_TYRInfo = CreateBus(Proxy_TxAVL_T_TYRInfo, DeList);
clear DeList;

Proxy_TxTEMP_BRKInfo = Simulink.Bus;
DeList={
    'TEMP_BRDSK_RLH_VRFD'
    'QU_TEMP_BRDSK_RLH_VRFD'
    'TEMP_BRDSK_RRH_VRFD'
    'QU_TEMP_BRDSK_RRH_VRFD'
    };
Proxy_TxTEMP_BRKInfo = CreateBus(Proxy_TxTEMP_BRKInfo, DeList);
clear DeList;

Proxy_TxAVL_P_TYRInfo = Simulink.Bus;
DeList={
    'AVL_P_TYR_FRH'
    'AVL_P_TYR_FLH'
    'AVL_P_TYR_RRH'
    'AVL_P_TYR_RLH'
    };
Proxy_TxAVL_P_TYRInfo = CreateBus(Proxy_TxAVL_P_TYRInfo, DeList);
clear DeList;

Proxy_TxFR_DBG_DSCInfo = Simulink.Bus;
DeList={
    'DBG_RDCi_13'
    'DBG_RDCi_04'
    'DBG_RDCi_05'
    'DBG_RDCi_06'
    'DBG_RDCi_01'
    'DBG_RDCi_02'
    'DBG_RDCi_03'
    'DBG_RDCi_07'
    'DBG_RDCi_11'
    'DBG_RDCi_10'
    'DBG_RDCi_09'
    'DBG_RDCi_15'
    'DBG_RDCi_14'
    'DBG_RDCi_12'
    'DBG_RDCi_00'
    'BDTM_TA_FR'
    'BDTM_TA_RL'
    'BDTM_TA_RR'
    'DBG_RDCi_08'
    'ST_STATE'
    'BDTM_TA_FL'
    'ABSRef'
    'LOW_VOLTAGE'
    'DEBUG_PCIB'
    'DBG_DSC'
    'LATACC'
    'TRIGGER_FS'
    'TRIGGER_IS'
    };
Proxy_TxFR_DBG_DSCInfo = CreateBus(Proxy_TxFR_DBG_DSCInfo, DeList);
clear DeList;

