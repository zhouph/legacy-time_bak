/**
 * @defgroup Proxy_RxByCom_Ifa Proxy_RxByCom_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Proxy_RxByCom_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PROXY_RXBYCOM_IFA_H_
#define PROXY_RXBYCOM_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */

/* Set Output DE MAcro Function */
#define Proxy_RxByCom_Write_Proxy_RxByComRxBms1Info(data) do \
{ \
    Proxy_RxByComRxBms1Info = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxYawSerialInfo(data) do \
{ \
    Proxy_RxByComRxYawSerialInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxYawAccInfo(data) do \
{ \
    Proxy_RxByComRxYawAccInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxTcu6Info(data) do \
{ \
    Proxy_RxByComRxTcu6Info = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxTcu5Info(data) do \
{ \
    Proxy_RxByComRxTcu5Info = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxTcu1Info(data) do \
{ \
    Proxy_RxByComRxTcu1Info = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxSasInfo(data) do \
{ \
    Proxy_RxByComRxSasInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxMcu2Info(data) do \
{ \
    Proxy_RxByComRxMcu2Info = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxMcu1Info(data) do \
{ \
    Proxy_RxByComRxMcu1Info = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxLongAccInfo(data) do \
{ \
    Proxy_RxByComRxLongAccInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxHcu5Info(data) do \
{ \
    Proxy_RxByComRxHcu5Info = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxHcu3Info(data) do \
{ \
    Proxy_RxByComRxHcu3Info = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxHcu2Info(data) do \
{ \
    Proxy_RxByComRxHcu2Info = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxHcu1Info(data) do \
{ \
    Proxy_RxByComRxHcu1Info = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxFact1Info(data) do \
{ \
    Proxy_RxByComRxFact1Info = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxEms3Info(data) do \
{ \
    Proxy_RxByComRxEms3Info = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxEms2Info(data) do \
{ \
    Proxy_RxByComRxEms2Info = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxEms1Info(data) do \
{ \
    Proxy_RxByComRxEms1Info = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxClu2Info(data) do \
{ \
    Proxy_RxByComRxClu2Info = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxClu1Info(data) do \
{ \
    Proxy_RxByComRxClu1Info = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxMsgOkFlgInfo(data) do \
{ \
    Proxy_RxByComRxMsgOkFlgInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo(data) do \
{ \
    Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComAVL_LTRQD_BAXInfo(data) do \
{ \
    Proxy_RxByComAVL_LTRQD_BAXInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo(data) do \
{ \
    Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info(data) do \
{ \
    Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info(data) do \
{ \
    Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComHGLV_VEH_FILTInfo(data) do \
{ \
    Proxy_RxByComHGLV_VEH_FILTInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo(data) do \
{ \
    Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo(data) do \
{ \
    Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComV_VEH_V_VEH_2Info(data) do \
{ \
    Proxy_RxByComV_VEH_V_VEH_2Info = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo(data) do \
{ \
    Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo(data) do \
{ \
    Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComAVL_STEA_FTAXInfo(data) do \
{ \
    Proxy_RxByComAVL_STEA_FTAXInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo(data) do \
{ \
    Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info(data) do \
{ \
    Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_7Info(data) do \
{ \
    Proxy_RxByComWMOM_DRV_7Info = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo(data) do \
{ \
    Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_BRKSYS_ENGMGInfo(data) do \
{ \
    Proxy_RxByComDT_BRKSYS_ENGMGInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComERRM_BN_UInfo(data) do \
{ \
    Proxy_RxByComERRM_BN_UInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComCTR_CRInfo(data) do \
{ \
    Proxy_RxByComCTR_CRInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComKLEMMENInfo(data) do \
{ \
    Proxy_RxByComKLEMMENInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info(data) do \
{ \
    Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComBEDIENUNG_WISCHERInfo(data) do \
{ \
    Proxy_RxByComBEDIENUNG_WISCHERInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_PT_2Info(data) do \
{ \
    Proxy_RxByComDT_PT_2Info = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_ENG_STA_AUTOInfo(data) do \
{ \
    Proxy_RxByComSTAT_ENG_STA_AUTOInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComST_ENERG_GENInfo(data) do \
{ \
    Proxy_RxByComST_ENERG_GENInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_PT_1Info(data) do \
{ \
    Proxy_RxByComDT_PT_1Info = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_GRDT_DRVInfo(data) do \
{ \
    Proxy_RxByComDT_GRDT_DRVInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDIAG_OBD_ENGMG_ELInfo(data) do \
{ \
    Proxy_RxByComDIAG_OBD_ENGMG_ELInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_CT_HABRInfo(data) do \
{ \
    Proxy_RxByComSTAT_CT_HABRInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_PT_3Info(data) do \
{ \
    Proxy_RxByComDT_PT_3Info = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComEINHEITEN_BN2020Info(data) do \
{ \
    Proxy_RxByComEINHEITEN_BN2020Info = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComA_TEMPInfo(data) do \
{ \
    Proxy_RxByComA_TEMPInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWISCHERGESCHWINDIGKEITInfo(data) do \
{ \
    Proxy_RxByComWISCHERGESCHWINDIGKEITInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_DS_VRFDInfo(data) do \
{ \
    Proxy_RxByComSTAT_DS_VRFDInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info(data) do \
{ \
    Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_ANHAENGERInfo(data) do \
{ \
    Proxy_RxByComSTAT_ANHAENGERInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComFZZSTDInfo(data) do \
{ \
    Proxy_RxByComFZZSTDInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComBEDIENUNG_FAHRWERKInfo(data) do \
{ \
    Proxy_RxByComBEDIENUNG_FAHRWERKInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComFAHRZEUGTYPInfo(data) do \
{ \
    Proxy_RxByComFAHRZEUGTYPInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComST_BLT_CT_SOCCUInfo(data) do \
{ \
    Proxy_RxByComST_BLT_CT_SOCCUInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComFAHRGESTELLNUMMERInfo(data) do \
{ \
    Proxy_RxByComFAHRGESTELLNUMMERInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRELATIVZEITInfo(data) do \
{ \
    Proxy_RxByComRELATIVZEITInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComKILOMETERSTANDInfo(data) do \
{ \
    Proxy_RxByComKILOMETERSTANDInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComUHRZEIT_DATUMInfo(data) do \
{ \
    Proxy_RxByComUHRZEIT_DATUMInfo = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxBms1Info_Bms1SocPc(data) do \
{ \
    Proxy_RxByComRxBms1Info.Bms1SocPc = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxYawSerialInfo_YawSerialNum_0(data) do \
{ \
    Proxy_RxByComRxYawSerialInfo.YawSerialNum_0 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxYawSerialInfo_YawSerialNum_1(data) do \
{ \
    Proxy_RxByComRxYawSerialInfo.YawSerialNum_1 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxYawSerialInfo_YawSerialNum_2(data) do \
{ \
    Proxy_RxByComRxYawSerialInfo.YawSerialNum_2 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxYawAccInfo_YawRateValidData(data) do \
{ \
    Proxy_RxByComRxYawAccInfo.YawRateValidData = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxYawAccInfo_YawRateSelfTestStatus(data) do \
{ \
    Proxy_RxByComRxYawAccInfo.YawRateSelfTestStatus = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxYawAccInfo_YawRateSignal_0(data) do \
{ \
    Proxy_RxByComRxYawAccInfo.YawRateSignal_0 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxYawAccInfo_YawRateSignal_1(data) do \
{ \
    Proxy_RxByComRxYawAccInfo.YawRateSignal_1 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxYawAccInfo_SensorOscFreqDev(data) do \
{ \
    Proxy_RxByComRxYawAccInfo.SensorOscFreqDev = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxYawAccInfo_Gyro_Fail(data) do \
{ \
    Proxy_RxByComRxYawAccInfo.Gyro_Fail = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxYawAccInfo_Raster_Fail(data) do \
{ \
    Proxy_RxByComRxYawAccInfo.Raster_Fail = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxYawAccInfo_Eep_Fail(data) do \
{ \
    Proxy_RxByComRxYawAccInfo.Eep_Fail = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxYawAccInfo_Batt_Range_Err(data) do \
{ \
    Proxy_RxByComRxYawAccInfo.Batt_Range_Err = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxYawAccInfo_Asic_Fail(data) do \
{ \
    Proxy_RxByComRxYawAccInfo.Asic_Fail = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxYawAccInfo_Accel_Fail(data) do \
{ \
    Proxy_RxByComRxYawAccInfo.Accel_Fail = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxYawAccInfo_Ram_Fail(data) do \
{ \
    Proxy_RxByComRxYawAccInfo.Ram_Fail = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxYawAccInfo_Rom_Fail(data) do \
{ \
    Proxy_RxByComRxYawAccInfo.Rom_Fail = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxYawAccInfo_Ad_Fail(data) do \
{ \
    Proxy_RxByComRxYawAccInfo.Ad_Fail = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxYawAccInfo_Osc_Fail(data) do \
{ \
    Proxy_RxByComRxYawAccInfo.Osc_Fail = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxYawAccInfo_Watchdog_Rst(data) do \
{ \
    Proxy_RxByComRxYawAccInfo.Watchdog_Rst = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxYawAccInfo_Plaus_Err_Pst(data) do \
{ \
    Proxy_RxByComRxYawAccInfo.Plaus_Err_Pst = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxYawAccInfo_RollingCounter(data) do \
{ \
    Proxy_RxByComRxYawAccInfo.RollingCounter = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxYawAccInfo_Can_Func_Err(data) do \
{ \
    Proxy_RxByComRxYawAccInfo.Can_Func_Err = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxYawAccInfo_AccelEratorRateSig_0(data) do \
{ \
    Proxy_RxByComRxYawAccInfo.AccelEratorRateSig_0 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxYawAccInfo_AccelEratorRateSig_1(data) do \
{ \
    Proxy_RxByComRxYawAccInfo.AccelEratorRateSig_1 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxYawAccInfo_LatAccValidData(data) do \
{ \
    Proxy_RxByComRxYawAccInfo.LatAccValidData = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxYawAccInfo_LatAccSelfTestStatus(data) do \
{ \
    Proxy_RxByComRxYawAccInfo.LatAccSelfTestStatus = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxTcu6Info_ShiftClass_Ccan(data) do \
{ \
    Proxy_RxByComRxTcu6Info.ShiftClass_Ccan = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxTcu5Info_Typ(data) do \
{ \
    Proxy_RxByComRxTcu5Info.Typ = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxTcu5Info_GearTyp(data) do \
{ \
    Proxy_RxByComRxTcu5Info.GearTyp = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxTcu1Info_Targe(data) do \
{ \
    Proxy_RxByComRxTcu1Info.Targe = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxTcu1Info_GarChange(data) do \
{ \
    Proxy_RxByComRxTcu1Info.GarChange = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxTcu1Info_Flt(data) do \
{ \
    Proxy_RxByComRxTcu1Info.Flt = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxTcu1Info_GarSelDisp(data) do \
{ \
    Proxy_RxByComRxTcu1Info.GarSelDisp = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxTcu1Info_TQRedReq_PC(data) do \
{ \
    Proxy_RxByComRxTcu1Info.TQRedReq_PC = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxTcu1Info_TQRedReqSlw_PC(data) do \
{ \
    Proxy_RxByComRxTcu1Info.TQRedReqSlw_PC = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxTcu1Info_TQIncReq_PC(data) do \
{ \
    Proxy_RxByComRxTcu1Info.TQIncReq_PC = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxSasInfo_Angle(data) do \
{ \
    Proxy_RxByComRxSasInfo.Angle = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxSasInfo_Speed(data) do \
{ \
    Proxy_RxByComRxSasInfo.Speed = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxSasInfo_Ok(data) do \
{ \
    Proxy_RxByComRxSasInfo.Ok = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxSasInfo_Cal(data) do \
{ \
    Proxy_RxByComRxSasInfo.Cal = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxSasInfo_Trim(data) do \
{ \
    Proxy_RxByComRxSasInfo.Trim = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxSasInfo_CheckSum(data) do \
{ \
    Proxy_RxByComRxSasInfo.CheckSum = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxSasInfo_MsgCount(data) do \
{ \
    Proxy_RxByComRxSasInfo.MsgCount = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxMcu2Info_Flt(data) do \
{ \
    Proxy_RxByComRxMcu2Info.Flt = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxMcu1Info_MoTestTQ_PC(data) do \
{ \
    Proxy_RxByComRxMcu1Info.MoTestTQ_PC = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxMcu1Info_MotActRotSpd_RPM(data) do \
{ \
    Proxy_RxByComRxMcu1Info.MotActRotSpd_RPM = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxLongAccInfo_IntSenFltSymtmActive(data) do \
{ \
    Proxy_RxByComRxLongAccInfo.IntSenFltSymtmActive = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxLongAccInfo_IntSenFaultPresent(data) do \
{ \
    Proxy_RxByComRxLongAccInfo.IntSenFaultPresent = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxLongAccInfo_LongAccSenCirErrPre(data) do \
{ \
    Proxy_RxByComRxLongAccInfo.LongAccSenCirErrPre = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxLongAccInfo_LonACSenRanChkErrPre(data) do \
{ \
    Proxy_RxByComRxLongAccInfo.LonACSenRanChkErrPre = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxLongAccInfo_LongRollingCounter(data) do \
{ \
    Proxy_RxByComRxLongAccInfo.LongRollingCounter = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxLongAccInfo_IntTempSensorFault(data) do \
{ \
    Proxy_RxByComRxLongAccInfo.IntTempSensorFault = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxLongAccInfo_LongAccInvalidData(data) do \
{ \
    Proxy_RxByComRxLongAccInfo.LongAccInvalidData = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxLongAccInfo_LongAccSelfTstStatus(data) do \
{ \
    Proxy_RxByComRxLongAccInfo.LongAccSelfTstStatus = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxLongAccInfo_LongAccRateSignal_0(data) do \
{ \
    Proxy_RxByComRxLongAccInfo.LongAccRateSignal_0 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxLongAccInfo_LongAccRateSignal_1(data) do \
{ \
    Proxy_RxByComRxLongAccInfo.LongAccRateSignal_1 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxHcu5Info_HevMod(data) do \
{ \
    Proxy_RxByComRxHcu5Info.HevMod = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxHcu3Info_TmIntQcMDBINV_PC(data) do \
{ \
    Proxy_RxByComRxHcu3Info.TmIntQcMDBINV_PC = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxHcu3Info_MotTQCMC_PC(data) do \
{ \
    Proxy_RxByComRxHcu3Info.MotTQCMC_PC = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxHcu3Info_MotTQCMDBINV_PC(data) do \
{ \
    Proxy_RxByComRxHcu3Info.MotTQCMDBINV_PC = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxHcu2Info_ServiceMod(data) do \
{ \
    Proxy_RxByComRxHcu2Info.ServiceMod = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxHcu2Info_RegenENA(data) do \
{ \
    Proxy_RxByComRxHcu2Info.RegenENA = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxHcu2Info_RegenBRKTQ_NM(data) do \
{ \
    Proxy_RxByComRxHcu2Info.RegenBRKTQ_NM = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxHcu2Info_CrpTQ_NM(data) do \
{ \
    Proxy_RxByComRxHcu2Info.CrpTQ_NM = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxHcu2Info_WhlDEMTQ_NM(data) do \
{ \
    Proxy_RxByComRxHcu2Info.WhlDEMTQ_NM = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxHcu1Info_EngCltStat(data) do \
{ \
    Proxy_RxByComRxHcu1Info.EngCltStat = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxHcu1Info_HEVRDY(data) do \
{ \
    Proxy_RxByComRxHcu1Info.HEVRDY = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxHcu1Info_EngTQCmdBinV_PC(data) do \
{ \
    Proxy_RxByComRxHcu1Info.EngTQCmdBinV_PC = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxHcu1Info_EngTQCmd_PC(data) do \
{ \
    Proxy_RxByComRxHcu1Info.EngTQCmd_PC = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxFact1Info_OutTemp_SNR_C(data) do \
{ \
    Proxy_RxByComRxFact1Info.OutTemp_SNR_C = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxEms3Info_EngColTemp_C(data) do \
{ \
    Proxy_RxByComRxEms3Info.EngColTemp_C = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxEms2Info_EngSpdErr(data) do \
{ \
    Proxy_RxByComRxEms2Info.EngSpdErr = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxEms2Info_AccPedDep_PC(data) do \
{ \
    Proxy_RxByComRxEms2Info.AccPedDep_PC = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxEms2Info_Tps_PC(data) do \
{ \
    Proxy_RxByComRxEms2Info.Tps_PC = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxEms1Info_TqStd_NM(data) do \
{ \
    Proxy_RxByComRxEms1Info.TqStd_NM = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxEms1Info_ActINDTQ_PC(data) do \
{ \
    Proxy_RxByComRxEms1Info.ActINDTQ_PC = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxEms1Info_EngSpd_RPM(data) do \
{ \
    Proxy_RxByComRxEms1Info.EngSpd_RPM = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxEms1Info_IndTQ_PC(data) do \
{ \
    Proxy_RxByComRxEms1Info.IndTQ_PC = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxEms1Info_FrictTQ_PC(data) do \
{ \
    Proxy_RxByComRxEms1Info.FrictTQ_PC = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxClu2Info_IGN_SW(data) do \
{ \
    Proxy_RxByComRxClu2Info.IGN_SW = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxClu1Info_P_Brake_Act(data) do \
{ \
    Proxy_RxByComRxClu1Info.P_Brake_Act = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxClu1Info_Cf_Clu_BrakeFluIDSW(data) do \
{ \
    Proxy_RxByComRxClu1Info.Cf_Clu_BrakeFluIDSW = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxMsgOkFlgInfo_Bms1MsgOkFlg(data) do \
{ \
    Proxy_RxByComRxMsgOkFlgInfo.Bms1MsgOkFlg = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxMsgOkFlgInfo_YawSerialMsgOkFlg(data) do \
{ \
    Proxy_RxByComRxMsgOkFlgInfo.YawSerialMsgOkFlg = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxMsgOkFlgInfo_YawAccMsgOkFlg(data) do \
{ \
    Proxy_RxByComRxMsgOkFlgInfo.YawAccMsgOkFlg = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxMsgOkFlgInfo_Tcu6MsgOkFlg(data) do \
{ \
    Proxy_RxByComRxMsgOkFlgInfo.Tcu6MsgOkFlg = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxMsgOkFlgInfo_Tcu5MsgOkFlg(data) do \
{ \
    Proxy_RxByComRxMsgOkFlgInfo.Tcu5MsgOkFlg = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxMsgOkFlgInfo_Tcu1MsgOkFlg(data) do \
{ \
    Proxy_RxByComRxMsgOkFlgInfo.Tcu1MsgOkFlg = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxMsgOkFlgInfo_SasMsgOkFlg(data) do \
{ \
    Proxy_RxByComRxMsgOkFlgInfo.SasMsgOkFlg = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxMsgOkFlgInfo_Mcu2MsgOkFlg(data) do \
{ \
    Proxy_RxByComRxMsgOkFlgInfo.Mcu2MsgOkFlg = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxMsgOkFlgInfo_Mcu1MsgOkFlg(data) do \
{ \
    Proxy_RxByComRxMsgOkFlgInfo.Mcu1MsgOkFlg = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxMsgOkFlgInfo_LongAccMsgOkFlg(data) do \
{ \
    Proxy_RxByComRxMsgOkFlgInfo.LongAccMsgOkFlg = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxMsgOkFlgInfo_Hcu5MsgOkFlg(data) do \
{ \
    Proxy_RxByComRxMsgOkFlgInfo.Hcu5MsgOkFlg = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxMsgOkFlgInfo_Hcu3MsgOkFlg(data) do \
{ \
    Proxy_RxByComRxMsgOkFlgInfo.Hcu3MsgOkFlg = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxMsgOkFlgInfo_Hcu2MsgOkFlg(data) do \
{ \
    Proxy_RxByComRxMsgOkFlgInfo.Hcu2MsgOkFlg = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxMsgOkFlgInfo_Hcu1MsgOkFlg(data) do \
{ \
    Proxy_RxByComRxMsgOkFlgInfo.Hcu1MsgOkFlg = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxMsgOkFlgInfo_Fatc1MsgOkFlg(data) do \
{ \
    Proxy_RxByComRxMsgOkFlgInfo.Fatc1MsgOkFlg = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxMsgOkFlgInfo_Ems3MsgOkFlg(data) do \
{ \
    Proxy_RxByComRxMsgOkFlgInfo.Ems3MsgOkFlg = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxMsgOkFlgInfo_Ems2MsgOkFlg(data) do \
{ \
    Proxy_RxByComRxMsgOkFlgInfo.Ems2MsgOkFlg = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxMsgOkFlgInfo_Ems1MsgOkFlg(data) do \
{ \
    Proxy_RxByComRxMsgOkFlgInfo.Ems1MsgOkFlg = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxMsgOkFlgInfo_Clu2MsgOkFlg(data) do \
{ \
    Proxy_RxByComRxMsgOkFlgInfo.Clu2MsgOkFlg = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRxMsgOkFlgInfo_Clu1MsgOkFlg(data) do \
{ \
    Proxy_RxByComRxMsgOkFlgInfo.Clu1MsgOkFlg = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo_ST_ECU_ST_REPAT_XTRQ_FTAX_BAX(data) do \
{ \
    Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.ST_ECU_ST_REPAT_XTRQ_FTAX_BAX = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo_ALIV_ST_REPAT_XTRQ_FTAX_BAX(data) do \
{ \
    Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.ALIV_ST_REPAT_XTRQ_FTAX_BAX = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo_CRC_ST_REPAT_XTRQ_FTAX_BAX(data) do \
{ \
    Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.CRC_ST_REPAT_XTRQ_FTAX_BAX = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo_QU_SER_REPAT_XTRQ_FTAX_BAX_ACT(data) do \
{ \
    Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.QU_SER_REPAT_XTRQ_FTAX_BAX_ACT = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo_AVL_REPAT_XTRQ_FTAX_BAX(data) do \
{ \
    Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.AVL_REPAT_XTRQ_FTAX_BAX = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo_QU_AVL_REPAT_XTRQ_FTAX_BAX(data) do \
{ \
    Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.QU_AVL_REPAT_XTRQ_FTAX_BAX = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComAVL_LTRQD_BAXInfo_QU_SER_LTRQD_BAX(data) do \
{ \
    Proxy_RxByComAVL_LTRQD_BAXInfo.QU_SER_LTRQD_BAX = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComAVL_LTRQD_BAXInfo_CRC_AVL_LTRQD_BAX(data) do \
{ \
    Proxy_RxByComAVL_LTRQD_BAXInfo.CRC_AVL_LTRQD_BAX = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComAVL_LTRQD_BAXInfo_QU_AVL_LTRQD_BAX(data) do \
{ \
    Proxy_RxByComAVL_LTRQD_BAXInfo.QU_AVL_LTRQD_BAX = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComAVL_LTRQD_BAXInfo_AVL_LTRQD_BAX(data) do \
{ \
    Proxy_RxByComAVL_LTRQD_BAXInfo.AVL_LTRQD_BAX = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComAVL_LTRQD_BAXInfo_ALIV_AVL_LTRQD_BAX(data) do \
{ \
    Proxy_RxByComAVL_LTRQD_BAXInfo.ALIV_AVL_LTRQD_BAX = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo_AVL_TORQ_CRSH_DMEE(data) do \
{ \
    Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.AVL_TORQ_CRSH_DMEE = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo_AVL_TORQ_CRSH(data) do \
{ \
    Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.AVL_TORQ_CRSH = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo_ST_SAIL_DRV_2(data) do \
{ \
    Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.ST_SAIL_DRV_2 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo_CRC_ANG_ACPD(data) do \
{ \
    Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.CRC_ANG_ACPD = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo_QU_AVL_RPM_ENG_CRSH(data) do \
{ \
    Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.QU_AVL_RPM_ENG_CRSH = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo_AVL_RPM_ENG_CRSH(data) do \
{ \
    Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.AVL_RPM_ENG_CRSH = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo_ALIV_TORQ_CRSH_1(data) do \
{ \
    Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.ALIV_TORQ_CRSH_1 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo_CRC_TORQ_CRSH_1(data) do \
{ \
    Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.CRC_TORQ_CRSH_1 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo_AVL_ANG_ACPD(data) do \
{ \
    Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.AVL_ANG_ACPD = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo_QU_AVL_ANG_ACPD(data) do \
{ \
    Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.QU_AVL_ANG_ACPD = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo_ALIV_ANG_ACPD(data) do \
{ \
    Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.ALIV_ANG_ACPD = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo_ST_ECU_ANG_ACPD(data) do \
{ \
    Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.ST_ECU_ANG_ACPD = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo_AVL_ANG_ACPD_VIRT(data) do \
{ \
    Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.AVL_ANG_ACPD_VIRT = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo_ECO_ANG_ACPD(data) do \
{ \
    Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.ECO_ANG_ACPD = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo_GRAD_AVL_ANG_ACPD(data) do \
{ \
    Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.GRAD_AVL_ANG_ACPD = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo_ST_INTF_DRASY(data) do \
{ \
    Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo.ST_INTF_DRASY = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info_AVL_RPM_BAX_RED(data) do \
{ \
    Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.AVL_RPM_BAX_RED = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info_ST_PENG_PT(data) do \
{ \
    Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.ST_PENG_PT = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info_ST_AVAI_INTV_PT_DRS(data) do \
{ \
    Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.ST_AVAI_INTV_PT_DRS = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info_QU_AVL_RPM_BAX_RED(data) do \
{ \
    Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.QU_AVL_RPM_BAX_RED = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info_QU_SER_WMOM_PT_SUM_DRS(data) do \
{ \
    Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.QU_SER_WMOM_PT_SUM_DRS = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info_QU_SER_WMOM_PT_SUM_STAB(data) do \
{ \
    Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.QU_SER_WMOM_PT_SUM_STAB = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info_ST_ECU_WMOM_DRV_5(data) do \
{ \
    Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.ST_ECU_WMOM_DRV_5 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info_TAR_WMOM_PT_SUM_COOTD(data) do \
{ \
    Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.TAR_WMOM_PT_SUM_COOTD = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info_QU_SER_COOR_TORQ_BDRV(data) do \
{ \
    Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.QU_SER_COOR_TORQ_BDRV = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info_ST_ECU_WMOM_DRV_4(data) do \
{ \
    Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.ST_ECU_WMOM_DRV_4 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info_ST_DRVDIR_DVCH(data) do \
{ \
    Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.ST_DRVDIR_DVCH = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info_CRC_WMOM_DRV_4(data) do \
{ \
    Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.CRC_WMOM_DRV_4 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info_ALIV_WMOM_DRV_4(data) do \
{ \
    Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.ALIV_WMOM_DRV_4 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info_ALIV_WMOM_DRV_5(data) do \
{ \
    Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.ALIV_WMOM_DRV_5 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info_CRC_WMOM_DRV_5(data) do \
{ \
    Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info.CRC_WMOM_DRV_5 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info_QU_AVL_WMOM_PT_SUM(data) do \
{ \
    Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.QU_AVL_WMOM_PT_SUM = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info_AVL_WMOM_PT_SUM_ERR_AMP(data) do \
{ \
    Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_ERR_AMP = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info_REIN_PT(data) do \
{ \
    Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.REIN_PT = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info_AVL_WMOM_PT_SUM(data) do \
{ \
    Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info_CRC_WMOM_DRV_1(data) do \
{ \
    Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.CRC_WMOM_DRV_1 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info_ALIV_WMOM_DRV_1(data) do \
{ \
    Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.ALIV_WMOM_DRV_1 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info_ST_ECU_WMOM_DRV_1(data) do \
{ \
    Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.ST_ECU_WMOM_DRV_1 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info_AVL_WMOM_PT_SUM_MAX(data) do \
{ \
    Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_MAX = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info_AVL_WMOM_PT_SUM_FAST_TOP(data) do \
{ \
    Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_FAST_TOP = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info_AVL_WMOM_PT_SUM_FAST_BOT(data) do \
{ \
    Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_FAST_BOT = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info_ST_ECU_WMOM_DRV_2(data) do \
{ \
    Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.ST_ECU_WMOM_DRV_2 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info_QU_REIN_PT(data) do \
{ \
    Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.QU_REIN_PT = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info_CRC_WMOM_DRV_2(data) do \
{ \
    Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.CRC_WMOM_DRV_2 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info_ALIV_WMOM_DRV_2(data) do \
{ \
    Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info.ALIV_WMOM_DRV_2 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComHGLV_VEH_FILTInfo_HGLV_VEH_FILT_FRH(data) do \
{ \
    Proxy_RxByComHGLV_VEH_FILTInfo.HGLV_VEH_FILT_FRH = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComHGLV_VEH_FILTInfo_HGLV_VEH_FILT_FLH(data) do \
{ \
    Proxy_RxByComHGLV_VEH_FILTInfo.HGLV_VEH_FILT_FLH = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComHGLV_VEH_FILTInfo_QU_HGLV_VEH_FILT_FRH(data) do \
{ \
    Proxy_RxByComHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_FRH = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComHGLV_VEH_FILTInfo_QU_HGLV_VEH_FILT_FLH(data) do \
{ \
    Proxy_RxByComHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_FLH = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComHGLV_VEH_FILTInfo_QU_HGLV_VEH_FILT_RRH(data) do \
{ \
    Proxy_RxByComHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_RRH = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComHGLV_VEH_FILTInfo_HGLV_VEH_FILT_RLH(data) do \
{ \
    Proxy_RxByComHGLV_VEH_FILTInfo.HGLV_VEH_FILT_RLH = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComHGLV_VEH_FILTInfo_ALIV_HGLV_VEH_FILT(data) do \
{ \
    Proxy_RxByComHGLV_VEH_FILTInfo.ALIV_HGLV_VEH_FILT = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComHGLV_VEH_FILTInfo_HGLV_VEH_FILT_RRH(data) do \
{ \
    Proxy_RxByComHGLV_VEH_FILTInfo.HGLV_VEH_FILT_RRH = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComHGLV_VEH_FILTInfo_QU_HGLV_VEH_FILT_RLH(data) do \
{ \
    Proxy_RxByComHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_RLH = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComHGLV_VEH_FILTInfo_CRC_HGLV_VEH_FILT(data) do \
{ \
    Proxy_RxByComHGLV_VEH_FILTInfo.CRC_HGLV_VEH_FILT = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo_ATTAV_ESTI(data) do \
{ \
    Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.ATTAV_ESTI = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo_ATTA_ESTI_ERR_AMP(data) do \
{ \
    Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.ATTA_ESTI_ERR_AMP = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo_VY_ESTI_ERR_AMP(data) do \
{ \
    Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.VY_ESTI_ERR_AMP = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo_VY_ESTI(data) do \
{ \
    Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.VY_ESTI = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo_ATTAV_ESTI_ERR_AMP(data) do \
{ \
    Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.ATTAV_ESTI_ERR_AMP = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo_CRC_VEH_DYNMC_DT_ESTI_VRFD(data) do \
{ \
    Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.CRC_VEH_DYNMC_DT_ESTI_VRFD = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo_ALIV_VEH_DYNMC_DT_ESTI_VRFD(data) do \
{ \
    Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.ALIV_VEH_DYNMC_DT_ESTI_VRFD = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo_QU_VEH_DYNMC_DT_ESTI(data) do \
{ \
    Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.QU_VEH_DYNMC_DT_ESTI = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo_ATTA_ESTI(data) do \
{ \
    Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo.ATTA_ESTI = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo_QU_ACLNY_COG(data) do \
{ \
    Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.QU_ACLNY_COG = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo_ACLNY_COG_ERR_AMP(data) do \
{ \
    Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNY_COG_ERR_AMP = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo_ALIV_ACLNX_COG(data) do \
{ \
    Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ALIV_ACLNX_COG = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo_ACLNX_COG(data) do \
{ \
    Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNX_COG = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo_ALIV_ACLNY_COG(data) do \
{ \
    Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ALIV_ACLNY_COG = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo_CRC_ACLNX_COG(data) do \
{ \
    Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.CRC_ACLNX_COG = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo_CRC_ACLNY_COG(data) do \
{ \
    Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.CRC_ACLNY_COG = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo_ACLNY_COG(data) do \
{ \
    Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNY_COG = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo_ACLNX_COG_ERR_AMP(data) do \
{ \
    Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNX_COG_ERR_AMP = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo_QU_ACLNX_COG(data) do \
{ \
    Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.QU_ACLNX_COG = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComV_VEH_V_VEH_2Info_CRC_V_VEH(data) do \
{ \
    Proxy_RxByComV_VEH_V_VEH_2Info.CRC_V_VEH = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComV_VEH_V_VEH_2Info_ALIV_V_VEH(data) do \
{ \
    Proxy_RxByComV_VEH_V_VEH_2Info.ALIV_V_VEH = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComV_VEH_V_VEH_2Info_QU_V_VEH_COG(data) do \
{ \
    Proxy_RxByComV_VEH_V_VEH_2Info.QU_V_VEH_COG = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComV_VEH_V_VEH_2Info_DVCO_VEH(data) do \
{ \
    Proxy_RxByComV_VEH_V_VEH_2Info.DVCO_VEH = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComV_VEH_V_VEH_2Info_ST_V_VEH_NSS(data) do \
{ \
    Proxy_RxByComV_VEH_V_VEH_2Info.ST_V_VEH_NSS = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComV_VEH_V_VEH_2Info_V_VEH_COG(data) do \
{ \
    Proxy_RxByComV_VEH_V_VEH_2Info.V_VEH_COG = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo_ALIV_VYAW_VEH(data) do \
{ \
    Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo.ALIV_VYAW_VEH = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo_VYAW_VEH_ERR_AMP(data) do \
{ \
    Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo.VYAW_VEH_ERR_AMP = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo_CRC_VYAW_VEH(data) do \
{ \
    Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo.CRC_VYAW_VEH = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo_QU_VYAW_VEH(data) do \
{ \
    Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo.QU_VYAW_VEH = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo_VYAW_VEH(data) do \
{ \
    Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo.VYAW_VEH = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo_QU_AVL_TRGR_RW(data) do \
{ \
    Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo.QU_AVL_TRGR_RW = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo_AVL_TRGR_RW(data) do \
{ \
    Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo.AVL_TRGR_RW = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo_AVL_LOGR_RW_FAST(data) do \
{ \
    Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo.AVL_LOGR_RW_FAST = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo_ALIV_TLT_RW(data) do \
{ \
    Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo.ALIV_TLT_RW = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo_CRC_TLT_RW(data) do \
{ \
    Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo.CRC_TLT_RW = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo_QU_AVL_LOGR_RW(data) do \
{ \
    Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo.QU_AVL_LOGR_RW = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo_AVL_LOGR_RW(data) do \
{ \
    Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo.AVL_LOGR_RW = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComAVL_STEA_FTAXInfo_QU_AVL_STEA_FTAX_PNI(data) do \
{ \
    Proxy_RxByComAVL_STEA_FTAXInfo.QU_AVL_STEA_FTAX_PNI = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComAVL_STEA_FTAXInfo_AVL_STEA_FTAX_PNI(data) do \
{ \
    Proxy_RxByComAVL_STEA_FTAXInfo.AVL_STEA_FTAX_PNI = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComAVL_STEA_FTAXInfo_CRC_AVL_STEA_FTAX(data) do \
{ \
    Proxy_RxByComAVL_STEA_FTAXInfo.CRC_AVL_STEA_FTAX = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComAVL_STEA_FTAXInfo_QU_AVL_STEA_FTAX_WHL(data) do \
{ \
    Proxy_RxByComAVL_STEA_FTAXInfo.QU_AVL_STEA_FTAX_WHL = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComAVL_STEA_FTAXInfo_ALIV_AVL_STEA_FTAX(data) do \
{ \
    Proxy_RxByComAVL_STEA_FTAXInfo.ALIV_AVL_STEA_FTAX = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComAVL_STEA_FTAXInfo_AVL_STEA_FTAX_WHL(data) do \
{ \
    Proxy_RxByComAVL_STEA_FTAXInfo.AVL_STEA_FTAX_WHL = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComAVL_STEA_FTAXInfo_AVL_STEA_FTAX_WHL_ERR_AMP(data) do \
{ \
    Proxy_RxByComAVL_STEA_FTAXInfo.AVL_STEA_FTAX_WHL_ERR_AMP = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_TAR_PRMSN_DBC_DCRN_MAX(data) do \
{ \
    Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_PRMSN_DBC_DCRN_MAX = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_QU_TAR_PRF_BRK(data) do \
{ \
    Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_PRF_BRK = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_QU_TAR_V_HDC(data) do \
{ \
    Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_V_HDC = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_TAR_PRMSN_DBC_DCRN_GRAD_MAX(data) do \
{ \
    Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_PRMSN_DBC_DCRN_GRAD_MAX = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_ALIV_SPEC_PRMSN_IBRK_HDC(data) do \
{ \
    Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.ALIV_SPEC_PRMSN_IBRK_HDC = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_CRC_SPEC_PRMSN_IBRK_HDC(data) do \
{ \
    Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.CRC_SPEC_PRMSN_IBRK_HDC = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_TAR_PRMSN_DBC_TR_THRV(data) do \
{ \
    Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_PRMSN_DBC_TR_THRV = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_TAR_BRTORQ_SUM(data) do \
{ \
    Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_BRTORQ_SUM = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_QU_TAR_BRTORQ_SUM(data) do \
{ \
    Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_BRTORQ_SUM = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_QU_TAR_PRMSN_DBC(data) do \
{ \
    Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_PRMSN_DBC = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_RQ_TAO_SSM(data) do \
{ \
    Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.RQ_TAO_SSM = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_CRC_RQ_BRTORQ_SUM(data) do \
{ \
    Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.CRC_RQ_BRTORQ_SUM = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_ALIV_RQ_BRTORQ_SUM(data) do \
{ \
    Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.ALIV_RQ_BRTORQ_SUM = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info_AVL_WMOM_PT_SUM_DTORQ_BOT(data) do \
{ \
    Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_DTORQ_BOT = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info_AVL_WMOM_PT_SUM_RECUP_MAX(data) do \
{ \
    Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_RECUP_MAX = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info_AVL_WMOM_PT_SUM_ILS(data) do \
{ \
    Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_ILS = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info_CRC_WMOM_DRV_3(data) do \
{ \
    Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.CRC_WMOM_DRV_3 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info_ALIV_WMOM_DRV_3(data) do \
{ \
    Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.ALIV_WMOM_DRV_3 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info_AVL_WMOM_PT_SUM_DTORQ_TOP(data) do \
{ \
    Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_DTORQ_TOP = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info_ST_ECU_WMOM_DRV_6(data) do \
{ \
    Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.ST_ECU_WMOM_DRV_6 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info_ALIV_WMOM_DRV_6(data) do \
{ \
    Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.ALIV_WMOM_DRV_6 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info_CRC_WMOM_DRV_6(data) do \
{ \
    Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info.CRC_WMOM_DRV_6 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_7Info_ST_EL_DRVG(data) do \
{ \
    Proxy_RxByComWMOM_DRV_7Info.ST_EL_DRVG = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_7Info_PRD_AVL_WMOM_PT_SUM_RECUP_MAX(data) do \
{ \
    Proxy_RxByComWMOM_DRV_7Info.PRD_AVL_WMOM_PT_SUM_RECUP_MAX = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_7Info_ALIV_WMOM_DRV_7(data) do \
{ \
    Proxy_RxByComWMOM_DRV_7Info.ALIV_WMOM_DRV_7 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_7Info_CRC_WMOM_DRV_7(data) do \
{ \
    Proxy_RxByComWMOM_DRV_7Info.CRC_WMOM_DRV_7 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_7Info_ST_ECU_WMOM_DRV_7(data) do \
{ \
    Proxy_RxByComWMOM_DRV_7Info.ST_ECU_WMOM_DRV_7 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWMOM_DRV_7Info_QU_SER_WMOM_PT_SUM_RECUP(data) do \
{ \
    Proxy_RxByComWMOM_DRV_7Info.QU_SER_WMOM_PT_SUM_RECUP = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo_TAR_DIFF_BRTORQ_BAX_YMR(data) do \
{ \
    Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.TAR_DIFF_BRTORQ_BAX_YMR = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo_FACT_TAR_COMPT_DRV_YMR(data) do \
{ \
    Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.FACT_TAR_COMPT_DRV_YMR = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo_QU_TAR_DIFF_BRTORQ_YMR(data) do \
{ \
    Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.QU_TAR_DIFF_BRTORQ_YMR = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo_CRC_RQ_DIFF_BRTORQ_YMR(data) do \
{ \
    Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.CRC_RQ_DIFF_BRTORQ_YMR = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo_ALIV_RQ_DIFF_BRTORQ_YMR(data) do \
{ \
    Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.ALIV_RQ_DIFF_BRTORQ_YMR = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo_TAR_DIFF_BRTORQ_FTAX_YMR(data) do \
{ \
    Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.TAR_DIFF_BRTORQ_FTAX_YMR = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_BRKSYS_ENGMGInfo_QU_AVL_LOWP_BRKFA(data) do \
{ \
    Proxy_RxByComDT_BRKSYS_ENGMGInfo.QU_AVL_LOWP_BRKFA = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_BRKSYS_ENGMGInfo_AVL_LOWP_BRKFA(data) do \
{ \
    Proxy_RxByComDT_BRKSYS_ENGMGInfo.AVL_LOWP_BRKFA = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_BRKSYS_ENGMGInfo_CRC_DT_BRKSYS_ENGMG(data) do \
{ \
    Proxy_RxByComDT_BRKSYS_ENGMGInfo.CRC_DT_BRKSYS_ENGMG = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_BRKSYS_ENGMGInfo_ALIV_DT_BRKSYS_ENGMG(data) do \
{ \
    Proxy_RxByComDT_BRKSYS_ENGMGInfo.ALIV_DT_BRKSYS_ENGMG = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComERRM_BN_UInfo_CTR_ERRM_BN_U(data) do \
{ \
    Proxy_RxByComERRM_BN_UInfo.CTR_ERRM_BN_U = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComCTR_CRInfo_ALIV_CTR_CR(data) do \
{ \
    Proxy_RxByComCTR_CRInfo.ALIV_CTR_CR = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComCTR_CRInfo_ST_EXCE_ACLN_THRV(data) do \
{ \
    Proxy_RxByComCTR_CRInfo.ST_EXCE_ACLN_THRV = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComCTR_CRInfo_CTR_PHTR_CR(data) do \
{ \
    Proxy_RxByComCTR_CRInfo.CTR_PHTR_CR = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComCTR_CRInfo_CTR_ITLI_CR(data) do \
{ \
    Proxy_RxByComCTR_CRInfo.CTR_ITLI_CR = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComCTR_CRInfo_CTR_CLSY_CR(data) do \
{ \
    Proxy_RxByComCTR_CRInfo.CTR_CLSY_CR = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComCTR_CRInfo_CTR_AUTOM_ECAL_CR(data) do \
{ \
    Proxy_RxByComCTR_CRInfo.CTR_AUTOM_ECAL_CR = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComCTR_CRInfo_CTR_SWO_EKP_CR(data) do \
{ \
    Proxy_RxByComCTR_CRInfo.CTR_SWO_EKP_CR = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComCTR_CRInfo_CRC_CTR_CR(data) do \
{ \
    Proxy_RxByComCTR_CRInfo.CRC_CTR_CR = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComCTR_CRInfo_CTR_PCSH_MST(data) do \
{ \
    Proxy_RxByComCTR_CRInfo.CTR_PCSH_MST = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComCTR_CRInfo_CTR_HAZW_CR(data) do \
{ \
    Proxy_RxByComCTR_CRInfo.CTR_HAZW_CR = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComKLEMMENInfo_ST_OP_MSA(data) do \
{ \
    Proxy_RxByComKLEMMENInfo.ST_OP_MSA = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComKLEMMENInfo_RQ_DRVG_RDI(data) do \
{ \
    Proxy_RxByComKLEMMENInfo.RQ_DRVG_RDI = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComKLEMMENInfo_ST_KL_DBG(data) do \
{ \
    Proxy_RxByComKLEMMENInfo.ST_KL_DBG = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComKLEMMENInfo_ST_KL_50_MSA(data) do \
{ \
    Proxy_RxByComKLEMMENInfo.ST_KL_50_MSA = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComKLEMMENInfo_ST_SSP(data) do \
{ \
    Proxy_RxByComKLEMMENInfo.ST_SSP = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComKLEMMENInfo_RWDT_BLS(data) do \
{ \
    Proxy_RxByComKLEMMENInfo.RWDT_BLS = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComKLEMMENInfo_ST_STCD_PENG(data) do \
{ \
    Proxy_RxByComKLEMMENInfo.ST_STCD_PENG = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComKLEMMENInfo_ST_KL(data) do \
{ \
    Proxy_RxByComKLEMMENInfo.ST_KL = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComKLEMMENInfo_ST_KL_DIV(data) do \
{ \
    Proxy_RxByComKLEMMENInfo.ST_KL_DIV = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComKLEMMENInfo_ST_VEH_CON(data) do \
{ \
    Proxy_RxByComKLEMMENInfo.ST_VEH_CON = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComKLEMMENInfo_CRC_KL(data) do \
{ \
    Proxy_RxByComKLEMMENInfo.CRC_KL = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComKLEMMENInfo_ALIV_COU_KL(data) do \
{ \
    Proxy_RxByComKLEMMENInfo.ALIV_COU_KL = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComKLEMMENInfo_ST_KL_30B(data) do \
{ \
    Proxy_RxByComKLEMMENInfo.ST_KL_30B = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComKLEMMENInfo_CON_CLT_SW(data) do \
{ \
    Proxy_RxByComKLEMMENInfo.CON_CLT_SW = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComKLEMMENInfo_CTR_ENG_STOP(data) do \
{ \
    Proxy_RxByComKLEMMENInfo.CTR_ENG_STOP = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComKLEMMENInfo_ST_PLK(data) do \
{ \
    Proxy_RxByComKLEMMENInfo.ST_PLK = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComKLEMMENInfo_ST_KL_15N(data) do \
{ \
    Proxy_RxByComKLEMMENInfo.ST_KL_15N = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComKLEMMENInfo_ST_KL_KEY_VLD(data) do \
{ \
    Proxy_RxByComKLEMMENInfo.ST_KL_KEY_VLD = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_RDC_MES_TSTMP(data) do \
{ \
    Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_MES_TSTMP = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_PCKG_ID(data) do \
{ \
    Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.PCKG_ID = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_SUPP_ID(data) do \
{ \
    Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.SUPP_ID = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_RDC_DT_5(data) do \
{ \
    Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_5 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_RDC_DT_8(data) do \
{ \
    Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_8 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_RDC_DT_7(data) do \
{ \
    Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_7 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_RDC_DT_6(data) do \
{ \
    Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_6 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_ALIV_RDC_DT_PCKG_2(data) do \
{ \
    Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.ALIV_RDC_DT_PCKG_2 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_RDC_DT_1(data) do \
{ \
    Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_1 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_TYR_ID(data) do \
{ \
    Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.TYR_ID = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_ALIV_RDC_DT_PCKG_1(data) do \
{ \
    Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.ALIV_RDC_DT_PCKG_1 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_RDC_DT_4(data) do \
{ \
    Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_4 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_RDC_DT_3(data) do \
{ \
    Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_3 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_RDC_DT_2(data) do \
{ \
    Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_2 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComBEDIENUNG_WISCHERInfo_ALIV_WISW(data) do \
{ \
    Proxy_RxByComBEDIENUNG_WISCHERInfo.ALIV_WISW = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComBEDIENUNG_WISCHERInfo_OP_WISW(data) do \
{ \
    Proxy_RxByComBEDIENUNG_WISCHERInfo.OP_WISW = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComBEDIENUNG_WISCHERInfo_OP_WIPO(data) do \
{ \
    Proxy_RxByComBEDIENUNG_WISCHERInfo.OP_WIPO = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_PT_2Info_ST_GRSEL_DRV(data) do \
{ \
    Proxy_RxByComDT_PT_2Info.ST_GRSEL_DRV = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_PT_2Info_TEMP_EOI_DRV(data) do \
{ \
    Proxy_RxByComDT_PT_2Info.TEMP_EOI_DRV = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_PT_2Info_RLS_ENGSTA(data) do \
{ \
    Proxy_RxByComDT_PT_2Info.RLS_ENGSTA = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_PT_2Info_RPM_ENG_MAX_ALW(data) do \
{ \
    Proxy_RxByComDT_PT_2Info.RPM_ENG_MAX_ALW = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_PT_2Info_RDUC_DOCTR_RPM_DRV_2(data) do \
{ \
    Proxy_RxByComDT_PT_2Info.RDUC_DOCTR_RPM_DRV_2 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_PT_2Info_TEMP_ENG_DRV(data) do \
{ \
    Proxy_RxByComDT_PT_2Info.TEMP_ENG_DRV = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_PT_2Info_ST_DRV_VEH(data) do \
{ \
    Proxy_RxByComDT_PT_2Info.ST_DRV_VEH = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_PT_2Info_ST_ECU_DT_PT_2(data) do \
{ \
    Proxy_RxByComDT_PT_2Info.ST_ECU_DT_PT_2 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_PT_2Info_ST_IDLG_ENG_DRV(data) do \
{ \
    Proxy_RxByComDT_PT_2Info.ST_IDLG_ENG_DRV = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_PT_2Info_ST_ILK_STRT_DRV(data) do \
{ \
    Proxy_RxByComDT_PT_2Info.ST_ILK_STRT_DRV = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_PT_2Info_ST_SW_CLT_DRV(data) do \
{ \
    Proxy_RxByComDT_PT_2Info.ST_SW_CLT_DRV = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_PT_2Info_CRC_DT_PT_2(data) do \
{ \
    Proxy_RxByComDT_PT_2Info.CRC_DT_PT_2 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_PT_2Info_ALIV_DT_PT_2(data) do \
{ \
    Proxy_RxByComDT_PT_2Info.ALIV_DT_PT_2 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_PT_2Info_ST_ENG_RUN_DRV(data) do \
{ \
    Proxy_RxByComDT_PT_2Info.ST_ENG_RUN_DRV = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_ENG_STA_AUTOInfo_RQ_MSA_ENG_STA_1(data) do \
{ \
    Proxy_RxByComSTAT_ENG_STA_AUTOInfo.RQ_MSA_ENG_STA_1 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_ENG_STA_AUTOInfo_RQ_MSA_ENG_STA_2(data) do \
{ \
    Proxy_RxByComSTAT_ENG_STA_AUTOInfo.RQ_MSA_ENG_STA_2 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_ENG_STA_AUTOInfo_ALIV_ST_ENG_STA_AUTO(data) do \
{ \
    Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ALIV_ST_ENG_STA_AUTO = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_ENG_STA_AUTOInfo_PSBTY_MSA_ENG_STOP_STA(data) do \
{ \
    Proxy_RxByComSTAT_ENG_STA_AUTOInfo.PSBTY_MSA_ENG_STOP_STA = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_ENG_STA_AUTOInfo_CRC_ST_ENG_STA_AUTO(data) do \
{ \
    Proxy_RxByComSTAT_ENG_STA_AUTOInfo.CRC_ST_ENG_STA_AUTO = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_ENG_STA_AUTOInfo_ST_SHFT_MSA_ENGSTP(data) do \
{ \
    Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_SHFT_MSA_ENGSTP = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_ENG_STA_AUTOInfo_RQ_SLIP_K0(data) do \
{ \
    Proxy_RxByComSTAT_ENG_STA_AUTOInfo.RQ_SLIP_K0 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_ENG_STA_AUTOInfo_ST_TAR_PENG_CENG(data) do \
{ \
    Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_TAR_PENG_CENG = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_ENG_STA_AUTOInfo_SPEC_TYP_ENGSTA(data) do \
{ \
    Proxy_RxByComSTAT_ENG_STA_AUTOInfo.SPEC_TYP_ENGSTA = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_ENG_STA_AUTOInfo_ST_RPM_CLCTR_MOT(data) do \
{ \
    Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_RPM_CLCTR_MOT = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_ENG_STA_AUTOInfo_ST_FN_MSA(data) do \
{ \
    Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_FN_MSA = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_ENG_STA_AUTOInfo_ST_AVL_PENG_CENG_ENGMG(data) do \
{ \
    Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_AVL_PENG_CENG_ENGMG = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_ENG_STA_AUTOInfo_DISP_REAS_PREV_SWO_CENG(data) do \
{ \
    Proxy_RxByComSTAT_ENG_STA_AUTOInfo.DISP_REAS_PREV_SWO_CENG = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_ENG_STA_AUTOInfo_VARI_TYP_ENGSTA(data) do \
{ \
    Proxy_RxByComSTAT_ENG_STA_AUTOInfo.VARI_TYP_ENGSTA = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_ENG_STA_AUTOInfo_ST_TAR_CENG(data) do \
{ \
    Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_TAR_CENG = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_ENG_STA_AUTOInfo_ST_AVAI_SAIL_DME(data) do \
{ \
    Proxy_RxByComSTAT_ENG_STA_AUTOInfo.ST_AVAI_SAIL_DME = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComST_ENERG_GENInfo_ST_LDST_GEN_DRV(data) do \
{ \
    Proxy_RxByComST_ENERG_GENInfo.ST_LDST_GEN_DRV = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComST_ENERG_GENInfo_DT_PCU_SCP(data) do \
{ \
    Proxy_RxByComST_ENERG_GENInfo.DT_PCU_SCP = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComST_ENERG_GENInfo_ST_GEN_DRV(data) do \
{ \
    Proxy_RxByComST_ENERG_GENInfo.ST_GEN_DRV = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComST_ENERG_GENInfo_AVL_I_GEN_DRV(data) do \
{ \
    Proxy_RxByComST_ENERG_GENInfo.AVL_I_GEN_DRV = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComST_ENERG_GENInfo_LDST_GEN_DRV(data) do \
{ \
    Proxy_RxByComST_ENERG_GENInfo.LDST_GEN_DRV = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComST_ENERG_GENInfo_ST_LDREL_GEN(data) do \
{ \
    Proxy_RxByComST_ENERG_GENInfo.ST_LDREL_GEN = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComST_ENERG_GENInfo_ST_CHG_STOR(data) do \
{ \
    Proxy_RxByComST_ENERG_GENInfo.ST_CHG_STOR = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComST_ENERG_GENInfo_TEMP_BT_14V(data) do \
{ \
    Proxy_RxByComST_ENERG_GENInfo.TEMP_BT_14V = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComST_ENERG_GENInfo_ST_I_IBS(data) do \
{ \
    Proxy_RxByComST_ENERG_GENInfo.ST_I_IBS = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComST_ENERG_GENInfo_ST_SEP_STOR(data) do \
{ \
    Proxy_RxByComST_ENERG_GENInfo.ST_SEP_STOR = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComST_ENERG_GENInfo_ST_BN2_SCP(data) do \
{ \
    Proxy_RxByComST_ENERG_GENInfo.ST_BN2_SCP = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_PT_1Info_RQ_SHPA_GRB_REGE_PAFI(data) do \
{ \
    Proxy_RxByComDT_PT_1Info.RQ_SHPA_GRB_REGE_PAFI = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_PT_1Info_ST_RTIR_DRV(data) do \
{ \
    Proxy_RxByComDT_PT_1Info.ST_RTIR_DRV = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_PT_1Info_CTR_SLCK_DRV(data) do \
{ \
    Proxy_RxByComDT_PT_1Info.CTR_SLCK_DRV = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_PT_1Info_RQ_STASS_ENGMG(data) do \
{ \
    Proxy_RxByComDT_PT_1Info.RQ_STASS_ENGMG = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_PT_1Info_ST_CAT_HT(data) do \
{ \
    Proxy_RxByComDT_PT_1Info.ST_CAT_HT = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_PT_1Info_RQ_SHPA_GRB_CHGBLC(data) do \
{ \
    Proxy_RxByComDT_PT_1Info.RQ_SHPA_GRB_CHGBLC = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_PT_1Info_TAR_RPM_IDLG_DRV_EXT(data) do \
{ \
    Proxy_RxByComDT_PT_1Info.TAR_RPM_IDLG_DRV_EXT = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_PT_1Info_SLCTN_BUS_COMM_ENG_GRB(data) do \
{ \
    Proxy_RxByComDT_PT_1Info.SLCTN_BUS_COMM_ENG_GRB = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_PT_1Info_TAR_RPM_IDLG_DRV(data) do \
{ \
    Proxy_RxByComDT_PT_1Info.TAR_RPM_IDLG_DRV = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_PT_1Info_ST_INFS_DRV(data) do \
{ \
    Proxy_RxByComDT_PT_1Info.ST_INFS_DRV = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_PT_1Info_ST_SW_WAUP_DRV(data) do \
{ \
    Proxy_RxByComDT_PT_1Info.ST_SW_WAUP_DRV = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_PT_1Info_AIP_ENG_DRV(data) do \
{ \
    Proxy_RxByComDT_PT_1Info.AIP_ENG_DRV = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_PT_1Info_RDUC_DOCTR_RPM_DRV(data) do \
{ \
    Proxy_RxByComDT_PT_1Info.RDUC_DOCTR_RPM_DRV = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_GRDT_DRVInfo_ST_OPMO_GRDT_DRV(data) do \
{ \
    Proxy_RxByComDT_GRDT_DRVInfo.ST_OPMO_GRDT_DRV = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_GRDT_DRVInfo_ST_RSTA_GRDT(data) do \
{ \
    Proxy_RxByComDT_GRDT_DRVInfo.ST_RSTA_GRDT = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_GRDT_DRVInfo_CRC_GRDT_DRV(data) do \
{ \
    Proxy_RxByComDT_GRDT_DRVInfo.CRC_GRDT_DRV = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_GRDT_DRVInfo_ALIV_GRDT_DRV(data) do \
{ \
    Proxy_RxByComDT_GRDT_DRVInfo.ALIV_GRDT_DRV = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDIAG_OBD_ENGMG_ELInfo_RQ_MIL_DIAG_OBD_ENGMG_EL(data) do \
{ \
    Proxy_RxByComDIAG_OBD_ENGMG_ELInfo.RQ_MIL_DIAG_OBD_ENGMG_EL = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDIAG_OBD_ENGMG_ELInfo_RQ_RST_OBD_DIAG(data) do \
{ \
    Proxy_RxByComDIAG_OBD_ENGMG_ELInfo.RQ_RST_OBD_DIAG = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_CT_HABRInfo_ST_CT_HABR(data) do \
{ \
    Proxy_RxByComSTAT_CT_HABRInfo.ST_CT_HABR = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_PT_3Info_TRNRAO_BAX(data) do \
{ \
    Proxy_RxByComDT_PT_3Info.TRNRAO_BAX = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_PT_3Info_QU_TRNRAO_BAX(data) do \
{ \
    Proxy_RxByComDT_PT_3Info.QU_TRNRAO_BAX = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComDT_PT_3Info_ALIV_DT_PT_3(data) do \
{ \
    Proxy_RxByComDT_PT_3Info.ALIV_DT_PT_3 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComEINHEITEN_BN2020Info_UN_TORQ_S_MOD(data) do \
{ \
    Proxy_RxByComEINHEITEN_BN2020Info.UN_TORQ_S_MOD = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComEINHEITEN_BN2020Info_UN_PWR_S_MOD(data) do \
{ \
    Proxy_RxByComEINHEITEN_BN2020Info.UN_PWR_S_MOD = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComEINHEITEN_BN2020Info_UN_DATE_EXT(data) do \
{ \
    Proxy_RxByComEINHEITEN_BN2020Info.UN_DATE_EXT = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComEINHEITEN_BN2020Info_UN_COSP_EL(data) do \
{ \
    Proxy_RxByComEINHEITEN_BN2020Info.UN_COSP_EL = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComEINHEITEN_BN2020Info_UN_TEMP(data) do \
{ \
    Proxy_RxByComEINHEITEN_BN2020Info.UN_TEMP = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComEINHEITEN_BN2020Info_UN_AIP(data) do \
{ \
    Proxy_RxByComEINHEITEN_BN2020Info.UN_AIP = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComEINHEITEN_BN2020Info_LANG(data) do \
{ \
    Proxy_RxByComEINHEITEN_BN2020Info.LANG = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComEINHEITEN_BN2020Info_UN_DATE(data) do \
{ \
    Proxy_RxByComEINHEITEN_BN2020Info.UN_DATE = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComEINHEITEN_BN2020Info_UN_T(data) do \
{ \
    Proxy_RxByComEINHEITEN_BN2020Info.UN_T = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComEINHEITEN_BN2020Info_UN_SPDM_DGTL(data) do \
{ \
    Proxy_RxByComEINHEITEN_BN2020Info.UN_SPDM_DGTL = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComEINHEITEN_BN2020Info_UN_MILE(data) do \
{ \
    Proxy_RxByComEINHEITEN_BN2020Info.UN_MILE = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComEINHEITEN_BN2020Info_UN_FU(data) do \
{ \
    Proxy_RxByComEINHEITEN_BN2020Info.UN_FU = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComEINHEITEN_BN2020Info_UN_COSP(data) do \
{ \
    Proxy_RxByComEINHEITEN_BN2020Info.UN_COSP = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComA_TEMPInfo_TEMP_EX_UNFILT(data) do \
{ \
    Proxy_RxByComA_TEMPInfo.TEMP_EX_UNFILT = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComA_TEMPInfo_TEMP_EX(data) do \
{ \
    Proxy_RxByComA_TEMPInfo.TEMP_EX = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWISCHERGESCHWINDIGKEITInfo_ST_RNSE(data) do \
{ \
    Proxy_RxByComWISCHERGESCHWINDIGKEITInfo.ST_RNSE = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWISCHERGESCHWINDIGKEITInfo_INT_RN(data) do \
{ \
    Proxy_RxByComWISCHERGESCHWINDIGKEITInfo.INT_RN = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComWISCHERGESCHWINDIGKEITInfo_V_WI(data) do \
{ \
    Proxy_RxByComWISCHERGESCHWINDIGKEITInfo.V_WI = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_DS_VRFDInfo_ST_DSW_DRD_SFY_CTRL(data) do \
{ \
    Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_DRD_SFY_CTRL = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_DS_VRFDInfo_ST_DSW_DVDR_VRFD(data) do \
{ \
    Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_DVDR_VRFD = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_DS_VRFDInfo_ST_DSW_DVDR_SFY_CTRL(data) do \
{ \
    Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_DVDR_SFY_CTRL = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_DS_VRFDInfo_ST_DSW_PSDR_SFY_CTRL(data) do \
{ \
    Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_PSDR_SFY_CTRL = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_DS_VRFDInfo_ST_DSW_PSD_SFY_CTRL(data) do \
{ \
    Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_PSD_SFY_CTRL = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_DS_VRFDInfo_ALIV_ST_DSW_VRFD(data) do \
{ \
    Proxy_RxByComSTAT_DS_VRFDInfo.ALIV_ST_DSW_VRFD = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_DS_VRFDInfo_CRC_ST_DSW_VRFD(data) do \
{ \
    Proxy_RxByComSTAT_DS_VRFDInfo.CRC_ST_DSW_VRFD = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_DS_VRFDInfo_ST_DSW_DRD_VRFD(data) do \
{ \
    Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_DRD_VRFD = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_DS_VRFDInfo_ST_DSW_PSDR_VRFD(data) do \
{ \
    Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_PSDR_VRFD = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_DS_VRFDInfo_ST_DSW_PSD_VRFD(data) do \
{ \
    Proxy_RxByComSTAT_DS_VRFDInfo.ST_DSW_PSD_VRFD = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info_AVL_MOD_SW_DRDY(data) do \
{ \
    Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info_RQ_SW_DRDY_MMID(data) do \
{ \
    Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.RQ_SW_DRDY_MMID = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info_RQ_SU_SW_DRDY(data) do \
{ \
    Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.RQ_SU_SW_DRDY = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info_CRC_SU_SW_DRDY(data) do \
{ \
    Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.CRC_SU_SW_DRDY = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info_ALIV_SU_SW_DRDY(data) do \
{ \
    Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.ALIV_SU_SW_DRDY = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info_RQ_SW_DRDY_KDIS(data) do \
{ \
    Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.RQ_SW_DRDY_KDIS = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info_AVL_MOD_SW_DRDY_CHAS(data) do \
{ \
    Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY_CHAS = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info_AVL_MOD_SW_DRDY_DRV(data) do \
{ \
    Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY_DRV = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info_AVL_MOD_SW_DRDY_STAB(data) do \
{ \
    Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY_STAB = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info_DISP_ST_DSC(data) do \
{ \
    Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info.DISP_ST_DSC = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_ANHAENGERInfo_ST_SYNCN_HAZWCL_TRAI(data) do \
{ \
    Proxy_RxByComSTAT_ANHAENGERInfo.ST_SYNCN_HAZWCL_TRAI = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_ANHAENGERInfo_ST_RFLI_DF_TRAI(data) do \
{ \
    Proxy_RxByComSTAT_ANHAENGERInfo.ST_RFLI_DF_TRAI = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_ANHAENGERInfo_ST_TRAI(data) do \
{ \
    Proxy_RxByComSTAT_ANHAENGERInfo.ST_TRAI = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_ANHAENGERInfo_ST_DI_DF_TRAI(data) do \
{ \
    Proxy_RxByComSTAT_ANHAENGERInfo.ST_DI_DF_TRAI = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComSTAT_ANHAENGERInfo_ST_PO_AHV(data) do \
{ \
    Proxy_RxByComSTAT_ANHAENGERInfo.ST_PO_AHV = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComFZZSTDInfo_ST_ILK_ERRM_FZM(data) do \
{ \
    Proxy_RxByComFZZSTDInfo.ST_ILK_ERRM_FZM = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComFZZSTDInfo_ST_ENERG_FZM(data) do \
{ \
    Proxy_RxByComFZZSTDInfo.ST_ENERG_FZM = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComFZZSTDInfo_ST_BT_PROTE_WUP(data) do \
{ \
    Proxy_RxByComFZZSTDInfo.ST_BT_PROTE_WUP = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComBEDIENUNG_FAHRWERKInfo_OP_MOD_TRCT_DSC(data) do \
{ \
    Proxy_RxByComBEDIENUNG_FAHRWERKInfo.OP_MOD_TRCT_DSC = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComBEDIENUNG_FAHRWERKInfo_OP_TPCT(data) do \
{ \
    Proxy_RxByComBEDIENUNG_FAHRWERKInfo.OP_TPCT = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComFAHRZEUGTYPInfo_QUAN_CYL(data) do \
{ \
    Proxy_RxByComFAHRZEUGTYPInfo.QUAN_CYL = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComFAHRZEUGTYPInfo_QUAN_GR(data) do \
{ \
    Proxy_RxByComFAHRZEUGTYPInfo.QUAN_GR = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComFAHRZEUGTYPInfo_TYP_VEH(data) do \
{ \
    Proxy_RxByComFAHRZEUGTYPInfo.TYP_VEH = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComFAHRZEUGTYPInfo_TYP_BODY(data) do \
{ \
    Proxy_RxByComFAHRZEUGTYPInfo.TYP_BODY = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComFAHRZEUGTYPInfo_TYP_ENG(data) do \
{ \
    Proxy_RxByComFAHRZEUGTYPInfo.TYP_ENG = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComFAHRZEUGTYPInfo_CLAS_PWR(data) do \
{ \
    Proxy_RxByComFAHRZEUGTYPInfo.CLAS_PWR = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComFAHRZEUGTYPInfo_TYP_CNT(data) do \
{ \
    Proxy_RxByComFAHRZEUGTYPInfo.TYP_CNT = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComFAHRZEUGTYPInfo_TYP_STE(data) do \
{ \
    Proxy_RxByComFAHRZEUGTYPInfo.TYP_STE = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComFAHRZEUGTYPInfo_TYP_GRB(data) do \
{ \
    Proxy_RxByComFAHRZEUGTYPInfo.TYP_GRB = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComFAHRZEUGTYPInfo_TYP_CAPA(data) do \
{ \
    Proxy_RxByComFAHRZEUGTYPInfo.TYP_CAPA = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComST_BLT_CT_SOCCUInfo_ST_SEAT_OCCU_RRH(data) do \
{ \
    Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_RRH = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComST_BLT_CT_SOCCUInfo_ST_BLTB_SW_RM(data) do \
{ \
    Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_BLTB_SW_RM = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComST_BLT_CT_SOCCUInfo_ST_BLTB_SW_RRH(data) do \
{ \
    Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_BLTB_SW_RRH = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComST_BLT_CT_SOCCUInfo_ST_ERR_SEAT_MT_DR(data) do \
{ \
    Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_ERR_SEAT_MT_DR = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComST_BLT_CT_SOCCUInfo_ST_BLTB_SW_DR(data) do \
{ \
    Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_BLTB_SW_DR = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComST_BLT_CT_SOCCUInfo_CRC_ST_BLT_CT_SOCCU(data) do \
{ \
    Proxy_RxByComST_BLT_CT_SOCCUInfo.CRC_ST_BLT_CT_SOCCU = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComST_BLT_CT_SOCCUInfo_ALIV_COU_ST_BLT_CT_SOCCU(data) do \
{ \
    Proxy_RxByComST_BLT_CT_SOCCUInfo.ALIV_COU_ST_BLT_CT_SOCCU = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComST_BLT_CT_SOCCUInfo_ST_SEAT_OCCU_DR(data) do \
{ \
    Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_DR = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComST_BLT_CT_SOCCUInfo_ST_BLTB_SW_RLH(data) do \
{ \
    Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_BLTB_SW_RLH = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComST_BLT_CT_SOCCUInfo_ST_SEAT_OCCU_RLH(data) do \
{ \
    Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_RLH = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComST_BLT_CT_SOCCUInfo_ST_BLTB_SW_PS(data) do \
{ \
    Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_BLTB_SW_PS = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComST_BLT_CT_SOCCUInfo_ST_SEAT_OCCU_PS(data) do \
{ \
    Proxy_RxByComST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_PS = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComFAHRGESTELLNUMMERInfo_NO_VECH_1(data) do \
{ \
    Proxy_RxByComFAHRGESTELLNUMMERInfo.NO_VECH_1 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComFAHRGESTELLNUMMERInfo_NO_VECH_2(data) do \
{ \
    Proxy_RxByComFAHRGESTELLNUMMERInfo.NO_VECH_2 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComFAHRGESTELLNUMMERInfo_NO_VECH_3(data) do \
{ \
    Proxy_RxByComFAHRGESTELLNUMMERInfo.NO_VECH_3 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComFAHRGESTELLNUMMERInfo_NO_VECH_6(data) do \
{ \
    Proxy_RxByComFAHRGESTELLNUMMERInfo.NO_VECH_6 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComFAHRGESTELLNUMMERInfo_NO_VECH_7(data) do \
{ \
    Proxy_RxByComFAHRGESTELLNUMMERInfo.NO_VECH_7 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComFAHRGESTELLNUMMERInfo_NO_VECH_4(data) do \
{ \
    Proxy_RxByComFAHRGESTELLNUMMERInfo.NO_VECH_4 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComFAHRGESTELLNUMMERInfo_NO_VECH_5(data) do \
{ \
    Proxy_RxByComFAHRGESTELLNUMMERInfo.NO_VECH_5 = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRELATIVZEITInfo_T_SEC_COU_REL(data) do \
{ \
    Proxy_RxByComRELATIVZEITInfo.T_SEC_COU_REL = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComRELATIVZEITInfo_T_DAY_COU_ABSL(data) do \
{ \
    Proxy_RxByComRELATIVZEITInfo.T_DAY_COU_ABSL = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComKILOMETERSTANDInfo_RNG(data) do \
{ \
    Proxy_RxByComKILOMETERSTANDInfo.RNG = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComKILOMETERSTANDInfo_ST_FLLV_FUTA_SPAR(data) do \
{ \
    Proxy_RxByComKILOMETERSTANDInfo.ST_FLLV_FUTA_SPAR = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComKILOMETERSTANDInfo_FLLV_FUTA_RH(data) do \
{ \
    Proxy_RxByComKILOMETERSTANDInfo.FLLV_FUTA_RH = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComKILOMETERSTANDInfo_FLLV_FUTA_LH(data) do \
{ \
    Proxy_RxByComKILOMETERSTANDInfo.FLLV_FUTA_LH = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComKILOMETERSTANDInfo_FLLV_FUTA(data) do \
{ \
    Proxy_RxByComKILOMETERSTANDInfo.FLLV_FUTA = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComKILOMETERSTANDInfo_MILE_KM(data) do \
{ \
    Proxy_RxByComKILOMETERSTANDInfo.MILE_KM = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComUHRZEIT_DATUMInfo_DISP_DATE_WDAY(data) do \
{ \
    Proxy_RxByComUHRZEIT_DATUMInfo.DISP_DATE_WDAY = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComUHRZEIT_DATUMInfo_DISP_DATE_DAY(data) do \
{ \
    Proxy_RxByComUHRZEIT_DATUMInfo.DISP_DATE_DAY = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComUHRZEIT_DATUMInfo_DISP_DATE_MON(data) do \
{ \
    Proxy_RxByComUHRZEIT_DATUMInfo.DISP_DATE_MON = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComUHRZEIT_DATUMInfo_ST_DISP_CTI_DATE(data) do \
{ \
    Proxy_RxByComUHRZEIT_DATUMInfo.ST_DISP_CTI_DATE = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComUHRZEIT_DATUMInfo_DISP_DATE_YR(data) do \
{ \
    Proxy_RxByComUHRZEIT_DATUMInfo.DISP_DATE_YR = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComUHRZEIT_DATUMInfo_DISP_HR(data) do \
{ \
    Proxy_RxByComUHRZEIT_DATUMInfo.DISP_HR = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComUHRZEIT_DATUMInfo_DISP_SEC(data) do \
{ \
    Proxy_RxByComUHRZEIT_DATUMInfo.DISP_SEC = *data; \
}while(0);

#define Proxy_RxByCom_Write_Proxy_RxByComUHRZEIT_DATUMInfo_DISP_MN(data) do \
{ \
    Proxy_RxByComUHRZEIT_DATUMInfo.DISP_MN = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PROXY_RXBYCOM_IFA_H_ */
/** @} */
