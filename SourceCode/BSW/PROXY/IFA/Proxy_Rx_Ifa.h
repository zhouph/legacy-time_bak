/**
 * @defgroup Proxy_Rx_Ifa Proxy_Rx_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Proxy_Rx_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PROXY_RX_IFA_H_
#define PROXY_RX_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Proxy_Rx_Read_Proxy_RxRxBms1Info(data) do \
{ \
    *data = Proxy_RxRxBms1Info; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxYawSerialInfo(data) do \
{ \
    *data = Proxy_RxRxYawSerialInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxYawAccInfo(data) do \
{ \
    *data = Proxy_RxRxYawAccInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxTcu6Info(data) do \
{ \
    *data = Proxy_RxRxTcu6Info; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxTcu5Info(data) do \
{ \
    *data = Proxy_RxRxTcu5Info; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxTcu1Info(data) do \
{ \
    *data = Proxy_RxRxTcu1Info; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxSasInfo(data) do \
{ \
    *data = Proxy_RxRxSasInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxMcu2Info(data) do \
{ \
    *data = Proxy_RxRxMcu2Info; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxMcu1Info(data) do \
{ \
    *data = Proxy_RxRxMcu1Info; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxLongAccInfo(data) do \
{ \
    *data = Proxy_RxRxLongAccInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxHcu5Info(data) do \
{ \
    *data = Proxy_RxRxHcu5Info; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxHcu3Info(data) do \
{ \
    *data = Proxy_RxRxHcu3Info; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxHcu2Info(data) do \
{ \
    *data = Proxy_RxRxHcu2Info; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxHcu1Info(data) do \
{ \
    *data = Proxy_RxRxHcu1Info; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxFact1Info(data) do \
{ \
    *data = Proxy_RxRxFact1Info; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxEms3Info(data) do \
{ \
    *data = Proxy_RxRxEms3Info; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxEms2Info(data) do \
{ \
    *data = Proxy_RxRxEms2Info; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxEms1Info(data) do \
{ \
    *data = Proxy_RxRxEms1Info; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxClu2Info(data) do \
{ \
    *data = Proxy_RxRxClu2Info; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxClu1Info(data) do \
{ \
    *data = Proxy_RxRxClu1Info; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxMsgOkFlgInfo(data) do \
{ \
    *data = Proxy_RxRxMsgOkFlgInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo(data) do \
{ \
    *data = Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxAVL_LTRQD_BAXInfo(data) do \
{ \
    *data = Proxy_RxAVL_LTRQD_BAXInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo(data) do \
{ \
    *data = Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info(data) do \
{ \
    *data = Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info(data) do \
{ \
    *data = Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxHGLV_VEH_FILTInfo(data) do \
{ \
    *data = Proxy_RxHGLV_VEH_FILTInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo(data) do \
{ \
    *data = Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo(data) do \
{ \
    *data = Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxV_VEH_V_VEH_2Info(data) do \
{ \
    *data = Proxy_RxV_VEH_V_VEH_2Info; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_DRDYSEN_EXT_VYAW_VEHInfo(data) do \
{ \
    *data = Proxy_RxDT_DRDYSEN_EXT_VYAW_VEHInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo(data) do \
{ \
    *data = Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxAVL_STEA_FTAXInfo(data) do \
{ \
    *data = Proxy_RxAVL_STEA_FTAXInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo(data) do \
{ \
    *data = Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info(data) do \
{ \
    *data = Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWMOM_DRV_7Info(data) do \
{ \
    *data = Proxy_RxWMOM_DRV_7Info; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo(data) do \
{ \
    *data = Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_BRKSYS_ENGMGInfo(data) do \
{ \
    *data = Proxy_RxDT_BRKSYS_ENGMGInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxERRM_BN_UInfo(data) do \
{ \
    *data = Proxy_RxERRM_BN_UInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxCTR_CRInfo(data) do \
{ \
    *data = Proxy_RxCTR_CRInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxKLEMMENInfo(data) do \
{ \
    *data = Proxy_RxKLEMMENInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info(data) do \
{ \
    *data = Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxBEDIENUNG_WISCHERInfo(data) do \
{ \
    *data = Proxy_RxBEDIENUNG_WISCHERInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_PT_2Info(data) do \
{ \
    *data = Proxy_RxDT_PT_2Info; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSTAT_ENG_STA_AUTOInfo(data) do \
{ \
    *data = Proxy_RxSTAT_ENG_STA_AUTOInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxST_ENERG_GENInfo(data) do \
{ \
    *data = Proxy_RxST_ENERG_GENInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_PT_1Info(data) do \
{ \
    *data = Proxy_RxDT_PT_1Info; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_GRDT_DRVInfo(data) do \
{ \
    *data = Proxy_RxDT_GRDT_DRVInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDIAG_OBD_ENGMG_ELInfo(data) do \
{ \
    *data = Proxy_RxDIAG_OBD_ENGMG_ELInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSTAT_CT_HABRInfo(data) do \
{ \
    *data = Proxy_RxSTAT_CT_HABRInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_PT_3Info(data) do \
{ \
    *data = Proxy_RxDT_PT_3Info; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxEINHEITEN_BN2020Info(data) do \
{ \
    *data = Proxy_RxEINHEITEN_BN2020Info; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxA_TEMPInfo(data) do \
{ \
    *data = Proxy_RxA_TEMPInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWISCHERGESCHWINDIGKEITInfo(data) do \
{ \
    *data = Proxy_RxWISCHERGESCHWINDIGKEITInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSTAT_DS_VRFDInfo(data) do \
{ \
    *data = Proxy_RxSTAT_DS_VRFDInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info(data) do \
{ \
    *data = Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSTAT_ANHAENGERInfo(data) do \
{ \
    *data = Proxy_RxSTAT_ANHAENGERInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxFZZSTDInfo(data) do \
{ \
    *data = Proxy_RxFZZSTDInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxBEDIENUNG_FAHRWERKInfo(data) do \
{ \
    *data = Proxy_RxBEDIENUNG_FAHRWERKInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxFAHRZEUGTYPInfo(data) do \
{ \
    *data = Proxy_RxFAHRZEUGTYPInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxST_BLT_CT_SOCCUInfo(data) do \
{ \
    *data = Proxy_RxST_BLT_CT_SOCCUInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxFAHRGESTELLNUMMERInfo(data) do \
{ \
    *data = Proxy_RxFAHRGESTELLNUMMERInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRELATIVZEITInfo(data) do \
{ \
    *data = Proxy_RxRELATIVZEITInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxKILOMETERSTANDInfo(data) do \
{ \
    *data = Proxy_RxKILOMETERSTANDInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxUHRZEIT_DATUMInfo(data) do \
{ \
    *data = Proxy_RxUHRZEIT_DATUMInfo; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxBms1Info_Bms1SocPc(data) do \
{ \
    *data = Proxy_RxRxBms1Info.Bms1SocPc; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxYawSerialInfo_YawSerialNum_0(data) do \
{ \
    *data = Proxy_RxRxYawSerialInfo.YawSerialNum_0; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxYawSerialInfo_YawSerialNum_1(data) do \
{ \
    *data = Proxy_RxRxYawSerialInfo.YawSerialNum_1; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxYawSerialInfo_YawSerialNum_2(data) do \
{ \
    *data = Proxy_RxRxYawSerialInfo.YawSerialNum_2; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxYawAccInfo_YawRateValidData(data) do \
{ \
    *data = Proxy_RxRxYawAccInfo.YawRateValidData; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxYawAccInfo_YawRateSelfTestStatus(data) do \
{ \
    *data = Proxy_RxRxYawAccInfo.YawRateSelfTestStatus; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxYawAccInfo_YawRateSignal_0(data) do \
{ \
    *data = Proxy_RxRxYawAccInfo.YawRateSignal_0; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxYawAccInfo_YawRateSignal_1(data) do \
{ \
    *data = Proxy_RxRxYawAccInfo.YawRateSignal_1; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxYawAccInfo_SensorOscFreqDev(data) do \
{ \
    *data = Proxy_RxRxYawAccInfo.SensorOscFreqDev; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxYawAccInfo_Gyro_Fail(data) do \
{ \
    *data = Proxy_RxRxYawAccInfo.Gyro_Fail; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxYawAccInfo_Raster_Fail(data) do \
{ \
    *data = Proxy_RxRxYawAccInfo.Raster_Fail; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxYawAccInfo_Eep_Fail(data) do \
{ \
    *data = Proxy_RxRxYawAccInfo.Eep_Fail; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxYawAccInfo_Batt_Range_Err(data) do \
{ \
    *data = Proxy_RxRxYawAccInfo.Batt_Range_Err; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxYawAccInfo_Asic_Fail(data) do \
{ \
    *data = Proxy_RxRxYawAccInfo.Asic_Fail; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxYawAccInfo_Accel_Fail(data) do \
{ \
    *data = Proxy_RxRxYawAccInfo.Accel_Fail; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxYawAccInfo_Ram_Fail(data) do \
{ \
    *data = Proxy_RxRxYawAccInfo.Ram_Fail; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxYawAccInfo_Rom_Fail(data) do \
{ \
    *data = Proxy_RxRxYawAccInfo.Rom_Fail; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxYawAccInfo_Ad_Fail(data) do \
{ \
    *data = Proxy_RxRxYawAccInfo.Ad_Fail; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxYawAccInfo_Osc_Fail(data) do \
{ \
    *data = Proxy_RxRxYawAccInfo.Osc_Fail; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxYawAccInfo_Watchdog_Rst(data) do \
{ \
    *data = Proxy_RxRxYawAccInfo.Watchdog_Rst; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxYawAccInfo_Plaus_Err_Pst(data) do \
{ \
    *data = Proxy_RxRxYawAccInfo.Plaus_Err_Pst; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxYawAccInfo_RollingCounter(data) do \
{ \
    *data = Proxy_RxRxYawAccInfo.RollingCounter; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxYawAccInfo_Can_Func_Err(data) do \
{ \
    *data = Proxy_RxRxYawAccInfo.Can_Func_Err; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxYawAccInfo_AccelEratorRateSig_0(data) do \
{ \
    *data = Proxy_RxRxYawAccInfo.AccelEratorRateSig_0; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxYawAccInfo_AccelEratorRateSig_1(data) do \
{ \
    *data = Proxy_RxRxYawAccInfo.AccelEratorRateSig_1; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxYawAccInfo_LatAccValidData(data) do \
{ \
    *data = Proxy_RxRxYawAccInfo.LatAccValidData; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxYawAccInfo_LatAccSelfTestStatus(data) do \
{ \
    *data = Proxy_RxRxYawAccInfo.LatAccSelfTestStatus; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxTcu6Info_ShiftClass_Ccan(data) do \
{ \
    *data = Proxy_RxRxTcu6Info.ShiftClass_Ccan; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxTcu5Info_Typ(data) do \
{ \
    *data = Proxy_RxRxTcu5Info.Typ; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxTcu5Info_GearTyp(data) do \
{ \
    *data = Proxy_RxRxTcu5Info.GearTyp; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxTcu1Info_Targe(data) do \
{ \
    *data = Proxy_RxRxTcu1Info.Targe; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxTcu1Info_GarChange(data) do \
{ \
    *data = Proxy_RxRxTcu1Info.GarChange; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxTcu1Info_Flt(data) do \
{ \
    *data = Proxy_RxRxTcu1Info.Flt; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxTcu1Info_GarSelDisp(data) do \
{ \
    *data = Proxy_RxRxTcu1Info.GarSelDisp; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxTcu1Info_TQRedReq_PC(data) do \
{ \
    *data = Proxy_RxRxTcu1Info.TQRedReq_PC; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxTcu1Info_TQRedReqSlw_PC(data) do \
{ \
    *data = Proxy_RxRxTcu1Info.TQRedReqSlw_PC; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxTcu1Info_TQIncReq_PC(data) do \
{ \
    *data = Proxy_RxRxTcu1Info.TQIncReq_PC; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxSasInfo_Angle(data) do \
{ \
    *data = Proxy_RxRxSasInfo.Angle; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxSasInfo_Speed(data) do \
{ \
    *data = Proxy_RxRxSasInfo.Speed; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxSasInfo_Ok(data) do \
{ \
    *data = Proxy_RxRxSasInfo.Ok; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxSasInfo_Cal(data) do \
{ \
    *data = Proxy_RxRxSasInfo.Cal; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxSasInfo_Trim(data) do \
{ \
    *data = Proxy_RxRxSasInfo.Trim; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxSasInfo_CheckSum(data) do \
{ \
    *data = Proxy_RxRxSasInfo.CheckSum; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxSasInfo_MsgCount(data) do \
{ \
    *data = Proxy_RxRxSasInfo.MsgCount; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxMcu2Info_Flt(data) do \
{ \
    *data = Proxy_RxRxMcu2Info.Flt; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxMcu1Info_MoTestTQ_PC(data) do \
{ \
    *data = Proxy_RxRxMcu1Info.MoTestTQ_PC; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxMcu1Info_MotActRotSpd_RPM(data) do \
{ \
    *data = Proxy_RxRxMcu1Info.MotActRotSpd_RPM; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxLongAccInfo_IntSenFltSymtmActive(data) do \
{ \
    *data = Proxy_RxRxLongAccInfo.IntSenFltSymtmActive; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxLongAccInfo_IntSenFaultPresent(data) do \
{ \
    *data = Proxy_RxRxLongAccInfo.IntSenFaultPresent; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxLongAccInfo_LongAccSenCirErrPre(data) do \
{ \
    *data = Proxy_RxRxLongAccInfo.LongAccSenCirErrPre; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxLongAccInfo_LonACSenRanChkErrPre(data) do \
{ \
    *data = Proxy_RxRxLongAccInfo.LonACSenRanChkErrPre; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxLongAccInfo_LongRollingCounter(data) do \
{ \
    *data = Proxy_RxRxLongAccInfo.LongRollingCounter; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxLongAccInfo_IntTempSensorFault(data) do \
{ \
    *data = Proxy_RxRxLongAccInfo.IntTempSensorFault; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxLongAccInfo_LongAccInvalidData(data) do \
{ \
    *data = Proxy_RxRxLongAccInfo.LongAccInvalidData; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxLongAccInfo_LongAccSelfTstStatus(data) do \
{ \
    *data = Proxy_RxRxLongAccInfo.LongAccSelfTstStatus; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxLongAccInfo_LongAccRateSignal_0(data) do \
{ \
    *data = Proxy_RxRxLongAccInfo.LongAccRateSignal_0; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxLongAccInfo_LongAccRateSignal_1(data) do \
{ \
    *data = Proxy_RxRxLongAccInfo.LongAccRateSignal_1; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxHcu5Info_HevMod(data) do \
{ \
    *data = Proxy_RxRxHcu5Info.HevMod; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxHcu3Info_TmIntQcMDBINV_PC(data) do \
{ \
    *data = Proxy_RxRxHcu3Info.TmIntQcMDBINV_PC; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxHcu3Info_MotTQCMC_PC(data) do \
{ \
    *data = Proxy_RxRxHcu3Info.MotTQCMC_PC; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxHcu3Info_MotTQCMDBINV_PC(data) do \
{ \
    *data = Proxy_RxRxHcu3Info.MotTQCMDBINV_PC; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxHcu2Info_ServiceMod(data) do \
{ \
    *data = Proxy_RxRxHcu2Info.ServiceMod; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxHcu2Info_RegenENA(data) do \
{ \
    *data = Proxy_RxRxHcu2Info.RegenENA; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxHcu2Info_RegenBRKTQ_NM(data) do \
{ \
    *data = Proxy_RxRxHcu2Info.RegenBRKTQ_NM; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxHcu2Info_CrpTQ_NM(data) do \
{ \
    *data = Proxy_RxRxHcu2Info.CrpTQ_NM; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxHcu2Info_WhlDEMTQ_NM(data) do \
{ \
    *data = Proxy_RxRxHcu2Info.WhlDEMTQ_NM; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxHcu1Info_EngCltStat(data) do \
{ \
    *data = Proxy_RxRxHcu1Info.EngCltStat; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxHcu1Info_HEVRDY(data) do \
{ \
    *data = Proxy_RxRxHcu1Info.HEVRDY; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxHcu1Info_EngTQCmdBinV_PC(data) do \
{ \
    *data = Proxy_RxRxHcu1Info.EngTQCmdBinV_PC; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxHcu1Info_EngTQCmd_PC(data) do \
{ \
    *data = Proxy_RxRxHcu1Info.EngTQCmd_PC; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxFact1Info_OutTemp_SNR_C(data) do \
{ \
    *data = Proxy_RxRxFact1Info.OutTemp_SNR_C; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxEms3Info_EngColTemp_C(data) do \
{ \
    *data = Proxy_RxRxEms3Info.EngColTemp_C; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxEms2Info_EngSpdErr(data) do \
{ \
    *data = Proxy_RxRxEms2Info.EngSpdErr; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxEms2Info_AccPedDep_PC(data) do \
{ \
    *data = Proxy_RxRxEms2Info.AccPedDep_PC; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxEms2Info_Tps_PC(data) do \
{ \
    *data = Proxy_RxRxEms2Info.Tps_PC; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxEms1Info_TqStd_NM(data) do \
{ \
    *data = Proxy_RxRxEms1Info.TqStd_NM; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxEms1Info_ActINDTQ_PC(data) do \
{ \
    *data = Proxy_RxRxEms1Info.ActINDTQ_PC; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxEms1Info_EngSpd_RPM(data) do \
{ \
    *data = Proxy_RxRxEms1Info.EngSpd_RPM; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxEms1Info_IndTQ_PC(data) do \
{ \
    *data = Proxy_RxRxEms1Info.IndTQ_PC; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxEms1Info_FrictTQ_PC(data) do \
{ \
    *data = Proxy_RxRxEms1Info.FrictTQ_PC; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxClu2Info_IGN_SW(data) do \
{ \
    *data = Proxy_RxRxClu2Info.IGN_SW; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxClu1Info_P_Brake_Act(data) do \
{ \
    *data = Proxy_RxRxClu1Info.P_Brake_Act; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxClu1Info_Cf_Clu_BrakeFluIDSW(data) do \
{ \
    *data = Proxy_RxRxClu1Info.Cf_Clu_BrakeFluIDSW; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxMsgOkFlgInfo_Bms1MsgOkFlg(data) do \
{ \
    *data = Proxy_RxRxMsgOkFlgInfo.Bms1MsgOkFlg; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxMsgOkFlgInfo_YawSerialMsgOkFlg(data) do \
{ \
    *data = Proxy_RxRxMsgOkFlgInfo.YawSerialMsgOkFlg; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxMsgOkFlgInfo_YawAccMsgOkFlg(data) do \
{ \
    *data = Proxy_RxRxMsgOkFlgInfo.YawAccMsgOkFlg; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxMsgOkFlgInfo_Tcu6MsgOkFlg(data) do \
{ \
    *data = Proxy_RxRxMsgOkFlgInfo.Tcu6MsgOkFlg; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxMsgOkFlgInfo_Tcu5MsgOkFlg(data) do \
{ \
    *data = Proxy_RxRxMsgOkFlgInfo.Tcu5MsgOkFlg; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxMsgOkFlgInfo_Tcu1MsgOkFlg(data) do \
{ \
    *data = Proxy_RxRxMsgOkFlgInfo.Tcu1MsgOkFlg; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxMsgOkFlgInfo_SasMsgOkFlg(data) do \
{ \
    *data = Proxy_RxRxMsgOkFlgInfo.SasMsgOkFlg; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxMsgOkFlgInfo_Mcu2MsgOkFlg(data) do \
{ \
    *data = Proxy_RxRxMsgOkFlgInfo.Mcu2MsgOkFlg; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxMsgOkFlgInfo_Mcu1MsgOkFlg(data) do \
{ \
    *data = Proxy_RxRxMsgOkFlgInfo.Mcu1MsgOkFlg; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxMsgOkFlgInfo_LongAccMsgOkFlg(data) do \
{ \
    *data = Proxy_RxRxMsgOkFlgInfo.LongAccMsgOkFlg; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxMsgOkFlgInfo_Hcu5MsgOkFlg(data) do \
{ \
    *data = Proxy_RxRxMsgOkFlgInfo.Hcu5MsgOkFlg; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxMsgOkFlgInfo_Hcu3MsgOkFlg(data) do \
{ \
    *data = Proxy_RxRxMsgOkFlgInfo.Hcu3MsgOkFlg; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxMsgOkFlgInfo_Hcu2MsgOkFlg(data) do \
{ \
    *data = Proxy_RxRxMsgOkFlgInfo.Hcu2MsgOkFlg; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxMsgOkFlgInfo_Hcu1MsgOkFlg(data) do \
{ \
    *data = Proxy_RxRxMsgOkFlgInfo.Hcu1MsgOkFlg; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxMsgOkFlgInfo_Fatc1MsgOkFlg(data) do \
{ \
    *data = Proxy_RxRxMsgOkFlgInfo.Fatc1MsgOkFlg; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxMsgOkFlgInfo_Ems3MsgOkFlg(data) do \
{ \
    *data = Proxy_RxRxMsgOkFlgInfo.Ems3MsgOkFlg; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxMsgOkFlgInfo_Ems2MsgOkFlg(data) do \
{ \
    *data = Proxy_RxRxMsgOkFlgInfo.Ems2MsgOkFlg; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxMsgOkFlgInfo_Ems1MsgOkFlg(data) do \
{ \
    *data = Proxy_RxRxMsgOkFlgInfo.Ems1MsgOkFlg; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxMsgOkFlgInfo_Clu2MsgOkFlg(data) do \
{ \
    *data = Proxy_RxRxMsgOkFlgInfo.Clu2MsgOkFlg; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRxMsgOkFlgInfo_Clu1MsgOkFlg(data) do \
{ \
    *data = Proxy_RxRxMsgOkFlgInfo.Clu1MsgOkFlg; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo_ST_ECU_ST_REPAT_XTRQ_FTAX_BAX(data) do \
{ \
    *data = Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.ST_ECU_ST_REPAT_XTRQ_FTAX_BAX; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo_QU_SER_REPAT_XTRQ_FTAX_BAX_ACT(data) do \
{ \
    *data = Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.QU_SER_REPAT_XTRQ_FTAX_BAX_ACT; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo_AVL_REPAT_XTRQ_FTAX_BAX(data) do \
{ \
    *data = Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.AVL_REPAT_XTRQ_FTAX_BAX; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo_QU_AVL_REPAT_XTRQ_FTAX_BAX(data) do \
{ \
    *data = Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo.QU_AVL_REPAT_XTRQ_FTAX_BAX; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxAVL_LTRQD_BAXInfo_QU_SER_LTRQD_BAX(data) do \
{ \
    *data = Proxy_RxAVL_LTRQD_BAXInfo.QU_SER_LTRQD_BAX; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxAVL_LTRQD_BAXInfo_QU_AVL_LTRQD_BAX(data) do \
{ \
    *data = Proxy_RxAVL_LTRQD_BAXInfo.QU_AVL_LTRQD_BAX; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxAVL_LTRQD_BAXInfo_AVL_LTRQD_BAX(data) do \
{ \
    *data = Proxy_RxAVL_LTRQD_BAXInfo.AVL_LTRQD_BAX; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo_AVL_TORQ_CRSH_DMEE(data) do \
{ \
    *data = Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.AVL_TORQ_CRSH_DMEE; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo_AVL_TORQ_CRSH(data) do \
{ \
    *data = Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.AVL_TORQ_CRSH; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo_ST_SAIL_DRV_2(data) do \
{ \
    *data = Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.ST_SAIL_DRV_2; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo_QU_AVL_RPM_ENG_CRSH(data) do \
{ \
    *data = Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.QU_AVL_RPM_ENG_CRSH; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo_AVL_RPM_ENG_CRSH(data) do \
{ \
    *data = Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.AVL_RPM_ENG_CRSH; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo_AVL_ANG_ACPD(data) do \
{ \
    *data = Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.AVL_ANG_ACPD; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo_QU_AVL_ANG_ACPD(data) do \
{ \
    *data = Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.QU_AVL_ANG_ACPD; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo_ST_ECU_ANG_ACPD(data) do \
{ \
    *data = Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.ST_ECU_ANG_ACPD; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo_AVL_ANG_ACPD_VIRT(data) do \
{ \
    *data = Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.AVL_ANG_ACPD_VIRT; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo_ECO_ANG_ACPD(data) do \
{ \
    *data = Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.ECO_ANG_ACPD; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo_GRAD_AVL_ANG_ACPD(data) do \
{ \
    *data = Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.GRAD_AVL_ANG_ACPD; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo_ST_INTF_DRASY(data) do \
{ \
    *data = Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo.ST_INTF_DRASY; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info_AVL_RPM_BAX_RED(data) do \
{ \
    *data = Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.AVL_RPM_BAX_RED; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info_ST_PENG_PT(data) do \
{ \
    *data = Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.ST_PENG_PT; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info_ST_AVAI_INTV_PT_DRS(data) do \
{ \
    *data = Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.ST_AVAI_INTV_PT_DRS; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info_QU_AVL_RPM_BAX_RED(data) do \
{ \
    *data = Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.QU_AVL_RPM_BAX_RED; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info_QU_SER_WMOM_PT_SUM_DRS(data) do \
{ \
    *data = Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.QU_SER_WMOM_PT_SUM_DRS; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info_QU_SER_WMOM_PT_SUM_STAB(data) do \
{ \
    *data = Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.QU_SER_WMOM_PT_SUM_STAB; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info_ST_ECU_WMOM_DRV_5(data) do \
{ \
    *data = Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.ST_ECU_WMOM_DRV_5; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info_TAR_WMOM_PT_SUM_COOTD(data) do \
{ \
    *data = Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.TAR_WMOM_PT_SUM_COOTD; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info_QU_SER_COOR_TORQ_BDRV(data) do \
{ \
    *data = Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.QU_SER_COOR_TORQ_BDRV; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info_ST_ECU_WMOM_DRV_4(data) do \
{ \
    *data = Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.ST_ECU_WMOM_DRV_4; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info_ST_DRVDIR_DVCH(data) do \
{ \
    *data = Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info.ST_DRVDIR_DVCH; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info_QU_AVL_WMOM_PT_SUM(data) do \
{ \
    *data = Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.QU_AVL_WMOM_PT_SUM; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info_AVL_WMOM_PT_SUM_ERR_AMP(data) do \
{ \
    *data = Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_ERR_AMP; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info_REIN_PT(data) do \
{ \
    *data = Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.REIN_PT; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info_AVL_WMOM_PT_SUM(data) do \
{ \
    *data = Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info_ST_ECU_WMOM_DRV_1(data) do \
{ \
    *data = Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.ST_ECU_WMOM_DRV_1; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info_AVL_WMOM_PT_SUM_MAX(data) do \
{ \
    *data = Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_MAX; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info_AVL_WMOM_PT_SUM_FAST_TOP(data) do \
{ \
    *data = Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_FAST_TOP; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info_AVL_WMOM_PT_SUM_FAST_BOT(data) do \
{ \
    *data = Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.AVL_WMOM_PT_SUM_FAST_BOT; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info_ST_ECU_WMOM_DRV_2(data) do \
{ \
    *data = Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.ST_ECU_WMOM_DRV_2; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info_QU_REIN_PT(data) do \
{ \
    *data = Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info.QU_REIN_PT; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxHGLV_VEH_FILTInfo_HGLV_VEH_FILT_FRH(data) do \
{ \
    *data = Proxy_RxHGLV_VEH_FILTInfo.HGLV_VEH_FILT_FRH; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxHGLV_VEH_FILTInfo_HGLV_VEH_FILT_FLH(data) do \
{ \
    *data = Proxy_RxHGLV_VEH_FILTInfo.HGLV_VEH_FILT_FLH; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxHGLV_VEH_FILTInfo_QU_HGLV_VEH_FILT_FRH(data) do \
{ \
    *data = Proxy_RxHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_FRH; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxHGLV_VEH_FILTInfo_QU_HGLV_VEH_FILT_FLH(data) do \
{ \
    *data = Proxy_RxHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_FLH; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxHGLV_VEH_FILTInfo_QU_HGLV_VEH_FILT_RRH(data) do \
{ \
    *data = Proxy_RxHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_RRH; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxHGLV_VEH_FILTInfo_HGLV_VEH_FILT_RLH(data) do \
{ \
    *data = Proxy_RxHGLV_VEH_FILTInfo.HGLV_VEH_FILT_RLH; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxHGLV_VEH_FILTInfo_HGLV_VEH_FILT_RRH(data) do \
{ \
    *data = Proxy_RxHGLV_VEH_FILTInfo.HGLV_VEH_FILT_RRH; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxHGLV_VEH_FILTInfo_QU_HGLV_VEH_FILT_RLH(data) do \
{ \
    *data = Proxy_RxHGLV_VEH_FILTInfo.QU_HGLV_VEH_FILT_RLH; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo_ATTAV_ESTI(data) do \
{ \
    *data = Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.ATTAV_ESTI; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo_ATTA_ESTI_ERR_AMP(data) do \
{ \
    *data = Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.ATTA_ESTI_ERR_AMP; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo_VY_ESTI_ERR_AMP(data) do \
{ \
    *data = Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.VY_ESTI_ERR_AMP; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo_VY_ESTI(data) do \
{ \
    *data = Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.VY_ESTI; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo_ATTAV_ESTI_ERR_AMP(data) do \
{ \
    *data = Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.ATTAV_ESTI_ERR_AMP; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo_QU_VEH_DYNMC_DT_ESTI(data) do \
{ \
    *data = Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.QU_VEH_DYNMC_DT_ESTI; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo_ATTA_ESTI(data) do \
{ \
    *data = Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo.ATTA_ESTI; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo_QU_ACLNY_COG(data) do \
{ \
    *data = Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.QU_ACLNY_COG; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo_ACLNY_COG_ERR_AMP(data) do \
{ \
    *data = Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNY_COG_ERR_AMP; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo_ACLNX_COG(data) do \
{ \
    *data = Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNX_COG; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo_ACLNY_COG(data) do \
{ \
    *data = Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNY_COG; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo_ACLNX_COG_ERR_AMP(data) do \
{ \
    *data = Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.ACLNX_COG_ERR_AMP; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo_QU_ACLNX_COG(data) do \
{ \
    *data = Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo.QU_ACLNX_COG; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxV_VEH_V_VEH_2Info_QU_V_VEH_COG(data) do \
{ \
    *data = Proxy_RxV_VEH_V_VEH_2Info.QU_V_VEH_COG; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxV_VEH_V_VEH_2Info_DVCO_VEH(data) do \
{ \
    *data = Proxy_RxV_VEH_V_VEH_2Info.DVCO_VEH; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxV_VEH_V_VEH_2Info_ST_V_VEH_NSS(data) do \
{ \
    *data = Proxy_RxV_VEH_V_VEH_2Info.ST_V_VEH_NSS; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxV_VEH_V_VEH_2Info_V_VEH_COG(data) do \
{ \
    *data = Proxy_RxV_VEH_V_VEH_2Info.V_VEH_COG; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_DRDYSEN_EXT_VYAW_VEHInfo_VYAW_VEH_ERR_AMP(data) do \
{ \
    *data = Proxy_RxDT_DRDYSEN_EXT_VYAW_VEHInfo.VYAW_VEH_ERR_AMP; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_DRDYSEN_EXT_VYAW_VEHInfo_QU_VYAW_VEH(data) do \
{ \
    *data = Proxy_RxDT_DRDYSEN_EXT_VYAW_VEHInfo.QU_VYAW_VEH; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_DRDYSEN_EXT_VYAW_VEHInfo_VYAW_VEH(data) do \
{ \
    *data = Proxy_RxDT_DRDYSEN_EXT_VYAW_VEHInfo.VYAW_VEH; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo_QU_AVL_TRGR_RW(data) do \
{ \
    *data = Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo.QU_AVL_TRGR_RW; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo_AVL_TRGR_RW(data) do \
{ \
    *data = Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo.AVL_TRGR_RW; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo_AVL_LOGR_RW_FAST(data) do \
{ \
    *data = Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo.AVL_LOGR_RW_FAST; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo_QU_AVL_LOGR_RW(data) do \
{ \
    *data = Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo.QU_AVL_LOGR_RW; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo_AVL_LOGR_RW(data) do \
{ \
    *data = Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo.AVL_LOGR_RW; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxAVL_STEA_FTAXInfo_QU_AVL_STEA_FTAX_PNI(data) do \
{ \
    *data = Proxy_RxAVL_STEA_FTAXInfo.QU_AVL_STEA_FTAX_PNI; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxAVL_STEA_FTAXInfo_AVL_STEA_FTAX_PNI(data) do \
{ \
    *data = Proxy_RxAVL_STEA_FTAXInfo.AVL_STEA_FTAX_PNI; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxAVL_STEA_FTAXInfo_QU_AVL_STEA_FTAX_WHL(data) do \
{ \
    *data = Proxy_RxAVL_STEA_FTAXInfo.QU_AVL_STEA_FTAX_WHL; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxAVL_STEA_FTAXInfo_AVL_STEA_FTAX_WHL(data) do \
{ \
    *data = Proxy_RxAVL_STEA_FTAXInfo.AVL_STEA_FTAX_WHL; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxAVL_STEA_FTAXInfo_AVL_STEA_FTAX_WHL_ERR_AMP(data) do \
{ \
    *data = Proxy_RxAVL_STEA_FTAXInfo.AVL_STEA_FTAX_WHL_ERR_AMP; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_TAR_PRMSN_DBC_DCRN_MAX(data) do \
{ \
    *data = Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_PRMSN_DBC_DCRN_MAX; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_QU_TAR_PRF_BRK(data) do \
{ \
    *data = Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_PRF_BRK; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_QU_TAR_V_HDC(data) do \
{ \
    *data = Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_V_HDC; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_TAR_PRMSN_DBC_DCRN_GRAD_MAX(data) do \
{ \
    *data = Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_PRMSN_DBC_DCRN_GRAD_MAX; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_TAR_PRMSN_DBC_TR_THRV(data) do \
{ \
    *data = Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_PRMSN_DBC_TR_THRV; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_TAR_BRTORQ_SUM(data) do \
{ \
    *data = Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.TAR_BRTORQ_SUM; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_QU_TAR_BRTORQ_SUM(data) do \
{ \
    *data = Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_BRTORQ_SUM; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_QU_TAR_PRMSN_DBC(data) do \
{ \
    *data = Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.QU_TAR_PRMSN_DBC; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_RQ_TAO_SSM(data) do \
{ \
    *data = Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo.RQ_TAO_SSM; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info_AVL_WMOM_PT_SUM_DTORQ_BOT(data) do \
{ \
    *data = Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_DTORQ_BOT; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info_AVL_WMOM_PT_SUM_RECUP_MAX(data) do \
{ \
    *data = Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_RECUP_MAX; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info_AVL_WMOM_PT_SUM_ILS(data) do \
{ \
    *data = Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_ILS; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info_AVL_WMOM_PT_SUM_DTORQ_TOP(data) do \
{ \
    *data = Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info.AVL_WMOM_PT_SUM_DTORQ_TOP; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info_ST_ECU_WMOM_DRV_6(data) do \
{ \
    *data = Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info.ST_ECU_WMOM_DRV_6; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWMOM_DRV_7Info_ST_EL_DRVG(data) do \
{ \
    *data = Proxy_RxWMOM_DRV_7Info.ST_EL_DRVG; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWMOM_DRV_7Info_PRD_AVL_WMOM_PT_SUM_RECUP_MAX(data) do \
{ \
    *data = Proxy_RxWMOM_DRV_7Info.PRD_AVL_WMOM_PT_SUM_RECUP_MAX; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWMOM_DRV_7Info_ST_ECU_WMOM_DRV_7(data) do \
{ \
    *data = Proxy_RxWMOM_DRV_7Info.ST_ECU_WMOM_DRV_7; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWMOM_DRV_7Info_QU_SER_WMOM_PT_SUM_RECUP(data) do \
{ \
    *data = Proxy_RxWMOM_DRV_7Info.QU_SER_WMOM_PT_SUM_RECUP; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo_TAR_DIFF_BRTORQ_BAX_YMR(data) do \
{ \
    *data = Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.TAR_DIFF_BRTORQ_BAX_YMR; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo_FACT_TAR_COMPT_DRV_YMR(data) do \
{ \
    *data = Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.FACT_TAR_COMPT_DRV_YMR; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo_QU_TAR_DIFF_BRTORQ_YMR(data) do \
{ \
    *data = Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.QU_TAR_DIFF_BRTORQ_YMR; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo_TAR_DIFF_BRTORQ_FTAX_YMR(data) do \
{ \
    *data = Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo.TAR_DIFF_BRTORQ_FTAX_YMR; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_BRKSYS_ENGMGInfo_QU_AVL_LOWP_BRKFA(data) do \
{ \
    *data = Proxy_RxDT_BRKSYS_ENGMGInfo.QU_AVL_LOWP_BRKFA; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_BRKSYS_ENGMGInfo_AVL_LOWP_BRKFA(data) do \
{ \
    *data = Proxy_RxDT_BRKSYS_ENGMGInfo.AVL_LOWP_BRKFA; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxERRM_BN_UInfo_CTR_ERRM_BN_U(data) do \
{ \
    *data = Proxy_RxERRM_BN_UInfo.CTR_ERRM_BN_U; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxCTR_CRInfo_ST_EXCE_ACLN_THRV(data) do \
{ \
    *data = Proxy_RxCTR_CRInfo.ST_EXCE_ACLN_THRV; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxCTR_CRInfo_CTR_PHTR_CR(data) do \
{ \
    *data = Proxy_RxCTR_CRInfo.CTR_PHTR_CR; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxCTR_CRInfo_CTR_ITLI_CR(data) do \
{ \
    *data = Proxy_RxCTR_CRInfo.CTR_ITLI_CR; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxCTR_CRInfo_CTR_CLSY_CR(data) do \
{ \
    *data = Proxy_RxCTR_CRInfo.CTR_CLSY_CR; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxCTR_CRInfo_CTR_AUTOM_ECAL_CR(data) do \
{ \
    *data = Proxy_RxCTR_CRInfo.CTR_AUTOM_ECAL_CR; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxCTR_CRInfo_CTR_SWO_EKP_CR(data) do \
{ \
    *data = Proxy_RxCTR_CRInfo.CTR_SWO_EKP_CR; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxCTR_CRInfo_CTR_PCSH_MST(data) do \
{ \
    *data = Proxy_RxCTR_CRInfo.CTR_PCSH_MST; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxCTR_CRInfo_CTR_HAZW_CR(data) do \
{ \
    *data = Proxy_RxCTR_CRInfo.CTR_HAZW_CR; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxKLEMMENInfo_ST_OP_MSA(data) do \
{ \
    *data = Proxy_RxKLEMMENInfo.ST_OP_MSA; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxKLEMMENInfo_RQ_DRVG_RDI(data) do \
{ \
    *data = Proxy_RxKLEMMENInfo.RQ_DRVG_RDI; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxKLEMMENInfo_ST_KL_DBG(data) do \
{ \
    *data = Proxy_RxKLEMMENInfo.ST_KL_DBG; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxKLEMMENInfo_ST_KL_50_MSA(data) do \
{ \
    *data = Proxy_RxKLEMMENInfo.ST_KL_50_MSA; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxKLEMMENInfo_ST_SSP(data) do \
{ \
    *data = Proxy_RxKLEMMENInfo.ST_SSP; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxKLEMMENInfo_RWDT_BLS(data) do \
{ \
    *data = Proxy_RxKLEMMENInfo.RWDT_BLS; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxKLEMMENInfo_ST_STCD_PENG(data) do \
{ \
    *data = Proxy_RxKLEMMENInfo.ST_STCD_PENG; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxKLEMMENInfo_ST_KL(data) do \
{ \
    *data = Proxy_RxKLEMMENInfo.ST_KL; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxKLEMMENInfo_ST_KL_DIV(data) do \
{ \
    *data = Proxy_RxKLEMMENInfo.ST_KL_DIV; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxKLEMMENInfo_ST_VEH_CON(data) do \
{ \
    *data = Proxy_RxKLEMMENInfo.ST_VEH_CON; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxKLEMMENInfo_ST_KL_30B(data) do \
{ \
    *data = Proxy_RxKLEMMENInfo.ST_KL_30B; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxKLEMMENInfo_CON_CLT_SW(data) do \
{ \
    *data = Proxy_RxKLEMMENInfo.CON_CLT_SW; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxKLEMMENInfo_CTR_ENG_STOP(data) do \
{ \
    *data = Proxy_RxKLEMMENInfo.CTR_ENG_STOP; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxKLEMMENInfo_ST_PLK(data) do \
{ \
    *data = Proxy_RxKLEMMENInfo.ST_PLK; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxKLEMMENInfo_ST_KL_15N(data) do \
{ \
    *data = Proxy_RxKLEMMENInfo.ST_KL_15N; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxKLEMMENInfo_ST_KL_KEY_VLD(data) do \
{ \
    *data = Proxy_RxKLEMMENInfo.ST_KL_KEY_VLD; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_RDC_MES_TSTMP(data) do \
{ \
    *data = Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_MES_TSTMP; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_PCKG_ID(data) do \
{ \
    *data = Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.PCKG_ID; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_SUPP_ID(data) do \
{ \
    *data = Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.SUPP_ID; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_RDC_DT_5(data) do \
{ \
    *data = Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_5; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_RDC_DT_8(data) do \
{ \
    *data = Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_8; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_RDC_DT_7(data) do \
{ \
    *data = Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_7; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_RDC_DT_6(data) do \
{ \
    *data = Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_6; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_RDC_DT_1(data) do \
{ \
    *data = Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_1; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_TYR_ID(data) do \
{ \
    *data = Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.TYR_ID; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_RDC_DT_4(data) do \
{ \
    *data = Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_4; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_RDC_DT_3(data) do \
{ \
    *data = Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_3; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_RDC_DT_2(data) do \
{ \
    *data = Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info.RDC_DT_2; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxBEDIENUNG_WISCHERInfo_OP_WISW(data) do \
{ \
    *data = Proxy_RxBEDIENUNG_WISCHERInfo.OP_WISW; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxBEDIENUNG_WISCHERInfo_OP_WIPO(data) do \
{ \
    *data = Proxy_RxBEDIENUNG_WISCHERInfo.OP_WIPO; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_PT_2Info_ST_GRSEL_DRV(data) do \
{ \
    *data = Proxy_RxDT_PT_2Info.ST_GRSEL_DRV; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_PT_2Info_TEMP_EOI_DRV(data) do \
{ \
    *data = Proxy_RxDT_PT_2Info.TEMP_EOI_DRV; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_PT_2Info_RLS_ENGSTA(data) do \
{ \
    *data = Proxy_RxDT_PT_2Info.RLS_ENGSTA; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_PT_2Info_RPM_ENG_MAX_ALW(data) do \
{ \
    *data = Proxy_RxDT_PT_2Info.RPM_ENG_MAX_ALW; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_PT_2Info_RDUC_DOCTR_RPM_DRV_2(data) do \
{ \
    *data = Proxy_RxDT_PT_2Info.RDUC_DOCTR_RPM_DRV_2; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_PT_2Info_TEMP_ENG_DRV(data) do \
{ \
    *data = Proxy_RxDT_PT_2Info.TEMP_ENG_DRV; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_PT_2Info_ST_DRV_VEH(data) do \
{ \
    *data = Proxy_RxDT_PT_2Info.ST_DRV_VEH; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_PT_2Info_ST_ECU_DT_PT_2(data) do \
{ \
    *data = Proxy_RxDT_PT_2Info.ST_ECU_DT_PT_2; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_PT_2Info_ST_IDLG_ENG_DRV(data) do \
{ \
    *data = Proxy_RxDT_PT_2Info.ST_IDLG_ENG_DRV; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_PT_2Info_ST_ILK_STRT_DRV(data) do \
{ \
    *data = Proxy_RxDT_PT_2Info.ST_ILK_STRT_DRV; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_PT_2Info_ST_SW_CLT_DRV(data) do \
{ \
    *data = Proxy_RxDT_PT_2Info.ST_SW_CLT_DRV; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_PT_2Info_ST_ENG_RUN_DRV(data) do \
{ \
    *data = Proxy_RxDT_PT_2Info.ST_ENG_RUN_DRV; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSTAT_ENG_STA_AUTOInfo_RQ_MSA_ENG_STA_1(data) do \
{ \
    *data = Proxy_RxSTAT_ENG_STA_AUTOInfo.RQ_MSA_ENG_STA_1; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSTAT_ENG_STA_AUTOInfo_RQ_MSA_ENG_STA_2(data) do \
{ \
    *data = Proxy_RxSTAT_ENG_STA_AUTOInfo.RQ_MSA_ENG_STA_2; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSTAT_ENG_STA_AUTOInfo_PSBTY_MSA_ENG_STOP_STA(data) do \
{ \
    *data = Proxy_RxSTAT_ENG_STA_AUTOInfo.PSBTY_MSA_ENG_STOP_STA; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSTAT_ENG_STA_AUTOInfo_ST_SHFT_MSA_ENGSTP(data) do \
{ \
    *data = Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_SHFT_MSA_ENGSTP; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSTAT_ENG_STA_AUTOInfo_RQ_SLIP_K0(data) do \
{ \
    *data = Proxy_RxSTAT_ENG_STA_AUTOInfo.RQ_SLIP_K0; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSTAT_ENG_STA_AUTOInfo_ST_TAR_PENG_CENG(data) do \
{ \
    *data = Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_TAR_PENG_CENG; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSTAT_ENG_STA_AUTOInfo_SPEC_TYP_ENGSTA(data) do \
{ \
    *data = Proxy_RxSTAT_ENG_STA_AUTOInfo.SPEC_TYP_ENGSTA; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSTAT_ENG_STA_AUTOInfo_ST_RPM_CLCTR_MOT(data) do \
{ \
    *data = Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_RPM_CLCTR_MOT; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSTAT_ENG_STA_AUTOInfo_ST_FN_MSA(data) do \
{ \
    *data = Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_FN_MSA; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSTAT_ENG_STA_AUTOInfo_ST_AVL_PENG_CENG_ENGMG(data) do \
{ \
    *data = Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_AVL_PENG_CENG_ENGMG; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSTAT_ENG_STA_AUTOInfo_DISP_REAS_PREV_SWO_CENG(data) do \
{ \
    *data = Proxy_RxSTAT_ENG_STA_AUTOInfo.DISP_REAS_PREV_SWO_CENG; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSTAT_ENG_STA_AUTOInfo_VARI_TYP_ENGSTA(data) do \
{ \
    *data = Proxy_RxSTAT_ENG_STA_AUTOInfo.VARI_TYP_ENGSTA; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSTAT_ENG_STA_AUTOInfo_ST_TAR_CENG(data) do \
{ \
    *data = Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_TAR_CENG; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSTAT_ENG_STA_AUTOInfo_ST_AVAI_SAIL_DME(data) do \
{ \
    *data = Proxy_RxSTAT_ENG_STA_AUTOInfo.ST_AVAI_SAIL_DME; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxST_ENERG_GENInfo_ST_LDST_GEN_DRV(data) do \
{ \
    *data = Proxy_RxST_ENERG_GENInfo.ST_LDST_GEN_DRV; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxST_ENERG_GENInfo_DT_PCU_SCP(data) do \
{ \
    *data = Proxy_RxST_ENERG_GENInfo.DT_PCU_SCP; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxST_ENERG_GENInfo_ST_GEN_DRV(data) do \
{ \
    *data = Proxy_RxST_ENERG_GENInfo.ST_GEN_DRV; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxST_ENERG_GENInfo_AVL_I_GEN_DRV(data) do \
{ \
    *data = Proxy_RxST_ENERG_GENInfo.AVL_I_GEN_DRV; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxST_ENERG_GENInfo_LDST_GEN_DRV(data) do \
{ \
    *data = Proxy_RxST_ENERG_GENInfo.LDST_GEN_DRV; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxST_ENERG_GENInfo_ST_LDREL_GEN(data) do \
{ \
    *data = Proxy_RxST_ENERG_GENInfo.ST_LDREL_GEN; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxST_ENERG_GENInfo_ST_CHG_STOR(data) do \
{ \
    *data = Proxy_RxST_ENERG_GENInfo.ST_CHG_STOR; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxST_ENERG_GENInfo_TEMP_BT_14V(data) do \
{ \
    *data = Proxy_RxST_ENERG_GENInfo.TEMP_BT_14V; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxST_ENERG_GENInfo_ST_I_IBS(data) do \
{ \
    *data = Proxy_RxST_ENERG_GENInfo.ST_I_IBS; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxST_ENERG_GENInfo_ST_SEP_STOR(data) do \
{ \
    *data = Proxy_RxST_ENERG_GENInfo.ST_SEP_STOR; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxST_ENERG_GENInfo_ST_BN2_SCP(data) do \
{ \
    *data = Proxy_RxST_ENERG_GENInfo.ST_BN2_SCP; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_PT_1Info_RQ_SHPA_GRB_REGE_PAFI(data) do \
{ \
    *data = Proxy_RxDT_PT_1Info.RQ_SHPA_GRB_REGE_PAFI; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_PT_1Info_ST_RTIR_DRV(data) do \
{ \
    *data = Proxy_RxDT_PT_1Info.ST_RTIR_DRV; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_PT_1Info_CTR_SLCK_DRV(data) do \
{ \
    *data = Proxy_RxDT_PT_1Info.CTR_SLCK_DRV; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_PT_1Info_RQ_STASS_ENGMG(data) do \
{ \
    *data = Proxy_RxDT_PT_1Info.RQ_STASS_ENGMG; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_PT_1Info_ST_CAT_HT(data) do \
{ \
    *data = Proxy_RxDT_PT_1Info.ST_CAT_HT; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_PT_1Info_RQ_SHPA_GRB_CHGBLC(data) do \
{ \
    *data = Proxy_RxDT_PT_1Info.RQ_SHPA_GRB_CHGBLC; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_PT_1Info_TAR_RPM_IDLG_DRV_EXT(data) do \
{ \
    *data = Proxy_RxDT_PT_1Info.TAR_RPM_IDLG_DRV_EXT; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_PT_1Info_SLCTN_BUS_COMM_ENG_GRB(data) do \
{ \
    *data = Proxy_RxDT_PT_1Info.SLCTN_BUS_COMM_ENG_GRB; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_PT_1Info_TAR_RPM_IDLG_DRV(data) do \
{ \
    *data = Proxy_RxDT_PT_1Info.TAR_RPM_IDLG_DRV; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_PT_1Info_ST_INFS_DRV(data) do \
{ \
    *data = Proxy_RxDT_PT_1Info.ST_INFS_DRV; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_PT_1Info_ST_SW_WAUP_DRV(data) do \
{ \
    *data = Proxy_RxDT_PT_1Info.ST_SW_WAUP_DRV; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_PT_1Info_AIP_ENG_DRV(data) do \
{ \
    *data = Proxy_RxDT_PT_1Info.AIP_ENG_DRV; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_PT_1Info_RDUC_DOCTR_RPM_DRV(data) do \
{ \
    *data = Proxy_RxDT_PT_1Info.RDUC_DOCTR_RPM_DRV; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_GRDT_DRVInfo_ST_OPMO_GRDT_DRV(data) do \
{ \
    *data = Proxy_RxDT_GRDT_DRVInfo.ST_OPMO_GRDT_DRV; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_GRDT_DRVInfo_ST_RSTA_GRDT(data) do \
{ \
    *data = Proxy_RxDT_GRDT_DRVInfo.ST_RSTA_GRDT; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDIAG_OBD_ENGMG_ELInfo_RQ_MIL_DIAG_OBD_ENGMG_EL(data) do \
{ \
    *data = Proxy_RxDIAG_OBD_ENGMG_ELInfo.RQ_MIL_DIAG_OBD_ENGMG_EL; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDIAG_OBD_ENGMG_ELInfo_RQ_RST_OBD_DIAG(data) do \
{ \
    *data = Proxy_RxDIAG_OBD_ENGMG_ELInfo.RQ_RST_OBD_DIAG; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSTAT_CT_HABRInfo_ST_CT_HABR(data) do \
{ \
    *data = Proxy_RxSTAT_CT_HABRInfo.ST_CT_HABR; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_PT_3Info_TRNRAO_BAX(data) do \
{ \
    *data = Proxy_RxDT_PT_3Info.TRNRAO_BAX; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxDT_PT_3Info_QU_TRNRAO_BAX(data) do \
{ \
    *data = Proxy_RxDT_PT_3Info.QU_TRNRAO_BAX; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxEINHEITEN_BN2020Info_UN_TORQ_S_MOD(data) do \
{ \
    *data = Proxy_RxEINHEITEN_BN2020Info.UN_TORQ_S_MOD; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxEINHEITEN_BN2020Info_UN_PWR_S_MOD(data) do \
{ \
    *data = Proxy_RxEINHEITEN_BN2020Info.UN_PWR_S_MOD; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxEINHEITEN_BN2020Info_UN_DATE_EXT(data) do \
{ \
    *data = Proxy_RxEINHEITEN_BN2020Info.UN_DATE_EXT; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxEINHEITEN_BN2020Info_UN_COSP_EL(data) do \
{ \
    *data = Proxy_RxEINHEITEN_BN2020Info.UN_COSP_EL; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxEINHEITEN_BN2020Info_UN_TEMP(data) do \
{ \
    *data = Proxy_RxEINHEITEN_BN2020Info.UN_TEMP; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxEINHEITEN_BN2020Info_UN_AIP(data) do \
{ \
    *data = Proxy_RxEINHEITEN_BN2020Info.UN_AIP; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxEINHEITEN_BN2020Info_LANG(data) do \
{ \
    *data = Proxy_RxEINHEITEN_BN2020Info.LANG; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxEINHEITEN_BN2020Info_UN_DATE(data) do \
{ \
    *data = Proxy_RxEINHEITEN_BN2020Info.UN_DATE; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxEINHEITEN_BN2020Info_UN_T(data) do \
{ \
    *data = Proxy_RxEINHEITEN_BN2020Info.UN_T; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxEINHEITEN_BN2020Info_UN_SPDM_DGTL(data) do \
{ \
    *data = Proxy_RxEINHEITEN_BN2020Info.UN_SPDM_DGTL; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxEINHEITEN_BN2020Info_UN_MILE(data) do \
{ \
    *data = Proxy_RxEINHEITEN_BN2020Info.UN_MILE; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxEINHEITEN_BN2020Info_UN_FU(data) do \
{ \
    *data = Proxy_RxEINHEITEN_BN2020Info.UN_FU; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxEINHEITEN_BN2020Info_UN_COSP(data) do \
{ \
    *data = Proxy_RxEINHEITEN_BN2020Info.UN_COSP; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxA_TEMPInfo_TEMP_EX_UNFILT(data) do \
{ \
    *data = Proxy_RxA_TEMPInfo.TEMP_EX_UNFILT; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxA_TEMPInfo_TEMP_EX(data) do \
{ \
    *data = Proxy_RxA_TEMPInfo.TEMP_EX; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWISCHERGESCHWINDIGKEITInfo_ST_RNSE(data) do \
{ \
    *data = Proxy_RxWISCHERGESCHWINDIGKEITInfo.ST_RNSE; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWISCHERGESCHWINDIGKEITInfo_INT_RN(data) do \
{ \
    *data = Proxy_RxWISCHERGESCHWINDIGKEITInfo.INT_RN; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxWISCHERGESCHWINDIGKEITInfo_V_WI(data) do \
{ \
    *data = Proxy_RxWISCHERGESCHWINDIGKEITInfo.V_WI; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSTAT_DS_VRFDInfo_ST_DSW_DRD_SFY_CTRL(data) do \
{ \
    *data = Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_DRD_SFY_CTRL; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSTAT_DS_VRFDInfo_ST_DSW_DVDR_VRFD(data) do \
{ \
    *data = Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_DVDR_VRFD; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSTAT_DS_VRFDInfo_ST_DSW_DVDR_SFY_CTRL(data) do \
{ \
    *data = Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_DVDR_SFY_CTRL; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSTAT_DS_VRFDInfo_ST_DSW_PSDR_SFY_CTRL(data) do \
{ \
    *data = Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_PSDR_SFY_CTRL; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSTAT_DS_VRFDInfo_ST_DSW_PSD_SFY_CTRL(data) do \
{ \
    *data = Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_PSD_SFY_CTRL; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSTAT_DS_VRFDInfo_ST_DSW_DRD_VRFD(data) do \
{ \
    *data = Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_DRD_VRFD; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSTAT_DS_VRFDInfo_ST_DSW_PSDR_VRFD(data) do \
{ \
    *data = Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_PSDR_VRFD; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSTAT_DS_VRFDInfo_ST_DSW_PSD_VRFD(data) do \
{ \
    *data = Proxy_RxSTAT_DS_VRFDInfo.ST_DSW_PSD_VRFD; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info_AVL_MOD_SW_DRDY(data) do \
{ \
    *data = Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info_RQ_SW_DRDY_MMID(data) do \
{ \
    *data = Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.RQ_SW_DRDY_MMID; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info_RQ_SU_SW_DRDY(data) do \
{ \
    *data = Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.RQ_SU_SW_DRDY; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info_RQ_SW_DRDY_KDIS(data) do \
{ \
    *data = Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.RQ_SW_DRDY_KDIS; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info_AVL_MOD_SW_DRDY_CHAS(data) do \
{ \
    *data = Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY_CHAS; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info_AVL_MOD_SW_DRDY_DRV(data) do \
{ \
    *data = Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY_DRV; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info_AVL_MOD_SW_DRDY_STAB(data) do \
{ \
    *data = Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.AVL_MOD_SW_DRDY_STAB; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info_DISP_ST_DSC(data) do \
{ \
    *data = Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info.DISP_ST_DSC; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSTAT_ANHAENGERInfo_ST_SYNCN_HAZWCL_TRAI(data) do \
{ \
    *data = Proxy_RxSTAT_ANHAENGERInfo.ST_SYNCN_HAZWCL_TRAI; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSTAT_ANHAENGERInfo_ST_RFLI_DF_TRAI(data) do \
{ \
    *data = Proxy_RxSTAT_ANHAENGERInfo.ST_RFLI_DF_TRAI; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSTAT_ANHAENGERInfo_ST_TRAI(data) do \
{ \
    *data = Proxy_RxSTAT_ANHAENGERInfo.ST_TRAI; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSTAT_ANHAENGERInfo_ST_DI_DF_TRAI(data) do \
{ \
    *data = Proxy_RxSTAT_ANHAENGERInfo.ST_DI_DF_TRAI; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxSTAT_ANHAENGERInfo_ST_PO_AHV(data) do \
{ \
    *data = Proxy_RxSTAT_ANHAENGERInfo.ST_PO_AHV; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxFZZSTDInfo_ST_ILK_ERRM_FZM(data) do \
{ \
    *data = Proxy_RxFZZSTDInfo.ST_ILK_ERRM_FZM; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxFZZSTDInfo_ST_ENERG_FZM(data) do \
{ \
    *data = Proxy_RxFZZSTDInfo.ST_ENERG_FZM; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxFZZSTDInfo_ST_BT_PROTE_WUP(data) do \
{ \
    *data = Proxy_RxFZZSTDInfo.ST_BT_PROTE_WUP; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxBEDIENUNG_FAHRWERKInfo_OP_MOD_TRCT_DSC(data) do \
{ \
    *data = Proxy_RxBEDIENUNG_FAHRWERKInfo.OP_MOD_TRCT_DSC; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxBEDIENUNG_FAHRWERKInfo_OP_TPCT(data) do \
{ \
    *data = Proxy_RxBEDIENUNG_FAHRWERKInfo.OP_TPCT; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxFAHRZEUGTYPInfo_QUAN_CYL(data) do \
{ \
    *data = Proxy_RxFAHRZEUGTYPInfo.QUAN_CYL; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxFAHRZEUGTYPInfo_QUAN_GR(data) do \
{ \
    *data = Proxy_RxFAHRZEUGTYPInfo.QUAN_GR; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxFAHRZEUGTYPInfo_TYP_VEH(data) do \
{ \
    *data = Proxy_RxFAHRZEUGTYPInfo.TYP_VEH; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxFAHRZEUGTYPInfo_TYP_BODY(data) do \
{ \
    *data = Proxy_RxFAHRZEUGTYPInfo.TYP_BODY; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxFAHRZEUGTYPInfo_TYP_ENG(data) do \
{ \
    *data = Proxy_RxFAHRZEUGTYPInfo.TYP_ENG; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxFAHRZEUGTYPInfo_CLAS_PWR(data) do \
{ \
    *data = Proxy_RxFAHRZEUGTYPInfo.CLAS_PWR; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxFAHRZEUGTYPInfo_TYP_CNT(data) do \
{ \
    *data = Proxy_RxFAHRZEUGTYPInfo.TYP_CNT; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxFAHRZEUGTYPInfo_TYP_STE(data) do \
{ \
    *data = Proxy_RxFAHRZEUGTYPInfo.TYP_STE; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxFAHRZEUGTYPInfo_TYP_GRB(data) do \
{ \
    *data = Proxy_RxFAHRZEUGTYPInfo.TYP_GRB; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxFAHRZEUGTYPInfo_TYP_CAPA(data) do \
{ \
    *data = Proxy_RxFAHRZEUGTYPInfo.TYP_CAPA; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxST_BLT_CT_SOCCUInfo_ST_SEAT_OCCU_RRH(data) do \
{ \
    *data = Proxy_RxST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_RRH; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxST_BLT_CT_SOCCUInfo_ST_BLTB_SW_RM(data) do \
{ \
    *data = Proxy_RxST_BLT_CT_SOCCUInfo.ST_BLTB_SW_RM; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxST_BLT_CT_SOCCUInfo_ST_BLTB_SW_RRH(data) do \
{ \
    *data = Proxy_RxST_BLT_CT_SOCCUInfo.ST_BLTB_SW_RRH; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxST_BLT_CT_SOCCUInfo_ST_ERR_SEAT_MT_DR(data) do \
{ \
    *data = Proxy_RxST_BLT_CT_SOCCUInfo.ST_ERR_SEAT_MT_DR; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxST_BLT_CT_SOCCUInfo_ST_BLTB_SW_DR(data) do \
{ \
    *data = Proxy_RxST_BLT_CT_SOCCUInfo.ST_BLTB_SW_DR; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxST_BLT_CT_SOCCUInfo_ST_SEAT_OCCU_DR(data) do \
{ \
    *data = Proxy_RxST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_DR; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxST_BLT_CT_SOCCUInfo_ST_BLTB_SW_RLH(data) do \
{ \
    *data = Proxy_RxST_BLT_CT_SOCCUInfo.ST_BLTB_SW_RLH; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxST_BLT_CT_SOCCUInfo_ST_SEAT_OCCU_RLH(data) do \
{ \
    *data = Proxy_RxST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_RLH; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxST_BLT_CT_SOCCUInfo_ST_BLTB_SW_PS(data) do \
{ \
    *data = Proxy_RxST_BLT_CT_SOCCUInfo.ST_BLTB_SW_PS; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxST_BLT_CT_SOCCUInfo_ST_SEAT_OCCU_PS(data) do \
{ \
    *data = Proxy_RxST_BLT_CT_SOCCUInfo.ST_SEAT_OCCU_PS; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxFAHRGESTELLNUMMERInfo_NO_VECH_1(data) do \
{ \
    *data = Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_1; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxFAHRGESTELLNUMMERInfo_NO_VECH_2(data) do \
{ \
    *data = Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_2; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxFAHRGESTELLNUMMERInfo_NO_VECH_3(data) do \
{ \
    *data = Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_3; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxFAHRGESTELLNUMMERInfo_NO_VECH_6(data) do \
{ \
    *data = Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_6; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxFAHRGESTELLNUMMERInfo_NO_VECH_7(data) do \
{ \
    *data = Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_7; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxFAHRGESTELLNUMMERInfo_NO_VECH_4(data) do \
{ \
    *data = Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_4; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxFAHRGESTELLNUMMERInfo_NO_VECH_5(data) do \
{ \
    *data = Proxy_RxFAHRGESTELLNUMMERInfo.NO_VECH_5; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRELATIVZEITInfo_T_SEC_COU_REL(data) do \
{ \
    *data = Proxy_RxRELATIVZEITInfo.T_SEC_COU_REL; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxRELATIVZEITInfo_T_DAY_COU_ABSL(data) do \
{ \
    *data = Proxy_RxRELATIVZEITInfo.T_DAY_COU_ABSL; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxKILOMETERSTANDInfo_RNG(data) do \
{ \
    *data = Proxy_RxKILOMETERSTANDInfo.RNG; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxKILOMETERSTANDInfo_ST_FLLV_FUTA_SPAR(data) do \
{ \
    *data = Proxy_RxKILOMETERSTANDInfo.ST_FLLV_FUTA_SPAR; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxKILOMETERSTANDInfo_FLLV_FUTA_RH(data) do \
{ \
    *data = Proxy_RxKILOMETERSTANDInfo.FLLV_FUTA_RH; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxKILOMETERSTANDInfo_FLLV_FUTA_LH(data) do \
{ \
    *data = Proxy_RxKILOMETERSTANDInfo.FLLV_FUTA_LH; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxKILOMETERSTANDInfo_FLLV_FUTA(data) do \
{ \
    *data = Proxy_RxKILOMETERSTANDInfo.FLLV_FUTA; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxKILOMETERSTANDInfo_MILE_KM(data) do \
{ \
    *data = Proxy_RxKILOMETERSTANDInfo.MILE_KM; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxUHRZEIT_DATUMInfo_DISP_DATE_WDAY(data) do \
{ \
    *data = Proxy_RxUHRZEIT_DATUMInfo.DISP_DATE_WDAY; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxUHRZEIT_DATUMInfo_DISP_DATE_DAY(data) do \
{ \
    *data = Proxy_RxUHRZEIT_DATUMInfo.DISP_DATE_DAY; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxUHRZEIT_DATUMInfo_DISP_DATE_MON(data) do \
{ \
    *data = Proxy_RxUHRZEIT_DATUMInfo.DISP_DATE_MON; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxUHRZEIT_DATUMInfo_ST_DISP_CTI_DATE(data) do \
{ \
    *data = Proxy_RxUHRZEIT_DATUMInfo.ST_DISP_CTI_DATE; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxUHRZEIT_DATUMInfo_DISP_DATE_YR(data) do \
{ \
    *data = Proxy_RxUHRZEIT_DATUMInfo.DISP_DATE_YR; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxUHRZEIT_DATUMInfo_DISP_HR(data) do \
{ \
    *data = Proxy_RxUHRZEIT_DATUMInfo.DISP_HR; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxUHRZEIT_DATUMInfo_DISP_SEC(data) do \
{ \
    *data = Proxy_RxUHRZEIT_DATUMInfo.DISP_SEC; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxUHRZEIT_DATUMInfo_DISP_MN(data) do \
{ \
    *data = Proxy_RxUHRZEIT_DATUMInfo.DISP_MN; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxEcuModeSts(data) do \
{ \
    *data = Proxy_RxEcuModeSts; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxPCtrlAct(data) do \
{ \
    *data = Proxy_RxPCtrlAct; \
}while(0);

#define Proxy_Rx_Read_Proxy_RxFuncInhibitProxySts(data) do \
{ \
    *data = Proxy_RxFuncInhibitProxySts; \
}while(0);


/* Set Output DE MAcro Function */
#define Proxy_Rx_Write_Proxy_RxCanRxRegenInfo(data) do \
{ \
    Proxy_RxCanRxRegenInfo = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxAccelPedlInfo(data) do \
{ \
    Proxy_RxCanRxAccelPedlInfo = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxIdbInfo(data) do \
{ \
    Proxy_RxCanRxIdbInfo = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEngTempInfo(data) do \
{ \
    Proxy_RxCanRxEngTempInfo = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo(data) do \
{ \
    Proxy_RxCanRxEscInfo = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxClu2Info(data) do \
{ \
    Proxy_RxCanRxClu2Info = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEemInfo(data) do \
{ \
    Proxy_RxCanRxEemInfo = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxInfo(data) do \
{ \
    Proxy_RxCanRxInfo = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxIMUCalcInfo(data) do \
{ \
    Proxy_RxIMUCalcInfo = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxRegenInfo_HcuRegenEna(data) do \
{ \
    Proxy_RxCanRxRegenInfo.HcuRegenEna = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxRegenInfo_HcuRegenBrkTq(data) do \
{ \
    Proxy_RxCanRxRegenInfo.HcuRegenBrkTq = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxAccelPedlInfo_AccelPedlVal(data) do \
{ \
    Proxy_RxCanRxAccelPedlInfo.AccelPedlVal = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxAccelPedlInfo_AccelPedlValErr(data) do \
{ \
    Proxy_RxCanRxAccelPedlInfo.AccelPedlValErr = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxIdbInfo_EngMsgFault(data) do \
{ \
    Proxy_RxCanRxIdbInfo.EngMsgFault = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxIdbInfo_TarGearPosi(data) do \
{ \
    Proxy_RxCanRxIdbInfo.TarGearPosi = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxIdbInfo_GearSelDisp(data) do \
{ \
    Proxy_RxCanRxIdbInfo.GearSelDisp = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxIdbInfo_TcuSwiGs(data) do \
{ \
    Proxy_RxCanRxIdbInfo.TcuSwiGs = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxIdbInfo_HcuServiceMod(data) do \
{ \
    Proxy_RxCanRxIdbInfo.HcuServiceMod = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxIdbInfo_SubCanBusSigFault(data) do \
{ \
    Proxy_RxCanRxIdbInfo.SubCanBusSigFault = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxIdbInfo_CoolantTemp(data) do \
{ \
    Proxy_RxCanRxIdbInfo.CoolantTemp = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxIdbInfo_CoolantTempErr(data) do \
{ \
    Proxy_RxCanRxIdbInfo.CoolantTempErr = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEngTempInfo_EngTemp(data) do \
{ \
    Proxy_RxCanRxEngTempInfo.EngTemp = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEngTempInfo_EngTempErr(data) do \
{ \
    Proxy_RxCanRxEngTempInfo.EngTempErr = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_Ax(data) do \
{ \
    Proxy_RxCanRxEscInfo.Ax = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_YawRate(data) do \
{ \
    Proxy_RxCanRxEscInfo.YawRate = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_SteeringAngle(data) do \
{ \
    Proxy_RxCanRxEscInfo.SteeringAngle = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_Ay(data) do \
{ \
    Proxy_RxCanRxEscInfo.Ay = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_PbSwt(data) do \
{ \
    Proxy_RxCanRxEscInfo.PbSwt = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_ClutchSwt(data) do \
{ \
    Proxy_RxCanRxEscInfo.ClutchSwt = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_GearRSwt(data) do \
{ \
    Proxy_RxCanRxEscInfo.GearRSwt = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_EngActIndTq(data) do \
{ \
    Proxy_RxCanRxEscInfo.EngActIndTq = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_EngRpm(data) do \
{ \
    Proxy_RxCanRxEscInfo.EngRpm = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_EngIndTq(data) do \
{ \
    Proxy_RxCanRxEscInfo.EngIndTq = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_EngFrictionLossTq(data) do \
{ \
    Proxy_RxCanRxEscInfo.EngFrictionLossTq = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_EngStdTq(data) do \
{ \
    Proxy_RxCanRxEscInfo.EngStdTq = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_TurbineRpm(data) do \
{ \
    Proxy_RxCanRxEscInfo.TurbineRpm = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_ThrottleAngle(data) do \
{ \
    Proxy_RxCanRxEscInfo.ThrottleAngle = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_TpsResol1000(data) do \
{ \
    Proxy_RxCanRxEscInfo.TpsResol1000 = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_PvAvCanResol1000(data) do \
{ \
    Proxy_RxCanRxEscInfo.PvAvCanResol1000 = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_EngChr(data) do \
{ \
    Proxy_RxCanRxEscInfo.EngChr = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_EngVol(data) do \
{ \
    Proxy_RxCanRxEscInfo.EngVol = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_GearType(data) do \
{ \
    Proxy_RxCanRxEscInfo.GearType = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_EngClutchState(data) do \
{ \
    Proxy_RxCanRxEscInfo.EngClutchState = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_EngTqCmdBeforeIntv(data) do \
{ \
    Proxy_RxCanRxEscInfo.EngTqCmdBeforeIntv = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_MotEstTq(data) do \
{ \
    Proxy_RxCanRxEscInfo.MotEstTq = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_MotTqCmdBeforeIntv(data) do \
{ \
    Proxy_RxCanRxEscInfo.MotTqCmdBeforeIntv = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_TqIntvTCU(data) do \
{ \
    Proxy_RxCanRxEscInfo.TqIntvTCU = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_TqIntvSlowTCU(data) do \
{ \
    Proxy_RxCanRxEscInfo.TqIntvSlowTCU = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_TqIncReq(data) do \
{ \
    Proxy_RxCanRxEscInfo.TqIncReq = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_DecelReq(data) do \
{ \
    Proxy_RxCanRxEscInfo.DecelReq = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_RainSnsStat(data) do \
{ \
    Proxy_RxCanRxEscInfo.RainSnsStat = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_EngSpdErr(data) do \
{ \
    Proxy_RxCanRxEscInfo.EngSpdErr = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_AtType(data) do \
{ \
    Proxy_RxCanRxEscInfo.AtType = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_MtType(data) do \
{ \
    Proxy_RxCanRxEscInfo.MtType = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_CvtType(data) do \
{ \
    Proxy_RxCanRxEscInfo.CvtType = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_DctType(data) do \
{ \
    Proxy_RxCanRxEscInfo.DctType = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_HevAtTcu(data) do \
{ \
    Proxy_RxCanRxEscInfo.HevAtTcu = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_TurbineRpmErr(data) do \
{ \
    Proxy_RxCanRxEscInfo.TurbineRpmErr = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_ThrottleAngleErr(data) do \
{ \
    Proxy_RxCanRxEscInfo.ThrottleAngleErr = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_TopTrvlCltchSwtAct(data) do \
{ \
    Proxy_RxCanRxEscInfo.TopTrvlCltchSwtAct = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_TopTrvlCltchSwtActV(data) do \
{ \
    Proxy_RxCanRxEscInfo.TopTrvlCltchSwtActV = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_HillDesCtrlMdSwtAct(data) do \
{ \
    Proxy_RxCanRxEscInfo.HillDesCtrlMdSwtAct = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_HillDesCtrlMdSwtActV(data) do \
{ \
    Proxy_RxCanRxEscInfo.HillDesCtrlMdSwtActV = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_WiperIntSW(data) do \
{ \
    Proxy_RxCanRxEscInfo.WiperIntSW = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_WiperLow(data) do \
{ \
    Proxy_RxCanRxEscInfo.WiperLow = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_WiperHigh(data) do \
{ \
    Proxy_RxCanRxEscInfo.WiperHigh = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_WiperValid(data) do \
{ \
    Proxy_RxCanRxEscInfo.WiperValid = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_WiperAuto(data) do \
{ \
    Proxy_RxCanRxEscInfo.WiperAuto = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEscInfo_TcuFaultSts(data) do \
{ \
    Proxy_RxCanRxEscInfo.TcuFaultSts = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxClu2Info_IgnRun(data) do \
{ \
    Proxy_RxCanRxClu2Info.IgnRun = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEemInfo_YawRateInvld(data) do \
{ \
    Proxy_RxCanRxEemInfo.YawRateInvld = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEemInfo_AyInvld(data) do \
{ \
    Proxy_RxCanRxEemInfo.AyInvld = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEemInfo_SteeringAngleRxOk(data) do \
{ \
    Proxy_RxCanRxEemInfo.SteeringAngleRxOk = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEemInfo_AxInvldData(data) do \
{ \
    Proxy_RxCanRxEemInfo.AxInvldData = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEemInfo_Ems1RxErr(data) do \
{ \
    Proxy_RxCanRxEemInfo.Ems1RxErr = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEemInfo_Ems2RxErr(data) do \
{ \
    Proxy_RxCanRxEemInfo.Ems2RxErr = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEemInfo_Tcu1RxErr(data) do \
{ \
    Proxy_RxCanRxEemInfo.Tcu1RxErr = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEemInfo_Tcu5RxErr(data) do \
{ \
    Proxy_RxCanRxEemInfo.Tcu5RxErr = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEemInfo_Hcu1RxErr(data) do \
{ \
    Proxy_RxCanRxEemInfo.Hcu1RxErr = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEemInfo_Hcu2RxErr(data) do \
{ \
    Proxy_RxCanRxEemInfo.Hcu2RxErr = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxEemInfo_Hcu3RxErr(data) do \
{ \
    Proxy_RxCanRxEemInfo.Hcu3RxErr = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxInfo_MainBusOffFlag(data) do \
{ \
    Proxy_RxCanRxInfo.MainBusOffFlag = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxInfo_SenBusOffFlag(data) do \
{ \
    Proxy_RxCanRxInfo.SenBusOffFlag = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxIMUCalcInfo_Reverse_Gear_flg(data) do \
{ \
    Proxy_RxIMUCalcInfo.Reverse_Gear_flg = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxIMUCalcInfo_Reverse_Judge_Time(data) do \
{ \
    Proxy_RxIMUCalcInfo.Reverse_Judge_Time = *data; \
}while(0);

#define Proxy_Rx_Write_Proxy_RxCanRxGearSelDispErrInfo(data) do \
{ \
    Proxy_RxCanRxGearSelDispErrInfo = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PROXY_RX_IFA_H_ */
/** @} */
