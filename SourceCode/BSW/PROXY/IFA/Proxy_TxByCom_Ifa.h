/**
 * @defgroup Proxy_TxByCom_Ifa Proxy_TxByCom_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Proxy_TxByCom_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PROXY_TXBYCOM_IFA_H_
#define PROXY_TXBYCOM_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Proxy_TxByCom_Read_Proxy_TxByComTxYawCbitInfo(data) do \
{ \
    *data = Proxy_TxByComTxYawCbitInfo; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxWhlSpdInfo(data) do \
{ \
    *data = Proxy_TxByComTxWhlSpdInfo; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxWhlPulInfo(data) do \
{ \
    *data = Proxy_TxByComTxWhlPulInfo; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxTcs5Info(data) do \
{ \
    *data = Proxy_TxByComTxTcs5Info; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxTcs1Info(data) do \
{ \
    *data = Proxy_TxByComTxTcs1Info; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxSasCalInfo(data) do \
{ \
    *data = Proxy_TxByComTxSasCalInfo; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxEsp2Info(data) do \
{ \
    *data = Proxy_TxByComTxEsp2Info; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxEbs1Info(data) do \
{ \
    *data = Proxy_TxByComTxEbs1Info; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxAhb1Info(data) do \
{ \
    *data = Proxy_TxByComTxAhb1Info; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComSenPwrMonitorData(data) do \
{ \
    *data = Proxy_TxByComSenPwrMonitorData; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo(data) do \
{ \
    *data = Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo(data) do \
{ \
    *data = Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_BRTORQ_WHLInfo(data) do \
{ \
    *data = Proxy_TxByComAVL_BRTORQ_WHLInfo; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_RPM_WHLInfo(data) do \
{ \
    *data = Proxy_TxByComAVL_RPM_WHLInfo; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComRQ_WMOM_PT_SUM_RECUPInfo(data) do \
{ \
    *data = Proxy_TxByComRQ_WMOM_PT_SUM_RECUPInfo; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info(data) do \
{ \
    *data = Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo(data) do \
{ \
    *data = Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_QUAN_EES_WHLInfo(data) do \
{ \
    *data = Proxy_TxByComAVL_QUAN_EES_WHLInfo; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTAR_LTRQD_BAXInfo(data) do \
{ \
    *data = Proxy_TxByComTAR_LTRQD_BAXInfo; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComST_YMRInfo(data) do \
{ \
    *data = Proxy_TxByComST_YMRInfo; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComWEGSTRECKEInfo(data) do \
{ \
    *data = Proxy_TxByComWEGSTRECKEInfo; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComDISP_CC_DRDY_00Info(data) do \
{ \
    *data = Proxy_TxByComDISP_CC_DRDY_00Info; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComDISP_CC_BYPA_00Info(data) do \
{ \
    *data = Proxy_TxByComDISP_CC_BYPA_00Info; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComST_VHSSInfo(data) do \
{ \
    *data = Proxy_TxByComST_VHSSInfo; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComDIAG_OBD_HYB_1Info(data) do \
{ \
    *data = Proxy_TxByComDIAG_OBD_HYB_1Info; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComSU_DSCInfo(data) do \
{ \
    *data = Proxy_TxByComSU_DSCInfo; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComST_TYR_RDCInfo(data) do \
{ \
    *data = Proxy_TxByComST_TYR_RDCInfo; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComST_TYR_RPAInfo(data) do \
{ \
    *data = Proxy_TxByComST_TYR_RPAInfo; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info(data) do \
{ \
    *data = Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComST_TYR_2Info(data) do \
{ \
    *data = Proxy_TxByComST_TYR_2Info; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_T_TYRInfo(data) do \
{ \
    *data = Proxy_TxByComAVL_T_TYRInfo; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTEMP_BRKInfo(data) do \
{ \
    *data = Proxy_TxByComTEMP_BRKInfo; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_P_TYRInfo(data) do \
{ \
    *data = Proxy_TxByComAVL_P_TYRInfo; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComFR_DBG_DSCInfo(data) do \
{ \
    *data = Proxy_TxByComFR_DBG_DSCInfo; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxYawCbitInfo_TestYawRateSensor(data) do \
{ \
    *data = Proxy_TxByComTxYawCbitInfo.TestYawRateSensor; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxYawCbitInfo_TestAcclSensor(data) do \
{ \
    *data = Proxy_TxByComTxYawCbitInfo.TestAcclSensor; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxYawCbitInfo_YawSerialNumberReq(data) do \
{ \
    *data = Proxy_TxByComTxYawCbitInfo.YawSerialNumberReq; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxWhlSpdInfo_WhlSpdFL(data) do \
{ \
    *data = Proxy_TxByComTxWhlSpdInfo.WhlSpdFL; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxWhlSpdInfo_WhlSpdFR(data) do \
{ \
    *data = Proxy_TxByComTxWhlSpdInfo.WhlSpdFR; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxWhlSpdInfo_WhlSpdRL(data) do \
{ \
    *data = Proxy_TxByComTxWhlSpdInfo.WhlSpdRL; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxWhlSpdInfo_WhlSpdRR(data) do \
{ \
    *data = Proxy_TxByComTxWhlSpdInfo.WhlSpdRR; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxWhlPulInfo_WhlPulFL(data) do \
{ \
    *data = Proxy_TxByComTxWhlPulInfo.WhlPulFL; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxWhlPulInfo_WhlPulFR(data) do \
{ \
    *data = Proxy_TxByComTxWhlPulInfo.WhlPulFR; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxWhlPulInfo_WhlPulRL(data) do \
{ \
    *data = Proxy_TxByComTxWhlPulInfo.WhlPulRL; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxWhlPulInfo_WhlPulRR(data) do \
{ \
    *data = Proxy_TxByComTxWhlPulInfo.WhlPulRR; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxTcs5Info_CfBrkAbsWLMP(data) do \
{ \
    *data = Proxy_TxByComTxTcs5Info.CfBrkAbsWLMP; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxTcs5Info_CfBrkEbdWLMP(data) do \
{ \
    *data = Proxy_TxByComTxTcs5Info.CfBrkEbdWLMP; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxTcs5Info_CfBrkTcsWLMP(data) do \
{ \
    *data = Proxy_TxByComTxTcs5Info.CfBrkTcsWLMP; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxTcs5Info_CfBrkTcsFLMP(data) do \
{ \
    *data = Proxy_TxByComTxTcs5Info.CfBrkTcsFLMP; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxTcs5Info_CfBrkAbsDIAG(data) do \
{ \
    *data = Proxy_TxByComTxTcs5Info.CfBrkAbsDIAG; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxTcs5Info_CrBrkWheelFL_KMH(data) do \
{ \
    *data = Proxy_TxByComTxTcs5Info.CrBrkWheelFL_KMH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxTcs5Info_CrBrkWheelFR_KMH(data) do \
{ \
    *data = Proxy_TxByComTxTcs5Info.CrBrkWheelFR_KMH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxTcs5Info_CrBrkWheelRL_KMH(data) do \
{ \
    *data = Proxy_TxByComTxTcs5Info.CrBrkWheelRL_KMH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxTcs5Info_CrBrkWheelRR_KMH(data) do \
{ \
    *data = Proxy_TxByComTxTcs5Info.CrBrkWheelRR_KMH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxTcs1Info_CfBrkTcsREQ(data) do \
{ \
    *data = Proxy_TxByComTxTcs1Info.CfBrkTcsREQ; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxTcs1Info_CfBrkTcsPAS(data) do \
{ \
    *data = Proxy_TxByComTxTcs1Info.CfBrkTcsPAS; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxTcs1Info_CfBrkTcsDEF(data) do \
{ \
    *data = Proxy_TxByComTxTcs1Info.CfBrkTcsDEF; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxTcs1Info_CfBrkTcsCTL(data) do \
{ \
    *data = Proxy_TxByComTxTcs1Info.CfBrkTcsCTL; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxTcs1Info_CfBrkAbsACT(data) do \
{ \
    *data = Proxy_TxByComTxTcs1Info.CfBrkAbsACT; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxTcs1Info_CfBrkAbsDEF(data) do \
{ \
    *data = Proxy_TxByComTxTcs1Info.CfBrkAbsDEF; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxTcs1Info_CfBrkEbdDEF(data) do \
{ \
    *data = Proxy_TxByComTxTcs1Info.CfBrkEbdDEF; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxTcs1Info_CfBrkTcsGSC(data) do \
{ \
    *data = Proxy_TxByComTxTcs1Info.CfBrkTcsGSC; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxTcs1Info_CfBrkEspPAS(data) do \
{ \
    *data = Proxy_TxByComTxTcs1Info.CfBrkEspPAS; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxTcs1Info_CfBrkEspDEF(data) do \
{ \
    *data = Proxy_TxByComTxTcs1Info.CfBrkEspDEF; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxTcs1Info_CfBrkEspCTL(data) do \
{ \
    *data = Proxy_TxByComTxTcs1Info.CfBrkEspCTL; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxTcs1Info_CfBrkMsrREQ(data) do \
{ \
    *data = Proxy_TxByComTxTcs1Info.CfBrkMsrREQ; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxTcs1Info_CfBrkTcsMFRN(data) do \
{ \
    *data = Proxy_TxByComTxTcs1Info.CfBrkTcsMFRN; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxTcs1Info_CfBrkMinGEAR(data) do \
{ \
    *data = Proxy_TxByComTxTcs1Info.CfBrkMinGEAR; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxTcs1Info_CfBrkMaxGEAR(data) do \
{ \
    *data = Proxy_TxByComTxTcs1Info.CfBrkMaxGEAR; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxTcs1Info_BrakeLight(data) do \
{ \
    *data = Proxy_TxByComTxTcs1Info.BrakeLight; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxTcs1Info_HacCtl(data) do \
{ \
    *data = Proxy_TxByComTxTcs1Info.HacCtl; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxTcs1Info_HacPas(data) do \
{ \
    *data = Proxy_TxByComTxTcs1Info.HacPas; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxTcs1Info_HacDef(data) do \
{ \
    *data = Proxy_TxByComTxTcs1Info.HacDef; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxTcs1Info_CrBrkTQI_PC(data) do \
{ \
    *data = Proxy_TxByComTxTcs1Info.CrBrkTQI_PC; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxTcs1Info_CrBrkTQIMSR_PC(data) do \
{ \
    *data = Proxy_TxByComTxTcs1Info.CrBrkTQIMSR_PC; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxTcs1Info_CrBrkTQISLW_PC(data) do \
{ \
    *data = Proxy_TxByComTxTcs1Info.CrBrkTQISLW_PC; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxTcs1Info_CrEbsMSGCHKSUM(data) do \
{ \
    *data = Proxy_TxByComTxTcs1Info.CrEbsMSGCHKSUM; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxSasCalInfo_CalSas_CCW(data) do \
{ \
    *data = Proxy_TxByComTxSasCalInfo.CalSas_CCW; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxSasCalInfo_CalSas_Lws_CID(data) do \
{ \
    *data = Proxy_TxByComTxSasCalInfo.CalSas_Lws_CID; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxEsp2Info_LatAccel(data) do \
{ \
    *data = Proxy_TxByComTxEsp2Info.LatAccel; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxEsp2Info_LatAccelStat(data) do \
{ \
    *data = Proxy_TxByComTxEsp2Info.LatAccelStat; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxEsp2Info_LatAccelDiag(data) do \
{ \
    *data = Proxy_TxByComTxEsp2Info.LatAccelDiag; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxEsp2Info_LongAccel(data) do \
{ \
    *data = Proxy_TxByComTxEsp2Info.LongAccel; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxEsp2Info_LongAccelStat(data) do \
{ \
    *data = Proxy_TxByComTxEsp2Info.LongAccelStat; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxEsp2Info_LongAccelDiag(data) do \
{ \
    *data = Proxy_TxByComTxEsp2Info.LongAccelDiag; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxEsp2Info_YawRate(data) do \
{ \
    *data = Proxy_TxByComTxEsp2Info.YawRate; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxEsp2Info_YawRateStat(data) do \
{ \
    *data = Proxy_TxByComTxEsp2Info.YawRateStat; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxEsp2Info_YawRateDiag(data) do \
{ \
    *data = Proxy_TxByComTxEsp2Info.YawRateDiag; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxEsp2Info_CylPres(data) do \
{ \
    *data = Proxy_TxByComTxEsp2Info.CylPres; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxEsp2Info_CylPresStat(data) do \
{ \
    *data = Proxy_TxByComTxEsp2Info.CylPresStat; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxEsp2Info_CylPresDiag(data) do \
{ \
    *data = Proxy_TxByComTxEsp2Info.CylPresDiag; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxEbs1Info_CfBrkEbsStat(data) do \
{ \
    *data = Proxy_TxByComTxEbs1Info.CfBrkEbsStat; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxEbs1Info_CrBrkRegenTQLimit_NM(data) do \
{ \
    *data = Proxy_TxByComTxEbs1Info.CrBrkRegenTQLimit_NM; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxEbs1Info_CrBrkEstTot_NM(data) do \
{ \
    *data = Proxy_TxByComTxEbs1Info.CrBrkEstTot_NM; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxEbs1Info_CfBrkSnsFail(data) do \
{ \
    *data = Proxy_TxByComTxEbs1Info.CfBrkSnsFail; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxEbs1Info_CfBrkRbsWLamp(data) do \
{ \
    *data = Proxy_TxByComTxEbs1Info.CfBrkRbsWLamp; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxEbs1Info_CfBrkVacuumSysDef(data) do \
{ \
    *data = Proxy_TxByComTxEbs1Info.CfBrkVacuumSysDef; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxEbs1Info_CrBrkEstHyd_NM(data) do \
{ \
    *data = Proxy_TxByComTxEbs1Info.CrBrkEstHyd_NM; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxEbs1Info_CrBrkStkDep_PC(data) do \
{ \
    *data = Proxy_TxByComTxEbs1Info.CrBrkStkDep_PC; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxEbs1Info_CfBrkPgmRun1(data) do \
{ \
    *data = Proxy_TxByComTxEbs1Info.CfBrkPgmRun1; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxAhb1Info_CfAhbWLMP(data) do \
{ \
    *data = Proxy_TxByComTxAhb1Info.CfAhbWLMP; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxAhb1Info_CfAhbDiag(data) do \
{ \
    *data = Proxy_TxByComTxAhb1Info.CfAhbDiag; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxAhb1Info_CfAhbAct(data) do \
{ \
    *data = Proxy_TxByComTxAhb1Info.CfAhbAct; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxAhb1Info_CfAhbDef(data) do \
{ \
    *data = Proxy_TxByComTxAhb1Info.CfAhbDef; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxAhb1Info_CfAhbSLMP(data) do \
{ \
    *data = Proxy_TxByComTxAhb1Info.CfAhbSLMP; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxAhb1Info_CfBrkAhbSTDep_PC(data) do \
{ \
    *data = Proxy_TxByComTxAhb1Info.CfBrkAhbSTDep_PC; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxAhb1Info_CfBrkBuzzR(data) do \
{ \
    *data = Proxy_TxByComTxAhb1Info.CfBrkBuzzR; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxAhb1Info_CfBrkPedalCalStatus(data) do \
{ \
    *data = Proxy_TxByComTxAhb1Info.CfBrkPedalCalStatus; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxAhb1Info_CfBrkAhbSnsFail(data) do \
{ \
    *data = Proxy_TxByComTxAhb1Info.CfBrkAhbSnsFail; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxAhb1Info_WhlSpdFLAhb(data) do \
{ \
    *data = Proxy_TxByComTxAhb1Info.WhlSpdFLAhb; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxAhb1Info_WhlSpdFRAhb(data) do \
{ \
    *data = Proxy_TxByComTxAhb1Info.WhlSpdFRAhb; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTxAhb1Info_CrAhbMsgChkSum(data) do \
{ \
    *data = Proxy_TxByComTxAhb1Info.CrAhbMsgChkSum; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComSenPwrMonitorData_SenPwrM_5V_Stable(data) do \
{ \
    *data = Proxy_TxByComSenPwrMonitorData.SenPwrM_5V_Stable; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComSenPwrMonitorData_SenPwrM_12V_Stable(data) do \
{ \
    *data = Proxy_TxByComSenPwrMonitorData.SenPwrM_12V_Stable; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo_QU_TAR_REPAT_XTRQ_FTAX_BAX_ACT(data) do \
{ \
    *data = Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.QU_TAR_REPAT_XTRQ_FTAX_BAX_ACT; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo_TAR_REPAT_XTRQ_FTAX_BAX_ACT(data) do \
{ \
    *data = Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.TAR_REPAT_XTRQ_FTAX_BAX_ACT; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo_TAR_WMOM_PT_SUM_STAB_FAST(data) do \
{ \
    *data = Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.TAR_WMOM_PT_SUM_STAB_FAST; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo_ST_ECU_RQ_WMOM_PT_SUM_STAB(data) do \
{ \
    *data = Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.ST_ECU_RQ_WMOM_PT_SUM_STAB; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo_QU_RQ_ILK_PT(data) do \
{ \
    *data = Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.QU_RQ_ILK_PT; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo_QU_TAR_WMOM_PT_SUM_STAB(data) do \
{ \
    *data = Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.QU_TAR_WMOM_PT_SUM_STAB; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo_TAR_WMOM_PT_SUM_STAB(data) do \
{ \
    *data = Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.TAR_WMOM_PT_SUM_STAB; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo_ST_ECU_AVL_BRTORQ_SUM(data) do \
{ \
    *data = Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.ST_ECU_AVL_BRTORQ_SUM; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo_QU_SER_PRMSN_DBC(data) do \
{ \
    *data = Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_SER_PRMSN_DBC; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo_QU_SER_PRF_BRK(data) do \
{ \
    *data = Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_SER_PRF_BRK; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo_AVL_BRTORQ_SUM_DVCH(data) do \
{ \
    *data = Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.AVL_BRTORQ_SUM_DVCH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo_AVL_BRTORQ_SUM(data) do \
{ \
    *data = Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.AVL_BRTORQ_SUM; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo_QU_AVL_BRTORQ_SUM(data) do \
{ \
    *data = Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_AVL_BRTORQ_SUM; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo_QU_AVL_BRTORQ_SUM_DVCH(data) do \
{ \
    *data = Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_AVL_BRTORQ_SUM_DVCH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_BRTORQ_WHLInfo_QU_AVL_BRTORQ_WHL_FLH(data) do \
{ \
    *data = Proxy_TxByComAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_FLH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_BRTORQ_WHLInfo_QU_AVL_BRTORQ_WHL_RRH(data) do \
{ \
    *data = Proxy_TxByComAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_RRH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_BRTORQ_WHLInfo_QU_AVL_BRTORQ_WHL_FRH(data) do \
{ \
    *data = Proxy_TxByComAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_FRH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_BRTORQ_WHLInfo_AVL_BRTORQ_WHL_RRH(data) do \
{ \
    *data = Proxy_TxByComAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_RRH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_BRTORQ_WHLInfo_AVL_BRTORQ_WHL_RLH(data) do \
{ \
    *data = Proxy_TxByComAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_RLH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_BRTORQ_WHLInfo_AVL_BRTORQ_WHL_FLH(data) do \
{ \
    *data = Proxy_TxByComAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_FLH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_BRTORQ_WHLInfo_QU_AVL_BRTORQ_WHL_RLH(data) do \
{ \
    *data = Proxy_TxByComAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_RLH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_BRTORQ_WHLInfo_AVL_BRTORQ_WHL_FRH(data) do \
{ \
    *data = Proxy_TxByComAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_FRH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_RPM_WHLInfo_AVL_RPM_WHL_RLH(data) do \
{ \
    *data = Proxy_TxByComAVL_RPM_WHLInfo.AVL_RPM_WHL_RLH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_RPM_WHLInfo_AVL_RPM_WHL_RRH(data) do \
{ \
    *data = Proxy_TxByComAVL_RPM_WHLInfo.AVL_RPM_WHL_RRH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_RPM_WHLInfo_AVL_RPM_WHL_FLH(data) do \
{ \
    *data = Proxy_TxByComAVL_RPM_WHLInfo.AVL_RPM_WHL_FLH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_RPM_WHLInfo_QU_AVL_RPM_WHL_FLH(data) do \
{ \
    *data = Proxy_TxByComAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_FLH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_RPM_WHLInfo_QU_AVL_RPM_WHL_FRH(data) do \
{ \
    *data = Proxy_TxByComAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_FRH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_RPM_WHLInfo_QU_AVL_RPM_WHL_RLH(data) do \
{ \
    *data = Proxy_TxByComAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_RLH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_RPM_WHLInfo_QU_AVL_RPM_WHL_RRH(data) do \
{ \
    *data = Proxy_TxByComAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_RRH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_RPM_WHLInfo_AVL_RPM_WHL_FRH(data) do \
{ \
    *data = Proxy_TxByComAVL_RPM_WHLInfo.AVL_RPM_WHL_FRH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComRQ_WMOM_PT_SUM_RECUPInfo_QU_TAR_WMOM_PT_SUM_RECUP(data) do \
{ \
    *data = Proxy_TxByComRQ_WMOM_PT_SUM_RECUPInfo.QU_TAR_WMOM_PT_SUM_RECUP; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComRQ_WMOM_PT_SUM_RECUPInfo_TAR_WMOM_PT_SUM_RECUP(data) do \
{ \
    *data = Proxy_TxByComRQ_WMOM_PT_SUM_RECUPInfo.TAR_WMOM_PT_SUM_RECUP; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComRQ_WMOM_PT_SUM_RECUPInfo_ST_ECU_RQ_WMOM_PT_SUM_RECUP(data) do \
{ \
    *data = Proxy_TxByComRQ_WMOM_PT_SUM_RECUPInfo.ST_ECU_RQ_WMOM_PT_SUM_RECUP; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info_ST_ECU_ST_STAB_DSC(data) do \
{ \
    *data = Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.ST_ECU_ST_STAB_DSC; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info_ST_BRG_MSA(data) do \
{ \
    *data = Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.ST_BRG_MSA; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info_ST_BRG_DV(data) do \
{ \
    *data = Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.ST_BRG_DV; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info_ST_UNRU_DSC(data) do \
{ \
    *data = Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.ST_UNRU_DSC; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info_QU_FN_FDR(data) do \
{ \
    *data = Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.QU_FN_FDR; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info_QU_FN_ABS(data) do \
{ \
    *data = Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.QU_FN_ABS; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info_QU_FN_ASC(data) do \
{ \
    *data = Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.QU_FN_ASC; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info_PRD_TIM_VEHSS(data) do \
{ \
    *data = Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info.PRD_TIM_VEHSS; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo_QU_SER_ECBA(data) do \
{ \
    *data = Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.QU_SER_ECBA; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo_TAR_BRTORQ_SUM_COOTD(data) do \
{ \
    *data = Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.TAR_BRTORQ_SUM_COOTD; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo_AVL_WAY_BRKFA(data) do \
{ \
    *data = Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.AVL_WAY_BRKFA; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo_AVL_FORC_BRKFA(data) do \
{ \
    *data = Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.AVL_FORC_BRKFA; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_QUAN_EES_WHLInfo_AVL_QUAN_EES_WHL_RLH(data) do \
{ \
    *data = Proxy_TxByComAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_RLH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_QUAN_EES_WHLInfo_AVL_QUAN_EES_WHL_RRH(data) do \
{ \
    *data = Proxy_TxByComAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_RRH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_QUAN_EES_WHLInfo_AVL_QUAN_EES_WHL_FLH(data) do \
{ \
    *data = Proxy_TxByComAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_FLH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_QUAN_EES_WHLInfo_AVL_QUAN_EES_WHL_FRH(data) do \
{ \
    *data = Proxy_TxByComAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_FRH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_QUAN_EES_WHLInfo_QU_AVL_QUAN_EES_WHL_FLH(data) do \
{ \
    *data = Proxy_TxByComAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_FLH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_QUAN_EES_WHLInfo_QU_AVL_QUAN_EES_WHL_RRH(data) do \
{ \
    *data = Proxy_TxByComAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_RRH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_QUAN_EES_WHLInfo_QU_AVL_QUAN_EES_WHL_FRH(data) do \
{ \
    *data = Proxy_TxByComAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_FRH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_QUAN_EES_WHLInfo_QU_AVL_QUAN_EES_WHL_RLH(data) do \
{ \
    *data = Proxy_TxByComAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_RLH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTAR_LTRQD_BAXInfo_TAR_LTRQD_BAX(data) do \
{ \
    *data = Proxy_TxByComTAR_LTRQD_BAXInfo.TAR_LTRQD_BAX; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTAR_LTRQD_BAXInfo_LIM_MAX_LTRQD_BAX(data) do \
{ \
    *data = Proxy_TxByComTAR_LTRQD_BAXInfo.LIM_MAX_LTRQD_BAX; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComST_YMRInfo_QU_SER_CLCTR_YMR(data) do \
{ \
    *data = Proxy_TxByComST_YMRInfo.QU_SER_CLCTR_YMR; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComWEGSTRECKEInfo_MILE_FLH(data) do \
{ \
    *data = Proxy_TxByComWEGSTRECKEInfo.MILE_FLH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComWEGSTRECKEInfo_MILE_FRH(data) do \
{ \
    *data = Proxy_TxByComWEGSTRECKEInfo.MILE_FRH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComWEGSTRECKEInfo_MILE_INTM_TOMSRM(data) do \
{ \
    *data = Proxy_TxByComWEGSTRECKEInfo.MILE_INTM_TOMSRM; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComWEGSTRECKEInfo_MILE(data) do \
{ \
    *data = Proxy_TxByComWEGSTRECKEInfo.MILE; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComDISP_CC_DRDY_00Info_ST_IDC_CC_DRDY_00(data) do \
{ \
    *data = Proxy_TxByComDISP_CC_DRDY_00Info.ST_IDC_CC_DRDY_00; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComDISP_CC_DRDY_00Info_ST_CC_DRDY_00(data) do \
{ \
    *data = Proxy_TxByComDISP_CC_DRDY_00Info.ST_CC_DRDY_00; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComDISP_CC_DRDY_00Info_TRANF_CC_DRDY_00(data) do \
{ \
    *data = Proxy_TxByComDISP_CC_DRDY_00Info.TRANF_CC_DRDY_00; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComDISP_CC_DRDY_00Info_NO_CC_DRDY_00(data) do \
{ \
    *data = Proxy_TxByComDISP_CC_DRDY_00Info.NO_CC_DRDY_00; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComDISP_CC_BYPA_00Info_TRANF_CC_BYPA_00(data) do \
{ \
    *data = Proxy_TxByComDISP_CC_BYPA_00Info.TRANF_CC_BYPA_00; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComDISP_CC_BYPA_00Info_ST_IDC_CC_BYPA_00(data) do \
{ \
    *data = Proxy_TxByComDISP_CC_BYPA_00Info.ST_IDC_CC_BYPA_00; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComDISP_CC_BYPA_00Info_ST_CC_BYPA_00(data) do \
{ \
    *data = Proxy_TxByComDISP_CC_BYPA_00Info.ST_CC_BYPA_00; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComDISP_CC_BYPA_00Info_NO_CC_BYPA_00(data) do \
{ \
    *data = Proxy_TxByComDISP_CC_BYPA_00Info.NO_CC_BYPA_00; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComST_VHSSInfo_ST_VEHSS(data) do \
{ \
    *data = Proxy_TxByComST_VHSSInfo.ST_VEHSS; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComDIAG_OBD_HYB_1Info_QU_FN_OBD_1_SEN_1_ERR(data) do \
{ \
    *data = Proxy_TxByComDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_1_ERR; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComDIAG_OBD_HYB_1Info_QU_FN_OBD_1_SEN_3_ERR(data) do \
{ \
    *data = Proxy_TxByComDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_3_ERR; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComDIAG_OBD_HYB_1Info_QU_FN_OBD_1_SEN_4_ERR(data) do \
{ \
    *data = Proxy_TxByComDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_4_ERR; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComDIAG_OBD_HYB_1Info_DIAG_OBD_HYB_1_MUX_MAX(data) do \
{ \
    *data = Proxy_TxByComDIAG_OBD_HYB_1Info.DIAG_OBD_HYB_1_MUX_MAX; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComDIAG_OBD_HYB_1Info_QU_FN_OBD_1_SEN_2_ERR(data) do \
{ \
    *data = Proxy_TxByComDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_2_ERR; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComDIAG_OBD_HYB_1Info_DIAG_OBD_HYB_1_MUX_IMME(data) do \
{ \
    *data = Proxy_TxByComDIAG_OBD_HYB_1Info.DIAG_OBD_HYB_1_MUX_IMME; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComSU_DSCInfo_SU_DSC_WSS(data) do \
{ \
    *data = Proxy_TxByComSU_DSCInfo.SU_DSC_WSS; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComST_TYR_RDCInfo_QU_TPL_RDC(data) do \
{ \
    *data = Proxy_TxByComST_TYR_RDCInfo.QU_TPL_RDC; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComST_TYR_RDCInfo_QU_FN_TYR_INFO_RDC(data) do \
{ \
    *data = Proxy_TxByComST_TYR_RDCInfo.QU_FN_TYR_INFO_RDC; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComST_TYR_RDCInfo_QU_TFAI_RDC(data) do \
{ \
    *data = Proxy_TxByComST_TYR_RDCInfo.QU_TFAI_RDC; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComST_TYR_RPAInfo_QU_TFAI_RPA(data) do \
{ \
    *data = Proxy_TxByComST_TYR_RPAInfo.QU_TFAI_RPA; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComST_TYR_RPAInfo_QU_FN_TYR_INFO_RPA(data) do \
{ \
    *data = Proxy_TxByComST_TYR_RPAInfo.QU_FN_TYR_INFO_RPA; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComST_TYR_RPAInfo_QU_TPL_RPA(data) do \
{ \
    *data = Proxy_TxByComST_TYR_RPAInfo.QU_TPL_RPA; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info_TYR_ID_2(data) do \
{ \
    *data = Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_2; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info_TYR_ID_1(data) do \
{ \
    *data = Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_1; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info_TYR_ID_3(data) do \
{ \
    *data = Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_3; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info_TYR_ID_4(data) do \
{ \
    *data = Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_4; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComST_TYR_2Info_QU_RDC_INIT_DISP(data) do \
{ \
    *data = Proxy_TxByComST_TYR_2Info.QU_RDC_INIT_DISP; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_T_TYRInfo_AVL_TEMP_TYR_RRH(data) do \
{ \
    *data = Proxy_TxByComAVL_T_TYRInfo.AVL_TEMP_TYR_RRH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_T_TYRInfo_AVL_TEMP_TYR_FLH(data) do \
{ \
    *data = Proxy_TxByComAVL_T_TYRInfo.AVL_TEMP_TYR_FLH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_T_TYRInfo_AVL_TEMP_TYR_RLH(data) do \
{ \
    *data = Proxy_TxByComAVL_T_TYRInfo.AVL_TEMP_TYR_RLH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_T_TYRInfo_AVL_TEMP_TYR_FRH(data) do \
{ \
    *data = Proxy_TxByComAVL_T_TYRInfo.AVL_TEMP_TYR_FRH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTEMP_BRKInfo_TEMP_BRDSK_RLH_VRFD(data) do \
{ \
    *data = Proxy_TxByComTEMP_BRKInfo.TEMP_BRDSK_RLH_VRFD; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTEMP_BRKInfo_QU_TEMP_BRDSK_RLH_VRFD(data) do \
{ \
    *data = Proxy_TxByComTEMP_BRKInfo.QU_TEMP_BRDSK_RLH_VRFD; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTEMP_BRKInfo_TEMP_BRDSK_RRH_VRFD(data) do \
{ \
    *data = Proxy_TxByComTEMP_BRKInfo.TEMP_BRDSK_RRH_VRFD; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComTEMP_BRKInfo_QU_TEMP_BRDSK_RRH_VRFD(data) do \
{ \
    *data = Proxy_TxByComTEMP_BRKInfo.QU_TEMP_BRDSK_RRH_VRFD; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_P_TYRInfo_AVL_P_TYR_FRH(data) do \
{ \
    *data = Proxy_TxByComAVL_P_TYRInfo.AVL_P_TYR_FRH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_P_TYRInfo_AVL_P_TYR_FLH(data) do \
{ \
    *data = Proxy_TxByComAVL_P_TYRInfo.AVL_P_TYR_FLH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_P_TYRInfo_AVL_P_TYR_RRH(data) do \
{ \
    *data = Proxy_TxByComAVL_P_TYRInfo.AVL_P_TYR_RRH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComAVL_P_TYRInfo_AVL_P_TYR_RLH(data) do \
{ \
    *data = Proxy_TxByComAVL_P_TYRInfo.AVL_P_TYR_RLH; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComFR_DBG_DSCInfo_DBG_RDCi_13(data) do \
{ \
    *data = Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_13; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComFR_DBG_DSCInfo_DBG_RDCi_04(data) do \
{ \
    *data = Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_04; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComFR_DBG_DSCInfo_DBG_RDCi_05(data) do \
{ \
    *data = Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_05; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComFR_DBG_DSCInfo_DBG_RDCi_06(data) do \
{ \
    *data = Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_06; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComFR_DBG_DSCInfo_DBG_RDCi_01(data) do \
{ \
    *data = Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_01; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComFR_DBG_DSCInfo_DBG_RDCi_02(data) do \
{ \
    *data = Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_02; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComFR_DBG_DSCInfo_DBG_RDCi_03(data) do \
{ \
    *data = Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_03; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComFR_DBG_DSCInfo_DBG_RDCi_07(data) do \
{ \
    *data = Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_07; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComFR_DBG_DSCInfo_DBG_RDCi_11(data) do \
{ \
    *data = Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_11; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComFR_DBG_DSCInfo_DBG_RDCi_10(data) do \
{ \
    *data = Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_10; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComFR_DBG_DSCInfo_DBG_RDCi_09(data) do \
{ \
    *data = Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_09; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComFR_DBG_DSCInfo_DBG_RDCi_15(data) do \
{ \
    *data = Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_15; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComFR_DBG_DSCInfo_DBG_RDCi_14(data) do \
{ \
    *data = Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_14; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComFR_DBG_DSCInfo_DBG_RDCi_12(data) do \
{ \
    *data = Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_12; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComFR_DBG_DSCInfo_DBG_RDCi_00(data) do \
{ \
    *data = Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_00; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComFR_DBG_DSCInfo_BDTM_TA_FR(data) do \
{ \
    *data = Proxy_TxByComFR_DBG_DSCInfo.BDTM_TA_FR; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComFR_DBG_DSCInfo_BDTM_TA_RL(data) do \
{ \
    *data = Proxy_TxByComFR_DBG_DSCInfo.BDTM_TA_RL; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComFR_DBG_DSCInfo_BDTM_TA_RR(data) do \
{ \
    *data = Proxy_TxByComFR_DBG_DSCInfo.BDTM_TA_RR; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComFR_DBG_DSCInfo_DBG_RDCi_08(data) do \
{ \
    *data = Proxy_TxByComFR_DBG_DSCInfo.DBG_RDCi_08; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComFR_DBG_DSCInfo_ST_STATE(data) do \
{ \
    *data = Proxy_TxByComFR_DBG_DSCInfo.ST_STATE; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComFR_DBG_DSCInfo_BDTM_TA_FL(data) do \
{ \
    *data = Proxy_TxByComFR_DBG_DSCInfo.BDTM_TA_FL; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComFR_DBG_DSCInfo_ABSRef(data) do \
{ \
    *data = Proxy_TxByComFR_DBG_DSCInfo.ABSRef; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComFR_DBG_DSCInfo_LOW_VOLTAGE(data) do \
{ \
    *data = Proxy_TxByComFR_DBG_DSCInfo.LOW_VOLTAGE; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComFR_DBG_DSCInfo_DEBUG_PCIB(data) do \
{ \
    *data = Proxy_TxByComFR_DBG_DSCInfo.DEBUG_PCIB; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComFR_DBG_DSCInfo_DBG_DSC(data) do \
{ \
    *data = Proxy_TxByComFR_DBG_DSCInfo.DBG_DSC; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComFR_DBG_DSCInfo_LATACC(data) do \
{ \
    *data = Proxy_TxByComFR_DBG_DSCInfo.LATACC; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComFR_DBG_DSCInfo_TRIGGER_FS(data) do \
{ \
    *data = Proxy_TxByComFR_DBG_DSCInfo.TRIGGER_FS; \
}while(0);

#define Proxy_TxByCom_Read_Proxy_TxByComFR_DBG_DSCInfo_TRIGGER_IS(data) do \
{ \
    *data = Proxy_TxByComFR_DBG_DSCInfo.TRIGGER_IS; \
}while(0);


/* Set Output DE MAcro Function */
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PROXY_TXBYCOM_IFA_H_ */
/** @} */
