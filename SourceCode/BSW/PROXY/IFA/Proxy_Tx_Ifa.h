/**
 * @defgroup Proxy_Tx_Ifa Proxy_Tx_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Proxy_Tx_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PROXY_TX_IFA_H_
#define PROXY_TX_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Proxy_Tx_Read_Proxy_TxEemCtrlInhibitData(data) do \
{ \
    *data = Proxy_TxEemCtrlInhibitData; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxCanTxInfo(data) do \
{ \
    *data = Proxy_TxCanTxInfo; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxPdt5msRawInfo(data) do \
{ \
    *data = Proxy_TxPdt5msRawInfo; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxPdf5msRawInfo(data) do \
{ \
    *data = Proxy_TxPdf5msRawInfo; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxTarRgnBrkTqInfo(data) do \
{ \
    *data = Proxy_TxTarRgnBrkTqInfo; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxEscSwtStInfo(data) do \
{ \
    *data = Proxy_TxEscSwtStInfo; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxWhlSpdInfo(data) do \
{ \
    *data = Proxy_TxWhlSpdInfo; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxWhlEdgeCntInfo(data) do \
{ \
    *data = Proxy_TxWhlEdgeCntInfo; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxPistP5msRawInfo(data) do \
{ \
    *data = Proxy_TxPistP5msRawInfo; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxLogicEepDataInfo(data) do \
{ \
    *data = Proxy_TxLogicEepDataInfo; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxEemLampData(data) do \
{ \
    *data = Proxy_TxEemLampData; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxEemCtrlInhibitData_Eem_BBS_DegradeModeFail(data) do \
{ \
    *data = Proxy_TxEemCtrlInhibitData.Eem_BBS_DegradeModeFail; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxEemCtrlInhibitData_Eem_BBS_DefectiveModeFail(data) do \
{ \
    *data = Proxy_TxEemCtrlInhibitData.Eem_BBS_DefectiveModeFail; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxCanTxInfo_TqIntvTCS(data) do \
{ \
    *data = Proxy_TxCanTxInfo.TqIntvTCS; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxCanTxInfo_TqIntvMsr(data) do \
{ \
    *data = Proxy_TxCanTxInfo.TqIntvMsr; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxCanTxInfo_TqIntvSlowTCS(data) do \
{ \
    *data = Proxy_TxCanTxInfo.TqIntvSlowTCS; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxCanTxInfo_MinGear(data) do \
{ \
    *data = Proxy_TxCanTxInfo.MinGear; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxCanTxInfo_MaxGear(data) do \
{ \
    *data = Proxy_TxCanTxInfo.MaxGear; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxCanTxInfo_TcsReq(data) do \
{ \
    *data = Proxy_TxCanTxInfo.TcsReq; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxCanTxInfo_TcsCtrl(data) do \
{ \
    *data = Proxy_TxCanTxInfo.TcsCtrl; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxCanTxInfo_AbsAct(data) do \
{ \
    *data = Proxy_TxCanTxInfo.AbsAct; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxCanTxInfo_TcsGearShiftChr(data) do \
{ \
    *data = Proxy_TxCanTxInfo.TcsGearShiftChr; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxCanTxInfo_EspCtrl(data) do \
{ \
    *data = Proxy_TxCanTxInfo.EspCtrl; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxCanTxInfo_MsrReq(data) do \
{ \
    *data = Proxy_TxCanTxInfo.MsrReq; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxCanTxInfo_TcsProductInfo(data) do \
{ \
    *data = Proxy_TxCanTxInfo.TcsProductInfo; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxPdt5msRawInfo_PdtSig(data) do \
{ \
    *data = Proxy_TxPdt5msRawInfo.PdtSig; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxPdf5msRawInfo_PdfSig(data) do \
{ \
    *data = Proxy_TxPdf5msRawInfo.PdfSig; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxTarRgnBrkTqInfo_TarRgnBrkTq(data) do \
{ \
    *data = Proxy_TxTarRgnBrkTqInfo.TarRgnBrkTq; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxTarRgnBrkTqInfo_EstTotBrkForce(data) do \
{ \
    *data = Proxy_TxTarRgnBrkTqInfo.EstTotBrkForce; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxTarRgnBrkTqInfo_EstHydBrkForce(data) do \
{ \
    *data = Proxy_TxTarRgnBrkTqInfo.EstHydBrkForce; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxTarRgnBrkTqInfo_EhbStat(data) do \
{ \
    *data = Proxy_TxTarRgnBrkTqInfo.EhbStat; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxEscSwtStInfo_EscDisabledBySwt(data) do \
{ \
    *data = Proxy_TxEscSwtStInfo.EscDisabledBySwt; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxEscSwtStInfo_TcsDisabledBySwt(data) do \
{ \
    *data = Proxy_TxEscSwtStInfo.TcsDisabledBySwt; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxWhlSpdInfo_FlWhlSpd(data) do \
{ \
    *data = Proxy_TxWhlSpdInfo.FlWhlSpd; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxWhlSpdInfo_FrWhlSpd(data) do \
{ \
    *data = Proxy_TxWhlSpdInfo.FrWhlSpd; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxWhlSpdInfo_RlWhlSpd(data) do \
{ \
    *data = Proxy_TxWhlSpdInfo.RlWhlSpd; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxWhlSpdInfo_RrlWhlSpd(data) do \
{ \
    *data = Proxy_TxWhlSpdInfo.RrlWhlSpd; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxWhlEdgeCntInfo_FlWhlEdgeCnt(data) do \
{ \
    *data = Proxy_TxWhlEdgeCntInfo.FlWhlEdgeCnt; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxWhlEdgeCntInfo_FrWhlEdgeCnt(data) do \
{ \
    *data = Proxy_TxWhlEdgeCntInfo.FrWhlEdgeCnt; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxWhlEdgeCntInfo_RlWhlEdgeCnt(data) do \
{ \
    *data = Proxy_TxWhlEdgeCntInfo.RlWhlEdgeCnt; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxWhlEdgeCntInfo_RrWhlEdgeCnt(data) do \
{ \
    *data = Proxy_TxWhlEdgeCntInfo.RrWhlEdgeCnt; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxPistP5msRawInfo_PistPSig(data) do \
{ \
    *data = Proxy_TxPistP5msRawInfo.PistPSig; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxLogicEepDataInfo_KPdtOffsEolReadVal(data) do \
{ \
    *data = Proxy_TxLogicEepDataInfo.KPdtOffsEolReadVal; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxLogicEepDataInfo_ReadInvld(data) do \
{ \
    *data = Proxy_TxLogicEepDataInfo.ReadInvld; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxEemLampData_Eem_Lamp_EBDLampRequest(data) do \
{ \
    *data = Proxy_TxEemLampData.Eem_Lamp_EBDLampRequest; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxEemLampData_Eem_Lamp_ABSLampRequest(data) do \
{ \
    *data = Proxy_TxEemLampData.Eem_Lamp_ABSLampRequest; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxEemLampData_Eem_Lamp_TCSLampRequest(data) do \
{ \
    *data = Proxy_TxEemLampData.Eem_Lamp_TCSLampRequest; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxEemLampData_Eem_Lamp_TCSOffLampRequest(data) do \
{ \
    *data = Proxy_TxEemLampData.Eem_Lamp_TCSOffLampRequest; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxEemLampData_Eem_Lamp_VDCLampRequest(data) do \
{ \
    *data = Proxy_TxEemLampData.Eem_Lamp_VDCLampRequest; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxEemLampData_Eem_Lamp_VDCOffLampRequest(data) do \
{ \
    *data = Proxy_TxEemLampData.Eem_Lamp_VDCOffLampRequest; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxEemLampData_Eem_Lamp_HDCLampRequest(data) do \
{ \
    *data = Proxy_TxEemLampData.Eem_Lamp_HDCLampRequest; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxEemLampData_Eem_Lamp_BBSBuzzorRequest(data) do \
{ \
    *data = Proxy_TxEemLampData.Eem_Lamp_BBSBuzzorRequest; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxEemLampData_Eem_Lamp_RBCSLampRequest(data) do \
{ \
    *data = Proxy_TxEemLampData.Eem_Lamp_RBCSLampRequest; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxEemLampData_Eem_Lamp_AHBLampRequest(data) do \
{ \
    *data = Proxy_TxEemLampData.Eem_Lamp_AHBLampRequest; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxEemLampData_Eem_Lamp_ServiceLampRequest(data) do \
{ \
    *data = Proxy_TxEemLampData.Eem_Lamp_ServiceLampRequest; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxEemLampData_Eem_Lamp_TCSFuncLampRequest(data) do \
{ \
    *data = Proxy_TxEemLampData.Eem_Lamp_TCSFuncLampRequest; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxEemLampData_Eem_Lamp_VDCFuncLampRequest(data) do \
{ \
    *data = Proxy_TxEemLampData.Eem_Lamp_VDCFuncLampRequest; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxEemLampData_Eem_Lamp_AVHLampRequest(data) do \
{ \
    *data = Proxy_TxEemLampData.Eem_Lamp_AVHLampRequest; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxEemLampData_Eem_Lamp_AVHILampRequest(data) do \
{ \
    *data = Proxy_TxEemLampData.Eem_Lamp_AVHILampRequest; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxEemLampData_Eem_Lamp_ACCEnable(data) do \
{ \
    *data = Proxy_TxEemLampData.Eem_Lamp_ACCEnable; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxEemLampData_Eem_Lamp_CDMEnable(data) do \
{ \
    *data = Proxy_TxEemLampData.Eem_Lamp_CDMEnable; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxEemLampData_Eem_Buzzor_On(data) do \
{ \
    *data = Proxy_TxEemLampData.Eem_Buzzor_On; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxEcuModeSts(data) do \
{ \
    *data = Proxy_TxEcuModeSts; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxIgnOnOffSts(data) do \
{ \
    *data = Proxy_TxIgnOnOffSts; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxDiagSci(data) do \
{ \
    *data = Proxy_TxDiagSci; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxDiagAhbSci(data) do \
{ \
    *data = Proxy_TxDiagAhbSci; \
}while(0);

#define Proxy_Tx_Read_Proxy_TxFuncInhibitProxySts(data) do \
{ \
    *data = Proxy_TxFuncInhibitProxySts; \
}while(0);


/* Set Output DE MAcro Function */
#define Proxy_Tx_Write_Proxy_TxTxESCSensorInfo(data) do \
{ \
    Proxy_TxTxESCSensorInfo = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxYawCbitInfo(data) do \
{ \
    Proxy_TxTxYawCbitInfo = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxWhlSpdInfo(data) do \
{ \
    Proxy_TxTxWhlSpdInfo = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxWhlPulInfo(data) do \
{ \
    Proxy_TxTxWhlPulInfo = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxTcs5Info(data) do \
{ \
    Proxy_TxTxTcs5Info = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxTcs1Info(data) do \
{ \
    Proxy_TxTxTcs1Info = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxSasCalInfo(data) do \
{ \
    Proxy_TxTxSasCalInfo = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxEsp2Info(data) do \
{ \
    Proxy_TxTxEsp2Info = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxEbs1Info(data) do \
{ \
    Proxy_TxTxEbs1Info = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxAhb1Info(data) do \
{ \
    Proxy_TxTxAhb1Info = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo(data) do \
{ \
    Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo(data) do \
{ \
    Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_BRTORQ_WHLInfo(data) do \
{ \
    Proxy_TxAVL_BRTORQ_WHLInfo = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_RPM_WHLInfo(data) do \
{ \
    Proxy_TxAVL_RPM_WHLInfo = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo(data) do \
{ \
    Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info(data) do \
{ \
    Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo(data) do \
{ \
    Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_QUAN_EES_WHLInfo(data) do \
{ \
    Proxy_TxAVL_QUAN_EES_WHLInfo = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTAR_LTRQD_BAXInfo(data) do \
{ \
    Proxy_TxTAR_LTRQD_BAXInfo = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxST_YMRInfo(data) do \
{ \
    Proxy_TxST_YMRInfo = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxWEGSTRECKEInfo(data) do \
{ \
    Proxy_TxWEGSTRECKEInfo = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxDISP_CC_DRDY_00Info(data) do \
{ \
    Proxy_TxDISP_CC_DRDY_00Info = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxDISP_CC_BYPA_00Info(data) do \
{ \
    Proxy_TxDISP_CC_BYPA_00Info = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxST_VHSSInfo(data) do \
{ \
    Proxy_TxST_VHSSInfo = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxDIAG_OBD_HYB_1Info(data) do \
{ \
    Proxy_TxDIAG_OBD_HYB_1Info = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxSU_DSCInfo(data) do \
{ \
    Proxy_TxSU_DSCInfo = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxST_TYR_RDCInfo(data) do \
{ \
    Proxy_TxST_TYR_RDCInfo = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxST_TYR_RPAInfo(data) do \
{ \
    Proxy_TxST_TYR_RPAInfo = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info(data) do \
{ \
    Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxST_TYR_2Info(data) do \
{ \
    Proxy_TxST_TYR_2Info = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_T_TYRInfo(data) do \
{ \
    Proxy_TxAVL_T_TYRInfo = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTEMP_BRKInfo(data) do \
{ \
    Proxy_TxTEMP_BRKInfo = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_P_TYRInfo(data) do \
{ \
    Proxy_TxAVL_P_TYRInfo = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxFR_DBG_DSCInfo(data) do \
{ \
    Proxy_TxFR_DBG_DSCInfo = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxESCSensorInfo_EstimatedYaw(data) do \
{ \
    Proxy_TxTxESCSensorInfo.EstimatedYaw = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxESCSensorInfo_EstimatedAY(data) do \
{ \
    Proxy_TxTxESCSensorInfo.EstimatedAY = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxESCSensorInfo_A_long_1_1000g(data) do \
{ \
    Proxy_TxTxESCSensorInfo.A_long_1_1000g = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxYawCbitInfo_TestYawRateSensor(data) do \
{ \
    Proxy_TxTxYawCbitInfo.TestYawRateSensor = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxYawCbitInfo_TestAcclSensor(data) do \
{ \
    Proxy_TxTxYawCbitInfo.TestAcclSensor = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxYawCbitInfo_YawSerialNumberReq(data) do \
{ \
    Proxy_TxTxYawCbitInfo.YawSerialNumberReq = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxWhlSpdInfo_WhlSpdFL(data) do \
{ \
    Proxy_TxTxWhlSpdInfo.WhlSpdFL = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxWhlSpdInfo_WhlSpdFR(data) do \
{ \
    Proxy_TxTxWhlSpdInfo.WhlSpdFR = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxWhlSpdInfo_WhlSpdRL(data) do \
{ \
    Proxy_TxTxWhlSpdInfo.WhlSpdRL = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxWhlSpdInfo_WhlSpdRR(data) do \
{ \
    Proxy_TxTxWhlSpdInfo.WhlSpdRR = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxWhlPulInfo_WhlPulFL(data) do \
{ \
    Proxy_TxTxWhlPulInfo.WhlPulFL = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxWhlPulInfo_WhlPulFR(data) do \
{ \
    Proxy_TxTxWhlPulInfo.WhlPulFR = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxWhlPulInfo_WhlPulRL(data) do \
{ \
    Proxy_TxTxWhlPulInfo.WhlPulRL = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxWhlPulInfo_WhlPulRR(data) do \
{ \
    Proxy_TxTxWhlPulInfo.WhlPulRR = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxTcs5Info_CfBrkAbsWLMP(data) do \
{ \
    Proxy_TxTxTcs5Info.CfBrkAbsWLMP = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxTcs5Info_CfBrkEbdWLMP(data) do \
{ \
    Proxy_TxTxTcs5Info.CfBrkEbdWLMP = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxTcs5Info_CfBrkTcsWLMP(data) do \
{ \
    Proxy_TxTxTcs5Info.CfBrkTcsWLMP = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxTcs5Info_CfBrkTcsFLMP(data) do \
{ \
    Proxy_TxTxTcs5Info.CfBrkTcsFLMP = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxTcs5Info_CfBrkAbsDIAG(data) do \
{ \
    Proxy_TxTxTcs5Info.CfBrkAbsDIAG = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxTcs5Info_CrBrkWheelFL_KMH(data) do \
{ \
    Proxy_TxTxTcs5Info.CrBrkWheelFL_KMH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxTcs5Info_CrBrkWheelFR_KMH(data) do \
{ \
    Proxy_TxTxTcs5Info.CrBrkWheelFR_KMH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxTcs5Info_CrBrkWheelRL_KMH(data) do \
{ \
    Proxy_TxTxTcs5Info.CrBrkWheelRL_KMH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxTcs5Info_CrBrkWheelRR_KMH(data) do \
{ \
    Proxy_TxTxTcs5Info.CrBrkWheelRR_KMH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxTcs1Info_CfBrkTcsREQ(data) do \
{ \
    Proxy_TxTxTcs1Info.CfBrkTcsREQ = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxTcs1Info_CfBrkTcsPAS(data) do \
{ \
    Proxy_TxTxTcs1Info.CfBrkTcsPAS = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxTcs1Info_CfBrkTcsDEF(data) do \
{ \
    Proxy_TxTxTcs1Info.CfBrkTcsDEF = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxTcs1Info_CfBrkTcsCTL(data) do \
{ \
    Proxy_TxTxTcs1Info.CfBrkTcsCTL = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxTcs1Info_CfBrkAbsACT(data) do \
{ \
    Proxy_TxTxTcs1Info.CfBrkAbsACT = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxTcs1Info_CfBrkAbsDEF(data) do \
{ \
    Proxy_TxTxTcs1Info.CfBrkAbsDEF = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxTcs1Info_CfBrkEbdDEF(data) do \
{ \
    Proxy_TxTxTcs1Info.CfBrkEbdDEF = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxTcs1Info_CfBrkTcsGSC(data) do \
{ \
    Proxy_TxTxTcs1Info.CfBrkTcsGSC = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxTcs1Info_CfBrkEspPAS(data) do \
{ \
    Proxy_TxTxTcs1Info.CfBrkEspPAS = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxTcs1Info_CfBrkEspDEF(data) do \
{ \
    Proxy_TxTxTcs1Info.CfBrkEspDEF = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxTcs1Info_CfBrkEspCTL(data) do \
{ \
    Proxy_TxTxTcs1Info.CfBrkEspCTL = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxTcs1Info_CfBrkMsrREQ(data) do \
{ \
    Proxy_TxTxTcs1Info.CfBrkMsrREQ = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxTcs1Info_CfBrkTcsMFRN(data) do \
{ \
    Proxy_TxTxTcs1Info.CfBrkTcsMFRN = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxTcs1Info_CfBrkMinGEAR(data) do \
{ \
    Proxy_TxTxTcs1Info.CfBrkMinGEAR = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxTcs1Info_CfBrkMaxGEAR(data) do \
{ \
    Proxy_TxTxTcs1Info.CfBrkMaxGEAR = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxTcs1Info_BrakeLight(data) do \
{ \
    Proxy_TxTxTcs1Info.BrakeLight = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxTcs1Info_HacCtl(data) do \
{ \
    Proxy_TxTxTcs1Info.HacCtl = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxTcs1Info_HacPas(data) do \
{ \
    Proxy_TxTxTcs1Info.HacPas = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxTcs1Info_HacDef(data) do \
{ \
    Proxy_TxTxTcs1Info.HacDef = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxTcs1Info_CrBrkTQI_PC(data) do \
{ \
    Proxy_TxTxTcs1Info.CrBrkTQI_PC = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxTcs1Info_CrBrkTQIMSR_PC(data) do \
{ \
    Proxy_TxTxTcs1Info.CrBrkTQIMSR_PC = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxTcs1Info_CrBrkTQISLW_PC(data) do \
{ \
    Proxy_TxTxTcs1Info.CrBrkTQISLW_PC = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxTcs1Info_CrEbsMSGCHKSUM(data) do \
{ \
    Proxy_TxTxTcs1Info.CrEbsMSGCHKSUM = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxSasCalInfo_CalSas_CCW(data) do \
{ \
    Proxy_TxTxSasCalInfo.CalSas_CCW = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxSasCalInfo_CalSas_Lws_CID(data) do \
{ \
    Proxy_TxTxSasCalInfo.CalSas_Lws_CID = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxEsp2Info_LatAccel(data) do \
{ \
    Proxy_TxTxEsp2Info.LatAccel = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxEsp2Info_LatAccelStat(data) do \
{ \
    Proxy_TxTxEsp2Info.LatAccelStat = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxEsp2Info_LatAccelDiag(data) do \
{ \
    Proxy_TxTxEsp2Info.LatAccelDiag = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxEsp2Info_LongAccel(data) do \
{ \
    Proxy_TxTxEsp2Info.LongAccel = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxEsp2Info_LongAccelStat(data) do \
{ \
    Proxy_TxTxEsp2Info.LongAccelStat = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxEsp2Info_LongAccelDiag(data) do \
{ \
    Proxy_TxTxEsp2Info.LongAccelDiag = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxEsp2Info_YawRate(data) do \
{ \
    Proxy_TxTxEsp2Info.YawRate = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxEsp2Info_YawRateStat(data) do \
{ \
    Proxy_TxTxEsp2Info.YawRateStat = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxEsp2Info_YawRateDiag(data) do \
{ \
    Proxy_TxTxEsp2Info.YawRateDiag = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxEsp2Info_CylPres(data) do \
{ \
    Proxy_TxTxEsp2Info.CylPres = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxEsp2Info_CylPresStat(data) do \
{ \
    Proxy_TxTxEsp2Info.CylPresStat = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxEsp2Info_CylPresDiag(data) do \
{ \
    Proxy_TxTxEsp2Info.CylPresDiag = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxEbs1Info_CfBrkEbsStat(data) do \
{ \
    Proxy_TxTxEbs1Info.CfBrkEbsStat = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxEbs1Info_CrBrkRegenTQLimit_NM(data) do \
{ \
    Proxy_TxTxEbs1Info.CrBrkRegenTQLimit_NM = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxEbs1Info_CrBrkEstTot_NM(data) do \
{ \
    Proxy_TxTxEbs1Info.CrBrkEstTot_NM = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxEbs1Info_CfBrkSnsFail(data) do \
{ \
    Proxy_TxTxEbs1Info.CfBrkSnsFail = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxEbs1Info_CfBrkRbsWLamp(data) do \
{ \
    Proxy_TxTxEbs1Info.CfBrkRbsWLamp = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxEbs1Info_CfBrkVacuumSysDef(data) do \
{ \
    Proxy_TxTxEbs1Info.CfBrkVacuumSysDef = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxEbs1Info_CrBrkEstHyd_NM(data) do \
{ \
    Proxy_TxTxEbs1Info.CrBrkEstHyd_NM = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxEbs1Info_CrBrkStkDep_PC(data) do \
{ \
    Proxy_TxTxEbs1Info.CrBrkStkDep_PC = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxEbs1Info_CfBrkPgmRun1(data) do \
{ \
    Proxy_TxTxEbs1Info.CfBrkPgmRun1 = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxAhb1Info_CfAhbWLMP(data) do \
{ \
    Proxy_TxTxAhb1Info.CfAhbWLMP = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxAhb1Info_CfAhbDiag(data) do \
{ \
    Proxy_TxTxAhb1Info.CfAhbDiag = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxAhb1Info_CfAhbAct(data) do \
{ \
    Proxy_TxTxAhb1Info.CfAhbAct = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxAhb1Info_CfAhbDef(data) do \
{ \
    Proxy_TxTxAhb1Info.CfAhbDef = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxAhb1Info_CfAhbSLMP(data) do \
{ \
    Proxy_TxTxAhb1Info.CfAhbSLMP = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxAhb1Info_CfBrkAhbSTDep_PC(data) do \
{ \
    Proxy_TxTxAhb1Info.CfBrkAhbSTDep_PC = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxAhb1Info_CfBrkBuzzR(data) do \
{ \
    Proxy_TxTxAhb1Info.CfBrkBuzzR = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxAhb1Info_CfBrkPedalCalStatus(data) do \
{ \
    Proxy_TxTxAhb1Info.CfBrkPedalCalStatus = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxAhb1Info_CfBrkAhbSnsFail(data) do \
{ \
    Proxy_TxTxAhb1Info.CfBrkAhbSnsFail = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxAhb1Info_WhlSpdFLAhb(data) do \
{ \
    Proxy_TxTxAhb1Info.WhlSpdFLAhb = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxAhb1Info_WhlSpdFRAhb(data) do \
{ \
    Proxy_TxTxAhb1Info.WhlSpdFRAhb = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTxAhb1Info_CrAhbMsgChkSum(data) do \
{ \
    Proxy_TxTxAhb1Info.CrAhbMsgChkSum = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo_QU_TAR_REPAT_XTRQ_FTAX_BAX_ACT(data) do \
{ \
    Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.QU_TAR_REPAT_XTRQ_FTAX_BAX_ACT = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo_TAR_REPAT_XTRQ_FTAX_BAX_ACT(data) do \
{ \
    Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.TAR_REPAT_XTRQ_FTAX_BAX_ACT = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo_TAR_WMOM_PT_SUM_STAB_FAST(data) do \
{ \
    Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.TAR_WMOM_PT_SUM_STAB_FAST = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo_ST_ECU_RQ_WMOM_PT_SUM_STAB(data) do \
{ \
    Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.ST_ECU_RQ_WMOM_PT_SUM_STAB = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo_QU_RQ_ILK_PT(data) do \
{ \
    Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.QU_RQ_ILK_PT = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo_QU_TAR_WMOM_PT_SUM_STAB(data) do \
{ \
    Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.QU_TAR_WMOM_PT_SUM_STAB = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo_TAR_WMOM_PT_SUM_STAB(data) do \
{ \
    Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo.TAR_WMOM_PT_SUM_STAB = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo_ST_ECU_AVL_BRTORQ_SUM(data) do \
{ \
    Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.ST_ECU_AVL_BRTORQ_SUM = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo_QU_SER_PRMSN_DBC(data) do \
{ \
    Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_SER_PRMSN_DBC = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo_QU_SER_PRF_BRK(data) do \
{ \
    Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_SER_PRF_BRK = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo_AVL_BRTORQ_SUM_DVCH(data) do \
{ \
    Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.AVL_BRTORQ_SUM_DVCH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo_AVL_BRTORQ_SUM(data) do \
{ \
    Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.AVL_BRTORQ_SUM = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo_QU_AVL_BRTORQ_SUM(data) do \
{ \
    Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_AVL_BRTORQ_SUM = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo_QU_AVL_BRTORQ_SUM_DVCH(data) do \
{ \
    Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo.QU_AVL_BRTORQ_SUM_DVCH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_BRTORQ_WHLInfo_QU_AVL_BRTORQ_WHL_FLH(data) do \
{ \
    Proxy_TxAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_FLH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_BRTORQ_WHLInfo_QU_AVL_BRTORQ_WHL_RRH(data) do \
{ \
    Proxy_TxAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_RRH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_BRTORQ_WHLInfo_QU_AVL_BRTORQ_WHL_FRH(data) do \
{ \
    Proxy_TxAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_FRH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_BRTORQ_WHLInfo_AVL_BRTORQ_WHL_RRH(data) do \
{ \
    Proxy_TxAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_RRH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_BRTORQ_WHLInfo_AVL_BRTORQ_WHL_RLH(data) do \
{ \
    Proxy_TxAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_RLH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_BRTORQ_WHLInfo_AVL_BRTORQ_WHL_FLH(data) do \
{ \
    Proxy_TxAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_FLH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_BRTORQ_WHLInfo_QU_AVL_BRTORQ_WHL_RLH(data) do \
{ \
    Proxy_TxAVL_BRTORQ_WHLInfo.QU_AVL_BRTORQ_WHL_RLH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_BRTORQ_WHLInfo_AVL_BRTORQ_WHL_FRH(data) do \
{ \
    Proxy_TxAVL_BRTORQ_WHLInfo.AVL_BRTORQ_WHL_FRH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_RPM_WHLInfo_AVL_RPM_WHL_RLH(data) do \
{ \
    Proxy_TxAVL_RPM_WHLInfo.AVL_RPM_WHL_RLH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_RPM_WHLInfo_AVL_RPM_WHL_RRH(data) do \
{ \
    Proxy_TxAVL_RPM_WHLInfo.AVL_RPM_WHL_RRH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_RPM_WHLInfo_AVL_RPM_WHL_FLH(data) do \
{ \
    Proxy_TxAVL_RPM_WHLInfo.AVL_RPM_WHL_FLH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_RPM_WHLInfo_QU_AVL_RPM_WHL_FLH(data) do \
{ \
    Proxy_TxAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_FLH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_RPM_WHLInfo_QU_AVL_RPM_WHL_FRH(data) do \
{ \
    Proxy_TxAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_FRH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_RPM_WHLInfo_QU_AVL_RPM_WHL_RLH(data) do \
{ \
    Proxy_TxAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_RLH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_RPM_WHLInfo_QU_AVL_RPM_WHL_RRH(data) do \
{ \
    Proxy_TxAVL_RPM_WHLInfo.QU_AVL_RPM_WHL_RRH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_RPM_WHLInfo_AVL_RPM_WHL_FRH(data) do \
{ \
    Proxy_TxAVL_RPM_WHLInfo.AVL_RPM_WHL_FRH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo_QU_TAR_WMOM_PT_SUM_RECUP(data) do \
{ \
    Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo.QU_TAR_WMOM_PT_SUM_RECUP = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo_TAR_WMOM_PT_SUM_RECUP(data) do \
{ \
    Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo.TAR_WMOM_PT_SUM_RECUP = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo_ST_ECU_RQ_WMOM_PT_SUM_RECUP(data) do \
{ \
    Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo.ST_ECU_RQ_WMOM_PT_SUM_RECUP = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info_ST_ECU_ST_STAB_DSC(data) do \
{ \
    Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info.ST_ECU_ST_STAB_DSC = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info_ST_BRG_MSA(data) do \
{ \
    Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info.ST_BRG_MSA = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info_ST_BRG_DV(data) do \
{ \
    Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info.ST_BRG_DV = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info_ST_UNRU_DSC(data) do \
{ \
    Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info.ST_UNRU_DSC = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info_QU_FN_FDR(data) do \
{ \
    Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info.QU_FN_FDR = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info_QU_FN_ABS(data) do \
{ \
    Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info.QU_FN_ABS = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info_QU_FN_ASC(data) do \
{ \
    Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info.QU_FN_ASC = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info_PRD_TIM_VEHSS(data) do \
{ \
    Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info.PRD_TIM_VEHSS = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo_QU_SER_ECBA(data) do \
{ \
    Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.QU_SER_ECBA = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo_TAR_BRTORQ_SUM_COOTD(data) do \
{ \
    Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.TAR_BRTORQ_SUM_COOTD = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo_AVL_WAY_BRKFA(data) do \
{ \
    Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.AVL_WAY_BRKFA = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo_AVL_FORC_BRKFA(data) do \
{ \
    Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo.AVL_FORC_BRKFA = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_QUAN_EES_WHLInfo_AVL_QUAN_EES_WHL_RLH(data) do \
{ \
    Proxy_TxAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_RLH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_QUAN_EES_WHLInfo_AVL_QUAN_EES_WHL_RRH(data) do \
{ \
    Proxy_TxAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_RRH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_QUAN_EES_WHLInfo_AVL_QUAN_EES_WHL_FLH(data) do \
{ \
    Proxy_TxAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_FLH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_QUAN_EES_WHLInfo_AVL_QUAN_EES_WHL_FRH(data) do \
{ \
    Proxy_TxAVL_QUAN_EES_WHLInfo.AVL_QUAN_EES_WHL_FRH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_QUAN_EES_WHLInfo_QU_AVL_QUAN_EES_WHL_FLH(data) do \
{ \
    Proxy_TxAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_FLH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_QUAN_EES_WHLInfo_QU_AVL_QUAN_EES_WHL_RRH(data) do \
{ \
    Proxy_TxAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_RRH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_QUAN_EES_WHLInfo_QU_AVL_QUAN_EES_WHL_FRH(data) do \
{ \
    Proxy_TxAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_FRH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_QUAN_EES_WHLInfo_QU_AVL_QUAN_EES_WHL_RLH(data) do \
{ \
    Proxy_TxAVL_QUAN_EES_WHLInfo.QU_AVL_QUAN_EES_WHL_RLH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTAR_LTRQD_BAXInfo_TAR_LTRQD_BAX(data) do \
{ \
    Proxy_TxTAR_LTRQD_BAXInfo.TAR_LTRQD_BAX = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTAR_LTRQD_BAXInfo_LIM_MAX_LTRQD_BAX(data) do \
{ \
    Proxy_TxTAR_LTRQD_BAXInfo.LIM_MAX_LTRQD_BAX = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxST_YMRInfo_QU_SER_CLCTR_YMR(data) do \
{ \
    Proxy_TxST_YMRInfo.QU_SER_CLCTR_YMR = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxWEGSTRECKEInfo_MILE_FLH(data) do \
{ \
    Proxy_TxWEGSTRECKEInfo.MILE_FLH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxWEGSTRECKEInfo_MILE_FRH(data) do \
{ \
    Proxy_TxWEGSTRECKEInfo.MILE_FRH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxWEGSTRECKEInfo_MILE_INTM_TOMSRM(data) do \
{ \
    Proxy_TxWEGSTRECKEInfo.MILE_INTM_TOMSRM = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxWEGSTRECKEInfo_MILE(data) do \
{ \
    Proxy_TxWEGSTRECKEInfo.MILE = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxDISP_CC_DRDY_00Info_ST_IDC_CC_DRDY_00(data) do \
{ \
    Proxy_TxDISP_CC_DRDY_00Info.ST_IDC_CC_DRDY_00 = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxDISP_CC_DRDY_00Info_ST_CC_DRDY_00(data) do \
{ \
    Proxy_TxDISP_CC_DRDY_00Info.ST_CC_DRDY_00 = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxDISP_CC_DRDY_00Info_TRANF_CC_DRDY_00(data) do \
{ \
    Proxy_TxDISP_CC_DRDY_00Info.TRANF_CC_DRDY_00 = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxDISP_CC_DRDY_00Info_NO_CC_DRDY_00(data) do \
{ \
    Proxy_TxDISP_CC_DRDY_00Info.NO_CC_DRDY_00 = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxDISP_CC_BYPA_00Info_TRANF_CC_BYPA_00(data) do \
{ \
    Proxy_TxDISP_CC_BYPA_00Info.TRANF_CC_BYPA_00 = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxDISP_CC_BYPA_00Info_ST_IDC_CC_BYPA_00(data) do \
{ \
    Proxy_TxDISP_CC_BYPA_00Info.ST_IDC_CC_BYPA_00 = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxDISP_CC_BYPA_00Info_ST_CC_BYPA_00(data) do \
{ \
    Proxy_TxDISP_CC_BYPA_00Info.ST_CC_BYPA_00 = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxDISP_CC_BYPA_00Info_NO_CC_BYPA_00(data) do \
{ \
    Proxy_TxDISP_CC_BYPA_00Info.NO_CC_BYPA_00 = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxST_VHSSInfo_ST_VEHSS(data) do \
{ \
    Proxy_TxST_VHSSInfo.ST_VEHSS = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxDIAG_OBD_HYB_1Info_QU_FN_OBD_1_SEN_1_ERR(data) do \
{ \
    Proxy_TxDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_1_ERR = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxDIAG_OBD_HYB_1Info_QU_FN_OBD_1_SEN_3_ERR(data) do \
{ \
    Proxy_TxDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_3_ERR = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxDIAG_OBD_HYB_1Info_QU_FN_OBD_1_SEN_4_ERR(data) do \
{ \
    Proxy_TxDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_4_ERR = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxDIAG_OBD_HYB_1Info_DIAG_OBD_HYB_1_MUX_MAX(data) do \
{ \
    Proxy_TxDIAG_OBD_HYB_1Info.DIAG_OBD_HYB_1_MUX_MAX = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxDIAG_OBD_HYB_1Info_QU_FN_OBD_1_SEN_2_ERR(data) do \
{ \
    Proxy_TxDIAG_OBD_HYB_1Info.QU_FN_OBD_1_SEN_2_ERR = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxDIAG_OBD_HYB_1Info_DIAG_OBD_HYB_1_MUX_IMME(data) do \
{ \
    Proxy_TxDIAG_OBD_HYB_1Info.DIAG_OBD_HYB_1_MUX_IMME = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxSU_DSCInfo_SU_DSC_WSS(data) do \
{ \
    Proxy_TxSU_DSCInfo.SU_DSC_WSS = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxST_TYR_RDCInfo_QU_TPL_RDC(data) do \
{ \
    Proxy_TxST_TYR_RDCInfo.QU_TPL_RDC = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxST_TYR_RDCInfo_QU_FN_TYR_INFO_RDC(data) do \
{ \
    Proxy_TxST_TYR_RDCInfo.QU_FN_TYR_INFO_RDC = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxST_TYR_RDCInfo_QU_TFAI_RDC(data) do \
{ \
    Proxy_TxST_TYR_RDCInfo.QU_TFAI_RDC = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxST_TYR_RPAInfo_QU_TFAI_RPA(data) do \
{ \
    Proxy_TxST_TYR_RPAInfo.QU_TFAI_RPA = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxST_TYR_RPAInfo_QU_FN_TYR_INFO_RPA(data) do \
{ \
    Proxy_TxST_TYR_RPAInfo.QU_FN_TYR_INFO_RPA = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxST_TYR_RPAInfo_QU_TPL_RPA(data) do \
{ \
    Proxy_TxST_TYR_RPAInfo.QU_TPL_RPA = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info_TYR_ID_2(data) do \
{ \
    Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_2 = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info_TYR_ID_1(data) do \
{ \
    Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_1 = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info_TYR_ID_3(data) do \
{ \
    Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_3 = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info_TYR_ID_4(data) do \
{ \
    Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info.TYR_ID_4 = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxST_TYR_2Info_QU_RDC_INIT_DISP(data) do \
{ \
    Proxy_TxST_TYR_2Info.QU_RDC_INIT_DISP = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_T_TYRInfo_AVL_TEMP_TYR_RRH(data) do \
{ \
    Proxy_TxAVL_T_TYRInfo.AVL_TEMP_TYR_RRH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_T_TYRInfo_AVL_TEMP_TYR_FLH(data) do \
{ \
    Proxy_TxAVL_T_TYRInfo.AVL_TEMP_TYR_FLH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_T_TYRInfo_AVL_TEMP_TYR_RLH(data) do \
{ \
    Proxy_TxAVL_T_TYRInfo.AVL_TEMP_TYR_RLH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_T_TYRInfo_AVL_TEMP_TYR_FRH(data) do \
{ \
    Proxy_TxAVL_T_TYRInfo.AVL_TEMP_TYR_FRH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTEMP_BRKInfo_TEMP_BRDSK_RLH_VRFD(data) do \
{ \
    Proxy_TxTEMP_BRKInfo.TEMP_BRDSK_RLH_VRFD = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTEMP_BRKInfo_QU_TEMP_BRDSK_RLH_VRFD(data) do \
{ \
    Proxy_TxTEMP_BRKInfo.QU_TEMP_BRDSK_RLH_VRFD = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTEMP_BRKInfo_TEMP_BRDSK_RRH_VRFD(data) do \
{ \
    Proxy_TxTEMP_BRKInfo.TEMP_BRDSK_RRH_VRFD = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxTEMP_BRKInfo_QU_TEMP_BRDSK_RRH_VRFD(data) do \
{ \
    Proxy_TxTEMP_BRKInfo.QU_TEMP_BRDSK_RRH_VRFD = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_P_TYRInfo_AVL_P_TYR_FRH(data) do \
{ \
    Proxy_TxAVL_P_TYRInfo.AVL_P_TYR_FRH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_P_TYRInfo_AVL_P_TYR_FLH(data) do \
{ \
    Proxy_TxAVL_P_TYRInfo.AVL_P_TYR_FLH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_P_TYRInfo_AVL_P_TYR_RRH(data) do \
{ \
    Proxy_TxAVL_P_TYRInfo.AVL_P_TYR_RRH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxAVL_P_TYRInfo_AVL_P_TYR_RLH(data) do \
{ \
    Proxy_TxAVL_P_TYRInfo.AVL_P_TYR_RLH = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxFR_DBG_DSCInfo_DBG_RDCi_13(data) do \
{ \
    Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_13 = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxFR_DBG_DSCInfo_DBG_RDCi_04(data) do \
{ \
    Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_04 = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxFR_DBG_DSCInfo_DBG_RDCi_05(data) do \
{ \
    Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_05 = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxFR_DBG_DSCInfo_DBG_RDCi_06(data) do \
{ \
    Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_06 = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxFR_DBG_DSCInfo_DBG_RDCi_01(data) do \
{ \
    Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_01 = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxFR_DBG_DSCInfo_DBG_RDCi_02(data) do \
{ \
    Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_02 = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxFR_DBG_DSCInfo_DBG_RDCi_03(data) do \
{ \
    Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_03 = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxFR_DBG_DSCInfo_DBG_RDCi_07(data) do \
{ \
    Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_07 = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxFR_DBG_DSCInfo_DBG_RDCi_11(data) do \
{ \
    Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_11 = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxFR_DBG_DSCInfo_DBG_RDCi_10(data) do \
{ \
    Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_10 = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxFR_DBG_DSCInfo_DBG_RDCi_09(data) do \
{ \
    Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_09 = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxFR_DBG_DSCInfo_DBG_RDCi_15(data) do \
{ \
    Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_15 = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxFR_DBG_DSCInfo_DBG_RDCi_14(data) do \
{ \
    Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_14 = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxFR_DBG_DSCInfo_DBG_RDCi_12(data) do \
{ \
    Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_12 = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxFR_DBG_DSCInfo_DBG_RDCi_00(data) do \
{ \
    Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_00 = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxFR_DBG_DSCInfo_BDTM_TA_FR(data) do \
{ \
    Proxy_TxFR_DBG_DSCInfo.BDTM_TA_FR = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxFR_DBG_DSCInfo_BDTM_TA_RL(data) do \
{ \
    Proxy_TxFR_DBG_DSCInfo.BDTM_TA_RL = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxFR_DBG_DSCInfo_BDTM_TA_RR(data) do \
{ \
    Proxy_TxFR_DBG_DSCInfo.BDTM_TA_RR = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxFR_DBG_DSCInfo_DBG_RDCi_08(data) do \
{ \
    Proxy_TxFR_DBG_DSCInfo.DBG_RDCi_08 = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxFR_DBG_DSCInfo_ST_STATE(data) do \
{ \
    Proxy_TxFR_DBG_DSCInfo.ST_STATE = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxFR_DBG_DSCInfo_BDTM_TA_FL(data) do \
{ \
    Proxy_TxFR_DBG_DSCInfo.BDTM_TA_FL = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxFR_DBG_DSCInfo_ABSRef(data) do \
{ \
    Proxy_TxFR_DBG_DSCInfo.ABSRef = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxFR_DBG_DSCInfo_LOW_VOLTAGE(data) do \
{ \
    Proxy_TxFR_DBG_DSCInfo.LOW_VOLTAGE = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxFR_DBG_DSCInfo_DEBUG_PCIB(data) do \
{ \
    Proxy_TxFR_DBG_DSCInfo.DEBUG_PCIB = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxFR_DBG_DSCInfo_DBG_DSC(data) do \
{ \
    Proxy_TxFR_DBG_DSCInfo.DBG_DSC = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxFR_DBG_DSCInfo_LATACC(data) do \
{ \
    Proxy_TxFR_DBG_DSCInfo.LATACC = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxFR_DBG_DSCInfo_TRIGGER_FS(data) do \
{ \
    Proxy_TxFR_DBG_DSCInfo.TRIGGER_FS = *data; \
}while(0);

#define Proxy_Tx_Write_Proxy_TxFR_DBG_DSCInfo_TRIGGER_IS(data) do \
{ \
    Proxy_TxFR_DBG_DSCInfo.TRIGGER_IS = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PROXY_TX_IFA_H_ */
/** @} */
