/**
 * @defgroup Proxy_TxByCom Proxy_TxByCom
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Proxy_TxByCom.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PROXY_TXBYCOM_H_
#define PROXY_TXBYCOM_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Proxy_Types.h"
#include "Proxy_Cfg.h"
#include "Proxy_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define PROXY_TXBYCOM_MODULE_ID      (0)
 #define PROXY_TXBYCOM_MAJOR_VERSION  (2)
 #define PROXY_TXBYCOM_MINOR_VERSION  (0)
 #define PROXY_TXBYCOM_PATCH_VERSION  (0)
 #define PROXY_TXBYCOM_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Proxy_TxByCom_HdrBusType Proxy_TxByComBus;

/* Version Info */
extern const SwcVersionInfo_t Proxy_TxByComVersionInfo;

/* Input Data Element */
extern Proxy_TxTxYawCbitInfo_t Proxy_TxByComTxYawCbitInfo;
extern Proxy_TxTxWhlSpdInfo_t Proxy_TxByComTxWhlSpdInfo;
extern Proxy_TxTxWhlPulInfo_t Proxy_TxByComTxWhlPulInfo;
extern Proxy_TxTxTcs5Info_t Proxy_TxByComTxTcs5Info;
extern Proxy_TxTxTcs1Info_t Proxy_TxByComTxTcs1Info;
extern Proxy_TxTxSasCalInfo_t Proxy_TxByComTxSasCalInfo;
extern Proxy_TxTxEsp2Info_t Proxy_TxByComTxEsp2Info;
extern Proxy_TxTxEbs1Info_t Proxy_TxByComTxEbs1Info;
extern Proxy_TxTxAhb1Info_t Proxy_TxByComTxAhb1Info;
extern SenPwrM_MainSenPwrMonitor_t Proxy_TxByComSenPwrMonitorData;
extern Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo_t Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo;
extern Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo_t Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo;
extern Proxy_TxAVL_BRTORQ_WHLInfo_t Proxy_TxByComAVL_BRTORQ_WHLInfo;
extern Proxy_TxAVL_RPM_WHLInfo_t Proxy_TxByComAVL_RPM_WHLInfo;
extern Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo_t Proxy_TxByComRQ_WMOM_PT_SUM_RECUPInfo;
extern Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info_t Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info;
extern Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo_t Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo;
extern Proxy_TxAVL_QUAN_EES_WHLInfo_t Proxy_TxByComAVL_QUAN_EES_WHLInfo;
extern Proxy_TxTAR_LTRQD_BAXInfo_t Proxy_TxByComTAR_LTRQD_BAXInfo;
extern Proxy_TxST_YMRInfo_t Proxy_TxByComST_YMRInfo;
extern Proxy_TxWEGSTRECKEInfo_t Proxy_TxByComWEGSTRECKEInfo;
extern Proxy_TxDISP_CC_DRDY_00Info_t Proxy_TxByComDISP_CC_DRDY_00Info;
extern Proxy_TxDISP_CC_BYPA_00Info_t Proxy_TxByComDISP_CC_BYPA_00Info;
extern Proxy_TxST_VHSSInfo_t Proxy_TxByComST_VHSSInfo;
extern Proxy_TxDIAG_OBD_HYB_1Info_t Proxy_TxByComDIAG_OBD_HYB_1Info;
extern Proxy_TxSU_DSCInfo_t Proxy_TxByComSU_DSCInfo;
extern Proxy_TxST_TYR_RDCInfo_t Proxy_TxByComST_TYR_RDCInfo;
extern Proxy_TxST_TYR_RPAInfo_t Proxy_TxByComST_TYR_RPAInfo;
extern Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info_t Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info;
extern Proxy_TxST_TYR_2Info_t Proxy_TxByComST_TYR_2Info;
extern Proxy_TxAVL_T_TYRInfo_t Proxy_TxByComAVL_T_TYRInfo;
extern Proxy_TxTEMP_BRKInfo_t Proxy_TxByComTEMP_BRKInfo;
extern Proxy_TxAVL_P_TYRInfo_t Proxy_TxByComAVL_P_TYRInfo;
extern Proxy_TxFR_DBG_DSCInfo_t Proxy_TxByComFR_DBG_DSCInfo;

/* Output Data Element */

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Proxy_TxByCom_Init(void);
extern void Proxy_TxByCom(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PROXY_TXBYCOM_H_ */
/** @} */
