/**
 * @defgroup Proxy_Rx Proxy_Rx
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Proxy_Rx.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PROXY_RX_H_
#define PROXY_RX_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Proxy_Types.h"
#include "Proxy_Cfg.h"
#include "Proxy_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define PROXY_RX_MODULE_ID      (0)
 #define PROXY_RX_MAJOR_VERSION  (2)
 #define PROXY_RX_MINOR_VERSION  (0)
 #define PROXY_RX_PATCH_VERSION  (0)
 #define PROXY_RX_BRANCH_VERSION (0)

#define CAN_overrun_flg             CanChk1.u1CAN_overrun
#define CAN_bus_off_flg             CanChk1.u1CAN_bus_off
#define ccu1TcuChkFlg               CanChk1.u1TcuChk
#define ccu1TxDisableFlg            CanChk1.u1can_tx_disable
#define ccu1TxSubDisableFlg         CanChk1.u1can_tx_disable_sub
#define ccu1AtmChkFlg               CanChk1.u1AtmChk
#define ccu1VariantErrSetFlg        CanChk1.VariantErrSet
#define ccu1TcuInfoRxOKflg          CanChk1.u1TcuInfoRxOKflg
#define ccu1SysCodingOkFlg          CanChk1.u1SysCodingOk
#define ccu1ChkTimeFixFlg           CanChk1.u1ChkTimeFix

#define ccu1IgnOnTimeOkFlg          CanChk2.u1IgnOnTimeOk
#define ccu1Fwd1RxCopyFlg           CanChk2.u1Fwd1RxCopy
#define ccu1MainCheckCanFMsgFlg     CanChk2.u1MainCheckFCanMsgFlg
#define ccu1SubCheckCanFMsgFlg      CanChk2.u1SubCheckFCanMsgFlg
#define ccu1InhibitToChk            CanChk2.u1InhibitToChk
#define ccu1DecideCheckSize         CanChk2.u1DecideCheckSize
#define cdu1DisNormCommFlg          CanChk2.u1DisNormComm
#define ccu1CanChkMinSpeedFlg       CanChk2.u1CanChkMinSpeed
#define ccu1CanChkInhbtFlg          CanChk2.u1CanChkInhbt

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Proxy_Rx_HdrBusType Proxy_RxBus;

/* Version Info */
extern const SwcVersionInfo_t Proxy_RxVersionInfo;

/* Input Data Element */
extern Proxy_RxByComRxBms1Info_t Proxy_RxRxBms1Info;
extern Proxy_RxByComRxYawSerialInfo_t Proxy_RxRxYawSerialInfo;
extern Proxy_RxByComRxYawAccInfo_t Proxy_RxRxYawAccInfo;
extern Proxy_RxByComRxTcu6Info_t Proxy_RxRxTcu6Info;
extern Proxy_RxByComRxTcu5Info_t Proxy_RxRxTcu5Info;
extern Proxy_RxByComRxTcu1Info_t Proxy_RxRxTcu1Info;
extern Proxy_RxByComRxSasInfo_t Proxy_RxRxSasInfo;
extern Proxy_RxByComRxMcu2Info_t Proxy_RxRxMcu2Info;
extern Proxy_RxByComRxMcu1Info_t Proxy_RxRxMcu1Info;
extern Proxy_RxByComRxLongAccInfo_t Proxy_RxRxLongAccInfo;
extern Proxy_RxByComRxHcu5Info_t Proxy_RxRxHcu5Info;
extern Proxy_RxByComRxHcu3Info_t Proxy_RxRxHcu3Info;
extern Proxy_RxByComRxHcu2Info_t Proxy_RxRxHcu2Info;
extern Proxy_RxByComRxHcu1Info_t Proxy_RxRxHcu1Info;
extern Proxy_RxByComRxFact1Info_t Proxy_RxRxFact1Info;
extern Proxy_RxByComRxEms3Info_t Proxy_RxRxEms3Info;
extern Proxy_RxByComRxEms2Info_t Proxy_RxRxEms2Info;
extern Proxy_RxByComRxEms1Info_t Proxy_RxRxEms1Info;
extern Proxy_RxByComRxClu2Info_t Proxy_RxRxClu2Info;
extern Proxy_RxByComRxClu1Info_t Proxy_RxRxClu1Info;
extern Proxy_RxByComRxMsgOkFlgInfo_t Proxy_RxRxMsgOkFlgInfo;
extern Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo_t Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo;
extern Proxy_RxByComAVL_LTRQD_BAXInfo_t Proxy_RxAVL_LTRQD_BAXInfo;
extern Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo_t Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo;
extern Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info_t Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info;
extern Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info_t Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info;
extern Proxy_RxByComHGLV_VEH_FILTInfo_t Proxy_RxHGLV_VEH_FILTInfo;
extern Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo_t Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo;
extern Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo_t Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo;
extern Proxy_RxByComV_VEH_V_VEH_2Info_t Proxy_RxV_VEH_V_VEH_2Info;
extern Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo_t Proxy_RxDT_DRDYSEN_EXT_VYAW_VEHInfo;
extern Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo_t Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo;
extern Proxy_RxByComAVL_STEA_FTAXInfo_t Proxy_RxAVL_STEA_FTAXInfo;
extern Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_t Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo;
extern Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info_t Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info;
extern Proxy_RxByComWMOM_DRV_7Info_t Proxy_RxWMOM_DRV_7Info;
extern Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo_t Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo;
extern Proxy_RxByComDT_BRKSYS_ENGMGInfo_t Proxy_RxDT_BRKSYS_ENGMGInfo;
extern Proxy_RxByComERRM_BN_UInfo_t Proxy_RxERRM_BN_UInfo;
extern Proxy_RxByComCTR_CRInfo_t Proxy_RxCTR_CRInfo;
extern Proxy_RxByComKLEMMENInfo_t Proxy_RxKLEMMENInfo;
extern Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_t Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info;
extern Proxy_RxByComBEDIENUNG_WISCHERInfo_t Proxy_RxBEDIENUNG_WISCHERInfo;
extern Proxy_RxByComDT_PT_2Info_t Proxy_RxDT_PT_2Info;
extern Proxy_RxByComSTAT_ENG_STA_AUTOInfo_t Proxy_RxSTAT_ENG_STA_AUTOInfo;
extern Proxy_RxByComST_ENERG_GENInfo_t Proxy_RxST_ENERG_GENInfo;
extern Proxy_RxByComDT_PT_1Info_t Proxy_RxDT_PT_1Info;
extern Proxy_RxByComDT_GRDT_DRVInfo_t Proxy_RxDT_GRDT_DRVInfo;
extern Proxy_RxByComDIAG_OBD_ENGMG_ELInfo_t Proxy_RxDIAG_OBD_ENGMG_ELInfo;
extern Proxy_RxByComSTAT_CT_HABRInfo_t Proxy_RxSTAT_CT_HABRInfo;
extern Proxy_RxByComDT_PT_3Info_t Proxy_RxDT_PT_3Info;
extern Proxy_RxByComEINHEITEN_BN2020Info_t Proxy_RxEINHEITEN_BN2020Info;
extern Proxy_RxByComA_TEMPInfo_t Proxy_RxA_TEMPInfo;
extern Proxy_RxByComWISCHERGESCHWINDIGKEITInfo_t Proxy_RxWISCHERGESCHWINDIGKEITInfo;
extern Proxy_RxByComSTAT_DS_VRFDInfo_t Proxy_RxSTAT_DS_VRFDInfo;
extern Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info_t Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info;
extern Proxy_RxByComSTAT_ANHAENGERInfo_t Proxy_RxSTAT_ANHAENGERInfo;
extern Proxy_RxByComFZZSTDInfo_t Proxy_RxFZZSTDInfo;
extern Proxy_RxByComBEDIENUNG_FAHRWERKInfo_t Proxy_RxBEDIENUNG_FAHRWERKInfo;
extern Proxy_RxByComFAHRZEUGTYPInfo_t Proxy_RxFAHRZEUGTYPInfo;
extern Proxy_RxByComST_BLT_CT_SOCCUInfo_t Proxy_RxST_BLT_CT_SOCCUInfo;
extern Proxy_RxByComFAHRGESTELLNUMMERInfo_t Proxy_RxFAHRGESTELLNUMMERInfo;
extern Proxy_RxByComRELATIVZEITInfo_t Proxy_RxRELATIVZEITInfo;
extern Proxy_RxByComKILOMETERSTANDInfo_t Proxy_RxKILOMETERSTANDInfo;
extern Proxy_RxByComUHRZEIT_DATUMInfo_t Proxy_RxUHRZEIT_DATUMInfo;
extern Mom_HndlrEcuModeSts_t Proxy_RxEcuModeSts;
extern Pct_5msCtrlPCtrlAct_t Proxy_RxPCtrlAct;
extern Eem_SuspcDetnFuncInhibitProxySts_t Proxy_RxFuncInhibitProxySts;

/* Output Data Element */
extern Proxy_RxCanRxRegenInfo_t Proxy_RxCanRxRegenInfo;
extern Proxy_RxCanRxAccelPedlInfo_t Proxy_RxCanRxAccelPedlInfo;
extern Proxy_RxCanRxIdbInfo_t Proxy_RxCanRxIdbInfo;
extern Proxy_RxCanRxEngTempInfo_t Proxy_RxCanRxEngTempInfo;
extern Proxy_RxCanRxEscInfo_t Proxy_RxCanRxEscInfo;
extern Proxy_RxCanRxClu2Info_t Proxy_RxCanRxClu2Info;
extern Proxy_RxCanRxEemInfo_t Proxy_RxCanRxEemInfo;
extern Proxy_RxCanRxInfo_t Proxy_RxCanRxInfo;
extern Proxy_RxIMUCalc_t Proxy_RxIMUCalcInfo;
extern Proxy_RxCanRxGearSelDispErrInfo_t Proxy_RxCanRxGearSelDispErrInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Proxy_Rx_Init(void);
extern void Proxy_Rx(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PROXY_RX_H_ */
/** @} */
