/**
 * @defgroup Proxy_Tx Proxy_Tx
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Proxy_Tx.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PROXY_TX_H_
#define PROXY_TX_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Proxy_Types.h"
#include "Proxy_Cfg.h"
#include "Proxy_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define PROXY_TX_MODULE_ID      (0)
 #define PROXY_TX_MAJOR_VERSION  (2)
 #define PROXY_TX_MINOR_VERSION  (0)
 #define PROXY_TX_PATCH_VERSION  (0)
 #define PROXY_TX_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Proxy_Tx_HdrBusType Proxy_TxBus;

/* Version Info */
extern const SwcVersionInfo_t Proxy_TxVersionInfo;

/* Input Data Element */
extern Eem_MainEemCtrlInhibitData_t Proxy_TxEemCtrlInhibitData;
extern Abc_CtrlCanTxInfo_t Proxy_TxCanTxInfo;
extern Pedal_SenSyncPdt5msRawInfo_t Proxy_TxPdt5msRawInfo;
extern Pedal_SenSyncPdf5msRawInfo_t Proxy_TxPdf5msRawInfo;
extern Rbc_CtrlTarRgnBrkTqInfo_t Proxy_TxTarRgnBrkTqInfo;
extern Swt_SenEscSwtStInfo_t Proxy_TxEscSwtStInfo;
extern Wss_SenWhlSpdInfo_t Proxy_TxWhlSpdInfo;
extern Wss_SenWhlEdgeCntInfo_t Proxy_TxWhlEdgeCntInfo;
extern Press_SenSyncPistP5msRawInfo_t Proxy_TxPistP5msRawInfo;
extern Nvm_HndlrLogicEepDataInfo_t Proxy_TxLogicEepDataInfo;
extern Eem_MainEemLampData_t Proxy_TxEemLampData;
extern Mom_HndlrEcuModeSts_t Proxy_TxEcuModeSts;
extern Prly_HndlrIgnOnOffSts_t Proxy_TxIgnOnOffSts;
extern Diag_HndlrDiagSci_t Proxy_TxDiagSci;
extern Diag_HndlrDiagAhbSci_t Proxy_TxDiagAhbSci;
extern Eem_SuspcDetnFuncInhibitProxySts_t Proxy_TxFuncInhibitProxySts;

/* Output Data Element */
extern Proxy_TxTxESCSensorInfo_t Proxy_TxTxESCSensorInfo;
extern Proxy_TxTxYawCbitInfo_t Proxy_TxTxYawCbitInfo;
extern Proxy_TxTxWhlSpdInfo_t Proxy_TxTxWhlSpdInfo;
extern Proxy_TxTxWhlPulInfo_t Proxy_TxTxWhlPulInfo;
extern Proxy_TxTxTcs5Info_t Proxy_TxTxTcs5Info;
extern Proxy_TxTxTcs1Info_t Proxy_TxTxTcs1Info;
extern Proxy_TxTxSasCalInfo_t Proxy_TxTxSasCalInfo;
extern Proxy_TxTxEsp2Info_t Proxy_TxTxEsp2Info;
extern Proxy_TxTxEbs1Info_t Proxy_TxTxEbs1Info;
extern Proxy_TxTxAhb1Info_t Proxy_TxTxAhb1Info;
extern Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo_t Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo;
extern Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo_t Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo;
extern Proxy_TxAVL_BRTORQ_WHLInfo_t Proxy_TxAVL_BRTORQ_WHLInfo;
extern Proxy_TxAVL_RPM_WHLInfo_t Proxy_TxAVL_RPM_WHLInfo;
extern Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo_t Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo;
extern Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info_t Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info;
extern Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo_t Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo;
extern Proxy_TxAVL_QUAN_EES_WHLInfo_t Proxy_TxAVL_QUAN_EES_WHLInfo;
extern Proxy_TxTAR_LTRQD_BAXInfo_t Proxy_TxTAR_LTRQD_BAXInfo;
extern Proxy_TxST_YMRInfo_t Proxy_TxST_YMRInfo;
extern Proxy_TxWEGSTRECKEInfo_t Proxy_TxWEGSTRECKEInfo;
extern Proxy_TxDISP_CC_DRDY_00Info_t Proxy_TxDISP_CC_DRDY_00Info;
extern Proxy_TxDISP_CC_BYPA_00Info_t Proxy_TxDISP_CC_BYPA_00Info;
extern Proxy_TxST_VHSSInfo_t Proxy_TxST_VHSSInfo;
extern Proxy_TxDIAG_OBD_HYB_1Info_t Proxy_TxDIAG_OBD_HYB_1Info;
extern Proxy_TxSU_DSCInfo_t Proxy_TxSU_DSCInfo;
extern Proxy_TxST_TYR_RDCInfo_t Proxy_TxST_TYR_RDCInfo;
extern Proxy_TxST_TYR_RPAInfo_t Proxy_TxST_TYR_RPAInfo;
extern Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info_t Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info;
extern Proxy_TxST_TYR_2Info_t Proxy_TxST_TYR_2Info;
extern Proxy_TxAVL_T_TYRInfo_t Proxy_TxAVL_T_TYRInfo;
extern Proxy_TxTEMP_BRKInfo_t Proxy_TxTEMP_BRKInfo;
extern Proxy_TxAVL_P_TYRInfo_t Proxy_TxAVL_P_TYRInfo;
extern Proxy_TxFR_DBG_DSCInfo_t Proxy_TxFR_DBG_DSCInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Proxy_Tx_Init(void);
extern void Proxy_Tx(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PROXY_TX_H_ */
/** @} */
