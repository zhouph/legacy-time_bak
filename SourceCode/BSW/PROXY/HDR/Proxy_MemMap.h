/**
 * @defgroup Proxy_MemMap Proxy_MemMap
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Proxy_MemMap.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PROXY_MEMMAP_H_
#define PROXY_MEMMAP_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/*------------------[MemMap error checking]----------------------------------*/

#if (defined MEMMAP_ERROR) /* to prevent double definition */
#error MEMMAP_ERROR defined, wrong MemMap.h usage
#endif /* if (!defined MEMMAP_ERROR) */

/** \brief Checks if this file has been correctly used
 **
 ** This definition checks if this file has been correctly included
 ** the last time.
 ** We do it here, before the big if-elif. */
#define MEMMAP_ERROR


#if 0
/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (PROXY_START_SEC_CODE)
  #undef PROXY_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_SEC_STARTED
    #error "PROXY section not closed"
  #endif
  #define CHK_PROXY_SEC_STARTED
  #define CHK_PROXY_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_STOP_SEC_CODE)
  #undef PROXY_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_SEC_CODE_STARTED
    #error "PROXY_SEC_CODE not opened"
  #endif
  #undef CHK_PROXY_SEC_STARTED
  #undef CHK_PROXY_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (PROXY_START_SEC_CONST_UNSPECIFIED)
  #undef PROXY_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_SEC_STARTED
    #error "PROXY section not closed"
  #endif
  #define CHK_PROXY_SEC_STARTED
  #define CHK_PROXY_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_STOP_SEC_CONST_UNSPECIFIED)
  #undef PROXY_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_SEC_CONST_UNSPECIFIED_STARTED
    #error "PROXY_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_PROXY_SEC_STARTED
  #undef CHK_PROXY_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (PROXY_START_SEC_VAR_UNSPECIFIED)
  #undef PROXY_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_SEC_STARTED
    #error "PROXY section not closed"
  #endif
  #define CHK_PROXY_SEC_STARTED
  #define CHK_PROXY_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_STOP_SEC_VAR_UNSPECIFIED)
  #undef PROXY_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_SEC_VAR_UNSPECIFIED_STARTED
    #error "PROXY_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_PROXY_SEC_STARTED
  #undef CHK_PROXY_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_START_SEC_VAR_32BIT)
  #undef PROXY_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_SEC_STARTED
    #error "PROXY section not closed"
  #endif
  #define CHK_PROXY_SEC_STARTED
  #define CHK_PROXY_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_STOP_SEC_VAR_32BIT)
  #undef PROXY_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_SEC_VAR_32BIT_STARTED
    #error "PROXY_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_PROXY_SEC_STARTED
  #undef CHK_PROXY_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef PROXY_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_SEC_STARTED
    #error "PROXY section not closed"
  #endif
  #define CHK_PROXY_SEC_STARTED
  #define CHK_PROXY_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef PROXY_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "PROXY_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_PROXY_SEC_STARTED
  #undef CHK_PROXY_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_START_SEC_VAR_NOINIT_32BIT)
  #undef PROXY_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_SEC_STARTED
    #error "PROXY section not closed"
  #endif
  #define CHK_PROXY_SEC_STARTED
  #define CHK_PROXY_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_STOP_SEC_VAR_NOINIT_32BIT)
  #undef PROXY_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_SEC_VAR_NOINIT_32BIT_STARTED
    #error "PROXY_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_PROXY_SEC_STARTED
  #undef CHK_PROXY_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (PROXY_START_SEC_CALIB_UNSPECIFIED)
  #undef PROXY_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_SEC_STARTED
    #error "PROXY section not closed"
  #endif
  #define CHK_PROXY_SEC_STARTED
  #define CHK_PROXY_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_STOP_SEC_CALIB_UNSPECIFIED)
  #undef PROXY_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "PROXY_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_PROXY_SEC_STARTED
  #undef CHK_PROXY_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR



/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (PROXY_RX_START_SEC_CODE)
  #undef PROXY_RX_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_RX_SEC_STARTED
    #error "PROXY_RX section not closed"
  #endif
  #define CHK_PROXY_RX_SEC_STARTED
  #define CHK_PROXY_RX_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_RX_STOP_SEC_CODE)
  #undef PROXY_RX_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_RX_SEC_CODE_STARTED
    #error "PROXY_RX_SEC_CODE not opened"
  #endif
  #undef CHK_PROXY_RX_SEC_STARTED
  #undef CHK_PROXY_RX_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (PROXY_RX_START_SEC_CONST_UNSPECIFIED)
  #undef PROXY_RX_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_RX_SEC_STARTED
    #error "PROXY_RX section not closed"
  #endif
  #define CHK_PROXY_RX_SEC_STARTED
  #define CHK_PROXY_RX_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_RX_STOP_SEC_CONST_UNSPECIFIED)
  #undef PROXY_RX_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_RX_SEC_CONST_UNSPECIFIED_STARTED
    #error "PROXY_RX_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_PROXY_RX_SEC_STARTED
  #undef CHK_PROXY_RX_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (PROXY_RX_START_SEC_VAR_UNSPECIFIED)
  #undef PROXY_RX_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_RX_SEC_STARTED
    #error "PROXY_RX section not closed"
  #endif
  #define CHK_PROXY_RX_SEC_STARTED
  #define CHK_PROXY_RX_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_RX_STOP_SEC_VAR_UNSPECIFIED)
  #undef PROXY_RX_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_RX_SEC_VAR_UNSPECIFIED_STARTED
    #error "PROXY_RX_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_PROXY_RX_SEC_STARTED
  #undef CHK_PROXY_RX_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_RX_START_SEC_VAR_32BIT)
  #undef PROXY_RX_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_RX_SEC_STARTED
    #error "PROXY_RX section not closed"
  #endif
  #define CHK_PROXY_RX_SEC_STARTED
  #define CHK_PROXY_RX_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_RX_STOP_SEC_VAR_32BIT)
  #undef PROXY_RX_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_RX_SEC_VAR_32BIT_STARTED
    #error "PROXY_RX_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_PROXY_RX_SEC_STARTED
  #undef CHK_PROXY_RX_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_RX_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef PROXY_RX_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_RX_SEC_STARTED
    #error "PROXY_RX section not closed"
  #endif
  #define CHK_PROXY_RX_SEC_STARTED
  #define CHK_PROXY_RX_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_RX_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef PROXY_RX_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_RX_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "PROXY_RX_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_PROXY_RX_SEC_STARTED
  #undef CHK_PROXY_RX_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_RX_START_SEC_VAR_NOINIT_32BIT)
  #undef PROXY_RX_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_RX_SEC_STARTED
    #error "PROXY_RX section not closed"
  #endif
  #define CHK_PROXY_RX_SEC_STARTED
  #define CHK_PROXY_RX_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_RX_STOP_SEC_VAR_NOINIT_32BIT)
  #undef PROXY_RX_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_RX_SEC_VAR_NOINIT_32BIT_STARTED
    #error "PROXY_RX_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_PROXY_RX_SEC_STARTED
  #undef CHK_PROXY_RX_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (PROXY_RX_START_SEC_CALIB_UNSPECIFIED)
  #undef PROXY_RX_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_RX_SEC_STARTED
    #error "PROXY_RX section not closed"
  #endif
  #define CHK_PROXY_RX_SEC_STARTED
  #define CHK_PROXY_RX_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_RX_STOP_SEC_CALIB_UNSPECIFIED)
  #undef PROXY_RX_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_RX_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "PROXY_RX_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_PROXY_RX_SEC_STARTED
  #undef CHK_PROXY_RX_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (PROXY_RXBYCOM_START_SEC_CODE)
  #undef PROXY_RXBYCOM_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_RXBYCOM_SEC_STARTED
    #error "PROXY_RXBYCOM section not closed"
  #endif
  #define CHK_PROXY_RXBYCOM_SEC_STARTED
  #define CHK_PROXY_RXBYCOM_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_RXBYCOM_STOP_SEC_CODE)
  #undef PROXY_RXBYCOM_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_RXBYCOM_SEC_CODE_STARTED
    #error "PROXY_RXBYCOM_SEC_CODE not opened"
  #endif
  #undef CHK_PROXY_RXBYCOM_SEC_STARTED
  #undef CHK_PROXY_RXBYCOM_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (PROXY_RXBYCOM_START_SEC_CONST_UNSPECIFIED)
  #undef PROXY_RXBYCOM_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_RXBYCOM_SEC_STARTED
    #error "PROXY_RXBYCOM section not closed"
  #endif
  #define CHK_PROXY_RXBYCOM_SEC_STARTED
  #define CHK_PROXY_RXBYCOM_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_RXBYCOM_STOP_SEC_CONST_UNSPECIFIED)
  #undef PROXY_RXBYCOM_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_RXBYCOM_SEC_CONST_UNSPECIFIED_STARTED
    #error "PROXY_RXBYCOM_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_PROXY_RXBYCOM_SEC_STARTED
  #undef CHK_PROXY_RXBYCOM_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (PROXY_RXBYCOM_START_SEC_VAR_UNSPECIFIED)
  #undef PROXY_RXBYCOM_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_RXBYCOM_SEC_STARTED
    #error "PROXY_RXBYCOM section not closed"
  #endif
  #define CHK_PROXY_RXBYCOM_SEC_STARTED
  #define CHK_PROXY_RXBYCOM_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_RXBYCOM_STOP_SEC_VAR_UNSPECIFIED)
  #undef PROXY_RXBYCOM_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_RXBYCOM_SEC_VAR_UNSPECIFIED_STARTED
    #error "PROXY_RXBYCOM_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_PROXY_RXBYCOM_SEC_STARTED
  #undef CHK_PROXY_RXBYCOM_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_RXBYCOM_START_SEC_VAR_32BIT)
  #undef PROXY_RXBYCOM_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_RXBYCOM_SEC_STARTED
    #error "PROXY_RXBYCOM section not closed"
  #endif
  #define CHK_PROXY_RXBYCOM_SEC_STARTED
  #define CHK_PROXY_RXBYCOM_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_RXBYCOM_STOP_SEC_VAR_32BIT)
  #undef PROXY_RXBYCOM_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_RXBYCOM_SEC_VAR_32BIT_STARTED
    #error "PROXY_RXBYCOM_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_PROXY_RXBYCOM_SEC_STARTED
  #undef CHK_PROXY_RXBYCOM_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_RXBYCOM_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef PROXY_RXBYCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_RXBYCOM_SEC_STARTED
    #error "PROXY_RXBYCOM section not closed"
  #endif
  #define CHK_PROXY_RXBYCOM_SEC_STARTED
  #define CHK_PROXY_RXBYCOM_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_RXBYCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef PROXY_RXBYCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_RXBYCOM_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "PROXY_RXBYCOM_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_PROXY_RXBYCOM_SEC_STARTED
  #undef CHK_PROXY_RXBYCOM_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_RXBYCOM_START_SEC_VAR_NOINIT_32BIT)
  #undef PROXY_RXBYCOM_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_RXBYCOM_SEC_STARTED
    #error "PROXY_RXBYCOM section not closed"
  #endif
  #define CHK_PROXY_RXBYCOM_SEC_STARTED
  #define CHK_PROXY_RXBYCOM_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_RXBYCOM_STOP_SEC_VAR_NOINIT_32BIT)
  #undef PROXY_RXBYCOM_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_RXBYCOM_SEC_VAR_NOINIT_32BIT_STARTED
    #error "PROXY_RXBYCOM_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_PROXY_RXBYCOM_SEC_STARTED
  #undef CHK_PROXY_RXBYCOM_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (PROXY_RXBYCOM_START_SEC_CALIB_UNSPECIFIED)
  #undef PROXY_RXBYCOM_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_RXBYCOM_SEC_STARTED
    #error "PROXY_RXBYCOM section not closed"
  #endif
  #define CHK_PROXY_RXBYCOM_SEC_STARTED
  #define CHK_PROXY_RXBYCOM_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_RXBYCOM_STOP_SEC_CALIB_UNSPECIFIED)
  #undef PROXY_RXBYCOM_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_RXBYCOM_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "PROXY_RXBYCOM_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_PROXY_RXBYCOM_SEC_STARTED
  #undef CHK_PROXY_RXBYCOM_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (PROXY_TX_START_SEC_CODE)
  #undef PROXY_TX_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_TX_SEC_STARTED
    #error "PROXY_TX section not closed"
  #endif
  #define CHK_PROXY_TX_SEC_STARTED
  #define CHK_PROXY_TX_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_TX_STOP_SEC_CODE)
  #undef PROXY_TX_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_TX_SEC_CODE_STARTED
    #error "PROXY_TX_SEC_CODE not opened"
  #endif
  #undef CHK_PROXY_TX_SEC_STARTED
  #undef CHK_PROXY_TX_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (PROXY_TX_START_SEC_CONST_UNSPECIFIED)
  #undef PROXY_TX_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_TX_SEC_STARTED
    #error "PROXY_TX section not closed"
  #endif
  #define CHK_PROXY_TX_SEC_STARTED
  #define CHK_PROXY_TX_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_TX_STOP_SEC_CONST_UNSPECIFIED)
  #undef PROXY_TX_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_TX_SEC_CONST_UNSPECIFIED_STARTED
    #error "PROXY_TX_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_PROXY_TX_SEC_STARTED
  #undef CHK_PROXY_TX_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (PROXY_TX_START_SEC_VAR_UNSPECIFIED)
  #undef PROXY_TX_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_TX_SEC_STARTED
    #error "PROXY_TX section not closed"
  #endif
  #define CHK_PROXY_TX_SEC_STARTED
  #define CHK_PROXY_TX_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_TX_STOP_SEC_VAR_UNSPECIFIED)
  #undef PROXY_TX_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_TX_SEC_VAR_UNSPECIFIED_STARTED
    #error "PROXY_TX_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_PROXY_TX_SEC_STARTED
  #undef CHK_PROXY_TX_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_TX_START_SEC_VAR_32BIT)
  #undef PROXY_TX_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_TX_SEC_STARTED
    #error "PROXY_TX section not closed"
  #endif
  #define CHK_PROXY_TX_SEC_STARTED
  #define CHK_PROXY_TX_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_TX_STOP_SEC_VAR_32BIT)
  #undef PROXY_TX_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_TX_SEC_VAR_32BIT_STARTED
    #error "PROXY_TX_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_PROXY_TX_SEC_STARTED
  #undef CHK_PROXY_TX_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_TX_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef PROXY_TX_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_TX_SEC_STARTED
    #error "PROXY_TX section not closed"
  #endif
  #define CHK_PROXY_TX_SEC_STARTED
  #define CHK_PROXY_TX_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_TX_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef PROXY_TX_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_TX_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "PROXY_TX_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_PROXY_TX_SEC_STARTED
  #undef CHK_PROXY_TX_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_TX_START_SEC_VAR_NOINIT_32BIT)
  #undef PROXY_TX_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_TX_SEC_STARTED
    #error "PROXY_TX section not closed"
  #endif
  #define CHK_PROXY_TX_SEC_STARTED
  #define CHK_PROXY_TX_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_TX_STOP_SEC_VAR_NOINIT_32BIT)
  #undef PROXY_TX_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_TX_SEC_VAR_NOINIT_32BIT_STARTED
    #error "PROXY_TX_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_PROXY_TX_SEC_STARTED
  #undef CHK_PROXY_TX_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (PROXY_TX_START_SEC_CALIB_UNSPECIFIED)
  #undef PROXY_TX_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_TX_SEC_STARTED
    #error "PROXY_TX section not closed"
  #endif
  #define CHK_PROXY_TX_SEC_STARTED
  #define CHK_PROXY_TX_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_TX_STOP_SEC_CALIB_UNSPECIFIED)
  #undef PROXY_TX_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_TX_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "PROXY_TX_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_PROXY_TX_SEC_STARTED
  #undef CHK_PROXY_TX_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (PROXY_TXBYCOM_START_SEC_CODE)
  #undef PROXY_TXBYCOM_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_TXBYCOM_SEC_STARTED
    #error "PROXY_TXBYCOM section not closed"
  #endif
  #define CHK_PROXY_TXBYCOM_SEC_STARTED
  #define CHK_PROXY_TXBYCOM_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_TXBYCOM_STOP_SEC_CODE)
  #undef PROXY_TXBYCOM_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_TXBYCOM_SEC_CODE_STARTED
    #error "PROXY_TXBYCOM_SEC_CODE not opened"
  #endif
  #undef CHK_PROXY_TXBYCOM_SEC_STARTED
  #undef CHK_PROXY_TXBYCOM_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (PROXY_TXBYCOM_START_SEC_CONST_UNSPECIFIED)
  #undef PROXY_TXBYCOM_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_TXBYCOM_SEC_STARTED
    #error "PROXY_TXBYCOM section not closed"
  #endif
  #define CHK_PROXY_TXBYCOM_SEC_STARTED
  #define CHK_PROXY_TXBYCOM_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_TXBYCOM_STOP_SEC_CONST_UNSPECIFIED)
  #undef PROXY_TXBYCOM_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_TXBYCOM_SEC_CONST_UNSPECIFIED_STARTED
    #error "PROXY_TXBYCOM_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_PROXY_TXBYCOM_SEC_STARTED
  #undef CHK_PROXY_TXBYCOM_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (PROXY_TXBYCOM_START_SEC_VAR_UNSPECIFIED)
  #undef PROXY_TXBYCOM_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_TXBYCOM_SEC_STARTED
    #error "PROXY_TXBYCOM section not closed"
  #endif
  #define CHK_PROXY_TXBYCOM_SEC_STARTED
  #define CHK_PROXY_TXBYCOM_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_TXBYCOM_STOP_SEC_VAR_UNSPECIFIED)
  #undef PROXY_TXBYCOM_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_TXBYCOM_SEC_VAR_UNSPECIFIED_STARTED
    #error "PROXY_TXBYCOM_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_PROXY_TXBYCOM_SEC_STARTED
  #undef CHK_PROXY_TXBYCOM_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_TXBYCOM_START_SEC_VAR_32BIT)
  #undef PROXY_TXBYCOM_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_TXBYCOM_SEC_STARTED
    #error "PROXY_TXBYCOM section not closed"
  #endif
  #define CHK_PROXY_TXBYCOM_SEC_STARTED
  #define CHK_PROXY_TXBYCOM_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_TXBYCOM_STOP_SEC_VAR_32BIT)
  #undef PROXY_TXBYCOM_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_TXBYCOM_SEC_VAR_32BIT_STARTED
    #error "PROXY_TXBYCOM_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_PROXY_TXBYCOM_SEC_STARTED
  #undef CHK_PROXY_TXBYCOM_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_TXBYCOM_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef PROXY_TXBYCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_TXBYCOM_SEC_STARTED
    #error "PROXY_TXBYCOM section not closed"
  #endif
  #define CHK_PROXY_TXBYCOM_SEC_STARTED
  #define CHK_PROXY_TXBYCOM_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_TXBYCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef PROXY_TXBYCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_TXBYCOM_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "PROXY_TXBYCOM_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_PROXY_TXBYCOM_SEC_STARTED
  #undef CHK_PROXY_TXBYCOM_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_TXBYCOM_START_SEC_VAR_NOINIT_32BIT)
  #undef PROXY_TXBYCOM_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_TXBYCOM_SEC_STARTED
    #error "PROXY_TXBYCOM section not closed"
  #endif
  #define CHK_PROXY_TXBYCOM_SEC_STARTED
  #define CHK_PROXY_TXBYCOM_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_TXBYCOM_STOP_SEC_VAR_NOINIT_32BIT)
  #undef PROXY_TXBYCOM_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_TXBYCOM_SEC_VAR_NOINIT_32BIT_STARTED
    #error "PROXY_TXBYCOM_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_PROXY_TXBYCOM_SEC_STARTED
  #undef CHK_PROXY_TXBYCOM_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (PROXY_TXBYCOM_START_SEC_CALIB_UNSPECIFIED)
  #undef PROXY_TXBYCOM_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_PROXY_TXBYCOM_SEC_STARTED
    #error "PROXY_TXBYCOM section not closed"
  #endif
  #define CHK_PROXY_TXBYCOM_SEC_STARTED
  #define CHK_PROXY_TXBYCOM_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (PROXY_TXBYCOM_STOP_SEC_CALIB_UNSPECIFIED)
  #undef PROXY_TXBYCOM_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_PROXY_TXBYCOM_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "PROXY_TXBYCOM_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_PROXY_TXBYCOM_SEC_STARTED
  #undef CHK_PROXY_TXBYCOM_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR


#endif


/*------------------[MemMap error checking]----------------------------------*/

#if (defined MEMMAP_ERROR) /* to prevent double definition */
#error MEMMAP_ERROR defined, wrong MemMap.h usage
#endif /* if (!defined MEMMAP_ERROR) */
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PROXY_MEMMAP_H_ */
/** @} */
