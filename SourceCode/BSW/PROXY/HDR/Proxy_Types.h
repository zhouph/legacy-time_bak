/**
 * @defgroup Proxy_Types Proxy_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Proxy_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PROXY_TYPES_H_
#define PROXY_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Rte_Types.h"
#include "RteDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"
#include "Mando_std_Types.h"
/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef struct
{
/* Input Data Element */
    Proxy_RxByComRxBms1Info_t Proxy_RxRxBms1Info;
    Proxy_RxByComRxYawSerialInfo_t Proxy_RxRxYawSerialInfo;
    Proxy_RxByComRxYawAccInfo_t Proxy_RxRxYawAccInfo;
    Proxy_RxByComRxTcu6Info_t Proxy_RxRxTcu6Info;
    Proxy_RxByComRxTcu5Info_t Proxy_RxRxTcu5Info;
    Proxy_RxByComRxTcu1Info_t Proxy_RxRxTcu1Info;
    Proxy_RxByComRxSasInfo_t Proxy_RxRxSasInfo;
    Proxy_RxByComRxMcu2Info_t Proxy_RxRxMcu2Info;
    Proxy_RxByComRxMcu1Info_t Proxy_RxRxMcu1Info;
    Proxy_RxByComRxLongAccInfo_t Proxy_RxRxLongAccInfo;
    Proxy_RxByComRxHcu5Info_t Proxy_RxRxHcu5Info;
    Proxy_RxByComRxHcu3Info_t Proxy_RxRxHcu3Info;
    Proxy_RxByComRxHcu2Info_t Proxy_RxRxHcu2Info;
    Proxy_RxByComRxHcu1Info_t Proxy_RxRxHcu1Info;
    Proxy_RxByComRxFact1Info_t Proxy_RxRxFact1Info;
    Proxy_RxByComRxEms3Info_t Proxy_RxRxEms3Info;
    Proxy_RxByComRxEms2Info_t Proxy_RxRxEms2Info;
    Proxy_RxByComRxEms1Info_t Proxy_RxRxEms1Info;
    Proxy_RxByComRxClu2Info_t Proxy_RxRxClu2Info;
    Proxy_RxByComRxClu1Info_t Proxy_RxRxClu1Info;
    Proxy_RxByComRxMsgOkFlgInfo_t Proxy_RxRxMsgOkFlgInfo;
    Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo_t Proxy_RxST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo;
    Proxy_RxByComAVL_LTRQD_BAXInfo_t Proxy_RxAVL_LTRQD_BAXInfo;
    Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo_t Proxy_RxTORQ_CRSH_1_ANG_ACPDInfo;
    Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info_t Proxy_RxWMOM_DRV_5_WMOM_DRV_4Info;
    Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info_t Proxy_RxWMOM_DRV_1_WMOM_DRV_2Info;
    Proxy_RxByComHGLV_VEH_FILTInfo_t Proxy_RxHGLV_VEH_FILTInfo;
    Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo_t Proxy_RxVEH_DYNMC_DT_ESTI_VRFDInfo;
    Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo_t Proxy_RxACLNX_MASSCNTR_ACLNY_MASSCNTRInfo;
    Proxy_RxByComV_VEH_V_VEH_2Info_t Proxy_RxV_VEH_V_VEH_2Info;
    Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo_t Proxy_RxDT_DRDYSEN_EXT_VYAW_VEHInfo;
    Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo_t Proxy_RxTLT_RW_STEA_FTAX_EFFVInfo;
    Proxy_RxByComAVL_STEA_FTAXInfo_t Proxy_RxAVL_STEA_FTAXInfo;
    Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_t Proxy_RxSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo;
    Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info_t Proxy_RxWMOM_DRV_3_WMOM_DRV_6Info;
    Proxy_RxByComWMOM_DRV_7Info_t Proxy_RxWMOM_DRV_7Info;
    Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo_t Proxy_RxTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo;
    Proxy_RxByComDT_BRKSYS_ENGMGInfo_t Proxy_RxDT_BRKSYS_ENGMGInfo;
    Proxy_RxByComERRM_BN_UInfo_t Proxy_RxERRM_BN_UInfo;
    Proxy_RxByComCTR_CRInfo_t Proxy_RxCTR_CRInfo;
    Proxy_RxByComKLEMMENInfo_t Proxy_RxKLEMMENInfo;
    Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_t Proxy_RxRDC_DT_PCKG_1_RDC_DT_PCKG_2Info;
    Proxy_RxByComBEDIENUNG_WISCHERInfo_t Proxy_RxBEDIENUNG_WISCHERInfo;
    Proxy_RxByComDT_PT_2Info_t Proxy_RxDT_PT_2Info;
    Proxy_RxByComSTAT_ENG_STA_AUTOInfo_t Proxy_RxSTAT_ENG_STA_AUTOInfo;
    Proxy_RxByComST_ENERG_GENInfo_t Proxy_RxST_ENERG_GENInfo;
    Proxy_RxByComDT_PT_1Info_t Proxy_RxDT_PT_1Info;
    Proxy_RxByComDT_GRDT_DRVInfo_t Proxy_RxDT_GRDT_DRVInfo;
    Proxy_RxByComDIAG_OBD_ENGMG_ELInfo_t Proxy_RxDIAG_OBD_ENGMG_ELInfo;
    Proxy_RxByComSTAT_CT_HABRInfo_t Proxy_RxSTAT_CT_HABRInfo;
    Proxy_RxByComDT_PT_3Info_t Proxy_RxDT_PT_3Info;
    Proxy_RxByComEINHEITEN_BN2020Info_t Proxy_RxEINHEITEN_BN2020Info;
    Proxy_RxByComA_TEMPInfo_t Proxy_RxA_TEMPInfo;
    Proxy_RxByComWISCHERGESCHWINDIGKEITInfo_t Proxy_RxWISCHERGESCHWINDIGKEITInfo;
    Proxy_RxByComSTAT_DS_VRFDInfo_t Proxy_RxSTAT_DS_VRFDInfo;
    Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info_t Proxy_RxSU_SW_DRDY_SU_SW_DRDY_2Info;
    Proxy_RxByComSTAT_ANHAENGERInfo_t Proxy_RxSTAT_ANHAENGERInfo;
    Proxy_RxByComFZZSTDInfo_t Proxy_RxFZZSTDInfo;
    Proxy_RxByComBEDIENUNG_FAHRWERKInfo_t Proxy_RxBEDIENUNG_FAHRWERKInfo;
    Proxy_RxByComFAHRZEUGTYPInfo_t Proxy_RxFAHRZEUGTYPInfo;
    Proxy_RxByComST_BLT_CT_SOCCUInfo_t Proxy_RxST_BLT_CT_SOCCUInfo;
    Proxy_RxByComFAHRGESTELLNUMMERInfo_t Proxy_RxFAHRGESTELLNUMMERInfo;
    Proxy_RxByComRELATIVZEITInfo_t Proxy_RxRELATIVZEITInfo;
    Proxy_RxByComKILOMETERSTANDInfo_t Proxy_RxKILOMETERSTANDInfo;
    Proxy_RxByComUHRZEIT_DATUMInfo_t Proxy_RxUHRZEIT_DATUMInfo;
    Mom_HndlrEcuModeSts_t Proxy_RxEcuModeSts;
    Pct_5msCtrlPCtrlAct_t Proxy_RxPCtrlAct;
    Eem_SuspcDetnFuncInhibitProxySts_t Proxy_RxFuncInhibitProxySts;

/* Output Data Element */
    Proxy_RxCanRxRegenInfo_t Proxy_RxCanRxRegenInfo;
    Proxy_RxCanRxAccelPedlInfo_t Proxy_RxCanRxAccelPedlInfo;
    Proxy_RxCanRxIdbInfo_t Proxy_RxCanRxIdbInfo;
    Proxy_RxCanRxEngTempInfo_t Proxy_RxCanRxEngTempInfo;
    Proxy_RxCanRxEscInfo_t Proxy_RxCanRxEscInfo;
    Proxy_RxCanRxClu2Info_t Proxy_RxCanRxClu2Info;
    Proxy_RxCanRxEemInfo_t Proxy_RxCanRxEemInfo;
    Proxy_RxCanRxInfo_t Proxy_RxCanRxInfo;
    Proxy_RxIMUCalc_t Proxy_RxIMUCalcInfo;
    Proxy_RxCanRxGearSelDispErrInfo_t Proxy_RxCanRxGearSelDispErrInfo;
}Proxy_Rx_HdrBusType;

typedef struct
{
/* Input Data Element */

/* Output Data Element */
    Proxy_RxByComRxBms1Info_t Proxy_RxByComRxBms1Info;
    Proxy_RxByComRxYawSerialInfo_t Proxy_RxByComRxYawSerialInfo;
    Proxy_RxByComRxYawAccInfo_t Proxy_RxByComRxYawAccInfo;
    Proxy_RxByComRxTcu6Info_t Proxy_RxByComRxTcu6Info;
    Proxy_RxByComRxTcu5Info_t Proxy_RxByComRxTcu5Info;
    Proxy_RxByComRxTcu1Info_t Proxy_RxByComRxTcu1Info;
    Proxy_RxByComRxSasInfo_t Proxy_RxByComRxSasInfo;
    Proxy_RxByComRxMcu2Info_t Proxy_RxByComRxMcu2Info;
    Proxy_RxByComRxMcu1Info_t Proxy_RxByComRxMcu1Info;
    Proxy_RxByComRxLongAccInfo_t Proxy_RxByComRxLongAccInfo;
    Proxy_RxByComRxHcu5Info_t Proxy_RxByComRxHcu5Info;
    Proxy_RxByComRxHcu3Info_t Proxy_RxByComRxHcu3Info;
    Proxy_RxByComRxHcu2Info_t Proxy_RxByComRxHcu2Info;
    Proxy_RxByComRxHcu1Info_t Proxy_RxByComRxHcu1Info;
    Proxy_RxByComRxFact1Info_t Proxy_RxByComRxFact1Info;
    Proxy_RxByComRxEms3Info_t Proxy_RxByComRxEms3Info;
    Proxy_RxByComRxEms2Info_t Proxy_RxByComRxEms2Info;
    Proxy_RxByComRxEms1Info_t Proxy_RxByComRxEms1Info;
    Proxy_RxByComRxClu2Info_t Proxy_RxByComRxClu2Info;
    Proxy_RxByComRxClu1Info_t Proxy_RxByComRxClu1Info;
    Proxy_RxByComRxMsgOkFlgInfo_t Proxy_RxByComRxMsgOkFlgInfo;
    Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo_t Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo;
    Proxy_RxByComAVL_LTRQD_BAXInfo_t Proxy_RxByComAVL_LTRQD_BAXInfo;
    Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo_t Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo;
    Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info_t Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info;
    Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info_t Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info;
    Proxy_RxByComHGLV_VEH_FILTInfo_t Proxy_RxByComHGLV_VEH_FILTInfo;
    Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo_t Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo;
    Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo_t Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo;
    Proxy_RxByComV_VEH_V_VEH_2Info_t Proxy_RxByComV_VEH_V_VEH_2Info;
    Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo_t Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo;
    Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo_t Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo;
    Proxy_RxByComAVL_STEA_FTAXInfo_t Proxy_RxByComAVL_STEA_FTAXInfo;
    Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_t Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo;
    Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info_t Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info;
    Proxy_RxByComWMOM_DRV_7Info_t Proxy_RxByComWMOM_DRV_7Info;
    Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo_t Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo;
    Proxy_RxByComDT_BRKSYS_ENGMGInfo_t Proxy_RxByComDT_BRKSYS_ENGMGInfo;
    Proxy_RxByComERRM_BN_UInfo_t Proxy_RxByComERRM_BN_UInfo;
    Proxy_RxByComCTR_CRInfo_t Proxy_RxByComCTR_CRInfo;
    Proxy_RxByComKLEMMENInfo_t Proxy_RxByComKLEMMENInfo;
    Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_t Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info;
    Proxy_RxByComBEDIENUNG_WISCHERInfo_t Proxy_RxByComBEDIENUNG_WISCHERInfo;
    Proxy_RxByComDT_PT_2Info_t Proxy_RxByComDT_PT_2Info;
    Proxy_RxByComSTAT_ENG_STA_AUTOInfo_t Proxy_RxByComSTAT_ENG_STA_AUTOInfo;
    Proxy_RxByComST_ENERG_GENInfo_t Proxy_RxByComST_ENERG_GENInfo;
    Proxy_RxByComDT_PT_1Info_t Proxy_RxByComDT_PT_1Info;
    Proxy_RxByComDT_GRDT_DRVInfo_t Proxy_RxByComDT_GRDT_DRVInfo;
    Proxy_RxByComDIAG_OBD_ENGMG_ELInfo_t Proxy_RxByComDIAG_OBD_ENGMG_ELInfo;
    Proxy_RxByComSTAT_CT_HABRInfo_t Proxy_RxByComSTAT_CT_HABRInfo;
    Proxy_RxByComDT_PT_3Info_t Proxy_RxByComDT_PT_3Info;
    Proxy_RxByComEINHEITEN_BN2020Info_t Proxy_RxByComEINHEITEN_BN2020Info;
    Proxy_RxByComA_TEMPInfo_t Proxy_RxByComA_TEMPInfo;
    Proxy_RxByComWISCHERGESCHWINDIGKEITInfo_t Proxy_RxByComWISCHERGESCHWINDIGKEITInfo;
    Proxy_RxByComSTAT_DS_VRFDInfo_t Proxy_RxByComSTAT_DS_VRFDInfo;
    Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info_t Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info;
    Proxy_RxByComSTAT_ANHAENGERInfo_t Proxy_RxByComSTAT_ANHAENGERInfo;
    Proxy_RxByComFZZSTDInfo_t Proxy_RxByComFZZSTDInfo;
    Proxy_RxByComBEDIENUNG_FAHRWERKInfo_t Proxy_RxByComBEDIENUNG_FAHRWERKInfo;
    Proxy_RxByComFAHRZEUGTYPInfo_t Proxy_RxByComFAHRZEUGTYPInfo;
    Proxy_RxByComST_BLT_CT_SOCCUInfo_t Proxy_RxByComST_BLT_CT_SOCCUInfo;
    Proxy_RxByComFAHRGESTELLNUMMERInfo_t Proxy_RxByComFAHRGESTELLNUMMERInfo;
    Proxy_RxByComRELATIVZEITInfo_t Proxy_RxByComRELATIVZEITInfo;
    Proxy_RxByComKILOMETERSTANDInfo_t Proxy_RxByComKILOMETERSTANDInfo;
    Proxy_RxByComUHRZEIT_DATUMInfo_t Proxy_RxByComUHRZEIT_DATUMInfo;
}Proxy_RxByCom_HdrBusType;

typedef struct
{
/* Input Data Element */
    Eem_MainEemCtrlInhibitData_t Proxy_TxEemCtrlInhibitData;
    Abc_CtrlCanTxInfo_t Proxy_TxCanTxInfo;
    Pedal_SenSyncPdt5msRawInfo_t Proxy_TxPdt5msRawInfo;
    Pedal_SenSyncPdf5msRawInfo_t Proxy_TxPdf5msRawInfo;
    Rbc_CtrlTarRgnBrkTqInfo_t Proxy_TxTarRgnBrkTqInfo;
    Swt_SenEscSwtStInfo_t Proxy_TxEscSwtStInfo;
    Wss_SenWhlSpdInfo_t Proxy_TxWhlSpdInfo;
    Wss_SenWhlEdgeCntInfo_t Proxy_TxWhlEdgeCntInfo;
    Press_SenSyncPistP5msRawInfo_t Proxy_TxPistP5msRawInfo;
    Nvm_HndlrLogicEepDataInfo_t Proxy_TxLogicEepDataInfo;
    Eem_MainEemLampData_t Proxy_TxEemLampData;
    Mom_HndlrEcuModeSts_t Proxy_TxEcuModeSts;
    Prly_HndlrIgnOnOffSts_t Proxy_TxIgnOnOffSts;
    Diag_HndlrDiagSci_t Proxy_TxDiagSci;
    Diag_HndlrDiagAhbSci_t Proxy_TxDiagAhbSci;
    Eem_SuspcDetnFuncInhibitProxySts_t Proxy_TxFuncInhibitProxySts;

/* Output Data Element */
    Proxy_TxTxESCSensorInfo_t Proxy_TxTxESCSensorInfo;
    Proxy_TxTxYawCbitInfo_t Proxy_TxTxYawCbitInfo;
    Proxy_TxTxWhlSpdInfo_t Proxy_TxTxWhlSpdInfo;
    Proxy_TxTxWhlPulInfo_t Proxy_TxTxWhlPulInfo;
    Proxy_TxTxTcs5Info_t Proxy_TxTxTcs5Info;
    Proxy_TxTxTcs1Info_t Proxy_TxTxTcs1Info;
    Proxy_TxTxSasCalInfo_t Proxy_TxTxSasCalInfo;
    Proxy_TxTxEsp2Info_t Proxy_TxTxEsp2Info;
    Proxy_TxTxEbs1Info_t Proxy_TxTxEbs1Info;
    Proxy_TxTxAhb1Info_t Proxy_TxTxAhb1Info;
    Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo_t Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo;
    Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo_t Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo;
    Proxy_TxAVL_BRTORQ_WHLInfo_t Proxy_TxAVL_BRTORQ_WHLInfo;
    Proxy_TxAVL_RPM_WHLInfo_t Proxy_TxAVL_RPM_WHLInfo;
    Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo_t Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo;
    Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info_t Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info;
    Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo_t Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo;
    Proxy_TxAVL_QUAN_EES_WHLInfo_t Proxy_TxAVL_QUAN_EES_WHLInfo;
    Proxy_TxTAR_LTRQD_BAXInfo_t Proxy_TxTAR_LTRQD_BAXInfo;
    Proxy_TxST_YMRInfo_t Proxy_TxST_YMRInfo;
    Proxy_TxWEGSTRECKEInfo_t Proxy_TxWEGSTRECKEInfo;
    Proxy_TxDISP_CC_DRDY_00Info_t Proxy_TxDISP_CC_DRDY_00Info;
    Proxy_TxDISP_CC_BYPA_00Info_t Proxy_TxDISP_CC_BYPA_00Info;
    Proxy_TxST_VHSSInfo_t Proxy_TxST_VHSSInfo;
    Proxy_TxDIAG_OBD_HYB_1Info_t Proxy_TxDIAG_OBD_HYB_1Info;
    Proxy_TxSU_DSCInfo_t Proxy_TxSU_DSCInfo;
    Proxy_TxST_TYR_RDCInfo_t Proxy_TxST_TYR_RDCInfo;
    Proxy_TxST_TYR_RPAInfo_t Proxy_TxST_TYR_RPAInfo;
    Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info_t Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info;
    Proxy_TxST_TYR_2Info_t Proxy_TxST_TYR_2Info;
    Proxy_TxAVL_T_TYRInfo_t Proxy_TxAVL_T_TYRInfo;
    Proxy_TxTEMP_BRKInfo_t Proxy_TxTEMP_BRKInfo;
    Proxy_TxAVL_P_TYRInfo_t Proxy_TxAVL_P_TYRInfo;
    Proxy_TxFR_DBG_DSCInfo_t Proxy_TxFR_DBG_DSCInfo;
}Proxy_Tx_HdrBusType;

typedef struct
{
/* Input Data Element */
    Proxy_TxTxYawCbitInfo_t Proxy_TxByComTxYawCbitInfo;
    Proxy_TxTxWhlSpdInfo_t Proxy_TxByComTxWhlSpdInfo;
    Proxy_TxTxWhlPulInfo_t Proxy_TxByComTxWhlPulInfo;
    Proxy_TxTxTcs5Info_t Proxy_TxByComTxTcs5Info;
    Proxy_TxTxTcs1Info_t Proxy_TxByComTxTcs1Info;
    Proxy_TxTxSasCalInfo_t Proxy_TxByComTxSasCalInfo;
    Proxy_TxTxEsp2Info_t Proxy_TxByComTxEsp2Info;
    Proxy_TxTxEbs1Info_t Proxy_TxByComTxEbs1Info;
    Proxy_TxTxAhb1Info_t Proxy_TxByComTxAhb1Info;
    SenPwrM_MainSenPwrMonitor_t Proxy_TxByComSenPwrMonitorData;
    Proxy_TxRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo_t Proxy_TxByComRQ_WMOM_PT_SUM_STAB_TAR_REPAT_XTRQ_FTAX_BAXInfo;
    Proxy_TxAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo_t Proxy_TxByComAVL_BRTORQ_SUM_ST_PRMSN_IBRKInfo;
    Proxy_TxAVL_BRTORQ_WHLInfo_t Proxy_TxByComAVL_BRTORQ_WHLInfo;
    Proxy_TxAVL_RPM_WHLInfo_t Proxy_TxByComAVL_RPM_WHLInfo;
    Proxy_TxRQ_WMOM_PT_SUM_RECUPInfo_t Proxy_TxByComRQ_WMOM_PT_SUM_RECUPInfo;
    Proxy_TxST_STAB_DSC_ST_STAB_DSC_2Info_t Proxy_TxByComST_STAB_DSC_ST_STAB_DSC_2Info;
    Proxy_TxQU_SER_ECBA_BRTORQ_SUM_COOTDInfo_t Proxy_TxByComQU_SER_ECBA_BRTORQ_SUM_COOTDInfo;
    Proxy_TxAVL_QUAN_EES_WHLInfo_t Proxy_TxByComAVL_QUAN_EES_WHLInfo;
    Proxy_TxTAR_LTRQD_BAXInfo_t Proxy_TxByComTAR_LTRQD_BAXInfo;
    Proxy_TxST_YMRInfo_t Proxy_TxByComST_YMRInfo;
    Proxy_TxWEGSTRECKEInfo_t Proxy_TxByComWEGSTRECKEInfo;
    Proxy_TxDISP_CC_DRDY_00Info_t Proxy_TxByComDISP_CC_DRDY_00Info;
    Proxy_TxDISP_CC_BYPA_00Info_t Proxy_TxByComDISP_CC_BYPA_00Info;
    Proxy_TxST_VHSSInfo_t Proxy_TxByComST_VHSSInfo;
    Proxy_TxDIAG_OBD_HYB_1Info_t Proxy_TxByComDIAG_OBD_HYB_1Info;
    Proxy_TxSU_DSCInfo_t Proxy_TxByComSU_DSCInfo;
    Proxy_TxST_TYR_RDCInfo_t Proxy_TxByComST_TYR_RDCInfo;
    Proxy_TxST_TYR_RPAInfo_t Proxy_TxByComST_TYR_RPAInfo;
    Proxy_TxRDC_TYR_ID1_RDC_TYR_ID2Info_t Proxy_TxByComRDC_TYR_ID1_RDC_TYR_ID2Info;
    Proxy_TxST_TYR_2Info_t Proxy_TxByComST_TYR_2Info;
    Proxy_TxAVL_T_TYRInfo_t Proxy_TxByComAVL_T_TYRInfo;
    Proxy_TxTEMP_BRKInfo_t Proxy_TxByComTEMP_BRKInfo;
    Proxy_TxAVL_P_TYRInfo_t Proxy_TxByComAVL_P_TYRInfo;
    Proxy_TxFR_DBG_DSCInfo_t Proxy_TxByComFR_DBG_DSCInfo;

/* Output Data Element */
}Proxy_TxByCom_HdrBusType;

typedef uint8 Proxy_MsgIdType;

typedef struct
{
    unsigned long    u16RxMissingCnt :16;
    unsigned long    u1RxMissingFlg  :1;
}CC_RxChkVar_t;

typedef struct
{
    unsigned long    u16RxErrCnt     :16;
    unsigned long    u16RxOkCnt      :16;
    unsigned long    u2NetChannel    :2;
    unsigned long    u1RxValidFlg    :1;
    unsigned long    u1RxOkFlg       :1;
    unsigned long    u1RxErrFlg      :1;
    unsigned long    ui1TimeoutSusp  :1;
    unsigned long    u1RxCopyOkFlg   :1;
}CC_TimeoutVar_t;

typedef struct
{
    unsigned long u1CAN_overrun      :1;
    unsigned long u1CAN_bus_off      :1;
    unsigned long u1TcuChk           :1;
    unsigned long u1can_tx_disable   :1;
    unsigned long u1can_tx_disable_sub :1;
    unsigned long u1AtmChk           :1;
    unsigned long VariantErrSet      :1;
    unsigned long u1TcuInfoRxOKflg   :1;
    unsigned long u1SysCodingOk      :1;
    unsigned long u1ChkTimeFix       :1;

}AC_CHK_FLG_t;

typedef struct
{
    unsigned long u1IgnOnTimeOk      :1;
    unsigned long u1Fwd1RxCopy       :1;
    unsigned long u1MainCheckFCanMsgFlg  :1;
    unsigned long u1SubCheckFCanMsgFlg   :1;
    unsigned long u1InhibitToChk     :1;
    unsigned long u1DecideCheckSize  :1;
    unsigned long u1DisNormComm      :1;
    unsigned long u1CanChkMinSpeed   :1;
    unsigned long u1CanChkInhbt      :1;

}AC_CHK2_FLG_t;

typedef struct
{
     uint16         ChkCanID;
     uint8          iTimeoutPeriod;
     uint8          cTimeoutPeriod;
}AC_ChkTimeOut_t;

typedef struct
{
    unsigned long    u16ChkPeriod    :16;
    unsigned long    u1RxCopyFlg     :1;
    unsigned long    u1RxFaultFlg    :1;
}Proxy_TimeoutSusp_t;

typedef struct
{
    unsigned long EMS11_RxMissing_Flg     :1;
    unsigned long EMS12_RxMissing_Flg     :1;
    unsigned long TCU12_RxMissing_Flg     :1;
    unsigned long MDPS12_RxMissing_Flg    :1;
    unsigned long SCC12_RxMissing_Flg     :1;
    unsigned long CLU13_RxMissing_Flg     :1;
    unsigned long YAW_RxMissing_Flg       :1;
    unsigned long AY_RxMissing_Flg        :1;
    unsigned long AX_RxMissing_Flg        :1;
}Proxy_RX_MISSING_FLG_t;

extern Proxy_RX_MISSING_FLG_t cc_RX_MISSING_t;

#define cu1_EMS11_RxMissing_Flg    cc_RX_MISSING_t.EMS11_RxMissing_Flg
#define cu1_EMS12_RxMissing_Flg    cc_RX_MISSING_t.EMS12_RxMissing_Flg
#define cu1_TCU12_RxMissing_Flg    cc_RX_MISSING_t.TCU12_RxMissing_Flg
#define cu1_MDPS12_RxMissing_Flg   cc_RX_MISSING_t.MDPS12_RxMissing_Flg
#define cu1_SCC12_RxMissing_Flg    cc_RX_MISSING_t.SCC12_RxMissing_Flg
#define cu1_CLU13_RxMissing_Flg    cc_RX_MISSING_t.CLU13_RxMissing_Flg
#define cu1_YAW_RxMissing_Flg      cc_RX_MISSING_t.YAW_RxMissing_Flg
#define cu1_AY_RxMissing_Flg       cc_RX_MISSING_t.AY_RxMissing_Flg
#define cu1_AX_RxMissing_Flg       cc_RX_MISSING_t.AX_RxMissing_Flg

typedef struct
{
    unsigned long    u1RefSigErrDet  :1;
    unsigned long    u1ErrorFlg      :1;
    unsigned long    u1OkFlg         :1;
    unsigned long    u1SigErrSuspFlg :1;
    unsigned long    u2NetChannel    :2;
    unsigned long    u16ErrorCnt     :16;
    unsigned long    u8OkCnt         :8;
}CC_SignalChk_t;

 /* Proxy direction type */
 typedef uint8 Proxy_ConfigVarType;

 /* Proxy config type */
 typedef struct
 {
    Proxy_ConfigVarType AxDirection;
    Proxy_ConfigVarType AyDirection;
    Proxy_ConfigVarType AxAySwapEnable;

 }Proxy_ConfigType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PROXY_TYPES_H_ */
/** @} */
