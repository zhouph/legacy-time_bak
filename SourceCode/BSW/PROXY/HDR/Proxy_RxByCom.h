/**
 * @defgroup Proxy_RxByCom Proxy_RxByCom
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Proxy_RxByCom.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef PROXY_RXBYCOM_H_
#define PROXY_RXBYCOM_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Proxy_Types.h"
#include "Proxy_Cfg.h"
#include "Proxy_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define PROXY_RXBYCOM_MODULE_ID      (0)
 #define PROXY_RXBYCOM_MAJOR_VERSION  (2)
 #define PROXY_RXBYCOM_MINOR_VERSION  (0)
 #define PROXY_RXBYCOM_PATCH_VERSION  (0)
 #define PROXY_RXBYCOM_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Proxy_RxByCom_HdrBusType Proxy_RxByComBus;

/* Version Info */
extern const SwcVersionInfo_t Proxy_RxByComVersionInfo;

/* Input Data Element */

/* Output Data Element */
extern Proxy_RxByComRxBms1Info_t Proxy_RxByComRxBms1Info;
extern Proxy_RxByComRxYawSerialInfo_t Proxy_RxByComRxYawSerialInfo;
extern Proxy_RxByComRxYawAccInfo_t Proxy_RxByComRxYawAccInfo;
extern Proxy_RxByComRxTcu6Info_t Proxy_RxByComRxTcu6Info;
extern Proxy_RxByComRxTcu5Info_t Proxy_RxByComRxTcu5Info;
extern Proxy_RxByComRxTcu1Info_t Proxy_RxByComRxTcu1Info;
extern Proxy_RxByComRxSasInfo_t Proxy_RxByComRxSasInfo;
extern Proxy_RxByComRxMcu2Info_t Proxy_RxByComRxMcu2Info;
extern Proxy_RxByComRxMcu1Info_t Proxy_RxByComRxMcu1Info;
extern Proxy_RxByComRxLongAccInfo_t Proxy_RxByComRxLongAccInfo;
extern Proxy_RxByComRxHcu5Info_t Proxy_RxByComRxHcu5Info;
extern Proxy_RxByComRxHcu3Info_t Proxy_RxByComRxHcu3Info;
extern Proxy_RxByComRxHcu2Info_t Proxy_RxByComRxHcu2Info;
extern Proxy_RxByComRxHcu1Info_t Proxy_RxByComRxHcu1Info;
extern Proxy_RxByComRxFact1Info_t Proxy_RxByComRxFact1Info;
extern Proxy_RxByComRxEms3Info_t Proxy_RxByComRxEms3Info;
extern Proxy_RxByComRxEms2Info_t Proxy_RxByComRxEms2Info;
extern Proxy_RxByComRxEms1Info_t Proxy_RxByComRxEms1Info;
extern Proxy_RxByComRxClu2Info_t Proxy_RxByComRxClu2Info;
extern Proxy_RxByComRxClu1Info_t Proxy_RxByComRxClu1Info;
extern Proxy_RxByComRxMsgOkFlgInfo_t Proxy_RxByComRxMsgOkFlgInfo;
extern Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo_t Proxy_RxByComST_REPAT_XTRQ_FTAX_BAX_XDRV_ACT_INFOInfo;
extern Proxy_RxByComAVL_LTRQD_BAXInfo_t Proxy_RxByComAVL_LTRQD_BAXInfo;
extern Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo_t Proxy_RxByComTORQ_CRSH_1_ANG_ACPDInfo;
extern Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info_t Proxy_RxByComWMOM_DRV_5_WMOM_DRV_4Info;
extern Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info_t Proxy_RxByComWMOM_DRV_1_WMOM_DRV_2Info;
extern Proxy_RxByComHGLV_VEH_FILTInfo_t Proxy_RxByComHGLV_VEH_FILTInfo;
extern Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo_t Proxy_RxByComVEH_DYNMC_DT_ESTI_VRFDInfo;
extern Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo_t Proxy_RxByComACLNX_MASSCNTR_ACLNY_MASSCNTRInfo;
extern Proxy_RxByComV_VEH_V_VEH_2Info_t Proxy_RxByComV_VEH_V_VEH_2Info;
extern Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo_t Proxy_RxByComDT_DRDYSEN_EXT_VYAW_VEHInfo;
extern Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo_t Proxy_RxByComTLT_RW_STEA_FTAX_EFFVInfo;
extern Proxy_RxByComAVL_STEA_FTAXInfo_t Proxy_RxByComAVL_STEA_FTAXInfo;
extern Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo_t Proxy_RxByComSPEC_PRMSN_IBRK_HDC_RQ_BRTORQ_SUMInfo;
extern Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info_t Proxy_RxByComWMOM_DRV_3_WMOM_DRV_6Info;
extern Proxy_RxByComWMOM_DRV_7Info_t Proxy_RxByComWMOM_DRV_7Info;
extern Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo_t Proxy_RxByComTAR_REPAT_YTRQ_BAX_ACT_RQ_DIFF_BRTORQ_YMRInfo;
extern Proxy_RxByComDT_BRKSYS_ENGMGInfo_t Proxy_RxByComDT_BRKSYS_ENGMGInfo;
extern Proxy_RxByComERRM_BN_UInfo_t Proxy_RxByComERRM_BN_UInfo;
extern Proxy_RxByComCTR_CRInfo_t Proxy_RxByComCTR_CRInfo;
extern Proxy_RxByComKLEMMENInfo_t Proxy_RxByComKLEMMENInfo;
extern Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info_t Proxy_RxByComRDC_DT_PCKG_1_RDC_DT_PCKG_2Info;
extern Proxy_RxByComBEDIENUNG_WISCHERInfo_t Proxy_RxByComBEDIENUNG_WISCHERInfo;
extern Proxy_RxByComDT_PT_2Info_t Proxy_RxByComDT_PT_2Info;
extern Proxy_RxByComSTAT_ENG_STA_AUTOInfo_t Proxy_RxByComSTAT_ENG_STA_AUTOInfo;
extern Proxy_RxByComST_ENERG_GENInfo_t Proxy_RxByComST_ENERG_GENInfo;
extern Proxy_RxByComDT_PT_1Info_t Proxy_RxByComDT_PT_1Info;
extern Proxy_RxByComDT_GRDT_DRVInfo_t Proxy_RxByComDT_GRDT_DRVInfo;
extern Proxy_RxByComDIAG_OBD_ENGMG_ELInfo_t Proxy_RxByComDIAG_OBD_ENGMG_ELInfo;
extern Proxy_RxByComSTAT_CT_HABRInfo_t Proxy_RxByComSTAT_CT_HABRInfo;
extern Proxy_RxByComDT_PT_3Info_t Proxy_RxByComDT_PT_3Info;
extern Proxy_RxByComEINHEITEN_BN2020Info_t Proxy_RxByComEINHEITEN_BN2020Info;
extern Proxy_RxByComA_TEMPInfo_t Proxy_RxByComA_TEMPInfo;
extern Proxy_RxByComWISCHERGESCHWINDIGKEITInfo_t Proxy_RxByComWISCHERGESCHWINDIGKEITInfo;
extern Proxy_RxByComSTAT_DS_VRFDInfo_t Proxy_RxByComSTAT_DS_VRFDInfo;
extern Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info_t Proxy_RxByComSU_SW_DRDY_SU_SW_DRDY_2Info;
extern Proxy_RxByComSTAT_ANHAENGERInfo_t Proxy_RxByComSTAT_ANHAENGERInfo;
extern Proxy_RxByComFZZSTDInfo_t Proxy_RxByComFZZSTDInfo;
extern Proxy_RxByComBEDIENUNG_FAHRWERKInfo_t Proxy_RxByComBEDIENUNG_FAHRWERKInfo;
extern Proxy_RxByComFAHRZEUGTYPInfo_t Proxy_RxByComFAHRZEUGTYPInfo;
extern Proxy_RxByComST_BLT_CT_SOCCUInfo_t Proxy_RxByComST_BLT_CT_SOCCUInfo;
extern Proxy_RxByComFAHRGESTELLNUMMERInfo_t Proxy_RxByComFAHRGESTELLNUMMERInfo;
extern Proxy_RxByComRELATIVZEITInfo_t Proxy_RxByComRELATIVZEITInfo;
extern Proxy_RxByComKILOMETERSTANDInfo_t Proxy_RxByComKILOMETERSTANDInfo;
extern Proxy_RxByComUHRZEIT_DATUMInfo_t Proxy_RxByComUHRZEIT_DATUMInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Proxy_RxByCom_Init(void);
extern void Proxy_RxByCom(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* PROXY_RXBYCOM_H_ */
/** @} */
