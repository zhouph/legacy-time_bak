/* all_test.c */
#include <stdio.h>
#include "unity_fixture.h"

static void RunAllTests(void)
{
    RUN_TEST_GROUP(Proxy_Rx);
    RUN_TEST_GROUP(Proxy_RxByCom);
    RUN_TEST_GROUP(Proxy_Tx);
    RUN_TEST_GROUP(Proxy_TxByCom);
}

int main(int argc, char * argv[])
{
    int retUnityMain = 0;

    retUnityMain = UnityMain(argc, argv, RunAllTests);

    return retUnityMain;
}
