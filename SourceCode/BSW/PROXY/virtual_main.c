/* Virtual main for linking */
/* purpose : Generation of map file */
#include "Proxy_Rx.h"
#include "Proxy_RxByCom.h"
#include "Proxy_Tx.h"
#include "Proxy_TxByCom.h"

int main(void)
{
    Proxy_Rx_Init();
    Proxy_RxByCom_Init();
    Proxy_Tx_Init();
    Proxy_TxByCom_Init();

    while(1)
    {
        Proxy_Rx();
        Proxy_RxByCom();
        Proxy_Tx();
        Proxy_TxByCom();
    }
}