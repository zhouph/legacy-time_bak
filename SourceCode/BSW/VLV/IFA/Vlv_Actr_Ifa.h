/**
 * @defgroup Vlv_Actr_Ifa Vlv_Actr_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Vlv_Actr_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef VLV_ACTR_IFA_H_
#define VLV_ACTR_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Vlv_Actr_Read_Vlv_ActrNormVlvReqInfo(data) do \
{ \
    *data = Vlv_ActrNormVlvReqInfo; \
}while(0);

#define Vlv_Actr_Read_Vlv_ActrVlvMonInfo(data) do \
{ \
    *data = Vlv_ActrVlvMonInfo; \
}while(0);

#define Vlv_Actr_Read_Vlv_ActrWhlVlvReqInfo(data) do \
{ \
    *data = Vlv_ActrWhlVlvReqInfo; \
}while(0);

#define Vlv_Actr_Read_Vlv_ActrBalVlvReqInfo(data) do \
{ \
    *data = Vlv_ActrBalVlvReqInfo; \
}while(0);

#define Vlv_Actr_Read_Vlv_ActrNormVlvReqInfo_PrimCutVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vlv_ActrNormVlvReqInfo.PrimCutVlvReqData[i]; \
}while(0);

#define Vlv_Actr_Read_Vlv_ActrNormVlvReqInfo_RelsVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vlv_ActrNormVlvReqInfo.RelsVlvReqData[i]; \
}while(0);

#define Vlv_Actr_Read_Vlv_ActrNormVlvReqInfo_SecdCutVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vlv_ActrNormVlvReqInfo.SecdCutVlvReqData[i]; \
}while(0);

#define Vlv_Actr_Read_Vlv_ActrNormVlvReqInfo_CircVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vlv_ActrNormVlvReqInfo.CircVlvReqData[i]; \
}while(0);

#define Vlv_Actr_Read_Vlv_ActrNormVlvReqInfo_SimVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vlv_ActrNormVlvReqInfo.SimVlvReqData[i]; \
}while(0);

#define Vlv_Actr_Read_Vlv_ActrVlvMonInfo_VlvCVMon(data) do \
{ \
    *data = Vlv_ActrVlvMonInfo.VlvCVMon; \
}while(0);

#define Vlv_Actr_Read_Vlv_ActrVlvMonInfo_VlvRLVMon(data) do \
{ \
    *data = Vlv_ActrVlvMonInfo.VlvRLVMon; \
}while(0);

#define Vlv_Actr_Read_Vlv_ActrVlvMonInfo_PrimCutVlvMon(data) do \
{ \
    *data = Vlv_ActrVlvMonInfo.PrimCutVlvMon; \
}while(0);

#define Vlv_Actr_Read_Vlv_ActrVlvMonInfo_SecdCutVlvMon(data) do \
{ \
    *data = Vlv_ActrVlvMonInfo.SecdCutVlvMon; \
}while(0);

#define Vlv_Actr_Read_Vlv_ActrVlvMonInfo_SimVlvMon(data) do \
{ \
    *data = Vlv_ActrVlvMonInfo.SimVlvMon; \
}while(0);

#define Vlv_Actr_Read_Vlv_ActrWhlVlvReqInfo_FlOvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vlv_ActrWhlVlvReqInfo.FlOvReqData[i]; \
}while(0);

#define Vlv_Actr_Read_Vlv_ActrWhlVlvReqInfo_FlIvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vlv_ActrWhlVlvReqInfo.FlIvReqData[i]; \
}while(0);

#define Vlv_Actr_Read_Vlv_ActrWhlVlvReqInfo_FrOvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vlv_ActrWhlVlvReqInfo.FrOvReqData[i]; \
}while(0);

#define Vlv_Actr_Read_Vlv_ActrWhlVlvReqInfo_FrIvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vlv_ActrWhlVlvReqInfo.FrIvReqData[i]; \
}while(0);

#define Vlv_Actr_Read_Vlv_ActrWhlVlvReqInfo_RlOvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vlv_ActrWhlVlvReqInfo.RlOvReqData[i]; \
}while(0);

#define Vlv_Actr_Read_Vlv_ActrWhlVlvReqInfo_RlIvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vlv_ActrWhlVlvReqInfo.RlIvReqData[i]; \
}while(0);

#define Vlv_Actr_Read_Vlv_ActrWhlVlvReqInfo_RrOvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vlv_ActrWhlVlvReqInfo.RrOvReqData[i]; \
}while(0);

#define Vlv_Actr_Read_Vlv_ActrWhlVlvReqInfo_RrIvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vlv_ActrWhlVlvReqInfo.RrIvReqData[i]; \
}while(0);

#define Vlv_Actr_Read_Vlv_ActrBalVlvReqInfo_PressDumpVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vlv_ActrBalVlvReqInfo.PressDumpVlvReqData[i]; \
}while(0);

#define Vlv_Actr_Read_Vlv_ActrBalVlvReqInfo_BalVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vlv_ActrBalVlvReqInfo.BalVlvReqData[i]; \
}while(0);

#define Vlv_Actr_Read_Vlv_ActrEcuModeSts(data) do \
{ \
    *data = Vlv_ActrEcuModeSts; \
}while(0);

#define Vlv_Actr_Read_Vlv_ActrAcmAsicInitCompleteFlag(data) do \
{ \
    *data = Vlv_ActrAcmAsicInitCompleteFlag; \
}while(0);

#define Vlv_Actr_Read_Vlv_ActrFuncInhibitVlvSts(data) do \
{ \
    *data = Vlv_ActrFuncInhibitVlvSts; \
}while(0);

#define Vlv_Actr_Read_Vlv_ActrVlvSync(data) do \
{ \
    *data = Vlv_ActrVlvSync; \
}while(0);


/* Set Output DE MAcro Function */
#define Vlv_Actr_Write_Vlv_ActrNormVlvDrvInfo(data) do \
{ \
    Vlv_ActrNormVlvDrvInfo = *data; \
}while(0);

#define Vlv_Actr_Write_Vlv_ActrWhlVlvDrvInfo(data) do \
{ \
    Vlv_ActrWhlVlvDrvInfo = *data; \
}while(0);

#define Vlv_Actr_Write_Vlv_ActrBalVlvDrvInfo(data) do \
{ \
    Vlv_ActrBalVlvDrvInfo = *data; \
}while(0);

#define Vlv_Actr_Write_Vlv_ActrNormVlvDrvInfo_PrimCutVlvDrvData(data) do \
{ \
    Vlv_ActrNormVlvDrvInfo.PrimCutVlvDrvData = *data; \
}while(0);

#define Vlv_Actr_Write_Vlv_ActrNormVlvDrvInfo_RelsVlvDrvData(data) do \
{ \
    Vlv_ActrNormVlvDrvInfo.RelsVlvDrvData = *data; \
}while(0);

#define Vlv_Actr_Write_Vlv_ActrNormVlvDrvInfo_SecdCutVlvDrvData(data) do \
{ \
    Vlv_ActrNormVlvDrvInfo.SecdCutVlvDrvData = *data; \
}while(0);

#define Vlv_Actr_Write_Vlv_ActrNormVlvDrvInfo_CircVlvDrvData(data) do \
{ \
    Vlv_ActrNormVlvDrvInfo.CircVlvDrvData = *data; \
}while(0);

#define Vlv_Actr_Write_Vlv_ActrNormVlvDrvInfo_SimVlvDrvData(data) do \
{ \
    Vlv_ActrNormVlvDrvInfo.SimVlvDrvData = *data; \
}while(0);

#define Vlv_Actr_Write_Vlv_ActrWhlVlvDrvInfo_FlOvDrvData(data) do \
{ \
    Vlv_ActrWhlVlvDrvInfo.FlOvDrvData = *data; \
}while(0);

#define Vlv_Actr_Write_Vlv_ActrWhlVlvDrvInfo_FlIvDrvData(data) do \
{ \
    Vlv_ActrWhlVlvDrvInfo.FlIvDrvData = *data; \
}while(0);

#define Vlv_Actr_Write_Vlv_ActrWhlVlvDrvInfo_FrOvDrvData(data) do \
{ \
    Vlv_ActrWhlVlvDrvInfo.FrOvDrvData = *data; \
}while(0);

#define Vlv_Actr_Write_Vlv_ActrWhlVlvDrvInfo_FrIvDrvData(data) do \
{ \
    Vlv_ActrWhlVlvDrvInfo.FrIvDrvData = *data; \
}while(0);

#define Vlv_Actr_Write_Vlv_ActrWhlVlvDrvInfo_RlOvDrvData(data) do \
{ \
    Vlv_ActrWhlVlvDrvInfo.RlOvDrvData = *data; \
}while(0);

#define Vlv_Actr_Write_Vlv_ActrWhlVlvDrvInfo_RlIvDrvData(data) do \
{ \
    Vlv_ActrWhlVlvDrvInfo.RlIvDrvData = *data; \
}while(0);

#define Vlv_Actr_Write_Vlv_ActrWhlVlvDrvInfo_RrOvDrvData(data) do \
{ \
    Vlv_ActrWhlVlvDrvInfo.RrOvDrvData = *data; \
}while(0);

#define Vlv_Actr_Write_Vlv_ActrWhlVlvDrvInfo_RrIvDrvData(data) do \
{ \
    Vlv_ActrWhlVlvDrvInfo.RrIvDrvData = *data; \
}while(0);

#define Vlv_Actr_Write_Vlv_ActrBalVlvDrvInfo_PressDumpVlvDrvData(data) do \
{ \
    Vlv_ActrBalVlvDrvInfo.PressDumpVlvDrvData = *data; \
}while(0);

#define Vlv_Actr_Write_Vlv_ActrBalVlvDrvInfo_BalVlvDrvData(data) do \
{ \
    Vlv_ActrBalVlvDrvInfo.BalVlvDrvData = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* VLV_ACTR_IFA_H_ */
/** @} */
