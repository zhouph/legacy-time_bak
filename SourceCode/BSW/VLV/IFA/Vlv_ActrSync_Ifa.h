/**
 * @defgroup Vlv_ActrSync_Ifa Vlv_ActrSync_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Vlv_ActrSync_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef VLV_ACTRSYNC_IFA_H_
#define VLV_ACTRSYNC_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Vlv_ActrSync_Read_Vlv_ActrSyncArbWhlVlvReqInfo(data) do \
{ \
    *data = Vlv_ActrSyncArbWhlVlvReqInfo; \
}while(0);

#define Vlv_ActrSync_Read_Vlv_ActrSyncArbNormVlvReqInfo(data) do \
{ \
    *data = Vlv_ActrSyncArbNormVlvReqInfo; \
}while(0);

#define Vlv_ActrSync_Read_Vlv_ActrSyncArbBalVlvReqInfo(data) do \
{ \
    *data = Vlv_ActrSyncArbBalVlvReqInfo; \
}while(0);

#define Vlv_ActrSync_Read_Vlv_ActrSyncArbResPVlvReqInfo(data) do \
{ \
    *data = Vlv_ActrSyncArbResPVlvReqInfo; \
}while(0);

#define Vlv_ActrSync_Read_Vlv_ActrSyncArbWhlVlvReqInfo_FlOvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vlv_ActrSyncArbWhlVlvReqInfo.FlOvReqData[i]; \
}while(0);

#define Vlv_ActrSync_Read_Vlv_ActrSyncArbWhlVlvReqInfo_FlIvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vlv_ActrSyncArbWhlVlvReqInfo.FlIvReqData[i]; \
}while(0);

#define Vlv_ActrSync_Read_Vlv_ActrSyncArbWhlVlvReqInfo_FrOvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vlv_ActrSyncArbWhlVlvReqInfo.FrOvReqData[i]; \
}while(0);

#define Vlv_ActrSync_Read_Vlv_ActrSyncArbWhlVlvReqInfo_FrIvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vlv_ActrSyncArbWhlVlvReqInfo.FrIvReqData[i]; \
}while(0);

#define Vlv_ActrSync_Read_Vlv_ActrSyncArbWhlVlvReqInfo_RlOvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vlv_ActrSyncArbWhlVlvReqInfo.RlOvReqData[i]; \
}while(0);

#define Vlv_ActrSync_Read_Vlv_ActrSyncArbWhlVlvReqInfo_RlIvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vlv_ActrSyncArbWhlVlvReqInfo.RlIvReqData[i]; \
}while(0);

#define Vlv_ActrSync_Read_Vlv_ActrSyncArbWhlVlvReqInfo_RrOvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vlv_ActrSyncArbWhlVlvReqInfo.RrOvReqData[i]; \
}while(0);

#define Vlv_ActrSync_Read_Vlv_ActrSyncArbWhlVlvReqInfo_RrIvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vlv_ActrSyncArbWhlVlvReqInfo.RrIvReqData[i]; \
}while(0);

#define Vlv_ActrSync_Read_Vlv_ActrSyncArbNormVlvReqInfo_PrimCutVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vlv_ActrSyncArbNormVlvReqInfo.PrimCutVlvReqData[i]; \
}while(0);

#define Vlv_ActrSync_Read_Vlv_ActrSyncArbNormVlvReqInfo_SecdCutVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vlv_ActrSyncArbNormVlvReqInfo.SecdCutVlvReqData[i]; \
}while(0);

#define Vlv_ActrSync_Read_Vlv_ActrSyncArbNormVlvReqInfo_SimVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vlv_ActrSyncArbNormVlvReqInfo.SimVlvReqData[i]; \
}while(0);

#define Vlv_ActrSync_Read_Vlv_ActrSyncArbNormVlvReqInfo_RelsVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vlv_ActrSyncArbNormVlvReqInfo.RelsVlvReqData[i]; \
}while(0);

#define Vlv_ActrSync_Read_Vlv_ActrSyncArbNormVlvReqInfo_CircVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vlv_ActrSyncArbNormVlvReqInfo.CircVlvReqData[i]; \
}while(0);

#define Vlv_ActrSync_Read_Vlv_ActrSyncArbNormVlvReqInfo_PressDumpVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vlv_ActrSyncArbNormVlvReqInfo.PressDumpVlvReqData[i]; \
}while(0);

#define Vlv_ActrSync_Read_Vlv_ActrSyncArbBalVlvReqInfo_BalVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vlv_ActrSyncArbBalVlvReqInfo.BalVlvReqData[i]; \
}while(0);

#define Vlv_ActrSync_Read_Vlv_ActrSyncArbResPVlvReqInfo_ResPVlvReqData(data) do \
{ \
    for(i=0;i<5;i++) *data[i] = Vlv_ActrSyncArbResPVlvReqInfo.ResPVlvReqData[i]; \
}while(0);

#define Vlv_ActrSync_Read_Vlv_ActrSyncEcuModeSts(data) do \
{ \
    *data = Vlv_ActrSyncEcuModeSts; \
}while(0);

#define Vlv_ActrSync_Read_Vlv_ActrSyncArbVlvSync(data) do \
{ \
    *data = Vlv_ActrSyncArbVlvSync; \
}while(0);

#define Vlv_ActrSync_Read_Vlv_ActrSyncAcmAsicInitCompleteFlag(data) do \
{ \
    *data = Vlv_ActrSyncAcmAsicInitCompleteFlag; \
}while(0);

#define Vlv_ActrSync_Read_Vlv_ActrSyncFuncInhibitVlvSts(data) do \
{ \
    *data = Vlv_ActrSyncFuncInhibitVlvSts; \
}while(0);


/* Set Output DE MAcro Function */
#define Vlv_ActrSync_Write_Vlv_ActrSyncNormVlvReqInfo(data) do \
{ \
    Vlv_ActrSyncNormVlvReqInfo = *data; \
}while(0);

#define Vlv_ActrSync_Write_Vlv_ActrSyncNormVlvCurrInfo(data) do \
{ \
    Vlv_ActrSyncNormVlvCurrInfo = *data; \
}while(0);

#define Vlv_ActrSync_Write_Vlv_ActrSyncWhlVlvReqInfo(data) do \
{ \
    Vlv_ActrSyncWhlVlvReqInfo = *data; \
}while(0);

#define Vlv_ActrSync_Write_Vlv_ActrSyncBalVlvReqInfo(data) do \
{ \
    Vlv_ActrSyncBalVlvReqInfo = *data; \
}while(0);

#define Vlv_ActrSync_Write_Vlv_ActrSyncNormVlvReqInfo_PrimCutVlvReqData(data) \
{ \
    for(i=0;i<5;i++) Vlv_ActrSyncNormVlvReqInfo.PrimCutVlvReqData[i] = *data[i]; \
}
#define Vlv_ActrSync_Write_Vlv_ActrSyncNormVlvReqInfo_RelsVlvReqData(data) \
{ \
    for(i=0;i<5;i++) Vlv_ActrSyncNormVlvReqInfo.RelsVlvReqData[i] = *data[i]; \
}
#define Vlv_ActrSync_Write_Vlv_ActrSyncNormVlvReqInfo_SecdCutVlvReqData(data) \
{ \
    for(i=0;i<5;i++) Vlv_ActrSyncNormVlvReqInfo.SecdCutVlvReqData[i] = *data[i]; \
}
#define Vlv_ActrSync_Write_Vlv_ActrSyncNormVlvReqInfo_CircVlvReqData(data) \
{ \
    for(i=0;i<5;i++) Vlv_ActrSyncNormVlvReqInfo.CircVlvReqData[i] = *data[i]; \
}
#define Vlv_ActrSync_Write_Vlv_ActrSyncNormVlvReqInfo_SimVlvReqData(data) \
{ \
    for(i=0;i<5;i++) Vlv_ActrSyncNormVlvReqInfo.SimVlvReqData[i] = *data[i]; \
}
#define Vlv_ActrSync_Write_Vlv_ActrSyncNormVlvCurrInfo_PrimCircVlv1msCurrMon(data) do \
{ \
    Vlv_ActrSyncNormVlvCurrInfo.PrimCircVlv1msCurrMon = *data; \
}while(0);

#define Vlv_ActrSync_Write_Vlv_ActrSyncNormVlvCurrInfo_SecdCirCVlv1msCurrMon(data) do \
{ \
    Vlv_ActrSyncNormVlvCurrInfo.SecdCirCVlv1msCurrMon = *data; \
}while(0);

#define Vlv_ActrSync_Write_Vlv_ActrSyncNormVlvCurrInfo_PrimCutVlv1msCurrMon(data) do \
{ \
    Vlv_ActrSyncNormVlvCurrInfo.PrimCutVlv1msCurrMon = *data; \
}while(0);

#define Vlv_ActrSync_Write_Vlv_ActrSyncNormVlvCurrInfo_SecdCutVlv1msCurrMon(data) do \
{ \
    Vlv_ActrSyncNormVlvCurrInfo.SecdCutVlv1msCurrMon = *data; \
}while(0);

#define Vlv_ActrSync_Write_Vlv_ActrSyncNormVlvCurrInfo_SimVlv1msCurrMon(data) do \
{ \
    Vlv_ActrSyncNormVlvCurrInfo.SimVlv1msCurrMon = *data; \
}while(0);

#define Vlv_ActrSync_Write_Vlv_ActrSyncWhlVlvReqInfo_FlOvReqData(data) \
{ \
    for(i=0;i<5;i++) Vlv_ActrSyncWhlVlvReqInfo.FlOvReqData[i] = *data[i]; \
}
#define Vlv_ActrSync_Write_Vlv_ActrSyncWhlVlvReqInfo_FlIvReqData(data) \
{ \
    for(i=0;i<5;i++) Vlv_ActrSyncWhlVlvReqInfo.FlIvReqData[i] = *data[i]; \
}
#define Vlv_ActrSync_Write_Vlv_ActrSyncWhlVlvReqInfo_FrOvReqData(data) \
{ \
    for(i=0;i<5;i++) Vlv_ActrSyncWhlVlvReqInfo.FrOvReqData[i] = *data[i]; \
}
#define Vlv_ActrSync_Write_Vlv_ActrSyncWhlVlvReqInfo_FrIvReqData(data) \
{ \
    for(i=0;i<5;i++) Vlv_ActrSyncWhlVlvReqInfo.FrIvReqData[i] = *data[i]; \
}
#define Vlv_ActrSync_Write_Vlv_ActrSyncWhlVlvReqInfo_RlOvReqData(data) \
{ \
    for(i=0;i<5;i++) Vlv_ActrSyncWhlVlvReqInfo.RlOvReqData[i] = *data[i]; \
}
#define Vlv_ActrSync_Write_Vlv_ActrSyncWhlVlvReqInfo_RlIvReqData(data) \
{ \
    for(i=0;i<5;i++) Vlv_ActrSyncWhlVlvReqInfo.RlIvReqData[i] = *data[i]; \
}
#define Vlv_ActrSync_Write_Vlv_ActrSyncWhlVlvReqInfo_RrOvReqData(data) \
{ \
    for(i=0;i<5;i++) Vlv_ActrSyncWhlVlvReqInfo.RrOvReqData[i] = *data[i]; \
}
#define Vlv_ActrSync_Write_Vlv_ActrSyncWhlVlvReqInfo_RrIvReqData(data) \
{ \
    for(i=0;i<5;i++) Vlv_ActrSyncWhlVlvReqInfo.RrIvReqData[i] = *data[i]; \
}
#define Vlv_ActrSync_Write_Vlv_ActrSyncBalVlvReqInfo_PressDumpVlvReqData(data) \
{ \
    for(i=0;i<5;i++) Vlv_ActrSyncBalVlvReqInfo.PressDumpVlvReqData[i] = *data[i]; \
}
#define Vlv_ActrSync_Write_Vlv_ActrSyncBalVlvReqInfo_BalVlvReqData(data) \
{ \
    for(i=0;i<5;i++) Vlv_ActrSyncBalVlvReqInfo.BalVlvReqData[i] = *data[i]; \
}
#define Vlv_ActrSync_Write_Vlv_ActrSyncVlvSync(data) do \
{ \
    Vlv_ActrSyncVlvSync = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* VLV_ACTRSYNC_IFA_H_ */
/** @} */
