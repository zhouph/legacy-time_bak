/**
 * @defgroup Vlv_Cfg Vlv_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Vlv_Cfg.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Vlv_Cfg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define FRAC16(x)          ((Vlv_PidFloatGainType)((x) < 0.999969482421875 ? ((x) >= -1 ? (x)*0x8000 : 0x8000) : 0x7FFF))

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define VLV_START_SEC_CONST_UNSPECIFIED
#include "Vlv_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/
/* Valve actuator config table */
 const Vlv_ActrConfigType Vlv_ActrConfig = 
 {
    /* VLV_PORT_FLNC */
    {
        (Vlv_DataType)         10000,                     /* Max input value */
        (Vlv_DataType)         0,                         /* Min input value */
        (Vlv_MulFactorType)    1,                         /* Multiply factor */
        (Vlv_DivFactorType)    1,                         /* Divide factor */
        (Valve_LlModuleType)   Valve_LlModule_Non_Pid,    /* Valve_LlModuleType */
        (Vlv_PidPortType)      0,                         /* Port index of Pid */
        (Vlv_PidIntGainType)   0,                         /* P-gain integer value */
        (Vlv_PidFloatGainType) 0,                         /* P-gain float value */
        (Vlv_PidIntGainType)   0,                         /* I-gain*Ts integer value */
        (Vlv_PidFloatGainType) 0,                         /* I-gain*Ts float value */
        (Vlv_PidIntGainType)   0,                         /* D-gain integer value */
        (Vlv_PidFloatGainType) 0,                         /* D-gain float value */
        (Vlv_PidIntGainType)   0,                         /* Back calculation integer value */
        (Vlv_PidFloatGainType) 0                          /* Back calculation float value */    
    },
    /* VLV_PORT_FRNC */
    {
        (Vlv_DataType)         10000,                     /* Max input value */
        (Vlv_DataType)         0,                         /* Min input value */
        (Vlv_MulFactorType)    1,                         /* Multiply factor */
        (Vlv_DivFactorType)    1,                         /* Divide factor */
        (Valve_LlModuleType)   Valve_LlModule_Non_Pid,    /* Valve_LlModuleType */
        (Vlv_PidPortType)      0,                         /* Port index of Pid */
        (Vlv_PidIntGainType)   0,                         /* P-gain integer value */
        (Vlv_PidFloatGainType) 0,                         /* P-gain float value */
        (Vlv_PidIntGainType)   0,                         /* I-gain*Ts integer value */
        (Vlv_PidFloatGainType) 0,                         /* I-gain*Ts float value */
        (Vlv_PidIntGainType)   0,                         /* D-gain integer value */
        (Vlv_PidFloatGainType) 0,                         /* D-gain float value */
        (Vlv_PidIntGainType)   0,                         /* Back calculation integer value */
        (Vlv_PidFloatGainType) 0                          /* Back calculation float value */    
    },
    /* VLV_PORT_RLNC */
    {

        (Vlv_DataType)         10000,                     /* Max input value */
        (Vlv_DataType)         0,                         /* Min input value */
        (Vlv_MulFactorType)    1,                         /* Multiply factor */
        (Vlv_DivFactorType)    1,                         /* Divide factor */
        (Valve_LlModuleType)   Valve_LlModule_Non_Pid,    /* Valve_LlModuleType */
        (Vlv_PidPortType)      0,                         /* Port index of Pid */
        (Vlv_PidIntGainType)   0,                         /* P-gain integer value */
        (Vlv_PidFloatGainType) 0,                         /* P-gain float value */
        (Vlv_PidIntGainType)   0,                         /* I-gain*Ts integer value */
        (Vlv_PidFloatGainType) 0,                         /* I-gain*Ts float value */
        (Vlv_PidIntGainType)   0,                         /* D-gain integer value */
        (Vlv_PidFloatGainType) 0,                         /* D-gain float value */
        (Vlv_PidIntGainType)   0,                         /* Back calculation integer value */
        (Vlv_PidFloatGainType) 0                          /* Back calculation float value */    
    },
    /* VLV_PORT_RRNC */
    {
        (Vlv_DataType)         10000,                     /* Max input value */
        (Vlv_DataType)         0,                         /* Min input value */
        (Vlv_MulFactorType)    1,                         /* Multiply factor */
        (Vlv_DivFactorType)    1,                         /* Divide factor */
        (Valve_LlModuleType)   Valve_LlModule_Non_Pid,    /* Valve_LlModuleType */
        (Vlv_PidPortType)      0,                         /* Port index of Pid */
        (Vlv_PidIntGainType)   0,                         /* P-gain integer value */
        (Vlv_PidFloatGainType) 0,                         /* P-gain float value */
        (Vlv_PidIntGainType)   0,                         /* I-gain*Ts integer value */
        (Vlv_PidFloatGainType) 0,                         /* I-gain*Ts float value */
        (Vlv_PidIntGainType)   0,                         /* D-gain integer value */
        (Vlv_PidFloatGainType) 0,                         /* D-gain float value */
        (Vlv_PidIntGainType)   0,                         /* Back calculation integer value */
        (Vlv_PidFloatGainType) 0                          /* Back calculation float value */    
    },
    /* VLV_PORT_FLNO */
    {
        (Vlv_DataType)         2250,                      /* Max input value */
        (Vlv_DataType)         0,                         /* Min input value */
        (Vlv_MulFactorType)    1,                         /* Multiply factor */
        (Vlv_DivFactorType)    1,                         /* Divide factor */
        (Valve_LlModuleType)   Valve_LlModule_Non_Pid,    /* Valve_LlModuleType */
        (Vlv_PidPortType)      0,                         /* Port index of Pid */
        (Vlv_PidIntGainType)   0,                         /* P-gain integer value */
        (Vlv_PidFloatGainType) 0,                         /* P-gain float value */
        (Vlv_PidIntGainType)   0,                         /* I-gain*Ts integer value */
        (Vlv_PidFloatGainType) 0,                         /* I-gain*Ts float value */
        (Vlv_PidIntGainType)   0,                         /* D-gain integer value */
        (Vlv_PidFloatGainType) 0,                         /* D-gain float value */
        (Vlv_PidIntGainType)   0,                         /* Back calculation integer value */
        (Vlv_PidFloatGainType) 0                          /* Back calculation float value */
    },
    /* VLV_PORT_FRNO */
    {
        (Vlv_DataType)         2250,                      /* Max input value */
        (Vlv_DataType)         0,                         /* Min input value */
        (Vlv_MulFactorType)    1,                         /* Multiply factor */
        (Vlv_DivFactorType)    1,                         /* Divide factor */
        (Valve_LlModuleType)   Valve_LlModule_Non_Pid,    /* Valve_LlModuleType */
        (Vlv_PidPortType)      0,                         /* Port index of Pid */
        (Vlv_PidIntGainType)   0,                         /* P-gain integer value */
        (Vlv_PidFloatGainType) 0,                         /* P-gain float value */
        (Vlv_PidIntGainType)   0,                         /* I-gain*Ts integer value */
        (Vlv_PidFloatGainType) 0,                         /* I-gain*Ts float value */
        (Vlv_PidIntGainType)   0,                         /* D-gain integer value */
        (Vlv_PidFloatGainType) 0,                         /* D-gain float value */
        (Vlv_PidIntGainType)   0,                         /* Back calculation integer value */
        (Vlv_PidFloatGainType) 0                          /* Back calculation float value */
    },
    /* VLV_PORT_RLNO */
    {
        (Vlv_DataType)         2250,                      /* Max input value */
        (Vlv_DataType)         0,                         /* Min input value */
        (Vlv_MulFactorType)    1,                         /* Multiply factor */
        (Vlv_DivFactorType)    1,                         /* Divide factor */
        (Valve_LlModuleType)   Valve_LlModule_Non_Pid,    /* Valve_LlModuleType */
        (Vlv_PidPortType)      0,                         /* Port index of Pid */
        (Vlv_PidIntGainType)   0,                         /* P-gain integer value */
        (Vlv_PidFloatGainType) 0,                         /* P-gain float value */
        (Vlv_PidIntGainType)   0,                         /* I-gain*Ts integer value */
        (Vlv_PidFloatGainType) 0,                         /* I-gain*Ts float value */
        (Vlv_PidIntGainType)   0,                         /* D-gain integer value */
        (Vlv_PidFloatGainType) 0,                         /* D-gain float value */
        (Vlv_PidIntGainType)   0,                         /* Back calculation integer value */
        (Vlv_PidFloatGainType) 0                          /* Back calculation float value */
    },
    /* VLV_PORT_RRNO */
    {
        (Vlv_DataType)         2250,                      /* Max input value */
        (Vlv_DataType)         0,                         /* Min input value */
        (Vlv_MulFactorType)    1,                         /* Multiply factor */
        (Vlv_DivFactorType)    1,                         /* Divide factor */
        (Valve_LlModuleType)   Valve_LlModule_Non_Pid,    /* Valve_LlModuleType */
        (Vlv_PidPortType)      0,                         /* Port index of Pid */
        (Vlv_PidIntGainType)   0,                         /* P-gain integer value */
        (Vlv_PidFloatGainType) 0,                         /* P-gain float value */
        (Vlv_PidIntGainType)   0,                         /* I-gain*Ts integer value */
        (Vlv_PidFloatGainType) 0,                         /* I-gain*Ts float value */
        (Vlv_PidIntGainType)   0,                         /* D-gain integer value */
        (Vlv_PidFloatGainType) 0,                         /* D-gain float value */
        (Vlv_PidIntGainType)   0,                         /* Back calculation integer value */
        (Vlv_PidFloatGainType) 0                          /* Back calculation float value */
    },
    /* VLV_PORT_SIM */
    {
        (Vlv_DataType)         2250,                      /* Max input value */
        (Vlv_DataType)         0,                         /* Min input value */
        (Vlv_MulFactorType)    1,                         /* Multiply factor */
        (Vlv_DivFactorType)    1,                         /* Divide factor */
        (Valve_LlModuleType)   Valve_LlModule_Pid,        /* Valve_LlModuleType */
        (Vlv_PidPortType)      VLV_PORT_SIM,              /* Port index of Pid */
        (Vlv_PidIntGainType)   0,                         /* P-gain integer value */
        (Vlv_PidFloatGainType) FRAC16(0.2),               /* P-gain float value */
        (Vlv_PidIntGainType)   0,                         /* I-gain*Ts integer value */
        (Vlv_PidFloatGainType) FRAC16(0.09),              /* I-gain*Ts float value */
        (Vlv_PidIntGainType)   0,                         /* D-gain integer value */
        (Vlv_PidFloatGainType) 0,                         /* D-gain float value */
        (Vlv_PidIntGainType)   5,                         /* Back calculation integer value */
        (Vlv_PidFloatGainType) 0                          /* Back calculation float value */
    },    
    /* VLV_PORT_CUT1 */
    {
        (Vlv_DataType)         2250,                      /* Max input value */
        (Vlv_DataType)         0,                         /* Min input value */
        (Vlv_MulFactorType)    1,                         /* Multiply factor */
        (Vlv_DivFactorType)    1,                         /* Divide factor */
        (Valve_LlModuleType)   Valve_LlModule_Pid,        /* Valve_LlModuleType */
        (Vlv_PidPortType)      VLV_PORT_CUT1,             /* Port index of Pid */
        (Vlv_PidIntGainType)   0,                         /* P-gain integer value */
        (Vlv_PidFloatGainType) FRAC16(0.2),               /* P-gain float value */
        (Vlv_PidIntGainType)   0,                         /* I-gain*Ts integer value */
        (Vlv_PidFloatGainType) FRAC16(0.16),              /* I-gain*Ts float value */
        (Vlv_PidIntGainType)   0,                         /* D-gain integer value */
        (Vlv_PidFloatGainType) 0,                         /* D-gain float value */
        (Vlv_PidIntGainType)   5,                         /* Back calculation integer value */
        (Vlv_PidFloatGainType) 0                          /* Back calculation float value */
    },
    /* VLV_PORT_CUT2 */
    {
        (Vlv_DataType)         2250,                      /* Max input value */
        (Vlv_DataType)         0,                         /* Min input value */
        (Vlv_MulFactorType)    1,                         /* Multiply factor */
        (Vlv_DivFactorType)    1,                         /* Divide factor */
        (Valve_LlModuleType)   Valve_LlModule_Pid,        /* Valve_LlModuleType */
        (Vlv_PidPortType)      VLV_PORT_CUT2,             /* Port index of Pid */
        (Vlv_PidIntGainType)   0,                         /* P-gain integer value */
        (Vlv_PidFloatGainType) FRAC16(0.2),               /* P-gain float value */
        (Vlv_PidIntGainType)   0,                         /* I-gain*Ts integer value */
        (Vlv_PidFloatGainType) FRAC16(0.16),              /* I-gain*Ts float value */
        (Vlv_PidIntGainType)   0,                         /* D-gain integer value */
        (Vlv_PidFloatGainType) 0,                         /* D-gain float value */
        (Vlv_PidIntGainType)   5,                         /* Back calculation integer value */
        (Vlv_PidFloatGainType) 0                          /* Back calculation float value */
    },
    /* VLV_PORT_RLV */
    /* ECU_B.INF VLV Coil == ECU_C.CV VLV Coil */
    {
        (Vlv_DataType)         2250,                      /* Max input value */
        (Vlv_DataType)         0,                         /* Min input value */
        (Vlv_MulFactorType)    1,                         /* Multiply factor */
        (Vlv_DivFactorType)    1,                         /* Divide factor */
        (Valve_LlModuleType)   Valve_LlModule_Pid,        /* Valve_LlModuleType */
        (Vlv_PidPortType)      VLV_PORT_RLV,              /* Port index of Pid */
        (Vlv_PidIntGainType)   0,                         /* P-gain integer value */
        (Vlv_PidFloatGainType) FRAC16(0.899),             /* P-gain float value */
        (Vlv_PidIntGainType)   0,                         /* I-gain*Ts integer value */
        (Vlv_PidFloatGainType) FRAC16(0.1),               /* I-gain*Ts float value */
        (Vlv_PidIntGainType)   0,                         /* D-gain integer value */
        (Vlv_PidFloatGainType) 0,                         /* D-gain float value */
        (Vlv_PidIntGainType)   1,                         /* Back calculation integer value */
        (Vlv_PidFloatGainType) FRAC16(0.111)              /* Back calculation float value */
    },
    /* VLV_PORT_CV */
    {
        (Vlv_DataType)         2250,                      /* Max input value */
        (Vlv_DataType)         0,                         /* Min input value */
        (Vlv_MulFactorType)    1,                         /* Multiply factor */
        (Vlv_DivFactorType)    1,                         /* Divide factor */
        (Valve_LlModuleType)   Valve_LlModule_Pid,        /* Valve_LlModuleType */
        (Vlv_PidPortType)      VLV_PORT_CV,              /* Port index of Pid */
        (Vlv_PidIntGainType)   0,                         /* P-gain integer value */
        (Vlv_PidFloatGainType) FRAC16(0.55),              /* P-gain float value */
        (Vlv_PidIntGainType)   0,                         /* I-gain*Ts integer value */
        (Vlv_PidFloatGainType) FRAC16(0.35),              /* I-gain*Ts float value */
        (Vlv_PidIntGainType)   0,                         /* D-gain integer value */
        (Vlv_PidFloatGainType) 0,                         /* D-gain float value */
        (Vlv_PidIntGainType)   1,                         /* Back calculation integer value */
        (Vlv_PidFloatGainType) FRAC16(0.818)              /* Back calculation float value */
    },
    /* VLV_PORT_PDV */
    {
        (Vlv_DataType)         2250,                      /* Max input value */
        (Vlv_DataType)         0,                         /* Min input value */
        (Vlv_MulFactorType)    1,                         /* Multiply factor */
        (Vlv_DivFactorType)    1,                         /* Divide factor */
        (Valve_LlModuleType)   Valve_LlModule_Non_Pid,    /* Valve_LlModuleType */
        (Vlv_PidPortType)      0,                         /* Port index of Pid */
        (Vlv_PidIntGainType)   0,                         /* P-gain integer value */
        (Vlv_PidFloatGainType) 0,                         /* P-gain float value */
        (Vlv_PidIntGainType)   0,                         /* I-gain*Ts integer value */
        (Vlv_PidFloatGainType) 0,                         /* I-gain*Ts float value */
        (Vlv_PidIntGainType)   0,                         /* D-gain integer value */
        (Vlv_PidFloatGainType) 0,                         /* D-gain float value */
        (Vlv_PidIntGainType)   0,                         /* Back calculation integer value */
        (Vlv_PidFloatGainType) 0                          /* Back calculation float value */
    },
    /* VLV_PORT_BALV */
    {
        (Vlv_DataType)         2250,                      /* Max input value */
        (Vlv_DataType)         0,                         /* Min input value */
        (Vlv_MulFactorType)    1,                         /* Multiply factor */
        (Vlv_DivFactorType)    1,                         /* Divide factor */
        (Valve_LlModuleType)   Valve_LlModule_Non_Pid,    /* Valve_LlModuleType */
        (Vlv_PidPortType)      0,                         /* Port index of Pid */
        (Vlv_PidIntGainType)   0,                         /* P-gain integer value */
        (Vlv_PidFloatGainType) 0,                         /* P-gain float value */
        (Vlv_PidIntGainType)   0,                         /* I-gain*Ts integer value */
        (Vlv_PidFloatGainType) 0,                         /* I-gain*Ts float value */
        (Vlv_PidIntGainType)   0,                         /* D-gain integer value */
        (Vlv_PidFloatGainType) 0,                         /* D-gain float value */
        (Vlv_PidIntGainType)   0,                         /* Back calculation integer value */
        (Vlv_PidFloatGainType) 0                          /* Back calculation float value */
    }
 };

 /* Valve sensor config table */
 const Vlv_SnsrConfigType Vlv_SnsrConfig = 
 {
    /* VLV_PORT_FLNC */
    {
        (Vlv_MulFactorType) 1,                         /* Multiply factor */
        (Vlv_DivFactorType) 1,                         /* Divide factor */
    },
    /* VLV_PORT_FRNC */
    {
        (Vlv_MulFactorType) 1,                         /* Multiply factor */
        (Vlv_DivFactorType) 1,                         /* Divide factor */
    },
    /* VLV_PORT_RLNC */
    {
        (Vlv_MulFactorType) 1,                         /* Multiply factor */
        (Vlv_DivFactorType) 1,                         /* Divide factor */
    },
    /* VLV_PORT_RRNC */
    {
        (Vlv_MulFactorType) 1,                         /* Multiply factor */
        (Vlv_DivFactorType) 1,                         /* Divide factor */
    },
    /* VLV_PORT_FLNO */
    {
        (Vlv_MulFactorType) 10,                        /* Multiply factor */
        (Vlv_DivFactorType) 11,                        /* Divide factor */
    },
    /* VLV_PORT_FRNO */
    { 
        (Vlv_MulFactorType) 10,                        /* Multiply factor */
        (Vlv_DivFactorType) 11,                        /* Divide factor */
    },
    /* VLV_PORT_RLNO */
    {
        (Vlv_MulFactorType) 10,                         /* Multiply factor */
        (Vlv_DivFactorType) 11,                         /* Divide factor */
    },
    /* VLV_PORT_RRNO */
    {
        (Vlv_MulFactorType) 10,                         /* Multiply factor */
        (Vlv_DivFactorType) 11,                         /* Divide factor */
    },
    /* VLV_PORT_SIM */
    {
        (Vlv_MulFactorType) 10,                           /* Multiply factor */
        (Vlv_DivFactorType) 11,                           /* Divide factor */
    },
    /* VLV_PORT_CUT1 */
    {   
        (Vlv_MulFactorType) 10,                           /* Multiply factor */
        (Vlv_DivFactorType) 11,                           /* Divide factor */
    },
    /* VLV_PORT_CUT2 */
    {
        (Vlv_MulFactorType) 10,                           /* Multiply factor */
        (Vlv_DivFactorType) 11,                           /* Divide factor */
    },
    /* VLV_PORT_RLV */
    {
        (Vlv_MulFactorType) 10,                               /* Multiply factor */
        (Vlv_DivFactorType) 11,                               /* Divide factor */
    },
    /* VLV_PORT_CV */
    {     
        (Vlv_MulFactorType) 10,                               /* Multiply factor */
        (Vlv_DivFactorType) 11,                               /* Divide factor */
    },
    /* VLV_PORT_PDV */
    {
        (Vlv_MulFactorType) 1,                         /* Multiply factor */
        (Vlv_DivFactorType) 1,                         /* Divide factor */
    },
    /* VLV_PORT_BALV */
    {
        (Vlv_MulFactorType) 1,                         /* Multiply factor */
        (Vlv_DivFactorType) 1,                         /* Divide factor */
    }
 };
/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define VLV_STOP_SEC_CONST_UNSPECIFIED
#include "Vlv_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define VLV_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlv_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define VLV_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlv_MemMap.h"
#define VLV_START_SEC_VAR_NOINIT_32BIT
#include "Vlv_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define VLV_STOP_SEC_VAR_NOINIT_32BIT
#include "Vlv_MemMap.h"
#define VLV_START_SEC_VAR_UNSPECIFIED
#include "Vlv_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define VLV_STOP_SEC_VAR_UNSPECIFIED
#include "Vlv_MemMap.h"
#define VLV_START_SEC_VAR_32BIT
#include "Vlv_MemMap.h"
/** Variable Section (32BIT)**/


#define VLV_STOP_SEC_VAR_32BIT
#include "Vlv_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define VLV_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlv_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define VLV_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlv_MemMap.h"
#define VLV_START_SEC_VAR_NOINIT_32BIT
#include "Vlv_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define VLV_STOP_SEC_VAR_NOINIT_32BIT
#include "Vlv_MemMap.h"
#define VLV_START_SEC_VAR_UNSPECIFIED
#include "Vlv_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define VLV_STOP_SEC_VAR_UNSPECIFIED
#include "Vlv_MemMap.h"
#define VLV_START_SEC_VAR_32BIT
#include "Vlv_MemMap.h"
/** Variable Section (32BIT)**/


#define VLV_STOP_SEC_VAR_32BIT
#include "Vlv_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define VLV_START_SEC_CODE
#include "Vlv_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define VLV_STOP_SEC_CODE
#include "Vlv_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
