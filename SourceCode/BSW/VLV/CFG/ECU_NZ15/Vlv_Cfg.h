/**
 * @defgroup Vlv_Cfg Vlv_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Vlv_Cfg.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef VLV_CFG_H_
#define VLV_CFG_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Vlv_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 /* Valve actuation data buffer max length */
 #define VLV_MAX_BUF_LEN        (Vlv_BufLengthType) 10

 #define VLV_PORT_SIM           (Vlv_PortType) 0
 #define VLV_PORT_CUT1          (Vlv_PortType) 1   
 #define VLV_PORT_CUT2          (Vlv_PortType) 2
 #define VLV_PORT_RLV           (Vlv_PortType) 3  /* NZ Quick C: RLV Valve */
 #define VLV_PORT_CV           	(Vlv_PortType) 4  /* NZ Quick C: CV Valve */

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/
 /* Valve conversion rule config */
 extern const Vlv_ActrConfigType Vlv_ActrConfig;
 extern const Vlv_SnsrConfigType Vlv_SnsrConfig;

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* VLV_CFG_H_ */
/** @} */
