#define S_FUNCTION_NAME      Vlv_ActrSync_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          84
#define WidthOutputPort         86

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Vlv_ActrSync.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Vlv_ActrSyncArbWhlVlvReqInfo.FlOvReqData[0] = input[0];
    Vlv_ActrSyncArbWhlVlvReqInfo.FlOvReqData[1] = input[1];
    Vlv_ActrSyncArbWhlVlvReqInfo.FlOvReqData[2] = input[2];
    Vlv_ActrSyncArbWhlVlvReqInfo.FlOvReqData[3] = input[3];
    Vlv_ActrSyncArbWhlVlvReqInfo.FlOvReqData[4] = input[4];
    Vlv_ActrSyncArbWhlVlvReqInfo.FlIvReqData[0] = input[5];
    Vlv_ActrSyncArbWhlVlvReqInfo.FlIvReqData[1] = input[6];
    Vlv_ActrSyncArbWhlVlvReqInfo.FlIvReqData[2] = input[7];
    Vlv_ActrSyncArbWhlVlvReqInfo.FlIvReqData[3] = input[8];
    Vlv_ActrSyncArbWhlVlvReqInfo.FlIvReqData[4] = input[9];
    Vlv_ActrSyncArbWhlVlvReqInfo.FrOvReqData[0] = input[10];
    Vlv_ActrSyncArbWhlVlvReqInfo.FrOvReqData[1] = input[11];
    Vlv_ActrSyncArbWhlVlvReqInfo.FrOvReqData[2] = input[12];
    Vlv_ActrSyncArbWhlVlvReqInfo.FrOvReqData[3] = input[13];
    Vlv_ActrSyncArbWhlVlvReqInfo.FrOvReqData[4] = input[14];
    Vlv_ActrSyncArbWhlVlvReqInfo.FrIvReqData[0] = input[15];
    Vlv_ActrSyncArbWhlVlvReqInfo.FrIvReqData[1] = input[16];
    Vlv_ActrSyncArbWhlVlvReqInfo.FrIvReqData[2] = input[17];
    Vlv_ActrSyncArbWhlVlvReqInfo.FrIvReqData[3] = input[18];
    Vlv_ActrSyncArbWhlVlvReqInfo.FrIvReqData[4] = input[19];
    Vlv_ActrSyncArbWhlVlvReqInfo.RlOvReqData[0] = input[20];
    Vlv_ActrSyncArbWhlVlvReqInfo.RlOvReqData[1] = input[21];
    Vlv_ActrSyncArbWhlVlvReqInfo.RlOvReqData[2] = input[22];
    Vlv_ActrSyncArbWhlVlvReqInfo.RlOvReqData[3] = input[23];
    Vlv_ActrSyncArbWhlVlvReqInfo.RlOvReqData[4] = input[24];
    Vlv_ActrSyncArbWhlVlvReqInfo.RlIvReqData[0] = input[25];
    Vlv_ActrSyncArbWhlVlvReqInfo.RlIvReqData[1] = input[26];
    Vlv_ActrSyncArbWhlVlvReqInfo.RlIvReqData[2] = input[27];
    Vlv_ActrSyncArbWhlVlvReqInfo.RlIvReqData[3] = input[28];
    Vlv_ActrSyncArbWhlVlvReqInfo.RlIvReqData[4] = input[29];
    Vlv_ActrSyncArbWhlVlvReqInfo.RrOvReqData[0] = input[30];
    Vlv_ActrSyncArbWhlVlvReqInfo.RrOvReqData[1] = input[31];
    Vlv_ActrSyncArbWhlVlvReqInfo.RrOvReqData[2] = input[32];
    Vlv_ActrSyncArbWhlVlvReqInfo.RrOvReqData[3] = input[33];
    Vlv_ActrSyncArbWhlVlvReqInfo.RrOvReqData[4] = input[34];
    Vlv_ActrSyncArbWhlVlvReqInfo.RrIvReqData[0] = input[35];
    Vlv_ActrSyncArbWhlVlvReqInfo.RrIvReqData[1] = input[36];
    Vlv_ActrSyncArbWhlVlvReqInfo.RrIvReqData[2] = input[37];
    Vlv_ActrSyncArbWhlVlvReqInfo.RrIvReqData[3] = input[38];
    Vlv_ActrSyncArbWhlVlvReqInfo.RrIvReqData[4] = input[39];
    Vlv_ActrSyncArbNormVlvReqInfo.PrimCutVlvReqData[0] = input[40];
    Vlv_ActrSyncArbNormVlvReqInfo.PrimCutVlvReqData[1] = input[41];
    Vlv_ActrSyncArbNormVlvReqInfo.PrimCutVlvReqData[2] = input[42];
    Vlv_ActrSyncArbNormVlvReqInfo.PrimCutVlvReqData[3] = input[43];
    Vlv_ActrSyncArbNormVlvReqInfo.PrimCutVlvReqData[4] = input[44];
    Vlv_ActrSyncArbNormVlvReqInfo.SecdCutVlvReqData[0] = input[45];
    Vlv_ActrSyncArbNormVlvReqInfo.SecdCutVlvReqData[1] = input[46];
    Vlv_ActrSyncArbNormVlvReqInfo.SecdCutVlvReqData[2] = input[47];
    Vlv_ActrSyncArbNormVlvReqInfo.SecdCutVlvReqData[3] = input[48];
    Vlv_ActrSyncArbNormVlvReqInfo.SecdCutVlvReqData[4] = input[49];
    Vlv_ActrSyncArbNormVlvReqInfo.SimVlvReqData[0] = input[50];
    Vlv_ActrSyncArbNormVlvReqInfo.SimVlvReqData[1] = input[51];
    Vlv_ActrSyncArbNormVlvReqInfo.SimVlvReqData[2] = input[52];
    Vlv_ActrSyncArbNormVlvReqInfo.SimVlvReqData[3] = input[53];
    Vlv_ActrSyncArbNormVlvReqInfo.SimVlvReqData[4] = input[54];
    Vlv_ActrSyncArbNormVlvReqInfo.RelsVlvReqData[0] = input[55];
    Vlv_ActrSyncArbNormVlvReqInfo.RelsVlvReqData[1] = input[56];
    Vlv_ActrSyncArbNormVlvReqInfo.RelsVlvReqData[2] = input[57];
    Vlv_ActrSyncArbNormVlvReqInfo.RelsVlvReqData[3] = input[58];
    Vlv_ActrSyncArbNormVlvReqInfo.RelsVlvReqData[4] = input[59];
    Vlv_ActrSyncArbNormVlvReqInfo.CircVlvReqData[0] = input[60];
    Vlv_ActrSyncArbNormVlvReqInfo.CircVlvReqData[1] = input[61];
    Vlv_ActrSyncArbNormVlvReqInfo.CircVlvReqData[2] = input[62];
    Vlv_ActrSyncArbNormVlvReqInfo.CircVlvReqData[3] = input[63];
    Vlv_ActrSyncArbNormVlvReqInfo.CircVlvReqData[4] = input[64];
    Vlv_ActrSyncArbNormVlvReqInfo.PressDumpVlvReqData[0] = input[65];
    Vlv_ActrSyncArbNormVlvReqInfo.PressDumpVlvReqData[1] = input[66];
    Vlv_ActrSyncArbNormVlvReqInfo.PressDumpVlvReqData[2] = input[67];
    Vlv_ActrSyncArbNormVlvReqInfo.PressDumpVlvReqData[3] = input[68];
    Vlv_ActrSyncArbNormVlvReqInfo.PressDumpVlvReqData[4] = input[69];
    Vlv_ActrSyncArbBalVlvReqInfo.BalVlvReqData[0] = input[70];
    Vlv_ActrSyncArbBalVlvReqInfo.BalVlvReqData[1] = input[71];
    Vlv_ActrSyncArbBalVlvReqInfo.BalVlvReqData[2] = input[72];
    Vlv_ActrSyncArbBalVlvReqInfo.BalVlvReqData[3] = input[73];
    Vlv_ActrSyncArbBalVlvReqInfo.BalVlvReqData[4] = input[74];
    Vlv_ActrSyncArbResPVlvReqInfo.ResPVlvReqData[0] = input[75];
    Vlv_ActrSyncArbResPVlvReqInfo.ResPVlvReqData[1] = input[76];
    Vlv_ActrSyncArbResPVlvReqInfo.ResPVlvReqData[2] = input[77];
    Vlv_ActrSyncArbResPVlvReqInfo.ResPVlvReqData[3] = input[78];
    Vlv_ActrSyncArbResPVlvReqInfo.ResPVlvReqData[4] = input[79];
    Vlv_ActrSyncEcuModeSts = input[80];
    Vlv_ActrSyncArbVlvSync = input[81];
    Vlv_ActrSyncAcmAsicInitCompleteFlag = input[82];
    Vlv_ActrSyncFuncInhibitVlvSts = input[83];

    Vlv_ActrSync();


    output[0] = Vlv_ActrSyncNormVlvReqInfo.PrimCutVlvReqData[0];
    output[1] = Vlv_ActrSyncNormVlvReqInfo.PrimCutVlvReqData[1];
    output[2] = Vlv_ActrSyncNormVlvReqInfo.PrimCutVlvReqData[2];
    output[3] = Vlv_ActrSyncNormVlvReqInfo.PrimCutVlvReqData[3];
    output[4] = Vlv_ActrSyncNormVlvReqInfo.PrimCutVlvReqData[4];
    output[5] = Vlv_ActrSyncNormVlvReqInfo.RelsVlvReqData[0];
    output[6] = Vlv_ActrSyncNormVlvReqInfo.RelsVlvReqData[1];
    output[7] = Vlv_ActrSyncNormVlvReqInfo.RelsVlvReqData[2];
    output[8] = Vlv_ActrSyncNormVlvReqInfo.RelsVlvReqData[3];
    output[9] = Vlv_ActrSyncNormVlvReqInfo.RelsVlvReqData[4];
    output[10] = Vlv_ActrSyncNormVlvReqInfo.SecdCutVlvReqData[0];
    output[11] = Vlv_ActrSyncNormVlvReqInfo.SecdCutVlvReqData[1];
    output[12] = Vlv_ActrSyncNormVlvReqInfo.SecdCutVlvReqData[2];
    output[13] = Vlv_ActrSyncNormVlvReqInfo.SecdCutVlvReqData[3];
    output[14] = Vlv_ActrSyncNormVlvReqInfo.SecdCutVlvReqData[4];
    output[15] = Vlv_ActrSyncNormVlvReqInfo.CircVlvReqData[0];
    output[16] = Vlv_ActrSyncNormVlvReqInfo.CircVlvReqData[1];
    output[17] = Vlv_ActrSyncNormVlvReqInfo.CircVlvReqData[2];
    output[18] = Vlv_ActrSyncNormVlvReqInfo.CircVlvReqData[3];
    output[19] = Vlv_ActrSyncNormVlvReqInfo.CircVlvReqData[4];
    output[20] = Vlv_ActrSyncNormVlvReqInfo.SimVlvReqData[0];
    output[21] = Vlv_ActrSyncNormVlvReqInfo.SimVlvReqData[1];
    output[22] = Vlv_ActrSyncNormVlvReqInfo.SimVlvReqData[2];
    output[23] = Vlv_ActrSyncNormVlvReqInfo.SimVlvReqData[3];
    output[24] = Vlv_ActrSyncNormVlvReqInfo.SimVlvReqData[4];
    output[25] = Vlv_ActrSyncNormVlvCurrInfo.PrimCircVlv1msCurrMon;
    output[26] = Vlv_ActrSyncNormVlvCurrInfo.SecdCirCVlv1msCurrMon;
    output[27] = Vlv_ActrSyncNormVlvCurrInfo.PrimCutVlv1msCurrMon;
    output[28] = Vlv_ActrSyncNormVlvCurrInfo.SecdCutVlv1msCurrMon;
    output[29] = Vlv_ActrSyncNormVlvCurrInfo.SimVlv1msCurrMon;
    output[30] = Vlv_ActrSyncWhlVlvReqInfo.FlOvReqData[0];
    output[31] = Vlv_ActrSyncWhlVlvReqInfo.FlOvReqData[1];
    output[32] = Vlv_ActrSyncWhlVlvReqInfo.FlOvReqData[2];
    output[33] = Vlv_ActrSyncWhlVlvReqInfo.FlOvReqData[3];
    output[34] = Vlv_ActrSyncWhlVlvReqInfo.FlOvReqData[4];
    output[35] = Vlv_ActrSyncWhlVlvReqInfo.FlIvReqData[0];
    output[36] = Vlv_ActrSyncWhlVlvReqInfo.FlIvReqData[1];
    output[37] = Vlv_ActrSyncWhlVlvReqInfo.FlIvReqData[2];
    output[38] = Vlv_ActrSyncWhlVlvReqInfo.FlIvReqData[3];
    output[39] = Vlv_ActrSyncWhlVlvReqInfo.FlIvReqData[4];
    output[40] = Vlv_ActrSyncWhlVlvReqInfo.FrOvReqData[0];
    output[41] = Vlv_ActrSyncWhlVlvReqInfo.FrOvReqData[1];
    output[42] = Vlv_ActrSyncWhlVlvReqInfo.FrOvReqData[2];
    output[43] = Vlv_ActrSyncWhlVlvReqInfo.FrOvReqData[3];
    output[44] = Vlv_ActrSyncWhlVlvReqInfo.FrOvReqData[4];
    output[45] = Vlv_ActrSyncWhlVlvReqInfo.FrIvReqData[0];
    output[46] = Vlv_ActrSyncWhlVlvReqInfo.FrIvReqData[1];
    output[47] = Vlv_ActrSyncWhlVlvReqInfo.FrIvReqData[2];
    output[48] = Vlv_ActrSyncWhlVlvReqInfo.FrIvReqData[3];
    output[49] = Vlv_ActrSyncWhlVlvReqInfo.FrIvReqData[4];
    output[50] = Vlv_ActrSyncWhlVlvReqInfo.RlOvReqData[0];
    output[51] = Vlv_ActrSyncWhlVlvReqInfo.RlOvReqData[1];
    output[52] = Vlv_ActrSyncWhlVlvReqInfo.RlOvReqData[2];
    output[53] = Vlv_ActrSyncWhlVlvReqInfo.RlOvReqData[3];
    output[54] = Vlv_ActrSyncWhlVlvReqInfo.RlOvReqData[4];
    output[55] = Vlv_ActrSyncWhlVlvReqInfo.RlIvReqData[0];
    output[56] = Vlv_ActrSyncWhlVlvReqInfo.RlIvReqData[1];
    output[57] = Vlv_ActrSyncWhlVlvReqInfo.RlIvReqData[2];
    output[58] = Vlv_ActrSyncWhlVlvReqInfo.RlIvReqData[3];
    output[59] = Vlv_ActrSyncWhlVlvReqInfo.RlIvReqData[4];
    output[60] = Vlv_ActrSyncWhlVlvReqInfo.RrOvReqData[0];
    output[61] = Vlv_ActrSyncWhlVlvReqInfo.RrOvReqData[1];
    output[62] = Vlv_ActrSyncWhlVlvReqInfo.RrOvReqData[2];
    output[63] = Vlv_ActrSyncWhlVlvReqInfo.RrOvReqData[3];
    output[64] = Vlv_ActrSyncWhlVlvReqInfo.RrOvReqData[4];
    output[65] = Vlv_ActrSyncWhlVlvReqInfo.RrIvReqData[0];
    output[66] = Vlv_ActrSyncWhlVlvReqInfo.RrIvReqData[1];
    output[67] = Vlv_ActrSyncWhlVlvReqInfo.RrIvReqData[2];
    output[68] = Vlv_ActrSyncWhlVlvReqInfo.RrIvReqData[3];
    output[69] = Vlv_ActrSyncWhlVlvReqInfo.RrIvReqData[4];
    output[70] = Vlv_ActrSyncBalVlvReqInfo.PrimBalVlvReqData[0];
    output[71] = Vlv_ActrSyncBalVlvReqInfo.PrimBalVlvReqData[1];
    output[72] = Vlv_ActrSyncBalVlvReqInfo.PrimBalVlvReqData[2];
    output[73] = Vlv_ActrSyncBalVlvReqInfo.PrimBalVlvReqData[3];
    output[74] = Vlv_ActrSyncBalVlvReqInfo.PrimBalVlvReqData[4];
    output[75] = Vlv_ActrSyncBalVlvReqInfo.PressDumpVlvReqData[0];
    output[76] = Vlv_ActrSyncBalVlvReqInfo.PressDumpVlvReqData[1];
    output[77] = Vlv_ActrSyncBalVlvReqInfo.PressDumpVlvReqData[2];
    output[78] = Vlv_ActrSyncBalVlvReqInfo.PressDumpVlvReqData[3];
    output[79] = Vlv_ActrSyncBalVlvReqInfo.PressDumpVlvReqData[4];
    output[80] = Vlv_ActrSyncBalVlvReqInfo.ChmbBalVlvReqData[0];
    output[81] = Vlv_ActrSyncBalVlvReqInfo.ChmbBalVlvReqData[1];
    output[82] = Vlv_ActrSyncBalVlvReqInfo.ChmbBalVlvReqData[2];
    output[83] = Vlv_ActrSyncBalVlvReqInfo.ChmbBalVlvReqData[3];
    output[84] = Vlv_ActrSyncBalVlvReqInfo.ChmbBalVlvReqData[4];
    output[85] = Vlv_ActrSyncVlvSync;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Vlv_ActrSync_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
