#define S_FUNCTION_NAME      Vlv_Actr_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          89
#define WidthOutputPort         16

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Vlv_Actr.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Vlv_ActrNormVlvReqInfo.PrimCutVlvReqData[0] = input[0];
    Vlv_ActrNormVlvReqInfo.PrimCutVlvReqData[1] = input[1];
    Vlv_ActrNormVlvReqInfo.PrimCutVlvReqData[2] = input[2];
    Vlv_ActrNormVlvReqInfo.PrimCutVlvReqData[3] = input[3];
    Vlv_ActrNormVlvReqInfo.PrimCutVlvReqData[4] = input[4];
    Vlv_ActrNormVlvReqInfo.RelsVlvReqData[0] = input[5];
    Vlv_ActrNormVlvReqInfo.RelsVlvReqData[1] = input[6];
    Vlv_ActrNormVlvReqInfo.RelsVlvReqData[2] = input[7];
    Vlv_ActrNormVlvReqInfo.RelsVlvReqData[3] = input[8];
    Vlv_ActrNormVlvReqInfo.RelsVlvReqData[4] = input[9];
    Vlv_ActrNormVlvReqInfo.SecdCutVlvReqData[0] = input[10];
    Vlv_ActrNormVlvReqInfo.SecdCutVlvReqData[1] = input[11];
    Vlv_ActrNormVlvReqInfo.SecdCutVlvReqData[2] = input[12];
    Vlv_ActrNormVlvReqInfo.SecdCutVlvReqData[3] = input[13];
    Vlv_ActrNormVlvReqInfo.SecdCutVlvReqData[4] = input[14];
    Vlv_ActrNormVlvReqInfo.CircVlvReqData[0] = input[15];
    Vlv_ActrNormVlvReqInfo.CircVlvReqData[1] = input[16];
    Vlv_ActrNormVlvReqInfo.CircVlvReqData[2] = input[17];
    Vlv_ActrNormVlvReqInfo.CircVlvReqData[3] = input[18];
    Vlv_ActrNormVlvReqInfo.CircVlvReqData[4] = input[19];
    Vlv_ActrNormVlvReqInfo.SimVlvReqData[0] = input[20];
    Vlv_ActrNormVlvReqInfo.SimVlvReqData[1] = input[21];
    Vlv_ActrNormVlvReqInfo.SimVlvReqData[2] = input[22];
    Vlv_ActrNormVlvReqInfo.SimVlvReqData[3] = input[23];
    Vlv_ActrNormVlvReqInfo.SimVlvReqData[4] = input[24];
    Vlv_ActrVlvMonInfo.VlvCVMon = input[25];
    Vlv_ActrVlvMonInfo.VlvRLVMon = input[26];
    Vlv_ActrVlvMonInfo.PrimCutVlvMon = input[27];
    Vlv_ActrVlvMonInfo.SecdCutVlvMon = input[28];
    Vlv_ActrVlvMonInfo.SimVlvMon = input[29];
    Vlv_ActrWhlVlvReqInfo.FlOvReqData[0] = input[30];
    Vlv_ActrWhlVlvReqInfo.FlOvReqData[1] = input[31];
    Vlv_ActrWhlVlvReqInfo.FlOvReqData[2] = input[32];
    Vlv_ActrWhlVlvReqInfo.FlOvReqData[3] = input[33];
    Vlv_ActrWhlVlvReqInfo.FlOvReqData[4] = input[34];
    Vlv_ActrWhlVlvReqInfo.FlIvReqData[0] = input[35];
    Vlv_ActrWhlVlvReqInfo.FlIvReqData[1] = input[36];
    Vlv_ActrWhlVlvReqInfo.FlIvReqData[2] = input[37];
    Vlv_ActrWhlVlvReqInfo.FlIvReqData[3] = input[38];
    Vlv_ActrWhlVlvReqInfo.FlIvReqData[4] = input[39];
    Vlv_ActrWhlVlvReqInfo.FrOvReqData[0] = input[40];
    Vlv_ActrWhlVlvReqInfo.FrOvReqData[1] = input[41];
    Vlv_ActrWhlVlvReqInfo.FrOvReqData[2] = input[42];
    Vlv_ActrWhlVlvReqInfo.FrOvReqData[3] = input[43];
    Vlv_ActrWhlVlvReqInfo.FrOvReqData[4] = input[44];
    Vlv_ActrWhlVlvReqInfo.FrIvReqData[0] = input[45];
    Vlv_ActrWhlVlvReqInfo.FrIvReqData[1] = input[46];
    Vlv_ActrWhlVlvReqInfo.FrIvReqData[2] = input[47];
    Vlv_ActrWhlVlvReqInfo.FrIvReqData[3] = input[48];
    Vlv_ActrWhlVlvReqInfo.FrIvReqData[4] = input[49];
    Vlv_ActrWhlVlvReqInfo.RlOvReqData[0] = input[50];
    Vlv_ActrWhlVlvReqInfo.RlOvReqData[1] = input[51];
    Vlv_ActrWhlVlvReqInfo.RlOvReqData[2] = input[52];
    Vlv_ActrWhlVlvReqInfo.RlOvReqData[3] = input[53];
    Vlv_ActrWhlVlvReqInfo.RlOvReqData[4] = input[54];
    Vlv_ActrWhlVlvReqInfo.RlIvReqData[0] = input[55];
    Vlv_ActrWhlVlvReqInfo.RlIvReqData[1] = input[56];
    Vlv_ActrWhlVlvReqInfo.RlIvReqData[2] = input[57];
    Vlv_ActrWhlVlvReqInfo.RlIvReqData[3] = input[58];
    Vlv_ActrWhlVlvReqInfo.RlIvReqData[4] = input[59];
    Vlv_ActrWhlVlvReqInfo.RrOvReqData[0] = input[60];
    Vlv_ActrWhlVlvReqInfo.RrOvReqData[1] = input[61];
    Vlv_ActrWhlVlvReqInfo.RrOvReqData[2] = input[62];
    Vlv_ActrWhlVlvReqInfo.RrOvReqData[3] = input[63];
    Vlv_ActrWhlVlvReqInfo.RrOvReqData[4] = input[64];
    Vlv_ActrWhlVlvReqInfo.RrIvReqData[0] = input[65];
    Vlv_ActrWhlVlvReqInfo.RrIvReqData[1] = input[66];
    Vlv_ActrWhlVlvReqInfo.RrIvReqData[2] = input[67];
    Vlv_ActrWhlVlvReqInfo.RrIvReqData[3] = input[68];
    Vlv_ActrWhlVlvReqInfo.RrIvReqData[4] = input[69];
    Vlv_ActrBalVlvReqInfo.PrimBalVlvReqData[0] = input[70];
    Vlv_ActrBalVlvReqInfo.PrimBalVlvReqData[1] = input[71];
    Vlv_ActrBalVlvReqInfo.PrimBalVlvReqData[2] = input[72];
    Vlv_ActrBalVlvReqInfo.PrimBalVlvReqData[3] = input[73];
    Vlv_ActrBalVlvReqInfo.PrimBalVlvReqData[4] = input[74];
    Vlv_ActrBalVlvReqInfo.PressDumpVlvReqData[0] = input[75];
    Vlv_ActrBalVlvReqInfo.PressDumpVlvReqData[1] = input[76];
    Vlv_ActrBalVlvReqInfo.PressDumpVlvReqData[2] = input[77];
    Vlv_ActrBalVlvReqInfo.PressDumpVlvReqData[3] = input[78];
    Vlv_ActrBalVlvReqInfo.PressDumpVlvReqData[4] = input[79];
    Vlv_ActrBalVlvReqInfo.ChmbBalVlvReqData[0] = input[80];
    Vlv_ActrBalVlvReqInfo.ChmbBalVlvReqData[1] = input[81];
    Vlv_ActrBalVlvReqInfo.ChmbBalVlvReqData[2] = input[82];
    Vlv_ActrBalVlvReqInfo.ChmbBalVlvReqData[3] = input[83];
    Vlv_ActrBalVlvReqInfo.ChmbBalVlvReqData[4] = input[84];
    Vlv_ActrEcuModeSts = input[85];
    Vlv_ActrAcmAsicInitCompleteFlag = input[86];
    Vlv_ActrFuncInhibitVlvSts = input[87];
    Vlv_ActrVlvSync = input[88];

    Vlv_Actr();


    output[0] = Vlv_ActrNormVlvDrvInfo.PrimCutVlvDrvData;
    output[1] = Vlv_ActrNormVlvDrvInfo.RelsVlvDrvData;
    output[2] = Vlv_ActrNormVlvDrvInfo.SecdCutVlvDrvData;
    output[3] = Vlv_ActrNormVlvDrvInfo.CircVlvDrvData;
    output[4] = Vlv_ActrNormVlvDrvInfo.SimVlvDrvData;
    output[5] = Vlv_ActrWhlVlvDrvInfo.FlOvDrvData;
    output[6] = Vlv_ActrWhlVlvDrvInfo.FlIvDrvData;
    output[7] = Vlv_ActrWhlVlvDrvInfo.FrOvDrvData;
    output[8] = Vlv_ActrWhlVlvDrvInfo.FrIvDrvData;
    output[9] = Vlv_ActrWhlVlvDrvInfo.RlOvDrvData;
    output[10] = Vlv_ActrWhlVlvDrvInfo.RlIvDrvData;
    output[11] = Vlv_ActrWhlVlvDrvInfo.RrOvDrvData;
    output[12] = Vlv_ActrWhlVlvDrvInfo.RrIvDrvData;
    output[13] = Vlv_ActrBalVlvDrvInfo.PrimBalVlvDrvData;
    output[14] = Vlv_ActrBalVlvDrvInfo.PressDumpVlvDrvData;
    output[15] = Vlv_ActrBalVlvDrvInfo.ChmbBalVlvDrvData;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Vlv_Actr_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
