#define S_FUNCTION_NAME      Vlv_Dummy_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          0
#define WidthOutputPort         24

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Vlv_Dummy.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];


    Vlv_Dummy();


    output[0] = Vlv_DummyEscVlvReqAswInfo.PtcReqData[0];
    output[1] = Vlv_DummyEscVlvReqAswInfo.PtcReqData[1];
    output[2] = Vlv_DummyEscVlvReqAswInfo.PtcReqData[2];
    output[3] = Vlv_DummyEscVlvReqAswInfo.PtcReqData[3];
    output[4] = Vlv_DummyEscVlvReqAswInfo.PtcReqData[4];
    output[5] = Vlv_DummyEscVlvReqAswInfo.StcReqData[0];
    output[6] = Vlv_DummyEscVlvReqAswInfo.StcReqData[1];
    output[7] = Vlv_DummyEscVlvReqAswInfo.StcReqData[2];
    output[8] = Vlv_DummyEscVlvReqAswInfo.StcReqData[3];
    output[9] = Vlv_DummyEscVlvReqAswInfo.StcReqData[4];
    output[10] = Vlv_DummyEscVlvReqAswInfo.PesvReqData[0];
    output[11] = Vlv_DummyEscVlvReqAswInfo.PesvReqData[1];
    output[12] = Vlv_DummyEscVlvReqAswInfo.PesvReqData[2];
    output[13] = Vlv_DummyEscVlvReqAswInfo.PesvReqData[3];
    output[14] = Vlv_DummyEscVlvReqAswInfo.PesvReqData[4];
    output[15] = Vlv_DummyEscVlvReqAswInfo.SesvReqData[0];
    output[16] = Vlv_DummyEscVlvReqAswInfo.SesvReqData[1];
    output[17] = Vlv_DummyEscVlvReqAswInfo.SesvReqData[2];
    output[18] = Vlv_DummyEscVlvReqAswInfo.SesvReqData[3];
    output[19] = Vlv_DummyEscVlvReqAswInfo.SesvReqData[4];
    output[20] = Vlv_DummyEscVlvReqAswInfo.PtcReq;
    output[21] = Vlv_DummyEscVlvReqAswInfo.StcReq;
    output[22] = Vlv_DummyEscVlvReqAswInfo.PesvReq;
    output[23] = Vlv_DummyEscVlvReqAswInfo.SesvReq;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Vlv_Dummy_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
