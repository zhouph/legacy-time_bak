/* Virtual main for linking */
/* purpose : Generation of map file */
#include "Vlv_Actr.h"
#include "Vlv_ActrSync.h"

int main(void)
{
    Vlv_Actr_Init();
    Vlv_ActrSync_Init();

    while(1)
    {
        Vlv_Actr();
        Vlv_ActrSync();
    }
}