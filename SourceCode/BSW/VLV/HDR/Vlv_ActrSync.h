/**
 * @defgroup Vlv_ActrSync Vlv_ActrSync
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Vlv_ActrSync.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef VLV_ACTRSYNC_H_
#define VLV_ACTRSYNC_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Vlv_Types.h"
#include "Vlv_Cfg.h"
#include "Vlv_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define VLV_ACTRSYNC_MODULE_ID      (0)
 #define VLV_ACTRSYNC_MAJOR_VERSION  (2)
 #define VLV_ACTRSYNC_MINOR_VERSION  (0)
 #define VLV_ACTRSYNC_PATCH_VERSION  (0)
 #define VLV_ACTRSYNC_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Vlv_ActrSync_HdrBusType Vlv_ActrSyncBus;

/* Version Info */
extern const SwcVersionInfo_t Vlv_ActrSyncVersionInfo;

/* Input Data Element */
extern Arbitrator_VlvArbWhlVlvReqInfo_t Vlv_ActrSyncArbWhlVlvReqInfo;
extern Arbitrator_VlvArbNormVlvReqInfo_t Vlv_ActrSyncArbNormVlvReqInfo;
extern Arbitrator_VlvArbBalVlvReqInfo_t Vlv_ActrSyncArbBalVlvReqInfo;
extern Arbitrator_VlvArbResPVlvReqInfo_t Vlv_ActrSyncArbResPVlvReqInfo;
extern Mom_HndlrEcuModeSts_t Vlv_ActrSyncEcuModeSts;
extern Arbitrator_VlvArbVlvSync_t Vlv_ActrSyncArbVlvSync;
extern Acm_MainAcmAsicInitCompleteFlag_t Vlv_ActrSyncAcmAsicInitCompleteFlag;
extern Eem_SuspcDetnFuncInhibitVlvSts_t Vlv_ActrSyncFuncInhibitVlvSts;

/* Output Data Element */
extern Vlv_ActrSyncNormVlvReqInfo_t Vlv_ActrSyncNormVlvReqInfo;
extern Vlv_ActrSyncNormVlvCurrInfo_t Vlv_ActrSyncNormVlvCurrInfo;
extern Vlv_ActrSyncWhlVlvReqInfo_t Vlv_ActrSyncWhlVlvReqInfo;
extern Vlv_ActrSyncBalVlvReqInfo_t Vlv_ActrSyncBalVlvReqInfo;
extern Vlv_ActrSyncVlvSync_t Vlv_ActrSyncVlvSync;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Vlv_ActrSync_Init(void);
extern void Vlv_ActrSync(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* VLV_ACTRSYNC_H_ */
/** @} */
