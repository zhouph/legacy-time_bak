/**
 * @defgroup Vlv_Actr Vlv_Actr
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Vlv_Actr.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef VLV_ACTR_H_
#define VLV_ACTR_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Vlv_Types.h"
#include "Vlv_Cfg.h"
#include "Vlv_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define VLV_ACTR_MODULE_ID      (0)
 #define VLV_ACTR_MAJOR_VERSION  (2)
 #define VLV_ACTR_MINOR_VERSION  (0)
 #define VLV_ACTR_PATCH_VERSION  (0)
 #define VLV_ACTR_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Vlv_Actr_HdrBusType Vlv_ActrBus;

/* Version Info */
extern const SwcVersionInfo_t Vlv_ActrVersionInfo;

/* Input Data Element */
extern Vlv_ActrSyncNormVlvReqInfo_t Vlv_ActrNormVlvReqInfo;
extern Ioc_InputSR1msVlvMonInfo_t Vlv_ActrVlvMonInfo;
extern Vlv_ActrSyncWhlVlvReqInfo_t Vlv_ActrWhlVlvReqInfo;
extern Vlv_ActrSyncBalVlvReqInfo_t Vlv_ActrBalVlvReqInfo;
extern Mom_HndlrEcuModeSts_t Vlv_ActrEcuModeSts;
extern Acm_MainAcmAsicInitCompleteFlag_t Vlv_ActrAcmAsicInitCompleteFlag;
extern Eem_SuspcDetnFuncInhibitVlvSts_t Vlv_ActrFuncInhibitVlvSts;
extern Vlv_ActrSyncVlvSync_t Vlv_ActrVlvSync;

/* Output Data Element */
extern Vlv_ActrNormVlvDrvInfo_t Vlv_ActrNormVlvDrvInfo;
extern Vlv_ActrWhlVlvDrvInfo_t Vlv_ActrWhlVlvDrvInfo;
extern Vlv_ActrBalVlvDrvInfo_t Vlv_ActrBalVlvDrvInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Vlv_Actr_Init(void);
extern void Vlv_Actr(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* VLV_ACTR_H_ */
/** @} */
