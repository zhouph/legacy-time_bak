/**
 * @defgroup Vlv_Types Vlv_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Vlv_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef VLV_TYPES_H_
#define VLV_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Rte_Types.h"
#include "RteDataHandle_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define VLV_PORT_MAX_NUM       (9)

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
typedef uint8 Vlv_5msBufIdx_t;
typedef struct
{
/* Input Data Element */
    Vlv_ActrSyncNormVlvReqInfo_t Vlv_ActrNormVlvReqInfo;
    Ioc_InputSR1msVlvMonInfo_t Vlv_ActrVlvMonInfo;
    Vlv_ActrSyncWhlVlvReqInfo_t Vlv_ActrWhlVlvReqInfo;
    Vlv_ActrSyncBalVlvReqInfo_t Vlv_ActrBalVlvReqInfo;
    Mom_HndlrEcuModeSts_t Vlv_ActrEcuModeSts;
    Acm_MainAcmAsicInitCompleteFlag_t Vlv_ActrAcmAsicInitCompleteFlag;
    Eem_SuspcDetnFuncInhibitVlvSts_t Vlv_ActrFuncInhibitVlvSts;
    Vlv_ActrSyncVlvSync_t Vlv_ActrVlvSync;

/* Output Data Element */
    Vlv_ActrNormVlvDrvInfo_t Vlv_ActrNormVlvDrvInfo;
    Vlv_ActrWhlVlvDrvInfo_t Vlv_ActrWhlVlvDrvInfo;
    Vlv_ActrBalVlvDrvInfo_t Vlv_ActrBalVlvDrvInfo;
/* Internal Data */

 	Haluint16 OutputCurrentValue[VLV_PORT_MAX_NUM];
    Haluint16 Vlv_1msOutputCnt;
	
}Vlv_Actr_HdrBusType;

typedef struct
{
/* Input Data Element */
    Arbitrator_VlvArbWhlVlvReqInfo_t Vlv_ActrSyncArbWhlVlvReqInfo;
    Arbitrator_VlvArbNormVlvReqInfo_t Vlv_ActrSyncArbNormVlvReqInfo;
    Arbitrator_VlvArbBalVlvReqInfo_t Vlv_ActrSyncArbBalVlvReqInfo;
    Arbitrator_VlvArbResPVlvReqInfo_t Vlv_ActrSyncArbResPVlvReqInfo;
    Mom_HndlrEcuModeSts_t Vlv_ActrSyncEcuModeSts;
    Arbitrator_VlvArbVlvSync_t Vlv_ActrSyncArbVlvSync;
    Acm_MainAcmAsicInitCompleteFlag_t Vlv_ActrSyncAcmAsicInitCompleteFlag;
    Eem_SuspcDetnFuncInhibitVlvSts_t Vlv_ActrSyncFuncInhibitVlvSts;

/* Output Data Element */
    Vlv_ActrSyncNormVlvReqInfo_t Vlv_ActrSyncNormVlvReqInfo;
    Vlv_ActrSyncNormVlvCurrInfo_t Vlv_ActrSyncNormVlvCurrInfo;
    Vlv_ActrSyncWhlVlvReqInfo_t Vlv_ActrSyncWhlVlvReqInfo;
    Vlv_ActrSyncBalVlvReqInfo_t Vlv_ActrSyncBalVlvReqInfo;
    Vlv_ActrSyncVlvSync_t Vlv_ActrSyncVlvSync;

/* Internal Data */
    Vlv_5msBufIdx_t Vlv_5msOutputFlOvBufIdx;
    Vlv_5msBufIdx_t Vlv_5msOutputFlIvBufIdx;
    Vlv_5msBufIdx_t Vlv_5msOutputFrOvBufIdx;
    Vlv_5msBufIdx_t Vlv_5msOutputFrIvBufIdx;
    Vlv_5msBufIdx_t Vlv_5msOutputRlOvBufIdx;
    Vlv_5msBufIdx_t Vlv_5msOutputRlIvBufIdx;
    Vlv_5msBufIdx_t Vlv_5msOutputRrOvBufIdx;
    Vlv_5msBufIdx_t Vlv_5msOutputRrIvBufIdx;
    Vlv_5msBufIdx_t Vlv_5msOutputPrimCutBufIdx;
    Vlv_5msBufIdx_t Vlv_5msOutputPrimCircBufIdx;
    Vlv_5msBufIdx_t Vlv_5msOutputSecdCutBufIdx;
    Vlv_5msBufIdx_t Vlv_5msOutputSecdCircBufIdx;
    Vlv_5msBufIdx_t Vlv_5msOutputSimVlvBufIdx;
}Vlv_ActrSync_HdrBusType;
/* Requesting client ID type */
 typedef uint8 Vlv_ClientIdType;

 /* Requesting buffer type */
 typedef uint16 Vlv_DataType;

 /* Current sense data type */
 typedef uint16 Vlv_CurrentType;

 /* Voltage sense data type */
 typedef uint16 Vlv_VoltageType;

 /* Requesting buffer length type */
 typedef uint8 Vlv_BufLengthType;

 /* Conversion multiply factor type */
 typedef uint32 Vlv_MulFactorType;

 /* Conversion divide factor type */
 typedef uint32 Vlv_DivFactorType;

 /* Valve port type */
 typedef uint8 Vlv_PortType;

 /* Valve Pid integer gain type */
 typedef sint16 Vlv_PidIntGainType;

 /* Valve Pid float gain type */
 typedef sint16 Vlv_PidFloatGainType;

 /* Valve Pid Current Limit type */
 typedef sint16 Vlv_PidLimitType;

 /* Valve port type */
 typedef uint8 Vlv_PidPortType;

 /* Enable status type */
 typedef enum
 {
    Vlv_EnStatus_Disabled = 0,
    Vlv_EnStatus_Enabled
 }Vlv_EnStatusType;

  /* Running status type */
 typedef enum
 {
    Vlv_RunStatus_Idle = 0,
    Vlv_RunStatus_Running,
    Vlv_RunStatus_Fail
 }Vlv_RunStatusType;

/* Client Status Type */
 typedef enum
 {
    Vlv_ReqStatus_NotRequested = 0,
    Vlv_ReqStatus_Requested
 }Vlv_ClientStsType;

 typedef enum
 {
    Vlv_Error_IdxOverflow = 0,
    Vlv_Error_Input,
    Vlv_Error_Timing,
    Vlv_Error_Config
 }Vlv_ErrorType;

 typedef enum
 {
    Valve_LlModule_Non_Pid = 0,
    Valve_LlModule_Pid
 }Valve_LlModuleType;

 /* Valve actuator config type */
 typedef struct
 {
    Vlv_DataType                  MaxValue;              /* Max input value */
    Vlv_DataType                  MinValue;              /* Min input value */
    Vlv_MulFactorType             MulFactor;             /* Multiply factor */
    Vlv_DivFactorType             DivFactor;             /* Divide factor */
    Valve_LlModuleType            IsPidtype;             /* Valve_LlModuleType */
    Vlv_PidPortType               PidPort;               /* Port index of Pid */
    Vlv_PidIntGainType            PgainInt;              /* P-gain integer value */
    Vlv_PidFloatGainType          PgainFloat;            /* P-gain float value(int16) */
    Vlv_PidIntGainType            IgainTsInt;            /* I-gain*Ts integer value */
    Vlv_PidFloatGainType          IgainTsFloat;          /* I-gain*Ts float value(int16) */
    Vlv_PidIntGainType            DgainInt;              /* D-gain integer value */
    Vlv_PidFloatGainType          DgainFloat;            /* D-gain float value(int16) */
    Vlv_PidIntGainType            BackCalcValInt;        /* Back calculation integer value */
    Vlv_PidFloatGainType          BackCalcValFloat;      /* Back calculation float value(int16) */    
 }Vlv_ActrChCfgType;
 
 typedef struct
 {
     Vlv_ActrChCfgType     FlOvCfg;
     Vlv_ActrChCfgType     FrOvCfg;
     Vlv_ActrChCfgType     RlOvCfg;
     Vlv_ActrChCfgType     RrOvCfg;
     Vlv_ActrChCfgType     FlIvCfg;
     Vlv_ActrChCfgType     FrIvCfg;
     Vlv_ActrChCfgType     RlIvCfg;
     Vlv_ActrChCfgType     RrIvCfg;
     Vlv_ActrChCfgType     SimCfg;
     Vlv_ActrChCfgType     PrimCutCfg;
     Vlv_ActrChCfgType     SecdCutCfg;
     Vlv_ActrChCfgType     ReslvCfg;
     Vlv_ActrChCfgType     CircvCfg;
     Vlv_ActrChCfgType     PDvCfg;
     Vlv_ActrChCfgType     BalvCfg;

 }Vlv_ActrConfigType;

 /* Valve sensor config type */
 typedef struct
 {
    Vlv_MulFactorType  MulFactor;
    Vlv_DivFactorType  DivFactor;
 }Vlv_SnsrChCfgType;

 typedef struct
 {
     Vlv_SnsrChCfgType     FlOvCfg;
     Vlv_SnsrChCfgType     FrOvCfg;
     Vlv_SnsrChCfgType     RlOvCfg;
     Vlv_SnsrChCfgType     RrOvCfg;
     Vlv_SnsrChCfgType     FlIvCfg;
     Vlv_SnsrChCfgType     FrIvCfg;
     Vlv_SnsrChCfgType     RlIvCfg;
     Vlv_SnsrChCfgType     RrIvCfg;
     Vlv_SnsrChCfgType     SimCfg;
     Vlv_SnsrChCfgType     PrimCutCfg;
     Vlv_SnsrChCfgType     SecdCutCfg;
     Vlv_SnsrChCfgType     ReslvCfg;
     Vlv_SnsrChCfgType     CircvCfg;
     Vlv_ActrChCfgType     PDvCfg;
     Vlv_ActrChCfgType     BalvCfg;
 }Vlv_SnsrConfigType;

 /* Valve actuator status type */
 typedef struct
 {
    Vlv_EnStatusType  EnStatus;
    Vlv_RunStatusType RunStatus;    
    Vlv_BufLengthType BufLen;
    Vlv_BufLengthType BufIdx;
 }Vlv_PortStsType;
/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* VLV_TYPES_H_ */
/** @} */
