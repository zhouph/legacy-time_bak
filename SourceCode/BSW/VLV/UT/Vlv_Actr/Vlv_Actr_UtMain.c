#include "unity.h"
#include "unity_fixture.h"
#include "Vlv_Actr.h"
#include "Vlv_Actr_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Saluint16 UtInput_Vlv_ActrNormVlvReqInfo_PrimCutVlvReqData_0[MAX_STEP] = VLV_ACTRNORMVLVREQINFO_PRIMCUTVLVREQDATA_0;
const Saluint16 UtInput_Vlv_ActrNormVlvReqInfo_PrimCutVlvReqData_1[MAX_STEP] = VLV_ACTRNORMVLVREQINFO_PRIMCUTVLVREQDATA_1;
const Saluint16 UtInput_Vlv_ActrNormVlvReqInfo_PrimCutVlvReqData_2[MAX_STEP] = VLV_ACTRNORMVLVREQINFO_PRIMCUTVLVREQDATA_2;
const Saluint16 UtInput_Vlv_ActrNormVlvReqInfo_PrimCutVlvReqData_3[MAX_STEP] = VLV_ACTRNORMVLVREQINFO_PRIMCUTVLVREQDATA_3;
const Saluint16 UtInput_Vlv_ActrNormVlvReqInfo_PrimCutVlvReqData_4[MAX_STEP] = VLV_ACTRNORMVLVREQINFO_PRIMCUTVLVREQDATA_4;
const Saluint16 UtInput_Vlv_ActrNormVlvReqInfo_RelsVlvReqData_0[MAX_STEP] = VLV_ACTRNORMVLVREQINFO_RELSVLVREQDATA_0;
const Saluint16 UtInput_Vlv_ActrNormVlvReqInfo_RelsVlvReqData_1[MAX_STEP] = VLV_ACTRNORMVLVREQINFO_RELSVLVREQDATA_1;
const Saluint16 UtInput_Vlv_ActrNormVlvReqInfo_RelsVlvReqData_2[MAX_STEP] = VLV_ACTRNORMVLVREQINFO_RELSVLVREQDATA_2;
const Saluint16 UtInput_Vlv_ActrNormVlvReqInfo_RelsVlvReqData_3[MAX_STEP] = VLV_ACTRNORMVLVREQINFO_RELSVLVREQDATA_3;
const Saluint16 UtInput_Vlv_ActrNormVlvReqInfo_RelsVlvReqData_4[MAX_STEP] = VLV_ACTRNORMVLVREQINFO_RELSVLVREQDATA_4;
const Saluint16 UtInput_Vlv_ActrNormVlvReqInfo_SecdCutVlvReqData_0[MAX_STEP] = VLV_ACTRNORMVLVREQINFO_SECDCUTVLVREQDATA_0;
const Saluint16 UtInput_Vlv_ActrNormVlvReqInfo_SecdCutVlvReqData_1[MAX_STEP] = VLV_ACTRNORMVLVREQINFO_SECDCUTVLVREQDATA_1;
const Saluint16 UtInput_Vlv_ActrNormVlvReqInfo_SecdCutVlvReqData_2[MAX_STEP] = VLV_ACTRNORMVLVREQINFO_SECDCUTVLVREQDATA_2;
const Saluint16 UtInput_Vlv_ActrNormVlvReqInfo_SecdCutVlvReqData_3[MAX_STEP] = VLV_ACTRNORMVLVREQINFO_SECDCUTVLVREQDATA_3;
const Saluint16 UtInput_Vlv_ActrNormVlvReqInfo_SecdCutVlvReqData_4[MAX_STEP] = VLV_ACTRNORMVLVREQINFO_SECDCUTVLVREQDATA_4;
const Saluint16 UtInput_Vlv_ActrNormVlvReqInfo_CircVlvReqData_0[MAX_STEP] = VLV_ACTRNORMVLVREQINFO_CIRCVLVREQDATA_0;
const Saluint16 UtInput_Vlv_ActrNormVlvReqInfo_CircVlvReqData_1[MAX_STEP] = VLV_ACTRNORMVLVREQINFO_CIRCVLVREQDATA_1;
const Saluint16 UtInput_Vlv_ActrNormVlvReqInfo_CircVlvReqData_2[MAX_STEP] = VLV_ACTRNORMVLVREQINFO_CIRCVLVREQDATA_2;
const Saluint16 UtInput_Vlv_ActrNormVlvReqInfo_CircVlvReqData_3[MAX_STEP] = VLV_ACTRNORMVLVREQINFO_CIRCVLVREQDATA_3;
const Saluint16 UtInput_Vlv_ActrNormVlvReqInfo_CircVlvReqData_4[MAX_STEP] = VLV_ACTRNORMVLVREQINFO_CIRCVLVREQDATA_4;
const Saluint16 UtInput_Vlv_ActrNormVlvReqInfo_SimVlvReqData_0[MAX_STEP] = VLV_ACTRNORMVLVREQINFO_SIMVLVREQDATA_0;
const Saluint16 UtInput_Vlv_ActrNormVlvReqInfo_SimVlvReqData_1[MAX_STEP] = VLV_ACTRNORMVLVREQINFO_SIMVLVREQDATA_1;
const Saluint16 UtInput_Vlv_ActrNormVlvReqInfo_SimVlvReqData_2[MAX_STEP] = VLV_ACTRNORMVLVREQINFO_SIMVLVREQDATA_2;
const Saluint16 UtInput_Vlv_ActrNormVlvReqInfo_SimVlvReqData_3[MAX_STEP] = VLV_ACTRNORMVLVREQINFO_SIMVLVREQDATA_3;
const Saluint16 UtInput_Vlv_ActrNormVlvReqInfo_SimVlvReqData_4[MAX_STEP] = VLV_ACTRNORMVLVREQINFO_SIMVLVREQDATA_4;
const Haluint16 UtInput_Vlv_ActrVlvMonInfo_VlvCVMon[MAX_STEP] = VLV_ACTRVLVMONINFO_VLVCVMON;
const Haluint16 UtInput_Vlv_ActrVlvMonInfo_VlvRLVMon[MAX_STEP] = VLV_ACTRVLVMONINFO_VLVRLVMON;
const Haluint16 UtInput_Vlv_ActrVlvMonInfo_PrimCutVlvMon[MAX_STEP] = VLV_ACTRVLVMONINFO_PRIMCUTVLVMON;
const Haluint16 UtInput_Vlv_ActrVlvMonInfo_SecdCutVlvMon[MAX_STEP] = VLV_ACTRVLVMONINFO_SECDCUTVLVMON;
const Haluint16 UtInput_Vlv_ActrVlvMonInfo_SimVlvMon[MAX_STEP] = VLV_ACTRVLVMONINFO_SIMVLVMON;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_FlOvReqData_0[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_FLOVREQDATA_0;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_FlOvReqData_1[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_FLOVREQDATA_1;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_FlOvReqData_2[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_FLOVREQDATA_2;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_FlOvReqData_3[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_FLOVREQDATA_3;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_FlOvReqData_4[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_FLOVREQDATA_4;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_FlIvReqData_0[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_FLIVREQDATA_0;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_FlIvReqData_1[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_FLIVREQDATA_1;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_FlIvReqData_2[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_FLIVREQDATA_2;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_FlIvReqData_3[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_FLIVREQDATA_3;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_FlIvReqData_4[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_FLIVREQDATA_4;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_FrOvReqData_0[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_FROVREQDATA_0;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_FrOvReqData_1[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_FROVREQDATA_1;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_FrOvReqData_2[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_FROVREQDATA_2;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_FrOvReqData_3[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_FROVREQDATA_3;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_FrOvReqData_4[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_FROVREQDATA_4;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_FrIvReqData_0[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_FRIVREQDATA_0;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_FrIvReqData_1[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_FRIVREQDATA_1;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_FrIvReqData_2[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_FRIVREQDATA_2;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_FrIvReqData_3[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_FRIVREQDATA_3;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_FrIvReqData_4[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_FRIVREQDATA_4;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_RlOvReqData_0[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_RLOVREQDATA_0;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_RlOvReqData_1[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_RLOVREQDATA_1;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_RlOvReqData_2[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_RLOVREQDATA_2;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_RlOvReqData_3[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_RLOVREQDATA_3;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_RlOvReqData_4[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_RLOVREQDATA_4;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_RlIvReqData_0[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_RLIVREQDATA_0;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_RlIvReqData_1[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_RLIVREQDATA_1;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_RlIvReqData_2[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_RLIVREQDATA_2;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_RlIvReqData_3[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_RLIVREQDATA_3;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_RlIvReqData_4[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_RLIVREQDATA_4;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_RrOvReqData_0[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_RROVREQDATA_0;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_RrOvReqData_1[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_RROVREQDATA_1;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_RrOvReqData_2[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_RROVREQDATA_2;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_RrOvReqData_3[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_RROVREQDATA_3;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_RrOvReqData_4[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_RROVREQDATA_4;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_RrIvReqData_0[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_RRIVREQDATA_0;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_RrIvReqData_1[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_RRIVREQDATA_1;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_RrIvReqData_2[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_RRIVREQDATA_2;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_RrIvReqData_3[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_RRIVREQDATA_3;
const Saluint16 UtInput_Vlv_ActrWhlVlvReqInfo_RrIvReqData_4[MAX_STEP] = VLV_ACTRWHLVLVREQINFO_RRIVREQDATA_4;
const Saluint16 UtInput_Vlv_ActrBalVlvReqInfo_PrimBalVlvReqData_0[MAX_STEP] = VLV_ACTRBALVLVREQINFO_PRIMBALVLVREQDATA_0;
const Saluint16 UtInput_Vlv_ActrBalVlvReqInfo_PrimBalVlvReqData_1[MAX_STEP] = VLV_ACTRBALVLVREQINFO_PRIMBALVLVREQDATA_1;
const Saluint16 UtInput_Vlv_ActrBalVlvReqInfo_PrimBalVlvReqData_2[MAX_STEP] = VLV_ACTRBALVLVREQINFO_PRIMBALVLVREQDATA_2;
const Saluint16 UtInput_Vlv_ActrBalVlvReqInfo_PrimBalVlvReqData_3[MAX_STEP] = VLV_ACTRBALVLVREQINFO_PRIMBALVLVREQDATA_3;
const Saluint16 UtInput_Vlv_ActrBalVlvReqInfo_PrimBalVlvReqData_4[MAX_STEP] = VLV_ACTRBALVLVREQINFO_PRIMBALVLVREQDATA_4;
const Saluint16 UtInput_Vlv_ActrBalVlvReqInfo_PressDumpVlvReqData_0[MAX_STEP] = VLV_ACTRBALVLVREQINFO_PRESSDUMPVLVREQDATA_0;
const Saluint16 UtInput_Vlv_ActrBalVlvReqInfo_PressDumpVlvReqData_1[MAX_STEP] = VLV_ACTRBALVLVREQINFO_PRESSDUMPVLVREQDATA_1;
const Saluint16 UtInput_Vlv_ActrBalVlvReqInfo_PressDumpVlvReqData_2[MAX_STEP] = VLV_ACTRBALVLVREQINFO_PRESSDUMPVLVREQDATA_2;
const Saluint16 UtInput_Vlv_ActrBalVlvReqInfo_PressDumpVlvReqData_3[MAX_STEP] = VLV_ACTRBALVLVREQINFO_PRESSDUMPVLVREQDATA_3;
const Saluint16 UtInput_Vlv_ActrBalVlvReqInfo_PressDumpVlvReqData_4[MAX_STEP] = VLV_ACTRBALVLVREQINFO_PRESSDUMPVLVREQDATA_4;
const Saluint16 UtInput_Vlv_ActrBalVlvReqInfo_ChmbBalVlvReqData_0[MAX_STEP] = VLV_ACTRBALVLVREQINFO_CHMBBALVLVREQDATA_0;
const Saluint16 UtInput_Vlv_ActrBalVlvReqInfo_ChmbBalVlvReqData_1[MAX_STEP] = VLV_ACTRBALVLVREQINFO_CHMBBALVLVREQDATA_1;
const Saluint16 UtInput_Vlv_ActrBalVlvReqInfo_ChmbBalVlvReqData_2[MAX_STEP] = VLV_ACTRBALVLVREQINFO_CHMBBALVLVREQDATA_2;
const Saluint16 UtInput_Vlv_ActrBalVlvReqInfo_ChmbBalVlvReqData_3[MAX_STEP] = VLV_ACTRBALVLVREQINFO_CHMBBALVLVREQDATA_3;
const Saluint16 UtInput_Vlv_ActrBalVlvReqInfo_ChmbBalVlvReqData_4[MAX_STEP] = VLV_ACTRBALVLVREQINFO_CHMBBALVLVREQDATA_4;
const Mom_HndlrEcuModeSts_t UtInput_Vlv_ActrEcuModeSts[MAX_STEP] = VLV_ACTRECUMODESTS;
const Acm_MainAcmAsicInitCompleteFlag_t UtInput_Vlv_ActrAcmAsicInitCompleteFlag[MAX_STEP] = VLV_ACTRACMASICINITCOMPLETEFLAG;
const Eem_SuspcDetnFuncInhibitVlvSts_t UtInput_Vlv_ActrFuncInhibitVlvSts[MAX_STEP] = VLV_ACTRFUNCINHIBITVLVSTS;
const Vlv_ActrSyncVlvSync_t UtInput_Vlv_ActrVlvSync[MAX_STEP] = VLV_ACTRVLVSYNC;

const Saluint16 UtExpected_Vlv_ActrNormVlvDrvInfo_PrimCutVlvDrvData[MAX_STEP] = VLV_ACTRNORMVLVDRVINFO_PRIMCUTVLVDRVDATA;
const Saluint16 UtExpected_Vlv_ActrNormVlvDrvInfo_RelsVlvDrvData[MAX_STEP] = VLV_ACTRNORMVLVDRVINFO_RELSVLVDRVDATA;
const Saluint16 UtExpected_Vlv_ActrNormVlvDrvInfo_SecdCutVlvDrvData[MAX_STEP] = VLV_ACTRNORMVLVDRVINFO_SECDCUTVLVDRVDATA;
const Saluint16 UtExpected_Vlv_ActrNormVlvDrvInfo_CircVlvDrvData[MAX_STEP] = VLV_ACTRNORMVLVDRVINFO_CIRCVLVDRVDATA;
const Saluint16 UtExpected_Vlv_ActrNormVlvDrvInfo_SimVlvDrvData[MAX_STEP] = VLV_ACTRNORMVLVDRVINFO_SIMVLVDRVDATA;
const Saluint16 UtExpected_Vlv_ActrWhlVlvDrvInfo_FlOvDrvData[MAX_STEP] = VLV_ACTRWHLVLVDRVINFO_FLOVDRVDATA;
const Saluint16 UtExpected_Vlv_ActrWhlVlvDrvInfo_FlIvDrvData[MAX_STEP] = VLV_ACTRWHLVLVDRVINFO_FLIVDRVDATA;
const Saluint16 UtExpected_Vlv_ActrWhlVlvDrvInfo_FrOvDrvData[MAX_STEP] = VLV_ACTRWHLVLVDRVINFO_FROVDRVDATA;
const Saluint16 UtExpected_Vlv_ActrWhlVlvDrvInfo_FrIvDrvData[MAX_STEP] = VLV_ACTRWHLVLVDRVINFO_FRIVDRVDATA;
const Saluint16 UtExpected_Vlv_ActrWhlVlvDrvInfo_RlOvDrvData[MAX_STEP] = VLV_ACTRWHLVLVDRVINFO_RLOVDRVDATA;
const Saluint16 UtExpected_Vlv_ActrWhlVlvDrvInfo_RlIvDrvData[MAX_STEP] = VLV_ACTRWHLVLVDRVINFO_RLIVDRVDATA;
const Saluint16 UtExpected_Vlv_ActrWhlVlvDrvInfo_RrOvDrvData[MAX_STEP] = VLV_ACTRWHLVLVDRVINFO_RROVDRVDATA;
const Saluint16 UtExpected_Vlv_ActrWhlVlvDrvInfo_RrIvDrvData[MAX_STEP] = VLV_ACTRWHLVLVDRVINFO_RRIVDRVDATA;
const Saluint16 UtExpected_Vlv_ActrBalVlvDrvInfo_PrimBalVlvDrvData[MAX_STEP] = VLV_ACTRBALVLVDRVINFO_PRIMBALVLVDRVDATA;
const Saluint16 UtExpected_Vlv_ActrBalVlvDrvInfo_PressDumpVlvDrvData[MAX_STEP] = VLV_ACTRBALVLVDRVINFO_PRESSDUMPVLVDRVDATA;
const Saluint16 UtExpected_Vlv_ActrBalVlvDrvInfo_ChmbBalVlvDrvData[MAX_STEP] = VLV_ACTRBALVLVDRVINFO_CHMBBALVLVDRVDATA;



TEST_GROUP(Vlv_Actr);
TEST_SETUP(Vlv_Actr)
{
    Vlv_Actr_Init();
}

TEST_TEAR_DOWN(Vlv_Actr)
{   /* Postcondition */

}

TEST(Vlv_Actr, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Vlv_ActrNormVlvReqInfo.PrimCutVlvReqData[0] = UtInput_Vlv_ActrNormVlvReqInfo_PrimCutVlvReqData_0[i];
        Vlv_ActrNormVlvReqInfo.PrimCutVlvReqData[1] = UtInput_Vlv_ActrNormVlvReqInfo_PrimCutVlvReqData_1[i];
        Vlv_ActrNormVlvReqInfo.PrimCutVlvReqData[2] = UtInput_Vlv_ActrNormVlvReqInfo_PrimCutVlvReqData_2[i];
        Vlv_ActrNormVlvReqInfo.PrimCutVlvReqData[3] = UtInput_Vlv_ActrNormVlvReqInfo_PrimCutVlvReqData_3[i];
        Vlv_ActrNormVlvReqInfo.PrimCutVlvReqData[4] = UtInput_Vlv_ActrNormVlvReqInfo_PrimCutVlvReqData_4[i];
        Vlv_ActrNormVlvReqInfo.RelsVlvReqData[0] = UtInput_Vlv_ActrNormVlvReqInfo_RelsVlvReqData_0[i];
        Vlv_ActrNormVlvReqInfo.RelsVlvReqData[1] = UtInput_Vlv_ActrNormVlvReqInfo_RelsVlvReqData_1[i];
        Vlv_ActrNormVlvReqInfo.RelsVlvReqData[2] = UtInput_Vlv_ActrNormVlvReqInfo_RelsVlvReqData_2[i];
        Vlv_ActrNormVlvReqInfo.RelsVlvReqData[3] = UtInput_Vlv_ActrNormVlvReqInfo_RelsVlvReqData_3[i];
        Vlv_ActrNormVlvReqInfo.RelsVlvReqData[4] = UtInput_Vlv_ActrNormVlvReqInfo_RelsVlvReqData_4[i];
        Vlv_ActrNormVlvReqInfo.SecdCutVlvReqData[0] = UtInput_Vlv_ActrNormVlvReqInfo_SecdCutVlvReqData_0[i];
        Vlv_ActrNormVlvReqInfo.SecdCutVlvReqData[1] = UtInput_Vlv_ActrNormVlvReqInfo_SecdCutVlvReqData_1[i];
        Vlv_ActrNormVlvReqInfo.SecdCutVlvReqData[2] = UtInput_Vlv_ActrNormVlvReqInfo_SecdCutVlvReqData_2[i];
        Vlv_ActrNormVlvReqInfo.SecdCutVlvReqData[3] = UtInput_Vlv_ActrNormVlvReqInfo_SecdCutVlvReqData_3[i];
        Vlv_ActrNormVlvReqInfo.SecdCutVlvReqData[4] = UtInput_Vlv_ActrNormVlvReqInfo_SecdCutVlvReqData_4[i];
        Vlv_ActrNormVlvReqInfo.CircVlvReqData[0] = UtInput_Vlv_ActrNormVlvReqInfo_CircVlvReqData_0[i];
        Vlv_ActrNormVlvReqInfo.CircVlvReqData[1] = UtInput_Vlv_ActrNormVlvReqInfo_CircVlvReqData_1[i];
        Vlv_ActrNormVlvReqInfo.CircVlvReqData[2] = UtInput_Vlv_ActrNormVlvReqInfo_CircVlvReqData_2[i];
        Vlv_ActrNormVlvReqInfo.CircVlvReqData[3] = UtInput_Vlv_ActrNormVlvReqInfo_CircVlvReqData_3[i];
        Vlv_ActrNormVlvReqInfo.CircVlvReqData[4] = UtInput_Vlv_ActrNormVlvReqInfo_CircVlvReqData_4[i];
        Vlv_ActrNormVlvReqInfo.SimVlvReqData[0] = UtInput_Vlv_ActrNormVlvReqInfo_SimVlvReqData_0[i];
        Vlv_ActrNormVlvReqInfo.SimVlvReqData[1] = UtInput_Vlv_ActrNormVlvReqInfo_SimVlvReqData_1[i];
        Vlv_ActrNormVlvReqInfo.SimVlvReqData[2] = UtInput_Vlv_ActrNormVlvReqInfo_SimVlvReqData_2[i];
        Vlv_ActrNormVlvReqInfo.SimVlvReqData[3] = UtInput_Vlv_ActrNormVlvReqInfo_SimVlvReqData_3[i];
        Vlv_ActrNormVlvReqInfo.SimVlvReqData[4] = UtInput_Vlv_ActrNormVlvReqInfo_SimVlvReqData_4[i];
        Vlv_ActrVlvMonInfo.VlvCVMon = UtInput_Vlv_ActrVlvMonInfo_VlvCVMon[i];
        Vlv_ActrVlvMonInfo.VlvRLVMon = UtInput_Vlv_ActrVlvMonInfo_VlvRLVMon[i];
        Vlv_ActrVlvMonInfo.PrimCutVlvMon = UtInput_Vlv_ActrVlvMonInfo_PrimCutVlvMon[i];
        Vlv_ActrVlvMonInfo.SecdCutVlvMon = UtInput_Vlv_ActrVlvMonInfo_SecdCutVlvMon[i];
        Vlv_ActrVlvMonInfo.SimVlvMon = UtInput_Vlv_ActrVlvMonInfo_SimVlvMon[i];
        Vlv_ActrWhlVlvReqInfo.FlOvReqData[0] = UtInput_Vlv_ActrWhlVlvReqInfo_FlOvReqData_0[i];
        Vlv_ActrWhlVlvReqInfo.FlOvReqData[1] = UtInput_Vlv_ActrWhlVlvReqInfo_FlOvReqData_1[i];
        Vlv_ActrWhlVlvReqInfo.FlOvReqData[2] = UtInput_Vlv_ActrWhlVlvReqInfo_FlOvReqData_2[i];
        Vlv_ActrWhlVlvReqInfo.FlOvReqData[3] = UtInput_Vlv_ActrWhlVlvReqInfo_FlOvReqData_3[i];
        Vlv_ActrWhlVlvReqInfo.FlOvReqData[4] = UtInput_Vlv_ActrWhlVlvReqInfo_FlOvReqData_4[i];
        Vlv_ActrWhlVlvReqInfo.FlIvReqData[0] = UtInput_Vlv_ActrWhlVlvReqInfo_FlIvReqData_0[i];
        Vlv_ActrWhlVlvReqInfo.FlIvReqData[1] = UtInput_Vlv_ActrWhlVlvReqInfo_FlIvReqData_1[i];
        Vlv_ActrWhlVlvReqInfo.FlIvReqData[2] = UtInput_Vlv_ActrWhlVlvReqInfo_FlIvReqData_2[i];
        Vlv_ActrWhlVlvReqInfo.FlIvReqData[3] = UtInput_Vlv_ActrWhlVlvReqInfo_FlIvReqData_3[i];
        Vlv_ActrWhlVlvReqInfo.FlIvReqData[4] = UtInput_Vlv_ActrWhlVlvReqInfo_FlIvReqData_4[i];
        Vlv_ActrWhlVlvReqInfo.FrOvReqData[0] = UtInput_Vlv_ActrWhlVlvReqInfo_FrOvReqData_0[i];
        Vlv_ActrWhlVlvReqInfo.FrOvReqData[1] = UtInput_Vlv_ActrWhlVlvReqInfo_FrOvReqData_1[i];
        Vlv_ActrWhlVlvReqInfo.FrOvReqData[2] = UtInput_Vlv_ActrWhlVlvReqInfo_FrOvReqData_2[i];
        Vlv_ActrWhlVlvReqInfo.FrOvReqData[3] = UtInput_Vlv_ActrWhlVlvReqInfo_FrOvReqData_3[i];
        Vlv_ActrWhlVlvReqInfo.FrOvReqData[4] = UtInput_Vlv_ActrWhlVlvReqInfo_FrOvReqData_4[i];
        Vlv_ActrWhlVlvReqInfo.FrIvReqData[0] = UtInput_Vlv_ActrWhlVlvReqInfo_FrIvReqData_0[i];
        Vlv_ActrWhlVlvReqInfo.FrIvReqData[1] = UtInput_Vlv_ActrWhlVlvReqInfo_FrIvReqData_1[i];
        Vlv_ActrWhlVlvReqInfo.FrIvReqData[2] = UtInput_Vlv_ActrWhlVlvReqInfo_FrIvReqData_2[i];
        Vlv_ActrWhlVlvReqInfo.FrIvReqData[3] = UtInput_Vlv_ActrWhlVlvReqInfo_FrIvReqData_3[i];
        Vlv_ActrWhlVlvReqInfo.FrIvReqData[4] = UtInput_Vlv_ActrWhlVlvReqInfo_FrIvReqData_4[i];
        Vlv_ActrWhlVlvReqInfo.RlOvReqData[0] = UtInput_Vlv_ActrWhlVlvReqInfo_RlOvReqData_0[i];
        Vlv_ActrWhlVlvReqInfo.RlOvReqData[1] = UtInput_Vlv_ActrWhlVlvReqInfo_RlOvReqData_1[i];
        Vlv_ActrWhlVlvReqInfo.RlOvReqData[2] = UtInput_Vlv_ActrWhlVlvReqInfo_RlOvReqData_2[i];
        Vlv_ActrWhlVlvReqInfo.RlOvReqData[3] = UtInput_Vlv_ActrWhlVlvReqInfo_RlOvReqData_3[i];
        Vlv_ActrWhlVlvReqInfo.RlOvReqData[4] = UtInput_Vlv_ActrWhlVlvReqInfo_RlOvReqData_4[i];
        Vlv_ActrWhlVlvReqInfo.RlIvReqData[0] = UtInput_Vlv_ActrWhlVlvReqInfo_RlIvReqData_0[i];
        Vlv_ActrWhlVlvReqInfo.RlIvReqData[1] = UtInput_Vlv_ActrWhlVlvReqInfo_RlIvReqData_1[i];
        Vlv_ActrWhlVlvReqInfo.RlIvReqData[2] = UtInput_Vlv_ActrWhlVlvReqInfo_RlIvReqData_2[i];
        Vlv_ActrWhlVlvReqInfo.RlIvReqData[3] = UtInput_Vlv_ActrWhlVlvReqInfo_RlIvReqData_3[i];
        Vlv_ActrWhlVlvReqInfo.RlIvReqData[4] = UtInput_Vlv_ActrWhlVlvReqInfo_RlIvReqData_4[i];
        Vlv_ActrWhlVlvReqInfo.RrOvReqData[0] = UtInput_Vlv_ActrWhlVlvReqInfo_RrOvReqData_0[i];
        Vlv_ActrWhlVlvReqInfo.RrOvReqData[1] = UtInput_Vlv_ActrWhlVlvReqInfo_RrOvReqData_1[i];
        Vlv_ActrWhlVlvReqInfo.RrOvReqData[2] = UtInput_Vlv_ActrWhlVlvReqInfo_RrOvReqData_2[i];
        Vlv_ActrWhlVlvReqInfo.RrOvReqData[3] = UtInput_Vlv_ActrWhlVlvReqInfo_RrOvReqData_3[i];
        Vlv_ActrWhlVlvReqInfo.RrOvReqData[4] = UtInput_Vlv_ActrWhlVlvReqInfo_RrOvReqData_4[i];
        Vlv_ActrWhlVlvReqInfo.RrIvReqData[0] = UtInput_Vlv_ActrWhlVlvReqInfo_RrIvReqData_0[i];
        Vlv_ActrWhlVlvReqInfo.RrIvReqData[1] = UtInput_Vlv_ActrWhlVlvReqInfo_RrIvReqData_1[i];
        Vlv_ActrWhlVlvReqInfo.RrIvReqData[2] = UtInput_Vlv_ActrWhlVlvReqInfo_RrIvReqData_2[i];
        Vlv_ActrWhlVlvReqInfo.RrIvReqData[3] = UtInput_Vlv_ActrWhlVlvReqInfo_RrIvReqData_3[i];
        Vlv_ActrWhlVlvReqInfo.RrIvReqData[4] = UtInput_Vlv_ActrWhlVlvReqInfo_RrIvReqData_4[i];
        Vlv_ActrBalVlvReqInfo.PrimBalVlvReqData[0] = UtInput_Vlv_ActrBalVlvReqInfo_PrimBalVlvReqData_0[i];
        Vlv_ActrBalVlvReqInfo.PrimBalVlvReqData[1] = UtInput_Vlv_ActrBalVlvReqInfo_PrimBalVlvReqData_1[i];
        Vlv_ActrBalVlvReqInfo.PrimBalVlvReqData[2] = UtInput_Vlv_ActrBalVlvReqInfo_PrimBalVlvReqData_2[i];
        Vlv_ActrBalVlvReqInfo.PrimBalVlvReqData[3] = UtInput_Vlv_ActrBalVlvReqInfo_PrimBalVlvReqData_3[i];
        Vlv_ActrBalVlvReqInfo.PrimBalVlvReqData[4] = UtInput_Vlv_ActrBalVlvReqInfo_PrimBalVlvReqData_4[i];
        Vlv_ActrBalVlvReqInfo.PressDumpVlvReqData[0] = UtInput_Vlv_ActrBalVlvReqInfo_PressDumpVlvReqData_0[i];
        Vlv_ActrBalVlvReqInfo.PressDumpVlvReqData[1] = UtInput_Vlv_ActrBalVlvReqInfo_PressDumpVlvReqData_1[i];
        Vlv_ActrBalVlvReqInfo.PressDumpVlvReqData[2] = UtInput_Vlv_ActrBalVlvReqInfo_PressDumpVlvReqData_2[i];
        Vlv_ActrBalVlvReqInfo.PressDumpVlvReqData[3] = UtInput_Vlv_ActrBalVlvReqInfo_PressDumpVlvReqData_3[i];
        Vlv_ActrBalVlvReqInfo.PressDumpVlvReqData[4] = UtInput_Vlv_ActrBalVlvReqInfo_PressDumpVlvReqData_4[i];
        Vlv_ActrBalVlvReqInfo.ChmbBalVlvReqData[0] = UtInput_Vlv_ActrBalVlvReqInfo_ChmbBalVlvReqData_0[i];
        Vlv_ActrBalVlvReqInfo.ChmbBalVlvReqData[1] = UtInput_Vlv_ActrBalVlvReqInfo_ChmbBalVlvReqData_1[i];
        Vlv_ActrBalVlvReqInfo.ChmbBalVlvReqData[2] = UtInput_Vlv_ActrBalVlvReqInfo_ChmbBalVlvReqData_2[i];
        Vlv_ActrBalVlvReqInfo.ChmbBalVlvReqData[3] = UtInput_Vlv_ActrBalVlvReqInfo_ChmbBalVlvReqData_3[i];
        Vlv_ActrBalVlvReqInfo.ChmbBalVlvReqData[4] = UtInput_Vlv_ActrBalVlvReqInfo_ChmbBalVlvReqData_4[i];
        Vlv_ActrEcuModeSts = UtInput_Vlv_ActrEcuModeSts[i];
        Vlv_ActrAcmAsicInitCompleteFlag = UtInput_Vlv_ActrAcmAsicInitCompleteFlag[i];
        Vlv_ActrFuncInhibitVlvSts = UtInput_Vlv_ActrFuncInhibitVlvSts[i];
        Vlv_ActrVlvSync = UtInput_Vlv_ActrVlvSync[i];

        Vlv_Actr();

        TEST_ASSERT_EQUAL(Vlv_ActrNormVlvDrvInfo.PrimCutVlvDrvData, UtExpected_Vlv_ActrNormVlvDrvInfo_PrimCutVlvDrvData[i]);
        TEST_ASSERT_EQUAL(Vlv_ActrNormVlvDrvInfo.RelsVlvDrvData, UtExpected_Vlv_ActrNormVlvDrvInfo_RelsVlvDrvData[i]);
        TEST_ASSERT_EQUAL(Vlv_ActrNormVlvDrvInfo.SecdCutVlvDrvData, UtExpected_Vlv_ActrNormVlvDrvInfo_SecdCutVlvDrvData[i]);
        TEST_ASSERT_EQUAL(Vlv_ActrNormVlvDrvInfo.CircVlvDrvData, UtExpected_Vlv_ActrNormVlvDrvInfo_CircVlvDrvData[i]);
        TEST_ASSERT_EQUAL(Vlv_ActrNormVlvDrvInfo.SimVlvDrvData, UtExpected_Vlv_ActrNormVlvDrvInfo_SimVlvDrvData[i]);
        TEST_ASSERT_EQUAL(Vlv_ActrWhlVlvDrvInfo.FlOvDrvData, UtExpected_Vlv_ActrWhlVlvDrvInfo_FlOvDrvData[i]);
        TEST_ASSERT_EQUAL(Vlv_ActrWhlVlvDrvInfo.FlIvDrvData, UtExpected_Vlv_ActrWhlVlvDrvInfo_FlIvDrvData[i]);
        TEST_ASSERT_EQUAL(Vlv_ActrWhlVlvDrvInfo.FrOvDrvData, UtExpected_Vlv_ActrWhlVlvDrvInfo_FrOvDrvData[i]);
        TEST_ASSERT_EQUAL(Vlv_ActrWhlVlvDrvInfo.FrIvDrvData, UtExpected_Vlv_ActrWhlVlvDrvInfo_FrIvDrvData[i]);
        TEST_ASSERT_EQUAL(Vlv_ActrWhlVlvDrvInfo.RlOvDrvData, UtExpected_Vlv_ActrWhlVlvDrvInfo_RlOvDrvData[i]);
        TEST_ASSERT_EQUAL(Vlv_ActrWhlVlvDrvInfo.RlIvDrvData, UtExpected_Vlv_ActrWhlVlvDrvInfo_RlIvDrvData[i]);
        TEST_ASSERT_EQUAL(Vlv_ActrWhlVlvDrvInfo.RrOvDrvData, UtExpected_Vlv_ActrWhlVlvDrvInfo_RrOvDrvData[i]);
        TEST_ASSERT_EQUAL(Vlv_ActrWhlVlvDrvInfo.RrIvDrvData, UtExpected_Vlv_ActrWhlVlvDrvInfo_RrIvDrvData[i]);
        TEST_ASSERT_EQUAL(Vlv_ActrBalVlvDrvInfo.PrimBalVlvDrvData, UtExpected_Vlv_ActrBalVlvDrvInfo_PrimBalVlvDrvData[i]);
        TEST_ASSERT_EQUAL(Vlv_ActrBalVlvDrvInfo.PressDumpVlvDrvData, UtExpected_Vlv_ActrBalVlvDrvInfo_PressDumpVlvDrvData[i]);
        TEST_ASSERT_EQUAL(Vlv_ActrBalVlvDrvInfo.ChmbBalVlvDrvData, UtExpected_Vlv_ActrBalVlvDrvInfo_ChmbBalVlvDrvData[i]);
    }
}

TEST_GROUP_RUNNER(Vlv_Actr)
{
    RUN_TEST_CASE(Vlv_Actr, All);
}
