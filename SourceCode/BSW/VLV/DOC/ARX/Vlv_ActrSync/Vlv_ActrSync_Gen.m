Vlv_ActrSyncArbWhlVlvReqInfo = Simulink.Bus;
DeList={
    'FlOvReqData_array_0'
    'FlOvReqData_array_1'
    'FlOvReqData_array_2'
    'FlOvReqData_array_3'
    'FlOvReqData_array_4'
    'FlIvReqData_array_0'
    'FlIvReqData_array_1'
    'FlIvReqData_array_2'
    'FlIvReqData_array_3'
    'FlIvReqData_array_4'
    'FrOvReqData_array_0'
    'FrOvReqData_array_1'
    'FrOvReqData_array_2'
    'FrOvReqData_array_3'
    'FrOvReqData_array_4'
    'FrIvReqData_array_0'
    'FrIvReqData_array_1'
    'FrIvReqData_array_2'
    'FrIvReqData_array_3'
    'FrIvReqData_array_4'
    'RlOvReqData_array_0'
    'RlOvReqData_array_1'
    'RlOvReqData_array_2'
    'RlOvReqData_array_3'
    'RlOvReqData_array_4'
    'RlIvReqData_array_0'
    'RlIvReqData_array_1'
    'RlIvReqData_array_2'
    'RlIvReqData_array_3'
    'RlIvReqData_array_4'
    'RrOvReqData_array_0'
    'RrOvReqData_array_1'
    'RrOvReqData_array_2'
    'RrOvReqData_array_3'
    'RrOvReqData_array_4'
    'RrIvReqData_array_0'
    'RrIvReqData_array_1'
    'RrIvReqData_array_2'
    'RrIvReqData_array_3'
    'RrIvReqData_array_4'
    };
Vlv_ActrSyncArbWhlVlvReqInfo = CreateBus(Vlv_ActrSyncArbWhlVlvReqInfo, DeList);
clear DeList;

Vlv_ActrSyncArbNormVlvReqInfo = Simulink.Bus;
DeList={
    'PrimCutVlvReqData_array_0'
    'PrimCutVlvReqData_array_1'
    'PrimCutVlvReqData_array_2'
    'PrimCutVlvReqData_array_3'
    'PrimCutVlvReqData_array_4'
    'SecdCutVlvReqData_array_0'
    'SecdCutVlvReqData_array_1'
    'SecdCutVlvReqData_array_2'
    'SecdCutVlvReqData_array_3'
    'SecdCutVlvReqData_array_4'
    'SimVlvReqData_array_0'
    'SimVlvReqData_array_1'
    'SimVlvReqData_array_2'
    'SimVlvReqData_array_3'
    'SimVlvReqData_array_4'
    'RelsVlvReqData_array_0'
    'RelsVlvReqData_array_1'
    'RelsVlvReqData_array_2'
    'RelsVlvReqData_array_3'
    'RelsVlvReqData_array_4'
    'CircVlvReqData_array_0'
    'CircVlvReqData_array_1'
    'CircVlvReqData_array_2'
    'CircVlvReqData_array_3'
    'CircVlvReqData_array_4'
    'PressDumpVlvReqData_array_0'
    'PressDumpVlvReqData_array_1'
    'PressDumpVlvReqData_array_2'
    'PressDumpVlvReqData_array_3'
    'PressDumpVlvReqData_array_4'
    };
Vlv_ActrSyncArbNormVlvReqInfo = CreateBus(Vlv_ActrSyncArbNormVlvReqInfo, DeList);
clear DeList;

Vlv_ActrSyncArbBalVlvReqInfo = Simulink.Bus;
DeList={
    'BalVlvReqData_array_0'
    'BalVlvReqData_array_1'
    'BalVlvReqData_array_2'
    'BalVlvReqData_array_3'
    'BalVlvReqData_array_4'
    };
Vlv_ActrSyncArbBalVlvReqInfo = CreateBus(Vlv_ActrSyncArbBalVlvReqInfo, DeList);
clear DeList;

Vlv_ActrSyncArbResPVlvReqInfo = Simulink.Bus;
DeList={
    'ResPVlvReqData_array_0'
    'ResPVlvReqData_array_1'
    'ResPVlvReqData_array_2'
    'ResPVlvReqData_array_3'
    'ResPVlvReqData_array_4'
    };
Vlv_ActrSyncArbResPVlvReqInfo = CreateBus(Vlv_ActrSyncArbResPVlvReqInfo, DeList);
clear DeList;

Vlv_ActrSyncEcuModeSts = Simulink.Bus;
DeList={'Vlv_ActrSyncEcuModeSts'};
Vlv_ActrSyncEcuModeSts = CreateBus(Vlv_ActrSyncEcuModeSts, DeList);
clear DeList;

Vlv_ActrSyncArbVlvSync = Simulink.Bus;
DeList={'Vlv_ActrSyncArbVlvSync'};
Vlv_ActrSyncArbVlvSync = CreateBus(Vlv_ActrSyncArbVlvSync, DeList);
clear DeList;

Vlv_ActrSyncAcmAsicInitCompleteFlag = Simulink.Bus;
DeList={'Vlv_ActrSyncAcmAsicInitCompleteFlag'};
Vlv_ActrSyncAcmAsicInitCompleteFlag = CreateBus(Vlv_ActrSyncAcmAsicInitCompleteFlag, DeList);
clear DeList;

Vlv_ActrSyncFuncInhibitVlvSts = Simulink.Bus;
DeList={'Vlv_ActrSyncFuncInhibitVlvSts'};
Vlv_ActrSyncFuncInhibitVlvSts = CreateBus(Vlv_ActrSyncFuncInhibitVlvSts, DeList);
clear DeList;

Vlv_ActrSyncNormVlvReqInfo = Simulink.Bus;
DeList={
    'PrimCutVlvReqData_array_0'
    'PrimCutVlvReqData_array_1'
    'PrimCutVlvReqData_array_2'
    'PrimCutVlvReqData_array_3'
    'PrimCutVlvReqData_array_4'
    'RelsVlvReqData_array_0'
    'RelsVlvReqData_array_1'
    'RelsVlvReqData_array_2'
    'RelsVlvReqData_array_3'
    'RelsVlvReqData_array_4'
    'SecdCutVlvReqData_array_0'
    'SecdCutVlvReqData_array_1'
    'SecdCutVlvReqData_array_2'
    'SecdCutVlvReqData_array_3'
    'SecdCutVlvReqData_array_4'
    'CircVlvReqData_array_0'
    'CircVlvReqData_array_1'
    'CircVlvReqData_array_2'
    'CircVlvReqData_array_3'
    'CircVlvReqData_array_4'
    'SimVlvReqData_array_0'
    'SimVlvReqData_array_1'
    'SimVlvReqData_array_2'
    'SimVlvReqData_array_3'
    'SimVlvReqData_array_4'
    };
Vlv_ActrSyncNormVlvReqInfo = CreateBus(Vlv_ActrSyncNormVlvReqInfo, DeList);
clear DeList;

Vlv_ActrSyncNormVlvCurrInfo = Simulink.Bus;
DeList={
    'PrimCircVlv1msCurrMon'
    'SecdCirCVlv1msCurrMon'
    'PrimCutVlv1msCurrMon'
    'SecdCutVlv1msCurrMon'
    'SimVlv1msCurrMon'
    };
Vlv_ActrSyncNormVlvCurrInfo = CreateBus(Vlv_ActrSyncNormVlvCurrInfo, DeList);
clear DeList;

Vlv_ActrSyncWhlVlvReqInfo = Simulink.Bus;
DeList={
    'FlOvReqData_array_0'
    'FlOvReqData_array_1'
    'FlOvReqData_array_2'
    'FlOvReqData_array_3'
    'FlOvReqData_array_4'
    'FlIvReqData_array_0'
    'FlIvReqData_array_1'
    'FlIvReqData_array_2'
    'FlIvReqData_array_3'
    'FlIvReqData_array_4'
    'FrOvReqData_array_0'
    'FrOvReqData_array_1'
    'FrOvReqData_array_2'
    'FrOvReqData_array_3'
    'FrOvReqData_array_4'
    'FrIvReqData_array_0'
    'FrIvReqData_array_1'
    'FrIvReqData_array_2'
    'FrIvReqData_array_3'
    'FrIvReqData_array_4'
    'RlOvReqData_array_0'
    'RlOvReqData_array_1'
    'RlOvReqData_array_2'
    'RlOvReqData_array_3'
    'RlOvReqData_array_4'
    'RlIvReqData_array_0'
    'RlIvReqData_array_1'
    'RlIvReqData_array_2'
    'RlIvReqData_array_3'
    'RlIvReqData_array_4'
    'RrOvReqData_array_0'
    'RrOvReqData_array_1'
    'RrOvReqData_array_2'
    'RrOvReqData_array_3'
    'RrOvReqData_array_4'
    'RrIvReqData_array_0'
    'RrIvReqData_array_1'
    'RrIvReqData_array_2'
    'RrIvReqData_array_3'
    'RrIvReqData_array_4'
    };
Vlv_ActrSyncWhlVlvReqInfo = CreateBus(Vlv_ActrSyncWhlVlvReqInfo, DeList);
clear DeList;

Vlv_ActrSyncBalVlvReqInfo = Simulink.Bus;
DeList={
    'PrimBalVlvReqData_array_0'
    'PrimBalVlvReqData_array_1'
    'PrimBalVlvReqData_array_2'
    'PrimBalVlvReqData_array_3'
    'PrimBalVlvReqData_array_4'
    'PressDumpVlvReqData_array_0'
    'PressDumpVlvReqData_array_1'
    'PressDumpVlvReqData_array_2'
    'PressDumpVlvReqData_array_3'
    'PressDumpVlvReqData_array_4'
    'ChmbBalVlvReqData_array_0'
    'ChmbBalVlvReqData_array_1'
    'ChmbBalVlvReqData_array_2'
    'ChmbBalVlvReqData_array_3'
    'ChmbBalVlvReqData_array_4'
    };
Vlv_ActrSyncBalVlvReqInfo = CreateBus(Vlv_ActrSyncBalVlvReqInfo, DeList);
clear DeList;

Vlv_ActrSyncVlvSync = Simulink.Bus;
DeList={'Vlv_ActrSyncVlvSync'};
Vlv_ActrSyncVlvSync = CreateBus(Vlv_ActrSyncVlvSync, DeList);
clear DeList;

