Vlv_ActrNormVlvReqInfo = Simulink.Bus;
DeList={
    'PrimCutVlvReqData_array_0'
    'PrimCutVlvReqData_array_1'
    'PrimCutVlvReqData_array_2'
    'PrimCutVlvReqData_array_3'
    'PrimCutVlvReqData_array_4'
    'RelsVlvReqData_array_0'
    'RelsVlvReqData_array_1'
    'RelsVlvReqData_array_2'
    'RelsVlvReqData_array_3'
    'RelsVlvReqData_array_4'
    'SecdCutVlvReqData_array_0'
    'SecdCutVlvReqData_array_1'
    'SecdCutVlvReqData_array_2'
    'SecdCutVlvReqData_array_3'
    'SecdCutVlvReqData_array_4'
    'CircVlvReqData_array_0'
    'CircVlvReqData_array_1'
    'CircVlvReqData_array_2'
    'CircVlvReqData_array_3'
    'CircVlvReqData_array_4'
    'SimVlvReqData_array_0'
    'SimVlvReqData_array_1'
    'SimVlvReqData_array_2'
    'SimVlvReqData_array_3'
    'SimVlvReqData_array_4'
    };
Vlv_ActrNormVlvReqInfo = CreateBus(Vlv_ActrNormVlvReqInfo, DeList);
clear DeList;

Vlv_ActrVlvMonInfo = Simulink.Bus;
DeList={
    'VlvCVMon'
    'VlvRLVMon'
    'PrimCutVlvMon'
    'SecdCutVlvMon'
    'SimVlvMon'
    };
Vlv_ActrVlvMonInfo = CreateBus(Vlv_ActrVlvMonInfo, DeList);
clear DeList;

Vlv_ActrWhlVlvReqInfo = Simulink.Bus;
DeList={
    'FlOvReqData_array_0'
    'FlOvReqData_array_1'
    'FlOvReqData_array_2'
    'FlOvReqData_array_3'
    'FlOvReqData_array_4'
    'FlIvReqData_array_0'
    'FlIvReqData_array_1'
    'FlIvReqData_array_2'
    'FlIvReqData_array_3'
    'FlIvReqData_array_4'
    'FrOvReqData_array_0'
    'FrOvReqData_array_1'
    'FrOvReqData_array_2'
    'FrOvReqData_array_3'
    'FrOvReqData_array_4'
    'FrIvReqData_array_0'
    'FrIvReqData_array_1'
    'FrIvReqData_array_2'
    'FrIvReqData_array_3'
    'FrIvReqData_array_4'
    'RlOvReqData_array_0'
    'RlOvReqData_array_1'
    'RlOvReqData_array_2'
    'RlOvReqData_array_3'
    'RlOvReqData_array_4'
    'RlIvReqData_array_0'
    'RlIvReqData_array_1'
    'RlIvReqData_array_2'
    'RlIvReqData_array_3'
    'RlIvReqData_array_4'
    'RrOvReqData_array_0'
    'RrOvReqData_array_1'
    'RrOvReqData_array_2'
    'RrOvReqData_array_3'
    'RrOvReqData_array_4'
    'RrIvReqData_array_0'
    'RrIvReqData_array_1'
    'RrIvReqData_array_2'
    'RrIvReqData_array_3'
    'RrIvReqData_array_4'
    };
Vlv_ActrWhlVlvReqInfo = CreateBus(Vlv_ActrWhlVlvReqInfo, DeList);
clear DeList;

Vlv_ActrBalVlvReqInfo = Simulink.Bus;
DeList={
    'PrimBalVlvReqData_array_0'
    'PrimBalVlvReqData_array_1'
    'PrimBalVlvReqData_array_2'
    'PrimBalVlvReqData_array_3'
    'PrimBalVlvReqData_array_4'
    'PressDumpVlvReqData_array_0'
    'PressDumpVlvReqData_array_1'
    'PressDumpVlvReqData_array_2'
    'PressDumpVlvReqData_array_3'
    'PressDumpVlvReqData_array_4'
    'ChmbBalVlvReqData_array_0'
    'ChmbBalVlvReqData_array_1'
    'ChmbBalVlvReqData_array_2'
    'ChmbBalVlvReqData_array_3'
    'ChmbBalVlvReqData_array_4'
    };
Vlv_ActrBalVlvReqInfo = CreateBus(Vlv_ActrBalVlvReqInfo, DeList);
clear DeList;

Vlv_ActrEcuModeSts = Simulink.Bus;
DeList={'Vlv_ActrEcuModeSts'};
Vlv_ActrEcuModeSts = CreateBus(Vlv_ActrEcuModeSts, DeList);
clear DeList;

Vlv_ActrAcmAsicInitCompleteFlag = Simulink.Bus;
DeList={'Vlv_ActrAcmAsicInitCompleteFlag'};
Vlv_ActrAcmAsicInitCompleteFlag = CreateBus(Vlv_ActrAcmAsicInitCompleteFlag, DeList);
clear DeList;

Vlv_ActrFuncInhibitVlvSts = Simulink.Bus;
DeList={'Vlv_ActrFuncInhibitVlvSts'};
Vlv_ActrFuncInhibitVlvSts = CreateBus(Vlv_ActrFuncInhibitVlvSts, DeList);
clear DeList;

Vlv_ActrVlvSync = Simulink.Bus;
DeList={'Vlv_ActrVlvSync'};
Vlv_ActrVlvSync = CreateBus(Vlv_ActrVlvSync, DeList);
clear DeList;

Vlv_ActrNormVlvDrvInfo = Simulink.Bus;
DeList={
    'PrimCutVlvDrvData'
    'RelsVlvDrvData'
    'SecdCutVlvDrvData'
    'CircVlvDrvData'
    'SimVlvDrvData'
    };
Vlv_ActrNormVlvDrvInfo = CreateBus(Vlv_ActrNormVlvDrvInfo, DeList);
clear DeList;

Vlv_ActrWhlVlvDrvInfo = Simulink.Bus;
DeList={
    'FlOvDrvData'
    'FlIvDrvData'
    'FrOvDrvData'
    'FrIvDrvData'
    'RlOvDrvData'
    'RlIvDrvData'
    'RrOvDrvData'
    'RrIvDrvData'
    };
Vlv_ActrWhlVlvDrvInfo = CreateBus(Vlv_ActrWhlVlvDrvInfo, DeList);
clear DeList;

Vlv_ActrBalVlvDrvInfo = Simulink.Bus;
DeList={
    'PrimBalVlvDrvData'
    'PressDumpVlvDrvData'
    'ChmbBalVlvDrvData'
    };
Vlv_ActrBalVlvDrvInfo = CreateBus(Vlv_ActrBalVlvDrvInfo, DeList);
clear DeList;

