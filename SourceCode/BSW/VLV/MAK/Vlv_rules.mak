# \file
#
# \brief Vlv
#
# This file contains the implementation of the SWC
# module Vlv.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Vlv_src

Vlv_src_FILES        += $(Vlv_SRC_PATH)\Vlv_Actr.c
Vlv_src_FILES        += $(Vlv_IFA_PATH)\Vlv_Actr_Ifa.c
Vlv_src_FILES        += $(Vlv_SRC_PATH)\Vlv_ActrSync.c
Vlv_src_FILES        += $(Vlv_IFA_PATH)\Vlv_ActrSync_Ifa.c
Vlv_src_FILES        += $(Vlv_SRC_PATH)\Vlv_Actuate.c
Vlv_src_FILES        += $(Vlv_SRC_PATH)\Vlv_SenseCurrent.c
Vlv_src_FILES        += $(Vlv_SRC_PATH)\Vlv_Mediate.c
Vlv_src_FILES        += $(Vlv_CFG_PATH)\Vlv_Cfg.c
Vlv_src_FILES        += $(Vlv_CAL_PATH)\Vlv_Cal.c

ifeq ($(ICE_COMPILE),true)
Vlv_src_FILES        += $(Vlv_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	Vlv_src_FILES        += $(Vlv_UNITY_PATH)\unity.c
	Vlv_src_FILES        += $(Vlv_UNITY_PATH)\unity_fixture.c	
	Vlv_src_FILES        += $(Vlv_UT_PATH)\main.c
	Vlv_src_FILES        += $(Vlv_UT_PATH)\Vlv_Actr\Vlv_Actr_UtMain.c
	Vlv_src_FILES        += $(Vlv_UT_PATH)\Vlv_ActrSync\Vlv_ActrSync_UtMain.c
endif