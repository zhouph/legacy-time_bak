# \file
#
# \brief Vlv
#
# This file contains the implementation of the SWC
# module Vlv.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Vlv_CORE_PATH     := $(MANDO_BSW_ROOT)\Vlv
Vlv_CAL_PATH      := $(Vlv_CORE_PATH)\CAL\$(Vlv_VARIANT)
Vlv_SRC_PATH      := $(Vlv_CORE_PATH)\SRC
Vlv_CFG_PATH      := $(Vlv_CORE_PATH)\CFG\$(Vlv_VARIANT)
Vlv_HDR_PATH      := $(Vlv_CORE_PATH)\HDR
Vlv_IFA_PATH      := $(Vlv_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Vlv_CMN_PATH      := $(Vlv_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	Vlv_UT_PATH		:= $(Vlv_CORE_PATH)\UT
	Vlv_UNITY_PATH	:= $(Vlv_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(Vlv_UT_PATH)
	CC_INCLUDE_PATH		+= $(Vlv_UNITY_PATH)
	Vlv_Actr_PATH 	:= Vlv_UT_PATH\Vlv_Actr
	Vlv_ActrSync_PATH 	:= Vlv_UT_PATH\Vlv_ActrSync
endif
CC_INCLUDE_PATH    += $(Vlv_CAL_PATH)
CC_INCLUDE_PATH    += $(Vlv_SRC_PATH)
CC_INCLUDE_PATH    += $(Vlv_CFG_PATH)
CC_INCLUDE_PATH    += $(Vlv_HDR_PATH)
CC_INCLUDE_PATH    += $(Vlv_IFA_PATH)
CC_INCLUDE_PATH    += $(Vlv_CMN_PATH)

