/**
 * @defgroup Vlv_ActrSync Vlv_ActrSync
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Vlv_ActrSync.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Vlv_ActrSync.h"
#include "Vlv_ActrSync_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define SAL_WHEEL_NO_VLVDRV_SCALE   (2.25)      // resl. : duty --> I Valve_SetI : 1 mA = 0.001A
#define SAL_WHEEL_NC_VLVDRV_SCALE   (10)        // resl. : 0.1 --> 0.01  Valve_SetDuty : 0.01 %
/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define VLV_ACTRSYNC_START_SEC_CONST_UNSPECIFIED
#include "Vlv_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define VLV_ACTRSYNC_STOP_SEC_CONST_UNSPECIFIED
#include "Vlv_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define VLV_ACTRSYNC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlv_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Vlv_ActrSync_HdrBusType Vlv_ActrSyncBus;

/* Version Info */
const SwcVersionInfo_t Vlv_ActrSyncVersionInfo = 
{   
    VLV_ACTRSYNC_MODULE_ID,           /* Vlv_ActrSyncVersionInfo.ModuleId */
    VLV_ACTRSYNC_MAJOR_VERSION,       /* Vlv_ActrSyncVersionInfo.MajorVer */
    VLV_ACTRSYNC_MINOR_VERSION,       /* Vlv_ActrSyncVersionInfo.MinorVer */
    VLV_ACTRSYNC_PATCH_VERSION,       /* Vlv_ActrSyncVersionInfo.PatchVer */
    VLV_ACTRSYNC_BRANCH_VERSION       /* Vlv_ActrSyncVersionInfo.BranchVer */
};
    
/* Input Data Element */
Arbitrator_VlvArbWhlVlvReqInfo_t Vlv_ActrSyncArbWhlVlvReqInfo;
Arbitrator_VlvArbNormVlvReqInfo_t Vlv_ActrSyncArbNormVlvReqInfo;
Arbitrator_VlvArbBalVlvReqInfo_t Vlv_ActrSyncArbBalVlvReqInfo;
Arbitrator_VlvArbResPVlvReqInfo_t Vlv_ActrSyncArbResPVlvReqInfo;
Mom_HndlrEcuModeSts_t Vlv_ActrSyncEcuModeSts;
Arbitrator_VlvArbVlvSync_t Vlv_ActrSyncArbVlvSync;
Acm_MainAcmAsicInitCompleteFlag_t Vlv_ActrSyncAcmAsicInitCompleteFlag;
Eem_SuspcDetnFuncInhibitVlvSts_t Vlv_ActrSyncFuncInhibitVlvSts;

/* Output Data Element */
Vlv_ActrSyncNormVlvReqInfo_t Vlv_ActrSyncNormVlvReqInfo;
Vlv_ActrSyncNormVlvCurrInfo_t Vlv_ActrSyncNormVlvCurrInfo;
Vlv_ActrSyncWhlVlvReqInfo_t Vlv_ActrSyncWhlVlvReqInfo;
Vlv_ActrSyncBalVlvReqInfo_t Vlv_ActrSyncBalVlvReqInfo;
Vlv_ActrSyncVlvSync_t Vlv_ActrSyncVlvSync;

uint32 Vlv_ActrSync_Timer_Start;
uint32 Vlv_ActrSync_Timer_Elapsed;

#define VLV_ACTRSYNC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlv_MemMap.h"
#define VLV_ACTRSYNC_START_SEC_VAR_NOINIT_32BIT
#include "Vlv_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define VLV_ACTRSYNC_STOP_SEC_VAR_NOINIT_32BIT
#include "Vlv_MemMap.h"
#define VLV_ACTRSYNC_START_SEC_VAR_UNSPECIFIED
#include "Vlv_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define VLV_ACTRSYNC_STOP_SEC_VAR_UNSPECIFIED
#include "Vlv_MemMap.h"
#define VLV_ACTRSYNC_START_SEC_VAR_32BIT
#include "Vlv_MemMap.h"
/** Variable Section (32BIT)**/


#define VLV_ACTRSYNC_STOP_SEC_VAR_32BIT
#include "Vlv_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define VLV_ACTRSYNC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlv_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define VLV_ACTRSYNC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlv_MemMap.h"
#define VLV_ACTRSYNC_START_SEC_VAR_NOINIT_32BIT
#include "Vlv_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define VLV_ACTRSYNC_STOP_SEC_VAR_NOINIT_32BIT
#include "Vlv_MemMap.h"
#define VLV_ACTRSYNC_START_SEC_VAR_UNSPECIFIED
#include "Vlv_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define VLV_ACTRSYNC_STOP_SEC_VAR_UNSPECIFIED
#include "Vlv_MemMap.h"
#define VLV_ACTRSYNC_START_SEC_VAR_32BIT
#include "Vlv_MemMap.h"
/** Variable Section (32BIT)**/


#define VLV_ACTRSYNC_STOP_SEC_VAR_32BIT
#include "Vlv_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define VLV_ACTRSYNC_START_SEC_CODE
#include "Vlv_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Vlv_ActrSync_Init(void)
{
    uint16 i;

    /* Initialize internal bus */
    for(i=0;i<5;i++) Vlv_ActrSyncBus.Vlv_ActrSyncArbWhlVlvReqInfo.FlOvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrSyncBus.Vlv_ActrSyncArbWhlVlvReqInfo.FlIvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrSyncBus.Vlv_ActrSyncArbWhlVlvReqInfo.FrOvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrSyncBus.Vlv_ActrSyncArbWhlVlvReqInfo.FrIvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrSyncBus.Vlv_ActrSyncArbWhlVlvReqInfo.RlOvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrSyncBus.Vlv_ActrSyncArbWhlVlvReqInfo.RlIvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrSyncBus.Vlv_ActrSyncArbWhlVlvReqInfo.RrOvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrSyncBus.Vlv_ActrSyncArbWhlVlvReqInfo.RrIvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrSyncBus.Vlv_ActrSyncArbNormVlvReqInfo.PrimCutVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrSyncBus.Vlv_ActrSyncArbNormVlvReqInfo.SecdCutVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrSyncBus.Vlv_ActrSyncArbNormVlvReqInfo.SimVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrSyncBus.Vlv_ActrSyncArbNormVlvReqInfo.RelsVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrSyncBus.Vlv_ActrSyncArbNormVlvReqInfo.CircVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrSyncBus.Vlv_ActrSyncArbNormVlvReqInfo.PressDumpVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrSyncBus.Vlv_ActrSyncArbBalVlvReqInfo.BalVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrSyncBus.Vlv_ActrSyncArbResPVlvReqInfo.ResPVlvReqData[i] = 0;   
    Vlv_ActrSyncBus.Vlv_ActrSyncEcuModeSts = 0;
    Vlv_ActrSyncBus.Vlv_ActrSyncArbVlvSync = 0;
    Vlv_ActrSyncBus.Vlv_ActrSyncAcmAsicInitCompleteFlag = 0;
    Vlv_ActrSyncBus.Vlv_ActrSyncFuncInhibitVlvSts = 0;
    for(i=0;i<5;i++) Vlv_ActrSyncBus.Vlv_ActrSyncNormVlvReqInfo.PrimCutVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrSyncBus.Vlv_ActrSyncNormVlvReqInfo.RelsVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrSyncBus.Vlv_ActrSyncNormVlvReqInfo.SecdCutVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrSyncBus.Vlv_ActrSyncNormVlvReqInfo.CircVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrSyncBus.Vlv_ActrSyncNormVlvReqInfo.SimVlvReqData[i] = 0;   
    Vlv_ActrSyncBus.Vlv_ActrSyncNormVlvCurrInfo.PrimCircVlv1msCurrMon = 0;
    Vlv_ActrSyncBus.Vlv_ActrSyncNormVlvCurrInfo.SecdCirCVlv1msCurrMon = 0;
    Vlv_ActrSyncBus.Vlv_ActrSyncNormVlvCurrInfo.PrimCutVlv1msCurrMon = 0;
    Vlv_ActrSyncBus.Vlv_ActrSyncNormVlvCurrInfo.SecdCutVlv1msCurrMon = 0;
    Vlv_ActrSyncBus.Vlv_ActrSyncNormVlvCurrInfo.SimVlv1msCurrMon = 0;
    for(i=0;i<5;i++) Vlv_ActrSyncBus.Vlv_ActrSyncWhlVlvReqInfo.FlOvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrSyncBus.Vlv_ActrSyncWhlVlvReqInfo.FlIvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrSyncBus.Vlv_ActrSyncWhlVlvReqInfo.FrOvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrSyncBus.Vlv_ActrSyncWhlVlvReqInfo.FrIvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrSyncBus.Vlv_ActrSyncWhlVlvReqInfo.RlOvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrSyncBus.Vlv_ActrSyncWhlVlvReqInfo.RlIvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrSyncBus.Vlv_ActrSyncWhlVlvReqInfo.RrOvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrSyncBus.Vlv_ActrSyncWhlVlvReqInfo.RrIvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrSyncBus.Vlv_ActrSyncBalVlvReqInfo.PressDumpVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrSyncBus.Vlv_ActrSyncBalVlvReqInfo.BalVlvReqData[i] = 0;   
    Vlv_ActrSyncBus.Vlv_ActrSyncVlvSync = 0;
}

void Vlv_ActrSync(void)
{
    uint16 i;
    
    Vlv_ActrSync_Timer_Start = STM0_TIM0.U;

    /* Input */
    Vlv_ActrSync_Read_Vlv_ActrSyncArbWhlVlvReqInfo(&Vlv_ActrSyncBus.Vlv_ActrSyncArbWhlVlvReqInfo);
    /*==============================================================================
    * Members of structure Vlv_ActrSyncArbWhlVlvReqInfo 
     : Vlv_ActrSyncArbWhlVlvReqInfo.FlOvReqData;
     : Vlv_ActrSyncArbWhlVlvReqInfo.FlIvReqData;
     : Vlv_ActrSyncArbWhlVlvReqInfo.FrOvReqData;
     : Vlv_ActrSyncArbWhlVlvReqInfo.FrIvReqData;
     : Vlv_ActrSyncArbWhlVlvReqInfo.RlOvReqData;
     : Vlv_ActrSyncArbWhlVlvReqInfo.RlIvReqData;
     : Vlv_ActrSyncArbWhlVlvReqInfo.RrOvReqData;
     : Vlv_ActrSyncArbWhlVlvReqInfo.RrIvReqData;
     =============================================================================*/
    
    Vlv_ActrSync_Read_Vlv_ActrSyncArbNormVlvReqInfo(&Vlv_ActrSyncBus.Vlv_ActrSyncArbNormVlvReqInfo);
    /*==============================================================================
    * Members of structure Vlv_ActrSyncArbNormVlvReqInfo 
     : Vlv_ActrSyncArbNormVlvReqInfo.PrimCutVlvReqData;
     : Vlv_ActrSyncArbNormVlvReqInfo.SecdCutVlvReqData;
     : Vlv_ActrSyncArbNormVlvReqInfo.SimVlvReqData;
     : Vlv_ActrSyncArbNormVlvReqInfo.RelsVlvReqData;
     : Vlv_ActrSyncArbNormVlvReqInfo.CircVlvReqData;
     : Vlv_ActrSyncArbNormVlvReqInfo.PressDumpVlvReqData;
     =============================================================================*/
    
    Vlv_ActrSync_Read_Vlv_ActrSyncArbBalVlvReqInfo(&Vlv_ActrSyncBus.Vlv_ActrSyncArbBalVlvReqInfo);
    /*==============================================================================
    * Members of structure Vlv_ActrSyncArbBalVlvReqInfo 
     : Vlv_ActrSyncArbBalVlvReqInfo.BalVlvReqData;
     =============================================================================*/
    
    Vlv_ActrSync_Read_Vlv_ActrSyncArbResPVlvReqInfo(&Vlv_ActrSyncBus.Vlv_ActrSyncArbResPVlvReqInfo);
    /*==============================================================================
    * Members of structure Vlv_ActrSyncArbResPVlvReqInfo 
     : Vlv_ActrSyncArbResPVlvReqInfo.ResPVlvReqData;
     =============================================================================*/
    
    Vlv_ActrSync_Read_Vlv_ActrSyncEcuModeSts(&Vlv_ActrSyncBus.Vlv_ActrSyncEcuModeSts);
    Vlv_ActrSync_Read_Vlv_ActrSyncArbVlvSync(&Vlv_ActrSyncBus.Vlv_ActrSyncArbVlvSync);
    Vlv_ActrSync_Read_Vlv_ActrSyncAcmAsicInitCompleteFlag(&Vlv_ActrSyncBus.Vlv_ActrSyncAcmAsicInitCompleteFlag);
    Vlv_ActrSync_Read_Vlv_ActrSyncFuncInhibitVlvSts(&Vlv_ActrSyncBus.Vlv_ActrSyncFuncInhibitVlvSts);

    /* Process */
    Vlv_5msOutput_MediateRequest();
    Vlv_ActrSyncBus.Vlv_ActrSyncVlvSync ^= 1;

    /* Output */
    Vlv_ActrSync_Write_Vlv_ActrSyncNormVlvReqInfo(&Vlv_ActrSyncBus.Vlv_ActrSyncNormVlvReqInfo);
    /*==============================================================================
    * Members of structure Vlv_ActrSyncNormVlvReqInfo 
     : Vlv_ActrSyncNormVlvReqInfo.PrimCutVlvReqData;
     : Vlv_ActrSyncNormVlvReqInfo.RelsVlvReqData;
     : Vlv_ActrSyncNormVlvReqInfo.SecdCutVlvReqData;
     : Vlv_ActrSyncNormVlvReqInfo.CircVlvReqData;
     : Vlv_ActrSyncNormVlvReqInfo.SimVlvReqData;
     =============================================================================*/
    
    Vlv_ActrSync_Write_Vlv_ActrSyncNormVlvCurrInfo(&Vlv_ActrSyncBus.Vlv_ActrSyncNormVlvCurrInfo);
    /*==============================================================================
    * Members of structure Vlv_ActrSyncNormVlvCurrInfo 
     : Vlv_ActrSyncNormVlvCurrInfo.PrimCircVlv1msCurrMon;
     : Vlv_ActrSyncNormVlvCurrInfo.SecdCirCVlv1msCurrMon;
     : Vlv_ActrSyncNormVlvCurrInfo.PrimCutVlv1msCurrMon;
     : Vlv_ActrSyncNormVlvCurrInfo.SecdCutVlv1msCurrMon;
     : Vlv_ActrSyncNormVlvCurrInfo.SimVlv1msCurrMon;
     =============================================================================*/
    
    Vlv_ActrSync_Write_Vlv_ActrSyncWhlVlvReqInfo(&Vlv_ActrSyncBus.Vlv_ActrSyncWhlVlvReqInfo);
    /*==============================================================================
    * Members of structure Vlv_ActrSyncWhlVlvReqInfo 
     : Vlv_ActrSyncWhlVlvReqInfo.FlOvReqData;
     : Vlv_ActrSyncWhlVlvReqInfo.FlIvReqData;
     : Vlv_ActrSyncWhlVlvReqInfo.FrOvReqData;
     : Vlv_ActrSyncWhlVlvReqInfo.FrIvReqData;
     : Vlv_ActrSyncWhlVlvReqInfo.RlOvReqData;
     : Vlv_ActrSyncWhlVlvReqInfo.RlIvReqData;
     : Vlv_ActrSyncWhlVlvReqInfo.RrOvReqData;
     : Vlv_ActrSyncWhlVlvReqInfo.RrIvReqData;
     =============================================================================*/
    
    Vlv_ActrSync_Write_Vlv_ActrSyncBalVlvReqInfo(&Vlv_ActrSyncBus.Vlv_ActrSyncBalVlvReqInfo);
    /*==============================================================================
    * Members of structure Vlv_ActrSyncBalVlvReqInfo 
     : Vlv_ActrSyncBalVlvReqInfo.PressDumpVlvReqData;
     : Vlv_ActrSyncBalVlvReqInfo.BalVlvReqData;
     =============================================================================*/
    
    Vlv_ActrSync_Write_Vlv_ActrSyncVlvSync(&Vlv_ActrSyncBus.Vlv_ActrSyncVlvSync);

    Vlv_ActrSync_Timer_Elapsed = STM0_TIM0.U - Vlv_ActrSync_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define VLV_ACTRSYNC_STOP_SEC_CODE
#include "Vlv_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
