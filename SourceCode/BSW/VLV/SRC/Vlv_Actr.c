/**
 * @defgroup Vlv_Actr Vlv_Actr
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Vlv_Actr.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Vlv_Actr.h"
#include "Vlv_Actr_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define VLV_ACTR_START_SEC_CONST_UNSPECIFIED
#include "Vlv_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define VLV_ACTR_STOP_SEC_CONST_UNSPECIFIED
#include "Vlv_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define VLV_ACTR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlv_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Vlv_Actr_HdrBusType Vlv_ActrBus;

/* Valve Test variable */

uint16 T32_Force_ASIC_TC1_BAL_VLV_ReqData;
uint16 T32_Force_ASIC_TC2_PD_VLV_ReqData;

uint16 T32_Force_ASIC_NC1_FRNC_VLV_ReqData;
uint16 T32_Force_ASIC_NC2_RRNC_VLV_ReqData;
uint16 T32_Force_ASIC_NC3_FLNC_VLV_ReqData;
uint16 T32_Force_ASIC_NC4_RLNC_VLV_ReqData;

uint16 T32_Force_ASIC_NO1_RLNO_VLV_ReqData;
uint16 T32_Force_ASIC_NO2_RRNO_VLV_ReqData;
uint16 T32_Force_ASIC_NO3_FRNO_VLV_ReqData;
uint16 T32_Force_ASIC_NO4_FLNO_VLV_ReqData;

uint16 T32_Force_MCU_GDCh0_CV_Vlv_ReqData;
uint16 T32_Force_MCU_GDCh1_RLV_Vlv_ReqData;
uint16 T32_Force_MCU_GDCh2_CutS_Vlv_ReqData;
uint16 T32_Force_MCU_GDCh3_CutP_Vlv_ReqData;
uint16 T32_Force_MCU_GDCh4_Sim_Vlv_ReqData;
uint16 T32_Force_MCU_GDCh5;

uint16 T32_Force_MCU_GDCh0_CV_Vlv_FeedBackMon;
uint16 T32_Force_MCU_GDCh1_RLV_Vlv_FeedBackMon;
uint16 T32_Force_MCU_GDCh2_CutS_Vlv_FeedBackMon;
uint16 T32_Force_MCU_GDCh3_CutP_Vlv_FeedBackMon;
uint16 T32_Force_MCU_GDCh4_Sim_Vlv_FeedBackMon;
uint16 T32_Force_MCU_GDCh5_FeedBackMon;

/* Version Info */
const SwcVersionInfo_t Vlv_ActrVersionInfo = 
{   
    VLV_ACTR_MODULE_ID,           /* Vlv_ActrVersionInfo.ModuleId */
    VLV_ACTR_MAJOR_VERSION,       /* Vlv_ActrVersionInfo.MajorVer */
    VLV_ACTR_MINOR_VERSION,       /* Vlv_ActrVersionInfo.MinorVer */
    VLV_ACTR_PATCH_VERSION,       /* Vlv_ActrVersionInfo.PatchVer */
    VLV_ACTR_BRANCH_VERSION       /* Vlv_ActrVersionInfo.BranchVer */
};
    
/* Input Data Element */
Vlv_ActrSyncNormVlvReqInfo_t Vlv_ActrNormVlvReqInfo;
Ioc_InputSR1msVlvMonInfo_t Vlv_ActrVlvMonInfo;
Vlv_ActrSyncWhlVlvReqInfo_t Vlv_ActrWhlVlvReqInfo;
Vlv_ActrSyncBalVlvReqInfo_t Vlv_ActrBalVlvReqInfo;
Mom_HndlrEcuModeSts_t Vlv_ActrEcuModeSts;
Acm_MainAcmAsicInitCompleteFlag_t Vlv_ActrAcmAsicInitCompleteFlag;
Eem_SuspcDetnFuncInhibitVlvSts_t Vlv_ActrFuncInhibitVlvSts;
Vlv_ActrSyncVlvSync_t Vlv_ActrVlvSync;

/* Output Data Element */
Vlv_ActrNormVlvDrvInfo_t Vlv_ActrNormVlvDrvInfo;
Vlv_ActrWhlVlvDrvInfo_t Vlv_ActrWhlVlvDrvInfo;
Vlv_ActrBalVlvDrvInfo_t Vlv_ActrBalVlvDrvInfo;

uint32 Vlv_Actr_Timer_Start;
uint32 Vlv_Actr_Timer_Elapsed;

#define VLV_ACTR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlv_MemMap.h"
#define VLV_ACTR_START_SEC_VAR_NOINIT_32BIT
#include "Vlv_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define VLV_ACTR_STOP_SEC_VAR_NOINIT_32BIT
#include "Vlv_MemMap.h"
#define VLV_ACTR_START_SEC_VAR_UNSPECIFIED
#include "Vlv_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define VLV_ACTR_STOP_SEC_VAR_UNSPECIFIED
#include "Vlv_MemMap.h"
#define VLV_ACTR_START_SEC_VAR_32BIT
#include "Vlv_MemMap.h"
/** Variable Section (32BIT)**/


#define VLV_ACTR_STOP_SEC_VAR_32BIT
#include "Vlv_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define VLV_ACTR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlv_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define VLV_ACTR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlv_MemMap.h"
#define VLV_ACTR_START_SEC_VAR_NOINIT_32BIT
#include "Vlv_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define VLV_ACTR_STOP_SEC_VAR_NOINIT_32BIT
#include "Vlv_MemMap.h"
#define VLV_ACTR_START_SEC_VAR_UNSPECIFIED
#include "Vlv_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define VLV_ACTR_STOP_SEC_VAR_UNSPECIFIED
#include "Vlv_MemMap.h"
#define VLV_ACTR_START_SEC_VAR_32BIT
#include "Vlv_MemMap.h"
/** Variable Section (32BIT)**/


#define VLV_ACTR_STOP_SEC_VAR_32BIT
#include "Vlv_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define VLV_ACTR_START_SEC_CODE
#include "Vlv_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Vlv_Actr_Init(void)
{
    uint16 i;

    /* Initialize internal bus */
    for(i=0;i<5;i++) Vlv_ActrBus.Vlv_ActrNormVlvReqInfo.PrimCutVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrBus.Vlv_ActrNormVlvReqInfo.RelsVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrBus.Vlv_ActrNormVlvReqInfo.SecdCutVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrBus.Vlv_ActrNormVlvReqInfo.CircVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrBus.Vlv_ActrNormVlvReqInfo.SimVlvReqData[i] = 0;   
    Vlv_ActrBus.Vlv_ActrVlvMonInfo.VlvCVMon = 0;
    Vlv_ActrBus.Vlv_ActrVlvMonInfo.VlvRLVMon = 0;
    Vlv_ActrBus.Vlv_ActrVlvMonInfo.PrimCutVlvMon = 0;
    Vlv_ActrBus.Vlv_ActrVlvMonInfo.SecdCutVlvMon = 0;
    Vlv_ActrBus.Vlv_ActrVlvMonInfo.SimVlvMon = 0;
    for(i=0;i<5;i++) Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.FlOvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.FlIvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.FrOvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.FrIvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.RlOvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.RlIvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.RrOvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.RrIvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrBus.Vlv_ActrBalVlvReqInfo.PressDumpVlvReqData[i] = 0;   
    for(i=0;i<5;i++) Vlv_ActrBus.Vlv_ActrBalVlvReqInfo.BalVlvReqData[i] = 0;   
    Vlv_ActrBus.Vlv_ActrEcuModeSts = 0;
    Vlv_ActrBus.Vlv_ActrAcmAsicInitCompleteFlag = 0;
    Vlv_ActrBus.Vlv_ActrFuncInhibitVlvSts = 0;
    Vlv_ActrBus.Vlv_ActrVlvSync = 0;
    Vlv_ActrBus.Vlv_ActrNormVlvDrvInfo.PrimCutVlvDrvData = 0;
    Vlv_ActrBus.Vlv_ActrNormVlvDrvInfo.RelsVlvDrvData = 0;
    Vlv_ActrBus.Vlv_ActrNormVlvDrvInfo.SecdCutVlvDrvData = 0;
    Vlv_ActrBus.Vlv_ActrNormVlvDrvInfo.CircVlvDrvData = 0;
    Vlv_ActrBus.Vlv_ActrNormVlvDrvInfo.SimVlvDrvData = 0;
    Vlv_ActrBus.Vlv_ActrWhlVlvDrvInfo.FlOvDrvData = 0;
    Vlv_ActrBus.Vlv_ActrWhlVlvDrvInfo.FlIvDrvData = 0;
    Vlv_ActrBus.Vlv_ActrWhlVlvDrvInfo.FrOvDrvData = 0;
    Vlv_ActrBus.Vlv_ActrWhlVlvDrvInfo.FrIvDrvData = 0;
    Vlv_ActrBus.Vlv_ActrWhlVlvDrvInfo.RlOvDrvData = 0;
    Vlv_ActrBus.Vlv_ActrWhlVlvDrvInfo.RlIvDrvData = 0;
    Vlv_ActrBus.Vlv_ActrWhlVlvDrvInfo.RrOvDrvData = 0;
    Vlv_ActrBus.Vlv_ActrWhlVlvDrvInfo.RrIvDrvData = 0;
    Vlv_ActrBus.Vlv_ActrBalVlvDrvInfo.PressDumpVlvDrvData = 0;
    Vlv_ActrBus.Vlv_ActrBalVlvDrvInfo.BalVlvDrvData = 0;

    Vlv_1ms_PidInit();
}

void Vlv_Actr(void)
{
    uint16 i;
    
    Vlv_Actr_Timer_Start = STM0_TIM0.U;

    /* Input */
    Vlv_Actr_Read_Vlv_ActrNormVlvReqInfo(&Vlv_ActrBus.Vlv_ActrNormVlvReqInfo);
    /*==============================================================================
    * Members of structure Vlv_ActrNormVlvReqInfo 
     : Vlv_ActrNormVlvReqInfo.PrimCutVlvReqData;
     : Vlv_ActrNormVlvReqInfo.RelsVlvReqData;
     : Vlv_ActrNormVlvReqInfo.SecdCutVlvReqData;
     : Vlv_ActrNormVlvReqInfo.CircVlvReqData;
     : Vlv_ActrNormVlvReqInfo.SimVlvReqData;
     =============================================================================*/
    
    Vlv_Actr_Read_Vlv_ActrVlvMonInfo(&Vlv_ActrBus.Vlv_ActrVlvMonInfo);
    /*==============================================================================
    * Members of structure Vlv_ActrVlvMonInfo 
     : Vlv_ActrVlvMonInfo.VlvCVMon;
     : Vlv_ActrVlvMonInfo.VlvRLVMon;
     : Vlv_ActrVlvMonInfo.PrimCutVlvMon;
     : Vlv_ActrVlvMonInfo.SecdCutVlvMon;
     : Vlv_ActrVlvMonInfo.SimVlvMon;
     =============================================================================*/
    
    Vlv_Actr_Read_Vlv_ActrWhlVlvReqInfo(&Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo);
    /*==============================================================================
    * Members of structure Vlv_ActrWhlVlvReqInfo 
     : Vlv_ActrWhlVlvReqInfo.FlOvReqData;
     : Vlv_ActrWhlVlvReqInfo.FlIvReqData;
     : Vlv_ActrWhlVlvReqInfo.FrOvReqData;
     : Vlv_ActrWhlVlvReqInfo.FrIvReqData;
     : Vlv_ActrWhlVlvReqInfo.RlOvReqData;
     : Vlv_ActrWhlVlvReqInfo.RlIvReqData;
     : Vlv_ActrWhlVlvReqInfo.RrOvReqData;
     : Vlv_ActrWhlVlvReqInfo.RrIvReqData;
     =============================================================================*/
    
    Vlv_Actr_Read_Vlv_ActrBalVlvReqInfo(&Vlv_ActrBus.Vlv_ActrBalVlvReqInfo);
    /*==============================================================================
    * Members of structure Vlv_ActrBalVlvReqInfo 
     : Vlv_ActrBalVlvReqInfo.PressDumpVlvReqData;
     : Vlv_ActrBalVlvReqInfo.BalVlvReqData;
     =============================================================================*/
    
    Vlv_Actr_Read_Vlv_ActrEcuModeSts(&Vlv_ActrBus.Vlv_ActrEcuModeSts);
    Vlv_Actr_Read_Vlv_ActrAcmAsicInitCompleteFlag(&Vlv_ActrBus.Vlv_ActrAcmAsicInitCompleteFlag);
    Vlv_Actr_Read_Vlv_ActrFuncInhibitVlvSts(&Vlv_ActrBus.Vlv_ActrFuncInhibitVlvSts);
    Vlv_Actr_Read_Vlv_ActrVlvSync(&Vlv_ActrBus.Vlv_ActrVlvSync);
#if 0
	for(i=0;i<5;i++) Vlv_ActrBus.Vlv_ActrNormVlvReqInfo.PrimCutVlvReqData[i] 	= T32_Force_MCU_GDCh3_CutP_Vlv_ReqData;
	for(i=0;i<5;i++) Vlv_ActrBus.Vlv_ActrNormVlvReqInfo.PrimCircVlvReqData[i] 	= T32_Force_MCU_GDCh1_RLV_Vlv_ReqData; 
	for(i=0;i<5;i++) Vlv_ActrBus.Vlv_ActrNormVlvReqInfo.SecdCutVlvReqData[i] 	= T32_Force_MCU_GDCh2_CutS_Vlv_ReqData;
	for(i=0;i<5;i++) Vlv_ActrBus.Vlv_ActrNormVlvReqInfo.SecdCircVlvReqData[i] 	= T32_Force_MCU_GDCh0_CV_Vlv_ReqData;
	for(i=0;i<5;i++) Vlv_ActrBus.Vlv_ActrNormVlvReqInfo.SimVlvReqData[i] 		= T32_Force_MCU_GDCh4_Sim_Vlv_ReqData;

    for(i=0;i<5;i++) Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.FlOvReqData[i] = T32_Force_ASIC_NC3_FLNC_VLV_ReqData;
    for(i=0;i<5;i++) Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.FrOvReqData[i] = T32_Force_ASIC_NC1_FRNC_VLV_ReqData;
    for(i=0;i<5;i++) Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.RlOvReqData[i] = T32_Force_ASIC_NC4_RLNC_VLV_ReqData;
    for(i=0;i<5;i++) Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.RrOvReqData[i] = T32_Force_ASIC_NC2_RRNC_VLV_ReqData;

    for(i=0;i<5;i++) Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.FlIvReqData[i] = T32_Force_ASIC_NO4_FLNO_VLV_ReqData;
	for(i=0;i<5;i++) Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.FrIvReqData[i] = T32_Force_ASIC_NO3_FRNO_VLV_ReqData;
    for(i=0;i<5;i++) Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.RlIvReqData[i] = T32_Force_ASIC_NO1_RLNO_VLV_ReqData;
    for(i=0;i<5;i++) Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.RrIvReqData[i] = T32_Force_ASIC_NO2_RRNO_VLV_ReqData;

	for(i=0;i<5;i++) Vlv_ActrBus.Vlv_ActrBalVlvReqInfo.SecdBalVlvReqData[i] = T32_Force_ASIC_TC2_PD_VLV_ReqData;
	for(i=0;i<5;i++) Vlv_ActrBus.Vlv_ActrBalVlvReqInfo.ChmbBalVlvReqData[i] = T32_Force_ASIC_TC1_BAL_VLV_ReqData;

	T32_Force_MCU_GDCh0_CV_Vlv_FeedBackMon 	= Vlv_ActrBus.Vlv_ActrVlvMonInfo.VlvCVMon; 	/* [ NZ Quick C: CV ] */
	T32_Force_MCU_GDCh1_RLV_Vlv_FeedBackMon = Vlv_ActrBus.Vlv_ActrVlvMonInfo.VlvRLVMon;	/* [ NZ Quick C: RLV ] */
	T32_Force_MCU_GDCh2_CutS_Vlv_FeedBackMon = Vlv_ActrBus.Vlv_ActrVlvMonInfo.SecdCutVlvMon;
	T32_Force_MCU_GDCh3_CutP_Vlv_FeedBackMon = Vlv_ActrBus.Vlv_ActrVlvMonInfo.PrimCutVlvMon;
	T32_Force_MCU_GDCh4_Sim_Vlv_FeedBackMon = Vlv_ActrBus.Vlv_ActrVlvMonInfo.SimVlvMon;
	T32_Force_MCU_GDCh5_FeedBackMon;
#endif 	 
    /* Process */
    Vlv_1msOutput_SenseCurrent();
    Vlv_1ms_Actuate();

    /* Output */
    Vlv_Actr_Write_Vlv_ActrNormVlvDrvInfo(&Vlv_ActrBus.Vlv_ActrNormVlvDrvInfo);
    /*==============================================================================
    * Members of structure Vlv_ActrNormVlvDrvInfo 
     : Vlv_ActrNormVlvDrvInfo.PrimCutVlvDrvData;
     : Vlv_ActrNormVlvDrvInfo.RelsVlvDrvData;
     : Vlv_ActrNormVlvDrvInfo.SecdCutVlvDrvData;
     : Vlv_ActrNormVlvDrvInfo.CircVlvDrvData;
     : Vlv_ActrNormVlvDrvInfo.SimVlvDrvData;
     =============================================================================*/
    
    Vlv_Actr_Write_Vlv_ActrWhlVlvDrvInfo(&Vlv_ActrBus.Vlv_ActrWhlVlvDrvInfo);
    /*==============================================================================
    * Members of structure Vlv_ActrWhlVlvDrvInfo 
     : Vlv_ActrWhlVlvDrvInfo.FlOvDrvData;
     : Vlv_ActrWhlVlvDrvInfo.FlIvDrvData;
     : Vlv_ActrWhlVlvDrvInfo.FrOvDrvData;
     : Vlv_ActrWhlVlvDrvInfo.FrIvDrvData;
     : Vlv_ActrWhlVlvDrvInfo.RlOvDrvData;
     : Vlv_ActrWhlVlvDrvInfo.RlIvDrvData;
     : Vlv_ActrWhlVlvDrvInfo.RrOvDrvData;
     : Vlv_ActrWhlVlvDrvInfo.RrIvDrvData;
     =============================================================================*/
    
    Vlv_Actr_Write_Vlv_ActrBalVlvDrvInfo(&Vlv_ActrBus.Vlv_ActrBalVlvDrvInfo);
    /*==============================================================================
    * Members of structure Vlv_ActrBalVlvDrvInfo 
     : Vlv_ActrBalVlvDrvInfo.PressDumpVlvDrvData;
     : Vlv_ActrBalVlvDrvInfo.BalVlvDrvData;
     =============================================================================*/
    

    Vlv_Actr_Timer_Elapsed = STM0_TIM0.U - Vlv_Actr_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define VLV_ACTR_STOP_SEC_CODE
#include "Vlv_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
