/**
 * @defgroup Vlv_1ms Vlv_1ms
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Vlv_1ms.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Vlv_Actr.h"
#include "Vlv_Actr_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
/* Macro for Unused parameters*/
 #define UNUSED_PARAMETER(VariableName)   if((VariableName) != 0U)\
                                          {/* Do Nothing */}

#define PWM_FREQ_16KHz                     (1250)
#define VALVE_PWM_FREQ                      PWM_FREQ_16KHz
#define VALVE_CURRENT_UPLIMIT               ((VALVE_PWM_FREQ*95)/100)
#define VALVE_CURRENT_LOWLIMIT              0

#define TWO_PWR_15      32768
#define mult_r(a, b)    ((sint16)(((sint32)((Frac16)(a)) * (sint32)((sint16)(b))) / TWO_PWR_15))

#define S16_MAX         (32767)
#define S16_MIN         (-32767)

//#define FRAC16(x)          ((Frac16)((x) < 0.999969482421875 ? ((x) >= -1 ? (x)*0x8000 : 0x8000) : 0x7FFF))

typedef short int           Frac16;
typedef signed long         Frac32;

typedef struct
{
    uint16    Valve_ActPWM;

    struct
    {
        sint16     s16Kp;
        Frac16     f16Kp;
        
        sint16     s16KiTs;
        Frac16     f16KiTs;
        sint16     s16Kb;
        Frac16     f16Kb;
        
        sint16     s16Kd;
        Frac16     f16Kd;
        sint16     s16fs;
        sint16     s16wc;
        
        sint16     s16uOutMax;
        sint16     s16uOutMin;
        
        sint32     s32uP;
        sint32     s32uI;
        sint32     s32uD;
        sint16     s16uDifference;
        sint16     s16errOld;
        sint16     s16errDotFiltOld;
        sint16     s16uOut;
    }PMPID;
}VALVE_PIDPARA;

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define VLV_ACTR_START_SEC_CONST_UNSPECIFIED
#include "Vlv_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define VLV_ACTR_STOP_SEC_CONST_UNSPECIFIED
#include "Vlv_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define VLV_ACTR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlv_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

#define VLV_ACTR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlv_MemMap.h"
#define VLV_ACTR_START_SEC_VAR_NOINIT_32BIT
#include "Vlv_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define VLV_ACTR_STOP_SEC_VAR_NOINIT_32BIT
#include "Vlv_MemMap.h"
#define VLV_ACTR_START_SEC_VAR_UNSPECIFIED
#include "Vlv_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define VLV_ACTR_STOP_SEC_VAR_UNSPECIFIED
#include "Vlv_MemMap.h"
#define VLV_ACTR_START_SEC_VAR_32BIT
#include "Vlv_MemMap.h"
/** Variable Section (32BIT)**/


#define VLV_ACTR_STOP_SEC_VAR_32BIT
#include "Vlv_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define VLV_ACTR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlv_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
VALVE_PIDPARA   wsPID_para[VLV_PORT_MAX_NUM];

#define VLV_ACTR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlv_MemMap.h"
#define VLV_ACTR_START_SEC_VAR_NOINIT_32BIT
#include "Vlv_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define VLV_ACTR_STOP_SEC_VAR_NOINIT_32BIT
#include "Vlv_MemMap.h"
#define VLV_ACTR_START_SEC_VAR_UNSPECIFIED
#include "Vlv_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define VLV_ACTR_STOP_SEC_VAR_UNSPECIFIED
#include "Vlv_MemMap.h"
#define VLV_ACTR_START_SEC_VAR_32BIT
#include "Vlv_MemMap.h"
/** Variable Section (32BIT)**/


#define VLV_ACTR_STOP_SEC_VAR_32BIT
#include "Vlv_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define VLV_ACTR_START_SEC_CODE
#include "Vlv_MemMap.h"
static Vlv_DataType Vlv_PidCurrentControl(Vlv_DataType targetCurrent, Vlv_DataType feedbackCurrent, Vlv_PortType port);
static Vlv_DataType Vlv_IDBs16PIDcontroller(Vlv_DataType xRef, Vlv_DataType x, Vlv_PortType port);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Vlv_1ms_PidInit(void)
{
    /* Initialization for P-gain parameter */
    wsPID_para[VLV_PORT_SIM].PMPID.s16Kp       = Vlv_ActrConfig.SimCfg.PgainInt;
    wsPID_para[VLV_PORT_SIM].PMPID.f16Kp       = Vlv_ActrConfig.SimCfg.PgainFloat;
    /* Initialization for I-gain parameter */
    wsPID_para[VLV_PORT_SIM].PMPID.s16KiTs     = Vlv_ActrConfig.SimCfg.IgainTsInt;
    wsPID_para[VLV_PORT_SIM].PMPID.f16KiTs     = Vlv_ActrConfig.SimCfg.IgainTsFloat;
    /* Initialization for D-gain parameter */
    wsPID_para[VLV_PORT_SIM].PMPID.s16Kd       = Vlv_ActrConfig.SimCfg.DgainInt;
    wsPID_para[VLV_PORT_SIM].PMPID.f16Kd       = Vlv_ActrConfig.SimCfg.DgainFloat;
    /* Initialization for BackCalcuation parameter */
    wsPID_para[VLV_PORT_SIM].PMPID.s16Kb       = Vlv_ActrConfig.SimCfg.BackCalcValInt;
    wsPID_para[VLV_PORT_SIM].PMPID.f16Kb       = Vlv_ActrConfig.SimCfg.BackCalcValFloat;
    /* Initialization for Min/max limitation value */
    wsPID_para[VLV_PORT_SIM].PMPID.s16uOutMax  = VALVE_CURRENT_UPLIMIT;
    wsPID_para[VLV_PORT_SIM].PMPID.s16uOutMin  = VALVE_CURRENT_LOWLIMIT;

    /* Initialization for P-gain parameter */
    wsPID_para[VLV_PORT_CUT1].PMPID.s16Kp       = Vlv_ActrConfig.PrimCutCfg.PgainInt;
    wsPID_para[VLV_PORT_CUT1].PMPID.f16Kp       = Vlv_ActrConfig.PrimCutCfg.PgainFloat;
    /* Initialization for I-gain parameter */
    wsPID_para[VLV_PORT_CUT1].PMPID.s16KiTs     = Vlv_ActrConfig.PrimCutCfg.IgainTsInt;
    wsPID_para[VLV_PORT_CUT1].PMPID.f16KiTs     = Vlv_ActrConfig.PrimCutCfg.IgainTsFloat;
    /* Initialization for D-gain parameter */
    wsPID_para[VLV_PORT_CUT1].PMPID.s16Kd       = Vlv_ActrConfig.PrimCutCfg.DgainInt;
    wsPID_para[VLV_PORT_CUT1].PMPID.f16Kd       = Vlv_ActrConfig.PrimCutCfg.DgainFloat;
    /* Initialization for BackCalcuation parameter */
    wsPID_para[VLV_PORT_CUT1].PMPID.s16Kb       = Vlv_ActrConfig.PrimCutCfg.BackCalcValInt;
    wsPID_para[VLV_PORT_CUT1].PMPID.f16Kb       = Vlv_ActrConfig.PrimCutCfg.BackCalcValFloat;
    /* Initialization for Min/max limitation value */
    wsPID_para[VLV_PORT_CUT1].PMPID.s16uOutMax  = VALVE_CURRENT_UPLIMIT;
    wsPID_para[VLV_PORT_CUT1].PMPID.s16uOutMin  = VALVE_CURRENT_LOWLIMIT;

    /* Initialization for P-gain parameter */
    wsPID_para[VLV_PORT_CUT2].PMPID.s16Kp       = Vlv_ActrConfig.SecdCutCfg.PgainInt;
    wsPID_para[VLV_PORT_CUT2].PMPID.f16Kp       = Vlv_ActrConfig.SecdCutCfg.PgainFloat;
    /* Initialization for I-gain parameter */
    wsPID_para[VLV_PORT_CUT2].PMPID.s16KiTs     = Vlv_ActrConfig.SecdCutCfg.IgainTsInt;
    wsPID_para[VLV_PORT_CUT2].PMPID.f16KiTs     = Vlv_ActrConfig.SecdCutCfg.IgainTsFloat;
    /* Initialization for D-gain parameter */
    wsPID_para[VLV_PORT_CUT2].PMPID.s16Kd       = Vlv_ActrConfig.SecdCutCfg.DgainInt;
    wsPID_para[VLV_PORT_CUT2].PMPID.f16Kd       = Vlv_ActrConfig.SecdCutCfg.DgainFloat;
    /* Initialization for BackCalcuation parameter */
    wsPID_para[VLV_PORT_CUT2].PMPID.s16Kb       = Vlv_ActrConfig.SecdCutCfg.BackCalcValInt;
    wsPID_para[VLV_PORT_CUT2].PMPID.f16Kb       = Vlv_ActrConfig.SecdCutCfg.BackCalcValFloat;
    /* Initialization for Min/max limitation value */
    wsPID_para[VLV_PORT_CUT2].PMPID.s16uOutMax  = VALVE_CURRENT_UPLIMIT;
    wsPID_para[VLV_PORT_CUT2].PMPID.s16uOutMin  = VALVE_CURRENT_LOWLIMIT;

    /* Initialization for P-gain parameter */ /* IDB 15NZ ECU C RLV --> PrimeCircuit */
    wsPID_para[VLV_PORT_RLV].PMPID.s16Kp       = Vlv_ActrConfig.ReslvCfg.PgainInt;
    wsPID_para[VLV_PORT_RLV].PMPID.f16Kp       = Vlv_ActrConfig.ReslvCfg.PgainFloat;
    /* Initialization for I-gain parameter */
    wsPID_para[VLV_PORT_RLV].PMPID.s16KiTs     = Vlv_ActrConfig.ReslvCfg.IgainTsInt;
    wsPID_para[VLV_PORT_RLV].PMPID.f16KiTs     = Vlv_ActrConfig.ReslvCfg.IgainTsFloat;
    /* Initialization for D-gain parameter */
    wsPID_para[VLV_PORT_RLV].PMPID.s16Kd       = Vlv_ActrConfig.ReslvCfg.DgainInt;
    wsPID_para[VLV_PORT_RLV].PMPID.f16Kd       = Vlv_ActrConfig.ReslvCfg.DgainFloat;
    /* Initialization for BackCalcuation parameter */
    wsPID_para[VLV_PORT_RLV].PMPID.s16Kb       = Vlv_ActrConfig.ReslvCfg.BackCalcValInt;
    wsPID_para[VLV_PORT_RLV].PMPID.f16Kb       = Vlv_ActrConfig.ReslvCfg.BackCalcValFloat;
    /* Initialization for Min/max limitation value */
    wsPID_para[VLV_PORT_RLV].PMPID.s16uOutMax  = VALVE_CURRENT_UPLIMIT;
    wsPID_para[VLV_PORT_RLV].PMPID.s16uOutMin  = VALVE_CURRENT_LOWLIMIT;

    /* Initialization for P-gain parameter */ /* IDB 15NZ ECU C CV --> SecdCircuit */
    wsPID_para[VLV_PORT_CV].PMPID.s16Kp       = Vlv_ActrConfig.CircvCfg.PgainInt;
    wsPID_para[VLV_PORT_CV].PMPID.f16Kp       = Vlv_ActrConfig.CircvCfg.PgainFloat;
    /* Initialization for I-gain parameter */
    wsPID_para[VLV_PORT_CV].PMPID.s16KiTs     = Vlv_ActrConfig.CircvCfg.IgainTsInt;
    wsPID_para[VLV_PORT_CV].PMPID.f16KiTs     = Vlv_ActrConfig.CircvCfg.IgainTsFloat;
    /* Initialization for D-gain parameter */
    wsPID_para[VLV_PORT_CV].PMPID.s16Kd       = Vlv_ActrConfig.CircvCfg.DgainInt;
    wsPID_para[VLV_PORT_CV].PMPID.f16Kd       = Vlv_ActrConfig.CircvCfg.DgainFloat;
    /* Initialization for BackCalcuation parameter */
    wsPID_para[VLV_PORT_CV].PMPID.s16Kb       = Vlv_ActrConfig.CircvCfg.BackCalcValInt;
    wsPID_para[VLV_PORT_CV].PMPID.f16Kb       = Vlv_ActrConfig.CircvCfg.BackCalcValFloat;
    /* Initialization for Min/max limitation value */
    wsPID_para[VLV_PORT_CV].PMPID.s16uOutMax  = VALVE_CURRENT_UPLIMIT;
    wsPID_para[VLV_PORT_CV].PMPID.s16uOutMin  = VALVE_CURRENT_LOWLIMIT;

}


void Vlv_1ms_Actuate(void)
{
    Haluint16 limited_value = 0;

    /*************************************************************/
    /* Fl outlet valve */
    if(Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.FlOvReqData[Vlv_ActrBus.Vlv_1msOutputCnt] > Vlv_ActrConfig.FlOvCfg.MaxValue)
    {
        limited_value = Vlv_ActrConfig.FlOvCfg.MaxValue;
    }
    else if(Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.FlOvReqData[Vlv_ActrBus.Vlv_1msOutputCnt] < Vlv_ActrConfig.FlOvCfg.MinValue)
    {
        limited_value = Vlv_ActrConfig.FlOvCfg.MinValue;
    }
    else
    {
        limited_value = Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.FlOvReqData[Vlv_ActrBus.Vlv_1msOutputCnt];
    }
    /* Fl outlet Pid Check */
    if (Vlv_ActrConfig.FlOvCfg.IsPidtype == Valve_LlModule_Pid)
    {
        Vlv_ActrBus.Vlv_ActrWhlVlvDrvInfo.FlOvDrvData = Vlv_PidCurrentControl(limited_value, Vlv_ActrBus.OutputCurrentValue[Vlv_ActrConfig.FlOvCfg.PidPort], Vlv_ActrConfig.FlOvCfg.PidPort);
    }
    else
    {
        Vlv_ActrBus.Vlv_ActrWhlVlvDrvInfo.FlOvDrvData = limited_value * Vlv_ActrConfig.FlOvCfg.MulFactor / Vlv_ActrConfig.FlOvCfg.DivFactor;
    }
	/*************************************************************/

	/*************************************************************/
	/* Fr outlet valve */
    if(Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.FrOvReqData[Vlv_ActrBus.Vlv_1msOutputCnt] > Vlv_ActrConfig.FrOvCfg.MaxValue)
    {
        limited_value = Vlv_ActrConfig.FrOvCfg.MaxValue;
    }
    else if(Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.FrOvReqData[Vlv_ActrBus.Vlv_1msOutputCnt] < Vlv_ActrConfig.FrOvCfg.MinValue)
    {
        limited_value = Vlv_ActrConfig.FrOvCfg.MinValue;
    }
    else
    {
        limited_value = Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.FrOvReqData[Vlv_ActrBus.Vlv_1msOutputCnt];
    }
    /* Fr outlet Pid Check */
    if (Vlv_ActrConfig.FrOvCfg.IsPidtype == Valve_LlModule_Pid)
    {
       Vlv_ActrBus.Vlv_ActrWhlVlvDrvInfo.FrOvDrvData = Vlv_PidCurrentControl(limited_value, Vlv_ActrBus.OutputCurrentValue[Vlv_ActrConfig.FrOvCfg.PidPort], Vlv_ActrConfig.FrOvCfg.PidPort);
    }
    else
    {
        Vlv_ActrBus.Vlv_ActrWhlVlvDrvInfo.FrOvDrvData = limited_value * Vlv_ActrConfig.FrOvCfg.MulFactor / Vlv_ActrConfig.FrOvCfg.DivFactor;
    }
	/*************************************************************/

	/*************************************************************/
	/* Rl outlet valve */
    if(Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.RlOvReqData[Vlv_ActrBus.Vlv_1msOutputCnt] > Vlv_ActrConfig.RlOvCfg.MaxValue)
    {
        limited_value = Vlv_ActrConfig.RlOvCfg.MaxValue;
    }
    else if(Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.RlOvReqData[Vlv_ActrBus.Vlv_1msOutputCnt] < Vlv_ActrConfig.RlOvCfg.MinValue)
    {
        limited_value = Vlv_ActrConfig.RlOvCfg.MinValue;
    }
    else
    {
        limited_value = Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.RlOvReqData[Vlv_ActrBus.Vlv_1msOutputCnt];
    }
    /* Rl outlet Pid Check */
    if (Vlv_ActrConfig.RlOvCfg.IsPidtype == Valve_LlModule_Pid)
    {
       Vlv_ActrBus.Vlv_ActrWhlVlvDrvInfo.RlOvDrvData = Vlv_PidCurrentControl(limited_value, Vlv_ActrBus.OutputCurrentValue[Vlv_ActrConfig.RlOvCfg.PidPort], Vlv_ActrConfig.RlOvCfg.PidPort);
    }
    else
    {
        Vlv_ActrBus.Vlv_ActrWhlVlvDrvInfo.RlOvDrvData = limited_value * Vlv_ActrConfig.RlOvCfg.MulFactor / Vlv_ActrConfig.RlOvCfg.DivFactor;
    }
	/*************************************************************/

	/*************************************************************/
	/* Rr outlet valve */
    if(Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.RrOvReqData[Vlv_ActrBus.Vlv_1msOutputCnt] > Vlv_ActrConfig.RrOvCfg.MaxValue)
    {
        limited_value = Vlv_ActrConfig.RrOvCfg.MaxValue;
    }
    else if(Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.RrOvReqData[Vlv_ActrBus.Vlv_1msOutputCnt] < Vlv_ActrConfig.RrOvCfg.MinValue)
    {
        limited_value = Vlv_ActrConfig.RrOvCfg.MinValue;
    }
    else
    {
        limited_value = Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.RrOvReqData[Vlv_ActrBus.Vlv_1msOutputCnt];
    }
    /* Rr outlet Pid Check */
    if (Vlv_ActrConfig.RrOvCfg.IsPidtype == Valve_LlModule_Pid)
    {
       Vlv_ActrBus.Vlv_ActrWhlVlvDrvInfo.RrOvDrvData = Vlv_PidCurrentControl(limited_value, Vlv_ActrBus.OutputCurrentValue[Vlv_ActrConfig.RrOvCfg.PidPort], Vlv_ActrConfig.RrOvCfg.PidPort);
    }
    else
    {
        Vlv_ActrBus.Vlv_ActrWhlVlvDrvInfo.RrOvDrvData = limited_value * Vlv_ActrConfig.RrOvCfg.MulFactor / Vlv_ActrConfig.RrOvCfg.DivFactor;
    }
	/*************************************************************/

	/*************************************************************/
	/* Fl inlet valve */
    if(Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.FlIvReqData[Vlv_ActrBus.Vlv_1msOutputCnt] > Vlv_ActrConfig.FlIvCfg.MaxValue)
    {
        limited_value = Vlv_ActrConfig.FlIvCfg.MaxValue;
    }
    else if(Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.FlIvReqData[Vlv_ActrBus.Vlv_1msOutputCnt] < Vlv_ActrConfig.FlIvCfg.MinValue)
    {
        limited_value = Vlv_ActrConfig.FlIvCfg.MinValue;
    }
    else
    {
        limited_value = Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.FlIvReqData[Vlv_ActrBus.Vlv_1msOutputCnt];
    }
    /* Fl inlet Pid Check */
    if (Vlv_ActrConfig.FlIvCfg.IsPidtype == Valve_LlModule_Pid)
    {
        Vlv_ActrBus.Vlv_ActrWhlVlvDrvInfo.FlIvDrvData = Vlv_PidCurrentControl(limited_value, Vlv_ActrBus.OutputCurrentValue[Vlv_ActrConfig.FlIvCfg.PidPort], Vlv_ActrConfig.FlIvCfg.PidPort);
	}
    else
    {
        Vlv_ActrBus.Vlv_ActrWhlVlvDrvInfo.FlIvDrvData = limited_value * Vlv_ActrConfig.FlIvCfg.MulFactor / Vlv_ActrConfig.FlIvCfg.DivFactor;
    }
	/*************************************************************/

	/*************************************************************/
    /* Fr inlet valve */
    if(Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.FrIvReqData[Vlv_ActrBus.Vlv_1msOutputCnt] > Vlv_ActrConfig.FrIvCfg.MaxValue)
    {
        limited_value = Vlv_ActrConfig.FrIvCfg.MaxValue;
    }
    else if(Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.FrIvReqData[Vlv_ActrBus.Vlv_1msOutputCnt] < Vlv_ActrConfig.FrIvCfg.MinValue)
    {
        limited_value = Vlv_ActrConfig.FrIvCfg.MinValue;
    }
    else
    {
        limited_value = Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.FrIvReqData[Vlv_ActrBus.Vlv_1msOutputCnt];
    }
    /* Fr inlet Pid Check */
    if (Vlv_ActrConfig.FrIvCfg.IsPidtype == Valve_LlModule_Pid)
    {
        Vlv_ActrBus.Vlv_ActrWhlVlvDrvInfo.FrIvDrvData = Vlv_PidCurrentControl(limited_value, Vlv_ActrBus.OutputCurrentValue[Vlv_ActrConfig.FrIvCfg.PidPort], Vlv_ActrConfig.FrIvCfg.PidPort);
	}
    else
    {
        Vlv_ActrBus.Vlv_ActrWhlVlvDrvInfo.FrIvDrvData = limited_value * Vlv_ActrConfig.FrIvCfg.MulFactor / Vlv_ActrConfig.FrIvCfg.DivFactor;
    }
	/*************************************************************/

	/*************************************************************/
    /* Rl inlet valve */
    if(Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.RlIvReqData[Vlv_ActrBus.Vlv_1msOutputCnt] > Vlv_ActrConfig.RlIvCfg.MaxValue)
    {
        limited_value = Vlv_ActrConfig.RlIvCfg.MaxValue;
    }
    else if(Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.RlIvReqData[Vlv_ActrBus.Vlv_1msOutputCnt] < Vlv_ActrConfig.RlIvCfg.MinValue)
    {
        limited_value = Vlv_ActrConfig.RlIvCfg.MinValue;
    }
    else
    {
        limited_value = Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.RlIvReqData[Vlv_ActrBus.Vlv_1msOutputCnt];
    }
    /* Rl inlet Pid Check */
    if (Vlv_ActrConfig.RlIvCfg.IsPidtype == Valve_LlModule_Pid)
    {
        Vlv_ActrBus.Vlv_ActrWhlVlvDrvInfo.RlIvDrvData = Vlv_PidCurrentControl(limited_value, Vlv_ActrBus.OutputCurrentValue[Vlv_ActrConfig.RlIvCfg.PidPort], Vlv_ActrConfig.RlIvCfg.PidPort);
	}
    else
    {
        Vlv_ActrBus.Vlv_ActrWhlVlvDrvInfo.RlIvDrvData = limited_value * Vlv_ActrConfig.RlIvCfg.MulFactor / Vlv_ActrConfig.RlIvCfg.DivFactor;
    }
	/*************************************************************/

	/*************************************************************/
    /* Rr inlet valve */
    if(Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.RrIvReqData[Vlv_ActrBus.Vlv_1msOutputCnt] > Vlv_ActrConfig.RrIvCfg.MaxValue)
    {
        limited_value = Vlv_ActrConfig.RrIvCfg.MaxValue;
    }
    else if(Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.RrIvReqData[Vlv_ActrBus.Vlv_1msOutputCnt] < Vlv_ActrConfig.RrIvCfg.MinValue)
    {
        limited_value = Vlv_ActrConfig.RrIvCfg.MinValue;
    }
    else
    {
        limited_value = Vlv_ActrBus.Vlv_ActrWhlVlvReqInfo.RrIvReqData[Vlv_ActrBus.Vlv_1msOutputCnt];
    }
    /* Rr inlet Pid Check */
    if (Vlv_ActrConfig.RrIvCfg.IsPidtype == Valve_LlModule_Pid)
    {
        Vlv_ActrBus.Vlv_ActrWhlVlvDrvInfo.RrIvDrvData = Vlv_PidCurrentControl(limited_value, Vlv_ActrBus.OutputCurrentValue[Vlv_ActrConfig.RrIvCfg.PidPort], Vlv_ActrConfig.RrIvCfg.PidPort);
	}
    else
    {
        Vlv_ActrBus.Vlv_ActrWhlVlvDrvInfo.RrIvDrvData = limited_value * Vlv_ActrConfig.RrIvCfg.MulFactor / Vlv_ActrConfig.RrIvCfg.DivFactor;
    }
 	/*************************************************************/

	/*************************************************************/   
    /* PrimCut valve */
    if(Vlv_ActrBus.Vlv_ActrNormVlvReqInfo.PrimCutVlvReqData[Vlv_ActrBus.Vlv_1msOutputCnt] > Vlv_ActrConfig.PrimCutCfg.MaxValue)
    {
        limited_value = Vlv_ActrConfig.PrimCutCfg.MaxValue;
    }
    else if(Vlv_ActrBus.Vlv_ActrNormVlvReqInfo.PrimCutVlvReqData[Vlv_ActrBus.Vlv_1msOutputCnt] < Vlv_ActrConfig.PrimCutCfg.MinValue)
    {
        limited_value = Vlv_ActrConfig.PrimCutCfg.MinValue;
    }
    else
    {
        limited_value = Vlv_ActrBus.Vlv_ActrNormVlvReqInfo.PrimCutVlvReqData[Vlv_ActrBus.Vlv_1msOutputCnt];
    }
    /* PrimCut Pid Check */
    if (Vlv_ActrConfig.PrimCutCfg.IsPidtype == Valve_LlModule_Pid)
    {
        Vlv_ActrBus.Vlv_ActrNormVlvDrvInfo.PrimCutVlvDrvData = Vlv_PidCurrentControl(limited_value, Vlv_ActrBus.OutputCurrentValue[Vlv_ActrConfig.PrimCutCfg.PidPort], Vlv_ActrConfig.PrimCutCfg.PidPort);
		if(limited_value == 0)
		{
			wsPID_para[VLV_PORT_CUT1].PMPID.s32uP = 0;
			wsPID_para[VLV_PORT_CUT1].PMPID.s32uI = 0;
			wsPID_para[VLV_PORT_CUT1].PMPID.s32uD = 0;
			wsPID_para[VLV_PORT_CUT1].PMPID.s16uDifference = 0;
			wsPID_para[VLV_PORT_CUT1].PMPID.s16errOld = 0;
			wsPID_para[VLV_PORT_CUT1].PMPID.s16errDotFiltOld = 0;
			wsPID_para[VLV_PORT_CUT1].PMPID.s16uOut = 0;
		
			Vlv_ActrBus.Vlv_ActrNormVlvDrvInfo.PrimCutVlvDrvData  = 0;
		}
	}
    else
    {
        Vlv_ActrBus.Vlv_ActrNormVlvDrvInfo.PrimCutVlvDrvData = limited_value * Vlv_ActrConfig.PrimCutCfg.MulFactor / Vlv_ActrConfig.PrimCutCfg.DivFactor;
    }
	/*************************************************************/

	/*************************************************************/
    /* SecdCut valve */
    if(Vlv_ActrBus.Vlv_ActrNormVlvReqInfo.SecdCutVlvReqData[Vlv_ActrBus.Vlv_1msOutputCnt] > Vlv_ActrConfig.SecdCutCfg.MaxValue)
    {
        limited_value = Vlv_ActrConfig.SecdCutCfg.MaxValue;
    }
    else if(Vlv_ActrBus.Vlv_ActrNormVlvReqInfo.SecdCutVlvReqData[Vlv_ActrBus.Vlv_1msOutputCnt] < Vlv_ActrConfig.SecdCutCfg.MinValue)
    {
        limited_value = Vlv_ActrConfig.SecdCutCfg.MinValue;
    }
    else
    {
        limited_value = Vlv_ActrBus.Vlv_ActrNormVlvReqInfo.SecdCutVlvReqData[Vlv_ActrBus.Vlv_1msOutputCnt];
    }
    /* SecdCut Pid Check */
    if (Vlv_ActrConfig.SecdCutCfg.IsPidtype == Valve_LlModule_Pid)
    {
        Vlv_ActrBus.Vlv_ActrNormVlvDrvInfo.SecdCutVlvDrvData = Vlv_PidCurrentControl(limited_value, Vlv_ActrBus.OutputCurrentValue[Vlv_ActrConfig.SecdCutCfg.PidPort], Vlv_ActrConfig.SecdCutCfg.PidPort);
		if(limited_value == 0)
		{
			wsPID_para[VLV_PORT_CUT2].PMPID.s32uP = 0;
			wsPID_para[VLV_PORT_CUT2].PMPID.s32uI = 0;
			wsPID_para[VLV_PORT_CUT2].PMPID.s32uD = 0;
			wsPID_para[VLV_PORT_CUT2].PMPID.s16uDifference = 0;
			wsPID_para[VLV_PORT_CUT2].PMPID.s16errOld = 0;
			wsPID_para[VLV_PORT_CUT2].PMPID.s16errDotFiltOld = 0;
			wsPID_para[VLV_PORT_CUT2].PMPID.s16uOut = 0;
		
			Vlv_ActrBus.Vlv_ActrNormVlvDrvInfo.SecdCutVlvDrvData  = 0;
		}
	}
    else
    {
        Vlv_ActrBus.Vlv_ActrNormVlvDrvInfo.SecdCutVlvDrvData = limited_value * Vlv_ActrConfig.SecdCutCfg.MulFactor / Vlv_ActrConfig.SecdCutCfg.DivFactor;
    }
	/*************************************************************/

	/*************************************************************/
    /* Resl valve */
    if(Vlv_ActrBus.Vlv_ActrNormVlvReqInfo.RelsVlvReqData[Vlv_ActrBus.Vlv_1msOutputCnt] > Vlv_ActrConfig.ReslvCfg.MaxValue)
    {
        limited_value = Vlv_ActrConfig.ReslvCfg.MaxValue;
    }
    else if(Vlv_ActrBus.Vlv_ActrNormVlvReqInfo.RelsVlvReqData[Vlv_ActrBus.Vlv_1msOutputCnt] < Vlv_ActrConfig.ReslvCfg.MinValue)
    {
        limited_value = Vlv_ActrConfig.ReslvCfg.MinValue;
    }
    else
    {
        limited_value = Vlv_ActrBus.Vlv_ActrNormVlvReqInfo.RelsVlvReqData[Vlv_ActrBus.Vlv_1msOutputCnt];
    }
    /* Resl Valve Pid Check */
    if (Vlv_ActrConfig.ReslvCfg.IsPidtype == Valve_LlModule_Pid) /* NZ Quick C: RLV */
    {
        Vlv_ActrBus.Vlv_ActrNormVlvDrvInfo.RelsVlvDrvData = Vlv_PidCurrentControl(limited_value, Vlv_ActrBus.OutputCurrentValue[Vlv_ActrConfig.ReslvCfg.PidPort], Vlv_ActrConfig.ReslvCfg.PidPort);
		if(limited_value == 0)
		{
			wsPID_para[VLV_PORT_RLV].PMPID.s32uP = 0;
			wsPID_para[VLV_PORT_RLV].PMPID.s32uI = 0;
			wsPID_para[VLV_PORT_RLV].PMPID.s32uD = 0;
			wsPID_para[VLV_PORT_RLV].PMPID.s16uDifference = 0;
			wsPID_para[VLV_PORT_RLV].PMPID.s16errOld = 0;
			wsPID_para[VLV_PORT_RLV].PMPID.s16errDotFiltOld = 0;
			wsPID_para[VLV_PORT_RLV].PMPID.s16uOut = 0;
		
			Vlv_ActrBus.Vlv_ActrNormVlvDrvInfo.RelsVlvDrvData  = 0;
		}
	}
    else
    {
        Vlv_ActrBus.Vlv_ActrNormVlvDrvInfo.RelsVlvDrvData = limited_value * Vlv_ActrConfig.ReslvCfg.MulFactor / Vlv_ActrConfig.ReslvCfg.DivFactor;
    }
	/*************************************************************/

	/*************************************************************/
    /* Circuit valve */
    if(Vlv_ActrBus.Vlv_ActrNormVlvReqInfo.CircVlvReqData[Vlv_ActrBus.Vlv_1msOutputCnt] > Vlv_ActrConfig.CircvCfg.MaxValue)
    {
        limited_value = Vlv_ActrConfig.CircvCfg.MaxValue;
    }
    else if(Vlv_ActrBus.Vlv_ActrNormVlvReqInfo.CircVlvReqData[Vlv_ActrBus.Vlv_1msOutputCnt] < Vlv_ActrConfig.CircvCfg.MinValue)
    {
        limited_value = Vlv_ActrConfig.CircvCfg.MinValue;
    }
    else
    {
        limited_value = Vlv_ActrBus.Vlv_ActrNormVlvReqInfo.CircVlvReqData[Vlv_ActrBus.Vlv_1msOutputCnt];
    }
    /* Circuit Pid Check */
    if (Vlv_ActrConfig.CircvCfg.IsPidtype == Valve_LlModule_Pid) /* NZ Quick C: CV */
    {
        Vlv_ActrBus.Vlv_ActrNormVlvDrvInfo.CircVlvDrvData = Vlv_PidCurrentControl(limited_value, Vlv_ActrBus.OutputCurrentValue[Vlv_ActrConfig.CircvCfg.PidPort], Vlv_ActrConfig.CircvCfg.PidPort);
		if(limited_value == 0)
		{
			wsPID_para[VLV_PORT_CV].PMPID.s32uP = 0;
			wsPID_para[VLV_PORT_CV].PMPID.s32uI = 0;
			wsPID_para[VLV_PORT_CV].PMPID.s32uD = 0;
			wsPID_para[VLV_PORT_CV].PMPID.s16uDifference = 0;
			wsPID_para[VLV_PORT_CV].PMPID.s16errOld = 0;
			wsPID_para[VLV_PORT_CV].PMPID.s16errDotFiltOld = 0;
			wsPID_para[VLV_PORT_CV].PMPID.s16uOut = 0;
		
			Vlv_ActrBus.Vlv_ActrNormVlvDrvInfo.CircVlvDrvData  = 0;
		}
	}
    else
    {
        Vlv_ActrBus.Vlv_ActrNormVlvDrvInfo.CircVlvDrvData = limited_value * Vlv_ActrConfig.CircvCfg.MulFactor / Vlv_ActrConfig.CircvCfg.DivFactor;
    }
	/*************************************************************/

    /*************************************************************/
    /* Sim valve */
    if(Vlv_ActrBus.Vlv_ActrNormVlvReqInfo.SimVlvReqData[Vlv_ActrBus.Vlv_1msOutputCnt] > Vlv_ActrConfig.SimCfg.MaxValue)
    {
        limited_value = Vlv_ActrConfig.SimCfg.MaxValue;
    }
    else if(Vlv_ActrBus.Vlv_ActrNormVlvReqInfo.SimVlvReqData[Vlv_ActrBus.Vlv_1msOutputCnt] < Vlv_ActrConfig.SimCfg.MinValue)
    {
        limited_value = Vlv_ActrConfig.SimCfg.MinValue;
    }
    else
    {
        limited_value = Vlv_ActrBus.Vlv_ActrNormVlvReqInfo.SimVlvReqData[Vlv_ActrBus.Vlv_1msOutputCnt];
    }
    /* Sim Pid Check */
    if (Vlv_ActrConfig.SimCfg.IsPidtype == Valve_LlModule_Pid)
    {
        Vlv_ActrBus.Vlv_ActrNormVlvDrvInfo.SimVlvDrvData = Vlv_PidCurrentControl(limited_value, Vlv_ActrBus.OutputCurrentValue[Vlv_ActrConfig.SimCfg.PidPort], Vlv_ActrConfig.SimCfg.PidPort);
		if(limited_value == 0)
		{
			wsPID_para[VLV_PORT_SIM].PMPID.s32uP = 0;
			wsPID_para[VLV_PORT_SIM].PMPID.s32uI = 0;
			wsPID_para[VLV_PORT_SIM].PMPID.s32uD = 0;
			wsPID_para[VLV_PORT_SIM].PMPID.s16uDifference = 0;
			wsPID_para[VLV_PORT_SIM].PMPID.s16errOld = 0;
			wsPID_para[VLV_PORT_SIM].PMPID.s16errDotFiltOld = 0;
			wsPID_para[VLV_PORT_SIM].PMPID.s16uOut = 0;
		
			Vlv_ActrBus.Vlv_ActrNormVlvDrvInfo.SimVlvDrvData  = 0;
		}
	}
    else
    {
        Vlv_ActrBus.Vlv_ActrNormVlvDrvInfo.SimVlvDrvData = limited_value * Vlv_ActrConfig.SimCfg.MulFactor / Vlv_ActrConfig.SimCfg.DivFactor;
    }
    /*************************************************************/
     
    /*************************************************************/
    /* PressDump valve */
    if(Vlv_ActrBus.Vlv_ActrBalVlvReqInfo.PressDumpVlvReqData[Vlv_ActrBus.Vlv_1msOutputCnt] > Vlv_ActrConfig.PDvCfg.MaxValue)
    {
        limited_value = Vlv_ActrConfig.PDvCfg.MaxValue;
    }
    else if(Vlv_ActrBus.Vlv_ActrBalVlvReqInfo.PressDumpVlvReqData[Vlv_ActrBus.Vlv_1msOutputCnt] < Vlv_ActrConfig.PDvCfg.MinValue)
    {
        limited_value = Vlv_ActrConfig.PDvCfg.MinValue;
    }
    else
    {
        limited_value = Vlv_ActrBus.Vlv_ActrBalVlvReqInfo.PressDumpVlvReqData[Vlv_ActrBus.Vlv_1msOutputCnt];
    }
    /* PressDump Valve Pid Check */
    if (Vlv_ActrConfig.PDvCfg.IsPidtype == Valve_LlModule_Pid)
    {
        Vlv_ActrBus.Vlv_ActrBalVlvDrvInfo.PressDumpVlvDrvData = Vlv_PidCurrentControl(limited_value, Vlv_ActrBus.OutputCurrentValue[Vlv_ActrConfig.PDvCfg.PidPort], Vlv_ActrConfig.PDvCfg.PidPort);
    }
    else
    {
        Vlv_ActrBus.Vlv_ActrBalVlvDrvInfo.PressDumpVlvDrvData = limited_value * Vlv_ActrConfig.PDvCfg.MulFactor / Vlv_ActrConfig.PDvCfg.DivFactor;
    }
    
    /*************************************************************/
    /* Balance valve */
    if(Vlv_ActrBus.Vlv_ActrBalVlvReqInfo.BalVlvReqData[Vlv_ActrBus.Vlv_1msOutputCnt] > Vlv_ActrConfig.BalvCfg.MaxValue)
    {
        limited_value = Vlv_ActrConfig.BalvCfg.MaxValue;
    }
    else if(Vlv_ActrBus.Vlv_ActrBalVlvReqInfo.BalVlvReqData[Vlv_ActrBus.Vlv_1msOutputCnt] < Vlv_ActrConfig.BalvCfg.MinValue)
    {
        limited_value = Vlv_ActrConfig.BalvCfg.MinValue;
    }
    else
    {
        limited_value = Vlv_ActrBus.Vlv_ActrBalVlvReqInfo.BalVlvReqData[Vlv_ActrBus.Vlv_1msOutputCnt];
    }
    /* Balance Valve Pid Check */
    if (Vlv_ActrConfig.BalvCfg.IsPidtype == Valve_LlModule_Pid)
    {
        Vlv_ActrBus.Vlv_ActrBalVlvDrvInfo.BalVlvDrvData = Vlv_PidCurrentControl(limited_value, Vlv_ActrBus.OutputCurrentValue[Vlv_ActrConfig.BalvCfg.PidPort], Vlv_ActrConfig.BalvCfg.PidPort);
    }
    else
    {
        Vlv_ActrBus.Vlv_ActrBalVlvDrvInfo.BalVlvDrvData = limited_value * Vlv_ActrConfig.BalvCfg.MulFactor / Vlv_ActrConfig.BalvCfg.DivFactor;
    }

    Vlv_ActrBus.Vlv_1msOutputCnt++;
	
   if (Vlv_ActrBus.Vlv_1msOutputCnt >=5) Vlv_ActrBus.Vlv_1msOutputCnt=0;
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
/****************************************************************************
| NAME:             Vlv_PidCurrentControl
| CALLED BY:        
| PRECONDITIONS:    none
|
| INPUT PARAMETERS: targetCurrent, feedbackCurrent, port
|
| RETURN VALUES:    none
|
| DESCRIPTION:      Valve Pid Control Set Duty
****************************************************************************/
static Vlv_DataType Vlv_PidCurrentControl(\
    Vlv_DataType targetCurrent, Vlv_DataType feedbackCurrent, Vlv_PortType port)
{
    Vlv_DataType retVal;
    retVal = Vlv_IDBs16PIDcontroller(targetCurrent, feedbackCurrent, port);
    /* Scale Matching 0 ~ 1250 to 0 ~ 10000(0.01%) */
    retVal = (Vlv_DataType)(((uint32)retVal)*10000/1250) ;
    return retVal;
}

/****************************************************************************
| NAME:             Vlv_IDBs16PIDcontroller
| CALLED BY:        
| PRECONDITIONS:    none
|
| INPUT PARAMETERS: Target Current, Feedback Current, Valve Port
|
| RETURN VALUES:    Pid Controlled Current Value
|
| DESCRIPTION:      Valve PidController
****************************************************************************/
static Vlv_DataType Vlv_IDBs16PIDcontroller(Vlv_DataType xRef, Vlv_DataType x, Vlv_PortType port)
{
    register sint16    s16Temp0 = 0, s16err = 0, s16uOut = 0, s16errDot = 0, s16errDotFilt = 0;
    register sint32    s32err = 0, s32uDifference = 0, s32uCalc = 0, s32errDot = 0, s32Temp0 = 0, s32errDotFilt = 0;
    
    /* for D-Controller */
    
    if (wsPID_para[port].PMPID.s16Kd > 100)
    {
        wsPID_para[port].PMPID.s16Kd = 100;
    }
    else if (wsPID_para[port].PMPID.s16Kd < -100)
    {
        wsPID_para[port].PMPID.s16Kd = -100;
    }
    else
    {
        ;
    }   
    
    if (wsPID_para[port].PMPID.s16fs > 20000)
    {
        wsPID_para[port].PMPID.s16fs = 20000;
    }
    else if (wsPID_para[port].PMPID.s16fs < -20000)
    {
        wsPID_para[port].PMPID.s16fs = -20000;
    }
    else
    {
        ;
    }
    
    if (wsPID_para[port].PMPID.s16wc > 1000)
    {
        wsPID_para[port].PMPID.s16wc = 1000;
    }
    else if (wsPID_para[port].PMPID.s16wc < -1000)
    {
        wsPID_para[port].PMPID.s16wc = -1000;
    }
    else
    {
        ;
    }
    /* for D-Controller */
    
    /* calculate err */
    s32err = (sint32)xRef - (sint32)x;
    
    /* constrain err range */
    if (s32err > S16_MAX)
    {
        s16err = S16_MAX;
    }
    else if (s32err < S16_MIN)
    {
        s16err = S16_MIN;
    }
    else
    {
        s16err = (sint16)s32err;
    }
    
    /* calculate uP */
    wsPID_para[port].PMPID.s32uP = (sint32)wsPID_para[port].PMPID.s16Kp * (sint32)s16err + (sint32)mult_r(wsPID_para[port].PMPID.f16Kp, s16err);
    
    /* calculate uI */
    s32err = s16err + (sint32)wsPID_para[port].PMPID.s16Kb * (sint32)wsPID_para[port].PMPID.s16uDifference + (sint32)mult_r(wsPID_para[port].PMPID.f16Kb, wsPID_para[port].PMPID.s16uDifference);
    if (s32err > S16_MAX)
    {
        s16err = S16_MAX;
    }
    else if (s32err < S16_MIN)
    {
        s16err = S16_MIN;
    }
    else
    {
        s16err = (sint16)s32err;
    }
    
    s16Temp0 = mult_r(wsPID_para[port].PMPID.f16KiTs, s16err);
    if (s16Temp0 == 0 && wsPID_para[port].PMPID.f16KiTs != 0 && s16err != 0)
    {
        if (wsPID_para[port].PMPID.f16KiTs > 0 && s16err > 0)
        {
            wsPID_para[port].PMPID.s32uI = wsPID_para[port].PMPID.s32uI + (sint32)wsPID_para[port].PMPID.s16KiTs * (sint32)s16err + 1;
        }
        else if (wsPID_para[port].PMPID.f16KiTs > 0 && s16err < 0)
        {
            wsPID_para[port].PMPID.s32uI = wsPID_para[port].PMPID.s32uI + (sint32)wsPID_para[port].PMPID.s16KiTs * (sint32)s16err - 1;
        }
        else if (wsPID_para[port].PMPID.f16KiTs < 0 && s16err > 0)
        {
            wsPID_para[port].PMPID.s32uI = wsPID_para[port].PMPID.s32uI + (sint32)wsPID_para[port].PMPID.s16KiTs * (sint32)s16err - 1;
        }
        else if (wsPID_para[port].PMPID.f16KiTs < 0 && s16err < 0)
        {
            wsPID_para[port].PMPID.s32uI = wsPID_para[port].PMPID.s32uI + (sint32)wsPID_para[port].PMPID.s16KiTs * (sint32)s16err + 1;
        }
        else
        {
            ;
        }
    }
    else
    {
        wsPID_para[port].PMPID.s32uI = wsPID_para[port].PMPID.s32uI + (sint32)wsPID_para[port].PMPID.s16KiTs * (sint32)s16err + (sint32)mult_r(wsPID_para[port].PMPID.f16KiTs, s16err);
    }

    if (wsPID_para[port].PMPID.s32uI > wsPID_para[port].PMPID.s16uOutMax)
    {
        wsPID_para[port].PMPID.s32uI = wsPID_para[port].PMPID.s16uOutMax;
    }
    else if (wsPID_para[port].PMPID.s32uI < wsPID_para[port].PMPID.s16uOutMin)
    {
        wsPID_para[port].PMPID.s32uI = wsPID_para[port].PMPID.s16uOutMin;
    }
    else
    {
        ;
    }
    
    s32errDot = (sint32)wsPID_para[port].PMPID.s16fs * ((sint32)s16err - (sint32)wsPID_para[port].PMPID.s16errOld);
    
    if (s32errDot > S16_MAX)
    {
        s16errDot = S16_MAX;
    }
    else if (s32errDot < S16_MIN)
    {
        s16errDot = S16_MIN;
    }
    else
    {
        s16errDot = (sint16)s32errDot;
    }           
    
    s32Temp0 = (sint32)wsPID_para[port].PMPID.s16wc + (sint32)wsPID_para[port].PMPID.s16fs;
    if (s32Temp0 > 0)
    {
        s32errDotFilt = ((sint32)wsPID_para[port].PMPID.s16fs * (sint32)wsPID_para[port].PMPID.s16errDotFiltOld / s32Temp0 + (sint32)wsPID_para[port].PMPID.s16wc * (sint32)s16errDot / s32Temp0);
    }
    else
    {
        s32errDotFilt = 0;
    }
    
    if (s32errDotFilt > S16_MAX)
    {
        s16errDotFilt = S16_MAX;
    }
    else if (s32errDotFilt < S16_MIN)
    {
        s16errDotFilt = S16_MIN;
    }
    else
    {
        s16errDotFilt = (sint16)s32errDotFilt;
    }
    
    wsPID_para[port].PMPID.s32uD = (sint32)wsPID_para[port].PMPID.s16Kd * (sint32)s16errDotFilt  + (sint32)mult_r(wsPID_para[port].PMPID.f16Kd, s16errDotFilt);
    s32uCalc = wsPID_para[port].PMPID.s32uP + wsPID_para[port].PMPID.s32uI + wsPID_para[port].PMPID.s32uD;

    
    if (s32uCalc > wsPID_para[port].PMPID.s16uOutMax)
    {
        s16uOut = wsPID_para[port].PMPID.s16uOutMax;
    }
    else if (s32uCalc < wsPID_para[port].PMPID.s16uOutMin)
    {
        s16uOut = wsPID_para[port].PMPID.s16uOutMin;
    }
    else
    {
        s16uOut = (sint16)s32uCalc;
    }
    
    s32uDifference = (sint32)s16uOut - s32uCalc;
    if (s32uDifference > S16_MAX)
    {
        wsPID_para[port].PMPID.s16uDifference = S16_MAX;
    }
    else if (s32uDifference < S16_MIN)
    {
        wsPID_para[port].PMPID.s16uDifference = S16_MIN;
    }
    else
    {
        wsPID_para[port].PMPID.s16uDifference = (sint16)s32uDifference;
    }
    
    wsPID_para[port].PMPID.s16errOld = s16err;
    wsPID_para[port].PMPID.s16errDotFiltOld = s16errDotFilt;
    
    wsPID_para[port].PMPID.s16uOut = s16uOut;

    return s16uOut;    
}

#define VLV_ACTR_STOP_SEC_CODE
#include "Vlv_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
