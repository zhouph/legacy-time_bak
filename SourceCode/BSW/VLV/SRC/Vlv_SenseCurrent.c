/**
 * @defgroup Vlv_1msOutput Vlv_1msOutput
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Vlv_1msOutput.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Vlv_Actr.h"
#include "Vlv_Actr_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/


/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define VLV_ACTR_START_SEC_CONST_UNSPECIFIED
#include "Vlv_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define VLV_ACTR_STOP_SEC_CONST_UNSPECIFIED
#include "Vlv_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define VLV_ACTR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlv_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */


#define VLV_ACTR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlv_MemMap.h"
#define VLV_ACTR_START_SEC_VAR_NOINIT_32BIT
#include "Vlv_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define VLV_ACTR_STOP_SEC_VAR_NOINIT_32BIT
#include "Vlv_MemMap.h"
#define VLV_ACTR_START_SEC_VAR_UNSPECIFIED
#include "Vlv_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define VLV_ACTR_STOP_SEC_VAR_UNSPECIFIED
#include "Vlv_MemMap.h"
#define VLV_ACTR_START_SEC_VAR_32BIT
#include "Vlv_MemMap.h"
/** Variable Section (32BIT)**/


#define VLV_ACTR_STOP_SEC_VAR_32BIT
#include "Vlv_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define VLV_ACTR_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlv_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

#define VLV_ACTR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlv_MemMap.h"
#define VLV_ACTR_START_SEC_VAR_NOINIT_32BIT
#include "Vlv_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define VLV_ACTR_STOP_SEC_VAR_NOINIT_32BIT
#include "Vlv_MemMap.h"
#define VLV_ACTR_START_SEC_VAR_UNSPECIFIED
#include "Vlv_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define VLV_ACTR_STOP_SEC_VAR_UNSPECIFIED
#include "Vlv_MemMap.h"
#define VLV_ACTR_START_SEC_VAR_32BIT
#include "Vlv_MemMap.h"
/** Variable Section (32BIT)**/


#define VLV_ACTR_STOP_SEC_VAR_32BIT
#include "Vlv_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define VLV_ACTR_START_SEC_CODE
#include "Vlv_MemMap.h"
void Vlv_1msOutput_SenseCurrent(void);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

void Vlv_1msOutput_SenseCurrent(void)
{
    /* NZ Quick C Sample: CUT-P */
    Vlv_ActrBus.OutputCurrentValue[Vlv_ActrConfig.PrimCutCfg.PidPort] = Vlv_ActrBus.Vlv_ActrVlvMonInfo.PrimCutVlvMon * Vlv_SnsrConfig.PrimCutCfg.MulFactor / Vlv_SnsrConfig.PrimCutCfg.DivFactor;
    /* NZ Quick C Sample: CUT-S */
    Vlv_ActrBus.OutputCurrentValue[Vlv_ActrConfig.SecdCutCfg.PidPort] = Vlv_ActrBus.Vlv_ActrVlvMonInfo.SecdCutVlvMon * Vlv_SnsrConfig.SecdCutCfg.MulFactor / Vlv_SnsrConfig.SecdCutCfg.DivFactor;
    /* NZ Quick C Sample: RLV */
    Vlv_ActrBus.OutputCurrentValue[Vlv_ActrConfig.ReslvCfg.PidPort] = Vlv_ActrBus.Vlv_ActrVlvMonInfo.VlvRLVMon * Vlv_SnsrConfig.ReslvCfg.MulFactor / Vlv_SnsrConfig.ReslvCfg.DivFactor;
    /* NZ Quick C Sample: CV */
    Vlv_ActrBus.OutputCurrentValue[Vlv_ActrConfig.CircvCfg.PidPort] = Vlv_ActrBus.Vlv_ActrVlvMonInfo.VlvCVMon * Vlv_SnsrConfig.CircvCfg.MulFactor / Vlv_SnsrConfig.CircvCfg.DivFactor;
    /* NZ Quick C Sample: SIM */
    Vlv_ActrBus.OutputCurrentValue[Vlv_ActrConfig.SimCfg.PidPort] = Vlv_ActrBus.Vlv_ActrVlvMonInfo.SimVlvMon * Vlv_SnsrConfig.SimCfg.MulFactor / Vlv_SnsrConfig.SimCfg.DivFactor;
}

#define VLV_ACTR_STOP_SEC_CODE
#include "Vlv_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
