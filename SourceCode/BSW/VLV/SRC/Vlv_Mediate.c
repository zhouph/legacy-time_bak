/**
 * @defgroup Vlv_5msOutput Vlv_5msOutput
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Vlv_5msOutput.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Vlv_ActrSync.h"
#include "Vlv_ActrSync_Ifa.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define VLV_ACTRSYNC_START_SEC_CONST_UNSPECIFIED
#include "Vlv_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define VLV_ACTRSYNC_STOP_SEC_CONST_UNSPECIFIED
#include "Vlv_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define VLV_ACTRSYNC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlv_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define VLV_ACTRSYNC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlv_MemMap.h"
#define VLV_ACTRSYNC_START_SEC_VAR_NOINIT_32BIT
#include "Vlv_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define VLV_ACTRSYNC_STOP_SEC_VAR_NOINIT_32BIT
#include "Vlv_MemMap.h"
#define VLV_ACTRSYNC_START_SEC_VAR_UNSPECIFIED
#include "Vlv_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define VLV_ACTRSYNC_STOP_SEC_VAR_UNSPECIFIED
#include "Vlv_MemMap.h"
#define VLV_ACTRSYNC_START_SEC_VAR_32BIT
#include "Vlv_MemMap.h"
/** Variable Section (32BIT)**/


#define VLV_ACTRSYNC_STOP_SEC_VAR_32BIT
#include "Vlv_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define VLV_ACTRSYNC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlv_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define VLV_ACTRSYNC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Vlv_MemMap.h"
#define VLV_ACTRSYNC_START_SEC_VAR_NOINIT_32BIT
#include "Vlv_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define VLV_ACTRSYNC_STOP_SEC_VAR_NOINIT_32BIT
#include "Vlv_MemMap.h"
#define VLV_ACTRSYNC_START_SEC_VAR_UNSPECIFIED
#include "Vlv_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define VLV_ACTRSYNC_STOP_SEC_VAR_UNSPECIFIED
#include "Vlv_MemMap.h"
#define VLV_ACTRSYNC_START_SEC_VAR_32BIT
#include "Vlv_MemMap.h"
/** Variable Section (32BIT)**/


#define VLV_ACTRSYNC_STOP_SEC_VAR_32BIT
#include "Vlv_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define VLV_ACTRSYNC_START_SEC_CODE
#include "Vlv_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Vlv_5msOutput_MediateRequest(void)
{
    uint8 i = 0;

    /* For Valve Actuation from Arbitrator_Vlv */
    for (i = 0; i < 5; i++)
    {
        /* Outlet Valve */
        Vlv_ActrSyncBus.Vlv_ActrSyncWhlVlvReqInfo.FlOvReqData[i] = Vlv_ActrSyncBus.Vlv_ActrSyncArbWhlVlvReqInfo.FlOvReqData[i];
        Vlv_ActrSyncBus.Vlv_ActrSyncWhlVlvReqInfo.FrOvReqData[i] = Vlv_ActrSyncBus.Vlv_ActrSyncArbWhlVlvReqInfo.FrOvReqData[i];
        Vlv_ActrSyncBus.Vlv_ActrSyncWhlVlvReqInfo.RlOvReqData[i] = Vlv_ActrSyncBus.Vlv_ActrSyncArbWhlVlvReqInfo.RlOvReqData[i];
        Vlv_ActrSyncBus.Vlv_ActrSyncWhlVlvReqInfo.RrOvReqData[i] = Vlv_ActrSyncBus.Vlv_ActrSyncArbWhlVlvReqInfo.RrOvReqData[i];

        /* Inlet Valve */
        Vlv_ActrSyncBus.Vlv_ActrSyncWhlVlvReqInfo.FlIvReqData[i] = Vlv_ActrSyncBus.Vlv_ActrSyncArbWhlVlvReqInfo.FlIvReqData[i];
        Vlv_ActrSyncBus.Vlv_ActrSyncWhlVlvReqInfo.FrIvReqData[i] = Vlv_ActrSyncBus.Vlv_ActrSyncArbWhlVlvReqInfo.FrIvReqData[i];
        Vlv_ActrSyncBus.Vlv_ActrSyncWhlVlvReqInfo.RlIvReqData[i] = Vlv_ActrSyncBus.Vlv_ActrSyncArbWhlVlvReqInfo.RlIvReqData[i];
        Vlv_ActrSyncBus.Vlv_ActrSyncWhlVlvReqInfo.RrIvReqData[i] = Vlv_ActrSyncBus.Vlv_ActrSyncArbWhlVlvReqInfo.RrIvReqData[i];

        /* Normal Valve */
        Vlv_ActrSyncBus.Vlv_ActrSyncNormVlvReqInfo.PrimCutVlvReqData[i] = Vlv_ActrSyncBus.Vlv_ActrSyncArbNormVlvReqInfo.PrimCutVlvReqData[i];
        Vlv_ActrSyncBus.Vlv_ActrSyncNormVlvReqInfo.RelsVlvReqData[i] = Vlv_ActrSyncBus.Vlv_ActrSyncArbNormVlvReqInfo.RelsVlvReqData[i]; /* NZ Quick C: RLV */
        Vlv_ActrSyncBus.Vlv_ActrSyncNormVlvReqInfo.SecdCutVlvReqData[i] = Vlv_ActrSyncBus.Vlv_ActrSyncArbNormVlvReqInfo.SecdCutVlvReqData[i];
        Vlv_ActrSyncBus.Vlv_ActrSyncNormVlvReqInfo.CircVlvReqData[i] = Vlv_ActrSyncBus.Vlv_ActrSyncArbNormVlvReqInfo.CircVlvReqData[i]; /* NZ Quick C: CV */
        Vlv_ActrSyncBus.Vlv_ActrSyncNormVlvReqInfo.SimVlvReqData[i] = Vlv_ActrSyncBus.Vlv_ActrSyncArbNormVlvReqInfo.SimVlvReqData[i];
	Vlv_ActrSyncBus.Vlv_ActrSyncBalVlvReqInfo.PressDumpVlvReqData[i] = Vlv_ActrSyncBus.Vlv_ActrSyncArbNormVlvReqInfo.PressDumpVlvReqData[i];	/*NZ Quick-C: PD*/
	
        /* Balance Valve */
        Vlv_ActrSyncBus.Vlv_ActrSyncBalVlvReqInfo.BalVlvReqData[i] = Vlv_ActrSyncBus.Vlv_ActrSyncArbBalVlvReqInfo.BalVlvReqData[i];
    }
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define VLV_ACTRSYNC_STOP_SEC_CODE
#include "Vlv_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
