#define S_FUNCTION_NAME      Ioc_InputSR5ms_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          267
#define WidthOutputPort         264

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Ioc_InputSR5ms.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Ioc_InputSR5msRisngIdxInfo.FlRisngIdx = input[0];
    Ioc_InputSR5msRisngIdxInfo.FrRisngIdx = input[1];
    Ioc_InputSR5msRisngIdxInfo.RlRisngIdx = input[2];
    Ioc_InputSR5msRisngIdxInfo.RrRisngIdx = input[3];
    Ioc_InputSR5msFallIdxInfo.FlFallIdx = input[4];
    Ioc_InputSR5msFallIdxInfo.FrFallIdx = input[5];
    Ioc_InputSR5msFallIdxInfo.RlFallIdx = input[6];
    Ioc_InputSR5msFallIdxInfo.RrFallIdx = input[7];
    Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[0] = input[8];
    Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[1] = input[9];
    Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[2] = input[10];
    Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[3] = input[11];
    Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[4] = input[12];
    Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[5] = input[13];
    Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[6] = input[14];
    Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[7] = input[15];
    Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[8] = input[16];
    Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[9] = input[17];
    Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[10] = input[18];
    Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[11] = input[19];
    Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[12] = input[20];
    Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[13] = input[21];
    Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[14] = input[22];
    Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[15] = input[23];
    Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[16] = input[24];
    Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[17] = input[25];
    Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[18] = input[26];
    Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[19] = input[27];
    Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[20] = input[28];
    Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[21] = input[29];
    Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[22] = input[30];
    Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[23] = input[31];
    Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[24] = input[32];
    Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[25] = input[33];
    Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[26] = input[34];
    Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[27] = input[35];
    Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[28] = input[36];
    Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[29] = input[37];
    Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[30] = input[38];
    Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[31] = input[39];
    Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[0] = input[40];
    Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[1] = input[41];
    Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[2] = input[42];
    Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[3] = input[43];
    Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[4] = input[44];
    Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[5] = input[45];
    Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[6] = input[46];
    Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[7] = input[47];
    Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[8] = input[48];
    Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[9] = input[49];
    Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[10] = input[50];
    Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[11] = input[51];
    Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[12] = input[52];
    Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[13] = input[53];
    Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[14] = input[54];
    Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[15] = input[55];
    Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[16] = input[56];
    Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[17] = input[57];
    Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[18] = input[58];
    Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[19] = input[59];
    Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[20] = input[60];
    Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[21] = input[61];
    Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[22] = input[62];
    Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[23] = input[63];
    Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[24] = input[64];
    Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[25] = input[65];
    Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[26] = input[66];
    Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[27] = input[67];
    Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[28] = input[68];
    Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[29] = input[69];
    Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[30] = input[70];
    Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[31] = input[71];
    Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[0] = input[72];
    Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[1] = input[73];
    Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[2] = input[74];
    Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[3] = input[75];
    Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[4] = input[76];
    Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[5] = input[77];
    Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[6] = input[78];
    Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[7] = input[79];
    Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[8] = input[80];
    Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[9] = input[81];
    Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[10] = input[82];
    Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[11] = input[83];
    Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[12] = input[84];
    Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[13] = input[85];
    Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[14] = input[86];
    Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[15] = input[87];
    Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[16] = input[88];
    Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[17] = input[89];
    Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[18] = input[90];
    Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[19] = input[91];
    Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[20] = input[92];
    Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[21] = input[93];
    Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[22] = input[94];
    Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[23] = input[95];
    Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[24] = input[96];
    Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[25] = input[97];
    Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[26] = input[98];
    Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[27] = input[99];
    Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[28] = input[100];
    Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[29] = input[101];
    Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[30] = input[102];
    Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[31] = input[103];
    Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[0] = input[104];
    Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[1] = input[105];
    Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[2] = input[106];
    Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[3] = input[107];
    Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[4] = input[108];
    Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[5] = input[109];
    Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[6] = input[110];
    Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[7] = input[111];
    Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[8] = input[112];
    Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[9] = input[113];
    Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[10] = input[114];
    Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[11] = input[115];
    Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[12] = input[116];
    Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[13] = input[117];
    Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[14] = input[118];
    Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[15] = input[119];
    Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[16] = input[120];
    Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[17] = input[121];
    Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[18] = input[122];
    Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[19] = input[123];
    Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[20] = input[124];
    Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[21] = input[125];
    Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[22] = input[126];
    Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[23] = input[127];
    Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[24] = input[128];
    Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[25] = input[129];
    Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[26] = input[130];
    Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[27] = input[131];
    Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[28] = input[132];
    Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[29] = input[133];
    Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[30] = input[134];
    Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[31] = input[135];
    Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[0] = input[136];
    Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[1] = input[137];
    Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[2] = input[138];
    Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[3] = input[139];
    Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[4] = input[140];
    Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[5] = input[141];
    Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[6] = input[142];
    Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[7] = input[143];
    Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[8] = input[144];
    Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[9] = input[145];
    Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[10] = input[146];
    Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[11] = input[147];
    Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[12] = input[148];
    Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[13] = input[149];
    Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[14] = input[150];
    Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[15] = input[151];
    Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[16] = input[152];
    Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[17] = input[153];
    Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[18] = input[154];
    Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[19] = input[155];
    Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[20] = input[156];
    Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[21] = input[157];
    Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[22] = input[158];
    Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[23] = input[159];
    Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[24] = input[160];
    Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[25] = input[161];
    Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[26] = input[162];
    Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[27] = input[163];
    Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[28] = input[164];
    Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[29] = input[165];
    Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[30] = input[166];
    Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[31] = input[167];
    Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[0] = input[168];
    Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[1] = input[169];
    Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[2] = input[170];
    Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[3] = input[171];
    Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[4] = input[172];
    Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[5] = input[173];
    Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[6] = input[174];
    Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[7] = input[175];
    Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[8] = input[176];
    Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[9] = input[177];
    Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[10] = input[178];
    Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[11] = input[179];
    Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[12] = input[180];
    Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[13] = input[181];
    Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[14] = input[182];
    Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[15] = input[183];
    Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[16] = input[184];
    Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[17] = input[185];
    Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[18] = input[186];
    Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[19] = input[187];
    Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[20] = input[188];
    Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[21] = input[189];
    Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[22] = input[190];
    Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[23] = input[191];
    Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[24] = input[192];
    Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[25] = input[193];
    Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[26] = input[194];
    Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[27] = input[195];
    Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[28] = input[196];
    Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[29] = input[197];
    Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[30] = input[198];
    Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[31] = input[199];
    Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[0] = input[200];
    Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[1] = input[201];
    Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[2] = input[202];
    Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[3] = input[203];
    Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[4] = input[204];
    Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[5] = input[205];
    Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[6] = input[206];
    Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[7] = input[207];
    Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[8] = input[208];
    Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[9] = input[209];
    Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[10] = input[210];
    Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[11] = input[211];
    Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[12] = input[212];
    Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[13] = input[213];
    Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[14] = input[214];
    Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[15] = input[215];
    Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[16] = input[216];
    Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[17] = input[217];
    Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[18] = input[218];
    Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[19] = input[219];
    Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[20] = input[220];
    Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[21] = input[221];
    Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[22] = input[222];
    Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[23] = input[223];
    Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[24] = input[224];
    Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[25] = input[225];
    Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[26] = input[226];
    Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[27] = input[227];
    Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[28] = input[228];
    Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[29] = input[229];
    Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[30] = input[230];
    Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[31] = input[231];
    Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[0] = input[232];
    Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[1] = input[233];
    Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[2] = input[234];
    Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[3] = input[235];
    Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[4] = input[236];
    Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[5] = input[237];
    Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[6] = input[238];
    Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[7] = input[239];
    Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[8] = input[240];
    Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[9] = input[241];
    Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[10] = input[242];
    Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[11] = input[243];
    Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[12] = input[244];
    Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[13] = input[245];
    Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[14] = input[246];
    Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[15] = input[247];
    Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[16] = input[248];
    Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[17] = input[249];
    Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[18] = input[250];
    Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[19] = input[251];
    Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[20] = input[252];
    Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[21] = input[253];
    Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[22] = input[254];
    Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[23] = input[255];
    Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[24] = input[256];
    Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[25] = input[257];
    Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[26] = input[258];
    Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[27] = input[259];
    Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[28] = input[260];
    Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[29] = input[261];
    Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[30] = input[262];
    Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[31] = input[263];
    Ioc_InputSR5msEcuModeSts = input[264];
    Ioc_InputSR5msAcmAsicInitCompleteFlag = input[265];
    Ioc_InputSR5msFuncInhibitIocSts = input[266];

    Ioc_InputSR5ms();


    output[0] = Ioc_InputSR5msWssMonInfo.FlRisngIdx;
    output[1] = Ioc_InputSR5msWssMonInfo.FlFallIdx;
    output[2] = Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[0];
    output[3] = Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[1];
    output[4] = Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[2];
    output[5] = Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[3];
    output[6] = Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[4];
    output[7] = Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[5];
    output[8] = Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[6];
    output[9] = Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[7];
    output[10] = Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[8];
    output[11] = Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[9];
    output[12] = Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[10];
    output[13] = Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[11];
    output[14] = Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[12];
    output[15] = Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[13];
    output[16] = Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[14];
    output[17] = Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[15];
    output[18] = Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[16];
    output[19] = Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[17];
    output[20] = Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[18];
    output[21] = Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[19];
    output[22] = Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[20];
    output[23] = Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[21];
    output[24] = Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[22];
    output[25] = Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[23];
    output[26] = Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[24];
    output[27] = Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[25];
    output[28] = Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[26];
    output[29] = Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[27];
    output[30] = Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[28];
    output[31] = Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[29];
    output[32] = Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[30];
    output[33] = Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[31];
    output[34] = Ioc_InputSR5msWssMonInfo.FlFallTiStamp[0];
    output[35] = Ioc_InputSR5msWssMonInfo.FlFallTiStamp[1];
    output[36] = Ioc_InputSR5msWssMonInfo.FlFallTiStamp[2];
    output[37] = Ioc_InputSR5msWssMonInfo.FlFallTiStamp[3];
    output[38] = Ioc_InputSR5msWssMonInfo.FlFallTiStamp[4];
    output[39] = Ioc_InputSR5msWssMonInfo.FlFallTiStamp[5];
    output[40] = Ioc_InputSR5msWssMonInfo.FlFallTiStamp[6];
    output[41] = Ioc_InputSR5msWssMonInfo.FlFallTiStamp[7];
    output[42] = Ioc_InputSR5msWssMonInfo.FlFallTiStamp[8];
    output[43] = Ioc_InputSR5msWssMonInfo.FlFallTiStamp[9];
    output[44] = Ioc_InputSR5msWssMonInfo.FlFallTiStamp[10];
    output[45] = Ioc_InputSR5msWssMonInfo.FlFallTiStamp[11];
    output[46] = Ioc_InputSR5msWssMonInfo.FlFallTiStamp[12];
    output[47] = Ioc_InputSR5msWssMonInfo.FlFallTiStamp[13];
    output[48] = Ioc_InputSR5msWssMonInfo.FlFallTiStamp[14];
    output[49] = Ioc_InputSR5msWssMonInfo.FlFallTiStamp[15];
    output[50] = Ioc_InputSR5msWssMonInfo.FlFallTiStamp[16];
    output[51] = Ioc_InputSR5msWssMonInfo.FlFallTiStamp[17];
    output[52] = Ioc_InputSR5msWssMonInfo.FlFallTiStamp[18];
    output[53] = Ioc_InputSR5msWssMonInfo.FlFallTiStamp[19];
    output[54] = Ioc_InputSR5msWssMonInfo.FlFallTiStamp[20];
    output[55] = Ioc_InputSR5msWssMonInfo.FlFallTiStamp[21];
    output[56] = Ioc_InputSR5msWssMonInfo.FlFallTiStamp[22];
    output[57] = Ioc_InputSR5msWssMonInfo.FlFallTiStamp[23];
    output[58] = Ioc_InputSR5msWssMonInfo.FlFallTiStamp[24];
    output[59] = Ioc_InputSR5msWssMonInfo.FlFallTiStamp[25];
    output[60] = Ioc_InputSR5msWssMonInfo.FlFallTiStamp[26];
    output[61] = Ioc_InputSR5msWssMonInfo.FlFallTiStamp[27];
    output[62] = Ioc_InputSR5msWssMonInfo.FlFallTiStamp[28];
    output[63] = Ioc_InputSR5msWssMonInfo.FlFallTiStamp[29];
    output[64] = Ioc_InputSR5msWssMonInfo.FlFallTiStamp[30];
    output[65] = Ioc_InputSR5msWssMonInfo.FlFallTiStamp[31];
    output[66] = Ioc_InputSR5msWssMonInfo.FrRisngIdx;
    output[67] = Ioc_InputSR5msWssMonInfo.FrFallIdx;
    output[68] = Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[0];
    output[69] = Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[1];
    output[70] = Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[2];
    output[71] = Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[3];
    output[72] = Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[4];
    output[73] = Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[5];
    output[74] = Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[6];
    output[75] = Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[7];
    output[76] = Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[8];
    output[77] = Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[9];
    output[78] = Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[10];
    output[79] = Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[11];
    output[80] = Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[12];
    output[81] = Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[13];
    output[82] = Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[14];
    output[83] = Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[15];
    output[84] = Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[16];
    output[85] = Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[17];
    output[86] = Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[18];
    output[87] = Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[19];
    output[88] = Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[20];
    output[89] = Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[21];
    output[90] = Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[22];
    output[91] = Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[23];
    output[92] = Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[24];
    output[93] = Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[25];
    output[94] = Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[26];
    output[95] = Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[27];
    output[96] = Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[28];
    output[97] = Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[29];
    output[98] = Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[30];
    output[99] = Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[31];
    output[100] = Ioc_InputSR5msWssMonInfo.FrFallTiStamp[0];
    output[101] = Ioc_InputSR5msWssMonInfo.FrFallTiStamp[1];
    output[102] = Ioc_InputSR5msWssMonInfo.FrFallTiStamp[2];
    output[103] = Ioc_InputSR5msWssMonInfo.FrFallTiStamp[3];
    output[104] = Ioc_InputSR5msWssMonInfo.FrFallTiStamp[4];
    output[105] = Ioc_InputSR5msWssMonInfo.FrFallTiStamp[5];
    output[106] = Ioc_InputSR5msWssMonInfo.FrFallTiStamp[6];
    output[107] = Ioc_InputSR5msWssMonInfo.FrFallTiStamp[7];
    output[108] = Ioc_InputSR5msWssMonInfo.FrFallTiStamp[8];
    output[109] = Ioc_InputSR5msWssMonInfo.FrFallTiStamp[9];
    output[110] = Ioc_InputSR5msWssMonInfo.FrFallTiStamp[10];
    output[111] = Ioc_InputSR5msWssMonInfo.FrFallTiStamp[11];
    output[112] = Ioc_InputSR5msWssMonInfo.FrFallTiStamp[12];
    output[113] = Ioc_InputSR5msWssMonInfo.FrFallTiStamp[13];
    output[114] = Ioc_InputSR5msWssMonInfo.FrFallTiStamp[14];
    output[115] = Ioc_InputSR5msWssMonInfo.FrFallTiStamp[15];
    output[116] = Ioc_InputSR5msWssMonInfo.FrFallTiStamp[16];
    output[117] = Ioc_InputSR5msWssMonInfo.FrFallTiStamp[17];
    output[118] = Ioc_InputSR5msWssMonInfo.FrFallTiStamp[18];
    output[119] = Ioc_InputSR5msWssMonInfo.FrFallTiStamp[19];
    output[120] = Ioc_InputSR5msWssMonInfo.FrFallTiStamp[20];
    output[121] = Ioc_InputSR5msWssMonInfo.FrFallTiStamp[21];
    output[122] = Ioc_InputSR5msWssMonInfo.FrFallTiStamp[22];
    output[123] = Ioc_InputSR5msWssMonInfo.FrFallTiStamp[23];
    output[124] = Ioc_InputSR5msWssMonInfo.FrFallTiStamp[24];
    output[125] = Ioc_InputSR5msWssMonInfo.FrFallTiStamp[25];
    output[126] = Ioc_InputSR5msWssMonInfo.FrFallTiStamp[26];
    output[127] = Ioc_InputSR5msWssMonInfo.FrFallTiStamp[27];
    output[128] = Ioc_InputSR5msWssMonInfo.FrFallTiStamp[28];
    output[129] = Ioc_InputSR5msWssMonInfo.FrFallTiStamp[29];
    output[130] = Ioc_InputSR5msWssMonInfo.FrFallTiStamp[30];
    output[131] = Ioc_InputSR5msWssMonInfo.FrFallTiStamp[31];
    output[132] = Ioc_InputSR5msWssMonInfo.RlRisngIdx;
    output[133] = Ioc_InputSR5msWssMonInfo.RlFallIdx;
    output[134] = Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[0];
    output[135] = Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[1];
    output[136] = Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[2];
    output[137] = Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[3];
    output[138] = Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[4];
    output[139] = Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[5];
    output[140] = Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[6];
    output[141] = Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[7];
    output[142] = Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[8];
    output[143] = Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[9];
    output[144] = Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[10];
    output[145] = Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[11];
    output[146] = Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[12];
    output[147] = Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[13];
    output[148] = Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[14];
    output[149] = Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[15];
    output[150] = Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[16];
    output[151] = Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[17];
    output[152] = Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[18];
    output[153] = Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[19];
    output[154] = Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[20];
    output[155] = Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[21];
    output[156] = Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[22];
    output[157] = Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[23];
    output[158] = Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[24];
    output[159] = Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[25];
    output[160] = Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[26];
    output[161] = Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[27];
    output[162] = Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[28];
    output[163] = Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[29];
    output[164] = Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[30];
    output[165] = Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[31];
    output[166] = Ioc_InputSR5msWssMonInfo.RlFallTiStamp[0];
    output[167] = Ioc_InputSR5msWssMonInfo.RlFallTiStamp[1];
    output[168] = Ioc_InputSR5msWssMonInfo.RlFallTiStamp[2];
    output[169] = Ioc_InputSR5msWssMonInfo.RlFallTiStamp[3];
    output[170] = Ioc_InputSR5msWssMonInfo.RlFallTiStamp[4];
    output[171] = Ioc_InputSR5msWssMonInfo.RlFallTiStamp[5];
    output[172] = Ioc_InputSR5msWssMonInfo.RlFallTiStamp[6];
    output[173] = Ioc_InputSR5msWssMonInfo.RlFallTiStamp[7];
    output[174] = Ioc_InputSR5msWssMonInfo.RlFallTiStamp[8];
    output[175] = Ioc_InputSR5msWssMonInfo.RlFallTiStamp[9];
    output[176] = Ioc_InputSR5msWssMonInfo.RlFallTiStamp[10];
    output[177] = Ioc_InputSR5msWssMonInfo.RlFallTiStamp[11];
    output[178] = Ioc_InputSR5msWssMonInfo.RlFallTiStamp[12];
    output[179] = Ioc_InputSR5msWssMonInfo.RlFallTiStamp[13];
    output[180] = Ioc_InputSR5msWssMonInfo.RlFallTiStamp[14];
    output[181] = Ioc_InputSR5msWssMonInfo.RlFallTiStamp[15];
    output[182] = Ioc_InputSR5msWssMonInfo.RlFallTiStamp[16];
    output[183] = Ioc_InputSR5msWssMonInfo.RlFallTiStamp[17];
    output[184] = Ioc_InputSR5msWssMonInfo.RlFallTiStamp[18];
    output[185] = Ioc_InputSR5msWssMonInfo.RlFallTiStamp[19];
    output[186] = Ioc_InputSR5msWssMonInfo.RlFallTiStamp[20];
    output[187] = Ioc_InputSR5msWssMonInfo.RlFallTiStamp[21];
    output[188] = Ioc_InputSR5msWssMonInfo.RlFallTiStamp[22];
    output[189] = Ioc_InputSR5msWssMonInfo.RlFallTiStamp[23];
    output[190] = Ioc_InputSR5msWssMonInfo.RlFallTiStamp[24];
    output[191] = Ioc_InputSR5msWssMonInfo.RlFallTiStamp[25];
    output[192] = Ioc_InputSR5msWssMonInfo.RlFallTiStamp[26];
    output[193] = Ioc_InputSR5msWssMonInfo.RlFallTiStamp[27];
    output[194] = Ioc_InputSR5msWssMonInfo.RlFallTiStamp[28];
    output[195] = Ioc_InputSR5msWssMonInfo.RlFallTiStamp[29];
    output[196] = Ioc_InputSR5msWssMonInfo.RlFallTiStamp[30];
    output[197] = Ioc_InputSR5msWssMonInfo.RlFallTiStamp[31];
    output[198] = Ioc_InputSR5msWssMonInfo.RrRisngIdx;
    output[199] = Ioc_InputSR5msWssMonInfo.RrFallIdx;
    output[200] = Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[0];
    output[201] = Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[1];
    output[202] = Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[2];
    output[203] = Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[3];
    output[204] = Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[4];
    output[205] = Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[5];
    output[206] = Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[6];
    output[207] = Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[7];
    output[208] = Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[8];
    output[209] = Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[9];
    output[210] = Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[10];
    output[211] = Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[11];
    output[212] = Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[12];
    output[213] = Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[13];
    output[214] = Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[14];
    output[215] = Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[15];
    output[216] = Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[16];
    output[217] = Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[17];
    output[218] = Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[18];
    output[219] = Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[19];
    output[220] = Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[20];
    output[221] = Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[21];
    output[222] = Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[22];
    output[223] = Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[23];
    output[224] = Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[24];
    output[225] = Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[25];
    output[226] = Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[26];
    output[227] = Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[27];
    output[228] = Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[28];
    output[229] = Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[29];
    output[230] = Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[30];
    output[231] = Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[31];
    output[232] = Ioc_InputSR5msWssMonInfo.RrFallTiStamp[0];
    output[233] = Ioc_InputSR5msWssMonInfo.RrFallTiStamp[1];
    output[234] = Ioc_InputSR5msWssMonInfo.RrFallTiStamp[2];
    output[235] = Ioc_InputSR5msWssMonInfo.RrFallTiStamp[3];
    output[236] = Ioc_InputSR5msWssMonInfo.RrFallTiStamp[4];
    output[237] = Ioc_InputSR5msWssMonInfo.RrFallTiStamp[5];
    output[238] = Ioc_InputSR5msWssMonInfo.RrFallTiStamp[6];
    output[239] = Ioc_InputSR5msWssMonInfo.RrFallTiStamp[7];
    output[240] = Ioc_InputSR5msWssMonInfo.RrFallTiStamp[8];
    output[241] = Ioc_InputSR5msWssMonInfo.RrFallTiStamp[9];
    output[242] = Ioc_InputSR5msWssMonInfo.RrFallTiStamp[10];
    output[243] = Ioc_InputSR5msWssMonInfo.RrFallTiStamp[11];
    output[244] = Ioc_InputSR5msWssMonInfo.RrFallTiStamp[12];
    output[245] = Ioc_InputSR5msWssMonInfo.RrFallTiStamp[13];
    output[246] = Ioc_InputSR5msWssMonInfo.RrFallTiStamp[14];
    output[247] = Ioc_InputSR5msWssMonInfo.RrFallTiStamp[15];
    output[248] = Ioc_InputSR5msWssMonInfo.RrFallTiStamp[16];
    output[249] = Ioc_InputSR5msWssMonInfo.RrFallTiStamp[17];
    output[250] = Ioc_InputSR5msWssMonInfo.RrFallTiStamp[18];
    output[251] = Ioc_InputSR5msWssMonInfo.RrFallTiStamp[19];
    output[252] = Ioc_InputSR5msWssMonInfo.RrFallTiStamp[20];
    output[253] = Ioc_InputSR5msWssMonInfo.RrFallTiStamp[21];
    output[254] = Ioc_InputSR5msWssMonInfo.RrFallTiStamp[22];
    output[255] = Ioc_InputSR5msWssMonInfo.RrFallTiStamp[23];
    output[256] = Ioc_InputSR5msWssMonInfo.RrFallTiStamp[24];
    output[257] = Ioc_InputSR5msWssMonInfo.RrFallTiStamp[25];
    output[258] = Ioc_InputSR5msWssMonInfo.RrFallTiStamp[26];
    output[259] = Ioc_InputSR5msWssMonInfo.RrFallTiStamp[27];
    output[260] = Ioc_InputSR5msWssMonInfo.RrFallTiStamp[28];
    output[261] = Ioc_InputSR5msWssMonInfo.RrFallTiStamp[29];
    output[262] = Ioc_InputSR5msWssMonInfo.RrFallTiStamp[30];
    output[263] = Ioc_InputSR5msWssMonInfo.RrFallTiStamp[31];
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Ioc_InputSR5ms_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
