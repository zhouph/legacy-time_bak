#define S_FUNCTION_NAME      Ioc_InputSR1ms_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          92
#define WidthOutputPort         96

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Ioc_InputSR1ms.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Ioc_InputSR1msAchAdcData.ASIC_RX_MSG_13mu10_Rx_13_ADC_DATA_OUT = input[0];
    Ioc_InputSR1msAchAdcData.ASIC_RX_MSG_13mu1_Rx_13_ADC_BUSY = input[1];
    Ioc_InputSR1msSwTrigPwrInfo.Ext5vMon = input[2];
    Ioc_InputSR1msSwTrigPwrInfo.CEMon = input[3];
    Ioc_InputSR1msSwTrigPwrInfo.Int5vMon = input[4];
    Ioc_InputSR1msSwTrigPwrInfo.CSPMon = input[5];
    Ioc_InputSR1msSwTrigPwrInfo.Vbatt01Mon = input[6];
    Ioc_InputSR1msSwTrigPwrInfo.Vbatt02Mon = input[7];
    Ioc_InputSR1msSwTrigPwrInfo.VddMon = input[8];
    Ioc_InputSR1msSwTrigPwrInfo.Vdd3Mon = input[9];
    Ioc_InputSR1msSwTrigPwrInfo.Vdd5Mon = input[10];
    Ioc_InputSR1msSwTrigFspInfo.FspAbsHMon = input[11];
    Ioc_InputSR1msSwTrigFspInfo.FspAbsMon = input[12];
    Ioc_InputSR1msSwTrigFspInfo.FspCbsHMon = input[13];
    Ioc_InputSR1msSwTrigFspInfo.FspCbsMon = input[14];
    Ioc_InputSR1msSwTrigTmpInfo.LineTestMon = input[15];
    Ioc_InputSR1msSwTrigTmpInfo.TempMon = input[16];
    Ioc_InputSR1msSwTrigMotInfo.MotPwrMon = input[17];
    Ioc_InputSR1msSwTrigMotInfo.StarMon = input[18];
    Ioc_InputSR1msSwTrigMotInfo.UoutMon = input[19];
    Ioc_InputSR1msSwTrigMotInfo.VoutMon = input[20];
    Ioc_InputSR1msSwTrigMotInfo.WoutMon = input[21];
    Ioc_InputSR1msSwTrigPdtInfo.PdfSigMon = input[22];
    Ioc_InputSR1msSwTrigPdtInfo.Pdt5vMon = input[23];
    Ioc_InputSR1msSwTrigPdtInfo.Pdf5vMon = input[24];
    Ioc_InputSR1msSwTrigPdtInfo.PdtSigMon = input[25];
    Ioc_InputSR1msSwTrigMocInfo.RlIMainMon = input[26];
    Ioc_InputSR1msSwTrigMocInfo.RlISubMonFs = input[27];
    Ioc_InputSR1msSwTrigMocInfo.RlMocNegMon = input[28];
    Ioc_InputSR1msSwTrigMocInfo.RlMocPosMon = input[29];
    Ioc_InputSR1msSwTrigMocInfo.RrIMainMon = input[30];
    Ioc_InputSR1msSwTrigMocInfo.RrISubMonFs = input[31];
    Ioc_InputSR1msSwTrigMocInfo.RrMocNegMon = input[32];
    Ioc_InputSR1msSwTrigMocInfo.RrMocPosMon = input[33];
    Ioc_InputSR1msSwTrigMocInfo.BFLMon = input[34];
    Ioc_InputSR1msHwTrigVlvInfo.VlvCVMon = input[35];
    Ioc_InputSR1msHwTrigVlvInfo.VlvRLVMon = input[36];
    Ioc_InputSR1msHwTrigVlvInfo.VlvCutPMon = input[37];
    Ioc_InputSR1msHwTrigVlvInfo.VlvCutSMon = input[38];
    Ioc_InputSR1msHwTrigVlvInfo.VlvSimMon = input[39];
    Ioc_InputSR1msSwTrigEscInfo.IF_A7_MON = input[40];
    Ioc_InputSR1msSwTrigEscInfo.IF_A2_MON = input[41];
    Ioc_InputSR1msSwTrigEscInfo.P1_NEG_MON = input[42];
    Ioc_InputSR1msSwTrigEscInfo.IF_A6_MON = input[43];
    Ioc_InputSR1msSwTrigEscInfo.MTR_FW_S_MON = input[44];
    Ioc_InputSR1msSwTrigEscInfo.IF_A4_MON = input[45];
    Ioc_InputSR1msSwTrigEscInfo.MTP_MON = input[46];
    Ioc_InputSR1msSwTrigEscInfo.IF_A3_MON = input[47];
    Ioc_InputSR1msSwTrigEscInfo.P1_POS_MON = input[48];
    Ioc_InputSR1msSwTrigEscInfo.IF_A5_MON = input[49];
    Ioc_InputSR1msSwTrigEscInfo.VDD_MON = input[50];
    Ioc_InputSR1msSwTrigEscInfo.COOL_MON = input[51];
    Ioc_InputSR1msSwTrigEscInfo.CAN_L_MON = input[52];
    Ioc_InputSR1msSwTrigEscInfo.P3_NEG_MON = input[53];
    Ioc_InputSR1msSwTrigEscInfo.IF_A1_MON = input[54];
    Ioc_InputSR1msSwTrigEscInfo.P2_POS_MON = input[55];
    Ioc_InputSR1msSwTrigEscInfo.IF_A8_MON = input[56];
    Ioc_InputSR1msSwTrigEscInfo.IF_A9_MON = input[57];
    Ioc_InputSR1msSwTrigEscInfo.P2_NEG_MON = input[58];
    Ioc_InputSR1msSwTrigEscInfo.P3_POS_MON = input[59];
    Ioc_InputSR1msSwTrigEscInfo.OP_OUT_MON = input[60];
    Ioc_InputSR1msSentHPressureInfo.McpPresData1 = input[61];
    Ioc_InputSR1msSentHPressureInfo.McpPresData2 = input[62];
    Ioc_InputSR1msSentHPressureInfo.McpSentCrc = input[63];
    Ioc_InputSR1msSentHPressureInfo.McpSentData = input[64];
    Ioc_InputSR1msSentHPressureInfo.McpSentMsgId = input[65];
    Ioc_InputSR1msSentHPressureInfo.McpSentSerialCrc = input[66];
    Ioc_InputSR1msSentHPressureInfo.McpSentConfig = input[67];
    Ioc_InputSR1msSentHPressureInfo.McpSentInvalid = input[68];
    Ioc_InputSR1msSentHPressureInfo.MCPSentSerialInvalid = input[69];
    Ioc_InputSR1msSentHPressureInfo.Wlp1PresData1 = input[70];
    Ioc_InputSR1msSentHPressureInfo.Wlp1PresData2 = input[71];
    Ioc_InputSR1msSentHPressureInfo.Wlp1SentCrc = input[72];
    Ioc_InputSR1msSentHPressureInfo.Wlp1SentData = input[73];
    Ioc_InputSR1msSentHPressureInfo.Wlp1SentMsgId = input[74];
    Ioc_InputSR1msSentHPressureInfo.Wlp1SentSerialCrc = input[75];
    Ioc_InputSR1msSentHPressureInfo.Wlp1SentConfig = input[76];
    Ioc_InputSR1msSentHPressureInfo.Wlp1SentInvalid = input[77];
    Ioc_InputSR1msSentHPressureInfo.Wlp1SentSerialInvalid = input[78];
    Ioc_InputSR1msSentHPressureInfo.Wlp2PresData1 = input[79];
    Ioc_InputSR1msSentHPressureInfo.Wlp2PresData2 = input[80];
    Ioc_InputSR1msSentHPressureInfo.Wlp2SentCrc = input[81];
    Ioc_InputSR1msSentHPressureInfo.Wlp2SentData = input[82];
    Ioc_InputSR1msSentHPressureInfo.Wlp2SentMsgId = input[83];
    Ioc_InputSR1msSentHPressureInfo.Wlp2SentSerialCrc = input[84];
    Ioc_InputSR1msSentHPressureInfo.Wlp2SentConfig = input[85];
    Ioc_InputSR1msSentHPressureInfo.Wlp2SentInvalid = input[86];
    Ioc_InputSR1msSentHPressureInfo.Wlp2SentSerialInvalid = input[87];
    Ioc_InputSR1msEcuModeSts = input[88];
    Ioc_InputSR1msAcmAsicInitCompleteFlag = input[89];
    Ioc_InputSR1msAdcInvalid = input[90];
    Ioc_InputSR1msFuncInhibitIocSts = input[91];

    Ioc_InputSR1ms();


    output[0] = Ioc_InputSR1msPwrMonInfoEsc.VoltVBatt01Mon_Esc;
    output[1] = Ioc_InputSR1msPwrMonInfoEsc.VoltVBatt02Mon_Esc;
    output[2] = Ioc_InputSR1msPwrMonInfoEsc.VoltGio2Mon_Esc;
    output[3] = Ioc_InputSR1msPwrMonInfoEsc.VoltExt5VSupplyMon_Esc;
    output[4] = Ioc_InputSR1msPwrMonInfoEsc.VoltExt12VSupplyMon_Esc;
    output[5] = Ioc_InputSR1msPwrMonInfoEsc.VoltSwVpwrMon_Esc;
    output[6] = Ioc_InputSR1msPwrMonInfoEsc.VoltVpwrMon_Esc;
    output[7] = Ioc_InputSR1msPwrMonInfoEsc.VoltGio1Mon_Esc;
    output[8] = Ioc_InputSR1msPwrMonInfoEsc.VoltIgnMon_Esc;
    output[9] = Ioc_InputSR1msVlvMonInfoEsc.VoltFspMon_Esc;
    output[10] = Ioc_InputSR1msMotMonInfoEsc.VoltMtpMon_Esc;
    output[11] = Ioc_InputSR1msMotMonInfoEsc.VoltMtrFwSMon_Esc;
    output[12] = Ioc_InputSR1msEpbMonInfoEsc.VoltEpbPwrMon_Esc;
    output[13] = Ioc_InputSR1msEpbMonInfoEsc.VoltEpbI1LMon_Esc;
    output[14] = Ioc_InputSR1msEpbMonInfoEsc.VoltEpbI1RMon_Esc;
    output[15] = Ioc_InputSR1msEpbMonInfoEsc.VoltEpbI2LMon_Esc;
    output[16] = Ioc_InputSR1msEpbMonInfoEsc.VoltEpbI2RMon_Esc;
    output[17] = Ioc_InputSR1msEpbMonInfoEsc.VoltEpbMonLPlus_Esc;
    output[18] = Ioc_InputSR1msEpbMonInfoEsc.VoltEpbMonLMinus_Esc;
    output[19] = Ioc_InputSR1msEpbMonInfoEsc.VoltEpbMonRPlus_Esc;
    output[20] = Ioc_InputSR1msEpbMonInfoEsc.VoltEpbMonRMinus_Esc;
    output[21] = Ioc_InputSR1msReservedMonInfoEsc.VoltMcuAdc1_Esc;
    output[22] = Ioc_InputSR1msReservedMonInfoEsc.VoltMcuAdc2_Esc;
    output[23] = Ioc_InputSR1msReservedMonInfoEsc.VoltMcuAdc3_Esc;
    output[24] = Ioc_InputSR1msReservedMonInfoEsc.VoltMcuAdc4_Esc;
    output[25] = Ioc_InputSR1msReservedMonInfoEsc.VoltMcuAdc5_Esc;
    output[26] = Ioc_InputSR1msHalPressureInfo.McpPresData1;
    output[27] = Ioc_InputSR1msHalPressureInfo.McpPresData2;
    output[28] = Ioc_InputSR1msHalPressureInfo.McpSentCrc;
    output[29] = Ioc_InputSR1msHalPressureInfo.McpSentData;
    output[30] = Ioc_InputSR1msHalPressureInfo.McpSentMsgId;
    output[31] = Ioc_InputSR1msHalPressureInfo.McpSentSerialCrc;
    output[32] = Ioc_InputSR1msHalPressureInfo.McpSentConfig;
    output[33] = Ioc_InputSR1msHalPressureInfo.Wlp1PresData1;
    output[34] = Ioc_InputSR1msHalPressureInfo.Wlp1PresData2;
    output[35] = Ioc_InputSR1msHalPressureInfo.Wlp1SentCrc;
    output[36] = Ioc_InputSR1msHalPressureInfo.Wlp1SentData;
    output[37] = Ioc_InputSR1msHalPressureInfo.Wlp1SentMsgId;
    output[38] = Ioc_InputSR1msHalPressureInfo.Wlp1SentSerialCrc;
    output[39] = Ioc_InputSR1msHalPressureInfo.Wlp1SentConfig;
    output[40] = Ioc_InputSR1msHalPressureInfo.Wlp2PresData1;
    output[41] = Ioc_InputSR1msHalPressureInfo.Wlp2PresData2;
    output[42] = Ioc_InputSR1msHalPressureInfo.Wlp2SentCrc;
    output[43] = Ioc_InputSR1msHalPressureInfo.Wlp2SentData;
    output[44] = Ioc_InputSR1msHalPressureInfo.Wlp2SentMsgId;
    output[45] = Ioc_InputSR1msHalPressureInfo.Wlp2SentSerialCrc;
    output[46] = Ioc_InputSR1msHalPressureInfo.Wlp2SentConfig;
    output[47] = Ioc_InputSR1msMotMonInfo.MotPwrVoltMon;
    output[48] = Ioc_InputSR1msMotMonInfo.MotStarMon;
    output[49] = Ioc_InputSR1msMotVoltsMonInfo.MotVoltPhUMon;
    output[50] = Ioc_InputSR1msMotVoltsMonInfo.MotVoltPhVMon;
    output[51] = Ioc_InputSR1msMotVoltsMonInfo.MotVoltPhWMon;
    output[52] = Ioc_InputSR1msPedlSigMonInfo.PdfSigMon;
    output[53] = Ioc_InputSR1msPedlSigMonInfo.PdtSigMon;
    output[54] = Ioc_InputSR1msPedlPwrMonInfo.Pdt5vMon;
    output[55] = Ioc_InputSR1msPedlPwrMonInfo.Pdf5vMon;
    output[56] = Ioc_InputSR1msMocMonInfo.RlCurrMainMon;
    output[57] = Ioc_InputSR1msMocMonInfo.RlCurrSubMon;
    output[58] = Ioc_InputSR1msMocMonInfo.RlMocNegMon;
    output[59] = Ioc_InputSR1msMocMonInfo.RlMocPosMon;
    output[60] = Ioc_InputSR1msMocMonInfo.RrCurrMainMon;
    output[61] = Ioc_InputSR1msMocMonInfo.RrCurrSubMon;
    output[62] = Ioc_InputSR1msMocMonInfo.RrMocNegMon;
    output[63] = Ioc_InputSR1msMocMonInfo.RrMocPosMon;
    output[64] = Ioc_InputSR1msVlvMonInfo.VlvCVMon;
    output[65] = Ioc_InputSR1msVlvMonInfo.VlvRLVMon;
    output[66] = Ioc_InputSR1msVlvMonInfo.PrimCutVlvMon;
    output[67] = Ioc_InputSR1msVlvMonInfo.SecdCutVlvMon;
    output[68] = Ioc_InputSR1msVlvMonInfo.SimVlvMon;
    output[69] = Ioc_InputSR1msWhlVlvFbMonInfo.FlOvCurrMon;
    output[70] = Ioc_InputSR1msWhlVlvFbMonInfo.FlIvDutyMon;
    output[71] = Ioc_InputSR1msWhlVlvFbMonInfo.FrOvCurrMon;
    output[72] = Ioc_InputSR1msWhlVlvFbMonInfo.FrIvDutyMon;
    output[73] = Ioc_InputSR1msWhlVlvFbMonInfo.RlOvCurrMon;
    output[74] = Ioc_InputSR1msWhlVlvFbMonInfo.RlIvDutyMon;
    output[75] = Ioc_InputSR1msWhlVlvFbMonInfo.RrOvCurrMon;
    output[76] = Ioc_InputSR1msWhlVlvFbMonInfo.RrIvDutyMon;
    output[77] = Ioc_InputSR1msVBatt1Mon;
    output[78] = Ioc_InputSR1msVBatt2Mon;
    output[79] = Ioc_InputSR1msFspCbsHMon;
    output[80] = Ioc_InputSR1msFspCbsMon;
    output[81] = Ioc_InputSR1msFspAbsHMon;
    output[82] = Ioc_InputSR1msFspAbsMon;
    output[83] = Ioc_InputSR1msExt5vMon;
    output[84] = Ioc_InputSR1msCEMon;
    output[85] = Ioc_InputSR1msLineTestMon;
    output[86] = Ioc_InputSR1msTempMon;
    output[87] = Ioc_InputSR1msVdd1Mon;
    output[88] = Ioc_InputSR1msVdd2Mon;
    output[89] = Ioc_InputSR1msVdd3Mon;
    output[90] = Ioc_InputSR1msVdd4Mon;
    output[91] = Ioc_InputSR1msVdd5Mon;
    output[92] = Ioc_InputSR1msVddMon;
    output[93] = Ioc_InputSR1msCspMon;
    output[94] = Ioc_InputSR1msInt5vMon;
    output[95] = Ioc_InputSR1msBFLMon;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Ioc_InputSR1ms_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
