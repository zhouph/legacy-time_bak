#define S_FUNCTION_NAME      Ioc_InputCS1ms_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          3
#define WidthOutputPort         29

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Ioc_InputCS1ms.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Ioc_InputCS1msEcuModeSts = input[0];
    Ioc_InputCS1msAcmAsicInitCompleteFlag = input[1];
    Ioc_InputCS1msFuncInhibitIocSts = input[2];

    Ioc_InputCS1ms();


    output[0] = Ioc_InputCS1msSwtMonInfo.AvhSwtMon;
    output[1] = Ioc_InputCS1msSwtMonInfo.BflSwtMon;
    output[2] = Ioc_InputCS1msSwtMonInfo.BlsSwtMon;
    output[3] = Ioc_InputCS1msSwtMonInfo.BsSwtMon;
    output[4] = Ioc_InputCS1msSwtMonInfo.DoorSwtMon;
    output[5] = Ioc_InputCS1msSwtMonInfo.EscSwtMon;
    output[6] = Ioc_InputCS1msSwtMonInfo.FlexBrkASwtMon;
    output[7] = Ioc_InputCS1msSwtMonInfo.FlexBrkBSwtMon;
    output[8] = Ioc_InputCS1msSwtMonInfo.HzrdSwtMon;
    output[9] = Ioc_InputCS1msSwtMonInfo.HdcSwtMon;
    output[10] = Ioc_InputCS1msSwtMonInfo.PbSwtMon;
    output[11] = Ioc_InputCS1msSwtMonInfoEsc.BlsSwtMon_Esc;
    output[12] = Ioc_InputCS1msSwtMonInfoEsc.AvhSwtMon_Esc;
    output[13] = Ioc_InputCS1msSwtMonInfoEsc.EscSwtMon_Esc;
    output[14] = Ioc_InputCS1msSwtMonInfoEsc.HdcSwtMon_Esc;
    output[15] = Ioc_InputCS1msSwtMonInfoEsc.PbSwtMon_Esc;
    output[16] = Ioc_InputCS1msSwtMonInfoEsc.GearRSwtMon_Esc;
    output[17] = Ioc_InputCS1msSwtMonInfoEsc.BlfSwtMon_Esc;
    output[18] = Ioc_InputCS1msSwtMonInfoEsc.ClutchSwtMon_Esc;
    output[19] = Ioc_InputCS1msSwtMonInfoEsc.ItpmsSwtMon_Esc;
    output[20] = Ioc_InputCS1msRlyMonInfo.RlyDbcMon;
    output[21] = Ioc_InputCS1msRlyMonInfo.RlyEssMon;
    output[22] = Ioc_InputCS1msRlyMonInfo.RlyFault;
    output[23] = Ioc_InputCS1msSwLineMonInfo.Sw1LineCMon;
    output[24] = Ioc_InputCS1msSwLineMonInfo.Sw2LineBMon;
    output[25] = Ioc_InputCS1msSwLineMonInfo.Sw3LineEMon;
    output[26] = Ioc_InputCS1msSwLineMonInfo.Sw4LineFMon;
    output[27] = Ioc_InputCS1msRsmDbcMon;
    output[28] = Ioc_InputCS1msPbcVdaAi;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Ioc_InputCS1ms_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
