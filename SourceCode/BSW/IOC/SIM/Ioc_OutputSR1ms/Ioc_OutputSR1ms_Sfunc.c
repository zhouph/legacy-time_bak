#define S_FUNCTION_NAME      Ioc_OutputSR1ms_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          16
#define WidthOutputPort         32

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Ioc_OutputSR1ms.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Ioc_OutputSR1msWhlVlvDrvInfo.FlOvDrvData = input[0];
    Ioc_OutputSR1msWhlVlvDrvInfo.FlIvDrvData = input[1];
    Ioc_OutputSR1msWhlVlvDrvInfo.FrOvDrvData = input[2];
    Ioc_OutputSR1msWhlVlvDrvInfo.FrIvDrvData = input[3];
    Ioc_OutputSR1msWhlVlvDrvInfo.RlOvDrvData = input[4];
    Ioc_OutputSR1msWhlVlvDrvInfo.RlIvDrvData = input[5];
    Ioc_OutputSR1msWhlVlvDrvInfo.RrOvDrvData = input[6];
    Ioc_OutputSR1msWhlVlvDrvInfo.RrIvDrvData = input[7];
    Ioc_OutputSR1msBalVlvDrvInfo.PrimBalVlvDrvData = input[8];
    Ioc_OutputSR1msBalVlvDrvInfo.PressDumpVlvDrvData = input[9];
    Ioc_OutputSR1msBalVlvDrvInfo.ChmbBalVlvDrvData = input[10];
    Ioc_OutputSR1msFsrCbsDrvInfo.FsrCbsDrv = input[11];
    Ioc_OutputSR1msFsrAbsDrvInfo.FsrAbsDrv = input[12];
    Ioc_OutputSR1msEcuModeSts = input[13];
    Ioc_OutputSR1msAcmAsicInitCompleteFlag = input[14];
    Ioc_OutputSR1msFuncInhibitIocSts = input[15];

    Ioc_OutputSR1ms();


    output[0] = Ioc_OutputSR1msIocDcMtrDutyData.MtrDutyDataFlg;
    output[1] = Ioc_OutputSR1msIocDcMtrDutyData.ACH_Tx8_PUMP_DTY_PWM;
    output[2] = Ioc_OutputSR1msIocDcMtrFreqData.MtrFreqDataFlg;
    output[3] = Ioc_OutputSR1msIocDcMtrFreqData.ACH_Tx8_PUMP_TCK_PWM;
    output[4] = Ioc_OutputSR1msIocVlvNo0Data.VlvNo0DataFlg;
    output[5] = Ioc_OutputSR1msIocVlvNo0Data.ACH_TxValve_SET_POINT;
    output[6] = Ioc_OutputSR1msIocVlvNo1Data.VlvNo1DataFlg;
    output[7] = Ioc_OutputSR1msIocVlvNo1Data.ACH_TxValve_SET_POINT;
    output[8] = Ioc_OutputSR1msIocVlvNo2Data.VlvNo2DataFlg;
    output[9] = Ioc_OutputSR1msIocVlvNo2Data.ACH_TxValve_SET_POINT;
    output[10] = Ioc_OutputSR1msIocVlvNo3Data.VlvNo3DataFlg;
    output[11] = Ioc_OutputSR1msIocVlvNo3Data.ACH_TxValve_SET_POINT;
    output[12] = Ioc_OutputSR1msIocVlvTc0Data.VlvTc0DataFlg;
    output[13] = Ioc_OutputSR1msIocVlvTc0Data.ACH_TxValve_SET_POINT;
    output[14] = Ioc_OutputSR1msIocVlvTc1Data.VlvTc1DataFlg;
    output[15] = Ioc_OutputSR1msIocVlvTc1Data.ACH_TxValve_SET_POINT;
    output[16] = Ioc_OutputSR1msIocVlvEsv0Data.VlvEsv0DataFlg;
    output[17] = Ioc_OutputSR1msIocVlvEsv0Data.ACH_TxValve_SET_POINT;
    output[18] = Ioc_OutputSR1msIocVlvEsv1Data.VlvEsv1DataFlg;
    output[19] = Ioc_OutputSR1msIocVlvEsv1Data.ACH_TxValve_SET_POINT;
    output[20] = Ioc_OutputSR1msIocVlvNc0Data.VlvNc0DataFlg;
    output[21] = Ioc_OutputSR1msIocVlvNc0Data.ACH_TxValve_SET_POINT;
    output[22] = Ioc_OutputSR1msIocVlvNc1Data.VlvNc1DataFlg;
    output[23] = Ioc_OutputSR1msIocVlvNc1Data.ACH_TxValve_SET_POINT;
    output[24] = Ioc_OutputSR1msIocVlvNc2Data.VlvNc2DataFlg;
    output[25] = Ioc_OutputSR1msIocVlvNc2Data.ACH_TxValve_SET_POINT;
    output[26] = Ioc_OutputSR1msIocVlvNc3Data.VlvNc3DataFlg;
    output[27] = Ioc_OutputSR1msIocVlvNc3Data.ACH_TxValve_SET_POINT;
    output[28] = Ioc_OutputSR1msIocVlvRelayData.VlvRelayDataFlg;
    output[29] = Ioc_OutputSR1msIocVlvRelayData.ACH_Tx1_FS_CMD;
    output[30] = Ioc_OutputSR1msIocAdcSelWriteData.AdcSelDataFlg;
    output[31] = Ioc_OutputSR1msIocAdcSelWriteData.ACH_Tx13_ADC_SEL;
    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Ioc_OutputSR1ms_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
