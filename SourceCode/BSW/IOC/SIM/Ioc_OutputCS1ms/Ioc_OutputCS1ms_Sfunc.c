#define S_FUNCTION_NAME      Ioc_OutputCS1ms_Sfunc
#define S_FUNCTION_LEVEL     2

#include "simstruc.h"
#include <stdio.h>
#define  SAMPLE_TIME         0.005

#define NUM_PARAMS              0
#define WidthInputPort          26
#define WidthOutputPort         0

real_T input[WidthInputPort];
real_T output[WidthOutputPort];
#define U(element) (*uPtrs[element])



#include "Ioc_OutputCS1ms.h"


static void mdlOutputs(SimStruct *S, int_T tid)
{ 
    int i,N;
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);  
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

    for ( i=0; i < WidthInputPort ; i++ ) input[i] = *uPtrs[i];

    Ioc_OutputCS1msNormVlvDrvInfo.PrimCutVlvDrvData = input[0];
    Ioc_OutputCS1msNormVlvDrvInfo.RelsVlvDrvData = input[1];
    Ioc_OutputCS1msNormVlvDrvInfo.SecdCutVlvDrvData = input[2];
    Ioc_OutputCS1msNormVlvDrvInfo.CircVlvDrvData = input[3];
    Ioc_OutputCS1msNormVlvDrvInfo.SimVlvDrvData = input[4];
    Ioc_OutputCS1msWhlVlvDrvInfo.FlOvDrvData = input[5];
    Ioc_OutputCS1msWhlVlvDrvInfo.FlIvDrvData = input[6];
    Ioc_OutputCS1msWhlVlvDrvInfo.FrOvDrvData = input[7];
    Ioc_OutputCS1msWhlVlvDrvInfo.FrIvDrvData = input[8];
    Ioc_OutputCS1msWhlVlvDrvInfo.RlOvDrvData = input[9];
    Ioc_OutputCS1msWhlVlvDrvInfo.RlIvDrvData = input[10];
    Ioc_OutputCS1msWhlVlvDrvInfo.RrOvDrvData = input[11];
    Ioc_OutputCS1msWhlVlvDrvInfo.RrIvDrvData = input[12];
    Ioc_OutputCS1msRlyDrvInfo.RlyDbcDrv = input[13];
    Ioc_OutputCS1msRlyDrvInfo.RlyEssDrv = input[14];
    Ioc_OutputCS1msFsrAbsDrvInfo.FsrAbsOff = input[15];
    Ioc_OutputCS1msFsrCbsDrvInfo.FsrCbsOff = input[16];
    Ioc_OutputCS1msSenPwrMonitorData.SenPwrM_12V_Drive_Req = input[17];
    Ioc_OutputCS1msEcuModeSts = input[18];
    Ioc_OutputCS1msAcmAsicInitCompleteFlag = input[19];
    Ioc_OutputCS1msFuncInhibitIocSts = input[20];
    Ioc_OutputCS1msMainCanEn = input[21];
    Ioc_OutputCS1msVlvDrvEnRst = input[22];
    Ioc_OutputCS1msAwdPrnDrv = input[23];
    Ioc_OutputCS1msFsrDcMtrShutDwn = input[24];
    Ioc_OutputCS1msFsrEnDrDrv = input[25];

    Ioc_OutputCS1ms();


    
    for ( i=0; i < WidthOutputPort ; i++ ) y[i] = output[i];
   
}


static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, NUM_PARAMS); 
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, WidthInputPort);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, WidthOutputPort);
    

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}


static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    Ioc_OutputCS1ms_Init();
}


static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);


}

static void mdlUpdate(SimStruct *S, int_T tid)
{
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

    UNUSED_ARG(tid);

}


static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S);
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
