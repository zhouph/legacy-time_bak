/**
 * @defgroup Ioc_Cfg Ioc_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_Cfg.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef IOC_CFG_H_
#define IOC_CFG_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ioc_Types.h"
#include "Dio.h"
/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/* Ioc_ModuleId : ModuleID_Adc */
#define IOC_IN_PORT_PDF_SIG_MON             (Ioc_PortType) 0
#define IOC_IN_PORT_5V_INT_MON              (Ioc_PortType) 1
#define IOC_IN_PORT_VDD5_MON                (Ioc_PortType) 2
#define IOC_IN_PORT_CE_MON                  (Ioc_PortType) 3
#define IOC_IN_PORT_FSP_ABS_MON             (Ioc_PortType) 4
#define IOC_IN_PORT_VDD1_MON                (Ioc_PortType) 5
#define IOC_IN_PORT_VLV_CUTV_S_MON          (Ioc_PortType) 6
#define IOC_IN_PORT_VLV_CUTV_P_MON          (Ioc_PortType) 7
#define IOC_IN_PORT_VLV_CV_MON            (Ioc_PortType) 8   /* NZ Quick C : CV */
#define IOC_IN_PORT_VLV_RLV_MON            (Ioc_PortType) 9   /* NZ Quick C : RLV */
#define IOC_IN_PORT_5V_EXT_MON              (Ioc_PortType) 10
#define IOC_IN_PORT_VBATT02_MON             (Ioc_PortType) 11
#define IOC_IN_PORT_FSP_CBS_MON             (Ioc_PortType) 12
#define IOC_IN_PORT_PDT_5V_MON              (Ioc_PortType) 13
#define IOC_IN_PORT_VDD3_MON                (Ioc_PortType) 14
#define IOC_IN_PORT_PDT_SIG_MON             (Ioc_PortType) 15
#define IOC_IN_PORT_VLV_SIMV_MON            (Ioc_PortType) 16
#define IOC_IN_PORT_BFL_MON                 (Ioc_PortType) 17
#define IOC_IN_PORT_MOT_POW_MON             (Ioc_PortType) 18
#define IOC_IN_PORT_MOT_U_OUT_MON           (Ioc_PortType) 19
#define IOC_IN_PORT_MOT_V_OUT_MON           (Ioc_PortType) 20
#define IOC_IN_PORT_MOT_W_OUT_MON           (Ioc_PortType) 21
#define IOC_IN_PORT_MOT_STAR_MON            (Ioc_PortType) 22
#define IOC_IN_PORT_VBATT01_MON             (Ioc_PortType) 23
#define IOC_IN_PORT_LINE_TEST_MON           (Ioc_PortType) 24
#define IOC_IN_PORT_TEMP_MON                (Ioc_PortType) 25
#define IOC_IN_PORT_RL_I_MAIN_MON           (Ioc_PortType) 26
#define IOC_IN_PORT_FSP_ABS_HMON            (Ioc_PortType) 27
#define IOC_IN_PORT_RR_I_MAIN_MON           (Ioc_PortType) 28
#define IOC_IN_PORT_FSP_CBS_HMON            (Ioc_PortType) 29
#define IOC_IN_PORT_RL_MOC_POS_MON          (Ioc_PortType) 30
#define IOC_IN_PORT_RL_MOC_NEG_MON          (Ioc_PortType) 31
#define IOC_IN_PORT_RR_MOC_POS_MON          (Ioc_PortType) 32
#define IOC_IN_PORT_RR_MOC_NEG_MON          (Ioc_PortType) 33
#define IOC_IN_PORT_RL_I_SUB_MON_FS         (Ioc_PortType) 34
#define IOC_IN_PORT_PDF_5V_MON              (Ioc_PortType) 35
#define IOC_IN_PORT_RR_I_SUB_MON_FS         (Ioc_PortType) 36
#define IOC_IN_PORT_CSP_MON                 (Ioc_PortType) 37
#define IOC_IN_PORT_MOT_U_PHASE_0_MON       (Ioc_PortType) 38
#define IOC_IN_PORT_MOT_V_PHASE_0_MON       (Ioc_PortType) 39
#define IOC_IN_PORT_MOT_U_PHASE_1_MON       (Ioc_PortType) 40
#define IOC_IN_PORT_MOT_V_PHASE_1_MON       (Ioc_PortType) 41
/* Ioc_MIOCleId : ModuleID_Dio */
#define IOC_OUT_PORT_RELAY_DBC_DRV          (Ioc_PortType) 42
#define IOC_OUT_PORT_MOC_RR_EN              (Ioc_PortType) 43
#define IOC_IN_PORT_AVH_SW_MON              (Ioc_PortType) 44
#define IOC_OUT_PORT_S_CAN_KILL             (Ioc_PortType) 45
#define IOC_OUT_PORT_MOC_RL_DRV             (Ioc_PortType) 46
#define IOC_OUT_PORT_M_CAN_ERR              (Ioc_PortType) 47
#define IOC_OUT_PORT_PDT5V_INH              (Ioc_PortType) 48
#define IOC_OUT_PORT_M_CAN_KILL             (Ioc_PortType) 49
#define IOC_OUT_PORT_D_CHAGE_DRV            (Ioc_PortType) 50
#define IOC_OUT_PORT_RELAY_EN               (Ioc_PortType) 51
#define IOC_OUT_PORT_WAKE_UP_STB            (Ioc_PortType) 52
#define IOC_OUT_PORT_LINE_TEST              (Ioc_PortType) 53
#define IOC_IN_PORT_PB_SW_MON               (Ioc_PortType) 54
#define IOC_IN_PORT_HDC_SW_MON              (Ioc_PortType) 55
#define IOC_IN_PORT_RELAY_FAULT             (Ioc_PortType) 56
#define IOC_OUT_PORT_FRA1_BGE               (Ioc_PortType) 57
#define IOC_IN_PORT_BLS_MON                 (Ioc_PortType) 58
#define IOC_IN_PORT_FLEX_BRAKE_A_MON        (Ioc_PortType) 59
#define IOC_IN_PORT_RELAY_ESS_MON           (Ioc_PortType) 60
#define IOC_IN_PORT_HAZARD_SW_MON           (Ioc_PortType) 61
#define IOC_IN_PORT_ESC_SW_MON              (Ioc_PortType) 62
#define IOC_OUT_PORT_ENB_MOC_POWER          (Ioc_PortType) 63
#define IOC_OUT_PORT_MOC_RL_DIR             (Ioc_PortType) 64
#define IOC_IN_PORT_DOOR_SW_MON             (Ioc_PortType) 65
#define IOC_OUT_PORT_MOC_RR_DRV             (Ioc_PortType) 66
#define IOC_IN_PORT_BS_SW_MON               (Ioc_PortType) 67
#define IOC_IN_PORT_FLEX_BRAKE_B_MON        (Ioc_PortType) 68
#define IOC_OUT_PORT_INHIBIT                (Ioc_PortType) 69
#define IOC_OUT_PORT_SW1_LINE_C_DRV         (Ioc_PortType) 70
#define IOC_OUT_PORT_SW2_LINE_B_DRV         (Ioc_PortType) 71
#define IOC_OUT_PORT_SW3_LINE_E_DRV         (Ioc_PortType) 72
#define IOC_OUT_PORT_SW4_LINE_F_DRV         (Ioc_PortType) 73
#define IOC_IN_PORT_SW1_LINE_C_MON          (Ioc_PortType) 74
#define IOC_OUT_PORT_PDF5V_INH              (Ioc_PortType) 75
#define IOC_OUT_PORT_MOC_RL_EN              (Ioc_PortType) 76
#define IOC_OUT_PORT_M_CAN_EN               (Ioc_PortType) 77
#define IOC_IN_PORT_RELAY_DBC_MON           (Ioc_PortType) 78
#define IOC_OUT_PORT_SPARE3_DRV             (Ioc_PortType) 79
#define IOC_OUT_PORT_EN_DR                  (Ioc_PortType) 80
#define IOC_IN_PORT_SW2_LINE_B_MON          (Ioc_PortType) 81
#define IOC_IN_PORT_FRA1_RXEN               (Ioc_PortType) 82
#define IOC_IN_PORT_FRA2_RXEN               (Ioc_PortType) 83
#define IOC_OUT_PORT_MOC_RR_DIR             (Ioc_PortType) 84
#define IOC_OUT_PORT_PRN                    (Ioc_PortType) 85
#define IOC_OUT_PORT_VDD1_MOC_DRV           (Ioc_PortType) 86
#define IOC_OUT_PORT_VDD2_MOC_DRV           (Ioc_PortType) 87
#define IOC_OUT_PORT_FRA2_BGE               (Ioc_PortType) 88
#define IOC_OUT_PORT_RELAY_ESS_DRV          (Ioc_PortType) 89
#define IOC_OUT_PORT_VSO1_DRV               (Ioc_PortType) 90
#define IOC_OUT_PORT_LIN_EN                 (Ioc_PortType) 91
#define IOC_OUT_PORT_VDR_EN                 (Ioc_PortType) 92
#define IOC_OUT_PORT_FRA1_INH               (Ioc_PortType) 93
#define IOC_OUT_PORT_MTR_FET_SD             (Ioc_PortType) 94
#define IOC_OUT_PORT_MTR_IC_EN              (Ioc_PortType) 95
#define IOC_OUT_PORT_MOT_DRV_EN             (Ioc_PortType) 96
#define IOC_IN_PORT_MTR_IC_ERR              (Ioc_PortType) 97
#define IOC_IN_PORT_SW3_LINE_E_MON          (Ioc_PortType) 98
#define IOC_IN_PORT_SW4_LINE_F_MON          (Ioc_PortType) 99
#define IOC_OUT_PORT_FRA2_STBN              (Ioc_PortType) 100
#define IOC_OUT_PORT_FSR_ABS_OFF            (Ioc_PortType) 101
#define IOC_OUT_PORT_MTR_IC_INHIBIT         (Ioc_PortType) 102
#define IOC_IN_PORT_FRA2_ERRN               (Ioc_PortType) 103
#define IOC_OUT_PORT_FRA1_EN                (Ioc_PortType) 104
#define IOC_OUT_PORT_CSP_DRV                (Ioc_PortType) 105
#define IOC_OUT_PORT_FRA1_STBN              (Ioc_PortType) 106
#define IOC_OUT_PORT_FSR_CBS_OFF            (Ioc_PortType) 107
#define IOC_IN_PORT_FRA1_ERRN               (Ioc_PortType) 108
#define IOC_OUT_PORT_FRA2_INH               (Ioc_PortType) 109
#define IOC_OUT_PORT_FRA2_EN                (Ioc_PortType) 110
/* Ioc_MIOCleId : ModuleID_IcuCdd */
#define IOC_IN_PORT_Feed_V_OUT              (Ioc_PortType) 111
#define IOC_IN_PORT_Feed_W_OUT              (Ioc_PortType) 112
#define IOC_IN_PORT_MPS2_IFA                (Ioc_PortType) 113
#define IOC_IN_PORT_MPS2_IFB                (Ioc_PortType) 114
#define IOC_IN_PORT_MPS2_IFC                (Ioc_PortType) 115
#define IOC_IN_PORT_MPS1_IFA                (Ioc_PortType) 116
#define IOC_IN_PORT_MPS1_IFB                (Ioc_PortType) 117
#define IOC_IN_PORT_MPS1_IFC                (Ioc_PortType) 118
#define IOC_IN_PORT_WSS_FL                  (Ioc_PortType) 119
#define IOC_IN_PORT_Feed_U_OUT              (Ioc_PortType) 120
#define IOC_IN_PORT_WSS_FR                  (Ioc_PortType) 121
#define IOC_IN_PORT_WSS_RR                  (Ioc_PortType) 122
#define IOC_IN_PORT_WSS_RL                  (Ioc_PortType) 123
/* Ioc_MIOCleId : ModuleID_Pwm */
#define IOC_OUT_PORT_MOC_RL                 (Ioc_PortType) 124
#define IOC_OUT_PORT_MOT_U_HIGH             (Ioc_PortType) 125
#define IOC_OUT_PORT_MOT_U_LOW              (Ioc_PortType) 126
#define IOC_OUT_PORT_MOT_V_HIGH             (Ioc_PortType) 127
#define IOC_OUT_PORT_VLV_SEC_CUT            (Ioc_PortType) 128
#define IOC_OUT_PORT_MOT_W_HIGH             (Ioc_PortType) 129
#define IOC_OUT_PORT_MOT_W_LOW              (Ioc_PortType) 130
#define IOC_OUT_PORT_VLV_SIM                (Ioc_PortType) 131
#define IOC_OUT_PORT_VLV_PRI_CUT            (Ioc_PortType) 132
#define IOC_OUT_PORT_MOT_V_LOW              (Ioc_PortType) 133
#define IOC_OUT_PORT_MOC_RR                 (Ioc_PortType) 134
#define IOC_OUT_PORT_VLV_INFL               (Ioc_PortType) 135
#define IOC_OUT_PORT_VLV_INFR               (Ioc_PortType) 136

/*******************************************************************************
 * Existing maintenance *******************************************************/
/* Ioc_MIOCleId : ModuleID_Spi */
#define IOC_IN_PORT_VLV_OUTFL               (Ioc_PortType) 137
#define IOC_IN_PORT_VLV_INFL                (Ioc_PortType) 138
#define IOC_IN_PORT_VLV_OUTFR               (Ioc_PortType) 139
#define IOC_IN_PORT_VLV_INFR                (Ioc_PortType) 140
#define IOC_IN_PORT_VLV_OUTRL               (Ioc_PortType) 141
#define IOC_IN_PORT_VLV_INRL                (Ioc_PortType) 142
#define IOC_IN_PORT_VLV_OUTRR               (Ioc_PortType) 143
#define IOC_IN_PORT_VLV_INRR                (Ioc_PortType) 144

#define IOC_OUT_PORT_FSR_DRV2               (Ioc_PortType) 145

#define IOC_OUT_PORT_VLV_INRR               (Ioc_PortType) 146
#define IOC_OUT_PORT_VLV_INRL               (Ioc_PortType) 147
#define IOC_OUT_PORT_VLV_OUTFL              (Ioc_PortType) 148
#define IOC_OUT_PORT_VLV_OUTFR              (Ioc_PortType) 149
#define IOC_OUT_PORT_VLV_OUTRL              (Ioc_PortType) 150
#define IOC_OUT_PORT_VLV_OUTRR              (Ioc_PortType) 151
#define IOC_OUT_PORT_VLV_PRI_BAL            (Ioc_PortType) 152
#define IOC_OUT_PORT_VLV_SEC_BAL            (Ioc_PortType) 153   /* NZ Quick C : PD */
#define IOC_OUT_PORT_VLV_CHM_BAL            (Ioc_PortType) 154

#define IOC_OUT_PORT_FSR_DRV1               (Ioc_PortType) 155

#define IOC_IN_PORT_MPS1_ANGLE_RAW          (Ioc_PortType) 156
#define IOC_IN_PORT_MPS1_ANGLE_DEG          (Ioc_PortType) 157
#define IOC_IN_PORT_MPS2_ANGLE_RAW          (Ioc_PortType) 158
#define IOC_IN_PORT_MPS2_ANGLE_DEG          (Ioc_PortType) 159

 /* Ioc_ModuleId : ModuleID_Adc for Esc*/
 #define IOC_VOLT_VBATT01_MON               (Ioc_PortType) 160   /* open */
 #define IOC_VOLT_VBATT02_MON               (Ioc_PortType) 161   /* Voltage Battery 2 Monitor  */
 #define IOC_VOLT_GIO_2_MON                 (Ioc_PortType) 162   /* Battery Power Supply Monitor */
 #define IOC_VOLT_EXT_5V_SUPPLY_MON         (Ioc_PortType) 163   /* External 5V power supply. It goes to connector */
 #define IOC_VOLT_EXT_12V_SUPPLY_MON        (Ioc_PortType) 164   /* External 12V power supply monitor. */
 #define IOC_VOLT_SW_VPWR_MON               (Ioc_PortType) 165   /* 12V Logic Power Supply Monitoring  */
 #define IOC_VOLT_VPWR_MON                  (Ioc_PortType) 166   /* VPower Monitor */
 #define IOC_VOLT_FSP_MON                   (Ioc_PortType) 167   /* Valve Voltage Monitor */
 #define IOC_VOLT_GIO_1_MON                 (Ioc_PortType) 168   /* Ignition Monitor  */
 #define IOC_VOLT_IGN_MON                   (Ioc_PortType) 169   /* open */
 #define IOC_VOLT_MTP_MON                   (Ioc_PortType) 170   /* Motor Voltage Monitor */
 #define IOC_VOLT_MTR_FW_S_MON              (Ioc_PortType) 171   /* Motor Speed Monitor */
 #define IOC_VOLT_EPB_PWR_MON               (Ioc_PortType) 172   /* EPB Power Supply Diagnosis */
 #define IOC_VOLT_EPB_I1_L_MON              (Ioc_PortType) 173   /* EPB Low Side Shunt L */
 #define IOC_VOLT_EPB_I1_R_MON              (Ioc_PortType) 174   /* EPB Low Side Shunt R */
 #define IOC_VOLT_EPB_I2_L_MON              (Ioc_PortType) 175   /* EPB Second Low Side Shunt Left Motor Monitor */
 #define IOC_VOLT_EPB_I2_R_MON              (Ioc_PortType) 176   /* EPB Second Low Side Shunt Right Motor Monitor */
 #define IOC_VOLT_EPB_MON_L_PLUS            (Ioc_PortType) 177   /* EPB Motor Cable Connection Diagnosis */
 #define IOC_VOLT_EPB_MON_L_MINUS           (Ioc_PortType) 178   /* EPB Motor Cable Connection Diagnosis */
 #define IOC_VOLT_EPB_MON_R_PLUS            (Ioc_PortType) 179   /* EPB Motor Cable Connection Diagnosis */
 #define IOC_VOLT_EPB_MON_R_MINUS           (Ioc_PortType) 180   /* EPB Motor Cable Connection Diagnosis */
 #define IOC_VOLT_MCU_ADC1                  (Ioc_PortType) 181   /* Multipurpose/Spare Input */
 #define IOC_VOLT_MCU_ADC2                  (Ioc_PortType) 182   /* Multipurpose/Spare Input */
 #define IOC_VOLT_MCU_ADC3                  (Ioc_PortType) 183   /* Multipurpose/Spare Input */
 #define IOC_VOLT_MCU_ADC4                  (Ioc_PortType) 184   /* Multipurpose/Spare Input */
 #define IOC_VOLT_MCU_ADC5                  (Ioc_PortType) 185   /* Multipurpose/Spare Input */

 #define IOC_OUT_PORT_VLV_PRI_CV            (Ioc_PortType) 186   /* NZ Quick C Sample: RLV */
 #define IOC_OUT_PORT_VLV_SEC_CV            (Ioc_PortType) 187   /* NZ Quick C Sample: CV */


 #define IOC_PORT_MAX_NUM                   (Ioc_PortType) 188

 #define DIO_CHANNEL_NOT_USED               0xFFFF
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

typedef struct
{
    /* GPDO */
    Dio_ChannelType MOC_RL_DRV;
    Dio_ChannelType S_CAN_KILL;
    Dio_ChannelType MOC_RR_EN;
    Dio_ChannelType RELAY_DBC_DRV;
    Dio_ChannelType PRN;
    Dio_ChannelType LIN_EN;
    Dio_ChannelType M_CAN_ERR;
    Dio_ChannelType PDT5V_INH;
    Dio_ChannelType M_CAN_KILL;
    Dio_ChannelType D_CHAGE_DRV;
    Dio_ChannelType RELAY_EN;
    Dio_ChannelType WAKE_UP_STB;
    Dio_ChannelType LINE_TEST;
    Dio_ChannelType MCU_FRA1_BGE;
    Dio_ChannelType ENB_MOC_POWER;
    Dio_ChannelType MOC_RL_DIR;
    Dio_ChannelType MOC_RR_DRV;
    Dio_ChannelType INHIBIT;
    Dio_ChannelType SW1LINE_C_DRV;
    Dio_ChannelType SW2LINE_B_DRV;
    Dio_ChannelType SW3LINE_E_DRV;
    Dio_ChannelType SW4LINE_F_DRV;
    Dio_ChannelType PDF5V_INH;
    Dio_ChannelType MOC_RL_EN;
    Dio_ChannelType M_CAN_EN;
    Dio_ChannelType SPARE3_DRV;
    Dio_ChannelType EN_DR;
    Dio_ChannelType MOC_RR_DIR;
    Dio_ChannelType VDD1_MOC_DRV;
    Dio_ChannelType VDD2_MOC_DRV;
    Dio_ChannelType MCU_FRA2_BGE;
    Dio_ChannelType RELAY_ESS_DRV;
    Dio_ChannelType VSO1_DRV;
    Dio_ChannelType MCU_FRA1_MCU_INH;
    Dio_ChannelType MTR_FET_SD;
    Dio_ChannelType MTR_IC_EN;
    Dio_ChannelType MOT_DRV_EN;
    Dio_ChannelType MCU_FRA2_STBN;
    Dio_ChannelType FSR_ABS_OFF;
    Dio_ChannelType MTR_IC_INHIBIT;
    Dio_ChannelType MCU_FRA1_EN;
    Dio_ChannelType CSP_DRV;
    Dio_ChannelType MCU_FRA1_STBN;
    Dio_ChannelType FSR_CBS_OFF;
    Dio_ChannelType MCU_FRA2_MCU_INH;
    Dio_ChannelType MCU_FRA2_EN;
    Dio_ChannelType VDR_EN;
    Dio_ChannelType MTP_SD;
    Dio_ChannelType SAFE_LS2_R;
    Dio_ChannelType RELAY2_DRV;
    Dio_ChannelType RELAY1_DRV;
    Dio_ChannelType REF5V_02_INHIBIT;
    Dio_ChannelType REF5V_01_INHIBIT;
    Dio_ChannelType SAFE_LS1_R;
    Dio_ChannelType SAFE_LS2_L;
    Dio_ChannelType VAC_PUMP_RELAY_DRV;
    Dio_ChannelType VDD_DRV;
    
    /* GPDI */
    Dio_ChannelType AVH_SW_MON;
    Dio_ChannelType PB_SW_MON;
    Dio_ChannelType HDC_SW_MON;
    Dio_ChannelType RELAY_FAULT;
    Dio_ChannelType BLS_MON;
    Dio_ChannelType FLEX_BRAKE_A_MON;
    Dio_ChannelType RELAY_ESS_MON;
    Dio_ChannelType HAZARD_SW_MON;
    Dio_ChannelType ESC_SW_MON;
    Dio_ChannelType DOOR_SW_MON;
    Dio_ChannelType BS_SW_MON;
    Dio_ChannelType FLEX_BRAKE_B_MON;
    Dio_ChannelType SW1LINE_C_MON;
    Dio_ChannelType RELAY_DBC_MON;
    Dio_ChannelType SW2LINE_B_MON;
    Dio_ChannelType MCU_FRA1_RXEN;
    Dio_ChannelType MCU_FRA2_RXEN;
    Dio_ChannelType MTR_IC_ERR;
    Dio_ChannelType SW3LINE_E_MON;
    Dio_ChannelType SW4LINE_F_MON;
    Dio_ChannelType MCU_FRA2_ERRN;
    Dio_ChannelType MCU_FRA1_ERRN;
}Ioc_DioMappingType;

typedef enum
{
    Asic100_FSR_ABS = 0,
    Asic100_FSR_CBS,
    Asic100_DcMotor,
    Asic100_VsoDrv,
    Asic100_WldDrv,
    Asic100_KlineDrv,
    Asic100_ShlsDrv,
    Asic100_NotUsed
}Asic100DriverUsageType;

typedef struct
{
    Asic100DriverUsageType FailSafePreDriver;
    Asic100DriverUsageType PumpMotorPreDriver;
    Asic100DriverUsageType VsoDriver;
    Asic100DriverUsageType WldDriver;
    Asic100DriverUsageType KlineDriver;
    Asic100DriverUsageType ShlsDriver;
}Asic100DriverRoutingType;

typedef enum    /* Possible Usage of Valve (based on Interface) */
{
    Asic100_FLIV = 0,
    Asic100_FRIV,
    Asic100_RLIV,
    Asic100_RRIV,
    Asic100_FLOV,
    Asic100_FROV,
    Asic100_RLOV,
    Asic100_RROV,
    Asic100_PTC,
    Asic100_STC,
    Asic100_PESV,
    Asic100_SESV,
    Asic100_PBAL,
    Asic100_SBAL,
    Asic100_CBAL,
    Asic100_VlvNotUsed,
    Asic100_VlvMax
}Asic100ValveUsageType;


/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/
extern const Ioc_RoutingType Ioc_RoutingConfig[IOC_PORT_MAX_NUM];
extern const Ioc_RoutingType Ioc_ValveRoutingConfig[IOC_VALVE_MAX_NUM];  /* for 100ASIC */
extern const Ioc_RoutingType Ioc_MotorRoutingConfig[IOC_MOTOR_MAX_NUM]; /* for 100ASIC */
extern const Asic100DriverRoutingType Asic100DriverRoutingConfig;
extern const Asic100ValveUsageType Asic100ValveMappingConfig[IOC_VALVE_MAX_NUM];
extern const Ioc_DioMappingType Ioc_DioMapping;

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* IOC_CFG_H_ */
/** @} */
