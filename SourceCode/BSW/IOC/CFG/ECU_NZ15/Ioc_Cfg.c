/**
 * @defgroup Ioc_Cfg Ioc_Cfg
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_Cfg.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ioc_Cfg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
 #define IOC_REF_VOLTAGE    5000
 #define IOC_MAX_RESOLUTION 4095
 #define IOC_VOLT_MUL         78 /* (IOC_REF_VOLTAGE << 8) / IOC_MAX_RESOLUTION */
 #define IOC_VOLT_DIV         64
/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define IOC_START_SEC_CONST_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/
const Ioc_RoutingType Ioc_RoutingConfig[IOC_PORT_MAX_NUM] =
{
    /* IOC_IN_PORT_PDF_SIG_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) (IOC_VOLT_MUL),                /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (IOC_VOLT_DIV)                 /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_5V_INT_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) ((100+100)*IOC_VOLT_MUL),      /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (100*IOC_VOLT_DIV)             /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_VDD5_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) (IOC_VOLT_MUL),                /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (IOC_VOLT_DIV)                 /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_CE_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) ((300+51)*IOC_VOLT_MUL),       /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (51*IOC_VOLT_DIV)              /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_FSP_ABS_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) ((300+51)*IOC_VOLT_MUL),       /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (51*IOC_VOLT_DIV)              /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_VDD1_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) ((300+51)*IOC_VOLT_MUL),       /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (51*IOC_VOLT_DIV)              /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_VLV_CUTV_S_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) (IOC_VOLT_MUL),                /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (IOC_VOLT_DIV)                 /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_VLV_CUTV_P_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) (IOC_VOLT_MUL),                /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (IOC_VOLT_DIV)                 /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_VLV_CV_MON */ /* NZ Quick C : CV */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) (IOC_VOLT_MUL),                /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (IOC_VOLT_DIV)                 /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_VLV_INFL_MON */ /* NZ Quick C: RLV */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) (IOC_VOLT_MUL),                /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (IOC_VOLT_DIV)                 /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_5V_EXT_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) ((100+100)*IOC_VOLT_MUL),      /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (100*IOC_VOLT_DIV)             /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_VBATT02_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) ((300+51)*IOC_VOLT_MUL),       /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (51*IOC_VOLT_DIV)              /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_FSP_CBS_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) ((300+51)*IOC_VOLT_MUL),       /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (51*IOC_VOLT_DIV)              /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_PDT_5V_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) ((100+100)*IOC_VOLT_MUL),      /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (100*IOC_VOLT_DIV)             /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_VDD3_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) (IOC_VOLT_MUL),                /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (IOC_VOLT_DIV)                 /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_PDT_SIG_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) (IOC_VOLT_MUL),                /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (IOC_VOLT_DIV)                 /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_VLV_SIMV_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) (IOC_VOLT_MUL),                /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (IOC_VOLT_DIV)                 /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_BFL_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) ((2000+1200)*IOC_VOLT_MUL),    /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1200*IOC_VOLT_DIV)            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_MOT_POW_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) ((300+51)*IOC_VOLT_MUL),       /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (51*IOC_VOLT_DIV)              /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_MOT_U_OUT_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) ((300+51)*IOC_VOLT_MUL),       /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (51*IOC_VOLT_DIV)              /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_MOT_V_OUT_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) ((300+51)*IOC_VOLT_MUL),       /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (51*IOC_VOLT_DIV)              /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_MOT_W_OUT_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) ((300+51)*IOC_VOLT_MUL),       /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (51*IOC_VOLT_DIV)              /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_MOT_STAR_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) ((300+51)*IOC_VOLT_MUL),       /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (51*IOC_VOLT_DIV)              /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_VBATT01_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) ((300+51)*IOC_VOLT_MUL),       /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (51*IOC_VOLT_DIV)              /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_LINE_TEST_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) (IOC_VOLT_MUL),                /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (IOC_VOLT_DIV)                 /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_TEMP_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) (IOC_VOLT_MUL),                /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (IOC_VOLT_DIV)                 /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_RL_I_MAIN_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) (IOC_VOLT_MUL),                /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (IOC_VOLT_DIV)                 /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_FSP_ABS_HMON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) (IOC_VOLT_MUL),                /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (IOC_VOLT_DIV)                 /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_RR_I_MAIN_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) (IOC_VOLT_MUL),                /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (IOC_VOLT_DIV)                 /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_FSP_CBS_HMON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) (IOC_VOLT_MUL),                /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (IOC_VOLT_DIV)                 /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_RL_MOC_POS_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) (IOC_VOLT_MUL),                /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (IOC_VOLT_DIV)                 /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_RL_MOC_NEG_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) (IOC_VOLT_MUL),                /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (IOC_VOLT_DIV)                 /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_RR_MOC_POS_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) (IOC_VOLT_MUL),                /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (IOC_VOLT_DIV)                 /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_RR_MOC_NEG_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) (IOC_VOLT_MUL),                /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (IOC_VOLT_DIV)                 /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_RL_I_SUB_MON_FS */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) (IOC_VOLT_MUL),                /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (IOC_VOLT_DIV)                 /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_PDF_5V_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) ((100+100)*IOC_VOLT_MUL),               /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (100*IOC_VOLT_DIV)                      /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_RR_I_SUB_MON_FS */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) (IOC_VOLT_MUL),                /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (IOC_VOLT_DIV)                 /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_CSP_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) ((300+51)*IOC_VOLT_MUL),       /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (51*IOC_VOLT_DIV)              /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_MOT_U_PHASE_0_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) (IOC_VOLT_MUL),                /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (IOC_VOLT_DIV)                 /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_MOT_V_PHASE_0_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) (IOC_VOLT_MUL),                /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (IOC_VOLT_DIV)                 /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_MOT_U_PHASE_1_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) (IOC_VOLT_MUL),                /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (IOC_VOLT_DIV)                 /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_MOT_V_PHASE_1_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Adc,                  /* ModuleID */
        (Ioc_MulFactorType) (IOC_VOLT_MUL),                /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (IOC_VOLT_DIV)                 /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_RELAY_DBC_DRV */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_MOC_RR_EN */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_AVH_SW_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_S_CAN_KILL */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_MOC_RL_DRV */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_M_CAN_ERR */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_PDT5V_INH */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_M_CAN_KILL */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_D_CHAGE_DRV */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_RELAY_EN */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_WAKE_UP_STB */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_LINE_TEST */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_PB_SW_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_HDC_SW_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_RELAY_FAULT */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_FRA1_BGE */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_BLS_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_FLEX_BRAKE_A_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_RELAY_ESS_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_HAZARD_SW_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_ESC_SW_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_ENB_MOC_POWER */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_MOC_RL_DIR */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_DOOR_SW_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_MOC_RR_DRV */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_BS_SW_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_FLEX_BRAKE_B_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_INHIBIT */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_SW1_LINE_C_DRV */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_SW2_LINE_B_DRV */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_SW3_LINE_E_DRV */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_SW4_LINE_F_DRV */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_SW1_LINE_C_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_PDF5V_INH */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_MOC_RL_EN */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_M_CAN_EN */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_RELAY_DBC_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_SPARE3_DRV */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_EN_DR */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_SW2_LINE_B_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_FRA1_RXEN */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_FRA2_RXEN */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_MOC_RR_DIR */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_PRN */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_VDD1_MOC_DRV */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_VDD2_MOC_DRV */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_FRA2_BGE */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_RELAY_ESS_DRV */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_VSO1_DRV */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_LIN_EN */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_VDR_EN */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_FRA1_INH */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_MTR_FET_SD */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_MTR_IC_EN */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_MOT_DRV_EN */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_MTR_IC_ERR */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_SW3_LINE_E_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_SW4_LINE_F_MON */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_FRA2_STBN */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_FSR_ABS_OFF */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_MTR_IC_INHIBIT */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_FRA2_ERRN */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_FRA1_EN */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_CSP_DRV */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_FRA1_STBN */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_FSR_CBS_OFF */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_FRA1_ERRN */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_FRA2_INH */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_FRA2_EN */
    {
        (Ioc_ModuleIDType)  ModuleID_Dio,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_Feed_V_OUT */
    {
        (Ioc_ModuleIDType)  ModuleID_IcuCdd,               /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_Feed_W_OUT */
    {
        (Ioc_ModuleIDType)  ModuleID_IcuCdd,               /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_MPS2_IFA */
    {
        (Ioc_ModuleIDType)  ModuleID_IcuCdd,               /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_MPS2_IFB */
    {
        (Ioc_ModuleIDType)  ModuleID_IcuCdd,               /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_MPS2_IFC */
    {
        (Ioc_ModuleIDType)  ModuleID_IcuCdd,               /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_MPS1_IFA */
    {
        (Ioc_ModuleIDType)  ModuleID_IcuCdd,               /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_MPS1_IFB */
    {
        (Ioc_ModuleIDType)  ModuleID_IcuCdd,               /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_MPS1_IFC */
    {
        (Ioc_ModuleIDType)  ModuleID_IcuCdd,               /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_WSS_FL */
    {
        (Ioc_ModuleIDType)  ModuleID_IcuCdd,               /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_Feed_U_OUT */
    {
        (Ioc_ModuleIDType)  ModuleID_IcuCdd,               /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_WSS_FR */
    {
        (Ioc_ModuleIDType)  ModuleID_IcuCdd,               /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_WSS_RR */
    {
        (Ioc_ModuleIDType)  ModuleID_IcuCdd,               /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_IN_PORT_WSS_RL */
    {
        (Ioc_ModuleIDType)  ModuleID_IcuCdd,               /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_MOC_RL */
    {
        (Ioc_ModuleIDType)  ModuleID_Pwm,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (2)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_MOT_U_HIGH */
    {
        (Ioc_ModuleIDType)  ModuleID_Pwm,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (2)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_MOT_U_LOW */
    {
        (Ioc_ModuleIDType)  ModuleID_Pwm,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (2)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_MOT_V_HIGH */
    {
        (Ioc_ModuleIDType)  ModuleID_Pwm,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (2)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_VLV_SEC_CUT */
    {
        (Ioc_ModuleIDType)  ModuleID_Pwm,                  /* ModuleID */
        (Ioc_MulFactorType) (625),                         /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1000)                         /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_MOT_W_HIGH */
    {
        (Ioc_ModuleIDType)  ModuleID_Pwm,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (2)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_MOT_W_LOW */
    {
        (Ioc_ModuleIDType)  ModuleID_Pwm,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (2)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_VLV_SIM */
    {
        (Ioc_ModuleIDType)  ModuleID_Pwm,                  /* ModuleID */
        (Ioc_MulFactorType) (625),                         /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1000)                         /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_VLV_PRI_CUT */
    {
        (Ioc_ModuleIDType)  ModuleID_Pwm,                  /* ModuleID */
        (Ioc_MulFactorType) (625),                         /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1000)                         /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_MOT_V_LOW */
    {
        (Ioc_ModuleIDType)  ModuleID_Pwm,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (2)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_MOC_RR */
    {
        (Ioc_ModuleIDType)  ModuleID_Pwm,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                           /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (2)                            /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_VLV_INFL */ /* NZ Quick C : RLV */
    {
        (Ioc_ModuleIDType)  ModuleID_Spi,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                         /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                         /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_VLV_INFR */ /* NZ Quick C : CV */
    {
        (Ioc_ModuleIDType)  ModuleID_Spi,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                         /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                         /* DivFactor : R2  * IOC_VOLT_DIV */
    },

/*******************************************************************************
 * Existing maintenance *******************************************************/

    /* IOC_IN_PORT_VLV_OUTFL */
    {
        (Ioc_ModuleIDType)  ModuleID_Spi,                       /* ModuleID */
        (Ioc_MulFactorType) 1,                                  /* ASIC Duty Register */
        (Ioc_DivFactorType) 1                                   /* Duty Percent */
    },
    /* IOC_IN_PORT_VLV_INFL */
    {
        (Ioc_ModuleIDType)  ModuleID_Spi,                       /* ModuleID */
        (Ioc_MulFactorType) 1,                                  /* ASIC Duty Register */
        (Ioc_DivFactorType) 1                                   /* Duty Percent */
    },
    /* IOC_IN_PORT_VLV_OUTFR */
    {
        (Ioc_ModuleIDType)  ModuleID_Spi,                       /* ModuleID */
        (Ioc_MulFactorType) 1,                                  /* ASIC Duty Register */
        (Ioc_DivFactorType) 1                                   /* Duty Percent */
    },
    /* IOC_IN_PORT_VLV_INFR */
    {
        (Ioc_ModuleIDType)  ModuleID_Spi,                       /* ModuleID */
        (Ioc_MulFactorType) 1,                                  /* ASIC Duty Register */
        (Ioc_DivFactorType) 1                                   /* Duty Percent */
    },
    /* IOC_IN_PORT_VLV_OUTRL */
    {
        (Ioc_ModuleIDType)  ModuleID_Spi,                       /* ModuleID */
        (Ioc_MulFactorType) 1,                                  /* ASIC Duty Register */
        (Ioc_DivFactorType) 1                                   /* Duty Percent */
    },
    /* IOC_IN_PORT_VLV_INRL */
    {
        (Ioc_ModuleIDType)  ModuleID_Spi,                       /* ModuleID */
        (Ioc_MulFactorType) 1,                                  /* ASIC Duty Register */
        (Ioc_DivFactorType) 1                                   /* Duty Percent */
    },
    /* IOC_IN_PORT_VLV_OUTRR */
    {
        (Ioc_ModuleIDType)  ModuleID_Spi,                       /* ModuleID */
        (Ioc_MulFactorType) 1,                                  /* ASIC Duty Register */
        (Ioc_DivFactorType) 1                                   /* Duty Percent */
    },
    /* IOC_IN_PORT_VLV_INRR */
    {
        (Ioc_ModuleIDType)  ModuleID_Spi,                       /* ModuleID */
        (Ioc_MulFactorType) 1,                                  /* ASIC Duty Register */
        (Ioc_DivFactorType) 1                                   /* Duty Percent */
    },
    /* IOC_OUT_PORT_FSR_DRV2 */
    {
        (Ioc_ModuleIDType)  ModuleID_Spi,                       /* ModuleID */
        (Ioc_MulFactorType) 1,                                  /* ASIC Duty Register */
        (Ioc_DivFactorType) 1                                   /* Duty Percent */
    },
    /* IOC_OUT_PORT_VLV_INRR */
    {
        (Ioc_ModuleIDType)  ModuleID_Spi,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                         /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                         /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_VLV_INRL */
    {
        (Ioc_ModuleIDType)  ModuleID_Spi,                  /* ModuleID */
        (Ioc_MulFactorType) (1),                         /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) (1)                         /* DivFactor : R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_VLV_OUTFL */
    {
        (Ioc_ModuleIDType)  ModuleID_Spi,                       /* ModuleID */
        (Ioc_MulFactorType) 1,                                  /* ASIC Duty Register */
        (Ioc_DivFactorType) 1                                   /* Duty Percent */
    },
    /* IOC_OUT_PORT_VLV_OUTFR */
    {
        (Ioc_ModuleIDType)  ModuleID_Spi,                       /* ModuleID */
        (Ioc_MulFactorType) 1,                                  /* ASIC Duty Register */
        (Ioc_DivFactorType) 1                                   /* Duty Percent */
    },
    /* IOC_OUT_PORT_VLV_OUTRL */
    {
        (Ioc_ModuleIDType)  ModuleID_Spi,                       /* ModuleID */
        (Ioc_MulFactorType) 1,                                  /* ASIC Duty Register */
        (Ioc_DivFactorType) 1                                   /* Duty Percent */
    },
    /* IOC_OUT_PORT_VLV_OUTRR */
    {
        (Ioc_ModuleIDType)  ModuleID_Spi,                       /* ModuleID */
        (Ioc_MulFactorType) 1,                                  /* ASIC Duty Register */
        (Ioc_DivFactorType) 1                                   /* Duty Percent */
    },
    /* IOC_OUT_PORT_VLV_PRI_BAL */
    {
        (Ioc_ModuleIDType)  ModuleID_Spi,                       /* ModuleID */
        (Ioc_MulFactorType) 1,                                  /* ASIC Duty Register */
        (Ioc_DivFactorType) 1                                   /* Duty Percent */
    },
    /* IOC_OUT_PORT_VLV_SEC_BAL */ /* NZ Quick-C: PD */
    {
        (Ioc_ModuleIDType)  ModuleID_Spi,                       /* ModuleID */
        (Ioc_MulFactorType) 1,                                  /* ASIC Duty Register */
        (Ioc_DivFactorType) 1                                   /* Duty Percent */
    },
    /* IOC_OUT_PORT_VLV_CHM_BAL */
    {
        (Ioc_ModuleIDType)  ModuleID_Spi,                       /* ModuleID */
        (Ioc_MulFactorType) 1,                                  /* ASIC Duty Register */
        (Ioc_DivFactorType) 1                                   /* Duty Percent */
    },
    /* IOC_OUT_PORT_FSR_DRV1 */
    {
        (Ioc_ModuleIDType)  ModuleID_Spi,                       /* ModuleID */
        (Ioc_MulFactorType) 1,                                  /* Not applicable */
        (Ioc_DivFactorType) 1                                   /* Not applicable */
    },
    /* IOC_IN_PORT_MPS1_ANGLE_RAW */
    {
        (Ioc_ModuleIDType)  ModuleID_Spi,                       /* ModuleID */
        (Ioc_MulFactorType) 1,                                  /* ASIC Duty Register */
        (Ioc_DivFactorType) 1                                   /* Duty Percent */
    },
    /* IOC_IN_PORT_MPS1_ANGLE_DEG */
    {
        (Ioc_ModuleIDType)  ModuleID_Spi,                       /* ModuleID */
        (Ioc_MulFactorType) 1,                                  /* ASIC Duty Register */
        (Ioc_DivFactorType) 1                                   /* Duty Percent */
    },
    /* IOC_IN_PORT_MPS2_ANGLE_RAW */
    {
        (Ioc_ModuleIDType)  ModuleID_Spi,                       /* ModuleID */
        (Ioc_MulFactorType) 1,                                  /* ASIC Duty Register */
        (Ioc_DivFactorType) 1                                   /* Duty Percent */
    },
    /* IOC_IN_PORT_MPS2_ANGLE_DEG */
    {
        (Ioc_ModuleIDType)  ModuleID_Spi,                       /* ModuleID */
        (Ioc_MulFactorType) 1,                                  /* ASIC Duty Register */
        (Ioc_DivFactorType) 1                                   /* Duty Percent */
    },
	/*****************/
	/* ESC Adc Start */
	/*****************/
	/* IOC_VOLT_VBATT01_MON         */
    {
        (Ioc_ModuleIDType)   ModuleID_Adc     ,               /* ModuleID */
        (Ioc_MulFactorType) ((680+130)* IOC_VOLT_MUL),        /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) ((130)    * IOC_VOLT_DIV)         /* DivFactor :       R2  * IOC_VOLT_DIV */
    },
    /* IOC_VOLT_VBATT02_MON         */
    {
        (Ioc_ModuleIDType)   ModuleID_Adc     ,               /* ModuleID */
        (Ioc_MulFactorType) ((680+130)* IOC_VOLT_MUL),        /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) ((130)    * IOC_VOLT_DIV)         /* DivFactor :       R2  * IOC_VOLT_DIV */
    },
    /* IOC_VOLT_GIO_2_MON           */
    {
        (Ioc_ModuleIDType)   ModuleID_Adc     ,               /* ModuleID */
        (Ioc_MulFactorType) ((300+51)* IOC_VOLT_MUL),        /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) ((51)    * IOC_VOLT_DIV)         /* DivFactor :       R2  * IOC_VOLT_DIV */
    },
    /* IOC_VOLT_EXT_5V_SUPPLY_MON   */
    {
        (Ioc_ModuleIDType)   ModuleID_Adc     ,               /* ModuleID */
        (Ioc_MulFactorType) ((100+100)* IOC_VOLT_MUL),        /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) ((100)    * IOC_VOLT_DIV)         /* DivFactor :       R2  * IOC_VOLT_DIV */
    },
    /* IOC_VOLT_EXT_12V_SUPPLY_MON  */
    {
        (Ioc_ModuleIDType)   ModuleID_Adc     ,               /* ModuleID */
        (Ioc_MulFactorType) ((680+130)* IOC_VOLT_MUL),        /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) ((130)    * IOC_VOLT_DIV)         /* DivFactor :       R2  * IOC_VOLT_DIV */
    },
    /* IOC_VOLT_SW_VPWR_MON         */
    {
        (Ioc_ModuleIDType)   ModuleID_Adc     ,               /* ModuleID */
        (Ioc_MulFactorType) ((300+51)* IOC_VOLT_MUL),        /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) ((51)    * IOC_VOLT_DIV)         /* DivFactor :       R2  * IOC_VOLT_DIV */
    },
    /* IOC_VOLT_VPWR_MON            */
    {
        (Ioc_ModuleIDType)   ModuleID_Adc     ,               /* ModuleID */
        (Ioc_MulFactorType) ((680+130)* IOC_VOLT_MUL),        /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) ((130)    * IOC_VOLT_DIV)         /* DivFactor :       R2  * IOC_VOLT_DIV */
    },
    /* IOC_VOLT_FSP_MON             */
    {
        (Ioc_ModuleIDType)   ModuleID_Adc     ,               /* ModuleID */
        (Ioc_MulFactorType) ((680+130)* IOC_VOLT_MUL),        /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) ((130)    * IOC_VOLT_DIV)         /* DivFactor :       R2  * IOC_VOLT_DIV */
    },
    /* IOC_VOLT_GIO_1_MON           */
    {
        (Ioc_ModuleIDType)   ModuleID_Adc     ,               /* ModuleID */
        (Ioc_MulFactorType) ((300+51)* IOC_VOLT_MUL),        /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) ((51)    * IOC_VOLT_DIV)         /* DivFactor :       R2  * IOC_VOLT_DIV */
    },
    /* IOC_VOLT_IGN_MON             */
    {
        (Ioc_ModuleIDType)   ModuleID_Adc     ,               /* ModuleID */
        (Ioc_MulFactorType) ((680+130)* IOC_VOLT_MUL),        /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) ((130)    * IOC_VOLT_DIV)         /* DivFactor :       R2  * IOC_VOLT_DIV */
    },
    /* IOC_VOLT_MTP_MON             */
    {
        (Ioc_ModuleIDType)   ModuleID_Adc     ,               /* ModuleID */
        (Ioc_MulFactorType) ((300+100)* IOC_VOLT_MUL),        /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) ((100)    * IOC_VOLT_DIV)         /* DivFactor :       R2  * IOC_VOLT_DIV */
    },
    /* IOC_VOLT_MTR_FW_S_MON        */
    {
        (Ioc_ModuleIDType)   ModuleID_Adc     ,               /* ModuleID */
        (Ioc_MulFactorType) ((300+51)* IOC_VOLT_MUL),        /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) ((51)    * IOC_VOLT_DIV)         /* DivFactor :       R2  * IOC_VOLT_DIV */
    },
    /* IOC_VOLT_EPB_PWR_MON         */
    {
        (Ioc_ModuleIDType)   ModuleID_Adc     ,               /* ModuleID */
        (Ioc_MulFactorType) ((680+130)* IOC_VOLT_MUL),        /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) ((130)    * IOC_VOLT_DIV)         /* DivFactor :       R2  * IOC_VOLT_DIV */
    },
    /* IOC_VOLT_EPB_I1_L_MON        */
    {
        (Ioc_ModuleIDType)   ModuleID_Adc     ,               /* ModuleID */
        (Ioc_MulFactorType) ((220+0)* IOC_VOLT_MUL),        /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) ((1)    * IOC_VOLT_DIV)         /* DivFactor :       R2  * IOC_VOLT_DIV */
    },
    /* IOC_VOLT_EPB_I1_R_MON        */
    {
        (Ioc_ModuleIDType)   ModuleID_Adc     ,               /* ModuleID */
        (Ioc_MulFactorType) ((220+0)* IOC_VOLT_MUL),        /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) ((1)    * IOC_VOLT_DIV)         /* DivFactor :       R2  * IOC_VOLT_DIV */
    },
    /* IOC_VOLT_EPB_I2_L_MON        */
    {
        (Ioc_ModuleIDType)   ModuleID_Adc     ,               /* ModuleID */
        (Ioc_MulFactorType) ((220+0)* IOC_VOLT_MUL),        /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) ((1)    * IOC_VOLT_DIV)         /* DivFactor :       R2  * IOC_VOLT_DIV */
    },
    /* IOC_VOLT_EPB_I2_R_MON        */
    {
        (Ioc_ModuleIDType)   ModuleID_Adc     ,               /* ModuleID */
        (Ioc_MulFactorType) ((220+0)* IOC_VOLT_MUL),        /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) ((1)    * IOC_VOLT_DIV)         /* DivFactor :       R2  * IOC_VOLT_DIV */
    },
    /* IOC_VOLT_EPB_MON_L_PLUS      */
    {
        (Ioc_ModuleIDType)   ModuleID_Adc     ,               /* ModuleID */
        (Ioc_MulFactorType) ((680+130)* IOC_VOLT_MUL),        /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) ((130)    * IOC_VOLT_DIV)         /* DivFactor :       R2  * IOC_VOLT_DIV */
    },
    /* IOC_VOLT_EPB_MON_L_MINUS     */
    {
        (Ioc_ModuleIDType)   ModuleID_Adc     ,               /* ModuleID */
        (Ioc_MulFactorType) ((680+130)* IOC_VOLT_MUL),        /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) ((130)    * IOC_VOLT_DIV)         /* DivFactor :       R2  * IOC_VOLT_DIV */
    },
    /* IOC_VOLT_EPB_MON_R_PLUS      */
    {
        (Ioc_ModuleIDType)   ModuleID_Adc     ,               /* ModuleID */
        (Ioc_MulFactorType) ((680+130)* IOC_VOLT_MUL),        /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) ((130)    * IOC_VOLT_DIV)         /* DivFactor :       R2  * IOC_VOLT_DIV */
    },
    /* IOC_VOLT_EPB_MON_R_MINUS     */
    {
        (Ioc_ModuleIDType)   ModuleID_Adc     ,               /* ModuleID */
        (Ioc_MulFactorType) ((680+130)* IOC_VOLT_MUL),        /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) ((1)    * IOC_VOLT_DIV)         /* DivFactor :       R2  * IOC_VOLT_DIV */
    },
    /* IOC_VOLT_MCU_ADC1            */
    {
        (Ioc_ModuleIDType)   ModuleID_Adc     ,               /* ModuleID */
        (Ioc_MulFactorType) ((300+100)* IOC_VOLT_MUL),        /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) ((100)    * IOC_VOLT_DIV)         /* DivFactor :       R2  * IOC_VOLT_DIV */
    },
    /* IOC_VOLT_MCU_ADC2            */
    {
        (Ioc_ModuleIDType)   ModuleID_Adc     ,               /* ModuleID */
        (Ioc_MulFactorType) ((300+100)* IOC_VOLT_MUL),        /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) ((100)    * IOC_VOLT_DIV)         /* DivFactor :       R2  * IOC_VOLT_DIV */
    },
    /* IOC_VOLT_MCU_ADC3            */
    {
        (Ioc_ModuleIDType)   ModuleID_Adc     ,               /* ModuleID */
        (Ioc_MulFactorType) ((300+100)* IOC_VOLT_MUL),        /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) ((100)    * IOC_VOLT_DIV)         /* DivFactor :       R2  * IOC_VOLT_DIV */
    },
    /* IOC_VOLT_MCU_ADC4            */
    {
        (Ioc_ModuleIDType)   ModuleID_Adc     ,               /* ModuleID */
        (Ioc_MulFactorType) ((300+100)* IOC_VOLT_MUL),        /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) ((100)    * IOC_VOLT_DIV)         /* DivFactor :       R2  * IOC_VOLT_DIV */
    },
    /* IOC_VOLT_MCU_ADC5            */
    {
        (Ioc_ModuleIDType)   ModuleID_Adc     ,               /* ModuleID */
        (Ioc_MulFactorType) ((300+100)* IOC_VOLT_MUL),        /* MulFactor : (R1 + R2) * IOC_VOLT_MUL */
        (Ioc_DivFactorType) ((100)    * IOC_VOLT_DIV)         /* DivFactor :       R2  * IOC_VOLT_DIV */
    },
    /* IOC_OUT_PORT_VLV_PRI_CV */ /* NZ Quick C : RLV */
    {
        (Ioc_ModuleIDType)  ModuleID_Pwm,                                 /* ModuleID */
        (Ioc_MulFactorType) 625,                                          /* Duty Percent */
        (Ioc_DivFactorType) 1000                                          /* Duty Percent */
    },
    /* IOC_OUT_PORT_VLV_SEC_CV */ /* NZ Quick C : CV */
    {
        (Ioc_ModuleIDType)  ModuleID_Pwm,                                 /* ModuleID */
        (Ioc_MulFactorType) 625,                                          /* Duty Percent */
        (Ioc_DivFactorType) 1000                                          /* Duty Percent */
    }
};

/* Valve Config */    /* for 100ASIC */
const Ioc_RoutingType Ioc_ValveRoutingConfig[IOC_VALVE_MAX_NUM] =
{
    /* HAL_VAVLE_NO_CH0     (Hal_PortType)  0  */
    {
        (Ioc_ModuleIDType)   ModuleID_Spi     ,               /* ModuleID */
        (Ioc_MulFactorType) (1500),                           /* MulFactor : 2047 = 3070mA to ASIC */
        (Ioc_DivFactorType) (2250)                             /* DivFactor : ASW resolution 1 = 1mA */
    },
    /* HAL_VAVLE_NO_CH1     (Hal_PortType)  1  */
    {
        (Ioc_ModuleIDType)   ModuleID_Spi     ,               /* ModuleID */
        (Ioc_MulFactorType) (1500),                           /* MulFactor : 2047 = 3070mA to ASIC */
        (Ioc_DivFactorType) (2250)                             /* DivFactor : ASW resolution 1 = 1mA */
    },
    /* HAL_VAVLE_NO_CH2     (Hal_PortType)  2  */
    {
        (Ioc_ModuleIDType)   ModuleID_Spi     ,               /* ModuleID */
        (Ioc_MulFactorType) (1500),                           /* MulFactor : 2047 = 3070mA to ASIC */
        (Ioc_DivFactorType) (2250)                             /* DivFactor : ASW resolution 1 = 1mA */
    },
    /* HAL_VAVLE_NO_CH3     (Hal_PortType)  3  */
    {
        (Ioc_ModuleIDType)   ModuleID_Spi     ,               /* ModuleID */
        (Ioc_MulFactorType) (1500),                           /* MulFactor : 2047 = 3070mA to ASIC */
        (Ioc_DivFactorType) (2250)                             /* DivFactor : ASW resolution 1 = 1mA */
    },
    /* HAL_VAVLE_TC_CH0     (Hal_PortType)  4  */
    {
        (Ioc_ModuleIDType)   ModuleID_Spi     ,               /* ModuleID */
        (Ioc_MulFactorType) (1500),                           /* MulFactor : 2047 = 3070mA to ASIC */
        (Ioc_DivFactorType) (2250)                             /* DivFactor : ASW resolution 1 = 1mA */
    },
    /* HAL_VAVLE_TC_CH1     (Hal_PortType)  5  */
    {
        (Ioc_ModuleIDType)   ModuleID_Spi     ,               /* ModuleID */
        (Ioc_MulFactorType) (1500),                           /* MulFactor : 2047 = 3070mA to ASIC */
        (Ioc_DivFactorType) (2250)                             /* DivFactor : ASW resolution 1 = 1mA */
    },
    /* HAL_VAVLE_ESV_CH0     (Hal_PortType)  6  */
    {
        (Ioc_ModuleIDType)   ModuleID_Spi     ,               /* ModuleID */
        (Ioc_MulFactorType) (1500),                           /* MulFactor : 2047 = 3070mA to ASIC */
        (Ioc_DivFactorType) (2250)                             /* DivFactor : ASW resolution 1 = 1mA */
    },
    /* HAL_VAVLE_ESV_CH1     (Hal_PortType)  7  */
    {
        (Ioc_ModuleIDType)   ModuleID_Spi     ,               /* ModuleID */
        (Ioc_MulFactorType) (1500),                           /* MulFactor : 2047 = 3070mA to ASIC */
        (Ioc_DivFactorType) (2250)                             /* DivFactor : ASW resolution 1 = 1mA */
    },
    /* HAL_VAVLE_NC_CH0     (Hal_PortType)  8  */
    {
        (Ioc_ModuleIDType)   ModuleID_Spi     ,               /* ModuleID */
        (Ioc_MulFactorType) (1),                              /* MulFactor : ON/OFF Control */
        (Ioc_DivFactorType) (1)                               /* DivFactor : ON/OFF Control */
    },
    /* HAL_VAVLE_NC_CH1     (Hal_PortType)  9  */
    {
        (Ioc_ModuleIDType)   ModuleID_Spi     ,               /* ModuleID */
        (Ioc_MulFactorType) (1),                              /* MulFactor : ON/OFF Control */
        (Ioc_DivFactorType) (1)                               /* DivFactor : ON/OFF Control */
    },
    /* HAL_VAVLE_NC_CH2     (Hal_PortType)  10  */
    {
        (Ioc_ModuleIDType)   ModuleID_Spi     ,               /* ModuleID */
        (Ioc_MulFactorType) (1),                              /* MulFactor : ON/OFF Control */
        (Ioc_DivFactorType) (1)                               /* DivFactor : ON/OFF Control */
    },
    /* HAL_VAVLE_NC_CH3     (Hal_PortType)  11  */
    {
        (Ioc_ModuleIDType)     ModuleID_Spi     ,               /* ModuleID */
        (Ioc_MulFactorType)   (1),                              /* MulFactor : ON/OFF Control */
        (Ioc_DivFactorType)   (1)                               /* DivFactor : ON/OFF Control */
    }
};

const Ioc_RoutingType Ioc_MotorRoutingConfig[IOC_MOTOR_MAX_NUM] =
{
    /* HAL_MOTOR_ABS        (Hal_PortType)  0  */
    {
        (Ioc_ModuleIDType)   ModuleID_Spi     ,               /* ModuleID */
        (Ioc_MulFactorType) (100),                            /* MulFactor : 100 = 100% Duty to ASIC */
        (Ioc_DivFactorType) (200)                             /* DivFactor : Receiving Duty range is 0 ~ 200 from RTE */
    }
};

const Asic100DriverRoutingType Asic100DriverRoutingConfig =
{
        Asic100_FSR_CBS,        /* FailSafePreDriver */
        Asic100_FSR_ABS,        /* PumpMotorPreDriver */
        Asic100_VsoDrv,         /* VsoDriver */
        Asic100_WldDrv,         /* WldDriver */
        Asic100_KlineDrv,       /* KlineDriver */
        Asic100_ShlsDrv         /* ShlsDriver */
};

const Asic100ValveUsageType Asic100ValveMappingConfig[IOC_VALVE_MAX_NUM] =
{
    Asic100_RLIV,                    	/* NO_CH0, H/W Pin : NO1 */
    Asic100_RRIV,                    	/* NO_CH1, H/W Pin : NO2 */
    Asic100_FRIV,                    	/* NO_CH2, H/W Pin : NO3 */
    Asic100_FLIV,                    	/* NO_CH3, H/W Pin : NO4 */
    Asic100_CBAL/*NZ ECU_C: BAL-TC1*/,	/* TC_CH0, H/W Pin : TC1 */ 
    Asic100_SBAL/*NZ ECU_C: PD-TC2*/, 	/* TC_CH1, H/W Pin : TC2 */ 
    Asic100_VlvNotUsed,              	/* ESV_CH0, H/W Pin : ESV1 */
    Asic100_VlvNotUsed,              	/* ESV_CH1, H/W Pin : ESV2 */
    Asic100_FROV,                    	/* NC_CH0, H/W Pin : NC1 */
    Asic100_RROV,                    	/* NC_CH1, H/W Pin : NC2 */
    Asic100_FLOV,                    	/* NC_CH2, H/W Pin : NC3 */
    Asic100_RLOV                     	/* NC_CH3, H/W Pin : NC4 */
};

const Ioc_DioMappingType Ioc_DioMapping =
{
    /* GPDO */
    (Dio_ChannelType)DIO_CHANNEL_32_7,        /* MOC_RL_DRV */                   /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_23_6,        /* S_CAN_KILL */                   /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_0_4,         /* MOC_RR_EN */                    /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_0_2,         /* RELAY_DBC_DRV */                /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_22_4,        /* PRN */                          /* COMMON */
    (Dio_ChannelType)DIO_CHANNEL_22_10,       /* LIN_EN */                       /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_1_3,         /* M_CAN_ERR */                    /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_2_8,         /* PDT5V_INH */                    /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_2_9,         /* M_CAN_KILL */                   /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_2_10,        /* D_CHAGE_DRV */                  /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_2_11,        /* RELAY_EN */                     /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_11_0,        /* WAKE_UP_STB */                  /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_11_1,        /* LINE_TEST */                    /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_11_8,        /* MCU_FRA1_BGE */                 /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_33_15,        /* ENB_MOC_POWER */                /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_33_8,        /* MOC_RL_DIR */                   /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_13_1,        /* MOC_RR_DRV */                   /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_14_8,        /* INHIBIT */                      /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_14_10,       /* SW1LINE_C_DRV */                /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_15_4,        /* SW2LINE_B_DRV */                /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_15_5,        /* SW3LINE_E_DRV */                /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_20_3,        /* SW4LINE_F_DRV */                /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_20_10,       /* PDF5V_INH */                    /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_21_0,        /* MOC_RL_EN */                    /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_21_1,        /* M_CAN_EN, CAN_EN */             /* COMMON */
    (Dio_ChannelType)DIO_CHANNEL_21_3,        /* SPARE3_DRV */                   /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_21_4,        /* EN_DR */                        /* COMMON */
    (Dio_ChannelType)DIO_CHANNEL_22_2,        /* MOC_RR_DIR */                   /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_22_5,        /* VDD1_MOC_DRV */                 /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_22_6,        /* VDD2_MOC_DRV */                 /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_22_7,        /* MCU_FRA2_BGE */                 /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_22_8,        /* RELAY_ESS_DRV */                /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_22_9,        /* VSO1_DRV, VSO_DRV */            /*COMMON*/
    (Dio_ChannelType)DIO_CHANNEL_23_0,        /* MCU_FRA1_MCU_INH */             /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_23_2,        /* MTR_FET_SD */                   /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_23_3,        /* MTR_IC_EN */                    /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_23_7,        /* MOT_DRV_EN */                   /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_32_4,        /* MCU_FRA2_STBN */                /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_32_5,        /* FSR_ABS_OFF, FSR_SD */          /* COMMON */ 
    (Dio_ChannelType)DIO_CHANNEL_32_6,        /* MTR_IC_INHIBIT */               /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_33_9,        /* MCU_FRA1_EN, FRA_EN_A */        /* COMMON */
    (Dio_ChannelType)DIO_CHANNEL_33_10,       /* CSP_DRV */                      /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_33_11,       /* MCU_FRA1_STBN, FRA_STBN_A */    /* COMMON */
    (Dio_ChannelType)DIO_CHANNEL_33_14,       /* FSR_CBS_OFF */                  /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_34_4,        /* MCU_FRA2_MCU_INH */             /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_34_5,        /* MCU_FRA2_EN */                  /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_22_11,       /* VDR_EN */                       /* IDB */
    (Dio_ChannelType)DIO_CHANNEL_NOT_USED,    /* MTP_SD */                       /* ESC */
    (Dio_ChannelType)DIO_CHANNEL_NOT_USED,    /* SAFE_LS2_R */                   /* ESC */
    (Dio_ChannelType)DIO_CHANNEL_NOT_USED,    /* RELAY2_DRV */                   /* ESC */
    (Dio_ChannelType)DIO_CHANNEL_NOT_USED,    /* RELAY1_DRV */                   /* ESC */
    (Dio_ChannelType)DIO_CHANNEL_NOT_USED,    /* 5VREF_02_INHIBIT */             /* ESC */
    (Dio_ChannelType)DIO_CHANNEL_NOT_USED,    /* 5VREF_01_INHIBIT */             /* ESC */
    (Dio_ChannelType)DIO_CHANNEL_NOT_USED,    /* SAFE_LS1_R */                   /* ESC */
    (Dio_ChannelType)DIO_CHANNEL_NOT_USED,    /* SAFE_LS2_L */                   /* ESC */
    (Dio_ChannelType)DIO_CHANNEL_NOT_USED,    /* VAC_PUMP_RELAY_DRV */           /* ESC */
    (Dio_ChannelType)DIO_CHANNEL_NOT_USED,    /* VDD_DRV */                      /* ESC */

    /* GPDI */
    (Dio_ChannelType)DIO_CHANNEL_0_10,       /* AVH_SW_MON */
    (Dio_ChannelType)DIO_CHANNEL_11_4,       /* PB_SW_MON */
    (Dio_ChannelType)DIO_CHANNEL_11_5,       /* HDC_SW_MON */
    (Dio_ChannelType)DIO_CHANNEL_11_7,       /* RELAY_FAULT */
    (Dio_ChannelType)DIO_CHANNEL_11_9,       /* BLS_MON */
    (Dio_ChannelType)DIO_CHANNEL_11_10,      /* FLEX_BRAKE_A_MON */
    (Dio_ChannelType)DIO_CHANNEL_11_12,      /* RELAY_ESS_MON */
    (Dio_ChannelType)DIO_CHANNEL_11_14,      /* HAZARD_SW_MON */
    (Dio_ChannelType)DIO_CHANNEL_11_15,      /* ESC_SW_MON */
    (Dio_ChannelType)DIO_CHANNEL_13_0,       /* DOOR_SW_MON */
    (Dio_ChannelType)DIO_CHANNEL_13_2,       /* BS_SW_MON */
    (Dio_ChannelType)DIO_CHANNEL_13_3,       /* FLEX_BRAKE_B_MON */
    (Dio_ChannelType)DIO_CHANNEL_20_6,       /* SW1LINE_C_MON */
    (Dio_ChannelType)DIO_CHANNEL_21_2,       /* RELAY_DBC_MON */
    (Dio_ChannelType)DIO_CHANNEL_21_5,       /* SW2LINE_B_MON */
    (Dio_ChannelType)DIO_CHANNEL_22_0,       /* MCU_FRA1_RXEN, FRA_RXEN_A */    /* COMMON */
    (Dio_ChannelType)DIO_CHANNEL_22_1,       /* MCU_FRA2_RXEN */
    (Dio_ChannelType)DIO_CHANNEL_32_0,       /* MTR_IC_ERR */
    (Dio_ChannelType)DIO_CHANNEL_32_2,       /* SW3LINE_E_MON */
    (Dio_ChannelType)DIO_CHANNEL_32_3,       /* SW4LINE_F_MON */
    (Dio_ChannelType)DIO_CHANNEL_33_0,       /* MCU_FRA2_ERRN */
    (Dio_ChannelType)DIO_CHANNEL_34_2        /* MCU_FRA1_ERRN, FRA_ERRN */      /* COMMON */
};


/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define IOC_STOP_SEC_CONST_UNSPECIFIED
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define IOC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define IOC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_START_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define IOC_STOP_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
#define IOC_START_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define IOC_STOP_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_START_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (32BIT)**/


#define IOC_STOP_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define IOC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define IOC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_START_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define IOC_STOP_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
#define IOC_START_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define IOC_STOP_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_START_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (32BIT)**/


#define IOC_STOP_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define IOC_START_SEC_CODE
#include "Ioc_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define IOC_STOP_SEC_CODE
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
