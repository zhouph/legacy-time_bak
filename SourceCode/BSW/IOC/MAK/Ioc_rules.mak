# \file
#
# \brief Ioc
#
# This file contains the implementation of the SWC
# module Ioc.
#
# \author Mando, Advanced R&D, Korea

# REGISTRY

LIBRARIES_TO_BUILD     += Ioc_src

Ioc_src_FILES        += $(Ioc_SRC_PATH)\Ioc_InputSR50us.c
Ioc_src_FILES        += $(Ioc_SRC_PATH)\Ioc_InputSR50usConditioning.c
Ioc_src_FILES        += $(Ioc_IFA_PATH)\Ioc_InputSR50us_Ifa.c
Ioc_src_FILES        += $(Ioc_SRC_PATH)\Ioc_OutputCS50us.c
Ioc_src_FILES        += $(Ioc_SRC_PATH)\Ioc_OutputCS50usConditioning.c
Ioc_src_FILES        += $(Ioc_IFA_PATH)\Ioc_OutputCS50us_Ifa.c
Ioc_src_FILES        += $(Ioc_SRC_PATH)\Ioc_InputSR1ms.c
Ioc_src_FILES        += $(Ioc_SRC_PATH)\Ioc_InputSR1msConditioning.c
Ioc_src_FILES        += $(Ioc_IFA_PATH)\Ioc_InputSR1ms_Ifa.c
Ioc_src_FILES        += $(Ioc_SRC_PATH)\Ioc_InputCS1ms.c
Ioc_src_FILES        += $(Ioc_SRC_PATH)\Ioc_InputCS1msConditioning.c
Ioc_src_FILES        += $(Ioc_IFA_PATH)\Ioc_InputCS1ms_Ifa.c
Ioc_src_FILES        += $(Ioc_SRC_PATH)\Ioc_OutputSR1ms.c
Ioc_src_FILES        += $(Ioc_SRC_PATH)\Ioc_OutputSR1msConditioning.c
Ioc_src_FILES        += $(Ioc_IFA_PATH)\Ioc_OutputSR1ms_Ifa.c
Ioc_src_FILES        += $(Ioc_SRC_PATH)\Ioc_OutputCS1ms.c
Ioc_src_FILES        += $(Ioc_SRC_PATH)\Ioc_OutputCS1msConditioning.c
Ioc_src_FILES        += $(Ioc_IFA_PATH)\Ioc_OutputCS1ms_Ifa.c
Ioc_src_FILES        += $(Ioc_SRC_PATH)\Ioc_InputSR5ms.c
Ioc_src_FILES        += $(Ioc_SRC_PATH)\Ioc_InputSR5msConditioning.c
Ioc_src_FILES        += $(Ioc_IFA_PATH)\Ioc_InputSR5ms_Ifa.c
Ioc_src_FILES        += $(Ioc_SRC_PATH)\Ioc_OutputCS5ms.c
Ioc_src_FILES        += $(Ioc_SRC_PATH)\Ioc_OutputCS5msConditioning.c
Ioc_src_FILES        += $(Ioc_IFA_PATH)\Ioc_OutputCS5ms_Ifa.c
Ioc_src_FILES        += $(Ioc_CFG_PATH)\Ioc_Cfg.c
Ioc_src_FILES        += $(Ioc_CAL_PATH)\Ioc_Cal.c

ifeq ($(ICE_COMPILE),true)
Ioc_src_FILES        += $(Ioc_CORE_PATH)\virtual_main.c
endif

ifeq ($(UNIT_TEST),true)
	Ioc_src_FILES        += $(Ioc_UNITY_PATH)\unity.c
	Ioc_src_FILES        += $(Ioc_UNITY_PATH)\unity_fixture.c	
	Ioc_src_FILES        += $(Ioc_UT_PATH)\main.c
	Ioc_src_FILES        += $(Ioc_UT_PATH)\Ioc_InputSR50us\Ioc_InputSR50us_UtMain.c
	Ioc_src_FILES        += $(Ioc_UT_PATH)\Ioc_OutputCS50us\Ioc_OutputCS50us_UtMain.c
	Ioc_src_FILES        += $(Ioc_UT_PATH)\Ioc_InputSR1ms\Ioc_InputSR1ms_UtMain.c
	Ioc_src_FILES        += $(Ioc_UT_PATH)\Ioc_InputCS1ms\Ioc_InputCS1ms_UtMain.c
	Ioc_src_FILES        += $(Ioc_UT_PATH)\Ioc_OutputSR1ms\Ioc_OutputSR1ms_UtMain.c
	Ioc_src_FILES        += $(Ioc_UT_PATH)\Ioc_OutputCS1ms\Ioc_OutputCS1ms_UtMain.c
	Ioc_src_FILES        += $(Ioc_UT_PATH)\Ioc_InputSR5ms\Ioc_InputSR5ms_UtMain.c
	Ioc_src_FILES        += $(Ioc_UT_PATH)\Ioc_OutputCS5ms\Ioc_OutputCS5ms_UtMain.c
endif