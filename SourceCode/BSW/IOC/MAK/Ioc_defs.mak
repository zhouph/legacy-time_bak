# \file
#
# \brief Ioc
#
# This file contains the implementation of the SWC
# module Ioc.
#
# \author Mando, Advanced R&D, Korea

# DEFINITIONS

Ioc_CORE_PATH     := $(MANDO_BSW_ROOT)\Ioc
Ioc_CAL_PATH      := $(Ioc_CORE_PATH)\CAL\$(Ioc_VARIANT)
Ioc_SRC_PATH      := $(Ioc_CORE_PATH)\SRC
Ioc_CFG_PATH      := $(Ioc_CORE_PATH)\CFG\$(Ioc_VARIANT)
Ioc_HDR_PATH      := $(Ioc_CORE_PATH)\HDR
Ioc_IFA_PATH      := $(Ioc_CORE_PATH)\IFA

ifeq ($(ICE_COMPILE), true)
    Ioc_CMN_PATH      := $(Ioc_CORE_PATH)\ICE\CMN
endif
ifeq ($(UNIT_TEST),true)
	Ioc_UT_PATH		:= $(Ioc_CORE_PATH)\UT
	Ioc_UNITY_PATH	:= $(Ioc_CORE_PATH)\UT\Unity
	CC_INCLUDE_PATH		+= $(Ioc_UT_PATH)
	CC_INCLUDE_PATH		+= $(Ioc_UNITY_PATH)
	Ioc_InputSR50us_PATH 	:= Ioc_UT_PATH\Ioc_InputSR50us
	Ioc_OutputCS50us_PATH 	:= Ioc_UT_PATH\Ioc_OutputCS50us
	Ioc_InputSR1ms_PATH 	:= Ioc_UT_PATH\Ioc_InputSR1ms
	Ioc_InputCS1ms_PATH 	:= Ioc_UT_PATH\Ioc_InputCS1ms
	Ioc_OutputSR1ms_PATH 	:= Ioc_UT_PATH\Ioc_OutputSR1ms
	Ioc_OutputCS1ms_PATH 	:= Ioc_UT_PATH\Ioc_OutputCS1ms
	Ioc_InputSR5ms_PATH 	:= Ioc_UT_PATH\Ioc_InputSR5ms
	Ioc_OutputCS5ms_PATH 	:= Ioc_UT_PATH\Ioc_OutputCS5ms
endif
CC_INCLUDE_PATH    += $(Ioc_CAL_PATH)
CC_INCLUDE_PATH    += $(Ioc_SRC_PATH)
CC_INCLUDE_PATH    += $(Ioc_CFG_PATH)
CC_INCLUDE_PATH    += $(Ioc_HDR_PATH)
CC_INCLUDE_PATH    += $(Ioc_IFA_PATH)
CC_INCLUDE_PATH    += $(Ioc_CMN_PATH)

