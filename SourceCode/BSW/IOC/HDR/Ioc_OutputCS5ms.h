/**
 * @defgroup Ioc_OutputCS5ms Ioc_OutputCS5ms
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_OutputCS5ms.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef IOC_OUTPUTCS5MS_H_
#define IOC_OUTPUTCS5MS_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ioc_Types.h"
#include "Ioc_Cfg.h"
#include "Ioc_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define IOC_OUTPUTCS5MS_MODULE_ID      (0)
 #define IOC_OUTPUTCS5MS_MAJOR_VERSION  (2)
 #define IOC_OUTPUTCS5MS_MINOR_VERSION  (0)
 #define IOC_OUTPUTCS5MS_PATCH_VERSION  (0)
 #define IOC_OUTPUTCS5MS_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Ioc_OutputCS5ms_HdrBusType Ioc_OutputCS5msBus;

/* Version Info */
extern const SwcVersionInfo_t Ioc_OutputCS5msVersionInfo;

/* Input Data Element */
extern Mom_HndlrEcuModeSts_t Ioc_OutputCS5msEcuModeSts;
extern Acm_MainAcmAsicInitCompleteFlag_t Ioc_OutputCS5msAcmAsicInitCompleteFlag;
extern Eem_SuspcDetnFuncInhibitIocSts_t Ioc_OutputCS5msFuncInhibitIocSts;

/* Output Data Element */

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Ioc_OutputCS5ms_Init(void);
extern void Ioc_OutputCS5ms(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* IOC_OUTPUTCS5MS_H_ */
/** @} */
