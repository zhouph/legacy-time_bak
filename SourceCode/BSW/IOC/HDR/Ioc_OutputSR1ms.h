/**
 * @defgroup Ioc_OutputSR1ms Ioc_OutputSR1ms
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_OutputSR1ms.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef IOC_OUTPUTSR1MS_H_
#define IOC_OUTPUTSR1MS_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ioc_Types.h"
#include "Ioc_Cfg.h"
#include "Ioc_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define IOC_OUTPUTSR1MS_MODULE_ID      (0)
 #define IOC_OUTPUTSR1MS_MAJOR_VERSION  (2)
 #define IOC_OUTPUTSR1MS_MINOR_VERSION  (0)
 #define IOC_OUTPUTSR1MS_PATCH_VERSION  (0)
 #define IOC_OUTPUTSR1MS_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Ioc_OutputSR1ms_HdrBusType Ioc_OutputSR1msBus;

/* Version Info */
extern const SwcVersionInfo_t Ioc_OutputSR1msVersionInfo;

/* Input Data Element */
extern Vlv_ActrWhlVlvDrvInfo_t Ioc_OutputSR1msWhlVlvDrvInfo;
extern Vlv_ActrBalVlvDrvInfo_t Ioc_OutputSR1msBalVlvDrvInfo;
extern Fsr_ActrFsrCbsDrvInfo_t Ioc_OutputSR1msFsrCbsDrvInfo;
extern Fsr_ActrFsrAbsDrvInfo_t Ioc_OutputSR1msFsrAbsDrvInfo;
extern Mom_HndlrEcuModeSts_t Ioc_OutputSR1msEcuModeSts;
extern Acm_MainAcmAsicInitCompleteFlag_t Ioc_OutputSR1msAcmAsicInitCompleteFlag;
extern Eem_SuspcDetnFuncInhibitIocSts_t Ioc_OutputSR1msFuncInhibitIocSts;

/* Output Data Element */
extern Ioc_OutputSR1msIocDcMtrDutyData_t Ioc_OutputSR1msIocDcMtrDutyData;
extern Ioc_OutputSR1msIocDcMtrFreqData_t Ioc_OutputSR1msIocDcMtrFreqData;
extern Ioc_OutputSR1msIocVlvNo0Data_t Ioc_OutputSR1msIocVlvNo0Data;
extern Ioc_OutputSR1msIocVlvNo1Data_t Ioc_OutputSR1msIocVlvNo1Data;
extern Ioc_OutputSR1msIocVlvNo2Data_t Ioc_OutputSR1msIocVlvNo2Data;
extern Ioc_OutputSR1msIocVlvNo3Data_t Ioc_OutputSR1msIocVlvNo3Data;
extern Ioc_OutputSR1msIocVlvTc0Data_t Ioc_OutputSR1msIocVlvTc0Data;
extern Ioc_OutputSR1msIocVlvTc1Data_t Ioc_OutputSR1msIocVlvTc1Data;
extern Ioc_OutputSR1msIocVlvEsv0Data_t Ioc_OutputSR1msIocVlvEsv0Data;
extern Ioc_OutputSR1msIocVlvEsv1Data_t Ioc_OutputSR1msIocVlvEsv1Data;
extern Ioc_OutputSR1msIocVlvNc0Data_t Ioc_OutputSR1msIocVlvNc0Data;
extern Ioc_OutputSR1msIocVlvNc1Data_t Ioc_OutputSR1msIocVlvNc1Data;
extern Ioc_OutputSR1msIocVlvNc2Data_t Ioc_OutputSR1msIocVlvNc2Data;
extern Ioc_OutputSR1msIocVlvNc3Data_t Ioc_OutputSR1msIocVlvNc3Data;
extern Ioc_OutputSR1msIocVlvRelayData_t Ioc_OutputSR1msIocVlvRelayData;
extern Ioc_OutputSR1msIocAdcSelWriteData_t Ioc_OutputSR1msIocAdcSelWriteData;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Ioc_OutputSR1ms_Init(void);
extern void Ioc_OutputSR1ms(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* IOC_OUTPUTSR1MS_H_ */
/** @} */
