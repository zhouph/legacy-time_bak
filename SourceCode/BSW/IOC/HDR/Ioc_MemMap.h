/**
 * @defgroup Ioc_MemMap Ioc_MemMap
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_MemMap.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef IOC_MEMMAP_H_
#define IOC_MEMMAP_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/*------------------[MemMap error checking]----------------------------------*/

#if (defined MEMMAP_ERROR) /* to prevent double definition */
#error MEMMAP_ERROR defined, wrong MemMap.h usage
#endif /* if (!defined MEMMAP_ERROR) */

/** \brief Checks if this file has been correctly used
 **
 ** This definition checks if this file has been correctly included
 ** the last time.
 ** We do it here, before the big if-elif. */
#define MEMMAP_ERROR


#if 0
/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (IOC_START_SEC_CODE)
  #undef IOC_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_SEC_STARTED
    #error "IOC section not closed"
  #endif
  #define CHK_IOC_SEC_STARTED
  #define CHK_IOC_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_STOP_SEC_CODE)
  #undef IOC_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_SEC_CODE_STARTED
    #error "IOC_SEC_CODE not opened"
  #endif
  #undef CHK_IOC_SEC_STARTED
  #undef CHK_IOC_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (IOC_START_SEC_CONST_UNSPECIFIED)
  #undef IOC_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_SEC_STARTED
    #error "IOC section not closed"
  #endif
  #define CHK_IOC_SEC_STARTED
  #define CHK_IOC_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_STOP_SEC_CONST_UNSPECIFIED)
  #undef IOC_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_SEC_CONST_UNSPECIFIED_STARTED
    #error "IOC_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_SEC_STARTED
  #undef CHK_IOC_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (IOC_START_SEC_VAR_UNSPECIFIED)
  #undef IOC_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_SEC_STARTED
    #error "IOC section not closed"
  #endif
  #define CHK_IOC_SEC_STARTED
  #define CHK_IOC_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_STOP_SEC_VAR_UNSPECIFIED)
  #undef IOC_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_SEC_VAR_UNSPECIFIED_STARTED
    #error "IOC_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_SEC_STARTED
  #undef CHK_IOC_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_START_SEC_VAR_32BIT)
  #undef IOC_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_SEC_STARTED
    #error "IOC section not closed"
  #endif
  #define CHK_IOC_SEC_STARTED
  #define CHK_IOC_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_STOP_SEC_VAR_32BIT)
  #undef IOC_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_SEC_VAR_32BIT_STARTED
    #error "IOC_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_IOC_SEC_STARTED
  #undef CHK_IOC_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef IOC_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_SEC_STARTED
    #error "IOC section not closed"
  #endif
  #define CHK_IOC_SEC_STARTED
  #define CHK_IOC_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef IOC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "IOC_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_SEC_STARTED
  #undef CHK_IOC_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_START_SEC_VAR_NOINIT_32BIT)
  #undef IOC_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_SEC_STARTED
    #error "IOC section not closed"
  #endif
  #define CHK_IOC_SEC_STARTED
  #define CHK_IOC_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_STOP_SEC_VAR_NOINIT_32BIT)
  #undef IOC_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_SEC_VAR_NOINIT_32BIT_STARTED
    #error "IOC_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_IOC_SEC_STARTED
  #undef CHK_IOC_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (IOC_START_SEC_CALIB_UNSPECIFIED)
  #undef IOC_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_SEC_STARTED
    #error "IOC section not closed"
  #endif
  #define CHK_IOC_SEC_STARTED
  #define CHK_IOC_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_STOP_SEC_CALIB_UNSPECIFIED)
  #undef IOC_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "IOC_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_SEC_STARTED
  #undef CHK_IOC_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR



/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (IOC_INPUTSR50US_START_SEC_CODE)
  #undef IOC_INPUTSR50US_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_INPUTSR50US_SEC_STARTED
    #error "IOC_INPUTSR50US section not closed"
  #endif
  #define CHK_IOC_INPUTSR50US_SEC_STARTED
  #define CHK_IOC_INPUTSR50US_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTSR50US_STOP_SEC_CODE)
  #undef IOC_INPUTSR50US_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_INPUTSR50US_SEC_CODE_STARTED
    #error "IOC_INPUTSR50US_SEC_CODE not opened"
  #endif
  #undef CHK_IOC_INPUTSR50US_SEC_STARTED
  #undef CHK_IOC_INPUTSR50US_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (IOC_INPUTSR50US_START_SEC_CONST_UNSPECIFIED)
  #undef IOC_INPUTSR50US_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_INPUTSR50US_SEC_STARTED
    #error "IOC_INPUTSR50US section not closed"
  #endif
  #define CHK_IOC_INPUTSR50US_SEC_STARTED
  #define CHK_IOC_INPUTSR50US_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTSR50US_STOP_SEC_CONST_UNSPECIFIED)
  #undef IOC_INPUTSR50US_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_INPUTSR50US_SEC_CONST_UNSPECIFIED_STARTED
    #error "IOC_INPUTSR50US_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_INPUTSR50US_SEC_STARTED
  #undef CHK_IOC_INPUTSR50US_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (IOC_INPUTSR50US_START_SEC_VAR_UNSPECIFIED)
  #undef IOC_INPUTSR50US_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_INPUTSR50US_SEC_STARTED
    #error "IOC_INPUTSR50US section not closed"
  #endif
  #define CHK_IOC_INPUTSR50US_SEC_STARTED
  #define CHK_IOC_INPUTSR50US_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTSR50US_STOP_SEC_VAR_UNSPECIFIED)
  #undef IOC_INPUTSR50US_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_INPUTSR50US_SEC_VAR_UNSPECIFIED_STARTED
    #error "IOC_INPUTSR50US_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_INPUTSR50US_SEC_STARTED
  #undef CHK_IOC_INPUTSR50US_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTSR50US_START_SEC_VAR_32BIT)
  #undef IOC_INPUTSR50US_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_INPUTSR50US_SEC_STARTED
    #error "IOC_INPUTSR50US section not closed"
  #endif
  #define CHK_IOC_INPUTSR50US_SEC_STARTED
  #define CHK_IOC_INPUTSR50US_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTSR50US_STOP_SEC_VAR_32BIT)
  #undef IOC_INPUTSR50US_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_INPUTSR50US_SEC_VAR_32BIT_STARTED
    #error "IOC_INPUTSR50US_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_IOC_INPUTSR50US_SEC_STARTED
  #undef CHK_IOC_INPUTSR50US_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTSR50US_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef IOC_INPUTSR50US_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_INPUTSR50US_SEC_STARTED
    #error "IOC_INPUTSR50US section not closed"
  #endif
  #define CHK_IOC_INPUTSR50US_SEC_STARTED
  #define CHK_IOC_INPUTSR50US_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTSR50US_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef IOC_INPUTSR50US_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_INPUTSR50US_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "IOC_INPUTSR50US_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_INPUTSR50US_SEC_STARTED
  #undef CHK_IOC_INPUTSR50US_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTSR50US_START_SEC_VAR_NOINIT_32BIT)
  #undef IOC_INPUTSR50US_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_INPUTSR50US_SEC_STARTED
    #error "IOC_INPUTSR50US section not closed"
  #endif
  #define CHK_IOC_INPUTSR50US_SEC_STARTED
  #define CHK_IOC_INPUTSR50US_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTSR50US_STOP_SEC_VAR_NOINIT_32BIT)
  #undef IOC_INPUTSR50US_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_INPUTSR50US_SEC_VAR_NOINIT_32BIT_STARTED
    #error "IOC_INPUTSR50US_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_IOC_INPUTSR50US_SEC_STARTED
  #undef CHK_IOC_INPUTSR50US_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (IOC_INPUTSR50US_START_SEC_CALIB_UNSPECIFIED)
  #undef IOC_INPUTSR50US_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_INPUTSR50US_SEC_STARTED
    #error "IOC_INPUTSR50US section not closed"
  #endif
  #define CHK_IOC_INPUTSR50US_SEC_STARTED
  #define CHK_IOC_INPUTSR50US_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTSR50US_STOP_SEC_CALIB_UNSPECIFIED)
  #undef IOC_INPUTSR50US_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_INPUTSR50US_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "IOC_INPUTSR50US_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_INPUTSR50US_SEC_STARTED
  #undef CHK_IOC_INPUTSR50US_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (IOC_OUTPUTCS50US_START_SEC_CODE)
  #undef IOC_OUTPUTCS50US_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_OUTPUTCS50US_SEC_STARTED
    #error "IOC_OUTPUTCS50US section not closed"
  #endif
  #define CHK_IOC_OUTPUTCS50US_SEC_STARTED
  #define CHK_IOC_OUTPUTCS50US_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTCS50US_STOP_SEC_CODE)
  #undef IOC_OUTPUTCS50US_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_OUTPUTCS50US_SEC_CODE_STARTED
    #error "IOC_OUTPUTCS50US_SEC_CODE not opened"
  #endif
  #undef CHK_IOC_OUTPUTCS50US_SEC_STARTED
  #undef CHK_IOC_OUTPUTCS50US_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (IOC_OUTPUTCS50US_START_SEC_CONST_UNSPECIFIED)
  #undef IOC_OUTPUTCS50US_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_OUTPUTCS50US_SEC_STARTED
    #error "IOC_OUTPUTCS50US section not closed"
  #endif
  #define CHK_IOC_OUTPUTCS50US_SEC_STARTED
  #define CHK_IOC_OUTPUTCS50US_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTCS50US_STOP_SEC_CONST_UNSPECIFIED)
  #undef IOC_OUTPUTCS50US_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_OUTPUTCS50US_SEC_CONST_UNSPECIFIED_STARTED
    #error "IOC_OUTPUTCS50US_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_OUTPUTCS50US_SEC_STARTED
  #undef CHK_IOC_OUTPUTCS50US_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (IOC_OUTPUTCS50US_START_SEC_VAR_UNSPECIFIED)
  #undef IOC_OUTPUTCS50US_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_OUTPUTCS50US_SEC_STARTED
    #error "IOC_OUTPUTCS50US section not closed"
  #endif
  #define CHK_IOC_OUTPUTCS50US_SEC_STARTED
  #define CHK_IOC_OUTPUTCS50US_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTCS50US_STOP_SEC_VAR_UNSPECIFIED)
  #undef IOC_OUTPUTCS50US_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_OUTPUTCS50US_SEC_VAR_UNSPECIFIED_STARTED
    #error "IOC_OUTPUTCS50US_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_OUTPUTCS50US_SEC_STARTED
  #undef CHK_IOC_OUTPUTCS50US_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTCS50US_START_SEC_VAR_32BIT)
  #undef IOC_OUTPUTCS50US_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_OUTPUTCS50US_SEC_STARTED
    #error "IOC_OUTPUTCS50US section not closed"
  #endif
  #define CHK_IOC_OUTPUTCS50US_SEC_STARTED
  #define CHK_IOC_OUTPUTCS50US_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTCS50US_STOP_SEC_VAR_32BIT)
  #undef IOC_OUTPUTCS50US_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_OUTPUTCS50US_SEC_VAR_32BIT_STARTED
    #error "IOC_OUTPUTCS50US_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_IOC_OUTPUTCS50US_SEC_STARTED
  #undef CHK_IOC_OUTPUTCS50US_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTCS50US_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef IOC_OUTPUTCS50US_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_OUTPUTCS50US_SEC_STARTED
    #error "IOC_OUTPUTCS50US section not closed"
  #endif
  #define CHK_IOC_OUTPUTCS50US_SEC_STARTED
  #define CHK_IOC_OUTPUTCS50US_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTCS50US_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef IOC_OUTPUTCS50US_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_OUTPUTCS50US_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "IOC_OUTPUTCS50US_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_OUTPUTCS50US_SEC_STARTED
  #undef CHK_IOC_OUTPUTCS50US_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTCS50US_START_SEC_VAR_NOINIT_32BIT)
  #undef IOC_OUTPUTCS50US_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_OUTPUTCS50US_SEC_STARTED
    #error "IOC_OUTPUTCS50US section not closed"
  #endif
  #define CHK_IOC_OUTPUTCS50US_SEC_STARTED
  #define CHK_IOC_OUTPUTCS50US_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTCS50US_STOP_SEC_VAR_NOINIT_32BIT)
  #undef IOC_OUTPUTCS50US_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_OUTPUTCS50US_SEC_VAR_NOINIT_32BIT_STARTED
    #error "IOC_OUTPUTCS50US_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_IOC_OUTPUTCS50US_SEC_STARTED
  #undef CHK_IOC_OUTPUTCS50US_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (IOC_OUTPUTCS50US_START_SEC_CALIB_UNSPECIFIED)
  #undef IOC_OUTPUTCS50US_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_OUTPUTCS50US_SEC_STARTED
    #error "IOC_OUTPUTCS50US section not closed"
  #endif
  #define CHK_IOC_OUTPUTCS50US_SEC_STARTED
  #define CHK_IOC_OUTPUTCS50US_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTCS50US_STOP_SEC_CALIB_UNSPECIFIED)
  #undef IOC_OUTPUTCS50US_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_OUTPUTCS50US_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "IOC_OUTPUTCS50US_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_OUTPUTCS50US_SEC_STARTED
  #undef CHK_IOC_OUTPUTCS50US_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (IOC_INPUTSR1MS_START_SEC_CODE)
  #undef IOC_INPUTSR1MS_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_INPUTSR1MS_SEC_STARTED
    #error "IOC_INPUTSR1MS section not closed"
  #endif
  #define CHK_IOC_INPUTSR1MS_SEC_STARTED
  #define CHK_IOC_INPUTSR1MS_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTSR1MS_STOP_SEC_CODE)
  #undef IOC_INPUTSR1MS_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_INPUTSR1MS_SEC_CODE_STARTED
    #error "IOC_INPUTSR1MS_SEC_CODE not opened"
  #endif
  #undef CHK_IOC_INPUTSR1MS_SEC_STARTED
  #undef CHK_IOC_INPUTSR1MS_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (IOC_INPUTSR1MS_START_SEC_CONST_UNSPECIFIED)
  #undef IOC_INPUTSR1MS_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_INPUTSR1MS_SEC_STARTED
    #error "IOC_INPUTSR1MS section not closed"
  #endif
  #define CHK_IOC_INPUTSR1MS_SEC_STARTED
  #define CHK_IOC_INPUTSR1MS_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTSR1MS_STOP_SEC_CONST_UNSPECIFIED)
  #undef IOC_INPUTSR1MS_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_INPUTSR1MS_SEC_CONST_UNSPECIFIED_STARTED
    #error "IOC_INPUTSR1MS_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_INPUTSR1MS_SEC_STARTED
  #undef CHK_IOC_INPUTSR1MS_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (IOC_INPUTSR1MS_START_SEC_VAR_UNSPECIFIED)
  #undef IOC_INPUTSR1MS_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_INPUTSR1MS_SEC_STARTED
    #error "IOC_INPUTSR1MS section not closed"
  #endif
  #define CHK_IOC_INPUTSR1MS_SEC_STARTED
  #define CHK_IOC_INPUTSR1MS_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTSR1MS_STOP_SEC_VAR_UNSPECIFIED)
  #undef IOC_INPUTSR1MS_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_INPUTSR1MS_SEC_VAR_UNSPECIFIED_STARTED
    #error "IOC_INPUTSR1MS_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_INPUTSR1MS_SEC_STARTED
  #undef CHK_IOC_INPUTSR1MS_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTSR1MS_START_SEC_VAR_32BIT)
  #undef IOC_INPUTSR1MS_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_INPUTSR1MS_SEC_STARTED
    #error "IOC_INPUTSR1MS section not closed"
  #endif
  #define CHK_IOC_INPUTSR1MS_SEC_STARTED
  #define CHK_IOC_INPUTSR1MS_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTSR1MS_STOP_SEC_VAR_32BIT)
  #undef IOC_INPUTSR1MS_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_INPUTSR1MS_SEC_VAR_32BIT_STARTED
    #error "IOC_INPUTSR1MS_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_IOC_INPUTSR1MS_SEC_STARTED
  #undef CHK_IOC_INPUTSR1MS_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTSR1MS_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef IOC_INPUTSR1MS_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_INPUTSR1MS_SEC_STARTED
    #error "IOC_INPUTSR1MS section not closed"
  #endif
  #define CHK_IOC_INPUTSR1MS_SEC_STARTED
  #define CHK_IOC_INPUTSR1MS_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTSR1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef IOC_INPUTSR1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_INPUTSR1MS_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "IOC_INPUTSR1MS_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_INPUTSR1MS_SEC_STARTED
  #undef CHK_IOC_INPUTSR1MS_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTSR1MS_START_SEC_VAR_NOINIT_32BIT)
  #undef IOC_INPUTSR1MS_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_INPUTSR1MS_SEC_STARTED
    #error "IOC_INPUTSR1MS section not closed"
  #endif
  #define CHK_IOC_INPUTSR1MS_SEC_STARTED
  #define CHK_IOC_INPUTSR1MS_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTSR1MS_STOP_SEC_VAR_NOINIT_32BIT)
  #undef IOC_INPUTSR1MS_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_INPUTSR1MS_SEC_VAR_NOINIT_32BIT_STARTED
    #error "IOC_INPUTSR1MS_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_IOC_INPUTSR1MS_SEC_STARTED
  #undef CHK_IOC_INPUTSR1MS_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (IOC_INPUTSR1MS_START_SEC_CALIB_UNSPECIFIED)
  #undef IOC_INPUTSR1MS_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_INPUTSR1MS_SEC_STARTED
    #error "IOC_INPUTSR1MS section not closed"
  #endif
  #define CHK_IOC_INPUTSR1MS_SEC_STARTED
  #define CHK_IOC_INPUTSR1MS_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTSR1MS_STOP_SEC_CALIB_UNSPECIFIED)
  #undef IOC_INPUTSR1MS_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_INPUTSR1MS_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "IOC_INPUTSR1MS_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_INPUTSR1MS_SEC_STARTED
  #undef CHK_IOC_INPUTSR1MS_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (IOC_INPUTCS1MS_START_SEC_CODE)
  #undef IOC_INPUTCS1MS_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_INPUTCS1MS_SEC_STARTED
    #error "IOC_INPUTCS1MS section not closed"
  #endif
  #define CHK_IOC_INPUTCS1MS_SEC_STARTED
  #define CHK_IOC_INPUTCS1MS_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTCS1MS_STOP_SEC_CODE)
  #undef IOC_INPUTCS1MS_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_INPUTCS1MS_SEC_CODE_STARTED
    #error "IOC_INPUTCS1MS_SEC_CODE not opened"
  #endif
  #undef CHK_IOC_INPUTCS1MS_SEC_STARTED
  #undef CHK_IOC_INPUTCS1MS_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (IOC_INPUTCS1MS_START_SEC_CONST_UNSPECIFIED)
  #undef IOC_INPUTCS1MS_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_INPUTCS1MS_SEC_STARTED
    #error "IOC_INPUTCS1MS section not closed"
  #endif
  #define CHK_IOC_INPUTCS1MS_SEC_STARTED
  #define CHK_IOC_INPUTCS1MS_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTCS1MS_STOP_SEC_CONST_UNSPECIFIED)
  #undef IOC_INPUTCS1MS_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_INPUTCS1MS_SEC_CONST_UNSPECIFIED_STARTED
    #error "IOC_INPUTCS1MS_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_INPUTCS1MS_SEC_STARTED
  #undef CHK_IOC_INPUTCS1MS_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (IOC_INPUTCS1MS_START_SEC_VAR_UNSPECIFIED)
  #undef IOC_INPUTCS1MS_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_INPUTCS1MS_SEC_STARTED
    #error "IOC_INPUTCS1MS section not closed"
  #endif
  #define CHK_IOC_INPUTCS1MS_SEC_STARTED
  #define CHK_IOC_INPUTCS1MS_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTCS1MS_STOP_SEC_VAR_UNSPECIFIED)
  #undef IOC_INPUTCS1MS_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_INPUTCS1MS_SEC_VAR_UNSPECIFIED_STARTED
    #error "IOC_INPUTCS1MS_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_INPUTCS1MS_SEC_STARTED
  #undef CHK_IOC_INPUTCS1MS_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTCS1MS_START_SEC_VAR_32BIT)
  #undef IOC_INPUTCS1MS_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_INPUTCS1MS_SEC_STARTED
    #error "IOC_INPUTCS1MS section not closed"
  #endif
  #define CHK_IOC_INPUTCS1MS_SEC_STARTED
  #define CHK_IOC_INPUTCS1MS_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTCS1MS_STOP_SEC_VAR_32BIT)
  #undef IOC_INPUTCS1MS_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_INPUTCS1MS_SEC_VAR_32BIT_STARTED
    #error "IOC_INPUTCS1MS_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_IOC_INPUTCS1MS_SEC_STARTED
  #undef CHK_IOC_INPUTCS1MS_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTCS1MS_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef IOC_INPUTCS1MS_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_INPUTCS1MS_SEC_STARTED
    #error "IOC_INPUTCS1MS section not closed"
  #endif
  #define CHK_IOC_INPUTCS1MS_SEC_STARTED
  #define CHK_IOC_INPUTCS1MS_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTCS1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef IOC_INPUTCS1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_INPUTCS1MS_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "IOC_INPUTCS1MS_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_INPUTCS1MS_SEC_STARTED
  #undef CHK_IOC_INPUTCS1MS_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTCS1MS_START_SEC_VAR_NOINIT_32BIT)
  #undef IOC_INPUTCS1MS_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_INPUTCS1MS_SEC_STARTED
    #error "IOC_INPUTCS1MS section not closed"
  #endif
  #define CHK_IOC_INPUTCS1MS_SEC_STARTED
  #define CHK_IOC_INPUTCS1MS_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTCS1MS_STOP_SEC_VAR_NOINIT_32BIT)
  #undef IOC_INPUTCS1MS_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_INPUTCS1MS_SEC_VAR_NOINIT_32BIT_STARTED
    #error "IOC_INPUTCS1MS_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_IOC_INPUTCS1MS_SEC_STARTED
  #undef CHK_IOC_INPUTCS1MS_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (IOC_INPUTCS1MS_START_SEC_CALIB_UNSPECIFIED)
  #undef IOC_INPUTCS1MS_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_INPUTCS1MS_SEC_STARTED
    #error "IOC_INPUTCS1MS section not closed"
  #endif
  #define CHK_IOC_INPUTCS1MS_SEC_STARTED
  #define CHK_IOC_INPUTCS1MS_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTCS1MS_STOP_SEC_CALIB_UNSPECIFIED)
  #undef IOC_INPUTCS1MS_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_INPUTCS1MS_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "IOC_INPUTCS1MS_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_INPUTCS1MS_SEC_STARTED
  #undef CHK_IOC_INPUTCS1MS_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (IOC_OUTPUTSR1MS_START_SEC_CODE)
  #undef IOC_OUTPUTSR1MS_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_OUTPUTSR1MS_SEC_STARTED
    #error "IOC_OUTPUTSR1MS section not closed"
  #endif
  #define CHK_IOC_OUTPUTSR1MS_SEC_STARTED
  #define CHK_IOC_OUTPUTSR1MS_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTSR1MS_STOP_SEC_CODE)
  #undef IOC_OUTPUTSR1MS_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_OUTPUTSR1MS_SEC_CODE_STARTED
    #error "IOC_OUTPUTSR1MS_SEC_CODE not opened"
  #endif
  #undef CHK_IOC_OUTPUTSR1MS_SEC_STARTED
  #undef CHK_IOC_OUTPUTSR1MS_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (IOC_OUTPUTSR1MS_START_SEC_CONST_UNSPECIFIED)
  #undef IOC_OUTPUTSR1MS_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_OUTPUTSR1MS_SEC_STARTED
    #error "IOC_OUTPUTSR1MS section not closed"
  #endif
  #define CHK_IOC_OUTPUTSR1MS_SEC_STARTED
  #define CHK_IOC_OUTPUTSR1MS_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTSR1MS_STOP_SEC_CONST_UNSPECIFIED)
  #undef IOC_OUTPUTSR1MS_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_OUTPUTSR1MS_SEC_CONST_UNSPECIFIED_STARTED
    #error "IOC_OUTPUTSR1MS_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_OUTPUTSR1MS_SEC_STARTED
  #undef CHK_IOC_OUTPUTSR1MS_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (IOC_OUTPUTSR1MS_START_SEC_VAR_UNSPECIFIED)
  #undef IOC_OUTPUTSR1MS_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_OUTPUTSR1MS_SEC_STARTED
    #error "IOC_OUTPUTSR1MS section not closed"
  #endif
  #define CHK_IOC_OUTPUTSR1MS_SEC_STARTED
  #define CHK_IOC_OUTPUTSR1MS_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTSR1MS_STOP_SEC_VAR_UNSPECIFIED)
  #undef IOC_OUTPUTSR1MS_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_OUTPUTSR1MS_SEC_VAR_UNSPECIFIED_STARTED
    #error "IOC_OUTPUTSR1MS_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_OUTPUTSR1MS_SEC_STARTED
  #undef CHK_IOC_OUTPUTSR1MS_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTSR1MS_START_SEC_VAR_32BIT)
  #undef IOC_OUTPUTSR1MS_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_OUTPUTSR1MS_SEC_STARTED
    #error "IOC_OUTPUTSR1MS section not closed"
  #endif
  #define CHK_IOC_OUTPUTSR1MS_SEC_STARTED
  #define CHK_IOC_OUTPUTSR1MS_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTSR1MS_STOP_SEC_VAR_32BIT)
  #undef IOC_OUTPUTSR1MS_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_OUTPUTSR1MS_SEC_VAR_32BIT_STARTED
    #error "IOC_OUTPUTSR1MS_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_IOC_OUTPUTSR1MS_SEC_STARTED
  #undef CHK_IOC_OUTPUTSR1MS_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTSR1MS_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef IOC_OUTPUTSR1MS_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_OUTPUTSR1MS_SEC_STARTED
    #error "IOC_OUTPUTSR1MS section not closed"
  #endif
  #define CHK_IOC_OUTPUTSR1MS_SEC_STARTED
  #define CHK_IOC_OUTPUTSR1MS_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTSR1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef IOC_OUTPUTSR1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_OUTPUTSR1MS_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "IOC_OUTPUTSR1MS_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_OUTPUTSR1MS_SEC_STARTED
  #undef CHK_IOC_OUTPUTSR1MS_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTSR1MS_START_SEC_VAR_NOINIT_32BIT)
  #undef IOC_OUTPUTSR1MS_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_OUTPUTSR1MS_SEC_STARTED
    #error "IOC_OUTPUTSR1MS section not closed"
  #endif
  #define CHK_IOC_OUTPUTSR1MS_SEC_STARTED
  #define CHK_IOC_OUTPUTSR1MS_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTSR1MS_STOP_SEC_VAR_NOINIT_32BIT)
  #undef IOC_OUTPUTSR1MS_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_OUTPUTSR1MS_SEC_VAR_NOINIT_32BIT_STARTED
    #error "IOC_OUTPUTSR1MS_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_IOC_OUTPUTSR1MS_SEC_STARTED
  #undef CHK_IOC_OUTPUTSR1MS_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (IOC_OUTPUTSR1MS_START_SEC_CALIB_UNSPECIFIED)
  #undef IOC_OUTPUTSR1MS_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_OUTPUTSR1MS_SEC_STARTED
    #error "IOC_OUTPUTSR1MS section not closed"
  #endif
  #define CHK_IOC_OUTPUTSR1MS_SEC_STARTED
  #define CHK_IOC_OUTPUTSR1MS_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTSR1MS_STOP_SEC_CALIB_UNSPECIFIED)
  #undef IOC_OUTPUTSR1MS_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_OUTPUTSR1MS_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "IOC_OUTPUTSR1MS_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_OUTPUTSR1MS_SEC_STARTED
  #undef CHK_IOC_OUTPUTSR1MS_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (IOC_OUTPUTCS1MS_START_SEC_CODE)
  #undef IOC_OUTPUTCS1MS_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_OUTPUTCS1MS_SEC_STARTED
    #error "IOC_OUTPUTCS1MS section not closed"
  #endif
  #define CHK_IOC_OUTPUTCS1MS_SEC_STARTED
  #define CHK_IOC_OUTPUTCS1MS_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTCS1MS_STOP_SEC_CODE)
  #undef IOC_OUTPUTCS1MS_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_OUTPUTCS1MS_SEC_CODE_STARTED
    #error "IOC_OUTPUTCS1MS_SEC_CODE not opened"
  #endif
  #undef CHK_IOC_OUTPUTCS1MS_SEC_STARTED
  #undef CHK_IOC_OUTPUTCS1MS_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (IOC_OUTPUTCS1MS_START_SEC_CONST_UNSPECIFIED)
  #undef IOC_OUTPUTCS1MS_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_OUTPUTCS1MS_SEC_STARTED
    #error "IOC_OUTPUTCS1MS section not closed"
  #endif
  #define CHK_IOC_OUTPUTCS1MS_SEC_STARTED
  #define CHK_IOC_OUTPUTCS1MS_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTCS1MS_STOP_SEC_CONST_UNSPECIFIED)
  #undef IOC_OUTPUTCS1MS_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_OUTPUTCS1MS_SEC_CONST_UNSPECIFIED_STARTED
    #error "IOC_OUTPUTCS1MS_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_OUTPUTCS1MS_SEC_STARTED
  #undef CHK_IOC_OUTPUTCS1MS_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (IOC_OUTPUTCS1MS_START_SEC_VAR_UNSPECIFIED)
  #undef IOC_OUTPUTCS1MS_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_OUTPUTCS1MS_SEC_STARTED
    #error "IOC_OUTPUTCS1MS section not closed"
  #endif
  #define CHK_IOC_OUTPUTCS1MS_SEC_STARTED
  #define CHK_IOC_OUTPUTCS1MS_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTCS1MS_STOP_SEC_VAR_UNSPECIFIED)
  #undef IOC_OUTPUTCS1MS_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_OUTPUTCS1MS_SEC_VAR_UNSPECIFIED_STARTED
    #error "IOC_OUTPUTCS1MS_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_OUTPUTCS1MS_SEC_STARTED
  #undef CHK_IOC_OUTPUTCS1MS_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTCS1MS_START_SEC_VAR_32BIT)
  #undef IOC_OUTPUTCS1MS_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_OUTPUTCS1MS_SEC_STARTED
    #error "IOC_OUTPUTCS1MS section not closed"
  #endif
  #define CHK_IOC_OUTPUTCS1MS_SEC_STARTED
  #define CHK_IOC_OUTPUTCS1MS_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTCS1MS_STOP_SEC_VAR_32BIT)
  #undef IOC_OUTPUTCS1MS_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_OUTPUTCS1MS_SEC_VAR_32BIT_STARTED
    #error "IOC_OUTPUTCS1MS_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_IOC_OUTPUTCS1MS_SEC_STARTED
  #undef CHK_IOC_OUTPUTCS1MS_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTCS1MS_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef IOC_OUTPUTCS1MS_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_OUTPUTCS1MS_SEC_STARTED
    #error "IOC_OUTPUTCS1MS section not closed"
  #endif
  #define CHK_IOC_OUTPUTCS1MS_SEC_STARTED
  #define CHK_IOC_OUTPUTCS1MS_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTCS1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef IOC_OUTPUTCS1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_OUTPUTCS1MS_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "IOC_OUTPUTCS1MS_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_OUTPUTCS1MS_SEC_STARTED
  #undef CHK_IOC_OUTPUTCS1MS_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTCS1MS_START_SEC_VAR_NOINIT_32BIT)
  #undef IOC_OUTPUTCS1MS_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_OUTPUTCS1MS_SEC_STARTED
    #error "IOC_OUTPUTCS1MS section not closed"
  #endif
  #define CHK_IOC_OUTPUTCS1MS_SEC_STARTED
  #define CHK_IOC_OUTPUTCS1MS_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTCS1MS_STOP_SEC_VAR_NOINIT_32BIT)
  #undef IOC_OUTPUTCS1MS_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_OUTPUTCS1MS_SEC_VAR_NOINIT_32BIT_STARTED
    #error "IOC_OUTPUTCS1MS_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_IOC_OUTPUTCS1MS_SEC_STARTED
  #undef CHK_IOC_OUTPUTCS1MS_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (IOC_OUTPUTCS1MS_START_SEC_CALIB_UNSPECIFIED)
  #undef IOC_OUTPUTCS1MS_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_OUTPUTCS1MS_SEC_STARTED
    #error "IOC_OUTPUTCS1MS section not closed"
  #endif
  #define CHK_IOC_OUTPUTCS1MS_SEC_STARTED
  #define CHK_IOC_OUTPUTCS1MS_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTCS1MS_STOP_SEC_CALIB_UNSPECIFIED)
  #undef IOC_OUTPUTCS1MS_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_OUTPUTCS1MS_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "IOC_OUTPUTCS1MS_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_OUTPUTCS1MS_SEC_STARTED
  #undef CHK_IOC_OUTPUTCS1MS_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (IOC_INPUTSR5MS_START_SEC_CODE)
  #undef IOC_INPUTSR5MS_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_INPUTSR5MS_SEC_STARTED
    #error "IOC_INPUTSR5MS section not closed"
  #endif
  #define CHK_IOC_INPUTSR5MS_SEC_STARTED
  #define CHK_IOC_INPUTSR5MS_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTSR5MS_STOP_SEC_CODE)
  #undef IOC_INPUTSR5MS_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_INPUTSR5MS_SEC_CODE_STARTED
    #error "IOC_INPUTSR5MS_SEC_CODE not opened"
  #endif
  #undef CHK_IOC_INPUTSR5MS_SEC_STARTED
  #undef CHK_IOC_INPUTSR5MS_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (IOC_INPUTSR5MS_START_SEC_CONST_UNSPECIFIED)
  #undef IOC_INPUTSR5MS_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_INPUTSR5MS_SEC_STARTED
    #error "IOC_INPUTSR5MS section not closed"
  #endif
  #define CHK_IOC_INPUTSR5MS_SEC_STARTED
  #define CHK_IOC_INPUTSR5MS_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTSR5MS_STOP_SEC_CONST_UNSPECIFIED)
  #undef IOC_INPUTSR5MS_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_INPUTSR5MS_SEC_CONST_UNSPECIFIED_STARTED
    #error "IOC_INPUTSR5MS_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_INPUTSR5MS_SEC_STARTED
  #undef CHK_IOC_INPUTSR5MS_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (IOC_INPUTSR5MS_START_SEC_VAR_UNSPECIFIED)
  #undef IOC_INPUTSR5MS_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_INPUTSR5MS_SEC_STARTED
    #error "IOC_INPUTSR5MS section not closed"
  #endif
  #define CHK_IOC_INPUTSR5MS_SEC_STARTED
  #define CHK_IOC_INPUTSR5MS_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTSR5MS_STOP_SEC_VAR_UNSPECIFIED)
  #undef IOC_INPUTSR5MS_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_INPUTSR5MS_SEC_VAR_UNSPECIFIED_STARTED
    #error "IOC_INPUTSR5MS_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_INPUTSR5MS_SEC_STARTED
  #undef CHK_IOC_INPUTSR5MS_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTSR5MS_START_SEC_VAR_32BIT)
  #undef IOC_INPUTSR5MS_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_INPUTSR5MS_SEC_STARTED
    #error "IOC_INPUTSR5MS section not closed"
  #endif
  #define CHK_IOC_INPUTSR5MS_SEC_STARTED
  #define CHK_IOC_INPUTSR5MS_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTSR5MS_STOP_SEC_VAR_32BIT)
  #undef IOC_INPUTSR5MS_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_INPUTSR5MS_SEC_VAR_32BIT_STARTED
    #error "IOC_INPUTSR5MS_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_IOC_INPUTSR5MS_SEC_STARTED
  #undef CHK_IOC_INPUTSR5MS_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTSR5MS_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef IOC_INPUTSR5MS_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_INPUTSR5MS_SEC_STARTED
    #error "IOC_INPUTSR5MS section not closed"
  #endif
  #define CHK_IOC_INPUTSR5MS_SEC_STARTED
  #define CHK_IOC_INPUTSR5MS_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTSR5MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef IOC_INPUTSR5MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_INPUTSR5MS_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "IOC_INPUTSR5MS_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_INPUTSR5MS_SEC_STARTED
  #undef CHK_IOC_INPUTSR5MS_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTSR5MS_START_SEC_VAR_NOINIT_32BIT)
  #undef IOC_INPUTSR5MS_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_INPUTSR5MS_SEC_STARTED
    #error "IOC_INPUTSR5MS section not closed"
  #endif
  #define CHK_IOC_INPUTSR5MS_SEC_STARTED
  #define CHK_IOC_INPUTSR5MS_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTSR5MS_STOP_SEC_VAR_NOINIT_32BIT)
  #undef IOC_INPUTSR5MS_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_INPUTSR5MS_SEC_VAR_NOINIT_32BIT_STARTED
    #error "IOC_INPUTSR5MS_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_IOC_INPUTSR5MS_SEC_STARTED
  #undef CHK_IOC_INPUTSR5MS_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (IOC_INPUTSR5MS_START_SEC_CALIB_UNSPECIFIED)
  #undef IOC_INPUTSR5MS_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_INPUTSR5MS_SEC_STARTED
    #error "IOC_INPUTSR5MS section not closed"
  #endif
  #define CHK_IOC_INPUTSR5MS_SEC_STARTED
  #define CHK_IOC_INPUTSR5MS_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_INPUTSR5MS_STOP_SEC_CALIB_UNSPECIFIED)
  #undef IOC_INPUTSR5MS_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_INPUTSR5MS_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "IOC_INPUTSR5MS_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_INPUTSR5MS_SEC_STARTED
  #undef CHK_IOC_INPUTSR5MS_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Code Sections
--------------------------------------------*/
#elif defined (IOC_OUTPUTCS5MS_START_SEC_CODE)
  #undef IOC_OUTPUTCS5MS_START_SEC_CODE
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_OUTPUTCS5MS_SEC_STARTED
    #error "IOC_OUTPUTCS5MS section not closed"
  #endif
  #define CHK_IOC_OUTPUTCS5MS_SEC_STARTED
  #define CHK_IOC_OUTPUTCS5MS_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTCS5MS_STOP_SEC_CODE)
  #undef IOC_OUTPUTCS5MS_STOP_SEC_CODE
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_OUTPUTCS5MS_SEC_CODE_STARTED
    #error "IOC_OUTPUTCS5MS_SEC_CODE not opened"
  #endif
  #undef CHK_IOC_OUTPUTCS5MS_SEC_STARTED
  #undef CHK_IOC_OUTPUTCS5MS_SEC_CODE_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Constant Sections
--------------------------------------------*/
#elif defined (IOC_OUTPUTCS5MS_START_SEC_CONST_UNSPECIFIED)
  #undef IOC_OUTPUTCS5MS_START_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_OUTPUTCS5MS_SEC_STARTED
    #error "IOC_OUTPUTCS5MS section not closed"
  #endif
  #define CHK_IOC_OUTPUTCS5MS_SEC_STARTED
  #define CHK_IOC_OUTPUTCS5MS_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTCS5MS_STOP_SEC_CONST_UNSPECIFIED)
  #undef IOC_OUTPUTCS5MS_STOP_SEC_CONST_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_OUTPUTCS5MS_SEC_CONST_UNSPECIFIED_STARTED
    #error "IOC_OUTPUTCS5MS_SEC_CONST_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_OUTPUTCS5MS_SEC_STARTED
  #undef CHK_IOC_OUTPUTCS5MS_SEC_CONST_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Variable Sections
--------------------------------------------*/
#elif defined (IOC_OUTPUTCS5MS_START_SEC_VAR_UNSPECIFIED)
  #undef IOC_OUTPUTCS5MS_START_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_OUTPUTCS5MS_SEC_STARTED
    #error "IOC_OUTPUTCS5MS section not closed"
  #endif
  #define CHK_IOC_OUTPUTCS5MS_SEC_STARTED
  #define CHK_IOC_OUTPUTCS5MS_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTCS5MS_STOP_SEC_VAR_UNSPECIFIED)
  #undef IOC_OUTPUTCS5MS_STOP_SEC_VAR_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_OUTPUTCS5MS_SEC_VAR_UNSPECIFIED_STARTED
    #error "IOC_OUTPUTCS5MS_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_OUTPUTCS5MS_SEC_STARTED
  #undef CHK_IOC_OUTPUTCS5MS_SEC_VAR_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTCS5MS_START_SEC_VAR_32BIT)
  #undef IOC_OUTPUTCS5MS_START_SEC_VAR_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_OUTPUTCS5MS_SEC_STARTED
    #error "IOC_OUTPUTCS5MS section not closed"
  #endif
  #define CHK_IOC_OUTPUTCS5MS_SEC_STARTED
  #define CHK_IOC_OUTPUTCS5MS_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTCS5MS_STOP_SEC_VAR_32BIT)
  #undef IOC_OUTPUTCS5MS_STOP_SEC_VAR_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_OUTPUTCS5MS_SEC_VAR_32BIT_STARTED
    #error "IOC_OUTPUTCS5MS_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_IOC_OUTPUTCS5MS_SEC_STARTED
  #undef CHK_IOC_OUTPUTCS5MS_SEC_VAR_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTCS5MS_START_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef IOC_OUTPUTCS5MS_START_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_OUTPUTCS5MS_SEC_STARTED
    #error "IOC_OUTPUTCS5MS section not closed"
  #endif
  #define CHK_IOC_OUTPUTCS5MS_SEC_STARTED
  #define CHK_IOC_OUTPUTCS5MS_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTCS5MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
  #undef IOC_OUTPUTCS5MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_OUTPUTCS5MS_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
    #error "IOC_OUTPUTCS5MS_SEC_VAR_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_OUTPUTCS5MS_SEC_STARTED
  #undef CHK_IOC_OUTPUTCS5MS_SEC_VAR_NOINIT_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTCS5MS_START_SEC_VAR_NOINIT_32BIT)
  #undef IOC_OUTPUTCS5MS_START_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_OUTPUTCS5MS_SEC_STARTED
    #error "IOC_OUTPUTCS5MS section not closed"
  #endif
  #define CHK_IOC_OUTPUTCS5MS_SEC_STARTED
  #define CHK_IOC_OUTPUTCS5MS_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTCS5MS_STOP_SEC_VAR_NOINIT_32BIT)
  #undef IOC_OUTPUTCS5MS_STOP_SEC_VAR_NOINIT_32BIT
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_OUTPUTCS5MS_SEC_VAR_NOINIT_32BIT_STARTED
    #error "IOC_OUTPUTCS5MS_SEC_VAR_32BIT not opened"
  #endif
  #undef CHK_IOC_OUTPUTCS5MS_SEC_STARTED
  #undef CHK_IOC_OUTPUTCS5MS_SEC_VAR_NOINIT_32BIT_STARTED
  #undef MEMMAP_ERROR

/*-------------------------------------------
   Calibration Sections
--------------------------------------------*/
#elif defined (IOC_OUTPUTCS5MS_START_SEC_CALIB_UNSPECIFIED)
  #undef IOC_OUTPUTCS5MS_START_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation here */
  #ifdef CHK_IOC_OUTPUTCS5MS_SEC_STARTED
    #error "IOC_OUTPUTCS5MS section not closed"
  #endif
  #define CHK_IOC_OUTPUTCS5MS_SEC_STARTED
  #define CHK_IOC_OUTPUTCS5MS_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR

#elif defined (IOC_OUTPUTCS5MS_STOP_SEC_CALIB_UNSPECIFIED)
  #undef IOC_OUTPUTCS5MS_STOP_SEC_CALIB_UNSPECIFIED
  /* add pragma for memory allocation to default section here */
  #ifndef CHK_IOC_OUTPUTCS5MS_SEC_CALIB_UNSPECIFIED_STARTED 
    #error "IOC_OUTPUTCS5MS_SEC_CALIB_UNSPECIFIED not opened"
  #endif
  #undef CHK_IOC_OUTPUTCS5MS_SEC_STARTED
  #undef CHK_IOC_OUTPUTCS5MS_SEC_CALIB_UNSPECIFIED_STARTED
  #undef MEMMAP_ERROR


#endif


/*------------------[MemMap error checking]----------------------------------*/

#if (defined MEMMAP_ERROR) /* to prevent double definition */
#error MEMMAP_ERROR defined, wrong MemMap.h usage
#endif /* if (!defined MEMMAP_ERROR) */
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* IOC_MEMMAP_H_ */
/** @} */
