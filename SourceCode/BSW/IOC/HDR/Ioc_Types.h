/**
 * @defgroup Ioc_Types Ioc_Types
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_Types.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef IOC_TYPES_H_
#define IOC_TYPES_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Bsw_Types.h"
#include "Hal_Types.h"
#include "HalDataHandle_Types.h"
#include "Sal_Types.h"
#include "SalDataHandle_Types.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/
/* IO HW Port ID type */
 typedef uint16 Ioc_PortType;

 /* Requesting buffer type */
 typedef uint16 Ioc_DataType;

 /* Requesting buffer length type */
 typedef uint8 Ioc_BufLenType;

 /* IO channel type */
 typedef uint16 Ioc_IoChannelType;

 /* Conversion multiply factor type */
 typedef uint32 Ioc_MulFactorType;
 
 /* Conversion divide factor type */
 typedef uint32 Ioc_DivFactorType;

 /* Digital Input Level type */
 typedef uint8  Ioc_LevelType;

 /* Edge Index Type */
 typedef uint16  Ioc_EdgeIdxType;

  /* Pulse Index Type */
 typedef uint32  Ioc_TimeStampArrayType;

 /* IO HW Module ID type */
 typedef enum
{
    ModuleID_Dio = 0,
    ModuleID_Spi,
    ModuleID_Pwm,
    ModuleID_Adc,
    ModuleID_IcuCdd
 }Ioc_ModuleIDType;

 typedef enum
 {
    DataIfType_Single = 0,
    DataIfType_Array
 }Ioc_DataIfType;

 /* Routing configuration table type */
typedef struct
 {
    Ioc_ModuleIDType   ModuleID;
    Ioc_MulFactorType  MulFactor;
    Ioc_DivFactorType  DivFactor;
 }Ioc_RoutingType;

/* Valve Channel */
typedef enum
{
    IOC_VAVLE_NO_CH0 = 0u,
    IOC_VAVLE_NO_CH1,
    IOC_VAVLE_NO_CH2,
    IOC_VAVLE_NO_CH3,
    IOC_VAVLE_TC_CH0,
    IOC_VAVLE_TC_CH1,
    IOC_VAVLE_ESV_CH0,
    IOC_VAVLE_ESV_CH1,
    IOC_VAVLE_NC_CH0,
    IOC_VAVLE_NC_CH1,
    IOC_VAVLE_NC_CH2,
    IOC_VAVLE_NC_CH3,

    IOC_VALVE_MAX_NUM
}Ioc_ValveChannelType;

typedef enum
{
    IOC_MOTOR_ABS = 0u,
    IOC_MOTOR_MAX_NUM
}Ioc_MotorChannelType;

typedef struct
{
/* Input Data Element */
    Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo_t Ioc_InputSR50usMpsD1IIFAngInfo;
    Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo_t Ioc_InputSR50usMpsD2IIFAngInfo;
    AdcIf_Conv50usHwTrigMotInfo_t Ioc_InputSR50usHwTrigMotInfo;
    Mom_HndlrEcuModeSts_t Ioc_InputSR50usEcuModeSts;
    Eem_SuspcDetnFuncInhibitIocSts_t Ioc_InputSR50usFuncInhibitIocSts;

/* Output Data Element */
    Ioc_InputSR50usMotCurrMonInfo_t Ioc_InputSR50usMotCurrMonInfo;
    Ioc_InputSR50usMotAngleMonInfo_t Ioc_InputSR50usMotAngleMonInfo;
}Ioc_InputSR50us_HdrBusType;

typedef struct
{
/* Input Data Element */
    Acmio_ActrMotPwmDataInfo_t Ioc_OutputCS50usMotPwmDataInfo;
    Mom_HndlrEcuModeSts_t Ioc_OutputCS50usEcuModeSts;
    Eem_SuspcDetnFuncInhibitIocSts_t Ioc_OutputCS50usFuncInhibitIocSts;

/* Output Data Element */
}Ioc_OutputCS50us_HdrBusType;

typedef struct
{
/* Input Data Element */
    Ach_InputAchAdcData_t Ioc_InputSR1msAchAdcData;
    AdcIf_Conv1msSwTrigPwrInfo_t Ioc_InputSR1msSwTrigPwrInfo;
    AdcIf_Conv1msSwTrigFspInfo_t Ioc_InputSR1msSwTrigFspInfo;
    AdcIf_Conv1msSwTrigTmpInfo_t Ioc_InputSR1msSwTrigTmpInfo;
    AdcIf_Conv1msSwTrigMotInfo_t Ioc_InputSR1msSwTrigMotInfo;
    AdcIf_Conv1msSwTrigPdtInfo_t Ioc_InputSR1msSwTrigPdtInfo;
    AdcIf_Conv1msSwTrigMocInfo_t Ioc_InputSR1msSwTrigMocInfo;
    AdcIf_Conv1msHwTrigVlvInfo_t Ioc_InputSR1msHwTrigVlvInfo;
    AdcIf_Conv1msSwTrigEscInfo_t Ioc_InputSR1msSwTrigEscInfo;
    SentH_MainSentHPressureInfo_t Ioc_InputSR1msSentHPressureInfo;
    Mom_HndlrEcuModeSts_t Ioc_InputSR1msEcuModeSts;
    Acm_MainAcmAsicInitCompleteFlag_t Ioc_InputSR1msAcmAsicInitCompleteFlag;
    AdcIf_Conv1msAdcInvalid_t Ioc_InputSR1msAdcInvalid;
    Eem_SuspcDetnFuncInhibitIocSts_t Ioc_InputSR1msFuncInhibitIocSts;

/* Output Data Element */
    Ioc_InputSR1msPwrMonInfoEsc_t Ioc_InputSR1msPwrMonInfoEsc;
    Ioc_InputSR1msVlvMonInfoEsc_t Ioc_InputSR1msVlvMonInfoEsc;
    Ioc_InputSR1msMotMonInfoEsc_t Ioc_InputSR1msMotMonInfoEsc;
    Ioc_InputSR1msEpbMonInfoEsc_t Ioc_InputSR1msEpbMonInfoEsc;
    Ioc_InputSR1msReservedMonInfoEsc_t Ioc_InputSR1msReservedMonInfoEsc;
    Ioc_InputSR1msHalPressureInfo_t Ioc_InputSR1msHalPressureInfo;
    Ioc_InputSR1msMotMonInfo_t Ioc_InputSR1msMotMonInfo;
    Ioc_InputSR1msMotVoltsMonInfo_t Ioc_InputSR1msMotVoltsMonInfo;
    Ioc_InputSR1msPedlSigMonInfo_t Ioc_InputSR1msPedlSigMonInfo;
    Ioc_InputSR1msPedlPwrMonInfo_t Ioc_InputSR1msPedlPwrMonInfo;
    Ioc_InputSR1msMocMonInfo_t Ioc_InputSR1msMocMonInfo;
    Ioc_InputSR1msVlvMonInfo_t Ioc_InputSR1msVlvMonInfo;
    Ioc_InputSR1msWhlVlvFbMonInfo_t Ioc_InputSR1msWhlVlvFbMonInfo;
    Ioc_InputSR1msVBatt1Mon_t Ioc_InputSR1msVBatt1Mon;
    Ioc_InputSR1msVBatt2Mon_t Ioc_InputSR1msVBatt2Mon;
    Ioc_InputSR1msFspCbsHMon_t Ioc_InputSR1msFspCbsHMon;
    Ioc_InputSR1msFspCbsMon_t Ioc_InputSR1msFspCbsMon;
    Ioc_InputSR1msFspAbsHMon_t Ioc_InputSR1msFspAbsHMon;
    Ioc_InputSR1msFspAbsMon_t Ioc_InputSR1msFspAbsMon;
    Ioc_InputSR1msExt5vMon_t Ioc_InputSR1msExt5vMon;
    Ioc_InputSR1msCEMon_t Ioc_InputSR1msCEMon;
    Ioc_InputSR1msLineTestMon_t Ioc_InputSR1msLineTestMon;
    Ioc_InputSR1msTempMon_t Ioc_InputSR1msTempMon;
    Ioc_InputSR1msVdd1Mon_t Ioc_InputSR1msVdd1Mon;
    Ioc_InputSR1msVdd2Mon_t Ioc_InputSR1msVdd2Mon;
    Ioc_InputSR1msVdd3Mon_t Ioc_InputSR1msVdd3Mon;
    Ioc_InputSR1msVdd4Mon_t Ioc_InputSR1msVdd4Mon;
    Ioc_InputSR1msVdd5Mon_t Ioc_InputSR1msVdd5Mon;
    Ioc_InputSR1msVddMon_t Ioc_InputSR1msVddMon;
    Ioc_InputSR1msCspMon_t Ioc_InputSR1msCspMon;
    Ioc_InputSR1msInt5vMon_t Ioc_InputSR1msInt5vMon;
    Ioc_InputSR1msBFLMon_t Ioc_InputSR1msBFLMon;
}Ioc_InputSR1ms_HdrBusType;

typedef struct
{
/* Input Data Element */
    Mom_HndlrEcuModeSts_t Ioc_InputCS1msEcuModeSts;
    Acm_MainAcmAsicInitCompleteFlag_t Ioc_InputCS1msAcmAsicInitCompleteFlag;
    Eem_SuspcDetnFuncInhibitIocSts_t Ioc_InputCS1msFuncInhibitIocSts;

/* Output Data Element */
    Ioc_InputCS1msSwtMonInfo_t Ioc_InputCS1msSwtMonInfo;
    Ioc_InputCS1msSwtMonInfoEsc_t Ioc_InputCS1msSwtMonInfoEsc;
    Ioc_InputCS1msRlyMonInfo_t Ioc_InputCS1msRlyMonInfo;
    Ioc_InputCS1msSwLineMonInfo_t Ioc_InputCS1msSwLineMonInfo;
    Ioc_InputCS1msRsmDbcMon_t Ioc_InputCS1msRsmDbcMon;
    Ioc_InputCS1msPbcVdaAi_t Ioc_InputCS1msPbcVdaAi;
}Ioc_InputCS1ms_HdrBusType;

typedef struct
{
/* Input Data Element */
    Vlv_ActrWhlVlvDrvInfo_t Ioc_OutputSR1msWhlVlvDrvInfo;
    Vlv_ActrBalVlvDrvInfo_t Ioc_OutputSR1msBalVlvDrvInfo;
    Fsr_ActrFsrCbsDrvInfo_t Ioc_OutputSR1msFsrCbsDrvInfo;
    Fsr_ActrFsrAbsDrvInfo_t Ioc_OutputSR1msFsrAbsDrvInfo;
    Mom_HndlrEcuModeSts_t Ioc_OutputSR1msEcuModeSts;
    Acm_MainAcmAsicInitCompleteFlag_t Ioc_OutputSR1msAcmAsicInitCompleteFlag;
    Eem_SuspcDetnFuncInhibitIocSts_t Ioc_OutputSR1msFuncInhibitIocSts;

/* Output Data Element */
    Ioc_OutputSR1msIocDcMtrDutyData_t Ioc_OutputSR1msIocDcMtrDutyData;
    Ioc_OutputSR1msIocDcMtrFreqData_t Ioc_OutputSR1msIocDcMtrFreqData;
    Ioc_OutputSR1msIocVlvNo0Data_t Ioc_OutputSR1msIocVlvNo0Data;
    Ioc_OutputSR1msIocVlvNo1Data_t Ioc_OutputSR1msIocVlvNo1Data;
    Ioc_OutputSR1msIocVlvNo2Data_t Ioc_OutputSR1msIocVlvNo2Data;
    Ioc_OutputSR1msIocVlvNo3Data_t Ioc_OutputSR1msIocVlvNo3Data;
    Ioc_OutputSR1msIocVlvTc0Data_t Ioc_OutputSR1msIocVlvTc0Data;
    Ioc_OutputSR1msIocVlvTc1Data_t Ioc_OutputSR1msIocVlvTc1Data;
    Ioc_OutputSR1msIocVlvEsv0Data_t Ioc_OutputSR1msIocVlvEsv0Data;
    Ioc_OutputSR1msIocVlvEsv1Data_t Ioc_OutputSR1msIocVlvEsv1Data;
    Ioc_OutputSR1msIocVlvNc0Data_t Ioc_OutputSR1msIocVlvNc0Data;
    Ioc_OutputSR1msIocVlvNc1Data_t Ioc_OutputSR1msIocVlvNc1Data;
    Ioc_OutputSR1msIocVlvNc2Data_t Ioc_OutputSR1msIocVlvNc2Data;
    Ioc_OutputSR1msIocVlvNc3Data_t Ioc_OutputSR1msIocVlvNc3Data;
    Ioc_OutputSR1msIocVlvRelayData_t Ioc_OutputSR1msIocVlvRelayData;
    Ioc_OutputSR1msIocAdcSelWriteData_t Ioc_OutputSR1msIocAdcSelWriteData;
}Ioc_OutputSR1ms_HdrBusType;

typedef struct
{
/* Input Data Element */
    Vlv_ActrNormVlvDrvInfo_t Ioc_OutputCS1msNormVlvDrvInfo;
    Vlv_ActrWhlVlvDrvInfo_t Ioc_OutputCS1msWhlVlvDrvInfo;
    Rly_ActrRlyDrvInfo_t Ioc_OutputCS1msRlyDrvInfo;
    Fsr_ActrFsrAbsDrvInfo_t Ioc_OutputCS1msFsrAbsDrvInfo;
    Fsr_ActrFsrCbsDrvInfo_t Ioc_OutputCS1msFsrCbsDrvInfo;
    SenPwrM_MainSenPwrMonitor_t Ioc_OutputCS1msSenPwrMonitorData;
    Mom_HndlrEcuModeSts_t Ioc_OutputCS1msEcuModeSts;
    Acm_MainAcmAsicInitCompleteFlag_t Ioc_OutputCS1msAcmAsicInitCompleteFlag;
    Eem_SuspcDetnFuncInhibitIocSts_t Ioc_OutputCS1msFuncInhibitIocSts;
    CanTrv_TLE6251_HndlrMainCanEn_t Ioc_OutputCS1msMainCanEn;
    Vlvd_A3944_HndlrVlvDrvEnRst_t Ioc_OutputCS1msVlvDrvEnRst;
    Awd_mainAwdPrnDrv_t Ioc_OutputCS1msAwdPrnDrv;
    Fsr_ActrFsrDcMtrShutDwn_t Ioc_OutputCS1msFsrDcMtrShutDwn;
    Fsr_ActrFsrEnDrDrv_t Ioc_OutputCS1msFsrEnDrDrv;

/* Output Data Element */
}Ioc_OutputCS1ms_HdrBusType;

typedef struct
{
/* Input Data Element */
    Icu_InputCaptureRisngIdxInfo_t Ioc_InputSR5msRisngIdxInfo;
    Icu_InputCaptureFallIdxInfo_t Ioc_InputSR5msFallIdxInfo;
    Icu_InputCaptureRisngTiStampInfo_t Ioc_InputSR5msRisngTiStampInfo;
    Icu_InputCaptureFallTiStampInfo_t Ioc_InputSR5msFallTiStampInfo;
    Mom_HndlrEcuModeSts_t Ioc_InputSR5msEcuModeSts;
    Acm_MainAcmAsicInitCompleteFlag_t Ioc_InputSR5msAcmAsicInitCompleteFlag;
    Eem_SuspcDetnFuncInhibitIocSts_t Ioc_InputSR5msFuncInhibitIocSts;

/* Output Data Element */
    Ioc_InputSR5msWssMonInfo_t Ioc_InputSR5msWssMonInfo;
}Ioc_InputSR5ms_HdrBusType;

typedef struct
{
/* Input Data Element */
    Mom_HndlrEcuModeSts_t Ioc_OutputCS5msEcuModeSts;
    Acm_MainAcmAsicInitCompleteFlag_t Ioc_OutputCS5msAcmAsicInitCompleteFlag;
    Eem_SuspcDetnFuncInhibitIocSts_t Ioc_OutputCS5msFuncInhibitIocSts;

/* Output Data Element */
}Ioc_OutputCS5ms_HdrBusType;

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* IOC_TYPES_H_ */
/** @} */
