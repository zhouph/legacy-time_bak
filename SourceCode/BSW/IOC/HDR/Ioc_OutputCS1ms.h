/**
 * @defgroup Ioc_OutputCS1ms Ioc_OutputCS1ms
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_OutputCS1ms.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef IOC_OUTPUTCS1MS_H_
#define IOC_OUTPUTCS1MS_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ioc_Types.h"
#include "Ioc_Cfg.h"
#include "Ioc_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define IOC_OUTPUTCS1MS_MODULE_ID      (0)
 #define IOC_OUTPUTCS1MS_MAJOR_VERSION  (2)
 #define IOC_OUTPUTCS1MS_MINOR_VERSION  (0)
 #define IOC_OUTPUTCS1MS_PATCH_VERSION  (0)
 #define IOC_OUTPUTCS1MS_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Ioc_OutputCS1ms_HdrBusType Ioc_OutputCS1msBus;

/* Version Info */
extern const SwcVersionInfo_t Ioc_OutputCS1msVersionInfo;

/* Input Data Element */
extern Vlv_ActrNormVlvDrvInfo_t Ioc_OutputCS1msNormVlvDrvInfo;
extern Vlv_ActrWhlVlvDrvInfo_t Ioc_OutputCS1msWhlVlvDrvInfo;
extern Rly_ActrRlyDrvInfo_t Ioc_OutputCS1msRlyDrvInfo;
extern Fsr_ActrFsrAbsDrvInfo_t Ioc_OutputCS1msFsrAbsDrvInfo;
extern Fsr_ActrFsrCbsDrvInfo_t Ioc_OutputCS1msFsrCbsDrvInfo;
extern SenPwrM_MainSenPwrMonitor_t Ioc_OutputCS1msSenPwrMonitorData;
extern Mom_HndlrEcuModeSts_t Ioc_OutputCS1msEcuModeSts;
extern Acm_MainAcmAsicInitCompleteFlag_t Ioc_OutputCS1msAcmAsicInitCompleteFlag;
extern Eem_SuspcDetnFuncInhibitIocSts_t Ioc_OutputCS1msFuncInhibitIocSts;
extern CanTrv_TLE6251_HndlrMainCanEn_t Ioc_OutputCS1msMainCanEn;
extern Vlvd_A3944_HndlrVlvDrvEnRst_t Ioc_OutputCS1msVlvDrvEnRst;
extern Awd_mainAwdPrnDrv_t Ioc_OutputCS1msAwdPrnDrv;
extern Fsr_ActrFsrDcMtrShutDwn_t Ioc_OutputCS1msFsrDcMtrShutDwn;
extern Fsr_ActrFsrEnDrDrv_t Ioc_OutputCS1msFsrEnDrDrv;

/* Output Data Element */

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Ioc_OutputCS1ms_Init(void);
extern void Ioc_OutputCS1ms(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* IOC_OUTPUTCS1MS_H_ */
/** @} */
