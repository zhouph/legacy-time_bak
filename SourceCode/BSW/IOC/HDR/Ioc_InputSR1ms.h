/**
 * @defgroup Ioc_InputSR1ms Ioc_InputSR1ms
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_InputSR1ms.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef IOC_INPUTSR1MS_H_
#define IOC_INPUTSR1MS_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ioc_Types.h"
#include "Ioc_Cfg.h"
#include "Ioc_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define IOC_INPUTSR1MS_MODULE_ID      (0)
 #define IOC_INPUTSR1MS_MAJOR_VERSION  (2)
 #define IOC_INPUTSR1MS_MINOR_VERSION  (0)
 #define IOC_INPUTSR1MS_PATCH_VERSION  (0)
 #define IOC_INPUTSR1MS_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Ioc_InputSR1ms_HdrBusType Ioc_InputSR1msBus;

/* Version Info */
extern const SwcVersionInfo_t Ioc_InputSR1msVersionInfo;

/* Input Data Element */
extern Ach_InputAchAdcData_t Ioc_InputSR1msAchAdcData;
extern AdcIf_Conv1msSwTrigPwrInfo_t Ioc_InputSR1msSwTrigPwrInfo;
extern AdcIf_Conv1msSwTrigFspInfo_t Ioc_InputSR1msSwTrigFspInfo;
extern AdcIf_Conv1msSwTrigTmpInfo_t Ioc_InputSR1msSwTrigTmpInfo;
extern AdcIf_Conv1msSwTrigMotInfo_t Ioc_InputSR1msSwTrigMotInfo;
extern AdcIf_Conv1msSwTrigPdtInfo_t Ioc_InputSR1msSwTrigPdtInfo;
extern AdcIf_Conv1msSwTrigMocInfo_t Ioc_InputSR1msSwTrigMocInfo;
extern AdcIf_Conv1msHwTrigVlvInfo_t Ioc_InputSR1msHwTrigVlvInfo;
extern AdcIf_Conv1msSwTrigEscInfo_t Ioc_InputSR1msSwTrigEscInfo;
extern SentH_MainSentHPressureInfo_t Ioc_InputSR1msSentHPressureInfo;
extern Mom_HndlrEcuModeSts_t Ioc_InputSR1msEcuModeSts;
extern Acm_MainAcmAsicInitCompleteFlag_t Ioc_InputSR1msAcmAsicInitCompleteFlag;
extern AdcIf_Conv1msAdcInvalid_t Ioc_InputSR1msAdcInvalid;
extern Eem_SuspcDetnFuncInhibitIocSts_t Ioc_InputSR1msFuncInhibitIocSts;

/* Output Data Element */
extern Ioc_InputSR1msPwrMonInfoEsc_t Ioc_InputSR1msPwrMonInfoEsc;
extern Ioc_InputSR1msVlvMonInfoEsc_t Ioc_InputSR1msVlvMonInfoEsc;
extern Ioc_InputSR1msMotMonInfoEsc_t Ioc_InputSR1msMotMonInfoEsc;
extern Ioc_InputSR1msEpbMonInfoEsc_t Ioc_InputSR1msEpbMonInfoEsc;
extern Ioc_InputSR1msReservedMonInfoEsc_t Ioc_InputSR1msReservedMonInfoEsc;
extern Ioc_InputSR1msHalPressureInfo_t Ioc_InputSR1msHalPressureInfo;
extern Ioc_InputSR1msMotMonInfo_t Ioc_InputSR1msMotMonInfo;
extern Ioc_InputSR1msMotVoltsMonInfo_t Ioc_InputSR1msMotVoltsMonInfo;
extern Ioc_InputSR1msPedlSigMonInfo_t Ioc_InputSR1msPedlSigMonInfo;
extern Ioc_InputSR1msPedlPwrMonInfo_t Ioc_InputSR1msPedlPwrMonInfo;
extern Ioc_InputSR1msMocMonInfo_t Ioc_InputSR1msMocMonInfo;
extern Ioc_InputSR1msVlvMonInfo_t Ioc_InputSR1msVlvMonInfo;
extern Ioc_InputSR1msWhlVlvFbMonInfo_t Ioc_InputSR1msWhlVlvFbMonInfo;
extern Ioc_InputSR1msVBatt1Mon_t Ioc_InputSR1msVBatt1Mon;
extern Ioc_InputSR1msVBatt2Mon_t Ioc_InputSR1msVBatt2Mon;
extern Ioc_InputSR1msFspCbsHMon_t Ioc_InputSR1msFspCbsHMon;
extern Ioc_InputSR1msFspCbsMon_t Ioc_InputSR1msFspCbsMon;
extern Ioc_InputSR1msFspAbsHMon_t Ioc_InputSR1msFspAbsHMon;
extern Ioc_InputSR1msFspAbsMon_t Ioc_InputSR1msFspAbsMon;
extern Ioc_InputSR1msExt5vMon_t Ioc_InputSR1msExt5vMon;
extern Ioc_InputSR1msCEMon_t Ioc_InputSR1msCEMon;
extern Ioc_InputSR1msLineTestMon_t Ioc_InputSR1msLineTestMon;
extern Ioc_InputSR1msTempMon_t Ioc_InputSR1msTempMon;
extern Ioc_InputSR1msVdd1Mon_t Ioc_InputSR1msVdd1Mon;
extern Ioc_InputSR1msVdd2Mon_t Ioc_InputSR1msVdd2Mon;
extern Ioc_InputSR1msVdd3Mon_t Ioc_InputSR1msVdd3Mon;
extern Ioc_InputSR1msVdd4Mon_t Ioc_InputSR1msVdd4Mon;
extern Ioc_InputSR1msVdd5Mon_t Ioc_InputSR1msVdd5Mon;
extern Ioc_InputSR1msVddMon_t Ioc_InputSR1msVddMon;
extern Ioc_InputSR1msCspMon_t Ioc_InputSR1msCspMon;
extern Ioc_InputSR1msInt5vMon_t Ioc_InputSR1msInt5vMon;
extern Ioc_InputSR1msBFLMon_t Ioc_InputSR1msBFLMon;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Ioc_InputSR1ms_Init(void);
extern void Ioc_InputSR1ms(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* IOC_INPUTSR1MS_H_ */
/** @} */
