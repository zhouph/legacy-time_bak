/**
 * @defgroup Ioc_InputSR5ms Ioc_InputSR5ms
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_InputSR5ms.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef IOC_INPUTSR5MS_H_
#define IOC_INPUTSR5MS_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ioc_Types.h"
#include "Ioc_Cfg.h"
#include "Ioc_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define IOC_INPUTSR5MS_MODULE_ID      (0)
 #define IOC_INPUTSR5MS_MAJOR_VERSION  (2)
 #define IOC_INPUTSR5MS_MINOR_VERSION  (0)
 #define IOC_INPUTSR5MS_PATCH_VERSION  (0)
 #define IOC_INPUTSR5MS_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Ioc_InputSR5ms_HdrBusType Ioc_InputSR5msBus;

/* Version Info */
extern const SwcVersionInfo_t Ioc_InputSR5msVersionInfo;

/* Input Data Element */
extern Icu_InputCaptureRisngIdxInfo_t Ioc_InputSR5msRisngIdxInfo;
extern Icu_InputCaptureFallIdxInfo_t Ioc_InputSR5msFallIdxInfo;
extern Icu_InputCaptureRisngTiStampInfo_t Ioc_InputSR5msRisngTiStampInfo;
extern Icu_InputCaptureFallTiStampInfo_t Ioc_InputSR5msFallTiStampInfo;
extern Mom_HndlrEcuModeSts_t Ioc_InputSR5msEcuModeSts;
extern Acm_MainAcmAsicInitCompleteFlag_t Ioc_InputSR5msAcmAsicInitCompleteFlag;
extern Eem_SuspcDetnFuncInhibitIocSts_t Ioc_InputSR5msFuncInhibitIocSts;

/* Output Data Element */
extern Ioc_InputSR5msWssMonInfo_t Ioc_InputSR5msWssMonInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Ioc_InputSR5ms_Init(void);
extern void Ioc_InputSR5ms(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* IOC_INPUTSR5MS_H_ */
/** @} */
