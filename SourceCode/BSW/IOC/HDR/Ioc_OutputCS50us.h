/**
 * @defgroup Ioc_OutputCS50us Ioc_OutputCS50us
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_OutputCS50us.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef IOC_OUTPUTCS50US_H_
#define IOC_OUTPUTCS50US_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ioc_Types.h"
#include "Ioc_Cfg.h"
#include "Ioc_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define IOC_OUTPUTCS50US_MODULE_ID      (0)
 #define IOC_OUTPUTCS50US_MAJOR_VERSION  (2)
 #define IOC_OUTPUTCS50US_MINOR_VERSION  (0)
 #define IOC_OUTPUTCS50US_PATCH_VERSION  (0)
 #define IOC_OUTPUTCS50US_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Ioc_OutputCS50us_HdrBusType Ioc_OutputCS50usBus;

/* Version Info */
extern const SwcVersionInfo_t Ioc_OutputCS50usVersionInfo;

/* Input Data Element */
extern Acmio_ActrMotPwmDataInfo_t Ioc_OutputCS50usMotPwmDataInfo;
extern Mom_HndlrEcuModeSts_t Ioc_OutputCS50usEcuModeSts;
extern Eem_SuspcDetnFuncInhibitIocSts_t Ioc_OutputCS50usFuncInhibitIocSts;

/* Output Data Element */

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Ioc_OutputCS50us_Init(void);
extern void Ioc_OutputCS50us(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* IOC_OUTPUTCS50US_H_ */
/** @} */
