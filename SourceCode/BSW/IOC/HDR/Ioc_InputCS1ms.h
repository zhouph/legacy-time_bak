/**
 * @defgroup Ioc_InputCS1ms Ioc_InputCS1ms
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_InputCS1ms.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef IOC_INPUTCS1MS_H_
#define IOC_INPUTCS1MS_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ioc_Types.h"
#include "Ioc_Cfg.h"
#include "Ioc_Cal.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define IOC_INPUTCS1MS_MODULE_ID      (0)
 #define IOC_INPUTCS1MS_MAJOR_VERSION  (2)
 #define IOC_INPUTCS1MS_MINOR_VERSION  (0)
 #define IOC_INPUTCS1MS_PATCH_VERSION  (0)
 #define IOC_INPUTCS1MS_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Ioc_InputCS1ms_HdrBusType Ioc_InputCS1msBus;

/* Version Info */
extern const SwcVersionInfo_t Ioc_InputCS1msVersionInfo;

/* Input Data Element */
extern Mom_HndlrEcuModeSts_t Ioc_InputCS1msEcuModeSts;
extern Acm_MainAcmAsicInitCompleteFlag_t Ioc_InputCS1msAcmAsicInitCompleteFlag;
extern Eem_SuspcDetnFuncInhibitIocSts_t Ioc_InputCS1msFuncInhibitIocSts;

/* Output Data Element */
extern Ioc_InputCS1msSwtMonInfo_t Ioc_InputCS1msSwtMonInfo;
extern Ioc_InputCS1msSwtMonInfoEsc_t Ioc_InputCS1msSwtMonInfoEsc;
extern Ioc_InputCS1msRlyMonInfo_t Ioc_InputCS1msRlyMonInfo;
extern Ioc_InputCS1msSwLineMonInfo_t Ioc_InputCS1msSwLineMonInfo;
extern Ioc_InputCS1msRsmDbcMon_t Ioc_InputCS1msRsmDbcMon;
extern Ioc_InputCS1msPbcVdaAi_t Ioc_InputCS1msPbcVdaAi;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Ioc_InputCS1ms_Init(void);
extern void Ioc_InputCS1ms(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* IOC_INPUTCS1MS_H_ */
/** @} */
