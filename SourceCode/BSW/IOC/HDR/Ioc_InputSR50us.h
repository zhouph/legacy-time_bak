/**
 * @defgroup Ioc_InputSR50us Ioc_InputSR50us
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_InputSR50us.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef IOC_INPUTSR50US_H_
#define IOC_INPUTSR50US_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ioc_Types.h"
#include "Ioc_Cfg.h"
#include "Ioc_Cal.h"
#include "Task_50us.h"
#include "Mps_TLE5012_Hndlr_50us.h"

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
 #define IOC_INPUTSR50US_MODULE_ID      (0)
 #define IOC_INPUTSR50US_MAJOR_VERSION  (2)
 #define IOC_INPUTSR50US_MINOR_VERSION  (0)
 #define IOC_INPUTSR50US_PATCH_VERSION  (0)
 #define IOC_INPUTSR50US_BRANCH_VERSION (0)


/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/
/* Internal Bus */
extern Ioc_InputSR50us_HdrBusType Ioc_InputSR50usBus;

/* Version Info */
extern const SwcVersionInfo_t Ioc_InputSR50usVersionInfo;

/* Input Data Element */
extern Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo_t Ioc_InputSR50usMpsD1IIFAngInfo;
extern Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo_t Ioc_InputSR50usMpsD2IIFAngInfo;
extern AdcIf_Conv50usHwTrigMotInfo_t Ioc_InputSR50usHwTrigMotInfo;
extern Mom_HndlrEcuModeSts_t Ioc_InputSR50usEcuModeSts;
extern Eem_SuspcDetnFuncInhibitIocSts_t Ioc_InputSR50usFuncInhibitIocSts;

/* Output Data Element */
extern Ioc_InputSR50usMotCurrMonInfo_t Ioc_InputSR50usMotCurrMonInfo;
extern Ioc_InputSR50usMotAngleMonInfo_t Ioc_InputSR50usMotAngleMonInfo;

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/
extern void Ioc_InputSR50us_Init(void);
extern void Ioc_InputSR50us(void);

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* IOC_INPUTSR50US_H_ */
/** @} */
