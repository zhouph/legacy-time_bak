/**
 * @defgroup Ioc_InputSR1ms Ioc_InputSR1ms
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_InputSR1ms.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ioc_InputSR1ms.h"
#include "Ioc_InputSR1ms_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define IOC_INPUTSR1MS_START_SEC_CONST_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define IOC_INPUTSR1MS_STOP_SEC_CONST_UNSPECIFIED
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define IOC_INPUTSR1MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Ioc_InputSR1ms_HdrBusType Ioc_InputSR1msBus;

/* Version Info */
const SwcVersionInfo_t Ioc_InputSR1msVersionInfo = 
{   
    IOC_INPUTSR1MS_MODULE_ID,           /* Ioc_InputSR1msVersionInfo.ModuleId */
    IOC_INPUTSR1MS_MAJOR_VERSION,       /* Ioc_InputSR1msVersionInfo.MajorVer */
    IOC_INPUTSR1MS_MINOR_VERSION,       /* Ioc_InputSR1msVersionInfo.MinorVer */
    IOC_INPUTSR1MS_PATCH_VERSION,       /* Ioc_InputSR1msVersionInfo.PatchVer */
    IOC_INPUTSR1MS_BRANCH_VERSION       /* Ioc_InputSR1msVersionInfo.BranchVer */
};
    
/* Input Data Element */
Ach_InputAchAdcData_t Ioc_InputSR1msAchAdcData;
AdcIf_Conv1msSwTrigPwrInfo_t Ioc_InputSR1msSwTrigPwrInfo;
AdcIf_Conv1msSwTrigFspInfo_t Ioc_InputSR1msSwTrigFspInfo;
AdcIf_Conv1msSwTrigTmpInfo_t Ioc_InputSR1msSwTrigTmpInfo;
AdcIf_Conv1msSwTrigMotInfo_t Ioc_InputSR1msSwTrigMotInfo;
AdcIf_Conv1msSwTrigPdtInfo_t Ioc_InputSR1msSwTrigPdtInfo;
AdcIf_Conv1msSwTrigMocInfo_t Ioc_InputSR1msSwTrigMocInfo;
AdcIf_Conv1msHwTrigVlvInfo_t Ioc_InputSR1msHwTrigVlvInfo;
AdcIf_Conv1msSwTrigEscInfo_t Ioc_InputSR1msSwTrigEscInfo;
SentH_MainSentHPressureInfo_t Ioc_InputSR1msSentHPressureInfo;
Mom_HndlrEcuModeSts_t Ioc_InputSR1msEcuModeSts;
Acm_MainAcmAsicInitCompleteFlag_t Ioc_InputSR1msAcmAsicInitCompleteFlag;
AdcIf_Conv1msAdcInvalid_t Ioc_InputSR1msAdcInvalid;
Eem_SuspcDetnFuncInhibitIocSts_t Ioc_InputSR1msFuncInhibitIocSts;

/* Output Data Element */
Ioc_InputSR1msPwrMonInfoEsc_t Ioc_InputSR1msPwrMonInfoEsc;
Ioc_InputSR1msVlvMonInfoEsc_t Ioc_InputSR1msVlvMonInfoEsc;
Ioc_InputSR1msMotMonInfoEsc_t Ioc_InputSR1msMotMonInfoEsc;
Ioc_InputSR1msEpbMonInfoEsc_t Ioc_InputSR1msEpbMonInfoEsc;
Ioc_InputSR1msReservedMonInfoEsc_t Ioc_InputSR1msReservedMonInfoEsc;
Ioc_InputSR1msHalPressureInfo_t Ioc_InputSR1msHalPressureInfo;
Ioc_InputSR1msMotMonInfo_t Ioc_InputSR1msMotMonInfo;
Ioc_InputSR1msMotVoltsMonInfo_t Ioc_InputSR1msMotVoltsMonInfo;
Ioc_InputSR1msPedlSigMonInfo_t Ioc_InputSR1msPedlSigMonInfo;
Ioc_InputSR1msPedlPwrMonInfo_t Ioc_InputSR1msPedlPwrMonInfo;
Ioc_InputSR1msMocMonInfo_t Ioc_InputSR1msMocMonInfo;
Ioc_InputSR1msVlvMonInfo_t Ioc_InputSR1msVlvMonInfo;
Ioc_InputSR1msWhlVlvFbMonInfo_t Ioc_InputSR1msWhlVlvFbMonInfo;
Ioc_InputSR1msVBatt1Mon_t Ioc_InputSR1msVBatt1Mon;
Ioc_InputSR1msVBatt2Mon_t Ioc_InputSR1msVBatt2Mon;
Ioc_InputSR1msFspCbsHMon_t Ioc_InputSR1msFspCbsHMon;
Ioc_InputSR1msFspCbsMon_t Ioc_InputSR1msFspCbsMon;
Ioc_InputSR1msFspAbsHMon_t Ioc_InputSR1msFspAbsHMon;
Ioc_InputSR1msFspAbsMon_t Ioc_InputSR1msFspAbsMon;
Ioc_InputSR1msExt5vMon_t Ioc_InputSR1msExt5vMon;
Ioc_InputSR1msCEMon_t Ioc_InputSR1msCEMon;
Ioc_InputSR1msLineTestMon_t Ioc_InputSR1msLineTestMon;
Ioc_InputSR1msTempMon_t Ioc_InputSR1msTempMon;
Ioc_InputSR1msVdd1Mon_t Ioc_InputSR1msVdd1Mon;
Ioc_InputSR1msVdd2Mon_t Ioc_InputSR1msVdd2Mon;
Ioc_InputSR1msVdd3Mon_t Ioc_InputSR1msVdd3Mon;
Ioc_InputSR1msVdd4Mon_t Ioc_InputSR1msVdd4Mon;
Ioc_InputSR1msVdd5Mon_t Ioc_InputSR1msVdd5Mon;
Ioc_InputSR1msVddMon_t Ioc_InputSR1msVddMon;
Ioc_InputSR1msCspMon_t Ioc_InputSR1msCspMon;
Ioc_InputSR1msInt5vMon_t Ioc_InputSR1msInt5vMon;
Ioc_InputSR1msBFLMon_t Ioc_InputSR1msBFLMon;

uint32 Ioc_InputSR1ms_Timer_Start;
uint32 Ioc_InputSR1ms_Timer_Elapsed;

#define IOC_INPUTSR1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_INPUTSR1MS_START_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define IOC_INPUTSR1MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
#define IOC_INPUTSR1MS_START_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define IOC_INPUTSR1MS_STOP_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_INPUTSR1MS_START_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (32BIT)**/


#define IOC_INPUTSR1MS_STOP_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define IOC_INPUTSR1MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define IOC_INPUTSR1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_INPUTSR1MS_START_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define IOC_INPUTSR1MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
#define IOC_INPUTSR1MS_START_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define IOC_INPUTSR1MS_STOP_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_INPUTSR1MS_START_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (32BIT)**/


#define IOC_INPUTSR1MS_STOP_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define IOC_INPUTSR1MS_START_SEC_CODE
#include "Ioc_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Ioc_InputSR1ms_Init(void)
{
    /* Initialize internal bus */
    Ioc_InputSR1msBus.Ioc_InputSR1msAchAdcData.ASIC_RX_MSG_13mu10_Rx_13_ADC_DATA_OUT = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msAchAdcData.ASIC_RX_MSG_13mu1_Rx_13_ADC_BUSY = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigPwrInfo.Ext5vMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigPwrInfo.CEMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigPwrInfo.Int5vMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigPwrInfo.CSPMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigPwrInfo.Vbatt01Mon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigPwrInfo.Vbatt02Mon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigPwrInfo.VddMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigPwrInfo.Vdd3Mon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigPwrInfo.Vdd5Mon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigFspInfo.FspAbsHMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigFspInfo.FspAbsMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigFspInfo.FspCbsHMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigFspInfo.FspCbsMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigTmpInfo.LineTestMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigTmpInfo.TempMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigMotInfo.MotPwrMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigMotInfo.StarMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigMotInfo.UoutMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigMotInfo.VoutMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigMotInfo.WoutMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigPdtInfo.PdfSigMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigPdtInfo.Pdt5vMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigPdtInfo.Pdf5vMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigPdtInfo.PdtSigMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigMocInfo.RlIMainMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigMocInfo.RlISubMonFs = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigMocInfo.RlMocNegMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigMocInfo.RlMocPosMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigMocInfo.RrIMainMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigMocInfo.RrISubMonFs = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigMocInfo.RrMocNegMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigMocInfo.RrMocPosMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigMocInfo.BFLMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msHwTrigVlvInfo.VlvCVMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msHwTrigVlvInfo.VlvRLVMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msHwTrigVlvInfo.VlvCutPMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msHwTrigVlvInfo.VlvCutSMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msHwTrigVlvInfo.VlvSimMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigEscInfo.IF_A7_MON = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigEscInfo.IF_A2_MON = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigEscInfo.P1_NEG_MON = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigEscInfo.IF_A6_MON = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigEscInfo.MTR_FW_S_MON = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigEscInfo.IF_A4_MON = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigEscInfo.MTP_MON = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigEscInfo.IF_A3_MON = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigEscInfo.P1_POS_MON = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigEscInfo.IF_A5_MON = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigEscInfo.VDD_MON = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigEscInfo.COOL_MON = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigEscInfo.CAN_L_MON = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigEscInfo.P3_NEG_MON = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigEscInfo.IF_A1_MON = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigEscInfo.P2_POS_MON = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigEscInfo.IF_A8_MON = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigEscInfo.IF_A9_MON = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigEscInfo.P2_NEG_MON = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigEscInfo.P3_POS_MON = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigEscInfo.OP_OUT_MON = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.McpPresData1 = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.McpPresData2 = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.McpSentCrc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.McpSentData = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.McpSentMsgId = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.McpSentSerialCrc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.McpSentConfig = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.McpSentInvalid = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.MCPSentSerialInvalid = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.Wlp1PresData1 = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.Wlp1PresData2 = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.Wlp1SentCrc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.Wlp1SentData = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.Wlp1SentMsgId = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.Wlp1SentSerialCrc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.Wlp1SentConfig = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.Wlp1SentInvalid = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.Wlp1SentSerialInvalid = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.Wlp2PresData1 = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.Wlp2PresData2 = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.Wlp2SentCrc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.Wlp2SentData = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.Wlp2SentMsgId = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.Wlp2SentSerialCrc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.Wlp2SentConfig = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.Wlp2SentInvalid = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.Wlp2SentSerialInvalid = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msEcuModeSts = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msAcmAsicInitCompleteFlag = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msAdcInvalid = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msFuncInhibitIocSts = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msPwrMonInfoEsc.VoltVBatt01Mon_Esc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msPwrMonInfoEsc.VoltVBatt02Mon_Esc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msPwrMonInfoEsc.VoltGio2Mon_Esc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msPwrMonInfoEsc.VoltExt5VSupplyMon_Esc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msPwrMonInfoEsc.VoltExt12VSupplyMon_Esc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msPwrMonInfoEsc.VoltSwVpwrMon_Esc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msPwrMonInfoEsc.VoltVpwrMon_Esc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msPwrMonInfoEsc.VoltGio1Mon_Esc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msPwrMonInfoEsc.VoltIgnMon_Esc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msVlvMonInfoEsc.VoltFspMon_Esc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msMotMonInfoEsc.VoltMtpMon_Esc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msMotMonInfoEsc.VoltMtrFwSMon_Esc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msEpbMonInfoEsc.VoltEpbPwrMon_Esc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msEpbMonInfoEsc.VoltEpbI1LMon_Esc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msEpbMonInfoEsc.VoltEpbI1RMon_Esc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msEpbMonInfoEsc.VoltEpbI2LMon_Esc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msEpbMonInfoEsc.VoltEpbI2RMon_Esc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msEpbMonInfoEsc.VoltEpbMonLPlus_Esc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msEpbMonInfoEsc.VoltEpbMonLMinus_Esc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msEpbMonInfoEsc.VoltEpbMonRPlus_Esc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msEpbMonInfoEsc.VoltEpbMonRMinus_Esc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msReservedMonInfoEsc.VoltMcuAdc1_Esc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msReservedMonInfoEsc.VoltMcuAdc2_Esc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msReservedMonInfoEsc.VoltMcuAdc3_Esc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msReservedMonInfoEsc.VoltMcuAdc4_Esc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msReservedMonInfoEsc.VoltMcuAdc5_Esc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.McpPresData1 = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.McpPresData2 = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.McpSentCrc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.McpSentData = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.McpSentMsgId = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.McpSentSerialCrc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.McpSentConfig = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.Wlp1PresData1 = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.Wlp1PresData2 = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.Wlp1SentCrc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.Wlp1SentData = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.Wlp1SentMsgId = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.Wlp1SentSerialCrc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.Wlp1SentConfig = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.Wlp2PresData1 = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.Wlp2PresData2 = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.Wlp2SentCrc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.Wlp2SentData = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.Wlp2SentMsgId = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.Wlp2SentSerialCrc = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.Wlp2SentConfig = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msMotMonInfo.MotPwrVoltMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msMotMonInfo.MotStarMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msMotVoltsMonInfo.MotVoltPhUMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msMotVoltsMonInfo.MotVoltPhVMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msMotVoltsMonInfo.MotVoltPhWMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msPedlSigMonInfo.PdfSigMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msPedlSigMonInfo.PdtSigMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msPedlPwrMonInfo.Pdt5vMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msPedlPwrMonInfo.Pdf5vMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msMocMonInfo.RlCurrMainMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msMocMonInfo.RlCurrSubMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msMocMonInfo.RlMocNegMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msMocMonInfo.RlMocPosMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msMocMonInfo.RrCurrMainMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msMocMonInfo.RrCurrSubMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msMocMonInfo.RrMocNegMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msMocMonInfo.RrMocPosMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msVlvMonInfo.VlvCVMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msVlvMonInfo.VlvRLVMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msVlvMonInfo.PrimCutVlvMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msVlvMonInfo.SecdCutVlvMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msVlvMonInfo.SimVlvMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msWhlVlvFbMonInfo.FlOvCurrMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msWhlVlvFbMonInfo.FlIvDutyMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msWhlVlvFbMonInfo.FrOvCurrMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msWhlVlvFbMonInfo.FrIvDutyMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msWhlVlvFbMonInfo.RlOvCurrMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msWhlVlvFbMonInfo.RlIvDutyMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msWhlVlvFbMonInfo.RrOvCurrMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msWhlVlvFbMonInfo.RrIvDutyMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msVBatt1Mon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msVBatt2Mon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msFspCbsHMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msFspCbsMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msFspAbsHMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msFspAbsMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msExt5vMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msCEMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msLineTestMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msTempMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msVdd1Mon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msVdd2Mon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msVdd3Mon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msVdd4Mon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msVdd5Mon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msVddMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msCspMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msInt5vMon = 0;
    Ioc_InputSR1msBus.Ioc_InputSR1msBFLMon = 0;
}

void Ioc_InputSR1ms(void)
{
    Ioc_InputSR1ms_Timer_Start = STM0_TIM0.U;

    /* Input */
    Ioc_InputSR1ms_Read_Ioc_InputSR1msAchAdcData(&Ioc_InputSR1msBus.Ioc_InputSR1msAchAdcData);
    /*==============================================================================
    * Members of structure Ioc_InputSR1msAchAdcData 
     : Ioc_InputSR1msAchAdcData.ASIC_RX_MSG_13mu10_Rx_13_ADC_DATA_OUT;
     : Ioc_InputSR1msAchAdcData.ASIC_RX_MSG_13mu1_Rx_13_ADC_BUSY;
     =============================================================================*/
    
    Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigPwrInfo(&Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigPwrInfo);
    /*==============================================================================
    * Members of structure Ioc_InputSR1msSwTrigPwrInfo 
     : Ioc_InputSR1msSwTrigPwrInfo.Ext5vMon;
     : Ioc_InputSR1msSwTrigPwrInfo.CEMon;
     : Ioc_InputSR1msSwTrigPwrInfo.Int5vMon;
     : Ioc_InputSR1msSwTrigPwrInfo.CSPMon;
     : Ioc_InputSR1msSwTrigPwrInfo.Vbatt01Mon;
     : Ioc_InputSR1msSwTrigPwrInfo.Vbatt02Mon;
     : Ioc_InputSR1msSwTrigPwrInfo.VddMon;
     : Ioc_InputSR1msSwTrigPwrInfo.Vdd3Mon;
     : Ioc_InputSR1msSwTrigPwrInfo.Vdd5Mon;
     =============================================================================*/
    
    Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigFspInfo(&Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigFspInfo);
    /*==============================================================================
    * Members of structure Ioc_InputSR1msSwTrigFspInfo 
     : Ioc_InputSR1msSwTrigFspInfo.FspAbsHMon;
     : Ioc_InputSR1msSwTrigFspInfo.FspAbsMon;
     : Ioc_InputSR1msSwTrigFspInfo.FspCbsHMon;
     : Ioc_InputSR1msSwTrigFspInfo.FspCbsMon;
     =============================================================================*/
    
    Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigTmpInfo(&Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigTmpInfo);
    /*==============================================================================
    * Members of structure Ioc_InputSR1msSwTrigTmpInfo 
     : Ioc_InputSR1msSwTrigTmpInfo.LineTestMon;
     : Ioc_InputSR1msSwTrigTmpInfo.TempMon;
     =============================================================================*/
    
    Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigMotInfo(&Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigMotInfo);
    /*==============================================================================
    * Members of structure Ioc_InputSR1msSwTrigMotInfo 
     : Ioc_InputSR1msSwTrigMotInfo.MotPwrMon;
     : Ioc_InputSR1msSwTrigMotInfo.StarMon;
     : Ioc_InputSR1msSwTrigMotInfo.UoutMon;
     : Ioc_InputSR1msSwTrigMotInfo.VoutMon;
     : Ioc_InputSR1msSwTrigMotInfo.WoutMon;
     =============================================================================*/
    
    Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigPdtInfo(&Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigPdtInfo);
    /*==============================================================================
    * Members of structure Ioc_InputSR1msSwTrigPdtInfo 
     : Ioc_InputSR1msSwTrigPdtInfo.PdfSigMon;
     : Ioc_InputSR1msSwTrigPdtInfo.Pdt5vMon;
     : Ioc_InputSR1msSwTrigPdtInfo.Pdf5vMon;
     : Ioc_InputSR1msSwTrigPdtInfo.PdtSigMon;
     =============================================================================*/
    
    Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigMocInfo(&Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigMocInfo);
    /*==============================================================================
    * Members of structure Ioc_InputSR1msSwTrigMocInfo 
     : Ioc_InputSR1msSwTrigMocInfo.RlIMainMon;
     : Ioc_InputSR1msSwTrigMocInfo.RlISubMonFs;
     : Ioc_InputSR1msSwTrigMocInfo.RlMocNegMon;
     : Ioc_InputSR1msSwTrigMocInfo.RlMocPosMon;
     : Ioc_InputSR1msSwTrigMocInfo.RrIMainMon;
     : Ioc_InputSR1msSwTrigMocInfo.RrISubMonFs;
     : Ioc_InputSR1msSwTrigMocInfo.RrMocNegMon;
     : Ioc_InputSR1msSwTrigMocInfo.RrMocPosMon;
     : Ioc_InputSR1msSwTrigMocInfo.BFLMon;
     =============================================================================*/
    
    Ioc_InputSR1ms_Read_Ioc_InputSR1msHwTrigVlvInfo(&Ioc_InputSR1msBus.Ioc_InputSR1msHwTrigVlvInfo);
    /*==============================================================================
    * Members of structure Ioc_InputSR1msHwTrigVlvInfo 
     : Ioc_InputSR1msHwTrigVlvInfo.VlvCVMon;
     : Ioc_InputSR1msHwTrigVlvInfo.VlvRLVMon;
     : Ioc_InputSR1msHwTrigVlvInfo.VlvCutPMon;
     : Ioc_InputSR1msHwTrigVlvInfo.VlvCutSMon;
     : Ioc_InputSR1msHwTrigVlvInfo.VlvSimMon;
     =============================================================================*/
    
    Ioc_InputSR1ms_Read_Ioc_InputSR1msSwTrigEscInfo(&Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigEscInfo);
    /*==============================================================================
    * Members of structure Ioc_InputSR1msSwTrigEscInfo 
     : Ioc_InputSR1msSwTrigEscInfo.IF_A7_MON;
     : Ioc_InputSR1msSwTrigEscInfo.IF_A2_MON;
     : Ioc_InputSR1msSwTrigEscInfo.P1_NEG_MON;
     : Ioc_InputSR1msSwTrigEscInfo.IF_A6_MON;
     : Ioc_InputSR1msSwTrigEscInfo.MTR_FW_S_MON;
     : Ioc_InputSR1msSwTrigEscInfo.IF_A4_MON;
     : Ioc_InputSR1msSwTrigEscInfo.MTP_MON;
     : Ioc_InputSR1msSwTrigEscInfo.IF_A3_MON;
     : Ioc_InputSR1msSwTrigEscInfo.P1_POS_MON;
     : Ioc_InputSR1msSwTrigEscInfo.IF_A5_MON;
     : Ioc_InputSR1msSwTrigEscInfo.VDD_MON;
     : Ioc_InputSR1msSwTrigEscInfo.COOL_MON;
     : Ioc_InputSR1msSwTrigEscInfo.CAN_L_MON;
     : Ioc_InputSR1msSwTrigEscInfo.P3_NEG_MON;
     : Ioc_InputSR1msSwTrigEscInfo.IF_A1_MON;
     : Ioc_InputSR1msSwTrigEscInfo.P2_POS_MON;
     : Ioc_InputSR1msSwTrigEscInfo.IF_A8_MON;
     : Ioc_InputSR1msSwTrigEscInfo.IF_A9_MON;
     : Ioc_InputSR1msSwTrigEscInfo.P2_NEG_MON;
     : Ioc_InputSR1msSwTrigEscInfo.P3_POS_MON;
     : Ioc_InputSR1msSwTrigEscInfo.OP_OUT_MON;
     =============================================================================*/
    
    Ioc_InputSR1ms_Read_Ioc_InputSR1msSentHPressureInfo(&Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo);
    /*==============================================================================
    * Members of structure Ioc_InputSR1msSentHPressureInfo 
     : Ioc_InputSR1msSentHPressureInfo.McpPresData1;
     : Ioc_InputSR1msSentHPressureInfo.McpPresData2;
     : Ioc_InputSR1msSentHPressureInfo.McpSentCrc;
     : Ioc_InputSR1msSentHPressureInfo.McpSentData;
     : Ioc_InputSR1msSentHPressureInfo.McpSentMsgId;
     : Ioc_InputSR1msSentHPressureInfo.McpSentSerialCrc;
     : Ioc_InputSR1msSentHPressureInfo.McpSentConfig;
     : Ioc_InputSR1msSentHPressureInfo.McpSentInvalid;
     : Ioc_InputSR1msSentHPressureInfo.MCPSentSerialInvalid;
     : Ioc_InputSR1msSentHPressureInfo.Wlp1PresData1;
     : Ioc_InputSR1msSentHPressureInfo.Wlp1PresData2;
     : Ioc_InputSR1msSentHPressureInfo.Wlp1SentCrc;
     : Ioc_InputSR1msSentHPressureInfo.Wlp1SentData;
     : Ioc_InputSR1msSentHPressureInfo.Wlp1SentMsgId;
     : Ioc_InputSR1msSentHPressureInfo.Wlp1SentSerialCrc;
     : Ioc_InputSR1msSentHPressureInfo.Wlp1SentConfig;
     : Ioc_InputSR1msSentHPressureInfo.Wlp1SentInvalid;
     : Ioc_InputSR1msSentHPressureInfo.Wlp1SentSerialInvalid;
     : Ioc_InputSR1msSentHPressureInfo.Wlp2PresData1;
     : Ioc_InputSR1msSentHPressureInfo.Wlp2PresData2;
     : Ioc_InputSR1msSentHPressureInfo.Wlp2SentCrc;
     : Ioc_InputSR1msSentHPressureInfo.Wlp2SentData;
     : Ioc_InputSR1msSentHPressureInfo.Wlp2SentMsgId;
     : Ioc_InputSR1msSentHPressureInfo.Wlp2SentSerialCrc;
     : Ioc_InputSR1msSentHPressureInfo.Wlp2SentConfig;
     : Ioc_InputSR1msSentHPressureInfo.Wlp2SentInvalid;
     : Ioc_InputSR1msSentHPressureInfo.Wlp2SentSerialInvalid;
     =============================================================================*/
    
    Ioc_InputSR1ms_Read_Ioc_InputSR1msEcuModeSts(&Ioc_InputSR1msBus.Ioc_InputSR1msEcuModeSts);
    Ioc_InputSR1ms_Read_Ioc_InputSR1msAcmAsicInitCompleteFlag(&Ioc_InputSR1msBus.Ioc_InputSR1msAcmAsicInitCompleteFlag);
    Ioc_InputSR1ms_Read_Ioc_InputSR1msAdcInvalid(&Ioc_InputSR1msBus.Ioc_InputSR1msAdcInvalid);
    Ioc_InputSR1ms_Read_Ioc_InputSR1msFuncInhibitIocSts(&Ioc_InputSR1msBus.Ioc_InputSR1msFuncInhibitIocSts);

    /* Process */
    Ioc_Input1msSRConditioning();    /* For IDB */
	Ioc_Input1msSRConditioningCommon();/* For Common */

    /* Output */
    Ioc_InputSR1ms_Write_Ioc_InputSR1msPwrMonInfoEsc(&Ioc_InputSR1msBus.Ioc_InputSR1msPwrMonInfoEsc);
    /*==============================================================================
    * Members of structure Ioc_InputSR1msPwrMonInfoEsc 
     : Ioc_InputSR1msPwrMonInfoEsc.VoltVBatt01Mon_Esc;
     : Ioc_InputSR1msPwrMonInfoEsc.VoltVBatt02Mon_Esc;
     : Ioc_InputSR1msPwrMonInfoEsc.VoltGio2Mon_Esc;
     : Ioc_InputSR1msPwrMonInfoEsc.VoltExt5VSupplyMon_Esc;
     : Ioc_InputSR1msPwrMonInfoEsc.VoltExt12VSupplyMon_Esc;
     : Ioc_InputSR1msPwrMonInfoEsc.VoltSwVpwrMon_Esc;
     : Ioc_InputSR1msPwrMonInfoEsc.VoltVpwrMon_Esc;
     : Ioc_InputSR1msPwrMonInfoEsc.VoltGio1Mon_Esc;
     : Ioc_InputSR1msPwrMonInfoEsc.VoltIgnMon_Esc;
     =============================================================================*/
    
    Ioc_InputSR1ms_Write_Ioc_InputSR1msVlvMonInfoEsc(&Ioc_InputSR1msBus.Ioc_InputSR1msVlvMonInfoEsc);
    /*==============================================================================
    * Members of structure Ioc_InputSR1msVlvMonInfoEsc 
     : Ioc_InputSR1msVlvMonInfoEsc.VoltFspMon_Esc;
     =============================================================================*/
    
    Ioc_InputSR1ms_Write_Ioc_InputSR1msMotMonInfoEsc(&Ioc_InputSR1msBus.Ioc_InputSR1msMotMonInfoEsc);
    /*==============================================================================
    * Members of structure Ioc_InputSR1msMotMonInfoEsc 
     : Ioc_InputSR1msMotMonInfoEsc.VoltMtpMon_Esc;
     : Ioc_InputSR1msMotMonInfoEsc.VoltMtrFwSMon_Esc;
     =============================================================================*/
    
    Ioc_InputSR1ms_Write_Ioc_InputSR1msEpbMonInfoEsc(&Ioc_InputSR1msBus.Ioc_InputSR1msEpbMonInfoEsc);
    /*==============================================================================
    * Members of structure Ioc_InputSR1msEpbMonInfoEsc 
     : Ioc_InputSR1msEpbMonInfoEsc.VoltEpbPwrMon_Esc;
     : Ioc_InputSR1msEpbMonInfoEsc.VoltEpbI1LMon_Esc;
     : Ioc_InputSR1msEpbMonInfoEsc.VoltEpbI1RMon_Esc;
     : Ioc_InputSR1msEpbMonInfoEsc.VoltEpbI2LMon_Esc;
     : Ioc_InputSR1msEpbMonInfoEsc.VoltEpbI2RMon_Esc;
     : Ioc_InputSR1msEpbMonInfoEsc.VoltEpbMonLPlus_Esc;
     : Ioc_InputSR1msEpbMonInfoEsc.VoltEpbMonLMinus_Esc;
     : Ioc_InputSR1msEpbMonInfoEsc.VoltEpbMonRPlus_Esc;
     : Ioc_InputSR1msEpbMonInfoEsc.VoltEpbMonRMinus_Esc;
     =============================================================================*/
    
    Ioc_InputSR1ms_Write_Ioc_InputSR1msReservedMonInfoEsc(&Ioc_InputSR1msBus.Ioc_InputSR1msReservedMonInfoEsc);
    /*==============================================================================
    * Members of structure Ioc_InputSR1msReservedMonInfoEsc 
     : Ioc_InputSR1msReservedMonInfoEsc.VoltMcuAdc1_Esc;
     : Ioc_InputSR1msReservedMonInfoEsc.VoltMcuAdc2_Esc;
     : Ioc_InputSR1msReservedMonInfoEsc.VoltMcuAdc3_Esc;
     : Ioc_InputSR1msReservedMonInfoEsc.VoltMcuAdc4_Esc;
     : Ioc_InputSR1msReservedMonInfoEsc.VoltMcuAdc5_Esc;
     =============================================================================*/
    
    Ioc_InputSR1ms_Write_Ioc_InputSR1msHalPressureInfo(&Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo);
    /*==============================================================================
    * Members of structure Ioc_InputSR1msHalPressureInfo 
     : Ioc_InputSR1msHalPressureInfo.McpPresData1;
     : Ioc_InputSR1msHalPressureInfo.McpPresData2;
     : Ioc_InputSR1msHalPressureInfo.McpSentCrc;
     : Ioc_InputSR1msHalPressureInfo.McpSentData;
     : Ioc_InputSR1msHalPressureInfo.McpSentMsgId;
     : Ioc_InputSR1msHalPressureInfo.McpSentSerialCrc;
     : Ioc_InputSR1msHalPressureInfo.McpSentConfig;
     : Ioc_InputSR1msHalPressureInfo.Wlp1PresData1;
     : Ioc_InputSR1msHalPressureInfo.Wlp1PresData2;
     : Ioc_InputSR1msHalPressureInfo.Wlp1SentCrc;
     : Ioc_InputSR1msHalPressureInfo.Wlp1SentData;
     : Ioc_InputSR1msHalPressureInfo.Wlp1SentMsgId;
     : Ioc_InputSR1msHalPressureInfo.Wlp1SentSerialCrc;
     : Ioc_InputSR1msHalPressureInfo.Wlp1SentConfig;
     : Ioc_InputSR1msHalPressureInfo.Wlp2PresData1;
     : Ioc_InputSR1msHalPressureInfo.Wlp2PresData2;
     : Ioc_InputSR1msHalPressureInfo.Wlp2SentCrc;
     : Ioc_InputSR1msHalPressureInfo.Wlp2SentData;
     : Ioc_InputSR1msHalPressureInfo.Wlp2SentMsgId;
     : Ioc_InputSR1msHalPressureInfo.Wlp2SentSerialCrc;
     : Ioc_InputSR1msHalPressureInfo.Wlp2SentConfig;
     =============================================================================*/
    
    Ioc_InputSR1ms_Write_Ioc_InputSR1msMotMonInfo(&Ioc_InputSR1msBus.Ioc_InputSR1msMotMonInfo);
    /*==============================================================================
    * Members of structure Ioc_InputSR1msMotMonInfo 
     : Ioc_InputSR1msMotMonInfo.MotPwrVoltMon;
     : Ioc_InputSR1msMotMonInfo.MotStarMon;
     =============================================================================*/
    
    Ioc_InputSR1ms_Write_Ioc_InputSR1msMotVoltsMonInfo(&Ioc_InputSR1msBus.Ioc_InputSR1msMotVoltsMonInfo);
    /*==============================================================================
    * Members of structure Ioc_InputSR1msMotVoltsMonInfo 
     : Ioc_InputSR1msMotVoltsMonInfo.MotVoltPhUMon;
     : Ioc_InputSR1msMotVoltsMonInfo.MotVoltPhVMon;
     : Ioc_InputSR1msMotVoltsMonInfo.MotVoltPhWMon;
     =============================================================================*/
    
    Ioc_InputSR1ms_Write_Ioc_InputSR1msPedlSigMonInfo(&Ioc_InputSR1msBus.Ioc_InputSR1msPedlSigMonInfo);
    /*==============================================================================
    * Members of structure Ioc_InputSR1msPedlSigMonInfo 
     : Ioc_InputSR1msPedlSigMonInfo.PdfSigMon;
     : Ioc_InputSR1msPedlSigMonInfo.PdtSigMon;
     =============================================================================*/
    
    Ioc_InputSR1ms_Write_Ioc_InputSR1msPedlPwrMonInfo(&Ioc_InputSR1msBus.Ioc_InputSR1msPedlPwrMonInfo);
    /*==============================================================================
    * Members of structure Ioc_InputSR1msPedlPwrMonInfo 
     : Ioc_InputSR1msPedlPwrMonInfo.Pdt5vMon;
     : Ioc_InputSR1msPedlPwrMonInfo.Pdf5vMon;
     =============================================================================*/
    
    Ioc_InputSR1ms_Write_Ioc_InputSR1msMocMonInfo(&Ioc_InputSR1msBus.Ioc_InputSR1msMocMonInfo);
    /*==============================================================================
    * Members of structure Ioc_InputSR1msMocMonInfo 
     : Ioc_InputSR1msMocMonInfo.RlCurrMainMon;
     : Ioc_InputSR1msMocMonInfo.RlCurrSubMon;
     : Ioc_InputSR1msMocMonInfo.RlMocNegMon;
     : Ioc_InputSR1msMocMonInfo.RlMocPosMon;
     : Ioc_InputSR1msMocMonInfo.RrCurrMainMon;
     : Ioc_InputSR1msMocMonInfo.RrCurrSubMon;
     : Ioc_InputSR1msMocMonInfo.RrMocNegMon;
     : Ioc_InputSR1msMocMonInfo.RrMocPosMon;
     =============================================================================*/
    
    Ioc_InputSR1ms_Write_Ioc_InputSR1msVlvMonInfo(&Ioc_InputSR1msBus.Ioc_InputSR1msVlvMonInfo);
    /*==============================================================================
    * Members of structure Ioc_InputSR1msVlvMonInfo 
     : Ioc_InputSR1msVlvMonInfo.VlvCVMon;
     : Ioc_InputSR1msVlvMonInfo.VlvRLVMon;
     : Ioc_InputSR1msVlvMonInfo.PrimCutVlvMon;
     : Ioc_InputSR1msVlvMonInfo.SecdCutVlvMon;
     : Ioc_InputSR1msVlvMonInfo.SimVlvMon;
     =============================================================================*/
    
    Ioc_InputSR1ms_Write_Ioc_InputSR1msWhlVlvFbMonInfo(&Ioc_InputSR1msBus.Ioc_InputSR1msWhlVlvFbMonInfo);
    /*==============================================================================
    * Members of structure Ioc_InputSR1msWhlVlvFbMonInfo 
     : Ioc_InputSR1msWhlVlvFbMonInfo.FlOvCurrMon;
     : Ioc_InputSR1msWhlVlvFbMonInfo.FlIvDutyMon;
     : Ioc_InputSR1msWhlVlvFbMonInfo.FrOvCurrMon;
     : Ioc_InputSR1msWhlVlvFbMonInfo.FrIvDutyMon;
     : Ioc_InputSR1msWhlVlvFbMonInfo.RlOvCurrMon;
     : Ioc_InputSR1msWhlVlvFbMonInfo.RlIvDutyMon;
     : Ioc_InputSR1msWhlVlvFbMonInfo.RrOvCurrMon;
     : Ioc_InputSR1msWhlVlvFbMonInfo.RrIvDutyMon;
     =============================================================================*/
    
    Ioc_InputSR1ms_Write_Ioc_InputSR1msVBatt1Mon(&Ioc_InputSR1msBus.Ioc_InputSR1msVBatt1Mon);
    Ioc_InputSR1ms_Write_Ioc_InputSR1msVBatt2Mon(&Ioc_InputSR1msBus.Ioc_InputSR1msVBatt2Mon);
    Ioc_InputSR1ms_Write_Ioc_InputSR1msFspCbsHMon(&Ioc_InputSR1msBus.Ioc_InputSR1msFspCbsHMon);
    Ioc_InputSR1ms_Write_Ioc_InputSR1msFspCbsMon(&Ioc_InputSR1msBus.Ioc_InputSR1msFspCbsMon);
    Ioc_InputSR1ms_Write_Ioc_InputSR1msFspAbsHMon(&Ioc_InputSR1msBus.Ioc_InputSR1msFspAbsHMon);
    Ioc_InputSR1ms_Write_Ioc_InputSR1msFspAbsMon(&Ioc_InputSR1msBus.Ioc_InputSR1msFspAbsMon);
    Ioc_InputSR1ms_Write_Ioc_InputSR1msExt5vMon(&Ioc_InputSR1msBus.Ioc_InputSR1msExt5vMon);
    Ioc_InputSR1ms_Write_Ioc_InputSR1msCEMon(&Ioc_InputSR1msBus.Ioc_InputSR1msCEMon);
    Ioc_InputSR1ms_Write_Ioc_InputSR1msLineTestMon(&Ioc_InputSR1msBus.Ioc_InputSR1msLineTestMon);
    Ioc_InputSR1ms_Write_Ioc_InputSR1msTempMon(&Ioc_InputSR1msBus.Ioc_InputSR1msTempMon);
    Ioc_InputSR1ms_Write_Ioc_InputSR1msVdd1Mon(&Ioc_InputSR1msBus.Ioc_InputSR1msVdd1Mon);
    Ioc_InputSR1ms_Write_Ioc_InputSR1msVdd2Mon(&Ioc_InputSR1msBus.Ioc_InputSR1msVdd2Mon);
    Ioc_InputSR1ms_Write_Ioc_InputSR1msVdd3Mon(&Ioc_InputSR1msBus.Ioc_InputSR1msVdd3Mon);
    Ioc_InputSR1ms_Write_Ioc_InputSR1msVdd4Mon(&Ioc_InputSR1msBus.Ioc_InputSR1msVdd4Mon);
    Ioc_InputSR1ms_Write_Ioc_InputSR1msVdd5Mon(&Ioc_InputSR1msBus.Ioc_InputSR1msVdd5Mon);
    Ioc_InputSR1ms_Write_Ioc_InputSR1msVddMon(&Ioc_InputSR1msBus.Ioc_InputSR1msVddMon);
    Ioc_InputSR1ms_Write_Ioc_InputSR1msCspMon(&Ioc_InputSR1msBus.Ioc_InputSR1msCspMon);
    Ioc_InputSR1ms_Write_Ioc_InputSR1msInt5vMon(&Ioc_InputSR1msBus.Ioc_InputSR1msInt5vMon);
    Ioc_InputSR1ms_Write_Ioc_InputSR1msBFLMon(&Ioc_InputSR1msBus.Ioc_InputSR1msBFLMon);

    Ioc_InputSR1ms_Timer_Elapsed = STM0_TIM0.U - Ioc_InputSR1ms_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define IOC_INPUTSR1MS_STOP_SEC_CODE
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
