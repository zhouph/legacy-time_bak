/**
 * @defgroup Ioc_InputSR1msConditioning Ioc_InputSR1msConditioning
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_InputSR1msConditioning.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ioc_InputSR1ms.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define FF2_DIRECTION_INPUT 0
#define FF2_DIRECTION_OUTPUT 1
/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define IOC_INPUTSR1MS_START_SEC_CONST_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define IOC_INPUTSR1MS_STOP_SEC_CONST_UNSPECIFIED
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define IOC_INPUTSR1MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

#define IOC_INPUTSR1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_INPUTSR1MS_START_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define IOC_INPUTSR1MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
#define IOC_INPUTSR1MS_START_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define IOC_INPUTSR1MS_STOP_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_INPUTSR1MS_START_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (32BIT)**/


#define IOC_INPUTSR1MS_STOP_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define IOC_INPUTSR1MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define IOC_INPUTSR1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_INPUTSR1MS_START_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define IOC_INPUTSR1MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
#define IOC_INPUTSR1MS_START_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define IOC_INPUTSR1MS_STOP_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_INPUTSR1MS_START_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (32BIT)**/


#define IOC_INPUTSR1MS_STOP_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define IOC_INPUTSR1MS_START_SEC_CODE
#include "Ioc_MemMap.h"

void Ioc_Input1msSRConditioning(void);
void Ioc_Input1msSRConditioningCommon(void);
static Ioc_DataType Ioc_Input1msSRGetVoltage(Ioc_PortType portId, Haluint16 rawVal);
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

void Ioc_Input1msSRConditioning(void)
{
    Ioc_DataType temp = 0;

    /* ADC */
    Ioc_InputSR1msBus.Ioc_InputSR1msPedlSigMonInfo.PdfSigMon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_PDF_SIG_MON , Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigPdtInfo.PdfSigMon);
    Ioc_InputSR1msBus.Ioc_InputSR1msInt5vMon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_5V_INT_MON, Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigPwrInfo.Int5vMon);
	Ioc_InputSR1msBus.Ioc_InputSR1msVdd4Mon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_5V_INT_MON, Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigPwrInfo.Int5vMon);/* IDB CJM ADD */
    Ioc_InputSR1msBus.Ioc_InputSR1msVdd5Mon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_VDD5_MON, Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigPwrInfo.Vdd5Mon);
    Ioc_InputSR1msBus.Ioc_InputSR1msCEMon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_CE_MON, Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigPwrInfo.CEMon);
    Ioc_InputSR1msBus.Ioc_InputSR1msFspAbsMon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_FSP_ABS_MON , Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigFspInfo.FspAbsMon);
    Ioc_InputSR1msBus.Ioc_InputSR1msVddMon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_VDD1_MON, Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigPwrInfo.VddMon);
	Ioc_InputSR1msBus.Ioc_InputSR1msVdd1Mon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_VDD1_MON, Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigPwrInfo.VddMon); /* IDB CJM ADD */
    Ioc_InputSR1msBus.Ioc_InputSR1msVlvMonInfo.SecdCutVlvMon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_VLV_CUTV_S_MON , Ioc_InputSR1msBus.Ioc_InputSR1msHwTrigVlvInfo.VlvCutSMon);
    Ioc_InputSR1msBus.Ioc_InputSR1msVlvMonInfo.PrimCutVlvMon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_VLV_CUTV_P_MON , Ioc_InputSR1msBus.Ioc_InputSR1msHwTrigVlvInfo.VlvCutPMon);
    Ioc_InputSR1msBus.Ioc_InputSR1msVlvMonInfo.VlvCVMon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_VLV_CV_MON, Ioc_InputSR1msBus.Ioc_InputSR1msHwTrigVlvInfo.VlvCVMon);
    Ioc_InputSR1msBus.Ioc_InputSR1msVlvMonInfo.VlvRLVMon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_VLV_RLV_MON, Ioc_InputSR1msBus.Ioc_InputSR1msHwTrigVlvInfo.VlvRLVMon);
    Ioc_InputSR1msBus.Ioc_InputSR1msExt5vMon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_5V_EXT_MON, Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigPwrInfo.Ext5vMon);
	Ioc_InputSR1msBus.Ioc_InputSR1msVdd2Mon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_5V_EXT_MON, Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigPwrInfo.Ext5vMon); /* IDB CJM ADD */
    Ioc_InputSR1msBus.Ioc_InputSR1msVBatt2Mon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_VBATT02_MON, Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigPwrInfo.Vbatt02Mon);
    Ioc_InputSR1msBus.Ioc_InputSR1msFspCbsMon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_FSP_CBS_MON , Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigFspInfo.FspCbsMon);
    Ioc_InputSR1msBus.Ioc_InputSR1msPedlPwrMonInfo.Pdt5vMon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_PDT_5V_MON , Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigPdtInfo.Pdt5vMon);
    Ioc_InputSR1msBus.Ioc_InputSR1msVdd3Mon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_VDD3_MON, Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigPwrInfo.Vdd3Mon);
    Ioc_InputSR1msBus.Ioc_InputSR1msPedlSigMonInfo.PdtSigMon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_PDT_SIG_MON , Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigPdtInfo.PdtSigMon);
    Ioc_InputSR1msBus.Ioc_InputSR1msVlvMonInfo.SimVlvMon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_VLV_SIMV_MON , Ioc_InputSR1msBus.Ioc_InputSR1msHwTrigVlvInfo.VlvSimMon);
    Ioc_InputSR1msBus.Ioc_InputSR1msBFLMon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_BFL_MON, Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigMocInfo.BFLMon);
    Ioc_InputSR1msBus.Ioc_InputSR1msMotMonInfo.MotPwrVoltMon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_MOT_POW_MON , Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigMotInfo.MotPwrMon);
    Ioc_InputSR1msBus.Ioc_InputSR1msMotVoltsMonInfo.MotVoltPhUMon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_MOT_U_OUT_MON , Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigMotInfo.UoutMon);
    Ioc_InputSR1msBus.Ioc_InputSR1msMotVoltsMonInfo.MotVoltPhVMon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_MOT_V_OUT_MON , Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigMotInfo.VoutMon);
    Ioc_InputSR1msBus.Ioc_InputSR1msMotVoltsMonInfo.MotVoltPhWMon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_MOT_W_OUT_MON , Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigMotInfo.WoutMon);
    Ioc_InputSR1msBus.Ioc_InputSR1msMotMonInfo.MotStarMon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_MOT_STAR_MON , Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigMotInfo.StarMon);
    Ioc_InputSR1msBus.Ioc_InputSR1msVBatt1Mon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_VBATT01_MON, Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigPwrInfo.Vbatt01Mon);
    Ioc_InputSR1msBus.Ioc_InputSR1msLineTestMon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_LINE_TEST_MON, Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigTmpInfo.LineTestMon);
    Ioc_InputSR1msBus.Ioc_InputSR1msTempMon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_TEMP_MON, Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigTmpInfo.TempMon);
    Ioc_InputSR1msBus.Ioc_InputSR1msMocMonInfo.RlCurrMainMon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_RL_I_MAIN_MON , Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigMocInfo.RlIMainMon);
    Ioc_InputSR1msBus.Ioc_InputSR1msFspAbsHMon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_FSP_ABS_HMON , Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigFspInfo.FspAbsHMon);
    Ioc_InputSR1msBus.Ioc_InputSR1msMocMonInfo.RrCurrMainMon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_RR_I_MAIN_MON , Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigMocInfo.RrIMainMon);
    Ioc_InputSR1msBus.Ioc_InputSR1msFspCbsHMon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_FSP_CBS_HMON , Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigFspInfo.FspCbsHMon);
    Ioc_InputSR1msBus.Ioc_InputSR1msMocMonInfo.RlMocPosMon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_RL_MOC_POS_MON , Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigMocInfo.RlMocPosMon);
    Ioc_InputSR1msBus.Ioc_InputSR1msMocMonInfo.RlMocNegMon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_RL_MOC_NEG_MON , Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigMocInfo.RlMocNegMon);
    Ioc_InputSR1msBus.Ioc_InputSR1msMocMonInfo.RrMocPosMon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_RR_MOC_POS_MON , Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigMocInfo.RrMocPosMon);
    Ioc_InputSR1msBus.Ioc_InputSR1msMocMonInfo.RrMocNegMon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_RR_MOC_NEG_MON , Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigMocInfo.RrMocNegMon);
    Ioc_InputSR1msBus.Ioc_InputSR1msMocMonInfo.RlCurrSubMon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_RL_I_SUB_MON_FS , Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigMocInfo.RlISubMonFs);
    Ioc_InputSR1msBus.Ioc_InputSR1msPedlPwrMonInfo.Pdf5vMon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_PDF_5V_MON, Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigPdtInfo.Pdf5vMon);
    Ioc_InputSR1msBus.Ioc_InputSR1msMocMonInfo.RrCurrSubMon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_RR_I_SUB_MON_FS , Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigMocInfo.RrISubMonFs);
    Ioc_InputSR1msBus.Ioc_InputSR1msCspMon = Ioc_Input1msSRGetVoltage(IOC_IN_PORT_CSP_MON ,Ioc_InputSR1msBus.Ioc_InputSR1msSwTrigPwrInfo.CSPMon);
}

void Ioc_Input1msSRConditioningCommon(void)
{
	/* Sent */
	Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.McpPresData1      = Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.McpPresData1     ;
	Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.McpPresData2      = Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.McpPresData2     ;
	Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.McpSentCrc        = Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.McpSentCrc       ;
	Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.McpSentData       = Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.McpSentData      ;
	Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.McpSentMsgId      = Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.McpSentMsgId     ;
	Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.McpSentSerialCrc  = Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.McpSentSerialCrc ;
	Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.McpSentConfig     = Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.McpSentConfig    ;
	Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.Wlp1PresData1     = Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.Wlp1PresData1    ;
	Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.Wlp1PresData2     = Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.Wlp1PresData2    ;
	Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.Wlp1SentCrc       = Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.Wlp1SentCrc      ;
	Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.Wlp1SentData      = Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.Wlp1SentData     ;
	Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.Wlp1SentMsgId     = Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.Wlp1SentMsgId    ;
	Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.Wlp1SentSerialCrc = Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.Wlp1SentSerialCrc;
	Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.Wlp1SentConfig    = Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.Wlp1SentConfig   ;
	Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.Wlp2PresData1     = Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.Wlp2PresData1    ;
	Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.Wlp2PresData2     = Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.Wlp2PresData2    ;
	Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.Wlp2SentCrc       = Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.Wlp2SentCrc      ;
	Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.Wlp2SentData      = Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.Wlp2SentData     ;
	Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.Wlp2SentMsgId     = Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.Wlp2SentMsgId    ;
	Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.Wlp2SentSerialCrc = Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.Wlp2SentSerialCrc;
	Ioc_InputSR1msBus.Ioc_InputSR1msHalPressureInfo.Wlp2SentConfig    = Ioc_InputSR1msBus.Ioc_InputSR1msSentHPressureInfo.Wlp2SentConfig   ;
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static Ioc_DataType Ioc_Input1msSRGetVoltage(Ioc_PortType portId, Haluint16 rawVal)
{
     Ioc_DataType retVal = 0;
     Ioc_MulFactorType mulFactor = Ioc_RoutingConfig[portId].MulFactor;
     Ioc_DivFactorType divFactor = Ioc_RoutingConfig[portId].DivFactor;
     
     retVal = ((Haluint32)rawVal * mulFactor)/ divFactor;

     return retVal;
}

#define IOC_INPUTSR1MS_STOP_SEC_CODE
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
