/**
 * @defgroup Ioc_OutputCS50us Ioc_OutputCS50us
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_OutputCS50us.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ioc_OutputCS50us.h"
#include "Ioc_OutputCS50us_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define IOC_OUTPUTCS50US_START_SEC_CONST_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define IOC_OUTPUTCS50US_STOP_SEC_CONST_UNSPECIFIED
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define IOC_OUTPUTCS50US_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Ioc_OutputCS50us_HdrBusType Ioc_OutputCS50usBus;

/* Version Info */
const SwcVersionInfo_t Ioc_OutputCS50usVersionInfo = 
{   
    IOC_OUTPUTCS50US_MODULE_ID,           /* Ioc_OutputCS50usVersionInfo.ModuleId */
    IOC_OUTPUTCS50US_MAJOR_VERSION,       /* Ioc_OutputCS50usVersionInfo.MajorVer */
    IOC_OUTPUTCS50US_MINOR_VERSION,       /* Ioc_OutputCS50usVersionInfo.MinorVer */
    IOC_OUTPUTCS50US_PATCH_VERSION,       /* Ioc_OutputCS50usVersionInfo.PatchVer */
    IOC_OUTPUTCS50US_BRANCH_VERSION       /* Ioc_OutputCS50usVersionInfo.BranchVer */
};
    
/* Input Data Element */
Acmio_ActrMotPwmDataInfo_t Ioc_OutputCS50usMotPwmDataInfo;
Mom_HndlrEcuModeSts_t Ioc_OutputCS50usEcuModeSts;
Eem_SuspcDetnFuncInhibitIocSts_t Ioc_OutputCS50usFuncInhibitIocSts;

/* Output Data Element */

uint32 Ioc_OutputCS50us_Timer_Start;
uint32 Ioc_OutputCS50us_Timer_Elapsed;

#define IOC_OUTPUTCS50US_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS50US_START_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define IOC_OUTPUTCS50US_STOP_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS50US_START_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define IOC_OUTPUTCS50US_STOP_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS50US_START_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (32BIT)**/


#define IOC_OUTPUTCS50US_STOP_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define IOC_OUTPUTCS50US_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define IOC_OUTPUTCS50US_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS50US_START_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define IOC_OUTPUTCS50US_STOP_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS50US_START_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define IOC_OUTPUTCS50US_STOP_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS50US_START_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (32BIT)**/


#define IOC_OUTPUTCS50US_STOP_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define IOC_OUTPUTCS50US_START_SEC_CODE
#include "Ioc_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Ioc_OutputCS50us_Init(void)
{
    /* Initialize internal bus */
    Ioc_OutputCS50usBus.Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhUhighData = 0;
    Ioc_OutputCS50usBus.Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhVhighData = 0;
    Ioc_OutputCS50usBus.Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhWhighData = 0;
    Ioc_OutputCS50usBus.Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhUlowData = 0;
    Ioc_OutputCS50usBus.Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhVlowData = 0;
    Ioc_OutputCS50usBus.Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhWlowData = 0;
    Ioc_OutputCS50usBus.Ioc_OutputCS50usEcuModeSts = 0;
    Ioc_OutputCS50usBus.Ioc_OutputCS50usFuncInhibitIocSts = 0;
}

void Ioc_OutputCS50us(void)
{
    Ioc_OutputCS50us_Timer_Start = STM0_TIM0.U;

    /* Input */
    Ioc_OutputCS50us_Read_Ioc_OutputCS50usMotPwmDataInfo(&Ioc_OutputCS50usBus.Ioc_OutputCS50usMotPwmDataInfo);
    /*==============================================================================
    * Members of structure Ioc_OutputCS50usMotPwmDataInfo 
     : Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhUhighData;
     : Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhVhighData;
     : Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhWhighData;
     : Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhUlowData;
     : Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhVlowData;
     : Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhWlowData;
     =============================================================================*/
    
    Ioc_OutputCS50us_Read_Ioc_OutputCS50usEcuModeSts(&Ioc_OutputCS50usBus.Ioc_OutputCS50usEcuModeSts);
    Ioc_OutputCS50us_Read_Ioc_OutputCS50usFuncInhibitIocSts(&Ioc_OutputCS50usBus.Ioc_OutputCS50usFuncInhibitIocSts);

    /* Process */
    Ioc_Output50usConditioning();

    /* Output */

    Ioc_OutputCS50us_Timer_Elapsed = STM0_TIM0.U - Ioc_OutputCS50us_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define IOC_OUTPUTCS50US_STOP_SEC_CODE
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
