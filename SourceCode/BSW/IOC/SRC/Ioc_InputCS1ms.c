/**
 * @defgroup Ioc_InputCS1ms Ioc_InputCS1ms
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_InputCS1ms.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ioc_InputCS1ms.h"
#include "Ioc_InputCS1ms_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define IOC_INPUTCS1MS_START_SEC_CONST_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define IOC_INPUTCS1MS_STOP_SEC_CONST_UNSPECIFIED
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define IOC_INPUTCS1MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Ioc_InputCS1ms_HdrBusType Ioc_InputCS1msBus;

/* Version Info */
const SwcVersionInfo_t Ioc_InputCS1msVersionInfo = 
{   
    IOC_INPUTCS1MS_MODULE_ID,           /* Ioc_InputCS1msVersionInfo.ModuleId */
    IOC_INPUTCS1MS_MAJOR_VERSION,       /* Ioc_InputCS1msVersionInfo.MajorVer */
    IOC_INPUTCS1MS_MINOR_VERSION,       /* Ioc_InputCS1msVersionInfo.MinorVer */
    IOC_INPUTCS1MS_PATCH_VERSION,       /* Ioc_InputCS1msVersionInfo.PatchVer */
    IOC_INPUTCS1MS_BRANCH_VERSION       /* Ioc_InputCS1msVersionInfo.BranchVer */
};
    
/* Input Data Element */
Mom_HndlrEcuModeSts_t Ioc_InputCS1msEcuModeSts;
Acm_MainAcmAsicInitCompleteFlag_t Ioc_InputCS1msAcmAsicInitCompleteFlag;
Eem_SuspcDetnFuncInhibitIocSts_t Ioc_InputCS1msFuncInhibitIocSts;

/* Output Data Element */
Ioc_InputCS1msSwtMonInfo_t Ioc_InputCS1msSwtMonInfo;
Ioc_InputCS1msSwtMonInfoEsc_t Ioc_InputCS1msSwtMonInfoEsc;
Ioc_InputCS1msRlyMonInfo_t Ioc_InputCS1msRlyMonInfo;
Ioc_InputCS1msSwLineMonInfo_t Ioc_InputCS1msSwLineMonInfo;
Ioc_InputCS1msRsmDbcMon_t Ioc_InputCS1msRsmDbcMon;
Ioc_InputCS1msPbcVdaAi_t Ioc_InputCS1msPbcVdaAi;

uint32 Ioc_InputCS1ms_Timer_Start;
uint32 Ioc_InputCS1ms_Timer_Elapsed;

#define IOC_INPUTCS1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_INPUTCS1MS_START_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define IOC_INPUTCS1MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
#define IOC_INPUTCS1MS_START_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define IOC_INPUTCS1MS_STOP_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_INPUTCS1MS_START_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (32BIT)**/


#define IOC_INPUTCS1MS_STOP_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define IOC_INPUTCS1MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define IOC_INPUTCS1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_INPUTCS1MS_START_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define IOC_INPUTCS1MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
#define IOC_INPUTCS1MS_START_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define IOC_INPUTCS1MS_STOP_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_INPUTCS1MS_START_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (32BIT)**/


#define IOC_INPUTCS1MS_STOP_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define IOC_INPUTCS1MS_START_SEC_CODE
#include "Ioc_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Ioc_InputCS1ms_Init(void)
{
    /* Initialize internal bus */
    Ioc_InputCS1msBus.Ioc_InputCS1msEcuModeSts = 0;
    Ioc_InputCS1msBus.Ioc_InputCS1msAcmAsicInitCompleteFlag = 0;
    Ioc_InputCS1msBus.Ioc_InputCS1msFuncInhibitIocSts = 0;
    Ioc_InputCS1msBus.Ioc_InputCS1msSwtMonInfo.AvhSwtMon = 0;
    Ioc_InputCS1msBus.Ioc_InputCS1msSwtMonInfo.BflSwtMon = 0;
    Ioc_InputCS1msBus.Ioc_InputCS1msSwtMonInfo.BlsSwtMon = 0;
    Ioc_InputCS1msBus.Ioc_InputCS1msSwtMonInfo.BsSwtMon = 0;
    Ioc_InputCS1msBus.Ioc_InputCS1msSwtMonInfo.DoorSwtMon = 0;
    Ioc_InputCS1msBus.Ioc_InputCS1msSwtMonInfo.EscSwtMon = 0;
    Ioc_InputCS1msBus.Ioc_InputCS1msSwtMonInfo.FlexBrkASwtMon = 0;
    Ioc_InputCS1msBus.Ioc_InputCS1msSwtMonInfo.FlexBrkBSwtMon = 0;
    Ioc_InputCS1msBus.Ioc_InputCS1msSwtMonInfo.HzrdSwtMon = 0;
    Ioc_InputCS1msBus.Ioc_InputCS1msSwtMonInfo.HdcSwtMon = 0;
    Ioc_InputCS1msBus.Ioc_InputCS1msSwtMonInfo.PbSwtMon = 0;
    Ioc_InputCS1msBus.Ioc_InputCS1msSwtMonInfoEsc.BlsSwtMon_Esc = 0;
    Ioc_InputCS1msBus.Ioc_InputCS1msSwtMonInfoEsc.AvhSwtMon_Esc = 0;
    Ioc_InputCS1msBus.Ioc_InputCS1msSwtMonInfoEsc.EscSwtMon_Esc = 0;
    Ioc_InputCS1msBus.Ioc_InputCS1msSwtMonInfoEsc.HdcSwtMon_Esc = 0;
    Ioc_InputCS1msBus.Ioc_InputCS1msSwtMonInfoEsc.PbSwtMon_Esc = 0;
    Ioc_InputCS1msBus.Ioc_InputCS1msSwtMonInfoEsc.GearRSwtMon_Esc = 0;
    Ioc_InputCS1msBus.Ioc_InputCS1msSwtMonInfoEsc.BlfSwtMon_Esc = 0;
    Ioc_InputCS1msBus.Ioc_InputCS1msSwtMonInfoEsc.ClutchSwtMon_Esc = 0;
    Ioc_InputCS1msBus.Ioc_InputCS1msSwtMonInfoEsc.ItpmsSwtMon_Esc = 0;
    Ioc_InputCS1msBus.Ioc_InputCS1msRlyMonInfo.RlyDbcMon = 0;
    Ioc_InputCS1msBus.Ioc_InputCS1msRlyMonInfo.RlyEssMon = 0;
    Ioc_InputCS1msBus.Ioc_InputCS1msRlyMonInfo.RlyFault = 0;
    Ioc_InputCS1msBus.Ioc_InputCS1msSwLineMonInfo.Sw1LineCMon = 0;
    Ioc_InputCS1msBus.Ioc_InputCS1msSwLineMonInfo.Sw2LineBMon = 0;
    Ioc_InputCS1msBus.Ioc_InputCS1msSwLineMonInfo.Sw3LineEMon = 0;
    Ioc_InputCS1msBus.Ioc_InputCS1msSwLineMonInfo.Sw4LineFMon = 0;
    Ioc_InputCS1msBus.Ioc_InputCS1msRsmDbcMon = 0;
    Ioc_InputCS1msBus.Ioc_InputCS1msPbcVdaAi = 0;
}

void Ioc_InputCS1ms(void)
{
    Ioc_InputCS1ms_Timer_Start = STM0_TIM0.U;

    /* Input */
    Ioc_InputCS1ms_Read_Ioc_InputCS1msEcuModeSts(&Ioc_InputCS1msBus.Ioc_InputCS1msEcuModeSts);
    Ioc_InputCS1ms_Read_Ioc_InputCS1msAcmAsicInitCompleteFlag(&Ioc_InputCS1msBus.Ioc_InputCS1msAcmAsicInitCompleteFlag);
    Ioc_InputCS1ms_Read_Ioc_InputCS1msFuncInhibitIocSts(&Ioc_InputCS1msBus.Ioc_InputCS1msFuncInhibitIocSts);

    /* Process */
    Ioc_Input1msCSConditioning();     /* For IDB */

    /* Output */
    Ioc_InputCS1ms_Write_Ioc_InputCS1msSwtMonInfo(&Ioc_InputCS1msBus.Ioc_InputCS1msSwtMonInfo);
    /*==============================================================================
    * Members of structure Ioc_InputCS1msSwtMonInfo 
     : Ioc_InputCS1msSwtMonInfo.AvhSwtMon;
     : Ioc_InputCS1msSwtMonInfo.BflSwtMon;
     : Ioc_InputCS1msSwtMonInfo.BlsSwtMon;
     : Ioc_InputCS1msSwtMonInfo.BsSwtMon;
     : Ioc_InputCS1msSwtMonInfo.DoorSwtMon;
     : Ioc_InputCS1msSwtMonInfo.EscSwtMon;
     : Ioc_InputCS1msSwtMonInfo.FlexBrkASwtMon;
     : Ioc_InputCS1msSwtMonInfo.FlexBrkBSwtMon;
     : Ioc_InputCS1msSwtMonInfo.HzrdSwtMon;
     : Ioc_InputCS1msSwtMonInfo.HdcSwtMon;
     : Ioc_InputCS1msSwtMonInfo.PbSwtMon;
     =============================================================================*/
    
    Ioc_InputCS1ms_Write_Ioc_InputCS1msSwtMonInfoEsc(&Ioc_InputCS1msBus.Ioc_InputCS1msSwtMonInfoEsc);
    /*==============================================================================
    * Members of structure Ioc_InputCS1msSwtMonInfoEsc 
     : Ioc_InputCS1msSwtMonInfoEsc.BlsSwtMon_Esc;
     : Ioc_InputCS1msSwtMonInfoEsc.AvhSwtMon_Esc;
     : Ioc_InputCS1msSwtMonInfoEsc.EscSwtMon_Esc;
     : Ioc_InputCS1msSwtMonInfoEsc.HdcSwtMon_Esc;
     : Ioc_InputCS1msSwtMonInfoEsc.PbSwtMon_Esc;
     : Ioc_InputCS1msSwtMonInfoEsc.GearRSwtMon_Esc;
     : Ioc_InputCS1msSwtMonInfoEsc.BlfSwtMon_Esc;
     : Ioc_InputCS1msSwtMonInfoEsc.ClutchSwtMon_Esc;
     : Ioc_InputCS1msSwtMonInfoEsc.ItpmsSwtMon_Esc;
     =============================================================================*/
    
    Ioc_InputCS1ms_Write_Ioc_InputCS1msRlyMonInfo(&Ioc_InputCS1msBus.Ioc_InputCS1msRlyMonInfo);
    /*==============================================================================
    * Members of structure Ioc_InputCS1msRlyMonInfo 
     : Ioc_InputCS1msRlyMonInfo.RlyDbcMon;
     : Ioc_InputCS1msRlyMonInfo.RlyEssMon;
     : Ioc_InputCS1msRlyMonInfo.RlyFault;
     =============================================================================*/
    
    Ioc_InputCS1ms_Write_Ioc_InputCS1msSwLineMonInfo(&Ioc_InputCS1msBus.Ioc_InputCS1msSwLineMonInfo);
    /*==============================================================================
    * Members of structure Ioc_InputCS1msSwLineMonInfo 
     : Ioc_InputCS1msSwLineMonInfo.Sw1LineCMon;
     : Ioc_InputCS1msSwLineMonInfo.Sw2LineBMon;
     : Ioc_InputCS1msSwLineMonInfo.Sw3LineEMon;
     : Ioc_InputCS1msSwLineMonInfo.Sw4LineFMon;
     =============================================================================*/
    
    Ioc_InputCS1ms_Write_Ioc_InputCS1msRsmDbcMon(&Ioc_InputCS1msBus.Ioc_InputCS1msRsmDbcMon);
    Ioc_InputCS1ms_Write_Ioc_InputCS1msPbcVdaAi(&Ioc_InputCS1msBus.Ioc_InputCS1msPbcVdaAi);

    Ioc_InputCS1ms_Timer_Elapsed = STM0_TIM0.U - Ioc_InputCS1ms_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define IOC_INPUTCS1MS_STOP_SEC_CODE
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
