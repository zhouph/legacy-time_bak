/**
 * @defgroup Ioc_OutputSR1msConditioning Ioc_OutputSR1msConditioning
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_OutputSR1msConditioning.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ioc_OutputSR1ms.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define FF2_DIRECTION_INPUT 0
#define FF2_DIRECTION_OUTPUT 1
/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define IOC_OUTPUTSR1MS_START_SEC_CONST_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define IOC_OUTPUTSR1MS_STOP_SEC_CONST_UNSPECIFIED
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define IOC_OUTPUTSR1MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

#define IOC_OUTPUTSR1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_OUTPUTSR1MS_START_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define IOC_OUTPUTSR1MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
#define IOC_OUTPUTSR1MS_START_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define IOC_OUTPUTSR1MS_STOP_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_OUTPUTSR1MS_START_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (32BIT)**/


#define IOC_OUTPUTSR1MS_STOP_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define IOC_OUTPUTSR1MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

static uint16 Ioc_ValveChInputCmd[IOC_VALVE_MAX_NUM];
static uint16 Ioc_ValveChInputCmdBef[IOC_VALVE_MAX_NUM];        /* To compare with the last command */
static uint16 Ioc_ValveChOutputCmd[IOC_VALVE_MAX_NUM];
static uint8  Ioc_ValveChFlg[IOC_VALVE_MAX_NUM];

static uint16 Ioc_ValveChInputCmd_New[9];
static uint16 Ioc_ValveChInputCmdBef_New[9];        /* To compare with the last command */
static uint16 Ioc_ValveChOutputCmd_New[9];
static uint8  Ioc_ValveChFlg_New[9];

#define IOC_OUTPUTSR1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_OUTPUTSR1MS_START_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define IOC_OUTPUTSR1MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
#define IOC_OUTPUTSR1MS_START_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define IOC_OUTPUTSR1MS_STOP_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_OUTPUTSR1MS_START_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (32BIT)**/


#define IOC_OUTPUTSR1MS_STOP_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define IOC_OUTPUTSR1MS_START_SEC_CODE
#include "Ioc_MemMap.h"

void Ioc_OutputSR1msConditioning(void);

static void Ioc_OutputSR1msSetSpiDuty(Ioc_PortType portId, Haluint16 inDuty, Haluint32* outDuty);
static void Ioc_OutputSR1msSetSpiLevel(Haluint16 inData, Haluint32* outData);
static void Ioc_OutputSR1msVlvCtrl100Asic(void);
static void Ioc_OutputSR1msDcMtrCtrl100Asic(void);
static void Ioc_OutputSR1msFSRCtrl100Asic(void);
static void Ioc_OutputSR1msVlvCtrl100Asic(void);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Ioc_OutputSR1msConditioning(void)
{  
    Saluint16 temp = 0;

    /* SPI */
    Ioc_OutputSR1msVlvCtrl100Asic();    /* Valve Control for 100 ASIC */
    Ioc_OutputSR1msDcMtrCtrl100Asic();  /* DC Motor Control for 100 ASIC */
    Ioc_OutputSR1msFSRCtrl100Asic();    /* FSR Control for 100 ASIC*/

#if 0 	/* SWD VARIANT */
    /* SPI */
    Ioc_OutputSR1msSetSpiDuty(IOC_OUT_PORT_VLV_FLNC , Ioc_OutputSR1msBus.Ioc_OutputSR1msWhlVlvDrvInfo.FlOvDrvData, &Ioc_OutputSR1msBus.Ioc_OutputSR1msSpiTxAsicActInfo.Lsd5dutycycle);
    Ioc_OutputSR1msSetSpiDuty(IOC_OUT_PORT_VLV_FLNO , Ioc_OutputSR1msBus.Ioc_OutputSR1msWhlVlvDrvInfo.FlIvDrvData, &Ioc_OutputSR1msBus.Ioc_OutputSR1msSpiTxAsicActInfo.Lsd1dutycycle);
    Ioc_OutputSR1msSetSpiDuty(IOC_OUT_PORT_VLV_FRNC , Ioc_OutputSR1msBus.Ioc_OutputSR1msWhlVlvDrvInfo.FrOvDrvData, &Ioc_OutputSR1msBus.Ioc_OutputSR1msSpiTxAsicActInfo.Lsd8dutycycle);
    Ioc_OutputSR1msSetSpiDuty(IOC_OUT_PORT_VLV_FRNO , Ioc_OutputSR1msBus.Ioc_OutputSR1msWhlVlvDrvInfo.FrIvDrvData, &Ioc_OutputSR1msBus.Ioc_OutputSR1msSpiTxAsicActInfo.Lsd4dutycycle);
    Ioc_OutputSR1msSetSpiDuty(IOC_OUT_PORT_VLV_RLNC , Ioc_OutputSR1msBus.Ioc_OutputSR1msWhlVlvDrvInfo.RlOvDrvData, &Ioc_OutputSR1msBus.Ioc_OutputSR1msSpiTxAsicActInfo.Lsd7dutycycle);
    Ioc_OutputSR1msSetSpiDuty(IOC_OUT_PORT_VLV_RLNO , Ioc_OutputSR1msBus.Ioc_OutputSR1msWhlVlvDrvInfo.RlIvDrvData, &Ioc_OutputSR1msBus.Ioc_OutputSR1msSpiTxAsicActInfo.Lsd3dutycycle);
    Ioc_OutputSR1msSetSpiDuty(IOC_OUT_PORT_VLV_RRNC , Ioc_OutputSR1msBus.Ioc_OutputSR1msWhlVlvDrvInfo.RrOvDrvData, &Ioc_OutputSR1msBus.Ioc_OutputSR1msSpiTxAsicActInfo.Lsd6dutycycle);
    Ioc_OutputSR1msSetSpiDuty(IOC_OUT_PORT_VLV_RRNO , Ioc_OutputSR1msBus.Ioc_OutputSR1msWhlVlvDrvInfo.RrIvDrvData, &Ioc_OutputSR1msBus.Ioc_OutputSR1msSpiTxAsicActInfo.Lsd2dutycycle);
    Ioc_OutputSR1msSetSpiLevel(Ioc_OutputSR1msBus.Ioc_OutputSR1msFsrCbsDrvInfo.FsrCbsDrv, &Ioc_OutputSR1msBus.Ioc_OutputSR1msSpiTxAsicActInfo.PdOn);
    Ioc_OutputSR1msSetSpiLevel(Ioc_OutputSR1msBus.Ioc_OutputSR1msFsrAbsDrvInfo.FsrAbsDrv, &Ioc_OutputSR1msBus.Ioc_OutputSR1msSpiTxAsicActInfo.HdOn);

    Ioc_OutputSR1msVlvCtrl100Asic();    /* Valve Control for 100 ASIC */
    Ioc_OutputSR1msDcMtrCtrl100Asic();  /* DC Motor Control for 100 ASIC */
    Ioc_OutputSR1msFSRCtrl100Asic();    /* FSR Control for 100 ASIC*/
#endif

}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static void Ioc_OutputSR1msSetSpiDuty(Ioc_PortType portId, Haluint16 inDuty, Haluint32* outDuty)
{
    Ioc_MulFactorType mulFactor = Ioc_RoutingConfig[portId].MulFactor;
    Ioc_DivFactorType divFactor = Ioc_RoutingConfig[portId].DivFactor;


    *outDuty  = ((Haluint32)inDuty * mulFactor) / divFactor ;
}

static void Ioc_OutputSR1msSetSpiLevel(Haluint16 inData, Haluint32* outData)
{
}

static void Ioc_OutputSR1msVlvCtrl100Asic(void)
{
    uint8 i = 0;

    for(i = 0 ; i < IOC_VALVE_MAX_NUM ; i++)
    {
        /* Copy Input Command */
        if(Asic100ValveMappingConfig[i] == Asic100_FLIV)
        {
            Ioc_ValveChInputCmd[i] = Ioc_OutputSR1msBus.Ioc_OutputSR1msWhlVlvDrvInfo.FlIvDrvData;
        }
        else if(Asic100ValveMappingConfig[i] == Asic100_FRIV)
        {
            Ioc_ValveChInputCmd[i] = Ioc_OutputSR1msBus.Ioc_OutputSR1msWhlVlvDrvInfo.FrIvDrvData;
        }
        else if(Asic100ValveMappingConfig[i] == Asic100_RLIV)
        {
            Ioc_ValveChInputCmd[i] = Ioc_OutputSR1msBus.Ioc_OutputSR1msWhlVlvDrvInfo.RlIvDrvData;
        }
        else if(Asic100ValveMappingConfig[i] == Asic100_RRIV)
        {
            Ioc_ValveChInputCmd[i] = Ioc_OutputSR1msBus.Ioc_OutputSR1msWhlVlvDrvInfo.RrIvDrvData;
        }
        else if(Asic100ValveMappingConfig[i] == Asic100_FLOV)
        {
            Ioc_ValveChInputCmd[i] = Ioc_OutputSR1msBus.Ioc_OutputSR1msWhlVlvDrvInfo.FlOvDrvData;
        }
        else if(Asic100ValveMappingConfig[i] == Asic100_FROV)
        {
            Ioc_ValveChInputCmd[i] = Ioc_OutputSR1msBus.Ioc_OutputSR1msWhlVlvDrvInfo.FrOvDrvData;
        }
        else if(Asic100ValveMappingConfig[i] == Asic100_RLOV)
        {
            Ioc_ValveChInputCmd[i] = Ioc_OutputSR1msBus.Ioc_OutputSR1msWhlVlvDrvInfo.RlOvDrvData;
        }
        else if(Asic100ValveMappingConfig[i] == Asic100_RROV)
        {
            Ioc_ValveChInputCmd[i] = Ioc_OutputSR1msBus.Ioc_OutputSR1msWhlVlvDrvInfo.RrOvDrvData;
        }
        else if(Asic100ValveMappingConfig[i] == Asic100_SBAL) /*NZ Quick-C: PD*/
        {
            Ioc_ValveChInputCmd[i] = Ioc_OutputSR1msBus.Ioc_OutputSR1msBalVlvDrvInfo.PressDumpVlvDrvData;
        }
        else if(Asic100ValveMappingConfig[i] == Asic100_CBAL)
        {
            Ioc_ValveChInputCmd[i] = Ioc_OutputSR1msBus.Ioc_OutputSR1msBalVlvDrvInfo.BalVlvDrvData;
        }
        else
        {
            Ioc_ValveChInputCmd[i] = 0;
        }
                
        Ioc_ValveChFlg[i] = 0;                                        /* Clear Valve Command Flag */
        if(Ioc_ValveChInputCmd[i] != Ioc_ValveChInputCmdBef[i])       /* Set Valve Command when the duty has changed */
        {
            Ioc_ValveChFlg[i] = 1;
        }
        Ioc_ValveChOutputCmd[i] = (uint16)(((uint32)Ioc_ValveChInputCmd[i] * Ioc_ValveRoutingConfig[i].MulFactor)/Ioc_ValveRoutingConfig[i].DivFactor);
        Ioc_ValveChInputCmdBef[i]  = Ioc_ValveChInputCmd[i];  /* Save command value to compare with the before value later */
    }

    /* Copy Output Command */
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNo0Data.VlvNo0DataFlg            = Ioc_ValveChFlg[IOC_VAVLE_NO_CH0];
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNo1Data.VlvNo1DataFlg            = Ioc_ValveChFlg[IOC_VAVLE_NO_CH1];
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNo2Data.VlvNo2DataFlg            = Ioc_ValveChFlg[IOC_VAVLE_NO_CH2];
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNo3Data.VlvNo3DataFlg            = Ioc_ValveChFlg[IOC_VAVLE_NO_CH3];
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvTc0Data.VlvTc0DataFlg            = Ioc_ValveChFlg[IOC_VAVLE_TC_CH0];
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvTc1Data.VlvTc1DataFlg            = Ioc_ValveChFlg[IOC_VAVLE_TC_CH1];
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvEsv0Data.VlvEsv0DataFlg          = Ioc_ValveChFlg[IOC_VAVLE_ESV_CH0];
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvEsv1Data.VlvEsv1DataFlg          = Ioc_ValveChFlg[IOC_VAVLE_ESV_CH1];
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNc0Data.VlvNc0DataFlg            = Ioc_ValveChFlg[IOC_VAVLE_NC_CH0];
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNc1Data.VlvNc1DataFlg            = Ioc_ValveChFlg[IOC_VAVLE_NC_CH1];
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNc2Data.VlvNc2DataFlg            = Ioc_ValveChFlg[IOC_VAVLE_NC_CH2];
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNc3Data.VlvNc3DataFlg            = Ioc_ValveChFlg[IOC_VAVLE_NC_CH3];
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNo0Data.ACH_TxValve_SET_POINT    = Ioc_ValveChOutputCmd[IOC_VAVLE_NO_CH0];
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNo1Data.ACH_TxValve_SET_POINT    = Ioc_ValveChOutputCmd[IOC_VAVLE_NO_CH1];
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNo2Data.ACH_TxValve_SET_POINT    = Ioc_ValveChOutputCmd[IOC_VAVLE_NO_CH2];
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNo3Data.ACH_TxValve_SET_POINT    = Ioc_ValveChOutputCmd[IOC_VAVLE_NO_CH3];
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvTc0Data.ACH_TxValve_SET_POINT    = Ioc_ValveChOutputCmd[IOC_VAVLE_TC_CH0];
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvTc1Data.ACH_TxValve_SET_POINT    = Ioc_ValveChOutputCmd[IOC_VAVLE_TC_CH1];
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvEsv0Data.ACH_TxValve_SET_POINT   = Ioc_ValveChOutputCmd[IOC_VAVLE_ESV_CH0];
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvEsv1Data.ACH_TxValve_SET_POINT   = Ioc_ValveChOutputCmd[IOC_VAVLE_ESV_CH1];
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNc0Data.ACH_TxValve_SET_POINT    = Ioc_ValveChOutputCmd[IOC_VAVLE_NC_CH0];
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNc1Data.ACH_TxValve_SET_POINT    = Ioc_ValveChOutputCmd[IOC_VAVLE_NC_CH1];
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNc2Data.ACH_TxValve_SET_POINT    = Ioc_ValveChOutputCmd[IOC_VAVLE_NC_CH2];
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNc3Data.ACH_TxValve_SET_POINT    = Ioc_ValveChOutputCmd[IOC_VAVLE_NC_CH3];
}
static void Ioc_OutputSR1msDcMtrCtrl100Asic(void)
{
    uint8 MtrCmdFlgTemp = 0;
    uint16 MtrCmdDataTemp = 0;

    if(Ioc_OutputSR1msBus.Ioc_OutputSR1msAcmAsicInitCompleteFlag == 1)
    {
        /* 
        if(Asic100DriverRoutingConfig.PumpMotorPreDriver == Asic100_DcMotor)
        {
            MtrCmdFlgTemp = Ioc_OutputSR1msBus.Ioc_OutputSR1msDcMtrDutyData.MotorDutyDataFlg;
            MtrCmdDataTemp = Ioc_OutputSR1msBus.Ioc_OutputSR1msDcMtrDutyData.MotorDuty;
        }
        else 
        */if(Asic100DriverRoutingConfig.PumpMotorPreDriver == Asic100_FSR_ABS)
        {
            if(Ioc_OutputSR1msBus.Ioc_OutputSR1msFsrAbsDrvInfo.FsrAbsDrv == 0)
            {
                MtrCmdFlgTemp = 1;
                MtrCmdDataTemp = 0;
            }
            else    /* FSR ABS is ON */
            {
                MtrCmdFlgTemp = 1;
                MtrCmdDataTemp = Ioc_MotorRoutingConfig[IOC_MOTOR_ABS].DivFactor;   /* to Send 100% data */
            }
        }
        else if(Asic100DriverRoutingConfig.PumpMotorPreDriver == Asic100_FSR_CBS)
        {
            if(Ioc_OutputSR1msBus.Ioc_OutputSR1msFsrCbsDrvInfo.FsrCbsDrv == 0)
            {
                MtrCmdFlgTemp = 1;
                MtrCmdDataTemp = 0;
            }
            else    /* FSR CBS is ON */
            {
                MtrCmdFlgTemp = 1;
                MtrCmdDataTemp = Ioc_MotorRoutingConfig[IOC_MOTOR_ABS].DivFactor;   /* to Send 100% data */
            }
        }
        else
        {
            ;
        }
        
        if(MtrCmdFlgTemp == 1)
        {
            Ioc_OutputSR1msBus.Ioc_OutputSR1msIocDcMtrDutyData.MtrDutyDataFlg = 1;
        }
    	else
        {
            Ioc_OutputSR1msBus.Ioc_OutputSR1msIocDcMtrDutyData.MtrDutyDataFlg = 0;
        }
        Ioc_OutputSR1msBus.Ioc_OutputSR1msIocDcMtrDutyData.ACH_Tx8_PUMP_DTY_PWM = (uint8)(((uint32)MtrCmdDataTemp *  \
        Ioc_MotorRoutingConfig[IOC_MOTOR_ABS].MulFactor)/Ioc_MotorRoutingConfig[IOC_MOTOR_ABS].DivFactor);
    }
}

static void Ioc_OutputSR1msFSRCtrl100Asic(void)
{
    uint8 FsrCmdTemp = 0;
    
    if(Ioc_OutputSR1msBus.Ioc_OutputSR1msAcmAsicInitCompleteFlag == 1)
    {
        if(Asic100DriverRoutingConfig.FailSafePreDriver == Asic100_FSR_ABS)
        {
            FsrCmdTemp = Ioc_OutputSR1msBus.Ioc_OutputSR1msFsrAbsDrvInfo.FsrAbsDrv;
        }
        else if(Asic100DriverRoutingConfig.FailSafePreDriver == Asic100_FSR_CBS)
        {
            FsrCmdTemp = Ioc_OutputSR1msBus.Ioc_OutputSR1msFsrCbsDrvInfo.FsrCbsDrv;
        }
        else
        {
            ;
        }
        if( FsrCmdTemp == 0)
        {
            Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvRelayData.VlvRelayDataFlg = 1;
            Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvRelayData.ACH_Tx1_FS_CMD = 0;
        }
        else
        {
            Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvRelayData.VlvRelayDataFlg = 1;
            Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvRelayData.ACH_Tx1_FS_CMD = 1;
        }
    }
}

#define IOC_OUTPUTSR1MS_STOP_SEC_CODE
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
