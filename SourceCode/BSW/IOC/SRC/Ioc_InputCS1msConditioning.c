/**
 * @defgroup Ioc_InputCS1msConditioning Ioc_InputCS1msConditioning
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_InputCS1msConditioning.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ioc_InputCS1ms.h"
#include "Dio.h"
#include "Pwm_17_Gtm.h"
#include "Port.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define FF2_DIRECTION_INPUT 0
#define FF2_DIRECTION_OUTPUT 1
/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define IOC_INPUTCS1MS_START_SEC_CONST_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define IOC_INPUTCS1MS_STOP_SEC_CONST_UNSPECIFIED
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define IOC_INPUTCS1MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

#define IOC_INPUTCS1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_INPUTCS1MS_START_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define IOC_INPUTCS1MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
#define IOC_INPUTCS1MS_START_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define IOC_INPUTCS1MS_STOP_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_INPUTCS1MS_START_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (32BIT)**/


#define IOC_INPUTCS1MS_STOP_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define IOC_INPUTCS1MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define IOC_INPUTCS1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_INPUTCS1MS_START_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define IOC_INPUTCS1MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
#define IOC_INPUTCS1MS_START_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define IOC_INPUTCS1MS_STOP_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_INPUTCS1MS_START_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (32BIT)**/


#define IOC_INPUTCS1MS_STOP_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define IOC_INPUTCS1MS_START_SEC_CODE
#include "Ioc_MemMap.h"

static Dio_LevelType Ioc_Input1msCs_Dio_ReadChannel(Dio_ChannelType ChannelId);

void Ioc_Input1msCSConditioning(void);

inline boolean Dio_1msReadChannel_Direct(Ifx_P *port, uint8 pinIndex);
inline void Dio_1msWriteChannel_Direct(Ifx_P *port, uint8 pinIndex, Dio_LevelType Level);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Ioc_Input1msCSConditioning(void)
{
    /* DIO */
    Ioc_InputCS1msBus.Ioc_InputCS1msSwtMonInfo.AvhSwtMon        = Dio_1msReadChannel_Direct(&MODULE_P00, 10);    /* IOC_IN_PORT_AVH_SW_MON       */
    Ioc_InputCS1msBus.Ioc_InputCS1msSwtMonInfo.PbSwtMon         = Dio_1msReadChannel_Direct(&MODULE_P11, 4);    /* IOC_IN_PORT_PB_SW_MON        */
    Ioc_InputCS1msBus.Ioc_InputCS1msSwtMonInfo.HdcSwtMon        = Dio_1msReadChannel_Direct(&MODULE_P11, 5);    /* IOC_IN_PORT_HDC_SW_MON       */
    Ioc_InputCS1msBus.Ioc_InputCS1msRlyMonInfo.RlyFault         = Dio_1msReadChannel_Direct(&MODULE_P11, 7);    /* IOC_IN_PORT_RELAY_FAULT      */
    Ioc_InputCS1msBus.Ioc_InputCS1msSwtMonInfo.BlsSwtMon        = Dio_1msReadChannel_Direct(&MODULE_P11, 9);    /* IOC_IN_PORT_BLS_MON          */
    Ioc_InputCS1msBus.Ioc_InputCS1msSwtMonInfo.FlexBrkASwtMon   = Dio_1msReadChannel_Direct(&MODULE_P11, 10);   /* IOC_IN_PORT_FLEX_BRAKE_A_MON */
    Ioc_InputCS1msBus.Ioc_InputCS1msRlyMonInfo.RlyEssMon        = Dio_1msReadChannel_Direct(&MODULE_P11, 12);   /* IOC_IN_PORT_RELAY_ESS_MON    */
    Ioc_InputCS1msBus.Ioc_InputCS1msSwtMonInfo.HzrdSwtMon       = Dio_1msReadChannel_Direct(&MODULE_P11, 14);   /* IOC_IN_PORT_HAZARD_SW_MON    */
    Ioc_InputCS1msBus.Ioc_InputCS1msSwtMonInfo.EscSwtMon        = Dio_1msReadChannel_Direct(&MODULE_P11, 15);   /* IOC_IN_PORT_ESC_SW_MON       */
    Ioc_InputCS1msBus.Ioc_InputCS1msSwtMonInfo.DoorSwtMon       = Dio_1msReadChannel_Direct(&MODULE_P13, 0);    /* IOC_IN_PORT_DOOR_SW_MON      */
    Ioc_InputCS1msBus.Ioc_InputCS1msSwtMonInfo.BsSwtMon         = Dio_1msReadChannel_Direct(&MODULE_P13, 2);    /* IOC_IN_PORT_BS_SW_MON        */
    Ioc_InputCS1msBus.Ioc_InputCS1msSwtMonInfo.FlexBrkBSwtMon   = Dio_1msReadChannel_Direct(&MODULE_P13, 3);    /* IOC_IN_PORT_FLEX_BRAKE_B_MON */
    Ioc_InputCS1msBus.Ioc_InputCS1msSwLineMonInfo.Sw1LineCMon   = Dio_1msReadChannel_Direct(&MODULE_P20, 6);    /* IOC_IN_PORT_SW1_LINE_C_MON   */
    Ioc_InputCS1msBus.Ioc_InputCS1msRlyMonInfo.RlyDbcMon        = Dio_1msReadChannel_Direct(&MODULE_P21, 2);    /* IOC_IN_PORT_RELAY_DBC_MON    */
    Ioc_InputCS1msBus.Ioc_InputCS1msSwLineMonInfo.Sw2LineBMon   = Dio_1msReadChannel_Direct(&MODULE_P21, 5);    /* IOC_IN_PORT_SW2_LINE_B_MON   */
    //(void)Ioc_Input1msCs_Dio_ReadChannel(Ioc_DioMapping.MCU_FRA1_RXEN);    /* IOC_IN_PORT_FRA1_RXEN        *//* TODO GH : maybe the direction of the channel is output*/
    //(void)Ioc_Input1msCs_Dio_ReadChannel(Ioc_DioMapping.MCU_FRA2_RXEN);    /* IOC_IN_PORT_FRA2_RXEN        *//* GH : maybe the direction of the channel is output*/
    //(void)Ioc_Input1msCs_Dio_ReadChannel(Ioc_DioMapping.MTR_IC_ERR);    /* IOC_IN_PORT_MTR_IC_ERR       */
    Ioc_InputCS1msBus.Ioc_InputCS1msSwLineMonInfo.Sw3LineEMon   = Dio_1msReadChannel_Direct(&MODULE_P32, 2);    /* IOC_IN_PORT_SW3_LINE_E_MON   */
    Ioc_InputCS1msBus.Ioc_InputCS1msSwLineMonInfo.Sw4LineFMon   = Dio_1msReadChannel_Direct(&MODULE_P32, 3);    /* IOC_IN_PORT_SW4_LINE_F_MON   */
    //(void)Ioc_Input1msCs_Dio_ReadChannel(Ioc_DioMapping.MCU_FRA2_ERRN);    /* IOC_IN_PORT_FRA2_ERRN        */
    //(void)Ioc_Input1msCs_Dio_ReadChannel(Ioc_DioMapping.MCU_FRA1_ERRN);    /* IOC_IN_PORT_FRA1_ERRN        */
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static Dio_LevelType Ioc_Input1msCs_Dio_ReadChannel(Dio_ChannelType ChannelId)
{
    Dio_LevelType rtn = 0;

    if(ChannelId != DIO_CHANNEL_NOT_USED)
    {
        rtn = Dio_ReadChannel(ChannelId);
    }
    
    return rtn;
}

inline boolean Dio_1msReadChannel_Direct(Ifx_P *port, uint8 pinIndex)
{
    return (__getbit(&port->IN.U, pinIndex) != 0) ? TRUE : FALSE;
}

inline void Dio_1msWriteChannel_Direct(Ifx_P *port, uint8 pinIndex, Dio_LevelType Level)
{
    if(Level==STD_HIGH)
    {
        port->OMR.U = (uint32)(1 << pinIndex);
    }
    else
    {
        port->OMR.U = (uint32)((1 << 16)<< pinIndex);
    }
}

#define IOC_INPUTCS1MS_STOP_SEC_CODE
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
