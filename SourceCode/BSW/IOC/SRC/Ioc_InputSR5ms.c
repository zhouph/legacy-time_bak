/**
 * @defgroup Ioc_InputSR5ms Ioc_InputSR5ms
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_InputSR5ms.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ioc_InputSR5ms.h"
#include "Ioc_InputSR5ms_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define IOC_INPUTSR5MS_START_SEC_CONST_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define IOC_INPUTSR5MS_STOP_SEC_CONST_UNSPECIFIED
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define IOC_INPUTSR5MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Ioc_InputSR5ms_HdrBusType Ioc_InputSR5msBus;

/* Version Info */
const SwcVersionInfo_t Ioc_InputSR5msVersionInfo = 
{   
    IOC_INPUTSR5MS_MODULE_ID,           /* Ioc_InputSR5msVersionInfo.ModuleId */
    IOC_INPUTSR5MS_MAJOR_VERSION,       /* Ioc_InputSR5msVersionInfo.MajorVer */
    IOC_INPUTSR5MS_MINOR_VERSION,       /* Ioc_InputSR5msVersionInfo.MinorVer */
    IOC_INPUTSR5MS_PATCH_VERSION,       /* Ioc_InputSR5msVersionInfo.PatchVer */
    IOC_INPUTSR5MS_BRANCH_VERSION       /* Ioc_InputSR5msVersionInfo.BranchVer */
};
    
/* Input Data Element */
Icu_InputCaptureRisngIdxInfo_t Ioc_InputSR5msRisngIdxInfo;
Icu_InputCaptureFallIdxInfo_t Ioc_InputSR5msFallIdxInfo;
Icu_InputCaptureRisngTiStampInfo_t Ioc_InputSR5msRisngTiStampInfo;
Icu_InputCaptureFallTiStampInfo_t Ioc_InputSR5msFallTiStampInfo;
Mom_HndlrEcuModeSts_t Ioc_InputSR5msEcuModeSts;
Acm_MainAcmAsicInitCompleteFlag_t Ioc_InputSR5msAcmAsicInitCompleteFlag;
Eem_SuspcDetnFuncInhibitIocSts_t Ioc_InputSR5msFuncInhibitIocSts;

/* Output Data Element */
Ioc_InputSR5msWssMonInfo_t Ioc_InputSR5msWssMonInfo;

uint32 Ioc_InputSR5ms_Timer_Start;
uint32 Ioc_InputSR5ms_Timer_Elapsed;

#define IOC_INPUTSR5MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_INPUTSR5MS_START_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define IOC_INPUTSR5MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
#define IOC_INPUTSR5MS_START_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define IOC_INPUTSR5MS_STOP_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_INPUTSR5MS_START_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (32BIT)**/


#define IOC_INPUTSR5MS_STOP_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define IOC_INPUTSR5MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define IOC_INPUTSR5MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_INPUTSR5MS_START_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define IOC_INPUTSR5MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
#define IOC_INPUTSR5MS_START_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define IOC_INPUTSR5MS_STOP_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_INPUTSR5MS_START_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (32BIT)**/


#define IOC_INPUTSR5MS_STOP_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define IOC_INPUTSR5MS_START_SEC_CODE
#include "Ioc_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Ioc_InputSR5ms_Init(void)
{
    uint16 i;

    /* Initialize internal bus */
    Ioc_InputSR5msBus.Ioc_InputSR5msRisngIdxInfo.FlRisngIdx = 0;
    Ioc_InputSR5msBus.Ioc_InputSR5msRisngIdxInfo.FrRisngIdx = 0;
    Ioc_InputSR5msBus.Ioc_InputSR5msRisngIdxInfo.RlRisngIdx = 0;
    Ioc_InputSR5msBus.Ioc_InputSR5msRisngIdxInfo.RrRisngIdx = 0;
    Ioc_InputSR5msBus.Ioc_InputSR5msFallIdxInfo.FlFallIdx = 0;
    Ioc_InputSR5msBus.Ioc_InputSR5msFallIdxInfo.FrFallIdx = 0;
    Ioc_InputSR5msBus.Ioc_InputSR5msFallIdxInfo.RlFallIdx = 0;
    Ioc_InputSR5msBus.Ioc_InputSR5msFallIdxInfo.RrFallIdx = 0;
    for(i=0;i<32;i++) Ioc_InputSR5msBus.Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[i] = 0;   
    for(i=0;i<32;i++) Ioc_InputSR5msBus.Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[i] = 0;   
    for(i=0;i<32;i++) Ioc_InputSR5msBus.Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[i] = 0;   
    for(i=0;i<32;i++) Ioc_InputSR5msBus.Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[i] = 0;   
    for(i=0;i<32;i++) Ioc_InputSR5msBus.Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[i] = 0;   
    for(i=0;i<32;i++) Ioc_InputSR5msBus.Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[i] = 0;   
    for(i=0;i<32;i++) Ioc_InputSR5msBus.Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[i] = 0;   
    for(i=0;i<32;i++) Ioc_InputSR5msBus.Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[i] = 0;   
    Ioc_InputSR5msBus.Ioc_InputSR5msEcuModeSts = 0;
    Ioc_InputSR5msBus.Ioc_InputSR5msAcmAsicInitCompleteFlag = 0;
    Ioc_InputSR5msBus.Ioc_InputSR5msFuncInhibitIocSts = 0;
    Ioc_InputSR5msBus.Ioc_InputSR5msWssMonInfo.FlRisngIdx = 0;
    Ioc_InputSR5msBus.Ioc_InputSR5msWssMonInfo.FlFallIdx = 0;
    for(i=0;i<32;i++) Ioc_InputSR5msBus.Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[i] = 0;   
    for(i=0;i<32;i++) Ioc_InputSR5msBus.Ioc_InputSR5msWssMonInfo.FlFallTiStamp[i] = 0;   
    Ioc_InputSR5msBus.Ioc_InputSR5msWssMonInfo.FrRisngIdx = 0;
    Ioc_InputSR5msBus.Ioc_InputSR5msWssMonInfo.FrFallIdx = 0;
    for(i=0;i<32;i++) Ioc_InputSR5msBus.Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[i] = 0;   
    for(i=0;i<32;i++) Ioc_InputSR5msBus.Ioc_InputSR5msWssMonInfo.FrFallTiStamp[i] = 0;   
    Ioc_InputSR5msBus.Ioc_InputSR5msWssMonInfo.RlRisngIdx = 0;
    Ioc_InputSR5msBus.Ioc_InputSR5msWssMonInfo.RlFallIdx = 0;
    for(i=0;i<32;i++) Ioc_InputSR5msBus.Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[i] = 0;   
    for(i=0;i<32;i++) Ioc_InputSR5msBus.Ioc_InputSR5msWssMonInfo.RlFallTiStamp[i] = 0;   
    Ioc_InputSR5msBus.Ioc_InputSR5msWssMonInfo.RrRisngIdx = 0;
    Ioc_InputSR5msBus.Ioc_InputSR5msWssMonInfo.RrFallIdx = 0;
    for(i=0;i<32;i++) Ioc_InputSR5msBus.Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[i] = 0;   
    for(i=0;i<32;i++) Ioc_InputSR5msBus.Ioc_InputSR5msWssMonInfo.RrFallTiStamp[i] = 0;   
}

void Ioc_InputSR5ms(void)
{
    uint16 i;
    
    Ioc_InputSR5ms_Timer_Start = STM0_TIM0.U;

    /* Input */
    Ioc_InputSR5ms_Read_Ioc_InputSR5msRisngIdxInfo(&Ioc_InputSR5msBus.Ioc_InputSR5msRisngIdxInfo);
    /*==============================================================================
    * Members of structure Ioc_InputSR5msRisngIdxInfo 
     : Ioc_InputSR5msRisngIdxInfo.FlRisngIdx;
     : Ioc_InputSR5msRisngIdxInfo.FrRisngIdx;
     : Ioc_InputSR5msRisngIdxInfo.RlRisngIdx;
     : Ioc_InputSR5msRisngIdxInfo.RrRisngIdx;
     =============================================================================*/
    
    Ioc_InputSR5ms_Read_Ioc_InputSR5msFallIdxInfo(&Ioc_InputSR5msBus.Ioc_InputSR5msFallIdxInfo);
    /*==============================================================================
    * Members of structure Ioc_InputSR5msFallIdxInfo 
     : Ioc_InputSR5msFallIdxInfo.FlFallIdx;
     : Ioc_InputSR5msFallIdxInfo.FrFallIdx;
     : Ioc_InputSR5msFallIdxInfo.RlFallIdx;
     : Ioc_InputSR5msFallIdxInfo.RrFallIdx;
     =============================================================================*/
    
    Ioc_InputSR5ms_Read_Ioc_InputSR5msRisngTiStampInfo(&Ioc_InputSR5msBus.Ioc_InputSR5msRisngTiStampInfo);
    /*==============================================================================
    * Members of structure Ioc_InputSR5msRisngTiStampInfo 
     : Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp;
     : Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp;
     : Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp;
     : Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp;
     =============================================================================*/
    
    Ioc_InputSR5ms_Read_Ioc_InputSR5msFallTiStampInfo(&Ioc_InputSR5msBus.Ioc_InputSR5msFallTiStampInfo);
    /*==============================================================================
    * Members of structure Ioc_InputSR5msFallTiStampInfo 
     : Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp;
     : Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp;
     : Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp;
     : Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp;
     =============================================================================*/
    
    Ioc_InputSR5ms_Read_Ioc_InputSR5msEcuModeSts(&Ioc_InputSR5msBus.Ioc_InputSR5msEcuModeSts);
    Ioc_InputSR5ms_Read_Ioc_InputSR5msAcmAsicInitCompleteFlag(&Ioc_InputSR5msBus.Ioc_InputSR5msAcmAsicInitCompleteFlag);
    Ioc_InputSR5ms_Read_Ioc_InputSR5msFuncInhibitIocSts(&Ioc_InputSR5msBus.Ioc_InputSR5msFuncInhibitIocSts);

    /* Process */
    Ioc_InputSR5msConditioning();

    /* Output */
    Ioc_InputSR5ms_Write_Ioc_InputSR5msWssMonInfo(&Ioc_InputSR5msBus.Ioc_InputSR5msWssMonInfo);
    /*==============================================================================
    * Members of structure Ioc_InputSR5msWssMonInfo 
     : Ioc_InputSR5msWssMonInfo.FlRisngIdx;
     : Ioc_InputSR5msWssMonInfo.FlFallIdx;
     : Ioc_InputSR5msWssMonInfo.FlRisngTiStamp;
     : Ioc_InputSR5msWssMonInfo.FlFallTiStamp;
     : Ioc_InputSR5msWssMonInfo.FrRisngIdx;
     : Ioc_InputSR5msWssMonInfo.FrFallIdx;
     : Ioc_InputSR5msWssMonInfo.FrRisngTiStamp;
     : Ioc_InputSR5msWssMonInfo.FrFallTiStamp;
     : Ioc_InputSR5msWssMonInfo.RlRisngIdx;
     : Ioc_InputSR5msWssMonInfo.RlFallIdx;
     : Ioc_InputSR5msWssMonInfo.RlRisngTiStamp;
     : Ioc_InputSR5msWssMonInfo.RlFallTiStamp;
     : Ioc_InputSR5msWssMonInfo.RrRisngIdx;
     : Ioc_InputSR5msWssMonInfo.RrFallIdx;
     : Ioc_InputSR5msWssMonInfo.RrRisngTiStamp;
     : Ioc_InputSR5msWssMonInfo.RrFallTiStamp;
     =============================================================================*/
    

    Ioc_InputSR5ms_Timer_Elapsed = STM0_TIM0.U - Ioc_InputSR5ms_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define IOC_INPUTSR5MS_STOP_SEC_CODE
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
