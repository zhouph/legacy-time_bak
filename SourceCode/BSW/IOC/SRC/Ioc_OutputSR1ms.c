/**
 * @defgroup Ioc_OutputSR1ms Ioc_OutputSR1ms
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_OutputSR1ms.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ioc_OutputSR1ms.h"
#include "Ioc_OutputSR1ms_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define IOC_OUTPUTSR1MS_START_SEC_CONST_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define IOC_OUTPUTSR1MS_STOP_SEC_CONST_UNSPECIFIED
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define IOC_OUTPUTSR1MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Ioc_OutputSR1ms_HdrBusType Ioc_OutputSR1msBus;

/* Version Info */
const SwcVersionInfo_t Ioc_OutputSR1msVersionInfo = 
{   
    IOC_OUTPUTSR1MS_MODULE_ID,           /* Ioc_OutputSR1msVersionInfo.ModuleId */
    IOC_OUTPUTSR1MS_MAJOR_VERSION,       /* Ioc_OutputSR1msVersionInfo.MajorVer */
    IOC_OUTPUTSR1MS_MINOR_VERSION,       /* Ioc_OutputSR1msVersionInfo.MinorVer */
    IOC_OUTPUTSR1MS_PATCH_VERSION,       /* Ioc_OutputSR1msVersionInfo.PatchVer */
    IOC_OUTPUTSR1MS_BRANCH_VERSION       /* Ioc_OutputSR1msVersionInfo.BranchVer */
};
    
/* Input Data Element */
Vlv_ActrWhlVlvDrvInfo_t Ioc_OutputSR1msWhlVlvDrvInfo;
Vlv_ActrBalVlvDrvInfo_t Ioc_OutputSR1msBalVlvDrvInfo;
Fsr_ActrFsrCbsDrvInfo_t Ioc_OutputSR1msFsrCbsDrvInfo;
Fsr_ActrFsrAbsDrvInfo_t Ioc_OutputSR1msFsrAbsDrvInfo;
Mom_HndlrEcuModeSts_t Ioc_OutputSR1msEcuModeSts;
Acm_MainAcmAsicInitCompleteFlag_t Ioc_OutputSR1msAcmAsicInitCompleteFlag;
Eem_SuspcDetnFuncInhibitIocSts_t Ioc_OutputSR1msFuncInhibitIocSts;

/* Output Data Element */
Ioc_OutputSR1msIocDcMtrDutyData_t Ioc_OutputSR1msIocDcMtrDutyData;
Ioc_OutputSR1msIocDcMtrFreqData_t Ioc_OutputSR1msIocDcMtrFreqData;
Ioc_OutputSR1msIocVlvNo0Data_t Ioc_OutputSR1msIocVlvNo0Data;
Ioc_OutputSR1msIocVlvNo1Data_t Ioc_OutputSR1msIocVlvNo1Data;
Ioc_OutputSR1msIocVlvNo2Data_t Ioc_OutputSR1msIocVlvNo2Data;
Ioc_OutputSR1msIocVlvNo3Data_t Ioc_OutputSR1msIocVlvNo3Data;
Ioc_OutputSR1msIocVlvTc0Data_t Ioc_OutputSR1msIocVlvTc0Data;
Ioc_OutputSR1msIocVlvTc1Data_t Ioc_OutputSR1msIocVlvTc1Data;
Ioc_OutputSR1msIocVlvEsv0Data_t Ioc_OutputSR1msIocVlvEsv0Data;
Ioc_OutputSR1msIocVlvEsv1Data_t Ioc_OutputSR1msIocVlvEsv1Data;
Ioc_OutputSR1msIocVlvNc0Data_t Ioc_OutputSR1msIocVlvNc0Data;
Ioc_OutputSR1msIocVlvNc1Data_t Ioc_OutputSR1msIocVlvNc1Data;
Ioc_OutputSR1msIocVlvNc2Data_t Ioc_OutputSR1msIocVlvNc2Data;
Ioc_OutputSR1msIocVlvNc3Data_t Ioc_OutputSR1msIocVlvNc3Data;
Ioc_OutputSR1msIocVlvRelayData_t Ioc_OutputSR1msIocVlvRelayData;
Ioc_OutputSR1msIocAdcSelWriteData_t Ioc_OutputSR1msIocAdcSelWriteData;

uint32 Ioc_OutputSR1ms_Timer_Start;
uint32 Ioc_OutputSR1ms_Timer_Elapsed;

#define IOC_OUTPUTSR1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_OUTPUTSR1MS_START_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define IOC_OUTPUTSR1MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
#define IOC_OUTPUTSR1MS_START_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define IOC_OUTPUTSR1MS_STOP_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_OUTPUTSR1MS_START_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (32BIT)**/


#define IOC_OUTPUTSR1MS_STOP_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define IOC_OUTPUTSR1MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define IOC_OUTPUTSR1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_OUTPUTSR1MS_START_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define IOC_OUTPUTSR1MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
#define IOC_OUTPUTSR1MS_START_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define IOC_OUTPUTSR1MS_STOP_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_OUTPUTSR1MS_START_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (32BIT)**/


#define IOC_OUTPUTSR1MS_STOP_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define IOC_OUTPUTSR1MS_START_SEC_CODE
#include "Ioc_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Ioc_OutputSR1ms_Init(void)
{
    /* Initialize internal bus */
    Ioc_OutputSR1msBus.Ioc_OutputSR1msWhlVlvDrvInfo.FlOvDrvData = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msWhlVlvDrvInfo.FlIvDrvData = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msWhlVlvDrvInfo.FrOvDrvData = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msWhlVlvDrvInfo.FrIvDrvData = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msWhlVlvDrvInfo.RlOvDrvData = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msWhlVlvDrvInfo.RlIvDrvData = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msWhlVlvDrvInfo.RrOvDrvData = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msWhlVlvDrvInfo.RrIvDrvData = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msBalVlvDrvInfo.PressDumpVlvDrvData = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msBalVlvDrvInfo.BalVlvDrvData = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msFsrCbsDrvInfo.FsrCbsDrv = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msFsrAbsDrvInfo.FsrAbsDrv = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msEcuModeSts = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msAcmAsicInitCompleteFlag = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msFuncInhibitIocSts = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocDcMtrDutyData.MtrDutyDataFlg = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocDcMtrDutyData.ACH_Tx8_PUMP_DTY_PWM = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocDcMtrFreqData.MtrFreqDataFlg = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocDcMtrFreqData.ACH_Tx8_PUMP_TCK_PWM = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNo0Data.VlvNo0DataFlg = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNo0Data.ACH_TxValve_SET_POINT = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNo1Data.VlvNo1DataFlg = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNo1Data.ACH_TxValve_SET_POINT = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNo2Data.VlvNo2DataFlg = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNo2Data.ACH_TxValve_SET_POINT = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNo3Data.VlvNo3DataFlg = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNo3Data.ACH_TxValve_SET_POINT = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvTc0Data.VlvTc0DataFlg = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvTc0Data.ACH_TxValve_SET_POINT = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvTc1Data.VlvTc1DataFlg = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvTc1Data.ACH_TxValve_SET_POINT = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvEsv0Data.VlvEsv0DataFlg = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvEsv0Data.ACH_TxValve_SET_POINT = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvEsv1Data.VlvEsv1DataFlg = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvEsv1Data.ACH_TxValve_SET_POINT = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNc0Data.VlvNc0DataFlg = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNc0Data.ACH_TxValve_SET_POINT = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNc1Data.VlvNc1DataFlg = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNc1Data.ACH_TxValve_SET_POINT = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNc2Data.VlvNc2DataFlg = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNc2Data.ACH_TxValve_SET_POINT = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNc3Data.VlvNc3DataFlg = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNc3Data.ACH_TxValve_SET_POINT = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvRelayData.VlvRelayDataFlg = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvRelayData.ACH_Tx1_FS_CMD = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocAdcSelWriteData.AdcSelDataFlg = 0;
    Ioc_OutputSR1msBus.Ioc_OutputSR1msIocAdcSelWriteData.ACH_Tx13_ADC_SEL = 0;
}

void Ioc_OutputSR1ms(void)
{
    Ioc_OutputSR1ms_Timer_Start = STM0_TIM0.U;

    /* Input */
    Ioc_OutputSR1ms_Read_Ioc_OutputSR1msWhlVlvDrvInfo(&Ioc_OutputSR1msBus.Ioc_OutputSR1msWhlVlvDrvInfo);
    /*==============================================================================
    * Members of structure Ioc_OutputSR1msWhlVlvDrvInfo 
     : Ioc_OutputSR1msWhlVlvDrvInfo.FlOvDrvData;
     : Ioc_OutputSR1msWhlVlvDrvInfo.FlIvDrvData;
     : Ioc_OutputSR1msWhlVlvDrvInfo.FrOvDrvData;
     : Ioc_OutputSR1msWhlVlvDrvInfo.FrIvDrvData;
     : Ioc_OutputSR1msWhlVlvDrvInfo.RlOvDrvData;
     : Ioc_OutputSR1msWhlVlvDrvInfo.RlIvDrvData;
     : Ioc_OutputSR1msWhlVlvDrvInfo.RrOvDrvData;
     : Ioc_OutputSR1msWhlVlvDrvInfo.RrIvDrvData;
     =============================================================================*/
    
    Ioc_OutputSR1ms_Read_Ioc_OutputSR1msBalVlvDrvInfo(&Ioc_OutputSR1msBus.Ioc_OutputSR1msBalVlvDrvInfo);
    /*==============================================================================
    * Members of structure Ioc_OutputSR1msBalVlvDrvInfo 
     : Ioc_OutputSR1msBalVlvDrvInfo.PrimBalVlvDrvData;
     : Ioc_OutputSR1msBalVlvDrvInfo.PressDumpVlvDrvData;
     : Ioc_OutputSR1msBalVlvDrvInfo.ChmbBalVlvDrvData;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Ioc_OutputSR1ms_Read_Ioc_OutputSR1msFsrCbsDrvInfo_FsrCbsDrv(&Ioc_OutputSR1msBus.Ioc_OutputSR1msFsrCbsDrvInfo.FsrCbsDrv);

    /* Decomposed structure interface */
    Ioc_OutputSR1ms_Read_Ioc_OutputSR1msFsrAbsDrvInfo_FsrAbsDrv(&Ioc_OutputSR1msBus.Ioc_OutputSR1msFsrAbsDrvInfo.FsrAbsDrv);

    Ioc_OutputSR1ms_Read_Ioc_OutputSR1msEcuModeSts(&Ioc_OutputSR1msBus.Ioc_OutputSR1msEcuModeSts);
    Ioc_OutputSR1ms_Read_Ioc_OutputSR1msAcmAsicInitCompleteFlag(&Ioc_OutputSR1msBus.Ioc_OutputSR1msAcmAsicInitCompleteFlag);
    Ioc_OutputSR1ms_Read_Ioc_OutputSR1msFuncInhibitIocSts(&Ioc_OutputSR1msBus.Ioc_OutputSR1msFuncInhibitIocSts);

    /* Process */
    Ioc_OutputSR1msConditioning();

    /* Output */
    Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocDcMtrDutyData(&Ioc_OutputSR1msBus.Ioc_OutputSR1msIocDcMtrDutyData);
    /*==============================================================================
    * Members of structure Ioc_OutputSR1msIocDcMtrDutyData 
     : Ioc_OutputSR1msIocDcMtrDutyData.MtrDutyDataFlg;
     : Ioc_OutputSR1msIocDcMtrDutyData.ACH_Tx8_PUMP_DTY_PWM;
     =============================================================================*/
    
    Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocDcMtrFreqData(&Ioc_OutputSR1msBus.Ioc_OutputSR1msIocDcMtrFreqData);
    /*==============================================================================
    * Members of structure Ioc_OutputSR1msIocDcMtrFreqData 
     : Ioc_OutputSR1msIocDcMtrFreqData.MtrFreqDataFlg;
     : Ioc_OutputSR1msIocDcMtrFreqData.ACH_Tx8_PUMP_TCK_PWM;
     =============================================================================*/
    
    Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvNo0Data(&Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNo0Data);
    /*==============================================================================
    * Members of structure Ioc_OutputSR1msIocVlvNo0Data 
     : Ioc_OutputSR1msIocVlvNo0Data.VlvNo0DataFlg;
     : Ioc_OutputSR1msIocVlvNo0Data.ACH_TxValve_SET_POINT;
     =============================================================================*/
    
    Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvNo1Data(&Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNo1Data);
    /*==============================================================================
    * Members of structure Ioc_OutputSR1msIocVlvNo1Data 
     : Ioc_OutputSR1msIocVlvNo1Data.VlvNo1DataFlg;
     : Ioc_OutputSR1msIocVlvNo1Data.ACH_TxValve_SET_POINT;
     =============================================================================*/
    
    Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvNo2Data(&Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNo2Data);
    /*==============================================================================
    * Members of structure Ioc_OutputSR1msIocVlvNo2Data 
     : Ioc_OutputSR1msIocVlvNo2Data.VlvNo2DataFlg;
     : Ioc_OutputSR1msIocVlvNo2Data.ACH_TxValve_SET_POINT;
     =============================================================================*/
    
    Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvNo3Data(&Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNo3Data);
    /*==============================================================================
    * Members of structure Ioc_OutputSR1msIocVlvNo3Data 
     : Ioc_OutputSR1msIocVlvNo3Data.VlvNo3DataFlg;
     : Ioc_OutputSR1msIocVlvNo3Data.ACH_TxValve_SET_POINT;
     =============================================================================*/
    
    Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvTc0Data(&Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvTc0Data);
    /*==============================================================================
    * Members of structure Ioc_OutputSR1msIocVlvTc0Data 
     : Ioc_OutputSR1msIocVlvTc0Data.VlvTc0DataFlg;
     : Ioc_OutputSR1msIocVlvTc0Data.ACH_TxValve_SET_POINT;
     =============================================================================*/
    
    Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvTc1Data(&Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvTc1Data);
    /*==============================================================================
    * Members of structure Ioc_OutputSR1msIocVlvTc1Data 
     : Ioc_OutputSR1msIocVlvTc1Data.VlvTc1DataFlg;
     : Ioc_OutputSR1msIocVlvTc1Data.ACH_TxValve_SET_POINT;
     =============================================================================*/
    
    Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvEsv0Data(&Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvEsv0Data);
    /*==============================================================================
    * Members of structure Ioc_OutputSR1msIocVlvEsv0Data 
     : Ioc_OutputSR1msIocVlvEsv0Data.VlvEsv0DataFlg;
     : Ioc_OutputSR1msIocVlvEsv0Data.ACH_TxValve_SET_POINT;
     =============================================================================*/
    
    Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvEsv1Data(&Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvEsv1Data);
    /*==============================================================================
    * Members of structure Ioc_OutputSR1msIocVlvEsv1Data 
     : Ioc_OutputSR1msIocVlvEsv1Data.VlvEsv1DataFlg;
     : Ioc_OutputSR1msIocVlvEsv1Data.ACH_TxValve_SET_POINT;
     =============================================================================*/
    
    Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvNc0Data(&Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNc0Data);
    /*==============================================================================
    * Members of structure Ioc_OutputSR1msIocVlvNc0Data 
     : Ioc_OutputSR1msIocVlvNc0Data.VlvNc0DataFlg;
     : Ioc_OutputSR1msIocVlvNc0Data.ACH_TxValve_SET_POINT;
     =============================================================================*/
    
    Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvNc1Data(&Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNc1Data);
    /*==============================================================================
    * Members of structure Ioc_OutputSR1msIocVlvNc1Data 
     : Ioc_OutputSR1msIocVlvNc1Data.VlvNc1DataFlg;
     : Ioc_OutputSR1msIocVlvNc1Data.ACH_TxValve_SET_POINT;
     =============================================================================*/
    
    Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvNc2Data(&Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNc2Data);
    /*==============================================================================
    * Members of structure Ioc_OutputSR1msIocVlvNc2Data 
     : Ioc_OutputSR1msIocVlvNc2Data.VlvNc2DataFlg;
     : Ioc_OutputSR1msIocVlvNc2Data.ACH_TxValve_SET_POINT;
     =============================================================================*/
    
    Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvNc3Data(&Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvNc3Data);
    /*==============================================================================
    * Members of structure Ioc_OutputSR1msIocVlvNc3Data 
     : Ioc_OutputSR1msIocVlvNc3Data.VlvNc3DataFlg;
     : Ioc_OutputSR1msIocVlvNc3Data.ACH_TxValve_SET_POINT;
     =============================================================================*/
    
    Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocVlvRelayData(&Ioc_OutputSR1msBus.Ioc_OutputSR1msIocVlvRelayData);
    /*==============================================================================
    * Members of structure Ioc_OutputSR1msIocVlvRelayData 
     : Ioc_OutputSR1msIocVlvRelayData.VlvRelayDataFlg;
     : Ioc_OutputSR1msIocVlvRelayData.ACH_Tx1_FS_CMD;
     =============================================================================*/
    
    Ioc_OutputSR1ms_Write_Ioc_OutputSR1msIocAdcSelWriteData(&Ioc_OutputSR1msBus.Ioc_OutputSR1msIocAdcSelWriteData);
    /*==============================================================================
    * Members of structure Ioc_OutputSR1msIocAdcSelWriteData 
     : Ioc_OutputSR1msIocAdcSelWriteData.AdcSelDataFlg;
     : Ioc_OutputSR1msIocAdcSelWriteData.ACH_Tx13_ADC_SEL;
     =============================================================================*/
    

    Ioc_OutputSR1ms_Timer_Elapsed = STM0_TIM0.U - Ioc_OutputSR1ms_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define IOC_OUTPUTSR1MS_STOP_SEC_CODE
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
