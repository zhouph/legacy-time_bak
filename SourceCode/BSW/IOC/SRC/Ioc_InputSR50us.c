/**
 * @defgroup Ioc_InputSR50us Ioc_InputSR50us
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_InputSR50us.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ioc_InputSR50us.h"
#include "Ioc_InputSR50us_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define IOC_INPUTSR50US_START_SEC_CONST_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define IOC_INPUTSR50US_STOP_SEC_CONST_UNSPECIFIED
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define IOC_INPUTSR50US_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Ioc_InputSR50us_HdrBusType Ioc_InputSR50usBus;

/* Version Info */
const SwcVersionInfo_t Ioc_InputSR50usVersionInfo = 
{   
    IOC_INPUTSR50US_MODULE_ID,           /* Ioc_InputSR50usVersionInfo.ModuleId */
    IOC_INPUTSR50US_MAJOR_VERSION,       /* Ioc_InputSR50usVersionInfo.MajorVer */
    IOC_INPUTSR50US_MINOR_VERSION,       /* Ioc_InputSR50usVersionInfo.MinorVer */
    IOC_INPUTSR50US_PATCH_VERSION,       /* Ioc_InputSR50usVersionInfo.PatchVer */
    IOC_INPUTSR50US_BRANCH_VERSION       /* Ioc_InputSR50usVersionInfo.BranchVer */
};
    
/* Input Data Element */
Mps_TLE5012_Hndlr_50usMpsD1IIFAngInfo_t Ioc_InputSR50usMpsD1IIFAngInfo;
Mps_TLE5012_Hndlr_50usMpsD2IIFAngInfo_t Ioc_InputSR50usMpsD2IIFAngInfo;
AdcIf_Conv50usHwTrigMotInfo_t Ioc_InputSR50usHwTrigMotInfo;
Mom_HndlrEcuModeSts_t Ioc_InputSR50usEcuModeSts;
Eem_SuspcDetnFuncInhibitIocSts_t Ioc_InputSR50usFuncInhibitIocSts;

/* Output Data Element */
Ioc_InputSR50usMotCurrMonInfo_t Ioc_InputSR50usMotCurrMonInfo;
Ioc_InputSR50usMotAngleMonInfo_t Ioc_InputSR50usMotAngleMonInfo;

uint32 Ioc_InputSR50us_Timer_Start;
uint32 Ioc_InputSR50us_Timer_Elapsed;

#define IOC_INPUTSR50US_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_INPUTSR50US_START_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define IOC_INPUTSR50US_STOP_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
#define IOC_INPUTSR50US_START_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define IOC_INPUTSR50US_STOP_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_INPUTSR50US_START_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (32BIT)**/


#define IOC_INPUTSR50US_STOP_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define IOC_INPUTSR50US_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define IOC_INPUTSR50US_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_INPUTSR50US_START_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define IOC_INPUTSR50US_STOP_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
#define IOC_INPUTSR50US_START_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define IOC_INPUTSR50US_STOP_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_INPUTSR50US_START_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (32BIT)**/


#define IOC_INPUTSR50US_STOP_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define IOC_INPUTSR50US_START_SEC_CODE
#include "Ioc_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Ioc_InputSR50us_Init(void)
{
    /* Initialize internal bus */
    Ioc_InputSR50usBus.Ioc_InputSR50usMpsD1IIFAngInfo.D1IIFAngRawData = 0;
    Ioc_InputSR50usBus.Ioc_InputSR50usMpsD1IIFAngInfo.D1IIFAngDegree = 0;
    Ioc_InputSR50usBus.Ioc_InputSR50usMpsD2IIFAngInfo.D2IIFAngRawData = 0;
    Ioc_InputSR50usBus.Ioc_InputSR50usMpsD2IIFAngInfo.D2IIFAngDegree = 0;
    Ioc_InputSR50usBus.Ioc_InputSR50usHwTrigMotInfo.Uphase0Mon = 0;
    Ioc_InputSR50usBus.Ioc_InputSR50usHwTrigMotInfo.Uphase1Mon = 0;
    Ioc_InputSR50usBus.Ioc_InputSR50usHwTrigMotInfo.Vphase0Mon = 0;
    Ioc_InputSR50usBus.Ioc_InputSR50usHwTrigMotInfo.VPhase1Mon = 0;
    Ioc_InputSR50usBus.Ioc_InputSR50usEcuModeSts = 0;
    Ioc_InputSR50usBus.Ioc_InputSR50usFuncInhibitIocSts = 0;
    Ioc_InputSR50usBus.Ioc_InputSR50usMotCurrMonInfo.MotCurrPhUMon0 = 0;
    Ioc_InputSR50usBus.Ioc_InputSR50usMotCurrMonInfo.MotCurrPhUMon1 = 0;
    Ioc_InputSR50usBus.Ioc_InputSR50usMotCurrMonInfo.MotCurrPhVMon0 = 0;
    Ioc_InputSR50usBus.Ioc_InputSR50usMotCurrMonInfo.MotCurrPhVMon1 = 0;
    Ioc_InputSR50usBus.Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle1deg = 0;
    Ioc_InputSR50usBus.Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle2deg = 0;
    Ioc_InputSR50usBus.Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle1raw = 0;
    Ioc_InputSR50usBus.Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle2raw = 0;
}

void Ioc_InputSR50us(void)
{
    Ioc_InputSR50us_Timer_Start = STM0_TIM0.U;

    /* Input */
    Ioc_InputSR50us_Read_Ioc_InputSR50usMpsD1IIFAngInfo(&Ioc_InputSR50usBus.Ioc_InputSR50usMpsD1IIFAngInfo);
    /*==============================================================================
    * Members of structure Ioc_InputSR50usMpsD1IIFAngInfo 
     : Ioc_InputSR50usMpsD1IIFAngInfo.D1IIFAngRawData;
     : Ioc_InputSR50usMpsD1IIFAngInfo.D1IIFAngDegree;
     =============================================================================*/
    
    Ioc_InputSR50us_Read_Ioc_InputSR50usMpsD2IIFAngInfo(&Ioc_InputSR50usBus.Ioc_InputSR50usMpsD2IIFAngInfo);
    /*==============================================================================
    * Members of structure Ioc_InputSR50usMpsD2IIFAngInfo 
     : Ioc_InputSR50usMpsD2IIFAngInfo.D2IIFAngRawData;
     : Ioc_InputSR50usMpsD2IIFAngInfo.D2IIFAngDegree;
     =============================================================================*/
    
    Ioc_InputSR50us_Read_Ioc_InputSR50usHwTrigMotInfo(&Ioc_InputSR50usBus.Ioc_InputSR50usHwTrigMotInfo);
    /*==============================================================================
    * Members of structure Ioc_InputSR50usHwTrigMotInfo 
     : Ioc_InputSR50usHwTrigMotInfo.Uphase0Mon;
     : Ioc_InputSR50usHwTrigMotInfo.Uphase1Mon;
     : Ioc_InputSR50usHwTrigMotInfo.Vphase0Mon;
     : Ioc_InputSR50usHwTrigMotInfo.VPhase1Mon;
     =============================================================================*/
    
    Ioc_InputSR50us_Read_Ioc_InputSR50usEcuModeSts(&Ioc_InputSR50usBus.Ioc_InputSR50usEcuModeSts);
    Ioc_InputSR50us_Read_Ioc_InputSR50usFuncInhibitIocSts(&Ioc_InputSR50usBus.Ioc_InputSR50usFuncInhibitIocSts);

    /* Process */
	Ioc_InputSR50usConditioning();
	
    /* Output */
    Ioc_InputSR50us_Write_Ioc_InputSR50usMotCurrMonInfo(&Ioc_InputSR50usBus.Ioc_InputSR50usMotCurrMonInfo);
    /*==============================================================================
    * Members of structure Ioc_InputSR50usMotCurrMonInfo 
     : Ioc_InputSR50usMotCurrMonInfo.MotCurrPhUMon0;
     : Ioc_InputSR50usMotCurrMonInfo.MotCurrPhUMon1;
     : Ioc_InputSR50usMotCurrMonInfo.MotCurrPhVMon0;
     : Ioc_InputSR50usMotCurrMonInfo.MotCurrPhVMon1;
     =============================================================================*/
    
    Ioc_InputSR50us_Write_Ioc_InputSR50usMotAngleMonInfo(&Ioc_InputSR50usBus.Ioc_InputSR50usMotAngleMonInfo);
    /*==============================================================================
    * Members of structure Ioc_InputSR50usMotAngleMonInfo 
     : Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle1deg;
     : Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle2deg;
     : Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle1raw;
     : Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle2raw;
     =============================================================================*/
    

    Ioc_InputSR50us_Timer_Elapsed = STM0_TIM0.U - Ioc_InputSR50us_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define IOC_INPUTSR50US_STOP_SEC_CODE
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
