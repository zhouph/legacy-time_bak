/**
 * @defgroup Ioc_OutputCS1msConditioning Ioc_OutputCS1msConditioning
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_OutputCS1msConditioning.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ioc_OutputCS1ms.h"
#include "Dio.h"
//#include "Pwm_17_Gtm.h"
#include "Pwm.h"
#include "Port.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/
#define FF2_DIRECTION_INPUT 0
#define FF2_DIRECTION_OUTPUT 1
/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define IOC_OUTPUTCS1MS_START_SEC_CONST_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define IOC_OUTPUTCS1MS_STOP_SEC_CONST_UNSPECIFIED
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define IOC_OUTPUTCS1MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

#define IOC_OUTPUTCS1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS1MS_START_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define IOC_OUTPUTCS1MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS1MS_START_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define IOC_OUTPUTCS1MS_STOP_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS1MS_START_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (32BIT)**/


#define IOC_OUTPUTCS1MS_STOP_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define IOC_OUTPUTCS1MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define IOC_OUTPUTCS1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS1MS_START_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define IOC_OUTPUTCS1MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS1MS_START_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define IOC_OUTPUTCS1MS_STOP_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS1MS_START_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (32BIT)**/


#define IOC_OUTPUTCS1MS_STOP_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define IOC_OUTPUTCS1MS_START_SEC_CODE
#include "Ioc_MemMap.h"

void Ioc_Output1msSRPortInit(void);
void Ioc_OutputCS1msConditioning(void);

static void Ioc_OutputCS1msSetPwmDuty(Ioc_PortType portId, Ioc_IoChannelType pwmCh, Haluint16 duty);
static void Ioc_OutputCS1ms_Dio_WriteChannel(Dio_ChannelType ChannelId, Dio_LevelType Level);

inline boolean Dio_ReadChannel_Direct(Ifx_P *port, uint8 pinIndex);
inline void Dio_WriteChannel_Direct(Ifx_P *port, uint8 pinIndex, Dio_LevelType Level);

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

void Ioc_Output1msSRPortInit(void)
{
    /* CAN Tranciver */
    Ioc_OutputCS1ms_Dio_WriteChannel(Ioc_DioMapping.M_CAN_EN, STD_HIGH);   /* IOC_OUT_PORT_M_CAN_EN */
    /* Fsr */
    Ioc_OutputCS1ms_Dio_WriteChannel(Ioc_DioMapping.FSR_CBS_OFF, STD_LOW);   /* IOC_OUT_PORT_FSR_CBS_OFF */
    Ioc_OutputCS1ms_Dio_WriteChannel(Ioc_DioMapping.FSR_ABS_OFF, STD_LOW);    /* IOC_OUT_PORT_FSR_ABS_OFF */
    /* Motor Gate Driver */
    /* TODO */
    /* SWD VARIANT - ESF */
    /* SWD VARIANT - MOT_DRV_RESET */
    /* SWD VARIANT - COAST */
    Ioc_OutputCS1ms_Dio_WriteChannel(Ioc_DioMapping.MOT_DRV_EN, STD_HIGH);   /* IOC_OUT_PORT_MOT_DRV_EN */
    /* Valve Driver */
    Ioc_OutputCS1ms_Dio_WriteChannel(Ioc_DioMapping.VDR_EN, STD_LOW);  /* IOC_OUT_PORT_VDR_EN */
    /* CSP */
    Ioc_OutputCS1ms_Dio_WriteChannel(Ioc_DioMapping.CSP_DRV, STD_HIGH);  /* IOC_OUT_PORT_CSP_DRV */
    /* PDT */
    Ioc_OutputCS1ms_Dio_WriteChannel(Ioc_DioMapping.PDT5V_INH, STD_HIGH);    /* IOC_OUT_PORT_PDT5V_INH */
    Ioc_OutputCS1ms_Dio_WriteChannel(Ioc_DioMapping.PDT5V_INH, STD_LOW);     /* TODO : Question. Why This channel toggles?  */
    /* EN DR*/
    Ioc_OutputCS1ms_Dio_WriteChannel(Ioc_DioMapping.EN_DR , STD_HIGH);
    /* VDD DRV*/
    Ioc_OutputCS1ms_Dio_WriteChannel(Ioc_DioMapping.VDD_DRV , STD_HIGH);	
}

uint16 T32_TOM1_Ch08_RlIvDrvData;
void Ioc_OutputCS1msConditioning(void)
{
    Saluint16 tempDrvData = 0;  /* TODO : this variable will be deleted after interface work */

/* NZ Quick C Sample */
    /* NZ Quick C Sample: CUT-P */
    Ioc_OutputCS1msSetPwmDuty(IOC_OUT_PORT_VLV_PRI_CUT, Pwm_17_GtmConf_PwmChannel_Valve_TOM1_Ch15, Ioc_OutputCS1msBus.Ioc_OutputCS1msNormVlvDrvInfo.PrimCutVlvDrvData);
    /* NZ Quick C Sample: CUT-S */
    Ioc_OutputCS1msSetPwmDuty(IOC_OUT_PORT_VLV_SEC_CUT, Pwm_17_GtmConf_PwmChannel_Valve_TOM1_Ch11, Ioc_OutputCS1msBus.Ioc_OutputCS1msNormVlvDrvInfo.SecdCutVlvDrvData);
    /* NZ Quick C Sample: RLV */
    Ioc_OutputCS1msSetPwmDuty(IOC_OUT_PORT_VLV_PRI_CV, Pwm_17_GtmConf_PwmChannel_Valve_TOM1_Ch13, Ioc_OutputCS1msBus.Ioc_OutputCS1msNormVlvDrvInfo.RelsVlvDrvData);
    /* NZ Quick C Sample: CV */
    Ioc_OutputCS1msSetPwmDuty(IOC_OUT_PORT_VLV_SEC_CV, Pwm_17_GtmConf_PwmChannel_Valve_TOM1_Ch12, Ioc_OutputCS1msBus.Ioc_OutputCS1msNormVlvDrvInfo.CircVlvDrvData);
    /* NZ Quick C Sample: SIM */
    Ioc_OutputCS1msSetPwmDuty(IOC_OUT_PORT_VLV_SIM, Pwm_17_GtmConf_PwmChannel_Valve_TOM1_Ch14, Ioc_OutputCS1msBus.Ioc_OutputCS1msNormVlvDrvInfo.SimVlvDrvData);

    Dio_WriteChannel_Direct(&MODULE_P00, 2, Ioc_OutputCS1msBus.Ioc_OutputCS1msRlyDrvInfo.RlyDbcDrv);
    Dio_WriteChannel_Direct(&MODULE_P22, 8, Ioc_OutputCS1msBus.Ioc_OutputCS1msRlyDrvInfo.RlyEssDrv);
    Dio_WriteChannel_Direct(&MODULE_P22, 11, Ioc_OutputCS1msBus.Ioc_OutputCS1msVlvDrvEnRst);

    Dio_WriteChannel_Direct(&MODULE_P21, 1, Ioc_OutputCS1msBus.Ioc_OutputCS1msMainCanEn);
    Dio_WriteChannel_Direct(&MODULE_P32, 5, Ioc_OutputCS1msBus.Ioc_OutputCS1msFsrAbsDrvInfo.FsrAbsOff);
    Dio_WriteChannel_Direct(&MODULE_P33, 14, Ioc_OutputCS1msBus.Ioc_OutputCS1msFsrCbsDrvInfo.FsrCbsOff);
    Dio_WriteChannel_Direct(&MODULE_P21, 4, Ioc_OutputCS1msBus.Ioc_OutputCS1msFsrEnDrDrv);
    Dio_WriteChannel_Direct(&MODULE_P33, 10, Ioc_OutputCS1msBus.Ioc_OutputCS1msSenPwrMonitorData.SenPwrM_12V_Drive_Req);
    //Dio_WriteChannel_Direct(&MODULE_P33, 7, Ioc_OutputCS1msBus.Ioc_OutputCS1msFsrDcMtrShutDwn);
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static void Ioc_OutputCS1msSetPwmDuty(Ioc_PortType portId, Ioc_IoChannelType pwmCh, Haluint16 duty)
{
    Ioc_MulFactorType mulFactor = Ioc_RoutingConfig[portId].MulFactor;
    Ioc_DivFactorType divFactor = Ioc_RoutingConfig[portId].DivFactor;

    Pwm_SetDutyCycle(pwmCh, ((Haluint32)duty * mulFactor) / divFactor);
}

static void Ioc_OutputCS1ms_Dio_WriteChannel(Dio_ChannelType ChannelId, Dio_LevelType Level)
{
    if(ChannelId != DIO_CHANNEL_NOT_USED)
    {
        Dio_WriteChannel(ChannelId, Level);
    }
}

inline boolean Dio_ReadChannel_Direct(Ifx_P *port, uint8 pinIndex)
{
    return (__getbit(&port->IN.U, pinIndex) != 0) ? TRUE : FALSE;
}

inline void Dio_WriteChannel_Direct(Ifx_P *port, uint8 pinIndex, Dio_LevelType Level)
{
    if(Level==STD_HIGH)
    {
        port->OMR.U = (uint32)(1 << pinIndex);
    }
    else
    {
        port->OMR.U = (uint32)((1 << 16)<< pinIndex);
    }
}

#define IOC_OUTPUTCS1MS_STOP_SEC_CODE
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
