/**
 * @defgroup Ioc_OutputCS5msCOnditioning Ioc_OutputCS5msCOnditioning
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_OutputCS5msCOnditioning.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ioc_OutputCS5ms.h"
#include "Dio.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define IOC_OUTPUTCS5MS_START_SEC_CONST_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define IOC_OUTPUTCS5MS_STOP_SEC_CONST_UNSPECIFIED
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define IOC_OUTPUTCS5MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

#define IOC_OUTPUTCS5MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS5MS_START_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define IOC_OUTPUTCS5MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS5MS_START_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define IOC_OUTPUTCS5MS_STOP_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS5MS_START_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (32BIT)**/


#define IOC_OUTPUTCS5MS_STOP_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define IOC_OUTPUTCS5MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define IOC_OUTPUTCS5MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS5MS_START_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define IOC_OUTPUTCS5MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS5MS_START_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define IOC_OUTPUTCS5MS_STOP_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS5MS_START_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (32BIT)**/


#define IOC_OUTPUTCS5MS_STOP_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define IOC_OUTPUTCS5MS_START_SEC_CODE
#include "Ioc_MemMap.h"

void Ioc_OutputCS55msPortInit(void);
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Ioc_OutputCS55msPortInit(void)
{
    /* Pedal */
    Dio_WriteChannel(DIO_CHANNEL_2_8, STD_LOW);     /* IOC_OUT_PORT_PDT5V_INH */
    Dio_WriteChannel(DIO_CHANNEL_20_10, STD_LOW);   /* IOC_OUT_PORT_PDF5V_INH */
    /* Power Relay */
    Dio_WriteChannel(DIO_CHANNEL_33_10, STD_HIGH);  /* IOC_OUT_PORT_CSP_DRV */
    Dio_WriteChannel(DIO_CHANNEL_14_8, STD_HIGH);   /* IOC_OUT_PORT_INHIBIT */
    /* Relay */
    Dio_WriteChannel(DIO_CHANNEL_2_11, STD_HIGH);   /* IOC_OUT_PORT_RELAY_EN */

#if 0 /* SWD VARIANT */
    /* Pedal */
    Dio_WriteChannel(DIO_CHANNEL_2_8, STD_LOW);
    Dio_WriteChannel(DIO_CHANNEL_20_10, STD_LOW);
    /* Power Relay */
    Dio_WriteChannel(DIO_CHANNEL_33_10, STD_HIGH);
    Dio_WriteChannel(DIO_CHANNEL_14_8, STD_HIGH);
    /* Relay */
    Dio_WriteChannel(DIO_CHANNEL_2_11, STD_HIGH);
#endif
}

/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define IOC_OUTPUTCS5MS_STOP_SEC_CODE
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
