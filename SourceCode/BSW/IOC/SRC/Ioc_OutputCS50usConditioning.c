/**
 * @defgroup Ioc_OutputCS50usConditioning Ioc_OutputCS50usConditioning
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_OutputCS50usConditioning.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ioc_OutputCS50us.h"
//#include "Pwm_17_Gtm.h"
#include "Pwm.h"
/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define IOC_OUTPUTCS50US_START_SEC_CONST_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define IOC_OUTPUTCS50US_STOP_SEC_CONST_UNSPECIFIED
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define IOC_OUTPUTCS50US_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

#define IOC_OUTPUTCS50US_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS50US_START_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define IOC_OUTPUTCS50US_STOP_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS50US_START_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define IOC_OUTPUTCS50US_STOP_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS50US_START_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (32BIT)**/


#define IOC_OUTPUTCS50US_STOP_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define IOC_OUTPUTCS50US_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define IOC_OUTPUTCS50US_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS50US_START_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define IOC_OUTPUTCS50US_STOP_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS50US_START_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define IOC_OUTPUTCS50US_STOP_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS50US_START_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (32BIT)**/


#define IOC_OUTPUTCS50US_STOP_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define IOC_OUTPUTCS50US_START_SEC_CODE
#include "Ioc_MemMap.h"
void Ioc_Output50usConditioning(void);

static void Ioc_Output50usSetPwmDuty(Ioc_PortType portId, Ioc_IoChannelType pwmCh, Haluint16 duty);
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Ioc_Output50usConditioning(void)
{
    /* U Port */
    Ioc_Output50usSetPwmDuty(IOC_OUT_PORT_MOT_U_HIGH, Pwm_17_GtmConf_PwmChannel_U_HIGH, Ioc_OutputCS50usBus.Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhUhighData);
    Ioc_Output50usSetPwmDuty(IOC_OUT_PORT_MOT_U_LOW,  Pwm_17_GtmConf_PwmChannel_U_LOW, Ioc_OutputCS50usBus.Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhUlowData);
    /* V Port */ /* Switched with W port on SWD15 specifications */
    Ioc_Output50usSetPwmDuty(IOC_OUT_PORT_MOT_V_HIGH, Pwm_17_GtmConf_PwmChannel_W_HIGH, Ioc_OutputCS50usBus.Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhVhighData);
    Ioc_Output50usSetPwmDuty(IOC_OUT_PORT_MOT_V_LOW,  Pwm_17_GtmConf_PwmChannel_W_LOW, Ioc_OutputCS50usBus.Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhVlowData);
    /* W Port */ /* Switched with V port on SWD15 specifications */
    Ioc_Output50usSetPwmDuty(IOC_OUT_PORT_MOT_W_HIGH, Pwm_17_GtmConf_PwmChannel_V_HIGH, Ioc_OutputCS50usBus.Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhWhighData);
    Ioc_Output50usSetPwmDuty(IOC_OUT_PORT_MOT_W_LOW,  Pwm_17_GtmConf_PwmChannel_V_LOW, Ioc_OutputCS50usBus.Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhWlowData);

#if 0	/* SWD VARIANT */
    /* U Port */
    Ioc_Output50usSetPwmDuty(IOC_OUT_PORT_MOT_U_HIGH, Pwm_17_GtmConf_PwmChannel_U_HIGH, Ioc_OutputCS50usBus.Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhUhighData);
    Ioc_Output50usSetPwmDuty(IOC_OUT_PORT_MOT_U_LOW,  Pwm_17_GtmConf_PwmChannel_U_LOW, Ioc_OutputCS50usBus.Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhUlowData);
    /* V Port */ /* Switched with W port on SWD15 specifications */
    Ioc_Output50usSetPwmDuty(IOC_OUT_PORT_MOT_V_HIGH, Pwm_17_GtmConf_PwmChannel_W_HIGH, Ioc_OutputCS50usBus.Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhVhighData);
    Ioc_Output50usSetPwmDuty(IOC_OUT_PORT_MOT_V_LOW,  Pwm_17_GtmConf_PwmChannel_W_LOW, Ioc_OutputCS50usBus.Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhVlowData);
    /* W Port */ /* Switched with V port on SWD15 specifications */
    Ioc_Output50usSetPwmDuty(IOC_OUT_PORT_MOT_W_HIGH, Pwm_17_GtmConf_PwmChannel_V_HIGH, Ioc_OutputCS50usBus.Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhWhighData);
    Ioc_Output50usSetPwmDuty(IOC_OUT_PORT_MOT_W_LOW,  Pwm_17_GtmConf_PwmChannel_V_LOW, Ioc_OutputCS50usBus.Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhWlowData);
#endif
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/
static void Ioc_Output50usSetPwmDuty(Ioc_PortType portId, Ioc_IoChannelType pwmCh, Haluint16 duty)
{
    Ioc_MulFactorType mulFactor = Ioc_RoutingConfig[portId].MulFactor;
    Ioc_DivFactorType divFactor = Ioc_RoutingConfig[portId].DivFactor;

    Pwm_SetDutyCycle(pwmCh, ((Haluint32)duty * mulFactor) / divFactor);
}

#define IOC_OUTPUTCS50US_STOP_SEC_CODE
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
