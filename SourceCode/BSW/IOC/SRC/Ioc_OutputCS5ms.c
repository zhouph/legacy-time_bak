/**
 * @defgroup Ioc_OutputCS5ms Ioc_OutputCS5ms
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_OutputCS5ms.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ioc_OutputCS5ms.h"
#include "Ioc_OutputCS5ms_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define IOC_OUTPUTCS5MS_START_SEC_CONST_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define IOC_OUTPUTCS5MS_STOP_SEC_CONST_UNSPECIFIED
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define IOC_OUTPUTCS5MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Ioc_OutputCS5ms_HdrBusType Ioc_OutputCS5msBus;

/* Version Info */
const SwcVersionInfo_t Ioc_OutputCS5msVersionInfo = 
{   
    IOC_OUTPUTCS5MS_MODULE_ID,           /* Ioc_OutputCS5msVersionInfo.ModuleId */
    IOC_OUTPUTCS5MS_MAJOR_VERSION,       /* Ioc_OutputCS5msVersionInfo.MajorVer */
    IOC_OUTPUTCS5MS_MINOR_VERSION,       /* Ioc_OutputCS5msVersionInfo.MinorVer */
    IOC_OUTPUTCS5MS_PATCH_VERSION,       /* Ioc_OutputCS5msVersionInfo.PatchVer */
    IOC_OUTPUTCS5MS_BRANCH_VERSION       /* Ioc_OutputCS5msVersionInfo.BranchVer */
};
    
/* Input Data Element */
Mom_HndlrEcuModeSts_t Ioc_OutputCS5msEcuModeSts;
Acm_MainAcmAsicInitCompleteFlag_t Ioc_OutputCS5msAcmAsicInitCompleteFlag;
Eem_SuspcDetnFuncInhibitIocSts_t Ioc_OutputCS5msFuncInhibitIocSts;

/* Output Data Element */

uint32 Ioc_OutputCS5ms_Timer_Start;
uint32 Ioc_OutputCS5ms_Timer_Elapsed;

#define IOC_OUTPUTCS5MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS5MS_START_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define IOC_OUTPUTCS5MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS5MS_START_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define IOC_OUTPUTCS5MS_STOP_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS5MS_START_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (32BIT)**/


#define IOC_OUTPUTCS5MS_STOP_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define IOC_OUTPUTCS5MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define IOC_OUTPUTCS5MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS5MS_START_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define IOC_OUTPUTCS5MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS5MS_START_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define IOC_OUTPUTCS5MS_STOP_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS5MS_START_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (32BIT)**/


#define IOC_OUTPUTCS5MS_STOP_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define IOC_OUTPUTCS5MS_START_SEC_CODE
#include "Ioc_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Ioc_OutputCS5ms_Init(void)
{
    /* Initialize internal bus */
    Ioc_OutputCS5msBus.Ioc_OutputCS5msEcuModeSts = 0;
    Ioc_OutputCS5msBus.Ioc_OutputCS5msAcmAsicInitCompleteFlag = 0;
    Ioc_OutputCS5msBus.Ioc_OutputCS5msFuncInhibitIocSts = 0;
	Ioc_OutputCS55msPortInit();
}

void Ioc_OutputCS5ms(void)
{
    Ioc_OutputCS5ms_Timer_Start = STM0_TIM0.U;

    /* Input */
    Ioc_OutputCS5ms_Read_Ioc_OutputCS5msEcuModeSts(&Ioc_OutputCS5msBus.Ioc_OutputCS5msEcuModeSts);
    Ioc_OutputCS5ms_Read_Ioc_OutputCS5msAcmAsicInitCompleteFlag(&Ioc_OutputCS5msBus.Ioc_OutputCS5msAcmAsicInitCompleteFlag);
    Ioc_OutputCS5ms_Read_Ioc_OutputCS5msFuncInhibitIocSts(&Ioc_OutputCS5msBus.Ioc_OutputCS5msFuncInhibitIocSts);

    /* Process */

    /* Output */

    Ioc_OutputCS5ms_Timer_Elapsed = STM0_TIM0.U - Ioc_OutputCS5ms_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define IOC_OUTPUTCS5MS_STOP_SEC_CODE
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
