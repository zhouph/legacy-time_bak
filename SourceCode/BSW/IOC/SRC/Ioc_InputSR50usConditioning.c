/**
 * @defgroup Ioc_InputSR50usConditioning Ioc_InputSR50usConditioning
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_InputSR50usConditioning.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ioc_InputSR50us.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define IOC_INPUTSR50US_START_SEC_CONST_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define IOC_INPUTSR50US_STOP_SEC_CONST_UNSPECIFIED
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define IOC_INPUTSR50US_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

#define IOC_INPUTSR50US_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_INPUTSR50US_START_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define IOC_INPUTSR50US_STOP_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
#define IOC_INPUTSR50US_START_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define IOC_INPUTSR50US_STOP_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_INPUTSR50US_START_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (32BIT)**/


#define IOC_INPUTSR50US_STOP_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define IOC_INPUTSR50US_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define IOC_INPUTSR50US_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_INPUTSR50US_START_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define IOC_INPUTSR50US_STOP_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
#define IOC_INPUTSR50US_START_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define IOC_INPUTSR50US_STOP_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_INPUTSR50US_START_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (32BIT)**/


#define IOC_INPUTSR50US_STOP_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define IOC_INPUTSR50US_START_SEC_CODE
#include "Ioc_MemMap.h"
void Ioc_InputSR50usConditioning(void);

static Ioc_DataType Ioc_InputSR50usGetVoltage(Ioc_PortType portId, Haluint16 rawVal);
static Ioc_DataType Ioc_InputSR50usGetAngle(Ioc_PortType portId, Haluint16 rawVal);
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Ioc_InputSR50usConditioning(void)
{
    Ioc_InputSR50usBus.Ioc_InputSR50usMotCurrMonInfo.MotCurrPhUMon0 = Ioc_InputSR50usGetVoltage(IOC_IN_PORT_MOT_U_PHASE_0_MON, Ioc_InputSR50usBus.Ioc_InputSR50usHwTrigMotInfo.Uphase0Mon);
    Ioc_InputSR50usBus.Ioc_InputSR50usMotCurrMonInfo.MotCurrPhUMon1 = Ioc_InputSR50usGetVoltage(IOC_IN_PORT_MOT_U_PHASE_1_MON, Ioc_InputSR50usBus.Ioc_InputSR50usHwTrigMotInfo.Uphase1Mon);
    Ioc_InputSR50usBus.Ioc_InputSR50usMotCurrMonInfo.MotCurrPhVMon0 = Ioc_InputSR50usGetVoltage(IOC_IN_PORT_MOT_V_PHASE_0_MON, Ioc_InputSR50usBus.Ioc_InputSR50usHwTrigMotInfo.Vphase0Mon);
    Ioc_InputSR50usBus.Ioc_InputSR50usMotCurrMonInfo.MotCurrPhVMon1 = Ioc_InputSR50usGetVoltage(IOC_IN_PORT_MOT_V_PHASE_1_MON, Ioc_InputSR50usBus.Ioc_InputSR50usHwTrigMotInfo.VPhase1Mon);

    Ioc_InputSR50usBus.Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle1raw = Ioc_InputSR50usGetVoltage(IOC_IN_PORT_MPS1_ANGLE_RAW, Ioc_InputSR50usBus.Ioc_InputSR50usMpsD1IIFAngInfo.D1IIFAngRawData);
    Ioc_InputSR50usBus.Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle1deg = Ioc_InputSR50usGetVoltage(IOC_IN_PORT_MPS1_ANGLE_DEG, Ioc_InputSR50usBus.Ioc_InputSR50usMpsD1IIFAngInfo.D1IIFAngDegree);
    Ioc_InputSR50usBus.Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle2raw = Ioc_InputSR50usGetVoltage(IOC_IN_PORT_MPS2_ANGLE_RAW, Ioc_InputSR50usBus.Ioc_InputSR50usMpsD2IIFAngInfo.D2IIFAngRawData);
    Ioc_InputSR50usBus.Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle2deg = Ioc_InputSR50usGetVoltage(IOC_IN_PORT_MPS2_ANGLE_DEG, Ioc_InputSR50usBus.Ioc_InputSR50usMpsD2IIFAngInfo.D2IIFAngDegree);

#if 0   /* SWD VARIANT */
    Ioc_InputSR50usBus.Ioc_InputSR50usMotCurrMonInfo.MotCurrPhUMon0 = Ioc_InputSR50usGetVoltage(IOC_IN_PORT_MOT_U_PHASE_0_MON, Ioc_InputSR50usBus.Ioc_InputSR50usHwTrigMotInfo.Uphase0Mon);
    Ioc_InputSR50usBus.Ioc_InputSR50usMotCurrMonInfo.MotCurrPhUMon1 = Ioc_InputSR50usGetVoltage(IOC_IN_PORT_MOT_U_PHASE_1_MON, Ioc_InputSR50usBus.Ioc_InputSR50usHwTrigMotInfo.Uphase1Mon);
    Ioc_InputSR50usBus.Ioc_InputSR50usMotCurrMonInfo.MotCurrPhVMon0 = Ioc_InputSR50usGetVoltage(IOC_IN_PORT_MOT_V_PHASE_0_MON, Ioc_InputSR50usBus.Ioc_InputSR50usHwTrigMotInfo.Vphase0Mon);
    Ioc_InputSR50usBus.Ioc_InputSR50usMotCurrMonInfo.MotCurrPhVMon1 = Ioc_InputSR50usGetVoltage(IOC_IN_PORT_MOT_V_PHASE_1_MON, Ioc_InputSR50usBus.Ioc_InputSR50usHwTrigMotInfo.VPhase1Mon);
    Ioc_InputSR50usBus.Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle1 = Ioc_InputSR50usGetAngle(IOC_IN_PORT_MPS1_ANGLE, Ioc_InputSR50usBus.Ioc_InputSR50usRxMpsAngleInfo.Mps1Angle);
    Ioc_InputSR50usBus.Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle2 = Ioc_InputSR50usGetAngle(IOC_IN_PORT_MPS2_ANGLE, Ioc_InputSR50usBus.Ioc_InputSR50usRxMpsAngleInfo.Mps2Angle);
#endif
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

 static Ioc_DataType Ioc_InputSR50usGetVoltage(Ioc_PortType portId, Haluint16 rawVal)
 {
     Ioc_DataType retVal = 0;
     Ioc_MulFactorType mulFactor = Ioc_RoutingConfig[portId].MulFactor;
     Ioc_DivFactorType divFactor = Ioc_RoutingConfig[portId].DivFactor;
     
     retVal = ((Haluint32)rawVal * mulFactor)/ divFactor;

     return retVal;
 }

 static Ioc_DataType Ioc_InputSR50usGetAngle(Ioc_PortType portId, Haluint16 rawVal)
 {
   Ioc_DataType retVal = 0;
   Ioc_MulFactorType mulFactor = Ioc_RoutingConfig[portId].MulFactor;
   Ioc_DivFactorType divFactor = Ioc_RoutingConfig[portId].DivFactor;
   
   retVal = ((uint32)rawVal * mulFactor)/ divFactor;

   return retVal;
 }

#define IOC_INPUTSR50US_STOP_SEC_CODE
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
