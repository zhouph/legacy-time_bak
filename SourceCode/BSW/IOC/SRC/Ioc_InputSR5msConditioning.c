/**
 * @defgroup Ioc_InputSR5msConditioning Ioc_InputSR5msConditioning
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_InputSR5msConditioning.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ioc_InputSR5ms.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define IOC_INPUTSR5MS_START_SEC_CONST_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define IOC_INPUTSR5MS_STOP_SEC_CONST_UNSPECIFIED
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define IOC_INPUTSR5MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/

#define IOC_INPUTSR5MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_INPUTSR5MS_START_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define IOC_INPUTSR5MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
#define IOC_INPUTSR5MS_START_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define IOC_INPUTSR5MS_STOP_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_INPUTSR5MS_START_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (32BIT)**/


#define IOC_INPUTSR5MS_STOP_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define IOC_INPUTSR5MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define IOC_INPUTSR5MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_INPUTSR5MS_START_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define IOC_INPUTSR5MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
#define IOC_INPUTSR5MS_START_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define IOC_INPUTSR5MS_STOP_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_INPUTSR5MS_START_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (32BIT)**/


#define IOC_INPUTSR5MS_STOP_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define IOC_INPUTSR5MS_START_SEC_CODE
#include "Ioc_MemMap.h"

void Ioc_InputSR5msConditioning(void);
/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/

void Ioc_InputSR5msConditioning(void)
{
    Haluint16 i;
    Ioc_InputSR5msBus.Ioc_InputSR5msWssMonInfo.FlRisngIdx = Ioc_InputSR5msBus.Ioc_InputSR5msRisngIdxInfo.FlRisngIdx;
    Ioc_InputSR5msBus.Ioc_InputSR5msWssMonInfo.FrRisngIdx = Ioc_InputSR5msBus.Ioc_InputSR5msRisngIdxInfo.FrRisngIdx;
    Ioc_InputSR5msBus.Ioc_InputSR5msWssMonInfo.RlRisngIdx = Ioc_InputSR5msBus.Ioc_InputSR5msRisngIdxInfo.RlRisngIdx;
    Ioc_InputSR5msBus.Ioc_InputSR5msWssMonInfo.RrRisngIdx = Ioc_InputSR5msBus.Ioc_InputSR5msRisngIdxInfo.RrRisngIdx;
    Ioc_InputSR5msBus.Ioc_InputSR5msWssMonInfo.FlFallIdx = Ioc_InputSR5msBus.Ioc_InputSR5msFallIdxInfo.FlFallIdx;
    Ioc_InputSR5msBus.Ioc_InputSR5msWssMonInfo.FrFallIdx = Ioc_InputSR5msBus.Ioc_InputSR5msFallIdxInfo.FrFallIdx;
    Ioc_InputSR5msBus.Ioc_InputSR5msWssMonInfo.RlFallIdx = Ioc_InputSR5msBus.Ioc_InputSR5msFallIdxInfo.RlFallIdx;
    Ioc_InputSR5msBus.Ioc_InputSR5msWssMonInfo.RrFallIdx = Ioc_InputSR5msBus.Ioc_InputSR5msFallIdxInfo.RrFallIdx;
    for(i=0;i<32;i++) Ioc_InputSR5msBus.Ioc_InputSR5msWssMonInfo.FlRisngTiStamp[i] = Ioc_InputSR5msBus.Ioc_InputSR5msRisngTiStampInfo.FlRisngTiStamp[i];
    for(i=0;i<32;i++) Ioc_InputSR5msBus.Ioc_InputSR5msWssMonInfo.FrRisngTiStamp[i] = Ioc_InputSR5msBus.Ioc_InputSR5msRisngTiStampInfo.FrRisngTiStamp[i];
    for(i=0;i<32;i++) Ioc_InputSR5msBus.Ioc_InputSR5msWssMonInfo.RlRisngTiStamp[i] = Ioc_InputSR5msBus.Ioc_InputSR5msRisngTiStampInfo.RlRisngTiStamp[i];
    for(i=0;i<32;i++) Ioc_InputSR5msBus.Ioc_InputSR5msWssMonInfo.RrRisngTiStamp[i] = Ioc_InputSR5msBus.Ioc_InputSR5msRisngTiStampInfo.RrRisngTiStamp[i];
    for(i=0;i<32;i++) Ioc_InputSR5msBus.Ioc_InputSR5msWssMonInfo.FlFallTiStamp[i] = Ioc_InputSR5msBus.Ioc_InputSR5msFallTiStampInfo.FlFallTiStamp[i];
    for(i=0;i<32;i++) Ioc_InputSR5msBus.Ioc_InputSR5msWssMonInfo.FrFallTiStamp[i] = Ioc_InputSR5msBus.Ioc_InputSR5msFallTiStampInfo.FrFallTiStamp[i];
    for(i=0;i<32;i++) Ioc_InputSR5msBus.Ioc_InputSR5msWssMonInfo.RlFallTiStamp[i] = Ioc_InputSR5msBus.Ioc_InputSR5msFallTiStampInfo.RlFallTiStamp[i];
    for(i=0;i<32;i++) Ioc_InputSR5msBus.Ioc_InputSR5msWssMonInfo.RrFallTiStamp[i] = Ioc_InputSR5msBus.Ioc_InputSR5msFallTiStampInfo.RrFallTiStamp[i];
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define IOC_INPUTSR5MS_STOP_SEC_CODE
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
