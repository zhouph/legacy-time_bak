/**
 * @defgroup Ioc_OutputCS1ms Ioc_OutputCS1ms
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_OutputCS1ms.c
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/
#include "Ioc_OutputCS1ms.h"
#include "Ioc_OutputCS1ms_Ifa.h"
#include "IfxStm_reg.h"

/*==============================================================================
 *                  LOCAL MACROS AND DEFINES
 =============================================================================*/

/*==============================================================================
 *                  LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DEFINITIONS
 =============================================================================*/
#define IOC_OUTPUTCS1MS_START_SEC_CONST_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Global Constant Section (UNSPECIFIED)**/

/*==============================================================================
 *                  LOCAL CONSTANT DEFINITIONS
 =============================================================================*/
/** Local Constant Section (UNSPECIFIED)**/

#define IOC_OUTPUTCS1MS_STOP_SEC_CONST_UNSPECIFIED
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  GLOBAL VARIABLE DEFINITIONS
 =============================================================================*/
#define IOC_OUTPUTCS1MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/
/* Internal Bus */
Ioc_OutputCS1ms_HdrBusType Ioc_OutputCS1msBus;

/* Version Info */
const SwcVersionInfo_t Ioc_OutputCS1msVersionInfo = 
{   
    IOC_OUTPUTCS1MS_MODULE_ID,           /* Ioc_OutputCS1msVersionInfo.ModuleId */
    IOC_OUTPUTCS1MS_MAJOR_VERSION,       /* Ioc_OutputCS1msVersionInfo.MajorVer */
    IOC_OUTPUTCS1MS_MINOR_VERSION,       /* Ioc_OutputCS1msVersionInfo.MinorVer */
    IOC_OUTPUTCS1MS_PATCH_VERSION,       /* Ioc_OutputCS1msVersionInfo.PatchVer */
    IOC_OUTPUTCS1MS_BRANCH_VERSION       /* Ioc_OutputCS1msVersionInfo.BranchVer */
};
    
/* Input Data Element */
Vlv_ActrNormVlvDrvInfo_t Ioc_OutputCS1msNormVlvDrvInfo;
Vlv_ActrWhlVlvDrvInfo_t Ioc_OutputCS1msWhlVlvDrvInfo;
Rly_ActrRlyDrvInfo_t Ioc_OutputCS1msRlyDrvInfo;
Fsr_ActrFsrAbsDrvInfo_t Ioc_OutputCS1msFsrAbsDrvInfo;
Fsr_ActrFsrCbsDrvInfo_t Ioc_OutputCS1msFsrCbsDrvInfo;
SenPwrM_MainSenPwrMonitor_t Ioc_OutputCS1msSenPwrMonitorData;
Mom_HndlrEcuModeSts_t Ioc_OutputCS1msEcuModeSts;
Acm_MainAcmAsicInitCompleteFlag_t Ioc_OutputCS1msAcmAsicInitCompleteFlag;
Eem_SuspcDetnFuncInhibitIocSts_t Ioc_OutputCS1msFuncInhibitIocSts;
CanTrv_TLE6251_HndlrMainCanEn_t Ioc_OutputCS1msMainCanEn;
Vlvd_A3944_HndlrVlvDrvEnRst_t Ioc_OutputCS1msVlvDrvEnRst;
Awd_mainAwdPrnDrv_t Ioc_OutputCS1msAwdPrnDrv;
Fsr_ActrFsrDcMtrShutDwn_t Ioc_OutputCS1msFsrDcMtrShutDwn;
Fsr_ActrFsrEnDrDrv_t Ioc_OutputCS1msFsrEnDrDrv;

/* Output Data Element */

uint32 Ioc_OutputCS1ms_Timer_Start;
uint32 Ioc_OutputCS1ms_Timer_Elapsed;

#define IOC_OUTPUTCS1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS1MS_START_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define IOC_OUTPUTCS1MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS1MS_START_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define IOC_OUTPUTCS1MS_STOP_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS1MS_START_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (32BIT)**/


#define IOC_OUTPUTCS1MS_STOP_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  LOCAL VARIABLE DEFINITIONS
 =============================================================================*/
#define IOC_OUTPUTCS1MS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_UNSPECIFIED)**/


#define IOC_OUTPUTCS1MS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS1MS_START_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (NOINIT_32BIT)**/


#define IOC_OUTPUTCS1MS_STOP_SEC_VAR_NOINIT_32BIT
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS1MS_START_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
/** Variable Section (UNSPECIFIED)**/


#define IOC_OUTPUTCS1MS_STOP_SEC_VAR_UNSPECIFIED
#include "Ioc_MemMap.h"
#define IOC_OUTPUTCS1MS_START_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/** Variable Section (32BIT)**/


#define IOC_OUTPUTCS1MS_STOP_SEC_VAR_32BIT
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  LOCAL FUNCTION PROTOTYPES
 =============================================================================*/
#define IOC_OUTPUTCS1MS_START_SEC_CODE
#include "Ioc_MemMap.h"

/*==============================================================================
 *                  GLOBAL FUNCTIONS
 =============================================================================*/
void Ioc_OutputCS1ms_Init(void)
{
    /* Initialize internal bus */
    Ioc_OutputCS1msBus.Ioc_OutputCS1msNormVlvDrvInfo.PrimCutVlvDrvData = 0;
    Ioc_OutputCS1msBus.Ioc_OutputCS1msNormVlvDrvInfo.RelsVlvDrvData = 0;
    Ioc_OutputCS1msBus.Ioc_OutputCS1msNormVlvDrvInfo.SecdCutVlvDrvData = 0;
    Ioc_OutputCS1msBus.Ioc_OutputCS1msNormVlvDrvInfo.CircVlvDrvData = 0;
    Ioc_OutputCS1msBus.Ioc_OutputCS1msNormVlvDrvInfo.SimVlvDrvData = 0;
    Ioc_OutputCS1msBus.Ioc_OutputCS1msWhlVlvDrvInfo.FlOvDrvData = 0;
    Ioc_OutputCS1msBus.Ioc_OutputCS1msWhlVlvDrvInfo.FlIvDrvData = 0;
    Ioc_OutputCS1msBus.Ioc_OutputCS1msWhlVlvDrvInfo.FrOvDrvData = 0;
    Ioc_OutputCS1msBus.Ioc_OutputCS1msWhlVlvDrvInfo.FrIvDrvData = 0;
    Ioc_OutputCS1msBus.Ioc_OutputCS1msWhlVlvDrvInfo.RlOvDrvData = 0;
    Ioc_OutputCS1msBus.Ioc_OutputCS1msWhlVlvDrvInfo.RlIvDrvData = 0;
    Ioc_OutputCS1msBus.Ioc_OutputCS1msWhlVlvDrvInfo.RrOvDrvData = 0;
    Ioc_OutputCS1msBus.Ioc_OutputCS1msWhlVlvDrvInfo.RrIvDrvData = 0;
    Ioc_OutputCS1msBus.Ioc_OutputCS1msRlyDrvInfo.RlyDbcDrv = 0;
    Ioc_OutputCS1msBus.Ioc_OutputCS1msRlyDrvInfo.RlyEssDrv = 0;
    Ioc_OutputCS1msBus.Ioc_OutputCS1msFsrAbsDrvInfo.FsrAbsOff = 0;
    Ioc_OutputCS1msBus.Ioc_OutputCS1msFsrCbsDrvInfo.FsrCbsOff = 0;
    Ioc_OutputCS1msBus.Ioc_OutputCS1msSenPwrMonitorData.SenPwrM_12V_Drive_Req = 0;
    Ioc_OutputCS1msBus.Ioc_OutputCS1msEcuModeSts = 0;
    Ioc_OutputCS1msBus.Ioc_OutputCS1msAcmAsicInitCompleteFlag = 0;
    Ioc_OutputCS1msBus.Ioc_OutputCS1msFuncInhibitIocSts = 0;
    Ioc_OutputCS1msBus.Ioc_OutputCS1msMainCanEn = 0;
    Ioc_OutputCS1msBus.Ioc_OutputCS1msVlvDrvEnRst = 0;
    Ioc_OutputCS1msBus.Ioc_OutputCS1msAwdPrnDrv = 0;
    Ioc_OutputCS1msBus.Ioc_OutputCS1msFsrDcMtrShutDwn = 0;
    Ioc_OutputCS1msBus.Ioc_OutputCS1msFsrEnDrDrv = 0;
	Ioc_Output1msSRPortInit();
}

void Ioc_OutputCS1ms(void)
{
    Ioc_OutputCS1ms_Timer_Start = STM0_TIM0.U;

    /* Input */
    Ioc_OutputCS1ms_Read_Ioc_OutputCS1msNormVlvDrvInfo(&Ioc_OutputCS1msBus.Ioc_OutputCS1msNormVlvDrvInfo);
    /*==============================================================================
    * Members of structure Ioc_OutputCS1msNormVlvDrvInfo 
     : Ioc_OutputCS1msNormVlvDrvInfo.PrimCutVlvDrvData;
     : Ioc_OutputCS1msNormVlvDrvInfo.RelsVlvDrvData;
     : Ioc_OutputCS1msNormVlvDrvInfo.SecdCutVlvDrvData;
     : Ioc_OutputCS1msNormVlvDrvInfo.CircVlvDrvData;
     : Ioc_OutputCS1msNormVlvDrvInfo.SimVlvDrvData;
     =============================================================================*/
    
    Ioc_OutputCS1ms_Read_Ioc_OutputCS1msWhlVlvDrvInfo(&Ioc_OutputCS1msBus.Ioc_OutputCS1msWhlVlvDrvInfo);
    /*==============================================================================
    * Members of structure Ioc_OutputCS1msWhlVlvDrvInfo 
     : Ioc_OutputCS1msWhlVlvDrvInfo.FlOvDrvData;
     : Ioc_OutputCS1msWhlVlvDrvInfo.FlIvDrvData;
     : Ioc_OutputCS1msWhlVlvDrvInfo.FrOvDrvData;
     : Ioc_OutputCS1msWhlVlvDrvInfo.FrIvDrvData;
     : Ioc_OutputCS1msWhlVlvDrvInfo.RlOvDrvData;
     : Ioc_OutputCS1msWhlVlvDrvInfo.RlIvDrvData;
     : Ioc_OutputCS1msWhlVlvDrvInfo.RrOvDrvData;
     : Ioc_OutputCS1msWhlVlvDrvInfo.RrIvDrvData;
     =============================================================================*/
    
    Ioc_OutputCS1ms_Read_Ioc_OutputCS1msRlyDrvInfo(&Ioc_OutputCS1msBus.Ioc_OutputCS1msRlyDrvInfo);
    /*==============================================================================
    * Members of structure Ioc_OutputCS1msRlyDrvInfo 
     : Ioc_OutputCS1msRlyDrvInfo.RlyDbcDrv;
     : Ioc_OutputCS1msRlyDrvInfo.RlyEssDrv;
     =============================================================================*/
    
    /* Decomposed structure interface */
    Ioc_OutputCS1ms_Read_Ioc_OutputCS1msFsrAbsDrvInfo_FsrAbsOff(&Ioc_OutputCS1msBus.Ioc_OutputCS1msFsrAbsDrvInfo.FsrAbsOff);

    /* Decomposed structure interface */
    Ioc_OutputCS1ms_Read_Ioc_OutputCS1msFsrCbsDrvInfo_FsrCbsOff(&Ioc_OutputCS1msBus.Ioc_OutputCS1msFsrCbsDrvInfo.FsrCbsOff);

    /* Decomposed structure interface */
    Ioc_OutputCS1ms_Read_Ioc_OutputCS1msSenPwrMonitorData_SenPwrM_12V_Drive_Req(&Ioc_OutputCS1msBus.Ioc_OutputCS1msSenPwrMonitorData.SenPwrM_12V_Drive_Req);

    Ioc_OutputCS1ms_Read_Ioc_OutputCS1msEcuModeSts(&Ioc_OutputCS1msBus.Ioc_OutputCS1msEcuModeSts);
    Ioc_OutputCS1ms_Read_Ioc_OutputCS1msAcmAsicInitCompleteFlag(&Ioc_OutputCS1msBus.Ioc_OutputCS1msAcmAsicInitCompleteFlag);
    Ioc_OutputCS1ms_Read_Ioc_OutputCS1msFuncInhibitIocSts(&Ioc_OutputCS1msBus.Ioc_OutputCS1msFuncInhibitIocSts);
    Ioc_OutputCS1ms_Read_Ioc_OutputCS1msMainCanEn(&Ioc_OutputCS1msBus.Ioc_OutputCS1msMainCanEn);
    Ioc_OutputCS1ms_Read_Ioc_OutputCS1msVlvDrvEnRst(&Ioc_OutputCS1msBus.Ioc_OutputCS1msVlvDrvEnRst);
    Ioc_OutputCS1ms_Read_Ioc_OutputCS1msAwdPrnDrv(&Ioc_OutputCS1msBus.Ioc_OutputCS1msAwdPrnDrv);
    Ioc_OutputCS1ms_Read_Ioc_OutputCS1msFsrDcMtrShutDwn(&Ioc_OutputCS1msBus.Ioc_OutputCS1msFsrDcMtrShutDwn);
    Ioc_OutputCS1ms_Read_Ioc_OutputCS1msFsrEnDrDrv(&Ioc_OutputCS1msBus.Ioc_OutputCS1msFsrEnDrDrv);

    /* Process */
    Ioc_OutputCS1msConditioning();

    /* Output */

    Ioc_OutputCS1ms_Timer_Elapsed = STM0_TIM0.U - Ioc_OutputCS1ms_Timer_Start;
}
/*==============================================================================
 *                  LOCAL FUNCTIONS
 =============================================================================*/

#define IOC_OUTPUTCS1MS_STOP_SEC_CODE
#include "Ioc_MemMap.h"
/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
/** @} */
