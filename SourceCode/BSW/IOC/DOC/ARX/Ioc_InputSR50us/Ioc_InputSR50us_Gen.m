Ioc_InputSR50usMpsD1IIFAngInfo = Simulink.Bus;
DeList={
    'D1IIFAngRawData'
    'D1IIFAngDegree'
    };
Ioc_InputSR50usMpsD1IIFAngInfo = CreateBus(Ioc_InputSR50usMpsD1IIFAngInfo, DeList);
clear DeList;

Ioc_InputSR50usMpsD2IIFAngInfo = Simulink.Bus;
DeList={
    'D2IIFAngRawData'
    'D2IIFAngDegree'
    };
Ioc_InputSR50usMpsD2IIFAngInfo = CreateBus(Ioc_InputSR50usMpsD2IIFAngInfo, DeList);
clear DeList;

Ioc_InputSR50usHwTrigMotInfo = Simulink.Bus;
DeList={
    'Uphase0Mon'
    'Uphase1Mon'
    'Vphase0Mon'
    'VPhase1Mon'
    };
Ioc_InputSR50usHwTrigMotInfo = CreateBus(Ioc_InputSR50usHwTrigMotInfo, DeList);
clear DeList;

Ioc_InputSR50usEcuModeSts = Simulink.Bus;
DeList={'Ioc_InputSR50usEcuModeSts'};
Ioc_InputSR50usEcuModeSts = CreateBus(Ioc_InputSR50usEcuModeSts, DeList);
clear DeList;

Ioc_InputSR50usFuncInhibitIocSts = Simulink.Bus;
DeList={'Ioc_InputSR50usFuncInhibitIocSts'};
Ioc_InputSR50usFuncInhibitIocSts = CreateBus(Ioc_InputSR50usFuncInhibitIocSts, DeList);
clear DeList;

Ioc_InputSR50usMotCurrMonInfo = Simulink.Bus;
DeList={
    'MotCurrPhUMon0'
    'MotCurrPhUMon1'
    'MotCurrPhVMon0'
    'MotCurrPhVMon1'
    };
Ioc_InputSR50usMotCurrMonInfo = CreateBus(Ioc_InputSR50usMotCurrMonInfo, DeList);
clear DeList;

Ioc_InputSR50usMotAngleMonInfo = Simulink.Bus;
DeList={
    'MotPosiAngle1deg'
    'MotPosiAngle2deg'
    'MotPosiAngle1raw'
    'MotPosiAngle2raw'
    };
Ioc_InputSR50usMotAngleMonInfo = CreateBus(Ioc_InputSR50usMotAngleMonInfo, DeList);
clear DeList;

