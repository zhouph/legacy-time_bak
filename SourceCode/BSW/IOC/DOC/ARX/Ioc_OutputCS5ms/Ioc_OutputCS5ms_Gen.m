Ioc_OutputCS5msEcuModeSts = Simulink.Bus;
DeList={'Ioc_OutputCS5msEcuModeSts'};
Ioc_OutputCS5msEcuModeSts = CreateBus(Ioc_OutputCS5msEcuModeSts, DeList);
clear DeList;

Ioc_OutputCS5msAcmAsicInitCompleteFlag = Simulink.Bus;
DeList={'Ioc_OutputCS5msAcmAsicInitCompleteFlag'};
Ioc_OutputCS5msAcmAsicInitCompleteFlag = CreateBus(Ioc_OutputCS5msAcmAsicInitCompleteFlag, DeList);
clear DeList;

Ioc_OutputCS5msFuncInhibitIocSts = Simulink.Bus;
DeList={'Ioc_OutputCS5msFuncInhibitIocSts'};
Ioc_OutputCS5msFuncInhibitIocSts = CreateBus(Ioc_OutputCS5msFuncInhibitIocSts, DeList);
clear DeList;

