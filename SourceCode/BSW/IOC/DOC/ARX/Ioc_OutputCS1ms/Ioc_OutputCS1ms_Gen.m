Ioc_OutputCS1msNormVlvDrvInfo = Simulink.Bus;
DeList={
    'PrimCutVlvDrvData'
    'RelsVlvDrvData'
    'SecdCutVlvDrvData'
    'CircVlvDrvData'
    'SimVlvDrvData'
    };
Ioc_OutputCS1msNormVlvDrvInfo = CreateBus(Ioc_OutputCS1msNormVlvDrvInfo, DeList);
clear DeList;

Ioc_OutputCS1msWhlVlvDrvInfo = Simulink.Bus;
DeList={
    'FlOvDrvData'
    'FlIvDrvData'
    'FrOvDrvData'
    'FrIvDrvData'
    'RlOvDrvData'
    'RlIvDrvData'
    'RrOvDrvData'
    'RrIvDrvData'
    };
Ioc_OutputCS1msWhlVlvDrvInfo = CreateBus(Ioc_OutputCS1msWhlVlvDrvInfo, DeList);
clear DeList;

Ioc_OutputCS1msRlyDrvInfo = Simulink.Bus;
DeList={
    'RlyDbcDrv'
    'RlyEssDrv'
    };
Ioc_OutputCS1msRlyDrvInfo = CreateBus(Ioc_OutputCS1msRlyDrvInfo, DeList);
clear DeList;

Ioc_OutputCS1msFsrAbsDrvInfo = Simulink.Bus;
DeList={
    'FsrAbsOff'
    };
Ioc_OutputCS1msFsrAbsDrvInfo = CreateBus(Ioc_OutputCS1msFsrAbsDrvInfo, DeList);
clear DeList;

Ioc_OutputCS1msFsrCbsDrvInfo = Simulink.Bus;
DeList={
    'FsrCbsOff'
    };
Ioc_OutputCS1msFsrCbsDrvInfo = CreateBus(Ioc_OutputCS1msFsrCbsDrvInfo, DeList);
clear DeList;

Ioc_OutputCS1msSenPwrMonitorData = Simulink.Bus;
DeList={
    'SenPwrM_12V_Drive_Req'
    };
Ioc_OutputCS1msSenPwrMonitorData = CreateBus(Ioc_OutputCS1msSenPwrMonitorData, DeList);
clear DeList;

Ioc_OutputCS1msEcuModeSts = Simulink.Bus;
DeList={'Ioc_OutputCS1msEcuModeSts'};
Ioc_OutputCS1msEcuModeSts = CreateBus(Ioc_OutputCS1msEcuModeSts, DeList);
clear DeList;

Ioc_OutputCS1msAcmAsicInitCompleteFlag = Simulink.Bus;
DeList={'Ioc_OutputCS1msAcmAsicInitCompleteFlag'};
Ioc_OutputCS1msAcmAsicInitCompleteFlag = CreateBus(Ioc_OutputCS1msAcmAsicInitCompleteFlag, DeList);
clear DeList;

Ioc_OutputCS1msFuncInhibitIocSts = Simulink.Bus;
DeList={'Ioc_OutputCS1msFuncInhibitIocSts'};
Ioc_OutputCS1msFuncInhibitIocSts = CreateBus(Ioc_OutputCS1msFuncInhibitIocSts, DeList);
clear DeList;

Ioc_OutputCS1msMainCanEn = Simulink.Bus;
DeList={'Ioc_OutputCS1msMainCanEn'};
Ioc_OutputCS1msMainCanEn = CreateBus(Ioc_OutputCS1msMainCanEn, DeList);
clear DeList;

Ioc_OutputCS1msVlvDrvEnRst = Simulink.Bus;
DeList={'Ioc_OutputCS1msVlvDrvEnRst'};
Ioc_OutputCS1msVlvDrvEnRst = CreateBus(Ioc_OutputCS1msVlvDrvEnRst, DeList);
clear DeList;

Ioc_OutputCS1msAwdPrnDrv = Simulink.Bus;
DeList={'Ioc_OutputCS1msAwdPrnDrv'};
Ioc_OutputCS1msAwdPrnDrv = CreateBus(Ioc_OutputCS1msAwdPrnDrv, DeList);
clear DeList;

Ioc_OutputCS1msFsrDcMtrShutDwn = Simulink.Bus;
DeList={'Ioc_OutputCS1msFsrDcMtrShutDwn'};
Ioc_OutputCS1msFsrDcMtrShutDwn = CreateBus(Ioc_OutputCS1msFsrDcMtrShutDwn, DeList);
clear DeList;

Ioc_OutputCS1msFsrEnDrDrv = Simulink.Bus;
DeList={'Ioc_OutputCS1msFsrEnDrDrv'};
Ioc_OutputCS1msFsrEnDrDrv = CreateBus(Ioc_OutputCS1msFsrEnDrDrv, DeList);
clear DeList;

