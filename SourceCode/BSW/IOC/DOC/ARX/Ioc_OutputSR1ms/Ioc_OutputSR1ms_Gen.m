Ioc_OutputSR1msWhlVlvDrvInfo = Simulink.Bus;
DeList={
    'FlOvDrvData'
    'FlIvDrvData'
    'FrOvDrvData'
    'FrIvDrvData'
    'RlOvDrvData'
    'RlIvDrvData'
    'RrOvDrvData'
    'RrIvDrvData'
    };
Ioc_OutputSR1msWhlVlvDrvInfo = CreateBus(Ioc_OutputSR1msWhlVlvDrvInfo, DeList);
clear DeList;

Ioc_OutputSR1msBalVlvDrvInfo = Simulink.Bus;
DeList={
    'PrimBalVlvDrvData'
    'PressDumpVlvDrvData'
    'ChmbBalVlvDrvData'
    };
Ioc_OutputSR1msBalVlvDrvInfo = CreateBus(Ioc_OutputSR1msBalVlvDrvInfo, DeList);
clear DeList;

Ioc_OutputSR1msFsrCbsDrvInfo = Simulink.Bus;
DeList={
    'FsrCbsDrv'
    };
Ioc_OutputSR1msFsrCbsDrvInfo = CreateBus(Ioc_OutputSR1msFsrCbsDrvInfo, DeList);
clear DeList;

Ioc_OutputSR1msFsrAbsDrvInfo = Simulink.Bus;
DeList={
    'FsrAbsDrv'
    };
Ioc_OutputSR1msFsrAbsDrvInfo = CreateBus(Ioc_OutputSR1msFsrAbsDrvInfo, DeList);
clear DeList;

Ioc_OutputSR1msEcuModeSts = Simulink.Bus;
DeList={'Ioc_OutputSR1msEcuModeSts'};
Ioc_OutputSR1msEcuModeSts = CreateBus(Ioc_OutputSR1msEcuModeSts, DeList);
clear DeList;

Ioc_OutputSR1msAcmAsicInitCompleteFlag = Simulink.Bus;
DeList={'Ioc_OutputSR1msAcmAsicInitCompleteFlag'};
Ioc_OutputSR1msAcmAsicInitCompleteFlag = CreateBus(Ioc_OutputSR1msAcmAsicInitCompleteFlag, DeList);
clear DeList;

Ioc_OutputSR1msFuncInhibitIocSts = Simulink.Bus;
DeList={'Ioc_OutputSR1msFuncInhibitIocSts'};
Ioc_OutputSR1msFuncInhibitIocSts = CreateBus(Ioc_OutputSR1msFuncInhibitIocSts, DeList);
clear DeList;

Ioc_OutputSR1msIocDcMtrDutyData = Simulink.Bus;
DeList={
    'MtrDutyDataFlg'
    'ACH_Tx8_PUMP_DTY_PWM'
    };
Ioc_OutputSR1msIocDcMtrDutyData = CreateBus(Ioc_OutputSR1msIocDcMtrDutyData, DeList);
clear DeList;

Ioc_OutputSR1msIocDcMtrFreqData = Simulink.Bus;
DeList={
    'MtrFreqDataFlg'
    'ACH_Tx8_PUMP_TCK_PWM'
    };
Ioc_OutputSR1msIocDcMtrFreqData = CreateBus(Ioc_OutputSR1msIocDcMtrFreqData, DeList);
clear DeList;

Ioc_OutputSR1msIocVlvNo0Data = Simulink.Bus;
DeList={
    'VlvNo0DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ioc_OutputSR1msIocVlvNo0Data = CreateBus(Ioc_OutputSR1msIocVlvNo0Data, DeList);
clear DeList;

Ioc_OutputSR1msIocVlvNo1Data = Simulink.Bus;
DeList={
    'VlvNo1DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ioc_OutputSR1msIocVlvNo1Data = CreateBus(Ioc_OutputSR1msIocVlvNo1Data, DeList);
clear DeList;

Ioc_OutputSR1msIocVlvNo2Data = Simulink.Bus;
DeList={
    'VlvNo2DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ioc_OutputSR1msIocVlvNo2Data = CreateBus(Ioc_OutputSR1msIocVlvNo2Data, DeList);
clear DeList;

Ioc_OutputSR1msIocVlvNo3Data = Simulink.Bus;
DeList={
    'VlvNo3DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ioc_OutputSR1msIocVlvNo3Data = CreateBus(Ioc_OutputSR1msIocVlvNo3Data, DeList);
clear DeList;

Ioc_OutputSR1msIocVlvTc0Data = Simulink.Bus;
DeList={
    'VlvTc0DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ioc_OutputSR1msIocVlvTc0Data = CreateBus(Ioc_OutputSR1msIocVlvTc0Data, DeList);
clear DeList;

Ioc_OutputSR1msIocVlvTc1Data = Simulink.Bus;
DeList={
    'VlvTc1DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ioc_OutputSR1msIocVlvTc1Data = CreateBus(Ioc_OutputSR1msIocVlvTc1Data, DeList);
clear DeList;

Ioc_OutputSR1msIocVlvEsv0Data = Simulink.Bus;
DeList={
    'VlvEsv0DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ioc_OutputSR1msIocVlvEsv0Data = CreateBus(Ioc_OutputSR1msIocVlvEsv0Data, DeList);
clear DeList;

Ioc_OutputSR1msIocVlvEsv1Data = Simulink.Bus;
DeList={
    'VlvEsv1DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ioc_OutputSR1msIocVlvEsv1Data = CreateBus(Ioc_OutputSR1msIocVlvEsv1Data, DeList);
clear DeList;

Ioc_OutputSR1msIocVlvNc0Data = Simulink.Bus;
DeList={
    'VlvNc0DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ioc_OutputSR1msIocVlvNc0Data = CreateBus(Ioc_OutputSR1msIocVlvNc0Data, DeList);
clear DeList;

Ioc_OutputSR1msIocVlvNc1Data = Simulink.Bus;
DeList={
    'VlvNc1DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ioc_OutputSR1msIocVlvNc1Data = CreateBus(Ioc_OutputSR1msIocVlvNc1Data, DeList);
clear DeList;

Ioc_OutputSR1msIocVlvNc2Data = Simulink.Bus;
DeList={
    'VlvNc2DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ioc_OutputSR1msIocVlvNc2Data = CreateBus(Ioc_OutputSR1msIocVlvNc2Data, DeList);
clear DeList;

Ioc_OutputSR1msIocVlvNc3Data = Simulink.Bus;
DeList={
    'VlvNc3DataFlg'
    'ACH_TxValve_SET_POINT'
    };
Ioc_OutputSR1msIocVlvNc3Data = CreateBus(Ioc_OutputSR1msIocVlvNc3Data, DeList);
clear DeList;

Ioc_OutputSR1msIocVlvRelayData = Simulink.Bus;
DeList={
    'VlvRelayDataFlg'
    'ACH_Tx1_FS_CMD'
    };
Ioc_OutputSR1msIocVlvRelayData = CreateBus(Ioc_OutputSR1msIocVlvRelayData, DeList);
clear DeList;

Ioc_OutputSR1msIocAdcSelWriteData = Simulink.Bus;
DeList={
    'AdcSelDataFlg'
    'ACH_Tx13_ADC_SEL'
    };
Ioc_OutputSR1msIocAdcSelWriteData = CreateBus(Ioc_OutputSR1msIocAdcSelWriteData, DeList);
clear DeList;

