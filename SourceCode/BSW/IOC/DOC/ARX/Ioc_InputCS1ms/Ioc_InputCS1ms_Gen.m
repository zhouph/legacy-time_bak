Ioc_InputCS1msEcuModeSts = Simulink.Bus;
DeList={'Ioc_InputCS1msEcuModeSts'};
Ioc_InputCS1msEcuModeSts = CreateBus(Ioc_InputCS1msEcuModeSts, DeList);
clear DeList;

Ioc_InputCS1msAcmAsicInitCompleteFlag = Simulink.Bus;
DeList={'Ioc_InputCS1msAcmAsicInitCompleteFlag'};
Ioc_InputCS1msAcmAsicInitCompleteFlag = CreateBus(Ioc_InputCS1msAcmAsicInitCompleteFlag, DeList);
clear DeList;

Ioc_InputCS1msFuncInhibitIocSts = Simulink.Bus;
DeList={'Ioc_InputCS1msFuncInhibitIocSts'};
Ioc_InputCS1msFuncInhibitIocSts = CreateBus(Ioc_InputCS1msFuncInhibitIocSts, DeList);
clear DeList;

Ioc_InputCS1msSwtMonInfo = Simulink.Bus;
DeList={
    'AvhSwtMon'
    'BflSwtMon'
    'BlsSwtMon'
    'BsSwtMon'
    'DoorSwtMon'
    'EscSwtMon'
    'FlexBrkASwtMon'
    'FlexBrkBSwtMon'
    'HzrdSwtMon'
    'HdcSwtMon'
    'PbSwtMon'
    };
Ioc_InputCS1msSwtMonInfo = CreateBus(Ioc_InputCS1msSwtMonInfo, DeList);
clear DeList;

Ioc_InputCS1msSwtMonInfoEsc = Simulink.Bus;
DeList={
    'BlsSwtMon_Esc'
    'AvhSwtMon_Esc'
    'EscSwtMon_Esc'
    'HdcSwtMon_Esc'
    'PbSwtMon_Esc'
    'GearRSwtMon_Esc'
    'BlfSwtMon_Esc'
    'ClutchSwtMon_Esc'
    'ItpmsSwtMon_Esc'
    };
Ioc_InputCS1msSwtMonInfoEsc = CreateBus(Ioc_InputCS1msSwtMonInfoEsc, DeList);
clear DeList;

Ioc_InputCS1msRlyMonInfo = Simulink.Bus;
DeList={
    'RlyDbcMon'
    'RlyEssMon'
    'RlyFault'
    };
Ioc_InputCS1msRlyMonInfo = CreateBus(Ioc_InputCS1msRlyMonInfo, DeList);
clear DeList;

Ioc_InputCS1msSwLineMonInfo = Simulink.Bus;
DeList={
    'Sw1LineCMon'
    'Sw2LineBMon'
    'Sw3LineEMon'
    'Sw4LineFMon'
    };
Ioc_InputCS1msSwLineMonInfo = CreateBus(Ioc_InputCS1msSwLineMonInfo, DeList);
clear DeList;

Ioc_InputCS1msRsmDbcMon = Simulink.Bus;
DeList={'Ioc_InputCS1msRsmDbcMon'};
Ioc_InputCS1msRsmDbcMon = CreateBus(Ioc_InputCS1msRsmDbcMon, DeList);
clear DeList;

Ioc_InputCS1msPbcVdaAi = Simulink.Bus;
DeList={'Ioc_InputCS1msPbcVdaAi'};
Ioc_InputCS1msPbcVdaAi = CreateBus(Ioc_InputCS1msPbcVdaAi, DeList);
clear DeList;

