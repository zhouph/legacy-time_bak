Ioc_InputSR1msAchAdcData = Simulink.Bus;
DeList={
    'ASIC_RX_MSG_13mu10_Rx_13_ADC_DATA_OUT'
    'ASIC_RX_MSG_13mu1_Rx_13_ADC_BUSY'
    };
Ioc_InputSR1msAchAdcData = CreateBus(Ioc_InputSR1msAchAdcData, DeList);
clear DeList;

Ioc_InputSR1msSwTrigPwrInfo = Simulink.Bus;
DeList={
    'Ext5vMon'
    'CEMon'
    'Int5vMon'
    'CSPMon'
    'Vbatt01Mon'
    'Vbatt02Mon'
    'VddMon'
    'Vdd3Mon'
    'Vdd5Mon'
    };
Ioc_InputSR1msSwTrigPwrInfo = CreateBus(Ioc_InputSR1msSwTrigPwrInfo, DeList);
clear DeList;

Ioc_InputSR1msSwTrigFspInfo = Simulink.Bus;
DeList={
    'FspAbsHMon'
    'FspAbsMon'
    'FspCbsHMon'
    'FspCbsMon'
    };
Ioc_InputSR1msSwTrigFspInfo = CreateBus(Ioc_InputSR1msSwTrigFspInfo, DeList);
clear DeList;

Ioc_InputSR1msSwTrigTmpInfo = Simulink.Bus;
DeList={
    'LineTestMon'
    'TempMon'
    };
Ioc_InputSR1msSwTrigTmpInfo = CreateBus(Ioc_InputSR1msSwTrigTmpInfo, DeList);
clear DeList;

Ioc_InputSR1msSwTrigMotInfo = Simulink.Bus;
DeList={
    'MotPwrMon'
    'StarMon'
    'UoutMon'
    'VoutMon'
    'WoutMon'
    };
Ioc_InputSR1msSwTrigMotInfo = CreateBus(Ioc_InputSR1msSwTrigMotInfo, DeList);
clear DeList;

Ioc_InputSR1msSwTrigPdtInfo = Simulink.Bus;
DeList={
    'PdfSigMon'
    'Pdt5vMon'
    'Pdf5vMon'
    'PdtSigMon'
    };
Ioc_InputSR1msSwTrigPdtInfo = CreateBus(Ioc_InputSR1msSwTrigPdtInfo, DeList);
clear DeList;

Ioc_InputSR1msSwTrigMocInfo = Simulink.Bus;
DeList={
    'RlIMainMon'
    'RlISubMonFs'
    'RlMocNegMon'
    'RlMocPosMon'
    'RrIMainMon'
    'RrISubMonFs'
    'RrMocNegMon'
    'RrMocPosMon'
    'BFLMon'
    };
Ioc_InputSR1msSwTrigMocInfo = CreateBus(Ioc_InputSR1msSwTrigMocInfo, DeList);
clear DeList;

Ioc_InputSR1msHwTrigVlvInfo = Simulink.Bus;
DeList={
    'VlvCVMon'
    'VlvRLVMon'
    'VlvCutPMon'
    'VlvCutSMon'
    'VlvSimMon'
    };
Ioc_InputSR1msHwTrigVlvInfo = CreateBus(Ioc_InputSR1msHwTrigVlvInfo, DeList);
clear DeList;

Ioc_InputSR1msSwTrigEscInfo = Simulink.Bus;
DeList={
    'IF_A7_MON'
    'IF_A2_MON'
    'P1_NEG_MON'
    'IF_A6_MON'
    'MTR_FW_S_MON'
    'IF_A4_MON'
    'MTP_MON'
    'IF_A3_MON'
    'P1_POS_MON'
    'IF_A5_MON'
    'VDD_MON'
    'COOL_MON'
    'CAN_L_MON'
    'P3_NEG_MON'
    'IF_A1_MON'
    'P2_POS_MON'
    'IF_A8_MON'
    'IF_A9_MON'
    'P2_NEG_MON'
    'P3_POS_MON'
    'OP_OUT_MON'
    };
Ioc_InputSR1msSwTrigEscInfo = CreateBus(Ioc_InputSR1msSwTrigEscInfo, DeList);
clear DeList;

Ioc_InputSR1msSentHPressureInfo = Simulink.Bus;
DeList={
    'McpPresData1'
    'McpPresData2'
    'McpSentCrc'
    'McpSentData'
    'McpSentMsgId'
    'McpSentSerialCrc'
    'McpSentConfig'
    'McpSentInvalid'
    'MCPSentSerialInvalid'
    'Wlp1PresData1'
    'Wlp1PresData2'
    'Wlp1SentCrc'
    'Wlp1SentData'
    'Wlp1SentMsgId'
    'Wlp1SentSerialCrc'
    'Wlp1SentConfig'
    'Wlp1SentInvalid'
    'Wlp1SentSerialInvalid'
    'Wlp2PresData1'
    'Wlp2PresData2'
    'Wlp2SentCrc'
    'Wlp2SentData'
    'Wlp2SentMsgId'
    'Wlp2SentSerialCrc'
    'Wlp2SentConfig'
    'Wlp2SentInvalid'
    'Wlp2SentSerialInvalid'
    };
Ioc_InputSR1msSentHPressureInfo = CreateBus(Ioc_InputSR1msSentHPressureInfo, DeList);
clear DeList;

Ioc_InputSR1msEcuModeSts = Simulink.Bus;
DeList={'Ioc_InputSR1msEcuModeSts'};
Ioc_InputSR1msEcuModeSts = CreateBus(Ioc_InputSR1msEcuModeSts, DeList);
clear DeList;

Ioc_InputSR1msAcmAsicInitCompleteFlag = Simulink.Bus;
DeList={'Ioc_InputSR1msAcmAsicInitCompleteFlag'};
Ioc_InputSR1msAcmAsicInitCompleteFlag = CreateBus(Ioc_InputSR1msAcmAsicInitCompleteFlag, DeList);
clear DeList;

Ioc_InputSR1msAdcInvalid = Simulink.Bus;
DeList={'Ioc_InputSR1msAdcInvalid'};
Ioc_InputSR1msAdcInvalid = CreateBus(Ioc_InputSR1msAdcInvalid, DeList);
clear DeList;

Ioc_InputSR1msFuncInhibitIocSts = Simulink.Bus;
DeList={'Ioc_InputSR1msFuncInhibitIocSts'};
Ioc_InputSR1msFuncInhibitIocSts = CreateBus(Ioc_InputSR1msFuncInhibitIocSts, DeList);
clear DeList;

Ioc_InputSR1msPwrMonInfoEsc = Simulink.Bus;
DeList={
    'VoltVBatt01Mon_Esc'
    'VoltVBatt02Mon_Esc'
    'VoltGio2Mon_Esc'
    'VoltExt5VSupplyMon_Esc'
    'VoltExt12VSupplyMon_Esc'
    'VoltSwVpwrMon_Esc'
    'VoltVpwrMon_Esc'
    'VoltGio1Mon_Esc'
    'VoltIgnMon_Esc'
    };
Ioc_InputSR1msPwrMonInfoEsc = CreateBus(Ioc_InputSR1msPwrMonInfoEsc, DeList);
clear DeList;

Ioc_InputSR1msVlvMonInfoEsc = Simulink.Bus;
DeList={
    'VoltFspMon_Esc'
    };
Ioc_InputSR1msVlvMonInfoEsc = CreateBus(Ioc_InputSR1msVlvMonInfoEsc, DeList);
clear DeList;

Ioc_InputSR1msMotMonInfoEsc = Simulink.Bus;
DeList={
    'VoltMtpMon_Esc'
    'VoltMtrFwSMon_Esc'
    };
Ioc_InputSR1msMotMonInfoEsc = CreateBus(Ioc_InputSR1msMotMonInfoEsc, DeList);
clear DeList;

Ioc_InputSR1msEpbMonInfoEsc = Simulink.Bus;
DeList={
    'VoltEpbPwrMon_Esc'
    'VoltEpbI1LMon_Esc'
    'VoltEpbI1RMon_Esc'
    'VoltEpbI2LMon_Esc'
    'VoltEpbI2RMon_Esc'
    'VoltEpbMonLPlus_Esc'
    'VoltEpbMonLMinus_Esc'
    'VoltEpbMonRPlus_Esc'
    'VoltEpbMonRMinus_Esc'
    };
Ioc_InputSR1msEpbMonInfoEsc = CreateBus(Ioc_InputSR1msEpbMonInfoEsc, DeList);
clear DeList;

Ioc_InputSR1msReservedMonInfoEsc = Simulink.Bus;
DeList={
    'VoltMcuAdc1_Esc'
    'VoltMcuAdc2_Esc'
    'VoltMcuAdc3_Esc'
    'VoltMcuAdc4_Esc'
    'VoltMcuAdc5_Esc'
    };
Ioc_InputSR1msReservedMonInfoEsc = CreateBus(Ioc_InputSR1msReservedMonInfoEsc, DeList);
clear DeList;

Ioc_InputSR1msHalPressureInfo = Simulink.Bus;
DeList={
    'McpPresData1'
    'McpPresData2'
    'McpSentCrc'
    'McpSentData'
    'McpSentMsgId'
    'McpSentSerialCrc'
    'McpSentConfig'
    'Wlp1PresData1'
    'Wlp1PresData2'
    'Wlp1SentCrc'
    'Wlp1SentData'
    'Wlp1SentMsgId'
    'Wlp1SentSerialCrc'
    'Wlp1SentConfig'
    'Wlp2PresData1'
    'Wlp2PresData2'
    'Wlp2SentCrc'
    'Wlp2SentData'
    'Wlp2SentMsgId'
    'Wlp2SentSerialCrc'
    'Wlp2SentConfig'
    };
Ioc_InputSR1msHalPressureInfo = CreateBus(Ioc_InputSR1msHalPressureInfo, DeList);
clear DeList;

Ioc_InputSR1msMotMonInfo = Simulink.Bus;
DeList={
    'MotPwrVoltMon'
    'MotStarMon'
    };
Ioc_InputSR1msMotMonInfo = CreateBus(Ioc_InputSR1msMotMonInfo, DeList);
clear DeList;

Ioc_InputSR1msMotVoltsMonInfo = Simulink.Bus;
DeList={
    'MotVoltPhUMon'
    'MotVoltPhVMon'
    'MotVoltPhWMon'
    };
Ioc_InputSR1msMotVoltsMonInfo = CreateBus(Ioc_InputSR1msMotVoltsMonInfo, DeList);
clear DeList;

Ioc_InputSR1msPedlSigMonInfo = Simulink.Bus;
DeList={
    'PdfSigMon'
    'PdtSigMon'
    };
Ioc_InputSR1msPedlSigMonInfo = CreateBus(Ioc_InputSR1msPedlSigMonInfo, DeList);
clear DeList;

Ioc_InputSR1msPedlPwrMonInfo = Simulink.Bus;
DeList={
    'Pdt5vMon'
    'Pdf5vMon'
    };
Ioc_InputSR1msPedlPwrMonInfo = CreateBus(Ioc_InputSR1msPedlPwrMonInfo, DeList);
clear DeList;

Ioc_InputSR1msMocMonInfo = Simulink.Bus;
DeList={
    'RlCurrMainMon'
    'RlCurrSubMon'
    'RlMocNegMon'
    'RlMocPosMon'
    'RrCurrMainMon'
    'RrCurrSubMon'
    'RrMocNegMon'
    'RrMocPosMon'
    };
Ioc_InputSR1msMocMonInfo = CreateBus(Ioc_InputSR1msMocMonInfo, DeList);
clear DeList;

Ioc_InputSR1msVlvMonInfo = Simulink.Bus;
DeList={
    'VlvCVMon'
    'VlvRLVMon'
    'PrimCutVlvMon'
    'SecdCutVlvMon'
    'SimVlvMon'
    };
Ioc_InputSR1msVlvMonInfo = CreateBus(Ioc_InputSR1msVlvMonInfo, DeList);
clear DeList;

Ioc_InputSR1msWhlVlvFbMonInfo = Simulink.Bus;
DeList={
    'FlOvCurrMon'
    'FlIvDutyMon'
    'FrOvCurrMon'
    'FrIvDutyMon'
    'RlOvCurrMon'
    'RlIvDutyMon'
    'RrOvCurrMon'
    'RrIvDutyMon'
    };
Ioc_InputSR1msWhlVlvFbMonInfo = CreateBus(Ioc_InputSR1msWhlVlvFbMonInfo, DeList);
clear DeList;

Ioc_InputSR1msVBatt1Mon = Simulink.Bus;
DeList={'Ioc_InputSR1msVBatt1Mon'};
Ioc_InputSR1msVBatt1Mon = CreateBus(Ioc_InputSR1msVBatt1Mon, DeList);
clear DeList;

Ioc_InputSR1msVBatt2Mon = Simulink.Bus;
DeList={'Ioc_InputSR1msVBatt2Mon'};
Ioc_InputSR1msVBatt2Mon = CreateBus(Ioc_InputSR1msVBatt2Mon, DeList);
clear DeList;

Ioc_InputSR1msFspCbsHMon = Simulink.Bus;
DeList={'Ioc_InputSR1msFspCbsHMon'};
Ioc_InputSR1msFspCbsHMon = CreateBus(Ioc_InputSR1msFspCbsHMon, DeList);
clear DeList;

Ioc_InputSR1msFspCbsMon = Simulink.Bus;
DeList={'Ioc_InputSR1msFspCbsMon'};
Ioc_InputSR1msFspCbsMon = CreateBus(Ioc_InputSR1msFspCbsMon, DeList);
clear DeList;

Ioc_InputSR1msFspAbsHMon = Simulink.Bus;
DeList={'Ioc_InputSR1msFspAbsHMon'};
Ioc_InputSR1msFspAbsHMon = CreateBus(Ioc_InputSR1msFspAbsHMon, DeList);
clear DeList;

Ioc_InputSR1msFspAbsMon = Simulink.Bus;
DeList={'Ioc_InputSR1msFspAbsMon'};
Ioc_InputSR1msFspAbsMon = CreateBus(Ioc_InputSR1msFspAbsMon, DeList);
clear DeList;

Ioc_InputSR1msExt5vMon = Simulink.Bus;
DeList={'Ioc_InputSR1msExt5vMon'};
Ioc_InputSR1msExt5vMon = CreateBus(Ioc_InputSR1msExt5vMon, DeList);
clear DeList;

Ioc_InputSR1msCEMon = Simulink.Bus;
DeList={'Ioc_InputSR1msCEMon'};
Ioc_InputSR1msCEMon = CreateBus(Ioc_InputSR1msCEMon, DeList);
clear DeList;

Ioc_InputSR1msLineTestMon = Simulink.Bus;
DeList={'Ioc_InputSR1msLineTestMon'};
Ioc_InputSR1msLineTestMon = CreateBus(Ioc_InputSR1msLineTestMon, DeList);
clear DeList;

Ioc_InputSR1msTempMon = Simulink.Bus;
DeList={'Ioc_InputSR1msTempMon'};
Ioc_InputSR1msTempMon = CreateBus(Ioc_InputSR1msTempMon, DeList);
clear DeList;

Ioc_InputSR1msVdd1Mon = Simulink.Bus;
DeList={'Ioc_InputSR1msVdd1Mon'};
Ioc_InputSR1msVdd1Mon = CreateBus(Ioc_InputSR1msVdd1Mon, DeList);
clear DeList;

Ioc_InputSR1msVdd2Mon = Simulink.Bus;
DeList={'Ioc_InputSR1msVdd2Mon'};
Ioc_InputSR1msVdd2Mon = CreateBus(Ioc_InputSR1msVdd2Mon, DeList);
clear DeList;

Ioc_InputSR1msVdd3Mon = Simulink.Bus;
DeList={'Ioc_InputSR1msVdd3Mon'};
Ioc_InputSR1msVdd3Mon = CreateBus(Ioc_InputSR1msVdd3Mon, DeList);
clear DeList;

Ioc_InputSR1msVdd4Mon = Simulink.Bus;
DeList={'Ioc_InputSR1msVdd4Mon'};
Ioc_InputSR1msVdd4Mon = CreateBus(Ioc_InputSR1msVdd4Mon, DeList);
clear DeList;

Ioc_InputSR1msVdd5Mon = Simulink.Bus;
DeList={'Ioc_InputSR1msVdd5Mon'};
Ioc_InputSR1msVdd5Mon = CreateBus(Ioc_InputSR1msVdd5Mon, DeList);
clear DeList;

Ioc_InputSR1msVddMon = Simulink.Bus;
DeList={'Ioc_InputSR1msVddMon'};
Ioc_InputSR1msVddMon = CreateBus(Ioc_InputSR1msVddMon, DeList);
clear DeList;

Ioc_InputSR1msCspMon = Simulink.Bus;
DeList={'Ioc_InputSR1msCspMon'};
Ioc_InputSR1msCspMon = CreateBus(Ioc_InputSR1msCspMon, DeList);
clear DeList;

Ioc_InputSR1msInt5vMon = Simulink.Bus;
DeList={'Ioc_InputSR1msInt5vMon'};
Ioc_InputSR1msInt5vMon = CreateBus(Ioc_InputSR1msInt5vMon, DeList);
clear DeList;

Ioc_InputSR1msBFLMon = Simulink.Bus;
DeList={'Ioc_InputSR1msBFLMon'};
Ioc_InputSR1msBFLMon = CreateBus(Ioc_InputSR1msBFLMon, DeList);
clear DeList;

