Ioc_OutputCS50usMotPwmDataInfo = Simulink.Bus;
DeList={
    'MotPwmPhUhighData'
    'MotPwmPhVhighData'
    'MotPwmPhWhighData'
    'MotPwmPhUlowData'
    'MotPwmPhVlowData'
    'MotPwmPhWlowData'
    };
Ioc_OutputCS50usMotPwmDataInfo = CreateBus(Ioc_OutputCS50usMotPwmDataInfo, DeList);
clear DeList;

Ioc_OutputCS50usEcuModeSts = Simulink.Bus;
DeList={'Ioc_OutputCS50usEcuModeSts'};
Ioc_OutputCS50usEcuModeSts = CreateBus(Ioc_OutputCS50usEcuModeSts, DeList);
clear DeList;

Ioc_OutputCS50usFuncInhibitIocSts = Simulink.Bus;
DeList={'Ioc_OutputCS50usFuncInhibitIocSts'};
Ioc_OutputCS50usFuncInhibitIocSts = CreateBus(Ioc_OutputCS50usFuncInhibitIocSts, DeList);
clear DeList;

