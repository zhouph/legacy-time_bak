#include "unity.h"
#include "unity_fixture.h"
#include "Ioc_OutputCS5ms.h"
#include "Ioc_OutputCS5ms_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Mom_HndlrEcuModeSts_t UtInput_Ioc_OutputCS5msEcuModeSts[MAX_STEP] = IOC_OUTPUTCS5MSECUMODESTS;
const Acm_MainAcmAsicInitCompleteFlag_t UtInput_Ioc_OutputCS5msAcmAsicInitCompleteFlag[MAX_STEP] = IOC_OUTPUTCS5MSACMASICINITCOMPLETEFLAG;
const Eem_SuspcDetnFuncInhibitIocSts_t UtInput_Ioc_OutputCS5msFuncInhibitIocSts[MAX_STEP] = IOC_OUTPUTCS5MSFUNCINHIBITIOCSTS;




TEST_GROUP(Ioc_OutputCS5ms);
TEST_SETUP(Ioc_OutputCS5ms)
{
    Ioc_OutputCS5ms_Init();
}

TEST_TEAR_DOWN(Ioc_OutputCS5ms)
{   /* Postcondition */

}

TEST(Ioc_OutputCS5ms, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Ioc_OutputCS5msEcuModeSts = UtInput_Ioc_OutputCS5msEcuModeSts[i];
        Ioc_OutputCS5msAcmAsicInitCompleteFlag = UtInput_Ioc_OutputCS5msAcmAsicInitCompleteFlag[i];
        Ioc_OutputCS5msFuncInhibitIocSts = UtInput_Ioc_OutputCS5msFuncInhibitIocSts[i];

        Ioc_OutputCS5ms();

    }
}

TEST_GROUP_RUNNER(Ioc_OutputCS5ms)
{
    RUN_TEST_CASE(Ioc_OutputCS5ms, All);
}
