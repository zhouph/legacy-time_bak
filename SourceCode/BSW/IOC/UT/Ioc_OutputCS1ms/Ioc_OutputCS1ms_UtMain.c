#include "unity.h"
#include "unity_fixture.h"
#include "Ioc_OutputCS1ms.h"
#include "Ioc_OutputCS1ms_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Saluint16 UtInput_Ioc_OutputCS1msNormVlvDrvInfo_PrimCutVlvDrvData[MAX_STEP] = IOC_OUTPUTCS1MSNORMVLVDRVINFO_PRIMCUTVLVDRVDATA;
const Saluint16 UtInput_Ioc_OutputCS1msNormVlvDrvInfo_RelsVlvDrvData[MAX_STEP] = IOC_OUTPUTCS1MSNORMVLVDRVINFO_RELSVLVDRVDATA;
const Saluint16 UtInput_Ioc_OutputCS1msNormVlvDrvInfo_SecdCutVlvDrvData[MAX_STEP] = IOC_OUTPUTCS1MSNORMVLVDRVINFO_SECDCUTVLVDRVDATA;
const Saluint16 UtInput_Ioc_OutputCS1msNormVlvDrvInfo_CircVlvDrvData[MAX_STEP] = IOC_OUTPUTCS1MSNORMVLVDRVINFO_CIRCVLVDRVDATA;
const Saluint16 UtInput_Ioc_OutputCS1msNormVlvDrvInfo_SimVlvDrvData[MAX_STEP] = IOC_OUTPUTCS1MSNORMVLVDRVINFO_SIMVLVDRVDATA;
const Saluint16 UtInput_Ioc_OutputCS1msWhlVlvDrvInfo_FlOvDrvData[MAX_STEP] = IOC_OUTPUTCS1MSWHLVLVDRVINFO_FLOVDRVDATA;
const Saluint16 UtInput_Ioc_OutputCS1msWhlVlvDrvInfo_FlIvDrvData[MAX_STEP] = IOC_OUTPUTCS1MSWHLVLVDRVINFO_FLIVDRVDATA;
const Saluint16 UtInput_Ioc_OutputCS1msWhlVlvDrvInfo_FrOvDrvData[MAX_STEP] = IOC_OUTPUTCS1MSWHLVLVDRVINFO_FROVDRVDATA;
const Saluint16 UtInput_Ioc_OutputCS1msWhlVlvDrvInfo_FrIvDrvData[MAX_STEP] = IOC_OUTPUTCS1MSWHLVLVDRVINFO_FRIVDRVDATA;
const Saluint16 UtInput_Ioc_OutputCS1msWhlVlvDrvInfo_RlOvDrvData[MAX_STEP] = IOC_OUTPUTCS1MSWHLVLVDRVINFO_RLOVDRVDATA;
const Saluint16 UtInput_Ioc_OutputCS1msWhlVlvDrvInfo_RlIvDrvData[MAX_STEP] = IOC_OUTPUTCS1MSWHLVLVDRVINFO_RLIVDRVDATA;
const Saluint16 UtInput_Ioc_OutputCS1msWhlVlvDrvInfo_RrOvDrvData[MAX_STEP] = IOC_OUTPUTCS1MSWHLVLVDRVINFO_RROVDRVDATA;
const Saluint16 UtInput_Ioc_OutputCS1msWhlVlvDrvInfo_RrIvDrvData[MAX_STEP] = IOC_OUTPUTCS1MSWHLVLVDRVINFO_RRIVDRVDATA;
const Saluint8 UtInput_Ioc_OutputCS1msRlyDrvInfo_RlyDbcDrv[MAX_STEP] = IOC_OUTPUTCS1MSRLYDRVINFO_RLYDBCDRV;
const Saluint8 UtInput_Ioc_OutputCS1msRlyDrvInfo_RlyEssDrv[MAX_STEP] = IOC_OUTPUTCS1MSRLYDRVINFO_RLYESSDRV;
const Saluint8 UtInput_Ioc_OutputCS1msFsrAbsDrvInfo_FsrAbsOff[MAX_STEP] = IOC_OUTPUTCS1MSFSRABSDRVINFO_FSRABSOFF;
const Saluint8 UtInput_Ioc_OutputCS1msFsrCbsDrvInfo_FsrCbsOff[MAX_STEP] = IOC_OUTPUTCS1MSFSRCBSDRVINFO_FSRCBSOFF;
const Saluint8 UtInput_Ioc_OutputCS1msSenPwrMonitorData_SenPwrM_12V_Drive_Req[MAX_STEP] = IOC_OUTPUTCS1MSSENPWRMONITORDATA_SENPWRM_12V_DRIVE_REQ;
const Mom_HndlrEcuModeSts_t UtInput_Ioc_OutputCS1msEcuModeSts[MAX_STEP] = IOC_OUTPUTCS1MSECUMODESTS;
const Acm_MainAcmAsicInitCompleteFlag_t UtInput_Ioc_OutputCS1msAcmAsicInitCompleteFlag[MAX_STEP] = IOC_OUTPUTCS1MSACMASICINITCOMPLETEFLAG;
const Eem_SuspcDetnFuncInhibitIocSts_t UtInput_Ioc_OutputCS1msFuncInhibitIocSts[MAX_STEP] = IOC_OUTPUTCS1MSFUNCINHIBITIOCSTS;
const CanTrv_TLE6251_HndlrMainCanEn_t UtInput_Ioc_OutputCS1msMainCanEn[MAX_STEP] = IOC_OUTPUTCS1MSMAINCANEN;
const Vlvd_A3944_HndlrVlvDrvEnRst_t UtInput_Ioc_OutputCS1msVlvDrvEnRst[MAX_STEP] = IOC_OUTPUTCS1MSVLVDRVENRST;
const Awd_mainAwdPrnDrv_t UtInput_Ioc_OutputCS1msAwdPrnDrv[MAX_STEP] = IOC_OUTPUTCS1MSAWDPRNDRV;
const Fsr_ActrFsrDcMtrShutDwn_t UtInput_Ioc_OutputCS1msFsrDcMtrShutDwn[MAX_STEP] = IOC_OUTPUTCS1MSFSRDCMTRSHUTDWN;
const Fsr_ActrFsrEnDrDrv_t UtInput_Ioc_OutputCS1msFsrEnDrDrv[MAX_STEP] = IOC_OUTPUTCS1MSFSRENDRDRV;




TEST_GROUP(Ioc_OutputCS1ms);
TEST_SETUP(Ioc_OutputCS1ms)
{
    Ioc_OutputCS1ms_Init();
}

TEST_TEAR_DOWN(Ioc_OutputCS1ms)
{   /* Postcondition */

}

TEST(Ioc_OutputCS1ms, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Ioc_OutputCS1msNormVlvDrvInfo.PrimCutVlvDrvData = UtInput_Ioc_OutputCS1msNormVlvDrvInfo_PrimCutVlvDrvData[i];
        Ioc_OutputCS1msNormVlvDrvInfo.RelsVlvDrvData = UtInput_Ioc_OutputCS1msNormVlvDrvInfo_RelsVlvDrvData[i];
        Ioc_OutputCS1msNormVlvDrvInfo.SecdCutVlvDrvData = UtInput_Ioc_OutputCS1msNormVlvDrvInfo_SecdCutVlvDrvData[i];
        Ioc_OutputCS1msNormVlvDrvInfo.CircVlvDrvData = UtInput_Ioc_OutputCS1msNormVlvDrvInfo_CircVlvDrvData[i];
        Ioc_OutputCS1msNormVlvDrvInfo.SimVlvDrvData = UtInput_Ioc_OutputCS1msNormVlvDrvInfo_SimVlvDrvData[i];
        Ioc_OutputCS1msWhlVlvDrvInfo.FlOvDrvData = UtInput_Ioc_OutputCS1msWhlVlvDrvInfo_FlOvDrvData[i];
        Ioc_OutputCS1msWhlVlvDrvInfo.FlIvDrvData = UtInput_Ioc_OutputCS1msWhlVlvDrvInfo_FlIvDrvData[i];
        Ioc_OutputCS1msWhlVlvDrvInfo.FrOvDrvData = UtInput_Ioc_OutputCS1msWhlVlvDrvInfo_FrOvDrvData[i];
        Ioc_OutputCS1msWhlVlvDrvInfo.FrIvDrvData = UtInput_Ioc_OutputCS1msWhlVlvDrvInfo_FrIvDrvData[i];
        Ioc_OutputCS1msWhlVlvDrvInfo.RlOvDrvData = UtInput_Ioc_OutputCS1msWhlVlvDrvInfo_RlOvDrvData[i];
        Ioc_OutputCS1msWhlVlvDrvInfo.RlIvDrvData = UtInput_Ioc_OutputCS1msWhlVlvDrvInfo_RlIvDrvData[i];
        Ioc_OutputCS1msWhlVlvDrvInfo.RrOvDrvData = UtInput_Ioc_OutputCS1msWhlVlvDrvInfo_RrOvDrvData[i];
        Ioc_OutputCS1msWhlVlvDrvInfo.RrIvDrvData = UtInput_Ioc_OutputCS1msWhlVlvDrvInfo_RrIvDrvData[i];
        Ioc_OutputCS1msRlyDrvInfo.RlyDbcDrv = UtInput_Ioc_OutputCS1msRlyDrvInfo_RlyDbcDrv[i];
        Ioc_OutputCS1msRlyDrvInfo.RlyEssDrv = UtInput_Ioc_OutputCS1msRlyDrvInfo_RlyEssDrv[i];
        Ioc_OutputCS1msFsrAbsDrvInfo.FsrAbsOff = UtInput_Ioc_OutputCS1msFsrAbsDrvInfo_FsrAbsOff[i];
        Ioc_OutputCS1msFsrCbsDrvInfo.FsrCbsOff = UtInput_Ioc_OutputCS1msFsrCbsDrvInfo_FsrCbsOff[i];
        Ioc_OutputCS1msSenPwrMonitorData.SenPwrM_12V_Drive_Req = UtInput_Ioc_OutputCS1msSenPwrMonitorData_SenPwrM_12V_Drive_Req[i];
        Ioc_OutputCS1msEcuModeSts = UtInput_Ioc_OutputCS1msEcuModeSts[i];
        Ioc_OutputCS1msAcmAsicInitCompleteFlag = UtInput_Ioc_OutputCS1msAcmAsicInitCompleteFlag[i];
        Ioc_OutputCS1msFuncInhibitIocSts = UtInput_Ioc_OutputCS1msFuncInhibitIocSts[i];
        Ioc_OutputCS1msMainCanEn = UtInput_Ioc_OutputCS1msMainCanEn[i];
        Ioc_OutputCS1msVlvDrvEnRst = UtInput_Ioc_OutputCS1msVlvDrvEnRst[i];
        Ioc_OutputCS1msAwdPrnDrv = UtInput_Ioc_OutputCS1msAwdPrnDrv[i];
        Ioc_OutputCS1msFsrDcMtrShutDwn = UtInput_Ioc_OutputCS1msFsrDcMtrShutDwn[i];
        Ioc_OutputCS1msFsrEnDrDrv = UtInput_Ioc_OutputCS1msFsrEnDrDrv[i];

        Ioc_OutputCS1ms();

    }
}

TEST_GROUP_RUNNER(Ioc_OutputCS1ms)
{
    RUN_TEST_CASE(Ioc_OutputCS1ms, All);
}
