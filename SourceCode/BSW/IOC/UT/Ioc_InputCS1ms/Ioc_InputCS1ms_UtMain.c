#include "unity.h"
#include "unity_fixture.h"
#include "Ioc_InputCS1ms.h"
#include "Ioc_InputCS1ms_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Mom_HndlrEcuModeSts_t UtInput_Ioc_InputCS1msEcuModeSts[MAX_STEP] = IOC_INPUTCS1MSECUMODESTS;
const Acm_MainAcmAsicInitCompleteFlag_t UtInput_Ioc_InputCS1msAcmAsicInitCompleteFlag[MAX_STEP] = IOC_INPUTCS1MSACMASICINITCOMPLETEFLAG;
const Eem_SuspcDetnFuncInhibitIocSts_t UtInput_Ioc_InputCS1msFuncInhibitIocSts[MAX_STEP] = IOC_INPUTCS1MSFUNCINHIBITIOCSTS;

const Haluint8 UtExpected_Ioc_InputCS1msSwtMonInfo_AvhSwtMon[MAX_STEP] = IOC_INPUTCS1MSSWTMONINFO_AVHSWTMON;
const Haluint8 UtExpected_Ioc_InputCS1msSwtMonInfo_BflSwtMon[MAX_STEP] = IOC_INPUTCS1MSSWTMONINFO_BFLSWTMON;
const Haluint8 UtExpected_Ioc_InputCS1msSwtMonInfo_BlsSwtMon[MAX_STEP] = IOC_INPUTCS1MSSWTMONINFO_BLSSWTMON;
const Haluint8 UtExpected_Ioc_InputCS1msSwtMonInfo_BsSwtMon[MAX_STEP] = IOC_INPUTCS1MSSWTMONINFO_BSSWTMON;
const Haluint8 UtExpected_Ioc_InputCS1msSwtMonInfo_DoorSwtMon[MAX_STEP] = IOC_INPUTCS1MSSWTMONINFO_DOORSWTMON;
const Haluint8 UtExpected_Ioc_InputCS1msSwtMonInfo_EscSwtMon[MAX_STEP] = IOC_INPUTCS1MSSWTMONINFO_ESCSWTMON;
const Haluint8 UtExpected_Ioc_InputCS1msSwtMonInfo_FlexBrkASwtMon[MAX_STEP] = IOC_INPUTCS1MSSWTMONINFO_FLEXBRKASWTMON;
const Haluint8 UtExpected_Ioc_InputCS1msSwtMonInfo_FlexBrkBSwtMon[MAX_STEP] = IOC_INPUTCS1MSSWTMONINFO_FLEXBRKBSWTMON;
const Haluint8 UtExpected_Ioc_InputCS1msSwtMonInfo_HzrdSwtMon[MAX_STEP] = IOC_INPUTCS1MSSWTMONINFO_HZRDSWTMON;
const Haluint8 UtExpected_Ioc_InputCS1msSwtMonInfo_HdcSwtMon[MAX_STEP] = IOC_INPUTCS1MSSWTMONINFO_HDCSWTMON;
const Haluint8 UtExpected_Ioc_InputCS1msSwtMonInfo_PbSwtMon[MAX_STEP] = IOC_INPUTCS1MSSWTMONINFO_PBSWTMON;
const Haluint8 UtExpected_Ioc_InputCS1msSwtMonInfoEsc_BlsSwtMon_Esc[MAX_STEP] = IOC_INPUTCS1MSSWTMONINFOESC_BLSSWTMON_ESC;
const Haluint8 UtExpected_Ioc_InputCS1msSwtMonInfoEsc_AvhSwtMon_Esc[MAX_STEP] = IOC_INPUTCS1MSSWTMONINFOESC_AVHSWTMON_ESC;
const Haluint8 UtExpected_Ioc_InputCS1msSwtMonInfoEsc_EscSwtMon_Esc[MAX_STEP] = IOC_INPUTCS1MSSWTMONINFOESC_ESCSWTMON_ESC;
const Haluint8 UtExpected_Ioc_InputCS1msSwtMonInfoEsc_HdcSwtMon_Esc[MAX_STEP] = IOC_INPUTCS1MSSWTMONINFOESC_HDCSWTMON_ESC;
const Haluint8 UtExpected_Ioc_InputCS1msSwtMonInfoEsc_PbSwtMon_Esc[MAX_STEP] = IOC_INPUTCS1MSSWTMONINFOESC_PBSWTMON_ESC;
const Haluint8 UtExpected_Ioc_InputCS1msSwtMonInfoEsc_GearRSwtMon_Esc[MAX_STEP] = IOC_INPUTCS1MSSWTMONINFOESC_GEARRSWTMON_ESC;
const Haluint8 UtExpected_Ioc_InputCS1msSwtMonInfoEsc_BlfSwtMon_Esc[MAX_STEP] = IOC_INPUTCS1MSSWTMONINFOESC_BLFSWTMON_ESC;
const Haluint8 UtExpected_Ioc_InputCS1msSwtMonInfoEsc_ClutchSwtMon_Esc[MAX_STEP] = IOC_INPUTCS1MSSWTMONINFOESC_CLUTCHSWTMON_ESC;
const Haluint8 UtExpected_Ioc_InputCS1msSwtMonInfoEsc_ItpmsSwtMon_Esc[MAX_STEP] = IOC_INPUTCS1MSSWTMONINFOESC_ITPMSSWTMON_ESC;
const Haluint8 UtExpected_Ioc_InputCS1msRlyMonInfo_RlyDbcMon[MAX_STEP] = IOC_INPUTCS1MSRLYMONINFO_RLYDBCMON;
const Haluint8 UtExpected_Ioc_InputCS1msRlyMonInfo_RlyEssMon[MAX_STEP] = IOC_INPUTCS1MSRLYMONINFO_RLYESSMON;
const Haluint8 UtExpected_Ioc_InputCS1msRlyMonInfo_RlyFault[MAX_STEP] = IOC_INPUTCS1MSRLYMONINFO_RLYFAULT;
const Haluint8 UtExpected_Ioc_InputCS1msSwLineMonInfo_Sw1LineCMon[MAX_STEP] = IOC_INPUTCS1MSSWLINEMONINFO_SW1LINECMON;
const Haluint8 UtExpected_Ioc_InputCS1msSwLineMonInfo_Sw2LineBMon[MAX_STEP] = IOC_INPUTCS1MSSWLINEMONINFO_SW2LINEBMON;
const Haluint8 UtExpected_Ioc_InputCS1msSwLineMonInfo_Sw3LineEMon[MAX_STEP] = IOC_INPUTCS1MSSWLINEMONINFO_SW3LINEEMON;
const Haluint8 UtExpected_Ioc_InputCS1msSwLineMonInfo_Sw4LineFMon[MAX_STEP] = IOC_INPUTCS1MSSWLINEMONINFO_SW4LINEFMON;
const Ioc_InputCS1msRsmDbcMon_t UtExpected_Ioc_InputCS1msRsmDbcMon[MAX_STEP] = IOC_INPUTCS1MSRSMDBCMON;
const Ioc_InputCS1msPbcVdaAi_t UtExpected_Ioc_InputCS1msPbcVdaAi[MAX_STEP] = IOC_INPUTCS1MSPBCVDAAI;



TEST_GROUP(Ioc_InputCS1ms);
TEST_SETUP(Ioc_InputCS1ms)
{
    Ioc_InputCS1ms_Init();
}

TEST_TEAR_DOWN(Ioc_InputCS1ms)
{   /* Postcondition */

}

TEST(Ioc_InputCS1ms, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Ioc_InputCS1msEcuModeSts = UtInput_Ioc_InputCS1msEcuModeSts[i];
        Ioc_InputCS1msAcmAsicInitCompleteFlag = UtInput_Ioc_InputCS1msAcmAsicInitCompleteFlag[i];
        Ioc_InputCS1msFuncInhibitIocSts = UtInput_Ioc_InputCS1msFuncInhibitIocSts[i];

        Ioc_InputCS1ms();

        TEST_ASSERT_EQUAL(Ioc_InputCS1msSwtMonInfo.AvhSwtMon, UtExpected_Ioc_InputCS1msSwtMonInfo_AvhSwtMon[i]);
        TEST_ASSERT_EQUAL(Ioc_InputCS1msSwtMonInfo.BflSwtMon, UtExpected_Ioc_InputCS1msSwtMonInfo_BflSwtMon[i]);
        TEST_ASSERT_EQUAL(Ioc_InputCS1msSwtMonInfo.BlsSwtMon, UtExpected_Ioc_InputCS1msSwtMonInfo_BlsSwtMon[i]);
        TEST_ASSERT_EQUAL(Ioc_InputCS1msSwtMonInfo.BsSwtMon, UtExpected_Ioc_InputCS1msSwtMonInfo_BsSwtMon[i]);
        TEST_ASSERT_EQUAL(Ioc_InputCS1msSwtMonInfo.DoorSwtMon, UtExpected_Ioc_InputCS1msSwtMonInfo_DoorSwtMon[i]);
        TEST_ASSERT_EQUAL(Ioc_InputCS1msSwtMonInfo.EscSwtMon, UtExpected_Ioc_InputCS1msSwtMonInfo_EscSwtMon[i]);
        TEST_ASSERT_EQUAL(Ioc_InputCS1msSwtMonInfo.FlexBrkASwtMon, UtExpected_Ioc_InputCS1msSwtMonInfo_FlexBrkASwtMon[i]);
        TEST_ASSERT_EQUAL(Ioc_InputCS1msSwtMonInfo.FlexBrkBSwtMon, UtExpected_Ioc_InputCS1msSwtMonInfo_FlexBrkBSwtMon[i]);
        TEST_ASSERT_EQUAL(Ioc_InputCS1msSwtMonInfo.HzrdSwtMon, UtExpected_Ioc_InputCS1msSwtMonInfo_HzrdSwtMon[i]);
        TEST_ASSERT_EQUAL(Ioc_InputCS1msSwtMonInfo.HdcSwtMon, UtExpected_Ioc_InputCS1msSwtMonInfo_HdcSwtMon[i]);
        TEST_ASSERT_EQUAL(Ioc_InputCS1msSwtMonInfo.PbSwtMon, UtExpected_Ioc_InputCS1msSwtMonInfo_PbSwtMon[i]);
        TEST_ASSERT_EQUAL(Ioc_InputCS1msSwtMonInfoEsc.BlsSwtMon_Esc, UtExpected_Ioc_InputCS1msSwtMonInfoEsc_BlsSwtMon_Esc[i]);
        TEST_ASSERT_EQUAL(Ioc_InputCS1msSwtMonInfoEsc.AvhSwtMon_Esc, UtExpected_Ioc_InputCS1msSwtMonInfoEsc_AvhSwtMon_Esc[i]);
        TEST_ASSERT_EQUAL(Ioc_InputCS1msSwtMonInfoEsc.EscSwtMon_Esc, UtExpected_Ioc_InputCS1msSwtMonInfoEsc_EscSwtMon_Esc[i]);
        TEST_ASSERT_EQUAL(Ioc_InputCS1msSwtMonInfoEsc.HdcSwtMon_Esc, UtExpected_Ioc_InputCS1msSwtMonInfoEsc_HdcSwtMon_Esc[i]);
        TEST_ASSERT_EQUAL(Ioc_InputCS1msSwtMonInfoEsc.PbSwtMon_Esc, UtExpected_Ioc_InputCS1msSwtMonInfoEsc_PbSwtMon_Esc[i]);
        TEST_ASSERT_EQUAL(Ioc_InputCS1msSwtMonInfoEsc.GearRSwtMon_Esc, UtExpected_Ioc_InputCS1msSwtMonInfoEsc_GearRSwtMon_Esc[i]);
        TEST_ASSERT_EQUAL(Ioc_InputCS1msSwtMonInfoEsc.BlfSwtMon_Esc, UtExpected_Ioc_InputCS1msSwtMonInfoEsc_BlfSwtMon_Esc[i]);
        TEST_ASSERT_EQUAL(Ioc_InputCS1msSwtMonInfoEsc.ClutchSwtMon_Esc, UtExpected_Ioc_InputCS1msSwtMonInfoEsc_ClutchSwtMon_Esc[i]);
        TEST_ASSERT_EQUAL(Ioc_InputCS1msSwtMonInfoEsc.ItpmsSwtMon_Esc, UtExpected_Ioc_InputCS1msSwtMonInfoEsc_ItpmsSwtMon_Esc[i]);
        TEST_ASSERT_EQUAL(Ioc_InputCS1msRlyMonInfo.RlyDbcMon, UtExpected_Ioc_InputCS1msRlyMonInfo_RlyDbcMon[i]);
        TEST_ASSERT_EQUAL(Ioc_InputCS1msRlyMonInfo.RlyEssMon, UtExpected_Ioc_InputCS1msRlyMonInfo_RlyEssMon[i]);
        TEST_ASSERT_EQUAL(Ioc_InputCS1msRlyMonInfo.RlyFault, UtExpected_Ioc_InputCS1msRlyMonInfo_RlyFault[i]);
        TEST_ASSERT_EQUAL(Ioc_InputCS1msSwLineMonInfo.Sw1LineCMon, UtExpected_Ioc_InputCS1msSwLineMonInfo_Sw1LineCMon[i]);
        TEST_ASSERT_EQUAL(Ioc_InputCS1msSwLineMonInfo.Sw2LineBMon, UtExpected_Ioc_InputCS1msSwLineMonInfo_Sw2LineBMon[i]);
        TEST_ASSERT_EQUAL(Ioc_InputCS1msSwLineMonInfo.Sw3LineEMon, UtExpected_Ioc_InputCS1msSwLineMonInfo_Sw3LineEMon[i]);
        TEST_ASSERT_EQUAL(Ioc_InputCS1msSwLineMonInfo.Sw4LineFMon, UtExpected_Ioc_InputCS1msSwLineMonInfo_Sw4LineFMon[i]);
        TEST_ASSERT_EQUAL(Ioc_InputCS1msRsmDbcMon, UtExpected_Ioc_InputCS1msRsmDbcMon[i]);
        TEST_ASSERT_EQUAL(Ioc_InputCS1msPbcVdaAi, UtExpected_Ioc_InputCS1msPbcVdaAi[i]);
    }
}

TEST_GROUP_RUNNER(Ioc_InputCS1ms)
{
    RUN_TEST_CASE(Ioc_InputCS1ms, All);
}
