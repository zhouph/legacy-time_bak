#include "unity.h"
#include "unity_fixture.h"
#include "Ioc_OutputSR1ms.h"
#include "Ioc_OutputSR1ms_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Saluint16 UtInput_Ioc_OutputSR1msWhlVlvDrvInfo_FlOvDrvData[MAX_STEP] = IOC_OUTPUTSR1MSWHLVLVDRVINFO_FLOVDRVDATA;
const Saluint16 UtInput_Ioc_OutputSR1msWhlVlvDrvInfo_FlIvDrvData[MAX_STEP] = IOC_OUTPUTSR1MSWHLVLVDRVINFO_FLIVDRVDATA;
const Saluint16 UtInput_Ioc_OutputSR1msWhlVlvDrvInfo_FrOvDrvData[MAX_STEP] = IOC_OUTPUTSR1MSWHLVLVDRVINFO_FROVDRVDATA;
const Saluint16 UtInput_Ioc_OutputSR1msWhlVlvDrvInfo_FrIvDrvData[MAX_STEP] = IOC_OUTPUTSR1MSWHLVLVDRVINFO_FRIVDRVDATA;
const Saluint16 UtInput_Ioc_OutputSR1msWhlVlvDrvInfo_RlOvDrvData[MAX_STEP] = IOC_OUTPUTSR1MSWHLVLVDRVINFO_RLOVDRVDATA;
const Saluint16 UtInput_Ioc_OutputSR1msWhlVlvDrvInfo_RlIvDrvData[MAX_STEP] = IOC_OUTPUTSR1MSWHLVLVDRVINFO_RLIVDRVDATA;
const Saluint16 UtInput_Ioc_OutputSR1msWhlVlvDrvInfo_RrOvDrvData[MAX_STEP] = IOC_OUTPUTSR1MSWHLVLVDRVINFO_RROVDRVDATA;
const Saluint16 UtInput_Ioc_OutputSR1msWhlVlvDrvInfo_RrIvDrvData[MAX_STEP] = IOC_OUTPUTSR1MSWHLVLVDRVINFO_RRIVDRVDATA;
const Saluint16 UtInput_Ioc_OutputSR1msBalVlvDrvInfo_PrimBalVlvDrvData[MAX_STEP] = IOC_OUTPUTSR1MSBALVLVDRVINFO_PRIMBALVLVDRVDATA;
const Saluint16 UtInput_Ioc_OutputSR1msBalVlvDrvInfo_PressDumpVlvDrvData[MAX_STEP] = IOC_OUTPUTSR1MSBALVLVDRVINFO_PRESSDUMPVLVDRVDATA;
const Saluint16 UtInput_Ioc_OutputSR1msBalVlvDrvInfo_ChmbBalVlvDrvData[MAX_STEP] = IOC_OUTPUTSR1MSBALVLVDRVINFO_CHMBBALVLVDRVDATA;
const Saluint8 UtInput_Ioc_OutputSR1msFsrCbsDrvInfo_FsrCbsDrv[MAX_STEP] = IOC_OUTPUTSR1MSFSRCBSDRVINFO_FSRCBSDRV;
const Saluint8 UtInput_Ioc_OutputSR1msFsrAbsDrvInfo_FsrAbsDrv[MAX_STEP] = IOC_OUTPUTSR1MSFSRABSDRVINFO_FSRABSDRV;
const Mom_HndlrEcuModeSts_t UtInput_Ioc_OutputSR1msEcuModeSts[MAX_STEP] = IOC_OUTPUTSR1MSECUMODESTS;
const Acm_MainAcmAsicInitCompleteFlag_t UtInput_Ioc_OutputSR1msAcmAsicInitCompleteFlag[MAX_STEP] = IOC_OUTPUTSR1MSACMASICINITCOMPLETEFLAG;
const Eem_SuspcDetnFuncInhibitIocSts_t UtInput_Ioc_OutputSR1msFuncInhibitIocSts[MAX_STEP] = IOC_OUTPUTSR1MSFUNCINHIBITIOCSTS;

const Haluint8 UtExpected_Ioc_OutputSR1msIocDcMtrDutyData_MtrDutyDataFlg[MAX_STEP] = IOC_OUTPUTSR1MSIOCDCMTRDUTYDATA_MTRDUTYDATAFLG;
const Haluint8 UtExpected_Ioc_OutputSR1msIocDcMtrDutyData_ACH_Tx8_PUMP_DTY_PWM[MAX_STEP] = IOC_OUTPUTSR1MSIOCDCMTRDUTYDATA_ACH_TX8_PUMP_DTY_PWM;
const Haluint8 UtExpected_Ioc_OutputSR1msIocDcMtrFreqData_MtrFreqDataFlg[MAX_STEP] = IOC_OUTPUTSR1MSIOCDCMTRFREQDATA_MTRFREQDATAFLG;
const Haluint8 UtExpected_Ioc_OutputSR1msIocDcMtrFreqData_ACH_Tx8_PUMP_TCK_PWM[MAX_STEP] = IOC_OUTPUTSR1MSIOCDCMTRFREQDATA_ACH_TX8_PUMP_TCK_PWM;
const Haluint8 UtExpected_Ioc_OutputSR1msIocVlvNo0Data_VlvNo0DataFlg[MAX_STEP] = IOC_OUTPUTSR1MSIOCVLVNO0DATA_VLVNO0DATAFLG;
const Haluint16 UtExpected_Ioc_OutputSR1msIocVlvNo0Data_ACH_TxValve_SET_POINT[MAX_STEP] = IOC_OUTPUTSR1MSIOCVLVNO0DATA_ACH_TXVALVE_SET_POINT;
const Haluint8 UtExpected_Ioc_OutputSR1msIocVlvNo1Data_VlvNo1DataFlg[MAX_STEP] = IOC_OUTPUTSR1MSIOCVLVNO1DATA_VLVNO1DATAFLG;
const Haluint16 UtExpected_Ioc_OutputSR1msIocVlvNo1Data_ACH_TxValve_SET_POINT[MAX_STEP] = IOC_OUTPUTSR1MSIOCVLVNO1DATA_ACH_TXVALVE_SET_POINT;
const Haluint8 UtExpected_Ioc_OutputSR1msIocVlvNo2Data_VlvNo2DataFlg[MAX_STEP] = IOC_OUTPUTSR1MSIOCVLVNO2DATA_VLVNO2DATAFLG;
const Haluint16 UtExpected_Ioc_OutputSR1msIocVlvNo2Data_ACH_TxValve_SET_POINT[MAX_STEP] = IOC_OUTPUTSR1MSIOCVLVNO2DATA_ACH_TXVALVE_SET_POINT;
const Haluint8 UtExpected_Ioc_OutputSR1msIocVlvNo3Data_VlvNo3DataFlg[MAX_STEP] = IOC_OUTPUTSR1MSIOCVLVNO3DATA_VLVNO3DATAFLG;
const Haluint16 UtExpected_Ioc_OutputSR1msIocVlvNo3Data_ACH_TxValve_SET_POINT[MAX_STEP] = IOC_OUTPUTSR1MSIOCVLVNO3DATA_ACH_TXVALVE_SET_POINT;
const Haluint8 UtExpected_Ioc_OutputSR1msIocVlvTc0Data_VlvTc0DataFlg[MAX_STEP] = IOC_OUTPUTSR1MSIOCVLVTC0DATA_VLVTC0DATAFLG;
const Haluint16 UtExpected_Ioc_OutputSR1msIocVlvTc0Data_ACH_TxValve_SET_POINT[MAX_STEP] = IOC_OUTPUTSR1MSIOCVLVTC0DATA_ACH_TXVALVE_SET_POINT;
const Haluint8 UtExpected_Ioc_OutputSR1msIocVlvTc1Data_VlvTc1DataFlg[MAX_STEP] = IOC_OUTPUTSR1MSIOCVLVTC1DATA_VLVTC1DATAFLG;
const Haluint16 UtExpected_Ioc_OutputSR1msIocVlvTc1Data_ACH_TxValve_SET_POINT[MAX_STEP] = IOC_OUTPUTSR1MSIOCVLVTC1DATA_ACH_TXVALVE_SET_POINT;
const Haluint8 UtExpected_Ioc_OutputSR1msIocVlvEsv0Data_VlvEsv0DataFlg[MAX_STEP] = IOC_OUTPUTSR1MSIOCVLVESV0DATA_VLVESV0DATAFLG;
const Haluint16 UtExpected_Ioc_OutputSR1msIocVlvEsv0Data_ACH_TxValve_SET_POINT[MAX_STEP] = IOC_OUTPUTSR1MSIOCVLVESV0DATA_ACH_TXVALVE_SET_POINT;
const Haluint8 UtExpected_Ioc_OutputSR1msIocVlvEsv1Data_VlvEsv1DataFlg[MAX_STEP] = IOC_OUTPUTSR1MSIOCVLVESV1DATA_VLVESV1DATAFLG;
const Haluint16 UtExpected_Ioc_OutputSR1msIocVlvEsv1Data_ACH_TxValve_SET_POINT[MAX_STEP] = IOC_OUTPUTSR1MSIOCVLVESV1DATA_ACH_TXVALVE_SET_POINT;
const Haluint8 UtExpected_Ioc_OutputSR1msIocVlvNc0Data_VlvNc0DataFlg[MAX_STEP] = IOC_OUTPUTSR1MSIOCVLVNC0DATA_VLVNC0DATAFLG;
const Haluint16 UtExpected_Ioc_OutputSR1msIocVlvNc0Data_ACH_TxValve_SET_POINT[MAX_STEP] = IOC_OUTPUTSR1MSIOCVLVNC0DATA_ACH_TXVALVE_SET_POINT;
const Haluint8 UtExpected_Ioc_OutputSR1msIocVlvNc1Data_VlvNc1DataFlg[MAX_STEP] = IOC_OUTPUTSR1MSIOCVLVNC1DATA_VLVNC1DATAFLG;
const Haluint16 UtExpected_Ioc_OutputSR1msIocVlvNc1Data_ACH_TxValve_SET_POINT[MAX_STEP] = IOC_OUTPUTSR1MSIOCVLVNC1DATA_ACH_TXVALVE_SET_POINT;
const Haluint8 UtExpected_Ioc_OutputSR1msIocVlvNc2Data_VlvNc2DataFlg[MAX_STEP] = IOC_OUTPUTSR1MSIOCVLVNC2DATA_VLVNC2DATAFLG;
const Haluint16 UtExpected_Ioc_OutputSR1msIocVlvNc2Data_ACH_TxValve_SET_POINT[MAX_STEP] = IOC_OUTPUTSR1MSIOCVLVNC2DATA_ACH_TXVALVE_SET_POINT;
const Haluint8 UtExpected_Ioc_OutputSR1msIocVlvNc3Data_VlvNc3DataFlg[MAX_STEP] = IOC_OUTPUTSR1MSIOCVLVNC3DATA_VLVNC3DATAFLG;
const Haluint16 UtExpected_Ioc_OutputSR1msIocVlvNc3Data_ACH_TxValve_SET_POINT[MAX_STEP] = IOC_OUTPUTSR1MSIOCVLVNC3DATA_ACH_TXVALVE_SET_POINT;
const Haluint8 UtExpected_Ioc_OutputSR1msIocVlvRelayData_VlvRelayDataFlg[MAX_STEP] = IOC_OUTPUTSR1MSIOCVLVRELAYDATA_VLVRELAYDATAFLG;
const Haluint8 UtExpected_Ioc_OutputSR1msIocVlvRelayData_ACH_Tx1_FS_CMD[MAX_STEP] = IOC_OUTPUTSR1MSIOCVLVRELAYDATA_ACH_TX1_FS_CMD;
const Haluint8 UtExpected_Ioc_OutputSR1msIocAdcSelWriteData_AdcSelDataFlg[MAX_STEP] = IOC_OUTPUTSR1MSIOCADCSELWRITEDATA_ADCSELDATAFLG;
const Haluint8 UtExpected_Ioc_OutputSR1msIocAdcSelWriteData_ACH_Tx13_ADC_SEL[MAX_STEP] = IOC_OUTPUTSR1MSIOCADCSELWRITEDATA_ACH_TX13_ADC_SEL;



TEST_GROUP(Ioc_OutputSR1ms);
TEST_SETUP(Ioc_OutputSR1ms)
{
    Ioc_OutputSR1ms_Init();
}

TEST_TEAR_DOWN(Ioc_OutputSR1ms)
{   /* Postcondition */

}

TEST(Ioc_OutputSR1ms, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Ioc_OutputSR1msWhlVlvDrvInfo.FlOvDrvData = UtInput_Ioc_OutputSR1msWhlVlvDrvInfo_FlOvDrvData[i];
        Ioc_OutputSR1msWhlVlvDrvInfo.FlIvDrvData = UtInput_Ioc_OutputSR1msWhlVlvDrvInfo_FlIvDrvData[i];
        Ioc_OutputSR1msWhlVlvDrvInfo.FrOvDrvData = UtInput_Ioc_OutputSR1msWhlVlvDrvInfo_FrOvDrvData[i];
        Ioc_OutputSR1msWhlVlvDrvInfo.FrIvDrvData = UtInput_Ioc_OutputSR1msWhlVlvDrvInfo_FrIvDrvData[i];
        Ioc_OutputSR1msWhlVlvDrvInfo.RlOvDrvData = UtInput_Ioc_OutputSR1msWhlVlvDrvInfo_RlOvDrvData[i];
        Ioc_OutputSR1msWhlVlvDrvInfo.RlIvDrvData = UtInput_Ioc_OutputSR1msWhlVlvDrvInfo_RlIvDrvData[i];
        Ioc_OutputSR1msWhlVlvDrvInfo.RrOvDrvData = UtInput_Ioc_OutputSR1msWhlVlvDrvInfo_RrOvDrvData[i];
        Ioc_OutputSR1msWhlVlvDrvInfo.RrIvDrvData = UtInput_Ioc_OutputSR1msWhlVlvDrvInfo_RrIvDrvData[i];
        Ioc_OutputSR1msBalVlvDrvInfo.PrimBalVlvDrvData = UtInput_Ioc_OutputSR1msBalVlvDrvInfo_PrimBalVlvDrvData[i];
        Ioc_OutputSR1msBalVlvDrvInfo.PressDumpVlvDrvData = UtInput_Ioc_OutputSR1msBalVlvDrvInfo_PressDumpVlvDrvData[i];
        Ioc_OutputSR1msBalVlvDrvInfo.ChmbBalVlvDrvData = UtInput_Ioc_OutputSR1msBalVlvDrvInfo_ChmbBalVlvDrvData[i];
        Ioc_OutputSR1msFsrCbsDrvInfo.FsrCbsDrv = UtInput_Ioc_OutputSR1msFsrCbsDrvInfo_FsrCbsDrv[i];
        Ioc_OutputSR1msFsrAbsDrvInfo.FsrAbsDrv = UtInput_Ioc_OutputSR1msFsrAbsDrvInfo_FsrAbsDrv[i];
        Ioc_OutputSR1msEcuModeSts = UtInput_Ioc_OutputSR1msEcuModeSts[i];
        Ioc_OutputSR1msAcmAsicInitCompleteFlag = UtInput_Ioc_OutputSR1msAcmAsicInitCompleteFlag[i];
        Ioc_OutputSR1msFuncInhibitIocSts = UtInput_Ioc_OutputSR1msFuncInhibitIocSts[i];

        Ioc_OutputSR1ms();

        TEST_ASSERT_EQUAL(Ioc_OutputSR1msIocDcMtrDutyData.MtrDutyDataFlg, UtExpected_Ioc_OutputSR1msIocDcMtrDutyData_MtrDutyDataFlg[i]);
        TEST_ASSERT_EQUAL(Ioc_OutputSR1msIocDcMtrDutyData.ACH_Tx8_PUMP_DTY_PWM, UtExpected_Ioc_OutputSR1msIocDcMtrDutyData_ACH_Tx8_PUMP_DTY_PWM[i]);
        TEST_ASSERT_EQUAL(Ioc_OutputSR1msIocDcMtrFreqData.MtrFreqDataFlg, UtExpected_Ioc_OutputSR1msIocDcMtrFreqData_MtrFreqDataFlg[i]);
        TEST_ASSERT_EQUAL(Ioc_OutputSR1msIocDcMtrFreqData.ACH_Tx8_PUMP_TCK_PWM, UtExpected_Ioc_OutputSR1msIocDcMtrFreqData_ACH_Tx8_PUMP_TCK_PWM[i]);
        TEST_ASSERT_EQUAL(Ioc_OutputSR1msIocVlvNo0Data.VlvNo0DataFlg, UtExpected_Ioc_OutputSR1msIocVlvNo0Data_VlvNo0DataFlg[i]);
        TEST_ASSERT_EQUAL(Ioc_OutputSR1msIocVlvNo0Data.ACH_TxValve_SET_POINT, UtExpected_Ioc_OutputSR1msIocVlvNo0Data_ACH_TxValve_SET_POINT[i]);
        TEST_ASSERT_EQUAL(Ioc_OutputSR1msIocVlvNo1Data.VlvNo1DataFlg, UtExpected_Ioc_OutputSR1msIocVlvNo1Data_VlvNo1DataFlg[i]);
        TEST_ASSERT_EQUAL(Ioc_OutputSR1msIocVlvNo1Data.ACH_TxValve_SET_POINT, UtExpected_Ioc_OutputSR1msIocVlvNo1Data_ACH_TxValve_SET_POINT[i]);
        TEST_ASSERT_EQUAL(Ioc_OutputSR1msIocVlvNo2Data.VlvNo2DataFlg, UtExpected_Ioc_OutputSR1msIocVlvNo2Data_VlvNo2DataFlg[i]);
        TEST_ASSERT_EQUAL(Ioc_OutputSR1msIocVlvNo2Data.ACH_TxValve_SET_POINT, UtExpected_Ioc_OutputSR1msIocVlvNo2Data_ACH_TxValve_SET_POINT[i]);
        TEST_ASSERT_EQUAL(Ioc_OutputSR1msIocVlvNo3Data.VlvNo3DataFlg, UtExpected_Ioc_OutputSR1msIocVlvNo3Data_VlvNo3DataFlg[i]);
        TEST_ASSERT_EQUAL(Ioc_OutputSR1msIocVlvNo3Data.ACH_TxValve_SET_POINT, UtExpected_Ioc_OutputSR1msIocVlvNo3Data_ACH_TxValve_SET_POINT[i]);
        TEST_ASSERT_EQUAL(Ioc_OutputSR1msIocVlvTc0Data.VlvTc0DataFlg, UtExpected_Ioc_OutputSR1msIocVlvTc0Data_VlvTc0DataFlg[i]);
        TEST_ASSERT_EQUAL(Ioc_OutputSR1msIocVlvTc0Data.ACH_TxValve_SET_POINT, UtExpected_Ioc_OutputSR1msIocVlvTc0Data_ACH_TxValve_SET_POINT[i]);
        TEST_ASSERT_EQUAL(Ioc_OutputSR1msIocVlvTc1Data.VlvTc1DataFlg, UtExpected_Ioc_OutputSR1msIocVlvTc1Data_VlvTc1DataFlg[i]);
        TEST_ASSERT_EQUAL(Ioc_OutputSR1msIocVlvTc1Data.ACH_TxValve_SET_POINT, UtExpected_Ioc_OutputSR1msIocVlvTc1Data_ACH_TxValve_SET_POINT[i]);
        TEST_ASSERT_EQUAL(Ioc_OutputSR1msIocVlvEsv0Data.VlvEsv0DataFlg, UtExpected_Ioc_OutputSR1msIocVlvEsv0Data_VlvEsv0DataFlg[i]);
        TEST_ASSERT_EQUAL(Ioc_OutputSR1msIocVlvEsv0Data.ACH_TxValve_SET_POINT, UtExpected_Ioc_OutputSR1msIocVlvEsv0Data_ACH_TxValve_SET_POINT[i]);
        TEST_ASSERT_EQUAL(Ioc_OutputSR1msIocVlvEsv1Data.VlvEsv1DataFlg, UtExpected_Ioc_OutputSR1msIocVlvEsv1Data_VlvEsv1DataFlg[i]);
        TEST_ASSERT_EQUAL(Ioc_OutputSR1msIocVlvEsv1Data.ACH_TxValve_SET_POINT, UtExpected_Ioc_OutputSR1msIocVlvEsv1Data_ACH_TxValve_SET_POINT[i]);
        TEST_ASSERT_EQUAL(Ioc_OutputSR1msIocVlvNc0Data.VlvNc0DataFlg, UtExpected_Ioc_OutputSR1msIocVlvNc0Data_VlvNc0DataFlg[i]);
        TEST_ASSERT_EQUAL(Ioc_OutputSR1msIocVlvNc0Data.ACH_TxValve_SET_POINT, UtExpected_Ioc_OutputSR1msIocVlvNc0Data_ACH_TxValve_SET_POINT[i]);
        TEST_ASSERT_EQUAL(Ioc_OutputSR1msIocVlvNc1Data.VlvNc1DataFlg, UtExpected_Ioc_OutputSR1msIocVlvNc1Data_VlvNc1DataFlg[i]);
        TEST_ASSERT_EQUAL(Ioc_OutputSR1msIocVlvNc1Data.ACH_TxValve_SET_POINT, UtExpected_Ioc_OutputSR1msIocVlvNc1Data_ACH_TxValve_SET_POINT[i]);
        TEST_ASSERT_EQUAL(Ioc_OutputSR1msIocVlvNc2Data.VlvNc2DataFlg, UtExpected_Ioc_OutputSR1msIocVlvNc2Data_VlvNc2DataFlg[i]);
        TEST_ASSERT_EQUAL(Ioc_OutputSR1msIocVlvNc2Data.ACH_TxValve_SET_POINT, UtExpected_Ioc_OutputSR1msIocVlvNc2Data_ACH_TxValve_SET_POINT[i]);
        TEST_ASSERT_EQUAL(Ioc_OutputSR1msIocVlvNc3Data.VlvNc3DataFlg, UtExpected_Ioc_OutputSR1msIocVlvNc3Data_VlvNc3DataFlg[i]);
        TEST_ASSERT_EQUAL(Ioc_OutputSR1msIocVlvNc3Data.ACH_TxValve_SET_POINT, UtExpected_Ioc_OutputSR1msIocVlvNc3Data_ACH_TxValve_SET_POINT[i]);
        TEST_ASSERT_EQUAL(Ioc_OutputSR1msIocVlvRelayData.VlvRelayDataFlg, UtExpected_Ioc_OutputSR1msIocVlvRelayData_VlvRelayDataFlg[i]);
        TEST_ASSERT_EQUAL(Ioc_OutputSR1msIocVlvRelayData.ACH_Tx1_FS_CMD, UtExpected_Ioc_OutputSR1msIocVlvRelayData_ACH_Tx1_FS_CMD[i]);
        TEST_ASSERT_EQUAL(Ioc_OutputSR1msIocAdcSelWriteData.AdcSelDataFlg, UtExpected_Ioc_OutputSR1msIocAdcSelWriteData_AdcSelDataFlg[i]);
        TEST_ASSERT_EQUAL(Ioc_OutputSR1msIocAdcSelWriteData.ACH_Tx13_ADC_SEL, UtExpected_Ioc_OutputSR1msIocAdcSelWriteData_ACH_Tx13_ADC_SEL[i]);
    }
}

TEST_GROUP_RUNNER(Ioc_OutputSR1ms)
{
    RUN_TEST_CASE(Ioc_OutputSR1ms, All);
}
