/* all_test.c */
#include <stdio.h>
#include "unity_fixture.h"

static void RunAllTests(void)
{
    RUN_TEST_GROUP(Ioc_InputSR50us);
    RUN_TEST_GROUP(Ioc_OutputCS50us);
    RUN_TEST_GROUP(Ioc_InputSR1ms);
    RUN_TEST_GROUP(Ioc_InputCS1ms);
    RUN_TEST_GROUP(Ioc_OutputSR1ms);
    RUN_TEST_GROUP(Ioc_OutputCS1ms);
    RUN_TEST_GROUP(Ioc_InputSR5ms);
    RUN_TEST_GROUP(Ioc_OutputCS5ms);
}

int main(int argc, char * argv[])
{
    int retUnityMain = 0;

    retUnityMain = UnityMain(argc, argv, RunAllTests);

    return retUnityMain;
}
