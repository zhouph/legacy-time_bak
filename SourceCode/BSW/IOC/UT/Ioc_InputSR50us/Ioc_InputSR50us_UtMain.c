#include "unity.h"
#include "unity_fixture.h"
#include "Ioc_InputSR50us.h"
#include "Ioc_InputSR50us_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Haluint16 UtInput_Ioc_InputSR50usMpsD1IIFAngInfo_D1IIFAngRawData[MAX_STEP] = IOC_INPUTSR50USMPSD1IIFANGINFO_D1IIFANGRAWDATA;
const Haluint16 UtInput_Ioc_InputSR50usMpsD1IIFAngInfo_D1IIFAngDegree[MAX_STEP] = IOC_INPUTSR50USMPSD1IIFANGINFO_D1IIFANGDEGREE;
const Haluint16 UtInput_Ioc_InputSR50usMpsD2IIFAngInfo_D2IIFAngRawData[MAX_STEP] = IOC_INPUTSR50USMPSD2IIFANGINFO_D2IIFANGRAWDATA;
const Haluint16 UtInput_Ioc_InputSR50usMpsD2IIFAngInfo_D2IIFAngDegree[MAX_STEP] = IOC_INPUTSR50USMPSD2IIFANGINFO_D2IIFANGDEGREE;
const Haluint16 UtInput_Ioc_InputSR50usHwTrigMotInfo_Uphase0Mon[MAX_STEP] = IOC_INPUTSR50USHWTRIGMOTINFO_UPHASE0MON;
const Haluint16 UtInput_Ioc_InputSR50usHwTrigMotInfo_Uphase1Mon[MAX_STEP] = IOC_INPUTSR50USHWTRIGMOTINFO_UPHASE1MON;
const Haluint16 UtInput_Ioc_InputSR50usHwTrigMotInfo_Vphase0Mon[MAX_STEP] = IOC_INPUTSR50USHWTRIGMOTINFO_VPHASE0MON;
const Haluint16 UtInput_Ioc_InputSR50usHwTrigMotInfo_VPhase1Mon[MAX_STEP] = IOC_INPUTSR50USHWTRIGMOTINFO_VPHASE1MON;
const Mom_HndlrEcuModeSts_t UtInput_Ioc_InputSR50usEcuModeSts[MAX_STEP] = IOC_INPUTSR50USECUMODESTS;
const Eem_SuspcDetnFuncInhibitIocSts_t UtInput_Ioc_InputSR50usFuncInhibitIocSts[MAX_STEP] = IOC_INPUTSR50USFUNCINHIBITIOCSTS;

const Haluint16 UtExpected_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhUMon0[MAX_STEP] = IOC_INPUTSR50USMOTCURRMONINFO_MOTCURRPHUMON0;
const Haluint16 UtExpected_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhUMon1[MAX_STEP] = IOC_INPUTSR50USMOTCURRMONINFO_MOTCURRPHUMON1;
const Haluint16 UtExpected_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhVMon0[MAX_STEP] = IOC_INPUTSR50USMOTCURRMONINFO_MOTCURRPHVMON0;
const Haluint16 UtExpected_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhVMon1[MAX_STEP] = IOC_INPUTSR50USMOTCURRMONINFO_MOTCURRPHVMON1;
const Haluint16 UtExpected_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle1deg[MAX_STEP] = IOC_INPUTSR50USMOTANGLEMONINFO_MOTPOSIANGLE1DEG;
const Haluint16 UtExpected_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle2deg[MAX_STEP] = IOC_INPUTSR50USMOTANGLEMONINFO_MOTPOSIANGLE2DEG;
const Haluint16 UtExpected_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle1raw[MAX_STEP] = IOC_INPUTSR50USMOTANGLEMONINFO_MOTPOSIANGLE1RAW;
const Haluint16 UtExpected_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle2raw[MAX_STEP] = IOC_INPUTSR50USMOTANGLEMONINFO_MOTPOSIANGLE2RAW;



TEST_GROUP(Ioc_InputSR50us);
TEST_SETUP(Ioc_InputSR50us)
{
    Ioc_InputSR50us_Init();
}

TEST_TEAR_DOWN(Ioc_InputSR50us)
{   /* Postcondition */

}

TEST(Ioc_InputSR50us, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Ioc_InputSR50usMpsD1IIFAngInfo.D1IIFAngRawData = UtInput_Ioc_InputSR50usMpsD1IIFAngInfo_D1IIFAngRawData[i];
        Ioc_InputSR50usMpsD1IIFAngInfo.D1IIFAngDegree = UtInput_Ioc_InputSR50usMpsD1IIFAngInfo_D1IIFAngDegree[i];
        Ioc_InputSR50usMpsD2IIFAngInfo.D2IIFAngRawData = UtInput_Ioc_InputSR50usMpsD2IIFAngInfo_D2IIFAngRawData[i];
        Ioc_InputSR50usMpsD2IIFAngInfo.D2IIFAngDegree = UtInput_Ioc_InputSR50usMpsD2IIFAngInfo_D2IIFAngDegree[i];
        Ioc_InputSR50usHwTrigMotInfo.Uphase0Mon = UtInput_Ioc_InputSR50usHwTrigMotInfo_Uphase0Mon[i];
        Ioc_InputSR50usHwTrigMotInfo.Uphase1Mon = UtInput_Ioc_InputSR50usHwTrigMotInfo_Uphase1Mon[i];
        Ioc_InputSR50usHwTrigMotInfo.Vphase0Mon = UtInput_Ioc_InputSR50usHwTrigMotInfo_Vphase0Mon[i];
        Ioc_InputSR50usHwTrigMotInfo.VPhase1Mon = UtInput_Ioc_InputSR50usHwTrigMotInfo_VPhase1Mon[i];
        Ioc_InputSR50usEcuModeSts = UtInput_Ioc_InputSR50usEcuModeSts[i];
        Ioc_InputSR50usFuncInhibitIocSts = UtInput_Ioc_InputSR50usFuncInhibitIocSts[i];

        Ioc_InputSR50us();

        TEST_ASSERT_EQUAL(Ioc_InputSR50usMotCurrMonInfo.MotCurrPhUMon0, UtExpected_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhUMon0[i]);
        TEST_ASSERT_EQUAL(Ioc_InputSR50usMotCurrMonInfo.MotCurrPhUMon1, UtExpected_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhUMon1[i]);
        TEST_ASSERT_EQUAL(Ioc_InputSR50usMotCurrMonInfo.MotCurrPhVMon0, UtExpected_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhVMon0[i]);
        TEST_ASSERT_EQUAL(Ioc_InputSR50usMotCurrMonInfo.MotCurrPhVMon1, UtExpected_Ioc_InputSR50usMotCurrMonInfo_MotCurrPhVMon1[i]);
        TEST_ASSERT_EQUAL(Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle1deg, UtExpected_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle1deg[i]);
        TEST_ASSERT_EQUAL(Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle2deg, UtExpected_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle2deg[i]);
        TEST_ASSERT_EQUAL(Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle1raw, UtExpected_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle1raw[i]);
        TEST_ASSERT_EQUAL(Ioc_InputSR50usMotAngleMonInfo.MotPosiAngle2raw, UtExpected_Ioc_InputSR50usMotAngleMonInfo_MotPosiAngle2raw[i]);
    }
}

TEST_GROUP_RUNNER(Ioc_InputSR50us)
{
    RUN_TEST_CASE(Ioc_InputSR50us, All);
}
