#include "unity.h"
#include "unity_fixture.h"
#include "Ioc_OutputCS50us.h"
#include "Ioc_OutputCS50us_UtMain.h"


#define MAX_STEP    UT_MAX_STEP

const Saluint16 UtInput_Ioc_OutputCS50usMotPwmDataInfo_MotPwmPhUhighData[MAX_STEP] = IOC_OUTPUTCS50USMOTPWMDATAINFO_MOTPWMPHUHIGHDATA;
const Saluint16 UtInput_Ioc_OutputCS50usMotPwmDataInfo_MotPwmPhVhighData[MAX_STEP] = IOC_OUTPUTCS50USMOTPWMDATAINFO_MOTPWMPHVHIGHDATA;
const Saluint16 UtInput_Ioc_OutputCS50usMotPwmDataInfo_MotPwmPhWhighData[MAX_STEP] = IOC_OUTPUTCS50USMOTPWMDATAINFO_MOTPWMPHWHIGHDATA;
const Saluint16 UtInput_Ioc_OutputCS50usMotPwmDataInfo_MotPwmPhUlowData[MAX_STEP] = IOC_OUTPUTCS50USMOTPWMDATAINFO_MOTPWMPHULOWDATA;
const Saluint16 UtInput_Ioc_OutputCS50usMotPwmDataInfo_MotPwmPhVlowData[MAX_STEP] = IOC_OUTPUTCS50USMOTPWMDATAINFO_MOTPWMPHVLOWDATA;
const Saluint16 UtInput_Ioc_OutputCS50usMotPwmDataInfo_MotPwmPhWlowData[MAX_STEP] = IOC_OUTPUTCS50USMOTPWMDATAINFO_MOTPWMPHWLOWDATA;
const Mom_HndlrEcuModeSts_t UtInput_Ioc_OutputCS50usEcuModeSts[MAX_STEP] = IOC_OUTPUTCS50USECUMODESTS;
const Eem_SuspcDetnFuncInhibitIocSts_t UtInput_Ioc_OutputCS50usFuncInhibitIocSts[MAX_STEP] = IOC_OUTPUTCS50USFUNCINHIBITIOCSTS;




TEST_GROUP(Ioc_OutputCS50us);
TEST_SETUP(Ioc_OutputCS50us)
{
    Ioc_OutputCS50us_Init();
}

TEST_TEAR_DOWN(Ioc_OutputCS50us)
{   /* Postcondition */

}

TEST(Ioc_OutputCS50us, All)
{   /* Test case */
    int i = 0;

    for(i=0; i<MAX_STEP; i++)
    {
        Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhUhighData = UtInput_Ioc_OutputCS50usMotPwmDataInfo_MotPwmPhUhighData[i];
        Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhVhighData = UtInput_Ioc_OutputCS50usMotPwmDataInfo_MotPwmPhVhighData[i];
        Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhWhighData = UtInput_Ioc_OutputCS50usMotPwmDataInfo_MotPwmPhWhighData[i];
        Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhUlowData = UtInput_Ioc_OutputCS50usMotPwmDataInfo_MotPwmPhUlowData[i];
        Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhVlowData = UtInput_Ioc_OutputCS50usMotPwmDataInfo_MotPwmPhVlowData[i];
        Ioc_OutputCS50usMotPwmDataInfo.MotPwmPhWlowData = UtInput_Ioc_OutputCS50usMotPwmDataInfo_MotPwmPhWlowData[i];
        Ioc_OutputCS50usEcuModeSts = UtInput_Ioc_OutputCS50usEcuModeSts[i];
        Ioc_OutputCS50usFuncInhibitIocSts = UtInput_Ioc_OutputCS50usFuncInhibitIocSts[i];

        Ioc_OutputCS50us();

    }
}

TEST_GROUP_RUNNER(Ioc_OutputCS50us)
{
    RUN_TEST_CASE(Ioc_OutputCS50us, All);
}
