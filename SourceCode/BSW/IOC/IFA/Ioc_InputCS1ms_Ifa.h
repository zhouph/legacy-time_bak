/**
 * @defgroup Ioc_InputCS1ms_Ifa Ioc_InputCS1ms_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_InputCS1ms_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef IOC_INPUTCS1MS_IFA_H_
#define IOC_INPUTCS1MS_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Ioc_InputCS1ms_Read_Ioc_InputCS1msEcuModeSts(data) do \
{ \
    *data = Ioc_InputCS1msEcuModeSts; \
}while(0);

#define Ioc_InputCS1ms_Read_Ioc_InputCS1msAcmAsicInitCompleteFlag(data) do \
{ \
    *data = Ioc_InputCS1msAcmAsicInitCompleteFlag; \
}while(0);

#define Ioc_InputCS1ms_Read_Ioc_InputCS1msFuncInhibitIocSts(data) do \
{ \
    *data = Ioc_InputCS1msFuncInhibitIocSts; \
}while(0);


/* Set Output DE MAcro Function */
#define Ioc_InputCS1ms_Write_Ioc_InputCS1msSwtMonInfo(data) do \
{ \
    Ioc_InputCS1msSwtMonInfo = *data; \
}while(0);

#define Ioc_InputCS1ms_Write_Ioc_InputCS1msSwtMonInfoEsc(data) do \
{ \
    Ioc_InputCS1msSwtMonInfoEsc = *data; \
}while(0);

#define Ioc_InputCS1ms_Write_Ioc_InputCS1msRlyMonInfo(data) do \
{ \
    Ioc_InputCS1msRlyMonInfo = *data; \
}while(0);

#define Ioc_InputCS1ms_Write_Ioc_InputCS1msSwLineMonInfo(data) do \
{ \
    Ioc_InputCS1msSwLineMonInfo = *data; \
}while(0);

#define Ioc_InputCS1ms_Write_Ioc_InputCS1msSwtMonInfo_AvhSwtMon(data) do \
{ \
    Ioc_InputCS1msSwtMonInfo.AvhSwtMon = *data; \
}while(0);

#define Ioc_InputCS1ms_Write_Ioc_InputCS1msSwtMonInfo_BflSwtMon(data) do \
{ \
    Ioc_InputCS1msSwtMonInfo.BflSwtMon = *data; \
}while(0);

#define Ioc_InputCS1ms_Write_Ioc_InputCS1msSwtMonInfo_BlsSwtMon(data) do \
{ \
    Ioc_InputCS1msSwtMonInfo.BlsSwtMon = *data; \
}while(0);

#define Ioc_InputCS1ms_Write_Ioc_InputCS1msSwtMonInfo_BsSwtMon(data) do \
{ \
    Ioc_InputCS1msSwtMonInfo.BsSwtMon = *data; \
}while(0);

#define Ioc_InputCS1ms_Write_Ioc_InputCS1msSwtMonInfo_DoorSwtMon(data) do \
{ \
    Ioc_InputCS1msSwtMonInfo.DoorSwtMon = *data; \
}while(0);

#define Ioc_InputCS1ms_Write_Ioc_InputCS1msSwtMonInfo_EscSwtMon(data) do \
{ \
    Ioc_InputCS1msSwtMonInfo.EscSwtMon = *data; \
}while(0);

#define Ioc_InputCS1ms_Write_Ioc_InputCS1msSwtMonInfo_FlexBrkASwtMon(data) do \
{ \
    Ioc_InputCS1msSwtMonInfo.FlexBrkASwtMon = *data; \
}while(0);

#define Ioc_InputCS1ms_Write_Ioc_InputCS1msSwtMonInfo_FlexBrkBSwtMon(data) do \
{ \
    Ioc_InputCS1msSwtMonInfo.FlexBrkBSwtMon = *data; \
}while(0);

#define Ioc_InputCS1ms_Write_Ioc_InputCS1msSwtMonInfo_HzrdSwtMon(data) do \
{ \
    Ioc_InputCS1msSwtMonInfo.HzrdSwtMon = *data; \
}while(0);

#define Ioc_InputCS1ms_Write_Ioc_InputCS1msSwtMonInfo_HdcSwtMon(data) do \
{ \
    Ioc_InputCS1msSwtMonInfo.HdcSwtMon = *data; \
}while(0);

#define Ioc_InputCS1ms_Write_Ioc_InputCS1msSwtMonInfo_PbSwtMon(data) do \
{ \
    Ioc_InputCS1msSwtMonInfo.PbSwtMon = *data; \
}while(0);

#define Ioc_InputCS1ms_Write_Ioc_InputCS1msSwtMonInfoEsc_BlsSwtMon_Esc(data) do \
{ \
    Ioc_InputCS1msSwtMonInfoEsc.BlsSwtMon_Esc = *data; \
}while(0);

#define Ioc_InputCS1ms_Write_Ioc_InputCS1msSwtMonInfoEsc_AvhSwtMon_Esc(data) do \
{ \
    Ioc_InputCS1msSwtMonInfoEsc.AvhSwtMon_Esc = *data; \
}while(0);

#define Ioc_InputCS1ms_Write_Ioc_InputCS1msSwtMonInfoEsc_EscSwtMon_Esc(data) do \
{ \
    Ioc_InputCS1msSwtMonInfoEsc.EscSwtMon_Esc = *data; \
}while(0);

#define Ioc_InputCS1ms_Write_Ioc_InputCS1msSwtMonInfoEsc_HdcSwtMon_Esc(data) do \
{ \
    Ioc_InputCS1msSwtMonInfoEsc.HdcSwtMon_Esc = *data; \
}while(0);

#define Ioc_InputCS1ms_Write_Ioc_InputCS1msSwtMonInfoEsc_PbSwtMon_Esc(data) do \
{ \
    Ioc_InputCS1msSwtMonInfoEsc.PbSwtMon_Esc = *data; \
}while(0);

#define Ioc_InputCS1ms_Write_Ioc_InputCS1msSwtMonInfoEsc_GearRSwtMon_Esc(data) do \
{ \
    Ioc_InputCS1msSwtMonInfoEsc.GearRSwtMon_Esc = *data; \
}while(0);

#define Ioc_InputCS1ms_Write_Ioc_InputCS1msSwtMonInfoEsc_BlfSwtMon_Esc(data) do \
{ \
    Ioc_InputCS1msSwtMonInfoEsc.BlfSwtMon_Esc = *data; \
}while(0);

#define Ioc_InputCS1ms_Write_Ioc_InputCS1msSwtMonInfoEsc_ClutchSwtMon_Esc(data) do \
{ \
    Ioc_InputCS1msSwtMonInfoEsc.ClutchSwtMon_Esc = *data; \
}while(0);

#define Ioc_InputCS1ms_Write_Ioc_InputCS1msSwtMonInfoEsc_ItpmsSwtMon_Esc(data) do \
{ \
    Ioc_InputCS1msSwtMonInfoEsc.ItpmsSwtMon_Esc = *data; \
}while(0);

#define Ioc_InputCS1ms_Write_Ioc_InputCS1msRlyMonInfo_RlyDbcMon(data) do \
{ \
    Ioc_InputCS1msRlyMonInfo.RlyDbcMon = *data; \
}while(0);

#define Ioc_InputCS1ms_Write_Ioc_InputCS1msRlyMonInfo_RlyEssMon(data) do \
{ \
    Ioc_InputCS1msRlyMonInfo.RlyEssMon = *data; \
}while(0);

#define Ioc_InputCS1ms_Write_Ioc_InputCS1msRlyMonInfo_RlyFault(data) do \
{ \
    Ioc_InputCS1msRlyMonInfo.RlyFault = *data; \
}while(0);

#define Ioc_InputCS1ms_Write_Ioc_InputCS1msSwLineMonInfo_Sw1LineCMon(data) do \
{ \
    Ioc_InputCS1msSwLineMonInfo.Sw1LineCMon = *data; \
}while(0);

#define Ioc_InputCS1ms_Write_Ioc_InputCS1msSwLineMonInfo_Sw2LineBMon(data) do \
{ \
    Ioc_InputCS1msSwLineMonInfo.Sw2LineBMon = *data; \
}while(0);

#define Ioc_InputCS1ms_Write_Ioc_InputCS1msSwLineMonInfo_Sw3LineEMon(data) do \
{ \
    Ioc_InputCS1msSwLineMonInfo.Sw3LineEMon = *data; \
}while(0);

#define Ioc_InputCS1ms_Write_Ioc_InputCS1msSwLineMonInfo_Sw4LineFMon(data) do \
{ \
    Ioc_InputCS1msSwLineMonInfo.Sw4LineFMon = *data; \
}while(0);

#define Ioc_InputCS1ms_Write_Ioc_InputCS1msRsmDbcMon(data) do \
{ \
    Ioc_InputCS1msRsmDbcMon = *data; \
}while(0);

#define Ioc_InputCS1ms_Write_Ioc_InputCS1msPbcVdaAi(data) do \
{ \
    Ioc_InputCS1msPbcVdaAi = *data; \
}while(0);

/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* IOC_INPUTCS1MS_IFA_H_ */
/** @} */
