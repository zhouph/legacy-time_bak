/**
 * @defgroup Ioc_OutputCS1ms_Ifa Ioc_OutputCS1ms_Ifa
 * @{
 */
/*------------------------------------------------------------------------------
 * Copyright (c) 2014 Mando Corp.
 *----------------------------------------------------------------------------*/
/***************************************************************************//**
 * @file        Ioc_OutputCS1ms_Ifa.h
 * @brief       Template file
 * @date        2015. 2. 5.
 ******************************************************************************/

#ifndef IOC_OUTPUTCS1MS_IFA_H_
#define IOC_OUTPUTCS1MS_IFA_H_

/*==============================================================================
 *                  INCLUDE FILES
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL MACROS AND DEFINES
 =============================================================================*/
/* Get Input DE Macro Function */
#define Ioc_OutputCS1ms_Read_Ioc_OutputCS1msNormVlvDrvInfo(data) do \
{ \
    *data = Ioc_OutputCS1msNormVlvDrvInfo; \
}while(0);

#define Ioc_OutputCS1ms_Read_Ioc_OutputCS1msWhlVlvDrvInfo(data) do \
{ \
    *data = Ioc_OutputCS1msWhlVlvDrvInfo; \
}while(0);

#define Ioc_OutputCS1ms_Read_Ioc_OutputCS1msRlyDrvInfo(data) do \
{ \
    *data = Ioc_OutputCS1msRlyDrvInfo; \
}while(0);

#define Ioc_OutputCS1ms_Read_Ioc_OutputCS1msFsrAbsDrvInfo(data) do \
{ \
    *data = Ioc_OutputCS1msFsrAbsDrvInfo; \
}while(0);

#define Ioc_OutputCS1ms_Read_Ioc_OutputCS1msFsrCbsDrvInfo(data) do \
{ \
    *data = Ioc_OutputCS1msFsrCbsDrvInfo; \
}while(0);

#define Ioc_OutputCS1ms_Read_Ioc_OutputCS1msSenPwrMonitorData(data) do \
{ \
    *data = Ioc_OutputCS1msSenPwrMonitorData; \
}while(0);

#define Ioc_OutputCS1ms_Read_Ioc_OutputCS1msNormVlvDrvInfo_PrimCutVlvDrvData(data) do \
{ \
    *data = Ioc_OutputCS1msNormVlvDrvInfo.PrimCutVlvDrvData; \
}while(0);

#define Ioc_OutputCS1ms_Read_Ioc_OutputCS1msNormVlvDrvInfo_RelsVlvDrvData(data) do \
{ \
    *data = Ioc_OutputCS1msNormVlvDrvInfo.RelsVlvDrvData; \
}while(0);

#define Ioc_OutputCS1ms_Read_Ioc_OutputCS1msNormVlvDrvInfo_SecdCutVlvDrvData(data) do \
{ \
    *data = Ioc_OutputCS1msNormVlvDrvInfo.SecdCutVlvDrvData; \
}while(0);

#define Ioc_OutputCS1ms_Read_Ioc_OutputCS1msNormVlvDrvInfo_CircVlvDrvData(data) do \
{ \
    *data = Ioc_OutputCS1msNormVlvDrvInfo.CircVlvDrvData; \
}while(0);

#define Ioc_OutputCS1ms_Read_Ioc_OutputCS1msNormVlvDrvInfo_SimVlvDrvData(data) do \
{ \
    *data = Ioc_OutputCS1msNormVlvDrvInfo.SimVlvDrvData; \
}while(0);

#define Ioc_OutputCS1ms_Read_Ioc_OutputCS1msWhlVlvDrvInfo_FlOvDrvData(data) do \
{ \
    *data = Ioc_OutputCS1msWhlVlvDrvInfo.FlOvDrvData; \
}while(0);

#define Ioc_OutputCS1ms_Read_Ioc_OutputCS1msWhlVlvDrvInfo_FlIvDrvData(data) do \
{ \
    *data = Ioc_OutputCS1msWhlVlvDrvInfo.FlIvDrvData; \
}while(0);

#define Ioc_OutputCS1ms_Read_Ioc_OutputCS1msWhlVlvDrvInfo_FrOvDrvData(data) do \
{ \
    *data = Ioc_OutputCS1msWhlVlvDrvInfo.FrOvDrvData; \
}while(0);

#define Ioc_OutputCS1ms_Read_Ioc_OutputCS1msWhlVlvDrvInfo_FrIvDrvData(data) do \
{ \
    *data = Ioc_OutputCS1msWhlVlvDrvInfo.FrIvDrvData; \
}while(0);

#define Ioc_OutputCS1ms_Read_Ioc_OutputCS1msWhlVlvDrvInfo_RlOvDrvData(data) do \
{ \
    *data = Ioc_OutputCS1msWhlVlvDrvInfo.RlOvDrvData; \
}while(0);

#define Ioc_OutputCS1ms_Read_Ioc_OutputCS1msWhlVlvDrvInfo_RlIvDrvData(data) do \
{ \
    *data = Ioc_OutputCS1msWhlVlvDrvInfo.RlIvDrvData; \
}while(0);

#define Ioc_OutputCS1ms_Read_Ioc_OutputCS1msWhlVlvDrvInfo_RrOvDrvData(data) do \
{ \
    *data = Ioc_OutputCS1msWhlVlvDrvInfo.RrOvDrvData; \
}while(0);

#define Ioc_OutputCS1ms_Read_Ioc_OutputCS1msWhlVlvDrvInfo_RrIvDrvData(data) do \
{ \
    *data = Ioc_OutputCS1msWhlVlvDrvInfo.RrIvDrvData; \
}while(0);

#define Ioc_OutputCS1ms_Read_Ioc_OutputCS1msRlyDrvInfo_RlyDbcDrv(data) do \
{ \
    *data = Ioc_OutputCS1msRlyDrvInfo.RlyDbcDrv; \
}while(0);

#define Ioc_OutputCS1ms_Read_Ioc_OutputCS1msRlyDrvInfo_RlyEssDrv(data) do \
{ \
    *data = Ioc_OutputCS1msRlyDrvInfo.RlyEssDrv; \
}while(0);

#define Ioc_OutputCS1ms_Read_Ioc_OutputCS1msFsrAbsDrvInfo_FsrAbsOff(data) do \
{ \
    *data = Ioc_OutputCS1msFsrAbsDrvInfo.FsrAbsOff; \
}while(0);

#define Ioc_OutputCS1ms_Read_Ioc_OutputCS1msFsrCbsDrvInfo_FsrCbsOff(data) do \
{ \
    *data = Ioc_OutputCS1msFsrCbsDrvInfo.FsrCbsOff; \
}while(0);

#define Ioc_OutputCS1ms_Read_Ioc_OutputCS1msSenPwrMonitorData_SenPwrM_12V_Drive_Req(data) do \
{ \
    *data = Ioc_OutputCS1msSenPwrMonitorData.SenPwrM_12V_Drive_Req; \
}while(0);

#define Ioc_OutputCS1ms_Read_Ioc_OutputCS1msEcuModeSts(data) do \
{ \
    *data = Ioc_OutputCS1msEcuModeSts; \
}while(0);

#define Ioc_OutputCS1ms_Read_Ioc_OutputCS1msAcmAsicInitCompleteFlag(data) do \
{ \
    *data = Ioc_OutputCS1msAcmAsicInitCompleteFlag; \
}while(0);

#define Ioc_OutputCS1ms_Read_Ioc_OutputCS1msFuncInhibitIocSts(data) do \
{ \
    *data = Ioc_OutputCS1msFuncInhibitIocSts; \
}while(0);

#define Ioc_OutputCS1ms_Read_Ioc_OutputCS1msMainCanEn(data) do \
{ \
    *data = Ioc_OutputCS1msMainCanEn; \
}while(0);

#define Ioc_OutputCS1ms_Read_Ioc_OutputCS1msVlvDrvEnRst(data) do \
{ \
    *data = Ioc_OutputCS1msVlvDrvEnRst; \
}while(0);

#define Ioc_OutputCS1ms_Read_Ioc_OutputCS1msAwdPrnDrv(data) do \
{ \
    *data = Ioc_OutputCS1msAwdPrnDrv; \
}while(0);

#define Ioc_OutputCS1ms_Read_Ioc_OutputCS1msFsrDcMtrShutDwn(data) do \
{ \
    *data = Ioc_OutputCS1msFsrDcMtrShutDwn; \
}while(0);

#define Ioc_OutputCS1ms_Read_Ioc_OutputCS1msFsrEnDrDrv(data) do \
{ \
    *data = Ioc_OutputCS1msFsrEnDrDrv; \
}while(0);


/* Set Output DE MAcro Function */
/*==============================================================================
 *                  GLOBAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL CONSTANT DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL VARIABLE DECLARATIONS
 =============================================================================*/

/*==============================================================================
 *                  GLOBAL FUNCTION PROTOTYPES
 =============================================================================*/

/*==============================================================================
 *                  END OF FILE
 =============================================================================*/
#endif /* IOC_OUTPUTCS1MS_IFA_H_ */
/** @} */
